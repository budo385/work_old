// create tab group
var tabGroup = Titanium.UI.createTabGroup();

Titanium.include('./body_replace.js','./application_customization.js');

var updateVersion=0;

//alert(Titanium.Locale.currentLanguage);

//First time create session varibles.
if(Ti.App.Properties.getInt('LANG_PICKER_ROW')==null)
{
    Ti.App.Properties.setInt('LANG_PICKER_ROW',0);
}
if(Ti.App.Properties.getString('TITLE_TEXT')==null)
{
    Ti.App.Properties.setString('TITLE_TEXT',setTitle(Ti.App.Properties.getInt('LANG_PICKER_ROW')));
}
//if(Ti.App.Properties.getString('MESSAGE_TEXT')==null)
//{
    //Ti.App.Properties.setString('MESSAGE_TEXT',setTitle(Ti.App.Properties.getInt('LANG_PICKER_ROW')));
//}
if(Ti.App.Properties.getInt('TEMPLATE_VERSION')==null)
{
    Ti.App.Properties.setInt('TEMPLATE_VERSION',0);
}
if(Ti.App.Properties.getString('TEMPLATES_XML')==null || Ti.App.Properties.getString('TEMPLATES_XML')=='')
{
    var f = Titanium.Filesystem.getFile(templatesXMLPath);
    var contents = f.read();
    Ti.App.Properties.setString('TEMPLATES_XML',contents.text);
}

//Check is device online.
function isOnline()
{
    if(!Titanium.Network.online)
    {
	var notOnlineDialog = Titanium.UI.createAlertDialog({
	    title:L("offline_message_title"),
	    message:L("offline_message_message"),
	    buttonNames:[L("ok")]
	});
	notOnlineDialog.show();
    }
    
}
isOnline();

// create base UI tab and root window
var win1 = Titanium.UI.createWindow({  
    title:L("designs"),
    backgroundGradient:{type:'linear',colors:[{color:backgroundColor,position:0.0}, {color:backgroundColorLighter,position:0.50},{color:backgroundColor,position:1.0}]},
    barColor:backgroundColor,
    tabBarHidden:true
});

if(Titanium.Platform.osname=='ipad')
{
    win1.orientationModes=[Titanium.UI.PORTRAIT,Titanium.UI.UPSIDE_PORTRAIT,Titanium.UI.LANDSCAPE_LEFT,Titanium.UI.LANDSCAPE_RIGHT];
}
else
{
    win1.orientationModes=[Titanium.UI.PORTRAIT];
}

var tab1 = Titanium.UI.createTab({  
    title:'Tab 1',
    window:win1
});

//  add tab
tabGroup.addTab(tab1);  

var ind=Titanium.UI.createProgressBar({
	width:200,
	min:0,
	max:1,
	value:0,
	height:70,
	color:'white',
	message:L("saving_templates"),
	font:{fontSize:14, fontWeight:'bold'},
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	top:10
});

var buttonObjects = [
	{title:'?', width:40}
];

function check_updates()
{
    var url=application_update_url;
    var xhr = Titanium.Network.createHTTPClient();
    xhr.timeout=5000;
    xhr.open("GET",url,false);
    xhr.send("");
    updateVersion=xhr.responseText;
    
    var templVersion=Ti.App.Properties.getString('TEMPLATE_VERSION');
    if(templVersion<updateVersion)
//    if(1)
    {
	buttonObjects = [
	    {title:'?', width:30},
	    {title:L("update"), width:'auto'}
	];
    }
}
check_updates();

var buttonBar = Titanium.UI.createButtonBar({
	labels:buttonObjects,
	backgroundColor:backgroundColor,
	style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

win1.leftNavButton=buttonBar;

var okButton = Titanium.UI.createButton({
	title:L("ok")
});

function deleteTemplates()
{
   var dirListing = templatesDir.getDirectoryListing();
    for (var i=0;i<dirListing.length;i++)  
    {
	var file=Ti.Filesystem.getFile(templatesPath+'/'+dirListing[i]);
        file.deleteFile();
    }
}

function updateTemplates()
{
    var url=application_xml_url+application_xml;
    var xhr = Titanium.Network.createHTTPClient();
    xhr.validateSecureCertificate = false;
    xhr.timeout=5000;
    xhr.onload = function()
    {
	//deleteTemplates();
	
	Ti.App.Properties.setString('TEMPLATES_XML',xhr.responseText);
        win1.fireEvent('templateDownloadComplete');
    };
    xhr.ondatastream = function(e)
    {
	ind.value = e.progress ;
    };
    xhr.error = function()
    {
        ind.hide();
	var alertErrorDialog = Titanium.UI.createAlertDialog({
	title:L("ok"),
	message:L("download_error_message_title"),
	buttonNames:[L("download_error_message_message")]
	});
	alertErrorDialog.show();
    };
    xhr.open("GET",url,false);
    xhr.send();

    ind.show();   
}

win1.rightNavButton=okButton;

var scrollableView = Titanium.UI.createScrollableView({
    clipViews:false,
    currentPage:0,
    backgroundColor:'transparent'
});

win1.add(scrollableView);

function loadTemplates()
{
    var templates=getTemplatesNames();
    var views=[];
    for(var i=0;i<templates.length;i++)
    {
	var view = Ti.UI.createView({
	    backgroundColor:'transparent'
	});
	var webview = Titanium.UI.createWebView({
	});

	view.templateName=templates[i];
	var html=getTemplateHTML(templates[i]);
    
	webview.html=html;
	webview.templateName=templates[i];
    
	view.add(webview);
	views.push(view);
    }

    scrollableView.views=views;
}

loadTemplates();

win1.addEventListener('templateDownloadComplete', function()
{
    loadTemplates();
    ind.hide();

    var alertDialog = Titanium.UI.createAlertDialog({
	title:L("downloaded_dialog_message"),
	buttonNames:[L("ok")]
    });
    alertDialog.show();
    Ti.App.Properties.setInt('TEMPLATE_VERSION',updateVersion);
    var bObjects = [
	{title:'?', width:40}
    ];
    buttonBar.labels=bObjects;
});

scrollableView.addEventListener('singletap', function(e)
{
    Ti.App.Properties.setString('TEMPLATE_NAME',scrollableView.views[scrollableView.currentPage].templateName);
    var w = Titanium.UI.createWindow({
        url:'./title_input.js'
    });
    tab1.open(w);
});

// move scroll view left
var left = Titanium.UI.createButton({
	image:'./images/navPrev.png'
});
left.addEventListener('click', function(e)
{
    var currentView=scrollableView.currentPage;
    if (currentView==0)
    {
	return;
    }
    currentView--;
    scrollableView.scrollToView(currentView);
});

// move scroll view right
var right = Titanium.UI.createButton({
	image:'./images/navNext.png'
});
right.addEventListener('click', function(e)
{
    var currentView=scrollableView.currentPage;
    if (currentView==(scrollableView.views.length-1))
    {
	return;
    }
    currentView++;
    scrollableView.scrollToView(currentView);
});

okButton.addEventListener('click', function(e)
{
    scrollableView.fireEvent('singletap');
});

Ti.App.addEventListener('languageChanged', function()
{
    loadTemplates();
});

buttonBar.addEventListener('click', function(e)
{
    if(!e.index)
    {
	var w = Titanium.UI.createWindow({
	    url:'./info.js'
	});
	var b = Titanium.UI.createButton({
		title:L("close"),
		style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});
	w.setLeftNavButton(b);
	b.addEventListener('click',function()
	{
		w.close();
	});
	w.open({modal:true});
    }
    else
    {
	updateTemplates();
    }
});

var flexSpace = Titanium.UI.createButton({
    systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

win1.setToolbar([left,flexSpace,ind,flexSpace,right]);

// open tab group
tabGroup.open();
