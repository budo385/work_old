Titanium.include('./application_customization.js');

var languageWin = Titanium.UI.currentWindow;
languageWin.barColor = backgroundColor;
languageWin.title=L("message_language");
languageWin.backgroundGradient={type:'linear',colors:[{color:backgroundColor,position:0.0}, {color:backgroundColorLighter,position:0.50},{color:backgroundColor,position:1.0}]};

var initValue;
if(Ti.App.Properties.getInt('LANG_PICKER_ROW')==null)
{
    initValue=0;
}
else
{
    initValue=Ti.App.Properties.getInt('LANG_PICKER_ROW');
}
var value;

var templatesPath = Titanium.Filesystem.resourcesDirectory+'/templates';

var picker = Ti.UI.createPicker();

var languagesXMLPath = Titanium.Filesystem.resourcesDirectory+'/languages.xml';
var f = Titanium.Filesystem.getFile(languagesXMLPath);
var contents = f.read();
var xml=Titanium.XML.parseString(contents.text);
var languages=xml.documentElement.getElementsByTagName("languages").item(0).getElementsByTagName("language");

var data = [];
for(var i=0;i<languages.length;i++)
{
    var language=languages.item(i).getElementsByTagName("name").item(0).text;
    var languageAbbreviation=languages.item(i).getElementsByTagName("abbreviation").item(0).text;
    
    data.push(Ti.UI.createPickerRow({title:L(language),custom_item:languageAbbreviation}));
}

picker.top=0;
picker.selectionIndicator=true;

picker.add(data);
languageWin.add(picker);

setTimeout(function(){ picker.setSelectedRow(0,Ti.App.Properties.getInt('LANG_PICKER_ROW'),true); },300);

picker.addEventListener('change', function(e)
{
    value=e.rowIndex;
    if(initValue!=value)
    {
	Ti.App.Properties.setInt('LANG_PICKER_ROW',value);
	Ti.App.fireEvent('languageChanged');
	//languageWin.close();
    }
});

