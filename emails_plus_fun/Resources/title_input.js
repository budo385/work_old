Titanium.include('./body_replace.js','./application_customization.js');

var titleWin = Titanium.UI.currentWindow;
titleWin.barColor = backgroundColor;
titleWin.title=L("title");
titleWin.backgroundGradient={type:'linear',colors:[{color:backgroundColor,position:0.0}, {color:backgroundColorLighter,position:0.50},{color:backgroundColor,position:1.0}]};
titleWin.templateName=Ti.App.Properties.getString('TEMPLATE_NAME');

if(Titanium.Platform.osname=='ipad')
{
    titleWin.orientationModes=[Titanium.UI.PORTRAIT,Titanium.UI.UPSIDE_PORTRAIT,Titanium.UI.LANDSCAPE_LEFT,Titanium.UI.LANDSCAPE_RIGHT];
}
else
{
    titleWin.orientationModes=[Titanium.UI.PORTRAIT];
}

var templatesPath = Titanium.Filesystem.resourcesDirectory+'/templates';

var buttonObjects = [];
if(Titanium.App.name!='emails_plus_fun')
{
    buttonObjects = [
	{image:'./images/Language16.png',width:50},
	{title:L("ok"), width:50}
    ];
}
else
{
    buttonObjects = [
	{title:L("ok"), width:50}
    ];
}

var buttonBar = Titanium.UI.createButtonBar({
    labels:buttonObjects,
    backgroundColor:backgroundColor,
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

titleWin.rightNavButton=buttonBar;

var titleTextField = Titanium.UI.createTextField({
    hintText:L("title_text"),
    height:35,
    top:30,
    left:10,
    right:10,
    zIndex:2,
    opacity:0.85,
    returnKeyType:Titanium.UI.RETURNKEY_DONE,
    font:{fontSize:17},
    borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED
});

if(Ti.App.Properties.getString('TITLE_TEXT')!=null)
{
    titleTextField.value=Ti.App.Properties.getString('TITLE_TEXT');
}
titleTextField.addEventListener('change', function(e)
{
    Ti.App.Properties.setString('TITLE_TEXT',e.value);
});

buttonBar.addEventListener('click', function(e)
{
    if(Titanium.App.name!='emails_plus_fun')
    {
	if(e.index)
	{
	    var w = Titanium.UI.createWindow({
		    url:'./message_input.js'
	    });
	    w.templateName = titleWin.templateName;
	    Titanium.UI.currentTab.open(w);
	}
	else
	{
	    var w1 = Titanium.UI.createWindow({
		url:'language_selector.js'
	    });
	    var b = Titanium.UI.createButton({
		    title:L("close"),
		    style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	    });
	    w1.templateName=titleWin.templateName;
	    w1.setLeftNavButton(b);
	    b.addEventListener('click',function()
	    {
		    w1.close();
	    });
	    w1.open({modal:true});
	}
    }
    else
    {
	var ww = Titanium.UI.createWindow({
		url:'./message_input.js'
	});
	ww.templateName = titleWin.templateName;
	Titanium.UI.currentTab.open(ww);
    }
});

Ti.App.addEventListener('languageChanged', function()
{
    titleTextField.value=setTitle(Ti.App.Properties.getInt('LANG_PICKER_ROW'));
    Ti.App.Properties.setString('TITLE_TEXT',titleTextField.value);
});

titleWin.add(titleTextField);

if(Titanium.App.name!='emails_plus_fun')
{
    var htmlPath= Titanium.Filesystem.resourcesDirectory+'/html';
    var f = Titanium.Filesystem.getFile(htmlPath, 'index.html');
    var contents = f.read();
    var html=contents.text;
    
    var webview = Titanium.UI.createWebView({
	html:html
    });
    
    titleWin.add(webview);
}