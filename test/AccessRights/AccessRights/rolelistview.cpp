#include "rolelistview.h"

RoleListView::RoleListView(QWidget *parent /*= 0*/)
	: QListView(parent)
{
	//Drag&Drop
	setAcceptDrops(false);
	setDragEnabled(true);

	//Selection
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectItems);

	//Tab navigation.
	setTabKeyNavigation(true);
}

RoleListView::~RoleListView()
{

}
