#include "persontreeview.h"
#include "rolelistview.h"
#include "gui_core/gui_core/rolelistmodel.h"

PersonTreeView::PersonTreeView(QWidget *parent /*= 0*/)
	: QTreeView(parent)
{
	//Header
	QHeaderView *Header = new QHeaderView(Qt::Horizontal);
	Header->setResizeMode(QHeaderView::Stretch);
	Header->hide();
	setHeader(Header);
	
	//Drag&Drop
	setAcceptDrops(true);
	setDragEnabled(true);
	setDropIndicatorShown(true);

	//Selection
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectItems);

	//Tab navigation.
	setTabKeyNavigation(true);
}

PersonTreeView::~PersonTreeView()
{

}

void PersonTreeView::dragMoveEvent(QDragMoveEvent *event)
{
	//Get drag index.
	QModelIndex index = indexAt(event->pos());
	
	//Can move only Roles not Persons.
	if (!index.parent().isValid())
		event->accept();
	else
		event->ignore();
}

void PersonTreeView::dragEnterEvent(QDragEnterEvent *event)
{
	//Check what are we dragging in.
	if (event->mimeData()->hasFormat("helix/roleitem"))
		event->accept();
	else
		event->ignore();
}

void PersonTreeView::dropEvent(QDropEvent *event)
{
	if (!event->mimeData()->hasFormat("helix/roleitem"))
	{
		event->ignore();
		return;
	}

	if (event->source() == this)
	{
		///Get destination index.
		QModelIndex index = indexAt(event->pos());

		//Check is index valid.
		if (!index.isValid())
		{
			event->ignore();
			return;
		}

		//Get destination Item.
		DbRecordSetItem *Item = static_cast<DbRecordSetItem*>(index.internalPointer());
		//Get parent children RowID list.
		QList<int> list = Item->GetChildrenRowIDList();

		//Get selected(dropped) index.
		QModelIndex SelectedIndex = this->selectionModel()->currentIndex();
		//Get Item.
		DbRecordSetItem *SelectedItem = static_cast<DbRecordSetItem*>(SelectedIndex.internalPointer());
		//Get dropped item RowID.
		int childRowID = SelectedItem->GetRowId();

		//If there is already that item in list Parent (that role in person) ignore drop.
		if (list.contains(childRowID) || index.parent().isValid())
		{
			event->ignore();
			return;
		}

		//Set dropped item to model.
		(static_cast<AccRightsTreeModel*> (model()))->SetDropedItem(SelectedItem);
		model()->dropMimeData(event->mimeData(), event->dropAction(), 0, 0, index);
		event->accept();
	}
	else
	{
		///Get destination index.
		QModelIndex index = indexAt(event->pos());
		//Check is index valid.
		if (!index.isValid())
		{
			event->ignore();
			return;
		}
		//Get destination Item.
		DbRecordSetItem *Item = static_cast<DbRecordSetItem*>(index.internalPointer());
		//Get parent children RowID list.
		QList<int> list = Item->GetChildrenRowIDList();

		//Get role view.
		RoleListView *ListView = (static_cast<RoleListView*> (event->source()));
		//Get role selected item index.
		QModelIndex RoleIndex = ListView->selectionModel()->currentIndex();
		//Get role item.
		DbRecordSetItem *RoleItem = static_cast<DbRecordSetItem*>(RoleIndex.internalPointer());
		int childRowID = RoleItem->GetRowId();

		//If there is already that item in list Parent (that role in person) ignore drop.
		if (list.contains(childRowID))
		{
			event->ignore();
			return;
		}

		//Set dropped item to model.
		(static_cast<AccRightsTreeModel*> (model()))->SetDropedItem(RoleItem);
		model()->dropMimeData(event->mimeData(), event->dropAction(), 0, 0, index);
		event->accept();
	}

	event->ignore();
}
