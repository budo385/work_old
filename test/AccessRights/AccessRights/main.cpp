#include <QtGui/QApplication>

#include "bus_client/bus_client/applicationstarter.h"
#include "accessrights.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	
	//init resources for all linked libraries 
	Q_INIT_RESOURCE(gui_core);

	//setup default params
	QString strServer = "127.0.0.1";	//default server
	if(argc > 1)
		strServer = argv[1];

	int nPort = 1111;					//default port
	if(argc > 2)
		nPort = atoi(argv[2]);

	AccessRights w;
	ApplicationStarter *App=new ApplicationStarter(&a,&w, strServer, nPort);

	QTimer::singleShot(0, App, SLOT(Start())); 

	a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
	
	return a.exec();
}
