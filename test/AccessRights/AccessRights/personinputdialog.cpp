#include "personinputdialog.h"

PersonInputDialog::PersonInputDialog(QString *PersonName, int *UserID, QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	m_pstrPersonName = PersonName;
	m_pnUserID = UserID;
	ui.PersonNameLineEdit->insert("New Person");
	ui.PersonNameLineEdit->setFocus(Qt::OtherFocusReason);
}

PersonInputDialog::~PersonInputDialog()
{

}


void PersonInputDialog::on_OkPushButton_clicked()
{
	if (ui.PersonNameLineEdit->text() == "")
	{
		QMessageBox::information(this, "Access Rights",
			"Person Name field empty.\n"
			"Please insert New person name.");
	}
	else
	{
		*m_pstrPersonName = ui.PersonNameLineEdit->text();
		*m_pnUserID = ui.UserIDSpinBox->value();
		accept();
	}
}


void PersonInputDialog::on_CancelPushButton_clicked()
{
	*m_pstrPersonName = "";
	*m_pnUserID = -1;
	reject();
}