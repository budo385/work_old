#ifndef USERWIDGET_H
#define USERWIDGET_H

#include <QWidget>
#include "ui_userwidget.h"

class UserWidget : public QWidget
{
    Q_OBJECT

public:
    UserWidget(QWidget *parent = 0);
    ~UserWidget();

private:
    Ui::UserWidgetClass ui;
};

#endif // USERWIDGET_H
