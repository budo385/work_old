#ifndef PERSONINPUTDIALOG_H
#define PERSONINPUTDIALOG_H

#include <QWidget>
#include <QMessageBox>
#include "ui_personinputdialog.h"

class PersonInputDialog : public QDialog
{
    Q_OBJECT

public:
    PersonInputDialog(QString *PersonName, int *UserID, QWidget *parent = 0);
    ~PersonInputDialog();

private:
    Ui::PersonInputDialogClass ui;

	QString *m_pstrPersonName;
	int		*m_pnUserID;

private slots:
	void on_CancelPushButton_clicked();
	void on_OkPushButton_clicked();
};

#endif // PERSONINPUTDIALOG_H
