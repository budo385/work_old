#ifndef PERSONTREEVIEW_H
#define PERSONTREEVIEW_H

#include <QTreeView>
#include <QHeaderView>
#include <QDragMoveEvent>
#include <QDragEnterEvent>
#include <QDebug>

#include "gui_core/gui_core/accrightstreemodel.h"
#include "gui_core/gui_core/dbrecordsetitem.h"

class PersonTreeView : public QTreeView
{
	Q_OBJECT

public:
    PersonTreeView(QWidget *parent = 0);
    ~PersonTreeView();

protected:
	void dragMoveEvent(QDragMoveEvent *event); 
	void dragEnterEvent(QDragEnterEvent *event);
	void dropEvent(QDropEvent *event);
};

#endif // PERSONTREEVIEW_H
