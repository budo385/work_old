#ifndef ROLELISTVIEW_H
#define ROLELISTVIEW_H

#include <QListView>
#include <QHeaderView>
#include <QDragLeaveEvent>
#include <QDebug>

#include "gui_core/gui_core/rolelistmodel.h"
#include "gui_core/gui_core/dbrecordsetitem.h"

class RoleListView : public QListView
{
	Q_OBJECT

public:
    RoleListView(QWidget *parent = 0);
    ~RoleListView();
};

#endif // ROLELISTVIEW_H
