#ifndef ACCESSRIGHTS_H
#define ACCESSRIGHTS_H

#include <QtGui/QMainWindow>
#include "ui_accessrights.h"
#include "userwidget.h"
#include "personwidget.h"

class AccessRights : public QMainWindow
{
    Q_OBJECT

public:
    AccessRights(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~AccessRights();

private:
    Ui::AccessRightsClass ui;
	UserWidget *User;
	PersonWidget *Person;
};

#endif // ACCESSRIGHTS_H
