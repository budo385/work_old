#include "personwidget.h"
extern Proxy_BusinessServiceSet *BoSet;

PersonWidget::PersonWidget(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);
	m_bSignalsConnected = false;

	m_strNewPersonName = "New Person";
	m_nNewPersonUserID = -1;

}

PersonWidget::~PersonWidget()
{
	delete(m_PersonsModel);
	delete(m_RoleModel);
}

void PersonWidget::LoadRoles(Status &pStatus, DbRecordSet &pPersonRoleRecordSet, SQLDBPointer* pDbConn /*= NULL*/)
{
	BoSet->app->AccessRights->GetRoleList(pStatus, pPersonRoleRecordSet, pDbConn);
	if(!pStatus.IsOK())
		qDebug() << pStatus.getErrorText();
	else
		qDebug() << "Person role list loaded.";
}

void PersonWidget::LoadPersons(Status &pStatus, DbRecordSet &pPersonsRecordSet, SQLDBPointer* pDbConn /*= NULL*/)
{
	BoSet->app->AccessRights->GetPersons(pStatus, pPersonsRecordSet, pDbConn);
	if(!pStatus.IsOK())
		qDebug() << pStatus.getErrorText();
	else
		qDebug() << "Persons list loaded.";
}

void PersonWidget::LoadPersonRoles(Status &pStatus, DbRecordSet &pPersonsRecordSet, int PersonID, SQLDBPointer* pDbConn /*= NULL*/)
{
	BoSet->app->AccessRights->GetPersonRoleList(pStatus, pPersonsRecordSet, PersonID, pDbConn);
	if(!pStatus.IsOK())
		qDebug() << pStatus.getErrorText();
	else
		qDebug() << "Persons list loaded.";
}

RoleListModel *PersonWidget::GetRoleModel()
{
	return m_RoleModel;
}

void PersonWidget::on_ReLoadPushButton_clicked()
{
	//Clear recordset.
	m_recRecordSet.clear();

	//Change cursor to wait.
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	//Load project list.
	LoadRoles(m_pStatus, m_recRecordSet);
	//Set model.
	m_RoleModel = new RoleListModel(this);
	m_RoleModel ->InitializeModel(&m_recRecordSet);
	//Clear recordset to free memory.
	m_recRecordSet.clear();

	//Load persons.
	LoadPersons(m_pStatus, m_recRecordSet);
	m_PersonsModel = new AccRightsTreeModel(this);
	m_PersonsModel ->InitializeModel(&m_recRecordSet);
	//Clear recordset to free memory.
	m_recRecordSet.clear();

	//Load Person roles.
	//QMultiHash<int, DbRecordSetItem*> PersonItems = m_PersonsModel->GetModelItems().values();
	QList<DbRecordSetItem *> ItemList = m_PersonsModel->GetModelItems()->values();
	int PersonsCount = ItemList.count();
	//Loop through items and load roles.
	for (int i = 0; i < PersonsCount; ++i)
	{
		DbRecordSetItem *Item = ItemList.at(i);
		//DbRecordSetItem Item(*PersonItems.value(i));
		int RowID = Item->GetRowId();
		LoadPersonRoles(m_pStatus, m_recRecordSet, RowID);
		if (m_recRecordSet.getRowCount() > 0)
			m_PersonsModel->AddDataToModel(m_recRecordSet);
		m_recRecordSet.clear();
	}

	//Set up model.
	ui.listView->setModel(m_RoleModel);
	ui.treeView->setModel(m_PersonsModel);

	ConnectSignals();

	//Restore cursor.
	QApplication::restoreOverrideCursor();
}

void PersonWidget::ConnectSignals()
{
	if (!m_bSignalsConnected)
	{
		//QObject::connect(ui.treeView->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(on_SelectionChange(const QModelIndex&, const QModelIndex&)));
		connect(ui.treeView, SIGNAL(expanded( const QModelIndex&)), this, SLOT(on_NodeExpanded(const QModelIndex&)));

		m_bSignalsConnected = true;
	}
}

void PersonWidget::on_NodeExpanded(const QModelIndex &ItemIndex)
{
	//Check if Item already has loaded children.
	int ItemChildCount = m_PersonsModel->GetLoadedChildrenCount(ItemIndex);

	//If children yet has not been loaded then go for loading.
	if (ItemChildCount == 0)
	{
		//Get item.
		DbRecordSetItem *Item = static_cast<DbRecordSetItem*>(ItemIndex.internalPointer());
		//Get item's row id.
		int ItemRowID = Item->GetRowId();

		//Change cursor to wait.
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

		//Load children.
		m_recRecordSet.clear();
		LoadPersonRoles(m_pStatus, m_recRecordSet, ItemRowID);

		//Look if Item has children, if not return.
		if (m_recRecordSet.getRowCount()<=0)
		{
			QApplication::restoreOverrideCursor();
			return;
		}
			
		//Add children to model.
		m_PersonsModel->AddDataToModel(m_recRecordSet);

		//Clear recordset to free memory.
		m_recRecordSet.clear();

		//Collapse and then again expand so that it can show new loaded items.
		ui.treeView->collapse(ItemIndex);
		ui.treeView->expand(ItemIndex);

		//Restore cursor.
		QApplication::restoreOverrideCursor();
	}

	//Clear memory
	m_recRecordSet.clear();
}

void PersonWidget::on_AddPushButton_clicked()
{
	PersonInputDialog *PersonInput = new PersonInputDialog(&m_strNewPersonName, &m_nNewPersonUserID);
	PersonInput->exec();
	
	if (PersonInput->Accepted)
		AddNewPerson();

	delete(PersonInput);
	return;
}

void PersonWidget::on_DeletePushButton_clicked()
{
	QModelIndex ItemIndex = ui.treeView->selectionModel()->currentIndex();

	//Check if item index is valid or is it really selected (or just remained selected from previous delete).
	if (!ItemIndex.isValid() || !ui.treeView->selectionModel()->isSelected(ItemIndex))
	{
		ui.treeView->selectionModel()->clear();
		return;
	}

	qDebug() << ItemIndex.data();

	m_PersonsModel->removeRows(ItemIndex.row(), 1, ItemIndex);

	ui.treeView->selectionModel()->clear();
}

void PersonWidget::AddNewPerson()
{
	if (m_nNewPersonUserID > 0)
		m_PersonsModel->AddNewPerson(m_strNewPersonName, m_nNewPersonUserID);
	
	m_nNewPersonUserID = -1;
}
