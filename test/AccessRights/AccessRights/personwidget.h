#ifndef PERSONWIDGET_H
#define PERSONWIDGET_H

#include <QWidget>
#include <QDialog>
#include <QDateTime>
#include <QHash>
#include <QCursor>

#include "ui_personwidget.h"

#include "bus_client/bus_client/bocliententity_testtree.h"
#include "bus_client/bus_client/proxy_businessserviceset.h"
#include "bus_client/bus_client/servermessagehandlerset.h"
#include "trans/trans/httpclientconnectionsettings.h"

#include "gui_core/gui_core/rolelistmodel.h"
#include "gui_core/gui_core/accrightstreemodel.h"
#include "gui_core/gui_core/fuibase.h"
#include "personinputdialog.h"

class PersonWidget : public QWidget
{
    Q_OBJECT

public:
    PersonWidget(QWidget *parent = 0);
    ~PersonWidget();

	void LoadRoles(Status &pStatus, DbRecordSet &pPersonRoleRecordSet, SQLDBPointer* pDbConn = NULL);
	void LoadPersons(Status &pStatus, DbRecordSet &pPersonsRecordSet, SQLDBPointer* pDbConn = NULL);
	void LoadPersonRoles(Status &pStatus, DbRecordSet &pPersonsRecordSet, int PersonID, SQLDBPointer* pDbConn = NULL);
	RoleListModel *GetRoleModel();

private:
	void ConnectSignals();
	void AddNewPerson();

    Ui::PersonWidgetClass	ui;
	Status					m_pStatus;
	DbRecordSet				m_recRecordSet;
	RoleListModel			*m_RoleModel;
	AccRightsTreeModel		*m_PersonsModel;
	bool					m_bSignalsConnected;
	QString					m_strNewPersonName;
	int						m_nNewPersonUserID;

private slots:
	void on_DeletePushButton_clicked();
	void on_AddPushButton_clicked();
	void on_ReLoadPushButton_clicked();
	void on_NodeExpanded(const QModelIndex&);
};

#endif // PERSONWIDGET_H
