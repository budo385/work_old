#ifndef WYSIWYG_TEST_H
#define WYSIWYG_TEST_H

#include <QtWidgets/QMainWindow>
#include "ui_wysiwyg_test.h"

class wysiwyg_test : public QMainWindow
{
	Q_OBJECT

public:
	wysiwyg_test(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~wysiwyg_test();

private:
	Ui::wysiwyg_testClass ui;

private slots:
	void on_reload_clicked();
	void on_reload_2_clicked();

};

#endif // WYSIWYG_TEST_H
