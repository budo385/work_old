#include "wysiwyg_test.h"

wysiwyg_test::wysiwyg_test(QWidget *parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	ui.lineEdit->setText("file:///c:/Work/SOKRATES_XP/test/wysiwyg_test/wysiwyg_test/tinymce/examples/index.html");
	ui.lineEdit_2->setText("file:///c:/Work/SOKRATES_XP/test/wysiwyg_test/wysiwyg_test/ckeditor/_samples/index.html");
//	ui.webView_1->load(QUrl::fromLocalFile("file:///c:/Work/SOKRATES_XP/test/wysiwyg_test/wysiwyg_test/tinymce/examples/index.html"));
//	ui.webView_2->load(QUrl::fromLocalFile("file:///c:/Work/SOKRATES_XP/test/wysiwyg_test/wysiwyg_test/ckeditor/_samples/api_marko_test1.html"));
	ui.webView_1->load(QUrl::fromLocalFile("C:/Work/test/wysiwyg_test/wysiwyg_test/tinymce/examples/index.html"));
	ui.webView_2->load(QUrl::fromLocalFile("C:/Work/test/wysiwyg_test/wysiwyg_test/ckeditor/_samples/api_marko_test1.html"));

	QWebSettings::globalSettings()->setAttribute(QWebSettings::JavascriptEnabled, true);
	ui.webView->settings()->setAttribute(QWebSettings::JavascriptEnabled, true);
	ui.webView->settings()->setAttribute(QWebSettings::PluginsEnabled, true);
	ui.webView_1->settings()->setAttribute(QWebSettings::JavascriptEnabled, true);
	ui.webView_1->settings()->setAttribute(QWebSettings::PluginsEnabled, true);
	ui.webView_2->settings()->setAttribute(QWebSettings::JavascriptEnabled, true);
	ui.webView_2->settings()->setAttribute(QWebSettings::PluginsEnabled, true);

	ui.webView->load(QUrl("http://88.198.201.40:810/socrates/myPARM/"));
}

wysiwyg_test::~wysiwyg_test()
{

}

void wysiwyg_test::on_reload_clicked()
{
	QString ssa;
	ssa = ui.lineEdit->text();

	ui.webView_1->load(QUrl(ssa));
}

void wysiwyg_test::on_reload_2_clicked()
{
	QString ssa;
	ssa = ui.lineEdit_2->text();

	ui.webView_2->load(QUrl(ssa));
}
