#include <QCoreApplication>
#include <QProcess>
#include <QTimer>
#include <QString>
#include <QStringList>
#include <QDebug>
#include <QFileInfo>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	QStringList lst=a.arguments();

	if (lst.size()<2)
	{
		qDebug()<<"Usage: AutoRun.exe path_to_executable [exe params]";
		QTimer::singleShot(0,&a,SLOT(quit()));
	}
	else
	{
		QString strApp=lst.at(1);
		QFileInfo info2(strApp);
		QProcess p;
		p.setWorkingDirectory(info2.absolutePath());
		lst.removeFirst();
		lst.removeFirst();
		p.startDetached(strApp,lst);
		QTimer::singleShot(0,&a,SLOT(quit()));
	}


	return a.exec();
}
