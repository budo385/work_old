#include <QtCore/QCoreApplication>


#include <QTimer>
#include "app.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


	App Appx;
	QTimer::singleShot(0, &Appx, SLOT(Start())); 

    return a.exec();
}
