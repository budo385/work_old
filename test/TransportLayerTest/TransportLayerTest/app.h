#ifndef APP_H
#define APP_H

#include "trans/trans/httpreadersync.h"
#include <QObject>
#include <QtCore>


class App : public QObject
{
	Q_OBJECT

public:
    App(QObject *parent=0);
    ~App();

public slots:

	void Start();

private:
    
};

#endif // APP_H
