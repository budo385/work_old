#ifndef MY_WIDGET_H
#define MY_WIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QPainter>

class My_Widget : public QWidget
{
	Q_OBJECT

public:
    My_Widget(QWidget *parent);
    ~My_Widget();

private:
	void paintEvent ( QPaintEvent *);
    
};

#endif // MY_WIDGET_H
