/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*
    treemodel.cpp

    Provides a simple tree model to show how to create and use hierarchical
    models.
*/

#include <QtGui>

#include "treeitem.h"
#include "treemodel.h"

TreeModel::TreeModel(const QString &data, QObject *parent)
    : QAbstractItemModel(parent)
{
    QList<QVariant> rootData;
    rootData << "Title";
    rootItem = new TreeItem(rootData);
    setupModelData(data.split(QString("\n")), rootItem);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

	if (!index.isValid())
        return QVariant();

	//Icons.
	if (role == Qt::DecorationRole)
	{
		//If first item don't put icon.
		if (index.row() == 0 && !index.parent().isValid())
			return QVariant();

		//Load icons in pixmap.
		QPixmap pixmap(":4");
		//Create one with double width.
		QPixmap pix1(QSize(16,16));
		pix1.fill(QColor(255,255,255,0));

		//Initialize painter object with pix1map.
		QPainter painter(&pix1);
		//Draw pix1maps one next to other.
		painter.drawPixmap(0, 0,pixmap);

		//Create icon.
		QIcon icon(pix1);

		return QVariant(icon);
	}

	//Font decoration.
	if (role == Qt::FontRole)
	{
		QFont font;
		font.setStyle(QFont::StyleItalic);
		font.setFamily("Helvetica");
		font.setBold(true);
		font.setPointSize(10);
		return QVariant(font);
	}

	if (role != Qt::DisplayRole)
        return QVariant();

    return item->data(index.column());
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

void TreeModel::setupModelData(const QStringList &lines, TreeItem *parent)
{
    QList<TreeItem*> parents;
    QList<int> indentations;
    parents << parent;

	QList<QVariant> RootData;
	RootData << "Data";
	TreeItem *root_item = new TreeItem(RootData, parents.first());
	parents.first()->appendChild(root_item);

	for (int row = 1; row < 4; row++)
	{
		QList<QVariant> Data;
		Data << "Data";

		TreeItem *item = new TreeItem(Data, root_item);
		root_item->appendChild(item);

		if (row == 3)
		{
			TreeItem *c_item = new TreeItem(Data, item);
			item->appendChild(c_item);

			TreeItem *cc_item = new TreeItem(Data, c_item);
			c_item->appendChild(cc_item);

		}
	}
}
