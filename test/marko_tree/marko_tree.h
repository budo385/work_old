#ifndef MARKO_TREE_H
#define MARKO_TREE_H

#include <QtGui/QMainWindow>
#include <QStandardItemModel>
#include <QPushButton>

#include "ui_marko_tree.h"
#include "my_widget.h"
#include "treemodel.h"

class marko_tree : public QMainWindow
{
    Q_OBJECT

public:
    marko_tree(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~marko_tree();

private:
    Ui::marko_treeClass ui;
};

#endif // MARKO_TREE_H
