#include <QtGui/QApplication>
#include "marko_tree.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    marko_tree w;
    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
    return a.exec();
}
