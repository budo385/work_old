#include "test_restrpc.h"
#include <QUrl>
#include <QMessageBox>
#include <QDebug>
#include <QTimer>
#include <QFile>
#include <QDateTime>

typedef bool (*tExecuteCall)(int *, char *,int, const char *,const char *,const char *, const char *,const char *,const char *,char *, int*);



Test_RestRPC::Test_RestRPC(QWidget *parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);
	ui.txtURL->setText("http://192.168.200.32:23000/rest");
	ui.ckbSC->setChecked(true);
}

Test_RestRPC::~Test_RestRPC()
{

}

void Test_RestRPC::GoWithDLL(int nLoginType, QString strUser,QString strPass,QString strHost, QString strMethod,QString strPath)
{
	//load DLL (if not already):
	if (!m_OrganizerDLL.isLoaded())
	{
		m_OrganizerDLL.setFileName("RestConnect");
		//m_OrganizerDLL.setFileName("db_actualize");
		if(!m_OrganizerDLL.load())
		{
			QMessageBox::warning(this,"Error","Failed to load dll");
			return;
		}
	}

	int nError;
	char szError[4096];

	tExecuteCall pfExecuteCall = (tExecuteCall) m_OrganizerDLL.resolve("ExecuteCall");
	if (pfExecuteCall)
	{
		int nRespBufferSize=10000000;
		m_RespBuffer.resize(nRespBufferSize);

		bool bOK=pfExecuteCall(&nError,szError,nLoginType,strUser.toLocal8Bit().constData(),strPass.toLocal8Bit().constData(),strHost.toLocal8Bit().constData(),strMethod.toLocal8Bit().constData(),strPath.toLocal8Bit().constData(),m_ReqBuffer.constData(),m_RespBuffer.data(),&nRespBufferSize);

		if (!bOK)
		{
			m_RespBuffer.clear();
			QString strError(szError);
			QMessageBox::warning(this,"Error",strError);
			return;
		}
		else
		{
			m_RespBuffer.resize(nRespBufferSize);
			ui.txtResponse->setPlainText(QString(m_RespBuffer));
		}
	}
	else
	{
		QMessageBox::warning(this,"Error","Failed to load dll");
		return ;
	}
}

void Test_RestRPC::on_btnGo_clicked()
{
	//GoWithDLL();
	
	//construct QHTTP object, make request, login: except 401, then make hash
	ui.txtRequest->clear();
	ui.txtResponse->clear();
	m_RespBuffer.clear();

	m_bGetRequest=true;
	MakeGetRequest();
}

void Test_RestRPC::on_btnPost_clicked()
{
	//construct QHTTP object, make request, login: except 401, then make hash
	ui.txtRequest->clear();
	ui.txtResponse->clear();
	m_RespBuffer.clear();

	m_bGetRequest=false; //means POST
	MakeGetRequest();
}



void Test_RestRPC::MakeGetRequest(QString stAuthToken)
{
	QUrl url(ui.txtURL->text());
	int nPort=url.port();
	QString strHost=url.scheme()+"://"+url.host()+":"+QString::number(url.port());

	QString strMethod="GET";
	if (!m_bGetRequest)
		strMethod="POST";


	m_ReqBuffer=ui.txtBody->toPlainText().toLatin1();

	GoWithDLL((int)ui.ckbSC->isChecked(),ui.txtUser->text(),ui.txtPass->text(),strHost,strMethod,url.path());
}


void Test_RestRPC::on_btnFind_clicked()
{
	ui.txtURL->setText(ui.txtURL->text()+"/contacts/search");

	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";
	strBody +="<nSearchType>";
	strBody +="0";
	strBody +="</nSearchType>";
	strBody +="<strField1_Name>";
	strBody +="BCNT_LASTNAME";
	strBody +="</strField1_Name>";
	strBody +="<strField1_Value>";
	strBody +="List";
	strBody +="</strField1_Value>";
	strBody +="<strField2_Name>";
	strBody +="</strField2_Name>";
	strBody +="<strField2_Value>";
	strBody +="</strField2_Value>";
	strBody +="<nGroupID xsi:nil=\"true\"/>";
	strBody +="<nFromN>-1</nFromN>";
	strBody +="<nToN>-1</nToN>";
	strBody +="<strFromLetter>a</strFromLetter>";
	strBody +="<strToLetter>z</strToLetter>";
	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";

	ui.txtBody->setText(strBody);
}

void Test_RestRPC::on_btn1_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST><PARAMETERS><nFilter>1</nFilter><nFromN>-1</nFromN><nToN>-1</nToN><strFromLetter>a</strFromLetter><strToLetter>c</strToLetter></PARAMETERS></REQUEST>";
	ui.txtBody->setText(strBody);

}
void Test_RestRPC::on_btn2_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST><PARAMETERS><nDocType>-1</nDocType><nFromN>-1</nFromN><nToN>-1</nToN></PARAMETERS></REQUEST>";
	ui.txtBody->setText(strBody);
}

void Test_RestRPC::on_btnDoc_request_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST><PARAMETERS><nDocType>1</nDocType><nFromN>-1</nFromN><nToN>-1</nToN></PARAMETERS></REQUEST>";
	ui.txtBody->setText(strBody);
}


void Test_RestRPC::on_btnDocFind_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST><PARAMETERS>";
	strBody +="<nDocType>1</nDocType>";
	strBody +="<nFromN>-1</nFromN>";
	strBody +="<nToN>-1</nToN>";
	strBody +="<nContactID>1</nContactID>";
	strBody +="<nProjectID>-1</nProjectID>";
	strBody +="<strSearchByName></strSearchByName>";
	strBody +="<datFromLastModified xsi:nil=\"true\"/>";
	strBody +="<datToLastModified xsi:nil=\"true\"/>";
	strBody +="<datFromCreated xsi:nil=\"true\"/>";
	strBody +="<datToCreated xsi:nil=\"true\"/>";
	strBody +="</PARAMETERS></REQUEST>";
	ui.txtBody->setText(strBody);
}
void Test_RestRPC::on_btnCheckOut_clicked()
{


}




// xml encodings (xml-encoded entities are preceded with '&')
static const char  AMP = '&';
static const QByteArray rawEntity[] = { "&",      "<",    ">",     "\'",     "\""};
static const QByteArray xmlEntity[] = { "&amp;", "&lt;", "&gt;",  "&apos;", "&quot;"};





/*!
Replace raw text with xml-encoded entities
\param xml	-external buffer with XMLRPC stream
\return binary stream (include copy op)
*/
QByteArray Test_RestRPC::xmlDecode(const QByteArray& xml)
{

	// check if no AMP there is no need to decode
	int pos=xml.indexOf(AMP);
	if(pos<0) return xml;

	QByteArray byteDecoded=xml;

	// iterate through list backwards: its very important to pars &amp last
	for (int iEntity=4; iEntity>=0; iEntity--)
		byteDecoded.replace(xmlEntity[iEntity],rawEntity[iEntity]);

	return byteDecoded;
}



/*!
Replace xml-encoded back to raw text

\param raw		-external buffer with XMLRPC stream
\return string (include copy op)
*/

QString Test_RestRPC::xmlEncode(const QString& raw)
{

	QString strEncoded=raw;

	// iterate through list forwad: its very important to encode &amp first
	if(strEncoded.indexOf("&")>=0)strEncoded.replace("&","&amp;");
	if(strEncoded.indexOf('<')>=0)strEncoded.replace("<","&lt;");
	if(strEncoded.indexOf(">")>=0)strEncoded.replace(">","&gt;");
	if(strEncoded.indexOf("\'")>=0)	strEncoded.replace("\'","&apos;");
	if(strEncoded.indexOf("\"")>=0)	strEncoded.replace("\"","&quot;");

	//	for (uint iEntity=2; iEntity<5; ++iEntity)
	//		strEncoded.replace(rawEntity[iEntity],xmlEntity[iEntity]);

	return strEncoded;
}



void Test_RestRPC::on_btnReadDirectory_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strAliasDirectory>Koko</strAliasDirectory>";
	strBody +="<nFromN>-1</nFromN>";
	strBody +="<nToN>-1</nToN>";
	strBody +="<nSortOrder>0</nSortOrder>";
	strBody +="<nPicWidth>75</nPicWidth>";
	strBody +="<nPicHeight>75</nPicHeight>";
	strBody +="<bKeepAspectRatio>1</bKeepAspectRatio>";
	strBody +="<bReturnThumbAsBinary>1</bReturnThumbAsBinary>";


	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";

	ui.txtBody->setText(strBody);
}


void Test_RestRPC::on_btnRotate_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strAliasDirectory>Koko</strAliasDirectory>";
	strBody +="<strFileName>Ante.JPG</strFileName>";
	strBody +="<nAngle>90</nAngle>";
	strBody +="<nPicWidth>75</nPicWidth>";
	strBody +="<nPicHeight>75</nPicHeight>";
	strBody +="<bKeepAspectRatio>1</bKeepAspectRatio>";
	strBody +="<bReturnThumbAsBinary>1</bReturnThumbAsBinary>";


	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";

	if (ui.txtURL->text().indexOf("/service/catalog/picture/rotate")<0)
		ui.txtURL->setText(ui.txtURL->text()+"/service/catalog/picture/rotate");

	ui.txtBody->setText(strBody);
}

void Test_RestRPC::on_btnGetPicture_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strAliasDirectory>Koko</strAliasDirectory>";
	strBody +="<strFileName>Koko.jpg</strFileName>";
	strBody +="<nHeading>0</nHeading>";
	strBody +="<nSortOrder>0</nSortOrder>";
	strBody +="<nPicWidth>75</nPicWidth>";
	strBody +="<nPicHeight>75</nPicHeight>";
	strBody +="<bKeepAspectRatio>1</bKeepAspectRatio>";
	strBody +="<bReturnThumbAsBinary>1</bReturnThumbAsBinary>";


	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";

	ui.txtBody->setText(strBody);
}

void Test_RestRPC::on_btnDeleteFile_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strAliasDirectory>Koko</strAliasDirectory>";
	strBody +="<strFileName>Koko.jpg</strFileName>";

	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";

	ui.txtBody->setText(strBody);
}

/*
void Test_RestRPC::on_ckbJames_clicked(bool checked)
{
	if (checked)
		m_nLoginType=0;
	else
		m_nLoginType=1;
}
*/

void Test_RestRPC::on_btnTarantulaAddr_clicked()
{
	//prepare request:
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<nProgramCode>0</nProgramCode>";			//FFH_LITE
	strBody +="<nVersionMajor>1</nVersionMajor>";
	strBody +="<nVersionMinor>0</nVersionMinor>";
	strBody +="<nVersionRevision>0</nVersionRevision>";
	strBody +="<strPlatformName>Linux</strPlatformName>";
	strBody +="<strPlatformUniqueID></strPlatformUniqueID>";
	strBody +="<strClientUniqueID></strClientUniqueID>";
	strBody +="<strHostUniqueID>43243214312</strHostUniqueID>";
	strBody +="<strHostIP></strHostIP>";
	strBody +="<nPublicPort></nPublicPort>";
	strBody +="<nStatsNatProtocol>0</nStatsNatProtocol>";

	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";

	ui.txtBody->setText(strBody);
	ui.txtURL->setText("http://192.168.200.10:11000/rest/service/register"); 
}

void Test_RestRPC::on_btnTarantula_clicked()
{
	QApplication::setOverrideCursor(Qt::WaitCursor);

	const int nTotalCalls = 1000;
	bool bAborted = false;
	QDateTime start = QDateTime::currentDateTime();
	for(int i=0; i<nTotalCalls; i++){
		on_btnPost_clicked();
		if(m_RespBuffer.isEmpty()){
			bAborted = true;
			break;	//error
		}
		//qDebug() << m_RespBuffer;
	}

	QApplication::restoreOverrideCursor();

	QDateTime end = QDateTime::currentDateTime();
	int nElapsedSec = start.secsTo(end);
	if(!bAborted)
		QMessageBox::information(NULL, "Results", QString("Done %1 calls in %2 sec (average %3 callse/sec)").arg(nTotalCalls).arg(nElapsedSec).arg(((double)nTotalCalls)/nElapsedSec));
}


void Test_RestRPC::on_btnCont_Emails_clicked()
{
	ui.txtURL->setText(ui.txtURL->text()+"/contact/1/emails");
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST><PARAMETERS><nFromN>0</nFromN><nToN>10</nToN></PARAMETERS></REQUEST>";
	ui.txtBody->setText(strBody);
}


void Test_RestRPC::on_btnInvite_clicked()
{
	QString strBody;
	strBody +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	
	strBody +="<nMessageType>1</nMessageType>";
	strBody +="<strAliasDirectory>Koko</strAliasDirectory>";
	strBody +="<strFirstName>Ante</strFirstName>";
	strBody +="<strLastName>Mise</strLastName>";
	strBody +="<strEmail>trumbic@sokrates.hr</strEmail>";
	strBody +="<strPhone></strPhone>";

	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";

	if (ui.txtURL->text().indexOf("/service/catalog/invite")<0)
		ui.txtURL->setText(ui.txtURL->text()+"/service/catalog/invite");

	ui.txtBody->setText(strBody);
}