#ifndef TEST_RESTRPC_H
#define TEST_RESTRPC_H

#include <QMainWindow>
//#include <QHttp>
//#include <QHttpResponseHeader>
#include <QProgressDialog>
#include <QLibrary>

#include "ui_test_restrpc.h"

class Test_RestRPC : public QMainWindow
{
	Q_OBJECT

public:
	Test_RestRPC(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~Test_RestRPC();

private slots:
	void on_btnGo_clicked();
	void on_btnPost_clicked();
	//void on_ckbJames_clicked(bool checked);
	
	void GoWithDLL(int nLoginType,QString strUser,QString strPass,QString strHost, QString strMethod,QString strPath);
	void MakeGetRequest(QString stAuthToken="");

	void on_btnFind_clicked();
	void on_btn1_clicked();
	void on_btn2_clicked();
	void on_btnDoc_request_clicked();
	void on_btnDocFind_clicked();
	void on_btnCheckOut_clicked();
	void on_btnReadDirectory_clicked();
	void on_btnRotate_clicked();
	void on_btnDeleteFile_clicked();
	void on_btnGetPicture_clicked();
	void on_btnTarantula_clicked();
	void on_btnTarantulaAddr_clicked();
	void on_btnCont_Emails_clicked();
	void on_btnInvite_clicked();
	

private:
	static QString xmlEncode(const QString& );
	static QByteArray xmlDecode(const QByteArray& );

	Ui::Test_RestRPCClass ui;

	QLibrary m_OrganizerDLL;
	//QHttp m_Http;
	QByteArray m_RespBuffer;
	QByteArray m_ReqBuffer;
	QProgressDialog m_Progres;
	//int nID;
	//QString strLastAuth;
	bool m_bGetRequest;
};

#endif // TEST_RESTRPC_H
