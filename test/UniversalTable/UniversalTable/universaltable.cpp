#include "universaltable.h"

UniversalTable::UniversalTable(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{
	ui.setupUi(this);
	connect(ui.listWidget,SIGNAL(itemSelectionChanged()),this,SLOT(SelChanged()));
}

UniversalTable::~UniversalTable()
{

}





void UniversalTable::on_AddRow_clicked()
{


	DbRecordSet *tmp=ui.grdUniversalTable->GetDataSource();

	tmp->addRow();
	tmp->setData(tmp->getRowCount()-1,0,QString("Jozo"));
	tmp->setData(tmp->getRowCount()-1,1,tmp->getRowCount()-1);

	ui.grdUniversalTable->RefreshDisplay();

}

void UniversalTable::on_EditMode_clicked()
{
	
	ui.grdUniversalTable->SetEditMode(!ui.grdUniversalTable->IsEditMode());

}

void UniversalTable::on_Delete_clicked()
{

	ui.grdUniversalTable->DeleteSelection();
}

void UniversalTable::on_Delete_2_clicked()
{

	ui.grdUniversalTable->DeleteTable();

}



void UniversalTable::SelChanged()
{
	int n=0;
	for(int i=0;i<4;++i)
	{
		if(ui.listWidget->isItemSelected(ui.listWidget->item(i)))
			n++;
	}
	qDebug()<<n;
}