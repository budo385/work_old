#include "gui_core/gui_core/universaltablewidget.h"
#include "bus_core/bus_core/entity_id_collection.h"



class Table1 : public UniversalTableWidget
{
	Q_OBJECT

public:
	Table1(QWidget * parent = 0):UniversalTableWidget(parent)
	{
		AddAcceptableDropTypes(MULTICOLUMNSORT); //can drop itself
	};

	int GetDropType(){return MULTICOLUMNSORT;}


};