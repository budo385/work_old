#ifndef LOCALTEST_H
#define LOCALTEST_H

//#include <QObject>
#include <QLocale>
#include <QDoubleSpinBox>
#include "math.h"

class LocalTest : public QLocale 
{
	//Q_OBJECT
public:
	LocalTest():QLocale(QLocale::system()){};
	virtual QChar decimalPoint()const {return QChar('.');};
   
};



class MySpin : public QDoubleSpinBox
{

public:
		
		MySpin(QWidget * parent = 0):QDoubleSpinBox(parent){};

		QString textFromValue(double v) const
		{
			return QVariant(v).toString();
		};


		double valueFromText(const QString &text) const
		{
			return QVariant(text).toDouble();
		}

		
		QValidator::State validate(QString &text, int &pos) const
		{
			bool ok;
			double x = QVariant(text).toDouble(&ok);
			if(ok)
				return QValidator::Acceptable;
			else
				return QValidator::Invalid;
		}
};



#endif // LOCALTEST_H
