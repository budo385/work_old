#ifndef UNIVERSALTABLE_H
#define UNIVERSALTABLE_H

#include <QtGui/QMainWindow>
#include "ui_universaltable.h"

class UniversalTable : public QMainWindow
{
    Q_OBJECT

public:
    UniversalTable(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~UniversalTable();

//private:
    Ui::UniversalTableClass ui;


private slots:
	void on_Delete_2_clicked();
	void on_Delete_clicked();
	void on_EditMode_clicked();
	void on_AddRow_clicked();

	void SelChanged();
};

#endif // UNIVERSALTABLE_H
