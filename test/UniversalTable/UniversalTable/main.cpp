#include <QtGui/QApplication>
#include "universaltable.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db_core/db_core/dbrecordset.h"
#include <QLocale>
#include <QPixmap>

#include "gui_core/gui_core/multisort.h"

//#include "gui_core/gui_core/tablewidget.h"


bool caseInsensitiveLessThan(const QString &s1, const QString &s2);

int main(int argc, char *argv[])
{
    
    
	QApplication a(argc, argv);

	UniversalTable w;

	MultiSortColumn windowSort;





	//table data:
	//--------------------------------------------------------
	DbRecordSet data, data_2;
	data.addColumn(QVariant::String,"Name");
	data.addColumn(QVariant::Bool,"Code");
	data.addColumn(DbRecordSet::GetVariantType(),"COMBO");
	data.addColumn(DbRecordSet::GetVariantType(),"LIST");
	data.addColumn(QVariant::String,"TEXT");
	data.addColumn(QVariant::ByteArray,"PIC");

	

	DbRecordSet list;

	list.addColumn(QVariant::String,"XXX");

	for (int i=0;i<4;++i)
	{
		list.addRow();
		list.setData(i,0,QVariant(i).toString()+QString("- Matezis"));

	}

	QPixmap pic;

	bool bOK=pic.load("D:/1.jpg");
    QByteArray bytes;
    QBuffer buffer(&bytes);
    buffer.open(QIODevice::WriteOnly);
    pic.save(&buffer, "JPG"); // writes pixmap into bytes in PNG format


	for (int i=0;i<5;++i)
	{
		data.addRow();
		data.setData(i,0,QVariant(i).toString()+QString("Mate ooooooooo rrrrr"));
		data.setData(i,1,true);
		data.setData(i,2,list);
		data.setData(i,3,list);
		data.setData(i,4,QString("Ante gggggggggg dgdg"));
		data.setData(i,5,bytes);

	}

	QTime tmp;
	

	tmp.start();
	DbRecordSet columns;
	w.ui.grdUniversalTable->AddColumnToSetup(columns,"Name","Header_Name");
	w.ui.grdUniversalTable->AddColumnToSetup(columns,"Code","Header_Code",120,true,"",UniversalTableWidget::COL_TYPE_CHECKBOX);
	w.ui.grdUniversalTable->AddColumnToSetup(columns,"COMBO","Header_Combo",120,true,"",UniversalTableWidget::COL_TYPE_COMBOBOX,QString("'No: '<<XXX"));
	w.ui.grdUniversalTable->AddColumnToSetup(columns,"LIST","Header_List",120,true,"",UniversalTableWidget::COL_TYPE_LIST,QString("'No: '<<XXX"));
	w.ui.grdUniversalTable->AddColumnToSetup(columns,"TEXT","Header_Text",120,true,"",UniversalTableWidget::COL_TYPE_MULTILINE_TEXT);
	w.ui.grdUniversalTable->AddColumnToSetup(columns,"PIC","Header_Pic",120,true,"",UniversalTableWidget::COL_TYPE_PICTURE);


	w.ui.grdUniversalTable->Initialize(&data,true,true,false,true,100);
	w.ui.grdUniversalTable->SetColumnSetup(columns);
	//tmp.start();
	//w.ui.grdUniversalTable->RefreshDisplay();
	qDebug()<<"time:"<<tmp.elapsed();



	//windowSort.SetColumnSetup(columns,&data);
	//windowSort.exec();

	//2ns grid:
	data_2.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_LOCKING));
	w.ui.grdUniversalTable_2->Initialize(&data_2);
	w.ui.grdUniversalTable_2->DefineColumnSetupFromView(columns,DbSqlTableView::TVIEW_CORE_LOCKING);
	w.ui.grdUniversalTable_2->SetColumnSetup(columns);


	
	/*
	w.ui.grdUniversalTable_2->setDragEnabled(true);
	w.ui.grdUniversalTable_2->setDropIndicatorShown(true);
	w.ui.grdUniversalTable_2->setAcceptDrops(true);
	//w.ui.grdUniversalTable_2->setColumnCount(1);

	for(int i=0;i<10;++i)
	{
		w.ui.grdUniversalTable_2->insertRow(i);
		QTableWidgetItem *newItem = new QTableWidgetItem("TableData");
		w.ui.grdUniversalTable_2->setItem(i, 0, newItem);
	}

*/



	w.ui.listWidget->setDragEnabled(true);
	w.ui.listWidget->setDropIndicatorShown(true);
	w.ui.listWidget->setAcceptDrops(true);
	w.ui.listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

	for(int i=0;i<4;++i)
	{
		w.ui.listWidget->insertItem(i,QString("Ante %1").arg(i));
	}




	
	w.ui.tableWidget->setDragEnabled(true);
	w.ui.tableWidget->setDropIndicatorShown(true);
	w.ui.tableWidget->setAcceptDrops(true);
	//w.ui.tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	w.ui.tableWidget->setColumnCount(1);

	for(int i=0;i<10;++i)
	{
		w.ui.tableWidget->insertRow(i);
		QTableWidgetItem *newItem = new QTableWidgetItem("TableData");
		w.ui.tableWidget->setItem(i, 0, newItem);
	}




	w.ui.treeWidget->setDragEnabled(true);
	w.ui.treeWidget->setDropIndicatorShown(true);
	w.ui.treeWidget->setAcceptDrops(true);

    w.ui.treeWidget->setColumnCount(1);
	QTreeWidget * parent=NULL;
    QList<QTreeWidgetItem *> items;
    for (int i = 0; i < 10; ++i)
        items.append(new QTreeWidgetItem(parent, QStringList(QString("item: %1").arg(i))));
    w.ui.treeWidget->insertTopLevelItems(0, items);




    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));


    return a.exec();
}



 bool caseInsensitiveLessThan(const QString &s1, const QString &s2)
    {
		bool OK=s1.toLower() < s2.toLower();
        return OK;
    }



