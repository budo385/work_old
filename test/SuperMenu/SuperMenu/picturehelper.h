#ifndef PICTUREHELPER_H
#define PICTUREHELPER_H

#include <QDebug>
#include <QMessageBox>
#include <QPixmap>
#include <QBuffer>
#include <QFileDialog>
#include <QClipboard>
#include <QImageReader>
#include <QImageWriter>
#include <QApplication>

//#include "db_core/db_core/dbrecordset.h"
//#include "common/common/status.h"
//#include "gui_core/gui_core/guidatamanipulator.h"




/*!
	\class PictureHelper
	\brief Use as static, provides method to load picture from file or from clipboard
	\ingroup GUICore_CustomWidgets

*/
class PictureHelper 
{


public:
	
	static void ConvertPixMapToByteArray(QByteArray &bufPicture, QPixmap &bufPixMap, QString strFormat="PNG");
	static void ResizePixMapToPreview(QPixmap &bufPixMap);

	static bool LoadPicture(QPixmap &bufPixMap,QString &strFormat);
	static bool SavePicture(QPixmap &bufPixMap);
	static bool PastePictureFromClipboard(QPixmap &bufPixMap);
	static bool CopyPictureToClipboard(QPixmap &bufPixMap);

/*
	static bool LoadPictureIfNeeded(DbRecordSet *pData, int nRow,QString strFieldPictureId,QString strFieldId);
	static void SavePictureIfNeeded(Status &Ret_pStatus,GuiDataManipulator *pDataMan,DbRecordSet &lstPictureWrite,QString strFieldPictureId, bool bIsPictureMandatory=true);
	*/

};

#endif // PICTUREHELPER_H


