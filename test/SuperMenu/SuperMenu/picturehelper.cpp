#include "picturehelper.h"
//#include "bus_client/bus_client/clientmanager.h"
//extern ClientManager *g_BoSet;						//global access to Bo services


static QString strStartDir=QApplication::applicationDirPath();


/*!
	Prompts user to pick picture from file

	\param bufPixMap	- return picture as pixmap
	\param strFormat	- format of picture
	\return				- false if user canceled or error occurred

*/
bool PictureHelper::LoadPicture(QPixmap &bufPixMap,QString &strFormat)
{

	//get list of supported images:
	QList<QByteArray> lstFormats = QImageReader::supportedImageFormats();
	QString strFilter ="Images (";
	for(int i=0;i<lstFormats.size();++i)
	{
		//if(i!=0)strFilter+=" ";
		strFilter+=" *."+lstFormats.at(i);
	}
	strFilter+=")";

	//if start dir empty then application path:
	if(strStartDir.isEmpty())strStartDir=QDir::currentPath(); 

	//open file dialog:
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		QT_TR_NOOP("Choose picture"),
		strStartDir,
		strFilter);


	//load picture into ByteArray
	if(!strFile.isEmpty())
	{
		QFileInfo picFile(strFile);
		strFormat=picFile.completeSuffix();

		bool bOK=bufPixMap.load(strFile);
		if(!bOK)
		{
			QMessageBox::critical(NULL,QT_TR_NOOP("Error"),QT_TR_NOOP("Picture can not be loaded!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return false;
		}

		strStartDir=picFile.absoluteDir().absolutePath(); //store back choosen dir
		return true;
	}
	else
		return false;

}





bool PictureHelper::SavePicture(QPixmap &bufPixMap)
{

	QByteArray bufPicture;

	//get list of supported images:
	QList<QByteArray> lstFormats = QImageWriter::supportedImageFormats();
	QString strFilter ="Images (";
	for(int i=0;i<lstFormats.size();++i)
	{
		//if(i!=0)strFilter+=" ";
		strFilter+=" *."+lstFormats.at(i);
	}
	strFilter+=")";

	//open file dialog:
	QString strFile = QFileDialog::getSaveFileName(
		NULL,
		QT_TR_NOOP("Save Picture As"),
		strStartDir,
		strFilter);


	//load picture into ByteArray
	if(!strFile.isEmpty())
	{

		QFileInfo picFile(strFile);
		QString strFormat=picFile.completeSuffix();

		QImageWriter pic_writer(strFile,strFormat.toAscii());
		bool bOK=pic_writer.write(bufPixMap.toImage());
		if(!bOK)
		{
			QMessageBox::critical(NULL,QT_TR_NOOP("Error"),QT_TR_NOOP("Error while saving picture!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return false;
		}

		qDebug()<<pic_writer.format();

		return true;
	}
	else
		return false;


}




/*!
	Check content of clipboards, tries to save to given variables

	\param bufPixMap	- return picture as pixmap
	\return				- false if user canceled or error occurred
*/
bool PictureHelper::PastePictureFromClipboard(QPixmap &bufPixMap)
{

	QClipboard *clipboard = QApplication::clipboard();
	bufPixMap = clipboard->pixmap(QClipboard::Clipboard);

	if (bufPixMap.isNull()) return false; //no picture in clipboard
	return true;
}



bool PictureHelper::CopyPictureToClipboard(QPixmap &bufPixMap)
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setPixmap(bufPixMap,QClipboard::Clipboard);
	return true;
}



//WARNING: all pics are converted into PNG format with this function......???
void PictureHelper::ConvertPixMapToByteArray(QByteArray &bufPicture, QPixmap &bufPixMap, QString strFormat)
{
	QBuffer buffer(&bufPicture);
	buffer.open(QIODevice::WriteOnly);		
	bufPixMap.save(&buffer, strFormat.toAscii());			// writes pic into with default PNG format (who knows original format?)
	return;

}



//scales picture to 80pix fixed by width, preserving aspect ratio
void PictureHelper::ResizePixMapToPreview(QPixmap &bufPixMap)
{
	bufPixMap=bufPixMap.scaledToWidth(120,Qt::SmoothTransformation);
}






//This is conv. method for reloading big picture if needed
//pData must have BPIC_PICTURE col

/*
bool PictureHelper::LoadPictureIfNeeded(DbRecordSet *pData, int nRow,QString strFieldPictureId,QString strFieldId)
{
	Status err;
	DbRecordSet rowPicture;
	if(nRow>pData->getRowCount()-1) return true; //if row is out of boundaries, exit with true

	//reload data from server if needed:
	bool bBigPictureLoaded=!pData->getDataRef(nRow,"BPIC_PICTURE").isNull();
	int nRecordID=pData->getDataRef(nRow,strFieldId).toInt();
	if(!bBigPictureLoaded && nRecordID!=0) //if flag is false and ID is valid, load from server
	{
		int nBigPicId=pData->getDataRef(nRow,strFieldPictureId).toInt();
		if(nBigPicId==0) return true; //pic not exists -> not loaded
		g_BoSet->app->BusContact->ReadBigPicture(err,nBigPicId,rowPicture);
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,(QString)QT_TR_NOOP("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return false; 
		}
		pData->setData(nRow,"BPIC_PICTURE",rowPicture.getDataRef(0,"BPIC_PICTURE")); //save picture
	}
	return true; 
}
*/




//Put in BoEntity before save: saves all pictures 
//pData must have BPIC_PICTURE col
//When deleting, just set PIC_ID=0, trigger will do da job, this will only update existing or add new ones...
//Probably special routine for cleaning DB is needed in future (unlinked PICS)
/*
void PictureHelper::SavePictureIfNeeded(Status &Ret_pStatus,GuiDataManipulator *pDataMan,DbRecordSet &lstContactPictures,QString strFieldPictureId, bool bIsPictureMandatory)
{

	
	//get data for write:
	DbRecordSet lstPictureWrite,lstPictureDelete;

	//writes picture data from picture entity (only images)
	lstPictureWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
	lstPictureDelete.copyDefinition(lstPictureWrite);
	lstContactPictures.copyDefinition(*(pDataMan->GetDataSource()));
	pDataMan->GetWriteData(lstContactPictures);

	lstContactPictures.clearSelection();

	//copy pic data for save (edit or insert)
	//delete is automatic on DB (trigger)
	int nSize=lstContactPictures.getRowCount();
	for (int i =0;i<nSize;i++)
	{
		if(lstContactPictures.getDataRef(i,"BPIC_PICTURE").toByteArray().size()==0) //check if picture exists
		{
			if(bIsPictureMandatory)
			{
				Ret_pStatus.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QT_TR_NOOP("Picture is mandatory field!"));	
				return;	
			}
			else
			{
				//if pic is null & ID is valid, set it to zero, trigger on BUS_PERSON must automatically delete pics
				QVariant nullVar(QVariant::Int);
				lstContactPictures.setData(i,strFieldPictureId,nullVar);
				continue;
			}
		}
			
		lstPictureWrite.addRow();
		lstPictureWrite.setData(lstPictureWrite.getRowCount()-1,"BPIC_ID",lstContactPictures.getDataRef(i,strFieldPictureId));
		lstPictureWrite.setData(lstPictureWrite.getRowCount()-1,"BPIC_PICTURE",lstContactPictures.getDataRef(i,"BPIC_PICTURE"));
		lstContactPictures.selectRow(i); //tagg row for write
		
	}



	//save and returns ID's back to list
	if(lstPictureWrite.getRowCount()>0)
	{
		//save pictures first, hmm little cranky...
		g_BoSet->app->BusContact->WriteBigPicture(Ret_pStatus,lstPictureWrite);
		if(!Ret_pStatus.IsOK())	return;

		//copy back new ID's:
		int j=0;
		for (int i =0;i<nSize;i++)
		{
			if(lstContactPictures.isRowSelected(i))
			{
				lstContactPictures.setData(i,strFieldPictureId,lstPictureWrite.getDataRef(j,0));
				j++;
			}
		}
	}


	//set data back:
	lstContactPictures.removeColumn(lstContactPictures.getColumnIdx("BPIC_PICTURE"));

	

}
*/