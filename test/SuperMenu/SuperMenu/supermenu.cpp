#include "supermenu.h"
#include "picturehelper.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QHeaderView>
#include <QDockWidget>



void MyLabel::mousePressEvent ( QMouseEvent * event )
{
	emit Clicked();
	QLabel::mousePressEvent(event);
}



SuperMenu::SuperMenu(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{

	ui.setupUi(this);

	QSize size(300,71);

	QString style="\
	QPushButton {\
	border-style: outset;\
	border-width: 2px;\
	border-radius: 10px;\
	border-color: beige;\
	color: white;\
	font-family: \"Arial\";\
	font: bold 16px; \
	min-width: 10em;\
	padding: 6px;\
	background-image:url(:Menu_Administrator.png); }\
	QPushButton:hover { background-image:url(:Menu_Administrator_Pressed.png) }";
	

	ui.pushButton_2->setStyleSheet(style);


	QPushButton *btn= ui.pushButton;

	MyLabel *label1 = new MyLabel(btn);
	//label1->setText("Ante");
	
	label1->setText("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">	p, li { white-space: pre-wrap; }\
	</style></head><body style=\" font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\">\
	<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:600; font-style:italic; color:#ffff00;\">Ovo je Super Button</span></p>\
	<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\
	<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:11pt; color:#ffffff;\">A ovo su mali zeleni,buttoni</span></p>\
	<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p></body></html>");
	

	//label1->setStyleSheet("QLabel:hover color:red")

	QHBoxLayout *layout = new QHBoxLayout();
	layout->setMargin(0);
	layout->setSpacing(3);
	layout->addSpacing(70);
	layout->addWidget(label1);
	layout->addStretch(5);


	//btn->setMinimumSize(size);
	//btn->setStyleSheet(style);
	btn->setLayout(layout);

	connect(label1, SIGNAL(Clicked()),this,SLOT(on_pushButton_clicked()));




	//new composed button:
	//QHBoxLayout *layoutBtn = new QHBoxLayout();
	//layout->setMargin(0);
	//layout->setSpacing(0);

	//QString style2="QPushButton {border-left:length 30; border-color: white; border-image:url(:Mn_SlikaLeft.png); background-image:url(:Mn_Tile.png); }\
	//			  QPushButton:hover { background-image:url(:Mn_Tile_Pressed.png);border-image:url(:Mn_SlikaLeft_Pressed.png) }";

	//QString style2="QPushButton { border-image: url(:Border.png) 27 27 27 27 repeat repeat; background-image:url(:Mn_SlikaLeft.png);background-position:left;background-repeat:no-repeat}\
	//			   QPushButton:hover { border-image:url(:Mn_Tile_Pressed.png) }";


	QString style2="QPushButton { border-left-width: 70px; border-image: url(:Mn_SlikaLeft.png) 0 0 0 100 stretch stretch; background-image:url(:Mn_Tile.png);}\
				   QPushButton:hover { border-left-width: 70px; border-image: url(:Mn_SlikaLeft_Pressed.png) 0 0 0 100 stretch stretch; background-image:url(:Mn_Tile_Pressed.png);} ";


	QPushButton *btnLeft=new QPushButton(this);
	btnLeft->setStyleSheet(style2);
	btnLeft->setMinimumSize(size);


	QPushButton *btn2=new QPushButton(this);
	btn2->setStyleSheet(style2);
	btn2->setMinimumSize(size);


	//QSize size(300,71);
	btn->setMinimumSize(size);
	//btn->setMaximumSize(size);
	btn->setStyleSheet(style2);

	//layout->addSpacing(70);
	//layout->addWidget(label1);
	//layout->addStretch(5);






	//ui.label->setPix
	

	ui.treeWidget->setVisible(false);
	ui.treeView->setVisible(false);



	//---------------------------------
	//			TREE 1
	//---------------------------------
	//Add some menu items:
	ui.treeWidget->header()->hide();
	ui.treeWidget->setAnimated(true);
	ui.treeWidget->setUniformRowHeights(false);
	//ui.treeWidget->setRootIsDecorated(false);

	QTreeWidgetItem *Item= new QTreeWidgetItem;
	Item->setData(0, Qt::DisplayRole,"Projects");
	Item->setIcon(0,QIcon(":Mn_SlikaLeft.png"));
	ui.treeWidget->addTopLevelItem(Item);
	
	QTreeWidgetItem *ItemP= new QTreeWidgetItem;
	ItemP->setData(0, Qt::DisplayRole,"Charges");
	ItemP->setIcon(0,QIcon(":clock.png"));

	QTreeWidgetItem *ChildItem= new QTreeWidgetItem;
	//ChildItem->setData(0, Qt::DisplayRole,"Expenses");
	//ChildItem->setIcon(0,QIcon(":cookies.png"));
	ItemP->addChild(ChildItem);
	ui.treeWidget->addTopLevelItem(ItemP);

	ui.treeWidget->setItemWidget(ChildItem, 0, btnLeft );
	//ChildItem->setExpanded(true);


/*
	Item= new QTreeWidgetItem;
	Item->setData(0, Qt::DisplayRole,"Projects");
	Item->setIcon(0,QIcon(":handwithbook.png"));
	ui.treeWidget->addTopLevelItem(Item);
*/
	QTreeWidgetItem *ChildItem2= new QTreeWidgetItem;
	/*
	ChildItem2->setData(0, Qt::DisplayRole,"Books");
	ChildItem2->setIcon(0,QIcon(":books.png"));
	Item->addChild(ChildItem2);
	Item->setExpanded(true);

	ItemP->setExpanded(true);
	ItemP->setExpanded(false);
	//ItemP->setExpanded(true);
*/

	QString style3="QTreeView {background-image:url(:Mn_SlikaLeft.png);background-color: yellow;background-origin: ccontent} QTreeView:hover { border-width: 20px; border-color:black} ";

	ui.treeView->setAutoFillBackground(false);
	//this->setWindowOpacity(0.8);
	//ui.treeWidget->setStyleSheet(style3);
	//ui.treeView->setStyleSheet(style3);


	//---------------------------------
	//			TREE 2
	//---------------------------------



	//Add some menu items:
	ui.treeView->header()->hide();
	//ui.treeView->setAnimated(true);
	ui.treeView->setUniformRowHeights(false);
	ui.treeView->setRootIsDecorated(false);

	Item= new QTreeWidgetItem;
	Item->setData(0, Qt::DisplayRole,"Projects");
	Item->setIcon(0,QIcon(":Mn_SlikaLeft.png"));
	ui.treeView->addTopLevelItem(Item);

	ItemP= new QTreeWidgetItem;
	ItemP->setData(0, Qt::DisplayRole,"Charges");
	ItemP->setIcon(0,QIcon(":oldman.jpg"));

	ChildItem= new QTreeWidgetItem;
	//ChildItem->setData(0, Qt::DisplayRole,"Expenses");
	//ChildItem->setIcon(0,QIcon(":cookies.png"));
	ItemP->addChild(ChildItem);
	ui.treeView->addTopLevelItem(ItemP);

	ui.treeView->setItemWidget(ChildItem, 0, btn2 );
	//ChildItem->setExpanded(true);



	Item= new QTreeWidgetItem;
	Item->setData(0, Qt::DisplayRole,"Projects");
	Item->setIcon(0,QIcon(":splash.png"));
	ui.treeView->addTopLevelItem(Item);

	QSize sizeIcon(100,100);
	ui.treeView->setIconSize(sizeIcon);

	ChildItem2= new QTreeWidgetItem;
	ChildItem2->setData(0, Qt::DisplayRole,"Books");
	ChildItem2->setIcon(0,QIcon(":books.png"));
	Item->addChild(ChildItem2);
	//Item->setExpanded(true);


	//ItemP->setExpanded(true);
	//ItemP->setExpanded(false);
	//ItemP->setExpanded(true);


	QString style4="QTreeView {selection-color: red;selection-background-color: green; background-image:url(:splash.png);background-color: grey;color: white;background-origin: padding}";
//QTreeView:hover { border-width: 20px; border-color:black} ";

	//ui.treeView->setStyleSheet(style4);
	//ui.tableWidget->setStyleSheet(style4);
	

	//background:

	QPixmap pix;
	pix.load(":Style_Paper-Button.png");

	QPalette pal = ui.treeWidget->palette();
	pal.setBrush(QPalette::Active, QPalette::Base, QBrush(pix));

	ui.treeWidget->setPalette(pal);
	ui.treeView->setPalette(pal);



}

SuperMenu::~SuperMenu()
{

}


void SuperMenu::on_pushButton_clicked()
{
	ui.treeWidget->setVisible(!ui.treeWidget->isVisible());

	//QMessageBox::information(this,"Warning","OK");
}

void SuperMenu::on_pushButton_2_clicked()
{
	ui.treeView->setVisible(!ui.treeView->isVisible());

	//QMessageBox::information(this,"Warning","OK");
}


//void SuperMenu::SetupSuperButton();