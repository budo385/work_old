#include <QtGui/QApplication>
#include "supermenu.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SuperMenu w;
    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
    return a.exec();
}
