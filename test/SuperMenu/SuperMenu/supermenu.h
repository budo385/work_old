#ifndef SUPERMENU_H
#define SUPERMENU_H

#include <QtGui/QMainWindow>
#include "ui_supermenu.h"
#include <QLabel>

class MyLabel: public QLabel
{
	Q_OBJECT
public:
	MyLabel(QWidget *parent):QLabel(parent){};

protected:
	void mousePressEvent ( QMouseEvent * event );

signals:
	void Clicked();

};


class SuperMenu : public QMainWindow
{
    Q_OBJECT



public:
    SuperMenu(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~SuperMenu();

	void SetupSuperButton();

private:
    Ui::SuperMenuClass ui;

private slots:
	void on_pushButton_clicked();
	void on_pushButton_2_clicked();
};

#endif // SUPERMENU_H
