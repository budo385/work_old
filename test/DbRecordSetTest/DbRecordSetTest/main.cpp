#include <QtCore/QCoreApplication>
#include <db_core/db_core/dbrecordset.h>
#include <db_core/db_core/dbrecordset_adv.h>
#include <time.h>





void SortTest();
void RowInRowTest();
void MergeTest1();
void MergeTest2();

void AssignRowTest();
void printList(DbRecordSet& set);


//static unsigned int nj = qRegisterMetaType<DbRec>("DbRec");

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

	QTime h1;

	/*
	DbRecordSetAdv rec;

	rec.addColumn(QVariant::Int, "prvi");
	rec.addColumn(QVariant::String, "drugi");
	rec.addColumn(QVariant::String, "x1");
	rec.addColumn(QVariant::String, "x2");
	rec.addColumn(QVariant::ByteArray, "treci");
	rec.addColumn(256, "cetvrti");
	

	DbRecordSetAdv small;
	small=rec;
	small.addRow();


	h1.start();
	for(int i=0;i<100000;++i)
	{
		rec.addRow();
	}
	qDebug()<<h1.elapsed();
	h1.start();
	for(int i=0;i<100000;++i)
	{
		//rec.setData(i,0,1);
		rec.setData(i,1,(QString)"Kukuk");
		rec.setData(i,5,small);
		DbRecordSetAdv &x=rec.getDataRefList(1,5);
	}
	qDebug()<<h1.elapsed();
*/

	DbRecordSet rec1;
	
	DbRecordSet small1;
	small1=rec1;
	small1.addRow();

	rec1.addColumn(QVariant::Int, "prvi");
	rec1.addColumn(QVariant::String, "drugi");
	rec1.addColumn(QVariant::String, "x1");
	rec1.addColumn(QVariant::String, "x2");
	rec1.addColumn(QVariant::ByteArray, "treci");
	rec1.addColumn(256, "cetvrti");


	h1.start();
	for(int i=0;i<100;++i)
	{
		rec1.addRow();
	}
	qDebug()<<h1.elapsed();
	h1.start();
	for(int i=0;i<100;++i)
	{
		//rec1.setData(i,0,1);
		//rec1.setData(i,1,(QString)"Kukuk");
		rec1.setData(i,1,"Kukuk");

		//rec1.setData(i,5,small1);
		//DbRecordSet &x=rec1.getDataRef(i,5).value<DbRecordSet>();

	}


	QString xx("Kukuk");
	rec1.find(1,xx);
	qDebug()<<rec1.getSelectedCount();

	xx="Ante";

	rec1.find(1,xx,false,false,true,false);
	qDebug()<<rec1.getSelectedCount();


	qDebug()<<h1.elapsed();




	//SortTest();
	//RowInRowTest();
	//MergeTest1();
	//MergeTest2();
	//AssignRowTest();

	printf("\nDone! Press Ctrl+C to exit.\n");

    return a.exec();
}

void SortTest()
{
	DbRecordSet set;

	set.addColumn(QVariant::Int, "prvi", "drugi", "tabla1");

	set.addRow();
	set.setData(0, 0, 123);

	//test 1: must read the same what was written into
	int nRead = 0;
	set.getData(0, 0, nRead);
	printf("Data set=123, data read=%d\n", nRead);

	srand(time(NULL));

	for(int k=0; k<34; k++)
	{
		set.addRow();
		set.setData(k, 0, rand());	//add random number

	}

	//test 2: sort
	set.sort(0);

	printf("Sorted data:\n");
	for(int i=0; i<set.getRowCount(); i++)
	{
		set.getData(i, 0, nRead);
		printf("set[%d]=%d\n", i, nRead);
	}

}

void RowInRowTest()
{
	//define and fill the first row
	DbRecordSet set;
	set.addColumn(QVariant::Int, "prvi", "drugi", "tabla1");
	set.addRow();
	set.setData(0, 0, 123);

	//define main row that will contain the first row
	DbRecordSet set1;
	set1.addColumn(DbRecordSet::GetVariantType(), "set", "set", "tabla1");
	set1.addRow();
	set1.setData(0, 0, set);

	//now try to read
	DbRecordSet set2;
	set1.getData(0,0, set2);

	int nValue = 0;
	set2.getData(0,0, nValue);

	Q_ASSERT(123 == nValue);
}

void MergeTest1()
{
	printf("\nMerge test 1\n\n");

	//define and fill the first row
	DbRecordSet set;
	set.addColumn(QVariant::Int, "Broj", "prvi", "tabla1");
	set.addColumn(QVariant::String, "Tekst", "prvi", "tabla1");
	
	set.addRow();
	set.setData(0, 0, 123);
	set.setData(0, 1, QString("Tekst"));

	//define slightly different row
	DbRecordSet set1;
	set1.addColumn(QVariant::Int, "Broj", "drugi", "tabla1");
	set1.addColumn(QVariant::String, "TekstDva", "prvi", "tabla1");

	set1.addRow();
	set1.setData(0, 0, 321);
	set1.setData(0, 1, QString("Novi tekst"));

	set.merge(set1);

	//print the new contents of the list
	int nCnt = set.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		int nValue;
		QString strValue;
		set.getData(i, 0, nValue);
		set.getData(i, 1, strValue);

		printf("Row[%d]: %d, %s\n", i, nValue, strValue.toLatin1().constData());
	}
}

//version with copydefinition
void MergeTest2()
{
	printf("\nMerge test 2\n\n");

	//define and fill the first row
	DbRecordSet set;
	set.addColumn(QVariant::Int, "Broj", "prvi", "tabla1");
	set.addColumn(QVariant::String, "Tekst", "prvi", "tabla1");
	
	set.addRow();
	set.setData(0, 0, 123);
	set.setData(0, 1, QString("Tekst"));

	//define slightly different row
	DbRecordSet set1;
	set1.copyDefinition(set);

	set1.addRow();
	set1.setData(0, 0, 321);
	set1.setData(0, 1, QString("Novi tekst"));

	set.merge(set1);

	//print the new contents of the list
	int nCnt = set.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		int nValue;
		QString strValue;
		set.getData(i, 0, nValue);
		set.getData(i, 1, strValue);

		printf("Row[%d]: %d, %s\n", i, nValue, strValue.toLatin1().constData());
	}
}


//version with copydefinition
void AssignRowTest()
{
	printf("\nAssignRow test 2\n\n");

	//define and fill the first row
	DbRecordSet set;
	set.addColumn(QVariant::Int, "Broj", "prvi", "tabla1");
	set.addColumn(QVariant::String, "Tekst", "prvi", "tabla1");
	
	set.addRow();
	set.setData(0, 0, 123);
	set.setData(0, 1, QString("Tekst"));
	set.addRow();
	set.setData(1, 0, 2);
	set.setData(1, 1, QString("xxTekst"));
	set.addRow();
	set.setData(2, 0, 3);
	set.setData(2, 1, QString("yyTekst"));

	//select 1st row
	set.selectRow(1);

	//define slightly different row
	DbRecordSet set1;
	set1.copyDefinition(set);

	set1.addRow();
	set1.setData(0, 0, 44444);
	set1.setData(0, 1, QString("Novi tekst"));

	printList(set);

	set.assignRow(set1);
	set.assignRow(2,set1,true);

	printList(set);
}





void printList(DbRecordSet& set)
{
	//print the new contents of the list
	int nCnt = set.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		int nValue;
		QString strValue;
		set.getData(i, 0, nValue);
		set.getData(i, 1, strValue);

		printf("Row[%d]: %d, %s\n", i, nValue, strValue.toLatin1().constData());
	}
}