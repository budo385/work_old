#include <QtCore/QCoreApplication>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>
#include <QtCore>


#include "db/db/db_include.h"
#include "common/common/status.h"
#include "db/db/dbsqlmanager.h"




int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	//load all drivers
	Status err;


/*
	QSqlDatabase db = QSqlDatabase::addDatabase(DBTYPE_ORACLE);
	db.setHostName("oraserver");
	db.setDatabaseName("oraserver");
	db.setUserName("sok_test");
	db.setPassword("sok_test");
*/

	DbConnectionSettings ConnSettings;
/*
	//Make MYSQL connection.
	ConnSettings.m_strDbType=DBTYPE_MYSQL;
	ConnSettings.m_strDbHostName="localhost";
	ConnSettings.m_strDbUserName="root";
	ConnSettings.m_strDbPassword="helix1";
	ConnSettings.m_strDbName="test";
*/
	ConnSettings.m_strDbType=DBTYPE_FIREBIRD;
	ConnSettings.m_strDbHostName="";
	ConnSettings.m_strDbUserName="SYSDBA";
	ConnSettings.m_strDbPassword="masterkey";
	ConnSettings.m_strDbName="C:\\xx.fdb";
	ConnSettings.m_nDbPort=0;

	//create SOKRATES_XP database:
	DbSqlManager m_pDbManager;

	m_pDbManager.Initialize(err, ConnSettings,10);
	if (!err.IsOK())
	{
		qDebug()<<err.getErrorText();
		return 0;
	}


	DbSqlQuery query(err,&m_pDbManager);

	query.Execute(err,"DROP TABLE TEST_1");
	query.Execute(err,"CREATE TABLE TEST_1 (COL_DEC TIMESTAMP default CURRENT_TIMESTAMP NOT NULL,COL_TEXT  blob sub_type text)");

	//query.Execute(err,"DROP TABLE BUS_BIG_PICTURE");
	//err.setError(0);
	//QString sql="create table BUS_BIG_PICTURE ( BPIC_ID INTEGER not null,BPIC_GLOBAL_ID VARCHAR(15) ,BPIC_DAT_LAST_MODIFIED TIMESTAMP not null,BPIC_PICTURE BLOB not null, constraint BUS_BIG_PICTURE_2PK primary key (BPIC_ID) )";

	//query.Execute(err,sql);

	//qDebug()<<err.getErrorText();
	//query.Execute(err,"INSERT INTO TEST_1 (COL_TEXT) VALUES ('xx')");
	query.Prepare(err,"INSERT INTO TEST_1 (COL_TEXT) VALUES (?)");

	QDateTime date; //int nInt=1;
	QString txt="Ante";
	//query.BindValue(0,date);
	query.BindValue(0,txt);
	query.ExecutePrepared(err);

	int nID=m_pDbManager.getNextInsertId(err,query.GetDbConnection(),"COAS_ID");


	txt="ww";
	query.BindValue(0,txt);
	query.ExecutePrepared(err);

	//query.Execute(err,"UPDATE TEST_1 SET COL_TEXT='BB' WHERE COL_TEXT='Ante'");

	query.Execute(err,"SELECT * FROM TEST_1");
	
	DbRecordSet lst;
	//lst.addColumn(QVariant::Double,"ID");
	query.FetchData(lst);

	lst.Dump();
	//qDebug()<<lst.getDataRef(0,0).toString()<<":"<<lst.getDataRef(0,0).type();
	//qDebug()<<lst.getDataRef(0,1).toString()<<":"<<lst.getDataRef(0,1).type();

/*
	int record_count = query.record().count();	//Record column count.
	while (query.next()) 
	{

		for(int i=0;i<record_count;++i)
		{
			qDebug()<<query.record().fieldName(i)<<" : "<<query.record().value(i).type();
		}
	}

*/


	return a.exec();
}
