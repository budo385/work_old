
class Foo{       
public: 
	double One( long inVal );        
	double Two( long inVal );
};


void main( int argc, char **argv )
{
 double (Foo::*funcPtr)( long ) = &Foo::One; 	
 Foo aFoo; 	
 double result =(aFoo.*funcPtr)( 2 );  	
 return 0;

}