#include "test_app.h"
#include <QMessageBox>
#include "common/common/entity_id_collection.h"

#include "bus_trans_client/bus_trans_client/businessservicemanager_thinclient.h"
extern BusinessServiceManager	*g_pBoSet;	

Test_App::Test_App(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);
}

Test_App::~Test_App()
{
	Status err;
	dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet)->QcLogout(err);
}


void Test_App::on_pushButton_clicked()
{
	HTTPClientConnectionSettings conn;
	conn.m_nPort=9999;
	conn.m_bUseSSL=true;
	conn.m_strServerIP="localhost";
	conn.m_bUseCompression=true;

	Status err;
	dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet)->QcLogin(err,"bt","bt",conn);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,"Error",err.getErrorText());
		return;
	}

	//lets try it: read all contacts (as for contact list):
	DbRecordSet filter;
	DbRecordSet data;
	g_pBoSet->app->MainEntitySelector->ReadData(err,ENTITY_BUS_CONTACT,filter,data);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,"Error",err.getErrorText());
	}

	//data.Dump();
}