#ifndef TEST_APP_H
#define TEST_APP_H

#include <QtGui/QMainWindow>
#include "ui_test_app.h"

class Test_App : public QMainWindow
{
	Q_OBJECT

public:
	Test_App(QWidget *parent = 0, Qt::WFlags flags = 0);
	~Test_App();

private slots:
	void on_pushButton_clicked();

private:
	Ui::Test_AppClass ui;
};

#endif // TEST_APP_H
