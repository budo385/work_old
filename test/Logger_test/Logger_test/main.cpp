#include <QtCore/QCoreApplication>
#include "common/logger.h"
#include "common/loggerfile.h"
#include "common/loggerremote.h"

//Logiranje preko remote funkcije (remote logger)
/*void writer(const QString &mate);
LoggerFile *log2;
void writer(const QString &mate)
{
	log2->logWrite(mate);
}
*/

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

	Logger *logger;
	logger = Logger::getInstance();

	QString string1 = "File Logiranje";
	LoggerFile logFile;
	logger->AddTarget(&logFile);
	logger->logWrite(string1);

// Ovo pise direktno u file
/*	QString string = "File loger";
	LoggerFile log;
	log.logWrite(string);
*/

// Ovo pise remote, preko pointera na funckiju. (vidi deklaraciju u zaglavlju)
/*	QString string1 = "File loger1";
	LoggerRemote log1;
	log2 = new LoggerFile;
	log1.Initialize(writer);
	log1.logWrite(string1);*/

    return a.exec();
}