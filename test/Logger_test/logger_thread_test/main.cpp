#include <QtCore/QCoreApplication>
#include "common/logger.h"
#include "common/loggerfile.h"
#include "write_log.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

	Logger *logger;
	logger = Logger::getInstance();
	LoggerFile logFile;
	logger->AddTarget(&logFile);

	write_log *pisi= new write_log	();
	write_log *pisi1= new write_log	();
	write_log *pisi2= new write_log	();
	pisi->start();
	pisi1->start();
	pisi2->start();

	return a.exec();
}