/****************************************************************************
**
** Copyright (C) 2011 Digia Plc
** All rights reserved.
** For any questions to Digia, please use contact form at http://qt.digia.com
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia nor the names of its contributors
**     may be used to endorse or promote products derived from this 
**     software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
** 
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
****************************************************************************/

//! [0]
#include <QtGui>


#include "mainwindow.h"
//! [0]

//! [1]
MainWindow::MainWindow()
{
    textEdit = new QTextEdit;
    setCentralWidget(textEdit);

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();
    createDockWindows();

    setWindowTitle(tr("Dock Widgets"));

    newLetter();
    setUnifiedTitleAndToolBarOnMac(true);
}
//! [1]

//! [2]
void MainWindow::newLetter()
{
    textEdit->clear();

    QTextCursor cursor(textEdit->textCursor());
    cursor.movePosition(QTextCursor::Start);
    QTextFrame *topFrame = cursor.currentFrame();
    QTextFrameFormat topFrameFormat = topFrame->frameFormat();
    topFrameFormat.setPadding(16);
    topFrame->setFrameFormat(topFrameFormat);

    QTextCharFormat textFormat;
    QTextCharFormat boldFormat;
    boldFormat.setFontWeight(QFont::Bold);
    QTextCharFormat italicFormat;
    italicFormat.setFontItalic(true);

    QTextTableFormat tableFormat;
    tableFormat.setBorder(1);
    tableFormat.setCellPadding(16);
    tableFormat.setAlignment(Qt::AlignRight);
    cursor.insertTable(1, 1, tableFormat);
    cursor.insertText("The Firm", boldFormat);
    cursor.insertBlock();
    cursor.insertText("321 City Street", textFormat);
    cursor.insertBlock();
    cursor.insertText("Industry Park");
    cursor.insertBlock();
    cursor.insertText("Some Country");
    cursor.setPosition(topFrame->lastPosition());
    cursor.insertText(QDate::currentDate().toString("d MMMM yyyy"), textFormat);
    cursor.insertBlock();
    cursor.insertBlock();
    cursor.insertText("Dear ", textFormat);
    cursor.insertText("NAME", italicFormat);
    cursor.insertText(",", textFormat);
    for (int i = 0; i < 3; ++i)
        cursor.insertBlock();
    cursor.insertText(tr("Yours sincerely,"), textFormat);
    for (int i = 0; i < 3; ++i)
        cursor.insertBlock();
    cursor.insertText("The Boss", textFormat);
    cursor.insertBlock();
    cursor.insertText("ADDRESS", italicFormat);
}
//! [2]

//! [3]
void MainWindow::print()
{
#ifndef QT_NO_PRINTDIALOG
    QTextDocument *document = textEdit->document();
    QPrinter printer;

    QPrintDialog *dlg = new QPrintDialog(&printer, this);
    if (dlg->exec() != QDialog::Accepted)
        return;

    document->print(&printer);

    statusBar()->showMessage(tr("Ready"), 2000);
#endif
}
//! [3]

//! [4]
void MainWindow::save()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                        tr("Choose a file name"), ".",
                        tr("HTML (*.html *.htm)"));
    if (fileName.isEmpty())
        return;
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Dock Widgets"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    QTextStream out(&file);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    out << textEdit->toHtml();
    QApplication::restoreOverrideCursor();

    statusBar()->showMessage(tr("Saved '%1'").arg(fileName), 2000);
}
//! [4]

//! [5]
void MainWindow::undo()
{
    QTextDocument *document = textEdit->document();
    document->undo();
}
//! [5]

//! [6]
void MainWindow::insertCustomer(const QString &customer)
{
    if (customer.isEmpty())
        return;
    QStringList customerList = customer.split(", ");
    QTextDocument *document = textEdit->document();
    QTextCursor cursor = document->find("NAME");
    if (!cursor.isNull()) {
        cursor.beginEditBlock();
        cursor.insertText(customerList.at(0));
        QTextCursor oldcursor = cursor;
        cursor = document->find("ADDRESS");
        if (!cursor.isNull()) {
            for (int i = 1; i < customerList.size(); ++i) {
                cursor.insertBlock();
                cursor.insertText(customerList.at(i));
            }
            cursor.endEditBlock();
        }
        else
            oldcursor.endEditBlock();
    }
}
//! [6]

//! [7]
void MainWindow::addParagraph(const QString &paragraph)
{
    if (paragraph.isEmpty())
        return;
    QTextDocument *document = textEdit->document();
    QTextCursor cursor = document->find(tr("Yours sincerely,"));
    if (cursor.isNull())
        return;
    cursor.beginEditBlock();
    cursor.movePosition(QTextCursor::PreviousBlock, QTextCursor::MoveAnchor, 2);
    cursor.insertBlock();
    cursor.insertText(paragraph);
    cursor.insertBlock();
    cursor.endEditBlock();

}
//! [7]

void MainWindow::about()
{
   QMessageBox::about(this, tr("About Dock Widgets"),
            tr("The <b>Dock Widgets</b> example demonstrates how to "
               "use Qt's dock widgets. You can enter your own text, "
               "click a customer to add a customer name and "
               "address, and click standard paragraphs to add them."));
}

void MainWindow::createActions()
{
    newLetterAct = new QAction(QIcon(":/images/new.png"), tr("&New Letter"),
                               this);
    newLetterAct->setShortcuts(QKeySequence::New);
    newLetterAct->setStatusTip(tr("Create a new form letter"));
    connect(newLetterAct, SIGNAL(triggered()), this, SLOT(newLetter()));

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save..."), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the current form letter"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    printAct = new QAction(QIcon(":/images/print.png"), tr("&Print..."), this);
    printAct->setShortcuts(QKeySequence::Print);
    printAct->setStatusTip(tr("Print the current form letter"));
    connect(printAct, SIGNAL(triggered()), this, SLOT(print()));

    undoAct = new QAction(QIcon(":/images/undo.png"), tr("&Undo"), this);
    undoAct->setShortcuts(QKeySequence::Undo);
    undoAct->setStatusTip(tr("Undo the last editing action"));
    connect(undoAct, SIGNAL(triggered()), this, SLOT(undo()));

    quitAct = new QAction(tr("&Quit"), this);
    quitAct->setShortcuts(QKeySequence::Quit);
    quitAct->setStatusTip(tr("Quit the application"));
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));

    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newLetterAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(quitAct);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(undoAct);

    viewMenu = menuBar()->addMenu(tr("&View"));

    menuBar()->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
}

void MainWindow::createToolBars()
{
    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(newLetterAct);
    fileToolBar->addAction(saveAct);
    fileToolBar->addAction(printAct);

    editToolBar = addToolBar(tr("Edit"));
    editToolBar->addAction(undoAct);
}

//! [8]
void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}
//! [8]

//! [9]
void MainWindow::createDockWindows()
{
	setTabPosition (Qt::BottomDockWidgetArea, QTabWidget::North );
	//setTabPosition (Qt::LeftDockWidgetArea, QTabWidget::West );


    QDockWidget *dock = new QDockWidget(tr("Customers"), this);
	m_lstpDockWins.append(dock);
    dock->setAllowedAreas(Qt::AllDockWidgetAreas); //Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	DockTitleBar *pTitleBar = new DockTitleBar(dock);
	dock->setTitleBarWidget(pTitleBar);
	pTitleBar->SetTitle("Customers","Customers u prosirenom");



    customerList = new QListWidget(dock);
    customerList->addItems(QStringList()
            << "John Doe, Harmony Enterprises, 12 Lakeside, Ambleton"
            << "Jane Doe, Memorabilia, 23 Watersedge, Beaton"
            << "Tammy Shea, Tiblanka, 38 Sea Views, Carlton"
            << "Tim Sheen, Caraba Gifts, 48 Ocean Way, Deal"
            << "Sol Harvey, Chicos Coffee, 53 New Springs, Eccleston"
            << "Sally Hobart, Tiroli Tea, 67 Long River, Fedula");
    dock->setWidget(customerList);
    addDockWidget(Qt::BottomDockWidgetArea, dock);
    viewMenu->addAction(dock->toggleViewAction());





	QDockWidget *dock1 = new QDockWidget(tr("Nabijem Te"), this);
	m_lstpDockWins.append(dock1);
	dock1->setAllowedAreas(Qt::AllDockWidgetAreas); //Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	pTitleBar = new DockTitleBar(dock1);
	dock1->setTitleBarWidget(pTitleBar);
	pTitleBar->SetTitle("Nabijem Te","Nabijem Te u prosirenom");


	customerList2 = new QListWidget(dock1);
	customerList2->addItems(QStringList()
		<< "John Doe, Harmony Enterprises, 12 Lakeside, Ambleton"
		<< "Jane Doe, Memorabilia, 23 Watersedge, Beaton"
		<< "Tammy Shea, Tiblanka, 38 Sea Views, Carlton"
		<< "Tim Sheen, Caraba Gifts, 48 Ocean Way, Deal"
		<< "Sol Harvey, Chicos Coffee, 53 New Springs, Eccleston"
		<< "Sally Hobart, Tiroli Tea, 67 Long River, Fedula");
	dock1->setWidget(customerList2);
	//addDockWidget(Qt::RightDockWidgetArea, dock);
	tabifyDockWidget(dock,dock1);
	viewMenu->addAction(dock1->toggleViewAction());






    QDockWidget *dock2 = new QDockWidget(tr("Paragraphs"), this);
	m_lstpDockWins.append(dock2);
	dock2->setAllowedAreas(Qt::AllDockWidgetAreas);
	pTitleBar = new DockTitleBar(dock2);
	dock2->setTitleBarWidget(pTitleBar);
	pTitleBar->SetTitle("Paragraphs","Paragraphs u prosirenom");

    paragraphsList = new QListWidget(dock2);
    paragraphsList->addItems(QStringList()
            << "Thank you for your payment which we have received today."
            << "Your order has been dispatched and should be with you "
               "within 28 days."
            << "We have dispatched those items that were in stock. The "
               "rest of your order will be dispatched once all the "
               "remaining items have arrived at our warehouse. No "
               "additional shipping charges will be made."
            << "You made a small overpayment (less than $5) which we "
               "will keep on account for you, or return at your request."
            << "You made a small underpayment (less than $1), but we have "
               "sent your order anyway. We'll add this underpayment to "
               "your next bill."
            << "Unfortunately you did not send enough money. Please remit "
               "an additional $. Your order will be dispatched as soon as "
               "the complete amount has been received."
            << "You made an overpayment (more than $5). Do you wish to "
               "buy more items, or should we return the excess to you?");
    dock2->setWidget(paragraphsList);
	tabifyDockWidget(dock1,dock2);
	viewMenu->addAction(dock2->toggleViewAction());





    connect(customerList, SIGNAL(currentTextChanged(QString)),
            this, SLOT(insertCustomer(QString)));
    connect(paragraphsList, SIGNAL(currentTextChanged(QString)),
            this, SLOT(addParagraph(QString)));


}
//! [9]





DockTitleBar::DockTitleBar(QDockWidget *parent)
:QFrame(parent),m_bMinimized(false),m_bHidden(false),m_pTabForHiddenDocks(NULL)
{
	m_strTitleStyle			= "QFrame#%1 {border: 1px solid rgb(140,134,123); background:qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 rgb(224,224,224),stop:0.5 rgb(186,186,186), stop:1  rgb(110,110,110));} QLabel {color:black}";
	m_strDockStyleHidden	= "QFrame#%1 {border: 1px solid rgb(140,134,123); background:qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 rgb(200,200,200), stop:1  rgb(110,110,110));}";
	m_strDockStyle			= "QDockWidget {border: %1px;  background:qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 rgb(200,200,200), stop:1  rgb(110,110,110));}";
	m_strToolButtonStyle	= "QToolButton:hover {border: 1px solid rgb(140,134,123); background-color: rgb(236,233,216);}";
	m_nDockBorderWidth		= 4;
	m_nResizeLimitOsffset	= 20;

	
	//create actions:
	//----------------------------------------------------------------
	m_pActClose = new QAction(tr("Close"), this);
	m_pActClose->setIcon(QIcon(":images/dock_close.png"));
	connect(m_pActClose, SIGNAL(triggered()), this, SLOT(OnClose()));

	m_pActFloat = new QAction(tr("Floatable"), this);
	m_pActFloat->setIcon(QIcon(":images/dock_float.png"));
	connect(m_pActFloat, SIGNAL(triggered()), this, SLOT(OnFloat()));

	m_pActMinMax = new QAction(tr("Minimize"), this);
	m_pActMinMax->setIcon(QIcon(":images/dock_arrow_down.png"));
	connect(m_pActMinMax, SIGNAL(triggered()), this, SLOT(OnMinMax()));

	m_pActHide = new QAction(tr("Auto Hide"), this);
	m_pActHide->setIcon(QIcon(":images/dock_auto_hide_ver.png"));
	connect(m_pActHide, SIGNAL(triggered()), this, SLOT(OnHide()));


	QSize buttonSize(17, 16);
	m_pBtnClose				= new QToolButton;
	m_pBtnFloat				= new QToolButton;
	m_pBtnMinMax			= new QToolButton;
	m_pBtnHide				= new QToolButton;
	
	//size
	m_pBtnClose	->setMaximumSize(buttonSize);
	m_pBtnFloat	->setMaximumSize(buttonSize);
	m_pBtnMinMax->setMaximumSize(buttonSize);
	m_pBtnHide	->setMaximumSize(buttonSize);

	m_pBtnClose	->setAutoRaise(true);
	m_pBtnFloat	->setAutoRaise(true);
	m_pBtnMinMax->setAutoRaise(true);
	m_pBtnHide	->setAutoRaise(true);

	m_pBtnClose		->setStyleSheet(m_strToolButtonStyle);
	m_pBtnFloat		->setStyleSheet(m_strToolButtonStyle);
	m_pBtnMinMax	->setStyleSheet(m_strToolButtonStyle);
	m_pBtnHide		->setStyleSheet(m_strToolButtonStyle);

	m_pBtnClose		->setDefaultAction(m_pActClose);
	m_pBtnFloat		->setDefaultAction(m_pActFloat);
	m_pBtnMinMax	->setDefaultAction(m_pActMinMax);
	m_pBtnHide		->setDefaultAction(m_pActHide);


	m_pTitle = new QLabel();


	QHBoxLayout *buttonLayout_menu_h = new QHBoxLayout;
	buttonLayout_menu_h->addWidget(m_pTitle);
	buttonLayout_menu_h->addStretch(1);
	buttonLayout_menu_h->addWidget(m_pBtnHide);
	buttonLayout_menu_h->addWidget(m_pBtnMinMax);
	buttonLayout_menu_h->addWidget(m_pBtnFloat);
	buttonLayout_menu_h->addWidget(m_pBtnClose);
	buttonLayout_menu_h->setContentsMargins(5,1,1,1);
	buttonLayout_menu_h->setSpacing(2);

	setLayout(buttonLayout_menu_h);

	setObjectName("dockTitle");
	setStyleSheet(m_strTitleStyle.arg("dockTitle"));
	parentWidget()->setStyleSheet(m_strDockStyle.arg(m_nDockBorderWidth));

	connect(parentWidget(),SIGNAL(dockLocationChanged(Qt::DockWidgetArea)),this,SLOT(OnDockLocationChanged(Qt::DockWidgetArea)));
	connect(parentWidget(),SIGNAL(topLevelChanged(bool)),this,SLOT(OnTopLevelChanged(bool)));

}

DockTitleBar::~DockTitleBar()
{


}

QSize DockTitleBar::minimumSizeHint () const
{
	return QSize(80,30);
}
QSize DockTitleBar::sizeHint () const
{
	return QSize(80,30);
}

void DockTitleBar::SetTitle(QString strWindowTitle, QString strTitleBar)
{
	parentWidget()->setWindowTitle(strWindowTitle);
	m_pTitle->setText(strTitleBar);
}

void DockTitleBar::SetStyle(QString strTitleStyle, QString strDockStyle, QString strToolButtonStyle, int nDockBorderWidth, int nResizeLimitOsffset)	
{m_strTitleStyle=strTitleStyle;m_strDockStyle=strDockStyle;m_strToolButtonStyle=strToolButtonStyle;m_nDockBorderWidth=nDockBorderWidth;m_nResizeLimitOsffset=nResizeLimitOsffset;}


void DockTitleBar::AddToolButton(QToolButton *button, bool bLeftAlign)
{
	QHBoxLayout *box=qobject_cast<QHBoxLayout*>(layout());

	if (bLeftAlign)
		box->insertWidget(1,button);
	else
		box->insertWidget(2,button);
}

void DockTitleBar::OnClose()
{
	parentWidget()->close();
	
}
void DockTitleBar::OnFloat()
{
	QDockWidget *dockWidget = qobject_cast<QDockWidget*>(parentWidget());
	if (dockWidget)
	{
		if(dockWidget->isFloating())
		{
			dockWidget->setFloating(false);
		}
		else
		{
			dockWidget->setFloating(true);
		}
	}
}
void DockTitleBar::OnMinMax()
{
	if (!m_bMinimized)
	{
		m_pActMinMax->setIcon(QIcon(":images/dock_arrow_up.png"));
		m_nPrevHeight=parentWidget()->height();
		parentWidget()->setMaximumHeight(30);
	}
	else
	{
		m_pActMinMax->setIcon(QIcon(":images/dock_arrow_down.png"));
		parentWidget()->setMaximumHeight(65535);
		parentWidget()->resize(parentWidget()->width(),m_nPrevHeight);
		updateGeometry();
	}
	m_bMinimized=!m_bMinimized;
}

void DockTitleBar::OnHide()
{
	QDockWidget *dockWidget = qobject_cast<QDockWidget*>(parentWidget());
	QMainWindow *pMain = qobject_cast<QMainWindow*>(dockWidget->parentWidget());

	if (pMain)
	{
		if (m_bHidden)
		{
			if(m_pTabForHiddenDocks)
				m_pTabForHiddenDocks->Destroy();
			m_pTabForHiddenDocks=NULL;
		}
		else
		{
			m_pTabForHiddenDocks = new DockTabBar(pMain);
			m_pTabForHiddenDocks->Create(dockWidget);
		}
	}
	
}


void DockTitleBar::OnDockLocationChanged(Qt::DockWidgetArea area)
{
	if (area==Qt::RightDockWidgetArea || area==Qt::LeftDockWidgetArea)
	{
		m_pActMinMax->setVisible(true);
	}
	else
	{
		m_pActMinMax->setVisible(false);
	}
}

//bHiddenChange=true then hidden status change else float status changed
void DockTitleBar::UpdateButtonState(bool bHiddenChange)
{
	if (bHiddenChange)
	{
		if (!m_bHidden)
		{
			m_pBtnFloat->setVisible(true);
			m_pBtnMinMax->setVisible(true);
			m_pActHide->setIcon(QIcon(":images/dock_auto_hide_ver.png"));
		}
		else
		{
			m_pBtnFloat->setVisible(false);
			m_pBtnMinMax->setVisible(false);
			m_pActHide->setIcon(QIcon(":images/dock_auto_hide_hor.png"));
		}
	}
	else
	{
		QDockWidget *dockWidget = qobject_cast<QDockWidget*>(parentWidget());
		if (!dockWidget->isFloating())
		{
			m_pBtnFloat->setVisible(true);
			m_pBtnMinMax->setVisible(true);
			m_pBtnHide->setVisible(true);
		}
		else
		{
			m_pBtnFloat->setVisible(false);
			m_pBtnMinMax->setVisible(false);
			m_pBtnHide->setVisible(false);
		}
	}



}

void DockTitleBar::OnTopLevelChanged(bool bFloat)
{
	if (m_bHidden) 
		return;

	UpdateButtonState(false);
}
bool DockTitleBar::event(QEvent *event)
{
	if (m_bHidden) //if hidden do not allow float dbl click events on TitleBar-> see if this block any buttons on top...
	{
		switch (event->type()) 
		{
		case QEvent::MouseButtonPress:
		case QEvent::MouseButtonDblClick:
		case QEvent::MouseMove:
			return true; 
		}
	}
	return false; //pass it on
}



//-----------------------------------------------------------------
//			DOCK TAB BAR for hidden docks
//-----------------------------------------------------------------


DockTabBar::DockTabBar(QMainWindow *pMain,QTabBar *parent)
:m_pMain(pMain),QTabBar(parent),m_pToolBarContainer(NULL),m_CurrAnimation(NULL),m_nTimerID(0),m_bTestMouseMove(false),m_nTabIndex_Hide(-1),m_nTabIndex_Show(-1)
{

}

DockTabBar::~DockTabBar()
{

}

QSize DockTabBar::sizeHint () const
{
	if(m_nSourceDirection==Qt::TopDockWidgetArea || m_nSourceDirection==Qt::BottomDockWidgetArea)
		return QSize(4000,QTabBar::sizeHint().height());
	else
		return QSize(QTabBar::sizeHint().width(),4000);
}

/*
	Get's docks windows, find 'em tabbed, make tabs for hide, and hide 'em
*/
void DockTabBar::Create(QDockWidget* pDock)
{

	//-------------------------------------------------
	//test what area
	//-------------------------------------------------
	m_nSourceDirection = m_pMain->dockWidgetArea (pDock);
	Qt::ToolBarArea nToolBarArea;

	switch(m_nSourceDirection)
	{
	case Qt::LeftDockWidgetArea:
		nToolBarArea=Qt::LeftToolBarArea;
		break;
	case Qt::RightDockWidgetArea:
		nToolBarArea=Qt::RightToolBarArea;
		break;
	case Qt::TopDockWidgetArea:
		nToolBarArea=Qt::TopToolBarArea;
		break;
	case Qt::BottomDockWidgetArea:
		nToolBarArea=Qt::BottomToolBarArea;
		break;
	}

	//-------------------------------------------------
	//get all tabified docks from same dock widget area
	//-------------------------------------------------
	QList<QDockWidget *> lstDocks = m_pMain->tabifiedDockWidgets(pDock);
	lstDocks.append(pDock);
	int nCntDocks=lstDocks.size();

	//find inside tabs on Main Window just for order of tabs...:
	bool bFound=false;
	QList<QTabBar *> lst = m_pMain->findChildren<QTabBar *>();
	int nSize=lst.size();
	for (int i=0;i<nSize;i++)
	{
		int nTabCnt=lst.at(i)->count();
		for (int k=0;k<nTabCnt;k++)
		{
			for (int z=0;z<nCntDocks;z++)
			{
				if (lst.at(i)->tabText(k)==lstDocks.at(z)->windowTitle())
				{
					bFound=true;
					m_lstDocks[k]=lstDocks.at(z);
					break; //from 2nd loop
				}
			}
		}
		if (bFound)
			break;
	}




	//-------------------------------------------------
	//hide all docks and create frames for animation hide/show, disable all buttons on docks title bars
	//-------------------------------------------------
	blockSignals(true);
	QMapIterator<int,QDockWidget *> i(m_lstDocks);
	while (i.hasNext()) 
	{
		i.next();

		//set flag hidden on docks
		DockTitleBar *dockTitle = qobject_cast<DockTitleBar*>(i.value()->titleBarWidget());
		if (dockTitle)
		{
			dockTitle->m_bHidden=true;
			dockTitle->UpdateButtonState(true);

			//add tab for each dock
			addTab(i.value()->windowTitle());
			m_lstPrevDockDimension[i.key()]=pDock->geometry(); //set all dims to active one, as this one only have right dimension
			i.value()->setFloating(true);
			i.value()->hide();
	

			//create frame for each hidden dock for animation
			QFrame *frame = new QFrame(m_pMain->centralWidget());
			QRect limit(dockTitle->m_nResizeLimitOsffset,dockTitle->m_nResizeLimitOsffset,m_pMain->centralWidget()->width()-2*dockTitle->m_nResizeLimitOsffset,m_pMain->centralWidget()->height()-2*dockTitle->m_nResizeLimitOsffset);
			Resizer *resizer = new Resizer(m_nSourceDirection,limit,frame);
			QVBoxLayout *buttonLayout_menu_h = new QVBoxLayout;
			buttonLayout_menu_h->addWidget(resizer);
			buttonLayout_menu_h->addWidget(i.value());
			buttonLayout_menu_h->setSpacing(1);
			buttonLayout_menu_h->setContentsMargins(dockTitle->m_nDockBorderWidth,0,dockTitle->m_nDockBorderWidth,dockTitle->m_nDockBorderWidth);
			frame->setLayout(buttonLayout_menu_h);

			frame->setObjectName("dockContainer");
			frame->setStyleSheet(dockTitle->m_strDockStyleHidden.arg("dockContainer"));

			//frame->setGeometry(0,m_pMain->centralWidget()->height(),m_pMain->centralWidget()->width(),1);
			frame->hide();
			m_lstDockContainers[i.key()]=frame; //store frame

		}
	}
	blockSignals(false);


	//-------------------------------------------------
	//create ToolBar container to set tabar inside it in MainWindow
	//-------------------------------------------------

	//setDrawBase(true);
	setExpanding(false);

	m_pToolBarContainer = new QToolBar(m_pMain);
	m_pToolBarContainer->addWidget(this);
	m_pToolBarContainer->setMovable(false);


	m_pMain->addToolBar(nToolBarArea,m_pToolBarContainer);

}

void DockTabBar::Destroy()
{
	//Hide all
	//destroy tabs
	//show dockwidgets docked..




}

bool DockTabBar::IsCursorInsideTargetArea()
{
	QPoint currentMousePos= QCursor::pos();

	QPoint p1=mapFromGlobal(currentMousePos);
	qDebug()<<p1.x();
	qDebug()<<p1.y();

	QPoint p2=m_lstDockContainers.value(m_nTabIndex_Hide)->mapFromGlobal(currentMousePos);
	qDebug()<<p2.x();
	qDebug()<<p2.y();

	return true;


}


void DockTabBar::OnExecuteAnimationShow()
{
	qDebug()<<"start show "<<m_nTabIndex_Show;
	m_lstDocks.value(m_nTabIndex_Show)->show();
	m_lstDockContainers.value(m_nTabIndex_Show)->show();

	m_CurrAnimation = new QPropertyAnimation(m_lstDockContainers.value(m_nTabIndex_Show), "geometry",this);
	m_CurrAnimation->setDuration(300);
	m_CurrAnimation->setLoopCount(1);
	connect(m_CurrAnimation,SIGNAL(finished ()),this,SLOT(OnAnimantionFinished()));

	//based on orientation and current dimension set new sizes:
	switch(m_nSourceDirection)
	{
	case Qt::LeftDockWidgetArea:
		{
			m_CurrAnimation->setStartValue(QRect(0, 0, 1, m_lstPrevDockDimension.value(m_nTabIndex_Show).height()));
			m_CurrAnimation->setEndValue(QRect(0, 0, m_lstPrevDockDimension.value(m_nTabIndex_Show).width(), m_lstPrevDockDimension.value(m_nTabIndex_Show).height()));
		}
		break;
	case Qt::RightDockWidgetArea:
		{
			m_CurrAnimation->setStartValue(QRect(0, m_pMain->centralWidget()->width(), 1, m_lstPrevDockDimension.value(m_nTabIndex_Show).height()));
			m_CurrAnimation->setEndValue(QRect(m_pMain->centralWidget()->width()-m_lstPrevDockDimension.value(m_nTabIndex_Show).width(),0, m_lstPrevDockDimension.value(m_nTabIndex_Show).width(), m_lstPrevDockDimension.value(m_nTabIndex_Show).height()));
		}
		break;
	case Qt::TopDockWidgetArea:
		{
			m_CurrAnimation->setStartValue(QRect(0, 0, m_lstPrevDockDimension.value(m_nTabIndex_Show).width(),1));
			m_CurrAnimation->setEndValue(QRect(0, 0, m_lstPrevDockDimension.value(m_nTabIndex_Show).width(), m_lstPrevDockDimension.value(m_nTabIndex_Show).height()));
		}
		break;
	case Qt::BottomDockWidgetArea:
		{
			m_CurrAnimation->setStartValue(QRect(0, m_pMain->centralWidget()->height(), m_lstPrevDockDimension.value(m_nTabIndex_Show).width(),1));
			m_CurrAnimation->setEndValue(QRect(0, m_pMain->centralWidget()->height()-m_lstPrevDockDimension.value(m_nTabIndex_Show).height(),m_lstPrevDockDimension.value(m_nTabIndex_Show).width(), m_lstPrevDockDimension.value(m_nTabIndex_Show).height()));
		}
		break;
	}

	m_CurrAnimation->start();

	//m_nTimerID=startTimer(10000); //start timer for 2 sec interval
}

void DockTabBar::OnExecuteAnimationHide()
{
	m_nTabIndex_Hide=m_nTabIndex_Show;
	qDebug()<<"start hide "<<m_nTabIndex_Hide;
	m_CurrAnimation = new QPropertyAnimation(m_lstDockContainers.value(m_nTabIndex_Hide), "geometry",this);
	m_CurrAnimation->setDuration(200);
	m_CurrAnimation->setLoopCount(1);
	connect(m_CurrAnimation,SIGNAL(finished ()),this,SLOT(OnAnimantionFinished()));

	//based on orientation and current dimension set new sizes:
	switch(m_nSourceDirection)
	{
	case Qt::LeftDockWidgetArea:
		{
			m_CurrAnimation->setStartValue(QRect(0, 0, m_lstPrevDockDimension.value(m_nTabIndex_Hide).width(), m_lstPrevDockDimension.value(m_nTabIndex_Hide).height()));
			m_CurrAnimation->setEndValue(QRect(0, 0, 1, m_lstPrevDockDimension.value(m_nTabIndex_Hide).height()));
		}
		break;
	case Qt::RightDockWidgetArea:
		{
			m_CurrAnimation->setStartValue(QRect(m_pMain->centralWidget()->width()-m_lstPrevDockDimension.value(m_nTabIndex_Hide).width(),0, m_lstPrevDockDimension.value(m_nTabIndex_Hide).width(), m_lstPrevDockDimension.value(m_nTabIndex_Hide).height()));
			m_CurrAnimation->setEndValue(QRect(0, m_pMain->centralWidget()->width(), 1, m_lstPrevDockDimension.value(m_nTabIndex_Hide).height()));
			
		}
		break;
	case Qt::TopDockWidgetArea:
		{
			m_CurrAnimation->setStartValue(QRect(0, 0, m_lstPrevDockDimension.value(m_nTabIndex_Hide).width(), m_lstPrevDockDimension.value(m_nTabIndex_Hide).height()));
			m_CurrAnimation->setEndValue(QRect(0, 0, m_lstPrevDockDimension.value(m_nTabIndex_Hide).width(),1));

		}
		break;
	case Qt::BottomDockWidgetArea:
		{
			m_CurrAnimation->setStartValue(QRect(0, m_pMain->centralWidget()->height()-m_lstPrevDockDimension.value(m_nTabIndex_Hide).height(),m_lstPrevDockDimension.value(m_nTabIndex_Hide).width(), m_lstPrevDockDimension.value(m_nTabIndex_Hide).height()));
			m_CurrAnimation->setEndValue(QRect(0, m_pMain->centralWidget()->height(), m_lstPrevDockDimension.value(m_nTabIndex_Hide).width(),1));
		}
		break;
	}

	m_CurrAnimation->start();
}


bool DockTabBar::event(QEvent *event)
{
	switch (event->type()) 
	{
	case QEvent::HoverEnter:
		{
			m_bTestMouseMove=true;
		}
		break;
	case QEvent::HoverLeave:
		{
			m_bTestMouseMove=false;
			if (m_nTabIndex_Show>=0)
			{
				//QTimer::singleShot(0,this,SLOT(OnExecuteAnimationHide()));
			}
		}
		break;
	case QEvent::HoverMove:
		{
			if (m_bTestMouseMove)
			{
				//calculate what tab
				int nCurrentIdx=-1;
				QPoint currentMousePos= mapFromGlobal(QCursor::pos());

				for (int i=0;i<count();i++)
				{
					QRect recTab=tabRect(i);
					if (recTab.contains(currentMousePos))
					{
						nCurrentIdx=i;
						break;
					}
				}

				if (!m_CurrAnimation)//if no current animation
				{
					if (nCurrentIdx==-1)
					{
						if (m_nTabIndex_Show>=0)
						{
							//QTimer::singleShot(0,this,SLOT(OnExecuteAnimationHide()));
						}
					}
					else
					{
						if (nCurrentIdx!=m_nTabIndex_Show)
						{
							if (m_nTabIndex_Show>=0)
							{
								m_lstDocks.value(m_nTabIndex_Show)->hide();
								m_lstDockContainers.value(m_nTabIndex_Show)->hide();
							}
							m_nTabIndex_Show=nCurrentIdx;
							QTimer::singleShot(0,this,SLOT(OnExecuteAnimationShow()));
						}
					}
				}
			}
		}
		break;
	}

	return QTabBar::event(event);
}

//frame does not catch hover!!!
//when shown: setTimer and check if mouse is inside, every 1sec:
//then close...

void DockTabBar::OnAnimantionFinished()
{
	//start another show/hide:
	if (m_nTabIndex_Hide>=0)
	{
		m_lstDocks.value(m_nTabIndex_Hide)->hide();
		m_lstDockContainers.value(m_nTabIndex_Hide)->hide();
		m_nTabIndex_Hide=-1;
		m_nTabIndex_Show=-1;
	}
	if (m_nTabIndex_Show)
	{
		m_nTimerID=startTimer(1000); //track mouse
	}

	m_CurrAnimation->deleteLater();
	m_CurrAnimation=NULL;

}

void DockTabBar::timerEvent(QTimerEvent * event)
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		if (m_nTabIndex_Show>=0)
		{
			bool bStillMouseInside=false;
			QPoint currentMousePos= QCursor::pos();

			//test if mouse is inside tab bars:
			QPoint p1=mapFromGlobal(currentMousePos);
			for (int i=0;i<count();i++)
			{
				QRect recTab=tabRect(i);
				if (recTab.contains(p1))
				{
					bStillMouseInside=true;
					break;
				}
			}

			//test if mouse is inside frame:
			QPoint p2=m_lstDockContainers.value(m_nTabIndex_Show)->parentWidget()->mapFromGlobal(currentMousePos);
			if (m_lstDockContainers.value(m_nTabIndex_Show)->geometry().contains(p2))
				bStillMouseInside=true;
/*
			qDebug()<<"x "<<p2.x();
			qDebug()<<"y "<<p2.y();
			qDebug()<<"x0 "<<m_lstDockContainers.value(m_nTabIndex_Show)->geometry().x();
			qDebug()<<"y0 "<<m_lstDockContainers.value(m_nTabIndex_Show)->geometry().y();
			qDebug()<<"w "<<m_lstDockContainers.value(m_nTabIndex_Show)->geometry().width();
			qDebug()<<"h "<<m_lstDockContainers.value(m_nTabIndex_Show)->geometry().height();
			*/

			if (bStillMouseInside)
			{
				m_nTimerID = startTimer(1000); //check for 1sek
			}
			else
			{
				m_nTimerID = -1;
				QTimer::singleShot(0,this,SLOT(OnExecuteAnimationHide()));
			}
		}
	}

}





//-----------------------------------------------------------------
//			RESIZER
//-----------------------------------------------------------------


Resizer::Resizer(int nSourceDirection, QRect rectLimits, QWidget *parent)
:QWidget(parent),m_pRubberBand(NULL),m_bPressed(false)
{
	m_nSourceDirection = nSourceDirection;
	m_rectLimits = rectLimits;

	if (m_nSourceDirection==Qt::LeftDockWidgetArea || m_nSourceDirection==Qt::RightDockWidgetArea)
		setCursor(Qt::SplitHCursor);
	else
		setCursor(Qt::SplitVCursor);

	setMinimumHeight(RESIZER_WIDTH);
}


Resizer::~Resizer()
{


}

void Resizer::mouseMoveEvent(QMouseEvent *e)
{
	if (!(e->buttons() & Qt::LeftButton) && !m_bPressed)
		return;

	if (m_pRubberBand)
	{
		QPoint curr_mouse_position = parentWidget()->mapToParent(mapToParent(e->pos()));

		//test limits:
		if (m_rectLimits.contains(curr_mouse_position))
		{
			if (m_nSourceDirection==Qt::LeftDockWidgetArea || m_nSourceDirection==Qt::RightDockWidgetArea)
				m_pRubberBand->setGeometry(curr_mouse_position.x(),0,RESIZER_WIDTH,m_OriginalParentGeometry.height());
			else
				m_pRubberBand->setGeometry(0,curr_mouse_position.y(),m_OriginalParentGeometry.width(),RESIZER_WIDTH);
		}
	}
}
void Resizer::mousePressEvent(QMouseEvent *e)
{
	if (e->button() == Qt::LeftButton) 
	{
		m_bPressed = true;
		m_OriginalParentGeometry=parentWidget()->geometry();

		QPoint curr_mouse_position = parentWidget()->mapToParent(mapToParent(e->pos()));
		//if (!rubberBand)
		m_pRubberBand = new QRubberBand(QRubberBand::Line, parentWidget()->parentWidget());

		switch(m_nSourceDirection)
		{
		case Qt::LeftDockWidgetArea:
			{
				m_pRubberBand->setGeometry(m_OriginalParentGeometry.width()-RESIZER_WIDTH,0,RESIZER_WIDTH,m_OriginalParentGeometry.height());
			}
			break;
		case Qt::RightDockWidgetArea:
			{
				m_pRubberBand->setGeometry(m_OriginalParentGeometry.x(),0,RESIZER_WIDTH,m_OriginalParentGeometry.height());
			}
			break;
		case Qt::TopDockWidgetArea:
			{
				m_pRubberBand->setGeometry(0,m_OriginalParentGeometry.height()-RESIZER_WIDTH,m_OriginalParentGeometry.width(),RESIZER_WIDTH);
			}
			break;
		case Qt::BottomDockWidgetArea:
			{
				m_pRubberBand->setGeometry(0,m_OriginalParentGeometry.y(),m_OriginalParentGeometry.width(),RESIZER_WIDTH);
			}
			break;
		}

		m_pRubberBand->show();
	}

}
void Resizer::mouseReleaseEvent(QMouseEvent *e)
{
	int nAbsX=m_pRubberBand->x();
	int nAbsY=m_pRubberBand->y();

	m_pRubberBand->hide();
	m_bPressed = false;
	m_pRubberBand->deleteLater();
	m_pRubberBand=NULL;


	switch(m_nSourceDirection)
	{
	case Qt::LeftDockWidgetArea:
		{
			parentWidget()->setGeometry(m_OriginalParentGeometry.x(),m_OriginalParentGeometry.y(),nAbsX,m_OriginalParentGeometry.height());
		}
		break;
	case Qt::RightDockWidgetArea:
		{
			parentWidget()->setGeometry(nAbsX,m_OriginalParentGeometry.y(),parentWidget()->parentWidget()->width()-nAbsX,m_OriginalParentGeometry.height());
		}
		break;
	case Qt::TopDockWidgetArea:
		{
			parentWidget()->setGeometry(m_OriginalParentGeometry.x(),m_OriginalParentGeometry.y(),m_OriginalParentGeometry.width(),nAbsY);
		}
	    break;
	case Qt::BottomDockWidgetArea:
		{
			parentWidget()->setGeometry(m_OriginalParentGeometry.x(),nAbsY,m_OriginalParentGeometry.width(),parentWidget()->parentWidget()->height()-nAbsY);
		}
	    break;
	}

	parentWidget()->update();
}


