/****************************************************************************
**
** Copyright (C) 2011 Digia Plc
** All rights reserved.
** For any questions to Digia, please use contact form at http://qt.digia.com
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia nor the names of its contributors
**     may be used to endorse or promote products derived from this 
**     software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
** 
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
class QAction;
class QListWidget;
class QMenu;
class QTextEdit;
QT_END_NAMESPACE

#include <QLabel>
#include <QTabBar>
#include <QMap>
#include <QRubberBand>
#include <QPropertyAnimation>
#include <QToolButton>

//! [0]
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

private slots:
    void newLetter();
    void save();
    void print();
    void undo();
    void about();
    void insertCustomer(const QString &customer);
    void addParagraph(const QString &paragraph);

private:
    void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();
    void createDockWindows();

    QTextEdit *textEdit;
    QListWidget *customerList;
	QListWidget *customerList2;
    QListWidget *paragraphsList;

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *viewMenu;
    QMenu *helpMenu;
    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    QAction *newLetterAct;
    QAction *saveAct;
    QAction *printAct;
    QAction *undoAct;
    QAction *aboutAct;
    QAction *aboutQtAct;
    QAction *quitAct;

	QList<QDockWidget *> m_lstpDockWins;

};
//! [0]

class DockTitleBar : public QFrame
{
	friend class DockTabBar;
	friend class Resizer;

	  Q_OBJECT
public:
	DockTitleBar(QDockWidget *parent);
	~DockTitleBar();

	void AddToolButton(QToolButton *button, bool bLeftAlign=true);
	void SetStyle(QString strTitleStyle, QString strDockStyle, QString strToolButtonStyle, int nDockBorderWidth, int nResizeLimitOsffset);
	void SetTitle(QString strWindowTitle, QString strTitleBar);

protected slots:
	void OnClose();
	void OnFloat();
	void OnMinMax();
	void OnHide();
	void OnDockLocationChanged(Qt::DockWidgetArea);
	void OnTopLevelChanged(bool);

protected:
	void UpdateButtonState(bool bHiddenChange);
	bool event(QEvent *event);
	QSize minimumSizeHint() const;
	QSize sizeHint () const;

private:

	//buttons and actions
	QAction* m_pActClose;
	QAction* m_pActFloat;
	QAction* m_pActMinMax;
	QAction* m_pActHide;
	QToolButton *m_pBtnClose;
	QToolButton *m_pBtnFloat;
	QToolButton *m_pBtnMinMax;
	QToolButton *m_pBtnHide;

	//flags
	QLabel*  m_pTitle;
	bool	 m_bMinimized;
	int		 m_nPrevHeight;
	bool	 m_bHidden;

	//style
	QString m_strTitleStyle; 
	QString m_strDockStyle; 
	QString m_strDockStyleHidden; 
	QString m_strToolButtonStyle;
	int		m_nDockBorderWidth;
	int		m_nResizeLimitOsffset; //minimum size of hidden dock widget

	DockTabBar *m_pTabForHiddenDocks;

};


class DockTabBar : public QTabBar
{
	Q_OBJECT
public:
	DockTabBar(QMainWindow *pMain,QTabBar *parent=0);
	~DockTabBar();

	void Create(QDockWidget* pDock);
	void Destroy();

	QSize sizeHint () const;

private slots:
	void OnExecuteAnimationShow();
	void OnExecuteAnimationHide();
	void OnAnimantionFinished();
	
protected:
	bool event(QEvent *event);
	void timerEvent(QTimerEvent * event);

private:
	bool IsCursorInsideTargetArea();
	
	QToolBar				*m_pToolBarContainer;
	QMap<int,QDockWidget *> m_lstDocks;
	QMap<int,QFrame *>		m_lstDockContainers;
	QMap<int,QRect>			m_lstPrevDockDimension;
	QMainWindow				*m_pMain;
	int						m_nSourceDirection;
	int						m_nTabIndex_Hide;
	int						m_nTabIndex_Show;
	QPropertyAnimation		*m_CurrAnimation;
	int						m_nTimerID;
	bool					m_bTestMouseMove;
	
};



class Resizer : public QWidget
{
	Q_OBJECT
public:

	#define RESIZER_WIDTH 6


	Resizer(int nSourceDirection, QRect rectLimits, QWidget *parent=0);
	~Resizer();

protected:
	void mouseMoveEvent(QMouseEvent *e);
	void mousePressEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);

private:

	bool			m_bPressed;
	QRect			m_OriginalParentGeometry;
	QRubberBand		*m_pRubberBand;
	int				m_nSourceDirection;
	QRect			m_rectLimits;
};

#endif
