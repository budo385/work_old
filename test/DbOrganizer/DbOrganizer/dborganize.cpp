#include "dborganize.h"

DbOrganize::DbOrganize(QString strConnectionName)
{
	DbConnectionSettings ConnSettings;

#if 1
	//Make MYSQL connection.
	ConnSettings.m_strDbType=DBTYPE_MYSQL;
	ConnSettings.m_strDbHostName="localhost";
	ConnSettings.m_strDbUserName="root";
	ConnSettings.m_strDbPassword="helix1";
	ConnSettings.m_strDbName=strConnectionName;
	//ConnSettings.m_strDbName="last_stable";
#endif
#if 0
	ConnSettings.m_strDbType=DBTYPE_MS_SQL;
	//ConnSettings.m_strDbHostName="HELIX-MR\SQLEXPRESS";
	ConnSettings.m_strDbUserName="korisnik";
	ConnSettings.m_strDbPassword="korisnik";
	ConnSettings.m_strDbName="test";
#endif
#if 0
	ConnSettings.m_strDbType=DBTYPE_FIREBIRD;
	ConnSettings.m_strDbHostName="";
	ConnSettings.m_strDbUserName="SYSDBA";
	ConnSettings.m_strDbPassword="masterkey";
	ConnSettings.m_strDbName="C:\\xx.fdb";
	ConnSettings.m_nDbPort=0;
#endif

	//Default directory.
	m_dirTableDataDir = QDir(QCoreApplication::applicationDirPath());
	
	//create SOKRATES_XP database:
	m_pDbManager = new DbSqlManager;

	m_pDbManager->Initialize(m_pStatus, ConnSettings,10);
	if (!m_pStatus.IsOK())
	{
		qDebug()<<m_pStatus.getErrorCode()<<"-"<<m_pStatus.getErrorText();
	}
		
}

void DbOrganize::Organize()
{
	
	qDebug() << "Start organizing tables!";
	DbOrganizer dbOrg(m_pDbManager);
	dbOrg.OrganizeDatabase(m_pStatus);
	if (!m_pStatus.IsOK())
		qDebug() << m_pStatus.getErrorText();//<<m_pStatus.getErrorDetails();
	else
		qDebug() << "Done organizing tables!";

}


void DbOrganize::FillData(QString strTable)
{
	
	qDebug() << "Start filling tables!";

	//Define tables to ignore on load.
	//m_lstIgnoreTableLoad << "CORE_ACCRSET" << "CORE_USER" << "BUS_ORGANIZATIONS";
	
	DbOrganizer dbOrg(m_pDbManager);

	//Make query.
	DbSqlQuery query(m_pStatus, m_pDbManager);

	//Get table file list.
	QStringList TableDataFileList;
	int tableCount = dbOrg.GetTableCreationList().count();
	for (int i = 0; i < tableCount; ++i)
		TableDataFileList << dbOrg.GetTableCreationList().value(i)->m_TableData.m_strTableName;

	//Remove tables to ignore on load.
	int removeCount = m_lstIgnoreTableLoad.count();
	for (int i = 0; i < removeCount; ++i)
		TableDataFileList.removeAll(m_lstIgnoreTableLoad.value(i));

	//Fill statement (valid only for MySql.
	QString strFillStatement = "load data infile '%1'	\
							   into table %2			\
							   fields terminated by ';'	\
							   enclosed by '''';";

	//Loop through list of files and try fill those tables.
	int TableDataFilesCount = TableDataFileList.count();
	for (int i = 0; i < TableDataFilesCount; ++i)
	{
		//Get table fill file name.
		QString TableDataFile = TableDataFileList.value(i);
		//Extract table name from file name.
		QString Table = TableDataFile;

		if(!strTable.isEmpty() && strTable==Table)
		{
			qDebug()<<"Loading table data:"<<Table;

			//Make full path to table fill file.
			TableDataFile = m_dirTableDataDir.absolutePath() + "/" + TableDataFile + ".txt";
			//Fill table with data.
			query.Execute(m_pStatus, strFillStatement.arg(TableDataFile, Table));
			if (!m_pStatus.IsOK())
				qDebug() << Table << m_pStatus.getErrorText();//<<m_pStatus.getErrorDetails();

			qDebug() << "Done filling tables!";
			return;
		}

		//if set jump:
		if(!strTable.isEmpty())
			continue;

		//Make full path to table fill file.
		TableDataFile = m_dirTableDataDir.absolutePath() + "/" + TableDataFile + ".txt";

		qDebug()<<"Loading table data:"<<Table;

		//Fill table with data.
		query.Execute(m_pStatus, strFillStatement.arg(TableDataFile, Table));
		if (!m_pStatus.IsOK())
			qDebug() << Table << m_pStatus.getErrorText();//<<m_pStatus.getErrorDetails();
	}
	
	qDebug() << "Done filling tables!";

	
}

void DbOrganize::UnloadData()
{
	
	qDebug() << "Tables unload started!";
	DbOrganizer dbOrg(m_pDbManager);
	
	//Get database tables name.
	QStringList TableDataFileList;
	int tableCount = dbOrg.GetTableCreationList().count();
	for (int i = 0; i < tableCount; ++i)
		TableDataFileList << dbOrg.GetTableCreationList().value(i)->m_TableData.m_strTableName;

	//Make query.
	DbSqlQuery query(m_pStatus, m_pDbManager);

	//Fill statement (valid only for MySql.
	QString strFillStatement = "SELECT *					\
								INTO OUTFILE '%1'			\
								FIELDS TERMINATED BY ';'	\
								ENCLOSED BY ''''			\
								FROM %2;";

	//Loop through list of tables and try to unload those tables.
	for (int i = 0; i < tableCount; ++i)
	{
		//Get table name and make full path to table fill file.
		QString TableDataFile = TableDataFileList.value(i);		
		QString Table = TableDataFile;
		TableDataFile = m_dirTableDataDir.absolutePath() + "/" + TableDataFile + ".txt";

		//Remove file before writing.
		QDir dir;
		if (dir.exists(TableDataFile))
			dir.remove(TableDataFile);

		//Fill table with data.
		query.Execute(m_pStatus, strFillStatement.arg(TableDataFile, Table));
		if (!m_pStatus.IsOK())
			qDebug() << Table << m_pStatus.getErrorText();//<<m_pStatus.getErrorDetails();
	}

	qDebug() << "Done unloading tables!";
	
}






