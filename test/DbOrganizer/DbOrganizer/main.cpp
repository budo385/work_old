#include <QtCore/QCoreApplication>
#include "dborganize.h"
#include <QStringList>




int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

	QStringList args = a.arguments();
	

	QString strConnectionName="test";
	if(args.size()>1)
	{
		strConnectionName=args.at(1);
	}
	else
	{
			qDebug()<<"--------------";
			qDebug()<<"Commands (dump/organize/fill) on 'test' db is default):";	
			qDebug()<<"1st param = database connection";
			qDebug()<<"2nd param = operation:";
			qDebug()<<"";
			qDebug()<<"1- LOAD Table(3d)";
			qDebug()<<"2- ONLY DUMP ";
			qDebug()<<"--------------";
	}
	DbOrganize organizer(strConnectionName);
	qDebug()<<"Using connection: "<<strConnectionName;


	//start program with two arguments: 
	//1st: conn name 
	//2nd
	//1- LOAD Table(3d)
	//2- ONLY DUMP

	if(args.size()>2)
	{
		switch(args.at(2).toInt())
		{
			case 1:
				if (args.size()==4)
					organizer.FillData(args.at(3));
					return 0;
				break;
			case 2:
				{
					organizer.UnloadData();
					return 0;
				}
				break;
			
		}
	}

	
	//Organize Db.
	organizer.Organize();
    qDebug() << "Done!";

	#if _DEBUG
		return a.exec();
	#else
		return 1;
	#endif
	
}
