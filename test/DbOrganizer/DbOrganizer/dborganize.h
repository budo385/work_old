#ifndef DBORGANIZE_H
#define DBORGANIZE_H

#include <QDir>
#include <QFile>

#include "common/common/status.h"

#include "db_actualize/db_actualize/dborganizer.h"
#include "db/db/dbconnsettings.h"
#include "db/db/dbsqlmanager.h"
#include "db/db/db_drivers.h"

class DbOrganize
{
public:
	DbOrganize(QString strConnectionName="test");

	void Organize();
	void FillData(QString strTable="");
	void UnloadData();

private:
	DbSqlManager	*m_pDbManager;
	Status			m_pStatus;
	QStringList		m_lstIgnoreTableLoad;
	QDir			m_dirTableDataDir;
};

#endif // DBORGANIZE_H
