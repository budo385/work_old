////   RAS Connect status function
////
////   Written by Gilad Novik
////   For any questions or comments, gilad@bmidas.com


#ifndef _RASSTATUS
#define _RASSTATUS
#include <ras.h>
#include <string>
using namespace std;


// We need to declare the functions type
typedef DWORD (WINAPI *RasEnumConnectionsType)(LPRASCONN lprasconn,LPDWORD lpcb,LPDWORD lpcConnections);
typedef DWORD (WINAPI *RasGetConnectStatusType)(HRASCONN hrasconn,LPRASCONNSTATUS lprasconnstatus);
typedef DWORD (WINAPI *FunctRasSetEntryProperties)(LPWSTR lpszPhoneBook, LPWSTR szEntry, LPRASENTRY lpbEntry, DWORD dwEntrySize, LPBYTE lpb, DWORD dwSize);
typedef DWORD (WINAPI *FunctRasGetEntryProperties)(LPWSTR lpszPhoneBook, LPWSTR szEntry, LPRASENTRY lpbEntry, LPDWORD lpdwEntrySize, LPBYTE lpb, LPDWORD lpdwSize);
typedef DWORD (WINAPI *FunctRasSetEntryDialParams)(LPWSTR lpszPhoneBook, LPRASDIALPARAMS lpRasDialParams, BOOL fRemovePassword);


#ifdef _UNICODE
#define RasFileName L"RASAPI32.DLL"
#define RasEnumConnectionsName L"RasEnumConnectionsW"
#define RasGetConnectStatusName L"RasGetConnectStatusW"
#define DfRasSetEntryProperties L"RasSetEntryPropertiesW"
#define DfRasGetEntryProperties L"RasGetEntryPropertiesW"
#define DfRasSetEntryDialParams L"RasSetEntryDialParamsW"
#else
#define RasFileName "RASAPI32.DLL"
#define RasEnumConnectionsName "RasEnumConnectionsA"
#define RasGetConnectStatusName "RasGetConnectStatusA"
#define DfRasSetEntryProperties "RasSetEntryPropertiesA"
#define DfRasGetEntryProperties "RasGetEntryPropertiesA"
#define DfRasSetEntryDialParams "RasSetEntryDialParamsA"

#endif

BOOL CreateRasEntry(string strEntryName,HINSTANCE hLib)
{
	FunctRasSetEntryProperties _RasSetEntryProperties = (FunctRasSetEntryProperties)GetProcAddress(hLib,DfRasSetEntryProperties);
	FunctRasGetEntryProperties _RasGetEntryProperties = (FunctRasGetEntryProperties)GetProcAddress(hLib,DfRasGetEntryProperties);

	LPRASENTRY lpRasEntry = NULL;
	DWORD cb = sizeof(RASENTRY);
	DWORD dwRet;
	DWORD dwBufferSize = 0;

	// This is important! Find the buffer size (different from sizeof(RASENTRY)).
	(*_RasGetEntryProperties)(NULL, (LPWSTR)"", NULL, &dwBufferSize, NULL, NULL);
	if(dwBufferSize == 0)
		return FALSE;

	lpRasEntry = (LPRASENTRY)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwBufferSize);
	if (lpRasEntry == NULL)
		return FALSE;

	// Options
	lpRasEntry->dwfOptions = RASEO_PreviewUserPw | RASEO_ShowDialingProgress | RASEO_Custom  | RASEO_RequirePAP | RASEO_PreviewDomain;
	lpRasEntry->dwfOptions=lpRasEntry->dwfOptions & ~RASEO_RequireSPAP;
	lpRasEntry->dwfOptions=lpRasEntry->dwfOptions & ~RASEO_RequireEAP;

	lpRasEntry->dwSize = dwBufferSize;
	lpRasEntry->dwType=RASET_Broadband;
	strcpy_s(lpRasEntry->szDeviceType, RASDT_PPPoE);
	strcpy_s(lpRasEntry->szDeviceName, (LPCSTR)"WAN Miniport (PPPOE)");


	dwRet = (*_RasSetEntryProperties)(NULL, (LPWSTR) strEntryName.c_str(), lpRasEntry, dwBufferSize, NULL, 0);

	HeapFree(GetProcessHeap(), 0, (LPVOID)lpRasEntry);

	if(dwRet == 0)
		return TRUE;
	else
		return FALSE;
}


// Saves the user info (username, password)
// Set bRemovePassword to TRUE if you don't want the password to be saved.
BOOL SetEntryDialParams(string strEntryName, string strUsername, string strPassword,HINSTANCE hLib)
{
	FunctRasSetEntryDialParams _RasSetEntryDialParams = (FunctRasSetEntryDialParams)GetProcAddress(hLib,DfRasSetEntryDialParams);

	RASDIALPARAMS rdParams;
	ZeroMemory(&rdParams, sizeof(RASDIALPARAMS));
	rdParams.dwSize = sizeof(RASDIALPARAMS);
	strcpy_s(rdParams.szEntryName, strEntryName.c_str());
	strcpy_s(rdParams.szUserName, strUsername.c_str());
	strcpy_s(rdParams.szPassword, strPassword.c_str());

	DWORD dwRet = (*_RasSetEntryDialParams)(NULL, &rdParams, FALSE);

	if(dwRet == 0)
		return TRUE;
	else 
		return FALSE;
}

BOOL CreateRASConnection(string strEntryName, string strUserName, string strPassword)
{
	HINSTANCE hLib = LoadLibrary(RasFileName);	// Try to load the library
	if (hLib == NULL)
		return FALSE;	// Return FALSE if we can't find it


	BOOL bCreateOK=FALSE;
	

	if(CreateRasEntry(strEntryName,hLib) == TRUE)
	{
		if(SetEntryDialParams(strEntryName, strUserName, strPassword,hLib) == TRUE)
			bCreateOK=TRUE;
	}
		

	FreeLibrary(hLib);	// Don't forget to unload the library from memory
	return bCreateOK;
}





#endif // _RASSTATUS


