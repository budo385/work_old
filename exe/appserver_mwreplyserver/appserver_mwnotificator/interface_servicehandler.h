#ifndef INTERFACE_SERVICEHANDLER_H
#define INTERFACE_SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_ServiceHandler
{
public:

	virtual ~Interface_ServiceHandler(){};
	virtual void CheckNewEmail(Status &pStatus, QString strEmail,bool &Ret_bNewMail)=0; 
	virtual void SetNewEmailStatus(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &lstNewEmailStatus)=0; 
};


/*
<Web_service_meta_data>

	<CheckNewEmail>
		<URL>/service/checknewemail</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</CheckNewEmail>
	
	<SetNewEmailStatus>
		<URL>/service/setnewemailstatus</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<lstNewEmailStatus>
		<DEFINE_LIST>
			lstNewEmailStatus.addColumn(QVariant::String,"EMAIL");
		</DEFINE_LIST>
		</lstNewEmailStatus>
	</SetNewEmailStatus>

	
</Web_service_meta_data>
*/

#endif // INTERFACE_SERVICEHANDLER_H

	
	
	