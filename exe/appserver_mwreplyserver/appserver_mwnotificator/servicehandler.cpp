#include "servicehandler.h"
#include "common/common/authenticator.h"
#include "common/common/sha2.h"

#include "common/common/logger.h"
extern Logger g_Logger;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;


#define SECRET_SPICE_PASSWORD "BANDIT"
#define STAT_USERNAME "mcadmin"
#define STAT_PASSWORD "pachelbel"


//client checks this every 1 minute
void ServiceHandler::CheckNewEmail(Status &pStatus, QString strEmail,bool &Ret_bNewMail) 
{
	strEmail=strEmail.toLower();
	QWriteLocker lock(&m_Lock);
	int nIdx=m_lstUsersWithNewEmail.indexOf(strEmail);
	if (nIdx>=0)
	{
		Ret_bNewMail=true;
		m_lstUsersWithNewEmail.removeAt(nIdx);
	}
	else
	{
		Ret_bNewMail=false;
	}
}

//mwreply calls this when new email arrives, nSumOfUsersWithNewMail is check point after installation
//if bNewMail=false, then delete from list
void ServiceHandler::SetNewEmailStatus(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &lstNewEmailStatus) 
{
	//authenticate user
	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

	int nSize=lstNewEmailStatus.getRowCount();
	if (nSize==0)
		return;

	QWriteLocker lock(&m_Lock);
	
	for (int i=0;i<nSize;i++)
	{
		if (!m_lstUsersWithNewEmail.contains(lstNewEmailStatus.getDataRef(i,0).toString()))
			m_lstUsersWithNewEmail.append(lstNewEmailStatus.getDataRef(i,0).toString());
	}

	nSize=m_lstUsersWithNewEmail.size();
	for (int i=0;i<nSize;i++)
	{
		qDebug()<<"New emails are alive for these users: "<<m_lstUsersWithNewEmail.at(i);
	}


}
