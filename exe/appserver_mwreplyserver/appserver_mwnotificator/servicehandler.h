#ifndef SERVICEHANDLER_H
#define SERVICEHANDLER_H

#include <QStringList>
#include <QReadWriteLock>

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "interface_servicehandler.h"

class ServiceHandler: public Interface_ServiceHandler
{
public:

	//public API over REST
	void CheckNewEmail(Status &pStatus, QString strEmail,bool &Ret_bNewMail); 
	void SetNewEmailStatus(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &lstNewEmailStatus); 

private:

	QReadWriteLock			m_Lock;
	QStringList				m_lstUsersWithNewEmail;
};

#endif // SERVICEHANDLER_H

/*
TODO: 
- skeleton for REST and incoming Dbrecordset->automatically
- authentication
- 


https://localhost:9999/rest/service/checknewemail
<REQUEST><PARAMETERS><strEmail>trumbic@helix-business-soft.hr</strEmail></PARAMETERS></REQUEST>

https://localhost:9999/rest/service/setnewemailstatus
<REQUEST><PARAMETERS><strConce></strConce><strAuthToken></strAuthToken>
<lstNewEmailStatus><row><EMAIL>sime@sime.hr</EMAIL></row></lstNewEmailStatus></PARAMETERS></REQUEST>


*/