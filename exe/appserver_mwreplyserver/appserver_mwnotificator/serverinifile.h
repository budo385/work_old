#ifndef SERVERINIFILE_H
#define SERVERINIFILE_H

#include "common/common/inifile.h"
#include <QFileInfo>



/*!
	\class ServerIniFile
	\brief INI file reader for app. server
	\ingroup AppServer

	Application server INI file reader


*/
class ServerIniFile
{
public:
	ServerIniFile();
	~ServerIniFile();

	
	void Clear();
	bool Load(QString strFile, bool bCreateIfNotExists=true);
	bool Save(QString strFile);

public:

	//data stored in the INI file
	QString m_strAppServerIPAddress;	// IP address bind (def is 0.0.0.0)
	int		m_nAppServerPort;			// port number
	int		m_nMaxConnections;			// maximum connections that app. server will accept
	QString	m_strServiceDescriptor;		// app name
	int		m_nAutoStartService;		//
	QString m_strDBConnectionName;		// NAME OF DB CONN
	QString m_strHTTPRegisterAddress;	// start of HTTP confirmation url: http://everxconnect.com:4567/register

	//SSL
	int		m_nSSLMode;					// accept only SSL(https) connections
	QString m_strSSLCertificateDir;		// path to the SSL certificates files (server.cert & server.pkey)

	//LOGGER
	int m_nLogLevel;					
	int m_nLogMaxSize;					
	int m_nLogBufferSize;				
		

protected:
	IniFile m_objIni;
	bool CheckValues();
	bool SaveDefaults(QString strFile);

};

#endif //SERVERINIFILE_H