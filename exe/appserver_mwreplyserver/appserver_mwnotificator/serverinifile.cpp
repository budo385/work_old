#include "serverinifile.h"
#include "common/common/logger.h"

ServerIniFile::ServerIniFile()
{
}

ServerIniFile::~ServerIniFile()
{
}

void ServerIniFile::Clear()
{
	//m_lstConnections.clear();
}

//loads, creates if not exists, cheks values
bool ServerIniFile::Load(QString strFile, bool bCreateIfNotExists)
{
	Clear();

	if(!QFileInfo(strFile).exists()&& bCreateIfNotExists)
		{return SaveDefaults(strFile);}

	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	// read INI values
	m_objIni.GetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.GetValue("Main", "AppServerPort",		m_nAppServerPort, 11111);
	m_objIni.GetValue("Main", "MaxConnections",		m_nMaxConnections, 100);
	m_objIni.GetValue("Main", "Service Descriptor",	m_strServiceDescriptor);
	m_objIni.GetValue("Main", "AutoStartService",	m_nAutoStartService,1);
	m_objIni.GetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.GetValue("Main", "HTTPRegisterAddress",	m_strHTTPRegisterAddress,"http://everxconnect.com/registration");
	
	m_objIni.GetValue("SSL", "SSLMode",			m_nSSLMode, 0);
	m_objIni.GetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);

	m_objIni.GetValue("Logging", "Log Level",												m_nLogLevel,Logger::LOG_LEVEL_BASIC);
	m_objIni.GetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize,10);
	m_objIni.GetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize,64);
	
	return CheckValues();
}

bool ServerIniFile::Save(QString strFile)
{
	m_objIni.SetPath(strFile);

	//fill the data inside
	m_objIni.RemoveSection("Main");		//cleanup existing data
	m_objIni.RemoveSection("Logging");	
	//m_objIni.RemoveSection("Backup");	
	m_objIni.RemoveSection("SSL");		

	// write INI values
	m_objIni.SetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.SetValue("Main", "AppServerPort",		m_nAppServerPort);
	m_objIni.SetValue("Main", "MaxConnections",		m_nMaxConnections);
	m_objIni.SetValue("Main", "Service Descriptor",	m_strServiceDescriptor);
	m_objIni.SetValue("Main", "AutoStartService",	m_nAutoStartService);
	m_objIni.SetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.SetValue("Main", "HTTPRegisterAddress",	m_strHTTPRegisterAddress);

	m_objIni.SetValue("SSL", "SSLMode",				m_nSSLMode);
	m_objIni.SetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);

	m_objIni.SetValue("Logging", "Log Level",												m_nLogLevel);
	m_objIni.SetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize);
	m_objIni.SetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize);

	return m_objIni.Save();
}



bool ServerIniFile::CheckValues()
{
	if(m_nMaxConnections<=0) return false;
	if(m_nSSLMode<0 || m_nSSLMode>1) return false;
	if(m_nAutoStartService<0 || m_nAutoStartService>1) return false;
	if(m_nAppServerPort<0 || m_nAppServerPort>65536) return false;

	if(m_nLogBufferSize<0) return false;

	//reset to default log level:
	if(m_nLogLevel<0)m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	if(m_nLogLevel>Logger::LOG_LEVEL_DEBUG)m_nLogLevel=Logger::LOG_LEVEL_DEBUG;

	return true;
}



bool ServerIniFile::SaveDefaults(QString strFile)
{
	m_strAppServerIPAddress="0.0.0.0";
	m_nAppServerPort=11111;
	m_nMaxConnections=1000;
	m_nSSLMode=0;
	m_nAutoStartService=1;

	m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	m_nLogMaxSize=10;				
	m_nLogBufferSize=64;	


	return Save(strFile);
}
