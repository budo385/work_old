#include "restrpcskeleton_service.h"
#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;

RestRpcSkeleton_Service::RestRpcSkeleton_Service(int RPCType):
RpcSkeleton(RPCType)
{
	SetNameSpace("service"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("POST/service/checknewemail",&RestRpcSkeleton_Service::CheckNewEmail);
	mFunctList.insert("POST/service/setnewemailstatus",&RestRpcSkeleton_Service::SetNewEmailStatus);
}

bool RestRpcSkeleton_Service::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;

	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) 
		return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);

		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);

			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

return true;
}

void RestRpcSkeleton_Service::CheckNewEmail(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strEmail;
	bool  Ret_bNewMail;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strEmail,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->CheckNewEmail(err,strEmail,Ret_bNewMail);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_bNewMail,"Ret_bNewMail");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::SetNewEmailStatus(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strConce;
	QString  strAuthToken;
	DbRecordSet  lstNewEmailStatus;
	
			lstNewEmailStatus.addColumn(QVariant::String,"EMAIL");
		

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strConce,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strAuthToken,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&lstNewEmailStatus,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->SetNewEmailStatus(err,strConce,strAuthToken,lstNewEmailStatus);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

