#include "DbTableDef.h"
#include "common/common/entity_id_collection.h"

bool DbTableDef::bInitialized=false;
QList<DbView> DbTableDef::s_lstDbViews;
bool DbTableDef::s_bAllLongVarCharFieldsInitialized=false;
QStringList DbTableDef::s_lstAllLongVarCharFields;


//in this function insert own views. NOTE: Every table must have at least one FULL view (all cols)
void DbTableDef::Initialize()
{
	if (bInitialized)	return;
	bInitialized=true;


	//-----------------------
	// VIEW_MAIN_DATA
	//-----------------------

	//add new sample view
	DbView view;
	view.m_nViewID		= VIEW_MAIN_DATA;
	view.m_strTables="MAIN_DATA"; //if more then one table separate by comma "TABLE,TABLE2"
	
	//Record set definition
	view.append(DbColumnEx("MD_ID", QVariant::Int));
	view.append(DbColumnEx("MD_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx("MD_DATABASE_VERSION" ,QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// VIEW_CS_HOSTS
	//-----------------------
	view.m_nViewID		= VIEW_CS_HOSTS;
	view.m_strTables="CS_HOSTS"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx( "CSH_ID",  QVariant::Int));
	view.append(DbColumnEx( "CSH_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx( "CSH_HOST_ADDRESS", QVariant::String));
	view.append(DbColumnEx( "CSH_TYPE", QVariant::Int));
	view.append(DbColumnEx( "CSH_STATUS", QVariant::Int));
	view.append(DbColumnEx( "CSH_EMAIL_ACCOUNTS",  QVariant::Int));
	view.append(DbColumnEx( "CSH_LAST_SYNC_TIME",  QVariant::DateTime));
	
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// VIEW_EMAILS
	//-----------------------
	view.m_nViewID		= VIEW_CS_EMAILS;
	view.m_strTables="CS_EMAILS"; 
	view.append(DbColumnEx( "CSE_ID",  QVariant::Int));
	view.append(DbColumnEx( "CSE_DAT_LAST_MODIFIED",  QVariant::DateTime));
	view.append(DbColumnEx( "CSE_EMAIL",  QVariant::String));
	view.append(DbColumnEx( "CSE_HOST_ID", QVariant::Int));
	
	s_lstDbViews.append(view);
	view.clear();


}

//-----------------------------------------------------
//				PUBLIC MEMEBERS
//-----------------------------------------------------



/*!
	Returns Recordset defintion view for given view ID
	\param  nViewID		- view id
	\return DbView		- view def
*/
DbView& DbTableDef::getView(int nViewID)
{
	Initialize();
	//if(!m_bInitialised)Initialize();		//automatic initialization
	return s_lstDbViews[findView(nViewID)]; //return view
}

/*!
	Returns user defined SQL view or generates SQL view based on recordset defintion
	\param  nViewID		- view id
	\return SQL			- SQL SELECT STATEMENT in format SELECT X1,X1 FROM TABLE
*/
QString DbTableDef::getSQLView(int nViewID, bool bDistinct /*= false*/)
{
	Initialize();
	QString strSql; 

	//find view, if SQL defined return
	int nIdx = findView(nViewID);
	strSql=s_lstDbViews[nIdx].m_strSelectSQL;
	if(!strSql.isEmpty()) return strSql;

	//if not defined, generate automatic, store back to list :)
	DbView RecordSetView = s_lstDbViews[nIdx];

	//loop through
	if (bDistinct)
		strSql="SELECT DISTINCT " ;
	else
		strSql="SELECT " ;


	
	int RecordSetViewSize = RecordSetView.size()-RecordSetView.m_nSkipLastCols;
	for(int i=0; i < RecordSetViewSize ;++i)
	{
		strSql+=RecordSetView.at(i).m_strName+",";
		//if(i!=RecordSetViewSize-1)strSql+=",";//add comma between
	}
	strSql.chop(1); //remove comma at end
	strSql+=" FROM "  + RecordSetView.m_strTables;

	//store back to skip init again:
	s_lstDbViews[nIdx].m_strSelectSQL=strSql;
	return strSql;
}

/*!
	Returns user defined SQL INSERT or generates SQL INSERT based on recordset defintion.
	Takes mapping into account if negative, or non exist, else, user must manually create sql
	\param  nViewID		- view id
	\return SQL			- SQL INSERT STATEMENT in format INSERT INTO XX (X1,X1) VALUES(?,?)
*/
QString DbTableDef::getSQLInsert(int nViewID,QSet<int> *pLstIsLongVarcharField)
{
	Initialize();

	QString strSql; 
	int nStart;

	//find view, if SQL definded return
	int nIdx = findView(nViewID);
	strSql=s_lstDbViews[nIdx].m_strInsertSQL;
	if(!strSql.isEmpty()) 
	{
		if (pLstIsLongVarcharField)
			*pLstIsLongVarcharField=s_lstDbViews[nIdx].m_LstIsLongVarcharField;
		return strSql;
	}

	//if not definded, generate automatic, store back to list :)
	DbView RecordSetView = s_lstDbViews[nIdx];

	//if map.size=1 and neg take it:
	nStart=s_lstDbViews[nIdx].m_nSkipFirstColsInsert;

	//clear
	QSet<int> lstIsLongVarcharField;

	//loop through
	strSql="INSERT INTO "  + RecordSetView.m_strTables+" ( ";
	int RecordSetViewSize = RecordSetView.size()-s_lstDbViews[nIdx].m_nSkipLastColsWrite;
	for(int i=nStart; i < RecordSetViewSize; ++i)
	{
		strSql+=RecordSetView.at(i).m_strName+",";

		//if (pLstIsLongVarcharField) //BT added for testing if longvarchar, for FB conversion from char to blob
		if (RecordSetView.at(i).m_bIsLongVarChar)
				lstIsLongVarcharField.insert(i);
	}
	strSql.chop(1); //remove comma at end
	strSql+=") VALUES (";
	for(int i=nStart; i < RecordSetViewSize; ++i)
	{
		strSql+="?,";
		//if(i!=RecordSetViewSize-1)strSql+=",";//add comma between
	}
	strSql.chop(1); //remove comma at end
	strSql+=") ";

	//store back to skip init again:
	s_lstDbViews[nIdx].m_strInsertSQL=strSql;
	s_lstDbViews[nIdx].m_LstIsLongVarcharField=lstIsLongVarcharField;
	if (pLstIsLongVarcharField)
		*pLstIsLongVarcharField=lstIsLongVarcharField;
	return strSql;
}

//ignore skip cols, same as insert-> for full data copy
QString DbTableDef::getFullSQLInsert(int nViewID,QSet<int> *pLstIsLongVarcharField)
{
	Initialize();
	QString strSql; 
	int nStart;

	//find view
	int nIdx = findView(nViewID);

	//if not definded, generate automatic, store back to list :)
	DbView RecordSetView = s_lstDbViews[nIdx];

	//if map.size=1 and neg take it:
	nStart=0;
	int RecordSetViewSize = RecordSetView.size();

	//loop through
	strSql="INSERT INTO "  + RecordSetView.m_strTables+" ( ";
	
	for(int i=nStart; i < RecordSetViewSize; ++i)
	{
		strSql+=RecordSetView.at(i).m_strName+",";

		if (pLstIsLongVarcharField) //BT added for testing if longvarchar, for FB conversion from char to blob
			if (RecordSetView.at(i).m_bIsLongVarChar)
					pLstIsLongVarcharField->insert(i);
	}
	strSql.chop(1); //remove comma at end

	strSql+=") VALUES (";
	for(int i=nStart; i < RecordSetViewSize; ++i)
	{
		strSql+="?,";
	}
	strSql.chop(1); //remove comma at end
	strSql+=") ";

	return strSql;
}

/*!
	Returns user defined SQL UPDATE or generates SQL UPDATE based on recordset defintion.
	Takes mapping into account if negative, or non exist, else, user must manually create sql
	\param  nViewID		- view id
	\return SQL			- SQL UPDATE STATEMENT in format UPDATE XX SET X1=?,X2=? (no where clause)
*/
QString DbTableDef::getSQLUpdate(int nViewID,QSet<int> *pLstIsLongVarcharField)
{
	Initialize();

	QString strSql; 
	int nStart;

	//find view, if SQL defined return
	int nIdx = findView(nViewID);
	strSql=s_lstDbViews[nIdx].m_strUpdateSQL;
	if(!strSql.isEmpty()) 
	{
		if (pLstIsLongVarcharField)
			*pLstIsLongVarcharField=s_lstDbViews[nIdx].m_LstIsLongVarcharField;
		return strSql;
	}

	//if not definded, generate automatic, store back to list :)
	DbView RecordSetView = s_lstDbViews[nIdx];

	//if map.size=1 and neg take it:
	nStart=s_lstDbViews[nIdx].m_nSkipFirstColsUpdate;

	//clear
	QSet<int> lstIsLongVarcharField;

	//loop through
	strSql="UPDATE "  + RecordSetView.m_strTables+" SET ";
	int RecordSetViewSize = RecordSetView.size()-s_lstDbViews[nIdx].m_nSkipLastColsWrite;
	for(int i=nStart;i<RecordSetViewSize;++i)
	{
		strSql+=RecordSetView.at(i).m_strName+" = ?,";
		//if (pLstIsLongVarcharField) //BT added for testing if longvarchar, for FB conversion from char to blob
		if (RecordSetView.at(i).m_bIsLongVarChar)
			lstIsLongVarcharField.insert(i);
	}
	strSql.chop(1); //remove comma at end

	

	//store back to skip init again:
	s_lstDbViews[nIdx].m_strUpdateSQL=strSql;
	s_lstDbViews[nIdx].m_LstIsLongVarcharField=lstIsLongVarcharField;
	if (pLstIsLongVarcharField)
		*pLstIsLongVarcharField=lstIsLongVarcharField;
	return strSql;
}

/*!
	Returns list of mapping (for SQL insert) between position in RecordSet View and SQL statemnt.
	Mapping can be define with view or it can be left default emopty in which case mapping is 1:1
	(from first to last column)
	if Mapping contains -1 or -2 or other negative values, it means that mapping list will be generated
	but first column will be skipped (-1) or first two (-2), and so on

	\param  nViewID		- view ID
	\return list		- Map list
*/
int DbTableDef::getInsertListMapping(int nViewID)
{
	Initialize();

	Q_ASSERT_X(findView(nViewID)>=0, "return of insert list", "Unknown ViewID");
	return s_lstDbViews[findView(nViewID)].m_nSkipFirstColsInsert;
}

/*!
	Returns list of mapping (for SQL update) between position in RecordSet View and SQL statemnt.
	Mapping can be define with view or it can be left default emopty in which case mapping is 1:1
	(from first to last column)
	if Mapping contains -1 or -2 or other negative values, it means that mapping list will be generated
	but first column will be skipped (-1) or first two (-2), and so on

	\param  nViewID		- view ID
	\return list		- Map list
*/

int DbTableDef::getUpdateListMapping(int nViewID)
{
	Initialize();

	Q_ASSERT_X(findView(nViewID)>=0, "return of update list", "Unknown ViewID");
	return s_lstDbViews[findView(nViewID)].m_nSkipFirstColsUpdate;
}

/* private: find that view */
int DbTableDef::findView(int nViewID)
{
	Initialize();

	int DbViewSize = s_lstDbViews.size();
	for(int i=0; i < DbViewSize; i++)
		if( s_lstDbViews[i].m_nViewID == nViewID)
			return i;

	Q_ASSERT_X(false,"VIEW","VIEW DOES NOT EXIST, ABORT ABORT");
	return -1;
}




/*!
	In debug mode, call this to check if there are view duplicates
*/
bool DbTableDef::CheckUniqueView()
{

	Initialize();

	int nSize=s_lstDbViews.size();
	for(int i=0;i<nSize;++i)
	{
		int nOccur=0;
		int nViewID=s_lstDbViews.at(i).m_nViewID;
		for(int j=0;j<nSize;++j)
		{
			if(nViewID==s_lstDbViews.at(j).m_nViewID)
				nOccur++;
		}
		if(nOccur>1)
		{
			Q_ASSERT(false);
			return false;
		}
	}
	return true;
}



//returns columns from view, comma separated:
QString DbTableDef::getSQLColumnsFromView(DbView &RecordSetView)
{
	//if not defined, generate automatic, store back to list :)
	QString strSql;

	int RecordSetViewSize = RecordSetView.size()-RecordSetView.m_nSkipLastCols;
	for(int i=0; i < RecordSetViewSize ;++i)
	{
		strSql+=RecordSetView.at(i).m_strName;
		strSql+=",";//add comma between
	}

	strSql.chop(1);
	//store back to skip init again:
	return strSql;
}


QStringList* DbTableDef::getAllLongVarCharFields()
{
	if(s_bAllLongVarCharFieldsInitialized)
		return &s_lstAllLongVarCharFields;

	Initialize();

	int nSize=s_lstDbViews.size();
	for(int i=0;i<nSize;++i)
	{
		DbView view=s_lstDbViews.at(i);
		int nColCnt=view.size();
		for(int j=0;j<nColCnt;++j)
		{
			if (view.at(j).m_bIsLongVarChar)
				s_lstAllLongVarCharFields<<view.at(j).m_strName;
		}
	}

	s_bAllLongVarCharFieldsInitialized=true;
	return &s_lstAllLongVarCharFields;
}

