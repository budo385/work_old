
function mc_ReadHosts(callback) {
	
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/readhosts";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}

function cbMC_ReadHosts(xml){
	checkErrorCode();
	
	var xmlobj = $(xml);
	var node = null;

	var CSH_ID				=null;
	var CSH_HOST_ADDRESS	=null;
	var CSH_TYPE			=null;
	var CSH_STATUS			=null;
	var CSH_EMAIL_ACCOUNTS	=null;
	
	var dataList = [];
	var Ret_lstHosts = xmlobj.find("Ret_lstHosts").text();
	
	xmlobj.find('Ret_lstHosts > row').each(function(index, value) 
	{  
		node = $(this);
		CSH_ID = node.find('CSH_ID').text();
		CSH_HOST_ADDRESS = xmlDecode(node.find('CSH_HOST_ADDRESS').text());
		CSH_TYPE = node.find('CSH_TYPE').text();
		CSH_STATUS = node.find('CSH_STATUS').text();
		CSH_EMAIL_ACCOUNTS = node.find('CSH_EMAIL_ACCOUNTS').text();
		
		dataList.push({
			id: CSH_ID,
			url: CSH_HOST_ADDRESS,
			type: CSH_TYPE,
			status: CSH_STATUS,
			status_txt: getHostStatus(CSH_STATUS),
			cnt_acc: CSH_EMAIL_ACCOUNTS
		});
	});
	
	return(dataList);
}
//if ADD then nHostID=-1 else set right nHostID
function mc_WriteHost(callback,nHostID, strHostURL, nType, nStatus) 
{
	var conce = getConce();
	var auth = getAuth(conce);
	
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
				  "<nHostID>"+nHostID+"</nHostID><strHostURL>"+strHostURL+"</strHostURL><nType>"+nType+"</nType><nStatus>"+nStatus+"</nStatus></PARAMETERS></REQUEST>";
	
	var strApiUrl = "/service/writehost";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_WriteHost(xml)
{
	checkErrorCode();
}

function mc_DeleteHost(callback,nHostID){
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><nHostID>"+nHostID+"</nHostID></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/deletehost";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_DeleteHost(xml){
	checkErrorCode();
}

function mc_LockHost(callback,nHostID, nLockReadOnly){
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><nHostID>"+nHostID+"</nHostID><nLockReadOnly>"+nLockReadOnly+"</nLockReadOnly></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/lockhost";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_LockHost(xml){
	checkErrorCode();
}

function mc_ReadEmails(callback,nHostID) {
	
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><nHostID>"+nHostID+"</nHostID></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/reademails";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}

function cbMC_ReadEmails(xml){
	checkErrorCode();
	
	var xmlobj = $(xml);
	var node = null;

	var CSE_EMAIL			=null;
	var CSH_HOST_ADDRESS	=null;
	var CSH_TYPE			=null;
	var CSH_STATUS			=null;
	var CSH_EMAIL_ACCOUNTS	=null;
	
	var dataList = [];
	var Ret_lstEmails = xmlobj.find("Ret_lstEmails").text();
	
	xmlobj.find('Ret_lstEmails > row').each(function(index, value) 
	{  
		node = $(this);
		CSE_EMAIL = xmlDecode(node.find('CSE_EMAIL').text());
		
		dataList.push({
			email: CSE_EMAIL
		});
	});
	
	return(dataList);
}


function mc_AddEmail(callback, strEmail, nHostID){
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><strEmail>"+strEmail+"</strEmail><nHostID>"+nHostID+"</nHostID></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/addemailtocs";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_AddEmail(xml){
	checkErrorCode();
}

function mc_RemoveEmail(callback, strEmail, nHostID){
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><strEmail>"+strEmail+"</strEmail><nHostID>"+nHostID+"</nHostID></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/removeemailtocs";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_RemoveEmail(xml){
	checkErrorCode();
}


function getConce()
{
	return(SHA256(new Date().getTime().toString()));	
}
function getAuth(strConce) // note: different from clientlib
{	
	var SECRET_SPICE_PASSWORD="BANDIT";
	var strUsername = sessionStorage.strUser;
	var strPassword = sessionStorage.strPass;
	
	return(SHA256(strUsername+strConce+SECRET_SPICE_PASSWORD+strPassword));
}

function getHostStatus(nStatus)
{
	nStatus =parseInt(nStatus);
	
	var strStatus = "";
	switch(nStatus)
	{
	case 0: strStatus = "OK"; 
		break;
	case 1:	strStatus = "LOCKED"; 
		break;
	case 2: strStatus = "DOWN"; 
		break;
	default:
	  break;
	}	
	return strStatus;
}

function checkErrorCode()
{
  if(mc_nLastBO_ErrorCode!=0) // error occured
  {									 			
	if(mc_nLastBO_ErrorCode==1000){ 			
		alert("[login error]:"+mc_strLastBO_ErrorText);  // login error
		
		sessionStorage.clear();
		//localStorage.clear(); PM: currently don't delete local storage, it holds the username only
		document.location="index.html";
	}
	else {
		 alert("[error]:"+mc_strLastBO_ErrorText);  // alert this general error
	}
	return;
  }	
}




