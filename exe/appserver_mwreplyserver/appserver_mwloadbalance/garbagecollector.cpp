#include "garbagecollector.h"
#include "common/common/threadid.h"

#include "common/common/logger.h"
extern Logger g_Logger;

#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;

GarbageCollector::GarbageCollector(HTTPServer *pHttpServer)
{
	m_HttpServer=pHttpServer;
}

GarbageCollector::~GarbageCollector()
{

}


//start every X minutes
void GarbageCollector::ExecuteTask(int nCallerID,int nTaskID, QString strData)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
	Status err;

	/* B.T. as of new version connector does not download new emails from me.meilagenat.com
	g_ServiceHandler->CleanUpData(err);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,err.getErrorCode(),"Error while cleaning expired emails: " + err.getErrorText());
	}
	*/

	g_ServiceHandler->CheckHosts(err);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,err.getErrorCode(),"GarbageCollector: error while checking host: " + err.getErrorText());
	}
	

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Garbage Collector Ended");
}


