#include "servicehandler.h"
#include "common/common/authenticator.h"
#include "trans/trans/smtpclient.h"
#include "dbtabledef.h"
#include "common/common/sha2.h"
#include "common/common/datahelper.h"
#include "trans/trans/resthttpclient.h"
#include "trans/trans/httpreadersync.h"

#include "common/common/logger.h"
extern Logger g_Logger;
#include "db/db/dbsqlmanager.h"
extern DbSqlManager	*g_DbManager;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;


#define SECRET_SPICE_PASSWORD "BANDIT"
#define STAT_USERNAME "mcadmin"
#define STAT_PASSWORD "pachelbel"

#define SERVER_INTERSERVER_COMM_USERNAME "system"
#define SERVER_INTERSERVER_COMM_PASSWORD "balrog10"

#define MW_LB_USERNAME "mwlb_admin"
#define MW_LB_PASSWORD "ronin10"

#define SERVER_STATUS_OK 0
#define SERVER_STATUS_LOCKED 1
#define SERVER_STATUS_DOWN 2

#define TURN_OFF_AUTHENTICATION 0
#define TURN_OFF_HOST_CHECK 0



void ServiceHandler::LookUpHost(Status &pStatus, QString &Ret_strHostAddress, int &Ret_nServerStatus, QString strEmail, int nType )
{
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	strEmail=strEmail.toLower();
	Ret_nServerStatus=SERVER_STATUS_OK;

	QString strSql;

	//check if email exists
	query.Execute(pStatus,"SELECT CSH_HOST_ADDRESS, CSH_STATUS FROM CS_EMAILS INNER JOIN CS_HOSTS ON CSE_HOST_ID=CSH_ID WHERE CSE_EMAIL = '"+strEmail+"' AND CSH_TYPE="+QString::number(nType));
	if (!pStatus.IsOK())return; 

	int nHostID=0;


	if (query.next())
	{
		//---------------------------------------------
		//		old client is incoming, just return address
		//---------------------------------------------

		Ret_strHostAddress = query.value(0).toString();
		Ret_nServerStatus = query.value(1).toInt();

		if (Ret_nServerStatus!=SERVER_STATUS_OK)
		{
			if (Ret_nServerStatus==SERVER_STATUS_DOWN)
			{
				pStatus.setError(1,"Load balance server: Mailegant Writer server at which your email is registered is currently down. Please try again later!");
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("LookUpHost: server %1 is down, client request denied").arg(Ret_strHostAddress));
				return;
			}
			else
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("LookUpHost: server %1 is locked, client is redirected to the server!").arg(Ret_strHostAddress));
			}
		}

		return;
	}
	else
	{
		//---------------------------------------------
		//		new client is incoming
		//---------------------------------------------

		//email is not registered, please register now, find host with min. accounts:
		query.Execute(pStatus,"SELECT FIRST 1 CSH_ID, CSH_HOST_ADDRESS FROM CS_HOSTS WHERE CSH_STATUS = "+QString::number(SERVER_STATUS_OK)+" AND CSH_TYPE="+QString::number(nType)+" ORDER BY CSH_EMAIL_ACCOUNTS ASC");
		if (!pStatus.IsOK())return; 

		if (query.next())
		{
			nHostID = query.value(0).toInt();
			Ret_strHostAddress = query.value(1).toString();
		}
		else
		{
			//no host found:
			pStatus.setError(1,"Load balance server: there are no available hosts at this moment to redirect client call!");
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("LookUpHost: failed to find available host to redirect client call, client was asking for host type = %1").arg(nType));
			return;
		}

		//store info into db right away and return to the client:
		DbRecordSet rowData;
		rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_CS_EMAILS));

		rowData.addRow();
		rowData.setData(0,"CSE_EMAIL",strEmail);
		rowData.setData(0,"CSE_HOST_ID",nHostID);

		query.WriteData(pStatus,"CS_EMAILS",rowData);
		if(!pStatus.IsOK()) 
			return;

		//update count of addresses:
		query.Execute(pStatus,"UPDATE CS_HOSTS SET CSH_EMAIL_ACCOUNTS = (SELECT COUNT(CSE_ID) FROM CS_EMAILS WHERE CSE_HOST_ID=CSH_ID) WHERE CSH_ID ="+QString::number(nHostID));
		if (!pStatus.IsOK())return; 
	}

}


//lstEmails must contain column named CSE_EMAIL
//strHostUniqueID is actually CSH_ID
void ServiceHandler::SyncEmailsFromCS( Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strlstEmails )
{

#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NONE,QString("Emails for sync: %1").arg(strlstEmails));


	DbRecordSet lstEmails;
	lstEmails.addColumn(QVariant::String,"CSE_EMAIL");
	QStringList tmpList=strlstEmails.split(";",QString::SkipEmptyParts);
	int nSize=tmpList.size();
	lstEmails.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		lstEmails.setData(i,0,tmpList.at(i));
	}

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//first delete old ones
	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK()) return;

	query.Execute(pStatus,"DELETE FROM CS_EMAILS WHERE CSE_HOST_ID ="+strHostUniqueID);
	if (!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}

	//store info into db right away and return to the client:
	DbRecordSet rowData;
	rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_CS_EMAILS));

	rowData.merge(lstEmails);
	rowData.setColValue("CSE_HOST_ID",strHostUniqueID.toInt());

	query.WriteData(pStatus,"CS_EMAILS",rowData);
	if (!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}

	//update email count and timestamp for hosts:
	query.Execute(pStatus,"UPDATE CS_HOSTS SET CSH_LAST_SYNC_TIME=CURRENT_TIMESTAMP, CSH_EMAIL_ACCOUNTS = (SELECT COUNT(CSE_ID) FROM CS_EMAILS WHERE CSE_HOST_ID=CSH_ID) WHERE CSH_ID ="+strHostUniqueID);
	if(!pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}

	query.Commit(pStatus);
}

//internal GBC: if detected host that not reported in last 10 minutes, notify admin
void ServiceHandler::CheckHosts( Status &pStatus )
{

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//check if email exists
	query.Execute(pStatus,"SELECT * FROM CS_HOSTS");
	if (!pStatus.IsOK())return; 

	DbRecordSet lstHosts;
	DbRecordSet lstHostsProbablyDown;
	DbRecordSet lstHostsDown;
	query.FetchData(lstHosts);

	lstHostsProbablyDown.copyDefinition(lstHosts);
	lstHostsDown.copyDefinition(lstHosts);

	for (int i=0;i<lstHosts.getRowCount();i++)
	{
		QDateTime datLastSync=lstHosts.getDataRef(i,"CSH_LAST_SYNC_TIME").toDateTime();

		if (lstHosts.getDataRef(i,"CSH_STATUS").toInt()==SERVER_STATUS_DOWN)
		{
			lstHostsDown.merge(lstHosts.getRow(i));
			continue;
		}
		if (datLastSync<QDateTime::currentDateTime().addSecs(-10*60)) //10 minutes
		{
			lstHostsProbablyDown.merge(lstHosts.getRow(i));
		}
	}

	//compose alert body:
	QString strBody;
	if (lstHostsDown.getRowCount()>0)
	{
		strBody+="Hosts down: "+QString::number(lstHostsDown.getRowCount())+"\n";
		strBody+="-----------------------------------------------------------\n";
		strBody+="ID\t";
		strBody+="URL\t\t";
		strBody+="EMAIL_CNT\n";
		strBody+="-----------------------------------------------------------\n";
		for (int i=0;i<lstHostsDown.getRowCount();i++)
		{
			strBody+=lstHostsDown.getDataRef(i,"CSH_ID").toString()+"\t";
			strBody+=lstHostsDown.getDataRef(i,"CSH_HOST_ADDRESS").toString()+"\t";
			strBody+=lstHostsDown.getDataRef(i,"CSH_EMAIL_ACCOUNTS").toString()+"\n";
		}

		strBody+="\n\n";
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("CheckHosts: found %1 servers with status down").arg(lstHostsDown.getRowCount()));
	}

	if (lstHostsProbablyDown.getRowCount()>0)
	{
		strBody+="Hosts not reported in last 10 minutes, probably down: "+QString::number(lstHostsProbablyDown.getRowCount())+"\n";
		strBody+="-----------------------------------------------------------\n";
		strBody+="ID\t";
		strBody+="URL\t\t";
		strBody+="EMAIL_CNT\n";
		strBody+="-----------------------------------------------------------\n";
		for (int i=0;i<lstHostsProbablyDown.getRowCount();i++)
		{
			strBody+=lstHostsProbablyDown.getDataRef(i,"CSH_ID").toString()+"\t";
			strBody+=lstHostsProbablyDown.getDataRef(i,"CSH_HOST_ADDRESS").toString()+"\t";
			strBody+=lstHostsProbablyDown.getDataRef(i,"CSH_EMAIL_ACCOUNTS").toString()+"\n";
		}


		strBody+="\n\n";
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("CheckHosts: found %1 servers probably down").arg(lstHostsProbablyDown.getRowCount()));
	}

	QString strSubject;
	if (!strBody.isEmpty())
	{
		strSubject+=QString("Report from load balance server %1:%2").arg(g_AppServer->m_INIFile.m_strHostDomain).arg(g_AppServer->m_INIFile.m_nAppServerPort);
		SendAlertEmail(pStatus,strSubject,strBody);
	}





}





//If server is down or slow, send alert email to registered admin:
void ServiceHandler::SendAlertEmail(Status &err, QString strSubject, QString strBody)
{
	SMTPClient client;

	err.setError(0);
	QList<QString> lstBcc;
	QString strFrom=g_AppServer->m_INIFile.m_strMailServerAddress;
	if(!client.SendMail(g_AppServer->m_INIFile.m_strMailServer,strFrom,g_AppServer->m_INIFile.m_strAdminEmail,strSubject,strBody,lstBcc,g_AppServer->m_INIFile.m_strMailServerUserName,g_AppServer->m_INIFile.m_strMailServerPassword,false,false,g_AppServer->m_INIFile.m_nMailServerPort))
	{
		err.setError(1,client.GetLastError());
	}
}

//write host and report to host
void ServiceHandler::WriteHost( Status &pStatus, QString strConce, QString strAuthToken, int nHostID, QString strHostURL,int nType,int nStatus )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	bool bGoLockUnlockCS=false;
	if (nHostID>0)
	{
		//Fufica Marin: select first then compare: 
		query.Execute(pStatus,"SELECT CSH_STATUS FROM CS_HOSTS WHERE CSH_ID = "+QString::number(nHostID));
		if(!pStatus.IsOK()) return;

		int nStatusFromDB=0;
		if (query.next())
		{
			nStatusFromDB = query.value(0).toInt();
			if (nStatusFromDB!=nStatus)
				bGoLockUnlockCS=true;
		}
	}


	//store info into db right away and return to the client:
	DbRecordSet rowData;
	rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_CS_HOSTS));

	rowData.addRow();
	if (nHostID>0)
	{
		rowData.setData(0,"CSH_ID",nHostID);
	}
	rowData.setData(0,"CSH_HOST_ADDRESS",strHostURL);
	rowData.setData(0,"CSH_TYPE",nType);
	rowData.setData(0,"CSH_STATUS",nStatus);
	rowData.setData(0,"CSH_LAST_SYNC_TIME",QDateTime::currentDateTime());
	
	query.WriteData(pStatus,"CS_HOSTS",rowData);
	if(!pStatus.IsOK()) 
		return;

	nHostID=rowData.getDataRef(0,"CSH_ID").toInt();

#if TURN_OFF_HOST_CHECK !=1

	if (nStatus!=SERVER_STATUS_DOWN)
	{
		QString strServerURL;
		if (g_AppServer->m_INIFile.m_nSSLMode)
			strServerURL="https://"+g_AppServer->m_INIFile.m_strHostDomain+":"+QString::number(g_AppServer->m_INIFile.m_nAppServerPort);
		else
			strServerURL="http://"+g_AppServer->m_INIFile.m_strHostDomain+":"+QString::number(g_AppServer->m_INIFile.m_nAppServerPort);

		QUrl url(strHostURL);
		QString strIP=url.host();
		int nPort=url.port();
		QString strScheme=url.scheme();
		int nIsSSL=0;
		if (strScheme.toLower()=="https")
			nIsSSL=1;

		RestHTTPClient client;
		client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
		client.Connect(pStatus);
		if (!pStatus.IsOK())
		{
			return;
		}

		//generate conce & auth token
		Sha256Hash Hasher;
		QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
		QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
		client.msg_send.AddParameter(&strConce);
		client.msg_send.AddParameter(&strAuthTokenServer);
		client.msg_send.AddParameter(&nHostID);
		client.msg_send.AddParameter(&strServerURL);

		client.RestSend(pStatus,"POST","/rest/service/isserveralive");
		if (!pStatus.IsOK())
		{
			return;
		}

		int nStatus=0;
		client.msg_recv.GetParameter(0,&nStatus,pStatus);
		if (!pStatus.IsOK())
		{
			return;
		}
		Status err2;
		client.Disconnect(err2);

		if (nStatus!=SERVER_STATUS_OK)
		{
			//update status:
			//check if email exists
			query.Execute(pStatus,"UPDATE CS_HOSTS SET CSH_STATUS ="+QString::number(nStatus)+" WHERE CSH_ID="+QString::number(nHostID));
			if (!pStatus.IsOK())return; 
		}
	}

#endif

	if (bGoLockUnlockCS && nStatus!=SERVER_STATUS_DOWN)
	{
		QUrl url(strHostURL);
		QString strIP=url.host();
		int nPort=url.port();
		QString strScheme=url.scheme();
		int nIsSSL=0;
		if (strScheme.toLower()=="https")
			nIsSSL=1;

		RestHTTPClient client;
		client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
		client.Connect(pStatus);
		if (!pStatus.IsOK())
		{
			return;
		}

		int nLockReadOnly=SERVER_STATUS_OK;
		if (nStatus==SERVER_STATUS_LOCKED)
			nLockReadOnly=1;

		//generate conce & auth token
		strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
		strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
		client.msg_send.AddParameter(&strConce);
		client.msg_send.AddParameter(&strAuthTokenServer);
		client.msg_send.AddParameter(&nLockReadOnly);

		client.RestSend(pStatus,"POST","/rest/service/lockdatabase");
		if (!pStatus.IsOK())
		{
			return;
		}

		Status err2;
		client.Disconnect(err2);
	}

}

void ServiceHandler::ReadHosts( Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstHosts )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//check if email exists
	query.Execute(pStatus,"SELECT CSH_ID,CSH_HOST_ADDRESS,CSH_TYPE,CSH_STATUS,CSH_EMAIL_ACCOUNTS FROM CS_HOSTS ORDER BY CSH_HOST_ADDRESS");
	if (!pStatus.IsOK())return; 

	query.FetchData(Ret_lstHosts);

}

void ServiceHandler::DeleteHost( Status &pStatus, QString strConce, QString strAuthToken, int nHostID )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//--------------------------------------------------------
	//select status and url first
	//--------------------------------------------------------
	query.Execute(pStatus,"SELECT CSH_STATUS,CSH_HOST_ADDRESS FROM CS_HOSTS WHERE CSH_ID = "+QString::number(nHostID));
	if(!pStatus.IsOK()) return;

	QString strHostURL;
	int nServerStatus=SERVER_STATUS_DOWN;
	if (query.next())
	{
		nServerStatus = query.value(0).toInt();
		strHostURL = query.value(1).toString();
	}
	
	//--------------------------------------------------------
	//delete host
	//--------------------------------------------------------
	query.Execute(pStatus,"DELETE FROM CS_HOSTS WHERE CSH_ID="+QString::number(nHostID));
	if (!pStatus.IsOK())return; 


	//--------------------------------------------------------
	//if status != DOWN then try to unregister:
	//--------------------------------------------------------
	if (nServerStatus!=SERVER_STATUS_DOWN && !strHostURL.isEmpty())
	{
		QString strServerURL;
		if (g_AppServer->m_INIFile.m_nSSLMode)
			strServerURL="https://"+g_AppServer->m_INIFile.m_strHostDomain+":"+QString::number(g_AppServer->m_INIFile.m_nAppServerPort);
		else
			strServerURL="http://"+g_AppServer->m_INIFile.m_strHostDomain+":"+QString::number(g_AppServer->m_INIFile.m_nAppServerPort);

		QUrl url(strHostURL);
		QString strIP=url.host();
		int nPort=url.port();
		QString strScheme=url.scheme();
		int nIsSSL=0;
		if (strScheme.toLower()=="https")
			nIsSSL=1;

		RestHTTPClient client;
		client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
		client.Connect(pStatus);
		if (!pStatus.IsOK())
		{
			return;
		}

		//generate conce & auth token
		Sha256Hash Hasher;
		QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
		QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
		client.msg_send.AddParameter(&strConce);
		client.msg_send.AddParameter(&strAuthTokenServer);
		client.msg_send.AddParameter(&nHostID);
		client.msg_send.AddParameter(&strServerURL);

		client.RestSend(pStatus,"POST","/rest/service/unregisterlbserver");
		if (!pStatus.IsOK())
		{
			return;
		}

		Status err2;
		client.Disconnect(err2);
	}
}


void ServiceHandler::AddEmailToCS( Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nHostID )
{
	Sha256Hash Hasher;
#if TURN_OFF_AUTHENTICATION !=1

	
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//get host url:
	QString strHostURL;
	query.Execute(pStatus,"SELECT CSH_HOST_ADDRESS FROM CS_HOSTS WHERE CSH_ID = "+QString::number(nHostID));
	if (!pStatus.IsOK())return; 
	if (query.next())
	{
		strHostURL = query.value(0).toString();
	}

	//-----------------------------------------
	//make remote server call:
	//-----------------------------------------
		QUrl url(strHostURL);
		QString strIP=url.host();
		int nPort=url.port();
		QString strScheme=url.scheme();
		int nIsSSL=0;
		if (strScheme.toLower()=="https")
			nIsSSL=1;

		RestHTTPClient client;
		client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
		client.Connect(pStatus);
		if (!pStatus.IsOK())
		{
			return;
		}

		//generate conce & auth token
		//Sha256Hash Hasher;
		strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
		strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
		client.msg_send.AddParameter(&strConce);
		client.msg_send.AddParameter(&strAuthTokenServer);
		client.msg_send.AddParameter(&strEmail);

		client.RestSend(pStatus,"POST","/rest/service/addemailtocs");
		if (!pStatus.IsOK())
		{
			return;
		}

		Status err2;
		client.Disconnect(err2);


	//store info into db right away and return to the client:
	DbRecordSet rowData;
	rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_CS_EMAILS));

	rowData.addRow();
	rowData.setData(0,"CSE_EMAIL",strEmail);
	rowData.setData(0,"CSE_HOST_ID",nHostID);

	query.WriteData(pStatus,"CS_EMAILS",rowData);
	if(!pStatus.IsOK()) 
		return;

	//update count of addresses:
	query.Execute(pStatus,"UPDATE CS_HOSTS SET CSH_EMAIL_ACCOUNTS = (SELECT COUNT(CSE_ID) FROM CS_EMAILS WHERE CSE_HOST_ID=CSH_ID) WHERE CSH_ID ="+QString::number(nHostID));

}

void ServiceHandler::RemoveEmailToCS( Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nHostID )
{
	Sha256Hash Hasher;
#if TURN_OFF_AUTHENTICATION !=1

	
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif
	
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//get host url:
	QString strHostURL;
	query.Execute(pStatus,"SELECT CSH_HOST_ADDRESS FROM CS_HOSTS WHERE CSH_ID = "+QString::number(nHostID));
	if (!pStatus.IsOK())return; 
	if (query.next())
	{
		strHostURL = query.value(0).toString();
	}

		//-----------------------------------------
		//make remote server call:
		//-----------------------------------------
		QUrl url(strHostURL);
		QString strIP=url.host();
		int nPort=url.port();
		QString strScheme=url.scheme();
		int nIsSSL=0;
		if (strScheme.toLower()=="https")
			nIsSSL=1;

		RestHTTPClient client;
		client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
		client.Connect(pStatus);
		if (!pStatus.IsOK())
		{
			return;
		}

		//generate conce & auth token
		//Sha256Hash Hasher;
		strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
		strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
		client.msg_send.AddParameter(&strConce);
		client.msg_send.AddParameter(&strAuthTokenServer);
		client.msg_send.AddParameter(&strEmail);

		client.RestSend(pStatus,"POST","/rest/service/removeemailtocs");
		if (!pStatus.IsOK())
		{
			return;
		}

		Status err2;
		client.Disconnect(err2);



	//then add email to our database:

	//check if email exists
	query.Execute(pStatus,"DELETE FROM CS_EMAILS WHERE CSE_EMAIL='"+strEmail+"' AND CSE_HOST_ID="+QString::number(nHostID));
	if (!pStatus.IsOK())return; 

	//update count of addresses:
	query.Execute(pStatus,"UPDATE CS_HOSTS SET CSH_EMAIL_ACCOUNTS = (SELECT COUNT(CSE_ID) FROM CS_EMAILS WHERE CSE_HOST_ID=CSH_ID) WHERE CSH_ID ="+QString::number(nHostID));

}

void ServiceHandler::LockHost( Status &pStatus, QString strConce, QString strAuthToken, int nHostID, int nLockReadOnly )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql;

	//check if email exists
	query.Execute(pStatus,"SELECT CSH_HOST_ADDRESS FROM CS_HOSTS WHERE CSH_ID="+QString::number(nHostID));
	if (!pStatus.IsOK())return; 

	QString strHostURL;
	if (query.next())
	{
		strHostURL=query.value(0).toString();
	}
	
	int nStatus=SERVER_STATUS_OK;
	if (nLockReadOnly)
		nStatus=SERVER_STATUS_LOCKED;

	
#if TURN_OFF_HOST_CHECK !=1
	QUrl url(strHostURL);
	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();
	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if (!pStatus.IsOK())
	{
		return;
	}

	//generate conce & auth token
	strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	client.msg_send.AddParameter(&strConce);
	client.msg_send.AddParameter(&strAuthTokenServer);
	client.msg_send.AddParameter(&nLockReadOnly);

	client.RestSend(pStatus,"POST","/rest/service/lockdatabase");
	if (!pStatus.IsOK())
	{
		return;
	}

	Status err2;
	client.Disconnect(err2);



#endif

	//update status:
	query.Execute(pStatus,"UPDATE CS_HOSTS SET CSH_STATUS ="+QString::number(nStatus)+" WHERE CSH_ID="+QString::number(nHostID));
	if (!pStatus.IsOK())return; 
}

void ServiceHandler::ReadEmails( Status &pStatus, QString strConce, QString strAuthToken, int nHostID, DbRecordSet &Ret_lstEmails )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//check if email exists
	query.Execute(pStatus,"SELECT CSE_EMAIL FROM CS_EMAILS WHERE CSE_HOST_ID="+QString::number(nHostID)+" ORDER BY CSE_EMAIL");
	if (!pStatus.IsOK())return; 

	query.FetchData(Ret_lstEmails);

}

//CS registered email in database: register it in LB server too:
void ServiceHandler::AddEmailFromCS( Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strEmail )
{
	//return; //quick patch

#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//check if exists already, if so, just skip:
	QString strSQL=QString("SELECT CSE_ID FROM CS_EMAILS WHERE CSE_EMAIL ='%1' AND CSE_HOST_ID=%2").arg(strEmail).arg(strHostUniqueID);
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) 
		return;

	int nExistingID=0;
	if (query.next())
	{
		nExistingID=query.value(0).toInt();
	}

	if (nExistingID>0)
	{
		return; //email already exists!!!
	}


	//store info into db right away and return to the client:
	DbRecordSet rowData;
	rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_CS_EMAILS));

	rowData.addRow();
	rowData.setData(0,"CSE_EMAIL",strEmail);
	rowData.setData(0,"CSE_HOST_ID",strHostUniqueID.toInt());

	query.WriteData(pStatus,"CS_EMAILS",rowData);
	if(!pStatus.IsOK()) 
		return;

	//update email count and timestamp for hosts:
	query.Execute(pStatus,"UPDATE CS_HOSTS SET CSH_LAST_SYNC_TIME=CURRENT_TIMESTAMP, CSH_EMAIL_ACCOUNTS = (SELECT COUNT(CSE_ID) FROM CS_EMAILS WHERE CSE_HOST_ID=CSH_ID) WHERE CSH_ID ="+strHostUniqueID);
	if(!pStatus.IsOK()) 
		return;

}