#ifndef DbTableDef_H
#define DbTableDef_H

#include <QString>
#include <QStringList>
#include "common/common/dbrecordsetdefintion.h"


/*!
	\class DbView
	\brief TableView is static data container of all views on all tables
	\ingroup Db_Core

	- holds view defintions for DbRecordSet class
	- holds INSERT, SELECT, UPDATE statements for easier SQL operations
	Use:
	- define own VIEW_ID in ViewID enumerator
	- define VIEW data in initialize function
*/

class DbTableDef 
{

public:
	enum ViewID							///< ViewID - ID type definition enum
	{
		VIEW_MAIN_DATA,
		VIEW_CS_HOSTS,
		VIEW_CS_EMAILS,
	};

	static DbView& getView(int nViewID);
	static QString getSQLView(int nViewID, bool bDistinct = false);
	static QString getSQLInsert(int nViewID,QSet<int> *pLstIsLongVarcharField=NULL);
	static QString getFullSQLInsert(int nViewID,QSet<int> *pLstIsLongVarcharField=NULL);
	static QString getSQLUpdate(int nViewID,QSet<int> *pLstIsLongVarcharField=NULL);
	static QString getSQLColumnsFromView(DbView &view);
	static QStringList* getAllLongVarCharFields();

	
	static int getInsertListMapping(int nViewID);
	static int getUpdateListMapping(int nViewID);


protected:
	static void Initialize();
	static int findView(int nViewID);
	static bool CheckUniqueView();
	static QList<DbView>	s_lstDbViews;
	static bool				bInitialized;				///<if false first one to use this class will init
	static QStringList		s_lstAllLongVarCharFields;
	static bool				s_bAllLongVarCharFieldsInitialized;
	
};


#endif



