#include <QFileInfo>
#include "appserverservice.h"

int main(int argc, char *argv[])
{
	// service application
	// Command line params:
	// -i  -install		Install the service.
	// -u  -uninstall   Uninstall the service.
	// -e  -exec
	//      Execute the service as a standalone application (useful for debug purposes).
	//      This is blocking call, service will be executed like normal application.
	//      In this mode you will not be able to communicate with the service from the contoller.
	// -t  -terminate   Stop the service.
	// -p  -pause       Pause the service.
	// -r  -resume      Resume a paused service.
	// -c {cmd} -command {cmd}
	//     Send the user defined command code {cmd} to the service application.
	// -v -version      Display version and status information.

	//take arg2 as additional service server descriptor: must have if more then ONE service is installed on same machine...
	QString strApplicationServerName="Mailegant Load Balance Server";  //app server will have same name as executable

	
	if (argc>1)
	{
		QString strBaseName=argv[2];
		if (!strBaseName.isEmpty())
		{
			strApplicationServerName+=" - ";
			strApplicationServerName+=strBaseName;
			
			argc--; //for QT service for successful start
		}

		
	}
	

	AppServerService service(argc, argv,strApplicationServerName);

	return service.exec();
}
