#include "applicationserver.h"
#include "common/common/datetimehandler.h"
#include "common/common/logger.h"
#include "trans/trans/tcphelper.h"
#include "servicehandler.h"
#include "common/common/threadid.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "db_core/db_core/dbconnsettings.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbconnectionsetup.h"
#include "db_core/db_core/dbsqltableview.h"

#define MASTER_PASSWORD	"SOKRATES(R)"
#define DB_VERSION	2
#define GARBAGE_TASKER_INTERVAL	4*60 //4min

//-----------------------------------------------
//	 SERVER GLOBALS:
//------------------------------------------------

//SPECIALS:
Logger							g_Logger;
ApplicationServer				*g_AppServer=NULL;
ServiceHandler					*g_ServiceHandler=NULL;
DbSqlManager					*g_DbManager=NULL;

/*!
	Constructor
*/
ApplicationServer::ApplicationServer(QObject *parent)
:QObject(parent)
{
	ClearPointers();
	m_nTimerID=-1;
	m_bIsRunning=false;
	connect(this,SIGNAL(RestartDelayedThreadSafeSignal(int)),this,SLOT(RestartDelayedThreadSafeSlot(int)),Qt::QueuedConnection);
	connect(this,SIGNAL(StopThreadSafeSignal()),this,SLOT(StopThreadSafeSlot()),Qt::QueuedConnection);
	ApplicationLogger::setApplicationLogger(&g_Logger);
	
}

void ApplicationServer::ClearPointers()
{
	m_DbObjectManager=NULL;
	m_RestHTTPServer=NULL;
	m_RestServiceRpcDispatcher=NULL;
	g_ServiceHandler=NULL;
	m_GarbageCollector=NULL;
	m_GbBkgTask=NULL;
	g_DbManager=NULL;
	m_HtmlHTTPServer = NULL;

	m_strIniFilePath="";
}

/*!
	Destructor: kill all objects
*/
ApplicationServer::~ApplicationServer()
{
	if (IsRunning())
		Stop();
}

void ApplicationServer::Restart(Status &pStatus)
{
	//QMutexLocker locker(&m_Mutex); //lock acces
	Stop();
	Start(pStatus);
	//exit if error:

}

void ApplicationServer::RestartDelayed(int nReStartAfterSec)
{
	QMutexLocker locker(&m_Mutex);							//lock acces
	emit RestartDelayedThreadSafeSignal(nReStartAfterSec);	//ensures that appserver thread executes this!
}

//ignore parameter: do it now!
void ApplicationServer::StopDelayed(int nStopAfterSec)
{
	QMutexLocker locker(&m_Mutex);	//lock acces
	emit StopThreadSafeSignal();	//ensures that appserver thread executes this!
}

void ApplicationServer::StopThreadSafeSlot()
{
	Stop();
}

void ApplicationServer::RestartDelayedPriv(int nReStartAfterSec)
{
	//already in restart mode, ignore
	if (m_nTimerID!=-1)
	{
		return;
	}
	//this is fine opportunity to fire warning to all clients->
	m_nTimerID=startTimer(nReStartAfterSec*1000);
}

void ApplicationServer::Stop()
{
	QMutexLocker locker(&m_Mutex); //lock acces
	
	//if (!m_bIsRunning)	return;

	//stop server:
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopping Http server");
	SokratesHttpServer.StopServer();
	SokratesHttpServer.ClearRequestHandlerList();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Http server stopped");


	//--------------------------------------------------
	//Stop sequence must be in particular order: (1st stop background services)
	//--------------------------------------------------
	if(m_GbBkgTask!=NULL)m_GbBkgTask->Stop();
	

	//save log & all rest:
	m_INIFile.m_nLogLevel=g_Logger.GetLogLevel();
	m_INIFile.m_nLogMaxSize=g_Logger.GetMaxLogSize();
	m_INIFile.m_nLogBufferSize=g_Logger.GetMemoryBufferSize();
	m_INIFile.Save(m_strIniFilePath);

	//destroy objects:
	DestroyObjectDbManager();

	//destroy tasker objects:
	if(m_GbBkgTask!=NULL)					delete m_GbBkgTask;

	//destroy main objects:
	if(m_HtmlHTTPServer != NULL)			delete m_HtmlHTTPServer;
	if(m_RestHTTPServer!=NULL)				delete m_RestHTTPServer;
	if(m_RestServiceRpcDispatcher!=NULL)	delete m_RestServiceRpcDispatcher;
	if(g_ServiceHandler!=NULL)				delete g_ServiceHandler;
	if(g_DbManager!=NULL)
	{
		g_DbManager->ShutDown();
		delete g_DbManager;
	}
	if(m_GarbageCollector!=NULL)			delete m_GarbageCollector;
	
	m_bIsRunning=false;
	ClearPointers();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_STOP);
	g_Logger.FlushFileLog(); 
}



/*!
	Start App. server
*/
void ApplicationServer::Start(Status &pStatus,QString strServiceDescriptor)
{
	QMutexLocker locker(&m_Mutex); //lock access
	if (m_bIsRunning)	return;

	HTTPServerConnectionSettings ServerSettings;
	DbConnectionSettings DbConnSettings;

	//----------------------------------------------
	//LOAD INI files:
	//----------------------------------------------
	LoadINI(pStatus);
	if(!pStatus.IsOK()) return;

	if (!strServiceDescriptor.isEmpty())
	{
		m_INIFile.m_strServiceDescriptor=strServiceDescriptor;
		m_INIFile.Save(m_strIniFilePath);
	}

	//----------------------------------------------
	//Start logger 
	//----------------------------------------------
	if (m_INIFile.m_nLogLevel>LoggerAbstract::LOG_LEVEL_NONE)
	{
		g_Logger.EnableFileLogging(QCoreApplication::applicationDirPath()+"/settings_lb/appserver.log",m_INIFile.m_nLogBufferSize,m_INIFile.m_nLogMaxSize);
		g_Logger.EnableConsoleLogging();
	}
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);

	//----------------------------------------------
	//LOAD NET & DB files:
	//----------------------------------------------
	LoadNetSettings(pStatus,ServerSettings);
	if(!pStatus.IsOK()) return;
	LoadDbSettings(pStatus,DbConnSettings);
	if(!pStatus.IsOK())
	{
		CreateNewDb(pStatus,DbConnSettings);
		if(!pStatus.IsOK()) return;
	}


	//----------------------------------------------
	//init Database
	//----------------------------------------------
	InitDatabaseConnection(pStatus,&g_DbManager,DbConnSettings,m_INIFile.m_nMaxDbConnections,m_INIFile.m_nMinDbConnections);
	if(!pStatus.IsOK())return;
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_DATABASE_CONN_SUCESS,DbConnSettings.m_strDbName+";"+DbConnSettings.m_strDbHostName);
	

	//----------------------------------------------
	//Launch Business & Private sets/init pointers:
	//----------------------------------------------
	g_ServiceHandler = new ServiceHandler();


	//----------------------------------------------
	//Start Garbage Collector in separate thread (GBC controls HTTP server):
	//----------------------------------------------
	m_GarbageCollector= new GarbageCollector(&SokratesHttpServer);
	m_GbBkgTask=new BackgroundTaskExecutor(m_GarbageCollector,this,GARBAGE_TASKER_INTERVAL*1000);	//10min timer to clean up the garbage James entries from the DB
	m_GbBkgTask->Start();


	//----------------------------------------------
	//Launch all server handlers:
	//----------------------------------------------
	//TcpHelper::ResolveWebAddressNetworkInterface(ServerSettings.m_strServerIP.toLower(),ServerSettings.m_nPort,ServerSettings.m_bUseSSL);
	m_RestServiceRpcDispatcher		= new RestRpcDispatcher();
	m_RestHTTPServer				= new RestHTTPServer_Local(m_RestServiceRpcDispatcher);

	QString strWWWRootDir="";
#ifdef _DEBUG //for all our servers with webservices..put 'em here:
	strWWWRootDir=QCoreApplication::applicationDirPath()+"/../appserver_mwloadbalance/";
#endif
	m_HtmlHTTPServer				= new HtmlHTTPServer(strWWWRootDir);
	
	SokratesHttpServer.ClearRequestHandlerList();
	SokratesHttpServer.RegisterRequestHandler(m_RestHTTPServer);
	m_HtmlHTTPServer->m_nDefaultSocketTimeOut=20;						//set timeout to 20min
	SokratesHttpServer.RegisterRequestHandler(m_HtmlHTTPServer);

	SokratesHttpServer.InitServer(ServerSettings,false);
	SokratesHttpServer.StartListen(pStatus);

	if (pStatus.IsOK())
	{
		QString appParams=ServerSettings.m_strServerIP+";"+QVariant(ServerSettings.m_nPort).toString()+";"+QVariant(ServerSettings.m_bUseSSL).toString();
		appParams+=";1;"+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString();
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_START,appParams);
		m_bIsRunning=true;
	}




}

void ApplicationServer::SaveSettings()
{
	QMutexLocker locker(&m_Mutex);			//lock acces
	m_INIFile.Save(m_strIniFilePath);
}

void ApplicationServer::LoadINI(Status &pStatus)
{
	//load ini file & server settings:
	//------------------------------------------
	m_strIniFilePath= QCoreApplication::applicationDirPath()+"/settings_lb/app_server.ini";
	bool bOK=m_INIFile.Load(m_strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,"INI file is missing,corrupted or has invalid values!");
		return;
	}

	QFileInfo ssl(m_INIFile.m_strSSLCertificateDir+"/server.cert");
	if (!ssl.exists())
	{
		m_INIFile.m_strSSLCertificateDir=QCoreApplication::applicationDirPath()+"/settings_lb";
	}
}

void ApplicationServer::LoadNetSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings)
{
	pServerSettings.m_strCertificateFile=m_INIFile.m_strSSLCertificateDir+"/server.cert";
	pServerSettings.m_strCACertificateFileBundle=m_INIFile.m_strSSLCertificateDir+"/server.ca-bundle";
	pServerSettings.m_strPrivateKeyFile=m_INIFile.m_strSSLCertificateDir+"/server.pkey";
	pServerSettings.m_nPort=m_INIFile.m_nAppServerPort;

	if(m_INIFile.m_strAppServerIPAddress.isEmpty())
		pServerSettings.m_strServerIP="0.0.0.0"; //listen on all ports
	else
		pServerSettings.m_strServerIP=m_INIFile.m_strAppServerIPAddress;
	pServerSettings.m_bUseSSL=m_INIFile.m_nSSLMode;
}

//overriden QObject timer method
void ApplicationServer::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		m_nTimerID=-1;
		Status err;
		Restart(err);
		if(!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
			Stop();
		}
	}
}

void ApplicationServer::RestartDelayedThreadSafeSlot(int nRestartAfter)
{
	RestartDelayedPriv(nRestartAfter);
}

//thread safe
void ApplicationServer::OnBcpManager_RestoreCommandIssued(int nCallerID)
{
	//schedule for RESTART APP SERVER in 20 seconds:
	emit RestartDelayedThreadSafeSignal(20);
}

void ApplicationServer::LoadDbSettings(Status &pStatus,DbConnectionSettings &pDbSettings)
{
	//load DB SETTINGS:
	//------------------------------------------
	DbConnectionsList lstDbSettings;
	if(m_INIFile.m_strDBConnectionName.isEmpty())
	{
		pStatus.setError(1,"Database connection is not defined!");
		return;
	}

	QString strDbSettingsPath= QCoreApplication::applicationDirPath()+"/settings_lb/database_connections.cfg";
	bool bOK=lstDbSettings.Load(strDbSettingsPath,MASTER_PASSWORD); //PASSWORD!???
	if(!bOK)
	{
		pStatus.setError(1,"Database setting file file is missing or corrupted!");
		return;
	}

	bOK=lstDbSettings.GetDbSettings(m_INIFile.m_strDBConnectionName,pDbSettings);
	if(!bOK)
	{
		pStatus.setError(1,"Database connection not found inside database settting file!");
		return;
	}
}

void ApplicationServer::InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections, int nMinDbConnections)
{
	//make Dbmanager, set to max conn:
	*pDbManager = new DbSqlManager;
	(*pDbManager)->Initialize(pStatus, ConnSettings,nMaxDbConnections,nMinDbConnections);
	if (!pStatus.IsOK()) return;

	CreateObjectDbManager();
	CheckDatabaseSchema(pStatus);
}


void ApplicationServer::CheckDatabaseSchema(Status &pStatus)
{
	//check if tables exist, if not create them
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql;
	QStringList lstTables = query.GetDbConnection()->tables();


	if (!lstTables.contains("MAIN_DATA", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: MAIN_DATA");

		//create table here
		strSql = "CREATE TABLE MAIN_DATA ("
			"MD_ID INTEGER not null,"
			"MD_DAT_LAST_MODIFIED TIMESTAMP not null,"
			"MD_DATABASE_VERSION INTEGER not null,"
			"constraint MAIN_DATA_PK primary key (MD_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "MAIN_DATA", "MD_ID");
		if (!pStatus.IsOK())return; 
	}

	if (!lstTables.contains("CS_HOSTS", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: CS_HOSTS");

		//create table here (include port and http(s): https//everxconnect.com:9999
		strSql = "CREATE TABLE CS_HOSTS ("
			"CSH_ID INTEGER NOT NULL,"
			"CSH_DAT_LAST_MODIFIED TIMESTAMP NOT NULL,"
			"CSH_HOST_ADDRESS VARCHAR(200)," 
			"CSH_TYPE INTEGER not null," 
			"CSH_STATUS INTEGER not null," 
			"CSH_EMAIL_ACCOUNTS INTEGER not null," 
			"CSH_LAST_SYNC_TIME TIMESTAMP," 
			"constraint CS_HOSTS_PK primary key (CSH_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "CS_HOSTS", "CSH_ID");
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create unique index IND_CSH_HOST_ADDRESS on CS_HOSTS (CSH_HOST_ADDRESS)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_CSH_CSH_TYPE on CS_HOSTS (CSH_TYPE)",true);
		if (!pStatus.IsOK())return; 
	}


	if (!lstTables.contains("CS_EMAILS", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: CS_EMAILS");

		//create table here
		strSql = "CREATE TABLE CS_EMAILS ("
				 "CSE_ID INTEGER NOT NULL,"
				 "CSE_DAT_LAST_MODIFIED TIMESTAMP NOT NULL,"
				 "CSE_EMAIL VARCHAR(120),"
				 "CSE_HOST_ID INTEGER,"
				 "constraint CS_EMAILS_PK primary key (CSE_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "CS_EMAILS", "CSE_ID");
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create unique index IND_HOST_EMAIL on CS_EMAILS (CSE_EMAIL,CSE_HOST_ID)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_CSE_EMAIL on CS_EMAILS (CSE_EMAIL)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "alter table CS_EMAILS add constraint CS_EMAILS_FK1 foreign key (CSE_HOST_ID) references CS_HOSTS (CSH_ID) ON DELETE CASCADE",true);
		if (!pStatus.IsOK())return; 
	}
	
	//append long varchars to enable Write method
	QStringList *lstAllLongVarCharFields =  DbSqlTableView::getAllLongVarCharFields();
	//lstAllLongVarCharFields->prepend("BEM_BODY");
	//lstAllLongVarCharFields->prepend("CT_DATA");
	
	//read database version from table, if <> then execute organize database....
	query.Execute(pStatus,"SELECT * FROM MAIN_DATA");
	if (!pStatus.IsOK())return; 
	DbRecordSet rowMainData;
	query.FetchData(rowMainData);
	if (rowMainData.getRowCount()!=1)
	{
		rowMainData.clear();
		rowMainData.addRow();
		rowMainData.setData(0,"MD_DATABASE_VERSION",DB_VERSION);
		query.WriteData(pStatus,"MAIN_DATA",rowMainData);
		if (!pStatus.IsOK())return; 
	}

	if (rowMainData.getDataRef(0,"MD_DATABASE_VERSION").toInt()!=DB_VERSION)
	{
		//reorganize database
		OrganizeDatabase(pStatus, &query, rowMainData.getDataRef(0,"MD_DATABASE_VERSION").toInt());
		if (!pStatus.IsOK())return; 

		//set new DB version
		rowMainData.setData(0,"MD_DATABASE_VERSION",DB_VERSION);
		query.WriteData(pStatus,"MAIN_DATA",rowMainData);
		if (!pStatus.IsOK())return; 
	}


}


//upgrade database; for now nothing to do...
void ApplicationServer::OrganizeDatabase(Status &pStatus,DbSqlQuery *query, int nCurrentDatabaseVersion)
{
	if (nCurrentDatabaseVersion<=2)
	{
		QString strSQL= "ALTER TABLE CS_HOSTS ADD CSH_LAST_SYNC_TIME TIMESTAMP";
		query->Execute(pStatus, strSQL,true);
		if (!pStatus.IsOK())return; 
	}

}


void ApplicationServer::CreateNewDb(Status &pStatus,DbConnectionSettings &pDbSettings)
{
	QString strSettingsPath=QCoreApplication::applicationDirPath()+"/settings_lb/database_connections.cfg";
	QString strDbName="DATA";
	QString strTargetDataFile=QCoreApplication::applicationDirPath()+"/data_lb";
	QDir setDir(strTargetDataFile);
	if (!setDir.exists())
	{
		QString strAppDir=QCoreApplication::applicationDirPath();
		QDir DirCreate(strAppDir);
		DirCreate.mkdir("data_lb");
	}

	QString strConnection="DATA";
	m_INIFile.m_strDBConnectionName=strConnection;
	m_INIFile.Save(m_strIniFilePath);
	DbConnectionSetup::SetupAppServerDatabase(pStatus,"SYSDBA","masterkey",DBTYPE_FIREBIRD,strDbName,strConnection,strTargetDataFile,strSettingsPath);
	if (!pStatus.IsOK())
		return;
	
	pDbSettings.m_strCustomName="DATA";
	pDbSettings.m_strDbName=strTargetDataFile+"/DATA.FDB";
	pDbSettings.m_strDbUserName="SYSDBA";
	pDbSettings.m_strDbPassword="masterkey";
	pDbSettings.m_strDbType=DBTYPE_FIREBIRD;
	DbConnectionSetup::CreateDatabaseConnection(pStatus,strSettingsPath,pDbSettings);
	
}



void ApplicationServer::CreateObjectDbManager()
{
	m_DbObjectManager=NULL;

	//make instance of proper DboManager:
	if(g_DbManager->GetDbType()==DBTYPE_ORACLE)
		m_DbObjectManager= new DbObjectManager_Oracle(g_DbManager->GetDbSettings());
	else if (g_DbManager->GetDbType()==DBTYPE_MYSQL)
		m_DbObjectManager= new DbObjectManager_MySQL(g_DbManager->GetDbSettings());
	else if (g_DbManager->GetDbType()==DBTYPE_FIREBIRD)
		m_DbObjectManager= new DbObjectManager_FireBird(g_DbManager->GetDbSettings());
	else
		Q_ASSERT_X(false,"DBO ORGANIZER", "Database object manager doesn't exists for specified base");
}

void ApplicationServer::DestroyObjectDbManager()
{
	if (m_DbObjectManager)
	{
		delete m_DbObjectManager;
		m_DbObjectManager=NULL;
	}
}

