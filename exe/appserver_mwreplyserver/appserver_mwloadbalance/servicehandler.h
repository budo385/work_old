#ifndef SERVICEHANDLER_H
#define SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "db/db/dbsqlquery.h"
#include "interface_servicehandler.h"

class ServiceHandler: public Interface_ServiceHandler
{
public:

	//public API:
	void LookUpHost(Status &pStatus, QString &Ret_strHostAddress, int &Ret_nServerStatus, QString strEmail, int nType); 

	//authenticated API for interserver comm:
	void SyncEmailsFromCS(Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strlstEmails);
	void AddEmailFromCS(Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strEmail);
	void AddEmailToCS(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nHostID);
	void RemoveEmailToCS(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nHostID);
	
	//web admin API
	void WriteHost(Status &pStatus, QString strConce, QString strAuthToken, int nHostID, QString strHostURL,int nType,int nStatus);
	void ReadHosts(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstHosts );
	void DeleteHost(Status &pStatus, QString strConce, QString strAuthToken, int nHostID);
	void LockHost(Status &pStatus, QString strConce, QString strAuthToken, int nHostID, int nLockReadOnly);

	void ReadEmails(Status &pStatus, QString strConce, QString strAuthToken, int nHostID, DbRecordSet &Ret_lstEmails );

	//for period check
	void CheckHosts(Status &pStatus); //for GBC: clean expired mails and dead confirmations

private:
	void SendAlertEmail(Status &err, QString strSubject, QString strBody);

};

#endif // SERVICEHANDLER_H
