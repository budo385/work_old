#ifndef INTERFACE_SERVICEHANDLER_H
#define INTERFACE_SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_ServiceHandler
{
public:

	//public API:
	virtual void LookUpHost(Status &pStatus, QString &Ret_strHostAddress, int &Ret_nServerStatus, QString strEmail, int nType)=0; 

	//authenticated API for interserver comm:
	virtual void SyncEmailsFromCS(Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID,QString strlstEmails)=0;
	virtual void AddEmailFromCS(Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strEmail)=0;
	virtual void AddEmailToCS(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nHostID)=0;
	virtual void RemoveEmailToCS(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nHostID)=0;

	//web admin API
	virtual void WriteHost(Status &pStatus, QString strConce, QString strAuthToken, int nHostID, QString strHostURL,int nType,int nStatus)=0;
	virtual void ReadHosts(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstHosts )=0;
	virtual void DeleteHost(Status &pStatus, QString strConce, QString strAuthToken, int nHostID)=0;
	virtual void LockHost(Status &pStatus, QString strConce, QString strAuthToken, int nHostID, int nLockReadOnly)=0;

	virtual void ReadEmails(Status &pStatus, QString strConce, QString strAuthToken, int nHostID, DbRecordSet &Ret_lstEmails )=0;
};


/*
<Web_service_meta_data>

	<LookUpHost>
		<URL>/service/lookuphost</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</LookUpHost>
	
	<WriteHost>
		<URL>/service/writehost</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</WriteHost>

	<ReadHosts>
		<URL>/service/readhosts</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</ReadHosts>
	
	<DeleteHost>
		<URL>/service/deletehost</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</DeleteHost>
	
	<LockHost>
		<URL>/service/lockhost</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</LockHost>

	<ReadEmails>
		<URL>/service/reademails</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</ReadEmails>

	<SyncEmailsFromCS>
		<URL>/service/syncemailsfromcs</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SyncEmailsFromCS>
	
	<AddEmailFromCS>
		<URL>/service/addemailfromcs</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</AddEmailFromCS>

	<AddEmailToCS>
		<URL>/service/addemailtocs</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</AddEmailToCS>

	<RemoveEmailToCS>
		<URL>/service/removeemailtocs</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</RemoveEmailToCS>

</Web_service_meta_data>
*/

#endif // INTERFACE_SERVICEHANDLER_H

	
	
	