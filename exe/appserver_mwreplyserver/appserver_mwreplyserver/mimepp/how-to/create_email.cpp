// create_mail.cpp -- shows how to create a new message using
// mimepp::email::Message

#pragma warning(disable: 4251)

#include <email/email.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
using namespace std;

void readFile(mimepp::String& buf, const char* filename, bool binary=false)
{
    ios_base::openmode flags = ios_base::in;
    if (binary) {
        flags |= ios_base::binary;
    }
    ifstream in(filename, flags);
    if (! in) {
        cout << "Can't open file " << filename << endl;
        exit(1);
    }
    in.seekg(0, ios::end);
    int size = in.tellg();
    in.seekg(0, ios::beg);
    buf.reserve(size);
    char ch;
    while (in.get(ch)) {
        buf.append(1, ch);
    }
}

int main()
{
    mimepp::Initialize();

    // Create a new mail::Message object
    mimepp::email::Message msg;
    // Set the From header field
    msg.originator(mimepp::email::Address("mueller@example.com",
        "J\374rgen M\374ller", "ISO-8859-1"));
    // Set the To header field
    msg.toRecipients().add(mimepp::email::Address("fuchs@example.com",
        "Andreas Fuchs", "ISO-8859-1"));
    msg.toRecipients().add(mimepp::email::Address("braun@example.com",
        "Martin Braun", "ISO-8859-1"));
    // Set the Cc header field
    msg.ccRecipients().add(mimepp::email::Address("renate@example.com",
        "Renate M\374ller", "ISO-8859-1"));
    // Set the Bcc header field
    msg.bccRecipients().add(mimepp::email::Address("mueller@example.com",
        "J\374rgen M\374ller", "ISO-8859-1"));
    // Set the Subject header field
    const char *subj = "Spielen wir heute nachmittag Fu\337ball?";
    msg.subject(mimepp::email::Text(subj, "ISO-8859-1", string(), false));
    // Set the message body
    const char *memo = "Spielen wir heute nachmittag Fu\337ball? "
        "Was meint ihr dazu?\n";
    msg.setMemoTextFromPlainText(mimepp::email::Text(memo, "ISO-8859-1",
        string(), true));
    // Add a file attachment
    // Note: readFile() is not a MIME++ library function
    mimepp::String buf;
    bool binary = true;
    readFile(buf, "ball.png", binary);
    mimepp::email::Attachment attach;
    attach.mediaType("image", "png");
    attach.fileName("ball.png");
    attach.content(buf);
    msg.attachments().add(attach);
    // Serialize the message to a string
    mimepp::String msgString;
    msg.serializeTo(msgString);
    // See how it looks
    cout << msgString.c_str();

    mimepp::Finalize();

    return 0;
}
