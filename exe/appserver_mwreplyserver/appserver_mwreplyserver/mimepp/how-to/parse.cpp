// parse.cpp -- shows how to parse a message

#include <mimepp/mimepp.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
using namespace std;

void readFile(mimepp::String& buf, const char* filename, bool binary=false)
{
    ios_base::openmode flags = ios_base::in;
    if (binary) {
        flags |= ios_base::binary;
    }
    ifstream in(filename, flags);
    if (! in) {
        cout << "Can't open file " << filename << endl;
        exit(1);
    }
    in.seekg(0, ios::end);
    int size = in.tellg();
    in.seekg(0, ios::beg);
    buf.reserve(size);
    char ch;
    while (in.get(ch)) {
        buf.append(1, ch);
    }
}

int main()
{
    mimepp::Initialize();

    // Create a new Message object
    mimepp::Message msg;
    // Set string representation of the message
    mimepp::String msgStr;
    readFile(msgStr, "simple-msg.txt");
    msg.setString(msgStr);
    // Parse the message
    msg.parse();
    // Show the originator, recipients, etc
    cout << msg.headers().from().text().c_str() << endl;
    cout << msg.headers().to().text().c_str() << endl;
    cout << msg.headers().subject().text().c_str() << endl;
    cout << msg.headers().date().text().c_str() << endl;
    // Show the message body
    cout << msg.body().getString().c_str() << endl;

    mimepp::Finalize();

	return 0;
}
