// create.cpp -- shows how to create a new message

#include <mimepp/mimepp.h>
#include <iostream>
#include <time.h>
using std::cout;


int main()
{
    mimepp::Initialize();

    // Create a new Message object
    mimepp::Message msg;
    // Set the From, To, Subject, and Date header fields
    msg.headers().from().setText("john@example.com");
    msg.headers().to().setText("mary@example.com");
    msg.headers().subject().setText("Call me");
    msg.headers().date().fromCalendarTime(time(0));
    // Set the message body
    msg.body().setString("Call me when you can.\n");
    // Finally, assemble the message into a string
    msg.assemble();
    // See how it looks
    cout << msg.getString().c_str();

    mimepp::Finalize();

	return 0;
}
