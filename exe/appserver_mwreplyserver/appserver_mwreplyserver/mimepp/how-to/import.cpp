// import.cpp -- shows how to import a large buffer efficiently

#include <mimepp/mimepp.h>
#include <string.h>
#include <stddef.h>
#include <iostream>
using namespace std;


void useThisBuffer(char* buffer, size_t size, size_t length)
{
    // Set refCount to 0 if you want MIME++ to delete the buffer. Set
    // it to 1 if you don't want MIME++ to delete the buffer.
    int refCount = 0;
    mimepp::StringRep* rep = mimepp::StringRep::create(buffer, size, refCount);
    int offset = 0;
    mimepp::String str(rep, offset, length);
    // Okay to use the String now.
    cout << str.c_str() << endl;
}

int main()
{
    mimepp::Initialize();

    size_t size = 200;
    char* buffer = (char*) mimepp::StringRep::allocBuffer(size);
    strcpy(buffer, "Hi, Mom!");
    size_t length = strlen(buffer);
    useThisBuffer(buffer, size, length);

    mimepp::Finalize();

    return 0;
}
