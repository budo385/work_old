// set_x_foo.cpp -- shows how to set the value of an X-Foo header field

#include <mimepp/mimepp.h>
#include <iostream>
using namespace std;


void setXFooHeader(mimepp::Entity& entity, const char* value)
{
    entity.headers().fieldBody("X-Foo").setText(value);
}

int main()
{
    mimepp::Initialize();

    mimepp::Message msg;
    setXFooHeader(msg, "All cows eat grass");
    msg.body().setString("Hi, Mom!");
    msg.assemble();
    cout << msg.getString().c_str() << endl;

    mimepp::Finalize();

    return 0;
}
