// Address.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include "email/email.h"
using namespace mimepp::email;
using namespace std;


/**
 * \class mimepp::email::Address
 *
 * <b>%Address</b> represents an Internet email address.
 *
 * The address consists of two parts: a global mailbox name and a display
 * name.  The global mailbox name is the SMTP email address: it's the name
 * that identifies the mailbox where mail should be delivered.  The display
 * name is the name that is typically displayed in the user interface of a
 * mail client application.  Consider the following string:
 *
 *    Jack Spratt &lt;jack.spratt\@example.net&gt;
 *
 * The display name is "Jack Spratt".  The mailbox name is
 * "jack.spratt@example.net".
 *
 * <b>%Address</b> has member functions to get and set the mailbox name
 * and the display name.
 */

/**
 * Default constructor.
 */
mimepp::email::Address::Address()
{
}

/**
 * Copy constructor.
 *
 * @param other reference to the object to copy
 */
mimepp::email::Address::Address(const Address& other)
  : mMailboxName(other.mMailboxName),
    mDisplayName(other.mDisplayName),
    mCharset(other.mCharset)
{
}

/**
 * Constructor with initial values.
 *
 * @param mailboxName global mailbox name, <i>e.g.</i> "spratt@example.net"
 * @param displayName display name, <i>e.g.</i> "Jack Spratt"
 * @param charset charset identifier of the display name, <i>e.g.</i> "UTF-8"
 */
mimepp::email::Address::Address(const string& mailboxName,
    const string& displayName, const string& charset)
  : mMailboxName(mailboxName),
    mDisplayName(displayName),
    mCharset(charset)
{
}

/**
 * Destructor.
 */
mimepp::email::Address::~Address()
{
}

/**
 * Assignment operator.
 *
 * @param other reference to the object to assign
 */
mimepp::email::Address& Address::operator = (const Address& other)
{
    if (this != &other) {
        mMailboxName = other.mMailboxName;
        mDisplayName = other.mDisplayName;
        mCharset = other.mCharset;
    }
    return *this;
}

/**
 * Sets Address object's value.
 *
 * @param mailboxName global mailbox name, <i>e.g.</i> "spratt@example.net"
 * @param displayName display name, <i>e.g.</i> "Jack Spratt"
 * @param charset charset identifier of the display name, <i>e.g.</i> "UTF-8"
 */
void mimepp::email::Address::set(const string& mailboxName,
    const string& displayName, const string& charset)
{
    mMailboxName = mailboxName;
    mDisplayName = displayName;
    mCharset = charset;
}

