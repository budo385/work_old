/* -*- mode: C -*- */

/**

\mainpage Hunny mimepp-email Library

\section About About the mimepp-email library

This project contains the source code for a high-level library that
simplifies MIME++ for use in Internet email.  We call this library the
mimepp-email library, because it uses the mimepp::email namespace.  With
the mimepp-email library, you can easily do the following:

  - create a message containing one or more attachments
  - convert a message to an HTML page to view in a web browser
  - get the main text of a message, converted to UTF-8
  - get all the attachments of a message

The mimepp-email library assumes that a message contains:

  - an originator (From)
  - one or more recipients (To, Cc, Bcc)
  - a date
  - a subject
  - a memo text (the text that is displayed in the main window of a client application)
  - optionally, one or more attachments

These attributes are standard in Internet email.  If you need to, you can
customize the source code for your own application.

\section Limitations

The mimepp-email library is simple to use, but it has some limitations.  If
you need to do complex tasks, then you may need to use the MIME++ library
directly.  Basically, if you need to generate a message or parse a message,
then you should probably use the mimepp-email library.  However, if you
need to modify an existing message, then you should probably use the MIME++
library directly.  The reason is that the mimepp-email library loses
information in the message when you parse it, modify it, and serialize it
again.  The MIME++ library, on the other hand, does not lose any
information and can modify messages very efficiently.

Here are some examples of when you should use the simplified mimepp-email
library and when you should use the MIME++ library directly:

<table border="1" cellspacing="0" cellpadding="3"
 summary="Suggested libraries to use for different tasks">
<tr><td><b>Programming Task</b></td><td><b>Suggested Library</b></td></tr>
<tr>
<td>Create a message, possibly with one or more attachments</td>
<td>mimepp-email</td>
</tr>
<tr>
<td>Display a message as a web page in a webmail application</td>
<td>mimepp-email</td>
</tr>
<tr>
<td>Change the subject line of a message, <i>e.g.</i> add ****SPAM**** to
the subject line</td>
<td>MIME++</td>
</tr>
<tr>
<td>Search the main text of a message</td>
<td>mimepp-email</td>
</tr>
<tr>
<td>Add an attachment or remove an attachment from an existing message</td>
<td>MIME++</td>
</tr>
</table>

If you have any questions about using the mimepp-email library or the
MIME++ library, please ask us!

\section Compiling

If you are using Visual C++, you can use the provided project file to
compile the mimepp-email library as a DLL.  You may need to change the
compiler options to specify location of the ICU header files.  You may need
to change the linker options to specify the location of the ICU import
library files.

If you are compiling on Linux or Solaris, you can use the makefiles
provided.  You may need to change the makefile to indicate the location of
the ICU header files and the ICU library files.

\section Using

We have included two different example programs that demonstrate the use of
the mimepp-email library.  The program msg2html_ex.cpp converts an email
message to an HTML file.  This is a good program to use for testing.  Try
running the program with the included test messages, then open the HTML
file with your web browser.  The msg2html_ex.cpp example program is also a
good starting point for webmail application.

The program create_ex.cpp creates a mail message by adding an originator,
recipients, and so on.

*/
