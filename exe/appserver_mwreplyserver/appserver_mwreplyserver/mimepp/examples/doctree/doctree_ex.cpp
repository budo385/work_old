// doctree.cpp
//
// This utility prints the document tree of a MIME message to standard
// output.
//
// To use the doctree utility, run it from the command line and provide the
// name of a file as a command line parameter.  The file should contain a
// MIME message.  The output shows the tree of objects that MIME++ has
// created.
//

#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <stdlib.h>
#include <mimepp/mimepp.h>
using namespace mimepp;

using std::cout;
using std::endl;

void PrintBodyPart(const String&, const String&, const BodyPart&);
void PrintMessage(const String& aIndent, const String& aIndentAdd,
    const Message& aMessage);


// Print tree node that has no child nodes

void PrintDefault(const String& aIndent, const Node& aNode)
{
    // Print this node
    const char* className = aNode.class_().name();
    cout << aIndent.c_str() << "+-" << className << endl;
}


// Print tree node for TransferEncodingType object

void PrintTransferEncodingType(const String& aIndent,
    const TransferEncodingType& aType)
{
    // Print this node
    cout << aIndent.c_str() << "+-TransferEncodingType ("
        << aType.type().c_str() << ")" << endl;
}


// Print tree node for Parameter object

void PrintParameter(const String& aIndent, const Parameter& aParam)
{
    // Print this node
    cout << aIndent.c_str() << "+-Parameter (" << aParam.name().c_str()
        << ")" << endl;
}


// Print tree node for Maibox object

void PrintMailbox(const String& aIndent, const Mailbox& aMailbox)
{
    // Print this node
    cout << aIndent.c_str() << "+-Mailbox (" << aMailbox.localPart().c_str()
        << "@" << aMailbox.domain().c_str() << ")" << endl;
}


// Print tree node for MailboxList object

void PrintMailboxList(const String& aIndent, const String& aIndentAdd,
    const MailboxList& aList)
{
    // Print this node
    cout << aIndent.c_str() << "+-MailboxList" << endl;

    String indent = aIndent + aIndentAdd;

    // Print the Mailbox objects
    int numMailboxes = aList.numMailboxes();
    for (int j=0; j < numMailboxes; ++j) {
        const Mailbox& mbox = aList.mailboxAt(j);
        PrintMailbox(indent, mbox);
    }
}


// Print tree node for Group object

void PrintGroup(const String& aIndent, const String& aIndentAdd,
    const Group& aGroup)
{
    // Print this node
    cout << aIndent.c_str() << "+-Group (" << aGroup.groupName().c_str()
        << ")" << endl;

    String indent = aIndent + aIndentAdd;

    // Print the MailboxList object
    String indentAdd = "  ";
    const MailboxList& mboxList = aGroup.mailboxList();
    PrintMailboxList(indent, indentAdd, mboxList);
}


// Print tree node for AddressList object

void PrintAddressList(const String& aIndent, const String& aIndentAdd,
    const AddressList& aList)
{
    // Print this node
    cout << aIndent.c_str() << "+-AddressList" << endl;

    String indent = aIndent + aIndentAdd;

    // Print the Address objects
    int numAddresses = aList.numAddresses();
    for (int j=0; j < numAddresses; ++j) {
        const Address& addr = aList.addressAt(j);
        if (addr.class_().isA(GROUP_CLASS)) {
            String indentAdd;
            if (j < numAddresses - 1) {
                indentAdd = "| ";
            }
            else {
                indentAdd = "  ";
            }
            PrintGroup(indent, indentAdd, (const Group&) addr);
        }
        else if (addr.class_().isA(MAILBOX_CLASS)) {
            PrintMailbox(indent, (const Mailbox&) addr);
        }
        else {
            assert(0);
        }
    }
}


// Print tree node for DispositionType object

void PrintDispositionType(const String& aIndent, const String& aIndentAdd,
    const DispositionType& aDtype)
{
    // Print this node
    cout << aIndent.c_str() << "+-DispositionType ("
        << aDtype.type().c_str() << ")" << endl;

    String indent = aIndent + aIndentAdd;

    // Print the Parameter objects
    int numParameters = aDtype.numParameters();
    for (int j=0; j < numParameters; ++j) {
        const Parameter& param = aDtype.parameterAt(j);
        PrintParameter(indent, param);
    }
}


// Print tree node for MediaType object

void PrintMediaType(const String& aIndent, const String& aIndentAdd,
    const MediaType& aMtype)
{
    // Print this node
    cout << aIndent.c_str() << "+-MediaType (" << aMtype.type().c_str()
        << "/" << aMtype.subtype().c_str() << ")" << endl;

    String indent = aIndent + aIndentAdd;

    // Print the Parameter objects
    int numParameters = aMtype.numParameters();
    for (int j=0; j < numParameters; ++j) {
        const Parameter& param = aMtype.parameterAt(j);
        PrintParameter(indent, param);
    }
}


// Print tree node for Text object

void PrintText(const String& aIndent, const String& aIndentAdd,
    const Text& aText)
{
    // Print this node
    cout << aIndent.c_str() << "+-Text" << endl;

    String indent = aIndent + aIndentAdd;

    // Print the EncodedWords objects
    int numEncodedWords = aText.numEncodedWords();
    for (int j=0; j < numEncodedWords; ++j) {
        const EncodedWord& word = aText.encodedWordAt(j);
        PrintDefault(indent, word);
    }
}


// Print tree node for Field object

void PrintField(const String& aIndent, const String& aIndentAdd,
    const Field& aField)
{
    // Print this node
    cout << aIndent.c_str() << "+-Field (" << aField.fieldName().c_str()
        << ")" << endl;

    String indent = aIndent + aIndentAdd;
    String indentAdd = "  ";

    // Print the FieldBody object
    const FieldBody& fldBody = aField.fieldBody();
    int id = fldBody.class_().id();
    switch (id) {
    case ADDRESS_LIST_CLASS:
        {
            const AddressList& addrListFieldBody = (AddressList&) fldBody;
            PrintAddressList(indent, indentAdd, addrListFieldBody);
        }
        break;
    case DISPOSITION_TYPE_CLASS:
        {
            const DispositionType& dtypeFieldBody = (DispositionType&)
                fldBody;
            PrintDispositionType(indent, indentAdd, dtypeFieldBody);
        }
        break;
    case MEDIA_TYPE_CLASS:
        {
            const MediaType& mtypeFieldBody = (MediaType&) fldBody;
            PrintMediaType(indent, indentAdd, mtypeFieldBody);
        }
        break;
    case MAILBOX_LIST_CLASS:
        {
            const MailboxList& mboxListFieldBody = (MailboxList&) fldBody;
            PrintMailboxList(indent, indentAdd, mboxListFieldBody);
        }
        break;
    case TEXT_CLASS:
        {
            const Text textFieldBody = (Text&) fldBody;
            PrintText(indent, indentAdd, textFieldBody);
        }
        break;
    case TRANSFER_ENCODING_TYPE_CLASS:
        {
            const TransferEncodingType te = (TransferEncodingType&) fldBody;
            PrintTransferEncodingType(indent, te);
        }
        break;
    default:
        {
            PrintDefault(indent, fldBody);
        }
        break;
    }
}


// Print tree node for Headers object

void PrintHeaders(const String& aIndent, const String& aIndentAdd,
    const Headers& aHeaders)
{
    // Print this node
    cout << aIndent.c_str() << "+-Headers" << endl;

    String indent = aIndent + aIndentAdd;

    // Print all the Field objects
    int numFields = aHeaders.numFields();
    for (int j=0; j < numFields; ++j) {
        String indentAdd;
        if (j < numFields - 1) {
            indentAdd = "| ";
        }
        else {
            indentAdd = "  ";
        }
        const Field& field = aHeaders.fieldAt(j);
        PrintField(indent, indentAdd, field);
    }
}


// Print tree node for Body object

void PrintBody(const String& aIndent, const String& aIndentAdd,
    const Body& aBody)
{
    // Print this node
    cout << aIndent.c_str() << "+-Body" << endl;

    String indent = aIndent + aIndentAdd;

    Node* node = aBody.parent();
    assert(node && node->class_().isA(ENTITY_CLASS));
    Entity* parent = (Entity *) node;
    Headers& headers = parent->headers();

    // If this is a multipart message, print all the BodyPart objects
    if (headers.hasField("Content-Type")
        && headers.contentType().typeAsEnum() == MediaType::MULTIPART) {
        int numBodyParts = aBody.numBodyParts();
        int j;
        for (j=0; j < numBodyParts; ++j) {
            String indentAdd;
            if (j < numBodyParts - 1) {
                indentAdd = "| ";
            }
            else {
                indentAdd = "  ";
            }
            const BodyPart& part = aBody.bodyPartAt(j);
            PrintBodyPart(indent, indentAdd, part);
        }
    }
    // If this is a message/rfc822, print the contained Message object
    if (headers.hasField("Content-Type")
        && headers.contentType().typeAsEnum() == MediaType::MESSAGE
        && 0 == mimepp::Strcasecmp(headers.contentType().subtype(),"rfc822")) {
        Message* msg = aBody.message();
        assert(msg);
        String indentAdd = "  ";
        PrintMessage(indent, indentAdd, *msg);
    }
}


// Print tree node for BodyPart object

void PrintBodyPart(const String& aIndent, const String& aIndentAdd,
    const BodyPart& aPart)
{
    // Print this node
    String type = "text";
    String subtype = "plain";
    if (aPart.headers().hasField("Content-Type")) {
        type = aPart.headers().contentType().type();
        subtype = aPart.headers().contentType().subtype();
    }
    cout << aIndent.c_str() << "+-BodyPart (" << type.c_str()
         << "/" << subtype.c_str() << ")" << endl;

    String indent = aIndent + aIndentAdd;

    // Print the Headers object
    String indentAdd = "| ";
    PrintHeaders(indent, indentAdd, aPart.headers());

    // Print the Body object
    indentAdd = "  ";
    PrintBody(indent, indentAdd, aPart.body());
}


// Print tree node for Message object

void PrintMessage(const String& aIndent, const String& aIndentAdd,
    const Message& aMessage)
{
    // Print this node
    String type = "text";
    String subtype = "plain";
    if (aMessage.headers().hasField("content-type")) {
        type = aMessage.headers().contentType().type();
        subtype = aMessage.headers().contentType().subtype();
    }
    cout << aIndent.c_str() << "+-Message (" << type.c_str() << "/"
       << subtype.c_str() << ")" << endl;

    String indent = aIndent + aIndentAdd;

    // Print the Headers object
    String indentAdd = "| ";
    PrintHeaders(indent, indentAdd, aMessage.headers());

    // Print the Body object
    indentAdd = "  ";
    PrintBody(indent, indentAdd, aMessage.body());
}


int main(int argc, char **argv)
{
    //
    // Initialize the library
    //
    mimepp::Initialize();

    //
    // MIME++ has certain global options that you may set only at start-up
    // time, before you create any threads.
    //
    // Currently, there is only one option: Max Depth.  This option limits
    // how deeply MIME++ automatically parses nested body parts.  If you
    // set the option to 0, then MIME++ does not parse any body parts
    // automatically.  If you set the option to 1, then it parses only the
    // first level of body parts.  This option exists for security.  If we
    // allow unlimited nesting of body parts, then an attacker could tie up
    // CPU resources by forcing excessive processing of messages, which
    // could contribute to a denial of service attack.
    //
    // Normally, you will not see any nesting depth beyond three or four.
    // However, you must be careful.  If your application is a security
    // application, and if you limit the parsing depth, an attacker may be
    // able to by-pass your security by constructing a message that is
    // nested more deeply than what your app checks for.  If you need to
    // parse messages with unlimited nesting of body parts, then I suggest
    // that you set Max Depth to 0 to prevent automatic parsing, then write
    // your application to parse the Body objects as it processes them
    // (that is, not automatically).
    //
    // The default is 5.
    //
    mimepp::setMaxDepth(5);

    //
    // Get a message from a file.  The file name is the first commandline
    // parameter.
    //
    if (argc < 2) {
        printf("Please specify a file name.\n");
        return 1;
    }
    FILE* in = fopen(argv[1], "rb");
    if (in == NULL) {
        printf("I can't open the file \"%s\".\n", argv[1]);
        return 1;
    }
    fseek(in, 0, SEEK_END);
    size_t fileLen = ftell(in);
    fseek(in, 0, SEEK_SET);
    String messageStr;
    messageStr.reserve(fileLen);
    char* buffer = (char*) malloc(8192);
    do {
        size_t count = fread(buffer, 1, 8192, in);
        messageStr.append(buffer, count);
    } while (! feof(in));
    free(buffer);
    buffer = 0;
    fclose(in);
    in = 0;
    //
    // Create a message object and parse the message
    //
    Message message(messageStr);
    message.parse();
    //
    // Print the document tree
    //
    String indent = "";
    String indentAdd = "  ";
    PrintMessage(indent, indentAdd, message);

    //
    // Finalize the library
    //
    mimepp::Finalize();

    return 0;
}
