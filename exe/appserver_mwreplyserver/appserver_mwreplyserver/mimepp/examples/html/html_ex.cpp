// html.cpp
//
// The example program in this file shows how to create a message
// containing HTML text with embedded images.
// 

#include <mimepp/mimepp.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;
using namespace mimepp;


void addText(Message& msg, const char* filename)
{

    // Get the HTML text from the file

    ifstream in(filename);
    if (! in) {
        cout << "I can't open the file \"" << filename << "\"." << endl;
        exit(EXIT_FAILURE);
    }
    in.seekg(0, ios::end);
    int fileLen = (int) in.tellg();
    in.seekg(0, ios::beg);
    String text;
    text.reserve(fileLen);
    char ch = 0;
    while (in.get(ch)) {
        text.append(1, ch);
    }
    in.close();

    // Encode the text with the quoted-printable transfer encoding

    QuotedPrintableEncoder qencoder;
    String encodedText = qencoder.encode(text);

    // Create a body part for the HTML text

    BodyPart* part = new BodyPart;

    // Set the content-type header field to text/plain; charset=iso-8859-1

    part->headers().contentType().setType("text");
    part->headers().contentType().setSubtype("html");
    Parameter* param = new Parameter;
    param->setName("charset");
    param->setValue("iso-8859-1");
    part->headers().contentType().addParameter(param);
    param = 0;

    // Set the content-transfer-encoding header field to quoted-printable

    part->headers().contentTransferEncoding()
        .fromEnum(TransferEncodingType::QUOTED_PRINTABLE);

    // Set the encoded HTML text as the body of the body part

    part->body().setString(encodedText);

    // Add the body part to the message

    msg.body().addBodyPart(part);
    part = 0;
}


void addImage(Message& msg, const char* filename, const char* mimeType,
    const char* cid)
{
    // Get the image content from the file

    ifstream in;
    in.open(filename, ios::in|ios::binary);
    if (! in) {
        cout << "Whoa! I can't open the file \"" << filename << "\"." << endl;
        exit(EXIT_FAILURE);
    }
    in.seekg(0, ios::end);
    int fileLen = (int) in.tellg();
    in.seekg(0, ios::beg);
    String img;
    img.reserve(fileLen);
    char ch;
    while (in.get(ch)) {
        img.append(1, ch);
    }
    in.close();

    // Encode the image content with the base64 transfer encoding

    Base64Encoder bencoder;
    String encodedImg = bencoder.encode(img);

    // Create a body part for the image

    BodyPart* part = new BodyPart;

    // Set the content-type header field

    part->headers().contentType().setString(mimeType);

    // Set the content-transfer-encoding header field to base64

    part->headers().contentTransferEncoding()
        .fromEnum(TransferEncodingType::BASE64);

    // Set the content-ID header field

    part->headers().contentId().setString(cid);

    // Set the encoded image content as the body of the body part

    part->body().setString(encodedImg);

    // Add the body part to the message

    msg.body().addBodyPart(part);
    part = 0;
}


int main(int argc, char **argv)
{
    // Initialize the library

    mimepp::Initialize();

    // Create a new message.

    Message msg;

    // Add basic header fields: To, From, Subject, etc. (not provided in
    // this example)

    // Set the content-type header field to multipart/related.  Add a
    // boundary parameter and a type parameter.

    MediaType& mtype = msg.headers().contentType();
    mtype.setType("multipart");
    mtype.setSubtype("related");
    mtype.createBoundary();
    Parameter* param = new Parameter;
    param->setName("type");
    param->setValue("text/html");
    mtype.addParameter(param);
    param = 0;

    // Set the HTML message text

    addText(msg, "msg.htm");

    // Add the images

    addImage(msg, "redball.png", "image/png", "<1001@frodo.hunnysoft.com>");
    addImage(msg, "greenball.png", "image/png", "<1002@frodo.hunnysoft.com>");
    addImage(msg, "blueball.png", "image/png", "<1003@frodo.hunnysoft.com>");

    // Build the message

    msg.assemble();

    // Write the message to an output file.  Note that the .mht extension
    // is recognized by Windows as an MHTML file, which Internet Explorer
    // can display.

    ofstream out("msg.mht");
    out.write(msg.getString().data(), msg.getString().length());
    out.close();

    // Finalize the library

    mimepp::Finalize();

    return 0;
}
