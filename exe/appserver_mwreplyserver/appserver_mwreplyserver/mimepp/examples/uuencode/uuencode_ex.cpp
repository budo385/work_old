// uuencode_ex.cpp -- Shows how to convert uuencoded email to MIME

#include <stdio.h>
#include <stdlib.h>
#include <mimepp/mimepp.h>
using namespace mimepp;


static int MatchesBeginPattern(const char *aPtr, int aPos, int aLen)
{
    // Pattern is: "begin ddd x"
    // where d is a digit and x is a filename character

    int matches = 0;
    const char *ptr = &aPtr[aPos];
    if (aLen - aPos >= 11
        && ptr[0] == 'b'
        && ptr[1] == 'e'
        && ptr[2] == 'g'
        && ptr[3] == 'i'
        && ptr[4] == 'n'
        && ptr[5] == ' '
        && 48 <= ptr[6] && ptr[6] <= 57
        && 48 <= ptr[7] && ptr[7] <= 57
        && 48 <= ptr[8] && ptr[8] <= 57
        && ptr[9] == ' '
        && 33 <= ptr[10] && ptr[10] <= 126) {

        matches = 1;
    }
    return matches;
}


static int FindEndPattern(const char* aPtr, int aPos, int aLen)
{
    int endPos = -1;
    int patternFound = 0;
    int pos = aPos;
    while (pos < aLen) {
        char ch = aPtr[pos++];
        if (ch == '\n') {
            if (aLen - pos >= 3
                && aPtr[pos  ] == 'e'
                && aPtr[pos+1] == 'n'
                && aPtr[pos+2] == 'd') {

                pos += 3;
                patternFound = 1;
                break;
            }
        }
    }
    if (patternFound) {
        // We matched the end pattern.  Now find either end of current line
        // of end of buffer
        while (pos < aLen) {
            char ch = aPtr[pos++];
            if (ch == '\n') {
                break;
            }
        }
        endPos = pos;
    }
    return endPos;
}


static void AddAttachment(Body& aBody, const String& aPartContent,
    const String& aFilename)
{
    // Create new body part

    BodyPart* part = BodyPart::newBodyPart();

    // Set headers like this:
    //
    // Content-Type: application/octet-stream; name="filename"
    // Content-Disposition: attachment; filename="filename"

    part->headers().contentType().setType("APPLICATION");
    part->headers().contentType().setSubtype("OCTET-STREAM");
    part->headers().contentType().setName(aFilename);
    part->headers().contentDisposition().
         fromEnum(DispositionType::ATTACHMENT);
    part->headers().contentDisposition().setFilename(aFilename);
    part->headers().contentTransferEncoding()
        .fromEnum(TransferEncodingType::BASE64);

    // Add the binary content to the body part after base64 encoding

    String encodedContent;
    Base64Encoder encoder;
    encoder.setOutputCrLf(true);
    encodedContent = encoder.encode(aPartContent);
    part->body().setString(encodedContent);

    // Add the body part to the body

    aBody.addBodyPart(part);
}


static void TextUuencodeToMime(Entity& aEntity)
{
    // Get the original body content

    String oldBodyContent = aEntity.body().getString();
    const char* oldBodyPtr = oldBodyContent.data();
    int oldBodyLen = (int) oldBodyContent.length();

    // Create a new body and a string for the text part.

    String textContent;
    Body body;

    // Set first part in body part as the text part

    BodyPart* textBodyPart = BodyPart::newBodyPart();
    textBodyPart->headers().contentType().setType("TEXT");
    textBodyPart->headers().contentType().setSubtype("PLAIN");
    body.addBodyPart(textBodyPart);

    // Search for uuencoded attachments and extract them

    int pos = 0;
    int textStart = 0;
    int textEnd = 0;
    int attachStart = -1;
    int attachEnd = -1;
    int numAttachments = 0;
    while (pos < oldBodyLen) {
        // Find "\nb"
        char ch = oldBodyPtr[pos];
        ++pos;
        if (ch == '\n') {
            ch = oldBodyPtr[pos];
            if (ch == 'b') {
                int b = MatchesBeginPattern(oldBodyPtr, pos, oldBodyLen);
                if (b) {
                    textEnd = pos;
                    attachStart = pos;
                    int pos1 = FindEndPattern(oldBodyPtr, pos, oldBodyLen);
                    if (pos1 > 0) {
                        pos = pos1;
                        attachEnd = pos;
                        String textFragment = oldBodyContent.substr(
                            textStart, textEnd-textStart);
                        textContent += textFragment;
                        String attachAscii = oldBodyContent.substr(
                            attachStart, attachEnd-attachStart);
                        Uuencode decoder;
                        decoder.setAsciiChars(attachAscii);
                        decoder.decode();
                        const String& attachBinary = decoder.binaryChars();
                        String filename = decoder.fileName();
                        AddAttachment(body, attachBinary, filename);
                        ++numAttachments;
                        textStart = pos;
                        textEnd = pos;
                    }
                    else {
                        // Couldn't find "end" pattern, so let's quit here
                        break;
                    }
                }
            }
        }
    }
    textEnd = pos;
    

    // Only change the original if an attachment was found

    if (numAttachments > 0) {

        // Update the text content

        String textFragment = 
            oldBodyContent.substr(textStart, textEnd-textStart);
        textContent += textFragment;
        body.bodyPartAt(0).body().setString(textContent);

        // Change message type to multipart/mixed

        aEntity.headers().contentType().setType("MULTIPART");
        aEntity.headers().contentType().setSubtype("MIXED");
        aEntity.headers().contentType().createBoundary();

        // Replace old body with new body

        aEntity.body() = body;
    }
}


static void DataUuencodeToMime(Entity& aEntity)
{
    Headers& headers = aEntity.headers();
    Body& body = aEntity.body();

    // If we have a content type other than text or message, then look at
    // the content-transfer-encoding.  If it's x-uuencode, then convert to
    // base64.

    if (headers.hasField("Content-Transfer-Encoding")) {
        const String& cte = headers.contentTransferEncoding().type();
        if (Strcasecmp(cte, "x-uuencode") == 0) {

            // Decode the uuencoded content

            String ascii = body.getString();
            Uuencode decoder;
            decoder.setAsciiChars(ascii);
            decoder.decode();
            const String& binary = decoder.binaryChars();
            String filename = decoder.fileName();

            // Encode as base64 content

            Base64Encoder encoder;
            encoder.setOutputCrLf(true);
            ascii = encoder.encode(binary);
            body.setString(ascii);
            headers.contentTransferEncoding().
                fromEnum(TransferEncodingType::BASE64);
            headers.contentType().setName(filename);
        }
    }
}


static void UuencodeToMimeImpl(Entity& aEntity)
{
    Headers& headers = aEntity.headers();
    Body& body = aEntity.body();
    MediaType::MType type = MediaType::TEXT;
    if (headers.hasField("Content-Type")) {
        type = headers.contentType().typeAsEnum();
    }
    if (type == MediaType::TEXT) {
        TextUuencodeToMime(aEntity);
    }
    else if (type == MediaType::MESSAGE) {
        if (Strcasecmp(headers.contentType().subtype(), "Rfc822") == 0) {
            Body& body = aEntity.body();
            Message* msg = body.message();
            if (msg != NULL) {
                UuencodeToMimeImpl(*msg);
            }
        }
    }
    else if (type == MediaType::MULTIPART) {
        if (Strcasecmp(headers.contentType().subtype(), "Mixed") == 0) {
            int numParts = body.numBodyParts();
            for (int i=0; i < numParts; ++i) {
                BodyPart& part = body.bodyPartAt(i);
                UuencodeToMimeImpl(part);
            }
        }
    }
    else {
        DataUuencodeToMime(aEntity);
    }
}


void UuencodeToMime(Entity& aEntity)
{
    UuencodeToMimeImpl(aEntity);
    aEntity.assemble();
}


int main(int argc, char** argv)
{
    // Initialize the library

    mimepp::Initialize();

    mimepp::TextUtil::EOL_CHARS = "\r\n";

    // Read message from file

    if (argc < 2) {
        printf("Please specify a file name.\n");
        return 1;
    }
    FILE* in = fopen(argv[1], "rb");
    if (in == NULL) {
        printf("I can't open the file \"%s\".\n", argv[1]);
        return 1;
    }
    fseek(in, 0, SEEK_END);
    size_t fileLen = ftell(in);
    fseek(in, 0, SEEK_SET);
    String messageStr;
    messageStr.reserve(fileLen);
    char* buffer = (char*) malloc(8192);
    do {
        size_t count = fread(buffer, 1, 8192, in);
        messageStr.append(buffer, count);
    } while (! feof(in));
    free(buffer);
    fclose(in);
    in = 0;

    // Create a Message and parse it.

    Message msg(messageStr);
    msg.parse();

    UuencodeToMime(msg);

    FILE* out = fopen("uu-out.txt", "wb");
    fwrite(msg.getString().data(), 1, msg.getString().length(), out);
    fclose(out);
    out = 0;

    // Finalize the library

    mimepp::Finalize();

    return 0;
}
