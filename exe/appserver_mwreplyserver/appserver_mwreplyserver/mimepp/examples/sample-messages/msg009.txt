Return-Path: <jack.squat@hunnysoft.com>
Received: (from root@localhost)
	by hunnysoft.com (8.11.6/8.11.6) id j9I9lnT30196;
	Fri, 13 May 2005 10:29:49 -0500
Message-ID: <428497C2.9040606@hunnysoft.com>
Date: Fri, 13 May 2005 08:04:18 -0400
From: Jack Squat <jack.squat@hunnysoft.com>
User-Agent: Mozilla Thunderbird 1.0 (Windows/20041206)
X-Accept-Language: en-us, en
MIME-Version: 1.0
To: Sam Hill <sam.hill@hunnysoft.com>
Subject: Plain text + rich HTML text (A)
Content-Type: multipart/alternative;
 boundary="------------040307040604060508030702"

This is a multi-part message in MIME format.
--------------040307040604060508030702
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 7bit

Hi, friends!

I just this moment felt led to let you know that there is AN ABUNDANCE 
of lost and found in the main office.  Location is to your right under 
the blue sliding door cabinets as you enter the office.  There's a 
variety of items - school clothes, sweatshirts galore, adult and kid 
jackets and even a pair of nice, rather large slip-on shoes.  Come on in 
and check out the merchandise and spread the word as you're able.  A 
number of these things were found outside during the rain (the items are 
dry now!) on the picnic table, soccer field, bench by the main building 
and gazebo.  If you or someone you know has lost something, our school's 
lost and found "store" might be your answer!

--
Jack Squat


--------------040307040604060508030702
Content-Type: multipart/related;
 boundary="------------050607050100040104020802"


--------------050607050100040104020802
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta content=3D"text/html;charset=3DUTF-8" http-equiv=3D"Content-Type"=
>
</head>
<body bgcolor=3D"#ffffff" text=3D"#000000">
<font size=3D"-1">Hi, friends!<br>
<br>
I just this moment felt led to let you know that there is AN ABUNDANCE
of lost and found in the main office.=C2=A0 Location is to your right und=
er
the blue sliding door cabinets as you enter the office.=C2=A0 There's a
variety of items - school clothes, sweatshirts galore, adult and kid
jackets and even a pair of nice, rather large slip-on shoes.=C2=A0 Come o=
n
in and check out the merchandise and spread the word as you're able.=C2=A0=
 A
number of these things were found outside during the rain (the items
are dry now!) on the picnic table, soccer field, bench by the main
building and gazebo.=C2=A0 If you or someone you know has lost something,=

our school's lost and found "store" might be your answer!<br>

<br>
<img alt=3D"" src=3D"cid:part1.04040701.07020308@hunnysoft.com" height=3D"10"
 width=3D"400"><br>
<br>
<br>
<font color=3D"#993399">--<br>
Jack Squat<br>

</font><br>
</font>
</body>
</html>

--------------050607050100040104020802
Content-Type: image/gif;
 name="blueline.gif"
Content-Transfer-Encoding: base64
Content-ID: <part1.04040701.07020308@hunnysoft.com>
Content-Disposition: inline;
 filename="blueline.gif"

R0lGODlhkAEKALMAAAAAHAAAKgAAMwAAOgAAQgAASgAAUwAAWAAAZgAAthISxxQUyBgYyUVF
32Zm8nV1/SH+FUdJRiBTbWFydFNhdmVyIFZlcjEuMQAsAAAAAJABCgAABPkQjEmrvTjrzbv/
YCiOZGmeaKqubCklcCzPdG3feK7vfO//wKBwSCwaj8ik8CBILBjQqHRKrVqv2Kx2y+16v+Cw
eEwum8/o9HdhaCoa8Lh8Tq/b7/i8fs/v+/+AgYKDhIWGh4iJfwsFbg0OkJGSk5SVlpeYmZqb
nJ2en6ChoqOkpaanqKmdDQoEjg4PsbKztLW2t7i5uru8vb6/wMHCw8TFxsfIycq8DqwEAwlv
qtPU1dbX2Nna29zUrBNOiuLj5OXm5+jp6uvlrQROavHy8/T19vf4+fr0jAEI/wADChxIsKDB
gwgTKlzIsKHDhxAjSpxIsaLFixgjBgAAOw==
--------------050607050100040104020802--

--------------040307040604060508030702--




