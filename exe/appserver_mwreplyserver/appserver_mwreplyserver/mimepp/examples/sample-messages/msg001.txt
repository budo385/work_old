Return-Path: <jack.squat@hunnysoft.com>
Received: (from root@localhost)
	by hunnysoft.com (8.11.6/8.11.6) id j9I9lnT30196;
	Fri, 13 May 2005 10:29:49 -0500
Message-ID: <42848F56.80206@hunnysoft.com>
Date: Fri, 13 May 2005 07:28:22 -0400
From: Jack Squat <jack.squat@hunnysoft.com>
User-Agent: Mozilla Thunderbird 1.0 (Windows/20041206)
X-Accept-Language: en-us, en
MIME-Version: 1.0
To: Sam Hill <sam.hill@hunnysoft.com>
Subject: Plain text
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 7bit

Hi, friends!

I just this moment felt led to let you know that there is AN ABUNDANCE 
of lost and found in the main office.  Location is to your right under 
the blue sliding door cabinets as you enter the office.  There's a 
variety of items - school clothes, sweatshirts galore, adult and kid 
jackets and even a pair of nice, rather large slip-on shoes.  Come on in 
and check out the merchandise and spread the word as you're able.  A 
number of these things were found outside during the rain (the items are 
dry now!) on the picnic table, soccer field, bench by the main building 
and gazebo.  If you or someone you know has lost something, our school's 
lost and found "store" might be your answer!

--
Jack Squat

