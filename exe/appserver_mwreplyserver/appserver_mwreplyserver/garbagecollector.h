#ifndef GARBAGECOLLECTOR_H
#define GARBAGECOLLECTOR_H

#include <QObject>
#include "common/common/backgroundtaskinterface.h"
#include "trans/trans/httpserver.h"



class GarbageCollector : public QObject, public BackgroundTaskInterface 
{
	Q_OBJECT

public:
	
	GarbageCollector(HTTPServer	*pHttpServer);
    ~GarbageCollector();

	void ExecuteTask(int nCallerID=-1,int nTaskID=-1, QString strData="");


private:
	HTTPServer			*m_HttpServer; 

    
};

#endif // GARBAGECOLLECTOR_H
