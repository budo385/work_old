var def_mcBO__protocol = "http:";          
var def_mcBO__domain = "everxconnect.com";
var def_mcBO__port = "41566";
var def_mcBO__password = "";
var def_mcBO__everDOC_IP = "";

// Call this to register a reply server before using it
function RegisterReplyServer(protocol,domain,port)
{
	def_mcBO__protocol = protocol;
	def_mcBO__domain = domain;
	def_mcBO__port = port;
	InitReplyServer();
}
// Call this init instead of RegisterReplyServer() to use the default reply server
function InitReplyServer()
{
	boc_RegisterServer(def_mcBO__protocol,def_mcBO__domain,def_mcBO__port);
}


function mc_checknewemail(strEmail,callback)
{
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+strEmail+"</strEmail></PARAMETERS></REQUEST>"; 
	var strApiUrl = "/service/checknewemail";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
	
	/*
		PM note:
		currently api url for checknewemail is fixed: http://everxconnect:41565/rest/service/checknewemai, although
		docs say that inside appserver.ini there is section [EmailNotificator] where you can set adress for notificator settings. 
	*/
}
function cbMC_checknewemail(xml)
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	return(xmlobj.find("Ret_bNewMail").text());	
}

// Checks if an email address has already been registered on the Reply Server.
// Returns a "1" if it is, and "0" otherwise.
function mc_IsRegistered(email,callback) {
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+email+"</strEmail></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/isregistered";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_IsRegistered(xml){
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	var Ret_IsRegistered = xmlobj.find("Ret_IsRegistered").text();
	return(Ret_IsRegistered);
}

function mc_SendConfirmationEmail(email,callback) {
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+email+"</strEmail></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/sendconfirmationemail";
	mcBO_Call("POST",strApiUrl,callback,strPost);
}
function cbMC_SendConfirmationEmail(xml){
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	var Ret_strClientPassword = xmlobj.find("Ret_strClientPassword").text(); 	
	return (Ret_strClientPassword);
}

function mc_verifyID(email,callback) {
	var conce = getConce();
	var auth = getAuth(email,conce);
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+email+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken></PARAMETERS></REQUEST>"
	var strApiUrl = "/service/reademails";
	mcBO_Call("POST",strApiUrl,callback,strPost);
}
function cbMC_verifyID(xml) {
	if (mc_checkErrorCodeSilent() != 0) {   // Error
		return(false);
	} else {
		return(true);
	};
}

function mc_reademails(email,callback){
	var conce = getConce();
	var auth = getAuth(email,conce);
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+email+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken></PARAMETERS></REQUEST>"
	var strApiUrl = "/service/reademails";
	mcBO_Call("POST",strApiUrl,callback,strPost);
}
function cbMC_reademails(xml)
{
	
	if (mc_checkErrorCodeSilent() != 0) {   // Error
		return([]);
	} else {
		var xmlobj = $(xml);
		var node = null;
		var BEM_ID = null;
		var BEM_SUBJECT = null;
		var BEM_RECV_TIME = null;
		var BEM_ORIGINATOR_EMAIL = null;
		var BEM_ORIGINATOR_NAME = null;
		var emailList = [];
		
		xmlobj.find('email').each(function() {  	
			node = $(this);
			BEM_ID = node.find('BEM_ID').text();
			BEM_SUBJECT = xmlDecode(node.find('BEM_SUBJECT').text());

			BEM_RECV_TIME = xmlDecode(node.find('BEM_RECV_TIME').text());
			BEM_ORIGINATOR_EMAIL = xmlDecode(node.find('BEM_ORIGINATOR_EMAIL').text());
			BEM_ORIGINATOR_NAME = xmlDecode(node.find('BEM_ORIGINATOR_NAME').text());
			
			emailList.push({"subject": BEM_SUBJECT, 
							"id": BEM_ID, 
							"recv_time": BEM_RECV_TIME, 
							"originator_email": BEM_ORIGINATOR_EMAIL, 
							"originator_name": BEM_ORIGINATOR_NAME});
		});
		return(emailList); 
	};
}

function mc_getemail(email,id,callback)
{
	var conce = getConce();
	var auth = getAuth(email,conce);
	
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+email+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><nEmailID>"+id+"</nEmailID></PARAMETERS></REQUEST>"
	var strApiUrl = "/service/getemail";
	
	mcBO_Call("POST",strApiUrl,callback,strPost);
}

function cbMC_getemail(xml) {
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	
	var BEM_ID = null;


	var BEM_RECV_TIME = null; //2012-10-01T01:50:21Z
	var BEM_SUBJECT = null;
	var BEM_FROM = null; 	//eBay &lt;ebay@ebay.com&gt;
	var BEM_TO = null; 	//&lt;petar@helix-business-soft.hr&gt;
	var BEM_CC = null;
	var BEM_BCC = null;

	var BEM_BODY = null;

	var BEM_DAT_LAST_MODIFIED = null; //2012-10-12T14:17:51Z
	var BEM_FK_USER_ID = null; //1

	var node = null;
	var emailFields = [];
	
	// return(xmlDecode(xmlobj.find("EmailData email > BEM_BODY").text()));
	
	BEM_ID = xmlobj.find('EmailData email > BEM_ID').text();
	BEM_RECV_TIME = xmlDecode(xmlobj.find('EmailData email > BEM_RECV_TIME').text());
	BEM_SUBJECT = xmlDecode(xmlobj.find('EmailData email > BEM_SUBJECT').text());
	BEM_FROM = xmlDecode(xmlobj.find('EmailData email > BEM_FROM').text());
	BEM_TO = xmlDecode(xmlobj.find('EmailData email > BEM_TO').text());
	BEM_CC = xmlDecode(xmlobj.find('EmailData email > BEM_CC').text());
	BEM_BCC = xmlDecode(xmlobj.find('EmailData email > BEM_BCC').text());
	BEM_BODY = xmlDecode(xmlobj.find('EmailData email > BEM_BODY').text());
	
	emailFields.push({"id": BEM_ID, 
					"recv_time": BEM_RECV_TIME, 
					"subject": BEM_SUBJECT,
					"from": BEM_FROM, 
					"to": BEM_TO,
					"cc": BEM_CC, 
					"bcc": BEM_BCC,
					"body": BEM_BODY
					});

	return(emailFields); 
}

function mc_sendemail(from,to,subject,mimebody,mimefrom,cc,bcc,callback)
{
	var encMimeBody = xmlEncode(mimebody);
	var encMimeFrom = xmlEncode(mimefrom);
	var encSubject = xmlEncode(subject);
	var conce = getConce();
	var auth = getAuth(from,conce);

	var strPost = "<REQUEST><PARAMETERS><strEmail>"+from+"</strEmail><strMimeFrom>"+encMimeFrom+"</strMimeFrom>"+
				  "<strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
				  "<strTo>"+to+"</strTo><lstCC>"+cc+"</lstCC><lstBCC>"+bcc+"</lstBCC>"+
				  "<strSubject>"+encSubject+"</strSubject><strMimeBody>"+encMimeBody+"</strMimeBody></PARAMETERS></REQUEST>"
	var strApiUrl = "/service/sendemail";
	mcBO_Call("POST",strApiUrl,callback,strPost);	
}

function cbMC_sendemail(xml)
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	var Ret_ID = xmlobj.find("Ret_ID").text();
	return(Ret_ID);
}

function cb_checkemailstatus(email,id,callback)
{
	var conce = getConce();
	var auth = getAuth(email,conce);

	var strPost = "<REQUEST><PARAMETERS><strEmail>"+email+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><lstEmailTasks><row><ID>"+id+"</ID></row></lstEmailTasks></PARAMETERS></REQUEST>"
	var strApiUrl = "/service/checkemailstatus";	
	mcBO_Call("POST",strApiUrl,callback,strPost);	
}

function cbMC_checkemailstatus(xml)
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	var Status = xmlobj.find("Ret_lstEmailTasksStatus > Row Status").text();
	return(Status);
}

function cb_unregistersmtpaccount(email,callback)
{
	var conce = getConce();
	var auth = getAuth(email,conce);

	var strPost = "<REQUEST><PARAMETERS><strEmail>"+email+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken></PARAMETERS></REQUEST>"
	var strApiUrl = "/service/unregistersmtpaccount";
	mcBO_Call("POST",strApiUrl,callback,strPost);	
}

function cbMC_unregistersmtpaccount(xml)
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
}

function cb_registersmtpaccount(strEmail,txt_strServerAddress,txt_strBCCAddressForSentMailCopy,txt_nPort,txt_strUserName,txt_strPassword,callback)
{
	var conce = getConce();
	var auth = getAuth(strEmail,conce);
	
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+strEmail+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
	"<strServerAddress>"+txt_strServerAddress+"</strServerAddress><strUserName>"+txt_strUserName+"</strUserName><strPassword>"+txt_strPassword+"</strPassword>"+
	"<nPort>"+txt_nPort+"</nPort><strBCCAddressForSentMailCopy>"+txt_strBCCAddressForSentMailCopy+"</strBCCAddressForSentMailCopy></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/registersmtpaccount";
	mcBO_Call("POST",strApiUrl,callback,strPost);	
}

function cbMC_registersmtpaccount(xml)
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
}


function mc_checkErrorCode()
{
  if(mc_nLastBO_ErrorCode==0) {	mc_strLastBO_ErrorText = '' };
  return [[mc_nLastBO_ErrorCode],[mc_strLastBO_ErrorText]];
/*  if(mc_nLastBO_ErrorCode!=0) // error occured
  {	

 	if(mc_nLastBO_ErrorCode==1000){ 			
		alert("[login error]:"+mc_strLastBO_ErrorText);  // login error
	}
	else {
		 alert("[error]:"+mc_strLastBO_ErrorText);  // alert this general error
		 // location.reload();
	}
	return;
 
  }	*/
}

function mc_errorAlert(code,text) {
	alert('Error ['+code+']: '+text);
}

function mc_checkErrorCodeSilent()
{
	return mc_nLastBO_ErrorCode;
}

function mc_setPassword(pw) {
	def_mcBO__password = pw;
}

function getConce()
{
	return(SHA256(new Date().getTime().toString()));	
}
function getAuth(email,strConce)
{
	var SECRET_SPICE_PASSWORD="BANDIT";
	//var pass = $('#txt_Password').val();
	var pass = def_mcBO__password;
	
	//console.log(getFileName(window.location.pathname) + "|" + pass);
	if(pass === '' || !pass){
		// alert("No password!"); 
		return; 
	}
	else{
		return(SHA256(email+strConce+SECRET_SPICE_PASSWORD+pass));
	}
}



function mc_startMultiRegisterPeriod(strEmail)
{
	var conce = getConce();
	var auth = getAuth(strEmail,conce);
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+strEmail+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken></PARAMETERS></REQUEST>"                   
	var strApiUrl = "/service/startmultiregisterperiod";
	mcBO_Call("POST",strApiUrl,cbMC_startMultiRegisterPeriod ,strPost);
}

function cbMC_startMultiRegisterPeriod (xml,textStatus)
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	
	switch ("textStatus") {
		case "success": alert('You have 60 minutes to register the same email address on another device!'); break;
		case "undefined": alert("You are not allowed to use the same email address in this app on another device!"); break;
		default: alert("Error: "+textStatus);
	} 
}

function mc_readDataList(strEmail,nType,callback)
{
	var conce = getConce();
	var auth = getAuth(strEmail,conce);
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+strEmail+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><nType>"+nType+"</nType></PARAMETERS></REQUEST>"                   
	var strApiUrl = "/service/readdatalist";
	mcBO_Call("POST",strApiUrl,callback ,strPost);
}

function cbMC_readDataList(xml)    
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	var node = null;
	var CT_TYPE = null;
	var CT_NAME = null;
	var CT_DAT_LAST_MODIFIED = null;
	var dataList = [];
	
	xmlobj.find('Ret_List row').each(function() {  	
		node = $(this);
		CT_TYPE = node.find('CT_TYPE').text();
		CT_DAT_LAST_MODIFIED = node.find('CT_DAT_LAST_MODIFIED').text();
		CT_NAME = xmlDecode(node.find('CT_NAME').text());
		dataList.push({"type": CT_TYPE, "name": CT_NAME, "modified": CT_DAT_LAST_MODIFIED});
	});
	return(dataList); 
}

var gDMName = '';
function mc_getDataModification(strEmail,nType,strName,callback)
{
	gDMName = strName;
	var conce = getConce();
	var auth = getAuth(strEmail,conce);
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+strEmail+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><nType>"+nType+"</nType></PARAMETERS></REQUEST>"                   
	var strApiUrl = "/service/readdatalist";
	mcBO_Call("POST",strApiUrl,callback ,strPost);
}
function cbMC_getDataModification(xml)    
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	var node = null;
	var CT_NAME = null;
	var CT_DAT_LAST_MODIFIED = null;
	var mod = '';
	var name = '';
	
	xmlobj.find('Ret_List row').each(function() {  	
		node = $(this);
		name = xmlDecode(node.find('CT_NAME').text());
		if (name == gDMName) {
			mod = node.find('CT_DAT_LAST_MODIFIED').text();
		};
	});
	return(mod); 
}
function mc_getData(strEmail,nType,strName,callback)
{
	var conce = getConce();
	var auth = getAuth(strEmail,conce);
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+strEmail+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><nType>"+nType+"</nType><strName>"+strName+"</strName></PARAMETERS></REQUEST>"                   
	var strApiUrl = "/service/getdata";
	mcBO_Call("POST",strApiUrl,callback ,strPost);
}

function cbMC_getData(xml)   
{
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	var xmlobj = $(xml);
	var Data = xmlDecode(xmlobj.find("Ret_strData").text());
	return (Data);
}
function mc_storeData(strEmail,nType,strName,strData,callback)
{

	var conce = getConce();
	var auth = getAuth(strEmail,conce);
	strData = xmlEncode(strData);
	var strPost = "<REQUEST><PARAMETERS><strEmail>"+strEmail+"</strEmail><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><nType>"+nType+"</nType><strName>"+strName+"</strName><strData>"+strData+"</strData></PARAMETERS></REQUEST>"                   
	var strApiUrl = "/service/storedata";
	mcBO_Call("POST",strApiUrl,callback ,strPost);
}
function cbMC_storeData(xml)    
{
	var err = mc_checkErrorCode();
	// if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	return err;
	// var xmlobj = $(xml);
}


//---------------------------------------------------------------------------------
//	everDOCS API: login(server ID, user, pass), logout, get catalogs, read catalogs
//---------------------------------------------------------------------------------
// OS DETECTION (PLATFORM) - Save & Get
function getPlatform()
{  
  var agent  =  navigator.appVersion.toLowerCase();  
  var OS = "Unknown OS"; 		  // indicates OS detection failure  

  if ( agent.indexOf("linux")  !=-1 )  OS = "linux";	// all versions of Linux
  if ( agent.indexOf("x11")    !=-1 )  OS = "unix";		// all other UNIX flavors 
  if ( agent.indexOf("win")    !=-1 )  OS = "windows"; 	// all versions of Windows
  if ( agent.indexOf("mac")    !=-1 )  OS = "macos";	// all versions of Macintosh OS	
  if ( agent.indexOf('iphone') !=-1 || agent.indexOf('ipod')!=-1) OS = "iphone"; // all versions of iPhone/iPod
  if ( agent.indexOf('ipad')   !=-1 )  OS = "ipad";		// all versions of iPad MacOS 

  return OS; 
}

/*
	Resolve IP based on ID -> Tarantula
*/
function mc_everDOC_ResolveServerIP(strServerID, callback)
{
 // requirements / config
	var PFH_program_code 				= 3;
	var PFH_program_version_major 		= 1;
	var PFH_program_version_minor 		= 0;
	var PFH_program_version_revision 	= 2;
	var PFH_program_platform_name 		= getPlatform();
	var PFH_program_platform_UID 		= "";
	var PFH_client_identity 			= (localStorage.wa_clientId) ? localStorage.wa_clientId : "";
	var PFH_host_identity 				= strServerID;
	var PFH_host_URL 					= "";
	var PFH_host_port 					= "";
	var PFH_host_stats_nat_used 		= "";
	var PFH_host_local_ip 				= "";
	var COOKIE_EXPIRE_MIN				= 20;
	var FFH_CONNECT_SERVER_TIMEOUT 		= 10*1000;
	var FFH_CONNECT_SERVER_MASTER_IP 	= "http://www.everxconnect.com:11000";
    var FFH_CONNECT_SERVER_LIST			= "http://www.everdocs.com/everxconnect.txt"
	
	// post
	strPost=
	'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS>'+
	'<PROGRAM_CODE>'+PFH_program_code+'</PROGRAM_CODE>'+
	'<PROGRAM_VERSION_MAJOR>'+PFH_program_version_major+'</PROGRAM_VERSION_MAJOR>'+
	'<PROGRAM_VERSION_MINOR>'+PFH_program_version_minor+'</PROGRAM_VERSION_MINOR>'+
	'<PROGRAM_VERSION_REVISION>'+PFH_program_version_revision+'</PROGRAM_VERSION_REVISION>'+
	'<PROGRAM_PLATFORM_NAME>'+PFH_program_platform_name+'</PROGRAM_PLATFORM_NAME>'+
	'<PROGRAM_PLATFORM_UID>'+PFH_program_platform_UID+'</PROGRAM_PLATFORM_UID>'+
	'<CLIENT_IDENTITY xsi:nil="true"/><HOST_IDENTITY>'+PFH_host_identity+'</HOST_IDENTITY>'+
	'<HOST_URL>'+PFH_host_URL+'</HOST_URL><HOST_PORT>'+PFH_host_port+'</HOST_PORT>'+
	'<HOST_STATS_NAT_USED>'+PFH_host_stats_nat_used+'</HOST_STATS_NAT_USED>'+
	'<HOST_SSL></HOST_SSL>'+
	'<HOST_LOCAL_IP>'+PFH_host_stats_nat_used+'</HOST_LOCAL_IP></PARAMETERS></REQUEST>';
	
	// ajax call
	mcBO_everDOCCall("POST",FFH_CONNECT_SERVER_MASTER_IP+"/rest/service/register",callback,strPost);
}

/*
	Returns false if server ip resolve failed, else true and IP is stored in local var
*/
function cbMC_everDOC_ResolveServerIP(xml)    
{
	if(mc_nLastBO_ErrorCode!=0)
	{ 
		alert("[error]:"+mc_strLastBO_ErrorText);  // alert this general error
		return false;
	}
		
	var xdom 	 = $(xml);
	var code  	 = xmlDecode(xdom.find("STATUS_CODE").text());
	var text  	 = xmlDecode(xdom.find("STATUS_TEXT").text());
	var url  	 = xmlDecode(xdom.find("HOST_URL").text());
	var port  	 = xmlDecode(xdom.find("HOST_PORT").text());
	var url_lan  = xmlDecode(xdom.find("HOST_LOCAL_IP").text());
	var clientID = xmlDecode(xdom.find("CLIENT_IDENTITY").text());
	var hostSSL  = xmlDecode(xdom.find("HOST_SSL").text());
	
//	var prot 	 = document.location.protocol+"//";
	var prot = "http://";
	if(hostSSL === "1")
	{
		prot = "https://";
	}
	
	if(code==0)
	{
		def_mcBO__everDOC_IP=prot+url+":"+port;
		
		if(window.location.hostname != '')
		{
			if(window.location.hostname == url_lan) //if this document origins from LAN IP then this means that LAN IP is accessbile:		
			{
				def_mcBO__everDOC_IP=prot+url_lan+":"+port;
			}
		}
		
		return true;
	}
	else
	{
		alert(text);
		return false;
	}
}


/*
	Login to everDOC server
*/

function mc_everDOC_Login(strUserName, strPassword, callback)
{
	var PLATFORM = getPlatform();
	var PROGRAM_VERSION_MAJOR	  		= 1;
	var PROGRAM_VERSION_MINOR 	  		= 0;
	var PROGRAM_VERSION_REVISION  		= 2;
	var PROGRAM_VERSION 		  		= PROGRAM_VERSION_MAJOR+"."+PROGRAM_VERSION_MINOR+"."+PROGRAM_VERSION_REVISION;
	var DIGIT_APP						= '11'; 								
	var DIGIT_PLATFORM = (PLATFORM=='iphone'||PLATFORM=='ipad')? '11' : '10';  	
	var DIGIT_STATUS					= '0'; 								
	var BIT_ADDON						= '000';								
	var PROGRAM_CODE 			  		= DIGIT_APP+DIGIT_PLATFORM+DIGIT_STATUS+BIT_ADDON;

	var PFH_program_code 			= PROGRAM_CODE;
	var PFH_program_version 		= PROGRAM_VERSION;
	var PFH_program_platform_name 	= PLATFORM;
	var PFH_program_platform_UID 	= "";
	var PFH_client_identity 		= (localStorage.wa_clientId) ? localStorage.wa_clientId : '';
	
	// ajax call
	mcBO_everDOCLogin(def_mcBO__everDOC_IP+"/rest/service/login",strUserName, strPassword, callback,PFH_program_code,PFH_program_version,PFH_client_identity,PFH_program_platform_name);
}

/*
	Returns false if login failed
*/
function cbMC_everDOC_Login(xml)    
{
  if(mc_nLastBO_ErrorCode!=0) // error occured
  {									 			
	if(mc_nLastBO_ErrorCode==1000){ 			
		alert("[login error]:"+mc_strLastBO_ErrorText);  // login error
	}
	else {
		 alert("[error]:"+mc_strLastBO_ErrorText);  // alert this general error
		 // location.reload();  MB: Never!
	}
	return false;
  }	
  return true;
}


/*
	Function return at once....true if logged, else false
*/
function mc_everDOC_IsLogged()
{
	return mcBO_everDOCIsLogged();
}


/*
	Logout from everDOC server, no callback, no check status
*/
function mc_everDOC_Logout()
{
	// ajax call
	mcBO_everDOCLogout(def_mcBO__everDOC_IP+"/rest/service/logout");
}


/*
	Get Catalogs
*/
function mc_everDOC_GetCatalogs(callback)
{
	// ajax call
	mcBO_everDOCCall("GET",def_mcBO__everDOC_IP+"/rest/service/catalogs",callback,"");
}

/*
	Returns list of catalogs
	<Catalogs>
		<catalog><ALIAS,CNT,THUMB,RIGHTS></catalog>
*/
function cbMC_everDOC_GetCatalogs(xml)    
{
	// checkErrorCode();
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	
	var xmlobj = $(xml);
	var node = null;
	var ALIAS = "";
	var CNT = 0;
	var THUMB = null;
	var RIGHTS = 0;
	var catList = [];
	
	xmlobj.find('catalog').each(function() {  	
		node = $(this);
		ALIAS = xmlDecode(node.find('ALIAS').text());
		CNT = parseInt(xmlDecode(node.find('CNT').text()));
		THUMB = xmlDecode(node.find('THUMB').text());
		catList.push({"ALIAS": ALIAS, "CNT": CNT, "THUMB": THUMB});
	});
	return(catList); 
}


/*
	Read Catalog from strAliasDirectory
	default for all param is -1, 
	nSortOrder =0 ->by name, else by extension
*/
function mc_everDOC_ReadCatalog(strAliasDirectory,nFromN,nToN,nSortOrder,callback)
{
	// post
	strPost=
	'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS>'+
	'<strAliasDirectory>'+strAliasDirectory+'</strAliasDirectory>'+
	'<nFromN>'+nFromN+'</nFromN>'+
	'<nToN>'+nToN+'</nToN>'+
	'<nFromN_SubFolder>0</nFromN_SubFolder>'+
	'<nToN_SubFolder>-1</nToN_SubFolder>'+
	'<nSortOrder>'+nSortOrder+'</nSortOrder>'+
	'</PARAMETERS></REQUEST>';
	
	// ajax call
	mcBO_everDOCCall("POST",def_mcBO__everDOC_IP+"/rest/service/catalog/read",callback,strPost);
}

/*
	Returns list, content of catalog:
	<TOTAL_CNT><DOC_LINK><DISPLAYNAME>
	
	DOC_LINK is apsolute url...
	
	When IS_FOLDER=1 then DOC_LINK is name of Catalog -> use ReadCatalog again to read inside of it....
*/
function cbMC_everDOC_ReadCatalog(xml)    
{
	// checkErrorCode();
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
		
	var xmlobj = $(xml);
	var node = null;
	var TOTAL_CNT = parseInt(xmlobj.find("nTotalCount").text());
	var TOTAL_CNT_SUBFOLDER = parseInt(xmlobj.find("nTotalCount_SubFolder").text());
	var DOC_LINK = "";
	var DISPLAYNAME = "";
	var SIZE = 0;
	var fileList = [];
	
	var strSession="?web_session="+getCookie('web_session_everDocs');
	
	xmlobj.find('file').each(function() {  	
		node = $(this);
		DOC_LINK = def_mcBO__everDOC_IP+"/USER_STORAGE/"+xmlDecode(node.find('DOC_LINK').text())+strSession;
		DISPLAYNAME = xmlDecode(node.find('DISPLAYNAME').text());
		SIZE = node.find('SIZE_STRING').text();
		fileList.push({"IS_FOLDER": 0, "TOTAL_CNT": TOTAL_CNT, "TOTAL_CNT_SUBFOLDER": TOTAL_CNT_SUBFOLDER, "DOC_LINK": DOC_LINK, "DISPLAYNAME": DISPLAYNAME, "SIZE": SIZE});
	});
	
	
	xmlobj.find('folder').each(function() {  	
		node = $(this);
		DOC_LINK = xmlDecode(node.find('ALIAS').text());
		DISPLAYNAME = xmlDecode(node.find('NAME').text());
		fileList.push({"IS_FOLDER": 1, "TOTAL_CNT": TOTAL_CNT, "TOTAL_CNT_SUBFOLDER": TOTAL_CNT_SUBFOLDER, "DOC_LINK": DOC_LINK, "DISPLAYNAME": DISPLAYNAME, "SIZE": ""});
	});
	
	return(fileList); 
}


/*
	Get File as base64 text variable....if >30Mb then error
*/
function mc_everDOC_GetFile(strLink, isZipped, callback)
{
	// post
	strPost=
	'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS>'+
	'<strFileURL>'+encodeURIComponent(strLink)+'</strFileURL>'+
	'<bIsZip>'+isZipped+'</bIsZip>'+
	'</PARAMETERS></REQUEST>';
	
	// ajax call
	mcBO_everDOCCall("POST",def_mcBO__everDOC_IP+"/rest/service/document/download",callback,strPost);
}

/*
	Get File Content (base64 encoded)
*/
function cbMC_everDOC_GetFile(xml)    
{
	// checkErrorCode();
	var err = mc_checkErrorCode();
	if (err[0] != 0) {mc_errorAlert(err[0],err[1]);};
	
	var xmlobj = $(xml);
	var  data = xmlobj.find("Ret_byteFileData").text(); //base64 encoded

	return(data); 
}
