// REST Config
var boc_protocol 			= document.location.protocol;
var boc_hostname			= document.location.hostname;
var boc_port 				= document.location.port;					
var boc_ROOT_URL 			= boc_protocol+'//'+boc_hostname+":"+boc_port;
var boc_REST_WEB_ROOT_URL 	= boc_ROOT_URL+'/rest';
var boc_REST_WEB_DOCS_URL 	= boc_ROOT_URL+"/user_storage/";
var boc_REST_LOGIN 			= boc_REST_WEB_ROOT_URL+"/service/login";
var boc_REST_LOGOUT 		= boc_REST_WEB_ROOT_URL+"/service/logout";
var boc_COOKIE_EXPIRE_MIN	= 20;


// Generally, when POST is used, utf8 encoding is needed when sending strings to appserver (č,ć,š,đ ¨, ^, ,,,)
var Utf8={encode:function(string){string=string.replace(/\r\n/g,"\n");var utftext="";for(var n=0;n<string.length;n++){var c=string.charCodeAt(n);if (c<128) {utftext+=String.fromCharCode(c);}else if((c>127)&&(c<2048)){utftext+=String.fromCharCode((c>>6)|192);utftext+=String.fromCharCode((c&63)|128);}else{utftext+=String.fromCharCode((c>>12)|224);utftext+=String.fromCharCode(((c>>6)&63)|128);utftext+=String.fromCharCode((c&63)|128);}}return utftext;},decode:function(utftext){var string="";var i=0;var c=c1=c2=0;while(i<utftext.length){c=utftext.charCodeAt(i);if(c<128){string+=String.fromCharCode(c);i++;}else if((c>191)&&(c<224)){c2=utftext.charCodeAt(i+1);string+=String.fromCharCode(((c&31)<<6)|(c2&63));i+=2;}else{c2=utftext.charCodeAt(i+1);c3=utftext.charCodeAt(i+2);string+=String.fromCharCode(((c&15)<<12)|((c2&63)<<6)|(c3&63));i+=3;}}return string;}}	

// get current filename 
function getFileName(str) {
	// get the full url
	var url = (str!='') ? str : document.location.href;
		//console.log("getFileName_str = "+url);
	// removes the anchor at the end, if any
	url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
	// removes the query after the file name, if any
	url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
	// removes everything before the last slash in the path
	url = url.substring(url.lastIndexOf("/") + 1, url.length);
	
return url;
} 

function boc_RegisterServer(protocol,host,port) {
	
	boc_ROOT_URL 			= protocol+'//'+host+":"+port;
	boc_REST_WEB_ROOT_URL 	= boc_ROOT_URL+'/rest';
	boc_REST_WEB_DOCS_URL 	= boc_ROOT_URL+"/user_storage/";
	boc_REST_LOGIN 			= boc_REST_WEB_ROOT_URL+"/service/login";
	boc_REST_LOGOUT 		= boc_REST_WEB_ROOT_URL+"/service/logout";	
}

function resetWebRootUrl2Local()
{
	var boc_protocol 			= document.location.protocol;
	var boc_hostname			= document.location.hostname;
	var boc_port 				= document.location.port;					
	//var boc_ROOT_URL 			= boc_protocol+'//'+boc_hostname+":"+boc_port;
	var boc_ROOT_URL 			= "http://localhost:41566";
	var boc_REST_WEB_ROOT_URL 	= boc_ROOT_URL+'/rest';
	return(boc_REST_WEB_ROOT_URL);
}



	
	  