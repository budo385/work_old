

//-----------------------------------------------
//	INTERNAL DB & CACHE STRUCTURE
//-----------------------------------------------

webdb = {};
webdb.db_handler = null;
webdb.db_type = 0 /* 0 - localstorage, 1 - webSQL, 2 - IndexedDB, 4- FileSystem  */

webdb.callBackOnInitSucess = null;
webdb.cache_list_name = new Array();
webdb.cache_list_data = new Array();

//File Ops:
webdb.callBackFileWrite = null;   //params: (relative path+filename,content)
webdb.callBackFileRead = null;    //params: (relative path+filename,callBackGetResults)
webdb.callBackFileDelete = null;  //params: (relative path+filename)
webdb.nCurrentListIdx = 0;
webdb.strTempLoadVar = ""; 		//this param is global for assembling chunks when
webdb.strTempLoadVarCnt = 0; 		//this param is global for assembling chunks when

//callBackGetFileReadResults = function(strArrayVal) 

//initialize DB:
webdb.initialize = function(callBackOnInitEnd) 
{
	//already initilaized:
	if(webdb.db_handler != null) 
	{
		callBackOnInitEnd();
		return;
	}
	
	//create DB or open to existing db, create table or use existing..
	if(webdb.db_type==0) //localstorage
	{
		callBackOnInitEnd(); //just call it:
		return;
	}
	else if(webdb.db_type==1) //webSQL
	{
	
		  var dbSize = 40 * 1024 * 1024; // 40MB
		  webdb.db_handler = openDatabase("MW_DATA4A", "1.0", "Mailegant Client Database", dbSize);
		  webdb.db_handler.transaction(function(tx) 
		  {
			tx.executeSql("CREATE TABLE IF NOT EXISTS MW_DATA4A(ID INTEGER PRIMARY KEY ASC, LIST TEXT, NAME TEXT, VALUE TEXT, ITEM_POSITION INTEGER)", [],null);
			tx.executeSql("CREATE INDEX IF NOT EXISTS MW_DATA4A_IDX1 ON MW_DATA4A(LIST)", [],null);
			tx.executeSql("CREATE INDEX IF NOT EXISTS MW_DATA4A_IDX2 ON MW_DATA4A(ITEM_POSITION)", [],callBackOnInitEnd);
		  });
	}
	else if(webdb.db_type==2) //indexedDB
	{
		
		window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
		window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
		window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
		if (!window.indexedDB) 
		{
			window.alert("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.")
		}
		var db;
		var request = window.indexedDB.open("MW_DATA7",1);
		request.onerror = function(event) 
		{
			//alert("Error!!");
			alert("Open indexedDB database error: " + event.target.errorCode);
		};
		request.onsuccess = function(event) {
		  webdb.db_handler = request.result;
	  	  webdb.db_handler.onerror = function(event) {alert("Database error: " + event.target.errorCode);};
		  
		  //read data from database into local cache:
		  webdb.initCallBackFirst();
		};
		
		//create objects: only one
		request.onupgradeneeded = function(event) 
		{
		  var db = event.target.result;
		  var objectStore = db.createObjectStore("LISTS", { keyPath: "LIST" }); //object pair: <LIST, ARRAY>
		};

	}
	else if(webdb.db_type==3) //file ops:
	{
		callBackOnInitEnd(); //just call it:
		return;
	}
	else
	{
		alert("Database not supported!");
	}
}

//after db connection and table creation load all data to local cache
webdb.initCallBackFirst = function(){

	if(webdb.db_type==0) //localstorage
	{
			for(var i=0;i<webdb.cache_list_name.length;i++)
			{
				//get list from memory
				myArr = new Array();
				var strArrayVal = localStorage.getItem(webdb.cache_list_name[i]);
				if(strArrayVal != null)
					myArr = jQuery.parseJSON(strArrayVal);
										
				//decode it from uri:
				for(var k=0;k<myArr.length;k++)
					myArr[k] = [decodeURIComponent(myArr[k][0]),decodeURIComponent(myArr[k][1])];

				webdb.cache_list_data[i]=myArr;
			}
			webdb.callBackOnInitSucess(); //call callback funct..
	}	
	else if(webdb.db_type==1) //webSQL
	{

		webdb.db_handler.transaction(function(tx) {
		tx.executeSql("SELECT * FROM MW_DATA4A ORDER BY LIST ASC, ITEM_POSITION ASC", [], function parseResult(tx, rs) 
		{
			var strCurrentListName = "";
			var nMaxPos = 1;
			var nCurrListIdx=0;
			for(var i=0;i<rs.rows.length;i++)
			{
				var strListName = rs.rows.item(i).LIST;
				
				//store data row, except list name itself
				var DataRow = new Array();
				DataRow[0]=decodeURIComponent(rs.rows.item(i).NAME);
				DataRow[1]=decodeURIComponent(rs.rows.item(i).VALUE);
	
				if(strListName != strCurrentListName)
				{
				  nCurrListIdx = webdb.cache_list_name.indexOf(strListName);
				  strCurrentListName = strListName;
				}
				var myArr = webdb.cache_list_data[nCurrListIdx];
				if(myArr == null)
					myArr = new Array();
				myArr.push(DataRow);
				webdb.cache_list_data[nCurrListIdx] = myArr;
			}
			
			webdb.callBackOnInitSucess(); //call callback funct..
			
		});
		});
	}
	else if(webdb.db_type==2) //indexedDB:
	{
		var objectStore = webdb.db_handler.transaction("LISTS").objectStore("LISTS");
		objectStore.openCursor().onsuccess = function(event)
		{
		  var cursor = event.target.result;
		  if (cursor) 
		  {
		  	var nCurrListIdx = webdb.cache_list_name.indexOf(cursor.key);
			if(nCurrListIdx>=0)
			{
				webdb.cache_list_data[nCurrListIdx] = cursor.value.LIST_DATA; //store array object directly 
			}
			cursor.continue();
		  }
		  else 
		  {
			webdb.callBackOnInitSucess(); //call callback funct..
		  }
		};
		objectStore.openCursor().onerror = function(event){alert("Database error: " + event.target.errorCode);};
	}
	else if(webdb.db_type==3) //file ops:
	{
		//alert("initCallBackFirst started");
		webdb.nCurrentListIdx = 0;
		if(webdb.cache_list_name.length>0)
		{
			webdb.strTempLoadVar="";
			webdb.strTempLoadVarCnt=0;
			//get list from memory
			if(webdb.callBackFileRead)
			{
				//alert("first callBackFileRead fired: "+"mw/"+webdb.cache_list_name[0]+" "+ webdb.callBackGetFileReadResults);
				webdb.callBackFileRead("mw/"+webdb.cache_list_name[0],webdb.callBackGetFileReadResults,0,20000);
			}
		}
	}

}

webdb.callBackGetFileReadResults = function(endOfFile, strArrayVal) 
{
	
	if(endOfFile==0)
	{
		webdb.strTempLoadVar += strArrayVal;
		alert("We read bytes in buffer: "+webdb.strTempLoadVar.length);
		webdb.strTempLoadVarCnt++;
		var start=webdb.strTempLoadVarCnt*20000;
		var end=start+20000;
		//alert("We are reading next bytes: " +start+" end: "+end);
		webdb.callBackFileRead("mw/"+webdb.cache_list_name[webdb.nCurrentListIdx],webdb.callBackGetFileReadResults,start,end);
		return;
	}
	else
	{
		webdb.strTempLoadVar += strArrayVal;
		if(webdb.strTempLoadVarCnt>0)
			strArrayVal = webdb.strTempLoadVar;
		webdb.strTempLoadVar="";
		webdb.strTempLoadVarCnt=0;
		//alert("We read bytes:"+strArrayVal.length);
	}
		

	//alert("callBackGetFileReadResults for started: "+strArrayVal);
	myArr = new Array();
	if(strArrayVal != null)
		if(strArrayVal.length != 0)
			myArr = jQuery.parseJSON(strArrayVal);
							
	//decode it from uri:
	for(var k=0;k<myArr.length;k++)
		myArr[k] = [decodeURIComponent(myArr[k][0]),decodeURIComponent(myArr[k][1])];

	webdb.cache_list_data[webdb.nCurrentListIdx]=myArr;
	webdb.nCurrentListIdx++;
	
	
		
	if(webdb.nCurrentListIdx == webdb.cache_list_name.length)
	{
		webdb.callBackOnInitSucess(); //call callback funct..
		//alert("callBackGetFileReadResults callBackOnInitSucess: "+strArrayVal+ " finished");
	}
	else
	{
		webdb.callBackFileRead("mw/"+webdb.cache_list_name[webdb.nCurrentListIdx],webdb.callBackGetFileReadResults,0,20000);
		//alert("callBackGetFileReadResults callBackFileRead: "+strArrayVal+" finished, with new position: "+webdb.nCurrentListIdx);
	}
	
}



//SQL alert handler
webdb.onError = function(tx, e) {
  alert("There has been an SQL error: " + e.message);
}
// Retrieve stored array from localStorage.itemListName (if empty declare Array() object) without decoding its items
webdb.getArray = function getArray(strListName)
{
	var myArr = webdb.cache_list_data[webdb.cache_list_name.indexOf(strListName)];
	if(myArr == null){
		myArr = new Array();
	}	
    return(myArr);
}
// Store an array in localStorage.itemListName  **MB**
webdb.setArray = function setArray(strListName,itemArray)
{
	webdb.cache_list_data[webdb.cache_list_name.indexOf(strListName)] = itemArray;
	if(webdb.db_type==0) //localstorage: save it right away in DB:
	{
		//encode it from uri:
		var myArr=new Array();
		for(var k=0;k<itemArray.length;k++)
			myArr.push( [encodeURIComponent(itemArray[k][0]),encodeURIComponent(itemArray[k][1])]);

		//stringify:
		var strArrayAsString = JSON.stringify(myArr);
		
		try 
		{
			localStorage.setItem(strListName, strArrayAsString);
		} catch (e) 
		{
			if (e.code == 22) {
			alert('ERROR: Memory full. You can not store additional data!');
			}
		};
	}
	else if(webdb.db_type==2) //indexedDB
	{
		const customerData = [
		  { LIST: strListName, LIST_DATA: itemArray}
		];
		
		var objectStore = webdb.db_handler.transaction("LISTS", "readwrite").objectStore("LISTS");
		var request = objectStore.put(customerData[0]);
		//var request = objectStore.add(customerData[0]);
		request.onsuccess = function(event)
		{
			//alert("Added OK!");
		};
		request.onerror  = function(event){
			alert("IndexedDB Database error: " + event.target.errorCode);
		};
	}
	else if(webdb.db_type==3)
	{
		//encode it from uri:
		var myArr=new Array();
		for(var k=0;k<itemArray.length;k++)
			myArr.push( [encodeURIComponent(itemArray[k][0]),encodeURIComponent(itemArray[k][1])]);
		//stringify:
		var strArrayAsString = JSON.stringify(myArr);
		if(webdb.callBackFileWrite)
			webdb.callBackFileWrite("mw/"+strListName,strArrayAsString);
	}

}	


//add item
webdb.SQLAddItem = function (itemListName,itemName,itemValue,itemPosition) {
	
	if(webdb.db_type==1) 
		webdb.db_handler.transaction(function(tx) 
		{
			tx.executeSql("INSERT INTO MW_DATA4A (LIST, NAME, VALUE, ITEM_POSITION) VALUES(?,?,?,?)", [itemListName, itemName, itemValue,itemPosition], null, webdb.onError);
		});
} 

//update positions
webdb.SQLUpdatePositionIncAfter = function (itemListName, itemPosition) {
	
	if(webdb.db_type==1) 
		webdb.db_handler.transaction(function(tx) 
		{
			tx.executeSql("UPDATE MW_DATA4A SET ITEM_POSITION=ITEM_POSITION+1 WHERE ITEM_POSITION >= ? AND LIST = ?", [itemPosition,itemListName], null, webdb.onError);
		});
} 
webdb.SQLUpdatePositionDecAfter = function (itemListName, itemPosition) {
	
	if(webdb.db_type==1) 
		webdb.db_handler.transaction(function(tx) 
		{
			tx.executeSql("UPDATE MW_DATA4A SET ITEM_POSITION=ITEM_POSITION-1 WHERE ITEM_POSITION >= ? AND LIST = ?", [itemPosition,itemListName], null, webdb.onError);
		});
} 

//delete item by pos
webdb.SQLDeleteByPosition = function (itemListName, itemPosition) {
	
	if(webdb.db_type==1) 
		webdb.db_handler.transaction(function(tx) 
		{
			tx.executeSql("DELETE FROM MW_DATA4A WHERE ITEM_POSITION = ? AND LIST = ?", [itemPosition,itemListName], null, webdb.onError);
		});
} 

webdb.SQLUpdateNewPosition = function (itemListName,itemOldPosition,itemNewPosition) {

	if(webdb.db_type==1) 	
		webdb.db_handler.transaction(function(tx) 
		{
			tx.executeSql("UPDATE MW_DATA4A SET ITEM_POSITION=? WHERE ITEM_POSITION = ? AND LIST = ?", [itemNewPosition,itemOldPosition,itemListName], null, webdb.onError);
		});
} 







//-----------------------------------------------
//	LIST ITEM INTERFACE
//-----------------------------------------------

// alocates spaces for new lists in cache
function il_InitItemList(itemListName) 
{   
	webdb.cache_list_name.push(itemListName);
}

// init database: create tables if not and load them into cache
//nDbType =  0 - localstorage, 1 - webSQL, 2 - IndexedDB, 3- FileOps 
function il_Init(nDbType, callBackFunc) 
{   
	webdb.db_type = nDbType;
	webdb.callBackOnInitSucess = callBackFunc;
	webdb.initialize(webdb.initCallBackFirst);
}

//call first if you want to use FileOps
function il_SetFileOpsCallBacks(callBackFileWrite,callBackFileRead,callBackFileDelete) 
{
	webdb.callBackFileWrite = callBackFileWrite;   
	webdb.callBackFileRead = callBackFileRead;    
	webdb.callBackFileDelete = callBackFileDelete; 
}

// inserts an item with the name itemName and the value itemValue on the position itemPosition in the item list itemListName (array.splice()). 
// If itemPosition is omitted (null), the item is appended to the end (--> array.push()).
function il_AddItem(itemListName,itemName,itemValue,itemPosition) 
{
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
	
	var myArr = webdb.getArray(itemListName);
	if(!itemPosition || itemPosition==0) //position is undefined: use maximum
	{
		var nMaxPos = myArr.length; 
		myArr.push([itemName,itemValue]);
		webdb.SQLAddItem (itemListName,encodeURIComponent(itemName),encodeURIComponent(itemValue),nMaxPos); //into DB they go encoded
	}
	else
	{
		myArr.splice(itemPosition,0,[itemName,itemValue]);
		webdb.SQLUpdatePositionIncAfter(itemListName,itemPosition);
		webdb.SQLAddItem (itemListName,encodeURIComponent(itemName),encodeURIComponent(itemValue),itemPosition); 
	}	
	webdb.setArray(itemListName,myArr);
}

// Removes the item on position itemPosition from the item list itemListName.
function il_DeleteItem(itemListName,itemPosition) 
{  
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
		
	var myArr = webdb.getArray(itemListName);	
	myArr.splice(itemPosition, 1);	
	webdb.setArray(itemListName,myArr);
	webdb.SQLDeleteByPosition(itemListName,itemPosition);
	webdb.SQLUpdatePositionDecAfter(itemListName,itemPosition);
}
 
// returns itemValue from the item on position itemPosition from the item list itemListName. 
function il_GetItem(itemListName,itemPosition) 
{  
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
 
	var myArr = webdb.getArray(itemListName);	
	return (myArr[itemPosition][1]);
}
 
// searches for an item with the name itemName in the item list itemListName. 
// If found, itemPosition is returned, -1 otherwise.
function il_FindItemPos(itemListName,itemName) 
{
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
		
	var myArr = webdb.getArray(itemListName);
	for(var i=myArr.length-1;i>=0;i--)
	{   
		if(myArr[i][0] == itemName)
			return (i);
	};
	
	return (-1);
}

// searches for an item with the name itemName in the item list itemListName. 
// If found, itemValue is returned,  an empty string otherwise. 
function il_FindItemValue(itemListName,itemName) 
{  
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return "";
 
	var myArr = webdb.getArray(itemListName);	
	for(var i=myArr.length-1;i>=0;i--)
	{   
		if(myArr[i][0] == itemName)
			return (myArr[i][1]);
	};
	
	return "";
}
 
// Moves the item on position itemOldPosition to the position itemNewPosition in the item list itemListName.
function il_MoveItem(itemListName,itemOldPosition,itemNewPosition) {  
 
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
 
	var myArr = webdb.getArray(itemListName);
	var myArrRow = myArr.splice(itemOldPosition,1);
	myArr.splice(itemNewPosition,0,myArrRow[0]);	
	
	webdb.setArray(itemListName,myArr);
	webdb.SQLUpdateNewPosition(itemListName,itemOldPosition,-2000); //temporarly move to undefined
	webdb.SQLUpdateNewPosition(itemListName,itemNewPosition,itemOldPosition); //update new with old
	webdb.SQLUpdateNewPosition(itemListName,-2000,itemNewPosition); //finnally old with new...
}


 
// returns one string with all itemName entries from the item list itemListName.
// Each itemName is prefixed by itemPrefix and postfixed by itemPostfix (if defined).
function il_GetItemListNames(itemListName,itemPrefix,itemPostfix)
{  
	var strOutput="";
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
		
	var myArr = webdb.getArray(itemListName);
	for(var i=0;i<myArr.length;i++)
	{
     	strOutput += itemPrefix + myArr[i][0]+ itemPostfix;
   	} 
	
	return(strOutput);	
}

// returns one string with all itemValues entries from the item list itemListName.
// Each itemValue is prefixed by itemPrefix and postfixed by itemPostfix (if defined).
function il_GetItemListValues(itemListName,itemPrefix,itemPostfix) 
{  
	var strOutput ="";
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
		
	var myArr = webdb.getArray(itemListName);
	for(var i=0;i<myArr.length;i++)
	{
     	strOutput += itemPrefix + myArr[i][1]+ itemPostfix;
   	} 
	
	return(strOutput);		
}
 
// returns one string with all itemName and itemValue entries from the list itemListName in the following form: 
// {itemPrefix}itemName{itemMidfix}itemValue{itemPostfix}
function il_GetItemListFull(itemListName,itemPrefix,itemMidfix,itemPostfix)
{  
	var itemListFull = "";
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
		
	var myArr = webdb.getArray(itemListName);
	for(var i=0;i<myArr.length;i++)
	{
     	itemListFull += itemPrefix + myArr[i][0] + itemMidfix + myArr[i][1]+ itemPostfix;
   	} 
	
	return(itemListFull);

}
 
// returns the array defined as itemListName .
function il_GetItemListArray(itemListName) 
{  	
	var nIdx=webdb.cache_list_name.indexOf(itemListName); //not alive
	if(nIdx==-1)
		return;
	
	return (webdb.getArray(itemListName));
}
 

// Delete all items of a given item list:
function il_DeleteItemList(itemListName) 
{
  var p = 0;
  var a = il_GetItemListArray(itemListName);
  var l = a.length;
  for (var i = l-1; i>=0; i--) {    
   p = il_FindItemPos(itemListName,a[i][0]);
   if (p >= 0) {il_DeleteItem(itemListName,p);};
  };
}

// Delete an item by name:
function il_DeleteItemByName(itemListName,itemName) 
{
  var p = il_FindItemPos(itemListName,itemName);
  if (p >= 0) {il_DeleteItem(itemListName,p);};
}
	
	