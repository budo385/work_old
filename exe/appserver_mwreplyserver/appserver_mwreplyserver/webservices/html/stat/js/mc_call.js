//--------------------------------------------------------------------
//				BUSINESS CALL INTERFACE
//--------------------------------------------------------------------
//All functions are async calls
//Return status is stored in variables: mc_nLastBO_ErrorCode & mc_strLastBO_ErrorText
//Return data is pass to the call back function. call back func must check errorcode to determine if call was OK
//mc_nLastBO_ErrorCode == 0 : call ok, !=0 : error detected. mc_nLastBO_ErrorCode == 1000 -> login failed.

//FUNCTION INTERFACE:
//1. mcBO_Call(strHttpMethod,strURL,pfCallBack,dataToSend);  
//2. mcBO_Login(strURL,user,pass,pfCallBack);

var mc_nLastBO_ErrorCode=0;
var mc_strLastBO_ErrorText='';

var mc_functCallBack_Login=null;
var mc_functCallBack_BOCall=null;

//--------------------------------------------------------------------
//				BUSINESS CALL HANDLER
//--------------------------------------------------------------------
// Makes async mcBO call, calls pfCallBack function when finish with returned data
// mc_nLastBO_ErrorCode and mc_strLastBO_ErrorText contain last error: if mc_nLastBO_ErrorCode <> 0 then error
// Automatically uses prviously stored username/password for re-authentication, if Login failed mc_nLastBO_ErrorCode=1000



//strHttpMethod - GET or other HTTP method
//strURL 		- relative URL to boc_REST_WEB_ROOT_URL
//pfCallBack 	- function called when call is finished, function must accept data string result and test mc_nLastBO_ErrorCode to see if call succeed
//data 			- request xml body, leave empty if no request body needed
function mcBO_Call(strHttpMethod,strURL,pfCallBack,dataToSend)
{
    mc_functCallBack_BOCall=pfCallBack;
	strURL = boc_REST_WEB_ROOT_URL+strURL;
	
	if(dataToSend.length>0)
	{
		
	 $.ajax({		   
		   type: strHttpMethod,
		   url: strURL,
		   dataType: "xml",
		   processData: 0,
		   data: dataToSend,	
   		   success: mcBO_Call_success,
		   error: mcBO_Call_error
		});	
	
	}
	else
	{
	 $.ajax({		   
		   type: strHttpMethod,
		   url: strURL,
		   dataType: "xml",
		   success: mcBO_Call_success,
		   error: mcBO_Call_error
		});
	}
}

function mcBO_Call_success(data, textStatus, jqXHR) 
{
	mc_functCallBack_BOCall(data, textStatus, jqXHR);
}


function mcBO_Call_error(xhr, textStatus, errorThrown) 
{
	mcBO_ParseError(xhr,textStatus);
	mc_functCallBack_BOCall(textStatus);
}


//--------------------------------------------------------------------
//				LOGIN HANDLER
//--------------------------------------------------------------------
// Logins on the server, makes async call, calls pfCallBack function when finish
// mc_nLastBO_ErrorCode and mc_strLastBO_ErrorText contain last error: if mc_nLastBO_ErrorCode <> 0 then error
// if Login failed mc_nLastBO_ErrorCode=1000
// pfCallBack receives http body data if any


//user		 	- username
//pass 			- password
//pfCallBack 	- function called when call is finished, function must accept data string result and test mc_nLastBO_ErrorCode to see if call succeed, if mc_nLastBO_ErrorCode=1000 -> login failed

function mcBO_Login(user,pass,pfCallBack,nClientType,strProgVer) 
{
	deleteCookie('web_session');
	var strXMLRequest=mcBO_GenerateLoginRequest(user,pass,nClientType,strProgVer);
	mc_functCallBack_Login=pfCallBack;
		
	 $.ajax({		   
		   type: "POST",
		   url: boc_REST_LOGIN,
		   processData: 0,
		   dataType: "xml",
		   data: strXMLRequest,
		   success: mcBO_OnLogin_success,
		   error: mcBO_OnLogin_error
		});
}

function mcBO_IsLogged() 
{
	var strSession=getCookie('web_session');
	if(strSession.length==0)
		return false;
	else
		return true;
}


function mcBO_LogOut() 
{
	var strSession=getCookie('web_session');
	var strXMLRequest=mcBO_GenerateLogoutRequest(strSession);
	 $.ajax({		   
	   type: "POST",
	   url: boc_REST_LOGOUT,
	   processData: 0,
	   dataType: "xml",
	   data: strXMLRequest,
	});
	deleteCookie('web_session');
}


function mcBO_OnLogin_success(data, textStatus, jqXHR)
{
	var strSession=mcBO_ParseLoginResponse(data);
	if(data.length==0){
		mc_nLastBO_ErrorCode=1000;
		mc_strLastBO_ErrorText="Login failed! Invalid username or password!";
	}
	else{
		mc_nLastBO_ErrorCode=0;
		mc_strLastBO_ErrorText='';
	}
	setCookie('web_session', strSession, COOKIE_EXPIRE_MIN);
	if (mc_functCallBack_Login!=null)
		mc_functCallBack_Login(data, textStatus, jqXHR);		
}

function mcBO_OnLogin_error(xhr, textStatus, errorThrown) 
{
	mcBO_ParseError(xhr,textStatus);
	deleteCookie('web_session');
	if (mc_functCallBack_Login!=null)
		mc_functCallBack_Login(textStatus);
}


function mcBO_GenerateLoginRequest(user,pass,nClientType,strProgVer)
{
	var exdate=new Date();
	var strClientNonce=user+":"+exdate.toLocaleTimeString();
	strClientNonce=SHA256(strClientNonce);
	
	var bytePassHash=user+":"+pass;
	bytePassHash=SHA256(bytePassHash);
	var authToken=user+":"+bytePassHash+":"+strClientNonce;
	authToken=SHA256(authToken);
	
	var strBody ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strUserName>"+user+"</strUserName>";
	strBody +="<strAuthToken>"+authToken+"</strAuthToken>";
	strBody +="<strClientNonce>"+strClientNonce+"</strClientNonce>";
	strBody +="<nClientType>"+nClientType+"</nClientType>";
	strBody +="<strProgVer>"+strProgVer+"</strProgVer>";

	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";
	
	return strBody;
}


function mcBO_ParseError(xhr,textStatus) 
{
	if(xhr.status == 0 && xhr.responseText.length == 0)
	{
		mc_nLastBO_ErrorCode=1;	
		mc_strLastBO_ErrorText="Error connecting to the server";
		return;
	}


	if(xhr.status == 401)
	{
		mc_nLastBO_ErrorCode=1000;
		mc_strLastBO_ErrorText="Login failed! Invalid username or password!";
	}
	else
	{
		if(mcBO_ParseErrorBody(xhr.responseText)==0)
		{
			mc_nLastBO_ErrorCode=1;	//general error: can be timeout or something:
			
			if(textStatus.length!=0)
				mc_strLastBO_ErrorText=textStatus;
			else
				mc_strLastBO_ErrorText=xhr.responseText.substr(0,1000); //only first 1000 chars
				
			if(mc_strLastBO_ErrorText.length == 0)
				mc_strLastBO_ErrorText="Unknown Error";
		}
	}
}

function mcBO_ParseErrorBody(xml_body) 
{
	if(xml_body.length == 0)
		return 0;
	mc_nLastBO_ErrorCode=$(xml_body).find("error_code").text();
	mc_strLastBO_ErrorText=$(xml_body).find("error_text").text();
	if (mc_nLastBO_ErrorCode>0)
		return 1;
	else
		return 0;
}

function mcBO_ParseLoginResponse(xml_body) 
{

	if(xml_body.length == 0)
		return "";
		
	var nContactID = xmlDecode($(xml_body).find("nContactID").text());
	var nPersonID = xmlDecode($(xml_body).find("nPersonID").text());
	
	setCookie('nContactID',nContactID);
	setCookie('nPersonID',nPersonID);
			
	return xmlDecode($(xml_body).find("Session").text());
}




function mcBO_GenerateLogoutRequest(strSessionID)
{
	var strBody ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strSessionID>"+strSessionID+"</strSessionID>";

	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";
	
	return strBody;
}




//--------------------------------------------------------------------
//				XML DECODER:
//--------------------------------------------------------------------

//------------------BASE64 EN/DECODER------------------------------------
//use functions from base64.js:
//$.base64Encode("I'm Persian.");
//$.base64Decode("SSdtIFBlcnNpYW4u");


//------------------STRING EN/DECODER------------------------------------

var AMP = '&';
var rawEntity = [ "&",      "<",    ">",     "\'",     "\""];
var xmlEntity = [ "&amp;", "&lt;", "&gt;",  "&apos;", "&quot;"];

//xml- string from server to decode
//returns decoded string
function xmlDecode(xml)
{
	// check if no AMP there is no need to decode
	var pos=xml.indexOf(AMP);
	if(pos<0) return xml;

	var byteDecoded=xml;

	// iterate through list backwards: its very important to pars &amp last
	
	byteDecoded = byteDecoded.replace(/&quot;/gi,"\"");
	byteDecoded = byteDecoded.replace(/&apos;/gi,"\'");
	byteDecoded = byteDecoded.replace(/&gt;/gi,">");
	byteDecoded = byteDecoded.replace(/&lt;/gi,"<");
	byteDecoded = byteDecoded.replace(/&amp;/gi,"&");
	
	return byteDecoded;
}

//raw- string to encode for sending to the server
//returns encoded string
function xmlEncode(raw)
{

	var byteDecoded=raw;
	
	byteDecoded = byteDecoded.replace(/&/gi,"&amp;");
	byteDecoded = byteDecoded.replace(/"/gi,"&quot;");
	byteDecoded = byteDecoded.replace(/'/gi,"&apos;");
	byteDecoded = byteDecoded.replace(/>/gi,"&gt;");
	byteDecoded = byteDecoded.replace(/</gi,"&lt;");

	return byteDecoded;
	
}


//------------------DATE EN/DECODER------------------------------------

//dateString in YYYY-MM-DDTHH:MM:SSZ

function dateTimeDecode(dateString) 
{
	if(dateString.length === 0) 
	{
		return "";
	}
	

	var nT_Idx=dateString.indexOf("T");
	if(nT_Idx===-1) 
	{
	   return "";
	}
	   
	var strDatePart=dateString.substr(0,nT_Idx);
	var strDatePartArray = strDatePart.split("-");
	if(strDatePartArray.length!=3) 
	{
		return "";
	}
	
	var strTimePart=dateString.substr(nT_Idx+1,8);
	var strTimePartArray = strTimePart.split(":");
	
	if(strTimePartArray.length!=3) 
	{ 
		return "";
	}

	var d =  new Date();
	

	d.setUTCFullYear(strDatePartArray[0], strDatePartArray[1]-1, strDatePartArray[2]);
	d.setUTCHours(strTimePartArray[0], strTimePartArray[1], strTimePartArray[2],0);
			
	return d;
	
}





//-----------------------STORAGE OPTIONS----------------------------

function setItem(c_name,value)
{
	//sessionStorage.setItem(c_name,value);
	setSessionkey(c_name,value); 
}

function getItem(c_name)
{
	return sessionStorage.getItem(c_name);
}


function setCookie(c_name,value,expire_min)
{
	var exdate=new Date();
	if(expire_min!=null)
	{
		exdate.setMinutes(exdate.getMinutes()+expire_min);
	}
	document.cookie=c_name+ "=" +escape(value)+
	((expire_min==null) ? "" : ";expires="+exdate.toUTCString())+';path=/'; 
}

function getCookie(c_name)
{
if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(c_name + "=");
  if (c_start!=-1)
    {
    c_start=c_start + c_name.length+1;
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
    }
  }
return "";
}


function deleteCookie(c_name) 
{
	document.cookie=c_name+ "= ;expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

