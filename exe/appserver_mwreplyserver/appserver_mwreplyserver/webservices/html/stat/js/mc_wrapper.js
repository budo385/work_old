var def_mcBO__protocol = "https:";
var def_mcBO__domain = "everxconnect.com";
var def_mcBO__port = "41566";

// Call this to register a reply server before using it
function RegisterReplyServer(protocol,domain,port)
{
	def_mcBO__protocol = protocol;
	def_mcBO__domain = domain;
	def_mcBO__port = port;
	InitReplyServer();
}
// Call this init instead of RegisterReplyServer() to use the default reply server
function InitReplyServer()
{
	boc_RegisterServer(def_mcBO__protocol,def_mcBO__domain,def_mcBO__port);
}

function mc_getstats(callback) {
	
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/getstats";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}

function cbMC_getstats(xml){
	checkErrorCode();
	var xmlobj = $(xml);
	var Ret_nRegisteredUsers= xmlobj.find("Ret_nRegisteredUsers").text();
	var Ret_nRegisteredUsersInLast7days= xmlobj.find("Ret_nRegisteredUsersInLast7days").text();
	var Ret_nUsersPendingConfirmation= xmlobj.find("Ret_nUsersPendingConfirmation").text();
	var Ret_nPaid= xmlobj.find("Ret_nPaid").text();
	var Ret_nPaidExpired= xmlobj.find("Ret_nPaidExpired").text();
	var Ret_nTest= xmlobj.find("Ret_nTest").text();
	var Ret_nTestExpired= xmlobj.find("Ret_nTestExpired").text();
	
	var statsList = {};
	statsList['nRegisteredUsers'] = Ret_nRegisteredUsers;
	statsList['nRegisteredUsersInLast7days'] = Ret_nRegisteredUsersInLast7days;
	statsList['nUsersPendingConfirmation'] = Ret_nUsersPendingConfirmation;
	statsList['nPaid'] = Ret_nPaid;	
	statsList['nPaidExpired'] = Ret_nPaidExpired;	
	statsList['nTest'] = Ret_nTest;	
	statsList['nTestExpired'] = Ret_nTestExpired;	
	
	return(statsList); 
}

function mc_getusers(callback) 
{
	var conce = getConce();
	var auth = getAuth(conce);
	
	var nSortOrder = "0"; // B.T.: Marin requested 2014-03-07 to sort by alphabet
	
	/*
	nSortOrder
		0 -by USR_EMAIL
		1- by USR_DAT_LAST_MODIFIED
		2- by STATUS, USR_EMAIL
	*/
	
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
				  "<nSortOrder>"+nSortOrder+"</nSortOrder><nFromN>-1</nFromN><nToN>-1</nToN></PARAMETERS></REQUEST>";
	
	var strApiUrl = "/service/getusers";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_getusers(xml)
{
	checkErrorCode();
	
	var xmlobj = $(xml);
	var node = null;
	
	var USR_ID							=null;
	var USR_EMAIL						=null;
	var USR_DAT_FIRST_LOGIN_STR			=null;
	var USR_DAT_LAST_LOGIN_STR			=null;
	var USR_DAT_LAST_RECEIPT_CHECK_STR	=null;
	var SUBSCRIPTION_STATUS				=null;
	var DAYS_LEFT						=null;
	var USR_ALLOW_LOGIN					=null;
	var STATUS							=null;
	var ESR_HOST_ADDRESS				=null;
	var ESR_CNT_MAILS					=null;
	var SUBSCRIPTION_STATUS_STR			=null;
	
	
	
	var usersList = [];
	var Ret_nTotalRegisteredUsers = xmlobj.find("Ret_nTotalRegisteredUsers").text();
	
	xmlobj.find('Ret_lstRegisteredEmails > row').each(function(index, value) 
	{  
		node = $(this);
		STATUS = node.find('STATUS').text();
		USR_EMAIL = xmlDecode(node.find('USR_EMAIL').text());
		USR_DAT_FIRST_LOGIN_STR = xmlDecode(node.find('USR_DAT_FIRST_LOGIN_STR').text());
		USR_DAT_LAST_LOGIN_STR = node.find('USR_DAT_LAST_LOGIN_STR').text();
		USR_DAT_LAST_RECEIPT_CHECK_STR = node.find('USR_DAT_LAST_RECEIPT_CHECK_STR').text();
		SUBSCRIPTION_STATUS = node.find('SUBSCRIPTION_STATUS').text();
		DAYS_LEFT = node.find('DAYS_LEFT').text();
		USR_ALLOW_LOGIN = node.find('USR_ALLOW_LOGIN').text();
		ESR_HOST_ADDRESS = node.find('ESR_HOST_ADDRESS').text();
		ESR_CNT_MAILS = node.find('ES_EMAIL_CNT').text();
		SUBSCRIPTION_STATUS_STR = node.find('SUBSCRIPTION_STATUS_STR').text();
		
		usersList.push({
			status: STATUS,
			usermail: USR_EMAIL,
			first_login: USR_DAT_FIRST_LOGIN_STR,
			last_login: USR_DAT_LAST_LOGIN_STR,
			last_receipt_check: USR_DAT_LAST_RECEIPT_CHECK_STR,
			subscription_status: SUBSCRIPTION_STATUS,
			days_left: DAYS_LEFT,
			allow_login: USR_ALLOW_LOGIN,
			host: ESR_HOST_ADDRESS,
			cnt_es_mails: ESR_CNT_MAILS,
			subscription_status_str: SUBSCRIPTION_STATUS_STR
		});
	});
	
	return(usersList);
}

function mc_deleteUser(callback,strEmail){
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><strEmail>"+strEmail+"</strEmail></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/deleteuser";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_deleteUser(xml){
	checkErrorCode();
}


function mc_setLoginAccessUser(callback,strEmail, nLevel){
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><strEmail>"+strEmail+"</strEmail><nLevel>"+nLevel+"</nLevel></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/setusersubscriptionaccess";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_setLoginAccessUser(xml){
	checkErrorCode();
}

function mc_sendAccessEmails(callback,strEmails){
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><strEmails>"+strEmails+"</strEmails></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/writeaccessemails";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_sendAccessEmails(xml){
	checkErrorCode();
	//console.log(xml);
	alert("Operation Done!");
}

function mc_deleteAccessEmails(callback,delEmail){
	var conce = getConce();
	var auth = getAuth(conce);
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken><strEmail>"+delEmail+"</strEmail></PARAMETERS></REQUEST>";
	var strApiUrl = "/service/deleteaccessemail ";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_deleteAccessEmails(xml){
	checkErrorCode();
	//console.log(xml);
	alert("Operation Done!");
}

function getConce()
{
	return(SHA256(new Date().getTime().toString()));	
}
function getAuth(strConce) // note: different from clientlib
{	
	var SECRET_SPICE_PASSWORD="BANDIT";
	var strUsername = sessionStorage.strUser;
	var strPassword = sessionStorage.strPass;
	
	return(SHA256(strUsername+strConce+SECRET_SPICE_PASSWORD+strPassword));
}
function getUserStatus(nStatus)
{
	nStatus =parseInt(nStatus);
	
	/*
	STATUS: Prepared (imported, but not yet registered), 
			Registered (the user has registered, but not yet confirmed), 
			Re-Registerd (the user has re-registered, but not yet confirmed),
			Confirmed (valid and active)
	*/
	
	var strStatus = "";
	switch(nStatus)
	{
	case 0: strStatus = "prepared"; 
		break;
	case 1:	strStatus = "registered"; 
		break;
	case 2: strStatus = "re-registerd"; 
		break;
	case 3:	strStatus = "confirmed "; 
	  break;
	default:
	  break;
	}	
	return strStatus;
}
function checkErrorCode()
{
  if(mc_nLastBO_ErrorCode!=0) // error occured
  {									 			
	if(mc_nLastBO_ErrorCode==1000){ 			
		alert("[login error]:"+mc_strLastBO_ErrorText);  // login error
		
		sessionStorage.clear();
		localStorage.clear();
		document.location="index.html";
	}
	else {
		 alert("[error]:"+mc_strLastBO_ErrorText);  // alert this general error
	}
	return;
  }	
}


//-----------------------------------------------
//		ES servers API: add, delete, etc..
//-----------------------------------------------

function mc_getMWES_Servers(callback) 
{
	var conce = getConce();
	var auth = getAuth(conce);
	
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
				  "</PARAMETERS></REQUEST>";
	
	var strApiUrl = "/service/geteshosts";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_getMWES_Servers(xml)
{
	checkErrorCode();
	
	var xmlobj = $(xml);
	var node = null;
	
	var ESR_HOST_ADDRESS			=null;
	var CNT							=null;
	
	var serverList = [];
	var Ret_lstESHosts = xmlobj.find("Ret_lstESHosts").text();
	
	xmlobj.find('Ret_lstESHosts > row').each(function(index, value) 
	{  
		node = $(this);
		ESR_HOST_ADDRESS = node.find('ESR_HOST_ADDRESS').text();
		CNT = xmlDecode(node.find('CNT').text());

		serverList.push({
			host: ESR_HOST_ADDRESS,
			cnt: CNT
		});
	});
	
	return(serverList);
}


function mc_deleteMWES_Server(callback,strESHost) 
{
	var conce = getConce();
	var auth = getAuth(conce);
	
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
				  "<strESHost>"+strESHost+"</strESHost></PARAMETERS></REQUEST>";
	
	var strApiUrl = "/service/deleteeshost";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_deleteMWES_Server(xml)
{
	checkErrorCode();
}

function mc_addMWES_Server(callback,strESHost) 
{
	var conce = getConce();
	var auth = getAuth(conce);
	
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
				  "<strESHost>"+strESHost+"</strESHost></PARAMETERS></REQUEST>";
	
	var strApiUrl = "/service/addeshost";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_addMWES_Server(xml)
{
	checkErrorCode();
}

function mc_addEmail2Server(callback,strEmail) 
{
	var conce = getConce();
	var auth = getAuth(conce);
	
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
				  "<strEmail>"+strEmail+"</strEmail></PARAMETERS></REQUEST>";
	
	var strApiUrl = "/service/registereshost2singleuser";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_addEmail2Server(xml)
{
	checkErrorCode();
}

function mc_registerMWES_Server2Users(callback) 
{
	var conce = getConce();
	var auth = getAuth(conce);
	
	var strPost = "<REQUEST><PARAMETERS><strConce>"+conce+"</strConce><strAuthToken>"+auth+"</strAuthToken>"+
				  "</PARAMETERS></REQUEST>";
	
	var strApiUrl = "/service/registereshost2users";
	mcBO_Call("POST",strApiUrl,callback,strPost); 
}
function cbMC_mc_registerMWES_Server2Users(xml)
{
	checkErrorCode();
}


