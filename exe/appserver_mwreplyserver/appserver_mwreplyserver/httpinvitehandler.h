#ifndef HTTPINVITEHANDLER_H
#define HTTPINVITEHANDLER_H

#include "trans/trans/httphandler.h"

class HttpInviteHandler : public HTTPHandler
{

public:
	HttpInviteHandler();
	~HttpInviteHandler();

	void HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);

private:
	void GenerateErrorResponse(HTTPResponse &response,QString strError);
	void GenerateSuccessResponse(HTTPResponse &response,QString strEmail,bool bReconfirm);
	void RegisterUserToESHost(Status &pStatus, int nESRID, QString strUser, QString strPass);
	void ChangePassUserToESHost(Status &pStatus, int nESRID, QString strUser, QString strPass);
	int	 FindESHost(QString &strHostIP);
};

#endif // HTTPINVITEHANDLER_H
