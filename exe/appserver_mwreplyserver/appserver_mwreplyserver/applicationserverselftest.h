#ifndef APPLICATIONSERVERSELFTEST_H
#define APPLICATIONSERVERSELFTEST_H

#include "common/common/status.h"

class ApplicationServerSelfTest
{
public:
	ApplicationServerSelfTest();
	~ApplicationServerSelfTest();

	void AddUserAccountsForTest();

private:
	void	CreateSingleUser(Status &pStatus, QString strEmail, QString strPass, bool &bSkipLB, int nIdx );
	QString ConvertNumValue(int nValue);

};

#endif // APPLICATIONSERVERSELFTEST_H
