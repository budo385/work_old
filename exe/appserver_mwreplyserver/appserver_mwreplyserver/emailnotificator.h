#ifndef EMAILNOTIFICATOR_H
#define EMAILNOTIFICATOR_H

#include <QReadWriteLock>
#include <QStringList>

#include "common/common/backgroundtaskinterface.h"
#include "common/common/status.h"
#include "common/common/dbrecordset.h"




class EmailNotificator : public QObject, public BackgroundTaskInterface
{
	Q_OBJECT

public:
	EmailNotificator();
	~EmailNotificator();

	void		ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");					
	void		ScheduleNewEmailNotification(const QStringList &lstUsersWithNewEmails); 


private:
	
	QStringList				m_lstUsersWithNewEmail;
	QReadWriteLock			m_Lock;
	QMutex					m_Mutex;
};

#endif // EMAILNOTIFICATOR_H
