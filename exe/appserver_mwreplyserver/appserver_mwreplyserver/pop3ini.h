#ifndef POP3INI_H
#define POP3INI_H

#include "common/common/inifile.h"
#include <QFileInfo>



/*!
	\class ServerIniFile
	\brief INI file reader for app. server
	\ingroup AppServer

	Application server INI file reader


*/
class Pop3Ini
{
public:
	Pop3Ini();
	~Pop3Ini();

	bool Load(QString strFile, bool bCreateIfNotExists=true);
	bool Save(QString strFile);

public:

	QString m_strConnectionName;
	QString m_strServerName;
	int		m_nIsPop;
	int		m_nActive;
	//int		m_nFreqSeconds;
	//QString m_strLastScanned;
	QString m_strEmailAddress;
	int		m_nPort;
	QString	m_strSSL;
	QString m_strUserName;
	QString m_strPassword;

	
protected:
	IniFile m_objIni;
	bool CheckValues();
	bool SaveDefaults(QString strFile);

};

#endif //Pop3Ini_H