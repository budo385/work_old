#include "httpinvitehandler.h"
#include "trans/trans/config_trans.h"
#include "common/common/datetimehandler.h"
#include "common/common/config_version_ffh.h"
#include "db/db/dbsqlquery.h"
#include "servicehandler.h"
#include "common/common/logger.h"
#include "common/common/datahelper.h"
#include "trans/trans/resthttpclient.h"
#include "common/common/threadid.h"

extern DbSqlManager	*g_DbManager;
extern Logger g_Logger;


#define SERVER_INTERSERVER_COMM_USERNAME "system"
#define SERVER_INTERSERVER_COMM_PASSWORD "balrog10"


HttpInviteHandler::HttpInviteHandler()
	: HTTPHandler()
{
	SetDomain("registration");
}

HttpInviteHandler::~HttpInviteHandler()
{
}

//confirmation is like: http://everxconnect.com/registration/confirm/(confirm string percent encoded)
void HttpInviteHandler::HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration handler: START. Thread: %1").arg(QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()));

	//--------------------------------------------------------
	//	check request, extract confirmatation string
	//--------------------------------------------------------
	QString strPath=request.path();
	QStringList lstParts=strPath.split("/",QString::SkipEmptyParts);

	if (lstParts.size()!=3 || !strPath.contains("/confirm/"))
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration server: Error - Invalid Request. Path: %1").arg(strPath));
		GenerateErrorResponse(response,"Invalid Request");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		return;
	}

	QString strConfirmString=QUrl::fromPercentEncoding(lstParts.at(lstParts.size()-1).toLatin1());

	//--------------------------------------------------------
	//Get user record if such exists:
	//--------------------------------------------------------
	Status pStatus;
	DbSqlQuery query(pStatus, g_DbManager);
	if (!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration server: Database Error: %1").arg(pStatus.getErrorText()));
		GenerateErrorResponse(response,"Database error, please try again!");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		return; 
	}

	//read database version from table, if <> then execute organize database....
	query.Execute(pStatus,"SELECT USR_ID,USR_IS_CONFIRMED,USR_EMAIL,USR_RECONFIRM_PASSWORD,ESR_ID,USR_PASSWORD FROM USER_DATA LEFT OUTER JOIN EMAIL_SERVERS ON ESR_ID=USR_EMAIL_SERVER_ID WHERE USR_CONFIRM_STRING = '"+strConfirmString+"'");
	if (!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration server: Database Error: %1").arg(pStatus.getErrorText()));
		GenerateErrorResponse(response,"Database error, please try again!");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		return; 
	}

	int nIsConfirmed=-1;
	int nUserID=-1;
	QString strEmail;
	QString strReconfirmPass;
	int nESR_ServerID_Existing=-1;
	QString strPass;
	if (query.next())
	{
		nUserID=query.value(0).toInt();
		nIsConfirmed=query.value(1).toInt();
		strEmail=query.value(2).toString();
		strReconfirmPass=query.value(3).toString(); //can be reconfirmed only if this is valid...
		nESR_ServerID_Existing=query.value(4).toInt();
		strPass=query.value(5).toString();
	}

	//record does not exist:
	if (nUserID==-1)
	{
		QString strResponse="The email address you are confirming has not yet been registered on 'Mailegant Reply'. Please use your Mailegant App to register it correctly and confirm the email you will receive from us.";
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration server:")+strResponse);
		GenerateErrorResponse(response,strResponse);
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		return;
	}


	//--------------------------------------------------------
	//confirm of reconfirm
	//--------------------------------------------------------

	bool bReconfirmOccured=false;
	//-----------------------------------------------------------
	//CONFIRM: check if confirm exists, if so check if not confirmed, if so confirm it, so help us god!
	//-----------------------------------------------------------
	if (nIsConfirmed==0)
	{
		//set flag as registered:
		QString strSql="UPDATE USER_DATA SET USR_IS_CONFIRMED=1 WHERE USR_ID = "+QString::number(nUserID);
		query.Execute(pStatus,strSql);
		if (!pStatus.IsOK())
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration server: Database Error: %1").arg(pStatus.getErrorText()));
			GenerateErrorResponse(response,"Database error, please try again!");
			status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
			return; 
		}

		//---------------------------------------------
		//	2014-04-28: when user is confirming then and only then create user account at mwES server:
		//---------------------------------------------
		if (nESR_ServerID_Existing>0)
		{
			RegisterUserToESHost(pStatus,nESR_ServerID_Existing,strEmail,strPass);
			if(!pStatus.IsOK()) 
			{
				return;
			}
		}


		//---------------------------------------------
		//	check receipt and all rest...
		//---------------------------------------------

	}
	else
	{
	//-----------------------------------------------------------
	//RE-CONFIRM: just copy new password for new device into right field
	//-----------------------------------------------------------
		if (strReconfirmPass.isEmpty()) //error: trying to click on same link twice
		{
			QString strResponse="The email address you are confirming is already registered on 'mailegant Connect'.";
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration server:")+strResponse);
			GenerateErrorResponse(response,strResponse);
			status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
			return;
		}

		//set flag as registered:
		QString strSql="UPDATE USER_DATA SET USR_IS_CONFIRMED=1, USR_PASSWORD = '"+strReconfirmPass+"',USR_RECONFIRM_PASSWORD=NULL WHERE USR_ID = "+QString::number(nUserID);
		query.Execute(pStatus,strSql);
		if (!pStatus.IsOK())
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration server: Database Error: %1").arg(pStatus.getErrorText()));
			GenerateErrorResponse(response,"Database error, please try again!");
			status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
			return; 
		}

		//---------------------------------------------
		//	2014-04-28: when user is reconfirming then update pass to mwES server or recreate new
		//---------------------------------------------
		if (nESR_ServerID_Existing>0)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::ChangePassUserToESHost");

			ChangePassUserToESHost(pStatus,nESR_ServerID_Existing,strEmail,strReconfirmPass);
			if(!pStatus.IsOK()) 
			{
				return;
			}
		}
		else //a sad MB sere opet, ako vec postoji user i ako nema ES host
		{

			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::RegisterUserToESHost");

			//on new insert associate ES server: get one with lowest users:
			QString strESHost;
			int nESR_ServerID = FindESHost(strESHost);
			if (nESR_ServerID>0)
			{
				//register user to that server:
				RegisterUserToESHost(pStatus,nESR_ServerID,strEmail,strPass);
				if(!pStatus.IsOK()) 
				{
					return;
				}
			}
		}

		bReconfirmOccured = true;
	}

	//MB: 29.03.2013: start multiregister period always:
	QString strSql="UPDATE USER_DATA SET USR_MULTI_REG_START=CURRENT_TIMESTAMP WHERE USR_ID = "+QString::number(nUserID);
	query.Execute(pStatus,strSql); //ignore error

	GenerateSuccessResponse(response,strEmail,bReconfirmOccured);

	status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Mailegant registration handler: SUCCESS END. Email confirmed: %1").arg(strEmail));
	
}

void HttpInviteHandler::GenerateErrorResponse(HTTPResponse &response,QString strError)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(500,"Internal Server Error");
	//response.setStatusLine(200,"OK");
	response.setValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setContentType("text/html");

	QString strBody;
	DataHelper::LoadFile(QCoreApplication::applicationDirPath()+"/confirm.html",strBody);
	strBody.replace("Confirmation",strError);

	response.GetBody()->append(strBody);
}


void HttpInviteHandler::GenerateSuccessResponse(HTTPResponse &response,QString strEmail,bool bReconfirm)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(200,"OK");
	response.setValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setContentType("text/html");

	QString strBody;
	QString strMsg;
	if (!bReconfirm)
	{
		strMsg="Your email address "+strEmail+" has successfully been registered for 'mailegant Connect'!";
	}
	else
	{
		strMsg="Your email address "+strEmail+" has successfully been re-registered for 'mailegant Connect'!";
		strMsg+=" If you use more than one mailegant Writer with this email address, please press the sync button (round arrow next to the mailegant stamp) once on each of them within the next hour to keep them all in sync";
	}

	DataHelper::LoadFile(QCoreApplication::applicationDirPath()+"/confirm.html",strBody);
	strBody.replace("Confirmation",strMsg);
	
	response.GetBody()->append(strBody);
}




void HttpInviteHandler::ChangePassUserToESHost( Status &pStatus, int nESRID, QString strUser, QString strPass )
{

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::ChangePassUserToESHost start");

	//make REST call to the ES server, change user pass

	//make REST call to the ES server, register user

	//get HOST, extract protocol, port and IP address:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strESHost;
	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS FROM EMAIL_SERVERS WHERE ESR_ID="+QString::number(nESRID));
	if(!pStatus.IsOK()) return;

	if (query.next())
	{
		strESHost=query.value(0).toString();
	}
	if (strESHost.isEmpty())
		return;


	QUrl url(strESHost);

	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();

	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;


	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost connecting to %1, %2, %3").arg(strIP).arg(nPort).arg(nIsSSL));

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost connect done"));
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost start session"));

	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,SERVER_INTERSERVER_COMM_USERNAME,SERVER_INTERSERVER_COMM_PASSWORD,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost session ok"));

	QString strLastName,strFirstName;
	strFirstName="";
	strLastName=strUser;

	//SEND:
	client.msg_send.AddParameter(&strUser,"strUser");
	client.msg_send.AddParameter(&strPass,"strPass");

	client.RestSend(pStatus,"POST","/rest/mwcloudservice/changeuserpassword",strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost api ok"));

	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost end session"));

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::ChangePassUserToESHost end");

}



//using what account!?
void HttpInviteHandler::RegisterUserToESHost( Status &pStatus, int nESRID, QString strUser, QString strPass )
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::RegisterUserToESHost start");

	//make REST call to the ES server, register user

	//get HOST, extract protocol, port and IP address:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strESHost;
	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS FROM EMAIL_SERVERS WHERE ESR_ID="+QString::number(nESRID));
	if(!pStatus.IsOK()) return;

	if (query.next())
	{
		strESHost=query.value(0).toString();
	}
	if (strESHost.isEmpty())
		return;

	QUrl url(strESHost);

	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();

	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::RegisterUserToESHost connecting to %1, %2, %3").arg(strIP).arg(nPort).arg(nIsSSL));


	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::RegisterUserToESHost connect done"));


	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,SERVER_INTERSERVER_COMM_USERNAME,SERVER_INTERSERVER_COMM_PASSWORD,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::RegisterUserToESHost session ok"));

	QString strLastName,strFirstName;
	strFirstName="";
	strLastName=strUser;

	//SEND:
	client.msg_send.AddParameter(&strUser,"strUser");
	client.msg_send.AddParameter(&strPass,"strPass");
	client.msg_send.AddParameter(&strFirstName,"strFirstName");
	client.msg_send.AddParameter(&strLastName,"strLastName");

	client.RestSend(pStatus,"POST","/rest/mwcloudservice/createuser",strSession);
	if(!pStatus.IsOK()) return;

	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);
	if(!pStatus.IsOK()) return;


	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::RegisterUserToESHost end");
}



int HttpInviteHandler::FindESHost(QString &strHostIP)
{
	DbRecordSet lstTemp;
	Status err;
	DbSqlQuery query(err, g_DbManager);
	if(!err.IsOK()) return -1;

	query.Execute(err,"SELECT ESR_ID, ESR_HOST_ADDRESS, COUNT(USR_EMAIL_SERVER_ID) CNT FROM EMAIL_SERVERS LEFT OUTER JOIN USER_DATA ON ESR_ID=USR_EMAIL_SERVER_ID GROUP BY ESR_ID,ESR_HOST_ADDRESS");
	if(!err.IsOK()) return -1;

	query.FetchData(lstTemp);

	lstTemp.sort(2);//sort by cnt:

	if (lstTemp.getRowCount()>0)
	{
		strHostIP=lstTemp.getDataRef(0,"ESR_HOST_ADDRESS").toString();
		return lstTemp.getDataRef(0,"ESR_ID").toInt();
	}

	return -1;
}