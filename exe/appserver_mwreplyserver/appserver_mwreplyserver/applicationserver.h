#ifndef APPLICATIONSERVER_H
#define APPLICATIONSERVER_H


#include <QObject>
#include "serverinifile.h"
#include "resthttpserver_local.h"
#include "restrpcdispatcher.h"
#include "trans/trans/httpserver.h"
#include "garbagecollector.h"
#include "common/common/backgroundtaskexecutor.h"
#include "db/db/dbsqlmanager.h"
#include "httpinvitehandler.h"
#include "db/db/dbomanager_oracle.h"
#include "db/db/dbomanager_mysql.h"
#include "db/db/dbomanager_firebird.h"
#include "emailimportthread.h"
#include "trans/trans/htmlhttpserver.h"
#include "backuptaskexecutor.h"


class ApplicationServer: public QObject
{
	Q_OBJECT 

public:

	ApplicationServer(QObject *parent=NULL);
	~ApplicationServer();

	//main API
	void Start(Status &pStatus,QString strServiceDescriptor="");
	void Stop();
	void Restart(Status &pStatus);
	void RestartDelayed(int nReStartAfterSec);
	void StopDelayed(int nStopAfterSec=0);
	bool IsRunning(){return m_bIsRunning;}
	void SaveSettings();

	HTTPServer& GetHttpServer(){	return SokratesHttpServer; }
	HTTPContext GetThreadContext(int nThreadID){return SokratesHttpServer.GetThreadContext(nThreadID);};

	ServerIniFile m_INIFile;
	
	//note: timers can not restart from another thread, use signals
protected slots:
	void RestartDelayedThreadSafeSlot(int);
	void StopThreadSafeSlot();
	void OnBcpManager_RestoreCommandIssued(int);
	void OnFillDatabaseAtStartForTest();

protected:
	void timerEvent(QTimerEvent *event); //overriden QObject timer method

signals:
	void RestartDelayedThreadSafeSignal(int);
	void StopThreadSafeSignal();

private:
	void ClearPointers();
	void RestartDelayedPriv(int nReStartAfterSec);
	void LoadINI(Status &pStatus);
	void LoadNetSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings);

	//---------------INIT & ORGANIZE DATABASE----------------------
	void LoadDbSettings(Status &pStatus,DbConnectionSettings &pDbSettings);
	void CreateNewDb(Status &pStatus,DbConnectionSettings &pDbSettings);
	void InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections=0, int nMinDbConnections=0);
	void CreateObjectDbManager();
	void DestroyObjectDbManager();
	void CheckDatabaseSchema(Status &pStatus);
	void OrganizeDatabase(Status &pStatus,DbSqlQuery *query,int nCurrentDatabaseVersion);


	//TRANSPORT
	//HtmlHTTPServer				*m_HtmlHTTPServer;
	RestHTTPServer_Local			*m_RestHTTPServer;
	RestRpcDispatcher				*m_RestServiceRpcDispatcher;
	HTTPServer						SokratesHttpServer;
	GarbageCollector				*m_GarbageCollector;
	BackgroundTaskExecutor			*m_GbBkgTask;
	BackgroundTaskExecutor			*m_EmailSubscribeBkgTask;
	BackgroundTaskExecutor			*m_EmailSenderTask;
	BackgroundTaskExecutor			*m_EmailNotificatorTask;
	BackupTaskExecutor				*m_SrvBackupBkgTask;
	HttpInviteHandler				*m_HttpInviteHandler;
	DbObjectManager					*m_DbObjectManager;		//db dependent object manager for creating new table structure based on DB
	EmailImportThread				*m_EmailSubscribe;
	HtmlHTTPServer					*m_HtmlHTTPServer;


	//INI
	QString m_strIniFilePath;

	bool m_bIsRunning;
	int m_nTimerID;
	QMutex m_Mutex;					///< mutex

};


#endif // APPLICATIONSERVER_H
