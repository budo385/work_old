#include "pop3ini.h"
#include "common/common/logger.h"

Pop3Ini::Pop3Ini()
{
}

Pop3Ini::~Pop3Ini()
{
}


//loads, creates if not exists, cheks values
bool Pop3Ini::Load(QString strFile, bool bCreateIfNotExists)
{

	if(!QFileInfo(strFile).exists()&& bCreateIfNotExists)
		{return SaveDefaults(strFile);}

	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	// read INI values
	m_objIni.GetValue("Email Account", "ConnectionName", m_strConnectionName);
	m_objIni.GetValue("Email Account", "Server Name", m_strServerName);
	m_objIni.GetValue("Email Account", "IsActive", m_nActive,1);
	m_objIni.GetValue("Email Account", "Is POP3 server (1-pop3, 0-imap)", m_nIsPop);
	//m_objIni.GetValue("Email Account", "Scanning Frequency (seconds)", m_nFreqSeconds);
	//m_objIni.GetValue("Email Account", "Last Scanned on", m_strLastScanned);
	m_objIni.GetValue("Email Account", "Email address", m_strEmailAddress);
	m_objIni.GetValue("Email Account", "Port", m_nPort,0);
	m_objIni.GetValue("Email Account", "SSL (NONE, SSL, STARTTLS)", m_strSSL,"SSL");
	m_objIni.GetValue("Email Account", "Username", m_strUserName);
	m_objIni.GetValue("Email Account", "Password", m_strPassword);

	return CheckValues();
}

bool Pop3Ini::Save(QString strFile)
{
	m_objIni.SetPath(strFile);
	m_objIni.SetValue("Email Account", "ConnectionName", m_strConnectionName);
	m_objIni.SetValue("Email Account", "Server Name", m_strServerName);
	m_objIni.SetValue("Email Account", "IsActive", m_nActive);
	m_objIni.SetValue("Email Account", "Is POP3 server (1-pop3, 0-imap)", m_nIsPop);
	//m_objIni.SetValue("Email Account", "Scanning Frequency (seconds)", m_nFreqSeconds);
	//m_objIni.SetValue("Email Account", "Last Scanned on", m_strLastScanned);
	m_objIni.SetValue("Email Account", "Email address", m_strEmailAddress);
	m_objIni.SetValue("Email Account", "Port", m_nPort);
	m_objIni.SetValue("Email Account", "SSL (NONE, SSL, STARTTLS)", m_strSSL);
	m_objIni.SetValue("Email Account", "Username", m_strUserName);
	m_objIni.SetValue("Email Account", "Password", m_strPassword);

	return m_objIni.Save();
}



bool Pop3Ini::CheckValues()
{
	return true;
}



bool Pop3Ini::SaveDefaults(QString strFile)
{

	return Save(strFile);
}
