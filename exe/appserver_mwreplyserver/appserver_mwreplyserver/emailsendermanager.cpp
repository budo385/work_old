#include "emailsendermanager.h"
#include "trans/trans/smtpclient.h"

//#include "emailhandler.h"
#include "trans/trans/smtpclient.h"
#include "common/common/config_version_ffh.h"
#include "common/common/datahelper.h"
#include "trans/trans/httputil.h"
#include "common/common/zipcompression.h"
#include "common/common/threadid.h"
#include "db/db/dbsqlquery.h"

#include "db/db/dbsqlmanager.h"
extern DbSqlManager	*g_DbManager;
#include "common/common/logger.h"
extern Logger			g_Logger;
#include "servicehandler.h"
extern ServiceHandler	*g_ServiceHandler;

EmailSenderManager::EmailSenderManager()
{
	m_nNextEmailID=0;
}

EmailSenderManager::~EmailSenderManager()
{

}

//execute everything in list
void EmailSenderManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	//qDebug()<<ThreadIdentificator::GetCurrentThreadID();

	if(!m_Mutex.tryLock(1000)) //thread is occupied leave it alone
		return;

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Email Handler execute: START, thread: %1").arg(QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()));

	//copy whole list and clear main list and release lock
	QHash<int,EmailTask>	lstScheduledEmails;

	m_Lock.lockForWrite();
	lstScheduledEmails=m_lstScheduledEmails;
	m_lstScheduledEmails.clear();
	m_Lock.unlock();

	Status err;
	DbSqlQuery query(err, g_DbManager);
	if(!err.IsOK()) 
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,1,QString("EmailSenderManager: failed to access database: %1").arg(err.getErrorText()));
		return;
	}

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Executing Email Tasks. Number of tasks waiting: %1").arg(lstScheduledEmails.size()));

	QHashIterator<int,EmailTask> i(lstScheduledEmails);
	while (i.hasNext()) 
	{
		i.next();
		EmailTask task=i.value();
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("EmailSenderManager: START sending email, ID=%1, subject='%2', body_first_100='%3', to='%4'").arg(i.key()).arg(task.subject).arg(task.body.left(100)).arg(task.to));

		query.Execute(err,"SELECT USR_ID,USR_SENT_EMAILS_OK,USR_SENT_EMAILS_FAIL FROM USER_DATA WHERE USR_EMAIL = '"+task.strEmailUser+"'");
		if(!err.IsOK()) 
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,1,QString("EmailSenderManager error: %1").arg(err.getErrorText()));
			continue;
		}
		DbRecordSet rowUserData;
		query.FetchData(rowUserData);
		if (rowUserData.getRowCount()!=1)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,1,QString("EmailSenderManager: user row empty %1").arg(err.getErrorText()));
			continue;
		}
		QString strSQL;

		SMTPClient client;
		if(!client.SendMail(task.strMailServer,task.from,task.to,task.subject,task.body,task.bcc,task.strUserName,task.strPassword,task.bBodyIsHtml,task.bSendBodyAsFormatedMimeMessage,task.nSmtpPort,true))
		{
			m_Lock.lockForWrite();
			m_lstStatusSentEmails[i.key()]=QString("FAILED");
			//m_lstStatusSentEmails[i.key()]=QString("Failed to send email %1 at email address: %2 with ID=%3").arg(task.subject).arg(task.to).arg(i.key());
			m_Lock.unlock();
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("EmailSenderManager: FAIL to send email, ID=%1. Last error: %2").arg(i.key()).arg(client.GetLastError()));
			int nFail=rowUserData.getDataRef(0,"USR_SENT_EMAILS_FAIL").toInt()+1;
			strSQL="UPDATE USER_DATA SET USR_SENT_EMAILS_FAIL="+QString::number(nFail)+" WHERE USR_ID = "+QString::number(rowUserData.getDataRef(0,"USR_ID").toInt());

			if (task.nESRHostID>0 || !task.strUser.isEmpty())
			{
				Status err;
				g_ServiceHandler->SendUserErrorPushMessageToESHost(err,task.nESRHostID,task.strUser,task.from,client.GetLastError());
				if (!err.IsOK())
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("EmailSenderManager: FAIL to send push error message to user %1, error: %2").arg(task.strUser).arg(err.getErrorText()));
				}
			}
		}
		else
		{
			m_Lock.lockForWrite();
			m_lstStatusSentEmails[i.key()]=QString("OK"); //ok if empty
			m_Lock.unlock();
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("EmailSenderManager: SUCCESS sending email, ID=%1").arg(i.key()));
			int nOK=rowUserData.getDataRef(0,"USR_SENT_EMAILS_OK").toInt()+1;
			strSQL="UPDATE USER_DATA SET USR_SENT_EMAILS_OK="+QString::number(nOK)+" WHERE USR_ID = "+QString::number(rowUserData.getDataRef(0,"USR_ID").toInt());
		}

		query.Execute(err,strSQL);
		if(!err.IsOK()) 
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,1,QString("EmailSenderManager error: %1").arg(err.getErrorText()));
	}

	m_Mutex.unlock();


}

//get status for mail id and clear it from list
//return false if id does not exist in status list
bool EmailSenderManager::GetStatusForMail(int nEmailTaskID,QString &strStatus)
{
	QWriteLocker lock(&m_Lock);
	if (m_lstStatusSentEmails.contains(nEmailTaskID))
	{
		strStatus=m_lstStatusSentEmails.value(nEmailTaskID,"NOT IN LIST");
		if (strStatus=="OK" || strStatus=="FAILED")
			m_lstStatusSentEmails.remove(nEmailTaskID);
		return true;
	}
	else
	{
		strStatus="NOT IN LIST";
		return false;
	}

}

//create task, store in list, create id, schedule for immediate execution
int	EmailSenderManager::ScheduleSendMail(QString strEmail, const QString &strMailServer, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc,  int nESRHostID,  QString strUser, QString strUserName, QString strPassword,const bool bBodyIsHtml, const bool bSendBodyAsFormatedMimeMessage, int nSmtpPort)
{
	EmailTask task;
	task.strEmailUser=strEmail;
	task.strMailServer=strMailServer;
	task.from=from;
	task.to=to;
	task.subject=subject;
	task.body=body;
	task.bcc=bcc;
	task.strUserName=strUserName;
	task.strPassword=strPassword;
	task.bBodyIsHtml=bBodyIsHtml;
	task.bSendBodyAsFormatedMimeMessage=bSendBodyAsFormatedMimeMessage;
	task.nSmtpPort=nSmtpPort;
	task.nESRHostID=nESRHostID;
	task.strUser=strUser;

	QWriteLocker lock(&m_Lock);
	m_nNextEmailID++;

	//m_Lock.lockForWrite();
	m_lstScheduledEmails[m_nNextEmailID]=task;
	m_lstStatusSentEmails[m_nNextEmailID]="NOT SENT";
		
	return m_nNextEmailID;
}



