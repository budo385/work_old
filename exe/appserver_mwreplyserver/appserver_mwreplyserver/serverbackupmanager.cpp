#include "serverbackupmanager.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbsqlmanager.h"
#include <QDir>
#include <QCoreApplication>
#include "common/common/datahelper.h"
#include "db/db/dbomanager_oracle.h"
#include "db/db/dbomanager_mysql.h"
#include "db/db/dbomanager_firebird.h"

#include "common/common/threadid.h"
extern DbSqlManager *g_DbManager;

#include "common/common/logger.h"
extern Logger				g_Logger;


#define MUTEX_LOCK_TIMEOUT 30000 //longer timeout



//: when called directly from client: callerid=personid, taskid is ignored
void ServerBackupManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	Status pStatus;
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT)) //all bcp data can be read out by others
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Backup process started, thread:" +QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());

	QString strBackupFileName=ExecBackup(pStatus);
	if (!pStatus.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_BACKUP_FAIL,pStatus.getErrorText());
	}
	

	m_RWMutex.unlock();
}



void ServerBackupManager::ClearDatabaseAfterRestore(Status &pStatus)
{
	//g_SystemServiceSet->AppSrvSession->CleanAllSessions(pStatus);		
}



void ServerBackupManager::StartDatabase()
{
	g_DbManager->EnablePool();
	g_DbManager->FillPoolWithMinConnections();
}
void ServerBackupManager::CloseDatabase()
{
	g_DbManager->ShutDown();
}

void ServerBackupManager::Initialize( Status &pStatus,const DbConnectionSettings &ConnSettings, bool bShutDownDatabaseWhenBackup/*=false*/,bool bUseApplicationInstallDirForBackupDir/*=true*/ )
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	m_DbConnSettings = ConnSettings;
	m_bShutDownDatabaseWhenBackup=bShutDownDatabaseWhenBackup;

	//backup location:
	QDir bcpDir(m_strBackupPath);
	if (!bcpDir.exists() || m_strBackupPath.isEmpty())
	{
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"bcp path:"+m_strBackupPath);
		QString strAppDir;
		if (bUseApplicationInstallDirForBackupDir)
			strAppDir=QCoreApplication::applicationDirPath();
		else
			strAppDir=DataHelper::GetApplicationHomeDir();

		QDir bcpDirCreate(strAppDir);
		if (bcpDirCreate.exists("backup"))
			m_strBackupPath=strAppDir+"/backup";
		else if(bcpDirCreate.mkdir("backup"))
			m_strBackupPath=strAppDir+"/backup";
		else
		{
			pStatus.setError(1,QObject::tr("Can not determine backup directory path!"));
			m_RWMutex.unlock();
			return;
		}

		m_DbActualizator.SetBackupPath(m_strBackupPath);
	}


	m_DbObjectManager=NULL;

	//make instance of proper DboManager:
	if(g_DbManager->GetDbType()==DBTYPE_ORACLE)
		m_DbObjectManager= new DbObjectManager_Oracle(g_DbManager->GetDbSettings());
	else if (g_DbManager->GetDbType()==DBTYPE_MYSQL)
		m_DbObjectManager= new DbObjectManager_MySQL(g_DbManager->GetDbSettings());
	else if (g_DbManager->GetDbType()==DBTYPE_FIREBIRD)
		m_DbObjectManager= new DbObjectManager_FireBird(g_DbManager->GetDbSettings());
	else
		Q_ASSERT_X(false,"DBO ORGANIZER", "Database object manager doesnt exists for specified base");



	m_RWMutex.unlock();

}

void ServerBackupManager::StartUpCheck( Status &pStatus )
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	CloseDatabase();

	if (CheckDatabaseForRestore())
	{
		ExecRestore(pStatus);
		if (!pStatus.IsOK())
		{
			StartDatabase();
			m_RWMutex.unlock();
			return;
		}
	}

	QDateTime date;
	if (CheckDatabaseForBackup(date))
	{
		ExecBackup(pStatus);
		if (!pStatus.IsOK()) 
		{
			StartDatabase();
			m_RWMutex.unlock();
			return;
		}

	}

	StartDatabase();
	m_RWMutex.unlock();

}

QString ServerBackupManager::ExecBackup( Status &pStatus )
{
	if(!m_BackupLock.tryLock(MUTEX_LOCK_TIMEOUT)) //but only 1 thread at time, can exec Backup !
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return "";
	}

	if (m_bShutDownDatabaseWhenBackup)
		CloseDatabase();//close all connection, although actualize module can be connected


	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_START);

	DbConnectionSettings tmpDbSettings=g_DbManager->GetDbSettings();
	QString strBackupPath=m_strBackupPath;
	QString strBcpName=m_DbObjectManager->BackupDatabase(pStatus,tmpDbSettings.m_strDbName,tmpDbSettings.m_strDbUserName,tmpDbSettings.m_strDbPassword,strBackupPath,"",tmpDbSettings.m_strDbHostName,QString::number(tmpDbSettings.m_nDbPort),NULL);
	if (pStatus.IsOK())
	{
		m_datBackupLastDate=QDateTime::currentDateTime().toString("dd.MM.yyyy-hh.mm.ss");
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_SUCCESS,strBcpName);
	}

	//issue 2576: delete old backups:
	if (m_nDeleteOldBackupsDay>0)
	{
		Status err;
		CleanOldBackups(err, m_nDeleteOldBackupsDay);
		if (!err.IsOK())
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1,QString("Failed to delete old backups: %1").arg(err.getErrorText()));
		}
	}
	
	if (m_bShutDownDatabaseWhenBackup)
		StartDatabase();

	m_BackupLock.unlock();
	return strBcpName;

}

void ServerBackupManager::ExecRestore( Status &pStatus )
{
	QString strRestore=QDir::cleanPath(m_strBackupPath+"/"+m_strRestoreOnNextStart);

	CloseDatabase();//close all connection, although actualize module can be connected
	
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_MANAGER_RESTORE_START,strRestore);

	DbConnectionSettings tmpDbSettings=g_DbManager->GetDbSettings();
	QString strBackupPath=m_strBackupPath;
	m_DbObjectManager->RestoreDatabase(pStatus,tmpDbSettings.m_strDbName,tmpDbSettings.m_strDbUserName,tmpDbSettings.m_strDbPassword,strRestore,tmpDbSettings.m_strDbHostName,QString::number(tmpDbSettings.m_nDbPort),NULL);

	//enable
	StartDatabase();

	//in event of restore, mark as fresh backup:
	if (pStatus.IsOK())
	{
		m_datBackupLastDate=QDateTime::currentDateTime().toString("dd.MM.yyyy-hh.mm.ss");
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_MANAGER_RESTORE_SUCCESS,strRestore);
		ClearDatabaseAfterRestore(pStatus);
	}

	m_strRestoreOnNextStart=""; //always reset flag
}



void ServerBackupManager::CleanOldBackups(Status &pStatus, int nDaysOld)
{

	QDateTime datTarget=QDateTime::currentDateTime().addDays(-nDaysOld);

	//just for firebird, muv out path chars:
	DbConnectionSettings tmpDbSettings=g_DbManager->GetDbSettings();
	QFileInfo info(tmpDbSettings.m_strDbName);
	QString strDbName=info.baseName();

	pStatus.setError(0);

	//read all files from backup:
	QDir dir(m_strBackupPath);
	if (!dir.exists())
	{
		pStatus.setError(1,QObject::tr("Error trying to delete old backups, backup Directory:")+m_strBackupPath+QObject::tr(" does not exists!"));
		return ;
	}

	//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isFile())
		{
			QString fileName=lstFiles.at(i).fileName();
			QString strOrderString;

			if(lstFiles.at(i).suffix()!="zip")
				continue;

			QString strFileTestDb=fileName.left(strDbName.length());
			QStringList lstParts=fileName.split("_",QString::SkipEmptyParts,Qt::CaseInsensitive);
			if (lstParts.size()<3)
				continue;

			//delete:
			if (lstFiles.at(i).lastModified()<datTarget)
			{
				QFile::remove(lstFiles.at(i).absoluteFilePath());
			}
		}

	}
}

ServerBackupManager::ServerBackupManager()
{
	m_DbObjectManager=NULL;
}

ServerBackupManager::~ServerBackupManager()
{
	if(m_DbObjectManager!=NULL) delete m_DbObjectManager;

}


/*
//delayed backup
void ServerBackupManager::Backup(Status &pStatus)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	if (m_bRestoreInProgress)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		m_RWMutex.unlock();
		return;
	}
	m_RWMutex.unlock();
	emit BackupCommandIssued(g_UserSessionManager->GetPersonID());
}
//delayed restore
void ServerBackupManager::Restore(Status &pStatus,QString strFileNameForRestore)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	m_strRestoreOnNextStart=strFileNameForRestore;
	m_bRestoreInProgress=true;
	m_RWMutex.unlock();
	emit RestoreCommandIssued(g_UserSessionManager->GetPersonID());
}*/

