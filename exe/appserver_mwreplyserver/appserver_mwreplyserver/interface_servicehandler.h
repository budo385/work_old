#ifndef INTERFACE_SERVICEHANDLER_H
#define INTERFACE_SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_ServiceHandler
{
public:

	virtual ~Interface_ServiceHandler(){};
	virtual void IsRegistered(Status &pStatus, QString strEmail,int &Ret_IsRegistered, QString &Ret_strESHost, int &Ret_nNumDaysLeft, int &Ret_nSubscriptionStatus, QString strReceipt="")=0; 
	virtual void SendConfirmationEmail(Status &pStatus, QString strEmail,QString &Ret_strClientPassword, QString &Ret_strESHost, bool bSkipLBNotify=false, QString strReceipt="")=0;
	virtual void ReadEmails(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,DbRecordSet &Ret_Emails)=0;
	virtual void GetEmail(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,int nEmailID, DbRecordSet &Ret_EmailData)=0;
	virtual void SetAddOnPass(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,QString strAddOnPass)=0;
	virtual void GetAuthenticationID(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,QString &Ret_strClientPassword)=0;
	
	virtual void RegisterSMTPAccount(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken, QString strServerAddress, QString strUserName, QString strPassword, int nPort, QString strBCCAddressForSentMailCopy) =0;
	virtual void UnregisterSMTPAccount(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken) = 0;
	virtual void SendEMail(Status &pStatus,QString strEmail, QString strMimeFrom,QString strConce, QString strAuthToken,QString strTo,QString lstCC,QString lstBCC,QString strSubject,QString strMimeBody,int &Ret_ID)=0;
	virtual void CheckEmailStatus(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken, DbRecordSet &lstEmailTasks, DbRecordSet &Ret_lstEmailTasksStatus)=0;
	virtual void StartMultiRegisterPeriod(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken)=0;

	virtual void GetStats(Status &pStatus, QString strConce, QString strAuthToken, int &Ret_nRegisteredUsers, int &Ret_nRegisteredUsersInLast7days, int &Ret_nUsersPendingConfirmation, int &Ret_nPaid,int &Ret_nPaidExpired, int &Ret_nTest,int &Ret_nTestExpired) =0;
	virtual void GetUsers(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstRegisteredEmails, int &Ret_nTotalRegisteredUsers, int nSortOrder=-1, int nFromN=-1, int nToN=-1) = 0;
	virtual void WriteAccessEmails(Status &pStatus, QString strConce, QString strAuthToken, QString strEmails)=0;
	virtual void DeleteAccessEmail(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail)=0;
	virtual void SetUserSubscriptionAccess(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nLoginLevel)=0;
	virtual void DeleteUser(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail)=0;

	virtual void ReadDataList(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, DbRecordSet &Ret_List)=0;
	virtual void GetData(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, QString strName, QString &Ret_strData)=0;
	virtual void StoreData(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, QString strName, QString strData)=0;

	virtual void AddESHost(Status &pStatus, QString strConce, QString strAuthToken, QString strESHost)=0;
	virtual void DeleteESHost(Status &pStatus, QString strConce, QString strAuthToken, QString strESHost)=0;
	virtual void GetESHosts(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstESHosts)=0;
	virtual void RegisterESHost2Users(Status &pStatus, QString strConce, QString strAuthToken)=0;
	virtual void RegisterESHost2SingleUser(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail)=0;

	//mwLB server API:
	virtual void LockDatabase(Status &pStatus, QString strConce, QString strAuthToken, int nLockReadOnly)=0;
	virtual void IsServerAlive(Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strLoadBalanceServerURL, int &Ret_nStatus)=0;
	virtual void AddEmailToCS(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail)=0;
	virtual void RemoveEmailToCS(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail)=0;
	virtual void UnregisterLBServer(Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strLoadBalanceServerURL)=0;

	virtual void GetServerTime(Status &Ret_Status, QString strEmail, QString strConce, QString strAuthToken, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt)=0;
	virtual void SetNextTestTime(Status &Ret_Status, QString strEmail, QString strConce, QString strAuthToken, QDateTime datSetTimeNextTest)=0;

	//internal API:
	//virtual void CleanUpOldAccounts(Status &pStatus)=0;
	//virtual void RegisterEmailToLBServer(Status &pStatus, QString strEmail)=0;
	//virtual void SyncEmailsToLBServer(Status &pStatus)=0;
};


/*
<Web_service_meta_data>

	<IsRegistered>
		<URL>/service/isregistered</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</IsRegistered>
	
	<SendConfirmationEmail>
		<URL>/service/sendconfirmationemail</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SendConfirmationEmail>

	<ReadEmails>
		<URL>/service/reademails</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_Emails>
			<DISPLAY_NAME>Emails</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
		</Ret_Emails>
	</ReadEmails>

	<GetEmail>
		<URL>/service/getemail</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_EmailData>
			<DISPLAY_NAME>EmailData</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
		</Ret_EmailData>
	</GetEmail>

	<RegisterSMTPAccount>
		<URL>/service/registersmtpaccount</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</RegisterSMTPAccount>

	<UnregisterSMTPAccount>
		<URL>/service/unregistersmtpaccount</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</UnregisterSMTPAccount>

	<SendEMail>
		<URL>/service/sendemail</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SendEMail>

	<CheckEmailStatus>
		<URL>/service/checkemailstatus</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<lstEmailTasks>
			<DEFINE_LIST>lstEmailTasks.addColumn(QVariant::Int,"ID");</DEFINE_LIST>
		</lstEmailTasks>
	</CheckEmailStatus>

	<GetStats>
		<URL>/service/getstats</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</GetStats>

	<GetUsers>
		<URL>/service/getusers</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</GetUsers>

	<StartMultiRegisterPeriod>
		<URL>/service/startmultiregisterperiod</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</StartMultiRegisterPeriod>

	<ReadDataList>
		<URL>/service/readdatalist</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</ReadDataList>

	<GetData>
		<URL>/service/getdata</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</GetData>

	<StoreData>
		<URL>/service/storedata</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</StoreData>

	<WriteAccessEmails>
		<URL>/service/writeaccessemails</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</WriteAccessEmails>

	<DeleteAccessEmail>
		<URL>/service/deleteaccessemail</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</DeleteAccessEmail>

	<SetUserSubscriptionAccess>
		<URL>/service/setusersubscriptionaccess</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SetUserSubscriptionAccess>

	<AddESHost>
		<URL>/service/addeshost</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</AddESHost>

	<DeleteESHost>
		<URL>/service/deleteeshost</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</DeleteESHost>	

	<GetESHosts>
		<URL>/service/geteshosts</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</GetESHosts>

	<RegisterESHost2Users>
		<URL>/service/registereshost2users</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</RegisterESHost2Users>

	<RegisterESHost2SingleUser>
		<URL>/service/registereshost2singleuser</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</RegisterESHost2SingleUser>

	<LockDatabase>
		<URL>/service/lockdatabase</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</LockDatabase>

	<IsServerAlive>
		<URL>/service/isserveralive</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</IsServerAlive>

	<AddEmailToCS>
		<URL>/service/addemailtocs</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</AddEmailToCS>
	
	<RemoveEmailToCS>
		<URL>/service/removeemailtocs</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</RemoveEmailToCS>

	<UnregisterLBServer>
		<URL>/service/unregisterlbserver</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</UnregisterLBServer>

	<SetAddOnPass>
		<URL>/service/setaddonpass</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SetAddOnPass>

	<GetAuthenticationID>
		<URL>/service/getauthenticationid</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</GetAuthenticationID>

	<GetServerTime>
		<URL>/service/getservertime</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</GetServerTime>

	<SetNextTestTime>
		<URL>/service/setnexttesttime</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SetNextTestTime>

	<DeleteUser>
		<URL>/service/deleteuser</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</DeleteUser>

	

</Web_service_meta_data>
*/

#endif // INTERFACE_SERVICEHANDLER_H

	
	
	