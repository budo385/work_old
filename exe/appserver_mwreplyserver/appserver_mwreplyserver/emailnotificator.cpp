#include "emailnotificator.h"
#include "common/common/datahelper.h"
#include "trans/trans/httputil.h"
#include "common/common/threadid.h"
#include "trans/trans/resthttpclient.h"

#include "common/common/logger.h"
extern Logger							g_Logger;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;

#define SECRET_SPICE_PASSWORD "BANDIT"
#define STAT_USERNAME "mcadmin"
#define STAT_PASSWORD "pachelbel"



EmailNotificator::EmailNotificator()
{
}

EmailNotificator::~EmailNotificator()
{

}

//execute everything in list
void EmailNotificator::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	
	if(!m_Mutex.tryLock(100)) //thread is occupied leave it alone
		return;

	int nSize=m_lstUsersWithNewEmail.size();
	if (nSize==0)
	{
		m_Mutex.unlock();
		return;
	}

	//REST client..
	//CONNECT:
	Status err;
	RestHTTPClient client;
	client.SetConnectionSettings(g_AppServer->m_INIFile.m_strNotificatorServerIP,g_AppServer->m_INIFile.m_nNotificatorServerPort,false);
	client.Connect(err);
	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("Mailegant server failed to connect to the notificator service: %1").arg(err.getErrorText()));
		m_Mutex.unlock();
		return;
	}

	//lock list & delete it
	QWriteLocker lock(&m_Lock);


	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();

	//copy data
	DbRecordSet lstEmails;
	lstEmails.addColumn(QVariant::String,"EMAIL");
	lstEmails.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		lstEmails.setData(i,0,m_lstUsersWithNewEmail.at(i));
	}
	
	client.msg_send.AddParameter(&strConce);
	client.msg_send.AddParameter(&strAuthTokenServer);
	client.msg_send.AddParameter(&lstEmails);

	client.RestSend(err,"POST","/rest/service/setnewemailstatus");
	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("Mailegant server failed to send command to the notificator service: %1").arg(err.getErrorText()));
		m_Mutex.unlock();
		return;
	}

	Status err2;
	client.Disconnect(err2);

	m_lstUsersWithNewEmail.clear();

	m_Mutex.unlock();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Mailegant server notified Email Notificator for new emails for %1 users").arg(nSize));

}


void EmailNotificator::ScheduleNewEmailNotification(const QStringList &lstNew) 
{
	QWriteLocker lock(&m_Lock);

	int nSize=lstNew.size();
	for (int i=0;i<nSize;i++)
	{
		if(!m_lstUsersWithNewEmail.contains(lstNew.at(i)))
			m_lstUsersWithNewEmail.append(lstNew.at(i));
	}
}

