#ifndef EMAILSENDERMANAGER_H
#define EMAILSENDERMANAGER_H

#include "common/common/backgroundtaskinterface.h"
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include <QReadWriteLock>


class EmailTask
{
public:
	QString strEmailUser;
	QString strMailServer;
	QString from; 
	QString to; 
	QString subject; 
	QString body; 
	QList<QString> bcc;
	QString strUserName;
	QString strPassword;
	bool bBodyIsHtml;
	bool bSendBodyAsFormatedMimeMessage;
	int nSmtpPort;

	//for error feedback:
	int		nESRHostID;
	QString strUser;
};

class EmailSenderManager : public QObject, public BackgroundTaskInterface
{
	Q_OBJECT

public:
	EmailSenderManager();
	~EmailSenderManager();

	void		ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");					
	int			ScheduleSendMail(QString strEmail,const QString &strMailServer, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc,  int nESRHostID=-1,  QString strUser="", QString strUserName ="", QString strPassword="",const bool bBodyIsHtml=false, const bool bSendBodyAsFormatedMimeMessage=false, int nSmtpPort=0); 
	bool		GetStatusForMail(int nEmailTaskID,QString &strStatus);


private:
	
	QReadWriteLock			m_Lock;
	QHash<int,EmailTask>	m_lstScheduledEmails;
	QHash<int,QString>		m_lstStatusSentEmails;
	int						m_nNextEmailID;
	QMutex					m_Mutex;
};

#endif // EMAILSENDERMANAGER_H
