#include "serverinifile.h"
#include "common/common/logger.h"

ServerIniFile::ServerIniFile()
{
}

ServerIniFile::~ServerIniFile()
{
}

void ServerIniFile::Clear()
{
	//m_lstConnections.clear();
}

//loads, creates if not exists, cheks values
bool ServerIniFile::Load(QString strFile, bool bCreateIfNotExists)
{
	Clear();

	if(!QFileInfo(strFile).exists()&& bCreateIfNotExists)
		{return SaveDefaults(strFile);}

	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	// read INI values
	m_objIni.GetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.GetValue("Main", "AppServerPort",		m_nAppServerPort, 1111);
	m_objIni.GetValue("Main", "MaxConnections",		m_nMaxConnections, 2000);
	m_objIni.GetValue("Main", "MaxDbConnections",	m_nMaxDbConnections, 300);
	m_objIni.GetValue("Main", "MinDbConnections",	m_nMinDbConnections, 10);
	m_objIni.GetValue("Main", "Service Descriptor",	m_strServiceDescriptor);
	m_objIni.GetValue("Main", "AutoStartService",	m_nAutoStartService,1);
	m_objIni.GetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.GetValue("Main", "HTTPRegisterAddress",	m_strHTTPRegisterAddress,"http://everxconnect.com/registration");
	m_objIni.GetValue("Main", "LockRegistration",	m_nLockRegistration,0);
	m_objIni.GetValue("Main", "LockWriteDatabase",	m_nLockWriteDatabase,0);	
	m_objIni.GetValue("Main", "DaysAfterUserAccountExpire",	m_nExpireEmailAccountDays,30);	
	m_objIni.GetValue("Main", "Host domain",	m_strHostDomain,"everxconnect.com");	
	m_objIni.GetValue("Main", "CloudTestingPeriodDays",	m_nNumCloudTestingPeriodDays,30);	
	

	m_objIni.GetValue("LoadBalance", "HostUniqueID (for load balance)",	m_strHostUniqueID,"");	
	m_objIni.GetValue("LoadBalance", "LoadBalanceServerURL",	m_strLoadBalanceServerURL,"");	
			
	m_objIni.GetValue("Apple", "AppleReceiptValidationUrl",			m_strAppleReceiptValidationUrl, "https://buy.itunes.apple.com/verifyReceipt");
	m_objIni.GetValue("Apple", "AppleReceiptPasswordCheck",			m_strAppleReceiptPasswordCheck, "50b15439673649febeca6b585720f865");
	
	m_objIni.GetValue("SSL", "SSLMode",			m_nSSLMode, 0);
	m_objIni.GetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);

	m_objIni.GetValue("Logging", "Log Level",												m_nLogLevel,Logger::LOG_LEVEL_BASIC);
	m_objIni.GetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize,10);
	m_objIni.GetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize,64);
	
	m_objIni.GetValue("SMTP", "Mail SMTP Server",	m_strMailServer,"mail.sokrates.ch");
	m_objIni.GetValue("SMTP", "Server Port",m_nMailServerPort,25);
	m_objIni.GetValue("SMTP", "Server Mail Address",m_strMailServerAddress);
	m_objIni.GetValue("SMTP", "Server Mail User",	m_strMailServerUserName);
	m_objIni.GetValue("SMTP", "Server Mail Password",m_strMailServerPassword);

	m_objIni.GetValue("User SMTP", "User Mail SMTP Server",	m_strUserSMTPMailServer,"mail.sokrates.ch");
	m_objIni.GetValue("User SMTP", "User Server Port",m_nUserSMTPMailServerPort,25);
	m_objIni.GetValue("User SMTP", "User Server Mail Address",m_strUserSMTPMailServerAddress);
	m_objIni.GetValue("User SMTP", "User Server Mail User",	m_strUserSMTPMailServerUserName);
	m_objIni.GetValue("User SMTP", "User Server Mail Password",m_strUserSMTPMailServerPassword);

	m_objIni.GetValue("EmailNotificator", "NotificatorIPAddress",	m_strNotificatorServerIP, "localhost");
	m_objIni.GetValue("EmailNotificator", "NotificatorPort",		m_nNotificatorServerPort, 41565);
	
	m_objIni.GetValue("Backup", "Backup Frequency",		m_nBackupFreq,2);
	m_objIni.GetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.GetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.GetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.GetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.GetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
	m_objIni.GetValue("Backup", "Delete old Backup Files Older Than",m_nDeleteOldBackupsDay,0);
	
	m_objIni.GetValue("Reply Server", "Max. time to keep emails (hours)",				m_nTimeToKeepEmailsHr,72);
	m_objIni.GetValue("Reply Server", "Max. emails to keep per user",					m_nMaxEmailCnt,50);
	m_objIni.GetValue("Reply Server", "Max. time before registration expires (hours)",	m_nRegistrationTimeOutHr,24);
	m_objIni.GetValue("Reply Server", "Email Scan Frequency (seconds)",	m_nScanFrequency,60);
	
	m_objIni.GetValue("Self Test Features", "StartUserNameToAdd (test procedure will add 4 digits at end, e.g. user0000, specify as 'user' or 'user@domain.com')",	m_strStartUserNameAdd);
	m_objIni.GetValue("Self Test Features", "StartPassWordToAdd (test procedure will add 4 digits at end, e.g. pw0000)",	m_strStartPassWordAdd);
	m_objIni.GetValue("Self Test Features", "CountUserToAdd",		m_nCountUserAdd,0);
	m_objIni.GetValue("Self Test Features", "StartCounterToAdd (start digits from this value)",		m_nStartCounterAdd,0);
	m_objIni.GetValue("Self Test Features", "LastUserNameAdded",	m_strLastUserNameAdded,"");
	m_objIni.GetValue("Self Test Features", "NextTestTime",	m_strNextTestTime,"");
	
	QString m_strStartUserNameAdd;
	QString m_strStartPassWordAdd;
	int m_nCountUserAdd;
	return CheckValues();
}

bool ServerIniFile::Save(QString strFile)
{
	m_objIni.SetPath(strFile);

	//fill the data inside
	m_objIni.RemoveSection("Main");		//cleanup existing data
	m_objIni.RemoveSection("Logging");	
	//m_objIni.RemoveSection("Backup");	
	m_objIni.RemoveSection("SSL");		

	// write INI values
	m_objIni.SetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.SetValue("Main", "AppServerPort",		m_nAppServerPort);
	m_objIni.SetValue("Main", "MaxConnections",		m_nMaxConnections);
	m_objIni.SetValue("Main", "MaxDbConnections",	m_nMaxDbConnections);
	m_objIni.SetValue("Main", "MinDbConnections",	m_nMinDbConnections);
	m_objIni.SetValue("Main", "Service Descriptor",	m_strServiceDescriptor);
	m_objIni.SetValue("Main", "AutoStartService",	m_nAutoStartService);
	m_objIni.SetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.SetValue("Main", "HTTPRegisterAddress",	m_strHTTPRegisterAddress);
	m_objIni.SetValue("Main", "LockRegistration",	m_nLockRegistration);
	m_objIni.SetValue("Main", "LockWriteDatabase",	m_nLockWriteDatabase);	
	m_objIni.SetValue("Main", "DaysAfterUserAccountExpire",	m_nExpireEmailAccountDays);	
	m_objIni.SetValue("Main", "Host domain",	m_strHostDomain);	
	m_objIni.SetValue("Main", "CloudTestingPeriodDays",	m_nNumCloudTestingPeriodDays);	

	m_objIni.SetValue("LoadBalance", "HostUniqueID (for load balance)",	m_strHostUniqueID);
	m_objIni.SetValue("LoadBalance", "LoadBalanceServerURL",	m_strLoadBalanceServerURL);	
	
	m_objIni.SetValue("Apple", "AppleReceiptValidationUrl",			m_strAppleReceiptValidationUrl);
	m_objIni.SetValue("Apple", "AppleReceiptPasswordCheck",			m_strAppleReceiptPasswordCheck);

	m_objIni.SetValue("SSL", "SSLMode",				m_nSSLMode);
	m_objIni.SetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);

	m_objIni.SetValue("SMTP", "Mail SMTP Server",	m_strMailServer);
	m_objIni.SetValue("SMTP", "Server Port",m_nMailServerPort);
	m_objIni.SetValue("SMTP", "Server Mail Address",m_strMailServerAddress);
	m_objIni.SetValue("SMTP", "Server Mail User",	m_strMailServerUserName);
	m_objIni.SetValue("SMTP", "Server Mail Password",m_strMailServerPassword);

	m_objIni.SetValue("User SMTP", "User Mail SMTP Server",	m_strUserSMTPMailServer);
	m_objIni.SetValue("User SMTP", "User Server Port",m_nUserSMTPMailServerPort);
	m_objIni.SetValue("User SMTP", "User Server Mail Address",m_strUserSMTPMailServerAddress);
	m_objIni.SetValue("User SMTP", "User Server Mail User",	m_strUserSMTPMailServerUserName);
	m_objIni.SetValue("User SMTP", "User Server Mail Password",m_strUserSMTPMailServerPassword);

	m_objIni.SetValue("EmailNotificator", "NotificatorIPAddress",	m_strNotificatorServerIP);
	m_objIni.SetValue("EmailNotificator", "NotificatorPort",		m_nNotificatorServerPort);

	m_objIni.SetValue("Logging", "Log Level",												m_nLogLevel);
	m_objIni.SetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize);
	m_objIni.SetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize);

	m_objIni.SetValue("Backup", "Backup Frequency",		m_nBackupFreq);
	m_objIni.SetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.SetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.SetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.SetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.SetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
	m_objIni.SetValue("Backup", "Delete old Backup Files Older Than",m_nDeleteOldBackupsDay);

	m_objIni.SetValue("Reply Server", "Max. time to keep emails (hours)",				m_nTimeToKeepEmailsHr);
	m_objIni.SetValue("Reply Server", "Max. emails to keep per user",					m_nMaxEmailCnt);
	m_objIni.SetValue("Reply Server", "Max. time before registration expires (hours)",	m_nRegistrationTimeOutHr);
	m_objIni.SetValue("Reply Server", "Email Scan Frequency (seconds)",	m_nScanFrequency);

	m_objIni.SetValue("Self Test Features", "StartUserNameToAdd (test procedure will add 4 digits at end, e.g. user0000, specify as 'user' or 'user@domain.com')",	m_strStartUserNameAdd);
	m_objIni.SetValue("Self Test Features", "StartPassWordToAdd (test procedure will add 4 digits at end, e.g. pw0000)",	m_strStartPassWordAdd);
	m_objIni.SetValue("Self Test Features", "CountUserToAdd",		m_nCountUserAdd);
	m_objIni.SetValue("Self Test Features", "StartCounterToAdd (start digits from this value)",		m_nStartCounterAdd);
	m_objIni.SetValue("Self Test Features", "LastUserNameAdded",	m_strLastUserNameAdded);
	m_objIni.SetValue("Self Test Features", "NextTestTime",	m_strNextTestTime);

	return m_objIni.Save();
}



bool ServerIniFile::CheckValues()
{
	if(m_nMaxConnections<=0) return false;
	if(m_nMaxDbConnections<=0) return false;
	if(m_nMinDbConnections<=0) return false;
	if(m_nSSLMode<0 || m_nSSLMode>1) return false;
	if(m_nAutoStartService<0 || m_nAutoStartService>1) return false;
	if(m_nAppServerPort<0 || m_nAppServerPort>65536) return false;

	if(m_nLogBufferSize<0) return false;

	//reset to default log level:
	if(m_nLogLevel<0)m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	if(m_nLogLevel>Logger::LOG_LEVEL_SPEED_TEST)m_nLogLevel=Logger::LOG_LEVEL_SPEED_TEST;

	return true;
}



bool ServerIniFile::SaveDefaults(QString strFile)
{
	m_strAppServerIPAddress="0.0.0.0";
	m_nAppServerPort=9999;
	m_nMaxConnections=2000;
	m_nMaxDbConnections=300;
	m_nMinDbConnections=10;
	m_nSSLMode=0;
	m_nAutoStartService=1;
	m_nDeleteOldBackupsDay=0;

	m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	m_nLogMaxSize=10;				
	m_nLogBufferSize=64;	
	m_strAppleReceiptValidationUrl="https://buy.itunes.apple.com/verifyReceipt";
	m_strAppleReceiptPasswordCheck="50b15439673649febeca6b585720f865";

	m_strHTTPRegisterAddress="http://everxconnect.com/registration";

	m_strMailServer="mail.sokrates.ch";
	m_strMailServerAddress="sokrates1@sokrates.ch";

	m_nTimeToKeepEmailsHr=72;
	m_nMaxEmailCnt=50;
	m_nRegistrationTimeOutHr=24;

	m_nBackupFreq=2;
	m_nBackupDay=0;
	m_nLockRegistration=0;
	m_nLockWriteDatabase=0;
	m_nExpireEmailAccountDays=30;
	m_nNumCloudTestingPeriodDays=30;

	m_nMailServerPort=25;
	m_nUserSMTPMailServerPort=25;
	m_strNotificatorServerIP="localhost";
	m_nNotificatorServerPort=41565;
	m_nScanFrequency=60;

	m_strStartUserNameAdd="";
	m_strStartPassWordAdd="";
	m_nCountUserAdd=0;
	m_nStartCounterAdd=0;

	return Save(strFile);
}
