//=============================================================================
// ByeResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef BYE_RESPONSE_H
#define BYE_RESPONSE_H


/// Class that represents an IMAP4 BYE response

class MAILPP_API ByeResponse : public Response
{
public:

    /// Default constructor
    ByeResponse();

    /// Copy constructor
    ByeResponse(const ByeResponse& other);

    /// Destructor
    virtual ~ByeResponse();

    /// Assignment operator
    ByeResponse& operator = (const ByeResponse& other);

    /// Returns the response code
    const char* ResponseCode() const;

    /// Returns the response text
    const char* ResponseText() const;

    /// Sets parameters for the response
    void SetParams(const char* s);

private:

    char* mResponseCode;
    char* mResponseText;

    void Clear();
};

#endif
