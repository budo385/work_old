//=============================================================================
// Response.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef RESPONSE_H
#define RESPONSE_H


/// Abstract class that represents an IMAP4 server response

class MAILPP_API Response
{
public:

    enum TypeCode {
        NULL_,
        OTHER,
        BAD,
        BYE,
        CAPABILITY,
        CONTINUATION,
        EXISTS,
        EXPUNGE,
        FETCH,
        FLAGS,
        LIST,
        LSUB,
        NO,
        OK,
        PREAUTH,
        RECENT,
        SEARCH,
        STATUS,
        QUOTA,
        QUOTAROOT,
        NAMESPACE
    };

    /// Destructor
    virtual ~Response();

    /// Returns the name of the response.
    const char* Name() const;

    /// Returns the type of the response as an enumerated value.
    TypeCode Type() const;

protected:

    /// Constructor that takes a name argument
    Response(const char* name, TypeCode type);

    /// Copy constructor
    Response(const Response& other);

    /// Assignment operator
    Response& operator = (const Response& other);

    char* mName;
    TypeCode mType;

    static void ParseResponseCode(const char* aParams,
        char*& aResponseCode, char*& aResponseText);
    static void NextWord(const char* str, int strLen, int* offset,
        int* length);

    void ParseQuotedString(const char* aStr, int* aLength);
    void ParseAtom(const char* aStr, int* aLength);
    void Unquote(char* aStr);

private:

    Response() {}
};

#endif
