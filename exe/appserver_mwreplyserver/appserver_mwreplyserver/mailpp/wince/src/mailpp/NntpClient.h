//=============================================================================
// NntpClient.h
//
// Copyright (c) 2000-2005 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef NNTP_CLIENT_H
#define NNTP_CLIENT_H

class ProtocolListener;

/// Class that represents an NNTP client connection

class MAILPP_API NntpClient
{
    friend class NntpClientErrorHandler;

public:

    enum {
        FIRST=1,
        LAST=2,
        CONVERT_EOL=4
    };

    /// Default constructor
    NntpClient();

    /// Destructor
    virtual ~NntpClient();

    /// Sets a Trace Output instance
    void SetTraceOutput(TraceOutput* out, bool takeOwnership);

    /// Sets the timeout value for network send and receive operations
    void SetTimeout(int secs);

    /// Sets the timeout value for network receive operations
    void SetReceiveTimeout(int secs);

    /// Sets the timeout value for network send operations
    void SetSendTimeout(int secs);

    /// Sets the listener object to process multiple line responses
    void SetListener(ProtocolListener* listener, bool del);

    /// Cancels a currently executing command
    void Cancel();

    /// Opens a connection to an NNTP server
    int Connect(const char* address, int port=119, bool wait=true);

    /// Opens a TLS connection over an existing TCP connection
    int ConnectTls(const char* serverHostName, bool wait=false);

    /// Establishes a TLS connection over an existing TCP connection
    int ConnectTlsOpenSSL(const char* serverHostName, void* ssl,
        bool wait=false);

    /// Disconnects from the server
    void Disconnect();

    /// Sends the MODE READER command
    int ModeReader();

    /// Sends the LIST EXTENSIONS command
    int ListExtensions();

    /// Sends the GROUP command
    int Group(const char* name);

    /// Sends the LAST command
    int Last();

    /// Sends the NEXT command
    int Next();

    /// Sends the ARTICLE command
    int Article(const char* messageId);

    /// Sends the ARTICLE command
    int Article(uint32_t articleNum = 0);

    /// Sends the HEAD command
    int Head(const char* messageId);

    /// Sends the HEAD command
    int Head(uint32_t articleNum = 0);

    /// Sends the BODY command
    int Body(const char* messageId);

    /// Sends the BODY command
    int Body(uint32_t articleNum = 0);

    /// Sends the STAT command
    int Stat(const char* messageId);

    /// Sends the STAT command
    int Stat(uint32_t articleNum = 0);

    /// Sends the POST command
    int Post();

    /// Sends the LIST command
    int List();

    /// Sends the LIST ACTIVE command
    int ListActive(const char* wildmat = 0);

    /// Sends the LIST OVERVIEW.FMT command
    int ListOverviewFmt();

    /// Sends the OVER command
    int Over(const char* range=0);

    /// Sends the XOVER command
    int Xover(const char* range=0);

    /// Sends the NEWGROUPS command
    int NewGroups(const char* date, const char* time, bool gmt);

    /// Sends the AUTHINFO USER command
    int AuthinfoUser(const char* username);

    /// Sends the AUTHINFO PASS command
    int AuthinfoPass(const char* password);

    /// Sends the QUIT command
    int Quit();

    /// Sends content as text lines
    int SendLines(const char* bytes, int length, int options);

    /// Gets the server's reply code
    int ReplyCode() const;

    /// Gets the text of the server's reply
    const char* ReplyText() const;

    /// Gets the platform-independent error code for the last error
    Error ErrorCode() const;

    /// Gets the platform-specific error code for the last error
    int OsErrorCode() const;

    /// Gets the error message for the last error
    const char* ErrorMessage() const;

    /// Sends the specified command
    int SendCommand(const char* commandStr);

    /// Receives a single line response
    int ReceiveSingleLineResponse();

    /// Receives a multiple line response
    int ReceiveMultipleLineResponse();

private:

    class ErrorHandler* mErrorHandler;
    class CriticalSectionImpl* mCriticalSectionImpl;
    class Socket* mSocket;

    bool mDeleteTraceOut;
    TraceOutput* mTraceOut;
    bool mIsConnected;
    int mReceiveTimeout;
    int mSendTimeout;
    int mBufferSize;
    char* mBuffer;
    Error mErrorCode;
    int mOsErrorCode;
    char* mErrorMessage;

    int mReplyCode;
    int mReplyTextSize;
    char* mReplyText;

    class ProtocolListener* mListener;
    int mDeleteListener;
    int mLastByte;

    int pReceiveLines();
    int pSendLinesBuffered(const char* bytes, int length, int options);
    int pSendLinesUnbuffered(const char* bytes, int length, int options);
    void pOnConnectionDropped();
    void pSetError(Error errorCode, int osErrorCode, const char* message);
    void pPrintCommand(const char* str);
    void pPrintResponse(const char* str);
    void pPrint(const char* str);
};

#endif
