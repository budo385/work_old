//=============================================================================
// AddressDataSequence.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef ADDRESS_DATA_SEQUENCE_H
#define ADDRESS_DATA_SEQUENCE_H

class ListItem;
class AddressData;


/// Container class for %AddressData objects

class MAILPP_API AddressDataSequence
{
public:

    /// Default constructor.
    AddressDataSequence();

    /// Constructor that takes an initial capacity.
    AddressDataSequence(int initialCapacity);

    /// Copy constructor.
    AddressDataSequence(const AddressDataSequence& other);

    /// Destructor.
    ~AddressDataSequence();

    /// Assignment operator.
    AddressDataSequence& operator = (const AddressDataSequence& other);

    /// Gets the number of %AddressData objects in the container.
    unsigned NumElements() const;

    /// Inserts an %AddressData object at the specified position.
    void InsertElement(unsigned pos, AddressData* element);

    /// Appends an %AddressData object.
    void AppendElement(AddressData* element);

    /// Removes an %AddressData object from the collection.
    AddressData* RemoveElement(unsigned index);

    /// Gets the %AddressData object at the specified position.
    AddressData* ElementAt(unsigned index) const;

    /// Removes and deletes all objects from the collection.
    void DeleteAll();

    /// Called by the parser to set the data for this object.
    void ImportData(ListItem* root);

private:

    unsigned mNumElements;
    unsigned mMaxElements;
    AddressData** mElements;

    void Copy(const AddressDataSequence&);
};

#endif
