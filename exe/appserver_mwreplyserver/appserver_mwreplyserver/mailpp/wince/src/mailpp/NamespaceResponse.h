//=============================================================================
// NamespaceResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef NAMESPACE_RESPONSE_H
#define NAMESPACE_RESPONSE_H


/// Class that represents an IMAP4 NAMESPACE response

class MAILPP_API NamespaceResponse : public Response
{
public:

    /// Default constructor
    NamespaceResponse();

    /// Copy constructor
    NamespaceResponse(const NamespaceResponse& other);

    /// Destructor
    virtual ~NamespaceResponse();

    /// Assignment operator
    NamespaceResponse& operator = (const NamespaceResponse& other);

    /// Gets the number of personal namespaces
    unsigned NumPersonalNamespaces() const;

    /// Gets the specified personal namespace
    const NamespaceAttributes& PersonalNamespaceAt(unsigned index) const;

    /// Gets the number of user namespaces
    unsigned NumUserNamespaces() const;

    /// Gets the specified user namespace
    const NamespaceAttributes& UserNamespaceAt(unsigned index) const;

    /// Gets the number of shared namespaces
    unsigned NumSharedNamespaces() const;

    /// Gets the specified shared namespace
    const NamespaceAttributes& SharedNamespaceAt(unsigned index) const;

    /// Called by the parser to set the data for this object
    void ImportData(ListItem* root);

private:
    mailpp::NamespaceAttributesSequence mPersonalNamespaces;
    mailpp::NamespaceAttributesSequence mUserNamespaces;
    mailpp::NamespaceAttributesSequence mSharedNamespaces;
};

#endif


