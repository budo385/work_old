// openssl_win32.cpp -- OpenSSL initialization

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <wincrypt.h>
#include <stdio.h>
#include <openssl/crypto.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>

// OpenSSL static locks

static HANDLE *locks;

static void locking_callback(int mode, int n, const char* /*file*/,
    int /*line*/)
{
    if (mode & CRYPTO_LOCK) {
        WaitForSingleObject(locks[n], INFINITE);
    }
	else {
		ReleaseMutex(locks[n]);
    }
}

static void thread_initialize(void)
{
    // Create Win32 Mutexes
    int numLocks = CRYPTO_num_locks();
    locks = (HANDLE *) OPENSSL_malloc(numLocks*sizeof(HANDLE));
    for (int i = 0; i < numLocks; ++i) {
        locks[i] = CreateMutex(NULL,FALSE,NULL);
    }
    // Set the locking callback function
    CRYPTO_set_locking_callback(locking_callback);
}

static void thread_finalize()
{
    // Remove the locking callback function
    CRYPTO_set_locking_callback(0);
    // Free all the Mutexes
    int numLocks = CRYPTO_num_locks();
    for (int i = 0; i < numLocks; ++i) {
        CloseHandle(locks[i]);
    }
	OPENSSL_free(locks);
}

// OpenSSL dynamic locks

struct CRYPTO_dynlock_value {
};

void dyn_create_function(const char* /*file*/, int /*line*/)
{
}

void dyn_lock_function(int mode, CRYPTO_dynlock_value* lock,
    const char* /*file*/, int /*line*/)
{
}

// OpenSSL random seed

static int random_initialize()
{
    // You can use the following code if you want.  However, OpenSSL 0.9.7
    // or later initializes the PRNG using CryptGenRandom() as well as
    // other Windows API calls, so this code is not necessary.
#if 0
    HCRYPTPROV hProv;
    LPCTSTR pszContainer = 0;  // default container
    LPCTSTR pszProvider = MS_DEF_PROV;  // default provider
    DWORD dwProvType = PROV_RSA_FULL;
    DWORD dwFlags = 0;
    BOOL ok = CryptAcquireContext(&hProv, pszContainer, pszProvider,
        dwProvType, dwFlags);
    if (! ok) {
        return -1;
    }
    const size_t bufferSize = 256;
    unsigned char buffer[bufferSize];
    ok = CryptGenRandom(hProv, bufferSize, buffer);
    if (! ok) {
        return -1;
    }
    RAND_seed(buffer, bufferSize);
#endif
    return 0;
}

static void random_finalize()
{
    RAND_cleanup();
}

// Externally visible functions

int openssl_initialize()
{
    SSL_load_error_strings();
    SSL_library_init();
    thread_initialize();
    // OpenSSL does, in fact, automatically seed the PRNG using various
    // Windows API function calls.  However, this feature is undocumented,
    // so perhaps it's best to initialize the PRNG explicitly.
    random_initialize();
	return 0;
}

void openssl_finalize()
{
    random_finalize();
    thread_finalize();
}
