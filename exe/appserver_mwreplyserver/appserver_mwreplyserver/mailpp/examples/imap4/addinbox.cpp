// addinbox.cpp
//
// This example program demonstrates how to use the APPEND command to
// append messages to a mail folder (which IMAP4 calls a "mailbox").
//
// The program reads a collection of messages and appends them to the
// INBOX.  (This is also a good way to add messages to an IMAP4 account for
// testing.)  The program reads all the files in a specified directory.
// Each file in the directory should contain a single message.
//
// Note: This program does not do the kind of error checking that would
// be required in a real application.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mailpp/mailpp.h>
using namespace mailpp;

#if defined(WIN32)
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
#  define snprintf _snprintf
#endif

#if defined(__unix)
#  include <sys/types.h>
#  include <sys/stat.h>
#  include <unistd.h>
#  include <dirent.h>
#endif


typedef struct File {
    struct File *next;
    char *name;
    unsigned int size;
} File;


File *GetAllFilesInDirectory(const char* directory);


int main(int argc, char** argv)
{
    // Get commandline arguments

    if (argc < 5) {
        printf("usage:\n");
        printf("    %s host user password directory\n", argv[0]);
        exit(1);
    }
    const char* host = argv[1];
    const char* user = argv[2];
    const char* password = argv[3];
    const char* directory = argv[4];

    // Get list of all files in directory.  There should be one message
    // per file, and every file should contain a message.

    File* files = GetAllFilesInDirectory(directory);

    // Create an IMAP4 client

    Imap4Client client;

    // For this example, set debugging on, so that we can see the protocol
    // messages exchanged.

    StdTraceOutput traceOut;
    client.SetTraceOutput(&traceOut, false);

    // Open connection to the server

    client.Connect(host);

    // Send LOGIN command

    client.Login(user, password);

    // For each file...

    for (File* file = files; file; file = file->next) {

        // Open the file and read it

        char path[256];
        snprintf(path, sizeof(path), "%s/%s", directory, file->name);
        FILE* in = fopen(path, "rb");
        char* buffer = (char *) malloc(file->size+10);
        int n = (int) fread(buffer, 1, file->size, in);
        fclose(in);
        buffer[n] = 0;

        // Send APPEND command to add the message to the INBOX

        client.Append("INBOX", NULL, NULL, buffer);
    }

    // Send LOGOUT command to end this connection

    client.Logout();

    // Call Disconnect to close the socket

    client.Disconnect();

    return 0;
}


#if defined(__unix)

// Returns a linked list of File structures, where each File contains the
// name and length of a file in the directory

File *GetAllFilesInDirectory(const char* directory)
{
    int err;
    size_t filelength;
    const char *filename;
    char pathname[256];
    DIR *pdir;
    struct dirent *pfile;
    File *head, *file;
    struct stat statbuf;

    head = NULL;

    pdir = opendir(directory);
    if (pdir) {
        while (1) {
            pfile = readdir(pdir);
            if (! pfile) {
                break;
            }
            filename = pfile->d_name;
            snprintf(pathname, sizeof(pathname), "%s/%s", directory, filename);
            err = stat(pathname, &statbuf);
            if (! err) {
                if (! S_ISDIR(statbuf.st_mode)) {
                    filelength = statbuf.st_size;
                    file = (File*) malloc(sizeof(File));
                    if (file != NULL) {
                        file->size = filelength;
                        file->name = strdup(filename);
                        file->next = head;
                        head = file;
                    }
                    else {
                        perror("malloc");
                    }
                }
            }
            else /* if (err) */ {
                perror("stat");
            }
        }
        closedir(pdir);
    }
    else /* if (! pdir) */ {
        perror("opendir");
    }
    return head;
}

#endif /* defined(__unix) */

#if defined(WIN32)

// Returns a linked list of File structures, where each File contains the
// name and length of a file in the directory

File *GetAllFilesInDirectory(const char* directory)
{
    char pattern[256];
    WIN32_FIND_DATA findData;
    HANDLE findHandle;
    BOOL more;
    File *head, *file;

    head = NULL;
    _snprintf(pattern, sizeof(pattern), "%s\\*.*", directory);
    findHandle = FindFirstFile(pattern, &findData);
    if (findHandle != INVALID_HANDLE_VALUE) {
        do {
            file = (File*) malloc(sizeof(File));
            if (file != NULL && strcmp(findData.cFileName, ".") != 0
                && strcmp(findData.cFileName, "..") != 0) {
                file->size = findData.nFileSizeLow;
                file->name = strdup(findData.cFileName);
                file->next = head;
                head = file;
            }
            more = FindNextFile(findHandle, &findData);
        } while (more);
        FindClose(findHandle);
    }
    else /* if (findHandle == INVALID_HANDLE_VALUE) */ {
        printf("FindFirstFile failed");
    }
    return head;
}

#endif  /* defined(WIN32) */
