// appendfile.cpp
//
// This example program demonstrates how to use the APPEND command to
// append a message to a mail folder (which IMAP4 calls a "mailbox").  This
// program uses the Imap4Client::Append member function that takes a
// StreamBuffer parameter.  StreamBuffer is an interface class (it has only
// pure virtual functions) that provides the message source.  This program
// defines a FileStreamBuffer class, which implements the StreamBuffer
// interface.
//
// Note: This program does not do the kind of error checking that would
// be required in a real application.
//


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mailpp/mailpp.h>
using namespace mailpp;


class FileStreamBuffer : public StreamBuffer
{
public:
    FileStreamBuffer();
    int Open(const char* filename);
    virtual int Size();
    virtual int Read(char* buffer, int bufferSize);
private:
    int mSize;
    FILE* mStream;
};


FileStreamBuffer::FileStreamBuffer()
{
    mSize = 0;
    mStream = 0;
}


int FileStreamBuffer::Open(const char* filename)
{
    int retVal = -1;
    mStream = fopen(filename, "rb");
    if (mStream != 0) {
        fseek(mStream, 0, SEEK_END);
        mSize = ftell(mStream);
        fseek(mStream, 0, SEEK_SET);
        retVal = 0;
    }
    return retVal;
}


int FileStreamBuffer::Size()
{
    return mSize;
}


int FileStreamBuffer::Read(char* buffer, int bufferSize)
{
    if (mStream != 0) {
        return (int) fread(buffer, 1, bufferSize, mStream);
    }
    else {
        return 0;
    }
}


int main(int argc, char** argv)
{
    // Get commandline arguments

    if (argc < 5) {
        printf("usage:\n");
        printf("    %s host user password folder filename\n", argv[0]);
        exit(1);
    }
    const char* host = argv[1];
    const char* user = argv[2];
    const char* password = argv[3];
    const char* folder = argv[4];
    const char* filename = argv[5];

    // Create an IMAP4 client
    
    Imap4Client client;

    // For this example, set debugging on, so that we can see the protocol
    // messages exchanged.

    StdTraceOutput traceOut;
    client.SetTraceOutput(&traceOut, false);

    // Open connection to the server

    client.Connect(host);

    // Send LOGIN command

    client.Login(user, password);

    // Send APPEND command to add the message to the folder

    FileStreamBuffer fbuf;
    fbuf.Open(filename);
    client.Append(folder, 0, 0, fbuf);

    // Send LOGOUT command to end this connection

    client.Logout();

    // Call Disconnect to close the socket

    client.Disconnect();

    return 0;
}
