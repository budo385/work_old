// archive.cpp
//
// This example program demonstrates how to use a SEARCH command.  It sends
// a SEARCH command to get all the messages older than a certain date, and
// then moves all those messages to a folder called "archive".
//
// Note: This program does not do the kind of error checking that would
// be required in a real application.
//

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mailpp/mailpp.h>
using namespace std;
using namespace mailpp;

#if defined(WIN32)
#  define snprintf       _snprintf
#  define strcasecmp(x,y) _stricmp(x,y)
#endif
using std::string;
using std::cout;
using std::endl;


int main(int argc, char** argv)
{
    // Get commandline arguments

    if (argc < 6) {
        cout << "usage:\n" << endl;
        cout << "    " << argv[0] << " host user password date mailbox\n"
            << endl;
        cout << "date format: dd-MMM-yyyy" << endl;
        cout << "example:\n" << endl;
        cout << "    " << argv[0]
            << " 192.168.1.20 pete secret 31-May-2001 archive\n" << endl;
        exit(1);
    }
    const char* host = argv[1];
    const char* user = argv[2];
    const char* password = argv[3];
    const char* date = argv[4];
    const char* mailbox = argv[5];

    // Create an IMAP4 client

    Imap4Client client;

    // For this example, set debugging on, so that we can see the protocol
    // messages exchanged.

    StdTraceOutput traceOut;
    client.SetTraceOutput(&traceOut, false);

    // Open a connection to the server

    client.Connect(host);

    // Send a LOGIN command

    client.Login(user, password);

    // Send SELECT to select the INBOX folder

    client.Select("INBOX");

    // Send a SEARCH command to find all messages sent before the
    // specified date

    char criteria[64];
    snprintf(criteria, sizeof(criteria), "SENTBEFORE %s", date);
    client.Search(0, criteria);

    // Iterate over all the responses to find the SEARCH response

    string set;
    int numResponses = client.NumResponses();
    int i, j;
    for (i=0; i < numResponses; ++i) {
        if (strcasecmp(client.ResponseAt(i).Name(), "search") == 0) {

            // Process the SEARCH response

            const SearchResponse& resp = (const SearchResponse&)
                client.ResponseAt(i);

            // Iterate over the matches.  Append the sequence numbers to
            // a string, separated by commas.

            int numMatches = resp.NumMatches();
            for (j=0; j < numMatches; ++j) {
                int seqNum = resp.Match(j);
                char seqNumBuf[24];
                snprintf(seqNumBuf, sizeof(seqNumBuf), "%d", seqNum);
                if (j > 0) set.append(",");
                set.append(seqNumBuf);
            }
            break;
        }
    }

    // If there were any messages that matched the search criterion (that
    // is, messages that were sent before the specified date), then copy
    // those message to the archive folder.  After copying, mark the
    // messages in the INBOX to be deleted.
    //
    // Note: In a real program, you would probably want to limit the number
    // of sequence numbers that you send in a single COPY or STORE command,
    // because some server implementations have a limit on the length of a
    // line that they will handle.  It's possible that the number of
    // matches could be 10,000 or more, which would make the line for the
    // COPY or STORE command to be very long.  Another possibility would be
    // to collapse the numbers into ranges, such as "1:10" instead of
    // "1,2,3,4,5,6,7,8,9,10".

    if (set.length() > 0) {

        // Send a COPY command to copy the messages to the archive folder
        // (mailbox)

        client.Copy(set.c_str(), mailbox);

        // Set the \Deleted flags on those messages in the INBOX

        client.Store(set.c_str(), "+FLAGS.SILENT", "(\\Deleted)");
    }

    // Send a CLOSE command to close the INBOX.  This causes all the
    // messages we marked with the \Deleted flag to be silently expunged.
    // The next time we select the INBOX, those messages will be gone.

    client.Close();

    // Send a LOGOUT command to end this connection

    client.Logout();

    // Call Disconnect to close the socket

    client.Disconnect();

    return 0;
}
