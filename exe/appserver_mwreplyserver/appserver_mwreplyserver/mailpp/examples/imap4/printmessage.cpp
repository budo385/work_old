// printMessage.cpp
//
// This example program demonstrates how to retrieve the essential header
// information (To, From, Date, Subject) and the memo text of a message,
// while leaving the other body parts (assumed to be attachments) on the
// server.
//
// So, how do you determine which part of a multipart message contains the
// memo text?  Well, the strategy used here is to find the first "leaf"
// body part, and if it contains text, then it is the memo text of the
// message.  This makes the most sense, because this is also the text that
// is first seen by a user whose mail user agent doesn't support MIME.
// This approach works with Outlook, Express, Netscape, Eudora, Pine,
// and many other common mail user agents.
//
// Note: This program does not do the kind of error checking that would
// be required in a real application.
//

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mailpp/mailpp.h>
using namespace std;
using namespace mailpp;

#if defined(WIN32)
#  define snprintf _snprintf
#  define strcasecmp _stricmp
#endif

using std::string;
using std::cout;
using std::endl;


// Attachment class saves information about body parts that are
// interpreted as attachments

class Attachment {
public:
    Attachment() { size = 0; }
    unsigned size;
    string filename;
    string contentType;
    string contentDescription;
    string bodyPartRef;    // reference we would use to retrieve the body part
};


// Message class saves essential information about the message, including
// a list of attachments

class Message {
public:
    Message() {size=0;numLines=0;partsCount=0;numAttachments=0;attachments=0;}
    ~Message();
    unsigned size;
    unsigned numLines;
    string to;
    string cc;
    string bcc;
    string from;
    string subject;
    string date;
    int partsCount;  // counts how many parts we have examined
    string memoRef;  // reference needed to retrieve memo text part
    string memo;
    int numAttachments;
    Attachment** attachments;
    void appendAttachment(Attachment* attachment);
};


Message::~Message()
{
    for (int i=0; i < numAttachments; ++i) {
        delete attachments[i];
    }
    if (attachments != 0) {
        free(attachments);
    }
}


void Message::appendAttachment(Attachment* attachment)
{
    Attachment** attachs = (Attachment**) realloc(attachments,
        (numAttachments+1)*sizeof(Attachment*));
    if (attachs != 0) {
        attachments = attachs;
        attachments[numAttachments] = attachment;
        ++numAttachments;
    }
    else {
        delete attachment;
    }
}


void processFetchFull(const Imap4Client& client, Message& message);
void processFetchBodySection(const Imap4Client& client, Message& message);
void processEnvelope(const EnvelopeData& envelope, Message& message);
void processBodyStruct(const BodyStructureData& bodyStruct,
    const string& partRef, Message& message);


int main(int argc, char** argv)
{
    // Get commandline arguments

    if (argc < 5) {
        printf("usage:\n");
        printf("    %s host user password seq_number\n", argv[0]);
        exit(1);
    }
    const char* host = argv[1];
    const char* user = argv[2];
    const char* password = argv[3];
    int seqNumber = atoi(argv[4]);

    // Create an IMAP4 client

    Imap4Client client;

    // For this example, set debugging on, so that we can see the protocol
    // messages exchanged.

    StdTraceOutput traceOut;
    client.SetTraceOutput(&traceOut, false);

    // Open connection to the server

    client.Connect(host);

    // Send LOGIN command

    client.Login(user, password);

    // Send SELECT to select the folder

    client.Select("INBOX");

    // Send a FETCH command

    client.Fetch(seqNumber, "(FLAGS INTERNALDATE RFC822.SIZE ENVELOPE "
        "BODYSTRUCTURE)");

    // Process all the responses to extract the information we need

    Message message;
    processFetchFull(client, message);

    // Set debugging off before we send the second FETCH command, since we
    // would see a lot of debugging output

    client.SetTraceOutput(0, false);

    // If we have identified a body part that contains the memo text, then
    // send a FETCH command to get it

    if (message.memoRef.size() > 0) {
        string bodySection = string("(BODY[") + message.memoRef + string("])");
        client.Fetch(seqNumber, bodySection.c_str());
        processFetchBodySection(client, message);
    }

    // Now print the information

    cout << "---------------------------------------"
        "---------------------------------------" << endl;
    cout << "   Date: " << message.date << endl;
    cout << "   From: " << message.from << endl;
    cout << "     To: " << message.to << endl;
    cout << "     Cc: " << message.cc << endl;
    cout << "    Bcc: " << message.bcc << endl;
    cout << "Subject: " << message.subject << endl;
    cout << "---------------------------------------"
        "---------------------------------------" << endl;
    cout << message.memo << endl;
    cout << "---------------------------------------"
        "---------------------------------------" << endl;
    for (int i=0; i < message.numAttachments; ++i) {
        cout << " Attach: " << message.attachments[i]->contentType;
        cout << " " << message.attachments[i]->filename;
        cout << " (" << message.attachments[i]->size << " bytes)" << endl;
    }
    cout << "---------------------------------------"
        "---------------------------------------" << endl;

    // Send LOGOUT command to end this connection

    client.Logout();

    // Call Disconnect to close the socket

    client.Disconnect();

    return 0;
}


// Processes the responses from the FETCH (...) command and saves essential
// information about the message

void processFetchFull(const Imap4Client& client, Message& message)
{
    // Iterate over the responses to find the FETCH response

    int numResponses = client.NumResponses();
    for (int i=0; i < numResponses; ++i) {
        const Response& r = client.ResponseAt(i);
        if (strcasecmp(r.Name(), "fetch") == 0) {

            // Process this FETCH response

            const FetchResponse& response = (const FetchResponse&) r;

            // Save the message size

            message.size = response.Rfc822Size();

            // If there is an ENVELOPE structure, then get the header
            // information from it

            const EnvelopeData* envelope = response.Envelope();
            if (envelope != 0) {
                processEnvelope(*envelope, message);
            }

            // If there is a BODYSTRUCTURE structure, then examine it to
            // find the body part that contains the memo text

            const BodyStructureData* bodyStruct = response.BodyStructure();
            if (bodyStruct != 0) {
                processBodyStruct(*bodyStruct, string(), message);
            }
        }
    }
}


// Processes the responses from the FETCH (BODY[...]) command and saves the
// memo text

void processFetchBodySection(const Imap4Client& client, Message& message)
{
    // Iterate over the responses to find the FETCH response

    int numResponses = client.NumResponses();
    for (int i=0; i < numResponses; ++i) {
        const Response& r = client.ResponseAt(i);
        if (strcasecmp(r.Name(), "fetch") == 0) {

            // Process this FETCH response

            const FetchResponse& response = (const FetchResponse&) r;

            // If there is a body section item, then process it to get
            // the memo text

            const BodySectionData* bodySection = response.BodySection();
            if (bodySection != 0) {
                message.memo = string(bodySection->Content(),
                    bodySection->Length());
            }
        }
    }
}


// Processes an ENVELOPE structure and saves essential information from the
// header fields

void processEnvelope(const EnvelopeData& envelope, Message& message)
{

    // To

    int n = envelope.NumTo();
    int i;
    for (i = 0; i < n; ++i) {
        if (i > 0) {
            message.to.append(", ");
        }
        const AddressData& to = envelope.To(i);
        if (to.PersonalName() != 0 && to.PersonalName()[0] != 0) {
            message.to.append(to.PersonalName());
        }
        else {
            message.to.append(to.MailboxName());
            message.to.append("@");
            message.to.append(to.HostName());
        }
    }

    // Cc

    n = envelope.NumCc();
    for (i = 0; i < n; ++i) {
        if (i > 0) {
            message.cc.append(", ");
        }
        const AddressData& cc = envelope.Cc(i);
        if (cc.PersonalName() != 0 && cc.PersonalName()[0] != 0) {
            message.cc.append(cc.PersonalName());
        }
        else {
            message.cc.append(cc.MailboxName());
            message.cc.append("@");
            message.cc.append(cc.HostName());
        }
    }

    // Bcc

    n = envelope.NumBcc();
    for (i = 0; i < n; ++i) {
        if (i > 0) {
            message.bcc.append(", ");
        }
        const AddressData& bcc = envelope.Bcc(i);
        if (bcc.PersonalName() != 0 && bcc.PersonalName()[0] != 0) {
            message.bcc.append(bcc.PersonalName());
        }
        else {
            message.bcc.append(bcc.MailboxName());
            message.bcc.append("@");
            message.bcc.append(bcc.HostName());
        }
    }

    // From

    n = envelope.NumFrom();
    for (i = 0; i < n; ++i) {
        if (i > 0) {
            message.from.append(", ");
        }
        const AddressData& from = envelope.From(i);
        if (from.PersonalName() != 0 && from.PersonalName()[0] != 0) {
            message.from.append(from.PersonalName());
        }
        else {
            message.from.append(from.MailboxName());
            message.from.append("@");
            message.from.append(from.HostName());
        }
    }

    // Subject

    message.subject = envelope.Subject();

    // Date

    message.date = envelope.Date();
}


// Processes a BODYSTRUCTURE structure and tries to determine which body
// part contains the memo text.  Saves a reference to this body part.
// Since we call this function recursively, we also have a 'level'
// parameter that indicates the recursion depth.

void processBodyStruct(const BodyStructureData& bodyStruct,
    const string& partRef, Message& message)
{
    // Check how many nested body parts are in this body part

    int numBodyParts = bodyStruct.NumBodyParts();
    if (numBodyParts == 0) {

        // If this is the first "leaf" body part we have seen, and if
        // the media type is "text", then this is the memo text part
        // we want.

        if (message.partsCount == 0
            && strcasecmp(bodyStruct.MediaType(), "text") == 0) {
            if (partRef.empty()) {
                message.memoRef = "TEXT";
            }
            else {
                message.memoRef = partRef;
            }
        }

        // Otherwise, this body part is considered an attachment

        else {

            // Save the information related to this attachment

            Attachment* attach = new Attachment;
            attach->size = bodyStruct.Size();
            attach->contentType = string(bodyStruct.MediaType()) + "/" +
                string(bodyStruct.MediaSubtype());
            attach->contentDescription = bodyStruct.ContentDescription();
            attach->bodyPartRef = partRef;
            for (int i=0; i < bodyStruct.NumContentTypeParams(); ++i) {
                if (strcasecmp(bodyStruct.ContentTypeParamName(i), "name")==0){
                    attach->filename = bodyStruct.ContentTypeParamValue(i);
                }
            }
            message.appendAttachment(attach);
        }
        ++message.partsCount;
    }
    else /* if (numBodyParts > 0) */ {

        // If there are "leaf" body parts, then call this function
        // recursively for each one

        for (int i=0; i < numBodyParts; ++i) {
            const BodyStructureData* nestedBodyStruct =
                bodyStruct.BodyPart(i);
            char s[64];
            if (partRef.size() > 0) {
                snprintf(s, sizeof(s), "%s.%d", partRef.c_str(), i+1);
            }
            else {
                snprintf(s, sizeof(s), "%d", i+1);
            }
            string childPartRef = s;
            processBodyStruct(*nestedBodyStruct, childPartRef, message);
        }
    }
}
