// SmtpSession.h

#include <mailpp/mailpp.h>

using mailpp::SmtpClient;

class MailTransaction;


class SmtpSession
{
public:

    SmtpSession(const char* hostname);
    virtual ~SmtpSession();

    int Open(const char* address, int port);
    void Close();
    int DoTransaction(MailTransaction& transaction);

protected:

    SmtpClient mClient;
	char* mHostname;
    mailpp::TraceOutput* mTraceOutput;

    virtual void ReportNetworkError(const char* message);
    virtual void ReportProtocolError(const char* message);
    int SayHello();

};
