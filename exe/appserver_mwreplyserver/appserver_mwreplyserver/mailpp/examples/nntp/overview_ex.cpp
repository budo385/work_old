// overview_ex.cpp
//
// This example program shows how to receive all the most important article
// headers from a newsgroup using the OVER (or XOVER) command.  The OVER
// command is documented in the current NNTP internet draft (which will
// eventually become the replacement for RFC 977).  The XOVER command
// is probably more widely implemented, but will never be part of the
// official standard.
//
// Two command line parameters are required: the name of the news server
// and the name of the newsgroup.
//

#include <mailpp/mailpp.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
using namespace std;
using namespace mailpp;

#if defined(WIN32)
#  define snprintf _snprintf
#endif


//
// OverListener -- this class processes the lines of text sent by the
// NNTP server in response to an OVER (or XOVER) command
//
class OverListener : public ProtocolListener {
public:
    OverListener(ostream&);
    virtual ~OverListener();
    virtual void ClearLines();
    virtual void LineReceived(const char* line);
private:
    ostream& out;
};

OverListener::OverListener(ostream& out_)
  : out(out_)
{
}

OverListener::~OverListener()
{
}

void OverListener::ClearLines()
{
}

void OverListener::LineReceived(const char* line)
{
	// The fields are separated by a TAB character
    char* ptr = (char*) line;
    // Article number
    int articleNum = strtoul(ptr, &ptr, 10);
    ++ptr;
    // Subject
    char subject[256];
    int n = (int) strcspn(ptr, "\t");
    int m = (n < sizeof(subject)-1) ? n : (sizeof(subject)-1);
    strncpy(subject, ptr, m);
    subject[m] = 0;
    ptr += n + 1;
    // Author
    char author[80];
    n = (int) strcspn(ptr, "\t");
    m = (n < sizeof(author)-1) ? n : (sizeof(author)-1);
    strncpy(author, ptr, m);
    author[m] = 0;
    ptr += n + 1;
    // Date
    char date[40];
    n = (int) strcspn(ptr, "\t");
    m = (n < sizeof(date)-1) ? n : (sizeof(date)-1);
    strncpy(date, ptr, m);
    date[m] = 0;
    ptr += n + 1;
    // Message-ID
    char messageId[80];
    n = (int) strcspn(ptr, "\t");
    m = (n < sizeof(messageId)-1) ? n : (sizeof(messageId)-1);
    strncpy(messageId, ptr, m);
    messageId[m] = 0;
    ptr += n + 1;
    // Reference
    char reference[80];
    n = (int) strcspn(ptr, "\t");
    m = (n < sizeof(reference)-1) ? n : (sizeof(reference)-1);
    strncpy(reference, ptr, m);
    reference[m] = 0;
    ptr += n + 1;
    // Number of bytes
    int bytes = strtoul(ptr, &ptr, 10);
    ++ptr;
    // Number of lines
    int lines = strtoul(ptr, &ptr, 10);
    ++ptr;
    out << "------------------------------------------------------------------"
        << endl;
    out << "Article: " << articleNum << endl;
    out << "Subject: " << subject << endl;
    out << "Author: " << author << endl;
    out << "Date: " << date << endl;
}


int main(int argc, char** argv)
{
    int retVal = 1;

    // Two command line parameters are required: the name of the news
    // server and the name of the newsgroup

    if (argc < 3) {
        printf("Usage: %s server newsgroup\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    const char* server = argv[1];
    const char* newsgroup = argv[2];

    // Create an NNTP client object

    NntpClient nntp;

    // Open a connection to the news server. Check for a 2xx reply code,
    // which indicates success.

    int err = nntp.Connect(server);
    if (! err && (nntp.ReplyCode()/100%10) == 2) {

        // Send a MODE READER command

        err = nntp.ModeReader();

        // Send a GROUP command, with the newsgroup name as a parameter.
        // Check for a 2xx reply code, which indicates success.

        err = nntp.Group(newsgroup);
        if (! err && (nntp.ReplyCode()/100%10) == 2) {

            // Parse the server's reply text, which is of this form:
            //
            // n f l s
            //
            // n = estimated number of articles
            // f = first article number
            // l = last article number
            // s = name of the newsgroup
            // (the fields are separated by a space)
            //
            // We want to get the first article number

            char* p = (char*) nntp.ReplyText();
            strtoul(p, &p, 10);
            int first = strtoul(p, &p, 10);
            int last = strtoul(p, &p, 10);

            // Create a listener object to receive the lines of the article
            // and print them to standard out

            OverListener listener(cout);
            nntp.SetListener(&listener, false);

            // Send an OVER command, with the the first- range
            // parameter.  Check for a 2xx reply code, which indicates
            // success.

            char range[40];
            snprintf(range, sizeof(range), "%d-", first);
            err = nntp.Over(range);
            if (! err && nntp.ReplyCode()/100%10 == 5) {
                // If the server didn't like the OVER command, try, the XOVER
                // command
                err = nntp.Xover(range);
            }
            if (! err && nntp.ReplyCode()/100%10 == 2) {
                retVal = 0;
            }
            else if (nntp.ReplyCode()/100%10 != 2) {
                cout << "Failure in OVER (or XOVER) command" << endl;
                cout << "The server's response was " << nntp.ReplyCode()
                     << " " << nntp.ReplyText() << endl;
            }
        }
        else if (nntp.ReplyCode()/100%10 != 2) {
            cout << "Failure in GROUP command" << endl;
            cout << "The server's response was " << nntp.ReplyCode()
                 << " " << nntp.ReplyText() << endl;
        }
    }

    // Send a QUIT command, unless there was a problem with the client/
    // server communication

    if (! err) {
        nntp.Quit();
    }

    // Close the connection

    nntp.Disconnect();

    return retVal;
}
