// article_ex.cpp
//
// This example program shows how to retrieve an article from a newsgroup.
//
// Three command line arguments are required: the name or address of the
// news server, the name of the newsgroup, and the number of the article to
// retrieve.
//
// To use the program, you will need a way to get a valid article number.
// One way to do this is to use the overview_ex example program to list
// summaries of all the articles in a newsgroup.
//

#include <mailpp/mailpp.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
using namespace mailpp;
#include "NntpListener.h"


int main(int argc, char** argv)
{
    int retval = 1;

    // Three command line arguments are required: the name or address of
    // the news server, the name of the newsgroup, and the number of the
    // article to retrieve.

    if (argc < 4) {
        printf("Usage: %s server newsgroup number\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    const char* server = argv[1];
    const char* newsgroup = argv[2];
    int articleNum = atoi(argv[3]);

    // Create an NNTP client object

    NntpClient nntp;

    // Open a connection to the news server. Check for a 2xx reply code,
    // which indicates success.

    int err = nntp.Connect(server);
    if (! err && (nntp.ReplyCode()/100%10) == 2) {

        // Send a MODE READER command

        err = nntp.ModeReader();

        // Send a GROUP command, with the newsgroup name as a parameter.
        // Check for a 2xx reply code, which indicates success.

        err = nntp.Group(newsgroup);
        if (! err && (nntp.ReplyCode()/100%10) == 2) {

            // Create a listener object to receive the lines of the article
            // and print them to standard out

            NntpListener listener(cout);
            nntp.SetListener(&listener, false);

            // Send an ARTICLE command, with the article number as a
            // parameter.  Check for a 2xx reply code, which indicates
            // success.

            err = nntp.Article(articleNum);
            if (! err && (nntp.ReplyCode()/100%10) == 2) {
                retval = 0;
            }
            else if (nntp.ReplyCode()/100%10 != 2) {
                cout << "Failure in ARTICLE command" << endl;
                cout << "The server's response was " << nntp.ReplyCode()
                     << " " << nntp.ReplyText() << endl;
            }
        }
        else if (nntp.ReplyCode()/100%10 != 2) {
            cout << "Failure in GROUP command" << endl;
            cout << "The server's response was " << nntp.ReplyCode()
                 << " " << nntp.ReplyText() << endl;
        }
    }

    // Send a QUIT command, unless there was a problem with the client/
    // server communication

    if (! err) {
        nntp.Quit();
    }

    // Show any error message

    if (err) {
        cout << "Error: (" << nntp.ErrorCode() << ") " << nntp.ErrorMessage()
             << endl;
    }

    // Close the connection

    nntp.Disconnect();

    return retval;
}
