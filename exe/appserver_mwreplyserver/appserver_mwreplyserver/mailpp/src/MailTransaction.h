// MailTransaction.h

#ifndef MAIL_TRANSACTION_H
#define MAIL_TRANSACTION_H

#include <fstream>


#ifndef WINCE
	#include "mailpp/mailpp.h"
#else
	#include "mailpp.h"
#endif

using mailpp::SmtpClient;
using std::ifstream;

class MailTransaction
{
public:

    MailTransaction();
    virtual ~MailTransaction();

    void SetFrom(const char* from);
    void AddTo(const char* to);
    void SetMessage(const char* message);
	void SetBody(const char* to);

    virtual int Send(SmtpClient& client);

	int m_nLastErr;
	std::string m_strLastErr;

protected:

    void ReportNetworkError(const char* message, SmtpClient& client);
    void ReportProtocolError(const char* message, SmtpClient& client);

private:

    char* mFrom;

    int mMaxTo;
    int mNumTo;
    char** mTo;
	
	char* mBody;

    int Send_MAIL(SmtpClient& client);
    int Send_RCPT(SmtpClient& client);
    int Send_DATA(SmtpClient& client);
};

#endif
