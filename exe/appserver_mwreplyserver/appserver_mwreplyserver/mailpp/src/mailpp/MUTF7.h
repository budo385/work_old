//=============================================================================
// MUTF7.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef MUTF7_H
#define MUTF7_H

/// Class for converting to or from modified UTF-7 encoding

class MAILPP_API MUTF7 {
public:
    /// Gets the length of the UTF-7 string that would result from
    /// encoding the UTF-16 string
    static size_t EncodeLen(const uint16_t* utf16, size_t utf16Len);
    /// Converts a UTF-16 string to UTF-7
    static size_t Encode(const uint16_t* utf16, size_t utf16Len,
        char* utf7, size_t utf7MaxLen);
    /// Gets the length of the UTF-16 string that would result from
    /// decoding the UTF-7 string
    static size_t DecodeLen(const char* utf7, size_t utf7Len);
    /// Converts a UTF-7 string to UTF-16
    static size_t Decode(const char* utf7, size_t utf7Len,
        uint16_t* utf16, size_t utf16MaxLen);
};

#endif
