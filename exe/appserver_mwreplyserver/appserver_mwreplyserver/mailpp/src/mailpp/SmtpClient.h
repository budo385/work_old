//=============================================================================
// SmtpClient.h
//
// Copyright (c) 2000-2005 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef SMTP_CLIENT_H
#define SMTP_CLIENT_H


/// Class that represents an SMTP client connection

class MAILPP_API SmtpClient
{
    friend class SmtpClientErrorHandler;

public:

    enum {
        FIRST=1,
        LAST=2,
        CONVERT_EOL=4
    };

    /// Default constructor
    SmtpClient();

    /// Destructor
    virtual ~SmtpClient();

    /// Sets a Trace Output instance
    void SetTraceOutput(TraceOutput* out, bool takeOwnership);

    /// Sets the timeout value for network send and receive operations
    void SetTimeout(int secs);

    /// Sets the timeout value for network receive operations
    void SetReceiveTimeout(int secs);

    /// Sets the timeout value for network send operations
    void SetSendTimeout(int secs);

    /// Cancels a currently executing command
    void Cancel();

    /// Opens a connection to an SMTP server
    int Connect(const char* address, int port=25, bool wait=true);

    /// Establishes a TLS connection over an existing TCP connection
    int ConnectTls(const char* serverHostName, bool wait=false);

    /// Establishes a TLS connection over an existing TCP connection
    int ConnectTlsOpenSSL(const char* serverHostName, void* ssl,
       bool wait=false);

    /// Closes the connection to the SMTP server
    void Disconnect();

    /// Sends the HELO command
    int Helo(const char* hostname);

    /// Sends the EHLO command
    int Ehlo(const char* hostname);

    /// Sends the AUTH command
    int Auth(const char* mechanism, const char* params=0);

    /// Sends the AUTH LOGIN command
    int AuthLogin(const char* name, const char* password);

    /// Sends the AUTH PLAIN command
    int AuthPlain(const char* name, const char* password);

    /// Sends the AUTH CRAM-MD5 command
    int AuthCramMd5(const char* name, const char* password);

    /// Sends the AUTH NTLM command
    int AuthNtlm(const char* name, const char* password);

    /// Sends the STARTTLS command
    int Starttls();

    /// Sends the RSET command
    int Rset();

    /// Sends the NOOP command
    int Noop();

    /// Sends the QUIT command
    int Quit();

    /// Sends the VRFY command
    int Vrfy(const char* recipient);

    /// Sends the MAIL command
    int MailFrom(const char* sender, const char* params=0);

    /// Sends the RCPT command
    int RcptTo(const char* recipient, const char* params=0);

    /// Sends the DATA command
    int Data();

    /// Sends mail content as uninterpreted bytes
    int SendBytes(const char* bytes, int length, int options);

    /// Sends mail content as text lines
    int SendLines(const char* bytes, int length, int options);

    /// Gets the number of server capability items
    int NumCapabilities() const;

    /// Gets the server capability item at specified index
    const char* CapabilityAt(int index) const;

    /// Gets true value if server has specified capability
    bool HasCapability(const char* name) const;

    /// Gets the server's reply code
    int ReplyCode() const;

    /// Gets the text of the server's reply
    const char* ReplyText() const;

    /// Gets the server's enhanced status code
    const char* EnhancedStatusCode() const;

    /// Gets the platform-independent error code of last error
    Error ErrorCode() const;

    /// Gets the platform-specific error code of last error
    int OsErrorCode() const;

    /// Gets the error message associated with last error
    const char* ErrorMessage() const;

    /// Sends a command to the server
    int SendCommand(const char* commandStr);

    /// Receives a response from the server
    int ReceiveResponse();

private:

    class ErrorHandler* mErrorHandler;
    class CriticalSectionImpl* mCriticalSectionImpl;
    class Socket* mSocket;

    bool mDeleteTraceOut;
    TraceOutput* mTraceOut;
    bool mIsConnected;
    int mReceiveTimeout;
    int mSendTimeout;
    int mBufferSize;
    char* mBuffer;
    Error mErrorCode;
    int mOsErrorCode;
    char* mErrorMessage;

    int mReplyCode;
    int mReplyTextSize;
    char* mReplyText;

    int mEnhancedStatusCodeSize;
    char* mEnhancedStatusCode;

    int mLastByte;

    int mNumCapabilities;
    int mMaxCapabilities;
    char** mCapabilities;

    int pReceiveEhloResponse();
    void pParseEnhancedStatusCode();
    void pDeleteAllCapabilities();
    void pAddCapability(char* capability);
    int pSendLinesBuffered(const char* bytes, int length, int options);
    int pSendLinesUnbuffered(const char* bytes, int length, int options);
    void pOnConnectionDropped();
    void pSetError(Error errorCode, int osErrorCode, const char* message);
    void pPrintCommand(const char* str);
    void pPrintResponse(const char* str);
    void pPrint(const char* str);
};

#endif
