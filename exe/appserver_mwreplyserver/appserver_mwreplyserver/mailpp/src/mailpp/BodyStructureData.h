//=============================================================================
// BodyStructureData.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef BODY_STRUCTURE_DATA_H
#define BODY_STRUCTURE_DATA_H

class ListItem;
class EnvelopeData;


/// Class that contains the data of an IMAP4 BODYSTRUCTURE structure

class MAILPP_API BodyStructureData
{
public:

    /// Default constructor.
    BodyStructureData();

    /// Copy constructor.
    BodyStructureData(const BodyStructureData& other);

    /// Destructor.
    virtual ~BodyStructureData();

    /// Assignment operator.
    BodyStructureData& operator = (const BodyStructureData& other);

    /// Gets the media type.
    const char* MediaType() const;

    /// Gets the media subtype.
    const char* MediaSubtype() const;

    /// Gets the number of parameters in the Content-Type header.
    unsigned NumContentTypeParams() const;

    /// Gets the name of the parameter at the specified position in the
    /// Content-Type header field.
    const char* ContentTypeParamName(unsigned index) const;

    /// Gets the value of the parameter at the specified position in the
    /// Content-Type header field.
    const char* ContentTypeParamValue(unsigned index) const;

    /// Gets the ENVELOPE structure.
    const EnvelopeData* Envelope() const;

    /// Gets the number of nested BODYSTRUCTURE items.
    int NumBodyParts() const;

    /// Gets the BODYSTRUCTURE item at the specified position.
    const BodyStructureData* BodyPart(unsigned index) const;

    /// Gets the value of the Content-ID header field.
    const char* ContentId() const;

    /// Gets the value of the Content-Description header field.
    const char* ContentDescription() const;

    /// Gets the value of the Content-Transfer-Encoding header field.
    const char* ContentTransferEncoding() const;

    /// Gets the value of the Content-Disposition header field.
    const char* ContentDisposition() const;

    /// Gets the number of parameters in the Content-Disposition header
    /// field.
    unsigned NumContentDispositionParams() const;

    /// Gets the name of the parameter at the specified position in the
    /// Content-Disposition header field.
    const char* ContentDispositionParamName(unsigned index) const;

    /// Gets the value of the parameter at the specified position in the
    /// Content-Disposition header field.
    const char* ContentDispositionParamValue(unsigned index) const;

    /// Gets the value of the Content-MD5 header field.
    const char* ContentMd5() const;

    /// Gets the body size in bytes.
    unsigned Size() const;

    /// Gets the number of lines in message or text body part.
    unsigned NumLines() const;

    /// Called by the parser to set the data for this object.
    void ImportData(const ListItem* root);

private:

    char* mMediaType;
    char* mMediaSubtype;

    NameValueSequence mContentTypeParams;
    NameValueSequence mContentDispositionParams;

    EnvelopeData* mEnvelope;

    BodyStructureDataSequence mBodyParts;

    char* mContentId;
    char* mContentDescription;
    char* mContentTransferEncoding;
    char* mContentMd5;
    char* mContentDisposition;
    unsigned mSize;
    unsigned mNumLines;

    void ImportDataForMultipartType(ListItem* item);
    void ImportDataForMessageType(ListItem* item);
    void ImportDataForTextType(ListItem* item);
    void ImportDataForBasicType(ListItem* item);
    ListItem* GetBodyFields(ListItem* item);
    ListItem* GetBodyExtFields(ListItem* item);
    void GetContentDisposition(ListItem* item);

    void Clear();

    void AppendContentTypeParams(ListItem* item);
    void AppendContentDispositionParams(ListItem* item);
    void AppendBodyStructureData(BodyStructureData* data);
    void AppendContentTypeParam(const char* name, const char* value);
    void AppendContentDispositionParam(const char* name, const char* value);
};

#endif
