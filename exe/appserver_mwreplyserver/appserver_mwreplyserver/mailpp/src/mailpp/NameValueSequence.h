//=============================================================================
// NameValueSequence.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef NAME_VALUE_SEQUENCE_H
#define NAME_VALUE_SEQUENCE_H

class NameValue;


class MAILPP_API NameValueSequence
{
public:

    NameValueSequence();
    NameValueSequence(int initialCapacity);
    NameValueSequence(const NameValueSequence& other);
    ~NameValueSequence();

    NameValueSequence& operator = (const NameValueSequence& other);

    unsigned NumElements() const;
    void InsertElement(unsigned pos, NameValue* element);
    void AppendElement(NameValue* element);
    NameValue* RemoveElement(unsigned index);
    NameValue* ElementAt(unsigned index) const;

    void DeleteAll();

private:

    unsigned mNumElements;
    unsigned mMaxElements;
    NameValue** mElements;

    void Copy(const NameValueSequence&);
};

#endif
