//=============================================================================
// EnvelopeData.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef ENVELOPE_DATA_H
#define ENVELOPE_DATA_H


/// Class that contains the data of an IMAP4 ENVELOPE structure

class MAILPP_API EnvelopeData
{
    friend class Parser;

public:

    /// Default constructor.
    EnvelopeData();

    /// Copy constructor.
    EnvelopeData(const EnvelopeData& other);

    /// Destructor.
    virtual ~EnvelopeData();

    /// Assignment operator.
    EnvelopeData& operator = (const EnvelopeData& other);

    /// Gets the value of the Date header field.
    const char* Date() const;

    /// Gets the value of the Subject header field.
    const char* Subject() const;

    /// Gets the value of the In-Reply-To header field.
    const char* InReplyTo() const;

    /// Gets the value of the Message-ID header field.
    const char* MessageId() const;

    /// Gets the number of From addresses.
    unsigned NumFrom() const;

    /// Gets the From address at the specified position.
    const AddressData& From(unsigned index) const;

    /// Gets the number of Sender addresses.
    unsigned NumSender() const;

    /// Gets the Sender address at the specified position.
    const AddressData& Sender(unsigned index) const;

    /// Gets the number of Reply-To addresses.
    unsigned NumReplyTo() const;

    /// Gets the Reply-To address at the specified position.
    const AddressData& ReplyTo(unsigned index) const;

    /// Gets the number of To addresses.
    unsigned NumTo() const;

    /// Gets the To address at the specified position.
    const AddressData& To(unsigned index) const;

    /// Gets the number of Cc addresses.
    unsigned NumCc() const;

    /// Gets the Cc address at the specified position.
    const AddressData& Cc(unsigned index) const;

    /// Gets the number of Bcc addresses.
    unsigned NumBcc() const;

    /// Gets the Bcc address at the specified position.
    const AddressData& Bcc(unsigned index) const;

    /// Called by the parser to set the data for this object.
    void ImportData(const ListItem* root);

private:

    char* mDate;
    char* mSubject;
    char* mInReplyTo;
    char* mMessageId;

    AddressDataSequence mFrom;
    AddressDataSequence mSender;
    AddressDataSequence mReplyTo;
    AddressDataSequence mTo;
    AddressDataSequence mCc;
    AddressDataSequence mBcc;

    void Clear();
};

#endif
