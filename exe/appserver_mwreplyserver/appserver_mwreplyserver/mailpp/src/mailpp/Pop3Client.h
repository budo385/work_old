//=============================================================================
// Pop3Client.h
//
// Copyright (c) 2000-2005 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef POP3_CLIENT_H
#define POP3_CLIENT_H

class ProtocolListener;

/// Class that represents a POP3 client connection

class MAILPP_API Pop3Client
{
    friend class Pop3ClientErrorHandler;

public:

    enum {
        OK = 43,
        ERR = 45
    };

    /// Default constructor
    Pop3Client();

    /// Destructor
    virtual ~Pop3Client();

    /// Sets a Trace Output instance
    void SetTraceOutput(TraceOutput* out, bool takeOwnership);

    /// Sets the timeout value for network send and receive operations
    void SetTimeout(int secs);

    /// Sets the timeout value for network receive operations
    void SetReceiveTimeout(int secs);

    /// Sets the timeout value for network send operations
    void SetSendTimeout(int secs);

    /// Sets the listener object to process multiple line responses
    void SetListener(ProtocolListener* listener, bool del);

    /// Cancels a currently executing command
    void Cancel();

    /// Opens a connection to a POP3 server
    int Connect(const char* address, int port=110, bool wait=true);

    /// Opens a TLS connection over an existing TCP connection
    int ConnectTls(const char* serverHostName, bool wait=false);

    /// Establishes a TLS connection over an existing TCP connection
    int ConnectTlsOpenSSL(const char* serverHostName, void* ssl,
        bool wait=false);

    /// Disconnects from the server
    void Disconnect();

    /// Sends the CAPA command
    int Capa();

    /// Sends the STLS command
    int Stls();

    /// Sends the USER command
    int User(const char* user);

    /// Sends the PASS command
    int Pass(const char* password);

    /// Sends the APOP command
    int Apop(const char* user, const char* password);

    /// Sends the AUTH command
    int Auth(const char* mechanism);

    /// Send the AUTH CRAM-MD5 command
    int AuthCramMd5(const char* user, const char* password);

    /// Send the AUTH NTLM command
    int AuthNtlm(const char* user, const char* password);

    /// Sends the STAT command
    int Stat();

    /// Sends the LIST command
    int List();

    /// Sends the LIST command with a message number argument
    int List(int msgNum);

    /// Sends the RETR command
    int Retr(int msgNum);

    /// Sends the DELE command
    int Dele(int msgNum);

    /// Sends the TOP command
    int Top(int msgNum, int numLines);

    /// Sends the UIDL command
    int Uidl();

    /// Sends the UIDL command with a message number argument
    int Uidl(int msgNum);

    /// Sends the NOOP command
    int Noop();

    /// Sends the RSET command
    int Rset();

    /// Sends the QUIT command
    int Quit();

    /// Gets the server's reply code
    int ReplyCode() const;

    /// Gets the text of the server's reply
    const char* ReplyText() const;

    /// Gets the platform-independent error code for the last error
    Error ErrorCode() const;

    /// Gets the platform-specific error code for the last error
    int OsErrorCode() const;

    /// Gets the error message for the last error
    const char* ErrorMessage() const;

private:

    class ErrorHandler* mErrorHandler;
    class CriticalSectionImpl* mCriticalSectionImpl;
    class Socket* mSocket;

    bool mDeleteTraceOut;
    TraceOutput* mTraceOut;
    bool mIsConnected;
    int mReceiveTimeout;
    int mSendTimeout;
    int mBufferSize;
    char* mBuffer;
    Error mErrorCode;
    int mOsErrorCode;
    char* mErrorMessage;

    int mReplyCode;
    int mReplyTextSize;
    char* mReplyText;

    char* mBannerTimestamp;

    class ProtocolListener* mListener;
    int mDeleteListener;

    int SendCommandCrLf(const char* commandStr);
    int SendCommand(const char* aCommandStr);
    int ReceiveSingleLineResponse();
    int ReceiveMultipleLineResponse();
    int ReceiveLines();
    int pParseBanner();
    void pSetError(Error errorCode, int osErrorCode, const char* errorMessage);
    void pPrintCommand(const char* str);
    void pPrintResponse(const char* str);
    void pPrint(const char* str);
};

#endif
