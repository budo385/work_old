#include "SmtpSession.h"
#include "MailTransaction.h"
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace mailpp;


#if defined(WIN32) || defined(_WIN32)
#  define strdup   _strdup
#  define snprintf _snprintf
#endif


SmtpSession::SmtpSession(const char* hostname)
{
    mHostname = strdup(hostname);
	//BT: does not work on WINCE...
    //mTraceOutput = new StdTraceOutput; 
	mTraceOutput = 0;
}

void SmtpSession::SetTracer(mailpp::TraceOutput* pTracer)
{
	//delete mTraceOutput;
    mTraceOutput = 0;
    mTraceOutput = pTracer;
}

SmtpSession::~SmtpSession()
{
    if (mHostname) free(mHostname);
    mHostname = 0;
    delete mTraceOutput;
    mTraceOutput = 0;
}

int SmtpSession::Open(const char* address, int port)
{
    mClient.SetTraceOutput(mTraceOutput, false);
    int connectErr = mClient.Connect(address, port);
    if (! connectErr) {
        int replyCode = mClient.ReplyCode();
        if (replyCode/100%10 == 2) {
            int err = SayHello();
            if (! err) {
                //
                // Return here on success
                //
                return 0;
            }
        }
        else /* if (replyCode/100%10 != 2) */ {
            ReportProtocolError("SMTP server is not accepting commands");
        }
        mClient.Disconnect();
    }
    else /* if (connectErr) */ {
        char s[200];
        snprintf(s, sizeof(s), "Can't connect to server at %s", address);
        ReportNetworkError(s);
    }
    //
    // Return here on failure
    //
    return -1;
}

void SmtpSession::Close()
{
    mClient.Quit();
    mClient.Disconnect();
}

int SmtpSession::DoTransaction(MailTransaction& transaction)
{
    return transaction.Send(mClient);
}

int SmtpSession::SayHello()
{
    int retVal = -1;
    //
    // Send the EHLO command
    //
    int commErr = mClient.Ehlo(mHostname);
    if (! commErr) {
        int replyCode = mClient.ReplyCode();
        if (replyCode/100%10 == 5) {
            //
            // If EHLO was not accepted, send the HELO command
            //
            commErr = mClient.Helo(mHostname);
            if (! commErr) {
                replyCode = mClient.ReplyCode();
            }
            else /* if (commErr) */ {
                ReportNetworkError("Network error");
            }
        }
        //
        // Check for a 2xx reply code, which indicates success
        //
        if (replyCode/100%10 == 2) {
            retVal = 0;
        }
        else /* if (replyCode/100%10 != 2) */ {
            ReportProtocolError("SMTP EHLO (and HELO) failed");
        }
    }
    else /* if (commErr) */ {
        ReportNetworkError("Network error");
    }
    return retVal;
}

void SmtpSession::ReportNetworkError(const char* message)
{
    cout << message << endl;
    cout << "Error code: " << mClient.ErrorCode() << endl;
    cout << "Error message: " << mClient.ErrorMessage() << endl;
    cout << "OS error code: " << mClient.OsErrorCode() << endl;
}

void SmtpSession::ReportProtocolError(const char* message)
{
    cout << message << endl;
    cout << "Server's reply was: " << mClient.ReplyCode() << " "
        << mClient.ReplyText() << endl;
}

