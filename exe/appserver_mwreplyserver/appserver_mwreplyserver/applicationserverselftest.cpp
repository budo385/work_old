#include "applicationserverselftest.h"
#include "common/common/authenticator.h"
#include "dbtabledef.h"

#include "common/common/logger.h"
extern Logger g_Logger;
#include "db/db/dbsqlmanager.h"
extern DbSqlManager	*g_DbManager;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;
#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;


ApplicationServerSelfTest::ApplicationServerSelfTest()
{

}

ApplicationServerSelfTest::~ApplicationServerSelfTest()
{

}


//Note: tester will add 4 characters as digits on end of the username & password
void ApplicationServerSelfTest::AddUserAccountsForTest()
{
	Status pStatus;

	pStatus.setError(0);
	if(g_AppServer->m_INIFile.m_strStartUserNameAdd.isEmpty() || g_AppServer->m_INIFile.m_strStartPassWordAdd.isEmpty() || g_AppServer->m_INIFile.m_nCountUserAdd==0)
		return;

	int nStart=g_AppServer->m_INIFile.m_nStartCounterAdd;
	int nEnd=nStart+g_AppServer->m_INIFile.m_nCountUserAdd;
	QString strEmailDomain;
	QString strUserStart=g_AppServer->m_INIFile.m_strStartUserNameAdd;
	if (strUserStart.contains("@"))
	{
		strEmailDomain=strUserStart.mid(strUserStart.indexOf("@"));
		strUserStart=strUserStart.left(strUserStart.indexOf("@"));
	}


	QString strPWStart=g_AppServer->m_INIFile.m_strStartPassWordAdd;

	if (nStart>99999 || nEnd>99999)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ApplicationServerSelfTest: start or end offset are greater then max value of 99999");
		return;
	}

	bool bSkipLB=false;
	for (int i=nStart;i<nEnd;i++)
	{
		QString strUser=strUserStart+ConvertNumValue(i)+strEmailDomain;
		QString strPass=strPWStart+ConvertNumValue(i);

		CreateSingleUser(pStatus,strUser,strPass,bSkipLB,i);
		if (!pStatus.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ApplicationServerSelfTest: aborting user creation, failed to create user: %1, pass: %2, error:%3").arg(strUser).arg(strPass).arg(pStatus.getErrorText()));
			return;
		}

		g_AppServer->m_INIFile.m_strLastUserNameAdded=strUser;
	}

	g_AppServer->m_INIFile.m_nCountUserAdd=0; //adding is finished, stop furter:
}

void ApplicationServerSelfTest::CreateSingleUser( Status &pStatus, QString strEmail, QString strPass, bool &bSkipLB, int nIdx )
{
	strEmail=strEmail.toLower();
	QString Ret_strESHost="";

	//if registered, send mail, then wait for unregister click..
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;
	
	QString strConfirmString = QString::number(nIdx)+Authenticator::GenerateRandomPassword(100);
	
	//-----------------------------------------------------
	//STEP1: Create user record locally in the cloud server
	//-----------------------------------------------------
	DbRecordSet rowData;
	rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_USER_DATA));
	rowData.addRow();
	rowData.setData(0,"USR_EMAIL",strEmail);
	rowData.setData(0,"USR_PASSWORD",strPass);
	rowData.setData(0,"USR_IS_CONFIRMED",1);
	rowData.setData(0,"USR_CONFIRM_STRING",strConfirmString);
	rowData.setData(0,"USR_EMAIL_IN_STORAGE",0);
	rowData.setData(0,"USR_EMAIL_TOTAL_DOWNLOADED",0);
	rowData.setData(0,"USR_CONFIRM_SENT",QDateTime::currentDateTime());
	
	//on new insert associate ES server: get one with lowest users:
	int nESRID = g_ServiceHandler->FindESHost(Ret_strESHost);
	if (nESRID>0)
		rowData.setData(0,"USR_EMAIL_SERVER_ID",nESRID);
		
	query.WriteData(pStatus,"USER_DATA",rowData);
	if (pStatus.getErrorCode()==StatusCodeSet::ERR_SQL_DUPLICATE_ENTRY || pStatus.getErrorCode()==StatusCodeSet::ERR_SQL_UNIQUE_CONSTRAINT_VIOLATION)
	{
		pStatus.setError(0);
		return;
	}
	if(!pStatus.IsOK()) 
		return;

	//-----------------------------------------------------
	//STEP1: register email to the LB server
	//-----------------------------------------------------
	if (!bSkipLB)
	{
		g_ServiceHandler->RegisterEmailToLBServer(pStatus,strEmail);// B.T. keep LB database in sync!!!
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_HTTP_FAILED_CONNECT)
		{
			pStatus.setError(0);
			bSkipLB=true;
		}
	}
	//LB will sync later anyway
	//if(!pStatus.IsOK()) 
	//	return;

	//-----------------------------------------------------------------------------------
	//STEP1: register email to the ES server (user: email, pass=same pass as in CS server
	//-----------------------------------------------------------------------------------
	if (nESRID>0)
	{
		g_ServiceHandler->RegisterUserToESHost(pStatus,nESRID,strEmail,strPass);
		if(!pStatus.IsOK()) 
			return;
	}
}

QString ApplicationServerSelfTest::ConvertNumValue( int nValue )
{
	QString strVal=QString::number(nValue);
	if (strVal.size()==1)
		return "000"+strVal;
	if (strVal.size()==2)
		return "00"+strVal;
	if (strVal.size()==3)
		return "0"+strVal;
	return strVal;
}