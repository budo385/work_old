#include "emailimportthread.h"
#include <QDateTime>

#include "common/common/threadid.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/datahelper.h"
#include "common/common/authenticator.h"
#include "common/common/sha256hash.h"
#include "bus_trans_client/bus_trans_client/userstoragehttpclient.h"
#include "mailpp/src/mailpp/mailpp.h"
#include "mimepp/src/mimepp/mimepp.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "bus_core/bus_core/accuarcore.h"
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include "dbtabledef.h"
#include "pop3ini.h"

#if defined(WIN32)
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
#  define strcasecmp _stricmp
#endif

extern "C"
{
 #include "applink.c"
}
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;

#define MAX_ATTACHMENT_LIST_SIZE 16777216

void FolderList2String(const QList<QStringList> &lstData, QString &strResult);
void String2FolderList(QString strData, QList<QStringList> &lstResult);
bool ParseMessageParts(mimepp::Entity &parent, DbRecordSet &lstRowResult, DbRecordSet &lstEmbeddedImgs);
void FilterEmailsByContact(Status status, DbRecordSet &lstData, int nPersonID, bool bSelectOnly = false);
void FilterAttachmentRecords(DbRecordSet &lstAttachments);
void ProcessAddressList(mimepp::AddressList &list, DbRecordSet &lstRowResult, int nCurRow, int nType, bool bAreTemplates, QSet<QString> &setTmp);
bool Pop3Connect(mailpp::Pop3Client &client, const QString &strHost, int nPort, bool bUseSTLS, bool bUseSSL);
void EmbeddCIDAttachments(mimepp::Entity &parent, DbRecordSet &lstRowResult, DbRecordSet &lstEmbeddedImgs);
void ResolveEmailSender(const QString &strBody, QString &strResSenderEmail, QString &strResSenderName);
int openssl_initialize();
void openssl_finalize();

class myTraceOutput : public mailpp::TraceOutput
{
public:
	virtual void Send (const char *buffer, unsigned length)
	{
#ifdef _DEBUG
		qDebug() << "POP3 >" << buffer;
#endif
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "SMTP > " + QString(buffer));
	};

	virtual void Receive (const char *buffer, unsigned length)
	{
#ifdef _DEBUG
		qDebug() << "POP3 <" << buffer;
#endif
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "SMTP < " + QString(buffer));
	};
};

class myProtocolListener : public mailpp::ProtocolListener
{
public:
	virtual void ClearLines() {
		m_lstLines.clear();
	};
	virtual void LineReceived(const char *line){
		m_lstLines.push_back(line);	
	};

public:
	std::vector<std::string> m_lstLines;
};

//-----------thread object-----------------
EmailImportThread::EmailImportThread()
{
	openssl_initialize();
}

EmailImportThread::~EmailImportThread()
{
	openssl_finalize();
}

//from app server settings directory load all files with name like emailacc_*.ini
//store it in m_lstEmailAccounts
//return number of email accounts, if zero, abort all
int EmailImportThread::LoadEmailAccounts()
{
	m_lstEmailAccounts.defineFromView(DbTableDef::getView(DbTableDef::VIEW_EMAIL_ACC));

	QDir dir(QCoreApplication::applicationDirPath()+"/settings");
	if (!dir.exists())
	{
		return 0;
	}

	//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
	QFileInfoList lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isFile())
		{
			QString fileName=lstFiles.at(i).fileName();
			if(lstFiles.at(i).suffix()!="ini" || fileName.toLower().left(QString("emailacc_").length())!="emailacc_")
				continue;

			Pop3Ini EmailData;
			if(EmailData.Load(lstFiles.at(i).absoluteFilePath(),false))
			{
				m_lstEmailAccounts.addRow();
				int nRow=m_lstEmailAccounts.getRowCount()-1;
				m_lstEmailAccounts.setData(nRow,"ACC_NAME",EmailData.m_strConnectionName);
				m_lstEmailAccounts.setData(nRow,"SERVER_NAME",EmailData.m_strServerName);
				m_lstEmailAccounts.setData(nRow,"IS_POP",EmailData.m_nIsPop);
				m_lstEmailAccounts.setData(nRow,"IS_ACTIVE",EmailData.m_nActive);
				m_lstEmailAccounts.setData(nRow,"EMAIL_ADDRESS",EmailData.m_strEmailAddress);
				m_lstEmailAccounts.setData(nRow,"PORT",EmailData.m_nPort);
				m_lstEmailAccounts.setData(nRow,"SSL_TYPE",EmailData.m_strSSL);
				m_lstEmailAccounts.setData(nRow,"USERNAME",EmailData.m_strUserName);
				m_lstEmailAccounts.setData(nRow,"PASSWORD",EmailData.m_strPassword);
			}
		}
	}

	//just create one dummy account:
	if (m_lstEmailAccounts.getRowCount()==0)
	{
		Pop3Ini EmailData;
		EmailData.Load(QCoreApplication::applicationDirPath()+"/settings/emailacc_dummy.ini");
	}

	return m_lstEmailAccounts.getRowCount();
}


//run in separate thread:
void EmailImportThread::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Email synchronization started, thread: "+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
	DoActionPop3();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Email synchronization done, thread: "+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
}

int verify_callback2(X509_STORE_CTX *,void *)
{
	return 1;	//do not terminate
}

void EmailImportThread::DoActionPop3()
{
#ifdef _WIN32
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start DoActionPop3");

	qDebug()<<"POP3 Sync thread:"<<ThreadIdentificator::GetCurrentThreadID();

	//fetch all data for all "email in" user settings
	
	int nAccounts = m_lstEmailAccounts.getRowCount();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Start email process for %1 accounts").arg(nAccounts));

	for(int i=0; i<nAccounts; i++)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Check account: %1").arg(m_lstEmailAccounts.getDataRef(i, "ACC_NAME").toString()));

		bool bActive	= m_lstEmailAccounts.getDataRef(i, "IS_ACTIVE").toInt() > 0;
		bool bIsPOP3	= m_lstEmailAccounts.getDataRef(i, "IS_POP").toInt() > 0;
		QString strSSL	= m_lstEmailAccounts.getDataRef(i, "SSL_TYPE").toString();	//(NONE, SSL, STARTTLS)
		QString strHost = m_lstEmailAccounts.getDataRef(i, "SERVER_NAME").toString();
		int nPort		= m_lstEmailAccounts.getDataRef(i, "PORT").toInt();
		QString strUser = m_lstEmailAccounts.getDataRef(i, "USERNAME").toString();
		QString strPass = m_lstEmailAccounts.getDataRef(i, "PASSWORD").toString();

		if(!bActive){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Skip this account, not active."));
			continue;
		}

		bool bUseSSL  = (strSSL == "SSL" || strSSL == "STARTTLS");
		bool bUseSTLS = (strSSL == "STARTTLS");	//requesting SSL over the standard (unencrypted) port
		bool bWait    = !bUseSSL || bUseSTLS;
		myTraceOutput tracer;

		if(bIsPOP3)
		{
			//
			// POP3 protocol
			//

			//init POP3 client
			myProtocolListener listener;
			mailpp::Pop3Client client;
			client.SetTraceOutput(&tracer, false);
			client.SetListener(&listener, false);

			if(!Pop3Connect(client, strHost, nPort, bUseSTLS, bUseSSL))
				return;

			//login
			if(client.User(strUser.toLocal8Bit().constData()) < 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to send POP3 command (USER).");
			#ifdef _DEBUG
				qDebug() << "Failed to send POP3 command (USER).";
			#endif
				client.Disconnect();
				return;
			}
			if(mailpp::Pop3Client::OK != client.ReplyCode()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
			#ifdef _DEBUG
				qDebug() << "POP3 error:" << client.ErrorMessage();
			#endif
				client.Quit();
				client.Disconnect();
				return;
			}

			if(client.Pass(strPass.toLocal8Bit().constData()) < 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to send POP3 command (PASS).");
			#ifdef _DEBUG
				qDebug() << "Failed to send POP3 command (PASS).";
			#endif
				client.Disconnect();
				return;
			}
			if(mailpp::Pop3Client::OK != client.ReplyCode()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
			#ifdef _DEBUG
				qDebug() << "POP3 error:" << client.ErrorMessage();
			#endif
				client.Quit();
				client.Disconnect();
				return;
			}
			
			//list messages available in the mailbox
			if(client.List() < 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to send POP3 command (LIST).");
			#ifdef _DEBUG
				qDebug() << "Failed to send POP3 command (LIST).";
			#endif
				client.Disconnect();
				return;
			}
			if(mailpp::Pop3Client::OK != client.ReplyCode()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
			#ifdef _DEBUG
				qDebug() << "POP3 error:" << client.ErrorMessage();
			#endif
				client.Quit();
				client.Disconnect();
				return;
			}

			//define IMPORT list:
			DbRecordSet lstDataEmails;	//email storage
			lstDataEmails.defineFromView(DbTableDef::getView(DbTableDef::VIEW_EMAILS)); 

			//for each message (line)
			DbRecordSet lstGlobalStatus; //BT:list<<DEL> = 0 no delete, 1 - delete from POP3 (indexes are in sync with lstDataEmails)
			std::vector<int> lstMsgIndexes;
			std::vector<std::string> lstMsgs = listener.m_lstLines;
			int nMessages = lstMsgs.size();
			for(int i=0; i<nMessages; i++)
			{
				//parse message index
				int nMsgNum = -1;
				int nSize  = -1;
				sscanf(lstMsgs[i].c_str(), "%d %d", &nMsgNum, &nSize);
				lstMsgIndexes.push_back(nMsgNum);

				if(client.Retr(nMsgNum) < 0){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to send POP3 command (RETR).");
				#ifdef _DEBUG
					qDebug() << "Failed to send POP3 command (RETR).";
				#endif
					client.Disconnect();
					return;
				}
				if(mailpp::Pop3Client::OK != client.ReplyCode()){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
				#ifdef _DEBUG
					qDebug() << "POP3 error:" << client.ErrorMessage();
				#endif
					client.Quit();
					client.Disconnect();
					return;
				}

				//fetch the message contents
				std::vector<std::string> lstLines = listener.m_lstLines;
				std::string strEmail;
				int nLines = lstLines.size();
				for(int j=0; j<nLines; j++){
					strEmail += lstLines[j];
				}
				int k=0;

				mimepp::Message msg(strEmail.c_str());
				msg.parse();

				//store the message into the database
				lstDataEmails.addRow();
				int nCurRow = lstDataEmails.getRowCount()-1;
				
				//store entire enooded email here
				QString strMime = strEmail.c_str();
				lstDataEmails.setData(nCurRow, "BEM_BODY_MIME", strMime);

				QString strFrom = msg.headers().from().getString().c_str();
				lstDataEmails.setData(nCurRow, "BEM_FROM", strFrom);

				//fix date to local date
				QDateTime dtRev;
				dtRev.setTime_t(msg.headers().received().date().asUnixTime());
				//dtRev.setTimeSpec(Qt::UTC);	//TOFIX?
				//QDateTime tmpDat=dtRev.toLocalTime();
				lstDataEmails.setData(nCurRow, "BEM_RECV_TIME", dtRev);

				// Check content-transfer-encoding, and decode if necessary
				QString strSubject;
				int cte = mimepp::TransferEncodingType::_7BIT;
				if (msg.headers().hasField("Content-Transfer-Encoding")) {
					cte = msg.headers().contentTransferEncoding().asEnum();
				}
				if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
					mimepp::QuotedPrintableDecoder dec;
					strSubject = dec.decode(msg.headers().subject().text().c_str()).c_str();
				}
				else if (cte == mimepp::TransferEncodingType::BASE64) {
					mimepp::Base64Decoder dec;
					strSubject = dec.decode(msg.headers().subject().text().c_str()).c_str();
				}
				else
					strSubject = msg.headers().subject().text().c_str();
				lstDataEmails.setData(nCurRow, "BEM_SUBJECT", strSubject);

				//add to/cc/bcc addresses
				QSet<QString> setTmp;
				ProcessAddressList(msg.headers().to(),  lstDataEmails,	nCurRow, 1,	false, setTmp);
				ProcessAddressList(msg.headers().cc(),  lstDataEmails,	nCurRow, 2,	false, setTmp);
				ProcessAddressList(msg.headers().bcc(), lstDataEmails,	nCurRow, 3,	false, setTmp);

				bool bBodyPartSet = false;
				bool bBodyPartHTML = false;

				//strip attachments
				DbRecordSet lstEmbeddedImgs;
				lstEmbeddedImgs.addColumn(QVariant::String,    "CID");
				lstEmbeddedImgs.addColumn(QVariant::String,	   "Name");
				lstEmbeddedImgs.addColumn(QVariant::ByteArray, "ImageData");
				if(ParseMessageParts(msg, lstDataEmails, lstEmbeddedImgs)){
					bBodyPartSet = true;
				}
				//only if no body part found (not multipart)
				if(!bBodyPartSet){
					QString strRTF = msg.body().getString().c_str();
					if(!strRTF.isEmpty()){
						lstDataEmails.setData(nCurRow, "BEM_BODY", strRTF);
					}
				}

				//can still be text-only, check mail type
				QString strRTF = lstDataEmails.getDataRef(nCurRow, "BEM_BODY").toString();
				bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0);
				if(bHtml)
					EmbeddCIDAttachments(msg, lstDataEmails, lstEmbeddedImgs);

				//heuristics: resolve sender of the original forwarded message
				QString strResSenderEmail, strResSenderName;
				ResolveEmailSender(strRTF, strResSenderEmail, strResSenderName);
				lstDataEmails.setData(nCurRow, "BEM_ORIGINATOR_EMAIL", strResSenderEmail);
				lstDataEmails.setData(nCurRow, "BEM_ORIGINATOR_NAME", strResSenderName);

			}	//for each message (line)

			//import all messages for a single account
			if(lstDataEmails.getRowCount() > 0){
			#ifdef _DEBUG
				//lstDataEmails.Dump();
			#endif

				Status status;
				DbRecordSet lstStatus; //BT:list<<DEL> = 0 no delete, 1 - delete from POP3 (indexes are in sync with lstDataEmails)
				g_ServiceHandler->WriteEmails(status, lstDataEmails,lstStatus);
				if(!status.IsOK()) return;

				if(lstGlobalStatus.getRowCount()<1)
					lstGlobalStatus.copyDefinition(lstStatus);
				lstGlobalStatus.merge(lstStatus);
			}

			//delete all processed messages from POP3 server in reverse order (from higher indexes to lower ones)
			//NOTE: delete works only in release, making debug tests more safe
		#ifndef _DEBUG
			for(int j=lstMsgIndexes.size()-1; j>=0; j--){
				//decide if we should delete row on one-by-one basis
				//list<<DEL> = 0 no delete, 1 - delete from POP3 (indexes are in sync with lstDataEmails)
				if(lstGlobalStatus.getDataRef(j, "DEL").toInt() < 1)
					continue;
				int nMsgNum = lstMsgIndexes[j];
				if(client.Dele(nMsgNum) < 0){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to send POP3 command (DELE).");
				#ifdef _DEBUG
					qDebug() << "Failed to send POP3 command (DELE).";
				#endif
					client.Disconnect();
					return;
				}
				if(mailpp::Pop3Client::OK != client.ReplyCode()){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
				#ifdef _DEBUG
					qDebug() << "POP3 error:" << client.ErrorMessage();
				#endif
					client.Quit();
					client.Disconnect();
					return;
				}
			}
		#endif

			//quit connection
			client.Quit();
			client.Disconnect();
		}
		else
		{
			//
			// IMAP protocol
			//

			mailpp::Imap4Client client;
			client.SetTraceOutput(&tracer, false);

			if(client.Connect(strHost.toLocal8Bit().constData(), nPort, bWait) < 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to connect to IMAP host.");
			#ifdef _DEBUG
				qDebug() << "Failed to connect to IMAP host.";
			#endif
				return;
			}
			//connected, check the reply code as well
			if(bWait){
				if(mailpp::Imap4Client::OK != client.CommandStatus()){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("IMAP error: %1").arg(client.ErrorMessage()));
				#ifdef _DEBUG
					qDebug() << "IMAP error:" << client.ErrorMessage();
				#endif
					client.Disconnect();
					return;
				}
			}

			//initiate SSL if needed
			if(bUseSTLS)
			{
				if(client.Starttls() < 0){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to arrange TLS connection to IMAP host.");
				#ifdef _DEBUG
					qDebug() << "Failed to arrange TLS connection to POP3 host.";
				#endif
					return;
				}
				if(mailpp::Imap4Client::OK != client.CommandStatus()){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("IMAP error: %1").arg(client.ErrorMessage()));
				#ifdef _DEBUG
					qDebug() << "IMAP error:" << client.ErrorMessage();
				#endif
					client.Logout();
					client.Disconnect();
					return;
				}
			}
			if(bUseSSL)
			{
				//
				//ignore error about hostname mismatch within the certificate
				//
		#if 1
				//SSL_CTX	*ctx = SSL_CTX_new(TLSv1_client_method());
				SSL_CTX	*ctx = SSL_CTX_new(SSLv3_client_method());
				SSL *ssl = NULL;
				if(ctx){
					SSL_CTX_set_options(ctx, SSL_OP_ALL);
					SSL_CTX_set_cert_verify_callback(ctx, verify_callback2, NULL);
					ssl = SSL_new(ctx);
				}
				else{
					char szBuf[1000];
					ERR_error_string(ERR_get_error(), szBuf);
					qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";
					client.Disconnect();
					return;
				}
				if( client.ConnectTlsOpenSSL(strHost.toLocal8Bit().constData(), ssl, true) < 0 &&
					mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
		#else
				if( client.ConnectTls(strPOP3.toLocal8Bit().constData(), true) < 0 &&
					mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
		#endif
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Failed to establish TLS connection to IMAP host (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));
				#ifdef _DEBUG
					qDebug() << QString("Failed to establish TLS connection to IMAP host (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage());
				#endif

					char szBuf[1000];
					ERR_error_string(ERR_get_error(), szBuf);
					qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";

					client.Logout();
					client.Disconnect();
					return;
				}

			    // Send LOGIN command
				if( client.Login(strUser.toLocal8Bit().constData(), strPass.toLocal8Bit().constData()) < 0)
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Login error (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));
				#ifdef _DEBUG
					qDebug() << QString("Login error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage());
				#endif
					client.Logout();
					client.Disconnect();
					return;
				}
				if(mailpp::Imap4Client::OK != client.CommandStatus()){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Login error (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));
				#ifdef _DEBUG
					qDebug() << QString("Login error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage());
				#endif
					client.Logout();
					client.Disconnect();
					return;
				}
				
			    // Send SELECT to select the folder
			    client.Select("/");	//TOFIX hardcoded

				//TOFIX fetch messages, ...
				/*
				// Find the EXISTS response, which will tell us how many messages are
				// in the folder.
				int numResponses = client->NumResponses();
				int numMessages = 0;
				int i;
				for (i=0; i < numResponses; ++i) {
					if (strcasecmp(client->ResponseAt(i).Name(), "exists") == 0) {
						const ExistsResponse& resp = (const ExistsResponse&)
							client->ResponseAt(i);
						numMessages = resp.Number();
					}
				}
				printf("------- %d messages in mailbox\n", numMessages);

				// Send a FETCH with the ALL macro (equivalent to (FLAGS INTERNALDATE
				// RFC822.SIZE ENVELOPE))
				client->Fetch(1, numMessages, "ALL");

				// Process each FETCH response -- one for each message in the folder.
				// Extract the information we want to print for our listing.
				numResponses = client->NumResponses();
				for (i=0; i < numResponses; ++i) {
					// Find the FETCH responses
					if (strcasecmp(client->ResponseAt(i).Name(), "fetch") == 0) {
						const FetchResponse& resp = (const FetchResponse&)client->ResponseAt(i);
						// Get the ENVELOPE structure
						const EnvelopeData* env = resp.Envelope();
						// Get the FLAGS list
						const FlagsData* flags = resp.Flags();
						// Then extract the desired information and print a summary line
						if (env != 0 && flags != 0) {
							printSummaryLine(*env, *flags);
						}
					}
				}
				*/

				client.Logout();
				client.Disconnect();
			}
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"End subscription mail process");
#endif //#ifdef _WIN32
}

#ifdef _WIN32
bool ParseMessageParts(mimepp::Entity &parent, DbRecordSet &lstRowResult, DbRecordSet &lstEmbeddedImgs)
{
	int nCurRow = lstRowResult.getRowCount()-1;

	//recursively drill down to find the correct body and attachments (may have multipart inside the multipart, ...)
	bool bBodyPartSet = false;
	bool bBodyPartHTML = false;

	int nParts = parent.body().numBodyParts();
	for(int i=0; i<nParts; i++)
	{
		mimepp::BodyPart &part = parent.body().bodyPartAt(i);
		mimepp::MediaType::MType type = part.headers().contentType().typeAsEnum();

		//extract attachment name
		QString strContType = part.headers().fieldBody("Content-Type").text().c_str();
		QString strName;
		int nPos = strContType.indexOf("name=\"", 0, Qt::CaseInsensitive);
		if(nPos >= 0){
			strName = strContType.mid(nPos+6);
			nPos = strName.indexOf("\"");
			if(nPos >= 0)
				strName = strName.left(nPos);
		}
		QString strCharset = "US-ASCII";
		if (part.headers().hasField("Content-Type")) {
			const mimepp::MediaType& mtype = part.headers().contentType();
			mimepp::String subtype = mtype.subtype();
			int numParams = mtype.numParameters();
			for (int i=0; i < numParams; ++i) {
				const mimepp::Parameter& param = part.headers().contentType().parameterAt(i);
				const mimepp::String& name = param.name();
				if (0 == strcasecmp("charset", name.c_str())) {
					strCharset = param.value().c_str();
				}
			}
		}

		if(strName.isEmpty())
		{
			// not an attachment, must be the body
			if(!bBodyPartSet || !bBodyPartHTML)
			{
				if (type == mimepp::MediaType::MULTIPART)
				{
					if(ParseMessageParts(part, lstRowResult, lstEmbeddedImgs)){
						 bBodyPartSet = true;
					}
				}
				else if (type == mimepp::MediaType::TEXT)
				{
					//get body, make sure to decode it correctly
					mimepp::String text = part.body().getString();
					int cte = mimepp::TransferEncodingType::_7BIT;
					if (part.headers().hasField("Content-Transfer-Encoding")) {
						cte = part.headers().contentTransferEncoding().asEnum();
					}
					if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
						mimepp::QuotedPrintableDecoder dec;
						text = dec.decode(text);
					}
					else if (cte == mimepp::TransferEncodingType::BASE64) {
						mimepp::Base64Decoder dec;
						text = dec.decode(text);
					}
					else{
						//mimepp::TransferEncodingType::_7BIT
						//mimepp::TransferEncodingType::_8BIT
						//mimepp::TransferEncodingType::BINARY
						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("ERROR: unsupported email body encoding: %1").arg(cte));
						//Q_ASSERT(false);	//unsupported attachment encoding
					}

					//make sure to convert to the proper code page
					QByteArray datRTF = text.c_str();
					QString strRTF = datRTF;
					if(!strCharset.isEmpty()){
						QTextCodec *codec = QTextCodec::codecForName(strCharset.toLocal8Bit().constData());
						if(codec)
							strRTF = codec->toUnicode(datRTF);
					}
					
					if(!strRTF.isEmpty()){
						//can still be text-only, check mail type
						bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0);
						lstRowResult.setData(nCurRow, "BEM_BODY", strRTF);
						bBodyPartHTML = bHtml;
					}
					bBodyPartSet = true;
				}
				else{
					//int i=0; //TOFIX ?
				}
			}
		}
		else
		{
			//an attachment

			//Attachment contents
			// Check content-transfer-encoding, and decode if necessary
			mimepp::String text = part.body().getString();
			int cte = mimepp::TransferEncodingType::_7BIT;
			if (part.headers().hasField("Content-Transfer-Encoding")) {
				cte = part.headers().contentTransferEncoding().asEnum();
			}
			if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
				mimepp::QuotedPrintableDecoder dec;
				text = dec.decode(text);
			}
			else if (cte == mimepp::TransferEncodingType::BASE64) {
				mimepp::Base64Decoder dec;
				text = dec.decode(text);
			}
			else{
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("ERROR: unsupported attachment encoding: %1").arg(cte));
				Q_ASSERT(false);	//unsupported attachment encoding
			}

			//extract CID
			if (part.headers().hasField("Content-ID")){
				QString strCID = part.headers().fieldBody("Content-ID").text().c_str();
				//strip <> quotes
				strCID.chop(1); 
				strCID = strCID.right(strCID.length()-1);
				qDebug() << "Attachment CID:" << strCID;

				//store embedded image (attachment) data
				if(!strCID.isEmpty()){
					lstEmbeddedImgs.addRow();
					int nCurRow = lstEmbeddedImgs.getRowCount()-1;

					lstEmbeddedImgs.setData(nCurRow, "CID", strCID);
					lstEmbeddedImgs.setData(nCurRow, "Name", strName);
					QByteArray data(text.c_str(), text.length());
					lstEmbeddedImgs.setData(nCurRow, "ImageData", data);
				}
			}
		}
	}
	return bBodyPartSet;
}

void EmbeddCIDAttachments(mimepp::Entity &parent, DbRecordSet &lstRowResult, DbRecordSet &lstEmbeddedImgs)
{
	int nCurRow = lstRowResult.getRowCount()-1;
	QString strBody = lstRowResult.getDataRef(nCurRow, "BEM_BODY").toString();
	bool bChanged = false;
	int nStartPos = 0;

	while(1)
	{
		//search sample: <img src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
		//search sample: <img id="aaaa" src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
		int nPos = strBody.indexOf("<img", nStartPos, Qt::CaseInsensitive);
		if(nPos < 0)
			break;

		int nPosLinkEnd = strBody.indexOf(">", nPos+1);
		bool bStrange = false;
		int nPos2 = strBody.indexOf("src=\"cid:", nPos+1);
		if(nPos2 < 0){
			nPos2 = strBody.indexOf("src=3D\"cid:", nPos+1);
			bStrange = true;
		}

		if(nPos2 < 0 || nPos2 > nPosLinkEnd)
		{
			nStartPos = nPos+1;
			continue;
		}

		bChanged = true;

		//extract CID
		QString strCIDLink;
		QString strCID;
		if(nPosLinkEnd >= 0){
			strCIDLink = strBody.mid(nPos, nPosLinkEnd-nPos+1);

			nPos2 += bStrange? strlen("src=3D\"cid:") : strlen("src=\"cid:");
			strCID = strBody.mid(nPos2, nPosLinkEnd-nPos2+1);
			int nEnd = strCID.indexOf("\"");
			if(nEnd > 0)
				strCID = strCID.mid(0, nEnd);
		}

		if(strCID.isEmpty()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("ERROR: invalid, empty CID in HTML"));
			Q_ASSERT(false);		//invalid CID in HTML
			nStartPos = nPos+1;
			continue;
		}

		//find attachment having matching CID
		int nAttIdx = -1;
		int nImgCount = lstEmbeddedImgs.getRowCount();
		for(int i=0; i<nImgCount; i++)
		{
			if(strCID == lstEmbeddedImgs.getDataRef(i, "CID").toString())
			{
				nAttIdx = i;
				break;
			}
		}

		//replace link with the embedded data
		//Q_ASSERT(nAttIdx >= 0);
		if(nAttIdx < 0){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("ERROR: could not find attachment by CID"));
			//Q_ASSERT(false);		//invalid CID in HTML
			nStartPos = nPos + qMin(1, strCIDLink.length());
			continue;				
		}

		//convert to base64
		QByteArray arDataBase64 = lstEmbeddedImgs.getDataRef(nAttIdx, "ImageData").toByteArray().toBase64();
		QString strFile = lstEmbeddedImgs.getDataRef(nAttIdx, "Name").toString();

		//rewrite tag in the HTML body
		strBody = strBody.mid(0, nPos) + strBody.mid(nPos + strCIDLink.length());

		//use extension for mime type
		QString strExt = strFile;
		int nPosExt = strExt.lastIndexOf('.');
		if(nPosExt > 0)
			strExt = strExt.mid(nPosExt+1);

		QString strNewTag = QString("<img src=\"data:image/%1;base64,").arg(strExt);
		strNewTag += arDataBase64;
		strNewTag += "\">";
		strBody.insert(nPos, strNewTag);

		//keep searching forward
		nStartPos = nPos + strNewTag.length();
	}
	if(bChanged)
	{
		//pack changed body back
		lstRowResult.setData(nCurRow, "BEM_BODY", strBody);
	}
}

#endif

//for one mail filters biggest attachment until sum <16mb
//FUI_ImportEmail::FilterAttachmentRecords
void FilterAttachmentRecords(DbRecordSet &lstAttachments)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - filter attachment records"));

	while(lstAttachments.getStoredByteSize()>MAX_ATTACHMENT_LIST_SIZE)
	{
		int nMaxAttach=0;
		int nRowWithBiggestAttachment=-1;
		int nSize=lstAttachments.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nAttchSize=lstAttachments.getDataRef(i,"BEA_CONTENT").toByteArray().size();
			if (nAttchSize>nMaxAttach)
			{
				nMaxAttach=nAttchSize;
				nRowWithBiggestAttachment=i;
			}
		}
		if (nRowWithBiggestAttachment==-1)
		{
			Q_ASSERT(false); //this means that list >40Mb and no attachments!?
			return;
		}
		QString strName = lstAttachments.getDataRef(nRowWithBiggestAttachment, "BEA_NAME").toString();
		//QByteArray datNewContents = qCompress(tr("Attachment '%1� is too big (%d MB), Skipped!")).arg(strName).arg(nAttSizeMB).toLatin1());
		QByteArray datNewContents = QString("Attachment size limits reached. '%1� Skipped!").arg(strName).toLatin1();
		datNewContents = qCompress(datNewContents,1);
		lstAttachments.setData(nRowWithBiggestAttachment, "BEA_CONTENT", datNewContents);
		//overwrite name
		strName = "Attachment Skipped � Read Info.txt";
		lstAttachments.setData(nRowWithBiggestAttachment, "BEA_NAME", strName);

		g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,0,QString("Attachment %1 are not imported, maximum size is exceeded").arg(lstAttachments.getDataRef(nRowWithBiggestAttachment,"BEA_NAME").toString()));
	}
}

#ifdef _WIN32
//MailManager::ProcessAddressList
void ProcessAddressList(mimepp::AddressList &list, DbRecordSet &lstRowResult, int nCurRow, int nType, bool bAreTemplates, QSet<QString> &setTmp)
{
	int nAddrCnt = list.numAddresses();
	for(int i=0; i<nAddrCnt; i++)
	{
		if(!bAreTemplates)
		{
			QString strName, strEmail;
			mimepp::Address &addr = list.addressAt(i);
			mimepp::Mailbox addr1(addr.getString());
			addr1.parse();

			strName  = QString::fromUtf8(addr1.displayNameUtf8().c_str());
			strEmail  = addr1.localPart().c_str();
			strEmail += "@";
			strEmail += addr1.domain().c_str();

			//append new recipient
			QString strValue;
			if(1 == nType)
				strValue = lstRowResult.getDataRef(nCurRow, "BEM_TO").toString();
			else if(2 == nType)
				strValue = lstRowResult.getDataRef(nCurRow, "BEM_CC").toString();
			else if(3 == nType)
				strValue = lstRowResult.getDataRef(nCurRow, "BEM_BCC").toString();

			if(!strValue.isEmpty())
				strValue += ";";

			QString strMail(strName);
			if(!strEmail.isEmpty()){
				strMail += " <";
				strMail += strEmail;
				strMail += ">";
			}

			strValue += strMail;

			//qDebug() << "Email" << strEmail;
			setTmp.insert(strEmail);

			//store back the value
			if(1 == nType)
				lstRowResult.setData(nCurRow, "BEM_TO", strValue);
			else if(2 == nType)
				lstRowResult.setData(nCurRow, "BEM_CC", strValue);
			else if(3 == nType)
				lstRowResult.setData(nCurRow, "BEM_BCC", strValue);
		}
	}
}
#endif

bool Pop3Connect(mailpp::Pop3Client &client, const QString &strHost, int nPort, bool bUseSTLS, bool bUseSSL)
{
#define NUM_SSL_METHODS 4
	bool bWait    = !bUseSSL || bUseSTLS;
	bool bConnected = false;

	if(client.Connect(strHost.toLocal8Bit().constData(), nPort, bWait) < 0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Failed to connect to POP3 host: %1:%2").arg(strHost).arg(nPort));
	#ifdef _DEBUG
		qDebug() << "Failed to connect to email host.";
	#endif
		return false;
	}
	//connected, check the reply code as well
	if(bWait && mailpp::Pop3Client::OK != client.ReplyCode()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
	#ifdef _DEBUG
		qDebug() << "POP3 error:" << client.ErrorMessage();
	#endif
		client.Disconnect();
		return false;
	}

	//initiate SSL if needed
	if(bUseSTLS)
	{
		if(client.Stls() < 0){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to arrange TLS connection to POP3 host.");
		#ifdef _DEBUG
			qDebug() << "Failed to arrange TLS connection to POP3 host.";
		#endif
			client.Disconnect();
			return false;
		}
		if(mailpp::Pop3Client::OK != client.ReplyCode()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
		#ifdef _DEBUG
			qDebug() << "POP3 error:" << client.ErrorMessage();
		#endif
			client.Quit();
			client.Disconnect();
			return false;
		}
	}
	if(bUseSSL)
	{
		//
		// using more complex code to be able to ignore SSL error about hostname mismatch within the certificate
		//
#if 1
		SSL_CTX	*ctx = SSL_CTX_new(SSLv23_client_method());	//supports SSLv2, SSLv2 and TLSv1
		SSL *ssl = NULL;
		if(ctx){
			SSL_CTX_set_options(ctx, SSL_OP_ALL);
			SSL_CTX_set_cert_verify_callback(ctx, verify_callback2, NULL);
			ssl = SSL_new(ctx);
		}
		else{
			char szBuf[1000];
			ERR_error_string(ERR_get_error(), szBuf);
			qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";
			client.Disconnect();
			return false;
		}
		if( client.ConnectTlsOpenSSL(strHost.toLocal8Bit().constData(), ssl, true) < 0 &&
			mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
#else
		//FIX: this one reports "bad packet length"	
		if( client.ConnectTls(strHost.toLocal8Bit().constData(), true) < 0 &&
			mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
#endif
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Failed to establish TLS connection to POP3 host (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));
		#ifdef _DEBUG
			qDebug() << QString("Failed to establish TLS connection to POP3 host (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage());
		#endif

			char szBuf[1000];
			ERR_error_string(ERR_get_error(), szBuf);
			qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";
			client.Disconnect();
			return false;
		}
		else{
			if(mailpp::Pop3Client::OK != client.ReplyCode()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
			#ifdef _DEBUG
				qDebug() << "POP3 error:" << client.ErrorMessage();
			#endif
				client.Quit();
				client.Disconnect();
				return false;
			}
		}
	}
	return true;
}

//
// Extracts email and name of the original sender of the forwarded message
// input body can be plain text or HTML
//
// example input strings:
//'<div class="gmail_quote">---------- Forwarded message ----------<br>From: <b class="gmail_sendername">Mario</b> <span dir="ltr">&lt;mmatkic@hotmail.com&gt;</span><br>Date: 2012/10/7<br>Subject: Termo-Plus mail<br>To: Matkovic Petar &lt;petarr@gmail.com&gt;<br>Cc: Jadran Matkovic &lt;jadran_72@yahoo.de&gt;<br><br><br>';
//'From: Bruno Kustre [mailto:bruno.kustre@gmail.com] Sent: Sunday, September 23, 2012 11:34 PM To: pero Subject: Re: bst 33 charger Ne bi ti prepor...';
//'Von: Bruno Kustre [mailto:Johan.Krauss@gmail.com] Sent: Sunday, September 23, 2012 11:34 PM An: pero Subject: Re: bst 33 charger Ne bi ti preporu...';

//MP's code converted to C++ by MR (needs testing!!!!!)
void ResolveEmailSender(const QString &strBody, QString &strResSenderEmail, QString &strResSenderName)
{
	//QString strBody;
	//DataHelper::LoadFile("D:\\body.txt",strBody);
	//initialize
	strResSenderEmail = "";
	strResSenderName = "";

	const static QString langFromTo[][2] = {
		{"from","to"},		//en
		{"od","do"},		//hr
		{"von","an"},		//de
		{"de","a"},			//fr
		{"de","para"}		//es
	};	

	int arrSize=5;	
	QString strFrom, strTo;	// from & to strings in particular language
	int idxFrom, idxTo;		// indexes

	// iterate trough langFromTo array searching only in 1st column ([0][0], [1][0],..) for a matching "from"
	for(int i=0; i<arrSize; i++)
	{
		strFrom = langFromTo[i][0];	// from string ("von", "de", "from",..)
		idxFrom = strBody.indexOf(strFrom+':',0,Qt::CaseInsensitive);		// first "from" occurence in str (*note: we heavily rely on "From:") 

		if(idxFrom > -1)
		{
			idxFrom += strFrom.length()+1;		// position idxFrom to end of the word "From:" 
			strTo = langFromTo[i][1];			// to string ("nach", "para", "to",..)	
			//idxTo = strBody.indexOf(strTo+':',0,Qt::CaseInsensitive);	

			//B.T. was here on 30.04.2013, MB requested that this works, so here I'm: lock for '@' and extract email after 'from' 
			//to can not be found: as mailto: is equal to To:
			//MAIL address:
			int nFromStartIdx=0,nFromEndIdx=0;
			int nCurrIdx=strBody.indexOf("@",idxFrom);
			if (nCurrIdx>0)
			{
				//start: try to find mailto:
				nCurrIdx=strBody.lastIndexOf("mailto:",nCurrIdx,Qt::CaseInsensitive);
				if (nCurrIdx>idxFrom)
				{
					nFromStartIdx=nCurrIdx+QString("mailto:").length(); //start of mail
					QString strEnclosedChar=strBody.mid(nCurrIdx-1,1);
					if (!strEnclosedChar.isEmpty())
					{
						if (strEnclosedChar=="[")
						{
							nCurrIdx = strBody.indexOf("]",nFromStartIdx,Qt::CaseInsensitive);
							if (nCurrIdx>0) 
							{
								nFromEndIdx = nCurrIdx;
							}
						}
						else if (strEnclosedChar=="\"")
						{
							nCurrIdx = strBody.indexOf("\"",nFromStartIdx,Qt::CaseInsensitive);
							if (nCurrIdx>0) 
							{
								nFromEndIdx = nCurrIdx;
							}
						}
						else if (strEnclosedChar=="<")
						{
							nCurrIdx = strBody.indexOf(">",nFromStartIdx,Qt::CaseInsensitive);
							if (nCurrIdx>0) 
							{
								nFromEndIdx = nCurrIdx;
							}
						}
					}
					else
					{
						//try to find ]
						nCurrIdx = strBody.indexOf("]",nFromStartIdx,Qt::CaseInsensitive);
						if (nCurrIdx>0)// && nCurrIdx < idxTo)
						{
							//try to find double quotes till To
							nFromEndIdx = nCurrIdx;
						}

					}

				}
			}

			if (nFromEndIdx>0 && nFromStartIdx>0)
			{
				strResSenderEmail = strBody.mid(nFromStartIdx,nFromEndIdx-nFromStartIdx).trimmed();
				if (strResSenderEmail.contains("<") || strResSenderEmail.contains("<"))
				{
					//we failed:
					return;
				}

				//MAIL name:
				//Name is hidden in string From: till mailto:
				nCurrIdx=strBody.indexOf("mailto:",idxFrom,Qt::CaseInsensitive);
				if (nCurrIdx>0)
				{
					QString strSuspect=strBody.mid(idxFrom,nCurrIdx-idxFrom);
					//strip tags:
					while (strSuspect.indexOf("<")>=0)
					{
						int nHtmlTagIdx=strSuspect.indexOf("<");
						nCurrIdx = strSuspect.indexOf(">",nHtmlTagIdx);
						if (nCurrIdx>0)
						{
							strSuspect.replace(nHtmlTagIdx,nCurrIdx-nHtmlTagIdx+1,"");
						}
						else
							strSuspect=strSuspect.left(nHtmlTagIdx); //buzz of last part if tag is not enclosed

					}
					strSuspect.replace("[","");
					strSuspect.replace("]","");
					if (strSuspect.contains("<") || strSuspect.contains("<"))
					{
						//we failed:
						return;
					}
					strSuspect=strSuspect.trimmed();
					if (!strSuspect.isEmpty())
					{
						strResSenderName=strSuspect;
					}
				}

				//break from FOR loop:
				break;

			}





/*

			// watch out for possibility: "From: Bruno Kustre [mailto:bruno.kustre@gmail.com] To: pero"
			// search for the "To" which precedes a space or CR (we don't want: "mailto")
			// To: not [mailto:
			QString strTemp = strBody.mid(0,idxTo);   
			if( strTemp.endsWith(" ")||
				strTemp.endsWith("&#32;")||
				strTemp.endsWith("<br>")||
				strTemp.endsWith("&#13;")||		
				strTemp.endsWith("&#10;"))
			{	
				idxTo = strBody.indexOf(strTo+':',0,Qt::CaseInsensitive);		
			}
			else{
				int nPos = strBody.indexOf(strTo+':',Qt::CaseInsensitive);	
				idxTo = strBody.indexOf(strTo+':',nPos+1,Qt::CaseInsensitive);		  // second "to:" apearance
			}

			strFrom = strBody.mid(idxFrom,idxTo-idxFrom).trimmed();	  // cut
			strFrom = strFrom.replace(QRegExp("/<\\/?[^>]+(>|$)/g"), ""); // strip html tags (keep only text)
			qDebug() << "clean:" << strFrom;

			// A) Look for: '<' and '>' E.G: <mailto:petarr@gmail.com>
			if(strFrom.indexOf("&lt;",0,Qt::CaseInsensitive) > -1)
			{
				idxFrom=strFrom.indexOf('&lt;',0,Qt::CaseInsensitive)+4;
				idxTo=strFrom.indexOf('&gt;',0,Qt::CaseInsensitive);
				strFrom=strFrom.mid(idxFrom,idxTo-idxFrom);
				strResSenderEmail = strFrom;
				return;
			}
			// B) '[' and ']' E.G: [mailto:petarr@gmail.com]
			else if(strFrom.indexOf("[mailto:",0,Qt::CaseInsensitive) > -1)
			{
				idxFrom=strFrom.indexOf("[mailto:",0,Qt::CaseInsensitive)+8;
				idxTo=strFrom.indexOf(']',idxFrom+1,Qt::CaseInsensitive);
				strFrom = strFrom.mid(idxFrom,idxTo-idxFrom);
				strResSenderEmail = strFrom;
				return;
			}
			else if(0)
			{
				// @
				// newline
				// email
				// spaces
				// ?? heuristics
			}*/
		}
	}		
}
