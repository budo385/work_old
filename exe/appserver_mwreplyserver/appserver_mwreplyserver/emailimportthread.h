#ifndef EMAILIMPORTTHREAD_H
#define EMAILIMPORTTHREAD_H

#include <QTimer>
#include <QReadWriteLock>

#include "common/common/backgroundtaskinterface.h"
#include "common/common/dbrecordset.h"
#include "common/common/status.h"


//Separate Thread object
class EmailImportThread : public BackgroundTaskInterface
{
	//int m_nIniLstRow;
public:
	EmailImportThread();
	~EmailImportThread();

	int		LoadEmailAccounts();

	void	ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");									//separate thread-> execute backup

private:
	void DoActionPop3();
	void WriteEmailAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments);

	QReadWriteLock	m_Lck_m_bThreadRunning;
	DbRecordSet		m_lstEmailAccounts;  //
};


#endif // EMAILIMPORTTHREAD_H
