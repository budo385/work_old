#ifndef SERVICEHANDLER_H
#define SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "db/db/dbsqlquery.h"
#include "interface_servicehandler.h"
#include "applicationserverselftest.h"

class ServiceHandler: public Interface_ServiceHandler
{
public:
	friend class ApplicationServerSelfTest;

	enum SubscriptionStatus
	{
		SUBSCRIPTION_STATUS_TEST=0,
		SUBSCRIPTION_STATUS_PAID=1,
		SUBSCRIPTION_STATUS_TEST_EXPIRED=2,
		SUBSCRIPTION_STATUS_PAID_EXPIRED=3
	};

	enum AllowLogin
	{
		LOGIN_DENY=0,
		LOGIN_ALLOW=1,
		LOGIN_ALWAYS_ALLOW=2
	};


	//public API over REST
	void IsRegistered(Status &pStatus, QString strEmail,int &Ret_IsRegistered, QString &Ret_strESHost, int &Ret_nNumDaysLeft, int &Ret_nSubscriptionStatus, QString strReceipt=""); 
	void SendConfirmationEmail(Status &pStatus, QString strEmail,QString &Ret_strClientPassword, QString &Ret_strESHost, bool bSkipLBNotify=false, QString strReceipt="");
	void ReadEmails(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,DbRecordSet &Ret_Emails);
	void GetEmail(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,int nEmailID, DbRecordSet &Ret_EmailData);
	void SetAddOnPass(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,QString strAddOnPass);
	void GetAuthenticationID(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,QString &Ret_strClientPassword);

	void RegisterSMTPAccount(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken, QString strServerAddress, QString strUserName, QString strPassword, int nPort, QString strBCCAddressForSentMailCopy);
	void UnregisterSMTPAccount(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken);
	void SendEMail(Status &pStatus,QString strEmail, QString strMimeFrom, QString strConce, QString strAuthToken,QString strTo,QString lstCC,QString lstBCC,QString strSubject,QString strMimeBody,int &Ret_ID);
	void CheckEmailStatus(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, DbRecordSet &lstEmailTasks, DbRecordSet &Ret_lstEmailTasksStatus);
	void StartMultiRegisterPeriod(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken);

	//cloud api:
	void ReadDataList(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, DbRecordSet &Ret_List);
	void GetData(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, QString strName, QString &Ret_strData);
	void StoreData(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, QString strName, QString strData);

	//admin stat api: (login is done on client)
	void GetStats(Status &pStatus, QString strConce, QString strAuthToken, int &Ret_nRegisteredUsers, int &Ret_nRegisteredUsersInLast7days, int &Ret_nUsersPendingConfirmation, int &Ret_nPaid,int &Ret_nPaidExpired, int &Ret_nTest,int &Ret_nTestExpired);
	void GetUsers(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstRegisteredEmails, int &Ret_nTotalRegisteredUsers, int nSortOrder=-1, int nFromN=-1, int nToN=-1);
	void WriteAccessEmails(Status &pStatus, QString strConce, QString strAuthToken, QString strEmails);
	void DeleteAccessEmail(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail);
	void SetUserSubscriptionAccess(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nLoginLevel);
	void DeleteUser(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail);

	void AddESHost(Status &pStatus, QString strConce, QString strAuthToken, QString strESHost);
	void DeleteESHost(Status &pStatus, QString strConce, QString strAuthToken, QString strESHost);
	void GetESHosts(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstESHosts);
	void RegisterESHost2Users(Status &pStatus, QString strConce, QString strAuthToken);
	void RegisterESHost2SingleUser(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail);

	//mwLB server API:
	void LockDatabase(Status &pStatus, QString strConce, QString strAuthToken, int nLockReadOnly);
	void IsServerAlive(Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strLoadBalanceServerURL, int &Ret_nStatus);
	void AddEmailToCS(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail);
	void RemoveEmailToCS(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail);
	void UnregisterLBServer(Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strLoadBalanceServerURL);
	//LB comm
	void RegisterEmailToLBServer(Status &pStatus, QString strEmail);
	void SyncEmailsToLBServer(Status &pStatus);
	
	//internal service API
	void WriteEmails(Status &pStatus, DbRecordSet &lstEmails,DbRecordSet &Ret_lstStatus); //for importer thread
	//void CleanUpData(Status &pStatus); //for GBC: clean expired mails and dead confirmations
	void CleanUpOldAccounts(Status &pStatus); //for GBC: clean expired mails and dead confirmations
	void SendUserErrorPushMessageToESHost(Status &pStatus, int nESRID, QString strUser, QString strEmailFrom, QString strErrorMessage);

	//for test purpose
	void GetServerTime(Status &Ret_Status, QString strEmail, QString strConce, QString strAuthToken, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt);
	void SetNextTestTime(Status &Ret_Status, QString strEmail, QString strConce, QString strAuthToken, QDateTime datSetTimeNextTest);
	void CheckAppleReceipt(Status &pStatus,QString strReceipt);

private:
	void AuthenticateUser(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken);
	void SendEmail(Status &err, QString strTO, QString strURL);//Subject, QString strBody);
	void MarkForDeleteSelectedRows(DbRecordSet &lstEmails, DbRecordSet &Ret_lstStatus);
	int		FindESHost(QString &strHostIP);
	void	RegisterUserToESHost(Status &pStatus, int nESRID, QString strUser, QString strPass);
	void	ChangePassUserToESHost(Status &pStatus, int nESRID, QString strUser, QString strPass);
	void	DeleteUserOnESHost(Status &pStatus, int nESRID, QString strUser);
	void	ReadEmailCntFromToESHost(Status &pStatus, int nESRID, DbRecordSet &Ret_lstData);
	void	EnableUserAccountOnESHost(Status &pStatus, int nESRID, QString strUser, bool bEnable);

	
};

#endif // SERVICEHANDLER_H

/*
TODO: 
- skeleton for REST and incoming Dbrecordset->automatically
- authentication
- 


https://localhost:9999/rest/service/isregistered
<REQUEST><PARAMETERS><strEmail>trumbic@helix-business-soft.hr</strEmail></PARAMETERS></REQUEST>

https://localhost:9999/rest/service/sendconfirmationemail
<REQUEST><PARAMETERS><strEmail>trumbic@helix-business-soft.hr</strEmail></PARAMETERS></REQUEST>

https://localhost:9999/rest/service/reademails
<REQUEST><PARAMETERS><strEmail>trumbic@helix-business-soft.hr</strEmail>
<strConce></strConce><strAuthToken></strAuthToken>
</PARAMETERS></REQUEST>

https://localhost:9999/rest/service/getemail
<REQUEST><PARAMETERS><strEmail>trumbic@helix-business-soft.hr</strEmail>
<strConce></strConce><strAuthToken></strAuthToken>
<nEmailID>1</nEmailID>
</PARAMETERS></REQUEST>

https://localhost:9999/rest/service/registersmtpaccount
<REQUEST><PARAMETERS>
<strEmail>trumbic@helix-business-soft.hr</strEmail>
<strConce></strConce><strAuthToken></strAuthToken>
<strServerAddress>mail.t-com.hr</strServerAddress>
<strUserName></strUserName><strPassword></strPassword>
<nPort>25</nPort><strBCCAddressForSentMailCopy>trumbic@helix-business-soft.hr</strBCCAddressForSentMailCopy>
</PARAMETERS></REQUEST>

https://localhost:9999/rest/service/unregisersmtpaccount
<REQUEST><PARAMETERS>
<strEmail>trumbic@helix-business-soft.hr</strEmail>
<strConce></strConce><strAuthToken></strAuthToken>
</PARAMETERS></REQUEST>

https://localhost:9999/rest/service/sendemail
<REQUEST><PARAMETERS>
<strEmail>trumbic@helix-business-soft.hr</strEmail>
<strConce></strConce><strAuthToken></strAuthToken>
<strTo>infoline@helix-business-soft.hr</strTo>
<lstCC></lstCC><lstBCC></lstBCC>
<strSubject>Pozdrav</strSubject>
<strMimeBody>Hello</strMimeBody>
</PARAMETERS></REQUEST>


https://localhost:9999/rest/service/checkemailstatus
<REQUEST><PARAMETERS>
<strEmail>trumbic@helix-business-soft.hr</strEmail>
<strConce></strConce><strAuthToken></strAuthToken>
<lstEmailTasks><row><ID>1</ID></row></lstEmailTasks>
</PARAMETERS></REQUEST>


https://localhost:9999/rest/service/getstats
<REQUEST><PARAMETERS><strConce></strConce><strAuthToken></strAuthToken></PARAMETERS></REQUEST>

https://localhost:9999/rest/service/getusers
<REQUEST><PARAMETERS><strConce></strConce><strAuthToken></strAuthToken><nSortOrder>-1</nSortOrder><nFromN>-1</nFromN><nToN>-1</nToN></PARAMETERS></REQUEST>

*/