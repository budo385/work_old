#include "servicehandler.h"
#include "common/common/authenticator.h"
#include "trans/trans/smtpclient.h"
#include "dbtabledef.h"
#include "common/common/sha2.h"
#include "common/common/datahelper.h"
#include "trans/trans/resthttpclient.h"
#include "trans/trans/httpreadersync.h"
#include "common/common/threadid.h"
#include <QJsonDocument>


#include "common/common/logger.h"
extern Logger g_Logger;
#include "db/db/dbsqlmanager.h"
extern DbSqlManager	*g_DbManager;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;
#include "emailsendermanager.h"
extern EmailSenderManager *g_pEmailSenderManager;



#define SECRET_SPICE_PASSWORD "BANDIT"
#define STAT_USERNAME "mcadmin"
#define STAT_PASSWORD "pachelbel"

#define SERVER_INTERSERVER_COMM_USERNAME "system"
#define SERVER_INTERSERVER_COMM_PASSWORD "balrog10"

#define MW_LB_USERNAME "mwlb_admin"
#define MW_LB_PASSWORD "ronin10"

#define APPLE_RECEIPT_CHECK_INTERVAL_DAYS 14

#define LB_SERVER_LOOKUP "http://mailegant.com/lb.txt"


#define TURN_OFF_AUTHENTICATION 0

//is registered
/*
	A - adresa nije u bazi
	B - adresa je registrirana, ali jos nije potvrdena (confirmation email sent)
	C - adresa je registrirana i potvrdena, ali ima novi confirmation request koji nije potvrden 
	D - adresa je registrirana i potvrdena

*/
void ServiceHandler::IsRegistered(Status &pStatus, QString strEmail,int &Ret_IsRegistered, QString &Ret_strESHost, int &Ret_nNumDaysLeft, int &Ret_nSubscriptionStatus, QString strReceipt)
{
	strEmail=strEmail.toLower();
	Ret_IsRegistered=false;
	Ret_strESHost="";
	Ret_nNumDaysLeft=-1;
	Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_TEST_EXPIRED;

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString		strSql;
	QString		strReconfirm;
	int			nIsConfirmed=0;
	QDateTime	datLastLogin,datFirstLogin,datLastReceiptCheck;
	QString		strReceiptDB;
	int			nAllowLogin=0;
	int			nIsExists=0;
	int			nUserID;

	//check if email exists
	query.Execute(pStatus,"SELECT USR_ID,USR_DAT_LAST_MODIFIED,USR_IS_CONFIRMED,USR_RECONFIRM_PASSWORD,USR_DAT_LAST_LOGIN, USR_DAT_FIRST_LOGIN, USR_DAT_LAST_RECEIPT_CHECK, USR_APPLE_RECEIPT, USR_ALLOW_LOGIN, ESR_HOST_ADDRESS, ESR_ID FROM USER_DATA LEFT OUTER JOIN EMAIL_SERVERS ON ESR_ID=USR_EMAIL_SERVER_ID WHERE USR_EMAIL = '"+strEmail+"'");
	if (!pStatus.IsOK())return; 

	DbRecordSet rowUserData;
	query.FetchData(rowUserData);

	if (rowUserData.getRowCount()>0)
	{
		int nEsrID=rowUserData.getDataRef(0,"ESR_ID").toInt();
		Ret_strESHost=rowUserData.getDataRef(0,"ESR_HOST_ADDRESS").toString();
		rowUserData.removeColumn(rowUserData.getColumnIdx("ESR_HOST_ADDRESS")); //remove it right away
		rowUserData.removeColumn(rowUserData.getColumnIdx("ESR_ID")); //remove it right away
		
		if (rowUserData.getDataRef(0,"USR_IS_CONFIRMED").toInt()==0)
		{
			Ret_IsRegistered=1;
		}
		else if (!rowUserData.getDataRef(0,"USR_RECONFIRM_PASSWORD").toString().isEmpty())
		{
			Ret_IsRegistered=2;
		}
		else
			Ret_IsRegistered=3;

		//-------------------------------------------------------------------------------------------
		//check if always allowed, if so then just update last login date and skip all other checks:
		//-------------------------------------------------------------------------------------------
		if (rowUserData.getDataRef(0,"USR_ALLOW_LOGIN").toInt()==LOGIN_ALWAYS_ALLOW)
		{			
			int nTrialPeriodDays=g_AppServer->m_INIFile.m_nNumCloudTestingPeriodDays;
			Ret_nNumDaysLeft=nTrialPeriodDays;
			Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_TEST; 
			rowUserData.removeColumn(rowUserData.getColumnIdx("USR_APPLE_RECEIPT"));
			rowUserData.setData(0,"USR_DAT_LAST_LOGIN",QDateTime::currentDateTime());
			query.WriteData(pStatus,"USER_DATA",rowUserData);
			return;
		}

		//-------------------------------------------------------------------------------------------
		//check if receiptDB!=receipt && receipt>0 -> register new receipt
		//-------------------------------------------------------------------------------------------
		if (rowUserData.getDataRef(0,"USR_APPLE_RECEIPT").toString()!=strReceipt && !strReceipt.isEmpty())
		{
			//we have new strReceipt, check on server right away
			CheckAppleReceipt(pStatus,strReceipt);
			if (!pStatus.IsOK())
			{
				//log status and return as expired paid user:
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("User %1 tried to login with invalid apple receipt string, status set to paid subscription expired. Apple server error was: %2!").arg(strEmail).arg(pStatus.getErrorText()));
				pStatus.setError(0);
				rowUserData.setData(0,"USR_DAT_LAST_RECEIPT_CHECK",QDateTime::currentDateTime());
				//rowUserData.setData(0,"USR_DAT_LAST_LOGIN",QDateTime::currentDateTime());
				rowUserData.setData(0,"USR_APPLE_RECEIPT",strReceipt);
				Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_PAID_EXPIRED;
				if (rowUserData.getDataRef(0,"USR_ALLOW_LOGIN").toInt()!=LOGIN_DENY)
					EnableUserAccountOnESHost(pStatus,nEsrID,strEmail,false);
				rowUserData.setData(0,"USR_ALLOW_LOGIN",LOGIN_DENY);
				query.WriteData(pStatus,"USER_DATA",rowUserData);
				return;
			}
			else
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("User %1 logged with new apple receipt string, status set to paid subscription!").arg(strEmail));
				//we have valid new receipt (first paid or renewable):
				rowUserData.setData(0,"USR_DAT_LAST_RECEIPT_CHECK",QDateTime::currentDateTime());
				rowUserData.setData(0,"USR_DAT_LAST_LOGIN",QDateTime::currentDateTime());
				rowUserData.setData(0,"USR_APPLE_RECEIPT",strReceipt);
				Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_PAID;
				if (rowUserData.getDataRef(0,"USR_ALLOW_LOGIN").toInt()!=LOGIN_ALLOW)
					EnableUserAccountOnESHost(pStatus,nEsrID,strEmail,true);
				rowUserData.setData(0,"USR_ALLOW_LOGIN",LOGIN_ALLOW);
				Ret_nNumDaysLeft=rowUserData.getDataRef(0,"USR_DAT_FIRST_LOGIN").toDateTime().daysTo(QDateTime::currentDateTime());
				query.WriteData(pStatus,"USER_DATA",rowUserData);
				return;
			}
		}

		//-------------------------------------------------------------------------------------------
		//check if receiptDB==receipt && receipt>0 ->last date check
		//check if receipt==0 &&  receiptDB>0 -> last date check
		//-------------------------------------------------------------------------------------------
		if (!rowUserData.getDataRef(0,"USR_APPLE_RECEIPT").toString().isEmpty())
		{
			Ret_nNumDaysLeft=rowUserData.getDataRef(0,"USR_DAT_FIRST_LOGIN").toDateTime().daysTo(QDateTime::currentDateTime());

			//check if period is over or if access denied:
			if(rowUserData.getDataRef(0,"USR_DAT_LAST_RECEIPT_CHECK").toDateTime().daysTo(QDateTime::currentDateTime())>APPLE_RECEIPT_CHECK_INTERVAL_DAYS || rowUserData.getDataRef(0,"USR_ALLOW_LOGIN").toInt()!=SUBSCRIPTION_STATUS_PAID)
			{
				CheckAppleReceipt(pStatus,rowUserData.getDataRef(0,"USR_APPLE_RECEIPT").toString());
				if (!pStatus.IsOK())
				{
					//log status and return as expired paid user:
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("User %1 tried to login with invalid apple receipt string, status set to paid subscription expired. Apple server error was: %2!").arg(strEmail).arg(pStatus.getErrorText()));
					pStatus.setError(0);
					rowUserData.setData(0,"USR_DAT_LAST_RECEIPT_CHECK",QDateTime::currentDateTime());
					//rowUserData.setData(0,"USR_DAT_LAST_LOGIN",QDateTime::currentDateTime());
					Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_PAID_EXPIRED;
					if (rowUserData.getDataRef(0,"USR_ALLOW_LOGIN").toInt()!=LOGIN_DENY)
						EnableUserAccountOnESHost(pStatus,nEsrID,strEmail,false);
					rowUserData.setData(0,"USR_ALLOW_LOGIN",LOGIN_DENY);

					rowUserData.removeColumn(rowUserData.getColumnIdx("USR_APPLE_RECEIPT"));
					query.WriteData(pStatus,"USER_DATA",rowUserData);
					return;
				}
				else
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("User %1 logged with old/empty apple receipt string, receipt in DB is still valid, login allowed!").arg(strEmail));
					//we have valid new receipt (first paid or renewable):
					rowUserData.setData(0,"USR_DAT_LAST_RECEIPT_CHECK",QDateTime::currentDateTime());
					rowUserData.setData(0,"USR_DAT_LAST_LOGIN",QDateTime::currentDateTime());
					if (rowUserData.getDataRef(0,"USR_ALLOW_LOGIN").toInt()!=LOGIN_ALLOW)
						EnableUserAccountOnESHost(pStatus,nEsrID,strEmail,true);
					rowUserData.setData(0,"USR_ALLOW_LOGIN",LOGIN_ALLOW);
					Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_PAID;

					rowUserData.removeColumn(rowUserData.getColumnIdx("USR_APPLE_RECEIPT"));
					query.WriteData(pStatus,"USER_DATA",rowUserData);
					return;
				}
			}
			else
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("User %1 logged with old/empty apple receipt string, receipt in DB is still valid, Apple server check is not performed, login allowed!").arg(strEmail));
				Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_PAID;
							
				//write status back to database, remove long char field to speed up:
				query.Execute(pStatus,"UPDATE USER_DATA SET USR_DAT_LAST_LOGIN=CURRENT_TIMESTAMP WHERE USR_ID = "+QString::number(rowUserData.getDataRef(0,"USR_ID").toInt()));
				if (!pStatus.IsOK())return; 
			}
		}
		//-------------------------------------------------------------------------------------------
		else //check if receipt==0 &&  receiptDB==0 -> test user, test first login
		{
		//-------------------------------------------------------------------------------------------
			int nTrialPeriodDays=g_AppServer->m_INIFile.m_nNumCloudTestingPeriodDays;
			Ret_nNumDaysLeft=nTrialPeriodDays - rowUserData.getDataRef(0,"USR_DAT_FIRST_LOGIN").toDateTime().daysTo(QDateTime::currentDateTime());
			if (Ret_nNumDaysLeft>=0)
			{
				rowUserData.setData(0,"USR_DAT_LAST_LOGIN",QDateTime::currentDateTime());
				Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_TEST;
				if (rowUserData.getDataRef(0,"USR_ALLOW_LOGIN").toInt()!=LOGIN_ALLOW)
					EnableUserAccountOnESHost(pStatus,nEsrID,strEmail,true);
				rowUserData.setData(0,"USR_ALLOW_LOGIN",LOGIN_ALLOW);
			}
			else
			{
				//we have valid new receipt (first paid or renewable):
				rowUserData.setData(0,"USR_DAT_LAST_LOGIN",QDateTime::currentDateTime());
				Ret_nSubscriptionStatus=SUBSCRIPTION_STATUS_TEST_EXPIRED;
				if (rowUserData.getDataRef(0,"USR_ALLOW_LOGIN").toInt()!=LOGIN_DENY)
					EnableUserAccountOnESHost(pStatus,nEsrID,strEmail,false);
				rowUserData.setData(0,"USR_ALLOW_LOGIN",LOGIN_DENY);
			}

			//write status back to database, remove long char field to speed up:
			rowUserData.removeColumn(rowUserData.getColumnIdx("USR_APPLE_RECEIPT"));
			query.WriteData(pStatus,"USER_DATA",rowUserData);
			return;
		}
	}
	else
	{
		Ret_IsRegistered=0;
	}
}



void ServiceHandler::ReadEmails(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,DbRecordSet &Ret_Emails)
{
	strEmail=strEmail.toLower();

#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;


	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	query.Execute(pStatus,"SELECT BEM_ID, BEM_SUBJECT, BEM_RECV_TIME,BEM_ORIGINATOR_EMAIL,BEM_ORIGINATOR_NAME,BEM_FROM FROM EMAILS,USER_DATA WHERE BEM_FK_USER_ID = USR_ID AND USR_EMAIL = '"+strEmail+"'");
	if (!pStatus.IsOK())return; 

	query.FetchData(Ret_Emails);
	Ret_Emails.Dump();

}

//get's email and deletes from server:
void ServiceHandler::GetEmail(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,int nEmailID, DbRecordSet &Ret_EmailData)
{
	strEmail=strEmail.toLower();
#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	query.Execute(pStatus,"SELECT * FROM EMAILS WHERE BEM_ID = "+QString::number(nEmailID));
	if (!pStatus.IsOK())return; 

	query.FetchData(Ret_EmailData);
	if (Ret_EmailData.getRowCount()!=1)
	{
		pStatus.setError(1,"Email does not exist on the server any more!");
		return;
	}

	//BT: utf8 problem:
	//BT: 23.04.2013, utf8 problem, when use as is, umlauts are good!!!..skip fromutf8....
	QByteArray byte=Ret_EmailData.getDataRef(0,"BEM_BODY").toByteArray();
	//QString strBody=QString::fromUtf8(byte);
	Ret_EmailData.setData(0,"BEM_BODY",QString(byte));

	int nUserID=Ret_EmailData.getDataRef(0,"BEM_FK_USER_ID").toInt();


	//check if lock
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		//pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}


	query.BeginTransaction(pStatus);
	if (!pStatus.IsOK())return; 

		//delete email from server:
		query.Execute(pStatus,"DELETE FROM EMAILS WHERE BEM_ID = "+QString::number(nEmailID));
		if(!pStatus.IsOK()) 
		{
			query.Rollback();
			return;
		}

		QString strCntUpate="SELECT COUNT(*) FROM EMAILS WHERE BEM_FK_USER_ID = "+QString::number(nUserID); //just to be sure in total numbers...
		//get all users and emails
		QString strSql="UPDATE USER_DATA SET USR_EMAIL_IN_STORAGE  = ("+strCntUpate+") WHERE USR_ID="+QString::number(nUserID);
		query.Execute(pStatus,strSql);
		if(!pStatus.IsOK()) 
		{
			query.Rollback();
			return;
		}

	query.Commit(pStatus);



}

//check user credentials
//compare: strEmail+conce+SECRET_SPICE_PASSWORD+clientpass == strAuthToken
void ServiceHandler::AuthenticateUser(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken)
{
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql;
	int nAllowLogin=0;
	pStatus.setError(0);

	//read database version from table, if <> then execute organize database....
	query.Execute(pStatus,"SELECT USR_PASSWORD,USR_ALLOW_LOGIN FROM USER_DATA WHERE USR_EMAIL = '"+strEmail+"' AND USR_IS_CONFIRMED=1");
	if (!pStatus.IsOK())return; 

	QString strPass;
	if (query.next())
	{
		strPass=query.value(0).toString();
		nAllowLogin=query.value(1).toInt();
	}
	else
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}


	Sha256Hash Hasher;

	QString strAuthTokenServer=Hasher.GetHash(QString(strEmail+strConce+SECRET_SPICE_PASSWORD+strPass).toUtf8()).toHex();
	if (strAuthTokenServer == strAuthToken)
	{
		//now check allow/deny if deny then set error: expire subscription:
		if (nAllowLogin==0)
		{
			pStatus.setError(1003,"User has no permission to access system: subscription expired!");
			return;
		}
	}
	else
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);

}


//sends confirmation email
//Ret_bSkipLBNotify set true to skip LB notify, marked as RET parametar to skip problems with skeleton
void ServiceHandler::SendConfirmationEmail(Status &pStatus, QString strEmail,QString &Ret_strClientPassword, QString &Ret_strESHost, bool bSkipLBNotify, QString strReceipt)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::Step 1");

	//check if lock
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}

	strEmail=strEmail.toLower();
	Ret_strESHost="";
	
	//if registered, send mail, then wait for unregister click..
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::Step 2");
	
	Ret_strClientPassword = Authenticator::GenerateRandomPassword(20);
	QString strConfirmString = Authenticator::GenerateRandomPassword(100);

	//check if email already exists (no matter if registered or not)
	bool bAlreadyExists=false;

	int nESR_ServerID=-1;
	QDateTime datMRegTime;
	QString strPass="";
	int nIsConfirmed=0;
	query.Execute(pStatus,"SELECT USR_MULTI_REG_START, USR_PASSWORD,USR_IS_CONFIRMED,ESR_HOST_ADDRESS, ESR_ID FROM USER_DATA LEFT OUTER JOIN EMAIL_SERVERS ON ESR_ID=USR_EMAIL_SERVER_ID WHERE USR_EMAIL = '"+strEmail+"'");
	if (!pStatus.IsOK())return; 
	if (query.next())
	{
		datMRegTime=query.value(0).toDateTime();
		strPass=query.value(1).toString();
		nIsConfirmed=query.value(2).toInt();
		Ret_strESHost=query.value(3).toString();
		nESR_ServerID=query.value(4).toInt();
		bAlreadyExists=true;
	}


	//----------------------------------------------------------------------------------------------------------
	//	Check if timestamp is less then 1 hour
	//----------------------------------------------------------------------------------------------------------
	if (!datMRegTime.isNull() && nIsConfirmed)
	{
		if (datMRegTime.addSecs(3600)>QDateTime::currentDateTime()) //B.T. 12.04.2013, if reg timestamp + 1hr > current then period is still active
		{
			//multi reg active: just return existing password and do not send email:
			Ret_strClientPassword=strPass;
			return;
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::Begin");

	//----------------------------------------------------------------------------------------------------------
	//	Generate new email set new confirm string, date and new pass
	//----------------------------------------------------------------------------------------------------------

	if (bAlreadyExists)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::bAlreadyExists step 1");
		QString strSQL;
		if (nESR_ServerID>0)
			strSQL="UPDATE USER_DATA SET USR_CONFIRM_STRING='"+strConfirmString+"', USR_RECONFIRM_PASSWORD = '"+Ret_strClientPassword +"', USR_EMAIL_SERVER_ID="+QString::number(nESR_ServerID)+" WHERE USR_EMAIL = '"+strEmail+"'";
		else
			strSQL="UPDATE USER_DATA SET USR_CONFIRM_STRING='"+strConfirmString+"', USR_RECONFIRM_PASSWORD = '"+Ret_strClientPassword +"' WHERE USR_EMAIL = '"+strEmail+"'";

		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) 
		{
			return;
		}
	}
	else
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::bAlreadyExists=false step 1");

		DbRecordSet rowData;
		rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_USER_DATA));

		rowData.addRow();
		rowData.setData(0,"USR_EMAIL",strEmail);
		rowData.setData(0,"USR_PASSWORD",Ret_strClientPassword);
		rowData.setData(0,"USR_IS_CONFIRMED",0);
		rowData.setData(0,"USR_CONFIRM_STRING",strConfirmString);
		rowData.setData(0,"USR_EMAIL_IN_STORAGE",0);
		rowData.setData(0,"USR_EMAIL_TOTAL_DOWNLOADED",0);
		rowData.setData(0,"USR_DAT_LAST_LOGIN",QDateTime::currentDateTime());
		rowData.setData(0,"USR_DAT_FIRST_LOGIN",QDateTime::currentDateTime());
		rowData.setData(0,"USR_ALLOW_LOGIN",LOGIN_DENY); //set to deny until email is confirmed...or something else

		//new record created, check receipt string:
		//we have new strReceipt, check on server right away
		if (!strReceipt.isEmpty())
		{
			CheckAppleReceipt(pStatus,strReceipt);
			if (!pStatus.IsOK())
			{
				//log status and return as expired paid user:
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("User %1 tried to register with invalid apple receipt string. Apple server error was: %2!").arg(strEmail).arg(pStatus.getErrorText()));
				pStatus.setError(0);
				rowData.setData(0,"USR_DAT_LAST_RECEIPT_CHECK",QDateTime::currentDateTime());
				rowData.setData(0,"USR_APPLE_RECEIPT",strReceipt);
				rowData.setData(0,"USR_ALLOW_LOGIN",LOGIN_DENY);
			}
			else
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("User %1 registered with new valid apple receipt string!").arg(strEmail));
				//we have valid new receipt (first paid or renewable):
				rowData.setData(0,"USR_DAT_LAST_RECEIPT_CHECK",QDateTime::currentDateTime());
				rowData.setData(0,"USR_APPLE_RECEIPT",strReceipt);
				rowData.setData(0,"USR_ALLOW_LOGIN",LOGIN_ALLOW);
			}
		}
		
		//on new insert associate ES server: get one with lowest users:
		int nESRID = FindESHost(Ret_strESHost);
		if (nESRID>0)
			rowData.setData(0,"USR_EMAIL_SERVER_ID",nESRID);
		
		query.WriteData(pStatus,"USER_DATA",rowData);
		if(!pStatus.IsOK()) 
		{
			return;
		}
		
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::bAlreadyExists=false RegisterUserToESHost");

		if (!bSkipLBNotify)
		{
			RegisterEmailToLBServer(pStatus,strEmail);// B.T. keep LB database in sync!!!
			if(!pStatus.IsOK()) 
			{
				//query.Rollback();
				return;
			}
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::step send email");


	//generate email confirmation:
	QString strURL=g_AppServer->m_INIFile.m_strHTTPRegisterAddress+"/confirm/"+QUrl::toPercentEncoding(strConfirmString);
	//QString strSubject ="Registration Confirmation for 'Mailegant Reply'";
	//QString strBody = "To confirm your registration of your email address for 'Mailegant Reply', please click on this link: "+strURL;

	SendEmail(pStatus,strEmail,strURL);//,strBody);
	if(!pStatus.IsOK()) 
	{
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::step send email ok");

	//set datetime of send:
	QString strSql="UPDATE USER_DATA SET USR_CONFIRM_SENT=? WHERE USR_EMAIL = '"+strEmail+"'";
	query.Prepare(pStatus,strSql);
	if(!pStatus.IsOK()) 
	{
		return;
	}
	query.bindValue(0,QDateTime::currentDateTime());
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) 
	{
		return;
	}


	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"SendConfirmationEmail::done");

}

//MB: mail on 29.03.2013: use mime template for conf mail
void ServiceHandler::SendEmail(Status &err, QString strTO, QString strURL)
{
	SMTPClient client;

	QString strBody;
	DataHelper::LoadFile(QCoreApplication::applicationDirPath()+"/Confirmation_Mail_MIME.txt",strBody);

	strBody.replace("{receiver}",strTO);
	strBody.replace("{confirmation_address}",strURL);

	err.setError(0);
	QList<QString> lstBcc;
	QString strFrom=g_AppServer->m_INIFile.m_strMailServerAddress;
	if(!client.SendMail(g_AppServer->m_INIFile.m_strMailServer,strFrom,strTO,"",strBody,lstBcc,g_AppServer->m_INIFile.m_strMailServerUserName,g_AppServer->m_INIFile.m_strMailServerPassword,true,true,g_AppServer->m_INIFile.m_nMailServerPort))
	{
		err.setError(1,client.GetLastError());
	}
}

/*
//GBC function:
void ServiceHandler::CleanUpData(Status &pStatus)
{
	//check if lock
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
		return;

	//if registered, send mail, then wait for unregister click..
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//--------------------------------------------------------------------------------------
	//clean emails for each account older then specified by m_nTimeToKeepEmailsHr
	//--------------------------------------------------------------------------------------
	QString strTimeStampDeadLine=QString::number(g_AppServer->m_INIFile.m_nTimeToKeepEmailsHr*60);
	QString strSql="SELECT BEM_ID FROM EMAILS WHERE BEM_DAT_LAST_MODIFIED <(CURRENT_TIMESTAMP-"+strTimeStampDeadLine+"/1440.0)";
	query.Execute(pStatus,strSql);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstEmailsToDelete;
	query.FetchData(lstEmailsToDelete);

	strSql="DELETE FROM EMAILS WHERE BEM_ID=?";
	query.Prepare(pStatus,strSql);
	if(!pStatus.IsOK()) return;

	int nSize=lstEmailsToDelete.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		query.bindValue(0,lstEmailsToDelete.getDataRef(i,0).toInt());
		query.ExecutePrepared(pStatus);
		if(!pStatus.IsOK()) return;
	}


	//--------------------------------------------------------------------------------------
	//update emails count
	//--------------------------------------------------------------------------------------
	strSql="UPDATE USER_DATA SET USR_EMAIL_IN_STORAGE  = (SELECT COUNT(*) FROM EMAILS WHERE BEM_FK_USER_ID =USR_ID)";
	query.Execute(pStatus,strSql);
	if(!pStatus.IsOK()) return; 


	//--------------------------------------------------------------------------------------
	//registration timeout: delete all accounts that are not confirmed within m_nRegistrationTimeOutHr
	//--------------------------------------------------------------------------------------
	strTimeStampDeadLine=QString::number(g_AppServer->m_INIFile.m_nRegistrationTimeOutHr*60);
	strSql="SELECT USR_ID FROM USER_DATA WHERE USR_IS_CONFIRMED = 0 AND USR_CONFIRM_SENT <(CURRENT_TIMESTAMP-"+strTimeStampDeadLine+"/1440.0)";
	query.Execute(pStatus,strSql);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstUsersToDelete;
	query.FetchData(lstUsersToDelete);

	strSql="DELETE FROM USER_DATA WHERE USR_ID=?";
	query.Prepare(pStatus,strSql);
	if(!pStatus.IsOK()) return;

	nSize=lstUsersToDelete.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		query.bindValue(0,lstUsersToDelete.getDataRef(i,0).toInt());
		query.ExecutePrepared(pStatus);
		if(!pStatus.IsOK()) return;
	}

}
*/

//Write emails: based on TO field, filter by email addresses, check counts
//Ret_lstStatus <DEL>
void ServiceHandler::WriteEmails(Status &pStatus, DbRecordSet &lstEmails,DbRecordSet &Ret_lstStatus)
{
	//if registered, send mail, then wait for unregister click..
 	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QStringList lstUsersWithNewEmails;

	//prepare all lists:
	Ret_lstStatus.destroy();
	Ret_lstStatus.addColumn(QVariant::Int,"DEL");
	//add idx column in emails
	lstEmails.addColumn(QVariant::Int,"IDX");
	int nSizeEmails=lstEmails.getRowCount();
	for (int i=0;i<nSizeEmails;i++)
		lstEmails.setData(i,"IDX",i);
	//mark all emails to be left on pop3 server
	Ret_lstStatus.addRow(nSizeEmails);
	Ret_lstStatus.setColValue(0,0); 
	//sort emails by from fld and go on
	lstEmails.sort("BEM_FROM");
	
	//get all users and emails
	QString strSql="SELECT USR_ID,USR_EMAIL,USR_EMAIL_IN_STORAGE,USR_EMAIL_TOTAL_DOWNLOADED FROM USER_DATA WHERE USR_IS_CONFIRMED=1";
	query.Execute(pStatus,strSql);
	if(!pStatus.IsOK()) return;
	DbRecordSet lstUsers;
	query.FetchData(lstUsers);

	int nMaxEmailsPerUser	=	g_AppServer->m_INIFile.m_nMaxEmailCnt;
	int nSizeUsers			=	lstUsers.getRowCount();
	int nTotalImported		=	0;
	
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("Thread Import: Importing %1 emails for %2 users").arg(nSizeEmails).arg(nSizeUsers));

	//loop on emails by from fld search for user->find block of mails, if not found in users mark for deletion->
	//if found try to import if failed mark for keep else mark for deletion

	QString strFromPrev;
	QString strCurrentUserEmail;
	lstEmails.clearSelection();
	int nCurrUserIdx=-1;
	for (int i=0;i<nSizeEmails;i++)
	{
		//-------------------------------------------------------------------------------------
		//loop loop
		//-------------------------------------------------------------------------------------
		QString strFrom=lstEmails.getDataRef(i,"BEM_FROM").toString().toLower();
		if (strFromPrev.isEmpty())
		{
			strFromPrev=strFrom; //only on first loop...
			i--;
			continue;
		}
		if (strFrom==strFromPrev) //we are in one block of emails for one user, just continue
		{
			lstEmails.selectRow(i);
			if (i<(nSizeEmails-1))
				continue;
		}

		//-------------------------------------------------------------------------------------
		//we encountered new block, find user for block of emails:
		//-------------------------------------------------------------------------------------
		for (int k=0;k<nSizeUsers;k++)
		{
			if (strFromPrev.indexOf(lstUsers.getDataRef(k,"USR_EMAIL").toString())>=0)
			{
				strCurrentUserEmail=lstUsers.getDataRef(k,"USR_EMAIL").toString();
				nCurrUserIdx=k;
				break;
			}
		}


		//-------------------------------------------------------------------------------------
		//if current user is empty, mark for delete, and we found mails, mark for delete
		//-------------------------------------------------------------------------------------
		if (strCurrentUserEmail.isEmpty() && lstEmails.getSelectedCount()>0)
		{
			MarkForDeleteSelectedRows(lstEmails,Ret_lstStatus);
		}

		//-------------------------------------------------------------------------------------
		//save emails:
		//-------------------------------------------------------------------------------------
		if (!strCurrentUserEmail.isEmpty() && lstEmails.getSelectedCount()>0)
		{
			DbRecordSet lstUserEmails;
			lstUserEmails.copyDefinition(lstEmails);
			lstUserEmails.merge(lstEmails,true); //copy user emails

			int nSizeUserEmails=lstUserEmails.getRowCount();
			lstUserEmails.sort("BEM_RECV_TIME"); //get oldest, ignore newest!??

			//check count's:
			int nCount=lstUsers.getDataRef(nCurrUserIdx,"USR_EMAIL_IN_STORAGE").toInt();
			int nNewEmails = nMaxEmailsPerUser-nCount;

			while (nNewEmails>0)//we can accept more emails mark for deletion
			{
				int nUserID=lstUsers.getDataRef(nCurrUserIdx,"USR_ID").toInt();

				lstUserEmails.clearSelection();
				if (nNewEmails<nSizeUserEmails)
				{
					for (int k=nNewEmails;k<nSizeUserEmails;k++)
					{
						lstUserEmails.selectRow(k);
					}
					MarkForDeleteSelectedRows(lstUserEmails,Ret_lstStatus); //before delete, mark them for delete on pop3 server
					lstUserEmails.deleteSelectedRows();
					nSizeUserEmails=lstUserEmails.getRowCount();
				}
				nCount +=nSizeUserEmails;
				nTotalImported+=nSizeUserEmails;
				lstUserEmails.setColValue("BEM_FK_USER_ID",nUserID);

				//remove idx before write
				lstUserEmails.removeColumn(lstUserEmails.getColumnIdx("IDX"));
				query.WriteData(pStatus,"EMAILS",lstUserEmails);
				if(!pStatus.IsOK()) 
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_NONE,QString("Thread Import registered error %1 while importing emails for user %2 (write emails)").arg(pStatus.getErrorText()).arg(nUserID));
					break;
				}

				lstUsersWithNewEmails.append(strCurrentUserEmail);

				int nTotalCount=lstUsers.getDataRef(nCurrUserIdx,"USR_EMAIL_TOTAL_DOWNLOADED").toInt()+nSizeUserEmails;

				QString strCntUpate="SELECT COUNT(*) FROM EMAILS WHERE BEM_FK_USER_ID = "+QString::number(nUserID); //just to be sure in total numbers...
				//get all users and emails
				QString strSql="UPDATE USER_DATA SET USR_EMAIL_IN_STORAGE  = ("+strCntUpate+"),USR_EMAIL_TOTAL_DOWNLOADED ="+QString::number(nTotalCount)+" WHERE USR_ID="+QString::number(nUserID);
				query.Execute(pStatus,strSql);
				if(!pStatus.IsOK()) 
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_NONE,QString("Thread Import registered error %1 while importing emails for user %2 (update user data)").arg(pStatus.getErrorText()).arg(nUserID));
					break;
				}

				//if all ok mark for delete, else leave it as is
				MarkForDeleteSelectedRows(lstEmails,Ret_lstStatus);
				break; //while loop serves only for break...
			}

			//mark for deletion if email storage is full...
			if (nNewEmails==0)
			{
				MarkForDeleteSelectedRows(lstEmails,Ret_lstStatus);
			}
		}

		//-------------------------------------------------------------------------------------
		//reset vars for new block:
		//-------------------------------------------------------------------------------------
		strCurrentUserEmail="";
		nCurrUserIdx=-1;
		lstEmails.clearSelection();
		strFromPrev = strFrom;
		if (i<(nSizeEmails-1))
			i--;
	}

	//g_pEmailNotificator->ScheduleNewEmailNotification(lstUsersWithNewEmails);

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("Thread Import: Imported %1 emails").arg(nTotalImported));

}



void ServiceHandler::RegisterSMTPAccount(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken, QString strServerAddress, QString strUserName, QString strPassword, int nPort, QString strBCCAddressForSentMailCopy)
{
	//-------------------------------------
	//check if lock
	//-------------------------------------
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}

	strEmail=strEmail.toLower();

#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql="UPDATE USER_DATA SET USR_SMTP_SERVER=?, USR_SMTP_USERNAME = ?, USR_SMTP_PASSWORD = ?, USR_SMTP_PORT = ?, USR_SMTP_BCC_EMAIL_COPY = ? WHERE USR_EMAIL = '"+strEmail+"'";
	query.Prepare(pStatus,strSql);
	query.bindValue(0,strServerAddress);
	query.bindValue(1,strUserName);
	query.bindValue(2,strPassword);
	query.bindValue(3,nPort);
	query.bindValue(4,strBCCAddressForSentMailCopy);
	query.ExecutePrepared(pStatus);

}
void ServiceHandler::UnregisterSMTPAccount(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken)
{
	//-------------------------------------
	//check if lock
	//-------------------------------------
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}


	strEmail=strEmail.toLower();

#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql="UPDATE USER_DATA SET USR_SMTP_SERVER=NULL, USR_SMTP_USERNAME = NULL, USR_SMTP_PASSWORD = NULL, USR_SMTP_PORT = NULL, USR_SMTP_BCC_EMAIL_COPY = NULL WHERE USR_EMAIL = '"+strEmail+"'";
	query.Execute(pStatus,strSql);

}

//cc and bcc are semicolon delimited
void ServiceHandler::SendEMail(Status &pStatus,QString strEmail, QString strMimeFrom, QString strConce, QString strAuthToken,QString strTo,QString lstCC,QString lstBCC,QString strSubject,QString strMimeBody,int &Ret_ID)
{

	//-------------------------------------
	//check if lock
	//-------------------------------------
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}


	strEmail=strEmail.toLower();
	
	//new MB request:
	strTo=strTo.simplified().replace(" ","");
	strTo=strTo.replace(",",";");

	lstCC=lstCC.simplified().replace(" ","");
	lstCC=lstCC.replace(",",";");

	lstBCC=lstBCC.simplified().replace(" ","");
	lstBCC=lstBCC.replace(",",";");

#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;
	query.Execute(pStatus,"SELECT * FROM USER_DATA WHERE USR_EMAIL = '"+strEmail+"'");
	if(!pStatus.IsOK()) return;

	DbRecordSet rowUserData;
	query.FetchData(rowUserData);
	if (rowUserData.getRowCount()!=1)
	{
		pStatus.setError(1,"Error while loading user record for email address "+strEmail);
		return;
	}

	//new issue by MB:
	//test if SMTP is defined by user, if not use global, if not, then report error
	QString strFrom; //extract exact email from mime from
	strFrom=strMimeFrom.mid(strMimeFrom.indexOf("<")+1).replace(">","").trimmed();

	if (rowUserData.getDataRef(0,"USR_SMTP_SERVER").toString().isEmpty())
	{
		if (!g_AppServer->m_INIFile.m_strUserSMTPMailServer.isEmpty())
		{
			//use global, just temporarly fill values (will not be stored in DB)
			rowUserData.setData(0,"USR_SMTP_SERVER",g_AppServer->m_INIFile.m_strUserSMTPMailServer);
			rowUserData.setData(0,"USR_SMTP_PORT",g_AppServer->m_INIFile.m_nUserSMTPMailServerPort);
			rowUserData.setData(0,"USR_SMTP_USERNAME",g_AppServer->m_INIFile.m_strUserSMTPMailServerUserName);
			rowUserData.setData(0,"USR_SMTP_PASSWORD",g_AppServer->m_INIFile.m_strUserSMTPMailServerPassword);

			QString strSender = g_AppServer->m_INIFile.m_strUserSMTPMailServerAddress;
			strMimeFrom=strMimeFrom.replace("\"","");
			strMimeFrom=strMimeFrom.replace("<","");
			strMimeFrom=strMimeFrom.replace(">","");

			//MB: requested that if user does not specify own smtp that we use sendmailegant user as from...
			strMimeFrom="\""+strMimeFrom+"\" <"+strSender+">";
			strFrom = strSender;

			//parse mimefrom and use it as from fld
			strMimeBody.prepend("From: "+strMimeFrom+"\n"); //add from as first line
		}
		else
		{
			//report error
			pStatus.setError(1,"SMTP server data is not defined, email can not be sent!");
			return;
		}
	}
	else
	{
		strMimeBody.prepend("From: "+strMimeFrom+"\n"); //add from as first line
	}

	//remove from To all, add first to strTo, rest to recipients:
	QStringList strLstRecepients; 
	QStringList lstTo=strTo.split(";",QString::SkipEmptyParts);
	if (lstTo.size()>0)
	{
		strTo = lstTo.at(0);
		for (int i=1;i<lstTo.size();i++)
		{
			strLstRecepients.append(lstTo.at(i));
		}
	}

	//add all to recipients, cc, bcc are irrelevant
	strLstRecepients += lstCC.split(";",QString::SkipEmptyParts);
	strLstRecepients += lstBCC.split(";",QString::SkipEmptyParts);
	if (!rowUserData.getDataRef(0,"USR_SMTP_BCC_EMAIL_COPY").toString().isEmpty())
	{
		strLstRecepients.append(rowUserData.getDataRef(0,"USR_SMTP_BCC_EMAIL_COPY").toString());
	}
	
	int nPort=rowUserData.getDataRef(0,"USR_SMTP_PORT").toInt();
	if (nPort==0)
		nPort=25;


	int nESR_HostID=rowUserData.getDataRef(0,"USR_EMAIL_SERVER_ID").toInt();
	QString strUser=rowUserData.getDataRef(0,"USR_EMAIL").toInt();		

	Ret_ID = g_pEmailSenderManager->ScheduleSendMail(strEmail,rowUserData.getDataRef(0,"USR_SMTP_SERVER").toString(),strFrom,strTo,strSubject,strMimeBody,strLstRecepients,nESR_HostID,strUser, rowUserData.getDataRef(0,"USR_SMTP_USERNAME").toString(),rowUserData.getDataRef(0,"USR_SMTP_PASSWORD").toString(), true, true,nPort);
}

//Input: email ID's
//Output: email ID, status <NOT IN LIST, OK, FAILED, NOT SENT>
void ServiceHandler::CheckEmailStatus(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,DbRecordSet &lstEmailTasks, DbRecordSet &Ret_lstEmailTasksStatus)
{
	strEmail=strEmail.toLower();

#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;

	int nSize=lstEmailTasks.getRowCount();
	Ret_lstEmailTasksStatus.addColumn(QVariant::Int,"ID");
	Ret_lstEmailTasksStatus.addColumn(QVariant::String,"Status");
	Ret_lstEmailTasksStatus.addRow(nSize);

	for (int i=0;i<nSize;i++)
	{
		int nID=lstEmailTasks.getDataRef(i,0).toInt();
		QString strStatus;
		Ret_lstEmailTasksStatus.setData(i,"ID",nID);
		if(g_pEmailSenderManager->GetStatusForMail(nID,strStatus))
		{
			Ret_lstEmailTasksStatus.setData(i,"Status",strStatus);
		}
		else
		{
			Ret_lstEmailTasksStatus.setData(i,"Status","NOT IN LIST");
		}
	}
}

void ServiceHandler::GetStats(Status &pStatus, QString strConce, QString strAuthToken, int &Ret_nRegisteredUsers, int &Ret_nRegisteredUsersInLast7days, int &Ret_nUsersPendingConfirmation, int &Ret_nPaid,int &Ret_nPaidExpired, int &Ret_nTest,int &Ret_nTestExpired)
{

#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	Ret_nRegisteredUsers=0;
	Ret_nRegisteredUsersInLast7days=0;
	Ret_nUsersPendingConfirmation=0;
	Ret_nPaid=0;
	Ret_nPaidExpired=0;
	Ret_nTest=0;
	Ret_nTestExpired=0;
	
	
	QString strSql;
	//get registered:
	query.Execute(pStatus,"SELECT COUNT(*) FROM USER_DATA WHERE USR_IS_CONFIRMED =1");
	if (!pStatus.IsOK())return; 
	if (query.next())
		Ret_nRegisteredUsers=query.value(0).toInt();

	//get pending:
	query.Execute(pStatus,"SELECT COUNT(*) FROM USER_DATA WHERE USR_IS_CONFIRMED =0");
	if (!pStatus.IsOK())return; 
	if (query.next())
		Ret_nUsersPendingConfirmation=query.value(0).toInt();

	//get users in last 7 days:
	QDateTime datLimit=QDateTime::currentDateTime().addDays(-7);
	strSql="SELECT COUNT(*) FROM USER_DATA WHERE USR_IS_CONFIRMED = 1 AND USR_DAT_FIRST_LOGIN >=?";
	query.Prepare(pStatus,strSql);
	if (!pStatus.IsOK())return; 
	query.bindValue(0,datLimit);
	query.ExecutePrepared(pStatus);
	if (!pStatus.IsOK())return; 
	if (query.next())
		Ret_nRegisteredUsersInLast7days=query.value(0).toInt();


	//get paid subscriptions
	query.Execute(pStatus,"SELECT COUNT(*) FROM USER_DATA WHERE USR_IS_CONFIRMED =1 AND USR_ALLOW_LOGIN=1 AND CHAR_LENGTH(USR_APPLE_RECEIPT)>0");
	if (!pStatus.IsOK())return; 
	if (query.next())
		Ret_nPaid=query.value(0).toInt();

	//get expired subscriptions
	query.Execute(pStatus,"SELECT COUNT(*) FROM USER_DATA WHERE USR_IS_CONFIRMED =1 AND USR_ALLOW_LOGIN=0 AND CHAR_LENGTH(USR_APPLE_RECEIPT)>0");
	if (!pStatus.IsOK())return; 
	if (query.next())
		Ret_nPaidExpired=query.value(0).toInt();

	//get test subscriptions
	query.Execute(pStatus,"SELECT COUNT(*) FROM USER_DATA WHERE USR_IS_CONFIRMED =1 AND (USR_ALLOW_LOGIN=1 OR USR_ALLOW_LOGIN=2) AND (CHAR_LENGTH(USR_APPLE_RECEIPT)=0 OR USR_APPLE_RECEIPT IS NULL)");
	if (!pStatus.IsOK())return; 
	if (query.next())
		Ret_nTest=query.value(0).toInt();

	//get expired subscriptions
	query.Execute(pStatus,"SELECT COUNT(*) FROM USER_DATA WHERE USR_IS_CONFIRMED =1 AND USR_ALLOW_LOGIN=0 AND (CHAR_LENGTH(USR_APPLE_RECEIPT)=0 OR USR_APPLE_RECEIPT IS NULL)");
	if (!pStatus.IsOK())return; 
	if (query.next())
		Ret_nTestExpired=query.value(0).toInt();



}
//nSortOrder=-1 ->by date (latest if first), 0 by alphabet, 2 - nSortOrder by status
//->not only registered users..but all...distnict by status
void ServiceHandler::GetUsers(Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstRegisteredEmails, int &Ret_nTotalRegisteredUsers, int nSortOrder, int nFromN, int nToN)
{

#if TURN_OFF_AUTHENTICATION !=1
	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
#endif
	

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstUsers;
	query.Execute(pStatus,"SELECT USR_ID,USR_EMAIL, USR_IS_CONFIRMED,USR_RECONFIRM_PASSWORD,USR_DAT_FIRST_LOGIN, USR_DAT_LAST_LOGIN, USR_DAT_LAST_RECEIPT_CHECK, CHAR_LENGTH(USR_APPLE_RECEIPT) AS APPLE_RECEIPT_LENGTH,USR_ALLOW_LOGIN, ESR_HOST_ADDRESS,USR_EMAIL_SERVER_ID FROM USER_DATA LEFT OUTER JOIN EMAIL_SERVERS ON ESR_ID=USR_EMAIL_SERVER_ID");
	if (!pStatus.IsOK())return; 
	query.FetchData(lstUsers);

	int nSize=lstUsers.getRowCount();
	Ret_lstRegisteredEmails = lstUsers;
	Ret_lstRegisteredEmails.addColumn(QVariant::Int,"STATUS");
	Ret_lstRegisteredEmails.addColumn(QVariant::String,"USR_DAT_FIRST_LOGIN_STR");
	Ret_lstRegisteredEmails.addColumn(QVariant::String,"USR_DAT_LAST_LOGIN_STR");
	Ret_lstRegisteredEmails.addColumn(QVariant::String,"USR_DAT_LAST_RECEIPT_CHECK_STR");
	Ret_lstRegisteredEmails.addColumn(QVariant::Int,"DAYS_LEFT");
	Ret_lstRegisteredEmails.addColumn(QVariant::Int,"SUBSCRIPTION_STATUS");
	Ret_lstRegisteredEmails.addColumn(QVariant::String,"SUBSCRIPTION_STATUS_STR");
	
	 /*
		0 - Prepared (imported, but not yet registered), 
		1 - Registered (the user has registered, but not yet confirmed), 
		2 - Re-Registerd (the user has re-registered, but not yet confirmed),
		3 - Confirmed (valid and active)
	 */


	for (int i=0;i<nSize;i++)
	{
		//reconvert dates to dd.MM.yyy-HH:mm
		Ret_lstRegisteredEmails.setData(i,"USR_DAT_FIRST_LOGIN_STR",Ret_lstRegisteredEmails.getDataRef(i,"USR_DAT_FIRST_LOGIN").toDateTime().toString("dd.MM.yyyy.HH:mm"));
		Ret_lstRegisteredEmails.setData(i,"USR_DAT_LAST_LOGIN_STR",Ret_lstRegisteredEmails.getDataRef(i,"USR_DAT_LAST_LOGIN").toDateTime().toString("dd.MM.yyyy.HH:mm"));
		Ret_lstRegisteredEmails.setData(i,"USR_DAT_LAST_RECEIPT_CHECK_STR",Ret_lstRegisteredEmails.getDataRef(i,"USR_DAT_LAST_RECEIPT_CHECK").toDateTime().toString("dd.MM.yyyy.HH:mm"));
		
		if (Ret_lstRegisteredEmails.getDataRef(i,"USR_ALLOW_LOGIN").toInt()==LOGIN_ALWAYS_ALLOW)
		{
			int nTrialPeriodDays=g_AppServer->m_INIFile.m_nNumCloudTestingPeriodDays;
			Ret_lstRegisteredEmails.setData(i,"DAYS_LEFT",nTrialPeriodDays);
			Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS",SUBSCRIPTION_STATUS_TEST);
			Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS_STR","Test permanent");
		}
		else if (Ret_lstRegisteredEmails.getDataRef(i,"APPLE_RECEIPT_LENGTH").toInt()>0)
		{
			if (Ret_lstRegisteredEmails.getDataRef(i,"USR_ALLOW_LOGIN").toInt()==0)
			{
				int nExpirePeriodDays=g_AppServer->m_INIFile.m_nExpireEmailAccountDays;
				int nDaysLeft=nExpirePeriodDays - Ret_lstRegisteredEmails.getDataRef(i,"USR_DAT_LAST_LOGIN").toDateTime().daysTo(QDateTime::currentDateTime());
				Ret_lstRegisteredEmails.setData(i,"DAYS_LEFT",nDaysLeft);

				Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS",SUBSCRIPTION_STATUS_PAID_EXPIRED);
				Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS_STR","Paid expired");
			}
			else
			{
				int nDaysLeft=Ret_lstRegisteredEmails.getDataRef(i,"USR_DAT_FIRST_LOGIN").toDateTime().daysTo(QDateTime::currentDateTime());
				Ret_lstRegisteredEmails.setData(i,"DAYS_LEFT",nDaysLeft);

				Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS",SUBSCRIPTION_STATUS_PAID);
				Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS_STR","Paid");
			}
		}
		else
		{

			if (Ret_lstRegisteredEmails.getDataRef(i,"USR_ALLOW_LOGIN").toInt()==0)
			{
				int nExpirePeriodDays=g_AppServer->m_INIFile.m_nExpireEmailAccountDays;
				int nDaysLeft=nExpirePeriodDays - Ret_lstRegisteredEmails.getDataRef(i,"USR_DAT_LAST_LOGIN").toDateTime().daysTo(QDateTime::currentDateTime());
				Ret_lstRegisteredEmails.setData(i,"DAYS_LEFT",nDaysLeft);

				Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS",SUBSCRIPTION_STATUS_TEST_EXPIRED);
				Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS_STR","Test expired");
			}
			else
			{
				int nTrialPeriodDays=g_AppServer->m_INIFile.m_nNumCloudTestingPeriodDays;
				int nDaysLeft=nTrialPeriodDays - Ret_lstRegisteredEmails.getDataRef(i,"USR_DAT_FIRST_LOGIN").toDateTime().daysTo(QDateTime::currentDateTime());
				Ret_lstRegisteredEmails.setData(i,"DAYS_LEFT",nDaysLeft);

				Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS",SUBSCRIPTION_STATUS_TEST);
				Ret_lstRegisteredEmails.setData(i,"SUBSCRIPTION_STATUS_STR","Test");
			}
		}


		int nStatus=1;

		if (Ret_lstRegisteredEmails.getDataRef(i,"USR_IS_CONFIRMED").toInt()==0)
		{
			nStatus=1;
		}
		else
		{
			if (!Ret_lstRegisteredEmails.getDataRef(i,"USR_RECONFIRM_PASSWORD").toString().isEmpty())
				nStatus=2;
			else
				nStatus=3;
		}
		Ret_lstRegisteredEmails.setData(i,"STATUS",nStatus);
	}

	Ret_lstRegisteredEmails.removeColumn(Ret_lstRegisteredEmails.getColumnIdx("USR_DAT_FIRST_LOGIN"));
	Ret_lstRegisteredEmails.removeColumn(Ret_lstRegisteredEmails.getColumnIdx("USR_DAT_LAST_LOGIN"));
	Ret_lstRegisteredEmails.removeColumn(Ret_lstRegisteredEmails.getColumnIdx("USR_DAT_LAST_RECEIPT_CHECK"));
	Ret_lstRegisteredEmails.removeColumn(Ret_lstRegisteredEmails.getColumnIdx("APPLE_RECEIPT_LENGTH"));


	Ret_nTotalRegisteredUsers=Ret_lstRegisteredEmails.getRowCount();

	if (nSortOrder==0)
	{
		Ret_lstRegisteredEmails.sort("USR_EMAIL");
	}
	else if (nSortOrder==1)
	{
		Ret_lstRegisteredEmails.sort("USR_DAT_LAST_MODIFIED",1);
	}
	else
	{
		SortDataList lst;
		lst<<SortData(Ret_lstRegisteredEmails.getColumnIdx("STATUS"),0);
		lst<<SortData(Ret_lstRegisteredEmails.getColumnIdx("USR_EMAIL"),0);
		Ret_lstRegisteredEmails.sortMulti(lst);
	}


	DataHelper::GetPartOfRecordSet(Ret_lstRegisteredEmails,nFromN,nToN);

	

	//----------------------------------------------------------------------------------------------
	//MB: requested: go on each ES server and load email cnt, then assign to this list and show it:
	//----------------------------------------------------------------------------------------------

	Ret_lstRegisteredEmails.addColumn(QVariant::Int,"ES_EMAIL_CNT");
	Ret_lstRegisteredEmails.setColValue("ES_EMAIL_CNT",0);


	query.Execute(pStatus,"SELECT ESR_ID, ESR_HOST_ADDRESS FROM EMAIL_SERVERS");
	if(!pStatus.IsOK()) return;
	
	DbRecordSet lstESHosts;
	query.FetchData(lstESHosts);

	DbRecordSet lstEmailCnt;
	lstEmailCnt.addColumn(QVariant::Int,"ESR_ID");   //es host
	lstEmailCnt.addColumn(QVariant::String,"EMAIL");
	lstEmailCnt.addColumn(QVariant::Int,"CNT");

	nSize=lstESHosts.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet lstEmailCntESHost;
		ReadEmailCntFromToESHost(pStatus,lstESHosts.getDataRef(i,"ESR_ID").toInt(),lstEmailCntESHost);
		if(!pStatus.IsOK()) return;

		lstEmailCntESHost.addColumn(QVariant::Int,"ESR_ID");
		lstEmailCntESHost.setColValue("ESR_ID",lstESHosts.getDataRef(i,"ESR_ID").toInt());

		lstEmailCnt.merge(lstEmailCntESHost);
	}

	//now go to user list and assign counts:
	nSize=Ret_lstRegisteredEmails.getRowCount();
	int nSize2=lstEmailCnt.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strEmail=Ret_lstRegisteredEmails.getDataRef(i,"USR_EMAIL").toString();
		int nESHostID=Ret_lstRegisteredEmails.getDataRef(i,"USR_EMAIL_SERVER_ID").toInt();

		for (int k=0;k<nSize2;k++)
		{
			if (lstEmailCnt.getDataRef(k,"EMAIL").toString()==strEmail && lstEmailCnt.getDataRef(k,"ESR_ID").toInt()==nESHostID)
			{
				Ret_lstRegisteredEmails.setData(i,"ES_EMAIL_CNT",lstEmailCnt.getDataRef(k,"CNT").toInt());
			}
		}
	}


}

//MB: mail from 20.05.2013: only allow registration from these emails:
// format is email; N\n\r
void ServiceHandler::WriteAccessEmails(Status &pStatus, QString strConce, QString strAuthToken, QString strEmails)
{
#if TURN_OFF_AUTHENTICATION !=1
	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstData;
	lstData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_USER_ACCESS));
	QStringList lstEmails = strEmails.split("\n");
	int nSize=lstEmails.size();
	for (int i=0;i<nSize;i++)
	{
		QString strEmail;
		int nType=0;
		int nPos=lstEmails.at(i).indexOf(";");
		if (nPos>0)
		{
			strEmail=lstEmails.at(i).left(nPos).trimmed().toLower();
			nType=lstEmails.at(i).mid(nPos+1).trimmed().toInt();
		}
		else
		{
			strEmail=lstEmails.at(i).left(nPos).trimmed().toLower();
		}

		lstData.addRow();
		lstData.setData(i,"USA_EMAIL",strEmail);
		lstData.setData(i,"USA_TYPE",nType);

		QString strSQL="SELECT USA_ID FROM USER_ACCESS WHERE USA_EMAIL='"+strEmail+"'";
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) return;
		int nCnt=0;
		if(query.next())
		{
			lstData.setData(i,"USA_ID",query.value(0).toInt());
		}

	}



	//check if email already exist, if so then update:



	query.WriteData(pStatus,"USER_ACCESS",lstData);
}


//MB: mail from 20.05.2013: only allow registration from these emails: delete email one by one: from access table and from main table...
void ServiceHandler::DeleteAccessEmail(Status &pStatus, QString strConce, QString strAuthToken, QString strEmail)
{
#if TURN_OFF_AUTHENTICATION !=1
	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
#endif

	strEmail=strEmail.toLower();

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK()) return;

	QString strSQL="DELETE FROM USER_ACCESS WHERE USA_EMAIL ='"+strEmail+"'";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}

	strSQL="DELETE FROM USER_DATA WHERE USR_EMAIL ='"+strEmail+"'";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}

	query.Commit(pStatus);
	
}

void ServiceHandler::SetUserSubscriptionAccess( Status &pStatus, QString strConce, QString strAuthToken, QString strEmail, int nLoginLevel )
{

#if TURN_OFF_AUTHENTICATION !=1
	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
#endif

	strEmail=strEmail.toLower();

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSQL="UPDATE USER_DATA SET USR_ALLOW_LOGIN="+QString::number(nLoginLevel)+" WHERE USR_EMAIL ='"+strEmail+"'";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) 
	{
		return;
	}

}

void ServiceHandler::DeleteUser( Status &pStatus, QString strConce, QString strAuthToken, QString strEmail )
{
#if TURN_OFF_AUTHENTICATION !=1
	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
#endif

	strEmail=strEmail.toLower();
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//get ESR host id:
	query.Execute(pStatus,"SELECT USR_EMAIL_SERVER_ID FROM USER_DATA WHERE USR_EMAIL ='"+strEmail+"'");
	if(!pStatus.IsOK()) return;
	int nEsrHostID=0;
	if (query.next())
	{
		nEsrHostID=query.value(0).toInt();
	}

	QString strSQL="DELETE FROM USER_DATA WHERE USR_EMAIL ='"+strEmail+"'";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) 
	{
		return;
	}
	
	if (nEsrHostID>0)
	{
		//now delete from ES host:
		DeleteUserOnESHost(pStatus,nEsrHostID,strEmail);
	}
}



//helper funct
void ServiceHandler::MarkForDeleteSelectedRows(DbRecordSet &lstEmails, DbRecordSet &Ret_lstStatus)
{
	DbRecordSet lstIdx;
	lstIdx.addColumn(QVariant::Int,"IDX");
	lstIdx.merge(lstEmails,true);
	int nSizeIdx=lstIdx.getRowCount();
	for (int z=0;z<nSizeIdx;z++)
	{
		Ret_lstStatus.setData(lstIdx.getDataRef(z,0).toInt(),0,1); //delete from server
	}

}



void ServiceHandler::StartMultiRegisterPeriod(Status &pStatus,QString strEmail, QString strConce, QString strAuthToken)
{
	//-------------------------------------
	//check if lock
	//-------------------------------------
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}


	strEmail=strEmail.toLower();

#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;


	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql="UPDATE USER_DATA SET USR_MULTI_REG_START=CURRENT_TIMESTAMP WHERE USR_EMAIL = '"+strEmail+"'";
	query.Execute(pStatus,strSql);
}




void ServiceHandler::ReadDataList(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, DbRecordSet &Ret_List)
{
#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	query.Execute(pStatus,"SELECT * FROM USER_DATA WHERE USR_EMAIL = '"+strEmail+"'");
	if(!pStatus.IsOK()) return;
	DbRecordSet rowUserData;
	query.FetchData(rowUserData);
	if (rowUserData.getRowCount()!=1)
	{
		pStatus.setError(1,"Error while loading user record for email address "+strEmail);
		return;
	}

	QString strSQL="SELECT CT_TYPE,CT_NAME,CT_DAT_LAST_MODIFIED FROM CLOUD_TEMPLATES WHERE CT_FK_USER_ID ="+rowUserData.getDataRef(0,"USR_ID").toString()+" AND CT_TYPE="+QString::number(nType);
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	query.FetchData(Ret_List);
}

void ServiceHandler::GetData(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, QString strName, QString &Ret_strData)
{
#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	query.Execute(pStatus,"SELECT * FROM USER_DATA WHERE USR_EMAIL = '"+strEmail+"'");
	if(!pStatus.IsOK()) return;
	DbRecordSet rowUserData;
	query.FetchData(rowUserData);
	if (rowUserData.getRowCount()!=1)
	{
		pStatus.setError(1,"Error while loading user record for email address "+strEmail);
		return;
	}

	QString strSQL="SELECT CT_DATA FROM CLOUD_TEMPLATES WHERE CT_FK_USER_ID ="+rowUserData.getDataRef(0,"USR_ID").toString()+" AND CT_TYPE="+QString::number(nType);
	strSQL+=" AND CT_NAME =?";
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	query.bindValue(0,strName);

	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstData;
	query.FetchData(lstData);

	//BT: utf8 problem:
	if (lstData.getRowCount()>0)
		Ret_strData=lstData.getDataRef(0,"CT_DATA").toString();
	else
		Ret_strData="";
	//Ret_strData=QString::fromUtf8(lstData.getDataRef(0,"CT_DATA").toByteArray());

}

void ServiceHandler::StoreData(Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, int nType, QString strName, QString strData)
{
	//-------------------------------------
	//check if lock
	//-------------------------------------
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}


#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	query.Execute(pStatus,"SELECT * FROM USER_DATA WHERE USR_EMAIL = '"+strEmail+"'");
	if(!pStatus.IsOK()) return;
	DbRecordSet rowUserData;
	query.FetchData(rowUserData);
	if (rowUserData.getRowCount()!=1)
	{
		pStatus.setError(1,"Error while loading user record for email address "+strEmail);
		return;
	}

	/*
		- If an item of the same name and type already exists: Replace without asking.
		- If an item of the same name and type already exosts AND strXMLData is empty: Delete item without asking.
	*/

	QString strSQL="SELECT * FROM CLOUD_TEMPLATES WHERE CT_FK_USER_ID ="+rowUserData.getDataRef(0,"USR_ID").toString()+" AND CT_TYPE="+QString::number(nType);
	strSQL+=" AND CT_NAME =?";
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	query.bindValue(0,strName);

	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstData;
	query.FetchData(lstData);
	if (lstData.getRowCount()==0)
	{
		lstData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_CLOUD_TEMPLATES));
		lstData.addRow();
	}

	if (!strData.isEmpty())
	{
		lstData.setData(0,"CT_FK_USER_ID",rowUserData.getDataRef(0,"USR_ID"));
		lstData.setData(0,"CT_TYPE",nType);
		lstData.setData(0,"CT_NAME",strName);
		lstData.setData(0,"CT_DATA",strData);
		lstData.setData(0,"CT_DAT_LAST_MODIFIED",QDateTime::currentDateTime());
		query.WriteData(pStatus,"CLOUD_TEMPLATES",lstData);
	}
	else
	{	//delete if id>0
		int nID=lstData.getDataRef(0,"CT_ID").toInt();
		strSQL="DELETE FROM CLOUD_TEMPLATES WHERE CT_ID="+QString::number(nID);
		query.Execute(pStatus,strSQL);
	}
}

void ServiceHandler::AddESHost( Status &pStatus, QString strConce, QString strAuthToken, QString strESHost )
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::AddESHost start");

#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	DbRecordSet rowData;
	rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_EMAIL_SERVERS));

	rowData.addRow();
	rowData.setData(0,"ESR_HOST_ADDRESS",strESHost);

	query.WriteData(pStatus,"EMAIL_SERVERS",rowData);

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::AddESHost end");

}

void ServiceHandler::DeleteESHost( Status &pStatus, QString strConce, QString strAuthToken, QString strESHost )
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::DeleteESHost start");

#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	DbRecordSet rowData;
	rowData.defineFromView(DbTableDef::getView(DbTableDef::VIEW_EMAIL_SERVERS));

	query.Execute(pStatus,"DELETE FROM EMAIL_SERVERS WHERE ESR_HOST_ADDRESS='"+strESHost+"'");

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::DeleteESHost end");
}

void ServiceHandler::GetESHosts( Status &pStatus, QString strConce, QString strAuthToken, DbRecordSet &Ret_lstESHosts )
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::GetESHosts start");
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;


	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS, COUNT(USR_EMAIL_SERVER_ID) CNT FROM EMAIL_SERVERS LEFT OUTER JOIN USER_DATA ON ESR_ID=USR_EMAIL_SERVER_ID GROUP BY ESR_HOST_ADDRESS");
	if(!pStatus.IsOK()) return;

	query.FetchData(Ret_lstESHosts);

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::GetESHosts end");
	
}

int ServiceHandler::FindESHost(QString &strHostIP)
{
	DbRecordSet lstTemp;
	Status err;
	DbSqlQuery query(err, g_DbManager);
	if(!err.IsOK()) return -1;
	
	query.Execute(err,"SELECT ESR_ID, ESR_HOST_ADDRESS, COUNT(USR_EMAIL_SERVER_ID) CNT FROM EMAIL_SERVERS LEFT OUTER JOIN USER_DATA ON ESR_ID=USR_EMAIL_SERVER_ID GROUP BY ESR_ID,ESR_HOST_ADDRESS");
	if(!err.IsOK()) return -1;

	query.FetchData(lstTemp);

	lstTemp.sort(2);//sort by cnt:

	if (lstTemp.getRowCount()>0)
	{
		strHostIP=lstTemp.getDataRef(0,"ESR_HOST_ADDRESS").toString();
		return lstTemp.getDataRef(0,"ESR_ID").toInt();
	}

	return -1;
}

//using what account!?
void ServiceHandler::RegisterUserToESHost( Status &pStatus, int nESRID, QString strUser, QString strPass )
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::RegisterUserToESHost start");

	//make REST call to the ES server, register user

	//get HOST, extract protocol, port and IP address:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strESHost;
	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS FROM EMAIL_SERVERS WHERE ESR_ID="+QString::number(nESRID));
	if(!pStatus.IsOK()) return;

	if (query.next())
	{
		strESHost=query.value(0).toString();
	}
	if (strESHost.isEmpty())
		return;

	QUrl url(strESHost);

	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();

	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::RegisterUserToESHost connecting to %1, %2, %3").arg(strIP).arg(nPort).arg(nIsSSL));


	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::RegisterUserToESHost connect done"));
	

	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,SERVER_INTERSERVER_COMM_USERNAME,SERVER_INTERSERVER_COMM_PASSWORD,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::RegisterUserToESHost session ok"));

	QString strLastName,strFirstName;
	strFirstName="";
	strLastName=strUser;

	//SEND:
	client.msg_send.AddParameter(&strUser,"strUser");
	client.msg_send.AddParameter(&strPass,"strPass");
	client.msg_send.AddParameter(&strFirstName,"strFirstName");
	client.msg_send.AddParameter(&strLastName,"strLastName");

	client.RestSend(pStatus,"POST","/rest/mwcloudservice/createuser",strSession);
	if(!pStatus.IsOK()) return;

	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);
	if(!pStatus.IsOK()) return;


	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::RegisterUserToESHost end");
}



void ServiceHandler::ChangePassUserToESHost( Status &pStatus, int nESRID, QString strUser, QString strPass )
{

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::ChangePassUserToESHost start");

	//make REST call to the ES server, change user pass

	//make REST call to the ES server, register user

	//get HOST, extract protocol, port and IP address:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strESHost;
	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS FROM EMAIL_SERVERS WHERE ESR_ID="+QString::number(nESRID));
	if(!pStatus.IsOK()) return;

	if (query.next())
	{
		strESHost=query.value(0).toString();
	}
	if (strESHost.isEmpty())
		return;


	QUrl url(strESHost);

	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();

	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;


	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost connecting to %1, %2, %3").arg(strIP).arg(nPort).arg(nIsSSL));

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost connect done"));
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost start session"));

	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,SERVER_INTERSERVER_COMM_USERNAME,SERVER_INTERSERVER_COMM_PASSWORD,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost session ok"));

	QString strLastName,strFirstName;
	strFirstName="";
	strLastName=strUser;

	//SEND:
	client.msg_send.AddParameter(&strUser,"strUser");
	client.msg_send.AddParameter(&strPass,"strPass");

	client.RestSend(pStatus,"POST","/rest/mwcloudservice/changeuserpassword",strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost api ok"));

	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ChangePassUserToESHost end session"));

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::ChangePassUserToESHost end");

}

//select all users with host=empty then assign by 10 to first server then to another etc...

void ServiceHandler::RegisterESHost2Users( Status &pStatus, QString strConce, QString strAuthToken )
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::RegisterESHost2Users start");
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstUsers;
	query.Execute(pStatus,"SELECT USR_ID, USR_EMAIL, USR_PASSWORD FROM USER_DATA WHERE USR_EMAIL_SERVER_ID IS NULL");
	if (!pStatus.IsOK())return; 
	query.FetchData(lstUsers);

	query.BeginTransaction(pStatus);
	if (!pStatus.IsOK())return; 

	QString strESHost;
	int nESHost=FindESHost(strESHost);
	int nSize=lstUsers.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (i%10==0)
		{
			nESHost = FindESHost(strESHost);
		}

		if (nESHost>0)
		{
			query.Execute(pStatus,"UPDATE USER_DATA SET USR_EMAIL_SERVER_ID = "+QString::number(nESHost)+" WHERE USR_ID="+lstUsers.getDataRef(i,0).toString());
			if(!pStatus.IsOK()) 
			{
				query.Rollback();
				return;
			}

			QString strEmail=lstUsers.getDataRef(i,"USR_EMAIL").toString();
			QString strPass=lstUsers.getDataRef(i,"USR_PASSWORD").toString();

			//register user to that server:
			RegisterUserToESHost(pStatus,nESHost,strEmail,strPass);
			if(!pStatus.IsOK()) 
			{
				query.Rollback();
				return;
			}
		}
	}

	query.Commit(pStatus);

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::RegisterESHost2Users end");
	
}

void ServiceHandler::RegisterESHost2SingleUser( Status &pStatus, QString strConce, QString strAuthToken, QString strEmail )
{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::RegisterESHost2SingleUser start");

#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstUsers;
	query.Execute(pStatus,"SELECT USR_ID, USR_EMAIL, USR_PASSWORD FROM USER_DATA WHERE USR_EMAIL='"+strEmail+"'");
	if (!pStatus.IsOK())return; 
	query.FetchData(lstUsers);

	query.BeginTransaction(pStatus);
	if (!pStatus.IsOK())return; 

	QString strESHost;
	int nESHost=FindESHost(strESHost);
	int nSize=lstUsers.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (i%10==0)
		{
			nESHost = FindESHost(strESHost);
		}

		if (nESHost>0)
		{
			query.Execute(pStatus,"UPDATE USER_DATA SET USR_EMAIL_SERVER_ID = "+QString::number(nESHost)+" WHERE USR_ID="+lstUsers.getDataRef(i,0).toString());
			if(!pStatus.IsOK()) 
			{
				query.Rollback();
				return;
			}

			QString strEmail=lstUsers.getDataRef(i,"USR_EMAIL").toString();
			QString strPass=lstUsers.getDataRef(i,"USR_PASSWORD").toString();

			//register user to that server:
			RegisterUserToESHost(pStatus,nESHost,strEmail,strPass);
			if(!pStatus.IsOK()) 
			{
				query.Rollback();
				return;
			}
		}
	}

	query.Commit(pStatus);

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::RegisterESHost2SingleUser end");


}

void ServiceHandler::ReadEmailCntFromToESHost( Status &pStatus, int nESRID, DbRecordSet &Ret_lstData)
{

	//get HOST, extract protocol, port and IP address:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strESHost;
	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS FROM EMAIL_SERVERS WHERE ESR_ID="+QString::number(nESRID));
	if(!pStatus.IsOK()) return;

	if (query.next())
	{
		strESHost=query.value(0).toString();
	}
	if (strESHost.isEmpty())
		return;


	QUrl url(strESHost);

	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();

	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ReadEmailCntFromToESHost connecting to %1, %2, %3").arg(strIP).arg(nPort).arg(nIsSSL));

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ReadEmailCntFromToESHost connect done, creating session"));


	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,SERVER_INTERSERVER_COMM_USERNAME,SERVER_INTERSERVER_COMM_PASSWORD,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::ReadEmailCntFromToESHost session create done, execute API"));

	client.RestSend(pStatus,"GET","/rest/mwcloudservice/getusersemailcnt",strSession);
	if(!pStatus.IsOK()) return;

	Ret_lstData.destroy();
	Ret_lstData.addColumn(QVariant::String,"EMAIL");
	Ret_lstData.addColumn(QVariant::Int,"CNT");

	client.msg_recv.GetParameter(0,&Ret_lstData,pStatus);
	if (!pStatus.IsOK())
		return;


	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);
	if(!pStatus.IsOK()) return;


}

void ServiceHandler::CleanUpOldAccounts( Status &pStatus )
{
	//check if lock
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
		return;

	//if registered, send mail, then wait for unregister click..
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//--------------------------------------------------------------------------------------
	//get old accounts 
	//--------------------------------------------------------------------------------------
	QString strTimeStampDeadLine=QString::number(g_AppServer->m_INIFile.m_nExpireEmailAccountDays*24*60);
	QString strSql="SELECT USR_ID,USR_EMAIL,USR_IS_CONFIRMED,USR_ALLOW_LOGIN, USR_EMAIL_SERVER_ID FROM USER_DATA WHERE USR_DAT_LAST_LOGIN <(CURRENT_TIMESTAMP-"+strTimeStampDeadLine+"/1440.0)";
	query.Execute(pStatus,strSql);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstAccountsToDelete;
	query.FetchData(lstAccountsToDelete);

	if (lstAccountsToDelete.getRowCount()==0)
		return;
	
	//delete all unconfirmed and when dat login passed X days and status is denied
	int nSize=lstAccountsToDelete.getRowCount();
	int nDeleted=0;
	for (int i=0;i<nSize;i++)
	{
		if (lstAccountsToDelete.getDataRef(i,"USR_IS_CONFIRMED").toInt()==0 || lstAccountsToDelete.getDataRef(i,"USR_ALLOW_LOGIN").toInt()==LOGIN_DENY)
		{
			QString strEmail=lstAccountsToDelete.getDataRef(i,"USR_EMAIL").toString();
			int nEsrHostID =lstAccountsToDelete.getDataRef(i,"USR_EMAIL_SERVER_ID").toInt();

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("CleanUpOldAccounts trying to delete user %1").arg(strEmail));

			QString strSQL="DELETE FROM USER_DATA WHERE USR_EMAIL ='"+strEmail+"'";
			query.Execute(pStatus,strSQL);
			if(!pStatus.IsOK()) return;

			//now delete from ES host:
			if (nEsrHostID>0)
			{
				DeleteUserOnESHost(pStatus,nEsrHostID,strEmail);
				if(!pStatus.IsOK()) return;
			}

			nDeleted++;
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("CleanUpOldAccounts successfully deleted user %1").arg(strEmail));
		}
	}

	if (nDeleted>0)
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("CleanUpOldAccounts successfully cleaned up %1 expired accounts").arg(nDeleted));
}


/*
//read txt file from http://mailegant.com/lb.txt
QString ServiceHandler::GetLBServerURL( Status &pStatus )
{
	pStatus.setError(0);

	QByteArray strURL;
	HttpReaderSync WebReader(6000);  //6sec..
	WebReader.ReadWebContent(LB_SERVER_LOOKUP, &strURL);;

	if (strURL.isEmpty())
	{
		pStatus.setError(1,"Error while resolving Load Balance Server URL address!");
		return "";
	}

	return strURL.trimmed();
}
*/
void ServiceHandler::LockDatabase( Status &pStatus, QString strConce, QString strAuthToken, int nLockReadOnly )
{

#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	g_AppServer->m_INIFile.m_nLockWriteDatabase=nLockReadOnly;
	g_AppServer->SaveSettings();
}

void ServiceHandler::IsServerAlive( Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strLoadBalanceServerURL, int &Ret_nStatus )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	Ret_nStatus=g_AppServer->m_INIFile.m_nLockWriteDatabase; // 0 -ok, 1- locked, 2...no reply at all
	g_AppServer->m_INIFile.m_strHostUniqueID=strHostUniqueID;
	g_AppServer->m_INIFile.m_strLoadBalanceServerURL=strLoadBalanceServerURL;
	
	g_AppServer->SaveSettings();
}

void ServiceHandler::AddEmailToCS( Status &pStatus, QString strConce, QString strAuthToken, QString strEmail )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
#endif

	//check if lock
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}

	//add email to user data, send conf email...
	QString Ret_strClientPassword;
	QString Ret_strESHost;

	SendConfirmationEmail(pStatus, strEmail,Ret_strClientPassword, Ret_strESHost,true);

}

//delete email from both user data and user acesss
void ServiceHandler::RemoveEmailToCS( Status &pStatus, QString strConce, QString strAuthToken, QString strEmail )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
#endif

	//check if lock
	if (g_AppServer->m_INIFile.m_nLockWriteDatabase)
	{
		pStatus.setError(1,"Server database is locked, no write is possible! Ask administrator for assistance!");
		return;
	}
	
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK()) return;


	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RemoveEmailToCS: DELETE FROM USER_DATA WHERE USR_EMAIL = '"+strEmail+"'" ));

	//check if email exists
	query.Execute(pStatus,"DELETE FROM USER_DATA WHERE USR_EMAIL = '"+strEmail+"'");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}

	query.Execute(pStatus,"DELETE FROM USER_ACCESS WHERE USA_EMAIL = '"+strEmail+"'");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}

	query.Commit(pStatus);
}

void ServiceHandler::RegisterEmailToLBServer( Status &pStatus, QString strEmail )
{
	//-------------------------------------
	//notify mwLB server: register new email
	//-------------------------------------
	//resolve URL:
	QString strLB_URL=g_AppServer->m_INIFile.m_strLoadBalanceServerURL;
	QString strHostUniqueID=g_AppServer->m_INIFile.m_strHostUniqueID;

	if (strHostUniqueID.isEmpty() || strLB_URL.isEmpty())
		return;

	QUrl url(strLB_URL);
	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();
	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if (!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("RegisterEmailToLBServer: failed to connect to the load balance service at %1, error was: %1").arg(strLB_URL).arg(pStatus.getErrorText()));
		return;
	}

	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	//QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();

	client.msg_send.AddParameter(&strConce);
	client.msg_send.AddParameter(&strAuthTokenServer);
	client.msg_send.AddParameter(&strHostUniqueID);
	client.msg_send.AddParameter(&strEmail);

	client.RestSend(pStatus,"POST","/rest/service/addemailfromcs");
	if (!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("RegisterEmailToLBServer: failed to call 'addemailfromcs' at load balance server at %1, error was: %1").arg(strLB_URL).arg(pStatus.getErrorText()));
		return;
	}

	Status err2;
	client.Disconnect(err2);

}

//used as notifier too: LB will check last reported time..
void ServiceHandler::SyncEmailsToLBServer( Status &pStatus)
{
	//load all emails from user data:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql;

	//check if email exists
	query.Execute(pStatus,"SELECT USR_EMAIL FROM USER_DATA");
	if (!pStatus.IsOK())return; 

	DbRecordSet lstData;
	query.FetchData(lstData);

	if (lstData.getRowCount()==0)
		return;
	
	QString strRegEmails;

	int nSizeEmails=lstData.getRowCount();
	for (int i=0;i<nSizeEmails;i++)
	{
		strRegEmails.append(lstData.getDataRef(i,0).toString()+";");
	}

	//-------------------------------------
	//notify mwLB server: register new email
	//-------------------------------------
	//resolve URL:
	QString strLB_URL=g_AppServer->m_INIFile.m_strLoadBalanceServerURL;
	QString strHostUniqueID=g_AppServer->m_INIFile.m_strHostUniqueID;

	if (strHostUniqueID.isEmpty() || strLB_URL.isEmpty())
		return;

	QUrl url(strLB_URL);
	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();
	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if (!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SyncEmailsToLBServer: failed to connect to the load balance service at %1, error was: %2").arg(strLB_URL).arg(pStatus.getErrorText()));
		return;
	}

	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	//QString strAuthTokenServer=Hasher.GetHash(QString(STAT_USERNAME+strConce+SECRET_SPICE_PASSWORD+STAT_PASSWORD).toLatin1()).toHex();

	client.msg_send.AddParameter(&strConce);
	client.msg_send.AddParameter(&strAuthTokenServer);
	client.msg_send.AddParameter(&strHostUniqueID);
	client.msg_send.AddParameter(&strRegEmails,"Emails");

	client.RestSend(pStatus,"POST","/rest/service/syncemailsfromcs");
	if (!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SyncEmailsToLBServer: failed to call 'SyncEmailsFromCS' at load balance server at %1, error was: %2").arg(strLB_URL).arg(pStatus.getErrorText()));
		return;
	}

	Status err2;
	client.Disconnect(err2);

}

//when LB is deleting CS server then it will try to unregister it so CS is no longer bonded to this LB server
void ServiceHandler::UnregisterLBServer( Status &pStatus, QString strConce, QString strAuthToken, QString strHostUniqueID, QString strLoadBalanceServerURL )
{
#if TURN_OFF_AUTHENTICATION !=1

	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(MW_LB_USERNAME+strConce+SECRET_SPICE_PASSWORD+MW_LB_PASSWORD).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

#endif

	if (strHostUniqueID==g_AppServer->m_INIFile.m_strHostUniqueID && g_AppServer->m_INIFile.m_strLoadBalanceServerURL==strLoadBalanceServerURL)
	{
		g_AppServer->m_INIFile.m_strHostUniqueID="";
		g_AppServer->m_INIFile.m_strLoadBalanceServerURL="";
		g_AppServer->SaveSettings();
	}
}

void ServiceHandler::SetAddOnPass( Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,QString strAddOnPass )
{

#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;



	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql;

	//B.T.: on every call update user data table just to refresh last modify field -> for garbage collector purposes:
	query.Prepare(pStatus,"UPDATE USER_DATA SET USR_ADDON_PASS=? WHERE USR_EMAIL = '"+strEmail+"'");
	if (!pStatus.IsOK())return; 

	query.bindValue(0,strAddOnPass);

	query.ExecutePrepared(pStatus);
	if (!pStatus.IsOK())return; 

}

void ServiceHandler::GetAuthenticationID( Status &pStatus, QString strEmail, QString strConce, QString strAuthToken,QString &Ret_strClientPassword )
{

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	Ret_strClientPassword="";
	QString strAddOnPass;
	query.Execute(pStatus,"SELECT USR_PASSWORD,USR_ADDON_PASS FROM USER_DATA WHERE USR_EMAIL = '"+strEmail+"'");
	if (!pStatus.IsOK())return; 
	if (query.next())
	{
		Ret_strClientPassword=query.value(0).toString();
		strAddOnPass=query.value(1).toString();
	}

	//now authenticate add on pass:
	Sha256Hash Hasher;
	QString strAuthTokenServer=Hasher.GetHash(QString(strEmail+strConce+SECRET_SPICE_PASSWORD+strAddOnPass).toLatin1()).toHex();
	if (strAuthTokenServer != strAuthToken)
	{
		Ret_strClientPassword="";
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

}

void ServiceHandler::SendUserErrorPushMessageToESHost(Status &pStatus, int nESRID, QString strUser, QString strEmailFrom, QString strErrorMessage)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::SendUserErrorPushMessageToESHost start");

	//get HOST, extract protocol, port and IP address:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strESHost;
	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS FROM EMAIL_SERVERS WHERE ESR_ID="+QString::number(nESRID));
	if(!pStatus.IsOK()) return;

	if (query.next())
	{
		strESHost=query.value(0).toString();
	}
	if (strESHost.isEmpty())
		return;


	QUrl url(strESHost);

	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();

	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;
	
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::SendUserErrorPushMessageToESHost connecting to %1, %2, %3").arg(strIP).arg(nPort).arg(nIsSSL));

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return;


	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,SERVER_INTERSERVER_COMM_USERNAME,SERVER_INTERSERVER_COMM_PASSWORD,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::SendUserErrorPushMessageToESHost session ok"));

	QString strLastName,strFirstName;
	strFirstName="";
	strLastName=strUser;

	//SEND:
	client.msg_send.AddParameter(&strUser,"strUser");
	client.msg_send.AddParameter(&strEmailFrom,"strEmailFrom");
	client.msg_send.AddParameter(&strErrorMessage,"strErrorMessage");

	client.RestSend(pStatus,"POST","/rest/mwcloudservice/sendusererrorpushmessage",strSession);
	if(!pStatus.IsOK()) return;

	
	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);
	if(!pStatus.IsOK()) return;


}

void ServiceHandler::GetServerTime( Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt)
{
	
#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;
	

	Ret_datCurrentServerTime=QDateTime::currentDateTime();
	Ret_datTimeNextTest =QDateTime::fromString(g_AppServer->m_INIFile.m_strNextTestTime,"yyyyMMddHHmm");

	Ret_nActiveClientSocketCnt = g_AppServer->GetHttpServer().GetActiveSocketsCount();
}

void ServiceHandler::SetNextTestTime( Status &pStatus, QString strEmail, QString strConce, QString strAuthToken, QDateTime datSetTimeNextTest )
{
#if TURN_OFF_AUTHENTICATION
	pStatus.setError(0);
#else
	AuthenticateUser(pStatus,strEmail,strConce,strAuthToken);
#endif
	if (!pStatus.IsOK())
		return;


	QString strDateTimeString=datSetTimeNextTest.toString("yyyyMMddHHmm");
	g_AppServer->m_INIFile.m_strNextTestTime=strDateTimeString;
	g_AppServer->SaveSettings();
}

void ServiceHandler::CheckAppleReceipt(Status &pStatus,QString strReceipt)
{
	//strReceipt="MIIT6wYJKoZIhvcNAQcCoIIT3DCCE9gCAQExCzAJBgUrDgMCGgUAMIIDnAYJKoZIhvcNAQcBoIIDjQSCA4kxggOFMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDgIBAQQDAgFoMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDQIBDQIBAQQFAgMBEXMwDQIBEwIBAQQFDAMxLjAwDgIBAwIBAQQGDAQxLjM3MA4CAQkCAQEEBgIEUDIzMTAYAgEEAgECBBBn7pyn4S0G64KZ0Z6mfwACMBsCAQACAQEEEwwRUHJvZHVjdGlvblNhbmRib3gwHAIBBQIBAQQULolnb73tm9HoeKii0+CUClqYPkwwHgIBDAIBAQQWFhQyMDE0LTA4LTA1VDA5OjE4OjMyWjAeAgESAgEBBBYWFDIwMTMtMDgtMDFUMDc6MDA6MDBaMCcCAQICAQEEHwwdY29tLm1haWxlZ2FudC5tYWlsZWdhbnR3cml0ZXIwNgIBBwIBAQQujdLJSEEs6f/Tstv0V3nCuUVgnQ6tKStU8iPDQiTroiieFWRpuFTydvfZAjAPCDBJAgEGAgEBBEH8GaGqPAyyVynI9n61Da1ZZpxF/+Lenil/VKyG9ZqL2RHWIrQtkBLVB1OQgm0+Viai4RfLj0K9nlAaDAGPXZPAQTCCAY4CARECAQEEggGEMYIBgDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6mePRLMBsCAganAgEBBBIMEDEwMDAwMDAxMTkxMzk3OTEwGwICBqkCAQEEEgwQMTAwMDAwMDExOTEzOTc5MTAfAgIGqAIBAQQWFhQyMDE0LTA4LTA1VDA5OjE4OjMyWjAfAgIGqgIBAQQWFhQyMDE0LTA4LTA1VDA5OjE4OjI4WjAfAgIGrAIBAQQWFhQyMDE0LTA4LTA1VDA5OjIzOjI3WjA6AgIGpgIBAQQxDC9jb20ubWFpbGVnYW50Lm1haWxlZ2FudHdyaXRlci5tb250aHN1YnNjcmlwdGlvbqCCDlUwggVrMIIEU6ADAgECAggYWUMhcnSc/DANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xMDExMTEyMTU4MDFaFw0xNTExMTEyMTU4MDFaMHgxJjAkBgNVBAMMHU1hYyBBcHAgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC2k8K3DyRe7dI0SOiFBeMzlGZb6Cc3v3tDSev5yReXM3MySUrIb2gpFLiUpvRlSztH19EsZku4mNm89RJRy+YvqfSznxzoKPxSwIGiy1ZigFqika5OQMN9KC7X0+1N2a2K+/JnSOzreb0CbQRZGP+MN5+KN/Fi/7uiA1CHCtWS4IYRXiNG9eElYyuiaoyyELeRI02aP4NA8mQJWveNrlZc1PW0bgMbBF0sG68AmRfXpftJkc7ioRExXhkBwNrOUINeyOtJO0kaKurgn7/SRkmc2Kuhg2FsD8H8s62ZdSr8I5vvIgjre1kUEZ9zNC3muTmmO/fmPuzKpvurrybfj4iBAgMBAAGjggHYMIIB1DAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3ME0GA1UdHwRGMEQwQqBAoD6GPGh0dHA6Ly9kZXZlbG9wZXIuYXBwbGUuY29tL2NlcnRpZmljYXRpb25hdXRob3JpdHkvd3dkcmNhLmNybDAOBgNVHQ8BAf8EBAMCB4AwHQYDVR0OBBYEFHV2JKJrYgyXNKH6Tl4IDCK/c+++MIIBEQYDVR0gBIIBCDCCAQQwggEABgoqhkiG92NkBQYBMIHxMIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMCkGCCsGAQUFBwIBFh1odHRwOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAoDvxh7xptLeDfBn0n8QCZN8CyY4xc8scPtwmB4v9nvPtvkPWjWEt5PDcFnMB1jSjaRl3FL+5WMdSyYYAf2xsgJepmYXoePOaEqd+ODhk8wTLX/L2QfsHJcsCIXHzRD/Q4nth90Ljq793bN0sUJyAhMWlb1hZekYxQWi7EzVFQqSM+hHVSxbyMjXeH7zSmV3I5gIyWZDojcs53yHaw3b7ejYaFhqYTIUb5itFLS9ZGi3GmtZmkqPSNlJQgCBNM8iymtZTYrFgUvD1930QUOQSv71xvrSAx23Eb1s5NdHnt96BICeOOFyChzpzYMTW8RygqWZEfs4MKJsjf6zs5qA73TCCBCMwggMLoAMCAQICARkwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA4MDIxNDE4NTYzNVoXDTE2MDIxNDE4NTYzNVowgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjga4wgaswDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMDYGA1UdHwQvMC0wK6ApoCeGJWh0dHA6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2Evcm9vdC5jcmwwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBANoyAJbFVJTTO4I3Zn0uaNXDxrjLJoxIkM8TJGpGjmPU8NATBt3YxME3FfIzEzkmLc4uVUDjCwOv+hLC5w0huNWAz6woL84ts06vhhkExulQ3UwpRxAj/Gy7G5hrSInhW53eRts1hTXvPtDiWEs49O11Wh9ccB1WORLl4Q0R5IklBr3VtBWOXtBZl5DpS4Hi3xivRHQeGaA6R8yRHTrrI1r+pS2X93u71odGQoXrUj0msmOotLHKj/TM4rPIR+C/mlmD+tqYUyqC9XxlLpXZM1317WXMMTfFWgToa+HniANKdZ6bKMtKQIhlQ3XdyzolI8WeV/guztKpkl5zLi8ldRUwggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIGFlDIXJ0nPwwCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQAktFg0bY07xQgT2SvQJg8pDWKxvZKiemnUAgQA4WYUnTqgaiF+cRx4p+T2TgTm+Zpn+dVwd6DdoSrCdsbyRJttJ21ha6GT+2ygWQLH3+C2bpcp+HnlRrDtuYqaMeiTHbC85HNiKBkpzA0WV64qmv69omLuEm/zoRq1/k/zQcVssb8O6/hHISGsginVlToMRnhH1peyhPxjNid9imag1dp/kwUUKhUdgjcXqmVV4wVdopRgdyq98oSGxiKDrDk1BzsMKTGHhokfsPpvvkIX/rBRlZh145De4eI6jrx0vAMPE1ijHjoQlvIqjx9WJiU6Pz6r77lDNfdE1jFYkemc1I1L";
	QString strJson="{\"receipt-data\": \""+strReceipt+"\",";
	strJson+="\"password\": \""+g_AppServer->m_INIFile.m_strAppleReceiptPasswordCheck+"\"}";

	//now send it:
	HttpReaderSync reader(10000); //10 sec timeout
	QByteArray buffer;
	if(reader.PostWebContent(g_AppServer->m_INIFile.m_strAppleReceiptValidationUrl,strJson.toLatin1(),&buffer,true))
	{
		//parse response
		QJsonParseError error;
		QJsonDocument jsonReturned = QJsonDocument::fromJson(buffer,&error);

		if(jsonReturned.isObject())
		{
			QJsonValue val=jsonReturned.object().value("status");
			if (val.isDouble())
			{
				double nReplyCode=val.toDouble();
				if (nReplyCode!=0)
				{
					pStatus.setError(1,QString("Apple receipt validation failed: receipt is not valid, status code=%1!").arg(nReplyCode));
				}
				return;
			}
		}
		pStatus.setError(1,"Apple receipt validation failed to parse server response!");
	}
	else
		pStatus.setError(1,"Apple receipt validation url is unaccessible!");
}

//if subscription expires: notify ES server about this:
void ServiceHandler::EnableUserAccountOnESHost(Status &pStatus, int nESRID, QString strUser, bool bEnable)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::EnableUserAccountOnESHost start");

	//make REST call to the ES server, register user

	//get HOST, extract protocol, port and IP address:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strESHost;
	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS FROM EMAIL_SERVERS WHERE ESR_ID="+QString::number(nESRID));
	if(!pStatus.IsOK()) return;

	if (query.next())
	{
		strESHost=query.value(0).toString();
	}
	if (strESHost.isEmpty())
		return;

	QUrl url(strESHost);

	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();

	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::EnableUserAccountOnESHost connecting to %1, %2, %3").arg(strIP).arg(nPort).arg(nIsSSL));

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::EnableUserAccountOnESHost connect done"));


	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,SERVER_INTERSERVER_COMM_USERNAME,SERVER_INTERSERVER_COMM_PASSWORD,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("ServiceHandler::EnableUserAccountOnESHost session ok"));

	QString strLastName,strFirstName;
	strFirstName="";
	strLastName=strUser;

	//SEND:
	client.msg_send.AddParameter(&strUser,"strUser");
	client.msg_send.AddParameter(&bEnable,"bEnable");

	client.RestSend(pStatus,"POST","/rest/mwcloudservice/enableuseraccount",strSession);
	if(!pStatus.IsOK()) return;

	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);
	if(!pStatus.IsOK()) return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"ServiceHandler::EnableUserAccountOnESHost end");
}

void ServiceHandler::DeleteUserOnESHost( Status &pStatus, int nESRID, QString strUser )
{

	//get HOST, extract protocol, port and IP address:
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strESHost;
	query.Execute(pStatus,"SELECT ESR_HOST_ADDRESS FROM EMAIL_SERVERS WHERE ESR_ID="+QString::number(nESRID));
	if(!pStatus.IsOK()) return;

	if (query.next())
	{
		strESHost=query.value(0).toString();
	}
	if (strESHost.isEmpty())
		return;


	QUrl url(strESHost);

	QString strIP=url.host();
	int nPort=url.port();
	QString strScheme=url.scheme();

	int nIsSSL=0;
	if (strScheme.toLower()=="https")
		nIsSSL=1;

	RestHTTPClient client;
	client.SetConnectionSettings(strIP,nPort,nIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return;

	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,SERVER_INTERSERVER_COMM_USERNAME,SERVER_INTERSERVER_COMM_PASSWORD,strSession);
	if(!pStatus.IsOK()) return;

	QString strLastName,strFirstName;
	strFirstName="";
	strLastName=strUser;

	//SEND:
	client.msg_send.AddParameter(&strUser,"strUser");

	client.RestSend(pStatus,"POST","/rest/mwcloudservice/deleteuser",strSession);
	if(!pStatus.IsOK()) return;

	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);
	if(!pStatus.IsOK()) return;

}

