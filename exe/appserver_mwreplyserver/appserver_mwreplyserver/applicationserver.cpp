#include "applicationserver.h"
#include "common/common/datetimehandler.h"
#include "common/common/logger.h"
#include "trans/trans/tcphelper.h"
#include "servicehandler.h"
#include "common/common/threadid.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "db_core/db_core/dbconnsettings.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbconnectionsetup.h"
#include "db_core/db_core/dbsqltableview.h"
#include "emailsendermanager.h"
#include "emailnotificator.h"
#include "applicationserverselftest.h"
#include "common/common/datahelper.h"
#include "common/common/stackwalkerlogger.h"
#include "serverbackupmanager.h"
#include "dbtabledef.h"

#define MASTER_PASSWORD	"SOKRATES(R)"
#define MWREPLY_DB_VERSION	7
#define MWREPLY_EMAIL_SYNC_INTERVAL 60
#define EMAIL_SENDER_TASKER_INTERVAL	1*10 //10sec
#define GARBAGE_TASKER_INTERVAL	4*60 //4min

CRASH_SIGNAL_HANDLER

//-----------------------------------------------
//	 SERVER GLOBALS:
//------------------------------------------------

//SPECIALS:
Logger							g_Logger;
ApplicationServer				*g_AppServer=NULL;
ServiceHandler					*g_ServiceHandler=NULL;
DbSqlManager					*g_DbManager=NULL;
EmailSenderManager				*g_pEmailSenderManager=NULL;
ApplicationServerSelfTest		*g_pAppSelfTester=NULL;
BackupManager					*g_pBackupManager;
//EmailNotificator				*g_pEmailNotificator=NULL;




/*!
	Constructor
*/
ApplicationServer::ApplicationServer(QObject *parent)
:QObject(parent)
{
	ClearPointers();
	m_nTimerID=-1;
	m_bIsRunning=false;
	connect(this,SIGNAL(RestartDelayedThreadSafeSignal(int)),this,SLOT(RestartDelayedThreadSafeSlot(int)),Qt::QueuedConnection);
	connect(this,SIGNAL(StopThreadSafeSignal()),this,SLOT(StopThreadSafeSlot()),Qt::QueuedConnection);
	ApplicationLogger::setApplicationLogger(&g_Logger);
	
}

void ApplicationServer::ClearPointers()
{
	m_DbObjectManager=NULL;
	m_RestHTTPServer=NULL;
	m_RestServiceRpcDispatcher=NULL;
	g_ServiceHandler=NULL;
	m_GarbageCollector=NULL;
	m_GbBkgTask=NULL;
	m_HttpInviteHandler=NULL;
	g_DbManager=NULL;
	g_pEmailSenderManager=NULL;
	g_pAppSelfTester=NULL;
	//g_pEmailNotificator=NULL;
	m_EmailSubscribe=NULL;
	m_EmailSubscribeBkgTask=NULL;
	m_EmailNotificatorTask=NULL;
	m_HtmlHTTPServer = NULL;
	g_pBackupManager=NULL;
	m_SrvBackupBkgTask=NULL;

	m_strIniFilePath="";
}

/*!
	Destructor: kill all objects
*/
ApplicationServer::~ApplicationServer()
{
	if (IsRunning())
		Stop();
}

void ApplicationServer::Restart(Status &pStatus)
{
	//QMutexLocker locker(&m_Mutex); //lock acces
	Stop();
	Start(pStatus);
	//exit if error:

}

void ApplicationServer::RestartDelayed(int nReStartAfterSec)
{
	QMutexLocker locker(&m_Mutex);							//lock acces
	emit RestartDelayedThreadSafeSignal(nReStartAfterSec);	//ensures that appserver thread executes this!
}

//ignore parameter: do it now!
void ApplicationServer::StopDelayed(int nStopAfterSec)
{
	QMutexLocker locker(&m_Mutex);	//lock acces
	emit StopThreadSafeSignal();	//ensures that appserver thread executes this!
}

void ApplicationServer::StopThreadSafeSlot()
{
	Stop();
}

void ApplicationServer::RestartDelayedPriv(int nReStartAfterSec)
{
	//already in restart mode, ignore
	if (m_nTimerID!=-1)
	{
		return;
	}
	//this is fine opportunity to fire warning to all clients->
	m_nTimerID=startTimer(nReStartAfterSec*1000);
}

void ApplicationServer::Stop()
{
	QMutexLocker locker(&m_Mutex); //lock acces
	
	//if (!m_bIsRunning)	return;

	//stop server:
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopping Http server");
	SokratesHttpServer.StopServer();
	SokratesHttpServer.ClearRequestHandlerList();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Http server stopped");


	//--------------------------------------------------
	//Stop sequence must be in particular order: (1st stop background services)
	//--------------------------------------------------
	if(m_GbBkgTask!=NULL)m_GbBkgTask->Stop();
	if(m_EmailSubscribeBkgTask!=NULL)m_EmailSubscribeBkgTask->Stop();
	if(m_EmailNotificatorTask!=NULL)m_EmailNotificatorTask->Stop();
	if(m_SrvBackupBkgTask!=NULL)m_SrvBackupBkgTask->Stop();
	
	//save backup options in INI:
	if (m_SrvBackupBkgTask!=NULL)
	{
		Status err;
		g_pBackupManager->GetBackupData(err,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);
	}


	//save log & all rest:
	m_INIFile.m_nLogLevel=g_Logger.GetLogLevel();
	m_INIFile.m_nLogMaxSize=g_Logger.GetMaxLogSize();
	m_INIFile.m_nLogBufferSize=g_Logger.GetMemoryBufferSize();
	m_INIFile.Save(m_strIniFilePath);

	//destroy objects:
	DestroyObjectDbManager();

	//destroy tasker objects:
	if(m_GbBkgTask!=NULL)					delete m_GbBkgTask;
	if(m_EmailSubscribeBkgTask!=NULL)		delete m_EmailSubscribeBkgTask;
	if(m_EmailNotificatorTask!=NULL)		delete m_EmailNotificatorTask;

	//destroy main objects:
	if(m_HtmlHTTPServer != NULL)			delete m_HtmlHTTPServer;
	if(m_RestHTTPServer!=NULL)				delete m_RestHTTPServer;
	if(m_RestServiceRpcDispatcher!=NULL)	delete m_RestServiceRpcDispatcher;
	if(m_EmailSubscribe!=NULL)				delete m_EmailSubscribe;
	if(g_ServiceHandler!=NULL)				delete g_ServiceHandler;
	if(g_DbManager!=NULL)
	{
		g_DbManager->ShutDown();
		delete g_DbManager;
	}
	if(m_GarbageCollector!=NULL)			delete m_GarbageCollector;
	if(m_HttpInviteHandler!=NULL)			delete m_HttpInviteHandler;
	if(g_pEmailSenderManager!=NULL)			delete g_pEmailSenderManager;
	if(g_pAppSelfTester!=NULL)				delete g_pAppSelfTester;
	if(m_SrvBackupBkgTask!=NULL)			delete m_SrvBackupBkgTask;
	if(g_pBackupManager!=NULL)				delete g_pBackupManager;

	//if(g_pEmailNotificator!=NULL)			delete g_pEmailNotificator;
	
	m_bIsRunning=false;
	ClearPointers();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_STOP);
	g_Logger.FlushFileLog(); 
}



/*!
	Start App. server
*/
void ApplicationServer::Start(Status &pStatus,QString strServiceDescriptor)
{
	//prepare crash log
	PrepareCrashLog();

	QMutexLocker locker(&m_Mutex); //lock access
	if (m_bIsRunning)	return;

	HTTPServerConnectionSettings ServerSettings;
	DbConnectionSettings DbConnSettings;

	//----------------------------------------------
	//LOAD INI files:
	//----------------------------------------------
	LoadINI(pStatus);
	if(!pStatus.IsOK()) return;
	
	//registration URL address check:
	if (!m_INIFile.m_strHostDomain.isEmpty())
	{
		if (m_INIFile.m_nSSLMode==1)
			m_INIFile.m_strHTTPRegisterAddress="https://"+m_INIFile.m_strHostDomain+":"+QString::number(m_INIFile.m_nAppServerPort)+"/registration";
		else
			m_INIFile.m_strHTTPRegisterAddress="http://"+m_INIFile.m_strHostDomain+":"+QString::number(m_INIFile.m_nAppServerPort)+"/registration";
		m_INIFile.Save(m_strIniFilePath);
	}

	if (!strServiceDescriptor.isEmpty())
	{
		m_INIFile.m_strServiceDescriptor=strServiceDescriptor;
		m_INIFile.Save(m_strIniFilePath);
	}

	//----------------------------------------------
	//Start logger 
	//----------------------------------------------
	if (m_INIFile.m_nLogLevel>LoggerAbstract::LOG_LEVEL_NONE)
	{
		g_Logger.EnableFileLogging(QCoreApplication::applicationDirPath()+"/settings/appserver.log",m_INIFile.m_nLogBufferSize,m_INIFile.m_nLogMaxSize);
		g_Logger.EnableConsoleLogging();
	}
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);

	//----------------------------------------------
	//LOAD NET & DB files:
	//----------------------------------------------
	LoadNetSettings(pStatus,ServerSettings);
	if(!pStatus.IsOK()) return;
	LoadDbSettings(pStatus,DbConnSettings);
	if(!pStatus.IsOK())
	{
		CreateNewDb(pStatus,DbConnSettings);
		if(!pStatus.IsOK()) return;
	}


	//----------------------------------------------
	//init Database
	//----------------------------------------------
	InitDatabaseConnection(pStatus,&g_DbManager,DbConnSettings,m_INIFile.m_nMaxDbConnections,m_INIFile.m_nMinDbConnections);
	if(!pStatus.IsOK())return;
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_DATABASE_CONN_SUCESS,DbConnSettings.m_strDbName+";"+DbConnSettings.m_strDbHostName);
	

	//----------------------------------------------
	//Launch Business & Private sets/init pointers:
	//----------------------------------------------
	g_ServiceHandler = new ServiceHandler();


	//---------------------------------------------
	//Backup manager -> only if master
	//---------------------------------------------
	ServerBackupManager *pSrvBcp=new ServerBackupManager;
	g_pBackupManager = pSrvBcp;
	g_pBackupManager->SetBackupData(pStatus,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);
	if(!pStatus.IsOK())return;
	g_pBackupManager->Initialize(pStatus,g_DbManager->GetDbSettings(),false);
	if(!pStatus.IsOK())return;


	bool bError=true;
	if(pStatus.IsOK())
	{
		g_pBackupManager->StartUpCheck(pStatus);
		if(pStatus.IsOK())
		{
			//save, whatever:
			g_pBackupManager->GetBackupData(pStatus,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);
			Q_ASSERT(pStatus.IsOK());
			m_INIFile.Save(m_strIniFilePath);

			//start bkg manager::
			m_SrvBackupBkgTask=new BackupTaskExecutor(pSrvBcp,this);
			m_SrvBackupBkgTask->Start();
			bError=false;
		}
	}

	//if error, log, exit: must exit if Db not reorganized!
	if (bError)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_FAIL,pStatus.getErrorText());
		return;
	}




	//----------------------------------------------
	//Start email sync
	//----------------------------------------------
	/* B.T. 2014-03-31 disable (requested by MB)
	m_EmailSubscribe= new EmailImportThread();
	int nAccounts = m_EmailSubscribe->LoadEmailAccounts();
	if (nAccounts==0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,1,"There are no email account files (emailacc_*.ini) define in the /settings folder");
		pStatus.setError(1,"There are no email account files (emailacc_*.ini) define in the /settings folder");
		return;
	}
	m_EmailSubscribeBkgTask=new BackgroundTaskExecutor(m_EmailSubscribe,this,m_INIFile.m_nScanFrequency*1000);
	m_EmailSubscribeBkgTask->Start();
	*/

	//----------------------------------------------
	//Start Garbage Collector in separate thread (GBC controls HTTP server):
	//----------------------------------------------
	m_GarbageCollector= new GarbageCollector(&SokratesHttpServer);
	m_GbBkgTask=new BackgroundTaskExecutor(m_GarbageCollector,this,GARBAGE_TASKER_INTERVAL*1000);	//10min timer to clean up the garbage James entries from the DB
	m_GbBkgTask->Start();


	//----------------------------------------------
	//Start Email sender
	//----------------------------------------------
	g_pEmailSenderManager			= new EmailSenderManager();
	m_EmailSenderTask=new BackgroundTaskExecutor(g_pEmailSenderManager,this,EMAIL_SENDER_TASKER_INTERVAL*1000);
	m_EmailSenderTask->Start(false,1000); //on start wait for 1sek

	//----------------------------------------------
	//Start Email notificator
	//----------------------------------------------
	//B.T. 2014-03-31 disable (requested by MB)
	//g_pEmailNotificator			= new EmailNotificator();
	//m_EmailNotificatorTask=new BackgroundTaskExecutor(g_pEmailNotificator,this,EMAIL_SENDER_TASKER_INTERVAL*1000);
	//m_EmailNotificatorTask->Start(false,1000); //on start wait for 1sek


	//----------------------------------------------
	//Launch all server handlers:
	//----------------------------------------------
	//TcpHelper::ResolveWebAddressNetworkInterface(ServerSettings.m_strServerIP.toLower(),ServerSettings.m_nPort,ServerSettings.m_bUseSSL);

	m_RestServiceRpcDispatcher		= new RestRpcDispatcher();
	m_RestHTTPServer				= new RestHTTPServer_Local(m_RestServiceRpcDispatcher);
	m_HttpInviteHandler				= new HttpInviteHandler();
	m_HtmlHTTPServer				= new HtmlHTTPServer();
	

	SokratesHttpServer.ClearRequestHandlerList();
	SokratesHttpServer.RegisterRequestHandler(m_RestHTTPServer);
	SokratesHttpServer.RegisterRequestHandler(m_HttpInviteHandler);

//#ifdef _DEBUG //only active in debug for test rpc...
	m_HtmlHTTPServer->m_nDefaultSocketTimeOut=20;						//set timeout to 20min
	SokratesHttpServer.RegisterRequestHandler(m_HtmlHTTPServer);
//#endif

	SokratesHttpServer.InitServer(ServerSettings,false);
	SokratesHttpServer.StartListen(pStatus);

	if (pStatus.IsOK())
	{
		QString appParams=ServerSettings.m_strServerIP+";"+QVariant(ServerSettings.m_nPort).toString()+";"+QVariant(ServerSettings.m_bUseSSL).toString();
		appParams+=";1;"+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString();
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_START,appParams);
		//g_Logger.logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_START,appParams);
		m_bIsRunning=true;
	}
	

	//----------------------------------------------
	//now start self test:
	//----------------------------------------------
	g_pAppSelfTester = new ApplicationServerSelfTest;
	QTimer::singleShot(0,this,SLOT(OnFillDatabaseAtStartForTest()));

	/*
	QList<QSqlDatabase *> lstDb;
	for (int i=0;i<300;i++)
	{
		Status err;
		lstDb.append(g_DbManager->ReserveConnection(err));
	}

	for (int i=0;i<300;i++)
	{
		Status err;
		QSqlDatabase *pDb=lstDb.at(i);
		g_DbManager->ReleaseConnection(pDb);
	}*/


	//test receipt:
	//Status err;
	//g_ServiceHandler->CheckAppleReceipt(err,"1234");


}

void ApplicationServer::SaveSettings()
{
	QMutexLocker locker(&m_Mutex);			//lock acces

	Status err;
	g_pBackupManager->GetBackupData(err,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);

	m_INIFile.Save(m_strIniFilePath);
}

void ApplicationServer::LoadINI(Status &pStatus)
{
	//load ini file & server settings:
	//------------------------------------------
	m_strIniFilePath= QCoreApplication::applicationDirPath()+"/settings/app_server.ini";
	bool bOK=m_INIFile.Load(m_strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,"INI file is missing,corrupted or has invalid values!");
		return;
	}

	QFileInfo ssl(m_INIFile.m_strSSLCertificateDir+"/server.cert");
	if (!ssl.exists())
	{
		m_INIFile.m_strSSLCertificateDir=QCoreApplication::applicationDirPath()+"/settings";
	}
}

void ApplicationServer::LoadNetSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings)
{
	pServerSettings.m_strCertificateFile=m_INIFile.m_strSSLCertificateDir+"/server.cert";
	pServerSettings.m_strCACertificateFileBundle=m_INIFile.m_strSSLCertificateDir+"/server.ca-bundle";
	pServerSettings.m_strPrivateKeyFile=m_INIFile.m_strSSLCertificateDir+"/server.pkey";
	pServerSettings.m_nPort=m_INIFile.m_nAppServerPort;

	if(m_INIFile.m_strAppServerIPAddress.isEmpty())
		pServerSettings.m_strServerIP="0.0.0.0"; //listen on all ports
	else
		pServerSettings.m_strServerIP=m_INIFile.m_strAppServerIPAddress;
	pServerSettings.m_bUseSSL=m_INIFile.m_nSSLMode;
}

//overriden QObject timer method
void ApplicationServer::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		m_nTimerID=-1;
		Status err;
		Restart(err);
		if(!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
			Stop();
		}
	}
}

void ApplicationServer::RestartDelayedThreadSafeSlot(int nRestartAfter)
{
	RestartDelayedPriv(nRestartAfter);
}

//thread safe
void ApplicationServer::OnBcpManager_RestoreCommandIssued(int nCallerID)
{
	//schedule for RESTART APP SERVER in 20 seconds:
	emit RestartDelayedThreadSafeSignal(20);
}

void ApplicationServer::LoadDbSettings(Status &pStatus,DbConnectionSettings &pDbSettings)
{
	//load DB SETTINGS:
	//------------------------------------------
	DbConnectionsList lstDbSettings;
	if(m_INIFile.m_strDBConnectionName.isEmpty())
	{
		pStatus.setError(1,"Database connection is not defined!");
		return;
	}

	QString strDbSettingsPath= QCoreApplication::applicationDirPath()+"/settings/database_connections.cfg";
	bool bOK=lstDbSettings.Load(strDbSettingsPath,MASTER_PASSWORD); //PASSWORD!???
	if(!bOK)
	{
		pStatus.setError(1,"Database setting file file is missing or corrupted!");
		return;
	}

	bOK=lstDbSettings.GetDbSettings(m_INIFile.m_strDBConnectionName,pDbSettings);
	if(!bOK)
	{
		pStatus.setError(1,"Database connection not found inside database settting file!");
		return;
	}
}

void ApplicationServer::InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections, int nMinDbConnections)
{
	//make Dbmanager, set to max conn:
	*pDbManager = new DbSqlManager;
	(*pDbManager)->Initialize(pStatus, ConnSettings,nMaxDbConnections,nMinDbConnections);
	if (!pStatus.IsOK()) return;

	CreateObjectDbManager();
	CheckDatabaseSchema(pStatus);
}


void ApplicationServer::CheckDatabaseSchema(Status &pStatus)
{
	//check if tables exist, if not create them
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql;
	QStringList lstTables = query.GetDbConnection()->tables();


	if (!lstTables.contains("MAIN_DATA", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: USER_DATA");

		//create table here
		strSql = "CREATE TABLE MAIN_DATA ("
			"MD_ID INTEGER not null,"
			"MD_DAT_LAST_MODIFIED TIMESTAMP not null,"
			"MD_DATABASE_VERSION INTEGER not null,"
			"constraint MAIN_DATA_PK primary key (MD_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "MAIN_DATA", "MD_ID");
		if (!pStatus.IsOK())return; 
	}

	if (!lstTables.contains("EMAIL_SERVERS", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: EMAIL_SERVERS");

		//create table here (inlcude port and http(s): https//everxconnect.com:9999
		strSql = "CREATE TABLE EMAIL_SERVERS ("
			"ESR_ID INTEGER NOT NULL,"
			"ESR_DAT_LAST_MODIFIED TIMESTAMP NOT NULL,"
			"ESR_HOST_ADDRESS VARCHAR(200)," 
			"constraint EMAIL_SERVERS_PK primary key (ESR_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "EMAIL_SERVERS", "ESR_ID");
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create unique index IND_EMAIL_UN on EMAIL_SERVERS (ESR_HOST_ADDRESS)",true);
		if (!pStatus.IsOK())return; 

	}


	if (!lstTables.contains("USER_DATA", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: USER_DATA");

		//create table here
		strSql = "CREATE TABLE USER_DATA ("
				 "USR_ID INTEGER NOT NULL,"
				 "USR_DAT_LAST_MODIFIED TIMESTAMP NOT NULL,"
				 "USR_EMAIL VARCHAR(100),"
				 "USR_PASSWORD VARCHAR(40),"
				 "USR_IS_CONFIRMED SMALLINT,"
				 "USR_CONFIRM_STRING VARCHAR(120),"
				 "USR_RECONFIRM_PASSWORD VARCHAR(40),"
				 "USR_CONFIRM_SENT TIMESTAMP,"
				 "USR_EMAIL_IN_STORAGE INTEGER,"
				 "USR_EMAIL_TOTAL_DOWNLOADED INTEGER,"
				 "USR_SMTP_SERVER VARCHAR(200),"
				 "USR_SMTP_USERNAME VARCHAR(200),"
				 "USR_SMTP_PASSWORD VARCHAR(200),"
				 "USR_SMTP_PORT INTEGER,"
				 "USR_SMTP_BCC_EMAIL_COPY VARCHAR(200),"
				 "USR_MULTI_REG_START TIMESTAMP,"
				 "USR_SENT_EMAILS_OK INTEGER,"
				 "USR_SENT_EMAILS_FAIL INTEGER,"
				 "USR_EMAIL_SERVER_ID INTEGER,"
				 "USR_ADDON_PASS VARCHAR(100),"
				 "USR_DAT_LAST_LOGIN TIMESTAMP,"
				 "USR_DAT_FIRST_LOGIN TIMESTAMP,"
				 "USR_DAT_LAST_RECEIPT_CHECK TIMESTAMP,"
				 "USR_APPLE_RECEIPT BLOB sub_type TEXT,"
				 "USR_ALLOW_LOGIN SMALLINT,"
				 "constraint USER_DATA_PK primary key (USR_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "USER_DATA", "USR_ID");
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create unique index IND_USR_EMAIL on USER_DATA (USR_EMAIL)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create unique index IND_USR_CONFIRM_STRING on USER_DATA (USR_CONFIRM_STRING)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_CI_USR_EMAIL_CONFIRM on USER_DATA (USR_EMAIL,USR_IS_CONFIRMED)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_USR_ALLOW_LOGIN on USER_DATA (USR_ALLOW_LOGIN)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_USR_DAT_LAST_RECEIPT_CHECK on USER_DATA (USR_DAT_LAST_RECEIPT_CHECK)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_USR_DAT_FIRST_LOGIN on USER_DATA (USR_DAT_FIRST_LOGIN)",true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "alter table USER_DATA add constraint USER_DATA_FK1 foreign key (USR_EMAIL_SERVER_ID) references EMAIL_SERVERS (ESR_ID) ON DELETE SET NULL",true);
		if (!pStatus.IsOK())return; 
	}

	if (!lstTables.contains("EMAILS", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: EMAILS");

		//create table here
		strSql = "CREATE TABLE EMAILS ("
			"BEM_ID INTEGER NOT NULL,"
			"BEM_DAT_LAST_MODIFIED TIMESTAMP NOT NULL,"
			"BEM_FK_USER_ID INTEGER NOT NULL,"
			"BEM_RECV_TIME TIMESTAMP,"
			"BEM_FROM VARCHAR(1000),"
			"BEM_TO VARCHAR(1000),"
			"BEM_CC VARCHAR(1000),"
			"BEM_BCC VARCHAR(1000),"
			"BEM_SUBJECT VARCHAR(1000),"
			"BEM_BODY BLOB sub_type TEXT,"
			"BEM_BODY_MIME BLOB sub_type TEXT,"
			"BEM_ORIGINATOR_EMAIL VARCHAR(1000),"
			"BEM_ORIGINATOR_NAME VARCHAR(1000),"
			"constraint EMAILS_PK primary key (BEM_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "EMAILS", "BEM_ID");
		if (!pStatus.IsOK())return; 
		strSql = "alter table EMAILS add constraint EMAILS_FK1 foreign key (BEM_FK_USER_ID)	references USER_DATA (USR_ID) ON DELETE CASCADE";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_BEM_RECV_TIME on EMAILS (BEM_RECV_TIME)",true);
		if (!pStatus.IsOK())return; 
	}


	if (!lstTables.contains("CLOUD_TEMPLATES", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: CLOUD_TEMPLATES");

		//create table here
		strSql = "CREATE TABLE CLOUD_TEMPLATES ("
			"CT_ID INTEGER NOT NULL,"
			"CT_DAT_LAST_MODIFIED TIMESTAMP NOT NULL,"
			"CT_FK_USER_ID INTEGER NOT NULL,"
			"CT_TYPE SMALLINT,"
			"CT_NAME VARCHAR(1000),"
			"CT_DATA BLOB sub_type TEXT,"
			"constraint CLOUD_TEMPLATES_PK primary key (CT_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "CLOUD_TEMPLATES", "CT_ID");
		if (!pStatus.IsOK())return; 
		strSql = "alter table CLOUD_TEMPLATES add constraint CLOUD_TEMPLATES_FK1 foreign key (CT_FK_USER_ID)	references USER_DATA (USR_ID) ON DELETE CASCADE";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_CT_TYPE on CLOUD_TEMPLATES (CT_TYPE)",true);
		query.Execute(pStatus, "create index IND_CI_CT_USER_TYPE on CLOUD_TEMPLATES (CT_FK_USER_ID,CT_TYPE)",true);
		//query.Execute(pStatus, "create index IND_CI_CT_USER_TYPE_NAME on CLOUD_TEMPLATES (CT_FK_USER_ID,CT_TYPE,CT_NAME)",true);
		if (!pStatus.IsOK())return; 
	}


	if (!lstTables.contains("USER_ACCESS", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: USER_ACCESS");

		//create table here
		strSql = "CREATE TABLE USER_ACCESS ("
			"USA_ID INTEGER NOT NULL,"
			"USA_DAT_LAST_MODIFIED TIMESTAMP NOT NULL,"
			"USA_EMAIL VARCHAR(100),"
			"USA_TYPE INTEGER,"
			"constraint USER_ACCESS_PK primary key (USA_ID))";
		query.Execute(pStatus, strSql,true);
		if (!pStatus.IsOK())return; 
		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "USER_ACCESS", "USA_ID");
		if (!pStatus.IsOK())return; 
		query.Execute(pStatus, "create index IND_USA_EMAIL on USER_ACCESS (USA_EMAIL)",true);
		if (!pStatus.IsOK())return; 
	}


	//append long varchars to enable Write method
	QStringList *lstAllLongVarCharFieldsSpecial =  DbTableDef::getAllLongVarCharFields();
	QStringList *lstAllLongVarCharFields =  DbSqlTableView::getAllLongVarCharFields();

	*lstAllLongVarCharFields = *lstAllLongVarCharFields+*lstAllLongVarCharFieldsSpecial;
	//lstAllLongVarCharFields->prepend("BEM_BODY");
	//lstAllLongVarCharFields->prepend("CT_DATA");


	//read database version from table, if <> then execute organize database....
	query.Execute(pStatus,"SELECT * FROM MAIN_DATA");
	if (!pStatus.IsOK())return; 
	DbRecordSet rowMainData;
	query.FetchData(rowMainData);
	if (rowMainData.getRowCount()!=1)
	{
		rowMainData.clear();
		rowMainData.addRow();
		rowMainData.setData(0,"MD_DATABASE_VERSION",MWREPLY_DB_VERSION);
		query.WriteData(pStatus,"MAIN_DATA",rowMainData);
		if (!pStatus.IsOK())return; 
	}

	if (rowMainData.getDataRef(0,"MD_DATABASE_VERSION").toInt()!=MWREPLY_DB_VERSION)
	{
		//reorganize database
		OrganizeDatabase(pStatus, &query, rowMainData.getDataRef(0,"MD_DATABASE_VERSION").toInt());
		if (!pStatus.IsOK())return; 

		//set new DB version
		rowMainData.setData(0,"MD_DATABASE_VERSION",MWREPLY_DB_VERSION);
		query.WriteData(pStatus,"MAIN_DATA",rowMainData);
		if (!pStatus.IsOK())return; 
	}


}


//upgrade database; for now nothing to do...
void ApplicationServer::OrganizeDatabase(Status &pStatus,DbSqlQuery *query, int nCurrentDatabaseVersion)
{
	if (nCurrentDatabaseVersion<=2)
	{
		QString strSQL= "ALTER TABLE EMAILS ADD BEM_BODY_MIME BLOB sub_type TEXT";
		query->Execute(pStatus, strSQL,true);
		if (!pStatus.IsOK())return; 
	}

	if (nCurrentDatabaseVersion<4)
	{
		QString strSQL= "ALTER TABLE USER_DATA ADD USR_SENT_EMAILS_OK INTEGER";
		query->Execute(pStatus, strSQL,true);
		if (!pStatus.IsOK())return; 

		strSQL= "ALTER TABLE USER_DATA ADD USR_SENT_EMAILS_FAIL INTEGER";
		query->Execute(pStatus, strSQL,true);
		if (!pStatus.IsOK())return; 
	}

	if (nCurrentDatabaseVersion<5)
	{
		QString strSQL= "ALTER TABLE USER_DATA ADD USR_EMAIL_SERVER_ID INTEGER";
		query->Execute(pStatus, strSQL,true);
		if (!pStatus.IsOK())return; 

		query->Execute(pStatus, "alter table USER_DATA add constraint USER_DATA_FK1 foreign key (USR_EMAIL_SERVER_ID) references EMAIL_SERVERS (ESR_ID) ON DELETE SET NULL",true);
		if (!pStatus.IsOK())return; 

	}

	if (nCurrentDatabaseVersion<6)
	{
		QString strSQL= "ALTER TABLE USER_DATA ADD USR_ADDON_PASS VARCHAR(100)";
		query->Execute(pStatus, strSQL,true);
		if (!pStatus.IsOK())return; 
	}

	if (nCurrentDatabaseVersion<7)
	{
		QString strSQL= "ALTER TABLE USER_DATA ADD USR_DAT_LAST_LOGIN TIMESTAMP,ADD USR_DAT_FIRST_LOGIN TIMESTAMP,ADD USR_DAT_LAST_RECEIPT_CHECK TIMESTAMP,ADD USR_APPLE_RECEIPT BLOB sub_type TEXT,ADD USR_ALLOW_LOGIN SMALLINT";
		query->Execute(pStatus, strSQL,true);
		if (!pStatus.IsOK())return; 
		query->Execute(pStatus, "create index IND_USR_ALLOW_LOGIN on USER_DATA (USR_ALLOW_LOGIN)",true);
		if (!pStatus.IsOK())return; 
		query->Execute(pStatus, "create index IND_USR_DAT_LAST_RECEIPT_CHECK on USER_DATA (USR_DAT_LAST_RECEIPT_CHECK)",true);
		if (!pStatus.IsOK())return; 
		query->Execute(pStatus, "create index IND_USR_DAT_FIRST_LOGIN on USER_DATA (USR_DAT_FIRST_LOGIN)",true);
		if (!pStatus.IsOK())return; 

		//set all existing account to permanent test user:
		strSQL= "UPDATE USER_DATA SET USR_ALLOW_LOGIN=2, USR_DAT_LAST_LOGIN=CURRENT_TIMESTAMP, USR_DAT_FIRST_LOGIN=CURRENT_TIMESTAMP";
		query->Execute(pStatus, strSQL,true);
	}


}


void ApplicationServer::CreateNewDb(Status &pStatus,DbConnectionSettings &pDbSettings)
{
	QString strSettingsPath=QCoreApplication::applicationDirPath()+"/settings/database_connections.cfg";
	QString strDbName="DATA";
	QString strTargetDataFile=QCoreApplication::applicationDirPath();
	QString strConnection="DATA";
	m_INIFile.m_strDBConnectionName=strConnection;
	m_INIFile.Save(m_strIniFilePath);
	DbConnectionSetup::SetupAppServerDatabase(pStatus,"SYSDBA","masterkey",DBTYPE_FIREBIRD,strDbName,strConnection,strTargetDataFile);
	if (!pStatus.IsOK())
		return;

	pDbSettings.m_strCustomName="DATA";
	pDbSettings.m_strDbName=strTargetDataFile+"/DATA.FDB";
	pDbSettings.m_strDbUserName="SYSDBA";
	pDbSettings.m_strDbPassword="masterkey";
	pDbSettings.m_strDbType=DBTYPE_FIREBIRD;
	DbConnectionSetup::CreateDatabaseConnection(pStatus,strSettingsPath,pDbSettings);
}



void ApplicationServer::CreateObjectDbManager()
{
	m_DbObjectManager=NULL;

	//make instance of proper DboManager:
	if(g_DbManager->GetDbType()==DBTYPE_ORACLE)
		m_DbObjectManager= new DbObjectManager_Oracle(g_DbManager->GetDbSettings());
	else if (g_DbManager->GetDbType()==DBTYPE_MYSQL)
		m_DbObjectManager= new DbObjectManager_MySQL(g_DbManager->GetDbSettings());
	else if (g_DbManager->GetDbType()==DBTYPE_FIREBIRD)
		m_DbObjectManager= new DbObjectManager_FireBird(g_DbManager->GetDbSettings());
	else
		Q_ASSERT_X(false,"DBO ORGANIZER", "Database object manager doesn't exists for specified base");
}

void ApplicationServer::DestroyObjectDbManager()
{
	if (m_DbObjectManager)
	{
		delete m_DbObjectManager;
		m_DbObjectManager=NULL;
	}
}

void ApplicationServer::OnFillDatabaseAtStartForTest()
{
	g_pAppSelfTester->AddUserAccountsForTest();
	m_INIFile.Save(m_strIniFilePath);
}







	


