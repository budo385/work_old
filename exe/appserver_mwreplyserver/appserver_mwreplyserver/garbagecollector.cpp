#include "garbagecollector.h"
#include "common/common/threadid.h"

#include "common/common/logger.h"
extern Logger g_Logger;
#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;
#include "db/db/dbsqlmanager.h"
extern DbSqlManager	*g_DbManager;

GarbageCollector::GarbageCollector(HTTPServer *pHttpServer)
{
	m_HttpServer=pHttpServer;
}

GarbageCollector::~GarbageCollector()
{

}


//start every X minutes
void GarbageCollector::ExecuteTask(int nCallerID,int nTaskID, QString strData)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());

	Status err;
	g_ServiceHandler->CleanUpOldAccounts(err);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,err.getErrorCode(),"Error while cleaning expired user accounts: " + err.getErrorText());
	}

	g_ServiceHandler->SyncEmailsToLBServer(err);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,err.getErrorCode(),"Error while cleaning expired user accounts: " + err.getErrorText());
	}

	g_DbManager->CheckConnections();
}


