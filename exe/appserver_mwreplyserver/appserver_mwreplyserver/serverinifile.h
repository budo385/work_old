#ifndef SERVERINIFILE_H
#define SERVERINIFILE_H

#include "common/common/inifile.h"
#include <QFileInfo>



/*!
	\class ServerIniFile
	\brief INI file reader for app. server
	\ingroup AppServer

	Application server INI file reader


*/
class ServerIniFile
{
public:
	ServerIniFile();
	~ServerIniFile();

	
	void Clear();
	bool Load(QString strFile, bool bCreateIfNotExists=true);
	bool Save(QString strFile);

public:

	//data stored in the INI file
	QString m_strAppServerIPAddress;	// IP address bind (def is 0.0.0.0)
	int		m_nAppServerPort;			// port number
	int		m_nMaxConnections;			// maximum connections that app. server will accept
	int		m_nMaxDbConnections;		// maximum Database connections (if 0) then unlimited
	int		m_nMinDbConnections;		// min Database connections that are always open
	QString	m_strServiceDescriptor;		// app name
	int		m_nAutoStartService;		//
	QString m_strDBConnectionName;		// NAME OF DB CONN
	QString m_strHTTPRegisterAddress;	// start of HTTP confirmation url: http://everxconnect.com:4567/register
	QString m_strHostDomain;
	int		m_nLockRegistration;
	int		m_nLockWriteDatabase;		//for migration
	int		m_nExpireEmailAccountDays;		//days after email is deleted from DB after test or paid period expire
	int		m_nNumCloudTestingPeriodDays;	//test period for new users, fixed for all

	

	QString m_strHostUniqueID;
	QString m_strLoadBalanceServerURL;
	
	//SELFTEST
	QString m_strStartUserNameAdd;
	QString m_strStartPassWordAdd;
	int m_nStartCounterAdd;
	int m_nCountUserAdd;
	QString m_strLastUserNameAdded;
	QString m_strNextTestTime;

	//SMTP
	QString m_strMailServer;			
	QString m_strMailServerAddress;	
	QString m_strMailServerUserName;	
	QString m_strMailServerPassword;	
	int		m_nMailServerPort;	

	//SMTP 2
	QString m_strUserSMTPMailServer;			
	QString m_strUserSMTPMailServerAddress;	
	QString m_strUserSMTPMailServerUserName;	
	QString m_strUserSMTPMailServerPassword;	
	int		m_nUserSMTPMailServerPort;	

	//EMAIL NOTIFICATOR
	QString m_strNotificatorServerIP;			
	int m_nNotificatorServerPort;	


	//SSL
	int		m_nSSLMode;					// accept only SSL(https) connections
	QString m_strSSLCertificateDir;		// path to the SSL certificates files (server.cert & server.pkey)

	//LOGGER
	int m_nLogLevel;					
	int m_nLogMaxSize;					
	int m_nLogBufferSize;				
		
	//BACKUP:
	int m_nBackupFreq;		
	QString m_strBackupPath;		
	QString m_datBackupLastDate;		
	QString m_strRestoreOnNextStart;		
	QString m_strBackupTime;
	int m_nBackupDay;
	int m_nDeleteOldBackupsDay;

	//mwReply
	int m_nTimeToKeepEmailsHr;
	int m_nMaxEmailCnt;
	int m_nRegistrationTimeOutHr;
	int m_nScanFrequency;

	
	//Apple check url
	QString m_strAppleReceiptValidationUrl;
	QString m_strAppleReceiptPasswordCheck;

protected:
	IniFile m_objIni;
	bool CheckValues();
	bool SaveDefaults(QString strFile);

};

#endif //SERVERINIFILE_H