#ifndef SERVERINIFILE_H
#define SERVERINIFILE_H

#include "common/common/inifile.h"
#include <QFileInfo>



/*!
	\class ServerIniFile
	\brief INI file reader for app. server
	\ingroup AppServer

	Application server INI file reader


*/
class ServerIniFile
{
public:
	ServerIniFile();
	~ServerIniFile();

	ServerIniFile(const ServerIniFile &other){ operator = (other); }
		
	void Clear();
	bool Load(QString strFile, bool bCreateIfNotExists=true);
	bool Save(QString strFile);

public:

	//data stored in the INI file
	QString m_strAppServerIPAddress;	// IP address bind (def is 0.0.0.0)
	int		m_nAppServerPort;			// port number
	int		m_nMaxConnections;			// maximum connections that app. server will accept
	QString	m_strServiceDescriptor;		// app name
	int		m_nAutoStartService;		//
	QString m_strDBConnectionName;		// NAME OF DB CONN
	
	QString m_strServerName;			
	QString m_strMailServer;			
	QString m_strMailAdminAddress;		
	QString m_strMailServerAddress;	
	QString m_strMailServerUserName;	
	QString m_strMailServerPassword;	

	QString	m_strSC_ClientSetupPrefix;
	QString	m_strSC_ServerSetupPrefix;
	QString	m_strSVN_DIR;				//build source, code location under Subversion control (svn tools must be installed so that svn update works from command line) e.g. D:\SokratesXP
	QString	m_strSVN_User;				//subversion user
	QString	m_strSVN_Pass;				//subversion pass
	QString	m_strDEPLOY_DIR;			//deploy directory, where deployment/setup files will be created, e.g. D:\Deploy
	QString	m_strVisualPath;			//visual studio path for environment var batch setup, e.g.: C:\Program Files\Microsoft Visual Studio 10.0\Common7\Tools\vsvars32.bat
	QString	m_strInnoSetupPath;			//Inno setup path for creating setup files: C:\Program Files\Inno Setup 5\Compil32.exe
	QString	m_strServerFarmPath;		//server farm path: only for Bugatti, where there are more servers installed (50): d:\Sokrates Servers

	//SSL
	int		m_nSSLMode;					// accept only SSL(https) connections
	QString m_strSSLCertificateDir;		// path to the SSL certificates files (server.cert & server.pkey)

	//LOGGER
	int m_nLogLevel;					
	int m_nLogMaxSize;					
	int m_nLogBufferSize;				
		
	//BACKUP:
	int m_nBackupFreq;		
	QString m_strBackupPath;		
	QString m_datBackupLastDate;		
	QString m_strRestoreOnNextStart;		
	QString m_strBackupTime;
	int m_nBackupDay;

	
protected:
	IniFile m_objIni;
	bool CheckValues();
	bool SaveDefaults(QString strFile);

};

#endif //SERVERINIFILE_H
