#include "serverinifile.h"
#include "common/common/logger.h"

ServerIniFile::ServerIniFile()
{
}

ServerIniFile::~ServerIniFile()
{
}

void ServerIniFile::Clear()
{
	//m_lstConnections.clear();
}

//loads, creates if not exists, cheks values
bool ServerIniFile::Load(QString strFile, bool bCreateIfNotExists)
{
	Clear();

	if(!QFileInfo(strFile).exists()&& bCreateIfNotExists)
		{return SaveDefaults(strFile);}

	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	// read INI values
	m_objIni.GetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.GetValue("Main", "AppServerPort",		m_nAppServerPort, 1111);
	m_objIni.GetValue("Main", "MaxConnections",		m_nMaxConnections, 100);
	m_objIni.GetValue("Main", "Service Descriptor",	m_strServiceDescriptor);
	m_objIni.GetValue("Main", "AutoStartService",	m_nAutoStartService,1);
	m_objIni.GetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.GetValue("Main", "Server Name",		m_strServerName,"DeployServer");
	m_objIni.GetValue("Main", "Mail SMTP Server",	m_strMailServer,"mail.t-com.hr");
	m_objIni.GetValue("Main", "Admin Mail Address",	m_strMailAdminAddress,"trumbic@sokrates.hr");
	m_objIni.GetValue("Main", "Server Mail Address",m_strMailServerAddress,"seronimo@sokrates.hr");
	m_objIni.GetValue("Main", "Server Mail User",	m_strMailServerUserName);
	m_objIni.GetValue("Main", "Server Mail Password",m_strMailServerPassword);
	
	m_objIni.GetValue("Setup Data", "SC Client Setup Prefix",m_strSC_ClientSetupPrefix);
	m_objIni.GetValue("Setup Data", "SC Server Setup Prefix",m_strSC_ServerSetupPrefix);
	m_objIni.GetValue("Setup Data", "SVN Local Directory",m_strSVN_DIR);
	m_objIni.GetValue("Setup Data", "SVN User",m_strSVN_User);
	m_objIni.GetValue("Setup Data", "SVN Password",m_strSVN_Pass);
	m_objIni.GetValue("Setup Data", "Deploy Local Directory",m_strDEPLOY_DIR);
	m_objIni.GetValue("Setup Data", "Visual Studio Path",m_strVisualPath);
	m_objIni.GetValue("Setup Data", "Inno Studio Path",m_strInnoSetupPath);
	m_objIni.GetValue("Setup Data", "Server Farm Path",m_strServerFarmPath);

	
	
	m_objIni.GetValue("SSL", "SSLMode",			m_nSSLMode, 0);
	m_objIni.GetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);

	m_objIni.GetValue("Logging", "Log Level",												m_nLogLevel,Logger::LOG_LEVEL_BASIC);
	m_objIni.GetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize,10);
	m_objIni.GetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize,64);
	
	/*
	m_objIni.GetValue("Backup", "Backup Frequency",		m_nBackupFreq,2);
	m_objIni.GetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.GetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.GetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.GetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.GetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
	*/

	return CheckValues();
}

bool ServerIniFile::Save(QString strFile)
{
	m_objIni.SetPath(strFile);

	//fill the data inside
	m_objIni.RemoveSection("Main");		//cleanup existing data
	m_objIni.RemoveSection("Logging");	
	//m_objIni.RemoveSection("Backup");	
	m_objIni.RemoveSection("SSL");		

	// write INI values
	m_objIni.SetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.SetValue("Main", "AppServerPort",		m_nAppServerPort);
	m_objIni.SetValue("Main", "MaxConnections",		m_nMaxConnections);
	m_objIni.SetValue("Main", "Service Descriptor",	m_strServiceDescriptor);
	m_objIni.SetValue("Main", "AutoStartService",	m_nAutoStartService);
	m_objIni.SetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.SetValue("Main", "Server Name",		m_strServerName);
	m_objIni.SetValue("Main", "Mail SMTP Server",	m_strMailServer);
	m_objIni.SetValue("Main", "Admin Mail Address",	m_strMailAdminAddress);
	m_objIni.SetValue("Main", "Server Mail Address",m_strMailServerAddress);
	m_objIni.SetValue("Main", "Server Mail User",	m_strMailServerUserName);
	m_objIni.SetValue("Main", "Server Mail Password",m_strMailServerPassword);


	m_objIni.SetValue("Setup Data", "SC Client Setup Prefix",m_strSC_ClientSetupPrefix);
	m_objIni.SetValue("Setup Data", "SC Server Setup Prefix",m_strSC_ServerSetupPrefix);
	m_objIni.SetValue("Setup Data", "SVN Local Directory",m_strSVN_DIR);
	m_objIni.SetValue("Setup Data", "SVN User",m_strSVN_User);
	m_objIni.SetValue("Setup Data", "SVN Password",m_strSVN_Pass);
	m_objIni.SetValue("Setup Data", "Deploy Local Directory",m_strDEPLOY_DIR);
	m_objIni.SetValue("Setup Data", "Visual Studio Path",m_strVisualPath);
	m_objIni.SetValue("Setup Data", "Inno Studio Path",m_strInnoSetupPath);
	m_objIni.SetValue("Setup Data", "Server Farm Path",m_strServerFarmPath);


	m_objIni.SetValue("SSL", "SSLMode",				m_nSSLMode);
	m_objIni.SetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);

	m_objIni.SetValue("Logging", "Log Level",												m_nLogLevel);
	m_objIni.SetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize);
	m_objIni.SetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize);
/*
	m_objIni.SetValue("Backup", "Backup Frequency",		m_nBackupFreq);
	m_objIni.SetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.SetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.SetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.SetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.SetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
*/
	return m_objIni.Save();
}



bool ServerIniFile::CheckValues()
{
	if(m_nMaxConnections<=0) return false;
	if(m_nSSLMode<0 || m_nSSLMode>1) return false;
	if(m_nAutoStartService<0 || m_nAutoStartService>1) return false;
	if(m_nAppServerPort<0 || m_nAppServerPort>65536) return false;

	if(m_nLogBufferSize<0) return false;

	//reset to default log level:
	if(m_nLogLevel<0)m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	if(m_nLogLevel>Logger::LOG_LEVEL_DEBUG)m_nLogLevel=Logger::LOG_LEVEL_DEBUG;

	return true;
}



bool ServerIniFile::SaveDefaults(QString strFile)
{
	m_strAppServerIPAddress="0.0.0.0";
	m_nAppServerPort=9999;
	m_nMaxConnections=1000;
	m_nSSLMode=1;
	m_nAutoStartService=1;

	m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	m_nLogMaxSize=10;				
	m_nLogBufferSize=64;	
	m_strServerName="DeployServer";
	m_strMailServer="mail.t-com.hr";
	m_strMailAdminAddress="trumbic@sokrates.hr";
	m_strMailServerAddress="seronimo@sokrates.hr";

	m_strSC_ServerSetupPrefix="setup_sasc_";
	m_strSC_ClientSetupPrefix="setup_scoc_";

	m_strSVN_DIR="D:\\Work\\SokratesXP";
	m_strDEPLOY_DIR="D:\\Deploy";
	m_strVisualPath="C:\\Program Files\\Microsoft Visual Studio 10.0\\Common7\\Tools\\vsvars32.bat";
	m_strSVN_User="DS";
	m_strSVN_Pass="";

	return Save(strFile);
}
