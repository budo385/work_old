#ifndef APPLICATIONSERVER_H
#define APPLICATIONSERVER_H


#include <QObject>
#include "serverinifile.h"
#include "resthttpserver_local.h"
#include "restrpcdispatcher.h"
#include "trans/trans/httpserver.h"
#include "common/common/backgroundtaskexecutor.h"
#include "batchmanagertaskexecutor.h"
#include "trans/trans/htmlhttpserver.h"

#define ROOT_USER "root"
#define ROOT_PASS "Konj1234"


class ApplicationServer: public QObject
{
	Q_OBJECT 

public:

	ApplicationServer(QObject *parent=NULL);
	~ApplicationServer();

	//main API
	void Start(Status &pStatus);
	void Stop();
	void Restart(Status &pStatus);
	void RestartDelayed(int nReStartAfterSec);
	void StopDelayed(int nStopAfterSec=0);
	bool IsRunning(){return m_bIsRunning;}
	void SaveSettings();
	ServerIniFile * GetSettings(){return &m_INIFile;}

	HTTPContext GetThreadContext(int nThreadID){return SokratesHttpServer.GetThreadContext(nThreadID);};

	//note: timers can not restart from another thread, use signals
protected slots:
	void RestartDelayedThreadSafeSlot(int);
	void StopThreadSafeSlot();
	void OnBcpManager_RestoreCommandIssued(int);

protected:
	void timerEvent(QTimerEvent *event); //overriden QObject timer method

signals:
	void RestartDelayedThreadSafeSignal(int);
	void StopThreadSafeSignal();

private:
	void ClearPointers();
	void RestartDelayedPriv(int nReStartAfterSec);
	void LoadINI(Status &pStatus);
	void LoadNetSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings);
	//void LoadDbSettings(Status &pStatus,DbConnectionSettings &pDbSettings);
	//void InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections=0);


	//TRANSPORT
	HtmlHTTPServer					*m_HtmlHTTPServer;
	RestHTTPServer_Local			*m_RestHTTPServer;
	RestRpcDispatcher				*m_RestServiceRpcDispatcher;
	HTTPServer						SokratesHttpServer;
	BatchManagerTaskExecutor		*m_BatTask;


	//INI
	ServerIniFile m_INIFile;
	QString m_strIniFilePath;

	bool m_bIsRunning;
	int m_nTimerID;
	QMutex m_Mutex;					///< mutex

};


#endif // APPLICATIONSERVER_H
