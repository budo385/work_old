#include "applicationserver.h"
#include "common/common/datetimehandler.h"
#include "common/common/logger.h"
#include "trans/trans/tcphelper.h"
#include "servicehandler.h"
#include "common/common/threadid.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "db_core/db_core/dbconnsettings.h"
#include "usersessionmanager.h"
#include "batchmanager.h"
//-----------------------------------------------
//	 SERVER GLOBALS:
//------------------------------------------------

//SPECIALS:
Logger							g_Logger;
ApplicationServer				*g_AppServer=NULL;
ServiceHandler					*g_ServiceHandler=NULL;
UserSessionManager				*g_UserSessionManager=NULL;
BatchManager					*g_BatchManager=NULL;


/*!
	Constructor
*/
ApplicationServer::ApplicationServer(QObject *parent)
:QObject(parent) //ServerThread(parent),SokratesHttpServer(this)
{
	//DateTimeHandler::s_bUseUTCTimeZone=true;//SERVER times and DB are set to the UTC0 timezone!!!
	ClearPointers();
	m_nTimerID=-1;
	m_bIsRunning=false;
	connect(this,SIGNAL(RestartDelayedThreadSafeSignal(int)),this,SLOT(RestartDelayedThreadSafeSlot(int)),Qt::QueuedConnection);
	connect(this,SIGNAL(StopThreadSafeSignal()),this,SLOT(StopThreadSafeSlot()),Qt::QueuedConnection);
	ApplicationLogger::setApplicationLogger(&g_Logger);
}

void ApplicationServer::ClearPointers()
{
	m_RestHTTPServer=NULL;
	m_RestServiceRpcDispatcher=NULL;
	g_ServiceHandler=NULL;
	m_BatTask=NULL;
	g_BatchManager=NULL;

	//g_DbManager=NULL;
	m_HtmlHTTPServer = NULL;
	//m_UserStorageHTTPServer=NULL;

	m_strIniFilePath="";
}

/*!
	Destructor: kill all objects
*/
ApplicationServer::~ApplicationServer()
{
	if (IsRunning())
		Stop();
}

void ApplicationServer::Restart(Status &pStatus)
{
	//QMutexLocker locker(&m_Mutex); //lock acces
	Stop();
	Start(pStatus);
	//exit if error:

}

void ApplicationServer::RestartDelayed(int nReStartAfterSec)
{
	QMutexLocker locker(&m_Mutex);							//lock acces
	emit RestartDelayedThreadSafeSignal(nReStartAfterSec);	//ensures that appserver thread executes this!
}

//ignore parameter: do it now!
void ApplicationServer::StopDelayed(int nStopAfterSec)
{
	QMutexLocker locker(&m_Mutex);	//lock acces
	emit StopThreadSafeSignal();	//ensures that appserver thread executes this!
}

void ApplicationServer::StopThreadSafeSlot()
{
	Stop();
}

void ApplicationServer::RestartDelayedPriv(int nReStartAfterSec)
{
	//already in restart mode, ignore
	if (m_nTimerID!=-1)
	{
		return;
	}
	//this is fine opportunity to fire warning to all clients->
	m_nTimerID=startTimer(nReStartAfterSec*1000);
}

void ApplicationServer::Stop()
{
	QMutexLocker locker(&m_Mutex); //lock acces
	
	//if (!m_bIsRunning)	return;

	//stop server:
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopping Http server");
	SokratesHttpServer.StopServer();
	SokratesHttpServer.ClearRequestHandlerList();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Http server stopped");

	//save log & all rest:
	m_INIFile.m_nLogLevel=g_Logger.GetLogLevel();
	m_INIFile.m_nLogMaxSize=g_Logger.GetMaxLogSize();
	m_INIFile.m_nLogBufferSize=g_Logger.GetMemoryBufferSize();
	m_INIFile.Save(m_strIniFilePath);

	//destroy objects:
	
	m_BatTask->Stop();
	if(m_BatTask!=NULL)						delete m_BatTask;

	if(m_HtmlHTTPServer != NULL)			delete m_HtmlHTTPServer;
	//if(m_UserStorageHTTPServer != NULL)	delete m_UserStorageHTTPServer;
	if(m_RestHTTPServer!=NULL)				delete m_RestHTTPServer;
	if(m_RestServiceRpcDispatcher!=NULL)	delete m_RestServiceRpcDispatcher;
	if(g_ServiceHandler!=NULL)				delete g_ServiceHandler;
	//if(g_DbManager!=NULL)					delete g_DbManager;
	if(g_BatchManager!=NULL)				delete g_BatchManager;
	

	m_bIsRunning=false;
	ClearPointers();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_STOP);
	g_Logger.FlushFileLog(); 
}



/*!
	Start App. server
*/
void ApplicationServer::Start(Status &pStatus)
{
	QMutexLocker locker(&m_Mutex); //lock acces
	if (m_bIsRunning)	return;

	HTTPServerConnectionSettings ServerSettings;
	//DbConnectionSettings DbConnSettings;

	//----------------------------------------------
	//LOAD INI files:
	//----------------------------------------------
	LoadINI(pStatus);
	if(!pStatus.IsOK()) return;

	//----------------------------------------------
	//Start logger 
	//----------------------------------------------
	if (m_INIFile.m_nLogLevel>LoggerAbstract::LOG_LEVEL_NONE)
	{
		g_Logger.EnableFileLogging(QCoreApplication::applicationDirPath()+"/settings/appserver.log",m_INIFile.m_nLogBufferSize,m_INIFile.m_nLogMaxSize);
		g_Logger.EnableConsoleLogging();
	}
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);

	//----------------------------------------------
	//LOAD NET & DB files:
	//----------------------------------------------
	LoadNetSettings(pStatus,ServerSettings);
	if(!pStatus.IsOK()) return;
	//LoadDbSettings(pStatus,DbConnSettings);
	//if(!pStatus.IsOK()) return;

	//----------------------------------------------
	//init Database
	//----------------------------------------------
	//InitDatabaseConnection(pStatus,&g_DbManager,DbConnSettings);
	//if(!pStatus.IsOK())return;
	//g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_DATABASE_CONN_SUCESS,DbConnSettings.m_strDbName+";"+DbConnSettings.m_strDbHostName);


	//----------------------------------------------
	//Launch Business & Private sets/init pointers:
	//----------------------------------------------
	g_UserSessionManager= new UserSessionManager();
	g_ServiceHandler = new ServiceHandler();


	//----------------------------------------------
	//Start Garbage Collector in separate thread (GBC controls HTTP server):
	//----------------------------------------------
	g_BatchManager = new BatchManager;
	m_BatTask = new BatchManagerTaskExecutor(g_BatchManager,this);
	//m_BatTask->Start();


	//----------------------------------------------
	//Launch all server handlers:
	//----------------------------------------------
	//TcpHelper::ResolveWebAddressNetworkInterface(ServerSettings.m_strServerIP.toLower(),ServerSettings.m_nPort,ServerSettings.m_bUseSSL);

	m_RestServiceRpcDispatcher		= new RestRpcDispatcher();
	m_RestHTTPServer				= new RestHTTPServer_Local(m_RestServiceRpcDispatcher);
	m_HtmlHTTPServer				= new HtmlHTTPServer;
	//m_UserStorageHTTPServer		= new UserStorageHTTPServer_Sokrates();


	SokratesHttpServer.ClearRequestHandlerList();
	SokratesHttpServer.RegisterRequestHandler(m_RestHTTPServer);
	//m_HtmlHTTPServer->m_nDefaultSocketTimeOut=20;						//set timeout to 20min
	SokratesHttpServer.RegisterRequestHandler(m_HtmlHTTPServer);

	SokratesHttpServer.InitServer(ServerSettings,false);
	SokratesHttpServer.StartListen(pStatus);

	if (pStatus.IsOK())
	{
		QString appParams=ServerSettings.m_strServerIP+";"+QVariant(ServerSettings.m_nPort).toString()+";"+QVariant(ServerSettings.m_bUseSSL).toString();
		appParams+=";1;"+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString();
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_START,appParams);
		m_bIsRunning=true;
	}
}



void ApplicationServer::SaveSettings()
{
	QMutexLocker locker(&m_Mutex);			//lock acces
	m_INIFile.Save(m_strIniFilePath);
}

void ApplicationServer::LoadINI(Status &pStatus)
{
	//load ini file & server settings:
	//------------------------------------------
	m_strIniFilePath= QCoreApplication::applicationDirPath()+"/settings/app_server.ini";
	bool bOK=m_INIFile.Load(m_strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,"INI file is missing,corrupted or has invalid values!");
		return;
	}

	QFileInfo ssl(m_INIFile.m_strSSLCertificateDir+"/server.cert");
	if (!ssl.exists())
	{
		m_INIFile.m_strSSLCertificateDir=QCoreApplication::applicationDirPath()+"/settings";
	}
}

void ApplicationServer::LoadNetSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings)
{
	pServerSettings.m_strCertificateFile=m_INIFile.m_strSSLCertificateDir+"/server.cert";
	pServerSettings.m_strPrivateKeyFile=m_INIFile.m_strSSLCertificateDir+"/server.pkey";
	pServerSettings.m_nPort=m_INIFile.m_nAppServerPort;
	if (QFile::exists(m_INIFile.m_strSSLCertificateDir+"/server.ca-bundle"))
		pServerSettings.m_strCACertificateFileBundle=m_INIFile.m_strSSLCertificateDir+"/server.ca-bundle";

	if(m_INIFile.m_strAppServerIPAddress.isEmpty())
		pServerSettings.m_strServerIP="0.0.0.0"; //listen on all ports
	else
		pServerSettings.m_strServerIP=m_INIFile.m_strAppServerIPAddress;
	pServerSettings.m_bUseSSL=m_INIFile.m_nSSLMode;

}


//overriden QObject timer method
void ApplicationServer::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		m_nTimerID=-1;
		Status err;
		Restart(err);
		if(!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
			Stop();
		}
	}
}



void ApplicationServer::RestartDelayedThreadSafeSlot(int nRestartAfter)
{
	RestartDelayedPriv(nRestartAfter);
}




//thread safe
void ApplicationServer::OnBcpManager_RestoreCommandIssued(int nCallerID)
{
	//schedule for RESTART APP SERVER in 20 seconds:
	emit RestartDelayedThreadSafeSignal(20);
}

/*
void ApplicationServer::LoadDbSettings(Status &pStatus,DbConnectionSettings &pDbSettings)
{
	//load DB SETTINGS:
	//------------------------------------------
	DbConnectionsList lstDbSettings;
	if(m_INIFile.m_strDBConnectionName.isEmpty())
	{
		pStatus.setError(1,"Database connection is not defined!");
		return;
	}

	QString strDbSettingsPath= QCoreApplication::applicationDirPath()+"/settings/database_connections.cfg";
	bool bOK=lstDbSettings.Load(strDbSettingsPath,MASTER_PASSWORD); //PASSWORD!???
	if(!bOK)
	{
		pStatus.setError(1,"Database setting file file is missing or corrupted!");
		return;
	}

	bOK=lstDbSettings.GetDbSettings(m_INIFile.m_strDBConnectionName,pDbSettings);
	if(!bOK)
	{
		pStatus.setError(1,"Database connection not found inside database settting file!");
		return;
	}

}
*/
/*
void ApplicationServer::InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections)
{
	//make Dbmanager, set to max conn:
	*pDbManager = new DbSqlManager;
	(*pDbManager)->Initialize(pStatus, ConnSettings,nMaxDbConnections);
	if (!pStatus.IsOK())return;
}
*/