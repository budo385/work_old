#ifndef BATCHMANAGER_H
#define BATCHMANAGER_H

#include "common/common/backgroundtaskinterface.h"
#include <QObject>
#include <QMutex>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "trans/trans/ftpclient.h"


//#define SVN_USER "DS"
//#define SVN_PASS "stardida10"
#define FERRARI_DEPLOY_REST_IP "188.138.84.245"
#define FERRARI_DEPLOY_REST_PORT 6666

class BatchManager : public QObject, public BackgroundTaskInterface 
{
	Q_OBJECT

public:

	BatchManager();

	enum Tasks
	{
		TASK_COMMUNICATORPRIVATEBUILD,
		TASK_COMMUNICATORPUBLICBUILD,
		TASK_EVERPICSSERVERBUILD,
		TASK_EVERDOCSSERVERBUILD,

		TASK_SERVERFARM_DEPLOY_NEW_FILES,
		TASK_SERVERFARM_UPDATE_PRIVATE_SERVERS,
		TASK_SERVERFARM_UPDATE_PUBLIC_SERVERS,
		/*
		TASK_FERRARI_DEPLOY_NEW_FILES,
		TASK_FERRARI_UPDATE_PRIVATE_SERVERS,
		TASK_FERRARI_UPDATE_PUBLIC_SERVERS,
		TASK_FERRARI_DEPLOY_NEW_FILES_REMOTECALL,
		TASK_FERRARI_UPDATE_PRIVATE_SERVERS_REMOTECALL,
		TASK_FERRARI_UPDATE_PUBLIC_SERVERS_REMOTECALL,
		*/

		TASK_BUILDALL,
		TASK_MAILEGANTBUILD

	};

	enum FTPLocations
	{
		FTP_SOKRATES_HR_PUBLIC_DIR,
		FTP_SOKRATES_HR_PRIVATE_DIR,
		//FTP_AKAMAI,
		FTP_SOKRATES_COMMUNICATOR_DOWNLOAD,
		FTP_SOKRATES_CH_VERSION_DIR,
		FTP_SOKRATES_HR_FFH,
		FTP_SOKRATES_HR_JOHN
	};


	void ExecuteTask(int nCallerID,int nTaskID,QString strData);
	void StartBkgTask(int nTaskID){emit SignalProcessTask(nTaskID);}
	void GetTasks(DbRecordSet &RetTasks);
	bool IsTaskRunning();

signals:
	void SignalProcessTask(int);

private:
	QMutex mutex;

	QHash<int,FTPConnectionSettings> lstFTPconections;

	void ExecuteTask_CommunicatorPrivateBuild(bool bSkipUpload=false);
	void ExecuteTask_CommunicatorPublicBuild();
	void ExecuteTask_FFHServerBuild(bool bSkipUpload=false);
	void ExecuteTask_JohnerverBuild(bool bSkipUpload=false);
	void ExecuteTask_MailegantBuild(bool bSkipUpload=false);
	void ExecuteTask_BuildAll_NoUpload();

	void ExecuteTask_ServerFarmDeployNewFiles();
	void ExecuteTask_ServerFarmUpdatePrivateServers();
	void ExecuteTask_ServerFarmUpdatePublicServers();
	
	/*
	void ExecuteTask_FerrariDeployNewFiles();
	void ExecuteTask_FerrariUpdatePrivateServers();
	void ExecuteTask_FerrariUpdatePublicServers();
	*/

	/*
	void ExecuteTask_FerrariDeployNewFilesRemoteCall();
	void ExecuteTask_FerrariUpdatePrivateServersRemoteCall();
	void ExecuteTask_FerrariUpdatePublicServersRemoteCall();
	*/

	QString GetTaskName(int nTaskID);

	QString SC_ExtractCurrentVersion(Status &err);
	QString SC_ExtractSPC_DBVersion(Status &err);
	void	GetLatestApplicationVersion(QString &strVersionExtended, QString &strAppVersion);
	void	SC_UpdatePublicVersionXML(Status &err,QString strVersion, QString strClientExeName, QString strServerName, bool bPutOneEntryBellow=false);
	QString FFH_ExtractCurrentVersion(Status &err);
	void	FFH_ExtractCurrentWebApp(Status &err, QString &strMajor,QString &strMinor, QString &strRevision);

	
	void	RunBatScript(Status &err, QString strScriptName, QStringList &lstArgs);
	void	FerrariRestCall(Status &err,int nTaskID);
	void	SucessHandler(Status &err,int nTaskID);
	void	ErrorHandler(Status &err, int nTaskID,QString strAdditionalDesc="");
	void	SendEmail(Status &err, QString strSubject, QString strBody);
	void	MakeSetup(QString strSetupBatFile,QString strSetupFile,Status &err);
	void	BuildAll(Status &err);
	void	FTPUpload(Status &err, QString strFile, int nDestination);
	void	FTPDownLoad(Status &err, QString strFTPFileName, QString strTargetFilePath, int nFTPLocation);
	void	SVNUpdate(Status &err);
	//void	SVNCommit(Status &err,QString strDirectory="");
};

#endif // BATCHMANAGER_H
