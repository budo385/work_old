#ifndef BATCHMANAGERTASKEXECUTOR_H
#define BATCHMANAGERTASKEXECUTOR_H


#include "common/common/backgroundtaskexecutor.h"

class BatchManagerTaskExecutor: public BackgroundTaskExecutor
{
	Q_OBJECT

public:
	BatchManagerTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent=NULL);

public slots:
	void OnProcessTask(int nTaskID);

	
};

#endif // BATCHMANAGERTASKEXECUTOR_H
