#include "appserverservice.h"
#include "common/common/config_version_ffh.h"
#include <QFile>

#include "common/common/logger.h"
extern Logger g_Logger;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;


//note on systems with more then 1 app service use strApplicationServiceName
AppServerService::AppServerService(int argc, char **argv, QString strApplicationServiceName)    
: QtService<QCoreApplication>(argc, argv, strApplicationServiceName)
{
	setServiceDescription("SOKRATES Deployment Server");
	setServiceFlags(QtServiceBase::CanBeSuspended/*Default*/);
}

AppServerService::~AppServerService()
{
}

void AppServerService::start()
{
	//1st time start:
	if (g_AppServer==NULL)
		g_AppServer=new ApplicationServer(application());

	Status err;
	g_AppServer->Start(err);
	if(!err.IsOK())
	{
		QCoreApplication *app = application();
		logMessage(QString("Failed to start application server"), QtServiceBase::Error);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
		g_AppServer->Stop();
		app->quit();
	}
}

void AppServerService::stop()
{
	if (g_AppServer!=NULL)
		if(g_AppServer->IsRunning())
			g_AppServer->Stop();

	QCoreApplication *app = application();
	app->quit();

}

//if pause, just stop listener, no new clients will be accepted, but connected ones will work!
void AppServerService::pause()
{
	if (g_AppServer!=NULL)
		g_AppServer->Stop();
}

//if resume, enable listener
void AppServerService::resume()
{
	if (g_AppServer!=NULL)
	{
		Status err;
		g_AppServer->Start(err);
		if(!err.IsOK())
		{
			QCoreApplication *app = application();
			logMessage(QString("Failed to start application server"), QtServiceBase::Error);
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
			g_AppServer->Stop();
			app->quit();
		}
	}
}
