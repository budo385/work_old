#ifndef USERSESSIONMANAGER_H
#define USERSESSIONMANAGER_H

#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "common/common/datetimehandler.h"
#include "trans/trans/httpcontext.h"
#include <QReadWriteLock>

class UserSessionData
{
public:
	//default constructor sets empty fields+activity date= current datetime
	UserSessionData(int nThread=0,QString strIP="",int SocketID=0):
		  strSession(""),	
		  strClientIP(strIP),
		  nThreadID(nThread),
		  nSocketID(SocketID),
		  nProgCode(-1),
		  bThreadActive(false),
		  datLastActivity(QDateTime::currentDateTime()){};

	 ~UserSessionData(){};


	  QString		strSession;				///<session
	  QDateTime		datLastActivity;		///<datetime of last client request
	  int			nThreadID;				///<unqiue thread id 
	  int			nSocketID;				///<unique socket ID when http
	  QString		strClientIP;			///<client IP (only in remote ops)
	  QString		strUserName; 
	  int			nProgCode;
	  QString		strProgVersionNumber; 
	  QString		strClientID; 
	  QString		strPlatformID;
	  bool			bThreadActive;

};
typedef QHash<int, UserSessionData> UserSessionList;	///session list uniquely defined by key=ThreadID
typedef QHashIterator<int,UserSessionData> UserSessionListIterator;



class UserSessionManager
{
public:
	UserSessionManager();
	~UserSessionManager();

	void	CreateSessionForWebService(Status &pStatus,QString &strSession,QString strUsername, QString strAuthToken,QString strNonce,int nProgCode,QString strProgVersionNumber, QString strClientID, QString strPlatformID); 
	void	ValidateWebUserRequest(Status &pStatus,QString strSession,const HTTPContext &ctx);
	bool	AuthenticateUser(QString strCookieValue,QString &strSessionID);
	void	DeleteSession(Status &pStatus, QString strSession);
	void	DeleteExpiredUserSessions(QList<int> &lstSocketsToRemove);
	void	GetSessionList(UserSessionList &pListSessions); 

	void	CreateTempRootSessionForAuthenticatedConnection(Status &pStatus,QString &strSession); 

	void	LockThreadActive();
	void	UnLockThreadActive();
	QString GetSessionID();
	QString GetClientIP(QString strSession="");
	QString GetUserNameExt(QString strSession="");
	int		GetUserProgCode(QString strSession="");
	int		GetSessionCount();

	void	ClearUserSessionStorage(QString strSession="");
	void	SetUserStorageDirectoryPath(QString strPath){m_strUserStorageDirectoryPath=strPath;}
	QString	GetUserStorageDirectoryPath(){return m_strUserStorageDirectoryPath;};
	QString InitializeUserStorageDirectory(Status &status,QString strUserSubDir);

private:

	QString				m_strUserStorageDirectoryPath;
	UserSessionList		m_SessionList;			
	QList<QString>		m_lstServerNonce;
	QReadWriteLock		m_SessionListRWLock;			///< RWlock for accessing m_SessionList
	QReadWriteLock		m_ServerNonceListRWLock;		///< RWlock for accessing m_SessionList

	int		FindSession(QString strSession);
	int		FindSocket(int nSocketID);


};

#endif // USERSESSIONMANAGER_H
