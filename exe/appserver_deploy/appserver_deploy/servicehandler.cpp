#include "servicehandler.h"

#include "usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "batchmanager.h"
extern BatchManager *g_BatchManager;

void ServiceHandler::ExecuteTask(Status &err, int nTaskID)
{
	if(!g_BatchManager->IsTaskRunning())
	{
		g_BatchManager->StartBkgTask(nTaskID);
	}
	else
	{
		err.setError(1,"Task can not be scheduled, because deploy server is already processing task");
	}
}

void ServiceHandler::GetTasks(Status &err, DbRecordSet &Ret_Tasks)
{
	g_BatchManager->GetTasks(Ret_Tasks);
}


void ServiceHandler::Logout(Status &pStatus,QString strSessionID)
{
	g_UserSessionManager->DeleteSession(pStatus,strSessionID);
}

void ServiceHandler::Login(Status& pStatus, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode, QString strProgVer, QString strClientID, QString strPlatform)
{
	g_UserSessionManager->CreateSessionForWebService(pStatus,RetOut_strSessionID,strUserName,strAuthToken,strClientNonce,nProgCode,strProgVer,strClientID,strPlatform);
}