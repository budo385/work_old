#ifndef SERVICEHANDLER_H
#define SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "interface_servicehandler.h"

class ServiceHandler: public Interface_ServiceHandler
{
public:

	void GetTasks(Status &err, DbRecordSet &Ret_Tasks);
	void ExecuteTask(Status &err, int nTaskID);
	void Login(Status& pStatus, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode=0, QString strProgVer="", QString strClientID="", QString strPlatform="");
	void Logout(Status &pStatus,QString strSessionID);

private:
	
};

#endif // SERVICEHANDLER_H
