#include "batchmanager.h"
#include <QDebug>
#include "common/common/threadid.h"
#include "trans/trans/smtpclient.h"
#include "trans/trans/resthttpclient.h"
#include "common/common/datahelper.h"
#include "common/common/config_version_sc.h"

#include "common/common/logger.h"
extern Logger							g_Logger;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;


BatchManager::BatchManager()
{
	FTPConnectionSettings conn;

	conn.m_strFTPUserName="sokratesch";
	conn.m_strFTPPassword="xe78bkvy";
	conn.m_strServerIP="mynx.iway.ch";
	conn.m_strStartDirectory="www/helixbusinesssofthr";

	lstFTPconections[FTP_SOKRATES_HR_PUBLIC_DIR]=conn;

	conn.m_strFTPUserName="sokratesch";
	conn.m_strFTPPassword="xe78bkvy";
	conn.m_strServerIP="mynx.iway.ch";
	conn.m_strStartDirectory="www/helixbusinesssofthr";

	lstFTPconections[FTP_SOKRATES_HR_PRIVATE_DIR]=conn;

	/*
	conn.m_strFTPUserName="sokrates";
	conn.m_strFTPPassword="party02";
	conn.m_strServerIP="solutionp2.upload.akamai.com";
	conn.m_strStartDirectory="5104/customers/sokrates";

	lstFTPconections[FTP_SOKRATES_COMMUNICATOR_DOWNLOAD]=conn;
	*/

	conn.m_strFTPUserName="sokratesch";
	conn.m_strFTPPassword="xe78bkvy";
	conn.m_strServerIP="mynx.iway.ch";
	conn.m_strStartDirectory="www/communicator/downloads";
	lstFTPconections[FTP_SOKRATES_COMMUNICATOR_DOWNLOAD]=conn;


	conn.m_strFTPUserName="sokratesch";
	conn.m_strFTPPassword="xe78bkvy";
	conn.m_strServerIP="mynx.iway.ch";
	conn.m_strStartDirectory="www/communicator/version";

	lstFTPconections[FTP_SOKRATES_CH_VERSION_DIR]=conn;

	conn.m_strFTPUserName="sokratesch";
	conn.m_strFTPPassword="xe78bkvy";
	conn.m_strServerIP="mynx.iway.ch";
	conn.m_strStartDirectory="www/everpics/downloads";

	lstFTPconections[FTP_SOKRATES_HR_FFH]=conn;


	conn.m_strFTPUserName="sokratesch";
	conn.m_strFTPPassword="xe78bkvy";
	conn.m_strServerIP="mynx.iway.ch";
	conn.m_strStartDirectory="www/everdocs/downloads";

	lstFTPconections[FTP_SOKRATES_HR_JOHN]=conn;



}

void BatchManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	QMutexLocker lock(&mutex);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Start ExecuteTask with id=%1").arg(nTaskID));


	switch(nTaskID)
	{
		case TASK_COMMUNICATORPRIVATEBUILD:
		{
			ExecuteTask_CommunicatorPrivateBuild();
			return;
		}
		case TASK_COMMUNICATORPUBLICBUILD:
		{
			ExecuteTask_CommunicatorPublicBuild();
			return;
		}
		case TASK_SERVERFARM_DEPLOY_NEW_FILES:
			{
				ExecuteTask_ServerFarmDeployNewFiles();
				return;
			}
		case TASK_SERVERFARM_UPDATE_PRIVATE_SERVERS:
			{
				ExecuteTask_ServerFarmUpdatePrivateServers();
				return;
			}
		case TASK_SERVERFARM_UPDATE_PUBLIC_SERVERS:
			{
				ExecuteTask_ServerFarmUpdatePublicServers();
				return;
			}


		/*
		case TASK_FERRARI_DEPLOY_NEW_FILES:
			{
				ExecuteTask_FerrariDeployNewFiles();
				return;
			}
		case TASK_FERRARI_UPDATE_PRIVATE_SERVERS:
			{
				ExecuteTask_FerrariUpdatePrivateServers();
				return;
			}
		case TASK_FERRARI_UPDATE_PUBLIC_SERVERS:
			{
				ExecuteTask_FerrariUpdatePublicServers();
				return;
			}
			
		case TASK_FERRARI_DEPLOY_NEW_FILES_REMOTECALL:
			{
				ExecuteTask_FerrariDeployNewFilesRemoteCall();
				return;
			}
		case TASK_FERRARI_UPDATE_PRIVATE_SERVERS_REMOTECALL:
			{
				ExecuteTask_FerrariUpdatePrivateServersRemoteCall();
				return;
			}
		case TASK_FERRARI_UPDATE_PUBLIC_SERVERS_REMOTECALL:
			{
				ExecuteTask_FerrariUpdatePublicServersRemoteCall();
				return;
			}
		*/
		case TASK_EVERPICSSERVERBUILD:
			{
				ExecuteTask_FFHServerBuild();
				return;
			}
		case TASK_EVERDOCSSERVERBUILD:
			{
				ExecuteTask_JohnerverBuild();
				return;
			}
		case TASK_BUILDALL:
			{
				ExecuteTask_BuildAll_NoUpload();
				return;
			}
		case TASK_MAILEGANTBUILD:
			{
				ExecuteTask_MailegantBuild();
				return;
			}

			void ExecuteTask_BuildAll_NoUpload();
			
		default:
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to execute task id: "+QString::number(nTaskID));
			return;
		}
	}

}


//if files are present then enable tasks:
void BatchManager::GetTasks(DbRecordSet &RetTasks)
{
	RetTasks.destroy();
	RetTasks.addColumn(QVariant::Int,"TASK_ID");
	RetTasks.addColumn(QVariant::String,"TASK_NAME");
	
	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_COMMUNICATORPRIVATEBUILD"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_COMMUNICATORPRIVATEBUILD);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_COMMUNICATORPRIVATEBUILD));
	}

	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_COMMUNICATORPUBLICBUILD"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_COMMUNICATORPUBLICBUILD);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_COMMUNICATORPUBLICBUILD));
	}

	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_EVERPICSSERVERBUILD"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_EVERPICSSERVERBUILD);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_EVERPICSSERVERBUILD));
	}

	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_EVERDOCSSERVERBUILD"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_EVERDOCSSERVERBUILD);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_EVERDOCSSERVERBUILD));
	}

	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_MAILEGANTBUILD"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_MAILEGANTBUILD);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_MAILEGANTBUILD));
	}


	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_SERVERFARM_DEPLOY_NEW_FILES"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_SERVERFARM_DEPLOY_NEW_FILES);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_SERVERFARM_DEPLOY_NEW_FILES));
	}

	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_SERVERFARM_UPDATE_PRIVATE_SERVERS"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_SERVERFARM_UPDATE_PRIVATE_SERVERS);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_SERVERFARM_UPDATE_PRIVATE_SERVERS));
	}

	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_SERVERFARM_UPDATE_PUBLIC_SERVERS"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_SERVERFARM_UPDATE_PUBLIC_SERVERS);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_SERVERFARM_UPDATE_PUBLIC_SERVERS));
	}

	/*
	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_FERRARI_DEPLOY_NEW_FILES"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_FERRARI_DEPLOY_NEW_FILES);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_FERRARI_DEPLOY_NEW_FILES));
	}

	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_FERRARI_UPDATE_PRIVATE_SERVERS"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_FERRARI_UPDATE_PRIVATE_SERVERS);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_FERRARI_UPDATE_PRIVATE_SERVERS));
	}

	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_FERRARI_UPDATE_PUBLIC_SERVERS"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_FERRARI_UPDATE_PUBLIC_SERVERS);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_FERRARI_UPDATE_PUBLIC_SERVERS));
	}
	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_FERRARI_DEPLOY_NEW_FILES_REMOTECALL"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_FERRARI_DEPLOY_NEW_FILES_REMOTECALL);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_FERRARI_DEPLOY_NEW_FILES_REMOTECALL));
	}
	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_FERRARI_UPDATE_PRIVATE_SERVERS_REMOTECALL"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_FERRARI_UPDATE_PRIVATE_SERVERS_REMOTECALL);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_FERRARI_UPDATE_PRIVATE_SERVERS_REMOTECALL));
	}
	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_FERRARI_UPDATE_PUBLIC_SERVERS_REMOTECALL"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_FERRARI_UPDATE_PUBLIC_SERVERS_REMOTECALL);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_FERRARI_UPDATE_PUBLIC_SERVERS_REMOTECALL));
	}
	*/
	if(QFile::exists(QCoreApplication::applicationDirPath()+"/TASK_BUILDALL"))
	{
		RetTasks.addRow();
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_ID",TASK_BUILDALL);
		RetTasks.setData(RetTasks.getRowCount()-1,"TASK_NAME",GetTaskName(TASK_BUILDALL));
	}

}



QString BatchManager::GetTaskName(int nTaskID)
{
	switch(nTaskID)
	{
	case TASK_COMMUNICATORPRIVATEBUILD:
		{
			return "Communicator PRIVATE Build (manually increase version before)";
		}
	case TASK_COMMUNICATORPUBLICBUILD:
		{
			return "Communicator PUBLIC Build (manually increase version before)";
		}
	case TASK_SERVERFARM_DEPLOY_NEW_FILES:
		{
			return "[ServerFarm] Copy files from latest build to be ready for update on all servers";
		}
	case TASK_SERVERFARM_UPDATE_PRIVATE_SERVERS:
		{
			return "[ServerFarm] Update private servers only now";
		}
	case TASK_SERVERFARM_UPDATE_PUBLIC_SERVERS:
		{
			return "[ServerFarm] Schedule update of all servers for tomorrow";
		}

		/*
	case TASK_FERRARI_DEPLOY_NEW_FILES:
		{
			return "Ferrari download new files from ftp";
		}
	case TASK_FERRARI_UPDATE_PRIVATE_SERVERS:
		{
			return "Ferrari update PRIVATE servers";
		}
	case TASK_FERRARI_UPDATE_PUBLIC_SERVERS:
		{
			return "Ferrari update PUBLIC servers";
		}

	case TASK_FERRARI_DEPLOY_NEW_FILES_REMOTECALL:
		{
			return "Send Ferrari command to download NEW deployment exe files";
		}
	case TASK_FERRARI_UPDATE_PRIVATE_SERVERS_REMOTECALL:
		{
			return "Send Ferrari command to update PRIVATE servers";
		}
	case TASK_FERRARI_UPDATE_PUBLIC_SERVERS_REMOTECALL:
		{
			return "Send Ferrari command to update PUBLIC servers";
		}
		*/
	case TASK_EVERPICSSERVERBUILD:
		{
			return "EverPICs Server: Deploy";
		}
	case TASK_EVERDOCSSERVERBUILD:
		{
			return "EverDOCs Server: Deploy";
		}
	case TASK_BUILDALL:
		{
			return "Build All, no upload";
		}
	case TASK_MAILEGANTBUILD:
		{
			return "Mailegant client writer deploy";
		}
		

	default:
		{
			return " TASK "+QString::number(nTaskID);
		}
	}
}


bool BatchManager::IsTaskRunning()
{
	if (mutex.tryLock())
	{
		mutex.unlock();
		return false;
	}
	return true;
}


void BatchManager::ExecuteTask_CommunicatorPrivateBuild(bool bSkipUpload)
{
	Status err;

	//update from svn:
	SVNUpdate(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: SVN Update");
		return;
	}

	//build all
	BuildAll(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: BuildAll");
		return;
	}

	QString strVersionExtended;

	//get version:
	QString strVersion=SC_ExtractCurrentVersion(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: SC_ExtractCurrentVersion");
		return;
	}

	QString strVersionSPC=SC_ExtractSPC_DBVersion(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: SC_ExtractSPC_DBVersion");
		return;
	}

	strVersionExtended=strVersion+"_SPC"+strVersionSPC;


	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Version is: "+strVersionExtended);

	QString strClientSetup=g_AppServer->GetSettings()->m_strSC_ClientSetupPrefix+strVersionExtended+".exe";
	QString strServerSetup=g_AppServer->GetSettings()->m_strSC_ServerSetupPrefix+strVersionExtended+".exe";

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Client setup is: "+strClientSetup);
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server setup is: "+strServerSetup);

	MakeSetup("make_setup_sc_client.bat",strClientSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: MakeSetup(make_setup_sc_client.bat)");
			return;
		}
	}

	//make setup + zip archives for server only
	MakeSetup("make_setup_sc_server.bat",strServerSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: MakeSetup(make_setup_sc_server.bat)");
			return;
		}
	}

	/*
	QStringList lstArgs;
	RunBatScript(err,"make_zip.bat",lstArgs);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: make_zip.bat");
			return;
		}
	}
	*/

	if (!bSkipUpload)
	{

		/*
		//upload data
		QString strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strClientSetup;
		FTPUpload(err,strFile,FTP_SOKRATES_HR_PUBLIC_DIR);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: FTPUpload client FTP_SOKRATES_HR_PUBLIC_DIR");
			return;
		}

		//upload data
		strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strServerSetup;
		FTPUpload(err,strFile,FTP_SOKRATES_HR_PUBLIC_DIR);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: FTPUpload server FTP_SOKRATES_HR_PUBLIC_DIR");
			return;
		}

		//upload data
		strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/new_code.zip";
		FTPUpload(err,strFile,FTP_SOKRATES_HR_PRIVATE_DIR);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: FTPUpload new_code.zip");
			return;
		}

		*/

		//BT: 21.09.2012: MB order to use public akamai system for distributing private builds: only entry in version.xml will NOT be on top of file, just one place below!!!!!!

		//upload data
		QString strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strClientSetup;
		FTPUpload(err,strFile,FTP_SOKRATES_COMMUNICATOR_DOWNLOAD);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
			return;
		}

		//upload data
		strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strServerSetup;
		FTPUpload(err,strFile,FTP_SOKRATES_COMMUNICATOR_DOWNLOAD);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
			return;
		}

		/*
		//upload data
		strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/new_code.zip";
		FTPUpload(err,strFile,FTP_SOKRATES_HR_PRIVATE_DIR);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
			return;
		}
		*/

		//FTP_SOKRATES_CH_VERSION_DIR
		SC_UpdatePublicVersionXML(err,strVersion,strClientSetup,strServerSetup,true);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
			return;
		}

		strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/version.xml";
		FTPUpload(err,strFile,FTP_SOKRATES_CH_VERSION_DIR);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
			return;
		}



		/*
		//inform ferrari server to execute deploy data
		FerrariRestCall(err,TASK_FERRARI_DEPLOY_NEW_FILES);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: FerrariRestCall");
			return;
		}
		*/

		//Deploy files:
		QStringList lstArgs;
		lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strDEPLOY_DIR);
		lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strServerFarmPath);

		RunBatScript(err,"serverfarm_deploy.bat",lstArgs);
		if (!err.IsOK())
		{
			if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
			{
				ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: serverfarm_deploy.bat");
				return;
			}
		}
	}


	SucessHandler(err,TASK_COMMUNICATORPRIVATEBUILD);
}


void BatchManager::ExecuteTask_CommunicatorPublicBuild()
{
	Status err;

	//update from svn:
	SVNUpdate(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
		return;
	}

	//build all
	BuildAll(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
		return;
	}


	QString strVersionExtended;

	//get version:
	QString strVersion=SC_ExtractCurrentVersion(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: SC_ExtractCurrentVersion");
		return;
	}

	QString strVersionSPC=SC_ExtractSPC_DBVersion(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: SC_ExtractSPC_DBVersion");
		return;
	}

	strVersionExtended=strVersion+"_SPC"+strVersionSPC;

	QString strClientSetup=g_AppServer->GetSettings()->m_strSC_ClientSetupPrefix+strVersionExtended+".exe";
	QString strServerSetup=g_AppServer->GetSettings()->m_strSC_ServerSetupPrefix+strVersionExtended+".exe";

	MakeSetup("make_setup_sc_client.bat",strClientSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
			return;
		}
	}

	//make setup + zip archives for server only
	MakeSetup("make_setup_sc_server.bat",strServerSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
			return;
		}
	}

	/*
	QStringList lstArgs;
	RunBatScript(err,"make_zip.bat",lstArgs);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
			return;
		}
	}
	*/

	//upload data
	QString strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strClientSetup;
	FTPUpload(err,strFile,FTP_SOKRATES_COMMUNICATOR_DOWNLOAD);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
		return;
	}

	//upload data
	strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strServerSetup;
	FTPUpload(err,strFile,FTP_SOKRATES_COMMUNICATOR_DOWNLOAD);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
		return;
	}

	/*
	//upload data
	strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/new_code.zip";
	FTPUpload(err,strFile,FTP_SOKRATES_HR_PRIVATE_DIR);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
		return;
	}

	*/
	//FTP_SOKRATES_CH_VERSION_DIR
	SC_UpdatePublicVersionXML(err,strVersion,strClientSetup,strServerSetup);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
		return;
	}

	strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/version.xml";
	FTPUpload(err,strFile,FTP_SOKRATES_CH_VERSION_DIR);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
		return;
	}

	/*
	//inform ferrari server to execute deploy data
	FerrariRestCall(err,TASK_FERRARI_DEPLOY_NEW_FILES);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_COMMUNICATORPUBLICBUILD);
		return;
	}
	*/

	//Deploy files:
	QStringList lstArgs;
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strDEPLOY_DIR);
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strServerFarmPath);

	RunBatScript(err,"serverfarm_deploy.bat",lstArgs);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_COMMUNICATORPRIVATEBUILD,"Step: serverfarm_deploy.bat");
			return;
		}
	}

	SucessHandler(err,TASK_COMMUNICATORPUBLICBUILD);

}

void BatchManager::ExecuteTask_FFHServerBuild(bool bSkipUpload)
{

	Status err;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start SVN");
	//update from svn:
	SVNUpdate(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: SVN Update");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start Build");

	//build all
	BuildAll(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: BuildAll");
		return;
	}

	/*
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start SVNCommit");

	//commit wedata.bin
	SVNCommit(err,"D:/Deploy");
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: SVNCommit");
		return;
	}
	*/

	
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start FFH_ExtractCurrentVersion");

	//get version:
	QString strVersion=FFH_ExtractCurrentVersion(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: ExtractCurrentVersion");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start FFH_ExtractCurrentWebApp");

	if (!bSkipUpload)
	{

	

		//get webapp version and save to file epwa_version.txt
		QString strMajor,strMinor,strRevision;
		FFH_ExtractCurrentWebApp(err,strMajor,strMinor,strRevision);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: FFH_ExtractCurrentWebApp");
			return;
		}

		
		//save to install dir /webdata.bin:
		//QByteArray byteData=strWebVersion.toAscii();
		QString strWebAppFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/epwa_version.txt";
		QFile file_ver(strWebAppFile);

		if(file_ver.open(QIODevice::ReadOnly))
		{
			QString strRev=strMajor+QString(".")+strMinor+QString(".")+strRevision;
			QByteArray byteVersionData=file_ver.readAll();
			file_ver.close();


			QStringList lstRevisions=QString(byteVersionData).split(";",QString::SkipEmptyParts);
			if (!lstRevisions.contains(strRev))
			{

				QFile file_ver2(strWebAppFile);
				if(file_ver2.open(QIODevice::Append))
				{
					QByteArray data=QString(strMajor+"."+strMinor+"."+strRevision+";").toLatin1();
					file_ver2.write(data);
					file_ver2.close();
				}
				else
				{
					ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: FFH_ExtractCurrentWebApp write to epwa_version.txt");
					return;
				}
			}
		}
		else
		{
			ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: FFH_ExtractCurrentWebApp write to epwa_version.txt");
			return;
		}



		//upload version
		FTPUpload(err,strWebAppFile,FTP_SOKRATES_HR_FFH);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: FTPUpload epwa_version.txt FTP_EVERPICS");
			return;
		}

		//upload webdata
		QString strWebAppData=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/webdata.bin";
		QString strWebAppDataNew=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/webdata"+strMajor+"."+strMinor+"."+strRevision+".bin";
		//rename to rev:
		QFile::rename(strWebAppData,strWebAppDataNew);

		FTPUpload(err,strWebAppDataNew,FTP_SOKRATES_HR_FFH);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: FTPUpload webdata.bin FTP_EVERPICS");
			return;
		}


	}

	//QString strServerSetup="FFHServerSetup_"+strVersion+".exe";
	QString strServerSetup="EverPICsBridgeSetup.exe";
	

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start make_setup_ffhserver.bat");

	//make setup + zip archives for server only
	MakeSetup("make_setup_ffhserver.bat",strServerSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: MakeSetup(make_setup_ffhserver.bat)");
			return;
		}
	}

	if (!bSkipUpload)
	{

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start upload");

		//upload data
		QString strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strServerSetup;
		FTPUpload(err,strFile,FTP_SOKRATES_HR_FFH);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_EVERPICSSERVERBUILD,"Step: FTPUpload server FTP_SOKRATES_HR_FFH");
			return;
		}

	}



	SucessHandler(err,TASK_EVERPICSSERVERBUILD);

}



void BatchManager::ExecuteTask_JohnerverBuild(bool bSkipUpload)
{

	Status err;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start SVN");
	//update from svn:
	SVNUpdate(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_EVERDOCSSERVERBUILD,"Step: SVN Update");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start Build");

	//build all
	BuildAll(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_EVERDOCSSERVERBUILD,"Step: BuildAll");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start JOHN_ExtractCurrentVersion");

/*
	//commit wedata.bin
	SVNCommit(err,"D:/Deploy");
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: SVNCommit");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start SVNCommit");


	//get version:
	QString strVersion=FFH_ExtractCurrentVersion(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: ExtractCurrentVersion");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start FFH_ExtractCurrentWebApp");


	//get webapp version and save to file epwa_version.txt
	QString strMajor,strMinor,strRevision;
	FFH_ExtractCurrentWebApp(err,strMajor,strMinor,strRevision);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: FFH_ExtractCurrentWebApp");
		return;
	}


	//save to install dir /webdata.bin:
	//QByteArray byteData=strWebVersion.toAscii();
	QString strWebAppFile=QCoreApplication::applicationDirPath()+"/epwa_version.txt";
	QFile file_ver(strWebAppFile);

	if(file_ver.open(QIODevice::ReadOnly))
	{
		QString strRev=strMajor+QString(".")+strMinor+QString(".")+strRevision;
		QByteArray byteVersionData=file_ver.readAll();
		file_ver.close();


		QStringList lstRevisions=QString(byteVersionData).split(";",QString::SkipEmptyParts);
		if (!lstRevisions.contains(strRev))
		{

			QFile file_ver2(strWebAppFile);
			if(file_ver2.open(QIODevice::Append))
			{
				QByteArray data=QString(strMajor+"."+strMinor+"."+strRevision+";").toAscii();
				file_ver2.write(data);
				file_ver2.close();
			}
			else
			{
				ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: FFH_ExtractCurrentWebApp write to epwa_version.txt");
				return;
			}
		}
	}
	else
	{
		ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: FFH_ExtractCurrentWebApp write to epwa_version.txt");
		return;
	}



	//upload version
	FTPUpload(err,strWebAppFile,FTP_SOKRATES_HR_FFH);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: FTPUpload epwa_version.txt FTP_EVERPICS");
		return;
	}

	//upload webdata
	QString strWebAppData=QCoreApplication::applicationDirPath()+"/webdata.bin";
	QString strWebAppDataNew=QCoreApplication::applicationDirPath()+"/webdata"+strMajor+"."+strMinor+"."+strRevision+".bin";
	//rename to rev:
	QFile::rename(strWebAppData,strWebAppDataNew);

	FTPUpload(err,strWebAppDataNew,FTP_SOKRATES_HR_FFH);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: FTPUpload webdata.bin FTP_EVERPICS");
		return;
	}
*/

	//QString strServerSetup="FFHServerSetup_"+strVersion+".exe";
	QString strServerSetup="EverDOCsBridgeSetup.exe";


	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start make_setup_johnserver");

	//make setup + zip archives for server only
	MakeSetup("make_setup_johnserver.bat",strServerSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_EVERDOCSSERVERBUILD,"Step: MakeSetup(make_setup_johnserver.bat)");
			return;
		}
	}

	if (!bSkipUpload)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"FTPUpload: FTP_SOKRATES_HR_FFH");

		//upload data
		QString strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strServerSetup;
		FTPUpload(err,strFile,FTP_SOKRATES_HR_JOHN);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_EVERDOCSSERVERBUILD,"Step: FTPUpload server FTP_SOKRATES_HR_FFH");
			return;
		}
	}


	SucessHandler(err,TASK_EVERDOCSSERVERBUILD);

}




void BatchManager::ExecuteTask_MailegantBuild(bool bSkipUpload)
{

	Status err;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start SVN");
	//update from svn:
	SVNUpdate(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_MAILEGANTBUILD,"Step: SVN Update");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start Build");

	//build all
	BuildAll(err);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_MAILEGANTBUILD,"Step: BuildAll");
		return;
	}

	QString strServerSetup="MailegantSetup.exe";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start make_setup_mailegant");

	//make setup + zip archives for server only
	MakeSetup("make_setup_mailegant.bat",strServerSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_MAILEGANTBUILD,"Step: MakeSetup(make_setup_mailegant.bat)");
			return;
		}
	}

	//CS,LB and tester also:
	strServerSetup="mw_loadbalanceserver_setup.exe";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start make_setup_loadbalanceserver_mailegant");

	//make setup + zip archives for server only
	MakeSetup("make_setup_loadbalanceserver_mailegant.bat",strServerSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_MAILEGANTBUILD,"Step: MakeSetup(make_setup_mailegant.bat)");
			return;
		}
	}

	strServerSetup="mw_cloudserver_setup.exe";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start make_setup_cloudserver_mailegant");

	//make setup + zip archives for server only
	MakeSetup("make_setup_cloudserver_mailegant.bat",strServerSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_MAILEGANTBUILD,"Step: MakeSetup(make_setup_mailegant.bat)");
			return;
		}
	}

	strServerSetup="ServerPerfomanceTester_Setup.exe";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start make_setup_serverperformancetester_mailegant.bat");

	//make setup + zip archives for server only
	MakeSetup("make_setup_serverperformancetester_mailegant.bat",strServerSetup,err);
	if (!err.IsOK())
	{
		if (!err.getErrorText().contains("SignTool Error:")) //ignore signtool error
		{
			ErrorHandler(err,TASK_MAILEGANTBUILD,"Step: MakeSetup(make_setup_mailegant.bat)");
			return;
		}
	}



	/*
	if (!bSkipUpload)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"FTPUpload: FTP_SOKRATES_HR_FFH");

		//upload data
		QString strFile=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/"+strServerSetup;
		FTPUpload(err,strFile,FTP_SOKRATES_HR_JOHN);
		if (!err.IsOK())
		{
			ErrorHandler(err,TASK_FFHSERVERBUILD,"Step: FTPUpload server FTP_SOKRATES_HR_FFH");
			return;
		}
	}
	*/

	SucessHandler(err,TASK_MAILEGANTBUILD);

}





void BatchManager::ExecuteTask_BuildAll_NoUpload()
{
	ExecuteTask_CommunicatorPrivateBuild(true);
	ExecuteTask_FFHServerBuild(true);
	ExecuteTask_JohnerverBuild(true);
	ExecuteTask_MailegantBuild(true);

	Status err;
	SucessHandler(err,TASK_BUILDALL);
}


/*
void BatchManager::ExecuteTask_FerrariDeployNewFiles()
{
	Status err;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start new_code.zip download");

	//download new-code.zip
	QString strFileTarget=QString(g_AppServer->GetSettings()->m_strDEPLOY_DIR)+"/new_code.zip";
	QString strFileSource="new_code.zip";
	FTPDownLoad(err,strFileSource,strFileTarget,FTP_SOKRATES_HR_PRIVATE_DIR);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FERRARI_DEPLOY_NEW_FILES);
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start make_unzip.bat");

	//extract
	QStringList lstArgs;
	RunBatScript(err,"make_unzip.bat",lstArgs);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FERRARI_DEPLOY_NEW_FILES);
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start deploy.bat");

	//run deploy script
	RunBatScript(err,"deploy.bat",lstArgs);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FERRARI_DEPLOY_NEW_FILES);
		return;
	}

	SucessHandler(err,TASK_FERRARI_DEPLOY_NEW_FILES);

}

void BatchManager::ExecuteTask_FerrariUpdatePrivateServers()
{
	Status err;
	//run deploy script
	QStringList lstArgs;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start start_private.bat");
	
	RunBatScript(err,"start_private.bat",lstArgs);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FERRARI_UPDATE_PRIVATE_SERVERS);
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"End start_private.bat");

	SucessHandler(err,TASK_FERRARI_UPDATE_PRIVATE_SERVERS);

}

void BatchManager::ExecuteTask_FerrariUpdatePublicServers()
{

	QString strDateTommorow=QDate::currentDate().addDays(1).toString("dd.MM.yyyy");
	//run script

	Status err;
	//run deploy script
	QStringList lstArgs;
	lstArgs<<strDateTommorow;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start schedule_srv_all.bat");

	RunBatScript(err,"schedule_srv_all.bat",lstArgs);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FERRARI_UPDATE_PRIVATE_SERVERS);
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"End schedule_srv_all.bat");

	SucessHandler(err,TASK_FERRARI_UPDATE_PUBLIC_SERVERS);
}
*/

/*
void BatchManager::ExecuteTask_FerrariUpdatePrivateServersRemoteCall()
{
	Status err;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start FerrariRestCall TASK_FERRARI_UPDATE_PRIVATE_SERVERS");

	FerrariRestCall(err,TASK_FERRARI_UPDATE_PRIVATE_SERVERS);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FERRARI_UPDATE_PRIVATE_SERVERS_REMOTECALL);
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"End FerrariRestCall TASK_FERRARI_UPDATE_PRIVATE_SERVERS");
	
	SucessHandler(err,TASK_FERRARI_UPDATE_PRIVATE_SERVERS_REMOTECALL);
}


void BatchManager::ExecuteTask_FerrariDeployNewFilesRemoteCall()
{
	Status err;

	//inform ferrari server to execute deploy data
	FerrariRestCall(err,TASK_FERRARI_DEPLOY_NEW_FILES);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FERRARI_DEPLOY_NEW_FILES_REMOTECALL);
		return;
	}

	SucessHandler(err,TASK_FERRARI_DEPLOY_NEW_FILES_REMOTECALL);

}

void BatchManager::ExecuteTask_FerrariUpdatePublicServersRemoteCall()
{
	Status err;

	//inform ferrari server to execute deploy data
	FerrariRestCall(err,TASK_FERRARI_UPDATE_PUBLIC_SERVERS);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_FERRARI_UPDATE_PUBLIC_SERVERS_REMOTECALL);
		return;
	}

	SucessHandler(err,TASK_FERRARI_UPDATE_PUBLIC_SERVERS_REMOTECALL);

}

*/














//build
void BatchManager::BuildAll(Status &err)
{
	QStringList lstArgs;
	QProcess tool;

	//tool.setWorkingDirectory(g_AppServer->GetSettings()->m_strDEPLOY_DIR);
	tool.setWorkingDirectory(QCoreApplication::applicationDirPath());

	//Note: Qprocess will add double qoutes only if path contains spaces, only on Windows OS
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strDEPLOY_DIR);
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strSVN_DIR);
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strVisualPath);

	//lstArgs<<g_AppServer->GetSettings()->m_strDEPLOY_DIR;
	//lstArgs<<g_AppServer->GetSettings()->m_strSVN_DIR;
	//lstArgs<<g_AppServer->GetSettings()->m_strVisualPath;

	tool.start("build_all.bat",lstArgs);
	if (!tool.waitForFinished(-1)) //wait forever
	{	
		QByteArray errContent=tool.readAllStandardOutput();
		err.setError(1,"Error Build 1: "+QString(errContent.left(4095)));
		return;
	}
	int nExitCode=tool.exitCode();
	QByteArray errContent=tool.readAllStandardError();

	if (nExitCode!=0 || errContent.size()!=0)
	{
		if (errContent.size()!=0)
			err.setError(1,"Error Build 2: "+QString(errContent.left(4095)));
		else
			err.setError(1,"Error Build 2: "+QString(tool.readAllStandardOutput().right(4095)));
		return;
	}

}

//update
void BatchManager::SVNUpdate(Status &err)
{
	QStringList lstArgs;
	lstArgs<<"update";
	lstArgs<<"--username";
	lstArgs<<g_AppServer->GetSettings()->m_strSVN_User;
	lstArgs<<"--password";
	lstArgs<<g_AppServer->GetSettings()->m_strSVN_Pass;

	QProcess tool;
	tool.setWorkingDirectory(g_AppServer->GetSettings()->m_strSVN_DIR);
	tool.start("svn",lstArgs);
	if (!tool.waitForFinished(-1)) //wait forever
	{	
		QByteArray errContent=tool.readAllStandardOutput();
		err.setError(1,"Error SVN update: "+QString(errContent.left(4095)));
		return;
	}
	int nExitCode=tool.exitCode();
	QByteArray errContent=tool.readAllStandardError();
	if (nExitCode!=0 || errContent.size()!=0)
	{
		if (errContent.size()!=0)
			err.setError(1,"Error SVN update 2: "+QString(errContent.left(4095)));
		else
			err.setError(1,"Error SVN update 2: "+QString(tool.readAllStandardOutput().right(4095)));
		return;
	}
}


//build
void BatchManager::FTPUpload(Status &err, QString strFile, int nFTPLocation)
{
	FTPConnectionSettings conn=lstFTPconections.value(nFTPLocation);

	FTPClient client;
	client.Connect(err,conn);
	if (!err.IsOK())return;

	client.File_Put(err,strFile);
	if (!err.IsOK())
	{
		Status err_temp;
		client.Disconnect(err_temp);
		return;
	}
	client.Disconnect(err);
}


//build
void BatchManager::FTPDownLoad(Status &err, QString strFTPFileName, QString strTargetFilePath, int nFTPLocation)
{
	FTPConnectionSettings conn=lstFTPconections.value(nFTPLocation);

	FTPClient client;
	client.Connect(err,conn);
	if (!err.IsOK())return;

	client.File_Get(err,strFTPFileName,strTargetFilePath);
	if (!err.IsOK())
	{
		Status err_temp;
		client.Disconnect(err_temp);
		return;
	}
	client.Disconnect(err);
}

void BatchManager::GetLatestApplicationVersion(QString &strVersionExtended, QString &strAppVersion)
{
	strAppVersion= APPLICATION_VERSION;
	strVersionExtended=strAppVersion+"_SPC"+QString::number(DATABASE_VERSION_SPC);
}

QString BatchManager::SC_ExtractCurrentVersion(Status &err)
{
	QString strConfigFile=g_AppServer->GetSettings()->m_strSVN_DIR+QString("/lib/common/common/config_version_sc.h");

	QFile file(strConfigFile);
	if(!file.open(QIODevice::ReadOnly))
	{
		QString strMsg=tr("Error while reading file: ")+" "+strConfigFile;
		err.setError(1,strMsg);
		return "";
	}

	QByteArray content=file.readAll();
	file.close();

	int nIdx=content.indexOf("#define APPLICATION_VERSION");
	if (nIdx<0)
	{
		err.setError(1,"Failed to update version");
		return "";
	}

	int nIdx_1=content.indexOf("\"",nIdx);
	if (nIdx_1<0)
	{
		err.setError(1,"Failed to update version");
		return "";
	}

	int nIdx_2=content.indexOf("\"",nIdx_1+1);
	if (nIdx_2<0)
	{
		err.setError(1,"Failed to update version");
		return "";
	}

	QString strVersion=content.mid(nIdx_1+1,nIdx_2-nIdx_1-1);
	return strVersion;
}

QString BatchManager::SC_ExtractSPC_DBVersion(Status &err)
{
	QString strConfigFile=g_AppServer->GetSettings()->m_strSVN_DIR+QString("/lib/common/common/config_version_sc.h");

	QFile file(strConfigFile);
	if(!file.open(QIODevice::ReadOnly))
	{
		QString strMsg=tr("Error while reading file: ")+" "+strConfigFile;
		err.setError(1,strMsg);
		return "";
	}

	QByteArray content=file.readAll();
	file.close();

	int nIdx=content.indexOf("#define DATABASE_VERSION_SPC");
	if (nIdx<0)
	{
		err.setError(1,"Failed to update version");
		return "";
	}
	nIdx += QString("#define DATABASE_VERSION_SPC").length()+1;

	int nIdx_1=content.indexOf("\n",nIdx);
	if (nIdx_1<0)
	{
		err.setError(1,"Failed to update version");
		return "";
	}
	QString strVersion=content.mid(nIdx,nIdx_1-nIdx).simplified().trimmed();
	nIdx=strVersion.indexOf(" ");
	if (nIdx>0)
	{
		strVersion=strVersion.left(nIdx).trimmed();
	}

	bool bOk=true;
	strVersion.toInt(&bOk);
	if (!bOk)
		return 0;
	else
		return strVersion;
}


QString BatchManager::FFH_ExtractCurrentVersion(Status &err)
{

	QString strConfigFile=g_AppServer->GetSettings()->m_strSVN_DIR+QString("/lib/common/common/config_version_ffh.h");

	QFile file(strConfigFile);
	if(!file.open(QIODevice::ReadOnly))
	{
		QString strMsg=tr("Error while reading file: ")+" "+strConfigFile;
		err.setError(1,strMsg);
		return "";
	}

	QByteArray content=file.readAll();
	file.close();

	int nIdx=content.indexOf("#define APPLICATION_VERSION");
	if (nIdx<0)
	{
		err.setError(1,"Failed to update version");
		return "";
	}

	int nIdx_1=content.indexOf("\"",nIdx);
	if (nIdx_1<0)
	{
		err.setError(1,"Failed to update version");
		return "";
	}

	int nIdx_2=content.indexOf("\"",nIdx_1+1);
	if (nIdx_2<0)
	{
		err.setError(1,"Failed to update version");
		return "";
	}

	QString strVersion=content.mid(nIdx_1+1,nIdx_2-nIdx_1-1);
	return strVersion;

}

//extract from major/minor/revision: return as major.minor.revision
void BatchManager::FFH_ExtractCurrentWebApp(Status &err, QString &strMajor,QString &strMinor, QString &strRevision)
{
	QString strConfigFile=g_AppServer->GetSettings()->m_strSVN_DIR+QString("/lib/common/common/config_version_ffh.h");

	QFile file(strConfigFile);
	if(!file.open(QIODevice::ReadOnly))
	{
		QString strMsg=tr("Error while reading file: ")+" "+strConfigFile;
		err.setError(1,strMsg);
		return;
	}

	QByteArray content=file.readAll();
	file.close();



	int nIdx=content.indexOf("#define JAMES_PROGRAM_VERSION_MAJOR");
	if (nIdx<0)
	{
		err.setError(1,"Failed to extract JAMES_PROGRAM_VERSION_MAJOR");
		return;
	}

	int nIdx_1=content.indexOf("\n",nIdx);
	if (nIdx_1<0)
	{
		err.setError(1,"Failed to extract JAMES_PROGRAM_VERSION_MAJOR");
		return;
	}

	nIdx+=QString("#define JAMES_PROGRAM_VERSION_MAJOR").length();
	strMajor=content.mid(nIdx+1,nIdx_1-nIdx-1).trimmed();


	nIdx=content.indexOf("#define JAMES_PROGRAM_VERSION_MINOR");
	if (nIdx<0)
	{
		err.setError(1,"Failed to extract JAMES_PROGRAM_VERSION_MINOR");
		return;
	}

	nIdx_1=content.indexOf("\n",nIdx);
	if (nIdx_1<0)
	{
		err.setError(1,"Failed to extract JAMES_PROGRAM_VERSION_MINOR");
		return;
	}

	nIdx+=QString("#define JAMES_PROGRAM_VERSION_MINOR").length();
	strMinor=content.mid(nIdx+1,nIdx_1-nIdx-1).trimmed();


	nIdx=content.indexOf("#define JAMES_PROGRAM_VERSION_REVISION");
	if (nIdx<0)
	{
		err.setError(1,"Failed to extract JAMES_PROGRAM_VERSION_REVISION");
		return;
	}

	nIdx_1=content.indexOf("\n",nIdx);
	if (nIdx_1<0)
	{
		err.setError(1,"Failed to extract JAMES_PROGRAM_VERSION_REVISION");
		return;
	}

	nIdx+=QString("#define JAMES_PROGRAM_VERSION_REVISION").length();
	strRevision=content.mid(nIdx+1,nIdx_1-nIdx-1).trimmed();


}


//if bPutOneEntryBellow then it's private build, do not put on top of file...
void BatchManager::SC_UpdatePublicVersionXML(Status &err,QString strVersion, QString strClientExeName, QString strServerExeName, bool bPutOneEntryBellow)
{
	QString strConfigFile=g_AppServer->GetSettings()->m_strDEPLOY_DIR+"/version.xml";
	QFile file(strConfigFile);
	if(!file.open(QIODevice::ReadOnly))
	{
		QString strMsg=tr("Error while reading file: ")+" "+strConfigFile;
		err.setError(1,strMsg);
		return;
	}
	QByteArray content=file.readAll();
	file.close();

	QFileInfo info_1(g_AppServer->GetSettings()->m_strDEPLOY_DIR+"/"+strClientExeName);
	int nSize=info_1.size();
	if (nSize==0)
		nSize=16*1024*1024;
	QString strSizeClient=DataHelper::GetFormatedFileSize(nSize,1,true);

	QFileInfo info_2(g_AppServer->GetSettings()->m_strDEPLOY_DIR+"/"+strServerExeName);
	nSize=info_2.size();
	if (nSize==0)
		nSize=16*1024*1024;
	QString strSizeServer=DataHelper::GetFormatedFileSize(nSize,1,true);


	QString strNewExeLine_Client="\n<CommunicatorClient category=\"Installation\" ID=\"1\" Title=\"SOKRATES Communicator PE, TE, BE\" Date=\"%1\" VersionID=\"%2\" DownloadURL=\"http://www.sokrates-communicator.com/downloads/%3\" Size_MB=\"%4\"></CommunicatorClient>";
	strNewExeLine_Client=strNewExeLine_Client.arg(QDate::currentDate().toString("dd.MM.yyyy")).arg(strVersion).arg(strClientExeName).arg(strSizeClient);
	strNewExeLine_Client+="\n";

	QString strNewExeLine_Server ="<CommunicatorAppServer category=\"Installation\" ID=\"1\" Title=\"SOKRATES Application Server for Communicator\" Date=\"%1\" VersionID=\"%2\" DownloadURL=\"http://www.sokrates-communicator.com/downloads/%3\" Size_MB=\"%4\"></CommunicatorAppServer>";
	strNewExeLine_Server=strNewExeLine_Server.arg(QDate::currentDate().toString("dd.MM.yyyy")).arg(strVersion).arg(strServerExeName).arg(strSizeServer);
	strNewExeLine_Server+="\n";

	int nIdx =0;

	if (!bPutOneEntryBellow)
	{
		nIdx=content.indexOf("<SOKRATES_Downloads>");
		if (nIdx<0)
		{
			err.setError(1,"Error while updating version.xml");
			return;
		}

		nIdx +=QString("<SOKRATES_Downloads>").size();
	}
	else
	{
		nIdx=content.indexOf("<CommunicatorAppServer");
		if (nIdx<0)
		{
			err.setError(1,"Error while updating version.xml");
			return;
		}
		nIdx=content.indexOf("<CommunicatorClient",nIdx);
		if (nIdx<0)
		{
			err.setError(1,"Error while updating version.xml");
			return;
		}
	}


	//nIdx +=QString("<SOKRATES_Downloads>").size();
	content.insert(nIdx,strNewExeLine_Client+strNewExeLine_Server);
	if (nIdx<0)
	{
		err.setError(1,"Error while updating version.xml");
		return;
	}

	if(!file.open(QIODevice::WriteOnly))
	{
		QString strMsg=tr("Error while writing file: ")+" "+strConfigFile;
		err.setError(1,strMsg);
		return;
	}
	file.write(content);
	file.close();
}
/*
void BatchManager::SVNCommit(Status &err,QString strDirectory)
{
	//commit:
	QStringList lstArgs;
	lstArgs<<"commit";
	lstArgs<<"--username";
	lstArgs<<SVN_USER;
	lstArgs<<"--password";
	lstArgs<<SVN_PASS;
	lstArgs<<"--message";
	lstArgs<<"Commit success";


	//lstArgs<<"-m ";
	//lstArgs<<"C:\\log.txt";

	QProcess tool;
	if (strDirectory.isEmpty())
		tool.setWorkingDirectory(g_AppServer->GetSettings()->m_strSVN_DIR);
	else
		tool.setWorkingDirectory(strDirectory);

	tool.start("svn",lstArgs);
	if (!tool.waitForFinished(-1)) //wait forever
	{	
		QByteArray errContent=tool.readAllStandardOutput();
		err.setError(1,"Error SVN commit: "+QString(errContent.left(4095)));
		return;
	}
	int nExitCode=tool.exitCode();
	QByteArray errContent=tool.readAllStandardError();
	if (nExitCode!=0 || errContent.size()!=0)
	{
		if (errContent.size()!=0)
			err.setError(1,"Error SVN commit 2: "+QString(errContent.left(4095)));
		else
			err.setError(1,"Error SVN commit 2: "+QString(tool.readAllStandardOutput().right(4095)));
		return;
	}

}
*/

void BatchManager::ErrorHandler(Status &err,int nTaskID,QString strAdditionalDesc)
{
	QString strMsg="Failed to execute task id: "+QString::number(nTaskID)+" Error: "+err.getErrorText();
	if (!strAdditionalDesc.isEmpty())
		strMsg+="\n\r "+strAdditionalDesc;
	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,strMsg);

	//send email to admin:
	if (!g_AppServer->GetSettings()->m_strMailAdminAddress.isEmpty())
	{
		SendEmail(err,QString("[Deployment Notification] %1 Reported Error").arg(g_AppServer->GetSettings()->m_strServerName),strMsg);
		if (!err.IsOK())
		{
			QString strMsg="Failed to send mail to: "+g_AppServer->GetSettings()->m_strMailAdminAddress+" Error: "+err.getErrorText();
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,strMsg);
		}
	}
}


void BatchManager::SucessHandler(Status &err,int nTaskID)
{
	QString strMsg=QString("[Deployment Notification] %1 successfully executed task %2").arg(g_AppServer->GetSettings()->m_strServerName).arg(GetTaskName(nTaskID));
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,strMsg);

	//send email to admin:
	if (!g_AppServer->GetSettings()->m_strMailAdminAddress.isEmpty())
	{
		SendEmail(err,QString("[Deployment Notification] %1 successfully executed task %2").arg(g_AppServer->GetSettings()->m_strServerName).arg(GetTaskName(nTaskID)),strMsg);
		if (!err.IsOK())
		{
			QString strMsg="Failed to send mail to: "+g_AppServer->GetSettings()->m_strMailAdminAddress+" Error: "+err.getErrorText();
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,strMsg);
		}
	}
}



void BatchManager::SendEmail(Status &err, QString strSubject, QString strBody)
{
	if(g_AppServer->GetSettings()->m_strMailServer.isEmpty())
		return;

	SMTPClient client;

	err.setError(0);
	QList<QString> lstBcc;
	QString strFrom=g_AppServer->GetSettings()->m_strMailServerAddress;
	if(!client.SendMail(g_AppServer->GetSettings()->m_strMailServer,strFrom,g_AppServer->GetSettings()->m_strMailAdminAddress,strSubject,strBody,lstBcc,g_AppServer->GetSettings()->m_strMailServerUserName,g_AppServer->GetSettings()->m_strMailServerPassword))
	{
		err.setError(1,client.GetLastError());
	}
}

void BatchManager::MakeSetup(QString strSetupBatFile,QString strSetupFile,Status &err)
{
	//commit:
	QStringList lstArgs;
	lstArgs<<strSetupFile;
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strDEPLOY_DIR);
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strInnoSetupPath);

	QProcess tool;
	//tool.setWorkingDirectory(g_AppServer->GetSettings()->m_strDEPLOY_DIR);
	tool.setWorkingDirectory(QCoreApplication::applicationDirPath());

	tool.start(strSetupBatFile,lstArgs);
	if (!tool.waitForFinished(-1)) //wait forever
	{	
		QByteArray errContent=tool.readAllStandardOutput();
		err.setError(1,"Error Make Setup: "+QString(errContent.left(4095)));
		return;
	}
	int nExitCode=tool.exitCode();
	QByteArray errContent=tool.readAllStandardError();
	if (nExitCode!=0 || errContent.size()!=0)
	{
		if (errContent.size()!=0)
			err.setError(1,"Error Make Setup: "+QString(errContent.left(4095)));
		else
			err.setError(1,"Error Make Setup: "+QString(tool.readAllStandardOutput().right(4095)));
		return;
	}

}

void BatchManager::FerrariRestCall(Status &err,int nTaskID)
{
	//CONNECT:
	RestHTTPClient client;
	client.SetConnectionSettings(FERRARI_DEPLOY_REST_IP,FERRARI_DEPLOY_REST_PORT,true);
	client.Connect(err);
	if (!err.IsOK())
		return;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"FerrariRestCall RestStartSession");

	QString strSession;
	client.RestStartSession(err,RestHTTPClient::REST_TYPE_JAMES,ROOT_USER,ROOT_PASS,strSession);
	if (!err.IsOK())return;

	//PREPARE TO SEND:
	client.msg_send.AddParameter(&nTaskID,"nTaskID");

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"FerrariRestCall RestSend");

	//SEND:
	client.RestSend(err,"POST","/rest/service/execute",strSession);
	if (!err.IsOK())return;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"FerrariRestCall RestEndSession");

	client.RestEndSession(err,RestHTTPClient::REST_TYPE_JAMES,strSession);
	if (!err.IsOK())return;
}


void BatchManager::RunBatScript(Status &err,QString strScriptName, QStringList &lstArgs)
{
	QProcess tool;
	//tool.setWorkingDirectory(g_AppServer->GetSettings()->m_strDEPLOY_DIR);
	tool.setWorkingDirectory(QCoreApplication::applicationDirPath());

	tool.start(strScriptName,lstArgs);
	if (!tool.waitForFinished(-1)) //wait forever
	{	
		QByteArray errContent=tool.readAllStandardOutput();
		err.setError(1,QString("Script %1 returned error timeout: %2").arg(strScriptName).arg(QString(errContent.left(4095))));
		return;
	}
	int nExitCode=tool.exitCode();
	QByteArray errContent=tool.readAllStandardError();
	if (nExitCode!=0 && errContent.size()!=0)
	{
		err.setError(1,QString("Script %1 returned error on exit: %2").arg(strScriptName).arg(QString(errContent.left(4095))));
		return;
	}
	if (nExitCode!=0 || errContent.size()!=0)
	{
		if (errContent.size()!=0)
			err.setError(1,QString("Script %1 returned error on exit: %2").arg(strScriptName).arg(QString(errContent.left(4095))));
		else
			err.setError(1,QString("Script %1 returned error on exit: %2").arg(strScriptName).arg(QString(tool.readAllStandardOutput().right(4095))));
		return;
	}

}

void BatchManager::ExecuteTask_ServerFarmDeployNewFiles()
{
	Status err;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start serverfarm_deploy.bat");

	//run deploy script
	QStringList lstArgs;
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strDEPLOY_DIR);
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strServerFarmPath);

	RunBatScript(err,"serverfarm_deploy.bat",lstArgs);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_SERVERFARM_DEPLOY_NEW_FILES);
		return;
	}

	SucessHandler(err,TASK_SERVERFARM_DEPLOY_NEW_FILES);


}

void BatchManager::ExecuteTask_ServerFarmUpdatePrivateServers()
{
	Status err;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start serverfarm_start_private.bat");

	//run deploy script
	QStringList lstArgs;
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strServerFarmPath);

	RunBatScript(err,"serverfarm_start_private.bat",lstArgs);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_SERVERFARM_UPDATE_PRIVATE_SERVERS);
		return;
	}

	SucessHandler(err,TASK_SERVERFARM_UPDATE_PRIVATE_SERVERS);
}

void BatchManager::ExecuteTask_ServerFarmUpdatePublicServers()
{
	QString strDateTommorow=QDate::currentDate().addDays(1).toString("dd.MM.yyyy");
	//run script

	Status err;
	//run deploy script
	QStringList lstArgs;
	lstArgs<<strDateTommorow;
	lstArgs<<QDir::toNativeSeparators(g_AppServer->GetSettings()->m_strServerFarmPath);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Start serverfarm_start_private.bat");

	RunBatScript(err,"serverfarm_start_private.bat",lstArgs);
	if (!err.IsOK())
	{
		ErrorHandler(err,TASK_SERVERFARM_UPDATE_PUBLIC_SERVERS);
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"End serverfarm_start_private.bat");

	SucessHandler(err,TASK_SERVERFARM_UPDATE_PUBLIC_SERVERS);

}


