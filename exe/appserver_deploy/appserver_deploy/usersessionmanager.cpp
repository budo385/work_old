#include "usersessionmanager.h"
#include "common/common/sha256hash.h"
#include "common/common/threadid.h"
#include "common/common/datahelper.h"

#include "common/common/authenticator.h"
#include "common/common/config_version_ffh.h"

#include "common/common/logger.h"
extern Logger			 g_Logger;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;


UserSessionManager::UserSessionManager()
{

}

UserSessionManager::~UserSessionManager()
{

}


void UserSessionManager::CreateSessionForWebService(Status &pStatus,QString &strSession,QString strUserName, QString strAuthToken,QString strNonce,int nProgCode,QString strProgVersionNumber, QString strClientID, QString strPlatformID)
{
	//authenticate user:
	QByteArray bytePassword = Authenticator::GeneratePassword(ROOT_USER,ROOT_PASS); //only this is valid

	if(!Authenticator::AuthenticateFromWebService(strNonce,strAuthToken,strUserName,bytePassword))
	{pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	HTTPContext ctx=g_AppServer->GetThreadContext(ThreadIdentificator::GetCurrentThreadID());
	if (ctx.m_nSocketID==-1)
	{
		pStatus.setError(1,"Invalid connection context");
		return;
	}

	//create new session:
	Sha256Hash Hasher;
	pStatus.setError(0);
	int nTries=10;
	int nStoredSessionID;
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Creating session, thread:"+QVariant(nCurrentThreadID).toString());

	//insert or update
	UserSessionData newSession(nCurrentThreadID,ctx.m_strPeerIP,ctx.m_nSocketID);

	//get max license count?
	//create unique session:
	bool bSessionCreated=false;
	while(nTries>0)
	{
		QByteArray byteRandomString=QTime::currentTime().toString("hh:mm:ss.zzz").toLatin1()+QVariant(nCurrentThreadID).toString().toLatin1()+QVariant(ctx.m_nSocketID).toString().toLatin1()+QVariant(ctx.m_strPeerIP).toString().toLatin1();
		strSession=Hasher.GetHash(byteRandomString).toBase64(); //32-50byte length hashed session as base 64 encoded string
		if(FindSession(strSession)==-1)
		{
			bSessionCreated=true;
			break;
		}
		nTries--;
	}
	if(nTries==0 && !bSessionCreated)
	{
		pStatus.setError(1,"Failed to create session"); 
		return;
	}

	//store all in list
	//------------------------------------------------
	QWriteLocker locker(&m_SessionListRWLock);

	newSession.nSocketID=ctx.m_nSocketID;
	newSession.nThreadID=nCurrentThreadID;
	newSession.strClientIP=ctx.m_strPeerIP;
	newSession.strSession=strSession;
	newSession.strUserName=strUserName;
	newSession.nProgCode=nProgCode;
	newSession.strProgVersionNumber=strProgVersionNumber;
	newSession.strClientID=strClientID;
	newSession.strPlatformID=strPlatformID;
	m_SessionList[nCurrentThreadID]=newSession;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_CURRENT,QVariant(m_SessionList.size()).toString());
}


void UserSessionManager::CreateTempRootSessionForAuthenticatedConnection(Status &pStatus,QString &strSession)
{
	QString strNonce=QString(Authenticator::GenerateRandomSequence(QString::number(SERVER_JAMES_WEBSERVICE_EXPIRE).toLatin1()).toHex()).toUtf8();
	QString strAuthToken=Authenticator::GenerateAuthenticationTokenForWebService("root","root",strNonce);

	CreateSessionForWebService(pStatus,strSession,"root", strAuthToken,strNonce,0 /*CLIENT_CODE_FFH_PRO*/,"","","");
}


void UserSessionManager::ValidateWebUserRequest(Status &err,QString strSession,const HTTPContext &ctx)
{
	UserSessionData SessionRow;
	err.setError(0);
	//set write lock
	QWriteLocker locker(&m_SessionListRWLock);
	int nIdx=FindSession(strSession);
	if (nIdx>=0)
	{
		SessionRow=m_SessionList.value(nIdx);
		int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		if (ctx.m_strPeerIP!=SessionRow.strClientIP) //must originate from same IP: allow other socket/thread
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
			return;
		}

		if (nIdx!=nCurrentThreadID)
			m_SessionList.remove(nIdx); //remove old thread, set new

		SessionRow.nSocketID=ctx.m_nSocketID;
		SessionRow.nThreadID=nCurrentThreadID;
		SessionRow.strClientIP=ctx.m_strPeerIP;
		SessionRow.datLastActivity=QDateTime::currentDateTime();
		
		m_SessionList[nCurrentThreadID]=SessionRow; 
	}
	else
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
	}
}


void UserSessionManager::DeleteSession(Status &pStatus, QString strSession)
{
	QWriteLocker locker(&m_SessionListRWLock);
	int nIdx=FindSession(strSession);
	if (nIdx<0)
		return;

	UserSessionData SessionRow=m_SessionList.value(nIdx);
	m_SessionList.remove(nIdx);
	ClearUserSessionStorage(strSession);
}

void UserSessionManager::DeleteExpiredUserSessions(QList<int> &lstSocketsToRemove)
{
	QWriteLocker locker(&m_SessionListRWLock);
	UserSessionData SessionRow;
	UserSessionListIterator i(m_SessionList);

	while (i.hasNext()) 
	{
		i.next();
		SessionRow=m_SessionList.value(i.key()); //get session

		//qDebug()<<"testing session: "<<SessionRow.strSession;

		if (SessionRow.bThreadActive)		//skip system sessions and active ones
			continue;

		//check dates and if some1 already set to expire
		if(SessionRow.datLastActivity.addSecs(SERVER_JAMES_WEBSERVICE_EXPIRE)<QDateTime::currentDateTime())
		{
			//qDebug()<<"Removing session: "<<SessionRow.strSession;
			m_SessionList.remove(i.key());
			ClearUserSessionStorage(SessionRow.strSession);
			lstSocketsToRemove.append(SessionRow.nSocketID);
		}
	}
}




//------------------------------------------------------------------------------------------------------
//							PUBLIC GET CONTEXT HANDLERS
//------------------------------------------------------------------------------------------------------


QString UserSessionManager::GetSessionID()
{

	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	QString strSession;

	QReadLocker locker(&m_SessionListRWLock);

	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		strSession=m_SessionList.value(nCurrentThreadID).strSession;
	else
	{
		strSession="";
	}

	return strSession;
};



void UserSessionManager::LockThreadActive()
{
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	QWriteLocker locker(&m_SessionListRWLock);

	if( m_SessionList.contains(nCurrentThreadID))
		m_SessionList[nCurrentThreadID].bThreadActive=true;
}

void UserSessionManager::UnLockThreadActive()
{
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	QWriteLocker locker(&m_SessionListRWLock);

	if( m_SessionList.contains(nCurrentThreadID))
	{
		m_SessionList[nCurrentThreadID].bThreadActive=false;
		m_SessionList[nCurrentThreadID].datLastActivity=QDateTime::currentDateTime();
	}
}



QString UserSessionManager::GetClientIP(QString strSession)
{
	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return "";//return invalid ID if not found
	}

	QString strClientIP;

	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		strClientIP=m_SessionList.value(nCurrentThreadID).strClientIP;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		strClientIP="";
	}
	return strClientIP;
}

QString UserSessionManager::GetUserNameExt(QString strSession)
{
	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return "";//return invalid ID if not found
	}

	QString strUserName;

	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		strUserName=m_SessionList.value(nCurrentThreadID).strUserName;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		strUserName="";
	}
	return strUserName;
}

int UserSessionManager::GetUserProgCode(QString strSession)
{
	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return -1;//return invalid ID if not found
	}

	return m_SessionList.value(nCurrentThreadID).nProgCode;
}




//------------------------------------------------------------------------------------------------------
//							PRIVATE HELPER
//------------------------------------------------------------------------------------------------------




/*!
	Finds Session in list. WARNING: no locking is done, but if u want safe way plz do lock before calling
	\return key or -1 if not found

*/
int UserSessionManager::FindSession(QString strSession)
{
	//qDebug()<<" find session: "<<strSession;

	UserSessionListIterator i(m_SessionList);
	while (i.hasNext()) 
	{
		i.next();
		//QString strSess=i.value().strSession;
		//qDebug()<<strSess;

		//qDebug()<<" find test session: "<<i.value().strSession;

		if(i.value().strSession==strSession)
			return i.key();
	}
		
	return -1;		
}


/*!
	Finds Socket in list. WARNING: no locking is done, but if u want safe way plz do lock before calling
	\return key or -1 if not found

*/
int UserSessionManager::FindSocket(int nSocketID)
{
		UserSessionListIterator i(m_SessionList);
		while (i.hasNext()) 
		{
			i.next();
			if(i.value().nSocketID==nSocketID)
				return i.key();
		}
		
	return -1;		
}







int UserSessionManager::GetSessionCount()
{
	QReadLocker locker(&m_SessionListRWLock);
	return m_SessionList.size();
}


void UserSessionManager::ClearUserSessionStorage(QString strSession)
{
	if(strSession.isEmpty())
		strSession=GetSessionID();
	if (strSession.isEmpty())
		return;

	QString strDirName=DataHelper::EncodeSession2Directory(strSession);
	DataHelper::RemoveAllFromDirectory(m_strUserStorageDirectoryPath+"/"+strDirName,true,true);
}


QString UserSessionManager::InitializeUserStorageDirectory(Status &status,QString strUserSubDir)
{
	status.setError(0);
	QString strUserDirectory=m_strUserStorageDirectoryPath+"/"+strUserSubDir;

	QDir dirUser(strUserDirectory);
	if (!dirUser.exists())
	{
		QDir dirMain(m_strUserStorageDirectoryPath); //must exists!
		if (!dirMain.exists())
		{
			status.setError(1,QString("Directory %1 does not exists! Service Unavailable!").arg(m_strUserStorageDirectoryPath));
			return "";
		}

		bool bOK=dirMain.mkdir(strUserSubDir);
		if (!bOK)
		{
			status.setError(1,QString("Failed to create user storage directory %1").arg(strUserSubDir));
			return "";
		}
	}

	return strUserDirectory;
}


bool UserSessionManager::AuthenticateUser(QString strCookieValue,QString &strSessionID)
{
	QStringList lstAuth=strCookieValue.split(";");
	int nSize=lstAuth.size();
	for (int i=0;i<nSize;++i)
	{
		QString strCookie=lstAuth.at(i).trimmed();
		if (strCookie.left(QString("web_session").length())=="web_session")
		{
			QString strCookieSession=QUrl::fromPercentEncoding(lstAuth.at(i).toLatin1());

			int nIdx=strCookieSession.indexOf("=");
			if (nIdx>=0)
			{
				QReadLocker locker(&m_SessionListRWLock);
				strSessionID=strCookieSession.mid(nIdx+1).trimmed();
				if(FindSession(strSessionID)>=0)
				{
					return true;
				}
			}
		}
	}

	return false;
}


void UserSessionManager::GetSessionList(UserSessionList &pListSessions)
{
	QReadLocker locker(&m_SessionListRWLock);
	pListSessions=m_SessionList;
}
