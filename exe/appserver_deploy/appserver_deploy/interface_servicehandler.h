#ifndef INTERFACE_SERVICEHANDLER_H
#define INTERFACE_SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_ServiceHandler
{
public:
	virtual ~Interface_ServiceHandler(){};

	virtual void GetTasks(Status &err, DbRecordSet &Ret_Tasks)=0;
	virtual void ExecuteTask(Status &err, int nTaskID)=0;
	virtual void Login(Status& pStatus, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode=0, QString strProgVer="", QString strClientID="", QString strPlatform="")=0;
	virtual void Logout(Status &pStatus,QString strSessionID)=0;

	
};

/*
<Web_service_meta_data>
	
	<GetTasks>
		<URL>/service/tasks</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</GetTasks>

	<ExecuteTask>
		<URL>/service/execute</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</ExecuteTask>

	<Login>
		<URL>/service/login</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</Login>

	<Logout>
		<URL>/service/logout</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</Logout>

	
</Web_service_meta_data>
*/


#endif // INTERFACE_SERVICEHANDLER_H
