#include "batchmanagertaskexecutor.h"
#include "batchmanager.h"

BatchManagerTaskExecutor::BatchManagerTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent)
:BackgroundTaskExecutor(pTask,parent,0,false)
{
	BatchManager *p=dynamic_cast<BatchManager*>(pTask);
	connect(p,SIGNAL(SignalProcessTask(int)),this,SLOT(OnProcessTask(int)));
}




void BatchManagerTaskExecutor::OnProcessTask(int nTaskID)
{
	StartNow(-1,nTaskID,"");
}