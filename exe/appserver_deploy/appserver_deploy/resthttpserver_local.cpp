#include "resthttpserver_local.h"

#include "usersessionmanager.h"
extern UserSessionManager *g_UserSessionManager;

bool RestHTTPServer_Local::AuthenticateUser(Status &err,HTTPContext &ctx,HTTPRequest &request,HTTPResponse &response, int &nClientTimeZoneOffsetMinutes)
{
	err.setError(0);

	//qDebug()<<"-----------------HTTP rest request-----------------------";
	//qDebug()<<request.toString();
	//qDebug()<<"----------------------------------------------------------";

	if (request.path().right(6).toLower()=="/login" || request.path().right(12).toLower()=="/server_data")
	{
		return true;
	}

	//test session cookie:
	QString strSession;
	if(g_UserSessionManager->AuthenticateUser(request.value("Cookie"),strSession))
	{
		g_UserSessionManager->ValidateWebUserRequest(err,strSession,ctx);
		if (!err.IsOK())
		{
			SetResponseHeader(request,response,401,"Unauthorized");
			return false;
		}
		return true;
	}

	SetResponseHeader(request,response,401,"Unauthorized");
	return false;
}
void RestHTTPServer_Local::LockThreadActive()
{
	
}
void RestHTTPServer_Local::UnLockThreadActive()
{
	
}

