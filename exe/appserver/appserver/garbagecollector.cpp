#include "garbagecollector.h"
#include "bus_server/bus_server/usersessionmanager_server.h"
#include "common/common/datahelper.h"

#include "common/common/logger.h"
extern Logger				g_Logger;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_server/bus_server/systemserviceset.h"
extern SystemServiceSet* g_SystemServiceSet;	
#include "bus_server/bus_server/privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "db/db/dbsqlmanager.h"
extern DbSqlManager *g_DbManager;
#include "message_notificator.h"
extern MessageNotificator*				g_MessageNotificator;

GarbageCollector::GarbageCollector(HTTPServer *pHttpServer, int nServerMode, int nAppServerSessionID)
{
	m_nServerMode=nServerMode;
	m_nAppServerSessionID=nAppServerSessionID;
	m_HttpServer=pHttpServer;

	//when HTTP detect client disconnection, pass it here to clean up his mess
	connect (m_HttpServer,SIGNAL(ClientThreadIsDead(int)),this,SLOT(ClientClosedConnection(int)),Qt::QueuedConnection);		//when server detects disconnect, delete session

}

GarbageCollector::~GarbageCollector()
{

}


//start every time, clears: user session table & core locking (cascade delete on DB)
//only used on APP. server (not on THICK)
void GarbageCollector::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	Status err;
	QSqlDatabase *pDbConn=NULL;
	DbConnectionReserver conn(err,g_DbManager); //reserve one connection to avoid multi connection lock
	if(!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GARBAGE_FAILED,"Failed to reserve database connection: "+err.getErrorText());
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());


	//----------------------------------------------------------------------------------------------------------
	//1. st: check inactive sessions: delete them (only if last modify < (current-is_alive_period)
	//----------------------------------------------------------------------------------------------------------
	g_SystemServiceSet->UserSession->DeleteInactiveSessions(err,m_nAppServerSessionID);
	if (!err.IsOK())
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GARBAGE_FAILED,"Failed to delete inactive sessions: "+err.getErrorText());
	

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,"Garbage Collector: 1st check completed");

	//----------------------------------------------------------------------------------------------------------
	//2nd: Delete Expire sessions & sockets
	//----------------------------------------------------------------------------------------------------------

	UserSessionList lstSessions;
	g_UserSessionManager->TestExpiredUserSessions(err,lstSessions);
	if (!err.IsOK())
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GARBAGE_FAILED,"Failed to delete expired sessions: "+err.getErrorText());

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Garbage Collector: TestExpiredUserSessions returned %1 records").arg(lstSessions.size()));

	/*
	//disconnect them from HTTP server, one by one, from this GBC thread (http server will function just fine)
	UserSessionListIterator i(lstSessions);
	QList<int> lstSocketsToDelete;
	while (i.hasNext()) 
	{
		i.next();

		QString strUserSes="deleted session: last_dat_actualized: "+i.value().datLastActivity.toString();
		strUserSes+=", soc: "+QVariant(i.value().nSocketID).toString();
		strUserSes+=", thread: "+QVariant(i.value().nThreadID).toString();
		strUserSes+=", ip: "+i.value().strClientIP;

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Deleting socket: %1").arg(strUserSes));
		if(!m_HttpServer->CloseConnection(i.value().nThreadID))
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Socket: %1 is still in use!").arg(i.value().nSocketID));
			g_UserSessionManager->ClientIsAlive(i.value().nSocketID);	//client socket is in use
		}
		else
		{
			lstSocketsToDelete.append(i.value().nSocketID);				//client socket is good for delete
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Socket deleted: %1").arg(i.value().nSocketID));
		}
		
	}
	if(lstSocketsToDelete.size()!=0)
	{
		g_UserSessionManager->DeleteExpiredUserSessions(err,lstSessions);
		if (err.IsOK())
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_CLEANED_EXPIRED_SESSIONS,QVariant(lstSocketsToDelete.size()).toString());
		else
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GARBAGE_FAILED,"Failed to delete expired sessions/connections: "+err.getErrorText());
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,"Garbage Collector: 2nd check completed");
	

	//----------------------------------------------------------------------------------------------------------
	//3rd: Delete Orphan client threads/sockets (can be when DFO process is done when server changes address):
	//NOTE: HTML server can open muiltiple sockets with keep alive option on all of them, GB MUST not delete those sockets even if they appear as orphans
	//2 approaches: 
	// a) let every handler determine max socket timeout for orphan sockets (not connected to the session)
	// b) fix orphan socket timeout:
	//In any case, socket information inside HTTP server must have some timeout counter or similiar
	//----------------------------------------------------------------------------------------------------------
	ClientConnections lstSockets;
	m_HttpServer->GetSocketList(lstSockets);
	g_UserSessionManager->GetSessionList(lstSessions);
	ClientConnectionsIterator z(lstSockets);
	lstSocketsToDelete.clear();
	while (z.hasNext()) //find orphan sockets that exists in HTTP server but not inside user session list
	{
		z.next();
		int nSocketID=z.key();
		bool bFound=false;
		UserSessionListIterator k(lstSessions);
		while (k.hasNext()) 
		{
			k.next();
			if(k.value().nSocketID==nSocketID)
			{
				bFound=true;
				break;
			}
		}
		if (!bFound)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Garbage Collector: Found unregistered socket %1 (not in session list) Step A").arg(nSocketID));

			//BT. 03.04.2009: when socket used by HTTP layer(user_storage,ping web) or in process of connecting, skip deletion:
			int nSocketState=z.value()->GetSocketState();
			if (nSocketState==HTTPServerSocketHandler::STATE_NOT_CONNECTED || nSocketState==HTTPServerSocketHandler::STATE_READY)
			{
				//BT. 15.02.2010: every handler can set own orphan socket timeout (this is designed for HTML handler: 20minutes timeout)
				if (z.value()->GetOrphanSocketTimeOut()>0)
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Garbage Collector: Found unregistered socket %1 (not in session list) Step B").arg(nSocketID));

					if (z.value()->GetSocketCreatedTime().addSecs(z.value()->GetOrphanSocketTimeOut()*60)>QDateTime::currentDateTime())
					{
						g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GARBAGE_FAILED,QString("Socket created at %1, expired at %2 ").arg(z.value()->GetSocketCreatedTime().toString("MM.dd.yyyy")).arg(QDateTime::currentDateTime().toString("MM.dd.yyyy")));
						lstSocketsToDelete.append(nSocketID);
					}
				}
				else
				{
						g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Garbage Collector: Found unregistered socket %1 (not in session list) Step C").arg(nSocketID));
						lstSocketsToDelete.append(nSocketID);
				}
			}
			else
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Garbage Collector: Trying to delete but it was busy unregistered socket %1 (not in session list) with state %2").arg(nSocketID).arg(nSocketState));
				//qDebug()<<"trying to delete socket: "<<nSocketID<<" but socket is busy: "<<nSocketState;
			}

			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Garbage Collector: Done with unregistered socket %1 (not in session list)").arg(nSocketID));
		}
	}
	if(lstSocketsToDelete.size()!=0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Garbage Collector: lstSocketsToDelete: deleting orphan sockets"));
		m_HttpServer->CloseConnection(lstSocketsToDelete);
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_CLEANED_ORPHAN_SOCKETS,QVariant(lstSocketsToDelete.size()).toString());
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,"Garbage Collector: 3rd check completed");

	*/

	//----------------------------------------------------------------------------------------------------------
	//4th: Delete Orphan sessions: if crashed client, thread can be terminated->sesion remains in memory+db as active with threadactive flag=1
	//----------------------------------------------------------------------------------------------------------
/*
	UserSessionList lstSessionsToDelete;
	UserSessionListIterator k(lstSessions); //all sessions
	while (k.hasNext()) 
	{
		k.next();
		if (k.value().bIsSystemThread) //if system thread skip: for import/export operations...
			continue;
		int nSocketID=k.value().nSocketID;
		HTTPServerThread* pClientThread=lstSockets.value(nSocketID,NULL);
		if (pClientThread==NULL) //session exists but socket not: delete: is alive can not detect this coz of: threadactiev flag=1
			lstSessionsToDelete[k.value().nThreadID]=k.value();
	}
	if(lstSessionsToDelete.size()!=0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,"Garbage Collector:DeleteOrphanUserSessions started");

		g_UserSessionManager->DeleteOrphanUserSessions(err,lstSessionsToDelete);
		if (err.IsOK())
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_CLEANED_ORPHAN_SESSIONS,QVariant(lstSessionsToDelete.size()).toString());
		else
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GARBAGE_FAILED,"Failed to delete orphan client sessions: "+err.getErrorText());
	}
	
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,"Garbage Collector: 4th check completed");
*/

	//----------------------------------------------------------------------------------------------------------
	//5th: Delete user_storage directory (app.ini->enable user storage/disk quota per user!?)
	//----------------------------------------------------------------------------------------------------------
	//get all from user_storage
	QString strUserStorageDirectoryPath =g_UserSessionManager->GetUserStorageDirectoryPath();
	if (!strUserStorageDirectoryPath.isEmpty())
	{
		QList<QString> lstDirs=DataHelper::GetSubDirectories(strUserStorageDirectoryPath);
		UserSessionListIterator k(lstSessions); //all sessions
		while (k.hasNext()) 
		{
			k.next();
			QString strDirName=DataHelper::EncodeSession2Directory(k.value().strSession);
			int nPos=lstDirs.indexOf(strDirName);
			if (nPos!=-1)
				lstDirs.removeAt(nPos);
		}
		//if something left: delete:
		int nSize=lstDirs.size();
		for(int i=0;i<nSize;i++)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Garbage Collector: RemoveAllFromDirectory %1").arg(strUserStorageDirectoryPath+"/"+lstDirs.at(i)));
			DataHelper::RemoveAllFromDirectory(strUserStorageDirectoryPath+"/"+lstDirs.at(i),true,true);
		}
	}

	//PUSH MSG clean up:
	g_MessageNotificator->CleanUp(err);
	if (!err.IsOK())
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GARBAGE_FAILED,"Failed to clean up old push messages: "+err.getErrorText());


	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,"Garbage Collector: All completed!");

	
}



//When HTTP or other closes connection, clear session
void GarbageCollector::ClientClosedConnection(int nThreadID)
{
	Status err;
	//QString strUserSession=dynamic_cast<UserSessionManager_Server*>(g_UserSessionManager)->GetSessionIDBySocketID(nSocketID);
	//int nThreadID=dynamic_cast<UserSessionManager_Server*>(g_UserSessionManager)->GetThreadBySocketID(nSocketID);
	//if(nThreadID==-1) //client never made to usersession manager or normal disconnect event
	//	return;

	//qDebug()<<"Socket self closed "<<nSocketID;

	//destroy all user data in one place:
	g_PrivateServiceSet->CoreUser->DestroyUserContextData(err,"",nThreadID);

}

/*
//if not create session for GBC:
void GarbageCollector::CreateSession(Status &err)
{
	if (m_strSystemSession.isEmpty())
	{
		DbRecordSet rowUserData;
		g_PrivateServiceSet->CoreUser->GetUserByUserName(err,"system",rowUserData);
		if (!err.IsOK())return;
		g_UserSessionManager->CreateSession(err,m_strSystemSession,rowUserData,"","",true);
		if (!err.IsOK())return;
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Garbage collector using session:"+m_strSystemSession);
	}
}
*/