#ifndef APPLICATIONSERVERSELFTEST_H
#define APPLICATIONSERVERSELFTEST_H

#include "common/common/dbrecordset.h"
#include "common/common/backgroundtaskinterface.h"
#include "common/common/status.h"
#include <QMutex>

class ApplicationServerSelfTest: public QObject, public BackgroundTaskInterface
{
	Q_OBJECT

public:
	ApplicationServerSelfTest();
	~ApplicationServerSelfTest();

	void ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");		

	void AddUserEmailAccountsForTest();
	void SendEmailSamplesForTest();

private:
	QString ConvertNumValue(int nValue);
	void LoadPersonEmailInAccounts(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData);
	void SavePersonEmailInAccounts(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData);
	void AddUserEmailAccountIn( Status &Ret_pStatus, int nPersonID, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, bool nIsImap, int nConnectionType, bool bAccountActive, int nDeleteEmailsAfterNoDays);
	int FindEmailInAccountIdx(const QString &strEmail, DbRecordSet &lstPersonAccData);
	QMutex					m_Mutex;
};

#endif // APPLICATIONSERVERSELFTEST_H

