#ifndef APPSERVER_H
#define APPSERVER_H

//COMMON
#include <QtCore>
#include "common/common/status.h"
#include "common/common/backgroundtaskexecutor.h"
#include "importexporttaskexecutor.h"
#include "serverremindertaskexecutor.h"
#include "bus_server/bus_server/serversessionmanager.h"
#include "backuptaskexecutor.h"
//#include "emailimportthread.h"
#include "emailimportmanager.h"

//TRANS
#include "bus_trans_server/bus_trans_server/xmlrpchttpserver.h"
#include "bus_trans_server/bus_trans_server/xmlrpcskeleton_serverset.h"
#include "trans/trans/httpserverconnectionsettings.h"
#include "trans/trans/httpserver.h"
#include "trans/trans/htmlhttpserver.h"
#include "bus_trans_server/bus_trans_server/resthttpserver_sokrates.h"
#include "bus_trans_server/bus_trans_server/userstoragehttpserver_sokrates.h"
#include "bus_trans_server/bus_trans_server/pinghttpserver.h"
#include "bus_trans_server/bus_trans_server/webservicerpcdispatcher.h"

//INI file:
#include "bus_core/bus_core/serverinifile.h"

//MODULE LICENSE:
#include "garbagecollector.h"
#include "mastergarbagecollector.h"
#include "bus_core/bus_core/servercontrolabstract.h"

#include "messagedispatcher_serverthread.h"



/*!
    \class ApplicationServer
    \brief Application server itself
    \ingroup AppServer

	Application server main object.
	Multi threaded, can be accessed from clients
*/
class ApplicationServer: public QObject, public ServerControlAbstract
{
    
	Q_OBJECT 


public:

	ApplicationServer(QObject *parent=NULL,QString strDescriptor="");
	~ApplicationServer();

	//main API
	void Start(Status &pStatus);
	void Stop();
	void Restart(Status &pStatus);
	void RestartDelayed(int nReStartAfterSec);
	void StopDelayed(int nStopAfterSec=0);
	bool IsRunning(){return m_bIsRunning;}
	void SaveSettings();
	HTTPContext GetThreadContext(int nThreadID){return SokratesHttpServer.GetThreadContext(nThreadID);};
	//demo:
	void TestDemoDatabaseSize(Status &pStatus,  bool &bFireWarningMsg, int &nCurrSizeKb, int &nMaxSizeKb); 
	ServerIniFile* GetIniFile(){return &m_INIFile;};
	void ReloadWebPages(Status &pStatus){m_HtmlHTTPServer->ReloadWebPages(pStatus);}

	void SendErrorMessage(int nPersonID, QString strErrorText, int nErrorCode=1, QString strEmailAccount="");
	void SendNewEmailNotification(int nPersonID, QString strEmailAccount,DbRecordSet &lstNewEmails);
	void SendPushNotification(int nPersonID, QString strMessage, QString strPushToken, int nOSType, QString strPushSoundName, int nBadgeNumber);
	
	HTTPServer* GetHttpServer(){return &SokratesHttpServer;};

	//note: timers can not restart from another thread, use signals
protected slots:
	void RestartDelayedThreadSafeSlot(int);
	void StopThreadSafeSlot();
	void OnBcpManager_RestoreCommandIssued(int);
	void OnFillDatabaseAtStartForTest();

protected:
	void timerEvent(QTimerEvent *event); //overriden QObject timer method

signals:
	void RestartDelayedThreadSafeSignal(int);
	void StopThreadSafeSignal();

private:
	void ClearPointers();
	void RestartDelayedPriv(int nReStartAfterSec);
	void LoadSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings, DbConnectionSettings &pDbSettings);
	void InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections=10,int nMinDbConnections=0);
	void ReadDemoFP();
	QString	InitUserStorage(QString strPath);

	ServerSessionManager		*m_ServerSessionManager;
	GarbageCollector			*m_GarbageCollector;
	MasterGarbageCollector		*m_MasterGarbageCollector;
	BackgroundTaskExecutor		*m_GbBkgTask;
	BackgroundTaskExecutor		*m_MasterGbBkgTask;
	BackgroundTaskExecutor		*m_SrvSessionBkgTask;
	BackgroundTaskExecutor		*m_EmailSubscribeBkgTask;
	BackupTaskExecutor			*m_SrvBackupBkgTask;
	ImportExportTaskExecutor	*m_SrvImportExportBkgTask;
	ServerReminderTaskExecutor	*m_SrvReminderBkgTask;
	EmailImportManager			*m_EmailSubscribe;
	BackgroundTaskExecutor		*m_MessageNotificatorTask;
	BackgroundTaskExecutor		*m_AppServerSelfTestTask;
	

	MessageDispatcher_ServerThread m_MsgDispatcherThread;

	//TRANSPORT
	XmlRpcSkeletonServerSet			*m_XmlRpcSkeletonServerSet;
	XmlRpcHTPPServer				*m_XmlRpcHTPPServer;
	HtmlHTTPServer					*m_HtmlHTTPServer;
	PingHTTPServer					*m_PingHttpServer;
	UserStorageHTTPServer_Sokrates	*m_UserStorageHTTPServer;
	RestHTTPServer_Sokrates			*m_RestHTTPServer;
	WebServiceRpcDispatcher			*m_WebServiceRpcDispatcher;


	HTTPServer SokratesHttpServer;

	//INI
	ServerIniFile m_INIFile;
	QString m_strIniFilePath;

	bool m_bIsRunning;
	int m_nTimerID;
	QMutex m_Mutex;					///< mutex
	QString m_strDescriptor;

	//demo figures (from FP/keyfile)
	int m_nMaxDemoDbKb;
	int m_nWarnDemoDbKb;
	bool m_bDemoDatabase;

};


#endif //USERSESSIONMANAGER_H



