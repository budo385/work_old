// received_date.cpp -- shows how to get the server time stamps

#include <mimepp/mimepp.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
using namespace std;


void printTimeStamps(mimepp::Headers& headers)
{
    int numFields = headers.numFields();
    for (int i=0; i < numFields; ++i) {
        const mimepp::FieldBody& fldBody = headers.fieldAt(i).fieldBody();
        if (fldBody.class_().id() == mimepp::STAMP_CLASS) {
            mimepp::Stamp& stamp = (mimepp::Stamp&) fldBody;
            cout << stamp.date().getString().c_str() << endl;
        }
    }
}

void readFile(mimepp::String& buf, const char* filename, bool binary=false)
{
    ios_base::openmode flags = ios_base::in;
    if (binary) {
        flags |= ios_base::binary;
    }
    ifstream in(filename, flags);
    if (! in) {
        cout << "Can't open file " << filename << endl;
        exit(1);
    }
    in.seekg(0, ios::end);
    int size = in.tellg();
    in.seekg(0, ios::beg);
    buf.reserve(size);
    char ch;
    while (in.get(ch)) {
        buf.append(1, ch);
    }
}

int main()
{
    mimepp::Initialize();

    // Create a new Message object
    mimepp::Message msg;
    // Set string representation of the message
    mimepp::String msgStr;
    readFile(msgStr, "full-msg.txt");
    msg.setString(msgStr);
    // Parse the message
    msg.parse();

    printTimeStamps(msg.headers());

    mimepp::Finalize();

    return 0;
}
