// add_attach.cpp -- example to show how to add an attachment to a message

#include <iostream>
#include <fstream>
#include <time.h>
#include <mimepp/mimepp.h>
using namespace mimepp;
using namespace std;

void readFile(mimepp::String& buf, const char* filename, bool binary=false)
{
    ios_base::openmode flags = ios_base::in;
    if (binary) {
        flags |= ios_base::binary;
    }
    ifstream in(filename, flags);
    if (! in) {
        cout << "Can't open file " << filename << endl;
        exit(1);
    }
    in.seekg(0, ios::end);
    int size = in.tellg();
    in.seekg(0, ios::beg);
    buf.reserve(size);
    char ch;
    while (in.get(ch)) {
        buf.append(1, ch);
    }
}

void makeMultipart(Message& msg)
{
    // If it's already a multipart/mixed message, do nothing.
    // The type must be "multipart/mixed", since we want to add attachments.
    if (msg.headers().contentType().typeAsEnum() != MediaType::MULTIPART
        || 0 != Strcasecmp(msg.headers().contentType().subtype(), "mixed")) {
        // Create a body part for the memo text (or whatever was in the
        // original message).  This must be the first body part.
        BodyPart* memoPart = new BodyPart;
        msg.body().addBodyPart(memoPart);
        // Use assignment operators to copy the headers and body
        memoPart->headers() = msg.headers();
        memoPart->body() = msg.body();
        // Remove header fields from the body part (all that are not
        // content-* header fields)
        Headers& partHeaders = memoPart->headers();
        int numFields = partHeaders.numFields();
        int i;
        for (i = numFields-1; i >= 0; --i) {
            Field& field = partHeaders.fieldAt(i);
            if (0 == Strncasecmp(field.fieldName(), "content-", 8)) {
                // keep this header field
            }
            else {
                partHeaders.removeFieldAt(i);
            }
        }
        // Remove content-* header fields from the message
        Headers& msgHeaders = msg.headers();
        numFields = msgHeaders.numFields();
        for (i = numFields-1; i >= 0; --i) {
            Field& field = msgHeaders.fieldAt(i);
            if (0 == Strncasecmp(field.fieldName(), "content-", 8)) {
                msgHeaders.removeFieldAt(i);
            }
        }
        // Set content-type header field of the message to "multipart/mixed"
        // and create a boundary string
        msgHeaders.contentType().setString("multipart/mixed");
        msgHeaders.contentType().parse();
        msgHeaders.contentType().createBoundary();
    }
}

void addAttachment(Message& msg, const char* filename, const char* mediaType)
{
    // Read the file content
    String content;
    bool binary = true;
    readFile(content, filename, binary);
    // Encode the file content
    Base64Encoder enc;
    content = enc.encode(content);
    // If the message is not a multipart message, make it so
    makeMultipart(msg);
    // Create a body part for the attachment
    BodyPart* attachPart = new BodyPart;
    msg.body().addBodyPart(attachPart);
    // Set content-type
    MediaType& contentType = attachPart->headers().contentType();
    contentType.setString(mediaType);
    contentType.parse();
    contentType.setName(filename);
    // Set content-disposition
    DispositionType& dispType = attachPart->headers().contentDisposition();
    dispType.setType("attachment");
    dispType.setFilename(filename);
    // Set content-transfer-encoding
    attachPart->headers().contentTransferEncoding().setType("base64");
    // Set the content
    attachPart->body().setString(content);
}

int main(int argc, char** argv)
{
    mimepp::Initialize();

    if (argc < 3) {
        cout << "Usage: " << argv[0] << " msg-file attach-file" << endl;
        exit(1);
    }
    const char* msgfile = argv[1];
    const char* attachfile = argv[2];
    // Read the message from the file
    String str;
    readFile(str, msgfile);
    // Create a message object
    Message msg(str);
    msg.parse();
    // Add the attachment
    addAttachment(msg, attachfile, "application/octet-stream");
    msg.assemble();
    // Print it to stdout
    cout.write(msg.getString().data(), (streamsize)msg.getString().length());

    mimepp::Finalize();

    return 0;
}
