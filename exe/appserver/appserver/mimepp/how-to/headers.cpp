// headers.cpp -- shows how to process header fields in a message or body part

#include <mimepp/mimepp.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
using namespace std;


void printAllHeaderFields(const mimepp::Headers& headers)
{
    int numFields = headers.numFields();
    for (int i=0; i < numFields; ++i) {
        const mimepp::Field& field = headers.fieldAt(i);
        cout << field.fieldName().c_str() << ": "
            << field.fieldBody().text().c_str()
            << endl;
    }
}

void processHeaderFields(const mimepp::Headers& headers)
{
    int numFields = headers.numFields();
    for (int i=0; i < numFields; ++i) {
        const mimepp::FieldBody& fieldBody = headers.fieldAt(i).fieldBody();
        int classId = fieldBody.class_().id();
        switch (classId) {
        case mimepp::TEXT_CLASS:
            // ...
            break;
        case mimepp::ADDRESS_LIST_CLASS:
            // ...
            break;
        case mimepp::MAILBOX_LIST_CLASS:
            // ...
            break;
        case mimepp::DATE_TIME_CLASS:
            // ...
            break;
        // etc, ...
        }
    }
}

void readFile(mimepp::String& buf, const char* filename, bool binary=false)
{
    ios_base::openmode flags = ios_base::in;
    if (binary) {
        flags |= ios_base::binary;
    }
    ifstream in(filename, flags);
    if (! in) {
        cout << "Can't open file " << filename << endl;
        exit(1);
    }
    in.seekg(0, ios::end);
    int size = in.tellg();
    in.seekg(0, ios::beg);
    buf.reserve(size);
    char ch;
    while (in.get(ch)) {
        buf.append(1, ch);
    }
}

int main()
{
    mimepp::Initialize();

    // Create a new Message object
    mimepp::Message msg;
    // Set string representation of the message
    mimepp::String msgStr;
    readFile(msgStr, "simple-msg.txt");
    msg.setString(msgStr);
    msg.parse();
    printAllHeaderFields(msg.headers());

    mimepp::Finalize();

	return 0;
}
