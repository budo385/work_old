// msg2html.cpp
//
// This example program provides a simple application that takes the name
// of an input file as a command line parameter, parses the content of that
// file as an email message, converts the message to HTML, and writes the
// HTML document to standard output.  The program uses the
// mimepp::email::Message class to parse the message.
//
// Attachments are extracted, written as separate files, and referenced by
// the HTML document via hyperlinks.
//

#pragma warning(disable: 4251)

#include <assert.h>
#include <stdio.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <functional>
#include <email/email.h>
using namespace std;
using namespace mimepp;
using namespace mimepp::email;

#if defined(_WIN32)
#  define strcasecmp _stricmp
#  define snprintf _snprintf
#endif

struct eq_nocase : public std::binary_function<char, char, bool> {
    bool operator()(char x, char y) const {
        return toupper(static_cast<unsigned char>(x)) ==
            toupper(static_cast<unsigned char>(y));
    }
};

// Compare strings, ignoring uppercase/lowercase differences
inline bool isEqualNoCase(const string& s, const string& t)
{
    return s.length() == t.length()
        && equal(s.begin(), s.end(), t.begin(), eq_nocase());
}

//
// Looks at the MIME type and creates a file name.  Really, this just means
// it creates the right file extension.  There are better ways to do this
// -- for example, you could check with the Windows registry.  We should
// probably come back later and improve this, at least to handle the most
// common types (PNG, GIF, JPG, WAV, etc).
//
static string fileNameFromContentType(const string& aContentType,
    const string& aContentSubtype, const string& aBase)
{
    string fileName = aBase;
    const char* contentType = aContentType.c_str();
    const char* contentSubtype = aContentSubtype.c_str();
    if (strcasecmp(contentType, "text") == 0) {
        if (strcasecmp(contentSubtype, "html") == 0) {
            fileName += ".html";
        }
    }
    else {
        fileName += ".dat";
    }
    return fileName;
}

//
// Encodes the characters that must be encoded in HTML:
//
//   '&'  -->  "&amp;"
//   '<'  -->  "&lt;"
//   '>'  -->  "&gt;"
//
static string encodeHTML(const string& aStr)
{
    const string& src = aStr;
    size_t len = src.length();
    string dst;
    dst.reserve((size_t)(1.2*len));
    for (size_t pos=0; pos < len; ++pos) {
        char ch = src[pos];
        if (ch == '&') {
            dst += "&amp;";
        }
        else if (ch == '<') {
            dst += "&lt;";
        }
        else if (ch == '>') {
            dst += "&gt;";
        }
        else {
            dst += ch;
        }
    }
    return dst;
}

static string uriEncode(const string& s)
{
    string encoded;
    for (size_t i = 0; i < s.length(); ++i) {
        char c = s.at(i);
        if (c == '@') {
            encoded.append("%40");
        }
        else if (c == ' ') {
            encoded.append("%20");
        }
        else {
            encoded.append(1, c);
        }
    }
    return encoded;
}

//
// Wraps a single long line by inserting '\n' characters
//
static string wrapOneLongLine(size_t aMaxLineLen, const string& aLine)
{
    assert(aMaxLineLen > 0);
    int count = 0;
    string line = aLine;
    string wrappedLine;
    while (line.length() > aMaxLineLen) {
        string::size_type pos = aMaxLineLen;
        char ch = line[pos];
        while (pos > 0 && ch != ' ' && ch != '\t') {
            --pos;
            ch = line[pos];
        }
        while (pos > 0 && (ch == ' ' || ch == '\t')) {
            --pos;
            ch = line[pos];
        }
        if (pos > 0) {
            ++pos;
            if (count > 0) {
                wrappedLine += "\n";
            }
            ++count;
            wrappedLine += line.substr(0, pos);
            ch = line[pos];
            while (ch == ' ' || ch == '\t') {
                ++pos;
                if (pos < line.length()) {
                    ch = line[pos];
                }
                else {
                    ch = 0;
                }
            }
            line = line.substr(pos);
        }
        else /* if (pos == 0) */ {
            pos = 0;
            ch = line[pos];
            while (ch != ' ' && ch != '\t') {
                ++pos;
                if (pos < line.length()) {
                    ch = line[pos];
                }
                else {
                    ch = ' ';
                }
            }
            if (count > 0) {
                wrappedLine += "\n";
            }
            ++count;
            wrappedLine += line.substr(0, pos);
            if (pos < line.length()) {
                ch = line[pos];
            }
            else {
                ch = 0;
            }
            while (ch == ' ' || ch == '\t') {
                ++pos;
                if (pos < line.length()) {
                    ch = line[pos];
                }
                else {
                    ch = 0;
                }
            }
            line = line.substr(pos);
        }
    }
    if (line.length() > 0) {
        if (count > 0) {
            wrappedLine += "\n";
        }
        wrappedLine += line;
    }
    return wrappedLine;
}

//
// Removes CR character
//
static string removeCR(const string& s)
{
    string t;
    t.reserve(s.length());
    size_t n = s.length();
    for (size_t i = 0; i < n; ++i) {
        char c = s[i];
        if (c != '\r') {
            t += c;
        }
    }
    return t;
}

//
// Wraps all lines in aStr that are longer than aMaxLineLen
//
static string wrapLongLines(size_t aMaxLineLen, const string& aStr)
{
    const string& src = aStr;
    string::size_type srcLen = src.length();
    string dst;
    dst.reserve((size_t)(1.2*srcLen));

    //
    // Get first line
    //
    string::size_type lineStart = 0;
    string::size_type lineEnd = 0;
    lineEnd = src.find('\n', lineStart);
    if (lineEnd == string::npos) {
        lineEnd = srcLen;
    }
    string::size_type lineLen = lineEnd - lineStart;
    while (lineStart < srcLen) {
        if (lineLen <= aMaxLineLen) {
            dst += src.substr(lineStart, lineLen);
            dst += "\n";
        }
        else /* if (lineLen > aMaxLineLen) */ {
            string line = src.substr(lineStart, lineLen);
            line = wrapOneLongLine(aMaxLineLen, line);
            dst += line;
            dst += "\n";
        }
        //
        // Get next line
        //
        lineStart = lineEnd + 1;
        if (lineStart < srcLen) {
            lineEnd = src.find('\n', lineStart);
            if (lineEnd == string::npos) {
                lineEnd = srcLen;
            }
        }
        else /* if (lineStart >= srcLen) */ {
            lineEnd = lineStart;
        }
        lineLen = lineEnd - lineStart;
    }

    return dst;
}

//
// Returns the position of the start of the string "<img ".  If the
// search fails, the function returns endPos.
//
static size_t findImgTag(size_t pos, size_t endPos, const string& htmlText)
{
    if (endPos - pos < 4) {
        return endPos;
    }
    // perform case-insensitive search for "<img "
    size_t end = endPos - 4;
    while (pos < end) {
        char c = htmlText.at(pos++);
        if (c == '<') {
            c = (char) (htmlText.at(pos) | 0x20);
            if (c == 'i') {
                ++pos;
                c = (char) (htmlText.at(pos) | 0x20);
                if (c == 'm') {
                    ++pos;
                    c = (char) (htmlText.at(pos) | 0x20);
                    if (c == 'g') {  // 'g'
                        ++pos;
                        c = htmlText.at(pos);
                        if (isspace(c)) {
                            // return here on success
                            return pos - 4;
                        }
                    }
                }
            }
        }
    }
    // return here on failure
    return endPos;
}

//
// Returns the position of the character just past the end of a tag.
// If the end of the tag is not found, the function returns endPos.
//
static size_t findTagEnd(size_t pos, size_t endPos, const string& htmlText)
{
    while (pos < endPos) {
        char c = htmlText[pos++];
        if (c == '>') {
            break;
        }
    }
    return pos;
}

//
// Searches within an IMG tag for the URI.  If the method finds the
// URL, it returns the position of the beginning of the URI.  If the
// method does not find the URI, it returns endPos.
//
static size_t getImgUriBegin(size_t pos, size_t endPos, const string& htmlText)
{
    if (endPos < 3) {
        return endPos;
    }
    // perform case-insensitive search for "src=\""
    size_t end = endPos - 3;
    while (pos < end) {
        char c = (char) (htmlText.at(pos++) | 0x20);
        if (c == 's') {
            c = (char) (htmlText.at(pos) | 0x20);
            if (c == 'r') {
                ++pos;
                c = (char) (htmlText.at(pos) | 0x20);
                if (c == 'c') {
                    ++pos;
                    c = htmlText.at(pos);
                    // Skip spaces
                    while (isspace(c) && pos < endPos) {
                        ++pos;
                        c = htmlText.at(pos);
                    }
                    if (c == '=' && pos < endPos) {
                        ++pos;
                        c = htmlText.at(pos);
                        // Skip spaces
                        while (isspace(c) && pos < endPos) {
                            ++pos;
                            c = htmlText.at(pos);
                        }
                        if (c == '"') {
                            ++pos;
                            // Return here on success
                            return pos;
                        }
                    }
                }
            }
        }
    }
    // Return here on failure
    return endPos;
}

//
// Returns the position of the end of the URI within an IMG tag.
//
static size_t getImgUriEnd(size_t pos, size_t endPos, const string& htmlText)
{
    while (pos < endPos) {
        char c = htmlText.at(pos);
        if (c == '"') {
            break;
        }
        ++pos;
    }
    return pos;
}

//
// Replaces all the "cid:" image URIs in the HTML text with the
// corresponding relative URI.  Returns the new HTML text.
//
static string replaceImageUris(const string& htmlText,
     const map<string,string>& uriMap)
{
    size_t pos = 0;
    size_t endPos = htmlText.length();
    size_t segmentBegin = pos;
    string buf;
    while (true) {
        pos = findImgTag(pos, endPos, htmlText);
        if (pos < endPos) {
            size_t tagBegin = pos;
            size_t tagEnd = findTagEnd(tagBegin, endPos, htmlText);
            size_t uriBegin = getImgUriBegin(tagBegin, tagEnd, htmlText);
            size_t uriEnd = getImgUriEnd(uriBegin, tagEnd, htmlText);
            string uri = htmlText.substr(uriBegin, uriEnd-uriBegin);
            map<string,string>::const_iterator iter = uriMap.find(uri);
            if (iter != uriMap.end()) {
                uri = iter->second;
            }
            buf.append(htmlText.substr(segmentBegin, uriBegin-segmentBegin));
            buf.append(uri);
            segmentBegin = uriEnd;
            pos = tagEnd;
        }
        else /* if (pos >= endPos) */ {
            break;
        }
    }
    buf.append(htmlText.substr(segmentBegin, endPos-segmentBegin));
    return buf;
}

static string flowedToHtml(const string& aFlowed)
{
    // Encode '<', etc
    string flowed = encodeHTML(aFlowed);
    // Remove CR LF.  Add <br> for hard line breaks.
    size_t len = flowed.length();
    string buf;
    buf.reserve(2*len);
    buf += "<p>";
    char lastCh = 0;
    size_t pos = 0;
    size_t endPos = len;
    while (pos < endPos) {
        char ch = flowed.at(pos++);
        // skip '\r'
        if (ch != '\r') {
            if (ch == '\n') {
                if (lastCh == ' ') {
                    // soft line break
                }
                else {
                    // hard linebreak
                    buf += "<br>";
                }
            }
            else {
                buf += ch;
            }
            lastCh = ch;
        }
    }
    buf += "</p>";
    return buf;
}

static string htmlToHtml(const string& html)
{
    size_t endPos = html.length();
    size_t pos = 0;
    // Find "<body "
    while (pos < endPos) {
        char ch = html.at(pos++);
        if (ch == '<') {
            if (endPos - pos >= 5
                && (html.at(pos+0)=='B' || html.at(pos+0)=='b')
                && (html.at(pos+1)=='O' || html.at(pos+1)=='o')
                && (html.at(pos+2)=='D' || html.at(pos+2)=='d')
                && (html.at(pos+3)=='Y' || html.at(pos+3)=='y')
                && (html.at(pos+4)=='>' || html.at(pos+4)==' '))
            {
                pos += 5;
                break;
            }
        }
    }
    // Find ">"
    while (pos < endPos) {
        char ch = html.at(pos++);
        if (ch == '>') {
            break;
        }
    }
    size_t bodyStart = pos;
    size_t bodyEnd = pos;
    // Find "</body "
    while (pos < endPos) {
        char ch = html.at(pos++);
        if (ch == '<') {
            if (endPos - pos >= 6
                && (html.at(pos+0)=='/')
                && (html.at(pos+1)=='B' || html.at(pos+1)=='b')
                && (html.at(pos+2)=='O' || html.at(pos+2)=='o')
                && (html.at(pos+3)=='D' || html.at(pos+3)=='d')
                && (html.at(pos+4)=='Y' || html.at(pos+4)=='y')
                && (html.at(pos+5)=='>' || isspace(html.at(pos+5))))
            {
                bodyEnd = pos - 1;
                pos += 6;
                break;
            }
        }
    }
    if (bodyStart == endPos) {
        bodyStart = 0;
        bodyEnd = endPos;
    }
    string ret = "<div>\r\n" + html.substr(bodyStart, bodyEnd-bodyStart)
        + "</div>\r\n";
    return ret;
}

//
// Returns a list of all the image URIs in the HTML text
//
static void getImageUris(const string& htmlText, vector<string>& uris)
{
    size_t pos = 0;
    size_t endPos = htmlText.length();
    while (true) {
        pos = findImgTag(pos, endPos, htmlText);
        if (pos < endPos) {
            size_t tagBegin = pos;
            size_t tagEnd = findTagEnd(tagBegin, endPos, htmlText);
            size_t uriBegin = getImgUriBegin(tagBegin, tagEnd, htmlText);
            size_t uriEnd = getImgUriEnd(uriBegin, tagEnd, htmlText);
            string uri = htmlText.substr(uriBegin, uriEnd-uriBegin);
            // Check if the URI is already in the collection.  Don't
            // add a URL that is already in the collection.
            bool isDuplicate = false;
            size_t n = uris.size();
            for (size_t i = 0; i < n; ++i) {
                if (uri == uris[i]) {
                    isDuplicate = true;
                    break;
                }
            }
            if (! isDuplicate) {
                uris.push_back(uri);
            }
            pos = tagEnd;
        }
        else /* if (pos >= endPos) */ {
            break;
        }
    }
}

static string fixImageRefs(const string& memo, const HtmlText& htmlText,
    const string& dir)
{
    // Get all the image URIs
    vector<string> uris;
    getImageUris(memo, uris);
    // For each "cid:" URI, create a corresponding "file:" URI
    const string cid("cid:");
    map<string,string> uriMap;
    for (vector<string>::iterator iter = uris.begin();
        iter != uris.end(); iter++) {

        string uri = *iter;
        string fileUri = uri;
        if (isEqualNoCase(uri.substr(0, 4), cid)) {
            string contentId = uri.substr(4);
            int idx = htmlText.find(contentId);
            if (idx >= 0) {
                const EmbeddedObject& obj = htmlText.get(idx);
                string fileName = obj.makeFileName();
                string path = dir + string("/") + fileName;
                ofstream out(path.c_str(),ios_base::out|ios_base::binary);
                const String& s = obj.content();
                out.write(s.data(), (streamsize) s.length());
                fileUri = uriEncode(path);
            }
        }
        uriMap[uri] = fileUri;
    }
    string r = replaceImageUris(memo, uriMap);
    return r;
}

void convertOneMessage(int index, const mimepp::String& messageStr,
    ostream& out)
{
    string dir = "html";

    // Parse the message

    mimepp::email::Message msg;
    msg.parseFrom(messageStr);

    // From

    string from = msg.originator().displayName();
	if (from.length() == 0) {
		from = msg.originator().mailboxName();
	}
	else {
	    from += " [";
	    from += msg.originator().mailboxName();
	    from += "]";
	}
    from = encodeHTML(from);

    // To

    string to;
    unsigned i;
    for (i=0; i < msg.toRecipients().count(); ++i) {
        if (to.length() > 0) {
            to += ", ";
        }
        string name = msg.toRecipients().get(i).displayName();
        string mboxName = msg.toRecipients().get(i).mailboxName();
        if (name.length() == 0) {
            name = mboxName;
        }
        else {
            name += " [";
            name += mboxName;
            name += "]";
        }
        to += name;
    }
    to = encodeHTML(to);

    // Cc

    string cc;
    for (i=0; i < msg.ccRecipients().count(); ++i) {
        if (cc.length() > 0) {
            cc += ", ";
        }
        string name = msg.ccRecipients().get(i).displayName();
        string mboxName = msg.ccRecipients().get(i).mailboxName();
        if (name.length() == 0) {
            name = mboxName;
        }
        else {
            name += " [";
            name += mboxName;
            name += "]";
        }
        cc += name;
    }
    cc = encodeHTML(cc);

    // Subject

    string subject = msg.subject().text();
    subject = encodeHTML(subject);

    // Date

    string date = msg.date().displayString();

    // Memo text

    string memo = "";
    string encoding = "US-ASCII";
    string language = "";

    const HtmlText& htmlText = msg.memoTextAsHtmlText();
    if (! htmlText.isNull()) {
        memo = htmlText.text();
        memo = removeCR(memo);
        memo = htmlToHtml(memo);
        encoding = htmlText.encoding();
        language = htmlText.language();
        memo = fixImageRefs(memo, htmlText, dir);
    }
    else {
        const mimepp::email::Text& plainText = msg.memoTextAsPlainText();
        memo = plainText.text();
        if (plainText.isFlowed()) {
            memo = flowedToHtml(memo);
        }
        else {
            memo = removeCR(memo);
            memo = wrapLongLines(76, memo);
            memo = encodeHTML(memo);
            memo = "<pre>" + memo + "</pre>\r\n";
        }
        encoding = plainText.encoding();
        language = plainText.language();
    }

    // Attachments

    string attachStr;
    AttachmentList& attachments = msg.attachments();
    for (unsigned j=0; j < attachments.count(); ++j) {
        const Attachment& att = attachments.get(j);
        const String& content = att.content();
        string type = att.type();
        string subtype = att.subtype();
        string fileName = att.fileName();
        if (fileName.length() == 0) {
            char s[20];
            snprintf(s, sizeof(s), "attach-%d", j+1);
            s[sizeof(s)-1] = 0;
            string base = s;
            fileName = fileNameFromContentType(type, subtype, base);
        }
        if (attachStr.length() > 0) {
            attachStr.append(", ");
        }
        string path = dir + string("/") + fileName;
        attachStr.append("<a href=\"");
        attachStr.append(path);
        attachStr.append("\">");
        attachStr.append(fileName);
        attachStr.append("</a>");
        ofstream out(path.c_str(), ios_base::out|ios_base::binary);
        out.write(content.data(), (streamsize)content.length());
        out.close();
    }

    // Write HTML to output stream

    out << "<!DOCTYPE HTML PUBLIC "
        "\"-//W3C//DTD HTML 4.0 Transitional//EN\" "
        "\"http://www.w3.org/TR/REC-html40/loose.dtd\">" << endl;
    out << "<html>" << endl;
    out << "<head>" << endl;
    out << "<meta http-equiv=\"content-type\" "
        "content=\"text/html; charset=" << encoding << "\">" << endl;
    if (language.length() > 0) {
        out << "<meta http-equiv=\"content-language\" "
            "content=\"" << language << "\">" << endl;
    }
    out << "<title>" << subject << "</title>" << endl;
    out << "</head>" << endl;
    out << "<body>" << endl;
    out << "<b>Date:</b> " << date << "<br>" << endl;
    out << "<b>From:</b> " << from << "<br>" << endl;
    out << "<b>To:</b> " << to << "<br>" << endl;
    out << "<b>Subject:</b> " << subject << "<br>" << endl;
    out << "<b>Attachments:</b> " << attachStr << "</p>" << endl;
    out << "<hr>" << endl;
    out << memo << "<br>" << endl;
    out << "</body>" << endl;
    out << "</html>" << endl;
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        cout << "Please specify a file name on command line" << endl;
        exit(EXIT_FAILURE);
    }
    const char* filename = argv[1];

    // Initialize the library

    mimepp::Initialize();

    // Read message from file

    ios_base::openmode flags = ios::in | ios::binary;
    ifstream in(filename, flags);
    if (! in) {
        cout << "Can't open file " << filename << endl;
        exit(1);
    }
    in.seekg(0, ios::end);
    size_t fileSize = (size_t) in.tellg();
    in.seekg(0, ios::beg);
    mimepp::String messageStr;
    messageStr.reserve(fileSize);
    const size_t bufSize = 8192;
    char* buf = (char*) malloc(bufSize);
    while (in) {
        in.read(buf, bufSize);
        size_t n = (size_t) in.gcount();
        messageStr.append(buf, n);
    }
    free(buf);
    in.close();

    int i = 0;
    convertOneMessage(i, messageStr, cout);

    // Finalize the library

    mimepp::Finalize();

    return 0;
}
