// HtmlText.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include <mimepp/mimepp.h>
#include "email/email.h"
using namespace mimepp::email;
using namespace std;

/**
 * Default constructor.
 */
mimepp::email::HtmlText::HtmlText()
{
}

/**
 * Copy constructor.
 */
mimepp::email::HtmlText::HtmlText(const HtmlText& other)
  : Text(other)
{
    mObjects = other.mObjects;
}

/**
 * Constructor that takes an initial value.
 *
 * The character encoding identifier should be one that is registered with
 * IANA.  Examples: "UTF-8", "ISO-8859-1", "Shift_JIS".
 *
 * The language identifier should be one that is specified in ISO&nbsp;639
 * (see also RFC&nbsp;3066).  Examples: "en", "jp", "de".
 *
 * @param text the text value of the string (UTF-8 encoding)
 * @param encoding the external character encoding identifier
 * @param language the two character language identifier
 */
mimepp::email::HtmlText::HtmlText(const string& text, const string& encoding,
    const string& language)
  : Text(text,encoding,language,false)
{
}

/**
 * Destructor.
 */
mimepp::email::HtmlText::~HtmlText()
{
}

/**
 * Assignment operator.
 */
HtmlText& mimepp::email::HtmlText::operator = (const HtmlText& other)
{
    if (this != &other) {
        Text::operator=(other);
        mObjects = other.mObjects;
    }
    return *this;
}

void mimepp::email::HtmlText::clear()
{
    Text::clear();
}

/**
 * Adds an embedded object to the list
 *
 * The member function makes a copy of its argument and adds the new
 * embedded object to the end of the list.
 *
 * @param obj embedded object to add to the list
 */
void mimepp::email::HtmlText::add(const EmbeddedObject& obj)
{
    mObjects.push_back(obj);
}

/**
 * Gets the number of embedded objects in the list
 *
 * @return number of embedded objects in the list
 */
unsigned mimepp::email::HtmlText::count() const
{
    return (unsigned) mObjects.size();
}

/**
 * Gets the embedded object at the specified position in the list
 *
 * The member function returns the embedded object at the position
 * specified by <i>index</i>, which must satisfy the condition 0 &lt;=
 * <i>index</i> &lt; count().  If the index is out of range, the behavior
 * is undefined.
 *
 * Indexing is zero-based, so the first embedded object is at position 0,
 * and the last embedded object is at position count()-1.
 *
 * @param index positon of the embedded object to return
 * @return embedded object at the position specified by <i>index</i>
 */
const mimepp::email::EmbeddedObject& HtmlText::get(int index) const
{
    return mObjects[index];
}

/**
 * Gets the index of an embedded object that has the specified
 * content ID.
 *
 * The member function searches for an embedded object by its content
 * identifier.  If the function find an embedded object with matching
 * content identifier, then it returns the index of the embedded object,
 * and you may call the get() member function to get a reference to the
 * embedded object.  If the function cannot find an embedded object, then
 * it returns -1.
 *
 * You might use this function if you have an image URL such as
 * "cid:1234@example.com" and you need to get the referenced embedded
 * image.  You would call this member function with the argument
 * "1234@example.com".
 *
 * Most commonly, an embedded object is an image.  However, an embedded
 * object could also be a stylesheet, and applet, a Flash file, or some
 * other object.
 *
 * @param contentId the embedded object to find
 * @return index of the embedded object, if found; otherwise, -1
 */
int HtmlText::find(const string& contentId) const
{
    int retval = -1;
    int n = (int) mObjects.size();
    for (int i = 0; i < n; ++i) {
        const EmbeddedObject& obj = mObjects[i];
        if (contentId == obj.contentID()) {
            retval = i;
            break;
        }
    }
    return retval;
}


void mimepp::email::HtmlText::_deleteObjects()
{
    mObjects.clear();
}


void mimepp::email::HtmlText::_copyObjects(const HtmlText& other)
{
    mObjects = other.mObjects;
}
