// EmbeddedObject.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include "email/email.h"
using namespace mimepp::email;
using namespace std;
using namespace mimepp;

#if defined(_WIN32) || defined(_WIN64)
#define strcasecmp _strcmpi
#endif

/**
 * \class mimepp::email::EmbeddedObject
 *
 * Represents and embedded image or other object.
 */

/**
 * Default constructor.
 */
mimepp::email::EmbeddedObject::EmbeddedObject()
{
}

/**
 * Copy constructor.
 */
mimepp::email::EmbeddedObject::EmbeddedObject(const EmbeddedObject& other)
  : mContentID(other.mContentID),
    mMediaType(other.mMediaType),
    mContent(other.mContent)
{
}

/**
 * Constructor with initial values.
 */
mimepp::email::EmbeddedObject::EmbeddedObject(const string& contentID,
    const string& mediaType, const String& content)
  : mContentID(contentID),
    mMediaType(mediaType),
    mContent(content)
{
}

/**
 * Destructor.
 */
mimepp::email::EmbeddedObject::~EmbeddedObject()
{
}

/**
 * Assignment operator.
 */
mimepp::email::EmbeddedObject& EmbeddedObject::operator = (const EmbeddedObject& other)
{
    if (this != &other) {
        mContentID = other.mContentID;
        mMediaType = other.mMediaType;
        mContent = other.mContent;
    }
    return *this;
}

void mimepp::email::EmbeddedObject::set(const string& contentID,
    const string& mediaType, const String& content)
{
    mContentID = contentID;
    mMediaType = mediaType;
    mContent = content;
}


string mimepp::email::EmbeddedObject::makeFileName() const
{
    string pre = "";
    string ext = ".dat";
    const char* mtype = mMediaType.c_str();
    if (0 == strcasecmp("image/gif", mtype)) {
        pre = "img-";
        ext = ".gif";
    }
    else if (0 == strcasecmp("image/jpeg", mtype)) {
        pre = "img-";
        ext = ".jpg";
    }
    else if (0 == strcasecmp("image/png", mtype)) {
        pre = "img-";
        ext = ".png";
    }
    string contentID = mContentID;
    if (contentID.length() > 0 && contentID.at(0) == '<') {
        contentID = contentID.substr(1);
    }
    if (contentID.length() > 0 && contentID.at(contentID.length()-1) == '>') {
        contentID = contentID.substr(0, contentID.length()-1);
    }
    string fileName;
    fileName.append(pre);
    for (size_t i = 0; i < contentID.length(); ++i) {
        char c = contentID.at(i);
        if (c == '/'
            || c == ':'
            || c == '*'
            || c == '?') {
            fileName.append("_");
        }
        else {
            fileName.append(1, c);
        }
    }
    fileName.append(ext);
    return fileName;
}
