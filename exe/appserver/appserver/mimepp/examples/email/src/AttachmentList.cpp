// AttachmentList.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include "email/email.h"
using namespace mimepp::email;
using namespace std;


/**
 * \class mimepp::email::AttachmentList
 *
 * <b>%AttachmentList</b> represents a list of file attachments.
 *
 * <b>%AttachmentList</b> contains member functions to manage the list of
 * attachments.  Call <b>count()</b> to get the number of attachments in the
 * list and call <b>get()</b> to get an attachment at a specified position.
 * Call <b>add()</b> to add an attachment to the end of the list.
 */

/**
 * Default constructor.
 */
mimepp::email::AttachmentList::AttachmentList()
{
}

/**
 * Copy constructor.
 */
mimepp::email::AttachmentList::AttachmentList(const AttachmentList& other)
{
    _copyAttachments(other);
}

/**
 * Destructor.
 */
mimepp::email::AttachmentList::~AttachmentList()
{
    _deleteAttachments();
}

/**
 * Assignment operator.
 */
AttachmentList& mimepp::email::AttachmentList::operator = (
    const AttachmentList& other)
{
    if (this != &other) {
        _deleteAttachments();
        _copyAttachments(other);
    }
    return *this;
}

/**
 * Adds an attachment to the list.
 *
 * The member function makes a copy of its argument and adds the new
 * Attachment object to the end of the list.
 *
 * @param attachment attachment to add to the list
 */
void mimepp::email::AttachmentList::add(const Attachment& attachment)
{
    mAttachments.push_back(attachment);
}

/**
 * Gets the number of attachments in the list.
 *
 * @return number of attachments in the list
 */
unsigned mimepp::email::AttachmentList::count() const
{
    return (unsigned) mAttachments.size();
}

/**
 * Gets the attachment at the specified position in the list.
 *
 * The member function returns the attachment at the position specified by
 * <i>index</i>, which must satisfy the condition 0 &lt;= <i>index</i>
 * &lt; count().  If the index is out of range, the behavior is undefined.
 *
 * Indexing is zero-based, so the first attachment is at position 0, and the
 * last attachment is at position count()-1.
 *
 * @param index positon of the attachment to return
 * @return attachment at the position specified by <i>index</i>
 */
const Attachment& mimepp::email::AttachmentList::get(int index) const
{
    return mAttachments[index];
}


void mimepp::email::AttachmentList::_deleteAttachments()
{
    mAttachments.clear();
}


void mimepp::email::AttachmentList::_copyAttachments(const AttachmentList& other)
{
    mAttachments = other.mAttachments;
}
