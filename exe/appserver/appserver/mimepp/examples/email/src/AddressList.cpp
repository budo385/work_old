// AddressList.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include "email/email.h"

using namespace mimepp::email;
using namespace std;

/**
 * \class mimepp::email::AddressList
 *
 * <b>%AddressList</b> represents a list of Internet email addresses.
 *
 * <b>%AddressList</b> contains member functions to manage the list of
 * addresses.  Call <b>count()</b> to get the number of addreses in the
 * list and call <b>get()</b> to get an address at a specified position.
 * Call <b>add()</b> to add an address to the end of the list.
 */

/**
 * Default constructor.
 */
mimepp::email::AddressList::AddressList()
{
}

/**
 * Copy constructor.
 */
mimepp::email::AddressList::AddressList(const AddressList& other)
{
    _copyAddresses(other);
}

/**
 * Destructor.
 */
mimepp::email::AddressList::~AddressList()
{
    _deleteAddresses();
}

/**
 * Assignment operator.
 */
AddressList& mimepp::email::AddressList::operator = (const AddressList& other)
{
    if (this != &other) {
        _deleteAddresses();
        _copyAddresses(other);
    }
    return *this;
}

/**
 * Adds an address to the list.
 *
 * The member function makes a copy of its argument and adds the new
 * Address object to the end of the list.
 *
 * @param addr address to add to the list
 */
void mimepp::email::AddressList::add(const Address& addr)
{
    mAddresses.push_back(addr);
}

/**
 * Gets the number of addresses in the list.
 *
 * @return number of addresses in the list
 */
unsigned mimepp::email::AddressList::count() const
{
    return (unsigned) mAddresses.size();
}

/**
 * Gets the address at the specified position in the list.
 *
 * The member function returns the address at the position specified by
 * <i>index</i>, which must satisfy the condition 0 &lt;= <i>index</i>
 * &lt; count().  If the index is out of range, the behavior is undefined.
 *
 * Indexing is zero-based, so the first address is at position 0, and the
 * last address is at position count()-1.
 *
 * @param index positon of the address to return
 * @return address at the position specified by <i>index</i>
 */
const mimepp::email::Address& AddressList::get(int index) const
{
    return mAddresses[index];
}


void mimepp::email::AddressList::_deleteAddresses()
{
    mAddresses.clear();
}


void mimepp::email::AddressList::_copyAddresses(const AddressList& other)
{
    mAddresses = other.mAddresses;
}
