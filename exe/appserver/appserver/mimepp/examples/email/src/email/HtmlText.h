// HtmlText.h

#ifndef MIMEPP_EMAIL_HTML_TEXT_H
#define MIMEPP_EMAIL_HTML_TEXT_H

#include <string>
#include <vector>


namespace mimepp {
namespace email {

    /// Represents an HTML text string
    class MIMEPP_EMAIL_API HtmlText : public Text {

    public:

        /// Default constructor
        HtmlText();

        /// Copy constructor
        HtmlText(const HtmlText& other);

        /// Constructor that takes an initial value
        HtmlText(const std::string& text, const std::string& encoding,
            const std::string& language);

        /// Destructor
        virtual ~HtmlText();

        /// Assignment operator
        HtmlText& operator=(const HtmlText& other);

        virtual void clear();

        /// Adds an embedded object to the list
        void add(const mimepp::email::EmbeddedObject& obj);

        /// Gets the number of embedded objects in the list
        unsigned count() const;

        /// Gets the embedded object at the specified position in the list
        const mimepp::email::EmbeddedObject& get(int index) const;

        /// Gets the index of an embedded object that has the specified
        /// content ID
        int find(const std::string& contentId) const;

    private:

        std::vector<mimepp::email::EmbeddedObject> mObjects;

        void _deleteObjects();
        void _copyObjects(const HtmlText&);

    };

} // namespace email
} // namespace mimepp

#endif
