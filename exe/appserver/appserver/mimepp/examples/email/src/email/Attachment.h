// Attachment.h

#ifndef MIMEPP_EMAIL_ATTACHMENT_H
#define MIMEPP_EMAIL_ATTACHMENT_H

#include <string>


namespace mimepp {
namespace email {

    /// File attachment
    class MIMEPP_EMAIL_API Attachment {

    public:

        /// Default constructor
        Attachment();

        /// Copy constructor
        Attachment(const Attachment& other);

        /// Destructor
        ~Attachment();

        /// Assignment operator
        Attachment& operator = (const Attachment& other);

        /// Gets the primary type of the attachment's MIME type
        const std::string& type() const;

        /// Gets the secondary type of the attachment's MIME type
        const std::string& subtype() const;

        /// Sets the attachment's MIME type
        void mediaType(const std::string& type, const std::string& subtype);

        /// Gets the charset of the attachment, if available
        const std::string& charset() const;

        /// Sets the charset of the attachment
        void charset(const std::string& charset);

        /// Gets the size of the attachment in bytes
        unsigned size() const;

        /// Gets the file name of the attachment, if available
        const std::string& fileName() const;

        /// Sets the file name of the attachment
        void fileName(const std::string& name);

        /// Gets the description of the attachment, if available
        const std::string& description() const;

        /// Sets the description of the attachment
        void description(const std::string& descr);

        /// Gets the content of the attachment
        const mimepp::String& content() const;

        /// Sets the content of the attachment
        void content(const mimepp::String& buffer);

    private:

        std::string mType;
        std::string mSubtype;
        std::string mCharset;
        std::string mFileName;
        std::string mDescription;
        mimepp::String mContent;

    };

} // namespace email
} // namespace mimepp

#endif
