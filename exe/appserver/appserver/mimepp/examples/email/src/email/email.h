// email.h

#ifndef MIMEPP_EMAIL_EMAIL_H
#define MIMEPP_EMAIL_EMAIL_H

#if !defined(MIMEPP_EMAIL_STATIC) && (defined(WIN32) || defined(_WIN32) || defined(_WIN32_WCE))
#  ifdef MIMEPP_EMAIL_EXPORTS
#    define MIMEPP_EMAIL_API __declspec(dllexport)
#  else
#    define MIMEPP_EMAIL_API __declspec(dllimport)
#  endif
#elif defined(__APPLE__)
#  ifdef MIMEPP_EMAIL_EXPORTS
#    define MIMEPP_EMAIL_API __attribute__((visibility("default")))
#  else
#    define MIMEPP_EMAIL_API
#  endif
#else
#  define MIMEPP_EMAIL_API
#endif

#include <mimepp/mimepp.h>
#include "Date.h"
#include "Address.h"
#include "AddressList.h"
#include "Attachment.h"
#include "AttachmentList.h"
#include "EmbeddedObject.h"
#include "Text.h"
#include "HtmlText.h"
#include "Message.h"

#endif
