// AddressList.h

#ifndef MIMEPP_EMAIL_ADDRESS_LIST_H
#define MIMEPP_EMAIL_ADDRESS_LIST_H

#include <vector>


namespace mimepp {
namespace email {

    /// List of Internet email addresses
    class MIMEPP_EMAIL_API AddressList {

    public:

        /// Default constructor
        AddressList();

        /// Copy constructor
        AddressList(const AddressList& other);

        /// Destructor
        virtual ~AddressList();

        /// Assignment operator
        AddressList& operator = (const AddressList& other);

        /// Adds an address to the list
        void add(const mimepp::email::Address& addr);

        /// Gets the number of addresses in the list
        unsigned count() const;

        /// Gets the address at the specified position in the list
        const mimepp::email::Address& get(int index) const;

    private:

        std::vector<mimepp::email::Address> mAddresses;

        void _deleteAddresses();
        void _copyAddresses(const AddressList&);

    };

} // namespace email
} // namespace mimepp

#endif
