// Text.h

#ifndef MIMEPP_EMAIL_TEXT_H
#define MIMEPP_EMAIL_TEXT_H

#include <string>


namespace mimepp {
namespace email {

    /// %Text string with encoding information
    class MIMEPP_EMAIL_API Text {

    public:

        /// Default constructor
        Text();

        /// Copy constructor
        Text(const Text& other);

        /// Constructor with initial text (UTF-8), preferred external
        /// encoding identifier, and language identifier
        Text(const std::string& text, const std::string& encoding,
            const std::string& language, bool isFlowed);

        /// Destructor
        virtual ~Text();

        /// Assignment operator
        Text& operator=(const Text& other);

        /// Set the value to "null"
        virtual void clear();

        /// Sets the value from an encoded string
        void fromExternal(const std::string& encodedText,
            const std::string& encoding, const std::string& language,
            bool isFlowed);

        /// Sets the value (UTF-8 encoding)
        void set(const std::string& text, const std::string& encoding,
            const std::string& language, bool isFlowed);

        /// Gets the text with external encoding
        std::string toExternal();

        /// Returns true if value is "null"
        bool isNull() const;

        /// Gets the text as a Unicode string
        const std::string& text() const;

        /// Gets the character encoding identifier
        const std::string& encoding() const;

        /// Gets the language identifier
        const std::string& language() const;

        /// Returns true if the plain text is flowed
        bool isFlowed() const;

    private:

        bool mIsNull;
        std::string mText;
        std::string mEncoding;
        std::string mLanguage;
        bool mIsFlowed;

    };

} // namespace email
} // namespace mimepp

#endif
