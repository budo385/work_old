// AttachmentList.h

#ifndef MIMEPP_EMAIL_ATTACHMENT_LIST_H
#define MIMEPP_EMAIL_ATTACHMENT_LIST_H

#include <vector>


namespace mimepp {
namespace email {

    /// List of file attachments
    class MIMEPP_EMAIL_API AttachmentList {
    public:

        /// Default constructor
        AttachmentList();

        /// Copy constructor
        AttachmentList(const AttachmentList& other);

        /// Destructor
        virtual ~AttachmentList();

        /// Assignment operator
        AttachmentList& operator = (const AttachmentList& other);

        /// Adds an attachment to the list
        void add(const mimepp::email::Attachment& attachment);

        /// Gets the number of attachments in the list
        unsigned count() const;

        /// Gets the attachment at the specified position
        const mimepp::email::Attachment& get(int index) const;

    private:

        std::vector<mimepp::email::Attachment> mAttachments;

        void _deleteAttachments();
        void _copyAttachments(const AttachmentList&);
    };

} // namespace email
} // namespace mimepp

#endif
