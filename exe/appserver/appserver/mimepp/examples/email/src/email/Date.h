// Date.h

#ifndef MIMEPP_EMAIL_DATE_H
#define MIMEPP_EMAIL_DATE_H

#include <string>


namespace mimepp {
namespace email {

    /// %Date and time value
    class MIMEPP_EMAIL_API Date {
    public:

        /// Default constructor
        Date();

        /// Copy constructor
        Date(const Date& other);

        /// Destructor
        virtual ~Date();

        /// Assignment operator
        Date& operator = (const Date& other);

        /// Sets the date and time value
        void setValues(int year, int month, int day, int hour, int minute,
            int second);

        /// Sets the date and time value, including time zone
        void setValues(int year, int month, int day, int hour, int minute,
            int second, int zone);

        /// Sets the value to the current date and time
        void setCurrent();

        /// Returns true if value has not been set
        bool isNull() const { return mIsNull; }

        /// Gets the year
        int year() const { return mYear; }

        /// Gets the month
        int month() const { return mMonth; }

        /// Gets the day of the month
        int day() const { return mDay; }

        /// Gets the hour
        int hour() const { return mHour; }

        /// Gets the minute
        int minute() const { return mMinute; }

        /// Gets the second
        int second() const { return mSecond; }

        /// Gets the time zone offset
        int zone() const { return mZone; }

        /// Gets a string representation of the date and time
        virtual const std::string& displayString() const;

    private:

        bool mIsNull;
        int mYear;
        int mMonth;
        int mDay;
        int mHour;
        int mMinute;
        int mSecond;
        int mZone;
        mutable std::string mString;

    };

} // namespace email
} // namespace mimepp

#endif
