// SystemError.h

#ifndef SYSTEM_ERROR_H
#define SYSTEM_ERROR_H

#include <exception>
#include <stdexcept>

class SystemError : public std::runtime_error {
public:
    SystemError(int errno_) : runtime_error("system error"), mErrno(errno_) {}
    int errno_() const { return mErrno; }
private:
    int mErrno;
};

#endif
