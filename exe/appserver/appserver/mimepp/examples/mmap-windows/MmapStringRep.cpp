// MmapStringRep.cpp

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "MmapStringRep.h"
#include "SystemError.h"


MmapStringRep::MmapStringRep(const char* pathname)
{
    WIN32_FILE_ATTRIBUTE_DATA fileInfo;
    GET_FILEEX_INFO_LEVELS infoLevelId = GetFileExInfoStandard;
    BOOL ok = GetFileAttributesEx(pathname, infoLevelId, &fileInfo);
    if (ok) {
        size_t length = fileInfo.nFileSizeLow;
        int err = init(pathname, 0, length);
        if (! err) {
            // Success
            return;
        }
    }
    int errno_ = GetLastError();
    throw SystemError(errno_);
}

MmapStringRep::MmapStringRep(const char* pathname, size_t offset)
{
    WIN32_FILE_ATTRIBUTE_DATA fileInfo;
    GET_FILEEX_INFO_LEVELS infoLevelId = GetFileExInfoStandard;
    BOOL ok = GetFileAttributesEx(pathname, infoLevelId, &fileInfo);
    if (ok) {
        size_t length = fileInfo.nFileSizeLow;
        int err = init(pathname, offset, length);
        if (! err) {
            // Success
            return;
        }
    }
    int errno_ = GetLastError();
    throw SystemError(errno_);
}

MmapStringRep::MmapStringRep(const char* pathname, size_t offset,
    size_t length)
{
    int err = init(pathname, offset, length);
    if (! err) {
        // Success
        return;
    }
    throw SystemError(errno);
}

int MmapStringRep::init(const char* pathname, size_t offset, size_t length)
{
    mFileHandle = INVALID_HANDLE_VALUE;
    mMappingHandle = INVALID_HANDLE_VALUE;
    mBuffer = 0;
    mSize = 0;
    mRefCount = 0;

    mFileHandle = CreateFile(pathname, GENERIC_READ, FILE_SHARE_READ, 0,
        OPEN_EXISTING, 0, 0);
    if (mFileHandle != INVALID_HANDLE_VALUE) {
        mMappingHandle = CreateFileMapping(mFileHandle, 0, PAGE_READONLY, 0,
            (DWORD) length, 0);
        if (mMappingHandle != INVALID_HANDLE_VALUE) {
            mBuffer = (char*)
                MapViewOfFile(mMappingHandle, FILE_MAP_READ, 0, 0, length);
            if (mBuffer != 0) {
                mSize = length;
                // Return here on success
				return 0;
            }
            CloseHandle(mMappingHandle);
            mMappingHandle = INVALID_HANDLE_VALUE;
        }
        CloseHandle(mFileHandle);
        mFileHandle = INVALID_HANDLE_VALUE;
    }
    // Return here on failure
	return -1;
}

MmapStringRep::~MmapStringRep()
{
    if (mBuffer != 0) {
        UnmapViewOfFile(mBuffer);
        mBuffer = 0;
    }
    if (mMappingHandle != INVALID_HANDLE_VALUE) {
        CloseHandle(mMappingHandle);
        mMappingHandle = INVALID_HANDLE_VALUE;
    }
    if (mFileHandle != INVALID_HANDLE_VALUE) {
        CloseHandle(mFileHandle);
        mFileHandle = INVALID_HANDLE_VALUE;
    }
}

char* MmapStringRep::buffer()
{
    return mBuffer;
}

size_t MmapStringRep::size()
{
    return mSize;
}

bool MmapStringRep::isShared()
{
    return (mRefCount > 1);
}

int MmapStringRep::incRef()
{
    InterlockedIncrement(&mRefCount);
    return mRefCount;
}

int MmapStringRep::decRef()
{
    InterlockedDecrement(&mRefCount);
    return mRefCount;
}
