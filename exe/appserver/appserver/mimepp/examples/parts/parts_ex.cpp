// parts_ex.cpp

#include <stdlib.h>
#include <stdio.h>
#include <mimepp/mimepp.h>
using namespace mimepp;


// Recursively print information about nested entities (body parts).

void PrintEntity(int aLevel, const Entity& aEntity)
{
    MediaType::MType type = MediaType::TEXT;
    String contentTypeStr;
    if (aEntity.headers().hasField("Content-Type")) {
        type = aEntity.headers().contentType().typeAsEnum();
        contentTypeStr += aEntity.headers().contentType().type();
        contentTypeStr += "/";
        contentTypeStr += aEntity.headers().contentType().subtype();
    }
    if (contentTypeStr == "") {
        contentTypeStr = "text/plain (implicit)";
    }
    if (type == MediaType::MULTIPART) {
        int i;
        printf(" ");
        for (i=0; i < aLevel-1; ++i) {
            printf("| ");
        }
        printf("\n");
        printf(" ");
        for (i=0; i < aLevel-2; ++i) {
            printf("| ");
        }
        for ( ; i < aLevel-1; ++i) {
            printf("+-");
        }
        printf("%s", contentTypeStr.c_str());
        int numBodyParts = aEntity.body().numBodyParts();
        if (numBodyParts == 1) {
            printf(" (1 body part)\n");
        }
        else {
            printf(" (%d body parts)\n", numBodyParts);
        }
        for (i=0; i < numBodyParts; ++i) {
            const BodyPart& part = aEntity.body().bodyPartAt(i);
            PrintEntity(aLevel+1, part);
        }
    }
    else if (type == MediaType::MESSAGE) {
        int i;
        printf(" ");
        for (i=0; i < aLevel-1; ++i) {
            printf("| ");
        }
        printf("\n");
        printf(" ");
        for (i=0; i < aLevel-2; ++i) {
            printf("| ");
        }
        for ( ; i < aLevel-1; ++i) {
            printf("+-");
        }
        printf("%s\n", contentTypeStr.c_str());
        const Message* msg = aEntity.body().message();
        if (msg != 0) {
            PrintEntity(aLevel+1, *msg);
        }
    }
    else {
        String str;
        str = contentTypeStr;
        if (aEntity.headers().hasField("Content-Description")) {
            str += " (";
            str += aEntity.headers().fieldBody("Content-Description").text();
            str += ")";
        }
        int i;
        printf(" ");
        for (i=0; i < aLevel-1; ++i) {
            printf("| ");
        }
        printf("\n");
        printf(" ");
        for (i=0; i < aLevel-2; ++i) {
            printf("| ");
        }
        for ( ; i < aLevel-1; ++i) {
            printf("+-");
        }
        printf("%s\n", str.c_str());
    }
}


int main(int argc, char **argv)
{
    //
    // Initialize the library
    //
    mimepp::Initialize();

    //
    // Get a message from a file.  The file name is the first commandline
    // parameter.
    //
    if (argc < 2) {
        printf("Please specify a file name.\n");
        return 1;
    }
    FILE* in = fopen(argv[1], "rb");
    if (in == NULL) {
        printf("I can't open the file \"%s\".\n", argv[1]);
        return 1;
    }
    fseek(in, 0, SEEK_END);
    size_t fileLen = ftell(in);
    fseek(in, 0, SEEK_SET);
    String messageStr;
    messageStr.reserve(fileLen);
    char* buffer = (char*) malloc(8192);
    do {
        size_t count = fread(buffer, 1, 8192, in);
        messageStr.append(buffer, count);
    } while (! feof(in));
    free(buffer);
    buffer = 0;
    fclose(in);
    in = 0;
    //
    // Create a message object and parse the message
    //
    Message message(messageStr);
    message.parse();
    //
    // Print the parts tree
    //
    PrintEntity(1, message);
    printf("\n");

    //
    // Finalize the library
    //
    mimepp::Finalize();

    return 0;
}
