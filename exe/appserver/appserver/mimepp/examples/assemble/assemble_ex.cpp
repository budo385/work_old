// assemble_ex.cpp
//
// This example program shows how to assemble a message by walking the
// document tree and serializing it to a buffer.
//
// The program takes a single command line parameter, which is the name of
// a file that contains a MIME message.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mimepp/mimepp.h>
using namespace mimepp;


// This StringWrapper class wraps a String object.  It has an important
// purpose though: if we set a NULL string, then it will just compute the
// required length.  This allows us to allocate enough memory in advance
// for the String, and it means we can use the same code for measuring the
// length and for appending.

class StringWrapper
{
public:
    StringWrapper(String* str) : mString(str), mLength(0) {}
    void append(const String& str);
    void append(const char* cstr);
    int mLength;
    String* mString;
};

void StringWrapper::append(const String& str)
{
    if (mString != 0) {
        mString->append(str);
    }
    mLength += (int) str.length();
}

void StringWrapper::append(const char* cstr)
{
    if (mString != 0) {
        mString->append(cstr);
    }
    mLength += (int) strlen(cstr);
}


// Assembles all the components of the entity (message or body part) into
// the string wrapper.  Uses recursion if there are nested entities.

static void assembleTo_(Entity& aEntity, StringWrapper& aStr)
{

    // Iterate over list of header fields.  Serialize each one.

    Headers& headers = aEntity.headers();
    int numHeaderFields = headers.numFields();
    for (int i=0; i < numHeaderFields; ++i) {
        Field& field = headers.fieldAt(i);
        field.fieldBody().assemble();
        aStr.append(field.fieldName());
        aStr.append(": ");
        aStr.append(field.fieldBody().getString());
        aStr.append(TextUtil::EOL_CHARS);
    }

    // Append blank line, which marks the end of the header fields

    aStr.append(TextUtil::EOL_CHARS);

    // Get the content type

    MediaType::MType contentType = MediaType::TEXT;
    if (headers.hasField("Content-Type")) {
        contentType = headers.contentType().typeAsEnum();
    }
    Body& body = aEntity.body();

    // If it's a multipart type, get the boundary and preamble.  Then
    // iterate over each body part, and serialize each one by calling this
    // function recursively.  Append the boundary to separate the body parts.

    if (contentType == MediaType::MULTIPART) {
        const String& boundary = headers.contentType().boundary();
        const String& preamble = body.preamble();
        if (preamble.length() > 0) {
            aStr.append(preamble);
        }
        int numParts = body.numBodyParts();
        for (int i=0; i < numParts; ++i) {
            BodyPart& part = body.bodyPartAt(i);
            aStr.append(TextUtil::EOL_CHARS);
            aStr.append("--");
            aStr.append(boundary);
            aStr.append(TextUtil::EOL_CHARS);
            assembleTo_(part, aStr);
        }
        aStr.append(TextUtil::EOL_CHARS);
        aStr.append("--");
        aStr.append(boundary);
        aStr.append("--" );
        aStr.append(TextUtil::EOL_CHARS);
    }

    // If it's a message/rfc822 type, then call this function recursively
    // to serialize the encapsulated message

    else if (contentType == MediaType::MESSAGE
        && Strcasecmp(headers.contentType().subtype(), "rfc822") == 0) {
        Message* message = body.message();
        if (message != 0) {
             assembleTo_(*message, aStr);
        }
        else {

            // If we arrive here, then something is wrong.  This should
            // recover.

            body.assemble();
            aStr.append(body.getString());
        }
    }

    // If it's a discrete body, then just append its string contents

    else {
        aStr.append(body.getString());
    }
}


void assembleTo(Message& message, String& str)
{

    // First pass: get the length

    StringWrapper str1(0);
    assembleTo_(message, str1);
    int length = str1.mLength;

    // Second pass: serialize everything to the string

    str.reserve(length);
    StringWrapper str2(&str);
    assembleTo_(message, str2);
}


int main(int argc, char** argv)
{

    // Initialize the library

    mimepp::Initialize();

    // Get a message from a file.  The file name is the first commandline
    // parameter.

    if (argc < 2) {
        printf("Please specify a file name.\n");
        return 1;
    }
    FILE* in = fopen(argv[1], "r");
    if (in == NULL) {
        printf("I can't open the file \"%s\".\n", argv[1]);
        return 1;
    }
    fseek(in, 0, SEEK_END);
    size_t fileLen = ftell(in);
    fseek(in, 0, SEEK_SET);
    String messageStr;
    messageStr.reserve(fileLen);
    char* buffer = (char*) malloc(8192);
    do {
        size_t count = fread(buffer, 1, 8192, in);
        messageStr.append(buffer, count);
    } while (! feof(in));
    free(buffer);
    buffer = 0;
    fclose(in);
    in = 0;

    // Create a message object and parse the message

    Message message(messageStr);
    message.parse();

    // Just to be sure that we are assembling correctly, let's make a
    // change to the subject line

    String subj = message.headers().subject().getString();
    subj = String("FLAG: ") + subj;
    message.headers().subject().setString(subj);

    // Assemble the message

    String str;
    assembleTo(message, str);

    // If we want to clean up some memory and restore the tree of Node
    // objects to a nice, consistent state, we could parse the message all
    // over again, like this:
    //
    //     message.setString(str);
    //     message.parse();
    //
    // If we parse the message again, as shown above, then all the Node
    // objects will contain strings that share the same internal buffer
    // space.  If the message was originally created from parts, or was
    // modified in a significant way, then this should reduce the amount of
    // memory used.

    // Write the assembled message to a file

    FILE* out = fopen("out.txt", "w");
    fwrite(str.data(), 1, str.length(), out);
    fclose(out);
    out = 0;

    // Finalize the library

    mimepp::Finalize();

    return 0;   
}
