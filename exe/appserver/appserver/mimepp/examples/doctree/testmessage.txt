From: "Garret A. Hobart" <hobart@vp.gov>,
 "Levi P. Morton" <morton@who.org>
Sender: "Levi P. Morton" <morton@who.org>
To: Lewis Cass <cass@somewhere.net>, Charles Pinckney <pinckney@xyz.edu>
Subject: Test message, =?iso-8859-1?q?nat=FCrlich?=
Date: Wed, 17 May 2000 19:47:24 -0400
Message-ID: <NDBBIAKOPKHFGPLCODIGOEKFCHAA.morton@who.org>
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="bOuNdArY.oUtEr"

This is a multi-part message in MIME format.

--bOuNdArY.oUtEr
Content-Type: multipart/alternative; boundary="bOuNdArY.iNnEr"

--bOuNdArY.iNnEr

Memo text

--bOuNdArY.iNnEr
Content-Type: text/html; charset=ISO-8859-1
Content-Transfer-Encoding: quoted-printable

<html><body>
Memo text
</body></html>

--bOuNdArY.iNnEr--

--bOuNdArY.oUtEr
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: quoted-printable
Content-Disposition: attachment; filename="1.txt"

First attachment

--bOuNdArY.oUtEr
Content-Disposition: attachment; filename="1.txt"; x-foo=1

Second attachment

--bOuNdArY.oUtEr--
