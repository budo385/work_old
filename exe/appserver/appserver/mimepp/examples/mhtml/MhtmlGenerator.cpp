// MhtmlGenerator.cpp

#pragma warning(disable: 4786)

#include "MhtmlGenerator.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

using namespace std;
using namespace mimepp;

void getImageUrls(String& htmlText, vector<string>& urls);
size_t findImgTag(size_t textPos, size_t textLen, const char* text);
size_t findTagEnd(size_t textPos, size_t textLen, const char* text);
string getImageUrl(size_t beginPos, size_t endPos, const char* text);
void getImageUrlPos(size_t beginPos, size_t endPos, const char* text,
   size_t* urlBegin, size_t* urlEnd);
void getSrcValue(size_t pos, size_t endPos, const char* text, size_t* valBegin,
   size_t* valEnd);
void replaceImageUrls(String& htmlText, map<string,string>& cids);


MhtmlGenerator::MhtmlGenerator()
{
}

MhtmlGenerator::~MhtmlGenerator()
{
}

void MhtmlGenerator::addTo(const string& addr)
{
    AddressList& to = mMessage.headers().to();
    Mailbox* mbox = new Mailbox(addr.c_str());
    mbox->parse();
    to.addAddress(mbox);
}

void MhtmlGenerator::addCc(const string& addr)
{
    AddressList& cc = mMessage.headers().cc();
    Mailbox* mbox = new Mailbox(addr.c_str());
    mbox->parse();
    cc.addAddress(mbox);
}

void MhtmlGenerator::setFrom(const string& addr)
{
    MailboxList& from = mMessage.headers().from();
    if (from.numMailboxes() > 0) {
        Mailbox* m = from.removeMailboxAt(0);
        delete m;
    }
    Mailbox* mbox = new Mailbox(addr.c_str());
    mbox->parse();
    from.addMailbox(mbox);
}

void MhtmlGenerator::setSubject(const string& text)
{
    mMessage.headers().subject().setText(text.c_str());
}

void MhtmlGenerator::setHtmlFile(const string& filename)
{
    mHtmlFile = filename;
}

void MhtmlGenerator::setMhtmlFile(const string& filename)
{
    mMhtmlFile =  filename;
}

int MhtmlGenerator::generate()
{
    int retVal = -1;

    // Get the HTML text from the HTML file
    ifstream in(mHtmlFile.c_str());
    if (in) {
        in.seekg(0, ios::end);
        int fileSize = in.tellg();
        in.seekg(0, ios::beg);
        String htmlText;
        htmlText.reserve(fileSize);
        char c;
        while (in.get(c)) {
            htmlText.append(1, c);
        }
        in.close();
        retVal= generate2(htmlText);
    }
    return retVal;
}

int MhtmlGenerator::generate2(String& htmlText)
{
    int retVal = 0;

    // Find all the image file names

    vector<string> urls;
    getImageUrls(htmlText, urls);

    // Assign a content-ID for each image

    map<string, string> cids;
    int i;
    int n = (int) urls.size();
    for (i = 0; i < n; ++i) {
        MsgId contentId;
        contentId.createDefault();
        string url = urls[i];
        const char* s = contentId.getString().data();
        size_t len = contentId.getString().length();
        len = (len >= 2) ? len - 2 : 0;
        string cid(&s[1], len);
        cids[url] = cid;
    }

    // Substitute the cid URL for the existing URL

    replaceImageUrls(htmlText, cids);

    // Set the content type to "multipart/related"

    MediaType& contentType = mMessage.headers().contentType();
    contentType.setType("multipart");
    contentType.setSubtype("related");
    contentType.createBoundary();
    Parameter* param = new Parameter;
    param->setName("type");
    param->setValue("text/html");
    contentType.addParameter(param);
    param = 0;

    // Add the HTML text as a body part

    QuotedPrintableEncoder qencoder;
    BodyPart* part = new BodyPart;
    part->headers().contentType().setString("text/html");
    part->headers().contentTransferEncoding().setType("quoted-printable");
    part->body().setString(qencoder.encode(htmlText));
    mMessage.body().addBodyPart(part);
    part = 0;

    // Add the image files as body parts

    Base64Encoder bencoder;
    n = (int) urls.size();
    for (i = 0; i < n; ++i) {
        String img;
        string filename = urls[i];
        ifstream in(filename.c_str(), ios::in|ios::binary);
        if (in) {
            char c;
            while (in.get(c)) {
                img.append(1, c);
            }
        }
        in.close();
        img = bencoder.encode(img);
        BodyPart* part = new BodyPart;
        int m = (int) filename.length() - 4;
        if (m > 0) {
            const char* ext = filename.c_str() + m;
            if (strcmp(ext, ".png") == 0) {
                part->headers().contentType().setString("image/png");
            }
            else if (strcmp(ext, ".gif") == 0) {
                part->headers().contentType().setString("image/gif");
            }
            else if (strcmp(ext, ".jpg") == 0) {
                part->headers().contentType().setString("image/jpeg");
            }
            string cid = cids[filename];
            cid = "<" + cid + ">";
            part->headers().contentId().setString(cid.c_str());
            part->headers().contentTransferEncoding().setType("base64");
            part->body().setString(img);
            mMessage.body().addBodyPart(part);
            part = 0;
        }
    }

    mMessage.assemble();

    ofstream out(mMhtmlFile.c_str());
    out.write(mMessage.getString().data(), (int) mMessage.getString().length());
    out.close();

    return retVal;
}

void getImageUrls(String& htmlText, vector<string>& urls)
{
    size_t textPos = 0;
    size_t textLen = htmlText.length();
    const char* text = htmlText.data();
    while (true) {
        textPos = findImgTag(textPos, textLen, text);
        if (textPos < textLen) {
            size_t beginPos = textPos;
            size_t endPos = findTagEnd(beginPos, textLen, text);
            string url = getImageUrl(beginPos, endPos, text);
            // Check if the URL is already in the collection.  Don't add
            // a URL that is already in the collection.
            bool isDuplicate = false;
            int n = (int) urls.size();
            for (int i=0; i < n; ++i) {
                if (urls[i] == url) {
                    isDuplicate = true;
                    break;
                }
            }
            if (! isDuplicate) {
                urls.push_back(url);
            }
            textPos = endPos;
        }
        else /* if (textPos >= textLen) */ {
            break;
        }
    }
}

//
// Returns the position of the start of the string "<img ".  If the search
// fails, the function returns textLen.
//
size_t findImgTag(size_t textPos, size_t textLen, const char* text)
{
    // perform case-insensitive search for "<img "
    size_t end = textLen - 4;
    while (textPos < end) {
        char c = text[textPos++];
        if (c == '<') {
            c = text[textPos++] | 0x20;
            if (c == 'i') {
                c = text[textPos++] | 0x20;
                if (c == 'm') {
                    c = text[textPos++] | 0x20;
                    if (c == 'g') {
                        c = text[textPos++];
                        if (c == ' ') {
                            // return here on success
                            return textPos - 5;
                        }
                        else /* if (c != ' ') */ {
                            --textPos;
                        }
                    }
                    else /* if (c != 'g') */ {
                        --textPos;
                    }
                }
                else /* if (c != 'm') */ {
                    --textPos;
                }
            }
            else /* if (c != 'i') */ {
                --textPos;
            }
        }
    }
    // return here on failure
    return textLen;
}

//
// Returns the position of the character just past the end of a tag.  If
// the end of the tag is not found, the function returns textLen.
//
size_t findTagEnd(size_t textPos, size_t textLen, const char* text)
{
    while (textPos < textLen) {
        char c = text[textPos++];
        if (c == '>') {
            break;
        }
    }
    return textPos;
}

//
// Returns the URL of the image element.  beginPos is the beginning of the
// "<img " string.
//
string getImageUrl(size_t beginPos, size_t endPos, const char* text)
{
    // perform case-insensitive search for "src="
    size_t pos = beginPos;
    size_t end = endPos - 3;
    while (pos < end) {
        char c = text[pos++] | 0x20;
        if (c == 's') {
            c = text[pos++] | 0x20;
            if (c == 'r') {
                c = text[pos++] | 0x20;
                if (c == 'c') {
                    c = text[pos++];
                    if (c == '=') {
                        size_t valBegin, valEnd;
                        getSrcValue(pos, endPos, text, &valBegin, &valEnd);
                        return string(&text[valBegin], valEnd-valBegin);
                    }
                    else /* if (c != '=') */ {
                        --pos;
                    }
                }
                else /* if (c != 'c') */ {
                    --pos;
                }
            }
            else /* if (c != 'r') */ {
                --pos;
            }
        }
    }
    // return here on failure
    return string();
}

void getImageUrlPos(size_t beginPos, size_t endPos, const char* text,
   size_t* urlBegin, size_t* urlEnd)
{
    // perform case-insensitive search for "src="
    size_t pos = beginPos;
    size_t end = endPos - 3;
    while (pos < end) {
        char c = text[pos++] | 0x20;
        if (c == 's') {
            c = text[pos++] | 0x20;
            if (c == 'r') {
                c = text[pos++] | 0x20;
                if (c == 'c') {
                    c = text[pos++];
                    if (c == '=') {
                        getSrcValue(pos, endPos, text, urlBegin, urlEnd);
                        return;
                    }
                    else /* if (c != '=') */ {
                        --pos;
                    }
                }
                else /* if (c != 'c') */ {
                    --pos;
                }
            }
            else /* if (c != 'r') */ {
                --pos;
            }
        }
    }
    // return here on failure
    *urlBegin = pos;
    *urlEnd = pos;
}

void getSrcValue(size_t pos, size_t endPos, const char* text, size_t* valBegin,
   size_t* valEnd)
{
    *valBegin = endPos;
    *valEnd = endPos;
    char delim = 0;
    while (pos < endPos) {
        char c = text[pos++];
        if (c == '"' || c == '\'') {
            *valBegin = pos;
            *valEnd = pos;
            delim = c;
            break;
        }
    }
    while (pos < endPos) {
        char c = text[pos++];
        if (c == delim) {
            --pos;
            *valEnd = pos;
            break;
        }
    }
}

void replaceImageUrls(String& origText, map<string,string>& cids)
{
    String newText;
    size_t segmentBegin = 0;
    size_t textPos = 0;
    size_t textLen = origText.length();
    const char* text = origText.data();
    while (true) {
        textPos = findImgTag(textPos, textLen, text);
        if (textPos < textLen) {
            size_t beginPos = textPos;
            size_t endPos = findTagEnd(beginPos, textLen, text);
            size_t urlBegin, urlEnd;
            getImageUrlPos(beginPos, endPos, text, &urlBegin, &urlEnd);
            string url(&text[urlBegin], urlEnd-urlBegin);
            string cid = cids[url];
            newText.append(origText, segmentBegin, urlBegin-segmentBegin);
            newText.append("cid:");
            newText.append(cid.c_str());
            segmentBegin = urlEnd;
            textPos = endPos;
        }
        else /* if (textPos >= textLen) */ {
            break;
        }
    }
    newText.append(origText, segmentBegin, textLen-segmentBegin);
    origText = newText;
}
