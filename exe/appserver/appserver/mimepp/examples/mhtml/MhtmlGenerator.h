// MhtmlGenerator.h

#ifndef MHTML_GENERATOR_H
#define MHTML_GENERATOR_H

#include <mimepp/mimepp.h>
#include <string>

class MhtmlGenerator {
public:

    MhtmlGenerator();
    virtual ~MhtmlGenerator();

    void addTo(const std::string& addr);
    void addCc(const std::string& addr);
    void setFrom(const std::string& addr);
    void setSubject(const std::string& text);
    void setHtmlFile(const std::string& filename);
    void setMhtmlFile(const std::string& filename);
    int generate();

private:

    mimepp::Message mMessage;
    std::string mHtmlFile;
    std::string mMhtmlFile;

    int generate2(mimepp::String& htmlText);

};

#endif
