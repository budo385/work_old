#include "serverimportexportmanager.h"
#include "common/common/logger.h"
#include "common/common/threadid.h"

extern Logger				g_Logger;
#include "bus_server/bus_server/privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_core/bus_core/messagedispatcher.h"
extern MessageDispatcher* g_MessageDispatcher;

ServerImportExportManager::ServerImportExportManager()
{
}



void ServerImportExportManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
	Status err;
	DbRecordSet rowUserData;
	//load from DB username, password, group, 
	g_PrivateServiceSet->CoreUser->GetUserByUserName(err,"system",rowUserData);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_FAIL,"Failed to found system user: " +err.getErrorText());
		return;
	}

	QString strContextSession;
		
	g_UserSessionManager->CreateSession(err,strContextSession,rowUserData,"SC-BE","",true);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_FAIL,"Failed to create system user session context: " +err.getErrorText());
		return;
	}

	QString strLang = "en";

	g_UserSessionManager->LockThreadActive();
	switch(nTaskID)
	{
	case PROCESS_TYPE_PROJECT:
		StartProcessProject(err);
		break;
	case PROCESS_TYPE_USER:
		StartProcessUser(err);
		break;
	case PROCESS_TYPE_CONTACT:
		StartProcessContact(err, strLang);
	    break;
	case PROCESS_TYPE_DEBTOR:
		StartProcessDebtor(err);
	    break;
	case PROCESS_TYPE_SPL:
		StartProcessStoredProjectList(err);
		break;
	default:
		StartAllScheduledTasks(err, strLang);
	    break;
	}
	g_UserSessionManager->UnLockThreadActive();
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_FAIL,err.getErrorText());
	}

	if (nCallerID!=-1)
	{
		QStringList lstPersonID;
		lstPersonID<<QVariant(nCallerID).toString();
		if (!err.IsOK())
			g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_FAIL,err.getErrorText(),&lstPersonID);
		else
		{
			QString strMsg=GetLastStatus(nTaskID);
			if (strMsg.isEmpty())
				g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SYSTEM_IMP_EXP_SUCCESS_ALL,"",&lstPersonID);
			else
				g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SYSTEM_IMP_EXP_SUCCESS,strMsg,&lstPersonID);
		}
	}

	g_UserSessionManager->DeleteSession(err,strContextSession);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_FAIL,"Failed to delete system user session context: "+err.getErrorText());
		return;
	}

}
void ServerImportExportManager::Client_StartTaskDelayed(Status &pStatus,int nTaskID)
{
	pStatus.setError(0);
	emit SignalStartTask(g_UserSessionManager->GetPersonID(),nTaskID); 
}

