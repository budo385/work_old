#include "messagedispatcher_server.h"
#include "common/common/threadid.h"

#include "common/common/logger.h"
extern Logger				g_Logger;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;

MessageDispatcher_Server::MessageDispatcher_Server(QObject *parent)
:MessageDispatcher(parent),m_RpcMsg(NULL)
{

}

MessageDispatcher_Server::~MessageDispatcher_Server()
{
 if (m_RpcMsg)
 {
	 delete m_RpcMsg;
 }
}


void MessageDispatcher_Server::Init(HTTPServer	*pHttpServer)
{
	m_RpcMsg= new RpcStubServerMessageSet(pHttpServer,RPC_PROTOCOL_TYPE_XML_RPC);
}


void MessageDispatcher_Server::NewMessage(int nMsgID)
{
	Q_ASSERT(m_RpcMsg!=NULL); //if nuill abort

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_MESSAGE_DISPATCHER_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());

	QWriteLocker lock(&m_mutex); //access to list
	
	int nMsgNumber=m_lstMessages.find("MSG_ID",nMsgID,true);
	if (nMsgNumber<0)
		return;
	DbRecordSet rowMsg=m_lstMessages.getRow(nMsgNumber);

	lock.unlock();

	QList<int> lstUserSockets;
	QString strUsers=rowMsg.getDataRef(0,"MSG_TARGET_PERSON_IDS").toString();
	int nLoggedPersonSocket=rowMsg.getDataRef(0,"MSG_SENDER_SOCKET_ID").toInt();
	
	if (!strUsers.isEmpty())
	{
		QStringList lstStrUsers=strUsers.split(";");
		QStringList lstStrUsersNotLogged;
		int nSize=lstStrUsers.size();
		for(int i=0;i<nSize;++i)
		{
			QStringList lstPersonSockets=g_UserSessionManager->GetSocketListForPersonID(lstStrUsers.at(i).toInt());
			if (lstPersonSockets.size()>0)
			{
				int nSize2=lstPersonSockets.size();
				for(int k=0;k<nSize2;k++)
				{
					if (nLoggedPersonSocket>0)
						if (nLoggedPersonSocket==lstPersonSockets.at(k).toInt()) //skip current user fireback
							continue;
					lstUserSockets.append(lstPersonSockets.at(k).toInt());
				}
			}
			else
				lstStrUsersNotLogged.append(lstStrUsers.at(i));
		}
		
		lock.relock();
		if (lstStrUsersNotLogged.size()>0)
		{
			m_lstMessages.setData(nMsgNumber,"MSG_TARGET_PERSON_IDS",lstStrUsersNotLogged.join(";")); //if list is not completly empty store ids whom msg is not sent
		}
		else
		{
			m_lstMessages.setData(nMsgNumber,"MSG_TARGET_PERSON_IDS",QString(""));
			m_lstMessages.setData(nMsgNumber,"MSG_EXPIRE_DATE",QDateTime::currentDateTime()); //else delete msg
		}
		lock.unlock();
	}

	//BT: if users are specified but not logged, god damn then skip send msg!!!!
	if (!strUsers.isEmpty() && lstUserSockets.size()==0)
	{
		ClearExpiredMessages();
		return;
	}

	Status err;
	m_RpcMsg->ServerMsg(err,lstUserSockets,rowMsg);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_MESSAGE_DISPATCHER_FAILED,"Send Message failed "+err.getErrorText());
	}
	
	ClearExpiredMessages();
}
