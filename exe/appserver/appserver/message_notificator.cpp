#include "message_notificator.h"
#include "common/common/datahelper.h"
#include "trans/trans/httputil.h"
#include "common/common/threadid.h"
#include "db/db/dbsimpleorm.h"
//#include "../appserver_common/common_constants.h"
#include "trans/trans/applepushnotification.h"
#include "trans/trans/androidpushnotification.h"
#include <QCoreApplication>
#include "bus_core/bus_core/optionsandsettingsid.h"

#include "db/db/dbsqlmanager.h"
extern DbSqlManager	*g_DbManager;
#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;
#include "common/common/logger.h"
extern Logger							g_Logger;

#define MAX_SEND_TRIES 30
#define MESSAGE_STATUS_CREATED 0
#define MESSAGE_STATUS_SENT 1
#define MESSAGE_STATUS_ERROR 2

MessageNotificator::MessageNotificator()
{
	
	//set data for apple pushers:
	ApplePushNotification::m_strLocalCertificatePath=QCoreApplication::applicationDirPath()+"/settings/"+g_AppServer->GetIniFile()->m_strAppleLocalCertificateFile;
	ApplePushNotification::m_strPrivateKeyPath=QCoreApplication::applicationDirPath()+"/settings/"+g_AppServer->GetIniFile()->m_strApplePrivateKeyFile;

	ApplePushNotification::m_strPushServerURL=g_AppServer->GetIniFile()->m_strApplePushServerURL;
	ApplePushNotification::m_nPushServerPort=g_AppServer->GetIniFile()->m_nApplePushServerPort;
	ApplePushNotification::m_strFeedBackServerURL=g_AppServer->GetIniFile()->m_strAppleFeedBackServerURL;
	ApplePushNotification::m_nFeedBackServerPort=g_AppServer->GetIniFile()->m_nAppleFeedBackServerPort;
	//ApplePushNotification::m_StrPeerName=SERVICE_NAME;

	//android pushers:
	AndroidPushNotification::m_strGoogleAuthenticationID=g_AppServer->GetIniFile()->m_strGoogleAuthenticationID;
	AndroidPushNotification::m_strGooglePushServerURL=g_AppServer->GetIniFile()->m_strGooglePushServerURL;
	
	// Load up root certificate for Apple
	QFile rootCAFile(QCoreApplication::applicationDirPath()+"/settings/"+g_AppServer->GetIniFile()->m_strAppleRootCACertificateFile);
	QSslCertificate *rootCertificate=NULL;
	if (rootCAFile.open( QIODevice::ReadOnly ))
		rootCertificate = new QSslCertificate( &rootCAFile, QSsl::Der	);
	rootCAFile.close();

	// Load up WWDR root certificate for Apple
	QFile rootCAFile2(QCoreApplication::applicationDirPath()+"/settings/"+g_AppServer->GetIniFile()->m_strAppleWWDRCACertificateFile);
	QSslCertificate *rootCertificate2=NULL;
	if (rootCAFile2.open( QIODevice::ReadOnly ))
		rootCertificate2 = new QSslCertificate( &rootCAFile2, QSsl::Der	);
	rootCAFile2.close();

	QSslConfiguration *sslConfig = new QSslConfiguration(QSslConfiguration::defaultConfiguration());
	QList<QSslCertificate> *caCerts = new QList<QSslCertificate>(sslConfig->caCertificates() );

	bool bCerOK=false;
	if(rootCertificate)
	{
		if (!rootCertificate->isNull())
		{
			caCerts->append( *rootCertificate );
			sslConfig->setCaCertificates( *caCerts );
			bCerOK=true;
		}
	}
	if (!bCerOK)
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Apple Root CA certificate is not loaded!"));

	bCerOK=false;
	if(rootCertificate2)
	{
		if (!rootCertificate2->isNull())
		{
			caCerts->append( *rootCertificate2 );
			sslConfig->setCaCertificates( *caCerts );
			bCerOK=true;
		}
	}
	if (!bCerOK)
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Apple Worldwide Developer Relations certificate is not loaded!"));
		

	/*
	//global CA:
	for (int i=0;i<caCerts->size();i++)
	{
		qDebug() << "CA Authority Certificate - Organization:" << caCerts->at(i).subjectInfo(QSslCertificate::Organization)
			<< "Common Name:"  <<  caCerts->at(i).subjectInfo(QSslCertificate::CommonName)
			<< "Country: " << caCerts->at(i).subjectInfo(QSslCertificate::CountryName);
	}
	*/

	
	//this enables same config for all new incoming connections:
	QSslConfiguration::setDefaultConfiguration( *sslConfig );
	
}

MessageNotificator::~MessageNotificator()
{

}

//execute everything in list
void MessageNotificator::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	
#ifdef _DEBUG
	//return;
#endif // _DEBUG
	
	
	if(!m_Mutex.tryLock(100)) //thread is occupied leave it alone
		return;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("MessageNotificator: started"));

	Status err;
	DbSimpleOrm TableOrm_MessageUser(err,BUS_PUSH_MESSAGE,g_DbManager);
	if(!err.IsOK()) return;
	
	//Read all messages: 
	QString strWhere=" WHERE BPM_DAT_SENT IS NULL ORDER BY BPM_CREATED,BPM_STATUS";
	DbRecordSet lstMessages;
	TableOrm_MessageUser.Read(err,lstMessages,strWhere,DbSqlTableView::TVIEW_BUS_PUSH_MESSAGE);
	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: failed to load messages from database: %1").arg(err.getErrorText()));
		m_Mutex.unlock();
		return;
	}

	lstMessages.addColumn(QVariant::Int,"FOR_DELETE");
	lstMessages.setColValue("FOR_DELETE",0);

	//lstMessages.Dump();
	/*
	QSslConfiguration *sslConfig = new QSslConfiguration(QSslConfiguration::defaultConfiguration());
	QList<QSslCertificate> *caCerts = new QList<QSslCertificate>(
		sslConfig->caCertificates() );

	for (int i=0;i<caCerts->size();i++)
	{
		QString strCert;

		strCert.append("CA Authority Certificate - Organization:");
		strCert.append(caCerts->at(i).subjectInfo(QSslCertificate::Organization).join("--"));
		strCert.append("Common Name:");
		strCert.append(caCerts->at(i).subjectInfo(QSslCertificate::CommonName).join("--"));
		strCert.append("Country: ");
		strCert.append(caCerts->at(i).subjectInfo(QSslCertificate::CountryName).join("--"));
		strCert.append("Expiry Date: ");
		strCert.append(caCerts->at(i).expiryDate().toString("ddMMyyyy"));
		strCert.append("Issuer name: ");
		strCert.append(caCerts->at(i).issuerInfo(QSslCertificate::CommonName).join("--"));
		strCert.append("Issuer org: ");
		strCert.append(caCerts->at(i).issuerInfo(QSslCertificate::Organization).join("--"));

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,strCert);

	}
	*/
	
	//Try to send them'all:
	int nSize=lstMessages.getRowCount();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("MessageNotificator: found %1 push message to process").arg(nSize));
	if (nSize==0)
	{
		m_Mutex.unlock();
		return;
	}

	int nOK=0;
	int nErr=0;
	PushSender *pPushSender=NULL;
	for (int i=0;i<nSize;i++)
	{
		int nOS=lstMessages.getDataRef(i,"BPM_OS_TYPE").toInt();
		QString strPushToken1=lstMessages.getDataRef(i,"BPM_PUSH_TOKEN").toString();
		int nUserMsgID=lstMessages.getDataRef(i,"BPM_ID").toInt();
		int nSendTries=lstMessages.getDataRef(i,"BPM_SEND_TRIES").toInt();
		QString strMsg=lstMessages.getDataRef(i,"BPM_MESSAGE_BODY").toString();
		QString strSound=lstMessages.getDataRef(i,"BPM_SOUNDNAME").toString();
		int nBadgeNo =lstMessages.getDataRef(i,"BPM_BADGE").toInt();
		QString strCustomData =lstMessages.getDataRef(i,"BPM_CUSTOM_DATA_LIST").toString(); //email id

		//BT test:
		//strMsg=strMsg.toLatin1();
		//strMsg=strMsg.replace("<","");
		///strMsg=strMsg.replace(">","");
		//strMsg=strMsg.replace("\"","");
		//strMsg=strMsg.replace("\'","");
		if (strMsg.size()>130)
		{
			strMsg=strMsg.left(130)+"...";
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: sending push msg: %1, truncated to 130 chars").arg(strMsg));
		}
		
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: sending push msg: %1, token: %2").arg(strMsg).arg(strPushToken1));

		if (strPushToken1.isEmpty())
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: push token is empty, deleting from buffer: msg = %1, push = %2").arg(strMsg).arg(strPushToken1));
			lstMessages.setData(i,"FOR_DELETE",1);// schedule for delete if found in database
			continue;
		}

		//B.T. check error push messages:
		if (nSendTries>MAX_SEND_TRIES && lstMessages.getDataRef(i,"BPM_STATUS").toInt()!=MESSAGE_STATUS_SENT)
		{
			lstMessages.setData(i,"FOR_DELETE",1);// schedule for delete if found in database
		}
		else
		{
			nSendTries++;
			lstMessages.setData(i,"BPM_SEND_TRIES",nSendTries);
		}
			
		int nStatus;
		QString strError;
		
		if (!pPushSender)
			pPushSender =CreatePusherObject(nOS);

		if (pPushSender)
		{
			QStringList lstCustomData;
			if (!strCustomData.isEmpty())
			{
				lstCustomData = strCustomData.split(";",QString::SkipEmptyParts);
			}

			//qDebug()<<"Sending msg to push: "<<strPushToken1;

			if(pPushSender->SendPushMessage(strPushToken1, "",strMsg,nUserMsgID,lstCustomData,strSound,nBadgeNo)) 
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: push successfully sent: %1, token: %2").arg(strMsg).arg(strPushToken1));
				lstMessages.setData(i,"BPM_STATUS",MESSAGE_STATUS_SENT);
				//QString strMsg=QString("Successfully sent on %1").arg(QDateTime::currentDateTime().toString("ddMMyyy.HH.mm"));
				//lstMessages.setData(i,"BPM_ERROR_TEXT",strMsg);
				lstMessages.setData(i,"BPM_ERROR_TEXT","");
				lstMessages.setData(i,"BPM_DAT_SENT",QDateTime::currentDateTime());
				//lstMessages.setData(i,"FOR_DELETE",1); //leave messages in database for 14 days, then delete
				nOK++;
			}
			else
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: failed to send push msg: %1, token: %2, error: %3").arg(strMsg).arg(strPushToken1).arg(pPushSender->GetLastError().left(1000)));
				lstMessages.setData(i,"BPM_STATUS",MESSAGE_STATUS_ERROR);
				lstMessages.setData(i,"BPM_ERROR_TEXT",pPushSender->GetLastError().left(1000));
				nErr++;
			}

			//nSendTries++;
			lstMessages.setData(i,"BPM_SEND_TRIES",nSendTries);

			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: destroying push data"));
			//pPushSender->deleteLater(); //destroy pusher
			//pPushSender=NULL;
			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: push data destroyed"));
		}
		
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: destroying push data"));
	if (pPushSender)
		pPushSender->deleteLater();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: push data destroyed"));


	//-------------------------------------------------------------------------
	//now refresh push token table: set last success date and check invalid ones
	//-------------------------------------------------------------------------
	DbRecordSet lstTemp;
	lstTemp.copyDefinition(lstMessages);
	lstMessages.find("BPM_STATUS",MESSAGE_STATUS_SENT,false,false,true,true); //find all that have status=0
	lstTemp.merge(lstMessages,true);
	lstTemp.removeDuplicates(lstTemp.getColumnIdx("BPM_PUSH_TOKEN"));
	//now from this list update last success date in push table and check all rest push tokens, if > 7 days then delete it from table:
	int nSizePushSuccess=lstTemp.getRowCount();
	for (int i=0;i<nSizePushSuccess;i++)
	{
		QString strSQL=QString("UPDATE BUS_PUSH_TOKEN_PERSON SET BPT_DAT_LAST_SUCCESS_SEND=CURRENT_TIMESTAMP WHERE BPT_PUSH_TOKEN='%1' AND BPT_PERSON_FK_ID=%2").arg(lstTemp.getDataRef(i,"BPM_PUSH_TOKEN").toString()).arg(lstTemp.getDataRef(i,"BPM_PERSON_FK_ID").toString());
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator, dump SQL: %1").arg(strSQL));
		TableOrm_MessageUser.GetDbSqlQuery()->Execute(err,strSQL);
		if (!err.IsOK())
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: failed to update push token table: %1").arg(err.getErrorText()));
		}
	}
	//delete old ones:
	QDateTime datOldPushTokens=QDateTime::currentDateTime().addDays(-14); //14days
	QString strSQL=QString("DELETE FROM BUS_PUSH_TOKEN_PERSON WHERE BPT_DAT_LAST_SUCCESS_SEND<?" );
	TableOrm_MessageUser.GetDbSqlQuery()->Prepare(err,strSQL);
	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: failed to delete old push tokens, prepare: %1").arg(err.getErrorText()));
	}
	TableOrm_MessageUser.GetDbSqlQuery()->bindValue(0,datOldPushTokens);
	TableOrm_MessageUser.GetDbSqlQuery()->ExecutePrepared(err);
	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: failed to delete old push tokens, execute: %1").arg(err.getErrorText()));
	}


	//-------------------------------------------------------------------------
	//now delete all push messages with FOR DELETE=1:
	//-------------------------------------------------------------------------
	/*
	lstTemp.clear();
	lstMessages.find("FOR_DELETE",1,false,false,true,true); //find all that are for delete
	lstTemp.merge(lstMessages,true);
	TableOrm_MessageUser.DeleteFromParentIDs(err,"BPM_ID",lstTemp,"BPM_ID");
	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: failed to delete sent push messages: %1").arg(err.getErrorText()));
	}
	*/

	//-------------------------------------------------------------------------
	//now update all rest back to DB:
	//-------------------------------------------------------------------------
	//lstMessages.Dump();
	lstTemp.clear();
	lstMessages.find("FOR_DELETE",0,false,false,true,true); //find all rest
	lstTemp.merge(lstMessages,true);
	lstTemp.removeColumn(lstTemp.getColumnIdx("FOR_DELETE"));

	TableOrm_MessageUser.Write(err,lstTemp);
	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("MessageNotificator: failed to delete sent push messages: %1").arg(err.getErrorText()));
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("MessageNotificator: tried to sent %1 total messages, successfully sent: %2, not sent (error): %3").arg(nSize).arg(nOK).arg(nErr));
	
	m_Mutex.unlock();
}





PushSender * MessageNotificator::CreatePusherObject(int nMobileOperatingSystem)
{
	switch (nMobileOperatingSystem)
	{
	case PushNotification::MOB_OS_MAC_OS:
		return new PushSender(PushSender::APPLE); //set 30secs
		break;
	case PushNotification::MOB_OS_WIN:
		return new PushSender(PushSender::WINDOWS);
		break;
	case PushNotification::MOB_OS_ANDROID:
		return new PushSender(PushSender::ANDROID);
		break;
	}

	return NULL;
}

/*
	Insert new push message into DB and return ID or error.
	Tasker will later pick up this from DB, if send is ok, it will be deleted from DB, else error status will be set.
	Tasker will also delete old push messages from database with error status if older >2 months.
*/

int MessageNotificator::SchedulePushMessageForSend(Status &pStatus, QString strMessage, QString strPushToken, int nPersonID, int nOSType, QString strPushSoundName, int nBadgeNumber )
{
	DbSimpleOrm TableOrm(pStatus, BUS_PUSH_MESSAGE, g_DbManager); 
	if(!pStatus.IsOK()) return 0;

	DbRecordSet rowPushMsg;
	rowPushMsg.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PUSH_MESSAGE));

	rowPushMsg.addRow();
	rowPushMsg.setData(0,"BPM_CREATED",QDateTime::currentDateTime());
	rowPushMsg.setData(0,"BPM_MESSAGE_BODY",strMessage);
	rowPushMsg.setData(0,"BPM_PUSH_TOKEN",strPushToken);
	rowPushMsg.setData(0,"BPM_OS_TYPE",nOSType);
	rowPushMsg.setData(0,"BPM_BADGE",nBadgeNumber);
	rowPushMsg.setData(0,"BPM_SOUNDNAME",strPushSoundName);
	rowPushMsg.setData(0,"BPM_PERSON_FK_ID",nPersonID);
	rowPushMsg.setData(0,"BPM_STATUS",0); //0 - ok, 1 -error
	rowPushMsg.setData(0,"BPM_SEND_TRIES",0); //0
		
	TableOrm.Write(pStatus,rowPushMsg);
	if(!pStatus.IsOK()) return 0;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("MessageNotificator, push created for send, push token: %1, person: %2, message: %3").arg(strPushToken).arg(nPersonID).arg(strMessage));

	return rowPushMsg.getDataRef(0,"BPM_ID").toInt();
}

//call this API when adding new account or per email account if there is problem sending email...
void MessageNotificator::SendErrorMessage(int nPersonID, QString strErrorText,int nErrorCode, QString strEmailAccount)
{
	strEmailAccount=strEmailAccount.toLower();

	Status pStatus;
	DbSimpleOrm TableOrm(pStatus, BUS_PUSH_MESSAGE, g_DbManager); 
	if(!pStatus.IsOK()) return;

	//load pers settings
	DbRecordSet lstPersSettings;
	LoadEmailInPersonSettings(pStatus,nPersonID,lstPersSettings);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstPersPushTokens;
	LoadPersonPushTokens(pStatus,nPersonID,lstPersPushTokens);
	if(!pStatus.IsOK()) return;
	

	//check flags:
	if (GetEmailInPersonSettings_Int(lstPersSettings,EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE)<=0 || lstPersPushTokens.getRowCount()==0)
		return;

	//check flags:
	QString strLastDateTimeErrorSent = GetEmailInPersonSettings_String(lstPersSettings,EMAIL_IN_SETTINGS_PUSH_ERROR_LAST_DATETIME);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendErrorMessage: last error msg sent '%1', user: %2").arg(strLastDateTimeErrorSent).arg(nPersonID));
	
	if (!strLastDateTimeErrorSent.isEmpty() && QDateTime::fromString(strLastDateTimeErrorSent,"ddMMyyyy-hhmm").isValid())
	{
		QDateTime lstDateTimeErr=QDateTime::fromString(strLastDateTimeErrorSent,"ddMMyyyy-hhmm");
		if (lstDateTimeErr>QDateTime::currentDateTime().addDays(-1))
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendErrorMessage: error msg not sent as is already sent in last 24hrs: %1, user: %2").arg(strLastDateTimeErrorSent).arg(nPersonID));
			return;
		}
	}

	
	//check per email account flags:
	if (!strEmailAccount.isEmpty())
	{	
		QStringList lstEmailAccOnOffList=GetEmailInPersonSettings_List(lstPersSettings,EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE_EMAIL_ACCOUNT_LIST);
		int nSize=lstEmailAccOnOffList.size();
		for (int i=0;i<nSize;i++)
		{
			if (lstEmailAccOnOffList.at(i).indexOf(strEmailAccount)>=0)
			{
				int nOnOff=lstEmailAccOnOffList.at(i).mid(lstEmailAccOnOffList.at(i).indexOf(",")+1).toInt();
				if (nOnOff==0)
					return;
			}
		}
	}

	DbRecordSet lstPushMsg;
	lstPushMsg.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PUSH_MESSAGE));

	QString strCustomData="email;"+strEmailAccount;

	//prepend email address to push message:
	strErrorText=strEmailAccount+": "+strErrorText;

	//now send push for all registered tokens, can be more then 1:
	int nSize=lstPersPushTokens.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strPushToken=lstPersPushTokens.getDataRef(i,"BPT_PUSH_TOKEN").toString();
		int nOSType=lstPersPushTokens.getDataRef(i,"BPT_OS_TYPE").toInt();
		//SchedulePushMessageForSend(pStatus,strErrorText,strPushToken,nPersonID,nOSType); //use default sound and badge for error messages

		lstPushMsg.addRow();
		int nRow=lstPushMsg.getRowCount()-1;
		lstPushMsg.setData(nRow,"BPM_CREATED",QDateTime::currentDateTime());
		lstPushMsg.setData(nRow,"BPM_MESSAGE_BODY",strErrorText);
		lstPushMsg.setData(nRow,"BPM_PUSH_TOKEN",strPushToken);
		lstPushMsg.setData(nRow,"BPM_OS_TYPE",nOSType);
		lstPushMsg.setData(nRow,"BPM_BADGE",0);
		lstPushMsg.setData(nRow,"BPM_SOUNDNAME","alarm.caf");
		lstPushMsg.setData(nRow,"BPM_PERSON_FK_ID",nPersonID);
		lstPushMsg.setData(nRow,"BPM_STATUS",0); //0 - ok, 1 -error
		lstPushMsg.setData(nRow,"BPM_SEND_TRIES",0); //0
		lstPushMsg.setData(nRow,"BPM_CUSTOM_DATA_LIST",strCustomData); 
	}

	TableOrm.Write(pStatus,lstPushMsg);
	if(!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendErrorMessage: failed to add push message to queue: %1").arg(pStatus.getErrorText()));
		return;
	}

	//write sent time:
	strLastDateTimeErrorSent = QDateTime::currentDateTime().toString("ddMMyyyy-hhmm");
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendErrorMessage: last error msg saved to %1 for user %2").arg(strLastDateTimeErrorSent).arg(nPersonID));
	SaveLastErrorMsgDateTime(pStatus,strLastDateTimeErrorSent,nPersonID);
	if(!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendErrorMessage: failed to execute SaveLastErrorMsgDateTime: %1").arg(pStatus.getErrorText()));
		return;
	}

}

//call this API per email account once
//if bem_recv_time < 1 day older then ignore
void MessageNotificator::SendNewEmailNotification(int nPersonID, QString strEmailAccount,DbRecordSet &lstNewEmails)
{
	strEmailAccount=strEmailAccount.toLower();

	if (lstNewEmails.getRowCount()==0)
		return;

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: try to send new email notification for user: %1, email account: %2").arg(nPersonID).arg(strEmailAccount));

	Status pStatus;
	DbSimpleOrm TableOrm(pStatus, BUS_PUSH_MESSAGE, g_DbManager); 
	if(!pStatus.IsOK()) return;

	//load pers settings
	DbRecordSet lstPersPushTokens;
	LoadPersonPushTokens(pStatus,nPersonID,lstPersPushTokens);
	if(!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendNewEmailNotification: failed to load personal push tokens: %1").arg(pStatus.getErrorText()));
		return;
	}
	if (lstPersPushTokens.getRowCount()==0)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: exiting no push tokens for user: %1").arg(nPersonID));
		return;
	}

	DbRecordSet lstPersSettings;
	LoadEmailInPersonSettings(pStatus,nPersonID,lstPersSettings);
	if(!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendNewEmailNotification: failed to load personal settings: %1").arg(pStatus.getErrorText()));
		return;
	}

	if (lstPersSettings.getRowCount()==0)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: exiting no personal settings for email in for user: %1").arg(nPersonID));
		return;
	}
	/*
	else
	{
		int nRow=lstPersSettings.find("BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE,true,false,true,true);
		if (nRow>=0)
		{
			int nSetting=lstPersSettings.getDataRef(nRow,"BOUS_VALUE").toInt();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE =%1 for user: %2").arg(nSetting).arg(nPersonID));
		}
		else
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: exiting no personal settings for EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE for user: %1").arg(nPersonID));
			return;
		}
	}
	*/

	//check flags:
	int nAllowToSendPush=GetEmailInPersonSettings_Int(lstPersSettings,EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE);
	if (nAllowToSendPush<=0 || lstPersPushTokens.getRowCount()==0)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: exiting, EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE=%1, user: %2").arg(nAllowToSendPush).arg(strEmailAccount));
		return;
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: proceed process for user: %1, email account: %2").arg(nPersonID).arg(strEmailAccount));

	//check per email account flags:
	if (!strEmailAccount.isEmpty())
	{	
		QStringList lstEmailAccOnOffList=GetEmailInPersonSettings_List(lstPersSettings,EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE_EMAIL_ACCOUNT_LIST);
		int nSize=lstEmailAccOnOffList.size();

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: checking allowed account list: %1, size: %2").arg(lstEmailAccOnOffList.join(";")).arg(lstEmailAccOnOffList.size()));

		for (int i=0;i<nSize;i++)
		{
			if (lstEmailAccOnOffList.at(i).indexOf(strEmailAccount)>=0)
			{
				int nOnOff=lstEmailAccOnOffList.at(i).mid(lstEmailAccOnOffList.at(i).indexOf(",")+1).toInt();

				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: value for acc:  %1").arg(lstEmailAccOnOffList.at(i).mid(lstEmailAccOnOffList.at(i).indexOf(",")+1)));

				if (nOnOff==0)
				{
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: exiting, as email is not allowed to accept emails: %1").arg(strEmailAccount));
					return;
				}
			}
		}
	}

	/*
	int nRow=lstPersSettings.find("BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE,true,false,true,true);
	if (nRow>=0)
	{
		int nSetting=lstPersSettings.getDataRef(nRow,"BOUS_VALUE").toInt();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE =%1 for user: %2").arg(nSetting).arg(nPersonID));
	}
	else
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: exiting no personal settings for EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE for user: %1").arg(nPersonID));
		return;
	}
	*/


	//now send push for all registered tokens, can be more then 1:
	int nState = GetEmailInPersonSettings_Int(lstPersSettings,EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE);
	if (nState==0) //not allowed to send push for new emails
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: exiting as EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE =%1 for user: %2").arg(nState).arg(nPersonID));
		return;
	}

	/*
	nRow=lstPersSettings.find("BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE,true,false,true,true);
	if (nRow>=0)
	{
		int nSetting=lstPersSettings.getDataRef(nRow,"BOUS_VALUE").toInt();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE =%1 for user: %2").arg(nSetting).arg(nPersonID));
	}
	else
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: exiting no personal settings for EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE for user: %1").arg(nPersonID));
		return;
	}*/

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: sending new email notification for user: %1, email account: %2 for %3 emails").arg(nPersonID).arg(strEmailAccount).arg(lstNewEmails.getRowCount()));

	QStringList lstAllowedEmails=GetEmailInPersonSettings_String(lstPersSettings,EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_ALLOWED_EMAIL_LIST).toLower().split(",",QString::SkipEmptyParts);
	int nSilentMode = GetEmailInPersonSettings_Int(lstPersSettings,EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE);
	int nBadge=0;
	QString strSoundName="";
	if (nSilentMode==0)
		strSoundName="nosound";
	else
		strSoundName="new-mail.caf";

	//get badge number (number of unread emails per person)
	GetUnreadEmailsCount(pStatus,nPersonID,nBadge);
	if(!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendNewEmailNotification: failed count unread emails: %1").arg(pStatus.getErrorText()));
		return;
	}
		
	DbRecordSet lstPushMsg;
	lstPushMsg.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PUSH_MESSAGE));

	int nSize=lstPersPushTokens.getRowCount();
	int nSize2=lstNewEmails.getRowCount();
	for (int i=0;i<nSize;i++)
	{

		QString strPushToken=lstPersPushTokens.getDataRef(i,"BPT_PUSH_TOKEN").toString();
		int nOSType=lstPersPushTokens.getDataRef(i,"BPT_OS_TYPE").toInt();
		
		//loop through list of emails
		for (int k=0;k<nSize2;k++)
		{
			QDateTime datReceived=lstNewEmails.getDataRef(k,"BEM_RECV_TIME").toDateTime();
			if (datReceived.isValid())
			{
				if (datReceived<QDateTime::currentDateTime().addDays(-1))
				{
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendNewEmailNotification: skip mail as recv_time is older then 24hrs: %1").arg(datReceived.toString("ddMMyyyy-hhmm")));
					continue;
				}
			}
			int nEmailID=lstNewEmails.getDataRef(k,"BEM_ID").toInt();
			//skip if id = 0...that was misfire
			if (nEmailID==0)
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendNewEmailNotification: skip mail as email id = 0"));
				continue;
			}

			QString strCustomData="id;"+lstNewEmails.getDataRef(k,"BEM_ID").toString();
			strCustomData+=";email;"+strEmailAccount;

			QString strMessage;
			if (nState==1 || nState==3)
			{
				if (nSilentMode==2) //if silent =2 then send message
				{
					strMessage=QString("New email from %1: %2").arg(lstNewEmails.getDataRef(k,"BEM_TRUE_FROM").toString()).arg(lstNewEmails.getDataRef(k,"BEM_SUBJECT").toString());
				}

				//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: create push record: %1, token: %2, badge: %3 ").arg(nPersonID).arg(strPushToken).arg(nBadge));

				//SchedulePushMessageForSend(pStatus,strMessage,strPushToken,nPersonID,nOSType,strSoundName,nBadge);
				lstPushMsg.addRow();
				int nRow=lstPushMsg.getRowCount()-1;
				lstPushMsg.setData(nRow,"BPM_CREATED",QDateTime::currentDateTime());
				lstPushMsg.setData(nRow,"BPM_MESSAGE_BODY",strMessage);
				lstPushMsg.setData(nRow,"BPM_PUSH_TOKEN",strPushToken);
				lstPushMsg.setData(nRow,"BPM_OS_TYPE",nOSType);
				lstPushMsg.setData(nRow,"BPM_BADGE",nBadge);
				lstPushMsg.setData(nRow,"BPM_SOUNDNAME",strSoundName);
				lstPushMsg.setData(nRow,"BPM_PERSON_FK_ID",nPersonID);
				lstPushMsg.setData(nRow,"BPM_STATUS",0); //0 - created, 1 - ok, 2 - error
				lstPushMsg.setData(nRow,"BPM_SEND_TRIES",0); //0
				lstPushMsg.setData(nRow,"BPM_CUSTOM_DATA_LIST",strCustomData); //semicolon separated pairs of name and value0

				if (nState==3) //lookup list, if not found sound='nosound', else sound
				{
					lstPushMsg.setData(nRow,"BPM_SOUNDNAME","nosound");

					QString strFrom=ExtractEmail(lstNewEmails.getDataRef(k,"BEM_TRUE_FROM").toString()).toLower();
					int nSize3=lstAllowedEmails.size();
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendNewEmailNotification: from address: %1, list: %2").arg(strFrom).arg(lstAllowedEmails.join(",")));
					for (int z=0;z<nSize3;z++)
					{
						if (strFrom.indexOf(lstAllowedEmails.at(z))>=0)
						{
							lstPushMsg.setData(nRow,"BPM_SOUNDNAME","new-mail.caf");
							break;
						}
					}
				}

				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: create push record, person: %1, token: %2, push msg: %3, email_id: %4 ").arg(nPersonID).arg(strPushToken).arg(nBadge).arg(strMessage).arg(nEmailID));
			}
			else
			{
				QString strFrom=ExtractEmail(lstNewEmails.getDataRef(k,"BEM_TRUE_FROM").toString()).toLower();
				int nSize3=lstAllowedEmails.size();
				for (int z=0;z<nSize3;z++)
				{
					if (strFrom.indexOf(lstAllowedEmails.at(z))>=0)
					{
						//SchedulePushMessageForSend(pStatus,strMessage,strPushToken,nPersonID,nOSType,strSoundName,nBadge);
						lstPushMsg.addRow();
						int nRow=lstPushMsg.getRowCount()-1;
						lstPushMsg.setData(nRow,"BPM_CREATED",QDateTime::currentDateTime());
						lstPushMsg.setData(nRow,"BPM_MESSAGE_BODY",strMessage);
						lstPushMsg.setData(nRow,"BPM_PUSH_TOKEN",strPushToken);
						lstPushMsg.setData(nRow,"BPM_OS_TYPE",nOSType);
						lstPushMsg.setData(nRow,"BPM_BADGE",nBadge);
						lstPushMsg.setData(nRow,"BPM_SOUNDNAME",strSoundName);
						lstPushMsg.setData(nRow,"BPM_PERSON_FK_ID",nPersonID);
						lstPushMsg.setData(nRow,"BPM_STATUS",0); //0 - ok, 1 -error
						lstPushMsg.setData(nRow,"BPM_SEND_TRIES",0); //0
						lstPushMsg.setData(nRow,"BPM_CUSTOM_DATA_LIST",strCustomData); //semicolon separated pairs of name and value0
						break;
					}
				}

				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("SendNewEmailNotification: create push record, person: %1, token: %2, push msg: %3, email_id: %4 ").arg(nPersonID).arg(strPushToken).arg(nBadge).arg(strMessage).arg(nEmailID));
			}
		}
	}

	TableOrm.Write(pStatus,lstPushMsg);
	if(!pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1, QString("SendNewEmailNotification: failed to add push message to queue: %1").arg(pStatus.getErrorText()));
		return;
	}

}

void MessageNotificator::LoadEmailInPersonSettings( Status &pStatus, int nPersonID, DbRecordSet &lstPersSettingsData )
{
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//load settings
	QString strSQL = QString("SELECT BOUS_SETTING_ID, BOUS_VALUE, BOUS_VALUE_STRING FROM BUS_OPT_SETTINGS WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_PERSON_ID =%2").arg(EMAIL_IN_SETTINGS).arg(nPersonID);
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	query.FetchData(lstPersSettingsData);
}

int MessageNotificator::GetEmailInPersonSettings_Int( DbRecordSet &lstPersSettingsData, int nSettingsID )
{
#ifdef _DEBUG
	return 1;
#endif

	int nRow=lstPersSettingsData.find("BOUS_SETTING_ID",nSettingsID,true,false,true,true);
	if (nRow>=0)
	{
		return lstPersSettingsData.getDataRef(nRow,"BOUS_VALUE").toInt();
	}
	else
		return 0;
}

QString MessageNotificator::GetEmailInPersonSettings_String( DbRecordSet &lstPersSettingsData, int nSettingsID )
{
	int nRow=lstPersSettingsData.find("BOUS_SETTING_ID",nSettingsID,true,false,true,true);
	if (nRow>=0)
	{
		return lstPersSettingsData.getDataRef(nRow,"BOUS_VALUE_STRING").toString();
	}
	else
		return "";
}

QStringList MessageNotificator::GetEmailInPersonSettings_List( DbRecordSet &lstPersSettingsData, int nSettingsID )
{

	int nRow=lstPersSettingsData.find("BOUS_SETTING_ID",nSettingsID,true,false,true,true);
	if (nRow>=0)
	{
		return lstPersSettingsData.getDataRef(nRow,"BOUS_VALUE_STRING").toString().split(";",QString::SkipEmptyParts);
	}
	else
		return QStringList();
}



void MessageNotificator::GetUnreadEmailsCount(Status &Ret_pStatus, int nPersonID, int &Ret_nCount)
{
	Ret_nCount=0;
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,g_DbManager);
	if(!Ret_pStatus.IsOK()) return;

	//delete rows in chunks
	QString strSQL="SELECT COUNT(BEM_ID) CNT FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID WHERE BEM_UNREAD_FLAG=1 AND CENT_OWNER_ID ="+QString::number(nPersonID);
	query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (query.next())
	{
		Ret_nCount=query.value(0).toInt();
	}
}

void MessageNotificator::LoadPersonPushTokens( Status &pStatus, int nPersonID, DbRecordSet &lstPersPushTokens )
{
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//load settings
	QString strSQL = QString("SELECT BPT_PUSH_TOKEN, BPT_OS_TYPE FROM BUS_PUSH_TOKEN_PERSON WHERE BPT_PERSON_FK_ID=%1").arg(nPersonID);
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	query.FetchData(lstPersPushTokens);

#ifdef _DEBUG

	if (lstPersPushTokens.getRowCount()==0)
	{
		lstPersPushTokens.destroy();
		lstPersPushTokens.addColumn(QVariant::String,"BPT_PUSH_TOKEN");
		lstPersPushTokens.addColumn(QVariant::Int,"BPT_OS_TYPE");
		lstPersPushTokens.addRow();
		lstPersPushTokens.setData(0,0,"aaaaaaaaaaaaaaaa");
		lstPersPushTokens.setData(0,1,1);
	}

#endif
}

void MessageNotificator::SaveLastErrorMsgDateTime(Status &pStatus,QString strDateErr, int nPersonID)
{
	DbSimpleOrm TableOrm(pStatus, BUS_OPT_SETTINGS, g_DbManager); 
	if(!pStatus.IsOK()) return;

	DbRecordSet lstData;
	TableOrm.Read(pStatus,lstData,QString("WHERE BOUS_SETTING_ID =%1 AND BOUS_PERSON_ID =%2").arg(EMAIL_IN_SETTINGS_PUSH_ERROR_LAST_DATETIME).arg(nPersonID));
	if(!pStatus.IsOK()) return;

	if (lstData.getRowCount()==0)
	{
		lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
		lstData.addRow();
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_SET_ID",EMAIL_IN_SETTINGS);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_ERROR_LAST_DATETIME);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_VISIBLE",1);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_TYPE",0);
		lstData.setData(lstData.getRowCount()-1,"BOUS_PERSON_ID",nPersonID);
		lstData.setData(lstData.getRowCount()-1,"BOUS_VALUE_STRING",strDateErr);
	}
	else
		lstData.setData(0,"BOUS_VALUE_STRING",strDateErr);

	TableOrm.Write(pStatus,lstData);
}

QString MessageNotificator::ExtractEmail(QString strEmail)
{
	if (strEmail.indexOf("<")>=0 && strEmail.indexOf(">")>=0)
	{
		int nPos1=strEmail.indexOf("<")+1;
		int nLength=strEmail.indexOf(">")-nPos1;
		return strEmail.mid(nPos1,nLength);
	}
	else
		return strEmail;
}

//clean up old push messages older then 30 days:
void MessageNotificator::CleanUp( Status &pStatus )
{
	DbSqlQuery query(pStatus, g_DbManager);	
	if(!pStatus.IsOK())return;
	
	//delete old ones:
	QDateTime datOldPushTokens=QDateTime::currentDateTime().addDays(-30); //14days
	QString strSQL=QString("DELETE FROM BUS_PUSH_MESSAGE WHERE BPM_CREATED<?" );
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	query.bindValue(0,datOldPushTokens);
	query.ExecutePrepared(pStatus);
}