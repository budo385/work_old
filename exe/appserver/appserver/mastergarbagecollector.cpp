#include "mastergarbagecollector.h"
#include "common/common/logger.h"
extern Logger				g_Logger;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_server/bus_server/privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 

MasterGarbageCollector::MasterGarbageCollector(SystemServiceSet * pSysServices, int nAppServerSessionID)
{
	m_pSysServices=pSysServices;
	m_nAppServerSessionID=nAppServerSessionID;
}

MasterGarbageCollector::~MasterGarbageCollector()
{

}




//start every time, clears: user session table & core locking (cascade delete on DB)
//only used on APP. server (not on THICK)
void MasterGarbageCollector::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	Status err;
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_MASTER_GARBAGE_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());

	//Check all dead server: take their sessions and assign it to master, set inactive=flag
	m_pSysServices->AppSrvSession->CheckServerSessions(err,m_nAppServerSessionID);
	if (!err.IsOK())
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_MASTER_GARBAGE_FAILED,"Failed to clean application server sessions: "+err.getErrorText());
	
}


/*
//if not create session for GBC:
void MasterGarbageCollector::CreateSession(Status &err)
{
	if (m_strSystemSession.isEmpty())
	{
		DbRecordSet rowUserData;
		g_PrivateServiceSet->CoreUser->GetUserByUserName(err,"system",rowUserData);
		if (!err.IsOK())return;
		g_UserSessionManager->CreateSession(err,m_strSystemSession,rowUserData,"","",true);
		if (!err.IsOK())return;
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Master Garbage collector using session:"+m_strSystemSession);
	}
}*/