#ifndef SERVERIMPORTEXPORTMANAGER_H
#define SERVERIMPORTEXPORTMANAGER_H

#include "common/common/backgroundtaskinterface.h"
#include "common/common/status.h"
#include "bus_core/bus_core/importexportmanagerabstract.h"
#include "trans/trans/ftpconnectionsettings.h"

class ServerImportExportManager : public ImportExportManagerAbstract, public BackgroundTaskInterface
{
public:
	ServerImportExportManager();

	void ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");	
	void Client_StartTaskDelayed(Status &pStatus,int nTaskID=-1);

	
};

#endif // SERVERIMPORTEXPORTMANAGER_H
