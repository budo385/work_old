// MailTransaction.cpp

// To do:
// * Provide an indication of whether an error was a communication error
//   or a protocol error.
// * Maybe close the connection on any comm error.  It might be a good idea
//   to have an SmptClient::IsConnected() member function.
// * Set the timeout values according to RFC 2821

#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "MailTransaction.h"
using namespace std;
using namespace mailpp;

#if defined(WIN32) || defined(_WIN32)
#  define strdup   _strdup
#  define snprintf _snprintf
#endif


MailTransaction::MailTransaction()
{
	m_nLastErr = 0;
	mFrom = 0;

	mMaxTo = 0;
    mNumTo = 0;
    mTo = 0;

	mBody = 0;
}

MailTransaction::~MailTransaction()
{
    if (mFrom) free(mFrom);
    mFrom = 0;
    
	while (mNumTo > 0) {
        --mNumTo;
        free(mTo[mNumTo]);
    }
    if (mTo) free(mTo);
    mTo = 0;
    mMaxTo = 0;
}

void MailTransaction::SetFrom(const char* from)
{
    if (mFrom) free(mFrom);
    mFrom = strdup(from);
}

void MailTransaction::AddTo(const char* to)
{
    if (mNumTo == mMaxTo) {
        int maxTo = mMaxTo + 10;
        char** to_ = (char**) realloc(mTo, maxTo*sizeof(char*));
        if (to_) {
            mMaxTo = maxTo;
            mTo = to_;
        }
    }
    if (mNumTo < mMaxTo) {
        mTo[mNumTo++] = strdup(to);
    }
}

void MailTransaction::SetBody(const char* body)
{
	if (mBody) free(mBody);
    mBody = strdup(body);
}

int MailTransaction::Send(SmtpClient& client)
{
    if (mFrom && mNumTo > 0) {
        return Send_MAIL(client);
    }
    else {
        cout << "Can't process incomplete mail transaction" << endl;
    }
    return -1;
}

int MailTransaction::Send_MAIL(SmtpClient& client)
{
    //
    // If the server supports it, send the MAIL command with the SIZE
    // parameter.
    //
    // This optional SMTP capability allows a server to reject a message
    // before it is sent if the server considers the message too big.  The
    // alternative is for the client to send the complete message, which is
    // then rejected by the server after it is sent.  Obviously, the SIZE
    // extension is a very useful extension.  Note that on Unix systems,
    // the data sent is actually larger than the file size because of the
    // LF -> CR LF conversion.  In most cases, a size estimate, and not the
    // exact size, is sufficient.
    //
	int commErr = 0;
    if (client.HasCapability("SIZE")) {
        int fileSize = (int) strlen(mBody);
        char params[40];
        snprintf(params, sizeof(params), "SIZE=%d", fileSize);
        commErr = client.MailFrom(mFrom, params);
    }
    else {
        commErr = client.MailFrom(mFrom);
    }
    if (! commErr) {
        int replyCode = client.ReplyCode();
        if (replyCode/100%10 == 2) {
            return Send_RCPT(client);
        }
        else /* if (replyCode/100%10 != 2) */ {
            ReportProtocolError("SMTP MAIL command failed", client);
        }
    }
    else /* if (commErr) */ {
        ReportNetworkError("Network error", client);
    }
    if (! commErr) {
        client.Rset();
    }
    return -1;
}

int MailTransaction::Send_RCPT(SmtpClient& client)
{
    int commErr = false;
    int okTo = 0;
    for (int i=0; i < mNumTo; ++i) {
        commErr = client.RcptTo(mTo[i]);
        if (! commErr) {
            int replyCode = client.ReplyCode();
            if (replyCode/100%10 == 2) {
                ++okTo;
            }
            else /* if (replyCode/100%10 != 2) */ {
                char buf[200];
                snprintf(buf, sizeof(buf), "SMTP RCPT command failed "
                    "for recipient %s", mTo[i]);
                ReportProtocolError(buf, client);
            }
        }
        else /* if (commErr) */ {
            ReportNetworkError("Network error", client);
            break;
        }
    }
    if (! commErr) {
        if (okTo > 0) {
            return Send_DATA(client);
        }
        else {
            client.Rset();
        }
    }
    return -1;
}

int MailTransaction::Send_DATA(SmtpClient& client)
{
    int commErr = 0;
    commErr = client.Data();
    if (! commErr) {
        int replyCode = client.ReplyCode();
        if (replyCode/100%10 == 3) {
			int nBodyPos = 0;
			int nBodyLen = (NULL != mBody) ? strlen(mBody) : 0;

            const int bufSize = 1024;
            char buf[bufSize];
            int bufPos = 0;
            int flags = SmtpClient::FIRST;
            bool isBufSent = false;
            char c;
            while (nBodyPos < nBodyLen) {
				//get char
				c = *(mBody+nBodyPos);
				nBodyPos++;

                // If the buffer is full, or nearly so, then send it
                if (bufPos + 2 > bufSize) {
                    commErr = client.SendLines(buf, bufPos, flags);
                    isBufSent = true;
                    bufPos = 0;
                    flags = 0;
                    if (commErr) {
                        ReportNetworkError("Network error", client);
                        break;
                    }
                }
                // Convert line ending to CR LF unconditionally
                if (c == '\n') {
                    buf[bufPos++] = '\r';
                    buf[bufPos++] = '\n';
                }
                else if (c == '\r') {
                }
                else {
                    buf[bufPos++] = c;
                }
            }
            if (! commErr) {
                // Send the final partial buffer
                flags = SmtpClient::LAST;
                if (! isBufSent) {
                    flags |= SmtpClient::FIRST;
                }
                commErr = client.SendLines(buf, bufPos, flags);
                if (! commErr) {
                    replyCode = client.ReplyCode();
                    if (replyCode/100%10 == 2) {
                        //
                        // Return here on success
                        //
                        return 0;
                    }
                    else /* if (replyCode/100%10 != 2) */ {
                        ReportProtocolError("Sending mail data failed",client);
                    }
                }
                else /* if (commErr) */ {
                    ReportNetworkError("Network error", client);
                }
            }
        }
        else /* if (replyCode/100%10 != 3) */ {
            ReportProtocolError("SMTP DATA command failed", client);
        }
    }
    else /* if (commErr) */ {
        ReportNetworkError("Network error", client);
    }
    if (! commErr) {
        client.Rset();
    }
    //
    // Return here on failure
    //
    return -1;
}

void MailTransaction::ReportNetworkError(const char* message,
    SmtpClient& client)
{
	m_nLastErr		= client.ErrorCode();
	m_strLastErr	= client.ErrorMessage();

    cout << message << endl;
    cout << "Error code: " << client.ErrorCode() << endl;
    cout << "Error message: " << client.ErrorMessage() << endl;
    cout << "OS error code: " << client.OsErrorCode() << endl;
}

void MailTransaction::ReportProtocolError(const char* message,
    SmtpClient& client)
{
	m_nLastErr		= client.ReplyCode();
	m_strLastErr	= client.ReplyText();

	cout << message << endl;
    cout << "Server's reply was: "
         << client.ReplyCode() << " "
         << client.ReplyText() << endl;
}
