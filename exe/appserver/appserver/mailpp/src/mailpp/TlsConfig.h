//=============================================================================
// TlsConfig.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef TLS_CONFIG_H
#define TLS_CONFIG_H

/// Class for setting TLS configuration options

class MAILPP_API TlsConfig
{
public:

    enum Option {
        OPENSSL_CA_FILE = 101,
        OPENSSL_CA_DIR = 102,
        OPENSSL_VERIFY_DEPTH = 103,
        OPENSSL_CIPHER_LIST = 104,
        OPENSSL_RANDOM_BYTES = 105
    };

    /// Gets the singleton instance
    static TlsConfig* Instance();

    /// Destructor
    virtual ~TlsConfig();

    /// Frees the TlsConfig object
    static void Release();

    /// Sets a TLS configuration option
    virtual int SetOption(Option option, const char* value) = 0;

protected:

    static TlsConfig* theInstance;
};

#endif
