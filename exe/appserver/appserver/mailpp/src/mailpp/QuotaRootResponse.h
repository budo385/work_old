//=============================================================================
// QuotaRootResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef QUOTA_ROOT_RESPONSE_H
#define QUOTA_ROOT_RESPONSE_H

/// Class that represents an IMAP4 QUOTAROOT response

class MAILPP_API QuotaRootResponse : public Response
{
public:

    /// Default constructor
    QuotaRootResponse();

    /// Copy constructor
    QuotaRootResponse(const QuotaRootResponse& other);

    /// Destructor
    virtual ~QuotaRootResponse();

    /// Assignment operator
    QuotaRootResponse& operator = (const QuotaRootResponse& other);

    /// Gets the name of the mailbox
    const char* MailboxName() const;

    /// Gets the number of quota roots
    unsigned NumQuotaRoots() const;

    /// Gets the quota root at the specified position
    const char* QuotaRootAt(unsigned index) const;

    /// Called by the parser to set the data for this object
    void ImportData(ListItem* root);

private:
    char* mMailboxName;
    StringSequence mQuotaRoots;

};

#endif


