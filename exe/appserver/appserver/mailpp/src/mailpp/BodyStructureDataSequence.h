//=============================================================================
// BodyStructureDataSequence.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef BODY_STRUCTURE_DATA_SEQUENCE_H
#define BODY_STRUCTURE_DATA_SEQUENCE_H

class BodyStructureData;


class MAILPP_API BodyStructureDataSequence
{
public:

    BodyStructureDataSequence();
    BodyStructureDataSequence(unsigned initialCapacity);
    BodyStructureDataSequence(const BodyStructureDataSequence& other);
    ~BodyStructureDataSequence();

    BodyStructureDataSequence& operator = (
        const BodyStructureDataSequence& other);

    unsigned NumElements() const;
    void InsertElement(unsigned pos, BodyStructureData* element);
    void AppendElement(BodyStructureData* element);
    BodyStructureData* RemoveElement(unsigned index);
    BodyStructureData* ElementAt(unsigned index) const;

    void DeleteAll();

private:

    unsigned mNumElements;
    unsigned mMaxElements;
    BodyStructureData** mElements;

    void Copy(const BodyStructureDataSequence&);
};

#endif
