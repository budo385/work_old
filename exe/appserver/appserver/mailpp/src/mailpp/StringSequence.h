//=============================================================================
// StringSequence.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef STRING_SEQUENCE_H
#define STRING_SEQUENCE_H

class MAILPP_API StringSequence
{
public:

    StringSequence();
    StringSequence(int initialCapacity);
    StringSequence(const StringSequence& other);
    ~StringSequence();

    StringSequence& operator = (const StringSequence& other);

    unsigned NumElements() const;
    void InsertElement(unsigned pos, char* element);
    void AppendElement(char* element);
    char* RemoveElement(unsigned index);
    const char* ElementAt(unsigned index) const;

    void DeleteAll();

private:

    unsigned mNumElements;
    unsigned mMaxElements;
    char** mElements;

    void Copy(const StringSequence&);
};

#endif
