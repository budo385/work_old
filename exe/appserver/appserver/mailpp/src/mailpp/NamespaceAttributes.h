//=============================================================================
// NamespaceAttributes.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef NAMESPACE_ATTRIBUTES_H
#define NAMESPACE_ATTRIBUTES_H

/// Class that contains the attributes of an IMAP4 namespace

class MAILPP_API NamespaceAttributes
{
public:
    /// Default constructor
    NamespaceAttributes();

    /// Constructor that takes an initial value
    NamespaceAttributes(const char* prefix, char delimiter);

    /// Copy constructor
    NamespaceAttributes(const NamespaceAttributes&);

    /// Destructor
    ~NamespaceAttributes();

    /// Assignment operator
    NamespaceAttributes& operator=(const NamespaceAttributes&);

    /// Gets the namespace prefix
    const char* Prefix() const;

    /// Gets the namespace delimiter
    char Delimiter() const;

    /// Called by the parser to set the data for this object
    void ImportData(ListItem* root);

private:
    char* mPrefix;
    char mDelimiter;
};

#endif
