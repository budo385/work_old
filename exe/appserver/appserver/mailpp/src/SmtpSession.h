// SmtpSession.h

#ifndef SMTP_TRANSACTION_H
#define SMTP_TRANSACTION_H

#ifndef WINCE
	#include "mailpp/mailpp.h"
#else
	#include "mailpp.h"
#endif

using mailpp::SmtpClient;

class MailTransaction;


class SmtpSession
{
public:

    SmtpSession(const char* hostname);
    virtual ~SmtpSession();

    int Open(const char* address, int port);
    void Close();
    int DoTransaction(MailTransaction& transaction);

		SmtpClient &GetClient(){ return mClient; }
		void SetTracer(mailpp::TraceOutput* pTracer);
protected:

    SmtpClient mClient;
	char* mHostname;
    mailpp::TraceOutput* mTraceOutput;

    virtual void ReportNetworkError(const char* message);
    virtual void ReportProtocolError(const char* message);
    int SayHello();

};


#endif //SMTP_TRANSACTION_H