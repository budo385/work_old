//=============================================================================
// ListItem.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef LIST_ITEM_H
#define LIST_ITEM_H


class ListItem
{
public:
    ListItem();
    virtual ~ListItem();
    void Reserve(unsigned);
    void AppendChar(int);
    void AddChild(ListItem*);
    void AddSibling(ListItem*);
    ListItem* mParent;
    ListItem* mChild;
    ListItem* mNext;
    int mDepth;
    int mIsNil;
    char* mStr;
    unsigned mStrSize;
    unsigned mStrLen;

    enum {
        TYPE_NIL,
        TYPE_ATOM,
        TYPE_QUOTED,
        TYPE_LITERAL
    };
};

#endif
