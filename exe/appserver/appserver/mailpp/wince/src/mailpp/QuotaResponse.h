//=============================================================================
// QuotaResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef QUOTA_RESPONSE_H
#define QUOTA_RESPONSE_H

/// Class that represents an item in the quota resource list

class MAILPP_API QuotaListItem {
public:
    /// Constructor.
    QuotaListItem(const char* name, uint32_t usage, uint32_t limit);

    /// Copy constructor
    QuotaListItem(const QuotaListItem& other);

    /// Destructor.
    ~QuotaListItem();

    /// Assignment operator
    QuotaListItem& operator=(const QuotaListItem& other);

    /// Gets the resource name
    const char* Name() const { return mName; }

    /// Gets the current resource usage
    uint32_t Usage() const { return mUsage; }

    /// Gets the resource limit
    uint32_t Limit() const { return mLimit; }

private:
    char* mName;
    uint32_t mUsage;
    uint32_t mLimit;
};


/// Class that represents an IMAP4 QUOTA response

class MAILPP_API QuotaResponse : public Response
{
public:

    /// Default constructor
    QuotaResponse();

    /// Copy constructor
    QuotaResponse(const QuotaResponse& other);

    /// Destructor
    virtual ~QuotaResponse();

    /// Assignment operator
    QuotaResponse& operator = (const QuotaResponse& other);

    /// Gets the name of the quota root
    const char* QuotaRootName() const;

    /// Gets the number of quota list items
    unsigned NumItems() const;

    /// Gets the quota list item at the specified position
    const QuotaListItem& ItemAt(unsigned index) const;

    /// Called by the parser to set the data for this object
    void ImportData(ListItem* root);

private:
    char* mQuotaRootName;
    unsigned mNumItems;
    unsigned mMaxItems;
    QuotaListItem** mItems;

    void ImportData_2(ListItem* root);
    void InsertItem(unsigned index, QuotaListItem* item);
    void AppendItem(QuotaListItem* item);
    void DeleteAll();
    void Copy(const QuotaResponse& other);
};

#endif


