// pop3_ex.cpp
//
// Command line arguments
//
// -a address     network address of POP3 server (no default)
// -p port        TCP port of POP3 server (default is 110)
// -d             delete messages after downloading
// -u user        user login name (no default)
// -s password    password (no default)
// -c             use APOP authentication (default is USER/PASS)
//
// Note: Most POP3 servers are configured to expunge messages from the
// mailbox only when they receive the QUIT command (in conformance with RFC
// 1939).  This has important consequences for unreliable connections, such
// as dial-up modems.  What can happen, is that a POP3 client
// implementation will attempt to retrieve all messages, marking them for
// deletion.  But if there are very many messages and an unreliable
// connection, the QUIT command may never be sent, which means the messages
// will not be deleted, which means at the next session they will be
// retrieved yet again.  The solution is to open a connection, retrieve and
// delete a limited number of messages -- say 50 or 100 -- then send the
// QUIT command to establish a checkpoint.  While there are still more
// messages, you establish another connection, retrieve and delete more
// messages, and send QUIT again.  Repeat this procedure until all messages
// have been retrieved and deleted.
//
// Another solution is to use the UIDL command to keep track of which
// messages have been downloaded.  This alternative solution may not always
// be possible, since UIDL is an optional POP3 command that may not be
// supported by all POP3 servers.
//
// To make things simple, this example program does not follow either
// procedure.

#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <mailpp/mailpp.h>

using namespace std;
using namespace mailpp;

#if defined(WIN32) || defined(_WIN32)
#  define snprintf _snprintf
#endif

// command line options as global variables

char* address = 0;
int port = 110;
int del = 0;
char* user = 0;
char* password = 0;
int apop = 0;

//
// MyPop3Listener -- this class receives the lines of text sent by the
// POP3 server and saves them to a file.
//
class MyPop3Listener : public ProtocolListener {
public:
    MyPop3Listener(ostream&);
    virtual ~MyPop3Listener();
    virtual void ClearLines();
    virtual void LineReceived(const char* line);
private:
    ostream& out;
};

MyPop3Listener::MyPop3Listener(ostream& out_)
  : out(out_)
{
}

MyPop3Listener::~MyPop3Listener()
{
}

void MyPop3Listener::ClearLines()
{
}

void MyPop3Listener::LineReceived(const char* line)
{
    // End of line characters are always CR LF.  This may be okay if you
    // want to write directly to a binary mode stream.  However, if you are
    // writing to a text stream, then you probably want to strip the CR and
    // conform to the standard C/C++ newline convention.
    int lineLen = (int) strlen(line) - 1;
    if (lineLen >= 0 && line[lineLen] == '\n') {
        --lineLen;
    }
    if (lineLen >= 0 && line[lineLen] == '\r') {
        --lineLen;
    }
    ++lineLen;
    out.write(line, lineLen);
    out << endl;
}

void GetCommandLineOptions(int argc, char** argv)
{
    //
    // -a address     network address of POP3 server (no default)
    // -p port        TCP port of POP3 server (default is 110)
    // -d             delete messages after downloading
    // -u user        user login name (no default)
    // -s password    password (no default)
    // -c             use APOP authentication (default is USER/PASS)
    //
    int i = 1;
    while (i < argc) {
        if (argv[i][0] != '-') {
            break;
        }
        if (argv[i][1] == 'a') {
            // server address
            ++i;
            if (i < argc) {
                if (address != 0) {
                    delete[] address;
                }
                size_t n = strlen(argv[i]);
                address = new char[n+1];
                strncpy(address, argv[i], n);
                address[n] = 0;
            }
        }
        else if (argv[i][1] == 'p') {
            // server port
            port = atoi(&argv[i][1]);
        }
        else if (argv[i][1] == 'd') {
            // delete messages
            del = 1;
        }
        else if (argv[i][1] == 'u') {
            // user
            ++i;
            if (i < argc) {
                if (user != 0) {
                    delete[] user;
                }
                size_t n = strlen(argv[i]);
                user = new char[n+1];
                strncpy(user, argv[i], n);
                user[n] = 0;
            }
        }
        else if (argv[i][1] == 's') {
            // password
            ++i;
            if (i < argc) {
                if (password != 0) {
                    delete[] password;
                }
                size_t n = strlen(argv[i]);
                password = new char[n+1];
                strncpy(password, argv[i], n);
                password[n] = 0;
            }
        }
        else if (argv[i][1] == 'c') {
            // use APOP
            apop = 1;
        }
        ++i;
    }
}

void DoPop3Commands(Pop3Client& client)
{
    int err, replyCode;
    const char* replyText;
    //
    // If we are using USER/PASS authentication, send the USER and PASS
    // commands
    //
    if (! apop) {
        err = client.User(user);
        // Check for error at network layer
        if (err) {
            cout << "Failed in USER command" << endl;
            cout << "Error code is " << client.ErrorCode() << endl;
            cout << "Error message is \"" << client.ErrorMessage() << "\""
                << endl;
            return;
        }
        // Check for error at application layer
        replyCode = client.ReplyCode();
        replyText = client.ReplyText();
        if (replyCode == Pop3Client::ERR) {
            cout << "Failed in PASS command" << endl;
            cout << "The server's reply was \"" << replyText << "\"" << endl;
            return;
        }
        err = client.Pass(password);
        // Check for error at network layer
        if (err) {
            cout << "Failed in PASS command" << endl;
            cout << "Error code is " << client.ErrorCode() << endl;
            cout << "Error message is \"" << client.ErrorMessage() << "\""
                << endl;
            return;
        }
        // Check for error at application layer
        replyCode = client.ReplyCode();
        replyText = client.ReplyText();
        if (replyCode == Pop3Client::ERR) {
            cout << "Failed in PASS command" << endl;
            cout << "The server's reply was \"" << replyText << "\"" << endl;
            return;
        }
    }
    //
    // If we are using APOP authentication, send the APOP command
    //
    else if (apop) {
        err = client.Apop(user, password);
        // Check for error at network layer
        if (err) {
            cout << "Failed in APOP command" << endl;
            cout << "Error code is " << client.ErrorCode() << endl;
            cout << "Error message is \"" << client.ErrorMessage() << "\""
                << endl;
            return;
        }
        // Check for error at application layer
        replyCode = client.ReplyCode();
        replyText = client.ReplyText();
        if (replyCode == Pop3Client::ERR) {
            cout << "Failed in APOP command" << endl;
            cout << "The server's reply was \"" << replyText << "\"" << endl;
            return;
        }
    }
    //
    // Send a STAT command to get the number of messages
    //
    err = client.Stat();
    // Check for error at network layer
    if (err) {
        cout << "Failed in STAT command" << endl;
        cout << "Error code is " << client.ErrorCode() << endl;
        cout << "Error message is \"" << client.ErrorMessage() << "\"" << endl;
        return;
    }
    // Check for error at application layer
    replyCode = client.ReplyCode();
    replyText = client.ReplyText();
    if (replyCode == Pop3Client::ERR) {
        cout << "Failed in STAT command" << endl;
        cout << "The server's reply was \"" << replyText << "\"" << endl;
        return;
    }
    int numMessages = atoi(replyText);
    //
    // Send a RETR command to get each message and save it to a file
    //
    for (int msgNum = 1; msgNum <= numMessages; ++msgNum) {
        //
        // Create an instance of MyPop3Listener, which will save a single
        // message to a file.
        //
        char filename[200];
        snprintf(filename, sizeof(filename), "msg-%04d.txt", msgNum);
        ofstream out(filename);
        MyPop3Listener listener(out);
        client.SetListener(&listener, false);
        //
        // Send the RETR command
        //
        err = client.Retr(msgNum);
        client.SetListener(0, false);
        out.close();
        // Check for error at network layer
        if (err) {
            cout << "Failed in RETR command" << endl;
            cout << "Error code is " << client.ErrorCode() << endl;
            cout << "Error message is \"" << client.ErrorMessage() << "\""
                << endl;
            return;
        }
        // Check for error at application layer
        replyCode = client.ReplyCode();
        replyText = client.ReplyText();
        if (replyCode == Pop3Client::ERR) {
            cout << "Failed in RETR command" << endl;
            cout << "The server's reply was \"" << replyText << "\"" << endl;
            return;
        }
        //
        // Optionally, delete the message
        //
        if (del) {
            err = client.Dele(msgNum);
            // Check for an error at the network layer
            if (err) {
                cout << "Failed in DELE command" << endl;
                cout << "Error code is " << client.ErrorCode() << endl;
                cout << "Error message is \"" << client.ErrorMessage() << "\""
                    << endl;
                return;
            }
            // Check for error at application layer
            replyCode = client.ReplyCode();
            replyText = client.ReplyText();
            if (replyCode == Pop3Client::ERR) {
                cout << "Failed in DELE command" << endl;
                cout << "The server's reply was \"" << replyText << "\"" << endl;
                return;
            }
        }
    }
    //
    // Send a QUIT command
    //
    err = client.Quit();
    // Check for an error at the network layer
    if (err) {
        cout << "Failed in QUIT command" << endl;
        cout << "Error code is " << client.ErrorCode() << endl;
        cout << "Error message is \"" << client.ErrorMessage() << "\""
            << endl;
        return;
    }
    // Check for error at application layer
    replyCode = client.ReplyCode();
    replyText = client.ReplyText();
    if (replyCode == Pop3Client::ERR) {
        cout << "Failed in QUIT command" << endl;
        cout << "The server's reply was \"" << replyText << "\"" << endl;
        return;
    }
}

int main(int argc, char** argv)
{
    GetCommandLineOptions(argc, argv);
    if (address == 0 || user == 0 || password == 0) {
        cout << "Command line arguments:" << endl;
        cout << endl;
        cout << "-a address     network address of POP3 server (no default)"
             << endl;
        cout << "-p port        TCP port of POP3 server (default is 110)"
             << endl;
        cout << "-d             delete messages after downloading"
             << endl;
        cout << "-u user        user login name (no default)"
             << endl;
        cout << "-s password    password (no default)"
             << endl;
        cout << "-c             use APOP authentication (default is USER/PASS)"
             << endl;
        return 1;
    }
    Pop3Client client;
    StdTraceOutput traceOut;
    client.SetTraceOutput(&traceOut, false);
    int connectErr = client.Connect(address, port);
    if (! connectErr) {
        DoPop3Commands(client);
    }
    else if (connectErr) {
        cout << "Failed to connect to " << address << " at port " << port
            << endl;
        cout << "Error code is " << client.ErrorCode() << endl;
        cout << "Error message is \"" << client.ErrorMessage() << "\"" << endl;
    }
    client.Disconnect();
    if (address != 0) delete[] address;
    if (user != 0) delete[] user;
    if (password != 0) delete[] password;
    return 0;
}
