// smtp_ex.cpp

#include "SmtpSession.h"
#include "MailTransaction.h"


int main(int argc, char** argv)
{
    //
    // Create an SmtpSession object and connect to the SMTP server
    //
    SmtpSession session("myhost.example.org");
    session.Open("smtp.example.net", 25);
    //
    // Create a MailTransaction instance.  The MailTransaction requires a
    // sender, at least one recipient, and the name of a file that contains
    // the message to send.
    //
    MailTransaction trans;
    trans.SetFrom("rolf@beispiel.de");
    trans.AddTo("pierre@exemple.fr");
    trans.SetFilename("msg.txt");
    //
    // Provide the MailTransaction to the SmtpSession to process
    //
    session.DoTransaction(trans);
    //
    // We could process more transactions now.  We won't.  But you can
    // if you want to. :-)  We will just close the session and quit
    // this example program.
    //
    session.Close();

    return 0;
}
