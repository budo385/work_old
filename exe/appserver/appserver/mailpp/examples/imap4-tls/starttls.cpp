// starttls.cpp
//
// This example program is the same as listmessages.cpp, except that it
// begins a secure TLS connection immediately after it completes the TCP
// connection.  It uses the STARTTLS IMAP4 command.  This example program
// works only if the IMAP4 server supports TLS and the STARTTLS command,
// and only if you are using Secure Mail++.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <mailpp/mailpp.h>
using namespace mailpp;

#if defined(WIN32)
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
#  define snprintf _snprintf
#  define strcasecmp _stricmp
#endif

#if defined(__unix)
#  include <unistd.h>
#endif

int openssl_initialize();
void openssl_finalize();
int DoImap4Session(Imap4Client* client,const char* host, const char* user,
    const char* password, const char* folder);
int DoSecureImap4Session(Imap4Client* client,const char* host,const char* user,
    const char* password, const char* folder);
void printSummaryLine(const EnvelopeData& env, const FlagsData& flags);
void ConvertDate(const char* rfc822date, char* date, size_t dateSize);
extern "C" void ERR_print_errors_fp(FILE *);


int main(int argc, char** argv)
{
    // Get commandline arguments

    if (argc < 5) {
        printf("usage:\n");
        printf("    %s host user password folder\n", argv[0]);
        exit(1);
    }
    const char* host = argv[1];
    const char* user = argv[2];
    const char* password = argv[3];
    const char* folder = argv[4];

    // Initialize the OpenSSL library

    openssl_initialize();

	// Set the location of the CA certificate(s)

    mailpp::TlsConfig* config = mailpp::TlsConfig::Instance();
    config->SetOption(mailpp::TlsConfig::OPENSSL_CA_FILE, "cacert.pem");
    //config->SetOption(mailpp::TlsConfig::OPENSSL_CA_DIR, "cacerts");

    // Create an IMAP4 client

    Imap4Client client;

    // Execute the IMAP4 session

    DoImap4Session(&client, host, user, password, folder);

    // Finalize the OpenSSL library

    openssl_finalize();

    return 0;
}


int DoImap4Session(Imap4Client* client,const char* host, const char* user,
    const char* password, const char* folder)
{
    // For this example, set debugging on, so that we can see the protocol
    // messages exchanged.

    client->SetDebugOn(true);

    // Open a connection to the server

    client->Connect(host);

    // Send the STARTTLS command.  Check for a tagged OK response.

    int err = client->Starttls();
    int numResponses = client->NumResponses();
    int i;
    bool ok = false;
    for (i=0; i < numResponses; ++i) {
        if (strcasecmp(client->ResponseAt(i).Name(), "ok") == 0) {
            const OkResponse& resp = (const OkResponse&) client->ResponseAt(i);
            const char* tag = resp.Tag();
            if (strcmp(tag, "*") != 0) {
                ok = true;
                break;
            }
        }
    }

    // If the server sent a tagged OK response, then establish a TLS
    // connection and continue the session.  Otherwise, close the
    // connection.

    if (ok) {
        return DoSecureImap4Session(client, host, user, password, folder);
    }
    else {
        printf("Server will not allow a TLS connection\n");

        // Send LOGOUT command to end this connection

        client->Logout();

        // Call Disconnect to close the socket

        client->Disconnect();

        return -1;
    }
}


int DoSecureImap4Session(Imap4Client* client,const char* host,const char* user,
    const char* password, const char* folder)
{
    // Open a TLS connection.  host is the name of the server that is in
    // the server's certificate.  Usually, it is the same as the hostname
    // used to open the TCP connection, but it's also possible for it to be
    // different.  If the host parameter in ConnectTls does not match the
    // server name in the certificate, then ConnectTls fails (although the
    // TLS connection may still be established).

    int err = client->ConnectTls(host);
    if (err) {
        printf("TLS connection failed\n");
        printf("Mail++ error: %s\n", client->ErrorMessage());
        // Uncomment the following OpenSSL function to get more detail on
        // the error that occurred.
        //ERR_print_errors_fp(stdout);
        return -1;
    }

    // Send LOGIN command

    client->Login(user, password);

    // Send SELECT to select the folder

    client->Select(folder);

    // Find the EXISTS response, which will tell us how many messages are
    // in the folder.

    int numResponses = client->NumResponses();
    int numMessages = 0;
	int i;
    for (i=0; i < numResponses; ++i) {
        if (strcasecmp(client->ResponseAt(i).Name(), "exists") == 0) {
            const ExistsResponse& resp = (const ExistsResponse&)
                client->ResponseAt(i);
            numMessages = resp.Number();
        }
    }

    printf("%d messages in mailbox\n", numMessages);

    // Send a FETCH with the ALL macro (equivalent to (FLAGS INTERNALDATE
    // RFC822.SIZE ENVELOPE))

    client->Fetch(1, numMessages, "ALL");

    // Process each FETCH response -- one for each message in the folder.
    // Extract the information we want to print for our listing.

    numResponses = client->NumResponses();
    for (i=0; i < numResponses; ++i) {

        // Find the FETCH responses

        if (strcasecmp(client->ResponseAt(i).Name(), "fetch") == 0) {
            const FetchResponse& resp = (const FetchResponse&)
                client->ResponseAt(i);

            // Get the ENVELOPE structure

            const EnvelopeData* env = resp.Envelope();

            // Get the FLAGS list

            const FlagsData* flags = resp.Flags();

            // Then extract the desired information and print a summary line

            if (env != 0 && flags != 0) {
                printSummaryLine(*env, *flags);
            }
        }
    }

    // Send LOGOUT command to end this connection

    client->Logout();

    // Call Disconnect to close the socket

    client->Disconnect();

    return 0;
}


#define FLAGS_SIZE     3
#define DATE_SIZE     11
#define FROM_SIZE     26
#define SUBJECT_SIZE  40
#define FORMAT        "%-3s%-11s%-26s%-40s"


void printSummaryLine(const EnvelopeData& envelopeData,
    const FlagsData& flagsData)
{
    char flags[FLAGS_SIZE];
    char date[DATE_SIZE];
    char from[FROM_SIZE];
    char subject[SUBJECT_SIZE];

    // Get the subject

    strncpy(subject, envelopeData.Subject(), sizeof(subject)-1);
    subject[sizeof(subject)-1] = 0;

    // Get the sender.  There could be more than one address, but we will
    // get only the first one.

    from[0] = 0;
    if (envelopeData.NumFrom() > 0) {
        const AddressData& addr = envelopeData.From(0);
        const char* name = addr.PersonalName();
        if (name[0] != 0) {
            strncpy(from, name, sizeof(from)-1);
        }
        else {
            snprintf(from, sizeof(from), "%s@%s", addr.MailboxName(),
                addr.HostName());
        }
        from[sizeof(from)-1] = 0;
    }

    // Get the date

    const char* rfc822date = envelopeData.Date();
    ConvertDate(rfc822date, date, sizeof(date));

    // Get the flags

    flags[0] = 0;
    if (sizeof(flags) >= 3) {
        flags[0] = flags[1] = ' ';
        flags[2] = 0;
        if (flagsData.Flag("\\Seen")) {
            flags[0] = 'S';
        }
        if (flagsData.Flag("\\Answered")) {
            flags[1] = 'A';
        }
    }
    else if (sizeof(flags) == 2) {
        flags[0] = ' ';
        flags[1] = 0;
        if (flagsData.Flag("\\Seen")) {
            flags[0] = 'S';
        }
    }

    printf(FORMAT "\n", flags, date, from, subject);
}


// Convert date from RFC 822 format (e.g., "Thu, 3 May 2001 12:58:40 -0400")
// to the format we want for printing (e.g., "2001-05-03")

void ConvertDate(const char* rfc822date, char* date, size_t dateSize)
{
    int pos = 0;
    while (rfc822date[pos] && (rfc822date[pos] < 48 || rfc822date[pos] > 57)) {
        ++pos;
    }
    int day = 0;
    while (rfc822date[pos] && 48 <= rfc822date[pos] && rfc822date[pos] <= 57) {
        day *= 10;
        day += rfc822date[pos] - 48;
        ++pos;
    }
    while (rfc822date[pos] && (rfc822date[pos] < 65 || rfc822date[pos] > 90)) {
        ++pos;
    }
    char monthStr[4];
    int mpos = 0;
    while (rfc822date[pos] && mpos < 3
        && ((65 <= rfc822date[pos] && rfc822date[pos] <= 90)
        || (97 <= rfc822date[pos] && rfc822date[pos] <= 122))) {

        int ch = rfc822date[pos];
        if (ch < 97) {
            ch += 32;
        }
        monthStr[mpos++] = ch;
        ++pos;
    }
    monthStr[mpos] = 0;
    int month = 0;
    if      (strcmp(monthStr, "jan") == 0) month = 1;
    else if (strcmp(monthStr, "feb") == 0) month = 2;
    else if (strcmp(monthStr, "mar") == 0) month = 3;
    else if (strcmp(monthStr, "apr") == 0) month = 4;
    else if (strcmp(monthStr, "may") == 0) month = 5;
    else if (strcmp(monthStr, "jun") == 0) month = 6;
    else if (strcmp(monthStr, "jul") == 0) month = 7;
    else if (strcmp(monthStr, "aug") == 0) month = 8;
    else if (strcmp(monthStr, "sep") == 0) month = 9;
    else if (strcmp(monthStr, "oct") == 0) month = 10;
    else if (strcmp(monthStr, "sep") == 0) month = 11;
    else if (strcmp(monthStr, "nov") == 0) month = 12;

    while (rfc822date[pos] && (rfc822date[pos] < 48 || rfc822date[pos] > 57)) {
        ++pos;
    }
    int year = 0;
    while (rfc822date[pos] && 48 <= rfc822date[pos] && rfc822date[pos] <= 57) {
        year *= 10;
        year += rfc822date[pos] - 48;
        ++pos;
    }
    snprintf(date, dateSize, "%04d-%02d-%02d", year, month, day);
}
