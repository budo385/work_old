// list_ex.cpp
//
// This example program shows how to list all the newsgroups available at
// a news server.  The list of newsgroups is written to an output file.
//
// The single command line parameter required is the name or address of the
// server.  The name of the output file is coded directly.
//

#include <mailpp/mailpp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;
using namespace mailpp;
#include "NntpListener.h"


int main(int argc, char** argv)
{
    int retVal = 1;

    // A single command line argument is required, which is the name or
    // address of the news server

    if (argc < 2) {
        cout << "Usage: " << argv[0] << " server" << endl;
        exit(EXIT_FAILURE);
    }
    const char* server = argv[1];

    // Open a file for output.  The list of newsgroups is written to this
    // file.

    const char* filename = "newsgroup-list.txt";
    ofstream out(filename);

    // Create an NNTP client object

    NntpClient nntp;

    // Open a connection to the news server. Check for a 2xx reply code,
    // which indicates success.

    int err = nntp.Connect(server);
    if (! err && (nntp.ReplyCode()/100%10) == 2) {

        // Send a MODE READER command

        err = nntp.ModeReader();

        // Create a listener object to receive the lines of the article
        // and write them to the file

        NntpListener listener(out);
        nntp.SetListener(&listener, false);

        // Send a LIST command.  While this command executes, our
        // listener receives each line and writes it to the file.

        err = nntp.List();
        if (! err && (nntp.ReplyCode()/100%10) == 2) {
            retVal = 0;
        }
        else if (nntp.ReplyCode()/100%10 != 2) {
            cout << "Failure in LIST command" << endl;
            cout << "The server's response was " << nntp.ReplyCode()
                 << " " << nntp.ReplyText() << endl;
        }
    }

    // Send a QUIT command, unless there was a problem with the client/
    // server communication

    if (! err) {
        nntp.Quit();
    }

    // Show any error message

    if (err) {
        cout << "Error: (" << nntp.ErrorCode() << ") " << nntp.ErrorMessage()
             << endl;
    }

    // Close the connection

    nntp.Disconnect();

    return retVal;
}
