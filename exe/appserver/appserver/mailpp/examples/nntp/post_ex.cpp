// post_ex.cpp
//
// This example program shows how to post an article to a newsgroup.
//
// Two command line parameters are required: the name or address of the NNTP
// server and the name of a file that contains the article to post.
//

#include <mailpp/mailpp.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
using namespace std;
using namespace mailpp;


int main(int argc, char** argv)
{
    int retVal = 1;

    // Two command line arguments are required:
    //   * the address of the news server
    //   * the name of a file containing the article to post

    if (argc < 3) {
        cout << "Usage: " << argv[0] << " server filename" << endl;
        exit(EXIT_FAILURE);
    }
    const char* server = argv[1];
    const char* filename = argv[2];

    // Open the file for input.  We will open it as a text file, which means
    // the end-of-line characters will be just LF alone.

    ifstream in(filename);
    if (! in) {
        cout << "Can't open file " << filename << endl;
        exit(EXIT_FAILURE);
    }

    // Create an NNTP client object

    NntpClient nntp;

    // Open a connection to the news server. Check for a 2xx reply code,
    // which indicates success.

    int err = nntp.Connect(server);
    if (! err && (nntp.ReplyCode()/100%10) == 2) {

        // Send a MODE READER command.  This command may be required by
        // some servers before it will accept the POST command.

        err = nntp.ModeReader();

        // Send a POST command.  Check for 3xx response, which means the
        // server is ready to accept the article.

        err = nntp.Post();
        if (! err && nntp.ReplyCode()/100%10 == 3) {

            // Send the article content.  When submitting the first buffer,
            // we set the NntpClient::FIRST option.  When submitting the
            // last buffer, we set the NntpClient::LAST option.  For all
            // buffers, we set the NntpClient::CONVERT_EOL option, so that
            // the end-of-line characters will be converted to CR LF.
            // After the last buffer is send (indicated by the
            // NntpClient::LAST option), the library code will receive the
            // server's response.

            int bufSize = 1200;
            char* buf = (char*) malloc(bufSize);
            int count = 0;
            do {
                ++count;
                in.read(buf, bufSize);
                int bufLen = (int) in.gcount();
                int options = NntpClient::CONVERT_EOL;
                if (count == 1) {
                    options |= NntpClient::FIRST;
                }
                if (bufLen < bufSize) {
                    options |= NntpClient::LAST;
                }
                err = nntp.SendLines(buf, bufLen, options);
            } while (! err && ! in.eof());
            free(buf);

            // Check the reply code.  2xx indicates success.

            if (! err && nntp.ReplyCode()/100%10 == 2) {
                retVal = 0;
            }
            else if (nntp.ReplyCode()/100%10 != 2) {
                cout << "Failure in posting the article" << endl;
                cout << "The server's response was " << nntp.ReplyCode()
                     << " " << nntp.ReplyText() << endl;
            }
        }
        else if (nntp.ReplyCode()/100%10 != 3) {
            cout << "Failure in posting the article" << endl;
            cout << "The server's response was " << nntp.ReplyCode()
                 << " " << nntp.ReplyText() << endl;
        }
    }

    // Send a QUIT command, unless there was a problem with the client/
    // server communication

    if (! err) {
        nntp.Quit();
    }

    // Show any error message

    if (err) {
        cout << "Error: (" << nntp.ErrorCode() << ") " << nntp.ErrorMessage()
             << endl;
    }

    // Close the connection

    nntp.Disconnect();

    return retVal;
}
