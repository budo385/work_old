
#ifndef NNTP_LISTENER_H
#define NNTP_LISTENER_H

//
// NntpListener -- this class receives the lines of text sent by the
// NNTP server and saves them to a C++ stream.
//
class NntpListener : public ProtocolListener {
public:
    NntpListener(ostream&);
    virtual ~NntpListener();
    virtual void ClearLines();
    virtual void LineReceived(const char* line);
private:
    ostream& out;
};

#endif
