#include "backuptaskexecutor.h"

BackupTaskExecutor::BackupTaskExecutor(ServerBackupManager *pTask,QObject *parent)
	:BackgroundTaskExecutor(pTask,parent,0,false,false,false)
{
	m_Backup=pTask;
	//connect(this,SIGNAL(RestartFromAnotherThreadSignal(bool)),this,SLOT(RestartFromAnotherThreadSlot(bool)));
}


void BackupTaskExecutor::Start()
{
	Status err;
	int nNewTimeMsec=m_Backup->GetNextBackupStart(err);
	if (!err.IsOK())
	{
		nNewTimeMsec=10*60000; //try to start after 10min;
	}

	if (nNewTimeMsec>0)
		BackgroundTaskExecutor::Start(false,nNewTimeMsec);
}

//restart if done
void BackupTaskExecutor::Finished()
{
	BackgroundTaskExecutor::Finished();
	Start();
}
/*
void BackupTaskExecutor::RestartFromAnotherThreadSlot(bool bStartNow)
{
	if (bStartNow)
		BackgroundTaskExecutor::StartNow();
	else
		Start();
}


void BackupTaskExecutor::RestartTaskSchedule()
{
	Stop();
	emit RestartFromAnotherThreadSignal(false);
}
void BackupTaskExecutor::StartNow(int nCallerID,int nTaskID)
{
	emit RestartFromAnotherThreadSignal(true);
}
*/

//thread safe
void BackupTaskExecutor::OnBcpManager_BackupCommandIssued(int nCallerID)
{
	BackgroundTaskExecutor::StartNow(nCallerID);
	//if (m_SrvBackupBkgTask==NULL)
	//	return;
	//m_SrvBackupBkgTask->StartNow();

}


//thread safe
void BackupTaskExecutor::OnBcpManager_SettingsChanged()
{
	//if (m_SrvBackupBkgTask==NULL)
	//	return;
	//m_SrvBackupBkgTask->RestartTaskSchedule();
	Start();
}