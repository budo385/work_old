#ifndef MESSAGE_NOTIFICATOR_H
#define MESSAGE_NOTIFICATOR_H

#include "common/common/backgroundtaskinterface.h"
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include <QReadWriteLock>
#include "trans/trans/pushsender.h"


/* PUSH messagas */
class MessageNotificator : public QObject, public BackgroundTaskInterface
{
	Q_OBJECT

public:
	MessageNotificator();
	~MessageNotificator();

	void	ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");					
	
	void	SendErrorMessage(int nPersonID, QString strErrorText, int nErrorCode=1, QString strEmailAccount="");
	void	SendNewEmailNotification(int nPersonID, QString strEmailAccount,DbRecordSet &lstNewEmails);
	int		SchedulePushMessageForSend(Status &pStatus, QString strMessage, QString strPushToken, int nPersonID, int nOSType, QString strPushSoundName="default", int nBadgeNumber=0);
	void	CleanUp(Status &pStatus);
	
private:
	
	PushSender*		CreatePusherObject(int nMobileOperatingSystem);
	void			LoadEmailInPersonSettings(Status &pStatus, int nPersonID, DbRecordSet &lstPersSettingsData);
	void			LoadPersonPushTokens(Status &pStatus, int nPersonID, DbRecordSet &lstPersPushTokens);

	int				GetEmailInPersonSettings_Int(DbRecordSet &lstPersSettingsData, int nSettingsID);
	QString			GetEmailInPersonSettings_String(DbRecordSet &lstPersSettingsData, int nSettingsID);
	QStringList		GetEmailInPersonSettings_List(DbRecordSet &lstPersSettingsData, int nSettingsID);
	void			GetUnreadEmailsCount(Status &Ret_pStatus, int nPersonID, int &Ret_nCount);
	QMutex			m_Mutex;
	void			SaveLastErrorMsgDateTime(Status &pStatus,QString strDateErr, int nPersonID);
	QString			ExtractEmail(QString strEmail);
};

#endif // MESSAGE_NOTIFICATOR_H
