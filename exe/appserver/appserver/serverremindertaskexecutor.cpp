#include "serverremindertaskexecutor.h"

ServerReminderTaskExecutor::ServerReminderTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent)
:BackgroundTaskExecutor(pTask,parent,0,true,false,false)
{
}

//thread safe
void ServerReminderTaskExecutor::OnGlobalPeriodChanged(int nPeriodSec)
{
	SetTimeInterval(nPeriodSec*1000);
	Stop();
	Start();
}

