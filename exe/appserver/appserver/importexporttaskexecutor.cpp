#include "importexporttaskexecutor.h"


ImportExportTaskExecutor::ImportExportTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent)
:BackgroundTaskExecutor(pTask,parent,0,true,false,false)
{
}


//thread safe
void ImportExportTaskExecutor::OnImportExportManager_GlobalPeriodChanged(int nPeriodMin)
{
	SetTimeInterval(nPeriodMin*60000);
	Stop();
	Start();
}

//thread safe
void ImportExportTaskExecutor::OnImportExportManager_StartTask(int nCallerID,int nTaskID)
{
	StartNow(nCallerID,nTaskID,"");
}