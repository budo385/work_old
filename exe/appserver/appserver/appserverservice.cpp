#include "appserverservice.h"
#include "common/common/logger.h"
#include <QFile>
extern Logger g_Logger;

#include "appserver.h"
ServerControlAbstract *g_AppServer=NULL;


//note on systems with more then 1 app service use strApplicationServiceName
AppServerService::AppServerService(int argc, char **argv, QString strApplicationServiceName)    
	: QtService<QCoreApplication>(argc, argv, strApplicationServiceName)
{
	//descriptor is app name
	if (argc>0)
	{
		m_strDescriptor=argv[0];
		QFileInfo info(m_strDescriptor);
		m_strDescriptor=info.baseName();
	}
	
    setServiceDescription("SOKRATES Application Server Service");
	setServiceFlags(QtServiceBase::CanBeSuspended/*Default*/);

}

AppServerService::~AppServerService()
{
	//destroyed by qApp()->child of his

	//if (g_AppServer!=NULL)
	//{
	//	delete g_AppServer;
	//}
}

void AppServerService::start()
{
	//1st time start:
	if (g_AppServer==NULL)
		g_AppServer=new ApplicationServer(application(),m_strDescriptor);

	Status err;
	g_AppServer->Start(err);
	if(!err.IsOK())
	{
		QCoreApplication *app = application();
		 logMessage(QString("Failed to start application server"), QtServiceBase::Error);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
		//try to write error into log:
		g_AppServer->GetIniFile()->m_strLastErrorText=err.getErrorText();
		g_AppServer->GetIniFile()->m_nLastErrorCode=StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_FAILED_TO_START;
		g_AppServer->GetIniFile()->Save();

		g_AppServer->Stop();
		app->quit();
	}
	else
	{
		//try to write error into log:
		g_AppServer->GetIniFile()->m_strLastErrorText="";
		g_AppServer->GetIniFile()->m_nLastErrorCode=StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTED_OK;
		g_AppServer->GetIniFile()->Save();
	}
	//else
	//	g_AppServer->RestartDelayed(50);		//test
}

void AppServerService::stop()
{
	if (g_AppServer!=NULL)
		if(g_AppServer->IsRunning())
			g_AppServer->Stop();

	g_AppServer->GetIniFile()->m_strLastErrorText="";
	g_AppServer->GetIniFile()->m_nLastErrorCode=0;
	g_AppServer->GetIniFile()->Save();


	QCoreApplication *app = application();
	app->quit();

	//m_pSrvThread->quit();
}

//if pause, just stop listener, no new clients will be accepted, but connected ones will work!
void AppServerService::pause()
{
	if (g_AppServer!=NULL)
		g_AppServer->Stop();
}

//if resume, enable listener
void AppServerService::resume()
{
	if (g_AppServer!=NULL)
	{
		Status err;
		g_AppServer->Start(err);
		if(!err.IsOK())
		{
			QCoreApplication *app = application();
			logMessage(QString("Failed to start application server"), QtServiceBase::Error);
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
			g_AppServer->Stop();
			app->quit();
		}
	}
}
