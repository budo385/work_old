#ifndef BACKUPTASKEXECUTOR_H
#define BACKUPTASKEXECUTOR_H

#include <QObject>
#include "common/common/backgroundtaskexecutor.h"
#include "serverbackupmanager.h"


//restart each time its finished

class BackupTaskExecutor : public BackgroundTaskExecutor
{
	Q_OBJECT

public:
	BackupTaskExecutor(ServerBackupManager *pTask,QObject *parent=NULL);

	void Start();
	void StartNow(int nCallerID=-1,int nTaskID=-1);
	//void RestartTaskSchedule();

public slots:
	void OnBcpManager_SettingsChanged();
	void OnBcpManager_BackupCommandIssued(int nCallerID=-1);

//signals:
	//void RestartFromAnotherThreadSignal(bool);

protected slots:
	virtual void Finished();
	//void RestartFromAnotherThreadSlot(bool);

private:
	ServerBackupManager *m_Backup;
	
};

#endif // BACKUPTASKEXECUTOR_H
