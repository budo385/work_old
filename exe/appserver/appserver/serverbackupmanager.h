#ifndef SERVERBACKUPMANAGER_H
#define SERVERBACKUPMANAGER_H

#include "common/common/backgroundtaskinterface.h"
#include "bus_core/bus_core/backupmanager.h"

/*!
	\class ServerBackupManager
	\brief Periodically backup database
	\ingroup AppServer

	WARNING: after DB and SystemSErvice set start!

*/

class ServerBackupManager : public BackgroundTaskInterface, public BackupManager
{

public:
	void	ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");									//separate thread-> execute backup
	void	Backup(Status &pStatus);				
	void	Restore(Status &pStatus,QString strFileNameForRestore);

private:
	void ClearDatabaseAfterRestore(Status &pStatus);
	void StartDatabase();
	void CloseDatabase();
	
};

#endif // SERVERBACKUPMANAGER_H
