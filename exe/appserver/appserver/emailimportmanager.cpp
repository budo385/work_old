#include "emailimportmanager.h"
#include "common/common/sleeper.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include <QWriteLocker>
#include "common/common/logger.h"
#include "common/common/threadid.h"

#include "bus_server/bus_server/businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;

#define  MAX_THREADS 100	
#define	 MIN_ACC_PER_THREAD 20
#define  MAX_ACC_PER_THREAD 100

EmailImportManager::EmailImportManager()
	:m_nActiveThreads(0)
{
}

EmailImportManager::~EmailImportManager()
{
}

//-------------------------------------------------------
// as usual...qDebug()<<"Single email started: "<<ThreadIdentificator::GetCurrentThreadID();
//-------------------------------------------------------
void EmailImportManager::ExecuteTask( int nCallerID/*=-1*/,int nTaskID/*=-1*/,QString strData/*=""*/ )
{

#ifdef _DEBUG
	//return;
#endif

	//load all
	DbRecordSet lstEmailAccounts;
	LoadAllEmailAccounts(lstEmailAccounts);
	int nSize=lstEmailAccounts.getRowCount();

	int nActiveThreads=GetActiveThreads();

	//activate only those that are not in progress:
	lstEmailAccounts.clearSelection();
	m_Lck.lockForWrite();

	//filter only accounts that are not being processed:
	int nSizeProcessingEmailAcc=m_lstEmailAccsInProcess.size();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::ExecuteTask, trying to start new job, threads currently in use: %1, account processing: %2").arg(nActiveThreads).arg(nSizeProcessingEmailAcc));
	int nNewEmailsToProcess=0;
	for (int i=0;i<nSize;i++)
	{
		int nPersonID=lstEmailAccounts.getDataRef(i,"BOUS_PERSON_ID").toInt();
		QString strEmail=lstEmailAccounts.getDataRef(i,"POP3_EMAIL").toString();

		QStringList lstEmails=m_lstEmailAccsInProcess.value(nPersonID);
		if (lstEmails.indexOf(strEmail)>=0)
			lstEmailAccounts.selectRow(i); //remove if in process
		else
		{
			nNewEmailsToProcess++;
			lstEmails.append(strEmail);
			m_lstEmailAccsInProcess[nPersonID]=lstEmails; //add if not exists
		}
	}
	lstEmailAccounts.deleteSelectedRows();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::ExecuteTask, trying to start new job, email accounts %1 with %2 address is about to be processed").arg(lstEmailAccounts.getRowCount()).arg(nNewEmailsToProcess));
	m_Lck.unlock();

	if (lstEmailAccounts.getRowCount()==0)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::ExecuteTask,nothing to process, exiting"));
		return;
	}

	//now divide to chunks
	int i=0;
	nSize=lstEmailAccounts.getRowCount();
	while(i<nSize)
	{
		DbRecordSet lstChunkEmailAcc;
		GetChunkOfEmailAccountsForThread(i,lstChunkEmailAcc, lstEmailAccounts);

		int nActiveThreads=GetActiveThreads();
		if (nActiveThreads<MAX_THREADS) //go ahead, spawn new thread
		{
			//lstChunkEmailAcc.Dump();
			EmailImportManagerThread *pThread = new EmailImportManagerThread();
			pThread->Start(this,lstChunkEmailAcc);
			//i+=lstChunkEmailAcc.getRowCount();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::ExecuteTask, spawning total threads: %1").arg(nActiveThreads));
		}
		else
		{
			ThreadSleeper::Sleep(100); //if we reach maximum, go to sleep 100ms
		}
	}

	//garbage collector, start to test if there are inactive threads: kill em:
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::ExecuteTask, test inactive threads (activity older then 10min)"));

	//test if there are some old ones:
	QList<EmailImportManagerThread*> lstImportThreadsForDelete;
	m_Lck.lockForWrite();
	i=0;
	while (i<m_lstImportThreads.size())
	{
		if (m_lstImportThreads.at(i)->GetLastActivity().addSecs(15*60)<QDateTime::currentDateTime()) //15 minutes
		{
			QString strEmailsProcessing;
			QDateTime datLastActive;
			m_lstImportThreads.at(i)->GetThreadInfo(datLastActive,strEmailsProcessing);

			lstImportThreadsForDelete.append(m_lstImportThreads.at(i));
			m_lstImportThreads.removeAt(i);
			m_nActiveThreads--;

			QList<int> lstIDToDelete;

			//accounts in process: find in them and delete it from list:
			QHashIterator <int,QStringList> x(m_lstEmailAccsInProcess);
			while (x.hasNext()) 
			{
				x.next();
				QStringList lstEmails = x.value();
				QStringList lstEmailsTerminated= strEmailsProcessing.split(" ",QString::SkipEmptyParts);
				
				int nSizeProcessingEmailAcc=lstEmailsTerminated.size();
				for (int k=0;k<nSizeProcessingEmailAcc;k++)
				{
					int nPos=lstEmails.indexOf(lstEmailsTerminated.at(k));
					if (nPos>=0)
					{
						lstEmails.removeAt(nPos);
					}
				}

				if (lstEmails.size()==0)
					lstIDToDelete.append(x.key());
				else
					m_lstEmailAccsInProcess[x.key()]=lstEmails;
			}

			for (int k=0;k<lstIDToDelete.size();k++)
			{
				m_lstEmailAccsInProcess.remove(lstIDToDelete.at(k));
			}

			continue;
		}
		i++;
	}
	m_Lck.unlock();

	//now delete if any:
	nSize=lstImportThreadsForDelete.size();
	for(int i=0;i<nSize;i++)
	{
		QDateTime datLastActive;
		QString strEmails;
		lstImportThreadsForDelete.at(i)->GetThreadInfo(datLastActive,strEmails);
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::ExecuteTask, thread detected inactive, last activity: %1, emails that thread was processing: %2").arg(datLastActive.toString("dd.MM.yyyy-hh.mm.ss")).arg(strEmails));
		lstImportThreadsForDelete.at(i)->TerminateAtOnce();
	}

}

void EmailImportManager::EmailImportThreadStarted(EmailImportManagerThread *pThread)
{
	QWriteLocker lock(&m_Lck);
	m_nActiveThreads++;
	m_lstImportThreads.append(pThread);
}

void EmailImportManager::EmailImportThreadStopped(EmailImportManagerThread *pThread)
{
	QWriteLocker lock(&m_Lck);
	m_nActiveThreads--;
	int nIdx =m_lstImportThreads.indexOf(pThread);
	if (nIdx>=0)
		m_lstImportThreads.removeAt(nIdx);
}

int EmailImportManager::GetActiveThreads()
{
	QReadLocker lock(&m_Lck);
	return m_nActiveThreads;
}

//warning: all accounts from 1 person must be all together
//B.T. 2014.02.25: ignore main list, just save this piece of junk to DB->rearrange by person of cors!
void EmailImportManager::SaveLastRefreshState(DbRecordSet &lstEmailAccounts)
{
	//load blob, find all with status = ok.> save 'em in blob...
	int nSize=lstEmailAccounts.getRowCount();
	int nPersonID=-1;
	DbRecordSet lstPersonAccData;
	for(int i=0; i<nSize; i++)
	{
		//for each person gather accounts:
		if(nPersonID!=lstEmailAccounts.getDataRef(i,"BOUS_PERSON_ID").toInt())
		{
			if (lstPersonAccData.getRowCount()>0)
			{
				SavePersonEmailAccData(nPersonID,lstPersonAccData);
			}

			//start with new:
			lstPersonAccData=lstEmailAccounts.getRow(i);
			lstPersonAccData.removeColumn(lstPersonAccData.getColumnIdx("BOUS_PERSON_ID"));
			lstPersonAccData.removeColumn(lstPersonAccData.getColumnIdx("BOUS_VALUE_BYTE"));
			nPersonID=lstEmailAccounts.getDataRef(i,"BOUS_PERSON_ID").toInt();
		}
		else
			lstPersonAccData.merge(lstEmailAccounts.getRow(i));
	}

	//save last one:
	if (nPersonID>0 && lstPersonAccData.getRowCount()>0)
	{
		SavePersonEmailAccData(nPersonID,lstPersonAccData);
	}
}



//first load from DB then save only necessary fields and save it back...
void EmailImportManager::SavePersonEmailAccData(int nPersonID, DbRecordSet &lstPersonAccData)
{
	//load first:
	DbRecordSet lstNewData;
	DbRecordSet lstDataForSave;
	lstDataForSave.copyDefinition(lstPersonAccData);
	LoadPersonEmailAccData(nPersonID,lstNewData);

	//lstNewData.Dump();
	//lstPersonAccData.Dump();

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::SavePersonEmailAccData, saving email acc data for person: %1").arg(nPersonID));
	
	//find by UID:
	if (lstNewData.getColumnIdx("POP3_ACC_UID")>=0)
	{
		int nSize2=lstPersonAccData.getRowCount();
		for (int k=0;k<nSize2;k++)
		{
			QString strUID=lstPersonAccData.getDataRef(k,"POP3_ACC_UID").toString();
			QString strEmailAcc=lstPersonAccData.getDataRef(k,"POP3_EMAIL").toString();

			int nRow=lstNewData.find("POP3_ACC_UID",strUID,true);
			if (nRow>=0)
			{
				lstDataForSave.merge(lstNewData.getRow(nRow));
				int nLastRow=lstDataForSave.getRowCount()-1;
				//only update fields that were modified by the thread
				lstDataForSave.setData(nLastRow,"POP3_LAST_MESSAGE",lstPersonAccData.getDataRef(k,"POP3_LAST_MESSAGE").toDateTime());
				lstDataForSave.setData(nLastRow,"POP3_LAST_CHECK",lstPersonAccData.getDataRef(k,"POP3_LAST_CHECK").toDateTime());
				lstDataForSave.setData(nLastRow,"POP3_FULL_SCAN_MODE",lstPersonAccData.getDataRef(k,"POP3_FULL_SCAN_MODE").toInt());

				if(lstDataForSave.getColumnIdx("ERROR_EMAIL_UID") < 0)
					lstDataForSave.addColumn(QVariant::String, "ERROR_EMAIL_UID");
				if(lstPersonAccData.getColumnIdx("ERROR_EMAIL_UID") < 0)
					lstPersonAccData.addColumn(QVariant::String, "ERROR_EMAIL_UID");
				lstDataForSave.setData(nLastRow,"ERROR_EMAIL_UID",lstPersonAccData.getDataRef(k,"ERROR_EMAIL_UID").toString());
				if(lstDataForSave.getColumnIdx("ACC_ERROR_MESSAGES") < 0)
					lstDataForSave.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");
				if(lstPersonAccData.getColumnIdx("ACC_ERROR_MESSAGES") < 0)
					lstPersonAccData.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");
				lstDataForSave.setData(nLastRow,"ACC_ERROR_MESSAGES",lstPersonAccData.getDataRef(k,"ACC_ERROR_MESSAGES").toString());
			}
			else
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::SavePersonEmailAccData, not succeed to find email acc data for person: %1, uid: %2, email: %3").arg(nPersonID).arg(strUID).arg(strEmailAcc));
			}
		}
	}
	else //run over to update new field data:
	{
		lstDataForSave=lstPersonAccData;
	}

	//save it baby:
	if (lstDataForSave.getRowCount()>0)
	{
		Status status;
		DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());
		if(!status.IsOK()) return;

		QByteArray binPersonSettings = XmlUtil::ConvertRecordSet2ByteArray_Fast(lstDataForSave);
		QString strSQL = QString("UPDATE BUS_OPT_SETTINGS SET BOUS_VALUE_BYTE=? WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID=%3").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS).arg(nPersonID);
		query.Prepare(status, strSQL);
		if (!status.IsOK()) return;
		query.bindValue(0, binPersonSettings);
		query.ExecutePrepared(status);
		if (!status.IsOK()) return;

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager::SavePersonEmailAccData, saved email acc data (rows=%1) for person: %2").arg(lstPersonAccData.getRowCount()).arg(nPersonID));
	}

	m_Lck.lockForWrite();//remove from processing list
	m_lstEmailAccsInProcess.remove(nPersonID);
	m_Lck.unlock();

}

void EmailImportManager::LoadPersonEmailAccData(int nPersonID, DbRecordSet &lstPersonAccData)
{
	Status status;
	DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());
	if(!status.IsOK()) return;
	
	QString strSQL = QString("SELECT BOUS_VALUE_BYTE FROM BUS_OPT_SETTINGS WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID =%3").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS).arg(nPersonID);
	query.Execute(status, strSQL);
	if (!status.IsOK()) return;

	if (query.next())
	{
		QByteArray binPersonSettings;
		binPersonSettings=query.value(0).toByteArray();
		lstPersonAccData = XmlUtil::ConvertByteArray2RecordSet_Fast(binPersonSettings);
	}

}


//load all email accounts
//warning: all accounts from 1 person must be all together
void EmailImportManager::LoadAllEmailAccounts(DbRecordSet &lstEmailAccounts)
{
	//fetch all data for all "email in" user settings
	Status status;
	DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());
	if(!status.IsOK()) return;
	QString strSQL = QString("SELECT BOUS_PERSON_ID, BOUS_VALUE_BYTE FROM BUS_OPT_SETTINGS WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND NOT (BOUS_PERSON_ID IS NULL)").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS);
	query.Execute(status,strSQL);
	if(!status.IsOK()) return;
	DbRecordSet data;
	query.FetchData(data);

	int nPersonID=-1;
	bool bSaveListBack=false;

	//for each person
	int nPersons = data.getRowCount();
	for(int x=0; x<nPersons; x++)
	{
		nPersonID = data.getDataRef(x, 0).toInt();
		QByteArray binPersonSettings;
		data.getData(x, 1, binPersonSettings);
		if (binPersonSettings.size()>0)
		{
			DbRecordSet dataPerson = XmlUtil::ConvertByteArray2RecordSet_Fast(binPersonSettings);
			if (dataPerson.getRowCount()>0)
			{
				if (dataPerson.getColumnIdx("POP3_ACCOUNT_ACTIVE")<0)
				{
					dataPerson.addColumn(QVariant::Int,"POP3_ACCOUNT_ACTIVE");
					dataPerson.setColValue("POP3_ACCOUNT_ACTIVE",1);
				}
				dataPerson.addColumn(QVariant::Int,"BOUS_PERSON_ID");
				dataPerson.addColumn(QVariant::ByteArray,"BOUS_VALUE_BYTE");
				dataPerson.setColValue("BOUS_PERSON_ID",nPersonID);
				dataPerson.setColValue("BOUS_VALUE_BYTE",binPersonSettings);
				

				//dataPerson.Dump();
				if (lstEmailAccounts.getColumnCount()==0)
					lstEmailAccounts.copyDefinition(dataPerson);

				//B.T.:check for duplicate coz of old bug:
				dataPerson.sort("POP3_EMAIL");
				int nSize=dataPerson.getRowCount();
				if (nSize>0)
				{
					QString strEmail;
					dataPerson.clearSelection();
					for (int i=0;i<nSize;i++)
					{
						if (strEmail!=dataPerson.getDataRef(i,"POP3_EMAIL").toString())
							strEmail=dataPerson.getDataRef(i,"POP3_EMAIL").toString();
						else
							dataPerson.selectRow(i);
					}

					//lstEmailAccounts.Dump();
					if (dataPerson.getSelectedCount()>0)
					{
						int nDuplicates=dataPerson.getSelectedCount();
						dataPerson.deleteSelectedRows();
						//lstEmailAccounts.Dump();

						//FIX:
						ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager found and deleted %1 duplicate email accounts for user %2").arg(nDuplicates).arg(nPersonID));

						bSaveListBack=true;
					}
				}

				lstEmailAccounts.merge(dataPerson);
			}
		}
	}	

	if (bSaveListBack)
	{
		//save it back:
		SaveLastRefreshState(lstEmailAccounts);
	}

	//check if account is inactive, if so, then remove from list:
	int nSize=lstEmailAccounts.getRowCount();
	lstEmailAccounts.clearSelection();
	for(int i=0; i<nSize; i++)
	{
		if (lstEmailAccounts.getColumnIdx("POP3_ACCOUNT_ACTIVE")>=0)
		{
			if (lstEmailAccounts.getDataRef(i, "POP3_ACCOUNT_ACTIVE").toInt() == 0)
				lstEmailAccounts.selectRow(i);
		}
		else
		{
			qDebug()<<"Something is wrong";
			lstEmailAccounts.Dump();
		}
	}
	lstEmailAccounts.deleteSelectedRows();



	/*

	//B.T.:check for duplicate coz of old bug:
	lstEmailAccounts.sort("POP3_EMAIL");
	int nSize=lstEmailAccounts.getRowCount();
	if (nSize>0)
	{
		QString strEmail;
		lstEmailAccounts.clearSelection();
		for (int i=0;i<nSize;i++)
		{
			if (strEmail!=lstEmailAccounts.getDataRef(i,"POP3_EMAIL").toString())
				strEmail=lstEmailAccounts.getDataRef(i,"POP3_EMAIL").toString();
			else
				lstEmailAccounts.selectRow(i);
		}

		//lstEmailAccounts.Dump();
		if (lstEmailAccounts.getSelectedCount()>0)
		{
			int nDuplicates=lstEmailAccounts.getSelectedCount();
			lstEmailAccounts.deleteSelectedRows();
			//lstEmailAccounts.Dump();

			//FIX:
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("EmailImportManager found and deleted %1 duplicate email accounts for user %2").arg(nDuplicates).arg(nPersonID));

			//save it back:
			SaveLastRefreshState(lstEmailAccounts);
		}
	}*/


}

//nPos - current position: [0-nSize] of m_lstEmailAccounts
//->Chunkerize by email accounts: keep 100 threads always active
void EmailImportManager::GetChunkOfEmailAccountsForThread(int &nPos, DbRecordSet &lstEmailAccountsChunk, DbRecordSet &lstEmailAccountsAll)
{
	lstEmailAccountsChunk.destroy();
	lstEmailAccountsChunk.copyDefinition(lstEmailAccountsAll);

	int nEndPos;
	int nSize=lstEmailAccountsAll.getRowCount();

	if (nSize<=MAX_THREADS)
	{
		nEndPos=nPos+1; //open new thread for each account
	}
	else
	{
		int nEachThreadAcc=nSize/MAX_THREADS;
		if (nSize%MAX_THREADS>0)
		{
			nEachThreadAcc++; //+1 for overhead
		}
		nEndPos=nPos+nEachThreadAcc;
	}

	/*
	if ((nSize-nPos)<MIN_ACC_PER_THREAD)
	{
		nEndPos=nSize;
	}
	else
	{	//some intelligent logic: if 20<size<100 -> use 2 threads
		if (nSize<100) //2
		{
			nEndPos = nPos+nSize/2;
		}
		else if (nSize<200) //if 20<size<200 -> use 3 threads
		{
			nEndPos = nPos+nSize/3;
		}
		else if (nSize<300) //if 20<size<300 -> use 4 threads
		{
			nEndPos = nPos+nSize/4;
		}
		else //each MAX_ACC_PER_THREAD
		{
			nEndPos = nPos+MAX_ACC_PER_THREAD;
		}

		if (nEndPos>nSize)
			nEndPos=nSize;
	}
	*/

	if (nEndPos>nSize)
		nEndPos=nSize;
	
	lstEmailAccountsAll.clearSelection();
	for (int i=nPos;i<nEndPos;i++)
	{
		lstEmailAccountsAll.selectRow(i);
	}
	lstEmailAccountsChunk.merge(lstEmailAccountsAll,true);

	//check even more if there are more accounts from same user...
	if (lstEmailAccountsChunk.getRowCount()>0)
	{
		int nPersonID=lstEmailAccountsChunk.getDataRef(lstEmailAccountsChunk.getRowCount()-1,"BOUS_PERSON_ID").toInt();
		while(nEndPos<nSize)
		{
			if(nPersonID==lstEmailAccountsAll.getDataRef(nEndPos,"BOUS_PERSON_ID").toInt())
				lstEmailAccountsChunk.merge(lstEmailAccountsAll.getRow(nEndPos));
			else
				break;
			nEndPos++;
		}
	}

	nPos=nEndPos;
}


//-------------------------------------------------------
// thread object for each email import thread
//-------------------------------------------------------

EmailImportManagerThread::EmailImportManagerThread()
{
	m_pEmailImportThreadTask=NULL;
}

EmailImportManagerThread::~EmailImportManagerThread()
{
	if(isRunning())
	{
		m_ThreadSync.ThreadSetForWait();	//prepare for wait
		emit Destroy();
		m_ThreadSync.ThreadWait();			//prepare for start

		//stop thread gently:
		quit();
		wait();
	}
}

void EmailImportManagerThread::TerminateAtOnce()
{
	emit Destroy();
	wait(5);

	//qDebug()<<"closing thread server id"<<ThreadIdentificator::GetCurrentThreadID();
	if(isRunning())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("EmailImportManagerThread:TerminateAtOnce thread was running, waiting 10 seconds to respond to quit, thread id = %1").arg(m_nThreadID));
		quit();		//stop thread
		bool bOK=wait(10000);		//wait 10sec
		if (!bOK)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("EmailImportManagerThread:TerminateAtOnce thread terminate, brute force, thread id = %1").arg(m_nThreadID));
			terminate();
		}
		else
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("EmailImportManagerThread:TerminateAtOnce thread stopped gently, thread id = %1").arg(m_nThreadID));
	}
}



void EmailImportManagerThread::Start(EmailImportManager *pEmailManager,DbRecordSet &lstEmailAccounts)
{
	//SetLastActivity();
	m_pEmailManager = pEmailManager;
	m_pEmailManager->EmailImportThreadStarted(this);
	m_lstEmailAccounts = lstEmailAccounts;
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	start(); //start thread
	m_ThreadSync.ThreadWait();			//prepare for start

	emit StartThreadTask();
}

void EmailImportManagerThread::OnThreadTaskStopped()
{
	m_pEmailImportThreadTask->GetEmailAccounts(m_lstEmailAccounts);
	m_pEmailManager->SaveLastRefreshState(m_lstEmailAccounts);
	m_pEmailManager->EmailImportThreadStopped(this);
	deleteLater();
}

void EmailImportManagerThread::run()
{
	m_pEmailImportThreadTask = new EmailImportThread(&m_ThreadSync,m_lstEmailAccounts);
	connect(this,SIGNAL(StartThreadTask()),m_pEmailImportThreadTask,SLOT(ExecuteTask()));
	connect(this,SIGNAL(Destroy()),m_pEmailImportThreadTask,SLOT(Destroy()));
	
	connect(m_pEmailImportThreadTask,SIGNAL(Stopped()),this,SLOT(OnThreadTaskStopped()));
		
	m_nThreadID=ThreadIdentificator::GetCurrentThreadID();
	m_ThreadSync.ThreadRelease();
	exec();
}

void EmailImportManagerThread::GetThreadInfo( QDateTime &datLastActive, QString &strEmailsProcessing )
{
	datLastActive=GetLastActivity();
	int nSize=m_lstEmailAccounts.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		strEmailsProcessing.append(" "+ m_lstEmailAccounts.getDataRef(i,"POP3_EMAIL").toString());
	}

}

QDateTime EmailImportManagerThread::GetLastActivity()
{
	if (m_pEmailImportThreadTask)
		return m_pEmailImportThreadTask->GetLastActivity();
	else
		return QDateTime::currentDateTime();
}