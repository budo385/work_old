#include "appserverservice.h"
#include "common/common/datahelper.h"


#ifndef QT_NO_DEBUG
	int g_BufferSize;
#endif

#include <QFileInfo>
#include "common/common/config_version_sc.h"

#include <openssl/ssl.h>
#include <openssl/rand.h>

#ifdef _WIN32
	#include "mimepp/src/mimepp/mimepp.h"
#endif



int main(int argc, char *argv[])
{
	//one-time initialization for OpenSSL
	SSL_load_error_strings();
	SSL_library_init();
	//RAND_screen();	//initialize random number generator

	/*
	char tmpBuff[1024];
	QString strA("Ante");
	QByteArray data = strA.toLatin1();
	const char* cKey=data.constData();
	int x =strlen(cKey);
	sprintf(tmpBuff, "\"%s\":[", cKey);
	int y =strlen(tmpBuff);
	QString a =  QString("Apple Push: inc,oming data is too large for push message: key=%1, payload=%2").arg("aa").arg(QString(cKey));
	*/


#ifdef _WIN32
	// Initialize the library
	mimepp::Initialize();
#endif

	// service application
	// Command line params:
    // -i  -install		Install the service.
    // -u  -uninstall   Uninstall the service.
    // -e  -exec
    //      Execute the service as a standalone application (useful for debug purposes).
    //      This is blocking call, service will be executed like normal application.
    //      In this mode you will not be able to communicate with the service from the contoller.
    // -t  -terminate   Stop the service.
    // -p  -pause       Pause the service.
    // -r  -resume      Resume a paused service.
    // -c {cmd} -command {cmd}
	//     Send the user defined command code {cmd} to the service application.
    // -v -version      Display version and status information.

	QString strApplicationServerName=SERVER_DESCRIPTION_TAG;  //app server will have same name as executable
	if (argc>0)
	{
		QFileInfo infoTarget(argv[0]);
		QString strBaseName=infoTarget.baseName();
		if (!strBaseName.isEmpty())
		{
			strApplicationServerName+=" - ";
			strApplicationServerName+=strBaseName;

		}

	}
	

	AppServerService service(argc, argv,strApplicationServerName);

    return service.exec();




}
/*


#ifdef _WIN32
	// Specialized stackwalker-output classes
	class StackWalkerToConsole : public StackWalker
	{
	public:
		QByteArray m_buffer;
	protected:
		virtual void OnOutput(LPCSTR szText)
		{
			m_buffer.append(szText);
		}

		virtual void OnLoadModule(LPCSTR img, LPCSTR mod, DWORD64 baseAddr, DWORD size, DWORD result, LPCSTR symType, LPCSTR pdbName, ULONGLONG fileVersion)
		{
			Q_UNUSED(img);
			Q_UNUSED(mod);
			Q_UNUSED(baseAddr);
			Q_UNUSED(size);
			Q_UNUSED(result);
			Q_UNUSED(symType);
			Q_UNUSED(pdbName);
			Q_UNUSED(fileVersion);
			
			//overriden to skip writing module info, not interested for now
		}
	};
#endif

void DumpBacktrace()
{
#ifdef _WIN32
	//get backtrace events in a buffer
	qDebug()<<"crash occurred, register stack";
	StackWalkerToConsole sw;
	sw.ShowCallstack();

	//write buffer
	QString strDatetime=QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm");
	DataHelper::WriteFile(QCoreApplication::applicationDirPath()+"/settings/crash_"+strDatetime+".log",sw.m_buffer);
	qDebug()<<"crash occurred, write log file";
#endif
}

extern "C" void signal_handler(int sig)
{
	DumpBacktrace();
	_exit(1);
}

void PrepareCrashLog()
{
//#ifdef GENERATE_CRASHLOG
	//install signal handlers to catch crash situations
	signal(SIGABRT, signal_handler);
	signal(SIGFPE,  signal_handler);
	signal(SIGINT,  signal_handler);
	signal(SIGILL,  signal_handler);
	signal(SIGSEGV, signal_handler);
#ifdef _DEBUG
	signal(SIGTERM, signal_handler);	//triggered manually with Ctrl+C in console, for testing only
#endif
//#endif
}
*/