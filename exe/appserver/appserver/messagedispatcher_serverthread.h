#ifndef MESSAGEDISPATCHER_SERVERTHREAD_H
#define MESSAGEDISPATCHER_SERVERTHREAD_H

#include <QObject>
#include "common/common/threadspawner.h"

class MessageDispatcher_ServerThread : public ThreadSpawner
{

public:
	void Stop();

protected:
	ThreadSpawnedObject* CreateThreadSpawnedObject();	
};

#endif // MESSAGEDISPATCHER_SERVERTHREAD_H
