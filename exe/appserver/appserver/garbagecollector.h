#ifndef GARBAGECOLLECTOR_H
#define GARBAGECOLLECTOR_H

#include <QObject>
#include "common/common/backgroundtaskinterface.h"
#include "bus_core/bus_core/usersessionmanager.h"
#include "bus_server/bus_server/systemserviceset.h"
#include "common/common/threadid.h"
#include "trans/trans/httpserver.h"
#include "db/db/dbconnectionreserver.h"




/*!
	\class GarbageCollector
	\brief Cleans expired sessions, run as Background Task in separate thread, 
	\ingroup AppServer


	Gets expired sessions from UserSessionManager, deletes them, sends signals about delete.
	Receives ClientClosedConnection (from HTTP server) to delete session.
	Every N time, process is started, DB and memory is cleaned of used resources!

*/
class GarbageCollector : public QObject, public BackgroundTaskInterface 
{
	Q_OBJECT

public:
	
	GarbageCollector(HTTPServer	*pHttpServer,int nServerMode, int nAppServerSessionID);
    ~GarbageCollector();

	void ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");

signals:
	void DeleteSocket(int); //only for HTTP

public slots:
	void ClientClosedConnection(int nThreadID); 

private:
	//void				CreateSession(Status &err);

	int					m_nServerMode;
	int					m_nAppServerSessionID;
	HTTPServer			*m_HttpServer; 
	QString				m_strSystemSession;
    
};

#endif // GARBAGECOLLECTOR_H
