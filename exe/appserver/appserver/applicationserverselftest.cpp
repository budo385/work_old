#include "applicationserverselftest.h"
#include "common/common/authenticator.h"
#include "common/common/datahelper.h"
#include "trans/trans/smtpclient.h"
#include "db/db/dbsqlquery.h"
#include "bus_server/bus_server/emailimportthread.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "trans/trans/xmlutil.h"

#include "common/common/logger.h"
extern Logger g_Logger;
#include "db/db/dbsqlmanager.h"
extern DbSqlManager	*g_DbManager;
#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;

#define EML_TYPE_POP3 1
#define EML_TYPE_IMAP 2

#define EML_SECURITY_NONE	  1
#define EML_SECURITY_STARTTLS 2
#define EML_SECURITY_SSLTLS	  3


ApplicationServerSelfTest::ApplicationServerSelfTest()
{

}

ApplicationServerSelfTest::~ApplicationServerSelfTest()
{

}


//Note: tester will add 4 characters as digits on end of the username & password
void ApplicationServerSelfTest::AddUserEmailAccountsForTest()
{
	Status pStatus;

	pStatus.setError(0);
	if(g_AppServer->GetIniFile()->m_strStartEmailAdd.isEmpty() || g_AppServer->GetIniFile()->m_strStartPassWordAdd.isEmpty() || g_AppServer->GetIniFile()->m_nCountUserAdd==0)
		return;

	int nStart=g_AppServer->GetIniFile()->m_nStartCounterAdd;
	int nEnd=nStart+g_AppServer->GetIniFile()->m_nCountUserAdd;
	QString strEmailDomain;
	QString strUserStart=g_AppServer->GetIniFile()->m_strStartEmailAdd;
	if (strUserStart.contains("@"))
	{
		strEmailDomain=strUserStart.mid(strUserStart.indexOf("@"));
		strUserStart=strUserStart.left(strUserStart.indexOf("@"));
	}


	QString strPWStart=g_AppServer->GetIniFile()->m_strStartPassWordAdd;

	if (nStart>99999 || nEnd>99999)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"AddUserEmailAccountsForTest: start or end offset are greater then max value of 9999");
		return;
	}

	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;
	QString strSql="SELECT BPER_ID,BPER_LAST_NAME FROM BUS_PERSON";
	query.Execute(pStatus,strSql);
	if(!pStatus.IsOK()) return;
	DbRecordSet lstUsers;
	query.FetchData(lstUsers);

	//lstUsers.Dump();
	if (lstUsers.getRowCount()==0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,"AddUserEmailAccountsForTest: aborting as there is no users in database!!");
		return;
	}

	for (int i=nStart;i<nEnd;i++)
	{
		QString strUser=strUserStart+ConvertNumValue(i)+strEmailDomain;
		QString strPass=strPWStart+ConvertNumValue(i);
		int nPersonID=0;
		int nRow=lstUsers.find("BPER_LAST_NAME",strUser,true,false,true,true);
		if (nRow>=0)
		{
			nPersonID=lstUsers.getDataRef(nRow,0).toInt();
			AddUserEmailAccountIn(pStatus,nPersonID,strUser,strUser,strPass,g_AppServer->GetIniFile()->m_strEmailServerAdd,g_AppServer->GetIniFile()->m_nEmailServerPortAdd,g_AppServer->GetIniFile()->m_nEmailServerIsImapAdd,g_AppServer->GetIniFile()->m_nEmailServerConnectionTypeAdd,1,60);
			if (!pStatus.IsOK())
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("AddUserEmailAccountsForTest: aborting user creation, failed to create email account: %1, pass: %2, error:%3").arg(strUser).arg(strPass).arg(pStatus.getErrorText()));
				return;
			}

			g_AppServer->GetIniFile()->m_strLastEmailAdded=strUser;
		}
	}

	g_AppServer->GetIniFile()->m_nCountUserAdd=0; //adding is finished, stop further:

}



QString ApplicationServerSelfTest::ConvertNumValue( int nValue )
{
	QString strVal=QString::number(nValue);
	if (strVal.size()==1)
		return "000"+strVal;
	if (strVal.size()==2)
		return "00"+strVal;
	if (strVal.size()==3)
		return "0"+strVal;
	return strVal;
}

//uses SMTP, port 25, no authentication, uses m_strEmailServerAdd from INI file and load all users from database which have email in username...
//loads 3 files: mime_example_1.txt, mime_example_2.txt, mime_example_3.txt
void ApplicationServerSelfTest::SendEmailSamplesForTest()
{
#ifdef _DEBUG
	return;
#endif
	//return; //skip for now

	Status pStatus;
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql="SELECT CUSR_USERNAME FROM CORE_USER";
	query.Execute(pStatus,strSql);
	if(!pStatus.IsOK()) return;

	DbRecordSet lstUsers;
	query.FetchData(lstUsers);

	int nSize=lstUsers.getRowCount();
	int nActualUsers=0;
	int nSucessSend=0;
	for (int i=0;i<nSize;i++)
	{
		QString strUser=lstUsers.getDataRef(i,0).toString();
		if (!strUser.contains("@"))
			continue;

		nActualUsers++;

		//load mime_example_1.txt from /app dir:
		QString strMime;
		DataHelper::LoadFile(QCoreApplication::applicationDirPath()+"/mime_example_1.txt",strMime);
		if (!strMime.isEmpty())
		{
			strMime.prepend("From: \"M. Bogdan infoline@sokrates.ch\" <sendmail@mailegant.com>\r\n");
			strMime.prepend("Message-ID: <Yet_another_mailegant_message_1258747751346485@mailegant.com>\r\n");
			strMime.prepend("MIME-Version: 1.0\r\n");
			strMime.prepend("To: "+strUser+"\r\n");
			strMime.prepend("Reply-to: infoline@sokrates.ch\r\n");
			
			QList<QString> lstEmpty;
			SMTPClient client;
			if(!client.SendMail(g_AppServer->GetIniFile()->m_strEmailServer_Test,"sendmail@mailegant.com",strUser,"",strMime,lstEmpty,"","",true,true,g_AppServer->GetIniFile()->m_nEmailServerSMTPPort_Test,true))
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("SendEmailSamplesForTest: sending sample email mime_example_1.txt for email: %1, produce error: %2").arg(strUser).arg(client.GetLastError()));
			}
			else
				nSucessSend++;
		}


		DataHelper::LoadFile(QCoreApplication::applicationDirPath()+"/mime_example_2.txt",strMime);
		if (!strMime.isEmpty())
		{
			strMime.prepend("From: \"M. Bogdan infoline@sokrates.ch\" <sendmail@mailegant.com>\r\n");
			strMime.prepend("Message-ID: <Yet_another_mailegant_message_1258747751346485@mailegant.com>\r\n");
			strMime.prepend("MIME-Version: 1.0\r\n");
			strMime.prepend("To: "+strUser+"\r\n");
			strMime.prepend("Reply-to: infoline@sokrates.ch\r\n");

			QList<QString> lstEmpty;
			SMTPClient client;
			if(!client.SendMail(g_AppServer->GetIniFile()->m_strEmailServer_Test,"sendmail@mailegant.com",strUser,"",strMime,lstEmpty,"","",true,true,g_AppServer->GetIniFile()->m_nEmailServerSMTPPort_Test,true))
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("SendEmailSamplesForTest: sending sample email mime_example_2.txt for email: %1, produce error: %2").arg(strUser).arg(client.GetLastError()));
			}
			else
				nSucessSend++;

		}

		DataHelper::LoadFile(QCoreApplication::applicationDirPath()+"/mime_example_3.txt",strMime);
		if (!strMime.isEmpty())
		{
			strMime.prepend("From: \"M. Bogdan infoline@sokrates.ch\" <sendmail@mailegant.com>\r\n");
			strMime.prepend("Message-ID: <Yet_another_mailegant_message_1258747751346485@mailegant.com>\r\n");
			strMime.prepend("MIME-Version: 1.0\r\n");
			strMime.prepend("To: "+strUser+"\r\n");
			strMime.prepend("Reply-to: infoline@sokrates.ch\r\n");

			QList<QString> lstEmpty;
			SMTPClient client;
			if(!client.SendMail(g_AppServer->GetIniFile()->m_strEmailServer_Test,"sendmail@mailegant.com",strUser,"",strMime,lstEmpty,"","",true,true,g_AppServer->GetIniFile()->m_nEmailServerSMTPPort_Test,true))
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("SendEmailSamplesForTest: sending sample email mime_example_3.txt for email: %1, produce error: %2").arg(strUser).arg(client.GetLastError()));
			}
			else
				nSucessSend++;

		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NONE,QString("SendEmailSamplesForTest: successfully sent %1 emails to %2 users").arg(nSucessSend).arg(nActualUsers));
}


//every N minutes as specified in ini file, send new emails to existing users in DB
void ApplicationServerSelfTest::ExecuteTask( int nCallerID/*=-1*/,int nTaskID/*=-1*/,QString strData/*=""*/ )
{
	if(!m_Mutex.tryLock(1000)) //thread is occupied leave it alone
		return;

	SendEmailSamplesForTest();

	m_Mutex.unlock();
}






void ApplicationServerSelfTest::AddUserEmailAccountIn( Status &Ret_pStatus, int nPersonID, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, bool nIsImap, int nConnectionType, bool bAccountActive, int nDeleteEmailsAfterNoDays)
{
	//load user accounts
	DbRecordSet recPersonAccounts;
	LoadPersonEmailInAccounts(Ret_pStatus, nPersonID, recPersonAccounts);
	if(Ret_pStatus.IsOK())
	{
		if(recPersonAccounts.getColumnCount() < 1){
			//ensure that the recordset is defined	
			recPersonAccounts.addColumn(QVariant::String, "POP3_EMAIL");
			recPersonAccounts.addColumn(QVariant::String, "POP3_HOST");
			recPersonAccounts.addColumn(QVariant::String, "POP3_PORT");
			recPersonAccounts.addColumn(QVariant::String, "POP3_USER");
			recPersonAccounts.addColumn(QVariant::String, "POP3_PASS");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_USE_SSL");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_KNOWN_CONTACT_ONLY");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_CHECK_EVERY_MIN");
			recPersonAccounts.addColumn(QVariant::DateTime, "POP3_LAST_CHECK");
			recPersonAccounts.addColumn(QVariant::DateTime, "POP3_LAST_MESSAGE");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_SERVER_TYPE");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_ACCOUNT_ACTIVE");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_DEL_EMAIL_AFTER_DAYS");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_FULL_SCAN_MODE");	
			recPersonAccounts.addColumn(QVariant::String, "POP3_ACC_UID");		//unique ID for this account (needs to be unique only locally within a set of accounts for a single person)
			recPersonAccounts.addColumn(QVariant::String, "ERROR_EMAIL_UID");	//UID that failed last time we did import
			recPersonAccounts.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");	//log with recent issues with this account
		}
		else{
			//some of these may not exist, not present since beginning
			if(recPersonAccounts.getColumnIdx("POP3_SERVER_TYPE")<0)
				recPersonAccounts.addColumn(QVariant::Int, "POP3_SERVER_TYPE");
			if(recPersonAccounts.getColumnIdx("POP3_KNOWN_CONTACT_ONLY")<0)	
				recPersonAccounts.addColumn(QVariant::Int, "POP3_KNOWN_CONTACT_ONLY");
			if(recPersonAccounts.getColumnIdx("POP3_ACCOUNT_ACTIVE")<0)	
				recPersonAccounts.addColumn(QVariant::Int, "POP3_ACCOUNT_ACTIVE");
			if(recPersonAccounts.getColumnIdx("POP3_DEL_EMAIL_AFTER_DAYS")<0)	
				recPersonAccounts.addColumn(QVariant::Int, "POP3_DEL_EMAIL_AFTER_DAYS");
			if(recPersonAccounts.getColumnIdx("POP3_FULL_SCAN_MODE")<0)	
				recPersonAccounts.addColumn(QVariant::Int, "POP3_FULL_SCAN_MODE");
			if(recPersonAccounts.getColumnIdx("POP3_ACC_UID")<0)
				recPersonAccounts.addColumn(QVariant::String, "POP3_ACC_UID");
			if(recPersonAccounts.getColumnIdx("ERROR_EMAIL_UID")<0)
				recPersonAccounts.addColumn(QVariant::String, "ERROR_EMAIL_UID");	//UID that failed last time we did import
			if(recPersonAccounts.getColumnIdx("ACC_ERROR_MESSAGES")<0)
				recPersonAccounts.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");	//log with recent issues with this account
		}

		//if existing row exists, overwrite it (match by email)
		bool bNewRow = false;
		int nRow = FindEmailInAccountIdx(strEmail, recPersonAccounts);
		if(nRow < 0){
			//add new row
			bNewRow = true;
			recPersonAccounts.addRow();
			nRow = recPersonAccounts.getRowCount()-1;
		}

		//fill in the data
		recPersonAccounts.setData(nRow, "POP3_EMAIL", strEmail);
		recPersonAccounts.setData(nRow, "POP3_HOST",  strServer);
		recPersonAccounts.setData(nRow, "POP3_PORT",  QString::number(nPort));
		recPersonAccounts.setData(nRow, "POP3_USER",  strLogin);
		recPersonAccounts.setData(nRow, "POP3_PASS",  strPassword);
		recPersonAccounts.setData(nRow, "POP3_SERVER_TYPE",  nIsImap ? EML_TYPE_IMAP : EML_TYPE_POP3);
		recPersonAccounts.setData(nRow, "POP3_USE_SSL",  nConnectionType+1);	//+1 maps input to internal flags, Input: 0=plain 1= start TLS 2= TLS/SSL, Output=+1
		recPersonAccounts.setData(nRow, "POP3_ACCOUNT_ACTIVE",  (int)(bAccountActive?1:0));
		recPersonAccounts.setData(nRow, "POP3_DEL_EMAIL_AFTER_DAYS",  nDeleteEmailsAfterNoDays);
		if(bNewRow){
			bool bIsGoogle = strEmail.endsWith("google.com") || strEmail.endsWith("gmail.com");
			recPersonAccounts.setData(nRow, "POP3_CHECK_EVERY_MIN",  bIsGoogle ? 7 : 12);	//default, only if new row
		}
		if(bNewRow)
			recPersonAccounts.setData(nRow, "POP3_KNOWN_CONTACT_ONLY",  0);	//default, only if new row

		//calculate when to start importing messages from (to skip importing old messages)
		QDateTime dtStart = QDateTime::currentDateTime();
		if(nDeleteEmailsAfterNoDays > 0)
			dtStart = dtStart.addDays(-nDeleteEmailsAfterNoDays);

		recPersonAccounts.setData(nRow, "POP3_LAST_MESSAGE",dtStart);	

		if(bNewRow) {
			QString strUID=EmailImportThread::GenerateAccUID();
			recPersonAccounts.setData(nRow, "POP3_ACC_UID", strUID);
		}

		//save user accounts
		SavePersonEmailInAccounts(Ret_pStatus, nPersonID, recPersonAccounts);
	}


}


//load all email in accounts for a single person
void ApplicationServerSelfTest::LoadPersonEmailInAccounts(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData)
{
	//fetch all data for all "email in" user settings
	DbSqlQuery query(Ret_pStatus, g_DbManager);
	if(!Ret_pStatus.IsOK()) return;
	QString strSQL = QString("SELECT BOUS_VALUE_BYTE FROM BUS_OPT_SETTINGS WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID=%3").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS).arg(nPersonID);
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	DbRecordSet data;
	query.FetchData(data);

	//for each person (should be only one)
	int nPersons = data.getRowCount();
	Q_ASSERT(nPersons <= 1);
	for(int x=0; x<nPersons; x++)
	{
		QByteArray binPersonSettings;
		data.getData(x, 0, binPersonSettings);
		if (binPersonSettings.size()>0)
		{
			DbRecordSet dataPerson = XmlUtil::ConvertByteArray2RecordSet_Fast(binPersonSettings);
			if (dataPerson.getRowCount()>0){
				//dataPerson.Dump();
				if (lstPersonAccData.getColumnCount()==0)
					lstPersonAccData.copyDefinition(dataPerson);
				lstPersonAccData.merge(dataPerson);
			}
		}
	}	
	//lstPersonAccData.Dump();
}


void ApplicationServerSelfTest::SavePersonEmailInAccounts(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData)
{
	DbSqlQuery query(Ret_pStatus, g_DbManager);
	if(!Ret_pStatus.IsOK()) return;

	QByteArray binPersonSettings = XmlUtil::ConvertRecordSet2ByteArray_Fast(lstPersonAccData);
	QString strSQL = QString("UPDATE BUS_OPT_SETTINGS SET BOUS_VALUE_BYTE=? WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID=%3").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS).arg(nPersonID);
	query.Prepare(Ret_pStatus, strSQL);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, binPersonSettings);
	query.ExecutePrepared(Ret_pStatus);
}


int ApplicationServerSelfTest::FindEmailInAccountIdx(const QString &strEmail, DbRecordSet &lstPersonAccData)
{
	int nResIdx = -1;
	int nCount = lstPersonAccData.getRowCount();
	for(int i=0; i<nCount; i++){
		QString strCurEmail;
		lstPersonAccData.getData(i, "POP3_EMAIL", strCurEmail);
		if(strCurEmail == strEmail){
			nResIdx = i;
			break;
		}
	}
	return nResIdx;
}