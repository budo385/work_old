#include "appserver.h"
#include "common/common/threadid.h"
#include "db/db/dbsqlmanager.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "db_core/db_core/dbconnsettings.h"
//BO
#include "bus_server/bus_server/systemserviceset.h"
#include "bus_server/bus_server/businessserviceset.h"
#include "bus_server/bus_server/privateserviceset.h"
#include "bus_server/bus_server/usersessionmanager_server.h"
#include "common/common/config.h"
#include "bus_core/bus_core/licenseaccessrights.h"
#include "bus_server/bus_server/loadbalancer.h"
#include "bus_server/bus_server/useraccessright_server.h"
#include "common/common/logger.h"
#include "messagedispatcher_server.h"
#include "bus_core/bus_core/fp_default.h" //defines FP codes 
#include "serverbackupmanager.h"
#include "serverimportexportmanager.h"
#include "serverremindermanager.h"
#include "common/common/zipcompression.h"
#include "trans/trans/tcphelper.h"
#include "bus_core/bus_core/customavailability.h"
#include "message_notificator.h"
#include "applicationserverselftest.h"
#include "common/common/stackwalkerlogger.h"

#define GBYTE 1073741824

CRASH_SIGNAL_HANDLER

//-----------------------------------------------
//	 SERVER GLOBALS:
//------------------------------------------------



//DB
DbSqlManager *g_DbManager;

//BO
BusinessServiceSet* g_BusinessServiceSet;
PrivateServiceSet*	g_PrivateServiceSet;
SystemServiceSet*	g_SystemServiceSet; 

//SPECIALS:
UserSessionManager*				g_UserSessionManager;
UserAccessRight*				g_AccessRight;
LicenseAccessRights				g_ModuleLicense;
LoadBalancer*					g_LoadBalancer;
Logger							g_Logger;
MessageDispatcher*				g_MessageDispatcher;
BackupManager*					g_pBackupManager;
ImportExportManagerAbstract*	g_pImportExportManager;
ServerReminderManagerAbstract*	g_pReminderManager;
MessageNotificator*				g_MessageNotificator;
ApplicationServerSelfTest*		g_pAppSelfTester;


/*!
	Constructor
*/
ApplicationServer::ApplicationServer(QObject *parent,QString strDescriptor)
:QObject(parent) //ServerThread(parent),SokratesHttpServer(this)
{
	m_strDescriptor=strDescriptor;
	ClearPointers();
	m_nTimerID=-1;
	m_bIsRunning=false;
	connect(this,SIGNAL(RestartDelayedThreadSafeSignal(int)),this,SLOT(RestartDelayedThreadSafeSlot(int)),Qt::QueuedConnection);
	connect(this,SIGNAL(StopThreadSafeSignal()),this,SLOT(StopThreadSafeSlot()),Qt::QueuedConnection);
	ApplicationLogger::setApplicationLogger(&g_Logger);
}

void ApplicationServer::ClearPointers()
{
	g_DbManager=NULL;
	m_XmlRpcSkeletonServerSet=NULL;
	g_PrivateServiceSet=NULL;
	g_BusinessServiceSet=NULL;
	g_UserSessionManager=NULL;
	g_SystemServiceSet=NULL;
	m_XmlRpcHTPPServer=NULL;
	m_RestHTTPServer=NULL;
	m_WebServiceRpcDispatcher=NULL;


	m_HtmlHTTPServer = NULL;
	m_PingHttpServer=NULL;
	m_UserStorageHTTPServer=NULL;

	m_ServerSessionManager=NULL;
	m_GarbageCollector=NULL;
	m_MasterGarbageCollector=NULL;
	m_strIniFilePath="";
	m_GbBkgTask=NULL;
	m_MasterGbBkgTask=NULL;
	m_SrvSessionBkgTask=NULL;
	m_SrvBackupBkgTask=NULL;
	m_SrvImportExportBkgTask=NULL;
	m_SrvReminderBkgTask=NULL;
	g_MessageDispatcher=NULL;
	g_pBackupManager=NULL;
	g_pImportExportManager =NULL;
	g_pReminderManager=NULL;
	m_EmailSubscribe=NULL;
	m_EmailSubscribeBkgTask=NULL;
	g_MessageNotificator=NULL;
	m_AppServerSelfTestTask=NULL;

	//GLOBALS:
	g_AccessRight=NULL;
	g_LoadBalancer=NULL;
	g_pAppSelfTester=NULL;

}

/*!
	Destructor: kill all objects
*/
ApplicationServer::~ApplicationServer()
{
	if (IsRunning())
		Stop();
}

void ApplicationServer::Restart(Status &pStatus)
{
	//QMutexLocker locker(&m_Mutex); //lock acces
	pStatus.setError(0);
	Stop();
	Start(pStatus);

	if(!pStatus.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,pStatus.getErrorText());
		//try to write error into log:
		GetIniFile()->m_strLastErrorText=pStatus.getErrorText();
		GetIniFile()->m_nLastErrorCode=StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_FAILED_TO_START;
		GetIniFile()->Save();
		Stop();
		qApp->quit(); //close app if err!!!
	}
	else
	{
		//try to write error into log:
		GetIniFile()->m_strLastErrorText="";
		GetIniFile()->m_nLastErrorCode=StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTED_OK;
		GetIniFile()->Save();
	}
}

void ApplicationServer::RestartDelayed(int nReStartAfterSec)
{
	QMutexLocker locker(&m_Mutex);							//lock acces
	emit RestartDelayedThreadSafeSignal(nReStartAfterSec);	//ensures that appserver thread executes this!
}

//ignore parameter: do it now!
void ApplicationServer::StopDelayed(int nStopAfterSec)
{
	QMutexLocker locker(&m_Mutex);	//lock acces
	emit StopThreadSafeSignal();	//ensures that appserver thread executes this!
}

void ApplicationServer::StopThreadSafeSlot()
{
	Stop();
	qApp->quit(); //close all
}
void ApplicationServer::SaveSettings()
{
	QMutexLocker locker(&m_Mutex);	//lock acces

	Status err;
	g_pBackupManager->GetBackupData(err,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);

	m_INIFile.m_nLogLevel=g_Logger.GetLogLevel();
	m_INIFile.m_nLogMaxSize=g_Logger.GetMaxLogSize();
	m_INIFile.m_nLogBufferSize=g_Logger.GetMemoryBufferSize();

	m_INIFile.Save(m_strIniFilePath);
}
void ApplicationServer::RestartDelayedPriv(int nReStartAfterSec)
{
	//already in restart mode, ignore
	if (m_nTimerID!=-1)
	{
		return;
	}
	//this is fine opportunity to fire warning to all clients->
	m_nTimerID=startTimer(nReStartAfterSec*1000);
}

void ApplicationServer::Stop()
{

	QMutexLocker locker(&m_Mutex); //lock acces


	//if (!m_bIsRunning)	return;

	//stop msg dispatcher (it uses server and all others)
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopping Message dispatcher");
	m_MsgDispatcherThread.Stop();
	g_MessageDispatcher=NULL;
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Message dispatcher stopped");


	

	//stop server:
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopping Http server");
	SokratesHttpServer.StopServer();
	SokratesHttpServer.ClearRequestHandlerList();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Http server stopped");

	//Stop sequence must be in particular order: (1st stop background services)
	if(m_GbBkgTask!=NULL)m_GbBkgTask->Stop();
	if(m_SrvSessionBkgTask!=NULL)m_SrvSessionBkgTask->Stop();
	if(m_SrvBackupBkgTask!=NULL)m_SrvBackupBkgTask->Stop();
	if(m_SrvImportExportBkgTask!=NULL)m_SrvImportExportBkgTask->Stop();
	if(m_SrvReminderBkgTask!=NULL)m_SrvReminderBkgTask->Stop();
	if(m_EmailSubscribeBkgTask!=NULL)m_EmailSubscribeBkgTask->Stop();
	if(m_AppServerSelfTestTask!=NULL)m_AppServerSelfTestTask->Stop();
	if(m_MessageNotificatorTask!=NULL)m_MessageNotificatorTask->Stop();
	if(g_MessageNotificator!=NULL)delete g_MessageNotificator;


	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Bkg Tassks stopped");
	
	//save backup options in INI:
	if (m_SrvBackupBkgTask!=NULL)
	{
		Status err;
		g_pBackupManager->GetBackupData(err,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);
		//if (err.IsOK())
		//Q_ASSERT(err.IsOK());
	}

	//save log & all rest:
	m_INIFile.m_nLogLevel=g_Logger.GetLogLevel();
	m_INIFile.m_nLogMaxSize=g_Logger.GetMaxLogSize();
	m_INIFile.m_nLogBufferSize=g_Logger.GetMemoryBufferSize();
	m_INIFile.Save(m_strIniFilePath);

	//destroy bkg tasks:
	if(m_GbBkgTask!=NULL) delete m_GbBkgTask;
	if(m_MasterGbBkgTask!=NULL) delete m_MasterGbBkgTask;
	if(m_SrvSessionBkgTask!=NULL) delete m_SrvSessionBkgTask;
	if(m_SrvBackupBkgTask!=NULL)delete m_SrvBackupBkgTask;
	if(m_SrvImportExportBkgTask!=NULL)delete m_SrvImportExportBkgTask;
	if(m_SrvReminderBkgTask!=NULL)delete m_SrvReminderBkgTask;
	if(m_EmailSubscribeBkgTask!=NULL)delete m_EmailSubscribeBkgTask;
	if(m_AppServerSelfTestTask!=NULL)delete m_AppServerSelfTestTask;
	if(m_MessageNotificatorTask!=NULL)delete m_MessageNotificatorTask;
	
	//save kick ban list: BUG!!!->CRASH!!!!!
	/*
	DbRecordSet lstAccessList;
	if(SokratesHttpServer.GetIPKickBanList()!=NULL)
		if(SokratesHttpServer.GetIPKickBanList()->IsListChanged())
		{
			Status err;
			SokratesHttpServer.GetIPKickBanList()->GetDynamicList(lstAccessList);
			m_ServerSessionManager->SaveKickBanList(err,m_INIFile.m_strIPBlockListNameDynamic,lstAccessList);
		}
		
	*/

	//destroy objects:
	if(m_XmlRpcHTPPServer!=NULL)			delete m_XmlRpcHTPPServer;
	if(m_HtmlHTTPServer != NULL)			delete m_HtmlHTTPServer;
	if(m_PingHttpServer != NULL)			delete m_PingHttpServer;
	if(m_UserStorageHTTPServer != NULL)		delete m_UserStorageHTTPServer;
	if(m_RestHTTPServer!=NULL)				delete m_RestHTTPServer;
	if(m_EmailSubscribe!=NULL)				delete m_EmailSubscribe;


	
	if(m_XmlRpcSkeletonServerSet!=NULL)	delete m_XmlRpcSkeletonServerSet;
	if(m_WebServiceRpcDispatcher!=NULL)	delete m_WebServiceRpcDispatcher;
	if(g_PrivateServiceSet!=NULL)	delete g_PrivateServiceSet;
	if(g_BusinessServiceSet!=NULL)	delete g_BusinessServiceSet;
	if(g_UserSessionManager!=NULL)	delete g_UserSessionManager;

	//try to stop app srv session:
	Status err;
	if(m_ServerSessionManager!=NULL)
	{
		m_ServerSessionManager->StopSession(err);
		delete m_ServerSessionManager;
	}
	if(m_GarbageCollector!=NULL) delete m_GarbageCollector;
	if(m_MasterGarbageCollector!=NULL)	delete m_MasterGarbageCollector;
	if(g_SystemServiceSet!=NULL) delete g_SystemServiceSet;
	if(g_DbManager!=NULL)
	{
		g_DbManager->ShutDown();
		delete g_DbManager;
	}
	if(g_pBackupManager!=NULL)delete g_pBackupManager;
	if(g_pImportExportManager!=NULL)delete g_pImportExportManager;
	if(g_pReminderManager!=NULL)delete g_pReminderManager;
	
	//GLOBALS:
	if(g_LoadBalancer!=NULL)delete g_LoadBalancer;
	if(g_AccessRight!=NULL)delete g_AccessRight;
	if(g_pAppSelfTester!=NULL)delete g_pAppSelfTester;
		
	m_bIsRunning=false;
	ClearPointers();

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_STOP);
	g_Logger.FlushFileLog(); //write all logs
	
}



/*!
	Start App. server
*/
void ApplicationServer::Start(Status &pStatus)
{
	//prepare crash log
	PrepareCrashLog();

	QMutexLocker locker(&m_Mutex); //lock acces

	if (m_bIsRunning)	return;

	HTTPServerConnectionSettings ServerSettings;
	DbConnectionSettings DbConnSettings;

	/*
	QString strFromHex=QByteArray::fromHex("6bc427c8a7981f4fe1f5ac65c1246b5f");
	strFromHex=QByteArray::fromHex("cf6dd3cf1923c950586d0dd595c8e20b");
	strFromHex=QByteArray::fromBase64("AFQjCNGkN2AUZcE5P8tTVcdw5af_rDAC8A");
	*/
	
	/*
	DbRecordSet lst;
	lst.addColumn(QVariant::Double,"X");
	lst.addRow();
	lst.setData(0,"X",0.00);
	double dbX = lst.getDataRef(0,"X").toDouble();
	*/

	//----------------------------------------------
	//LOAD INI files:
	//----------------------------------------------
	LoadSettings(pStatus,ServerSettings,DbConnSettings);
	//save back descriptor:
	m_INIFile.m_strDescriptor=m_strDescriptor;
	m_INIFile.Save(m_strIniFilePath);

	
	//----------------------------------------------
	//Start logger 
	//----------------------------------------------
	if (m_INIFile.m_nLogLevel>LoggerAbstract::LOG_LEVEL_NONE)
	{
		if (m_INIFile.m_nLogTargetFile)
			g_Logger.EnableFileLogging(QCoreApplication::applicationDirPath()+"/settings/appserver.log",m_INIFile.m_nLogBufferSize,m_INIFile.m_nLogMaxSize);
		if (m_INIFile.m_nLogTargetConsole)
			g_Logger.EnableConsoleLogging();
	}
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);

	//bring up logger, then test for error...
	if(!pStatus.IsOK()) return;


	//try to log start sequence:
	m_INIFile.m_strLastErrorText="";
	m_INIFile.m_nLastErrorCode=StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTING;
	m_INIFile.Save();


	//get data for testing if app. server was alive:
	int nUniqueLicenseID=g_ModuleLicense.GetUniqueLicenseID();

	CustomAvailability::SetCustomSolutionID(g_ModuleLicense.GetCustomSolutionID());


	//----------------------------------------------
	//init Database
	//----------------------------------------------

	InitDatabaseConnection(pStatus,&g_DbManager,DbConnSettings,m_INIFile.m_nMaxDbConnections,m_INIFile.m_nMinDbConnections);
	if(!pStatus.IsOK())return;
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_DATABASE_CONN_SUCESS,DbConnSettings.m_strDbName+";"+DbConnSettings.m_strDbHostName);

	//----------------------------------------------
	//Sys services
	//----------------------------------------------
	g_SystemServiceSet= new SystemServiceSet(g_DbManager);


	//---------------------------------------------
	//Backup manager -> only if master
	//---------------------------------------------
	ServerBackupManager *pSrvBcp=new ServerBackupManager;
	g_pBackupManager = pSrvBcp;
	g_pBackupManager->SetBackupData(pStatus,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);
	if(!pStatus.IsOK())return;
	g_pBackupManager->Initialize(pStatus,g_DbManager->GetDbSettings(),false);
	if(!pStatus.IsOK())return;

	//try to log db check sequence:
	m_INIFile.m_strLastErrorText="";
	m_INIFile.m_nLastErrorCode=StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_REORGANIZING_DATABASE;
	m_INIFile.Save();


	if (m_INIFile.m_nIsMasterServer)
	{
		bool bError=true;
		if(pStatus.IsOK())
		{
			g_pBackupManager->StartUpCheck(pStatus);
			if(pStatus.IsOK())
			{
				//save, whatever:
				g_pBackupManager->GetBackupData(pStatus,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);
				Q_ASSERT(pStatus.IsOK());
				m_INIFile.Save(m_strIniFilePath);

				//start bkg manager::
				m_SrvBackupBkgTask=new BackupTaskExecutor(pSrvBcp,this);
				m_SrvBackupBkgTask->Start();
				bError=false;
			}
		}

		//if error, log, exit: must exit if Db not reorganized!
		if (bError)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_FAIL,pStatus.getErrorText());
			return;
		}

		//signal connect: only if master server
		connect(g_pBackupManager,SIGNAL(RestoreCommandIssued(int)),this,SLOT(OnBcpManager_RestoreCommandIssued(int)));
		connect(g_pBackupManager,SIGNAL(SettingsChanged()),m_SrvBackupBkgTask,SLOT(OnBcpManager_SettingsChanged()));
		connect(g_pBackupManager,SIGNAL(BackupCommandIssued(int)),m_SrvBackupBkgTask,SLOT(OnBcpManager_BackupCommandIssued(int)));
	}


	//todo: read from keyfile:
	ReadDemoFP();
	//m_nMaxDemoDbKb=260000;
	//m_nWarnDemoDbKb=255000;
	//m_bDemoDatabase=true;
	g_pBackupManager->CheckForDemoDatabase(pStatus,m_bDemoDatabase,m_nMaxDemoDbKb,m_nWarnDemoDbKb);
	if(!pStatus.IsOK())return;


	//try to log db check sequence:
	m_INIFile.m_strLastErrorText="";
	m_INIFile.m_nLastErrorCode=StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTING_SERVICES;
	m_INIFile.Save();


	//----------------------------------------------
	//start App. server session (automatic garbage cleaning) in separate thred:
	//----------------------------------------------
	m_ServerSessionManager=new ServerSessionManager(g_SystemServiceSet);

	QString IPAdrr;
//	IPAdrr=IPDetector::GetLocalIPAddress();
	IPAdrr=ServerSettings.m_strServerIP+":"+QVariant(ServerSettings.m_nPort).toString();
	int nAppSrvSessionID=m_ServerSessionManager->StartSession(pStatus,IPAdrr,nUniqueLicenseID,SRV_ROLE_APPSERVER,m_INIFile.m_nMaxConnections,m_INIFile.m_nLoadBalanceActive,m_INIFile.m_nIsMasterServer,m_INIFile.m_strUpdateTime, m_INIFile.m_strWarningUpdateTime,"",m_INIFile.m_strName);
	if(!pStatus.IsOK())return;

	//load kick ban lists:
	DbRecordSet lstIPAccessStatic,lstIPAccessDynamic;
	/*
	if(m_INIFile.m_bUseSecurityOptions)
	{
		m_ServerSessionManager->LoadKickBanList(pStatus,m_INIFile.m_strIPBlockListNameDynamic,lstIPAccessDynamic);
		if(!pStatus.IsOK())return;
		m_ServerSessionManager->LoadKickBanList(pStatus,m_INIFile.m_strIPBlockListNamePreset,lstIPAccessStatic);
		if(!pStatus.IsOK())return;
	}
	*/
	
	//start reporter:
	m_SrvSessionBkgTask=new BackgroundTaskExecutor(m_ServerSessionManager,this,APP_SESSION_REPORT_INTERVAL*1000);
	m_SrvSessionBkgTask->Start();

	//----------------------------------------------
	//Start LBO (only used if m_nLoadBalanceActive=1), but started always as it can be used not only for LBO:
	//----------------------------------------------

	if(m_INIFile.m_nLoadBalanceActive)
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Load Balance process started");
	g_LoadBalancer=new LoadBalancer(m_INIFile.m_nLoadBalanceActive,g_SystemServiceSet,IPAdrr,LBO_LIST_REFRESH_INTERVAL);


	//----------------------------------------------
	//UserSession manager (heart of system):
	//----------------------------------------------
	//--------> interval of expired sessions are set to GBC interval, altough client reports at CLIENT_IS_ALIVE_INTERVAL (safe zone for network slow..)
	g_UserSessionManager =new UserSessionManager_Server(g_SystemServiceSet,&g_ModuleLicense,CLIENT_IS_ALIVE_INTERVAL_CHECK,nAppSrvSessionID,IPAdrr,m_INIFile.m_nMaxConnections,m_INIFile.m_nAcceptDFConnections);



	//----------------------------------------------
	//Launch Business & Private sets/init pointers:
	//----------------------------------------------
	g_PrivateServiceSet = new PrivateServiceSet(g_DbManager,g_UserSessionManager);
	g_BusinessServiceSet = new BusinessServiceSet(g_DbManager,g_UserSessionManager);



	//----------------------------------------------
	//Launch AR tester global
	//----------------------------------------------
	g_AccessRight=new UserAccessRight_Server;


	//---------------------------------------------
	//Import & Export  (must be after business/ar)
	//---------------------------------------------
	if (m_INIFile.m_nIsMasterServer)
	{
		ServerImportExportManager *pImportTasker=new ServerImportExportManager;
		g_pImportExportManager = pImportTasker;
		m_SrvImportExportBkgTask=new ImportExportTaskExecutor(pImportTasker,this);
		g_pImportExportManager->SetBusinessServiceSet(pStatus,g_BusinessServiceSet);
		if(!pStatus.IsOK())return;
		if(g_pImportExportManager->ReloadSettings(pStatus))
		{
			connect(g_pImportExportManager,SIGNAL(GlobalPeriodChanged(int)),m_SrvImportExportBkgTask,SLOT(OnImportExportManager_GlobalPeriodChanged(int)));
			connect(g_pImportExportManager,SIGNAL(SignalStartTask(int,int)),m_SrvImportExportBkgTask,SLOT(OnImportExportManager_StartTask(int,int)));
			int nPeriod=g_pImportExportManager->GetGlobalPeriod(pStatus);
			if (nPeriod>0)
			{
				m_SrvImportExportBkgTask->OnImportExportManager_GlobalPeriodChanged(nPeriod);
			}
		}
		if(!pStatus.IsOK())return;

		//reminder manager:
		ServerReminderManager *pReminder = new ServerReminderManager;
		g_pReminderManager = pReminder;
		m_SrvReminderBkgTask=new ServerReminderTaskExecutor(pReminder,this);
		connect(pReminder,SIGNAL(GlobalPeriodChanged(int)),m_SrvReminderBkgTask,SLOT(OnGlobalPeriodChanged(int)));
		g_pReminderManager->RebuildReminderList();
	}


	//----------------------------------------------
	//Launch all server handlers:
	//----------------------------------------------
	//TcpHelper::ResolveWebAddressNetworkInterface(ServerSettings.m_strServerIP.toLower(),ServerSettings.m_nPort,ServerSettings.m_bUseSSL);

	m_XmlRpcSkeletonServerSet = new XmlRpcSkeletonServerSet(g_BusinessServiceSet);
	m_XmlRpcHTPPServer =		new XmlRpcHTPPServer(m_XmlRpcSkeletonServerSet,g_UserSessionManager);
	m_WebServiceRpcDispatcher = new WebServiceRpcDispatcher(g_BusinessServiceSet);
	if (m_INIFile.m_nRestEnable)
		m_RestHTTPServer		  = new RestHTTPServer_Sokrates(m_WebServiceRpcDispatcher);
	if (m_INIFile.m_nWWWEnable)
		m_HtmlHTTPServer =			new HtmlHTTPServer(m_INIFile.m_strWebDocumentsPath);

	m_PingHttpServer =			new PingHTTPServer();
	m_UserStorageHTTPServer =	new UserStorageHTTPServer_Sokrates();


	//----------------------------------------------
	//Launch Server->client message handler
	//----------------------------------------------
	m_MsgDispatcherThread.Start();
	g_MessageDispatcher = dynamic_cast<MessageDispatcher_Server*>(m_MsgDispatcherThread.GetThreadSpawnedObject());
	if (g_MessageDispatcher==NULL)
	{
		pStatus.setError(1,"Message dispatcher failed to load");
		return;
	}
	else
	{
		dynamic_cast<MessageDispatcher_Server*>(g_MessageDispatcher)->Init(&SokratesHttpServer);
	}


	//----------------------------------------------
	//Start Garbage Collector in separate thread (GBC controls HTTP server):
	//----------------------------------------------
	m_GarbageCollector= new GarbageCollector(&SokratesHttpServer,SRV_ROLE_APPSERVER,nAppSrvSessionID);
	m_GbBkgTask=new BackgroundTaskExecutor(m_GarbageCollector,this,GARBAGE_COLLECTOR_INTERVAL*1000);
	m_GbBkgTask->Start();

	if(m_INIFile.m_nIsMasterServer)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Master process started");
		m_MasterGarbageCollector= new MasterGarbageCollector(g_SystemServiceSet,nAppSrvSessionID);
		m_MasterGbBkgTask=new BackgroundTaskExecutor(m_MasterGarbageCollector,this,MASTER_GARBAGE_COLLECTOR_INTERVAL*1000);
		m_MasterGbBkgTask->Start(true); //as soon as another thread starts, run master cleanup!!!
	}

	//----------------------------------------------
	//Start email sync
	//----------------------------------------------
	if (m_INIFile.m_nEnableEmailSync)
	{
		m_EmailSubscribe= new EmailImportManager();
		m_EmailSubscribeBkgTask=new BackgroundTaskExecutor(m_EmailSubscribe,this,EMAIL_SYNC_INTERVAL*1000);
		m_EmailSubscribeBkgTask->Start();
		m_EmailSubscribeBkgTask->setPriority(QThread::LowestPriority);

		//----------------------------------------------
		//Start push messaging
		//----------------------------------------------
		g_MessageNotificator= new MessageNotificator();
		m_MessageNotificatorTask=new BackgroundTaskExecutor(g_MessageNotificator,this,EMAIL_PUSH_CHECK_INTERVAL*1000);
		m_MessageNotificatorTask->Start();
		m_MessageNotificatorTask->setPriority(QThread::LowestPriority);
	}


	//----------------------------------------------
	//Launch HTTP server, register XML-RPC server
	//----------------------------------------------
	//init this handler:
	QString strUserStorage=InitUserStorage(m_INIFile.m_strTempUserStorageDirectory);
	if (strUserStorage.isEmpty())
	{
		pStatus.setError(1,"Failed to init user storage handler!");
		return;
	}
	g_UserSessionManager->SetUserStorageDirectoryPath(strUserStorage);
	g_UserSessionManager->SetUserStorageDiskQuota((qint64)m_INIFile.m_nDiskUserStorageQuota*(qint64)GBYTE); //fix at 200Gb...
	m_UserStorageHTTPServer->SetBackupDirectoryPath(m_INIFile.m_strBackupPath);

	SokratesHttpServer.ClearRequestHandlerList();
	SokratesHttpServer.RegisterRequestHandler(m_XmlRpcHTPPServer);
	SokratesHttpServer.RegisterRequestHandler(m_PingHttpServer);
	SokratesHttpServer.RegisterRequestHandler(m_UserStorageHTTPServer);
	if (m_INIFile.m_nRestEnable)
		SokratesHttpServer.RegisterRequestHandler(m_RestHTTPServer);
	if (m_INIFile.m_nWWWEnable)
	{
		m_HtmlHTTPServer->m_nDefaultSocketTimeOut=20; //set timeout to 20min
		SokratesHttpServer.RegisterRequestHandler(m_HtmlHTTPServer);
	}

	SokratesHttpServer.InitServer(ServerSettings,false,/*m_INIFile.m_bUseSecurityOptions*/&lstIPAccessStatic,&lstIPAccessDynamic);
	SokratesHttpServer.StartListen(pStatus);


	//LOG sucessfull startup:
	if (pStatus.IsOK())
	{
		QString appParams=ServerSettings.m_strServerIP+";"+QVariant(ServerSettings.m_nPort).toString()+";"+QVariant(ServerSettings.m_bUseSSL).toString();
		appParams+=";"+QVariant(nAppSrvSessionID).toString()+";"+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString();
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_START,appParams);
		m_bIsRunning=true;
	}
	

	//----------------------------------------------
	//now start self test:
	//----------------------------------------------
	g_pAppSelfTester = new ApplicationServerSelfTest;
	if (m_INIFile.m_nTestSendingEmailsPeriodMinutes>0)
	{
		m_AppServerSelfTestTask=new BackgroundTaskExecutor(g_pAppSelfTester,this,m_INIFile.m_nTestSendingEmailsPeriodMinutes*60*1000);
		m_AppServerSelfTestTask->Start();
		m_AppServerSelfTestTask->setPriority(QThread::LowestPriority);
	}
	QTimer::singleShot(0,this,SLOT(OnFillDatabaseAtStartForTest()));


//	int n=0;
//	int x=100/n;

	//test:
/*
	QFile filek("D:/stunnel-4.26-installer.exe");
	filek.open(QIODevice::ReadOnly);
	QByteArray data=filek.readAll();
	QByteArray ziped;
	bool bOK=ZipCompression::Deflate(data,ziped);
	qDebug()<<ziped.size();
	filek.close();
	QByteArray newX;
	bOK=ZipCompression::Inflate(ziped,newX);
	qDebug()<<newX.size();
	QFile filek2("D:/fffff.exe");
	filek2.open(QIODevice::WriteOnly);
	filek2.write(newX);
	filek2.close();
	qDebug()<<ziped.size();

*/

}





/*!
	Inits global DbManager

	\param pStatus				if err occur, returned here
	\param pDbManager			dbManager will be created here if suceed
	\param ConnSettings			db conn settings
	\param nMaxDbConnections	max db connections
*/
void ApplicationServer::InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections,int nMinDbConnections)
{


#if 0
	//Make ORACLE connectiion
	ConnSettings.m_strDbType=DBTYPE_ORACLE;
	ConnSettings.m_strDbHostName="ORASERVER";
	ConnSettings.m_strDbUserName="SOK";
	ConnSettings.m_strDbPassword="SOK";
	ConnSettings.m_strDbName="ORASERVER";
#endif
	
#if 0
	//Make MYSQL connectiion
	ConnSettings.m_strDbType=DBTYPE_MYSQL;
	ConnSettings.m_strDbHostName="localhost";
	ConnSettings.m_strDbUserName="root";
	ConnSettings.m_strDbPassword="helix1";
	ConnSettings.m_strDbName="test";
#endif

	//ConnSettings.m_nDbPoolTimeout=500000; //500sec

	//make Dbmanager, set to max conn:
	*pDbManager = new DbSqlManager;
	(*pDbManager)->Initialize(pStatus, ConnSettings,nMaxDbConnections,nMinDbConnections);
	if (!pStatus.IsOK())return;
}




void ApplicationServer::LoadSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings, DbConnectionSettings &pDbSettings)
{



	//load ini file & server settings:
	//------------------------------------------
	m_strIniFilePath= QCoreApplication::applicationDirPath()+"/settings/app_server.ini";
	bool bOK=m_INIFile.Load(m_strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,"INI file is missing,corrupted or has invalid values!");
		return;
	}


	QFileInfo ssl(m_INIFile.m_strSSLCertificateDir+"/server.cert");
	if (!ssl.exists())
	{
		m_INIFile.m_strSSLCertificateDir=QCoreApplication::applicationDirPath()+"/settings";
	}


	pServerSettings.m_strCertificateFile=m_INIFile.m_strSSLCertificateDir+"/server.cert";
	if (QFile::exists(m_INIFile.m_strSSLCertificateDir+"/server.ca-bundle"))
		pServerSettings.m_strCACertificateFileBundle=m_INIFile.m_strSSLCertificateDir+"/server.ca-bundle";
	pServerSettings.m_strPrivateKeyFile=m_INIFile.m_strSSLCertificateDir+"/server.pkey";
	pServerSettings.m_nPort=m_INIFile.m_nAppServerPort;
	pServerSettings.m_nSSLNoPrivateKey=m_INIFile.m_nSSLNoPrivateKey;
	//pServerSettings.m_nDefaultTimeOut=m_INIFile.m_nTimeOutWhenInactive;
	pServerSettings.m_nKickPeriodAfterIntrusion=m_INIFile.m_nKickPeriodAfterIntrusion;
	pServerSettings.m_nIntrusionTries=m_INIFile.m_nIntrusionTries;

	if(m_INIFile.m_strAppServerIPAddress.isEmpty())
		pServerSettings.m_strServerIP="0.0.0.0"; //listen on all ports
	else
		pServerSettings.m_strServerIP=m_INIFile.m_strAppServerIPAddress;
	pServerSettings.m_bUseSSL=m_INIFile.m_nSSLMode;



	


	//load DB SETTINGS:
	//------------------------------------------
	DbConnectionsList lstDbSettings;
	if(m_INIFile.m_strDBConnectionName.isEmpty())
	{
		pStatus.setError(1,"Database connection is not defined!");
		return;
	}

	QString strDbSettingsPath= QCoreApplication::applicationDirPath()+"/settings/database_connections.cfg";
	bOK=lstDbSettings.Load(strDbSettingsPath,MASTER_PASSWORD); //PASSWORD!???
	if(!bOK)
	{
		pStatus.setError(1,"Database setting file file is missing or corrupted!");
		return;
	}

	bOK=lstDbSettings.GetDbSettings(m_INIFile.m_strDBConnectionName,pDbSettings);
	if(!bOK)
	{
		pStatus.setError(1,"Database connection not found inside database settting file!");
		return;
	}

	



	//load MODULE LICENSE file:
	//------------------------------------------
	QString strLicensePath= QCoreApplication::applicationDirPath()+"/settings/sokrates.key";
	bOK=g_ModuleLicense.LoadLicense(strLicensePath);
	if(!bOK)
	{
		pStatus.setError(1,"License file is missing or corrupted!");
		return;
	}
}


//overriden QObject timer method
void ApplicationServer::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		m_nTimerID=-1;
		Status err;
		Restart(err);
	}
}



void ApplicationServer::RestartDelayedThreadSafeSlot(int nRestartAfter)
{
	RestartDelayedPriv(nRestartAfter);
}



void ApplicationServer::TestDemoDatabaseSize(Status &pStatus, bool &bFireWarningMsg, int &nCurrSizeKb, int &nMaxSizeKb)
{
	QMutexLocker locker(&m_Mutex); //lock acces
	pStatus.setError(0);
	if (!m_bDemoDatabase)
	{
		bFireWarningMsg=false;
		return;
	}

	g_pBackupManager->RecalcDemoDatabaseSize(pStatus,nCurrSizeKb);
	nMaxSizeKb=m_nMaxDemoDbKb;
	if (nCurrSizeKb>m_nWarnDemoDbKb)
		bFireWarningMsg=true;
	else
		bFireWarningMsg=false;
}


///reads data for disk qouta/demo base //after keyfile load
void ApplicationServer::ReadDemoFP()
{
	m_bDemoDatabase=false;

	QHash<QString,QString> lstModules;
	g_ModuleLicense.GetModuleCodes(lstModules);
	QHashIterator <QString,QString> x(lstModules);
	while (x.hasNext()) 
	{
		x.next();

		int nValue=0;
		bool bPresent=g_ModuleLicense.CanUseFP(x.key(),x.value(),FP_DISK_QUOTA_FOR_BLOBS,nValue);
		if (bPresent && nValue>0)
		{
			m_nMaxDemoDbKb=nValue*1024;
			m_nWarnDemoDbKb=0.9*m_nMaxDemoDbKb;
			m_bDemoDatabase=true;
			return;
		}
	}
}



//thread safe
void ApplicationServer::OnBcpManager_RestoreCommandIssued(int nCallerID)
{
	//schedule for RESTART APP SERVER in 20 seconds:
	emit RestartDelayedThreadSafeSignal(20);
}


QString ApplicationServer::InitUserStorage(QString strPath)
{
	if (strPath.isEmpty())
		strPath = QCoreApplication::applicationDirPath();

	QFileInfo info(g_DbManager->GetDbName());
	QString strDb=info.baseName();
	QString strUserStorageDir=QDir::cleanPath(strPath+"/"+strDb+"/user_storage");
	
	QDir dir(strPath);
	if (!dir.exists(strDb))
		if(!dir.mkdir(strDb))
			return "";
	QDir dir1(strPath+"/"+strDb);
	if (!dir1.exists("user_storage"))
		if(!dir1.mkdir("user_storage"))
			return "";

	return strUserStorageDir;
}

void ApplicationServer::SendErrorMessage( int nPersonID, QString strErrorText, int nErrorCode/*=1*/, QString strEmailAccount/*=""*/ )
{
	g_MessageNotificator->SendErrorMessage(nPersonID,strErrorText,nErrorCode,strEmailAccount);
}

void ApplicationServer::SendNewEmailNotification( int nPersonID, QString strEmailAccount,DbRecordSet &lstNewEmails )
{
	g_MessageNotificator->SendNewEmailNotification(nPersonID,strEmailAccount,lstNewEmails);
}

void ApplicationServer::SendPushNotification( int nPersonID, QString strMessage, QString strPushToken, int nOSType, QString strPushSoundName, int nBadgeNumber )
{
	Status err;
	g_MessageNotificator->SchedulePushMessageForSend(err,strMessage,strPushToken,nPersonID,nOSType,strPushSoundName,nBadgeNumber);
}


void ApplicationServer::OnFillDatabaseAtStartForTest()
{
	g_pAppSelfTester->AddUserEmailAccountsForTest();
	m_INIFile.Save(m_strIniFilePath);
}