#include "messagedispatcher_serverthread.h"
#include "messagedispatcher_server.h"
#include "common/common/logger.h"
extern Logger				g_Logger;

ThreadSpawnedObject* MessageDispatcher_ServerThread::CreateThreadSpawnedObject()
{
	return new MessageDispatcher_Server;
}


void MessageDispatcher_ServerThread::Stop()
{
	if(isRunning())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Closing message thread"));
		quit();		//stop thread
		bool bOK=wait(30000);		//wait 30sec
		if (!bOK)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_MESSAGE_DISPATCHER_FAILED,QString("Thread failed to finish, terminate issued!"));
			terminate();
		}
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Message thread stopped"));
	}

}