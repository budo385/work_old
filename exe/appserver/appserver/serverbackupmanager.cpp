#include "serverbackupmanager.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbsqlmanager.h"
#include "common/common/threadid.h"

extern DbSqlManager *g_DbManager;
#include "bus_server/bus_server/systemserviceset.h"
extern SystemServiceSet* g_SystemServiceSet;
#include "bus_server/bus_server/businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "common/common/logger.h"
extern Logger				g_Logger;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_core/bus_core/messagedispatcher.h"
extern MessageDispatcher* g_MessageDispatcher;

#define MUTEX_LOCK_TIMEOUT 30000 //longer timeout



//: when called directly from client: callerid=personid, taskid is ignored
void ServerBackupManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	Status pStatus;
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT)) //all bcp data can be read out by others
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Backup process started, thread:" +QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());

	QString strBackupFileName=ExecBackup(pStatus);
	if (!pStatus.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_BACKUP_FAIL,pStatus.getErrorText());
	}
	
	if (nCallerID!=-1)
	{
		QStringList lstPersonID;
		lstPersonID<<QVariant(nCallerID).toString();
		if (!pStatus.IsOK())
			g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_BACKUP_FAIL,pStatus.getErrorText(),&lstPersonID);
		else
			g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_SUCCESS,strBackupFileName,&lstPersonID);
	}

	m_RWMutex.unlock();
}



void ServerBackupManager::ClearDatabaseAfterRestore(Status &pStatus)
{
	g_SystemServiceSet->AppSrvSession->CleanAllSessions(pStatus);		
}



void ServerBackupManager::StartDatabase()
{
	g_DbManager->EnablePool();
	g_DbManager->FillPoolWithMinConnections();
}
void ServerBackupManager::CloseDatabase()
{
	g_DbManager->ShutDown();
}


//delayed backup
void ServerBackupManager::Backup(Status &pStatus)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	if (m_bRestoreInProgress)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		m_RWMutex.unlock();
		return;
	}
	m_RWMutex.unlock();
	emit BackupCommandIssued(g_UserSessionManager->GetPersonID());
}
//delayed restore
void ServerBackupManager::Restore(Status &pStatus,QString strFileNameForRestore)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	m_strRestoreOnNextStart=strFileNameForRestore;
	m_bRestoreInProgress=true;
	m_RWMutex.unlock();
	emit RestoreCommandIssued(g_UserSessionManager->GetPersonID());
}