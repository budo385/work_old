#ifndef IMPORTEXPORTTASKEXECUTOR_H
#define IMPORTEXPORTTASKEXECUTOR_H

#include <QObject>
#include "common/common/backgroundtaskexecutor.h"
//#include "serverimportexportmanager.h"

class ImportExportTaskExecutor : public BackgroundTaskExecutor
{
	Q_OBJECT

public:
	ImportExportTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent=NULL);

public slots:
	void OnImportExportManager_GlobalPeriodChanged(int nPeriodMin);
	void OnImportExportManager_StartTask(int nCallerID=-1,int nTaskID=-1);

};
#endif // IMPORTEXPORTTASKEXECUTOR_H
