#ifndef MESSAGEDISPATCHER_SERVER_H
#define MESSAGEDISPATCHER_SERVER_H

#include <QObject>
#include "bus_trans_server/bus_trans_server/rpcstub_servermessageset.h"
#include "trans/trans/httpserver.h"
#include "bus_core/bus_core/messagedispatcher.h"



class MessageDispatcher_Server : public MessageDispatcher
{
	Q_OBJECT

public:
	MessageDispatcher_Server(QObject *parent=NULL);
	virtual ~MessageDispatcher_Server();

	void Init(HTTPServer	*pHttpServer);

protected slots:
	void NewMessage(int nMsgID);

private:
	RpcStubServerMessageSet	*m_RpcMsg;

	
};

#endif // MESSAGEDISPATCHER_SERVER_H
