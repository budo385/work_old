#ifndef SERVERREMINDERMANAGER_H
#define SERVERREMINDERMANAGER_H

#include "bus_core/bus_core/serverremindermanagerabstract.h"
#include "common/common/backgroundtaskinterface.h"
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include <QReadWriteLock>

class ServerReminderManager : public QObject, public BackgroundTaskInterface, public ServerReminderManagerAbstract
{
	Q_OBJECT

public:
	ServerReminderManager();
	~ServerReminderManager();

	void		ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");									//separate thread-> execute backup
	void		CreateReminder(Status &Ret_pStatus,QString strSubject, QDateTime datDue,int nCategory, int nOwnerID, int nRecvContactID,int nTableID=-1, int nRecordID=-1, QString strData="",bool bOneShoot=false);
	void		GetAllActiveRemindersForUser(Status &Ret_pStatus,int nPersonID,DbRecordSet &RetReminders);
	void		DeleteReminders(Status &Ret_pStatus,DbRecordSet &lstReminders);
	void		PostPoneReminders(Status &Ret_pStatus,DbRecordSet &lstReminders, int nMinutes);
	void		RebuildReminderList();
	void		ReminderCleanUp();
	int			GetNextSpawnTimeSeconds();
	QDateTime	GetNextSpawnTimeDateTime();

signals:
	void		GlobalPeriodChanged(int nPeriodSec); 
	void		SendReminder_Delayed(int nReminderID); 

private slots:
	void		OnSendReminder_Delayed(int nReminderID);

private:
	void		SendReminder(Status &Ret_pStatus,int nReminderID,QDateTime &datNextExecution);
	void		GetOwnerSettings(Status &Ret_pStatus, int nOwnerID,int nCategory,bool &bIsAllowedToRemind, DbRecordSet &RetSettings);
	QDateTime	CalculateReminderFireDateTime(DbRecordSet &RetSettings,QDateTime datDue);
	void		ScheduleReminderExecution(int nReminderID,QDateTime &datNextExecution);
	void		RecalcNextSpawnDateTime();

	void		SendReminder_BySokrates(Status &Ret_pStatus,int nPersonID,const DbRecordSet &rowReminder);
	void		SendReminder_ByEmail(Status &Ret_pStatus,QString strEmail,const DbRecordSet &rowReminder){};
	void		SendReminder_BySms(Status &Ret_pStatus,QString strPhone,const DbRecordSet &rowReminder){};
	
	QReadWriteLock			m_ReminderListLock;
	QHash<int,QDateTime>	m_lstScheduledReminders;
	QReadWriteLock			m_SpawnDateLock;
	QDateTime				m_datNextSpawn;
	int						m_nSecondsToNextSpawn;
};

#endif // SERVERREMINDERMANAGER_H
