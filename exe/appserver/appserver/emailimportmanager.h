#ifndef EMAILIMPORTMANAGER_H
#define EMAILIMPORTMANAGER_H

#include "common/common/backgroundtaskinterface.h"
#include "common/common/dbrecordset.h"
#include "bus_server/bus_server/emailimportthread.h"
#include "common/common/threadsync.h"
#include "common/common/logger.h"

/*
	Load balancing mega importer.
*/


class EmailImportManagerThread;
class EmailImportManager : public QObject, public BackgroundTaskInterface
{
	Q_OBJECT

public:
	EmailImportManager();
	~EmailImportManager();

	void ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");		
	void SaveLastRefreshState(DbRecordSet &lstEmailAccounts);

public slots:
	void EmailImportThreadStarted(EmailImportManagerThread *pThread);
	void EmailImportThreadStopped(EmailImportManagerThread *pThread);

private:
	void LoadAllEmailAccounts(DbRecordSet &lstEmailAccounts);
	void GetChunkOfEmailAccountsForThread(int &nPos, DbRecordSet &lstEmailAccountsChunk, DbRecordSet &lstEmailAccountsAll);
	void SavePersonEmailAccData(int nPersonID, DbRecordSet &lstPersonAccData);
	void LoadPersonEmailAccData(int nPersonID, DbRecordSet &lstPersonAccData);

	int GetActiveThreads();
	int m_nActiveThreads;
	QReadWriteLock m_Lck;
	QHash<int,QStringList> m_lstEmailAccsInProcess; //person id, <email addreses in process>
	QList<EmailImportManagerThread*> m_lstImportThreads;
	//DbRecordSet m_lstEmailAccounts;
};


class EmailImportManagerThread : public QThread
{
	Q_OBJECT
public:
	EmailImportManagerThread();
	~EmailImportManagerThread();

	void		Start(EmailImportManager *pEmailManager, DbRecordSet &lstEmailAccounts);
	void		GetThreadInfo(QDateTime &datLastActive, QString &strEmailsProcessing);
	void		TerminateAtOnce();
	QDateTime	GetLastActivity();

signals:
	void StartThreadTask();
	void Destroy();

public slots:
	void OnThreadTaskStopped();

private:
	void run();
	
	DbRecordSet				m_lstEmailAccounts;
	ThreadSynchronizer		m_ThreadSync;
	EmailImportThread		*m_pEmailImportThreadTask;
	EmailImportManager		*m_pEmailManager;
	int						m_nThreadID;
};

#endif // EMAILIMPORTMANAGER_H
