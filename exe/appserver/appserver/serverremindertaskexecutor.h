#ifndef SERVERREMINDERTASKEXECUTOR_H
#define SERVERREMINDERTASKEXECUTOR_H

#include <QObject>
#include "common/common/backgroundtaskexecutor.h"

class ServerReminderTaskExecutor : public BackgroundTaskExecutor
{
	Q_OBJECT

public:
	ServerReminderTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent);

public slots:
	void OnGlobalPeriodChanged(int nPeriodSec);
	
};

#endif // SERVERREMINDERTASKEXECUTOR_H
