#ifndef MASTERGARBAGECOLLECTOR_H
#define MASTERGARBAGECOLLECTOR_H


#include "common/common/backgroundtaskinterface.h"
#include "bus_core/bus_core/usersessionmanager.h"
#include "bus_server/bus_server/systemserviceset.h"
#include "common/common/threadid.h"



/*!
	\class MasterGarbageCollector
	\brief Cleans expired sessions, run as Background Task in separate thread, 
	\ingroup AppServer


	Gets expired sessions from UserSessionManager, deletes them, sends signals about delete.
	Receives ClientClosedConnection (from HTTP server) to delete session.
	Every N time, process is started, DB and memory is cleaned of used resources!

*/
class MasterGarbageCollector :  public BackgroundTaskInterface 
{

public:
	
	MasterGarbageCollector(SystemServiceSet * pSysServices, int nAppServerSessionID);
    ~MasterGarbageCollector();

	void ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");



private:
	//void				CreateSession(Status &err);
	SystemServiceSet	*m_pSysServices;
	int					m_nAppServerSessionID;
	QString				m_strSystemSession;
    
};

#endif // GARBAGECOLLECTOR_H
