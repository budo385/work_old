#include "serverremindermanager.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "bus_core/bus_core/reminderhelpercore.h"
#include "trans/trans/xmlutil.h"
#include <QWriteLocker>
#include <QReadLocker>

#include "bus_server/bus_server/businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "db/db/dbsqlmanager.h"
extern DbSqlManager *g_DbManager;
#include "common/common/logger.h"
extern Logger				g_Logger;
#include "bus_core/bus_core/messagedispatcher.h"
extern MessageDispatcher* g_MessageDispatcher;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;

ServerReminderManager::ServerReminderManager()
{
	connect(this,SIGNAL(SendReminder_Delayed(int)),this,SLOT(OnSendReminder_Delayed(int)),Qt::QueuedConnection);
}

ServerReminderManager::~ServerReminderManager()
{

}

void ServerReminderManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	QWriteLocker lock(&m_ReminderListLock);

	QDateTime datCurrent=QDateTime::currentDateTime();
	QHash<int,QDateTime> lstRemindersForExecute;

	//get first N reminders within 60seconds:
	QHashIterator<int,QDateTime> i(m_lstScheduledReminders);
	while (i.hasNext()) 
	{
		i.next();
		int nSecondsTo=i.value().secsTo(datCurrent);
		if (nSecondsTo<60) //covers even negative (should already be sent)
		{
			lstRemindersForExecute[i.key()]=i.value();
		}
	}

	//execute each reminder, store new next date into list:
	QHashIterator<int,QDateTime> k(lstRemindersForExecute);
	while (i.hasNext()) 
	{
		k.next();
		Status err;
		QDateTime dateNext;
		SendReminder(err,k.key(),dateNext);
		if (!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_FAILED_TO_SEND_REMINDER,QString::number(k.key())+";"+err.getErrorText());
		}

		m_lstScheduledReminders.remove(k.key()); //remove executed reminders
		if (dateNext.isValid()) 
		{
			m_lstScheduledReminders[k.key()]=dateNext; //reassign new date if they are recurring
		}
	}

	//calc next spawn time:
	lock.unlock();
	RecalcNextSpawnDateTime();
}

void ServerReminderManager::RecalcNextSpawnDateTime()
{
	QReadLocker lock1(&m_ReminderListLock);
	QWriteLocker lock2(&m_SpawnDateLock);

	QDateTime lastDate=m_datNextSpawn;

	QDateTime datCurrent=QDateTime::currentDateTime();

	m_datNextSpawn=QDateTime();
	m_nSecondsToNextSpawn=-1;

	//get first one as Qmap is sorted by key
	QHashIterator<int,QDateTime> i(m_lstScheduledReminders);
	if (i.hasNext()) 
	{
		i.next();
		if (!m_datNextSpawn.isValid())
			m_datNextSpawn=i.value();
		else if (m_datNextSpawn>i.value())
			m_datNextSpawn=i.value();
	}

	if (m_datNextSpawn.isValid())
	{
		m_nSecondsToNextSpawn = datCurrent.secsTo(m_datNextSpawn)-30;
	}

	if (m_nSecondsToNextSpawn<=0 || m_nSecondsToNextSpawn>30*60) //no longer then 30min
	{
		m_nSecondsToNextSpawn=30*60;
	}

	m_datNextSpawn=datCurrent.addSecs(m_nSecondsToNextSpawn);

	//emit period change only if shorter then previous:
	if (!lastDate.isValid())
	{
		emit GlobalPeriodChanged(m_nSecondsToNextSpawn);
	}
	else if (m_datNextSpawn<lastDate)
	{
		emit GlobalPeriodChanged(m_nSecondsToNextSpawn);
	}
		
}

int	ServerReminderManager::GetNextSpawnTimeSeconds()
{
	QReadLocker lock(&m_SpawnDateLock);
	return m_nSecondsToNextSpawn;
}

QDateTime ServerReminderManager::GetNextSpawnTimeDateTime()
{
	QReadLocker lock(&m_SpawnDateLock);
	return m_datNextSpawn;
}

void ServerReminderManager::ScheduleReminderExecution(int nReminderID,QDateTime &datNextExecution)
{
	QWriteLocker lock(&m_ReminderListLock);
	m_lstScheduledReminders[nReminderID]=datNextExecution;
}


//at start: read DB, compile reminderlist+spawn times
void ServerReminderManager::RebuildReminderList()
{
	ReminderCleanUp();

	Status err;
	DbSimpleOrm TableOrm(err, BUS_REMINDERS, g_DbManager); 
	if(!err.IsOK())
		{	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_REMINDER_REBUILD_FAILED,err.getErrorText());return; 	}	

	DbRecordSet lstData;
	QString strWhere=" WHERE BRE_ONE_SHOOT_REMIND = 0";
	TableOrm.Read(err,lstData,strWhere);
	if(!err.IsOK())
	{	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_REMINDER_REBUILD_FAILED,err.getErrorText());return; 	}	


	QWriteLocker lock(&m_ReminderListLock);
	m_lstScheduledReminders.clear();

	int nOwnerID=-1;
	int nCategory=-1;
	DbRecordSet rowSettings;
	bool bIsAllowedToRemind=true;

	//sort by owner/category:
	SortDataList lstSort;
	lstSort<<SortData(lstData.getColumnIdx("BRE_OWNER_ID"),0);
	lstSort<<SortData(lstData.getColumnIdx("BRE_CATEGORY"),0);
	lstData.sortMulti(lstSort);

	int nSize=lstData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (lstData.getDataRef(i,"BRE_OWNER_ID").toInt()!=nOwnerID && lstData.getDataRef(i,"BRE_CATEGORY").toInt()!=nCategory)
		{
			nOwnerID=lstData.getDataRef(i,"BRE_OWNER_ID").toInt();
			nCategory=lstData.getDataRef(i,"BRE_CATEGORY").toInt();
			GetOwnerSettings(err,nOwnerID,nCategory,bIsAllowedToRemind,rowSettings);
			if(!err.IsOK()) //if failed one, just skip that one
			{	
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_FAILED_READ_REMINDER_SETTINGS,err.getErrorText());
				continue;
			}	
		}

		//recalc next execution date, put in list only valid ones, save others in the database
		QDateTime datNextExecution;
		if (!bIsAllowedToRemind || rowSettings.getRowCount()==0)
			datNextExecution=QDateTime();
		else
			datNextExecution=CalculateReminderFireDateTime(rowSettings,lstData.getDataRef(i,"BRE_DAT_DUE").toDateTime());

		lstData.setData(i,"BRE_DAT_SEND_REMIND",datNextExecution);
		if (datNextExecution.isValid())
			m_lstScheduledReminders[lstData.getDataRef(i,"BRE_ID").toInt()]=datNextExecution;
	}

	lock.unlock();

	//recalc time:
	RecalcNextSpawnDateTime();

	//save changes into db:
	if (lstData.getRowCount()>0)
	{
		TableOrm.Write(err,lstData);
		if(!err.IsOK())
		{	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_REMINDER_REBUILD_FAILED,err.getErrorText());return; 	}	
	}

}

//garbage collector: all one shot delete on due date + all other on due_date+24hr if enabled.
//before DB deletion, check dynamic list: if exists, clean from there, then from DB.
void ServerReminderManager::ReminderCleanUp()
{
	Status err;
	DbSimpleOrm TableOrm(err, BUS_REMINDERS, g_DbManager); 
	if(!err.IsOK())
		{	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_REMINDER_GBC_FAILED,err.getErrorText());return; 	}	

	DbSqlQuery *query=TableOrm.GetDbSqlQuery();
	DbRecordSet lstData;
	QString strSQL="SELECT BRE_ID FROM BUS_REMINDERS WHERE BRE_DAT_EXPIRE IS NOT NULL AND BRE_DAT_EXPIRE >= ?";
	query->Prepare(err,strSQL);
	if(!err.IsOK())
		{	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_REMINDER_GBC_FAILED,err.getErrorText());return; 	}


	QDateTime datCurrent=QDateTime::currentDateTime();
	query->bindValue(0,datCurrent);
	query->ExecutePrepared(err);
	if(!err.IsOK())
	{	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_REMINDER_GBC_FAILED,err.getErrorText());return; 	}

	query->FetchData(lstData);
	if (lstData.getRowCount()>0)
	{
		DeleteReminders(err,lstData);
		if(!err.IsOK())
			{	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_REMINDER_GBC_FAILED,err.getErrorText());return; 	}
	}
}

//gets all one_shoot reminders + next_date=NULL + all overdue + all not due - but with failed notify by SC
void ServerReminderManager::GetAllActiveRemindersForUser(Status &Ret_pStatus,int nPersonID,DbRecordSet &RetReminders)
{
	RetReminders.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_REMINDERS));

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_REMINDERS, g_DbManager); 
	if(!Ret_pStatus.IsOK()) return;

	//get all one-shot and expired:
	DbRecordSet lstDataExpired;
	QString strSQL=" WHERE BRE_OWNER_ID=" +QString::number(nPersonID)+" AND (BRE_ONE_SHOOT_REMIND = 1 OR BRE_DAT_SEND_REMIND IS NULL)";
	TableOrm.Read(Ret_pStatus,lstDataExpired,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	//get all overdue:
	DbSqlQuery *query=TableOrm.GetDbSqlQuery();
	strSQL=DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_BUS_REMINDERS)+" WHERE BRE_OWNER_ID=" +QString::number(nPersonID)+" AND BRE_DAT_DUE > ?";
	query->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	QDateTime datCurrent=QDateTime::currentDateTime();
	query->bindValue(0,datCurrent);
	query->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstDataOverDue;
	query->FetchData(lstDataOverDue);

	//get all not overdue but with notify Sc failed
	strSQL=DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_BUS_REMINDERS)+" WHERE BRE_OWNER_ID=" +QString::number(nPersonID)+" AND BRE_SC_NOTIFY_FAILED=1 AND BRE_DAT_DUE < ?";
	query->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	query->bindValue(0,datCurrent);
	query->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;
	DbRecordSet lstDataDueButNotSeenByUser;
	query->FetchData(lstDataDueButNotSeenByUser);

	RetReminders.merge(lstDataExpired);
	RetReminders.merge(lstDataOverDue);
	RetReminders.merge(lstDataDueButNotSeenByUser);

}

void ServerReminderManager::DeleteReminders(Status &Ret_pStatus,DbRecordSet &lstReminders)
{
	QWriteLocker lock(&m_ReminderListLock);
	int nSize=lstReminders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		m_lstScheduledReminders.remove(lstReminders.getDataRef(i,"BRE_ID").toInt());
	}
	lock.unlock();

	RecalcNextSpawnDateTime(); //leave spawn as is..

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_REMINDERS, g_DbManager); 
	if(!Ret_pStatus.IsOK()) return;
	TableOrm.DeleteFast(Ret_pStatus,lstReminders);
}

void ServerReminderManager::PostPoneReminders(Status &Ret_pStatus,DbRecordSet &lstReminders, int nMinutes)
{
	if (nMinutes<=0)
		return;

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_REMINDERS, g_DbManager); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstRemindersForUpdate; //re-read reminders from database:
	TableOrm.ReadFromParentIDs(Ret_pStatus,lstRemindersForUpdate,"BRE_ID",lstReminders,"BRE_ID");
	if(!Ret_pStatus.IsOK()) return;

	QWriteLocker lock(&m_ReminderListLock);
	lstRemindersForUpdate.clearSelection();
	int nSize=lstRemindersForUpdate.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (lstRemindersForUpdate.getDataRef(i,"BRE_ONE_SHOOT_REMIND").toInt()>0)
		{
			lstRemindersForUpdate.selectRow(i);
			continue;
		}
		int nKeyID=lstRemindersForUpdate.getDataRef(i,"BRE_ID").toInt();
		if (m_lstScheduledReminders.contains(nKeyID))
		{
			QDateTime newDateTime=m_lstScheduledReminders.value(nKeyID).addSecs(nMinutes*60);
			m_lstScheduledReminders[nKeyID]=newDateTime;
			lstRemindersForUpdate.setData(i,"BRE_DAT_SEND_REMIND",newDateTime);
		}
		else
		{
			QDateTime newDateTime=lstRemindersForUpdate.getDataRef(i,"BRE_DAT_SEND_REMIND").toDateTime().addSecs(nMinutes*60);
			lstRemindersForUpdate.setData(i,"BRE_DAT_SEND_REMIND",newDateTime);
		}
	}
	lock.unlock();

	RecalcNextSpawnDateTime(); //leave spawn as is..

	lstRemindersForUpdate.deleteSelectedRows();

	//write into db new values:
	TableOrm.Write(Ret_pStatus,lstRemindersForUpdate);
}

//save reminder into database: if one shoot exec at once, return, else add to schedule list:
void ServerReminderManager::CreateReminder(Status &Ret_pStatus,QString strSubject, QDateTime datDue,int nCategory, int nOwnerID, int nRecvContactID,int nTableID, int nRecordID, QString strData,bool bOneShoot)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_REMINDERS, g_DbManager); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet rowSettings;
	bool bIsAllowedToRemind;
	GetOwnerSettings(Ret_pStatus,nOwnerID,nCategory,bIsAllowedToRemind,rowSettings);
	if(!Ret_pStatus.IsOK()) return;

	if (!bIsAllowedToRemind)
		return;

	DbRecordSet rowReminder;
	rowReminder.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_REMINDERS));

	rowReminder.addRow();
	rowReminder.setData(0,"BRE_EVENT_TABLE_ID",nTableID);
	rowReminder.setData(0,"BRE_EVENT_RECORD_ID",nRecordID);
	rowReminder.setData(0,"BRE_EVENT_DATA",strData);
	rowReminder.setData(0,"BRE_SUBJECT",strSubject);
	rowReminder.setData(0,"BRE_DAT_DUE",datDue);
	rowReminder.setData(0,"BRE_CATEGORY",nCategory);
	rowReminder.setData(0,"BRE_ONE_SHOOT_REMIND",(int)bOneShoot);
	rowReminder.setData(0,"BRE_SC_NOTIFY_FAILED",0);
	rowReminder.setData(0,"BRE_OWNER_ID",nOwnerID);
	rowReminder.setData(0,"BRE_RECV_CONTACT_ID",nRecvContactID);

	QDateTime datNextExecution;
	QDateTime datExpireDate; //reminder will be deleted if one shoot after 24hr or if overdue after 24 if enabled
	//calc date of next execution
	if (rowReminder.getDataRef(0,"BRE_ONE_SHOOT_REMIND").toInt()>0)
	{
		datExpireDate=datDue.addDays(1);
		datNextExecution=QDateTime();
	}
	else
	{
		if (rowSettings.getRowCount()==0)
			datNextExecution=QDateTime();
		else
			datNextExecution=CalculateReminderFireDateTime(rowSettings,rowReminder.getDataRef(0,"BRE_DAT_DUE").toDateTime());


		if (rowSettings.getDataRef(0,"REM_DELETE_IF_OVERDUE").toInt()>0)
			datExpireDate=datDue.addDays(1);
	}
	//save changes into db:
	rowReminder.setData(0,"BRE_DAT_SEND_REMIND",datNextExecution);
	rowReminder.setData(0,"BRE_DAT_EXPIRE",datExpireDate);
	
	TableOrm.Write(Ret_pStatus,rowReminder);

	//shot right away or insert into list
	int nReminderID=rowReminder.getDataRef(0,"BRE_ID").toInt();
	emit SendReminder_Delayed(nReminderID);
}

//when reminder is to be sent: load record, and send:
void ServerReminderManager::OnSendReminder_Delayed(int nReminderID)
{
	Status err;
	DbSimpleOrm TableOrm(err, BUS_REMINDERS, g_DbManager); 
	if(!err.IsOK()) 
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_FAILED_TO_SEND_REMINDER,QString::number(nReminderID)+";"+err.getErrorText());
		return;
	}

	DbRecordSet rowReminder;
	TableOrm.Read(err,rowReminder," WHERE BRE_ID="+QString::number(nReminderID));
	if(!err.IsOK()) 
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_FAILED_TO_SEND_REMINDER,QString::number(nReminderID)+";"+err.getErrorText());
		return;
	}


	if (rowReminder.getRowCount()==0)
	{
			return;
	}


	if (rowReminder.getDataRef(0,"BRE_ONE_SHOOT_REMIND").toInt()>0)
	{
		QDateTime dat;
		SendReminder(err,nReminderID,dat);
		if(!err.IsOK()) 
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_FAILED_TO_SEND_REMINDER,QString::number(nReminderID)+";"+err.getErrorText());
			return;
		}
	}
	else
	{
		ScheduleReminderExecution(nReminderID,rowReminder.getDataRef(0,"BRE_DAT_SEND_REMIND").toDateTime()); //schedule for execution in another reminder executor thread...
	}

}


//send reminder, save changes into db, return calculated next execution date if needed: can return error or
void ServerReminderManager::SendReminder(Status &Ret_pStatus,int nReminderID,QDateTime &datNextExecution)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_REMINDERS, g_DbManager); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet rowReminder;
	TableOrm.Read(Ret_pStatus,rowReminder," WHERE BRE_ID="+QString::number(nReminderID));
	if(!Ret_pStatus.IsOK()) return;

	if (rowReminder.getRowCount()==0)
	{
		datNextExecution=QDateTime();
		return;
	}

	DbRecordSet rowSettings;
	bool bIsAllowedToRemind;
	int nContactID=rowReminder.getDataRef(0,"BRE_RECV_CONTACT_ID").toInt();
	int nOwnerID=rowReminder.getDataRef(0,"BRE_OWNER_ID").toInt();
	int nCategory=rowReminder.getDataRef(0,"BRE_CATEGORY").toInt();
	
	GetOwnerSettings(Ret_pStatus,nOwnerID,nCategory,bIsAllowedToRemind,rowSettings);
	if(!Ret_pStatus.IsOK()) return;
	
	if (!bIsAllowedToRemind || rowSettings.getRowCount()==0)
	{
		datNextExecution=QDateTime();
		return;
	}

	//Get contact data for sending:
	QString strEmail,strPhone;
	int nPersonID=-1;
	g_BusinessServiceSet->BusContact->ReadContactReminderData(Ret_pStatus,nContactID,nPersonID,strEmail,strPhone);
	if(!Ret_pStatus.IsOK()) return;

	if (rowSettings.getDataRef(0,"REM_SEND_BY_SOKRATES").toInt()>0 && nPersonID>0)
	{
		SendReminder_BySokrates(Ret_pStatus,nPersonID,rowReminder);
		if (!Ret_pStatus.IsOK()) //only log if failed:
		{
			//set status inside record / no log:
			rowReminder.setData(0,"BRE_SC_NOTIFY_FAILED",1);
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_FAILED_TO_SEND_REMINDER_SOKRATES,rowReminder.getDataRef(0,"BRE_SUBJECT").toString());
		}
	}

	if (rowSettings.getDataRef(0,"REM_SEND_BY_EMAIL").toInt()>0 && !strEmail.isEmpty())
	{
		SendReminder_ByEmail(Ret_pStatus,strEmail,rowReminder);
		if (!Ret_pStatus.IsOK()) //only log if failed:
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_FAILED_TO_SEND_REMINDER_EMAIL,rowReminder.getDataRef(0,"BRE_SUBJECT").toString()+";"+strEmail);
		}
	}

	if (rowSettings.getDataRef(0,"REM_SEND_BY_SMS").toInt()>0 && !strPhone.isEmpty())
	{
		SendReminder_BySms(Ret_pStatus,strPhone,rowReminder);
		if (!Ret_pStatus.IsOK()) //only log if failed:
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_FAILED_TO_SEND_REMINDER_SMS,rowReminder.getDataRef(0,"BRE_SUBJECT").toString()+";"+strPhone);
		}
	}

	//calc date of next execution
	if (rowReminder.getDataRef(0,"BRE_ONE_SHOOT_REMIND").toInt()>0)
	{
		datNextExecution=QDateTime();
	}
	else
	{
		datNextExecution=CalculateReminderFireDateTime(rowSettings,rowReminder.getDataRef(0,"BRE_DAT_DUE").toDateTime());
	}

	//save changes into db:
	rowReminder.setData(0,"BRE_DAT_SEND_REMIND",datNextExecution);
	TableOrm.Write(Ret_pStatus,rowReminder);
}

void ServerReminderManager::GetOwnerSettings(Status &Ret_pStatus, int nOwnerID,int nCategory,bool &bIsAllowedToRemind, DbRecordSet &RetSettings)
{
	RetSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_REMINDER_SETTINGS));

	DbRecordSet lstSettings;
	g_BusinessServiceSet->CoreServices->GetPersonalSettingsByPersonID(Ret_pStatus,nOwnerID,lstSettings);
	if(!Ret_pStatus.IsOK()) return;

	int nCategorySettingsEnableID=-1;
	int nCategorySettingsDataID=-1;

	switch (nCategory)
	{
	case ReminderHelperCore::REMINDER_CATEGORY_CALENDAR:
		{
			nCategorySettingsEnableID=REMINDER_CALENDAR_ENABLED;
			nCategorySettingsDataID=REMINDER_CALENDAR_SETTINGS;
		}
		break;
	case ReminderHelperCore::REMINDER_CATEGORY_TASK:
		{
			nCategorySettingsEnableID=REMINDER_TASK_ENABLED;
			nCategorySettingsDataID=REMINDER_TASK_SETTINGS;
		}
		break;
	case ReminderHelperCore::REMINDER_CATEGORY_APPSERVER:
		{
			nCategorySettingsEnableID=REMINDER_APPSERVER_ENABLED;
			nCategorySettingsDataID=REMINDER_APPSERVER_SETTINGS;
		}
		break;
	default:
		Ret_pStatus.setError(StatusCodeSet::ERR_SYSTEM_FAILED_TO_CREATE_REMINDER,"Invalid category");
	}


	//extract settings data or create default:
	int nRow=lstSettings.find("BOUS_SETTING_ID",nCategorySettingsEnableID,true);
	if (nRow<0) //create default settings
	{
		bIsAllowedToRemind=true;
		ReminderHelperCore::CreateDefaultCategorySettings(RetSettings);
	}
	else
	{
		bIsAllowedToRemind=lstSettings.getDataRef(nRow,"BOUS_VALUE").toBool();
		if (bIsAllowedToRemind)
		{
			nRow=lstSettings.find("BOUS_SETTING_ID",nCategorySettingsDataID,true);
			if (nRow<0) //create default settings
			{
				ReminderHelperCore::CreateDefaultCategorySettings(RetSettings);
			}
			else
			{
				QByteArray data=lstSettings.getDataRef(nRow,"BOUS_VALUE_BYTE").toByteArray();
				RetSettings=XmlUtil::ConvertByteArray2RecordSet_Fast(data);
				if (RetSettings.getRowCount()==0)
					ReminderHelperCore::CreateDefaultCategorySettings(RetSettings);
			}
		}
	}
}


//based on dueDate and settings, calculate next fire date: not needed if one-shoot event
QDateTime ServerReminderManager::CalculateReminderFireDateTime(DbRecordSet &RetSettings,QDateTime datDue)
{
	if (RetSettings.getRowCount()==0)
	{
		return QDateTime();
	}

	QDateTime datCurrent=QDateTime::currentDateTime();

	if (datCurrent>datDue)
	{
		int nOverDuePeriod=RetSettings.getDataRef(0,"REM_EVERY_N_AFTER").toInt();
		if (nOverDuePeriod<=0)
		{
			return QDateTime(); //no next event
		}
		int nSecondAfter=datDue.secsTo(datCurrent);
		if (nSecondAfter>=MAX_HR_AFTER_EVENT*60*60)
		{
			return QDateTime(); //no next event
		}

		int nNextEventOffsetSeconds=nOverDuePeriod*60+(nSecondAfter/(nOverDuePeriod*60))*nOverDuePeriod*60;
		return datDue.addSecs(nNextEventOffsetSeconds);
	}
	else
	{
		QDateTime datForFireOnce;
		int nFireOncePeriod=RetSettings.getDataRef(0,"REM_ONCE_BEFORE").toInt();
		if (nFireOncePeriod>0)
				datForFireOnce=datDue.addSecs(-nFireOncePeriod*60);
		if (datForFireOnce<datCurrent)
			datForFireOnce=QDateTime();

		QDateTime datForDuePeriod;
		int nDuePeriod=RetSettings.getDataRef(0,"REM_EVERY_N_BEFORE").toInt();
		int nSecondBefore=datCurrent.secsTo(datDue);
		if (nSecondBefore<MAX_HR_BEFORE_EVENT*60*60 && nDuePeriod>=0)
		{
			//divide and find:
			int nNextEventOffsetSeconds=(nSecondBefore/(nDuePeriod*60))*nDuePeriod*60;
			if (nNextEventOffsetSeconds>0)
				datForDuePeriod=datDue.addSecs(-nNextEventOffsetSeconds);
		}

		if (datForFireOnce.isValid() && datForDuePeriod.isValid())
		{
			if (datForDuePeriod>datForFireOnce)
				return datForFireOnce;
			else
				return datForDuePeriod;
		}
		else if (datForFireOnce.isValid())
			return datForFireOnce;
		else if (datForDuePeriod.isValid())
			return datForDuePeriod;
		else
			return datDue; //next event: is at due date.
	}


	//if datDue in past->check for overdue, else check for before fire

	return QDateTime();
}


//send reminder by internal system:
void ServerReminderManager::SendReminder_BySokrates(Status &Ret_pStatus,int nPersonID,const DbRecordSet &rowReminder)
{
	QStringList lstPersons;
	lstPersons<<QString::number(nPersonID);
	QString strMsg=rowReminder.getDataRef(0,"BRE_ID").toString();

	QStringList lstSockets=g_UserSessionManager->GetSocketListForPersonID(nPersonID);
	if (lstSockets.size()==0)
	{
		Ret_pStatus.setError(1); //user not logged, return that 
		return;
	}

	//send reminder id,nothing less, nothing more:
	g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_SILENT_SERVER_NOTIFICATION,StatusCodeSet::MSG_BUS_NEW_REMINDER_NOTIFICATION,strMsg,&lstPersons,-1,-1,QDateTime::currentDateTime());
}