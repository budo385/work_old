#update all sources from svn
cd ../../
svn up
cd exe/appserver_tarantula
chmod +x ./compile.sh

#compile common
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/common/common/Makefile ../../lib/common/common/common.pro
make -C ../../lib/common/common/ clean all  || exit 1
cp ../../lib/debug/libcommon.a  ./ || exit 1

#compile trans
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/trans/trans/Makefile ../../lib/trans/trans/trans.pro
make -C ../../lib/trans/trans/ clean all  || exit 1
cp ../../lib/debug/libtrans.a  ./ || exit 1

#compile db
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/db/db/Makefile ../../lib/db/db/db.pro
make -C ../../lib/db/db/ clean all  || exit 1
cp ../../lib/debug/libdb.a  ./ || exit 1

#compile db_core
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/db_core/db_core/Makefile ../../lib/db_core/db_core/db_core.pro
make -C ../../lib/db_core/db_core/ clean all  || exit 1
cp ../../lib/debug/libdb_core.a  ./ || exit 1

#final compilation
qmake -makefile -macx -spec macx-g++ -Wall -o Makefile ./appserver_tarantula.pro
make
