#include "garbagecollector.h"
#include "common/common/threadid.h"

#include "common/common/logger.h"
extern Logger g_Logger;

#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;

GarbageCollector::GarbageCollector(HTTPServer *pHttpServer)
{
	m_HttpServer=pHttpServer;
}

GarbageCollector::~GarbageCollector()
{

}


//start every X minutes
void GarbageCollector::ExecuteTask(int nCallerID,int nTaskID, QString strData)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());

	//every 10 minutes, clean up dead James entries from the DB 
	g_ServiceHandler->CleanupJamesServerList();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Garbage Collector Ended");
}


