#ifndef SERVICEHANDLER_H
#define SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "db/db/dbsqlquery.h"

class ServiceHandler
{
public:
	void SystemRegister(Status &status, int nProgramCode, int nVersionMajor, int nVersionMinor, int nVersionRevision, QString strPlatformName, QString	strPlatformUniqueID, QString &Ret_strClientUniqueID, QString strHostUniqueID, QString &Ret_strHostIP, int &Ret_nPublicPort, int nStatsNatProtocol, int &Ret_nStatusCode, QString &Ret_strStatusMsg, int &RetOut_IsSSL, QString &Ret_strHostLocalIP, int &Ret_nIPForced);
	void SystemUnregister(Status &status, QString &strClientUniqueID, int &Ret_nStatusCode, QString &Ret_strStatusMsg);
	void TestConnectivity(Status &status, QString strHostUniqueID, QString &Ret_strHostIP, int &Ret_nPublicPort, int &Ret_nStatusCode, QString &Ret_strStatusMsg);
	void GetLocalServers(Status &status, QString &strClientUniqueID, int nProgramCode, DbRecordSet &Ret_recLocalServers, int &Ret_nStatusCode, QString &Ret_strStatusMsg);

	static bool ResolveJamesFromID(Status &status, DbSqlQuery &query, int &nJamesProgramCode, QString strHostUniqueID, QString &Ret_strHostIP, int &Ret_nPublicPort, QString &Ret_strStatusMsg, int &Ret_nStatusCode, int &RetOut_IsSSL, QString &Ret_strHostLocalIP, int &Ret_nNATType, int &Ret_nIPForced);

	//internal method
	static void CleanupJamesServerList();

private:
	void StoreNewClient(Status &status, DbSqlQuery &query, int nProgramCode, int nVersionMajor, int nVersionMinor, int nVersionRevision, QString strPlatformName, QString strPlatformUniqueID, QString &Ret_strClientUniqueID, int &nClientDBId, QString strIP, int nPort, QString strLocalIP, int nNatStats, bool bIsSsl, bool bIPForced, bool bCustomIDRequest = false);
	static void UpdateClientCacheFields(Status &status, DbSqlQuery &query, int nProgramCode, QString strHostUniqueID, QString strHostIP, int nPublicPort, bool bIsSSL, QString strHostLocalIP, int nNATType, int nIPForced, QString &strErrText, int &nErrCode);
	void GenerateUniqueID(DbSqlQuery &query, QString &strUID);
	static bool TestConnectivity(QString strHost, int nPort);
};

#endif // SERVICEHANDLER_H
