#ifndef SERVERINIFILE_H
#define SERVERINIFILE_H

#include "common/common/inifile.h"
#include <QFileInfo>



/*!
	\class ServerIniFile
	\brief INI file reader for app. server
	\ingroup AppServer

	Application server INI file reader


*/
class ServerIniFile
{
public:
	ServerIniFile();
	~ServerIniFile();

	ServerIniFile(const ServerIniFile &other){ operator = (other); }
	
	void operator = (const ServerIniFile &other)
	{
		if(this != &other)
		{
			m_strAppServerIPAddress		= other.m_strAppServerIPAddress;
			m_nAppServerPort			= other.m_nAppServerPort;		
			m_nMaxConnections			= other.m_nMaxConnections;		
			m_strServiceDescriptor		= other.m_strServiceDescriptor;
			m_nAutoStartService			= other.m_nAutoStartService;
			m_strDBConnectionName		= other.m_strDBConnectionName;

			m_nSSLMode					= other.m_nSSLMode;				
			m_strSSLCertificateDir		= other.m_strSSLCertificateDir;	

			m_nLogLevel					= other.m_nLogLevel;
			m_nLogMaxSize				= other.m_nLogMaxSize;
			m_nLogBufferSize			= other.m_nLogBufferSize;
/*
			m_nBackupFreq				= other.m_nBackupFreq;
			m_strBackupPath				= other.m_strBackupPath;
			m_datBackupLastDate			= other.m_datBackupLastDate;
			m_strRestoreOnNextStart		= other.m_strRestoreOnNextStart;
			m_strBackupTime				= other.m_strBackupTime;
			m_nBackupDay				= other.m_nBackupDay;
*/
		}
	}
	
	void Clear();
	bool Load(QString strFile, bool bCreateIfNotExists=true);
	bool Save(QString strFile);

public:

	//data stored in the INI file
	QString m_strAppServerIPAddress;	// IP address bind (def is 0.0.0.0)
	int		m_nAppServerPort;			// port number
	int		m_nMaxConnections;			// maximum connections that app. server will accept
	QString	m_strServiceDescriptor;		// app name
	int		m_nAutoStartService;		//
	QString m_strDBConnectionName;		// NAME OF DB CONN

	//SSL
	int		m_nSSLMode;					// accept only SSL(https) connections
	QString m_strSSLCertificateDir;		// path to the SSL certificates files (server.cert & server.pkey)

	//LOGGER
	int m_nLogLevel;					
	int m_nLogMaxSize;					
	int m_nLogBufferSize;				
		
	//BACKUP:
	int m_nBackupFreq;		
	QString m_strBackupPath;		
	QString m_datBackupLastDate;		
	QString m_strRestoreOnNextStart;		
	QString m_strBackupTime;
	int m_nBackupDay;

	
protected:
	IniFile m_objIni;
	bool CheckValues();
	bool SaveDefaults(QString strFile);

};

#endif //SERVERINIFILE_H