#ifndef RESTHTTPSERVER_LOCAL_H
#define RESTHTTPSERVER_LOCAL_H

#include "trans/trans/resthttpserver.h"

class RestHTTPServer_Local :public RestHTTPServer
{

public:
	RestHTTPServer_Local(RpcSkeleton *pServerSkeletonSet):RestHTTPServer(pServerSkeletonSet){};

protected:

	bool	AuthenticateUser(Status &err,HTTPContext &ctx,HTTPRequest &request,HTTPResponse &response, int &nClientTimeZoneOffsetMinutes);	
	void	LockThreadActive();
	void	UnLockThreadActive();

};

#endif // RESTHTTPSERVER_LOCAL_H
