#ifndef HTTPINVITEHANDLER_H
#define HTTPINVITEHANDLER_H

#include "trans/trans/httphandler.h"

class HttpInviteHandler : public HTTPHandler
{

public:
	HttpInviteHandler();
	~HttpInviteHandler();

	void HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);

private:

	void GenerateErrorResponse(HTTPResponse &response,QString strError);
	void GenerateRedirectResponse(HTTPResponse &response,QString strURL, bool bSkipRedirect=false);
	void GenerateBookMarkBody(HTTPResponse &response,QString strURL);

	
};

#endif // HTTPINVITEHANDLER_H
