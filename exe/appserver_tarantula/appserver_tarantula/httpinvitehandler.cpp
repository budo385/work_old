
#include "httpinvitehandler.h"
#include "trans/trans/config_trans.h"
#include "common/common/datetimehandler.h"
#include "common/common/config_version_ffh.h"
#include "db/db/dbsqlquery.h"
#include "servicehandler.h"
#include "common/common/logger.h"

extern DbSqlManager	*g_DbManager;
extern Logger g_Logger;

HttpInviteHandler::HttpInviteHandler()
	: HTTPHandler()
{
	SetDomain("invite");
}

HttpInviteHandler::~HttpInviteHandler()
{
}

//request is like: http://tarantula/invite/id_james/web/invite.html?catalog=(percent encoded)
//output is: http://james_ip:port/web/invite.html?catalog=(percent encoded)

//NEW: process bookmark request like: http://tarantula/invite/id_james/bookmark/invite.html?catalog=(percent encoded)
void HttpInviteHandler::HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("HttpInviteHandler: START. Thread: %1").arg(QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()));

	QString strPath=request.path();

	if (!strPath.contains("web/") && !strPath.contains("iphone/"))
	{
		strPath += "/iphone/index.html";
	}

	QStringList lstParts=strPath.split("/",QString::SkipEmptyParts);

	if (lstParts.size()<4 || lstParts.size()>5)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("HttpInviteHandler: Error - Invalid Request. Path: %1").arg(strPath));
		GenerateErrorResponse(response,"Invalid Request");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		return;
	}

	QString strJamesID=lstParts.at(1);

	//MIRO: find james ip:
	DbSqlQuery query(status, g_DbManager);
	if(!status.IsOK()){ 
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("HttpInviteHandler: Error - Database error. Path: %1").arg(strPath));
		GenerateErrorResponse(response,"Database error");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		return;
	}
	QString Ret_strHostIP;
	int Ret_nPublicPort;
	QString Ret_strStatusMsg;
	int Ret_nStatusCode;
	int RetOut_IsSSL;
	int nNatType;
	QString Ret_strLocalIP;
	int Ret_nIPForced;
	int nJamesProgramCode = SERVER_CODE_JAMES;
	if(!ServiceHandler::ResolveJamesFromID(status, query, nJamesProgramCode, strJamesID, Ret_strHostIP, Ret_nPublicPort, Ret_strStatusMsg, Ret_nStatusCode, RetOut_IsSSL, Ret_strLocalIP, nNatType, Ret_nIPForced))
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("HttpInviteHandler: Error - Server lookup error (not found or down). Path: %1").arg(strPath));
		GenerateErrorResponse(response,"Server lookup error (not found or down)");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		return;
	}

	lstParts.removeFirst();
	lstParts.removeFirst();
	bool bBookmark=false;
	if (lstParts.contains("bookmark"))
	{
		bBookmark=true;
		lstParts.removeFirst();
	}

	QString strURLPart=lstParts.join("/");

	//should we use external or local IP
	QString strIP = Ret_strHostIP;
	if(Ret_strHostIP == ctx.m_strPeerIP)	//external James IP equal to external Lucy IP
		if(0 == Ret_nIPForced)				//do not force public IP
			strIP = Ret_strLocalIP;

	//MIRO: metni pravi james ip u url i prosljedi, a ovo moje comentiraj..
	//output is: http://james_ip:port/iphone/catalog_invite.html?catalog=(percent encoded)

	if (strURLPart.contains("?"))
		strURLPart=strURLPart+"&server_id="+strJamesID;
	else
		strURLPart=strURLPart+"?server_id="+strJamesID;
	
	QString strJamesURL;
	if(RetOut_IsSSL)
		strJamesURL=QString("https://%1:%2/%3").arg(strIP).arg(Ret_nPublicPort).arg(strURLPart);
	else
		strJamesURL=QString("http://%1:%2/%3").arg(strIP).arg(Ret_nPublicPort).arg(strURLPart);
	//QString strJamesURL="http://192.168.200.18:10001/"+strURLPart;

	GenerateRedirectResponse(response,strJamesURL,bBookmark);

	//BT: added 07.02.2011: new MB request: skip bookmark page, only on request
	if (bBookmark)
		GenerateBookMarkBody(response,strJamesURL);


	status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("HttpInviteHandler: SUCCESS END. Path: %1 Redirected to %2").arg(strPath).arg(strJamesURL));
}

void HttpInviteHandler::GenerateErrorResponse(HTTPResponse &response,QString strError)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(500,"Internal Server Error");
	response.setValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setContentType("text/plain");
	response.GetBody()->append(strError);
}

void HttpInviteHandler::GenerateRedirectResponse(HTTPResponse &response,QString strURL, bool bSkipRedirect)
{
	//set response header, content length & compression is done in htttp server
	//response.setStatusLine(500,"Internal Server Error");
	response.setStatusLine(200,"OK");
	response.setValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));

	//prevent caching of this data on the client side
	//response.setValue("Pragma","no-cache"); //;MP noticed bug: probably client is caching by default on session level: instruct to bypass cache
	response.setValue("Expires","Sat, 26 Jul 1997 05:00:00 GMT"); //;MP noticed bug: probably client is caching by default on session level: instruct to bypass cache

	response.setContentType("text/html");

	if (!bSkipRedirect)
	{
		QString strBody;
		strBody="<html><head><meta http-equiv=\"Refresh\" content=\"0;url="+strURL+"\"></head><body>Redirecting to the server...</body></html>";
		response.GetBody()->append(strBody);
	}
}

void HttpInviteHandler::GenerateBookMarkBody(HTTPResponse &response,QString strURL)
{
	static QByteArray strTemplateContent;
	if(strTemplateContent.isEmpty())
	{
		//read template file to string (once in app lifetime - see static above)
		QString strTemplatePath = QCoreApplication::applicationDirPath() + "/settings/bookmark.html";
		QFile file(strTemplatePath);
		if(file.open(QIODevice::ReadOnly)){
			strTemplateContent = file.readAll();
			file.close();
		}
		else
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR, 1, QString("Failed to open template: %1").arg(strTemplatePath));
	}

	QString strBody;
	if(strTemplateContent.isEmpty())
	{
		//template file not found, use old BODY
		strBody="<html><head><meta http-equiv=\"Refresh\" content=\"0;url="+strURL+"\"></head><body>Redirecting to the server...</body></html>";
	}
	else
	{
		//instantiate body from template file
		strBody=strTemplateContent;
		strBody.replace("[james_url]", strURL);
	}

	response.GetBody()->append(strBody);
}
