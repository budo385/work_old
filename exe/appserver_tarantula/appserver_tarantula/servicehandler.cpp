#include "servicehandler.h"
#include "common/common/config_version_ffh.h"
#include <stdlib.h>
#include <time.h>
#include "common/common/logger.h"
#include "applicationserver.h"
#include "trans/trans/tcphelper.h"

extern DbSqlManager	*g_DbManager;
extern ApplicationServer *g_AppServer;
extern Logger g_Logger;

//TOFIX to be used
//"ACCESS_CALL" values
#define ACCESS_CALL_REGISTER	1
#define ACCESS_CALL_UNREGISTER	2

//"HOST_STATS_NAT_USED" values
#define STATS_NAT_NONE			0
#define STATS_NAT_NATPMP		1
#define STATS_NAT_UPNP			2

//TOFIX specify all error codes here


void ServiceHandler::SystemRegister(Status &status, int nProgramCode, int nVersionMajor, int nVersionMinor, int nVersionRevision, QString strPlatformName, QString	strPlatformUniqueID, QString &Ret_strClientUniqueID, QString strHostUniqueID, QString &Ret_strHostIP, int &Ret_nPublicPort, int nStatsNatProtocol, int &Ret_nStatusCode, QString &Ret_strStatusMsg, int &RetOut_IsSSL, QString &Ret_strHostLocalIP, int &Ret_nIPForced)
{
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Enter register call (program code: %1; UID: %2)").arg(nProgramCode).arg(Ret_strClientUniqueID));

	//prepare a single message per API call with all the important summary info (needed for logger)
	QString strInfoMsg;
	HTTPContext ctx=g_AppServer->GetThreadContext(ThreadIdentificator::GetCurrentThreadID());
	strInfoMsg += "|Call=Register";
	strInfoMsg += "|ClientIP=" + ctx.m_strPeerIP;	//TOFIX? Ret_strHostIP
	strInfoMsg += QString("|ClientPort=%1").arg((Ret_nPublicPort != 0) ? Ret_nPublicPort : ctx.m_nPeerPort);
	strInfoMsg += QString("|ClientCode=%1").arg(nProgramCode);
	strInfoMsg += QString("|ClientVersion=%1.%2.%3").arg(nVersionMajor).arg(nVersionMinor).arg(nVersionRevision);
	strInfoMsg += "|ServerID=" + strHostUniqueID;
	strInfoMsg += QString("|ClientNatStats=%1").arg(nStatsNatProtocol);
	strInfoMsg += QString("|Date=%1").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss.zzz"));
	strInfoMsg += "|ClientPlatform=" + strPlatformName;
	strInfoMsg += "|ClientPlatformUID=" + strPlatformUniqueID;

	//STEP 1: test for validity of data and validity of license
	if( SERVER_CODE_TARANTULA == nProgramCode ){
		status.setError(1, "Invalid Parameters");
		return;
	}

	//init result variables
	Ret_nStatusCode = 0;	//OK
	Ret_strStatusMsg = "";	//OK
	
	int nClientDBId = -1;
	DbSqlQuery query(status, g_DbManager);
	if(!status.IsOK()){
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, status.getErrorText());
		return;	//TOFIX error code
	}

	//generate new ID if needed
	if(Ret_strClientUniqueID.isEmpty())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: generate new client ID");

		StoreNewClient(status, query, nProgramCode, nVersionMajor, nVersionMinor, nVersionRevision, strPlatformName, strPlatformUniqueID, Ret_strClientUniqueID, nClientDBId, ctx.m_strPeerIP, Ret_nPublicPort, Ret_strHostLocalIP, nStatsNatProtocol, RetOut_IsSSL, Ret_nIPForced);
		if (!status.IsOK()){
			//error inserting new client into DB!
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Error storing user info!");
			Ret_nStatusCode = 4;
			Ret_strStatusMsg = "Error storing user info!";

			//dump status message
			strInfoMsg += "|CallStatusMsg=" + Ret_strStatusMsg;
			strInfoMsg += QString("|CallStatusCode=%1").arg(Ret_nStatusCode);
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
			return;
		}

		strInfoMsg += "|ClientID=" + Ret_strClientUniqueID;
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Register call: generate new client ID done (%1)").arg(Ret_strClientUniqueID));
	}
	else
	{
		strInfoMsg += "|ClientID=" + Ret_strClientUniqueID;
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: search existing client by ID");

		//detect if the client ID is inside of square brackets
		bool bCustomIDRequest = false;
		if(Ret_strClientUniqueID.at(0) == '['){
			Ret_strClientUniqueID.chop(1);		//strip ending bracket
			Ret_strClientUniqueID.remove(0, 1);	//strip start bracket
			bCustomIDRequest = true;
		}

		//search database using supplied client ID
		QString strSql = "SELECT CLI_ID, CLI_PROGRAM_CODE, CLI_PROGRAM_VERSION_MAJOR, CLI_PROGRAM_VERSION_MINOR, CLI_PROGRAM_VERSION_REVISION, CLI_PROGRAM_PLATFORM_NAME, CLI_PROGRAM_PLATFORM_UID FROM TARANTULA_CLIENT WHERE CLI_CLIENT_IDENTITY=?";
		query.Prepare(status, strSql);
		if (status.IsOK()){
			query.bindValue(0, Ret_strClientUniqueID);
			query.ExecutePrepared(status);
			DbRecordSet lstRes;
			query.FetchData(lstRes);

			if(lstRes.getRowCount() < 1){
				//TOFIX error, could not find the client record, generate new ID !?
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Error: could not find user with given UID! Will recreate new ID!");

				StoreNewClient(status, query, nProgramCode, nVersionMajor, nVersionMinor, nVersionRevision, strPlatformName, strPlatformUniqueID, Ret_strClientUniqueID, nClientDBId, ctx.m_strPeerIP, Ret_nPublicPort, Ret_strHostLocalIP, nStatsNatProtocol, RetOut_IsSSL, Ret_nIPForced, bCustomIDRequest);
				if (!status.IsOK()){
					//error inserting new client into DB!
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Error storing user info!");
					Ret_nStatusCode = 4;
					Ret_strStatusMsg = "Error storing user info!";

					//dump status message
					strInfoMsg += "|CallStatusMsg=" + Ret_strStatusMsg;
					strInfoMsg += QString("|CallStatusCode=%1").arg(Ret_nStatusCode);
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
					return;
				}
			}
			else
			{
				nClientDBId = lstRes.getDataRef(0, "CLI_ID").toInt();
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Register call: search existing client by ID: found (PK=%1)").arg(nClientDBId));
			}
		}
		else{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Register call: search existing client by ID (ERROR: %1)").arg(status.getErrorText()));
			
			strInfoMsg += "|CallStatusMsg=" + status.getErrorCode();
			strInfoMsg += QString("|CallStatusCode=%1").arg(status.getErrorText());
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
			return;
		}

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: search existing client by ID done");
	}
	
	QString strLocalIP = Ret_strHostLocalIP;

	if(SERVER_CODE_JAMES == nProgramCode || SERVER_CODE_JOHN == nProgramCode)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: test James/John connectivity");

		//if James, send public IP, test connectivity
		Ret_strHostIP = ctx.m_strPeerIP;

		//update the cache in case something changed (server switched SSL from on to off)
		QString strErrText;
		int nErrCode = 0;
		UpdateClientCacheFields(status, query, nProgramCode, Ret_strClientUniqueID, Ret_strHostIP, Ret_nPublicPort, RetOut_IsSSL, Ret_strHostLocalIP, nStatsNatProtocol, Ret_nIPForced, strErrText, nErrCode);

		strInfoMsg += "|ServerIP=" + Ret_strHostIP;
		strInfoMsg += QString("|ServerPort=%1").arg(Ret_nPublicPort);
		strInfoMsg += QString("|ServerIsSSL=%1").arg(RetOut_IsSSL);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Register call: James/John IP:%1, Resolved IP:%2, Port:%3").arg(ctx.m_strPeerIP).arg(Ret_strHostIP).arg(Ret_nPublicPort));

		if(!TestConnectivity(ctx.m_strPeerIP, Ret_nPublicPort)){
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: test James/John connectivity failed");

			//error, James is not accessible
			Ret_nStatusCode = 2; //james must resolve if port is not accessble: by code=2
			Ret_strStatusMsg = "Server not accessible!";

			//dump status message
			strInfoMsg += "|CallStatusMsg=" + Ret_strStatusMsg;
			strInfoMsg += QString("|CallStatusCode=%1").arg(Ret_nStatusCode);
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
			return;
		}

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: test James/John connectivity done");
	}
	else{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Register call: client request, resolve James/John (ID:%1) for Lucy/Sara (ID:%2)").arg(strHostUniqueID).arg(Ret_strClientUniqueID));

		int nNatType = 0;
		int nIPForced = 0;
		int nJamesProgramCode = 0;
		if(!ResolveJamesFromID(status, query, nJamesProgramCode, strHostUniqueID, Ret_strHostIP, Ret_nPublicPort, Ret_strStatusMsg, Ret_nStatusCode, RetOut_IsSSL, Ret_strHostLocalIP, nNatType, nIPForced))
		{
			//dump status message
			strInfoMsg += "|CallStatusMsg=" + Ret_strStatusMsg;
			strInfoMsg += QString("|CallStatusCode=%1").arg(Ret_nStatusCode);
			
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
			return;
		}

		//now determine if the client is in the same network as James
		if(Ret_strHostIP == ctx.m_strPeerIP)	//external James IP equal to external Lucy IP
		{
			if(0 != nIPForced)					//force public IP, no need for local IP
				Ret_strHostLocalIP = "";
		}
		else
			Ret_strHostLocalIP = "";

		strInfoMsg += "|ServerIP=" + Ret_strHostIP;
		strInfoMsg += "|ServerLocalIP=" + Ret_strHostLocalIP;
		strInfoMsg += QString("|ServerPort=%1").arg(Ret_nPublicPort);
		strInfoMsg += QString("|ServerIsSSL=%1").arg(RetOut_IsSSL);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: client request, resolve James/John for Lucy/Sara - done");
	}

	//update the cache for future lookup
	QString strErrText;
	int nErrCode = 0;
	UpdateClientCacheFields(status, query, nProgramCode, Ret_strClientUniqueID, Ret_strHostIP, Ret_nPublicPort, RetOut_IsSSL, Ret_strHostLocalIP, nStatsNatProtocol, Ret_nIPForced, strErrText, nErrCode);

	//dump status message
	strInfoMsg += "|CallStatusMsg=" + strErrText;
	strInfoMsg += QString("|CallStatusCode=%1").arg(nErrCode);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
}

void ServiceHandler::GenerateUniqueID(DbSqlQuery &query, QString &strUID)
{
	//MB: 5 consonant leters + 5 digits
	const char *szLetters = "bcdfghklmnpqrstvwxyz";	//20 consonant letters

	const int nMAX_TRIES = 50;
	int nTries = 0;
	bool bUnique = false;
	int nSeed = time(NULL);
	while(!bUnique)
	{
		nTries ++;
		if(nTries > nMAX_TRIES){
			strUID = "";	//error
			break;
		}

		srand(nSeed);

		char szBuffer[11];
		szBuffer[0]  = szLetters[ rand()%20 ];
		szBuffer[1]  = szLetters[ rand()%20 ];
		szBuffer[2]  = szLetters[ rand()%20 ];
		szBuffer[3]  = szLetters[ rand()%20 ];
		szBuffer[4]  = szLetters[ rand()%20 ];
		
		szBuffer[5]  = '0' + rand()%10;
		szBuffer[6]  = '0' + rand()%10;
		szBuffer[7]  = '0' + rand()%10;
		szBuffer[8]  = '0' + rand()%10;
		szBuffer[9]  = '0' + rand()%10;

		szBuffer[10] = '\0';
		
		strUID = szBuffer;

		//check if the ID exists in the database
		QString strSql = "SELECT count(*) FROM TARANTULA_CLIENT WHERE CLI_CLIENT_IDENTITY=?";
		Status status;
		query.Prepare(status, strSql);
		if (status.IsOK()){
			query.bindValue(0, strUID);
			query.ExecutePrepared(status);
			DbRecordSet lstRes;
			query.FetchData(lstRes);

			if(lstRes.getRowCount() > 0){
				//data exist
				int nCount = lstRes.getDataRef(0, 0).toInt();
				if(nCount < 1)
					bUnique = true;	//can exit the loop
			}
			//else log DB error
		}

		//update seed
		nSeed += rand() + 20;
	}
}

void ServiceHandler::SystemUnregister(Status &status, QString &strClientUniqueID, int &Ret_nStatusCode, QString &Ret_strStatusMsg)
{
	//init result variables
	Ret_nStatusCode = 0;	//OK
	Ret_strStatusMsg = "";	//OK

	//prepare a single message per API call with all the important summary info (needed for logger)
	QString strInfoMsg;
	HTTPContext ctx=g_AppServer->GetThreadContext(ThreadIdentificator::GetCurrentThreadID());
	strInfoMsg += "|Call=Unregister";
	strInfoMsg += "|ClientIP=" + ctx.m_strPeerIP;
	strInfoMsg += QString("|ClientPort=%1").arg(ctx.m_nPeerPort);
	strInfoMsg += QString("|ClientID=%1").arg(strClientUniqueID);
	strInfoMsg += QString("|Date=%1").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss.zzz"));

	int nClientDBId = -1;
	DbSqlQuery query(status, g_DbManager);
	if(!status.IsOK()){
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, status.getErrorText());

		//dump status message
		strInfoMsg += "|CallStatusMsg=" + status.getErrorCode();
		strInfoMsg += QString("|CallStatusCode=%1").arg(status.getErrorText());
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
		return;
	}
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Unregister call: begin");

	//update database using supplied client ID
	QString strErrText;
	int nErrCode = 0;
	UpdateClientCacheFields(status, query, -1, strClientUniqueID, "", 0, 0, "", 0, 0, strErrText, nErrCode);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Unregister call: end");

	//dump status message
	strInfoMsg += "|CallStatusMsg=OK";
	strInfoMsg += QString("|CallStatusCode=0");
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
}

bool ServiceHandler::TestConnectivity(QString strHost, int nPort)
{
	QTcpSocket socket;
    socket.connectToHost(strHost, nPort);
    socket.waitForConnected(10000); //10sec
    return (socket.state() == QTcpSocket::ConnectedState);
}

void ServiceHandler::TestConnectivity(Status &status, QString strHostUniqueID, QString &Ret_strHostIP, int &Ret_nPublicPort, int &Ret_nStatusCode, QString &Ret_strStatusMsg)
{
	HTTPContext ctx = g_AppServer->GetHttpServer().GetThreadContext(ThreadIdentificator::GetCurrentThreadID());

	//init result variables
	Ret_nStatusCode = 0;	//OK
	Ret_strStatusMsg = "";	//OK

	//Ret_strHostIP = ctx.m_strPeerIP;
	Ret_strHostIP =TcpHelper::ResolveLocalHostURLService(ctx.m_strPeerIP,Ret_nPublicPort); //BT: localhost can init SSL, I need true IP address
	if(!TestConnectivity(ctx.m_strPeerIP, Ret_nPublicPort)){
		//error, James is not accessible
		Ret_nStatusCode = 2;
		Ret_strStatusMsg = "Server not accessible!";
	}
}

void ServiceHandler::StoreNewClient(Status &status, DbSqlQuery &query, int nProgramCode, int nVersionMajor, int nVersionMinor, int nVersionRevision, QString strPlatformName, QString strPlatformUniqueID, QString &Ret_strClientUniqueID, int &nClientDBId, QString strIP, int nPort, QString strLocalIP, int nNatStats, bool bIsSsl, bool bIPForced, bool bCustomIDRequest)
{
	if(!bCustomIDRequest)
		GenerateUniqueID(query, Ret_strClientUniqueID);

	//insert the new client into DB
	QString strSql = "INSERT INTO TARANTULA_CLIENT (CLI_PROGRAM_CODE, CLI_PROGRAM_VERSION_MAJOR, CLI_PROGRAM_VERSION_MINOR, CLI_PROGRAM_VERSION_REVISION, CLI_PROGRAM_PLATFORM_NAME, CLI_PROGRAM_PLATFORM_UID, CLI_CLIENT_IDENTITY, CLI_HOST_URL, CLI_HOST_PORT, CLI_HOST_LOCAL_IP, CLI_HOST_STATS_NAT_USED, CLI_HOST_IS_SSL, CLI_HOST_IP_FORCED) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	query.Prepare(status, strSql);
	if (status.IsOK()){
		query.bindValue(0, nProgramCode);
		query.bindValue(1, nVersionMajor);
		query.bindValue(2, nVersionMinor);
		query.bindValue(3, nVersionRevision);
		query.bindValue(4, strPlatformName);
		query.bindValue(5, strPlatformUniqueID);
		query.bindValue(6, Ret_strClientUniqueID);
		//new cache fields below
		query.bindValue(7, strIP);
		query.bindValue(8, nPort);
		query.bindValue(9, strLocalIP);
		query.bindValue(10, nNatStats);
		query.bindValue(11, bIsSsl);
		query.bindValue(12, bIPForced);

		query.ExecutePrepared(status);

		nClientDBId = query.GetLastInsertedID(status, "CLI_ID");
	}
}

bool ServiceHandler::ResolveJamesFromID(Status &status, DbSqlQuery &query, int &nJamesProgramCode, QString strHostUniqueID, QString &Ret_strHostIP, int &Ret_nPublicPort, QString &Ret_strStatusMsg, int &Ret_nStatusCode, int &RetOut_IsSSL, QString &Ret_strHostLocalIP, int &Ret_nNATType, int &Ret_nIPForced)
{
	//FIRST try to find the data using faster lookup, using the TARANTULA_CLIENT cache fields
	//search database using supplied client ID
	//new fields added to cache data from the access log for system speedup
	QString strSql = "SELECT CLI_HOST_URL, CLI_HOST_PORT, CLI_HOST_LOCAL_IP, CLI_HOST_STATS_NAT_USED, CLI_HOST_IS_SSL, CLI_HOST_IP_FORCED, CLI_PROGRAM_CODE FROM TARANTULA_CLIENT WHERE CLI_CLIENT_IDENTITY=?";
	query.Prepare(status, strSql);
	if (status.IsOK()){
		query.bindValue(0, strHostUniqueID);
		query.ExecutePrepared(status);
		DbRecordSet lstRes;
		query.FetchData(lstRes);

		if(lstRes.getRowCount() > 0){
			if(!lstRes.getDataRef(0, "CLI_HOST_URL").isNull()){
				//fill return data
				Ret_strHostIP = lstRes.getDataRef(0, "CLI_HOST_URL").toString();
				Ret_nPublicPort = lstRes.getDataRef(0, "CLI_HOST_PORT").toInt();
				RetOut_IsSSL = lstRes.getDataRef(0, "CLI_HOST_IS_SSL").toInt(); 
				Ret_strHostLocalIP = lstRes.getDataRef(0, "CLI_HOST_LOCAL_IP").toString();
				Ret_nNATType = lstRes.getDataRef(0, "CLI_HOST_STATS_NAT_USED").toInt();		//INTEGER: 0-NONE, 1-NATPMP, 2-UPNP
				Ret_nIPForced = lstRes.getDataRef(0, "CLI_HOST_IP_FORCED").toInt();		//INTEGER: 0-NO, 1-SI TORO
				nJamesProgramCode = lstRes.getDataRef(0, "CLI_PROGRAM_CODE").toInt();

				//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, 1, QString("SQL [%1]\nID: to fetch cache fields: CLI_HOST_URL=%2, CLI_CLIENT_IDENTITY=%3").arg(strSql).arg(Ret_strHostIP).arg(strHostUniqueID));

				//check if last ACCESS_CALL is 2 (unregister):
				//was the last record the "UNREGISTERED" call (in that case the host is empty)
				if(Ret_strHostIP.isEmpty())
				{
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: client request, resolve James/John for Lucy/Sara - resolved but down");

					Ret_nStatusCode = 6;
					Ret_strStatusMsg = "Server is currently down!";
					return false;
				}
				return true;
			}
		}
	}

	//some client, resolve requested James server from its unique ID (Program code 4 = "James")
	strSql = "SELECT FIRST 1 ACL_HOST_URL, ACL_HOST_PORT, ACL_ACCESS_CALL, ACL_DAT_LAST_MODIFIED, ACL_HOST_IS_SSL, ACL_HOST_LOCAL_IP, ACL_HOST_STATS_NAT_USED, ACL_HOST_IP_FORCED, CLI_PROGRAM_CODE FROM TARANTULA_ACCESS_LOG, TARANTULA_CLIENT WHERE CLI_CLIENT_IDENTITY=? AND CLI_ID=ACL_CLIENT_ID ORDER BY ACL_DAT_LAST_MODIFIED DESC";
	query.Prepare(status, strSql);
	if (status.IsOK()){
		query.bindValue(0, strHostUniqueID);
		query.ExecutePrepared(status);
		DbRecordSet lstRes;
		query.FetchData(lstRes);
		//_DUMP(lstRes);

		if(lstRes.getRowCount() < 1)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Register call: client request, resolve James/John (ID:%1) for Lucy/Sara - error, James/John not found").arg(strHostUniqueID));

			//error finding the matching James
			Ret_nStatusCode = 3;
			Ret_strStatusMsg = "Matching Server not found!";
			return false;
		}
		else
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Register call: client request, resolve James/John (ID:%1) for Lucy/Sara - resolved").arg(strHostUniqueID));

			//fill return data
			Ret_strHostIP = lstRes.getDataRef(0, "ACL_HOST_URL").toString();
			Ret_nPublicPort = lstRes.getDataRef(0, "ACL_HOST_PORT").toInt();
			RetOut_IsSSL = lstRes.getDataRef(0, "ACL_HOST_IS_SSL").toInt(); 
			Ret_strHostLocalIP = lstRes.getDataRef(0, "ACL_HOST_LOCAL_IP").toString();
			Ret_nNATType = lstRes.getDataRef(0, "ACL_HOST_STATS_NAT_USED").toInt();		//INTEGER: 0-NONE, 1-NATPMP, 2-UPNP
			Ret_nIPForced = lstRes.getDataRef(0, "ACL_HOST_IP_FORCED").toInt();		//INTEGER: 0-NO, 1-SI TORO
			nJamesProgramCode = lstRes.getDataRef(0, "CLI_PROGRAM_CODE").toInt();	

			//we got the data the hard way, now update the cache for future lookup
			QString strErrText;
			int nErrCode = 0;
			UpdateClientCacheFields(status, query, nJamesProgramCode, strHostUniqueID, Ret_strHostIP, Ret_nPublicPort, RetOut_IsSSL, Ret_strHostLocalIP, Ret_nNATType, Ret_nIPForced, strErrText, nErrCode);
			if(nErrCode != 0){
				//error finding the matching James
				Ret_nStatusCode = nErrCode;
				Ret_strStatusMsg = strErrText;
				return false;
			}

			//check if last ACCESS_CALL is 2 (unregister):
			//was the last record the "UNREGISTERED" call (in that case the host is NULL)
			if(Ret_strHostIP.isEmpty())
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Register call: client request, resolve James/John for Lucy/Sara - resolved but down");

				Ret_nStatusCode = 6;
				Ret_strStatusMsg = "Server is currently down!";
				return false;
			}
		}
	}
	else{
		//TOFIX error preparing Sql
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, status.getErrorText());
		return false;
	}

	return true;
}

void ServiceHandler::CleanupJamesServerList()
{
	Status status;
	DbSqlQuery query(status, g_DbManager);
	if(!status.IsOK()){
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, status.getErrorText());
		return;	//TOFIX error code
	}

	QDateTime date = QDateTime::currentDateTime();
	QDateTime date_Minus10min = date.addSecs(-600);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Garbage collector: before executing SELECT"));

	//FIX: working to remove the TARANTULA_ACCESS_LOG table
	/*
	//fetch all records where James did not contact us for at least 10 minutes 
	//(Program code 4 = "James", (10.0/1440.0)= 10min as days, ACL_ACCESS_CALL=1 -> last call was "register", never unregistered)
	QString strSql = "SELECT A.ACL_ID, A.ACL_CLIENT_ID, A.ACL_ACCESS_CALL, A.ACL_DAT_LAST_MODIFIED FROM \
		TARANTULA_CLIENT, \
		TARANTULA_ACCESS_LOG AS A, \
		(SELECT ACL_CLIENT_ID, MAX(ACL_DAT_LAST_MODIFIED) AS max_date FROM TARANTULA_ACCESS_LOG GROUP BY ACL_CLIENT_ID) AS B \
		WHERE \
		CLI_ID=A.ACL_CLIENT_ID AND \
		A.ACL_CLIENT_ID = B.ACL_CLIENT_ID AND \
		A.ACL_ACCESS_CALL=1 AND \
		A.ACL_DAT_LAST_MODIFIED = max_date AND \
		(CLI_PROGRAM_CODE=4 || CLI_PROGRAM_CODE=6) AND \
		A.ACL_DAT_LAST_MODIFIED < ?"; // < (CURRENT_TIMESTAMP-10.0/1440.0) does not work because our Qt layer fills ACL_DAT_LAST_MODIFIED with UTC time of the .exe process

	query.Prepare(status, strSql);
	if (status.IsOK()){
		query.bindValue(0, date_Minus10min);
		query.ExecutePrepared(status);
		DbRecordSet lstRes;
		query.FetchData(lstRes);

		int nCnt = lstRes.getRowCount();
		if(nCnt > 0)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Garbage collector: found %1 records, going insert").arg(nCnt));

			//_DUMP(lstRes);
			//return;
			for(int i=0; i<nCnt; i++)
			{
				//mark this entry as "unregistered" as of now
				int nClientID = lstRes.getDataRef(i, 1).toInt();

				strSql = "INSERT INTO TARANTULA_ACCESS_LOG (ACL_CLIENT_ID, ACL_ACCESS_CALL) VALUES (?, 2)";
				query.Prepare(status, strSql);
				if (!status.IsOK()){ 
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, status.getErrorText());
					return;
				}
				query.bindValue(0, nClientID);
				query.ExecutePrepared(status);
			}
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Garbage collector: cleaned up %1 stale James/John sessions").arg(nCnt));
		}
		else
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1, QString("Garbage collector: no stale James/John sessions found"));
	}
	else{
		//TOFIX error preparing Sql
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, status.getErrorText());
	}
	*/

	//TOFIX same for TARANTULA_CLIENT table?
}

void ServiceHandler::UpdateClientCacheFields(Status &status, DbSqlQuery &query, int nProgramCode, QString strHostUniqueID, QString strHostIP, int nPublicPort, bool bIsSSL, QString strHostLocalIP, int nNATType, int nIPForced, QString &strErrText, int &nErrCode)
{
	strErrText = "OK";
	nErrCode = 0;

	Q_ASSERT(!strHostUniqueID.isEmpty());

	QString strSql = "UPDATE TARANTULA_CLIENT SET CLI_HOST_URL=?, CLI_HOST_PORT=?, CLI_HOST_IS_SSL=?, CLI_HOST_LOCAL_IP=?, CLI_HOST_STATS_NAT_USED=?, CLI_HOST_IP_FORCED=? WHERE CLI_CLIENT_IDENTITY=?";
	query.Prepare(status, strSql);
	if (status.IsOK()){
		query.bindValue(0, strHostIP);
		query.bindValue(1, nPublicPort);
		query.bindValue(2, bIsSSL);
		query.bindValue(3, strHostLocalIP);
		query.bindValue(4, nNATType);
		query.bindValue(5, nIPForced);
		query.bindValue(6, strHostUniqueID);
		query.ExecutePrepared(status);

		if(!status.IsOK()){
			strErrText = status.getErrorText();
			nErrCode = status.getErrorCode();

			//check if update deadlock-ed and retry once ("Deadlock detected, update failed!")
			if(319 == status.getErrorCode())
			{
				query.Prepare(status, strSql);
				if (status.IsOK()){
					query.bindValue(0, strHostIP);
					query.bindValue(1, nPublicPort);
					query.bindValue(2, bIsSSL);
					query.bindValue(3, strHostLocalIP);
					query.bindValue(4, nNATType);
					query.bindValue(5, nIPForced);
					query.bindValue(6, strHostUniqueID);
					query.ExecutePrepared(status);

					if (status.IsOK()){
						strErrText = "OK";
						nErrCode = 0;
					}
				}
			}
		
			if (!status.IsOK()){
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, 1, QString("ERROR: SQL [%1]\nID: to update cache fields: CLI_HOST_URL=%2, CLI_CLIENT_IDENTITY=%3").arg(strSql).arg(strHostIP).arg(strHostUniqueID));
			}
			else
			{
				//FIX: update this field too because at one time James was acting as a John server	
				if(nProgramCode >= 0)
				{
					strSql = "UPDATE TARANTULA_CLIENT SET CLI_PROGRAM_CODE=? WHERE CLI_CLIENT_IDENTITY=?";
					query.Prepare(status, strSql);
					if (status.IsOK()){
						query.bindValue(0, nProgramCode);
						query.bindValue(1, strHostUniqueID);
						query.ExecutePrepared(status);
					}
				}
			}
		}
	}
	else
	{
		strErrText = status.getErrorText();
		nErrCode = status.getErrorCode();
	}

	if(0 != nErrCode)
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR, 1, QString("Failed to update cache fields: %1").arg(strErrText));
}

void ServiceHandler::GetLocalServers(Status &status, QString &strClientUniqueID, int nProgramCode, DbRecordSet &Ret_recLocalServers, int &Ret_nStatusCode, QString &Ret_strStatusMsg)
{
	//init result variables
	Ret_nStatusCode = 0;	//OK
	Ret_strStatusMsg = "";	//OK

	//prepare a single message per API call with all the important summary info (needed for logger)
	QString strInfoMsg;
	HTTPContext ctx=g_AppServer->GetThreadContext(ThreadIdentificator::GetCurrentThreadID());
	strInfoMsg += "|Call=GetLocalServers";
	strInfoMsg += QString("|ClientCode=%1").arg(nProgramCode);
	strInfoMsg += "|ClientIP=" + ctx.m_strPeerIP;
	strInfoMsg += QString("|ClientPort=%1").arg(ctx.m_nPeerPort);
	strInfoMsg += QString("|ClientID=%1").arg(strClientUniqueID);
	strInfoMsg += QString("|Date=%1").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss.zzz"));

	int nClientDBId = -1;
	DbSqlQuery query(status, g_DbManager);
	if(!status.IsOK()){
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, status.getErrorText());

		//dump status message
		strInfoMsg += "|CallStatusMsg=" + status.getErrorCode();
		strInfoMsg += QString("|CallStatusCode=%1").arg(status.getErrorText());
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
		return;
	}
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"GetLocalServers call: begin");

	//get server code that matches this client
	int nTargetServerCode = -1;
	QString strClientCode = QString("%1").arg(nProgramCode);
	if(strClientCode.startsWith(DIGIT_APP_EVERDOCS))
	{
		nTargetServerCode = SERVER_CODE_JOHN;
	}
	else if(strClientCode.startsWith(DIGIT_APP_EVERPICS))
	{
		nTargetServerCode = SERVER_CODE_JAMES;
	}
	else{
		Ret_nStatusCode = 1;	//error
		Ret_strStatusMsg = "Unsupported client program type";	//
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, Ret_strStatusMsg);
		return;
	}

	//TOFIX filter data by is alive!!!!!
	//QDateTime date = DateTimeHandler::currentDateTime();
	//QDateTime date_Minus10min = date.addSecs(-600);

	//fetch all servers in the same local network (matching the same external IP and required server type: everdocs or everpics)
	QString strSql = "SELECT CLI_CLIENT_IDENTITY FROM TARANTULA_CLIENT WHERE CLI_PROGRAM_CODE=? AND CLI_HOST_URL=?";
	query.Prepare(status, strSql);
	if (status.IsOK()){
		query.bindValue(0, nTargetServerCode);
		query.bindValue(1, ctx.m_strPeerIP);
		query.ExecutePrepared(status);
		DbRecordSet lstRes;
		query.FetchData(Ret_recLocalServers);
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"GetLocalServers call: end");

	//dump status message
	strInfoMsg += "|CallStatusMsg=OK";
	strInfoMsg += QString("|CallStatusCode=0");
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::MSG_TARANTULA_INFO, strInfoMsg);
}
