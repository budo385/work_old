#include "applicationserver.h"
#include "common/common/datetimehandler.h"
#include "common/common/logger.h"
#include "trans/trans/tcphelper.h"
#include "servicehandler.h"
#include "common/common/threadid.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "db_core/db_core/dbconnsettings.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbconnectionsetup.h"

#define MASTER_PASSWORD	"SOKRATES(R)"

//-----------------------------------------------
//	 SERVER GLOBALS:
//------------------------------------------------

//SPECIALS:
Logger							g_Logger;
ApplicationServer				*g_AppServer=NULL;
ServiceHandler					*g_ServiceHandler=NULL;
DbSqlManager					*g_DbManager=NULL;

/*!
	Constructor
*/
ApplicationServer::ApplicationServer(QObject *parent)
:QObject(parent)
{
	ClearPointers();
	m_nTimerID=-1;
	m_bIsRunning=false;
	connect(this,SIGNAL(RestartDelayedThreadSafeSignal(int)),this,SLOT(RestartDelayedThreadSafeSlot(int)),Qt::QueuedConnection);
	connect(this,SIGNAL(StopThreadSafeSignal()),this,SLOT(StopThreadSafeSlot()),Qt::QueuedConnection);
	ApplicationLogger::setApplicationLogger(&g_Logger);
	
}

void ApplicationServer::ClearPointers()
{
	m_DbObjectManager=NULL;
	m_RestHTTPServer=NULL;
	m_RestServiceRpcDispatcher=NULL;
	g_ServiceHandler=NULL;
	m_GarbageCollector=NULL;
	m_GbBkgTask=NULL;
	m_HttpInviteHandler=NULL;
	g_DbManager=NULL;

	m_strIniFilePath="";
}

/*!
	Destructor: kill all objects
*/
ApplicationServer::~ApplicationServer()
{
	if (IsRunning())
		Stop();
}

void ApplicationServer::Restart(Status &pStatus)
{
	//QMutexLocker locker(&m_Mutex); //lock acces
	Stop();
	Start(pStatus);
	//exit if error:

}

void ApplicationServer::RestartDelayed(int nReStartAfterSec)
{
	QMutexLocker locker(&m_Mutex);							//lock acces
	emit RestartDelayedThreadSafeSignal(nReStartAfterSec);	//ensures that appserver thread executes this!
}

//ignore parameter: do it now!
void ApplicationServer::StopDelayed(int nStopAfterSec)
{
	QMutexLocker locker(&m_Mutex);	//lock acces
	emit StopThreadSafeSignal();	//ensures that appserver thread executes this!
}

void ApplicationServer::StopThreadSafeSlot()
{
	Stop();
}

void ApplicationServer::RestartDelayedPriv(int nReStartAfterSec)
{
	//already in restart mode, ignore
	if (m_nTimerID!=-1)
	{
		return;
	}
	//this is fine opportunity to fire warning to all clients->
	m_nTimerID=startTimer(nReStartAfterSec*1000);
}

void ApplicationServer::Stop()
{
	QMutexLocker locker(&m_Mutex); //lock acces
	
	//if (!m_bIsRunning)	return;

	//stop server:
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopping Http server");
	SokratesHttpServer.StopServer();
	SokratesHttpServer.ClearRequestHandlerList();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Http server stopped");

	//save log & all rest:
	m_INIFile.m_nLogLevel=g_Logger.GetLogLevel();
	m_INIFile.m_nLogMaxSize=g_Logger.GetMaxLogSize();
	m_INIFile.m_nLogBufferSize=g_Logger.GetMemoryBufferSize();
	m_INIFile.Save(m_strIniFilePath);

	//destroy objects:
	DestroyObjectDbManager();
	if(m_GbBkgTask!=NULL)					delete m_GbBkgTask;
	if(m_RestHTTPServer!=NULL)				delete m_RestHTTPServer;
	if(m_RestServiceRpcDispatcher!=NULL)	delete m_RestServiceRpcDispatcher;
	if(g_ServiceHandler!=NULL)				delete g_ServiceHandler;
	if(g_DbManager!=NULL)					delete g_DbManager;
	if(m_GarbageCollector!=NULL)			delete m_GarbageCollector;
	if(m_HttpInviteHandler!=NULL)			delete m_HttpInviteHandler;
	

	m_bIsRunning=false;
	ClearPointers();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_STOP);
	g_Logger.FlushFileLog(); 
}



/*!
	Start App. server
*/
void ApplicationServer::Start(Status &pStatus)
{
	QMutexLocker locker(&m_Mutex); //lock acces
	if (m_bIsRunning)	return;

	HTTPServerConnectionSettings ServerSettings;
	DbConnectionSettings DbConnSettings;

	//----------------------------------------------
	//LOAD INI files:
	//----------------------------------------------
	LoadINI(pStatus);
	if(!pStatus.IsOK()) return;

	//----------------------------------------------
	//Start logger 
	//----------------------------------------------
	if (m_INIFile.m_nLogLevel>LoggerAbstract::LOG_LEVEL_NONE)
	{
		g_Logger.EnableFileLogging(QCoreApplication::applicationDirPath()+"/settings/appserver.log",m_INIFile.m_nLogBufferSize,m_INIFile.m_nLogMaxSize);
		g_Logger.EnableConsoleLogging();
	}
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);

	//----------------------------------------------
	//LOAD NET & DB files:
	//----------------------------------------------
	LoadNetSettings(pStatus,ServerSettings);
	if(!pStatus.IsOK()) return;
	LoadDbSettings(pStatus,DbConnSettings);
	if(!pStatus.IsOK())
	{
		CreateNewDb(pStatus,DbConnSettings);
		if(!pStatus.IsOK()) return;
	}


	//----------------------------------------------
	//init Database
	//----------------------------------------------
	InitDatabaseConnection(pStatus,&g_DbManager,DbConnSettings);
	if(!pStatus.IsOK())return;
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_DATABASE_CONN_SUCESS,DbConnSettings.m_strDbName+";"+DbConnSettings.m_strDbHostName);
	

	//----------------------------------------------
	//Launch Business & Private sets/init pointers:
	//----------------------------------------------
	g_ServiceHandler = new ServiceHandler();


	//----------------------------------------------
	//Start Garbage Collector in separate thread (GBC controls HTTP server):
	//----------------------------------------------
	m_GarbageCollector= new GarbageCollector(&SokratesHttpServer);
	m_GbBkgTask=new BackgroundTaskExecutor(m_GarbageCollector,this,600*1000);	//10min timer to clean up the garbage James entries from the DB
	m_GbBkgTask->Start();


	//----------------------------------------------
	//Launch all server handlers:
	//----------------------------------------------
	//TcpHelper::ResolveWebAddressNetworkInterface(ServerSettings.m_strServerIP.toLower(),ServerSettings.m_nPort,ServerSettings.m_bUseSSL);

	m_RestServiceRpcDispatcher		= new RestRpcDispatcher();
	m_RestHTTPServer				= new RestHTTPServer_Local(m_RestServiceRpcDispatcher);
	m_HttpInviteHandler				= new HttpInviteHandler();

	SokratesHttpServer.ClearRequestHandlerList();
	SokratesHttpServer.RegisterRequestHandler(m_RestHTTPServer);
	SokratesHttpServer.RegisterRequestHandler(m_HttpInviteHandler);

	SokratesHttpServer.InitServer(ServerSettings,false);
	SokratesHttpServer.StartListen(pStatus);

	if (pStatus.IsOK())
	{
		QString appParams=ServerSettings.m_strServerIP+";"+QVariant(ServerSettings.m_nPort).toString()+";"+QVariant(ServerSettings.m_bUseSSL).toString();
		appParams+=";1;"+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString();
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_START,appParams);
		m_bIsRunning=true;
	}
}

void ApplicationServer::SaveSettings()
{
	QMutexLocker locker(&m_Mutex);			//lock acces
	m_INIFile.Save(m_strIniFilePath);
}

void ApplicationServer::LoadINI(Status &pStatus)
{
	//load ini file & server settings:
	//------------------------------------------
	m_strIniFilePath= QCoreApplication::applicationDirPath()+"/settings/app_server.ini";
	bool bOK=m_INIFile.Load(m_strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,"INI file is missing,corrupted or has invalid values!");
		return;
	}

	QFileInfo ssl(m_INIFile.m_strSSLCertificateDir+"/server.cert");
	if (!ssl.exists())
	{
		m_INIFile.m_strSSLCertificateDir=QCoreApplication::applicationDirPath()+"/settings";
	}
}

void ApplicationServer::LoadNetSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings)
{
	pServerSettings.m_strCertificateFile=m_INIFile.m_strSSLCertificateDir+"/server.cert";
	pServerSettings.m_strPrivateKeyFile=m_INIFile.m_strSSLCertificateDir+"/server.pkey";
	pServerSettings.m_strCACertDir=m_INIFile.m_strSSLCertificateDir;
	pServerSettings.m_nPort=m_INIFile.m_nAppServerPort;

	if(m_INIFile.m_strAppServerIPAddress.isEmpty())
		pServerSettings.m_strServerIP="0.0.0.0"; //listen on all ports
	else
		pServerSettings.m_strServerIP=m_INIFile.m_strAppServerIPAddress;
	pServerSettings.m_bUseSSL=m_INIFile.m_nSSLMode;
}

//overriden QObject timer method
void ApplicationServer::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		m_nTimerID=-1;
		Status err;
		Restart(err);
		if(!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
			Stop();
		}
	}
}

void ApplicationServer::RestartDelayedThreadSafeSlot(int nRestartAfter)
{
	RestartDelayedPriv(nRestartAfter);
}

//thread safe
void ApplicationServer::OnBcpManager_RestoreCommandIssued(int nCallerID)
{
	//schedule for RESTART APP SERVER in 20 seconds:
	emit RestartDelayedThreadSafeSignal(20);
}

void ApplicationServer::LoadDbSettings(Status &pStatus,DbConnectionSettings &pDbSettings)
{
	//load DB SETTINGS:
	//------------------------------------------
	DbConnectionsList lstDbSettings;
	if(m_INIFile.m_strDBConnectionName.isEmpty())
	{
		pStatus.setError(1,"Database connection is not defined!");
		return;
	}

	QString strDbSettingsPath= QCoreApplication::applicationDirPath()+"/settings/database_connections.cfg";
	bool bOK=lstDbSettings.Load(strDbSettingsPath,MASTER_PASSWORD); //PASSWORD!???
	if(!bOK)
	{
		pStatus.setError(1,"Database setting file file is missing or corrupted!");
		return;
	}

	bOK=lstDbSettings.GetDbSettings(m_INIFile.m_strDBConnectionName,pDbSettings);
	if(!bOK)
	{
		pStatus.setError(1,"Database connection not found inside database settting file!");
		return;
	}
}

void ApplicationServer::InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections)
{
	//make Dbmanager, set to max conn:
	*pDbManager = new DbSqlManager;
	(*pDbManager)->Initialize(pStatus, ConnSettings,nMaxDbConnections);
	if (!pStatus.IsOK()) return;

	CreateObjectDbManager();
	CheckDatabaseSchema(pStatus);
}

bool ApplicationServer::CheckFieldExists(DbSqlQuery &query, const QString &strTable, const QString &strField)
{
	QString strSql = QString("SELECT FIRST 1 * FROM %1").arg(strTable);

	Status status;
	query.Prepare(status, strSql);
	if (!status.IsOK()) 
		return false; 
	query.ExecutePrepared(status);
	if (!status.IsOK()) 
		return false;

	DbRecordSet lstRes;
	query.FetchData(lstRes);

	int nFieldCnt = lstRes.getColumnCount();
	for(int i=0; i<nFieldCnt; i++){
		if(strField == lstRes.getColumnName(i))
			return true;
	}
	return false;
}

void ApplicationServer::CheckDatabaseSchema(Status &pStatus)
{
	//check if tables exist, if not create them
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	QString strSql;
	QStringList lstTables = query.GetDbConnection()->tables();
	if (!lstTables.contains("TARANTULA_CLIENT", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Creating DB table: TARANTULA_CLIENT");

		//create table here
		strSql = "CREATE TABLE TARANTULA_CLIENT ("
				 "CLI_ID INTEGER NOT NULL,"
				 "CLI_DAT_LAST_MODIFIED TIMESTAMP NOT NULL,"
				 "CLI_PROGRAM_CODE SMALLINT NOT NULL,"
				 "CLI_PROGRAM_VERSION_MAJOR SMALLINT NOT NULL,"
				 "CLI_PROGRAM_VERSION_MINOR SMALLINT NOT NULL,"
				 "CLI_PROGRAM_VERSION_REVISION SMALLINT NOT NULL,"
				 "CLI_PROGRAM_PLATFORM_NAME VARCHAR(50),"
				 "CLI_PROGRAM_PLATFORM_UID VARCHAR(100),"
				 "CLI_CLIENT_IDENTITY VARCHAR(15) NOT NULL,"

				 //new fields added to cache data from the access log for system speedup
				 "CLI_HOST_URL VARCHAR(255),"
				 "CLI_HOST_PORT INTEGER,"
				 "CLI_HOST_LOCAL_IP VARCHAR(255),"
				 "CLI_HOST_STATS_NAT_USED SMALLINT,"
				 "CLI_HOST_IS_SSL INTEGER,"
				 "CLI_HOST_IP_FORCED SMALLINT,"	//is NAT forced

				 "constraint TARANTULA_CLIENT_PK primary key (CLI_ID))";
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);

		m_DbObjectManager->CreateAutoIncrementID(pStatus, query.GetDbConnection(), "TARANTULA_CLIENT", "CLI_ID");

		//create index on CLIENT_IDENTITY field
		strSql = "create index IND_TC_CLIENT_CODE on TARANTULA_CLIENT (CLI_CLIENT_IDENTITY)";  
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);

		//create index on CLI_HOST_URL field (external IP)
		strSql = "create index IND_TC_EXT_IP on TARANTULA_CLIENT (CLI_HOST_URL)";  
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);

		//create index on CLI_PROGRAM_CODE field (external IP)
		strSql = "create index IND_TC_PROG_CODE on TARANTULA_CLIENT (CLI_PROGRAM_CODE)";  
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);
	}
	else{
		//check if these fields exist
		if(!CheckFieldExists(query, "TARANTULA_CLIENT", "CLI_HOST_URL")){

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Old DB table version found: TARANTULA_CLIENT, adding new fields");

			//add the new fields
			strSql = "ALTER TABLE TARANTULA_CLIENT ADD CLI_HOST_URL VARCHAR(255)";  
			query.Prepare(pStatus, strSql);
			if (!pStatus.IsOK()) 
				return; 
			query.ExecutePrepared(pStatus);

			strSql = "ALTER TABLE TARANTULA_CLIENT ADD CLI_HOST_PORT INTEGER";
			query.Prepare(pStatus, strSql);
			if (!pStatus.IsOK()) 
				return; 
			query.ExecutePrepared(pStatus);

			strSql = "ALTER TABLE TARANTULA_CLIENT ADD CLI_HOST_LOCAL_IP VARCHAR(255)";
			query.Prepare(pStatus, strSql);
			if (!pStatus.IsOK()) 
				return; 
			query.ExecutePrepared(pStatus);

			strSql = "ALTER TABLE TARANTULA_CLIENT ADD CLI_HOST_STATS_NAT_USED SMALLINT";
			query.Prepare(pStatus, strSql);
			if (!pStatus.IsOK()) 
				return; 
			query.ExecutePrepared(pStatus);

			strSql = "ALTER TABLE TARANTULA_CLIENT ADD CLI_HOST_IS_SSL INTEGER";
			query.Prepare(pStatus, strSql);
			if (!pStatus.IsOK()) 
				return; 
			query.ExecutePrepared(pStatus);

			strSql = "ALTER TABLE TARANTULA_CLIENT ADD CLI_HOST_IP_FORCED SMALLINT";
			query.Prepare(pStatus, strSql);
			if (!pStatus.IsOK()) 
				return; 
			query.ExecutePrepared(pStatus);
		}
	}

	//add new indexes (ignore errors, they may already exist and I have no way to check this)
	//TOFIX check for index existence with ?
	//lstTables = query.GetDbConnection()->tables(QSql::SystemTables);
	//create index on CLI_HOST_URL field (external IP)
	strSql = "create index IND_TC_EXT_IP on TARANTULA_CLIENT (CLI_HOST_URL)";  
	Status status;
	query.Prepare(status, strSql);
	query.ExecutePrepared(status);

	//create index on CLI_PROGRAM_CODE field (external IP)
	strSql = "create index IND_TC_PROG_CODE on TARANTULA_CLIENT (CLI_PROGRAM_CODE)";  
	query.Prepare(status, strSql);
	query.ExecutePrepared(status);
	
	lstTables = query.GetDbConnection()->tables();
	if (lstTables.contains("TARANTULA_ACCESS_LOG", Qt::CaseInsensitive))	//check if table already exists
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Deleting DB table: TARANTULA_ACCESS_LOG");

		//first remove the foreign key
		strSql = "ALTER TABLE TARANTULA_ACCESS_LOG DROP CONSTRAINT TARANTULA_ACCESS_CLI_FK1";
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);
		
		//remove duplicates I've found in the real-life DB (TARANTULA_CLIENT table)
		strSql = "DELETE FROM TARANTULA_CLIENT x WHERE x.CLI_ID IN (SELECT a.CLI_ID from TARANTULA_CLIENT a LEFT JOIN TARANTULA_CLIENT b ON a.CLI_CLIENT_IDENTITY=b.CLI_CLIENT_IDENTITY WHERE NOT (a.CLI_ID =b.CLI_ID) AND a.CLI_ID < b.CLI_ID)";
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);

		//recreate index on TARANTULA_CLIENT table to force UNIQUE-ness
		strSql = "DROP INDEX IND_TC_CLIENT_CODE";
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);

		strSql = "CREATE UNIQUE INDEX IND_TC_CLIENT_CODE ON TARANTULA_CLIENT (CLI_CLIENT_IDENTITY)";
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);

/*
		//TOFIX now update the newly added TARANTULA_CLIENT fields and get rid of the TARANTULA_ACCESS_LOG
		
		//delete the table and its indexes
		strSql = "DROP INDEX IND_TACC_CLIENT_ID";
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);

		strSql = "DROP INDEX IND_TACC_ACCESS_DATE";
		query.Prepare(pStatus, strSql);
		if (!pStatus.IsOK()) 
			return; 
		query.ExecutePrepared(pStatus);

		strSql = "DROP TABLE TARANTULA_ACCESS_LOG";
		query.Prepare(pStatus, strSql);
		if (pStatus.IsOK()) 
			query.ExecutePrepared(pStatus);
*/
	}

	CheckSequences(pStatus); 	//BT: sequence names changed: from name_SEQ to SEQ_name (Omnis compability)
}


void ApplicationServer::CreateNewDb(Status &pStatus,DbConnectionSettings &pDbSettings)
{
	QString strSettingsPath=QCoreApplication::applicationDirPath()+"/settings/database_connections.cfg";
	QString strDbName="DATA";
	QString strTargetDataFile=QCoreApplication::applicationDirPath();
	QString strConnection="DATA";
	m_INIFile.m_strDBConnectionName=strConnection;
	m_INIFile.Save(m_strIniFilePath);
	DbConnectionSetup::SetupAppServerDatabase(pStatus,"SYSDBA","masterkey",DBTYPE_FIREBIRD,strDbName,strConnection,strTargetDataFile);
	if (!pStatus.IsOK())
		return;

	pDbSettings.m_strCustomName="DATA";
	pDbSettings.m_strDbName=strTargetDataFile+"/DATA.FDB";
	pDbSettings.m_strDbUserName="SYSDBA";
	pDbSettings.m_strDbPassword="masterkey";
	pDbSettings.m_strDbType=DBTYPE_FIREBIRD;
	DbConnectionSetup::CreateDatabaseConnection(pStatus,strSettingsPath,pDbSettings);
}

//B.T. sequences were named as name_SEQ, now it is SEQ_name so check if exist, if not then create new and caculate new value for it, as max...
void ApplicationServer::CheckSequences(Status &pStatus)
{
	DbSqlQuery query(pStatus, g_DbManager);
	if(!pStatus.IsOK()) return;

	//CLI_ID
	if(m_DbObjectManager->CheckIfExists_Sequence(query.GetDbConnection(), "SEQ_CLI_ID"))
		return;

	m_DbObjectManager->CreateSequence(pStatus, query.GetDbConnection(), "SEQ_CLI_ID");
	if(!pStatus.IsOK()) return;

	m_DbObjectManager->AdjustAutoIncrementAfterDbCopy(pStatus, query.GetDbConnection(),"TARANTULA_CLIENT", "CLI_ID");

}

void ApplicationServer::CreateObjectDbManager()
{
	m_DbObjectManager=NULL;

	//make instance of proper DboManager:
	if(g_DbManager->GetDbType()==DBTYPE_ORACLE)
		m_DbObjectManager= new DbObjectManager_Oracle(g_DbManager->GetDbSettings());
	else if (g_DbManager->GetDbType()==DBTYPE_MYSQL)
		m_DbObjectManager= new DbObjectManager_MySQL(g_DbManager->GetDbSettings());
	else if (g_DbManager->GetDbType()==DBTYPE_FIREBIRD)
		m_DbObjectManager= new DbObjectManager_FireBird(g_DbManager->GetDbSettings());
	else
		Q_ASSERT_X(false,"DBO ORGANIZER", "Database object manager doesn't exists for specified base");
}

void ApplicationServer::DestroyObjectDbManager()
{
	if (m_DbObjectManager)
	{
		delete m_DbObjectManager;
		m_DbObjectManager=NULL;
	}
}

