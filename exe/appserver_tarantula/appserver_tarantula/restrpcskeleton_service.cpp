#include "restrpcskeleton_service.h"

#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;

RestRpcSkeleton_Service::RestRpcSkeleton_Service(int RPCType):
RpcSkeleton(RPCType)
{
	SetNameSpace("service");	//set namespace (business object name)

	mFunctList.insert("POST/service/register",&RestRpcSkeleton_Service::SystemRegister);
	mFunctList.insert("POST/service/unregister",&RestRpcSkeleton_Service::SystemUnregister);
	mFunctList.insert("POST/service/ping",&RestRpcSkeleton_Service::TestConnectivity);
	mFunctList.insert("POST/service/list",&RestRpcSkeleton_Service::GetLocalServers);
}

bool RestRpcSkeleton_Service::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod,int nClientTimeZoneOffsetMinutes)
{
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;
	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nClientTimeZoneOffsetMinutes);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);
		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);
			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

	return true;
}

void RestRpcSkeleton_Service::SystemRegister(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount() != 13 &&
	   rpc.msg_in->GetParameterCount() != 14){
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	int nProgramCode;
	int nVersionMajor;
	int nVersionMinor;
	int nVersionRevision;
	QString strPlatformName;
	QString strPlatformUniqueID;
	QString Ret_strClientUniqueID;
	QString strHostUniqueID;
	QString Ret_strHostIP;
	int Ret_nPublicPort;
	int nStatsNatProtocol;
	int RetOut_nIsSSL=0; //default=false incoming for james, outgoing for lucy
	QString Ret_strHostLocalIP;
	int Ret_nIPForced;

	rpc.msg_in->GetParameter(0,&nProgramCode,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nVersionMajor,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nVersionMinor,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nVersionRevision,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strPlatformName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&strPlatformUniqueID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&Ret_strClientUniqueID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&strHostUniqueID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&Ret_strHostIP,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(9,&Ret_nPublicPort,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(10,&nStatsNatProtocol,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(11,&RetOut_nIsSSL,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(12,&Ret_strHostLocalIP,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	if(rpc.msg_in->GetParameterCount() == 14)
		rpc.msg_in->GetParameter(13,&Ret_nIPForced,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	int Ret_nStatusCode;
	QString Ret_strStatusMsg;

	g_ServiceHandler->SystemRegister(err, nProgramCode, nVersionMajor, nVersionMinor, nVersionRevision, strPlatformName, strPlatformUniqueID, Ret_strClientUniqueID, strHostUniqueID, Ret_strHostIP, Ret_nPublicPort, nStatsNatProtocol, Ret_nStatusCode, Ret_strStatusMsg,RetOut_nIsSSL, Ret_strHostLocalIP, Ret_nIPForced);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nStatusCode, "STATUS_CODE");
	rpc.msg_out->AddParameter(&Ret_strStatusMsg, "STATUS_TEXT");
	rpc.msg_out->AddParameter(&Ret_strClientUniqueID, "CLIENT_IDENTITY");
	rpc.msg_out->AddParameter(&Ret_strHostIP, "HOST_URL");
	rpc.msg_out->AddParameter(&Ret_nPublicPort, "HOST_PORT");
	rpc.msg_out->AddParameter(&RetOut_nIsSSL, "HOST_SSL");
	rpc.msg_out->AddParameter(&Ret_strHostLocalIP, "HOST_LOCAL_IP");
		
	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::SystemUnregister(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1){
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString strClientUniqueID;
	int Ret_nStatusCode;
	QString Ret_strStatusMsg;
	if(rpc.msg_in->GetParameterCount()>0)
	{
		rpc.msg_in->GetParameter(0,&strClientUniqueID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	}

	//invoke handler
	g_ServiceHandler->SystemUnregister(err, strClientUniqueID, Ret_nStatusCode, Ret_strStatusMsg);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nStatusCode, "STATUS_CODE");
	rpc.msg_out->AddParameter(&Ret_strStatusMsg, "STATUS_TEXT");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::TestConnectivity(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2){
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString strClientUniqueID;
	int nPublicPort;
	if(rpc.msg_in->GetParameterCount()>0)
	{
		rpc.msg_in->GetParameter(0,&strClientUniqueID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
		rpc.msg_in->GetParameter(1,&nPublicPort,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	}

	//invoke handler
	int Ret_nStatusCode;
	QString Ret_strStatusMsg;
	QString Ret_strHostIP;
	g_ServiceHandler->TestConnectivity(err, strClientUniqueID, Ret_strHostIP, nPublicPort, Ret_nStatusCode, Ret_strStatusMsg);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nStatusCode, "STATUS_CODE");
	rpc.msg_out->AddParameter(&Ret_strStatusMsg, "STATUS_TEXT");
	rpc.msg_out->AddParameter(&Ret_strHostIP, "HOST_URL");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::GetLocalServers(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount() != 2){
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString strClientUniqueID;
	int nProgramCode;

	rpc.msg_in->GetParameter(0,&strClientUniqueID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nProgramCode,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	DbRecordSet Ret_recLocalServers;
	int Ret_nStatusCode;
	QString Ret_strStatusMsg;

	g_ServiceHandler->GetLocalServers(err, strClientUniqueID, nProgramCode, Ret_recLocalServers, Ret_nStatusCode, Ret_strStatusMsg);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nStatusCode, "STATUS_CODE");
	rpc.msg_out->AddParameter(&Ret_strStatusMsg, "STATUS_TEXT");
	rpc.msg_out->AddParameter(&Ret_recLocalServers, "SERVER_LIST");
		
	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}
