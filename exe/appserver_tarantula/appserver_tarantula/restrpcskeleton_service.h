#ifndef RESTRPCSKELETON_SERVICE_H
#define RESTRPCSKELETON_SERVICE_H

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"

class RestRpcSkeleton_Service : public RpcSkeleton 
{
	typedef void (RestRpcSkeleton_Service::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id);

public:
	RestRpcSkeleton_Service(int RPCType);
	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod,int nClientTimeZoneOffsetMinutes=0);

private:
	//skeleton function for each method publicly accessible
	void SystemRegister(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id);
	void SystemUnregister(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id);
	void TestConnectivity(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id);
	void GetLocalServers(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id);

	QHash<QString,PFN> mFunctList;
};


#endif // RESTRPCSKELETON_SERVICE_H
