#!/bin/bash

CERT_NAME="3rd Party Mac Developer Application: Helix Business-Soft AG"
CERT_INSTALLER="3rd Party Mac Developer Installer: Helix Business-Soft AG"
APP_NAME=Sokrates-SPC
APP_BUNDLE=$APP_NAME.app
SETUP_NAME=SokratesSPCSetup
APP_BUNDLE_ID=ch.sokrates.SokratesSPC
APP_VERSION=1.0.0

CLEAN_OBJS=1

echo "------------------------------------------------------------"
echo " Compiling $APP_NAME"
echo "------------------------------------------------------------"

#update all sources from svn
cd ../../
svn up
cd exe/sokrates_spc
chmod +x ./compile.sh

#cleanup builds
rm -rf ./debug

#compile common
echo "compile common lib"
if [ "$CLEAN_OBJS" == "1" ]; then
	rm -rf ../../lib/common/common/Debug
fi
rm -f ../../lib/common/common/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/common/common/Makefile ../../lib/common/common/common.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/common/common/ clean
fi
make -C ../../lib/common/common/ all  || exit 1
cp ../../lib/debug/libcommon.a  ./ || exit 1

#compile trans
echo "compile trans lib"
rm -f ../../lib/trans/trans/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/trans/trans/Makefile ../../lib/trans/trans/trans.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/trans/trans/ clean
fi
make -C ../../lib/trans/trans/ all  || exit 1
cp ../../lib/debug/libtrans.a  ./ || exit 1

#compile db_core
echo "compile db_core lib"
rm -f ../../lib/db_core/db_core/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/db_core/db_core/Makefile ../../lib/db_core/db_core/db_core.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/db_core/db_core/ clean
fi
make -C ../../lib/db_core/db_core/ all  || exit 1
cp ../../lib/debug/libdb_core.a  ./ || exit 1

#compile db
echo "compile db lib"
rm -f ../../lib/db/db/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/db/db/Makefile ../../lib/db/db/db.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/db/db/ clean
fi
make -C ../../lib/db/db/ all  || exit 1
cp ../../lib/debug/libdb.a  ./ || exit 1

#compile bus_core
echo "compile bus_core lib"
rm -f ../../lib/bus_core/bus_core/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/bus_core/bus_core/Makefile ../../lib/bus_core/bus_core/bus_core.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/bus_core/bus_core/ clean
fi
make -C ../../lib/bus_core/bus_core/ all  || exit 1
cp ../../lib/debug/libbus_core.a  ./ || exit 1

#compile gui_core
echo "compile gui_core lib"
rm -f ../../lib/gui_core/gui_core/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/gui_core/gui_core/Makefile ../../lib/gui_core/gui_core/gui_core.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/gui_core/gui_core/ clean
fi
make -C ../../lib/gui_core/gui_core/ all  || exit 1
cp ../../lib/debug/libgui_core.a  ./ || exit 1

#compile bus_interface
echo "compile bus_interface lib"
rm -f ../../lib/bus_interface/bus_interface/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/bus_interface/bus_interface/Makefile ../../lib/bus_interface/bus_interface/bus_interface.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/bus_interface/bus_interface/ clean
fi
make -C ../../lib/bus_interface/bus_interface/ all  || exit 1
cp ../../lib/debug/libbus_interface.a  ./ || exit 1

#compile bus_trans_client
echo "compile bus_trans_client lib"
rm -f ../../lib/bus_trans_client/bus_trans_client/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/bus_trans_client/bus_trans_client/Makefile ../../lib/bus_trans_client/bus_trans_client/bus_trans_client.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/bus_trans_client/bus_trans_client/ clean
fi
make -C ../../lib/bus_trans_client/bus_trans_client/ all  || exit 1
cp ../../lib/debug/libbus_trans_client.a  ./ || exit 1

#compile bus_thick_client
echo "compile bus_thick_client lib"
rm -f ../../lib/bus_thick_client/bus_thick_client/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/bus_thick_client/bus_thick_client/Makefile ../../lib/bus_thick_client/bus_thick_client/bus_thick_client.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/bus_thick_client/bus_thick_client/ clean
fi
make -C ../../lib/bus_thick_client/bus_thick_client/ all  || exit 1
cp ../../lib/debug/libbus_thick_client.a  ./ || exit 1

#compile bus_server
echo "compile bus_server lib"
rm -f ../../lib/bus_server/bus_server/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/bus_server/bus_server/Makefile ../../lib/bus_server/bus_server/bus_server.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/bus_server/bus_server/ clean
fi
make -C ../../lib/bus_server/bus_server/ all  || exit 1
cp ../../lib/debug/libbus_server.a  ./ || exit 1

#compile os_specific
echo "compile os_specific lib"
rm -f ../../lib/os_specific/os_specific/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/os_specific/os_specific/Makefile ../../lib/os_specific/os_specific/os_specific.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/os_specific/os_specific/ clean
fi
make -C ../../lib/os_specific/os_specific/ all  || exit 1
cp ../../lib/debug/libos_specific.a  ./ || exit 1

#compile admin_tools
echo "compile admin_tools lib"
rm -f ../../lib/admin_tools/admin_tools/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/admin_tools/admin_tools/Makefile ../../lib/admin_tools/admin_tools/admin_tools.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/admin_tools/admin_tools/ clean
fi
make -C ../../lib/admin_tools/admin_tools/ all  || exit 1
cp ../../lib/debug/libadmin_tools.a  ./ || exit 1

#compile bus_client "na divljaka" (circular dependency to fui_collection)
echo "compile bus_client lib"
rm -f ../../lib/bus_client/bus_client/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/bus_client/bus_client/Makefile ../../lib/bus_client/bus_client/bus_client.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/bus_client/bus_client/ clean
fi
#temporary switch off exiting on error here
set +e
make -C ../../lib/bus_client/bus_client/ all
set -e

#compile fui_collection
echo "compile fui_collection lib"
rm -f ../../lib/fui_collection/fui_collection/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/fui_collection/fui_collection/Makefile ../../lib/fui_collection/fui_collection/fui_collection.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/fui_collection/fui_collection/ clean
fi
make -C ../../lib/fui_collection/fui_collection/ all  || exit 1
cp ../../lib/debug/libfui_collection.a  ./ || exit 1

#compile bus_client "u fino"
echo "compile bus_client lib again"
rm -f ../../lib/bus_client/bus_client/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/bus_client/bus_client/Makefile ../../lib/bus_client/bus_client/bus_client.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/bus_client/bus_client/ clean
fi
make -C ../../lib/bus_client/bus_client/ all  || exit 1
cp ../../lib/debug/libbus_client.a  ./ || exit 1

#compile report_core
echo "compile report_core lib"
rm -f ../../lib/report_core/report_core/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/report_core/report_core/Makefile ../../lib/report_core/report_core/report_core.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/report_core/report_core/ clean
fi
make -C ../../lib/report_core/report_core/ all  || exit 1
cp ../../lib/debug/libreport_core.a  ./ || exit 1

#compile report_definitions
echo "compile report_definitions lib"
rm -f ../../lib/report_definitions/report_definitions/Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/report_definitions/report_definitions/Makefile ../../lib/report_definitions/report_definitions/report_definitions.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/report_definitions/report_definitions/ clean
fi
make -C ../../lib/report_definitions/report_definitions/ all  || exit 1
cp ../../lib/debug/libreport_definitions.a  ./ || exit 1

#final compilation
echo "compile sokrates_spc"
rm -f ./Makefile
qmake -makefile -macx -spec macx-g++ -Wall -o Makefile ./sokrates_spc/sokrates_spc.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make clean
fi
make all || { echo "sokrates_app compilation failed"; exit 1; }

#copy all the required files to the Sokrates-SPC.app bundle, create dirs too
echo "Copy files to bundle"
install -m644 ../../_data/Translation/sokrates_de.qm ./debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
install -d -m755 ../../deploy/Setup/SPCDeploy/themes ./Debug/$APP_BUNDLE/Contents/MacOS/themes
install -m644 ../../deploy/Setup/SPCDeploy/themes/*.rcc ./Debug/$APP_BUNDLE/Contents/MacOS/themes/ || echo "Some files were not copied"
install -d -m755 ../../deploy/Setup/SPCDeploy/help ./Debug/$APP_BUNDLE/Contents/MacOS/help
install -d -m755 ../../deploy/Setup/SPCDeploy/help/images ./Debug/$APP_BUNDLE/Contents/MacOS/help/images
install -m644 ../../deploy/Setup/SPCDeploy/help/* ./Debug/$APP_BUNDLE/Contents/MacOS/help/ || echo "Some files were not copied"
install -m644  ../../deploy/Setup/SPCDeploy/help/images/* ./Debug/$APP_BUNDLE/Contents/MacOS/help/images/ || echo "Some files were not copied"
install -d -m755 ../../deploy/Setup/SPCDeploy/data ./Debug/$APP_BUNDLE/Contents/MacOS/data
install -m644 ../../deploy/Setup/SPCDeploy/data/* ./Debug/$APP_BUNDLE/Contents/MacOS/data/ || echo "Some files were not copied"
install -d -m755 ../../deploy/Setup/SPCDeploy/settings ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings
install -m644 ../../deploy/Setup/SPCDeploy/settings/* ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/ || echo "Some files were not copied"
install -d -m755 ../../deploy/Setup/SPCDeploy/template_data ./Debug/$APP_BUNDLE/Contents/MacOS/template_data
install -m644 ../../deploy/Setup/SPCDeploy/template_data/* ./Debug/$APP_BUNDLE/Contents/MacOS/template_data/ || echo "Some files were not copied"
install -d -m755 ../../deploy/Setup/SPCDeploy/report_definitions ./Debug/$APP_BUNDLE/Contents/MacOS/report_definitions
install -m644 ../../deploy/Setup/SPCDeploy/report_definitions/* ./Debug/$APP_BUNDLE/Contents/MacOS/report_definitions/ || echo "Some files were not copied"
install -m644 ../../deploy/Setup/SPCDeploy/ding.wav ./Debug/$APP_BUNDLE/Contents/MacOS/ding.wav || echo "Some files were not copied"
install -m644 ../../deploy/Setup/SPCDeploy/*.hed ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"

#delete .svn folders
find ./Debug/$APP_BUNDLE/Contents/MacOS/ -name ".svn" | xargs rm -rf \;

#copies all the Qt dependencies into the main bundle
macdeployqt ./debug/$APP_BUNDLE

mkdir -p ./debug/Resources
cp -R ../../deploy/Setup/FFHServer/license.txt ./debug/Resources/License.txt

# use our own Info file to overwrite auto-generated one
cp -R ../../deploy/Setup/SPCDeploy/info.plist ./debug/$APP_BUNDLE/Contents/info.plist

# NOTE: this non-signed version is for ordinary download (not for MacStore!!!)
# create flat package (.pkg)
/Developer/Applications/Utilities/PackageMaker.app/Contents/MacOS/PackageMaker -AppleLanguages "(English)" -r ./debug/$APP_BUNDLE/ -o ./Debug/SokratesSPCInstall.pkg -v $APP_VERSION -t "SokratesSPC Setup" -i $APP_BUNDLE_ID --no-relocate --verbose --target 10.5 --resources ./debug/Resources/

echo "signing the APP";
codesign --verbose -fs "3rd Party Mac Developer Application: Helix Business-Soft AG" ./debug/Sokrates-SPC.app

#http://developer.apple.com/devcenter/mac/documents/submitting.html
#Quote: "Note: Using the PackageMaker application to archive your application is not supported"
#productbuild --component ./debug/$APP_BUNDLE /Applications --sign $CERT_INSTALLER ./debug/$SETUP_NAME.pkg
productbuild --component ./debug/Sokrates-SPC.app /Applications --sign "3rd Party Mac Developer Installer: Helix Business-Soft AG" ./debug/$SETUP_NAME.pkg

sudo installer -store -pkg ./debug/$SETUP_NAME.pkg  -target /
