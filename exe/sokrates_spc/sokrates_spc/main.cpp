#include <QApplication>
#include "sokrates_spc.h"
#include <QTranslator>
#include <QTimer>
#include "sokrates_sideBar.h"
#include "common/common/datahelper.h"

#ifdef _WIN32
	#include <signal.h>
	#include "stackwalker.h"	//backtrace for Windows
#endif

#include <openssl/ssl.h>
#include <openssl/rand.h>

#ifdef _WIN32
 #include "os_specific/os_specific/mimepp/src/mimepp/mimepp.h"
#endif

//#include "bus_thick_client/bus_thick_client/businessservicemanager_thickclient.h"
//extern BusinessServiceManager *g_pBoSet;
#include "globalobjectmanager.h"
GlobalObjectManager *g_pGlobalObjectManager=NULL;
#include "bus_client/bus_client/clientmanagerext.h"
ClientManagerExt *g_pClientManager=NULL;

extern QTranslator *g_translator;

#ifndef QT_NO_DEBUG
	int g_BufferSize;
#endif

void PrepareCrashLog();




/*
void CreateThickClient(Status &err)
{
	g_pBoSet = new BusinessServiceManager_ThickClient;
	dynamic_cast<BusinessServiceManager_ThickClient*>(g_pBoSet)->Thick_Initialize(err,g_pClientManager->GetIniFile(), static_cast<ClientBackupManager*>(g_pClientManager->GetBackupManager()));
}
*/

void PrepareCrashLog();

int main(int argc, char *argv[])
{
	//one-time initialization for OpenSSL
	SSL_load_error_strings();
	SSL_library_init();
	//RAND_screen();	//initialize random number generator

#ifdef _WIN32
	// Initialize the library
	mimepp::Initialize();
#endif

	//prepare crash log
	PrepareCrashLog();

    QApplication a(argc, argv);
	a.setQuitOnLastWindowClosed(true);
	
	//init resources for all linked libraries 
	Q_INIT_RESOURCE(gui_core);
	

	Status err;
	g_pClientManager= new ClientManagerExt(&a); 
	g_pClientManager->Initialize(err,true,true);
	if (!err.IsOK())
		return 1;		//error

	g_pGlobalObjectManager= new GlobalObjectManager(&a);


	//
	//TOFIX handle translation loading for all languages
	//

	g_pClientManager->SetApplicationDisplayName(QString("SOKRATES<sup>")+QChar(174)+QString("</sup> <i>Communicator</i>"));

	g_translator = new QTranslator();
	if(g_pClientManager->GetIniFile()->m_strLangCode == "de")
	{
		bool bOK = g_translator->load("sokrates_de", QCoreApplication::applicationDirPath());
		Q_ASSERT(bOK);
		a.installTranslator(g_translator);
	}

	//order form (must be in this place, after translator, only for THIN):
	if (g_pClientManager->CheckForOrderForm())
		return 1;

	//bring up windows:
	sokrates_spc *w = new sokrates_spc;
	Sokrates_SideBar *sideBar = new Sokrates_SideBar;
	QTimer::singleShot(0, g_pClientManager, SLOT(Login()));			//wait to start until QCoreApp enters event loop
	g_pGlobalObjectManager->Initialize(w,sideBar);
	//sideBar->Initialize();
	//w->Initialize();
	return a.exec();

	// no need to use, but one day ...
	// Finalize the library
	//
	//mimepp::Finalize();
}

#ifdef _WIN32
	// Specialized stackwalker-output classes
	class StackWalkerToConsole : public StackWalker
	{
	public:
		QByteArray m_buffer;
	protected:
		virtual void OnOutput(LPCSTR szText)
		{
			m_buffer.append(szText);
		}

		virtual void OnLoadModule(LPCSTR img, LPCSTR mod, DWORD64 baseAddr, DWORD size, DWORD result, LPCSTR symType, LPCSTR pdbName, ULONGLONG fileVersion)
		{
			Q_UNUSED(img);
			Q_UNUSED(mod);
			Q_UNUSED(baseAddr);
			Q_UNUSED(size);
			Q_UNUSED(result);
			Q_UNUSED(symType);
			Q_UNUSED(pdbName);
			Q_UNUSED(fileVersion);
			
			//overriden to skip writing module info, not interested for now
		}
	};
#endif

void DumpBacktrace()
{
#ifdef _WIN32
	//get backtrace events in a buffer
	qDebug()<<"crash occurred, register stack";
	StackWalkerToConsole sw;
	sw.ShowCallstack();

	//write buffer
	Status pStatus;
	QString strSettings = g_pClientManager->CheckSettingsDirectory(pStatus);
	QString strDatetime=QDateTime::currentDateTime().toString("yyyy-MM-dd_HH-mm");
	QString strFile = strSettings+"/crash_"+strDatetime+".log";
	DataHelper::WriteFile(strFile,sw.m_buffer);
	qDebug()<<"crash occurred, write log file:" << strFile;
#endif
}

extern "C" void signal_handler(int sig)
{
	DumpBacktrace();
	_exit(1);
}

void PrepareCrashLog()
{
//#ifdef GENERATE_CRASHLOG
	//install signal handlers to catch crash situations
	signal(SIGABRT, signal_handler);
	signal(SIGFPE,  signal_handler);
	signal(SIGINT,  signal_handler);
	signal(SIGILL,  signal_handler);
	signal(SIGSEGV, signal_handler);
#ifdef _DEBUG
	signal(SIGTERM, signal_handler);	//triggered manually with Ctrl+C in console, for testing only
	signal(SIGBREAK, signal_handler);	//Ctrl+Break
#endif
//#endif
}
