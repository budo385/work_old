#ifndef SOKRATES_SPC_H
#define SOKRATES_SPC_H

#include <QMainWindow>
#include "generatedfiles/ui_sokrates_spc.h"
#include <QCloseEvent>
#include <QProgressBar>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>
#include <QDateTime>
//#include <QtAssistant/QAssistantClient>
//#include <QWindowsMime>
#include "fui_collection/fui_collection/dlg_messenger.h"
#include "common/common/observer_ptrn.h"
#include "gui_core/gui_core/styledpushbutton.h"
#include "common/common/status.h"
//#include "emailimportthread.h"
#include "bus_client/bus_client/clientimportexportmanager.h"

class sokrates_spc : public QMainWindow, public ObsrPtrn_Observer
{
	friend class Sokrates_SideBar;

    Q_OBJECT

public:
    sokrates_spc(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~sokrates_spc();
	

	enum MENU_BUTTON
	{
		MENU_BUTTON_DESKTOP,
		MENU_BUTTON_CONTACT,
		MENU_BUTTON_PROJECT,
		MENU_BUTTON_CALENDAR,
		MENU_BUTTON_PROJMANAGER
	};

	void OnFuiWindowClose(QWidget *pWindow);
	void UpdateLoginData(bool bLogged);

	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

	//QHtmlMenuData m_menu;

	//banner widgets
	QGraphicsScene *m_pScene;
	QGraphicsPixmapItem *m_pPix1;
	QGraphicsPixmapItem *m_pPix2;
	QGraphicsView *m_pView;
	QGraphicsTextItem *m_pLink1;
	QGraphicsTextItem *m_pLink2;
	QGraphicsTextItem *m_pLink3;
	QGraphicsTextItem *m_pClock1;
	QGraphicsTextItem *m_pClock2;

protected:
	virtual void resizeEvent(QResizeEvent *event);
	virtual void closeEvent(QCloseEvent *event);
	virtual void keyPressEvent( QKeyEvent * event ); 

private:
    Ui::sokrates_spcClass ui;

	QProgressBar *m_ProgressBar;
	QLabel *m_pInfo;

	//GUI setup:
	//---------------------------------
	void CreateLayout();
	bool DestroyLayout();

	void SetupBanner();
	void SetupMenuActions();
	void BuildToolBar();
	void SetupMenu(bool bLogged);
	void SetupGUI();
	void UpdateMenuState();
	void CloseAllModalWindows();
	void ApplyPersonSettings();
	void ApplyFunctionPoints();
	//bool ProcessOnStartUpWizard(Status &pStatus);
	
	//---------------------------------

	Dlg_Messenger DlgMessenger;


public slots:
	void Initialize();		//called on startup
	void OnClientLogged();
	void OnClientLogout();
	void OnProcessBeforeLogout();
	void OnCommunicationFatalError(int nErrorCode);
	void OnThemeChanged();
	void OnViewChanged();

private slots:

	//Login & logut & failure handlers
	void OnClientClosedLoginWnd();
	void OnCommunicationInProgress(int nTicks,qint64 done, qint64 total);
	void OnServerMessageReceived(int nCode, QString strText);
	void OnMenu_DesktopClick();
	void OnMenu_ContactClick();
	void OnMenu_ProjectClick();
	void OnMenu_CalendarClick();
	void OnMenu_ProjManagerClick();

	void On_bannerClicked(const QUrl & link);
	void OnCloseTab();
	void OnFuiTabSelected(int nIndex);
	void OnFuiChanged();
	void OnFuiTabWantsFullScreen();
	void OnSplitterMoved();
	void OnListenSkypeCalls_UpdateUI();
	void UpdateTime();
	void OnBannerLink(const QString& strLink);
	void OnCheckInAllChangedDocuments();

	//menus:
	void OnStyledMenuClick(int nMenuButtonID,bool bCTRL,bool bALT);
	void on_MenuButton_clicked(int nMenuButtonID);

	//-----------------------------------------------------------
	//						MENU TRIGGERS
	//-----------------------------------------------------------
	
	void OnMenu_OnLogin();
	void OnMenu_OnLogout();
	void OnMenu_ChangePass();
	void OnMenu_Options();
	void OnMenu_PersonalSettings();
	void OnMenu_Backup();

	void OnMenu_Cut();
	void OnMenu_Copy();
	void OnMenu_Paste();
	void OnMenu_ViewToolbar();
	void OnMenu_ViewStatusbar();
	void OnMenu_ViewBanner();
	void OnMenu_ViewFullScreen();
	void OnMenu_ViewDlgManager();
	
	void OnMenu_OpenComm();
	void OnMenu_OpenContact();
	void OnMenu_OpenProject();
	
	void OnMenu_ListenSkypeCalls();
	void OnMenu_NewVoiceCall();
	void OnMenu_Users();
	
	void OnMenu_TypeDefintions();
	void OnMenu_CE_Type_Document();
	void OnMenu_CE_Type_Email();
	void OnMenu_CE_Type_Voice();
	void OnMenu_CE_Type_Calendar();
	void OnMenu_AddressSchema();
	void OnMenu_RoleDefintions();
	void OnMenu_PersonRoleDefintions();
	void OnMenu_Resources();
	void OnMenu_CalendarViews();
	void OnMenu_PaymentConditions();
	void OnMenu_CustomFields();

	//void OnMenu_CalendarEvent();
	//void OnMenu_CalendarFui();

	void OnMenu_ImportContacts();
	void OnMenu_ImportProjects();
	void OnMenu_ImportUsers();
	void OnMenu_ImportEmails();
	void OnMenu_ImportUserContactRel();
	void OnMenu_ImportContactContactRel();
	void OnMenu_ImportGroupAssignments();
	void OnMenu_ImportStoredProjectLists();
	void OnMenu_ImportUserDefinedFields();
	void OnMenu_ImportAppointments();
	void OnMenu_ProjectsSync();
	void OnMenu_UserSync();
	void OnMenu_DebtorSync();
	void OnMenu_ExportProjects();
	void OnMenu_ExportUsers();
	void OnMenu_ExportContacts();
	void OnMenu_ExportContactContactRel();
	void OnMenu_ImportCenter();

	void OnMenu_PortableSkypeInstall();
	void OnMenu_PortableSkypeUpdate();
	void OnMenu_PortableAppRegister();

	void OnMenu_Reporting();
	void OnMenu_ActualReports();
	//void OnMenu_SetupPrinter();
	void OnMenu_ThickCheckForUpdates();
	void OnMenu_OnlineHelp();
	void OnMenu_About();
	void OnMenu_Support();
	void OnMenu_HelpFirstStep();

	void OnHeader_SwitcherClick();
	void OnSwitcher_Pro();
	void OnSwitcher_Light();
	void OnSwitcher_Sidebar();

private:
	void OpenDesktopFUIWithViews();
	void ChangeMainToolBarFocus(int nMenuButtonID);
	void OpenStartUpFUIs();
	bool PassKeyEventToCommmenu(QKeyEvent * event);

	int m_nToolBarButtonIDCurrentFocus;
	//QAssistantClient *m_pHelpClient;
	bool m_bLoginExecuted;
	int m_nCurrentWindowTheme;
	ClientImportExportManager *m_ImportExportManager;	//import/export manager


	//dynamic menues (stored as actions inside menuBar):
	QAction*	m_pMenuFile;
	QAction*	m_pMenuEdit;
	QAction*	m_pMenuView;
	QAction*	m_pMenuMainFunctions;
	QAction*	m_pMenuTel;
	QAction*	m_pMenuOrg;
	QAction*	m_pMenuDataImport;
	QAction*	m_pMenuPrint;
	QAction*	m_pMenuWindows;
	QAction*	m_pMenuSupport;
	QAction*	m_pMenuPortableSkypeInstall;
	QAction*	m_pMenuPortableSkypeUpdate;
	QAction*	m_pMenuPortableAppRegister;
	QAction*	m_pMenuHelp;
	QMenu*		m_menuWindows;		//static for displaying currently open windows (FUIs)

	//menu actions (stored to be disabled, enabled, checked, etc..)
	QAction*	m_pActLogin;
	QAction*	m_pActLogout;
	QAction*	m_pActChangePass;
	QAction*	m_pActAdminTool;
	QAction*	m_pActQuit;
	
	QAction*	m_pActViewToolBar;
	QAction*	m_pActStatusBar;
	QAction*	m_pActFullScreen;
	QAction*	m_pActBanner;
	QAction*	m_pActDlgManager;
	
	QAction*	m_pActCut;
	QAction*	m_pActCopy;
	QAction*	m_pActPaste;

	//main funct
	QAction*	m_pActOpenContact;
	QAction*	m_pActOpenProject;
	QAction*	m_pActOpenComm;

	//FUI's:
	QAction*	m_pActFUI_Options;
	QAction*	m_pActFUI_PersonalSettings;
	
	QAction*	m_pActListen_for_Skype_calls;
	QAction*	m_pActFUI_NewVoiceCall;
	
	QAction*	m_pActFUI_Users;
	QAction*	m_pActFUI_TypeDefinitions;
	QAction*	m_pActFUI_CE_Type_Document;
	QAction*	m_pActFUI_CE_Type_Email;
	QAction*	m_pActFUI_CE_Type_Voice;
	QAction*	m_pActFUI_CE_Type_Calendar;

	QAction*	m_pActFUI_AddressSchema;
	QAction*	m_pActFUI_RoleDefinitions;
	QAction*	m_pActFUI_PersonRoleDefinitions;
	QAction*	m_pActFUI_Resources;
	QAction*	m_pActFUI_CalendarViews;
	QAction*	m_pActFUI_CalendarEvent;
	QAction*	m_pActFUI_Calendar;
	QAction*	m_pActFUI_PaymentConditions;
	QAction*	m_pActFUI_CustomFields;

	QAction*	m_pActFUI_ImportContacts;
	QAction*	m_pActFUI_ImportProjects;
	QAction*	m_pActFUI_ImportUsers;
	QAction*	m_pActFUI_ImportEmails;
	QAction*	m_pActFUI_ImportAppointments;
	QAction*	m_pActFUI_ImportUserContactRel;
	QAction*	m_pActFUI_ImportContactContactRel;
	QAction*	m_pActFUI_ImportGroupAssignments;
	QAction*	m_pActFUI_ImportStoredProjectLists;
	QAction*	m_pActFUI_ImportUserDefinedFields;
	QAction*	m_pActFUI_ExportProjects;
	QAction*	m_pActFUI_ExportUsers;
	QAction*	m_pActFUI_ExportContacts;
	QAction*	m_pActFUI_ExportContactContactRel;
	QAction*	m_pActFUI_ProjectsSync;
	QAction*	m_pActFUI_UserSync;
	QAction*	m_pActFUI_DebtorSync;
	QAction*	m_pActFUI_ImportCenter;



	QAction*	m_pActFUI_Reporting;
	QAction*	m_pActFUI_ActualReports;
	QAction*	m_pActFUI_SetUpPrinter;

	QAction*	m_pActFUI_Support;

	QAction*	m_pActFUI_ThickUpdateCheck;
	QAction*	m_pActFUI_OnlineHelp;
	QAction*	m_pActFUI_HelpFirstStep;
	QAction*	m_pActFUI_About;
	
	QAction		*m_pView_Pro;
	QAction		*m_pView_Light;
	QAction		*m_pView_SideBar;
	
	StyledPushButton *m_switcher;
	StyledPushButton *m_pBtnComm;
	StyledPushButton *m_pBtnProject;
	StyledPushButton *m_pBtnContact;
	StyledPushButton *m_pBtnCalendar;
	StyledPushButton *m_pBtnProjManager;
};

#endif // SOKRATES_SPC_H
