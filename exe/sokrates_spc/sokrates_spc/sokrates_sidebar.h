#ifndef SOKRATES_SIDEBAR_H
#define SOKRATES_SIDEBAR_H

#include <QWidget>
#include "ui_sokrates_sidebar.h"

#include <QSystemTrayIcon>
#include <QStackedWidget>
#include "gui_core/gui_core/styledpushbutton.h"
#include "sidebar_handle.h"
#include <QStatusBar>
#include <QProgressBar>
#include "fui_collection/fui_collection/dlg_messenger.h"

class Sokrates_SideBar : public QWidget
{
	Q_OBJECT

public:
	Sokrates_SideBar(QWidget *parent = 0);
	~Sokrates_SideBar();

	enum MENU_BUTTON
	{
		MENU_BUTTON_DESKTOP,
		MENU_BUTTON_CONTACT,
		MENU_BUTTON_PROJECT,
		MENU_BUTTON_CALENDAR
	};

public slots:
	void Initialize();		//called on startup
	void OnClientLogged();
	void OnClientLogout();
	void OnProcessBeforeLogout();
	void OnCommunicationFatalError(int nErrorCode);
	void OnThemeChanged();
	void OnViewChanged();

private slots:
	//void OnScreenGeometryChanged(int);
	void OnCommunicationInProgress(int nTicks,qint64 done, qint64 total);

	void OnMouse_DesktopEnter();
	void OnMouse_DesktopLeave();
	void OnMouse_ContactEnter();
	void OnMouse_ContactLeave();
	void OnMouse_ProjectEnter();
	void OnMouse_ProjectLeave();
	void OnMouse_CalendarEnter();
	void OnMouse_CalendarLeave();

	void OnMenu_DesktopClick();
	void OnMenu_ContactClick();
	void OnMenu_ProjectClick();
	void OnMenu_CalendarClick();
	void on_MenuButton_clicked(int nMenuButtonID);

	void OnMenu_PortableSkypeInstall();
	void OnMenu_PortableSkypeUpdate();
	void OnMenu_PortableAppRegister();

	void OnHeader_HelpClick();
	void OnHeader_HideClick();
	void OnHeader_CloseClick();
	void OnHeader_SwitcherClick();

	void OnMenu_Support();

	void OnCustomContextMenu_Switcher(const QPoint &);
	void OnSwitcher_Pro();
	void OnSwitcher_Light();
	void OnSwitcher_Sidebar();
	void MoveToSide(bool bLeft=true,bool bSetY=false, bool bAdjustOnFirstShow=false);
	void onFrameChanged_WorkingIcon(int);
	
	void OnResizedDelayed();
	void OnResizeToMin(); //used by resizeEvent();
	void OnChildChapterResized(int nFuiTypeID,int nChapterIdx,bool bBothChaptersCollapsed); 

	void OnMenu_HelpFirstStep();
	void OnMenu_OnlineHelp();
	void OnMenu_About();
	void OnMenu_PersonalSettings();
	void OnLoginDialogCreated(QDialog *pDlg);
	void OnLoginDialogBeforeDestroy(QDialog *pDlg);
	void OnCheckInAllChangedDocuments();


protected:
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent * event);
	void closeEvent ( QCloseEvent * event );
	void resizeEvent ( QResizeEvent * event );
	void timerEvent ( QTimerEvent * event ) ;
	void keyPressEvent( QKeyEvent * event ); 

signals:
	void ResizeDelayed(int nHeight);

private:
	void SetupButtons();
	void OpenDesktopFUIWithViews();
	void SetMenuText(QString strText);
	void SetClockText(QString strText);
	void UpdateClock();
	void ChangeMainToolBarFocus(int nMenuButtonID, bool bCheckIfAlreadySet=true);
	void AdjustSizeOnLogon();
	bool PassKeyEventToCommmenu(QKeyEvent * event);

	int m_nToolBarButtonIDCurrentFocus;
	QPoint m_dragPosition;

	Ui::Sokrates_SideBarClass ui;
	
	bool m_bBothChaptersCollapsed;
	QLabel *m_pInfo;
	QLabel *m_pClock;
	StyledPushButton *m_pBtnComm;
	StyledPushButton *m_pBtnProject;
	StyledPushButton *m_pBtnContact;
	StyledPushButton *m_pBtnCalendar;

	QSystemTrayIcon	m_SysTray;
	bool m_bResizeInProgress;
	bool m_bVertResizeInProgress;
	QStackedWidget *m_stackWidget;

	SidebarHandle *m_pSidebarHandleWnd;
	StyledPushButton *m_switcher;
	QAction *m_pView_Pro;
	QAction *m_pView_Light;
	QAction *m_pView_SideBar;
	bool			m_bWindowInitialized;
	bool			m_bLoginExecuted;
	bool			m_bOnClientLoggedProcedureExecutionInProgress;
	int				m_nCurrentWindowTheme;

	//QRect m_desktopRec;
	QStatusBar  *m_statusBar;
	QProgressBar *m_ProgressBar;
	Dlg_Messenger DlgMessenger;
	QLabel *m_labelWorkingIcon;

	int m_nTimer;
	int m_nWindowHeight;
	int m_nResizeinProgressInFui_ID;
	int m_nResizeinProgressInFuiChapter_Idx;

	StyledPushButton *m_help;

};

#endif // SOKRATES_SIDEBAR_H
