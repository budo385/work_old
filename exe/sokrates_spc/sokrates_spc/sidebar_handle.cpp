#include <QtGui>
#include "sidebar_handle.h"
#include <QDebug>
#include <QApplication>
#include <QDesktopWidget>
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;


SidebarHandle::SidebarHandle(QWidget *pWnd)
    : QWidget(NULL, Qt::FramelessWindowHint)//|Qt::SubWindow|Qt::SplashScreen)
{
	setVisible(false);
	//setAttribute(Qt::WA_ContentsPropagated);
	setWindowFlags(Qt::WindowStaysOnTopHint);
	this->setWindowOpacity(0.85);

	//make fixeD:
	QSize maxSize(25,94);
	QSize minSize(25,94);
	setMaximumSize(maxSize);
	setMinimumSize(minSize);
	QSizePolicy pol(QSizePolicy::Fixed,QSizePolicy::Fixed);
	setSizePolicy(pol);

	m_pWndControlled = pWnd;
	m_bImageLeftSide = true;
	m_bWindowVisible = true;
	m_pixCurrent = QPixmap(":Handle_Left_Arrow_Left.png");

	QImage image(":Handle_Left_Arrow_Left.png");
	//QImage mask = image.createHeuristicMask();
	QImage mask = image.createAlphaMask();
	QBitmap bmpMask = QBitmap::fromImage(mask);
	setMask(bmpMask);



	//resize(24,93);
	m_nTimer=-1;


	//desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
	//connect(dynamic_cast<QApplication*>(qApp)->desktop(),SIGNAL(workAreaResized (int)),this,SLOT(OnScreenGeometryChanged(int)));

	//m_WinPosition = m_pWndControlled->pos();
}


void SidebarHandle::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
	painter.setBackgroundMode(Qt::TransparentMode);
	painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
	painter.drawPixmap(0, 0, m_pixCurrent);
}

QSize SidebarHandle::sizeHint() const
{
    return QSize(24,93);
}


void SidebarHandle::FlipState()
{
	if(m_pWndControlled->isVisible())
	{
		m_pWndControlled->hide();
		//m_WinPosition = m_pWndControlled->pos();
		m_bWindowVisible = false;
		ShowHandle();
	}
	else
	{
		m_pWndControlled->show();
		m_bWindowVisible = true;
		Slide_WndControlled();
		HideHandle();
	}
	//UpdateHandleBitmap();
}

void SidebarHandle::UpdateHandleBitmap()
{
	QImage image;
	if(m_bImageLeftSide)
	{
		image = QImage(":Handle_Left_Arrow_Left.png");
		m_pixCurrent = QPixmap(":Handle_Left_Arrow_Left.png");

		/*
		image = QImage(":Handle_Left_Arrow_Left.png");
		if(m_bWindowVisible)
			m_pixCurrent = QPixmap(":Handle_Left_Arrow_Right.png");
		else
			m_pixCurrent = QPixmap(":Handle_Left_Arrow_Left.png");
		*/
	}
	else
	{
		image = QImage(":Handle_Right_Arrow_Right.png");
		m_pixCurrent = QPixmap(":Handle_Right_Arrow_Right.png");

		/*
		if(m_bWindowVisible)
			m_pixCurrent = QPixmap(":Handle_Right_Arrow_Left.png");
		else
			m_pixCurrent = QPixmap(":Handle_Right_Arrow_Right.png");
		*/
	}

	//QImage mask = image.createHeuristicMask();
	QImage mask = image.createAlphaMask();
	QBitmap bmpMask = QBitmap::fromImage(mask);
	setMask(bmpMask);
}


void SidebarHandle::ShowHandle()
{
	//show on far left or far right....
	QRect desktopRec = QApplication::desktop()->availableGeometry(m_pWndControlled);

	bool bTwoScreenReversed = g_pSettings->GetPersonSetting(TWO_SCREEN_REVERSED).toBool();

	// update icon, if 65 pix< 0 then left:
	bool bLeftSide = true;
	if((m_pWndControlled->geometry().left() - desktopRec.x()-65)<=0)
	{
		bLeftSide = false;
	}


	bool bUpdateBitmap=false;

	if(m_bImageLeftSide != bLeftSide)
	{
		//changed sides
		m_bImageLeftSide = bLeftSide;
		bUpdateBitmap=true;
	}

	int nScr=QApplication::desktop()->screenNumber(m_pWndControlled);
	if (QApplication::desktop()->screenCount()>1)
	{	
		if (nScr==(QApplication::desktop()->screenCount()-1))
		{
			m_bImageLeftSide=true; //means handle is on right side, but pointing to the left, o jea...
		}
		else
		{
			m_bImageLeftSide=false;
		}
	}
	else
		bTwoScreenReversed=false; //only valid if scr>1


	if (bTwoScreenReversed)
		m_bImageLeftSide=!m_bImageLeftSide;


	//if (bUpdateBitmap)
	UpdateHandleBitmap();

	//qDebug()<<this->width();
	//qDebug()<<this->width();
	//qDebug()<<frameGeometry().width();

	//move handler to left or right side:
	if (m_bImageLeftSide)
	{
		//update move to right side (handler points to ,left)
		int nY=desktopRec.y()+desktopRec.height()/2-this->height()/2;
		int nX=desktopRec.x()+desktopRec.width()-28;//this->width();
		//int nX=desktopRec.x()+desktopRec.width()-30;
		move(nX,nY);
	}
	else
	{
		//update move to left side (handler points to right)
		int nY=desktopRec.y()+desktopRec.height()/2-this->height()/2;
		//int nY=m_pWndControlled->y();
		int nX=desktopRec.x()-4;
		move(nX,nY);
	}

	

	//move parent (always to align)
	if (m_bImageLeftSide)
	{
		//update move to right side (handler points to left)
		//int nY=desktopRec.y()+desktopRec.height()/2-m_pWndControlled->height()/2;
		int nX=desktopRec.x()+desktopRec.width();//-m_pWndControlled->width();
		//m_pWndControlled->move(nX,this->y());
		m_pWndControlled->move(nX,m_pWndControlled->y());
	}
	else
	{
		//update move to left side (handler points to right)
		//int nY=desktopRec.y()+desktopRec.height()/2-m_pWndControlled->height()/2;
		int nX=desktopRec.x()-m_pWndControlled->width();
		//m_pWndControlled->move(nX,this->y());
		m_pWndControlled->move(nX,m_pWndControlled->y());
	}

	//qDebug()<<this->mapToGlobal(QPoint(0,0)).x();
	//qDebug()<<this->mapToGlobal(QPoint(0,0)).y();
	show();

}




void SidebarHandle::Slide_WndControlled()
{
	m_nTimer=startTimer(1);
	m_nStep=0;
}

void SidebarHandle::timerEvent ( QTimerEvent * event ) 
{
	QRect desktopRec = QApplication::desktop()->availableGeometry(this);

	if (event->timerId()==m_nTimer)
	{
		m_nStep++;
		if (m_nStep<=20)
		{
			int nStepFoward=m_pWndControlled->width()/20;
			if (m_bImageLeftSide)
				m_pWndControlled->move(m_pWndControlled->geometry().left()-nStepFoward,m_pWndControlled->geometry().top());
			else
				m_pWndControlled->move(m_pWndControlled->geometry().left()+nStepFoward,m_pWndControlled->geometry().top());
		}
		else
		{
			killTimer(m_nTimer);
			if (m_bImageLeftSide)
			{
				int nX=desktopRec.x()+desktopRec.width()-m_pWndControlled->width();
				m_pWndControlled->move(nX,m_pWndControlled->geometry().top());
			}
			else
			{
				int nX=desktopRec.x();
				m_pWndControlled->move(nX,m_pWndControlled->geometry().top());
			}
		}
	}
}

/*
void SidebarHandle::OnScreenGeometryChanged(int)
{
	desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
}
*/


void SidebarHandle::mousePressEvent(QMouseEvent *event)
{
	m_bMoveInProgress=false;
	if (event->button() == Qt::LeftButton) 
	{
		m_dragPosition = event->globalPos() - frameGeometry().topLeft();
		event->accept();
	}
	else
		m_dragPosition=QPoint(0,0);

	m_nStartX=event->globalPos().x();

	//QMainWindow::mousePressEvent(event); //pass event through
} 


void SidebarHandle::mouseMoveEvent(QMouseEvent *event)
{
	if (event->buttons() & Qt::LeftButton && m_dragPosition!=QPoint(0,0)) 
	{
		//moving vertical:
		QPoint diff=event->globalPos();
		diff.setX(m_nStartX);
		move(diff-m_dragPosition);
		event->accept();
		m_dragPosition = diff - frameGeometry().topLeft();
		m_bMoveInProgress=true;
	}


	//m_pSidebarHandleWnd->UpdatePosition();
} 

void SidebarHandle::mouseReleaseEvent(QMouseEvent * event)
{
	if (m_bMoveInProgress)
	{
		event->accept();
		//muv parent right or left:

		//move parent (always to align)

		// update icon, if 65 pix< 0 then left:
		/*
		bool bLeftSide = true;
		if((this->frameGeometry().left() - desktopRec.x()-85)<=0)
		{
			bLeftSide = false;
		}
		if(m_bImageLeftSide != bLeftSide)
		{
			//changed sides
			m_bImageLeftSide = bLeftSide;
			UpdateHandleBitmap();
		}
		if (m_bImageLeftSide)
		{
			int nX=desktopRec.x()+desktopRec.width();//-m_pWndControlled->width();
			m_pWndControlled->move(nX,this->y());
		}
		else
		{
			int nX=desktopRec.x()-m_pWndControlled->width();
			m_pWndControlled->move(nX,this->y());
		}
		*/
		return;
	}
	else
	{
		FlipState();
	}
}