#include "sokrates_spc.h"
//GET CLIENT OBJECTS:

#include "fui_collection/fui_collection/calendartaskwidget.h"

 //defines FP codes
//CREATE GUI GLOBAL OBJECTS:
#include "globalobjectmanager.h"
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;
#include "fui_collection/fui_collection/communicationmanager.h"
extern CommunicationManager g_CommManager;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "gui_core/gui_core/thememanager.h"
#include "bus_core/bus_core/importexportmanagerabstract.h"
extern ImportExportManagerAbstract *g_pImportExportManager;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger
#include "globalobjectmanager.h"
extern GlobalObjectManager *g_pGlobalObjectManager;
#include "bus_client/bus_client/clientdownloadmanager.h"
extern ClientDownloadManager		*g_DownloadManager;

#include "common/common/statuscodeset.h"
#include "fui_collection/fui_collection/fui_voicecallcenter.h"
#include "fui_collection/fui_collection/calendarhelper.h"

#include "os_specific/os_specific/skypemanager.h"
#include "common/common/config_version_sc.h"
#include "fui_collection/fui_collection/reportmanager.h"
#include "common/common/datahelper.h"
#include <QtWidgets/QMessageBox>
#include <QToolButton>
#include <QDesktopWidget>
#include "gui_core/gui_core/dateselectordlg.h"
#include <QScrollBar>
#include "fui_collection/fui_collection/fui_importemail.h"
#include "fui_collection/fui_collection/fui_importappointment.h"
#include "fui_collection/fui_collection/fui_communicationcenter.h"
#include "fui_collection/fui_collection/fui_contacts.h"
#include "fui_collection/fui_collection/dlg_passwordchange.h"
#include "bus_client/bus_client/selectionpopupfloat.h"
#include "fui_collection/fui_collection/admintool.h"
#include "fui_collection/fui_collection/fui_calendarevent.h"
#include "fui_collection/fui_collection/fui_calendar.h"
#include "fui_collection/fui_collection/fui_invitationcard.h"

#include "gui_core/gui_core/aboutwidget.h"
#include "bus_core/bus_core/formatphone.h"
#include "fui_collection/fui_collection/fui_ce_types.h"
#include "bus_core/bus_core/globalconstants.h"
#include "fui_collection/fui_collection/dlg_contactaddressschemas.h"
#include "fui_collection/fui_collection/wizardregistration.h"
#include "netviewerdlg.h"
#include "common/common/threadid.h"
#include <QDesktopServices>
#include "bus_client/bus_client/documenthelper.h"

#include "fui_collection/fui_collection/importcenter.h"
#include "os_specific/os_specific/stylemanager.h"
#include "fui_collection/fui_collection/startupwizard.h"
#include "fui_collection/fui_collection/emaildialog.h"
#include "fui_collection/fui_collection/fui_projects.h"
#include "os_specific/os_specific/mapimanager.h"
#include "bus_client/bus_client/simpleselectionwizfileselectionexp.h"
#include "common/common/csvimportfile.h"
#include <QFileDialog>
#include "bus_core/bus_core/customavailability.h"
#include "bus_client/bus_client/clientapihelper.h"

//---------------------------------TO BE CLEAR---------------------------------------

#ifdef _WIN32
#include "os_specific/os_specific/mapi/mapiex.h"
long CALLBACK OnNewMessage(LPVOID lpvContext,ULONG cNotification,LPNOTIFICATION	pNotif);
#endif


int GetMainMenuCount();
int GetHtmlTemplateCount();
void OnFuiClose(QWidget *pWidget, QWidget *pMainWnd);
bool CsvProgress(unsigned long nUserData);

#ifdef _WIN32
 #include "os_specific/os_specific/skype/skypeparser.h"
 SkypeParser	g_objParser;
#endif

extern QString g_strLastDir_FileOpen;

//QStyle g_DefaultApplictionStyle;

//temporary: application version of Communicator, later it will be as set of FP key loaded from keyfile
//for now: 
//int g_nApplicationVersion;


//---------------------------------TO BE CLEAR---------------------------------------



sokrates_spc::sokrates_spc(QWidget *parent, Qt::WindowFlags flags)
: QMainWindow(parent, flags),m_pScene(NULL),m_menuWindows(NULL),m_pPix1(NULL),m_pPix2(NULL),m_pView(NULL),m_pInfo(NULL),m_nCurrentWindowTheme(-1),m_bLoginExecuted(false)
{

	ui.setupUi(this);
	//m_pDropOutlookMimeHandler= new WindowsMimeOutlook; //bring up outlook handler;
	//GlobalObjectManager::InitGlobalHandlers();


	//DbRecordSet rec;
	//rec.addColumn(DbRecordSet::GetVariantType(),"XX");
	//qDebug()<<rec.getColumnType(0);

	//init help system
	//m_pHelpClient = NULL;
	/*
	m_pHelpClient = new QAssistantClient(QCoreApplication::applicationDirPath(), this);
	QStringList arguments;
	arguments << "-profile" << QCoreApplication::applicationDirPath() + QDir::separator() + QString("help") + QDir::separator() + QString("SOKRATES_Communicator.adp");
	m_pHelpClient->setArguments(arguments);
	*/

	g_ChangeManager.registerObserver(this);

	//common window flags:
	setAttribute(Qt::WA_QuitOnClose);
	setAttribute(Qt::WA_DeleteOnClose);
	connect(this, SIGNAL(destroyed(QObject*)), qApp, SLOT(quit()));


	//--------------------------BUILD DEFAULT GUI---------------------------------------

	m_nToolBarButtonIDCurrentFocus=-1;

	//g_nApplicationVersion=SOKRATES_COMMUNICATOR_VERSION_TEAM;

	//BANNER:
	SetupBanner();

	//MAIN MENU
	//m_StyleMenuManager = new StyledMenuManager(ui.MainMenu);	
	//m_StyleMenuManager->BuildMenu(0,g_arMainMenu,GetMainMenuCount()); //build
	//connect(m_StyleMenuManager,SIGNAL(MenuButton_clicked(int,bool,bool)),this,SLOT(OnStyledMenuClick(int,bool,bool)));

	//LIGHT TOOLBAR
	BuildToolBar();

	//MENU ACTIONS:
	SetupMenuActions();
	SetupMenu(false);

	//default:
	ui.mainToolBar->setVisible(false);
	ui.frameToolBar->setVisible(false);


	//--------------------------SETUP STATUS BAR & LOG MSG BUTTON---------------------------------------

	//progress m_ProgressBar on status m_ProgressBar:
	m_ProgressBar = new QProgressBar;
	QSize sizeBar(150,14);
	m_ProgressBar->setMaximumSize(sizeBar);
	ui.statusBar->addPermanentWidget(m_ProgressBar);
	m_ProgressBar->setTextVisible(false);
	m_ProgressBar->setRange(0,9);
	m_ProgressBar->setVisible(false);
	

	//log msg:
	QSize buttonSize(24, 24);
	QToolButton *btnOpenLog= new QToolButton;
	btnOpenLog->setMaximumSize(buttonSize);
	btnOpenLog->setAutoRaise(true);
	ui.statusBar->addPermanentWidget(btnOpenLog);
	DlgMessenger.Initialize(btnOpenLog,ui.statusBar);


	//--------------------------INIT FUI MANAGER---------------------------------------
	//Warning: must be last, after GUI init
	connect(&g_objFuiManager, SIGNAL(TabWantsToMaximizeToFullScreen()), this, SLOT(OnFuiTabWantsFullScreen()));
	connect(&g_objFuiManager, SIGNAL(FuiChanged()), this, SLOT(OnFuiChanged()));
	connect(ui.banner, SIGNAL(anchorClicked(const QUrl&)), this, SLOT(On_bannerClicked(const QUrl&)));
	g_objFuiManager.Initialize(this, ui.stackedWidget, m_menuWindows);
	
	
	//--------------------------INIT LOGIN CONTROLLER---------------------------------------
	//connect(g_pClientManager, SIGNAL(ClientLogged()), this, SLOT(OnClientLogged()));
	//connect(g_pClientManager, SIGNAL(ClientLogout()), this, SLOT(OnClientLogout()));
	connect(g_pClientManager, SIGNAL(ClientClosedLoginWnd()), this, SLOT(OnClientClosedLoginWnd()));
	//connect(g_pClientManager, SIGNAL(CommunicationFatalError(int)),this,SLOT(OnCommunicationFatalError(int)));
	//connect(g_pClientManager, SIGNAL(ClientBeforeLogout()),this,SLOT(OnProcessBeforeLogout()));
	connect(g_pClientManager, SIGNAL(ServerMessageReceived(int, QString)),this,SLOT(OnServerMessageReceived(int,QString)));
	g_CommManager.Initialize();

	m_ImportExportManager=NULL;
	m_ImportExportManager=static_cast<ClientImportExportManager*>(g_pClientManager->GetImportExportManager());
	UpdateMenuState();

	
}

sokrates_spc::~sokrates_spc()
{
	g_pClientManager->StopProgressDialog(); //issue 2735, stop progress dialog at all costs...
	g_pGlobalObjectManager->OnApplicationClose(this);
}

void sokrates_spc::CreateLayout()
{
	SetupGUI();
	//if (g_nApplicationVersion==SOKRATES_COMMUNICATOR_VERSION_TEAM)
	//	m_StyleMenuManager->ShowMenu(2); //open section 2 as default: TOFIX later, to lookup in menu items, rebuild based on fp's---
}

//When logged off, destroy all except baner:
bool sokrates_spc::DestroyLayout()
{
	if(g_objFuiManager.CloseAllFUIWindows(false))
	{
		//hide menu actions:
		SetupMenu(false);
		//ui.MainMenu->setVisible(false);
		ui.frameToolBar->setVisible(false);
		return true;
	}
	return false;
}




void sokrates_spc::On_bannerClicked(const QUrl & link)
{
	QUrl dummy;
	ui.banner->setSource(dummy);

	QString strPath;
	strPath = link.path();

	//TOFIX handle different banner link clicks
	QMessageBox::information(this, "", "Handle action here!");
}

void sokrates_spc::OnClientLogged()
{
	if(!g_pClientManager->IsLogged())return;
	if (m_bLoginExecuted) return;
	m_bLoginExecuted=true;

	QApplication::setOverrideCursor(Qt::WaitCursor);

	OnThemeChanged();
	CreateLayout();
	ApplyFunctionPoints();
	ApplyPersonSettings();

	//hide banner if screen size not big enough (MB request)
	int nHeight = QApplication::desktop()->rect().height();
	if(nHeight < 920){
		if(ui.banner->isVisible()){
			ui.banner->setVisible(false);
			m_pActBanner->setChecked(false);
		}
	}

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
		m_pBtnCalendar->setVisible(false);
	else
		m_pBtnCalendar->setVisible(true);

	if(!g_FunctionPoint.IsFPAvailable(FP_RESOURCE_FUI))
		m_pActFUI_Resources->setVisible(false);
	else
		m_pActFUI_Resources->setVisible(true);

	//if(CustomAvailability::IsAvailable_AS_CalendarViewWizard())
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
		m_pActFUI_CalendarViews->setVisible(false);
	else
		m_pActFUI_CalendarViews->setVisible(true);
	
	OpenStartUpFUIs();

	QApplication::restoreOverrideCursor();
	g_pClientManager->CheckServerMessagesAfterLogin(); //spawn msges
}



//called after logout from server
//just close mapi:
void sokrates_spc::OnClientLogout()
{
	FUI_ImportEmail::ClearInternalCache();
	m_bLoginExecuted=false;
}


//not logged, but can browse menus:
void sokrates_spc::OnClientClosedLoginWnd()
{
	SetupMenu(false);		//to be on safe side, disable all menu actions
}

void sokrates_spc::OnCloseTab()
{
	g_objFuiManager.CloseCurrentFuiTab();
}


void sokrates_spc::OnFuiTabSelected(int nIndex)
{
	g_objFuiManager.OnFuiTabSelected(nIndex);
}

void sokrates_spc::OnFuiWindowClose(QWidget *pWindow)
{
	g_objFuiManager.OnFuiWindowClose(pWindow);
}

void OnFuiClose(QWidget *pWidget, QWidget *pMainWnd)
{
	((sokrates_spc *)pMainWnd)->OnFuiWindowClose(pWidget);
}

void sokrates_spc::resizeEvent ( QResizeEvent * event )
{
	SetupBanner();

	//after splitter to avoid flickering
	QWidget::resizeEvent(event);
}

void sokrates_spc::OnSplitterMoved()
{
	//QList<int> lstSizes = ui.splitter->sizes();
	//m_nMenuWidth = lstSizes[0];
}

void sokrates_spc::closeEvent(QCloseEvent *event)
{
	if(!g_objFuiManager.CloseAllFUIWindows(false))
	{
		event->ignore();
		return;
	}

	//reload check-out docs:
	//issue 2675:
	//g_pSettings->SetPersonSetting(DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS,0);
	//bool bX = g_pSettings->GetPersonSetting(DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS).toBool();
	//int nX = g_pSettings->GetPersonSetting(PERSON_DEFAULT_DOCUMENT_TYPE).toInt();
	int nTestIfThereAreCheckOutDocs = g_pSettings->GetPersonSetting(DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS).toInt();
	if (nTestIfThereAreCheckOutDocs)
	{
		DbRecordSet lstDummy;
		DocumentHelper::GetCheckedOutDocs(lstDummy,false);
		if (lstDummy.getRowCount()>0)
		{
			int nResult = QMessageBox::question(this, "", QString(tr("There are %1 checked out documents. Quit anyway?")).arg(lstDummy.getRowCount()), tr("Yes"),tr("No"));
			if (nResult!=0)
			{
				event->ignore();
				return;
			}
		}
	}

	/*
	if (DocumentHelper::DWatcher_CheckForUnsavedDocuments())
	{
		event->ignore();
		QTimer::singleShot(0,this,SLOT(OnCheckInAllChangedDocuments()));
		return;
	}
	*/


	//if active downloads:
	if (g_DownloadManager->IsOperationInProgress())
	{
		int nResult = QMessageBox::question(this, "", tr("There are active download/upload operations. Do you want to terminate those operations and close application?"), tr("Yes"),tr("No"));
		if (nResult!=0)
		{
			event->ignore();
			return;
		}
	}


	g_pClientManager->Logout(); //try logout before destroy
	//if (m_pHelpClient)
	//	m_pHelpClient->closeAssistant();

	event->accept();

}





void sokrates_spc::OnListenSkypeCalls_UpdateUI()
{
	//update menu action "checked" state
	m_pActListen_for_Skype_calls->setChecked(SkypeManager::GetListenIncomingCalls() && SkypeManager::IsSkypeLogged());
}

void sokrates_spc::SetupBanner()
{
	//set banner contents
	//QString strBanner = "<html><body style=\"margin: 0px; background-color: rgb(180, 180, 180);\"><table style=\"text-align: left; width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td style=\"width: 522px;\"><img valign=\"top\" src=\":banner.png\" border=\"0\" height=\"72\" width=\"522\"></td><td style=\"background-color: rgb(180, 180, 180);\" width=\"100%\">&nbsp;&nbsp;&nbsp;<a valign=\"top\" href=\"email\">New messages (10)</a><br>&nbsp;&nbsp;&nbsp;<a href=\"phone\">Phone calls (4)</a><br>&nbsp;&nbsp;&nbsp;<a href=\"tasks\">Tasks (4)</a></td></tr></tbody></table></body></html>";
	//QString strBanner = "<html><body style=\"margin: 0px; background-color: rgb(180, 180, 180);\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\":Header_Info_Display.png\"><tr><td width=\"590\"><img src=\":SC_TE_Header_Left.png\" height=\"73\" width=\"590\" border=\"0\"></a></td><td><small style=\"color: rgb(255, 255, 255);\"><a style=\"font-weight: bold; font-family: Arial; color: rgb(255, 255, 255);\" href=\"http://www.sokrates.ch/\" target=\"_blank\">4 Voice Mail Received...</a><br style=\"font-weight: bold; font-family: Arial;\"><a style=\"font-weight: bold; font-family: Arial; color: rgb(255, 255, 255);\" href=\"http://www.sokrates.hr/\" target=\"_blank\">3 Tasks to do...</a><br style=\"background-image: url(:Header_Info_Display.png); font-weight: bold; font-family: Arial;\"><a style=\"font-weight: bold; font-family: Arial; color: rgb(255, 255, 255);\" href=\"http://www.trolltech.com\" target=\"_blank\">1 Project Alert...</a></small></td><td align=\"right\" width=\"92\" background=\":Header_Right.png\" width=\"92\"><big><span style=\"color: rgb(202, 221, 220); font-family: Arial;\">08:22</span></big></font></big><br style=\"color: rgb(202, 221, 220); font-family: Arial;\"><span style=\"color: rgb(202, 221, 220); font-family: Arial;\">10.10.2006</span></td></tr></table></body></html>";
	//QString strBanner = "<html><body style=\"margin: 0px;\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\":Header_Info_Display.png\"><tr><td width=\"590\"><img src=\":SC_TE_Header_Left.png\" height=\"73\" width=\"590\" border=\"0\"></a></td><td><small style=\"color: rgb(255, 255, 255);\"><a style=\"font-weight: bold; font-family: Arial; color: rgb(255, 255, 255);\" href=\"http://www.sokrates.ch/\" target=\"_blank\">4 Voice Mail Received...</a><br style=\"font-weight: bold; font-family: Arial;\"><a style=\"font-weight: bold; font-family: Arial; color: rgb(255, 255, 255);\" href=\"http://www.sokrates.hr/\" target=\"_blank\">3 Tasks to do...</a><br style=\"font-weight: bold; font-family: Arial;\"><a style=\"font-weight: bold; font-family: Arial; color: rgb(255, 255, 255);\" href=\"http://www.trolltech.com\" target=\"_blank\">1 Project Alert...</a></small></td><td align=\"right\" width=\"92\" background=\":Header_Right.png\" width=\"92\"><big><span style=\"color: rgb(202, 221, 220); font-family: Arial;\">08:22</span></big></font></big><br style=\"color: rgb(202, 221, 220); font-family: Arial;\"><span style=\"color: rgb(202, 221, 220); font-family: Arial;\">10.10.2006</span></td></tr></table></body></html>";
	//ui.banner->setHtml(strBanner);
	QString strModule = g_pClientManager->GetModuleCode();

	int nSceneWidth = ui.banner->geometry().width() - 5;

	if(NULL == m_pScene)
	{
		m_pScene = new QGraphicsScene(ui.banner);
		m_pScene->setSceneRect(0, 0, nSceneWidth, 71);

		QPixmap img1;
		img1.load(ThemeManager::GetHeaderLeft(strModule));

		/*
		if(strModule == "SC-TE")
			img1.load(":SC_TE_Header_Left.png");
		else if(strModule == "SC-PE")
			img1.load(":SC_PE_Header_Left.png");
		else if(strModule == "SC-BE")
			img1.load(":SC_BE_Header_Left.png");
		else if(strModule == "SC-EE")
			img1.load(":SC_EE_Header_Left.png");
		else
			img1.load(":SC_Empty_Header_Left.png");
		*/

		m_pPix1 = m_pScene->addPixmap(img1);
		m_pPix1->setOffset(QPointF(0.0, 0.0));
		m_pPix1->setPos(QPointF(0.0, 0.0));

		QPixmap img2;
		//img2.load(":Header_Right.png");
		img2.load(ThemeManager::GetHeaderRight());
		
		m_pPix2 = m_pScene->addPixmap(img2);
		m_pPix2->setOffset(QPointF(0.0, 0.0));
		m_pPix2->setPos(QPointF(nSceneWidth-90, 0.0));
		
		QFont font;
		//font.setUnderline(true);
		font.setWeight(QFont::DemiBold);
		font.setPixelSize(12);

		m_pLink1 = m_pScene->addText(QString(), font);
		//m_pLink1->setEnabled(true);
		//m_pLink1->setFlags((QGraphicsItem::ItemIsFocusable|QGraphicsItem::ItemIsSelectable));
		//pLink1->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
		//m_pLink1->setHtml("<HTML><body><a href=\"link1\"><font color=\"#FFFFFF\">3 Voice messages ...</font></a></body></HTML>");

		//m_pLink1->setHtml(QString().sprintf("<HTML><body><font color=\"#FFFFFF\">Logged User: %s</font></body></HTML>", ""));
		m_pLink1->setPos(QPointF(598, 10.0));
		connect(m_pLink1, SIGNAL(linkActivated(const QString&)), this, SLOT(OnBannerLink(const QString&)));

		m_pLink2 = m_pScene->addText(QString(), font);
		//m_pLink2->setHtml("<HTML><body><a href=\"link2\"><font color=\"#FFFFFF\">2 Voice Calls missed ...</font></a></body></HTML>");
		//m_pLink2->setHtml(QString().sprintf("<HTML><body><font color=\"#FFFFFF\">Current Connection: %s</font></body></HTML>", "Split"));
		m_pLink2->setPos(QPointF(598, 29.0));
		connect(m_pLink2, SIGNAL(linkActivated(const QString&)), this, SLOT(OnBannerLink(const QString&)));

		m_pLink3 = m_pScene->addText(QString(), font);
		//m_pLink3->setHtml("<HTML><body><a href=\"link3\"><font color=\"#FFFFFF\">4 Project Alerts ...</font></a></body></HTML>");
		m_pLink3->setPos(QPointF(598, 49.0));
		connect(m_pLink3, SIGNAL(linkActivated(const QString&)), this, SLOT(OnBannerLink(const QString&)));

		QDateTime tm = QDateTime::currentDateTime();

		font.setStyleStrategy((QFont::StyleStrategy)(QFont::PreferAntialias|QFont::PreferOutline));
		font.setFamily("Sans Serif");
		font.setWeight(QFont::Normal);
		font.setPixelSize(30);
		m_pClock1 = m_pScene->addText(QString(), font);
		m_pClock1->setHtml(QString().sprintf("<HTML><body><font color=\"#CADDDC\">%s</font></body></HTML>", tm.toString("hh:mm").toLatin1().constData()));
		m_pClock1->setPos(QPointF(nSceneWidth-82, 7.0));
		m_pClock1->setZValue(1);

		font.setPixelSize(18);
		m_pClock2 = m_pScene->addText(QString(), font);
		m_pClock2->setHtml(QString().sprintf("<HTML><body><font color=\"#CADDDC\">%s</font></body></HTML>", tm.toString("dd.MM.yy").toLatin1().constData()));
		m_pClock2->setPos(QPointF(nSceneWidth-79, 40.0));
		m_pClock2->setZValue(1);

		m_pView = new QGraphicsView(m_pScene);
		m_pView->setRenderHint(QPainter::Antialiasing);
		//m_pView->setBackgroundBrush(QPixmap(":Header_Info_Display.png"));
		m_pView->setBackgroundBrush(QPixmap(ThemeManager::GetHeaderCenter()));
		m_pView->setCacheMode(QGraphicsView::CacheBackground);
		m_pView->setAlignment(Qt::AlignLeft|Qt::AlignTop);

		QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Expanding);
		sizePolicy7.setHorizontalStretch(1);
		sizePolicy7.setVerticalStretch(1);
		m_pView->setSizePolicy(sizePolicy7);

		QHBoxLayout *layout = new QHBoxLayout();
		layout->setMargin(0);
		layout->addWidget(m_pView);
		ui.banner->setLayout(layout);

		QTimer *timer = new QTimer();
		connect(timer, SIGNAL(timeout()), this, SLOT(UpdateTime()));
		timer->start(1000);
	}
	else
	{
		m_pScene->setSceneRect(0, 0, nSceneWidth, 71);
		m_pPix2->setPos(QPointF(nSceneWidth-90, 0.0));
		m_pClock1->setPos(QPointF(nSceneWidth-82, 7.0));
		m_pClock2->setPos(QPointF(nSceneWidth-79, 40.0));
	}
}

void sokrates_spc::UpdateTime()
{
	QDateTime tm = QDateTime::currentDateTime();
	m_pClock1->setHtml(QString().sprintf("<HTML><body><font color=\"#CADDDC\">%s</font></body></HTML>", tm.toString("hh:mm").toLatin1().constData()));
	m_pClock2->setHtml(QString().sprintf("<HTML><body><font color=\"#CADDDC\">%s</font></body></HTML>", tm.toString("dd.MM.yy").toLatin1().constData()));
}

void sokrates_spc::UpdateLoginData(bool bLogged)
{
	if (bLogged)
	{
		DbRecordSet recUserData;
		g_pClientManager->GetUserData(recUserData);
		//recUserData.Dump();
		QString strCurrentConnection=g_pClientManager->GetConnectionName();

		QString strFullName = recUserData.getDataRef(0, "CUSR_FIRST_NAME").toString();
		strFullName += " ";
		strFullName += recUserData.getDataRef(0, "CUSR_LAST_NAME").toString();

		m_pLink1->setHtml(QString("<HTML><body><font color=\"#FFFFFF\">%1 %2</font></body></HTML>").arg(tr("Logged User:")).arg(strFullName));
		m_pLink2->setHtml(QString("<HTML><body><font color=\"#FFFFFF\">%1 %2</font></body></HTML>").arg(tr("Current Connection:")).arg(strCurrentConnection));

		//display current connection on the title bar
		QString strTitle = QString("SOKRATES")+QChar(174)+QString(" Communicator - ");
		strTitle += strCurrentConnection;
		setWindowTitle(strTitle);
	}
	else
	{
		m_pLink1->setHtml("");
		m_pLink2->setHtml("");
		QString strTitle = QString("SOKRATES")+QChar(174)+QString(" Communicator");
		setWindowTitle(strTitle);
	}
}

void sokrates_spc::OnBannerLink(const QString& strLink)
{
	QMessageBox::information(this, "", "Handle action here!");
}


void sokrates_spc::OnStyledMenuClick(int nMenuButtonID,bool bCTRL,bool bALT)
{

	if(nMenuButtonID!=-1)
		g_objFuiManager.OpenFUI(nMenuButtonID, bCTRL, bALT);


}





//------------------------------------------------------------------------
//					SETUP GUI
//------------------------------------------------------------------------


void sokrates_spc::SetupMenu(bool bLogged)
{

	//IF NOT LOGGED: Allow File/Login/Quit, Windows, Help
	if (!bLogged)
	{
		//FILE:
		m_pActFUI_Options->setVisible(false);
		m_pActFUI_PersonalSettings->setVisible(false);
		m_pActChangePass->setVisible(false);
		m_pActAdminTool->setVisible(false);

		//remove all others (keep actions)
		m_pMenuFile->setVisible(true);
		m_pMenuEdit->setVisible(false);
		m_pMenuView->setVisible(false);
		m_pMenuTel->setVisible(false);
		m_pMenuMainFunctions->setVisible(false);
		m_pMenuOrg->setVisible(false);
		m_pMenuDataImport->setVisible(false);
		m_pMenuPrint->setVisible(false);
		m_pMenuSupport->setVisible(false);
		m_pMenuWindows->setVisible(true);
		m_pMenuHelp->setVisible(true);

		return;
	}


		m_pActFUI_Options->setVisible(true);
		m_pActFUI_PersonalSettings->setVisible(true);
		m_pActChangePass->setVisible(true);
		m_pActAdminTool->setVisible(true);

		//remove all others (keep actions)
		m_pMenuFile->setVisible(true);
		m_pMenuEdit->setVisible(false); //issue 980
 		m_pMenuView->setVisible(true);
		m_pMenuTel->setVisible(true);
		m_pMenuMainFunctions->setVisible(true);
		m_pMenuOrg->setVisible(true);
		m_pMenuDataImport->setVisible(true);
		m_pMenuPrint->setVisible(true);
		m_pMenuSupport->setVisible(true);
		m_pMenuWindows->setVisible(true);
		m_pMenuHelp->setVisible(true);

}




//creates toolbar actions & buttons
void sokrates_spc::BuildToolBar()
{
	//LAYOUT:
	QSize buttonSize(170, 62);

	//QString	styleSectionButtonLvUp="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_Inactive.png) 4px 4px 4px 4px stretch stretch }\
	//							   QPushButton:hover {  border-width: 4px; color: white;border-image:url(:Button_Over.png) 4px 4px 4px 4px stretch stretch} \
	//							   QPushButton:pressed {  border-width: 4px; color: white;border-image:url(:Button_Pressed.png) 4px 4px 4px 4px stretch stretch} ";

	//QString	styleSectionButtonLvDown="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_Active.png) 4px 4px 4px 4px stretch stretch }";

	QString	styleNew=ThemeManager::GetToolbarButtonStyle();
	//QString	styleNew="QLabel {color:white} QWidget { background-image:url(:MainMenu_BG.png)}";

	//CREATE BUTTONS/SIZE

	m_pBtnComm		=	new StyledPushButton(this,":Desktop_G.png",":Desktop_C.png",styleNew,QString("<html><b><font size=\"5\">%1 </font></b></html>").arg(tr("Desktop")));
	m_pBtnContact	=	new StyledPushButton(this,":Contacts_G.png",":Contacts_C.png",styleNew,QString("<html><b><font size=\"5\">%1 </font></b></html>").arg(tr("Contacts")));
	m_pBtnProject	=	new StyledPushButton(this,":Projects_G.png",":Projects_C.png",styleNew,QString("<html><b><font size=\"5\">%1 </font></b></html>").arg(tr("Projects")));
	m_pBtnCalendar	=	new StyledPushButton(this,":Calendar_G.png",":Calendar_C.png",styleNew,QString("<html><b><font size=\"5\">%1 </font></b></html>").arg(tr("Calendar")));
	m_pBtnProjManager=	new StyledPushButton(this,":ProjMgr_G.png",":ProjMgr_C.png",styleNew,QString("<html><b><font size=\"5\">%1 </font></b></html>").arg(tr("Project Manager")));

	m_pInfo		= new QLabel;
	m_pInfo->setText(	QString("<html><body style=\" font-family:MS Shell Dlg 2; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\
					<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic; color:%2;\">%1</span></p>\
					</body></html>").arg(tr("Click=Open<br>CTRL-Click=Open New Window<br>ALT-Click=Open New Tab<br>ALT-CTRL-Click=Open New Selection")).arg(ThemeManager::GetToolbarTextColor()));
	

	//m_nToolBarButtonIDCurrentFocus=1;

	connect(m_pBtnComm,SIGNAL(clicked()),this,SLOT(OnMenu_DesktopClick()));
	connect(m_pBtnContact,SIGNAL(clicked()),this,SLOT(OnMenu_ContactClick()));
	connect(m_pBtnProject,SIGNAL(clicked()),this,SLOT(OnMenu_ProjectClick()));
	connect(m_pBtnCalendar,SIGNAL(clicked()),this,SLOT(OnMenu_CalendarClick()));
	connect(m_pBtnProjManager,SIGNAL(clicked()),this,SLOT(OnMenu_ProjManagerClick()));

	//size
	m_pBtnComm			->setMaximumSize(buttonSize);
	m_pBtnComm			->setMinimumSize(buttonSize);
	m_pBtnProject		->setMaximumSize(buttonSize);
	m_pBtnProject		->setMinimumSize(buttonSize);
	m_pBtnContact		->setMaximumSize(buttonSize);
	m_pBtnContact		->setMinimumSize(buttonSize);
	m_pBtnCalendar		->setMaximumSize(buttonSize);
	m_pBtnCalendar		->setMinimumSize(buttonSize);

	QSize buttonSize2(300, 62);
	m_pBtnProjManager	->setMaximumSize(buttonSize2);
	m_pBtnProjManager	->setMinimumSize(buttonSize2);
	

	//SWITCHER:
	QSize buttonSize_menu(45, 56);
	m_switcher	= new StyledPushButton(this,":Icon_SwitchSidebar.png",":Icon_SwitchSidebar.png","");
	connect(m_switcher,SIGNAL(clicked()),this,SLOT(OnHeader_SwitcherClick()));
	m_switcher	->setMaximumSize(buttonSize_menu);
	m_switcher	->setMinimumSize(buttonSize_menu);
	m_switcher	->setToolTip(tr("Switch to Sidebar View"));
	QVBoxLayout *buttonLayout_menu_v = new QVBoxLayout;
	buttonLayout_menu_v->addWidget(m_switcher);
	buttonLayout_menu_v->setSpacing(0);
	buttonLayout_menu_v->setContentsMargins(0,0,0,0);
	m_pView_Pro = new QAction(tr("Professional View"), this);
	m_pView_Light = new QAction(tr("Light View"), this);
	m_pView_SideBar = new QAction(tr("Sidebar View"), this);
	m_pView_Pro->setCheckable(true);
	m_pView_Light->setCheckable(true);
	m_pView_SideBar->setCheckable(true);
	m_pView_Pro->setChecked(true);

	connect(m_pView_Pro, SIGNAL(triggered()), this, SLOT(OnSwitcher_Pro()));
	//connect(m_pView_Light, SIGNAL(triggered()), this, SLOT(OnSwitcher_Light()));
	connect(m_pView_SideBar, SIGNAL(triggered()), this, SLOT(OnSwitcher_Sidebar()));


	//when setting frame style, filter only background, coz all child labels will inherit this:
	//QString	strFrameLayout="QFrame#frameToolBar { border-width: 4px; color: white; border-image:url(:Header_Right.png) 4px 4px 4px 4px stretch stretch }";
	QString	strFrameLayout=ThemeManager::GetToolbarBkgStyle();
		
	//Tool m_ProgressBar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;

	buttonLayout->setAlignment(Qt::AlignLeft);
	//buttonLayout->addSpacing(5);
	buttonLayout->addWidget(m_pBtnComm);
	buttonLayout->addWidget(m_pBtnContact);
	buttonLayout->addWidget(m_pBtnProject);
	buttonLayout->addWidget(m_pBtnCalendar);
	buttonLayout->addWidget(m_pBtnProjManager);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pInfo);
	buttonLayout->addLayout(buttonLayout_menu_v);

	buttonLayout->setContentsMargins(0,0,0,0);
	buttonLayout->setSpacing(1);

	ui.frameToolBar->setLayout(buttonLayout);
	ui.frameToolBar->setStyleSheet(strFrameLayout);


	//MB: told to use this FP to hide PM icon on menu (skype talk on 22.02.2013)
	if (!g_FunctionPoint.IsFPAvailable(FP_PC__BUDGETING_AND_VALUATION))
		m_pBtnProjManager->setVisible(false);

}



void sokrates_spc::SetupGUI()
{
	ui.frameToolBar->setVisible(true);
	/*
	if (g_nApplicationVersion==SOKRATES_COMMUNICATOR_VERSION_LIGHT)
	{
		//ui.MainMenu->setVisible(false);
		
	}
	else
	{
		//ui.MainMenu->setVisible(true);
		ui.frameToolBar->setVisible(false);
	}
	*/

	SetupMenu(true);
	UpdateMenuState();
}


void sokrates_spc::UpdateMenuState()
{
	m_pActViewToolBar->setChecked(ui.mainToolBar->isVisible());
	m_pActStatusBar->setChecked(ui.statusBar->isVisible());
	m_pActBanner->setChecked(ui.banner->isVisible());
	if(g_pBoSet->IsServiceStarted())
	{
		m_pActLogin->setEnabled(!g_pClientManager->IsLogged());
		m_pActLogout->setEnabled(g_pClientManager->IsLogged());
		UpdateLoginData(g_pClientManager->IsLogged());			//logged

		if(!g_AccessRight->TestAR(ADMINISTRATION_TOOLS_ACCESS))
			m_pActAdminTool->setEnabled(false);
		else
			m_pActAdminTool->setEnabled(true);

		if(!g_AccessRight->TestAR(ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU))
			m_pActFUI_AddressSchema->setEnabled(false);
		else
			m_pActFUI_AddressSchema->setEnabled(true);

		if(!g_AccessRight->TestAR(IMPORT_PROJECTS))
			m_pActFUI_ImportProjects->setEnabled(false);
		else
			m_pActFUI_ImportProjects->setEnabled(true);

		if(!g_AccessRight->TestAR(EXPORT_PROJECTS))
			m_pActFUI_ExportProjects->setEnabled(false);
		else
			m_pActFUI_ExportProjects->setEnabled(true);

		if(!g_AccessRight->TestAR(OPTIONS))
			m_pActFUI_Options->setEnabled(false);
		else
			m_pActFUI_Options->setEnabled(true);

	}
	else
	{
		m_pActLogin->setEnabled(true);
		m_pActLogout->setEnabled(false);
		UpdateLoginData(false);							//logof
	}

	if (g_pClientManager->IsThinClient())
		m_pActFUI_ThickUpdateCheck->setVisible(false);
	else
		m_pActFUI_ThickUpdateCheck->setVisible(true);


	//if export/import enabled:
	if (!m_ImportExportManager)
	{
		m_pActFUI_ExportProjects->setEnabled(false);
		m_pActFUI_ExportUsers->setEnabled(false);
		m_pActFUI_ExportContacts->setEnabled(false);
		m_pActFUI_ProjectsSync->setEnabled(false);
		m_pActFUI_UserSync->setEnabled(false);
		m_pActFUI_DebtorSync->setEnabled(false);
		m_pActFUI_ExportContactContactRel->setEnabled(false);
	}
	
}



void sokrates_spc::on_MenuButton_clicked(int nMenuButtonID)
{

	bool bCTRL=false,bALT=false;
	Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();

	if(Qt::ControlModifier == (Qt::ControlModifier & keys))
		bCTRL = true;

	//remember if the Alt key was pressed
	if(Qt::AltModifier == (Qt::AltModifier & keys))
		bALT = true;


	if (bCTRL && bALT)
	{
		//OPEN SELECTOR (project & contact for now, el stupido did think about person...)
		switch(nMenuButtonID)
		{
		case MENU_BUTTON_CONTACT:
			SelectionPopupFloat::openPopUpSelector(ENTITY_BUS_CONTACT);
			break;
		case MENU_BUTTON_PROJECT:
			SelectionPopupFloat::openPopUpSelector(ENTITY_BUS_PROJECT);
			break;
		}
	}
	else //NORMAL OPS:
	{
		switch(nMenuButtonID)
		{
		case MENU_BUTTON_DESKTOP:
			{
				if (!g_objFuiManager.IsInstanceAlive(MENU_COMM_CENTER) && !bCTRL) //if not alive and not in new win, try by views
					OpenDesktopFUIWithViews();
				else
					g_objFuiManager.OpenFUI(MENU_COMM_CENTER,bCTRL,bALT);
			}

			break;
		case MENU_BUTTON_CONTACT:
			{
				//QTime time;
				//time.start();
				g_objFuiManager.OpenFUI(MENU_CONTACTS,bCTRL,bALT);
				//qDebug()<<"Menu contact FUI"<<time.elapsed();
			}
			break;
		case MENU_BUTTON_PROJECT:
			{
				g_objFuiManager.OpenFUI(MENU_PROJECTS,bCTRL,bALT);

			}
			break;
		case MENU_BUTTON_CALENDAR:
			{
				g_objFuiManager.OpenFUI(MENU_CALENDAR,bCTRL,bALT);

			}
			break;
		case MENU_BUTTON_PROJMANAGER:
			{
				g_objFuiManager.OpenFUI(MENU_SPC_PROJECT_MANAGER,bCTRL,bALT);
			}
			break;
		}

		//only if not new window:
		if (!bALT)
		{
			ChangeMainToolBarFocus(nMenuButtonID);
		}

	}


	
}



//hardcode all actions:
void sokrates_spc::SetupMenuActions()
{

	//------------------------FILE----------------------------
	m_pActLogin= new QAction(tr("Login"),this);
	connect(m_pActLogin, SIGNAL(triggered()), this, SLOT(OnMenu_OnLogin()));

	m_pActLogout= new QAction(tr("Logout"),this);
	connect(m_pActLogout, SIGNAL(triggered()), this, SLOT(OnMenu_OnLogout()));

	m_pActChangePass= new QAction(tr("Password"),this);
	connect(m_pActChangePass, SIGNAL(triggered()), this, SLOT(OnMenu_ChangePass()));

	m_pActAdminTool= new QAction(tr("Administrator Tools"),this);
	connect(m_pActAdminTool, SIGNAL(triggered()), this, SLOT(OnMenu_Backup()));

	m_pActQuit= new QAction(tr("Quit"),this);
	QKeySequence key(tr("Ctrl+Q"));
	m_pActQuit->setShortcut(key);
	connect(m_pActQuit, SIGNAL(triggered()), this, SLOT(close()));
	
	m_pActFUI_Options= new QAction(tr("Options"),this);;
	connect(m_pActFUI_Options, SIGNAL(triggered()), this, SLOT(OnMenu_Options()));

	m_pActFUI_PersonalSettings= new QAction(tr("Personal Settings"),this);;
	connect(m_pActFUI_PersonalSettings, SIGNAL(triggered()), this, SLOT(OnMenu_PersonalSettings()));

	
	//------------------------EDIT----------------------------
	m_pActCut= new QAction(tr("Cut"),this);
	QKeySequence key3(tr("Ctrl+X"));
	m_pActCut->setShortcut(key3);
	connect(m_pActCut, SIGNAL(triggered()), this, SLOT(OnMenu_Cut()));

	m_pActCopy= new QAction(tr("Copy"),this);
	QKeySequence key4(tr("Ctrl+C"));
	m_pActCopy->setShortcut(key4);
	connect(m_pActCopy, SIGNAL(triggered()), this, SLOT(OnMenu_Copy()));

	m_pActPaste= new QAction(tr("Paste"),this);
	//QKeySequence key5(tr("Ctrl+V"));
	//m_pActPaste->setShortcut(key5);
	connect(m_pActPaste, SIGNAL(triggered()), this, SLOT(OnMenu_Paste()));


	//------------------------------VIEW------------------------
	m_pActViewToolBar=new QAction(tr("Toolbar"),this);
	m_pActViewToolBar->setCheckable(true);
	m_pActViewToolBar->setChecked(ui.mainToolBar->isVisible());
	connect(m_pActViewToolBar, SIGNAL(triggered()), this, SLOT(OnMenu_ViewToolbar()));

	m_pActStatusBar=new QAction(tr("Status Bar"),this);
	m_pActStatusBar->setCheckable(true);
	m_pActStatusBar->setChecked(ui.statusBar->isVisible());
	connect(m_pActStatusBar, SIGNAL(triggered()), this, SLOT(OnMenu_ViewStatusbar()));

	m_pActBanner=new QAction(tr("Header"),this);
	m_pActBanner->setCheckable(true);
	m_pActBanner->setChecked(ui.banner->isVisible());
	connect(m_pActBanner, SIGNAL(triggered()), this, SLOT(OnMenu_ViewBanner()));

	//QKeySequence key2(tr("F5"));
	m_pActFullScreen=new QAction(tr("Full Screen"),this);
	//m_pActFullScreen->setShortcut(key2);
	m_pActFullScreen->setCheckable(true);
	m_pActFullScreen->setChecked(false);
	connect(m_pActFullScreen, SIGNAL(triggered()), this, SLOT(OnMenu_ViewFullScreen()));

	m_pActDlgManager=new QAction(tr("View Downloads/Uploads in progress"),this);
	connect(m_pActDlgManager, SIGNAL(triggered()), this, SLOT(OnMenu_ViewDlgManager()));


	//----------------------------MAIN FUNCT------------------------
	m_pActOpenComm=new QAction(tr("Desktop"),this);
	connect(m_pActOpenComm, SIGNAL(triggered()), this, SLOT(OnMenu_OpenComm()));

	m_pActOpenContact=new QAction(tr("Contacts"),this);
	connect(m_pActOpenContact, SIGNAL(triggered()), this, SLOT(OnMenu_OpenContact()));

	m_pActOpenProject=new QAction(tr("Projects"),this);
	connect(m_pActOpenProject, SIGNAL(triggered()), this, SLOT(OnMenu_OpenProject()));


	//-------------------------TELEPHONY------------------------
	m_pActListen_for_Skype_calls=new QAction(tr("Listen For Skype Calls"),this);
	m_pActListen_for_Skype_calls->setCheckable(true);
	connect(m_pActListen_for_Skype_calls, SIGNAL(triggered()), this, SLOT(OnMenu_ListenSkypeCalls()));
	

	m_pActFUI_NewVoiceCall=new QAction(tr("New Phone Call"),this);
	connect(m_pActFUI_NewVoiceCall, SIGNAL(triggered()), this, SLOT(OnMenu_NewVoiceCall()));


	//------------------------ORG----------------------------
	m_pActFUI_Users= new QAction(tr("Users"),this);
	connect(m_pActFUI_Users, SIGNAL(triggered()), this, SLOT(OnMenu_Users()));

	m_pActFUI_TypeDefinitions= new QAction(tr("Type Definitions"),this);
	connect(m_pActFUI_TypeDefinitions, SIGNAL(triggered()), this, SLOT(OnMenu_TypeDefintions()));

	m_pActFUI_CE_Type_Document= new QAction(tr("Document Types"),this);
	connect(m_pActFUI_CE_Type_Document, SIGNAL(triggered()), this, SLOT(OnMenu_CE_Type_Document()));
	
	m_pActFUI_CE_Type_Email= new QAction(tr("Email Types"),this);
	connect(m_pActFUI_CE_Type_Email, SIGNAL(triggered()), this, SLOT(OnMenu_CE_Type_Email()));
	
	m_pActFUI_CE_Type_Voice= new QAction(tr("Phone Call Types"),this);
	connect(m_pActFUI_CE_Type_Voice, SIGNAL(triggered()), this, SLOT(OnMenu_CE_Type_Voice()));

	m_pActFUI_CE_Type_Calendar= new QAction(tr("Calendar Types"),this);
	connect(m_pActFUI_CE_Type_Calendar, SIGNAL(triggered()), this, SLOT(OnMenu_CE_Type_Calendar()));

	m_pActFUI_AddressSchema= new QAction(tr("Address Schemas"),this);
	connect(m_pActFUI_AddressSchema, SIGNAL(triggered()), this, SLOT(OnMenu_AddressSchema()));

	m_pActFUI_RoleDefinitions= new QAction(tr("Role Definitions"),this);
	connect(m_pActFUI_RoleDefinitions, SIGNAL(triggered()), this, SLOT(OnMenu_RoleDefintions()));

	m_pActFUI_PersonRoleDefinitions= new QAction(tr("Role To User Assignment"),this);
	connect(m_pActFUI_PersonRoleDefinitions, SIGNAL(triggered()), this, SLOT(OnMenu_PersonRoleDefintions()));

	m_pActFUI_Resources= new QAction(tr("Resources"),this);
	connect(m_pActFUI_Resources, SIGNAL(triggered()), this, SLOT(OnMenu_Resources()));

	m_pActFUI_CalendarViews= new QAction(tr("Calendar Views"),this);
	connect(m_pActFUI_CalendarViews, SIGNAL(triggered()), this, SLOT(OnMenu_CalendarViews()));

	m_pActFUI_PaymentConditions= new QAction(tr("Payment Conditions"),this);
	connect(m_pActFUI_PaymentConditions, SIGNAL(triggered()), this, SLOT(OnMenu_PaymentConditions()));

	m_pActFUI_CustomFields= new QAction(tr("Custom Fields Definition"),this);
	connect(m_pActFUI_CustomFields, SIGNAL(triggered()), this, SLOT(OnMenu_CustomFields()));


	//m_pActFUI_CalendarEvent= new QAction(tr("Calendar Event"),this);
	//connect(m_pActFUI_CalendarEvent, SIGNAL(triggered()), this, SLOT(OnMenu_CalendarEvent()));

	//m_pActFUI_Calendar = new QAction(tr("Calendar"), this);
	//connect(m_pActFUI_Calendar, SIGNAL(triggered()), this, SLOT(OnMenu_CalendarFui()));

	//------------------------IMPORT----------------------------
	m_pActFUI_ImportContacts= new QAction(tr("Import Contacts"),this);
	connect(m_pActFUI_ImportContacts, SIGNAL(triggered()), this, SLOT(OnMenu_ImportContacts()));

	m_pActFUI_ImportProjects= new QAction(tr("Import Projects"),this);
	connect(m_pActFUI_ImportProjects, SIGNAL(triggered()), this, SLOT(OnMenu_ImportProjects()));

	m_pActFUI_ImportUsers= new QAction(tr("Import Users"),this);
	connect(m_pActFUI_ImportUsers, SIGNAL(triggered()), this, SLOT(OnMenu_ImportUsers()));

	m_pActFUI_ImportEmails= new QAction(tr("Import Emails"),this);
	connect(m_pActFUI_ImportEmails, SIGNAL(triggered()), this, SLOT(OnMenu_ImportEmails()));

	m_pActFUI_ImportAppointments= new QAction(tr("Import Calendar Events"),this);
	connect(m_pActFUI_ImportAppointments, SIGNAL(triggered()), this, SLOT(OnMenu_ImportAppointments()));

	m_pActFUI_ImportUserContactRel= new QAction(tr("Import User-Contact Relationships"),this);
	connect(m_pActFUI_ImportUserContactRel, SIGNAL(triggered()), this, SLOT(OnMenu_ImportUserContactRel()));

	m_pActFUI_ImportContactContactRel= new QAction(tr("Import Contact-Contact Relationships"),this);
	connect(m_pActFUI_ImportContactContactRel, SIGNAL(triggered()), this, SLOT(OnMenu_ImportContactContactRel()));

	m_pActFUI_ImportGroupAssignments= new QAction(tr("Import Group Assignments"),this);
	connect(m_pActFUI_ImportGroupAssignments, SIGNAL(triggered()), this, SLOT(OnMenu_ImportGroupAssignments()));

	m_pActFUI_ImportStoredProjectLists= new QAction(tr("Import Stored Project Lists"),this);
	connect(m_pActFUI_ImportStoredProjectLists, SIGNAL(triggered()), this, SLOT(OnMenu_ImportStoredProjectLists()));
	
	m_pActFUI_ImportUserDefinedFields= new QAction(tr("Import Contact User-Defined Fields"),this);
	connect(m_pActFUI_ImportUserDefinedFields, SIGNAL(triggered()), this, SLOT(OnMenu_ImportUserDefinedFields()));

	m_pActFUI_ExportProjects= new QAction(tr("Export Projects "),this);
	connect(m_pActFUI_ExportProjects, SIGNAL(triggered()), this, SLOT(OnMenu_ExportProjects()));

	m_pActFUI_ExportUsers= new QAction(tr("Export Users "),this);
	connect(m_pActFUI_ExportUsers, SIGNAL(triggered()), this, SLOT(OnMenu_ExportUsers()));
	
	m_pActFUI_ExportContacts= new QAction(tr("Export Contacts"),this);
	connect(m_pActFUI_ExportContacts, SIGNAL(triggered()), this, SLOT(OnMenu_ExportContacts()));

	m_pActFUI_ExportContactContactRel= new QAction(tr("Export Contact-Contact Relationships"),this);
	connect(m_pActFUI_ExportContactContactRel, SIGNAL(triggered()), this, SLOT(OnMenu_ExportContactContactRel()));

	m_pActFUI_ProjectsSync= new QAction(tr("Projects: Synchronize Now!"),this);
	connect(m_pActFUI_ProjectsSync, SIGNAL(triggered()), this, SLOT(OnMenu_ProjectsSync()));

	m_pActFUI_UserSync= new QAction(tr("Users: Synchronize Now! "),this);
	connect(m_pActFUI_UserSync, SIGNAL(triggered()), this, SLOT(OnMenu_UserSync()));

	m_pActFUI_DebtorSync= new QAction(tr("Debtors: Synchronize Now! "),this);
	connect(m_pActFUI_DebtorSync, SIGNAL(triggered()), this, SLOT(OnMenu_DebtorSync()));

	m_pActFUI_ImportCenter= new QAction(tr("Import Center"),this);
	connect(m_pActFUI_ImportCenter, SIGNAL(triggered()), this, SLOT(OnMenu_ImportCenter()));


	//------------------------PRINT----------------------------
	m_pActFUI_Reporting= new QAction(tr("Reporting"),this);
	connect(m_pActFUI_Reporting, SIGNAL(triggered()), this, SLOT(OnMenu_Reporting()));

	m_pActFUI_ActualReports= new QAction(tr("Actual Reports"),this);
	connect(m_pActFUI_ActualReports, SIGNAL(triggered()), this, SLOT(OnMenu_ActualReports()));

	//m_pActFUI_SetUpPrinter= new QAction(tr("Set Up Printer"),this);
	//connect(m_pActFUI_SetUpPrinter, SIGNAL(triggered()), this, SLOT(OnMenu_SetupPrinter()));


	// Support
	m_pActFUI_Support= new QAction(tr("Support Desktop Sharing"),this);
	connect(m_pActFUI_Support, SIGNAL(triggered()), this, SLOT(OnMenu_Support()));

	//------------------------HELP----------------------------
	m_pActFUI_OnlineHelp= new QAction(tr("Online Help"),this);
	connect(m_pActFUI_OnlineHelp, SIGNAL(triggered()), this, SLOT(OnMenu_OnlineHelp()));
	m_pActFUI_OnlineHelp->setShortcut(QKeySequence("F1"));

	m_pActFUI_HelpFirstStep= new QAction(tr("First Steps"),this);
	connect(m_pActFUI_HelpFirstStep, SIGNAL(triggered()), this, SLOT(OnMenu_HelpFirstStep()));


	m_pActFUI_About= new QAction(tr("About"),this);
	connect(m_pActFUI_About, SIGNAL(triggered()), this, SLOT(OnMenu_About()));
	
	m_pActFUI_ThickUpdateCheck= new QAction(tr("Check For Available Updates"),this);
	connect(m_pActFUI_ThickUpdateCheck, SIGNAL(triggered()), this, SLOT(OnMenu_ThickCheckForUpdates()));


	//--------------------------------------------------------
	//						Menu Setup
	////--------------------------------------------------------

	//Create Menus:
	QMenuBar *menuBar=this->menuBar();

	QMenu *menuFile= new QMenu(tr("File"));
	menuFile->addAction(m_pActLogin);
	menuFile->addAction(m_pActLogout);
	menuFile->addAction(m_pActChangePass);
	menuFile->addAction(m_pActAdminTool);
	menuFile->addSeparator();
	menuFile->addAction(m_pActFUI_Options);
	menuFile->addAction(m_pActFUI_PersonalSettings);
	menuFile->addSeparator();
	menuFile->addAction(m_pActQuit);
	m_pMenuFile=menuBar->addMenu(menuFile);


	QMenu *menuEdit= new QMenu(tr("Edit"));
	menuEdit->addAction(m_pActCut);
	menuEdit->addAction(m_pActCopy);
	menuEdit->addAction(m_pActPaste);
	m_pMenuEdit=menuBar->addMenu(menuEdit);

	QMenu *menuView= new QMenu(tr("View"));
	//menuView->addAction(m_pActViewToolBar);
	menuView->addAction(m_pActStatusBar);
	menuView->addAction(m_pActFullScreen);
	menuView->addAction(m_pActBanner);
	menuView->addSeparator();
	menuView->addAction(m_pView_Pro);
	menuView->addAction(m_pView_SideBar);
	menuView->addSeparator();
	menuView->addAction(m_pActDlgManager);
	m_pMenuView=menuBar->addMenu(menuView);



	QMenu *menuMainFunctions= new QMenu(tr("Main"));
	menuMainFunctions->addAction(m_pActOpenComm);
	menuMainFunctions->addAction(m_pActOpenContact);
	menuMainFunctions->addAction(m_pActOpenProject);
	m_pMenuMainFunctions=menuBar->addMenu(menuMainFunctions);

	QMenu *menuTel= new QMenu(tr("Telephony"));
	menuTel->addAction(m_pActListen_for_Skype_calls);
	menuTel->addAction(m_pActFUI_NewVoiceCall);
	m_pMenuTel=menuBar->addMenu(menuTel);



	QMenu *menuOrg= new QMenu(tr("Organization"));
	menuOrg->addAction(m_pActFUI_Users);
	menuOrg->addSeparator();
	menuOrg->addAction(m_pActFUI_TypeDefinitions);
	menuOrg->addAction(m_pActFUI_CE_Type_Document);
	menuOrg->addAction(m_pActFUI_CE_Type_Email);
	menuOrg->addAction(m_pActFUI_CE_Type_Voice);
	menuOrg->addAction(m_pActFUI_CE_Type_Calendar);
	menuOrg->addSeparator();
	menuOrg->addAction(m_pActFUI_AddressSchema);
	
//#ifdef _DEBUG //If in debug show roles and roles for person - for checking purpose.
	menuOrg->addAction(m_pActFUI_RoleDefinitions);
	menuOrg->addAction(m_pActFUI_PersonRoleDefinitions);
//#endif
//#ifdef _DEBUG
	menuOrg->addAction(m_pActFUI_Resources);
	menuOrg->addAction(m_pActFUI_CalendarViews);
	menuOrg->addAction(m_pActFUI_PaymentConditions);
	menuOrg->addAction(m_pActFUI_CustomFields);
	
//#endif

//#ifdef _DEBUG
	//menuOrg->addAction(m_pActFUI_CalendarEvent);
	//menuOrg->addAction(m_pActFUI_Calendar);
//#endif
	m_pMenuOrg=menuBar->addMenu(menuOrg);


	QMenu *menuData1= new QMenu(tr("Data Exchange"));
	
	menuData1->addAction(m_pActFUI_ProjectsSync);
	menuData1->addAction(m_pActFUI_UserSync);
	menuData1->addAction(m_pActFUI_DebtorSync);
	menuData1->addSeparator();
	menuData1->addAction(m_pActFUI_ImportContacts);
	menuData1->addAction(m_pActFUI_ImportProjects);
	menuData1->addAction(m_pActFUI_ImportUsers);
	menuData1->addAction(m_pActFUI_ImportEmails);

#ifdef _DEBUG
	menuData1->addAction(m_pActFUI_ImportAppointments);
#endif

	menuData1->addAction(m_pActFUI_ImportUserContactRel);
	menuData1->addAction(m_pActFUI_ImportContactContactRel);
	menuData1->addAction(m_pActFUI_ImportGroupAssignments);
	menuData1->addAction(m_pActFUI_ImportStoredProjectLists);
	menuData1->addAction(m_pActFUI_ImportUserDefinedFields);
	menuData1->addSeparator();
	menuData1->addAction(m_pActFUI_ExportProjects);
	menuData1->addAction(m_pActFUI_ExportUsers);
	menuData1->addAction(m_pActFUI_ExportContacts);
	menuData1->addAction(m_pActFUI_ExportContactContactRel);
	menuData1->addSeparator();
	menuData1->addAction(m_pActFUI_ImportCenter);
	m_pMenuDataImport=menuBar->addMenu(menuData1);

	QMenu *menuPrint2= new QMenu(tr("Print"));
	menuPrint2->addAction(m_pActFUI_Reporting);
	menuPrint2->addAction(m_pActFUI_ActualReports);
	//menuPrint2->addAction(m_pActFUI_SetUpPrinter);
	m_pMenuPrint=menuBar->addMenu(menuPrint2);

	m_menuWindows= new QMenu(tr("Windows"));
	m_pMenuWindows=menuBar->addMenu(m_menuWindows);

	//
	// "Portable" submenu
	//
	if(DataHelper::IsPortableVersion())
	{
		m_pMenuPortableSkypeInstall = new QAction(tr("Install Portable Skype(R)"),this);
		connect(m_pMenuPortableSkypeInstall, SIGNAL(triggered()), this, SLOT(OnMenu_PortableSkypeInstall()));

		m_pMenuPortableSkypeUpdate = new QAction(tr("Update Portable Skype(R)"),this);
		connect(m_pMenuPortableSkypeUpdate, SIGNAL(triggered()), this, SLOT(OnMenu_PortableSkypeUpdate()));
		if(!SkypeManager::IsSkypeInstalled())
			m_pMenuPortableSkypeUpdate->setEnabled(false);

		m_pMenuPortableAppRegister = new QAction(tr("Register Applications on Portable Memory Device"),this);
		connect(m_pMenuPortableAppRegister, SIGNAL(triggered()), this, SLOT(OnMenu_PortableAppRegister()));

		QMenu *menuPortable= new QMenu(tr("Portable"));
		menuPortable->addAction(m_pMenuPortableSkypeInstall);
		menuPortable->addAction(m_pMenuPortableSkypeUpdate);
		menuPortable->addAction(m_pMenuPortableAppRegister);
		menuBar->addMenu(menuPortable);
	}

	QMenu *menuSupport= new QMenu(tr("Support"));
	menuSupport->addAction(m_pActFUI_Support);
	m_pMenuSupport = menuBar->addMenu(menuSupport);
	
	QMenu *menuHelp= new QMenu(tr("Help"));
	menuHelp->addAction(m_pActFUI_ThickUpdateCheck);
	menuHelp->addAction(m_pActFUI_OnlineHelp);
	menuHelp->addAction(m_pActFUI_HelpFirstStep);
	menuHelp->addAction(m_pActFUI_About);
	m_pMenuHelp=menuBar->addMenu(menuHelp);


	connect(menuTel, SIGNAL(aboutToShow()), this, SLOT(OnListenSkypeCalls_UpdateUI()) ); //update state of skype chkbox when is about to show

}


//------------------------------------------------------------------------
//			ACTION SLOTS:
//------------------------------------------------------------------------

//----------------------------FILE---------------------------

void sokrates_spc::OnMenu_OnLogin()
{
	if(!DestroyLayout()) //will be recreated on login success
		return;

	g_pClientManager->Logout(); //try logout
	UpdateMenuState();
	g_pClientManager->Login();
}

void sokrates_spc::OnMenu_OnLogout()
{
	if(!DestroyLayout()) //will be recreated on login success
		return;
	
	//QApplication::processEvents(QEventLoop::DeferredDeletion); //kill all windows while BO objects are alive

	g_pClientManager->Logout(); //try logout
	UpdateMenuState();

	//QApplication::processEvents(QEventLoop::DeferredDeletion); //kill all windows while BO objects are alive
	
	if (!g_pClientManager->IsLogged())
		QTimer::singleShot(100,g_pClientManager,SLOT(Login()));

}


void sokrates_spc::OnMenu_ChangePass()
{
	Dlg_PasswordChange Dlg;
	if(Dlg.exec())
	{
		//logout:
		OnMenu_OnLogout();
	}

}

void sokrates_spc::OnMenu_Backup()
{
	AdminTool Dlg;
	if(Dlg.Initialize())
		//Dlg.open();
		Dlg.exec();
}

void sokrates_spc::OnMenu_Options()
{
	g_objFuiManager.OpenFUI(MENU_OPTIONS,true);
}

void sokrates_spc::OnMenu_PersonalSettings()
{
	g_objFuiManager.OpenFUI(MENU_PERSONAL_SETTINGS,true,false,FuiBase::MODE_EMPTY,g_pClientManager->GetPersonID());
}

//----------------------------EDIT---------------------------
void sokrates_spc::OnMenu_Cut()
{
	//what should I do?
}

void sokrates_spc::OnMenu_Copy()
{
	//what should I do?
}

void sokrates_spc::OnMenu_Paste()
{
	//what should I do?
}
//----------------------------VIEW---------------------------

void sokrates_spc::OnMenu_ViewToolbar()
{
	ui.mainToolBar->setVisible(!ui.mainToolBar->isVisible());
	m_pActViewToolBar->setChecked(ui.mainToolBar->isVisible());
}

void sokrates_spc::OnMenu_ViewStatusbar()
{
	ui.statusBar->setVisible(!ui.statusBar->isVisible());
	m_pActStatusBar->setChecked(ui.statusBar->isVisible());
	g_pSettings->SetPersonSetting(SHOW_STATUS_BAR,m_pActStatusBar->isChecked());
}

void sokrates_spc::OnMenu_ViewBanner()
{
	ui.banner->setVisible(!ui.banner->isVisible());
	if(ui.banner->isVisible())
		SetupBanner(); //MR: fix when "View header" causes the clock part to stay at the left side
	m_pActBanner->setChecked(ui.banner->isVisible());
	g_pSettings->SetPersonSetting(SHOW_HEADER,m_pActBanner->isChecked());
}

void sokrates_spc::OnMenu_ViewFullScreen()
{
	if(!g_pClientManager->IsLogged()) return; //disable if not logged

	if(!m_pActFullScreen->isChecked())
	{
		//ui.MainMenu->setVisible((g_nApplicationVersion==SOKRATES_COMMUNICATOR_VERSION_TEAM));
		ui.frameToolBar->setVisible(true); //(g_nApplicationVersion==SOKRATES_COMMUNICATOR_VERSION_LIGHT));

		//restore to previous state:
		ui.mainToolBar->setVisible(m_pActViewToolBar->isChecked());
		ui.statusBar->setVisible(m_pActStatusBar->isChecked());
		ui.banner->setVisible(m_pActBanner->isChecked());
	}
	else
	{
		ui.frameToolBar->setVisible(false);
		ui.statusBar->setVisible(false);
		m_pActStatusBar->setChecked(false);
		ui.banner->setVisible(false);
		m_pActBanner->setChecked(false);
	}
	g_pSettings->SetPersonSetting(SHOW_FULL_SCREEN,m_pActFullScreen->isChecked());
}

void sokrates_spc::OnMenu_ViewDlgManager()
{
	g_DownloadManager->OnShowDlg();
}

//-------------------------TELEPHONY------------------------

void sokrates_spc::OnMenu_ListenSkypeCalls()
{
	//toggle option
	SkypeManager::SetListenIncomingCalls(!SkypeManager::GetListenIncomingCalls());
	m_pActListen_for_Skype_calls->setChecked(SkypeManager::GetListenIncomingCalls());
	SkypeManager::EnsureInitialised(); //startup Skype if required
}



void sokrates_spc::OnMenu_NewVoiceCall()
{
	g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER,true);
}


//-------------------------ORG------------------------

void sokrates_spc::OnMenu_Users()
{
	g_objFuiManager.OpenFUI(MENU_PERSON,true);
}

void sokrates_spc::OnMenu_TypeDefintions()
{
	g_objFuiManager.OpenFUI(MENU_CONTACT_TYPES,false);
}

void sokrates_spc::OnMenu_RoleDefintions()
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_USERS))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}
	g_objFuiManager.OpenFUI(MENU_ROLE_DEFINITION,false);
}

void sokrates_spc::OnMenu_PersonRoleDefintions()
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_USERS))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}
	g_objFuiManager.OpenFUI(MENU_PERSON_ACCESS_RIGHTS,false);
}

void sokrates_spc::OnMenu_Resources()
{
	g_objFuiManager.OpenFUI(MENU_RESOURCES,false);
}

void sokrates_spc::OnMenu_PaymentConditions()
{
	g_objFuiManager.OpenFUI(MENU_PAYMENT_CONDITIONS,false);
}

void sokrates_spc::OnMenu_CustomFields()
{
	g_objFuiManager.OpenFUI(MENU_CUSTOM_FIELDS,false);
}



//------------------------IMPORT----------------------------

void sokrates_spc::OnMenu_ImportContacts()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT,false);
}

void sokrates_spc::OnMenu_ImportProjects()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_PROJECT,false);
}

void sokrates_spc::OnMenu_ImportUsers()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_PERSON,false);
}

void sokrates_spc::OnMenu_ImportEmails()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_EMAIL,false);
}

void sokrates_spc::OnMenu_ImportUserContactRel()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_USER_CONTACT_RELATIONSHIP,false);
}

void sokrates_spc::OnMenu_ImportContactContactRel()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT_CONTACT_RELATIONSHIP,false);
}

void sokrates_spc::OnMenu_ImportAppointments()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_APPOINTMENT,false);
}

void sokrates_spc::OnMenu_OpenComm()
{
	g_objFuiManager.OpenFUI(MENU_COMM_CENTER,false);
}
void sokrates_spc::OnMenu_OpenContact()
{
	g_objFuiManager.OpenFUI(MENU_CONTACTS,false);
}
void sokrates_spc::OnMenu_OpenProject()
{
	g_objFuiManager.OpenFUI(MENU_PROJECTS,false);
}




//------------------------PRINT----------------------------

void sokrates_spc::OnMenu_Reporting()
{
	//g_objFuiManager.OpenFUI(MENU_REPORTS_PREVIEW, false, true);
	ReportManager Rpt;
	Rpt.PrintReportAll(); //BT: new report wizard
}


void sokrates_spc::OnMenu_ActualReports()
{
	//??
}


//void sokrates_spc::OnMenu_SetupPrinter()
//{
	///??
	
//}


//------------------------HELP----------------------------

void sokrates_spc::OnMenu_OnlineHelp()
{
	QString strHelp;
	if(g_pClientManager->GetIniFile()->m_strLangCode == "de"){
		strHelp = "http://www.sokrates-communicator.com/de/help/";
	}
	else{
		strHelp = "http://www.sokrates-communicator.com/en/help/";
	}

	QDesktopServices::openUrl(strHelp); //BT improved;
/*
	HINSTANCE hRes = ShellExecute(GetActiveWindow(), L"open", (LPCWSTR)strHelp.utf16(), NULL, NULL, SW_SHOWNORMAL);
	if((int)hRes <= 32)
		QMessageBox::information(NULL, "", tr("Failed to open URL %1 (error: %2)!").arg(strHelp).arg((int)hRes));
*/

}



void sokrates_spc::OnMenu_HelpFirstStep()
{

	QString strHelp;
	if(g_pClientManager->GetIniFile()->m_strLangCode == "de")
	{
		strHelp = "http://www.sokrates-communicator.com/de/firststeps/index.html";
	}
	else
	{
		strHelp = "http://www.sokrates-communicator.com/en/firststeps/index.html";
	}

	QDesktopServices::openUrl(strHelp); //BT improved;

}


void sokrates_spc::OnMenu_About()
{
   int nSN;
   QString strUser,strRep1,strRep2;
   QString strCustomerID;
   int nCustomerSolutionID;
   g_FunctionPoint.GetLicenseInfo(strUser, nSN, strRep1, strRep2,strCustomerID,nCustomerSolutionID);

   AboutWidget dlg(this,g_pClientManager->GetApplicationDisplayName(),g_pClientManager->GetModuleCode(),strUser,nSN,strRep1,strRep2);
   dlg.exec();
}

//go go
void sokrates_spc::OnCommunicationInProgress(int nTicks,qint64 done, qint64 total)
{
	if (!this->isVisible())
		return;

	if (nTicks>0)
	{
		if (!m_ProgressBar->isVisible())m_ProgressBar->setVisible(true);
		nTicks=nTicks/4;
		int nOffset=nTicks/10;
		int nTicksProgress=nTicks-nOffset*10;
		m_ProgressBar->setValue(nTicksProgress);
		if (total>0 && done < total)
		{
			double speed=done/double(nTicks);
			QString strSpeed=DataHelper::GetFormatedFileSize((double)speed,2);
			ui.statusBar->showMessage(QString("Data Transfer In Progress: bytes transfered: %1/%2 at speed: %3/s, time elapsed: %4s").arg(DataHelper::GetFormatedFileSize((double)done,2)).arg(DataHelper::GetFormatedFileSize((double)total,2)).arg(strSpeed).arg(nTicks));
		}
		else
			ui.statusBar->showMessage(QString("Data Transfer In Progress: time elapsed: %1s").arg(nTicks));

	}
	else
	{
		if (!m_ProgressBar->isVisible() )return;
		m_ProgressBar->reset();
		ui.statusBar->clearMessage();
		m_ProgressBar->setVisible(false);
		ui.statusBar->repaint();
	}
}

//close all FUI's: no network traffic is allowed:
void sokrates_spc::OnCommunicationFatalError(int nErrorCode)
{
	//adjust GUI to start screen:
	ui.frameToolBar->setVisible(false);
	SetupMenu(false);
	UpdateMenuState();
	m_bLoginExecuted=false;

}


void sokrates_spc::OnProcessBeforeLogout()
{
	//MR FIX? MB complains that header misteriously dissapears #2031
	//g_pSettings->SetPersonSetting(SHOW_HEADER,m_pActBanner->isChecked());
};


void sokrates_spc::OnFuiTabWantsFullScreen()
{
	m_pActFullScreen->setChecked(!m_pActFullScreen->isChecked());
	OnMenu_ViewFullScreen();
}


void sokrates_spc::OnFuiChanged()
{
	int nMainFUIID=-1;
	int nFP=g_objFuiManager.GetCurrentFP();

	switch(nFP)
	{
	case MENU_COMM_CENTER:
		nMainFUIID=0;
		break;
	case MENU_CONTACTS:
		nMainFUIID=1;
		break;
	case MENU_PROJECTS:
		nMainFUIID=2;
	    break;
	}

	ChangeMainToolBarFocus(nMainFUIID);
}



//close all top windows
void sokrates_spc::CloseAllModalWindows()
{
	QWidget *pWidget=QApplication::activeModalWidget();
	while (pWidget)
	{
		pWidget->close();
		QCoreApplication::instance()->processEvents();
		pWidget=QApplication::activeModalWidget();
	}

	//QTimer::singleShot(0,this,SLOT(CloseAllModalWindows()));
}

void sokrates_spc::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_DISPLAY_STATUS_TEXT)
	{
		if (nMsgDetail==0)
			nMsgDetail=15000;
		ui.statusBar->showMessage(val.toString(),nMsgDetail);
		return;
	}

	if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_MENU_HEADER)
	{
		ui.banner->setVisible(nMsgDetail);
		m_pActBanner->setChecked(nMsgDetail);
		return;
	}

	if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_MENU_STATUSBAR)
	{
		ui.statusBar->setVisible(nMsgDetail);
		m_pActStatusBar->setChecked(nMsgDetail);
		return;
	}

	if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_MENU_FULLSCREEN)
	{
		m_pActFullScreen->setChecked(nMsgDetail);
		OnMenu_ViewFullScreen();
		return;
	}

}


void sokrates_spc::ChangeMainToolBarFocus(int nMenuButtonID)
{
	if (nMenuButtonID==m_nToolBarButtonIDCurrentFocus)
		return;

	m_nToolBarButtonIDCurrentFocus=nMenuButtonID;

	QString	styleSectionButtonLvUp="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_Inactive.png) 4px 4px 4px 4px stretch stretch }\
								   QPushButton:hover {  border-width: 4px; color: white;border-image:url(:Button_Over.png) 4px 4px 4px 4px stretch stretch} \
								   QPushButton:pressed {  border-width: 4px; color: white;border-image:url(:Button_Pressed.png) 4px 4px 4px 4px stretch stretch} ";

	QString	styleSectionButtonLvDown="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_Active.png) 4px 4px 4px 4px stretch stretch }";

	//if(m_pBtnProject->isEnabled())m_pBtnProject->setStyleSheet(styleSectionButtonLvUp);
	//if(m_pBtnContact->isEnabled())m_pBtnContact->setStyleSheet(styleSectionButtonLvUp);
	//if(m_pBtnComm->isEnabled())m_pBtnComm->setStyleSheet(styleSectionButtonLvUp);


	if(m_pBtnProject->isEnabled())m_pBtnProject->setIcon(":Projects_G.png");
	if(m_pBtnContact->isEnabled())m_pBtnContact->setIcon(":Contacts_G.png");
	if(m_pBtnComm->isEnabled())m_pBtnComm->setIcon(":Desktop_G.png");
	if(m_pBtnCalendar->isEnabled())m_pBtnCalendar->setIcon(":Calendar_G.png");


	switch(nMenuButtonID)
	{
	case 0:
		{
			//m_pBtnComm->setStyleSheet(styleSectionButtonLvDown);
			m_pBtnComm->setIcon(":Desktop_C.png");
		}

		break;
	case 1:
		{
			//m_pBtnContact->setStyleSheet(styleSectionButtonLvDown);
			m_pBtnContact->setIcon(":Contacts_C.png");
			
		}
		break;
	case 2:
		{
			//m_pBtnProject->setStyleSheet(styleSectionButtonLvDown);
			m_pBtnProject->setIcon(":Projects_C.png");
		}
	case 3:
		{
			//m_pBtnProject->setStyleSheet(styleSectionButtonLvDown);
			m_pBtnCalendar->setIcon(":Calendar_C.png");
		}
		break;
	}
}

#ifdef _WIN32
long CALLBACK OnNewMessage(LPVOID lpvContext,ULONG cNotification,LPNOTIFICATION	pNotif)
{
	if(fnevCriticalError == cNotification){
		qDebug() << "MAPI NOTIFICATION: Critical error\n";
		QByteArray entryID=QByteArray::fromRawData((const char *)pNotif->info.err.lpEntryID->ab, pNotif->info.err.cbEntryID);
		//CommunicationManager::ProcessEmailContentFromOutlook(entryID);
	}
	else if(fnevExtended == cNotification)
		qDebug() << "MAPI NOTIFICATION: Extended event\n";
	else if(fnevNewMail == cNotification)
		qDebug() << "MAPI NOTIFICATION: New mail received\n";
	else if(fnevObjectCreated == cNotification)
		qDebug() << "MAPI NOTIFICATION: Object created\n";
	else if(fnevObjectCopied == cNotification)
		qDebug() << "MAPI NOTIFICATION: Object copied\n";
	else if(fnevObjectDeleted == cNotification)
		qDebug() << "MAPI NOTIFICATION: Object deleted\n";
	else if(fnevObjectModified == cNotification)
		qDebug() << "MAPI NOTIFICATION: Object modified\n";
	else if(fnevObjectMoved == cNotification)
		qDebug() << "MAPI NOTIFICATION: Object moved\n";
	else if(fnevSearchComplete == cNotification)
		qDebug() << "MAPI NOTIFICATION: Search completed\n";

	return 0;
}
#endif

//
void sokrates_spc::ApplyPersonSettings()
{
	//show banner
	m_pActBanner->setChecked(g_pSettings->GetPersonSetting(SHOW_HEADER).toBool());
	ui.banner->setVisible(m_pActBanner->isChecked());

	//show status
	m_pActStatusBar->setChecked(g_pSettings->GetPersonSetting(SHOW_STATUS_BAR).toBool());
	ui.statusBar->setVisible(m_pActStatusBar->isChecked());

	//show full screen
	m_pActFullScreen->setChecked(g_pSettings->GetPersonSetting(SHOW_FULL_SCREEN).toBool());
	OnMenu_ViewFullScreen();

	
}

void sokrates_spc::OnMenu_Support()
{
	//verify if Netviewer exists
	QString strExe = QApplication::applicationDirPath();
	strExe += "\\support\\NV_support.exe";
	
	if(!QFile::exists(strExe)){
		QMessageBox::information(NULL, tr("Error"), tr("NetViewer client has not been found!"));
		return;
	}

	NetviewerDlg dlg;
	int nRes = dlg.exec();
	if(nRes)
	{
		//start NetViewer client
		//int nResult=QMessageBox::question(NULL, tr("Question"), tr("Do you want to share your monitor view with a support specialist? Ask him for a session number!"),tr("Yes"),tr("No"));
		//if (nResult==0)
		{
			//append param
			QString strParam = "-sinr:";
			strParam += dlg.GetSessionNum();

			QStringList lstArgs;
			lstArgs << strParam;
			QProcess::startDetached(strExe, lstArgs);
		}
	}
}


void sokrates_spc::OpenDesktopFUIWithViews()
{
	Status err;
	DbRecordSet lstData;
	_SERVER_CALL(BusCommunication->GetDefaultDesktopFilterViews(err,g_pClientManager->GetPersonID(),lstData))
	_CHK_ERR(err);

	if (lstData.getRowCount()>0)
	{
		QSize size(300,100);
		QProgressDialog progress(tr("Preparing Desktop..."),tr("Cancel"),0,lstData.getRowCount(),this);
		progress.setWindowTitle(tr("Read In Progress"));
		progress.setMinimumSize(size);
		progress.setWindowModality(Qt::NonModal);
		progress.setMinimumDuration(1200);//2.4s before pops up

		int nFirstFUI;
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			progress.setValue(i);
			qApp->processEvents();
			if (progress.wasCanceled())
			{
				progress.close();
				break;
			}
			int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_CENTER, false, true);//,FuiBase::MODE_EMPTY, -1,true);

			//Remember first fui for setting it active on startup.
			if (i == 0)
				nFirstFUI = nNewFUI;

			FUI_CommunicationCenter* pFui=dynamic_cast<FUI_CommunicationCenter*>(g_objFuiManager.GetFUIWidget(nNewFUI));
			if (pFui)
			{
				DbRecordSet row = lstData.getRow(i);
				pFui->DesktopStartupInitialization(row);
			}

		}

		g_objFuiManager.ActivateFUI(nFirstFUI);
	}
	else
	{
		g_objFuiManager.OpenFUI(MENU_COMM_CENTER, false, false);
	}
}

void sokrates_spc::Initialize()
{
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
		return;
	//showMaximized();
	//m_bFirstShow=false;
	showMaximized();
}


void sokrates_spc::ApplyFunctionPoints()
{
	//test FP access rights
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_PROJECT_FUI, nValue))
	{
		QString	styleSectionButtonLvUp="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_Inactive.png) 4px 4px 4px 4px stretch stretch }";
		m_pBtnProject->setStyleSheet(styleSectionButtonLvUp);
		m_pBtnProject->setEnabled(false);
		m_pActOpenProject->setEnabled(false);
	}
	else
	{
		//QString	styleSectionButtonLvUp="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_Inactive.png) 4px 4px 4px 4px stretch stretch }";
		//m_pBtnProject->setStyleSheet(styleSectionButtonLvUp);
		m_pBtnProject->setEnabled(true);
		m_pActOpenProject->setEnabled(true);
	}

	if(!g_FunctionPoint.IsFPAvailable(FP_CONTACTS_FUI, nValue))
	{
		QString	styleSectionButtonLvUp="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_Inactive.png) 4px 4px 4px 4px stretch stretch }";
		m_pBtnContact->setStyleSheet(styleSectionButtonLvUp);
		m_pBtnContact->setEnabled(false);
		m_pActOpenContact->setEnabled(false);
	}
	else
	{
		//QString	styleSectionButtonLvUp="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_Inactive.png) 4px 4px 4px 4px stretch stretch }";
		//m_pBtnContact->setStyleSheet(styleSectionButtonLvUp);
		m_pBtnContact->setEnabled(true);
		m_pActOpenContact->setEnabled(true);
	}

	if(!g_FunctionPoint.IsFPAvailable(FP_TELEPHONY_SKYPE_LISTEN_AND_ANSWER, nValue)){
		m_pActFUI_NewVoiceCall->setEnabled(false);
		m_pActListen_for_Skype_calls->setEnabled(false);
	}
	else{
		m_pActFUI_NewVoiceCall->setEnabled(true);
		m_pActListen_for_Skype_calls->setEnabled(true);

		//start Skype listening
		if(g_pSettings->GetPersonSetting(COMM_SETTINGS_LISTEN_SKYPE).toBool())
			OnMenu_ListenSkypeCalls();
	}

	if (!g_FunctionPoint.IsFPAvailable(FP_USER_FUI))
		m_pActFUI_Users->setEnabled(false);
	else
		m_pActFUI_Users->setEnabled(true);

	//import:
	if (!g_FunctionPoint.IsFPAvailable(FP_IMPORT_CONTACTS_MANUALLY))
		m_pActFUI_ImportContacts->setEnabled(false);
	else
		m_pActFUI_ImportContacts->setEnabled(true);
	if (!g_FunctionPoint.IsFPAvailable(FP_IMPORT_USERS_MANUALLY))
		m_pActFUI_ImportUsers->setEnabled(false);
	else
		m_pActFUI_ImportUsers->setEnabled(true);
	if (!g_FunctionPoint.IsFPAvailable(FP_IMPORT_PROJECTS_MANUALLY))
		m_pActFUI_ImportProjects->setEnabled(false);
	else
		m_pActFUI_ImportProjects->setEnabled(true);
	if (!g_FunctionPoint.IsFPAvailable(FP_IMPORT_EMAILS_MANUALLY))
		m_pActFUI_ImportEmails->setEnabled(false);
	if (!g_FunctionPoint.IsFPAvailable(FP_EXPORT_PROJECTS_MANUALLY))
		m_pActFUI_ExportProjects->setEnabled(false);
	if (!g_FunctionPoint.IsFPAvailable(FP_EXPORT_USERS_MANUALLY))
		m_pActFUI_ExportUsers->setEnabled(false);
	else
		m_pActFUI_ImportEmails->setEnabled(true);
}


void sokrates_spc::OnMenu_CE_Type_Document()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_CE_TYPES, true);
	FUI_CE_Types* pFui=dynamic_cast<FUI_CE_Types*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->SetDefaultType(GlobalConstants::CE_TYPE_DOCUMENT);
	}
}

void sokrates_spc::OnMenu_CE_Type_Email()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_CE_TYPES, true);
	FUI_CE_Types* pFui=dynamic_cast<FUI_CE_Types*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->SetDefaultType(GlobalConstants::CE_TYPE_EMAIL);
	}

}
void sokrates_spc::OnMenu_CE_Type_Voice()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_CE_TYPES, true);
	FUI_CE_Types* pFui=dynamic_cast<FUI_CE_Types*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->SetDefaultType(GlobalConstants::CE_TYPE_VOICE_CALL);
	}
}
void sokrates_spc::OnMenu_CE_Type_Calendar()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_CE_TYPES, true);
	FUI_CE_Types* pFui=dynamic_cast<FUI_CE_Types*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->SetDefaultType(GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);
	}
}
void sokrates_spc::OnMenu_AddressSchema()
{
	Dlg_ContactAddressSchemas dlg;
	dlg.Initialize("");
	dlg.exec();
}




void sokrates_spc::OnMenu_ExportProjects()
{
	if(!m_ImportExportManager) return;

	QList<DbRecordSet> RecordSetList;
	WizardRegistration wizReg(WizardRegistration::PROJECT_EXPORT_SELECTION_WIZARD);
	//issue  1616:
	bool bAskUnicode=true;
	if (!g_pSettings->GetApplicationOption(APP_EXPORT_IMPORT_SHOW_OPTION_USE_UNICODE).toBool())
	{
		bAskUnicode=false;
		wizReg.m_hshWizardPageHash[WizardRegistration::PROJECT_EXPORT_SELECTION_WIZARD]->RemoveLastPage();
	}
	WizardBase wiz(WizardRegistration::PROJECT_EXPORT_SELECTION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);
	//wiz.SetRetryMode();

	//MR: issue #2493: "open it centered in 2/3 of the height and width of the actual screen"
	int nDesktopWidth = QApplication::desktop()->availableGeometry().width();	//account for dock bar
	int nDesktopHeight = QApplication::desktop()->availableGeometry().height();	//account for dock bar
	wiz.resize(nDesktopWidth*2/3, nDesktopHeight*2/3);	//resize window to 2/3 of desktop
	wiz.move(nDesktopWidth/6, nDesktopHeight/6);	//center the window on screen (1/6 of screen left from each side)

	//If wizard accepted.
	if (wiz.exec())
	{
		wiz.GetWizPageRecordSetList(RecordSetList);
		DbRecordSet lstProjects;
		lstProjects=RecordSetList.at(0);
		bool bUtf8 =m_ImportExportManager->IsUnicodeEnabled();
		if (bAskUnicode)
			bUtf8 = RecordSetList.at(2).getDataRef(0,0).toBool();
		Status err;
		m_ImportExportManager->ExportProjects(err,RecordSetList.at(1).getDataRef(0,0).toString(),bUtf8,&lstProjects);
		if (err.getErrorCode()==StatusCodeSet::ERR_BUS_EXPORT_IMPORT_SUCESS)
		{
			QMessageBox::information(this,tr("Information"),err.getErrorText());
		}
		else
		{
			_CHK_ERR(err);
		}
		
	}

}

void sokrates_spc::OnMenu_ExportUsers()
{
	if(!m_ImportExportManager) return;

	QList<DbRecordSet> RecordSetList;
	WizardRegistration wizReg(WizardRegistration::USER_EXPORT_SELECTION_WIZARD);
	//issue  1616:
	bool bAskUnicode=true;
	if (!g_pSettings->GetApplicationOption(APP_EXPORT_IMPORT_SHOW_OPTION_USE_UNICODE).toBool())
	{
		bAskUnicode=false;
		wizReg.m_hshWizardPageHash[WizardRegistration::USER_EXPORT_SELECTION_WIZARD]->RemoveLastPage();
	}
	WizardBase wiz(WizardRegistration::USER_EXPORT_SELECTION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);
	//wiz.SetRetryMode();

	//If wizard accepted.
	if (wiz.exec())
	{
		wiz.GetWizPageRecordSetList(RecordSetList);
		DbRecordSet lstProjects;
		lstProjects=RecordSetList.at(0);

		bool bUtf8 =m_ImportExportManager->IsUnicodeEnabled();
		if (bAskUnicode)
			bUtf8 = RecordSetList.at(2).getDataRef(0,0).toBool();

		Status err;
		m_ImportExportManager->ExportUsers(err,RecordSetList.at(1).getDataRef(0,0).toString(),bUtf8,&lstProjects);
		if (err.getErrorCode()==StatusCodeSet::ERR_BUS_EXPORT_IMPORT_SUCESS)
		{
			QMessageBox::information(this,tr("Information"),err.getErrorText());
		}
		else
		{
			_CHK_ERR(err);
		}
	}
}

void sokrates_spc::OnMenu_ExportContacts()
{
	if(!m_ImportExportManager) return;

	QList<DbRecordSet> RecordSetList;
	WizardRegistration wizReg(WizardRegistration::CONTACT_EXPORT_SELECTION_WIZARD);
	//issue  1616:
	bool bAskUnicode=true;
	if (!g_pSettings->GetApplicationOption(APP_EXPORT_IMPORT_SHOW_OPTION_USE_UNICODE).toBool())
	{
		bAskUnicode=false;
		wizReg.m_hshWizardPageHash[WizardRegistration::CONTACT_EXPORT_SELECTION_WIZARD]->RemoveLastPage();
	}
	WizardBase wiz(WizardRegistration::CONTACT_EXPORT_SELECTION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);
	//wiz.SetRetryMode();

	//MR: issue #2493: "open it centered in 2/3 of the height and width of the actual screen"
	int nDesktopWidth = QApplication::desktop()->availableGeometry().width();	//account for dock bar
	int nDesktopHeight = QApplication::desktop()->availableGeometry().height();	//account for dock bar
	wiz.resize(nDesktopWidth*2/3, nDesktopHeight*2/3);	//resize window to 2/3 of desktop
	wiz.move(nDesktopWidth/6, nDesktopHeight/6);	//center the window on screen (1/6 of screen left from each side)

	//If wizard accepted.
	if (wiz.exec())
	{
		wiz.GetWizPageRecordSetList(RecordSetList);
		DbRecordSet lstContacts;
		lstContacts=RecordSetList.at(0);
		bool bUtf8 =m_ImportExportManager->IsUnicodeEnabled();
		if (bAskUnicode)
			bUtf8 = RecordSetList.at(2).getDataRef(0,0).toBool();

		//leave only ID field filled (lighter traffic)
		DbRecordSet lstContactsLite;
		lstContactsLite.addColumn(QVariant::Int,"BCNT_ID");
		lstContactsLite.merge(lstContacts);
		//lstContactsLite.Dump();

		QString strOutFile  = RecordSetList.at(1).getDataRef(0,0).toString();
		bool bSokratesHdr   = RecordSetList.at(1).getDataRef(0,1).toBool();
		bool bColumnTitles  = RecordSetList.at(1).getDataRef(0,2).toBool();
		bool bOutlookExport = RecordSetList.at(1).getDataRef(0,3).toBool();
		QString strLang = g_pClientManager->GetIniFile()->m_strLangCode;


		Status err;
		g_pClientManager->ShowTransferProgressDlg(tr("Export of the contacts in progress"));
		m_ImportExportManager->ExportContacts(err,strOutFile, bUtf8, bSokratesHdr, bColumnTitles, bOutlookExport, strLang, &lstContactsLite);
		g_pClientManager->HideTransferProgressDlg();
		if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED)
			return;

		if (err.IsOK())
		{
			//QMessageBox::information(this,tr("Information"),err.getErrorText());
			QMessageBox::information(this,tr("Information"),tr("Done!"));
		}
		else
		{
			_CHK_ERR(err);
		}
	}
}

void sokrates_spc::OnMenu_ExportContactContactRel()
{
	if(!m_ImportExportManager) return;

	QList<DbRecordSet> RecordSetList;
	WizardRegistration wizReg(WizardRegistration::CONTACT_EXPORT_SELECTION_WIZARD);
	//issue  1616:
	bool bAskUnicode=true;
	if (!g_pSettings->GetApplicationOption(APP_EXPORT_IMPORT_SHOW_OPTION_USE_UNICODE).toBool())
	{
		bAskUnicode=false;
		wizReg.m_hshWizardPageHash[WizardRegistration::CONTACT_EXPORT_SELECTION_WIZARD]->RemoveLastPage();
	}
	WizardBase wiz(WizardRegistration::CONTACT_EXPORT_SELECTION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);
	
	//hide "Export to Outlook"
	SimpleSelectionWizFileSelectionExp* pWizPage = (SimpleSelectionWizFileSelectionExp *)wiz.GetWizardPage(1);
	if(pWizPage){
		pWizPage->m_bHideOutlookExpChk = true;
	}
	//wiz.SetRetryMode();

	//MR: issue #2493: "open it centered in 2/3 of the height and width of the actual screen"
	int nDesktopWidth = QApplication::desktop()->availableGeometry().width();	//account for dock bar
	int nDesktopHeight = QApplication::desktop()->availableGeometry().height();	//account for dock bar
	wiz.resize(nDesktopWidth*2/3, nDesktopHeight*2/3);	//resize window to 2/3 of desktop
	wiz.move(nDesktopWidth/6, nDesktopHeight/6);	//center the window on screen (1/6 of screen left from each side)

	//If wizard accepted.
	if (wiz.exec())
	{
		wiz.GetWizPageRecordSetList(RecordSetList);
		DbRecordSet lstContacts;
		lstContacts=RecordSetList.at(0);
		bool bUtf8 =m_ImportExportManager->IsUnicodeEnabled();
		if (bAskUnicode)
			bUtf8 = RecordSetList.at(2).getDataRef(0,0).toBool();

		//leave only ID field filled (lighter traffic)
		DbRecordSet lstContactsLite;
		lstContactsLite.addColumn(QVariant::Int,"BCNT_ID");
		lstContactsLite.merge(lstContacts);
		//lstContactsLite.Dump();

		QString strOutFile  = RecordSetList.at(1).getDataRef(0,0).toString();
		bool bSokratesHdr   = RecordSetList.at(1).getDataRef(0,1).toBool();
		bool bColumnTitles  = RecordSetList.at(1).getDataRef(0,2).toBool();
		bool bOutlookExport = false; //RecordSetList.at(1).getDataRef(0,3).toBool();
		QString strLang = g_pClientManager->GetIniFile()->m_strLangCode;


		Status err;
		g_pClientManager->ShowTransferProgressDlg(tr("Export of the contacts in progress"));
		m_ImportExportManager->ExportContactContactRelationships(err,strOutFile, bUtf8, bSokratesHdr, bColumnTitles, bOutlookExport, strLang, &lstContactsLite);
		g_pClientManager->HideTransferProgressDlg();
		if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED)
			return;

		if (err.IsOK())
		{
			//QMessageBox::information(this,tr("Information"),err.getErrorText());
			QMessageBox::information(this,tr("Information"),tr("Done!"));
		}
		else
		{
			_CHK_ERR(err);
		}
	}
}

void sokrates_spc::OnMenu_ProjectsSync()
{
	Status err;
	_SERVER_CALL(ServerControl->StartProcessProject(err))
	_CHK_ERR(err);
	if (g_pClientManager->IsThinClient())
	{
		QMessageBox::information(this,tr("Information"),tr("Process started on server. You will be notified when operation is completed!"));
		return;
	}
	QMessageBox::information(this,tr("Information"),tr("Projects successfully synchronized, check log for details."));
	//clear cache:
	g_ClientCache.PurgeCache(ENTITY_BUS_PROJECT);
	QVariant empty;
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_PROJECT,empty);

}
void sokrates_spc::OnMenu_UserSync()
{
	Status err;
	_SERVER_CALL(ServerControl->StartProcessUser(err))
	_CHK_ERR(err);
	if (g_pClientManager->IsThinClient())
	{
		QMessageBox::information(this,tr("Information"),tr("Process started on server. You will be notified when operation is completed!"));
		return;
	}
	QMessageBox::information(this,tr("Information"),tr("Users successfully synchronized, check log for details."));

	//clear cache:
	g_ClientCache.PurgeCache(ENTITY_BUS_PERSON);
	QVariant empty;
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_PERSON,empty);

}
void sokrates_spc::OnMenu_DebtorSync()
{
	Status err;
	_SERVER_CALL(ServerControl->StartProcessDebtor(err));
	_CHK_ERR(err);
	if (g_pClientManager->IsThinClient())
	{
		QMessageBox::information(this,tr("Information"),tr("Process started on server. You will be notified when operation is completed!"));
		return;
	}
	QMessageBox::information(this,tr("Information"),tr("Debtors successfully synchronized, check log for details."));

	//clear cache:
	g_ClientCache.PurgeCache(ENTITY_BUS_CONTACT);
	QVariant empty;
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_CONTACT,empty);
}


void sokrates_spc::OnMenu_ImportCenter()
{
	QDialog *pDialog= new ImportCenter(this);
	pDialog->setModal(false);
	pDialog->show();
}


void sokrates_spc::OnMenu_DesktopClick()
{
	on_MenuButton_clicked(MENU_BUTTON_DESKTOP);
}


void sokrates_spc::OnMenu_ContactClick()
{
	on_MenuButton_clicked(MENU_BUTTON_CONTACT);
}

void sokrates_spc::OnMenu_CalendarClick()
{
	on_MenuButton_clicked(MENU_BUTTON_CALENDAR);
}

void sokrates_spc::OnMenu_ProjManagerClick()
{
	on_MenuButton_clicked(MENU_BUTTON_PROJMANAGER);
}

void sokrates_spc::OnMenu_ProjectClick()
{
	on_MenuButton_clicked(MENU_BUTTON_PROJECT);
}


void sokrates_spc::OnMenu_ThickCheckForUpdates()
{
	int nRes=g_pClientManager->CheckForUpdatesThickClient();
	if (nRes==2)
	{
		qApp->quit();
		return;
	}
	else if (nRes==0)
	{
		QMessageBox::information(this,tr("Information"),tr("Update not available"));
		return;
	}

}



void sokrates_spc::OnThemeChanged()
{
	if(m_nCurrentWindowTheme==ThemeManager::GetCurrentThemeID()) return;

	//header:
	if (m_pPix1)m_pPix1->setPixmap(QPixmap(ThemeManager::GetHeaderLeft(g_pClientManager->GetModuleCode())));
	if (m_pPix2)m_pPix2->setPixmap(QPixmap(ThemeManager::GetHeaderRight()));
	if (m_pView)m_pView->setBackgroundBrush(QPixmap(ThemeManager::GetHeaderCenter()));

	//toolbar:
	ui.frameToolBar->setStyleSheet(ThemeManager::GetToolbarBkgStyle());
	QString	styleNew=ThemeManager::GetToolbarButtonStyle();
	m_pBtnComm->setStyleSheet(styleNew);
	m_pBtnContact->setStyleSheet(styleNew);
	m_pBtnProject->setStyleSheet(styleNew);
	m_pBtnCalendar->setStyleSheet(styleNew);

	m_pInfo->setText(	QString("<html><body style=\" font-family:MS Shell Dlg 2; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\
							<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic; color:%2;\">%1</span></p>\
							</body></html>").arg(tr("Click=Open<br>CTRL-Click=Open New Window<br>ALT-Click=Open New Tab<br>ALT-CTRL-Click=Open New Selection")).arg(ThemeManager::GetToolbarTextColor()));

	m_nCurrentWindowTheme=ThemeManager::GetCurrentThemeID();
}

void sokrates_spc::OnViewChanged()
{
	if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
	{
		showMaximized();
		OnClientLogged();
	}
	else
		hide();

	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_PRO)
	{
		m_pView_Light->setChecked(false);
		m_pView_SideBar->setChecked(false);
		m_pView_Pro->setChecked(true);
	}

	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_EASY)
	{
		m_pView_Light->setChecked(true);
		m_pView_SideBar->setChecked(false);
		m_pView_Pro->setChecked(false);
	}
}

void sokrates_spc::OnHeader_SwitcherClick()
{
	OnSwitcher_Sidebar();
}


void sokrates_spc::OnSwitcher_Pro()
{
	g_pGlobalObjectManager->ChangeView(ThemeManager::VIEW_PRO);
}
void sokrates_spc::OnSwitcher_Light()
{
	g_pGlobalObjectManager->ChangeView(ThemeManager::VIEW_EASY);

}
void sokrates_spc::OnSwitcher_Sidebar()
{
	//cursor:
	QApplication::setOverrideCursor(Qt::WaitCursor);
	g_pGlobalObjectManager->ChangeView(ThemeManager::VIEW_SIDEBAR);
	QApplication::restoreOverrideCursor();
}



void sokrates_spc::OpenStartUpFUIs()
{

	//get settings
	int nFuiToOpen=g_pSettings->GetPersonSetting(STARTING_MODULE).toInt();

	switch(nFuiToOpen)
	{
	case 0:
		{
			OpenDesktopFUIWithViews();
		}
		break;
	case 3:
		{
			g_objFuiManager.OpenFUI(MENU_CONTACTS, false, false);
		}
		break;
	case 4:
		{
			//_START_TIMER(open_contact);

			//open FUI
			int nNewFUI=g_objFuiManager.OpenFUI(MENU_CONTACTS, false, false,FuiBase::MODE_EMPTY, -1,true);
			FUI_Contacts* pFui=dynamic_cast<FUI_Contacts*>(g_objFuiManager.GetFUIWidget(nNewFUI));
			if (pFui)
			{
				pFui->SetCurrentTab(1);
				pFui->show();  
			}

			//_STOP_TIMER(open_contact);
		}
		break;
	case 5:
		g_objFuiManager.OpenFUI(MENU_PROJECTS, false, false);
		break;
	default:
		g_objFuiManager.OpenFUI(MENU_CONTACTS, false, false);
		break;
	}

}



//TRUE if to exit login procedure
/*
bool sokrates_spc::ProcessOnStartUpWizard(Status &pStatus)
{
	g_pClientManager->StopProgressDialog();

	Status err;
	bool bRestart=StartupWizard::StartWizard(); //start wizard
	StartupWizard::ResetOnStartUpFlag();		//reset flags
	//g_pClientManager->SaveIniFile();
	//g_pSettings->SavePersonSettings(err);
	*/
	/*
	if (bRestart)//ignore
	{
		Status err;
		g_pBoSet->app->UserLogon->Logout(err);
		return true;
	}
	else
	{
		//g_pClientManager->ReadUserData(pStatus);
		if(!pStatus.IsOK()) 
		{
			Status err;
			g_pBoSet->app->UserLogon->Logout(err);
			return true;
		}
*/
/*
		//load persons & org:
		MainEntitySelectionController pers;
		pers.Initialize(ENTITY_BUS_PERSON);
		pers.ReloadData(true);

		MainEntitySelectionController org;
		org.Initialize(ENTITY_BUS_ORGANIZATION);
		org.ReloadData(true);

		//store username back
		//g_pClientManager->GetIniFile()->m_strLastUserName=g_pClientManager->GetLoggedUserName();
		//g_pClientManager->GetIniFile()->m_strConnectionName=g_pClientManager->GetConnectionName();

	//}
	g_pClientManager->StartProgressDialog(tr("Login in Progress..."));

	return false;
}
*/



void sokrates_spc::OnMenu_PortableSkypeInstall()
{
	int nResult = QMessageBox::question(this, "", tr("Copy the actual Skype(R) installation from hard disk to your portable memory device. This can take a long time. Continue?"), tr("Yes"),tr("No"));
	if (nResult==0)
	{
		g_pGlobalObjectManager->CopyPortableSkype();
	}
}



void sokrates_spc::OnMenu_PortableSkypeUpdate()
{
	int nResult = QMessageBox::question(this, "", tr("Do you want to update your Skype(R) installation on your portable memory device using your Skype(R) installation on the hard disk?"), tr("Yes"),tr("No"));
	if (nResult==0)
	{
		g_pGlobalObjectManager->UpdatePortableSkype();
	}
}

void sokrates_spc::OnMenu_PortableAppRegister()
{

}

void sokrates_spc::OnServerMessageReceived(int nCode, QString strText)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Server message received"));

	//B.T. 2013.03.05, issue 2700: just reload STARTUP AVATAR setting to ensure that settings is correctly read out: settings are saved on user session log out...
	Status err;
	g_pSettings->ReaLoadAvatarStartupSetting(err);

	if( nCode == StatusCodeSet::ERR_BUS_NEW_TASK_NOTIFICATION ||
		nCode == StatusCodeSet::ERR_BUS_TASK_NOTIFY_ORIGINATOR)
	{
		//parse string of the format "BTKS_ID;BTKS_TASK_TYPE_ID;BTKS_SYSTEM_TYPE;BTKS_SYSTEM_STATUS;ID;ORIGINATOR_ID"
		QStringList lstData = strText.split(";");
		Q_ASSERT(lstData.size() == 6);
		int nTaskID = lstData[0].toInt();
		int nTaskSystemType = lstData[2].toInt(); //TASK_TYPE_SCHEDULED_DOC=1,TASK_TYPE_SCHEDULED_VOICE_CALL=2,TASK_TYPE_SCHEDULED_EMAIL=3
		int nRecID = lstData[4].toInt();
		int nOriginRecID = lstData[5].toInt();

		//open the task with record ID
		int nFuiType = -1;
		if(GlobalConstants::TASK_TYPE_SCHEDULED_DOC == nTaskSystemType)
			nFuiType = MENU_DM_DOCUMENTS;
		else if(GlobalConstants::TASK_TYPE_SCHEDULED_EMAIL == nTaskSystemType)
			nFuiType = MENU_EMAIL;
		else if(GlobalConstants::TASK_TYPE_SCHEDULED_VOICE_CALL == nTaskSystemType)
			nFuiType = MENU_COMM_VOICE_CENTER;
		else
			Q_ASSERT(false);

		if(nCode == StatusCodeSet::ERR_BUS_TASK_NOTIFY_ORIGINATOR)
		{
			//try finding already opened FUI for this task ID
			int nGID = g_objFuiManager.FindFuiByType(nFuiType, nTaskID);
			if(nGID >= 0)
			{
				//refresh and activate existing FUI/Avatar instead of opening new one
				g_objFuiManager.RefreshFUI(nGID, true);
				QApplication::beep();
				return;
			}
			//MR: commenting lines below fixes the issue #2712
			if (g_pClientManager->GetPersonID()==nOriginRecID)
				return;
			//if (g_pClientManager->GetPersonID()!=nOriginRecID) //else if originator, open new
			//	return; //BT: 2009-10-12 MB on Skype told that if TASK is modified only currently open task card are changed and there is no need to reopen task card if not already shown!
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Open FUI to handle server message"));

		//open FUI
		int nGID = -1;
		if(nFuiType != MENU_COMM_VOICE_CENTER)
			nGID = g_objFuiManager.OpenFUI(nFuiType,true,false,FuiBase::MODE_READ,nRecID, true);
		else
			nGID = g_objFuiManager.OpenFUI(nFuiType,true,false);

		//minimize immediately to make it an avatar
		QWidget *pFui = g_objFuiManager.GetFUIWidget(nGID);
		if(pFui){
			if(nFuiType == MENU_COMM_VOICE_CENTER){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Need to load call data"));
				((FUI_VoiceCallCenter *)pFui)->LoadCallData(nRecID);
			}
			pFui->showMinimized();

			//make it "Open on startup" by default (issue #2186)
			//dynamic_cast<FuiBase*>(pFui)->m_pWndAvatar->OnMenu_Startup();

			QTimer::singleShot(0,pFui,SLOT(hide()));//B.T. delay hide
		}

		QApplication::beep();
	}

	//invite notification:
	if (nCode == StatusCodeSet::ERR_BUS_NEW_INVITE_NOTIFICATION)
	{
		//QApplication::beep();
		//QMessageBox::information(this,"New msg",strText);
		//qDebug()<<strText;
		//QApplication::beep();

		QStringList lst=strText.split(";",QString::SkipEmptyParts);
		int nCalEvID=lst.at(0).toInt();
		int nPartID=lst.at(1).toInt();
		int nOwnerID=lst.at(2).toInt();
		int nInviteID=lst.at(3).toInt();
		int nPersonID=lst.at(4).toInt();

		QString strInitials;
		DbRecordSet recPers;
		MainEntitySelectionController cachePerson;
		cachePerson.Initialize(ENTITY_BUS_PERSON);
		cachePerson.ReloadData();
		cachePerson.GetEntityRecord(nOwnerID, recPers);
		if (recPers.getRowCount()>0){
			recPers.getData(0,"BPER_INITIALS ",strInitials);
		}

		//call part info
		Status status;
		DbRecordSet RetOut_RowPart;
		DbRecordSet RetOut_RowActiveOption;
		_SERVER_CALL(BusCalendar->ReadOnePart(status, nPartID, RetOut_RowPart, RetOut_RowActiveOption));
		_CHK_ERR(status);

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Open Invitation card to handle server message"));

		//popup the invitation card
		int nNewFUI=g_objFuiManager.OpenFUI(MENU_INVITATION_CARD,true,false,FuiBase::MODE_EMPTY);
		FUI_InvitationCard* pFui=dynamic_cast<FUI_InvitationCard*>(g_objFuiManager.GetFUIWidget(nNewFUI));
		if (pFui)
		{
			pFui->SetInvitationInfo(nCalEvID, nPartID, nOwnerID, nInviteID, nPersonID);
			pFui->SetOwnerInitials(strInitials);

			if(RetOut_RowActiveOption.getRowCount()>0){
				QString strSubject = RetOut_RowActiveOption.getDataRef(0, "BCOL_SUBJECT").toString();
				pFui->SetLabel(strSubject);
			}
			if(RetOut_RowPart.getRowCount()>0){
				QString strDates("  ");
				strDates += RetOut_RowPart.getDataRef(0, "PART_FROM").toDateTime().toString("dd.MM.yyyy hh:mm");
				strDates += "\n- ";
				strDates += RetOut_RowPart.getDataRef(0, "PART_TO").toDateTime().toString("dd.MM.yyyy hh:mm");
				pFui->SetTaskDate(strDates);
			}
		}
	}

	//back to sender 
	if (nCode == StatusCodeSet::ERR_BUS_INVITE_NOTIFY_ORIGINATOR)
	{
		QStringList lst=strText.split(";",QString::SkipEmptyParts);
		int nCalEvID=lst.at(0).toInt();
		int nPartID=lst.at(1).toInt();
		int nOwnerID=lst.at(2).toInt();
		int nInviteID=lst.at(3).toInt();
		int nContactId_ConfirmedBy=lst.at(4).toInt();
		int nPersonID_ConfirmedBy=lst.at(5).toInt();
		int nStatus=lst.at(6).toInt();

		//get owner name
		QString strContactName;
		QString strInitials;
		DbRecordSet recPers;
		MainEntitySelectionController cachePerson;
		cachePerson.Initialize(ENTITY_BUS_PERSON);
		cachePerson.ReloadData();
		cachePerson.GetEntityRecord(nOwnerID, recPers);
		int nOrgID = 0;
		if (recPers.getRowCount()>0){
			QString strName, strLastName;
			recPers.getData(0,"BPER_FIRST_NAME",strName);
			recPers.getData(0,"BPER_LAST_NAME",strLastName);
			strContactName = strName + " " + strLastName;
			recPers.getData(0,"BPER_ORGANIZATION_ID",nOrgID);
		}

		//get organization
		QString strOrganization;
		DbRecordSet recOrg;
		MainEntitySelectionController cacheOrg;
		cacheOrg.Initialize(ENTITY_BUS_ORGANIZATION);
		cacheOrg.ReloadData();
		cacheOrg.GetEntityRecord(nOrgID, recOrg);
		if (recOrg.getRowCount()>0){
			recOrg.getData(0,"BORG_NAME",strOrganization);
		}

		//call part info
		Status status;
		DbRecordSet RetOut_RowPart;
		DbRecordSet RetOut_RowActiveOption;
		_SERVER_CALL(BusCalendar->ReadOnePart(status, nPartID, RetOut_RowPart, RetOut_RowActiveOption));
		_CHK_ERR(status);

		QString strEventTitle;
		if(RetOut_RowActiveOption.getRowCount()>0){
			strEventTitle = RetOut_RowActiveOption.getDataRef(0, "BCOL_SUBJECT").toString();
		}
		QString strDateFrom;
		QString strDateTo;
		if(RetOut_RowPart.getRowCount()>0){
			strDateFrom = RetOut_RowPart.getDataRef(0, "PART_FROM").toDateTime().toString("dd.MM.yyyy hh:mm");
			strDateTo = RetOut_RowPart.getDataRef(0, "PART_TO").toDateTime().toString("dd.MM.yyyy hh:mm");
		}

		QString strAction;
		switch(nStatus){
			case GlobalConstants::STATUS_CAL_INVITE_CONFIRMED:
				strAction = tr("confirmed");	break;
			case GlobalConstants::STATUS_CAL_INVITE_REJECTED:
				strAction = tr("rejected");	break;
			case GlobalConstants::STATUS_CAL_INVITE_NONE:
				strAction = tr("none");	break;	
			case GlobalConstants::STATUS_CAL_INVITE_ANSWERED:
				strAction = tr("answered");	break;	
		}

		//"The contact/User [name] [organization] has confirmed/rejected your invitation for the Calendar Event [event title] from [from] to [to]. {Open Event} {OK}"
		//qDebug()<<strText;
		QApplication::beep();
		QString strMsg = QString(tr("The contact/User %1 %2 has %6 your invitation for the Calendar Event %3 from %4 to %5.")).arg(strContactName).arg(strOrganization, strEventTitle).arg(strDateFrom).arg(strDateTo).arg(strAction);
		QMessageBox msgBox;
		QPushButton *pBtnOpen = msgBox.addButton (tr("Open Event"), QMessageBox::YesRole);
		QPushButton *pBtnOK   = msgBox.addButton (QMessageBox::Ok);
		msgBox.setText(strMsg);
		msgBox.setWindowTitle(tr("Owner notified"));

		msgBox.exec();
		if (msgBox.clickedButton() == pBtnOpen) 
		{
			//open the FUI with invitation details
			int nNewFUI=g_objFuiManager.OpenFUI(BUS_CAL_EVENT,true,false,FuiBase::MODE_READ);
			FUI_CalendarEvent* pFui=dynamic_cast<FUI_CalendarEvent*>(g_objFuiManager.GetFUIWidget(nNewFUI));
			if (pFui)
			{
				pFui->GoInviteAcceptMode(nCalEvID, nPartID, nInviteID, g_pClientManager->GetPersonID(), g_pClientManager->GetPersonContactID(), nOwnerID, true);
			}
		}
	}

	if( nCode == StatusCodeSet::MSG_BUS_NEW_REMINDER_NOTIFICATION)
	{
		//ola

	}
}

void sokrates_spc::OnCheckInAllChangedDocuments()
{
	DocumentHelper::DWatcher_SaveChanges(true);
}

//issue 2246: create shortcuts:
void sokrates_spc::keyPressEvent ( QKeyEvent * event ) 
{
	if(!g_pClientManager->IsLogged())return;

	switch(event->key())
	{
	case Qt::Key_F1: 
		{
			event->accept();

			if (event->modifiers() & Qt::AltModifier)
			{
				OnMenu_Support(); // F1 is reserved for online help.
			}
			else
			{
				OnMenu_OnlineHelp(); //ALT-F1 Support/Publish Screen for Support (-> Netviewer)
			}
			return;
		}
		break;
	case Qt::Key_F2: 
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier) //ALT-F2: Switches "Overview" on an off
			{

				FUI_Contacts* pFui=dynamic_cast<FUI_Contacts*>(g_objFuiManager.GetActiveFUI());
				if (pFui)
				{
					pFui->FlipShowQuickInfo();
				}
			}
			else
			{
				OnSwitcher_Sidebar(); //F2 can be used to switch between sidebar and pro view.
			}
			return;
		}
		break;
	case Qt::Key_F3:
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier) //ALT-F3: Switches between "Contacts" and "Favorites" (Contacts FUI only)
			{
				FUI_Contacts* pFui=dynamic_cast<FUI_Contacts*>(g_objFuiManager.GetActiveFUI());
				if (pFui)
				{
					pFui->FlipSelectorToFavoriteTabOrList();
				}
			}
		}
		break;
	case Qt::Key_F5:
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier) //ALT-F5: Open Tabs Details + Description
			{
				FUI_Contacts* pFui=dynamic_cast<FUI_Contacts*>(g_objFuiManager.GetActiveFUI());
				if (pFui)
				{
					pFui->SetCurrentTab(FUI_Contacts::TAB_MAIN_DETAILS,FUI_Contacts::TAB_DETAIL_DESC);
				}
				FUI_Projects* pFui2=dynamic_cast<FUI_Projects*>(g_objFuiManager.GetActiveFUI());
				if (pFui2)
				{
					pFui2->SetCurrentTab(FUI_Projects::TAB_MAIN_DETAILS,FUI_Projects::TAB_DETAIL_DESC);
				}
			}
			else
				PassKeyEventToCommmenu(event);
			return;
		}
	    break;
	case Qt::Key_F6://ALT-F6: Open Tabs Documents&Relations + Overview
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier) //ALT-F5: Open Tabs Details + Description
			{
				FUI_Contacts* pFui=dynamic_cast<FUI_Contacts*>(g_objFuiManager.GetActiveFUI());
				if (pFui)
				{
					pFui->SetCurrentTab(FUI_Contacts::TAB_MAIN_COMM,FUI_Contacts::TAB_COMM_OVERVIEW);
				}
				FUI_Projects* pFui2=dynamic_cast<FUI_Projects*>(g_objFuiManager.GetActiveFUI());
				if (pFui2)
				{
					pFui2->SetCurrentTab(FUI_Projects::TAB_MAIN_COMM,FUI_Projects::TAB_COMM_OVERVIEW);
				}
			}
			else
				PassKeyEventToCommmenu(event);
			return;

		}
		break;
	case Qt::Key_F7: //ALT-F7: Open Tabs Documents&Relations + Contacts/Users
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier) 
			{
				FUI_Contacts* pFui=dynamic_cast<FUI_Contacts*>(g_objFuiManager.GetActiveFUI());
				if (pFui)
				{
					pFui->SetCurrentTab(FUI_Contacts::TAB_MAIN_COMM,FUI_Contacts::TAB_COMM_USERS_CONTACTS);
				}
				FUI_Projects* pFui2=dynamic_cast<FUI_Projects*>(g_objFuiManager.GetActiveFUI());
				if (pFui2)
				{
					pFui2->SetCurrentTab(FUI_Projects::TAB_MAIN_COMM,FUI_Projects::TAB_COMM_USERS_CONTACTS);
				}
			}
			else
				PassKeyEventToCommmenu(event);

			return;
		}
		break;
	case Qt::Key_F8: //ALT-F8: Open Tabs Documents&Relations + Assigned Projcts (Contact FUI) or Details + Budget (Project FUI)
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier) 
			{
				FUI_Contacts* pFui=dynamic_cast<FUI_Contacts*>(g_objFuiManager.GetActiveFUI());
				if (pFui)
				{
					pFui->SetCurrentTab(FUI_Contacts::TAB_MAIN_COMM,FUI_Contacts::TAB_COMM_PROJECTS);
				}
				FUI_Projects* pFui2=dynamic_cast<FUI_Projects*>(g_objFuiManager.GetActiveFUI());
				if (pFui2)
				{
					pFui2->SetCurrentTab(FUI_Projects::TAB_MAIN_DETAILS,FUI_Projects::TAB_DETAIL_BUDGET);
				}
			}
			else
				PassKeyEventToCommmenu(event);
			return;
		}
		break;
	case Qt::Key_F9: //ALT-F9: Open Calendar
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier) 
			{
				g_objFuiManager.OpenFUI(MENU_CALENDAR);
			}
			else
				PassKeyEventToCommmenu(event);
			return;
		}
		break;
	case Qt::Key_F10: 
	case Qt::Key_F11:
	case Qt::Key_F12:
		{
			if (PassKeyEventToCommmenu(event))
				return;
		}
		break;

	}


	QMainWindow::keyPressEvent(event);
}

//true event catched, else not
bool sokrates_spc::PassKeyEventToCommmenu(QKeyEvent * event)
{
	FUI_Contacts* pFui=dynamic_cast<FUI_Contacts*>(g_objFuiManager.GetActiveFUI());
	if (pFui)
	{
		CommunicationMenu* px=dynamic_cast<CommunicationMenu*>(pFui->GetComMenu());
		if (px)
			return px->ProcessKeyPressEventFromMainWindow(event);
	}
	FUI_Projects* pFui2=dynamic_cast<FUI_Projects*>(g_objFuiManager.GetActiveFUI());
	if (pFui2)
	{
		CommunicationMenu* px=dynamic_cast<CommunicationMenu*>(pFui2->GetComMenu());
		if (px)
			return px->ProcessKeyPressEventFromMainWindow(event);
	}
	FUI_CommunicationCenter* pFui3=dynamic_cast<FUI_CommunicationCenter*>(g_objFuiManager.GetActiveFUI());
	if (pFui3)
	{
		CommunicationMenu* px=dynamic_cast<CommunicationMenu*>(pFui3->GetComMenu());
		if (px)
			return px->ProcessKeyPressEventFromMainWindow(event);
	}


	return false;
}

void sokrates_spc::OnMenu_ImportStoredProjectLists()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.dat";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;

	QString strHdrFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Choose Stored Project Lists Headers file"),
		strStartDir,
		strFilter);
	if (strHdrFile.isEmpty())
		return;
	
	//same dir where header was found
	strStartDir = QFileInfo(strHdrFile).dir().absolutePath();

	QString strDetailFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Choose Stored Project Lists Details file"),
		strStartDir,
		strFilter);
	if (strDetailFile.isEmpty())
		return;

	//Load both files into the byte array object
	QFile fileHdr(strHdrFile);
	if(!fileHdr.open(QIODevice::ReadOnly)){
		QMessageBox::information(this, "Error", QString(tr("Failed to load header file:\n%1!")).arg(strHdrFile));
		return;
	}
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	QByteArray datHeader = fileHdr.readAll();

	QFile fileDetail(strDetailFile);
	if(!fileDetail.open(QIODevice::ReadOnly)){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Error", QString(tr("Failed to load detail file:\n%1!")).arg(strDetailFile));
		return;
	}
	QByteArray datDetail = fileDetail.readAll();

	//fix for long server operation causing issue with the IsAlive timer is to do entire operation on client
	Status status;
	ClientApiHelper::ProcessStoredProjectList_Import(status, datHeader, datDetail);	 
	//_SERVER_CALL(BusImport->ProcessStoredProjectList_Import(status,datHeader, datDetail))
	if(!status.IsOK()){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Error", status.getErrorText());
		return;
	}
	QApplication::restoreOverrideCursor();
}

void sokrates_spc::OnMenu_ImportUserDefinedFields()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.dat";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;

	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Choose Contact User-Defined Fields import file"),
		strStartDir,
		strFilter);
	if (strFile.isEmpty())
		return;
	
	//Load data
	QSize size(300,100);
	QProgressDialog progress(tr("Loading Import File..."),tr("Cancel"),0,0,this);
	progress.setWindowTitle(tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	Status status;
	CsvImportFile import;
	QStringList lstFormatPlain;
	lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Contacts - Subformat: Contact User-Defined Field";
	QStringList lstFormatUtf8;
	lstFormatUtf8 << "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Contacts - Subformat: Contact User-Defined Field";
	DbRecordSet data;
	import.Load(status, strFile, data, lstFormatPlain, lstFormatUtf8, false, CsvProgress, (unsigned long)&progress);
	if(!status.IsOK()){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Error", status.getErrorText());
		return;
	}
	int nFieldsCnt = data.getColumnCount();
	Q_ASSERT(nFieldsCnt == 4);
	//data.Dump();

	int nCnt = data.getRowCount();
	if(nCnt < 1){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Error", tr("Invalid file format!"));
		return;
	}
	QString strFieldName = data.getDataRef(0, 0).toString();

	//fill formatted list
	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_CONTACT_DATA_IMPORT));
	
	//merge results with destination list (skip line with custom field name)
	for(int i=1; i<nCnt; i++)
	{
		lstData.addRow();
		int nCol = 0;
		int nRow = i-1;
		lstData.setData(nRow, "BCNT_FIRSTNAME",				data.getDataRef(i, nCol++).toString());
		lstData.setData(nRow, "BCNT_LASTNAME",				data.getDataRef(i, nCol++).toString());
		lstData.setData(nRow, "BCNT_ORGANIZATIONNAME",		data.getDataRef(i, nCol++).toString());
		lstData.setData(nRow, "BCF_VALUE",		            data.getDataRef(i, nCol++).toString());

		//update progress
		progress.setValue(1);
		qApp->processEvents();
		if (progress.wasCanceled())
			break;
	}

	if (progress.wasCanceled()){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Information", tr("Import canceled!"));
		return;
	}

	//fix for long server operation causing issue with the IsAlive timer is to do entire operation on client
	_SERVER_CALL(BusImport->ImportContactCustomData(status, strFieldName, BUS_CM_CONTACT, lstData))
	if(!status.IsOK()){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Error", status.getErrorText());
		return;
	}
	QApplication::restoreOverrideCursor();

	//filter result list to keep only rows having an error
	//STATUS_CODE: 0-ok, 1-matching contact not found, 2-matching custom field not found, 3 - wrong data type, 4-other error-> check STATUS_TEXT
	lstData.find("STATUS_CODE", 0);
	lstData.deleteSelectedRows();

	//TOFIX? show error list, or log exact rows having an error
	int nErrCnt = lstData.getRowCount();
	if(nErrCnt > 0)
		QMessageBox::information(this, "Error", QString(tr("Failed to import %1 out of %2 rows!")).arg(nErrCnt).arg(nCnt-1));
	else
		QMessageBox::information(this, "Information", tr("Done!"));
}

void sokrates_spc::OnMenu_ImportGroupAssignments()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.txt";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Choose file"),
		strStartDir,
		strFilter);
	if (!strFile.isEmpty())
	{
		QSize size(300,100);
		QProgressDialog progress(tr("Loading Import File..."),tr("Cancel"),0,0,this);
		progress.setWindowTitle(tr("Operation In Progress"));
		progress.setMinimumSize(size);
		progress.setWindowModality(Qt::WindowModal);
		progress.setMinimumDuration(1200);//1.2s before pops up

		//load file
		DbRecordSet data;
		Status status;
		CsvImportFile import;

		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

		QStringList lstFormatPlain;
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Group Assignments - Subformat: default";
		QStringList lstFormatUtf8;
		lstFormatUtf8 << "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Group Assignments - Subformat: default";
		import.Load(status, strFile, data, lstFormatPlain, lstFormatUtf8, false, CsvProgress, (unsigned long)&progress);
		if(!status.IsOK()){
			QApplication::restoreOverrideCursor();
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}
		int nFieldsCnt = data.getColumnCount();
		Q_ASSERT(nFieldsCnt == 9);
		//data.Dump();

		//merge lists
		DbRecordSet lstWork;
		lstWork.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_GROUP_IMPORT));

		//merge results with destination list
		int nCnt = data.getRowCount();
		for(int i=0; i<nCnt; i++)
		{
			lstWork.addRow();
			int nRow = i;
			int nCol = 0;

			lstWork.setData(nRow, "BCNT_FIRSTNAME",				data.getDataRef(i, nCol++).toString());
			lstWork.setData(nRow, "BCNT_LASTNAME",				data.getDataRef(i, nCol++).toString());
			lstWork.setData(nRow, "BCNT_ORGANIZATIONNAME",		data.getDataRef(i, nCol++).toString());
			lstWork.setData(nRow, "BGIT_CODE",		            data.getDataRef(i, nCol++).toString());
			lstWork.setData(nRow, "BGIT_EXTERNAL_CATEGORY",		data.getDataRef(i, nCol++).toString());
			lstWork.setData(nRow, "MODE",						data.getDataRef(i, nCol++).toInt());
			lstWork.setData(nRow, "UPDATE_MODE",				data.getDataRef(i, nCol++).toInt());
			lstWork.setData(nRow, "BGCN_CMCA_VALID_FROM",		DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			lstWork.setData(nRow, "BGCN_CMCA_VALID_TO",			DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));

			//update progress
			progress.setValue(1);
			qApp->processEvents();
			//if (progress.wasCanceled())
			//	break;
		}

		//lstWork.Dump();

		_SERVER_CALL(BusContact->WriteGroupAssignments(status,lstWork))
		if(!status.IsOK()){
			QApplication::restoreOverrideCursor();
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}
		int nErrorRows = lstWork.getRowCount();
		if(nErrorRows > 0){
			QApplication::restoreOverrideCursor();
			QMessageBox::information(this, "Error", QString(tr("Failed to import %1 items!")).arg(nErrorRows));
			return;
		}
		QApplication::restoreOverrideCursor();
	}
}

void sokrates_spc::OnMenu_CalendarViews()
{
	g_objFuiManager.OpenFUI(MENU_CALENDAR_VIEWS);
}
