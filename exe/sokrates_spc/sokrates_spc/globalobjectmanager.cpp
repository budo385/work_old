#include "globalobjectmanager.h"
#include "bus_client/bus_client/client_global_objects_create.h"
#include "fui_collection/fui_collection/fui_global_objects_create.h"

#include "os_specific/os_specific/tapi/TapiLine.h"
#include "common/common/datahelper.h"
#include "gui_core/gui_core/thememanager.h"
#include "fui_collection/fui_collection/dlg_rfparser.h"

#include "os_specific/os_specific/mapimanager.h"
#include "os_specific/os_specific/mailmanager.h"
#include "bus_client/bus_client/documenthelper.h"
#include "fui_collection/fui_collection/startupwizard.h"
#include "gui_core/gui_core/gui_helper.h"
#include "os_specific/os_specific/skype/skype.h"
#include "os_specific/os_specific/skypemanager.h"
#include <QDesktopWidget>
#include "fui_collection/fui_collection/fui_contacts.h"
#include "fui_collection/fui_collection/fui_projects.h"
#include "os_specific/os_specific/stylemanager.h"
#include "bus_core/bus_core/countries.h"
#include "common/common/threadid.h"

#ifdef _WIN32
 #include "os_specific/os_specific/skype/skypeparser.h"
 extern SkypeParser	g_objParser;
#endif

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

SkypeFuiHandler s_handler;




GlobalObjectManager::GlobalObjectManager(QObject *parent)
: QObject(parent),m_bProcessingFatalError(false),m_AppClosingInProcess(false), m_pwSidebar(NULL), m_pwPro(NULL)
{
	InitGlobalHandlers();
#ifdef _WIN32
	//m_pDropOutlookMimeHandler= new WindowsMimeOutlook; //bring up outlook handler;
	CTapiLine::InitTAPI();
#endif
	StyleManager::DetermineDefaultStyle(); //save def. style
	connect(this,SIGNAL(ShowFatalErrorMsg(QString)),this,SLOT(OnFatalErrorMsg(QString)));
}

GlobalObjectManager::~GlobalObjectManager()
{

}

void GlobalObjectManager::InitGlobalHandlers()
{
#ifdef _WIN32
	g_objParser.SetSkypeCallHandler(&s_handler);
#endif
	ApplicationLogger::setApplicationLogger(&g_Logger);
	g_ClientCache.SetGlobalMsgDispatcher(&g_ChangeManager,ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED,ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED);
}



void GlobalObjectManager::Initialize(sokrates_spc *pwPro,Sokrates_SideBar *pwSidebar)
{
	m_pwPro=pwPro;
	m_pwSidebar=pwSidebar;
	connect(g_pClientManager, SIGNAL(ClientLogged()), this, SLOT(OnClientLogged()));
	connect(g_pClientManager, SIGNAL(ClientLogout()), this, SLOT(OnClientLogout()));
	connect(g_pClientManager, SIGNAL(CommunicationFatalError(int)),this,SLOT(OnCommunicationFatalError(int)));
	connect(g_pClientManager, SIGNAL(ClientBeforeLogout()),this,SLOT(OnProcessBeforeLogout()));
	m_pwPro->Initialize();
	m_pwSidebar->Initialize();
}

void GlobalObjectManager::ChangeView(int nViewID)
{
	if (nViewID==ThemeManager::GetViewMode())
		return;
	ThemeManager::SetViewMode(nViewID);

	int nScreenPro = QApplication::desktop()->screenNumber(m_pwPro);
	int nScreenSideBar = QApplication::desktop()->screenNumber(m_pwSidebar);

	if (nViewID==ThemeManager::VIEW_SIDEBAR)
	{
		if (nScreenSideBar!=nScreenPro)
		{
			QRect rectPro=QApplication::desktop()->screenGeometry(nScreenPro);
			QRect rectSide=QApplication::desktop()->screenGeometry(nScreenSideBar);
			int nX_Pro = 0;
			if (nScreenSideBar<nScreenPro)
				nX_Pro = m_pwSidebar->geometry().left()+rectSide.width()+100;
			else
				nX_Pro = m_pwSidebar->geometry().left()-rectPro.width()-100;
			m_pwSidebar->move(nX_Pro,m_pwPro->geometry().top());
		}

		m_pwPro->hide();
		m_pwSidebar->OnViewChanged(); //auto -login
	}
	else
	{
		if (nScreenSideBar!=nScreenPro)
		{
			QRect rectPro=QApplication::desktop()->screenGeometry(nScreenPro);
			QRect rectSide=QApplication::desktop()->screenGeometry(nScreenSideBar);
			int nX_Pro = 0;
			if (nScreenPro<nScreenSideBar)
				nX_Pro = m_pwPro->geometry().left()+rectPro.width()+100;
			else
				nX_Pro = m_pwPro->geometry().left()-rectSide.width()-100;
			m_pwPro->move(nX_Pro,m_pwPro->geometry().top());
		}

		m_pwSidebar->hide();
		m_pwPro->OnViewChanged();  //auto - login
	}
}

void GlobalObjectManager::OnClientLogged()
{
	m_bProcessingFatalError=false;

	//set country/ predial:
	FormatPhone::SetDefaults(Countries::GetPredialFromISO(g_pSettings->GetPersonSetting(CURRENT_COUNTRY_ISO_CODE).toString()),g_pSettings->GetPersonSetting(CURRENT_LOCATION_AREA_CODE).toString());
	//set country lang:
	int nLang=g_pSettings->GetPersonSetting(DEFAULT_LANGUAGE).toInt();
	Countries::SetLanguage(nLang);
	//load window properties:
	QByteArray byteVal=g_pSettings->GetPersonSetting(APPEAR_WINDOW_PROPERTIES).toByteArray();
	GUI_Helper::ReadWindowProperties(byteVal);
	//load skin:
	int nSkin=g_pSettings->GetPersonSetting(SKIN).toInt();
	StyleManager::SetStyle(nSkin);
	//Theme:
	int nThemeID=g_pSettings->GetPersonSetting(APPEAR_THEME).toInt();
	ThemeManager::SetCurrentThemeID(nThemeID);
	g_pClientManager->GetIniFile()->m_nLastTheme=nThemeID;
	g_DownloadManager->OnThemeChanged();


	//View:
	int nViewID=g_pSettings->GetPersonSetting(APPEAR_VIEW_MODE).toInt();
	ThemeManager::SetViewMode(nViewID);

	CommunicationManager::BatchReadAllCEMenuData(); //read all contact menu data (on right)

	//StartUp Wizard:
	Status err;
	if (g_pClientManager->IsStartUpWizard()) //if wizard then go:
		if(ProcessOnStartUpWizard(err)) 
		{
			//_CHK_ERR_NO_RET(err);
			//OnMenu_OnLogout();
			//return;
		}

	//Portable handlers:
	if( DataHelper::IsPortableVersion() && g_pClientManager->GetIniFile()->m_bAskPortableSkypeInstallation)
	{
		int nResult = QMessageBox::question(NULL, "", tr("Do you want to copy your actual Skype installation to your portable memory device to have it available everywhere?"), tr("Yes"),tr("No"));
		if (nResult==0)
		{
			CopyPortableSkype();
		}
		else
			g_pClientManager->GetIniFile()->m_bAskPortableSkypeInstallation = 0;
	}


	if( DataHelper::IsPortableVersion() && g_pClientManager->GetIniFile()->m_bAskForPortableAppsInstallation)
	{
		int nResult = QMessageBox::question(NULL, "", tr("Do your want to register your portable applications into SOKRATES(R) Communicator automatically?"), tr("Yes"),tr("No"));
		if (nResult==0)
		{
			Dlg_RFParser Dlg;
			Status err;
			if(Dlg.Initialize(err))
			{
				_CHK_ERR(err);
				if(Dlg.exec())
				{
					QString strPath;
					DbRecordSet lstApps,lstTemplates,lstInterfaces;
					Dlg.GetResult(lstApps,lstTemplates,lstInterfaces,strPath);
					QFileInfo filek(strPath);
					DocumentHelper::RegisterApplicationsAndTemplatesFromRFFile(lstApps,lstTemplates,lstInterfaces,filek.absolutePath());
				}

			}
		}
		g_pClientManager->GetIniFile()->m_bAskPortableSkypeInstallation = 0;
	}

	StartupWizard::StartImportWizard();

	//FIX: issue #2012 delay sync start by 120 sec
	//m_EmailSubscribe.StartUserSession();
	QTimer::singleShot(120000, this, SLOT(OnClientLogged_EmailAutoImport()));

	if (nViewID==ThemeManager::VIEW_SIDEBAR)
	{
		m_pwSidebar->OnViewChanged(); //auto -login
		m_pwPro->hide();
	}
	else
	{
		m_pwPro->OnViewChanged();  //auto - login
		m_pwSidebar->hide();
	}

	OpenStartupAvatars();

	//issue 1518: keep focus on main window-> not on avatars:
	if (nViewID==ThemeManager::VIEW_SIDEBAR)
		QApplication::setActiveWindow(m_pwSidebar); 	
	else
		QApplication::setActiveWindow(m_pwPro); 	

}
void GlobalObjectManager::OnClientLogout()
{
	m_EmailSubscribe.EndUserSession();
	m_pwPro->OnClientLogout();
	m_pwSidebar->OnClientLogout();
}
void GlobalObjectManager::OnProcessBeforeLogout()
{
	m_EmailSubscribe.EndUserSession();
	g_CommManager.EndUserSession();	

	QByteArray byteData;
	GUI_Helper::SaveWindowProperties(byteData); //save window data (assume that are all closed now)
	g_pSettings->SetPersonSetting(APPEAR_WINDOW_PROPERTIES,byteData);

	m_pwPro->OnProcessBeforeLogout();
	m_pwSidebar->OnProcessBeforeLogout();
};


void GlobalObjectManager::OnApplicationClose(QWidget *pSourceWnd)
{
	if (m_AppClosingInProcess)
		return;
	m_AppClosingInProcess=true;

	//delete possible junk pictures
	QDir dir(QDir::tempPath());
	QStringList filters;
	filters << "SokratesImage*.*";
	QStringList lstResult = dir.entryList(filters, QDir::Files|QDir::NoDotAndDotDot);
	int nCount = lstResult.size();
	for(int i=0; i<nCount; i++){
		QFile::remove(lstResult[i]);
	}

#ifdef _WIN32
	CTapiLine::ShutdownTAPI();
#endif
	//qDebug()<<"Stopping Outlook Thread";
	m_EmailSubscribe.StopAll();
	//qDebug()<<"Outlook Thread stopped";
	SkypeManager::ReleaseInstance();
#ifdef _WIN32
	MapiManager::UnIntiliazeMapiDll();
	//if (m_pDropOutlookMimeHandler) delete m_pDropOutlookMimeHandler;
#endif

	if (dynamic_cast<sokrates_spc*>(pSourceWnd)!=NULL)
		m_pwSidebar->close();
	else
		m_pwPro->close();

#ifdef _WIN32
	MapiManager::UninitGlobalMapi();	//version used by main thread
#endif

}


//close all FUI's: no network traffic is allowed:
void GlobalObjectManager::OnCommunicationFatalError(int nErrorCode)
{
	if (m_bProcessingFatalError) return;   //prevent to loop itself!!!-> just precaution, QTimer::singleshot fires two times sometimes...
	m_bProcessingFatalError=true;

	qDebug()<<"Fatal Error 1 in OnCommunicationFatalError, thread: " <<ThreadIdentificator::GetCurrentThreadID();

	m_EmailSubscribe.EndUserSession(true); //in case of connection failure, terminate threads at once
	//m_EmailSubscribe.StopAll();
	SkypeManager::ReleaseInstance();
#ifdef _WIN32
	MapiManager::UnIntiliazeMapiDll();
	MapiManager::UninitGlobalMapi();	//version used by main thread
#endif
	g_CommManager.ClearData();

	//abort and hide:
	g_DownloadManager->AbortAllOperations();
	g_DownloadManager->OnHideDlg();

	//filter out any events from queue:
	QCoreApplication::instance()->processEvents();

	//CloseAllModalWindows();					
	g_objFuiManager.CloseAllFUIWindows(true);	

	//if some1 uses hourglass when accessing BO, restore to normal:
	QApplication::changeOverrideCursor(Qt::ArrowCursor);

	Status err;
	//kill BO SET-> if any1 left in event queue this will crash application, in all tests, this wasn't happen yet!
	g_pClientManager->StopAfterFatalError(err); //clear all session data cached
	if(!err.IsOK())	
	{
		//show error message
		//QMessageBox::warning(NULL, tr("Error Message"), err.getErrorText());
	}

	m_pwPro->OnCommunicationFatalError(nErrorCode);
	m_pwSidebar->OnCommunicationFatalError(nErrorCode);

	//pop up massage (important to be last: to enable all threads/sync to stop properly)
	QString strErrorTxt;
	StatusCodeSet::getErrorDetails(nErrorCode,strErrorTxt);
	if (strErrorTxt.isEmpty() || strErrorTxt==tr("Unknown Error!"))
	{
		strErrorTxt=tr("Connection to server is lost!");
	}
	strErrorTxt=tr("Session is terminated. Connection to server has been lost. Reason: ")+strErrorTxt;

	qDebug()<<"Fatal Error 2 in OnCommunicationFatalError, thread: " <<ThreadIdentificator::GetCurrentThreadID();

	emit ShowFatalErrorMsg(strErrorTxt);
}

void GlobalObjectManager::OnFatalErrorMsg(QString strText)
{
	QMessageBox::critical(NULL, tr("Error Message"), strText);
	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,strText);
}


//TRUE if to exit login procedure
bool GlobalObjectManager::ProcessOnStartUpWizard(Status &pStatus)
{
	g_pClientManager->StopProgressDialog();

	Status err;
	bool bRestart=StartupWizard::StartWizard(); //start wizard
	StartupWizard::ResetOnStartUpFlag();		//reset flags

	//load persons & org:
	MainEntitySelectionController pers;
	pers.Initialize(ENTITY_BUS_PERSON);
	pers.ReloadData(true);

	MainEntitySelectionController org;
	org.Initialize(ENTITY_BUS_ORGANIZATION);
	org.ReloadData(true);

	g_pClientManager->StartProgressDialog(tr("Login in Progress..."));

	return false;
}



void GlobalObjectManager::OpenStartupAvatars()
{
	//g_pSettings->SetPersonSetting(AVATAR_STARTUP_LIST, "");
	int nHeight = QApplication::desktop()->rect().height();
	int nWidth  = QApplication::desktop()->rect().width();
	int nDisplacement = 0;

	int nPersonID=g_pClientManager->GetPersonID();

	QString strAvatars = g_pSettings->GetPersonSetting(AVATAR_STARTUP_LIST).toString();
	//this is to reduce "losing last avatar" effects, caused by strAvatarsNew code bug
	if(!strAvatars.isEmpty() && !strAvatars.endsWith(";")) 
		strAvatars += ";";	//MR: list must be terminated

	QString strAvatarsNew;
	int nPos = strAvatars.indexOf(";");
	while(nPos >= 0)
	{
		QString strSegment = strAvatars.mid(0, nPos);
		if(!strSegment.isEmpty())
		{
			//decode avatar data "avatar_type,record_id:left,top"
			int nPos1 = strSegment.indexOf(",");
			if(nPos1 > 0)
			{
				int nEntityType=-1;
				int nType  = strSegment.mid(0, nPos1).toInt();
				int nLeft  = 0;
				int nTop   = 0;
				int nRecID = -1;
				int nPos2  = strSegment.indexOf(":", nPos1+1);
				if(nPos2 >= 0){
					nRecID = strSegment.mid(nPos1+1, nPos2-nPos1-1).toInt();
					int nPos3 = strSegment.indexOf(",", nPos2+1);
					if(nPos3 >= 0){
						nLeft  = strSegment.mid(nPos2+1, nPos3-nPos2-1).toInt();
						nTop   = strSegment.mid(nPos3+1).toInt();
					}
				}

				//fix pos if needed
				if(nDisplacement >= nWidth || nDisplacement >= nHeight)
					nDisplacement = 0;
				if(nLeft >= nWidth || nTop >= nHeight)
				{
					nLeft = nWidth/2 + nDisplacement;
					nTop  = nHeight/2 + nDisplacement;
					nDisplacement += 20;
				}

				int nFuiType = -1;
				if(AVATAR_PROJECT == nType)
				{
					nFuiType = MENU_PROJECTS;
					nEntityType=ENTITY_BUS_PROJECT;
				}
				else if(AVATAR_CONTACT == nType)
				{
					nFuiType = MENU_CONTACTS;
					nEntityType=ENTITY_BUS_CONTACT;
				}
				else if(AVATAR_TASK_NOTE == nType)
				{
					nFuiType = MENU_DM_DOCUMENTS;
					nEntityType=ENTITY_BUS_DM_DOCUMENTS;
				}
				else if(AVATAR_TASK_EMAIL == nType)
				{
					nFuiType = MENU_EMAIL;
					nEntityType=ENTITY_BUS_EMAILS;
				}
				else if(AVATAR_TASK_CALL == nType)
				{
					nFuiType = MENU_COMM_VOICE_CENTER;
					nEntityType=ENTITY_BUS_VOICECALLS;
				}
								
				if(nFuiType >= 0)
				{
					//B.T. 04.03.2013: Check if record exists, if deleted then skip avatar open and update avatar personal settings:
					bool bRecordExists=false;
					Status err;
					_SERVER_CALL(BusCommunication->CheckIfRecordExists(err,nEntityType,nRecID,bRecordExists));
					_CHK_ERR(err);

					if (bRecordExists)
					{
						//open FUI
						int nGID = g_objFuiManager.OpenFUI(nFuiType,true,false,FuiBase::MODE_READ,nRecID, true, true);

						//minimize immediately to make it an avatar
						QWidget *pFui = g_objFuiManager.GetFUIWidget(nGID);
						if(pFui){
							if(AVATAR_PROJECT == nType)
								((FUI_Projects *)pFui)->m_bHideOnMinimize = false; //we will hide ti below
							else if(AVATAR_CONTACT == nType)
								((FUI_Contacts *)pFui)->m_bHideOnMinimize = false; //we will hide ti below
							pFui->showMinimized();
							QTimer::singleShot(0,pFui,SLOT(hide()));//B.T. delay hide
							if(((FuiBase *)pFui)->m_pWndAvatar){
								((FuiBase *)pFui)->m_pWndAvatar->move(nLeft, nTop);
								((FuiBase *)pFui)->m_pWndAvatar->activateWindow();
							}
						}

						strAvatarsNew=strAvatarsNew+";"+strSegment;
					}
				}
			}
		}

		//keep going
		strAvatars.remove(0, nPos+1);
		nPos = strAvatars.indexOf(";");
	}

	//save new avatar settings:
	if(!strAvatarsNew.isEmpty()) strAvatarsNew += ";";	//MR: list must be terminated
	g_pSettings->SetPersonSetting(AVATAR_STARTUP_LIST,strAvatarsNew);
}


void GlobalObjectManager::CopyPortableSkype()
{
	if(SkypeLine::IsSkypeInstalled())
	{
		QString strAppPath = QCoreApplication::applicationDirPath();
		QDir dir(strAppPath);
		dir.mkdir("Skype");

		//STEP 1: copy Skype .exe
		QString strSkypePath = SkypeLine::GetSkypeAppPath();
		QFile file(strSkypePath);
		file.copy(strAppPath + "/Skype/Skype.exe");

		//STEP 2: copy Skype user accounts
		dir.cd("Skype");
		dir.mkdir("Userdata");
		DataHelper::CopyFolderContent(DataHelper::GetAppDataDir()+"/Skype", strAppPath + "/Skype/Userdata", true);
	}
	else
		QMessageBox::information(NULL, "", QObject::tr("No Skype installation found!"));
}


void GlobalObjectManager::UpdatePortableSkype()
{
	if(SkypeLine::IsSkypeInstalled())
	{
		//STEP 1: copy Skype .exe
		QString strSkypePath = SkypeLine::GetSkypeAppPath();
		QString strAppPath = QCoreApplication::applicationDirPath();

		QDir dir(strAppPath);
		dir.mkdir("Skype");

		QFile file(strSkypePath);
		file.copy(strAppPath + "/Skype/Skype.exe");
	}
	else
		QMessageBox::information(NULL, "", QObject::tr("No Skype installation found!"));
}

void GlobalObjectManager::OnClientLogged_EmailAutoImport()
{
	m_EmailSubscribe.StartUserSession();
}





int SkypeFuiHandler::OpenSkypeIncomingCallHandler()
{
	return g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false);
}

SkypeInterface* SkypeFuiHandler::GetSkypeIncomingCallHandler(int nSkypeIncomingCallHandlerID)
{
	return dynamic_cast<SkypeInterface *>(g_objFuiManager.GetFUIWidget(nSkypeIncomingCallHandlerID));
}




