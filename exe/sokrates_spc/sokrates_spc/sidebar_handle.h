#ifndef SidebarHandle_H
#define SidebarHandle_H

#include <QWidget>

class SidebarHandle : public QWidget
{
    Q_OBJECT

public:
    SidebarHandle(QWidget *pWnd = 0);
    QSize sizeHint() const;

	void ShowHandle();
	void HideHandle(){	hide(); };
	void UpdatePosition();
	void FlipState();
	void UpdateHandleBitmap();

public slots:
	//void OnScreenGeometryChanged(int);

protected:
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent * event);
    void paintEvent(QPaintEvent *event);
	void timerEvent ( QTimerEvent * event ) ;

private:

	void Slide_WndControlled();
    QPixmap	m_pixCurrent;
	bool	m_bImageLeftSide;
	bool	m_bWindowVisible;
	QWidget *m_pWndControlled;
	int		m_nTimer;
	int		m_nStep;
	//QRect m_desktopRec;
	QPoint  m_WinPosition;
	int		m_nStartX;
	bool m_bMoveInProgress;
	QPoint m_dragPosition;

};

#endif
