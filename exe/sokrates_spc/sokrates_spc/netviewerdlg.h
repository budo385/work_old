#ifndef NETVIEWERDLG_H
#define NETVIEWERDLG_H

#include <QtWidgets/QDialog>
#include "ui_netviewerdlg.h"

class NetviewerDlg : public QDialog
{
	Q_OBJECT

public:
	NetviewerDlg(QWidget *parent = 0);
	~NetviewerDlg();

	QString GetSessionNum();

private slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();

private:
	Ui::NetviewerDlgClass ui;
};

#endif // NETVIEWERDLG_H
