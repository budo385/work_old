#ifndef GLOBALOBJECTMANAGER_H
#define GLOBALOBJECTMANAGER_H

#include "os_specific/os_specific/skype/skypeparser.h"
#include "sokrates_sidebar.h"
#include "sokrates_spc.h"
#include "emailimportthread.h"


//main purpose: init global handler and syncronize work between sidebar & sokrats_spc
class GlobalObjectManager : public QObject
{
	Q_OBJECT
public:
	GlobalObjectManager(QObject *parent);
	~GlobalObjectManager();

	static void InitGlobalHandlers();
	void Initialize(sokrates_spc *pwPro,Sokrates_SideBar *pwSidebar);
	void ChangeView(int nViewID);
	void CopyPortableSkype();
	void UpdatePortableSkype();

public slots:
	void OnClientLogged();
	void OnClientLogout();
	void OnProcessBeforeLogout();
	void OnCommunicationFatalError(int nErrorCode);
	void OnApplicationClose(QWidget *pSourceWnd);
	void OnClientLogged_EmailAutoImport();
	void OnFatalErrorMsg(QString strText);

signals:
	void ShowFatalErrorMsg(QString);

public:
	sokrates_spc					*m_pwPro;
	Sokrates_SideBar				*m_pwSidebar;

private:
	void OpenStartupAvatars();

	bool ProcessOnStartUpWizard(Status &pStatus);

	EmailSynchronizationManager		m_EmailSubscribe;
	bool							m_bProcessingFatalError;
	bool							m_AppClosingInProcess;
	
};


class SkypeFuiHandler : public SkypeCallHanlder
{
public:
	int OpenSkypeIncomingCallHandler();
	SkypeInterface * GetSkypeIncomingCallHandler(int nSkypeIncomingCallHandlerID);
};


#endif // GLOBALOBJECTMANAGER_H
