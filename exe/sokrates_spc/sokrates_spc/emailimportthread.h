#ifndef EMAILIMPORTTHREAD_H
#define EMAILIMPORTTHREAD_H

#include "os_specific/os_specific/mapimanager.h"
#include "common/common/threadspawner.h"
#include <QTimer>
#include "common/common/observer_ptrn.h"
#include "common/common/dbrecordset.h"
#include "common/common/status.h"

//Separate Thread interface
class EmailImportThread : public ThreadSpawner
{
	Q_OBJECT

public:
	EmailImportThread(bool bThunderbirdMode = false);

protected:

	bool m_bThunderbirdMode;
	ThreadSpawnedObject* CreateThreadSpawnedObject();
};


//Separate Thread object
class EmailImportAction : public ThreadSpawnedObject
{
	Q_OBJECT

	bool m_bThunderbirdMode;
	int m_nIniLstRow;

public:
	EmailImportAction(bool bThunderbirdMode);
	~EmailImportAction();

	void ScheduleAction();
	bool IsThreadRunning();
	void SetTerminateFlag();
	bool IsTerminateFlagSet();
signals:
	void StartAction();

public slots:
	void DoAction();
	void DoActionOutlook();
	void DoActionThunderbird();
	void DoMapiCleanup();
	void OnKillBoOperation();

private:

	void WriteEmailAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments);

	QReadWriteLock m_Lck_m_bThreadRunning;
	bool m_bThreadRunning;
	bool m_bTerminateThread; //thread must check this flag in every loop and if set, exit thread gently, thus avoiding terminate() command which can break MAPI/Outlook

#ifdef _WIN32
	CMAPIEx m_mapi;
#endif
	bool m_bMapiLoggedOK;
};


//Email synchronization manager:use as global or member variable
class EmailSynchronizationManager : public QObject, public ObsrPtrn_Observer
{
	Q_OBJECT
public:
	EmailSynchronizationManager();
	~EmailSynchronizationManager();
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void StartUserSession();
	void EndUserSession(bool bTerminateAndDontAskUser=false);
	void StopAll();
	
	static QDateTime GetOutlookLastScan();
	static QDateTime GetThunderBirdLastScan();

signals:
	void KillBoOperationInWorkingThread();

private slots:
	void OnOutlookCheckTimer();
	void OnThunderbirdCheckTimer();

private:
	bool IsThreadRunning();
	bool EnableOutlookSync();
	bool EnableThunderBirdSync();
	QTimer m_EmailSubscriptionTimerOutlook;
	QTimer m_EmailSubscriptionTimerThunderbird;

	EmailImportThread *m_pEmailImportThreadOutlook;
	EmailImportThread *m_pEmailImportThreadThunderbird;
};

#endif // EMAILIMPORTTHREAD_H
