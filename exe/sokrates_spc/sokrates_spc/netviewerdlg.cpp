#include "netviewerdlg.h"
#include <QtWidgets/QMessageBox>

NetviewerDlg::NetviewerDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
}

NetviewerDlg::~NetviewerDlg()
{
}

QString NetviewerDlg::GetSessionNum()
{
	return ui.txtSessionNum->text();
}

void NetviewerDlg::on_btnOK_clicked()
{
	if(GetSessionNum().isEmpty())
	{
		QMessageBox::information(NULL, "", tr("Please fill the session number!"));
		return;
	}

	done(1);
}

void NetviewerDlg::on_btnCancel_clicked()
{
	done(0);
}

