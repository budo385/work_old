#ifdef _WIN32
 #include "os_specific/os_specific/mapi/mapiex.h"
#endif
#include "emailimportthread.h"
#include <QDateTime>

#include "fui_collection/fui_collection/fui_importemail.h"
#include "os_specific/os_specific/mapimanager.h"
#include "common/common/threadid.h"
#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#include "os_specific/os_specific/mailmanager.h"
#include "trans/trans/xmlutil.h"
#include "gui_core/gui_core/macros.h"
#include "gui_core/gui_core/timemessagebox.h"
#include "bus_core/bus_core/globalconstants.h"
#include "fui_collection/fui_collection/calendarhelper.h"
#include "common/common/datahelper.h"
#include "common/common/authenticator.h"
#include "bus_trans_client/bus_trans_client/userstoragehttpclient.h"
#include "bus_trans_client/bus_trans_client/businessservicemanager_thinclient.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

//#include "fui_collection/fui_collection/settings_outlook.h"
void FolderList2String(const QList<QStringList> &lstData, QString &strResult);
void String2FolderList(QString strData, QList<QStringList> &lstResult);

#define CRASH_TEST

//-----------thread-----------------
EmailImportThread::EmailImportThread(bool bThunderbirdMode)
{
	m_bThunderbirdMode = bThunderbirdMode;
}

ThreadSpawnedObject* EmailImportThread::CreateThreadSpawnedObject()
{
	return new EmailImportAction(m_bThunderbirdMode);
}

void EmailImportAction::ScheduleAction()
{
	if (!m_Lck_m_bThreadRunning.tryLockForRead(1000)) //try for 1sec, if no...wait for another loop
		return;

	if(!m_bThreadRunning){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Fire StartAction");
		emit StartAction();
	}
	m_Lck_m_bThreadRunning.unlock();
}

//-----------thread object-----------------
EmailImportAction::EmailImportAction(bool bThunderbirdMode)
:m_bThreadRunning(false),m_bTerminateThread(false)
{
	m_bMapiLoggedOK = false;
	//return;

	//find/prepare Ini row (per-connection settings)
	if(bThunderbirdMode)
		m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	else
		m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstOutlookSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	if(-1 == m_nIniLstRow){
		if(bThunderbirdMode)
		{
			g_pClientManager->GetIniFile()->m_lstThunderbirdSync.addRow();
			m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getRowCount()-1;
			g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_ConnectionName", g_pClientManager->GetConnectionName());
		}
		else{
			g_pClientManager->GetIniFile()->m_lstOutlookSync.addRow();
			m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstOutlookSync.getRowCount()-1;
			g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_ConnectionName", g_pClientManager->GetConnectionName());
		}
	}
	else{
		Q_ASSERT(!m_bMapiLoggedOK);

#ifdef _WIN32
		m_bMapiLoggedOK = true;
		if(!CMAPIEx::Init(false)){
			m_bMapiLoggedOK = false;
		}
		else if(!m_mapi.Login()){
			m_bMapiLoggedOK = false;
			CMAPIEx::Term();
		}
		if(!m_bMapiLoggedOK){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Sync Thread: Error logging into MAPI system!");
		}
		else
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Sync Thread: logged into MAPI system!");
#endif //#ifdef _WIN32
	}


	//QTimer::singleShot(0, this, SLOT(DoAction()));
	m_bThunderbirdMode = bThunderbirdMode;
	connect(this,SIGNAL(StartAction()),this,SLOT(DoAction()));
}

EmailImportAction::~EmailImportAction()
{
}

bool EmailImportAction::IsThreadRunning()
{
	if (!m_Lck_m_bThreadRunning.tryLockForRead(1000)) //try for 1sec, if no...wait for another loop
		return true;
	bool bRunning=m_bThreadRunning;
	m_Lck_m_bThreadRunning.unlock();
	return bRunning;
}

void EmailImportAction::SetTerminateFlag()
{
	m_Lck_m_bThreadRunning.lockForWrite();
	m_bTerminateThread=true;				//resource protected with same lock as for isrunnig
	m_Lck_m_bThreadRunning.unlock();
}

bool EmailImportAction::IsTerminateFlagSet()
{
	QReadLocker lock(&m_Lck_m_bThreadRunning);
	return m_bTerminateThread;
}

void EmailImportAction::DoMapiCleanup()
{
#ifdef _WIN32
	if(m_bMapiLoggedOK){
		m_mapi.Logout();
		CMAPIEx::Term();

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Sync Thread: logged off from MAPI system!");
	}
	else
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Sync Thread: no need to log off from MAPI.");
	m_bMapiLoggedOK = false;
#endif
}

void EmailImportAction::DoAction()
{
	QWriteLocker lock(&m_Lck_m_bThreadRunning); //lock flag access
	
	if (m_bTerminateThread)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Mail Import/Export process terminated.");
		lock.unlock();
		return;
	}
	
	m_bThreadRunning=true; //disable access to thread if running
	lock.unlock();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start sync thread");

	Status err;
	g_pClientManager->StartThreadConnection(err);
	qDebug()<<"after start conn";
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,tr("Mail Import/Export thread failed to start. Reason: ")+err.getErrorText());
		lock.relock();
		m_bThreadRunning=false; //enable access to thread if running
		return;
	}

	if(m_bThunderbirdMode)
		DoActionThunderbird();
	else
		DoActionOutlook();

	g_pClientManager->CloseThreadConnection(err);
	if (!err.IsOK())
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,tr("Mail Import/Export thread failed to finish. Reason: ")+err.getErrorText());

	lock.relock();
	m_bThreadRunning=false; //enable access to thread if running
}

void EmailImportAction::OnKillBoOperation()
{
	g_pBoSet->AbortDataTransfer();
}

void EmailImportAction::DoActionOutlook()
{
#ifdef _WIN32
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start DoActionOutlook");

	if(!m_bMapiLoggedOK){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start DoActionOutlook failed (MAPI not logged in)");
		return;
	}
	if(IsTerminateFlagSet()){
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Outlook subscription process terminated.");
		return;
	}

	qDebug()<<"Outlook Sync thread:"<<ThreadIdentificator::GetCurrentThreadID();
	if(!g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool()){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start DoActionOutlook done (sync not enabled)");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook Sync enabled, proceed");

	int nPeriod = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt();
	if(nPeriod < 1){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Start DoActionOutlook done (sync period < 1), period=%1").arg(nPeriod));
		return;
	}

	bool bOnlyKnownEmails = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_OnlyKnownEmails").toBool();
	bool bOnlyOutgoingEmails = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_OnlyOutgoingEmails").toBool();

	QString strMessage=tr("Start subscription Outlook process");
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);

	//check if we are allowed to scan the default Outlook profile
	QStringList lstPersEmails;
	MapiManager::GetPersonalEmails(lstPersEmails, &m_mapi);
	QString strOurEmail = (lstPersEmails.size() > 0)? lstPersEmails.at(0) : "";
	if(strOurEmail.isEmpty()){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, "Start DoActionOutlook done (failed to fetch user's email)");
		return;
	}
	if(IsTerminateFlagSet()){
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Outlook subscription process terminated.");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Step1: GetPersonalEmails Done");

	strOurEmail  = ";" + strOurEmail;
	strOurEmail += ";";
	QString strAllowedEmails = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_AllowedProfiles").toString();
	if(strAllowedEmails.indexOf(strOurEmail) < 0){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, "Start DoActionOutlook done (email not in the list of allowed profiles)");
		return;
	}

	//OK, we may scan the Outlook profile
	QList<QStringList> lstFolders;

	//load outlook folder selection setting
	QString strData = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_ScanFolders").toString();
	String2FolderList(strData, lstFolders);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Step2: Start GetEmailEntries, GetLastScan time");

	QList<QByteArray> lstEntryIDs;
	QDateTime dtLastEmail; 
	QDateTime dtLastOutlookScan = EmailSynchronizationManager::GetOutlookLastScan();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Step 2.1: Call GetEmailEntries");

	MapiManager::GetEmailEntries(lstFolders,lstEntryIDs,dtLastEmail, dtLastOutlookScan, &m_mapi);
	int nTotalCount = lstEntryIDs.count();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Step2: End GetEmailEntries: %1").arg(nTotalCount));
	
	if(nTotalCount > 0)
	{
		const int nMaxAllowedAttSize = 5000000; //cca 5MB
		const int nMaxChunkCount = 30;
		int nChunkStartPos = 0;

		while(1)
		{
			if(IsTerminateFlagSet()){
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Outlook subscription process terminated.");
				return;
			}

			//chunk can be smaller than nMaxChunkCount (end chunk)
			int nChunkSize = qMin(nMaxChunkCount, nTotalCount-nChunkStartPos);
			if(nChunkSize <= 0)
				break;

			//prepare chunk
			QList<QByteArray> lstEntryIDsChunk;
			for(int i=0; i<nChunkSize; i++)
				lstEntryIDsChunk.append(lstEntryIDs[nChunkStartPos + i]);

			if (lstEntryIDsChunk.size()==0) //BT: just another check
				break;
			
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Chunked import loop, current pos: %1, total %2").arg(nChunkStartPos).arg(nTotalCount));

			Status status; 
			DbRecordSet lstData;
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Step3: Start FUI_ImportEmail::ReadEmails - chunk (%1)").arg(nChunkSize));
			FUI_ImportEmail::ReadEmails(status, lstEntryIDsChunk, lstData, false, -1, NULL, bOnlyKnownEmails, bOnlyOutgoingEmails, &m_mapi);
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Step3: End FUI_ImportEmail::ReadEmails ");
			_CHK_ERR_NO_MSG(status); //TOFIX what if only one fails

			//check if any of the mails has the big attachment
			DbRecordSet bigAttachment;
			int nBigAttChunkIdx = -1;
			int nTotalAttSize = 0;
			int nMax = lstData.getRowCount();
			for(int j=0; j<nMax; j++)
			{
				int nEmailAttSize = lstData.getDataRef(j, "ATTACHMENTS_SIZE").toInt();
				if(nEmailAttSize > nMaxAllowedAttSize || 
					(nEmailAttSize + nTotalAttSize) > nMaxAllowedAttSize)
				{
					//terminate chunk after this one
					lstData.getData(j, "ATTACHMENTS", bigAttachment);
					nBigAttChunkIdx = j;

					//clear attachment contents for this item, we'll write them separately
					lstData.setData(j, "ATTACHMENTS", DbRecordSet());
					break;
				}
				nTotalAttSize += nEmailAttSize;
			}

			qDebug()<<"BO object alive: "<<g_pBoSet->app;

			//increment for the next loop
			if(nBigAttChunkIdx >= 0){
				nChunkStartPos += nBigAttChunkIdx+1;	//we ended the chunk prematurely, total att size exceeded	
			}
			else
				nChunkStartPos += nChunkSize;			//increment for the next loop

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Step4: Start FUI_ImportEmail::FilterEmailsByContact data rowcount= %1").arg(lstData.getRowCount()));
			FUI_ImportEmail::FilterEmailsByContact(status, lstData, true);
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Step4: End FUI_ImportEmail::FilterEmailsByContact ");
			_CHK_ERR_NO_MSG(status); //TOFIX what if only one fails

			QString strMessage=tr("Outlook importing %1 emails").arg(lstData.getRowCount());
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);

			//TOFIX cleanup empty data rows
			int nSkippedOrReplacedCnt = 0; 

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Step5: Start FUI_ImportEmail::ImportEmails data rowcount= %1").arg(lstData.getRowCount()));

			//adapt nBigAttChunkIdx index to list filtering
			if(nBigAttChunkIdx >= 0)
			{
				if(lstData.isRowSelected(nBigAttChunkIdx)){
					nBigAttChunkIdx = -1;	//row will be deleted
				}
				else
				{
					int nSelectedBeforeCnt = 0;
					for(int k=0; k<nBigAttChunkIdx; k++){
						if(lstData.isRowSelected(k))
							nSelectedBeforeCnt ++;
					}
					nBigAttChunkIdx -= nSelectedBeforeCnt;	//account for the rows that will be deleted due to the filtering
				}
			}

			//delete filtered rows
			lstData.deleteSelectedRows();

			FUI_ImportEmail::ImportEmails(status, lstData, nSkippedOrReplacedCnt, false, true, true);
			_CHK_ERR_NO_MSG(status); //TOFIX what if only one fails

			if(nBigAttChunkIdx >= 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Step5: separately import attachment for email %1").arg(nBigAttChunkIdx+1));
				int nEmailID = lstData.getDataRef(nBigAttChunkIdx, "BEM_ID").toInt();
				//some emails may be skipped (already existing in the database, ID=0 (NULL))
				if(nEmailID > 0)
				{
					WriteEmailAttachments(status, nEmailID, bigAttachment);
					//_CHK_ERR_NO_RET(status); //TOFIX what if only one fails
				}
			}

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Step5: End FUI_ImportEmail::ImportEmails ");
			
			//now check if any of those mails were Appointment request replies
			int nMax1 = lstData.getRowCount();
			for(int j=0; j<nMax1; j++)
			{
				//test if this is an appointment reply

				//extract entry ID
				SBinary entryID;
				entryID.lpb = (LPBYTE)lstEntryIDsChunk[j].constData();
				entryID.cb	= lstEntryIDsChunk[j].size();
				
				CMAPIAppointment appointment;
				if(appointment.Open(&m_mapi, entryID))
				{
					QString strMessageClass;
					appointment.GetMessageClass(strMessageClass);
					//qDebug() << "Message Class:" << strMessageClass;

					//PARSE event:
					DbRecordSet set;
					set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS));
					set.addRow();
					if(strMessageClass.startsWith("IPM.Schedule.Meeting.Resp.Pos"))
						set.setData(0, "BCIV_STATUS", GlobalConstants::STATUS_CAL_INVITE_CONFIRMED);
					else if(strMessageClass.startsWith("IPM.Schedule.Meeting.Resp.Neg"))
						set.setData(0, "BCIV_STATUS", GlobalConstants::STATUS_CAL_INVITE_REJECTED);

					//extract contacts for this Appointment
					DbRecordSet lstAllRecipients;//I need all recipients in list + organizer store in 
					lstAllRecipients.addColumn(QVariant::Int,"BNMR_TABLE_KEY_ID_2");

					DbRecordSet lstContactLinks;
					lstData.getData(j, "CONTACTS", lstContactLinks);
					lstContactLinks.addColumn(QVariant::String,"BNRO_NAME");

					//get organizer and recipient email+id?
					if(strMessageClass.startsWith("IPM.Schedule.Meeting.Resp."))
					{
						int nRow=lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_FROM,true);
						if (nRow>=0)
						{
							set.setData(0,"RECIPIENT_CONTACT_ID",lstContactLinks.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
							lstContactLinks.setColValue("BNRO_NAME",tr("Participant"));
						}
						nRow=lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_TO,true);
						if (nRow>=0)
						{
							set.setData(0,"ORGANIZER_CONTACT_ID",lstContactLinks.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
							lstContactLinks.setData(nRow,"BNRO_NAME",tr("Organizer"));
						}
					}
					else
					{
						int nRow=lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_FROM,true);
						if (nRow>=0)
						{
							set.setData(0,"ORGANIZER_CONTACT_ID",lstContactLinks.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
							lstContactLinks.setData(nRow,"BNRO_NAME",tr("Organizer"));
						}
						nRow=lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_TO,true);
						if (nRow>=0)
						{
							set.setData(0,"RECIPIENT_CONTACT_ID",lstContactLinks.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
							lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_TO);
							lstContactLinks.setColValue("BNRO_NAME",tr("Participant"),true);
						}
					}


					//lstContactLinks.Dump();

					set.setData(0,"CONTACT_ASSIGN_LIST",lstContactLinks); //??? who is organizer....??
					

					QString strID=CalendarHelper::ParseSubjectID(lstData.getDataRef(j, "BEM_SUBJECT").toString());
					set.setData(0, "BCIV_SUBJECT_ID", strID);
					set.setData(0, "RECIPIENT_MAIL", lstData.getDataRef(j, "BEM_FROM").toString());
					QString strBody=lstData.getDataRef(j, "BEM_BODY").toString();
					set.setData(0, "BCIV_ANSWER_TEXT", lstData.getDataRef(j, "BEM_BODY").toString());

					//qDebug()<<strBody;

					QString strData;
					appointment.GetSubject(strData);
					set.setData(0, "BCEV_TITLE", strData);
					set.setData(0, "BCOL_SUBJECT", strData);

					appointment.GetLocation(strData);
					set.setData(0, "BCOL_LOCATION", strData);
					appointment.GetVCalendarUID(strData);
					set.setData(0, "BCIV_OUID", strData);
					
					appointment.GetStartDate(strData);
					QDateTime dateTime=QDateTime::fromString(strData,"MM/dd/yyyy hh:mm:ss AP");
					set.setData(0, "BCOL_FROM", dateTime);
					//qDebug()<<dateTime.toString("dd.MM.yyyy hh:mm:ss");
					
					appointment.GetEndDate(strData);
					dateTime=QDateTime::fromString(strData,"MM/dd/yyyy hh:mm:ss AP");
					//qDebug()<<dateTime.toString("dd.MM.yyyy hh:mm:ss");
					set.setData(0, "BCOL_TO", dateTime);

					//CalendarHelper::ParseCelandar(strBody,set);
					//set.Dump();

					if(strMessageClass.startsWith("IPM.Schedule.Meeting.Resp."))
					{
						//NOTE: being here proves the entry is an appointment response message:
						//IPM.Schedule.Meeting.Resp.Neg
						//IPM.Schedule.Meeting.Resp.Pos
						//IPM.Schedule.Meeting.Resp.Tent
											
						Status status;
						_SERVER_CALL(BusCalendar->WriteInviteReplyStatus(status, set));
						//FIX: this skips the important code below _CHK_ERR_NO_MSG(status);
					}
					else if (strMessageClass.startsWith("IPM.Schedule.Meeting.Request"))
					{
						Status status;
						_SERVER_CALL(BusCalendar->WriteInviteReplyStatus(status, set,1));

					}
					else if (strMessageClass.startsWith("IPM.Schedule.Meeting.Canceled"))
					{
						Status status;
						_SERVER_CALL(BusCalendar->WriteInviteReplyStatus(status, set,2));
					}
				}
			}
		}

		if(dtLastEmail.isValid()){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, "SyncThread Update LastScan time to: " + dtLastEmail.toString("dd.MM.yyyy hh:mm:ss"));
			g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_LastScan", dtLastEmail.toString("dd.MM.yyyy hh:mm:ss"));
		}
	}

	strMessage=tr("End subscription Outlook process");
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);
#endif //#ifdef _WIN32
}


void EmailImportAction::DoActionThunderbird()
{
	if(IsTerminateFlagSet())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Thunderbird subscription process terminated.");
		return;
	}

	qDebug()<<"ThunderBird Sync thread:"<<ThreadIdentificator::GetCurrentThreadID();
	if(!g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool()) //g_pSettings->GetPersonSetting(THUNDERBIRD_SETTINGS_SUBSCRIBED).toBool()
		return;
	int nPeriod = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt(); //g_pSettings->GetPersonSetting(THUNDERBIRD_SETTINGS_TIMER_MIN).toInt();
	if(nPeriod < 1)
		return;

	bool bOnlyKnownEmails = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_OnlyKnownEmails").toBool(); //g_pSettings->GetPersonSetting(THUNDERBIRD_SETTINGS_IMPORT_ONLY_KNOWN).toBool();
	bool bOnlyOutgoingEmails = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_OnlyOutgoingEmails").toBool();
	
	qDebug()<<ThreadIdentificator::GetCurrentThreadID();
	
	QString strMessage=tr("Start subscription Thunderbird process");
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);

	//OK, we may scan the Thunderbird profile
	QList<QStringList> lstFolders;

	//load Thunderbird folder selection setting
	QString strData = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_ScanFolders").toString();
	String2FolderList(strData, lstFolders);

	QList<QByteArray> lstEntryIDs;
	QDateTime dtLastEmail; 

	//define list:
	DbRecordSet lstDataEmails;	//email storage
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	lstDataEmails.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
	lstDataEmails.copyDefinition(set, false);
	lstDataEmails.addColumn(QVariant::String, "PROJECT_NAME");
	lstDataEmails.addColumn(QVariant::String, "PROJECT_CODE");
	lstDataEmails.addColumn(QVariant::String, "EMAIL_UNASSIGNED");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "ATTACHMENTS");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "CONTACTS");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "UAR_TABLE");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "GAR_TABLE");
	lstDataEmails.addColumn(QVariant::Int, "ATTACHMENTS_SIZE");

	QString strOffsets = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_LastScanFileOffsets").toString();
	QStringList lstOffsets = strOffsets.split(";");

	int nFoldersCnt = lstFolders.count();
	for(int i=0; i<nFoldersCnt; i++)
	{
		if(IsTerminateFlagSet())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Thunderbird subscription process terminated.");
			return;
		}

		QString strMboxPath = OutlookFolderPickerDlg::ThunderbirdFolder2String(lstFolders[i]);
		if(!strMboxPath.isEmpty())
		{
			int nStartOffset = 0;
			if(lstOffsets.count() > i)
				nStartOffset = lstOffsets[i].toInt();

			//Note: mbox file does not store mails sorted by date
			MailManager::GetThunderBirdEmailEntries(strMboxPath,lstDataEmails,dtLastEmail,EmailSynchronizationManager::GetThunderBirdLastScan(), nStartOffset, bOnlyOutgoingEmails);

			//now update the offset
			QFile file(strMboxPath);
			nStartOffset = file.size();

			if(lstOffsets.count() <= i)
				lstOffsets << QString("%1").arg(nStartOffset);
			else
				lstOffsets[i] = QString("%1").arg(nStartOffset);
		}
	}

	//now update the offsets setting
	QString strNewOffsets;
	int nSize = lstOffsets.count();
	for(int i=0; i<nSize; i++){
		strNewOffsets += QString("%1").arg(lstOffsets[i]);
		if(i+1 < nSize)
			strNewOffsets += ";";
	}
	g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_LastScanFileOffsets", strNewOffsets);

	if(IsTerminateFlagSet())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Thunderbird subscription process terminated.");
		return;
	}


	int nCntMails = lstDataEmails.getRowCount();
	if(nCntMails > 0)
	{
		Status status; 
		FUI_ImportEmail::FilterEmailsByContact(status, lstDataEmails);
		_CHK_ERR_NO_MSG(status);
		int nSkippedOrReplacedCnt = 0; 
		FUI_ImportEmail::ImportEmails(status, lstDataEmails, nSkippedOrReplacedCnt, false, true, true);
		_CHK_ERR_NO_MSG(status);
	}

	if(dtLastEmail.isValid()){
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_LastScan", dtLastEmail.toString("dd.MM.yyyy hh:mm:ss"));//g_pSettings->SetPersonSetting(THUNDERBIRD_SETTINGS_LAST_SCANNED, dtLastEmail.toString("dd.MM.yyyy hh:mm:ss"));
	}

	strMessage=tr("End subscription Thunderbird process");
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);
}

//----------------------------EMAIL MANAGER----------------------------------
//----------------------------EMAIL MANAGER----------------------------------

EmailSynchronizationManager::EmailSynchronizationManager()
:m_pEmailImportThreadOutlook(NULL),m_pEmailImportThreadThunderbird(NULL)
{
	g_ChangeManager.registerObserver(this);

}
EmailSynchronizationManager::~EmailSynchronizationManager()
{
	g_ChangeManager.unregisterObserver(this);
	StopAll();
	if (m_pEmailImportThreadOutlook)
		delete m_pEmailImportThreadOutlook;
	if (m_pEmailImportThreadThunderbird)
		delete m_pEmailImportThreadThunderbird;
}

bool EmailSynchronizationManager::EnableOutlookSync()
{
#ifdef _WIN32
	if (!m_pEmailImportThreadOutlook) //if thread is alive then its ok
	{
		//test mapi
		bool bMapiOK = MapiManager::IsOutlookInstalled();

		if(!bMapiOK)
		{
	#ifdef _DEBUG //BT:on systems without Outlook & mapi, allow normal startup
			QMessageBox::information(NULL, "Warning", tr("Failed to initialize MAPI!"));
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, tr("Failed to initialize MAPI!"));
	#endif
			return false; 
		}
		m_pEmailImportThreadOutlook = new EmailImportThread();
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start outlook thread");
		m_pEmailImportThreadOutlook->Start();
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"End of Start outlook thread");
		connect(&m_EmailSubscriptionTimerOutlook, SIGNAL(timeout()), this, SLOT(OnOutlookCheckTimer()));

		//FIX: ne radi
		//connect to perform the MAPI cleanup when thread finished (destructor was not called, so we must use this) 
		//EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadOutlook->GetThreadSpawnedObject());
		//if(pAction){
			//connect(m_pEmailImportThreadOutlook, SIGNAL(finished()), pAction,SLOT(DoMapiCleanup()));
			//connect(m_pEmailImportThreadOutlook, SIGNAL(terminated()), pAction,SLOT(DoMapiCleanup()));
		//}

		//BT: 12.01.2009:
		//when terminate flag is set to kill process, enable signal to pass to working thread to kill socket if working thread is stuck in BO operations with server
		EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadOutlook->GetThreadSpawnedObject());
		if(pAction){
		//connect(m_pEmailImportThreadOutlook, SIGNAL(finished()), pAction,SLOT(DoMapiCleanup()));
			connect(this, SIGNAL(KillBoOperationInWorkingThread()), pAction,SLOT(OnKillBoOperation()));
		}
	}
#else
	return false;
#endif
	return true;

}
bool EmailSynchronizationManager::EnableThunderBirdSync()
{
	if (!m_pEmailImportThreadThunderbird) //if thread is alive then its ok
	{
		if(!MailManager::IsThunderbirdInstalled())return false;
		m_pEmailImportThreadThunderbird = new EmailImportThread(true);
		m_pEmailImportThreadThunderbird->Start();
		connect(&m_EmailSubscriptionTimerThunderbird, SIGNAL(timeout()), this, SLOT(OnThunderbirdCheckTimer()));
	}
	return true;
}
void EmailSynchronizationManager::StopAll()
{
	if (m_pEmailImportThreadOutlook)
	{
		m_EmailSubscriptionTimerOutlook.stop();
		EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadOutlook->GetThreadSpawnedObject());
		if(pAction)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start Outlook Thread MAPI Clean Up");
			pAction->DoMapiCleanup();	//na divljaka
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"End Outlook Thread MAPI Clean Up");
		}
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Stopping Outlook Thread");
		m_pEmailImportThreadOutlook->Stop();
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook Thread Stopped");

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Delete Outlook Thread");
		delete m_pEmailImportThreadOutlook;
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook Thread Deleted!");
		m_pEmailImportThreadOutlook=NULL;

	}
	if (m_pEmailImportThreadThunderbird)
	{
		m_EmailSubscriptionTimerThunderbird.stop();
		m_pEmailImportThreadThunderbird->Stop();
		delete m_pEmailImportThreadThunderbird;
		m_pEmailImportThreadThunderbird=NULL;
	}
}

bool EmailSynchronizationManager::IsThreadRunning() //check both threads:
{
	bool bRunning=false;
	if (m_pEmailImportThreadOutlook)
	{
		EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadOutlook->GetThreadSpawnedObject());
		if(pAction)
			bRunning=pAction->IsThreadRunning();
	}
	if (m_pEmailImportThreadThunderbird)
	{
		EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadThunderbird->GetThreadSpawnedObject());
		if(pAction)
			bRunning=bRunning | pAction->IsThreadRunning();
	}

	return bRunning;
}
void EmailSynchronizationManager::OnOutlookCheckTimer()
{
	if (m_pEmailImportThreadOutlook)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start OnOutlookCheckTimer");
		EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadOutlook->GetThreadSpawnedObject());
		if(pAction)
			pAction->ScheduleAction();
	}

}
void EmailSynchronizationManager::OnThunderbirdCheckTimer()
{
	if (m_pEmailImportThreadThunderbird)
	{
		EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadThunderbird->GetThreadSpawnedObject());
		if(pAction)
			pAction->ScheduleAction();
	}
}
QDateTime EmailSynchronizationManager::GetOutlookLastScan()
{
	QDateTime dtLastScan;
	int m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstOutlookSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	if(-1 < m_nIniLstRow)
	{
		QString strDate = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_LastScan").toString();
		if(strDate.isEmpty())
			dtLastScan = QDateTime::currentDateTime(); //TOFIX?
		else
			dtLastScan = QDateTime::fromString(strDate, "dd.MM.yyyy hh:mm:ss");
	}

	return dtLastScan;
}
QDateTime EmailSynchronizationManager::GetThunderBirdLastScan()
{
	QDateTime dtLastScan;
	int m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	if(-1 < m_nIniLstRow)
	{
		QString strDate = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_LastScan").toString(); //g_pSettings->GetPersonSetting(THUNDERBIRD_SETTINGS_LAST_SCANNED).toString();
		if(strDate.isEmpty())
			dtLastScan = QDateTime::currentDateTime(); //TOFIX?
		else
			dtLastScan = QDateTime::fromString(strDate, "dd.MM.yyyy hh:mm:ss");
	}
	return dtLastScan;
}

void EmailSynchronizationManager::StartUserSession()
{
	//init outlook:
	//find Ini row (per-connection settings)
	int m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstOutlookSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	if(-1 < m_nIniLstRow)
	{
		if(g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool())
		{
			if(EnableOutlookSync())
			{
				//start ourlook?
				int nPeriod = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt();
				if(nPeriod > 0)
				{
					nPeriod *= 60 * 1000;	// min to ms
					m_EmailSubscriptionTimerOutlook.start(nPeriod);
				}
			}
		}
	}

	//init thunderbird:
	//find Ini row (per-connection settings)
	m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	if(m_nIniLstRow >= 0)
	{
		if(g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool()) //g_pSettings->GetPersonSetting(THUNDERBIRD_SETTINGS_SUBSCRIBED).toBool()) //if enabled start timer:
		{
			if (EnableThunderBirdSync())
			{
					//start thunder?
					int nPeriod1 = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt(); //g_pSettings->GetPersonSetting(THUNDERBIRD_SETTINGS_TIMER_MIN).toInt();
					if(nPeriod1 > 0)
					{
						nPeriod1 *= 60 * 1000;	// min to ms
						m_EmailSubscriptionTimerThunderbird.start(nPeriod1);
					}
			}
		}
	}
}
void EmailSynchronizationManager::EndUserSession(bool bTerminateAndDontAskUser) //threads are idle...
{
	m_EmailSubscriptionTimerOutlook.stop();
	m_EmailSubscriptionTimerThunderbird.stop();

	//Ask user to terminate now or to wait finish: (must be before actual logoout from server):
	if (!bTerminateAndDontAskUser)
	{
		if (IsThreadRunning())
		{
			QString strMsg=tr("Email Import Thread is working. Do you want to terminate Email Import and close application?");
			TimeMessageBox box(20,true,QMessageBox::Question,tr("Confirmation"),strMsg,QMessageBox::Yes | QMessageBox::No);
			box.exec();
			int nv2=box.result();	
			if (nv2 == QMessageBox::Yes)
			{
				if (m_pEmailImportThreadOutlook)
				{
					EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadOutlook->GetThreadSpawnedObject());
					if(pAction)
						pAction->SetTerminateFlag();
				}
				if (m_pEmailImportThreadThunderbird)
				{
					EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadThunderbird->GetThreadSpawnedObject());
					if(pAction)
						pAction->SetTerminateFlag();
				}

				emit KillBoOperationInWorkingThread();
				ThreadSleeper::Sleep(500); //give signal a chance to jump over to working thread
			}
		}
	}
	else
	{
		if (m_pEmailImportThreadOutlook)
		{
			EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadOutlook->GetThreadSpawnedObject());
			if(pAction)
				pAction->SetTerminateFlag();
		}
		if (m_pEmailImportThreadThunderbird)
		{
			EmailImportAction *pAction = dynamic_cast<EmailImportAction*>(m_pEmailImportThreadThunderbird->GetThreadSpawnedObject());
			if(pAction)
				pAction->SetTerminateFlag();
		}
	}


	StopAll(); //always stop threads when logout or exit application
}


void EmailSynchronizationManager::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_SUBSCRIPTION_TIMER_OUTLOOK_CHANGED)
	{
		m_EmailSubscriptionTimerOutlook.stop();
		if (nMsgDetail>0)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start EnableOutlookSync");
			if (EnableOutlookSync())//if not started try to start outlook interface
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Start updateObserver Outlook, timeout in: %1").arg(nMsgDetail));
				m_EmailSubscriptionTimerOutlook.start(nMsgDetail);
			}
		}
	}
	if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_SUBSCRIPTION_TIMER_THUNDERBIRD_CHANGED)
	{
		m_EmailSubscriptionTimerThunderbird.stop();
		if (nMsgDetail>0)
			if (EnableThunderBirdSync()) //if not started try to start thunder interface
				m_EmailSubscriptionTimerThunderbird.start(nMsgDetail);
	}
}

//Writes attachments to DB
void EmailImportAction::WriteEmailAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments)
{


	//Write content of attachments to local temp location, best: temp+/email_attach_+email_id
	QString strAttachmentSubDir="_email_attach_"+QString::number(nEmailID);
	QString strAttachmentSubDirFullPath= QDir::tempPath()+"/"+strAttachmentSubDir;

	QDir attach(strAttachmentSubDirFullPath);
	if (attach.exists())
	{
		DataHelper::RemoveAllFromDirectory(QDir::tempPath()+"/"+strAttachmentSubDir,true,false);
	}
	else
	{
		QDir attach_create(QDir::tempPath());
		if(!attach_create.mkdir(strAttachmentSubDir))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to create directory "+strAttachmentSubDirFullPath);
			return;
		}
	}

	//rename all files with prefix (to avoid problems)
	QString strAttachmentPrefix="email_attach_"+QString::number(nEmailID)+"_";

	//prepare object for server upload:
	HTTPClientConnectionSettings settings;
	Authenticator auth;
	BusinessServiceManager_ThinClient *pBo = dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet);
	if (pBo==NULL)
	{
		Ret_pStatus.setError(1,tr("Not available in the thick client"));
		return;
	}
	pBo->GetSettings(settings,auth);

	UserStorageHTTPClient *pHttpclient = new UserStorageHTTPClient;
	pHttpclient->SetConnectionSettings(settings,auth);
	
	//write files one by one to local disk
	int nSize=Attachments.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strName=strAttachmentPrefix+Attachments.getDataRef(i,"BEA_NAME").toString();
		QString strFilePath=strAttachmentSubDirFullPath+"/"+strName;
		int nAttachID=Attachments.getDataRef(i,"BEA_ID").toInt();

		QFile file(strFilePath);
		if(!file.open(QIODevice::WriteOnly))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to create file "+strFilePath);
			pHttpclient->deleteLater();
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
			return;
		}
		QByteArray	byteFileContent=Attachments.getDataRef(0,"BEA_CONTENT").toByteArray(); 
		int nErr=file.write(byteFileContent);
		if (nErr==-1)
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to write to file "+strFilePath);
			pHttpclient->deleteLater();
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
			return;
		}

		file.close();

		//upload files one by one to server
		pHttpclient->UpLoadFile(Ret_pStatus,strFilePath,true);
		if (!Ret_pStatus.IsOK())
		{
			pHttpclient->deleteLater();
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
			return;
		}

		QFile::remove(strFilePath);
	}

	//clear temp object:
	pHttpclient->deleteLater();
	DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
	QByteArray empty;
	Attachments.setColValue("BEA_CONTENT",empty);

	//actually write attachment records on server (no blob)
	g_pBoSet->app->BusEmail->WriteAttachments(Ret_pStatus,nEmailID,Attachments);
	if (!Ret_pStatus.IsOK())
	{
		pHttpclient->deleteLater();
		DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
		return;
	}

	//send signal to server to fetch files and write to db
	g_pBoSet->app->BusEmail->WriteEmailAttachmentsFromUserStorage(Ret_pStatus,nEmailID,Attachments);
}
