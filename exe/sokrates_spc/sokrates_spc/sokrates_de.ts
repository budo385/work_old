<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>@default</name>
    <message>
        <source>Communication Center</source>
        <translation type="obsolete">Persönliche Kommunikation</translation>
    </message>
    <message>
        <source>Emails, Voice Calls&lt;br&gt;And Documents</source>
        <translation type="obsolete">E-Mails, Anrufe&lt;br&gt;und Dokumente</translation>
    </message>
    <message>
        <source>Phone Call</source>
        <translation type="obsolete">Telefonanruf</translation>
    </message>
    <message>
        <source>Call Forwarding</source>
        <translation type="obsolete">Anrufweiterleitung</translation>
    </message>
    <message>
        <source>Project Management</source>
        <translation type="obsolete">Projekt Management</translation>
    </message>
    <message>
        <source>Define, Monitor And Control&lt;br&gt;Structured Projects</source>
        <translation type="obsolete">Definition, Überwachung und Controlling&lt;br&gt;strukturierter Projekte</translation>
    </message>
    <message>
        <source>Projects</source>
        <translation type="obsolete">Projekte</translation>
    </message>
    <message>
        <source>Import Projects</source>
        <translation type="obsolete">Projekte importieren</translation>
    </message>
    <message>
        <source>Contact Management</source>
        <translation type="obsolete">Contact Management</translation>
    </message>
    <message>
        <source>Maintain Contact Persons&lt;br&gt;And Organizations</source>
        <translation type="obsolete">Kontaktpersonen und&lt;br&gt;Organisationen bearbeiten</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="obsolete">Kontakte</translation>
    </message>
    <message>
        <source>Import Contacts</source>
        <translation type="obsolete">Kontakte importieren</translation>
    </message>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
    <message>
        <source>Enter And Organize Main&lt;br&gt;Data Structures</source>
        <translation type="obsolete">Erfassung und Organisation von&lt;br&gt;Datenstrukturen</translation>
    </message>
    <message>
        <source>Organizations</source>
        <translation type="obsolete">Organisationen</translation>
    </message>
    <message>
        <source>Cost Centers</source>
        <translation type="obsolete">Kostenstellen</translation>
    </message>
    <message>
        <source>Departments</source>
        <translation type="obsolete">Abteilungen</translation>
    </message>
    <message>
        <source>Users</source>
        <translation type="obsolete">Benutzer</translation>
    </message>
    <message>
        <source>Login Accounts</source>
        <translation type="obsolete">Login-Konto</translation>
    </message>
    <message>
        <source>Import Users</source>
        <translation type="obsolete">Benutzer importieren</translation>
    </message>
    <message>
        <source>Reporting</source>
        <translation type="obsolete">Auswertungen</translation>
    </message>
    <message>
        <source>Print And Export Reports</source>
        <translation type="obsolete">Druck und Export von Auswertungen</translation>
    </message>
    <message>
        <source>Actual Reports</source>
        <translation type="obsolete">Aktuelle Auswertungen</translation>
    </message>
    <message>
        <source>Administrator</source>
        <translation type="obsolete">Administrator</translation>
    </message>
    <message>
        <source>System Configuration,&lt;br&gt;Maintenance And Rights</source>
        <translation type="obsolete">Systemkonfiguration,&lt;br&gt;Unterhalt und Rechte</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Optionen</translation>
    </message>
    <message>
        <source>Personal Settings</source>
        <translation type="obsolete">Persönliche Einstellungen</translation>
    </message>
    <message>
        <source>Rights &amp; Roles</source>
        <translation type="obsolete">Rechte &amp; Rollen</translation>
    </message>
    <message>
        <source>Role To User Assignment</source>
        <translation type="obsolete">Benutzern Rollen zuweisen</translation>
    </message>
    <message>
        <source>Login Account Access Rights</source>
        <translation type="obsolete">Zugriffsrechte Login-Konto</translation>
    </message>
    <message>
        <source>Role Definition</source>
        <translation type="obsolete">Rollendefinition</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="obsolete">Konfiguration</translation>
    </message>
    <message>
        <source>Type Definitions</source>
        <translation type="obsolete">Typendefinitionen</translation>
    </message>
    <message>
        <source>Communication Categories</source>
        <translation type="obsolete">Kommunikations-Kategorien</translation>
    </message>
    <message>
        <source>Communication Event Types</source>
        <translation type="obsolete">Arten von Kommunikations-Ereignissen</translation>
    </message>
    <message>
        <source>Assignment Role Definitions</source>
        <translation type="obsolete">Rollen zuweisen</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="obsolete">Desktop</translation>
    </message>
    <message>
        <source>Import Emails</source>
        <translation type="obsolete">Import E-Mails</translation>
    </message>
</context>
<context>
    <name>EmailImportAction</name>
    <message>
        <location filename="emailimportthread.cpp" line="+230"/>
        <source>Start subscription Outlook process</source>
        <translation>Start Synchronisation von Outlook-Nachrichten </translation>
    </message>
    <message>
        <location line="+298"/>
        <source>End subscription Outlook process</source>
        <translation>Ende Synchronisation von Outlook-Nachrichten</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start subscription Thunderbird process</source>
        <translation>Start Synchronisation von Thunderbird-Nachrichten </translation>
    </message>
    <message>
        <location line="+94"/>
        <source>End subscription Thunderbird process</source>
        <translation>Ende Synchronisation von Thunderbird-Nachrichten</translation>
    </message>
    <message>
        <location line="-301"/>
        <source>Outlook importing %1 emails</source>
        <translation>%1 E-Mails werden aus Outlook® importiert</translation>
    </message>
    <message>
        <location line="-172"/>
        <source>Mail Import/Export thread failed to start. Reason: </source>
        <translation>Import/Export-Thread für E-Mails konnte nicht gestartet werden. Grund:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Mail Import/Export thread failed to finish. Reason: </source>
        <translation>Import/Export-Thread für E-Mails konnte nicht beendet werden. Grund:</translation>
    </message>
    <message>
        <location line="+245"/>
        <location line="+22"/>
        <source>Participant</source>
        <translation>Teilnehmer</translation>
    </message>
    <message>
        <location line="-16"/>
        <location line="+9"/>
        <source>Organizer</source>
        <translation>Organisator</translation>
    </message>
    <message>
        <location line="+531"/>
        <source>Not available in the thick client</source>
        <translation>Nicht verfügbar</translation>
    </message>
</context>
<context>
    <name>EmailSynchronizationManager</name>
    <message>
        <location line="-297"/>
        <location line="+1"/>
        <source>Failed to initialize MAPI!</source>
        <translation>MAPI-Interface konnte nicht initialisiert werden!</translation>
    </message>
    <message>
        <location line="+194"/>
        <source>Email Import Thread is working. Do you want to terminate Email Import and close application?</source>
        <translation>Der Import von E-Mails wird gerade ausgeführt. Wollen Sie den Import abbrechen und die Anwendung beenden?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
</context>
<context>
    <name>GlobalObjectManager</name>
    <message>
        <location filename="globalobjectmanager.cpp" line="+165"/>
        <source>Do you want to copy your actual Skype installation to your portable memory device to have it available everywhere?</source>
        <translation>Wollen Sie die Skype®-Installation auf der Festplatte auf Ihren tragbaren Datenspeicher kopieren um sie überall verfügbar zu haben?</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+12"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-12"/>
        <location line="+12"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Do your want to register your portable applications into SOKRATES(R) Communicator automatically?</source>
        <translation>Wollen Sie die Anwendungen auf Ihrem tragbaren Datenspeicher automatisch in SOKRATES® Communicator registrieren lassen?</translation>
    </message>
    <message>
        <location line="+153"/>
        <source>Unknown Error!</source>
        <translation>Unbekannter Fehler!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connection to server is lost!</source>
        <translation>Verbindung zum Server unterbrochen!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Session is terminated. Connection to server has been lost. Reason: </source>
        <translation>Sitzung beendet. Die Verbindung zum Server wurde unterbrochen. Ursache: </translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Error Message</source>
        <translation>Fehlermeldung</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Login in Progress...</source>
        <translation>Login wird durchgeführt...</translation>
    </message>
</context>
<context>
    <name>NetviewerDlg</name>
    <message>
        <location filename="netviewerdlg.cpp" line="+23"/>
        <source>Please fill the session number!</source>
        <translation>Bitte geben Sie die Sitzungsnummer ein!</translation>
    </message>
</context>
<context>
    <name>NetviewerDlgClass</name>
    <message>
        <location filename="netviewerdlg.ui" line="+16"/>
        <source>Starting Netviewer</source>
        <translation>Netviewer wird gestartet</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Please enter the desktop sharing session number:</source>
        <translation>Bitte geben Sie die Desktop-Sharing-Sitzungsnummer ein:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>If you do not have a session number: Ask your SOKRATES(R) support consultant over phone or email.</source>
        <translation>Falls Sie die Sitzungsnummer nicht kennen: Fragen Sie Ihren SOKRATES-Supportberater danach.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Communication Center</source>
        <translation type="obsolete">Communication Center</translation>
    </message>
    <message>
        <source>Emails, Voice Calls&lt;br&gt;And Documents</source>
        <translation type="obsolete">E-Mails, Anrufe&lt;br&gt;und Dokumente</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="obsolete">Desktop</translation>
    </message>
    <message>
        <source>Phone Call</source>
        <translation type="obsolete">Telefonanruf</translation>
    </message>
    <message>
        <source>Call Forwarding</source>
        <translation type="obsolete">Anrufweiterleitung</translation>
    </message>
    <message>
        <source>Project Management</source>
        <translation type="obsolete">Projekt Management</translation>
    </message>
    <message>
        <source>Define, Monitor And Control&lt;br&gt;Structured Projects</source>
        <translation type="obsolete">Definition, Überwachung und Controlling&lt;br&gt;strukturierter Projekte</translation>
    </message>
    <message>
        <source>Projects</source>
        <translation type="obsolete">Projekte</translation>
    </message>
    <message>
        <source>Import Projects</source>
        <translation type="obsolete">Projektimport</translation>
    </message>
    <message>
        <source>Contact Management</source>
        <translation type="obsolete">Contact Management</translation>
    </message>
    <message>
        <source>Maintain Contact Persons&lt;br&gt;And Organizations</source>
        <translation type="obsolete">Kontaktpersonen und&lt;br&gt;Organisationen bearbeiten</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="obsolete">Kontakte</translation>
    </message>
    <message>
        <source>Import Contacts</source>
        <translation type="obsolete">Kontaktimport</translation>
    </message>
    <message>
        <source>Import Emails</source>
        <translation type="obsolete">Import E-Mails</translation>
    </message>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
    <message>
        <source>Enter And Organize Main&lt;br&gt;Data Structures</source>
        <translation type="obsolete">Erfassung und Organisation von&lt;br&gt;Datenstrukturen</translation>
    </message>
    <message>
        <source>Organizations</source>
        <translation type="obsolete">Organisationen</translation>
    </message>
    <message>
        <source>Cost Centers</source>
        <translation type="obsolete">Kostenstellen</translation>
    </message>
    <message>
        <source>Departments</source>
        <translation type="obsolete">Abteilungen</translation>
    </message>
    <message>
        <source>Users</source>
        <translation type="obsolete">Benutzer</translation>
    </message>
    <message>
        <source>Login Accounts</source>
        <translation type="obsolete">Login-Konto</translation>
    </message>
    <message>
        <source>Import Users</source>
        <translation type="obsolete">Benutzerimport</translation>
    </message>
    <message>
        <source>Reporting</source>
        <translation type="obsolete">Auswertungen</translation>
    </message>
    <message>
        <source>Print And Export Reports</source>
        <translation type="obsolete">Druck und Export von Auswertungen</translation>
    </message>
    <message>
        <source>Actual Reports</source>
        <translation type="obsolete">Aktuelle Auswertungen</translation>
    </message>
    <message>
        <source>Administrator</source>
        <translation type="obsolete">Administrator</translation>
    </message>
    <message>
        <source>System Configuration,&lt;br&gt;Maintenance And Rights</source>
        <translation type="obsolete">Systemkonfiguration,&lt;br&gt;Unterhalt und Rechte</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Optionen</translation>
    </message>
    <message>
        <source>Personal Settings</source>
        <translation type="obsolete">Persönliche Einstellungen</translation>
    </message>
    <message>
        <source>Rights &amp; Roles</source>
        <translation type="obsolete">Rechte &amp; Rollen</translation>
    </message>
    <message>
        <source>User Access Rights</source>
        <translation type="obsolete">Benutzer-Zugriffsrechte</translation>
    </message>
    <message>
        <source>Login Account Access Rights</source>
        <translation type="obsolete">Zugriffsrechte Login-Konto</translation>
    </message>
    <message>
        <source>Role Definition</source>
        <translation type="obsolete">Rollendefinition</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="obsolete">Konfiguration</translation>
    </message>
    <message>
        <source>Type Definitions</source>
        <translation type="obsolete">Typendefinitionen</translation>
    </message>
    <message>
        <source>Communication Categories</source>
        <translation type="obsolete">Kommunikations-Kategorien</translation>
    </message>
    <message>
        <source>Communication Event Types</source>
        <translation type="obsolete">Arten von Kommunikations-Ereignissen</translation>
    </message>
    <message>
        <source>Assignment Role Definitions</source>
        <translation type="obsolete">Rollen zuweisen</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="obsolete">Login</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="obsolete">Logout</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Passwort</translation>
    </message>
    <message>
        <source>Administrator Tools</source>
        <translation type="obsolete">Administrationswerkzeuge</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">Beenden</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="obsolete">Ausschneiden</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="obsolete">Kopieren</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Einfügen</translation>
    </message>
    <message>
        <source>Toolbar</source>
        <translation type="obsolete">Werkzeugleiste</translation>
    </message>
    <message>
        <source>Status Bar</source>
        <translation type="obsolete">Statusleiste</translation>
    </message>
    <message>
        <source>Header</source>
        <translation type="obsolete">Kopfteil</translation>
    </message>
    <message>
        <source>Full Screen</source>
        <translation type="obsolete">Ganzer Schirm</translation>
    </message>
    <message>
        <source>Listen For Skype Calls</source>
        <translation type="obsolete">Skype-Anrufe entgegennehmen</translation>
    </message>
    <message>
        <source>New Phone Call</source>
        <translation type="obsolete">Neuer Anruf</translation>
    </message>
    <message>
        <source>Document Types</source>
        <translation type="obsolete">Dokumentenarten</translation>
    </message>
    <message>
        <source>Email Types</source>
        <translation type="obsolete">Email-Typen</translation>
    </message>
    <message>
        <source>Phone Call Types</source>
        <translation type="obsolete">Telefonanruf-Typen</translation>
    </message>
    <message>
        <source>Address Schemas</source>
        <translation type="obsolete">Adressschemas</translation>
    </message>
    <message>
        <source>Set Up Printer</source>
        <translation type="obsolete">Drucker einrichten</translation>
    </message>
    <message>
        <source>Support Desktop Sharing</source>
        <translation type="obsolete">Bildschirm für Support freigeben</translation>
    </message>
    <message>
        <source>Communicator Help</source>
        <translation type="obsolete">Hilfe zu Communicator</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">Über</translation>
    </message>
    <message>
        <source>Session is terminated. Connection to server has been lost. Reason: </source>
        <translation type="obsolete">Sitzung beendet. Die Verbindung zum Server wurde unterbrochen. Ursache: </translation>
    </message>
    <message>
        <source>Error Message</source>
        <translation type="obsolete">Fehlermeldung</translation>
    </message>
    <message>
        <source>Click=Open&lt;br&gt;CTRL-Click=Open New Window&lt;br&gt;ALT-Click=Open New Tab&lt;br&gt;ALT-CTRL-Click=Open New Selection</source>
        <translation type="obsolete">Click=Öffnen&lt;br&gt;CTRL-Click=Neues Fenster öffnen&lt;br&gt;ALT-Click=Neues Register öffnen&lt;br&gt;ALT-CTRL-Click=Auswahl öffnen</translation>
    </message>
    <message>
        <source>Unknown Error!</source>
        <translation type="obsolete">Unbekannter Fehler!</translation>
    </message>
    <message>
        <source>Connection to server is lost!</source>
        <translation type="obsolete">Verbindung zum Server unterbrochen!</translation>
    </message>
    <message>
        <source>Start subscription Outlook process</source>
        <translation type="obsolete">Outlook-Nachrichten abonnieren</translation>
    </message>
    <message>
        <source>New message found:</source>
        <translation type="obsolete">Neue Meldung:</translation>
    </message>
    <message>
        <source>End subscription Outlook process</source>
        <translation type="obsolete">Abonnierung von Outlook-Nachrichten beenden</translation>
    </message>
    <message>
        <source>Export Projects </source>
        <translation type="obsolete">Projekte exportieren </translation>
    </message>
    <message>
        <source>Export Users </source>
        <translation type="obsolete">Benutzer exportieren </translation>
    </message>
    <message>
        <source>Projects: Synchronize Now! </source>
        <translation type="obsolete">Projekte: Jetzt synchronisieren! </translation>
    </message>
    <message>
        <source>Users: Synchronize Now! </source>
        <translation type="obsolete">Benutzer: Jetzt syncrhonisieren! </translation>
    </message>
    <message>
        <source>Debtors: Synchronize Now! </source>
        <translation type="obsolete">Debitoren: Jetzt synchronisieren! </translation>
    </message>
    <message>
        <location filename="globalobjectmanager.cpp" line="+148"/>
        <location line="+19"/>
        <source>No Skype installation found!</source>
        <translation>Keine Installation von Skype® gefunden!</translation>
    </message>
</context>
<context>
    <name>Sokrates_SideBar</name>
    <message>
        <source>Login</source>
        <translation type="obsolete">Login</translation>
    </message>
    <message>
        <location filename="sokrates_sidebar.cpp" line="+365"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide</source>
        <translation>Ausblenden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <source>Select View</source>
        <translation type="obsolete">Ansichtsmodus auswählen</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Professional View</source>
        <translation>Pro-Modus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Light View</source>
        <translation>Easy-Modus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sidebar View</source>
        <translation>Sidebar-Modus</translation>
    </message>
    <message>
        <location line="+72"/>
        <location line="+65"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location line="-53"/>
        <location line="+60"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="-48"/>
        <location line="+55"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location line="+1218"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NetViewer client has not been found!</source>
        <translation>Der NetViewer-Client konnte nicht gefunden werden!</translation>
    </message>
    <message>
        <source>Unknown Error!</source>
        <translation type="obsolete">Unbekannter Fehler!</translation>
    </message>
    <message>
        <source>Connection to server is lost!</source>
        <translation type="obsolete">Verbindung zum Server unterbrochen!</translation>
    </message>
    <message>
        <source>Session is terminated. Connection to server has been lost. Reason: </source>
        <translation type="obsolete">Sitzung beendet. Die Verbindung zum Server wurde unterbrochen. Ursache: </translation>
    </message>
    <message>
        <location line="-1462"/>
        <source>Switch to Professional View</source>
        <translation>Zur Pro-Ansicht wechseln</translation>
    </message>
    <message>
        <location line="+1059"/>
        <source>Preparing Desktop...</source>
        <translation>Desktop wird vorbereitet...</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Read In Progress</source>
        <translation>Einlesen wird durchgeführt</translation>
    </message>
    <message>
        <source>Communicator Help</source>
        <translation type="obsolete">Hilfe zu Communicator</translation>
    </message>
    <message>
        <location line="-477"/>
        <source>Online Help</source>
        <translation>Online-Hilfe</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>First Steps</source>
        <translation>Erste Schritte</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Personal Settings</source>
        <translation>Persönliche Einstellungen</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>Failed to open help file %1!</source>
        <translation type="obsolete">Help-Datei %1 konnte nicht geöffnet werden!</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Install Portable Skype(R)</source>
        <translation>Skype® Portable installieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Update Portable Skype(R)</source>
        <translation>Skype® Portable aktualisieren</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Register Applications on Portable Memory Device</source>
        <translation>Anwendungen auf tragbaren Datenspeicher registrieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Portable</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location line="+580"/>
        <source>Copy the actual Skype(R) installation from hard disk to your portable memory device. This can take a long time. Continue?</source>
        <translation>Aktuelle Skype® - Installation von der Festplatte auf den tragbaren Datenspeicher kopieren. Dies kann lange dauern. Weiterfahren?</translation>
    </message>
    <message>
        <location line="-476"/>
        <location line="+476"/>
        <location line="+9"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-485"/>
        <location line="+476"/>
        <location line="+9"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Do you want to update your Skype(R) installation on your portable memory device using your Skype(R) installation on the hard disk?</source>
        <translation>Wollen Sie die Skype®-Installation of Ihrem tragbaren Datenspeichern aus Ihrer Skype®-Installation auf der Festplatte aktualisieren?</translation>
    </message>
    <message>
        <location line="-485"/>
        <source>There are active download/upload operations. Do you want to terminate those operations and close application?</source>
        <translation>Es sind noch Datei-Downloads oder -Uploads aktiv. Wollen Sie diese abbrechen und das Programm beenden?</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+49"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
</context>
<context>
    <name>Sokrates_SideBarClass</name>
    <message>
        <location filename="sokrates_sidebar.ui" line="+20"/>
        <source>Sokrates_SideBar</source>
        <translation>Sokrates_SideBar</translation>
    </message>
</context>
<context>
    <name>sokrates_spc</name>
    <message>
        <source>Version</source>
        <translation type="obsolete">Version</translation>
    </message>
    <message>
        <location filename="sokrates_spc.cpp" line="+374"/>
        <location line="+22"/>
        <location line="+2198"/>
        <location line="+11"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-2231"/>
        <location line="+22"/>
        <location line="+2198"/>
        <location line="+11"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-1670"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Logout</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Personal Settings</source>
        <translation>Persönliche Einstellungen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Toolbar</source>
        <translation>Werkzeugleiste</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Status Bar</source>
        <translation>Statusleiste</translation>
    </message>
    <message>
        <source>Banner</source>
        <translation type="obsolete">Überschrift</translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="obsolete">F5</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Full Screen</source>
        <translation>Ganzer Schirm</translation>
    </message>
    <message>
        <source>Communication WorkBench</source>
        <translation type="obsolete">Communication Workbench</translation>
    </message>
    <message>
        <location line="-322"/>
        <location line="+336"/>
        <source>Contacts</source>
        <translation>Kontakte</translation>
    </message>
    <message>
        <location line="-335"/>
        <location line="+338"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Listen For Skype Calls</source>
        <translation>Skype-Anrufe entgegennehmen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Users</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Type Definitions</source>
        <translation>Typdefinitionen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Calendar Types</source>
        <translation>Kalendereintrags-Arten</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Import Contacts</source>
        <translation>Kontakte importieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Import Projects</source>
        <translation>Projekte importieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Import Users</source>
        <translation>Benutzer importieren</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Reporting</source>
        <translation>Auswertungen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Actual Reports</source>
        <translation>Aktuelle Auswertungen</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Import Center</source>
        <translation>Import-Zentrale</translation>
    </message>
    <message>
        <source>Communicator Help</source>
        <translation type="obsolete">Hilfe zu Communicator</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>Main Functions</source>
        <translation type="obsolete">Hauptfunktionen</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Telephony</source>
        <translation>Telefonie</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Data Exchange</source>
        <translation>Datenaustausch</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Windows</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Register Applications on Portable Memory Device</source>
        <translation>Anwendungen auf tragbarem Datenspeicher registieren</translation>
    </message>
    <message>
        <source>Error Message</source>
        <translation type="obsolete">Fehlermeldung</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Install Portable Skype(R)</source>
        <translation>Skype® Portable installieren</translation>
    </message>
    <message>
        <location line="-343"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Administrator Tools</source>
        <translation>Administrationswerkzeuge</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Header</source>
        <translation>Kopfteil</translation>
    </message>
    <message>
        <location line="-317"/>
        <location line="+334"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>New Phone Call</source>
        <translation>Neuer Anruf</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Document Types</source>
        <translation>Dokumentenarten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Email Types</source>
        <translation>Email-Typen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Phone Call Types</source>
        <translation>Telefonanruf-Typen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Address Schemas</source>
        <translation>Adressschemas</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Import Emails</source>
        <translation>Import E-Mails</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Import Calendar Events</source>
        <translation>Termine werden  importiert...</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Support Desktop Sharing</source>
        <translation>Bildschirm für Support freigeben</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Main</source>
        <translation>Zentrale</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Support</source>
        <translation>Support</translation>
    </message>
    <message>
        <location line="+583"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NetViewer client has not been found!</source>
        <translation>Der NetViewer client konnte nicht gefunden werden!</translation>
    </message>
    <message>
        <source>Question</source>
        <translation type="obsolete">Frage</translation>
    </message>
    <message>
        <source>Do you want to share your monitor view with a support specialist? Ask him for a session number!</source>
        <translation type="obsolete">Wollen Sie Ihren Bildschirm für den Support-Spezialisten freigeben? Lassen Sie sich von ihm eine Session-Nummer geben!</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Preparing Desktop...</source>
        <translation>Desktop wird vorbereitet...</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+1210"/>
        <location line="+101"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-1310"/>
        <source>Read In Progress</source>
        <translation>Einlesen wird durchgeführt</translation>
    </message>
    <message>
        <location line="-1360"/>
        <source>Logged User:</source>
        <translation>Angemeldeter Benutzer:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Current Connection:</source>
        <translation>Aktuelle Verbindung:</translation>
    </message>
    <message>
        <location line="+1567"/>
        <location line="+42"/>
        <location line="+64"/>
        <location line="+70"/>
        <location line="+16"/>
        <location line="+3"/>
        <location line="+14"/>
        <location line="+3"/>
        <location line="+15"/>
        <location line="+3"/>
        <location line="+54"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location line="-89"/>
        <source>Projects successfully synchronized, check log for details.</source>
        <translation>Projekte erfolgreich synchronisiert, Einzelheiten stehen im Protokoll.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Users successfully synchronized, check log for details.</source>
        <translation>Benutzer erfolgreich synchronisiert, weitere Informationen in der Logdatei.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Debtors successfully synchronized, check log for details.</source>
        <translation>Debitoren erfolgreich synchronisiert, Einzelheiten stehen im Protokoll.</translation>
    </message>
    <message>
        <location line="-1679"/>
        <location line="+1760"/>
        <source>Click=Open&lt;br&gt;CTRL-Click=Open New Window&lt;br&gt;ALT-Click=Open New Tab&lt;br&gt;ALT-CTRL-Click=Open New Selection</source>
        <translation>Click=Öffnen&lt;br&gt;CTRL-Click=Neues Fenster öffnen&lt;br&gt;ALT-Click=Neues Register öffnen&lt;br&gt;ALT-CTRL-Click=Auswahl öffnen</translation>
    </message>
    <message>
        <location line="-1337"/>
        <source>Export Projects </source>
        <translation>Projekte exportieren </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export Users </source>
        <translation>Benutzer exportieren </translation>
    </message>
    <message>
        <source>Projects: Synchronize Now! </source>
        <translation type="obsolete">Projekte: Jetzt synchronisieren! </translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Users: Synchronize Now! </source>
        <translation>Benutzer: Jetzt synchronisieren! </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Debtors: Synchronize Now! </source>
        <translation>Debitoren: Jetzt synchronisieren! </translation>
    </message>
    <message>
        <source>Unknown Error!</source>
        <translation type="obsolete">Unbekannter Fehler!</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Export Contact-Contact Relationships</source>
        <translation>Beziehungen zwischen Kontakten exportieren</translation>
    </message>
    <message>
        <source>New message found:</source>
        <translation type="obsolete">Neue Meldung:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Projects: Synchronize Now!</source>
        <translation>Projekte: Jetzt synchronisieren! </translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Role Definitions</source>
        <translation>Definition Rollen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Role To User Assignment</source>
        <translation>Benutzern Rollen zuweisen</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Online Help</source>
        <translation>Online-Hilfe</translation>
    </message>
    <message>
        <source>Failed to open URL %1 (error: %2)!</source>
        <translation type="obsolete">Konnte URL %1 nicht öffnen (Fehler: %2)!</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Check For Available Updates</source>
        <translation>Auf verfügbare Updates prüfen</translation>
    </message>
    <message>
        <location line="+1258"/>
        <source>Update not available</source>
        <translation>Kein Update verfügbar</translation>
    </message>
    <message>
        <source>Select View</source>
        <translation type="obsolete">Ansichtsmodus auswählen</translation>
    </message>
    <message>
        <location line="-1696"/>
        <source>Professional View</source>
        <translation>Pro-Modus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Light View</source>
        <translation>Easy-Modus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sidebar View</source>
        <translation>Sidebar-Modus</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Switch to Sidebar View</source>
        <translation>Zur Sidebar-Ansicht wechseln</translation>
    </message>
    <message>
        <location line="+436"/>
        <source>First Steps</source>
        <translation>Erste Schritte</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Export Contacts</source>
        <translation>Kontakte exportieren</translation>
    </message>
    <message>
        <location line="+377"/>
        <location line="+10"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-10"/>
        <location line="+10"/>
        <source>You do not have sufficient access rights to perform this operation!</source>
        <translation>Nicht genügend Zugriffsrechte!</translation>
    </message>
    <message>
        <source>Failed to open help file %1 !</source>
        <translation type="obsolete">Hilfe-Datei %1 kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <location line="+825"/>
        <location line="+17"/>
        <location line="+18"/>
        <source>Process started on server. You will be notified when operation is completed!</source>
        <translation>Die Verarbeitung wurde auf dem Server gestartet. Sie werden nach der Fertigstellung benachrichtigt!</translation>
    </message>
    <message>
        <location line="-1068"/>
        <source>Update Portable Skype(R)</source>
        <translation>Skype® Portable aktualisieren</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Portable</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location line="+1299"/>
        <source>Copy the actual Skype(R) installation from hard disk to your portable memory device. This can take a long time. Continue?</source>
        <translation>Skype®-Installation von der Festplatte auf einen tragbaren Datenspeicher kopieren. Dies kann lange dauern. Weiterfahren?</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Do you want to update your Skype(R) installation on your portable memory device using your Skype(R) installation on the hard disk?</source>
        <translation>Wollen Sie die Skype®-Installation of Ihrem tragbaren Datenspeichern aus Ihrer Skype®-Installation auf der Festplatte aktualisieren?</translation>
    </message>
    <message>
        <location line="-380"/>
        <location line="+70"/>
        <source>Export of the contacts in progress</source>
        <translation>Kontaktexport wird durchgeführt</translation>
    </message>
    <message>
        <location line="-61"/>
        <location line="+70"/>
        <location line="+905"/>
        <source>Done!</source>
        <translation>Fertig!</translation>
    </message>
    <message>
        <location line="-2813"/>
        <source>There are active download/upload operations. Do you want to terminate those operations and close application?</source>
        <translation>Es sind noch Datei-Downloads oder -Uploads aktiv. Wollen Sie diese abbrechen und das Programm beenden?</translation>
    </message>
    <message>
        <location line="+603"/>
        <source>View Downloads/Uploads in progress</source>
        <translation>Downloads/Uploads in Bearbeitung anzeigen</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Resources</source>
        <translation>Ressourcen</translation>
    </message>
    <message>
        <source>Calendar Event</source>
        <translation type="obsolete">Kalendereintrag</translation>
    </message>
    <message>
        <location line="-379"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
    <message>
        <location line="+2136"/>
        <source>confirmed</source>
        <translation>bestätigt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>rejected</source>
        <translation>abgelehnt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>none</source>
        <translation>kein</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>answered</source>
        <translation>beantwortet</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The contact/User %1 %2 has %6 your invitation for the Calendar Event %3 from %4 to %5.</source>
        <translation>Der Konakt/Benutzer %1 %2 hat die Einladung für den Kalendereintrag %3 von %4 bis %5 %6.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open Event</source>
        <translation>Kalendereintrag öffnen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Owner notified</source>
        <translation>Besitzer benachrichtigt</translation>
    </message>
    <message>
        <location line="-1739"/>
        <source>Import User-Contact Relationships</source>
        <translation>Beziehungen zwischen Kontakten und Benutzern importieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Import Contact-Contact Relationships</source>
        <translation>Beziehungen zwischen Kontakten importieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Import Group Assignments</source>
        <translation>Gruppenzuweisungen importieren</translation>
    </message>
    <message>
        <location line="+2131"/>
        <source>Choose file</source>
        <translation>Datei wählen</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+101"/>
        <source>Loading Import File...</source>
        <translation>Import-Datei wird geladen...</translation>
    </message>
    <message>
        <location line="-100"/>
        <location line="+101"/>
        <source>Operation In Progress</source>
        <translation>Verarbeitung wird ausgeführt</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>Failed to import %1 items!</source>
        <translation>Import von %1 Datensätzen misslungen!</translation>
    </message>
    <message>
        <location line="-2242"/>
        <source>Calendar Views</source>
        <translation>Kalenderansichten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Payment Conditions</source>
        <translation>Zahlungskonditionen</translation>
    </message>
    <message>
        <location line="-684"/>
        <source>There are %1 checked out documents. Quit anyway?</source>
        <translation>Es gibt noch %1 ausgecheckte Dokumente. Trotzdem schliessen?</translation>
    </message>
    <message>
        <location line="+300"/>
        <source>Project Manager</source>
        <translation>Project Manager</translation>
    </message>
    <message>
        <location line="+422"/>
        <source>Import Stored Project Lists</source>
        <translation>Gespeicherte Projektlisten importieren</translation>
    </message>
    <message>
        <location line="+1966"/>
        <source>Choose Stored Project Lists Headers file</source>
        <translation>Auswahl Header-Datei für gespeicherte Projektlisten</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Choose Stored Project Lists Details file</source>
        <translation>Auswahl Details-Datei für gespeicherte Projektlisten</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Failed to load header file:
%1!</source>
        <translation>Header-Datei %1 konnte nicht geöffnet werden!</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Failed to load detail file:
%1!</source>
        <translation>Details-Datei %1 konnte nicht geöffnet werden!</translation>
    </message>
    <message>
        <location line="-2030"/>
        <source>Custom Fields Definition</source>
        <translation>Definition benutzerdefinierter Felder</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Import Contact User-Defined Fields</source>
        <translation>Import benutzerdefinierter Felder</translation>
    </message>
    <message>
        <location line="+2022"/>
        <source>Choose Contact User-Defined Fields import file</source>
        <translation>Auswahl Importdatei für benutzerdefinierte Felder</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Invalid file format!</source>
        <translation>Ungültiges Dateiformat!</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Import canceled!</source>
        <translation>Import abgeborchen!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Failed to import %1 out of %2 rows!</source>
        <translation>Fehler beim Import von  %1 aus %2 Zeilen!</translation>
    </message>
</context>
<context>
    <name>sokrates_spcClass</name>
    <message>
        <location filename="sokrates_spc.ui" line="+13"/>
        <source>SOKRATES® Communicator</source>
        <translation>SOKRATES® Communicator</translation>
    </message>
</context>
</TS>
