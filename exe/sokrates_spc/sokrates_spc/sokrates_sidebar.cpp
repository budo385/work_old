#include "os_specific/os_specific/mapimanager.h"
#include "sokrates_sidebar.h"
#include "gui_core/gui_core/gui_helper.h"
#include "fui_collection/fui_collection/sidebarfui_contact.h"
#include "fui_collection/fui_collection/sidebarfui_desktop.h"
#include "fui_collection/fui_collection/sidebarfui_project.h"
#include "fui_collection/fui_collection/fui_calendar.h"
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QMovie>
//#include "fui_collection/fui_collection/fui_projects.h"
//#include "fui_collection/fui_collection/fui_contacts.h"
#include "fui_collection/fui_collection/fui_communicationcenter.h"
#include "bus_client/bus_client/selectionpopupfloat.h"
#include "common/common/config_version_sc.h"
#include "gui_core/gui_core/aboutwidget.h"
#include "common/common/datahelper.h"
#include "bus_client/bus_client/documenthelper.h"
#include "fui_collection/fui_collection/dlg_rfparser.h"
#include "os_specific/os_specific/skypemanager.h"
#include "fui_collection/fui_collection/emaildialog.h"
#include "fui_collection/fui_collection/fui_projects.h"
#include "common/common/threadid.h"

//#include "fui_collection/fui_collection/fui_voicecallcenter.h"
#include "netviewerdlg.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "globalobjectmanager.h"
extern GlobalObjectManager *g_pGlobalObjectManager;
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "bus_client/bus_client/clientdownloadmanager.h"
extern ClientDownloadManager		*g_DownloadManager;


Sokrates_SideBar::Sokrates_SideBar(QWidget *parent)
	: QWidget(parent, Qt::FramelessWindowHint),m_SysTray(this),m_pBtnComm(NULL),m_pBtnProject(NULL),m_pBtnContact(NULL),m_pInfo(NULL),m_pBtnCalendar(NULL),
	m_bResizeInProgress(false),m_stackWidget(NULL),m_bVertResizeInProgress(false),m_bLoginExecuted(false),
	m_bWindowInitialized(false),m_pClock(NULL),m_nCurrentWindowTheme(-1),m_nResizeinProgressInFui_ID(-1),m_nResizeinProgressInFuiChapter_Idx(-1),m_bOnClientLoggedProcedureExecutionInProgress(false)
{
	ui.setupUi(this);
	m_nToolBarButtonIDCurrentFocus=-1;

	resize(280,140);

	//connect(g_pClientManager, SIGNAL(ClientLogged()), this, SLOT(OnClientLogged()));
	//connect(g_pClientManager, SIGNAL(ClientLogout()), this, SLOT(OnClientLoggOff()));
	connect(g_pClientManager, SIGNAL(LoginDialogCreated(QDialog *)), this, SLOT(OnLoginDialogCreated(QDialog *)));
	connect(g_pClientManager, SIGNAL(LoginDialogBeforeDestroy(QDialog *)), this, SLOT(OnLoginDialogBeforeDestroy(QDialog *)));
	connect(g_pClientManager, SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	//connect(g_pClientManager, SIGNAL(CommunicationFatalError(int)),this,SLOT(OnCommunicationFatalError(int)));
	//connect(g_pClientManager, SIGNAL(ClientBeforeLogout()),this,SLOT(OnProcessBeforeLogout()));
	

	m_SysTray.setIcon(QIcon(":Contacts16.png"));
	//m_SysTray.show();

	setAttribute(Qt::WA_QuitOnClose);
	setAttribute(Qt::WA_DeleteOnClose);
	m_pSidebarHandleWnd = new SidebarHandle(this);

	QString strTitle = QString("SOKRATES")+QChar(174)+QString(" Communicator");
	setWindowTitle(strTitle);
	m_pSidebarHandleWnd->setWindowTitle(strTitle);

	//desktopRec=QApplication::desktop()->availableGeometry(this);
	//connect(dynamic_cast<QApplication*>(qApp)->desktop(),SIGNAL(workAreaResized (int)),this,SLOT(OnScreenGeometryChanged(int)));
	//connect(this,SIGNAL(ResizeDelayed(int)),this,SLOT(OnResizeDelayed(int)),Qt::QueuedConnection);

	m_nTimer=startTimer(1000); //start now then every min
	//Default window height.
	m_nWindowHeight = 100;

	//QAction *dummy1 = new QAction(this);
	//dummy1->setVisible(false);
	//dummy1->setShortcut(Qt::ALT + Qt::Key_P);
	//dummy1->setShortcutContext(Qt::ApplicationShortcut);
	//connect(dummy1, SIGNAL(triggered()), this, SLOT(OnMenu_ProjectClick()));
}

Sokrates_SideBar::~Sokrates_SideBar()
{
	g_pGlobalObjectManager->OnApplicationClose(this);
	m_SysTray.hide();
	m_pSidebarHandleWnd->close();
	delete m_pSidebarHandleWnd;
}

void Sokrates_SideBar::Initialize ()
{
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
	{
		m_bWindowInitialized=true;
		SetupButtons();
		MoveToSide(false,false,true);
		show();
	}
}

void Sokrates_SideBar::OnClientLogged()
{
	qDebug()<<"Enter Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();

	if(!g_pClientManager->IsLogged())return;
	if (m_bLoginExecuted) return;
	m_bLoginExecuted=true;
	if (m_bOnClientLoggedProcedureExecutionInProgress) return;
	m_bOnClientLoggedProcedureExecutionInProgress=true;

	QApplication::setOverrideCursor(Qt::WaitCursor);
	OnThemeChanged();

	qDebug()<<"Still in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();

	m_pBtnComm->setIcon(":Desktop_G.png");
	m_pBtnContact->setIcon(":Contacts_G.png");
	m_pBtnProject->setIcon(":Projects_G.png");
	m_pBtnCalendar->setIcon(":Calendar_G.png");

	//switch to selected FUI:
	if (!m_stackWidget) //create stacked widget:
	{
		qDebug()<<"Create widget 1 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();

		m_stackWidget= new QStackedWidget(this);
		SideBarFui_Desktop *pDesktop= new SideBarFui_Desktop(m_stackWidget);
		if(!g_pClientManager->IsLogged()) //to prevent crash if server goes down between success login and this
		{
			qDebug()<<"Get Out 1 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();
			m_bOnClientLoggedProcedureExecutionInProgress=false;
			return;
		}

		qDebug()<<"Create widget 2 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();

		m_stackWidget->insertWidget(0,pDesktop);
		SideBarFui_Contact *pCont= new SideBarFui_Contact(m_stackWidget);
		if(!g_pClientManager->IsLogged()) //to prevent crash if server goes down between success login and this
		{
			qDebug()<<"Get Out 2 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();
			m_bOnClientLoggedProcedureExecutionInProgress=false;
			return;
		}

		qDebug()<<"Create widget 3 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();

		m_stackWidget->insertWidget(1,pCont);
		SideBarFui_Project *pProj= new SideBarFui_Project(m_stackWidget);
		if(!g_pClientManager->IsLogged()) //to prevent crash if server goes down between success login and this
		{
			qDebug()<<"Get Out 3 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();
			m_bOnClientLoggedProcedureExecutionInProgress=false;
			return;
		}
		m_stackWidget->insertWidget(2,pProj);

		connect(pDesktop,SIGNAL(ChapterCollapsed(int,int,bool)),this,SLOT(OnChildChapterResized(int,int,bool)));
		connect(pCont,SIGNAL(ChapterCollapsed(int,int,bool)),this,SLOT(OnChildChapterResized(int,int,bool)));
		connect(pProj,SIGNAL(ChapterCollapsed(int,int,bool)),this,SLOT(OnChildChapterResized(int,int,bool)));

		//remove splash:
		//ThemeManager::AddWidgetToSideBar(m_stackWidget);
		this->layout()->itemAt(2)->widget()->setVisible(false); //splash frame is in-visible
		(dynamic_cast<QVBoxLayout*>(this->layout()))->insertWidget(2,m_stackWidget);
	}

	//if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
	if (true)
		m_pBtnCalendar->setVisible(false);
	else
		m_pBtnCalendar->setVisible(true);


	qDebug()<<"4 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();

	//get settings
	int nFuiToOpen=g_pSettings->GetPersonSetting(STARTING_MODULE).toInt();
	if(!g_pClientManager->IsLogged()) //to prevent crash if server goes down between success login and this
	{
		m_bOnClientLoggedProcedureExecutionInProgress=false;
		return;
	}

	switch(nFuiToOpen)
	{
	case 0:
		OnMenu_DesktopClick();
		break;
	case 3:
	case 4:
		OnMenu_ContactClick();
		break;
	case 5:
		OnMenu_ProjectClick();
		break;
	default:
		OnMenu_ContactClick();
		break;
	}

	qDebug()<<"5 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();

	m_switcher->setVisible(true);
	QString strCurrentConnection=g_pClientManager->GetConnectionName();
	//QString strTitle = "SOKRATES� Communicator - ";
	QString strTitle = QString("SOKRATES")+QChar(174)+QString(" Communicator - ");
	strTitle += strCurrentConnection;
	setWindowTitle(strTitle);
	m_pSidebarHandleWnd->setWindowTitle(strTitle);
	
	qDebug()<<"6 in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();

	AdjustSizeOnLogon();
	QApplication::restoreOverrideCursor();

	m_bOnClientLoggedProcedureExecutionInProgress=false;
}

void Sokrates_SideBar::OnClientLogout()
{
	m_bLoginExecuted=false;

	if(m_stackWidget)
	{
		delete m_stackWidget;
		m_stackWidget=NULL;
	}
		
	//QString strTitle = "SOKRATES� Communicator";
	QString strTitle = QString("SOKRATES")+QChar(174)+QString(" Communicator");
	setWindowTitle(strTitle);
	m_pSidebarHandleWnd->setWindowTitle(strTitle);
	
	//m_bWindowInitialized=false;
}



void Sokrates_SideBar::SetMenuText(QString strText)
{
	if (!m_pInfo)return;
	
	m_pInfo->setProperty("ActualText",strText);
	m_pInfo->setText(	QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
								<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:400; font-style:italic; color:%1;\">%2</span></p>\
								</body></html>").arg(ThemeManager::GetToolbarTextColor()).arg(strText));
		

	//m_pInfo->setText(QString("<html><b><i><font family=Arial; size=\"5\" color:%1>%2 </font></i></b></html>").arg(ThemeManager::GetToolbarTextColor()).arg(strText));
}

void Sokrates_SideBar::SetClockText(QString strText)
{
	if (!m_pClock)return;

	//m_pClock->setText(QString("<html><body style=\" font-family:Arial Narrow; text-decoration:none;\">\
	//						  <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:500; color:white;\">%1</span></p>\
	//						  </body></html>").arg(strText));
	m_pClock->setStyleSheet("color: white");
	m_pClock->setText(strText);
}

void Sokrates_SideBar::SetupButtons()
{
	QSize buttonSize(70, 62);
	QString	styleNew=ThemeManager::GetToolbarButtonStyle();
	m_pBtnComm = new StyledPushButton(this, ":Desktop_G.png", ":Desktop_C.png", styleNew);
	m_pBtnContact	=	new StyledPushButton(this,":Contacts_G.png",":Contacts_C.png",styleNew);
	m_pBtnProject	=	new StyledPushButton(this,":Projects_G.png",":Projects_C.png",styleNew);
	m_pBtnCalendar	=	new StyledPushButton(this,":Calendar_G.png",":Calendar_C.png",styleNew);
	m_pBtnCalendar->setVisible(false);

	//MB always lite on on start:
	m_pBtnComm->setIcon(":Desktop_C.png");
	m_pBtnContact->setIcon(":Contacts_C.png");
	m_pBtnProject->setIcon(":Projects_C.png");
	m_pBtnCalendar->setIcon(":Calendar_C.png");


	m_pInfo		= new QLabel;
	m_pInfo->setAlignment(Qt::AlignCenter);
	//SetMenuText(tr("Login"));
	SetMenuText(tr("")); //issue 1778
	m_nToolBarButtonIDCurrentFocus=-1;
	//connect signals
	connect(m_pBtnComm,SIGNAL(clicked()),this,SLOT(OnMenu_DesktopClick()));
	connect(m_pBtnContact,SIGNAL(clicked()),this,SLOT(OnMenu_ContactClick()));
	connect(m_pBtnProject,SIGNAL(clicked()),this,SLOT(OnMenu_ProjectClick()));
	connect(m_pBtnCalendar,SIGNAL(clicked()),this,SLOT(OnMenu_CalendarClick()));
	connect(m_pBtnComm,SIGNAL(mouse_over()),this,SLOT(OnMouse_DesktopEnter()));
	connect(m_pBtnComm,SIGNAL(mouse_leave()),this,SLOT(OnMouse_DesktopLeave()));
	connect(m_pBtnContact,SIGNAL(mouse_over()),this,SLOT(OnMouse_ContactEnter()));
	connect(m_pBtnContact,SIGNAL(mouse_leave()),this,SLOT(OnMouse_ContactLeave()));
	connect(m_pBtnProject,SIGNAL(mouse_over()),this,SLOT(OnMouse_ProjectEnter()));
	connect(m_pBtnProject,SIGNAL(mouse_leave()),this,SLOT(OnMouse_ProjectLeave()));
	connect(m_pBtnCalendar,SIGNAL(mouse_over()),this,SLOT(OnMouse_CalendarEnter()));
	connect(m_pBtnCalendar,SIGNAL(mouse_leave()),this,SLOT(OnMouse_CalendarLeave()));

	//size
	m_pBtnComm			->setMaximumSize(buttonSize);
	m_pBtnComm			->setMinimumSize(buttonSize);
	m_pBtnProject		->setMaximumSize(buttonSize);
	m_pBtnProject		->setMinimumSize(buttonSize);
	m_pBtnContact		->setMaximumSize(buttonSize);
	m_pBtnContact		->setMinimumSize(buttonSize);
	m_pBtnCalendar		->setMaximumSize(buttonSize);
	m_pBtnCalendar		->setMinimumSize(buttonSize);


	//Tool m_ProgressBar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;
	//buttonLayout->setAlignment(Qt::AlignCenter);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnComm);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnContact);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnProject);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnCalendar);
	buttonLayout->addStretch(1);
	buttonLayout->setContentsMargins(0,2,0,0);
	buttonLayout->setSpacing(0);

	QVBoxLayout *buttonLayout_v = new QVBoxLayout;
	buttonLayout_v->addLayout(buttonLayout);
	buttonLayout_v->addWidget(m_pInfo);
	buttonLayout_v->setSpacing(3);
	buttonLayout_v->setContentsMargins(0,0,0,2);


	ui.frameToolBar->setLayout(buttonLayout_v);
	ui.frameToolBar->setStyleSheet(ThemeManager::GetToolbarBkgStyle());

	//HEADER:
	ui.frameBannerLeft->setStyleSheet(ThemeManager::GetSidebar_HeaderLeft());
	ui.frameBannerCenter->setStyleSheet(ThemeManager::GetSidebar_HeaderCenter());
	ui.frameBannerRight->setStyleSheet(ThemeManager::GetSidebar_HeaderRight());

	QSize buttonSize_menu(13, 11);
	m_help		= new StyledPushButton(this,":Icon_Help.png",":Icon_Help.png","");
	StyledPushButton *hide		= new StyledPushButton(this,":Icon_Hide.png",":Icon_Hide.png","");
	StyledPushButton *close		= new StyledPushButton(this,":Icon_Close.png",":Icon_Close.png","");
	m_switcher	= new StyledPushButton(this,":Icon_Switcher.png",":Icon_Switcher.png","");

	
	m_help		->setMaximumSize(buttonSize_menu);
	m_help		->setMinimumSize(buttonSize_menu);
	hide		->setMaximumSize(buttonSize_menu);
	hide		->setMinimumSize(buttonSize_menu);
	close		->setMaximumSize(buttonSize_menu);
	close		->setMinimumSize(buttonSize_menu);
	m_switcher	->setMaximumSize(buttonSize_menu);
	m_switcher	->setMinimumSize(buttonSize_menu);

	m_help		->setToolTip(tr("Help"));
	hide		->setToolTip(tr("Hide") );
	close		->setToolTip(tr("Close"));
	m_switcher	->setToolTip(tr("Switch to Professional View"));

	//label clock:
	m_pClock= new QLabel;
	m_pClock	->setAlignment(Qt::AlignRight);
	m_pClock	->setMaximumSize(QSize(90,16));
	m_pClock	->setMinimumSize(QSize(90,16));

	QHBoxLayout *layout_clock = new QHBoxLayout;
	layout_clock->addStretch(1);
	layout_clock->addWidget(m_pClock);
	layout_clock->setContentsMargins(0, 0, 0, 0);

	QHBoxLayout *buttonLayout_menu_h = new QHBoxLayout;
	buttonLayout_menu_h->addStretch(1);
	buttonLayout_menu_h->addWidget(m_help);
	buttonLayout_menu_h->addWidget(m_switcher);
	buttonLayout_menu_h->addWidget(hide);
	buttonLayout_menu_h->addWidget(close);
	buttonLayout_menu_h->setContentsMargins(0,3,0,0);
	buttonLayout_menu_h->setSpacing(0);

	QVBoxLayout *buttonLayout_menu_v = new QVBoxLayout;
	buttonLayout_menu_v->addLayout(buttonLayout_menu_h);
	buttonLayout_menu_v->addStretch(1);
	buttonLayout_menu_v->addLayout(layout_clock);
	buttonLayout_menu_v->setSpacing(0);
	buttonLayout_menu_v->setContentsMargins(0, 0, 8, 0);
	ui.frameBannerCenter->setLayout(buttonLayout_menu_v);

	connect(m_help,SIGNAL(clicked()),this,SLOT(OnHeader_HelpClick()));
	connect(hide,SIGNAL(clicked()),this,SLOT(OnHeader_HideClick()));
	connect(close,SIGNAL(clicked()),this,SLOT(OnHeader_CloseClick()));
	connect(m_switcher,SIGNAL(clicked()),this,SLOT(OnHeader_SwitcherClick()));


	//FOOTER:
	ui.frameFooterLeft->setStyleSheet(ThemeManager::GetSidebar_FooterLeft());
	ui.frameFooterCenter->setStyleSheet(ThemeManager::GetSidebar_FooterCenter());
	ui.frameFooterRight->setStyleSheet(ThemeManager::GetSidebar_FooterRight());

	ui.frameFooterLeft->setVisible(false);
	ui.frameFooterCenter->setVisible(false);
	ui.frameFooterRight->setVisible(false);

	QString strTitle = QString("SOKRATES") + QChar(174) + QString(" Communicator");
	QLabel *lblTitle = new QLabel(strTitle);
	QHBoxLayout *titleHLayout = new QHBoxLayout;
	titleHLayout->addWidget(lblTitle);
	titleHLayout->addStretch(1);
	titleHLayout->setSpacing(0);
	titleHLayout->setContentsMargins(8, 4, 4, 4);
	ui.frameBannerLeft->setLayout(titleHLayout);

	StyledPushButton *handle= new StyledPushButton(this,ThemeManager::GetSidebar_CenterHandle(),ThemeManager::GetSidebar_CenterHandle(),"","",0,0,Qt::SizeVerCursor);
	StyledPushButton *resizer= new StyledPushButton(this,"","","","",0,0,Qt::SizeFDiagCursor);

	resizer		->setMaximumSize(QSize(16,16));
	resizer		->setMinimumSize(QSize(16,16));
	handle		->setMaximumSize(QSize(67,10));
	handle		->setMinimumSize(QSize(67,10));

	QVBoxLayout *buttonLayout_footer_v = new QVBoxLayout;
	buttonLayout_footer_v->addStretch(1);
	buttonLayout_footer_v->addWidget(handle);
	buttonLayout_footer_v->addStretch(1);
	buttonLayout_footer_v->setSpacing(0);
	buttonLayout_footer_v->setContentsMargins(0,0,0,0);

	QHBoxLayout *buttonLayout_footer_h = new QHBoxLayout;
	buttonLayout_footer_h->addStretch(1);
	buttonLayout_footer_h->addLayout(buttonLayout_footer_v);
	buttonLayout_footer_h->addStretch(1);
	buttonLayout_footer_h->setSpacing(0);
	buttonLayout_footer_h->setContentsMargins(0,0,0,0);

	ui.frameFooterCenter->setLayout(buttonLayout_footer_h);

	QVBoxLayout *buttonLayout_footer_right = new QVBoxLayout;
	buttonLayout_footer_right->addWidget(resizer);
	buttonLayout_footer_right->setSpacing(0);
	buttonLayout_footer_right->setContentsMargins(0,0,0,0);
	
	ui.frameFooterRight->setLayout(buttonLayout_footer_right);

	//frame splash:
	ui.frameSplash->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg());

	//switcher menu:
	//m_switcher->setContextMenuPolicy(Qt::CustomContextMenu);
	//connect(m_switcher,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnCustomContextMenu_Switcher(const QPoint &)));
	m_pView_Pro = new QAction(tr("Professional View"), this);
	m_pView_Light = new QAction(tr("Light View"), this);
	m_pView_SideBar = new QAction(tr("Sidebar View"), this);
	connect(m_pView_Pro, SIGNAL(triggered()), this, SLOT(OnSwitcher_Pro()));
	//connect(m_pView_Light, SIGNAL(triggered()), this, SLOT(OnSwitcher_Light()));
	connect(m_pView_SideBar, SIGNAL(triggered()), this, SLOT(OnSwitcher_Sidebar()));
	m_pView_Pro->setCheckable(true);
	m_pView_Light->setCheckable(true);
	m_pView_SideBar->setCheckable(true);
	m_pView_SideBar->setChecked(true);

	m_switcher->setVisible(false);


	//STATUS BAR:
	m_statusBar = new QStatusBar(this);
	m_statusBar->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));

	m_labelWorkingIcon = new QLabel(this);
	QMovie *movie = new QMovie(":Icon_Working.mng","mng",this);
	m_labelWorkingIcon->setMovie(movie);
	movie->setCacheMode(QMovie::CacheAll) ;
	connect(m_labelWorkingIcon->movie(),SIGNAL(frameChanged(int)),this,SLOT(onFrameChanged_WorkingIcon(int)));
	m_labelWorkingIcon->hide();
	m_labelWorkingIcon		->setMaximumSize(QSize(35,19));
	m_labelWorkingIcon		->setMinimumSize(QSize(35,19));

	//progress m_ProgressBar on status m_ProgressBar:
	m_ProgressBar = new QProgressBar;
	QSize sizeBar(55,22);
	m_ProgressBar->setMaximumSize(sizeBar);
	m_statusBar->addPermanentWidget(m_ProgressBar);
	m_ProgressBar->setTextVisible(false);
	m_ProgressBar->setRange(0,9);
	m_ProgressBar->setVisible(false);

	//log msg:
	QSize buttonSizeX(24, 24);
	QToolButton *btnOpenLog= new QToolButton(this);
	btnOpenLog->setVisible(false);
	btnOpenLog->setMaximumSize(buttonSizeX);
	btnOpenLog->setAutoRaise(true);
	//m_statusBar->addPermanentWidget(btnOpenLog);
	DlgMessenger.Initialize(btnOpenLog,m_statusBar);

	m_statusBar->layout()->setContentsMargins(0,0,0,0);

	QHBoxLayout *buttonLayout_status = new QHBoxLayout;
	buttonLayout_status->addWidget(m_labelWorkingIcon);
	buttonLayout_status->addWidget(m_statusBar);
	buttonLayout_status->setSpacing(0);
	buttonLayout_status->setContentsMargins(0,0,0,0);

	m_statusBar->setStyleSheet("* {color:white; font-weight:500;font-family:Arial; font-size:8pt}");
	ui.frameStatusBar->setLayout(buttonLayout_status);
	ui.frameStatusBar->setStyleSheet("QFrame#frameStatusBar "+ThemeManager::GetSidebarChapter_Bkg());

	//ui.frameStatusBar->setVisible(false);


	setStyleSheet(ThemeManager::GetGlobalWidgetStyle());

	//B.T. set clock at once:
	UpdateClock();
}






void Sokrates_SideBar::OnMouse_DesktopEnter()
{
	if(g_pClientManager->IsLogged())
		SetMenuText(tr("Desktop"));
}

void Sokrates_SideBar::OnMouse_DesktopLeave()
{
	if(g_pClientManager->IsLogged())
		ChangeMainToolBarFocus(m_nToolBarButtonIDCurrentFocus,false);
}

void Sokrates_SideBar::OnMouse_ContactEnter()
{
	if(g_pClientManager->IsLogged())
		SetMenuText(tr("Contact"));
}

void Sokrates_SideBar::OnMouse_ContactLeave()
{
	if(g_pClientManager->IsLogged())
		ChangeMainToolBarFocus(m_nToolBarButtonIDCurrentFocus,false);
}

void Sokrates_SideBar::OnMouse_ProjectEnter()
{
	if(g_pClientManager->IsLogged())
		SetMenuText(tr("Project"));
}

void Sokrates_SideBar::OnMouse_ProjectLeave()
{
	if(g_pClientManager->IsLogged())
		ChangeMainToolBarFocus(m_nToolBarButtonIDCurrentFocus,false);
}

void Sokrates_SideBar::OnMouse_CalendarEnter()
{
	if(g_pClientManager->IsLogged())
		SetMenuText(tr("Calendar"));
}

void Sokrates_SideBar::OnMouse_CalendarLeave()
{
	if(g_pClientManager->IsLogged())
		ChangeMainToolBarFocus(m_nToolBarButtonIDCurrentFocus,false);
}


void Sokrates_SideBar::ChangeMainToolBarFocus(int nMenuButtonID, bool bCheckIfAlreadySet)
{
	if (nMenuButtonID==m_nToolBarButtonIDCurrentFocus && bCheckIfAlreadySet)
		return;

	m_nToolBarButtonIDCurrentFocus=nMenuButtonID;


	if(m_pBtnProject->isEnabled())m_pBtnProject->setIcon(":Projects_G.png");
	if(m_pBtnContact->isEnabled())m_pBtnContact->setIcon(":Contacts_G.png");
	if(m_pBtnComm->isEnabled())m_pBtnComm->setIcon(":Desktop_G.png");
	if(m_pBtnCalendar->isEnabled())m_pBtnCalendar->setIcon(":Calendar_G.png");


	switch(nMenuButtonID)
	{
	case 0:
		{
			m_pBtnComm->setIcon(":Desktop_C.png");
			SetMenuText(tr("Desktop"));
		}

		break;
	case 1:
		{
			m_pBtnContact->setIcon(":Contacts_C.png");
			SetMenuText(tr("Contact"));

		}
		break;
	case 2:
		{
			m_pBtnProject->setIcon(":Projects_C.png");
			SetMenuText(tr("Project"));
		}
		break;
	case 3:
		{
			m_pBtnCalendar->setIcon(":Calendar_C.png");
			SetMenuText(tr("Calendar"));
		}
		break;
	default:
		if (g_pClientManager->IsLogged())
			SetMenuText("");
		else
			SetMenuText(tr(""));//issue 1778
			//SetMenuText(tr("Login"));
	}
}
void Sokrates_SideBar::OnMenu_DesktopClick()
{
	if(!g_pClientManager->IsLogged())return;
	on_MenuButton_clicked(MENU_BUTTON_DESKTOP);
}


void Sokrates_SideBar::OnMenu_ContactClick()
{
	if(!g_pClientManager->IsLogged())return;
	on_MenuButton_clicked(MENU_BUTTON_CONTACT);

}


void Sokrates_SideBar::OnMenu_ProjectClick()
{
	if(!g_pClientManager->IsLogged())return;
	on_MenuButton_clicked(MENU_BUTTON_PROJECT);
}

void Sokrates_SideBar::OnMenu_CalendarClick()
{
	if(!g_pClientManager->IsLogged())return;
	on_MenuButton_clicked(MENU_BUTTON_CALENDAR);
}


void Sokrates_SideBar::on_MenuButton_clicked(int nMenuButtonID)
{
	bool bCTRL=false,bALT=false;
	Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();

	if(Qt::ControlModifier == (Qt::ControlModifier & keys))
		bCTRL = true;

	//remember if the Alt key was pressed
	if(Qt::AltModifier == (Qt::AltModifier & keys))
		bALT = true;


	if (bCTRL && bALT)
	{
		//OPEN SELECTOR (project & contact for now, el stupido did think about person...)
		switch(nMenuButtonID)
		{
		case MENU_BUTTON_CONTACT:
			SelectionPopupFloat::openPopUpSelector(ENTITY_BUS_CONTACT);
			break;
		case MENU_BUTTON_PROJECT:
			SelectionPopupFloat::openPopUpSelector(ENTITY_BUS_PROJECT);
			break;
		}
		return;
	}
	else if (bCTRL) //|| bALT)//ALT does not make any sense at all
	{
		switch(nMenuButtonID)
		{
		case MENU_BUTTON_DESKTOP:
			{
				if (!g_objFuiManager.IsInstanceAlive(MENU_COMM_CENTER) && !bCTRL) //if not alive and not in new win, try by views
					OpenDesktopFUIWithViews();
				else
					g_objFuiManager.OpenFUI(MENU_COMM_CENTER,bCTRL,bALT);
			}
			break;
		case MENU_BUTTON_CONTACT:
			{
				g_objFuiManager.OpenFUI(MENU_CONTACTS,bCTRL,bALT);
			}
			break;
		case MENU_BUTTON_PROJECT:
			{
				g_objFuiManager.OpenFUI(MENU_PROJECTS,bCTRL,bALT);
			}
			break;
		}
		return;
	}
	else
	{

		switch(nMenuButtonID)
		{
		case MENU_BUTTON_DESKTOP:
			{
				if (bCTRL || bALT)
				{
				}
				(static_cast<SideBarFui_Desktop*>(m_stackWidget->widget(0)))->SetCurrentFUI(true);
				(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->SetCurrentFUI(false);
				(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->SetCurrentFUI(false);
				m_stackWidget->setCurrentIndex(0);
				if (!m_stackWidget->isVisible())
					m_stackWidget->setVisible(true);
			}
			break;
		case MENU_BUTTON_CONTACT:
			{
				(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->SetCurrentFUI(true);
				(static_cast<SideBarFui_Desktop*>(m_stackWidget->widget(0)))->SetCurrentFUI(false);
				(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->SetCurrentFUI(false);

				m_stackWidget->setCurrentIndex(1);
				if (!m_stackWidget->isVisible())
					m_stackWidget->setVisible(true);
			}
			break;
		case MENU_BUTTON_PROJECT:
			{
				(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->SetCurrentFUI(true);
				(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->SetCurrentFUI(false);
				(static_cast<SideBarFui_Desktop*>(m_stackWidget->widget(0)))->SetCurrentFUI(false);
				m_stackWidget->setCurrentIndex(2);
				if (!m_stackWidget->isVisible())
					m_stackWidget->setVisible(true);
			}
			break;
		case MENU_BUTTON_CALENDAR:
			{
				//(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->SetCurrentFUI(false);
				//(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->SetCurrentFUI(false);
				//(static_cast<SideBarFui_Desktop*>(m_stackWidget->widget(0)))->SetCurrentFUI(false);
				//m_stackWidget->setCurrentIndex(2);
				if (!m_stackWidget->isVisible())
					m_stackWidget->setVisible(true);

				//open for logged user calendar:
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
			}
			break;
		}

	}

	ChangeMainToolBarFocus(nMenuButtonID);

}



void Sokrates_SideBar::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && event->y()<30) 
	{
		m_dragPosition = event->globalPos() - frameGeometry().topLeft();
		event->accept();
	}
	else if (event->button() == Qt::LeftButton && event->y()>(height()-16) && event->x()>(width()-16)) 
	{
		m_bResizeInProgress=true;
		event->accept();
		return;
	}
	else if (event->button() == Qt::LeftButton && event->y()>(height()-16) && event->x()>(width()/2-34) && event->x()<(width()/2+34))
	{
		m_bVertResizeInProgress=true;
		event->accept();
		return;
	}
	else
		m_dragPosition=QPoint(0,0);
	
	//QMainWindow::mousePressEvent(event); //pass event through
} 


void Sokrates_SideBar::mouseMoveEvent(QMouseEvent *event)
{
	QRect desktopRec = QApplication::desktop()->availableGeometry(this);
	bool bTwoScreenReversed = false;
	
	if(g_pClientManager->IsLogged()) //B.T.: error can occur if user is not logged and login dlg is moved
		bTwoScreenReversed = g_pSettings->GetPersonSetting(TWO_SCREEN_REVERSED).toBool();

	if(QApplication::desktop()->screenCount()==1) //only enable if scr cnt > 1
		bTwoScreenReversed=false;

	static int nStartX=this->geometry().x()+this->geometry().width();
	static bool bMoveInProgress=false;

	if (bMoveInProgress)
	{
		event->ignore();
		return;
	}

	if (event->buttons() & Qt::LeftButton && m_dragPosition!=QPoint(0,0) && m_bResizeInProgress==false && m_bVertResizeInProgress==false) 
	{
		//moving
		move(event->globalPos() - m_dragPosition);
		event->accept();

		//bool bSkipDocking = QApplication::desktop()->screenNumber(this) != QApplication::desktop()->screenCount()-1

		//docking
		int nRightCorner=this->geometry().x()+this->geometry().width();
		int nLeftCorner=this->geometry().x();

		//qDebug()<<"move, right corn:" <<nRightCorner;
		//qDebug()<<"move, desktop.width:" <<desktopRec.x()+desktopRec.width()-20;
		//qDebug()<<"move, nStartX:" <<nStartX;
		//docker:
		if (nRightCorner>(desktopRec.x()+desktopRec.width()-20) && nRightCorner>nStartX && nRightCorner<(desktopRec.x()+desktopRec.width()+4))
		{			
			if (!bTwoScreenReversed)
			{
				if (QApplication::desktop()->screenNumber(this)==QApplication::desktop()->screenCount()-1)
				{
						bMoveInProgress=true;
						MoveToSide(false,false);
						bMoveInProgress=false;
						m_dragPosition = event->globalPos() - frameGeometry().topLeft();

				}
			}
			else
			{
				if (QApplication::desktop()->screenNumber(this)==0)
				{
					bMoveInProgress=true;
					MoveToSide(false,false);
					bMoveInProgress=false;
					m_dragPosition = event->globalPos() - frameGeometry().topLeft();

				}
			}

		}
		else if (nLeftCorner<(desktopRec.x()+20) && nRightCorner<nStartX && (nLeftCorner>desktopRec.x()-4)) //RIGHT
		{
			if (!bTwoScreenReversed)
			{
				if (QApplication::desktop()->screenNumber(this)==0)
				{
					bMoveInProgress=true;
					MoveToSide(true,false);
					bMoveInProgress=false;
					m_dragPosition = event->globalPos() - frameGeometry().topLeft();
				}
			}
			else
			{
				if (QApplication::desktop()->screenNumber(this)==QApplication::desktop()->screenCount()-1)
				{
					bMoveInProgress=true;
					MoveToSide(true,false);
					bMoveInProgress=false;
					m_dragPosition = event->globalPos() - frameGeometry().topLeft();
				}
			}
		}
	}

	if (m_bResizeInProgress)
	{
		QPoint mousePos=event->globalPos();
		QPoint topleftPos=this->mapToGlobal(QPoint(0,0));
		if (mousePos.y()>(topleftPos.y()+3) && mousePos.x()>(topleftPos.x()+3))
		{
			resize(mousePos.x()-topleftPos.x(),mousePos.y()-topleftPos.y());
		}

		event->accept();
	}
	if (m_bVertResizeInProgress)
	{
		QPoint mousePos=event->globalPos();
		QPoint topleftPos=this->mapToGlobal(QPoint(0,0));
		if (mousePos.y()>(topleftPos.y()+3))
		{
			resize(width(),mousePos.y()-topleftPos.y());
		}

		event->accept();
	}
	
	nStartX=this->geometry().x()+this->geometry().width();

	//m_pSidebarHandleWnd->UpdatePosition();
} 

void Sokrates_SideBar::mouseReleaseEvent(QMouseEvent * event)
{
	if (m_bResizeInProgress)
	{
		m_bResizeInProgress=false;
		

		QPoint mousePos=event->globalPos();
		QPoint topleftPos=this->mapToGlobal(QPoint(0,0));
		if (mousePos.y()>topleftPos.y() && mousePos.x()>topleftPos.x())
		{
			resize(mousePos.x()-topleftPos.x(),mousePos.y()-topleftPos.y());
		}
		
		event->accept();
	}

	if (m_bVertResizeInProgress)
	{
		m_bVertResizeInProgress=false;
		QPoint mousePos=event->globalPos();
		QPoint topleftPos=this->mapToGlobal(QPoint(0,0));
		if (mousePos.y()>(topleftPos.y()+3))
		{
			resize(width(),mousePos.y()-topleftPos.y());
		}

		event->accept();
	}
}


void Sokrates_SideBar::OnHeader_HelpClick()
{

	//------------------------HELP----------------------------

		QMenu CnxtMenu(this);

		QAction *pActFUI_OnlineHelp= new QAction(tr("Online Help"),this);
		connect(pActFUI_OnlineHelp, SIGNAL(triggered()), this, SLOT(OnMenu_OnlineHelp()));
		CnxtMenu.addAction(pActFUI_OnlineHelp);

		QAction * pActFUI_HelpFirstStep= new QAction(tr("First Steps"),this);
		connect(pActFUI_HelpFirstStep, SIGNAL(triggered()), this, SLOT(OnMenu_HelpFirstStep()));
		CnxtMenu.addAction(pActFUI_HelpFirstStep);

		CnxtMenu.addSeparator();


		QAction * pActFUI_PersonalSettings= new QAction(tr("Personal Settings"),this);;
		connect(pActFUI_PersonalSettings, SIGNAL(triggered()), this, SLOT(OnMenu_PersonalSettings()));
		CnxtMenu.addAction(pActFUI_PersonalSettings);

		if (!g_pClientManager->IsLogged())
			pActFUI_PersonalSettings->setEnabled(false);

		//
		// "Portable" submenu
		//
#ifndef _DEBUG
		if(DataHelper::IsPortableVersion())
#endif
		{
			QAction* pMenuPortableSkypeInstall = new QAction(tr("Install Portable Skype(R)"),this);
			connect(pMenuPortableSkypeInstall, SIGNAL(triggered()), this, SLOT(OnMenu_PortableSkypeInstall()));

			QAction* pMenuPortableSkypeUpdate = new QAction(tr("Update Portable Skype(R)"),this);
			connect(pMenuPortableSkypeUpdate, SIGNAL(triggered()), this, SLOT(OnMenu_PortableSkypeUpdate()));
			if(!SkypeManager::IsSkypeInstalled())
				pMenuPortableSkypeUpdate->setEnabled(false);

			QAction* pMenuPortableAppRegister = new QAction(tr("Register Applications on Portable Memory Device"),this);
			connect(pMenuPortableAppRegister, SIGNAL(triggered()), this, SLOT(OnMenu_PortableAppRegister()));

			QMenu *menuPortable= new QMenu(tr("Portable"));
			menuPortable->addAction(pMenuPortableSkypeInstall);
			menuPortable->addAction(pMenuPortableSkypeUpdate);
			menuPortable->addAction(pMenuPortableAppRegister);
			CnxtMenu.addMenu(menuPortable);
		}

		CnxtMenu.addSeparator();

		QAction * pActFUI_About= new QAction(tr("About"),this);
		connect(pActFUI_About, SIGNAL(triggered()), this, SLOT(OnMenu_About()));
		CnxtMenu.addAction(pActFUI_About);


		CnxtMenu.exec(m_help->mapToGlobal(QPoint(0,0)));
}

void Sokrates_SideBar::OnHeader_HideClick()
{
	m_pSidebarHandleWnd->FlipState();
}

void Sokrates_SideBar::OnHeader_CloseClick()
{
	this->close();
}

void Sokrates_SideBar::OnHeader_SwitcherClick()
{
	OnSwitcher_Pro();
	//OnCustomContextMenu_Switcher(QPoint(0,0));
}



void Sokrates_SideBar::OnViewChanged()
{
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
	{
		if (!m_bWindowInitialized)
		{
			SetupButtons();
			MoveToSide(false,false,true);
			m_bWindowInitialized=true;
		}
		OnClientLogged();
		show(); 
	}
	else
		hide();

	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
	{
		m_pView_Light->setChecked(false);
		m_pView_SideBar->setChecked(true);
		m_pView_Pro->setChecked(false);
	}
	
}

void Sokrates_SideBar::OnCustomContextMenu_Switcher(const QPoint &pos)
{
	QMenu CnxtMenu(this);
	CnxtMenu.addAction(m_pView_Pro);
	//CnxtMenu.addAction(m_pView_Light);
	CnxtMenu.addAction(m_pView_SideBar);
	CnxtMenu.exec(m_switcher->mapToGlobal(pos));

}


void Sokrates_SideBar::OnSwitcher_Pro()
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	g_pGlobalObjectManager->ChangeView(ThemeManager::VIEW_PRO);
	QApplication::restoreOverrideCursor();
}
void Sokrates_SideBar::OnSwitcher_Light()
{
	g_pGlobalObjectManager->ChangeView(ThemeManager::VIEW_EASY);
}
void Sokrates_SideBar::OnSwitcher_Sidebar()
{
	g_pGlobalObjectManager->ChangeView(ThemeManager::VIEW_SIDEBAR);

}

void Sokrates_SideBar::closeEvent ( QCloseEvent * event )
{
	if(!g_objFuiManager.CloseAllFUIWindows(false))
	{
		event->ignore();
		return;
	}
	if (DocumentHelper::DWatcher_CheckForUnsavedDocuments())
	{
		event->ignore();
		QTimer::singleShot(0,this,SLOT(OnCheckInAllChangedDocuments()));
		return;
	}

	//if active downloads:
	if (g_DownloadManager->IsOperationInProgress())
	{
		int nResult = QMessageBox::question(this, "", tr("There are active download/upload operations. Do you want to terminate those operations and close application?"), tr("Yes"),tr("No"));
		if (nResult!=0)
		{
			event->ignore();
			return;
		}
	}

	g_pClientManager->Logout(); //try logout before destroy
	
	if (m_stackWidget){
		SideBarFui_Contact *pWnd = static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1));
		if(pWnd)
			pWnd->CloseChildWindows(); //close all childs
	}
	event->accept();

	//QWidget::closeEvent(event);
}

void Sokrates_SideBar::MoveToSide(bool bLeft,bool bSetY, bool bAdjustOnFirstShow)
{
	QRect desktopRec = QApplication::desktop()->availableGeometry(this);
/*
	if (QApplication::desktop()->screenCount()>1)
	{	
		int nCnt=QApplication::desktop()->screenCount();
		for (int i=0; i<nCnt;i++)
		{
			desktopRec.setWidth(desktopRec.width() + QApplication::desktop()->screenGeometry(i).width());
			desktopRec.setHeight(desktopRec.height() + QApplication::desktop()->screenGeometry(i).height());
			desktopRec.setX(desktopRec.x() + QApplication::desktop()->screenGeometry(i).x());
			desktopRec.setY(desktopRec.y() + QApplication::desktop()->screenGeometry(i).y());
		}
	}
	*/

	if (bAdjustOnFirstShow)	//load last position;
	{
		QPoint pos=this->mapToGlobal(QPoint(0,0));
		QString strPos;
		
		if (g_pClientManager->IsLogged())
			strPos=g_pSettings->GetPersonSetting(APPEAR_SIDEBAR_LAST_POSITION).toString();
		else
		{
			if (g_pClientManager->GetIniFile()->m_nLastX==0 && g_pClientManager->GetIniFile()->m_nLastY==0)
				strPos=QString("%1;%2;%3;%4").arg(desktopRec.x()+desktopRec.width()-this->width()-37).arg(desktopRec.y()).arg(width()).arg(height());
			else
				strPos=QString("%1;%2;%3;%4").arg(g_pClientManager->GetIniFile()->m_nLastX).arg(g_pClientManager->GetIniFile()->m_nLastY).arg(width()).arg(height());
		}

		if (!strPos.isEmpty())
		{
			QStringList list=strPos.split(";");
			if (list.size()>=2)
			{
				bool bOK1,bOK2;
				int nX=list.at(0).toInt(&bOK1);
				int nY=list.at(1).toInt(&bOK2);
				if (bOK1 && bOK2)
				{
					if (nX<desktopRec.x() || nX>(desktopRec.x()+desktopRec.width()-this->width()-37)) //if sb is leaving X
						nX=desktopRec.x()+desktopRec.width()-this->width()-37;

					if (nY<desktopRec.y() || nY>(desktopRec.y()+desktopRec.height()-this->height())) //if sb is leaving Y
						nY=desktopRec.y();

					this->move(nX,nY);
					return;
				}

			}
		}
	}


	if (bLeft)
	{
		int nY=this->geometry().top();
		if (bSetY)
			nY=desktopRec.y()+desktopRec.height()/2-this->height()/2;
		int nX=desktopRec.x();
		this->move(nX,nY);

	}
	else
	{
		//update move to right side (handler points to left)
		int nY=this->geometry().top();
		if (bSetY)
			nY=desktopRec.y()+desktopRec.height()/2-this->height()/2;
		int nX=desktopRec.x()+desktopRec.width()-this->width();
		int nActual=desktopRec.x()+this->geometry().x();
		this->move(nX,nY);
	}
}
/*
void Sokrates_SideBar::OnScreenGeometryChanged(int)
{
	desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
}
*/


void Sokrates_SideBar::resizeEvent ( QResizeEvent * event )
{
	//qDebug()<<"old size"<<event->oldSize().height();
	//qDebug()<<"new size"<<event->size().height();

	/*
	//avoid height to be > screen height:
	if ((desktopRec.y()+this->frameGeometry().top()+event->size().height())>(desktopRec.y()+desktopRec.height()))
	{
		//event->ignore();
		qDebug()<<"ignore event";
		OnResizeToMin();
		//QTimer::singleShot(0,this,SLOT(OnResizeToMin()));
		//return;
	}
	*/
	
	QWidget::resizeEvent(event);
}

void Sokrates_SideBar::OnResizedDelayed()
{

	//qDebug()<<"resize to "<<m_nWindowHeight;
	resize(width(),m_nWindowHeight);
	qApp->processEvents();

	/*
	QRect geo=geometry();
	
	qDebug()<<"win y:"<<geo.y();
	qDebug()<<"win h:"<<geo.height();

	geo.setY(geo.y()-5);
	geo.setHeight(m_nWindowHeight);
	setGeometry(geo);
	*/
}

//BT: fact: when u call resize or manually setVisible false/true, x,y are not changed
//x,y, height, width of window is only registered/changed in resizeEvent (resize is already done);
//this function is called from resizeEvent->give all sidebear elemnts to resize until its all done.
//prior to this function any subFUI that performs collapse/decollapse should emit signal ...chapter collapsed 
void Sokrates_SideBar::OnResizeToMin()
{
	//resize(width(),140);
	//qApp->processEvents();
	//return;

	
	int nFUIID=m_nResizeinProgressInFui_ID;
	int nChapterIdx=m_nResizeinProgressInFuiChapter_Idx;
	m_nResizeinProgressInFui_ID=-1;
	m_nResizeinProgressInFuiChapter_Idx=-1;

	//m_nWindowHeight=desktopRec.height()-desktopRec.y()-this->frameGeometry().top()-2; //new min height
	m_nWindowHeight=100;

	if (nFUIID==MENU_CONTACTS)
	{
		
		bool bCollapsed=(static_cast<SideBarFui_Base*>(m_stackWidget->widget(1)))->HideVisibleFramesIfNeeded(nChapterIdx);
		if (GUI_Helper::IsWidgetOutOfScreen(this) || m_bBothChaptersCollapsed)
		{
			resize(width(),height()+1); //induce resize...
			QTimer::singleShot(0,this,SLOT(OnResizedDelayed()));
			//qDebug()<<"reducing size, something collapsed";

			//resize(width(),140);
			//qApp->processEvents();

			//resize(width(),height()+1); //induce resize...
			//QTimer::singleShot(0,this,SLOT(OnResizedDelayed()));
		}
		
	}
	if (nFUIID==MENU_PROJECTS)
	{
		bool bCollapsed=(static_cast<SideBarFui_Base*>(m_stackWidget->widget(2)))->HideVisibleFramesIfNeeded(nChapterIdx);
		if (GUI_Helper::IsWidgetOutOfScreen(this) || m_bBothChaptersCollapsed)
		{
			resize(width(),height()+1); //induce resize...
			QTimer::singleShot(0,this,SLOT(OnResizedDelayed()));
			//qDebug()<<"reducing size, something collapsed";

			//resize(width(),140);
			//qApp->processEvents();
		}
		
	}
	if (nFUIID==MENU_COMM_CENTER)
	{
		bool bCollapsed=(static_cast<SideBarFui_Base*>(m_stackWidget->widget(0)))->HideVisibleFramesIfNeeded(nChapterIdx);
		if (GUI_Helper::IsWidgetOutOfScreen(this) || m_bBothChaptersCollapsed)
		{
			resize(width(),height()+1); //induce resize...
			QTimer::singleShot(0,this,SLOT(OnResizedDelayed()));
			//qDebug()<<"reducing size, something collapsed";

			//resize(width(),140);
			//qApp->processEvents();
		}
	}
	
}

void Sokrates_SideBar::OnChildChapterResized(int nFuiTypeID,int nChapterIdx,bool bBothChaptersCollapsed)
{
	//qDebug()<<"chapter signal detected, geometry updated";
	this->updateGeometry();
	m_nResizeinProgressInFui_ID=nFuiTypeID;
	m_nResizeinProgressInFuiChapter_Idx=nChapterIdx;
	m_bBothChaptersCollapsed=bBothChaptersCollapsed;
	//resize(280,140);
	//resize(width(),height()+1); //induce resize...
	QTimer::singleShot(0,this,SLOT(OnResizeToMin()));
}

void Sokrates_SideBar::OnProcessBeforeLogout()
{
	//save position;
	QPoint pos=this->mapToGlobal(QPoint(0,0));
	QString strPos=QString("%1;%2;%3;%4").arg(pos.x()).arg(pos.y()).arg(width()).arg(height());
	g_pSettings->SetPersonSetting(APPEAR_SIDEBAR_LAST_POSITION,strPos);
	g_pClientManager->GetIniFile()->m_nLastX=pos.x();
	g_pClientManager->GetIniFile()->m_nLastY=pos.y();
}


void Sokrates_SideBar::OnCommunicationInProgress(int nTicks,qint64 done, qint64 total)
{
	if (!this->isVisible())
		return;

	if (nTicks>0)
	{
		if (!m_labelWorkingIcon->isVisible())
			m_labelWorkingIcon->show();
		if (m_labelWorkingIcon->movie()->state()!=QMovie::Running)
			m_labelWorkingIcon->movie()->start();

		if (!m_ProgressBar->isVisible())m_ProgressBar->setVisible(true);
		nTicks=nTicks/4;
		int nOffset=nTicks/10;
		int nTicksProgress=nTicks-nOffset*10;
		m_ProgressBar->setValue(nTicksProgress);
		if (total>0 && done < total)
		{
			double speed=done/double(nTicks);
			QString strSpeed=DataHelper::GetFormatedFileSize((double)speed,2);
			m_statusBar->showMessage(QString("Transfer: %1/%2-%3/s").arg(DataHelper::GetFormatedFileSize((double)done,2)).arg(DataHelper::GetFormatedFileSize((double)total,2)).arg(strSpeed));
		}
		else
			m_statusBar->showMessage(QString("Transfer: %1s").arg(nTicks));
	}
	else
	{
		if (!m_ProgressBar->isVisible() )return;
		m_ProgressBar->reset();
		m_statusBar->clearMessage();
		m_ProgressBar->setVisible(false);
		m_statusBar->repaint();
		m_labelWorkingIcon->hide();
	}
}

void Sokrates_SideBar::onFrameChanged_WorkingIcon(int nFrame)
{
	if (nFrame==m_labelWorkingIcon->movie()->frameCount()-1)
	{
		m_labelWorkingIcon->movie()->jumpToFrame(0);
	}
}



//close all FUI's: no network traffic is allowed:
void Sokrates_SideBar::OnCommunicationFatalError(int nErrorCode)
{
	qDebug()<<"Fatal Error inside sidebar in Sokrates_SideBar::OnClientLogged(), thread: " <<ThreadIdentificator::GetCurrentThreadID();
	m_bLoginExecuted=false;
	if (m_bOnClientLoggedProcedureExecutionInProgress) //to prevent crash
	{
		g_pGlobalObjectManager->m_pwSidebar = NULL;
		deleteLater(); //to prevent crash
		QCoreApplication::quit();
		return;
	}
	
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR){
		//close();
		g_pGlobalObjectManager->m_pwSidebar = NULL;
		deleteLater(); //to prevent crash
		QCoreApplication::quit();
	}
	else
		OnClientLogout();

	return;
}


void Sokrates_SideBar::UpdateClock()
{
	//every sec:
	QString strClock=QDateTime::currentDateTime().toString("HH:mm dd.MM.yyyy");
	SetClockText(strClock);
}

void Sokrates_SideBar::timerEvent ( QTimerEvent * event ) 
{	
	if (event->timerId()==m_nTimer)
	{
		killTimer(m_nTimer);
		m_nTimer=startTimer(60000); //start now then every min
		UpdateClock();
	}
}



void Sokrates_SideBar::OpenDesktopFUIWithViews()
{
	Status err;
	DbRecordSet lstData;
	_SERVER_CALL(BusCommunication->GetDefaultDesktopFilterViews(err,g_pClientManager->GetPersonID(),lstData))
	_CHK_ERR(err);

	if (lstData.getRowCount()>0)
	{
		QSize size(300,100);
		QProgressDialog progress(tr("Preparing Desktop..."),tr("Cancel"),0,lstData.getRowCount(),this);
		progress.setWindowTitle(tr("Read In Progress"));
		progress.setMinimumSize(size);
		progress.setWindowModality(Qt::NonModal);
		progress.setMinimumDuration(1200);//2.4s before pops up

		int nFirstFUI;
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			progress.setValue(i);
			qApp->processEvents();
			if (progress.wasCanceled())
			{				progress.close();
				break;
			}
			int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_CENTER, false, true);//,FuiBase::MODE_EMPTY, -1,true);

			//Remember first fui for setting it active on startup.
			if (i == 0)
				nFirstFUI = nNewFUI;

			FUI_CommunicationCenter* pFui=dynamic_cast<FUI_CommunicationCenter*>(g_objFuiManager.GetFUIWidget(nNewFUI));
			if (pFui)
			{
				DbRecordSet row = lstData.getRow(i);
				pFui->DesktopStartupInitialization(row);
			}

		}

		g_objFuiManager.ActivateFUI(nFirstFUI);
	}
	else
	{
		g_objFuiManager.OpenFUI(MENU_COMM_CENTER, false, false);
	}
}



void Sokrates_SideBar::OnMenu_PersonalSettings()
{
	g_objFuiManager.OpenFUI(MENU_PERSONAL_SETTINGS,true);
}

void Sokrates_SideBar::OnMenu_OnlineHelp()
{
	QString strHelp;
	if(g_pClientManager->GetIniFile()->m_strLangCode == "de"){
		strHelp = "http://www.sokrates-communicator.com/de/help/";
	}
	else{
		strHelp = "http://www.sokrates-communicator.com/en/help/";
	}

	QDesktopServices::openUrl(strHelp); //BT improved;
}



void Sokrates_SideBar::OnMenu_HelpFirstStep()
{

	QString strHelp;
	if(g_pClientManager->GetIniFile()->m_strLangCode == "de")
	{
		strHelp = "http://www.sokrates-communicator.com/de/firststeps/index.html";
	}
	else
	{
		strHelp = "http://www.sokrates-communicator.com/en/firststeps/index.html";
	}

	QDesktopServices::openUrl(strHelp); //BT improved;

}


void Sokrates_SideBar::OnMenu_About()
{
	int nSN;
	QString strUser,strRep1,strRep2;
	QString strCustomerID;
	int nCustomerSolutionID;
	g_FunctionPoint.GetLicenseInfo(strUser, nSN, strRep1, strRep2,strCustomerID,nCustomerSolutionID);

	AboutWidget dlg(this,g_pClientManager->GetApplicationDisplayName(),g_pClientManager->GetModuleCode(),strUser,nSN,strRep1,strRep2);
	dlg.exec();
}

void Sokrates_SideBar::OnLoginDialogCreated(QDialog *pDlg)
{
	if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
		return;
	//pDlg->setStyleSheet("QDialog "+ThemeManager::GetSidebar_Bkg());
	this->layout()->itemAt(2)->widget()->setVisible(false); //splash frame is in-visible
	(dynamic_cast<QVBoxLayout*>(this->layout()))->insertWidget(2,pDlg);

}
void Sokrates_SideBar::OnLoginDialogBeforeDestroy(QDialog *pDlg)
{
	if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
		return;
	this->layout()->removeWidget(pDlg);
	this->layout()->itemAt(2)->widget()->setVisible(true); //splash frame is visible
}


void Sokrates_SideBar::OnThemeChanged()
{
	if(m_nCurrentWindowTheme==ThemeManager::GetCurrentThemeID()) return;

	if (m_pInfo)
	{
		QString strText=m_pInfo->property("ActualText").toString();
		SetMenuText(strText);
	}
	ui.frameToolBar->setStyleSheet(ThemeManager::GetToolbarBkgStyle());
	QString	styleNew=ThemeManager::GetToolbarButtonStyle();
	if(m_pBtnComm)m_pBtnComm->setStyleSheet(styleNew);
	if(m_pBtnContact)m_pBtnContact->setStyleSheet(styleNew);
	if(m_pBtnProject)m_pBtnProject->setStyleSheet(styleNew);
	if(m_pBtnCalendar)m_pBtnCalendar->setStyleSheet(styleNew);

	ui.frameBannerLeft->setStyleSheet(ThemeManager::GetSidebar_HeaderLeft());
	ui.frameBannerCenter->setStyleSheet(ThemeManager::GetSidebar_HeaderCenter());
	ui.frameBannerRight->setStyleSheet(ThemeManager::GetSidebar_HeaderRight());
	ui.frameFooterLeft->setStyleSheet(ThemeManager::GetSidebar_FooterLeft());
	ui.frameFooterCenter->setStyleSheet(ThemeManager::GetSidebar_FooterCenter());
	ui.frameFooterRight->setStyleSheet(ThemeManager::GetSidebar_FooterRight());
	ui.frameSplash->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg());
	ui.frameStatusBar->setStyleSheet("QFrame#frameStatusBar "+ThemeManager::GetSidebarChapter_Bkg());
	
	m_nCurrentWindowTheme=ThemeManager::GetCurrentThemeID();
}


void Sokrates_SideBar::OnMenu_PortableSkypeInstall()
{
	int nResult = QMessageBox::question(this, "", tr("Copy the actual Skype(R) installation from hard disk to your portable memory device. This can take a long time. Continue?"), tr("Yes"),tr("No"));
	if (nResult==0)
	{
		g_pGlobalObjectManager->CopyPortableSkype();
	}
}

void Sokrates_SideBar::OnMenu_PortableSkypeUpdate()
{
	int nResult = QMessageBox::question(this, "", tr("Do you want to update your Skype(R) installation on your portable memory device using your Skype(R) installation on the hard disk?"), tr("Yes"),tr("No"));
	if (nResult==0)
	{
		g_pGlobalObjectManager->UpdatePortableSkype();
	}
}

void Sokrates_SideBar::OnMenu_PortableAppRegister()
{
	Dlg_RFParser Dlg;
	Status err;
	if(Dlg.Initialize(err))
	{
		_CHK_ERR(err);
		if(Dlg.exec())
		{
			QString strPath;
			DbRecordSet lstApps,lstTemplates,lstInterfaces;
			Dlg.GetResult(lstApps,lstTemplates,lstInterfaces,strPath);
			QFileInfo filek(strPath);
			DocumentHelper::RegisterApplicationsAndTemplatesFromRFFile(lstApps,lstTemplates,lstInterfaces,filek.absolutePath());
		}

	}

}


void Sokrates_SideBar::AdjustSizeOnLogon()
{
	QRect desktopRec = QApplication::desktop()->availableGeometry(this);
	QString strPos=g_pSettings->GetPersonSetting(APPEAR_SIDEBAR_LAST_POSITION).toString();
	QStringList list=strPos.split(";");
	if (list.size()==4) //issue 1845
	{
		bool bOK1,bOK2;
		int nWidth=list.at(2).toInt(&bOK1);
		int nHeight=list.at(3).toInt(&bOK2);
		if (bOK1 && bOK2)
		{
			QPoint pos=this->mapToGlobal(QPoint(0,0));
			if (pos.x()+nWidth>desktopRec.width()) //if width > X
				nWidth=desktopRec.width()-pos.x();

			if (pos.y()+nHeight>desktopRec.height()) //if width > X
				nHeight=desktopRec.height()-pos.y();

			this->resize(nWidth,nHeight);
			//qApp->processEvents();
		}
	}
}
void Sokrates_SideBar::OnCheckInAllChangedDocuments()
{
	DocumentHelper::DWatcher_SaveChanges(true);
}


//issue 2246: create shortcuts:
void Sokrates_SideBar::keyPressEvent ( QKeyEvent * event ) 
{
	if(!g_pClientManager->IsLogged())return;

	switch(event->key())
	{
	case Qt::Key_F1: // Support/Publish Screen for Support (-> Netviewer)
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier)
			{
				OnMenu_Support();
			}
			else
			{
				OnMenu_OnlineHelp();
			}
			return;
		}
		break;
	case Qt::Key_F2: //Opens the Contact FUI (Pro or Sidebar) and opens the Details Tab (Pro View) or expands the actual contact area and shows the Quick Info tab. If no actual contact is loaded, the currently selected contact is loaded first.
		{
			event->accept();
			OnHeader_SwitcherClick();
			return;
		}
		break;
	case Qt::Key_F3://ALT-F3: Switches between "Contacts" and "Favorites" (Contacts FUI only)
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier)
			{
				if ((static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->FlipSelectorToFavoriteTabOrList();
				}
			}
			else
			{
				if((static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->ShowActualFrameDetails(0);
				}
				else if ((static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->ShowActualFrameDetails(0);
				}
			}
			//else
			//	PassKeyEventToCommmenu(event);
			return;
		}
		break;
	case Qt::Key_F5://First icon in information toolbar --> Quick Info
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier)
			{
				if((static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->ShowActualFrameDetails(0);
				}
				else if ((static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->ShowActualFrameDetails(0);
				}
			}
			else if (event->modifiers() & Qt::ControlModifier)
			{
				PassKeyEventToCommmenu(event);
			}
			else
			{
				OnMenu_DesktopClick();
			}
			
			return;
		}
		break;
	case Qt::Key_F6://ALT-F6: Second icon in information toolbar --> CE Grid
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier)
			{
				if((static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->ShowActualFrameDetails(1);
				}
				else if ((static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->ShowActualFrameDetails(1);
				}
			}
			else if (event->modifiers() & Qt::ControlModifier)
			{
				PassKeyEventToCommmenu(event);
			}
			else
			{
				OnMenu_ContactClick();
			}
				
			return;
		}
		break;
	case Qt::Key_F7://ALT-F7: Third icon in information toolbar --> Assigned Contacts
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier)
			{
				if((static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->ShowActualFrameDetails(2);
				}
				else if ((static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->ShowActualFrameDetails(2);
				}
			}
			else if (event->modifiers() & Qt::ControlModifier)
			{
				PassKeyEventToCommmenu(event);
			}
			else
			{
				OnMenu_ProjectClick();
			}
			return;
		}
		break;
	case Qt::Key_F8://ALT-F8: Fourth icon in information toolbar --> Assigned Projcts (Contact FUI) or Calendar (Project FUI)
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier)
			{
				if((static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->OnDetail_CalendarClick();
				}
				else if ((static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->ShowActualFrameDetails(3);
				}
			}
			else if (event->modifiers() & Qt::ControlModifier)
			{
				PassKeyEventToCommmenu(event);
			}
			else
			{
				OnMenu_CalendarClick();
			}
		
			return;
		}
		break;
		
	case Qt::Key_F9://ALT-F9: Fifth icon in information toolbar --> Calendar (Contact FUI)
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier)
			{
				if ((static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->IsCurrentFUI())
				{
					(static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->OnDetail_CalendarClick();
				}
			}
			else
				PassKeyEventToCommmenu(event);
				
			return;
		}
		break;
	case Qt::Key_F10: 
	case Qt::Key_F11:
	case Qt::Key_F12:
		{
			if (PassKeyEventToCommmenu(event))
				return;
		}
		break;

	}

	QWidget::keyPressEvent(event);
}


void Sokrates_SideBar::OnMenu_Support()
{
	//verify if Netviewer exists
	QString strExe = QApplication::applicationDirPath();
	strExe += "\\support\\NV_support.exe";

	if(!QFile::exists(strExe)){
		QMessageBox::information(NULL, tr("Error"), tr("NetViewer client has not been found!"));
		return;
	}

	NetviewerDlg dlg;
	int nRes = dlg.exec();
	if(nRes)
	{
		//start NetViewer client
		//int nResult=QMessageBox::question(NULL, tr("Question"), tr("Do you want to share your monitor view with a support specialist? Ask him for a session number!"),tr("Yes"),tr("No"));
		//if (nResult==0)
		{
			//append param
			QString strParam = "-sinr:";
			strParam += dlg.GetSessionNum();

			QStringList lstArgs;
			lstArgs << strParam;
			QProcess::startDetached(strExe, lstArgs);
		}
	}
}


//true event catched, else not
bool Sokrates_SideBar::PassKeyEventToCommmenu(QKeyEvent * event)
{
	if((static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->IsCurrentFUI())
	{
		return (static_cast<SideBarFui_Project*>(m_stackWidget->widget(2)))->ProcessKeyPressEventFromMainWindow(event);
	}
	else if ((static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->IsCurrentFUI())
	{
		return (static_cast<SideBarFui_Contact*>(m_stackWidget->widget(1)))->ProcessKeyPressEventFromMainWindow(event);
	}
	else if ((static_cast<SideBarFui_Desktop*>(m_stackWidget->widget(0)))->IsCurrentFUI())
	{
		return (static_cast<SideBarFui_Desktop*>(m_stackWidget->widget(0)))->ProcessKeyPressEventFromMainWindow(event);
	}

	return false;
}