#include "serverinifile.h"
#include "common/common/logger.h"
#include "common/common/config_version_ffh.h"

ServerIniFile::ServerIniFile()
{
}

ServerIniFile::~ServerIniFile()
{
}

void ServerIniFile::Clear()
{
	//m_lstConnections.clear();
}

//loads, creates if not exists, cheks values
bool ServerIniFile::Load(QString strFile, bool bCreateIfNotExists)
{
	Clear();

	if(!QFileInfo(strFile).exists()&& bCreateIfNotExists)
		{return SaveDefaults(strFile);}

	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	// read INI values
	m_objIni.GetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.GetValue("Main", "AppServerPort",		m_nAppServerPort, 55000);
	m_objIni.GetValue("Main", "AppServerPortEnd",	m_nAppServerPortRangeEnd, 55000);
	m_objIni.GetValue("Main", "AppServerPortActual",	m_nAppServerPortActual, 55000);
	m_objIni.GetValue("Main", "MaxConnections",		m_nMaxConnections, 100);
	m_objIni.GetValue("Main", "Service Descriptor",	m_strServiceDescriptor);
	m_objIni.GetValue("Main", "AutoStartService",	m_nAutoStartService,1);
	m_objIni.GetValue("Main", "Enable UPNP",		m_nEnableUpnp);
	m_objIni.GetValue("Main", "Enable NAT-PMP",		m_nEnableNAT);
	m_objIni.GetValue("Main", "Unique ID",			m_strAppServerID);
	m_objIni.GetValue("Main", "Outer IP Address",	m_strOuterIP);
	m_objIni.GetValue("Main", "LanguageCode",		m_strLangCode);
	m_objIni.GetValue("Main", "Force Public IP",	m_nForcePublicIP);
	m_objIni.GetValue("Main", "Forward Method",		m_nPortForwardMethod);
	m_objIni.GetValue("Main", "EnablePermanentDelete",		m_nEnablePermanentDelete,0);
	m_objIni.GetValue("Main", "Disable Settings Page",		m_nDisableSettingPage,0);

	m_objIni.GetValue("Internal", "Asked For User",		m_nAskedForUser,0);
	m_objIni.GetValue("Internal", "Asked For Email",	m_nAskedForEmail,0);
	m_objIni.GetValue("Internal", "Asked For NewDir",	m_nAskedForNewDir,0);
	m_objIni.GetValue("Internal", "WebDocumentsPath",	m_strWebDocumentsPath);
	m_objIni.GetValue("Internal", "LastErrorCode",		m_nLastErrorCode);
	m_objIni.GetValue("Internal", "LastErrorText",		m_strLastErrorText);
	m_objIni.GetValue("Internal", "WebDataVersion",		m_strWebAppVersion);
	m_objIni.GetValue("Internal", "ServerTestedConnection",		m_nServerTestedConnection,0);

	m_objIni.GetValue("Thumbs", "JPEG Quality (30-100)",	m_nJPEGQuality);
	m_objIni.GetValue("Thumbs", "Thumbnail directory",		m_strThumbDir);
	m_objIni.GetValue("Thumbs", "Catalog directory",		m_strCatalogDir);

	
	//m_objIni.GetValue("Main", "First Start",		m_nFirstStart,1);
	
	m_objIni.GetValue("SSL", "SSLMode",			m_nSSLMode, 0);
	m_objIni.GetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);

	m_objIni.GetValue("Logging", "Log Level",												m_nLogLevel,Logger::LOG_LEVEL_DEBUG);
	m_objIni.GetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize,10);
	m_objIni.GetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize,0);
	
	/*
	m_objIni.GetValue("Backup", "Backup Frequency",		m_nBackupFreq,2);
	m_objIni.GetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.GetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.GetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.GetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.GetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
	*/

	return CheckValues();
}

bool ServerIniFile::Save(QString strFile)
{
	m_objIni.SetPath(strFile);

	//fill the data inside
	m_objIni.RemoveSection("Main");		//cleanup existing data
	m_objIni.RemoveSection("Logging");	
	//m_objIni.RemoveSection("Backup");	
	m_objIni.RemoveSection("SSL");		

	// write INI values
	m_objIni.SetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.SetValue("Main", "AppServerPort",		m_nAppServerPort);
	m_objIni.SetValue("Main", "AppServerPortEnd",	m_nAppServerPortRangeEnd);
	m_objIni.SetValue("Main", "AppServerPortActual",	m_nAppServerPortActual);

	m_objIni.SetValue("Main", "MaxConnections",		m_nMaxConnections);
	m_objIni.SetValue("Main", "Service Descriptor",	m_strServiceDescriptor);
	m_objIni.SetValue("Main", "AutoStartService",	m_nAutoStartService);
	m_objIni.SetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.SetValue("Main", "Enable UPNP",		m_nEnableUpnp);
	m_objIni.SetValue("Main", "Enable NAT-PMP",		m_nEnableNAT);
	m_objIni.SetValue("Main", "Unique ID",			m_strAppServerID);
	m_objIni.SetValue("Main", "Outer IP Address",	m_strOuterIP);
	m_objIni.SetValue("Main", "LanguageCode",		m_strLangCode);
	m_objIni.SetValue("Main", "Force Public IP",	m_nForcePublicIP);
	m_objIni.SetValue("Main", "Forward Method",		m_nPortForwardMethod);
	m_objIni.SetValue("Main", "EnablePermanentDelete",		m_nEnablePermanentDelete);
	m_objIni.SetValue("Main", "Disable Settings Page",		m_nDisableSettingPage);

	m_objIni.SetValue("Internal", "Asked For User",		m_nAskedForUser);
	m_objIni.SetValue("Internal", "Asked For Email",	m_nAskedForEmail);
	m_objIni.SetValue("Internal", "Asked For NewDir",	m_nAskedForNewDir);
	m_objIni.SetValue("Internal", "WebDocumentsPath",	m_strWebDocumentsPath);
	m_objIni.SetValue("Internal", "LastErrorCode",		m_nLastErrorCode);
	m_objIni.SetValue("Internal", "LastErrorText",		m_strLastErrorText);
	m_objIni.SetValue("Internal", "WebDataVersion",		m_strWebAppVersion);
	m_objIni.SetValue("Internal", "ServerTestedConnection",		m_nServerTestedConnection);


	m_objIni.SetValue("Thumbs", "JPEG Quality (30-100)",	m_nJPEGQuality);
	m_objIni.SetValue("Thumbs", "Thumbnail directory",		m_strThumbDir);
	m_objIni.SetValue("Thumbs", "Catalog directory",		m_strCatalogDir);


	m_objIni.SetValue("SSL", "SSLMode",				m_nSSLMode);
	m_objIni.SetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);

	m_objIni.SetValue("Logging", "Log Level",												m_nLogLevel);
	m_objIni.SetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize);
	m_objIni.SetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize);
/*
	m_objIni.SetValue("Backup", "Backup Frequency",		m_nBackupFreq);
	m_objIni.SetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.SetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.SetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.SetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.SetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
*/
	return m_objIni.Save();
}



bool ServerIniFile::CheckValues()
{
	if(m_nMaxConnections<=0) return false;
	if(m_nSSLMode<0 || m_nSSLMode>1) return false;
	if(m_nAutoStartService<0 || m_nAutoStartService>1) return false;
	if(m_nAppServerPort<0 || m_nAppServerPort>65536) return false;
	if(m_nAppServerPortRangeEnd<0 || m_nAppServerPortRangeEnd>65536) return false;

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("ServerIniFile::CheckValues 2"));

	//MB: if update then set to right value:
	if(m_nAppServerPortRangeEnd<m_nAppServerPort)
	{
		m_nAppServerPortRangeEnd=m_nAppServerPort; //+1000; disable port hoping on default
		if (m_nAppServerPortRangeEnd>65536)
			m_nAppServerPortRangeEnd=65536;
	}

	if(m_nLogBufferSize<0) return false;

	//reset to default log level:
	if(m_nLogLevel<0)m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	if(m_nLogLevel>Logger::LOG_LEVEL_DEBUG)m_nLogLevel=Logger::LOG_LEVEL_DEBUG;

	return true;
}



bool ServerIniFile::SaveDefaults(QString strFile)
{
	m_strAppServerIPAddress="0.0.0.0";
	m_nAppServerPort=55000;
	m_nAppServerPortRangeEnd=55000;
	m_nAppServerPortActual=55000;

	m_nMaxConnections=1000;
	m_nSSLMode=0;
	m_nAutoStartService=1;
	m_nLastErrorCode=0;

	m_nLogLevel=Logger::LOG_LEVEL_DEBUG;
	m_nLogMaxSize=10;				
	m_nLogBufferSize=0;	
	m_nEnableUpnp=1;
	m_nEnableNAT=1;
	m_nForcePublicIP=0;
	m_strLangCode = "en";
	m_nPortForwardMethod=0;
	m_nJPEGQuality = 65;
	m_nServerTestedConnection=0;
	m_strWebAppVersion=QString::number(JAMES_PROGRAM_VERSION_MAJOR)+"."+QString::number(JAMES_PROGRAM_VERSION_MINOR)+"."+QString::number(JAMES_PROGRAM_VERSION_REVISION);
	m_nEnablePermanentDelete=0;
	m_nDisableSettingPage = 0;
	m_nAskedForUser=0;
	m_nAskedForEmail=0;
	m_nAskedForNewDir=0;

	return Save(strFile);
}
