#include "filecruncher.h"
#include <QProcess>
#include <QFileInfoList>
#include <QDir>
#include "common/common/encryptedinifile.h"
#include "common/common/zipcompression.h"
#include "common/common/datahelper.h"
//#include "common/common/logger.h"
#include "iphotohandler.h"

#define STATUS_OK 0
#define STATUS_INDEXING 1
#define STATUS_NOT_INDEXED 2

#ifdef _WIN32
#include <shlobj.h>
#endif



QStringList FileCruncher::s_lstPicExt=QStringList();

#define RAW_EXTENSIONS "cr2;crw;pef;dng;raw;raf;3fr;dcr;dcs;kdc;mef;nef;orf;rw2;x3f;srf;sr2;arw"
#define BINARY_DELIMITER ";----------file"

//lstPicturePaths = <PATH,NAME,XT>
void FileCruncher::CreateThumbsFromFiles(DbRecordSet &lstPicturePaths, QString strOutFolder, QString strSuffix_Ext, int nWidth, int nHeight, bool bKeepAspect)
{
	QProcess gentool;

	QString strPicsExt=GetFileTypesBasedOnCode(FILE_TYPE_PIC);

	//call gentool.exe for each-> load into memory, delete output file
	int nSize=lstPicturePaths.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (!strPicsExt.contains(lstPicturePaths.getDataRef(i,"EXT").toString().toLower()))
			continue;

		QString strInFilePath=lstPicturePaths.getDataRef(i,"PATH").toString();
		ReplaceNetworkPathWithUNC(strInFilePath);
		QString strOutFilePath=strOutFolder+"/"+lstPicturePaths.getDataRef(i,"NAME").toString()+strSuffix_Ext+".jpg"; //all to jpg

		QStringList lstArgs;
		lstArgs<<"-f";
		lstArgs<<"pic";
		lstArgs<<"-i";
		lstArgs<<strInFilePath;
		lstArgs<<"-o";
		lstArgs<<strOutFilePath;
		lstArgs<<"-w";
		lstArgs<<QString::number(nWidth);
		lstArgs<<"-h";
		lstArgs<<QString::number(nHeight);
		if (bKeepAspect)
			lstArgs<<"-a";

		gentool.start("gentool",lstArgs);
		if (!gentool.waitForFinished(10000)) //wait 10sec
		{		
			//lstPicturePaths.setData(i,"STATUS_TEXT","Failed to execute gentool!");
			continue;
		}
		int nExitCode=gentool.exitCode();
		QByteArray errContent=gentool.readAllStandardError();
		if (errContent.size()!=0)
		{
			//lstPicturePaths.setData(i,"STATUS_TEXT",QString(errContent.left(4095)));
			continue;
		}
	}
}


//get files: ifbUseRelativeFilePath then PATH will contain only relative path+filename to strFolderPath, else full path with filename
bool FileCruncher::GetFilesFromFolder(QString strFolderPath,DbRecordSet &lstPaths, DbRecordSet &lstSubDirs, QString strAllowedExtension,QString strFileSuffixFilter)
{
	lstPaths.destroy();
	lstSubDirs.destroy();
	lstPaths.addColumn(QVariant::String,"PATH");
	lstPaths.addColumn(QVariant::String,"NAME");
	lstPaths.addColumn(QVariant::String,"EXT");
	lstPaths.addColumn(QVariant::String,"FILENAME");
	lstPaths.addColumn(QVariant::DateTime,"LAST_MODIFY");
	lstPaths.addColumn(QVariant::Int,"SIZE");

	lstSubDirs.addColumn(QVariant::String,"PATH");
	lstSubDirs.addColumn(QVariant::String,"NAME");


	QDir dir(strFolderPath);
	if (!dir.exists())
		return false;

	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isDir())
		{
			//skip current and parent directory:
			if (lstFiles.at(i).fileName()==".") continue;
			if (lstFiles.at(i).fileName()=="..") continue;

			lstSubDirs.addRow();
			lstSubDirs.setData(lstSubDirs.getRowCount()-1,"PATH",lstFiles.at(i).absoluteFilePath());
			lstSubDirs.setData(lstSubDirs.getRowCount()-1,"NAME",lstFiles.at(i).completeBaseName());
		}
		else
		{
			//Get all files from folder if list is empty or filter 'em
			if (!strAllowedExtension.isEmpty())
			{
				if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
					continue;
			}
			if (!strFileSuffixFilter.isEmpty())
			{
				if (lstFiles.at(i).completeBaseName().right(strFileSuffixFilter.length())!=strFileSuffixFilter)
					continue;
			}
			lstPaths.addRow();
			lstPaths.setData(lstPaths.getRowCount()-1,0,lstFiles.at(i).absoluteFilePath());
			lstPaths.setData(lstPaths.getRowCount()-1,1,lstFiles.at(i).completeBaseName());
			lstPaths.setData(lstPaths.getRowCount()-1,2,lstFiles.at(i).suffix());
			lstPaths.setData(lstPaths.getRowCount()-1,3,lstFiles.at(i).fileName());
			lstPaths.setData(lstPaths.getRowCount()-1,4,lstFiles.at(i).lastModified());
			lstPaths.setData(lstPaths.getRowCount()-1,5,(int)lstFiles.at(i).size());
		}
	}

	lstPaths.sort(lstPaths.getColumnIdx("NAME"));

	return true;
}

int FileCruncher::GetFileCountFromFolder(QString strFolderPath,QString strAllowedExtension)
{
	int nCnt=0;
	QDir dir(strFolderPath);
	if (!dir.exists())
		return 0;

	QFileInfoList	lstFiles=dir.entryInfoList(QDir::Files,QDir::Name);
	//return lstFiles.size();
	int nSize=lstFiles.size();
	QString strNamePrev="";
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isFile())
		{
			if (strNamePrev==lstFiles.at(i).completeBaseName())
				continue; //skip if same name

			//Get all files from folder if list is empty or filter 'em
			if (!strAllowedExtension.isEmpty())
			{
				if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
					continue;
				//quick fix for raw thumbs:
				if (lstFiles.at(i).completeSuffix()=="thumb.jpg" || lstFiles.at(i).completeSuffix()=="thumb.ppm")
					continue;
			}
			nCnt++;
		}

		strNamePrev = lstFiles.at(i).completeBaseName();
	}
	return nCnt;
}

QString	FileCruncher::GetFirstFileFromFolder(QString strFolderPath,QString strAllowedExtension)
{
	QDir dir(strFolderPath);
	if (!dir.exists())
		return "";

	QFileInfoList lstFiles=dir.entryInfoList(QDir::Files |QDir::Readable,QDir::Name);
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		//Get all files from folder if list is empty or filter 'em
		if (!strAllowedExtension.isEmpty())
		{
			if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
				continue;
		}
		return lstFiles.at(i).absoluteFilePath();
	}
	return "";
}

//created is more accurate as it's updated in time of copy/move
QDateTime FileCruncher::GetLastModifyDateFromFolder(QString strFolderPath,QString strAllowedExtension)
{
	QDir dir(strFolderPath);
	if (!dir.exists())
		return QDateTime();

	QFileInfoList lstFiles=dir.entryInfoList(QDir::Files | QDir::Readable);
	int nSize=lstFiles.size();
	QDateTime datMax;
	if (nSize>0)
		datMax =lstFiles.at(0).created();
	else
		return datMax;

	for(int i=0;i<nSize;++i)
	{
			//Get all files from folder if list is empty or filter 'em
			if (!strAllowedExtension.isEmpty())
			{
				if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
					continue;
			}
			if(lstFiles.at(i).created()>datMax)
				datMax=lstFiles.at(i).created();
	}

	return datMax; 
}



QString	FileCruncher::GetFileTypesBasedOnCode(int nFileTypesCode, bool bIncludeRawFormats)
{
	switch (nFileTypesCode)
	{
	case FILE_TYPE_ALL:
			 return "";
	case FILE_TYPE_PIC:
		{
			QString strExt="bmp;gif;ico;jpeg;jpg;mng;pbm;pgm;png;ppm;svg;tif;tiff;xbm;xpm;";
			if (bIncludeRawFormats)
				return strExt+RAW_EXTENSIONS;
			else
				return strExt;
		}
	case FILE_TYPE_OFFICE:
			return "pdf;doc;docx;xls;xlslx;ppt;pptx";
	default:
			return "";
	}
}

QStringList FileCruncher::GetPicFileTypesNameFilterFormat()
{
	if (s_lstPicExt.count()!=0)
		return s_lstPicExt;

	s_lstPicExt<<"*.bmp"<<"*.gif"<<"*.ico"<<"*.jpeg"<<"*.jpg"<<"*.mng"<<"*.pbm"<<"*.pgm"<<"*.png"<<"*.ppm"<<"*.svg"<<"*.tif"<<"*.tiff"<<"*.xbm"<<"*.xpm;";
	s_lstPicExt<<"*.cr2"<<"*.crw"<<"*.pef"<<"*.dng"<<"*.raw"<<"*.raf"<<"*.3fr"<<"*.dcr"<<"*.dcs"<<"*.kdc"<<"*.mef"<<"*.nef"<<"*.orf"<<"*.rw2"<<"*.x3f;"<<"*.srf;"<<"*.sr2;"<<"*.arw;";

	return s_lstPicExt;
}

QStringList FileCruncher::CreateFiltersFromExtensionString(QString strAllowedExtension)
{
	QStringList lstFilter;
	QStringList lstExt=strAllowedExtension.split(";");
	int nSize=lstExt.size();
	for (int i=0;i<nSize;i++)
	{
		lstFilter.append("*."+lstExt.at(i));
	}

	return lstFilter;
}


bool FileCruncher::IsRAWPicture(QString strExt)
{
	if (QString(RAW_EXTENSIONS).indexOf(strExt.toLower())>=0) //test if raw picture
	{
		return true;
	}
	else
	{
		return false;
	}
}


//get files: ifbUseRelativeFilePath then PATH will contain only relative path+filename to strFolderPath, else full path with filename
void FileCruncher::GetAllSubFolders(QString strParentDir,QList<QString> &lstSubDirs)
{
	QDir dir(strParentDir);
	if (!dir.exists())
		return;

	QFileInfoList lstFiles=dir.entryInfoList(QDir::AllDirs);
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		//skip current and parent directory:
		if (lstFiles.at(i).fileName()==".") continue;
		if (lstFiles.at(i).fileName()=="..") continue;

		QString strDirPath=lstFiles.at(i).absoluteFilePath();
		QString strDirName=lstFiles.at(i).completeBaseName();
		QList<QString> lstSubDirs_Nest;
		GetAllSubFolders(strDirPath,lstSubDirs_Nest);

		lstSubDirs.append(strDirName);
		int nSize2=lstSubDirs_Nest.size();
		for(int k=0;k<nSize2;++k)
		{
			lstSubDirs.append(strDirName+"/"+lstSubDirs_Nest.at(k));
		}
	}
}



bool FileCruncher::RemoveAllFromDirectory(QString strFolderPath, bool bRecursiveFolders, bool bRemoveDirectory)
{
	QDir dir(strFolderPath);
	if (!dir.exists())
		return false;

	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isDir())
		{
			//skip current and parent directory:
			if (lstFiles.at(i).fileName()==".") continue;
			if (lstFiles.at(i).fileName()=="..") continue;

			//Get all files from folder:
			bool bOK=true;
			if (bRecursiveFolders)
				bOK=RemoveAllFromDirectory(lstFiles.at(i).absoluteFilePath(),true,true);
			if (!bOK)
				return false;
		}
		else
		{
			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Delete RemoveAllFromDirectory 1, file: "+ lstFiles.at(i).absoluteFilePath());
			bool bOK=QFile::remove(lstFiles.at(i).absoluteFilePath());
			if (!bOK)
				return false;
		}
	}

	if (bRemoveDirectory)
		return dir.rmdir(strFolderPath);

	return true;
}


//one way encoding: chars: * . " / \ [ ] : ; | = 
bool FileCruncher::IsNameValidAsDirectory(const QString &strDirName)
{
	if (strDirName.contains("*")) return false;
	//if (strDirName.contains(".")) return false;
	if (strDirName.contains("\"")) return false;
	if (strDirName.contains("/")) return false;
	if (strDirName.contains("\\")) return false;
	if (strDirName.contains("[")) return false;
	if (strDirName.contains("]")) return false;
	if (strDirName.contains(":")) return false;
	if (strDirName.contains(";")) return false;
	if (strDirName.contains("|")) return false;
	if (strDirName.contains("=")) return false;
	//if (strDirName.contains(",")) return false;

	return true;
}
//compare all files from strFolder to strCompareToFolder with suffixes if defined
//:NOTE hardocded for thumbs. jpg!
//Returns TRUE if files in strFolder are all newer then in compare folder and all names are matched
bool FileCruncher::CompareFolderAndDeleteNonExisting(QString strFolder, QString strCompareToFolder,QStringList lstSuffixes,QString strAllowedExtension)
{
	QDir dirIN(strFolder);
	QDir dirCompare(strCompareToFolder);

	if (!strAllowedExtension.isEmpty())
	{
		QStringList lstFilters=CreateFiltersFromExtensionString(strAllowedExtension);
		dirIN.setNameFilters(lstFilters);
		dirCompare.setNameFilters(lstFilters);
	}

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("CompareFolderAndDeleteNonExisting 0"));

	QFileInfoList lstFilesIN=dirIN.entryInfoList(QDir::Files | QDir::Readable);
	QFileInfoList lstFilesComp=dirCompare.entryInfoList(QDir::Files |QDir::Readable);
	
	//compare file by file from source folder if not found delete from all entries inside filesIN else compare create() date of first if source>target then return false
	bool bFoldersSame=true;
	int nSize_Source=lstFilesComp.size();
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("CompareFolderAndDeleteNonExisting A: Source:%1, Target:%2").arg(nSize_Source).arg(lstFilesIN.size()));
	for(int i=0;i<nSize_Source;++i)
	{
		//QString strName=lstFilesComp.at(i).completeBaseName();
		QString strName=lstFilesComp.at(i).completeBaseName()+"_"; //add "_" to distinct shit
		int nSize_Target=lstFilesIN.size();
		bool bFound=false;
		for(int k=nSize_Target-1;k>=0;--k)
		{
			if (lstFilesIN.at(k).completeBaseName().indexOf(strName)==0)
			{
				bFound=true;
				if (bFoldersSame)
				{
					if (lstFilesIN.at(k).created()<lstFilesComp.at(i).created())
						bFoldersSame=false;
				}
				lstFilesIN.removeAt(k); //found, remove from list
			}
		}
		if (!bFound) //not found from source in target
			bFoldersSame=false;
	}


	
	//delete files from target:
	int nSize_Delete=lstFilesIN.size();
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("CompareFolderAndDeleteNonExisting B: For Delete files: %1").arg(nSize_Delete));
	for(int i=0;i<nSize_Delete;++i)
		QFile::remove(lstFilesIN.at(i).absoluteFilePath());


	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("CompareFolderAndDeleteNonExisting End"));


	return bFoldersSame;
}


bool FileCruncher::GetJpegSize(unsigned const char* data, unsigned int data_size, int *width, int *height) 
{
	//Check for valid JPEG image
	int i=0;   // Keeps track of the position within the file

	if(data[i] == 0xFF && data[i+1] == 0xD8 && data[i+2] == 0xFF && data[i+3] == 0xE0) {
		i += 4;
		// Check for valid JPEG header (null terminated JFIF)
		if(data[i+2] == 'J' && data[i+3] == 'F' && data[i+4] == 'I' && data[i+5] == 'F' && data[i+6] == 0x00) {
			//Retrieve the block length of the first block since the first block will not contain the size of file
			unsigned short block_length = data[i] * 256 + data[i+1];
			while(i<data_size) {
				i+=block_length;               //Increase the file index to get to the next block
				if(i >= data_size) return false;   //Check to protect against segmentation faults
				if(data[i] != 0xFF) return false;   //Check that we are truly at the start of another block
				if(data[i+1] == 0xC0) {            //0xFFC0 is the "Start of frame" marker which contains the file size
					//The structure of the 0xFFC0 block is quite simple [0xFFC0][ushort length][uchar precision][ushort x][ushort y]
					*height = data[i+5]*256 + data[i+6];
					*width = data[i+7]*256 + data[i+8];
					return true;
				}
				else
				{
					i+=2;                              //Skip the block marker
					block_length = data[i] * 256 + data[i+1];   //Go to the next block
				}
			}
			return false;                     //If this point is reached then no size was found
		}else{ return false; }                  //Not a valid JFIF string

	}else{ return false; }                     //Not a valid SOI header
}

void FileCruncher::GetFolderStatus(Status &pStatus, DbRecordSet &Ret_Folders,int nThumbCount, DbRecordSet &lstFolders,QString strOutThumbDirRoot)
{
	Ret_Folders.destroy();
	Ret_Folders.addColumn(QVariant::String,"ALIAS");
	Ret_Folders.addColumn(QVariant::String,"PATH");
	Ret_Folders.addColumn(QVariant::Int,"SOURCE_COUNT");
	Ret_Folders.addColumn(QVariant::Int,"TARGET_COUNT");
	Ret_Folders.addColumn(QVariant::Int,"STATUS");


	int nSize=lstFolders.getRowCount();

	Ret_Folders.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		QString strCatalogPath=lstFolders.getDataRef(i,"PATH").toString();
		ReplaceNetworkPathWithUNC(strCatalogPath);
		QString strAlias=lstFolders.getDataRef(i,"ALIAS").toString();
		QString strPhotoAlbumID=lstFolders.getDataRef(i,"IPHOTO_ID").toString();
		
		Ret_Folders.setData(i,"ALIAS",strAlias);
		Ret_Folders.setData(i,"PATH",strCatalogPath);


		QList<QString> lstSubFolders;
	
		if (strPhotoAlbumID.isEmpty())
			FileCruncher::GetAllSubFolders(strCatalogPath,lstSubFolders);

		int nFileTypesCode=lstFolders.getDataRef(i,"FILETYPES_CODE").toInt();
		QString strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(nFileTypesCode);

		int nCount = 0;
		if (strPhotoAlbumID.isEmpty())
			nCount=FileCruncher::GetFileCountFromFolder(strCatalogPath,strAllowedFileTypes);
		else
			nCount=FileCruncher_iPhoto::GetFileCountFromFolder(strPhotoAlbumID,strAllowedFileTypes);


		int nSize2=lstSubFolders.size();
		for (int k=0;k<nSize2;k++)
		{
			QString strSubDirPath=strCatalogPath+"/"+lstSubFolders.at(k);
			if (strPhotoAlbumID.isEmpty())
				nCount += FileCruncher::GetFileCountFromFolder(strSubDirPath,strAllowedFileTypes);
			else
				nCount += FileCruncher_iPhoto::GetFileCountFromFolder(strPhotoAlbumID,strAllowedFileTypes);
		}

		//resolve thumb path then same:
		QString strThumbPath=GetThumbPathBasedOnCatalogPath(strAlias,strOutThumbDirRoot);
		int nCount_2=FileCruncher::GetFileCountFromFolder(strThumbPath,strAllowedFileTypes);
		for (int k=0;k<nSize2;k++)
		{
			QString strSubDirPath=strThumbPath+"/"+lstSubFolders.at(k);
			nCount_2 += FileCruncher::GetFileCountFromFolder(strSubDirPath,strAllowedFileTypes);
		}

		Ret_Folders.setData(i,"SOURCE_COUNT",nCount*nThumbCount); //total images to be processed
		Ret_Folders.setData(i,"TARGET_COUNT",nCount_2); //images processed

		if (nCount_2==0)
		{
			Ret_Folders.setData(i,"STATUS",STATUS_NOT_INDEXED);
		}
		else if (nCount_2< nCount*nThumbCount)
		{
			Ret_Folders.setData(i,"STATUS",STATUS_INDEXING);
		}
		else
		{
			Ret_Folders.setData(i,"STATUS",STATUS_OK);
		}

	}
}

void FileCruncher::GetFolderStatusFast(Status &pStatus, DbRecordSet &Ret_Folders,int nThumbCount, DbRecordSet &lstFolders,QString strOutThumbDirRoot)
{
	Ret_Folders.destroy();
	Ret_Folders.addColumn(QVariant::String,"ALIAS");
	Ret_Folders.addColumn(QVariant::String,"PATH");
	Ret_Folders.addColumn(QVariant::Int,"SOURCE_COUNT");
	Ret_Folders.addColumn(QVariant::Int,"TARGET_COUNT");
	Ret_Folders.addColumn(QVariant::Int,"STATUS");


	int nSize=lstFolders.getRowCount();
	//if (nSize!=1)
	//	return;

	QStringList lstPicExt=GetPicFileTypesNameFilterFormat();

	int nCntSrc=0;

	Ret_Folders.addRow(1);
	int nCountSource=0;
	for (int i=0;i<nSize;i++)
	{
		QString strCatalogPath=lstFolders.getDataRef(i,"PATH").toString();
		ReplaceNetworkPathWithUNC(strCatalogPath);
		QString strAlias=lstFolders.getDataRef(i,"ALIAS").toString();
		QString strPhotoAlbumID=lstFolders.getDataRef(i,"IPHOTO_ID").toString();

		Ret_Folders.setData(i,"ALIAS",strAlias);
		Ret_Folders.setData(i,"PATH",strCatalogPath);

		QList<QString> lstSubFolders;
		if (strPhotoAlbumID.isEmpty())
			FileCruncher::GetAllSubFolders(strCatalogPath,lstSubFolders);

		int nCount = 0;
		int nCount_2 = 0;
		int nSize2 = 0;
				
		if (strPhotoAlbumID.isEmpty())
		{
			QDir dir(strCatalogPath);
			if (dir.exists())
				nCount +=dir.entryInfoList(lstPicExt,QDir::Files).count();

			nSize2=lstSubFolders.size();
			for (int k=0;k<nSize2;k++)
			{
				QString strSubDirPath=strCatalogPath+"/"+lstSubFolders.at(k);
				QDir dir(strSubDirPath);
				if (dir.exists())
					nCount +=dir.entryInfoList(lstPicExt,QDir::Files).count();
			}
		}
		else
		{
			QFileInfoList lstFilesComp;  
			iPhotoHandler::GetAlbumPicturesExt(strPhotoAlbumID,lstFilesComp); 
			nCount +=lstFilesComp.count();
		}


		//resolve thumb path then same:
		QString strThumbPath=GetThumbPathBasedOnCatalogPath(strAlias,strOutThumbDirRoot);
		QDir dir_2(strThumbPath);
		if (dir_2.exists())
			nCount_2 +=dir_2.entryInfoList(lstPicExt,QDir::Files).count();

		for (int k=0;k<nSize2;k++)
		{
			QString strSubDirPath=strThumbPath+"/"+lstSubFolders.at(k);
			QDir dir_2(strSubDirPath);
			if (dir_2.exists())
				nCount_2 +=dir_2.entryInfoList(lstPicExt,QDir::Files).count();
		}

		Ret_Folders.setData(i,"SOURCE_COUNT",nCount*nThumbCount); //total images to be processed
		Ret_Folders.setData(i,"TARGET_COUNT",nCount_2); //images processed

		if (nCount_2==0)
		{
			Ret_Folders.setData(i,"STATUS",STATUS_NOT_INDEXED);
		}
		else if (nCount_2< nCount*nThumbCount)
		{
			Ret_Folders.setData(i,"STATUS",STATUS_INDEXING);
		}
		else
		{
			Ret_Folders.setData(i,"STATUS",STATUS_OK);
		}
	}
}

int FileCruncher::GetFileCountFromFolderFast(QString strFolderPath)
{
	int nCount_2=0;
	//resolve thumb path then same:
	QDir dir_2(strFolderPath);
	if (dir_2.exists())
		nCount_2 +=dir_2.entryInfoList(QDir::Files).count();

	QStringList lstSubFolders;
	GetAllSubFolders(strFolderPath,lstSubFolders);

	int nSize2=lstSubFolders.size();
	for (int k=0;k<nSize2;k++)
	{
		QString strSubDirPath=strFolderPath+"/"+lstSubFolders.at(k);
		QDir dir_2(strSubDirPath);
		if (dir_2.exists())
			nCount_2 +=dir_2.entryInfoList(QDir::Files).count();
	}

	return nCount_2;

}

//if not exist it creates it:
QString FileCruncher::GetThumbPathBasedOnCatalogPath(QString strCatalogDirPath,QString strOutThumbDirRoot)
{
	QString strPath=QDir::cleanPath(strOutThumbDirRoot+"/"+strCatalogDirPath);
	QDir dir(strPath);
	if (!dir.exists())
	{
		if(!dir.mkpath(strPath))
			return "";
	}
	return strPath;
}


//remove root folders:
void FileCruncher::RemoveRootFolders(QStringList &lstFolders)
{
	QStringList lstRootFolders;

	int nSize2=lstFolders.size();
	for (int k=0;k<nSize2;k++)
	{
		QDir dirIN(lstFolders.at(k));
		QFileInfoList lstFilesIN=dirIN.entryInfoList(QDir::Dirs);
		int nSize=lstFilesIN.size();
		bool bFound=false;
		for (int i=0;i<nSize;i++)
		{
			if (bFound)
				break;
			

			if (lstFilesIN.at(i).completeBaseName().isEmpty() || lstFilesIN.at(i).completeBaseName()=="." || lstFilesIN.at(i).completeBaseName()=="..")
				continue;

			QString strFolderCompletePath=lstFolders.at(k).toLower()+"/"+lstFilesIN.at(i).completeBaseName().toLower();
			//qDebug()<<strFolderCompletePath;

			for (int z=k+1;z<nSize2;z++)
			{
				//qDebug()<<lstFolders.at(z);
				if (strFolderCompletePath==lstFolders.at(z).toLower())
				{
					lstRootFolders.append(lstFolders.at(k));
					bFound=true;
					break;
				}
			}
		}
	}


	//qDebug()<<lstRootFolders;
	

	int nSize=lstRootFolders.size();
	for (int i=nSize2-1;i>=0;i--)
	{
		for (int k=0;k<nSize;k++)
		{
			if (lstRootFolders.at(k)==lstFolders.at(i))
			{
				lstFolders.removeAt(i);
				break;
			}
		}
	}

	//qDebug()<<lstFolders;


}

//Reads all files from directory (all subfolders): zip each file, AES encode file, store relative path and size of file: delimit with ";----" (5char delimiter) + all to hex
//paths are relative to root
bool FileCruncher::EncodeDirectory2Binary(QString strPassword,const QString strDirPath,QByteArray &byteData)
{
	byteData.clear();

	//get all files from dir:
	DbRecordSet lstPaths,lstApps;
	DataHelper::GetFilesFromFolder(strDirPath,lstPaths,lstApps,true,true,strDirPath);

	//read one by one, zip, encode, store
	int nSize=lstPaths.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strRelPath=lstPaths.getDataRef(i,"PATH").toString();
		QString strPath=strDirPath+"/"+strRelPath;

		QFile file(strPath);
		QFileInfo fileInfo(strPath);

		if(!file.open(QIODevice::ReadOnly))
			return false;
		QByteArray data=file.readAll();
		QByteArray zip_data;
		if(!ZipCompression::Deflate(data,zip_data))
			return false;
		int nFileSize;
		QByteArray enc_data;
		if(!EncryptedIniFile::AES_EncryptBinaryData(strPassword,zip_data,enc_data,nFileSize))
			return false;

		byteData.append(lstPaths.getDataRef(i,"PATH").toString().toLatin1()+"\n\r");
		byteData.append(QString::number(nFileSize).toLatin1()+"\n\r");
		byteData.append(fileInfo.lastModified().toString(Qt::ISODate).toLatin1()+"\n\r");
		byteData.append(enc_data);
		byteData.append(BINARY_DELIMITER);

	}

	//byteData=byteData.toHex();

	return true;
}




//revert from encode: result is stored in list of decoded binary data with relative paths
bool FileCruncher::DecodeBinary2Directory(QString strPassword, const QByteArray &byteData,QList<QString> &lstPaths, QList<QDateTime> &lstLastModifyTimes,QList<QByteArray> &lstBinaryContent)
{
	//QByteArray byteDataTemp=QByteArray::fromHex(byteData);

	QList<QByteArray> lstFilesEncoded;
	int nLastPos=0;
	int nPos=byteData.indexOf(BINARY_DELIMITER,nLastPos);
	while(nPos>0)
	{
		QByteArray bytePart=byteData.mid(nLastPos,nPos-nLastPos);
		lstFilesEncoded.append(bytePart);
		nLastPos=nPos+QString(BINARY_DELIMITER).length();
		nPos=byteData.indexOf(BINARY_DELIMITER,nLastPos);
	}

	

	int nSize=lstFilesEncoded.size();
	for (int i=0;i<nSize;i++)
	{
		QByteArray enc_data=lstFilesEncoded.at(i);

		int nNewLine1=enc_data.indexOf("\n\r");
		int nNewLine2=enc_data.indexOf("\n\r",nNewLine1+1);
		int nNewLine3=enc_data.indexOf("\n\r",nNewLine2+1);
		QString strPath=enc_data.left(nNewLine1);
		QString strSize=enc_data.mid(nNewLine1+2,nNewLine2-nNewLine1-2);
		int nFileSize=strSize.toInt();
		QString strLastModify=enc_data.mid(nNewLine2+2,nNewLine3-nNewLine2-2);
		enc_data=enc_data.mid(nNewLine3+2);
		QDateTime datLastModify=QDateTime::fromString(strLastModify,Qt::ISODate);

		QByteArray zip_data;
		if(!EncryptedIniFile::AES_DecryptBinaryData(strPassword,enc_data,zip_data,nFileSize))
			return false;

		QByteArray data;
		if(!ZipCompression::Inflate(zip_data,data))
			return false;

		lstPaths.append(strPath);
		lstBinaryContent.append(data);
		lstLastModifyTimes.append(datLastModify);
	}

	return true;
}





QString	FileCruncher_iPhoto::GetFirstFileFromFolder(QString strPhotoAlbumID,QString strAllowedExtension)
{
	
	QFileInfoList lstFiles;
	if(!iPhotoHandler::GetAlbumPicturesExt(strPhotoAlbumID,lstFiles))
		return "";

	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		//Get all files from folder if list is empty or filter 'em
		if (!strAllowedExtension.isEmpty())
		{
			if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
				continue;
		}
		return lstFiles.at(i).absoluteFilePath();
	}
	return "";
}

QDateTime FileCruncher_iPhoto::GetLastModifyDateFromFolder(QString strPhotoAlbumID,QString strAllowedExtension)
{
	QDateTime datMax;
	QFileInfoList lstFiles;  
	if(!iPhotoHandler::GetAlbumPicturesExt(strPhotoAlbumID,lstFiles)) 
		return datMax;

	int nSize=lstFiles.size();
	
	if (nSize>0)
		datMax =lstFiles.at(0).created();
	else
		return datMax;

	for(int i=0;i<nSize;++i)
	{
		//Get all files from folder if list is empty or filter 'em
		if (!strAllowedExtension.isEmpty())
		{
			if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
				continue;
		}
		if(lstFiles.at(i).created()>datMax)
			datMax=lstFiles.at(i).created();
	}

	return datMax; 


}


int FileCruncher_iPhoto::GetFileCountFromFolder(QString strPhotoAlbumID,QString strAllowedExtension)
{
	int nCnt=0;

	QFileInfoList lstFiles;  
	if(!iPhotoHandler::GetAlbumPicturesExt(strPhotoAlbumID,lstFiles)) 
		return 0;


	//return lstFiles.size();
	int nSize=lstFiles.size();
	QString strNamePrev="";
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isFile())
		{
			if (strNamePrev==lstFiles.at(i).completeBaseName())
				continue; //skip if same name

			//Get all files from folder if list is empty or filter 'em
			if (!strAllowedExtension.isEmpty())
			{
				if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
					continue;
			}
			nCnt++;
		}

		strNamePrev = lstFiles.at(i).completeBaseName();
	}
	return nCnt;
}


//get files: ifbUseRelativeFilePath then PATH will contain only relative path+filename to strFolderPath, else full path with filename
bool FileCruncher_iPhoto::GetFilesFromFolder(QString strPhotoAlbumID,DbRecordSet &lstPaths, DbRecordSet &lstSubDirs, QString strAllowedExtension,QString strFileSuffixFilter)
{
	lstPaths.destroy();
	lstSubDirs.destroy();
	lstPaths.addColumn(QVariant::String,"PATH");
	lstPaths.addColumn(QVariant::String,"NAME");
	lstPaths.addColumn(QVariant::String,"EXT");
	lstPaths.addColumn(QVariant::String,"FILENAME");
	lstPaths.addColumn(QVariant::DateTime,"LAST_MODIFY");
	lstPaths.addColumn(QVariant::Int,"SIZE");

	lstSubDirs.addColumn(QVariant::String,"PATH");
	lstSubDirs.addColumn(QVariant::String,"NAME");


	QFileInfoList lstFiles;  
	if(!iPhotoHandler::GetAlbumPicturesExt(strPhotoAlbumID,lstFiles)) 
		return false;


	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isDir())
		{
			//skip current and parent directory:
			if (lstFiles.at(i).fileName()==".") continue;
			if (lstFiles.at(i).fileName()=="..") continue;

			lstSubDirs.addRow();
			lstSubDirs.setData(lstSubDirs.getRowCount()-1,"PATH",lstFiles.at(i).absoluteFilePath());
			lstSubDirs.setData(lstSubDirs.getRowCount()-1,"NAME",lstFiles.at(i).completeBaseName());
		}
		else
		{
			//Get all files from folder if list is empty or filter 'em
			if (!strAllowedExtension.isEmpty())
			{
				if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
					continue;
			}
			if (!strFileSuffixFilter.isEmpty())
			{
				if (lstFiles.at(i).completeBaseName().right(strFileSuffixFilter.length())!=strFileSuffixFilter)
					continue;
			}
			lstPaths.addRow();
			lstPaths.setData(lstPaths.getRowCount()-1,0,lstFiles.at(i).absoluteFilePath());
			lstPaths.setData(lstPaths.getRowCount()-1,1,lstFiles.at(i).completeBaseName());
			lstPaths.setData(lstPaths.getRowCount()-1,2,lstFiles.at(i).suffix());
			lstPaths.setData(lstPaths.getRowCount()-1,3,lstFiles.at(i).fileName());
			lstPaths.setData(lstPaths.getRowCount()-1,4,lstFiles.at(i).lastModified());
			lstPaths.setData(lstPaths.getRowCount()-1,5,(int)lstFiles.at(i).size());
		}
	}

	lstPaths.sort(lstPaths.getColumnIdx("NAME"));

	return true;
}


//compare all files from strFolder to strCompareToFolder with suffixes if defined
//:NOTE hardcoded for thumbs. jpg!
//Returns TRUE if files in strFolder are all newer then in compare folder and all names are matched
//WARNING: lstSuffixes must be SORTED alphabetically as lstFilesIN list by filename to match first thumb with extenstion: _1024_768 (for now)
bool FileCruncher_iPhoto::CompareFolderAndDeleteNonExisting(QString strFolder,  QString strPhotoAlbumID,QStringList lstSuffixes,QString strAllowedExtension)
{
	QDir dirIN(strFolder);
	//QDir dirCompare(strCompareToFolder);

	if (!strAllowedExtension.isEmpty())
	{
		QStringList lstFilters=FileCruncher::CreateFiltersFromExtensionString(strAllowedExtension);
		dirIN.setNameFilters(lstFilters);
		//dirCompare.setNameFilters(lstFilters);
	}

	QFileInfoList lstFilesIN=dirIN.entryInfoList(QDir::Files |QDir::Readable,QDir::Name);
	QFileInfoList lstFilesComp;  
	iPhotoHandler::GetAlbumPicturesExt(strPhotoAlbumID,lstFilesComp); 


	//compare file by file from source folder if not found delete from all entries inside filesIN else compare create() date of first if source>target then return false
	bool bFoldersSame=true;
	int nSize_Source=lstFilesComp.size();
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("CompareFolderAndDeleteNonExisting_iPhoto A: Source:%1, Target:%2").arg(nSize_Source).arg(lstFilesIN.size()));
	for(int i=0;i<nSize_Source;++i)
	{
		QString strName=lstFilesComp.at(i).completeBaseName();
		int nSize_Target=lstFilesIN.size();
		bool bFound=false;
		for(int k=nSize_Target-1;k>=0;--k)
		{
			if (lstFilesIN.at(k).completeBaseName().indexOf(strName)==0)
			{
				bFound=true;
				if (bFoldersSame)
				{
					if (lstFilesIN.at(k).created()<lstFilesComp.at(i).created())
						bFoldersSame=false;
				}
				lstFilesIN.removeAt(k); //found, remove from list
			}
		}
		if (!bFound) //not found from source in target
			bFoldersSame=false;
	}



	//delete files from target:
	int nSize_Delete=lstFilesIN.size();
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("CompareFolderAndDeleteNonExisting_iPhoto B: For Delete files: %1").arg(nSize_Delete));
	for(int i=0;i<nSize_Delete;++i)
		QFile::remove(lstFilesIN.at(i).absoluteFilePath());


	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("CompareFolderAndDeleteNonExisting_iPhoto End"));


	return bFoldersSame;
}




void FileCruncher::ParseNetworkDrives(QStringList &lstDrives,QStringList &lstPaths)
{

#ifdef Q_WS_WIN
	QProcess tool;
	QStringList lstArgs;
	lstArgs<<"use";

	tool.start("net.exe",lstArgs);
	tool.waitForFinished(-1);
	QByteArray errContent = tool.readAllStandardOutput();

	int nPos=0;
	nPos=errContent.indexOf(":");
	do 
	{
		QString strLetter=errContent.mid(nPos-1,2)+"/";
		QString strPath;
		int nPos2=errContent.indexOf("\\\\",nPos);
		if (nPos2>=0)
		{
			int nPos3=errContent.indexOf(" ",nPos2);
			if (nPos3>=0)
				strPath=errContent.mid(nPos2,nPos3-nPos2).trimmed();
			else
				continue;
		}
		else
			continue;
		lstDrives.append(strLetter);
		strPath.replace("\\","/"); //normallize to the "/"
		lstPaths.append(strPath);

		nPos=errContent.indexOf(":",nPos+1);

	} while(nPos>=0);

	
#else


	return;


#endif
}



bool FileCruncher::ReplaceNetworkPathWithUNC(QString &strDirPath)
{
	QStringList lstRemoteDrives;
	QStringList lstRemotePaths;
	ParseNetworkDrives(lstRemoteDrives,lstRemotePaths);

	strDirPath=strDirPath.toLower();

	//BT: added: special funct: change PATH on existing shared
	int nSizeOfPublished=lstRemoteDrives.size();
	for (int n=0;n<nSizeOfPublished;n++)
	{
		if (strDirPath.indexOf(lstRemoteDrives.at(n).toLower())==0)
		{
			strDirPath=QDir::cleanPath(lstRemotePaths.at(n)+"/"+strDirPath.mid(3)).toLower();
			return true;
		}
	}

	return false;

}



void FileCruncher::GetDrives(QStringList &lstDriveNames,QStringList &lstDriveLetters,QStringList &lstPaths,QList<int> &lstTypes)
{



#if defined _WIN32

	TCHAR szDrives[128];
	TCHAR *pDrive;
	TCHAR szVolNAme[256];

	DWORD r = GetLogicalDriveStrings(sizeof(szDrives), szDrives);
	if(r = 0 || r > sizeof(szDrives)) return;

	pDrive = szDrives;  // Point to the first drive
	while(*pDrive != '\0')
	{
		switch(GetDriveType(pDrive))
		{
		case DRIVE_CDROM:
		case DRIVE_RAMDISK:
			lstTypes.append(JOHN_DRIVE_CDROM);
			break;
		case DRIVE_REMOVABLE:
			lstTypes.append(JOHN_DRIVE_REMOVABLE);
			break;
		case DRIVE_REMOTE:
			{
				lstTypes.append(JOHN_DRIVE_REMOTE);
				//ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Remote type detected"));
				break;
			}
		default:
			lstTypes.append(JOHN_DRIVE_FIXED);
		}

		/*
		if(GetVolumeInformation(pDrive,szVolNAme,sizeof(szVolNAme),NULL,NULL,NULL,NULL,NULL))
		{
			QString	strResult = QString::fromUtf16((const ushort *)szVolNAme);
			lstDriveNames.append(strResult);
		}
		else
		*/
		//lstDriveNames.append("");

		//QString strDrive = QString::fromUtf16((const ushort *)pDrive);
		//lstDriveLetters.append(strDrive);

		pDrive += 4;  // Point to the next drive
	}




#else
	lstTypes.append(JOHN_DRIVE_FIXED);
#endif 

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Drive U access: start"));
	//QDir test("\\\\192.168.200.10\\Development\\");
	//int nCntDirs = test.entryInfoList(QDir::AllDirs).size();
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Drive U access: stop %1").arg(nCntDirs));


	QFileInfoList lstFiles = QDir::drives();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{

		QString strDriveLetter=lstFiles.at(i).absoluteFilePath();

		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("Drive listed: (%1)").arg(strDriveLetter));

		lstDriveLetters.append(strDriveLetter);

	#if defined _WIN32
		strDriveLetter.replace("/","");
	#endif
		QString strDriveName;
		//if (!lstDriveNames.at(i).isEmpty())
		//	strDriveName = lstDriveNames.at(i);
		//else
		//{
			if (lstTypes.at(i)==JOHN_DRIVE_CDROM)
			{
				strDriveName = "DVD-RW Drive";
			}
			else if (lstTypes.at(i)==JOHN_DRIVE_REMOVABLE)
			{
				strDriveName = "Removable Drive";
			}
			else if (lstTypes.at(i)==JOHN_DRIVE_REMOTE)
			{
				strDriveName = "Remote Drive";
			}
			else
				strDriveName = "Disk Drive";
		//}

		if (!strDriveName.isEmpty())
			strDriveName = strDriveName +" ("+strDriveLetter+")";
		else
			strDriveName = strDriveLetter;


		lstDriveNames.append(strDriveName);
	}

	lstPaths=lstDriveLetters;


//unser service under WIN32 I must manualy access shared drives and list 'em as QDir::drives does not return them
//#if defined _WIN32
	
	//search for mapped drives: if not in list add, if yes
	QStringList lstRemoteDrives;
	QStringList lstRemotePaths;
	ParseNetworkDrives(lstRemoteDrives,lstRemotePaths);

	int nSize3=lstRemoteDrives.size();
	for(int i=0;i<nSize3;++i)
	{
		int nIdx=lstDriveLetters.indexOf(lstRemoteDrives.at(i));
		if (nIdx<0)
		{
			QString strLetter = lstRemoteDrives.at(i);
			lstDriveLetters.append(strLetter);

			strLetter.replace("/","");

			QString strDriveName = "Remote Drive ("+strLetter+")";
			lstDriveNames.append(strDriveName);
			lstTypes.append(JOHN_DRIVE_REMOTE);

			

			lstPaths.append(lstRemotePaths.at(i));

			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Adding network drive with letter %1 and path %2 ").arg(strLetter).arg(lstRemotePaths.at(i)));
		}
		else
		{
			lstTypes[nIdx] = JOHN_DRIVE_REMOTE;
			QString strLetter = lstRemoteDrives.at(i);
			lstDriveLetters[nIdx] = strLetter;
			strLetter.replace("/","");
			QString strDriveName = "Remote Drive ("+strLetter+")";
			lstDriveNames[nIdx] = strDriveName;
			
			lstPaths[nIdx] = lstRemotePaths.at(i);

			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Setting on existing entry network drive with letter %1 and path %2 ").arg(strLetter).arg(lstRemotePaths.at(i)));
		}
	}


//#endif



}
