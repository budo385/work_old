#ifndef EMAILHANDLER_H
#define EMAILHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class EmailHandler
{

public:

	static void SendEmail(Status &err, QString strAppServerID, DbRecordSet rowFolder, DbRecordSet recMail, DbRecordSet lstUsersToSendEmail, QString strSubject, QString strBody,QString strCatalogNameFullPath="");
	static void SendAppServerLog(Status &err, QString strAppServerID, QString strLogPath, QString strFrom, QString strTo, QString strMailServer, int nMailPort, QString strEmailServerUser, QString strEmailServerPass);

private:
	
};

#endif // EMAILHANDLER_H
