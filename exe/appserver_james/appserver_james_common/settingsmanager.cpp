#include "settingsmanager.h"
#include "common/common/authenticator.h"
#include "trans/trans/xmlutil.h"
#include "common/common/config_version_ffh.h"
#include "filecruncher.h"
#include <QFile>
#include <QFileInfo>



SettingsManager::SettingsManager()
:m_nSettingsChanged(0),m_lckVars(QReadWriteLock::Recursive)
{

}

SettingsManager::~SettingsManager()
{

}

void SettingsManager::Initialize(QString strPath)
{
	m_IniFile.SetPassword(SETTINGS_PASSWORD);
	m_strSettingsPath=strPath;
	m_strIniPath=strPath+"/settings/user_data";
	m_IniFile.SetPath(m_strIniPath);

}

bool SettingsManager::AuthenticateUser(QString strUserName,QString strPass)
{
	QString strAdminUser = GetAdminUserName();

	QReadLocker lock(&m_lckVars);

	QByteArray bytePass=Authenticator::GeneratePassword(strUserName,strPass);
	if (strUserName==strAdminUser)
	{
		if (bytePass!=m_byteRootPass)
			return false;
		else
			return true;
	}
	/*
	else if (strUserName=="guest")
	{
		if (bytePass!=m_byteGuestPass)
			return false;
		else
			return true;
	}
	*/
	int nRow=m_lstUsers.find("USERNAME",strUserName,true);
	if (nRow<0)
		return false;

	if (m_lstUsers.getDataRef(nRow,"PASSWORD")!=strPass)
		return false;
	else
		return true;
}

bool SettingsManager::GetUserPassword(QString strUserName,QByteArray &bytePass)
{
	QString strAdminUser = GetAdminUserName();
	QReadLocker lock(&m_lckVars);

	bytePass.clear();
	if (strUserName==strAdminUser)
	{
		bytePass=m_byteRootPass;
		return true;
	}
	/*
	else if (strUserName=="guest")
	{
		bytePass=m_byteGuestPass;
		return true;
	}
	*/
	int nRow=m_lstUsers.find("USERNAME",strUserName,true);
	if (nRow<0)
		return false;

	bytePass=Authenticator::GeneratePassword(strUserName,m_lstUsers.getDataRef(nRow,"PASSWORD").toString());
	return true;
}

void SettingsManager::GetRootPass(QString &strPass)
{
	QReadLocker lock(&m_lckVars);
	strPass=m_strRootPassPlain;
}
void SettingsManager::SetRootPass(QString strPass)
{
	QString strAdminUser = GetAdminUserName();
	QWriteLocker lock(&m_lckVars);
	m_strRootPassPlain=strPass;
	m_byteRootPass =Authenticator::GeneratePassword(strAdminUser,m_strRootPassPlain);
}

void SettingsManager::GetServerThumb(QByteArray &byteThumb,QByteArray &byteThumbLarge)
{
	QReadLocker lock(&m_lckVars);
	byteThumb=m_byteServerThumb;
	byteThumbLarge=m_byteServerThumb_Large;
}

QString SettingsManager::GetServerName()
{
	QReadLocker lock(&m_lckVars);
	return m_strServerName;
}

void SettingsManager::SetServerName(QString strName)
{
	QWriteLocker lock(&m_lckVars);
	m_strServerName=strName;
}



void SettingsManager::SetServerThumb(QByteArray byteThumb,QByteArray byteThumbLarge)
{
	QWriteLocker lock(&m_lckVars);
	m_byteServerThumb=byteThumb;
	m_byteServerThumb_Large=byteThumbLarge;
	m_datServerThumbModify=QDateTime::currentDateTime();
}

void SettingsManager::GetFolders(DbRecordSet &lstFolders)
{
	QReadLocker lock(&m_lckVars);
	lstFolders=m_lstFolders;
}
void SettingsManager::GetUserRights(DbRecordSet &lstUserRights)
{
	QReadLocker lock(&m_lckVars);
	lstUserRights=m_lstUserRights;
}
void SettingsManager::SetFolders(DbRecordSet &lstFolders)
{
	QWriteLocker lock(&m_lckVars);
	m_lstFolders=lstFolders;
}
void SettingsManager::SetUserRights(DbRecordSet &lstUserRights)
{
	QWriteLocker lock(&m_lckVars);
	m_lstUserRights=lstUserRights;
}

void SettingsManager::GetUsers(DbRecordSet &lstUsers)
{
	QReadLocker lock(&m_lckVars);
	lstUsers=m_lstUsers;
}
void SettingsManager::SetUsers(DbRecordSet &lstUsers)
{
	QWriteLocker lock(&m_lckVars);
	m_lstUsers=lstUsers;
}

void SettingsManager::GetEmailSettings(DbRecordSet &lstData)
{
	QReadLocker lock(&m_lckVars);
	lstData=m_lstEmailSettings;
}

void SettingsManager::SetEmailSettings(DbRecordSet &lstData)
{
	QWriteLocker lock(&m_lckVars);
	m_lstEmailSettings=lstData;
}

QString SettingsManager::GetAdminUserName()
{
	QReadLocker lock(&m_lckVars);
	return m_lstGeneralSettings.getDataRef(0,"ADMIN_USER").toString();
}
void SettingsManager::SetAdminUserName(QString strAdminUserName)
{
	QWriteLocker lock(&m_lckVars);
	m_lstGeneralSettings.setData(0,"ADMIN_USER",strAdminUserName);
}

bool SettingsManager::GetUserRightRecord(QString strUserName,QString strAlias,DbRecordSet &rowUserRights)
{
	DbRecordSet lstUserRights;
	GetUserRights(lstUserRights);

	int nSelected=lstUserRights.find("USERNAME",strUserName);
	if (nSelected<=0)
		return false;

	nSelected=lstUserRights.find("ALIAS",strAlias,false,true,true,true);
	if (nSelected!=1)
		return false;

	rowUserRights=lstUserRights.getSelectedRecordSet();
	return true;
}

bool SettingsManager::TestUserRight(QString strUserName,QString strAlias,int nOperation)
{
	if (strUserName==GetAdminUserName())
		return true;

	if (strAlias.indexOf("/")>=0)
		strAlias=strAlias.split("/").at(0);

	/*
	if (strUserName=="guest")
	{
		if (nOperation==OP_DOWNLOAD)
			return true;
		else
			return false;
	}
	*/

	DbRecordSet rowUserRights;
	if(!GetUserRightRecord(strUserName,strAlias,rowUserRights))
		return false;

	switch (nOperation)
	{
	case OP_DOWNLOAD:
		return rowUserRights.getDataRef(0,"CAN_DOWNLOAD").toBool();
		break;
	case OP_UPLOAD:
		return rowUserRights.getDataRef(0,"CAN_UPLOAD").toBool();
		break;
	case OP_DELETE:
		return rowUserRights.getDataRef(0,"CAN_DELETE").toBool();
		break;
	case OP_SEND_INVITE:
		return rowUserRights.getDataRef(0,"CAN_SEND_INVITE").toBool();
		break;
	default:
		return false;
	}

	return false;
}

bool SettingsManager::TestModuleRight(int nProgCode,int nOperation)
{
	return true;

	/*
	switch (nOperation)
	{
	case OP_MORE_THEN_ONE_CATALOG:
		{
			if (nProgCode==CLIENT_CODE_FFH_PRO || nProgCode==CLIENT_CODE_PFH_PRO)
				return true;
			else
				return false;
		}
		break;
	case OP_VIEW_RAW_FILES:
		{
			if (nProgCode==CLIENT_CODE_FFH_PRO || nProgCode==CLIENT_CODE_PFH_PRO)
				return true;
			else
				return false;
		}
		break;
	default:
		return false;
	}

	return false;
	*/
}


void SettingsManager::GetDirectoryPath(Status &pStatus,QString strAliasPath, QString &strAlias,QString &strDirPath,int &nFileTypesCode,QString &strAllowedFileTypes, QString &strAlbumIPhotoID)
{
	DbRecordSet lstFolders;
	GetFolders(lstFolders);
	QString strSubFolder;

	int nIdx=strAliasPath.indexOf("/");
	if (nIdx>0)
	{
		strAlias=strAliasPath.left(nIdx);
		strSubFolder=strAliasPath.mid(nIdx);
	}
	else
		strAlias=strAliasPath;

	int nRow=lstFolders.find("ALIAS",strAlias,true);
	if (nRow<0)
	{
		pStatus.setError(1,"Error while trying to fetch directory!");
		return;
	}

	strAlbumIPhotoID=lstFolders.getDataRef(nRow,"IPHOTO_ID").toString();

	if (strAlbumIPhotoID.isEmpty())
	{
		strDirPath=lstFolders.getDataRef(nRow,"PATH").toString()+strSubFolder;
		FileCruncher::ReplaceNetworkPathWithUNC(strDirPath); //replace with UNC
		/*
		if (!QFile::exists(strDirPath))
		{
			pStatus.setError(1,"Error while trying to fetch directory!");
			return;
		}
		*/
	}

	nFileTypesCode=lstFolders.getDataRef(nRow,"FILETYPES_CODE").toInt();
	strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(nFileTypesCode);
}

bool SettingsManager::LoadSettings()
{
	QWriteLocker lock(&m_lckVars);

	//if not exists, create defaults/save:
	if(!QFile::exists(m_strIniPath))
	{
		lock.unlock();
		CreateDefaults();
		if(!SaveSettings())
		{
			return false;
		}
		lock.relock();
	}

	if(!m_IniFile.LoadEnc())
		return false;

	m_IniFile.GetValue("MAIN","APPSERVER_ID",m_strAppServerID);

	QString strData;
	m_IniFile.GetValue("MAIN","ROOT_PASS",strData);
	m_byteRootPass=QByteArray::fromBase64(strData.toLatin1());

	m_IniFile.GetValue("MAIN","ROOT_PASS_PLAIN",m_strRootPassPlain);

	m_IniFile.GetValue("MAIN","SERVER_THUMB",strData);
	m_byteServerThumb=QByteArray::fromBase64(strData.toLatin1());

	m_IniFile.GetValue("MAIN","SERVER_THUMB_LARGE",strData);
	m_byteServerThumb_Large=QByteArray::fromBase64(strData.toLatin1());

	QString strTemp;
	m_IniFile.GetValue("MAIN","SERVER_NAME",strTemp);
	m_strServerName=QString::fromUtf8(strTemp.toLatin1());

	m_IniFile.GetValue("MAIN","SERVER_THUMB_DATE",strData);
	if (!strData.isEmpty())
		m_datServerThumbModify=QDateTime::fromString(strData,Qt::ISODate);

	//m_IniFile.GetValue("MAIN","SIGNATURE",m_strSignature);

	//m_IniFile.GetValue("MAIN","GUEST_PASS",strData);
	//m_byteGuestPass=QByteArray::fromBase64(strData.toLatin1());

	m_IniFile.GetValue("MAIN","REC_USERS",strData);
	QByteArray data = strData.toLatin1();
	m_lstUsers=XmlUtil::ConvertByteArray2RecordSet_Fast(data);

	m_IniFile.GetValue("MAIN","REC_FOLDERS",strData);
	data = strData.toLatin1();
	m_lstFolders=XmlUtil::ConvertByteArray2RecordSet_Fast(data);

	m_IniFile.GetValue("MAIN","REC_USER_RIGHTS",strData);
	data = strData.toLatin1();
	m_lstUserRights=XmlUtil::ConvertByteArray2RecordSet_Fast(data);

	m_IniFile.GetValue("MAIN","REC_EMAIL_SETTINGS",strData);
	data = strData.toLatin1();
	m_lstEmailSettings=XmlUtil::ConvertByteArray2RecordSet_Fast(data);

	m_IniFile.GetValue("MAIN","REC_GENERAL_SETTINGS",strData);
	data = strData.toLatin1();
	m_lstGeneralSettings=XmlUtil::ConvertByteArray2RecordSet_Fast(data);


	if (m_lstUsers.getColumnCount()==0)
		DefineUsers(m_lstUsers);
	if (m_lstFolders.getColumnCount()==0)
		DefineFolders(m_lstFolders);
	if (m_lstUserRights.getColumnCount()==0)
		DefineUserRights(m_lstUserRights);
	if (m_lstGeneralSettings.getColumnCount()==0)
		DefineGeneralSettings(m_lstGeneralSettings);

	ActualizeData();
	
	return true;
}

bool SettingsManager::SaveSettings()
{
	QReadLocker lock(&m_lckVars);

	m_IniFile.SetValue("MAIN","APPSERVER_ID",m_strAppServerID);

	QString strData;
	strData=m_byteRootPass.toBase64();
	m_IniFile.SetValue("MAIN","ROOT_PASS",strData);

	m_IniFile.SetValue("MAIN","ROOT_PASS_PLAIN",m_strRootPassPlain);

	strData=m_byteServerThumb.toBase64();
	m_IniFile.SetValue("MAIN","SERVER_THUMB",strData);

	strData=m_byteServerThumb_Large.toBase64();
	m_IniFile.SetValue("MAIN","SERVER_THUMB_LARGE",strData);

	QString strTemp=m_strServerName.toUtf8();
	m_IniFile.SetValue("MAIN","SERVER_NAME",strTemp);
	m_IniFile.SetValue("MAIN","SERVER_THUMB_DATE",m_datServerThumbModify.toString(Qt::ISODate));

//	m_IniFile.SetValue("MAIN","SIGNATURE",m_strSignature);


	//strData=m_byteGuestPass.toBase64();
	//m_IniFile.SetValue("MAIN","GUEST_PASS",strData);

	strData=QString(XmlUtil::ConvertRecordSet2ByteArray_Fast(m_lstUsers));
	m_IniFile.SetValue("MAIN","REC_USERS",strData);

	strData=QString(XmlUtil::ConvertRecordSet2ByteArray_Fast(m_lstFolders));
	m_IniFile.SetValue("MAIN","REC_FOLDERS",strData);

	strData=QString(XmlUtil::ConvertRecordSet2ByteArray_Fast(m_lstUserRights));
	m_IniFile.SetValue("MAIN","REC_USER_RIGHTS",strData);

	strData=QString(XmlUtil::ConvertRecordSet2ByteArray_Fast(m_lstEmailSettings));
	m_IniFile.SetValue("MAIN","REC_EMAIL_SETTINGS",strData);

	strData=QString(XmlUtil::ConvertRecordSet2ByteArray_Fast(m_lstGeneralSettings));
	m_IniFile.SetValue("MAIN","REC_GENERAL_SETTINGS",strData);

	return m_IniFile.SaveEnc(); //try again to save
}

//empty records + master pass= root and guest=''
void SettingsManager::CreateDefaults()
{
	DefineUsers(m_lstUsers);
	DefineFolders(m_lstFolders);
	DefineUserRights(m_lstUserRights);
	DefineEmailSettings(m_lstEmailSettings);
	DefineGeneralSettings(m_lstGeneralSettings);
	m_strServerName="My Server";
	m_strRootPassPlain="admin";

	//fill general settings
	if (m_lstGeneralSettings.getRowCount()==0)
	{
		m_lstGeneralSettings.addRow();
		m_lstGeneralSettings.setData(0,"ADMIN_USER","admin");
	}

	m_byteRootPass =Authenticator::GeneratePassword(GetAdminUserName(),m_strRootPassPlain);
}


void SettingsManager::DefineUsers(DbRecordSet &lstData)
{
	lstData.destroy();
	lstData.addColumn(QVariant::String,"USERNAME");
	lstData.addColumn(QVariant::String,"PASSWORD");
	lstData.addColumn(QVariant::String,"FIRST_NAME");
	lstData.addColumn(QVariant::String,"LAST_NAME");
	lstData.addColumn(QVariant::String,"COMPANY");
	lstData.addColumn(QVariant::String,"SMS");
	lstData.addColumn(QVariant::String,"EMAIL");
	lstData.addColumn(QVariant::String,"LAST_ACCESS_PROG_CODE");
	lstData.addColumn(QVariant::String,"SALUT");
}

void SettingsManager::DefineFolders(DbRecordSet &lstData)
{
	lstData.destroy();
	lstData.addColumn(QVariant::String,"ALIAS");
	lstData.addColumn(QVariant::String,"PATH");
	//lstData.addColumn(QVariant::String,"FILETYPES"); //";" delimited
	lstData.addColumn(QVariant::Int,"FILETYPES_CODE"); 
	lstData.addColumn(QVariant::ByteArray,"THUMB"); 
	lstData.addColumn(QVariant::ByteArray,"THUMB_LARGE"); 
	lstData.addColumn(QVariant::Int,"STATUS");
	lstData.addColumn(QVariant::Int,"PIC_CNT");
	lstData.addColumn(QVariant::String,"IPHOTO_ID"); //0 by default, if non 0 then iphoto album
	lstData.addColumn(QVariant::Int,"THUMB_LARGE_WIDTH"); 
	lstData.addColumn(QVariant::Int,"THUMB_LARGE_HEIGHT"); 
	
}

void SettingsManager::DefineUserRights(DbRecordSet &lstData)
{
	lstData.destroy();
	lstData.addColumn(QVariant::String,"USERNAME");
	lstData.addColumn(QVariant::String,"ALIAS");
	lstData.addColumn(QVariant::Int,"CAN_DOWNLOAD");
	lstData.addColumn(QVariant::Int,"CAN_UPLOAD");
	lstData.addColumn(QVariant::Int,"CAN_DELETE");
	lstData.addColumn(QVariant::Int,"CAN_SEND_INVITE");
	lstData.addColumn(QVariant::Int,"CAN_SEND_EMAIL");
}
void SettingsManager::CreateDefaultUserRights(DbRecordSet &rowData)
{
	DefineUserRights(rowData);
	rowData.addRow();
	rowData.setData(0,"CAN_DOWNLOAD",1);
	rowData.setData(0,"CAN_UPLOAD",1);
	rowData.setData(0,"CAN_DELETE",1);
	rowData.setData(0,"CAN_SEND_INVITE",1);
	rowData.setData(0,"CAN_SEND_EMAIL",1);
}

void SettingsManager::DefineEmailSettings(DbRecordSet &lstData)
{
	lstData.destroy();
	lstData.addColumn(QVariant::String,"SMTP_SERVER");
	lstData.addColumn(QVariant::String,"EMAIL_ADDRESS");
	lstData.addColumn(QVariant::String,"USERNAME");
	lstData.addColumn(QVariant::String,"PASSWORD");
	lstData.addColumn(QVariant::ByteArray,"SIGNATURE");
	lstData.addColumn(QVariant::Int,"SMTP_PORT");
}

void SettingsManager::DefineGeneralSettings(DbRecordSet &lstData)
{
	lstData.destroy();
	lstData.addColumn(QVariant::String,"ADMIN_USER");
}


void SettingsManager::AddFileToShadowList(QString strAlias,QString strFileName,QString strDisplayName, int nHide)
{
	//QWriteLocker lock(&m_lckMetaData);

	m_lstShadow.addRow();
	m_lstShadow.setData(m_lstShadow.getRowCount()-1,"ALIAS",strAlias);
	m_lstShadow.setData(m_lstShadow.getRowCount()-1,"FILENAME",strFileName);
	m_lstShadow.setData(m_lstShadow.getRowCount()-1,"DISPLAYNAME",strDisplayName);
	m_lstShadow.setData(m_lstShadow.getRowCount()-1,"IS_HIDDEN",nHide);
}

void SettingsManager::RemoveFileFromShadowList(QString strAlias,QString strFileName)
{
	QWriteLocker lock(&m_lckMetaData);

	if(m_lstShadow.find("ALIAS",strAlias)>0)
	{
		int nRow=m_lstShadow.find("FILENAME",strFileName,true,true);
		if (nRow>=0)
			m_lstShadow.deleteRow(nRow);
	}
}

void SettingsManager::HideFile(QString strAlias,QString strFileName,int nHide)
{
	QWriteLocker lock(&m_lckMetaData);

	if(m_lstShadow.find("ALIAS",strAlias)>0)
	{
		int nRow=m_lstShadow.find("FILENAME",strFileName,true,true);
		if (nRow<0)
		{
			AddFileToShadowList(strAlias,strFileName,strFileName,nHide);
		}
		else
		{
			m_lstShadow.setData(nRow,"IS_HIDDEN",nHide);
		}
	}
	else
	{
		AddFileToShadowList(strAlias,strFileName,strFileName,nHide);
	}
}

void SettingsManager::GetHidePictureList(QString strAlias,DbRecordSet &Ret_Files)
{
	QReadLocker lock(&m_lckMetaData);
	Ret_Files.copyDefinition(m_lstShadow);
	if(m_lstShadow.find("ALIAS",strAlias)>0)
	{
		m_lstShadow.find("IS_HIDDEN",1,false,true);
		Ret_Files.merge(m_lstShadow,true);
	}
}

int	SettingsManager::GetHidePictureCnt( QString strAlias)
{
	QReadLocker lock(&m_lckMetaData);
	if(m_lstShadow.find("ALIAS",strAlias)>0)
	{
		return m_lstShadow.find("IS_HIDDEN",1,false,true); //just return hidden count
	}

	return 0;
}


void SettingsManager::RenameFile(QString strAlias,QString strFileName,QString strDisplayName)
{
	QWriteLocker lock(&m_lckMetaData);

	if(m_lstShadow.find("ALIAS",strAlias)>0)
	{
		int nRow=m_lstShadow.find("FILENAME",strFileName,true,true);
		if (nRow<0)
		{
			AddFileToShadowList(strAlias,strFileName,strDisplayName,0);
		}
		else
		{
			m_lstShadow.setData(nRow,"DISPLAYNAME",strDisplayName);
		}
	}
	else
	{
		AddFileToShadowList(strAlias,strFileName,strDisplayName,0);
	}


}

void SettingsManager::RenameCatalogInShadowList(QString strAlias,QString strNewName)
{
	QWriteLocker lock(&m_lckMetaData);

	int nSize=m_lstShadow.getRowCount();
	for (int i=nSize-1;i>=0;i--)
	{
		if (strAlias==m_lstShadow.getDataRef(i,"ALIAS").toString())
			m_lstShadow.setData(i,"ALIAS",strNewName);
	}

}

//filter + set names:
void SettingsManager::FilterFileList(DbRecordSet &lstData,QString strAliasDir)
{
	//by default set all display name=filename
	int nSize=lstData.getRowCount();
	for (int i=nSize-1;i>=0;i--)
	{
		QString strFile=lstData.getDataRef(i,"FILENAME").toString();
		lstData.setData(i,"DISPLAYNAME",strFile);
	}

	QReadLocker lock(&m_lckMetaData);
	if (m_lstShadow.getRowCount()==0)
		return;

	DbRecordSet lstTemp;
	lstTemp.copyDefinition(m_lstShadow);
	if(m_lstShadow.find("ALIAS",strAliasDir)<=0)
		return;
	lstTemp.merge(m_lstShadow,true);
	
	lock.unlock();

	for (int i=nSize-1;i>=0;i--)
	{
		QString strFile=lstData.getDataRef(i,"FILENAME").toString();
		int nRow=lstTemp.find("FILENAME",strFile,true);
		if (nRow>=0)
		{
			if (lstTemp.getDataRef(nRow,"IS_HIDDEN").toInt()!=0)
			{
				qDebug()<<"hidden pic:"<<strFile;

				lstData.deleteRow(i);
			}
			else
				lstData.setData(i,"DISPLAYNAME",lstTemp.getDataRef(nRow,"DISPLAYNAME"));
		}
	}

}

bool SettingsManager::LoadMetaData()
{
	bool bOK=true;
	QString strPath=m_strSettingsPath+"/settings/meta_data";
	m_IniFileMeta.SetPath(strPath);

	DbRecordSet lstData;
	if(!QFile::exists(strPath))
	{
		m_lstShadow.destroy();
		m_lstShadow.addColumn(QVariant::String,"USERNAME");
		m_lstShadow.addColumn(QVariant::String,"ALIAS");
		m_lstShadow.addColumn(QVariant::String,"FILENAME");
		m_lstShadow.addColumn(QVariant::String,"DISPLAYNAME");
		m_lstShadow.addColumn(QVariant::Int,"IS_HIDDEN");

		QString strData=QString(XmlUtil::ConvertRecordSet2ByteArray_Fast(m_lstShadow));
		m_IniFileMeta.SetValue("MAIN","REC_SHADOW_LIST",strData);

		if(!m_IniFileMeta.SaveEnc())
			return m_IniFileMeta.SaveEnc();
		else
			return true;
	}
	else
	{

		if(!m_IniFileMeta.LoadEnc())
			bOK=m_IniFileMeta.LoadEnc();

		if (!bOK) return false;

		QString strData;
		m_IniFile.GetValue("MAIN","REC_SHADOW_LIST",strData);
		QByteArray data = strData.toLatin1();
		m_lstShadow=XmlUtil::ConvertByteArray2RecordSet_Fast(data);

		m_IniFileMeta.Clear(); //save mem

		if (m_lstShadow.getColumnCount()==0)
		{
			m_lstShadow.destroy();
			m_lstShadow.addColumn(QVariant::String,"USERNAME");
			m_lstShadow.addColumn(QVariant::String,"ALIAS");
			m_lstShadow.addColumn(QVariant::String,"FILENAME");
			m_lstShadow.addColumn(QVariant::String,"DISPLAYNAME");
			m_lstShadow.addColumn(QVariant::Int,"IS_HIDDEN");
		}
	}

	return bOK;
}

bool SettingsManager::SaveMetaData()
{
	QString strData=QString(XmlUtil::ConvertRecordSet2ByteArray_Fast(m_lstShadow));
	m_IniFileMeta.SetValue("MAIN","REC_SHADOW_LIST",strData);

	if(!m_IniFileMeta.SaveEnc())
		return m_IniFileMeta.SaveEnc();
	else
		return true;
}



void SettingsManager::SetSettingsChangedNeedReloadFlag()
{
	QWriteLocker lock(&m_lckSettingsChanged);
	m_nSettingsChanged=1;
}
int SettingsManager::GetSettingsChangedNeedReloadFlag()
{
	QReadLocker lock(&m_lckSettingsChanged);
	return m_nSettingsChanged;
}

void SettingsManager::ClearSettingsChangedNeedReloadFlag()
{
	QWriteLocker lock(&m_lckSettingsChanged);
	m_nSettingsChanged=0;
}
void SettingsManager::NotifyClientSettingsChangedNeedReload()
{
	QFileInfo info_ini(m_strIniPath);
	QString strFileName=info_ini.absolutePath()+"/settings_reload";
	QFile file(strFileName);
	if(file.open(QIODevice::WriteOnly))
	{
		file.write("reload");
		file.close();
	}
}

void SettingsManager::ActualizeData()
{
	
	if (QString(APPLICATION_VERSION) > QString("V2011-001.000.058"))
	{//add email port:
		if (m_lstEmailSettings.getColumnIdx("SMTP_PORT")<0)
		{
			m_lstEmailSettings.addColumn(QVariant::Int,"SMTP_PORT");
			m_lstEmailSettings.setColValue("SMTP_PORT",25);
		}
	}

	if (QString(APPLICATION_VERSION) > QString("V2011-001.000.090"))
	{
		if (m_lstFolders.getColumnIdx("THUMB_LARGE_WIDTH")<0)
		{
			m_lstFolders.addColumn(QVariant::Int,"THUMB_LARGE_WIDTH");
			m_lstFolders.addColumn(QVariant::Int,"THUMB_LARGE_HEIGHT");
		}
	}


	if (m_lstGeneralSettings.getColumnIdx("ADMIN_USER")<0)
	{
		m_lstGeneralSettings.addColumn(QVariant::String,"ADMIN_USER");
	}
	if (m_lstGeneralSettings.getRowCount()==0)
	{
		m_lstGeneralSettings.addRow();
		m_lstGeneralSettings.setData(0,"ADMIN_USER","admin");
	}



}