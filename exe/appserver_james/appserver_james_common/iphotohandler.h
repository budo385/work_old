#ifndef IPHOTOHANDLER_H
#define IPHOTOHANDLER_H

#include <QSqlDatabase>
#include <QFileInfoList>

class iPhotoAlbum
{
public:
	QString m_strName;
	QString m_strID;
};

class iPhotoEntry
{
public:
	QString m_strPath;
};

class iPhotoHandler
{
public:
	iPhotoHandler():m_pDb(NULL), m_strDbName(), m_bIPhoto11(false) { TestVersion(); };
	~iPhotoHandler(){Logout();};
	bool Login();
	void Logout();
	bool IsLogged();

	bool GetAlbums(QList<iPhotoAlbum> &lstResult);

	bool GetAlbumPictures(QString &strAlbumID, QList<iPhotoEntry> &lstResult);
	static bool GetAlbumPicturesExt(QString &strAlbumID, QFileInfoList &lstFiles);

protected:
	QString GetDbName();
	QString GetPicFolder();
	void TestVersion();

protected:
	QSqlDatabase *m_pDb;
	QString m_strDbName;
	bool m_bIPhoto11;
};

#endif // EMAILHANDLER_H
