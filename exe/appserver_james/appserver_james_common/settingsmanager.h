#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include "common/common/dbrecordset.h"
#include "common/common/encryptedinifile.h"
#include "common/common/status.h"
#include <QReadWriteLock>


// saves all data in encyrpted ini file:
// master_pass
// guest_pass
// users, folders, userright table as xml



class SettingsManager
{
public:

	enum UserOperations
	{
		OP_DOWNLOAD,
		OP_UPLOAD,
		OP_DELETE,
		OP_SEND_INVITE,
		OP_SEND_EMAIL
	};

	enum ModuleOperations
	{
		OP_MORE_THEN_ONE_CATALOG,
		OP_VIEW_RAW_FILES,
	};

	SettingsManager();
	~SettingsManager();


	void Initialize(QString strPath);

	bool LoadSettings();
	bool SaveSettings();

	void		GetRootPass(QString &strPass);
	void		SetRootPass(QString strPass);
	void		GetServerThumb(QByteArray &byteThumb,QByteArray &byteThumbLarge);
	QDateTime	GetServerThumbLastModify(){return m_datServerThumbModify;}
	void		SetServerThumb(QByteArray byteThumb,QByteArray byteThumbLarge);
	QString		GetServerName();
	void		SetServerName(QString strName);


	void GetFolders(DbRecordSet &lstFolders);
	void GetUserRights(DbRecordSet &lstUserRights);
	void GetUsers(DbRecordSet &lstUsers);
	void SetUsers(DbRecordSet &lstUsers);
	void GetEmailSettings(DbRecordSet &lstData);
	void SetEmailSettings(DbRecordSet &lstData);
	void SetFolders(DbRecordSet &lstFolders);
	void SetUserRights(DbRecordSet &lstUserRights);

	//--------meta data file ops on server:----------------------
	void RemoveFileFromShadowList(QString strAlias,QString strFileName);
	void RenameFile(QString strAlias,QString strFileName,QString strDisplayName);
	void HideFile(QString strAlias,QString strFileName,int nHide);
	void FilterFileList(DbRecordSet &lstData,QString strAliasDir);
	void RenameCatalogInShadowList(QString strAlias,QString strNewName);
	void GetHidePictureList(QString strAlias,DbRecordSet &Ret_Files);
	int	 GetHidePictureCnt(QString strAlias);
	bool LoadMetaData();
	bool SaveMetaData();

	void GetDirectoryPath(Status &pStatus,QString strAliasPath, QString &strAlias, QString &strDirPath,int &nFileTypesCode,QString &strAllowedFileTypes, QString &strAlbumIPhotoID);

	bool GetUserRightRecord(QString strUserName,QString strAlias,DbRecordSet &rowUserRights);
	bool TestUserRight(QString strUserName,QString strAlias,int nOperation);
	bool TestModuleRight(int nProgCode,int nOperation);
	bool AuthenticateUser(QString strUserName,QString strPass);
	bool GetUserPassword(QString strUserName,QByteArray &bytePass);

	static void DefineUsers(DbRecordSet &lstData);
	static void DefineFolders(DbRecordSet &lstData);
	static void DefineUserRights(DbRecordSet &lstData);
	static void DefineEmailSettings(DbRecordSet &lstData);
	static void CreateDefaultUserRights(DbRecordSet &rowData);
	static void DefineGeneralSettings(DbRecordSet &lstData);

	void	SetSettingsChangedNeedReloadFlag();
	int		GetSettingsChangedNeedReloadFlag();
	void	ClearSettingsChangedNeedReloadFlag();
	void	NotifyClientSettingsChangedNeedReload();

	QString GetAdminUserName();
	void SetAdminUserName(QString strAdminUserName);



private:
	void ActualizeData();
	void AddFileToShadowList(QString strAlias,QString strFileName,QString strDisplayName, int nHide);
	void CreateDefaults();

	//variables are mutex protected: server can be signalled to reload...
	DbRecordSet m_lstUsers;
	DbRecordSet m_lstFolders;
	DbRecordSet m_lstUserRights;
	DbRecordSet m_lstEmailSettings;
	DbRecordSet m_lstShadow;
	DbRecordSet m_lstGeneralSettings;

	QString		m_strRootPassPlain;
	QByteArray	m_byteRootPass;
	QString		m_strAppServerID;
	QString		m_strSignature;
	
	QByteArray	m_byteServerThumb;
	QByteArray	m_byteServerThumb_Large;
	QDateTime	m_datServerThumbModify;
	QString		m_strServerName;

	QReadWriteLock m_lckVars;
	QReadWriteLock m_lckMetaData;


	EncryptedIniFile	m_IniFileMeta;
	EncryptedIniFile	m_IniFile;
	QString				m_strIniPath;
	QString				m_strSettingsPath;

	QReadWriteLock		m_lckSettingsChanged;
	int					m_nSettingsChanged;
};

#endif // SETTINGSMANAGER_H
