#!/bin/bash
# Script to build everPICs-Bridge package of Mac OS X

CERT_NAME="3rd Party Mac Developer Application: Helix Business-Soft AG"
CERT_INSTALLER="3rd Party Mac Developer Installer: Helix Business-Soft AG"
APP_NAME=everPICs-Bridge
APP_BUNDLE=$APP_NAME.app
SETUP_NAME=EverPICsBridgeAppStore
APP_BUNDLE_ID=com.everPICs.everPICsBridge 
APP_VERSION=1.0.0

echo "------------------------------------------------------------"
echo " Compiling $APP_NAME"
echo "------------------------------------------------------------"

# call with "./compile.sh sign_only" to skip compilation part 
if [[ "$1" != "sign_only" ]]
then

#before updating, store (our own) script's date
OLD=`stat -f '%m' $0`

#update all sources from svn
cd ../../
svn up
cd exe/appserver_james

# compare it with old
NEW=`stat -f '%m' $0`
if [ "$NEW" != "$OLD" ]; then
	echo "Compile script has changed, restarting"
	exec bash $0
	exit
fi

chmod +x ./compile.sh

#cleanup builds
rm -rf ./Debug

echo "Compiling NatTraversal"
make -C ../../lib/NATTraversal/NATTraversal/libnatpmp/ clean libnatpmp.a  || exit 1
make -C ../../lib/NATTraversal/NATTraversal/miniupnpc/ clean libminiupnpc.a   || exit 1
cp ../../lib/NATTraversal/NATTraversal/libnatpmp/libnatpmp.a ./ || exit 1
cp ../../lib/NATTraversal/NATTraversal/miniupnpc/libminiupnpc.a ./ || exit 1
rm -f ../../lib/NATTraversal/Makefile
qmake -makefile  -spec macx-g++ -Wall -o ../../lib/NATTraversal/Makefile ../../lib/NATTraversal/NATTraversal/NATTraversal.pro
make -C ../../lib/NATTraversal all  || exit 1
cp ../../lib/NATTraversal/libNATTraversal.a  ./ || exit 1

echo "Compiling common"
rm -rf ../../lib/common/common/Debug
rm -f ../../lib/common/common/Makefile
qmake -makefile  -spec macx-g++ -Wall -o ../../lib/common/common/Makefile ../../lib/common/common/common.pro || { echo "Failed generating Makefile"; exit 1; }
make -C ../../lib/common/common/ clean all  || exit 1
cp ../../lib/debug/libcommon.a  ./ || exit 1

echo "Compiling trans"
rm -f ../../lib/trans/trans/Makefile
qmake -makefile  -spec macx-g++ -Wall -o ../../lib/trans/trans/Makefile ../../lib/trans/trans/trans.pro
make -C ../../lib/trans/trans/ clean all  || exit 1
cp ../../lib/debug/libtrans.a  ./ || exit 1

echo "Compiling appserver_james_common"
rm -f ./appserver_james_common/Makefile
qmake -makefile  -spec macx-g++ -Wall -o ./appserver_james_common/Makefile ./appserver_james_common/appserver_james_common.pro
make -C ./appserver_james_common/ clean all  || exit 1
cp ./Debug/libappserver_james_common.a  ./ || exit 1

echo "Compiling db"
rm -f ../../lib/common/common/Makefile
qmake -makefile  -spec macx-g++ -Wall -o ../../lib/common/common/Makefile ../../lib/common/common/common.pro
make -C ../../lib/common/common/ clean all  || exit 1
cp ../../lib/debug/libcommon.a  ./ || exit 1

echo "Compiling GenTool"
rm -f ../../tool/GenTool/GenTool/Makefile
qmake -makefile  -spec macx-g++ -Wall -o ../../tool/GenTool/GenTool/Makefile ../../tool/GenTool/GenTool/GenTool.pro
make -C ../../tool/GenTool/GenTool/ clean all  || exit 1
#cp ../../lib/debug/libcommon.a  ./ || exit 1

echo "Final compilation"
rm -f ./Makefile
qmake -makefile  -spec macx-g++ -Wall -o Makefile ./appserver_james.pro
make clean all || { echo "appserver_james compilation failed"; exit 1; }

echo "Compiling RawLib"
cd ../../tool/RawLib/
make clean all || echo "RawLib compilation failed"
cd ../../exe/appserver_james
mkdir -m 755 -p ./Debug/$APP_BUNDLE/Contents/MacOS/
cp -f ../../tool/RawLib/bin/RawLib  ./Debug/$APP_BUNDLE/Contents/MacOS/  || echo "RawLib copy failed"

echo "Deploying app"
# do the bundle thing on all other non-main Qt programs so they will be relinked to framework OQ
macdeployqt ./Debug/GenTool.app
macdeployqt ./Debug/everPICs-Service.app

#copy all the required files to the everPICs-Service.app bundle
echo "Copy files to bundle"
cp ./Debug/GenTool.app/Contents/MacOS/GenTool  ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
#cp ./Debug/everPICs-Bridge.app/Contents/MacOS/everPICs-Bridge  ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
cp ./Debug/everPICs-Service.app/Contents/MacOS/everPICs-Service  ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
cp ../../_data/Translation/EXB_Manager_de.qm ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
cp ../../deploy/Setup/FFHServer/webdata.bin ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
#cp ../../exe/appserver_james/appserver_james_gui/Resources/Sokrates.icns ./Debug/$APP_BUNDLE/Contents/Resources/ || echo "Some files were not copied"

mkdir -m 755 -p ./Debug/$APP_BUNDLE/Contents/MacOS/
cp -pRv ../../_data/DefSettings/appserver ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings > /dev/null || echo "Some files were not copied"
#delete .svn folders
find ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/ -name ".svn" | xargs rm -rf \;

#copies all the Qt dependencies into the main bundle
macdeployqt ./Debug/$APP_BUNDLE

mkdir -p ./Debug/Scripts
cp -R ../../deploy/Setup/FFHServer/uninstallosx.sh ./Debug/Scripts/preupgrade
chmod +x ./Debug/Scripts/preupgrade

# ne radi!
#cp -R ../../deploy/Setup/FFHServer/postinstallosx.sh ./Debug/Scripts/postflight
#chmod +x ./Debug/Scripts/postflight

mkdir -p ./Debug/Resources
cp -R ../../deploy/Setup/FFHServer/license.txt ./Debug/Resources/License.txt

# use our own Info file to overwrite auto-generated one
cp -R ../../deploy/Setup/FFHServer/info.plist ./Debug/$APP_BUNDLE/Contents/info.plist

cp -R ../../_data/EXB_EmailTemplates/everPICs_Invitation.html ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/everPICs_Invitation.html

cp -R ../../_data/EXB_EmailTemplates/everPICs_Doc.html ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/everPICs_Doc.html

cp -R ../../_data/Deploy/AppServerDeploy/settings/server.cert ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/server.cert

cp -R ../../_data/Deploy/AppServerDeploy/settings/server.pkey ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/server.pkey

# remove OSX resource forks
find ./Debug/$APP_BUNDLE/ -name ".DS_Store" -exec rm -f {} \;

# NOTE: this non-signed version is for ordinary download (not for MacStore!!!)
# create flat package (.pkg)
/Developer/Applications/Utilities/PackageMaker.app/Contents/MacOS/PackageMaker -AppleLanguages "(English)" -r ./Debug/$APP_BUNDLE/ -o ./Debug/EverPICsBridgeInstall.pkg -v $APP_VERSION -t "everPICs Bridge Setup" -i $APP_BUNDLE_ID --no-relocate --verbose --target 10.5 --scripts ./Debug/Scripts/ --resources ./Debug/Resources/

fi

echo "signing the APP";
# "Each of these must be signed independently"
# http://www.manpagez.com/man/1/codesign/
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/MacOS/everPICs-Bridge  || echo "Signing everPICs-Bridge failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/MacOS/everPICs-Service  || echo "Signing everPICs-Service failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/MacOS/GenTool  || echo "Signing GenTool failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/MacOS/RawLib  || echo "Signing RawLib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtCore.framework/Versions/4/QtCore   || echo "Signing QtCore failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtDeclarative.framework/Versions/4/QtDeclarative  || echo "Signing QtDeclarative failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtGui.framework/Versions/4/QtGui || echo "Signing QtGui failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtNetwork.framework/Versions/4/QtNetwork  || echo "Signing QtNetwork failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtScript.framework/Versions/4/QtScript || echo "Signing QtScript failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtSql.framework/Versions/4/QtSql || echo "Signing QtSql failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtSvg.framework/Versions/4/QtSvg || echo "Signing QtSvg failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtXml.framework/Versions/4/QtXml  || echo "Signing QtXml failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtXmlPatterns.framework/Versions/4/QtXmlPatterns  || echo "Signing QtXmlPatterns failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/accessible/libqtaccessiblewidgets.dylib   || echo "Signing libqtaccessiblewidgets.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/bearer/libqgenericbearer.dylib  || echo "Signing libqgenericbearer.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/codecs/libqcncodecs.dylib   || echo "Signing libqcncodecs.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/codecs/libqjpcodecs.dylib   || echo "Signing libqjpcodecs.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/codecs/libqkrcodecs.dylib   || echo "Signing libqkrcodecs.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/codecs/libqtwcodecs.dylib   || echo "Signing libqtwcodecs.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/graphicssystems/libqtracegraphicssystem.dylib    || echo "Signing libqtracegraphicssystem.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqgif.dylib     || echo "Signing libqgif.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqico.dylib     || echo "Signing libqico.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqjpeg.dylib     || echo "Signing libqjpeg.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqmng.dylib     || echo "Signing libqmng.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqtiff.dylib     || echo "Signing libqtiff.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/qmltooling/libqmldbg_tcp.dylib      || echo "Signing libqmldbg_tcp.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/sqldrivers/libqsqlite.dylib       || echo "Signing libqsqlite.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/sqldrivers/libqsqlodbc.dylib        || echo "Signing libqsqlodbc.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/sqldrivers/libqsqlpsql.dylib         || echo "Signing libqsqlpsql.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE         || echo "Signing entire bundle failed";

# automatically sign all executable files
#echo "Automatic binary signing code"
#TOFIX no xarg on OSx
#find ./Debug/$APP_BUNDLE/ -type f -perm u+x -print | xarg codesign -s "$CERT_NAME" -fv {} || echo "Signing {} failed"

#http://developer.apple.com/devcenter/mac/documents/submitting.html
#Quote: "Note: Using the PackageMaker application to archive your application is not supported"
productbuild --component ./Debug/$APP_BUNDLE /Applications --sign "$CERT_INSTALLER" ./Debug/$SETUP_NAME.pkg

# use installer to test the archive validity
# http://developer.apple.com/library/mac/#documentation/Darwin/Reference/ManPages/man8/installer.8.html
echo "Testing the installer"
say "Testing the installer"
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo "helix1" | sudo installer -verbose -store -pkg ./Debug/$SETUP_NAME.pkg  -target /