#include "dlgfolders.h"
#include <QDir>
#include <QStyle>
#include <QDebug>
#include "pichelper.h"
#include "iphotohandler.h"
#include "filecruncher.h"
#include <QMessageBox>


#ifdef _WIN32
	#include <QCommonStyle>
	#include <shlobj.h>
#else
	#include <QCommonStyle>
#endif



DlgFolders::DlgFolders(QWidget *parent,QString strTitle,bool bIPhotoMode)
	: QDialog(parent)
{
	ui.setupUi(this);
	//ui.lineSelectedFolders->setEnabled(false);
	setWindowTitle(strTitle);
	this->setStyleSheet(PicHelper::GetDialogStyle());

	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnCancel->setStyleSheet(strStyleBtn);
	ui.btnOK->setStyleSheet(strStyleBtn);


	m_bIPhotoMode=bIPhotoMode;

	QIcon icon;
#ifdef _WIN32
	QCommonStyle style;
	icon = style.standardIcon(QStyle::SP_DriveHDIcon);
	m_icon_DirOpen = style.standardIcon(QStyle::SP_DirOpenIcon);
	m_icon_DirClose = style.standardIcon(QStyle::SP_DirClosedIcon);
#else
	QCommonStyle style;
	icon= style.standardIcon(QStyle::SP_DriveHDIcon);
	m_icon_DirOpen = style.standardIcon(QStyle::SP_DirOpenIcon);
	m_icon_DirClose = style.standardIcon(QStyle::SP_DirClosedIcon);
#endif

	if (m_bIPhotoMode)
		return;



	FileCruncher::GetDrives(m_lstDriveNames,m_lstDriveLetters,m_lstPaths,m_lstTypes);


	//get all drives -> set as root items. ? folder icon ?
	int nSize=m_lstDriveNames.size();
	for(int i=0;i<nSize;++i)
	{
		QTreeWidgetItem *pFolder = new QTreeWidgetItem;

		pFolder->setText(0,	m_lstDriveNames.at(i));
		pFolder->setIcon(0, icon);
		pFolder->setData(0,	Qt::ToolTipRole, m_lstDriveLetters.at(i));
		pFolder->setData(0,	Qt::UserRole, m_lstPaths.at(i));
		pFolder->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
		pFolder->setCheckState(0, Qt::Unchecked);
		ui.treeFolders->addTopLevelItem(pFolder);

		//skip count of dirs on 1st level and show + sign, afterwards try to count on click
		if (m_lstTypes.at(i)==JOHN_DRIVE_REMOTE || m_lstTypes.at(i)==JOHN_DRIVE_REMOVABLE)
		{
			pFolder->setChildIndicatorPolicy(QTreeWidgetItem::ShowIndicator);
		}
		else
		{
			FillSubDirs(m_lstDriveLetters.at(i),pFolder);
		}
	}
	
	connect(ui.treeFolders,SIGNAL(itemChanged(QTreeWidgetItem *, int )),this,SLOT(OnItemChanged(QTreeWidgetItem *, int )));
	connect(ui.treeFolders,SIGNAL(itemExpanded(QTreeWidgetItem *)),this,SLOT(OnItemExpanded(QTreeWidgetItem *)));
	connect(ui.treeFolders,SIGNAL(itemCollapsed(QTreeWidgetItem *)),this,SLOT(OnItemCollapsed(QTreeWidgetItem *)));



	ui.treeFolders->setStyleSheet(PicHelper::GetTreeStyle());


	//on item change: checked set in text edit
	//on item expanded-> get 1 level inside...
}

DlgFolders::~DlgFolders()
{



}

bool DlgFolders::GetIPhotoAlbums()
{
	if (!m_bIPhotoMode)
		return false;
	
	//test iPhoto integration
	iPhotoHandler iPhotoObj;
	if(!iPhotoObj.Login())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Could not log on to the iPhoto repository!"));
		return false;
	}

	QList<iPhotoAlbum> lstAlbums;
	if(!iPhotoObj.GetAlbums(lstAlbums))
	{
		QMessageBox::warning(this,tr("Warning"),tr("Could not fetch albums from the iPhoto repository!"));
		return false;
	}


	int nSize=lstAlbums.size();
	for(int i=0;i<nSize;++i)
	{
		QTreeWidgetItem *pFolder = new QTreeWidgetItem;
		pFolder->setText(0,	lstAlbums.at(i).m_strName);
		pFolder->setIcon(0, m_icon_DirClose);
		pFolder->setData(0,	Qt::ToolTipRole, lstAlbums.at(i).m_strName);
		pFolder->setData(0,	Qt::UserRole, lstAlbums.at(i).m_strID);
		pFolder->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
		pFolder->setCheckState(0, Qt::Unchecked);
		ui.treeFolders->addTopLevelItem(pFolder);
	}
	

	connect(ui.treeFolders,SIGNAL(itemChanged(QTreeWidgetItem *, int )),this,SLOT(OnItemChanged(QTreeWidgetItem *, int )));
	ui.treeFolders->setStyleSheet(PicHelper::GetTreeStyle());

	return true;

}

void DlgFolders::OnItemChanged(QTreeWidgetItem *item, int column)
{
	int nCheckState = item->checkState(0);

	if (!m_bIPhotoMode)
	{
		if (nCheckState==Qt::Checked)
		{
			QString strPath=item->data(0,Qt::ToolTipRole).toString();
			int nRow = m_lstFolders.indexOf(strPath);
			if (nRow<0)
			{
				m_lstFolders.append(strPath);
				m_lstAlbumIDs.append("");
			}
		}
		else
		{
			QString strPath=item->data(0,Qt::ToolTipRole).toString();
			int nRow = m_lstFolders.indexOf(strPath);
			if (nRow>=0)
			{
				m_lstFolders.removeAt(nRow);
				m_lstAlbumIDs.removeAt(nRow);
			}
		}
	}
	else
	{
		if (nCheckState==Qt::Checked)
		{
			QString strAlbumID = item->data(0,Qt::UserRole).toString();
			int nRow = m_lstAlbumIDs.indexOf(strAlbumID);
			if (nRow<0)
			{
				m_lstFolders.append(item->data(0,Qt::ToolTipRole).toString());
				m_lstAlbumIDs.append(item->data(0,Qt::UserRole).toString());
			}
		}
		else
		{
			QString strAlbumID = item->data(0,Qt::UserRole).toString();
			int nRow = m_lstAlbumIDs.indexOf(strAlbumID);
			if (nRow>=0)
			{
				m_lstFolders.removeAt(nRow);
				m_lstAlbumIDs.removeAt(nRow);
			}
		}
	}


	//refresh dir entry field:
	int nSize= m_lstFolders.size();
	QString strSelected;
	for (int i=0;i< nSize ;i++)
	{
		QFileInfo info (m_lstFolders.at(i));
		if (info.isRoot())
			strSelected += "\""+info.absoluteFilePath()+"\", ";
		else
			strSelected += "\""+info.baseName()+"\", ";
	
	}
	if (strSelected.length()>0)
		strSelected.chop(2);

	ui.lineSelectedFolders->setText(strSelected);

}

void DlgFolders::OnItemExpanded(QTreeWidgetItem *item)
{
	if (!item) return;

	if (item->parent()!=NULL)
		item->setIcon(0,m_icon_DirOpen);

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	if (item->childCount()==0)
	{
		FillSubDirs(item->data(0,Qt::UserRole).toString(),item);

		if (item->childCount()==0)
			item->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
	}


	int nCnt=item->childCount();
	for(int i=0;i<nCnt;++i)
	{
		QTreeWidgetItem *pFolder = item->child(i);
		if (pFolder->childCount()==0)
			FillSubDirs(pFolder->data(0,Qt::UserRole).toString(),pFolder);
	}
	QApplication::restoreOverrideCursor();
}

void DlgFolders::OnItemCollapsed(QTreeWidgetItem *item)
{
	if (item->parent()!=NULL)
		item->setIcon(0,m_icon_DirClose);
}




void DlgFolders::SetInitialFolder(QString strPath)
{
	if (strPath.isEmpty())
		return;

	if (!QFile::exists(strPath))
		return;

	//penetrate to the folder recurrence:
	QStringList lstParts=strPath.split("/");
	int nSize=lstParts.size();
	QTreeWidgetItem *pCurrentFolder=NULL;
	for(int i=0;i<nSize;++i)
	{

		if (i==0)
		{
			int nCntRoots=ui.treeFolders->topLevelItemCount();
			for(int i=0;i<nCntRoots;++i)
			{
				QTreeWidgetItem *pRootFolder = ui.treeFolders->topLevelItem(i);
				//qDebug()<<pRootFolder->text(0);
				if (pRootFolder->data(0,Qt::ToolTipRole).toString().contains(lstParts.at(0)))
				{
					pCurrentFolder=pRootFolder;
					break;
				}
			}
			if (!pCurrentFolder) //fail to find
				return;
			else
				continue;
		}

		//*qDebug()<<pCurrentFolder->text(0);
		pCurrentFolder = GetNodeByFolderName(pCurrentFolder,lstParts.at(i));
		//qDebug()<<pCurrentFolder->text(0);
		if (pCurrentFolder)
		{
			FillSubDirs(pCurrentFolder->data(0,Qt::ToolTipRole).toString(),pCurrentFolder);
			OnItemExpanded(pCurrentFolder);
		}
	}


	//select it:
	if (pCurrentFolder)
	{
		//qDebug()<<pCurrentFolder->text(0);
		//pCurrentFolder->setExpanded(true);
		//pCurrentFolder->setSelected(true);
		ui.treeFolders->setCurrentItem(pCurrentFolder);
		ui.treeFolders->expandItem(pCurrentFolder);
	}
}


QTreeWidgetItem* DlgFolders::GetNodeByFolderName(QTreeWidgetItem* item,QString strFolderName)
{
	if (!item)
		return NULL;

	int nCnt=item->childCount();
	for(int i=0;i<nCnt;++i)
	{
		QTreeWidgetItem *pFolder = item->child(i);
		if (pFolder->text(0)==strFolderName)
			return pFolder;
	}

	return NULL;
}



void DlgFolders::GetSelectedFolders(QStringList &lstFolders,QList<QString> &lstAlbumIDs /* only for iPhoto */)
{
	lstFolders = m_lstFolders;
	lstAlbumIDs = m_lstAlbumIDs;
}



void DlgFolders::FillSubDirs(QString strDirPath,QTreeWidgetItem *item)
{
	if (item->childCount()>0)
		return;

	QDir dir(strDirPath);
	if (!dir.exists())
		return;

	QFileInfoList lstFiles=dir.entryInfoList(QDir::AllDirs);
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		//skip current and parent directory:
		if (lstFiles.at(i).fileName()==".") continue;
		if (lstFiles.at(i).fileName()=="..") continue;

		//qDebug()<<lstFiles.at(i).absoluteFilePath();

		QTreeWidgetItem *pFolder = new QTreeWidgetItem;
		pFolder->setText(0,	lstFiles.at(i).fileName());
		pFolder->setIcon(0, m_icon_DirClose);
		QString strPath=lstFiles.at(i).absoluteFilePath();
		if (strPath.left(2)=="//")
			strPath=ReplaceUNCPath(strPath);
		pFolder->setData(0,	Qt::ToolTipRole, strPath);
		pFolder->setData(0,	Qt::UserRole, lstFiles.at(i).absoluteFilePath());
		pFolder->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
		pFolder->setCheckState(0, Qt::Unchecked);
		item->addChild(pFolder);
	}
}


bool DlgFolders::GetDriveNamesAndTypes(QStringList &lstDrives, QStringList &lstNames,QList<QStyle::StandardPixmap> &lstTypes)
{

#if defined _WIN32

	TCHAR szDrives[128];
	TCHAR *pDrive;

	TCHAR szVolNAme[256];
	//TCHAR *pDrive;

	DWORD r = GetLogicalDriveStrings(sizeof(szDrives), szDrives);
	if(r = 0 || r > sizeof(szDrives)) return false;

	pDrive = szDrives;  // Point to the first drive
	while(*pDrive != '\0')
	{
		switch(GetDriveType(pDrive))
		{
			case DRIVE_CDROM:
			case DRIVE_RAMDISK:
				lstTypes.append(QStyle::SP_DriveDVDIcon);
				break;
			case DRIVE_REMOVABLE:
				lstTypes.append(QStyle::SP_DriveFDIcon);
				break;
			case DRIVE_REMOTE:
				lstTypes.append(QStyle::SP_DriveNetIcon);
				break;
			default:
				lstTypes.append(QStyle::SP_DriveHDIcon);
		}

		if(GetVolumeInformation(pDrive,szVolNAme,sizeof(szVolNAme),NULL,NULL,NULL,NULL,NULL))
		{
			QString	strResult = QString::fromUtf16((const ushort *)szVolNAme);
			lstNames.append(strResult);
		}
		else
			lstNames.append("");

		QString strDrive = QString::fromUtf16((const ushort *)pDrive);
		lstDrives.append(strDrive);

		pDrive += 4;  // Point to the next drive
	}

	return true;

#else
	return false;
#endif 

}

QString DlgFolders::ReplaceUNCPath(QString strPath)
{
	int nSize=m_lstPaths.size();
	for (int i=0;i<nSize;i++)
	{
		if (strPath.indexOf(m_lstPaths.at(i))==0)
		{
			if (i<m_lstDriveLetters.size())
			{
				strPath.replace(m_lstPaths.at(i),m_lstDriveLetters.at(i));
				return QDir::cleanPath(strPath);
			}
		}
	}

	return strPath;
}



