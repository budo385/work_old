#ifndef UNIVERSALTABLEWIDGETEX_H
#define UNIVERSALTABLEWIDGETEX_H

#include <QTableWidget>
#include <QContextMenuEvent>
#include <QAction>
#include <QItemDelegate>
#include "common/common/dbrecordset.h"

//maps data from DbRecordSet to tablewidget: 
//- selection on widget produces changes in selection inside DbRecordSet
//- change of data is stored in DbRecordSet
//- when using custom widgets, implement signals for selection change & editing


class UniversalTableWidgetEx : public QTableWidget
{
	Q_OBJECT

public:

	enum ColumnType
	{
		COL_TYPE_TEXT,
		COL_TYPE_CHECKBOX,
		COL_TYPE_CUSTOM_WIDGET
	};

	enum DefaultContextActions
	{
		SHOW_CNXT_ALL=0,
		SHOW_CNXT_INSERT_ROW=1,
		SHOW_CNXT_DELETE_SELECTION=2,
		SHOW_CNXT_DUPLICATE_SELECTION=4,
		SHOW_CNXT_DELETE_ALL=8,
		SHOW_CNXT_SELECT_ALL=16,
		SHOW_CNXT_DESELECT_ALL=32,
		SHOW_CNXT_SORT_DIALOG=64
	};

	UniversalTableWidgetEx(QWidget *parent);
	~UniversalTableWidgetEx();

	void				Initialize(DbRecordSet *lstData,DbRecordSet *plstColumns=NULL,bool bSingleSelection=false,bool bSelectRows=true, int nVerticalRowSize=25);
	virtual void		SetEditMode(bool bEdit=true);
	bool				IsEditMode(){return m_bEdit;};
	void				EnableDrag(bool bEnable,int nDragMimeType=-1, QString strDragIcon="");
	void				EnableDrop(bool bEnable,int nDropMimeType=-1);
	void				SetContextMenuActions(QList<QAction*>& lstActions){m_lstActions=lstActions;};		//set actions for cnxt menu (alternative approach for setting context menu)
	QList<QAction*>&	GetContextMenuActions(){ return m_lstActions; };
	void				CreateDefaultContextMenuActions(QList<QAction*>& lstActions, int nShowFilter=SHOW_CNXT_ALL);
	void				ScrollToLastItem(bool bSelectLastRow=false);					//scroll to last item in table (use after add)
	void				ScrollToLastSelectedItem();										//scroll to last selected item in table
	void				ScrollToFirstItem(bool bSelectFirstRow=false);					//scroll to last item in table (use after add)
	static void			AddColumnToSetup(DbRecordSet &lstSetup,QString strColName,QString strHeaderName="",int nWidth=100,bool bEditable=true, QString strToolTip="",int nColType=COL_TYPE_TEXT,QString strDataFormat=QString());
	void				SetColumnSetup(DbRecordSet &lstColumnSetup);
	void				GetColumnSetup(DbRecordSet &lstColumnSetup);
	void				SetColumnEditable(int nColumn,bool bEditable);
	DbRecordSet*		GetDataSource(){return m_plstData;};
	void				GetSelection(DbRecordSet &lstDataSelected);
	void				StoreCustomWidgetChanges(){Table2Data();};

signals:
	void				SignalSelectionChanged();
	void				SignalDataChanged(int nRow, int nCol); 
	void				SignalRowInserted(int nRow); 
	void				SignalRowsDeleted(); 
	void				SignalContentChanged();
	void				SignalDataDroped(int nDropType, DbRecordSet &lstDropValue, QDropEvent *event);
	
public slots:
	virtual void		RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);
	virtual void		RefreshSelection(); 
	virtual void		SortMultiColumn(DbRecordSet lstCols/* in MVIEW_GRID_COLUMN_SETUP */);
	virtual void		InsertRow();
	virtual void		DeleteSelection();
	virtual void		DeleteTable();
	virtual void		SelectAll();
	virtual void		DeselectAll();
	virtual void		DuplicateSelection();
	virtual void		OpenSortDialog();

protected slots:
	virtual void		OnItemSelectionChanged();
	virtual void		OnCellEdited(int nRow, int nCol);
	virtual void		OnCellClicked(int nRow, int nCol);
	virtual void		OnCustomWidgetCellClicked(int nRow, int nCol); //call after custom widget cell clicked
	virtual void		OnSortColumn(int);

protected:
	virtual void		Data2Table(int nRow = -1);
	virtual void		Table2Data(int nRow = -1); //only for custom widgets
	virtual void		Data2CustomWidget(int nRow, int nCol){}; //implement if using COL_TYPE_CUSTOM_WIDGET
	virtual void		CustomWidget2Data(int nRow, int nCol){}; //implement if using COL_TYPE_CUSTOM_WIDGET
	virtual void		RefreshHorizontalHeaderSetup();
	virtual void		RefreshVerticalHeaderSetup(int nRow = -1);
	virtual void		GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip){};	//get icon to display on vertical header for specified row, override
	virtual void		SetStyleSheetGlobal();
	virtual void		GoEditCell(int nRow,int nCol);
	virtual void		contextMenuEvent(QContextMenuEvent *event);
	virtual QMimeData *		mimeData ( const QList<QTableWidgetItem *> items ) const;
	virtual QStringList		mimeTypes () const;
	virtual void			startDrag(Qt::DropActions supportedActions);
	virtual Qt::DropActions	supportedDropActions() const;
	virtual void			dropEvent(QDropEvent *event);
	virtual void			dragEnterEvent ( QDragEnterEvent * event );
	virtual void			dragMoveEvent ( QDragMoveEvent * event );

	DbRecordSet			m_lstColumnSetup;   //columns for display
	QHash<int,int>		m_lstColumnMapping; //mapping from table col 2 m_plstData col m_lstColumnMapping[table_row]=data_row //can be -1 
	DbRecordSet			*m_plstData;		//data
	bool				m_bEdit;			//true: all columns marked as editable can be edited
	QList<QAction*>		m_lstActions;		//cntx-menu actions
	SortDataList		m_lstLastSort;		//when sorted by user click, store last
	int					m_nDragMimeType;
	int					m_nDropMimeType;
	QString				m_strDragIcon;
};


class DummyNoFocusDelegate : public QItemDelegate
{
public:
	DummyNoFocusDelegate(QObject *parent):QItemDelegate(parent){};
	void drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const {};
};

#endif // UNIVERSALTABLEWIDGETEX_H
