#ifndef DLGSENDEMAILBODY_H
#define DLGSENDEMAILBODY_H

#include <QDialog>
#include "generatedfiles/ui_dlgsendemailbody.h"

class DlgSendEmailBody : public QDialog
{
	Q_OBJECT

public:
	DlgSendEmailBody(QWidget *parent = 0);
	~DlgSendEmailBody();

	void SetData(QString strSubject, QString strBody);
	void GetData(QString &strSubject, QString &strBody);

	private slots:
		void on_btnCancel_clicked();
		void on_btnOK_clicked();

private:
	Ui::DlgSendEmailBodyClass ui;
};

#endif // DLGSENDEMAILBODY_H
