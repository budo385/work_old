#include "styledpushbutton.h"
#include <QPixmap>
#include <QEvent>
#include <QMouseEvent>
#include <QApplication>
#include <QMovie>
#include <QDebug>
#include <QMessageBox>
#include <QGraphicsDropShadowEffect>

StyledPushButton::StyledPushButton(QWidget *parent,QString strIcon,QString strIconHover, QString strStyle,QString strTextHtml, int nIdent,int nSpacing, Qt::CursorShape hovercursor)
	: QWidget(parent),m_labelIcon(NULL),m_bUseMovieIcon(false),m_bEnableMovie(false),m_labelText(NULL)
{

	m_strCheckedStyle= " QWidget { border-width: 0px 3px 4px 0px; border-image:url(:Mobile_Det_Focus.png) 4px 4px 4px 4px stretch stretch }";
	m_bPressed=false;
	m_hovercursor=hovercursor;
	m_strIcon=strIcon;
	m_strIconHover=strIconHover;

	if (m_strIconHover!=m_strIcon)
		m_bHoverIconEnabled=true;
	else
		m_bHoverIconEnabled=false;

	m_pLayout = new QHBoxLayout();
	m_pLayout->setMargin(2);
	m_pLayout->setSpacing(nSpacing);
	m_pLayout->addSpacing(nIdent);
	if (!strIcon.isEmpty())
	{
		m_labelIcon = new QLabel;
		m_labelIcon->setPixmap(QPixmap(strIcon));
		m_pLayout->addWidget(m_labelIcon);
	}
	if (!strTextHtml.isEmpty())
	{
		m_labelText = new QLabel(this);
		m_labelText->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
		m_labelText->setTextInteractionFlags(Qt::NoTextInteraction);
		m_labelText->setText(strTextHtml);
		m_labelText->setStyleSheet("QLabel { background: transparent; }");
		
		QGraphicsDropShadowEffect * effect = new QGraphicsDropShadowEffect();
		effect->setXOffset(1);
		effect->setYOffset(1);
		effect->setColor(Qt::black);
		effect->setBlurRadius(3.0);
		m_labelText->setGraphicsEffect(effect);

		m_pLayout->addWidget(m_labelText);
		//connect(m_label,SIGNAL(Clicked(int)),this,SLOT(OnClick()));
	}
	m_pLayout->addStretch(1);
	//layout->setSpacing(0);
	m_pLayout->setContentsMargins(0,0,0,0);

	if ((!strIcon.isEmpty() && strTextHtml.isEmpty()) || (strIcon.isEmpty() && !strTextHtml.isEmpty()))
		m_pLayout->insertStretch(0,1);


	setLayout(m_pLayout);

	//style:
	setStyleSheet(strStyle);
	m_strDefaultStyle=strStyle;


	if (m_hovercursor!=Qt::ArrowCursor)
		setCursor(m_hovercursor);
}


StyledPushButton::StyledPushButton(QString strMovieIcon,QString strMovieFormat,bool bPlayMode,QWidget *parent,QString strStyle,QString strTextHtml, int nIdent,int nSpacing, Qt::CursorShape hovercursor)
: QWidget(parent),m_labelIcon(NULL),m_bUseMovieIcon(true),m_bEnableMovie(true)
{
	m_hovercursor=hovercursor;
	m_strIcon=strMovieIcon;
	m_strIconHover="";
	m_bHoverIconEnabled=false;
	m_bPlayMode=bPlayMode;

	m_pLayout = new QHBoxLayout();
	//layout->setMargin(0);
	m_pLayout->setSpacing(nSpacing);
	m_pLayout->addSpacing(nIdent);
	if (!strMovieIcon.isEmpty())
	{
		m_labelIcon = new QLabel(this);
		QMovie *movie = new QMovie(strMovieIcon,strMovieFormat.toLatin1(),this);
		m_labelIcon->setMovie(movie);
		movie->setCacheMode(QMovie::CacheAll) ;
		if (m_labelIcon->movie()->frameCount()==1)
			m_bPlayMode=false;

		connect(m_labelIcon->movie(),SIGNAL(frameChanged(int)),this,SLOT(onFrameChanged(int)));
		m_pLayout->addWidget(m_labelIcon);
	}
	if (!strTextHtml.isEmpty())
	{
		QLabel *label = new QLabel(this);
		label->setTextInteractionFlags(Qt::NoTextInteraction);
		label->setText(strTextHtml);
		m_pLayout->addWidget(label);
		//connect(m_label,SIGNAL(Clicked(int)),this,SLOT(OnClick()));
	}
	m_pLayout->addStretch(1);
	m_pLayout->setSpacing(0);
	m_pLayout->setContentsMargins(0,0,0,0);

	if ((!strMovieIcon.isEmpty() && strTextHtml.isEmpty()) || (strMovieIcon.isEmpty() && !strTextHtml.isEmpty()))
		m_pLayout->insertStretch(0,1);


	setLayout(m_pLayout);

	//style:
	setStyleSheet(strStyle);
	m_strDefaultStyle=strStyle;

	if (m_hovercursor!=Qt::ArrowCursor)
		setCursor(m_hovercursor);

}

StyledPushButton::~StyledPushButton()
{

}

void StyledPushButton::setText(QString strText)
{
	if (m_labelText)
		m_labelText->setText(strText);
}
void StyledPushButton::setIcon(QPixmap pixIcon)
{
	if (m_labelIcon)
		m_labelIcon->setPixmap(pixIcon);
}
void StyledPushButton::setIcon(QString strIcon)
{

	if (m_labelIcon)
	{

		if (!m_bUseMovieIcon)
			m_labelIcon->setPixmap(QPixmap(strIcon));
		else
		{
			if (m_labelIcon->movie()->state()==QMovie::Running)
				m_labelIcon->movie()->stop();
			m_labelIcon->movie()->setFileName(strIcon);
			m_labelIcon->movie()->jumpToFrame(0);
			if (m_bPlayMode && m_bEnableMovie)
				m_labelIcon->movie()->start();
		}
	}
	m_strIcon=strIcon;
}


void StyledPushButton::enterEvent ( QEvent * event )
{
	//if (m_hovercursor!=Qt::ArrowCursor)
	//	QApplication::setOverrideCursor(QCursor(m_hovercursor));

	if (m_labelIcon && m_bHoverIconEnabled)
	{
		m_labelIcon->setPixmap(QPixmap(m_strIconHover));
		//event->accept();
		emit mouse_over();
	}
	//else
		//event->ignore();
	QWidget::enterEvent(event);
}


void StyledPushButton::leaveEvent ( QEvent * event )
{
	//if (m_hovercursor!=Qt::ArrowCursor)
	///	QApplication::restoreOverrideCursor();


	if (m_labelIcon && m_bHoverIconEnabled)
	{
		m_labelIcon->setPixmap(QPixmap(m_strIcon));
		//event->accept();
		emit mouse_leave();
	}
	//else
		//event->ignore();

	QWidget::leaveEvent(event);
}

//pass event to parent:
void StyledPushButton::mousePressEvent ( QMouseEvent * event )
{
	m_bPressed=true;
	if (!m_strPressedIcon.isEmpty())
	{
		m_labelIcon->setPixmap(QPixmap(m_strPressedIcon));
	}
	QWidget::mousePressEvent(event);
}

//pass event to parent:
void StyledPushButton::mouseReleaseEvent(QMouseEvent * event )
{
	QWidget::mouseReleaseEvent(event);
	if (m_bPressed)
	{
		if (!m_strPressedIcon.isEmpty())
		{
			m_labelIcon->setPixmap(QPixmap(m_strIcon));
		}

		m_bPressed=false;
		if (!m_bPlayMode && m_bUseMovieIcon && m_bEnableMovie)
			m_labelIcon->movie()->start();
		emit clicked();
	}
}


void StyledPushButton::onFrameChanged(int nFrame)
{
	if (nFrame==m_labelIcon->movie()->frameCount()-1 )
	{
		if (!m_bPlayMode)
			m_labelIcon->movie()->stop();
		m_labelIcon->movie()->jumpToFrame(0);
	}
}

void StyledPushButton::showEvent ( QShowEvent * event )
{
	QWidget::showEvent(event);

	if (m_bPlayMode && m_bUseMovieIcon && m_bEnableMovie)
	{
		m_labelIcon->movie()->start();
	}
}

void StyledPushButton::hideEvent ( QHideEvent * event )
{
	if (m_bPlayMode && m_bUseMovieIcon)
	{
		m_labelIcon->movie()->stop();
		m_labelIcon->movie()->jumpToFrame(0);
	}

	QWidget::hideEvent(event);
}


void StyledPushButton::StartMovie(bool bStart)
{
	if (bStart && m_labelIcon->movie()->state()!=QMovie::Running)
	{
		m_labelIcon->movie()->start();
	}
	else if (!bStart)
	{
		m_labelIcon->movie()->stop();
		m_labelIcon->movie()->jumpToFrame(0);
	}
}

QHBoxLayout* StyledPushButton::GetLayout()
{
	return m_pLayout;
}

void StyledPushButton::setChecked(bool bChecked)
{
	if (bChecked)
	{
		this->setStyleSheet(m_strDefaultStyle+m_strCheckedStyle);
	}
	else
	{
		this->setStyleSheet(m_strDefaultStyle);
	}
}


