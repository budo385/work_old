#ifndef DLGUSERCHANGE_H
#define DLGUSERCHANGE_H

#include <QDialog>
#include "generatedfiles/ui_dlguserchange.h"
#include "common/common/dbrecordset.h"

class DlgUserChange : public QDialog
{
	Q_OBJECT

public:
	DlgUserChange(QWidget *parent = 0);
	~DlgUserChange();

	void SetData(DbRecordSet recUser);
	void GetData(DbRecordSet &recUser);

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_txtFirstName_editingFinished();

private:
	Ui::DlgUserChangeClass ui;
};

#endif // DLGUSERCHANGE_H
