#include "dlgsendemailbody.h"
#include "pichelper.h"

DlgSendEmailBody::DlgSendEmailBody(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle(tr("Invitation Email"));
	this->setStyleSheet(PicHelper::GetDialogStyle());

	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnCancel->setStyleSheet(strStyleBtn);
	ui.btnOK->setStyleSheet(strStyleBtn);

}

DlgSendEmailBody::~DlgSendEmailBody()
{

}

void DlgSendEmailBody::SetData(QString strSubject, QString strBody)
{
	ui.txtSubject->setText(strSubject);
	ui.txtBody->setPlainText(strBody);
}

void DlgSendEmailBody::GetData(QString &strSubject, QString &strBody)
{
	strSubject=ui.txtSubject->text();
	strBody=ui.txtBody->toPlainText();
}

void DlgSendEmailBody::on_btnCancel_clicked()
{
	done(0);
}

void DlgSendEmailBody::on_btnOK_clicked()
{
	done(1);
}
