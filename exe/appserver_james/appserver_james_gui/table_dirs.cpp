#include "table_dirs.h"
#include "filecruncher.h"
#include <QComboBox>
#include <QUrl>
#include <QFileDialog>
#include <QMimeData>
#include <QLabel>
#include <QListView>
#include <QTreeView>
#include <QMessageBox>
#include "pichelper.h"
#include "common_constants.h"
#include "dlgfolders.h"

#define COL_STATUS		0
#define COL_ALIAS		1	
#define COL_PATH		2
#define COL_FILE_TYPE	3



Table_Dirs::Table_Dirs(QWidget * parent)
:UniversalTableWidgetEx(parent)
{
	connect(this,SIGNAL(SignalDataChanged(int,int)),this,SLOT(OnCellChanged(int,int)));
}


Table_Dirs::~Table_Dirs()
{

}

void Table_Dirs::Initialize(DbRecordSet *plstData)
{
	//set data source for table, enable drop
	DbRecordSet columns;
	AddColumnToSetup(columns,"",tr(""),20,true,"" ,COL_TYPE_CUSTOM_WIDGET,"");
	AddColumnToSetup(columns,"ALIAS",tr("Catalog Name"),250,true,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"PIC_CNT",tr("Pics"),50,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"PATH",tr("Path"),550,false,"" ,COL_TYPE_TEXT,"");
	
	//AddColumnToSetup(columns,"",tr("Publish"),100,false, "",COL_TYPE_CUSTOM_WIDGET);

	UniversalTableWidgetEx::Initialize(plstData,&columns);
	EnableDrop(true);

	horizontalHeader()->setStretchLastSection(true);

	//CreateContextMenu();

	QList<QAction*> lstActions;
	QAction *SeparatorAct;

	//Insert new row:
	QAction* pAction = new QAction(tr("Publish All"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnPublishAll()));
	lstActions.append(pAction);
	
	pAction = new QAction(tr("Publish Pictures"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnPublishPictures()));
	lstActions.append(pAction);

	//pAction = new QAction(tr("Publish Documents"), this);
	//connect(pAction, SIGNAL(triggered()), this, SLOT(OnPublishDocuments()));
	//lstActions.append(pAction);

	pAction = new QAction(tr("UnPublish"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnUnPublish()));
	lstActions.append(pAction);

	lstActions.append(pAction);
	SetContextMenuActions(lstActions);


	connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
}


void Table_Dirs::Data2CustomWidget(int nRow, int nCol)
{
	if (nCol==COL_FILE_TYPE)
	{
		QComboBox *pCombo = dynamic_cast<QComboBox *>(cellWidget(nRow,nCol));
		if (!pCombo)
		{
			pCombo = new QComboBox;
			pCombo->setEditable(false);
			pCombo->setProperty("combo_row",nRow); //to identify sender
			connect(pCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(OnComboIndexChanged(int)));  //connect on change signals:

			pCombo->blockSignals(true);
			pCombo->clear();
			pCombo->addItem("All",FileCruncher::FILE_TYPE_ALL);
			pCombo->addItem("Pictures",FileCruncher::FILE_TYPE_PIC);
			pCombo->addItem("Documents Files",FileCruncher::FILE_TYPE_OFFICE);
			pCombo->blockSignals(false);
			pCombo->setEnabled(m_bEdit);
			pCombo->setEditable(false);
			setCellWidget(nRow,nCol,pCombo);
		}

		int nCode=m_plstData->getDataRef(nRow,"FILETYPES_CODE").toInt();
		pCombo->blockSignals(true);
		pCombo->setCurrentIndex(nCode);
		pCombo->blockSignals(false);
	}
	else if (nCol == COL_STATUS)
	{
		QLabel *pPix = dynamic_cast<QLabel *>(cellWidget(nRow,nCol));
		if (!pPix)
		{
			pPix = new QLabel;
			setCellWidget(nRow,nCol,pPix);
		}

		int nCode=m_plstData->getDataRef(nRow,"STATUS").toInt();

		if (nCode==STATUS_INDEXING)
		{
			pPix->setPixmap(QPixmap(":Resources/YellowDot16.png"));
			pPix->setToolTip(tr("Indexing"));
		}
		else if (nCode==STATUS_NOT_INDEXED)
		{
			pPix->setPixmap(QPixmap(":Resources/RedDot16.png"));
			pPix->setToolTip(tr("Not yet Indexed"));
		}
		else if (nCode==STATUS_COULD_NOT_BE_INDEXED)
		{
			pPix->setPixmap(QPixmap(":Resources/DarkGreenDot16.png"));
			pPix->setToolTip(tr("Could not be Indexed (some files are invalid or path can not be resolved. If shared disk then EXB-service must be run with local account access rights!)"));
		}
		else
		{
			pPix->setPixmap(QPixmap(":Resources/LightGreenDot16.png"));
			pPix->setToolTip(tr("Indexed"));
		}
	}


}
void Table_Dirs::CustomWidget2Data(int nRow, int nCol)
{
	if (nCol==COL_FILE_TYPE)
	{
			QComboBox *pCombo = dynamic_cast<QComboBox *>(cellWidget(nRow,nCol));
			if (pCombo)
			{
				int nIdx=pCombo->currentIndex();
				m_plstData->setData(nRow,"FILETYPES_CODE",nIdx);
				//m_plstData->setData(nRow,"FILETYPES",FileCruncher::GetFileTypesBasedOnCode(nIdx));
			}
	}
}

void Table_Dirs::OnCellDoubleClicked(int nRow,int nCol)
{
	//open directory selector/change path:
	if (nCol==COL_PATH)
	{
		QString strLastPath=m_plstData->getDataRef(nRow,"PATH").toString();
		FileCruncher::ReplaceNetworkPathWithUNC(strLastPath);
		QString	strDir=QFileDialog::getExistingDirectory ( this, "Select Directory for publish", strLastPath);
		if (!strDir.isEmpty())
		{
			m_plstData->setData(nRow,"PATH",strDir);
			m_strLastDirPath=strDir;
			RefreshDisplay();
		}
	}
}

void Table_Dirs::DialogAddNewDirectory(QString strDefaultPath, bool bIPhoto)
{
	if (strDefaultPath.isEmpty())
		strDefaultPath=m_strLastDirPath;


	DlgFolders Dlg(this,tr("Select directories for publish"),bIPhoto);
	QStringList lstDirs;
	QList<QString> lstAlbumIDs;

	if (bIPhoto)
	{
		if (!Dlg.GetIPhotoAlbums())
			return;
	}
	else
		Dlg.SetInitialFolder(strDefaultPath);

	if (Dlg.exec())
		Dlg.GetSelectedFolders(lstDirs,lstAlbumIDs);
	else
		return;
	
	Dlg.close();




	//eliminate root folders:
	if (!bIPhoto)
		FileCruncher::RemoveRootFolders(lstDirs);

	if (lstDirs.count()>0)
	{
		this->blockSignals(true);
		for (int i=0;i<lstDirs.count();++i)
		{
			AddNewDirectory(lstDirs.at(i),lstAlbumIDs.at(i));
			m_strLastDirPath=lstDirs.at(i);
		}
		this->blockSignals(false);

		emit ReloadCatalogs();
	}
}
bool Table_Dirs::AddNewDirectory(QString strPath, QString strAlbumIPhotoID, int nFileTypesCode)
{
	//strPath=strPath.toLower();
#ifdef WIN32
	if (strPath.left(1)=="/")
		strPath=strPath.mid(1);
#endif

	//QMessageBox::information(this,"1","Adding:"+ strPath+" id: "+QString::number(nAlbumIPhotoID));

	QString strAlias;
	QStringList lstParts=strPath.split("/",QString::SkipEmptyParts);
	if (lstParts.size()>0)
		strAlias=lstParts.at(lstParts.size()-1);
	else
		strAlias=strPath;

	if (strAlias.isEmpty())
		strAlias=strPath;

	//QMessageBox::information(this,"2","Alias:"+ strAlias);

	//check if valid:
	if (!FileCruncher::IsNameValidAsDirectory(strAlias))
	{
		QMessageBox::warning(this,"Warning",QString("Catalog can not be added: Catalog name %1 is not valid. Catalog name can't contain characters: *.\"/\\[]:;|=,").arg(strAlias));
		return false;
	}


	//check if path is in thumb cache:
	if (strPath.toLower().indexOf(m_strCatalogDir)==0)
	{
		QMessageBox::warning(this,"Warning",QString("Catalog can not have path in the directory for new catalogs: %1").arg(m_strCatalogDir));
		return false;
	}
	//check if path is in thumb cache:
	if (strPath.toLower().indexOf(m_strThumbDir)==0)
	{
		QMessageBox::warning(this,"Warning",QString("No catalogs from the thumbnail cache directory may be registered: %1").arg(m_strThumbDir));
		return false;
	}




	//int nRow=m_plstData->find("PATH",strPath,true);
	//if (nRow>=0)
	//	return false;

	if (m_plstData->find("ALIAS",strAlias,true)>=0)
	{
		int nCounter=0;
		do 
		{
			strAlias+=QString::number(nCounter);
			nCounter++;
		} while(m_plstData->find("ALIAS",strAlias,true)>=0 && nCounter<10000);
	}

	//QMessageBox::information(this,"3","GetFileCountFromFolder");

	int nCount=0;
	if (strAlbumIPhotoID.isEmpty())
	{
		nCount=FileCruncher::GetFileCountFromFolder(strPath,FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC));
		//get all subfolders:
		QList<QString> lstSubFolders;
		FileCruncher::GetAllSubFolders(strPath,lstSubFolders);
		int nSize=lstSubFolders.size();
		for (int i=0;i<nSize;i++)
		{
			nCount += FileCruncher::GetFileCountFromFolder(strPath+"/"+lstSubFolders.at(i),FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC));
		}
	}
	else
		nCount=FileCruncher_iPhoto::GetFileCountFromFolder(strAlbumIPhotoID,FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC));

	//QMessageBox::information(this,"4","Create recordset");

	m_plstData->addRow();
	//m_plstData->Dump();
	m_plstData->setData(m_plstData->getRowCount()-1,"ALIAS",strAlias);

	if (!strAlbumIPhotoID.isEmpty())
		m_plstData->setData(m_plstData->getRowCount()-1,"PATH",tr("iPhoto Album: ")+strPath);
	else
		m_plstData->setData(m_plstData->getRowCount()-1,"PATH",strPath);

	m_plstData->setData(m_plstData->getRowCount()-1,"FILETYPES_CODE",nFileTypesCode);
	m_plstData->setData(m_plstData->getRowCount()-1,"STATUS",STATUS_NOT_INDEXED);
	m_plstData->setData(m_plstData->getRowCount()-1,"PIC_CNT",nCount);
	m_plstData->setData(m_plstData->getRowCount()-1,"IPHOTO_ID",strAlbumIPhotoID);

	RefreshDisplay();


	//QMessageBox::information(this,"5","GetFirstFileFromFolder");

	QString strFile;
	if (strAlbumIPhotoID.isEmpty())
		strFile=FileCruncher::GetFirstFileFromFolder(strPath,FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC));
	else
		strFile=FileCruncher_iPhoto::GetFirstFileFromFolder(strAlbumIPhotoID,FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC));


	//QMessageBox::information(this,"6","CreateThumbFromFile");

	//THUMB_LARGE_WIDTH

	//load, create thumb for dir icon:
	if (!strFile.isEmpty())
	{
		QByteArray bytePic;
		QByteArray bytePicLarge;
		QPixmap pix,pixLarge;

		QFileInfo info(strFile);
		if(FileCruncher::IsRAWPicture(info.completeSuffix()))
		{
			PicHelper::LoadThumbFromRaw(bytePic,pix,strFile,THUMB_CATALOG_SMALL_WIDTH,THUMB_CATALOG_SMALL_HEIGHT);
			PicHelper::LoadThumbFromRaw(bytePicLarge,pixLarge,strFile,THUMB_CATALOG_LARGE_WIDTH,THUMB_CATALOG_LARGE_HEIGHT);
		}
		else
		{
			PicHelper::CreateThumbFromFile(strFile,bytePic,pix,THUMB_CATALOG_SMALL_WIDTH,THUMB_CATALOG_SMALL_HEIGHT);
			PicHelper::CreateThumbFromFile(strFile,bytePicLarge,pixLarge,THUMB_CATALOG_LARGE_WIDTH,THUMB_CATALOG_LARGE_HEIGHT);
		}

		if (bytePic.size()>0)
		{
			m_plstData->setData(m_plstData->getRowCount()-1,"THUMB",bytePic);
		}
		if (bytePicLarge.size()>0)
		{
			m_plstData->setData(m_plstData->getRowCount()-1,"THUMB_LARGE",bytePicLarge);
			m_plstData->setData(m_plstData->getRowCount()-1,"THUMB_LARGE_WIDTH",pixLarge.width());
			m_plstData->setData(m_plstData->getRowCount()-1,"THUMB_LARGE_HEIGHT",pixLarge.height());
		}
	}

	emit ReloadCatalogs();

	return true;
}

//if not edit mode: disable drop & drag
void Table_Dirs::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	if (mime->hasUrls())
	{
		QList<QUrl> lst =mime->urls();

		this->blockSignals(true);
		int nSize=lst.size();
		for (int i=0;i<nSize;i++)
		{
			AddNewDirectory(lst.at(i).path());
		}
		this->blockSignals(false);
		event->accept();
		emit ReloadCatalogs();
		return;
	}

	event->ignore();
	return;
}


void Table_Dirs::dragMoveEvent ( QDragMoveEvent * event )
{
	event->acceptProposedAction();
}
void Table_Dirs::dragEnterEvent(QDragEnterEvent *event)
{

	event->acceptProposedAction();
}
void Table_Dirs::dragLeaveEvent(QDragLeaveEvent *event)
{
	event->accept();
}
		
void Table_Dirs::OnComboIndexChanged(int nIndex)
{
	QComboBox *pCombo = dynamic_cast<QComboBox *>(sender());
	if (pCombo)
	{
		int nRow=pCombo->property("combo_row").toInt();
		Q_ASSERT(nRow>=0 && nRow <m_plstData->getRowCount()); //row must be inside data
		m_plstData->setData(nRow,"FILETYPES_CODE",nIndex);
		//m_plstData->setData(nRow,"FILETYPES",FileCruncher::GetFileTypesBasedOnCode(nIndex));
		emit ReloadCatalogs();
	}
}

void Table_Dirs::OnPublishAll()
{
	int nSize=m_plstData->getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (m_plstData->isRowSelected(i))
		{
			m_plstData->setData(i,"FILETYPES_CODE",FileCruncher::FILE_TYPE_ALL);
		}
	}
	emit ReloadCatalogs();
	RefreshDisplay();
	
}

void Table_Dirs::OnPublishPictures()
{
	int nSize=m_plstData->getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (m_plstData->isRowSelected(i))
		{
			m_plstData->setData(i,"FILETYPES_CODE",FileCruncher::FILE_TYPE_PIC);
		}
	}
	emit ReloadCatalogs();
	RefreshDisplay();
	//emit ReloadCatalogs();
}

void Table_Dirs::OnPublishDocuments()
{
	int nSize=m_plstData->getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (m_plstData->isRowSelected(i))
		{
			m_plstData->setData(i,"FILETYPES_CODE",FileCruncher::FILE_TYPE_OFFICE);
		}
	}
	emit ReloadCatalogs();
	RefreshDisplay();
}

void Table_Dirs::OnUnPublish()
{
	emit RemoveSelectedCatalogs();
}
void Table_Dirs::SetDirs(QString strCatalogDir,QString strThumbDir)
{
	m_strCatalogDir=strCatalogDir.toLower();
	m_strThumbDir=strThumbDir.toLower();
}

void Table_Dirs::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	m_plstData->sort("ALIAS");
	UniversalTableWidgetEx::RefreshDisplay(nRow,bApplyLastSortModel);
}
