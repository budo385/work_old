#ifndef TABLE_DIRS_H
#define TABLE_DIRS_H

#include "universaltablewidgetex.h"
#include "filecruncher.h"


class Table_Dirs : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_Dirs(QWidget *parent);
	~Table_Dirs();

	void Initialize(DbRecordSet *plstData);
	bool AddNewDirectory(QString strPath, QString strAlbumIPhotoID="", int nFileTypesCode=FileCruncher::FILE_TYPE_PIC);
	void DialogAddNewDirectory(QString strDefaultPath="", bool bIPhoto=false);
	void SetDirs(QString strCatalogDir,QString strThumbDir);

signals:
	void RenameCatalog(int nRow);
	void RemoveSelectedCatalogs();
	void ReloadCatalogs();

private slots:
	void OnCellDoubleClicked(int nRow,int nCol);
	void OnComboIndexChanged(int nIndex);
	void OnCellChanged(int nRow, int nCol){emit RenameCatalog(nRow);};

	void OnPublishAll();
	void OnPublishPictures();
	void OnPublishDocuments();
	void OnUnPublish();
	
public slots:
	void RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);


protected:
	
	void Data2CustomWidget(int nRow, int nCol);
	void CustomWidget2Data(int nRow, int nCol);
	void dropEvent(QDropEvent *event);
	void dragMoveEvent ( QDragMoveEvent * event );
	void dragEnterEvent(QDragEnterEvent *event);
	void dragLeaveEvent(QDragLeaveEvent *event);

private:
	QString m_strLastDirPath;
	QString m_strCatalogDir;
	QString m_strThumbDir;
};

#endif // TABLE_DIRS_H
