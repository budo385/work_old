#ifndef DLGUSERS_H
#define DLGUSERS_H

#include <QDialog>
#include "generatedfiles/ui_dlgusers.h"
#include "common/common/dbrecordset.h"
#include "settingsmanager.h"

class DlgUsers : public QDialog
{
	Q_OBJECT

public:
	DlgUsers(QWidget *parent = 0);
	~DlgUsers();

	void SetData(DbRecordSet lstUsers, SettingsManager *pSettings, bool bSelectMode=false, bool bAddUserEnabled=false, bool bSelectAll=false);
	void GetData(DbRecordSet &lstUsersSelected,DbRecordSet &lstUsersAll,DbRecordSet &lstDeletedUsers);

private slots:
	void on_btnAdd_clicked();
	void on_btnDelete_clicked();
	void on_btnPass_clicked();
	void OnCellDoubleClicked(int,int);

private:
	Ui::DlgUsersClass ui;
	
	DbRecordSet m_lstUsers;
	DbRecordSet m_lstDeletedUsers;
	bool m_bSelectMode;
	SettingsManager *m_pSettings;
};

#endif // DLGUSERS_H
