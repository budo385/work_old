#include "universaltablewidgetex.h"
#include "trans/trans/xmlutil.h"
#include <QTableWidgetSelectionRange>
#include <QMenu>
#include <QHeaderView>
#include <QApplication>
#include <QCommonStyle>
#include <QDrag>
#include <QMimeData>

#define OUTLOOK_MIME_MAIL_ID			"outlook/mailid"	//from outlook (mail id inside RenPrivMessage)
#define SOKRATES_MIME_LIST				"sokrates/list"		//sokrates->sokrates (list as bytearray)
#define SOKRATES_MIME_DROP_TYPE			"sokrates/type"		//sokrates->sokrates (drop type= entity id from bus_core/entity_id_collection.h)

UniversalTableWidgetEx::UniversalTableWidgetEx(QWidget *parent)
	: QTableWidget(parent),m_plstData(NULL),m_nDropMimeType(-1),m_nDragMimeType(-1)
{

}

UniversalTableWidgetEx::~UniversalTableWidgetEx()
{

}



void UniversalTableWidgetEx::Initialize(DbRecordSet *lstData,DbRecordSet *plstColumns,bool bSingleSelection,bool bSelectRows, int nVerticalRowSize)
{
	Q_ASSERT(!m_plstData); //if not NULL->can not initialize 2x
	m_plstData=lstData;

	setShowGrid(false);

	//issue 1649
	DummyNoFocusDelegate* DeafultTableDelegate = new DummyNoFocusDelegate(this);
	setItemDelegate (DeafultTableDelegate);
	//selection mode:
	if(bSelectRows) 
		setSelectionBehavior( QAbstractItemView::SelectRows);
	else
		setSelectionBehavior( QAbstractItemView::SelectItems);
	//if true then only one row can be selected at time:
	if(bSingleSelection) 
		setSelectionMode( QAbstractItemView::SingleSelection);
	else
		setSelectionMode( QAbstractItemView::ExtendedSelection);
	
	//for editing one cell:
	connect(this,SIGNAL(cellChanged(int,int)),this,SLOT(OnCellEdited(int,int)));
	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(OnItemSelectionChanged()));
	connect(this,SIGNAL(cellClicked(int,int)),this,SLOT(OnCellClicked(int,int)));

	//for sorting:
	connect((QObject *)this->horizontalHeader(),SIGNAL(sectionPressed(int)),this,SLOT(OnSortColumn(int)));
	setSortingEnabled(false); //we implement our custom sorting mechanism
	disconnect(horizontalHeader(), SIGNAL(sectionPressed(int)), this, SLOT(selectColumn(int)));
	horizontalHeader()->setSortIndicatorShown(true);

	//set all actions to false:
	SetEditMode(false);
	verticalHeader()->setDefaultSectionSize(nVerticalRowSize);
	SetStyleSheetGlobal();
	
	if (plstColumns)
		if (plstColumns->getRowCount()>0)
			SetColumnSetup(*plstColumns);
}


/*!
	Destroy any widgets inside table, recreates horizontal header: sets header info + tooltip + later icon can be added
	\param lstCols		- list with column No in grid and sort order for each column in grid
*/
void UniversalTableWidgetEx::RefreshHorizontalHeaderSetup()
{
	blockSignals(true);
	clear();
	setRowCount(0);
	int nSize=m_lstColumnSetup.getRowCount();
	setColumnCount(nSize);	//sets column count

	//issue 1684:
#ifdef __APPLE__
	QFont strStyle1("Arial",11,QFont::Bold,true);
#else
	QFont strStyle1("Arial",8,QFont::Bold,true);
#endif

	strStyle1.setStyleStrategy(QFont::PreferAntialias);

	QTableWidgetItem *newItem;
	//add columns:
	for( int i=0;i<nSize;++i)
	{
		setColumnWidth(i,m_lstColumnSetup.getDataRef(i,3).toInt()); //resize cols to given values
		newItem=horizontalHeaderItem(i);
		if(newItem==NULL) //if not created, create it:
		{
			newItem = new QTableWidgetItem();
			newItem->setFont(strStyle1);
			setHorizontalHeaderItem(i,newItem);
		}
		//set header + tooltip
		newItem->setText(m_lstColumnSetup.getDataRef(i,0).toString());
		newItem->setToolTip(m_lstColumnSetup.getDataRef(i,7).toString());
	}
	verticalHeader()->setHighlightSections(false);
	horizontalHeader()->setHighlightSections(false);
	blockSignals(false);
}

//disable all widgets and items and contx actions inside table
void UniversalTableWidgetEx::SetEditMode(bool bEdit)
{
	blockSignals(true);

	//clear selection when exiting edit mode
	if(m_bEdit && !bEdit)
		m_plstData->clearSelection();

	m_bEdit=bEdit;
	int nRowSize=rowCount();
	int nColSize=columnCount();
	for(int nRow=0;nRow<nRowSize;++nRow)
		for(int nCol=0;nCol<nColSize;++nCol)
		{	
			if(m_lstColumnSetup.getDataRef(nCol,5).toInt()==0) //if not editable do not change
				continue;
			int nColType=m_lstColumnSetup.getDataRef(nCol,2).toInt();
			QTableWidgetItem *pItem=item(nRow,nCol); //try if item
			if (pItem)
			{
				Qt::ItemFlags flags=pItem->flags();
				if (m_bEdit)
				{
					if (nColType==COL_TYPE_TEXT)
						flags = flags | Qt::ItemIsEditable;
					else
						flags = flags | Qt::ItemIsUserCheckable;
				}	
				else
				{
					if (nColType==COL_TYPE_TEXT)
						flags = flags & ~Qt::ItemIsEditable;	  //negate bit
					else
						flags = flags & ~Qt::ItemIsUserCheckable; //negate bit
				}
				pItem->setFlags(flags);
			}
			else
			{
				QWidget *widget=cellWidget(nRow,nCol); //try if widget
				if (widget)
					widget->setEnabled(m_bEdit);
			}

		}

	//based on edit mode, enable/disable items on cnxt menu && keyboard shortcuts:
	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
	{
		//means that action is separator or is always enable:
		if(m_lstActions.at(i)->isSeparator() || m_lstActions.at(i)->data().toBool()) continue;
		//enable/disable item:
		m_lstActions.at(i)->setEnabled(m_bEdit);
	}
	blockSignals(false);

}



/*!
	Initializes table for display, can be called after Initialize function
	Can be called infinite times: all 
	\param lstColumnSetup	- list in MVIEW_GRID_COLUMN_SETUP format
*/
void UniversalTableWidgetEx::SetColumnSetup(DbRecordSet &lstColumnSetup)
{
	//can not initialize table without data source
	Q_ASSERT(m_plstData!=NULL);
	//create columns:
	m_lstColumnSetup=lstColumnSetup;
	m_lstColumnSetup.sort(4); //sort by position
	m_lstColumnMapping.clear();
	//map columns from datasource
	int nSize=m_lstColumnSetup.getRowCount();
	int nColPos;
	for( int i=0;i<nSize;++i)
	{
		nColPos=m_plstData->getColumnIdx(m_lstColumnSetup.getDataRef(i,1).toString());
		//BT: if no datasource: leave empty -1 must be handled!
		m_lstColumnMapping[i]=nColPos; //maps (column displayed=column in list) 
	}

	//create header:
	RefreshHorizontalHeaderSetup();
	int nSection=horizontalHeader()->sortIndicatorSection(); 
	if (nSection>=0 && m_lstColumnMapping[nSection]>=0) 	//define sort order, look up on grid, destroy previous:
	{
		m_lstLastSort.clear();
		m_lstLastSort<<SortData(m_lstColumnMapping[nSection],horizontalHeader()->sortIndicatorOrder());
	}
	RefreshDisplay();
}


/*!
	Get column setup: column widths are updated to new

	\param lstColumnSetup	- list in MVIEW_GRID_COLUMN_SETUP format
*/
void UniversalTableWidgetEx::GetColumnSetup(DbRecordSet &lstColumnSetup)
{
	int nSize=m_lstColumnSetup.getRowCount();
	//update size:
	for( int i=0;i<nSize;++i)
		m_lstColumnSetup.setData(i,"BOGW_COLUMN_SIZE",columnWidth(i));

	lstColumnSetup=m_lstColumnSetup;
}

/*!
	Sets if column is editable, only effective after resfresh

	\param nColumn		- column number as added inside setup
	\param bEditable	- true,false
*/
void UniversalTableWidgetEx::SetColumnEditable(int nColumn,bool bEditable)
{
	m_lstColumnSetup.setData(nColumn,"BOGW_EDITABLE",bEditable);
}

/*!
	Add's column setup info to given list, if not defined, list will be defined,
	Position is determined by order of add.
	Column name  must match those in data source

	\param lstSetup		- returned column setup, redefined and filled 
	\param nView		- id of view from which to define table column setup
*/
void UniversalTableWidgetEx::AddColumnToSetup(DbRecordSet &lstSetup,QString strColName,QString strHeaderName,int nWidth,bool bEditable,QString strToolTip,int nColType,QString strDataFormat)
{
	if(lstSetup.getColumnCount()==0)
	{
		DbView view;
		view.m_nViewID		= 1;
		view.append(DbColumnEx("BOGW_HEADER_TEXT",			QVariant::String));
		view.append(DbColumnEx("BOGW_COLUMN_NAME",			QVariant::String));
		view.append(DbColumnEx("BOGW_COLUMN_TYPE",			QVariant::Int));		
		view.append(DbColumnEx("BOGW_COLUMN_SIZE",			QVariant::Int));
		view.append(DbColumnEx("BOGW_COLUMN_POSITION",		QVariant::Int));
		view.append(DbColumnEx("BOGW_EDITABLE",				QVariant::Bool));
		view.append(DbColumnEx("BOGW_SORT_ORDER",			QVariant::Int));		
		view.append(DbColumnEx("BOGW_TOOLTIP",				QVariant::String));		
		view.append(DbColumnEx("BOGW_DATASOURCE",			QVariant::String));		
		view.append(DbColumnEx("BOGW_REPORT_WIDTH",			QVariant::Int));		
		lstSetup.defineFromView(view);
	}


	int nPosition=lstSetup.getRowCount();
	//add column setup:
	lstSetup.addRow();
	lstSetup.setData(nPosition,0,strHeaderName);
	lstSetup.setData(nPosition,1,strColName);
	lstSetup.setData(nPosition,2,nColType);
	lstSetup.setData(nPosition,3,nWidth);
	lstSetup.setData(nPosition,4,nPosition);
	lstSetup.setData(nPosition,5,bEditable);
	lstSetup.setData(nPosition,6,0);			//sort order: default=asc
	lstSetup.setData(nPosition,7,strToolTip);
	lstSetup.setData(nPosition,8,strDataFormat);
	lstSetup.setData(nPosition,9,100);

}


void UniversalTableWidgetEx::SetStyleSheetGlobal()
{
	//this->setStyleSheet(ThemeManager::GetTableViewBkg());
}


void UniversalTableWidgetEx::contextMenuEvent(QContextMenuEvent *event)
{
	//event:
	if(m_lstActions.size()==0) 
	{
		event->ignore();	
		return;
	}
	QMenu CnxtMenu(this);
	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));

	CnxtMenu.exec(event->globalPos());
}

//warning: when selecting last row: refresh display is trigered
void UniversalTableWidgetEx::ScrollToLastItem(bool bSelectLastRow)
{
	if (m_plstData->getRowCount()==0) return;
	int nLastRow=m_plstData->getRowCount()-1;
	if (bSelectLastRow)
	{
		m_plstData->clearSelection();
		m_plstData->selectRow(nLastRow);
		RefreshDisplay();
	}

	if(columnCount()>0)
		setCurrentItem(item(nLastRow,0));
}

void UniversalTableWidgetEx::ScrollToLastSelectedItem()
{
	if (m_plstData->getRowCount()==0) return;
	if(columnCount()==0) return;

	int nSize=m_plstData->getRowCount()-1;
	for(int i=nSize;i>=0;i--)
	{
		if (m_plstData->isRowSelected(i))
		{
			setCurrentItem(item(i,0));
			return;
		}
	}
}

//warning: when selecting last row: refresh display is trigered
void UniversalTableWidgetEx::ScrollToFirstItem(bool bSelectFirstRow)
{
	if (m_plstData->getRowCount()==0) return;
	if (bSelectFirstRow)
	{
		m_plstData->clearSelection();
		m_plstData->selectRow(0);
		RefreshDisplay();
	}

	if(columnCount()>0)
	{
		setCurrentItem(item(0,0));
		scrollToItem(item(0,0));
	}
}



/*!
	When selected rows inside data source are changed call this function to refresh view.
	Faster then calling RefreshDisplay()
	\param bClearDataSelected - if true (default) selection from data source is also cleared
*/
void UniversalTableWidgetEx::RefreshSelection()
{
	blockSignals(true);
	//clear old selection:
	int nRowSize=m_plstData->getRowCount();
	int nColSize=columnCount();
	int nSelectedCount=m_plstData->getSelectedCount();

	//clear old selection:
	QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
	setRangeSelected(range,false);

	if (nSelectedCount==0)
	{
		blockSignals(false);
		return;
	}
	//all selected:
	if (nSelectedCount==nRowSize)
	{
		QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
		setRangeSelected(range,true);
		blockSignals(false);
		return;
	}
	//one selected:
	if (nSelectedCount==1)
	{
		int nRow=m_plstData->getSelectedRow();
		QTableWidgetSelectionRange range(nRow,0,nRow,nColSize-1);  //TOFIX: can be optimized
		setRangeSelected(range,true);
		if (nColSize==1)
			setCurrentItem(item(nRow,0)); //if one columned table, set on item current
		blockSignals(false);
		return;
	}

	//refresh selection:
	//if not selection & row selected
	int nRowRange=0;
	for( int nRow=0;nRow<nRowSize;++nRow)
	{
		if (m_plstData->isRowSelected(nRow))
			nRowRange++;
		else
		{
			if (nRowRange>0)
			{
				QTableWidgetSelectionRange range(nRow-nRowRange,0,nRow-1,nColSize-1);  //TOFIX: can be optimized
				setRangeSelected(range,true);
				nRowRange=0;
			}
		}
	}
	//last:
	if (nRowRange>0)
	{
		QTableWidgetSelectionRange range(nRowSize-nRowRange,0,nRowSize-1,nColSize-1);  //TOFIX: can be optimized
		setRangeSelected(range,true);
	}
	blockSignals(false);

}



/*!
	Based on datasource, refreshes contents of displayed items.
	Call this when data is changed (add, indert, delete, select/deselect)
	If only selection is change, call reshresh selection (clears current, sets new one)
	Column count must remain!
*/
void UniversalTableWidgetEx::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	if (m_plstData==NULL) return;
	if (m_plstData->getColumnCount()==0) return;
	setUpdatesEnabled(false);
	int nRowSize=m_plstData->getRowCount();
	blockSignals(true);
	setRowCount(nRowSize);

	if (bApplyLastSortModel && m_lstLastSort.size()>0)
	{
		m_plstData->sortMulti(m_lstLastSort);
	}
	RefreshVerticalHeaderSetup(nRow); //rebuild vertical header
	Data2Table(nRow); //show data
	//repair bug in vertical header refresh (resize up-down 1 pixel horizontal section):
	horizontalHeader()->resizeSection(0,horizontalHeader()->sectionSize(0)+1);
	horizontalHeader()->resizeSection(0,horizontalHeader()->sectionSize(0)-1);
	blockSignals(false);	
	RefreshSelection();
	setUpdatesEnabled(true);
}


//creates vertical header if visible: number's for each row and custom icon
void UniversalTableWidgetEx::RefreshVerticalHeaderSetup(int nRow)
{
	if (verticalHeader()->isHidden())
		return;

	//creates widget of specific type for each column
	int nRowCount=rowCount();
	int nColCount=columnCount();
	if (nRow!=-1)
		nRowCount=nRow+1;
	else
		nRow=0;

	//issue 1684:
	QFont strStyle1("Arial",8,QFont::Normal);
	strStyle1.setStyleStrategy(QFont::PreferAntialias);
	QFont strStyle2("Arial Narrow",8,QFont::Normal);
	strStyle2.setStyleStrategy(QFont::PreferAntialias);

		//first refresh icon:
		for( int i=nRow;i<nRowCount;++i)
		{
			QIcon icon;
			QString strStatusTip;
			//bool bResizeVert=false;
			GetRowIcon(i,icon,strStatusTip);
			QTableWidgetItem *vertItem = verticalHeaderItem(i);

			if (icon.isNull() && vertItem==NULL)		//only fly by if icon is null and item is not existant 
				continue;

			if(vertItem==NULL)
			{
				vertItem = new QTableWidgetItem(0);
				//vertItem->setTextAlignment(Qt::AlignRight);
				vertItem->setTextAlignment(Qt::AlignLeft);
				//vertItem->setData(Qt::DisplayRole,nRow+1);
				vertItem->setFont(strStyle1);
				vertItem->setText(QVariant(i+1).toString());
				vertItem->setIcon(icon);
				vertItem->setToolTip(strStatusTip);
				setVerticalHeaderItem(i,vertItem);
			}
			else
			{
				vertItem->setIcon(icon);
				vertItem->setToolTip(strStatusTip);
			}
		}
}


void UniversalTableWidgetEx::Data2Table(int nStartRow)
{
	//issue 1684:
	QFont strStyle1("Arial",8,QFont::Normal);
	strStyle1.setStyleStrategy(QFont::PreferAntialias);
	QFont strStyle2("Arial Narrow",8,QFont::Normal);
	strStyle2.setStyleStrategy(QFont::PreferAntialias);


	int nRowCount=rowCount();
	int nColCount=columnCount();
	if (nStartRow!=-1)
		nRowCount=nStartRow+1;
	else
		nStartRow=0;

	//loop by cols/rows
	bool bNewItem=false;
	QString strText;

	for( int i=0;i<nColCount;++i)
	{
		int nCol			=m_lstColumnMapping[i];
		int nColType		=m_lstColumnSetup.getDataRef(i,2).toInt();
		bool bEditable		=m_lstColumnSetup.getDataRef(i,5).toBool();
		bool bIsCheckBox	=(COL_TYPE_CHECKBOX==m_lstColumnSetup.getDataRef(i,2).toInt());


		//BT--------------------------------: create dummy item for dummy column without datasource
		if (nCol==-1) //create dummy items:
		{
			if (nColType<COL_TYPE_CUSTOM_WIDGET)
				for( int nRow=nStartRow;nRow<nRowCount;++nRow)
				{
					QTableWidgetItem *newItem=item(nRow,i);
					if(newItem==NULL)	//create new if not
					{
						newItem = new QTableWidgetItem;
						//non EDIT always for dummies
						newItem->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled); //Qt::ItemIsEnabled
						newItem->setFont(strStyle2);
						setItem(nRow,i,newItem);
					}
				}
			else
				for( int nRow=nStartRow;nRow<nRowCount;++nRow)
					Data2CustomWidget(nRow,i);

			continue; //can be -1-> skip all
		}
		//BT--------------------------------: create dummy item for dummy column without datasource

		
		int	nDataType		=m_plstData->getColumnType(nCol);

		if (nColType==COL_TYPE_TEXT)
		{
			for( int nRow=nStartRow;nRow<nRowCount;++nRow)
			{
				bool bNewItem=false;
				QTableWidgetItem *newItem=item(nRow,i);
				if(newItem==NULL)	//create new if not
				{
					newItem = new QTableWidgetItem;
					bNewItem=true;
					if(m_bEdit && bEditable)
						newItem->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
					else 
						newItem->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled); //Qt::ItemIsEnabled
				}
				switch(nDataType) //set data
				{
				case QVariant::Date:
					newItem->setData(Qt::DisplayRole,m_plstData->getDataRef(nRow,nCol).toDate().toString(Qt::LocaleDate));
					break;
				case QVariant::DateTime:
					newItem->setData(Qt::DisplayRole,m_plstData->getDataRef(nRow,nCol).toDateTime().toString(Qt::LocaleDate));
					break;
				default:
					newItem->setData(Qt::DisplayRole,m_plstData->getDataRef(nRow,nCol));
				}
				if (bNewItem) //actually set on table
					setItem(nRow, i, newItem);
			}

		}
		else if (nColType == COL_TYPE_CHECKBOX)
		{
			for( int nRow=nStartRow;nRow<nRowCount;++nRow)
			{
				bool bNewItem=false;
				QTableWidgetItem *newItem=item(nRow,i);
				if(newItem==NULL)	//create new if not
				{
					newItem = new QTableWidgetItem;
					bNewItem=true;
					if(m_bEdit && bEditable)
						newItem->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled);
					else 
						newItem->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled); //Qt::ItemIsEnabled
				}
				newItem->setCheckState(m_plstData->getDataRef(nRow,nCol).toBool() ? Qt::Checked : Qt::Unchecked); //set data
				if (bNewItem) //actually set on table
					setItem(nRow, i, newItem);
			}
		}
		else
		{
			for( int nRow=nStartRow;nRow<nRowCount;++nRow)
				Data2CustomWidget(nRow,i);
		}

	}
}



/*!
	When selection is changed, record set rows are selected/deselected
*/
void UniversalTableWidgetEx::OnItemSelectionChanged()
{
	//m_bSelectionChangeEmitted=true;
	QList<QTableWidgetSelectionRange> lstSelections = selectedRanges();
	int nSize=lstSelections.size();

	//if CTRL+A,then select all:
	if(nSize==1)
		if (lstSelections.at(0).topRow()==0 && lstSelections.at(0).bottomRow()==m_plstData->getRowCount()-1)
		{
			m_plstData->selectAll();
			emit SignalSelectionChanged();
			return;
		}
	m_plstData->clearSelection();
	for(int i=0;i<nSize;++i)
	{
		QTableWidgetSelectionRange range=lstSelections.at(i);
		for(int j=range.topRow();j<=range.bottomRow();++j)
		{	
			m_plstData->selectRow(j);
		}
	}

	emit SignalSelectionChanged();
}


/*!
When cell clicked and selection change is not triggered, use this:

\param nRow		- cell widget row
\param nCol		- cell widget col
*/
void UniversalTableWidgetEx::OnCellClicked(int nRow, int nCol)
{
	Table2Data(nRow);

	//trigger selection only when row inside list that is already selected:
	//if(m_bSelectionChangeEmitted)
	//	m_bSelectionChangeEmitted=false;
	//else if(m_plstData->getRowCount()==1) //only when 1 row:
	//	emit SignalSelectionChanged();
	if (m_lstColumnSetup.getDataRef(nCol,"BOGW_COLUMN_TYPE").toInt()>=COL_TYPE_CUSTOM_WIDGET)
	{
		OnCustomWidgetCellClicked(nRow,nCol);
	}

	GoEditCell(nRow,nCol);
}

/*!
	When cell is changed, data is written back to recordset
	Warning:
	- disable signals when chaning items in grid, because they cause EDITING

	\param nRow - row
	\param nCol - column

*/
void UniversalTableWidgetEx::OnCellEdited(int nRow, int nCol)
{
	if (m_lstColumnMapping[nCol]==-1) return; 
	QTableWidgetItem *pItem=item(nRow,nCol);
	if (!pItem) return;//if NULL return
	if (!m_lstColumnSetup.getDataRef(nCol,"BOGW_EDITABLE").toBool()) return; //if not editable jump back

	int nColType	= m_lstColumnSetup.getDataRef(nCol,"BOGW_COLUMN_TYPE").toInt();
	int nDataType	= m_plstData->getColumnType(m_lstColumnMapping[nCol]);
	QVariant value	= pItem->data(Qt::DisplayRole);
	bool bValidationOK;

	if (nColType==COL_TYPE_TEXT)
	{
		//reverse: DAte is stored as string, revert to date:
		if (nDataType==QVariant::Date)
			value=value.toDate();
		else if (nDataType==QVariant::DateTime)
			value=value.toDateTime();

		//validate and store new data
		m_plstData->setData(nRow,m_lstColumnMapping[nCol],value);
	}
	else if(nColType==COL_TYPE_CHECKBOX)
	{
		if(pItem->checkState()==Qt::Checked)
			value=QVariant(true);
		else
			value=QVariant(false);
		if (nDataType==QVariant::Int) //convert to int if not bool
			value=value.toInt();

		//store:
		m_plstData->setData(nRow,m_lstColumnMapping[nCol],value);
		
	}
	else
		return;

	emit SignalDataChanged(nRow,nCol); 

}




/*!
	Sort column: sorting order is temporarly stored in column setup
	Whole list is resorted to keep it in sync with table view

	\param nColPos - column in grid

*/
void UniversalTableWidgetEx::OnSortColumn(int nColPos)
{
	if (m_lstColumnMapping[nColPos]==-1) return;
	int nColType=m_lstColumnSetup.getDataRef(nColPos,2).toInt();
	if(nColType>COL_TYPE_CHECKBOX) return; //no sorting on special widgets

	int nSortOrder = m_lstColumnSetup.getDataRef(nColPos,6).toInt(); // 0 - asc ,1 -desc
	//revert existing sort order (when first time, probably it go descending)
	nSortOrder = (nSortOrder==1 ? 0:1); 
	//store last:
	m_lstLastSort.clear();
	m_lstLastSort<<SortData(m_lstColumnMapping[nColPos],nSortOrder);
	horizontalHeader()->setSortIndicator(nColPos, (Qt::SortOrder)nSortOrder);
	//m_plstData->Dump();
	//if (m_bGridContainsBoldItems) //if no bold items, do it, else
	//{
	//	emit SignalRefreshBoldRows();
	//}
	m_plstData->sort(m_lstColumnMapping[nColPos],nSortOrder);
	RefreshDisplay();
	m_lstColumnSetup.setData(nColPos,6,nSortOrder); //store back reverse
}


/*!
Sort columns in grid/datasource by order given in list.
*/
void UniversalTableWidgetEx::SortMultiColumn(DbRecordSet lstCols)
{
	//set sort indicator on first col in list:
	if(lstCols.getRowCount()==0)return;

	SortDataList lstSort;
	bool bSortIndicatorSet=false;

	int nSize=lstCols.getRowCount();
	for(int i=0;i<nSize;++i)
	{

		lstSort.append(SortData(m_plstData->getColumnIdx(lstCols.getDataRef(i,"BOGW_COLUMN_NAME").toString()),lstCols.getDataRef(i,"BOGW_SORT_ORDER").toInt())); // prepare sort list:
		int nRow=m_lstColumnSetup.find(1,lstCols.getDataRef(i,"BOGW_COLUMN_NAME").toString(),true);
		if(nRow!=-1)
		{
			m_lstColumnSetup.setData(nRow,6,lstCols.getDataRef(i,"BOGW_SORT_ORDER").toInt()); //store back asc/desc values for each col
			if(!bSortIndicatorSet)	//set sort indicator to first col in multisort
			{
				horizontalHeader()->setSortIndicator(nRow, (Qt::SortOrder)lstCols.getDataRef(i,"BOGW_SORT_ORDER").toInt());
				bSortIndicatorSet=true;
			}
		}
	}
	m_lstLastSort=lstSort;
	m_plstData->sortMulti(lstSort);
	RefreshDisplay(); 
}


//when drag: send selected items + type
QMimeData * UniversalTableWidgetEx::mimeData( const QList<QTableWidgetItem *> items ) const
{
	QMimeData *mimeData=QTableWidget::mimeData(items);
	if (mimeData)
	{
		DbRecordSet lstSelectedItems=m_plstData->getSelectedRecordSet();
		if (lstSelectedItems.getRowCount()>0 && m_nDragMimeType!=-1)
		{
			QByteArray byteListData=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstSelectedItems);
			mimeData->setData(SOKRATES_MIME_LIST,byteListData);
			mimeData->setData(SOKRATES_MIME_DROP_TYPE,QVariant(m_nDragMimeType).toString().toLatin1()); //pass unique type
		}
	}

	return mimeData;
}

//add custom mime types: selected list items + drop type
QStringList UniversalTableWidgetEx::mimeTypes() const
{
	QStringList lstDefault=QTableWidget::mimeTypes();
	lstDefault<<SOKRATES_MIME_LIST;
	lstDefault<<SOKRATES_MIME_DROP_TYPE;

	return lstDefault;
}


void UniversalTableWidgetEx::EnableDrag(bool bEnable,int nDragMimeType, QString strDragIcon)
{
	setDragEnabled(bEnable);
	m_nDragMimeType=nDragMimeType;
	m_strDragIcon=strDragIcon;
}

/*!
Starts a drag by calling drag->start() using the given \a supportedActions.
*/
void UniversalTableWidgetEx::startDrag(Qt::DropActions supportedActions)
{
	//QTableWidget::startDrag(supportedActions);
	//return;

	//Q_D(QAbstractItemView);
	QModelIndexList indexes = selectedIndexes();
	if (indexes.count() > 0) 
	{
		QMimeData *data = model()->mimeData(indexes);
		if (!data)
			return;
		QRect rect;
		QPixmap pixmap(m_strDragIcon); //= d->renderToPixmap(indexes, &rect);
		QDrag *drag = new QDrag(this);
		drag->setPixmap(pixmap);
		drag->setMimeData(data);
		drag->setHotSpot(QPoint(-10,0)); //drag->pixmap().width()/2,0drag->pixmap().height()));

		if (drag->start(supportedActions) == Qt::MoveAction)
		{
			//------------------B.T. copied form void QAbstractItemViewPrivate::clearOrRemove()
			// we can't remove the rows so reset the items (i.e. the view is like a table)
			QModelIndexList list = selectedIndexes();
			for (int i=0; i < list.size(); ++i) 
			{
				QModelIndex index = list.at(i);
				QMap<int, QVariant> roles = model()->itemData(index);
				for (QMap<int, QVariant>::Iterator it = roles.begin(); it != roles.end(); ++it)
					it.value() = QVariant();
				model()->setItemData(index, roles);
			}
			//------------------B.T. copied form void QAbstractItemViewPrivate::clearOrRemove()
		}

	}
}

//call after custom widget has been clicked
void UniversalTableWidgetEx::OnCustomWidgetCellClicked(int nRow, int nCol)
{
	if (m_plstData)
	{
		bool bCTRL=false,bShift=false;
		Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();

		if(Qt::ControlModifier == (Qt::ControlModifier & keys))
			bCTRL = true;

		//remember if the Alt key was pressed
		if(Qt::ShiftModifier == (Qt::ShiftModifier & keys))
			bShift = true;

		if(bCTRL)
			m_plstData->selectRow(nRow,!m_plstData->isRowSelected(nRow));

		if (bShift)
			m_plstData->selectRow(nRow);

		if (!bShift && !bCTRL)
		{
			if (m_plstData->isRowSelected(nRow)) //if already selected then ignore: no sense to deselect, select and fire signal again
				return;
			m_plstData->clearSelection();
			m_plstData->selectRow(nRow);
		}

		RefreshSelection(); 
		emit SignalSelectionChanged();
	}
}


void UniversalTableWidgetEx::DeleteSelection()
{
	if(m_plstData->getSelectedCount()==rowCount()) 
		{DeleteTable();return;}
	m_plstData->deleteSelectedRows();
	emit SignalRowsDeleted();
	emit SignalSelectionChanged();
	emit SignalContentChanged();
	RefreshDisplay();
}

void UniversalTableWidgetEx::DeleteTable()
{
	m_plstData->clear();
	RefreshHorizontalHeaderSetup();
	emit SignalRowsDeleted();
	emit SignalSelectionChanged();
	emit SignalContentChanged(); 
}

void UniversalTableWidgetEx::InsertRow()
{
	m_plstData->addRow();
	int nRowSize=m_plstData->getRowCount();
	setRowCount(nRowSize);
	m_plstData->clearSelection();
	m_plstData->selectRow(nRowSize-1);
	emit SignalRowInserted(nRowSize-1);
	RefreshDisplay();
	emit SignalSelectionChanged();
	emit SignalContentChanged();
	GoEditCell(nRowSize-1,0); //if editable go insert mode
}

void UniversalTableWidgetEx::SelectAll()
{
	blockSignals(true);
	m_plstData->selectAll();
	int nColSize=columnCount();
	QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
	setRangeSelected(range,true);
	blockSignals(false);
	emit SignalSelectionChanged();
}

void UniversalTableWidgetEx::DeselectAll()
{
	blockSignals(true);
	m_plstData->clearSelection();
	int nColSize=columnCount();
	QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
	setRangeSelected(range,false);
	blockSignals(false);
	emit SignalSelectionChanged();

}


/*!
DuplicateSelection: all selected rows are duplicated:
- new row is inserted just below selection
- new rows are selected 

*/
void UniversalTableWidgetEx::DuplicateSelection()
{
	//for each selected row
	int i=0,nSize=m_plstData->getRowCount();
	while(i<nSize)
	{
		//if selected
		if(m_plstData->isRowSelected(i))
		{
			DbRecordSet Row=m_plstData->getRow(i);
			m_plstData->insertRow(i+1);
			m_plstData->assignRow(i+1,Row);
			emit SignalRowInserted(i+1);
			nSize=nSize+1;
			m_plstData->selectRow(i,false);
			m_plstData->selectRow(i+1,true);
			++i;//advance to next
		}
		++i;
	}
	RefreshDisplay();
	emit SignalSelectionChanged();
	emit SignalContentChanged();
}

/*!
Opens sorting dialog for multicolumn sorting
*/
void UniversalTableWidgetEx::OpenSortDialog()
{
	/*
	MultiSortColumn dlgSorter;

	DbRecordSet lstCols=m_lstColumnSetup;
	lstCols.find("BOGW_COLUMN_NAME",QString(""));
	lstCols.deleteSelectedRows();

	//init
	dlgSorter.SetColumnSetup(lstCols);

	//show:
	if(dlgSorter.exec()>0)
		SortMultiColumn(dlgSorter.GetResultRecordSet()); //if press OK, sort it:
		*/

}


void UniversalTableWidgetEx::GoEditCell(int nRow,int nCol)
{
	if(m_bEdit)
	{
		//get first cell on new row
		QWidget *pWidget=cellWidget(nRow,nCol);
		if (pWidget)
		{
			pWidget->setFocus(Qt::MouseFocusReason);
		}
		else
		{
			QTableWidgetItem * cell=item(nRow,nCol); 
			if (cell)
				if(cell->flags() & Qt::ItemIsEditable)
					editItem(cell);
		}
	}
}



/*!
	Creates default cnxt menu actions. You must manually call this method to fill action list then
	SetContextMenuActions() to set actions on grid.


	Defaults are:

	- Add row
	------------------
	- Delete selection
	- Duplicate selection
	- Delete whole content
	------------------
	- Select All
	- DeSelect All
	------------------
	- Sort

	\param nShowFilter - create all or just selected (no separators

*/
void UniversalTableWidgetEx::CreateDefaultContextMenuActions(QList<QAction*>& lstActions, int nShowFilter)
{

	QCommonStyle style;
	QAction* pAction=NULL;

	if (nShowFilter & SHOW_CNXT_INSERT_ROW || nShowFilter==SHOW_CNXT_ALL)
	{
		QAction* pAction = new QAction(tr("&Add New Entry"), this);
		pAction->setShortcut(tr("Ins"));
		pAction->setIcon(QIcon(":handwrite.png"));
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(InsertRow()));
		lstActions.append(pAction);
		this->addAction(pAction);
	}
	if (nShowFilter & SHOW_CNXT_DELETE_SELECTION || nShowFilter==SHOW_CNXT_ALL)
	{
		pAction = new QAction(tr("&Delete Selection"), this);
		pAction->setShortcut(tr("Del"));
		pAction->setIcon(style.standardIcon(QStyle::SP_TrashIcon));
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteSelection()));
		lstActions.append(pAction);
		this->addAction(pAction);
	}
	if (nShowFilter & SHOW_CNXT_DELETE_ALL || nShowFilter==SHOW_CNXT_ALL)
	{
		pAction = new QAction(tr("Delete All"), this);
		pAction->setShortcut(tr("CTRL+S"));
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteTable()));
		lstActions.append(pAction);
		this->addAction(pAction);
	}
	if (nShowFilter & SHOW_CNXT_DUPLICATE_SELECTION || nShowFilter==SHOW_CNXT_ALL)
	{
		pAction = new QAction(tr("Duplicate Selection"), this);
		pAction->setShortcut(tr("CTRL+D"));
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(DuplicateSelection()));
		lstActions.append(pAction);
		this->addAction(pAction);
	}
	if(this->selectionMode()!=QAbstractItemView::SingleSelection) //ignore if single selection
	{
		if (nShowFilter & SHOW_CNXT_SELECT_ALL || nShowFilter==SHOW_CNXT_ALL)
		{
			pAction = new QAction(tr("&Select All"), this);
			pAction->setShortcut(tr("CTRL+A"));
			pAction->setData(QVariant(true));	//means that is always enabled
			connect(pAction, SIGNAL(triggered()), this, SLOT(SelectAll()));
			lstActions.append(pAction);
		}
		if (nShowFilter & SHOW_CNXT_DESELECT_ALL || nShowFilter==SHOW_CNXT_ALL)
		{
			pAction = new QAction(tr("D&eSelect All"), this);
			pAction->setShortcut(tr("CTRL+E"));
			pAction->setData(QVariant(true));	//means that is always enabled
			connect(pAction, SIGNAL(triggered()), this, SLOT(DeselectAll()));
			lstActions.append(pAction);
			this->addAction(pAction);
		}
	}
	/*
	if (nShowFilter & SHOW_CNXT_SORT_DIALOG || nShowFilter==SHOW_CNXT_ALL)
	{
		pAction = new QAction(tr("S&ort"), this);
		pAction->setShortcut(tr("CTRL+S"));
		pAction->setIcon(QIcon(":rollingdice.png"));
		pAction->setData(QVariant(true));	//means that is always enabled
		connect(pAction, SIGNAL(triggered()), this, SLOT(OpenSortDialog()));
		lstActions.append(pAction);
	}
	*/

}

//if nDropMimeType is left -1, signal SignalDataDroped is emited on every drop for sokrates lists, else only for selected types....
void UniversalTableWidgetEx::EnableDrop(bool bEnable,int nDropMimeType)
{
	setAcceptDrops(bEnable);
	m_nDropMimeType=nDropMimeType;
}

//if not edit mode: disable drop & drag
void UniversalTableWidgetEx::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	//------------------------------------------------
	// LIST of documents entities:
	//------------------------------------------------
	//internal/external
	if (mime->hasFormat(SOKRATES_MIME_LIST))
	{
		int nEntityID;
		DbRecordSet lstDropped;

		//import emails:
		QByteArray arData = mime->data(SOKRATES_MIME_LIST);
		if(arData.size() > 0)
		{
			lstDropped=XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
		}
		QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
		if(arDataType.size() > 0)
		{
			bool bOK;
			nEntityID=arDataType.toInt(&bOK);
			if (!bOK) nEntityID=-1;
		}

		//check if ok:
		if (m_nDropMimeType==-1) //if -1 always emit!!
			emit SignalDataDroped(nEntityID, lstDropped, event);
		else if (m_nDropMimeType!=-1 && m_nDropMimeType==nEntityID)
			emit SignalDataDroped(nEntityID, lstDropped, event);

		event->accept();
		return;
	}

	event->ignore();
	return;
}

Qt::DropActions UniversalTableWidgetEx::supportedDropActions() const 
{
	return  Qt::CopyAction | Qt::MoveAction | Qt::IgnoreAction | Qt::LinkAction | Qt::ActionMask | Qt::TargetMoveAction;
}
void UniversalTableWidgetEx::dragMoveEvent ( QDragMoveEvent * event )
{
	if (event->mimeData()->hasFormat(SOKRATES_MIME_LIST))
		event->accept();
	else
		event->ignore();
	return;
}
void UniversalTableWidgetEx::dragEnterEvent ( QDragEnterEvent * event )
{
	event->accept();
	return;
}


void UniversalTableWidgetEx::GetSelection(DbRecordSet &lstDataSelected)
{
	lstDataSelected.copyDefinition(*m_plstData);
	lstDataSelected.merge(*m_plstData,true);
}

void UniversalTableWidgetEx::Table2Data(int nRow)
{
	int nColSize=columnCount();
	if (nRow!=-1 && nRow>=0 && nRow<m_plstData->getRowCount())
		for (int i=0;i<nColSize;i++)
			CustomWidget2Data(nRow,i);

	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
		for (int k=0;k<nColSize;k++)
			CustomWidget2Data(i,k);
}