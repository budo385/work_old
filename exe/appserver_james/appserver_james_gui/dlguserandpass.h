#ifndef DLGUSERANDPASS_H
#define DLGUSERANDPASS_H

#include <QDialog>
#include "ui_dlguserandpass.h"

class DlgUserAndPass : public QDialog
{
	Q_OBJECT

public:
	DlgUserAndPass(QWidget *parent = 0);
	~DlgUserAndPass();

	void GetData(QString &strUserName,QString &strPass){strUserName=ui.txtUserName->text(); strPass=ui.txtPassword1->text();};

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();

private:
	Ui::DlgUserAndPass ui;
};

#endif // DLGUSERANDPASS_H
