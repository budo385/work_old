#ifndef APPSERVER_JAMES_GUI_H
#define APPSERVER_JAMES_GUI_H

#include <QWidget>
#include "generatedfiles/ui_appserver_james_gui.h"
#include "settingsmanager.h"
#include "serverinifile.h"
#include "common/common/appserverconfigurator.h"
#include "styledpushbutton.h"
#include <QAction>
#include <QProgressBar>



class appserver_james_gui : public QWidget
{
	Q_OBJECT

public:
	appserver_james_gui(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~appserver_james_gui();

	enum ButtonStatus
	{
		STATUS_OFFLINE,
		STATUS_ONLINE_WLAN,
		STATUS_ONLINE_FULL,
		STATUS_POWER_UP,
	};

	void LoadSettings();
	bool ActualizeSettings();
	bool SaveSettings(bool bSkipMsg=false,bool bSkipCatalogReload=false, bool bSkipServerReload=false);
	void LoadINISettings();
	bool SaveINISettings(bool bUseSilentValueCheck=false);

private slots:

	void on_btnAddFolder_clicked();
	void on_btnAddiPhoto_clicked();
	void on_btnRemoveFolder_clicked();
	void on_btnResetService_clicked();

	void on_btnStart_clicked();
	void on_btnServerPicChange_clicked();
	void on_btnServerPicClear_clicked();
	void on_btnDirectoryPicChange_clicked();
	void on_btnDirectoryPicClear_clicked();
	void OnDirectorySelectionChanged();
	void on_btnUsers_clicked();
	void on_btnAddUserRight_clicked();
	void on_btnDeleteUserRight_clicked();
	void on_btnInvite_clicked();
	void on_btnLogSend_clicked();
	void on_btnLogSave_clicked();

	void on_btnAdd_clicked();
	void on_btnDelete_clicked();
	void on_btnPass_clicked();
	void OnUserSelectionChanged();

	void OnReloadCatalogs();
	void OnRenameCatalog(int nRow);

	void on_txtRootPass_editingFinished();
	void on_txtServerName_editingFinished();
	void on_txtMailPort_editingFinished();

	void OnlabelServerID_customContextMenuRequested ( const QPoint & );
	void OnlabelIP_customContextMenuRequested ( const QPoint & );
	void OnlabelIPpriv_customContextMenuRequested ( const QPoint & );
	void OnlabelIDString_customContextMenuRequested ( const QPoint & );

	void OnCopyServerID();
	void OnCopyIP();
	void OnCopyIPLocal();
	void OnCopyIDString();

	void OnMenu_CatalogsClick();
	void OnMenu_UsersClick();
	void OnMenu_SettingsClick();
	void OnMenu_InfoClick();
	void OnMenu_InfoClickLink( const QString &){OnMenu_InfoClick();};

	void on_btnSystemOverview_clicked();
	void on_btnConnectClients_clicked();
	void on_btnPublishing_clicked();
	void on_btnAccessRight_clicked();
	void on_btnAccessFromInternet_clicked();
	void on_btnAbout_clicked();
	
	void on_btnDataDir_clicked();
	void on_btnCatalogDir_clicked();
	
	
	void OnHelp1_linkActivated( const QString &);
	void OnHelp2_linkActivated( const QString &);
	void OnHelp3_linkActivated( const QString &);

	void OnLink_BridgeID_ClickLink( const QString &);
	void OnLink_Access_ClickLink( const QString &);
	void UpdateWebPages();

	void on_txtPort_editingFinished();
	void on_txtPortEnd_editingFinished();

protected:
	void	timerEvent(QTimerEvent *event);
	void	keyPressEvent(QKeyEvent* event);

private:
	Ui::appserver_james_guiClass ui;

	void RefreshServerStatus(bool bSkipINIRefresh=false, bool bPowerUp=false);
	void SendReloadCommandToServer(Status &err,bool bSkipCatalogReload=false);
	void SendRenameCommandToServer(Status &err,QString strOldCatalog, QString strNewCatalog);
	void SendGetStatusCommandToServer(Status &err,DbRecordSet &Ret_Folders,int &nReloadSettings);
	void RefreshServerIcon();
	void RefreshDirectoryIcon(int nRow);
	void RefreshUserRightTable(int nRow);
	DbRecordSet GetAssignedUsers(QString strCatalog);
	void SetEmailSettings();
	void AddMyPictureDir();
	void BuildStyleWidgtets();
	void SetButtonStatus(int nStatus);
	void InitializeSettingsDirectory(QString &strHomePath);
	void InitDirs();
	void RefreshProgressBar();
	void ResetProgressBar(bool bForceShutDown=false);
	QString GetCurrentProcessingCatalog();
	void ProgressBar_CallServerForStatus(Status &err);
	void SetForwardMethod(int nMethod=-1);
	bool TestEmailSettings();
	bool TestAdmin();


	QString					m_strProgress_LastCatalogIndexing;
	int						m_nProgress_ServerCall_Count;
	QDateTime				m_datProgressStart;
	int						m_nProgressLastRemainToIndex;
	DbRecordSet				m_RetFolders;
	QLabel					*m_pLabel_info;

	
	SettingsManager			m_SettingsManager;
	DbRecordSet				m_lstFolders;
	DbRecordSet				m_lstFolders_Old;
	DbRecordSet				m_lstUsers;
	DbRecordSet				m_lstUserRights;
	DbRecordSet				m_lstUserRightsForFolder;
	DbRecordSet				m_lstUserRightsForFolder_Dummy;
	QString					m_strLastDir_FileOpen;
	
	AppServerConfigurator	*m_pServiceConfig;
	ServerIniFile			m_INIFile;
	QString					m_strIniFilePath;
	int						m_nTimerID;
	bool					m_bWasStarted;
	QString					m_strAppServerID;
	QString					m_strOuterIP;
	QString					m_strAppServerIPpriv;

	QAction*				m_pActionCopy_ServerID;
	QAction*				m_pActionCopy_IP;
	QAction*				m_pActionCopy_IPLocal;
	QAction*				m_pActionCopy_ID_StringAll;

	QString					m_strHomePath;
	QString					m_strConnectionString;

	StyledPushButton		*m_pBtnCatalogs;
	StyledPushButton		*m_pBtnUsers;
	StyledPushButton		*m_pBtnSettings;
	StyledPushButton		*m_pBtnInfo;
	StyledPushButton		*m_pBtnStart;
	QProgressBar			*m_ProgressBar;

	StyledPushButton		*m_pBtnHelp_Overview;
	StyledPushButton		*m_pBtnHelp_Client;
	StyledPushButton		*m_pBtnHelp_Publish;
	StyledPushButton		*m_pBtnHelp_Internet;
	int						nSkipClearStatus;
	int						m_nUpdateInterval;

};

#endif // APPSERVER_JAMES_GUI_H



