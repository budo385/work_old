#include "dlguserandpass.h"
#include <QMessageBox>


#if _WIN32
	#include <windows.h>
#endif



DlgUserAndPass::DlgUserAndPass(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle("Enter Windows username and password");

#if _WIN32
	TCHAR szUserName[128];
	unsigned int nSize=sizeof(szUserName);
	LPDWORD lpSize = (LPDWORD)&nSize;
	GetUserName(szUserName,lpSize);
	QString	strUser = QString::fromUtf16((const ushort *)szUserName);
	ui.txtUserName->setText(strUser);
	if (!strUser.isEmpty())
		ui.txtPassword1->setFocus();
#endif

}

DlgUserAndPass::~DlgUserAndPass()
{

}

void DlgUserAndPass::on_btnCancel_clicked()
{
	done(0);
}



void DlgUserAndPass::on_btnOK_clicked()
{
	if (ui.txtUserName->text().isEmpty())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Username must be set!"));
		return;
	}

	//test if username or password contain any spaces or utf8 chars:
	if (ui.txtUserName->text().simplified() != ui.txtUserName->text().simplified())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Username can not contain spaces!"));
		return;
	}


	done(1);
}