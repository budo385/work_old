#include "dlgusers.h"
#include "dlguserchange.h"
#include <QMessageBox>
#include <QTimer>

#include "common/common/config_version_ffh.h"
#include "pichelper.h"

DlgUsers::DlgUsers(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	setWindowTitle("Users");
	this->setStyleSheet(PicHelper::GetDialogStyle());
	SettingsManager::DefineUsers(m_lstUsers);
	m_lstDeletedUsers.copyDefinition(m_lstUsers);
	m_lstUsers.addColumn(QVariant::String,"USERNAME_OLD");

	//set data source for table, enable drop
	DbRecordSet columns;
	UniversalTableWidgetEx::AddColumnToSetup(columns,"USERNAME",tr("User Name"),140,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"LAST_NAME",tr("Last Name"),120,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"FIRST_NAME",tr("First Name"),120,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	ui.tableWidget->Initialize(&m_lstUsers,&columns);
	m_lstUsers.sort("USERNAME");
	ui.tableWidget->RefreshDisplay();
	m_bSelectMode=false;

	connect(ui.tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));

	ui.tableWidget->setStyleSheet(PicHelper::GetTableStyle());

	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnPass->setStyleSheet(strStyleBtn);
	ui.btnDelete->setStyleSheet(strStyleBtn);
	ui.btnAdd->setStyleSheet(strStyleBtn);

}

DlgUsers::~DlgUsers()
{

}

void DlgUsers::GetData(DbRecordSet &lstUsersSelected,DbRecordSet &lstUsersAll,DbRecordSet &lstDeletedUsers)
{
	lstUsersSelected.copyDefinition(m_lstUsers);
	lstUsersSelected.merge(m_lstUsers,true);
	lstUsersAll=m_lstUsers;
	lstDeletedUsers=m_lstDeletedUsers;
}


void DlgUsers::SetData(DbRecordSet lstUsers, SettingsManager *pSettings, bool bSelectMode, bool bAddUserEnabled, bool bSelectAll)
{
	m_pSettings=pSettings;

	m_lstUsers=lstUsers;
	m_lstUsers.addColumn(QVariant::String,"USERNAME_OLD");
	if (bSelectMode)
	{

		setWindowTitle("Select Users");
		if(bAddUserEnabled)
			ui.btnAdd->setVisible(true);
		else
			ui.btnAdd->setVisible(false);

		ui.btnDelete->setVisible(false);
		ui.btnPass->setText("OK");
	}
	m_lstUsers.sort("USERNAME");
	if (bSelectAll)
	{
		m_lstUsers.selectAll();
	}
	ui.tableWidget->RefreshDisplay();

	m_bSelectMode=bSelectMode;

	if (m_bSelectMode && bAddUserEnabled && m_lstUsers.getRowCount()==0)
	{
		QTimer::singleShot(0,this,SLOT(on_btnAdd_clicked()));
	}
}


void DlgUsers::on_btnAdd_clicked()
{
	DlgUserChange dlg;
	if(dlg.exec())
	{
		DbRecordSet recUser;
		dlg.GetData(recUser);

		QString strUserName=recUser.getDataRef(0,"USERNAME").toString();
		QString strPass=recUser.getDataRef(0,"PASSWORD").toString();

		if(m_lstUsers.find("USERNAME",strUserName)>0 || strUserName==m_pSettings->GetAdminUserName())
		{
			QMessageBox::critical(this,"Error","User with that username already exists!");
			return;
		}

		m_lstUsers.addRow();
		m_lstUsers.assignRow(m_lstUsers.getRowCount()-1,recUser,true);
		m_lstUsers.sort("USERNAME");
		m_lstUsers.find("USERNAME",strUserName);
		ui.tableWidget->RefreshDisplay();
	}
}
void DlgUsers::on_btnDelete_clicked()
{
	int nRowCount=m_lstUsers.getSelectedCount();
	if (nRowCount>0)
	{
		if(QMessageBox::question(this,"Confirmation",QString("Do you really want to delete selected users?").arg(nRowCount), tr("Yes"),tr("No"))==0)
		{
			m_lstDeletedUsers.merge(m_lstUsers,true);
			m_lstUsers.deleteSelectedRows();
			ui.tableWidget->RefreshDisplay();
		}
	}
}

void DlgUsers::on_btnPass_clicked()
{
	if (m_bSelectMode)
	{
		done(1);
		return;
	}

	int nRow=m_lstUsers.getSelectedRow();
	if (nRow<0)
	{
		QMessageBox::warning(this,"Warning","Select user to change password!");
		return;
	}

	DlgUserChange dlg(this);
	dlg.SetData(m_lstUsers.getRow(nRow));
	if(dlg.exec())
	{
		DbRecordSet recUser;
		dlg.GetData(recUser);
		
		QString strUserNameOld=m_lstUsers.getDataRef(nRow,"USERNAME").toString();
		QString strUserName=recUser.getDataRef(0,"USERNAME").toString();
		QString strPass=recUser.getDataRef(0,"PASSWORD").toString();

		if(m_lstUsers.find("USERNAME",strUserName)>1 || strUserName==m_pSettings->GetAdminUserName())
		{
			QMessageBox::critical(this,"Error","User with that username already exists!");
			return;
		}
		
		m_lstUsers.assignRow(nRow,recUser,true);
		m_lstUsers.setData(nRow,"USERNAME_OLD",strUserNameOld);
		/*
		m_lstUsers.setData(nRow,"USERNAME_OLD",m_lstUsers.getDataRef(nRow,"USERNAME").toString()); //saev old username in case this fruit cake decide to change username...
		m_lstUsers.setData(nRow,"USERNAME",strUserName);
		m_lstUsers.setData(nRow,"PASSWORD",strPass);
		*/
		m_lstUsers.sort("USERNAME");
		m_lstUsers.find("USERNAME",strUserName);
		ui.tableWidget->RefreshDisplay();
	}
}


void DlgUsers::OnCellDoubleClicked(int,int)
{
	done(1);
}