#include "table_userright.h"

Table_UserRight::Table_UserRight(QWidget *parent)
	: UniversalTableWidgetEx(parent)
{

}

Table_UserRight::~Table_UserRight()
{

}

void Table_UserRight::Initialize(DbRecordSet *plstData,bool bShowCatalogCol)
{
	//set data source for table, enable drop
	DbRecordSet columns;
	if (bShowCatalogCol)
		AddColumnToSetup(columns,"ALIAS",tr("Catalog"),120,false,"" ,COL_TYPE_TEXT,"");
	else
		AddColumnToSetup(columns,"USERNAME",tr("User Name"),120,false,"" ,COL_TYPE_TEXT,"");

	AddColumnToSetup(columns,"CAN_DOWNLOAD",tr("Can Download"),100,true, "",COL_TYPE_CHECKBOX);
	AddColumnToSetup(columns,"CAN_UPLOAD",tr("Can Upload"),100,true, "",COL_TYPE_CHECKBOX);
	AddColumnToSetup(columns,"CAN_DELETE",tr("Can Delete"),100,true, "",COL_TYPE_CHECKBOX);
	AddColumnToSetup(columns,"CAN_SEND_INVITE",tr("Can Send Invite"),100,true, "",COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::Initialize(plstData,&columns);
	//EnableDrop(true);
	//CreateContextMenu();
	//connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
}
