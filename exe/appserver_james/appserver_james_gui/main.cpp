#include "appserver_james_gui.h"
#include <QApplication>
#include "common/common/config_version_ffh.h"
#include "filecruncher.h"
#include "common/common/datahelper.h"

#include <QTranslator>
QTranslator *g_translator;




int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QCoreApplication::setOrganizationName("Helix Business Soft");
	QCoreApplication::setOrganizationDomain("everpics.com");
	QCoreApplication::setApplicationName("EverPICs Bridge");


	QStringList lstArgs=qApp->arguments();
	int nRow=lstArgs.indexOf("-convert");
	if (nRow>0)
	{
		if ((nRow+1)<lstArgs.size())
		{
			QString strDirToConvert=lstArgs.at(nRow+1);

			QByteArray data;
			if(!FileCruncher::EncodeDirectory2Binary(SETTINGS_PASSWORD,strDirToConvert,data))
				return false;

			QFile webdata(QCoreApplication::applicationDirPath()+"/webdata.bin");
			if(!webdata.open(QIODevice::WriteOnly))
				return false;
			webdata.write(data);
			webdata.close();

			return 0;
		}
	}

	
	
	//LOAD INI FILE and set lang: on first start ini will be in template settings: INNO setup will set to 'de' if german OS, on every other startup look for INI in home path
	ServerIniFile			m_INIFile;
	QString strIniFilePath= DataHelper::GetApplicationHomeDir()+"/"+QCoreApplication::applicationName()+"/settings/app_server.ini";
	bool bINILoaded=false;
	if(m_INIFile.Load(strIniFilePath,false))
		bINILoaded=true;
	else if(m_INIFile.Load(QCoreApplication::applicationDirPath()+"/template_settings/client.ini"))
		bINILoaded=true;

	if (bINILoaded)
	{
		if(m_INIFile.m_strLangCode == "de" )
		{
			g_translator = new QTranslator();
			bool bOK = g_translator->load("EXB_Manager_de.qm", QCoreApplication::applicationDirPath());
			Q_ASSERT(bOK);
			a.installTranslator(g_translator);
		}
	}


	appserver_james_gui w;
	w.show();
	

	return a.exec();
}
