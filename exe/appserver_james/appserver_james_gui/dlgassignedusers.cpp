#include "dlgassignedusers.h"
#include "settingsmanager.h"
#include "dlgusers.h"
#include <QMessageBox>
#include "dlgdefaultuserrights.h"
#include "pichelper.h"


DlgAssignedUsers::DlgAssignedUsers(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	

	SettingsManager::DefineUserRights(m_lstUserRightsForFolder);
	ui.tableUserRight->setStyleSheet(PicHelper::GetTableStyle());
	ui.tableUserRight->Initialize(&m_lstUserRightsForFolder,false);
	ui.tableUserRight->SetEditMode(true);
	
	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnCancel->setStyleSheet(strStyleBtn);
	ui.btnOK->setStyleSheet(strStyleBtn);
	ui.btnAddUserRight->setStyleSheet(strStyleBtn);
	ui.btnDeleteUserRight->setStyleSheet(strStyleBtn);

	this->setStyleSheet(PicHelper::GetDialogStyle());


}

DlgAssignedUsers::~DlgAssignedUsers()
{

}

void DlgAssignedUsers::SetData(DbRecordSet &lstUsers, SettingsManager *pSettings, DbRecordSet &lstUserRights,QString strCurrentFolder)
{
	m_pSettings=pSettings;
	m_lstUsers=lstUsers;
	m_lstUserRights=lstUserRights;
	m_strCurrentFolder=strCurrentFolder;
	
	m_lstUserRights.find("ALIAS",strCurrentFolder);
	m_lstUserRightsForFolder.merge(m_lstUserRights,true);
	m_lstUserRightsForFolder.sort("USERNAME");
	ui.tableUserRight->RefreshDisplay();

	setWindowTitle(QString("Assigned Users of \"%1\" ").arg(strCurrentFolder));
}


void DlgAssignedUsers::GetData(DbRecordSet &lstUsers, DbRecordSet &lstUserRights)
{
	//folders: set data back:
	//save previous values to the main list:
	int nSize=m_lstUserRightsForFolder.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strUserName=m_lstUserRightsForFolder.getDataRef(i,"USERNAME").toString();
		QString strCatalog=m_lstUserRightsForFolder.getDataRef(i,"ALIAS").toString();
		m_lstUserRights.find("ALIAS",strCatalog);
		m_lstUserRights.find("USERNAME",strUserName,false,true,true,true);

		int nRow=m_lstUserRights.getSelectedRow();
		if (nRow>=0)
		{
			DbRecordSet tmp=m_lstUserRightsForFolder.getRow(i);
			m_lstUserRights.assignRow(tmp,true);
		}

	}

	lstUsers=m_lstUsers;
	lstUserRights=m_lstUserRights;
}

void DlgAssignedUsers::on_btnAddUserRight_clicked()
{
	//open user dialog in selection mode:
	DlgUsers Dlg(this);
	Dlg.SetData(m_lstUsers,m_pSettings,true,true);
	if(!Dlg.exec())
	{
		//MB: requested this silly option...hahaha
		DbRecordSet lstSelectedData,lstUsers,lstDeleted;
		Dlg.GetData(lstSelectedData,lstUsers,lstDeleted);
		m_lstUsers.clear();
		m_lstUsers.merge(lstUsers);
		return;
	}

	DbRecordSet lstSelectedData,lstUsers,lstDeleted;
	Dlg.GetData(lstSelectedData,lstUsers,lstDeleted);
	m_lstUsers.clear();
	m_lstUsers.merge(lstUsers);

	lstSelectedData.clearSelection();
	int nSize=lstSelectedData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strUserName=lstSelectedData.getDataRef(i,"USERNAME").toString();
		QString strCatalog=m_strCurrentFolder;

		if (m_lstUserRightsForFolder.find("USERNAME",strUserName,true)>=0)
		{
			QMessageBox::warning(this,"Warning",QString("User %1 is already assigned to the catalog %2").arg(strUserName).arg(strCatalog));
			lstSelectedData.selectRow(i);
			continue;
		}


	}

	lstSelectedData.deleteSelectedRows();

	//selected user or users
	int nCount=lstSelectedData.getRowCount();
	if (nCount==0)
	{
		QMessageBox::warning(this,"Warning","Select one or more users to assign them to the catalog!");
		return;
	}
	

	//add to both total list and per folder:
	//if more > 1 show to choose access right for all
	if (lstSelectedData.getRowCount()>0)
	{

		DlgDefaultUserRights Dlg;
		if(!Dlg.exec())
			return;

		DbRecordSet rowUserRights=Dlg.GetDefaultUserRight();
		nSize=lstSelectedData.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			QString strCatalog=m_strCurrentFolder;

			m_lstUserRightsForFolder.addRow();
			int nRow=m_lstUserRightsForFolder.getRowCount()-1;
			m_lstUserRightsForFolder.assignRow(nRow,rowUserRights);
			m_lstUserRightsForFolder.setData(nRow,"USERNAME",lstSelectedData.getDataRef(i,"USERNAME"));
			m_lstUserRightsForFolder.setData(nRow,"ALIAS",strCatalog);

			m_lstUserRights.addRow();
			int nRow2=m_lstUserRights.getRowCount()-1;
			m_lstUserRights.assignRow(nRow2,rowUserRights);
			m_lstUserRights.setData(nRow2,"USERNAME",lstSelectedData.getDataRef(i,"USERNAME"));
			m_lstUserRights.setData(nRow2,"ALIAS",strCatalog);
		
		}

		ui.tableUserRight->RefreshDisplay();
	}

}

void DlgAssignedUsers::on_btnDeleteUserRight_clicked()
{
	int nCount=m_lstUserRightsForFolder.getSelectedCount();
	if (nCount==0)
	{
		QMessageBox::warning(this,"Warning","Select user right to delete it!");
		return;
	}

	//delete from main list and from local:
	int nSize=m_lstUserRightsForFolder.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (!m_lstUserRightsForFolder.isRowSelected(i))
			continue;

		QString strUserName=m_lstUserRightsForFolder.getDataRef(i,"USERNAME").toString();
		QString strCatalog=m_lstUserRightsForFolder.getDataRef(i,"ALIAS").toString();
		m_lstUserRights.find("ALIAS",strCatalog);
		m_lstUserRights.find("USERNAME",strUserName,false,true,true,true);

		m_lstUserRights.deleteSelectedRows();
	}

	m_lstUserRightsForFolder.deleteSelectedRows();
	ui.tableUserRight->RefreshDisplay();
}



void DlgAssignedUsers::on_btnCancel_clicked()
{
	done(0);
}

void DlgAssignedUsers::on_btnOK_clicked()
{
	done(1);
}