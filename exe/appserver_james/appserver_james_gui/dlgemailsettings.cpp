#include "dlgemailsettings.h"
#include "settingsmanager.h"
#include "pichelper.h"

DlgEmailSettings::DlgEmailSettings(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle("Email Settings");
	this->setStyleSheet(PicHelper::GetDialogStyle());

	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnCancel->setStyleSheet(strStyleBtn);
	ui.btnOK->setStyleSheet(strStyleBtn);

}

DlgEmailSettings::~DlgEmailSettings()
{

}


void DlgEmailSettings::SetData(DbRecordSet recData)
{
	ui.txtServer->setText(recData.getDataRef(0,"SMTP_SERVER").toString());
	ui.txtMailPort->setText(recData.getDataRef(0,"SMTP_PORT").toString());
	ui.txtEmail->setText(recData.getDataRef(0,"EMAIL_ADDRESS").toString());
	ui.txtUser->setText(recData.getDataRef(0,"USERNAME").toString());
	ui.txtPass->setText(recData.getDataRef(0,"PASSWORD").toString());
}
void DlgEmailSettings::GetData(DbRecordSet &recData)
{
	SettingsManager::DefineEmailSettings(recData);
	recData.addRow();
	
	recData.setData(0,"SMTP_SERVER", ui.txtServer->text());
	recData.setData(0,"SMTP_PORT", ui.txtMailPort->text().toInt());
	recData.setData(0,"EMAIL_ADDRESS", ui.txtEmail->text());
	recData.setData(0,"USERNAME", ui.txtUser->text());
	recData.setData(0,"PASSWORD", ui.txtPass->text());
}

void DlgEmailSettings::on_btnCancel_clicked()
{
	done(0);
}

void DlgEmailSettings::on_btnOK_clicked()
{
	done(1);
}

