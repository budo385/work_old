#ifndef PICHELPER_H
#define PICHELPER_H

#include <QByteArray>
#include <QPixmap>
#include <QGraphicsDropShadowEffect>

class PicHelper
{
public:

	static bool		CreateThumbFromFile(QString strFile, QByteArray &bufPicture,QPixmap &bufPixMap,int nWidth, int nHeight);
	static bool		LoadPicture(QByteArray &bufPicture,QByteArray &bufPictureLarge,QPixmap &bufPixMap,QPixmap &bufPixMapLarge,QString strCurrentPath="");
	static bool		LoadThumbFromRaw(QByteArray &bufPicture,QPixmap &bufPixMap, QString strIn,int nWidth, int nHeight);
	static QString	GetStyledButtonColorBkg(QString strBkgImage,QString strFontColor, int nFontSize=10,QString strHoverImage="");
	static QString	GetTableStyle();
	static QString	GetTreeStyle();
	static QString	GetBorderFrameStyle();
	static QString	GetDialogStyle();
	static void		SetPlatformDepedentFontSize();

	static QString	GetMyPicturesFolder();
	static QString	GetMyDocumentsFolder();
	static QGraphicsDropShadowEffect * GetShadowEffect();
	
};

#endif // PICHELPER_H
