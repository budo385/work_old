#ifndef DLGASKFORSERVICELOGON_H
#define DLGASKFORSERVICELOGON_H

#include <QDialog>
#include "ui_dlgaskforservicelogon.h"

class DlgAskForServiceLogon : public QDialog
{
	Q_OBJECT

public:
	DlgAskForServiceLogon(QWidget *parent = 0);
	~DlgAskForServiceLogon();

	bool GetPromptUserForPass(){return ui.rb2->isChecked();};

private slots:
	void on_btnCancel_clicked(){done(0);};
	void on_btnOK_clicked(){done(1);};

private:
	Ui::DlgAskForServiceLogon ui;
};

#endif // DLGASKFORSERVICELOGON_H
