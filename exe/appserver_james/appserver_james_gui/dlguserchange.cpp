#include "dlguserchange.h"
#include "common/common/authenticator.h"
#include <QMessageBox>
#include "settingsmanager.h"
#include "pichelper.h"

DlgUserChange::DlgUserChange(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle("User Details");
	this->setStyleSheet(PicHelper::GetDialogStyle());

	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnCancel->setStyleSheet(strStyleBtn);
	ui.btnOK->setStyleSheet(strStyleBtn);

}

DlgUserChange::~DlgUserChange()
{

}


void DlgUserChange::SetData(DbRecordSet recUser)
{
	ui.txtUserName->setText(recUser.getDataRef(0,"USERNAME").toString());
	ui.txtPassword1->setText(recUser.getDataRef(0,"PASSWORD").toString());
	ui.txtPassword2->setText(recUser.getDataRef(0,"PASSWORD").toString());
	ui.txtSalut->setText(recUser.getDataRef(0,"SALUT").toString());
	

	ui.txtFirstName->setText(recUser.getDataRef(0,"FIRST_NAME").toString());
	ui.txtLastName->setText(recUser.getDataRef(0,"LAST_NAME").toString());
	ui.txtCompany->setText(recUser.getDataRef(0,"COMPANY").toString());
	ui.txtSMS->setText(recUser.getDataRef(0,"SMS").toString());
	ui.txtEmail->setText(recUser.getDataRef(0,"EMAIL").toString());
}
void DlgUserChange::GetData(DbRecordSet &recUser)
{
	SettingsManager::DefineUsers(recUser);
	recUser.addRow();

	recUser.setData(0,"USERNAME", ui.txtUserName->text());
	recUser.setData(0,"PASSWORD", ui.txtPassword1->text());
	recUser.setData(0,"FIRST_NAME", ui.txtFirstName->text());
	recUser.setData(0,"LAST_NAME", ui.txtLastName->text());
	recUser.setData(0,"COMPANY", ui.txtCompany->text());
	recUser.setData(0,"SMS", ui.txtSMS->text());
	recUser.setData(0,"EMAIL", ui.txtEmail->text());
	recUser.setData(0,"SALUT", ui.txtSalut->text());
}

void DlgUserChange::on_btnCancel_clicked()
{
	done(0);
}


void DlgUserChange::on_txtFirstName_editingFinished()
{
	if (ui.txtSalut->text().isEmpty() && !ui.txtFirstName->text().isEmpty())
	{
		//�Dear [First_Name]�
		ui.txtSalut->setText("Dear "+ui.txtFirstName->text());
	}

}

void DlgUserChange::on_btnOK_clicked()
{
	if (ui.txtPassword1->text()!=ui.txtPassword2->text())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Password don't match!"));
		return;
	}

	if (ui.txtUserName->text().isEmpty())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Username must be set!"));
		return;
	}

	//test if username or password contain any spaces or utf8 chars:
	if (ui.txtUserName->text().simplified() != ui.txtUserName->text().simplified())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Username can not contain spaces!"));
		return;
	}
	if (ui.txtPassword1->text().simplified() != ui.txtPassword1->text().simplified())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Password can not contain spaces!"));
		return;
	}



	done(1);
}

