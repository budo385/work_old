#ifndef DLGEMAILSETTINGS_H
#define DLGEMAILSETTINGS_H

#include <QDialog>
#include "generatedfiles/ui_dlgemailsettings.h"
#include "common/common/dbrecordset.h"

class DlgEmailSettings : public QDialog
{
	Q_OBJECT

public:
	DlgEmailSettings(QWidget *parent = 0);
	~DlgEmailSettings();


	void SetData(DbRecordSet recData);
	void GetData(DbRecordSet &recData);

	private slots:
		void on_btnCancel_clicked();
		void on_btnOK_clicked();

private:
	Ui::DlgEmailSettingsClass ui;
};

#endif // DLGEMAILSETTINGS_H
