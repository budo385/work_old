#ifndef SERVICEHANDLER_H
#define SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "interface_servicehandler.h"

class ServiceHandler: public Interface_ServiceHandler
{
public:

	ServiceHandler();
	
	//catalog/files:
	void ReadCatalog(Status &pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Files, DbRecordSet &Ret_SubFolders, QString strAliasDirectory, int nFromN=-1,int nToN=-1, int nSortOrder=0, int nPicWidth=75,int nPicHeight=75, bool bKeepAspectRatio=false, bool bReturnThumbAsBinary=false);
	void GetPicture(Status &pStatus, int &Ret_nTotalCount, int &Ret_nCurrentPos, DbRecordSet &Ret_Files, QString strAliasDirectory, QString strFileName, int nHeading=0, int nSortOrder=0, int nPicWidth=75,int nPicHeight=75, bool bKeepAspectRatio=false, bool bReturnThumbAsBinary=false);
	void RotatePicture(Status &pStatus, DbRecordSet &Ret_Files, QString strAliasDirectory, QString strFileName, int nAngle=90,int nPicWidth=75,int nPicHeight=75, bool bKeepAspectRatio=false, bool bReturnThumbAsBinary=false);
	void DeleteDocument(Status &pStatus, QString strAliasDirectory,QString strFileName);
	void UploadDocument(Status &pStatus, QString strAliasDirectory,QString strFileName,QByteArray &byteFileData, bool bOverWrite=true,QString strSession="");
	void DownloadDocument(Status &pStatus, QString strAliasDirectory,QString strFileName,QByteArray &Ret_byteFileData,QString &strThumbLink,bool bReturnThumbAsBinary=false,bool bSkipAccess=false);
	void DownloadPicture(Status &pStatus, DbRecordSet &Ret_Files, QString strAliasDirectory,DbRecordSet &FileNames, int nFull, int nPicWidth=75,int nPicHeight=75, bool bReturnThumbAsBinary=false,bool bSkipAccess=false);
	void HidePicture(Status &pStatus, QString strAliasDirectory, QString strFileName,int nHide=0);
	void GetHidePictureList(Status &pStatus, QString strAliasDirectory, DbRecordSet &Ret_Files, int nPicWidth,int nPicHeight, bool bReturnThumbAsBinary);
	void RenamePicture(Status &pStatus, QString strAliasDirectory, QString strFileName, QString strDisplayName);
	
	void GetCatalogs(Status &pStatus, DbRecordSet &Ret_Catalogs);
	void GetCatalogs_iPad(Status &pStatus, DbRecordSet &Ret_Catalogs);
	void GetCatalogPicture(Status &pStatus, QString strCatalogName, QByteArray &Ret_byteFileData, QByteArray &Ret_byteFileDataLarge, int &Ret_nTotalCount, QString &Ret_strName);
	void RenameCatalog(Status &pStatus, QString strOldName, QString strNewName);
	void RenameCatalogClient(Status &pStatus, QString strOldName, QString strNewName);
	void CreateCatalogClient(Status &pStatus, QString strCatalogName);
	void UnpublishCatalogClient(Status &pStatus, QString strAliasDirectory);


	void Login(Status& pStatus, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode=0, QString strProgVer="", QString strClientID="", QString strPlatform="");
	void Logout(Status &pStatus,QString strSessionID);
	void GetFolderStatus(Status &pStatus, DbRecordSet &Ret_Folders,int &Ret_nSettingsChanged);
	void MakeInvitation(Status &pStatus, QString &Ret_strBody, int nMessageType, QString strCatalogName, QString strFirstName, QString strLastName,QString strEmail, QString strPhone,int nIsHtml,QString strEmailSubject,QString strEmailBody);

	//server control:
	void GetServerData(Status &pStatus, QString &Ret_strName,QByteArray &Ret_SrvPic,QString &Ret_strEmailSignature,int &nMajor,int &nMinor, int &nRev);
	void ReloadSettings(Status &pStatus, bool bSkipReloadCatalogs=false);
	
	

private:
	void GetPictureFromCatalog(Status &pStatus,QString strAliasDirectory,QString strFileName,int nWidth, int nHeight,int nWidthP, int nHeightP,QByteArray &byteFileData);
	void AddUser(Status &pStatus, DbRecordSet &rowUser,  QString strCatalogName,QString strFirstName, QString strLastName,QString strEmail, QString strPhone);
	void GetCatalogsAll(Status &pStatus, DbRecordSet &Ret_Catalogs);


};

#endif // SERVICEHANDLER_H
