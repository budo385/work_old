#ifndef APPLICATIONSERVER_H
#define APPLICATIONSERVER_H


#include <QObject>
#include "serverinifile.h"
#include "resthttpserver_local.h"
#include "restrpcdispatcher.h"
#include "trans/trans/httpserver.h"
#include "trans/trans/htmlhttpserver.h"
#include "common/common/backgroundtaskexecutor.h"
#include "garbagecollector.h"
#include "connectionhandler.h"
#include "thumbmanagertaskexecutor.h"
#include "trans/trans/httpmultipartserver.h"
#include "httpmessagehandler.h"

class ApplicationServer: public QObject
{
	Q_OBJECT 

public:

	ApplicationServer(QObject *parent=NULL);
	~ApplicationServer();

	//main API
	void Start(Status &pStatus);
	void Stop();
	void Restart(Status &pStatus);
	void RestartDelayed(int nReStartAfterSec);
	void StopDelayed(int nStopAfterSec=0);
	bool IsServiceRunning(){return m_bIsRunning;}
	void SaveSettings();
	QString InitUserStorage();
	HTTPContext GetThreadContext(int nThreadID){return SokratesHttpServer.GetThreadContext(nThreadID);};
	void SetLastError(int nErrorCode,QString strErrorTextArgs="");
	ServerIniFile* GetIniFile(){return &m_INIFile;};
	QString GetIniFilePath(){return m_strIniFilePath;};
	void ChangeHTTPServerPortDelayed(int nNewPort);
	bool IsHttpStarted();
	

	
	//note: timers can not restart from another thread, use signals
protected slots:
	void ChangeHTTPServerPortSlot(int nNewPort);
	void RestartDelayedThreadSafeSlot(int);
	void StopThreadSafeSlot();
	

protected:
	void timerEvent(QTimerEvent *event); //overriden QObject timer method

signals:
	void ChangeHTTPServerPortSignal(int);
	void RestartDelayedThreadSafeSignal(int);
	void StopThreadSafeSignal();

private:
	bool InitWebPages(QHash<QString,HtmlDocument> &lstWebPages);
	void ClearPointers();
	void RestartDelayedPriv(int nReStartAfterSec);
	void LoadINI(Status &pStatus);
	void LoadNetSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings);
	void InitializeSettingsDirectory(QString &strHomePath);




	//void LoadDbSettings(Status &pStatus,DbConnectionSettings &pDbSettings);
	//void InitDatabaseConnection(Status &pStatus, DbSqlManager **pDbManager,  DbConnectionSettings &ConnSettings, int nMaxDbConnections=0);


	//TRANSPORT
	HtmlHTTPServer					*m_HtmlHTTPServer;
	RestHTTPServer_Local			*m_RestHTTPServer;
	HTTPMultipartServer				*m_HttpMultipartMessageServer;
	HTTPMessageHandler				*m_HttpMessageHandler;

	RestRpcDispatcher				*m_RestServiceRpcDispatcher;
	GarbageCollector				*m_GarbageCollector;
	ConnectionHandler				*m_ConnectionHandler;
	HTTPServer						SokratesHttpServer;
	ThumbManagerTaskExecutor		*m_ThumbTask;
	BackgroundTaskExecutor			*m_GbBkgTask;
	BackgroundTaskExecutor			*m_ConnectionHandlerTask;

	//INI
	ServerIniFile m_INIFile;
	QString m_strIniFilePath;

	bool m_bIsRunning;
	bool m_HttpServerStarted;
	int m_nTimerID;
	QMutex m_Mutex;					///< mutex

};


#endif // APPLICATIONSERVER_H
