#include "connectionhandler.h"
#include "trans/trans/resthttpclient.h"
#include "common/common/config_version_john.h"
#include "trans/trans/httpreadersync.h"
#include "trans/trans/tcphelper.h"
#include "common/common/threadid.h"

#include "common/common/logger.h"
extern Logger							g_Logger;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;

#define N_TRIES_TO_CONNECT 1

void NATLogger(int, const char *szMsg, unsigned long)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, szMsg);
}

ConnectionHandler::ConnectionHandler(ServerIniFile *INIFile, QString strIniFilePath,QObject *parent)
: QObject(parent),m_bRegisterNotFirstTime(false)
{
	m_INIFile=INIFile;
	m_strIniFilePath=strIniFilePath;
	m_bRegisterDone=false;
	m_nPortForwardMethod=0;
	m_bTarantulaWasAliveLastTimeChecked=false;
	m_objNAT.RegisterLogger(NATLogger, 0);
	m_nCurrentPort=m_INIFile->m_nAppServerPort;
}


ConnectionHandler::~ConnectionHandler()
{
	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Conn Handler destruct: 1");
	Status err;

	if (m_bRegisterDone)
	{
		UnRegister(err);
		if (!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_UNREGISTER,err.getErrorText());
		}
	}

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Conn Handler destruct: 2");

	StopPortForward(err);
	m_objNAT.Uninitialize();

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Conn Handler destruct: done");
}


//test accessability every N minutes
void ConnectionHandler::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Conn Handler execute: START, thread: %1").arg(QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()));

	Status err;
	if (!m_bRegisterDone)
	{
		StartPortForward(err);
		if (!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_FORWARD_PORT,QString::number(m_INIFile->m_nAppServerPort));
			//g_AppServer->SetLastError(StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_FORWARD_PORT,QString::number(m_INIFile->m_nAppServerPort));
		}
		m_bRegisterDone=true;
		//return;

	}

	Register(err);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_REGISTER,err.getErrorText());
		g_AppServer->SetLastError(StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_REGISTER,err.getErrorText());
		if (!m_bRegisterNotFirstTime) //feature by MB: test for tarantula, then if tarantula failes: do not stop server, just keep on...
			g_AppServer->StopDelayed();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Conn Handler execute: STOP, thread: %1").arg(QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()));

	m_bRegisterNotFirstTime=true;

}

//MB: on 04.10.2011 MB requested that I try 3 times on 3 different ports in port range:
//stop/start http server, if can not start on last port then stop server
void ConnectionHandler::Register(Status &err)
{
	//CONNECT:
	RestHTTPClient client;

	int nTry=0;
	QString strTarantulaIP=GetTarantulaIP(nTry);
	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Tarantula IP: "+strTarantulaIP);
	while (!strTarantulaIP.isEmpty() && nTry<N_TRIES_TO_CONNECT)
	{
		client.SetConnectionSettingsURL(strTarantulaIP,FFH_CONNECT_SERVER_TIMEOUT);
		client.Connect(err);
		if (err.IsOK())
		{
			break;
		}
		nTry++;
		strTarantulaIP=GetTarantulaIP(nTry);

		//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Tarantula IP: "+strTarantulaIP);
	}

	if (!client.IsConnected())
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Unable to connect to the connection server!");
		return;
	}

	m_bTarantulaWasAliveLastTimeChecked=true;

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Register to tarantula");

	//PREPARE TO SEND:
	int nCode=SERVER_CODE_JAMES;
	int nMajor=JOHN_PROGRAM_VERSION_MAJOR;
	int nMinor=JOHN_PROGRAM_VERSION_MINOR;
	int nRevision=JOHN_PROGRAM_VERSION_REVISION;
	QString strPlatformUID="";
	QString strHostIdentitiy="";
	QString strHostUrl="";
	int nPort=m_nCurrentPort;
	QString strID=m_INIFile->m_strAppServerID;
	int nSSL=m_INIFile->m_nSSLMode;
	int nForcePublicIP=m_INIFile->m_nForcePublicIP;

	QString strPlatformName="Windows";

#ifndef _WIN32
	strPlatformName="OS X";
#endif

	int nCntTries=3;
	int strStatusCode;
	QString strStatusText;
	bool bErrDetected=false;

	do 
	{
		bErrDetected=false;

		//refresh local IP on every call:
		QString strAppServerIPLocal=TcpHelper::ResolveLocalHostURLService("0.0.0.0",m_nCurrentPort);

		client.msg_send.ClearData();
		client.msg_recv.ClearData();

		client.msg_send.AddParameter(&nCode,"PROGRAM_CODE");
		client.msg_send.AddParameter(&nMajor,"PROGRAM_VERSION_MAJOR");
		client.msg_send.AddParameter(&nMinor,"PROGRAM_VERSION_REVISION");
		client.msg_send.AddParameter(&nRevision,"PROGRAM_PLATFORM_NAME");
		client.msg_send.AddParameter(&strPlatformName,"PROGRAM_PLATFORM_NAME");
		client.msg_send.AddParameter(&strPlatformUID,"PROGRAM_PLATFORM_UID");
		client.msg_send.AddParameter(&strID,"CLIENT_IDENTITY");
		client.msg_send.AddParameter(&strHostIdentitiy,"HOST_IDENTITY");
		client.msg_send.AddParameter(&strHostUrl,"HOST_URL");
		client.msg_send.AddParameter(&m_nCurrentPort,"HOST_PORT");
		client.msg_send.AddParameter(&m_nPortForwardMethod,"HOST_STATS_NAT_USED");
		client.msg_send.AddParameter(&nSSL,"HOST_SSL");
		client.msg_send.AddParameter(&strAppServerIPLocal,"HOST_LOCAL_IP");
		client.msg_send.AddParameter(&nForcePublicIP,"FORCE_PUBLIC_IP");

		//SEND:
		client.RestSend(err,"POST","/rest/service/register");
		if (!err.IsOK())
		{
			return;
		}

		client.msg_recv.GetParameter(0,&strStatusCode,err);	if (!err.IsOK())return;
		client.msg_recv.GetParameter(1,&strStatusText,err);if (!err.IsOK())return;
		client.msg_recv.GetParameter(2,&strID,err);if (!err.IsOK())return;
		client.msg_recv.GetParameter(3,&strHostUrl,err);if (!err.IsOK())return;
		client.msg_recv.GetParameter(4,&nPort,err);if (!err.IsOK())return;

		nCntTries--;

		//if IP is changed leave port as just refresh nat pnp objects: -> bug reported by MB on skype on 10.01.2014
		if (m_INIFile->m_strOuterIP != strHostUrl)
		{
			//nat pnpn com com
			Status err_x;
			StopPortForward(err_x);
			m_objNAT.Uninitialize();
			StartPortForward(err_x);
		}

		if (strStatusCode==2)
		{
			bErrDetected=true;

			if (m_INIFile->m_nAppServerPortRangeEnd > m_INIFile->m_nAppServerPort)
			{
				m_nCurrentPort++;
				if (m_nCurrentPort>m_INIFile->m_nAppServerPortRangeEnd)
					m_nCurrentPort=m_INIFile->m_nAppServerPort;

				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("ConnectionHandler: can't forward port, trying port: %1").arg(m_nCurrentPort));
				//stop server:
				g_AppServer->ChangeHTTPServerPortDelayed(m_nCurrentPort);
				int nMaxCnt=20; //max minute or so then abort all
				while (!g_AppServer->IsHttpStarted() && nMaxCnt>0)
				{
					nMaxCnt--;
					ThreadSleeper::Sleep(5000);
				}

				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("ConnectionHandler: port changed, continue"));
			}
			else
				nCntTries=0; //if only one port available and that port is not open, then reduce tries to 1.

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Reset UPNP objects"));

			//nat pnpn com com
			Status err_x;
			StopPortForward(err_x);
			m_objNAT.Uninitialize();
			StartPortForward(err_x);
			if (!err_x.IsOK())
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_FORWARD_PORT,QString::number(m_nCurrentPort));
		}


	} while(nCntTries>0 && bErrDetected);



	if (strStatusCode)
	{
		if (strStatusCode==2)
		{
			g_AppServer->SetLastError(StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_TEST_CONNECTIVITY); //just write down error
			m_nPortForwardMethod=0; //port not active
		}
		else
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Error while trying to register server: %1!").arg(strStatusText));
			return;
		}
	}
	else
	{
		if (m_nPortForwardMethod==0) //if not upnp or nat then set to manual
			m_nPortForwardMethod=JOHN_PORT_FORWARD_MANUAL;
		g_AppServer->SetLastError(0); //reset error
	}


	//save all into INI:
	m_INIFile->m_strAppServerID=strID;
	m_INIFile->m_nAppServerPortActual=m_nCurrentPort;


	//reset forward method to manual if no upnp or nat detected
	if(strStatusCode!=2 && m_nPortForwardMethod==0)
		m_nPortForwardMethod=JOHN_PORT_FORWARD_MANUAL;

	m_INIFile->m_nPortForwardMethod=m_nPortForwardMethod;
	m_INIFile->m_strOuterIP=strHostUrl;
	m_INIFile->m_nServerTestedConnection=1;
	m_INIFile->Save(m_strIniFilePath);
}


void ConnectionHandler::UnRegister(Status &err)
{
	if(!m_bTarantulaWasAliveLastTimeChecked)
		return;


	//CONNECT:
	RestHTTPClient client;

	int nTry=0;
	QString strTarantulaIP=GetTarantulaIP(nTry);
	while (!strTarantulaIP.isEmpty() && nTry<N_TRIES_TO_CONNECT)
	{
		client.SetConnectionSettingsURL(strTarantulaIP,FFH_CONNECT_SERVER_TIMEOUT);
		client.Connect(err);
		if (err.IsOK())
		{
			break;
		}
		nTry++;
		strTarantulaIP=GetTarantulaIP(nTry);
	}

	if (!client.IsConnected())
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Unable to connect to the connection server!");
		return;
	}

	//PREPARE TO SEND:
	QString strID=m_INIFile->m_strAppServerID;
	client.msg_send.AddParameter(&strID,"CLIENT_IDENTITY");

	//SEND:
	client.RestSend(err,"POST","/rest/service/unregister");
	if (!err.IsOK())
	{
		return;
	}

	int strStatusCode;
	QString strStatusText;

	client.msg_recv.GetParameter(0,&strStatusCode,err);	if (!err.IsOK())return;
	client.msg_recv.GetParameter(1,&strStatusText,err);if (!err.IsOK())return;
	if (strStatusCode)
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Error while trying to un register server: %1!").arg(strStatusText));
		return;
	}

	//save all into INI:
	//m_INIFile->m_strAppServerID=strID;
	//m_INIFile->Save(m_strIniFilePath);

}
void ConnectionHandler::TestConnect(Status &err)
{
	if(!m_bTarantulaWasAliveLastTimeChecked)
		return;

	//CONNECT:
	RestHTTPClient client;

	int nTry=0;
	QString strTarantulaIP=GetTarantulaIP(nTry);
	while (!strTarantulaIP.isEmpty() && nTry<N_TRIES_TO_CONNECT)
	{
		client.SetConnectionSettingsURL(strTarantulaIP,FFH_CONNECT_SERVER_TIMEOUT);
		client.Connect(err);
		if (err.IsOK())
		{
			break;
		}
		nTry++;
		strTarantulaIP=GetTarantulaIP(nTry);
	}

	if (!client.IsConnected())
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Unable to connect to the connection server!");
		return;
	}

	//PREPARE TO SEND:
	QString strHostUrl="";
	int nPort=m_nCurrentPort;
	QString strID=m_INIFile->m_strAppServerID;

	client.msg_send.AddParameter(&strID,"CLIENT_IDENTITY");
	client.msg_send.AddParameter(&nPort,"HOST_PORT");

	//SEND:
	client.RestSend(err,"POST","/rest/service/ping");
	if (!err.IsOK())
	{
		return;
	}

	int strStatusCode;
	QString strStatusText;
	client.msg_recv.GetParameter(0,&strStatusCode,err);	if (!err.IsOK())return;
	client.msg_recv.GetParameter(1,&strStatusText,err);if (!err.IsOK())return;
	client.msg_recv.GetParameter(2,&strHostUrl,err);if (!err.IsOK())return;

	if (strStatusCode)
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Error while trying to ping connection server: %1!").arg(strStatusText));
		return;
	}

	client.Disconnect(err);

	//save all into INI:
	if (m_INIFile->m_strOuterIP!=strHostUrl)
	{
		//register again:
		Register(err);
	}
	m_INIFile->m_strOuterIP=strHostUrl;
	m_INIFile->Save(m_strIniFilePath);
}

QString ConnectionHandler::GetTarantulaIP(int nTry)
{

	if (nTry==0)
	{
		return FFH_CONNECT_SERVER_MASTER_IP;
	}
	else if (nTry!=0)
	{
		//http client, get page:
		QByteArray strResult;
		HttpReaderSync WebReader(10000);  //6sec..
		if(WebReader.ReadWebContent(FFH_CONNECT_SERVER_LIST, &strResult))
		{
			strResult = "http://"+strResult;
			return strResult;
		}

	}

	return "";

}

//changes m_nPortForwardMethod, if 0 then port is not forwareded
void ConnectionHandler::StartPortForward(Status &err)
{
	m_nPortForwardMethod=0;

	m_objNAT.Uninitialize();
	m_objNAT.Initialize();

	if (m_INIFile->m_nEnableNAT)
	{
		m_objNAT.EnableNatPmp(true);
		int nStatus=m_objNAT.StartPortForwardRequest(m_nCurrentPort);
		if (nStatus!=PFS_PORT_MAPPED)
		{
			m_objNAT.EnableNatPmp(false);
			err.setError(1,"Failed to forward port using NATPMP");
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to forward port using NATPMP");
		}
		else
		{
			m_nPortForwardMethod=JOHN_PORT_FORWARD_NATPMP;
		}
	}

	if (m_INIFile->m_nEnableUpnp)
	{
		m_objNAT.EnableUPnP(true);
		int nStatus=m_objNAT.StartPortForwardRequest(m_nCurrentPort);
		if (nStatus!=PFS_PORT_MAPPED)
		{
			m_objNAT.EnableUPnP(false);
			err.setError(1,"Failed to forward port using NATPMP");
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to forward port using UPNP");
		}
		else
		{
			m_nPortForwardMethod=JOHN_PORT_FORWARD_UPNP;
		}
	}

	if (m_nPortForwardMethod)
	{
		err.setError(0);
	}

	m_INIFile->m_nPortForwardMethod=m_nPortForwardMethod;
	m_INIFile->Save(m_strIniFilePath);
}

void ConnectionHandler::StopPortForward(Status &err)
{
	err.setError(0);
	if (m_nPortForwardMethod)
	{
		m_objNAT.StopPortForwardRequest(m_nCurrentPort);
		if (m_nPortForwardMethod==JOHN_PORT_FORWARD_NATPMP)
			m_objNAT.EnableNatPmp(false);
		else
			m_objNAT.EnableUPnP(false);
	}

}