#include "thumbmanager.h"
#include "filecruncher.h"
#include <QDir>
#include "common/common/datahelper.h"
#include "common/common/threadid.h"
#include "iphotohandler.h"

#include "settingsmanager.h"
extern SettingsManager		*g_SettingsManager;
#include "common/common/logger.h"
extern Logger				g_Logger;
extern QString				g_strHomePath;
#include "applicationserver.h"
extern ApplicationServer				*g_AppServer;


ThumbManager::ThumbManager(QObject *parent)
	: QObject(parent)
{
	m_lstThumbSettings.addColumn(QVariant::Int,"Width");
	m_lstThumbSettings.addColumn(QVariant::Int,"Height");
	m_lstThumbSettings.addColumn(QVariant::Int,"KeepRatio");
	m_lstThumbSettings.addColumn(QVariant::String,"ThumbSuffix");

}

ThumbManager::~ThumbManager()
{

}

void ThumbManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	//qDebug()<<"thumb man thread: "<< ThreadIdentificator::GetCurrentThreadID();
	ProcessAll();
	//CleanGarbage(); <-in garbage collector already
}


//add this on beginning as its not protected by mutex
void ThumbManager::AddThumbTypeForProcess(int nWidth,int nHeight,QString strSuffix,bool bKeepAspectRatio)
{
	m_lstThumbSettings.addRow();
	int nRow=m_lstThumbSettings.getRowCount()-1;
	m_lstThumbSettings.setData(nRow,"Width",nWidth);
	m_lstThumbSettings.setData(nRow,"Height",nHeight);
	m_lstThumbSettings.setData(nRow,"KeepRatio",(int)bKeepAspectRatio);
	m_lstThumbSettings.setData(nRow,"ThumbSuffix",strSuffix);

	m_lstThumbSufix.append(strSuffix);

}

bool ThumbManager::IsSupporttedThumbType(int nWidth,int nHeight,bool bKeepAspectRatio)
{
	//hardcoded:
	if (nWidth==THUMB_SMALL_WIDTH && nHeight==THUMB_SMALL_HEIGHT)
	{
		return true;
	}
	if (nWidth==THUMB_SMALL_2_WIDTH && nHeight==THUMB_SMALL_2_HEIGHT)
	{
		return true;
	}
	else if (nWidth==THUMB_LARGE_WIDTH && nHeight==THUMB_LARGE_HEIGHT)
	{
		return true;
	}
	//else if (nWidth==THUMB_LARGE_WIDTH_PORTRAIT && nHeight==THUMB_LARGE_HEIGHT_PORTRAIT)
	//{
	//	return true;
	//}
	else if (nWidth==THUMB_LARGE_3_WIDTH && nHeight==THUMB_LARGE_3_HEIGHT)
	{
		return true;
	}
	//else if (nWidth==THUMB_LARGE_3_WIDTH_PORTRAIT && nHeight==THUMB_LARGE_3_HEIGHT_PORTRAIT)
	//{
	//	return true;
	//}
	else if (nWidth==THUMB_LARGE_4_WIDTH && nHeight==THUMB_LARGE_4_HEIGHT)
	{
		return true;
	}
	//else if (nWidth==THUMB_LARGE_4_WIDTH_PORTRAIT && nHeight==THUMB_LARGE_4_HEIGHT_PORTRAIT)
	//{
	//	return true;
	//}

	return false;
}

QString ThumbManager::GetThumbSuffix(int nWidth,int nHeight)
{
	return "_"+QString::number(nWidth)+"x"+QString::number(nHeight);
}

bool ThumbManager::Initialize(QString strOutThumbDir, int nJpegQuality)
{
	if (strOutThumbDir.isEmpty())
	{
		m_strOutThumbDirRoot=g_strHomePath+"/user_storage/thumb_cache";
		if (!QFile::exists(m_strOutThumbDirRoot))
		{
			QDir dir(g_strHomePath+"/user_storage");
			return dir.mkdir("thumb_cache");
		}
	}
	else
	{
		m_strOutThumbDirRoot=strOutThumbDir;
		if (!QFile::exists(m_strOutThumbDirRoot))
			return false;
	}


	m_lstThumbSufix.append("_full"); //suffix for original pic: added here to avoid deletion
	m_lstThumbSufix.sort();

	if (nJpegQuality<30 || nJpegQuality>100)
		m_nJpegQuality=40;
	m_nJpegQuality=nJpegQuality;
	return true;
}


//get all folders, catalogs/dismantle all of them per subdirectories and parse each dir
//on timer, on signal
void ThumbManager::ProcessAll()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Thumb: process all started, thread: %1").arg(ThreadIdentificator::GetCurrentThreadID()));

	//if locked then it's ok: some1 is processing already
	if(!m_mutexProcessAll.tryLock())
	{
		return;
	}
	
	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);

	//----------------RE-CHECK ALL COUNTS and purge all deleted directories
	int nSize=lstFolders.getRowCount();
	QString straAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC);

	//clear cnt 
	m_lstCatalogCntLock.lockForWrite();
	m_lstCatalogFileCnt.clear();
	m_lstCatalogCntLock.unlock();

	
	bool bMainCatalogSetForIndexingInProgress=false;

	for (int i=0;i<nSize;i++)
	{
		QString strAlias=			lstFolders.getDataRef(i,"ALIAS").toString();
		QString strAlbumIPhotoID=	lstFolders.getDataRef(i,"IPHOTO_ID").toString();
		QString strPath =			lstFolders.getDataRef(i,"PATH").toString();
		
		FileCruncher::ReplaceNetworkPathWithUNC(strPath);

		int nCount=0;
		if (strAlbumIPhotoID.isEmpty())
			nCount=FileCruncher::GetFileCountFromFolder(strPath,straAllowedFileTypes);
		else
			nCount=FileCruncher_iPhoto::GetFileCountFromFolder(strAlbumIPhotoID,straAllowedFileTypes);

		SetCatalogCnt(strAlias,nCount);

		//set for new folders status:
		m_lstCatalogStatusLock.lockForWrite();
		if (!m_lstCatalogStatus.contains(strAlias) && !bMainCatalogSetForIndexingInProgress)
		{
			bMainCatalogSetForIndexingInProgress=true; //ensure to set only 1 catalog at time to indexing status
			m_lstCatalogStatus[strAlias]=STATUS_INDEXING;
		}
		m_lstCatalogStatusLock.unlock();


		//get all subfolders:
		QList<QString> lstSubFolders;
		FileCruncher::GetAllSubFolders(strPath,lstSubFolders);
		int nSize2=lstSubFolders.size();

		for (int k=0;k<nSize2;k++)
		{
			QString strAliasPath=strPath+"/"+lstSubFolders.at(k);
			QString strAliasSub=strAlias+"/"+lstSubFolders.at(k);
			if (strAlbumIPhotoID.isEmpty())
				nCount=FileCruncher::GetFileCountFromFolder(strAliasPath,straAllowedFileTypes);
			else
				nCount=FileCruncher_iPhoto::GetFileCountFromFolder(strAlbumIPhotoID,straAllowedFileTypes);
			SetCatalogCnt(strAliasSub,nCount);
			//set for new folders status:
			m_lstCatalogStatusLock.lockForWrite();
			if (!m_lstCatalogStatus.contains(strAliasSub))
				m_lstCatalogStatus[strAliasSub]=STATUS_NOT_INDEXED;
			m_lstCatalogStatusLock.unlock();

		}
	}


	//clear all  status'es that are not in list, if dir not exists then skip
	m_lstCatalogStatusLock.lockForWrite();
	lstFolders.clearSelection();	



	QStringList lstToDelete;

	QHashIterator<QString,int> k(m_lstCatalogStatus); 
	while (k.hasNext()) 
	{
		k.next();
		QString strAliasSearched = k.key();

		if (strAliasSearched.indexOf("/")>0)
		{
			strAliasSearched=strAliasSearched.split("/").at(0);
		}

		if (lstFolders.find("ALIAS",strAliasSearched,true)<0)
			lstToDelete<<k.key();
	}

	for (int i=0;i<lstToDelete.size();i++)
		m_lstCatalogStatus.remove(lstToDelete.at(i));

	m_lstCatalogStatusLock.unlock();




	//----------------Make Thumbs
	nSize=lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strCatalogPath=lstFolders.getDataRef(i,"PATH").toString();
		FileCruncher::ReplaceNetworkPathWithUNC(strCatalogPath);
		QString strAlias=lstFolders.getDataRef(i,"ALIAS").toString();

		QList<QString> lstSubFolders;
		FileCruncher::GetAllSubFolders(strCatalogPath,lstSubFolders);

		CreateThumbsFromAlias(strAlias);
		int nSize2=lstSubFolders.size();

		int nStatus = GetCatalogStatus(strAlias);
		SetCatalogStatus(strAlias,STATUS_INDEXING); //set root folder to indexing
		
		for (int k=0;k<nSize2;k++)
		{
			QString strCatalogDirPath=strAlias+"/"+lstSubFolders.at(k);
			CreateThumbsFromAlias(strCatalogDirPath);
		}
		SetCatalogStatus(strAlias,nStatus); //set root folder to indexing
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Thumb: process all ended"));

	m_mutexProcessAll.unlock();
}

//get all thumb folder, compare with real folder one by one: delete if not exists
void ThumbManager::CleanGarbage()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("ThumbManager: Start clean garbage"));


	QString straAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC);


	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);

	int nSize=lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strCatalogPath=lstFolders.getDataRef(i,"PATH").toString();
		FileCruncher::ReplaceNetworkPathWithUNC(strCatalogPath);
		QString strAlias=lstFolders.getDataRef(i,"ALIAS").toString();
		QString strCatalogThumbRootPath=m_strOutThumbDirRoot+"/"+strAlias;
		QString strAlbumIPhotoID=lstFolders.getDataRef(i,"IPHOTO_ID").toString();


		QList<QString> lstSubFolders;
		if (strAlbumIPhotoID.isEmpty())
		{


			FileCruncher::GetAllSubFolders(strCatalogPath,lstSubFolders);
			if (!QFile::exists(strCatalogPath))
			{
				FileCruncher::RemoveAllFromDirectory(strCatalogThumbRootPath);
				continue;
			}


	
			FileCruncher::CompareFolderAndDeleteNonExisting(strCatalogThumbRootPath,strCatalogPath,m_lstThumbSufix,straAllowedFileTypes);


			int nSize2=lstSubFolders.size();
			for (int k=0;k<nSize2;k++)
			{
				QString strSourceDirPath=strCatalogPath+"/"+lstSubFolders.at(k);
				QString strThumbDirPath=strCatalogThumbRootPath+"/"+lstSubFolders.at(k);
				if (!QFile::exists(strSourceDirPath))
				{
					FileCruncher::RemoveAllFromDirectory(strThumbDirPath);
				}
				else
					FileCruncher::CompareFolderAndDeleteNonExisting(strThumbDirPath,strSourceDirPath,m_lstThumbSufix,straAllowedFileTypes);
			}


		}
		else
		{
			FileCruncher_iPhoto::CompareFolderAndDeleteNonExisting(strCatalogThumbRootPath,strAlbumIPhotoID,m_lstThumbSufix,straAllowedFileTypes);
		}

	}

	//get all dirs from thumb dir:
	QList<QString> lstDirs=DataHelper::GetSubDirectories(m_strOutThumbDirRoot);

	nSize=lstDirs.size();
	int nSize2=lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString dirName=lstDirs.at(i);

		bool bFound=false;
		for (int k=0;k<nSize2;k++)
		{
			QString strAlias=lstFolders.getDataRef(k,"ALIAS").toString();
			if (strAlias==dirName)
			{
				bFound=true;
				break;
			}
		}

		if (!bFound)
		{
			FileCruncher::RemoveAllFromDirectory(m_strOutThumbDirRoot+"/"+dirName);
		}

	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("ThumbManager:End clean garbage"));
}

//if not exist it creates it:
QString ThumbManager::GetThumbPathBasedOnCatalogPath(QString strCatalogDirPath)
{
	QString strPath=QDir::cleanPath(m_strOutThumbDirRoot+"/"+strCatalogDirPath);
	QDir dir(strPath);
	if (!dir.exists())
	{
		if(!dir.mkpath(strPath))
			return "";
	}
	return strPath;
}

//based on catalog path find actual path and create thumbs based on the m_lstThumbSettings
void ThumbManager::CreateThumbsFromAlias(QString strCatalogDirPath)
{

	SetCatalogStatus(strCatalogDirPath,STATUS_INDEXING);

	QString strDirInPath;
	QString strDirOutPath;
	QString strAlias;
	QString straAllowedFileTypes;
	Status err;
	int nFileTypesCode;
	QString strAlbumIPhotoID;
	g_SettingsManager->GetDirectoryPath(err,strCatalogDirPath,strAlias,strDirInPath,nFileTypesCode,straAllowedFileTypes,strAlbumIPhotoID);
	if (!err.IsOK())
	{
		SetCatalogStatus(strCatalogDirPath,STATUS_COULD_NOT_BE_INDEXED); 
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Could not resolve catalog directory %1").arg(strCatalogDirPath));
		return;
	}


	//only thumb creation for file types all or pics
	if (nFileTypesCode==FileCruncher::FILE_TYPE_ALL || nFileTypesCode==FileCruncher::FILE_TYPE_PIC)
	{
		//before creating subdir or even initiate creation of the subfolder, test if subfolder contains file types allowed:
		QDateTime datTimeInput;
		if (strAlbumIPhotoID.isEmpty())
			datTimeInput=FileCruncher::GetLastModifyDateFromFolder(strDirInPath,straAllowedFileTypes);
		else
			datTimeInput=FileCruncher_iPhoto::GetLastModifyDateFromFolder(strAlbumIPhotoID,straAllowedFileTypes);


		if (!datTimeInput.isValid()) //probably no files to process in folder-> mark folder as GO
		{
			SetCatalogStatus(strCatalogDirPath,STATUS_OK); 
			return;
		}



		//resolve output for thumbs (create dir if not exists)
		strDirOutPath=GetThumbPathBasedOnCatalogPath(strCatalogDirPath);
		if (strDirOutPath.isEmpty())
		{
			SetCatalogStatus(strCatalogDirPath,STATUS_COULD_NOT_BE_INDEXED); 
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Could not create thumb directory %1").arg(m_strOutThumbDirRoot+"/"+strCatalogDirPath));
			return;
		}



		//only for picture, so only set filter to pics:
		straAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC);


		if (strAlbumIPhotoID.isEmpty())
		{
			

			if(CreateThumbs(strDirInPath,strDirOutPath,straAllowedFileTypes))
				SetCatalogStatus(strCatalogDirPath,STATUS_OK);
			else
				SetCatalogStatus(strCatalogDirPath,STATUS_COULD_NOT_BE_INDEXED); //badly formated files
		}
		else
		{
			

			if(CreateThumbsiPhoto(strAlbumIPhotoID,strDirOutPath,straAllowedFileTypes))
				SetCatalogStatus(strCatalogDirPath,STATUS_OK);
			else
				SetCatalogStatus(strCatalogDirPath,STATUS_COULD_NOT_BE_INDEXED); //badly formated files
		}

		

		int nCount=0;
		if (strAlbumIPhotoID.isEmpty())
			nCount=FileCruncher::GetFileCountFromFolder(strDirOutPath,straAllowedFileTypes);
		else
			nCount=FileCruncher_iPhoto::GetFileCountFromFolder(strAlbumIPhotoID,straAllowedFileTypes);

		

		SetCatalogCnt(strDirOutPath,nCount/THUMB_COUNT);
	}
}


//process any input path and generate thumbs
bool ThumbManager::CreateThumbs(QString strDirInPath,QString strDirOutPath,QString straAllowedFileTypes)
{
	//check last modify time and skip if ok, check if path is already in process, then skip it.

	

	bool bExit=false;
	m_lckPaths.lockForWrite();
	if (m_lstCurrentPathProcessing.contains(strDirOutPath))
	{
		m_lckPaths.unlock();
		return true;
	}
	else
	{
		m_lstCurrentPathProcessing.append(strDirOutPath);
		m_lckPaths.unlock();
	}

	

	//if folder content match then skip refreshing: can speed up starting time
	if(FileCruncher::CompareFolderAndDeleteNonExisting(strDirOutPath,strDirInPath,m_lstThumbSufix,straAllowedFileTypes))
	{
		m_lckPaths.lockForWrite();
		m_lstCurrentPathProcessing.removeAll(strDirOutPath);
		m_lckPaths.unlock();
		return true;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Processing: %1").arg(strDirOutPath));
	

	//MB: new engine: speedy one:
	QString strSizes;
	int nSize=m_lstThumbSettings.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		strSizes+=m_lstThumbSettings.getDataRef(i,"Width").toString()+"x"+m_lstThumbSettings.getDataRef(i,"Height").toString();
		if (i<nSize-1)
			strSizes+=",";
	}

	bool bOk=true;
	QProcess gentool;
	QStringList lstArgs;
	lstArgs<<"-f";
	lstArgs<<"pic";
	lstArgs<<"-id";
	lstArgs<<strDirInPath;
	lstArgs<<"-od";
	lstArgs<<strDirOutPath;
	lstArgs<<"-ext";
	lstArgs<<straAllowedFileTypes;
	lstArgs<<"-thumb-sizes";
	lstArgs<<strSizes;
	lstArgs<<"-q";
	lstArgs<<QString::number(m_nJpegQuality);
	lstArgs<<"-a";
	lstArgs<<"-auto_potrait";

	gentool.start(QCoreApplication::applicationDirPath()+"/GenTool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to execute gentool");
		bOk=false;
	}
	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (errContent.size()!=0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Thumb creation process returned errors: "+QString(errContent.left(4095)));
		bOk=false;
	}


	

	m_lckPaths.lockForWrite();
	m_lstCurrentPathProcessing.removeAll(strDirOutPath);
	m_lckPaths.unlock();

	

	return bOk;
}

bool ThumbManager::CreateThumbsiPhoto(QString strAlbumIPhotoID,QString strDirOutPath,QString straAllowedFileTypes)
{

	//check last modify time and skip if ok, check if path is already in process, then skip it.
	bool bExit=false;
	m_lckPaths.lockForWrite();
	if (m_lstCurrentPathProcessing.contains(strDirOutPath))
	{
		m_lckPaths.unlock();
		return true;
	}
	else
	{
		m_lstCurrentPathProcessing.append(strDirOutPath);
		m_lckPaths.unlock();
	}

	//if folder content match then skip refreshing: can speed up starting time
	if(FileCruncher_iPhoto::CompareFolderAndDeleteNonExisting(strDirOutPath,strAlbumIPhotoID,m_lstThumbSufix,straAllowedFileTypes))
		return true;


	


	//MB: new engine: speedy one:
	QString strSizes;
	int nSize=m_lstThumbSettings.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		strSizes+=m_lstThumbSettings.getDataRef(i,"Width").toString()+"x"+m_lstThumbSettings.getDataRef(i,"Height").toString();
		if (i<nSize-1)
			strSizes+=",";
	}

	bool bOk=true;
	QProcess gentool;
	QStringList lstArgs;
	lstArgs<<"-f";
	lstArgs<<"pic";
	lstArgs<<"-iphoto_album_id";
	lstArgs<<strAlbumIPhotoID;
	lstArgs<<"-od";
	lstArgs<<strDirOutPath;
	lstArgs<<"-ext";
	lstArgs<<straAllowedFileTypes;
	lstArgs<<"-thumb-sizes";
	lstArgs<<strSizes;
	lstArgs<<"-q";
	lstArgs<<QString::number(m_nJpegQuality);
	lstArgs<<"-a";
	lstArgs<<"-auto_potrait";

	gentool.start(QCoreApplication::applicationDirPath()+"/GenTool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to execute gentool");
		bOk=false;
	}
	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (errContent.size()!=0)
	{
		//if (errContent.indexOf("QSqlDatabase")==0)
		//{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Thumb creation process returned errors: "+QString(errContent.left(4095)));
			bOk=false;
		//}
	}

	m_lckPaths.lockForWrite();
	m_lstCurrentPathProcessing.removeAll(strDirOutPath);
	m_lckPaths.unlock();

	return bOk;

}

//when GUI changes: reload
void ThumbManager::RenameCatalog(Status &pStatus, QString strOldName, QString strNewName)
{
	QDir dirRoot(m_strOutThumbDirRoot);
	if(!dirRoot.rename(strOldName,strNewName))
	{
		pStatus.setError(1,"Catalog rename failed. Thumb cache process is still running. Please try again in few minutes!");
	}

	//+ rename meta data:
	g_SettingsManager->RenameCatalogInShadowList(strOldName,strNewName);



	//+ all statuses clear
	QWriteLocker lock2(&m_lstCatalogCntLock);
	QStringList lstToDelete;

	HashIterator i(m_lstCatalogFileCnt);
	while (i.hasNext()) 
	{
		i.next();
		QString strAliasPath=i.key();
		QString strAlias=strAliasPath;
		bool bAppend=false;
		if (strAlias.indexOf("/")>=0)
		{
			strAlias=strAlias.split("/").at(0);
			bAppend=true;
		}
	
		if (strAlias==strOldName)
		{
			if (bAppend)
				strAliasPath=strNewName+strAliasPath.mid(strAliasPath.indexOf("/"));
			else
				strAliasPath=strNewName;

			m_lstCatalogFileCnt[strAliasPath]=i.value();
			lstToDelete<<i.key();
		}
	}

	for (int i=0;i<lstToDelete.size();i++)
		m_lstCatalogFileCnt.remove(lstToDelete.at(i));

	lstToDelete.clear();


	QWriteLocker lock(&m_lstCatalogStatusLock);

	HashIterator k(m_lstCatalogStatus);
	while (k.hasNext()) 
	{
		k.next();
		QString strAliasPath=k.key();
		QString strAlias=strAliasPath;
		bool bAppend=false;
		if (strAlias.indexOf("/")>=0)
		{
			strAlias=strAlias.split("/").at(0);
			bAppend=true;
		}
		if (strAlias==strOldName)
		{
			if (bAppend)
				strAliasPath=strNewName+strAliasPath.mid(strAliasPath.indexOf("/"));
			else
				strAliasPath=strNewName;

			m_lstCatalogStatus[strAliasPath]=i.value();
			lstToDelete<<i.key();
		}
	}

	for (int i=0;i<lstToDelete.size();i++)
		m_lstCatalogStatus.remove(lstToDelete.at(i));


	
}

void ThumbManager::RenameCatalogClient(Status &pStatus, QString strOldName, QString strNewName)
{
	//test if valid name
	if (!FileCruncher::IsNameValidAsDirectory(strNewName))
	{
		pStatus.setError(1,QString("Catalog can not be renamed. Catalog name %1 is not valid. Catalog name can't contain characters: *.\"/\\[]:;|=,").arg(strNewName));
		return;
	}


	//change in settings
	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);
	int nSize=lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (lstFolders.getDataRef(i,"ALIAS").toString()==strOldName)
			lstFolders.setData(i,"ALIAS",strNewName);
	}

	//user rights:
	DbRecordSet lstUserRights;
	g_SettingsManager->GetUserRights(lstUserRights);
	nSize=lstUserRights.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (lstUserRights.getDataRef(i,"ALIAS").toString()==strOldName)
			lstUserRights.setData(i,"ALIAS",strNewName);
	}

	RenameCatalog(pStatus,strOldName,strNewName);

	g_SettingsManager->SetFolders(lstFolders);
	g_SettingsManager->SetUserRights(lstUserRights);
	g_SettingsManager->SaveSettings();
	g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	g_SettingsManager->NotifyClientSettingsChangedNeedReload();
}



//create new catalog inside documen settings / catalogs/xxxyy 
//test if valid name
//set in settings->notify client
void ThumbManager::CreateCatalogClient(Status &pStatus, QString strName)
{
	QString strCatalogDir=g_AppServer->GetIniFile()->m_strCatalogDir;
	
	QString strCatalogPath = strCatalogDir;
	if (!QFile::exists(strCatalogPath))
	{
		pStatus.setError(1,QString("Can not create directory for new catalog! Invalid path set in server settings: %1").arg(strCatalogDir));
		/*

		QDir dir(g_strHomePath);
		if(!dir.mkdir("catalogs"))
		{
			pStatus.setError(1,"Can not create directory for new catalog!");
			return;
		}
		*/
	}

	if (!FileCruncher::IsNameValidAsDirectory(strName))
	{
		pStatus.setError(1,QString("Catalog can not be added: Catalog name %1 is not valid. Catalog name can't contain characters: *.\"/\\[]:;|=,").arg(strName));
		return;
	}

	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);

	if (lstFolders.find("ALIAS",strName,true)>=0)
	{
		pStatus.setError(1,QString("Catalog with name %1 already exists!").arg(strName));
		return;
	}

	QDir dir(strCatalogPath);
	if(!dir.mkdir(strName))
	{
		pStatus.setError(1,"Can not create directory for new catalog!");
		return;
	}

	lstFolders.addRow();
	int nRow=lstFolders.getRowCount()-1;
	lstFolders.setData(nRow,"ALIAS",strName);
	lstFolders.setData(nRow,"PATH",strCatalogPath+"/"+strName);
	lstFolders.setData(nRow,"FILETYPES_CODE",FileCruncher::FILE_TYPE_PIC);
	lstFolders.setData(nRow,"STATUS",STATUS_OK);
	lstFolders.setData(nRow,"PIC_CNT",0);
	lstFolders.setData(nRow,"IPHOTO_ID","");
	g_SettingsManager->SetFolders(lstFolders);

	g_SettingsManager->SaveSettings();
	g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	g_SettingsManager->NotifyClientSettingsChangedNeedReload();


}


void ThumbManager::RotateThumbByExifOrienatation(Status &pStatus, QString strFilePath)
{
	bool bOk=true;
	QProcess gentool;
	QStringList lstArgs;
	lstArgs<<"-f";
	lstArgs<<"pic";
	lstArgs<<"-i";
	lstArgs<<strFilePath;
	lstArgs<<"-rexif";
	lstArgs<<"-q";
	lstArgs<<QString::number(DEFAULT_JPG_QUALITY);
	lstArgs<<"-a";

	gentool.start(QCoreApplication::applicationDirPath()+"/GenTool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to execute gentool");
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}
	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (errContent.size()!=0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Thumb rotation failed: "+QString(errContent.left(4095)));
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}

}



//rotate both:
void ThumbManager::RotateThumb(Status &pStatus, QString strCatalogDirPath, QString strFileName, int nAngle)
{
	QString strDirInPath;
	QString strAlias;
	QString straAllowedFileTypes;
	Status err;
	int nFileTypesCode;
	QString strAlbumIPhotoID;
	g_SettingsManager->GetDirectoryPath(err,strCatalogDirPath,strAlias,strDirInPath,nFileTypesCode,straAllowedFileTypes,strAlbumIPhotoID);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Could not resolve catalog directory %1").arg(strCatalogDirPath));
		return;
	}


	QString strThumbSuffix_Small=GetThumbSuffix(THUMB_SMALL_WIDTH,THUMB_SMALL_HEIGHT)+".jpg";
	QString strThumbSuffix_Small_2=GetThumbSuffix(THUMB_SMALL_2_WIDTH,THUMB_SMALL_2_HEIGHT)+".jpg";
	QString strThumbSuffix_Large=GetThumbSuffix(THUMB_LARGE_WIDTH,THUMB_LARGE_HEIGHT)+".jpg";
	//QString strThumbSuffix_Large_2=GetThumbSuffix(THUMB_LARGE_WIDTH_PORTRAIT,THUMB_LARGE_HEIGHT_PORTRAIT)+".jpg";
	QString strThumbSuffix_Large_3=GetThumbSuffix(THUMB_LARGE_3_WIDTH,THUMB_LARGE_3_HEIGHT)+".jpg";
	//QString strThumbSuffix_Large_4=GetThumbSuffix(THUMB_LARGE_3_WIDTH_PORTRAIT,THUMB_LARGE_3_HEIGHT_PORTRAIT)+".jpg";
	QString strThumbSuffix_Large_5=GetThumbSuffix(THUMB_LARGE_4_WIDTH,THUMB_LARGE_4_HEIGHT)+".jpg";
	//QString strThumbSuffix_Large_6=GetThumbSuffix(THUMB_LARGE_4_WIDTH_PORTRAIT,THUMB_LARGE_4_HEIGHT_PORTRAIT)+".jpg";


	//->find iPhoto pic
	if (!strAlbumIPhotoID.isEmpty())
	{
		QString strFilePath="";
		//if iPhoto then get all files find ours: set it
		DbRecordSet lstSubs;
		DbRecordSet lstPaths;
		FileCruncher_iPhoto::GetFilesFromFolder(strAlbumIPhotoID,lstPaths,lstSubs);
		int nSize=lstPaths.getRowCount();
		for (int i=0;i<nSize;++i)
		{
			if (lstPaths.getDataRef(i,"FILENAME").toString().toLower()==strFileName.toLower())
			{
				strFilePath=lstPaths.getDataRef(i,"PATH").toString();
			}
		}
		if (strFilePath.isEmpty())
		{
			pStatus.setError(1,QString("Can not find picture %1 inside iPhoto album").arg(strFileName));
			return;
		}

		strFileName=strFilePath;
	}
	else
		strFileName = strDirInPath+"/"+strFileName;


	QFileInfo info(strFileName);

	QString strDirOutPath;
	//resolve output for thumbs (create dir if not exists)
	strDirOutPath=GetThumbPathBasedOnCatalogPath(strCatalogDirPath);
	if (strDirOutPath.isEmpty())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Could not resolve thumb directory %1").arg(m_strOutThumbDirRoot+"/"+strCatalogDirPath));
		pStatus.setError(1,QString("Could not resolve thumb directory %1").arg(m_strOutThumbDirRoot+"/"+strCatalogDirPath));
		return;
	}



	//TEST if full thumb exists, if not create one and rotate it:
	QString strOriginalCopyFile=strDirOutPath+"/"+info.completeBaseName()+"_full.jpg";
	if (!QFile::exists(strOriginalCopyFile))
	{
		if(FileCruncher::IsRAWPicture(info.completeSuffix()))
		{
			//get thumb from raw:
			GetThumbFromRaw(pStatus,strFileName,strOriginalCopyFile);
			if (!pStatus.IsOK())
				return;
		}
		else
		{
			GetJpegFromPicture(pStatus,strFileName,strOriginalCopyFile);
			if (!pStatus.IsOK())
				return;
		}
	}

	QString strFilePath_Small = strDirOutPath+"/"+info.completeBaseName()+strThumbSuffix_Small;
	QString strFilePath_Small_2 = strDirOutPath+"/"+info.completeBaseName()+strThumbSuffix_Small_2;
	QString strFilePath_Large = strDirOutPath+"/"+info.completeBaseName()+strThumbSuffix_Large;
	//QString strFilePath_Large_2 = strDirOutPath+"/"+info.completeBaseName()+strThumbSuffix_Large_2;
	QString strFilePath_Large_3 = strDirOutPath+"/"+info.completeBaseName()+strThumbSuffix_Large_3;
	//QString strFilePath_Large_4 = strDirOutPath+"/"+info.completeBaseName()+strThumbSuffix_Large_4;
	QString strFilePath_Large_5 = strDirOutPath+"/"+info.completeBaseName()+strThumbSuffix_Large_5;
	//QString strFilePath_Large_6 = strDirOutPath+"/"+info.completeBaseName()+strThumbSuffix_Large_6;

	RotateGenToolExe(pStatus,strOriginalCopyFile,strFilePath_Small,nAngle,THUMB_SMALL_WIDTH,THUMB_SMALL_HEIGHT,m_nJpegQuality);
	if (!pStatus.IsOK()) return;
	RotateGenToolExe(pStatus,strOriginalCopyFile,strFilePath_Small_2,nAngle,THUMB_SMALL_2_WIDTH,THUMB_SMALL_2_HEIGHT,m_nJpegQuality);
	if (!pStatus.IsOK()) return;
	RotateGenToolExe(pStatus,strOriginalCopyFile,strFilePath_Large,nAngle,THUMB_LARGE_WIDTH,THUMB_LARGE_HEIGHT,m_nJpegQuality);
	if (!pStatus.IsOK()) return;
	//RotateGenToolExe(pStatus,strOriginalCopyFile,strFilePath_Large_2,nAngle,THUMB_LARGE_WIDTH_PORTRAIT,THUMB_LARGE_HEIGHT_PORTRAIT);
	//if (!pStatus.IsOK()) return;
	RotateGenToolExe(pStatus,strOriginalCopyFile,strFilePath_Large_3,nAngle,THUMB_LARGE_3_WIDTH,THUMB_LARGE_3_HEIGHT,m_nJpegQuality);
	if (!pStatus.IsOK()) return;
	//RotateGenToolExe(pStatus,strOriginalCopyFile,strFilePath_Large_4,nAngle,THUMB_LARGE_3_WIDTH_PORTRAIT,THUMB_LARGE_3_HEIGHT_PORTRAIT);
	//if (!pStatus.IsOK()) return;
	RotateGenToolExe(pStatus,strOriginalCopyFile,strFilePath_Large_5,nAngle,THUMB_LARGE_4_WIDTH,THUMB_LARGE_4_HEIGHT,m_nJpegQuality);
	if (!pStatus.IsOK()) return;
	//RotateGenToolExe(pStatus,strOriginalCopyFile,strFilePath_Large_6,nAngle,THUMB_LARGE_4_WIDTH_PORTRAIT,THUMB_LARGE_4_HEIGHT_PORTRAIT);
	//if (!pStatus.IsOK()) return;
	RotateGenToolExe(pStatus,strOriginalCopyFile,strOriginalCopyFile,nAngle,-1,-1,DEFAULT_JPG_QUALITY);
	if (!pStatus.IsOK()) return;

}

void ThumbManager::RotateGenToolExe(Status &pStatus, QString strFile, QString strFileOut, int nAngle, int nWidth, int nHeight, int nJpegQuality)
{
	QProcess gentool;
	QStringList lstArgs;

	lstArgs.clear();
	lstArgs<<"-f";
	lstArgs<<"pic";
	lstArgs<<"-i";
	lstArgs<<strFile;
	lstArgs<<"-o";
	lstArgs<<strFileOut;
	lstArgs<<"-r";
	lstArgs<<QString::number(nAngle);
	lstArgs<<"-w";
	lstArgs<<QString::number(nWidth);
	lstArgs<<"-h";
	lstArgs<<QString::number(nHeight);
	lstArgs<<"-a";
	lstArgs<<"-auto_potrait";
	lstArgs<<"-q";
	lstArgs<<QString::number(nJpegQuality);


	gentool.start(QCoreApplication::applicationDirPath()+"/GenTool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to execute gentool");
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}
	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (errContent.size()!=0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Thumb rotation failed: "+QString(errContent.left(4095)));
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}
}

void ThumbManager::GetThumbFromRaw(Status &pStatus, QString strFileIn, QString strFileOut)
{
	QProcess gentool;
	QStringList lstArgs;

	lstArgs.clear();
	lstArgs<<"-f";
	lstArgs<<"pic";
	lstArgs<<"-i";
	lstArgs<<strFileIn;
	lstArgs<<"-o";
	lstArgs<<strFileOut;
	lstArgs<<"-raw_thumb";
	lstArgs<<"-a";
	lstArgs<<"-w";
	lstArgs<<QString::number(MAX_THUMB_PIC_WIDTH_LARGE);
	lstArgs<<"-h";
	lstArgs<<QString::number(MAX_THUMB_PIC_HEIGHT_LARGE);
	lstArgs<<"-q";
	lstArgs<<QString::number(DEFAULT_JPG_QUALITY); //for rotate keep max quality + scale down to max res

	gentool.start(QCoreApplication::applicationDirPath()+"/GenTool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to execute gentool");
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}
	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (errContent.size()!=0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"GetThumbFromRaw failed: "+QString(errContent.left(4095)));
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}

}

void ThumbManager::GetJpegFromPicture(Status &pStatus, QString strFileIn, QString strFileOut)
{
	QProcess gentool;
	QStringList lstArgs;

	lstArgs.clear();
	lstArgs<<"-f";
	lstArgs<<"pic";
	lstArgs<<"-i";
	lstArgs<<strFileIn;
	lstArgs<<"-o";
	lstArgs<<strFileOut;
	lstArgs<<"-to_jpg";
	lstArgs<<"-w";
	lstArgs<<QString::number(MAX_THUMB_PIC_WIDTH_LARGE);
	lstArgs<<"-h";
	lstArgs<<QString::number(MAX_THUMB_PIC_HEIGHT_LARGE);
	lstArgs<<"-q";
	lstArgs<<QString::number(DEFAULT_JPG_QUALITY); //for rotate keep max quality + scale down to max res

	gentool.start(QCoreApplication::applicationDirPath()+"/GenTool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to execute gentool");
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}
	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (errContent.size()!=0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Thumb rotation failed: "+QString(errContent.left(4095)));
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}

}

void ThumbManager::CreateOneThumb(Status &pStatus, QString strPathIn, QString strPathOut, int nPicWidth,int nPicHeight)
{

	bool bOk=true;
	QProcess gentool;
	QStringList lstArgs;
	lstArgs<<"-f";
	lstArgs<<"pic";
	lstArgs<<"-i";
	lstArgs<<strPathIn;
	lstArgs<<"-o";
	lstArgs<<strPathOut;
	lstArgs<<"-w";
	lstArgs<<QString::number(nPicWidth);
	lstArgs<<"-h";
	lstArgs<<QString::number(nPicHeight);
	lstArgs<<"-q";
	lstArgs<<QString::number(m_nJpegQuality);
	lstArgs<<"-a";


	gentool.start(QCoreApplication::applicationDirPath()+"/GenTool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to execute gentool");
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}
	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (errContent.size()!=0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Thumb rotation failed: "+QString(errContent.left(4095)));
		pStatus.setError(1,"Failed to execute gentool");
		return;
	}

}


//Must have <alias, status, pic_cnt,cnt, path,photo_id>
void ThumbManager::RecalculateCounts(DbRecordSet &Ret_Folders)
{
	QHash<QString,int> hshStatus;
	GetCatalogStatusList(hshStatus); //if not in list then not indexed if in then indexing or ok

	QHash<QString,int> hshCnt;
	GetCatalogCntList(hshCnt);

	int nSize=Ret_Folders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strAlias = Ret_Folders.getDataRef(i,"ALIAS").toString();

		
		int nStatus =  -1;
		int nCntOfOne=hshCnt.value(strAlias,0)-g_SettingsManager->GetHidePictureCnt(strAlias);
		Ret_Folders.setData(i,"CNT",nCntOfOne); //set CNT as original
		Ret_Folders.setData(i,"STATUS",hshStatus.value(strAlias,STATUS_NOT_INDEXED));
	
		int nTotalCnt = nCntOfOne;

		bool bFound=false;

		//search all that start with this:
		QHashIterator<QString,int> k(hshStatus); 
		QString strAlias2=strAlias+"/";
		while (k.hasNext()) 
		{
			k.next();

			QString strAliasSearched = k.key();

			if (strAliasSearched.indexOf(strAlias2)==0) //must be at start of subfolder
			{
				bFound=true;

				int nCnt = hshCnt.value(strAliasSearched,0);
				//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Thumbs: %1 %2").arg(strAliasSearched).arg(nCnt));
				nCnt = nCnt-g_SettingsManager->GetHidePictureCnt(strAliasSearched);
				nTotalCnt += nCnt;
				/*
				if (nStatus==-1)
					nStatus= k.value(); //set status
				else if (k.value()!=STATUS_OK && nStatus ==STATUS_OK)
					nStatus= k.value(); //status of one that is not ok...
				*/
			}
		}

		/*
		//if big dir is processing->means others will wait to process then recalc manually:
		if (!bFound)
		{
			QString straAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC);
			QString strAlias=			Ret_Folders.getDataRef(i,"ALIAS").toString();
			QString strAlbumIPhotoID=	Ret_Folders.getDataRef(i,"IPHOTO_ID").toString();
			QString strPath =			Ret_Folders.getDataRef(i,"PATH").toString();
			
			if (strAlbumIPhotoID.isEmpty())
				nTotalCnt=FileCruncher::GetFileCountFromFolder(strPath,straAllowedFileTypes);
			else
				nTotalCnt=FileCruncher_iPhoto::GetFileCountFromFolder(strAlbumIPhotoID,straAllowedFileTypes);

			SetCatalogCnt(strAlias,nTotalCnt);

			nTotalCnt = nTotalCnt-g_SettingsManager->GetHidePictureCnt(strAlias); //total cnt is main

			//get all subfolders:
			QList<QString> lstSubFolders;
			FileCruncher::GetAllSubFolders(strPath,lstSubFolders);
			int nSize=lstSubFolders.size();
			for (int i=0;i<nSize;i++)
			{
				int nCount=0;
				QString strAliasPath=strPath+"/"+lstSubFolders.at(i);
				QString strAliasSub=strAlias+"/"+lstSubFolders.at(i);
				if (strAlbumIPhotoID.isEmpty())
					nCount=FileCruncher::GetFileCountFromFolder(strAliasPath,straAllowedFileTypes);
				else
					nCount=FileCruncher_iPhoto::GetFileCountFromFolder(strAlbumIPhotoID,straAllowedFileTypes);
				SetCatalogCnt(strAliasSub,nCount);

				nTotalCnt += nCount-g_SettingsManager->GetHidePictureCnt(strAliasSub); //total cnt is main
			}
		}

		//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Main Catalog Thumbs: %1 %2").arg(strAlias).arg(nTotalCnt));

		if (nStatus==-1)
			nStatus=STATUS_NOT_INDEXED;
		*/
		//Ret_Folders.setData(i,"STATUS",nStatus);
		Ret_Folders.setData(i,"PIC_CNT",nTotalCnt);
	}

}


void ThumbManager::GetCatalogStatusList(QHash<QString,int> &hshStatus)
{
	QReadLocker lock(&m_lstCatalogStatusLock);
	hshStatus=m_lstCatalogStatus;
}

void ThumbManager::SetCatalogStatus(QString strCatalogName, int nStatus)
{
	QWriteLocker lock(&m_lstCatalogStatusLock);
	m_lstCatalogStatus[strCatalogName]=nStatus;
}
int	ThumbManager::GetCatalogStatus(QString strCatalogName)
{
	QReadLocker lock(&m_lstCatalogStatusLock);
	return m_lstCatalogStatus.value(strCatalogName,STATUS_OK);
}


void ThumbManager::GetCatalogCntList(QHash<QString,int> &hshStatus)
{
	QReadLocker lock(&m_lstCatalogCntLock);
	hshStatus=m_lstCatalogFileCnt;
}

void ThumbManager::SetCatalogCnt(QString strCatalogName, int nCnt)
{
	QWriteLocker lock(&m_lstCatalogCntLock);
	m_lstCatalogFileCnt[strCatalogName]=nCnt;
}


//clear from list, notify
void ThumbManager::UnpublishCatalogClient(Status &pStatus, QString strAliasDirectory)
{
	//change in settings
	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);

	int nRow=lstFolders.find("ALIAS",strAliasDirectory,true);
	if (nRow<0)
	{
		pStatus.setError(1,"Catalog not found!");
		return;
	}
	else
	{
		//special: if folder is newly created then delete it:
		QString strPath=QDir::cleanPath(lstFolders.getDataRef(nRow,"PATH").toString().toLower());
		FileCruncher::ReplaceNetworkPathWithUNC(strPath);
		QString strPathForNewCatalogs=QDir::cleanPath(g_AppServer->GetIniFile()->m_strCatalogDir.toLower());
		if (strPath.indexOf(strPathForNewCatalogs)==0)
		{
			FileCruncher::RemoveAllFromDirectory(strPath,true,true);
		}
		lstFolders.deleteRow(nRow);
	}

	//user rights:
	DbRecordSet lstUserRights;
	g_SettingsManager->GetUserRights(lstUserRights);
	lstUserRights.find("ALIAS",strAliasDirectory);
	lstUserRights.deleteSelectedRows();

	g_SettingsManager->SetFolders(lstFolders);
	g_SettingsManager->SetUserRights(lstUserRights);
	g_SettingsManager->SaveSettings();
	g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	g_SettingsManager->NotifyClientSettingsChangedNeedReload();

	QWriteLocker lock(&m_lstCatalogCntLock);
	QWriteLocker lock2(&m_lstCatalogStatusLock);
	m_lstCatalogFileCnt.remove(strAliasDirectory);
	m_lstCatalogStatus.remove(strAliasDirectory);
}


