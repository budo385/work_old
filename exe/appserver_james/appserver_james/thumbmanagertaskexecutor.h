#ifndef THUMBMANAGERTASKEXECUTOR_H
#define THUMBMANAGERTASKEXECUTOR_H

#include "common/common/backgroundtaskexecutor.h"

class ThumbManagerTaskExecutor : public BackgroundTaskExecutor
{
	Q_OBJECT

public:
	ThumbManagerTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent=NULL, int nInterval=0);

	void Stop();

public slots:
	void OnSignalProcessAll();
	
};

#endif // THUMBMANAGERTASKEXECUTOR_H
