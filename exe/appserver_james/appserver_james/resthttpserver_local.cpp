#include "resthttpserver_local.h"
#include <QHostAddress>
#include <QNetworkInterface>
#include "common/common/logger.h"

#include "usersessionmanager.h"
extern UserSessionManager *g_UserSessionManager;

RestHTTPServer_Local::RestHTTPServer_Local(RpcSkeleton *pServerSkeletonSet,QString strWWWRootDir)
:RestHTTPServer(pServerSkeletonSet,strWWWRootDir)
{

	QList<QHostAddress> lstAddress=	QNetworkInterface::allAddresses();
	int nSize=lstAddress.size();
	for(int i=0;i<nSize;++i)
	{
		m_lstAllowedIPAddresses.append(lstAddress.at(i).toString());
	}

}
//Challenge: WWW-Authenticate: SokratesAuth nonce="84e0a095cfd25153b2e4014ea87a0980"
//Response: Authorization: SokratesAuth user="MB" token="j/qGVP4C0T7UixSpKJpTdw" nonce="84e0a095cfd25153b2e4014ea87a0980" progver="" clientid="" platform=""
bool RestHTTPServer_Local::AuthenticateUser(Status &err,HTTPContext &ctx,HTTPRequest &request,HTTPResponse &response, int &nClientTimeZoneOffsetMinutes)
{
	nClientTimeZoneOffsetMinutes=0;
	err.setError(0);

	//qDebug()<<"-----------------HTTP rest authenticate request-----------------------";
	//qDebug()<<request.toString();
	//QString strData(request.GetBody()->constData());
	//qDebug()<<strData;
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,request.toString());
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,strData);
	//qDebug()<<"----------------------------------------------------------";

	if (request.path().right(6).toLower()=="/login" || request.path().right(12).toLower()=="/server_data" || request.path().right(16).toLower()=="/catalog/picture" || request.path().right(14).toLower()=="catalog/rename" || request.path().right(15).toLower()=="catalogs/status")
	{
		return true;
	}

	//test session cookie:
	QString strSession;
	bool bAuthenticated=false;
	if(g_UserSessionManager->AuthenticateUser(request.value("Cookie"),strSession))
		bAuthenticated=true;
	else if(g_UserSessionManager->AuthenticateUser(request.value("Auth"),strSession))
		bAuthenticated=true;

	if (bAuthenticated)
	{
		g_UserSessionManager->ValidateWebUserRequest(err,strSession,ctx);
		if (!err.IsOK())
		{
			SetResponseHeader(request,response,401,"Unauthorized");
			//response.SetBody("Unauthorized");
			return false;
		}
		return true;
	}

	//test allowed IP's: localhost+tarantula: reload command only possible from local client:
	if (m_lstAllowedIPAddresses.contains(ctx.m_strPeerIP) && request.path().right(7).toLower()=="/reload")
	{
		return true;
	}

	SetResponseHeader(request,response,401,"Unauthorized");
	//response.SetBody("Unauthorized");
	return false;
}
void RestHTTPServer_Local::LockThreadActive()
{
	g_UserSessionManager->LockThreadActive();
	
}
void RestHTTPServer_Local::UnLockThreadActive()
{
	g_UserSessionManager->UnLockThreadActive();
}

