#include "thumbmanagertaskexecutor.h"
#include "thumbmanager.h"
#include "common/common/logger.h"
extern Logger							g_Logger;

ThumbManagerTaskExecutor::ThumbManagerTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent, int nInterval)
:BackgroundTaskExecutor(pTask,parent,nInterval)
{
	ThumbManager *p=dynamic_cast<ThumbManager*>(pTask);
	connect(p,SIGNAL(SignalProcessAll()),this,SLOT(OnSignalProcessAll()));
}




void ThumbManagerTaskExecutor::OnSignalProcessAll()
{
	StartNow();
}

void ThumbManagerTaskExecutor::Stop()
{
	emit StopTask();
	if(isRunning())
	{
		quit();
		bool bOK=wait(3000);		//wait 3sec
		if (!bOK)
		{
			terminate();
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"thumb thread terminated");
		}
	}
}