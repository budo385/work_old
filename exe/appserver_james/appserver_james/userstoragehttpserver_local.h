#ifndef USERSTORAGEHTTPSERVER_LOCAL_H
#define USERSTORAGEHTTPSERVER_LOCAL_H

#include "trans/trans/userstoragehttpserver.h"

class UserStorageHTTPServer_Local: public UserStorageHTTPServer
{

public:
	void	SetThumbCacheDir(QString strThumbDirRoot){m_strThumbDirRoot=strThumbDirRoot;};

protected:
	bool	AuthenticateUser(QString strCookieValue,QString &strSessionID);
	void	CheckPermission(Status &pStatus,QString strSession,int nMethodType,QString strAliasDir,QString strDirPath,QString strFileName,qint64 nFileSizeForUpload);
	QString InitializeUserStorageDirectory(Status &pStatus,QString strUserSubDir);
	void	GetFilePath(Status &status,int nMethodType,QString strSession,const HTTPRequest &request, const QUrl &url,QString &strAliasDir, QString &strDirPath,QString &strFileName, qint64 &nFileSizeForUpload);


private:
	QString m_strThumbDirRoot;
};

#endif // USERSTORAGEHTTPSERVER_LOCAL_H
