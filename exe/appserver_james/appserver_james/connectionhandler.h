#ifndef CONNECTIONHANDLER_H
#define CONNECTIONHANDLER_H

#include "common/common/status.h"
#include "common/common/backgroundtaskinterface.h"
#include "serverinifile.h"
#include <QMutex>
#include <QStringList>
#include <QReadWriteLock>
#include "NATTRaversal/NATTraversal/nattraversal.h"

class ConnectionHandler : public QObject , public BackgroundTaskInterface 
{
	Q_OBJECT

public:
	ConnectionHandler(ServerIniFile *INIFile, QString strIniFilePath, QObject *parent=0);
	~ConnectionHandler();

	void	ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");

	//tarantula:
	void	Register(Status &err);
	void	UnRegister(Status &err);
	void	TestConnect(Status &err);
	QString GetTarantulaIP(int nTry);
	void	StartPortForward(Status &err);
	void	StopPortForward(Status &err);


private:
	ServerIniFile	*	m_INIFile;
	QString				m_strIniFilePath;
	bool				m_bRegisterDone;
	bool				m_bRegisterNotFirstTime; //feature by MB: only test for tarantula first time, then if tarantula failes: do not stop server, just keep on...
	int					m_nPortForwardMethod;
	NATTraversal		m_objNAT;
	bool				m_bTarantulaWasAliveLastTimeChecked;
	int					m_nCurrentPort;

};

#endif // CONNECTIONHANDLER_H
