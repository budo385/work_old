Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');

var win = Ti.UI.currentWindow; 
win.titleControl = null;
win.titleImage = './images/everPics_TitleBar.png';
win.backButtonTitle =L("back");
win.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var previousWindows = win.previousWindows;
previousWindows.push(win);
win.fullscreen = true;

var sendButton = Titanium.UI.createButton({
	title:L("send")
});
win.rightNavButton=sendButton;

var strSalut = L("invitation_salutation")+' ' + win.firstName;
var strSignature = Titanium.App.Properties.getString('EMAIL_SIGNATURE');
var strEmailSubject = L("invitation_subject")+" \""+win.catalogName+"\"";
var strEmailBody = strSalut + "\n\n" + L("invitation_body_0")+" \"" +
		win.catalogName +"\" "+L("invitation_body_1")+ "\n" + strSignature;

//setup main view
var viewLayout = Titanium.UI.createScrollView({
		showVerticalScrollIndicator:true,
		showHorizontalScrollIndicator:false,
		contentWidth:'auto',
		contentHeight:'auto',
		top:0,
		bottom:0,
		left:0,
		right:0
});

var subjectLabel = Titanium.UI.createLabel({
		height:30,
		top:0,
		left:5,
		textAlign:'left',
		color:'white',
		text:L("subject")+':'
});

var subjectTextField = Titanium.UI.createTextField({
		hintText:L("subject"),
		value: strEmailSubject,
		height:30,
		top:35,
		left:5,
		right:5,
		keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
		borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED
});

var messageLabel = Titanium.UI.createLabel({
		left:5,
		height:30,
		top:70,
		textAlign:'left',
		color:'white',
		text:L("message")+':'
});

var messageTextField = Titanium.UI.createTextArea({
		value:strEmailBody,
		textAlign:'left',
		font:{fontSize: 17},
		suppressReturn: false,
		editable: true,
		top:105,
		height:305,
		left:5,
		right:5,
		borderRadius:5,
		appearance:Titanium.UI.KEYBOARD_APPEARANCE_DEFAULT,
		keyboardType:Titanium.UI.KEYBOARD_ASCII,
		returnKeyType:Titanium.UI.RETURNKEY_DEFAULT
});

function pfCallBackSucess(xml)
{
		var alertDialog = Ti.UI.createAlertDialog({
				title:L("invitation_email_success_dialog_title"),
				message:L("invitation_email_success_dialog_message")
		});
		alertDialog.show();
    
		//go back two steps
		win.close();
		Ti.App.fireEvent('close_invitation');
}

function pfCallBackError(xml)
{
		var alertDialogBOError = Ti.UI.createAlertDialog({
		    title:L("invitation_email_error_dialog_title"),
		    message: strLastBO_ErrorText
		});
		alertDialogBOError.show();
}

function sendMail()
{
		//set invitation through the server
		var strPost=
		'<?xml version="1.0" encoding="UTF-8"?>\n\n' +
		'<REQUEST><PARAMETERS><nMessageType>1</nMessageType>' + 
		'<strAliasDirectory>' + win.catalogName + '</strAliasDirectory>' +
		'<strFirstName>' + win.firstName  + '</strFirstName>' +
		'<strLastName>' + win.strLastName  + '</strLastName>' + 
		'<strEmail>' + win.selectedEmail + '</strEmail>' + 
		'<strPhone>' + win.selectedPhone + '</strPhone>' +
		'<nIsHtml>' + win.nIsHtml + '</nIsHtml>' +
		'<strSubject>' + subjectTextField.value + '</strSubject>' +
		'<strBody>' + messageTextField.value + '</strBody>' +
		'</PARAMETERS></REQUEST>';
		
		var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
		BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/invite", pfCallBackSucess, pfCallBackError, strPost); 
}

sendButton.addEventListener('click', function()
{
		sendMail();
});

//initial layout
viewLayout.add(subjectLabel);
viewLayout.add(subjectTextField);
viewLayout.add(messageLabel);
viewLayout.add(messageTextField);
//messageScrollField.add(messageTextField);
//viewLayout.add(messageScrollField);
win.add(viewLayout);
win.show();

