Titanium.include('./load_indicator.js','./bo_call_new.js');

var win = Titanium.UI.currentWindow;

function pfRenamePictureSucess(xml)
{
	hideLoadIndicator(win);
	Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
	if(!pictures.isFolder)
	{
		var alertRenameDialog = Titanium.UI.createAlertDialog({
			title:L("rename_catalog_alert_title"),
			message:L("rename_catalog_alert_message"),
			buttonNames:[L("ok")],
			cancel:0
		});
		alertRenameDialog.show();
		alertRenameDialog.addEventListener('click',function(e)
		{
			win.fireEvent('focus');
		});
	}
	else
	{
		win.fireEvent('focus');
	}
}

function pfRenamePictureError(xml)
{
	hideLoadIndicator(win);
	var alertRenameDialog = Titanium.UI.createAlertDialog({
		title:L("rename_catalog_alert_title"),
		message:L("rename_catalog_error_alert_message"),
		buttonNames:[L("ok")],
		cancel:0
	});
	alertRenameDialog.show();
}

function renameCatalog(oldName,newName)
{
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strCatalog>'+oldName+'</strCatalog>'+
    '<strCatalogNew>'+newName+'</strCatalogNew></PARAMETERS></REQUEST>';
    
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(win);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/rename_client",pfRenamePictureSucess,pfRenamePictureError,strPost); 
}