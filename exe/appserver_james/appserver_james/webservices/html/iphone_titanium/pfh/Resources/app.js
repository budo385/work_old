Titanium.include('./mem_pool.js');

var mainWin = Titanium.UI.createWindow({  
    backgroundColor:'#0F3752',
//    barColor:'#111',
    navBarHidden: false,
    tabBarHidden: true
});

var timeoutID=0;
var introDuration=15000;
if(Titanium.Platform.osname=='ipad')
{
    introDuration=15000;
}

var path = Titanium.Filesystem.resourcesDirectory;
mainWin.backgroundImage = './images/PooltableBlue256.png';
mainWin.barColor = '#0F3752';

var webview = Titanium.UI.createWebView();
webview.backgroundImage = './images/PooltableBlue256.png';
webview.backgroundColor='#0F3752';

if(Titanium.Platform.osname=='ipad')
{
    webview.url='./html/animation_ipad/index.html';
}
else
{
    webview.url='./html/animation_iphone/index.html';
}
mainWin.fullscreen=false;

// initialize to all modes
mainWin.orientationModes = [
    Titanium.UI.PORTRAIT
];

var skipIntroButton = Titanium.UI.createButton({
	title:L("skip_intro")
});

mainWin.rightNavButton=skipIntroButton;

//App session storage.
if(Ti.App.Properties.getString('CATALOG_PAGE_SIZE')==null)
{
    Ti.App.Properties.setString('CATALOG_PAGE_SIZE',100);
}
if(Ti.App.Properties.getString('playslidefor')==null)
{
    Ti.App.Properties.setString('playslidefor',3);
}
if(Titanium.App.Properties.getBool('RELOAD_CATALOGS')==null)
{
    Titanium.App.Properties.setBool('RELOAD_CATALOGS', false);
}
if(Ti.App.Properties.getString('skipIntro')==null)
{
    Ti.App.Properties.setString('skipIntro','false');
}
if(Ti.App.Properties.getString('closePictureGrid')==null)
{
    Ti.App.Properties.setString('closePictureGrid','false');
}
Ti.App.Properties.setInt('win_orientation',Titanium.UI.PORTRAIT);

if(Ti.App.Properties.getString('PROGRAM_CODE')==null)
{
    Ti.App.Properties.setString('PROGRAM_CODE','11000000');
}

// create tab group
var tabGroup = Titanium.UI.createTabGroup();

var tab1 = Titanium.UI.createTab({  
    title:'',
    window:mainWin
});

function closeFullScreen()
{
    var mem_pool = new MemoryPool();
    mem_pool.clean(webview);

    var win = null;
    if (Ti.Platform.name == "android")
    {
	win = Titanium.UI.createWindow({
	url:'./connect_server.js',
	navBarHidden:false,
	tabBarHidden:true
	});	
    }
    else
    {
	win = Titanium.UI.createWindow({
	url:'./connect_server.js',
	navBarHidden:false,
	tabBarHidden:true
	});
    }
	
    //Titanium.UI.currentTab.open(win,{animated:true});
    tab1.open(win,{animated:false});
}

skipIntroButton.addEventListener('click', function()
{
    if(timeoutID!=0)
    {
        clearTimeout(timeoutID);
        timeoutID=0;
    }

    closeFullScreen();
});

mainWin.addEventListener('open', function(e)
{
   mainWin.add(webview);
   if(Ti.App.Properties.getBool('skipIntro'))
   {
	closeFullScreen();
   }
   else
   {
	timeoutID=setTimeout(closeFullScreen, introDuration);
   }
});

Ti.Gesture.addEventListener('orientationchange',function(e)
{
    Ti.App.Properties.setInt('win_orientation',e.orientation);
});

//  add tab
tabGroup.addTab(tab1);  
// open tab group
tabGroup.open();
//tabGroup.open({transition:Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});
