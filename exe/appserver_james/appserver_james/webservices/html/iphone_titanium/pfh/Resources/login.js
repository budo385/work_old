Titanium.include('./login_table.js','./load_indicator.js','./login_calls.js','./database_calls.js','./background_orientation.js');

var loginWin = Titanium.UI.currentWindow;
var path = Titanium.Filesystem.resourcesDirectory;

var previousWindows=[];
//previousWindows.push(loginWin);

loginWin.titleControl = null;
loginWin.titleImage = './images/everPics_TitleBar.png';
loginWin.fullscreen=false;

loginWin.orientationModes = [
    Titanium.UI.PORTRAIT
];

loginWin.add(scrollview);

var path = Titanium.Filesystem.resourcesDirectory;
//Navigation bar.
var infoButton = Titanium.UI.createButton({
	title:L("info")
});

infoButton.addEventListener('click', function()
{
	var w = Titanium.UI.createWindow({
		url:'./info.js',
		previousWindows:previousWindows
	});
	Titanium.UI.currentTab.open(w);
});

loginWin.backButtonTitle=L("id_button");
loginWin.rightNavButton=infoButton;

function Login_CallBackSucess(xml)
{
    hideLoadIndicator(loginWin);
    Ti.App.Properties.setString('Username',usernameTextField.value);
    if(Ti.App.Properties.getBool('rememberPassword'))
    {
        Ti.App.Properties.setString('Password',passwordTextField.value);
    }
    else
    {
        Ti.App.Properties.removeProperty('Password');
    }
    
    var w = Titanium.UI.createWindow({
	url:'./catalogs.js',
	serverName:loginWin.serverName,
	previousWindows:previousWindows
    });
    Titanium.UI.currentTab.open(w);
}

function Login_CallBackError(xml)
{
//Ti.API.info(xml);
    hideLoadIndicator(loginWin);
    var alertDialogLogError = Ti.UI.createAlertDialog({
        title:L("login_error_alert_title"),
        //message: 'Unable to Connect to Server!'
        message: strLastBO_ErrorText
    });
    alertDialogLogError.show();
}

loginButton.addEventListener('click', function()
{
    var username = usernameTextField.value;
    var password = passwordTextField.value;
    
    //username = 'admin';
    //password = 'admin';
    
    showLoadIndicator(loginWin);
    BO_Login(username,password,Login_CallBackSucess,Login_CallBackError);
});

loginWin.addEventListener('open', function()
{
//    Ti.API.info('Login win open function');
});

/*Ti.App.addEventListener('loginToCatalogs', function(e)
{
    setTimeout(function(){loginButton.fireEvent('click');},10);
});*/

//Window open event.
loginWin.addEventListener('focus', function()
{
    if(loginWin.serverPicture64.length == 0)
    {
        serverImage.image = './images/user75.png';
    }
    else
    {
        var serverPicture = Titanium.Utils.base64decode(loginWin.serverPicture64);
        serverImage.image = serverPicture;
    }
    serverNameLabel.text = loginWin.serverName;

    
    if(loginWin.bHasInvite)
    {
        loginWin.bHasInvite=false;
        usernameTextField.value=loginWin.inviteUsername;
        passwordTextField.value=loginWin.invitePassword;
        
        loginButton.fireEvent('click');
    }
    else
    {
        if(Ti.App.Properties.hasProperty('Username'))
        {
            usernameTextField.value = Ti.App.Properties.getString('Username');
        }
	else
	{
            usernameTextField.value='admin';
	}

        if(Ti.App.Properties.getBool('rememberPassword'))
        {
            if(Ti.App.Properties.hasProperty('Password'))
            {
                passwordTextField.value = Ti.App.Properties.getString('Password');
            }
        }
        else
        {
            Ti.App.Properties.removeProperty('Password');
        }
    }
});
