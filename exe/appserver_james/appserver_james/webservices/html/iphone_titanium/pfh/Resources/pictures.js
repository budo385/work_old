Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js','./rename_catalog.js',
		 './create_catalog.js','./unpublish_catalog.js');

var pictures = Titanium.UI.currentWindow;
pictures.titleControl = null;
pictures.titleImage = './images/everPics_TitleBar.png';

var previousWindows = pictures.previousWindows;
previousWindows.push(pictures);

var catalogPageSize=Ti.App.Properties.getInt('CATALOG_PAGE_SIZE');
var isGridView=1;
var catalogTotalCount = 0;
var files = null;
var folders = null;
var thumbSize=75;
var folderSize=60;
var tableViewRowHeight=95;
var backgroundImagePic='./images/Square75.png';
var imageOffset = 4;
var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
var tableData = []; 
var rowToPictureName = []; 
var rowToPictureThumbLink = []; 
var foldersCount=0;

if(Titanium.Platform.osname=='ipad')
{
    imageOffset=3;
    thumbSize=150;
    backgroundImagePic='./images/Square75.png';
    folderSize=130;
    tableViewRowHeight=170;
}

var tableViewLoaded=0;
var scrollBarLoaded=0;

pictures.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

var editOptionDialog = Titanium.UI.createOptionDialog({
	options:[L("rename"),L("new"),L("unpublish"),L("undelete_pictures"),L("cancel")],
	cancel:4,
	title:L("modify_catalog_dialog_title")
});

if(Titanium.Platform.osname=='iphone')
{
	editOptionDialog.cancel=4;
}

function catalogDialog(dialogType)
{
    var w = Ti.UI.createWindow({
	    url:'./modal_dialog.js'
    });

    if(dialogType==0)
    {
	w.dialogType=0;
	w.catalogName=pictures.originalCatalogName;
    }
    else if(dialogType==1)
    {
	w.dialogType=1;
    }
    
    w.open({modal:true,modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_CROSS_DISSOLVE,navBarHidden:true,fullscreen:true});
}

editOptionDialog.addEventListener('click',function(e)
{
    if(e.index==0)
    {
	if(pictures.canDelete==0)
	{
	    var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
	    });
	    alertDialog.show();
	    return;
	}

	catalogDialog(0);
    }
    else if(e.index==1)
    {
	if(pictures.canUpload==0)
	{
	    var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
	    });
	    alertDialog.show();
	    return;
	}

	catalogDialog(1);
    }
    else if(e.index==2)
    {
	if(pictures.canDelete==0)
	{
	    var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
	    });
	    alertDialog.show();
	    return;
	}

	unpublishCatalog(pictures.catalogName);
    }
    else if(e.index==3)
    {
	if(pictures.canDelete==0)
	{
	    var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
	    });
	    alertDialog.show();
	    return;
	}

	var w = Titanium.UI.createWindow({
	    url:'./undelete_pictures.js',
	    previousWindows:previousWindows
	});
    
	w.catalogName = pictures.catalogName;
	Titanium.UI.currentTab.open(w);
    }
    else if(e.index==4)
    {
    }
    else
    {
	return;
    }
});

Ti.App.addEventListener('renameCatalog', function(e)
{
    renameCatalog(pictures.catalogName, e.name);
    //Ti.API.info('rename '+e.name);
});

Ti.App.addEventListener('createCatalog', function(e)
{
    createNewCatalog(e.name);
//    Ti.API.info('create '+e.name);
});

//Navigation bar.
var infoButton = Titanium.UI.createButton({
    title:'Info',
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

function editCatalog()
{
    var w = Titanium.UI.createWindow({
        url:'./edit_pictures.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.catalogName = pictures.catalogName;
    w.canDelete=pictures.canDelete;
    Titanium.UI.currentTab.open(w);
}

function openInfoWin()
{
    var w = Titanium.UI.createWindow({
	    url:'./info.js',
	    barColor:'#111',
	    previousWindows:previousWindows
	    //tabBarHidden:false
    });
    Titanium.UI.currentTab.open(w);
}

var buttonObjects = [
    {title:L("edit"), width:50},
    {title:L("info"), width:50}
];

if(Titanium.Platform.locale=='de')
{
    buttonObjects = [
	{title:L("edit"), width:70},
	{title:L("info"), width:40}
    ];
}

var buttonBar = Titanium.UI.createButtonBar({
    labels:buttonObjects,
    backgroundColor:'#0F3752',
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

buttonBar.addEventListener('click', function(e)
{
    if(e.index==0)
    {
	editOptionDialog.show();
    }
    else
    {
	openInfoWin();
    }
});

infoButton.addEventListener('click', function()
{
    openInfoWin();
}); 

pictures.backButtonTitle=L("catalogs");
pictures.rightNavButton=buttonBar;

var catalogNameLabel = Ti.UI.createLabel({
    height:20,
    top:5,
    left:10,
    right:0,
    color:'white',
    textAlign:'center',
    text:pictures.catalogName
});

pictures.add(catalogNameLabel);

//Toolbar.
var gridButton = Titanium.UI.createButton({
    backgroundImage:'./images/PicsGrid30.png',
    height:30,
    width:30
});

var listButton = Titanium.UI.createButton({
    backgroundImage:'./images/PicsList30.png',
    height:30,
    width:30
});

var contactsButton = Titanium.UI.createButton({
    backgroundImage:'./images/Contacts_Icon30-1.png',
    height:30,
    width:30
});

var uploadButton = Titanium.UI.createButton({
    backgroundImage:'./images/Upload.png',
    height:30,
    width:30
});

var downloadButton = Titanium.UI.createButton({
    backgroundImage:'./images/Download_Icon30-1.png',
    height:30,
    width:30
});

contactsButton.addEventListener('click', function()
{
    if(pictures.canInvite==0)
    {
	var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
        });
        alertDialog.show();
	return;
    }

    var w = Titanium.UI.createWindow({
        url:'./invitation.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.catalogName = pictures.catalogName;
    Titanium.UI.currentTab.open(w);
});

uploadButton.addEventListener('click', function()
{
    if(pictures.canUpload==0)
    {
	var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
        });
        alertDialog.show();
	return;
    }
    
    var w = Titanium.UI.createWindow({
        url:'./upload_picture.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.catalogName = pictures.catalogName;
    Titanium.UI.currentTab.open(w);
});

downloadButton.addEventListener('click', function()
{
    if(pictures.canDownload==0)
    {
	var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
        });
        alertDialog.show();
	return;
    }

    var w = Titanium.UI.createWindow({
        url:'./download_picture.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.catalogName = pictures.catalogName;
    Titanium.UI.currentTab.open(w);
});

var flexSpace = Titanium.UI.createButton({
	systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

var editButton = Titanium.UI.createButton({
    backgroundImage:'./images/Edit_Icon30-4.png',
    height:30,
    width:30
//    systemButton:Titanium.UI.iPhone.SystemButton.COMPOSE
});

editButton.addEventListener('click', function()
{
    var w = Titanium.UI.createWindow({
        url:'./edit_pictures.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.canDelete=pictures.canDelete;
    w.catalogName = pictures.catalogName;
    Titanium.UI.currentTab.open(w);
});

var toolbar = Titanium.UI.createToolbar({
	items:[gridButton,flexSpace,listButton,flexSpace,contactsButton,flexSpace,uploadButton,flexSpace,downloadButton,flexSpace,editButton],
	bottom:0,
	borderTop:false,
	borderBottom:false,
	translucent:true,
	barColor:'#0F3752'
});

pictures.add(toolbar);

//Content view.
var contentView = Ti.UI.createView({
    left:0,
    right:0,
    top:0,
    bottom:50
});
pictures.add(contentView);

//----------------------
var scrollView = Ti.UI.createScrollView({
    contentWidth:Titanium.Platform.displayCaps.platformWidth,
    contentHeight:'auto',
    showVerticalScrollIndicator:true,
    showHorizontalScrollIndicator:false,
    top:30,
    left:0,
    right:0,
    bottom:0,
    layout:'horizontal'
});

Ti.Gesture.addEventListener('orientationchange',function(e)
{
    if(e.orientation==Titanium.UI.PORTRAIT || e.orientation==Titanium.UI.UPSIDE_PORTRAIT)
    {
	scrollView.contentWidth=Titanium.Platform.displayCaps.platformWidth;
	scrollView.contentHeight='auto';
    }
    if(e.orientation==Titanium.UI.LANDSCAPE_LEFT || e.orientation==Titanium.UI.LANDSCAPE_RIGHT)
    {
	scrollView.contentWidth=Titanium.Platform.displayCaps.platformHeight;
	scrollView.contentHeight='auto';
    }
});

function createScrollFolder(folderName,folderPixCount,folderStartIndex,folderEndIndex,folderSubCount,folderTotalSubCount,folderPixCountTotal)
{
    var imageView = Ti.UI.createImageView({
	top:0,
	height:folderSize,
	width:folderSize,
	image:'./images/folder_yellow.png',
	fileName:folderName,
	isFolder:true
	//pictureIndex:i
    });
    
    imageView.folderName=folderName;
    imageView.catalogStartIndex=folderStartIndex;
    imageView.catalogEndIndex=folderEndIndex;
    imageView.catalogSubCount=folderSubCount;
    imageView.catalogTotalSubCount=folderTotalSubCount;
    imageView.folderPixCount=folderPixCount;
    imageView.folderPixCountTotal=folderPixCountTotal;

    var folderLabel = Ti.UI.createLabel({
	height:10,
	bottom:3,
	left:0,
	color:'gray',
	font:{fontSize:12, fontStyle:'italic'},
	textAlign:'center'
	//text:folderName
    });
    
    if(folderTotalSubCount>1)
    {
	folderLabel.text = folderName + " (" + folderPixCountTotal + ")"+ " " +(folderSubCount+1)+ "/" +folderTotalSubCount;
    }
    else
    {
	folderLabel.text = folderName + " (" + folderPixCountTotal + ")";
    }

    var view = Ti.UI.createView({
	height:thumbSize,
	width:thumbSize,
	top:imageOffset,
	left:imageOffset,
	fileName:folderName,
	isFolder:true
	//pictureIndex:i
    });

    view.folderName=folderName;
    view.catalogStartIndex=folderStartIndex;
    view.catalogEndIndex=folderEndIndex;
    view.catalogSubCount=folderSubCount;
    view.catalogTotalSubCount=folderTotalSubCount;
    view.folderPixCount=folderPixCount;
    view.folderPixCountTotal=folderPixCountTotal;
    
    view.add(imageView);
    view.add(folderLabel);
    
    scrollView.add(view);
}

function createFolder(folderName,folderPixCount,folderStartIndex,folderEndIndex,folderSubCount,folderTotalSubCount,folderPixCountTotal)
{
    var row = Ti.UI.createTableViewRow();
    row.height=tableViewRowHeight;
    row.rightImage='./images/arrow_white.png';
    row.pictureName = folderName; 
    row.catalogName = pictures.catalogName;
    row.isFolder = true;
    //row.pictureIndex = i;

    row.catalogStartIndex = folderStartIndex;
    row.catalogEndIndex = folderEndIndex;
    row.catalogSubCount = folderSubCount;
    row.catalogTotalSubCount = folderTotalSubCount;
    row.folderPixCount = folderPixCount;
    row.folderPixCountTotal=folderPixCountTotal;
    
    var imageView = Ti.UI.createImageView({
	image:'./images/folder_yellow.png'
    });
    
    var view = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
    });
    
    view.add(imageView);
    
    var nameLabel = Ti.UI.createLabel({
    height:45,
    top:0,
    color:'white',
    left:tableViewRowHeight
    });	
    
    if(folderTotalSubCount>1)
    {
	nameLabel.text = folderName + " (" + folderPixCountTotal + ")"+ " " +(folderSubCount+1)+ "/" +folderTotalSubCount;
    }
    else
    {
	nameLabel.text = folderName + " (" + folderPixCountTotal + ")";
    }

    row.add(view);
    row.add(nameLabel);
    tableData.push(row);
}

function splitAndCreateFolders(folders,bTableFolder)
{
    foldersCount=folders.length;
    for (i=0;i<folders.length;i++) 
    {
	var folderName = folders.item(i).getElementsByTagName('NAME').item(0).text;
	var folderPixCount=folders.item(i).getElementsByTagName('CNT').item(0).text;
	var folderPixCountTotal=folders.item(i).getElementsByTagName('PIC_CNT').item(0).text;

	var folderTotalSubCount=Math.ceil(folderPixCount/catalogPageSize);
	for(var j=0;j<folderTotalSubCount;j++)
	{
	    var folderStartIndex=j*catalogPageSize;
	    var folderEndIndex;
	    if((j+1)*catalogPageSize<folderPixCount)
	    {
		folderEndIndex=(j+1)*catalogPageSize-1;
	    }
	    else
	    {
		folderEndIndex=(j)*catalogPageSize+(folderPixCount-(j)*catalogPageSize)-1;
	    }
	    
	    if(bTableFolder)
	    {
		createFolder(folderName,folderPixCount,folderStartIndex,folderEndIndex,j,folderTotalSubCount,folderPixCountTotal);
	    }
	    else
	    {
		createScrollFolder(folderName,folderPixCount,folderStartIndex,folderEndIndex,j,folderTotalSubCount,folderPixCountTotal);
	    }
	}
    }
}

function fillScrollBar(files, folders)
{
    if(scrollBarLoaded)
    {
        return;
    }

    contentView.remove(scrollView);

//Ti.API.info("folders.length "+folders.length);
    //first add folders, then list files
    if(folders != null)
    {
	splitAndCreateFolders(folders,false);
    }
	
//Ti.API.info("files.length "+files.length);

    if(files != null)
    {
	for (i=0;i<files.length;i++) 
//	for (i=pictures.catalogStartIndex;i<pictures.catalogEndIndex;i++) 
	{
	    var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
	    var random=new Date().getTime();
	    var thumb_link = REST_WEB_DOCS_URL+files.item(i).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
	    thumb_link+='_'+random;
	    var thumb_width = files.item(i).getElementsByTagName('THUMB_WIDTH').item(0).text;
	    var thumb_height = files.item(i).getElementsByTagName('THUMB_HEIGHT').item(0).text;
				
	    var imageView = Ti.UI.createImageView({
		image:thumb_link,
		//height:thumbSize-8,
		//width:thumbSize-8,
		//image:thumb_binary,
		fileName:filename,
		isFolder:false,
		pictureIndex:i+pictures.catalogStartIndex
	    });

	    var imageContainerView = Ti.UI.createView({
		height:thumbSize-8,
		width:thumbSize-8,
		fileName:filename,
		isFolder:false,
		pictureIndex:i+pictures.catalogStartIndex
	    });
	
	    var view = Ti.UI.createView({
		height:thumbSize,
		width:thumbSize,
		top:imageOffset,
		left:imageOffset,
		backgroundImage:backgroundImagePic,
		fileName:filename,
		isFolder:false,
		pictureIndex:i+pictures.catalogStartIndex
	    });
	
	    imageContainerView.add(imageView);
	    view.add(imageContainerView);
	    scrollView.add(view);
	}
    }

//Ti.API.info("end ");

    scrollBarLoaded=1;
    contentView.add(scrollView);
}

function openFolder(catalogName,pictureName,catalogStartIndex,catalogEndIndex,catalogSubCount,catalogTotalSubCount,
		     folderPixCount,folderPixCountTotal,canDownload,canUpload,canDelete,canInvite,canSendEmail)
{
    w = Titanium.UI.createWindow({
	url:'./pictures.js',
	barColor:'#111',
	previousWindows:previousWindows
    });
    w.catalogName = catalogName + "/" + pictureName;	//enter sub catalog
    w.originalCatalogName=pictures.originalCatalogName;
    w.isFolder=true;
    w.catalogStartIndex = catalogStartIndex;
    w.catalogEndIndex = catalogEndIndex;
    w.catalogSubCount = catalogSubCount;
    w.catalogTotalSubCount = catalogTotalSubCount;
    
    w.catalogPixCount=folderPixCount; //count of pictures only in this directory
    w.catalogPixCountTotal=folderPixCountTotal; //count of pictures in this directory and subdirectories
    
    w.canDownload=canDownload;
    w.canUpload=canUpload;
    w.canDelete=canDelete;
    w.canInvite=canInvite;
    w.canSendEmail=canSendEmail;

    Titanium.UI.currentTab.open(w);
}

scrollView.addEventListener('click',function(ev)
{
    if(ev.source.fileName != null)
    {
	//alert("Is Folder: " + ev.source.isFolder);

	var w;
	if(ev.source.isFolder)
	{
	    openFolder(pictures.catalogName,ev.source.folderName,ev.source.catalogStartIndex,ev.source.catalogEndIndex,
			ev.source.catalogSubCount,ev.source.catalogTotalSubCount,ev.source.folderPixCount,
			ev.source.folderPixCountTotal,pictures.canDownload,pictures.canUpload,pictures.canDelete,
			pictures.canInvite,pictures.canSendEmail);
	}
	else
	{
	    w = Titanium.UI.createWindow({
		url:'./image_viewer_all_pics_oneview_onepic.js',
		barColor:'#111',
		top:0,
		left:0,
		right:0,
		bottom:0,
		previousWindows:previousWindows
	    });

	    w.catalogName = pictures.catalogName;
	    w.fileName = ev.source.fileName;
	    w.pictureIndex = ev.source.pictureIndex;
	    w.catalogTotalCount = catalogTotalCount;
	    w.navBarHidden=true;
	    w.fullscreen=false;
//	    w.fullscreen=true;

//Ti.API.info('pictures.catalogName '+pictures.catalogName);
//Ti.API.info('pictures.catalogStartIndex '+pictures.catalogStartIndex);

	    w.catalogStartIndex = pictures.catalogStartIndex;
	    w.catalogEndIndex = pictures.catalogEndIndex;
	    w.catalogSubCount = pictures.catalogSubCount;
	    w.catalogTotalSubCount = pictures.catalogTotalSubCount;

	    w.canDownload=pictures.canDownload;
	    w.canUpload=pictures.canUpload;
	    w.canDelete=pictures.canDelete;
	    w.canInvite=pictures.canInvite;
	    w.canSendEmail=pictures.canSendEmail;

	    Titanium.UI.currentTab.open(w);
        }
        //w.open({fullscreen:true});
    }
});

var tableView = Titanium.UI.createTableView({
    //data:data,
    top:30,
    rowHeight:60.00,
    separatorColor:'white',
    backgroundColor:'transparent',
    style:Titanium.UI.iPhone.TableViewStyle.GROUPED
});

function createTablePictureRow(i,filename,displayName,thumb_link)
{
    var row = Ti.UI.createTableViewRow();
    row.height=tableViewRowHeight;
    row.rightImage='./images/arrow_white.png';
    row.pictureName = filename; 
    row.displayName = displayName; 
    row.catalogName = pictures.catalogName;
    row.pictureIndex = i+pictures.catalogStartIndex;
    row.thumb_link=thumb_link;
    row.isFolder = false;

    var imageView = Ti.UI.createImageView({
    image:thumb_link
    });
    
    view = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
    });

    view.add(imageView);

    nameLabel = Ti.UI.createLabel({
    height:45,
    color:'white',
    top:0,
    left:tableViewRowHeight
    });	

    nameLabel.text=displayName;
    
    row.add(view);
    row.add(nameLabel);
    
    return row;
}

function fillTableView(files, folders)
{
    if(tableViewLoaded)
    {
        return;
    }
    
    //first add folders, then list files
    var i;
    if(folders != null)
    {
	splitAndCreateFolders(folders,true);
    }
	    
   // Ti.API.info(files.length);
   if(files != null)
    {
	for (i=0;i<files.length;i++) 
//	for (i=pictures.catalogStartIndex;i<pictures.catalogEndIndex;i++) 
	{
	    filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
	    displayName = files.item(i).getElementsByTagName('DISPLAYNAME').item(0).text;
//	    Ti.API.info('display name '+displayName);
	    var thumb_link = REST_WEB_DOCS_URL+files.item(i).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
	    //var thumb = files.item(i).getElementsByTagName('THUMB_BINARY').item(0).text;
	    //var thumb_binary = Titanium.Utils.base64decode(thumb);
	    
	    var row=createTablePictureRow(i,filename,displayName,thumb_link);
	    tableData.push(row);
	    rowToPictureName.push(filename);
	    rowToPictureThumbLink.push(thumb_link);
        }
    }
		
    tableView.data = tableData;
    tableViewLoaded=1;
}

Ti.App.addEventListener('pictureRenamed',function(e)
{
    for (var i=0;i<rowToPictureName.length;i++) 
    {
	if(rowToPictureName[i]==e.nameOld)
	{
	    var row=createTablePictureRow(i,rowToPictureName[i],e.nameNew,rowToPictureThumbLink[i]);
	    tableData[i+foldersCount]=row;
	}
    }
    
    tableView.data=tableData;
//    Ti.App.fireEvent('pictureRenamed',{nameOld:views[scrollView.currentPage].fileName, nameNew:row0TextField.value});
});

tableView.addEventListener('click',function(e)
{
    var w;
    if(e.rowData.isFolder)
    {
	openFolder(e.rowData.catalogName,e.rowData.pictureName,e.rowData.catalogStartIndex,e.rowData.catalogEndIndex,
		    e.rowData.catalogSubCount,e.rowData.catalogTotalSubCount,e.rowData.folderPixCount,
		    e.rowData.folderPixCountTotal,pictures.canDownload,pictures.canUpload,pictures.canDelete,pictures.canInvite,
		    pictures.canSendEmail);
    }
    else
    {
//Ti.API.info('index '+e.source.pictureIndex);

	w = Titanium.UI.createWindow({
	    url:'./image_viewer_all_pics_oneview_onepic.js',
	    barColor:'#111',
	    previousWindows:previousWindows
	});
	w.catalogName = e.rowData.catalogName;
	w.fileName = e.rowData.pictureName;
	w.pictureIndex = e.source.pictureIndex;
	w.catalogTotalCount = catalogTotalCount;    
	w.navBarHidden=true;
	w.fullscreen=true;

	w.catalogStartIndex = pictures.catalogStartIndex;
	w.catalogEndIndex = pictures.catalogEndIndex;
	w.catalogSubCount = pictures.catalogSubCount;
	w.catalogTotalSubCount = pictures.catalogTotalSubCount;

	w.canDownload=pictures.canDownload;
	w.canUpload=pictures.canUpload;
	w.canDelete=pictures.canDelete;
	w.canInvite=pictures.canInvite;
	w.canSendEmail=pictures.canSendEmail;

        Titanium.UI.currentTab.open(w);
    }
});

function addDataToView()
{
    fillScrollBar(files, folders);
    fillTableView(files, folders);
    hideLoadIndicator(pictures);
}

function closethis()
{
//    Ti.UI.currentTab.close(pictures,{animated:true});
    setTimeout(function(){Ti.UI.currentTab.close(pictures,{animated:true});}, nCloseWinTimeout);
}

function pfCallBackSucess(xml)
{
    //Ti.API.info('pfCallBackSucess '+xhrBOCall.responseText);
    if((xml.documentElement.getElementsByTagName('ERROR')!=null && xml.documentElement.getElementsByTagName('ERROR').length>0)
       || xml.documentElement.getElementsByTagName('nTotalCount')==null)
    {
	//Ti.API.info('efsfdsfdsfds '+xml.documentElement.getElementsByTagName('ERROR').length);
	hideLoadIndicator(pictures);
	var alertDialogError = Ti.UI.createAlertDialog({
	    title: 'Get picture error',
	    message: xml.documentElement.getElementsByTagName('ERROR').item(0).getElementsByTagName('ERROR_TEXT').item(0).text
	});
	alertDialogError.show();

//	Ti.API.info('error ');
	Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
	closethis();
	return;
    }

    catalogTotalCount = xml.documentElement.getElementsByTagName('nTotalCount').item(0).text;

    //Ti.API.info('catalogTotalCount '+catalogTotalCount);
//    Ti.API.info('pictures.catalogPixCount '+pictures.catalogPixCount);
//    Ti.API.info('pfCallBackSucess '+xhrBOCall.responseText);

//    if(catalogTotalCount!=pictures.catalogPixCount)
//    {
//	Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
//	closethis();
//    }
  
//Ti.API.info('start index '+pictures.catalogStartIndex);
//Ti.API.info('end index '+pictures.catalogEndIndex);
//Ti.API.info('response image viewer ' +xhrBOCall.responseText);

    
    files = xml.documentElement.getElementsByTagName('file');

    if(pictures.catalogSubCount==0)
    {
	folders = xml.documentElement.getElementsByTagName('folder');
    }

    addDataToView();
}

function pfCallBackError(xml)
{
//    Ti.API.info('pfCallBackError '+xhrBOCall.responseText);

    hideLoadIndicator(pictures);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_alert_error"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getCatalog()
{
    var catalog = pictures.catalogName;
    catalog=xmlEncode(catalog);

    //Get catalog pictures.
    var strPost=
    '<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
//    '<nFromN>-1</nFromN><nToN>-1</nToN><nSortOrder>0</nSortOrder><nPicWidth>'+thumbSize+'</nPicWidth><nPicHeight>'+thumbSize+'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';	
    '<nFromN>'+pictures.catalogStartIndex+'</nFromN><nToN>'+pictures.catalogEndIndex+'</nToN><nSortOrder>0</nSortOrder><nPicWidth>'+thumbSize+'</nPicWidth><nPicHeight>'+thumbSize+'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';	

//    Ti.API.info(strPost);


    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(pictures);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

getCatalog();

//Open event.
pictures.addEventListener('open', function()
{
});

gridButton.addEventListener('click', function()
{
    if(isGridView)
    {
        return;
    }
    else
    {
	contentView.remove(tableView);
	contentView.add(scrollView);
        isGridView=1;
    }
});

listButton.addEventListener('click', function()
{
    if(isGridView)
    {
	contentView.remove(scrollView);
	contentView.add(tableView);
        isGridView=0;
    }
    else
    {
        return;
    }
});

//Focus event.
pictures.addEventListener('focus', function()
{
    //Ti.API.info('focus pictures '+Titanium.App.Properties.getBool('RELOAD_CATALOGS'));
    if(Titanium.App.Properties.getBool('RELOAD_CATALOGS'))
    {
	closethis();
    }
    else
    {
        pictures.showNavBar();
    }
});

//Close event.
pictures.addEventListener('close', function()
{
    if(Titanium.App.Properties.getBool('RELOAD_CATALOGS'))
    {
	//If we are in a folder close all folders and then set 'RELOAD_CATALOGS' to false.
	//This way correctly reloads catalogs after renaming a catalog from subdirectory.
	if(!pictures.isFolder)
	{
	    Titanium.App.Properties.setBool('RELOAD_CATALOGS', false);
	    var newCatalogName=Titanium.App.Properties.getString('NEW_CATALOG_NAME');
	    Titanium.App.Properties.setString('NEW_CATALOG_NAME', '');
	    Ti.App.fireEvent('reloadcatalog',{name:newCatalogName});
	}
    }
});
