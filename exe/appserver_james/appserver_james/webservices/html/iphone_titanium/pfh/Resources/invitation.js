Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');

var person = null;
var isSelectedEmail = false;
var selectedFirstName='';
var selectedLastName='';
var selectedEmail = '';
var selectedPhone = '';
						
var Win = Titanium.UI.currentWindow;
Win.titleControl = null;
Win.titleImage = './images/everPics_TitleBar.png';
Win.backButtonTitle=L("back");

var previousWindows = Win.previousWindows;
previousWindows.push(Win);

//function openEmailInvitation(strEmailSubject,strEmailBody,nIsHtml,strFirstName,strLastName,
//			     selectedEmail,selectedPhone,msgType,previousWindows)
function openEmailInvitation(firstName,strLastName,selectedEmail,selectedPhone)
{
	var nIsHtml = 0;
	if (Ti.Platform.name == 'iPhone OS')
	{ 
		nIsHtml = 1;	//HTML email body is supported only for iPhone family
	}

	var w = Titanium.UI.createWindow({
		url:'./invitation_email.js',
		firstName:firstName,
		strLastName:strLastName,
		catalogName:Win.catalogName,
		nIsHtml:nIsHtml,
		selectedEmail:selectedEmail,
		selectedPhone:selectedPhone,
		previousWindows:previousWindows
	});
	Titanium.UI.currentTab.open(w);
}

var emailLabel = Titanium.UI.createLabel({
	height:30,
	top:0,
	left:5,
	textAlign:'left',
	color:'white',
	text:L("email")+':'
});

var emailTextField = Titanium.UI.createTextField({
	hintText:L("email"),
	//value: strEmailSubject,
	height:30,
	top:35,
	left:5,
	right:5,
	keyboardType:Titanium.UI.KEYBOARD_EMAIL,
	returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
	autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED
});

var sendButton = Titanium.UI.createButton({
	title:L("compose_message"),
	left:5,
	right:5,
	height:40,
	top:70,
	font:{fontWeight:'bold',fontSize:20, fontFamily:'Helvetica Neue'},
	backgroundImage:'./images/greenButton.png'
});

sendButton.addEventListener('click', function()
{
	if(emailTextField.value=='')
	{
		var alertDialog = Ti.UI.createAlertDialog({
		    title:L("email"),
		    message:L("email_insert_alert")
		});
		alertDialog.show();
	}
	else
	{
		selectedFirstName='';
		selectedLastName='';
		selectedEmail=emailTextField.value;
		openEmailInvitation('','',emailTextField.value,'');
	}
});

Win.add(emailLabel);
Win.add(emailTextField);
Win.add(sendButton);

var values = {
	animated: true,
	fields:['email'],
	cancel: function ()
	{
	},

	selectedProperty: function(e)
	{
		selectedFirstName=e.person.firstName;
		selectedLastName=e.person.lastName;
		emailTextField.value=e.value;
		selectedEmail=e.value;
		openEmailInvitation(selectedFirstName,selectedLastName,selectedEmail,selectedPhone);
	}
};

//first select contact
Titanium.Contacts.showContacts(values);

Ti.App.addEventListener('close_invitation', function(e)
{
    setTimeout(function(){Win.close();}, nCloseWinTimeout);
});
