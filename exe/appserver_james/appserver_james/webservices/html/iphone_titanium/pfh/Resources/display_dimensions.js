var win_orientation=Ti.App.Properties.getInt('win_orientation');
var displayWidth;
var displayHeight;

if(win_orientation==Titanium.UI.PORTRAIT || win_orientation==Titanium.UI.UPSIDE_PORTRAIT)
{
    displayWidth=Titanium.Platform.displayCaps.platformWidth;
    displayHeight=Titanium.Platform.displayCaps.platformHeight;
}
else if(win_orientation==Titanium.UI.LANDSCAPE_LEFT || win_orientation==Titanium.UI.LANDSCAPE_RIGHT)
{
    displayWidth=Titanium.Platform.displayCaps.platformHeight;
    displayHeight=Titanium.Platform.displayCaps.platformWidth;
}
else
{
    displayWidth=Titanium.Platform.displayCaps.platformWidth;
    displayHeight=Titanium.Platform.displayCaps.platformHeight;
}
