Titanium.include('./load_indicator.js','./bo_call_new.js');

var newPicName;

var optionDialog = Titanium.UI.createOptionDialog({
	options:[L("rotate_left"),L("rotate_right"),L("delete"),L("rename"),L("cancel")],
	title:L("edit_single_dialog_title")
});

if(Titanium.Platform.osname=='iphone')
{
	optionDialog.cancel=4;
}

function addOption()
{
	optionDialog.show();
}

var deleteDialogConfirmation = Titanium.UI.createOptionDialog({
	title:L("edit_single_delete_dialog_title"),
	buttonNames:[L("ok"),L("cancel")]
});

if(Titanium.Platform.osname=='iphone')
{
	deleteDialogConfirmation.cancel=1;
}

function onDeleteSingle()
{
	if(imageViewer.canDelete==0)
	{
	    var alertDialog = Ti.UI.createAlertDialog({
		title:L("access_right_title"),
		message: strLastBO_ErrorText
	    });
	    alertDialog.show();
	    return;
	}

	deleteDialogConfirmation.message=L("edit_single_delete_dialog_message");
	deleteDialogConfirmation.show();
}

function pfRotateSucess(xml)
{
	hideLoadIndicator(Titanium.UI.currentWindow);
	var random=new Date().getTime();
	var thumb_link=scrollView.views[scrollView.currentPage].thumbLink+'_'+random;
	scrollView.views[scrollView.currentPage].image=thumb_link;
	Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
}

function pfRotateError(xml)
{
	hideLoadIndicator(Titanium.UI.currentWindow);
	//Ti.API.info(xhrBOCall.responseText);
	var alertDialogBOError = Ti.UI.createAlertDialog({
	    title:L("edit_single_rotate_dialog_title"),
	    message: strLastBO_ErrorText
	});
	alertDialogBOError.show();
}

function rotateSingleImage(nAngle)
{
	var views = [];
	views=scrollView.views;
	var picWidth=Titanium.Platform.displayCaps.platformWidth;
	var picHeight=Titanium.Platform.displayCaps.platformHeight;
	var catalog = Titanium.UI.currentWindow.catalogName;
	catalog=xmlEncode(catalog);
	var fileName = views[scrollView.currentPage].fileName;
	
	var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
	'<strFileName>'+fileName+'</strFileName><nAngle>'+nAngle+'</nAngle><nPicWidth>'+picWidth+'</nPicWidth><nPicHeight>'+picHeight+
	'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';
	
	var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
	showLoadIndicator(Titanium.UI.currentWindow);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/rotate",pfRotateSucess,pfRotateError,strPost); 
}

function pfDeleteSinglePictureSucess(xml)
{
	hideLoadIndicator(Titanium.UI.currentWindow);
	var random=new Date().getTime();
	var thumb_link=scrollView.views[scrollView.currentPage].thumbLink+'_'+random;
	var deleteImage='./images/DelPic_iPhone.png';
	if(Titanium.Platform.osname=='ipad')
	{
		deleteImage='./images/DelPic_iPad.png';
	}
	scrollView.views[scrollView.currentPage].image=deleteImage;
	Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
	bImageDeleted=true;
}

function pfDeleteSinglePictureError(xml)
{
	hideLoadIndicator(Titanium.UI.currentWindow);
	var dialog = Titanium.UI.createAlertDialog(
	{
		buttonNames:[L("ok")],
		cancel:0
	});
	dialog.message=L("edit_single_delete_dialog_message");
	dialog.show();
}

function deleteSingleImage()
{
	var views = [];
	views=scrollView.views;
	var fileName = views[scrollView.currentPage].fileName;
	var catalog = Titanium.UI.currentWindow.catalogName;
	catalog=xmlEncode(catalog);
	var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAlias>'+catalog+'</strAlias>'+
	'<strFileName>'+fileName+'</strFileName><nHide>1</nHide></PARAMETERS></REQUEST>';
	
	//Ti.API.info(strPost);
	var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
	showLoadIndicator(Titanium.UI.currentWindow);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/hide",pfDeleteSinglePictureSucess,pfDeleteSinglePictureError,strPost); 
}

deleteDialogConfirmation.addEventListener('click',function(e)
{
	if(e.index==1)
	{
		return;
	}

	deleteSingleImage();
});

function pfRenameSinglePictureSucess(xml)
{
	hideLoadIndicator(Titanium.UI.currentWindow);
	var random=new Date().getTime();
	var thumb_link=scrollView.views[scrollView.currentPage].thumbLink+'_'+random;
	scrollView.views[scrollView.currentPage].image=thumb_link;
	Ti.App.fireEvent('pictureRenamed',{nameOld:scrollView.views[scrollView.currentPage].fileName, nameNew:newPicName});
	var dialog = Titanium.UI.createAlertDialog(
	{
		buttonNames:[L("ok")],
		cancel:0
	});
	dialog.message=L("edit_single_rename_dialog_message");
	dialog.show();
//Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
}

function pfRenameSinglePictureError(xml)
{
	hideLoadIndicator(Titanium.UI.currentWindow);
	var dialog = Titanium.UI.createAlertDialog(
	{
		buttonNames:[L("ok")],
		cancel:0
	});
	dialog.message=L("edit_single_rename_dialog_error_message");
	dialog.show();
}

function onRenameSingle(newPictureName)
{
	newPicName=newPictureName;
	var views = [];
	views=scrollView.views;
	var catalog = Titanium.UI.currentWindow.catalogName;
	catalog=xmlEncode(catalog);
	var fileName = views[scrollView.currentPage].fileName;
	var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAlias>'+catalog+'</strAlias>'+
	'<strFileName>'+fileName+'</strFileName><strDisplayName>'+newPicName+'</strDisplayName></PARAMETERS></REQUEST>';
	
	//Ti.API.info(strPost);
	var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
	showLoadIndicator(Titanium.UI.currentWindow);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/rename",pfRenameSinglePictureSucess,pfRenameSinglePictureError,strPost); 
}

Ti.App.addEventListener('renamePicture', function(e)
{
    onRenameSingle(e.name);
});

function onRenameClicked()
{
	var w = Ti.UI.createWindow({
	    url:'./modal_dialog.js',
	    dialogType:2,
	    oldName:scrollView.views[scrollView.currentPage].displayName
	});
	
	w.open({modal:true,modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_CROSS_DISSOLVE,navBarHidden:true,fullscreen:true});
}

optionDialog.addEventListener('click',function(e)
{
	if(e.index==0)
	{
		rotateSingleImage(-90);
	}
	else if(e.index==1)
	{
		rotateSingleImage(90);
	}
	else if(e.index==2)
	{
		onDeleteSingle();
	}
	else if(e.index==3)
	{
		onRenameClicked();
	}
	else if(e.index==4)
	{
		return;
	}
});
