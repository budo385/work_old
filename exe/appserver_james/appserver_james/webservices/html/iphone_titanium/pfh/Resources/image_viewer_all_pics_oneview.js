Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');

var imageViewer = Titanium.UI.currentWindow;

var previousWindows = imageViewer.previousWindows;
previousWindows.push(imageViewer);

var viewsPortrait = [];
var viewsLandscape = [];
var viewsPortraitThumbLink = [];
var viewsLandscapeThumbLink = [];
var forcePrevious = false;
var forceNext = false;
var bHasFocus=true;
var hidden=true;
var bContinueSlideShow=false;
var toolbarOpacity=1;
var landscapeLoaded=false;
var portraitLoaded=false;
var scrollView=null;

var a = Titanium.UI.createAnimation();
a.duration = 500;
a.opacity = 0;

var slideshowInterval=Titanium.App.Properties.getString('playslidefor')*1000;
var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');

// initialize to all modes
imageViewer.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

Titanium.include('./display_dimensions.js');

var startImageIndex = imageViewer.pictureIndex;
Ti.API.info(startImageIndex);
var intervalID=0;
var timeoutID=0;

var scrollView = Titanium.UI.createScrollableView({
    contentWidth:'auto',
    contentHeight:'auto',
//    backgroundImage:'images/PooltableBlue256.png',
    maxZoomScale:100,
    minZoomScale:0.1,
    top:0,
    left:0,
    bottom:0,
    right:0,
    zIndex:1
});

//imageViewer.add(scrollView);

function functionCallBackError(xml)
{
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Get picture error',
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getPicturesPortrait(catalog,indexFrom,indexTo,pfCallBackSucess,pfCallBackError)
{
    var Width;
    var Height;
    
    if(Titanium.Platform.displayCaps.platformWidth<Titanium.Platform.displayCaps.platformHeight)
    {
	Width=Titanium.Platform.displayCaps.platformWidth;
	Height=Titanium.Platform.displayCaps.platformHeight;
    }
    else
    {
	Width=Titanium.Platform.displayCaps.platformHeight;
	Height=Titanium.Platform.displayCaps.platformWidth;
    }

    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
'<nFromN>'+indexFrom+'</nFromN><nToN>'+indexTo+'</nToN><nSortOrder>0</nSortOrder><nPicWidth>'+Width+'</nPicWidth><nPicHeight>'+Height+'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';

    Ti.API.info('getPicturesPortratit '+strPost);

    showLoadIndicator(imageViewer);
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

function getPicturesLandscape(catalog,indexFrom,indexTo,pfCallBackSucess,pfCallBackError)
{
    if(Titanium.Platform.displayCaps.platformWidth<Titanium.Platform.displayCaps.platformHeight)
    {
	Width=Titanium.Platform.displayCaps.platformWidth;
	Height=Titanium.Platform.displayCaps.platformHeight;
    }
    else
    {
	Width=Titanium.Platform.displayCaps.platformHeight;
	Height=Titanium.Platform.displayCaps.platformWidth;
    }

    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
'<nFromN>'+indexFrom+'</nFromN><nToN>'+indexTo+'</nToN><nSortOrder>0</nSortOrder><nPicWidth>'+Height+'</nPicWidth><nPicHeight>'+Width+'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';
    
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

var path = Titanium.Filesystem.resourcesDirectory;
var previousButton = Titanium.UI.createButton({
	backgroundImage:path +'/images/navPrev.png',
    height:19,
    width:23
});

var playSlideShowButton = Titanium.UI.createButton({
    backgroundImage:path+'/images/navPlay.png',
    height:19,
    width:23
});

var downloadButton = Titanium.UI.createButton({
    backgroundImage:path+'/images/Download_Icon30-1.png',
    height:30,
    width:30
});

var rotateRightButton = Titanium.UI.createButton({
    backgroundImage:path+'/images/Rotate_Icon30-1.png',
    height:30,
    width:30
});

var showOriginalButton = Titanium.UI.createButton({
    backgroundImage:path+'/images/ShowOriginal_Icon30-3.png',
    height:30,
    width:30
});

var nextButton = Titanium.UI.createButton({
    backgroundImage:path+'/images/navNext.png',
    height:19,
    width:23
});

var flexSpace = Titanium.UI.createButton({
	systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

var toolbar = Titanium.UI.createToolbar({
    items:[flexSpace,previousButton,flexSpace,playSlideShowButton,flexSpace,downloadButton,flexSpace,rotateRightButton,flexSpace,showOriginalButton,flexSpace,nextButton,flexSpace],
    bottom:0,
    borderTop:true,
    borderBottom:true,
    barColor:'#0F3752',
    opacity:toolbarOpacity,
    zIndex:10
});

var backButton = Titanium.UI.createButton({
    title:'Catalog',
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

backButton.addEventListener('click', function()
{
//    Titanium.UI.currentWindow.close();
    imageViewer.close();
});

var navImageView = Ti.UI.createImageView({
    top:0,
    left:0,
    bottom:0,
    right:0,
    width:'auto',
    height:'auto',
    image:'./images/everPics_TitleBar.png',
    zIndex:10
});

var titleLabel = Titanium.UI.createLabel({
    color:'white',
    height:35,
    left:10,
    width:100,
    textAlign:'right'
});

function setPageTitle()
{
    var currentReadableIndex = scrollView.currentPage+1;
    titleLabel.text = currentReadableIndex+'/'+imageViewer.catalogTotalCount;
}

var navigationToolbar = Titanium.UI.createToolbar({
    items:[backButton,flexSpace,titleLabel],
    top:0,
    left:0,
    right:0,
    borderTop:true,
    borderBottom:true,
    barColor:'#0F3752',
    opacity:toolbarOpacity,
    zIndex:10
});

navigationToolbar.add(navImageView);

function hideButtons()
{
    timeoutID=0;
    
    toolbar.animate(a);
    a.addEventListener('complete', function(e)
    {
	imageViewer.remove(toolbar);
    });

    navigationToolbar.animate(a);
    a.addEventListener('complete', function(e)
    {
	imageViewer.remove(navigationToolbar);
    });
}

function showButtons()
{
    if(timeoutID!=0)
    {
        clearTimeout(timeoutID);
        timeoutID=0;
    }
    
    if(intervalID!=0)
    {
	clearInterval(intervalID);
	intervalID=0;
    }
    
    toolbar.opacity=toolbarOpacity;
    imageViewer.add(toolbar);
    navigationToolbar.opacity=toolbarOpacity;
    imageViewer.add(navigationToolbar);

    timeoutID=setTimeout(hideButtons,4000);
}

function onPlaySlideShow()
{
    if((scrollView.currentPage+1)<scrollView.views.length)
    {
	scrollView.scrollToView(scrollView.currentPage+1);
    }
    else
    {
	scrollView.currentPage=0;
    }
}

previousButton.addEventListener('click', function()
{
    showButtons();
    
    if(scrollView.currentPage>0)
    {
	scrollView.scrollToView(scrollView.currentPage-1);
    }
});

function openFullImageView()
{
    var views = [];
    views=scrollView.views;
    var catalog = imageViewer.catalogName;
    var fileName = views[scrollView.currentPage].fileName;

    var w = Titanium.UI.createWindow({
        url:'./full_image_viewer.js',
        fileName:fileName,
        catalogName:catalog,
	barColor:'#111',
        navBarHidden:false,
	previousWindows:previousWindows
	//tabBarHidden:false
    });

    Titanium.UI.currentTab.open(w,{transition:Titanium.UI.iPhone.AnimationStyle.CURL_DOWN});
}

showOriginalButton.addEventListener('click', function()
{
    openFullImageView();
});

function pfGetPictureSucess(xml)
{
    var completeDialog = Titanium.UI.createAlertDialog({
        title:'Picture Saved to Photo Gallery',
        buttonNames:['OK'],
        cancel:0
    });

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Download picture error',
        message: strLastBO_ErrorText
    });

    var xhr = Titanium.Network.createHTTPClient();
    xhr.validateSecureCertificate = false;
    
    xhr.onerror = function(e)
    {
	hideLoadIndicator(imageViewer);
	alertDialogBOError.show();
    };
    xhr.onload = function()
    {
	Titanium.Media.saveToPhotoGallery(this.responseData);
	hideLoadIndicator(imageViewer);
    
	completeDialog.show();
    };

    var files = xml.documentElement.getElementsByTagName('file');
    var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
    var thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');

    xhr.open('GET',thumb_link);
    // send the data
    xhr.send();
}

function pfGetPictureError(xml)
{
    hideLoadIndicator(imageViewer);

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Download picture error',
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getImage()
{
    var views = [];
    views=scrollView.views;
    var catalog = imageViewer.catalogName;
    var fileName = views[scrollView.currentPage].fileName;
    
    var strFile='<list>';
    strFile+='<row><strFileName>'+fileName+'</strFileName></row>';
    strFile+='</list>';
    
    //Get catalog pictures.
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+strFile+'<nReturnFull>1</nReturnFull><nPicWidth>320</nPicWidth><nPicHeight>480</nPicHeight><bReturnThumbAsBinary>0</bReturnThumbAsBinary><bSkipAccess>0</bSkipAccess></PARAMETERS></REQUEST>';
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(imageViewer);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/download",pfGetPictureSucess,pfGetPictureError,strPost);
}

downloadButton.addEventListener('click', function()
{
    getImage();
});

function onPlaySlideShowButton()
{
    intervalID=setInterval(onPlaySlideShow, slideshowInterval);
}

playSlideShowButton.addEventListener('click', function()
{
    onPlaySlideShowButton();
    hideButtons();
});

scrollView.addEventListener('scroll', function(e)
{
    setPageTitle();
});

imageViewer.addEventListener('imagesLoaded', function(e)
{
    if(portraitLoaded && landscapeLoaded)
    {
	var w_orientation=Ti.App.Properties.getInt('win_orientation');
	if(w_orientation==Titanium.UI.LANDSCAPE_LEFT || w_orientation==Titanium.UI.LANDSCAPE_RIGHT)
	{
	Ti.API.info('landscape win orientation');
	    //scrollView.views=null;
	    scrollView.views=viewsLandscape;
	}
        else //if(w_orientation==Titanium.UI.PORTRAIT || w_orientation==Titanium.UI.UPSIDE_PORTRAIT)
	{
	Ti.API.info('portrait win orientation');
	    //scrollView.views=null;
	    scrollView.views=viewsPortrait;
	}
	
	hideLoadIndicator(imageViewer);
	//scrollView.scrollToView(startImageIndex-imageViewer.catalogStartIndex);
	scrollView.currentPage=startImageIndex-imageViewer.catalogStartIndex;
	
	imageViewer.add(scrollView);



	Ti.API.info('loaded '+(startImageIndex-imageViewer.catalogStartIndex));

	setPageTitle();
    }
});

function functionCallBackSucessLandscape(xml)
{
    var files = xml.documentElement.getElementsByTagName('file');

//    for (var i=0;i<files.length;i++) 
    for (var i=imageViewer.catalogStartIndex;i<imageViewer.catalogEndIndex;i++) 
	{
            var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
	    var thumb_link = REST_WEB_DOCS_URL+files.item(i).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
            
            var imageView = Ti.UI.createImageView({
            	width:'auto',
                height:'auto',
		fileName:filename,
		thumbLink:thumb_link,
		image:thumb_link
            });

	viewsLandscapeThumbLink.push(thumb_link);
	Ti.API.info('thumb link 222 '+thumb_link);

            viewsLandscape.push(imageView);
 	}
    
    landscapeLoaded=true;
    imageViewer.fireEvent('imagesLoaded');
}

function functionFirstCallBackError(xml)
{
    hideLoadIndicator(imageViewer);

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Get picture error',
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
    imageViewer.close();
}

function functionCallBackSucessPortrait(xml)
{
    var files = xml.documentElement.getElementsByTagName('file');

//    for (var i=0;i<files.length;i++) 
    for (var i=imageViewer.catalogStartIndex;i<imageViewer.catalogEndIndex;i++) 
    {
	var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
	var thumb_link = REST_WEB_DOCS_URL+files.item(i).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
	
	var imageView = Ti.UI.createImageView({
	    width:'auto',
	    height:'auto',
	    fileName:filename,
	    thumbLink:thumb_link,
	    image:thumb_link
	});

	viewsPortraitThumbLink.push(thumb_link);

	Ti.API.info('thumb link '+thumb_link);

	viewsPortrait.push(imageView);
    }
    
    portraitLoaded=true;
    imageViewer.fireEvent('imagesLoaded');
      
    getPicturesLandscape(imageViewer.catalogName,imageViewer.catalogStartIndex,imageViewer.catalogEndIndex,functionCallBackSucessLandscape,functionFirstCallBackError);
//    getPicturesLandscape(imageViewer.catalogName,0,imageViewer.catalogTotalCount-1,functionCallBackSucessLandscape,functionFirstCallBackError);
}

scrollView.addEventListener('singletap', function()
{
    showButtons();
});
scrollView.addEventListener('doubletap', function()
{
    openFullImageView();
});
/*imageViewer.addEventListener('singletap', function()
{
    showButtons();
});
imageViewer.addEventListener('doubletap', function()
{
    openFullImageView();
});*/

nextButton.addEventListener('click', function()
{
    showButtons();

    if((scrollView.currentPage+1)<scrollView.views.length)
    {
	scrollView.scrollToView(scrollView.currentPage+1);
    }
});

getPicturesPortrait(imageViewer.catalogName,imageViewer.catalogStartIndex,imageViewer.catalogEndIndex,functionCallBackSucessPortrait,functionFirstCallBackError);

imageViewer.addEventListener('open', function()
{
    showButtons();

    //Get catalog pictures.

    Ti.API.info('imageViewer.catalogStartIndex '+imageViewer.catalogStartIndex);
    Ti.API.info('imageViewer.catalogEndIndex '+imageViewer.catalogEndIndex);

//    getPicturesPortrait(imageViewer.catalogName,0,imageViewer.catalogTotalCount-1,functionCallBackSucessPortrait,functionFirstCallBackError);
});

// orientation change listener
Ti.Gesture.addEventListener('orientationchange',function(e)
{
    var random=new Date().getTime();

    if(e.orientation==Titanium.UI.PORTRAIT || e.orientation==Titanium.UI.UPSIDE_PORTRAIT)
    {
	//alert('portrait ' + e.orientation);
	Ti.API.info('portrait');
//	startImageIndex=scrollView.currentPage;
//	scrollView.views=null;
//	scrollView.views=viewsPortrait;
//	scrollView.currentPage=startImageIndex;

	for(var i=0;i<viewsPortraitThumbLink.length;i++)
	{
    //	Ti.API.info(view[i]);
	    
	    scrollView.views[i].image=viewsPortraitThumbLink[i]+'_'+random;
	    //Ti.API.info(viewsPortraitThumbLink[i]);
	}

	if(displayWidth<displayHeight)
	{
	    scrollView.contentWidth=displayWidth;
	    scrollView.contentHeight=displayHeight;
	    Ti.API.info('portrait width '+displayWidth);
	    Ti.API.info('portrait height '+displayHeight);
	}
	else
	{
	    Ti.API.info('portrait 1 width '+displayWidth);
	    Ti.API.info('portrait 1 height '+displayHeight);
	    scrollView.contentWidth=displayHeight;
	    scrollView.contentHeight=displayWidth;
	}
    }
    else if(e.orientation==Titanium.UI.LANDSCAPE_LEFT || e.orientation==Titanium.UI.LANDSCAPE_RIGHT)
    {
	Ti.API.info('landscape');
	//alert('landscape ' + e.orientation);
//	startImageIndex=scrollView.currentPage;
//	scrollView.views=null;
//	scrollView.views=viewsLandscape;
//	scrollView.currentPage=startImageIndex;

	for(var j=0;j<viewsLandscapeThumbLink.length;j++)
	{
    //	Ti.API.info(view[j]);
	    
	    scrollView.views[j].image=viewsLandscapeThumbLink[j]+'_'+random;
	    //Ti.API.info(viewsLandscapeThumbLink[j]);
	}

	if(displayWidth<displayHeight)
	{
	    Ti.API.info('landscape width '+displayWidth);
	    Ti.API.info('landscape height '+displayHeight);
	    scrollView.contentWidth=displayHeight;
	    scrollView.contentHeight=displayWidth;
	}
	else
	{
	    Ti.API.info('landscape 1 width '+displayWidth);
	    Ti.API.info('landscape 1 height '+displayHeight);
	    scrollView.contentWidth=displayWidth;
	    scrollView.contentHeight=displayHeight;
	}
    }
    else
    {
	//alert('neka orijentacija ' + e.orientation);
    }
});

function pfRotateSucess(xml)
{
    hideLoadIndicator(imageViewer);
    var random=new Date().getTime();
    
//    var thumb_link_portrait=scrollViewPortrait.views[scrollView.currentPage].thumbLink+'_'+random;
//    var thumb_link_landscape=scrollViewLandscape.views[scrollView.currentPage].thumbLink+'_'+random;

    scrollView.views[scrollView.currentPage].image=thumb_link_portrait;
//    scrollViewLandscape.views[scrollView.currentPage].image=thumb_link_landscape;
}

function pfRotateError(xml)
{
    hideLoadIndicator(imageViewer);
    //Ti.API.info(xhrBOCall.responseText);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Rotate picture error',
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function rotateImage(nAngle)
{
    var views = [];
    views=scrollView.views;
    var picWidth=Titanium.Platform.displayCaps.platformWidth;
    var picHeight=Titanium.Platform.displayCaps.platformHeight;
    var catalog = imageViewer.catalogName;
    var fileName = views[scrollView.currentPage].fileName;

    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
    '<strFileName>'+fileName+'</strFileName><nAngle>'+nAngle+'</nAngle><nPicWidth>'+picWidth+'</nPicWidth><nPicHeight>'+picHeight+
    '</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';
    
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(imageViewer);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/rotate",pfRotateSucess,pfRotateError,strPost); 
}

var rotateDialog = Titanium.UI.createOptionDialog({
	options:['Rotate Left', 'Rotate Right', 'Cancel'],
	title:'Rotate Image'
});

if(Titanium.Platform.osname=='iphone')
{
	rotateDialog.cancel=2;
}

// add event listener
rotateDialog.addEventListener('click',function(e)
{
    Ti.API.info('rotate dialog e.index ');
	if(e.index==0)
	{
	    showButtons();
	    rotateImage(-90);
	}
	else if(e.index==1)
	{
	    showButtons();
	    rotateImage(90);
	}
});

rotateRightButton.addEventListener('click', function()
{
    rotateDialog.show();
});
