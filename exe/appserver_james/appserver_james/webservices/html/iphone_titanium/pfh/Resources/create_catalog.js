Titanium.include('./load_indicator.js','./bo_call_new.js');

var win = Titanium.UI.currentWindow;

var newCatalogName='';
var bFromCatalogWin=false;
//From pictures win.

//From catalog win.
function pfCreateCatalogSucess(xml)
{
	hideLoadIndicator(win);
	if(xml.documentElement.getElementsByTagName('ERROR')==null)
	{
		if(!bFromCatalogWin)
		{
			Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
			Titanium.App.Properties.setString('NEW_CATALOG_NAME', newCatalogName);
		}

		if(bFromCatalogWin)
		{
			Titanium.App.Properties.setString('NEW_CATALOG_NAME','');
			Ti.App.fireEvent('reloadcatalog',{name:newCatalogName});
		}
		else
		{
			win.fireEvent('focus');
		}
	}
	else
	{
		var alertCreateDialogError = Titanium.UI.createAlertDialog({
			title:L("create_catalog_error_alert"),
			message: xml.documentElement.getElementsByTagName('ERROR').item(0).getElementsByTagName('ERROR_TEXT').item(0).text,
			buttonNames:[L("ok")],
			cancel:0
		});
		alertCreateDialogError.show();
		alertCreateDialogError.addEventListener('click',function(e)
		{
			win.fireEvent('focus');
		});
	}
}

function pfCreateCatalogError(xml)
{
	hideLoadIndicator(win);
	var alertCreateDialog = Titanium.UI.createAlertDialog({
		title:L("create_catalog_error_alert_title"),
		message:L("create_catalog_error_alert_message"),
		buttonNames:[L("ok")],
		cancel:0
	});
	alertCreateDialog.show();
	
	if(!bFromCatalogWin)
	{
		Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
	}
}

function createNewCat(newName,bFromCatalogWindow)
{
	bFromCatalogWin=bFromCatalogWindow;
	newCatalogName=newName;
	newName=xmlEncode(newName);
	var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strCatalog>'+newName+'</strCatalog></PARAMETERS></REQUEST>';
	var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
	showLoadIndicator(win);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/create_client",pfCreateCatalogSucess,pfCreateCatalogError,strPost); 
}

function createNewCatalog(newName)
{
	bFromCatalogWin=false;
	createNewCat(newName,bFromCatalogWin);
}

function createNewCatalogFromCatalogWin(newName)
{
	bFromCatalogWin=true;
	createNewCat(newName,bFromCatalogWin);
}