Titanium.include('./background_orientation.js');

//init main window properties
var w = Titanium.UI.currentWindow;
w.titleControl = null;
w.titleImage = './images/everPics_TitleBar.png';
w.backButtonTitle=L("back");

if(!w.bFromIDWindow)
{
	var previousWindows = w.previousWindows;
	previousWindows.push(w);
}

var websiteButton = Titanium.UI.createButton({
	title:L("website")
});

websiteButton.addEventListener('click', function()
{
	Titanium.Platform.openURL('http://www.everpics.com');
});

w.rightNavButton=websiteButton;

var webView=Titanium.UI.createWebView({
	url:'http://www.everpics.com/onlinehelp/epa/en/info.html',
	top:0,
	bottom:0,
	left:0,
	right:0
});

if(Titanium.Platform.locale=='de')
{
	webView.url='http://www.everpics.com/onlinehelp/epa/de/info.html';
}

w.add(webView);
