Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');

var imageViewer = Titanium.UI.currentWindow;
var forcePrevious = false;
var forceNext = false;
var bHasFocus=true;
var hidden=true;
var bContinueSlideShow=false;
var toolbarOpacity=1;

var a = Titanium.UI.createAnimation();
a.duration = 500;
a.opacity = 0;

var REST_WEB_DOCS_URL='';
var useImageURL=true;
if(useImageURL)
{
    REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
}

// initialize to all modes
imageViewer.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

var currentImageIndex = imageViewer.pictureIndex;
var intervalID=0;
var isPlayingSlideShow=0;
var timeoutID=0;

function setPageTitle()
{
    var currentReadableIndex = currentImageIndex+1;
    imageViewer.title = currentReadableIndex+'/'+imageViewer.catalogTotalCount;
}

var scrollView = Titanium.UI.createScrollableView({
    contentWidth:'auto',
    contentHeight:'auto',
    backgroundImage:'./images/PooltableBlue256.png',
    maxZoomScale:100,
    minZoomScale:0.1,
    top:0,
    left:0,
    bottom:0,
    right:0
});

function functionCallBackError(xml)
{
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Get picture error',
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getPictures(catalog,indexFrom,indexTo,pfCallBackSucess,pfCallBackError)
{
    showLoadIndicator(imageViewer);

    var picWidth=Titanium.Platform.displayCaps.platformWidth;
    var picHeight=Titanium.Platform.displayCaps.platformHeight;

    var strPost='';
    if(useImageURL)
    {
	strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
	'<nFromN>'+indexFrom+'</nFromN><nToN>'+indexTo+'</nToN><nSortOrder>0</nSortOrder><nPicWidth>'+picWidth+'</nPicWidth><nPicHeight>'+picHeight+'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';
    }
    else
    {
	strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
	'<nFromN>'+indexFrom+'</nFromN><nToN>'+indexTo+'</nToN><nSortOrder>0</nSortOrder><nPicWidth>'+picWidth+'</nPicWidth><nPicHeight>'+picHeight+'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>1</bReturnThumbAsBinary></PARAMETERS></REQUEST>';
    }
    
    //Ti.API.info(strPost);
    
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

function nextFunctionCallBackSuccess(xml)
{
    var views = [];
    views=scrollView.views;

    var files = xml.documentElement.getElementsByTagName('file');
    var filename = files.item(0).getElementsByTagName('FILENAME').item(0).text;
            
    var thumb_link;
    var thumb;
    var thumb_binary;
    if(useImageURL)
    {
	thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
    }
    else
    {
	thumb = files.item(0).getElementsByTagName('THUMB_BINARY').item(0).text;
	thumb_binary = Titanium.Utils.base64decode(thumb);
    }

    views = scrollView.views;
    views[0].image = views[1].image;
    views[0].filename = views[1].filename;
    views[1].image = views[2].image;
    views[1].filename = views[2].filename;
    if(useImageURL)
    {
	views[2].image = thumb_link;
    }
    else
    {
	views[2].image = thumb_binary;
    }
    views[2].filename = filename;

    scrollView.currentPage=1;
    hideLoadIndicator(imageViewer);

    /*
    var views = [];
    views=scrollView.views;
    var newView = [];
    var imageView = Ti.UI.createImageView({
        width:'auto',
        height:'auto'
    });
    
    var files = xml.documentElement.getElementsByTagName('file');
    var filename = files.item(0).getElementsByTagName('FILENAME').item(0).text;
            
    var thumb = files.item(0).getElementsByTagName('THUMB_BINARY').item(0).text;
    var thumb_binary = Titanium.Utils.base64decode(thumb);
    //var thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
    
    imageView.image=thumb_binary;
    //imageView.image=thumb_link;
    imageView.fileName=filename;
    
    scrollView.addView(imageView);
    views.push(imageView);
    
    //scrollView.removeView(views[0]);
    
    newView[0]=views[1];
    newView[1]=views[2];
    newView[2]=views[2];
    
    scrollView.views=newView;
    scrollView.currentPage=1;
    newView[2]=views[3];
    scrollView.views=newView;
    views=newView;
    hideLoadIndicator(imageViewer);*/
}

function nextPicture()
{
    //Ti.API.info(currentImageIndex);
    if(currentImageIndex<=1)
    {
        return;
    }
    if(currentImageIndex==(imageViewer.catalogTotalCount-1))
    {
        return;
    }

    //Get catalog pictures.
    getPictures(imageViewer.catalogName,-1,-1,nextFunctionCallBackSuccess,functionCallBackError);
}

function previousFunctionCallBackSuccess(xml)
{
    var views = [];
    views=scrollView.views;

    var files = xml.documentElement.getElementsByTagName('file');
    var filename = files.item(0).getElementsByTagName('FILENAME').item(0).text;
            
    var thumb_link;
    var thumb;
    var thumb_binary;
    if(useImageURL)
    {
	thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
    }
    else
    {
	thumb = files.item(0).getElementsByTagName('THUMB_BINARY').item(0).text;
	thumb_binary = Titanium.Utils.base64decode(thumb);
    }

    views[2].image = views[1].image;
    views[2].filename = views[1].filename;
    views[1].image = views[0].image;
    views[1].filename = views[0].filename;
    if(useImageURL)
    {
	views[0].image = thumb_link;
    }
    else
    {
	views[0].image = thumb_binary;
    }
    views[0].filename = filename;

    scrollView.currentPage=1;
    hideLoadIndicator(imageViewer);

/*    var views = [];
    views=scrollView.views;
    var newView = [];
    var imageView = Ti.UI.createImageView({
        width:'auto',
        height:'auto'
    });
    
    var files = xml.documentElement.getElementsByTagName('file');
    var filename = files.item(0).getElementsByTagName('FILENAME').item(0).text;
            
    var thumb = files.item(0).getElementsByTagName('THUMB_BINARY').item(0).text;
    var thumb_binary = Titanium.Utils.base64decode(thumb);
//    var thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
    
    imageView.image=thumb_binary;
//    imageView.image=thumb_link;
    imageView.fileName=filename;
    
    newView[0]=views[0];
    newView[1]=views[0];
    newView[2]=views[1];
    
    scrollView.views=newView;
    scrollView.currentPage=1;
    newView[0]=imageView;
    scrollView.views=newView;
    views=newView;
    hideLoadIndicator(imageViewer);*/
}

function getPreviousPicture()
{
    //Get catalog pictures.
    if(currentImageIndex==0)
    {
        return;
    }
    if(currentImageIndex>=(imageViewer.catalogTotalCount-2))
    {
        return;
    }
    
    getPictures(imageViewer.catalogName,currentImageIndex-1,currentImageIndex-1,previousFunctionCallBackSuccess,functionCallBackError);
}

var previousButton = Titanium.UI.createButton({
	backgroundImage:'./images/navPrev.png',
    height:19,
    width:23
});

var pauseSlideShowButton = Titanium.UI.createButton({
    backgroundImage:'./images/navPause.png',
    height:19,
    width:23
});

var playSlideShowButton = Titanium.UI.createButton({
    backgroundImage:'./images/navPlay.png',
    height:19,
    width:23
});

var downloadButton = Titanium.UI.createButton({
    backgroundImage:'./images/Download_Icon30-1.png',
    height:30,
    width:30
});

var rotateRightButton = Titanium.UI.createButton({
    backgroundImage:'./images/Rotate_Icon30-1.png',
    height:30,
    width:30
});

var showOriginalButton = Titanium.UI.createButton({
    backgroundImage:'./images/ShowOriginal_Icon30-3.png',
    height:30,
    width:30
});

var nextButton = Titanium.UI.createButton({
    backgroundImage:'./images/navNext.png',
    height:19,
    width:23
});

var flexSpace = Titanium.UI.createButton({
	systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

var playToolbar = Titanium.UI.createToolbar({
    items:[flexSpace,previousButton,flexSpace,playSlideShowButton,flexSpace,downloadButton,flexSpace,rotateRightButton,flexSpace,showOriginalButton,flexSpace,nextButton,flexSpace],
    bottom:0,
    borderTop:true,
    borderBottom:true,
    barColor:'#0F3752',
    opacity:toolbarOpacity
});

var pauseToolbar = Titanium.UI.createToolbar({
    items:[flexSpace,previousButton,flexSpace,pauseSlideShowButton,flexSpace,downloadButton,flexSpace,rotateRightButton,flexSpace,showOriginalButton,flexSpace,nextButton,flexSpace],
    bottom:0,
    borderTop:true,
    borderBottom:true,
    barColor:'#0F3752',
    opacity:toolbarOpacity
});

var backButton = Titanium.UI.createButton({
    title:'Catalog',
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

backButton.addEventListener('click', function()
{
    imageViewer.close();
});

var navImageView = Ti.UI.createImageView({
    top:0,
    left:0,
    bottom:0,
    right:0,
    width:'auto',
    height:'auto',
    image:'./images/everPics_TitleBar.png'
});

var navigationToolbar = Titanium.UI.createToolbar({
    items:[backButton, flexSpace],
    top:0,
    borderTop:true,
    borderBottom:true,
    barColor:'#0F3752',
    opacity:toolbarOpacity
});

navigationToolbar.add(navImageView);

//Set toolbar.
function setToolBarWithPlay()
{
    playToolbar.opacity=toolbarOpacity;
    imageViewer.add(playToolbar);
}

function setToolBarWithPause()
{
    pauseToolbar.opacity=toolbarOpacity;
    imageViewer.add(pauseToolbar);
}

function hideButtons()
{
    timeoutID=0;
    
    if(intervalID!=0)
    {
        pauseToolbar.animate(a);
	a.addEventListener('complete', function(e)
	{
	    imageViewer.remove(pauseToolbar);
	});
    }
    else
    {
        playToolbar.animate(a);
	a.addEventListener('complete', function(e)
	{
	    imageViewer.remove(playToolbar);
	});
    }
    
    navigationToolbar.animate(a);
    a.addEventListener('complete', function(e)
    {
	imageViewer.remove(navigationToolbar);
    });
}

function showButtons()
{
    if(timeoutID!=0)
    {
        clearTimeout(timeoutID);
        timeoutID=0;
    }
    
    if(intervalID!=0)
    {
        setToolBarWithPause();
    }
    else
    {
        setToolBarWithPlay();
    }
    
    navigationToolbar.opacity=toolbarOpacity;
    imageViewer.add(navigationToolbar);

    timeoutID=setTimeout(hideButtons,4000);
}

function onPauseSlideShow()
{
    clearInterval(intervalID);
    intervalID=0;
    showButtons();
}

function onPlaySlideShow()
{
    nextButton.fireEvent('click');
}

previousButton.addEventListener('click', function()
{
    showButtons();

    if(currentImageIndex==0)
    {
        return;
    }
    
    var views = [];
    views=scrollView.views;

    if(currentImageIndex==1)
    {
        scrollView.scrollToView(views[0]);
        return;
    }

    if(currentImageIndex==(imageViewer.catalogTotalCount-1))
    {
        scrollView.scrollToView(views[1]);
        forcePrevious = true;
        return;
    }
    
    scrollView.scrollToView(views[0]);
});

function openFullImageView()
{
    var views = [];
    views=scrollView.views;
    var catalog = imageViewer.catalogName;
    var fileName = views[scrollView.currentPage].fileName;

    var w = Titanium.UI.createWindow({
        url:'./full_image_viewer.js',
        fileName:fileName,
        catalogName:catalog,
	barColor:'#111',
        navBarHidden:false
	//tabBarHidden:false
    });

    Titanium.UI.currentTab.open(w,{transition:Titanium.UI.iPhone.AnimationStyle.CURL_DOWN});
}

showOriginalButton.addEventListener('click', function()
{
    openFullImageView();
});

function pfGetPictureSucess(xml)
{
    var completeDialog = Titanium.UI.createAlertDialog({
        title:'Picture Saved to Photo Gallery',
        buttonNames:['OK'],
        cancel:0
    });

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Download picture error',
        message: strLastBO_ErrorText
    });

    var xhr = Titanium.Network.createHTTPClient();
    xhr.validateSecureCertificate = false;
    
    xhr.onerror = function(e)
    {
	hideLoadIndicator(imageViewer);
	alertDialogBOError.show();
    };
    xhr.onload = function()
    {
	Titanium.Media.saveToPhotoGallery(this.responseData);
	hideLoadIndicator(imageViewer);
    
	completeDialog.show();
    };

    var files = xml.documentElement.getElementsByTagName('file');
    var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
    var thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');

    xhr.open('GET',thumb_link);
    // send the data
    xhr.send();
}

function pfGetPictureError(xml)
{
    hideLoadIndicator(imageViewer);

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Download picture error',
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getImage()
{
    var views = [];
    views=scrollView.views;
    var catalog = imageViewer.catalogName;
    var fileName = views[scrollView.currentPage].fileName;
    
    var strFile='<list>';
    strFile+='<row><strFileName>'+fileName+'</strFileName></row>';
    strFile+='</list>';
    
    //Get catalog pictures.
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+strFile+'<nReturnFull>1</nReturnFull><nPicWidth>320</nPicWidth><nPicHeight>480</nPicHeight><bReturnThumbAsBinary>0</bReturnThumbAsBinary><bSkipAccess>0</bSkipAccess></PARAMETERS></REQUEST>';
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(imageViewer);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/download",pfGetPictureSucess,pfGetPictureError,strPost);
}

downloadButton.addEventListener('click', function()
{
    getImage();
});

function onPlaySlideShowButton()
{
    if(currentImageIndex==(imageViewer.catalogTotalCount-1))
    {
        return;
    }

    intervalID=setInterval(onPlaySlideShow, 5000);
}

playSlideShowButton.addEventListener('click', function()
{
    onPlaySlideShowButton();
    showButtons();
});

pauseSlideShowButton.addEventListener('click', function()
{
    onPauseSlideShow();
});

scrollView.addEventListener('scroll', function(e)
{
    var views = [];
    views=scrollView.views;
    if(e.currentPage<1 || forcePrevious || currentImageIndex==imageViewer.catalogTotalCount-1)
    {
        forcePrevious = false;

        currentImageIndex--;
        if(currentImageIndex<0)
        {
            currentImageIndex=0;
        }
        setPageTitle();
        getPreviousPicture();
        return;
    }
    if(e.currentPage>1 || forceNext || currentImageIndex==0)
    {
        forceNext = false;
        
        currentImageIndex++;
        
        if(currentImageIndex>(imageViewer.catalogTotalCount-1))
        {
            currentImageIndex=imageViewer.catalogTotalCount-1;
            if(intervalID!=0)
            {
                onPauseSlideShow();
                scrollView.scrollToView(views[2]);
            }
        }
        setPageTitle();
        nextPicture();
        return;
    }
});

function functionCallBackSucess(xml)
{
    hideLoadIndicator(imageViewer);
//Ti.API.info('draw now');
//Ti.App.fireEvent('showLoading');
//    showIndicator();    
    var files = xml.documentElement.getElementsByTagName('file');
    var newViews = [];
    for (var i=0;i<files.length;i++) 
	{
            var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
            
	    var thumb_link;
	    var thumb;
	    var thumb_binary;
	    if(useImageURL)
	    {
		thumb_link = REST_WEB_DOCS_URL+files.item(i).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
	    }
	    else
	    {
            	thumb = files.item(i).getElementsByTagName('THUMB_BINARY').item(0).text;
		thumb_binary = Titanium.Utils.base64decode(thumb);
	    }
            
            var imageView = Ti.UI.createImageView({
            	width:'auto',
                height:'auto',
		fileName:filename
            });

	    if(useImageURL)
	    {
		imageView.image=thumb_link;
	    }
	    else
	    {
                imageView.image=thumb_binary;
	    }
            
            newViews.push(imageView);
 	}
    
    scrollView.views=newViews;

    showButtons();

    if(currentImageIndex==0)
    {
        scrollView.currentPage=0;
        imageViewer.add(scrollView);
    }
    else if(currentImageIndex==(imageViewer.catalogTotalCount-1))
    {
        scrollView.currentPage=2;
        imageViewer.add(scrollView);
    }
    else
    {
        scrollView.currentPage=1;
        imageViewer.add(scrollView);
    }
    
    if(bContinueSlideShow==true)
    {
        bContinueSlideShow=false;
        onPlaySlideShowButton();
    }
}

function functionFirstCallBackError(xml)
{
    hideLoadIndicator(imageViewer);

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Get picture error',
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
    imageViewer.close();
}

imageViewer.addEventListener('singletap', function()
{
    showButtons();
});

imageViewer.addEventListener('doubletap', function()
{
    openFullImageView();
});

function onOpen(bFromOpen)
{
    setPageTitle();

    var catalog = imageViewer.catalogName;
    var catalogCount = imageViewer.catalogTotalCount;
    var indexFrom = currentImageIndex-1;
    var indexTo = currentImageIndex+1;

 //   Ti.API.info(catalog+' '+catalogCount);
   
    if(currentImageIndex==0)
    {
        indexFrom = 0;
        indexTo = 2;
    }

    if(currentImageIndex==(catalogCount-1))
    {
        indexFrom = currentImageIndex-2;
        indexTo = currentImageIndex;
    }
    
    if(bFromOpen)
    {
        timeoutID=setTimeout(hideButtons,4000);
    }

    //Get catalog pictures.
    getPictures(catalog,indexFrom,indexTo,functionCallBackSucess,functionFirstCallBackError);
}
nextButton.addEventListener('click', function()
{
    if(intervalID==0)
    {
        showButtons();
    }
    
    var views = [];
    views=scrollView.views;

    if(currentImageIndex==0)
    {   
        forceNext = true;
        scrollView.scrollToView(views[1]);
        return;
    }
    
    if(currentImageIndex==(imageViewer.catalogTotalCount-1))
    {
        if(intervalID!=0)
        {
            onPauseSlideShow();
            bContinueSlideShow=true;
            currentImageIndex=0;
            hideButtons();
            onOpen(false);
            return;
        }
    }
    
    scrollView.scrollToView(views[2]);
});

imageViewer.addEventListener('open', function()
{
//    Ti.API.info('open');
    onOpen(true);
});

imageViewer.addEventListener('focus', function()
{
//    Ti.API.info('focus');
    bHasFocus=true;
    //showButtons();
});

imageViewer.addEventListener('blur', function()
{
//    Ti.API.info('blur');
    bHasFocus=false;
});

// orientation change listener
Ti.Gesture.addEventListener('orientationchange',function(e)
{
//    Ti.API.info('orientationchange');
/*    if(!bHasFocus)
    {
        return;
    }*/
    //onOpen(false);
    //imageViewer.remove(scrollView);
    //scrollView.width=Titanium.Platform.displayCaps.platformWidth;
    //scrollView.height=Titanium.Platform.displayCaps.platformHeight;
    //imageViewer.add(scrollView);
});

function pfRotateSucess(xml)
{
    //Ti.API.info('pfRotateSucess');

    hideLoadIndicator(imageViewer);
    var views = [];
    views=scrollView.views;
    var newView = [];
    var imageView = Ti.UI.createImageView({
        width:'auto',
        height:'auto'
    });
    
    var files = xml.documentElement.getElementsByTagName('file');
    var filename = files.item(0).getElementsByTagName('FILENAME').item(0).text;
            
    var thumb = files.item(0).getElementsByTagName('THUMB_BINARY').item(0).text;
    var thumb_binary = Titanium.Utils.base64decode(thumb);
    
    imageView.image=thumb_binary;
    imageView.fileName=filename;
    
    var currentPage=scrollView.currentPage;
    
    for(var i=0;i<3;i++)
    {
        if(i==currentPage)
        {
            newView[i]=imageView;
        }
        else
        {
            newView[i]=views[i];
        }
    }
    
    scrollView.views=newView;
    //views=newView;
    //Ti.API.info(currentImageIndex);
    //scrollView.currentPage=1;
}

function pfRotateError(xml)
{
    hideLoadIndicator(imageViewer);
    //Ti.API.info(xhrBOCall.responseText);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title: 'Rotate picture error',
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function rotateImage(nAngle)
{
    var views = [];
    views=scrollView.views;
    var picWidth=Titanium.Platform.displayCaps.platformWidth;
    var picHeight=Titanium.Platform.displayCaps.platformHeight;
    var catalog = imageViewer.catalogName;
    var fileName = views[scrollView.currentPage].fileName;

    var strPost=
	'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
	'<strFileName>'+fileName+'</strFileName><nAngle>'+nAngle+'</nAngle><nPicWidth>'+picWidth+'</nPicWidth><nPicHeight>'+picHeight+'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>1</bReturnThumbAsBinary></PARAMETERS></REQUEST>';
    
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(imageViewer);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/rotate",pfRotateSucess,pfRotateError,strPost); 
}

rotateRightButton.addEventListener('click', function()
{
    showButtons();
    rotateImage(90);
});

Ti.App.addEventListener('forcenewlogin', function()
{
    imageViewer.close();
});
