Titanium.include('./load_indicator.js','./bo_call_new.js','./download_dialog.js','./background_orientation.js');

var downloadPicture = Titanium.UI.currentWindow;
downloadPicture.backButtonTitle = L("catalog");
downloadPicture.title =L("download");

var previousWindows = downloadPicture.previousWindows;
previousWindows.push(downloadPicture);

var selectedPictures=[];
var downloadImagesURLs=[];
var imageID=0;

var progressBar=Titanium.UI.createProgressBar({
	width:Titanium.Platform.displayCaps.platformWidth,
	min:0,
	value:0,
	height:70,
	bottom:0,
	color:'#fff',
	font:{fontSize:14, fontWeight:'bold'},
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	backgroundColor:'#0F3752',
	zIndex:10
});

downloadPicture.add(progressBar);

var downloadButton = Titanium.UI.createButton({
	title:L("download"),
	style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

var alertDownloadDialog = Titanium.UI.createAlertDialog({
	title:L("download"),
	message:L("download_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
});

downloadButton.addEventListener('click', function()
{
    if(selectedPictures.length==0)
    {
	alertDownloadDialog.show();
        return;
    }
    alertDialog.show();
});

var errorDialog = Titanium.UI.createAlertDialog(
{
	buttonNames:[L("ok")],
	cancel:0
});

errorDialog.addEventListener('click', function(e)
{
    uploadPicture.close();
});

downloadPicture.rightNavButton=downloadButton;

// initialize to some modes
downloadPicture.orientationModes = [
	Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var completeDialog = Titanium.UI.createAlertDialog({
	title:L("download_complete_dialog_title"),
	buttonNames:[L("ok")],
	cancel:0
    });

completeDialog.addEventListener('click', function(e)
{
    downloadPicture.close();
});

var headerLabel = Ti.UI.createLabel({
    height:20,
    top:5,
    left:0,
    right:0,
    color:'white',
    textAlign:'center',
    text:L("download_table_header_label")+' '+downloadPicture.catalogName
});

downloadPicture.add(headerLabel);

var tableView = Titanium.UI.createTableView({
    //data:data,
    top:40,
    rowHeight:60.00,
    backgroundColor:'transparent',
    separatorColor:'white',
    style:Titanium.UI.iPhone.TableViewStyle.GROUPED
}); 

function addRemovePicNameToArray(pictureName,bAdd)
{
    var nIndex=-1;
    for(var i=0;i<selectedPictures.length;i++)
    {
        if(pictureName==selectedPictures[i])
        {
            nIndex=i;
        }
    }
    
    if(bAdd)
    {
        if(nIndex<0)
        {
            selectedPictures.push(pictureName);
        }
    }
    else
    {
        if(nIndex>=0)
        {
            var newArray = [];
            var nCount=0;
            for(var j=0;j<selectedPictures.length;j++)
            {
                if(j!=nIndex)
                {
                    newArray[nCount]=selectedPictures[j];
                    nCount++;
                }
            }
            selectedPictures=newArray;
        }
    }
}

var path = Titanium.Filesystem.resourcesDirectory;

tableView.addEventListener('click',function(e)
{
	var checkImageView = e.row.children[2];

	if(e.rowData.checked)
	{
		checkImageView.image=null;
		e.rowData.checked=0;
		addRemovePicNameToArray(e.rowData.pictureName,false);
	}
	else
	{
		Ti.API.info('selectedPictures[j].length '+selectedPictures.length);
		if(selectedPictures.length==5)
		{
			var alertDialogDownload = Ti.UI.createAlertDialog({
			    title:L("download"),
			    message:L("download_alert_message")
			});
			alertDialogDownload.show();
			return;
		}
	
		checkImageView.image='./images/Checked.png';
		e.rowData.checked=1;
		addRemovePicNameToArray(e.rowData.pictureName,true);
	}
});

downloadPicture.add(tableView);
function pfCallBackSucess(xml)
{
    catalogTotalCount = xml.documentElement.getElementsByTagName('nTotalCount').item(0).text;
    files = xml.documentElement.getElementsByTagName('file');

    var data = []; 
   // Ti.API.info(files.length);
    for (var i=0;i<files.length;i++) 
	{
        var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
        var thumb = files.item(i).getElementsByTagName('THUMB_BINARY').item(0).text;
        var thumb_binary = Titanium.Utils.base64decode(thumb);
           
        var row = Ti.UI.createTableViewRow();
        row.height=95;
        row.rightImage=null;
        row.checked=0;
	row.pictureName = filename; 
        row.catalogName = downloadPicture.catalogName;
        row.pictureIndex = i;

        var imageView = Ti.UI.createImageView({
            image:thumb_binary
        });
            
        var view = Ti.UI.createView({
            height:75,
            width:75,
            top:10,
            left:10
        });
        
        view.add(imageView);

        var nameLabel = Ti.UI.createLabel({
            height:45,
            top:0,
	    right:30,
	    color:'white',
	    left:95
        });	

        var checkImageView = Ti.UI.createImageView({
            top:0,
	    right:5,
	    width:20,
	    bottom:0
        });

        nameLabel.text=filename;
        
        row.add(view);
        row.add(nameLabel);
        row.add(checkImageView);
        data.push(row); 
    }
		
	tableView.data = data;
    tableViewLoaded=1;
    hideLoadIndicator(downloadPicture);
}

function pfCallBackError(xml)
{
    hideLoadIndicator(downloadPicture);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_alert_error"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getCatalog()
{
    var catalog = downloadPicture.catalogName;
    catalog=xmlEncode(catalog);
    
    //Get catalog pictures.
    var strPost=
	'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
	'<nFromN>-1</nFromN><nToN>-1</nToN><nSortOrder>0</nSortOrder><nPicWidth>75</nPicWidth><nPicHeight>75</nPicHeight>'+	
	'<bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>1</bReturnThumbAsBinary></PARAMETERS></REQUEST>';	
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(downloadPicture);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

//Open event.
downloadPicture.addEventListener('open', function()
{
    getCatalog();
});

function createFileList()
{
    var strFile='<list>';
    for(var i=0;i<selectedPictures.length;i++)
    {
        var fileName = selectedPictures[i];
        strFile+='<row><strFileName>'+fileName+'</strFileName></row>';
    }
    strFile+='</list>';
    return strFile;
}

function downloadImage(imageId,imageURL)
{
	var xhr = Titanium.Network.createHTTPClient();
	xhr.validateSecureCertificate = false;
	
	xhr.onerror = function(e)
	{
	    errorDialog.title=L("error")+' '+e.error;
	    errorDialog.show();
	};
	xhr.onload = function()
	{
		Titanium.Media.saveToPhotoGallery(this.responseData);
		downloadPicture.fireEvent('downloadnext');
	};
	
	Ti.API.info(imageURL);

	progressBar.message=L("download_progress_bar_message_0")+' '+(imageId+1)+' '+L("of")+' '+downloadImagesURLs.length;
	progressBar.value = imageId+1;
	// open the client
	xhr.open('GET',imageURL);
	// send the data
	xhr.send();
}

function download(imageId)
{
	downloadImage(imageId,downloadImagesURLs[imageId]);
}

downloadPicture.addEventListener('downloadnext', function(e)
{
	imageID++;
	if(imageID<downloadImagesURLs.length)
	{
		download(imageID);	
	}
	else
	{
		progressBar.hide();
		completeDialog.show();
	}
});

function pfGetPictureSucess(xml)
{
	hideLoadIndicator(downloadPicture);
	var files = xml.documentElement.getElementsByTagName('file');
	
	progressBar.message=L("download_progress_bar_message_1")+' '+files.length;
	progressBar.max=files.length;
	
	downloadPicture.setToolbar([progressBar]);
	
	for(var i=0;i<files.length;i++)
	{
		var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
		var thumb_link = REST_WEB_DOCS_URL+files.item(i).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
		
		downloadImagesURLs.push(thumb_link);
	}
	
	imageID=0;
	download(imageID);
}

function pfGetPictureError(xml)
{
    hideLoadIndicator(downloadPicture);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_error_alert_message"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getFullSize()
{
    var catalog = downloadPicture.catalogName;
    catalog=xmlEncode(catalog);

    //Get catalog pictures.
    var fileList = createFileList();
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'	+fileList+'<nReturnFull>1</nReturnFull><nPicWidth>320</nPicWidth><nPicHeight>480</nPicHeight>'+	
	'<bReturnThumbAsBinary>0</bReturnThumbAsBinary><bSkipAccess>0</bSkipAccess></PARAMETERS></REQUEST>';	
    
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(downloadPicture);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/download",pfGetPictureSucess,pfGetPictureError,strPost); 
}

function getDeviceOptimized()
{
    var Width;
    var Height;
    
    if(Titanium.Platform.displayCaps.platformWidth>Titanium.Platform.displayCaps.platformHeight)
    {
	Width=Titanium.Platform.displayCaps.platformWidth;
	Height=Titanium.Platform.displayCaps.platformHeight;
    }
    else
    {
	Width=Titanium.Platform.displayCaps.platformHeight;
	Height=Titanium.Platform.displayCaps.platformWidth;
    }

    var catalog = downloadPicture.catalogName;
    catalog=xmlEncode(catalog);
    
    //Get catalog pictures.
    var fileList = createFileList();
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+fileList+'<nReturnFull>0</nReturnFull><nPicWidth>'+Width+'</nPicWidth><nPicHeight>'+Height+'</nPicHeight>'+	
	'<bReturnThumbAsBinary>0</bReturnThumbAsBinary><bSkipAccess>0</bSkipAccess></PARAMETERS></REQUEST>';	

    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(downloadPicture);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/download",pfGetPictureSucess,pfGetPictureError,strPost); 
}

alertDialog.addEventListener('click', function(e)
{
	if(e.index==0)
	{
		progressBar.show();
		getFullSize();
	}
	if(e.index==1)
	{
		progressBar.show();
		getDeviceOptimized();
	}
});














