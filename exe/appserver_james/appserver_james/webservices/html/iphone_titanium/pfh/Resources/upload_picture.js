Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');

var uploadPicture = Titanium.UI.currentWindow;
uploadPicture.backgroundColor = 'black';
var images = [];
var imageID=0;

var previousWindows = uploadPicture.previousWindows;
previousWindows.push(uploadPicture);

var downloadDir = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,'downloaddir');
if(downloadDir.exists())
{
	downloadDir.deleteDirectory();
	downloadDir.createDirectory();
}
else
{
	downloadDir.createDirectory();
}

uploadPicture.addEventListener('close', function(e)
{
	downloadDir.deleteDirectory();
});

// initialize to some modes
uploadPicture.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var completeDialog = Titanium.UI.createAlertDialog({
	title:L("upload"),
	message:L("upload_complete_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
});

completeDialog.addEventListener('click', function(e)
{
    uploadPicture.close();
});

var errorDialog = Titanium.UI.createAlertDialog(
{
	title:L("upload_error_dialog_title"),
	buttonNames:[L("ok")],
	cancel:0
});

errorDialog.addEventListener('click', function(e)
{
    uploadPicture.close();
});

var addPictureButton = Titanium.UI.createButton({
	title:L("add_picture_upload_button"),
	//width:300,
	left:5,
	right:5,
	height:40,
	top:10,
	font:{fontWeight:'bold', fontSize:20/*, fontFamily:'Helvetica Neue'*/},
	backgroundImage:'./images/greenButton.png'
});

var takePhotoButton = Titanium.UI.createButton({
	title:L("take_photo_upload_button"),
	//width:300,
	left:5,
	right:5,
	height:40,
	top:60,
	font:{fontWeight:'bold', fontSize:20/*, fontFamily:'Helvetica Neue'*/},
	backgroundImage:'./images/greenButton.png'
});

var uploadToServerButton = Titanium.UI.createButton({
	title:L("upload_added_pictures_button"),
	//width:300,
	left:5,
	right:5,
	height:40,
	top:110,
	font:{fontWeight:'bold', fontSize:20/*, fontFamily:'Helvetica Neue'*/},
	backgroundImage:'./images/greenButton.png'
});

var cancelButton = Titanium.UI.createButton({
	title:L("cancel"),
	//width:300,
	left:5,
	right:5,
	height:40,
	top:160,
	font:{fontWeight:'bold', fontSize:20/*, fontFamily:'Helvetica Neue'*/},
	backgroundImage:'./images/greenButton.png'
});

var Label = Ti.UI.createLabel({
	height:20,
	top:210,
	left:10,
	right:0,
	color:'white',
	textAlign:'left'
});

var ind=Titanium.UI.createProgressBar({
	left:5,
	right:5,
	height:40,
	bottom:30,
	min:0,
	max:1,
	value:0,
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	font:{fontSize:12, fontWeight:'bold'},
	color:'#888'
});

var imageView = Titanium.UI.createImageView({
	height:200,
	width:200,
	top:20,
	left:10,
	hide:true
});

uploadPicture.add(imageView);

var popoverView;
var arrowDirection;

if (Titanium.Platform.osname == 'ipad')
{
	// photogallery displays in a popover on the ipad and we
	// want to make it relative to our image with a left arrow
	arrowDirection = Ti.UI.iPad.POPOVER_ARROW_DIRECTION_LEFT;
	popoverView = imageView;
}

addPictureButton.addEventListener('click', function(e)
{
	Titanium.Media.openPhotoGallery(
	{
		success:function(event)
		{
			var blob = event.media;
			
			//var random=Math.floor(Math.random()*10000000);
			var random=new Date().getTime();
			var filename=random+'.jpg';
	
			var newFile = Titanium.Filesystem.getFile(downloadDir.nativePath,filename);
			newFile.write(blob);
			
			images.push(filename);
			Label.text=images.length+' '+L("upload_pictures_label");
		},
		cancel:function()
		{
			//uploadPicture.close();
		},
		error:function(error)
		{
			//uploadPicture.close();
		},
		allowEditing:false,
		showControls:true,
		popoverView:popoverView,
		arrowDirection:arrowDirection,
		mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
	});
});

takePhotoButton.addEventListener('click', function(e)
{
	Titanium.Media.showCamera(
	{
		success:function(event)
		{
			showLoadIndicator(uploadPicture);
			var cropRect = event.cropRect;
			var blob = event.media;
			
			//var random=Math.floor(Math.random()*10000000);
			var random=new Date().getTime();
			var filename=random+'.jpg';
	
			var newFile = Titanium.Filesystem.getFile(downloadDir.nativePath,filename);
			newFile.write(blob);
			
			images.push(filename);
			Label.text=images.length+' '+L("upload_pictures_label");
			hideLoadIndicator(uploadPicture);
			takePhotoButton.fireEvent('click');
		},
		cancel:function()
		{
	
		},
		error:function(error)
		{
			// create alert
			var a = Titanium.UI.createAlertDialog({title:L("camera_error_alert_title")});
	
			// set message
			if (error.code == Titanium.Media.NO_CAMERA)
			{
				a.setMessage(L("camera_error_alert_message_0"));
			}
			else
			{
				a.setMessage(L("camera_error_alert_message_1")+' '+error.code);
			}
	
			// show alert
			a.show();
		},
		allowEditing:false
	});
});

function uploadImage(imageId)
{
	var xhr = Titanium.Network.createHTTPClient();
	xhr.onerror = function(e)
	{
	    errorDialog.message=L("error")+e.error;
	    errorDialog.show();
	};
	xhr.setTimeout(20000);
	var ROOT_URL=Titanium.App.Properties.getString('serverURL');

	var filename=images[imageId];
	var f = Titanium.Filesystem.getFile(downloadDir.nativePath, filename);
	var blob=f.read();

	xhr.onload = function()
	{
		uploadPicture.fireEvent('uploadnext');
	};
	xhr.onsendstream = function(e)
	{
		ind.message=L("upload_indicator_message")+' '+(imageId+1)+' '+L("of")+' '+images.length;
		ind.value = e.progress ;
		ind.show();
	};
	xhr.open('POST',ROOT_URL+"/MULTIPART_UPLOAD");
	
	var session=Titanium.App.Properties.getString('web_session');
	var random=Math.floor(Math.random()*10000000);
	var catalog=uploadPicture.catalogName;
	
	var overwrite='1';

	// send the data
	xhr.send({media:blob,session:session,catalog:catalog,filename:filename,overwrite:overwrite});
}

function upload(imageId)
{
	uploadImage(imageId);
}

uploadPicture.addEventListener('uploadnext', function(e)
{
	imageID++;
	if(imageID<images.length)
	{
		upload(imageID);	
	}
	else
	{
		Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
		completeDialog.show();
	}
});

uploadToServerButton.addEventListener('click', function(e)
{
	if(!images.length)
	{
		errorDialog.message=L("upload_error_dialog_message");
		errorDialog.show();
		return;
	}
	imageID=0;
	upload(imageID);
});

cancelButton.addEventListener('click', function(e)
{
	uploadPicture.close();
});

uploadPicture.addEventListener('close', function(e)
{
	//Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
});

uploadPicture.add(addPictureButton);
uploadPicture.add(takePhotoButton);
uploadPicture.add(uploadToServerButton);
uploadPicture.add(cancelButton);
uploadPicture.add(Label);
uploadPicture.add(ind);
