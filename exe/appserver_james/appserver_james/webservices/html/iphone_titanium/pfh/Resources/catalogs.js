Titanium.include('./load_indicator.js','./bo_call_new.js','login_calls.js','./background_orientation.js','./create_catalog.js');

var catalogsWin = Titanium.UI.currentWindow;
//catalogsWin.titleControl = null;
catalogsWin.titleImage = './images/everPics_TitleBar.png';

var catalogPageSize=Ti.App.Properties.getString('CATALOG_PAGE_SIZE');
var path = Titanium.Filesystem.resourcesDirectory;
var bNewCatalogCreated=false;
var newCatalogNameForScroll='';
var catalogRowCounter='';
var newCatalogRowForScrollTo='';
var newCatalogRow=null;

var logoutButton = Titanium.UI.createButton({
    title:L("logout")
});

var previousWindows = catalogsWin.previousWindows;
previousWindows.push(catalogsWin);

logoutButton.addEventListener('click', function()
{
    var REST_LOGOUT = Titanium.App.Properties.getString('REST_LOGOUT');
    BO_LogOut(REST_LOGOUT);
    Titanium.UI.currentWindow.close();
});

function openCatalogInfoWin()
{
    var w = Titanium.UI.createWindow({
	    url:'./info.js',
	    barColor:'#111',
	    previousWindows:previousWindows
	    //tabBarHidden:false
    });
    Titanium.UI.currentTab.open(w);
}

var editCatalogOptionDialog = Titanium.UI.createOptionDialog({
	options:[L("new"),L("refresh"),L("cancel")],
	cancel:2,
	title:L("catalogs")
});

if(Titanium.Platform.osname=='iphone')
{
	editCatalogOptionDialog.cancel=2;
}

var buttonObjectsCatalog = [
    {title:L("edit"), width:50},
    {title:L("info"), width:50}
];

if(Titanium.Platform.locale=='de')
{
    buttonObjectsCatalog = [
	{title:L("edit"), width:70},
	{title:L("info"), width:40}
    ];
}

var buttonBarCatalog = Titanium.UI.createButtonBar({
    labels:buttonObjectsCatalog,
    backgroundColor:'#0F3752',
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

buttonBarCatalog.addEventListener('click', function(e)
{
    if(e.index==0)
    {
	editCatalogOptionDialog.show();
    }
    else
    {
	openCatalogInfoWin();
    }
});

function catalogEditDialog()
{
    var w = Ti.UI.createWindow({
	url:'./modal_dialog.js',
	dialogType:1,
	fromCatalogWin:true
    });
    
    w.open({modal:true,modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_CROSS_DISSOLVE,navBarHidden:true,fullscreen:true});
}

editCatalogOptionDialog.addEventListener('click',function(e)
{
    if(e.index==0)
    {
	catalogEditDialog();
    }
    else if(e.index==1)
    {
	Ti.App.fireEvent('reloadcatalog');
    }
    else
    {
	return;
    }
});

catalogsWin.leftNavButton=logoutButton;
catalogsWin.rightNavButton=buttonBarCatalog;

// initialize to all modes
catalogsWin.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

//------------------
//Table view.
//------------------
//Add items.
var footerRow = Titanium.UI.createView();
footerRow.height=90;

var serverNameLabel = Titanium.UI.createLabel({
	text:catalogsWin.serverName,
	height:30,
	left:10,
	top:0,
	color:'white',
	font:{fontWeight:'bold',fontFamily:'Helvetica Neue'},
	textAlign:'left'
});
var headerRow = Titanium.UI.createView();
headerRow.height=30;
headerRow.add(serverNameLabel);

//Upper table view.
var tableView = Titanium.UI.createTableView({
    //data:data,
    top:0,
    rowHeight:60.00,
    backgroundColor:'transparent',
    //separatorStyle:Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
    separatorColor:'transparent',
    headerView:headerRow,
    footerView:footerRow
    //style:Titanium.UI.iPhone.TableViewStyle.GROUPED
});

catalogsWin.add(tableView);

//------------------------------------------------------
// BO call methods (get catalogs, fill the list)
//------------------------------------------------------
function openPicturesWin(catalogName,catalogStartIndex,catalogEndIndex,catalogSubCount,catalogTotalSubCount,
			catalogPixCount,catalogPixCountTotal,canDownload,canUpload,canDelete,canInvite,canSendEmail)
{
    var w = Titanium.UI.createWindow({
        url:'./pictures.js',
	previousWindows:previousWindows
    });

    w.catalogName=catalogName;
    w.originalCatalogName=catalogName;
    w.isFolder=false;
    w.catalogStartIndex = catalogStartIndex;
    w.catalogEndIndex = catalogEndIndex;
    w.catalogSubCount = catalogSubCount;
    w.catalogTotalSubCount = catalogTotalSubCount;
    w.catalogPixCount = catalogPixCount; //count of pictures only in this catalog
    w.catalogPixCountTotal=catalogPixCountTotal; //count of pics with pictures in subfolders.
    w.canDownload=canDownload;
    w.canUpload=canUpload;
    w.canDelete=canDelete;
    w.canInvite=canInvite;
    w.canSendEmail=canSendEmail;

//Ti.API.info('e.rowData.catalogTotalSubCount '+e.rowData.catalogTotalSubCount);
//Ti.API.info('e.rowData.catalogPixCount '+e.rowData.catalogPixCount);
//Ti.API.info('e.rowData.canDownload '+e.rowData.canDownload);
//Ti.API.info('e.rowData.canUpload '+e.rowData.canUpload);
//Ti.API.info('e.rowData.canDelete '+e.rowData.canDelete);
//Ti.API.info('e.rowData.canInvite '+e.rowData.canInvite);

    Titanium.UI.currentTab.open(w);
}

function createCatalog(catalogName,catalogPixCount,catalogThumb,catalogStartIndex,catalogEndIndex,catalogSubCount,
		       catalogTotalSubCount,canDownload,canUpload,canDelete,canInvite,canSendEmail,catalogPixCountTotal)
{
    	var row = Ti.UI.createTableViewRow();
	row.height=95;
	row.rightImage='./images/arrow_white.png';

        row.catalogName = catalogName;
        row.catalogStartIndex = catalogStartIndex;
        row.catalogEndIndex = catalogEndIndex;
        row.catalogSubCount = catalogSubCount;
        row.catalogTotalSubCount = catalogTotalSubCount;
	row.catalogPixCount=catalogPixCount;
	row.catalogPixCountTotal=catalogPixCountTotal;
	row.backgroundImage='./images/PTB_TableElement.jpg';
	row.selectedBackgroundImage='./images/PTB_TableElement_Sel.png';
	row.canDownload=canDownload;
	row.canUpload=canUpload;
	row.canDelete=canDelete;
	row.canInvite=canInvite;
	row.canSendEmail=canSendEmail;

	var imageView = Ti.UI.createImageView({
	});
	if(catalogThumb=='' || catalogThumb==null)
	{
	    imageView.image='./images/user75.png';
	}
	else
	{
	    imageView.image=Titanium.Utils.base64decode(catalogThumb);
	}
        
        var view = Ti.UI.createView({
            height:75,
            width:75,
            top:10,
            left:10
        });
	
        view.add(imageView);

	var nameLabel = Ti.UI.createLabel({
	height:45,
	top:0,
	color:'white',
	font:{fontWeight:'bold',fontFamily:'Helvetica Neue'},
	left:95
	});	
	
	//Row views.
	if(catalogTotalSubCount>1)
	{
	    nameLabel.text = catalogName + " (" + catalogPixCountTotal + ")"+ " " +(catalogSubCount+1)+ "/" +catalogTotalSubCount;
	}
	else
	{
	    nameLabel.text = catalogName + " (" + catalogPixCountTotal + ")";
	}

	row.add(view);
	row.add(nameLabel);
	
	//Scroll to setting.
	if(newCatalogNameForScroll==catalogName && bNewCatalogCreated)
	{
	    newCatalogRowForScrollTo=catalogRowCounter;
	    bNewCatalogCreated=false;
	    newCatalogRow=row;
	}

	catalogRowCounter++;
	
	tableView.appendRow(row);
}

var path = Titanium.Filesystem.resourcesDirectory;
function parseCatalogXML(xml) 
{
    //alert(xhrBOCall.responseText);

//Ti.API.info('2222 '+xhrBOCall.responseText);
    newCatalogRowForScrollTo=0;
    catalogRowCounter=0;
    newCatalogRow=null;
    var data=[];
    tableView.data=data;
    catalogsWin.remove(tableView);
    //parse incoming XML DOM and process it immediately
    var catalogs = xml.documentElement.getElementsByTagName('catalog');
    if(catalogs===null)
    {
	hideLoadIndicator(catalogsWin);
        return;
    }

    for (var i=0;i<catalogs.length;i++)  
    { 
	var catalogName=catalogs.item(i).getElementsByTagName('ALIAS').item(0).text;
	catalogName=xmlDecode(catalogName);
	var catalogPixCountTotal=catalogs.item(i).getElementsByTagName('PIC_CNT').item(0).text;
	var catalogPixCount=catalogs.item(i).getElementsByTagName('CNT').item(0).text;
	var catalogThumbData=catalogs.item(i).getElementsByTagName('THUMB').item(0).text;
	var canDownload=catalogs.item(i).getElementsByTagName('CAN_DOWNLOAD').item(0).text;
	var canUpload=catalogs.item(i).getElementsByTagName('CAN_UPLOAD').item(0).text;
	var canDelete=catalogs.item(i).getElementsByTagName('CAN_DELETE').item(0).text;
	var canInvite=catalogs.item(i).getElementsByTagName('CAN_SEND_INVITE').item(0).text;
	var canSendEmail=catalogs.item(i).getElementsByTagName('CAN_SEND_EMAIL').item(0).text;

	var catalogTotalSubCount=Math.ceil(catalogPixCount/catalogPageSize);
	if(catalogPixCount==0)
	{
	    createCatalog(catalogName,catalogPixCount,catalogThumbData,0,0,0,0,canDownload,canUpload,canDelete,canInvite,canSendEmail,catalogPixCountTotal);
	}
	else
	{
	    for(var j=0;j<catalogTotalSubCount;j++)
	    {
		var catalogStartIndex=j*catalogPageSize;
		var catalogEndIndex;
		if((j+1)*catalogPageSize<catalogPixCount)
		{
		    catalogEndIndex=(j+1)*catalogPageSize-1;
		}
		else
		{
		    catalogEndIndex=(j)*catalogPageSize+(catalogPixCount-(j)*catalogPageSize)-1;
		}
		
		createCatalog(catalogName,catalogPixCount,catalogThumbData,catalogStartIndex,catalogEndIndex,j,catalogTotalSubCount,canDownload,canUpload,canDelete,canInvite,canSendEmail,catalogPixCountTotal);
	    }
	}
    }
	
    if(newCatalogNameForScroll!='' && newCatalogRow!=null)
    {
//	Ti.API.info('newCatalogRowForScrollTo '+newCatalogRow.catalogName);
	tableView.scrollToIndex(newCatalogRowForScrollTo);
	newCatalogRowForScrollTo='';
	openPicturesWin(newCatalogRow.catalogName,newCatalogRow.catalogStartIndex,newCatalogRow.catalogEndIndex,
			newCatalogRow.catalogSubCount,newCatalogRow.catalogTotalSubCount,newCatalogRow.catalogPixCount,
			newCatalogRow.catalogPixCountTotal,newCatalogRow.canDownload,newCatalogRow.canUpload,
			newCatalogRow.canDelete,newCatalogRow.canInvite,newCatalogRow.canSendEmail);
    }
    catalogsWin.add(tableView);
    hideLoadIndicator(catalogsWin);
}

function onBoCallError(xml)
{
    hideLoadIndicator(catalogsWin);
    alert(L("catalog_error_alert"));
}

function getCatalogs()
{
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(catalogsWin);
    BO_Call("GET",REST_WEB_ROOT_URL+"/service/catalogs",parseCatalogXML, onBoCallError, '');
}

getCatalogs();

tableView.addEventListener('click', function(e)
{
    var w = Titanium.UI.createWindow({
        url:'./pictures.js',
	previousWindows:previousWindows
    });

    openPicturesWin(e.rowData.catalogName,e.rowData.catalogStartIndex,e.rowData.catalogEndIndex,e.rowData.catalogSubCount,
		    e.rowData.catalogTotalSubCount,e.rowData.catalogPixCount,e.rowData.catalogPixCountTotal,
		    e.rowData.canDownload,e.rowData.canUpload,e.rowData.canDelete,e.rowData.canInvite,e.rowData.canSendEmail);
});

Ti.App.addEventListener('reloadcatalog', function(e)
{
    if(e.name!=null || e.name!='')
    {
	newCatalogNameForScroll=e.name;
	bNewCatalogCreated=true;
    }
    //getCatalogs();
    
    //Ti.API.info('reload catalog');
    setTimeout(getCatalogs, nCloseWinTimeout);
});

Ti.App.addEventListener('createCatalogFromCatalogWin', function(e)
{
    createNewCatalogFromCatalogWin(e.name);
});

