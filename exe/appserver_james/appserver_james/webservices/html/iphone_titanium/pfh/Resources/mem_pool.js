var MemoryPool = function() {
    var _window;
    /*
     Here we make our "auto-release" pool. It's simply a window.
     We hide it upon creation so it won't interfere with our view hierarchy.
 
     5/3/2011: It seems that the window does not need to be a subcontext, just a regular window will do.
     */
    this.init = function() {
        _window = Ti.UI.createWindow({
	orientationModes:[
			Titanium.UI.PORTRAIT,
			Titanium.UI.UPSIDE_PORTRAIT,
			Titanium.UI.LANDSCAPE_LEFT,
			Titanium.UI.LANDSCAPE_RIGHT
			]
	});
        _window.hide();
        _window.open(/*{modal:true}*/);
    }
    // This is where we clear out the memPool by closing it then reopening it again.
    this.clean = function(obj) {
        if(obj instanceof Array) {
            var arLen=obj.length;
            for ( var i=0, len=arLen; i<len; ++i ) {
                // We then stick the entire view into the pool
                _window.add(obj[i]);
            }
        } else {
            // We then stick the entire view into the pool
            _window.add(obj);
        }
        Ti.API.info('Cleaning MemoryPool.');
 
        // We empty the pool by closing it.
        _window.close();
 
        // We recreate the window again for the next object
        //this.init();
    };
    this.close = function() {
	_window.close();
    };

    this.init();
}