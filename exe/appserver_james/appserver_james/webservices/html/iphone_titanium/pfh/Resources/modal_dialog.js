Titanium.include('./background_orientation.js');

var modal_dialog = Titanium.UI.currentWindow;
modal_dialog.fullscreen=true;
var okPressed=false;

/*modal_dialog.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];*/

var data = [];

//Header view.   
var headerLabel = Titanium.UI.createLabel({
    height:30,
    top:5,
    left:10,
    color:'white',
    font:{fontWeight:'bold',fontSize:18, fontFamily:'Helvetica Neue'},
    textAlign:'left'
});
var headerView = Titanium.UI.createView();
headerView.height=40;
headerView.add(headerLabel);

var row0 = Titanium.UI.createTableViewRow();
//row0.backgroundColor='transparent';
row0.height=40;
row0.top=0;

var row0TextField = Titanium.UI.createTextField({
    height:30,
    top:5,
    left:5,
    right:5,
    keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
    autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
    borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED
});
row0.add(row0TextField);

data.push(row0);

//Table view.
var tableView = Titanium.UI.createTableView({
    data:data,
    backgroundColor:'transparent',
    separatorStyle:Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
    separatorColor:'transparent',
    top:10,
    left:0,
    right:0,
    height:80,
    //rowHeight:40.00,
    headerView:headerView//,
    //style:Titanium.UI.iPhone.TableViewStyle.GROUPED
});

var okButton = Titanium.UI.createButton({
    title:L("create_catalog_button"),
    top:100,
    left:5,
    right:5,
    height:30
});

okButton.addEventListener('click', function()
{
    if(row0TextField.value=='')
    {
	var alertDialog = Ti.UI.createAlertDialog({
	    title:L("create_catalog_alert_title"),
	    message:L("create_catalog_alert_message")
	});
	alertDialog.show();

	return;
    }
    
    okPressed=true;
    modal_dialog.close();
});

var cancelButton = Titanium.UI.createButton({
    title:L("cancel"),
    top:140,
    left:5,
    right:5,
    height:30
});

cancelButton.addEventListener('click', function()
{
    modal_dialog.close();
});

//dialogType=0 -> rename catalog.
if(modal_dialog.dialogType==0)
{
    okButton.title=L("modal_dialog_ok_button_type_0");
    headerLabel.text=L("modal_dialog_header_label_type_0");
    row0TextField.value=modal_dialog.catalogName;
}
//dialogType=1 -> new catalog.
else if(modal_dialog.dialogType==1)
{
    okButton.title=L("modal_dialog_ok_button_type_1");
    headerLabel.text=L("modal_dialog_header_label_type_1");
    row0TextField.hintText=L("modal_dialog_textfield_hint_type_1");
}
//dialogType=2 -> rename picture.
else if(modal_dialog.dialogType==2)
{
    okButton.title=L("modal_dialog_ok_button_type_2");
    headerLabel.text=L("modal_dialog_header_label_type_2");
    row0TextField.value=modal_dialog.oldName;
}

modal_dialog.addEventListener('close', function()
{
    if(!okPressed)
    {
	return;
    }
    
    //dialogType=0 -> rename catalog.
    if(modal_dialog.dialogType==0)
    {
	Ti.App.fireEvent('renameCatalog',{name:row0TextField.value});
    }
    //dialogType=1 -> new catalog.
    else if(modal_dialog.dialogType==1)
    {
	if(modal_dialog.fromCatalogWin)
	{
	    Ti.App.fireEvent('createCatalogFromCatalogWin',{name:row0TextField.value});
	}
	else
	{
	    Ti.App.fireEvent('createCatalog',{name:row0TextField.value});
	}
    }
    //dialogType=2 -> rename picture.
    else if(modal_dialog.dialogType==2)
    {
	Ti.App.fireEvent('renamePicture',{name:row0TextField.value});
    }
});

modal_dialog.add(tableView);
modal_dialog.add(okButton);
modal_dialog.add(cancelButton);
