Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');

var rotatePicture = Titanium.UI.currentWindow;
rotatePicture.backButtonTitle=L("catalog");
rotatePicture.title=L("rotate");

var previousWindows = rotatePicture.previousWindows;
previousWindows.push(rotatePicture);

var selectedPictures=[];
var imageID=0;
var rotationAngle=0;
var errorCount=0;

var progressBar=Titanium.UI.createProgressBar({
	width:Titanium.Platform.displayCaps.platformWidth,
	min:0,
	height:70,
	bottom:0,
	value:0,
	color:'#fff',
	font:{fontSize:14, fontWeight:'bold'},
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	backgroundColor:'#0F3752',
	zIndex:10
});

rotatePicture.add(progressBar);

var rotateButton = Titanium.UI.createButton({
	title:L("rotate"),
	style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

var alertRotateDialog = Titanium.UI.createAlertDialog({
	title:L("rotate"),
	message:L("rotate_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
});

var rotateOptionDialog = Titanium.UI.createOptionDialog({
	options:[L("rotate_left"),L("rotate_right"),L("cancel")],
	cancel:2,
	title:L("rotate_image")
});

if(Titanium.Platform.osname=='iphone')
{
	rotateOptionDialog.cancel=2;
}

rotateButton.addEventListener('click', function()
{
    if(selectedPictures.length==0)
    {
	alertRotateDialog.show();
        return;
    }
    rotateOptionDialog.show();
});

var errorDialog = Titanium.UI.createAlertDialog(
{
	buttonNames:[L("ok")],
	cancel:0
});

errorDialog.addEventListener('click', function(e)
{
	setTimeout(function(){rotatePicture.close();},nCloseWinTimeout);
});

rotatePicture.rightNavButton=rotateButton;

// initialize to some modes
rotatePicture.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var completeDialog = Titanium.UI.createAlertDialog({
	title:L("rotate_dialog_success_title"),
	message:L("rotate_dialog_success_message"),
	buttonNames:[L("ok")],
	cancel:0
    });

completeDialog.addEventListener('click', function(e)
{
	setTimeout(function(){rotatePicture.close();},nCloseWinTimeout);
});

var headerLabel = Ti.UI.createLabel({
    height:20,
    top:5,
    left:0,
    right:0,
    color:'white',
    textAlign:'center',
    text:L("rotate_pictures_table_header")+': '+rotatePicture.catalogName
});

rotatePicture.add(headerLabel);

var tableView = Titanium.UI.createTableView({
    //data:data,
    top:40,
    rowHeight:60.00,
    backgroundColor:'transparent',
    separatorColor:'white',
    style:Titanium.UI.iPhone.TableViewStyle.GROUPED
}); 

function addRemovePicNameToArray(pictureName,bAdd)
{
    var nIndex=-1;
    for(var i=0;i<selectedPictures.length;i++)
    {
        if(pictureName==selectedPictures[i])
        {
            nIndex=i;
        }
    }
    
    if(bAdd)
    {
        if(nIndex<0)
        {
            selectedPictures.push(pictureName);
        }
    }
    else
    {
        if(nIndex>=0)
        {
            var newArray = [];
            var nCount=0;
            for(var j=0;j<selectedPictures.length;j++)
            {
                if(j!=nIndex)
                {
                    newArray[nCount]=selectedPictures[j];
                    nCount++;
                }
            }
            selectedPictures=newArray;
        }
    }
}

tableView.addEventListener('click',function(e)
{
	var checkImageView = e.row.children[2];

	if(e.rowData.checked)
	{
		checkImageView.image=null;
		e.rowData.checked=0;
		addRemovePicNameToArray(e.rowData.pictureName,false);
	}
	else
	{
		checkImageView.image='./images/Checked.png';
		e.rowData.checked=1;
		addRemovePicNameToArray(e.rowData.pictureName,true);
	}
});

rotatePicture.add(tableView);

function pfCallBackSucess(xml)
{
    catalogTotalCount = xml.documentElement.getElementsByTagName('nTotalCount').item(0).text;
    files = xml.documentElement.getElementsByTagName('file');

    var data = []; 
   // Ti.API.info(files.length);
    for (var i=0;i<files.length;i++) 
	{
        var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
        var thumb = files.item(i).getElementsByTagName('THUMB_BINARY').item(0).text;
        var thumb_binary = Titanium.Utils.base64decode(thumb);
           
        var row = Ti.UI.createTableViewRow();
        row.height=95;
        row.rightImage=null;
        row.checked=0;
	row.pictureName = filename; 
        row.catalogName = rotatePicture.catalogName;
        row.pictureIndex = i;

        var imageView = Ti.UI.createImageView({
            image:thumb_binary
        });
            
        var view = Ti.UI.createView({
            height:75,
            width:75,
            top:10,
            left:10
        });
        
        view.add(imageView);

        var nameLabel = Ti.UI.createLabel({
            height:45,
            top:0,
	    right:30,
	    color:'white',
	    left:95
        });	

        var checkImageView = Ti.UI.createImageView({
            top:0,
	    right:5,
	    width:20,
	    bottom:0
        });

        nameLabel.text=filename;
        
        row.add(view);
        row.add(nameLabel);
        row.add(checkImageView);
        data.push(row); 
    }
		
	tableView.data = data;
    tableViewLoaded=1;
    hideLoadIndicator(rotatePicture);
}

function pfCallBackError(xml)
{
    hideLoadIndicator(rotatePicture);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_alert_error"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getCatalog()
{
    var catalog = rotatePicture.catalogName;
    
    //Get catalog pictures.
    var strPost=
	'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
	'<nFromN>-1</nFromN><nToN>-1</nToN><nSortOrder>0</nSortOrder><nPicWidth>75</nPicWidth><nPicHeight>75</nPicHeight>'+	
	'<bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>1</bReturnThumbAsBinary></PARAMETERS></REQUEST>';	
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(rotatePicture);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

getCatalog();

function pfRotatePictureSucess(xml)
{
	imageID++;
	progressBar.message=L("rotating")+' '+imageID+' '+L("of")+' '+selectedPictures.length;
	progressBar.value=imageID;
	rotatePicture.fireEvent('rotateNext');
}

function pfRotatePictureError(xml)
{
	errorCount++;
}

function rotateImage(nAngle)
{
    var fileName = selectedPictures[imageID];

    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+rotatePicture.catalogName+'</strAliasDirectory>'+
    '<strFileName>'+fileName+'</strFileName><nAngle>'+rotationAngle+'</nAngle><nPicWidth>320</nPicWidth><nPicHeight>480</nPicHeight>'+
    '<bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';
    
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(rotatePicture);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/rotate",pfRotatePictureSucess,pfRotatePictureError,strPost); 
}

rotatePicture.addEventListener('rotateNext', function(e)
{
	if(imageID<selectedPictures.length)
	{
		rotateImage(rotationAngle);
	}
	else
	{
		hideLoadIndicator(rotatePicture);
		progressBar.hide();
		if(errorCount==0)
		{
			completeDialog.show();
		}
		else
		{
			completeDialog.message=(selectedPictures.length-errorCount)+' '+L("of")+' '+selectedPictures.length + ' '+L("edit_pictures_rotating_success");
			completeDialog.show();
		}
	}
});

rotateOptionDialog.addEventListener('click',function(e)
{
	imageID=0;

	progressBar.max=selectedPictures.length;
	progressBar.message=L("rotating")+' '+(imageID+1)+' '+L("of")+' '+selectedPictures.length;
	progressBar.value=imageID+1;

	if(e.index==0)
	{
		rotationAngle=-90;
		rotatePicture.fireEvent('rotateNext');
		progressBar.show();
	}
	else if(e.index==1)
	{
		rotationAngle=90;
		rotatePicture.fireEvent('rotateNext');
		progressBar.show();
	}
});
