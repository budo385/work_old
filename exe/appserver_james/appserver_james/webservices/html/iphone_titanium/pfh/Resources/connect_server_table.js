//Layout for ipad and iphonoids.
var scrollview = Ti.UI.createScrollView({
    contentWidth:'auto',
    contentHeight:'auto',
    top:0,
    bottom:0,
    left:0,
    right:0,
    showVerticalScrollIndicator:true,
    showHorizontalScrollIndicator:false
});

//Server image.
var headerImage = Titanium.UI.createImageView({
	image:'./images/EPL_Logo_M.png'
});

var backgroundView = Titanium.UI.createView({
    backgroundImage:'./images/EntryArea_M.png',
    left:0,
    right:0
});

var IDlabel = Titanium.UI.createLabel({
    text:L("insert_id_label"),
    font:{fontWeight:'bold', fontSize:18/*, fontFamily:'Helvetica Neue'*/},
    color:'white',
    textAlign:'left'
});

var textField = Titanium.UI.createTextField({ 
	keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
	returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
	autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED
});

var connectToServerButton = Titanium.UI.createButton({
	title:L("connect_button"),
	font:{fontWeight:'bold', fontSize:20},
	backgroundImage:'./images/greenButton.png'
});

if(Titanium.Platform.osname=='ipad')
{
    headerImage.top=90;
    headerImage.width=300;
    headerImage.height=300;

    backgroundView.top=535;
    backgroundView.height=360;

    IDlabel.top=110;
    IDlabel.left=10;
    IDlabel.right=10;
    IDlabel.height=40;

    textField.top=150;
    textField.left=10;
    textField.right=10;
    textField.height=40;

    connectToServerButton.top=200;
    connectToServerButton.left=10;
    connectToServerButton.right=10;
    connectToServerButton.height=40;
}
else
{
    headerImage.top=50;
    headerImage.width=150;
    headerImage.height=150;

    backgroundView.top=250;
    backgroundView.height=150;

    IDlabel.top=10;
    IDlabel.left=5;
    IDlabel.right=5;
    IDlabel.height=40;

    textField.height=40;
    textField.top=45;
    textField.left=5;
    textField.right=5;

    connectToServerButton.left=5;
    connectToServerButton.right=5;
    connectToServerButton.height=40;
    connectToServerButton.top=90;
}

backgroundView.add(IDlabel);
backgroundView.add(textField);
backgroundView.add(connectToServerButton);

scrollview.add(headerImage);
scrollview.add(backgroundView);
