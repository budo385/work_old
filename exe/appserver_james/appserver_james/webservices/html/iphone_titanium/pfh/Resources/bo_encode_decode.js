//------------------STRING EN/DECODER------------------------------------
var AMP = '&';
var rawEntity = [ "&",      "<",    ">",     "\'",     "\""];
var xmlEntity = [ "&amp;", "&lt;", "&gt;",  "&apos;", "&quot;"];

//xml- string from server to decode
//returns decoded string
function xmlDecode(xml)
{
	// check if no AMP there is no need to decode
	var pos=xml.indexOf(AMP);
	if(pos<0) {
		return xml;
	}
	var byteDecoded=xml;
	// iterate through list backwards: its very important to pars &amp last
	for (var iEntity=4; iEntity>=0; iEntity--) 
	{
		byteDecoded = byteDecoded.replace(xmlEntity[iEntity],rawEntity[iEntity]);
	}

	return byteDecoded;
};

//raw- string to encode for sending to the server
//returns encoded string
function xmlEncode(raw)
{
	var strEncoded=raw;

	// iterate through list forwad: its very important to encode &amp first
	if(strEncoded.indexOf("&")>=0)
	{
		strEncoded = strEncoded.replace("&","&amp;"); 
	}
	if(strEncoded.indexOf('<')>=0){
		strEncoded = strEncoded.replace("<","&lt;");
	}
	if(strEncoded.indexOf(">")>=0){
		strEncoded = strEncoded.replace(">","&gt;");
	}
	if(strEncoded.indexOf("\'")>=0){
		strEncoded = strEncoded.replace("\'","&apos;");
	}
	if(strEncoded.indexOf("\"")>=0){
		strEncoded =strEncoded.replace("\"","&quot;");
	}

	//	for (uint iEntity=2; iEntity<5; ++iEntity)
	//	  strEncoded.replace(rawEntity[iEntity],xmlEntity[iEntity]);

	return strEncoded;
};
