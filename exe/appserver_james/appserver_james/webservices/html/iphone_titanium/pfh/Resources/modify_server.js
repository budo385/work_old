Titanium.include('./database_calls.js','./background_orientation.js');

var modifyServerWin = Titanium.UI.currentWindow;
modifyServerWin.title=L("delete_servers");
modifyServerWin.backButtonTitle=L("back");

// initialize to some modes
modifyServerWin.orientationModes = [
	Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var deleteButton = Titanium.UI.createButton({
    title:L("delete"),
    backgroundColor:'#ff0000',
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

modifyServerWin.rightNavButton=deleteButton;

//------------------
//Table view.
//------------------
//Add items.
var footerRow = Titanium.UI.createView();
footerRow.height=90;

var data = [];

/*var selectServerLabel = Titanium.UI.createLabel({
	text:'Select Servers to Delete:',
	font:{fontWeight:'bold', fontSize:16},
	left:10,
	height:30,
	top:10,
	color:'white',
	textAlign:'left'});

var headerRow = Titanium.UI.createView();
headerRow.height=40;
headerRow.add(selectServerLabel);*/

//Upper table view.
var tableView = Titanium.UI.createTableView({
	data:data,
	top:0,
	rowHeight:40.00,
	//headerView:headerRow,
	//headerTitle:'Select Servers to Delete:',
	footerView:footerRow,
	editable:true,
	backgroundColor:'transparent',
	style:Titanium.UI.iPhone.TableViewStyle.GROUPED
});


// add delete event listener
tableView.addEventListener('delete',function(e)
{
    var serverRowID = e.rowData.serverRowID;
    var resultSet;
    resultSet = deleteServer(serverRowID);
    resultSet.close();
});

deleteButton.addEventListener('click', function()
{
	//modifyServerWin.setRightNavButton(cancel);
	tableView.editing = true;
});

//Window open event.
modifyServerWin.addEventListener('open', function()
{
    //Get server list.
    var serverList;
    serverList = getServers();
    
    var i = 0;
    while (serverList.isValidRow())
    {
        var row = Ti.UI.createTableViewRow();
        row.height=95;
        var nameLabel = Ti.UI.createLabel({
        height:45,
        top:0,
       	color:'white',
        left:95
        });	
        var idLabel = Ti.UI.createLabel({
        height:40,
        top:45,
       	color:'white',
        left:95
        });
        var imageView = Ti.UI.createImageView({
        height:75,
        width:75,
        top:10,
       	color:'white',
        left:10
        });
        
        //Row views.
        nameLabel.text = serverList.fieldByName('slst_name');
	Ti.API.info(nameLabel.text);
        idLabel.text = serverList.fieldByName('slst_server_id');
        var image64 = serverList.fieldByName('slst_image');
        if(image64.length == 0)
        {
            imageView.image = './images/user75.png';
        }
        else
        {
            imageView.image = Titanium.Utils.base64decode(image64);
        }
        
        //Row data.
        row.serverRowID = serverList.fieldByName('slst_id');
        
        row.add(nameLabel);
        row.add(idLabel);
        row.add(imageView);
        data.push(row);
        serverList.next();
        i++;
    }
    
    serverList.close();
    tableView.data = data;
});

modifyServerWin.add(tableView);