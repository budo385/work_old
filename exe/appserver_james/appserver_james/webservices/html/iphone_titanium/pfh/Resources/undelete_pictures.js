Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');

var undeletePictures = Titanium.UI.currentWindow;
undeletePictures.backButtonTitle=L("catalog");
undeletePictures.title=L("edit");

var previousWindows = undeletePictures.previousWindows;
previousWindows.push(undeletePictures);

var selectedUndeletePictures=[];
var undeleteImageID=0;
var undeleteErrorCount=0;

var undeleteProgressBar=Titanium.UI.createProgressBar({
	width:Titanium.Platform.displayCaps.platformWidth,
	min:0,
	height:70,
	bottom:0,
	value:0,
	color:'#fff',
	font:{fontSize:14, fontWeight:'bold'},
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	backgroundColor:'#0F3752',
	zIndex:10
});

undeletePictures.add(undeleteProgressBar);

var alertUndeleteDialog = Titanium.UI.createAlertDialog({
	title:L("undelete"),
	message:L("undelete_aler_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
});

function onUndeleteDelete()
{
	if(selectedUndeletePictures.length==0)
	{
	    alertUndeleteDialog.show();
	    return;
	}

	undeleteImageID=0;
	undeleteProgressBar.max=selectedUndeletePictures.length;
	undeleteProgressBar.message=L("undeleting")+' '+(undeleteImageID+1)+' '+L("of")+' '+selectedUndeletePictures.length;
	undeleteProgressBar.value=undeleteImageID+1;
	undeletePictures.fireEvent('unDeleteNext');
}

//Navigation bar.
var undeleteButton = Titanium.UI.createButton({
    title:L("undelete"),
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

undeleteButton.addEventListener('click', function(e)
{
	onUndeleteDelete();
});

undeletePictures.rightNavButton=undeleteButton;

// initialize to some modes
undeletePictures.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var undeleteCompleteDialog = Titanium.UI.createAlertDialog({
	title:L("undelete_pictures"),
	message:L("undelete_complete_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
    });

undeleteCompleteDialog.addEventListener('click', function(e)
{
	setTimeout(function(){undeletePictures.close();},nCloseWinTimeout);
});

var headerLabel = Ti.UI.createLabel({
    height:20,
    top:5,
    left:0,
    right:0,
    color:'white',
    textAlign:'center',
    text:'Select Pictures'
});

undeletePictures.add(headerLabel);

var tableView = Titanium.UI.createTableView({
    //data:data,
    top:40,
    rowHeight:60.00,
    backgroundColor:'transparent',
    separatorColor:'white',
    style:Titanium.UI.iPhone.TableViewStyle.GROUPED
}); 

function addRemovePicNameToArray(pictureName,bAdd)
{
    var nIndex=-1;
    for(var i=0;i<selectedUndeletePictures.length;i++)
    {
        if(pictureName==selectedUndeletePictures[i])
        {
            nIndex=i;
        }
    }
    
    if(bAdd)
    {
        if(nIndex<0)
        {
            selectedUndeletePictures.push(pictureName);
        }
    }
    else
    {
        if(nIndex>=0)
        {
            var newArray = [];
            var nCount=0;
            for(var j=0;j<selectedUndeletePictures.length;j++)
            {
                if(j!=nIndex)
                {
                    newArray[nCount]=selectedUndeletePictures[j];
                    nCount++;
                }
            }
            selectedUndeletePictures=newArray;
        }
    }
}

tableView.addEventListener('click',function(e)
{
	var checkImageView = e.row.children[2];

	if(e.rowData.checked)
	{
		checkImageView.image=null;
		e.rowData.checked=0;
		addRemovePicNameToArray(e.rowData.pictureName,false);
	}
	else
	{
		checkImageView.image='./images/Checked.png';
		e.rowData.checked=1;
		addRemovePicNameToArray(e.rowData.pictureName,true);
	}
});

undeletePictures.add(tableView);

function pfCallBackSucess(xml)
{
    hideLoadIndicator(undeletePictures);

    files = xml.documentElement.getElementsByTagName('file');
//Ti.API.info('undelete ' +xhrBOCall.responseText);

   if(files == null)
   {
	return;
   }

    var data = []; 
   // Ti.API.info(files.length);
    for (var i=0;i<files.length;i++) 
	{
        var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
        var thumb = files.item(i).getElementsByTagName('THUMB_BINARY').item(0).text;
        var thumb_binary = Titanium.Utils.base64decode(thumb);
           
        var row = Ti.UI.createTableViewRow();
        row.height=95;
        row.rightImage=null;
        row.checked=0;
	row.pictureName = filename; 
        row.catalogName = undeletePictures.catalogName;
        row.pictureIndex = i;

        var imageView = Ti.UI.createImageView({
            image:thumb_binary
        });
            
        var view = Ti.UI.createView({
            height:75,
            width:75,
            top:10,
            left:10
        });
        
        view.add(imageView);

        var nameLabel = Ti.UI.createLabel({
            height:45,
            top:0,
	    right:30,
	    color:'white',
	    left:95
        });	

        var checkImageView = Ti.UI.createImageView({
            top:0,
	    right:5,
	    width:20,
	    bottom:0
        });

        nameLabel.text=filename;
        
        row.add(view);
        row.add(nameLabel);
        row.add(checkImageView);
        data.push(row); 
    }
		
	tableView.data = data;
    tableViewLoaded=1;
}

function pfCallBackError(xml)
{
    hideLoadIndicator(undeletePictures);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_alert_error"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getUndeletedPictures()
{
    var catalog = undeletePictures.catalogName;
    catalog=xmlEncode(catalog);

    //Get catalog pictures.
    var strPost=
	'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
	'<nPicWidth>75</nPicWidth><nPicHeight>75</nPicHeight><bReturnThumbAsBinary>1</bReturnThumbAsBinary></PARAMETERS></REQUEST>';	
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(undeletePictures);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/hidelist",pfCallBackSucess,pfCallBackError,strPost); 
}

getUndeletedPictures();

function pfDeletePictureSucess(xml)
{
//Ti.API.info('image success');
	undeleteImageID++;
	undeleteProgressBar.message=L("undeleting")+' '+undeleteImageID+' '+L("of")+' '+selectedUndeletePictures.length;
	undeleteProgressBar.value=undeleteImageID;
	undeletePictures.fireEvent('unDeleteNext');
}

function pfDeletePictureError(xml)
{
	undeleteErrorCount++;
	pfDeletePictureSucess(xml);
}

function unDeleteImage()
{
    var catalog = undeletePictures.catalogName;
    catalog=xmlEncode(catalog);

    var fileName = selectedUndeletePictures[undeleteImageID];
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAlias>'+catalog+'</strAlias>'+
    '<strFileName>'+fileName+'</strFileName><nHide>0</nHide></PARAMETERS></REQUEST>';
    
Ti.API.info(strPost);
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(undeletePictures);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/hide",pfDeletePictureSucess,pfDeletePictureError,strPost); 
}

undeletePictures.addEventListener('unDeleteNext', function(e)
{
	if(undeleteImageID<selectedUndeletePictures.length)
	{
		unDeleteImage();
	}
	else
	{
		Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
		hideLoadIndicator(undeletePictures);
		undeleteProgressBar.hide();
		if(undeleteErrorCount==0)
		{
			undeleteCompleteDialog.title=L("undelete_complete_dialog_title");
			undeleteCompleteDialog.message=L("undelete_complete_dialog_message");
			undeleteCompleteDialog.show();
		}
		else
		{
			undeleteCompleteDialog.title=L("undelete_pictures");
			undeleteCompleteDialog.message=(selectedUndeletePictures.length-undeleteErrorCount)+' '+L("of")+' '+selectedUndeletePictures.length+' '+L("undelete_complete_dialog_message_suffix");
			undeleteCompleteDialog.show();
		}
	}
});
