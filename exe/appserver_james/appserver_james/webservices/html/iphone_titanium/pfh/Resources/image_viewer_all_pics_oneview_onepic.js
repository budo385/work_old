Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js','./edit_single_picture.js');

var imageViewer = Titanium.UI.currentWindow;

var previousWindows = imageViewer.previousWindows;
previousWindows.push(imageViewer);

var forcePrevious = false;
var forceNext = false;
var bHasFocus=true;
var hidden=true;
var bContinueSlideShow=false;
var toolbarOpacity=1;
var catalogPageSize=Ti.App.Properties.getString('CATALOG_PAGE_SIZE');
var buttonBarState=0;
var bImageDeleted=false;
var bFromNavigation=false;

var showToolbarAnimationDuration=500;
var hideToolbarAnimationDuration=200;
var a = Titanium.UI.createAnimation();
a.opacity = 0;

var slideshowInterval=Titanium.App.Properties.getString('playslidefor')*1000;
var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');

// initialize to all modes

imageViewer.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

var intervalID=0;
var timeoutID=0;

var scrollView = Titanium.UI.createScrollableView({
    contentWidth:'auto',
    contentHeight:'auto',
    top:0,
    left:0,
    bottom:0,
    right:0,
    zIndex:1
});

imageViewer.add(scrollView);

function functionCallBackError(xml)
{
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("edit_pictures_get_catalog_error_title"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getPictures(catalog,indexFrom,indexTo,pfCallBackSucess,pfCallBackError)
{
    var Width;
    var Height;
    
    if(Titanium.Platform.displayCaps.platformWidth>Titanium.Platform.displayCaps.platformHeight)
    {
	Width=Titanium.Platform.displayCaps.platformWidth;
	Height=Titanium.Platform.displayCaps.platformHeight;
    }
    else
    {
	Width=Titanium.Platform.displayCaps.platformHeight;
	Height=Titanium.Platform.displayCaps.platformWidth;
    }

    catalog=xmlEncode(catalog);

    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
'<nFromN>'+indexFrom+'</nFromN><nToN>'+indexTo+'</nToN><nSortOrder>0</nSortOrder><nPicWidth>'+Width+'</nPicWidth><nPicHeight>'+Height+'</nPicHeight><bKeepAspectRatio>1</bKeepAspectRatio><bReturnThumbAsBinary>0</bReturnThumbAsBinary></PARAMETERS></REQUEST>';

//Ti.API.info('strPost '+strPost);

    showLoadIndicator(imageViewer);
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

var previousButton = Titanium.UI.createButton({
	backgroundImage:'./images/navPrev.png',
    height:19,
    width:23
});

var playSlideShowButton = Titanium.UI.createButton({
    backgroundImage:'./images/navPlay.png',
    height:30,
    width:30
});

var downloadButton = Titanium.UI.createButton({
    backgroundImage:'./images/Download_Icon30-1.png',
    height:30,
    width:30
});

var showOriginalButton = Titanium.UI.createButton({
    backgroundImage:'./images/ShowOriginal_Icon30-3.png',
    height:30,
    width:30
});

var nextButton = Titanium.UI.createButton({
    backgroundImage:'./images/navNext.png',
    height:19,
    width:23
});

var flexSpace = Titanium.UI.createButton({
	systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

var editButton = Titanium.UI.createButton({
    backgroundImage:'./images/Edit_Icon30-4.png',
    height:30,
    width:30
    //systemButton:Titanium.UI.iPhone.SystemButton.COMPOSE
});

editButton.addEventListener('click', function()
{
    addOption();
});

var toolbar = Titanium.UI.createToolbar({
    items:[flexSpace,previousButton,flexSpace,playSlideShowButton,flexSpace,downloadButton,flexSpace,editButton,flexSpace,showOriginalButton,flexSpace,nextButton,flexSpace],
    bottom:0,
    borderTop:true,
    borderBottom:true,
    barColor:'#0F3752',
    opacity:toolbarOpacity,
    zIndex:10
});

var backButton = Titanium.UI.createButton({
    title:L("catalog"),
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

backButton.addEventListener('click', function()
{
    Ti.UI.currentTab.close(imageViewer,{animated:true});
});

var titleLabel = Titanium.UI.createLabel({
    color:'white',
    height:35,
    right:0,
    width:65,
    textAlign:'right'
});

var buttonObjects = [
    {title:"<", width:35},
    {title:L("catalog"), width:60},
    {title:">", width:35}
];

var buttonObjectsTmp = [
    {title:L("catalog"), width:60}
];

var buttonBar = Titanium.UI.createButtonBar({
    labels:buttonObjectsTmp,
    backgroundColor:'#0F3752',
    width:95,
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

function setPageTitleAndNavButtons()
{
    var currentReadableIndex = imageViewer.catalogSubCount*catalogPageSize + scrollView.currentPage+1;
    titleLabel.text = currentReadableIndex+'/'+imageViewer.catalogTotalCount;
    
    if(imageViewer.catalogTotalSubCount==1 || bImageDeleted)
    {
	//Only catalog.
	buttonBarState=0;
	buttonBar.labels=buttonObjectsTmp;
    }
    else
    {
	if(scrollView.currentPage==0 && imageViewer.catalogSubCount!=0)
	{
	    //With previous catalog.
	    buttonBarState=1;
	    var buttons1=[];
	    buttons1.push(buttonObjects[0]);
	    buttons1.push(buttonObjects[1]);
	    buttonBar.labels=buttons1;
	}
	else if((scrollView.currentPage==(catalogPageSize-1) || currentReadableIndex==imageViewer.catalogTotalCount) && ((imageViewer.catalogSubCount+1)!=imageViewer.catalogTotalSubCount))
	{
	    //With next catalog.
	    buttonBarState=2;
	    var buttons2=[];
	    buttons2.push(buttonObjects[1]);
	    buttons2.push(buttonObjects[2]);
	    buttonBar.labels=buttons2;
	}
	else
	{
	    //Only catalog.
	    buttonBarState=0;
	    buttonBar.labels=buttonObjectsTmp;
	}
    }
}

function functionGetPicturesCallBackSucess(xml)
{
//Ti.API.info('start index '+imageViewer.catalogStartIndex);
//Ti.API.info('end index '+imageViewer.catalogEndIndex);
//Ti.API.info('response image viewer ' +xhrBOCall.responseText);

    var files = xml.documentElement.getElementsByTagName('file');
    if(files==null)
    {
	hideLoadIndicator(imageViewer);
	return;
    }
    
    var views=[];
    scrollView.views=views;
	scrollView.currentPage=0;
    //for (var i=imageViewer.catalogStartIndex;i<imageViewer.catalogEndIndex;i++) 
    for (var i=0;i<files.length;i++)
    {
	var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
	var dispName=files.item(i).getElementsByTagName('DISPLAYNAME').item(0).text;
	var thumb_link = REST_WEB_DOCS_URL+files.item(i).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
	
	var imageView = Ti.UI.createImageView({
	    fileName:filename,
	    displayName:dispName,
	    thumbLink:thumb_link,
	    image:thumb_link
	});

	views.push(imageView);
    }
    
    hideLoadIndicator(imageViewer);
    scrollView.views=views;
    scrollView.currentPage=imageViewer.pictureIndex-imageViewer.catalogStartIndex;

//	Ti.API.info('reload catalogSubCount '+imageViewer.catalogSubCount);
//	Ti.API.info('reload catalogTotalSubCount '+imageViewer.catalogTotalSubCount);
//	Ti.API.info('reload pictureIndex '+imageViewer.pictureIndex);
//	Ti.API.info('reload currentPage '+(imageViewer.pictureIndex-imageViewer.catalogStartIndex));
    setPageTitleAndNavButtons();
}

function functionFirstCallBackError(xml)
{
    hideLoadIndicator(imageViewer);

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("edit_pictures_get_catalog_error_title"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
    Ti.UI.currentTab.close(imageViewer,{animated:true});
}

function showPreviousSubCatalog()
{
    var catalogSubCount=imageViewer.catalogSubCount-1;
    var catalogStartIndex=catalogSubCount*catalogPageSize;
    var catalogEndIndex=(catalogSubCount+1)*catalogPageSize-1;
    
    //    scrollView.currentPage=imageViewer.pictureIndex-imageViewer.catalogStartIndex;
    //Ti.API.info('back catalogPageSize '+catalogPageSize);
    //Ti.API.info('back catalogSubCount '+catalogSubCount);
    	//Ti.API.info('back picture index '+catalogEndIndex);
    	//Ti.API.info('back picture index '+catalogStartIndex);
    
    imageViewer.catalogStartIndex = catalogStartIndex;
    imageViewer.catalogEndIndex = catalogEndIndex;
    imageViewer.catalogSubCount = catalogSubCount;
    imageViewer.pictureIndex = catalogEndIndex;
    
    getPictures(imageViewer.catalogName,imageViewer.catalogStartIndex,imageViewer.catalogEndIndex,functionGetPicturesCallBackSucess,functionFirstCallBackError);
}

function showNextSubCatalog()
{
    var catalogSubCount=imageViewer.catalogSubCount+1;
    
    //Ti.API.info('next catalogPageSize '+catalogPageSize);
//    Ti.API.info('next catalogSubCount '+catalogSubCount);
    	//Ti.API.info('next picture index '+catalogEndIndex);
//    	Ti.API.info('next picture index '+catalogStartIndex);

    var catalogStartIndex=catalogSubCount*catalogPageSize;
    var catalogPixCount=imageViewer.catalogTotalCount;
    var catalogEndIndex;
    if((catalogSubCount+1)*catalogPageSize<catalogPixCount)
    {
	    catalogEndIndex=(catalogSubCount+1)*catalogPageSize-1;
    }
    else
    {
	    catalogEndIndex=(catalogSubCount)*catalogPageSize+(catalogPixCount-(catalogSubCount)*catalogPageSize)-1;
    }
    
//    Ti.API.info('next catalogSubCount '+catalogSubCount);
    
    imageViewer.catalogStartIndex = catalogStartIndex;
    imageViewer.catalogEndIndex = catalogEndIndex;
    imageViewer.catalogSubCount = catalogSubCount;
    imageViewer.pictureIndex = catalogStartIndex;
    
    getPictures(imageViewer.catalogName,imageViewer.catalogStartIndex,imageViewer.catalogEndIndex,functionGetPicturesCallBackSucess,functionFirstCallBackError);
}

var navImageView = Ti.UI.createImageView({
    top:0,
    left:0,
    bottom:0,
    right:0,
    width:'auto',
    height:'auto',
    image:'./images/everPics_TitleBar.png',
    zIndex:10
});

buttonBar.addEventListener('click', function(e)
{
    //Ti.API.info('button bar state '+ buttonBarState);
    switch(buttonBarState)
    {
	case 0:
		Ti.UI.currentTab.close(imageViewer,{animated:true});
	  break;
	case 1:
		if(e.index==0)
		{
		    showPreviousSubCatalog();
		}
		else
		{
		    Ti.UI.currentTab.close(imageViewer,{animated:true});
		}
	  break;
	case 2:
		if(e.index==0)
		{
		    Ti.UI.currentTab.close(imageViewer,{animated:true});
		}
		else
		{
		    showNextSubCatalog();
		}
	  break;
    }
});

var navigationToolbar = Titanium.UI.createToolbar({
    items:[buttonBar,flexSpace,titleLabel],
//    items:[backButton,catalogBackButton,flexSpace,catalogNextButton,titleLabel],
    top:0,
    left:0,
    right:0,
    borderTop:true,
    borderBottom:true,
    barColor:'#0F3752',
    opacity:toolbarOpacity,
    zIndex:10
});

navigationToolbar.add(navImageView);

function hideButtons()
{
    timeoutID=0;
    
    toolbar.animate(a);
    a.addEventListener('complete', function(e)
    {
	imageViewer.remove(toolbar);
    });

    a.duration=hideToolbarAnimationDuration;
    navigationToolbar.animate(a);
    a.addEventListener('complete', function(e)
    {
	imageViewer.remove(navigationToolbar);
    });
}

function showButtons()
{
    if(bFromNavigation)
    {
	bFromNavigation=false;
	if(timeoutID!=0)
	{
	    clearTimeout(timeoutID);
	    timeoutID=0;
	}
    }
    else
    {
	if(timeoutID!=0)
	{
	    clearTimeout(timeoutID);
	    timeoutID=0;
	    hideButtons();
	    if(intervalID!=0)
	    {
		clearInterval(intervalID);
		intervalID=0;
	    }
	    return;
	}
    }
    
    if(intervalID!=0)
    {
	clearInterval(intervalID);
	intervalID=0;
    }
    
    a.duration=showToolbarAnimationDuration;
    toolbar.opacity=toolbarOpacity;
    imageViewer.add(toolbar);
    navigationToolbar.opacity=toolbarOpacity;
    imageViewer.add(navigationToolbar);

    timeoutID=setTimeout(hideButtons,4000);
}

function onPlaySlideShow()
{
    if((scrollView.currentPage+1)<scrollView.views.length)
    {
        scrollView.scrollToView(scrollView.currentPage+1);
    }
    else
    {
	scrollView.currentPage=0;
    }
}

previousButton.addEventListener('click', function()
{
    bFromNavigation=true;
    showButtons();
    
    if(scrollView.currentPage>0)
    {
	scrollView.scrollToView(scrollView.currentPage-1);
    }
});

function openFullImageView()
{
    var views = [];
    views=scrollView.views;
    var catalog = imageViewer.catalogName;
    var fileName = views[scrollView.currentPage].fileName;

    var w = Titanium.UI.createWindow({
        url:'./full_image_viewer.js',
        fileName:fileName,
	barColor:'#0F3752',
        catalogName:catalog,
        navBarHidden:false,
	previousWindows:previousWindows//,
	//backButtonTitle:'Catalog Pictures'
    });

    w.canDownload=imageViewer.canDownload;
    w.canUpload=imageViewer.canUpload;
    w.canDelete=imageViewer.canDelete;
    w.canInvite=imageViewer.canInvite;
    w.canSendEmail=imageViewer.canSendEmail;
    //w.fullscreen=true;

    w.open({modal:true,modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_CROSS_DISSOLVE});
    //Titanium.UI.currentTab.open(w,{transition:Titanium.UI.iPhone.AnimationStyle.CURL_DOWN});
}

showOriginalButton.addEventListener('click', function()
{
    openFullImageView();
});

function completeShow()
{
    var completeDialog = Titanium.UI.createAlertDialog({
        title:L("image_viewer_download_complete_dialog_title"),
        buttonNames:[L("ok")],
        cancel:0
    });

    completeDialog.show();
}

function pfGetPictureSucess(xml)
{

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_error_alert_message"),
        message: strLastBO_ErrorText
    });

    var xhr = Titanium.Network.createHTTPClient();
    xhr.validateSecureCertificate = false;
    
    xhr.onerror = function(e)
    {
	hideLoadIndicator(imageViewer);
	alertDialogBOError.show();
    };
    xhr.onload = function()
    {
	Titanium.Media.saveToPhotoGallery(this.responseData);
	hideLoadIndicator(imageViewer);
    
	setTimeout(completeShow,1000);
    };

    var files = xml.documentElement.getElementsByTagName('file');
    var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
    var thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');

    xhr.open('GET',thumb_link);
    // send the data
    xhr.send();
}

function pfGetPictureError(xml)
{
    hideLoadIndicator(imageViewer);

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_error_alert_message"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getImage()
{
    var views = [];
    views=scrollView.views;
    var catalog = imageViewer.catalogName;
    catalog=xmlEncode(catalog);
    var fileName = views[scrollView.currentPage].fileName;
    
    var strFile='<list>';
    strFile+='<row><strFileName>'+fileName+'</strFileName></row>';
    strFile+='</list>';
    
    //Get catalog pictures.
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+strFile+'<nReturnFull>1</nReturnFull><nPicWidth>320</nPicWidth><nPicHeight>480</nPicHeight><bReturnThumbAsBinary>0</bReturnThumbAsBinary><bSkipAccess>0</bSkipAccess></PARAMETERS></REQUEST>';
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(imageViewer);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/download",pfGetPictureSucess,pfGetPictureError,strPost);
}

downloadButton.addEventListener('click', function()
{
    if(imageViewer.canDownload==0)
    {
	var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
        });
        alertDialog.show();
	return;
    }

    getImage();
});

function onPlaySlideShowButton()
{
    intervalID=setInterval(onPlaySlideShow, slideshowInterval);
    //Ti.API.info('intervalID '+intervalID);
}

playSlideShowButton.addEventListener('click', function()
{
    onPlaySlideShowButton();
    hideButtons();
});

scrollView.addEventListener('scroll', function(e)
{
    setPageTitleAndNavButtons();
});

scrollView.addEventListener('singletap', function()
{
    showButtons();
});
scrollView.addEventListener('doubletap', function()
{
    openFullImageView();
});
/*imageViewer.addEventListener('singletap', function()
{
    showButtons();
});
imageViewer.addEventListener('doubletap', function()
{
    openFullImageView();
});*/

nextButton.addEventListener('click', function()
{
    bFromNavigation=true;
    showButtons();

    if((scrollView.currentPage+1)<scrollView.views.length)
    {
	scrollView.scrollToView(scrollView.currentPage+1);
    }
});

   //Ti.API.info(imageViewer.catalogName);
getPictures(imageViewer.catalogName,imageViewer.catalogStartIndex,imageViewer.catalogEndIndex,functionGetPicturesCallBackSucess,functionFirstCallBackError);

imageViewer.addEventListener('open', function()
{
    showButtons();
//    Ti.API.info('imageViewer.catalogStartIndex '+imageViewer.catalogStartIndex);
//    Ti.API.info('imageViewer.catalogEndIndex '+imageViewer.catalogEndIndex);
});

imageViewer.addEventListener('focus', function()
{
    scrollView.currentPage=scrollView.currentPage;
});

imageViewer.addEventListener('close', function()
{
    if(timeoutID!=0)
    {
        clearTimeout(timeoutID);
        timeoutID=0;
    }
    
    if(intervalID!=0)
    {
	clearInterval(intervalID);
	intervalID=0;
    }
});

// orientation change listener
Ti.Gesture.addEventListener('orientationchange',function(e)
{
    scrollView.currentPage=scrollView.currentPage;
});
