Titanium.include('./load_indicator.js','./bo_call_new.js');

var win = Titanium.UI.currentWindow;
var catName;
var alertUnpublishDialog = Titanium.UI.createAlertDialog({
	title:L("unpublish_catalog_alert_title"),
	message:L("unpublish_catalog_alert_message")+' '+catName+'?',
	buttonNames:[L("ok"),L("cancel")],
	cancel:1
});

function pfUnpublishCatalogSucess(xml)
{
	hideLoadIndicator(win);
	Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
	var alertDialog = Titanium.UI.createAlertDialog({
		title:L("unpublish_catalog_alert_title"),
		message:L("unpublish_catalog_success_alert_message"),
		buttonNames:[L("ok")],
		cancel:0
	});
	alertDialog.show();
	alertDialog.addEventListener('click',function(e)
	{
		win.fireEvent('focus');
	});
}

function pfUnpublishCatalogError(xml)
{
	hideLoadIndicator(win);
	Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
	var alertRenameDialog = Titanium.UI.createAlertDialog({
		title:L("unpublish_catalog_alert_title"),
		message:L("unpublish_catalog_error_alert_message"),
		buttonNames:[L("ok")],
		cancel:0
	});
	alertRenameDialog.show();
}

function unpublishCatalog(catalogName)
{
	catName=catalogName;

	alertUnpublishDialog.message=L("unpublish_catalog_alert_message")+' '+catName+'?';
	alertUnpublishDialog.show();
}

function unpublish()
{
	var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strCatalog>'+xmlEncode(catName)+'</strCatalog></PARAMETERS></REQUEST>';
	
	var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
	showLoadIndicator(win);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/unpublish_client",pfUnpublishCatalogSucess,pfUnpublishCatalogError,strPost); 
}

alertUnpublishDialog.addEventListener('click',function(e)
{
	if(e.index==0)
	{
	    unpublish();
	}
	else
	{
	    win.close();
	}
});
