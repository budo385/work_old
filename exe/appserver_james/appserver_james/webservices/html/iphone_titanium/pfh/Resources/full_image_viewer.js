Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');
var fullImageViewer = Titanium.UI.currentWindow;
fullImageViewer.titleControl = null;
fullImageViewer.titleImage = './images/everPics_TitleBar.png';

//fullImageViewer.backButtonTitle = 'Catalog Pictures';

var previousWindows = fullImageViewer.previousWindows;
previousWindows.push(fullImageViewer);

// initialize to all modes
fullImageViewer.orientationModes = [
	Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var toolbarOpacity=1;
var a = Titanium.UI.createAnimation();
a.duration = 500;
a.opacity = 0;

var completeDialog = Titanium.UI.createAlertDialog({
	title:L("image_viewer_download_complete_dialog_title_0"),
	buttonNames:[L("ok")],
	cancel:0
    });

//Navigation bar.
var downloadButton = Titanium.UI.createButton({
    title:L("download"),
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

fullImageViewer.rightNavButton=downloadButton;

var backButton = Titanium.UI.createButton({
    title:L("image_viewer_back_button"),
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

backButton.addEventListener('click', function()
{
    fullImageViewer.close();
});

fullImageViewer.setLeftNavButton(backButton);

/*var flexSpace = Titanium.UI.createButton({
	systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

var navImageView = Ti.UI.createImageView({
    top:0,
    left:0,
    bottom:0,
    right:0,
    width:'auto',
    height:'auto',
    image:'./images/everPics_TitleBar.png',
    zIndex:10
});

var navigationToolbar = Titanium.UI.createToolbar({
    items:[backButton,flexSpace,downloadButton],
    top:0,
    left:0,
    right:0,
    borderTop:true,
    borderBottom:true,
    barColor:'#0F3752',
    opacity:toolbarOpacity,
    zIndex:10
});

navigationToolbar.add(navImageView);*/

function fDownloadPictureSucess(xml)
{
    var files = xml.documentElement.getElementsByTagName('file');
    var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
    var thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');

    var xhr = Titanium.Network.createHTTPClient();
    xhr.error = function()
    {
	hideLoadIndicator(fullImageViewer);
    };
    xhr.onload = function()
    {
        Titanium.Media.saveToPhotoGallery(xhr.responseData);
	hideLoadIndicator(fullImageViewer);
	completeDialog.show();
    };
    // open the client
    xhr.open('GET',thumb_link);
    
    // send the data
    xhr.send();
}

function fDownloDPictureError(xml)
{
    hideLoadIndicator(fullImageViewer);

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_error_alert_message"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function downloadImage()
{
    var catalog = fullImageViewer.catalogName;
    catalog=xmlEncode(catalog);
    var fileName = fullImageViewer.fileName;
    
    var strFile='<list>';
    strFile+='<row><strFileName>'+fileName+'</strFileName></row>';
    strFile+='</list>';

    var Width;
    var Height;
    
    if(Titanium.Platform.displayCaps.platformWidth>Titanium.Platform.displayCaps.platformHeight)
    {
	Width=Titanium.Platform.displayCaps.platformWidth;
	Height=Titanium.Platform.displayCaps.platformHeight;
    }
    else
    {
	Width=Titanium.Platform.displayCaps.platformHeight;
	Height=Titanium.Platform.displayCaps.platformWidth;
    }
    
    //Get catalog pictures.
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+strFile+
    '<nReturnFull>0</nReturnFull><nPicWidth>'+Width+'</nPicWidth><nPicHeight>'+Height+
    '</nPicHeight><bReturnThumbAsBinary>0</bReturnThumbAsBinary><bSkipAccess>0</bSkipAccess></PARAMETERS></REQUEST>';
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(fullImageViewer);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/download",fDownloadPictureSucess,fDownloDPictureError,strPost);
}

downloadButton.addEventListener('click', function()
{
	if(fullImageViewer.canDownload==0)
	{
		var alertDialog = Ti.UI.createAlertDialog({
		    title:L("access_right_title"),
		    message: strLastBO_ErrorText
		});
		alertDialog.show();
		return;
	}

	downloadImage();
});

var imageView = Ti.UI.createImageView({
	width:'auto',
	height:'auto'
});

var currentImageIndex = fullImageViewer.pictureIndex;
var timeoutID=0;

var scrollView = Titanium.UI.createScrollView({
    top:0,
    left:0,
    right:0,
    bottom:0,
    showVerticalScrollIndicator:true,
    showHorizontalScrollIndicator:true,
    maxZoomScale:10.0,
    minZoomScale:0.1,
    contentWidth:'auto',
    contentHeight:'auto'
});

/*function hideButtons()
{
    timeoutID=0;
    
    navigationToolbar.animate(a);
    a.addEventListener('complete', function(e)
    {
	fullImageViewer.remove(navigationToolbar);
	scrollView.top=0;
	scrollView.left=0;
	scrollView.right=0;
	scrollView.bottom=0;
    });
}

function showButtons()
{
    if(timeoutID!=0)
    {
        clearTimeout(timeoutID);
        timeoutID=0;
    }
    
    navigationToolbar.opacity=toolbarOpacity;
    fullImageViewer.add(navigationToolbar);

    timeoutID=setTimeout(hideButtons,4000);
}*/

function hideButtons()
{
    timeoutID=0;
    fullImageViewer.hideNavBar();
}

function showButtons()
{
    if(timeoutID!=0)
    {
        clearTimeout(timeoutID);
        timeoutID=0;
    }
    
    fullImageViewer.showNavBar();
    timeoutID=setTimeout(hideButtons,4000);
}

showButtons();

function pfGetPictureSucess(xml)
{
/*	var html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">'+
	'<html>'+
	'<head>'+
	'<meta http-equiv="Content-Type"  content="text/html; charset=utf-8"/>'+
	'<meta name="apple-mobile-web-app-status-bar-style" content="black"/>'+
	'<meta name="apple-mobile-web-app-capable" content="yes"/>'+
	'<meta name="viewport" content="minimum-scale=0.1, width=device-width, maximum-scale=10, user-scalable=yes"/>'+
	'<meta name="description" content="everPICs PRO ONLINE - by Helix Business Soft AG, CH-5405 Baden-D&auml;ttwil"/>'+
	'</head>'+
	'<body>'+
	'<img src="[Image]" alt="Lean">'+
	'</body>'+
	'</html>'
*//*	var html ='<html>'+
	'<head>'+
	'</head>'+
	'<body>'+
	'<img src="[Image]">'+
	'</body>'+
	'</html>';*/

	//Ti.API.info('response image viewer ' +xhrBOCall.responseText);
	
	var files = xml.documentElement.getElementsByTagName('file');
	if(files==null)
	{
	    fullImageViewer.close();
	}
	
	var fileName= fullImageViewer.fileName;
	var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
	//var thumb_link = REST_WEB_DOCS_URL+files.item(0).getElementsByTagName('THUMB_LINK').item(0).text+'?web_session='+Titanium.App.Properties.getString('web_session');
	var thumb_binary=files.item(0).getElementsByTagName('THUMB_BINARY').item(0).text;
	thumb_binary=Titanium.Utils.base64decode(thumb_binary);

/*var webview = Titanium.UI.createWebView();
webview.backgroundImage = './images/PooltableBlue256.png';
webview.backgroundColor='red';
fullImageViewer.add(webview);*/
//webview.top=0;
//webview.bottom=0;
//webview.left=0;
//webview.right=0;

	//html=html.replace("[Image]", thumb_link);

//Ti.API.info(html);

//webview.url=html;

//	fullImageViewer.add(webview);

	
	scrollView.add(imageView);
	fullImageViewer.add(scrollView);
	imageView.image=thumb_binary;
	hideLoadIndicator(fullImageViewer);
}

function pfGetPictureError(xml)
{
    hideLoadIndicator(fullImageViewer);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("download_error_alert_message"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getImage()
{
    var catalog = fullImageViewer.catalogName;
    catalog=xmlEncode(catalog);
    var fileName= fullImageViewer.fileName;
    var strFile='<list>';
    strFile+='<row><strFileName>'+fileName+'</strFileName></row>';
    strFile+='</list>';
   
    var Width;
    var Height;
    
    if(Titanium.Platform.displayCaps.platformWidth>Titanium.Platform.displayCaps.platformHeight)
    {
	Width=Titanium.Platform.displayCaps.platformWidth;
	Height=Titanium.Platform.displayCaps.platformHeight;
    }
    else
    {
	Width=Titanium.Platform.displayCaps.platformHeight;
	Height=Titanium.Platform.displayCaps.platformWidth;
    }
    
    //Get catalog pictures.
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+
    '</strAliasDirectory>'+strFile+'<nReturnFull>1</nReturnFull><nPicWidth>'+Width+'</nPicWidth><nPicHeight>'+Height+'</nPicHeight>'+	
	'<bReturnThumbAsBinary>1</bReturnThumbAsBinary><bSkipAccess>1</bSkipAccess></PARAMETERS></REQUEST>';	
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(fullImageViewer);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/picture/download",pfGetPictureSucess,pfGetPictureError,strPost); 
}

getImage();

// orientation change listener
imageView.addEventListener('singletap', function()
{
    showButtons();
});

imageView.addEventListener('doubletap', function()
{
    fullImageViewer.close();
});
