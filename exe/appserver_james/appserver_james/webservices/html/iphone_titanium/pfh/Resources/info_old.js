//
// This file implements a single window to display all "help" related displays 
// (Overview, Manual, Manual Install, Manual Master, Manual Visit pages)
//
Titanium.include('./background_orientation.js');

//init main window properties
var w = Titanium.UI.currentWindow;
w.titleControl = null;
w.titleImage = 'images/everPics_TitleBar.png';
w.backButtonTitle=L("back");
w.orientationModes = [
	Titanium.UI.PORTRAIT
];

var previousWindows = w.previousWindows;
previousWindows.push(w);

var pathResources = Titanium.Filesystem.resourcesDirectory; 

//
// Create all widgets, only some are visible at a time
// (switching between 3 main views)
//

var buttonBar = Titanium.UI.createButtonBar({
	labels:[L("overview_button_label"),L("manual_button_label"),L("more_button_label")],
	height:30,
	top:10
});

//possible top level views placed below the button bar
var viewTopWebViewInfo = Titanium.UI.createView({
	top:40
}); 	//webview for "overview" topic

var viewTopTableView = Titanium.UI.createView({
	top:40
});		//table view for "manual" topics menu

var viewTopWebViewManual = Titanium.UI.createView({
	top:40
});	//webview for 3 "manual" topics (different layout than "Overview")

//Manual table view
var tableView = Titanium.UI.createTableView({
	top:0,
	rowHeight:45.00,
	headerTitle:L("read_how_to_header_title"),
	style:Titanium.UI.iPhone.TableViewStyle.GROUPED
});

//Manual pages Name label
var nameLabel = Titanium.UI.createLabel({
	height:35,
	top:0,
	textAlign:'center'
});

var sokratesLabel1 = Titanium.UI.createLabel({
	text:L("powered_by_sokrates"),
	height:30,
	bottom:0,
	textAlign:'center'
});

var sokratesLabel2 = Titanium.UI.createLabel({
	text:L("powered_by_sokrates"),
	height:30,
	bottom:0,
	textAlign:'center'
});

var sokratesLabel3 = Titanium.UI.createLabel({
	text:L("powered_by_sokrates"),
	height:30,
	bottom:0,
	textAlign:'center'
});

var webviewInfo = Titanium.UI.createWebView({
	url: pathResources + './html/info.html',
	height:330,
	top:20,
	border: true,
	borderRadius: 15,
	borderWidth: 1,
	borderColor: 'white'
});

var webviewManual = Titanium.UI.createWebView({
	height:300,
	top:40,
	border: true,
	borderRadius: 15,
	borderWidth: 1,
	borderColor: 'white'
});

//initialization
buttonBar.addEventListener('click',function(ev)
{
	if(ev.index == 2)
	{
		return;	//'More' page not yet implemented
	}
    
	var page;
	if(ev.index == 0)
	{
		viewTopTableView.hide();
		viewTopWebViewManual.hide();		
		viewTopWebViewInfo.show();
	}
	else if(ev.index == 1)
	{
		viewTopWebViewInfo.hide();
		viewTopWebViewManual.hide();		
		viewTopTableView.show();
	}
});

w.addEventListener('open', function()
{
	var data = [];

	// fill the Table View with links to individual manual pages
	var row = Ti.UI.createTableViewRow();
	row.hasChild=true;
	var nameLabel1 = Ti.UI.createLabel({
		height:45,
		top:0,
		left:15,
		text:L("install_own_server_label")
	});
	row.add(nameLabel1);
	data.push(row);

	nameLabel1.addEventListener('click', function(e)
	{
		webviewManual.url = pathResources + './html/manual_install.html';
		nameLabel.text=L("install_own_server_label");

		viewTopWebViewInfo.hide();
		viewTopTableView.hide();
		viewTopWebViewManual.show();
	});

	//
	row = Ti.UI.createTableViewRow();
	row.hasChild=true;
	var nameLabel2 = Ti.UI.createLabel({
		height:45,
		top:0,
		left:15,
		text:L("master_access_server_label")
	});	
	row.add(nameLabel2);
	data.push(row);

	nameLabel2.addEventListener('click', function(e)
	{
		webviewManual.url = pathResources + './html/manual_master.html';
		nameLabel.text =L("master_access_server_label");

		viewTopWebViewInfo.hide();
		viewTopTableView.hide();
		viewTopWebViewManual.show();
	});

	//
	row = Ti.UI.createTableViewRow();
	row.hasChild=true;
	var nameLabel3 = Ti.UI.createLabel({
		height:45,
		top:0,
		left:15,
		text:L("visit_friend_catalog_label")
	});	
	row.add(nameLabel3);
	data.push(row);

	nameLabel3.addEventListener('click', function(e)
	{
		webviewManual.url = pathResources + './html/manual_visit.html';
		nameLabel.text =L("visit_friend_catalog_label");

		viewTopWebViewInfo.hide();
		viewTopTableView.hide();
		viewTopWebViewManual.show();
	});

	//init table view data
	tableView.data = data;
});

 
//initial layout
w.add(buttonBar);
w.add(viewTopWebViewInfo);
w.add(viewTopTableView);
w.add(viewTopWebViewManual);

//create view contents
viewTopWebViewInfo.add(webviewInfo);
viewTopWebViewInfo.add(sokratesLabel1);
viewTopWebViewInfo.show();
        
viewTopTableView.add(tableView);
viewTopTableView.add(sokratesLabel2);
viewTopTableView.hide();

viewTopWebViewManual.add(nameLabel);
viewTopWebViewManual.add(webviewManual);
viewTopWebViewManual.add(sokratesLabel3);
viewTopWebViewManual.hide();