Titanium.include('./bo_encode_decode.js','./sha256.js','./config_titanium.js','bo_call_before_send_call.js');
//Locals
var nLastBO_ErrorCode=0;
var strLastBO_ErrorText='';
//Login callback.
var functCallBackSucess_Login=null;
var functCallBackError_Login=null;

//--------------------------------------------------------------------
//				LOGIN HANDLER
//--------------------------------------------------------------------
// Logins on the server, makes async call, calls pfCallBack function when finish
// nLastBO_ErrorCode and strLastBO_ErrorText contain last error: if nLastBO_ErrorCode <> 0 then error
// if Login failed nLastBO_ErrorCode=1000
// pfCallBack receives http body data if any

//user			- username
//pass			- password
//pfCallBack	- function called when call is finished, function must accept data string result and test nLastBO_ErrorCode to see if call succeed, if nLastBO_ErrorCode=1000 -> login failed

function BO_GenerateLoginRequest(user,pass,progCode,progVer,clientID,platform)
{
	var exdate=new Date();
	var strClientNonce=user+":"+exdate.toLocaleTimeString();
	strClientNonce=SHA256(strClientNonce);
	var bytePassHash=user+":"+pass;
	bytePassHash=SHA256(bytePassHash);
	var authToken=user+":"+bytePassHash+":"+strClientNonce;
	authToken=SHA256(authToken);
	
	var strBody ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";
	strBody +="<strUserName>"+user+"</strUserName>";
	strBody +="<strAuthToken>"+authToken+"</strAuthToken>";
	strBody +="<strClientNonce>"+strClientNonce+"</strClientNonce>";
	strBody +="<nProgCode>"+progCode+"</nProgCode>";
	strBody +="<strProgVer>"+progVer+"</strProgVer>";
	strBody +="<strClientID>"+clientID+"</strClientID>";
	strBody +="<strPlatform>"+platform+"</strPlatform>";
	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";
	
	//Ti.API.info(strBody);
	
	return strBody;
}

function BO_ParseLoginResponse(xml_body)
{
	var xdom = xml_body.documentElement;

	if(xdom.getElementsByTagName("Session") != null)
	{
		var session = xdom.getElementsByTagName("Session").item(0).text;
		return session;
	}

	return "";
}

function BO_GenerateLogoutRequest(strSessionID)
{
	var strBody ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";
	strBody +="<strSessionID>"+strSessionID+"</strSessionID>";
	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";
	
	return strBody;
}

function BO_ParseLoginError(xml_response)
{
	if(xml_response==null)
	{
		return;
	}
	xdom = xml_response.documentElement;
	nLastBO_ErrorCode = xdom.getElementsByTagName('ERROR_CODE').item(0).text;
	strLastBO_ErrorText = xdom.getElementsByTagName('ERROR_TEXT').item(0).text;
}

function BO_OnLogin_error(xhr) 
{
//Ti.API.info('2222 '+xhr.responseText);
	BO_ParseLoginError(xhr.responseXML);
	Titanium.App.Properties.removeProperty('web_session');
	functCallBackError_Login(xhr.responseXML);
}

function BO_OnLogin_success(xhr)
{
	var strSession=BO_ParseLoginResponse(xhr.responseXML);
	
	if(strSession=='')
	{
		BO_OnLogin_error(xhr);
		return;
	}
	else
	{
		nLastBO_ErrorCode=0;
		strLastBO_ErrorText='';
	}

	if(strSession != '')
	{
		Titanium.App.Properties.setString('web_session', strSession);
	}

	functCallBackSucess_Login(xhr.responseXML);
}

function BO_Login(user,pass,pfCallBackSucess,pfCallBackError)
{
	Titanium.App.Properties.removeProperty('web_session');
	var PFH_program_version = PROGRAM_VERSION_MAJOR+"."+PROGRAM_VERSION_MINOR+"."+PROGRAM_VERSION_REVISION;
	var PFH_program_platform_UID = ""; 
	var PFH_client_identity;
	if(Ti.App.Properties.hasProperty('clientID'))
	{
	    PFH_client_identity = Ti.App.Properties.getString('clientID');
	}
	else
	{
	    PFH_client_identity = '';
	}

	var xhr = Titanium.Network.createHTTPClient();
	var programCode=Ti.App.Properties.getString('PROGRAM_CODE');
	var strXMLRequest=BO_GenerateLoginRequest(user,pass,programCode,PFH_program_version,PFH_client_identity,Ti.Platform.osname);
	functCallBackSucess_Login=pfCallBackSucess;
	functCallBackError_Login=pfCallBackError;
	xhr.onerror = function()
	{
		BO_OnLogin_error(xhr);
	};
	xhr.onload = function()
	{
		BO_OnLogin_success(xhr);
	};
	xhr.validateSecureCertificate = false;
	xhr.open("POST",Titanium.App.Properties.getString('REST_LOGIN'));
	xhr.send(strXMLRequest);
}

function BO_IsLogged()
{
	var strSession=Titanium.App.Properties.getString('web_session');
	if(Titanium.App.Properties.hasProperty('web_session'))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function BO_OnLogout_success(xhr) 
{
/*	
    var data = xhrLogin.responseXML;
	var strSession=BO_ParseLoginResponse(data);

	if(data.length===0)
	{
		nLastBO_ErrorCode=1000;
		strLastBO_ErrorText="Logout failed! Invalid username or password!";
	}
	else
	{
		nLastBO_ErrorCode=0;
		strLastBO_ErrorText='';
	}
    
    Titanium.App.Properties.setString('web_session', strSession);

	functCallBackSucess_Login(xhrLogin.responseXML);
*/
}

function BO_OnLogout_error(xhr) 
{
/*
	BO_ParseError(xhrLogin,xhrLogin.responseText);
    Titanium.App.Properties.removeProperty('web_session');
	
	functCallBackError_Login(xhrLogin.responseXML);
*/
}

function BO_LogOut(strURL)
{
	var strSession=Titanium.App.Properties.getString('web_session');

	var xhr = Titanium.Network.createHTTPClient();
	var strXMLRequest=BO_GenerateLogoutRequest(strSession);
	xhr.onerror = function()
	{
		BO_OnLogout_error(xhr);
	};
	xhr.onload = function()
	{
		BO_OnLogout_success(xhr);
	};

	xhr.validateSecureCertificate = false;
	xhr.open("POST",strURL);
	
	BO_Call_beforeSend(xhr);
	xhr.send(strXMLRequest);
	
	Titanium.App.Properties.removeProperty('web_session');
}
