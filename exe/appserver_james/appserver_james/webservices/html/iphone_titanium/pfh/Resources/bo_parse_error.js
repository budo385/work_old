//Locals
Titanium.include('./win_close_timeout.js');
var nLastBO_ErrorCode=0;
var strLastBO_ErrorText='';


function BO_ParseErrorBody(xml_body, xhr)
{
	if(xml_body == '' || xml_body === null)
	{
		return 0;
	}
	if(xhr!=null)
	{
		var error = xhr.documentElement;
		nLastBO_ErrorCode=error.getElementsByTagName("ERROR_CODE").item(0).text; //$(xml_body).find("error_code").text();
		strLastBO_ErrorText=error.getElementsByTagName("ERROR_TEXT").item(0).text; //$(xml_body).find("error_text").text();
		if (nLastBO_ErrorCode>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	return 0;
}

function killWindowsToLogin()
{
	var previousWindows = Ti.UI.currentWindow.previousWindows;

	for(var i = 0; i<Ti.UI.currentWindow.previousWindows.length;i++){
		Ti.UI.currentTab.close(previousWindows[i],{animated:false});
	}
	Ti.UI.currentTab.close(Ti.UI.currentWindow,{animated:true});
}

function BO_ParseError(xhr,textStatus)
{
	if(xhr.status == 0 && xhr.responseText == '')
	{
		nLastBO_ErrorCode=1;
		strLastBO_ErrorText=L("bo_parse_error_code_1");
		return;
	}
	
	if(xhr.status == 401)
	{
		nLastBO_ErrorCode=1000;
		strLastBO_ErrorText=L("bo_parse_error_code_1000");
		setTimeout(killWindowsToLogin,nCloseWinTimeout);
		return;
	}
	else
	{
		if(BO_ParseErrorBody(xhr.responseText, xhr.responseXML)!=0)
		{
			nLastBO_ErrorCode=1;	//general error: can be timeout or something:
			strLastBO_ErrorText=xhr.responseText;
			if(strLastBO_ErrorText.length === 0)
			{
				strLastBO_ErrorText=textStatus;
			}
		}
		else
		{
		    nLastBO_ErrorCode=1;
		    strLastBO_ErrorText=textStatus;
		}
	        return;
	}
    
	if(textStatus != '')
	{
		nLastBO_ErrorCode=1;
		strLastBO_ErrorText=textStatus;
	}
}
