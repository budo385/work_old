//Database.

//Connect database - create if not exists.
function openDB()
{
    var db;
    db = Titanium.Database.open('pfh');
    db.execute('CREATE TABLE IF NOT EXISTS server_list (slst_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, slst_server_id TEXT UNIQUE NOT NULL, slst_name TEXT NOT NULL, slst_image TEXT)');
   
    return db;
}

//Get servers function.
function getServers()
{
    var db;
    db = openDB();
    var resultSet;
    resultSet = db.execute('SELECT * FROM server_list');
    return resultSet;
}

function deleteServer(serverRowID)
{
    var db;
    db = openDB();
    db.execute('DELETE FROM server_list WHERE slst_id = ?',serverRowID);
}

function insertServer(serverID, serverName, serverPicture)
{
    var db;
    db = openDB();
    var resultSet;
    resultSet = db.execute("SELECT * FROM server_list WHERE slst_server_id = '" + serverID + "'");
    if(resultSet.rowCount == 0)
    {
        db.execute('INSERT INTO server_list (slst_server_id, slst_name, slst_image) VALUES(?,?,?)',serverID, serverName, serverPicture);
    }
    
    resultSet.close();
}
