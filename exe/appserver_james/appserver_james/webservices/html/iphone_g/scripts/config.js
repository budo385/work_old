// Hiding URL bar in mobile safari
window.onload = function() { setTimeout(function(){window.scrollTo(0, 1);}, 100); }

// REST config
var protocol 						=	document.location.protocol;					
var hostname						= 	document.location.hostname;					
var port							= 	document.location.port;						

var ROOT_URL 						= 	protocol+'//'+hostname+":"+port;
var REST_WEB_ROOT_URL 				= 	protocol+'//'+hostname+":"+port+'/rest';
var REST_WEB_DOCS_URL 				= 	ROOT_URL+"/user_storage/";
var REST_LOGIN 						= 	REST_WEB_ROOT_URL+"/service/login";
var REST_LOGOUT 					= 	REST_WEB_ROOT_URL+"/service/logout";

var COOKIE_EXPIRE_MIN				= 20;
var FFH_CONNECT_SERVER_MASTER_IP 	= "http://85.25.129.105:11000";
var FFH_CONNECT_SERVER_LIST 		= "www.sokrates.ch/ffh_servers.php";
var FFH_CONNECT_SERVER_TIMEOUT 		= 10*1000;

var PROGRAM_VERSION_MAJOR	  		= 1;
var PROGRAM_VERSION_MINOR 	  		= 0;
var PROGRAM_VERSION_REVISION  		= 11;
var PROGRAM_VERSION 		  		= PROGRAM_VERSION_MAJOR+"."+PROGRAM_VERSION_MINOR+"."+PROGRAM_VERSION_REVISION;

var DIGIT_APP						= '10'; 								
var DIGIT_PLATFORM = (getua()=='iphone'||getua()=='ipad')? '11' : '10';  	
var DIGIT_STATUS					= '0'; 								
var BIT_ADDON						= '000';								
var PROGRAM_CODE 			  		= DIGIT_APP+DIGIT_PLATFORM+DIGIT_STATUS+BIT_ADDON;

/*
10110000 - EVERPICS MOBILE WEBAPP
10100000 - EVERPICS BROWSER WEBAPP
10-everPICs
11-MOBILE_IPHONE_WEBAPP (10- BROWSER_WEBAPP)
0-status_none
000-addons_none
*/


// Storage patch
function setSessionkey(key, val) {
	sessionStorage.removeItem(key);
	sessionStorage.setItem(key, val);
} 
function setLocalkey(key, val) {
	localStorage.removeItem(key);
	localStorage.setItem(key, val);
}
	  
// OS DETECTION (PLATFORM) - Save & Get
function getua() 
{  

  if(localStorage.ga_platform!=0 || localStorage.ga_platform!=''){
  	return(localStorage.ga_platform);
  } 
  else
  {
	var agent  =  navigator.appVersion.toLowerCase(); 	
	var OSName = "Unknown OS"; 								 // indicates OS detection failure  
	if ( agent.indexOf("linux")  !=-1 )  OSName = "linux";	 // all versions of Linux
	if ( agent.indexOf("x11")    !=-1 )  OSName = "unix";	 // all other UNIX flavors 
	if ( agent.indexOf("win")    !=-1 )  OSName = "Windows"; // all versions of Windows
	if ( agent.indexOf("mac")    !=-1 )  OSName = "macos";	 // all versions of Macintosh OS	
	if ( agent.indexOf('ipad')   !=-1 )  OSName = "ipad";	 // all versions of iPad MacOS 
	if ( agent.indexOf('iphone') || agent.indexOf('ipod')!=-1 )  OSName = "iphone";  // iPhone + iPod	 	
	setLocalkey("ga_platform",OSName.toLowerCase());
    return(localStorage.ga_platform); 
  }
}
 
// Check Errorcode to Determine if call was OK (used in call back)
function errCheck() 
{
	if(nLastBO_ErrorCode!=0) // error
	{ 
		if(nLastBO_ErrorCode==1000)	//login failed
			alert(strLastBO_ErrorText); 
		
		//window.location="login.html";
	 return false;
	}
	else 
	{
		return true;
	}
}