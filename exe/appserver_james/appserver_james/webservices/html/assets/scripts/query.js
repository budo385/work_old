//--------------------------------------------------------------------
//	           Get URL parameters & values (QueryString)
//--------------------------------------------------------------------

//    USAGE:
// -> If the url/query string was:  http://www.xyz.com/index.html?lang=en
// -> Call: var langval = $.urlParam('lang'); 
// -> var langval = "en";

$.urlParam = function(name){
	var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (!results) { return 0; }
	return results[1] || 0;
}