#include "restrpcskeleton_service.h"

#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;

RestRpcSkeleton_Service::RestRpcSkeleton_Service(int RPCType):
RpcSkeleton(RPCType)
{
	SetNameSpace("service");	//set namespace (business object name)

	mFunctList.insert("GET/service/catalogs",&RestRpcSkeleton_Service::GetCatalogs);
	mFunctList.insert("GET/service/catalogs_ipad",&RestRpcSkeleton_Service::GetCatalogs_iPad);
	mFunctList.insert("GET/service/catalogs/status",&RestRpcSkeleton_Service::GetFolderStatus);
	mFunctList.insert("POST/service/catalog/read",&RestRpcSkeleton_Service::ReadCatalog);
	mFunctList.insert("POST/service/catalog/rename",&RestRpcSkeleton_Service::RenameCatalog);
	mFunctList.insert("POST/service/catalog/rename_client",&RestRpcSkeleton_Service::RenameCatalogClient);
	mFunctList.insert("POST/service/catalog/create_client",&RestRpcSkeleton_Service::CreateCatalogClient);
	mFunctList.insert("POST/service/catalog/unpublish_client",&RestRpcSkeleton_Service::UnpublishCatalogClient);
	mFunctList.insert("POST/service/catalog/invite",&RestRpcSkeleton_Service::MakeInvitation);
	mFunctList.insert("POST/service/document/delete",&RestRpcSkeleton_Service::DeleteDocument);
	mFunctList.insert("POST/service/document/upload",&RestRpcSkeleton_Service::UploadDocument);
	mFunctList.insert("POST/service/document/download",&RestRpcSkeleton_Service::DownloadDocument);
	mFunctList.insert("POST/service/document/picture",&RestRpcSkeleton_Service::GetPicture);
	mFunctList.insert("POST/service/settings/reload",&RestRpcSkeleton_Service::ReloadSettings);
	mFunctList.insert("GET/service/settings/server_data",&RestRpcSkeleton_Service::GetServerData);
	mFunctList.insert("POST/service/login",&RestRpcSkeleton_Service::Login);
	mFunctList.insert("POST/service/logout",&RestRpcSkeleton_Service::Logout);
	mFunctList.insert("POST/service/catalog/picture",&RestRpcSkeleton_Service::GetCatalogPicture);
	mFunctList.insert("POST/service/catalog/picture/download",&RestRpcSkeleton_Service::DownloadPicture);
	mFunctList.insert("POST/service/catalog/picture/rotate",&RestRpcSkeleton_Service::RotateCatalogPicture);
	mFunctList.insert("POST/service/catalog/picture/rename",&RestRpcSkeleton_Service::RenamePicture);
	mFunctList.insert("POST/service/catalog/picture/hide",&RestRpcSkeleton_Service::HidePicture);
	mFunctList.insert("POST/service/catalog/picture/hidelist",&RestRpcSkeleton_Service::GetHidePictureList);
	
	
	
	//mFunctList.insert("POST/service/document/upload",&RestRpcSkeleton_Service::DocumentPath_Upload);	

}

bool RestRpcSkeleton_Service::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod,int nClientTimeZoneOffsetMinutes)
{
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;
	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,0);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);
		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);
			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

	return true;
}

void RestRpcSkeleton_Service::ReadCatalog(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=8)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	DbRecordSet RetFiles;
	DbRecordSet RetSubFolders;
	int Ret_nTotalCount;
	int nFromN=-1;
	int nToN=-1;
	int nSortOrder=0;
	int nPicWidth=64;
	int nPicHeight=64;
	bool bKeepAspectRatio=false;
	bool bReturnThumbAsBinary=false;
	QString strAliasDirectory="";

	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nSortOrder,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nPicWidth,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&nPicHeight,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&bKeepAspectRatio,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&bReturnThumbAsBinary,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}


	//invoke handler
	g_ServiceHandler->ReadCatalog(err, Ret_nTotalCount,RetFiles, RetSubFolders, strAliasDirectory,nFromN, nToN, nSortOrder, nPicWidth,nPicHeight, bKeepAspectRatio,bReturnThumbAsBinary);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&RetFiles,"files","file");
	rpc.msg_out->AddParameter(&RetSubFolders,"folders","folder");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}



void RestRpcSkeleton_Service::ReloadSettings(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{

	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	bool bSkipCatalogReload=false;
	rpc.msg_in->GetParameter(0,&bSkipCatalogReload,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->ReloadSettings(err,bSkipCatalogReload);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::GetCatalogs(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	DbRecordSet Ret_Catalogs;

	//invoke handler
	g_ServiceHandler->GetCatalogs(err,Ret_Catalogs);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Catalogs,"Catalogs","catalog");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}
void RestRpcSkeleton_Service::GetCatalogs_iPad(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	DbRecordSet Ret_Catalogs;

	//invoke handler
	g_ServiceHandler->GetCatalogs_iPad(err,Ret_Catalogs);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Catalogs,"Catalogs","catalog");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::DeleteDocument(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{

	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString strAliasDirectory="";
	QString strFileName="";

	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->DeleteDocument(err, strAliasDirectory,strFileName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}


	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}


void RestRpcSkeleton_Service::UploadDocument(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{

	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=4)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString strAliasDirectory="";
	QString strFileName="";
	bool bOverWrite=true;
	QByteArray byteFileData;

	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&byteFileData,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&bOverWrite,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->UploadDocument(err, strAliasDirectory,strFileName,byteFileData,bOverWrite);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}


	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}

void RestRpcSkeleton_Service::DownloadDocument(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{

	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=4)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString strAliasDirectory="";
	QString strFileName="";
	QString strLink="";
	QByteArray Ret_byteFileData;
	bool bSkipAccess=false;
	bool bReturnAsBinary=false;

	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&bReturnAsBinary,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&bSkipAccess,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}


	//invoke handler
	g_ServiceHandler->DownloadDocument(err, strAliasDirectory,strFileName,Ret_byteFileData,strLink,bReturnAsBinary,bSkipAccess);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	rpc.msg_out->AddParameter(&Ret_byteFileData,"PictureData");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::DownloadPicture(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	//BT: quick patch for James V2011-001.000.093 (20.07.2011) version: do not touch please!!!!

	if(rpc.msg_in->GetParameterCount()<6 && rpc.msg_in->GetParameterCount()>7)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	DbRecordSet RetFiles;
	int nReturnFull;
	int nSortOrder=0;
	int nPicWidth=32;
	int nPicHeight=32;
	bool bReturnThumbAsBinary=false;
	QString strAliasDirectory;
	DbRecordSet FileNames;
	bool bSkipAccess=false;
	FileNames.addColumn(QVariant::String,"FileName"); //define before parsing

	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&FileNames,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nReturnFull,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nPicWidth,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nPicHeight,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&bReturnThumbAsBinary,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	if(rpc.msg_in->GetParameterCount()==7)
	{
		rpc.msg_in->GetParameter(6,&bSkipAccess,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	}

	//invoke handler
	g_ServiceHandler->DownloadPicture(err,RetFiles, strAliasDirectory,FileNames,nReturnFull,nPicWidth,nPicHeight,bReturnThumbAsBinary,bSkipAccess);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	rpc.msg_out->AddParameter(&RetFiles,"files","file");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}


void RestRpcSkeleton_Service::GetServerData(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	/*
	if(rpc.msg_in->GetParameterCount()!=1)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	*/
	//extract parameters to local vars
	QString  Ret_strName;
	QByteArray Ret_SrvPic;
	QString Ret_strEmailSignature;
	int nMajor;
	int nMinor;
	int nRev;

	//invoke handler
	g_ServiceHandler->GetServerData(err,Ret_strName,Ret_SrvPic,Ret_strEmailSignature,nMajor,nMinor,nRev);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_strName,"ServerName");
	rpc.msg_out->AddParameter(&Ret_SrvPic,"ServerPicture");
	rpc.msg_out->AddParameter(&Ret_strEmailSignature,"EmailSignature");
	rpc.msg_out->AddParameter(&nMajor,"nMajor");
	rpc.msg_out->AddParameter(&nMinor,"nMinor");
	rpc.msg_out->AddParameter(&nRev,"nRev");


	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}


void RestRpcSkeleton_Service::GetPicture(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{

	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=8)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	DbRecordSet RetFiles;
	int Ret_nTotalCount;
	int Ret_nCurrentPos;
	int nHeading=0;
	int nSortOrder=0;
	int nPicWidth=32;
	int nPicHeight=32;
	bool bKeepAspectRatio=false;
	bool bReturnThumbAsBinary=false;
	QString strAliasDirectory;
	QString strFileName;
	
	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nHeading,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nSortOrder,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nPicWidth,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&nPicHeight,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&bKeepAspectRatio,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&bReturnThumbAsBinary,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}


	//invoke handler
	g_ServiceHandler->GetPicture(err, Ret_nTotalCount,Ret_nCurrentPos, RetFiles, strAliasDirectory,strFileName,nHeading, nSortOrder, nPicWidth,nPicHeight, bKeepAspectRatio,bReturnThumbAsBinary);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_nCurrentPos,"nCurrentPos");
	rpc.msg_out->AddParameter(&RetFiles,"files","file");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::Logout(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString strSessionID;
	
	rpc.msg_in->GetParameter(0,&strSessionID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->Logout(err,strSessionID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::Login(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count

	//qDebug()<<rpc.msg_in->GetParameterCount();
	if(rpc.msg_in->GetParameterCount()!=7)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString RetOut_strSessionID;
	QString strUserName;
	QString strAuthToken;
	QString strClientNonce;
	int nProgCode=0;
	QString strProgVer="";
	QString strClientID="";
	QString strPlatform="";

	rpc.msg_in->GetParameter(0,&strUserName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strAuthToken,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strClientNonce,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nProgCode,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strProgVer,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&strClientID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&strPlatform,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}


	//invoke handler
	g_ServiceHandler->Login(err, RetOut_strSessionID,strUserName, strAuthToken,strClientNonce, nProgCode, strProgVer, strClientID, strPlatform);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	rpc.msg_out->AddParameter(&RetOut_strSessionID,"Session");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}



void RestRpcSkeleton_Service::GetCatalogPicture(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//qDebug()<<rpc.msg_in->GetParameterCount();
	if(rpc.msg_in->GetParameterCount()!=1)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	QString strCatalog;
	QByteArray Ret_byteFileData;
	QString Ret_strName;
	int Ret_nTotalCount;
	QByteArray Ret_byteFileDataLarge;

	rpc.msg_in->GetParameter(0,&strCatalog,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->GetCatalogPicture(err, strCatalog,Ret_byteFileData,Ret_byteFileDataLarge,Ret_nTotalCount,Ret_strName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
		{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	rpc.msg_out->AddParameter(&Ret_byteFileData,"CatalogPicture");
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_strName,"ServerName");
	rpc.msg_out->AddParameter(&Ret_byteFileDataLarge,"CatalogPictureLarge");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}


void RestRpcSkeleton_Service::RotateCatalogPicture(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//qDebug()<<rpc.msg_in->GetParameterCount();
	if(rpc.msg_in->GetParameterCount()!=7)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	QString strFileName;
	int nAngle;
	DbRecordSet RetFiles;
	int nPicWidth=75;
	int nPicHeight=75;
	bool bKeepAspectRatio=false;
	bool bReturnThumbAsBinary=false;
	QString strAliasDirectory="";

	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nAngle,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nPicWidth,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nPicHeight,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&bKeepAspectRatio,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&bReturnThumbAsBinary,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->RotatePicture(err,RetFiles,strAliasDirectory,strFileName,nAngle, nPicWidth, nPicHeight, bKeepAspectRatio, bReturnThumbAsBinary);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	rpc.msg_out->AddParameter(&RetFiles,"File","file");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}


void RestRpcSkeleton_Service::RenameCatalog(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	if(rpc.msg_in->GetParameterCount()!=2)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	QString strCatalog;
	QString strCatalogNew;

	rpc.msg_in->GetParameter(0,&strCatalog,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strCatalogNew,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->RenameCatalog(err, strCatalog,strCatalogNew);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
		{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::GetFolderStatus(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	DbRecordSet Ret_Folders;
	int Ret_nSettingsChanged;
	//invoke handler
	g_ServiceHandler->GetFolderStatus(err,Ret_Folders,Ret_nSettingsChanged);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	rpc.msg_out->AddParameter(&Ret_Folders,"Folders","folder");
	rpc.msg_out->AddParameter(&Ret_nSettingsChanged,"SeettingsChanged");
	
	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);

	//qDebug()<<rpc.msg_out->GetBuffer()->constData();

	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::MakeInvitation(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//qDebug()<<rpc.msg_in->GetParameterCount();
	if(rpc.msg_in->GetParameterCount()!=9)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	int nMessageType;
	QString strCatalogName;
	QString strFirstName;
	QString strLastName;
	QString strEmail;
	QString strSMS;
	QString Ret_strBody;
	int nIsHtml;
	QString strEmailSubject;
	QString strEmailBody;

	rpc.msg_in->GetParameter(0,&nMessageType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strCatalogName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strFirstName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&strLastName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strEmail,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&strSMS,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&nIsHtml,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&strEmailSubject,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&strEmailBody,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->MakeInvitation(err,Ret_strBody,nMessageType,strCatalogName,strFirstName,strLastName, strEmail, strSMS,nIsHtml,strEmailSubject,strEmailBody);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	rpc.msg_out->AddParameter(&Ret_strBody,"Message");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}


void RestRpcSkeleton_Service::RenameCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	if(rpc.msg_in->GetParameterCount()!=2)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	QString strCatalog;
	QString strCatalogNew;

	rpc.msg_in->GetParameter(0,&strCatalog,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strCatalogNew,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->RenameCatalogClient(err, strCatalog,strCatalogNew);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}

void RestRpcSkeleton_Service::CreateCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{

	if(rpc.msg_in->GetParameterCount()!=1)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	QString strCatalog;

	rpc.msg_in->GetParameter(0,&strCatalog,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->CreateCatalogClient(err, strCatalog);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}
void RestRpcSkeleton_Service::RenamePicture(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	if(rpc.msg_in->GetParameterCount()!=3)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	QString strAlias;
	QString strFileName;
	QString strDisplayName;

	rpc.msg_in->GetParameter(0,&strAlias,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strDisplayName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->RenamePicture(err, strAlias,strFileName,strDisplayName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::HidePicture(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	if(rpc.msg_in->GetParameterCount()!=3)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	QString strAlias;
	QString strFileName;
	int nHide;

	rpc.msg_in->GetParameter(0,&strAlias,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nHide,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->HidePicture(err, strAlias,strFileName,nHide);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}


void RestRpcSkeleton_Service::GetHidePictureList(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=4)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	DbRecordSet RetFiles;
	DbRecordSet RetSubFolders;
	int nPicWidth=64;
	int nPicHeight=64;
	bool bReturnThumbAsBinary=false;
	QString strAliasDirectory="";

	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nPicWidth,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nPicHeight,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&bReturnThumbAsBinary,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}


	//invoke handler
	g_ServiceHandler->GetHidePictureList(err,strAliasDirectory,RetFiles,nPicWidth,nPicHeight,bReturnThumbAsBinary);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetFiles,"files","file");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}


void RestRpcSkeleton_Service::UnpublishCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	if(rpc.msg_in->GetParameterCount()!=1)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}
	//extract parameters to local vars
	QString strAlias;

	rpc.msg_in->GetParameter(0,&strAlias,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->UnpublishCatalogClient(err, strAlias);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}
