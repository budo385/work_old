#ifndef RESTHTTPSERVER_LOCAL_H
#define RESTHTTPSERVER_LOCAL_H

#include "trans/trans/resthttpserver.h"

class RestHTTPServer_Local :public RestHTTPServer
{

public:
	RestHTTPServer_Local(RpcSkeleton *pServerSkeletonSet,QString strWWWRootDir="");

	void	AddAllowedIPAddress(QString strIP){m_lstAllowedIPAddresses.append(strIP);}

protected:

	bool	AuthenticateUser(Status &err,HTTPContext &ctx,HTTPRequest &request,HTTPResponse &response, int &nClientTimeZoneOffsetMinutes);	
	void	LockThreadActive();
	void	UnLockThreadActive();

	QStringList m_lstAllowedIPAddresses; 
};

#endif // RESTHTTPSERVER_LOCAL_H
