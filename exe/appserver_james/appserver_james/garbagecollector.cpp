#include "garbagecollector.h"
#include "common/common/threadid.h"
#include "common/common/config_version_ffh.h"
#include "common/common/datahelper.h"

#include "common/common/logger.h"
extern Logger g_Logger;
#include "usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "thumbmanager.h"
extern ThumbManager		*g_ThumbManager;
#include "settingsmanager.h"
extern SettingsManager	*g_SettingsManager;


GarbageCollector::GarbageCollector(HTTPServer *pHttpServer)
{
	m_pHttpServer=pHttpServer;
}

GarbageCollector::~GarbageCollector()
{

}


//start every X minutes
void GarbageCollector::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	//return;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());


	//----------------------------------------------------------------------------------------------------------
	//1. st: check inactive sessions: delete them (only if last modify < (current-is_alive_period)
	//----------------------------------------------------------------------------------------------------------
	QList<int> lstDeadSockets;
	g_UserSessionManager->DeleteExpiredUserSessions(lstDeadSockets);
	m_pHttpServer->CloseConnection(lstDeadSockets);


	//----------------------------------------------------------------------------------------------------------
	//2nd: disconnect all sockets that are not in read/write mode and that are not in session list
	//----------------------------------------------------------------------------------------------------------
	ClientConnections lstSockets;
	m_pHttpServer->GetSocketList(lstSockets);
	UserSessionList lstSessions;
	g_UserSessionManager->GetSessionList(lstSessions);

	ClientConnectionsIterator z(lstSockets);
	QList<int> lstSocketsToDelete;
	while (z.hasNext()) //find orphan sockets that exists in HTTP server but not inside user session list
	{
		z.next();
		int nSocketID=z.key();
		int nSocketThreadID=z.value()->GetSocketThreadID();
		if (!lstSessions.contains(nSocketThreadID))
		{
			if (z.value()->GetSocketState()!=HTTPServerSocketHandler::STATE_READ && z.value()->GetSocketState()!=HTTPServerSocketHandler::STATE_WRITE)
			{
				lstSocketsToDelete.append(nSocketID);
			}
		}
	}

	if(lstSocketsToDelete.size()!=0)
	{
		m_pHttpServer->CloseConnection(lstSocketsToDelete);
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_CLEANED_ORPHAN_SOCKETS,QVariant(lstSocketsToDelete.size()).toString());
	}


	//----------------------------------------------------------------------------------------------------------
	//2th: Delete user_storage directory (app.ini->enable user storage/disk quota per user!?)
	//----------------------------------------------------------------------------------------------------------
	//get all from user_storage
	QString strUserStorageDirectoryPath =g_UserSessionManager->GetUserStorageDirectoryPath();
	if (!strUserStorageDirectoryPath.isEmpty())
	{
		QList<QString> lstDirs=DataHelper::GetSubDirectories(strUserStorageDirectoryPath);
		UserSessionListIterator k(lstSessions); //all sessions
		while (k.hasNext()) 
		{
			k.next();
			QString strDirName=DataHelper::EncodeSession2Directory(k.value().strSession);
			int nPos=lstDirs.indexOf(strDirName);
			if (nPos!=-1)
				lstDirs.removeAt(nPos);
		}
		//if something left: delete:
		int nSize=lstDirs.size();
		for(int i=0;i<nSize;i++)
		{
			if (lstDirs.at(i).toLower()==THUMB_CACHE_PREFIX) //skip thumb cache subdir
				continue;
			DataHelper::RemoveAllFromDirectory(strUserStorageDirectoryPath+"/"+lstDirs.at(i),true,true);
		}
	}

	//----------------------------------------------------------------------------------------------------------
	//3th: Cleanup thumbs
	//----------------------------------------------------------------------------------------------------------

	g_ThumbManager->CleanGarbage();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_GENERAL,"End of Garbage Collector");

	//Save meta data on every turn
	g_SettingsManager->SaveMetaData();
}


