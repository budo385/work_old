#include "servicehandler.h"
#include "common/common/datahelper.h"
#include "filecruncher.h"
#include "common/common/userstoragehelper.h"
#include "common/common/config_version_ffh.h"
#include "common/common/authenticator.h"
#include "emailhandler.h"


#include "common/common/logger.h"
extern Logger							g_Logger;
#include "usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "settingsmanager.h"
extern SettingsManager	*g_SettingsManager;
#include "thumbmanager.h"
extern ThumbManager		*g_ThumbManager;
#include "applicationserver.h"
extern ApplicationServer				*g_AppServer;


ServiceHandler::ServiceHandler()
{

}

//strDirectory: alias+/subdir
//RetData: returns first subfolders by abc then files by sort order..
//nSortOrder:0 by name, 1 by ext
//if strAliasPath is empty then get 1st or all on which he has right
//nSortOrder -ignored!!!
void ServiceHandler::ReadCatalog(Status &pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Files, DbRecordSet &Ret_SubFolders, QString strAliasDirectory,int nFromN,int nToN, int nSortOrder, int nPicWidth,int nPicHeight, bool bKeepAspectRatio, bool bReturnThumbAsBinary)
{
	QTime t1;
	
	//qDebug()<<"Read 1";

	//read files from catalog actual dir
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strAliasDirectory,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;

	//if pph then limit file types only to pics:
	int nProgCode=g_UserSessionManager->GetUserProgCode();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("ReadCatalog: alias %1, path %2").arg(strAliasDirectory).arg(strDirPath));

	
	//if (nProgCode==CLIENT_CODE_PFH_LITE || nProgCode==CLIENT_CODE_PFH_PRO)
	//{
	strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC);
	//}

	if (strPhotoAlbumID.isEmpty())
		FileCruncher::GetFilesFromFolder(strDirPath,Ret_Files, Ret_SubFolders,strAllowedFileTypes);
	else
		FileCruncher_iPhoto::GetFilesFromFolder(strPhotoAlbumID,Ret_Files, Ret_SubFolders,strAllowedFileTypes);

	
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("ReadCatalog/GetFilesFromFolder count files %1").arg(Ret_Files.getRowCount()));

	//qDebug()<<"Read 2";


	Ret_Files.addColumn(QVariant::String,"THUMB_LINK");
	Ret_Files.addColumn(QVariant::String,"DOC_LINK");
	Ret_Files.addColumn(QVariant::ByteArray,"THUMB_BINARY");
	Ret_Files.addColumn(QVariant::Int,"THUMB_WIDTH");
	Ret_Files.addColumn(QVariant::Int,"THUMB_HEIGHT");
	Ret_Files.addColumn(QVariant::String,"DISPLAYNAME"); //marin wants this to be used!!! jaaaaaaaahuuuuu

	g_SettingsManager->FilterFileList(Ret_Files,strAliasDirectory);  //MB requested shadow list
	Ret_nTotalCount=Ret_Files.getRowCount();
	DataHelper::GetPartOfRecordSet(Ret_Files,nFromN,nToN);
	//_DUMP(Ret_Files);
	
	//qDebug()<<"Read 3";

	//get sub count for each directory;
	Ret_SubFolders.addColumn(QVariant::String,"ALIAS");
	Ret_SubFolders.addColumn(QVariant::String,"IPHOTO_ID");
	Ret_SubFolders.addColumn(QVariant::Int,"STATUS");
	Ret_SubFolders.addColumn(QVariant::Int,"PIC_CNT");
	Ret_SubFolders.addColumn(QVariant::Int,"CNT");
	int nSizeX=Ret_SubFolders.getRowCount();
	for (int i=0;i<nSizeX;i++)
	{
		QString strAlias=strAliasDirectory+"/"+Ret_SubFolders.getDataRef(i,"NAME").toString();
		Ret_SubFolders.setData(i,"ALIAS",strAlias);
	}
	g_ThumbManager->RecalculateCounts(Ret_SubFolders);

	
	//Thumb check:

	//qDebug()<<"Read 4";

	//1st: check if thumb manager supports w,h,a
	bool bCreateThumbsOnFly=true;
	if(g_ThumbManager->IsSupporttedThumbType(nPicWidth,nPicHeight,bKeepAspectRatio))
	{

		//qDebug()<<"Read Start Thumb Creation if already not";
		QTime t1;
		t1.start();
		g_ThumbManager->CreateThumbsFromAlias(strAliasDirectory);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,1,"Check Folder:"+QString::number(t1.elapsed()));

		QString strThumbSuffix=g_ThumbManager->GetThumbSuffix(nPicWidth,nPicHeight);

		QUrl url(strAliasDirectory);
		QString strAliasDirEnc=url.toEncoded();

		int nSize=Ret_Files.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			QString strFileName=Ret_Files.getDataRef(i,"FILENAME").toString();
			strFileName=QUrl::toPercentEncoding(strFileName);
			QString strThumbFileName=Ret_Files.getDataRef(i,"NAME").toString()+strThumbSuffix+".jpg";
			strThumbFileName=QUrl::toPercentEncoding(strThumbFileName);

			QString strThumbLink=QString(THUMB_CACHE_PREFIX)+"/"+strAliasDirEnc+"/"+strThumbFileName;
			QString strLink=QString(CATALOG_STORAGE_PREFIX)+"/"+strAliasDirEnc+"/"+strFileName;

			Ret_Files.setData(i,"THUMB_LINK",strThumbLink);
			Ret_Files.setData(i,"DOC_LINK",strLink);

			QString strThumbFilePath=g_ThumbManager->GetThumbDirRoot()+"/"+strAliasDirectory+"/"+Ret_Files.getDataRef(i,"NAME").toString()+strThumbSuffix+".jpg";
			if (!QFile::exists(strThumbFilePath))
			{
				Ret_Files.setData(0,"THUMB_LINK","");
				//Ret_Files.setData(0,"DOC_LINK","");
			}
			else if (bReturnThumbAsBinary)
			{
				//MP: load binary
				QFile in_file(strThumbFilePath);
				if(in_file.open(QIODevice::ReadOnly))
				{
					QByteArray buffer=in_file.readAll();
					Ret_Files.setData(i,"THUMB_BINARY",buffer);

					//retrieve height and width:
					int nWidth,nHeight;
					if(FileCruncher::GetJpegSize((unsigned char *)buffer.data(), buffer.size(),&nWidth,&nHeight))
					{
						Ret_Files.setData(i,"THUMB_WIDTH",nWidth);
						Ret_Files.setData(i,"THUMB_HEIGHT",nHeight);
					}
				}
			}
		}
	}
	else
	{
		//resolve unique session temp directory:
		QString session=g_UserSessionManager->GetSessionID();
		QString strEncodedUserSubDir=DataHelper::EncodeSession2Directory(session);
		QString strUserSubDir=g_UserSessionManager->InitializeUserStorageDirectory(pStatus,strEncodedUserSubDir);
		
		QString strThumbSuffix=g_ThumbManager->GetThumbSuffix(nPicWidth,nPicHeight);

		QUrl url(strAliasDirectory);
		QString strAliasDirEnc=url.toEncoded();
	
		FileCruncher::CreateThumbsFromFiles(Ret_Files,strUserSubDir,strThumbSuffix,nPicWidth,nPicHeight,bKeepAspectRatio);

		int nSize=Ret_Files.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			QString strFileName=Ret_Files.getDataRef(i,"FILENAME").toString();
			strFileName=QUrl::toPercentEncoding(strFileName);
			QString strThumbFileName=Ret_Files.getDataRef(i,"NAME").toString()+strThumbSuffix+".jpg";
			strThumbFileName=QUrl::toPercentEncoding(strThumbFileName);

			QString strThumbLink=QString(USER_STORAGE_PREFIX)+"/"+strEncodedUserSubDir+"/"+strThumbFileName;
			QString strLink=QString(CATALOG_STORAGE_PREFIX)+"/"+strAliasDirEnc+"/"+strFileName;

			Ret_Files.setData(i,"THUMB_LINK",strThumbLink);
			Ret_Files.setData(i,"DOC_LINK",strLink);

			QString strThumbFilePath=strUserSubDir+"/"+Ret_Files.getDataRef(i,"NAME").toString()+strThumbSuffix+".jpg";
			if (!QFile::exists(strThumbFilePath))
			{
				Ret_Files.setData(0,"THUMB_LINK","");
				//Ret_Files.setData(0,"DOC_LINK","");
			}
			else if (bReturnThumbAsBinary)
			{
				//MP: load binary
				QFile in_file(strThumbFilePath);
				if(in_file.open(QIODevice::ReadOnly))
				{
					QByteArray buffer=in_file.readAll();
					Ret_Files.setData(i,"THUMB_BINARY",buffer);
					//retrieve height and width:
					int nWidth,nHeight;
					if(FileCruncher::GetJpegSize((unsigned char *)buffer.data(), buffer.size(),&nWidth,&nHeight))
					{
						Ret_Files.setData(i,"THUMB_WIDTH",nWidth);
						Ret_Files.setData(i,"THUMB_HEIGHT",nHeight);
					}
				}
			}
		}
	}

	//qDebug()<<"Read exit loop";

	Ret_Files.removeColumn(Ret_Files.getColumnIdx("PATH"));
	Ret_SubFolders.removeColumn(Ret_SubFolders.getColumnIdx("PATH"));
	

	//Ret_Files.Dump();


	if (nSortOrder==0)
		Ret_Files.sort("NAME");
	else
		Ret_Files.sort("EXT");




}



//based on strFileName + heading + sort order, it will return 1 record: current, prev or next
//nHeading=0, current, -1 prev, +1 next
//nSortOrder:0 by name, 1 by ext
//if nPicWidth nad height are supported it will be fast else..
void ServiceHandler::GetPicture(Status &pStatus, int &Ret_nTotalCount, int &Ret_nCurrentPos, DbRecordSet &Ret_Files, QString strAliasDirectory, QString strFileName, int nHeading, int nSortOrder, int nPicWidth,int nPicHeight, bool bKeepAspectRatio, bool bReturnThumbAsBinary)
{
	//get all list, get next/prev and current pos
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strAliasDirectory,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;

	//if pph then limit file types only to pics:
	int nProgCode=g_UserSessionManager->GetUserProgCode();
	//if (nProgCode==CLIENT_CODE_PFH_LITE || nProgCode==CLIENT_CODE_PFH_PRO)
	//{
		strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC);
	//}

	DbRecordSet lstSubDirs;
	if (strPhotoAlbumID.isEmpty())
		FileCruncher::GetFilesFromFolder(strDirPath,Ret_Files, lstSubDirs,strAllowedFileTypes);
	else
		FileCruncher_iPhoto::GetFilesFromFolder(strPhotoAlbumID,Ret_Files, lstSubDirs,strAllowedFileTypes);

	Ret_nTotalCount=Ret_Files.getRowCount();
	if (nSortOrder==0)
		Ret_Files.sort("NAME");
	else
		Ret_Files.sort("EXT");

	Ret_nCurrentPos=Ret_Files.find("FILENAME",strFileName,true);
	if (Ret_nCurrentPos<0)
	{
		pStatus.setError(1,"File not found in the catalog!");
		return;
	}

	//calc position, check boundaries:
	Ret_nCurrentPos=Ret_nCurrentPos+nHeading;
	if (Ret_nCurrentPos<0 || Ret_nCurrentPos>=Ret_nTotalCount)
	{
		pStatus.setError(1,"Position is out of range!");
		return;
	}

	Ret_Files.selectRow(Ret_nCurrentPos);
	Ret_Files.deleteUnSelectedRows();
	Ret_Files.addColumn(QVariant::String,"THUMB_LINK");
	Ret_Files.addColumn(QVariant::String,"DOC_LINK");
	Ret_Files.addColumn(QVariant::ByteArray,"THUMB_BINARY");
	Ret_Files.addColumn(QVariant::Int,"THUMB_WIDTH");
	Ret_Files.addColumn(QVariant::Int,"THUMB_HEIGHT");
	Ret_nCurrentPos++; //so he can show 1/123 or that...

	QString strThumbSuffix=g_ThumbManager->GetThumbSuffix(nPicWidth,nPicHeight);

	//get filename+path+last modif of original
	QString strOriginalFileName=Ret_Files.getDataRef(0,"PATH").toString();
	FileCruncher::ReplaceNetworkPathWithUNC(strOriginalFileName);

	QFileInfo fileinfo_Original(strOriginalFileName);
	QDateTime datOriginal=fileinfo_Original.lastModified();

	//get last modif of the thumb file->check if supported by thumb manager then compare last modif if not supported look into user_Storage
	if(g_ThumbManager->IsSupporttedThumbType(nPicWidth,nPicHeight,bKeepAspectRatio))
	{
		QUrl url(strAliasDirectory);
		QString strAliasDirEnc=url.toEncoded();

		QString strFileName=Ret_Files.getDataRef(0,"FILENAME").toString();
		strFileName=QUrl::toPercentEncoding(strFileName);
		QString strThumbFileName=Ret_Files.getDataRef(0,"NAME").toString()+strThumbSuffix+".jpg";
		strThumbFileName=QUrl::toPercentEncoding(strThumbFileName);

		QString strThumbLink=QString(THUMB_CACHE_PREFIX)+"/"+strAliasDirEnc+"/"+strThumbFileName;
		QString strLink=QString(CATALOG_STORAGE_PREFIX)+"/"+strAliasDirEnc+"/"+strFileName;

		Ret_Files.setData(0,"THUMB_LINK",strThumbLink);
		Ret_Files.setData(0,"DOC_LINK",strLink);

		QString strThumbFilePath=g_ThumbManager->GetThumbDirRoot()+"/"+strAliasDirectory+"/"+Ret_Files.getDataRef(0,"NAME").toString()+strThumbSuffix+".jpg";
		QFileInfo fileinfo_Picture(strThumbFilePath);
		QDateTime datPicture=fileinfo_Picture.lastModified();

		if (datPicture<datOriginal || !fileinfo_Picture.exists())
		{
			g_ThumbManager->CreateThumbsFromAlias(strAliasDirectory);
		}

		if (!QFile::exists(strThumbFilePath))
		{
			Ret_Files.setData(0,"THUMB_LINK","");
			Ret_Files.setData(0,"DOC_LINK","");
		}
		else if (bReturnThumbAsBinary)
		{
			//MP: load binary
			QFile in_file(strThumbFilePath);
			if(in_file.open(QIODevice::ReadOnly))
			{
				QByteArray buffer=in_file.readAll();
				Ret_Files.setData(0,"THUMB_BINARY",buffer);
				//retrieve height and width:
				int nWidth,nHeight;
				if(FileCruncher::GetJpegSize((unsigned char *)buffer.data(), buffer.size(),&nWidth,&nHeight))
				{
					Ret_Files.setData(0,"THUMB_WIDTH",nWidth);
					Ret_Files.setData(0,"THUMB_HEIGHT",nHeight);
				}
			}
		}
	}
	else
	{
		QString session=g_UserSessionManager->GetSessionID();
		QString strEncodedUserSubDir=DataHelper::EncodeSession2Directory(session);
		QString strUserSubDir=g_UserSessionManager->InitializeUserStorageDirectory(pStatus,strEncodedUserSubDir);

		QUrl url(strAliasDirectory);
		QString strAliasDirEnc=url.toEncoded();

		QString strFileName=Ret_Files.getDataRef(0,"FILENAME").toString();
		strFileName=QUrl::toPercentEncoding(strFileName);
		QString strThumbFileName=Ret_Files.getDataRef(0,"NAME").toString()+strThumbSuffix+".jpg";
		strThumbFileName=QUrl::toPercentEncoding(strThumbFileName);

		QString strThumbLink=QString(USER_STORAGE_PREFIX)+"/"+strEncodedUserSubDir+"/"+strThumbFileName;
		QString strLink=QString(CATALOG_STORAGE_PREFIX)+"/"+strAliasDirEnc+"/"+strFileName;

		Ret_Files.setData(0,"THUMB_LINK",strThumbLink);
		Ret_Files.setData(0,"DOC_LINK",strLink);

		QString strThumbFilePath=strUserSubDir+"/"+Ret_Files.getDataRef(0,"NAME").toString()+strThumbSuffix+".jpg";
		QFileInfo fileinfo_Picture(strThumbFilePath);
		QDateTime datPicture=fileinfo_Picture.lastModified();

		//try to create thumb:
		if (datPicture<datOriginal || !fileinfo_Picture.exists())
		{
			FileCruncher::CreateThumbsFromFiles(Ret_Files,strUserSubDir,strThumbSuffix,nPicWidth,nPicHeight,bKeepAspectRatio);
		}

		if (!QFile::exists(strThumbFilePath))
		{
			Ret_Files.setData(0,"THUMB_LINK","");
			Ret_Files.setData(0,"DOC_LINK","");
		}
		else if (bReturnThumbAsBinary)
		{
			//MP: load binary
			QFile in_file(strThumbFilePath);
			if(in_file.open(QIODevice::ReadOnly))
			{
				QByteArray buffer=in_file.readAll();
				Ret_Files.setData(0,"THUMB_BINARY",buffer);
				int nWidth,nHeight;
				if(FileCruncher::GetJpegSize((unsigned char *)buffer.data(), buffer.size(),&nWidth,&nHeight))
				{
					Ret_Files.setData(0,"THUMB_WIDTH",nWidth);
					Ret_Files.setData(0,"THUMB_HEIGHT",nHeight);
				}
			}
		}
	}


	Ret_Files.removeColumn(Ret_Files.getColumnIdx("PATH"));
}


void ServiceHandler::RotatePicture(Status &pStatus, DbRecordSet &Ret_Files, QString strAliasDirectory, QString strFileName, int nAngle,int nPicWidth,int nPicHeight, bool bKeepAspectRatio, bool bReturnThumbAsBinary)
{
	g_ThumbManager->RotateThumb(pStatus,strAliasDirectory,strFileName,nAngle);

	int Ret_nTotalCount;
	int Ret_nCurrentPos;
	GetPicture(pStatus, Ret_nTotalCount, Ret_nCurrentPos, Ret_Files, strAliasDirectory, strFileName, 0, 0, nPicWidth, nPicHeight, bKeepAspectRatio, bReturnThumbAsBinary);

	//Ret_Files.Dump();






}

//RetDirs: <ALIAS,CNT,THUMB, RIGHTS>
//normal: only small thumb left
void ServiceHandler::GetCatalogs(Status &pStatus, DbRecordSet &Ret_Catalogs)
{
	GetCatalogsAll(pStatus,Ret_Catalogs);
	Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("THUMB_LARGE"));
}

void ServiceHandler::GetCatalogs_iPad(Status &pStatus, DbRecordSet &Ret_Catalogs)
{
	GetCatalogsAll(pStatus,Ret_Catalogs);
	Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("THUMB"));
	Ret_Catalogs.renameColumn("THUMB_LARGE","THUMB");
}


void ServiceHandler::GetCatalogPicture(Status &pStatus, QString strCatalogName, QByteArray &Ret_byteFileData, QByteArray &Ret_byteFileDataLarge, int &Ret_nTotalCount, QString &Ret_strName)
{
	Ret_byteFileData.clear();

	DbRecordSet lstDirs;
	g_SettingsManager->GetFolders(lstDirs);

	int nRow=lstDirs.find("ALIAS",strCatalogName,true);
	if (nRow>=0)
	{
		Ret_byteFileData=lstDirs.getDataRef(nRow,"THUMB").toByteArray();
		Ret_byteFileDataLarge=lstDirs.getDataRef(nRow,"THUMB_LARGE").toByteArray();
	}

	//get all list, get next/prev and current pos
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strCatalogName,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;

	//if pph then limit file types only to pics:
	int nProgCode=g_UserSessionManager->GetUserProgCode();
	//if (nProgCode==CLIENT_CODE_PFH_LITE || nProgCode==CLIENT_CODE_PFH_PRO)
	//{
		strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC);
	//}

	DbRecordSet lstSubDirs;
	DbRecordSet Ret_Files;
	if (strPhotoAlbumID.isEmpty())
		FileCruncher::GetFilesFromFolder(strDirPath,Ret_Files, lstSubDirs,strAllowedFileTypes);
	else
		FileCruncher_iPhoto::GetFilesFromFolder(strPhotoAlbumID,Ret_Files, lstSubDirs,strAllowedFileTypes);

	Ret_nTotalCount=Ret_Files.getRowCount();

	Ret_strName=g_SettingsManager->GetServerName();


}

void ServiceHandler::ReloadSettings(Status &pStatus, bool bSkipReloadCatalogs)
{
	QString strSession;
	g_UserSessionManager->CreateTempRootSessionForAuthenticatedConnection(pStatus,strSession);
	if (!pStatus.IsOK())
		return;

	QString strIP=g_UserSessionManager->GetClientIP();
	if (strIP=="127.0.0.1" || strIP=="localhost")
	{
		//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Reload Settings");
		g_SettingsManager->LoadSettings();
		//g_ThumbManager->ClearHashes();
		//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Kill Hash");

		if (!bSkipReloadCatalogs)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Process All Emit Signal Triggered");
			g_ThumbManager->StartProcessAll(); //schedule start
		}
	}
	else
		pStatus.setError(1,"Access not allowed");
	
	g_UserSessionManager->DeleteSession(pStatus,strSession);
}


void ServiceHandler::DeleteDocument(Status &pStatus, QString strAliasDirectory,QString strFileName)
{
	//read files from catalog actual dir
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strAliasDirectory,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;

	QString strFilePath=strDirPath+"/"+strFileName;

	//check permission to delete:
	QString strUserName=g_UserSessionManager->GetUserNameExt();
	if(!g_SettingsManager->TestUserRight(strUserName,strAlias,SettingsManager::OP_DELETE))
	{
		pStatus.setError(1,"Insufficient access right!");
		return;
	}

	if(!QFile::remove(strFilePath))
	{
		pStatus.setError(1,"Failed to delete file!");
		return;
	}
}

void ServiceHandler::UploadDocument(Status &pStatus, QString strAliasDirectory,QString strFileName,QByteArray &byteFileData, bool bOverWrite,QString strSession)
{
	//read files from catalog actual dir
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strAliasDirectory,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;

	QString strFilePath=strDirPath+"/"+strFileName;

	//check permission to delete:
	QString strUserName=g_UserSessionManager->GetUserNameExt(strSession);
	if(!g_SettingsManager->TestUserRight(strUserName,strAlias,SettingsManager::OP_UPLOAD))
	{
		pStatus.setError(1,"Insufficient access right!");
		return;
	}

	//open INI file for writing
	QFile filek(strFilePath);
	if (filek.exists() && !bOverWrite)
	{
		pStatus.setError(1,"File already exists");
		return;
	}

	if (!filek.open(QIODevice::WriteOnly))
	{
		pStatus.setError(1,"Error while trying to save file");
		return;
	}

	filek.write(byteFileData);
	filek.close();


	//MB: when upload pic try to rotate: HAHAHAHAHAHAHAAHAHAHAHAAHAHAA
	//if error ignore, this is big pile of shit anyway
	g_ThumbManager->RotateThumbByExifOrienatation(pStatus,strFilePath);
	pStatus.setError(0);


	/*	Create catalog pic if empty:  */
	int nCnt= FileCruncher::GetFileCountFromFolderFast(strDirPath);
	if (nCnt==1)
	{
		//change in settings
		DbRecordSet lstFolders;
		g_SettingsManager->GetFolders(lstFolders);

		int nRow=lstFolders.find("ALIAS",strAliasDirectory,true);
		if (nRow>=0)
		{
			QFileInfo info2(strFilePath);
			QString strFileOut=info2.absolutePath()+"/"+info2.completeBaseName()+"_75x75.jpg";
			QString strFilePath=strDirPath+"/"+strFileName;

			//gentool: create thumb 150x150 and small...
			g_ThumbManager->CreateOneThumb(pStatus,strFilePath,strFileOut,75,75);
			if (pStatus.IsOK())
			{
				//load binary
				QFile in_file(strFileOut);
				if(in_file.open(QIODevice::ReadOnly))
				{
					QByteArray buffer=in_file.readAll();
					lstFolders.setData(nRow,"THUMB",buffer);
				}
			}
			QFile::remove(strFileOut);
			strFileOut=info2.absolutePath()+"/"+info2.completeBaseName()+"_1505x150.jpg";


			//gentool: create thumb 150x150
			g_ThumbManager->CreateOneThumb(pStatus,strFilePath,strFileOut,150,150);
			if (pStatus.IsOK())
			{
				//load binary
				QFile in_file(strFileOut);
				if(in_file.open(QIODevice::ReadOnly))
				{
					QByteArray buffer=in_file.readAll();
					lstFolders.setData(nRow,"THUMB_LARGE",buffer);
				}
			}
			QFile::remove(strFileOut);


			g_SettingsManager->SetFolders(lstFolders);
			g_SettingsManager->SaveSettings();
			g_SettingsManager->SetSettingsChangedNeedReloadFlag();
			g_SettingsManager->NotifyClientSettingsChangedNeedReload();
		 }

		}

	g_ThumbManager->StartProcessAll(); 
}


//get original: if raw get thumb and return it
//else based on platform get max thumb size and return it
//when gettting original:
//check if already exists thumb with suffix: _full. If so return. Else create
void ServiceHandler::DownloadDocument(Status &pStatus, QString strAliasDirectory,QString strFileName,QByteArray &Ret_byteFileData,QString &strThumbLink,bool bReturnThumbAsBinary,bool bSkipAccess)
{
	//check permission to delete:
	QString strUserName=g_UserSessionManager->GetUserNameExt();
	if (!bSkipAccess)
		if(!g_SettingsManager->TestUserRight(strUserName,strAliasDirectory,SettingsManager::OP_DOWNLOAD))
		{
			pStatus.setError(1,"Insufficient access right!");
			return;
		}

	//read files from catalog actual dir
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strAliasDirectory,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;

	QString strThumbDir=g_ThumbManager->GetThumbPathBasedOnCatalogPath(strAliasDirectory);
	QString strFilePath=strDirPath+"/"+strFileName;

	
	if (!strPhotoAlbumID.isEmpty())
	{
		strFilePath="";
		//if iPhoto then get all files find ours: set it
		DbRecordSet lstSubs;
		DbRecordSet lstPaths;
		FileCruncher_iPhoto::GetFilesFromFolder(strPhotoAlbumID,lstPaths,lstSubs);
		int nSize=lstPaths.getRowCount();
		for (int i=0;i<nSize;++i)
		{
			if (lstPaths.getDataRef(i,"FILENAME").toString().toLower()==strFileName.toLower())
			{
				strFilePath=lstPaths.getDataRef(i,"PATH").toString();
			}
		}
		if (strFilePath.isEmpty())
		{
			pStatus.setError(1,QString("Can not find picture %1 inside iPhoto album").arg(strFileName));
			return;
		}
	}
	

	QFileInfo info(strFilePath);
	QString strOutputFile=strThumbDir+"/"+info.completeBaseName()+"_full.jpg";

	//If file exists return it, else create it
	if (QFile::exists(strOutputFile))
	{
		if (!bReturnThumbAsBinary)
		{
			QUrl url(strAliasDirectory);
			QString strAliasDirEnc=url.toEncoded();
			strOutputFile=QUrl::toPercentEncoding(info.completeBaseName()+"_full.jpg");
			strThumbLink=QString(THUMB_CACHE_PREFIX)+"/"+strAliasDirEnc+"/"+strOutputFile;
			return;
		}

		QFile in_file(strOutputFile);
		if(!in_file.open(QIODevice::ReadOnly))
		{
			QString strErr="Error: failed to open input file: "+strOutputFile;
			pStatus.setError(1,strErr);
			return;
		}
		Ret_byteFileData=in_file.readAll();
		if (Ret_byteFileData.size()==0)
		{
			in_file.close();
			QString strErr="Error: failed to read file: "+strOutputFile;
			pStatus.setError(1,strErr);
			return;
		}
		return;
	}

	if(FileCruncher::IsRAWPicture(info.completeSuffix()))
	{
		//get thumb from raw:
		g_ThumbManager->GetThumbFromRaw(pStatus,strFilePath,strOutputFile);
		if (pStatus.IsOK())
		{

			if (!bReturnThumbAsBinary)
			{
				QUrl url(strAliasDirectory);
				QString strAliasDirEnc=url.toEncoded();
				strOutputFile=QUrl::toPercentEncoding(info.completeBaseName()+"_full.jpg");
				strThumbLink=QString(THUMB_CACHE_PREFIX)+"/"+strAliasDirEnc+"/"+strOutputFile;
				return;
			}

			QFile in_file(strOutputFile);
			if(!in_file.open(QIODevice::ReadOnly))
			{
				QString strErr="Error: failed to open input file: "+strOutputFile;
				pStatus.setError(1,strErr);
				return;
			}
			Ret_byteFileData=in_file.readAll();
			if (Ret_byteFileData.size()==0)
			{
				in_file.close();
				QString strErr="Error: failed to read file: "+strOutputFile;
				pStatus.setError(1,strErr);
				return;
			}
		}
	}
	else
	{
		QString strFileToRead=strFilePath;

		QFileInfo info_2(strFilePath);
		if ((info_2.suffix().toLower()!="jpg" && info_2.suffix().toLower()!="jpeg") || info_2.size()>FULL_PIC_MAX_SIZE)
		{
			g_ThumbManager->GetJpegFromPicture(pStatus,strFilePath,strOutputFile);
			if (pStatus.IsOK())
				strFileToRead=strOutputFile;
			else
				return;
		}

		//copy to thumb dir as full:
		QFile::copy(strFileToRead,strOutputFile);

		if (!bReturnThumbAsBinary)
		{
			QUrl url(strAliasDirectory);
			QString strAliasDirEnc=url.toEncoded();
			strOutputFile=QUrl::toPercentEncoding(info.completeBaseName()+"_full.jpg");
			strThumbLink=QString(THUMB_CACHE_PREFIX)+"/"+strAliasDirEnc+"/"+strOutputFile;
			return;
		}


		//read original file:
		QFile in_file(strFileToRead);
		if(!in_file.open(QIODevice::ReadOnly))
		{
			QString strErr="Error: failed to open input file: "+strFileToRead;
			pStatus.setError(1,strErr);
			return;
		}
		Ret_byteFileData=in_file.readAll();
		if (Ret_byteFileData.size()==0)
		{
			in_file.close();
			QString strErr="Error: failed to read file: "+strFileToRead;
			pStatus.setError(1,strErr);
			return;
		}
	}


}

void ServiceHandler::GetServerData(Status &pStatus, QString &Ret_strName,QByteArray &Ret_SrvPic,QString &Ret_strEmailSignature,int &nMajor,int &nMinor, int &nRev)
{
	Ret_strName=g_SettingsManager->GetServerName();
	QByteArray bytePicSmall;
	g_SettingsManager->GetServerThumb(bytePicSmall,Ret_SrvPic);

	nMajor=JAMES_PROGRAM_VERSION_MAJOR;
	nMinor=JAMES_PROGRAM_VERSION_MINOR;
	nRev=JAMES_PROGRAM_VERSION_REVISION;


	/*
	QDateTime datLastModify=g_SettingsManager->GetServerThumbLastModify();
	if (datLastModify!=datThumbLastModify)
	{
		QByteArray bytePicSmall;
		g_SettingsManager->GetServerThumb(bytePicSmall,Ret_SrvPic);
		datThumbLastModify=datLastModify;
	}
	else
	{
		Ret_SrvPic.clear();
	}
	*/

	DbRecordSet recMail;
	g_SettingsManager->GetEmailSettings(recMail);
	Ret_strEmailSignature=recMail.getDataRef(0,"SIGNATURE").toString();

}

void ServiceHandler::Logout(Status &pStatus,QString strSessionID)
{
	g_UserSessionManager->DeleteSession(pStatus,strSessionID);
}
//strProgVer = major.minor.rev
void ServiceHandler::Login(Status& pStatus, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode, QString strProgVer, QString strClientID, QString strPlatform)
{
	QString strProgCode=QString::number(nProgCode);

	if (strProgCode>"1.0.10")
	{
		if (nProgCode & DIGIT_APP_EVERPICS != DIGIT_APP_EVERPICS)
		{
			pStatus.setError(1,"EverPICs Bridge reported: authentication failed, wrong type of the client tried to connect!");
			return;
		}
	}

	/*
	//authenticate user:
	QByteArray bytePassword;
	if(!g_SettingsManager->GetUserPassword(strUserName,bytePassword))
		{pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	if(!Authenticator::AuthenticateFromWebService(strClientNonce,strAuthToken,strUserName,bytePassword))
		{pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}
	*/

	g_UserSessionManager->CreateSessionForWebService(pStatus,RetOut_strSessionID,strUserName,strAuthToken,strClientNonce,nProgCode,strProgVer,strClientID,strPlatform);
}

void ServiceHandler::RenameCatalog(Status &pStatus, QString strOldName, QString strNewName)
{
	QString strSession;
	g_UserSessionManager->CreateTempRootSessionForAuthenticatedConnection(pStatus,strSession);
	if (!pStatus.IsOK())
		return;

	QString strIP=g_UserSessionManager->GetClientIP();
	if (strIP=="127.0.0.1" || strIP=="localhost")
	{
		g_SettingsManager->LoadSettings();
		g_ThumbManager->RenameCatalog(pStatus,strOldName,strNewName); 
	}
	else
		pStatus.setError(1,"Access not allowed");

	Status err;
	g_UserSessionManager->DeleteSession(err,strSession);
}

void ServiceHandler::RenameCatalogClient(Status &pStatus, QString strOldName, QString strNewName)
{
	g_ThumbManager->RenameCatalogClient(pStatus,strOldName,strNewName);
}


void ServiceHandler::CreateCatalogClient(Status &pStatus, QString strCatalogName)
{
	g_ThumbManager->CreateCatalogClient(pStatus,strCatalogName);
	g_ThumbManager->StartProcessAll(); //schedule start
}


void ServiceHandler::GetPictureFromCatalog(Status &pStatus,QString strAliasDirectory,QString strFileName,int nWidth, int nHeight,int nWidthP, int nHeightP,QByteArray &byteFileData)
{
	//get both landscape and potrait  take bigger:
	int Ret_nTotalCount;
	int Ret_nCurrentPos;
	DbRecordSet Ret_Files;
	GetPicture(pStatus, Ret_nTotalCount, Ret_nCurrentPos, Ret_Files, strAliasDirectory, strFileName, 0, 0, nWidth, nHeight, true, true);
	DbRecordSet Ret_Files_2;
	GetPicture(pStatus, Ret_nTotalCount, Ret_nCurrentPos, Ret_Files_2, strAliasDirectory, strFileName, 0, 0, nWidthP, nHeightP, true, true);

	if (Ret_Files.getRowCount()==0 || Ret_Files_2.getRowCount()==0)
	{
		pStatus.setError(1,"Failed to rea file");
		return;
	}

	if (Ret_Files.getDataRef(0,"THUMB_BINARY").toByteArray().size()>Ret_Files_2.getDataRef(0,"THUMB_BINARY").toByteArray().size())
		byteFileData=Ret_Files.getDataRef(0,"THUMB_BINARY").toByteArray();
	else
		byteFileData=Ret_Files_2.getDataRef(0,"THUMB_BINARY").toByteArray();

}


//new format: Ret_Folders <alias,status> -> if all ok or could not be indexed, stop progress, else continue
void ServiceHandler::GetFolderStatus(Status &pStatus, DbRecordSet &Ret_Folders,int &Ret_nSettingsChanged)
{
	DbRecordSet lstDirs;
	g_SettingsManager->GetFolders(lstDirs);

	Ret_Folders.destroy();
	Ret_Folders.addColumn(QVariant::String,"ALIAS");
	Ret_Folders.addColumn(QVariant::Int,"STATUS");
	Ret_Folders.addColumn(QVariant::Int,"PIC_CNT");
	Ret_Folders.addColumn(QVariant::Int,"CNT");
	Ret_Folders.addColumn(QVariant::String,"IPHOTO_ID");
	Ret_Folders.addColumn(QVariant::String,"PATH");

	Ret_Folders.merge(lstDirs);

	g_ThumbManager->RecalculateCounts(Ret_Folders);

	Ret_Folders.removeColumn(Ret_Folders.getColumnIdx("PATH"));
	Ret_Folders.removeColumn(Ret_Folders.getColumnIdx("IPHOTO_ID"));


	//Ret_Folders.Dump();

	Ret_nSettingsChanged=g_SettingsManager->GetSettingsChangedNeedReloadFlag();
	g_SettingsManager->ClearSettingsChangedNeedReloadFlag();
}


void ServiceHandler::DownloadPicture(Status &pStatus, DbRecordSet &Ret_Files, QString strAliasDirectory,DbRecordSet &FileNames, int nFull, int nPicWidth,int nPicHeight, bool bReturnThumbAsBinary,bool bSkipAccess)
{
	g_SettingsManager->DefineFolders(Ret_Files);
	Ret_Files.addColumn(QVariant::String,"THUMB_LINK");
	Ret_Files.addColumn(QVariant::String,"DOC_LINK");
	Ret_Files.addColumn(QVariant::ByteArray,"THUMB_BINARY");
	Ret_Files.addColumn(QVariant::Int,"THUMB_WIDTH");
	Ret_Files.addColumn(QVariant::Int,"THUMB_HEIGHT");

	int nSize=FileNames.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strFileName=FileNames.getDataRef(i,0).toString();

		if (nFull==0)
		{
			DbRecordSet lstData;
			int nTotal;
			int nCurrent;
			GetPicture(pStatus,nTotal,nCurrent,lstData,strAliasDirectory,strFileName,0,0,nPicWidth,nPicHeight,true,bReturnThumbAsBinary);
			if (!pStatus.IsOK()) return;

			if (Ret_Files.getColumnCount()==0)
				Ret_Files=lstData;

			Ret_Files.merge(lstData);
		}
		else
		{
			Ret_Files.addRow();
			QByteArray Ret_byteFileData;
			QString strLink;
			DownloadDocument(pStatus, strAliasDirectory,strFileName,Ret_byteFileData,strLink,bReturnThumbAsBinary,bSkipAccess);
			if (!pStatus.IsOK()) return;
			Ret_Files.setData(Ret_Files.getRowCount()-1,"THUMB_BINARY",Ret_byteFileData);
			Ret_Files.setData(Ret_Files.getRowCount()-1,"THUMB_LINK",strLink);
			
		}
	}
}

void ServiceHandler::AddUser(Status &pStatus, DbRecordSet &rowUser, QString strCatalogName, QString strFirstName, QString strLastName,QString strEmail, QString strPhone)
{

	DbRecordSet lstUsers;
	g_SettingsManager->GetUsers(lstUsers);

	bool bFound=false;
	int nSize=lstUsers.getRowCount();
	for (int i=0; i<nSize;i++)
	{
		if (strFirstName.toLower().trimmed()==lstUsers.getDataRef(i,"FIRST_NAME").toString().toLower().trimmed() && strLastName.toLower().trimmed()==lstUsers.getDataRef(i,"LAST_NAME").toString().toLower().trimmed() && strEmail.toLower().trimmed()==lstUsers.getDataRef(i,"EMAIL").toString().toLower().trimmed() && strPhone.toLower().trimmed()==lstUsers.getDataRef(i,"SMS").toString().toLower().trimmed() )
		{
			rowUser=lstUsers.getRow(i);
			bFound=true;
			break;
		}
	}
	
	if (!bFound)
	{
		lstUsers.addRow();
		int nRow=nSize;

		bool bUserGenerated=false;

		//calc new username: 4left from lastname + 01 if already exists:
		QString strUserNameStart=strLastName.left(4);
		if (strUserNameStart.isEmpty())
			strUserNameStart=strFirstName.left(4);
		if (strUserNameStart.isEmpty())
			strUserNameStart="guest";

		QString strUserName=strUserNameStart;
		
		for (int i=0; i<1000;i++)
		{
			if(lstUsers.find("USERNAME",strUserName,true)<0)
			{
				bUserGenerated=true;
				break;
			}
			strUserName=strUserNameStart+QString::number(i);
		}

		if (!bUserGenerated)
		{
			pStatus.setError(1,"Username can not be generated automatically");
			return;
		}

		QString strPass=Authenticator::GenerateRandomPassword(5,true).toLower();


		lstUsers.setData(nRow,"USERNAME",strUserName);
		lstUsers.setData(nRow,"PASSWORD",strPass);
		lstUsers.setData(nRow,"FIRST_NAME",strFirstName);
		lstUsers.setData(nRow,"LAST_NAME",strLastName);
		lstUsers.setData(nRow,"EMAIL",strEmail);
		lstUsers.setData(nRow,"SMS",strPhone);
		lstUsers.setData(nRow,"COMPANY","");
		lstUsers.setData(nRow,"LAST_ACCESS_PROG_CODE","");
		lstUsers.setData(nRow,"SALUT",QString("Dear "+strFirstName));

		rowUser=lstUsers.getRow(nRow);
		g_SettingsManager->SetUsers(lstUsers);
		g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	}

	QString strUserName=rowUser.getDataRef(0,"USERNAME").toString();


	//add user to the catalog if not already if already then set read only right:

	bool bUserRightExist=false;
	DbRecordSet lstUserRights;
	g_SettingsManager->GetUserRights(lstUserRights);
	//lstUserRights.Dump();
	int nSelected=lstUserRights.find("USERNAME",strUserName);
	if (nSelected>0)
	{
		int nSelected_x=lstUserRights.find("ALIAS",strCatalogName,false,true,true,true);
		if (nSelected_x==1)
		{
			bUserRightExist=true;
			int nRow=lstUserRights.getSelectedRow();

			if (lstUserRights.getDataRef(nRow,"CAN_DOWNLOAD").toInt()==0)
			{
				lstUserRights.setData(nRow,"CAN_DOWNLOAD",1);
				g_SettingsManager->SetUserRights(lstUserRights);
				g_SettingsManager->SetSettingsChangedNeedReloadFlag();
			}
		}
	}

	if (!bUserRightExist)
	{
		lstUserRights.addRow();
		int nRow=lstUserRights.getRowCount()-1;
		lstUserRights.setData(nRow,"USERNAME",strUserName);
		lstUserRights.setData(nRow,"ALIAS",strCatalogName);
		lstUserRights.setData(nRow,"CAN_DOWNLOAD",1);
		lstUserRights.setData(nRow,"CAN_UPLOAD",0);
		lstUserRights.setData(nRow,"CAN_DELETE",0);
		lstUserRights.setData(nRow,"CAN_SEND_INVITE",0);
		lstUserRights.setData(nRow,"CAN_SEND_EMAIL",0);
		g_SettingsManager->SetUserRights(lstUserRights);
		g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	}

	






}

void ServiceHandler::MakeInvitation(Status &pStatus, QString &Ret_strBody, int nMessageType,QString strCatalogName, QString strFirstName, QString strLastName,QString strEmail, QString strPhone,int nIsHtml,QString strEmailSubject,QString strEmailBody)
{
	//check permission to delete:
	QString strUserNameLogged=g_UserSessionManager->GetUserNameExt();
	if(!g_SettingsManager->TestUserRight(strUserNameLogged,strCatalogName,SettingsManager::OP_SEND_INVITE))
	{
		pStatus.setError(1,"Insufficient access right!");
		return;
	}


	if (strFirstName.toLower()=="null")
		strFirstName="";
	if (strLastName.toLower()=="null")
		strLastName="";

	if (strFirstName.toLower()=="undefined")
		strFirstName="";
	if (strLastName.toLower()=="undefined")
		strLastName="";

	//MB want to make invite on subfolders, so....be carefull
	QString strAliasPath=strCatalogName;

	DbRecordSet lstCatalogs;
	g_SettingsManager->GetFolders(lstCatalogs);
	int nRow=lstCatalogs.find("ALIAS",strCatalogName,true);
	if (nRow<0)
	{
		//otribi: do prvog "/"
		if (strCatalogName.indexOf("/")>=0)
			strCatalogName=strCatalogName.split("/").at(0);

		nRow=lstCatalogs.find("ALIAS",strCatalogName,true);
		if (nRow<0)
		{
			pStatus.setError(1,"Catalog can not be found!");
			return;
		}
	}
	else
		strAliasPath=""; //no sub dir so clear


	DbRecordSet rowUser;
	AddUser(pStatus,rowUser,strCatalogName,strFirstName,strLastName,strEmail,strPhone);
	if (!pStatus.IsOK()) return;

	if (g_SettingsManager->GetSettingsChangedNeedReloadFlag())
	{
		g_SettingsManager->SaveSettings();
		g_SettingsManager->NotifyClientSettingsChangedNeedReload();
	}

	//based on message type generate invitation body:
	QString strUserName=rowUser.getDataRef(0,"USERNAME").toString();
	QString strUserPass=rowUser.getDataRef(0,"PASSWORD").toString();
	QString strServerID=g_AppServer->GetIniFile()->m_strAppServerID;

	//get catalog and email settings:
	DbRecordSet recMail;
	g_SettingsManager->GetEmailSettings(recMail);
	QString strFrom=recMail.getDataRef(0,"EMAIL_ADDRESS").toString();


	DbRecordSet rowCatalog=lstCatalogs.getRow(nRow);

	QString strCatalogBase64Pic = lstCatalogs.getDataRef(nRow,"THUMB_LARGE").toByteArray().toBase64();
	
	if (nMessageType==1)
	{
		if (rowUser.getDataRef(0,"EMAIL").toString().isEmpty())
		{
			pStatus.setError(1,"Invitation can not be sent as contact does not have email address!");
			return;
		}

		//-> sent all to emailhandler:
		if (strAliasPath.isEmpty())
			EmailHandler::SendEmail(pStatus,strServerID,rowCatalog,recMail,rowUser,strEmailSubject,strEmailBody);
		else
			EmailHandler::SendEmail(pStatus,strServerID,rowCatalog,recMail,rowUser,strEmailSubject,strEmailBody,strAliasPath);


	}
	else
	{
		if (rowUser.getDataRef(0,"SMS").toString().isEmpty())
		{
			pStatus.setError(1,"Invitation can not be sent as contact does not have phone number address!");
			return;
		}
		//send mail... format as <<gdfgdgf>>
		QString strEmail="See my pics here:%1\n\r\n\r";
		strEmail+="or use the iPhone app \"EverPics\" and paste this message into the ID field: %2\n\r";

		QString strLink;
		if (strAliasPath.isEmpty())
			strLink=QString(FFH_CONNECT_SERVER_MASTER_IP)+"/invite/"+strServerID+"/web/invite.html?catalog="+QUrl::toPercentEncoding(strCatalogName);
		else
			strLink=QString(FFH_CONNECT_SERVER_MASTER_IP)+"/invite/"+strServerID+"/web/invite.html?catalog="+QUrl::toPercentEncoding(strAliasPath);

		strLink+="&user="+QUrl::toPercentEncoding(strUserName)+"&pass="+QUrl::toPercentEncoding(strUserPass.toLatin1().toBase64());
		QString strIPhone="<<"+strServerID+"//"+strUserName+"//"+strUserPass+">>";

		//QString strSubject="PFH invitation";
		Ret_strBody=strEmail.arg(strLink).arg(strIPhone);
	}

	//qDebug()<<Ret_strBody;

}

void ServiceHandler::GetCatalogsAll(Status &pStatus, DbRecordSet &Ret_Catalogs)
{
	DbRecordSet lstUserRights;
	g_SettingsManager->DefineFolders(Ret_Catalogs);
	Ret_Catalogs.addColumn(QVariant::Int,"CNT");
	g_SettingsManager->DefineUserRights(lstUserRights);
	Ret_Catalogs.copyDefinition(lstUserRights,false);

	DbRecordSet lstDirs;
	g_SettingsManager->GetFolders(lstDirs);
	lstDirs.addColumn(QVariant::Int,"CNT");

	int nProgCode=g_UserSessionManager->GetUserProgCode();

	//get file count & status:
	g_ThumbManager->RecalculateCounts(lstDirs);


	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Finished Get Catalog Files Step 1");


	//get all rights for users (if root) then by default it has all:
	g_SettingsManager->GetUserRights(lstUserRights);
	lstUserRights.find("USERNAME",g_UserSessionManager->GetUserNameExt());
	lstUserRights.deleteUnSelectedRows();
	lstUserRights.removeColumn(lstUserRights.getColumnIdx("USERNAME"));

	Ret_Catalogs.merge(lstDirs);
	Ret_Catalogs.clearSelection();

	int nSize=Ret_Catalogs.getRowCount();

	if(g_UserSessionManager->GetUserNameExt()==g_SettingsManager->GetAdminUserName())
	{
		//get default user rights:
		DbRecordSet rowDefRights;
		g_SettingsManager->CreateDefaultUserRights(rowDefRights);
		rowDefRights.removeColumn(rowDefRights.getColumnIdx("ALIAS"));
		rowDefRights.removeColumn(rowDefRights.getColumnIdx("USERNAME"));

		//for each directory: find 1 right, if not exists then set all true:
		for (int i=0;i<nSize;i++)
		{
			Ret_Catalogs.assignRow(i,rowDefRights,true);
		}
	}
	else
	{
		//for each directory: find 1 right, if not exists then set all true:
		for (int i=0;i<nSize;i++)
		{
			int nRow=lstUserRights.find("ALIAS",Ret_Catalogs.getDataRef(i,"ALIAS").toString(),true);
			if (nRow>=0)
			{
				DbRecordSet rowTmp = lstUserRights.getRow(nRow);
				Ret_Catalogs.assignRow(i, rowTmp, true);
			}
			else
			{
				Ret_Catalogs.selectRow(i);
			}
		}
	}
	Ret_Catalogs.deleteSelectedRows(); //user does not have access to other catalogs if not defined


	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Get Catalog Step 2");

	Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("PATH"));
	//Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("FILETYPES"));
	Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("FILETYPES_CODE"));
	Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("USERNAME"));

	Ret_Catalogs.sort("ALIAS");

	if (Ret_Catalogs.getRowCount()!=0)
	{
		if (!g_SettingsManager->TestModuleRight(g_UserSessionManager->GetUserProgCode(),SettingsManager::OP_MORE_THEN_ONE_CATALOG))
		{
			Ret_Catalogs.clearSelection();
			Ret_Catalogs.selectRow(0);
			Ret_Catalogs.deleteUnSelectedRows();
		}
	}


	//MB: RS: A.FFH.05. Information: show only catalogs that are indexed...
	DbRecordSet lstCatalogStatus;

	QHash<QString,int> hshStatus;
	g_ThumbManager->GetCatalogStatusList(hshStatus); //if not in list then not indexed if in then indexing or ok
	//g_ThumbManager->GetFolderStatus(pStatus,lstCatalogStatus); .-> to slow
	//if (!pStatus.IsOK()) return;

	//Ret_Catalogs.Dump();

	//Ret_Catalogs.Dump();

	nSize=Ret_Catalogs.getRowCount();
	for (int i=nSize-1;i>=0;i--)
	{
		QString strAlias = Ret_Catalogs.getDataRef(i,"ALIAS").toString();
		int nStatus= hshStatus.value(strAlias,STATUS_NOT_INDEXED);
		if (nStatus==STATUS_NOT_INDEXED) // || nStatus == STATUS_COULD_NOT_BE_INDEXED)
		{
				Ret_Catalogs.deleteRow(i);
		}
	}

	//Ret_Catalogs.Dump();
	
}

void ServiceHandler::HidePicture(Status &pStatus, QString strAliasDirectory, QString strFileName,int nHide)
{
	//check permission to delete:
	QString strUserName=g_UserSessionManager->GetUserNameExt();
	if(!g_SettingsManager->TestUserRight(strUserName,strAliasDirectory,SettingsManager::OP_DELETE))
	{
		pStatus.setError(1,"Insufficient access right!");
		return;
	}

	if (g_AppServer->GetIniFile()->m_nEnablePermanentDelete) //MB:
		DeleteDocument(pStatus,strAliasDirectory,strFileName);
	else
		g_SettingsManager->HideFile(strAliasDirectory,strFileName,nHide);
}


void ServiceHandler::GetHidePictureList(Status &pStatus, QString strAliasDirectory, DbRecordSet &Ret_Files, int nPicWidth,int nPicHeight, bool bReturnThumbAsBinary)
{
	Ret_Files.destroy();
	Ret_Files.addColumn(QVariant::String,"PATH");
	Ret_Files.addColumn(QVariant::String,"NAME");
	Ret_Files.addColumn(QVariant::String,"EXT");
	Ret_Files.addColumn(QVariant::String,"FILENAME");
	Ret_Files.addColumn(QVariant::DateTime,"LAST_MODIFY");
	Ret_Files.addColumn(QVariant::Int,"SIZE");
	Ret_Files.addColumn(QVariant::String,"THUMB_LINK");
	Ret_Files.addColumn(QVariant::String,"DOC_LINK");
	Ret_Files.addColumn(QVariant::ByteArray,"THUMB_BINARY");
	Ret_Files.addColumn(QVariant::Int,"THUMB_WIDTH");
	Ret_Files.addColumn(QVariant::Int,"THUMB_HEIGHT");
	Ret_Files.addColumn(QVariant::String,"DISPLAYNAME"); //marin wants this to be used!!! jaaaaaaaahuuuuu

	pStatus.setError(0);
	//->from settings manager extract list...
	DbRecordSet lstPaths;
	g_SettingsManager->GetHidePictureList(strAliasDirectory,lstPaths);
	int nSize=lstPaths.getRowCount();
	if (nSize==0)
		return;

	//get path of directory
	//read files from catalog actual dir
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strAliasDirectory,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;


	QString strThumbSuffix=g_ThumbManager->GetThumbSuffix(nPicWidth,nPicHeight);
	QUrl url(strAliasDirectory);
	QString strAliasDirEnc=url.toEncoded();

	Ret_Files.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		QString strFilePath=strDirPath+QString("/")+lstPaths.getDataRef(i,"FILENAME").toString();
		Ret_Files.setData(i,"PATH",strFilePath);
		QFileInfo info_temp(lstPaths.getDataRef(i,"FILENAME").toString());
		Ret_Files.setData(i,"NAME",info_temp.completeBaseName());
		Ret_Files.setData(i,"FILENAME",lstPaths.getDataRef(i,"FILENAME"));
		Ret_Files.setData(i,"DISPLAYNAME",lstPaths.getDataRef(i,"DISPLAYNAME"));
		
		//set thumb links:
		QString strFileName=Ret_Files.getDataRef(i,"FILENAME").toString();
		strFileName=QUrl::toPercentEncoding(strFileName);
		QString strThumbFileName=Ret_Files.getDataRef(i,"NAME").toString()+strThumbSuffix+".jpg";
		strThumbFileName=QUrl::toPercentEncoding(strThumbFileName);

		QString strThumbLink=QString(THUMB_CACHE_PREFIX)+"/"+strAliasDirEnc+"/"+strThumbFileName;
		QString strLink=QString(CATALOG_STORAGE_PREFIX)+"/"+strAliasDirEnc+"/"+strFileName;

		Ret_Files.setData(i,"THUMB_LINK",strThumbLink);
		Ret_Files.setData(i,"DOC_LINK",strLink);

		QString strThumbFilePath=g_ThumbManager->GetThumbDirRoot()+"/"+strAliasDirectory+"/"+Ret_Files.getDataRef(i,"NAME").toString()+strThumbSuffix+".jpg";
		if (!QFile::exists(strThumbFilePath))
		{
			Ret_Files.setData(0,"THUMB_LINK","");
			//Ret_Files.setData(0,"DOC_LINK","");
		}
		else if (bReturnThumbAsBinary)
		{
			//MP: load binary
			QFile in_file(strThumbFilePath);
			if(in_file.open(QIODevice::ReadOnly))
			{
				QByteArray buffer=in_file.readAll();
				Ret_Files.setData(i,"THUMB_BINARY",buffer);

				//retrieve height and width:
				int nWidth,nHeight;
				if(FileCruncher::GetJpegSize((unsigned char *)buffer.data(), buffer.size(),&nWidth,&nHeight))
				{
					Ret_Files.setData(i,"THUMB_WIDTH",nWidth);
					Ret_Files.setData(i,"THUMB_HEIGHT",nHeight);
				}
			}
		}
	}


	//Ret_Files.Dump();



}

void ServiceHandler::RenamePicture(Status &pStatus, QString strAliasDirectory, QString strFileName, QString strDisplayName)
{
	g_SettingsManager->RenameFile(strAliasDirectory,strFileName,strDisplayName);
}



void ServiceHandler::UnpublishCatalogClient(Status &pStatus, QString strAliasDirectory)
{
	g_ThumbManager->UnpublishCatalogClient(pStatus,strAliasDirectory);

}
