#include "httpmessagehandler.h"
#include <QUrl>

#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;
#include "usersessionmanager.h"
extern UserSessionManager *g_UserSessionManager;



//specific handler for upload picture: find header with value 'filename'
//authenticate: existing - session_id as username
bool HTTPMessageHandler::HandleMessage(Status &err, QList<HTTPRequest> lstRequests, QByteArray *pBufResponse, QString strHTTPPath)
{
	int nSessionIdx=-1;
	int nFileNameIdx=-1;
	int nCatalogNameIdx=-1;
	int nFileName_ManualIdx=-1;
	int nOverwriteIdx=-1;

	
	QString strFileName="test.jpg";

	int nSize=lstRequests.count();
	for (int i=0;i<nSize;i++)
	{
		//qDebug()<<lstRequests.at(i).toString();

		if (lstRequests.at(i).value("name")=="session")
		{
			nSessionIdx=i;
		}
		else if (lstRequests.at(i).value("name")=="catalog")
		{
			nCatalogNameIdx=i;
		}
		else if (lstRequests.at(i).value("name")=="filename")
		{
			nFileName_ManualIdx=i;
		}
		else if (lstRequests.at(i).value("name")=="overwrite")
		{
			nOverwriteIdx=i;
		}
		else if (!lstRequests.at(i).value("filename").isEmpty())
		{
			nFileNameIdx=i;
		}
		
		
	}

	///fields are missing
	if (nSessionIdx == -1 || nCatalogNameIdx==-1)
	{
		err.setError(StatusCodeSet::ERR_HTTP_MULTIPART_BAD_REQUEST);
		return false;
	}

	//strFileName=QUrl::fromPercentEncoding(strFileName.toLatin1());
	QString strCatalogName=QUrl::fromPercentEncoding(QString(*lstRequests[nCatalogNameIdx].GetBody()).trimmed().toLatin1());
	QString strSession=QString(*lstRequests[nSessionIdx].GetBody()).trimmed();
	bool bOverWrite = (QString(*lstRequests[nOverwriteIdx].GetBody()).trimmed()=="1");

	if (nFileName_ManualIdx!=-1)
	{
		strFileName = QUrl::fromPercentEncoding(QString(*lstRequests[nFileName_ManualIdx].GetBody()).trimmed().toLatin1());
	}
	else if (nFileNameIdx!=-1)
	{
		strFileName = QUrl::fromPercentEncoding(lstRequests.at(nFileNameIdx).value("filename").toLatin1());
	}

	//qDebug()<<lstRequests.at(nSessionIdx).value("username");

	//try to authenticate user:
	if (g_UserSessionManager->GetUserNameExt(strSession).isEmpty())
	{
		err.setError(StatusCodeSet::ERR_HTTP_MULTIPART_AUTHENTICATION_FAILED);
		return false;
	}
	
	
	QByteArray data=*lstRequests[nFileNameIdx].GetBody(); //get data-> ? if compressed it will be automatic
	g_ServiceHandler->UploadDocument(err,strCatalogName,strFileName,data,bOverWrite,strSession);

	if (err.IsOK())
		return true;
	else
		return false;
}
