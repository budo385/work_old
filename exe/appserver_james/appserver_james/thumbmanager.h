#ifndef THUMBMANAGER_H
#define THUMBMANAGER_H

#include "common/common/dbrecordset.h"
#include "common/common/backgroundtaskinterface.h"
#include <QMutex>
#include <QStringList>
#include <QReadWriteLock>
#include "common/common/status.h"
#include "common_constants.h"


typedef QHashIterator<QString,int> HashIterator;


class ThumbManager : public QObject, public BackgroundTaskInterface 
{
	Q_OBJECT


public:

	ThumbManager(QObject *parent=0);
	~ThumbManager();

	void	ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");
	void	CleanGarbage();

	QString GetThumbDirRoot(){return m_strOutThumbDirRoot;};
	bool	Initialize(QString strOutThumbDir, int nJpegQuality=40);
	void	AddThumbTypeForProcess(int nWidth,int nHeight,QString strSuffix,bool bKeepAspectRatio=true);
	QString GetThumbPathBasedOnCatalogPath(QString strCatalogDirPath);
	bool	IsSupporttedThumbType(int nWidth,int nHeight,bool bKeepAspectRatio);
	QString GetThumbSuffix(int nWidth,int nHeight);
	void	RenameCatalog(Status &pStatus, QString strOldName, QString strNewName);
	void	RenameCatalogClient(Status &pStatus, QString strOldName, QString strNewName);
	void	CreateCatalogClient(Status &pStatus, QString strName);
	void	RotateThumb(Status &pStatus, QString strCatalogDirPath, QString strFileName, int nAngle=90);
	void	RotateThumbByExifOrienatation(Status &pStatus, QString strFilePath);
	void	CreateOneThumb(Status &pStatus, QString strPathIn, QString strPathOut, int nPicWidth,int nPicHeight);
	void	GetThumbFromRaw(Status &pStatus, QString strFileIn, QString strFileOut);
	void	GetJpegFromPicture(Status &pStatus, QString strFileIn, QString strFileOut);
	
	void	GetCatalogStatusList(QHash<QString,int> &hshStatus);
	void	GetCatalogCntList(QHash<QString,int> &hshStatus);
	void	UnpublishCatalogClient(Status &pStatus, QString strAliasDirectory);
	void	RecalculateCounts(DbRecordSet &Ret_Folders);

	//for async start:
	void	StartProcessAll(){emit SignalProcessAll();}
	
signals:
	void	SignalProcessAll();

public slots:
	void	ProcessAll();
	void	CreateThumbsFromAlias(QString strCatalogDirPath);


	
private:
	void	RotateGenToolExe(Status &pStatus,QString strFile, QString strFileOut, int nAngle, int nWidth, int nHeight, int nJpegQuality);
	void	SetCatalogStatus(QString strCatalogName, int nStatus);
	int		GetCatalogStatus(QString strCatalogName);
	void	SetCatalogCnt(QString strCatalogName, int nCount);
	bool	CreateThumbs(QString strDirInPath,QString strDirOutPath,QString straAllowedFileTypes);
	bool	CreateThumbsiPhoto(QString strAlbumIPhotoID,QString strDirOutPath,QString straAllowedFileTypes);

	QString							m_strOutThumbDirRoot;
	QMutex							m_mutexProcessAll;
	QReadWriteLock					m_lckPaths;
	QHash<QString,QDateTime>		m_lstProcessedPaths;
	QStringList						m_lstCurrentPathProcessing;
	QStringList						m_lstThumbSufix;
	DbRecordSet						m_lstThumbSettings;
	int								m_nJpegQuality;

	QReadWriteLock					m_lstCatalogStatusLock;
	QReadWriteLock					m_lstCatalogCntLock;
	QHash<QString,int>				m_lstCatalogStatus;
	QHash<QString,int>				m_lstCatalogFileCnt;
};

#endif // THUMBMANAGER_H
