#!/bin/bash
# Script to build everDOCs-Bridge package of Mac OS X

CERT_NAME="3rd Party Mac Developer Application: Helix Business-Soft AG"
CERT_INSTALLER="3rd Party Mac Developer Installer: Helix Business-Soft AG"
APP_NAME=everDOCs-Bridge
APP_BUNDLE=$APP_NAME.app
SETUP_NAME=EverDOCsBridgeAppStore
APP_BUNDLE_ID=com.everDOCs.everDOCsBridge 
APP_VERSION=1.0.0

# comment to prevent cleanup - faster rebuilds
#CLEAN_OBJS=1

echo "------------------------------------------------------------"
echo " Compiling $APP_NAME"
echo "------------------------------------------------------------"

# call with "./compile.sh sign_only" to skip compilation part 
if [[ "$1" != "sign_only" ]]
then

#before updating, store (our own) script's date
OLD=`stat -f '%m' $0`

#update all sources from svn
cd ../../
svn up
cd exe/appserver_john

# compare it with old
NEW=`stat -f '%m' $0`
if [ "$NEW" != "$OLD" ]; then
	echo "Compile script has changed, restarting"
	exec bash $0
	exit
fi

chmod +x ./compile.sh

#cleanup builds
if [ "$CLEAN_OBJS" == "1" ]; then
	rm -rf ./Debug
fi 

#compile NatTraversal
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/NATTraversal/NATTraversal/libnatpmp/ clean
fi
make -C ../../lib/NATTraversal/NATTraversal/libnatpmp/ libnatpmp.a  || exit 1
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/NATTraversal/NATTraversal/miniupnpc/ clean
fi 
make -C ../../lib/NATTraversal/NATTraversal/miniupnpc/ libminiupnpc.a   || exit 1
cp ../../lib/NATTraversal/NATTraversal/libnatpmp/libnatpmp.a ./ || exit 1
cp ../../lib/NATTraversal/NATTraversal/miniupnpc/libminiupnpc.a ./ || exit 1
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o ../../lib/NATTraversal/Makefile ../../lib/NATTraversal/NATTraversal/NATTraversal.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make -C ../../lib/NATTraversal clean
fi
make -C ../../lib/NATTraversal all  || exit 1
cp ../../lib/NATTraversal/libNATTraversal.a  ./ || exit 1

#compile common
if [ "$CLEAN_OBJS" == "1" ]; then
 rm -rf ../../lib/common/common/Debug
fi 
rm -f ../../lib/common/common/Makefile
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o ../../lib/common/common/Makefile ../../lib/common/common/common.pro
if [ "$CLEAN_OBJS" == "1" ]; then
 make -C ../../lib/common/common/ clean
fi
make -C ../../lib/common/common/ all  || exit 1
cp ../../lib/debug/libcommon.a  ./ || exit 1

#compile trans
rm -f ../../lib/trans/trans/Makefile
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o ../../lib/trans/trans/Makefile ../../lib/trans/trans/trans.pro
if [ "$CLEAN_OBJS" == "1" ]; then
 make -C ../../lib/trans/trans/ clean
fi
make -C ../../lib/trans/trans/ all  || exit 1
cp ../../lib/debug/libtrans.a  ./ || exit 1

#compile appserver_john_common
rm -f ./appserver_john_common/Makefile
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o ./appserver_john_common/Makefile ./appserver_john_common/appserver_john_common.pro
if [ "$CLEAN_OBJS" == "1" ]; then
 make -C ./appserver_john_common/ clean
fi 
make -C ./appserver_john_common/ all  || exit 1 
cp ./Debug/libappserver_john_common.a  ./ || exit 1

#compile db
rm -f ../../lib/common/common/Makefile
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o ../../lib/common/common/Makefile ../../lib/common/common/common.pro
if [ "$CLEAN_OBJS" == "1" ]; then
 make -C ../../lib/common/common/ clean
fi
make -C ../../lib/common/common/ all  || exit 1
cp ../../lib/debug/libcommon.a  ./ || exit 1

#compile appserver_john
rm -f ./appserver_john/Makefile
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o ./appserver_john/Makefile ./appserver_john/appserver_john.pro
if [ "$CLEAN_OBJS" == "1" ]; then
 make -C ./appserver_john/ clean
fi
make -C ./appserver_john/ all  || exit 1

#compile appserver_john_gui
rm -f ./appserver_john_gui/Makefile
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o ./appserver_john_gui/Makefile ./appserver_john_gui/appserver_john_gui.pro
if [ "$CLEAN_OBJS" == "1" ]; then
 make -C ./appserver_john_gui/ clean
fi 
make -C ./appserver_john_gui/ all  || exit 1

#compile GenTool
rm -f ../../tool/GenTool/GenTool/Makefile
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o ../../tool/GenTool/GenTool/Makefile ../../tool/GenTool/GenTool/GenTool.pro
if [ "$CLEAN_OBJS" == "1" ]; then
 make -C ../../tool/GenTool/GenTool/ clean
fi 
make -C ../../tool/GenTool/GenTool/ all  || exit 1

#final compilation
rm -f ./Makefile
rm -f ./appserver_john/Makefile
qmake -makefile  -spec macx-g++ CONFIG+="x86 x86_64" -Wall -o Makefile ./appserver_john.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make clean
fi  
make all || { echo "appserver_john compilation failed"; exit 1; }

#compile RawLib
cd ../../tool/RawLib/
if [ "$CLEAN_OBJS" == "1" ]; then
 make clean
fi
make all || echo "RawLib compilation failed"
cd ../../exe/appserver_john
mkdir -m 755 -p ./Debug/$APP_BUNDLE/Contents/MacOS/
cp -f ../../tool/RawLib/bin/RawLib  ./Debug/$APP_BUNDLE/Contents/MacOS/  || echo "RawLib copy failed"

# do the bundle thing on all other non-main Qt programs so they will be relinked to framework OQ
macdeployqt ./Debug/everDOCs-Service.app

#copy all the required files to the everDOCs-Service.app bundle
echo "Copy files to bundle"
#cp ./Debug/everDOCs-Bridge.app/Contents/MacOS/everDOCs-Bridge  ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
cp ./Debug/everDOCs-Service.app/Contents/MacOS/everDOCs-Service  ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
cp ../../_data/Translation/EXB_Manager_de.qm ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
#cp ../../exe/appserver_john/appserver_john_gui/Resources/Sokrates.icns ./Debug/$APP_BUNDLE/Contents/Resources/ || echo "Some files were not copied"

mkdir -m 755 -p ./Debug/$APP_BUNDLE/Contents/MacOS/
cp -pRv ../../_data/DefSettings/appserver ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings > /dev/null || echo "Some files were not copied"
#delete .svn folders
find ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/ -name ".svn" | xargs rm -rf \;

#copies all the Qt dependencies into the main bundle
macdeployqt ./Debug/$APP_BUNDLE

# add scripts and resources
mkdir -p ./Debug/buildroot/Applications
cp -R ./Debug/$APP_BUNDLE ./Debug/buildroot/Applications/

mkdir -p ./Debug/Scripts
cp -R ../../deploy/Setup/FFHServer/uninstallosx.sh ./Debug/Scripts/preupgrade
chmod +x ./Debug/Scripts/preupgrade

# ne radi!
#cp -R ../../deploy/Setup/FFHServer/postinstallosx.sh ./Debug/Scripts/postflight
#chmod +x ./Debug/Scripts/postflight

mkdir -p ./Debug/Resources
cp -R ../../deploy/Setup/FFHServer/license.txt ./Debug/Resources/License.txt

# use our own Info file to overwrite auto-generated one
cp -R ../../deploy/Setup/JohnServer/info.plist ./Debug/$APP_BUNDLE/Contents/info.plist

cp -R ../../_data/EXB_EmailTemplates/everDOCs_Invitation.html ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/everDOCs_Invitation.html

cp -R ../../_data/EXB_EmailTemplates/everDOCs_Doc.html ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/everDOCs_Doc.html

cp -R ../../_data/Deploy/AppServerDeploy/settings/server.cert ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/server.cert

cp -R ../../_data/Deploy/AppServerDeploy/settings/server.pkey ./Debug/$APP_BUNDLE/Contents/MacOS/template_settings/server.pkey

# remove OSX resource forks
find ./Debug/buildroot/ -name ".DS_Store" -exec rm -f {} \;

# NOTE: this non-signed version is for ordinary download (not for MacStore!!!)
# create flat package (.pkg)
/Developer/Applications/Utilities/PackageMaker.app/Contents/MacOS/PackageMaker -AppleLanguages "(English)" -r ./Debug/$APP_BUNDLE/ -o ./Debug/EverDOCsBridgeInstall.pkg -v $APP_VERSION -t "everDOCs Bridge Setup" -i $APP_BUNDLE_ID --no-relocate --verbose --target 10.5 --scripts ./Debug/Scripts/ --resources ./Debug/Resources/

fi

echo "signing the APP";
# "Each of these must be signed independently"
# http://www.manpagez.com/man/1/codesign/
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/MacOS/everDOCs-Bridge  || echo "Signing everDOCs-Bridge failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/MacOS/everDOCs-Service  || echo "Signing everDOCs-Service failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/MacOS/RawLib  || echo "Signing RawLib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtCore.framework/Versions/4/QtCore   || echo "Signing QtCore failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtDeclarative.framework/Versions/4/QtDeclarative  || echo "Signing QtDeclarative failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtGui.framework/Versions/4/QtGui || echo "Signing QtGui failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtNetwork.framework/Versions/4/QtNetwork  || echo "Signing QtNetwork failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtScript.framework/Versions/4/QtScript || echo "Signing QtScript failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtSql.framework/Versions/4/QtSql || echo "Signing QtSql failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtSvg.framework/Versions/4/QtSvg || echo "Signing QtSvg failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtXml.framework/Versions/4/QtXml  || echo "Signing QtXml failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/Frameworks/QtXmlPatterns.framework/Versions/4/QtXmlPatterns  || echo "Signing QtXmlPatterns failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/accessible/libqtaccessiblewidgets.dylib   || echo "Signing libqtaccessiblewidgets.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/bearer/libqgenericbearer.dylib  || echo "Signing libqgenericbearer.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/codecs/libqcncodecs.dylib   || echo "Signing libqcncodecs.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/codecs/libqjpcodecs.dylib   || echo "Signing libqjpcodecs.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/codecs/libqkrcodecs.dylib   || echo "Signing libqkrcodecs.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/codecs/libqtwcodecs.dylib   || echo "Signing libqtwcodecs.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/graphicssystems/libqtracegraphicssystem.dylib    || echo "Signing libqtracegraphicssystem.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqgif.dylib     || echo "Signing libqgif.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqico.dylib     || echo "Signing libqico.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqjpeg.dylib     || echo "Signing libqjpeg.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqmng.dylib     || echo "Signing libqmng.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/imageformats/libqtiff.dylib     || echo "Signing libqtiff.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/qmltooling/libqmldbg_tcp.dylib      || echo "Signing libqmldbg_tcp.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/sqldrivers/libqsqlite.dylib       || echo "Signing libqsqlite.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/sqldrivers/libqsqlodbc.dylib        || echo "Signing libqsqlodbc.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE/Contents/PlugIns/sqldrivers/libqsqlpsql.dylib         || echo "Signing libqsqlpsql.dylib failed";
codesign -s "$CERT_NAME" -fv ./Debug/$APP_BUNDLE	|| echo "Signing entire bundle failed"; 

#TEST automatically sign all executable files
#echo "Automatic binary signing code"
#TOFIX no xarg on OSx
#find ./Debug/$APP_BUNDLE/ -type f -perm u+x -print | xarg codesign -s "$CERT_NAME" -fv {} || echo "Signing {} failed"

#http://developer.apple.com/devcenter/mac/documents/submitting.html
#Quote: "Note: Using the PackageMaker application to archive your application is not supported"
productbuild --component ./Debug/$APP_BUNDLE /Applications --sign "$CERT_INSTALLER" ./Debug/$SETUP_NAME.pkg

# use installer to test the archive validity
# http://developer.apple.com/library/mac/#documentation/Darwin/Reference/ManPages/man8/installer.8.html 
echo "Testing the installer"
say "Testing the installer"
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo -ne '\007'
echo "helix1" | sudo installer -verbose -store -pkg ./Debug/$SETUP_NAME.pkg  -target /
