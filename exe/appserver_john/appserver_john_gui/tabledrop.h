#ifndef TABLEDROP_H
#define TABLEDROP_H

#include <QTableWidget>

#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QUrl>

class TableDrop : public QTableWidget
{
	Q_OBJECT

public:
	TableDrop(QWidget *parent = 0);
	~TableDrop();


protected:
	void dragEnterEvent(QDragEnterEvent *event);
	void dragMoveEvent(QDragMoveEvent *event);
	void dragLeaveEvent(QDragLeaveEvent *event);
	void dropEvent(QDropEvent *event);

};

#endif // TABLEDROP_H
