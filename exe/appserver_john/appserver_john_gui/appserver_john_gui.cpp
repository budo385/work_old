#include "appserver_john_gui.h"
#include <QMessageBox>
#include <QImageReader>
#include <QFileDialog>
#include "common/common/config_version_john.h"
#include "trans/trans/resthttpclient.h"
#include "dlgusers.h"
#include "dlgdefaultuserrights.h"
#include "pichelper.h"
#include "trans/trans/tcphelper.h"
#include "dlgemailsettings.h"
#include "trans/trans/smtpclient.h"
#include "dlguserchange.h"
#include "dlgassignedusers.h"
#include "dlgcatalogs.h"
#include <QMenu>
#include <QClipboard>
#include <QApplication>
#include <QSettings>
#include "common/common/datahelper.h"
#include "common_constants.h"
#include "dlgsendemailbody.h"
#include <QDesktopServices>
#include "emailhandler.h"
#include "../appserver_john_common/iphotohandler.h"
#include "trans/trans/httpreadersync.h"
#include "dlguserandpass.h"
#include "dlgaskforservicelogon.h"
#include "common/common/logger.h"
#include "common/common/zipcompression.h"


QString g_strLastDir_FileOpen="";

Logger	g_Logger;




#ifdef __APPLE__

	#define HTML_START_F1				"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:13pt; font-weight:bold; font-style:normal;\">"
	#define HTML_END_F1					"</span></p></body></html>"
	#define HTML_START_F1_LARGE			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt; font-weight:bold; font-style:normal;\">"
	#define HTML_START_F1_BLUE			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; color: #c6e9fe; font-style:normal;\">"
	#define HTML_START_F1_BLACK			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; color: black; font-style:normal;\">"

	#define HTML_END_F1_LARGE			"</span></p></body></html>"
	#define HTML_1_START_BUTTON			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:12pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:15pt; font-weight:bold; font-style:normal;\">%1</span></p></body></html>"
	#define HTML_2_SERVER_ONLINE		"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:16pt; font-weight:bold; font-style:italic; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:19pt; font-weight:bold; font-style:italic;\">%1<br>%2</span></p></body></html>"
	#define HTML_3_CONN_STRING			"<html><p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Arial'; font-size:17pt;\">Bridge ID: </span><a href=\"http://1\" ><span style=\"font-family:Arial; color: blue; font-size:17pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></p></html>"
	#define HTML_3_URL_STRING			"<html><div><a href=\"%1\" ><span style=\"font-family:Arial; color: blue; font-size:17pt; font-weight:bold; font-style:normal; text-decoration:none;\">%2</span></a></div></html>"
	#define HTML_3_URL_STRING_WHITE		"<html><div><a href=\"%1\" ><span style=\"font-family:Arial; color: white; font-size:17pt; font-weight:bold; font-style:normal; text-decoration:none;\">%2</span></a></div></html>"
	#define HTML_4_HELP_1				"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:11pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"http://link_1\"><span style=\" font-size:14pt; font-weight:600; text-decoration: underline; color:yellow;\">%1</span></a></p></body></html>"
	#define HTML_5_STATUS_STRING		"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:11pt; font-weight:bold; font-style:italic; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:11pt; font-weight:bold; font-style:italic;\">%1</span></p></body></html>"
	#define HTML_5_STATUS_URL			"<a href=\"http://link_1\"><span style=\" font-size:11pt; font-weight:600; text-decoration: underline; color:#0000ff;\">How to solve?</span></a>"
	#define HTML_LABEL_INFO				"<html><div><a href=\"http://1\" ><span style=\"font-family:Arial; color: white; font-size:16pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>"
	#define HTML_PORT_FORWARD			"<html><p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Arial'; font-size:16pt;\"></span><a href=\"http://1\" ><span style=\"font-family:Arial; color: green; font-size:16pt; font-weight:bold; font-style:italic; text-decoration:none;\">%1</span></a></p></html>"
	#define HTML_URL_STRING_SMALL		"<html><div><a href=\"%1\" ><span style=\"font-family:Arial; color: #c6e9fe; font-size:12pt; font-style:normal; text-decoration:none;\">%2</span></a></div></html>"
	#define HTML_PORT_FORWARD_SMALL		"<html><p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Arial'; font-size:14pt;\"></span><a href=\"http://1\" ><span style=\"font-family:Arial; color: white; font-size:12pt; font-weight:bold; font-style:italic; text-decoration:none;\">%1</span></a></p></html>"

#else

	#define HTML_START_F1					"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; font-style:normal;\">"
	#define HTML_END_F1						"</span></p></body></html>"
	#define HTML_START_F1_LARGE				"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:bold; font-style:normal;\">"
	#define HTML_END_F1_LARGE				"</span></p></body></html>"
	#define HTML_START_F1_BLUE				"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; color: #c6e9fe; font-style:normal;\">"
	#define HTML_START_F1_BLACK				"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; color: black; font-style:normal;\">"

	#define HTML_1_START_BUTTON				"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:12pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:bold; font-style:normal;\">%1</span></p></body></html>"
	#define HTML_2_SERVER_ONLINE			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:16pt; font-weight:bold; font-style:italic; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:bold; font-style:italic;\">%1<br>%2</span></p></body></html>"
	#define HTML_3_CONN_STRING				"<html><p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Arial'; font-size:14pt;\">Bridge ID: </span><a href=\"http://1\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></p></html>"
	#define HTML_3_URL_STRING				"<html><div><a href=\"%1\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%2</span></a></div></html>"
	#define HTML_3_URL_STRING_WHITE			"<html><div><a href=\"%1\" ><span style=\"font-family:Arial; color: white; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%2</span></a></div></html>"
	#define HTML_4_HELP_1					"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:11pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"http://link_1\"><span style=\" font-size:11pt; font-weight:600; text-decoration: underline; color:yellow;\">%1</span></a></p></body></html>"
	#define HTML_5_STATUS_STRING			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:bold; font-style:italic;\">%1</span></p></body></html>"
	#define HTML_5_STATUS_URL				"<a href=\"http://link_1\"><span style=\" font-size:8pt; font-weight:600; text-decoration: underline; color:#0000ff;\">How to solve?</span></a>"
	#define HTML_LABEL_INFO					"<html><div><a href=\"http://1\" ><span style=\"font-family:Arial; color: white; font-size:13pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>"
	#define HTML_PORT_FORWARD				"<html><p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Arial'; font-size:14pt;\"></span><a href=\"http://1\" ><span style=\"font-family:Arial; color: green; font-size:14pt; font-weight:bold; font-style:italic; text-decoration:none;\">%1</span></a></p></html>"
	#define HTML_URL_STRING_SMALL			"<html><div><a href=\"%1\" ><span style=\"font-family:Arial; color: #c6e9fe; font-size:12pt; font-style:normal; text-decoration:none;\">%2</span></a></div></html>"
	#define HTML_PORT_FORWARD_SMALL			"<html><p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Arial'; font-size:14pt;\"></span><a href=\"http://1\" ><span style=\"font-family:Arial; color: white; font-size:12pt; font-weight:bold; font-style:italic; text-decoration:none;\">%1</span></a></p></html>"
#endif


#define SERVER_CALL_INTERVAL 30 /* 150 sec */
#define SERVER_PROGRESS_BAR_IDLE_INTERVAL 60 /* 5 min */
#define TIMER_CHECK_SERVER 5*1000 //every 5 sec check 

#define APPSERVER_EXE_NAME "appserver_john.exe"

appserver_john_gui::appserver_john_gui(QWidget *parent, Qt::WindowFlags flags)
	: QWidget(parent, flags)
{


	m_nTimerID=-1;
	m_nUpdateInterval=0;
	m_bWasStarted=false;
	ui.setupUi(this);



	nSkipClearStatus=0;
	ResetProgressBar();
	setWindowTitle("everDOCs Bridge "+QString(APPLICATION_VERSION));


	ui.btnServerPicChange->setIcon(QIcon(":Resources/B_Insert16.png"));
	ui.btnServerPicChange->setToolTip("Change Server Picture");
	ui.btnServerPicClear->setIcon(QIcon(":Resources/B_Delete16.png"));
	ui.btnServerPicClear->setToolTip("Clear Server Picture");

	ui.btnDirectoryPicChange->setIcon(QIcon(":Resources/B_Insert16.png"));
	ui.btnDirectoryPicChange->setToolTip("Change Shared Folder Picture");
	ui.btnDirectoryPicClear->setIcon(QIcon(":Resources/B_Delete16.png"));
	ui.btnDirectoryPicClear->setToolTip("Clear Shared Folder Picture");

	ui.ckbForcePubliceIP->setToolTip(tr("This option is only used for complex LAN environment with multiple segments. Port-Forwarding must be configured, UPnP is not available."));
	connect(ui.tableDirs,SIGNAL(SignalSelectionChanged()),this,SLOT(OnDirectorySelectionChanged()));

	setAttribute(Qt::WA_QuitOnClose);

	m_pServiceConfig= new AppServerConfigurator();
	m_pServiceConfig->m_strAppExeName=SERVER_JOHN_SERVICE_NAME;
	m_pServiceConfig->m_strServiceName=SERVER_JOHN_SERVICE_NAME;

	//----------------------------------------------
	//Start logger 
	//----------------------------------------------
	//g_Logger.EnableFileLogging(m_strHomePath+"/settings/client.log",0,200);
	g_Logger.EnableFileLogging(QDir::homePath()+"/client.log",0,200);
	g_Logger.SetLogLevel(3); //only for testing purposes
	ApplicationLogger::setApplicationLogger(&g_Logger);


	InitializeSettingsDirectory(m_strHomePath);
	m_SettingsManager.Initialize(m_strHomePath);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"m_strHomePath="+m_strHomePath);






	//ui.txtPort->setInputMask("00000");
	//ui.groupBoxMsg->setVisible(false);

	//QString strLineEdit="QLineEdit:disabled { color: black; background:rgb(229,229,229);}"; 
	//ui.txtRootName->setStyleSheet(strLineEdit);
	//ui.txtRootName->setText(USERNAME_ROOT);

	m_pActionCopy_ServerID = new QAction(tr("Copy Server ID"), this);
	connect(m_pActionCopy_ServerID, SIGNAL(triggered()), this, SLOT(OnCopyServerID()));
	m_pActionCopy_IP = new QAction(tr("Copy Public IP Address"), this);
	connect(m_pActionCopy_IP, SIGNAL(triggered()), this, SLOT(OnCopyIP()));
	m_pActionCopy_IPLocal = new QAction(tr("Copy Local IP Address"), this);
	connect(m_pActionCopy_IPLocal, SIGNAL(triggered()), this, SLOT(OnCopyIPLocal()));
	m_pActionCopy_ID_StringAll = new QAction(tr("Copy Connection String"), this);
	connect(m_pActionCopy_ID_StringAll, SIGNAL(triggered()), this, SLOT(OnCopyIDString()));


	ui.labelServerID->setContextMenuPolicy(Qt::CustomContextMenu);
	ui.labelIP->setContextMenuPolicy(Qt::CustomContextMenu);
	ui.labelIPpriv->setContextMenuPolicy(Qt::CustomContextMenu);
	ui.labelServerConnect->setContextMenuPolicy(Qt::CustomContextMenu);

	connect(ui.labelServerID,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnlabelServerID_customContextMenuRequested ( const QPoint & )));
	connect(ui.labelIP,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnlabelIP_customContextMenuRequested ( const QPoint & )));
	connect(ui.labelIPpriv,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnlabelIPpriv_customContextMenuRequested ( const QPoint & )));
	connect(ui.labelServerConnect,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnlabelIDString_customContextMenuRequested ( const QPoint & )));


	m_strIniFilePath= m_strHomePath+"/settings/app_server.ini";
	bool bOK=m_INIFile.Load(m_strIniFilePath);
	if(!bOK)
	{
		QMessageBox::information(this,"Error","INI file is missing,corrupted or has invalid values!");
	}

	//QMessageBox::information(this,"PORT",QString::number(m_INIFile.m_nAppServerPort));

	m_strAppServerID=m_INIFile.m_strAppServerID;
	ui.labelServerID->setText (HTML_START_F1+m_strAppServerID+HTML_END_F1);
	ui.labelServerID_2->setText (HTML_START_F1_BLUE+QString("ID: "+m_strAppServerID)+HTML_END_F1);
	ui.labelServerID_3->setText (HTML_START_F1_BLACK+m_strAppServerID+HTML_END_F1);


	InitiDirs();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Constructor 1");

	//reset last err in ini:
	Status err;
	if (!m_pServiceConfig->IsServiceRunning())
	{
		m_INIFile.m_nLastErrorCode=0;
		m_INIFile.m_strLastErrorText="";
		m_INIFile.Save(m_strIniFilePath);
	}

	//QMessageBox::information(this,"PORT",QString::number(m_INIFile.m_nAppServerPort));

	
	m_SettingsManager.DefineFolders(m_lstFolders);
	ui.tableDirs->setStyleSheet(PicHelper::GetTableStyle());
	ui.tableDirs->Initialize(&m_lstFolders);
	ui.tableDirs->SetEditMode(true);

	m_SettingsManager.DefineUserRights(m_lstUserRights);
	m_SettingsManager.DefineUserRights(m_lstUserRightsForFolder);

	DbRecordSet columns_users;
	UniversalTableWidgetEx::AddColumnToSetup(columns_users,"ALIAS",tr("Shared Folder"),120,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns_users,"CAN_DOWNLOAD",tr("Download"),100,true, "",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::AddColumnToSetup(columns_users,"CAN_UPLOAD",tr("Upload"),100,true, "",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::AddColumnToSetup(columns_users,"CAN_DELETE",tr("Delete"),100,true, "",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::AddColumnToSetup(columns_users,"CAN_SEND_INVITE",tr("Send Invitations"),100,true, "",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);

	ui.tableWidgetRights->setStyleSheet(PicHelper::GetTableStyle());
	ui.tableWidgetRights->Initialize(&m_lstUserRightsForFolder,&columns_users);
	ui.tableWidgetRights->SetEditMode(true);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Constructor 2");
	BuildStyleWidgets();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Constructor 3");
	
	connect(ui.tableDirs,SIGNAL(ReloadCatalogs()),this,SLOT(OnReloadCatalogs()));
	connect(ui.tableDirs,SIGNAL(RenameCatalog(int )),this,SLOT(OnRenameCatalog(int)));
	connect(ui.tableDirs,SIGNAL(RemoveSelectedCatalogs()),this,SLOT(on_btnRemoveFolder_clicked()));
	
	//set data source for table, enable drop
	DbRecordSet columns;
	UniversalTableWidgetEx::AddColumnToSetup(columns,"USERNAME",tr("User Name"),140,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"LAST_NAME",tr("Last Name"),140,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"FIRST_NAME",tr("First Name"),140,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"COMPANY",tr("Company"),160,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"EMAIL",tr("Email"),160,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	ui.tableWidget->setStyleSheet(PicHelper::GetTableStyle());

	m_SettingsManager.DefineUsers(m_lstUsers);
	ui.tableWidget->Initialize(&m_lstUsers,&columns);


	LoadSettings();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Constructor 4");

	connect(ui.tableWidget,SIGNAL(SignalSelectionChanged()),this,SLOT(OnUserSelectionChanged()));


	m_strAppServerIPpriv=TcpHelper::ResolveLocalHostURLService("0.0.0.0",m_INIFile.m_nAppServerPort);
	RefreshServerStatus(false,true);

	PicHelper::SetPlatformDepedentFontSize(); //adjust fonts on MAC
	OnMenu_CatalogsClick();

	LoadINISettings();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Constructor 5");

	//ui.tableDirs->SetDirs(m_INIFile.m_strCatalogDir,m_INIFile.m_strThumbDir);
	ui.tableDirs->SetDirs(m_INIFile.m_strCatalogDir,m_strHomePath+"/user_storage/thumb_cache");
	ui.txtDataDir->setReadOnly(true);
	ui.txtCatalogDir->setReadOnly(true);


	if (m_pServiceConfig->IsServiceRunning())
	{
		m_nTimerID=startTimer(TIMER_CHECK_SERVER);	//only refresh when service is running.
	}


#if 0
		//test iPhoto integration
		iPhotoHandler iPhotoObj;
		if(iPhotoObj.Login()){
			QList<iPhotoAlbum> lstAlbums;
			if(iPhotoObj.GetAlbums(lstAlbums)){
				qDebug() << lstAlbums.length() << "albums found!";
			}
			else
				qDebug() << "Error fetching albums!";
			int nAlbumID = 3;//"miro" album
			QList<iPhotoEntry> lstPics;
			if(iPhotoObj.GetAlbumPictures(nAlbumID, lstPics)){
				int nLen = lstPics.length();
				qDebug() << nLen << "pics in 'miro' album:";
				for(int i=0; i<nLen; i++){
					qDebug() << i << ": " << lstPics[i].m_strPath;	
					QFileInfo info(lstPics[i].m_strPath);
					qDebug()<<info.size();
				}
			}
			else
				qDebug() << "Error fetching album pictures!";
			iPhotoObj.Logout();		
		}
		else
			qDebug() << "Error logging to iPhoto database!";
#endif
	
	//ui.ckbSSL->setVisible(false);

	this->resize(width(),740);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Constructor 6");
	//QTimer::singleShot(0,this,SLOT(UpdateWebPages()));
	
}

appserver_john_gui::~appserver_john_gui()
{
#ifdef __APPLE__
	if (m_pServiceConfig->IsServiceRunning())
	{
		if(QMessageBox::question(this,"Confirmation",QString("everDOCs Bridge will continue to work in the background and stream documents when you request them from your iDevice or your browser. After a re-boot, everDOCs Bridge will continue to do so. Do you agree?"), tr("Yes"),tr("No"))==1)
		{
			//stop server then execute this:
			on_btnStart_clicked();
		}
	}
#endif


	//check if needed to reload:
	//read file reload and if exists go reload
	QFileInfo info_ini(m_strIniFilePath);
	QString strFileName=info_ini.absolutePath()+"/"+QString::number(m_INIFile.m_nAppServerPort)+"_reload";
	if (QFile::exists(strFileName))
	{
		QFile::remove(strFileName);
		LoadSettings();
	}

	SaveSettings(true);

	if (ui.txtPort->isEnabled())
	{
		SaveINISettings(true);
	}

	delete m_pServiceConfig;


}

void appserver_john_gui::on_btnStart_clicked()
{
	if (m_nTimerID>=0)
		killTimer(m_nTimerID);

	QString strDir=m_pServiceConfig->GetDir();
	//QMessageBox::information(NULL,"Warning","Dir Path: "+strDir);

	Status err;
	if (m_pServiceConfig->IsServiceRunning())
	{
		//MB: if some catalog is indexing then do not allow to stop:

		if (m_lstFolders.find("STATUS",STATUS_INDEXING,true)>=0)
		{
			QMessageBox::warning(this,tr("Warning"),tr("The everDOCS Bridge server process can not be stopped while the catalog indexing is in progress!"));
			m_nTimerID=startTimer(TIMER_CHECK_SERVER);
			return;
		}

		m_pServiceConfig->Stop(err);
		if (!err.IsOK())
		{
			QMessageBox::critical(this,tr("Error"),err.getErrorText());
			return;
		}
		m_bWasStarted=false;
		RefreshServerStatus();
	}
	else
	{
		//set email if not:
		if (!m_INIFile.m_nAskedForEmail)
		{
			if(!TestEmailSettings())
				return;
			m_INIFile.m_nAskedForEmail=1;
		}

		if (!m_INIFile.m_nAskedForUser)
		{
			if (!TestAdmin())
				return;
			m_INIFile.m_nAskedForUser=1;
		}

		if (!m_INIFile.m_nAskedForNewDir)
		{
			if (m_lstFolders.getRowCount()==0)
			{
				AddMyPictureDir(); //if folders empty ask user to import mypic
			}
			m_INIFile.m_nAskedForNewDir=1;
		}

		//reset this flag: when setted then exit power up state
		m_INIFile.m_nServerTestedConnection=0;

		//if failed to save:
		if(!SaveSettings(true))
			return;

		//save INI start server:
		QString strName=m_INIFile.m_strServiceDescriptor;
		if (strName.indexOf(" ")!=-1 || strName.toLatin1().simplified()!=strName)
		{
			QMessageBox::critical(this,tr("Error"),tr("Service Descriptor can not contain spaces nor special characters (national)!"));
			return;
		}

		if(!SaveINISettings(false))
		{
			QMessageBox::critical(this,tr("Error"),tr("Failed to save INI settings, operation aborted!"));
			return;
		}


while(1)
{
		m_pServiceConfig->SetServiceUserAndPass("","");
		bool bServiceJustInstalled=false;

#if _WIN32
		QString strServiceUserName;
		QString strServicePass;

		if(!m_pServiceConfig->IsServiceInstalled())
		{
			DlgAskForServiceLogon DlgLogon;
			if(DlgLogon.exec())
			{
				if (DlgLogon.GetPromptUserForPass())
				{
					DlgUserAndPass DlgUser;
					if(DlgUser.exec())
					{
						DlgUser.GetData(strServiceUserName,strServicePass);
						m_pServiceConfig->SetServiceUserAndPass(strServiceUserName,strServicePass);
						bServiceJustInstalled=true;
					}
				}
			}
			else
			{
				//MB: if first dialog is cancel then abort
				return;
			}
		}
#endif

//B.T.: before start kill all ghost services on MAC
#ifdef __APPLE__

		Status tmpErr;
		m_pServiceConfig->Stop(tmpErr);
#endif
		m_pServiceConfig->StartOrInstall(err,true);

		if (!err.IsOK())
		{
			QMessageBox::critical(this,tr("Error"),err.getErrorText());
			if(bServiceJustInstalled)
			{
				if(QMessageBox::question(this,"Confirmation",QString("There was problem with service registration and startup. Would you like to try again?"), tr("Yes"),tr("No"))==0)
				{
					Status err;
					m_pServiceConfig->UnInstallService(err);
					continue;
				}
			}
			return;
		}
		
		//m_pServiceConfig->CreateUnInstallBat();
		m_pServiceConfig->Start(err);
		if (!err.IsOK())
		{
			QMessageBox::critical(this,tr("Error"),err.getErrorText());
			if(bServiceJustInstalled)
			{
				if(QMessageBox::question(this,"Confirmation",QString("There was problem with service registration and startup. Would you like to try again?"), tr("Yes"),tr("No"))==0)
				{
					Status err;
					m_pServiceConfig->UnInstallService(err);
					continue;
				}
			}
			return;
		}

		break;
}
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

		ResetProgressBar();
		m_bWasStarted=true;
		//sleep for two seconds before server refresh
		
		ThreadSleeper::Sleep(5000); 
		RefreshServerStatus(false,m_INIFile.m_nServerTestedConnection==0);
		QApplication::restoreOverrideCursor();


		m_nTimerID=startTimer(TIMER_CHECK_SERVER);	
		m_strAppServerIPpriv=TcpHelper::ResolveLocalHostURLService("0.0.0.0",m_INIFile.m_nAppServerPort);
		//m_pBtnStart->setEnabled(false);

	}

}


void appserver_john_gui::RefreshServerStatus(bool bSkipINIRefresh, bool bPowerUp)
{
	m_pBtnStart->setEnabled(true);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("m_INIFile.Load start"));

	m_INIFile.Load(m_strIniFilePath);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 1"));
	
	m_strAppServerID=m_INIFile.m_strAppServerID;
	ui.labelServerID->setText(HTML_START_F1+m_strAppServerID+HTML_END_F1);
	ui.labelServerID_2->setText(HTML_START_F1_BLUE+QString("ID: "+m_strAppServerID)+HTML_END_F1_LARGE);
	ui.labelServerID_3->setText (HTML_START_F1_BLACK+m_strAppServerID+HTML_END_F1);

	m_strOuterIP=m_INIFile.m_strOuterIP;
	ui.labelIP->setText(HTML_START_F1+m_strOuterIP+HTML_END_F1);
	ui.labelIPpriv->setText(HTML_START_F1+m_strAppServerIPpriv+HTML_END_F1);
	ui.labelIP_2->setText(HTML_START_F1_LARGE+m_strOuterIP+HTML_END_F1_LARGE);
	ui.labelIPpriv_2->setText(HTML_START_F1_LARGE+m_strAppServerIPpriv+HTML_END_F1_LARGE);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 2"));
	
	//QFileInfo info(qApp->arguments().at(0));
	m_pServiceConfig->m_strServiceDescriptor=m_INIFile.m_strServiceDescriptor;


	Status err;
	if (m_pServiceConfig->IsServiceRunning())
	{
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 3"));

		ui.txtPort->setEnabled(false);
		ui.txtPortEnd->setEnabled(false);
		ui.ckbNAT->setEnabled(false);
		ui.ckbUpnp->setEnabled(false);
		ui.ckbSSL->setEnabled(false);
		ui.ckbForcePubliceIP->setEnabled(false);

		ui.txtJPEG->setEnabled(false);
		ui.txtDataDir->setEnabled(false);
		ui.btnDataDir->setEnabled(false);
		ui.txtCatalogDir->setEnabled(false);
		ui.btnCatalogDir->setEnabled(false);
		ui.btnResetService->setEnabled(false);

		ui.txtRootName->setEnabled(false);
		ui.txtRootPass->setEnabled(false);

		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 3"));

		SetForwardMethod(m_INIFile.m_nPortForwardMethod);

		if (bPowerUp)
		{
			SetButtonStatus(STATUS_POWER_UP);
			ui.labelStatus->clear();
			return;
		}

		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 4"));

		if (m_INIFile.m_nLastErrorCode==StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_REGISTER)
		{
			if (m_INIFile.m_nAppServerPortActual!=0)
				m_strConnectionString=m_strAppServerID+"/"+m_strAppServerIPpriv+":"+QString::number(m_INIFile.m_nAppServerPortActual);
			else
				m_strConnectionString=m_strAppServerID+"/"+m_strAppServerIPpriv+":"+QString::number(m_INIFile.m_nAppServerPort);

			if (!m_strAppServerID.isEmpty())
			{
				ui.labelServerConnect->setText(QString(HTML_3_CONN_STRING).arg(m_strConnectionString));
				//ui.labelServerConnect->setText(QString("<html><div><a href=\"http://1\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>").arg(m_strConnectionString));

				QString strAccess = "http://everxconnect.com:11000/invite/"+m_strAppServerID;
				ui.labelURL_Access->setText(QString(HTML_3_URL_STRING).arg(strAccess).arg(strAccess));
				////ui.labelURL_Access_2->setText(QString(HTML_3_URL_STRING_WHITE).arg(strAccess).arg(strAccess));
				
				//ui.labelConnectString->setText(QString(HTML_URL_STRING_SMALL).arg(strAccess).arg(strAccess));

				//ui.labelURL_Access->setText(QString("<html><div><a href=\""+strAccess+"\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>").arg(strAccess));

			}
			SetButtonStatus(STATUS_ONLINE_WLAN);
			//ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(tr("Warning: server only accessible through WLAN")));
			ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(tr("Warning: internet connection is probably down. Only LAN/WLAN access is possible. ")));
		}
		else if (m_INIFile.m_nLastErrorCode==StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_TEST_CONNECTIVITY)
		{
			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 5a"));

			if (m_INIFile.m_nAppServerPortActual!=0)
				m_strConnectionString=m_strAppServerID+"/"+m_strAppServerIPpriv+":"+QString::number(m_INIFile.m_nAppServerPortActual);
			else
				m_strConnectionString=m_strAppServerID+"/"+m_strAppServerIPpriv+":"+QString::number(m_INIFile.m_nAppServerPort);

			if (!m_strAppServerID.isEmpty())
			{
				ui.labelServerConnect->setText(QString(HTML_3_CONN_STRING).arg(m_strConnectionString));
				//ui.labelServerConnect->setText(QString("<html><div><a href=\"http://1\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>").arg(m_strConnectionString));

				QString strAccess = "http://everxconnect.com:11000/invite/"+m_strAppServerID;
				ui.labelURL_Access->setText(QString(HTML_3_URL_STRING).arg(strAccess).arg(strAccess));
				//ui.labelURL_Access_2->setText(QString(HTML_3_URL_STRING_WHITE).arg(strAccess).arg(strAccess));
				//ui.labelConnectString->setText(QString(HTML_URL_STRING_SMALL).arg(strAccess).arg(strAccess));
				//ui.labelURL_Access->setText(QString("<html><div><a href=\""+strAccess+"\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>").arg(strAccess));

			}

			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 5b"));

			SetButtonStatus(STATUS_ONLINE_WLAN);
			//ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(tr("Warning: server only accessible through WLAN")));
			ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(tr("Warning: Your IP or port have changed and are no more visible from the internet. ")+QString(HTML_5_STATUS_URL)));

			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 5c"));
		}
		else if (m_INIFile.m_nLastErrorCode==0)
		{
			m_strConnectionString=m_strAppServerID;
			if (!m_strAppServerID.isEmpty())
			{
				ui.labelServerConnect->setText(QString(HTML_3_CONN_STRING).arg(m_strConnectionString));
				//ui.labelServerConnect->setText(QString("<html><div><a href=\"http://1\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>").arg(m_strConnectionString));

				QString strAccess = "http://everxconnect.com:11000/invite/"+m_strAppServerID;
				ui.labelURL_Access->setText(QString(HTML_3_URL_STRING).arg(strAccess).arg(strAccess));
				//ui.labelURL_Access_2->setText(QString(HTML_3_URL_STRING_WHITE).arg(strAccess).arg(strAccess));
				//ui.labelConnectString->setText(QString(HTML_URL_STRING_SMALL).arg(strAccess).arg(strAccess));
				//ui.labelURL_Access->setText(QString("<html><div><a href=\""+strAccess+"\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>").arg(strAccess));
			}
			SetButtonStatus(STATUS_ONLINE_FULL);
			if (!ui.progressBar->isVisible()) //only clear  if progres bar is not active
			{
				nSkipClearStatus--;
				if (nSkipClearStatus<=0)
				{
					ui.labelStatus->clear();
					nSkipClearStatus=0;
				}
			}
		}
		else if (m_INIFile.m_nLastErrorCode)
		{
			ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(tr("Error:")+m_INIFile.m_strLastErrorText));
		}
	
	}
	else
	{


		SetForwardMethod(-1);
		nSkipClearStatus--;
		if (nSkipClearStatus<=0)
		{
			ui.labelStatus->clear();
			nSkipClearStatus=0;
		}
		ui.progressBar->setVisible(false);

		ui.txtPort->setEnabled(true);
		ui.txtPortEnd->setEnabled(true);
		ui.ckbNAT->setEnabled(true);
		ui.ckbSSL->setEnabled(true);
		ui.ckbForcePubliceIP->setEnabled(true);
		ui.ckbUpnp->setEnabled(true);
		ui.txtJPEG->setEnabled(true);
		ui.txtDataDir->setEnabled(true);
		ui.btnDataDir->setEnabled(true);
		ui.txtCatalogDir->setEnabled(true);
		ui.btnCatalogDir->setEnabled(true);
		ui.btnResetService->setEnabled(true);
		ui.txtRootName->setEnabled(true);
		ui.txtRootPass->setEnabled(true);


		SetButtonStatus(STATUS_OFFLINE);

		if (m_bWasStarted)
		{
			if (m_INIFile.m_nLastErrorCode)
			{
				QMessageBox::critical(this,"Application server reported error",m_INIFile.m_strLastErrorText);
			}
			m_bWasStarted=false;
		}
	}

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshServerStatus 6"));

#ifdef NO_ADMIN_BUILD
	m_pBtnStart->setEnabled(false);
	ui.btnDataDir->setEnabled(false);
#endif
}


void appserver_john_gui::LoadSettings()
{
	m_SettingsManager.LoadSettings();
	m_SettingsManager.GetFolders(m_lstFolders);
	m_SettingsManager.GetUsers(m_lstUsers);
	//m_lstUsers.Dump();
	m_SettingsManager.GetUserRights(m_lstUserRights);

	DbRecordSet lstGeneralSettings;
	m_SettingsManager.GetGeneralSettings(lstGeneralSettings);

	//lstGeneralSettings.Dump();

	ui.txtRootName->setText(m_SettingsManager.GetAdminUserName());



	bool bSaveSettings = ActualizeSettings();

	ui.txtServerName->setText(m_SettingsManager.GetServerName());


	DbRecordSet recData;
	m_SettingsManager.DefineEmailSettings(recData);
	m_SettingsManager.GetEmailSettings(recData);

	if (recData.getRowCount()==0)
		recData.addRow();

	//mail:
	ui.txtServer->setText(recData.getDataRef(0,"SMTP_SERVER").toString());
	ui.txtMailPort->setText(recData.getDataRef(0,"SMTP_PORT").toString());
	if (recData.getDataRef(0,"SMTP_PORT").toString().isEmpty() || recData.getDataRef(0,"SMTP_PORT").toString()=="0")
		ui.txtMailPort->setText("25");
	ui.txtEmail->setText(recData.getDataRef(0,"EMAIL_ADDRESS").toString());
	ui.txtUserEmail->setText(recData.getDataRef(0,"USERNAME").toString());
	ui.txtPassEmail->setText(recData.getDataRef(0,"PASSWORD").toString());
	QString strData=QString::fromUtf8(recData.getDataRef(0,"SIGNATURE").toByteArray());
	ui.txtSignature->setPlainText(strData);

	ui.ckbShowAllDrives->blockSignals(true);
	ui.ckbShowOnlyDocuments->blockSignals(true);
	if (lstGeneralSettings.getRowCount()>0)
	{
		ui.ckbShowAllDrives->setChecked(lstGeneralSettings.getDataRef(0,"SHOW_ALL_DRIVES").toBool());
		ui.ckbShowOnlyDocuments->setChecked(lstGeneralSettings.getDataRef(0,"SHOW_ONLY_DOCUMENTS").toBool());
	}
	else
	{
		ui.ckbShowAllDrives->setChecked(0);
		ui.ckbShowOnlyDocuments->setChecked(1);
	}
	ui.ckbShowAllDrives->blockSignals(false);
	ui.ckbShowOnlyDocuments->blockSignals(false);


	ui.tableDirs->RefreshDisplay();
	QString strPass;
	m_SettingsManager.GetRootPass(strPass);
	ui.txtRootPass->setText(strPass);

	m_lstUsers.sort("USERNAME");
	ui.tableWidget->RefreshDisplay();

	RefreshServerIcon();
	OnDirectorySelectionChanged();

	m_lstFolders_Old=m_lstFolders;

	if (bSaveSettings)
		SaveSettings(true,true);
}

void appserver_john_gui::LoadINISettings()
{

	ui.txtPort->blockSignals(true);
	ui.txtPort->setText(QString::number(m_INIFile.m_nAppServerPort));
	ui.txtPort->blockSignals(false);

	ui.txtPortEnd->blockSignals(true);
	ui.txtPortEnd->setText(QString::number(m_INIFile.m_nAppServerPortRangeEnd));
	ui.txtPortEnd->blockSignals(true);

	ui.ckbUpnp->setChecked(m_INIFile.m_nEnableUpnp);
	ui.ckbNAT->setChecked(m_INIFile.m_nEnableNAT);
	ui.ckbSSL->blockSignals(true);
	ui.ckbSSL->setChecked(m_INIFile.m_nSSLMode);
	ui.ckbSSL->blockSignals(false);
	ui.ckbForcePubliceIP->setChecked(m_INIFile.m_nForcePublicIP);

	if (m_INIFile.m_nJPEGQuality<30) m_INIFile.m_nJPEGQuality = 30;
	if (m_INIFile.m_nJPEGQuality>100) m_INIFile.m_nJPEGQuality = 100;
	ui.txtJPEG->setText(QString::number(m_INIFile.m_nJPEGQuality));
	//ui.txtDataDir->setText(m_INIFile.m_strThumbDir);


	ui.txtCatalogDir->setText(m_INIFile.m_strCatalogDir);


}
bool appserver_john_gui::SaveINISettings(bool bUseSilentValueCheck)
{

	if (bUseSilentValueCheck)
	{
		int nPort=ui.txtPort->text().trimmed().toInt();
		if (nPort<=0 || nPort>65535)
		{
			QMessageBox::critical(this,tr("Error"),tr("Port is out range (1-65535)!"));
			return false;
		}
	}

	int nPort=ui.txtPort->text().trimmed().toInt();
	if (nPort>0 && nPort<65535)
		m_INIFile.m_nAppServerPort=nPort;
	else
		m_INIFile.m_nAppServerPort=9999;

	int nPortEnd=ui.txtPortEnd->text().trimmed().toInt();
	if (nPortEnd>0 && nPortEnd<65535)
	{
		if (nPortEnd<nPort)
			m_INIFile.m_nAppServerPortRangeEnd=nPort;
		else
			m_INIFile.m_nAppServerPortRangeEnd=nPortEnd;
	}
	else
		m_INIFile.m_nAppServerPortRangeEnd=nPort;

	//save settings into INI file:
	m_INIFile.m_nEnableNAT=(int)ui.ckbNAT->isChecked();
	m_INIFile.m_nEnableUpnp=(int)ui.ckbUpnp->isChecked();
	m_INIFile.m_nSSLMode=(int)ui.ckbSSL->isChecked();
	m_INIFile.m_nForcePublicIP=(int)ui.ckbForcePubliceIP->isChecked();

	int nJpeg=ui.txtJPEG->text().toInt();
	if(nJpeg<30) nJpeg=30;
	if(nJpeg>100) nJpeg=100;
	m_INIFile.m_nJPEGQuality=nJpeg;
	m_INIFile.m_strCatalogDir=ui.txtCatalogDir->text();
/*
	if (ui.rbUndelete->isChecked())
		m_INIFile.m_nEnablePermanentDelete = 0;
	else
		m_INIFile.m_nEnablePermanentDelete = 1;
*/
	return	m_INIFile.Save(m_strIniFilePath);


}


void appserver_john_gui::on_ckbShowAllDrives_stateChanged(int nState)
{
	SaveSettings(true,true);
}

void appserver_john_gui::on_ckbShowOnlyDocuments_stateChanged(int nState)
{
	SaveSettings(true,true);
}

void appserver_john_gui::on_ckbSSL_stateChanged(int nState)
{
	if (nState==Qt::Checked)
	{
		QMessageBox::information(this,"Warning","WARNING: SSL significantly increases your security, but your free everDOCs app has to be upgraded to use SSL! The everDOCs app will ask you for this (very cheap) upgrade if you leave SSL on know.");
	}
}

bool appserver_john_gui::SaveSettings(bool bSkipMsg,bool bSkipCatalogReload, bool bSkipServerReload)
{
	DbRecordSet tmp=m_lstFolders;
	
	//check before save if duplicate catalogs:
	int nSize=m_lstFolders.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strCatalog=m_lstFolders.getDataRef(i,"ALIAS").toString();

		if (!FileCruncher::IsNameValidAsDirectory(strCatalog))
		{
			QMessageBox::warning(this,"Warning",QString("Operation aborted: Shared Folder name %1 is not valid. Shared Folder name can't contain characters: *.\"/\\[]:;|=,").arg(strCatalog));
			m_lstFolders=tmp;
			return false;
		}

		if(m_lstFolders.find("ALIAS",strCatalog)>1)
		{
			QMessageBox::warning(this,"Warning",QString("Operation aborted: duplicate catalog name found %1").arg(strCatalog));
			m_lstFolders=tmp;
			return false;
		}

		if (strCatalog.toLower()=="user_catalog" || strCatalog.toLower()=="user_temp" || strCatalog.toLower()=="thumb_cache")
		{
			QMessageBox::warning(this,"Warning",QString("Operation aborted: Shared Folder name %1 is not valid. Shared Folder name must be different from: 'user_catalog', 'user_temp' and 'thumb_cache'").arg(strCatalog));
			m_lstFolders=tmp;
			return false;
		}
	}


	//general settings
	DbRecordSet lstGeneralSettings;
	m_SettingsManager.GetGeneralSettings(lstGeneralSettings);
	if (lstGeneralSettings.getRowCount()==0)
		lstGeneralSettings.addRow();

	lstGeneralSettings.setData(0,"SHOW_ALL_DRIVES",(int)ui.ckbShowAllDrives->isChecked());
	lstGeneralSettings.setData(0,"SHOW_ONLY_DOCUMENTS",(int)ui.ckbShowOnlyDocuments->isChecked());
	
	m_SettingsManager.SetGeneralSettings(lstGeneralSettings);


	//to save selection:
	m_lstFolders=tmp;




	//ui.tableDirs->RefreshDisplay();
	m_SettingsManager.SetFolders(m_lstFolders);
	m_SettingsManager.SetUsers(m_lstUsers);
	QString strPass=ui.txtRootPass->text();
	m_SettingsManager.SetAdminUserName(ui.txtRootName->text());
	m_SettingsManager.SetRootPass(strPass);
	m_SettingsManager.SetServerName(ui.txtServerName->text());
	m_SettingsManager.SetUserRights(m_lstUserRights);
	


	SetEmailSettings();
	m_SettingsManager.SaveSettings();

	m_lstFolders_Old=m_lstFolders;

	//if server is running: tell him to reload:
	if (!bSkipServerReload)
	{
		Status err;
		if (m_pServiceConfig->IsServiceRunning())
		{
			SendReloadCommandToServer(err, bSkipCatalogReload);
			if (!err.IsOK())
			{
				if (!bSkipMsg)
					QMessageBox::critical(this,tr("Error"),tr("Error while trying to notify server about changes! Try again. If problem still exists, please restart server!"));
				return false;
			}
		}
	}


	if (!bSkipMsg)
		QMessageBox::information(this,tr("Success"),tr("Settings successfully saved!"));

	return true;
}
void appserver_john_gui::on_btnAddFolder_clicked()
{
	ui.tableDirs->DialogAddNewDirectory();
}

/*
void appserver_john_gui::on_btnAddiPhoto_clicked()
{
	//m_lstFolders.Dump();

	ui.tableDirs->DialogAddNewDirectory("",true);
}
*/



void appserver_john_gui::on_btnResetService_clicked()
{
	Status err;
	m_pServiceConfig->UnInstallService(err);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
	}
	else
	{
		QMessageBox::information(this,tr("Information"),tr("Service is successfully unregistered from the system."));
	}

}


void appserver_john_gui::on_btnRemoveFolder_clicked()
{
	int nRowCount=m_lstFolders.getSelectedCount();
	if (nRowCount>0)
	{
		if(QMessageBox::question(this,"Confirmation",QString("Do you really want to unpublish selected directories?"), tr("Yes"),tr("No"))==0)
		{

			DbRecordSet lstSelected=m_lstFolders.getSelectedRecordSet();

			int nSize=lstSelected.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				QString strAlias=lstSelected.getDataRef(i,"ALIAS").toString();
				m_lstUserRights.find("ALIAS",strAlias);
				m_lstUserRights.deleteSelectedRows();
			}

			m_lstFolders.deleteSelectedRows();
			ui.tableDirs->RefreshDisplay();
			OnDirectorySelectionChanged();

			ui.tableWidget->RefreshDisplay();
			OnUserSelectionChanged();

			SaveSettings(true);

			ResetProgressBar();
		}
	}
}


void appserver_john_gui::timerEvent(QTimerEvent *event)
{
	//out timer is here, then fire Time out error, save our asses!!!
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("timerEvent is about to be executed"));
		RefreshServerStatus(true,m_INIFile.m_nServerTestedConnection==0);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("timerEvent/RefreshServerStatus is  executed"));

		RefreshProgressBar();

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("timerEvent/RefreshProgressBar is  executed"));

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("timerEvent executed"));
		m_nTimerID=startTimer(TIMER_CHECK_SERVER);
	}
}

void appserver_john_gui::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Escape)
	{
		close();
		return;
	}
	QWidget::keyPressEvent(event);
}



void appserver_john_gui::SendReloadCommandToServer(Status &err,bool bSkipCatalogReload)
{
	//engange REST client: call GET/service/settings/reload
	//CONNECT:
	RestHTTPClient client;
	client.SetConnectionSettings("127.0.0.1",m_INIFile.m_nAppServerPortActual,m_INIFile.m_nSSLMode,3);
	client.Connect(err);
	if (!err.IsOK())
	{
		/*
		QMessageBox::information(this,"","Error connecting localhost: "+err.getErrorText());
		RestHTTPClient client2;
		client2.SetConnectionSettings("127.0.0.1",m_INIFile.m_nAppServerPort,m_INIFile.m_nSSLMode);
		client2.Connect(err);
		if (!err.IsOK())
		{
			QMessageBox::information(this,"","Error connecting 127.0.0.1: "+err.getErrorText());
			return;
		}
		else
		{
			QMessageBox::information(this,"","Success connecting to the 127.0.0.1!!");
		}
		*/
		return;
	}

	//SEND:
	client.msg_send.AddParameter(&bSkipCatalogReload,"bSkipCatalogReload");
	client.RestSend(err,"POST","/rest/service/settings/reload");

}

void appserver_john_gui::SendRenameCommandToServer(Status &err,QString strOldCatalog, QString strNewCatalog)
{
	//CONNECT:
	RestHTTPClient client;
	client.SetConnectionSettings("127.0.0.1",m_INIFile.m_nAppServerPortActual,m_INIFile.m_nSSLMode,3);
	client.Connect(err);
	if (!err.IsOK())
		return;

	//SEND:
	client.msg_send.AddParameter(&strOldCatalog,"strOldCatalog");
	client.msg_send.AddParameter(&strNewCatalog,"strNewCatalog");

	client.RestSend(err,"POST","/rest/service/catalog/rename");
	if (!err.IsOK())return;
}

void appserver_john_gui::SendGetStatusCommandToServer(Status &err,DbRecordSet &Ret_Folders,int &nReloadSettings)
{
	//CONNECT:
	RestHTTPClient client;
	client.SetConnectionSettings("127.0.0.1",m_INIFile.m_nAppServerPortActual,m_INIFile.m_nSSLMode,3);
	client.Connect(err);
	if (!err.IsOK())
		return;

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("SendGetStatusCommandToServer/RestSend is about to be executed"));


	client.RestSend(err,"GET","/rest/service/catalogs/status");
	if (!err.IsOK())return;

	Ret_Folders.destroy();
	Ret_Folders.addColumn(QVariant::String,"ALIAS");
	Ret_Folders.addColumn(QVariant::Int,"STATUS");
	Ret_Folders.addColumn(QVariant::Int,"PIC_CNT");
	Ret_Folders.addColumn(QVariant::Int,"CNT");
	//Ret_Folders.addColumn(QVariant::Int,"CNT_FILES");
	//Ret_Folders.addColumn(QVariant::Int,"CNT_SUBDIRS");

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("SendGetStatusCommandToServer/RestSend is executed"));

	client.msg_recv.GetParameter(0,&Ret_Folders,err);
	if (!err.IsOK())
		return;
	client.msg_recv.GetParameter(1,&nReloadSettings,err);
	if (!err.IsOK())
		return;
	
	//Ret_Folders.Dump();

	Status err2;
	client.Disconnect(err2);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("SendGetStatusCommandToServer is executed"));
	
}


void appserver_john_gui::on_btnServerPicChange_clicked()
{
	QByteArray bytePic,bytePicLarge;
	QPixmap bufPixMap,bufPixMapLarge;
	if (PicHelper::LoadPicture(bytePic,bytePicLarge,bufPixMap,bufPixMapLarge))
	{
		m_SettingsManager.SetServerThumb(bytePic,bytePicLarge);

		//scale to 32x32:
		QPixmap pixMap;
		pixMap.loadFromData(bytePic);
		pixMap=pixMap.scaled(75,75);
		ui.labelServerPic->setPixmap(pixMap);

		SaveSettings(true,true);
	}


}

void appserver_john_gui::on_btnServerPicClear_clicked()
{
	QByteArray bytePic;
	QByteArray bytePicLarge;
	m_SettingsManager.SetServerThumb(bytePic,bytePicLarge);
	ui.labelServerPic->clear();
	SaveSettings(true,true);
}

void appserver_john_gui::on_btnInvite_clicked()
{
	SetEmailSettings(); //if user just added mail settings, save 'em in member object

	int nCount=m_lstFolders.getSelectedCount();
	int nRow=m_lstFolders.getSelectedRow();
	if (nCount!=1)
	{
		QMessageBox::warning(this,"Warning","Select one catalog to send invite!");
		return;
	}

	if (nRow>=0 && nRow<m_lstFolders.getRowCount())
	{
		DbRecordSet rowCatalog=m_lstFolders.getSelectedRecordSet();
		QString strCatalogName=rowCatalog.getDataRef(0,"ALIAS").toString();
		
		bool bEmailSettingsSet=false;
		DbRecordSet recMail;
		m_SettingsManager.GetEmailSettings(recMail);
		if(recMail.getRowCount()>0)
			if (!recMail.getDataRef(0,"SMTP_SERVER").toString().isEmpty())
				bEmailSettingsSet=true;

		if (!bEmailSettingsSet)
		{
			QMessageBox::warning(this,"Warning","Email settings are not set. Go to the Settings page and define Email settings!");
			return;
		}

		//get list of users: select users to send mail
		DlgUsers Dlg(this);
		DbRecordSet lstData=GetAssignedUsers(strCatalogName);
		if (lstData.getRowCount()==0)
		{
			QMessageBox::warning(this,"Operation Aborted",QString("Shared Folder %1 does not have any user assigned. To invite other people to see you catalog you must first assign them to the catalog!").arg(strCatalogName));
			return;
		}
		Dlg.SetData(lstData,&m_SettingsManager,true,false,true);
		if(!Dlg.exec())
			return;

		DbRecordSet lstSelectedData,lstUsers,lstDeleted;
		Dlg.GetData(lstSelectedData,lstUsers,lstDeleted);

		int nSize=lstSelectedData.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			QString strUserName=lstSelectedData.getDataRef(i,"USERNAME").toString();
			QString strFirstName=lstSelectedData.getDataRef(i,"FIRST_NAME").toString();
			QString strLastName=lstSelectedData.getDataRef(i,"LAST_NAME").toString();

			if (lstSelectedData.getDataRef(i,"EMAIL").toString().isEmpty())
			{
				QMessageBox::warning(this,"Operation Aborted",QString("User %1 - %2 %3  does not have email address set. Click 'Users', select user and set email addres!").arg(strUserName).arg(strLastName).arg(strFirstName));
				return;
			}
		}

		DlgSendEmailBody DlgEmailSend;
		QString strSubject,strBody;
		strSubject = "Watch my catalog \""+strCatalogName+"\"";
		QString strSalut;
		if (nSize==1)
		{
			strSalut=lstSelectedData.getDataRef(0,"SALUT").toString();
			//strBody = strSalut+"\n\r" + "You are invited to watch my picture catalog \"" +strCatalogName +"\" I have published on my computer using everDOCs!" + "\n\r" + recMail.getDataRef(0,"SIGNATURE").toString();
			//qDebug()<<recMail.getDataRef(0,"SIGNATURE").toString();
			strBody = strSalut+"\n\r" + "You are invited to see my document shared folder \"" +strCatalogName +"\" I have shared on my computer using everDOCs. Please follow the instructions below!\n\r" + recMail.getDataRef(0,"SIGNATURE").toString(); //MB: want to remove new line- RS 21.02.2011
		}
		else
			strBody = "You are invited to see my document shared folder \"" +strCatalogName +"\" I have shared on my computer using everDOCs. Please follow the instructions below!";

		DlgEmailSend.SetData(strSubject,strBody);
		if(!DlgEmailSend.exec()) return;
		DlgEmailSend.GetData(strSubject,strBody);


		Status err;
		EmailHandler::SendEmail(err,m_strAppServerID,rowCatalog,recMail,lstSelectedData,strSubject,strBody);

		if (!err.IsOK())
		{
			QMessageBox::warning(this,"Operation Failed",err.getErrorText());
		}
		else
		{
			//---//
			nSkipClearStatus=3;
			if (nSize==1)
				ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(QString("Successfully sent %1 email!").arg(nSize)));
			else
				ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(QString("Successfully sent %1 emails!").arg(nSize)));
		}
	}


}

void appserver_john_gui::on_btnDirectoryPicChange_clicked()
{
	int nCount=m_lstFolders.getSelectedCount();
	int nRow=m_lstFolders.getSelectedRow();
	if (nCount!=1)
	{
		QMessageBox::warning(this,"Warning","Select one catalog to define user rights!");
		return;
	}

	if (nRow>=0 && nRow<m_lstFolders.getRowCount())
	{
	QByteArray bytePic,bytePicLarge;
	QPixmap bufPixMap,bufPixMapLarge;
		if (PicHelper::LoadPicture(bytePic,bytePicLarge,bufPixMap,bufPixMapLarge,m_lstFolders.getDataRef(nRow,"PATH").toString()))
		{
			m_lstFolders.setData(nRow,"THUMB",bytePic);
			m_lstFolders.setData(nRow,"THUMB_LARGE",bytePicLarge);
			m_lstFolders.setData(nRow,"THUMB_LARGE_WIDTH",bufPixMapLarge.width());
			m_lstFolders.setData(nRow,"THUMB_LARGE_HEIGHT",bufPixMapLarge.height());


			//scale to 32x32:
			QPixmap pixMap;
			pixMap.loadFromData(bytePic);
			pixMap=pixMap.scaled(32,32);
			//ui.labelServerPic->setPixmap(pixMap);
			ui.labelDirPic->setPixmap(bufPixMap);

			SaveSettings(true,true);
		}
	}
}

void appserver_john_gui::on_btnDirectoryPicClear_clicked()
{
	int nCount=m_lstFolders.getSelectedCount();
	int nRow=m_lstFolders.getSelectedRow();
	if (nCount!=1)
	{
		QMessageBox::warning(this,"Warning","Select one catalog to define user rights!");
		return;
	}

	if (nRow>=0 && nRow<m_lstFolders.getRowCount())
	{
		ui.labelDirPic->clear();
		QByteArray bytePic;
		m_lstFolders.setData(nRow,"THUMB",bytePic);
		m_lstFolders.setData(nRow,"THUMB_LARGE",bytePic);
		SaveSettings(true,true);
	}

}


void appserver_john_gui::RefreshServerIcon()
{
	QByteArray bytePicture,bytePictureLarge;
	m_SettingsManager.GetServerThumb(bytePicture,bytePictureLarge);
	QPixmap pixMap;
	pixMap.loadFromData(bytePicture);
	pixMap=pixMap.scaled(75,75);
	ui.labelServerPic->setPixmap(pixMap);
}

void appserver_john_gui::RefreshDirectoryIcon(int nRow)
{
	if (nRow>=0 && nRow<m_lstFolders.getRowCount())
	{
		ui.btnDirectoryPicChange->setEnabled(true);
		ui.btnDirectoryPicClear->setEnabled(true);

		QByteArray bytePicture=m_lstFolders.getDataRef(nRow,"THUMB").toByteArray();
		QPixmap pixMap;
		pixMap.loadFromData(bytePicture);
		pixMap=pixMap.scaled(32,32);
		ui.labelDirPic->setPixmap(pixMap);
	}
	else
	{
		ui.btnDirectoryPicChange->setEnabled(false);
		ui.btnDirectoryPicClear->setEnabled(false);
		ui.labelDirPic->clear();
	}

}



void appserver_john_gui::OnDirectorySelectionChanged()
{
	int nCount=m_lstFolders.getSelectedCount();
	int nRowSel=m_lstFolders.getSelectedRow();
	if (nCount==1)
	{
		RefreshDirectoryIcon(nRowSel);
	}
	else
	{
		RefreshDirectoryIcon(-1);
	}
}


//show assigned users per one catalog: can add new assignment for N users or remove
void appserver_john_gui::on_btnUsers_clicked()
{
	int nCount=m_lstFolders.getSelectedCount();
	int nRow=m_lstFolders.getSelectedRow();
	if (nCount!=1)
	{
		QMessageBox::warning(this,"Warning","Please select only one catalog to view assigned users!");
		return;
	}

 	QString strCatalog=m_lstFolders.getDataRef(nRow,"ALIAS").toString();

	DlgAssignedUsers Dlg(this);
	Dlg.SetData(m_lstUsers,&m_SettingsManager,m_lstUserRights,strCatalog);
	if(!Dlg.exec())
	{ //MB: requested this silly option...hahaha
		Dlg.GetData(m_lstUsers,m_lstUserRights);
		ui.tableWidget->RefreshDisplay();
		m_lstUserRightsForFolder.clear();
		OnUserSelectionChanged();
		SaveSettings(true,true);
		return;
	}

	Dlg.GetData(m_lstUsers,m_lstUserRights);

	ui.tableWidget->RefreshDisplay();
	m_lstUserRightsForFolder.clear();
	OnUserSelectionChanged();

	SaveSettings(true,true);
}



void appserver_john_gui::on_btnAddUserRight_clicked()
{
	//selected user or users
	int nCount=m_lstUsers.getSelectedCount();
	if (nCount==0)
	{
		QMessageBox::warning(this,"Warning","Select one or more users to assign them to the catalog!");
		return;
	}
	//get selected users:
	DbRecordSet lstSelectedData;
	lstSelectedData.copyDefinition(m_lstUsers);
	lstSelectedData.merge(m_lstUsers,true);


	//open dialog to pick catalog(s)
	DlgCatalogs Dlg(this);
	Dlg.SetData(m_lstFolders);
	if(!Dlg.exec())
		return;

	DbRecordSet lstCatalogs=Dlg.GetData();
	if (lstCatalogs.getRowCount()==0)
		return;

/*
	int nSize=lstSelectedData.getRowCount();
	int nSize2=lstCatalogs.getRowCount();

	
	for (int i=0;i<nSize;i++)
	{
		QString strUserName=lstSelectedData.getDataRef(i,"USERNAME").toString();

		int nCnt=0;
		for (int k=0;k<nSize2;k++)
		{
			QString strCatalog=lstCatalogs.getDataRef(k,"ALIAS").toString();

			int nSelected=m_lstUserRights.find("ALIAS",strCatalog);
		
			if (m_lstUserRights.find("USERNAME",strUserName,false,true,true,true)>0)
			{
				QMessageBox::warning(this,"Warning",QString("User %1 is already assigned to the catalog %2. Assignment for user is aborted!").arg(strUserName).arg(strCatalog));
				lstSelectedData.selectRow(i); //dumpy user if hes already assign to some catalog
				continue;
			}
		}
	}
	lstSelectedData.deleteSelectedRows();
	*/

	//if more > 1 show to choose access right for all
	if (lstSelectedData.getRowCount()>0)
	{

		DlgDefaultUserRights Dlg;
		if(!Dlg.exec())
			return;

		lstSelectedData.clearSelection();
		lstCatalogs.clearSelection();

		bool bFireQuestion=false;
		bool bFireQuestionAnswerYes=false;

		DbRecordSet rowUserRights=Dlg.GetDefaultUserRight();
		int nSize=lstSelectedData.getRowCount();
		int nSize2=lstCatalogs.getRowCount();

		//rowUserRights.Dump();
		//lstSelectedData.Dump();
		//lstCatalogs.Dump();

		for (int i=0;i<nSize;i++)
		{
			QString strUserName=lstSelectedData.getDataRef(i,"USERNAME").toString();

			for (int k=0;k<nSize2;k++)
			{
				QString strCatalog=lstCatalogs.getDataRef(k,"ALIAS").toString();

				int nSelected=m_lstUserRights.find("ALIAS",strCatalog);
				if (m_lstUserRights.find("USERNAME",strUserName,false,true,true,true)>0)
				{
					
					//MB: RS: ask before overwrite (only once for all users)
					if (!bFireQuestion)
					{
						if(QMessageBox::question(this,"Confirmation",QString("Do you want to overwrite the user rights of the existing assignments?"), tr("Yes"),tr("No"))==0)
						{
							bFireQuestionAnswerYes=true;
						}
						bFireQuestion=true;
					}

					if (bFireQuestionAnswerYes)
					{
						m_lstUserRights.deleteSelectedRows();
					}

				}

				m_lstUserRights.addRow();
				int nRow2=m_lstUserRights.getRowCount()-1;
				m_lstUserRights.assignRow(nRow2,rowUserRights);
				m_lstUserRights.setData(nRow2,"USERNAME",lstSelectedData.getDataRef(i,"USERNAME"));
				m_lstUserRights.setData(nRow2,"ALIAS",strCatalog);

				//m_lstUserRights.Dump();
			}

		}

		//m_lstUserRights.Dump();

		//clear existing ar:
		//NABIJEM ONOGA KO JE OVO ZAKOMENTIRA...
		m_lstUserRightsForFolder.clear();
		OnUserSelectionChanged();
	}
	else
	{
		//QMessageBox::warning(this,"Warning","No assignment possible as assignments already exists!");
		return;
	}

	SaveSettings(true,true);
	
}

void appserver_john_gui::on_btnDeleteUserRight_clicked()
{
	int nCount=m_lstUserRightsForFolder.getSelectedCount();
	if (nCount==0)
	{
		QMessageBox::warning(this,"Warning","Select user right to delete it!");
		return;
	}

	//delete from main list and from local:
	int nSize=m_lstUserRightsForFolder.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (!m_lstUserRightsForFolder.isRowSelected(i))
			continue;

		QString strUserName=m_lstUserRightsForFolder.getDataRef(i,"USERNAME").toString();
		QString strCatalog=m_lstUserRightsForFolder.getDataRef(i,"ALIAS").toString();
		m_lstUserRights.find("ALIAS",strCatalog);
		m_lstUserRights.find("USERNAME",strUserName,false,true,true,true);

		m_lstUserRights.deleteSelectedRows();
	}

	m_lstUserRightsForFolder.deleteSelectedRows();
	ui.tableWidgetRights->RefreshDisplay();
	SaveSettings(true,true);
}


void appserver_john_gui::RefreshUserRightTable(int nRow)
{
	//save previous values to the main list:
	bool bSaveNeed=false;
	int nSize=m_lstUserRightsForFolder.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strUserName=m_lstUserRightsForFolder.getDataRef(i,"USERNAME").toString();
		QString strCatalog=m_lstUserRightsForFolder.getDataRef(i,"ALIAS").toString();
		m_lstUserRights.find("ALIAS",strCatalog);
		m_lstUserRights.find("USERNAME",strUserName,false,true,true,true);
		int nRow=m_lstUserRights.getSelectedRow();
		if (nRow>=0)
		{
			DbRecordSet rowTmp = m_lstUserRightsForFolder.getRow(i);
			//compare two rows: if <> assign and set flag to save:
			if (!bSaveNeed)
			{
				if (rowTmp.getDataRef(0,"CAN_DOWNLOAD").toInt()!=m_lstUserRights.getDataRef(nRow,"CAN_DOWNLOAD").toInt())
					bSaveNeed=true;
				if (rowTmp.getDataRef(0,"CAN_UPLOAD").toInt()!=m_lstUserRights.getDataRef(nRow,"CAN_UPLOAD").toInt())
					bSaveNeed=true;
				if (rowTmp.getDataRef(0,"CAN_DELETE").toInt()!=m_lstUserRights.getDataRef(nRow,"CAN_DELETE").toInt())
					bSaveNeed=true;
				if (rowTmp.getDataRef(0,"CAN_SEND_INVITE").toInt()!=m_lstUserRights.getDataRef(nRow,"CAN_SEND_INVITE").toInt())
					bSaveNeed=true;
			}
			m_lstUserRights.assignRow(rowTmp, true); //always set...
		}
	}

	if (bSaveNeed)
		SaveSettings(true,true);

	if (nRow==-1)
	{
		m_lstUserRightsForFolder.clear();
		ui.btnAddUserRight->setEnabled(true);
		ui.btnDeleteUserRight->setEnabled(false);
	}
	else
	{
		ui.btnAddUserRight->setEnabled(true);
		ui.btnDeleteUserRight->setEnabled(true);
		QString strUserName=m_lstUsers.getDataRef(nRow,"USERNAME").toString();
		m_lstUserRights.find("USERNAME",strUserName);

		m_lstUserRightsForFolder.clear();
		m_lstUserRightsForFolder.merge(m_lstUserRights,true);
		m_lstUserRightsForFolder.sort("ALIAS");
	}

	ui.tableWidgetRights->RefreshDisplay();
}

DbRecordSet appserver_john_gui::GetAssignedUsers(QString strCatalog)
{
	DbRecordSet lstAssignedUsers;
	lstAssignedUsers.copyDefinition(m_lstUsers);
	m_lstUserRights.find("ALIAS",strCatalog);

	int nSize=m_lstUserRights.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (m_lstUserRights.getDataRef(i,"ALIAS").toString()==strCatalog)
		{
			QString strUserName=m_lstUserRights.getDataRef(i,"USERNAME").toString();
			int nRow=m_lstUsers.find("USERNAME",strUserName,true);
			if (nRow>=0)
			{
				lstAssignedUsers.addRow();
				DbRecordSet rowTmp = m_lstUsers.getRow(nRow);
				lstAssignedUsers.assignRow(lstAssignedUsers.getRowCount()-1, rowTmp);
			}
		}
	}

	return lstAssignedUsers;
}





void appserver_john_gui::on_btnAdd_clicked()
{
	DlgUserChange dlg;
	if(dlg.exec())
	{
		DbRecordSet recUser;
		dlg.GetData(recUser);

		QString strUserName=recUser.getDataRef(0,"USERNAME").toString();
		QString strPass=recUser.getDataRef(0,"PASSWORD").toString();

		if(m_lstUsers.find("USERNAME",strUserName)>0 || strUserName==m_SettingsManager.GetAdminUserName())
		{
			QMessageBox::critical(this,"Error","User with that username already exists!");
			return;
		}

		m_lstUsers.addRow();
		m_lstUsers.assignRow(m_lstUsers.getRowCount()-1,recUser,true);
		m_lstUsers.sort("USERNAME");
		m_lstUsers.find("USERNAME",strUserName);
		ui.tableWidget->RefreshDisplay();
		OnUserSelectionChanged();
		SaveSettings(true,true);
	}
}
void appserver_john_gui::on_btnDelete_clicked()
{
	int nRowCount=m_lstUsers.getSelectedCount();
	if (nRowCount>0)
	{
		if(QMessageBox::question(this,"Confirmation",QString("Do you really want to delete selected users?").arg(nRowCount), tr("Yes"),tr("No"))==0)
		{
			DbRecordSet lstSelected=m_lstUsers.getSelectedRecordSet();

			int nSize=lstSelected.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				QString strUserName=lstSelected.getDataRef(i,"USERNAME").toString();
				m_lstUserRights.find("USERNAME",strUserName);
				m_lstUserRights.deleteSelectedRows();
			}

			m_lstUsers.deleteSelectedRows();

			m_lstUsers.selectRow(0);
			ui.tableWidget->RefreshDisplay();
			OnUserSelectionChanged();
			SaveSettings(true,true);
		}
	}
}

void appserver_john_gui::on_btnPass_clicked()
{

	int nRow=m_lstUsers.getSelectedRow();
	if (nRow<0)
	{
		QMessageBox::warning(this,"Warning","Select user to change user data!");
		return;
	}

	DlgUserChange dlg(this);
	dlg.SetData(m_lstUsers.getRow(nRow));
	if(dlg.exec())
	{
		DbRecordSet recUser;
		dlg.GetData(recUser);
		
		QString strUserNameOld=m_lstUsers.getDataRef(nRow,"USERNAME").toString();
		QString strUserName=recUser.getDataRef(0,"USERNAME").toString();
		QString strPass=recUser.getDataRef(0,"PASSWORD").toString();

		if(m_lstUsers.find("USERNAME",strUserName)>1 || strUserName==m_SettingsManager.GetAdminUserName())
		{
			QMessageBox::critical(this,"Error","User with that username already exists!");
			return;
		}
		
		m_lstUsers.assignRow(nRow,recUser,true);
		m_lstUsers.sort("USERNAME");
		m_lstUsers.find("USERNAME",strUserName);

		//change username for changed usernames
		int nSize2=m_lstUserRights.getRowCount();
		for (int k=0;k<nSize2;k++)
		{
			if (m_lstUserRights.getDataRef(k,"USERNAME").toString()==strUserNameOld)
			{
				m_lstUserRights.setData(k,"USERNAME",strUserName);
			}
		}

		//find all user rights then rename:
		ui.tableWidget->RefreshDisplay();
		OnUserSelectionChanged();
		SaveSettings(true,true);
	}
}


//n user change, load all catalogs for selected user and display 'em
void appserver_john_gui::OnUserSelectionChanged()
{
	int nCount=m_lstUsers.getSelectedCount();
	int nRowSel=m_lstUsers.getSelectedRow();
	if (nCount==1)
	{
		RefreshUserRightTable(nRowSel);
	}
	else
	{
		RefreshUserRightTable(-1);
	}

}


//set email settings into db:
void appserver_john_gui::SetEmailSettings()
{
	DbRecordSet recData;
	SettingsManager::DefineEmailSettings(recData);
	recData.addRow();

	recData.setData(0,"SMTP_SERVER", ui.txtServer->text());
	recData.setData(0,"SMTP_PORT", ui.txtMailPort->text().toInt());
	recData.setData(0,"EMAIL_ADDRESS", ui.txtEmail->text());
	recData.setData(0,"USERNAME", ui.txtUserEmail->text());
	recData.setData(0,"PASSWORD", ui.txtPassEmail->text());
	QByteArray data=ui.txtSignature->toPlainText().toUtf8();
	recData.setData(0,"SIGNATURE", data);

	m_SettingsManager.SetEmailSettings(recData);
}


void appserver_john_gui::OnReloadCatalogs()
{
	killTimer(m_nTimerID);
	ResetProgressBar();
	SaveSettings(true);
	RefreshProgressBar();
	m_nTimerID=startTimer(TIMER_CHECK_SERVER);
}

void appserver_john_gui::OnRenameCatalog(int nRow)
{
	QString strOldName=m_lstFolders_Old.getDataRef(nRow,"ALIAS").toString();
	QString strNewName=m_lstFolders.getDataRef(nRow,"ALIAS").toString();
	if (strNewName==strOldName)
		return;

	//delete from main list and from local:
	int nSize=m_lstUserRights.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (m_lstUserRights.getDataRef(i,"ALIAS").toString()==strOldName)
			m_lstUserRights.setData(i,"ALIAS",strNewName);
	}
	OnUserSelectionChanged();



	SaveSettings(true,true,true);

	/*

	if (m_pServiceConfig->IsServiceRunning())
	{
		Status err;
		SendRenameCommandToServer(err,strOldName,strNewName);
		if (!err.IsOK())
		{
			QMessageBox::critical(this,tr("Error"),err.getErrorText());
			return;
		}
	}
	else
	{

		QString strOutThumbDirRoot=m_INIFile.m_strThumbDir;
		if (strOutThumbDirRoot.isEmpty())
			strOutThumbDirRoot=m_strHomePath+"/user_storage/thumb_cache";

		QDir dirRoot(strOutThumbDirRoot);
		if (dirRoot.exists(strOldName))
		{
			if(!dirRoot.rename(strOldName,strNewName))
			{
				QMessageBox::critical(this,tr("Error"),"Shared Folder rename failed! Restart server to fix problem!");
			}
		}

	}
*/
}


void appserver_john_gui::on_txtRootPass_editingFinished()
{
	QString strPass;
	m_SettingsManager.GetRootPass(strPass);
	if (strPass!=ui.txtRootPass->text())
	{
		SaveSettings(true,true);
	}
	
}

void appserver_john_gui::on_txtServerName_editingFinished()
{
	if (m_SettingsManager.GetServerName()!=ui.txtServerName->text())
	{
		SaveSettings(true,true);
	}
}
void appserver_john_gui::on_txtMailPort_editingFinished()
{
	SaveSettings(true,true);
}


void appserver_john_gui::OnlabelServerID_customContextMenuRequested ( const QPoint & pos)
{
	QMenu CnxtMenu(this);
	CnxtMenu.addAction(m_pActionCopy_ServerID);
	CnxtMenu.exec(ui.labelServerID->mapToGlobal(pos));
}
void appserver_john_gui::OnlabelIP_customContextMenuRequested ( const QPoint & pos)
{
	QMenu CnxtMenu(this);
	CnxtMenu.addAction(m_pActionCopy_IP);
	CnxtMenu.exec(ui.labelIP->mapToGlobal(pos));
}
void appserver_john_gui::OnlabelIPpriv_customContextMenuRequested ( const QPoint & pos )
{
	QMenu CnxtMenu(this);
	CnxtMenu.addAction(m_pActionCopy_IPLocal);
	CnxtMenu.exec(ui.labelIPpriv->mapToGlobal(pos));
}

void appserver_john_gui::OnlabelIDString_customContextMenuRequested ( const QPoint & pos)
{
	QMenu CnxtMenu(this);
	CnxtMenu.addAction(m_pActionCopy_ID_StringAll);
	CnxtMenu.exec(ui.labelServerConnect->mapToGlobal(pos));
}

void appserver_john_gui::OnCopyServerID()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(m_strAppServerID);
}
void appserver_john_gui::OnCopyIP()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(m_strOuterIP);
}

void appserver_john_gui::OnCopyIPLocal()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(m_strAppServerIPpriv);
}

void appserver_john_gui::OnCopyIDString()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(m_strConnectionString);
}


void appserver_john_gui::AddMyPictureDir()
{
	QString strFolderToImport=PicHelper::GetMyPicturesFolder();

	if (!strFolderToImport.isEmpty())
	{
		if(QMessageBox::question(this,"Create Shared Folder",QString("Do you want to add some Shared Folders for publishing now?"), tr("Yes"),tr("No"))==0)
		{
			ui.tableDirs->DialogAddNewDirectory(strFolderToImport);
		}
	}

}


void appserver_john_gui::BuildStyleWidgets()
{
	//QString strStyle= " {background:qradialgradient(cx:0.5, cy:0.5, radius: 1, fx:0.5, fy:0.5, stop:0 rgb( 178, 186, 202), stop:1 rgb(79,98,135));}";
	
	QString strStyle= " {background-image:url(:Resources/PooltableBlue256.png); background-repeat: repeat-xy; background-color: rgb(255, 0, 0)}";
	ui.frame_Bkg->setObjectName("FFH_MANAGER");
	ui.frame_Bkg->setStyleSheet("QFrame#FFH_MANAGER " + strStyle);

	ui.stackedWidgetHelp->setStyleSheet("QLabel {color:white}");

	//LAYOUT:
	QSize buttonSize(190, 62);
	QSize buttonSize_2(62, 62);

	QString	styleNew="QLabel {color:white;}";

	//CREATE BUTTONS/SIZE
	//m_pBtnCatalogs	=	new StyledPushButton(this,":Resources/Menu_Catalog_BW.png",":Resources/Menu_Catalog_Col.png",styleNew,QString("<html>"+strStyleShadow+"<span id=\"text\">text shadow</span></html>"));
	m_pBtnCatalogs	=	new StyledPushButton(this,":Resources/Menu_Catalog_BW.png",":Resources/Menu_Catalog_Col.png",styleNew,QString("<html><div style=\"font-family:Arial; font-size:13pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1<br>%2</div></html>").arg(tr("Shared")).arg(tr("Folders")),5,20);
	m_pBtnUsers	=		new StyledPushButton(this,":Resources/Menu_Users_BW.png",":Resources/Menu_Users_Col.png",styleNew,QString("<html><div style=\"font-family:Arial; font-size:13pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</div></html>").arg(tr("Users")),5,20);
	m_pBtnSettings	=	new StyledPushButton(this,":Resources/Menu_Settings_BW.png",":Resources/Menu_Settings_Col.png",styleNew,QString("<html><div style=\"font-family:Arial; font-size:13pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</div></html>").arg(tr("Settings")),5,20);
	m_pBtnInfo	=		new StyledPushButton(this,":Resources/Menu_EverXBridge_BW.png",":Resources/Menu_EverXBridge_Col.png",styleNew,"",5,20);


	m_pLabel_info = new QLabel;
	m_pLabel_info->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	m_pLabel_info->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
	m_pLabel_info->setText(QString(HTML_LABEL_INFO).arg(tr("Overview")));
	m_pLabel_info->setStyleSheet("QLabel {color:white; background: transparent; }");
	m_pLabel_info->setCursor(Qt::PointingHandCursor);
	connect(m_pLabel_info,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnMenu_InfoClickLink( const QString &)));
	QGraphicsDropShadowEffect * effect = new QGraphicsDropShadowEffect();
	effect->setXOffset(1);
	effect->setYOffset(1);
	effect->setColor(Qt::black);
	effect->setBlurRadius(3.0);
	m_pLabel_info->setGraphicsEffect(effect);


	#define BKG_CE_MENU_DROPZONE	"QFrame#frameBkg { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }"


	//m_nToolBarButtonIDCurrentFocus=1;
	connect(m_pBtnCatalogs,SIGNAL(clicked()),this,SLOT(OnMenu_CatalogsClick()));
	connect(m_pBtnUsers,SIGNAL(clicked()),this,SLOT(OnMenu_UsersClick()));
	connect(m_pBtnSettings,SIGNAL(clicked()),this,SLOT(OnMenu_SettingsClick()));
	connect(m_pBtnInfo,SIGNAL(clicked()),this,SLOT(OnMenu_InfoClick()));

	//size
	m_pBtnCatalogs	->setMaximumSize(buttonSize);
	m_pBtnCatalogs	->setMinimumSize(buttonSize);
	m_pBtnUsers		->setMaximumSize(buttonSize);
	m_pBtnUsers		->setMinimumSize(buttonSize);
	m_pBtnSettings	->setMaximumSize(buttonSize);
	m_pBtnSettings	->setMinimumSize(buttonSize);
	m_pBtnInfo		->setMaximumSize(buttonSize_2);
	m_pBtnInfo		->setMinimumSize(buttonSize_2);


	

	QString	strFrameLayout=QString("QFrame#frameMenu { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }").arg(":Resources/Theme_PoolTableBlue_ToolbarBkg.png");

	//Tool m_ProgressBar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;

	buttonLayout->setAlignment(Qt::AlignLeft);
	//buttonLayout->addSpacing(5);
	buttonLayout->addWidget(m_pBtnCatalogs);
	buttonLayout->addWidget(m_pBtnUsers);
	buttonLayout->addWidget(m_pBtnSettings);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnInfo);
	buttonLayout->addSpacing(7);
	buttonLayout->addWidget(m_pLabel_info);

	buttonLayout->setContentsMargins(0,2,5,0);
	buttonLayout->setSpacing(1);

	ui.frameMenu->setLayout(buttonLayout);
	ui.frameMenu->setStyleSheet(strFrameLayout);

	strStyle= " { border-top-width: 10px; border-right-width: 7px; border-bottom-width: 7px; border-left-width: 9px; border-image:url(:Resources/Inset_Chapter_GR.png) 10px 7px 7px 9px stretch stretch; background: transparent; background-color: rgb(17,62,94)} QLabel {color:black} ";
	ui.frameServerId->setStyleSheet("QFrame#frameServerId " + strStyle);

	ui.frameMaster->setStyleSheet("QFrame#frameMaster " + strStyle);
	ui.frameData->setStyleSheet("QFrame#frameData " + strStyle);
	ui.frameThumb->setStyleSheet("QFrame#frameThumb " + strStyle);
	ui.frameServer->setStyleSheet("QFrame#frameServer " + strStyle);
	ui.frameAccess->setStyleSheet("QFrame#frameAccess " + strStyle);
	ui.frameServerData->setStyleSheet("QFrame#frameServerData " + strStyle);
	ui.frameEmail->setStyleSheet("QFrame#frameEmail " + strStyle);


	//STATUS BAR:
	ui.progressBar->setValue(0);
	ui.progressBar->setVisible(false);
	ui.frameStatusBar->setStyleSheet("QFrame#frameStatusBar {background-color: rgb(226,229,235)} QLabel {color:black; font-style:italic; font-weight:500;font-family:Arial; font-size:8pt}");

	ui.labelSemafor1->setPixmap(QPixmap(":Resources/Status_Green22.png"));
	ui.labelSemafor2->setPixmap(QPixmap(":Resources/Status_Green22.png"));
	ui.labelSemafor1_2->setPixmap(QPixmap(":Resources/Status_Green.png"));
	ui.labelSemafor2_2->setPixmap(QPixmap(":Resources/Status_Green.png"));

	ui.labelSemaforBig->setPixmap(QPixmap(":Resources/Status_Red.png"));
	ui.labelHome->setPixmap(QPixmap(":Resources/Home_Symbol.png"));
	ui.labeWLAN->setPixmap(QPixmap(":Resources/WLAN_Symbol.png"));
	ui.labeInternet->setPixmap(QPixmap(":Resources/Internet_Symbol.png"));

	ui.labelHelp->setText(QString(HTML_4_HELP_1).arg(tr("What is the problem?")));
	connect(ui.labelHelp,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnHelp1_linkActivated( const QString &)));
	connect(ui.labelStatus,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnHelp1_linkActivated( const QString &)));
	connect(ui.labelPortForward,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnHelp1_linkActivated( const QString &)));
	connect(ui.label_ReadMore,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnHelp3_linkActivated( const QString &)));
	//connect(ui.labelURL_HowToConnect,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnHelp2_linkActivated( const QString &)));

	//ui.labelServerStatus->setStyleSheet(styleNew);
	//ui.labelStart->setStyleSheet(styleNew);
	//QString strStyleBlack="QLabel {color:black}";
	//ui.labelServerConnect->setStyleSheet(strStyleBlack);
	ui.labelServerConnect->clear();
	ui.labelStatus->clear();
	ui.labelURL_Access->clear();
	//ui.labelURL_Access_2->clear();
	ui.labelConnectString->clear();

	QSize buttonSizeStart(44, 44);
	m_pBtnStart	=	new StyledPushButton(this,":Resources/Start_Button.png",":Resources/Start_Button.png",styleNew);
	m_pBtnStart	->setMaximumSize(buttonSizeStart);
	m_pBtnStart	->setMinimumSize(buttonSizeStart);
	m_pBtnStart->setPressedIcon(":Resources/Start_Button_Pressed.png");

	connect(m_pBtnStart,SIGNAL(clicked()),this,SLOT(on_btnStart_clicked()));

	ui.verticalLayout_Start->removeWidget(ui.widgetDummy);
	ui.verticalLayout_Start->insertWidget(1,m_pBtnStart);

	
	//ui.label_h1->setPixmap(QPixmap(":Resources/WhiteDot16.png"));
	//ui.label_h2->setPixmap(QPixmap(":Resources/WhiteDot16.png"));
	//ui.label_h3->setPixmap(QPixmap(":Resources/WhiteDot16.png"));
	ui.label_h1_1->setPixmap(QPixmap(":Resources/WhiteDot16.png"));
	ui.label_h2_1->setPixmap(QPixmap(":Resources/WhiteDot16.png"));
	ui.label_h3_1->setPixmap(QPixmap(":Resources/WhiteDot16.png"));
	ui.label_h2_a->setPixmap(QPixmap(":Resources/WhiteDot16.png"));
	ui.label_h2_b->setPixmap(QPixmap(":Resources/WhiteDot16.png"));


//	ui.label_h4->setPixmap(QPixmap(":Resources/WhiteDot16.png"));
	//ui.label_h3a->setPixmap(QPixmap(":Resources/Upload_Icon32.png"));
	//ui.label_h4a->setPixmap(QPixmap(":Resources/Upload_Icon32.png"));
	ui.label_appstore->setPixmap(QPixmap(":Resources/Appstore.png"));
	ui.label_appstore->SetURL("http://itunes.apple.com/app/everpics/id428497847?mt=8");
	ui.label_appstore_2->setPixmap(QPixmap(":Resources/Appstore.png"));
	ui.label_appstore_2->SetURL("http://itunes.apple.com/app/everpics/id428497847?mt=8");

	QString	strFrameLayout1=QString("QFrame#frameSysOverview1 { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }").arg(":Resources/SystemOverviewPicBkg.png");
	ui.frameSysOverview1->setStyleSheet(strFrameLayout1);

	QString	strFrameLayout5=QString("QFrame#frameHowToConnect { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }").arg(":Resources/HowToConnect_WithoutText.png");
	ui.frameHowToConnect->setStyleSheet(strFrameLayout5);

	

	QString	strFrameLayout2=QString("QFrame#frameLogoEverPics { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }").arg(":Resources/everPICs_Text_HR.png");
	ui.frameLogoEverPics->setStyleSheet(strFrameLayout2);
	

	ui.labelServerID_2->setStyleSheet(styleNew);
	ui.labelIP_2->setStyleSheet(styleNew);
	ui.labelIPpriv_2->setStyleSheet(styleNew);
		
	//shadow effect:
	ui.labelServerStatus->setStyleSheet("QLabel {color:white; background: transparent; }");
	ui.labelStart->setStyleSheet("QLabel {color:white; background: transparent; }");
	ui.labelStart->setGraphicsEffect(PicHelper::GetShadowEffect());
	ui.labelServerStatus->setGraphicsEffect(PicHelper::GetShadowEffect());
	//ui.labelTitle->setStyleSheet("QLabel {color:white; background: transparent; }");
	//ui.labelTitle->setGraphicsEffect(PicHelper::GetShadowEffect());

	ui.labelCat1->setStyleSheet("QLabel {color:white; background: transparent; }");
	ui.labelCat1->setGraphicsEffect(PicHelper::GetShadowEffect());
	ui.labelCat2->setStyleSheet("QLabel {color:white; background: transparent; }");
	ui.labelCat2->setGraphicsEffect(PicHelper::GetShadowEffect());
	
	
	
	//CREATE BUTTONS/SIZE
	on_btnSystemOverview_clicked();
	//OnMenu_CatalogsClick();


	//ABOUT WIDGET
	QString strMsg;
	//if (QString(APPLICATION_VERSION).right(3).toInt()>0 && QString(APPLICATION_VERSION).indexOf(".")>=0)
	//	strMsg.sprintf(tr("%sP (%s %s)").toLatin1().constData(), APPLICATION_VERSION, DataHelper::GetCompileDate(), __TIME__); //this is private build: Marin wants P after version
	//else
	strMsg =QString("%1<br>%2 %3").arg(APPLICATION_VERSION).arg(DataHelper::GetCompileDate()).arg(__TIME__);

	strMsg =QString(HTML_1_START_BUTTON).arg(strMsg);
	ui.labelVersion->setText(strMsg);



	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnAddFolder->setStyleSheet(strStyleBtn);
//	ui.btnAddiPhoto->setStyleSheet(strStyleBtn);
	ui.btnRemoveFolder->setStyleSheet(strStyleBtn);
	ui.btnUsers->setStyleSheet(strStyleBtn);
	ui.btnInvite->setStyleSheet(strStyleBtn);
	
	ui.btnAdd->setStyleSheet(strStyleBtn);
	ui.btnDelete->setStyleSheet(strStyleBtn);
	ui.btnPass->setStyleSheet(strStyleBtn);
	ui.btnAddUserRight->setStyleSheet(strStyleBtn);
	ui.btnDeleteUserRight->setStyleSheet(strStyleBtn);

	ui.btnAdd->setStyleSheet(strStyleBtn);
	ui.btnDelete->setStyleSheet(strStyleBtn);
	ui.btnPass->setStyleSheet(strStyleBtn);
	ui.btnAddUserRight->setStyleSheet(strStyleBtn);
	ui.btnDeleteUserRight->setStyleSheet(strStyleBtn);

	ui.label_iPhone->setPixmap(QPixmap(":Resources/App_iPhone47.png"));
	ui.label_Access->setPixmap(QPixmap(":Resources/Webapp_Earth44.png"));


	ui.labelServerConnect->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	ui.labelServerConnect->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
	//ui.labelServerID->setText(QString("<html><div><a href=\"http://1\" ><span style=\"font-family:Arial; color: blue; font-size:14pt; font-weight:bold; font-style:normal; text-decoration:none;\">%1</span></a></div></html>").arg(tr("serverid")));
	ui.labelServerConnect->setCursor(Qt::PointingHandCursor);
	connect(ui.labelServerConnect,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnLink_BridgeID_ClickLink( const QString &)));

	ui.labelURL_Access->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
	ui.labelURL_Access->setCursor(Qt::PointingHandCursor);
	//ui.labelURL_Access_2->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
	//ui.labelURL_Access_2->setCursor(Qt::PointingHandCursor);
	connect(ui.labelURL_Access,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnLink_Access_ClickLink( const QString &)));
	//connect(ui.labelURL_Access_2,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnLink_Access_ClickLink( const QString &)));
	//connect(ui.labelConnectString,SIGNAL(linkActivated ( const QString &)),this,SLOT(OnLink_Access_ClickLink( const QString &)));
	

//#if __APPLE__
//	ui.btnAddiPhoto->setVisible(true);
//#else
//	ui.btnAddiPhoto->setVisible(false);
//#endif


	//ui.txtDataDir->setVisible(false);
	ui.txtCatalogDir->setVisible(false);
	//ui.label_28->setVisible(false);
	//ui.label_34->setVisible(false);
	//ui.btnDataDir->setVisible(false);
	ui.btnCatalogDir->setVisible(false);
	ui.groupBoxDelete->setVisible(false);

	ui.frameAccess->setVisible(false);
	ui.frameThumb->setVisible(false);

	ui.ckbShowAllDrives->setStyleSheet("QCheckBox {color:white}");
	ui.ckbShowOnlyDocuments->setStyleSheet("QCheckBox {color:white}");


	QString strStyle2=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnLogSend->setStyleSheet(strStyle2);
	ui.btnLogSave->setStyleSheet(strStyle2);

	//B.T.2012.03.29 -by Marin: disable settings page:
	if (m_INIFile.m_nDisableSettingPage)
	{
		m_pBtnSettings->setVisible(false);
	}

#ifdef NO_ADMIN_BUILD
	m_pBtnStart->setEnabled(false);
	ui.btnDataDir->setEnabled(false);
#endif


}

void appserver_john_gui::OnMenu_CatalogsClick()
{
	ui.stackedWidget->setCurrentIndex(0);

	QString	styleWhite="QLabel {color:white}";
	QString	styleYellow="QLabel {color:yellow}";
	
	m_pBtnCatalogs->setIcon(":Resources/Menu_Catalog_Col.png");
	m_pBtnCatalogs->setStyleSheet(styleYellow);

	m_pBtnUsers->setIcon(":Resources/Menu_Users_BW.png");
	m_pBtnUsers->setStyleSheet(styleWhite);

	m_pBtnSettings->setIcon(":Resources/Menu_Settings_BW.png");
	m_pBtnSettings->setStyleSheet(styleWhite);

	m_pBtnInfo->setIcon(":Resources/Menu_EverXBridge_BW.png");
	m_pLabel_info->setText(QString(HTML_LABEL_INFO).arg(tr("Overview")));


}
void appserver_john_gui::OnMenu_UsersClick()
{
	ui.stackedWidget->setCurrentIndex(1);

	QString	styleWhite="QLabel {color:white}";
	QString	styleYellow="QLabel {color:yellow}";

	m_pBtnCatalogs->setIcon(":Resources/Menu_Catalog_BW.png");
	m_pBtnCatalogs->setStyleSheet(styleWhite);

	m_pBtnUsers->setIcon(":Resources/Menu_Users_Col.png");
	m_pBtnUsers->setStyleSheet(styleYellow);

	m_pBtnSettings->setIcon(":Resources/Menu_Settings_BW.png");
	m_pBtnSettings->setStyleSheet(styleWhite);

	m_pBtnInfo->setIcon(":Resources/Menu_EverXBridge_BW.png");
	m_pLabel_info->setText(QString(HTML_LABEL_INFO).arg(tr("Overview")));

}
void appserver_john_gui::OnMenu_SettingsClick()
{
	ui.stackedWidget->setCurrentIndex(2);

	QString	styleWhite="QLabel {color:white}";
	QString	styleYellow="QLabel {color:yellow}";

	m_pBtnCatalogs->setIcon(":Resources/Menu_Catalog_BW.png");
	m_pBtnCatalogs->setStyleSheet(styleWhite);

	m_pBtnUsers->setIcon(":Resources/Menu_Users_BW.png");
	m_pBtnUsers->setStyleSheet(styleWhite);

	m_pBtnSettings->setIcon(":Resources/Menu_Settings_Col.png");
	m_pBtnSettings->setStyleSheet(styleYellow);

	m_pBtnInfo->setIcon(":Resources/Menu_EverXBridge_BW.png");
	m_pLabel_info->setText(QString(HTML_LABEL_INFO).arg(tr("Overview")));

}
void appserver_john_gui::OnMenu_InfoClick()
{
	ui.stackedWidget->setCurrentIndex(3);

	QString	styleWhite="QLabel {color:white}";
	QString	styleYellow="QLabel {color:yellow}";

	m_pBtnCatalogs->setIcon(":Resources/Menu_Catalog_BW.png");
	m_pBtnCatalogs->setStyleSheet(styleWhite);

	m_pBtnUsers->setIcon(":Resources/Menu_Users_BW.png");
	m_pBtnUsers->setStyleSheet(styleWhite);

	m_pBtnSettings->setIcon(":Resources/Menu_Settings_BW.png");
	m_pBtnSettings->setStyleSheet(styleWhite);

	m_pBtnInfo->setIcon(":Resources/Menu_EverXBridge_Col.png");
	m_pLabel_info->setText(QString(HTML_LABEL_INFO).arg(tr("Overview")));


}

void appserver_john_gui::on_btnSystemOverview_clicked()
{
	ui.stackedWidgetHelp->setCurrentIndex(0);

	QString strStylePressed=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button_Pressed.png","white");
	QString strStyle2=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnSystemOverview->setStyleSheet(strStylePressed);
	ui.btnConnectClients->setStyleSheet(strStyle2);
	ui.btnAccessRight->setStyleSheet(strStyle2);
	ui.btnPublishing->setStyleSheet(strStyle2);
	ui.btnAccessFromInternet->setStyleSheet(strStyle2);
	ui.btnAbout->setStyleSheet(strStyle2);


}

void appserver_john_gui::on_btnConnectClients_clicked()
{
	ui.stackedWidgetHelp->setCurrentIndex(1);
	QString strStylePressed=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button_Pressed.png","white");
	QString strStyle2=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnSystemOverview->setStyleSheet(strStyle2);
	ui.btnConnectClients->setStyleSheet(strStylePressed);
	ui.btnAccessRight->setStyleSheet(strStyle2);
	ui.btnPublishing->setStyleSheet(strStyle2);
	ui.btnAccessFromInternet->setStyleSheet(strStyle2);
	ui.btnAbout->setStyleSheet(strStyle2);
}
void appserver_john_gui::on_btnPublishing_clicked()
{
	ui.stackedWidgetHelp->setCurrentIndex(2);
	QString strStylePressed=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button_Pressed.png","white");
	QString strStyle2=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnSystemOverview->setStyleSheet(strStyle2);
	ui.btnConnectClients->setStyleSheet(strStyle2);
	ui.btnAccessRight->setStyleSheet(strStyle2);
	ui.btnPublishing->setStyleSheet(strStylePressed);
	ui.btnAccessFromInternet->setStyleSheet(strStyle2);
	ui.btnAbout->setStyleSheet(strStyle2);
}


void appserver_john_gui::on_btnAccessRight_clicked()
{
	ui.stackedWidgetHelp->setCurrentIndex(3);
	QString strStylePressed=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button_Pressed.png","white");
	QString strStyle2=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnSystemOverview->setStyleSheet(strStyle2);
	ui.btnConnectClients->setStyleSheet(strStyle2);
	ui.btnAccessRight->setStyleSheet(strStylePressed);
	ui.btnPublishing->setStyleSheet(strStyle2);
	ui.btnAccessFromInternet->setStyleSheet(strStyle2);
	ui.btnAbout->setStyleSheet(strStyle2);
}




void appserver_john_gui::on_btnAccessFromInternet_clicked()
{
	ui.stackedWidgetHelp->setCurrentIndex(4);
	QString strStylePressed=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button_Pressed.png","white");
	QString strStyle2=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnSystemOverview->setStyleSheet(strStyle2);
	ui.btnConnectClients->setStyleSheet(strStyle2);
	ui.btnPublishing->setStyleSheet(strStyle2);
	ui.btnAccessRight->setStyleSheet(strStyle2);
	ui.btnAccessFromInternet->setStyleSheet(strStylePressed);
	ui.btnAbout->setStyleSheet(strStyle2);
}

void appserver_john_gui::on_btnAbout_clicked()
{
	ui.stackedWidgetHelp->setCurrentIndex(5);
	QString strStylePressed=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button_Pressed.png","white");
	QString strStyle2=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnSystemOverview->setStyleSheet(strStyle2);
	ui.btnConnectClients->setStyleSheet(strStyle2);
	ui.btnPublishing->setStyleSheet(strStyle2);
	ui.btnAccessFromInternet->setStyleSheet(strStyle2);
	ui.btnAccessRight->setStyleSheet(strStyle2);
	ui.btnAbout->setStyleSheet(strStylePressed);
}

void appserver_john_gui::SetButtonStatus(int nStatus)
{

	switch (nStatus)
	{
	case STATUS_OFFLINE:
		{
			//m_pBtnStart->setIcon(":Resources/Start_Button.png");
			//m_pBtnStart->setPressedIcon(":Resources/Start_Button_Pressed.png");
			ui.labelStart->setText(QString(HTML_1_START_BUTTON).arg(tr("START")));
			ui.labelServerStatus->setText(QString(HTML_2_SERVER_ONLINE).arg(tr("Server")).arg(tr("Stopped")));
			ui.frameBkg1->setVisible(true);
			//ui.labelSemaforBig->setVisible(true);
			
			ui.labelSemafor1->setVisible(false);
			ui.labelSemafor2->setVisible(false);
			ui.labelSemafor1_2->setVisible(false);
			ui.labelSemafor2_2->setVisible(false);
			ui.labelHome->setVisible(false);
			ui.labeWLAN->setVisible(false);
			ui.labeInternet->setVisible(false);
			ui.labelHelp->setVisible(false);
		}
		break;
	case STATUS_ONLINE_WLAN:
		{
			//m_pBtnStart->setIcon(":Resources/Start_Button_Pressed.png");
			//m_pBtnStart->setPressedIcon(":Resources/Start_Button.png");
			ui.labelStart->setText(QString(HTML_1_START_BUTTON).arg(tr("STOP")));
			ui.labelServerStatus->setText(QString(HTML_2_SERVER_ONLINE).arg(tr("Server")).arg(tr(" Online ")));
			//ui.labelSemaforBig->setVisible(false);
			ui.frameBkg1->setVisible(false);

			ui.labelSemafor2->setPixmap(QPixmap(":Resources/Status_Red22.png"));
			ui.labelSemafor1->setVisible(true);
			ui.labelSemafor2->setVisible(true);
			ui.labelSemafor1_2->setVisible(true);
			ui.labelSemafor2_2->setVisible(true);
			ui.labelSemafor2_2->setPixmap(QPixmap(":Resources/Status_Red.png"));


			ui.labelHome->setVisible(true);
			ui.labeWLAN->setVisible(true);
			ui.labeInternet->setVisible(true);
			ui.labelHelp->setVisible(true);

		}
		break;
	case STATUS_ONLINE_FULL:
		{
			//m_pBtnStart->setIcon(":Resources/Start_Button_Pressed.png");
			//m_pBtnStart->setPressedIcon(":Resources/Start_Button.png");
			ui.labelStart->setText(QString(HTML_1_START_BUTTON).arg(tr("STOP")));
			ui.labelServerStatus->setText(QString(HTML_2_SERVER_ONLINE).arg(tr("Server")).arg(tr(" Online ")));
			//ui.labelSemaforBig->setVisible(false);
			ui.frameBkg1->setVisible(false);

			ui.labelSemafor2->setPixmap(QPixmap(":Resources/Status_Green22.png"));
			ui.labelSemafor1->setVisible(true);
			ui.labelSemafor2->setVisible(true);
			ui.labelSemafor1_2->setVisible(true);
			ui.labelSemafor2_2->setVisible(true);
			ui.labelSemafor2_2->setPixmap(QPixmap(":Resources/Status_Green.png"));

			ui.labelHome->setVisible(true);
			ui.labeWLAN->setVisible(true);
			ui.labeInternet->setVisible(true);
			ui.labelHelp->setVisible(false);
		}
		break;
	case STATUS_POWER_UP:
		{
			//m_pBtnStart->setIcon(":Resources/Start_Button_Pressed.png");
			//m_pBtnStart->setPressedIcon(":Resources/Start_Button.png");
			ui.labelStart->setText(QString(HTML_1_START_BUTTON).arg(tr("STOP")));
			ui.labelServerStatus->setText(QString(HTML_2_SERVER_ONLINE).arg(tr("Server")).arg(tr("Checking Status")));
			//ui.labelSemaforBig->setVisible(false);
			ui.frameBkg1->setVisible(false);

			ui.labelSemafor2->setPixmap(QPixmap(":Resources/Status_Yellow22.png"));
			ui.labelSemafor1->setVisible(true);
			ui.labelSemafor2->setVisible(true);
			ui.labelSemafor1_2->setVisible(true);
			ui.labelSemafor2_2->setVisible(true);

			ui.labelHome->setVisible(true);
			ui.labeWLAN->setVisible(true);
			ui.labeInternet->setVisible(true);
			ui.labelHelp->setVisible(false);
		}
		break;
	}


}

void appserver_john_gui::OnHelp1_linkActivated( const QString &link)
{
	OnMenu_InfoClick();
	on_btnAccessFromInternet_clicked();
}

void appserver_john_gui::OnHelp2_linkActivated( const QString &link)
{
	OnMenu_InfoClick();
	on_btnConnectClients_clicked();
}


void appserver_john_gui::OnHelp3_linkActivated( const QString &link)
{
	OnMenu_InfoClick();
	on_btnAccessFromInternet_clicked();
}



//if error, strHomePath=""
//2012-B.T: new logic:
//1. check home path of application inside registry -> if exists then re-route to settings there, if not, create new from start (copy all that is needed)
//2. check former Thumb directory -> if settings path is not at start of this then reroute all settings to this dir, but only when passing from 106 and below version to newer.
//thumb: /user_storage/thumb_cache
//new cat: /catalogs
//settings: /settings
//3. m_strThumbDir will contain settings path (home path of application). thumb data: is now hardcoded as: home_path/user_storage/thumb_cache

void appserver_john_gui::InitializeSettingsDirectory(QString &strHomePath)
{
	bool bNewUser=false;
	//determine home path:
	QSettings settings(QSettings::SystemScope,QCoreApplication::organizationName(),QCoreApplication::applicationName());
	strHomePath = settings.value("SettingsPath","").toString();

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory 1="+strHomePath);

	if (strHomePath.isEmpty())
	{
		bNewUser=true;
		strHomePath=DataHelper::GetApplicationHomeDir();
		if (strHomePath.isEmpty())
			return;
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory 2="+strHomePath);



	//-------------------------------------
	//	normal behaviour for new users:
	//-------------------------------------

	//re-create settings subdir:
	QString strSettingsDir=strHomePath+"/settings";
	QDir setDir(strSettingsDir);
	if (!setDir.exists())
	{
		QDir bcpDirCreate(strHomePath);
		if(bcpDirCreate.mkdir("settings"))
			strSettingsDir=strHomePath+"/settings";
		else
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory bNewUser=failed to create settings in the"+strHomePath);
			strHomePath="";
			return ;
		}
	}

	//to be sure copy these file to new settings dir:
	if(!QFile::exists(strSettingsDir+"/server.cert"))
		QFile::copy(QCoreApplication::applicationDirPath()+"/template_settings/server.cert",strSettingsDir+"/server.cert");
	if(!QFile::exists(strSettingsDir+"/server.pkey"))
		QFile::copy(QCoreApplication::applicationDirPath()+"/template_settings/server.pkey",strSettingsDir+"/server.pkey");


	//write new value:
	settings.setValue("SettingsPath",strHomePath);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory 3="+strHomePath);


	//------------------------------------------------------
	//	behavior for old users who came from <106 version:
	//------------------------------------------------------
	if (bNewUser)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory bNewUser="+strHomePath);

		//this will create new ini file!!!
		ServerIniFile INIFile;
		INIFile.Load(strSettingsDir+"/app_server.ini");
		INIFile.m_strThumbDir=strHomePath;
		INIFile.Save(strSettingsDir+"/app_server.ini");

		//create thumb dir:
		bool bOk=true;
		QString strThumbDir=strHomePath+"/user_storage/thumb_cache";
		if (!QFile::exists(strThumbDir))
		{
			QDir dir(strHomePath);
			if(!dir.mkdir("user_storage")) //try to create thumb dir, if fail then return error
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory bNewUser=failed to create user_storage in the"+strHomePath);
				strHomePath="";
				return;
			}
			else
			{
				QDir dirx(strHomePath+"/user_storage");
				if(!dirx.mkdir("thumb_cache")) //try to create thumb dir, if fail then return error
				{
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory bNewUser=failed to create thumb_cache in the"+strHomePath+"/user_storage");
					strHomePath="";
					return;
				}
			}
		}

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory bNewUser=OK");

	}
	else
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory OldUser 0="+strHomePath);

		//only if thumb cache dir is different then make all fuss...
		ServerIniFile INIFile;
		if(INIFile.Load(strSettingsDir+"/app_server.ini",false))
		{
			QString strThumbDir=INIFile.m_strThumbDir;
			//this means that this is normal user that upgrades from normal settings dir path, and thumb cache dir is not re-allocated.
			if (strThumbDir.toLower() == QString(strHomePath+"/user_storage/thumb_cache").toLower() || strThumbDir.isEmpty())
			{
				INIFile.m_strThumbDir=strHomePath;
				INIFile.Save(strSettingsDir+"/app_server.ini");
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory OldUser 1="+strHomePath);
			}
			else if (strThumbDir!=strHomePath) //thumb cache dir points to new location -> copy all files to new location, make adjustment . If not then
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory OldUser 2="+strHomePath);

				//re-create settings subdir in new target:
				QString strSettingsDirNEW=strThumbDir+"/settings";
				QDir setDirNEW(strSettingsDirNEW);
				if (!setDirNEW.exists())
				{
					QDir bcpDirCreate(strThumbDir);
					if(bcpDirCreate.mkdir("settings"))
						strSettingsDirNEW=strThumbDir+"/settings";
					else
					{
						strHomePath="";
						return ;
					}
				}

				//thumb data:
				QDir dir2(strThumbDir);
				if(!dir2.mkdir("user_storage"))
				{
					strHomePath="";
					return ;
				}
				else
				{
					QDir dirx(strThumbDir+"/user_storage");
					if(!dirx.mkdir("thumb_cache")) //try to create thumb dir, if fail then return error
					{
						strHomePath="";
						return;
					}
				}


				//catalog dir: leave as is!!??

				//copy settings file
				QFile::copy(strSettingsDir+"/app_server.ini",strSettingsDirNEW+"/app_server.ini");
				QFile::copy(strSettingsDir+"/user_data",strSettingsDirNEW+"/user_data");
				QFile::copy(strSettingsDir+"/meta_data",strSettingsDirNEW+"/meta_data");
				QFile::copy(QCoreApplication::applicationDirPath()+"/template_settings/server.cert",strSettingsDirNEW+"/server.cert");
				QFile::copy(QCoreApplication::applicationDirPath()+"/template_settings/server.pkey",strSettingsDirNEW+"/server.pkey");

				//mark new location:
				strHomePath = INIFile.m_strThumbDir;
				settings.setValue("SettingsPath",strHomePath);
				INIFile.Save(strSettingsDirNEW+"/app_server.ini");

			}
			else //thumbdir == path -> so this is newest version of code: just to be sure, check if settings substructure exists, if not create new...
			{
				if(!QFile::exists(strHomePath+"/settings/app_server.ini"))
				{
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory OldUser 3="+strHomePath);

					//create new subdirs, ignore errors (INI file will be created as new inside ini manager:
					QDir dirHome(strHomePath);
					dirHome.mkdir("settings");
					dirHome.mkdir("user_storage");
					QDir dirx(strHomePath+"/user_storage");
					dirx.mkdir("thumb_cache");


					QFile::copy(QCoreApplication::applicationDirPath()+"/template_settings/server.cert",strHomePath+"/settings/server.cert");
					QFile::copy(QCoreApplication::applicationDirPath()+"/template_settings/server.pkey",strHomePath+"/settings/server.pkey");
				}

			}

		}
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"InitializeSettingsDirectory Final="+strHomePath);

	ui.txtDataDir->setText(strHomePath);

}

void appserver_john_gui::InitiDirs()
{
	//does not use catalog nor thumb cache dir

	return;
}


//1st call server
//2nd if list empty call calculate all
//3th on every call search first: indexing then calculate for it: set to list and go on
//4th store last indexing, if changed-> call server.. or on regular interval

//read settings n/total + estimate time....

void appserver_john_gui::ResetProgressBar(bool bForceShutDown)
{
	m_strProgress_LastCatalogIndexing="";
	m_RetFolders.clear();
	m_nProgress_ServerCall_Count=0;
	m_nProgressLastRemainToIndex=0;
	ui.progressBar->setVisible(false);
	nSkipClearStatus--;
	if (nSkipClearStatus<=0)
	{
		ui.labelStatus->clear();
		nSkipClearStatus=0;
	}
}

QString appserver_john_gui::GetCurrentProcessingCatalog()
{
	DbRecordSet lstTemp=m_lstFolders;
	int nRowCurr=lstTemp.find("STATUS",STATUS_INDEXING,true);
	if (nRowCurr>=0)
		return lstTemp.getDataRef(nRowCurr,"ALIAS").toString();
	else
		return "";
}

//only place to set status of all catalogs: direct from server
void appserver_john_gui::ProgressBar_CallServerForStatus(Status &err)
{

	err.setError(0);
	DbRecordSet lstFolderStatusServer;
	int nReloadSettings=0;

	SendGetStatusCommandToServer(err,lstFolderStatusServer,nReloadSettings);
	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("SendGetStatusCommandToServer failed %1").arg(err.getErrorText()));
		//supress negative messages:
		//QMessageBox::critical(this,tr("Error"),err.getErrorText());
		return;
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("ProgressBar_CallServerForStatus/SendGetStatusCommandToServer is executed"));

	if (nReloadSettings)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("ProgressBar_CallServerForStatus/Reload Settings is about to be executed"));

		LoadSettings();
		OnUserSelectionChanged(); //reload user rights
	}

	//_DUMP(lstFolderStatusServer);

	//1. set status icon of all .
	//m_lstFolders.setColValue("STATUS",STATUS_NOT_INDEXED);



	if (lstFolderStatusServer.getColumnIdx("ALIAS")<0 || lstFolderStatusServer.getColumnIdx("STATUS")<0 || lstFolderStatusServer.getColumnIdx("PIC_CNT")<0)
		return;

	int nSize=m_lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strAlias = m_lstFolders.getDataRef(i,"ALIAS").toString();
		int nRow= lstFolderStatusServer.find("ALIAS",strAlias,true);
		if (nRow>=0)
		{
			m_lstFolders.setData(i,"STATUS",lstFolderStatusServer.getDataRef(nRow,"STATUS").toInt());
			if (lstFolderStatusServer.getDataRef(nRow,"PIC_CNT").toInt()>0)
				m_lstFolders.setData(i,"PIC_CNT",lstFolderStatusServer.getDataRef(nRow,"PIC_CNT").toInt());
		}
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("ProgressBar_CallServerForStatus RefreshDisplay()"));

	ui.tableDirs->RefreshDisplay();
}

void appserver_john_gui::RefreshProgressBar()
{

	//if enabled then server is not running-> no point to refresh bar! is it!
	if (ui.txtPort->isEnabled())
	{
		return;
	}


	Status err;
	m_nProgress_ServerCall_Count++;
	if (m_nProgress_ServerCall_Count>SERVER_CALL_INTERVAL) //reset counter when rich
		m_nProgress_ServerCall_Count=0;

	//read file reload and if exists go reload
	QFileInfo info_ini(m_strIniFilePath);
	QString strFileName=info_ini.absolutePath()+"/settings_reload";
	if (QFile::exists(strFileName))
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshProgressBar/reload settings"));

		m_nProgress_ServerCall_Count=1; //force server reload
		QFile::remove(strFileName);
	}


	//always call server to confirm status codes!
	if (m_nProgress_ServerCall_Count==1) //call server every 1-2minutes instead of every 5sec as before...
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RefreshProgressBar/ProgressBar_CallServerForStatus is about to be executed"));

		//refresh local IP on server interval:
		m_strAppServerIPpriv=TcpHelper::ResolveLocalHostURLService("0.0.0.0",m_INIFile.m_nAppServerPort);

		Status err;
		ProgressBar_CallServerForStatus(err);
		if (!err.IsOK())
			return;
	}

	return;

	/*

	//CHECK IF WE NEED progress bar at all:
	DbRecordSet lstTemp=m_lstFolders; //to avoid move selection
	int nCntIndexed=0;
	int nSize=lstTemp.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nStatus=lstTemp.getDataRef(i,"STATUS").toInt();
		if (nStatus == STATUS_OK || nStatus == STATUS_COULD_NOT_BE_INDEXED)
		{
			nCntIndexed++;
		}
	}
	if (nCntIndexed==m_lstFolders.getRowCount())
	{
		if (ui.progressBar->isVisible()) //reset only if active
			ResetProgressBar();
		return;
	}

	//keep calling server non stop..
	m_nProgress_ServerCall_Count=0;

	//calc thumb root
	QString strOutThumbDirRoot=m_INIFile.m_strThumbDir;
	if (strOutThumbDirRoot.isEmpty())
		strOutThumbDirRoot=m_strHomePath+"/user_storage/thumb_cache";


	int nCurrentCount =0;
	int nTotalCount=0;

	//save folders status:
	nSize=m_lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nStatus=m_lstFolders.getDataRef(i,"STATUS").toInt();
		if (nStatus==STATUS_OK) // || nStatus==STATUS_COULD_NOT_BE_INDEXED)
			nCurrentCount += m_lstFolders.getDataRef(i,"PIC_CNT").toInt()*THUMB_COUNT;
		else if (nStatus==STATUS_INDEXING || nStatus ==STATUS_NOT_INDEXED)
		{
			QString strThumbCacheDir=strOutThumbDirRoot+"/"+m_lstFolders.getDataRef(i,"ALIAS").toString();
			nCurrentCount +=FileCruncher::GetFileCountFromFolderFast(strThumbCacheDir);
		}
	}

	//save folders status:
	//nSize=m_lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nStatus=m_lstFolders.getDataRef(i,"STATUS").toInt();
		if (nStatus==STATUS_COULD_NOT_BE_INDEXED)
			continue; //skip marked as can not
		nTotalCount += m_lstFolders.getDataRef(i,"PIC_CNT").toInt();
	}
	nTotalCount=nTotalCount*THUMB_COUNT;

	if (nCurrentCount>=nTotalCount)
	{
		ResetProgressBar();
		return;
	}

	int nRemainToIndex= nTotalCount - nCurrentCount;

	//progress bar:
	int nVal=0;
	if (m_nProgressLastRemainToIndex>0)
		nVal=((float)(m_nProgressLastRemainToIndex-nRemainToIndex)/m_nProgressLastRemainToIndex)*100;

	ui.progressBar->setValue(nVal);
	ui.progressBar->setVisible(true);

	QString strEstimateTime;
	float fSecondsEstimate = 0;
	if (m_nProgressLastRemainToIndex>0)
	{
		int nDiv=m_nProgressLastRemainToIndex - nRemainToIndex;
		//if (nDiv<=0)
		//	nDiv=1;

		if (nDiv>0)
		{
			int nSecElapsed=m_datProgressStart.secsTo(QDateTime::currentDateTime());
			float fFactor=(float)nSecElapsed/nDiv;
			fSecondsEstimate = fFactor*nRemainToIndex; 

			int nMinEstimate = fSecondsEstimate/60;
			int nSecEstimate = fSecondsEstimate-60*nMinEstimate;
			if (fSecondsEstimate>0)
				if (nMinEstimate>0)
					strEstimateTime=QString(tr(": Catalog available in %1min %2 sec...")).arg(nMinEstimate).arg(nSecEstimate);
				else
					strEstimateTime=QString(tr(": Catalog available in %1 sec...")).arg(nSecEstimate);
		}

	}

	if (m_nProgressLastRemainToIndex==0 || m_nProgressLastRemainToIndex<nRemainToIndex)
	{
		m_nProgressLastRemainToIndex=nRemainToIndex;
		m_datProgressStart=QDateTime::currentDateTime();
	}
	
	QString strMessage=QString(tr("Indexing %1 documents ")).arg(m_nProgressLastRemainToIndex/THUMB_COUNT);

	if (!strEstimateTime.isEmpty())
		strMessage+=strEstimateTime;

	ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(strMessage));
*/
}



void appserver_john_gui::on_btnDataDir_clicked()
{

	//get open dir:
	QString strDefaultPath = m_INIFile.m_strThumbDir;
	if (strDefaultPath.isEmpty())
	{
		//open file dialog:
		if (!g_strLastDir_FileOpen.isEmpty())
			strDefaultPath=g_strLastDir_FileOpen;
	}

	QString	strDir=QFileDialog::getExistingDirectory ( this, "Select Directory for EverPICs Bridge data storage", strDefaultPath);
	if (!strDir.isEmpty())
	{
		m_INIFile.m_strThumbDir = strDir;
		g_strLastDir_FileOpen=strDir;
		ui.txtDataDir->setText(strDir);

		//save settings
		SaveSettings(true);
		SaveINISettings();

		//create substructure in new place
		//copy content from old place
		InitializeSettingsDirectory(m_strHomePath);

		//INI:
		m_strIniFilePath= m_strHomePath+"/settings/app_server.ini";

		//Settings manager:
		m_SettingsManager.Initialize(m_strHomePath);

		g_Logger.EnableFileLogging(m_strHomePath+"/settings/client.log",0,200);

		//bog nam pomogao!!!

	}

	//->settings manager..etc...etc...
	//->thumb manager...etc..etc...

	//ui.tableDirs->SetDirs(m_INIFile.m_strCatalogDir,m_INIFile.m_strThumbDir);
	ui.tableDirs->SetDirs(m_INIFile.m_strCatalogDir,m_strHomePath+"/user_storage/thumb_cache");

}


void appserver_john_gui::on_btnCatalogDir_clicked()
{
	//get open dir:
	QString strDefaultPath = m_INIFile.m_strCatalogDir;
	if (strDefaultPath.isEmpty())
	{
		//open file dialog:
		if (!g_strLastDir_FileOpen.isEmpty())
			strDefaultPath=g_strLastDir_FileOpen;
	}

	QString	strDir=QFileDialog::getExistingDirectory ( this, "Select Directory for new catalogs", strDefaultPath);
	if (!strDir.isEmpty())
	{
		if (m_INIFile.m_strThumbDir.toLower().contains(strDir.toLower()) || strDir.toLower().contains(m_INIFile.m_strThumbDir.toLower()))
		{
			QMessageBox::warning(this,"Warning",QString("Thumb cahce and catalog directory can not have same path"));
			return;
		}

		m_INIFile.m_strCatalogDir = strDir;
		g_strLastDir_FileOpen=strDir;
		ui.txtCatalogDir->setText(strDir);
	}

	//ui.tableDirs->SetDirs(m_INIFile.m_strCatalogDir,m_INIFile.m_strThumbDir);
	ui.tableDirs->SetDirs(m_INIFile.m_strCatalogDir,m_strHomePath+"/user_storage/thumb_cache");
}


void appserver_john_gui::OnLink_BridgeID_ClickLink( const QString &)
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(m_strAppServerID);
}
void appserver_john_gui::OnLink_Access_ClickLink( const QString &)
{

	QString strAccess = "http://everxconnect.com:11000/invite/"+m_strAppServerID;
#ifdef __APPLE__
	QDesktopServices::openUrl(strAccess);
#else
	bool bOK=false;
	QString strPath;
	QSettings settingsD("HKEY_LOCAL_MACHINE\\SOFTWARE\\Apple Computer, Inc.\\Safari",	QSettings::NativeFormat);
	strPath = settingsD.value("BrowserExe").toString();
	if (!strPath.isEmpty())
	{
		//start from cmd line
		//QString strApp="cmd /C "+strPath;
		QString strApp="\""+QDir::toNativeSeparators(strPath)+"\"";
		QString strURL=strAccess.trimmed();
		//strURL.prepend("\"");
		//strURL.append("\"");
		strApp=strApp+" "+strURL;
		if(QProcess::startDetached(strApp))
			bOK=true;
	}

	if (!bOK)
		QDesktopServices::openUrl(strAccess);
#endif
}


void appserver_john_gui::SetForwardMethod(int nMethod)
{
	QString strMsg=" ";


	if (nMethod==3)
	{
		strMsg="Port Forwarding Active";
	}
	else if (nMethod==1)
	{
		strMsg="NAT-PMP Active";
	}
	else if (nMethod==2)
	{
		strMsg="UPnP Active";
	}

	if (m_INIFile.m_nAppServerPortActual!=0)
		strMsg = strMsg+"<br>"+QString::number(m_INIFile.m_nAppServerPortActual);

	ui.labelPortForward_Show->setText(QString(HTML_PORT_FORWARD).arg(strMsg));
	ui.labelPortForward_2->setText(QString(HTML_PORT_FORWARD_SMALL).arg(strMsg));
}



bool appserver_john_gui::TestEmailSettings()
{
	bool bEmailSettingsSet=false;
	DbRecordSet recMail;
	m_SettingsManager.GetEmailSettings(recMail);
	if(recMail.getRowCount()>0)
		if (!recMail.getDataRef(0,"SMTP_SERVER").toString().isEmpty())
			bEmailSettingsSet=true;

	if (!bEmailSettingsSet)
	{
		if(QMessageBox::question(this,"Confirmation",QString("The email settings are not configured. The configuration is required if you want to invite friends by email. Do you want to configure the email settings before you start the server?"), tr("Yes"),tr("No"))==0)
		{
			OnMenu_SettingsClick();
			ui.txtServer->setFocus();
			return false; //abort START and go to email page
		}
	}

	return true;
}

bool appserver_john_gui::TestAdmin()
{
	if (ui.txtRootPass->text()=="admin")
	{
			if(QMessageBox::question(this,"Confirmation",QString("You did not change the default password \"admin\" in the Settings tab. This could be a security problem. Do you want to change it now?"), tr("Yes"),tr("No"))==0)
			{
				ui.txtRootPass->setFocus();
				return false; //abort START and go to email page
			}
	}

	return true;
}


void appserver_john_gui::UpdateWebPages()
{
	return;

	HttpReaderSync WebReader(6000);  //6sec..
	QByteArray byteVersionData;
	if(!WebReader.ReadWebContent(FFH_WEBAPP_VERSION_URL, &byteVersionData))
		return;

	//parse file:
	QStringList lstRevisions=QString(byteVersionData).split(";",QString::SkipEmptyParts);

	//find max revision with major & minor number
	QString strTarget=QString::number(JOHN_PROGRAM_VERSION_MAJOR)+"."+QString::number(JOHN_PROGRAM_VERSION_MINOR);
	QString strFoundTarget="";
	for (int i=0;i<lstRevisions.size();i++)
	{
		if (lstRevisions.at(i).indexOf(strTarget)==0)
		{
			if (lstRevisions.at(i)>strFoundTarget)
			{
				strFoundTarget=lstRevisions.at(i);
			}
		}
	}

	strTarget = strTarget+"."+QString::number(JOHN_PROGRAM_VERSION_REVISION);

	if (m_INIFile.m_strWebAppVersion.isEmpty())
	{
		m_INIFile.m_strWebAppVersion=strTarget;
		m_INIFile.Save(m_strIniFilePath);
	}

	if (!strFoundTarget.isEmpty() & strFoundTarget!=strTarget)
	{
		if (m_INIFile.m_strWebAppVersion < strFoundTarget)
		{

			if(QMessageBox::question(this,"Confirmation",QString("The newer version of web application files are found. Do you want to update and restart server?"), tr("Yes"),tr("No"))==0)
			{
				QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

				QString strURL=FFH_WEBAPP_DATA_URL+strFoundTarget+".bin";
				//download:
				HttpReaderSync WebReader_1(20000);  
				QByteArray byteData;
				ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(tr("Downloading webdata.bin. Please be patient!")));
				if(!WebReader_1.ReadWebContent(strURL, &byteData))
				{
					QMessageBox::warning(this,"Error","Failed to download update!");
					return;
				}
				ui.labelStatus->clear();

				QApplication::restoreOverrideCursor();

				//save to install dir /webdata.bin:
				QFile data(QCoreApplication::applicationDirPath()+"/webdata.bin");
				if(data.open(QIODevice::WriteOnly))
				{
					data.write(byteData);
					data.close();

					//restart server:
					if (m_pServiceConfig->IsServiceRunning())
					{
						on_btnStart_clicked();
						on_btnStart_clicked();
					}

					m_INIFile.m_strWebAppVersion=strFoundTarget;
					m_INIFile.Save(m_strIniFilePath);
				}
				else
				{
					QMessageBox::warning(this,"Error","Failed to write webdata.bin!");
				}

				return;
			}
		}

	}



	
}


//true if settings are to be saved
bool appserver_john_gui::ActualizeSettings()
{

	//--------------------update width and height-------------------------
	bool bStatus=false;

	if (m_lstFolders.getColumnIdx("THUMB_LARGE_WIDTH")<0)
	{
		m_lstFolders.addColumn(QVariant::Int,"THUMB_LARGE_WIDTH");
		m_lstFolders.addColumn(QVariant::Int,"THUMB_LARGE_HEIGHT");
	}

	int nSize=m_lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (m_lstFolders.getDataRef(i,"THUMB_LARGE_WIDTH").toInt()==0)
		{
			bStatus=true;

			//load pic and set height and width and save settings
			QByteArray bytePic = m_lstFolders.getDataRef(i,"THUMB_LARGE").toByteArray();
			QPixmap pix;
			pix.loadFromData(bytePic);
			int nWidth=pix.width();
			int nHeight=pix.height();

			m_lstFolders.setData(i,"THUMB_LARGE_WIDTH",nWidth);
			m_lstFolders.setData(i,"THUMB_LARGE_HEIGHT",nHeight);
		}
	}
	//--------------------update width and height-------------------------





	return bStatus;
}


void appserver_john_gui::on_txtPort_editingFinished()
{
	//ui.txtPortEnd->blockSignals(true);
	//ui.txtPortEnd->setText(ui.txtPort->text());
	//ui.txtPortEnd->blockSignals(false);
}

void appserver_john_gui::on_txtPortEnd_editingFinished()
{
	int nPortEnd=ui.txtPortEnd->text().trimmed().toInt();
	int nPort=ui.txtPort->text().trimmed().toInt();
	if (nPortEnd>0 && nPortEnd<65535)
	{
		if (nPortEnd<nPort)
		{
			ui.txtPortEnd->blockSignals(true);
			ui.txtPortEnd->setText(ui.txtPort->text());
			ui.txtPortEnd->blockSignals(false);
		}
	}
	else
	{
		ui.txtPortEnd->blockSignals(true);
		ui.txtPortEnd->setText(ui.txtPort->text());
		ui.txtPortEnd->blockSignals(false);
	}

}

void appserver_john_gui::on_btnLogSend_clicked()
{
	//zip log & send
	Status err;
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(tr("Sending log to the mylog@everpics.com. Please wait.")));
	EmailHandler::SendAppServerLog(err, m_strAppServerID, m_strHomePath+"/settings/appserver.log", "mylog@everpics.com", "mylog@everpics.com", "smtp.interway.ch", 0, "mylog@everpics.com", "zqjk8943");
	QApplication::restoreOverrideCursor();
	ui.labelStatus->setText("");
	if (err.IsOK())
	{
		QMessageBox::information(this,tr("Success!"),QString("Log successfully sent!"));
	}
	else
	{
		QMessageBox::critical(this,tr("Error occurred!"),QString("Failed to send email. Error: %1").arg(err.getErrorText()));
	}
}

void appserver_john_gui::on_btnLogSave_clicked()
{

	QString strSource=m_strHomePath+"/settings/appserver.log";

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	ui.labelStatus->setText(QString(HTML_5_STATUS_STRING).arg(tr("Compressing log..... Please wait.")));

	//-> zip log()
	QFileInfo file_info(strSource);
	QStringList lst;
	lst.append(strSource);
	QString strFile = QDir::tempPath()+"/"+file_info.completeBaseName()+".zip";
	if(!ZipCompression::ZipFiles(strFile,lst))
	{
		ui.labelStatus->setText("");
		QApplication::restoreOverrideCursor();
		QMessageBox::critical(this,tr("Error occurred!"),QString("Error occurred while zipping file: %1!").arg(strSource));
		return;
	}
	ui.labelStatus->setText("");
	QApplication::restoreOverrideCursor();

	//ask user to save:
	//open file dialog:
	QString strStartDir=QCoreApplication::applicationDirPath();

	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;

	QString strFileName="appserver.zip";
	QString strTarget = QFileDialog::getSaveFileName(
		NULL,
		QObject::tr("Save Log As"),
		strStartDir,"*.zip",&strFileName);

	if (!strFile.isEmpty())
	{

		if(QFile::copy(strFile,strTarget))
		{
			QMessageBox::information(this,tr("Success!"),QString("Log successfully saved!"));
		}
		else
		{
			QMessageBox::critical(this,tr("Error occurred!"),QString("Failed to save log"));
		}
	}



}
