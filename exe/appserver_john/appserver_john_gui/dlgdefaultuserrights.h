#ifndef DLGDEFAULTUSERRIGHTS_H
#define DLGDEFAULTUSERRIGHTS_H

#include <QDialog>
#include "generatedfiles/ui_dlgdefaultuserrights.h"

class DlgDefaultUserRights : public QDialog
{
	Q_OBJECT

public:
	DlgDefaultUserRights(QWidget *parent = 0);
	~DlgDefaultUserRights();

	DbRecordSet GetDefaultUserRight(){return m_rowData;};

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();

private:
	Ui::DlgDefaultUserRightsClass ui;
	DbRecordSet m_rowData;
};

#endif // DLGDEFAULTUSERRIGHTS_H
