#include "dlgcatalogs.h"
#include "settingsmanager.h"
#include "pichelper.h"

DlgCatalogs::DlgCatalogs(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	setWindowTitle("Shared Folders");
	SettingsManager::DefineFolders(m_lstCatalogs);

	//set data source for table, enable drop
	DbRecordSet columns;
	UniversalTableWidgetEx::AddColumnToSetup(columns,"ALIAS",tr("Shared Folder"),200,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	ui.tableWidget->Initialize(&m_lstCatalogs,&columns);
	m_lstCatalogs.sort("ALIAS");
	ui.tableWidget->RefreshDisplay();
	ui.tableWidget->setStyleSheet(PicHelper::GetTableStyle());

	this->setStyleSheet(PicHelper::GetDialogStyle());

	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnCancel->setStyleSheet(strStyleBtn);
	ui.btnOK->setStyleSheet(strStyleBtn);

	connect(ui.tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
}

DlgCatalogs::~DlgCatalogs()
{

}

void DlgCatalogs::on_btnCancel_clicked()
{
	done(0);
}

void DlgCatalogs::on_btnOK_clicked()
{
	done(1);
}

void DlgCatalogs::OnCellDoubleClicked(int,int)
{
	done(1);
}

