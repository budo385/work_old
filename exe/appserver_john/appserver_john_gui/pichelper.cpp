#include "pichelper.h"
#include <QMessageBox>
#include <QImageReader>
#include <QFileDialog>
#include <QFileInfo>
#include <QBuffer>
#include <QProcess>
#include <QApplication>
#include <QLabel>
#include "filecruncher.h"
#include "common_constants.h"

extern QString g_strLastDir_FileOpen;


bool PicHelper::CreateThumbFromFile(QString strFile, QByteArray &bufPicture,QPixmap &bufPixMap,int nWidth, int nHeight)
{
	QString strFormat;

	//load picture into ByteArray
	if(!strFile.isEmpty())
	{
		QFileInfo picFile(strFile);
		strFormat=picFile.completeSuffix();

		bool bOK=bufPixMap.load(strFile);
		if(!bOK)
		{
			QMessageBox::critical(NULL,QObject::tr("Error"),QObject::tr("Picture can not be loaded!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return false;
		}

		bufPixMap=bufPixMap.scaled(nWidth,nHeight,Qt::KeepAspectRatio,Qt::SmoothTransformation);
		QBuffer buffer(&bufPicture);
		buffer.open(QIODevice::WriteOnly);		
		bOK=bufPixMap.save(&buffer, "jpg"); //bufPixMap.save(&buffer, strFormat.toLatin1());	
		//if (!bOK)
		//	bufPixMap.save(&buffer, "jpg");	
		return true;
	}
	else
		return false;

}


bool PicHelper::LoadPicture(QByteArray &bufPicture,QByteArray &bufPictureLarge,QPixmap &bufPixMap,QPixmap &bufPixMapLarge,QString strCurrentPath)
{
	QString strFilter ="Images (";
	/*
	QString strFormat;

	//get list of supported images:
	QList<QByteArray> lstFormats = QImageReader::supportedImageFormats();
	QString strFilter ="Images (";
	for(int i=0;i<lstFormats.size();++i)
	{
		//if(i!=0)strFilter+=" ";
		strFilter+=" *."+lstFormats.at(i);
	}
	*/
	
	QStringList lstNameFilters=FileCruncher::CreateFiltersFromExtensionString(FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC));
	for(int i=0;i<lstNameFilters.size();++i)
	{
		strFilter+=" "+lstNameFilters.at(i);
	}
	strFilter+=")";


	//if start dir empty then application path:
	QString strStartDir= QDir::currentPath(); 
	if (!strCurrentPath.isEmpty())
	{
		strStartDir=strCurrentPath;
	}
	else
	{
		//open file dialog:
		if (g_strLastDir_FileOpen.isEmpty())
			g_strLastDir_FileOpen=strStartDir;
		else
			strStartDir=g_strLastDir_FileOpen;
	}


	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		QObject::tr("Select Picture"),
		strStartDir,
		strFilter);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}


	QFileInfo info(strFile);
	if(FileCruncher::IsRAWPicture(info.completeSuffix()))
	{
		bool bOK=LoadThumbFromRaw(bufPicture,bufPixMap,strFile,THUMB_CATALOG_SMALL_WIDTH,THUMB_CATALOG_SMALL_HEIGHT);
		if (bOK)
			return LoadThumbFromRaw(bufPictureLarge,bufPixMapLarge,strFile,THUMB_CATALOG_LARGE_WIDTH,THUMB_CATALOG_LARGE_HEIGHT);
		else
			return false;
	}
	else
	{
		bool bOK=CreateThumbFromFile(strFile,bufPicture,bufPixMap,THUMB_CATALOG_SMALL_WIDTH,THUMB_CATALOG_SMALL_HEIGHT);
		if (bOK)
			return CreateThumbFromFile(strFile,bufPictureLarge,bufPixMapLarge,THUMB_CATALOG_LARGE_WIDTH,THUMB_CATALOG_LARGE_HEIGHT);
		else
			return false;
	}

}



bool PicHelper::LoadThumbFromRaw(QByteArray &bufPicture,QPixmap &bufPixMap, QString strIn,int nWidth, int nHeight)
{
	//qDebug()<<"Creating thumb from "<<strIn;

	QString strErr;
	QProcess dcraw;
	QStringList lstArgs;
	lstArgs<<"-e";
	lstArgs<<strIn;
	dcraw.start("RawLib",lstArgs);
	if (!dcraw.waitForFinished(-1)) //wait forever
	{	
		strErr="Failed to execute RawLib";
		return false;
	}
	int nExitCode=dcraw.exitCode();
	QByteArray errContent=dcraw.readAllStandardError();
	if (errContent.size()!=0)
	{
		strErr="Thumb from RAW format process returned errors: "+QString(errContent.left(4095));
		return false;
	}

	//if succeed then try to find name+".thumbxx" +jpg??:
	QFileInfo infoIn(strIn);
	QString strThumbName=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".thumb.jpg";
	QString strThumbName2=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".thumb.ppm";
	if (QFile::exists(strThumbName))
	{
		//qDebug()<<"thumb extracted "<<strThumbName;

		bool bOK=CreateThumbFromFile(strThumbName,bufPicture,bufPixMap,nWidth,nHeight);
		QFile::remove(strThumbName);
		QFile::remove(strThumbName2);
		return bOK;
	}
	else if (QFile::exists(strThumbName2))
	{
		//qDebug()<<"thumb extracted "<<strThumbName2;

		bool bOK=CreateThumbFromFile(strThumbName2,bufPicture,bufPixMap,nWidth,nHeight);
		QFile::remove(strThumbName);
		QFile::remove(strThumbName2);
		return bOK;
	}

	//else generate tiff:
	lstArgs.clear();
	lstArgs<<"-T";
	lstArgs<<strIn;
	dcraw.start("RawLib",lstArgs);
	if (!dcraw.waitForFinished(-1)) //wait forever
	{	
		strErr="Failed to execute RawLib for TIFF conversion";
		return false;
	}
	nExitCode=dcraw.exitCode();
	errContent=dcraw.readAllStandardError();
	if (errContent.size()!=0)
	{
		strErr="Thumb from RAW format (TIFF conversion) process returned errors: "+QString(errContent.left(4095));
		return false;
	}

	//if succeed then try to find name+tiff?:
	QString strTiffName=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".tiff";
	if (QFile::exists(strTiffName))
	{
		//qDebug()<<"tiff extracted "<<strTiffName;

		bool bOK=CreateThumbFromFile(strTiffName,bufPicture,bufPixMap,nWidth,nHeight);
		QFile::remove(strThumbName);
		return bOK;
	}

	strErr="Failed to create thumb from picture: "+infoIn.completeBaseName();
	return false;
}



//for windows set vars
#ifdef _WIN32
	#include <shlobj.h>
	#define MAX_VALUE_LENGTH 16383
#endif


//empty if failes..
QString PicHelper::GetMyPicturesFolder()
{

#if defined _WIN32
	//get MY DOCUMENTS: http://technet.microsoft.com/en-us/library/cc749104%28WS.10%29.aspx
	QString strAppDir;
	TCHAR buf[MAX_VALUE_LENGTH];
	if(SHGetSpecialFolderPath(NULL,buf,CSIDL_MYPICTURES,true))
		strAppDir=QString::fromUtf16((ushort*)buf);//, wsstrlen(buf));

	QDir testDir(strAppDir);
	if (!testDir.exists())
		return "";
	else
		return testDir.path();
#else
	return "";
#endif
}

QString PicHelper::GetMyDocumentsFolder()
{

#if defined _WIN32
	//get MY DOCUMENTS: http://technet.microsoft.com/en-us/library/cc749104%28WS.10%29.aspx
	QString strAppDir;
	TCHAR buf[MAX_VALUE_LENGTH];
	if(SHGetSpecialFolderPath(NULL,buf,CSIDL_MYDOCUMENTS ,true))
		strAppDir=QString::fromUtf16((ushort*)buf);

	QDir testDir(strAppDir);
	if (!testDir.exists())
		return "";
	else
		return testDir.path();
#else
	return "";
#endif

}

QString	PicHelper::GetDialogStyle()
{
	return "QDialog {background-image:url(:Resources/PooltableBlue256.png); background-repeat: repeat-xy; background-color: rgb(255, 0, 0)} QLabel {color:white}  QGroupBox::title {color:white}";
}

//sets bkg image
//strFontColor = white, red, green, black, yellow, or rgb(xxx,xxx,xxx) as string
QString PicHelper::GetStyledButtonColorBkg(QString strBkgImage,QString strFontColor, int nFontSize,QString strHoverImage)
{
	QString strFontStyle="font-weight:bold; font-size:"+QString::number(nFontSize)+"pt; ";
	QString strLabelFont="QLabel {color:"+strFontColor+"; "+strFontStyle+"}";
	if (!strHoverImage.isEmpty())
		 strLabelFont+=" QPushButton:hover {  border-top-width: 8px; border-right-width: 8px; border-bottom-width: 8px; border-left-width: 8px; color: yellow;border-image:url("+strHoverImage+") 4px 4px 4px 4px stretch stretch}";

	return strLabelFont +" QPushButton { "+strFontStyle+ "border-top-width: 8px; border-right-width: 8px; border-bottom-width: 8px; border-left-width: 8px; color: "+strFontColor+";border-image:url("+strBkgImage+") 4px 4px 4px 4px stretch stretch }";

	
}


QString PicHelper::GetTableStyle()
{
	QString strTableStyle;
	//strTableStyle= "QTableView {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
	strTableStyle= "QTableView {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(110,134,149), stop:1 rgb(24,61,86));}";
	strTableStyle+="QTableView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
	strTableStyle+="QTableView {gridline-color: white; color: white;}";
	strTableStyle+="QComboBox  {color: white; background:transparent;} QLineEdit {color: black;}";
	strTableStyle+="QTableView:disabled {background: rgb(70,82,106)}";
	strTableStyle+="QHeaderView:section {color: white; background:rgb(70,82,106);} QTableView QTableCornerButton::section {background:rgb(70,82,106);border: 1px}  QHeaderView {background:transparent;}";

	return strTableStyle;
}
QString	PicHelper::GetTreeStyle()
{
	QString strTableStyle;
	strTableStyle= "QTreeView {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(110,134,149), stop:1 rgb(24,61,86));}";
	strTableStyle+="QTreeView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
	strTableStyle+="QTreeView {gridline-color: white; color: white;}";
	return strTableStyle;

}

QString PicHelper::GetBorderFrameStyle()
{
	return "QTableView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
}

QGraphicsDropShadowEffect * PicHelper::GetShadowEffect()
{
	QGraphicsDropShadowEffect * effect = new QGraphicsDropShadowEffect();
	effect->setXOffset(1);
	effect->setYOffset(1);
	effect->setColor(Qt::black);
	effect->setBlurRadius(3.0);

	return effect;
}


void PicHelper::SetPlatformDepedentFontSize()
{
#ifdef __APPLE__

	QWidgetList lstWidgets = qApp->allWidgets();

	int nSize=lstWidgets.size();
	for (int i=0; i<nSize; i++)
	{
		QLabel *pLabel=dynamic_cast<QLabel*>(lstWidgets.at(i));
		if (pLabel)
		{
			QString strText=pLabel->text();

			//search: "font-size:11pt;"

			int nIdx=strText.indexOf("font-size:");
			while(nIdx>0)
			{
				int nIdx2=strText.indexOf("pt",nIdx);
				if (nIdx2>0)
				{
					int nLength=nIdx2-nIdx-10;

					int nPointSize=strText.mid(nIdx+10,nIdx2-nIdx-10).trimmed().toInt();
					if (nPointSize>0)
					{
						nPointSize+=3;
						strText.replace(nIdx+10,nLength,QString::number(nPointSize));
						pLabel->setText(strText);
					}
				}
				nIdx=strText.indexOf("font-size:",nIdx+1);
			}
		}
	}

#endif

}