#include "clicklabelwidget.h"
#include <QDesktopServices>
#include <QUrl>

ClickLabelWidget::ClickLabelWidget(QWidget *parent)
	: QLabel(parent)
{
	m_bPressed=false;
	setCursor(Qt::PointingHandCursor);
	connect(this,SIGNAL(clicked()),this,SLOT(OnClick()));
}



//pass event to parent:
void ClickLabelWidget::mousePressEvent ( QMouseEvent * event )
{
	m_bPressed=true;
	QLabel::mousePressEvent(event);
}


//pass event to parent:
void ClickLabelWidget::mouseReleaseEvent(QMouseEvent * event )
{
	QLabel::mouseReleaseEvent(event);

	if (m_bPressed)
	{
		emit clicked();
	}
}



void ClickLabelWidget::OnClick()
{
	//QString strAccess = "http://everxconnect.com:11000/invite/"+m_strAppServerID;
	//QDesktopServices::openUrl(strAccess);

	QDesktopServices::openUrl(m_strURL);
}
