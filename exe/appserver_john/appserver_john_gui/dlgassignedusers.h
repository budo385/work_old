#ifndef DLGASSIGNEDUSERS_H
#define DLGASSIGNEDUSERS_H

#include <QDialog>
#include "generatedfiles/ui_dlgassignedusers.h"
#include "common/common/dbrecordset.h"
#include "settingsmanager.h"

class DlgAssignedUsers : public QDialog
{
	Q_OBJECT

public:
	DlgAssignedUsers(QWidget *parent = 0);
	~DlgAssignedUsers();

	void SetData(DbRecordSet &lstUsers, SettingsManager *pSettings, DbRecordSet &lstUserRights, QString strCurrentFolder);
	void GetData(DbRecordSet &lstUsers, DbRecordSet &lstUserRights);

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnAddUserRight_clicked();
	void on_btnDeleteUserRight_clicked();

private:
	Ui::DlgAssignedUsersClass ui;

	DbRecordSet m_lstUserRights;
	DbRecordSet m_lstUserRightsForFolder;
	DbRecordSet m_lstUsers;
	QString m_strCurrentFolder;
	SettingsManager *m_pSettings;

};

#endif // DLGASSIGNEDUSERS_H
