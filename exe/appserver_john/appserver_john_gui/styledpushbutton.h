#ifndef STYLEDPUSHBUTTON_H
#define STYLEDPUSHBUTTON_H

#include <QPushButton>
#include <QHBoxLayout>
#include <QLabel>
#include <QString>

class StyledPushButton : public QWidget
{
	Q_OBJECT

public:
	StyledPushButton(QWidget *parent,QString strIcon,QString strIconHover, QString strStyle,QString strTextHtml="", int nIdent=5,int nSpacing=10, Qt::CursorShape hovercursor = Qt::PointingHandCursor);
	StyledPushButton(QString strMovieIcon,QString strMovieFormat,bool bPlayMode, /*0 on press, 1- endless */QWidget *parent,QString strStyle,QString strTextHtml="", int nIdent=5,int nSpacing=10, Qt::CursorShape hovercursor = Qt::PointingHandCursor);
	~StyledPushButton();

	void setCheckedStyle(QString strStyle){m_strCheckedStyle=strStyle;};
	void setPressedIcon(QString strIcon){m_strPressedIcon=strIcon;};
	void setChecked(bool bChecked);
	void setIcon(QString strIcon);
	void setIcon(QPixmap pixIcon);
	void setText(QString strText);
	void EnableMoviePlay(bool bEnable){m_bEnableMovie=bEnable;}
	void StartMovie(bool bStart);
	QHBoxLayout* GetLayout();

signals:
	void clicked();
	void mouse_over();
	void mouse_leave();

protected:
	void enterEvent ( QEvent * event ); 
	void leaveEvent ( QEvent * event ); 
	void mousePressEvent ( QMouseEvent * event ); 
	void mouseReleaseEvent(QMouseEvent * event );
	void showEvent ( QShowEvent * event );
	void hideEvent ( QHideEvent * event );

private slots:
	void onFrameChanged(int nFrame);

private:
	QString m_strPressedIcon;
	QString m_strCheckedStyle;
	QString m_strIcon;
	QString m_strIconHover;
	QLabel *m_labelIcon;
	QLabel *m_labelText;
	Qt::CursorShape m_hovercursor;
	bool m_bHoverIconEnabled;
	bool m_bPlayMode;
	bool m_bUseMovieIcon;
	bool m_bEnableMovie;
	QString m_strDefaultStyle;
	QHBoxLayout *m_pLayout;
	bool m_bPressed;
};




#endif // STYLEDPUSHBUTTON_H
