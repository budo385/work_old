#ifndef DLGFOLDERS_H
#define DLGFOLDERS_H

#include <QDialog>
#include "ui_dlgfolders.h"
#include <QTreeWidgetItem>



class DlgFolders : public QDialog
{
	Q_OBJECT

public:
	DlgFolders(QWidget *parent = 0,QString strTitle="Select Directories",bool bIPhotoMode=false);
	~DlgFolders();

	bool GetIPhotoAlbums();
	void SetInitialFolder(QString strPath);
	void GetSelectedFolders(QStringList &lstFolders,QList<QString> &lstAlbumIDs /* only for iPhoto */);

private slots:
	void		OnItemChanged(QTreeWidgetItem *item, int column);
	void		OnItemExpanded(QTreeWidgetItem *item);
	void		OnItemCollapsed( QTreeWidgetItem * item );
	void		on_btnOK_clicked(){done(1);};
	void		on_btnCancel_clicked(){done(0);}

private:
	void FillSubDirs(QString strDirPath,QTreeWidgetItem *item);
	bool GetDriveNamesAndTypes(QStringList &lstDrives, QStringList &lstNames,QList<QStyle::StandardPixmap> &lstTypes);
	QTreeWidgetItem* GetNodeByFolderName(QTreeWidgetItem* item,QString strFolderName);
	QString ReplaceUNCPath(QString strPath);

private:
	Ui::DlgFoldersClass ui;
	QStringList m_lstFolders;
	QList<QString>	m_lstAlbumIDs;
	QIcon m_icon_DirOpen;
	QIcon m_icon_DirClose;
	bool  m_bIPhotoMode;

	QStringList m_lstDriveNames;
	QStringList m_lstDriveLetters;
	QStringList m_lstPaths;
	QList<int> m_lstTypes;
};

#endif // DLGFOLDERS_H
