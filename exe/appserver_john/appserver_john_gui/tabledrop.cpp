#include "tabledrop.h"
#include <QTableWidgetItem>
#include <QDebug>

TableDrop::TableDrop(QWidget *parent)
	: QTableWidget(parent)
{
	setAcceptDrops(true);
	setColumnCount(1);
	setColumnWidth(0,300);
	clear();
	setRowCount(0);
}

TableDrop::~TableDrop()
{

}


void TableDrop::dragEnterEvent(QDragEnterEvent *event)
{
	event->acceptProposedAction();
}

void TableDrop::dragMoveEvent(QDragMoveEvent *event)
{
	event->acceptProposedAction();
}

void TableDrop::dropEvent(QDropEvent *event)
{
	const QMimeData *mimeData = event->mimeData();

	if (mimeData->hasUrls())
	{
		QList<QUrl> lst =mimeData->urls();
		int nSize=lst.size();
		for (int i=0;i<nSize;i++)
		{
			QTableWidgetItem *newItem = new QTableWidgetItem;
			newItem->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
			qDebug()<<lst.at(i).toString();
			newItem->setData(Qt::DisplayRole,lst.at(i).toString());
			int nRow=rowCount();
			setRowCount(nRow+1);
			setItem(nRow, 0, newItem);
			
		}
	}

	event->acceptProposedAction();
}

void TableDrop::dragLeaveEvent(QDragLeaveEvent *event)
{
	event->accept();
}


