#include "table_userright.h"

Table_UserRight::Table_UserRight(QWidget *parent)
	: UniversalTableWidgetEx(parent)
{

}

Table_UserRight::~Table_UserRight()
{

}

void Table_UserRight::Initialize(DbRecordSet *plstData,bool bShowCatalogCol)
{
	//set data source for table, enable drop
	DbRecordSet columns;
	if (bShowCatalogCol)
		AddColumnToSetup(columns,"ALIAS",tr("Shared Folder"),120,false,"" ,COL_TYPE_TEXT,"");
	else
		AddColumnToSetup(columns,"USERNAME",tr("User Name"),120,false,"" ,COL_TYPE_TEXT,"");

	AddColumnToSetup(columns,"CAN_DOWNLOAD",tr("Download"),100,true, "",COL_TYPE_CHECKBOX);
	AddColumnToSetup(columns,"CAN_UPLOAD",tr("Upload"),100,true, "",COL_TYPE_CHECKBOX);
	AddColumnToSetup(columns,"CAN_DELETE",tr("Delete"),100,true, "",COL_TYPE_CHECKBOX);
	AddColumnToSetup(columns,"CAN_SEND_INVITE",tr("Send Invitations"),100,true, "",COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::Initialize(plstData,&columns);
	//EnableDrop(true);
	//CreateContextMenu();
	//connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
}
