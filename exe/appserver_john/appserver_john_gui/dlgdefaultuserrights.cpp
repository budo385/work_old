#include "dlgdefaultuserrights.h"
#include "settingsmanager.h"
#include "pichelper.h"

DlgDefaultUserRights::DlgDefaultUserRights(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	setWindowTitle("Define User Rights");
	ui.tableWidget->setStyleSheet(PicHelper::GetTableStyle());

	SettingsManager::CreateDefaultUserRights(m_rowData);

	//set data source for table, enable drop
	DbRecordSet columns;
	//ui.tableWidget->AddColumnToSetup(columns,"USERNAME",tr("User Name"),120,false,"" ,UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	ui.tableWidget->AddColumnToSetup(columns,"CAN_DOWNLOAD",tr("Download"),100,true, "",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	ui.tableWidget->AddColumnToSetup(columns,"CAN_UPLOAD",tr("Upload"),100,true, "",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	ui.tableWidget->AddColumnToSetup(columns,"CAN_DELETE",tr("Delete"),100,true, "",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	ui.tableWidget->AddColumnToSetup(columns,"CAN_SEND_INVITE",tr("Send Invitations"),100,true, "",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	ui.tableWidget->Initialize(&m_rowData,&columns,true);
	ui.tableWidget->SetEditMode(true);
	
	this->setStyleSheet(PicHelper::GetDialogStyle());

	QString strStyleBtn=PicHelper::GetStyledButtonColorBkg(":Resources/Blue_Button.png","white",10,":Resources/Blue_Button_Mouseover.png");
	ui.btnCancel->setStyleSheet(strStyleBtn);
	ui.btnOK->setStyleSheet(strStyleBtn);

}

DlgDefaultUserRights::~DlgDefaultUserRights()
{

}

void DlgDefaultUserRights::on_btnCancel_clicked()
{
	done(0);
}

void DlgDefaultUserRights::on_btnOK_clicked()
{
	done(1);
}