#ifndef CLICKLABELWIDGET_H
#define CLICKLABELWIDGET_H

#include <QLabel>
#include <QWidget>

class ClickLabelWidget : public QLabel
{
	Q_OBJECT

public:
	ClickLabelWidget(QWidget *parent=0);
	~ClickLabelWidget(){};

	void SetURL(QString strURL){m_strURL=strURL;};

protected:
	void mousePressEvent ( QMouseEvent * event );
	void mouseReleaseEvent(QMouseEvent * event );

signals:
	void clicked();

private slots:
	void OnClick();

private:
	QString m_strURL;
	bool m_bPressed;


};

#endif // CLICKLABELWIDGET_H
