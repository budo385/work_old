#ifndef TABLE_USERRIGHT_H
#define TABLE_USERRIGHT_H

#include "universaltablewidgetex.h"

class Table_UserRight : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_UserRight(QWidget *parent);
	~Table_UserRight();

	void Initialize(DbRecordSet *plstData,bool bShowCatalogCol=true);

private:
	
};

#endif // TABLE_USERRIGHT_H
