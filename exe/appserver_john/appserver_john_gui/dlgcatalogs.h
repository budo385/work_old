#ifndef DLGCATALOGS_H
#define DLGCATALOGS_H

#include <QDialog>
#include "generatedfiles/ui_dlgcatalogs.h"
#include "common/common/dbrecordset.h"

class DlgCatalogs : public QDialog
{
	Q_OBJECT

public:
	DlgCatalogs(QWidget *parent = 0);
	~DlgCatalogs();

	void		SetData(DbRecordSet &data){m_lstCatalogs=data; m_lstCatalogs.sort("ALIAS"); ui.tableWidget->RefreshDisplay();};
	DbRecordSet GetData(){m_lstCatalogs.deleteUnSelectedRows(); return m_lstCatalogs;};


private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void OnCellDoubleClicked(int,int);

private:
	Ui::DlgCatalogsClass ui;
	DbRecordSet m_lstCatalogs;
};

#endif // DLGCATALOGS_H
