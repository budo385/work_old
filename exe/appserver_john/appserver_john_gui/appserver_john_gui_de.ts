<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>DlgAssignedUsersClass</name>
    <message>
        <location filename="dlgassignedusers.ui" line="14"/>
        <source>DlgAssignedUsers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgassignedusers.ui" line="20"/>
        <source>Catalog User Rights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgassignedusers.ui" line="69"/>
        <source>Assign User To Catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgassignedusers.ui" line="82"/>
        <source>Remove User Assignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgassignedusers.ui" line="128"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgassignedusers.ui" line="135"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgCatalogs</name>
    <message>
        <location filename="dlgcatalogs.cpp" line="15"/>
        <source>Catalog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgCatalogsClass</name>
    <message>
        <location filename="dlgcatalogs.ui" line="14"/>
        <source>DlgCatalogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgcatalogs.ui" line="44"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgcatalogs.ui" line="51"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgDefaultUserRights</name>
    <message>
        <location filename="dlgdefaultuserrights.cpp" line="17"/>
        <source>Can Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgdefaultuserrights.cpp" line="18"/>
        <source>Can Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgdefaultuserrights.cpp" line="19"/>
        <source>Can Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgdefaultuserrights.cpp" line="20"/>
        <source>Can Send Invite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgDefaultUserRightsClass</name>
    <message>
        <location filename="dlgdefaultuserrights.ui" line="14"/>
        <source>DlgDefaultUserRights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgdefaultuserrights.ui" line="44"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgdefaultuserrights.ui" line="51"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgEmailSettingsClass</name>
    <message>
        <location filename="dlgemailsettings.ui" line="14"/>
        <source>DlgEmailSettings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgemailsettings.ui" line="35"/>
        <source>SMTP server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgemailsettings.ui" line="52"/>
        <source>Admin Email Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgemailsettings.ui" line="69"/>
        <source>Authentication (if needed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgemailsettings.ui" line="75"/>
        <source>SMTP User:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgemailsettings.ui" line="92"/>
        <source>SMTP Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgemailsettings.ui" line="139"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgemailsettings.ui" line="146"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgUserChange</name>
    <message>
        <location filename="dlguserchange.cpp" line="72"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.cpp" line="54"/>
        <source>Password don&apos;t match!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.cpp" line="60"/>
        <source>Username must be set!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.cpp" line="67"/>
        <source>Username can not contain spaces!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.cpp" line="72"/>
        <source>Password can not contain spaces!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgUserChangeClass</name>
    <message>
        <location filename="dlguserchange.ui" line="14"/>
        <source>DlgUserChange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="41"/>
        <source>Username:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="64"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="87"/>
        <source>Confirm Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="122"/>
        <source>First Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="139"/>
        <source>Last Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="156"/>
        <source>Company:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="173"/>
        <source>SMS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="190"/>
        <source>E-mail:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="231"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlguserchange.ui" line="238"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgUsers</name>
    <message>
        <location filename="dlgusers.cpp" line="20"/>
        <source>User Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgusers.cpp" line="21"/>
        <source>Last Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgusers.cpp" line="22"/>
        <source>First Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgusers.cpp" line="103"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgusers.cpp" line="103"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgUsersClass</name>
    <message>
        <location filename="dlgusers.ui" line="14"/>
        <source>DlgUsers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgusers.ui" line="32"/>
        <source>Create New User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgusers.ui" line="45"/>
        <source>Delete User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dlgusers.ui" line="58"/>
        <source>Change User Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="pichelper.cpp" line="27"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pichelper.cpp" line="27"/>
        <source>Picture can not be loaded!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pichelper.cpp" line="87"/>
        <source>Select Picture</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table_Dirs</name>
    <message>
        <location filename="table_dirs.cpp" line="28"/>
        <source>Catalog Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table_dirs.cpp" line="29"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table_dirs.cpp" line="30"/>
        <source>File Types</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table_UserRight</name>
    <message>
        <location filename="table_userright.cpp" line="19"/>
        <source>Catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table_userright.cpp" line="21"/>
        <source>User Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table_userright.cpp" line="23"/>
        <source>Can Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table_userright.cpp" line="24"/>
        <source>Can Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table_userright.cpp" line="25"/>
        <source>Can Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="table_userright.cpp" line="26"/>
        <source>Can Send Invite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UniversalTableWidgetEx</name>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1065"/>
        <source>&amp;Add New Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1066"/>
        <source>Ins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1075"/>
        <source>&amp;Delete Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1076"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1085"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1086"/>
        <source>CTRL+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1094"/>
        <source>Duplicate Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1095"/>
        <source>CTRL+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1105"/>
        <source>&amp;Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1106"/>
        <source>CTRL+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1113"/>
        <source>D&amp;eSelect All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="universaltablewidgetex.cpp" line="1114"/>
        <source>CTRL+E</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>appserver_james_gui</name>
    <message>
        <location filename="appserver_james_gui.cpp" line="80"/>
        <source>Copy Server ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="82"/>
        <source>Copy Public IP Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="84"/>
        <source>Copy Local IP Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="86"/>
        <source>Copy Connection String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="131"/>
        <source>Catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="132"/>
        <source>Can Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="133"/>
        <source>Can Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="134"/>
        <source>Can Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="135"/>
        <source>Can Send Invite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="150"/>
        <source>User Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="151"/>
        <source>Last Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="152"/>
        <source>First Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="153"/>
        <source>Company</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1196"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="226"/>
        <source>Service Descriptor can not contain spaces nor special characters (national)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="232"/>
        <source>Port is out range (1-65535)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="304"/>
        <source>Warning: server only accessible through WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="318"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="427"/>
        <source>Error while trying to notify server about changes! Try again. If problem still exists, please restart server!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="437"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="437"/>
        <source>Settings successfully saved!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1279"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1279"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1299"/>
        <source>Published</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1299"/>
        <source>Catalogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1300"/>
        <source>Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1301"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1302"/>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1362"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1534"/>
        <source>START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1572"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1535"/>
        <source>Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1571"/>
        <source>STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1572"/>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1705"/>
        <source>: Catalog available in %1min %2sec...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1707"/>
        <source>: Catalog available in %1sec...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.cpp" line="1745"/>
        <source>Indexing %1 creating %2 thumbs (%3)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>appserver_james_guiClass</name>
    <message>
        <location filename="appserver_james_gui.ui" line="20"/>
        <source>appserver_james_gui</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="144"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:12pt; font-weight:600;&quot;&gt;STOP&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="256"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="438"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:14pt; font-weight:600;&quot;&gt;Server&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="495"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:14pt; font-weight:600; font-style:italic;&quot;&gt;Server Identification&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="509"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="519"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://link_1&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; text-decoration: underline; color:#0000ff;&quot;&gt;How To Connect...&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="563"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:14pt; font-weight:600; font-style:italic;&quot;&gt;Master User&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1090"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="602"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="646"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic; color:#ffff00;&quot;&gt;Add directories to be published to the following list by &amp;quot;Publish Directory&amp;quot; or by&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic; color:#ffff00;&quot;&gt;dragging them from your file manager onto the list:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="669"/>
        <source>Publish Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="682"/>
        <source>Remove catalog from published catalog list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="685"/>
        <source>Unpublish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="698"/>
        <source>Users with Catalog Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="701"/>
        <source>Assigned Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="714"/>
        <source>Invite Users to view selected catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="717"/>
        <source>Invite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="737"/>
        <source>Catalog Picture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1733"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic; color:#ffff00;&quot;&gt;Create and manage users and define their rights for each published catalog. The master user &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic; color:#ffff00;&quot;&gt;&amp;quot;admin&amp;quot; can not be changed.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="869"/>
        <source>Create New User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="882"/>
        <source>Delete Selected Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="895"/>
        <source>Change User Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="924"/>
        <source>Catalog User Rights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="951"/>
        <source>Assign User To Catalog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="964"/>
        <source>Remove User Assignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1036"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic; color:#ffff00;&quot;&gt;Server configuration: Your server&apos;s name and picture are visible to the users.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic; color:#ffff00;&quot;&gt;To see your server on the road (3G), you have to configure &amp;quot;Port Forwarding&amp;quot;&lt;/span&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;    &lt;/span&gt;&lt;a href=&quot;http://link_1&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic; text-decoration: underline; color:#0000ff;&quot;&gt;Read more&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic; color:#ffff00;&quot;&gt;Enter your email settings if you want to send email invitations for published catalogs.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1076"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:14pt; font-weight:600; font-style:italic;&quot;&gt;Server&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:11pt; font-weight:600; font-style:italic;&quot;&gt;   &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-style:italic;&quot;&gt;(stop server to modify)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1123"/>
        <source>IP Port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1174"/>
        <source>Title Picture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1306"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:14pt; font-weight:600; font-style:italic;&quot;&gt;Server Data&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:11pt; font-weight:600; font-style:italic;&quot;&gt;  &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-style:italic;&quot;&gt;(stop server to modify)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1322"/>
        <source>ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1345"/>
        <source>Public IP address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1368"/>
        <source>Local IP address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1393"/>
        <source>Enable NAT-PMP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1400"/>
        <source>Enable UPnP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1407"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://link_1&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-weight:600; text-decoration: underline; color:#0000ff;&quot;&gt;How to Configure Router Port Forwarding&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://link_1&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:10pt; font-weight:600; text-decoration: underline; color:#0000ff;&quot;&gt;for Access From The Internet (3G)...&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1452"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Arial&apos;; font-size:14pt; font-weight:600; font-style:italic;&quot;&gt;Email Settings For Invitations&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1472"/>
        <source>SMTP Server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1489"/>
        <source>Admin Email Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1506"/>
        <source>Authentication (if needed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1512"/>
        <source>SMTP User:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1529"/>
        <source>SMTP Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1627"/>
        <source>System Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1646"/>
        <source>Connect Clients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1665"/>
        <source>Publishing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1684"/>
        <source>Access From Internet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1720"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600; font-style:italic; color:#ffff00;&quot;&gt;System Overview&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1835"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600; font-style:italic; color:#ffff00;&quot;&gt;Publishing&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="appserver_james_gui.ui" line="1854"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600; font-style:italic; color:#ffff00;&quot;&gt;Access From Internet&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
