#include "applicationserver.h"
#include "common/common/logger.h"
#include "trans/trans/tcphelper.h"
#include "servicehandler.h"
#include "common/common/threadid.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "db_core/db_core/dbconnsettings.h"
#include "settingsmanager.h"
#include "usersessionmanager.h"
#include "userstoragehttpserver_local.h"
//#include "thumbmanager.h"
#include "common/common/datahelper.h"
#include "filecruncher.h"
#include "common/common/config_version_john.h"


#define EMAIL_TASKER_INTERVAL	1*10 //10sec
#define GARBAGE_TASKER_INTERVAL	4*60 //4min
#define THUMB_TASKER_INTERVAL	5*60 //5min
#define CONNECTION_TEST_INTERVAL 4*60 //4min
//#define THUMB_SUBDIR			"thumb_cache"

//-----------------------------------------------
//	 SERVER GLOBALS:
//-----------------------------------------------

//SPECIALS:
Logger							g_Logger;
ApplicationServer				*g_AppServer=NULL;
ServiceHandler					*g_ServiceHandler=NULL;
SettingsManager					*g_SettingsManager=NULL;
UserSessionManager				*g_UserSessionManager=NULL;
UserStorageHTTPServer_Local		*g_UserStorage=NULL;
//ThumbManager					*g_ThumbManager=NULL;
QString							g_strHomePath="";
EmailSenderManager				*g_pEmailSenderManager=NULL;
//DbSqlManager					*g_DbManager=NULL;

/*!
	Constructor
*/
ApplicationServer::ApplicationServer(QObject *parent)
:QObject(parent) //ServerThread(parent),SokratesHttpServer(this)
{
	ClearPointers();
	m_nTimerID=-1;
	m_nTimerIDControlLoop=-1;
	m_bIsRunning=false;
	connect(this,SIGNAL(RestartDelayedThreadSafeSignal(int)),this,SLOT(RestartDelayedThreadSafeSlot(int)),Qt::QueuedConnection);
	connect(this,SIGNAL(StopThreadSafeSignal()),this,SLOT(StopThreadSafeSlot()),Qt::QueuedConnection);
	connect(this,SIGNAL(ChangeHTTPServerPortSignal(int)),this,SLOT(ChangeHTTPServerPortSlot(int)));
	
	ApplicationLogger::setApplicationLogger(&g_Logger);

	QCoreApplication::setOrganizationName("Helix Business Soft");
	QCoreApplication::setOrganizationDomain("everdocs.com");
	QCoreApplication::setApplicationName("EverDOCs Bridge");
}

void ApplicationServer::ClearPointers()
{
	m_RestHTTPServer=NULL;
	m_RestServiceRpcDispatcher=NULL;
	g_ServiceHandler=NULL;
	g_SettingsManager=NULL;
	g_UserSessionManager=NULL;
	g_UserStorage=NULL;
	g_pEmailSenderManager=NULL;
	m_HtmlHTTPServer = NULL;
	//g_ThumbManager = NULL;
	m_GbBkgTask=NULL;
	m_EmailBkgTask=NULL;
	m_GarbageCollector=NULL;
	m_strIniFilePath="";
	m_ConnectionHandler=NULL;
	m_ConnectionHandlerTask=NULL;
	//m_ThumbTask=NULL;
	m_HttpMultipartMessageServer=NULL;
	m_HttpMessageHandler=NULL;
}

/*!
	Destructor: kill all objects
*/
ApplicationServer::~ApplicationServer()
{
	if (IsServiceRunning())
		Stop();
}

void ApplicationServer::Restart(Status &pStatus)
{
	//QMutexLocker locker(&m_Mutex); //lock acces
	Stop();
	Start(pStatus);
	//exit if error:

}

void ApplicationServer::RestartDelayed(int nReStartAfterSec)
{
	QMutexLocker locker(&m_Mutex);							//lock acces
	emit RestartDelayedThreadSafeSignal(nReStartAfterSec);	//ensures that appserver thread executes this!
}

//ignore parameter: do it now!
void ApplicationServer::StopDelayed(int nStopAfterSec)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"stop delayed lock before");

	QMutexLocker locker(&m_Mutex);	//lock acces
	emit StopThreadSafeSignal();	//ensures that appserver thread executes this!
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"stop delayed");
}

void ApplicationServer::StopThreadSafeSlot()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"stop go");
	Stop();
	qApp->quit();
}


//ignore parameter: do it now!
void ApplicationServer::ChangeHTTPServerPortDelayed(int nNewPort)
{
	QMutexLocker locker(&m_Mutex);	
	m_HttpServerStarted=false;
	emit ChangeHTTPServerPortSignal(nNewPort);	
}

void ApplicationServer::ChangeHTTPServerPortSlot(int nNewPort)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Server changing port to %1").arg(nNewPort));
	
	Status err;
	SokratesHttpServer.StopListen();
	SokratesHttpServer.StartListen(err,nNewPort);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
		StopDelayed();
		qApp->quit();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Server successfully changed port to %1").arg(nNewPort));

	QMutexLocker locker(&m_Mutex);
	m_HttpServerStarted=true;
}



void ApplicationServer::SetLastError(int nErrorCode,QString strErrorTextArgs)
{
	QMutexLocker locker(&m_Mutex);
	Status err;
	err.setError(nErrorCode,strErrorTextArgs);

	m_INIFile.m_nLastErrorCode=err.getErrorCode();
	m_INIFile.m_strLastErrorText=err.getErrorText();
	m_INIFile.Save(m_strIniFilePath);
}

void ApplicationServer::RestartDelayedPriv(int nReStartAfterSec)
{
	//already in restart mode, ignore
	if (m_nTimerID!=-1)
	{
		return;
	}
	//this is fine opportunity to fire warning to all clients->
	m_nTimerID=startTimer(nReStartAfterSec*1000);
}

void ApplicationServer::Stop()
{
	QMutexLocker locker(&m_Mutex); //lock acces
	
	//if (!m_bIsRunning)	return;

	//Save meta data on exit
	//g_SettingsManager->SaveMetaData();
	if (g_SettingsManager)
		g_SettingsManager->SaveCntData();

	//stop server:
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopping Http server");
	SokratesHttpServer.StopServer();
	SokratesHttpServer.ClearRequestHandlerList();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Http server stopped");

	//save log & all rest:
	//m_INIFile.m_nLogLevel=g_Logger.GetLogLevel();
	//g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,1,"loglevel: "+QString::number(g_Logger.GetLogLevel()));
	//m_INIFile.m_nLogMaxSize=g_Logger.GetMaxLogSize();
	//m_INIFile.m_nLogBufferSize=g_Logger.GetMemoryBufferSize();
	m_INIFile.Save(m_strIniFilePath);

	//stop tasks:
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopping thumb manager");
	//if(m_ThumbTask!=NULL) m_ThumbTask->Stop();
	//if(m_ThumbTask!=NULL) delete m_ThumbTask;
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopped thumb manager");

	if(m_GbBkgTask!=NULL) m_GbBkgTask->Stop();
	if(m_GbBkgTask!=NULL) delete m_GbBkgTask;
	if(m_EmailBkgTask!=NULL) m_EmailBkgTask->Stop();
	if(m_EmailBkgTask!=NULL) delete m_EmailBkgTask;
	
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopped garbage collector");
	if(m_ConnectionHandlerTask!=NULL) m_ConnectionHandlerTask->Stop();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopped connection handler");
	if(m_ConnectionHandlerTask!=NULL) delete m_ConnectionHandlerTask;
	if(m_ConnectionHandler != NULL)	delete m_ConnectionHandler;
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Connection handler deleted");
	
	//destroy objects:
	//if(g_ThumbManager != NULL)				delete g_ThumbManager;
	if(m_GarbageCollector != NULL)			delete m_GarbageCollector;
	if(m_HtmlHTTPServer != NULL)			delete m_HtmlHTTPServer;
	if(m_RestHTTPServer!=NULL)				delete m_RestHTTPServer;
	if(m_HttpMultipartMessageServer!=NULL)	delete m_HttpMultipartMessageServer;
	if(m_HttpMessageHandler!=NULL)			delete m_HttpMessageHandler;
	if(m_RestServiceRpcDispatcher!=NULL)	delete m_RestServiceRpcDispatcher;
	if(g_ServiceHandler!=NULL)				delete g_ServiceHandler;
	if(g_SettingsManager!=NULL)				delete g_SettingsManager;
	if(g_UserSessionManager!=NULL)			delete g_UserSessionManager;
	if(g_UserStorage!=NULL)					delete g_UserStorage;
	if(g_pEmailSenderManager!=NULL)			delete g_pEmailSenderManager;


	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Server STOP: Stopped all services");
	

	m_bIsRunning=false;
	ClearPointers();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_STOP);
	g_Logger.FlushFileLog(); 
}



/*!
	Start App. server
*/
void ApplicationServer::Start(Status &pStatus)
{
	QMutexLocker locker(&m_Mutex); //lock acces
	if (m_bIsRunning)	return;

	HTTPServerConnectionSettings ServerSettings;
	//DbConnectionSettings DbConnSettings;

	//----------------------------------------------
	//LOAD INI files:
	//----------------------------------------------
	InitializeSettingsDirectory(g_strHomePath);
	if (g_strHomePath.isEmpty())
	{
		pStatus.setError(1,"Error while trying to initialize application directory!");
		return;
	}


	LoadINI(pStatus);
	if(!pStatus.IsOK()) return;

	m_INIFile.m_nLastErrorCode=0;
	m_INIFile.m_strLastErrorText="";
	m_INIFile.Save(m_strIniFilePath);

	//----------------------------------------------
	//Start logger 
	//----------------------------------------------
	if (m_INIFile.m_nLogLevel>LoggerAbstract::LOG_LEVEL_NONE)
	{
		g_Logger.EnableFileLogging(g_strHomePath+"/settings/appserver.log",m_INIFile.m_nLogBufferSize,m_INIFile.m_nLogMaxSize);
		g_Logger.EnableConsoleLogging();
	}
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);

	QTime time;
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Starting service");
	//time.start();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("InitializeSettingsDirectory: g_strHomePath: %1").arg(g_strHomePath));
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("InitializeSettingsDirectory: DataHelper::GetApplicationHomeDir(): %1").arg(DataHelper::GetApplicationHomeDir()));
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("InitializeSettingsDirectory: QDir::homePath();: %1").arg(QDir::homePath()));


	bool bBinDataExists=true;
	//QHash<QString,HtmlDocument> lstWebPages;
	//if(!InitWebPages(lstWebPages))
	//{
		bBinDataExists=false;
		//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,"Failed to load html pages");
		//SetLastError(StatusCodeSet::ERR_GENERAL,"Failed to load html pages");
		//return;
	//}


	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Starting service 1: After InitWebPages() (ms): "+QString::number(time.elapsed()));
	//time.restart();


	//----------------------------------------------
	//LOAD NET & DB files:
	//----------------------------------------------
	LoadNetSettings(pStatus,ServerSettings);
	if(!pStatus.IsOK()) return;
	//LoadDbSettings(pStatus,DbConnSettings);
	//if(!pStatus.IsOK()) return;

	//----------------------------------------------
	//init Database
	//----------------------------------------------
	//InitDatabaseConnection(pStatus,&g_DbManager,DbConnSettings);
	//if(!pStatus.IsOK())return;
	//g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_DATABASE_CONN_SUCESS,DbConnSettings.m_strDbName+";"+DbConnSettings.m_strDbHostName);


	//----------------------------------------------
	//Launch Business & Private sets/init pointers:
	//----------------------------------------------
	g_UserSessionManager= new UserSessionManager();
	g_ServiceHandler	= new ServiceHandler();
	g_SettingsManager	= new SettingsManager();
	//g_ThumbManager		= new ThumbManager();
	m_ConnectionHandler = new ConnectionHandler(&m_INIFile,m_strIniFilePath);
	m_GarbageCollector  = new GarbageCollector(&SokratesHttpServer);
	

	//reminder manager:
	EmailSenderManager *pEmailSender = new EmailSenderManager;
	g_pEmailSenderManager = pEmailSender;
	//m_EmailSenderTasker=new EmailSenderManagerExecutor(pEmailSender,this);
	//connect(pEmailSender,SIGNAL(GlobalPeriodChanged(int)),m_EmailSenderTasker,SLOT(OnGlobalPeriodChanged(int)));

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Starting service 2: After Init Global Objects() (ms): "+QString::number(time.elapsed()));
	//time.restart();


	//----------------------------------------------
	//init usersessionmanager:
	//----------------------------------------------
	QString strUserStorage=InitUserStorage();
	if (strUserStorage.isEmpty())
	{
		pStatus.setError(1,"Failed to init user storage handler!");
		return;
	}
	g_UserSessionManager->SetUserStorageDirectoryPath(strUserStorage);

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Starting service 3: After InitUserStorage() (ms): "+QString::number(time.elapsed()));
	//time.restart();

	//----------------------------------------------
	//init g_SettingsManager:
	//----------------------------------------------
	g_SettingsManager->Initialize(g_strHomePath);
	g_SettingsManager->LoadSettings();
	g_SettingsManager->LoadCntData();

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Admin user name: %1").arg(g_SettingsManager->GetAdminUserName()));

	//----------------------------------------------
	//Launch all server handlers:
	//----------------------------------------------
	//TcpHelper::ResolveWebAddressNetworkInterface(ServerSettings.m_strServerIP.toLower(),ServerSettings.m_nPort,ServerSettings.m_bUseSSL);

	m_RestServiceRpcDispatcher		= new RestRpcDispatcher();
	m_RestHTTPServer				= new RestHTTPServer_Local(m_RestServiceRpcDispatcher,m_INIFile.m_strWebDocumentsPath);
	m_HttpMessageHandler			= new HTTPMessageHandler;
	m_HttpMultipartMessageServer	= new HTTPMultipartServer(m_HttpMessageHandler);
	g_UserStorage					= new UserStorageHTTPServer_Local();

	
	//if (bBinDataExists)
	//	m_HtmlHTTPServer				= new HtmlHTTPServer(m_INIFile.m_strWebDocumentsPath,&lstWebPages);
	//else

#ifdef _DEBUG
	m_HtmlHTTPServer				= new HtmlHTTPServer(m_INIFile.m_strWebDocumentsPath);
#endif
	



	//g_UserStorage->SetThumbCacheDir(g_ThumbManager->GetThumbDirRoot());

	SokratesHttpServer.ClearRequestHandlerList();
	SokratesHttpServer.RegisterRequestHandler(m_RestHTTPServer);
	SokratesHttpServer.RegisterRequestHandler(g_UserStorage);
#ifdef _DEBUG
	m_HtmlHTTPServer->m_nDefaultSocketTimeOut=20;						//set timeout to 20min
	SokratesHttpServer.RegisterRequestHandler(m_HtmlHTTPServer);
#endif
	SokratesHttpServer.RegisterRequestHandler(m_HttpMultipartMessageServer);
	SokratesHttpServer.InitServer(ServerSettings,false);

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Starting service 4: After Init HTTP Server() (ms): "+QString::number(time.elapsed()));
	//time.restart();


	SokratesHttpServer.StartListen(pStatus);

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Starting service 4: After StartListen() HTTP Server (ms): "+QString::number(time.elapsed()));
	//time.restart();


	if (pStatus.IsOK())
	{
		QString appParams=ServerSettings.m_strServerIP+";"+QVariant(ServerSettings.m_nPort).toString()+";"+QVariant(ServerSettings.m_bUseSSL).toString();
		appParams+=";1;"+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString();
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SRV_SUCESS_START,appParams);
		m_bIsRunning=true;
	}
	else
		return;

	//----------------------------------------------
	//Start bkg tasks after init:
	//----------------------------------------------
	//m_ThumbTask=new ThumbManagerTaskExecutor(g_ThumbManager,this,THUMB_TASKER_INTERVAL*1000);
	//m_ThumbTask->Start(false,20000); //on start wait for 20sek

	m_EmailBkgTask=new BackgroundTaskExecutor(g_pEmailSenderManager,this,EMAIL_TASKER_INTERVAL*1000);
	m_EmailBkgTask->Start(false,1000); //on start wait for 1sek
	

	m_GbBkgTask=new BackgroundTaskExecutor(m_GarbageCollector,this,GARBAGE_TASKER_INTERVAL*1000);
	m_GbBkgTask->Start(false,60000); //on start wait for 1minute

	m_ConnectionHandlerTask=new BackgroundTaskExecutor(m_ConnectionHandler,this,CONNECTION_TEST_INTERVAL*1000);
	m_ConnectionHandlerTask->Start(false,2000); //on start register

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Starting service 5: End (ms): "+QString::number(time.elapsed()));
	//time.restart();


	//already in restart mode, ignore
	m_nTimerIDControlLoop=startTimer(60000); //every 1min check main thread

}



void ApplicationServer::SaveSettings()
{
	QMutexLocker locker(&m_Mutex);			//lock acces
	m_INIFile.Save(m_strIniFilePath);
}

void ApplicationServer::LoadINI(Status &pStatus)
{
	//load ini file & server settings:
	//------------------------------------------
	m_strIniFilePath= g_strHomePath+"/settings/app_server.ini";
	bool bOK=m_INIFile.Load(m_strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,"INI file is missing,corrupted or has invalid values!");
		return;
	}

	QFileInfo ssl(m_INIFile.m_strSSLCertificateDir+"/server.cert");
	if (!ssl.exists())
	{
		m_INIFile.m_strSSLCertificateDir=g_strHomePath+"/settings";
	}
}

void ApplicationServer::LoadNetSettings(Status &pStatus,HTTPServerConnectionSettings &pServerSettings)
{
	pServerSettings.m_strCertificateFile=m_INIFile.m_strSSLCertificateDir+"/server.cert";
	pServerSettings.m_strPrivateKeyFile=m_INIFile.m_strSSLCertificateDir+"/server.pkey";
	pServerSettings.m_strCACertificateFileBundle=m_INIFile.m_strSSLCertificateDir+"/server.ca-bundle";
	pServerSettings.m_nPort=m_INIFile.m_nAppServerPort;

	if(m_INIFile.m_strAppServerIPAddress.isEmpty())
		pServerSettings.m_strServerIP="0.0.0.0"; //listen on all ports
	else
		pServerSettings.m_strServerIP=m_INIFile.m_strAppServerIPAddress;
	pServerSettings.m_bUseSSL=m_INIFile.m_nSSLMode;

}


//overriden QObject timer method
void ApplicationServer::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		m_nTimerID=-1;
		Status err;
		Restart(err);
		if(!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
			Stop();
		}
	}

	if(event->timerId()==m_nTimerIDControlLoop)
	{
		//killTimer(m_nTimerIDControlLoop);
		//m_nTimerIDControlLoop=-1;
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("ApplicationServer: control loop CHECK, main thread: %1").arg(QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()));
	}


}




void ApplicationServer::RestartDelayedThreadSafeSlot(int nRestartAfter)
{
	RestartDelayedPriv(nRestartAfter);
}



QString ApplicationServer::InitUserStorage()
{
	QDir dir1(g_strHomePath);
	if (!dir1.exists("user_storage"))
		if(!dir1.mkdir("user_storage"))
			return "";

	return g_strHomePath+"/user_storage";
}




//if error, strHomePath=""
void ApplicationServer::InitializeSettingsDirectory(QString &strHomePath)
{
	
	//determine home path:
	QSettings settings(QSettings::SystemScope,QCoreApplication::organizationName(),QCoreApplication::applicationName());
	strHomePath = settings.value("SettingsPath","").toString();

	qDebug()<<"strHomePath from common location: "<<strHomePath;

	if (strHomePath.isEmpty())
	{
		strHomePath=DataHelper::GetApplicationHomeDir();
		if (strHomePath.isEmpty())
			return;

		qDebug()<<"strHomePath determined: "<<strHomePath;
	}

}

bool ApplicationServer::InitWebPages(QHash<QString,HtmlDocument> &lstWebPages)
{

	// LOAD WEB PAGES INTO "data.bin" if data.bin does not exist and path is set
#ifdef _DEBUG	
	if (!QFile::exists(QCoreApplication::applicationDirPath()+"/webdata.bin"))
	{
		QString strPath=QCoreApplication::applicationDirPath()+"/webservices/html";
		if (!m_INIFile.m_strWebDocumentsPath.isEmpty())
			strPath=QDir::cleanPath(m_INIFile.m_strWebDocumentsPath+"/webservices/html");

		

		QByteArray data;
		if(!FileCruncher::EncodeDirectory2Binary(SETTINGS_PASSWORD,strPath,data))
			return false;

		QFile webdata(QCoreApplication::applicationDirPath()+"/webdata.bin");
		if(!webdata.open(QIODevice::WriteOnly))
			return false;
		webdata.write(data);
		webdata.close();
	}
#endif

	lstWebPages.clear();

	// IF data.bin exists, use it for web dir
	QByteArray data;
	QFile webdata(QCoreApplication::applicationDirPath()+"/webdata.bin");
	if(!webdata.open(QIODevice::ReadOnly))
		return false;
	data = webdata.readAll();
	webdata.close();

	QList<QString> lstPaths;
	QList<QDateTime> lstLastModifyTimes;
	QList<QByteArray> lstBinaryContent;

	if(!FileCruncher::DecodeBinary2Directory(SETTINGS_PASSWORD,data,lstPaths,lstLastModifyTimes,lstBinaryContent))
		return false;

	int nSize=lstPaths.size();
	for(int i=0;i<nSize;++i)
	{
		lstWebPages["/"+lstPaths.at(i).toLower()]=HtmlDocument(lstBinaryContent.at(i),lstLastModifyTimes.at(i));
	}

	return true;
}




bool ApplicationServer::IsHttpStarted()
{
	QMutexLocker locker(&m_Mutex);
	return m_HttpServerStarted;

}