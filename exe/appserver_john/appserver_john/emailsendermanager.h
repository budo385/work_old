#ifndef EMAILSENDERMANAGER_H
#define EMAILSENDERMANAGER_H

#include "common/common/backgroundtaskinterface.h"
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include <QReadWriteLock>
#include <QStringList>
#include <QCoreApplication>

class EmailTask
{
public:
	QString strMailServer;
	QString from; 
	QString to; 
	QString subject; 
	QString body; 
	QList<QString> bcc;
	QString strUserName;
	QString strPassword;
	bool bBodyIsHtml;
	bool bSendBodyAsFormatedMimeMessage;
	int nSmtpPort;
};

class EmailSenderManager : public QObject, public BackgroundTaskInterface
{
	Q_OBJECT

public:
	EmailSenderManager();
	~EmailSenderManager();

	void		ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");					
	void		SendEmail(Status &err, DbRecordSet &lstReturnedEmailTasksID, QString strAppServerID, DbRecordSet rowFolder, DbRecordSet recMail, DbRecordSet lstUsersToSendEmail, QString strSubject, QString strBody,QString strCatalogNameFullPath="",QStringList lstAttachments = QStringList(),bool bZipAttachment=false);
	bool		GetStatusForMail(int nEmailTaskID,QString &strStatus);

signals:
	void		GlobalPeriodChanged(int nPeriodSec); 
private:
	int			ScheduleSendMail(const QString &strMailServer, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc,  QString strUserName ="", QString strPassword="",const bool bBodyIsHtml=false, const bool bSendBodyAsFormatedMimeMessage=false, int nSmtpPort=0); 


	QReadWriteLock			m_Lock;
	QHash<int,EmailTask>	m_lstScheduledEmails;
	QHash<int,QString>		m_lstStatusSentEmails;
	int						m_nNextEmailID;
	QMutex					m_Mutex;
};

#endif // EMAILSENDERMANAGER_H
