#ifndef EMAILSENDERMANAGEREXECUTOR_H
#define EMAILSENDERMANAGEREXECUTOR_H

#include <QObject>
#include "common/common/backgroundtaskexecutor.h"

class EmailSenderManagerExecutor : public BackgroundTaskExecutor
{
	Q_OBJECT


public:
	EmailSenderManagerExecutor(BackgroundTaskInterface *pTask,QObject *parent);

public slots:
	void OnGlobalPeriodChanged(int nPeriodSec);
	
};

#endif // EMAILSENDERMANAGEREXECUTOR_H
