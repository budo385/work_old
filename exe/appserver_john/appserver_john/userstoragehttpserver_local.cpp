#include "userstoragehttpserver_local.h"
#include "common/common/authenticator.h"
#include "common/common/datahelper.h"
#include "trans/trans/httputil.h"
#include <QFileInfo>
#include "common/common/config_version_john.h"
#include "filecruncher.h"

#include "common/common/logger.h"
extern Logger				g_Logger;
#include "usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "settingsmanager.h"
extern SettingsManager	*g_SettingsManager;



QString UserStorageHTTPServer_Local::InitializeUserStorageDirectory(Status &pStatus,QString strUserSubDir)
{
	return g_UserSessionManager->InitializeUserStorageDirectory(pStatus,strUserSubDir);
}

//patch for MP: web_session=strCookieValue+_xxxxxx
bool UserStorageHTTPServer_Local::AuthenticateUser(QString strCookieValue,QString &strSessionID)
{
	if (strCookieValue.indexOf("web_session_everDocs")<0)
	{
		if (strCookieValue.indexOf("_",11)>0)
		{
			strCookieValue=strCookieValue.left(strCookieValue.indexOf("_",11));
		}
	}

	return g_UserSessionManager->AuthenticateUser(strCookieValue,strSessionID);
}
//strAliasDir can be catalog or catalog/subfolder
void UserStorageHTTPServer_Local::CheckPermission(Status &pStatus,QString strSession,int nMethodType,QString strAliasDir,QString strDirPath,QString strFileName,qint64 nFileSizeForUpload)
{
	int nOperation=-1;

	if (nMethodType==METHOD_TYPE_DOWNLOAD)
		nOperation=SettingsManager::OP_DOWNLOAD;
	else if (nMethodType==METHOD_TYPE_UPLOAD)
		nOperation=SettingsManager::OP_UPLOAD;
	else if (nMethodType==METHOD_TYPE_CLEAR)
		nOperation=SettingsManager::OP_DELETE;


	QString strCatalogName;
	int nIdx=strAliasDir.indexOf("/");
	if (nIdx>0)
	{
		strCatalogName=strAliasDir.left(nIdx);
	}
	else
		strCatalogName=strAliasDir;


	/*
	QString strUsername=g_UserSessionManager->GetUserName(strSession);
	if(!g_SettingsManager->TestUserRight(strUsername,strCatalogName,nOperation))
	{
		pStatus.setError(1,"Unauthorized");
		return;
	}
	*/
}


void UserStorageHTTPServer_Local::GetFilePath(Status &status,int nMethodType,QString strSession, const HTTPRequest &request, const QUrl &url,QString &strAliasDir, QString &strDirPath,QString &strFileName, qint64 &nFileSizeForUpload)
{
	//find target directory + filename (upload?)
	//---------------------------------------------------
	//strAliasDir=QUrl::fromPercentEncoding(request.path().toLatin1());
	strAliasDir=url.path(QUrl::FullyDecoded);
	if (strAliasDir.left(14).toLower()=="/user_storage/")
		strAliasDir=strAliasDir.mid(14);

	QString strFileTypes;

	if (strAliasDir.left(THUMB_CACHE_PREFIX_SIZE)==THUMB_CACHE_PREFIX)
	{
		strAliasDir=strAliasDir.mid(THUMB_CACHE_PREFIX_SIZE+1);
		QFileInfo info(strAliasDir);
		strAliasDir=info.path();
		strFileName=info.fileName();
		strDirPath=m_strThumbDirRoot+"/"+strAliasDir;
	}
	else if (strAliasDir.left(CATALOG_STORAGE_PREFIX_SIZE)==CATALOG_STORAGE_PREFIX)
	{
		strAliasDir=strAliasDir.mid(CATALOG_STORAGE_PREFIX_SIZE+1);
		QFileInfo info(strAliasDir);
		strAliasDir=info.path();
		strFileName=info.fileName();

		QString strAlias;
		bool bIsDrive;
		DbRecordSet lstFolders;
		g_SettingsManager->GetFolders(lstFolders);
		strDirPath = FileCruncher::ResolveCatalogPath(lstFolders, strAliasDir, strAlias,bIsDrive);
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("UserStorageHTTPServer_Local::GetFilePath (%1), resolved to path (%2)").arg(strAliasDir).arg(strDirPath));

	}
	else if (strAliasDir.left(USER_STORAGE_PREFIX_SIZE)==USER_STORAGE_PREFIX)
	{
		strAliasDir=strAliasDir.mid(USER_STORAGE_PREFIX_SIZE+1);
		QFileInfo info(strAliasDir);
		strAliasDir=info.path();
		strFileName=info.fileName();
		strDirPath=g_UserSessionManager->GetUserStorageDirectoryPath()+"/"+strAliasDir;
	}
	else
	{
		status.setError(1,"Path can not be resolved!");
		return;
	}

	//find target filename
	//---------------------------------------------------
	if (nMethodType==METHOD_TYPE_UPLOAD)
	{
		strFileName=GetFileName_Upload(url,request,nFileSizeForUpload);
		if (strFileName.isEmpty())
		{
			status.setError(1,"Filename not specified!");
			return;
		}
	}

}

