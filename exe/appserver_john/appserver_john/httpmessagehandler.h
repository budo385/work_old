#ifndef HTTPMESSAGEHANDLER_H
#define HTTPMESSAGEHANDLER_H

#include "trans/trans/httpmultipartserver.h"

class HTTPMessageHandler : public HTTPMultipartHandler
{
public:

	bool HandleMessage(Status &err, QList<HTTPRequest> lstRequests, QByteArray *pBufResponse, QString strHTTPPath);	

private:
	
};

#endif // HTTPMESSAGEHANDLER_H
