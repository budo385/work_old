#include "restrpcskeleton_service.h"
#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;

RestRpcSkeleton_Service::RestRpcSkeleton_Service(int RPCType):
RpcSkeleton(RPCType)
{
	SetNameSpace("service"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("POST/service/catalog/read",&RestRpcSkeleton_Service::ReadCatalog);
	mFunctList.insert("POST/service/document/picture",&RestRpcSkeleton_Service::GetPicture);
	mFunctList.insert("POST/service/catalog/picture/rotate",&RestRpcSkeleton_Service::RotatePicture);
	mFunctList.insert("POST/service/document/delete",&RestRpcSkeleton_Service::DeleteDocument);
	mFunctList.insert("POST/service/document/upload",&RestRpcSkeleton_Service::UploadDocument);
	mFunctList.insert("POST/service/document/download",&RestRpcSkeleton_Service::DownloadDocument);
	mFunctList.insert("GET/service/catalogs",&RestRpcSkeleton_Service::GetCatalogs);
	mFunctList.insert("POST/service/catalog/picture",&RestRpcSkeleton_Service::GetCatalogPicture);
	mFunctList.insert("POST/service/catalog/rename",&RestRpcSkeleton_Service::RenameCatalog);
	mFunctList.insert("POST/service/catalog/rename_client",&RestRpcSkeleton_Service::RenameCatalogClient);
	mFunctList.insert("POST/service/catalog/create_client",&RestRpcSkeleton_Service::CreateCatalogClient);
	mFunctList.insert("POST/service/catalog/unpublish_client",&RestRpcSkeleton_Service::UnpublishCatalogClient);
	mFunctList.insert("POST/service/login",&RestRpcSkeleton_Service::Login);
	mFunctList.insert("POST/service/Logout",&RestRpcSkeleton_Service::Logout);
	mFunctList.insert("GET/service/catalogs/status",&RestRpcSkeleton_Service::GetFolderStatus);
	mFunctList.insert("POST/service/catalog/invite",&RestRpcSkeleton_Service::MakeInvitation);
	mFunctList.insert("GET/service/settings/server_data",&RestRpcSkeleton_Service::GetServerData);
	mFunctList.insert("POST/service/settings/reload",&RestRpcSkeleton_Service::ReloadSettings);
}

bool RestRpcSkeleton_Service::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;

	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) 
		return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);

		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);

			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

return true;
}

void RestRpcSkeleton_Service::ReadCatalog(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=6)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	int  Ret_nTotalCount_SubFolders;
	DbRecordSet  Ret_Files;
	DbRecordSet  Ret_SubFolders;
	QString  strAliasDirectory;
	int  nFromN;
	int  nToN;
	int  nFromN_SubFolder;
	int  nToN_SubFolder;
	int  nSortOrder;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nFromN_SubFolder,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nToN_SubFolder,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&nSortOrder,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->ReadCatalog(err,Ret_nTotalCount,Ret_nTotalCount_SubFolders,Ret_Files,Ret_SubFolders,strAliasDirectory,nFromN,nToN,nFromN_SubFolder,nToN_SubFolder,nSortOrder);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_nTotalCount_SubFolders,"nTotalCount_SubFolder");
	rpc.msg_out->AddParameter(&Ret_Files,"files","file");
	rpc.msg_out->AddParameter(&Ret_SubFolders,"folders","folder");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::GetPicture(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=8)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	int  Ret_nCurrentPos;
	DbRecordSet  Ret_Files;
	QString  strAliasDirectory;
	QString  strFileName;
	int  nHeading;
	int  nSortOrder;
	int  nPicWidth;
	int  nPicHeight;
	bool  bKeepAspectRatio;
	bool  bReturnThumbAsBinary;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nHeading,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nSortOrder,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nPicWidth,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&nPicHeight,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&bKeepAspectRatio,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&bReturnThumbAsBinary,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->GetPicture(err,Ret_nTotalCount,Ret_nCurrentPos,Ret_Files,strAliasDirectory,strFileName,nHeading,nSortOrder,nPicWidth,nPicHeight,bKeepAspectRatio,bReturnThumbAsBinary);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_nCurrentPos,"nCurrentPos");
	rpc.msg_out->AddParameter(&Ret_Files,"files","file");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::RotatePicture(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=7)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_Files;
	QString  strAliasDirectory;
	QString  strFileName;
	int  nAngle;
	int  nPicWidth;
	int  nPicHeight;
	bool  bKeepAspectRatio;
	bool  bReturnThumbAsBinary;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nAngle,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nPicWidth,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nPicHeight,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&bKeepAspectRatio,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&bReturnThumbAsBinary,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->RotatePicture(err,Ret_Files,strAliasDirectory,strFileName,nAngle,nPicWidth,nPicHeight,bKeepAspectRatio,bReturnThumbAsBinary);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Files,"File","file");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::DeleteDocument(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strAliasDirectory;
	QString  strFileName;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->DeleteDocument(err,strAliasDirectory,strFileName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::UploadDocument(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=5)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strAliasDirectory;
	QString  strFileName;
	QByteArray  byteFileData;
	bool  bOverWrite;
	QString  strSession;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strFileName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&byteFileData,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&bOverWrite,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strSession,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->UploadDocument(err,strAliasDirectory,strFileName,byteFileData,bOverWrite,strSession);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::DownloadDocument(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strFileURL;
	QByteArray  Ret_byteFileData;
	bool		bIsZip;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strFileURL,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&bIsZip,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->DownloadDocument(err,strFileURL,Ret_byteFileData,bIsZip);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_byteFileData,"Ret_byteFileData");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::GetCatalogs(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	DbRecordSet  Ret_Catalogs;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	g_ServiceHandler->GetCatalogs(err,Ret_Catalogs);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Catalogs,"Catalogs","catalog");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::GetCatalogPicture(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strCatalogName;
	QByteArray  Ret_byteFileData;
	QByteArray  Ret_byteFileDataLarge;
	int  Ret_nTotalCount;
	QString  Ret_strName;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strCatalogName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->GetCatalogPicture(err,strCatalogName,Ret_byteFileData,Ret_byteFileDataLarge,Ret_nTotalCount,Ret_strName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_byteFileData,"CatalogPicture");
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_strName,"ServerName");
	rpc.msg_out->AddParameter(&Ret_byteFileDataLarge,"CatalogPictureLarge");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::RenameCatalog(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strOldName;
	QString  strNewName;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strOldName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strNewName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->RenameCatalog(err,strOldName,strNewName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::RenameCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strOldName;
	QString  strNewName;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strOldName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strNewName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->RenameCatalogClient(err,strOldName,strNewName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::CreateCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strCatalogName;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strCatalogName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->CreateCatalogClient(err,strCatalogName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::UnpublishCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strAliasDirectory;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->UnpublishCatalogClient(err,strAliasDirectory);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::Login(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=7)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	QString RetOut_strSessionID;
	QString strUserName;
	QString strAuthToken;
	QString strClientNonce;
	int nProgCode=0;
	QString strProgVer="";
	QString strClientID="";
	QString strPlatform="";

	rpc.msg_in->GetParameter(0,&strUserName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strAuthToken,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strClientNonce,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nProgCode,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strProgVer,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&strClientID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&strPlatform,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->Login(err,RetOut_strSessionID,strUserName,strAuthToken,strClientNonce,nProgCode,strProgVer,strClientID,strPlatform);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_strSessionID,"Session");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::Logout(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strSessionID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strSessionID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->Logout(err,strSessionID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::GetFolderStatus(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	DbRecordSet  Ret_Folders;
	int  Ret_nSettingsChanged;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	g_ServiceHandler->GetFolderStatus(err,Ret_Folders,Ret_nSettingsChanged);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Folders,"Folders","folder");
	rpc.msg_out->AddParameter(&Ret_nSettingsChanged,"SeettingsChanged");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::MakeInvitation(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=11)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  Ret_strBody;
	int  nMessageType;
	QString  strCatalogName;
	QString  strFirstName;
	QString  strLastName;
	QString  strEmail;
	QString  strPhone;
	int  nIsHtml;
	QString  strEmailSubject;
	QString  strEmailBody;
	DbRecordSet  FileNames;
	bool  bZip;
	int  Ret_ID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&nMessageType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strCatalogName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strFirstName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&strLastName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strEmail,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&strPhone,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&nIsHtml,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&strEmailSubject,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&strEmailBody,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(9,&FileNames,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(10,&bZip,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->MakeInvitation(err,Ret_strBody,nMessageType,strCatalogName,strFirstName,strLastName,strEmail,strPhone,nIsHtml,strEmailSubject,strEmailBody,FileNames,bZip,Ret_ID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_strBody,"Message");
	rpc.msg_out->AddParameter(&Ret_ID,"Email_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::GetServerData(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	/*
	if(rpc.msg_in->GetParameterCount()!=3)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}
	*/

	//extract parameters to local vars
	QString  Ret_strName;
	QByteArray  Ret_SrvPic;
	QString  Ret_strEmailSignature;
	int  nMajor;
	int  nMinor;
	int  nRev;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	//rpc.msg_in->GetParameter(0,&nMajor,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	//rpc.msg_in->GetParameter(1,&nMinor,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	//rpc.msg_in->GetParameter(2,&nRev,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->GetServerData(err,Ret_strName,Ret_SrvPic,Ret_strEmailSignature,nMajor,nMinor,nRev);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_strName,"ServerName");
	rpc.msg_out->AddParameter(&Ret_SrvPic,"ServerPicture");
	rpc.msg_out->AddParameter(&Ret_strEmailSignature,"EmailSignature");
	rpc.msg_out->AddParameter(&nMajor,"nMajor");
	rpc.msg_out->AddParameter(&nMinor,"nMinor");
	rpc.msg_out->AddParameter(&nRev,"nRev");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::ReloadSettings(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	bool  bSkipReloadCatalogs;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&bSkipReloadCatalogs,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	g_ServiceHandler->ReloadSettings(err,bSkipReloadCatalogs);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

