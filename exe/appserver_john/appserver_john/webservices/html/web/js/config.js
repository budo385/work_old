//REST CONFIG (web browsers)

// example:							https://93.142.131.180:12000/iphone/index.html
var protocol 					 =	document.location.protocol;						//	https, http, ftp,..
var hostname					 = 	document.location.hostname;						//	93.142.131.180
var port						 = 	document.location.port;							//	12000,..

var ROOT_URL 					 = 	protocol+'//'+hostname+":"+port;
var REST_WEB_ROOT_URL 			 = 	protocol+'//'+hostname+":"+port+'/rest';
var REST_WEB_DOCS_URL 			 = 	ROOT_URL+"/user_storage/";
var REST_LOGIN 					 = 	REST_WEB_ROOT_URL+"/service/login";
var REST_LOGOUT 				 = 	REST_WEB_ROOT_URL+"/service/logout";
var COOKIE_EXPIRE_MIN			 =  20;
var FFH_CONNECT_SERVER_MASTER_IP =  "http://85.25.129.105:11000";
var FFH_CONNECT_SERVER_SLAVE_IP  =  "http://85.25.129.105:11000";
var FFH_CONNECT_SERVER_LIST 	 =  "www.sokrates.ch/ffh_servers.php";
var FFH_CONNECT_SERVER_TIMEOUT 	 =  10*1000;

var PROGRAM_VERSION_MAJOR	  	 = 	1;
var PROGRAM_VERSION_MINOR 	  	 = 	0;
var PROGRAM_VERSION_REVISION  	 = 	2;
var PROGRAM_CODE 			  	 = 	3; 
var PROGRAM_VERSION 		  	 = 	PROGRAM_VERSION_MAJOR+"."+PROGRAM_VERSION_MINOR+"."+PROGRAM_VERSION_REVISION;

  // session storage
  function setSessionkey(key, val) 
  {
	  sessionStorage.removeItem(key);
	  sessionStorage.setItem(key, val);
  } 
  // local storage
  function setLocalkey(key, val) 
  {
	  localStorage.removeItem(key);
	  localStorage.setItem(key, val);
  }
  
  // device detection
  function deviceInfo()
  { 
     var agent  =  navigator.userAgent.toLowerCase();		
	 var device = "Unknown Device";

	 if (agent.indexOf("linux")!=-1||agent.indexOf("x11")!=-1||agent.indexOf("win")!=-1||agent.indexOf("mac")!=-1){
		 device = "desktop";
	 }
	 if (agent.indexOf("iphone")!=-1||agent.indexOf("ipod")!=-1||agent.indexOf("ipad")!=-1) {
		 device  = "idevice"; 
		 iFilter(agent);
	 } 
	 if (agent.indexOf("android")!=-1){
		 device  = "android"; 
     }  
	 if (agent.indexOf("android")!=-1 && agent.indexOf("webkit")!=-1) {
		 device  = "android-webkit"; 
	 }	
	 
	 /*
	 if ( agent.indexOf("linux")  !=-1 )  OSName = "linux";		// all versions of Linux
	 if ( agent.indexOf("x11")    !=-1 )  OSName = "unix";		// all other UNIX flavors 
	 if ( agent.indexOf("win")    !=-1 )  OSName = "windows"; 	// all versions of Windows
	 if ( agent.indexOf("mac")    !=-1 )  OSName = "macos";		// all versions of Macintosh OS	
	 if ( agent.indexOf("iphone") !=-1 )  OSName = "iphone";  	// all versions of iPhone MacOS  
	 if ( agent.indexOf("ipod")   !=-1 )  OSName = "ipod";  	// all versions of iPhone MacOS  
	 if ( agent.indexOf("ipad")   !=-1 )  OSName = "ipad";		// all versions of iPad MacOS  
	 */
	  			
  return(device);
  }
  
  function iFilter(str)
  {  
	if (str.indexOf("iphone")!=-1||str.indexOf("ipod")!=-1)
	  setLocalkey("ga_platform","iphone");
	
	if (str.indexOf("ipad")!=-1)
	  setLocalkey("ga_platform","ipad");
  }