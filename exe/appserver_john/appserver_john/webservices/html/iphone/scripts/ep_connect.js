  var db, IPjames, Destination, IP_PORT_url;
  var x=0;
  var strPost='';
  var val_Input='';
$(document).ready(function()
{
	sessionStorage.clear();
	localStorage.removeItem('cat');
	localStorage.removeItem('serverid');
	localStorage.removeItem('user');
	localStorage.removeItem('pass');
	localStorage.removeItem('server');
	
	setSessionkey("enabledConnectBtn", "enabled");
	//enableConnectBtn(true);
	
	var strServerID = getParameterByName('server_id');

	// autofill server id
	if(localStorage.wa_serverId)  
		$('#IDfield').val(localStorage.wa_serverId);
	else if(strServerID) 
		$('#IDfield').val(strServerID);
	
	// LOGIN PROCEDURE @
	$("#ConnectBtn").click(function(event)
	{ 
	  event.preventDefault();
	  if(sessionStorage.enabledConnectBtn == "disabled") {
		return false;
	  }	
	
	  val_Input = $('#IDfield').val();
	  val_Input =val_Input.toLowerCase();
	  
	  if(sessionStorage.enabledConnectBtn == "enabled"){
		 $('#ConnectBtn').attr('disabled', "disabled");
		 setSessionkey("enabledConnectBtn", "disabled");
	  }
	  
	  /*   standard ID     "skxvr16989"
	   *   invite system   "<<zxxgc21392//John//abc>>"
	   *   ID/IP:PORT      "wjdos82202/192.168.101.23:55000"
	   */
	  if(val_Input)
	  {
		 var temp_ = val_Input;

		 // <<zxxgc21392//John//abc>>
		 if(temp_.match("<<") && temp_.match(">>"))
		 {
			temp_= temp_.substring(2, temp_.length-2);
			var array = temp_.split("//");
			val_Input = array[0];
			setSessionkey("inv_user",array[1]);  
			setSessionkey("inv_pass",array[2]);	
		 }
		 // wjdos82202/192.168.101.23:55000
		 if(temp_.match("/") && temp_.match(":"))
		 {
			 x=1;
			 var array = temp_.split("/");
			 val_Input = array[0];
			 IP_PORT_url = array[1];
			 setSessionkey("val_Input",array[0]);
			 setSessionkey("IP_PORT_url",array[1]);
		 }
		 
	  	var PFH_program_code 				= PROGRAM_CODE;
		var PFH_program_version_major 		= PROGRAM_VERSION_MAJOR;
		var PFH_program_version_minor 		= PROGRAM_VERSION_MINOR;
		var PFH_program_version_revision 	= PROGRAM_VERSION_REVISION;
		var PFH_program_platform_name 		= getua();
		var PFH_program_platform_UID 		= "";
		var PFH_client_identity 			= (localStorage.wa_clientId) ? localStorage.wa_clientId : "";
		var PFH_host_identity 				= val_Input;
		var PFH_host_URL 					= "";
		var PFH_host_port 					= "";
		var PFH_host_stats_nat_used 		= "";
		var PFH_host_local_ip 				= "";

		strPost=
		'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS>'+
		'<PROGRAM_CODE>'+PFH_program_code+'</PROGRAM_CODE>'+
		'<PROGRAM_VERSION_MAJOR>'+PFH_program_version_major+'</PROGRAM_VERSION_MAJOR>'+
		'<PROGRAM_VERSION_MINOR>'+PFH_program_version_minor+'</PROGRAM_VERSION_MINOR>'+
		'<PROGRAM_VERSION_REVISION>'+PFH_program_version_revision+'</PROGRAM_VERSION_REVISION>'+
		'<PROGRAM_PLATFORM_NAME>'+PFH_program_platform_name+'</PROGRAM_PLATFORM_NAME>'+
		'<PROGRAM_PLATFORM_UID>'+PFH_program_platform_UID+'</PROGRAM_PLATFORM_UID>'+
		'<CLIENT_IDENTITY xsi:nil="true"/><HOST_IDENTITY>'+PFH_host_identity+'</HOST_IDENTITY>'+
		'<HOST_URL>'+PFH_host_URL+'</HOST_URL><HOST_PORT>'+PFH_host_port+'</HOST_PORT>'+
		'<HOST_STATS_NAT_USED>'+PFH_host_stats_nat_used+'</HOST_STATS_NAT_USED>'+
		'<HOST_SSL></HOST_SSL>'+
		'<HOST_LOCAL_IP>'+PFH_host_stats_nat_used+'</HOST_LOCAL_IP></PARAMETERS></REQUEST>';

		connect();	

	}
	else alert("A Server ID or IP:PORT is Required.");
	});
    
	// connect ajax call
	function connect(t)
	{
	  // with delay = t. example: connect(2000);
	  if(t!=0){
		  setTimeout(function() {
			  BO_Call("POST",FFH_CONNECT_SERVER_MASTER_IP+"/rest/service/register",pfCallBack,strPost);
		  },t);
	  }
	  // no delay
	  else
	  	BO_Call("POST",FFH_CONNECT_SERVER_MASTER_IP+"/rest/service/register",pfCallBack,strPost);
	}  
	
	// connect callback
	function pfCallBack(xml)
	{
		if(nLastBO_ErrorCode!=0)
		{ 
		  alert(strLastBO_ErrorText);
		  if(nLastBO_ErrorCode==1000) window.location="index.html";
		  else window.location=document.referrer;
		return;
		}
		
		var xdom 	 = $(xml);
		var code  	 = xmlDecode(xdom.find("STATUS_CODE").text());
		var text  	 = xmlDecode(xdom.find("STATUS_TEXT").text());
		var url  	 = xmlDecode(xdom.find("HOST_URL").text());
		var port  	 = xmlDecode(xdom.find("HOST_PORT").text());
		var url_lan  = xmlDecode(xdom.find("HOST_LOCAL_IP").text());
		var clientID = xmlDecode(xdom.find("CLIENT_IDENTITY").text());
		var prot 	 = document.location.protocol+"//";
		
		if (x==1 && (code==0 || code==2))
	    {
			IPjames=prot+sessionStorage.val_Input+":"+sessionStorage.IP_PORT_url;
			setLocalkey("wa_serverId",val_Input);
			setLocalkey("wa_clientId",clientID);
			setLocalkey("wa_serverUrl",IPjames);
			Destination=IPjames+"/iphone/login.html";
			redirect();
	  	}
	    else if(x==0 && code==0)
		{
			IPjames=prot+url+":"+port;
					
			//if this document origins from LAN IP then this means that LAN IP is accessbile
			if(window.location.hostname == url_lan)
			{
				IPjames=prot+url_lan+":"+port;
			}
			
			setLocalkey("wa_serverId",val_Input);
			setLocalkey("wa_clientId",clientID);
			setLocalkey("wa_serverUrl",IPjames);
			
			Destination=IPjames+"/iphone/login.html";
			redirect();
		}
		else {
			alert(text);
			return;
		}
	}
	
	// redirection
	function redirect(){
		window.location=Destination;
	return;
	}

	//$("#rightnav").click(function(event) {event.preventDefault();});
	
});

function getParameterByName( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function enableConnectBtn(enable)
{
    var $handle = $('#ConnectBtn');	
	
    if (enable) {
        $handle.attr('disabled', false);
		setSessionkey("enabledConnectBtn", "enabled");
        //$handle.removeClass('disabled');
    } 
	else {
        $handle.attr('disabled', true);
		setSessionkey("enabledConnectBtn", "disabled");
        //$handle.addClass('disabled');
    }
}
