// Hiding URL bar in mobile safari
window.onload = function() { setTimeout(function(){window.scrollTo(0, 1);}, 100); }

// REST config
var protocol 						=	document.location.protocol;					
var hostname						= 	document.location.hostname;					
var port							= 	document.location.port;						

var ROOT_URL 						= 	protocol+'//'+hostname+":"+port;
var REST_WEB_ROOT_URL 				= 	protocol+'//'+hostname+":"+port+'/rest';
var REST_WEB_DOCS_URL 				= 	ROOT_URL+"/user_storage/";
var REST_LOGIN 						= 	REST_WEB_ROOT_URL+"/service/login";
var REST_LOGOUT 					= 	REST_WEB_ROOT_URL+"/service/logout";

var COOKIE_EXPIRE_MIN				= 20;
var FFH_CONNECT_SERVER_MASTER_IP 	= "http://www.everxconnect.com:11000"; 			
var FFH_CONNECT_SERVER_LIST			= "http://www.everdocs.com/everxconnect.txt"
var FFH_CONNECT_SERVER_TIMEOUT 		= 10*1000; //10 sec
var PROGRAM_VERSION_MAJOR	  		= 1;
var PROGRAM_VERSION_MINOR 	  		= 0;
var PROGRAM_VERSION_REVISION  		= 2;
var PROGRAM_VERSION 		  		= PROGRAM_VERSION_MAJOR+"."+PROGRAM_VERSION_MINOR+"."+PROGRAM_VERSION_REVISION;

var DIGIT_APP						= '11'; 								
var DIGIT_PLATFORM = (getua()=='iphone'||getua()=='ipad')? '11' : '10';  	
var DIGIT_STATUS					= '0'; 								
var BIT_ADDON						= '000';								
var PROGRAM_CODE 			  		= DIGIT_APP+DIGIT_PLATFORM+DIGIT_STATUS+BIT_ADDON;

// SET SESSION STORAGE - patch
function setSessionkey(key, val)
{
	sessionStorage.removeItem(key);
	sessionStorage.setItem(key, val);
} 
// SET LOCAL STORAGE - patch
function setLocalkey(key, val)
{
	localStorage.removeItem(key);
	localStorage.setItem(key, val);
}
	
// OS DETECTION (PLATFORM) - Save & Get
function getua()
{  
  var agent  =  navigator.appVersion.toLowerCase();  
  var OS = "Unknown OS"; 								// indicates OS detection failure  
  if ( agent.indexOf("linux")  !=-1 )  OS = "linux";	// all versions of Linux
  if ( agent.indexOf("x11")    !=-1 )  OS = "unix";		// all other UNIX flavors 
  if ( agent.indexOf("win")    !=-1 )  OS = "windows"; 	// all versions of Windows
  if ( agent.indexOf("mac")    !=-1 )  OS = "macos";	// all versions of Macintosh OS	
  if ( agent.indexOf('iphone') !=-1 || agent.indexOf('ipod')!=-1) OS = "iphone"; // all versions of iPhone/iPod
  if ( agent.indexOf('ipad')   !=-1 )  OS = "ipad";		// all versions of iPad MacOS 
  
  setLocalkey("wa_platform",OS.toLowerCase());
  return(OS); 
}

//check errorcode to determine if call was OK (used in call back func)
function errCheck()
{
	if(nLastBO_ErrorCode!=0) 
	{
		if(nLastBO_ErrorCode==1000)	 alert(strLastBO_ErrorText);	
		window.location="login.html";
	 return false;
	}
	else return true;
}

  