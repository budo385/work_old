#include "appserverservice.h"
#include "common/common/config_version_john.h"
//#include <QFile>

#include "common/common/logger.h"
extern Logger g_Logger;
#include "applicationserver.h"
extern ApplicationServer *g_AppServer;


//note on systems with more then 1 app service use strApplicationServiceName
AppServerService::AppServerService(int argc, char **argv, QString strApplicationServiceName,bool bAutoStartUp)    
: QtService<QCoreApplication>(argc, argv, strApplicationServiceName)
{
	setServiceDescription(SERVER_JOHN_SERVICE_DESC);
	setServiceFlags(QtServiceBase::CanBeSuspended);

	if (bAutoStartUp)
		setStartupType(QtServiceController::AutoStartup);
	//else
	//	setStartupType(QtServiceController::ManualStartup);

	//RegisterPowerEvents();
}

AppServerService::~AppServerService()
{
}

void AppServerService::start()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Service: start");

	//1st time start:
	if (g_AppServer==NULL)
		g_AppServer=new ApplicationServer(application());

	Status err;
	g_AppServer->Start(err);
	if(!err.IsOK())
	{
		QCoreApplication *app = application();
		logMessage(QString("Failed to start application server"), QtServiceBase::Error);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
		if (err.getErrorCode()!=666) //skip if first time reset.
			g_AppServer->SetLastError(err.getErrorCode(),err.getErrorTextRaw());
		g_AppServer->Stop();
		app->quit();
	}
}

void AppServerService::stop()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Service: stop");
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Service received STOP signal");

	if (g_AppServer!=NULL)
		if(g_AppServer->IsServiceRunning())
			g_AppServer->Stop();

	QCoreApplication *app = application();
	app->quit();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Service executed quit()");

}

//if pause, just stop listener, no new clients will be accepted, but connected ones will work!
void AppServerService::pause()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Service: pause");

	if (g_AppServer!=NULL)
		g_AppServer->Stop();
}

//if resume, enable listener
void AppServerService::resume()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Service: resume");

	if (g_AppServer!=NULL)
	{
		Status err;
		g_AppServer->Start(err);
		if(!err.IsOK())
		{
			QCoreApplication *app = application();
			logMessage(QString("Failed to start application server"), QtServiceBase::Error);
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START,err.getErrorText());
			g_AppServer->Stop();
			app->quit();
		}
	}
}

void AppServerService::shutdown()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Service: shutdown");

	Status err;
	g_AppServer->GetConnectionHandler()->StopPortForward(err);

	if (g_AppServer!=NULL)
		g_AppServer->Stop();
}

#ifdef _WIN32
#include <windows.h>
//TOFIX PoUnregisterPowerSettingCallback
DWORD WINAPI MyPowerHandlerEx(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{
	AppServerService *pObj = (AppServerService *)lpContext;

	if( SERVICE_CONTROL_STOP == dwControl ||
		SERVICE_CONTROL_SHUTDOWN == dwControl)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Service stop notification: reset port forwarding");

		Status err;
		g_AppServer->GetConnectionHandler()->StopPortForward(err);
		return NO_ERROR;
	}

	//additional events registered with RegisterPowerSettingNotification call
	if(SERVICE_CONTROL_POWEREVENT == dwControl)
	{
		if(PBT_APMSUSPEND == dwEventType)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Windows suspend notification: reset port forwarding");

			Status err;
			g_AppServer->GetConnectionHandler()->StopPortForward(err);
		}
	}
	return NO_ERROR;
}
#endif

//WARNING: works only from Windows Vista forward (but will not crash on older, dynamic API loading)
void AppServerService::RegisterPowerEvents()
{
#ifdef _WIN32
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Windows events registration: start");

	SERVICE_STATUS_HANDLE hService = RegisterServiceCtrlHandlerEx(L"EverDOCs-Service", MyPowerHandlerEx, this);
	if(!hService){
		DWORD dwErr = GetLastError();
		char buffer[1128] = "";
		FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwErr, 0, buffer, sizeof(buffer), NULL);
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Windows events registration: failed to register handler function. Error code: %1 (%2)").arg(dwErr).arg(buffer));
		return;
	}

	//find API dynamically (to fallback gracefully on WinXP)
	HMODULE hUserLib = LoadLibrary(L"User32.dll");
	if(!hUserLib){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Windows events registration: failed to load user32.dll");
	    return;	//failed to load library
	}

	//DLL Function Signatures
	typedef LPVOID (WINAPI *REGISTERPOWERSETTINGNOTIFICATION)( HANDLE, LPCGUID, DWORD );
	REGISTERPOWERSETTINGNOTIFICATION  regPowerAPI = (REGISTERPOWERSETTINGNOTIFICATION)GetProcAddress(hUserLib, "RegisterPowerSettingNotification");
	if(!regPowerAPI){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Windows events registration: failed to load RegisterPowerSettingNotification API");
		FreeLibrary(hUserLib);	
		return;	//function not found
	}

	//now register
	LPVOID h_power_source_notify = regPowerAPI(hService, &GUID_ACDC_POWER_SOURCE, DEVICE_NOTIFY_SERVICE_HANDLE);
	if(!h_power_source_notify){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Windows events registration: error calling RegisterPowerSettingNotification API");
		FreeLibrary(hUserLib);	
		return;	//error registering notification
	}
    
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Windows events registration: success");
	FreeLibrary(hUserLib);
#endif
}
