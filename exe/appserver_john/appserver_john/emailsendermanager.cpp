#include "emailsendermanager.h"
#include "trans/trans/smtpclient.h"
#include <QFile>
#include <QFileInfo>
#include <QDir>

#include "emailhandler.h"
#include "trans/trans/smtpclient.h"
#include "common/common/config_version_ffh.h"
#include "common/common/datahelper.h"
#include "trans/trans/httputil.h"
#include "common/common/zipcompression.h"
#include "common/common/threadid.h"


#include "common/common/logger.h"
extern Logger							g_Logger;


EmailSenderManager::EmailSenderManager()
{
	m_nNextEmailID=0;
}

EmailSenderManager::~EmailSenderManager()
{

}

//execute everything in list
void EmailSenderManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	//qDebug()<<ThreadIdentificator::GetCurrentThreadID();

	if(!m_Mutex.tryLock(1000)) //thread is occupied leave it alone
		return;

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Email Handler execute: START, thread: %1").arg(QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()));

	//copy whole list and clear main list and release lock
	QHash<int,EmailTask>	lstScheduledEmails;

	m_Lock.lockForWrite();
	lstScheduledEmails=m_lstScheduledEmails;
	m_lstScheduledEmails.clear();
	m_Lock.unlock();

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Executing Email Tasks. Number of tasks waiting: %1").arg(lstScheduledEmails.size()));

	QHashIterator<int,EmailTask> i(lstScheduledEmails);
	while (i.hasNext()) 
	{
		i.next();
		EmailTask task=i.value();

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Sending email %1 to address %2 with ID=%3").arg(task.subject).arg(task.to).arg(i.key()));

		SMTPClient client;
		if(!client.SendMail(task.strMailServer,task.from,task.to,task.subject,task.body,task.bcc,task.strUserName,task.strPassword,task.bBodyIsHtml,task.bSendBodyAsFormatedMimeMessage,task.nSmtpPort))
		{
			m_Lock.lockForWrite();
			m_lstStatusSentEmails[i.key()]=QString("FAILED");
			//m_lstStatusSentEmails[i.key()]=QString("Failed to send email %1 at email address: %2 with ID=%3").arg(task.subject).arg(task.to).arg(i.key());
			m_Lock.unlock();
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Failed to send email %1. Last error: %2").arg(task.subject).arg(client.GetLastError()));
		}
		else
		{
			m_Lock.lockForWrite();
			m_lstStatusSentEmails[i.key()]=QString("OK"); //ok if empty
			m_Lock.unlock();
			//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("OK"));
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Successfully to sent email %1").arg(task.subject));
		}
	}

	m_Mutex.unlock();

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Email Handler execute: STOP, thread: %1").arg(QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()));

}

//get status for mail id and clear it from list
//return false if id does not exist in status list
bool EmailSenderManager::GetStatusForMail(int nEmailTaskID,QString &strStatus)
{
	QWriteLocker lock(&m_Lock);
	if (m_lstStatusSentEmails.contains(nEmailTaskID))
	{
		strStatus=m_lstStatusSentEmails.value(nEmailTaskID,"NOT IN LIST");
		if (strStatus=="OK" || strStatus=="FAILED")
			m_lstStatusSentEmails.remove(nEmailTaskID);
		return true;
	}
	else
	{
		strStatus="NOT IN LIST";
		return false;
	}

}

//create task, store in list, create id, schedule for immediate execution
int	EmailSenderManager::ScheduleSendMail(const QString &strMailServer, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc,  QString strUserName, QString strPassword,const bool bBodyIsHtml, const bool bSendBodyAsFormatedMimeMessage, int nSmtpPort)
{
	EmailTask task;
	task.strMailServer=strMailServer;
	task.from=from;
	task.to=to;
	task.subject=subject;
	task.body=body;
	task.bcc=bcc;
	task.strUserName=strUserName;
	task.strPassword=strPassword;
	task.bBodyIsHtml=bBodyIsHtml;
	task.bSendBodyAsFormatedMimeMessage=bSendBodyAsFormatedMimeMessage;
	task.nSmtpPort=nSmtpPort;

	QWriteLocker lock(&m_Lock);
	m_nNextEmailID++;

	//m_Lock.lockForWrite();
	m_lstScheduledEmails[m_nNextEmailID]=task;
	m_lstStatusSentEmails[m_nNextEmailID]="NOT SENT";
		
	return m_nNextEmailID;
}





/*
	Attachments will be added one by one and zipped in template:
*/

void EmailSenderManager::SendEmail(Status &err, DbRecordSet &lstReturnedEmailTasksID, QString strAppServerID, DbRecordSet rowFolder, DbRecordSet recMail, DbRecordSet lstUsersToSendEmail, QString strSubject, QString strBody,QString strCatalogNameFullPath,QStringList lstAttachments,bool bZipAttachment)
{

		lstReturnedEmailTasksID.clear();
		lstReturnedEmailTasksID.addColumn(QVariant::Int,"ID");


		QString strCatalogName="";
		if (strCatalogNameFullPath.isEmpty())
			strCatalogName=rowFolder.getDataRef(0,"ALIAS").toString();
		else
			strCatalogName=strCatalogNameFullPath;

		QString strCatPath=rowFolder.getDataRef(0,"PATH").toString();


		QByteArray strCatalogBase64Pic = rowFolder.getDataRef(0,"THUMB_LARGE").toByteArray().toBase64();
		strCatalogBase64Pic = DataHelper::EncodeBase64To73LineLength(strCatalogBase64Pic);

		QString strWidth = QString::number(rowFolder.getDataRef(0,"THUMB_LARGE_WIDTH").toInt());
		QString strHeight = QString::number(rowFolder.getDataRef(0,"THUMB_LARGE_HEIGHT").toInt());
	
		QString strPathToTemplateEmail;
		if (lstAttachments.size()==0)
			strPathToTemplateEmail= QCoreApplication::applicationDirPath()+"/template_settings/everDOCs_Invitation.html";
		else
			strPathToTemplateEmail= QCoreApplication::applicationDirPath()+"/template_settings/everDOCs_Doc.html";
	

		QFile file_invite(strPathToTemplateEmail);
		if (!file_invite.exists())
		{
			err.setError(1,QString("Invitation email template %1 not found!").arg(strPathToTemplateEmail));
			return;
		}

		if(!file_invite.open(QIODevice::ReadOnly))
		{
			err.setError(1,QString("Invitation email template %1 can not be red!").arg(strPathToTemplateEmail));
			return;
		}

		QString strDataMail = file_invite.readAll();

		/*
		1 - body
		2,3 invite link
		4 - catalog name
		5 - parameters for Download_App_Instructions.html: same as invite!
		6 - <<id/user/pass>>
		*/

		//send mail... format as <<gdfgdgf>>
		int nSize=lstUsersToSendEmail.getRowCount();

		QString strBodyPlainText=strBody;
		strBody.replace("\n","<br>");
		if (nSize!=1)
			//strBody = strBody + recMail.getDataRef(0,"SIGNATURE").toString();
			strBody = strBody + "<br>" + recMail.getDataRef(0,"SIGNATURE").toString();


		//Marin requested that CID be changeable: random:
		QString strDate=QDateTime::currentDateTime().toString("yyyyMMdd");
		QString strTime=QDateTime::currentDateTime().toString("0mmsszzz");
		QString strCID="part5."+strDate+"."+strTime+"@sokrates.ch";
		//QString strCID="part5.00050207."+strTime+"@sokrates.ch";
		strDataMail.replace("part5.00050207.08090708@sokrates.ch",strCID);

	
		//int nCnt=0;
		for (int i=0;i<nSize;i++)
		{
			QString strSalut=lstUsersToSendEmail.getDataRef(i,"SALUT").toString();
			QString strUserName=lstUsersToSendEmail.getDataRef(i,"USERNAME").toString();
			QString strUserPass=lstUsersToSendEmail.getDataRef(i,"PASSWORD").toString();
			QString strUserEmail=lstUsersToSendEmail.getDataRef(i,"EMAIL").toString();

			QString strParameters="?catalog="+QUrl::toPercentEncoding(strCatalogName);
			strParameters+="&user="+QUrl::toPercentEncoding(strUserName)+"&pass="+QUrl::toPercentEncoding(strUserPass.toLatin1().toBase64());
			QString strLink=QString(FFH_CONNECT_SERVER_MASTER_IP)+"/invite/"+strAppServerID+"/web/invite.html"+strParameters;
			QString strIPhoneInvite = "&lt;&lt;"+strAppServerID+"//"+strUserName+"//"+strUserPass+"&gt;&gt;";
			QString strIPhonePlain = "<<"+strAppServerID+"//"+strUserName+"//"+strUserPass+">>";

			QList<QString> lstBcc;
			QString strMailServer=recMail.getDataRef(0,"SMTP_SERVER").toString();
			int nMailPort=recMail.getDataRef(0,"SMTP_PORT").toInt();
			QString strFrom=recMail.getDataRef(0,"EMAIL_ADDRESS").toString();
			QString strEmailServerUser=recMail.getDataRef(0,"USERNAME").toString();
			QString strEmailServerPass=recMail.getDataRef(0,"PASSWORD").toString();

			QString strParameters_2="?server_id="+QUrl::toPercentEncoding(strAppServerID);
			strParameters_2+="&user="+QUrl::toPercentEncoding(strUserName)+"&pass="+QUrl::toPercentEncoding(strUserPass.toLatin1().toBase64());

			QString strBodyForSend=strBody;
			if (nSize>1)
				strBodyForSend = "<p>"+strSalut +"<br>" +strBody+"</p>";  

			QString strBodyHtml=strBodyForSend;

//#ifdef __APPLE__
			DataHelper::ReencodeStringToUnicode(strBodyForSend,strBodyHtml);
//#endif

			QString strDataMailToSend = strDataMail;
			strDataMailToSend.replace("[From_Email_Address]",strFrom);
			strDataMailToSend.replace("[To_Email_Address]",strUserEmail);
			strDataMailToSend.replace("[Subject]",strSubject);
			strDataMailToSend.replace("[Intro_Text]",strBodyPlainText);
			strDataMailToSend.replace("[Intro_Text_HTML]",strBodyHtml);
			strDataMailToSend.replace("[Invitation_URL]",strLink);
			strDataMailToSend.replace("[Login_Code]",strParameters_2);
			strDataMailToSend.replace("[Login_Code_HTML]",strIPhoneInvite);
			strDataMailToSend.replace("[Login_Code_Plain]",strIPhonePlain);
			strDataMailToSend.replace("[Catalog_Name]",strCatalogName);
			strDataMailToSend.replace("[Catalog_Pic_Base64]",strCatalogBase64Pic);
			strDataMailToSend.replace("[Cat_Pic_Width]",strWidth);
			strDataMailToSend.replace("[Cat_Pic_Height]",strHeight);


			//----------------------------------------LOAD FILE ONE BY ONE, ZIP, SET MIME HEADER----------------------------------------
			//
			//----------------------------------------LOAD FILE ONE BY ONE, ZIP, SET MIME HEADER----------------------------------------

			int nCntOfAttachments=lstAttachments.size();
			for (int k=0;k<nCntOfAttachments;k++)
			{
				QString strFile=strCatPath+"/"+lstAttachments.at(k);
				if (bZipAttachment)
				{
					QFileInfo file_info(strCatPath+"/"+lstAttachments.at(k));
					QStringList lst;
					lst.append(strCatPath+"/"+lstAttachments.at(k));
					strFile = QDir::tempPath()+"/"+file_info.completeBaseName()+".zip";
					if (QFile::exists(strFile))
						QFile::remove(strFile);
					if(!ZipCompression::ZipFiles(strFile,lst))
						continue;
				}

				QFileInfo info(strFile);
				QHttpRequestHeader header;
				HTTPUtil::SetHeaderDataBasedOnFile(&header,strFile);
				QString strContentType=header.value("Content-Type");

				QFile file(strFile);
				if(file.open(QIODevice::ReadOnly))
				{
					strDataMailToSend.append("\n--everDOCs--183964186");
					strDataMailToSend.append("\nContent-Disposition: attachment;\r\n filename*=[Attachment_Filename]");
					strDataMailToSend.append("\nContent-Type: [Attachment_Type];\r\n name=\"[Attachment_Name]\"");
					strDataMailToSend.append("\nContent-Transfer-Encoding: base64");
					strDataMailToSend.append("\n\n[Attachment_Data]");

					//on APPLE does not work with umlaut
					QString str1,str2;
					DataHelper::ReencodeStringToUnicodeHex(info.fileName(),str1);
					DataHelper::ReencodeStringToUnicodeHexMime(info.fileName(),str2);
					str1="iso-8859-1''"+str1;
					
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Email Handler name of url encoded filename: %1").arg(str1));
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Email Handler name of to unicode encoded filename: %1").arg(str2));

					strDataMailToSend.replace("[Attachment_Filename]",str1);
					strDataMailToSend.replace("[Attachment_Name]",str2);

					strDataMailToSend.replace("[Attachment_Type]",strContentType);

					QByteArray byteData=file.readAll().toBase64();
					QString strDataEncoded = DataHelper::EncodeBase64To73LineLength(byteData);

					if (bZipAttachment)
					{
						file.close();
						file.remove(strFile);
					}

					strDataMailToSend.replace("[Attachment_Data]",strDataEncoded);
				}
			}


			strDataMailToSend.append("\n--everDOCs--183964186--");


			int nID=ScheduleSendMail(strMailServer,strFrom,strUserEmail,strSubject,strDataMailToSend,lstBcc,strEmailServerUser,strEmailServerPass,true,true,nMailPort);
			lstReturnedEmailTasksID.addRow();
			lstReturnedEmailTasksID.setData(lstReturnedEmailTasksID.getRowCount()-1,0,nID);
		}


}