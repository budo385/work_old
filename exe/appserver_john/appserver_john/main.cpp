
#include <QFileInfo>
#include "common/common/config_version_john.h"
#include "appserverservice.h"
#include "common/common/datahelper.h"

int main(int argc, char *argv[])
{
	QCoreApplication::setOrganizationName("Helix Business Soft");
	QCoreApplication::setOrganizationDomain("everdocs.com");
	QCoreApplication::setApplicationName("EverDOCs Bridge");

	// service application
	// Command line params:
	// -i  -install		Install the service.
	// -u  -uninstall   Uninstall the service.
	// -e  -exec
	//      Execute the service as a standalone application (useful for debug purposes).
	//      This is blocking call, service will be executed like normal application.
	//      In this mode you will not be able to communicate with the service from the contoller.
	// -t  -terminate   Stop the service.
	// -p  -pause       Pause the service.
	// -r  -resume      Resume a paused service.
	// -c {cmd} -command {cmd}
	//     Send the user defined command code {cmd} to the service application.
	// -v -version      Display version and status information.

	//take arg2 as additional service server descriptor: must have if more then ONE service is installed on same machine...


	//user & pass: service will be always automatic
	//if 
	bool bAutoStart=false;
	QString strApplicationServerName=SERVER_JOHN_SERVICE_NAME;  //app server will have same name as executable


	//if AUTO then must be on end of list of arguments
	//if descriptor then must be last or before auto
	if (QString(argv[argc-1]).trimmed().toLower()==QString("auto"))
	{
		bAutoStart=true;
		argc--;
	}
	//if 3 or 5 then last is descriptor, two others as user & pass
	QString strBaseName="";
	QString strUser="";
	QString strPass="";

	if (argc == 3)
	{
		strBaseName=argv[2];
	}
	else if (argc == 4)
	{
		strUser=argv[2];
		strPass=argv[3];
	}
	else if (argc == 5)
	{

		strUser=argv[2];
		strPass=argv[3];
		strBaseName=argv[4];
	}
	if (!strBaseName.trimmed().isEmpty())
	{
		strApplicationServerName+=" - ";
		strApplicationServerName+=strBaseName;
		argc--;
	}


	AppServerService service(argc, argv,strApplicationServerName,bAutoStart);

	return service.exec();
}
