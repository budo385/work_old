#include "servicehandler.h"
#include "common/common/datahelper.h"
#include "filecruncher.h"
#include "common/common/userstoragehelper.h"
#include "common/common/config_version_john.h"
#include "common/common/authenticator.h"
#include "emailhandler.h"
#include "common_constants.h"
#include "common/common/zipcompression.h"


#include "common/common/logger.h"
extern Logger							g_Logger;
#include "usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "settingsmanager.h"
extern SettingsManager	*g_SettingsManager;
#include "applicationserver.h"
extern ApplicationServer				*g_AppServer;
#include "emailsendermanager.h"
extern EmailSenderManager				*g_pEmailSenderManager;


ServiceHandler::ServiceHandler()
{

}

//strDirectory: alias+/subdir
//RetData: returns first subfolders by abc then files by sort order..
//nSortOrder:0 by name, 1 by ext
//if strAliasPath is empty then get 1st or all on which he has right
//nSortOrder -ignored!!!
void ServiceHandler::ReadCatalog(Status &pStatus, int &Ret_nTotalCount,int &Ret_nTotalCount_SubFolders, DbRecordSet &Ret_Files, DbRecordSet &Ret_SubFolders, QString strAliasDirectory,int nFromN,int nToN, int nFromN_SubFolder,int nToN_SubFolder, int nSortOrder)
{
	QString strAlias;
	QString strAllowedFileTypes;
	bool bIsDrive;
	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);
	QString	strDirPath = FileCruncher::ResolveCatalogPath(lstFolders, strAliasDirectory, strAlias, bIsDrive);
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("ReadCatalog (%1), resolved to path (%2)").arg(strAliasDirectory).arg(strDirPath));
	if (strDirPath.isEmpty())
	{
		pStatus.setError(1,"Path could not be resolved. If accessing remote or removable drive, please make sure that drive is accessible!");
		return;
	}

/*
	QDir test("//helix-00/development/");
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("//helix-00/development/ files cnt (%1)").arg(test.entryInfoList().size()));
	QDir test1("//helix-00/development");
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("//helix-00/development files cnt (%1)").arg(test1.entryInfoList().size()));
*/

	//get check box: show all dirs:
	DbRecordSet recSettings;
	g_SettingsManager->GetGeneralSettings(recSettings);
	bool bShowAllDrives=false;
	bool bShowOnlyDocuments=false;
	if(recSettings.getRowCount()>0)
	{
		bShowAllDrives=recSettings.getDataRef(0,"SHOW_ALL_DRIVES").toBool();
		bShowOnlyDocuments=recSettings.getDataRef(0,"SHOW_ONLY_DOCUMENTS").toBool();
	}

	if (bShowOnlyDocuments)
		strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_OFFICE);
	else
		strAllowedFileTypes="";

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetFilesFromFolder START, path (%1)").arg(strDirPath));
	FileCruncher::GetFilesFromFolder(strDirPath,Ret_Files, Ret_SubFolders,strAllowedFileTypes);
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetFilesFromFolder STOP, path (%1)").arg(strDirPath));

	Ret_Files.addColumn(QVariant::String,"DOC_LINK");
	Ret_Files.addColumn(QVariant::String,"DISPLAYNAME"); 
	Ret_nTotalCount=Ret_Files.getRowCount();


	//store cnt into list (only for root catalogs)
	//g_SettingsManager->SetCntData(strDirPath,Ret_nTotalCount); //for allothers store by ....
	g_SettingsManager->SetCntData(strAliasDirectory,Ret_nTotalCount); //for 1st level it's usefull to store by alias so 1st screen always gets

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Files detected: (%1)").arg(Ret_nTotalCount));
	

	//MB: if >300 then auto chunk first and hope that client will detect other
	/*
	if (Ret_nTotalCount>300 && nFromN==0 && nToN==0)
	{
		nToN=100;
	}
	*/

	//DataHelper::GetPartOfRecordSet(Ret_Files,nFromN,nToN);


	//-----------------------------------------
	//			FILES -> determine link for download
	//-----------------------------------------
	//qDebug()<<"Read Start Thumb Creation if already not";
	//QUrl url(strAliasDirectory);
	//QString strAliasDirEnc=QUrl::toPercentEncoding(strAliasDirectory);

	Ret_Files.addColumn(QVariant::String,"DATE_STRING");
	Ret_Files.addColumn(QVariant::String,"SIZE_STRING");
	int nSize=Ret_Files.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strFileName=Ret_Files.getDataRef(i,"FILENAME").toString();
		Ret_Files.setData(i,"DISPLAYNAME",strFileName); 
		//strFileName=QUrl::toPercentEncoding(strFileName);
		QString strLink=QString(CATALOG_STORAGE_PREFIX)+"/"+strAliasDirectory+"/"+strFileName;
		strLink=QUrl::toPercentEncoding(strLink); //encode content, but do not encode slashes...khmmm
		strLink.replace("%2F","/");
		Ret_Files.setData(i,"DOC_LINK",strLink);

		
		Ret_Files.setData(i,"DATE_STRING",Ret_Files.getDataRef(i,"LAST_MODIFY").toDateTime().toString(Qt::SystemLocaleLongDate));
		qulonglong nSize=Ret_Files.getDataRef(i,"SIZE").toULongLong();
		QString strSize = DataHelper::GetFormatedFileSize(nSize,1);
		Ret_Files.setData(i,"SIZE_STRING",strSize);
		Ret_Files.setData(i,"EXT",Ret_Files.getDataRef(i,"EXT").toString().toLower());
	}



	//-----------------------------------------
	//			SUBFOLDERS -> calc counts
	//-----------------------------------------
	//get sub count for each directory;
	Ret_SubFolders.addColumn(QVariant::String,"ALIAS");
	Ret_SubFolders.addColumn(QVariant::Int,"STATUS");
	Ret_SubFolders.addColumn(QVariant::Int,"PIC_CNT");
	Ret_SubFolders.addColumn(QVariant::Int,"CNT");
	Ret_SubFolders.addColumn(QVariant::Int,"CNT_FILES");
	Ret_SubFolders.addColumn(QVariant::Int,"CNT_SUBDIRS");
	Ret_SubFolders.addColumn(QVariant::Int,"TYPE_DRIVE");
	Ret_SubFolders.setColValue("TYPE_DRIVE",JOHN_DRIVE_FIXED);

	Ret_nTotalCount_SubFolders=Ret_SubFolders.getRowCount();
	for (int i=0;i<Ret_nTotalCount_SubFolders;i++)
	{
		QString strAlias=strAliasDirectory+"/"+Ret_SubFolders.getDataRef(i,"NAME").toString();
		Ret_SubFolders.setData(i,"ALIAS",strAlias);

		int nCnt = g_SettingsManager->GetCntData(strAlias); 

		Ret_SubFolders.setData(i,"CNT",nCnt);
		Ret_SubFolders.setData(i,"CNT_FILES",nCnt);
		Ret_SubFolders.setData(i,"PIC_CNT",nCnt);
	}

	//MB said only counts for root folders
	//RecalculateCounts(Ret_SubFolders,true);

	Ret_Files.removeColumn(Ret_Files.getColumnIdx("PATH"));
	Ret_SubFolders.removeColumn(Ret_SubFolders.getColumnIdx("PATH"));


	if (nFromN_SubFolder>=Ret_nTotalCount_SubFolders)
	{
		Ret_SubFolders.clear();
	}
	DataHelper::GetPartOfRecordSet(Ret_SubFolders,nFromN_SubFolder,nToN_SubFolder);

	//test limit: if above total cnt then ripp off files until subfolders are left, if all subfolders then ripp all files off

	//algorithm: from,to  by 10 (e.g. 24 files, 3 subfolders)
	//0-9: sub=3, files=7 (7)
	//10-19: sub=0, files=8-17 (10)
	//20-29: sub=0, files=18-23 (6)

	//(sub=13, file=24)
	//0-9: sub=0-9, file=0
	//10-19:sub=10-12,file=0,6
	//20-29:sub=0, file=7,16
	//30-39:sub=0, file=17-23

	//ignore subfolders
	if (nFromN_SubFolder == -1 && nToN_SubFolder==-1)
	{
		Ret_SubFolders.clear();
		DataHelper::GetPartOfRecordSet(Ret_Files,nFromN,nToN);
	}
	else if (nFromN!=-1 && nToN!=-1)
	{
		int nFrom_2=nFromN-Ret_nTotalCount_SubFolders;
		if (nFrom_2<0)
			nFrom_2=0;

		int nTo_2=nToN-Ret_nTotalCount_SubFolders;
		if (nTo_2<0)
			nTo_2=0;

		//check boundary on last:


		if (nFrom_2==0 && nTo_2==0)
			Ret_Files.clear();
		else
			DataHelper::GetPartOfRecordSet(Ret_Files,nFrom_2,nTo_2);

	}

	//Ret_SubFolders.Dump();
	//Ret_Files.Dump();


	if (nSortOrder==0)
		Ret_Files.sort("NAME");
	else
		Ret_Files.sort("EXT");
}



//based on strFileName + heading + sort order, it will return 1 record: current, prev or next
//nHeading=0, current, -1 prev, +1 next
//nSortOrder:0 by name, 1 by ext
//if nPicWidth nad height are supported it will be fast else..
void ServiceHandler::GetPicture(Status &pStatus, int &Ret_nTotalCount, int &Ret_nCurrentPos, DbRecordSet &Ret_Files, QString strAliasDirectory, QString strFileName, int nHeading, int nSortOrder, int nPicWidth,int nPicHeight, bool bKeepAspectRatio, bool bReturnThumbAsBinary)
{
	/*


	//get all list, get next/prev and current pos
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strAliasDirectory,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;

	//if pph then limit file types only to pics:
	int nProgCode=g_UserSessionManager->GetUserProgCode();
	if (nProgCode==CLIENT_CODE_PFH_LITE || nProgCode==CLIENT_CODE_PFH_PRO)
	{
		strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_PIC);
	}

	DbRecordSet lstSubDirs;
	if (strPhotoAlbumID.isEmpty())
		FileCruncher::GetFilesFromFolder(strDirPath,Ret_Files, lstSubDirs,strAllowedFileTypes);
	else
		FileCruncher_iPhoto::GetFilesFromFolder(strPhotoAlbumID,Ret_Files, lstSubDirs,strAllowedFileTypes);

	Ret_nTotalCount=Ret_Files.getRowCount();
	if (nSortOrder==0)
		Ret_Files.sort("NAME");
	else
		Ret_Files.sort("EXT");

	Ret_nCurrentPos=Ret_Files.find("FILENAME",strFileName,true);
	if (Ret_nCurrentPos<0)
	{
		pStatus.setError(1,"File not found in the catalog!");
		return;
	}

	//calc position, check boundaries:
	Ret_nCurrentPos=Ret_nCurrentPos+nHeading;
	if (Ret_nCurrentPos<0 || Ret_nCurrentPos>=Ret_nTotalCount)
	{
		pStatus.setError(1,"Position is out of range!");
		return;
	}

	Ret_Files.selectRow(Ret_nCurrentPos);
	Ret_Files.deleteUnSelectedRows();
	Ret_Files.addColumn(QVariant::String,"THUMB_LINK");
	Ret_Files.addColumn(QVariant::String,"DOC_LINK");
	Ret_Files.addColumn(QVariant::ByteArray,"THUMB_BINARY");
	Ret_Files.addColumn(QVariant::Int,"THUMB_WIDTH");
	Ret_Files.addColumn(QVariant::Int,"THUMB_HEIGHT");
	Ret_nCurrentPos++; //so he can show 1/123 or that...

	QString strThumbSuffix=g_ThumbManager->GetThumbSuffix(nPicWidth,nPicHeight);

	//get filename+path+last modif of original
	QString strOriginalFileName=Ret_Files.getDataRef(0,"PATH").toString();
	QFileInfo fileinfo_Original(strOriginalFileName);
	QDateTime datOriginal=fileinfo_Original.lastModified();

	//get last modif of the thumb file->check if supported by thumb manager then compare last modif if not supported look into user_Storage
	if(g_ThumbManager->IsSupporttedThumbType(nPicWidth,nPicHeight,bKeepAspectRatio))
	{
		QUrl url(strAliasDirectory);
		QString strAliasDirEnc=url.encodedPath();

		QString strFileName=Ret_Files.getDataRef(0,"FILENAME").toString();
		strFileName=QUrl::toPercentEncoding(strFileName);
		QString strThumbFileName=Ret_Files.getDataRef(0,"NAME").toString()+strThumbSuffix+".jpg";
		strThumbFileName=QUrl::toPercentEncoding(strThumbFileName);

		QString strThumbLink=QString(THUMB_CACHE_PREFIX)+"/"+strAliasDirEnc+"/"+strThumbFileName;
		QString strLink=QString(CATALOG_STORAGE_PREFIX)+"/"+strAliasDirEnc+"/"+strFileName;

		Ret_Files.setData(0,"THUMB_LINK",strThumbLink);
		Ret_Files.setData(0,"DOC_LINK",strLink);

		QString strThumbFilePath=g_ThumbManager->GetThumbDirRoot()+"/"+strAliasDirectory+"/"+Ret_Files.getDataRef(0,"NAME").toString()+strThumbSuffix+".jpg";
		QFileInfo fileinfo_Picture(strThumbFilePath);
		QDateTime datPicture=fileinfo_Picture.lastModified();

		if (datPicture<datOriginal || !fileinfo_Picture.exists())
		{
			g_ThumbManager->CreateThumbsFromAlias(strAliasDirectory);
		}

		if (!QFile::exists(strThumbFilePath))
		{
			Ret_Files.setData(0,"THUMB_LINK","");
			Ret_Files.setData(0,"DOC_LINK","");
		}
		else if (bReturnThumbAsBinary)
		{
			//MP: load binary
			QFile in_file(strThumbFilePath);
			if(in_file.open(QIODevice::ReadOnly))
			{
				QByteArray buffer=in_file.readAll();
				Ret_Files.setData(0,"THUMB_BINARY",buffer);
				//retrieve height and width:
				int nWidth,nHeight;
				if(FileCruncher::GetJpegSize((unsigned char *)buffer.data(), buffer.size(),&nWidth,&nHeight))
				{
					Ret_Files.setData(0,"THUMB_WIDTH",nWidth);
					Ret_Files.setData(0,"THUMB_HEIGHT",nHeight);
				}
			}
		}
	}
	else
	{
		QString session=g_UserSessionManager->GetSessionID();
		QString strEncodedUserSubDir=DataHelper::EncodeSession2Directory(session);
		QString strUserSubDir=g_UserSessionManager->InitializeUserStorageDirectory(pStatus,strEncodedUserSubDir);

		QUrl url(strAliasDirectory);
		QString strAliasDirEnc=url.encodedPath();

		QString strFileName=Ret_Files.getDataRef(0,"FILENAME").toString();
		strFileName=QUrl::toPercentEncoding(strFileName);
		QString strThumbFileName=Ret_Files.getDataRef(0,"NAME").toString()+strThumbSuffix+".jpg";
		strThumbFileName=QUrl::toPercentEncoding(strThumbFileName);

		QString strThumbLink=QString(USER_STORAGE_PREFIX)+"/"+strEncodedUserSubDir+"/"+strThumbFileName;
		QString strLink=QString(CATALOG_STORAGE_PREFIX)+"/"+strAliasDirEnc+"/"+strFileName;

		Ret_Files.setData(0,"THUMB_LINK",strThumbLink);
		Ret_Files.setData(0,"DOC_LINK",strLink);

		QString strThumbFilePath=strUserSubDir+"/"+Ret_Files.getDataRef(0,"NAME").toString()+strThumbSuffix+".jpg";
		QFileInfo fileinfo_Picture(strThumbFilePath);
		QDateTime datPicture=fileinfo_Picture.lastModified();

		//try to create thumb:
		if (datPicture<datOriginal || !fileinfo_Picture.exists())
		{
			FileCruncher::CreateThumbsFromFiles(Ret_Files,strUserSubDir,strThumbSuffix,nPicWidth,nPicHeight,bKeepAspectRatio);
		}

		if (!QFile::exists(strThumbFilePath))
		{
			Ret_Files.setData(0,"THUMB_LINK","");
			Ret_Files.setData(0,"DOC_LINK","");
		}
		else if (bReturnThumbAsBinary)
		{
			//MP: load binary
			QFile in_file(strThumbFilePath);
			if(in_file.open(QIODevice::ReadOnly))
			{
				QByteArray buffer=in_file.readAll();
				Ret_Files.setData(0,"THUMB_BINARY",buffer);
				int nWidth,nHeight;
				if(FileCruncher::GetJpegSize((unsigned char *)buffer.data(), buffer.size(),&nWidth,&nHeight))
				{
					Ret_Files.setData(0,"THUMB_WIDTH",nWidth);
					Ret_Files.setData(0,"THUMB_HEIGHT",nHeight);
				}
			}
		}
	}


	Ret_Files.removeColumn(Ret_Files.getColumnIdx("PATH"));
	*/
}


void ServiceHandler::RotatePicture(Status &pStatus, DbRecordSet &Ret_Files, QString strAliasDirectory, QString strFileName, int nAngle,int nPicWidth,int nPicHeight, bool bKeepAspectRatio, bool bReturnThumbAsBinary)
{
	/*
	g_ThumbManager->RotateThumb(pStatus,strAliasDirectory,strFileName,nAngle);

	int Ret_nTotalCount;
	int Ret_nCurrentPos;
	GetPicture(pStatus, Ret_nTotalCount, Ret_nCurrentPos, Ret_Files, strAliasDirectory, strFileName, 0, 0, nPicWidth, nPicHeight, bKeepAspectRatio, bReturnThumbAsBinary);

	//Ret_Files.Dump();

*/




}

//RetDirs: <ALIAS,CNT,THUMB, RIGHTS>
//normal: only small thumb left
void ServiceHandler::GetCatalogs(Status &pStatus, DbRecordSet &Ret_Catalogs)
{
	GetCatalogsAll(pStatus,Ret_Catalogs);
	Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("THUMB_LARGE"));
	//Ret_Catalogs.Dump();
}


void ServiceHandler::GetCatalogPicture(Status &pStatus, QString strCatalogName, QByteArray &Ret_byteFileData, QByteArray &Ret_byteFileDataLarge, int &Ret_nTotalCount, QString &Ret_strName)
{
	Ret_byteFileData.clear();

	DbRecordSet lstDirs;
	g_SettingsManager->GetFolders(lstDirs);

	int nRow=lstDirs.find("ALIAS",strCatalogName,true);
	if (nRow>=0)
	{
		Ret_byteFileData=lstDirs.getDataRef(nRow,"THUMB").toByteArray();
		Ret_byteFileDataLarge=lstDirs.getDataRef(nRow,"THUMB_LARGE").toByteArray();
	}

	/*
	//get all list, get next/prev and current pos
	QString strAlias;
	QString strAllowedFileTypes;
	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);
	QString	strDirPath = FileCruncher::ResolveCatalogPath(lstFolders, strCatalogName, strAlias);
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetCatalogPicture (%1), resolved to path (%2)").arg(strCatalogName).arg(strDirPath));


	strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_OFFICE);

	DbRecordSet lstSubDirs;
	DbRecordSet Ret_Files;
	//if (strPhotoAlbumID.isEmpty())
	FileCruncher::GetFilesFromFolder(strDirPath,Ret_Files, lstSubDirs,strAllowedFileTypes);
	//else
	//	FileCruncher_iPhoto::GetFilesFromFolder(strPhotoAlbumID,Ret_Files, lstSubDirs,strAllowedFileTypes);

	Ret_nTotalCount=Ret_Files.getRowCount();
	*/

	Ret_strName=g_SettingsManager->GetServerName();


}

void ServiceHandler::ReloadSettings(Status &pStatus, bool bSkipReloadCatalogs)
{
	QString strSession;
	g_UserSessionManager->CreateTempRootSessionForAuthenticatedConnection(pStatus,strSession);
	if (!pStatus.IsOK())
		return;

	QString strIP=g_UserSessionManager->GetClientIP();
	if (strIP=="127.0.0.1" || strIP=="localhost")
	{
		//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Reload Settings");
		g_SettingsManager->LoadSettings();
		//g_ThumbManager->ClearHashes();
		//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Kill Hash");

		if (!bSkipReloadCatalogs)
		{
			//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Process All Emit Signal Triggered");
			//g_ThumbManager->StartProcessAll(); //schedule start
		}
	}
	else
		pStatus.setError(1,"Access not allowed");
	
	g_UserSessionManager->DeleteSession(pStatus,strSession);
}


void ServiceHandler::DeleteDocument(Status &pStatus, QString strAliasDirectory,QString strFileName)
{
	QString strAlias;
	DbRecordSet lstFolders;
	bool bIsDrive;
	g_SettingsManager->GetFolders(lstFolders);
	QString	strDirPath = FileCruncher::ResolveCatalogPath(lstFolders, strAliasDirectory, strAlias,bIsDrive);
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("DeleteDocument (%1), resolved to path (%2)").arg(strAliasDirectory).arg(strDirPath));


	QString strFilePath=strDirPath+"/"+strFileName;

	//check permission to delete:
	QString strUserName=g_UserSessionManager->GetUserNameExt();
	if(!g_SettingsManager->TestUserRight(strUserName,strAlias,SettingsManager::OP_DELETE))
	{
		pStatus.setError(1,"Insufficient access right!");
		return;
	}

	if(!QFile::remove(strFilePath))
	{
		pStatus.setError(1,"Failed to delete file!");
		return;
	}
}

void ServiceHandler::UploadDocument(Status &pStatus, QString strAliasDirectory,QString strFileName,QByteArray &byteFileData, bool bOverWrite,QString strSession)
{
	/*
	//read files from catalog actual dir
	QString strDirPath;
	QString strAlias;
	QString strAllowedFileTypes;
	int nFileTypesCode;
	QString strPhotoAlbumID;
	g_SettingsManager->GetDirectoryPath(pStatus,strAliasDirectory,strAlias,strDirPath,nFileTypesCode,strAllowedFileTypes,strPhotoAlbumID);
	if (!pStatus.IsOK())return;

	QString strFilePath=strDirPath+"/"+strFileName;

	//check permission to delete:
	QString strUserName=g_UserSessionManager->GetUserName(strSession);
	if(!g_SettingsManager->TestUserRight(strUserName,strAlias,SettingsManager::OP_UPLOAD))
	{
		pStatus.setError(1,"Insufficient access right!");
		return;
	}

	//open INI file for writing
	QFile filek(strFilePath);
	if (filek.exists() && !bOverWrite)
	{
		pStatus.setError(1,"File already exists");
		return;
	}

	if (!filek.open(QIODevice::WriteOnly))
	{
		pStatus.setError(1,"Error while trying to save file");
		return;
	}

	filek.write(byteFileData);
	filek.close();


	//MB: when upload pic try to rotate: HAHAHAHAHAHAHAAHAHAHAHAAHAHAA
	//if error ignore, this is big pile of shit anyway
	g_ThumbManager->RotateThumbByExifOrienatation(pStatus,strFilePath);
	pStatus.setError(0);


	
	int nCnt= FileCruncher::GetFileCountFromFolderFast(strDirPath);
	if (nCnt==1)
	{
		//change in settings
		DbRecordSet lstFolders;
		g_SettingsManager->GetFolders(lstFolders);

		int nRow=lstFolders.find("ALIAS",strAliasDirectory,true);
		if (nRow>=0)
		{
			QFileInfo info2(strFilePath);
			QString strFileOut=info2.absolutePath()+"/"+info2.completeBaseName()+"_75x75.jpg";
			QString strFilePath=strDirPath+"/"+strFileName;

			//gentool: create thumb 150x150 and small...
			g_ThumbManager->CreateOneThumb(pStatus,strFilePath,strFileOut,75,75);
			if (pStatus.IsOK())
			{
				//load binary
				QFile in_file(strFileOut);
				if(in_file.open(QIODevice::ReadOnly))
				{
					QByteArray buffer=in_file.readAll();
					lstFolders.setData(nRow,"THUMB",buffer);
				}
			}
			QFile::remove(strFileOut);
			strFileOut=info2.absolutePath()+"/"+info2.completeBaseName()+"_1505x150.jpg";


			//gentool: create thumb 150x150
			g_ThumbManager->CreateOneThumb(pStatus,strFilePath,strFileOut,150,150);
			if (pStatus.IsOK())
			{
				//load binary
				QFile in_file(strFileOut);
				if(in_file.open(QIODevice::ReadOnly))
				{
					QByteArray buffer=in_file.readAll();
					lstFolders.setData(nRow,"THUMB_LARGE",buffer);
				}
			}
			QFile::remove(strFileOut);


			g_SettingsManager->SetFolders(lstFolders);
			g_SettingsManager->SaveSettings();
			g_SettingsManager->SetSettingsChangedNeedReloadFlag();
			g_SettingsManager->NotifyClientSettingsChangedNeedReload();
		 }

		}

	g_ThumbManager->StartProcessAll(); 
	*/
}


//get original bytearray
void ServiceHandler::DownloadDocument(Status &pStatus, QString strFileURL,QByteArray &Ret_byteFileData, bool bIsZip)
{
	QString strDirPath;
	QString strFileName;
	QUrl url_link = QUrl(QUrl::fromPercentEncoding(strFileURL.toLatin1()));
	QString strpath=url_link.path();

	QString strAliasDir=url_link.path();

	if (strAliasDir.left(14).toLower()=="/user_storage/")
		strAliasDir=strAliasDir.mid(14);

	
	if (strAliasDir.left(CATALOG_STORAGE_PREFIX_SIZE)==CATALOG_STORAGE_PREFIX)
	{
		strAliasDir=strAliasDir.mid(CATALOG_STORAGE_PREFIX_SIZE+1);
		QFileInfo info(strAliasDir);
		strAliasDir=info.path();
		strFileName=info.fileName();

		QString strAlias;
		bool bIsDrive;
		DbRecordSet lstFolders;
		g_SettingsManager->GetFolders(lstFolders);
		//lstFolders.Dump();
		strDirPath = FileCruncher::ResolveCatalogPath(lstFolders, strAliasDir, strAlias,bIsDrive);
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("UserStorageHTTPServer_Local::GetFilePath (%1), resolved to path (%2)").arg(strAliasDir).arg(strDirPath));

	}
	else if (strAliasDir.left(USER_STORAGE_PREFIX_SIZE)==USER_STORAGE_PREFIX)
	{
		strAliasDir=strAliasDir.mid(USER_STORAGE_PREFIX_SIZE+1);
		QFileInfo info(strAliasDir);
		strAliasDir=info.path();
		strFileName=info.fileName();
		strDirPath=g_UserSessionManager->GetUserStorageDirectoryPath()+"/"+strAliasDir;
	}
	else
	{
		pStatus.setError(1,"Path can not be resolved!");
		return;
	}

	//check file:
	QFileInfo infoFile(strDirPath+"/"+strFileName);
	if (!infoFile.exists())
	{
		pStatus.setError(1,"Path can not be resolved!");
		return;
	}

	if (bIsZip)
	{
		QString strTempZip=QDir::tempPath()+"/everdocs_"+infoFile.completeBaseName()+".zip";
		if(QFile::exists(strTempZip))
			QFile::remove(strTempZip);
		QStringList lstFiles;
		lstFiles.append(strDirPath+"/"+strFileName);
		ZipCompression::ZipFiles(strTempZip,lstFiles);

		QFileInfo infoZip(strTempZip);
		//check size: (max xml binary data, set to 30Mb):
		if (infoZip.size()>30000000)
		{
			pStatus.setError(1,"Operation failed: file size is over 30Mb!");
			return;
		}

		DataHelper::LoadFile(strTempZip,Ret_byteFileData);
	}
	else
	{
		//check size: (max xml binary data, set to 30Mb):
		if (infoFile.size()>30000000)
		{
			pStatus.setError(1,"Operation failed: file size is over 30Mb!");
			return;
		}

		DataHelper::LoadFile(strDirPath+"/"+strFileName,Ret_byteFileData);
	}


}

void ServiceHandler::GetServerData(Status &pStatus, QString &Ret_strName,QByteArray &Ret_SrvPic,QString &Ret_strEmailSignature,int &nMajor,int &nMinor, int &nRev)
{
	Ret_strName=g_SettingsManager->GetServerName();
	QByteArray bytePicSmall;
	g_SettingsManager->GetServerThumb(bytePicSmall,Ret_SrvPic);

	nMajor=JOHN_PROGRAM_VERSION_MAJOR;
	nMinor=JOHN_PROGRAM_VERSION_MINOR;
	nRev=JOHN_PROGRAM_VERSION_REVISION;


	/*
	QDateTime datLastModify=g_SettingsManager->GetServerThumbLastModify();
	if (datLastModify!=datThumbLastModify)
	{
		QByteArray bytePicSmall;
		g_SettingsManager->GetServerThumb(bytePicSmall,Ret_SrvPic);
		datThumbLastModify=datLastModify;
	}
	else
	{
		Ret_SrvPic.clear();
	}
	*/

	DbRecordSet recMail;
	g_SettingsManager->GetEmailSettings(recMail);
	Ret_strEmailSignature=recMail.getDataRef(0,"SIGNATURE").toString();

}

void ServiceHandler::Logout(Status &pStatus,QString strSessionID)
{
	g_UserSessionManager->DeleteSession(pStatus,strSessionID);
}
//strProgVer = major.minor.rev
//RetOut_strSessionID -> just return param, but leave it...
void ServiceHandler::Login(Status& pStatus, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode, QString strProgVer, QString strClientID, QString strPlatform)
{
	QString strProgCode=QString::number(nProgCode);
	if (strProgCode>"1.0.10")
	{
		if (strProgCode.left(2)!= DIGIT_APP_EVERDOCS)
		{
			pStatus.setError(1,"EverDOCs Bridge reported: authentication failed, wrong type of the client tried to connect!");
			return;
		}
	}

	g_UserSessionManager->CreateSessionForWebService(pStatus,RetOut_strSessionID,strUserName,strAuthToken,strClientNonce,nProgCode,strProgVer,strClientID,strPlatform);
}

void ServiceHandler::RenameCatalog(Status &pStatus, QString strOldName, QString strNewName)
{
	QString strSession;
	g_UserSessionManager->CreateTempRootSessionForAuthenticatedConnection(pStatus,strSession);
	if (!pStatus.IsOK())
		return;

	QString strIP=g_UserSessionManager->GetClientIP();
	if (strIP=="127.0.0.1" || strIP=="localhost")
	{
		g_SettingsManager->LoadSettings();

		g_SettingsManager->RenameCatalogInShadowList(strOldName,strNewName);
	}
	else
		pStatus.setError(1,"Access not allowed");

	Status err;
	g_UserSessionManager->DeleteSession(err,strSession);
}

void ServiceHandler::RenameCatalogClient(Status &pStatus, QString strOldName, QString strNewName)
{
	//test if valid name
	if (!FileCruncher::IsNameValidAsDirectory(strNewName))
	{
		pStatus.setError(1,QString("Catalog can not be renamed. Catalog name %1 is not valid. Catalog name can't contain characters: *.\"/\\[]:;|=,").arg(strNewName));
		return;
	}


	//change in settings
	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);
	int nSize=lstFolders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (lstFolders.getDataRef(i,"ALIAS").toString()==strOldName)
			lstFolders.setData(i,"ALIAS",strNewName);
	}

	//user rights:
	DbRecordSet lstUserRights;
	g_SettingsManager->GetUserRights(lstUserRights);
	nSize=lstUserRights.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (lstUserRights.getDataRef(i,"ALIAS").toString()==strOldName)
			lstUserRights.setData(i,"ALIAS",strNewName);
	}

	RenameCatalog(pStatus,strOldName,strNewName);

	g_SettingsManager->SetFolders(lstFolders);
	g_SettingsManager->SetUserRights(lstUserRights);
	g_SettingsManager->SaveSettings();
	g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	g_SettingsManager->NotifyClientSettingsChangedNeedReload();
}


void ServiceHandler::CreateCatalogClient(Status &pStatus, QString strCatalogName)
{
	QString strCatalogDir=g_AppServer->GetIniFile()->m_strCatalogDir;

	QString strCatalogPath = strCatalogDir;
	if (!QFile::exists(strCatalogPath))
	{
		pStatus.setError(1,QString("Can not create directory for new catalog! Invalid path set in server settings: %1").arg(strCatalogDir));
	}

	if (!FileCruncher::IsNameValidAsDirectory(strCatalogName))
	{
		pStatus.setError(1,QString("Catalog can not be added: Catalog name %1 is not valid. Catalog name can't contain characters: *.\"/\\[]:;|=,").arg(strCatalogName));
		return;
	}

	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);

	if (lstFolders.find("ALIAS",strCatalogName,true)>=0)
	{
		pStatus.setError(1,QString("Catalog with name %1 already exists!").arg(strCatalogName));
		return;
	}

	QDir dir(strCatalogPath);
	if(!dir.mkdir(strCatalogName))
	{
		pStatus.setError(1,"Can not create directory for new catalog!");
		return;
	}

	int nCount=0;
	nCount=FileCruncher::GetFileCountFromFolder(strCatalogPath+"/"+strCatalogName,FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_OFFICE));
	//get all subfolders:
	QList<QString> lstSubFolders;
	FileCruncher::GetAllSubFolders(strCatalogPath+"/"+strCatalogName,lstSubFolders);
	int nSize=lstSubFolders.size();
	for (int i=0;i<nSize;i++)
	{
		nCount += FileCruncher::GetFileCountFromFolder(strCatalogPath+"/"+strCatalogName+"/"+lstSubFolders.at(i),FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_OFFICE));
	}


	lstFolders.addRow();
	int nRow=lstFolders.getRowCount()-1;
	lstFolders.setData(nRow,"ALIAS",strCatalogName);
	lstFolders.setData(nRow,"PATH",strCatalogPath+"/"+strCatalogName);
	lstFolders.setData(nRow,"FILETYPES_CODE",FileCruncher::FILE_TYPE_OFFICE);
	lstFolders.setData(nRow,"STATUS",STATUS_OK);
	lstFolders.setData(nRow,"PIC_CNT",0);
	lstFolders.setData(nRow,"IPHOTO_ID","");
	g_SettingsManager->SetFolders(lstFolders);

	g_SettingsManager->SaveSettings();
	g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	g_SettingsManager->NotifyClientSettingsChangedNeedReload();
}


void ServiceHandler::GetPictureFromCatalog(Status &pStatus,QString strAliasDirectory,QString strFileName,int nWidth, int nHeight,int nWidthP, int nHeightP,QByteArray &byteFileData)
{
	/*
	//get both landscape and potrait  take bigger:
	int Ret_nTotalCount;
	int Ret_nCurrentPos;
	DbRecordSet Ret_Files;
	GetPicture(pStatus, Ret_nTotalCount, Ret_nCurrentPos, Ret_Files, strAliasDirectory, strFileName, 0, 0, nWidth, nHeight, true, true);
	DbRecordSet Ret_Files_2;
	GetPicture(pStatus, Ret_nTotalCount, Ret_nCurrentPos, Ret_Files_2, strAliasDirectory, strFileName, 0, 0, nWidthP, nHeightP, true, true);

	if (Ret_Files.getRowCount()==0 || Ret_Files_2.getRowCount()==0)
	{
		pStatus.setError(1,"Failed to rea file");
		return;
	}

	if (Ret_Files.getDataRef(0,"THUMB_BINARY").toByteArray().size()>Ret_Files_2.getDataRef(0,"THUMB_BINARY").toByteArray().size())
		byteFileData=Ret_Files.getDataRef(0,"THUMB_BINARY").toByteArray();
	else
		byteFileData=Ret_Files_2.getDataRef(0,"THUMB_BINARY").toByteArray();
		*/

}


//new format: Ret_Folders <alias,status> -> if all ok or could not be indexed, stop progress, else continue
void ServiceHandler::GetFolderStatus(Status &pStatus, DbRecordSet &Ret_Folders,int &Ret_nSettingsChanged)
{

	QString strSession;
	g_UserSessionManager->CreateTempRootSessionForAuthenticatedConnection(pStatus,strSession);
	if (!pStatus.IsOK())
		return;

	DbRecordSet lstFolders;
	GetCatalogs(pStatus,lstFolders);

	Ret_Folders.destroy();
	Ret_Folders.addColumn(QVariant::String,"ALIAS");
	Ret_Folders.addColumn(QVariant::Int,"STATUS");
	Ret_Folders.addColumn(QVariant::Int,"PIC_CNT");
	Ret_Folders.addColumn(QVariant::Int,"CNT");

	Ret_Folders.merge(lstFolders);


	Ret_nSettingsChanged=g_SettingsManager->GetSettingsChangedNeedReloadFlag();
	g_SettingsManager->ClearSettingsChangedNeedReloadFlag();

	g_UserSessionManager->DeleteSession(pStatus,strSession);
}



void ServiceHandler::AddUser(Status &pStatus, DbRecordSet &rowUser, QString strCatalogName, QString strFirstName, QString strLastName,QString strEmail, QString strPhone)
{

	DbRecordSet lstUsers;
	g_SettingsManager->GetUsers(lstUsers);

	bool bFound=false;
	int nSize=lstUsers.getRowCount();
	for (int i=0; i<nSize;i++)
	{
		if (strFirstName.toLower().trimmed()==lstUsers.getDataRef(i,"FIRST_NAME").toString().toLower().trimmed() && strLastName.toLower().trimmed()==lstUsers.getDataRef(i,"LAST_NAME").toString().toLower().trimmed() && strEmail.toLower().trimmed()==lstUsers.getDataRef(i,"EMAIL").toString().toLower().trimmed() && strPhone.toLower().trimmed()==lstUsers.getDataRef(i,"SMS").toString().toLower().trimmed() )
		{
			rowUser=lstUsers.getRow(i);
			bFound=true;
			break;
		}
	}
	
	if (!bFound)
	{
		lstUsers.addRow();
		int nRow=nSize;

		bool bUserGenerated=false;

		//calc new username: 4left from lastname + 01 if already exists:
		QString strUserNameStart=strLastName.left(4);
		if (strUserNameStart.isEmpty())
			strUserNameStart=strFirstName.left(4);
		if (strUserNameStart.isEmpty())
			strUserNameStart="guest";

		QString strUserName=strUserNameStart;
		
		for (int i=0; i<1000;i++)
		{
			if(lstUsers.find("USERNAME",strUserName,true)<0)
			{
				bUserGenerated=true;
				break;
			}
			strUserName=strUserNameStart+QString::number(i);
		}

		if (!bUserGenerated)
		{
			pStatus.setError(1,"Username can not be generated automatically");
			return;
		}

		QString strPass=Authenticator::GenerateRandomPassword(5,true).toLower();


		lstUsers.setData(nRow,"USERNAME",strUserName);
		lstUsers.setData(nRow,"PASSWORD",strPass);
		lstUsers.setData(nRow,"FIRST_NAME",strFirstName);
		lstUsers.setData(nRow,"LAST_NAME",strLastName);
		lstUsers.setData(nRow,"EMAIL",strEmail);
		lstUsers.setData(nRow,"SMS",strPhone);
		lstUsers.setData(nRow,"COMPANY","");
		lstUsers.setData(nRow,"LAST_ACCESS_PROG_CODE","");
		lstUsers.setData(nRow,"SALUT",QString("Dear "+strFirstName));

		rowUser=lstUsers.getRow(nRow);
		g_SettingsManager->SetUsers(lstUsers);
		g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	}

	QString strUserName=rowUser.getDataRef(0,"USERNAME").toString();


	//add user to the catalog if not already if already then set read only right:

	if (!strCatalogName.isEmpty())
	{
		bool bUserRightExist=false;
		DbRecordSet lstUserRights;
		g_SettingsManager->GetUserRights(lstUserRights);
		//lstUserRights.Dump();
		int nSelected=lstUserRights.find("USERNAME",strUserName);
		if (nSelected>0)
		{
			int nSelected_x=lstUserRights.find("ALIAS",strCatalogName,false,true,true,true);
			if (nSelected_x==1)
			{
				bUserRightExist=true;
				int nRow=lstUserRights.getSelectedRow();

				if (lstUserRights.getDataRef(nRow,"CAN_DOWNLOAD").toInt()==0)
				{
					lstUserRights.setData(nRow,"CAN_DOWNLOAD",1);
					g_SettingsManager->SetUserRights(lstUserRights);
					g_SettingsManager->SetSettingsChangedNeedReloadFlag();
				}
			}
		}

		if (!bUserRightExist)
		{
			lstUserRights.addRow();
			int nRow=lstUserRights.getRowCount()-1;
			lstUserRights.setData(nRow,"USERNAME",strUserName);
			lstUserRights.setData(nRow,"ALIAS",strCatalogName);
			lstUserRights.setData(nRow,"CAN_DOWNLOAD",1);
			lstUserRights.setData(nRow,"CAN_UPLOAD",0);
			lstUserRights.setData(nRow,"CAN_DELETE",0);
			lstUserRights.setData(nRow,"CAN_SEND_INVITE",0);
			lstUserRights.setData(nRow,"CAN_SEND_EMAIL",0);
			g_SettingsManager->SetUserRights(lstUserRights);
			g_SettingsManager->SetSettingsChangedNeedReloadFlag();
		}

	}




}

void ServiceHandler::MakeInvitation(Status &pStatus, QString &Ret_strBody, int nMessageType,QString strCatalogName, QString strFirstName, QString strLastName,QString strEmail, QString strPhone,int nIsHtml,QString strEmailSubject,QString strEmailBody,DbRecordSet &FileNames,bool bZip,int &Ret_ID)
{
	Ret_ID=-1;

	//QTime time;
	//time.start();


	if (strFirstName.toLower()=="null")
		strFirstName="";
	if (strLastName.toLower()=="null")
		strLastName="";

	if (strFirstName.toLower()=="undefined")
		strFirstName="";
	if (strLastName.toLower()=="undefined")
		strLastName="";

	//MB want to make invite on subfolders, so....be carefull
	QString strNewCatalog="";
	QString strNewCatalogPath="";

	DbRecordSet lstCatalogs;
	g_SettingsManager->GetFolders(lstCatalogs);

	//get catalog and email settings:
	bool bEmailSettingsSet=false;
	DbRecordSet recMail;
	g_SettingsManager->GetEmailSettings(recMail);
	if(recMail.getRowCount()>0)
		if (!recMail.getDataRef(0,"SMTP_SERVER").toString().isEmpty())
			bEmailSettingsSet=true;

	if (!bEmailSettingsSet)
	{
		pStatus.setError(0,"Email settings are not set. Go to the everDocs-Bridge application and in the settings page define Email settings!");
		return;
	}

	QString strFrom=recMail.getDataRef(0,"EMAIL_ADDRESS").toString();


	DbRecordSet rowCatalog;
	DbRecordSet rowUser;
	QString strUserName;
	QString strUserPass;
	QString strServerID;

	QString strOldCatalogName=strCatalogName;

	QString strAlias;
	DbRecordSet lstFolders;
	bool bIsDrive;
	g_SettingsManager->GetFolders(lstFolders);
	QString	strDirPath = FileCruncher::ResolveCatalogPath(lstFolders, strCatalogName, strAlias,bIsDrive);
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("MakeInvitation (%1), resolved to path (%2)").arg(strCatalogName).arg(strDirPath));


	if (bIsDrive)
	{
		strCatalogName=""; //reset catalog name: only set for invite

		//create bcatalog = path of invite (invite is when attachments=0
		if(FileNames.getRowCount()==0)
		{
			int nX=lstCatalogs.find("PATH",strDirPath,true);
			if (nX>=0)
			{
				strCatalogName=lstCatalogs.getDataRef(nX,"ALIAS").toString();
			}
			else
			{
				QStringList lstParts=strDirPath.split("/");
				strNewCatalog = lstParts.at(lstParts.size()-1);
				CreateNewCatalogFromExistingDirectory(pStatus,strDirPath,strNewCatalog);
				if (!pStatus.IsOK()) return;
				strCatalogName=strNewCatalog;
			}
		}


		AddUser(pStatus,rowUser,strCatalogName,strFirstName,strLastName,strEmail,strPhone);
		if (!pStatus.IsOK()) return;

		if (g_SettingsManager->GetSettingsChangedNeedReloadFlag())
		{
			g_SettingsManager->SaveSettings();
			g_SettingsManager->NotifyClientSettingsChangedNeedReload();
		}

		//based on message type generate invitation body:
		strUserName=rowUser.getDataRef(0,"USERNAME").toString();
		strUserPass=rowUser.getDataRef(0,"PASSWORD").toString();
		strServerID=g_AppServer->GetIniFile()->m_strAppServerID;

		rowCatalog.copyDefinition(lstCatalogs);
		rowCatalog.addRow();
		int nRow=0;
		if (strCatalogName.isEmpty())
			rowCatalog.setData(nRow,"ALIAS",strAlias);
		else
			rowCatalog.setData(nRow,"ALIAS",strCatalogName);


		rowCatalog.setData(nRow,"PATH",strDirPath);
		int nCount=0;
		rowCatalog.setData(nRow,"FILETYPES_CODE",FileCruncher::FILE_TYPE_OFFICE);
		rowCatalog.setData(nRow,"STATUS",STATUS_OK);
		rowCatalog.setData(nRow,"PIC_CNT",nCount);

		//if ordinary invite then set new catalog name in subject
		if (!strCatalogName.isEmpty() && FileNames.getRowCount()==0)
		{
			strEmailBody.replace(strOldCatalogName,strCatalogName);
			strEmailSubject.replace(strOldCatalogName,strCatalogName);
		}

	}
	else
	{

		int nRow=lstCatalogs.find("ALIAS",strCatalogName,true);
		if (nRow<0)
		{
			//otribi: do prvog "/"
			QStringList lstParts;
			if (strCatalogName.indexOf("/")>=0)
				lstParts=strCatalogName.split("/");

			strCatalogName = lstParts.at(0);
			strNewCatalog = lstParts.at(lstParts.size()-1);


			nRow=lstCatalogs.find("ALIAS",strCatalogName,true);
			if (nRow<0)
			{
				pStatus.setError(1,"Catalog can not be found!");
				return;
			}

			lstParts.removeFirst();
			QString strPathToNewCatalog=lstParts.join("/");
			strNewCatalogPath=QDir::cleanPath(lstCatalogs.getDataRef(nRow,"PATH").toString()+"/"+strPathToNewCatalog);
		}



		//create new catalog
		if (!strNewCatalogPath.isEmpty() && !strNewCatalog.isEmpty()) 
		{
			CreateNewCatalogFromExistingDirectory(pStatus,strNewCatalogPath,strNewCatalog);
			pStatus.setError(0);
			//if (!pStatus.IsOK()) return;
			strCatalogName=strNewCatalog;
		}



		//check permission to invite:
		QString strUserNameLogged=g_UserSessionManager->GetUserNameExt();
		if(!g_SettingsManager->TestUserRight(strUserNameLogged,strCatalogName,SettingsManager::OP_SEND_INVITE))
		{
			pStatus.setError(1,"Insufficient access right!");
			return;
		}



		g_SettingsManager->GetFolders(lstCatalogs);
		nRow=lstCatalogs.find("ALIAS",strCatalogName,true);
		if (nRow<0)
		{
			pStatus.setError(1,"New catalog is not created successfully!");
			return;
		}



		AddUser(pStatus,rowUser,strCatalogName,strFirstName,strLastName,strEmail,strPhone);
		if (!pStatus.IsOK()) return;

		if (g_SettingsManager->GetSettingsChangedNeedReloadFlag())
		{
			g_SettingsManager->SaveSettings();
			g_SettingsManager->NotifyClientSettingsChangedNeedReload();
		}

		//based on message type generate invitation body:
		strUserName=rowUser.getDataRef(0,"USERNAME").toString();
		strUserPass=rowUser.getDataRef(0,"PASSWORD").toString();
		strServerID=g_AppServer->GetIniFile()->m_strAppServerID;

		rowCatalog=lstCatalogs.getRow(nRow);
	}



	

	//QString strCatalogBase64Pic = lstCatalogs.getDataRef(nRow,"THUMB_LARGE").toByteArray().toBase64();
	
	if (nMessageType==1)
	{
		if (rowUser.getDataRef(0,"EMAIL").toString().isEmpty())
		{
			pStatus.setError(1,"Invitation can not be sent as contact does not have email address!");
			return;
		}

		QStringList lstAttachments;
		if (FileNames.getRowCount()>0)
		{
			for (int i=0;i<FileNames.getRowCount();i++)
			{
				lstAttachments.append(FileNames.getDataRef(i,0).toString());
			}
		}

		//-> sent all to emailhandler:
		DbRecordSet lstReturnedEmailTasksID;
		g_pEmailSenderManager->SendEmail(pStatus,lstReturnedEmailTasksID,strServerID,rowCatalog,recMail,rowUser,strEmailSubject,strEmailBody,"",lstAttachments,bZip);

		if (lstReturnedEmailTasksID.getRowCount()>0)
			Ret_ID=lstReturnedEmailTasksID.getDataRef(0,0).toInt();

		//EmailHandler::SendEmail(pStatus,strServerID,rowCatalog,recMail,rowUser,strEmailSubject,strEmailBody,"",lstAttachments,bZip);

	}
	else
	{
		if (rowUser.getDataRef(0,"SMS").toString().isEmpty())
		{
			pStatus.setError(1,"Invitation can not be sent as contact does not have phone number address!");
			return;
		}
		//send mail... format as <<gdfgdgf>>
		QString strEmail="See my pics here:%1\n\r\n\r";
		strEmail+="or use the iPhone app \"EverDocs\" and paste this message into the ID field: %2\n\r";

		QString strLink;
		strLink=QString(FFH_CONNECT_SERVER_MASTER_IP)+"/invite/"+strServerID+"/web/invite.html?catalog="+QUrl::toPercentEncoding(strCatalogName);

		strLink+="&user="+QUrl::toPercentEncoding(strUserName)+"&pass="+QUrl::toPercentEncoding(strUserPass.toLatin1().toBase64());
		QString strIPhone="<<"+strServerID+"//"+strUserName+"//"+strUserPass+">>";

		//QString strSubject="PFH invitation";
		Ret_strBody=strEmail.arg(strLink).arg(strIPhone);
	}

	
	//qDebug()<<"Time elapsed for Make invite: "<<time.elapsed();
}


void ServiceHandler::CheckEmailStatus(Status &pStatus, DbRecordSet &lstEmailTasks, DbRecordSet &Ret_lstEmailTasksStatus)
{

	int nSize=lstEmailTasks.getRowCount();
	Ret_lstEmailTasksStatus.addColumn(QVariant::Int,"ID");
	Ret_lstEmailTasksStatus.addColumn(QVariant::String,"Status");
	Ret_lstEmailTasksStatus.addRow(nSize);

	for (int i=0;i<nSize;i++)
	{
		int nID=lstEmailTasks.getDataRef(i,0).toInt();
		QString strStatus;
		Ret_lstEmailTasksStatus.setData(i,"ID",nID);
		if(g_pEmailSenderManager->GetStatusForMail(nID,strStatus))
		{
			Ret_lstEmailTasksStatus.setData(i,"Status",strStatus);
		}
		else
		{
			Ret_lstEmailTasksStatus.setData(i,"Status","NOT IN LIST");
		}
	}


}

void ServiceHandler::GetCatalogsAll(Status &pStatus, DbRecordSet &Ret_Catalogs)
{
	DbRecordSet lstUserRights;
	g_SettingsManager->DefineFolders(Ret_Catalogs);
	Ret_Catalogs.addColumn(QVariant::Int,"CNT");
	
	Ret_Catalogs.addColumn(QVariant::Int,"IS_DRIVE");
	Ret_Catalogs.addColumn(QVariant::Int,"TYPE_DRIVE");
	Ret_Catalogs.addColumn(QVariant::Int,"CNT_FILES");
	Ret_Catalogs.addColumn(QVariant::Int,"CNT_SUBDIRS");

	g_SettingsManager->DefineUserRights(lstUserRights);
	Ret_Catalogs.copyDefinition(lstUserRights,false);


	//get check box: show all dirs:
	DbRecordSet recSettings;
	g_SettingsManager->GetGeneralSettings(recSettings);

	bool bShowAllDrives=false;
	bool bShowOnlyDocuments=false;
	if(recSettings.getRowCount()>0)
	{
		bShowAllDrives=recSettings.getDataRef(0,"SHOW_ALL_DRIVES").toBool();
		bShowOnlyDocuments=recSettings.getDataRef(0,"SHOW_ONLY_DOCUMENTS").toBool();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetCatalogsAll GetFolders"));

	DbRecordSet lstDirs;
	g_SettingsManager->GetFolders(lstDirs);
	lstDirs.addColumn(QVariant::Int,"CNT");
	lstDirs.addColumn(QVariant::Int,"IS_DRIVE");
	lstDirs.addColumn(QVariant::Int,"TYPE_DRIVE");
	
	lstDirs.addColumn(QVariant::Int,"CNT_FILES");
	lstDirs.addColumn(QVariant::Int,"CNT_SUBDIRS");
	
	//lstDirs.addColumn(QVariant::Int,"TYPE_DIR");
	lstDirs.setColValue("IS_DRIVE",0);
	lstDirs.setColValue("CNT_SUBDIRS",0);
	lstDirs.setColValue("CNT_FILES",0);
	//lstDirs.setColValue("PIC_CNT",0);
	lstDirs.setColValue("CNT",0);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetCatalogsAll start"));

	

	//get all drives:
	if (bShowAllDrives)
	{
		DbRecordSet lstDirsDrives;
		lstDirsDrives.copyDefinition(lstDirs);

		QStringList lstDriveNames;
		QStringList lstDriveLetters;
		QStringList lstPaths;
		
		QList<int> lstTypes;
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetCatalogsAll/FileCruncher::GetDrives START"));
		FileCruncher::GetDrives(lstDriveNames,lstDriveLetters,lstPaths,lstTypes);
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetCatalogsAll/FileCruncher::GetDrives STOP"));

		int nSizeX=lstDriveNames.size();
		for (int i=0;i<nSizeX;i++)
		{
			QString strAlias=lstDriveNames.at(i);
			QString strPath=lstPaths.at(i);
			if (strPath.toLower().left(1)=="a" || strPath.toLower().left(1)=="b")
				continue;

			//strPath.replace("\\","/");
			lstDirsDrives.addRow();
			int nRow=lstDirsDrives.getRowCount()-1;
			lstDirsDrives.setData(nRow,"ALIAS",strAlias);
			lstDirsDrives.setData(nRow,"PATH",strPath);
			lstDirsDrives.setData(nRow,"FILETYPES_CODE",FileCruncher::FILE_TYPE_OFFICE);
			
			lstDirsDrives.setData(nRow,"IS_DRIVE",1);
			lstDirsDrives.setData(nRow,"TYPE_DRIVE",lstTypes.at(i));

			int nCount=0;

			//lstDirs.setData(nRow,"FILETYPES_CODE",FileCruncher::FILE_TYPE_OFFICE);
			lstDirsDrives.setData(nRow,"STATUS",STATUS_OK);
			lstDirsDrives.setData(nRow,"PIC_CNT",nCount);
			lstDirsDrives.setData(nRow,"CNT",nCount);
			lstDirsDrives.sort("PATH");



		}

		lstDirs.sort("ALIAS");
		lstDirsDrives.sort("ALIAS");

		lstDirs.merge(lstDirsDrives);
	}


	
	//instead of calc count, retrieve em from file
	int nSize=lstDirs.getRowCount();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetCatalogsAll count of drives: %1").arg(nSize));

	for (int i=0;i<nSize;i++)
	{
		QString strPath=lstDirs.getDataRef(i,"ALIAS").toString();
		//get cnt from list:
		int nCnt = g_SettingsManager->GetCntData(strPath);

		if (nCnt==0) //if count = 0 use stored count
			nCnt=lstDirs.getDataRef(i,"PIC_CNT").toInt();

		lstDirs.setData(i,"PIC_CNT",nCnt);
		lstDirs.setData(i,"CNT_FILES",nCnt);
		lstDirs.setData(i,"CNT",nCnt);
	}

	//sort: first catalogs then drives:

	///lstDirs.sort("ALIAS");
	//lstDirs.Dump();

	//Ret_Catalogs.sort("ALIAS");
	//Ret_Catalogs.Dump();


	//get all rights for users (if root) then by default it has all:
	g_SettingsManager->GetUserRights(lstUserRights);
	lstUserRights.find("USERNAME",g_UserSessionManager->GetUserNameExt());
	lstUserRights.deleteUnSelectedRows();
	lstUserRights.removeColumn(lstUserRights.getColumnIdx("USERNAME"));

	Ret_Catalogs.merge(lstDirs);
	Ret_Catalogs.clearSelection();


	nSize=Ret_Catalogs.getRowCount();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetCatalogsAll count of catalogs: %1").arg(nSize));

	if(g_UserSessionManager->GetUserNameExt()==g_SettingsManager->GetAdminUserName())
	{
		//get default user rights:
		DbRecordSet rowDefRights;
		g_SettingsManager->CreateDefaultUserRights(rowDefRights);
		rowDefRights.removeColumn(rowDefRights.getColumnIdx("ALIAS"));
		rowDefRights.removeColumn(rowDefRights.getColumnIdx("USERNAME"));

		//for each directory: find 1 right, if not exists then set all true:
		for (int i=0;i<nSize;i++)
		{
			Ret_Catalogs.assignRow(i,rowDefRights,true);
		}
	}
	else
	{
		//for each directory: find 1 right, if not exists then set all true:
		for (int i=0;i<nSize;i++)
		{
			int nRow=lstUserRights.find("ALIAS",Ret_Catalogs.getDataRef(i,"ALIAS").toString(),true);
			if (nRow>=0)
			{
				DbRecordSet rowTmp = lstUserRights.getRow(nRow);
				Ret_Catalogs.assignRow(i, rowTmp, true);
			}
			else
			{
				Ret_Catalogs.selectRow(i);
			}
		}
	}
	Ret_Catalogs.deleteSelectedRows(); //user does not have access to other catalogs if not defined


	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Get Catalog Step 2");

	//Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("PATH"));
	//Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("FILETYPES"));
	Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("FILETYPES_CODE"));
	Ret_Catalogs.removeColumn(Ret_Catalogs.getColumnIdx("USERNAME"));


	//SortDataList lstCols;
	//lstCols.append(SortData(Ret_Catalogs.getColumnIdx("IS_DIR")));
	//lstCols.append(SortData(Ret_Catalogs.getColumnIdx("ALIAS")));
	//Ret_Catalogs.sortMulti(lstCols);
	

	if (Ret_Catalogs.getRowCount()!=0)
	{
		if (!g_SettingsManager->TestModuleRight(g_UserSessionManager->GetUserProgCode(),SettingsManager::OP_MORE_THEN_ONE_CATALOG))
		{
			Ret_Catalogs.clearSelection();
			Ret_Catalogs.selectRow(0);
			Ret_Catalogs.deleteUnSelectedRows();
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("GetCatalogsAll done"));

	//BT: 2013-11-29: recalc counts always:
	RecalculateCounts(Ret_Catalogs,true);
	
}





void ServiceHandler::UnpublishCatalogClient(Status &pStatus, QString strAliasDirectory)
{
	//change in settings
	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);

	int nRow=lstFolders.find("ALIAS",strAliasDirectory,true);
	if (nRow<0)
	{
		pStatus.setError(1,"Catalog not found!");
		return;
	}
	else
	{
		//special: if folder is newly created then delete it:
		QString strPath=QDir::cleanPath(lstFolders.getDataRef(nRow,"PATH").toString().toLower());
		QString strPathForNewCatalogs=QDir::cleanPath(g_AppServer->GetIniFile()->m_strCatalogDir.toLower());
		if (strPath.indexOf(strPathForNewCatalogs)==0)
		{
			FileCruncher::RemoveAllFromDirectory(strPath,true,true);
		}
		lstFolders.deleteRow(nRow);
	}

	//user rights:
	DbRecordSet lstUserRights;
	g_SettingsManager->GetUserRights(lstUserRights);
	lstUserRights.find("ALIAS",strAliasDirectory);
	lstUserRights.deleteSelectedRows();

	g_SettingsManager->SetFolders(lstFolders);
	g_SettingsManager->SetUserRights(lstUserRights);
	g_SettingsManager->SaveSettings();
	g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	g_SettingsManager->NotifyClientSettingsChangedNeedReload();

}


//Ret_Catalogs must have path, pic, and pic_cnt column
void ServiceHandler::RecalculateCounts(DbRecordSet &Ret_Catalogs,bool bSkipRecursiveCnt)
{
	DbRecordSet recSettings;
	g_SettingsManager->GetGeneralSettings(recSettings);
	bool bShowOnlyDocuments=false;
	if(recSettings.getRowCount()>0)
		bShowOnlyDocuments=recSettings.getDataRef(0,"SHOW_ONLY_DOCUMENTS").toBool();

	QString strAllowedFileTypes;
	if (bShowOnlyDocuments)
		strAllowedFileTypes=FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_OFFICE);


	Ret_Catalogs.setColValue("STATUS",STATUS_OK);

	int nSizeX=Ret_Catalogs.getRowCount();
	for (int i=0;i<nSizeX;i++)
	{
		if (Ret_Catalogs.getDataRef(i,"TYPE_DRIVE").toInt()==JOHN_DRIVE_REMOTE)
			continue;
		if (Ret_Catalogs.getDataRef(i,"TYPE_DRIVE").toInt()==JOHN_DRIVE_CDROM)
			continue;

		QString strPath=Ret_Catalogs.getDataRef(i,"PATH").toString();
		int nCntFiles=FileCruncher::GetFileCountFromFolder(strPath,strAllowedFileTypes);
		Ret_Catalogs.setData(i,"CNT",nCntFiles);

		if (!bSkipRecursiveCnt)
		{
			QList<QString> lstSubs;
			FileCruncher::GetAllSubFolders(strPath,lstSubs);
			int nSizeY=lstSubs.size();
			int nTotalFileCnt=0;
			for (int k=0;k<nSizeY;k++)
			{
				nTotalFileCnt = FileCruncher::GetFileCountFromFolder(lstSubs.at(k),strAllowedFileTypes);
			}
			nTotalFileCnt +=nCntFiles; //add own files
			Ret_Catalogs.setData(i,"PIC_CNT",nTotalFileCnt); //total no of files
		}
		else
			Ret_Catalogs.setData(i,"PIC_CNT",nCntFiles); 


		//Cnt all files:
		int nCntAllFiles=FileCruncher::GetFileCountFromFolder(strPath,"")-nCntFiles;
		Ret_Catalogs.setData(i,"CNT_FILES",nCntAllFiles); 

		QDir dir(strPath);
		if (dir.exists())
		{
			QFileInfoList lstFiles=dir.entryInfoList(QDir::AllDirs);
			int nCntDirs=lstFiles.size();
			Ret_Catalogs.setData(i,"CNT_SUBDIRS",nCntDirs); 
		}

	}


}



void ServiceHandler::CreateNewCatalogFromExistingDirectory(Status &pStatus, QString strCatalogPath, QString strCatalogName)
{

	if (!FileCruncher::IsNameValidAsDirectory(strCatalogName))
	{
		pStatus.setError(1,QString("Catalog can not be added: Catalog name %1 is not valid. Catalog name can't contain characters: *.\"/\\[]:;|=,").arg(strCatalogName));
		return;
	}

	DbRecordSet lstFolders;
	g_SettingsManager->GetFolders(lstFolders);

	if (lstFolders.find("ALIAS",strCatalogName,true)>=0)
	{
		pStatus.setError(1,QString("Catalog with name %1 already exists!").arg(strCatalogName));
		return;
	}


	int nCount=0;
	nCount=FileCruncher::GetFileCountFromFolder(strCatalogPath,FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_OFFICE));
	//get all subfolders:
	QList<QString> lstSubFolders;
	FileCruncher::GetAllSubFolders(strCatalogPath,lstSubFolders);
	int nSize=lstSubFolders.size();
	for (int i=0;i<nSize;i++)
	{
		nCount += FileCruncher::GetFileCountFromFolder(strCatalogPath+"/"+lstSubFolders.at(i),FileCruncher::GetFileTypesBasedOnCode(FileCruncher::FILE_TYPE_OFFICE));
	}


	lstFolders.addRow();
	int nRow=lstFolders.getRowCount()-1;
	lstFolders.setData(nRow,"ALIAS",strCatalogName);
	lstFolders.setData(nRow,"PATH",strCatalogPath);
	lstFolders.setData(nRow,"FILETYPES_CODE",FileCruncher::FILE_TYPE_OFFICE);
	lstFolders.setData(nRow,"STATUS",STATUS_OK);
	lstFolders.setData(nRow,"PIC_CNT",0);
	lstFolders.setData(nRow,"IPHOTO_ID","");
	g_SettingsManager->SetFolders(lstFolders);

	g_SettingsManager->SaveSettings();
	g_SettingsManager->SetSettingsChangedNeedReloadFlag();
	g_SettingsManager->NotifyClientSettingsChangedNeedReload();
}


