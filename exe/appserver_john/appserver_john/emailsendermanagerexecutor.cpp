#include "emailsendermanagerexecutor.h"


EmailSenderManagerExecutor::EmailSenderManagerExecutor(BackgroundTaskInterface *pTask,QObject *parent)
:BackgroundTaskExecutor(pTask,parent,0,true,false,false)
{
}

//thread safe: start after
void EmailSenderManagerExecutor::OnGlobalPeriodChanged(int nPeriodSec)
{
	//SetTimeInterval(nPeriodSec*1000);
	//Stop();
	Start(false,1);
}

