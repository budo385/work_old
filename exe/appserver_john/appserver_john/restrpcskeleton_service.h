#ifndef RESTRPCSKELETON_SERVICE_H__
#define RESTRPCSKELETON_SERVICE_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"

class RestRpcSkeleton_Service : public RpcSkeleton
{

typedef void (RestRpcSkeleton_Service::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );

public:
	RestRpcSkeleton_Service(int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadCatalog(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetPicture(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void RotatePicture(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void DeleteDocument(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void UploadDocument(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void DownloadDocument(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetCatalogs(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetCatalogPicture(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void RenameCatalog(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void RenameCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void CreateCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void UnpublishCatalogClient(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void Login(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void Logout(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetFolderStatus(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void MakeInvitation(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetServerData(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReloadSettings(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);

	QHash<QString,PFN> mFunctList;
};

#endif	// RESTRPCSKELETON_SERVICE_H__
