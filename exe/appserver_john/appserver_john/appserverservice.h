#ifndef APPSERVERSERVICE_H
#define APPSERVERSERVICE_H

#include <QCoreApplication>
#include "common/common/status.h"
#include "common/common/service/qtservice.h"

class AppServerService : public QtService<QCoreApplication>
{

public:
	AppServerService(int argc, char **argv, QString strApplicationServiceName,bool bAutoStartUp=false);
	~AppServerService();

protected:
	void start();
	void stop();
	void pause();
	void resume();
	void shutdown();

	void RegisterPowerEvents();
};

#endif // APPSERVERSERVICE_H
