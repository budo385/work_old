#include "iphotohandler.h"
#include <QDir>
#include <QSqlQuery>
#include <QVariant>
#include <QThread>
#include <QTime>

//#define LST_FORBIDDEN_ALBUMS "Events;Photos;Faces;Places;Last Import;Months;Last 12 Months;Flagged;Trash"
#define LST_FORBIDDEN_ALBUMS "Last Import;Flagged"

int GetCurrentThreadID()
{
	Qt::HANDLE pHandler = QThread::currentThreadId();
	int nThreadID =(long)pHandler;	//FIX: must use long or the compilation on OSX fails (64bit build)
	Q_ASSERT_X(nThreadID!=0,"Illegal Server Thread","Trying to get user context data for non initalized thread, not subclassed from Common.ServerThread!!");
	return nThreadID;
}

void iPhotoHandler::TestVersion()
{
	QDir dir(QDir::homePath() + "/Pictures/iPhoto Library/Database");
	if(dir.exists()){
		m_bIPhoto11 = true;
	}
}

QString iPhotoHandler::GetDbName()
{

#ifdef _DEBUG
	m_bIPhoto11=true;
	return "D:\\Library.apdb";
#else
	QString strDbFile = QDir::homePath();
	if(m_bIPhoto11){
		strDbFile += "/Pictures/iPhoto Library/Database/Library.apdb";
	}
	else{
		strDbFile += "/Pictures/iPhoto Library/iPhotoMain.db";
	}
	return strDbFile;
#endif
}

QString iPhotoHandler::GetPicFolder()
{

#ifdef _DEBUG
	return "D:/Pictures/";
#else
	QString strDir = QDir::homePath();
	if(m_bIPhoto11){
		strDir += "/Pictures/iPhoto Library/Masters/";
	}
	else
		strDir += "/Pictures/iPhoto Library/";
	return strDir;
#endif
}

bool iPhotoHandler::Login()
{
	if (m_pDb)
	{
		m_pDb->close();
		delete m_pDb;
	}

	//open Sqlite database file
	QString strRnd; 
	strRnd.setNum(qrand());
	m_strDbName = QTime::currentTime().toString("hhmmsszzz") + QString::number(GetCurrentThreadID())+strRnd;

	//m_db = QSqlDatabase::addDatabase("QSQLITE");
	m_pDb = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE", m_strDbName));

	if(m_pDb->isValid()){
		m_pDb->setDatabaseName(GetDbName());
		m_pDb->setConnectOptions("QSQLITE_OPEN_READONLY"); //FIX: we must access iPhoto base as read only, in order not to break Apple guidelines
		if(m_pDb->open()){
			return true;
		}
	}
	return false;
}

bool iPhotoHandler::IsLogged()
{
	if (!m_pDb)
		return false;
	return m_pDb->isOpen();
}

void iPhotoHandler::Logout()
{
	if(IsLogged())
	{
		m_pDb->close();
		delete m_pDb;
		QSqlDatabase::removeDatabase(m_strDbName);
	}
}

bool iPhotoHandler::GetAlbums(QList<iPhotoAlbum> &lstResult)
{
	if(!IsLogged())
		return false;

	//list all albums
	QSqlQuery query(*m_pDb);
	if(m_bIPhoto11){
		//there are lots of fake albums out there
		//if(!query.exec("SELECT DISTINCT name, folderUuid FROM RKAlbum INNER JOIN RKAlbumVersion ON RKAlbum.modelId=RKAlbumVersion.albumID WHERE RKAlbum.isInTrash=0 AND RKAlbum.isHidden=0"))
		if(!query.exec("SELECT DISTINCT RKAlbum.name, RKAlbum.modelId FROM RKAlbum INNER JOIN RKAlbumVersion ON RKAlbum.modelId=RKAlbumVersion.albumID WHERE RKAlbum.isInTrash=0 AND RKAlbum.isHidden=0"))
			return false;
	}
	else{
		if(!query.exec("SELECT name, primaryKey FROM SqAlbum WHERE className='Album' AND visible=1"))
			return false;
	}

	lstResult.clear();
	while(query.next())
	{
		iPhotoAlbum item;
		item.m_strName = query.value(0).toString();
		item.m_strID = query.value(1).toString();

		//Filter all albums for iPhoto11:
		//if (m_bIPhoto11)
		{
			if (item.m_strName.isEmpty())
				continue;
			else if (QString(LST_FORBIDDEN_ALBUMS).split(";").contains(item.m_strName))
				continue;
		}

		lstResult << item;
	}
	return true;
}

bool iPhotoHandler::GetAlbumPictures(QString &strAlbumID, QList<iPhotoEntry> &lstResult)
{
	if(!IsLogged())
		return false;

	//list all photos for a given album ID
	QSqlQuery query(*m_pDb);
	if(m_bIPhoto11){
		//if(!query.prepare("SELECT imagePath FROM RKMaster WHERE projectUuid=:AlbumID"))
		if(!query.prepare("SELECT imagePath FROM RKMaster INNER JOIN RKVersion ON RKMaster.uuid = RKVersion.masterUuid INNER JOIN RKAlbumVersion ON RKVersion.modelId=RKAlbumVersion.versionId WHERE albumId=:AlbumID"))
			return false;
	}
	else{
		if(!query.prepare("SELECT SqFileInfo.relativePath FROM SqFileInfo, SqPhotoInfo, SqFileImage, AlbumsPhotosJoin WHERE AlbumsPhotosJoin.sqAlbum=:AlbumID AND AlbumsPhotosJoin.sqPhotoInfo=SqPhotoInfo.primaryKey AND SqPhotoInfo.primaryKey=SqFileImage.photoKey AND SqFileImage.sqFileInfo=SqFileInfo.primaryKey AND SqPhotoInfo.isVisible=1 AND SqPhotoInfo.isUserHidden=0 AND SqFileImage.imageType=6 ORDER BY AlbumsPhotosJoin.photosOrder"))
			return false;
	}
	query.bindValue(0, strAlbumID);
	if(!query.exec())
		return false;

	QString strPicDir = GetPicFolder();
	lstResult.clear();
	while(query.next()){
		iPhotoEntry item;
		item.m_strPath = strPicDir + query.value(0).toString();
		lstResult << item;
	}
	return true;
}

bool iPhotoHandler::GetAlbumPicturesExt(QString &strAlbumID, QFileInfoList &lstFiles)
{
	iPhotoHandler iPhotoObj;

	if (!iPhotoObj.IsLogged())
		if(!iPhotoObj.Login())
			return false;

	QList<iPhotoEntry> lstPics;
	if(!iPhotoObj.GetAlbumPictures(strAlbumID, lstPics))
		return false;

	int nLen = lstPics.length();
	for(int i=0; i<nLen; i++)
	{
		lstFiles<<QFileInfo(lstPics.at(i).m_strPath);
	}

	return true;
}




