#include "emailhandler.h"
#include "trans/trans/smtpclient.h"
#include "common/common/config_version_ffh.h"
#include "common/common/datahelper.h"
#include "trans/trans/httputil.h"
#include "common/common/zipcompression.h"
#include <QCoreApplication>
#include <QFile>
#include <QFileInfo>
#include <QDir>

/*
	Attachments will be added one by one and zipped in template:
*/

void EmailHandler::SendEmail(Status &err, QString strAppServerID, DbRecordSet rowFolder, DbRecordSet recMail, DbRecordSet lstUsersToSendEmail, QString strSubject, QString strBody,QString strCatalogNameFullPath,QStringList lstAttachments,bool bZipAttachment)
{

	//bZipAttachment=true;
	//lstAttachments.append("D:/ORGANSKO_sjeme.pdf");
	//lstAttachments.append("D:/HowToConnect_WithoutText.png");
	//lstAttachments.append("D:/Temp/test_everdocs/SUFIZAM_Zavrsna.doc");


		QString strCatalogName="";
		if (strCatalogNameFullPath.isEmpty())
			strCatalogName=rowFolder.getDataRef(0,"ALIAS").toString();
		else
			strCatalogName=strCatalogNameFullPath;

		QString strCatPath=rowFolder.getDataRef(0,"PATH").toString();


		QByteArray strCatalogBase64Pic = rowFolder.getDataRef(0,"THUMB_LARGE").toByteArray().toBase64();
		strCatalogBase64Pic = DataHelper::EncodeBase64To73LineLength(strCatalogBase64Pic);

		QString strWidth = QString::number(rowFolder.getDataRef(0,"THUMB_LARGE_WIDTH").toInt());
		QString strHeight = QString::number(rowFolder.getDataRef(0,"THUMB_LARGE_HEIGHT").toInt());
	
		QString strPathToTemplateEmail;
		if (lstAttachments.size()==0)
			strPathToTemplateEmail= QCoreApplication::applicationDirPath()+"/template_settings/everDOCs_Invitation.html";
		else
			strPathToTemplateEmail= QCoreApplication::applicationDirPath()+"/template_settings/everDOCs_Doc.html";
	

		QFile file_invite(strPathToTemplateEmail);
		if (!file_invite.exists())
		{
			err.setError(1,QString("Invitation email template %1 not found!").arg(strPathToTemplateEmail));
			return;
		}

		if(!file_invite.open(QIODevice::ReadOnly))
		{
			err.setError(1,QString("Invitation email template %1 can not be red!").arg(strPathToTemplateEmail));
			return;
		}

		QString strDataMail = file_invite.readAll();

		/*
		1 - body
		2,3 invite link
		4 - catalog name
		5 - parameters for Download_App_Instructions.html: same as invite!
		6 - <<id/user/pass>>
		*/

		//send mail... format as <<gdfgdgf>>
		int nSize=lstUsersToSendEmail.getRowCount();

		QString strBodyPlainText=strBody;
		strBody.replace("\n","<br>");
		if (nSize!=1)
			//strBody = strBody + recMail.getDataRef(0,"SIGNATURE").toString();
			strBody = strBody + "<br>" + recMail.getDataRef(0,"SIGNATURE").toString();


		//Marin requested that CID be changeable: random:
		QString strDate=QDateTime::currentDateTime().toString("yyyyMMdd");
		QString strTime=QDateTime::currentDateTime().toString("0mmsszzz");
		QString strCID="part5."+strDate+"."+strTime+"@sokrates.ch";
		//QString strCID="part5.00050207."+strTime+"@sokrates.ch";
		strDataMail.replace("part5.00050207.08090708@sokrates.ch",strCID);

	
		//int nCnt=0;
		for (int i=0;i<nSize;i++)
		{
			QString strSalut=lstUsersToSendEmail.getDataRef(i,"SALUT").toString();
			QString strUserName=lstUsersToSendEmail.getDataRef(i,"USERNAME").toString();
			QString strUserPass=lstUsersToSendEmail.getDataRef(i,"PASSWORD").toString();
			QString strUserEmail=lstUsersToSendEmail.getDataRef(i,"EMAIL").toString();

			QString strParameters="?catalog="+QUrl::toPercentEncoding(strCatalogName);
			strParameters+="&user="+QUrl::toPercentEncoding(strUserName)+"&pass="+QUrl::toPercentEncoding(strUserPass.toLatin1().toBase64());
			QString strLink=QString(FFH_CONNECT_SERVER_MASTER_IP)+"/invite/"+strAppServerID+"/web/invite.html"+strParameters;
			QString strIPhoneInvite = "&lt;&lt;"+strAppServerID+"//"+strUserName+"//"+strUserPass+"&gt;&gt;";
			QString strIPhonePlain = "<<"+strAppServerID+"//"+strUserName+"//"+strUserPass+">>";

			QList<QString> lstBcc;
			QString strMailServer=recMail.getDataRef(0,"SMTP_SERVER").toString();
			int nMailPort=recMail.getDataRef(0,"SMTP_PORT").toInt();
			QString strFrom=recMail.getDataRef(0,"EMAIL_ADDRESS").toString();
			QString strEmailServerUser=recMail.getDataRef(0,"USERNAME").toString();
			QString strEmailServerPass=recMail.getDataRef(0,"PASSWORD").toString();

			QString strParameters_2="?server_id="+QUrl::toPercentEncoding(strAppServerID);
			strParameters_2+="&user="+QUrl::toPercentEncoding(strUserName)+"&pass="+QUrl::toPercentEncoding(strUserPass.toLatin1().toBase64());

			QString strBodyForSend=strBody;
			if (nSize>1)
				strBodyForSend = "<p>"+strSalut +"<br>" +strBody+"</p>";  

			QString strBodyHtml=strBodyForSend;

//#ifdef __APPLE__
			DataHelper::ReencodeStringToUnicode(strBodyForSend,strBodyHtml);
//#endif

			QString strDataMailToSend = strDataMail;
			strDataMailToSend.replace("[From_Email_Address]",strFrom);
			strDataMailToSend.replace("[To_Email_Address]",strUserEmail);
			strDataMailToSend.replace("[Subject]",strSubject);
			strDataMailToSend.replace("[Intro_Text]",strBodyPlainText);
			strDataMailToSend.replace("[Intro_Text_HTML]",strBodyHtml);
			strDataMailToSend.replace("[Invitation_URL]",strLink);
			strDataMailToSend.replace("[Login_Code]",strParameters_2);
			strDataMailToSend.replace("[Login_Code_HTML]",strIPhoneInvite);
			strDataMailToSend.replace("[Login_Code_Plain]",strIPhonePlain);
			strDataMailToSend.replace("[Catalog_Name]",strCatalogName);
			strDataMailToSend.replace("[Catalog_Pic_Base64]",strCatalogBase64Pic);
			strDataMailToSend.replace("[Cat_Pic_Width]",strWidth);
			strDataMailToSend.replace("[Cat_Pic_Height]",strHeight);


			//----------------------------------------LOAD FILE ONE BY ONE, ZIP, SET MIME HEADER----------------------------------------
			//
			//----------------------------------------LOAD FILE ONE BY ONE, ZIP, SET MIME HEADER----------------------------------------

			int nCntOfAttachments=lstAttachments.size();
			for (int k=0;k<nCntOfAttachments;k++)
			{
				QString strFile=strCatPath+"/"+lstAttachments.at(k);
				if (bZipAttachment)
				{
					QFileInfo file_info(strCatPath+"/"+lstAttachments.at(k));
					QStringList lst;
					lst.append(strCatPath+"/"+lstAttachments.at(k));
					strFile = QDir::tempPath()+"/"+file_info.completeBaseName()+".zip";
					if(!ZipCompression::ZipFiles(strFile,lst))
						continue;
				}

				QFileInfo info(strFile);
				QHttpRequestHeader header;
				HTTPUtil::SetHeaderDataBasedOnFile(&header,strFile);
				QString strContentType=header.value("Content-Type");

				QFile file(strFile);
				if(file.open(QIODevice::ReadOnly))
				{
					strDataMailToSend.append("\n--everDOCs--183964186");
					strDataMailToSend.append("\nContent-Disposition: attachment;filename=\"[Attachment_Filename]\"");
					strDataMailToSend.append("\nContent-Type: [Attachment_Type];name=\"[Attachment_Filename]\"");
					strDataMailToSend.append("\nContent-Transfer-Encoding: base64");
					strDataMailToSend.append("\n\n[Attachment_Data]");

					//on APPLE does not work with umlaut
					QString str1=info.fileName().toLatin1();
					//QString str2;
					QString str2="iso-8859-1''"+QUrl::toPercentEncoding(info.fileName());
					//DataHelper::ReencodeStringToUnicode(str1,str2);

					strDataMailToSend.replace("[Attachment_Filename]",str2);
					strDataMailToSend.replace("[Attachment_Type]",strContentType);

					QByteArray byteData=file.readAll().toBase64();
					QString strDataEncoded = DataHelper::EncodeBase64To73LineLength(byteData);

					if (bZipAttachment)
					{
						file.close();
						file.remove(strFile);
					}

					strDataMailToSend.replace("[Attachment_Data]",strDataEncoded);
				}
			}


			strDataMailToSend.append("\n--everDOCs--183964186--");
			SMTPClient client;

			if(!client.SendMail(strMailServer,strFrom,strUserEmail,strSubject,strDataMailToSend,lstBcc,strEmailServerUser,strEmailServerPass,true,true,nMailPort))
			{
				err.setError(1,QString("Failed to send email to %1 at email address: %2").arg(strUserName).arg(strUserEmail));
				return;
			}
		}
}

void EmailHandler::SendAppServerLog(Status &err, QString strAppServerID, QString strLogPath, QString strFrom, QString strTo, QString strMailServer, int nMailPort, QString strEmailServerUser, QString strEmailServerPass)
{

		QStringList lstAttachments;
		bool bZipAttachment=true;
		lstAttachments.append(strLogPath);

		QString strPathToTemplateEmail;
		strPathToTemplateEmail= QCoreApplication::applicationDirPath()+"/template_settings/everDOCs_Doc.html";
	
		QFile file_invite(strPathToTemplateEmail);
		if (!file_invite.exists())
		{
			err.setError(1,QString("Log email template %1 not found!").arg(strPathToTemplateEmail));
			return;
		}

		if(!file_invite.open(QIODevice::ReadOnly))
		{
			err.setError(1,QString("Log email template %1 can not be red!").arg(strPathToTemplateEmail));
			return;
		}

		QString strDataMail = file_invite.readAll();


		QString strBodyPlainText=QString("Log File from server with ID=%1 captured at %2").arg(strAppServerID).arg(QDateTime::currentDateTime().toString(Qt::SystemLocaleLongDate));
		QString strSubject=strBodyPlainText;
		QString strBody=strBodyPlainText;
		strBody.replace("\n","<br>");


		//Marin requested that CID be changeable: random:
		QString strDate=QDateTime::currentDateTime().toString("yyyyMMdd");
		QString strTime=QDateTime::currentDateTime().toString("0mmsszzz");
		QString strCID="part5."+strDate+"."+strTime+"@sokrates.ch";
		//QString strCID="part5.00050207."+strTime+"@sokrates.ch";
		strDataMail.replace("part5.00050207.08090708@sokrates.ch",strCID);

		QString strBodyForSend=strBody;
		//QString strBodyHtml=strBodyForSend;
		//DataHelper::ReencodeStringToUnicode(strBodyForSend,strBodyHtml);

		QString strDataMailToSend = strDataMail;
		strDataMailToSend.replace("[From_Email_Address]",strFrom);
		strDataMailToSend.replace("[To_Email_Address]",strTo);
		strDataMailToSend.replace("[Subject]",strSubject);
		strDataMailToSend.replace("[Intro_Text]",strBodyPlainText);
		//strDataMailToSend.replace("[Intro_Text_HTML]",strBodyHtml);



			//----------------------------------------LOAD FILE ONE BY ONE, ZIP, SET MIME HEADER----------------------------------------
			//
			//----------------------------------------LOAD FILE ONE BY ONE, ZIP, SET MIME HEADER----------------------------------------

			int nCntOfAttachments=lstAttachments.size();
			for (int k=0;k<nCntOfAttachments;k++)
			{
				QString strFile=lstAttachments.at(k);
				QFileInfo file_info(lstAttachments.at(k));
				QStringList lst;
				lst.append(lstAttachments.at(k));
				strFile = QDir::tempPath()+"/"+file_info.completeBaseName()+".zip";
				if(!ZipCompression::ZipFiles(strFile,lst))
				{
					err.setError(1,QString("Error occurred while zipping file: %1!").arg(lstAttachments.at(k)));
					return;
				}

				QFileInfo info(strFile);
				QHttpRequestHeader header;
				HTTPUtil::SetHeaderDataBasedOnFile(&header,strFile);
				QString strContentType=header.value("Content-Type");

				QFile file(strFile);
				if(file.open(QIODevice::ReadOnly))
				{
					strDataMailToSend.append("\n--everDOCs--183964186");
					strDataMailToSend.append("\nContent-Disposition: attachment;filename=\"[Attachment_Filename]\"");
					strDataMailToSend.append("\nContent-Type: [Attachment_Type];name=\"[Attachment_Filename]\"");
					strDataMailToSend.append("\nContent-Transfer-Encoding: base64");
					strDataMailToSend.append("\n\n[Attachment_Data]");

					//on APPLE does not work with umlaut
					QString str1=info.fileName().toLatin1();
					//QString str2;
					QString str2=QUrl::toPercentEncoding(info.fileName());
					//DataHelper::ReencodeStringToUnicode(str1,str2);

					strDataMailToSend.replace("[Attachment_Filename]",str2);
					strDataMailToSend.replace("[Attachment_Type]",strContentType);

					QByteArray byteData=file.readAll().toBase64();
					QString strDataEncoded = DataHelper::EncodeBase64To73LineLength(byteData);

					if (bZipAttachment)
					{
						file.close();
						file.remove(strFile);
					}

					strDataMailToSend.replace("[Attachment_Data]",strDataEncoded);
				}
			}


			strDataMailToSend.append("\n--everDOCs--183964186--");
			SMTPClient client;

			QList<QString> lstBcc;
			if(!client.SendMail(strMailServer,strFrom,strTo,strSubject,strDataMailToSend,lstBcc,strEmailServerUser,strEmailServerPass,true,true,nMailPort))
			{
				err.setError(1,QString("Failed to send email log '%1' at email address: '%2'. Last Error: %3").arg(strSubject).arg(strTo).arg(client.GetLastError()));
				return;
			}
}