#ifndef EMAILHANDLER_H
#define EMAILHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include <QString>
#include <QStringList>

class EmailHandler
{

public:

	static void SendEmail(Status &err, QString strAppServerID, DbRecordSet rowFolder, DbRecordSet recMail, DbRecordSet lstUsersToSendEmail, QString strSubject, QString strBody,QString strCatalogNameFullPath="",QStringList lstAttachments = QStringList(),bool bZipAttachment=false);
	static void SendAppServerLog(Status &err, QString strAppServerID, QString strLogPath, QString strFrom, QString strTo, QString strMailServer, int nMailPort, QString strEmailServerUser, QString strEmailServerPass);

private:
	
};

#endif // EMAILHANDLER_H
