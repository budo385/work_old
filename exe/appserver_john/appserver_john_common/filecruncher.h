#ifndef FILECRUNCHER_H
#define FILECRUNCHER_H

#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "common/common/logger.h"
#include <QStringList>

#define JOHN_DRIVE_UNKNOWN     0
#define JOHN_DRIVE_NO_ROOT_DIR 1
#define JOHN_DRIVE_REMOVABLE   2
#define JOHN_DRIVE_FIXED       3
#define JOHN_DRIVE_REMOTE      4
#define JOHN_DRIVE_CDROM       5
#define JOHN_DRIVE_RAMDISK     6


	#define _LISTFILES(qdir,list_dirs_cmd,file_list) \
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("entryInfoList START execute on path: (%1)").arg(qdir.path()));\
	file_list = list_dirs_cmd;\
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,1,QString("entryInfoList END execute on path: (%1)").arg(qdir.path()));
	
 

class FileCruncher
{
public:

	enum FileTypeGroups
	{
		FILE_TYPE_ALL=0,
		FILE_TYPE_PIC=1,
		FILE_TYPE_OFFICE=2
	};

	static void			GetDrives(QStringList &lstDriveNames,QStringList &lstDriverLetters,QStringList &lstPaths,QList<int> &lstTypes);
	static QString		GetFirstFileFromFolder(QString strFolderPath,QString strAllowedExtension="");
	static QDateTime	GetLastModifyDateFromFolder(QString strFolderPath,QString strAllowedExtension="");
	static int			GetFileCountFromFolder(QString strFolderPath,QString strAllowedExtension="");
	static int			GetFileCountFromFolderFast(QString strFolderPath);
	static bool			GetFilesFromFolder(QString strFolderPath,DbRecordSet &lstPaths, DbRecordSet &lstSubDirs, QString strAllowedExtension="",QString strFileSuffixFilter="");
	static void			CreateThumbsFromFiles(DbRecordSet &lstPicturePaths, QString strOutFolder, QString strSuffix_Ext, int nWidth, int nHeight, bool bKeepAspect);
	static QString		GetFileTypesBasedOnCode(int nFileTypesCode, bool bIncludeRawFormats=true);
	static QStringList	GetPicFileTypesNameFilterFormat();
	static void			GetAllSubFolders(QString strParentDir,QList<QString> &lstSubDirs);
	static bool			RemoveAllFromDirectory(QString strFolderPath, bool bRecursiveFolders=true, bool bRemoveDirectory=true);
	static bool			IsNameValidAsDirectory(const QString &strDirName);
	static bool			CompareFolderAndDeleteNonExisting(QString strFolder, QString strComapreToFolder,QStringList lstSuffixes,QString strAllowedExtension="");
	static QStringList	CreateFiltersFromExtensionString(QString strAllowedExtension);
	static bool			IsRAWPicture(QString strExt);
	static bool			GetJpegSize(unsigned const char* data, unsigned int data_size, int *width, int *height); 
	static void			RemoveRootFolders(QStringList &lstFolders); 

	static void			GetFolderStatus(Status &pStatus, DbRecordSet &Ret_Folders,int nThumbCount, DbRecordSet &lstFolders,QString strOutThumbDirRoot);
	static void			GetFolderStatusFast(Status &pStatus, DbRecordSet &Ret_Folders,int nThumbCount, DbRecordSet &lstFolders,QString strOutThumbDirRoot);
	static QString		GetThumbPathBasedOnCatalogPath(QString strCatalogDirPath,QString strOutThumbDirRoot);

	static bool			EncodeDirectory2Binary(QString strPassword, const QString strDirPath,QByteArray &byteData);
	static bool			DecodeBinary2Directory(QString strPassword, const QByteArray &byteData,QList<QString> &lstPaths, QList<QDateTime> &lstLastModifyTimes, QList<QByteArray> &lstBinaryContent);

	static void			ParseNetworkDrives(QStringList &lstDrives,QStringList &lstPaths);
	static bool			ReplaceNetworkPathWithUNC(QString &strDirPath);
	
	static QString		ResolveCatalogPath(DbRecordSet &lstFolders,QString strAliasPath, QString &strAlias, bool &bIsDrive);

private:
	static QStringList s_lstPicExt;
	
};


/*
	For each folder operation, test if iPhotoId exists and if so, use these functions
	Figure for gentool to accept parameters for iPhoto album.
*/

class FileCruncher_iPhoto
{
public:

	static QString		GetFirstFileFromFolder(QString strPhotoAlbumID,QString strAllowedExtension="");
	static QDateTime	GetLastModifyDateFromFolder(QString strPhotoAlbumID,QString strAllowedExtension="");
	static int			GetFileCountFromFolder(QString strPhotoAlbumID,QString strAllowedExtension="");
	static bool			GetFilesFromFolder(QString strPhotoAlbumID,DbRecordSet &lstPaths, DbRecordSet &lstSubDirs, QString strAllowedExtension="",QString strFileSuffixFilter="");
	static bool			CompareFolderAndDeleteNonExisting(QString strFolder, QString strPhotoAlbumID,QStringList lstSuffixes,QString strAllowedExtension="");

};

#endif // FILECRUNCHER_H
