#ifndef SERVERINIFILE_H
#define SERVERINIFILE_H

#include "common/common/inifile.h"
#include <QFileInfo>



/*!
	\class ServerIniFile
	\brief INI file reader for app. server
	\ingroup AppServer

	Application server INI file reader


*/
class ServerIniFile
{
public:
	ServerIniFile();
	~ServerIniFile();

	ServerIniFile(const ServerIniFile &other){ operator = (other); }
	
	void operator = (const ServerIniFile &other)
	{
		if(this != &other)
		{
			m_strAppServerIPAddress		= other.m_strAppServerIPAddress;
			m_nAppServerPort			= other.m_nAppServerPort;		
			m_nAppServerPortRangeEnd	= other.m_nAppServerPortRangeEnd;		
			m_nAppServerPortActual		= other.m_nAppServerPortActual;		
			m_nMaxConnections			= other.m_nMaxConnections;		
			m_strServiceDescriptor		= other.m_strServiceDescriptor;
			m_nAutoStartService			= other.m_nAutoStartService;
			m_strDBConnectionName		= other.m_strDBConnectionName;
			m_nEnableUpnp				= other.m_nEnableUpnp;
			m_nEnableNAT				= other.m_nEnableNAT;
			m_nForcePublicIP			= other.m_nForcePublicIP;
			m_strWebDocumentsPath		= other.m_strWebDocumentsPath;
			m_strAppServerID			= other.m_strAppServerID;
			m_strLangCode				= other.m_strLangCode;

			m_nSSLMode					= other.m_nSSLMode;				
			m_strSSLCertificateDir		= other.m_strSSLCertificateDir;	

			m_nLogLevel					= other.m_nLogLevel;
			m_nLogMaxSize				= other.m_nLogMaxSize;
			m_nLogBufferSize			= other.m_nLogBufferSize;
			m_nLastErrorCode			= other.m_nLastErrorCode;
			m_strLastErrorText			= other.m_strLastErrorText;
			m_strOuterIP				= other.m_strOuterIP;

			m_nJPEGQuality				= other.m_nJPEGQuality;
			m_strThumbDir				= other.m_strThumbDir;
			m_strCatalogDir				= other.m_strCatalogDir;
			m_nPortForwardMethod		= other.m_nPortForwardMethod;
			m_strWebAppVersion			= other.m_strWebAppVersion;
			m_nServerTestedConnection	= other.m_nServerTestedConnection;
			m_nDisableSettingPage		= other.m_nDisableSettingPage;

			m_nAskedForUser				= other.m_nAskedForUser;
			m_nAskedForEmail			= other.m_nAskedForEmail;
			m_nAskedForNewDir			= other.m_nAskedForNewDir;

		}
	}
	
	void Clear();
	bool Load(QString strFile, bool bCreateIfNotExists=true);
	bool Save(QString strFile="");

public:

	//data stored in the INI file
	QString m_strAppServerIPAddress;	// IP address bind (def is 0.0.0.0)
	int		m_nAppServerPort;			// port number
	int		m_nAppServerPortRangeEnd;			// port number
	int		m_nAppServerPortActual;		// port number
	int		m_nMaxConnections;			// maximum connections that app. server will accept
	QString	m_strServiceDescriptor;		// app name
	int		m_nAutoStartService;		//
	QString m_strDBConnectionName;		// NAME OF DB CONN
	QString m_strWebDocumentsPath;		// default app/webservices
	int		m_nEnableUpnp;					
	int		m_nEnableNAT;
	int		m_nForcePublicIP;
	QString	m_strAppServerID;
	QString	m_strOuterIP;
	QString m_strLangCode;
	int		m_nDisableSettingPage;
	//int		m_nFirstStart;
	int		m_nAskedForUser;
	int		m_nAskedForEmail;
	int		m_nAskedForNewDir;


	int		m_nJPEGQuality;
	QString	m_strThumbDir;
	int		m_nPortForwardMethod;
	QString	m_strCatalogDir;
	QString	m_strWebAppVersion;
	int		m_nServerTestedConnection;



	int		m_nLastErrorCode;
	QString	m_strLastErrorText;

	//SSL
	int		m_nSSLMode;					// accept only SSL(https) connections
	QString m_strSSLCertificateDir;		// path to the SSL certificates files (server.cert & server.pkey)

	//LOGGER
	int m_nLogLevel;					
	int m_nLogMaxSize;					
	int m_nLogBufferSize;				
		
	//BACKUP:
	int m_nBackupFreq;		
	QString m_strBackupPath;		
	QString m_datBackupLastDate;		
	QString m_strRestoreOnNextStart;		
	QString m_strBackupTime;
	int m_nBackupDay;





	
protected:
	IniFile m_objIni;
	bool CheckValues();
	bool SaveDefaults(QString strFile);
	QString m_strLastPathUsed;

};

#endif //SERVERINIFILE_H