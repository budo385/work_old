# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

HEADERS += ./common_constants.h \
    ./emailhandler.h \
    ./filecruncher.h \
    ./iphotohandler.h \
    ./serverinifile.h \
    ./settingsmanager.h
SOURCES += ./emailhandler.cpp \
    ./filecruncher.cpp \
    ./iphotohandler.cpp \
    ./serverinifile.cpp \
    ./settingsmanager.cpp
