#ifndef SOKRATES_SPC_EMBEDDED_H
#define SOKRATES_SPC_EMBEDDED_H

#include <QObject>

class sokrates_spc_embedded : public QObject
{
	Q_OBJECT

public:
	sokrates_spc_embedded();
	~sokrates_spc_embedded();
	
public slots:
	void Initialize();		//called on startup

private:
//	Ui::sokrates_spc_embeddedClass ui;
};

#endif // SOKRATES_SPC_EMBEDDED_H
