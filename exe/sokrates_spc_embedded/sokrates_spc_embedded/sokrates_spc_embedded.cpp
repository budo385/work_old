#include "sokrates_spc_embedded.h"
#include "os_specific/os_specific/tapi/TapiLine.h"
//#include <QMessageBox>
//#include <QFileDialog>

#include "common/common/datahelper.h"
#include "bus_core/bus_core/dbconnectionsetup.h"
#include "client_global_objects_create.h"
//#include "fui_embedded/fui_embedded/fuicodeidlist.h"

#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "fuimanager.h"
FuiManager *g_FuiManager = NULL;

sokrates_spc_embedded::sokrates_spc_embedded()
{
//	ui.setupUi(this);

	//Initialize FuiManager.
	g_FuiManager = new FuiManager();
	CTapiLine::InitTAPI();
}

sokrates_spc_embedded::~sokrates_spc_embedded()
{
	CTapiLine::ShutdownTAPI();

	g_pClientManager->Logout();
	if(g_FuiManager)
		delete(g_FuiManager);
}


void sokrates_spc_embedded::Initialize()
{
	//SetupMenus(false);
	//g_pClientManager->Login();
}

