#include <QtGui/QApplication>
#include "sokrates_spc_embedded.h"
//#include "sokrates_spc_embedded_mainwidget.h"
#include <QTimer>
#include "clientmanagerext.h"
ClientManagerExt *g_pClientManager=NULL;

void logOutput(QtMsgType type, const char *msg);
FILE *g_pOut = NULL;

int main(int argc, char *argv[])
{
#if 0
	g_pOut = fopen("/sokrates_log.txt", "w");
	qInstallMsgHandler(logOutput);
#endif

	QApplication a(argc, argv);
	a.setQuitOnLastWindowClosed(true);
	
	//init resources for all linked libraries 
	//Q_INIT_RESOURCE(gui_core);

	Status err;
	g_pClientManager= new ClientManagerExt(&a); 
	g_pClientManager->Initialize(err,false,false);
	if (!err.IsOK())
		return 1;		//error

	sokrates_spc_embedded w;
	//QTimer::singleShot(0, &w, SLOT(Initialize()));			//wait to start until QCoreApp enters event loop
	//w.showMaximized();
	//w.showFullScreen();
	
	bool bRes = a.exec();

#if 0
	fclose(g_pOut);
#endif

	return bRes;
}

void logOutput(QtMsgType type, const char *msg)
{
	fprintf(g_pOut, "%s\n", msg);
}
