#include "servicehandler.h"
#include "trans/trans/resthttpclient.h"

void ServiceHandler::Test(Status &err, int nInput, DbRecordSet &RetData)
{
	//qDebug()<<nInput;
	RetData.destroy();
	RetData.addColumn(QVariant::String,"ANTE");
	RetData.addRow();
	RetData.setData(0,0,"Kakalo");

	//TEST: connect to other REST server and send with POST method on https://192.168.200.18/rest/contacts/filter:
	//<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><nFilter>1</nFilter><nFromN>-1</nFromN><nToN>-1</nToN><strFromLetter>a</strFromLetter><strToLetter>c</strToLetter></PARAMETERS></REQUEST>//

	//CONNECT:
	RestHTTPClient client;
	client.SetConnectionSettings("192.168.200.18",1111,true);

	client.Connect(err);
	if (!err.IsOK())
		return;

	//PREPARE TO SEND:
	int nFilter=1;
	int nFrom=-1;
	int nToN=-1;
	QString strFromLetter="a";
	QString strToLetter="d";

	client.msg_send.AddParameter(&nFilter,"nFilter");
	client.msg_send.AddParameter(&nFrom,"nFromN");
	client.msg_send.AddParameter(&nToN,"nToN");
	client.msg_send.AddParameter(&strFromLetter,"strFromLetter");
	client.msg_send.AddParameter(&strToLetter,"strToLetter");

	//SEND:
	client.RestSend(err,"POST","/rest/contacts/filter");
	if (!err.IsOK())return;

	int nTotalCount;

	client.msg_recv.GetParameter(0,&nTotalCount,err);
	if (!err.IsOK())return;

	RetData.destroy();
	RetData.addColumn(QVariant::String,"Contact_ID");
	RetData.addColumn(QVariant::String,"LastName");
	RetData.addColumn(QVariant::String,"FirstName");
	RetData.addColumn(QVariant::String,"OrganizationName");
	RetData.addColumn(QVariant::Int,"Type");
	RetData.addColumn(QVariant::String,"CalculatedName");

	client.msg_recv.GetParameter(1,&RetData,err);
	if (!err.IsOK())return;

}