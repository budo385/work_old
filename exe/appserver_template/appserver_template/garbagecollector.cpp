#include "garbagecollector.h"
#include "common/common/threadid.h"

#include "common/common/logger.h"
extern Logger g_Logger;

GarbageCollector::GarbageCollector(HTTPServer *pHttpServer)
{
	m_HttpServer=pHttpServer;
	//when HTTP detect client disconnection, pass it here to clean up his mess
	connect (m_HttpServer,SIGNAL(ClientSocketIsDead(int)),this,SLOT(ClientClosedConnection(int)),Qt::QueuedConnection);		//when server detects disconnect, delete session
}

GarbageCollector::~GarbageCollector()
{

}


//start every X minutes
void GarbageCollector::ExecuteTask(int nCallerID,int nTaskID)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_GARBAGE_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
}


//When HTTP or other closes connection, clear session
void GarbageCollector::ClientClosedConnection(int nSocketID)
{
	//Status err;
	//QString strUserSession=dynamic_cast<UserSessionManager_Server*>(g_UserSessionManager)->GetSessionIDBySocketID(nSocketID);
	//int nThreadID=dynamic_cast<UserSessionManager_Server*>(g_UserSessionManager)->GetThreadBySocketID(nSocketID);
	//if(nThreadID==-1) //client never made to usersession manager or normal disconnect event
	//return;

	qDebug()<<"Socket closed "<<nSocketID;

	//destroy all user data in one place:
	//g_PrivateServiceSet->CoreUser->DestroyUserContextData(err,"",nThreadID);
}
