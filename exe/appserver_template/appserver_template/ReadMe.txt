AppServer Template v1.0 - 08.04.2010

Install:
1. Includes: HTTP server, REST server, REST client, garbagecollector,logger, basic INI file, packed as service (run with -e for debug)
2. Use: copy all files, rename project dirs/sln/vcproj and inside them to appserver_xxxxx
3. To compile: from _files dir copy all content to the new appserver_xxxx (rename first appserver_template dir). Needed, coz qtservice.moc will not compile...only from appserver.sln project those files are successfully compiled!?
4. Database support: disabled by default, to enable go to the applicationserver.cpp and enable g_DbManager and all functions related to it. Include projects: db_core and db.

HTTP Server Framework: 
1. HTTP server supports SSL connections (certificates +ssl dll's must be present). Port and other connection settings is defined inside INI file.
2. HTTP server spawns new thread for each new connection so don't use member/shared variables without mutex, semaphore, readwrite locks.
3. To handle any HTTP request handler must be registered. Base class: trans/HTTPHandler. Handler is uniquely identified by 'domain' string. 
4. HTTP Handler main method is HandleRequest in which request is handled and response is generated. Client connection, authentication and other things are done by handler. HTTP server only returns response to the client and keeps connection or disconnects based on handler response.
5. By default appserver_template supports only REST handler which will be activated if client request URL path that starts with /rest
6. All start-up registration and initialization of HTTP server is done inside applicationserver.cpp/Start() method.

REST Server Framework (basic): 
1. On HTTP layer REST layer is using HTTP body for XML messages. REST is using HTTP method and URL path for method resolving. 
2. For example 'GET /rest/my_method_path' and 'POST /rest/my_method_path' are two different methods
3. It's common to map HTTP methods as: GET = Read, PUT = Edit, POST = Add, DELETE = Delete
4. Inside appserver_template RestHTTPServer_Local is REST server handler that is registered inside HTTP server
5. Rest server can handle multiple sets of functions, which can be logically organized into business objects (contacts, projects). 
6. Each set is defined by 'namespace'. Each function sets corresponds to the one RpcSkeleton object.
7. RpcSkeletonDispatcher contains all RpcSkeleton's or function sets. This object is responsible to dispatch REST call to the RpcSkeleton based on namespace. Each RpcSkeleton must resolve if it handles REST call.
8. RpcSkeleton is object that parses REST XML body and call's appropriate Business object method, returns variables and encodes them into REST XML format.
9. Summary: RpcSkeleton parses and resolves REST function calls and call's assigned Business object methods.
10. Summary: RpcSkeletonDispatcher is collection of all RpcSkeleton instances and must be registered with RestHTTPServer.
11. By default in appserver_template: 
- RestRpcSkeleton_Service is RpcSkeleton, calls ServiceHandler methods (by default namespace is 'service', so to call method 'my_method' full path will be: https://myserver:9999/rest/service/my_method)
- ServiceHandler is Business handler
- RestRpcDispatcher - is collection of all skeletons
- RestHTTPServer_Local - is REST HTTP handler that dispatches HTTP calls to the RestRpcDispatcher

REST Server Framework (simple):
1. Add new REST API:
 - Add method inside ServiceHandler
 - Add method inside RestRpcSkeleton_Service that will call that ServiceHandler method (make parser for incoming/outgoing msg)
 - Publish that method inside RestRpcSkeleton_Service constructor by defining http method and URL path to the method. e.g. GET/service/my_method_name
2. Add new REST API collection (skeleton):
 - Create skeleton object, register and make instance inside RestRpcDispatcher
 
REST Client Framework: 
1. Establishes sync HTTP calls (waits for response).
2. Use RestHTTPClient class. Example is servicehandler.cpp/Test();
 
GarbageCollector/Background Periodical Task Framework:
1. Run in separates thread periodically. Initialization is done inside appserverservice.cpp/Start()
2. Use GBC for clean up. Can detect if client is disconnected.
3. To create new separate periodical process use GBC framework. DO NOT USE timers from MAIN appserver thread!!!

