#ifndef RESTRPCSKELETON_SERVICE_H
#define RESTRPCSKELETON_SERVICE_H

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"

class RestRpcSkeleton_Service : public RpcSkeleton 
{
	typedef void (RestRpcSkeleton_Service::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id);

public:
	RestRpcSkeleton_Service(int RPCType);
	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod);

private:
	//skeleton function for each method publicly accessible
	void Test(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id);

	QHash<QString,PFN> mFunctList;
};


#endif // RESTRPCSKELETON_SERVICE_H
