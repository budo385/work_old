#ifndef SERVICEHANDLER_H
#define SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class ServiceHandler
{
public:
	void Test(Status &err, int nInput, DbRecordSet &RetData);

private:
	
};

#endif // SERVICEHANDLER_H
