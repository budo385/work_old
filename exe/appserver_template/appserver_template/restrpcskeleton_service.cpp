#include "restrpcskeleton_service.h"

#include "servicehandler.h"
extern ServiceHandler *g_ServiceHandler;

RestRpcSkeleton_Service::RestRpcSkeleton_Service(int RPCType):
RpcSkeleton(RPCType)
{
	SetNameSpace("service");	//set namespace (business object name)

	mFunctList.insert("GET/service/test",&RestRpcSkeleton_Service::Test);
	/*
	mFunctList.insert("POST/contacts/filter",&RestRpcSkeleton_Service::ReadContactsWithFilter);
	mFunctList.insert("GET/contact/[RES_ID]",&RestRpcSkeleton_Service::ReadContactDetails);
	mFunctList.insert("PUT/contact",&RestRpcSkeleton_Service::ModifyContact);
	mFunctList.insert("POST/contact",&RestRpcSkeleton_Service::AddContact);
	mFunctList.insert("DELETE/contact/[RES_ID]",&RestRpcSkeleton_Service::DeleteContact);
	mFunctList.insert("POST/contacts/search",&RestRpcSkeleton_Service::SearchContacts);
	mFunctList.insert("GET/contacts/favorites",&RestRpcSkeleton_Service::ReadFavorites);
	mFunctList.insert("GET/contact/[RES_ID]/addresses",&RestRpcSkeleton_Service::ReadContactAddresses);
	mFunctList.insert("POST/contact/[RES_ID]/documents",&RestRpcSkeleton_Service::ReadContactDocuments);
	mFunctList.insert("GET/contact/[RES_ID]/documents/count",&RestRpcSkeleton_Service::ReadContactDocumentsCount);
	mFunctList.insert("GET/contact/[RES_ID]/emails/count",&RestRpcSkeleton_Service::ReadContactEmailCount);
	*/
}

bool RestRpcSkeleton_Service::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod)
{
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;
	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);
		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);
			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

	return true;
}

void RestRpcSkeleton_Service::Test(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1 && rpc.msg_in->GetParameterCount()>0)
	{
		err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
		return;
	}

	//extract parameters to local vars
	int nX=-1;
	DbRecordSet Ret_Test;
	if(rpc.msg_in->GetParameterCount()>0)
	{
		rpc.msg_in->GetParameter(0,&nX,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	}

	//invoke handler
	g_ServiceHandler->Test(err,nX,Ret_Test);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Test,"data","record");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}


/*
void RestRpcSkeleton_Service::ReadContactsWithFilter(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=5 && rpc.msg_in->GetParameterCount()>0)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet Ret_Contacts;
	int Ret_nTotalCount;
	int nFilter=-1;
	int nFromN=-1;
	int nToN=-1;
	QString strFromLetter="";
	QString strToLetter="";

	if(rpc.msg_in->GetParameterCount()>0)
	{
		rpc.msg_in->GetParameter(0,&nFilter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
		rpc.msg_in->GetParameter(1,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
		rpc.msg_in->GetParameter(2,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
		rpc.msg_in->GetParameter(3,&strFromLetter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
		rpc.msg_in->GetParameter(4,&strToLetter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	}


	//invoke handler
	m_Handler->ReadContactsWithFilter(err,Ret_nTotalCount,Ret_Contacts,nFilter,nFromN,nToN,strFromLetter,strToLetter);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Contacts,"Contacts","contact",GetWebServiceURL()+"/contact","Contact_ID");


	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("contacts_response_with_count.xsd");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::ReadFavorites(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	//if(rpc.msg_in->GetParameterCount()!=3)
	//{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	//DbRecordSet  Filter;
	DbRecordSet  Ret_Favorites;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	//rpc.msg_in->GetParameter(1,&Filter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadFavorites(err,Ret_Favorites);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Favorites,"Favorites","contact",GetWebServiceURL()+"/contact","Contact_ID");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("contacts_favorites_response.xsd");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}


void RestRpcSkeleton_Service::ReadContactDetails(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	//if(rpc.msg_in->GetParameterCount()!=1)
	//{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int nContactID;
	DbRecordSet Ret_ContactData;
	DbRecordSet Ret_Emails;
	DbRecordSet Ret_WebSites;
	DbRecordSet Ret_Phones;
	DbRecordSet Ret_Sms;
	DbRecordSet Ret_Skype;
	DbRecordSet Ret_Addresses;

	//rpc.msg_in->GetParameter(0,&nContactID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	nContactID=nResource_id;

	//invoke handler
	m_Handler->ReadContactDetails(err,nContactID,Ret_ContactData,Ret_Emails,Ret_WebSites,Ret_Phones,Ret_Sms,Ret_Skype,Ret_Addresses);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)

	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_ContactData,"ContactData","contact",GetWebServiceURL()+"/contact","Contact_ID");
	rpc.msg_out->AddParameter(&Ret_Emails,"Emails","email");
	rpc.msg_out->AddParameter(&Ret_WebSites,"WebSites","website");
	rpc.msg_out->AddParameter(&Ret_Phones,"Phones","phone");
	rpc.msg_out->AddParameter(&Ret_Sms,"Sms","sms");
	rpc.msg_out->AddParameter(&Ret_Skype,"Skype","skype");
	rpc.msg_out->AddParameter(&Ret_Addresses,"Addresses","address");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("contacts_details_response.xsd");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}


void RestRpcSkeleton_Service::ReadContactAddresses(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	//if(rpc.msg_in->GetParameterCount()!=1)
	//{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int nContactID;
	DbRecordSet Ret_Emails;
	DbRecordSet Ret_WebSites;
	DbRecordSet Ret_Phones;
	DbRecordSet Ret_Sms;
	DbRecordSet Ret_Skype;
	DbRecordSet Ret_Addresses;

	//rpc.msg_in->GetParameter(0,&nContactID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	nContactID=nResource_id;

	//invoke handler
	m_Handler->ReadContactAddresses(err,nContactID,Ret_Emails,Ret_WebSites,Ret_Phones,Ret_Sms,Ret_Skype,Ret_Addresses);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Emails,"Emails","email");
	rpc.msg_out->AddParameter(&Ret_WebSites,"WebSites","website");
	rpc.msg_out->AddParameter(&Ret_Phones,"Phones","phone");
	rpc.msg_out->AddParameter(&Ret_Sms,"Sms","sms");
	rpc.msg_out->AddParameter(&Ret_Skype,"Skype","skype");
	rpc.msg_out->AddParameter(&Ret_Addresses,"Addresses","address");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("contacts_details_response.xsd");
	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RestRpcSkeleton_Service::SearchContacts(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=10)
		//qDebug()<<*(rpc.msg_in->GetBuffer());
		//qDebug()<<rpc.msg_in->GetParameterCount();
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int Ret_nTotalCount;
	int nSearchType;
	QString strField1_Name;
	QString strField1_Value;
	QString strField2_Name;
	QString strField2_Value;
	int nGroupID=-1;
	int nFromN=-1;
	int nToN=-1;
	QString strFromLetter="";
	QString strToLetter="";
	DbRecordSet Ret_Contacts;

	rpc.msg_in->GetParameter(0,&nSearchType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strField1_Name,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strField1_Value,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&strField2_Name,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strField2_Value,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&nGroupID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&strFromLetter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(9,&strToLetter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}


	//invoke handler
	m_Handler->SearchContacts(err,Ret_nTotalCount,Ret_Contacts,nSearchType,strField1_Name,strField1_Value,strField2_Name,strField2_Value,nGroupID,nFromN,nToN);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Contacts,"Contacts","contact",GetWebServiceURL()+"/contact","Contact_ID");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("contacts_response_with_count.xsd");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}


void RestRpcSkeleton_Service::ReadContactDocuments(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3 && rpc.msg_in->GetParameterCount()>0)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int nContactID;
	DbRecordSet Ret_Documents;
	int Ret_nTotalCount;
	int nDocType=-1;
	int nFromN=-1;
	int nToN=-1;

	if(rpc.msg_in->GetParameterCount()>0)
	{
		rpc.msg_in->GetParameter(0,&nDocType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
		rpc.msg_in->GetParameter(1,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
		rpc.msg_in->GetParameter(2,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	}

	nContactID=nResource_id;

	//invoke handler
	m_Handler->ReadContactDocuments(err,Ret_nTotalCount,Ret_Documents,nContactID,nDocType,nFromN,nToN);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Documents,"Documents","document",GetWebServiceURL()+"/document","Document_ID");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("documents_response_with_count.xsd");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}



void RestRpcSkeleton_Service::ReadContactDocumentsCount(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count

	//extract parameters to local vars
	int nContactID;
	int Ret_nFileDocCount;
	int Ret_nWebSiteCount;
	int Ret_nNotesCount;

	nContactID=nResource_id;

	m_Handler->ReadContactDocumentsCount(err,nContactID,Ret_nFileDocCount,Ret_nWebSiteCount,Ret_nNotesCount);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_out->AddParameter(&Ret_nFileDocCount,"nFileDocCount");
	rpc.msg_out->AddParameter(&Ret_nWebSiteCount,"nWebSiteCount");
	rpc.msg_out->AddParameter(&Ret_nNotesCount,"nNotesCount");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("contacts_documents_count_response.xsd");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}


void RestRpcSkeleton_Service::ReadContactEmailCount(Status &err,RpcSkeletonMessageHandler &rpc, int nResource_id, int nResource_parent_id)
{
	//test parameter count

	int nContactID;
	int Ret_nCount;

	nContactID=nResource_id;

	m_Handler->ReadContactEmailCount(err,nContactID,Ret_nCount);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_out->AddParameter(&Ret_nCount,"nCount");

	//generate response
	QString strNamespaces=GetServiceXmlNameSpace("contacts_emails_count_response.xsd");

	rpc.msg_out->GenerateResponse(err,strNamespaces);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

}
*/