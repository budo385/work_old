#ifndef RESTRPCDISPATCHER_H
#define RESTRPCDISPATCHER_H

#include "trans/trans/rpcskeletondispatcher.h"
#include "servicehandler.h"
#include "restrpcskeleton_service.h"

class RestRpcDispatcher : public RpcSkeletonDispatcher
{
public:
	RestRpcDispatcher();

private:
	RestRpcSkeleton_Service		*Service;
};



#endif // RESTRPCDISPATCHER_H
