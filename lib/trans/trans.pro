TEMPLATE	= lib
CONFIG		= qt warn_on release staticlib QtNetwork
HEADERS		= trans/httpclientconnection.h \
		  trans/httpclientconnectionsettings.h \
		  trans/httpclientsockethandler.h \
		  trans/httpcontext.h \
		  trans/httphandler.h \
		  trans/httphostresolver.h \
		  trans/httpreader.h \
		  trans/httpreadersync.h \
		  trans/httprequest.h \
		  trans/httpresponse.h \
		  trans/httpserver.h \
		  trans/httpserverconnectionsettings.h \
		  trans/httpserversockethandler.h \
		  trans/httpserverthread.h \
		  trans/httputil.h \
		  trans/ipaccesslist.h \
		  trans/ipaddress.h \
		  trans/networkconnections.h \
		  trans/qtsslsocket.h \
		  trans/rpchttpheader.h \
		  trans/rpcmessage.h \
		  trans/rpcprotocol.h \
		  trans/rpcskeleton.h \
		  trans/rpcskeletondispatcher.h \
		  trans/rpcskeletonmessagehandler.h \
		  trans/rpcstub.h \
		  trans/rpcstubmessagehandler.h \
		  trans/xmlrpchttpclienthandler.h \
		  trans/xmlrpchttpserverhandler.h \
		  trans/xmlrpcmessage.h \
		  trans/xmlrpcutil.h \
		  trans/xmlrpcvalue.h
SOURCES		= trans/httpclientconnection.cpp \
		  trans/httpclientsockethandler.cpp \
		  trans/httphandler.cpp \
		  trans/httphostresolver.cpp \
		  trans/httpreader.cpp \
		  trans/httpreadersync.cpp \
		  trans/httprequest.cpp \
		  trans/httpresponse.cpp \
		  trans/httpserver.cpp \
		  trans/httpserversockethandler.cpp \
		  trans/httpserverthread.cpp \
		  trans/httputil.cpp \
		  trans/ipaccesslist.cpp \
		  trans/ipaddress.cpp \
		  trans/networkconnections.cpp \
		  trans/qtsslsocket.cpp \
		  trans/rpchttpheader.cpp \
		  trans/rpcprotocol.cpp \
		  trans/rpcskeletondispatcher.cpp \
		  trans/rpcskeletonmessagehandler.cpp \
		  trans/rpcstubmessagehandler.cpp \
		  trans/xmlrpchttpclienthandler.cpp \
		  trans/xmlrpchttpserverhandler.cpp \
		  trans/xmlrpcmessage.cpp \
		  trans/xmlrpcutil.cpp \
		  trans/xmlrpcvalue.cpp
INTERFACES	= 
TARGET		= trans
LIBS		+= -lssl -lcrypto QtNetwork
INCLUDEPATH	+= ../	/usr/lib/qt4/include/QtNetwork
