#include "httputil.h"
#include <QFileInfo>

#include "trans/trans/config_trans.h"

/*!
	From PATH field from HTTPRequest message extracts first string enclosed in / / or string that begins with /
	e.g. /RPC/ or /PING.....
	If not found returns empty string, if root (\) then returns /, else only string inside
	/param pHTTPRequest incoming HTTP request headerdata stream for checkout
	/param strDomain= "", "/", everything behind first / or domain without slashes e.g. RPC2 etc.. (domain is our user defined area: first thing after /)
*/
void HTTPUtil::GetDomain(const HTTPRequest& pHTTPRequest, QString& strDomain)
{
	GetDomain(pHTTPRequest.path(),strDomain);
}
void HTTPUtil::GetDomain(const QString &strPath, QString& strDomain)
{
		//find the proper handler for this request, based on request domain (first part of the path)
		strDomain = QUrl(strPath).path().toLower();
		int nPos = strDomain.indexOf('/');
		if(nPos >= 0)
		{
			int nPosEnd = strDomain.indexOf('/',nPos+1);
			if (nPosEnd>=0)
				strDomain = strDomain.mid(nPos+1,nPosEnd-nPos-1);
			else
				strDomain = strDomain.mid(nPos+1); //get all behind:

			if (strDomain.trimmed().isEmpty()) //if not found
				strDomain="/";
		}
		else
			strDomain.clear();	//empty domain
}


/*!
	Cheks incoming stream, determines if HTTP is request or response, and if package is complete
	/param pIncomingBuffer incoming data stream for checkout. WARNING: strips any data at end that is not part of HTTP msg
	/param pHTTPType HTTP_HEADER_NOTCOMPLETE,HTTP_HEADER_REQUEST,HTTP_HEADER_RESPONSE,HTTP_HEADER_INVALID
	/return false-HTTP header not complete, true- HTTP header complete and valid
*/
bool HTTPUtil::CheckHTTPHeader(const QByteArray& pIncomingBuffer, int& pHTTPType, qint64 &nContentLength, int &nHdrEnd){

	nHdrEnd = pIncomingBuffer.indexOf("\r\n\r\n");
		if(nHdrEnd > 0)
			nHdrEnd += 4;	// point past the end of header

	//incomplete header received, return;
	if(nHdrEnd< 0)	
	{ 
		pHTTPType=HTTP_HEADER_NOTCOMPLETE;
		return false;
	}

	//determine type of header:
	//Response is defined by Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
	//so just look for HTTP at begining fo stream,
	int nPost=pIncomingBuffer.indexOf("HTTP");
	nContentLength=0;
	if (nPost!=0)
	{
		pHTTPType=HTTP_HEADER_REQUEST; //if not at 0 then its request
		QHttpRequestHeader HTTPHeader(pIncomingBuffer.left(nHdrEnd)); //fill header only with header data, just to determine content length and valid
		if(!HTTPHeader.isValid())
			{pHTTPType=HTTP_HEADER_INVALID;return false;}; //invalid header: discard whole data! 

		if (HTTPHeader.value("Transfer-Encoding")=="chunked")
			nContentLength=-1;
		else
			nContentLength=HTTPHeader.contentLength();
	}
	else 
	{
		pHTTPType=HTTP_HEADER_RESPONSE;
		QHttpResponseHeader HTTPHeader((QString)pIncomingBuffer.left(nHdrEnd)); //fill header only with header data, just to determine content length and valid
		if(!HTTPHeader.isValid())
			{pHTTPType=HTTP_HEADER_INVALID;return false;}; //invalid header: discard whole data! 
		
		if (HTTPHeader.value("Transfer-Encoding")=="chunked")
			nContentLength=-1;
		else
			nContentLength=HTTPHeader.contentLength();

	}
	
	return true;
}
//true: end of msg (chop all at end), else not...
bool HTTPUtil::CheckHTTPEndOfMessage(QByteArray& pIncomingBuffer, qint64 nContentLength)
{
	if (nContentLength>=0) //normal transfer:
	{
		//test content length:
		if((pIncomingBuffer.size())>=nContentLength) 
		{//extract contentlength

			//STRIP any data at end of msg:
			if (pIncomingBuffer.size()>nContentLength)
				pIncomingBuffer.chop(pIncomingBuffer.size()-nContentLength);
			return true;	
		}
		else
			return false; //msg not complete
	}
	else //chunked transfer: look for 0 at end of
	{
		if(pIncomingBuffer.right(9)=="\r\n0  \r\n\r\n")
			return true; 
		else
			return false;
	}
}
bool HTTPUtil::ExtractHTTPChunk(QByteArray& pIncomingBuffer, QByteArray& pChunk)
{
	int nEndPosSize=pIncomingBuffer.indexOf("\r\n");
	if (nEndPosSize<=0)return false;

	if (nEndPosSize>30) //max for chunk length
		return false;

	QString strChunkSizeLength=pIncomingBuffer.left(nEndPosSize).trimmed();

	//HEX:
	bool bOK=false;
	int nChunkSize=strChunkSizeLength.toInt(&bOK, 16);
	if (!bOK) return false;

	int nStartPos=nEndPosSize+2; //skip CLRF:
	if (nStartPos+nChunkSize+2>pIncomingBuffer.size())
		return false;

	pChunk=pIncomingBuffer.mid(nStartPos,nChunkSize);
	pIncomingBuffer=pIncomingBuffer.mid(nStartPos+nChunkSize+2); //remove extracted chunk
	return true;
}



/*!
	Maps TCP socket errors to our custom set of errors
	/param pStatus contain our custom error
	/param QAbstractSocket::SocketError errSocket
*/
void HTTPUtil::MapSocketErrors(Status& pStatus, QAbstractSocket::SocketError errSocket,QString strSocketErrorDesc){

	switch(errSocket){

		case QAbstractSocket::SocketTimeoutError:
			pStatus.setError(StatusCodeSet::ERR_HTTP_OPERATION_TIMEOUT); break;
		case QAbstractSocket::HostNotFoundError:
			pStatus.setError(StatusCodeSet::ERR_HTTP_HOST_NOT_FOUND); break;
		case QAbstractSocket::RemoteHostClosedError:
			pStatus.setError(StatusCodeSet::ERR_HTTP_HOST_CLOSED); break;
		case QAbstractSocket::ConnectionRefusedError:
			pStatus.setError(StatusCodeSet::ERR_HTTP_FAILED_CONNECT); break;
		default:
			{
				if (strSocketErrorDesc.isEmpty())
					pStatus.setError(StatusCodeSet::ERR_HTTP_GENERAL_SOCKET_ERROR);
				else
					pStatus.setError(StatusCodeSet::ERR_HTTP_GENERAL_SOCKET_ERROR_DESC,strSocketErrorDesc);
			}
			
	}	

}

void HTTPUtil::MapFTPSocketErrors(Status& pStatus, QFtp::Error errSocket)
{
	switch(errSocket)
	{
		case QAbstractSocket::SocketTimeoutError:
			pStatus.setError(StatusCodeSet::ERR_HTTP_OPERATION_TIMEOUT); break;
		case QFtp::HostNotFound:
			pStatus.setError(StatusCodeSet::ERR_HTTP_HOST_NOT_FOUND); break;
		case QFtp::NotConnected:
			pStatus.setError(StatusCodeSet::ERR_HTTP_HOST_CLOSED); break;
		case QFtp::ConnectionRefused:
			pStatus.setError(StatusCodeSet::ERR_HTTP_FAILED_CONNECT); break;
		default:
			pStatus.setError(StatusCodeSet::ERR_HTTP_GENERAL_SOCKET_ERROR);
	}
}

/*!
Invoked by client thread itself, searches handlers by registered domain:
/param HTTPRequest contain domain in PATH tag
/return pHandler returned handler, if not found then NULL
*/
HTTPHandler* HTTPUtil::GetRequestHandler(QList<HTTPHandler *> &lstHandlers,HTTPRequest& pRequest)
{
	return GetRequestHandler(lstHandlers,pRequest.path());
}

HTTPHandler* HTTPUtil::GetRequestHandler(QList<HTTPHandler *> &lstHandlers,const QString &strPath)
{
	//find handler with right domain
	QString strDomain;
	HTTPUtil::GetDomain(strPath,strDomain);
	int nSize=lstHandlers.size();
	for(int i=0; i<nSize; i++)
	{
		if(lstHandlers.at(i)->GetDomain() == strDomain){
			return lstHandlers.at(i);
		}
	}

	//if root path, then return HTML handler if active, if not return NULL: abort connection
	if (strPath.left(1)=="/")
	{
		for(int i=0; i<nSize; i++)
		{
			if(lstHandlers.at(i)->GetDomain() == QString(DOMAIN_HTTP_HTML).toLower()){
				return lstHandlers.at(i);
			}
		}
	}

	return NULL;
}

//strFileName : complete path to the file
//set: Content-Encoding: deflate (if non zip or other files)
//set: Content-Type and Content-Disposition
//set: Content-Disposition: Attachment; filename=foo.html; filesize=xxx
//see http://www.w3schools.com/media/media_mimeref.asp for all mime's
//see http://stackoverflow.com/questions/179315/downloading-docx-from-ie-setting-mime-types-in-iis
void HTTPUtil::SetHeaderDataBasedOnFile(QHttpHeader *header, QString strFileName,bool bAddContentDisposition,int nDeflateIfAboveByte)
{
	QFileInfo info(strFileName);
	qint64 nOriginalFileSize=info.size();
	QString strExt=info.suffix().toLower();	 //MR: 2014.03.24. must work for uppercase ext too (MB reported issue)
	bool bCompressData=false;

	if (bAddContentDisposition)
		//header->setValue("Content-Disposition",QString("Attachment; filename=%1; filesize=%2").arg(info.fileName()).arg(nOriginalFileSize));
		//header->setValue("Content-Disposition",QString("Attachment; filename=\"%1\"; filesize=%2").arg(info.fileName()).arg(nOriginalFileSize));
		header->setValue("Content-Disposition",QString("Attachment; filename=\"%1\"; size=%2").arg(QString(QUrl::toPercentEncoding(info.fileName()))).arg(nOriginalFileSize));

	if (strExt=="html" || strExt=="htm")
	{
		header->setValue("Content-Type","text/html");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="txt")
	{
		header->setValue("Content-Type","text/plain");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="js")
	{
		header->setValue("Content-Type","text/javascript");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="css")
	{
		header->setValue("Content-Type","text/css");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="xml")
	{
		header->setValue("Content-Type","text/xml");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="doc" || strExt=="dot")
	{
		header->setValue("Content-Type","application/msword");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="docx")
	{
		header->setValue("Content-Type","application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="zip")
	{
		header->setValue("Content-Type","application/zip");
		return;
	}
	if (strExt=="pdf")
	{
		header->setValue("Content-Type","application/pdf");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="gif")
	{
		header->setValue("Content-Type","image/gif");
		return;
	}
	if (strExt=="jpeg" || strExt=="jpg")
	{
		header->setValue("Content-Type","image/jpeg");
		return;
	}
	if (strExt=="png")
	{
		header->setValue("Content-Type","image/png");
		return;
	}
	if (strExt=="tiff" || strExt=="tif")
	{
		header->setValue("Content-Type","image/tiff");
		return;
	}
	if (strExt=="mpeg" || strExt=="mpg")
	{
		header->setValue("Content-Type","video/mpeg");
		return;
	}
	if (strExt=="mp4")
	{
		header->setValue("Content-Type","video/mp4");
		return;
	}
	if (strExt=="wmv")
	{
		header->setValue("Content-Type","video/x-ms-wmv");
		return;
	}
	if (strExt=="wav")
	{
		header->setValue("Content-Type","audio/x-wav");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="exe")
	{
		header->setValue("Content-Type","application/octet-stream");
		return;
	}
	if (strExt=="xls") 
	{
		header->setValue("Content-Type","application/vnd.ms-excel");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="xlsx") 
	{
		header->setValue("Content-Type","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="ppt")
	{
		header->setValue("Content-Type","application/vnd.ms-powerpoint");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="pptx")
	{
		header->setValue("Content-Type","application/vnd.openxmlformats-officedocument.presentationml.presentation");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="ppsx")
	{
		header->setValue("Content-Type","application/vnd.openxmlformats-officedocument.presentationml.slideshow");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="dll")
	{
		header->setValue("Content-Type","application/x-msdownload");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="odt")
	{
		header->setValue("Content-Type","application/vnd.oasis.opendocument.text");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="odg")
	{
		header->setValue("Content-Type","application/vnd.oasis.opendocument.graphics");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="odp")
	{
		header->setValue("Content-Type","application/vnd.oasis.opendocument.graphics");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="ods")
	{
		header->setValue("Content-Type","application/vnd.oasis.opendocument.graphics");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	if (strExt=="manifest")
	{
		header->setValue("Content-Type","text/cache-manifest");
		if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
		return;
	}
	


	//default (binary data):
	header->setValue("Content-Type","application/octet-stream");
	if(nOriginalFileSize>nDeflateIfAboveByte && nDeflateIfAboveByte!=-1)header->setValue("Content-Encoding","deflate");
}

//parse format: Content-Disposition: Attachment; filename=foo.html; filesize=xxx
bool HTTPUtil::ExtractFileInfoFromContentDisposition(QString strContentDisposition,QString& strFileName, qint64& nFileSize,QString &strFormFieldName)
{
	strFileName="";
	nFileSize=0;
	strFormFieldName="";

	//filename
	int nPos=strContentDisposition.indexOf("filename");
	if (nPos>=0)
	{
		nPos=strContentDisposition.indexOf("=",nPos);
		if (nPos>=0)
		{
			int nPosEnd=strContentDisposition.indexOf(";",nPos);
			if (nPosEnd>=0)
				strFileName=strContentDisposition.mid(nPos+1,nPosEnd-nPos-1).trimmed();
			else
				strFileName=strContentDisposition.mid(nPos+1).trimmed();
			strFileName=strFileName.replace("\"",""); //remove " if exists:
			strFileName=QUrl::fromPercentEncoding(strFileName.toLatin1());
		}
	}


	//name
	nPos=strContentDisposition.indexOf("name");
	if (nPos>=0)
	{
		nPos=strContentDisposition.indexOf("=",nPos);
		if (nPos>=0)
		{
			int nPosEnd=strContentDisposition.indexOf(";",nPos);
			if (nPosEnd>=0)
				strFormFieldName=strContentDisposition.mid(nPos+1,nPosEnd-nPos-1).trimmed();
			else
				strFormFieldName=strContentDisposition.mid(nPos+1).trimmed();

			strFormFieldName=strFormFieldName.replace("\"",""); //remove " if exists:
		}
	}

	
	//custom parameter: can be used to check disk space or else..
	nPos=strContentDisposition.indexOf("size");
	if (nPos<0)
		return !strFileName.isEmpty(); //return true if filename exists
	nPos=strContentDisposition.indexOf("=",nPos);
	if (nPos<0)
		return !strFileName.isEmpty(); //return true if filename exists

	QString strFileSize=strContentDisposition.mid(nPos+1).trimmed();
	bool bOK;
	nFileSize=strFileSize.toLongLong(&bOK);
	return bOK;
}

