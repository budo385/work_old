#ifndef IPDETECTOR_H
#define IPDETECTOR_H

#include <QHostAddress>
#include <QHostInfo>
#include <QDebug>


/*!
	\class IPDetector
	\brief Gets local IP address from mulitple interfaces
	\ingroup IPModule
*/

class IPDetector 
{

public:
	static QString GetLocalIPAddress();

private:
    
};

#endif // IPDETECTOR_H
