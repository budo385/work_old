#ifndef SMTPCLIENT_H
#define SMTPCLIENT_H

#include <QThread>
#include "common/common/threadsync.h"
#include "smtpsocket.h"
#include <QStringList>

//note uses AUTH LOIGN method: http://www.fehcom.de/qmail/smtpauth.html
class SMTPClient : public QThread
{
	Q_OBJECT

public:
	SMTPClient(int nTimeOut=10000);
	~SMTPClient();

	bool	SendMail(const QString &strMailServer, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc,  QString strUserName ="", QString strPassword="",const bool bBodyIsHtml=false, const bool bSendBodyAsFormatedMimeMessage=false, int nSmtpPort=0, bool bIsUTF8=false); 
	QString GetLastError();
	static QString ComposeMultipartMimeMessage(QStringList &lstImage_Name, QStringList &lstImage_Base64, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc, const bool bBodyIsHtml=false);


signals:
	void SendMailSignal(); 
	void Destroy();

private:
	void run();

	SMTPSocket *m_SmtpSocket;
	int m_nTimeOut;
	ThreadSynchronizer m_ThreadSync;///< for syncing between mastr & slave thread

	
};

#endif // SMTPCLIENT_H
