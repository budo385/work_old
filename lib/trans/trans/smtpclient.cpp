#include "smtpclient.h"
#include <QCoreApplication>
#include <QTime>
#include <QLocale>

SMTPClient::SMTPClient(int nTimeOut)
{
	m_nTimeOut=nTimeOut;
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	start(); //start thread
	m_ThreadSync.ThreadWait();			//prepare for start
}

SMTPClient::~SMTPClient()
{
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit Destroy();
	m_ThreadSync.ThreadWait();			//prepare for start

	//stop thread gently:
	quit();
	wait();
}


//start Thread
void SMTPClient::run()
{
	m_SmtpSocket = new SMTPSocket(&m_ThreadSync);
	connect(this,SIGNAL(SendMailSignal()),m_SmtpSocket,SLOT(SendMail()));
	connect(this,SIGNAL(Destroy()),m_SmtpSocket,SLOT(Destroy()));
	m_ThreadSync.ThreadRelease();
	exec();
}


bool SMTPClient::SendMail(const QString &strMailServer, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc,  QString strUserName, QString strPassword,  const bool bBodyIsHtml, const bool bSendBodyAsFormatedMimeMessage, int nSmtpPort, bool bIsUTF8)
{
	m_SmtpSocket->SetParameters(strMailServer,m_nTimeOut,from,to,subject,body,bcc,strUserName,strPassword,bBodyIsHtml,bSendBodyAsFormatedMimeMessage,nSmtpPort, bIsUTF8);
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit SendMailSignal();

	while (!m_ThreadSync.ThreadWaitTimeOut(1000))
	{
		qApp->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
	}


	return !m_SmtpSocket->m_bErrorOccured;
}

QString SMTPClient::GetLastError()
{
	return m_SmtpSocket->m_strLastError;
}


#define MULTIPART_DELIMITER "-------1234567890frontier"
#define MULTIPART_DELIMITER_LAST "-------1234567890frontier--"

#define MULTIPART_DELIMITER_2 "-------2-1234567890frontier"
#define MULTIPART_DELIMITER_LAST_2 "-------2-1234567890frontier--"

#define CID_BASE "10000777"
#define CID_PREFIX "cid:image_no"

QString SMTPClient::ComposeMultipartMimeMessage(QStringList &lstImage_Name, QStringList &lstImage_Base64, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc, const bool bBodyIsHtml)
{
	//All about SMTP headers: 
	// to work must specify correct MIME type, to work on mail.sokrats.ch server header must be in order: subject, date, from to..+"\n" on end...
	//strMessage="\n";

	//Generate date
	QTime myTime = QDateTime::currentDateTime().toUTC().time();
	QLocale myLocal = QLocale(QLocale::C);
	QString date = myLocal.toString(QDate::currentDate(), "ddd, dd MMM yyyy ");
	date += myLocal.toString(myTime, "hh:mm:ss");
	date += " +0000 (UTC)";

	QString strMessage;
	strMessage="";
	//strMessage.append("Date: Fri, 10 Sep 2010 08:43:35 +0200 \n");
	strMessage.append("Subject: " + subject + "\n");
	strMessage.append("Date: " + date + "\n");
	strMessage.append("From: " + from + "\n");
	strMessage.append("To: " + to + "\n");

	strMessage.append("MIME-Version: 1.0 \n");
	strMessage.append("Content-Type: multipart/alternative; boundary=\"");
	strMessage.append(MULTIPART_DELIMITER);
	strMessage.append("\" \n");
	strMessage.append("\n");
	

	strMessage.append("This is a multi-part message in MIME format.");
	strMessage.append("\n");
	strMessage.append(MULTIPART_DELIMITER);
	strMessage.append("\n");


	strMessage.append("Content-Type: text/plain; charset=\"iso-8859-15\" ; format=flowed \n");
	strMessage.append("Content-Transfer-Encoding: 7bit \n");
	strMessage.append("\n");
	strMessage.append("Ante je ovdje i javlja se da je sve u redu! \n");
	strMessage.append(MULTIPART_DELIMITER);
	strMessage.append("\n");

	strMessage.append("Content-Type: multipart/related; boundary=\"");
	strMessage.append(MULTIPART_DELIMITER_2);
	strMessage.append("\" \n");
	strMessage.append("\n");
	strMessage.append("\n");
	strMessage.append(MULTIPART_DELIMITER_2);
	strMessage.append("\n");

	if(bBodyIsHtml)
	{
		strMessage.append("Content-Type: text/html; charset=\"iso-8859-15\" \n");
		strMessage.append("Content-Transfer-Encoding: 7bit \n");
	}
	else
	{
		strMessage.append("Content-Type: text/plain; charset=\"iso-8859-15\" \n");
		strMessage.append("Content-Transfer-Encoding: 7bit \n");

	}
	strMessage.append("\n");
	strMessage.append(body);
	strMessage.append("\n");
	strMessage.append(MULTIPART_DELIMITER_LAST_2);
	strMessage.append("\n");



/*

	int nCID=QString(CID_BASE).toInt();

	int nSize=lstImage_Base64.size();
	for (int i=0;i<nSize;i++)
	{
		//strMessage.append("\n");
		strMessage.append(MULTIPART_DELIMITER);
		strMessage.append("\n");
		QString strName=lstImage_Name.at(i);
		QString strBase64=lstImage_Base64.at(i);
		QString strCID=CID_PREFIX+QString::number(nCID);

		strMessage.replace(strName,strCID); //reference to the new id

		strMessage.append("Content-Type: image/jpg; name="+strName+"\n");
		strMessage.append("Content-Transfer-Encoding: base64 \n");
		strMessage.append("Content-ID: <"+strCID+"> \n");
		strMessage.append("Content-Disposition: inline; filename="+ strName+ "\n");

		strMessage.append("\n");
		strMessage.append(strBase64);

		nCID++;
	}

	*/
	
	strMessage.append("\n");
	strMessage.append(MULTIPART_DELIMITER_LAST);
	strMessage.append("\n");



	strMessage.replace( QString::fromLatin1( "\n" ), QString::fromLatin1( "\r\n" ) );
	strMessage.replace( QString::fromLatin1( "\r\n.\r\n" ),QString::fromLatin1( "\r\n..\r\n" ) );

	return strMessage;
}


