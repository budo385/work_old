#include "rpcskeleton.h"
#include <QFile>
#include <QStringList>
#include <QFileInfoList>
#include <QDir>

QHash<QString,QByteArray> RpcSkeleton::s_hshResData;
bool RpcSkeleton::s_bUseDescription=false;

bool RpcSkeleton::TestNameSpaceForRest(const QString strRPCMethod,QString &strComparePath,int &nResID,int &nParentResID, QString &strHttpMethod)
{
	//strRPCMethod: replace and store all ID's
	QList<int> lstIDs;
	QStringList lstFavsToRead = strRPCMethod.split("/",QString::SkipEmptyParts);
	int nParts=lstFavsToRead.size();
	if (nParts<2) return false;
	if (lstFavsToRead.at(1)!=GetNameSpace()) //test if request is ours:
		if (QString(lstFavsToRead.at(1)+"s")!=GetNameSpace())
			if (lstFavsToRead.at(1)!=QString(GetNameSpace()+"s"))
				return false;

	strHttpMethod=lstFavsToRead.at(0);
	for (int i=nParts-1;i>=0;i--)
	{
		bool bOk;
		int nID=lstFavsToRead.at(i).toInt(&bOk);
		if (bOk)
		{
			lstIDs.append(nID);
			lstFavsToRead[i]="[RES_ID]";
		}
	}
	strComparePath=lstFavsToRead.join("/");

	//use only last two digits for ID's:
	if (lstIDs.size()>1)
	{
		nResID=lstIDs.at(1);
		nParentResID=lstIDs.at(0);
	}
	else if (lstIDs.size()==1)
	{
		nResID=lstIDs.at(0);
	}

	return true;
}

//resource data is format: parent_res_name+"_"+resource_name+".xml"
void RpcSkeleton::LoadServiceDescriptionData(Status &err,const QString strFilePath)
{
	err.setError(0);
	s_hshResData.clear();

	//read all files resource directory:
	QDir dir(strFilePath);
	if (!dir.exists())
	{
		err.setError(1,QObject::tr("Resource Directory:")+strFilePath+QObject::tr(" does not exists!"));
		return ;
	}

	QFileInfoList lstFilesXML=dir.entryInfoList();
	int nSize2=lstFilesXML.size();

	for(int k=0;k<nSize2;++k)
	{
		if(lstFilesXML.at(k).completeSuffix()=="xml")
		{
			//qDebug()<<lstFilesXML.at(k).absoluteFilePath();
			QFile xmlFile(lstFilesXML.at(k).absoluteFilePath());
			if(!xmlFile.open(QIODevice::ReadOnly))
			{
				err.setError(1,QObject::tr("File: ")+lstFilesXML.at(k).absoluteFilePath()+QObject::tr(" can not be read!"));
				return;
			}
			s_hshResData[lstFilesXML.at(k).fileName()]=xmlFile.readAll().simplified();
			xmlFile.close();
		}
	}

}

QString RpcSkeleton::GetServiceDescription(const QString strServiceDescFileName,int nResID, int nParentResID)
{
	if (!s_bUseDescription)
		return "";
	QByteArray byteData=s_hshResData.value(strServiceDescFileName,QByteArray());
	if (byteData.size()==0)
		return "";

	QString strData(byteData);

	if (nResID>0)
		strData.replace("[RES_ID]",QString::number(nResID));
	if (nParentResID>0)
		strData.replace("[PARENT_RES_ID]",QString::number(nParentResID));
	strData.replace("[ROOT_URL]",m_strWebServiceURLPath);

	return strData;
}

QString	RpcSkeleton::GetServiceXmlNameSpace(const QString strSchemaName)
{
	if (strSchemaName.isEmpty())
		return "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
	else
		return "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  xsi:noNamespaceSchemaLocation=\"rest/schemas/"+strSchemaName+"\"";
	
	//QReadLocker lock(&m_lckWebServiceURLPath);
	//return "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  xsi:noNamespaceSchemaLocation=\""+ m_strWebServiceURLPath + "/schemas/"+strSchemaName+"\"";
}
QString	RpcSkeleton::GetWebServiceURL()
{
	QReadLocker lock(&m_lckWebServiceURLPath);
	return m_strWebServiceURLPath;
}

void RpcSkeleton::SetWebServiceURL(QString strURL)
{
	m_lckWebServiceURLPath.lockForWrite();
	m_strWebServiceURLPath=strURL;
	m_lckWebServiceURLPath.unlock();
}