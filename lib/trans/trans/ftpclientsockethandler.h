#ifndef OnFTPCLIENTSOCKETHANDLER_H
#define OnFTPCLIENTSOCKETHANDLER_H

#include <QObject>
#include <QBuffer>
#include "qftp.h"
#include "ftpclient.h"


class FTPClientSocketHandler : public QObject
{
	Q_OBJECT

public:
	
	FTPClientSocketHandler(FTPClient *pClientThread,QSemaphore *pBufferSempahore,ThreadSynchronizer *ThreadSync,QWaitCondition *waitCond,QObject *parent = 0);
	virtual ~FTPClientSocketHandler();


public slots:
	void OnFTPConnect();
	void OnFTPDisconnect();
	void OnFTPDestroy();

	void OnFTPDir_Change(QString strDir);
	void OnFTPDir_Create(QString strDir);
	void OnFTPDir_Remove(QString strDir);
	void OnFTPDir_List();

	void OnFTPFile_Put(QString strFileName);
	void OnFTPFile_Get(QString strFileName);
	void OnFTPFile_Remove(QString strFileName);
	void OnFTPFile_Rename(QString strFileName,QString strNewFileName);

	void OnFTPSendRawCommand(QString strCommand);


private slots:
	void OnDone(bool error );
	void OnDataTransferProgress( qint64 done, qint64 total );
	void OnListInfo(const QUrlInfo & i );
	void OnCommandFinished( int id, bool error );

protected:
	void timerEvent(QTimerEvent *event);
	void ClearSocketState(int LastErrCode=0);

	QFtp m_Ftp;
	int m_nState;									///< state of socket
	int m_nTimerID;									///< timer ID for timeout errors
	QByteArray m_reqBufferData;						///< temporary buffer for accepting new server messages or responses
	QBuffer m_reqBuffer;
	QList<QUrlInfo> m_lstDirContent;
	FTPConnectionSettings m_ConnSettings;			///<for storing current connection settings
	QWaitCondition *m_waitCond;


	// shared data between threads (socket and main):
	QSemaphore* m_BufferSemaphore;					///<resource to protect: response buffer when receiving message
	FTPClient *m_ClientThread;						///<poiner to client thread shared data
	ThreadSynchronizer *m_ThreadSync;				///< for syncing between mastr & slave thread

	//qint64 m_Done,m_Total;
};

#endif // OnFTPCLIENTSOCKETHANDLER_H
