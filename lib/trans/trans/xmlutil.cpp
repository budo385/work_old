#include "xmlutil.h"
#include "trans/trans/xmlrpcvalue.h"

/*!
	Finds coordinates in buffer of content between given TAGs from startoffset

	\param pBuffer	-external buffer with XMLRPC stream
	\param strStartTag start tag (e.g. <PARAM>)
	\param strEndTag end tag (e.g. </PARAM>)
	\param nStartOffset start offset of TAG content in buffer from 0 (-1 if start tag not found)
	\param nLength of content (-1 if end tag not found)
	\param nCount number of occurence to parse+1:e.g. if 2 then means skip 0 and 1 and process 3rd occurence of TAG
*/
void XmlUtil::GetTagContent(QByteArray* pBuffer,QString strStartTag,QString strEndTag, int& nStartOffset,int& nLength,int nCount)
{
	nLength=-1;

	// search for n count if available
	for(int i=0;i<=nCount;++i)
	{
		//find start TAG
		nStartOffset=pBuffer->indexOf(strStartTag,nStartOffset);
		if (nStartOffset==-1) {Q_ASSERT(false);return;} //error
		nStartOffset+=strStartTag.length();
	}


	// if start offset is greater then buffer return with error
	if (nStartOffset>=pBuffer->size()) {Q_ASSERT(false);return;} //error

	//calc content end
	int nContentEnd=pBuffer->indexOf(strEndTag,nStartOffset);
	if (nContentEnd==-1) {Q_ASSERT(false);return;} //error

	//calc content length
	nLength=nContentEnd-nStartOffset;

	Q_ASSERT(nLength>=0); //intercept possible bug
}






/*!
Returns first TAG inside given boundaries
\param pBuffer	-external buffer with XMLRPC stream
\param nStartOffset start offset for TAG search, updated to one after tag >
\param nLength inside search will be done!
\return TAG or "" if nothing found
*/
QString XmlUtil::GetNextTag(QByteArray* pBuffer,int& nStartOffset,int nLength){

	//find start TAG
	int nStartTagOffset=pBuffer->indexOf("<",nStartOffset);
	if (nStartTagOffset==-1||nStartTagOffset>=(nStartOffset+nLength)) return ""; //not found or out of boundaries


	//find end TAG
	int nEndTagOffset=pBuffer->indexOf(">",nStartOffset);
	if(nEndTagOffset==-1||nEndTagOffset>=(nStartOffset+nLength)||nEndTagOffset<=nStartTagOffset) return ""; //not found or out of boundaries

	//update offset boundaries
	int nTagLength=nEndTagOffset-nStartTagOffset+1;
	nStartOffset+=nTagLength;

	return pBuffer->mid(nStartTagOffset,nTagLength);
}






// xml encodings (xml-encoded entities are preceded with '&')
static const char  AMP = '&';
static const QByteArray rawEntity[] = { "&",      "<",    ">",     "\'",     "\""};
static const QByteArray xmlEntity[] = { "&amp;", "&lt;", "&gt;",  "&apos;", "&quot;"};





/*!
Replace raw text with xml-encoded entities
\param xml	-external buffer with XMLRPC stream
\return binary stream (include copy op)
*/
QByteArray XmlUtil::xmlDecode(const QByteArray& xml)
{

	// check if no AMP there is no need to decode
	int pos=xml.indexOf(AMP);
	if(pos<0) return xml;

	QByteArray byteDecoded=xml;

	// iterate through list backwards: its very important to pars &amp last
	for (int iEntity=4; iEntity>=0; iEntity--)
		byteDecoded.replace(xmlEntity[iEntity],rawEntity[iEntity]);

	return byteDecoded;
}



/*!
Replace xml-encoded back to raw text

\param raw		-external buffer with XMLRPC stream
\return string (include copy op)
*/

QString XmlUtil::xmlEncode(const QString& raw)
{

	QString strEncoded=raw;

	// iterate through list forwad: its very important to encode &amp first
	if(strEncoded.indexOf("&")>=0)strEncoded.replace("&","&amp;");
	if(strEncoded.indexOf('<')>=0)strEncoded.replace("<","&lt;");
	if(strEncoded.indexOf(">")>=0)strEncoded.replace(">","&gt;");
	if(strEncoded.indexOf("\'")>=0)	strEncoded.replace("\'","&apos;");
	if(strEncoded.indexOf("\"")>=0)	strEncoded.replace("\"","&quot;");

	//	for (uint iEntity=2; iEntity<5; ++iEntity)
	//		strEncoded.replace(rawEntity[iEntity],xmlEntity[iEntity]);

	return strEncoded;
}

/*!
	Avoid 1 time binary copy then QByteArray->fromBase64
	
	\param data		-external buffer with XMLRPC stream
	\param nStart	-external buffer with XMLRPC stream
	\param nStart	-external buffer with XMLRPC stream
	\return QByteArray binary
*/
QByteArray XmlUtil::fromBase64(QByteArray *data,quint64 nStart, quint64 nLength)
{

	quint64 nSize=nLength-nStart;
	quint64 nSizeTotal=nStart+nLength;

	unsigned int buf = 0;
	int nbits = 0;
	QByteArray tmp;
	tmp.resize((nSize * 3) / 4);

	int offset = 0;
	for (int i = nStart; i < nSizeTotal; ++i) {
		int ch = data->at(i);
		int d;

		if (ch >= 'A' && ch <= 'Z')
			d = ch - 'A';
		else if (ch >= 'a' && ch <= 'z')
			d = ch - 'a' + 26;
		else if (ch >= '0' && ch <= '9')
			d = ch - '0' + 52;
		else if (ch == '+')
			d = 62;
		else if (ch == '/')
			d = 63;
		else
			d = -1;

		if (d != -1) {
			buf = (buf << 6) | d;
			nbits += 6;
			if (nbits >= 8) {
				nbits -= 8;
				tmp[offset++] = buf >> nbits;
				buf &= (1 << nbits) - 1;
			}
		}
	}

	tmp.truncate(offset);
	return tmp;
}






//converts record set into the xmlrpc string stream
QByteArray XmlUtil::ConvertRecordSet2ByteArray(DbRecordSet &data)
{
	QByteArray out;
	XmlRpcValueDbRecordSet converter;
	converter.SetBuffer(&out);
	converter.Serialize(&data);
	return out;
}

//converts from xmlrpc string stream to recordset
DbRecordSet XmlUtil::ConvertByteArray2RecordSet(QByteArray &data)
{
	DbRecordSet out;
	XmlRpcValueDbRecordSet converter;
	converter.SetBuffer(&data);
	if(0 == data.size())
		return DbRecordSet();
	int nStart=0;
	Status err;
	converter.DeSerialize(nStart,data.size(),&out,err);
	Q_ASSERT(err.IsOK()); //must be ok
	return out;
}

//converts record set into the xmlrpc string stream
//WARNING: format not compatible with non fast ver
QByteArray XmlUtil::ConvertRecordSet2ByteArray_Fast(DbRecordSet &data)
{
	QByteArray out;
	XmlRpcValueDbRecordSet converter;
	converter.SetBuffer(&out);
	converter.SerializeFast(&data);
	return out;
}

//converts from xmlrpc string stream to recordset
//WARNING: format not compatible with non fast ver
DbRecordSet XmlUtil::ConvertByteArray2RecordSet_Fast(QByteArray &data)
{
	DbRecordSet out;
	XmlRpcValueDbRecordSet converter;
	converter.SetBuffer(&data);
	if(0 == data.size())
		return DbRecordSet();
	int nStart=0;
	Status err;
	converter.DeSerializeFast(nStart,data.size(),&out,err);
	Q_ASSERT(err.IsOK()); //must be ok
	return out;
}



