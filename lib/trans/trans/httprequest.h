/*!
\class  HTTPRequest
\ingroup TransportLayer
\author MR
\brief  Provides complete information on a single HTTP request packet

This class is a wrapper around QHttpRequestHeader, adding storage of HTTP body
(not only header), and some other convenient access methods.
*/
#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H

#include "qhttp.h"


class HTTPRequest : public QHttpRequestHeader
{
public:
	HTTPRequest();
    HTTPRequest(const QHttpRequestHeader &header, const QByteArray& body);
    ~HTTPRequest();

	QByteArray		*GetBody(){	return &m_body; } //convinient funct
	void			SetBody(QByteArray &body){	m_body = body; }
	void			SetData(const QHttpRequestHeader &header, const QByteArray& body);
	void			GetData(QByteArray& binaryPackage);

private:
    QByteArray m_body;
};

#endif // HTTPREQUEST_H

