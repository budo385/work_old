#ifndef HTTPFILESTREAMER_H
#define HTTPFILESTREAMER_H

#include "trans/trans/httpstreamhandler.h"
#include <QFile>

#define  HTTP_FILE_CHUNK_SIZE 500000 //< lower then TCP_CHUNK_SIZE !!!

class HTTPFileStreamer : public HTTPStreamHandler
{
public:
	HTTPFileStreamer();
	virtual ~HTTPFileStreamer();

	void	SetRequestHeader(const QHttpRequestHeader &header);
	void	SetResponseHeader(const QHttpResponseHeader &header);
	void	GetChunk(Status &pStatus, QByteArray &data,bool bInsertResponseHeaderInFirstChunk=false,bool bInsertRequestHeaderInFirstChunk=false);
	void	SetChunk(Status &pStatus, const QByteArray &data,bool bEof=false);
	void	InitFileOperation(Status &pStatus, QString strFileName, bool bWrite=false, bool bOverWrite=false);
	void	Close();
	qint64	GetFileSize(){return m_file.size();};
	bool	DeleteFile(){return m_file.remove();};
	qint64	GetDefaultChunkSize(){return HTTP_FILE_CHUNK_SIZE;};

private:
	QFile	m_file;
	bool	m_bCompressionActive;
	int		m_nSetChunkCnt;
	int		m_nGetChunkCnt;


};

#endif // HTTPFILESTREAMER_H
