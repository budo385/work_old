#include "httpreadersync.h"
#include <QCoreApplication>

//another thread is started, QHttp is alive in this thread, waiting for async calls!
HttpReaderSync::HttpReaderSync(int nTimeOut)
{
	m_nTimeOut=nTimeOut;
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	start(); //start thread
	m_ThreadSync.ThreadWait();			//prepare for start

}
//special method for destroying object in another thread
HttpReaderSync::~HttpReaderSync()
{
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit Destroy();
	m_ThreadSync.ThreadWait();			//prepare for start
	
	//stop thread gently:
	quit();
	wait();
}

//start Thread
void HttpReaderSync::run()
{
	m_HttpReader = new HttpReader(&m_ThreadSync,m_nTimeOut);
	connect(this,SIGNAL(ReadURL(QString,bool,int)),m_HttpReader,SLOT(ReadURL(QString, bool, int)));
	connect(this,SIGNAL(PostData(QString,QByteArray,bool,int)),m_HttpReader,SLOT(PostData(QString, QByteArray,bool, int)));
	
	connect(this,SIGNAL(Destroy()),m_HttpReader,SLOT(Destroy()));
	connect(m_HttpReader,SIGNAL(ReadInProgress(int,int)),this,SIGNAL(ReadInProgress(int,int)));
	m_ThreadSync.ThreadRelease();
	exec();
}




/*!
	Makes sync HTTP request, waits until response or error or Timeout (2sec def)

	\param pUrl		- URL of http page
	\param pContent	- returned content of http page
	\return			- false if error

*/
bool HttpReaderSync::ReadWebContent(QString strURL, QByteArray *pContent,bool bSSL,int nPort)
{

	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit ReadURL(strURL,bSSL,nPort);				
	
	while (!m_ThreadSync.ThreadWaitTimeOut(1000))
	{
		qApp->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
	}
	

	if (m_HttpReader->IsAuthRequired())
	{
		QAuthenticator auth;
		QString strHost=m_HttpReader->GetAuthRequiredHost();
		emit ReEmitAuthenticationRequired (strHost , 0 , &auth );
		m_HttpReader->SetAuthentication(&auth);
		//try again with auth:
		m_ThreadSync.ThreadSetForWait();	//prepare for wait
		emit ReadURL(strURL,bSSL,nPort);				
		while (!m_ThreadSync.ThreadWaitTimeOut(1000))
		{
			qApp->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
		}
	}
	
	return m_HttpReader->GetResponse(pContent);
}


HttpReaderSync::HttpReaderSync(const HttpReaderSync &that)
{
	operator = (that);
}
HttpReaderSync &HttpReaderSync::operator =(const HttpReaderSync &that)
{
	if(this != &that)
	{
		m_HttpReader= that.m_HttpReader;
		m_nTimeOut=that.m_nTimeOut;
		m_ThreadSync=that.m_ThreadSync;
	}
	return *this;
}

bool HttpReaderSync::PostWebContent( QString strURL, QByteArray pContentPost, QByteArray *pContentResponse,bool bSSL/*=false*/,int nPort/*=0*/ )
{

	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit PostData(strURL,pContentPost,bSSL,nPort);				

	while (!m_ThreadSync.ThreadWaitTimeOut(1000))
	{
		qApp->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
	}


	if (m_HttpReader->IsAuthRequired())
	{
		QAuthenticator auth;
		QString strHost=m_HttpReader->GetAuthRequiredHost();
		emit ReEmitAuthenticationRequired (strHost , 0 , &auth );
		m_HttpReader->SetAuthentication(&auth);
		//try again with auth:
		m_ThreadSync.ThreadSetForWait();	//prepare for wait
		emit ReadURL(strURL,bSSL,nPort);				
		while (!m_ThreadSync.ThreadWaitTimeOut(1000))
		{
			qApp->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
		}
	}

	return m_HttpReader->GetResponse(pContentResponse);

}



