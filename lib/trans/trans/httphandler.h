
#ifndef HTTPHANDLER_H
#define HTTPHANDLER_H

#include "common/common/status.h"
#include "httprequest.h"
#include "httpresponse.h"
#include "httpcontext.h"
#include "httpstreamhandler.h"


/*!
\class  HTTPHandler
\ingroup HTTPModule
\brief  This is a abstract class defining an object to handle HTTP packet

	Every HTTP handler must specify domain for which it will handle HTTP request
	E.g. when client posts requests: POST /rpc2/server.method
	Handler with domain=rpc2 will be invoked to handle this request, another one:
	POST /web/webserver.method	- handler with "web" domain will be used
	POST /						- handler with "/" will be used
	Note: when registering handlers to one dispatcher, dispatcher must ensure that there are no duplicate handlers assigned to same domain
	Use: when making handler, inherit this abstract class and implement HandleRequest() method, set domain

	Chunked: when http header tag Transfer-Encoding:chunked
	Then http body is sent in chunks (each chunk can be compressed):

	A chunked message body contains a series of chunks, followed by a line with "0" (zero), followed by optional footers (just like headers), and a blank line. Each chunk consists of two parts:
		* a line with the size of the chunk data, in hex, possibly followed by a semicolon and extra parameters you can ignore (none are currently standard), and ending with CRLF.
		* the data itself, followed by CRLF. 

	Implementation of chunked handler:
	1) Create pStreamHandler object and return back InitChunkedHttpStream()
	2) pStreamHandler->SetChunk() methos is invoked to write request data
	3) when last chunk is receivied pStreamHandler->SetChunk() and HandleRequest is also called with pStreamHandler
	4) if resonse is also chunked: pStreamHandler->GetChunk() will be called until buffer is >0
	5) Server socket thread is holding pStreamHandler object and it is separete for each connection: handler is responsible to create object, Server socket thread will delete it after request is processed!

*/
class HTTPHandler
{
public:
    HTTPHandler();
    virtual ~HTTPHandler();

	QString GetDomain(){ return m_strDomain; }
	void SetDomain(QString strValue){ m_strDomain = strValue.toLower(); }

	virtual void HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL) = 0;
	virtual bool InitChunkedHttpStream(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL){status.setError(StatusCodeSet::ERR_HTTP_HANDLER_NOT_SUPPORT_CHUNK);return false;};

	int m_nDefaultSocketTimeOut; //by default = 0, defines in minutes default socket timeout, 0 means, no timeout: delete when there is no activity
protected:    
	QString m_strDomain;	// each handler is tied to a single domain (like "RPC2")
};

#endif // HTTPHANDLER_H

