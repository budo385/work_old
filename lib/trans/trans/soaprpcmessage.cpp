#include "soaprpcmessage.h"
#include "xmlutil.h"





QtSoapStruct *SoapRpcMessage::DbRecordSetSerialize(DbRecordSet *pValue)
{

/*
	Q_ASSERT(m_byteXMLMsg!=NULL);

	int nRows=pValue->getRowCount();
	int nCols=pValue->getColumnCount();


	//create columns:
	for(int i=0;i<nCols;i++)
	{
		int nColumnType;
		QString strColName;
		pValue->getColumn(i,nColumnType,strColName);

		switch(nColumnType)
		{
		case QVariant::String:
			QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::String,nRows);
			lstSOAP_Arrays.append(arrayString);
			break;
		case QVariant::Int:
			QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::Integer,nRows);
			lstSOAP_Arrays.append(arrayString);
			break;
		case QVariant::ByteArray:
			QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::Base64Binary,nRows);
			lstSOAP_Arrays.append(arrayString);
			break;
		case QVariant::Boolean:
			QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::Boolean,nRows);
			lstSOAP_Arrays.append(arrayString);
			break;
		case QVariant::DateTime:
			QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::DateTime,nRows);
			lstSOAP_Arrays.append(arrayString);
			break;
		case QVariant::Date:
			QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::Date,nRows);
			lstSOAP_Arrays.append(arrayString);
			break;
			break;
		case QVariant::Time:
			QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::Time,nRows);
			lstSOAP_Arrays.append(arrayString);
			break;
			break;
		case QVariant::Double:
			QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::Double,nRows);
			lstSOAP_Arrays.append(arrayString);
			break;
		default:
			Q_ASSERT(0); //data type not supported!!
			return String;
			break;
		default:
			//check for default data type:
			if( data->userType()==DbRecordSet::GetVariantType())
			{
				QtSoapArray *arrayString=new QtSoapArray(QtSoapQName(strColName),QtSoapType::Array,nRows);
				lstSOAP_Arrays.append(arrayString);
			}
			else
				Q_ASSERT(0); //data type not supported!!
		}


	}

	//fill data:
	for(int i=0;i<nCols;i++)
	{
		data=&(pValue->getDataRef(i,j));
		switch(data->type())
		{
		case QVariant::String:
			QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),data->toString());
			lstSOAP_SimpleTypes.append(pSoapVal);
			m_SoapMsg.addMethodArgument(pSoapVal);
			break;

		case QVariant::Double:
			QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),data->toDouble());
			lstSOAP_SimpleTypes.append(pSoapVal);
			m_SoapMsg.addMethodArgument(pSoapVal);
			break;

		case QVariant::Bool:
			QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),data->toBool());
			lstSOAP_SimpleTypes.append(pSoapVal);
			m_SoapMsg.addMethodArgument(pSoapVal);
			break;

		case QVariant::Int:
			QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),data->toInt());
			lstSOAP_SimpleTypes.append(pSoapVal);
			m_SoapMsg.addMethodArgument(pSoapVal);
			break;

		case QVariant::ByteArray:
			QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),data->toByteArray());
			lstSOAP_SimpleTypes.append(pSoapVal);
			m_SoapMsg.addMethodArgument(pSoapVal);
			break;

		case QVariant::Date:
			QDateTime tmp=QDateTime(data->toDate(),QTime(0,0));
			QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),tmp);
			lstSOAP_SimpleTypes.append(pSoapVal);
			m_SoapMsg.addMethodArgument(pSoapVal);
			break;

		case QVariant::DateTime:
			QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),data->toDateTime());
			lstSOAP_SimpleTypes.append(pSoapVal);
			m_SoapMsg.addMethodArgument(pSoapVal);
			break;

		case QVariant::Time:
			QDateTime tmp=QDateTime(QDate::currentDate(),pValue->toTime());
			QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),tmp);
			lstSOAP_SimpleTypes.append(pSoapVal);
			m_SoapMsg.addMethodArgument(pSoapVal);
			break;

		default:
			//check for default data type:
			if( data->userType()==DbRecordSet::GetVariantType())
			{
				DbRecordSet childSet=(pValue->getDataRef(i,j)).value<DbRecordSet>();
				//xmlDbRecordSet.Serialize(&childSet);
			}
			else
				Q_ASSERT(0); //data type not supported!!

		}





	}

	QtSoapArray arrayString(QtSoapQName("COLUMN1_NAME"),QtSoapType::String,10);
	QtSoapArray arrayInt(QtSoapQName("COLUMN2_NAME"),QtSoapType::Integer,10);


	QtSoapSimpleType *pSoapVal=new QtSoapSimpleType(QtSoapQName(strParamName),*pValue());
	lstSOAP_SimpleTypes.append(pSoapVal);

	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlDbRecordSet.SerializeFast(pValue);
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;
	*/
	static QtSoapStruct data;
	return &data;
}