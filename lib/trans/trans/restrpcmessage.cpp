#include "restrpcmessage.h"
#include "restrpcvalue.h"
#include "xmlutil.h"
#include "trans/trans/config_trans.h"
#include "common/common/datahelper.h"


/*! METHOD REQUEST*/
static const QString REQUEST_TAG = "<REQUEST>";
static const QString REQUEST_ETAG = "</REQUEST>";
/*! XML RESPONSE*/
static const QString RESPONSE_TAG = "<RESPONSE>";
static const QString RESPONSE_ETAG = "</RESPONSE>";

static const QString PARAMS_TAG = "<PARAMETERS>";
static const QString PARAMS_ETAG = "</PARAMETERS>";
static const QString ERROR_TAG = "<ERROR>";
static const QString ERROR_ETAG = "</ERROR>";

/*! XML IDENTIFIER*/
static const QString XMLRPC_SIGNATURE = "SOKRATES_XP Module 0.2";
static const QString XML_VERSION ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";






/*! -------------------------MAIN INTERFACE--------------------------------*/

/*!
	Generates XML REST request message which is stored in the data() pointer to external buffer

	\param strMethodName				method calling: combination of http method + " "+relative url, e.g GET contacts/1
	\param pStatus						error if method name='' or params=0 
	\param strXmlBodyNamespaces			string will be appended to the XML body tag to declare namespace and instance, at least it must be xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
*/
void RestRpcMessage::GenerateRPC (QString strMethodName,Status& pStatus, QString strXmlBodyNamespaces){

	Q_ASSERT(m_byteXMLMsg!=NULL);//check if buffer is set

	//new test: total size can not exceed 80Mb:
	if (m_byteXMLMsg->size()>XML_MAXIMUM_BINARY_SIZE)
	{
		QString strSizeActual=DataHelper::GetFormatedFileSize((double)m_byteXMLMsg->size(),1);
		pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_SIZE_EXCEEDED,strSizeActual);
		return;
	}

	/* call must have method name && at least one parameter */
	if (m_byteXMLMsg->size()==0)
		return;
		//{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_GENERATE_FAIL);return;}


	/* build XML header: method name */
	m_byteXMLMsg->insert(0,XML_VERSION+"<REQUEST "+strXmlBodyNamespaces+">"+PARAMS_TAG+"\r\n");

	/* close XML msg */
	*m_byteXMLMsg+="\r\n"+PARAMS_ETAG+REQUEST_ETAG;
	pStatus.setError(0);
}


/*!
	Generates XMLRPC fault message: clears buffer
	\param nFaultCode error code
	\param strFaultText error text
*/
void RestRpcMessage::GenerateFault (int nFaultCode, QString strFaultText)
{
	/* build XML header: method response */
	m_byteXMLMsg->clear();
	*m_byteXMLMsg+=XML_VERSION+"<RESPONSE "+">""\r\n";

	RestRpcValue::Serialize_Error(m_byteXMLMsg,nFaultCode,strFaultText);

	/* build XML header: method response */
	*m_byteXMLMsg+="\r\n"+RESPONSE_ETAG;
}


/*!
	Overloaded function: uses Status object to fetch error text
	\param nFaultCode error code
*/

void RestRpcMessage::GenerateFault (int nFaultCode)
{
	Status err;
	err.setError(nFaultCode);

	GenerateFault(nFaultCode,err.getErrorText()); //no row msg..return text if possible
}



/*!
	Generates XMLRPC response message
	\param pStatus returned err inside or 0 if all OK
	\param strXmlBodyNamespaces XML namespace and instance added to first TAG element
	\param strAddOnXmlData is added after <parameters> element as is

*/
void RestRpcMessage::GenerateResponse (Status& pStatus, QString strXmlBodyNamespaces, QString strAddOnXmlData)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);//check if buffer is set

	//append header, body can contain parameters, but not necessary..only xml data...
	if (m_byteXMLMsg->size()>0)
	{
		m_byteXMLMsg->insert(0,XML_VERSION+"<RESPONSE "+strXmlBodyNamespaces+">"+PARAMS_TAG+"\r\n");
		*m_byteXMLMsg+=PARAMS_ETAG;
	}
	else
	{
		m_byteXMLMsg->insert(0,XML_VERSION+"<RESPONSE "+strXmlBodyNamespaces+">\r\n");
	}
	
	//add extra xml at end
	if (!strAddOnXmlData.isEmpty())
		*m_byteXMLMsg+=strAddOnXmlData.toLatin1();

	//close xml msg
	*m_byteXMLMsg+="\r\n"+RESPONSE_ETAG;
	pStatus.setError(0);
}


/*!
	Checks if XML message contains fault and returns it back:
	\param pStatus returned err inside or 0 if all OK
*/
void RestRpcMessage::CheckForFault (Status& pStatus)
{
	pStatus.setError(0);
	QDomNodeList errorLst = m_XmlResponseBody.elementsByTagName("ERROR");

	if (errorLst.size()!=1)
		return;

	RestRpcValue::DeSerialize_Error(errorLst.at(0),pStatus);
}

void RestRpcMessage::StartResponseParsing(Status& pStatus)
{
	m_XmlResponseBody.clear();
	QString strErrorMsg;
	int nLine;
	int nColumn;
	if (m_byteXMLMsg->isEmpty())
		return;
	m_XmlResponseBody.setContent(*m_byteXMLMsg,false,&strErrorMsg,&nLine,&nColumn);
	if (!strErrorMsg.isEmpty())
		pStatus.setError(StatusCodeSet::ERR_XML_BODY_PARSE_FAIL,QString::number(nLine)+";"+QString::number(nColumn)+";"+strErrorMsg);

}

//return node for parameter at pos POS, if not found then return FALSE
bool RestRpcMessage::GetParameterNode(QDomNode &node,quint16 nParamPosition)
{
	//extract parameters elements:
	QDomNodeList Parameters = m_XmlResponseBody.elementsByTagName("PARAMETERS");
	QDomNodeList ParameterList=Parameters.at(0).childNodes();

	if (nParamPosition>=ParameterList.size())
		return false;

	node=ParameterList.at(nParamPosition);
	return true;
}



/*!
	SetBuffer
	\param buffer reference to the external buffer
*/
void RestRpcMessage::SetBuffer(QByteArray* buffer)
{
	m_byteXMLMsg=buffer;
}


/*!
	Parameter count
	\return parameter count in buffer
*/
int RestRpcMessage::GetParameterCount()
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	//extract parameters elements:
	QDomNodeList Parameters = m_XmlResponseBody.elementsByTagName("PARAMETERS");
	QDomNodeList ParameterList=Parameters.at(0).childNodes();

	return ParameterList.size();
}

/*!
	Get Method name: for REST this is not possible when processing request (its on HTTP layer) and at response it does not make sense
	\param pStatus error if method TAGs doesnt exists
	\return method name
*/
QString RestRpcMessage::GetMethodName(Status &pStatus)
{
	return "";
}





/*! -------------------------ADD PARAMETER--------------------------------*/



/*!
Appends string at end of parameter list. If NULL sets <nil/>, 
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(QString* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);
	RestRpcValue::Serialize_String(m_byteXMLMsg,pValue,strParamName);
	return true;
}

/*!
Appends bool,int or double at end of parameter list
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(QVariant* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	switch(pValue->type())
	{
	case QVariant::Bool:

		RestRpcValue::Serialize_Bool(m_byteXMLMsg,pValue,strParamName);
		break;

	case QVariant::Int:

		RestRpcValue::Serialize_Integer(m_byteXMLMsg,pValue,strParamName);
		break;
	case QVariant::ULongLong:

		RestRpcValue::Serialize_ULongLong(m_byteXMLMsg,pValue,strParamName);
		break;

	case QVariant::Double:

		RestRpcValue::Serialize_Double(m_byteXMLMsg,pValue,strParamName);

		break;

	default:
		return false;
	};
	return true;
}




/*!
Appends bool at end of parameter list
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(bool* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	QVariant var(*pValue);
	RestRpcValue::Serialize_Bool(m_byteXMLMsg,&var,strParamName);
	return true;
}

/*!
Appends int at end of parameter list
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(int* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	QVariant var(*pValue);
	RestRpcValue::Serialize_Integer(m_byteXMLMsg,&var,strParamName);
	return true;

	return true;
}

bool RestRpcMessage::AddParameter(qulonglong* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	QVariant var(*pValue);
	RestRpcValue::Serialize_ULongLong(m_byteXMLMsg,&var,strParamName);
	return true;

	return true;
}

/*!
Appends double at end of parameter list
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(double* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	QVariant var(*pValue);
	RestRpcValue::Serialize_Double(m_byteXMLMsg,&var,strParamName);
	return true;
}





/*!
Appends date at end of parameter list. If NULL sets <nil/>
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(QDate* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	RestRpcValue::Serialize_Date(m_byteXMLMsg,pValue,strParamName);
	return true;
}

/*!
Appends DateTime at end of parameter list. If NULL sets <nil/>
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(QDateTime* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	RestRpcValue::Serialize_DateTime(m_byteXMLMsg,pValue,strParamName,m_nTimeZoneOffsetSeconds);
	return true;
}

/*!
Appends time at end of parameter list. If NULL sets <nil/>
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(QTime* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	RestRpcValue::Serialize_Time(m_byteXMLMsg,pValue,strParamName);
	return true;
}

/*!
Appends binary at end of parameter list. If NULL sets <nil/>
\param pValue value for xml convert
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(QByteArray* pValue,QString strParamName)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	RestRpcValue::Serialize_Binary(m_byteXMLMsg,pValue,strParamName);
	return true;
}


/*!
Appends record set at end of parameter list. If NULL see record set
\param pValue value for xml convert
Warning: can not use this parameter if using nested RecordSets->ASSERT will be fired
Warning: on other side, accepting recordset must be defined before getting parameter
\return false if type invalid
*/
bool RestRpcMessage::AddParameter(DbRecordSet* pValue,QString strParamName, const QString strRowName, const QString strRowURLAttr,QString strRowURLAttr_IDField)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	RestRpcValue::Serialize_DbRecordSet(m_byteXMLMsg,pValue,strParamName,strRowName,strRowURLAttr,strRowURLAttr_IDField);
	return true;
}




/*! -------------------------GET PARAMETER--------------------------------*/


/*!
	Gets STRING parameter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, QString* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	RestRpcValue::DeSerialize_String(node,pValue,pStatus);
};


/*!
	Gets INT, BOOL or DOUBLE paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, QVariant* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}


	switch(pValue->type())
	{
	case QVariant::Bool:
		RestRpcValue::DeSerialize_Bool(node,pValue,pStatus);
		break;

	case QVariant::Int:
		RestRpcValue::DeSerialize_Integer(node,pValue,pStatus);
		break;

	case QVariant::ULongLong:
		RestRpcValue::DeSerialize_ULongLong(node,pValue,pStatus);
		break;

	case QVariant::Double:
		RestRpcValue::DeSerialize_Double(node,pValue,pStatus);
		break;

	default:
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}
	};

};


/*!
	Gets BOOL  paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, bool* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	QVariant var;
	RestRpcValue::DeSerialize_Bool(node,&var,pStatus);
	if(pStatus.IsOK())
		*pValue=var.toBool();
}



/*!
	Gets INT  paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, int* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	QVariant var;
	RestRpcValue::DeSerialize_Integer(node,&var,pStatus);
	if(pStatus.IsOK())
		*pValue=var.toInt();
}


void RestRpcMessage::GetParameter(quint16 nPosition, qulonglong* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
	{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
	{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	QVariant var;
	RestRpcValue::DeSerialize_ULongLong(node,&var,pStatus);
	if(pStatus.IsOK())
		*pValue=var.toULongLong();
}




/*!
	Gets DOUBLE  paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, double* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	QVariant var;
	RestRpcValue::DeSerialize_Double(node,&var,pStatus);
	if(pStatus.IsOK())
		*pValue=var.toDouble();
}






/*!
	Gets DATE paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, QDate* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	RestRpcValue::DeSerialize_Date(node,pValue,pStatus);
};


/*!
	Gets TIME paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, QTime* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	RestRpcValue::DeSerialize_Time(node,pValue,pStatus);
};


/*!
	Gets DATETIME paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, QDateTime* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	RestRpcValue::DeSerialize_DateTime(node,pValue,pStatus,NULL,m_nTimeZoneOffsetSeconds);
};



/*!
	Gets BINARY paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
*/
void RestRpcMessage::GetParameter(quint16 nPosition, QByteArray* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	RestRpcValue::DeSerialize_Binary(node,pValue,pStatus);
};



/*!
	Gets RecordSet parameter from specified position from xml stream and convert to value

	\param nPosition parameter position (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
	
	but pValue must be then defined to match incoming data, if not ASSERT or error in pStatus will be returned

	Warning: if false then pValue recordset will be destroyed and completly rebuilded
*/
void RestRpcMessage::GetParameter(quint16 nPosition, DbRecordSet* pValue, Status& pStatus)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);

	if (m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//qDebug()<<m_byteXMLMsg->constData();
	//qDebug()<<m_XmlResponseBody;

	QDomNode node;
	if(!GetParameterNode(node,nPosition))
		{pStatus.setError(StatusCodeSet::ERR_XML_PARAM_MISSING,QString::number(nPosition));return;}

	RestRpcValue::DeSerialize_DbRecordSet(node,pValue,pStatus);
};






