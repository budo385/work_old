#include "httpclientconnection.h"
#include "httpclientsockethandler.h"
#include "common/common/threadid.h"
#include "common/common/loggerabstract.h"
//#include "

/*!
	Constructor: starts socket thread
*/
HTTPClientThread::HTTPClientThread()
	: QThread(NULL),m_bSocketIsInErrorState(false),m_StreamHandler(NULL)
{
	m_BufferSemaphore= new QSemaphore(1); //for shared resource: buffer
	m_bConnected=false;

	InvalidateBuffers(); // sets buffers to NULL

	m_ThreadSync.ThreadSetForWait();
	start(); //start thread
	m_ThreadSync.ThreadWait(); //wait until thread is started

}


/*!
	Destructor: stops socket thread
*/
HTTPClientThread::~HTTPClientThread()
{

	//m_ThreadSync.ThreadSetForWait();	//prepare thread for wait condition
	emit SocketDestroy();				//signal socket to init auto-destruct sequence
	//m_ThreadSync.ThreadWait();		//wait until thread is stopped
	wait(100);

	if(isRunning())
	{
		quit();		//stop thread
		bool bOK=wait(10000);		//wait 10sec
		if (!bOK)
		{
			//qDebug()<<"thread is still running after quit+wait"<<ThreadIdentificator::GetCurrentThreadID();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTPClientThread: Client socket terminated after 10 seconds"));
			terminate();
		}
		else
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTPClientThread: Client socket destroyed"));
	}
	

	delete m_BufferSemaphore;



}

/*!
	Socket thread main loop
*/
void HTTPClientThread::run()
{

	//create socket object that will live in thread //destroyed by SocketDisconnect() signal
	HTTPClientSocketHandler* pSocketHandler=new HTTPClientSocketHandler(this,m_BufferSemaphore,&m_ThreadSync,&m_waitCond);


	//connect main & this worker thread with signal/slot technology:
	connect(this,SIGNAL(SocketConnect()),pSocketHandler,SLOT(SocketConnect()),Qt::QueuedConnection);
	connect(this,SIGNAL(SocketDisconnect()),pSocketHandler,SLOT(SocketDisconnect()),Qt::QueuedConnection);
	connect(this,SIGNAL(SocketDestroy()),pSocketHandler,SLOT(SocketDestroy()),Qt::QueuedConnection);
	connect(this,SIGNAL(SocketClientRequest()),pSocketHandler,SLOT(SocketWrite()),Qt::QueuedConnection);
	connect(pSocketHandler,SIGNAL(ServerMessageReceived(int)),this,SLOT(ServerMessageReceived(int)));
	//connect(this,SIGNAL(SocketAbort()),pSocketHandler,SLOT(OnAbort()),Qt::QueuedConnection);
	
	//connect(pSocketHandler,SIGNAL(SocketErrorDetected(int)),this,SLOT(SocketErrorDetected(int)));

	m_ThreadSync.ThreadRelease();//release thread

	exec(); //start event loop
}


/*!
	Connect to server based on given conn. settings. Error if already connected
	/param pStatus, 0 if OK
	/param pConnSettings contains connection settings
*/
void HTTPClientThread::Connect(Status& pStatus,HTTPClientConnectionSettings& pConnSettings)
{

	//serialize access
	QMutexLocker locker(&m_Mutex); //lock acces

	m_bSocketIsInErrorState=false;  //reset flag here as obviously master is trying to recover from error state

	SetError(0); //reset error

	SetConnectionSettings(pConnSettings); 		//write conn. settings:
	if (pConnSettings.m_strServerIP.isEmpty())	//if empty fire TimeOut error at once
	{
		pStatus.setError(StatusCodeSet::ERR_HTTP_HOST_NOT_FOUND);
		return;
	}
	
	m_ThreadSync.ThreadSetForWait();		//prepare for wait
	emit SocketConnect();	//send connect request:
	m_ThreadSync.ThreadWait();			//wait until something is come back:
	
	pStatus=m_LastError; 	//check error:
	//because of wild threads, check state once again, if not connected set error:
	if(!IsConnected() && pStatus.IsOK())
		pStatus.setError(StatusCodeSet::ERR_HTTP_HOST_CLOSED);

}



/*!
	Disconnect from server: wait for current operation
*/
void HTTPClientThread::Disconnect(Status& pStatus)
{

	//serialize access
	QMutexLocker locker(&m_Mutex); //lock acces

	SetError(0); //reset error
	
	m_ThreadSync.ThreadSetForWait();		//prepare for wait
	emit SocketDisconnect();				//send disconnect request:
	m_ThreadSync.ThreadWait();				//wait until something is come back:
	
	
	pStatus=m_LastError; 		//check error:
	
	//if normal error (host disconnect, ignore),
	if(pStatus.getErrorCode()==StatusCodeSet::ERR_HTTP_HOST_CLOSED)
		pStatus.setError(0);


	//if all ok, reset flag
	if(pStatus.IsOK())
		m_bSocketIsInErrorState=false;
}



/*!
	Send request to the server, always SYNC calls
	/param pStatus, 0 if OK
	/param pHTTPRequest client request
	/param HTTPResponse server response, 
	/param pStreamHandler for stream write of request use this object: pHTTPRequest can only contain header...
	
*/
void HTTPClientThread::SendData(Status& pStatus, HTTPRequest* pHTTPRequest, HTTPResponse* pHTTPResponse,HTTPStreamHandler *pStreamHandler)
{
	QMutexLocker locker(&m_Mutex);				
	if(m_bSocketIsInErrorState)  return;		
	m_bIsChunkedTransfer=false;
	
	
	m_bCancel=false;
	SetDataTransferParametersAndCheckIsCancel(0,0,false); 
	SetError(0);					

	Q_ASSERT(pHTTPRequest!=NULL); 

	if (pStreamHandler)
		if (pHTTPRequest->value("Transfer-Encoding")=="chunked") //only if chunked request
			m_bIsChunkedTransfer=true;

	m_StreamHandler=pStreamHandler;
	m_RequestBuffer=pHTTPRequest;	
	m_ResponseBuffer=pHTTPResponse;
	
	//qDebug()<<"START thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" -"<<pHTTPRequest->toString();;

	//---------------------------------------------------
	//					SEND REQUEST
	//---------------------------------------------------

	//Q_ASSERT(m_BufferSemaphore->available()==1); //buffer must be available before making request...

	m_BufferSemaphore->acquire(1);			//lock buffers
	//qDebug()<<"B(+) thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" pointer m_BufferSemaphore: "<<m_BufferSemaphore;
	emit SocketClientRequest();				//emit signal that request is ready for send (this can trigger thread switch: it can write & read socket in one pass)

	//---------------------------------------------------
	//				SYNC MODE
	//---------------------------------------------------

//	qDebug()<<m_BufferSemaphore->available();
	if(m_BufferSemaphore->available()==0) //if Write & Read on socket is already executed, skip this, (buffers will be unlocked by socket)
	{
		int nWaitTicks=0; //can be used for progress bar;
		while (!m_waitCond.wait(&m_Mutex,250))//wait for socket to notify or wait 1sec, which comes first
		{
			//issue 1061:if cancel abort all, clear socket
			//if (!IsCanceled())
			//{
				//emit progress signal & process events (refresh GUI)
				nWaitTicks++;
				qint64 done,total;
				GetDataTransferParameters(done,total);
				emit SocketOperationInProgress(nWaitTicks,done,total);
				QCoreApplication::instance()->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
			//}
			//else
			//{
			//	emit SocketAbort();
			//	msleep(100);
			//	QCoreApplication::instance()->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
			//}
			if(m_BufferSemaphore->available()==1) //when we exited wait() condition, socket maybe finish job, check it (buffers will be unlocked by socket)
				break; //exit loop if buffers unlocked
		}
	}

	//socket thread has something for us: read status+response buffer:
	if (!m_bCancel)
		pStatus=m_LastError;
	else
		pStatus.setError(StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED);

	InvalidateBuffers();	//clear pointers, socket already stored result in buffer in pHTTPResponse
	Q_ASSERT(m_BufferSemaphore->available()==1); //buffer must be available;
	emit SocketOperationInProgress(0,0,0); //reset progress to 0:
	m_StreamHandler=NULL;

	//qDebug()<<"END thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" -"<<pHTTPResponse->toString();

}




/*------------------------------------------------------------------------*/
/*							CONNECTION SETTINGS							  */	
/*------------------------------------------------------------------------*/


/*!
	Used by socket thread to signal error
	\param pErrorCode 
	\param pErrorTxt 
*/
void HTTPClientThread::SetError(int pErrorCode, QString rawText)
{
	QWriteLocker locker(&m_RWLock);
	m_LastError.setError(pErrorCode,rawText);
}


/*!
	Used by main thread to set connection data for socket thread
	\param pConnSettings 
*/
void HTTPClientThread::	SetConnectionSettings(const HTTPClientConnectionSettings& pConnSettings)
{
	QWriteLocker locker(&m_RWLock);
	m_ConnSettings=pConnSettings;
}


/*!
	Used by slave thread to set connected state
*/
void HTTPClientThread::SetConnected(bool bConnected)
{
	QWriteLocker locker(&m_RWLock);
	m_bConnected=bConnected;
}


bool HTTPClientThread::IsConnected()
{
	QReadLocker locker(&m_RWLock);
	return m_bConnected;
}




/*------------------------------------------------------------------------*/
/*						  REQUEST/RESPONSE BUFFERS						  */
/*------------------------------------------------------------------------*/


/*!
	Used by socket thread to write response message from server
	
	/param pStatus		can return error: socket must abort operation...as canceled..
	/param header		response header
	/param pBuffer		incoming stream (without header)
	/param bLastPart	if false then chunked transfer is in progress and 
*/
void HTTPClientThread::WriteResponseBuffer(Status &pStatus, const QHttpResponseHeader &header, const QByteArray &pBuffer)
{
	if (m_ResponseBuffer==NULL)	
		return;
	m_ResponseBuffer->SetData(header,pBuffer);
}

//only if chunked response, use chunker (header is stored in response bufer, body is written by chunker)
void HTTPClientThread::WriteResponseBufferChunked(Status &pStatus, const QHttpResponseHeader &header, const QByteArray &pBuffer, bool bLastPart)
{
	if (m_ResponseBuffer==NULL)	
		return;
	if (m_StreamHandler==NULL)	
		return;

	m_StreamHandler->SetChunk(pStatus,pBuffer,bLastPart);
	if (bLastPart && m_ResponseBuffer!=NULL)
		m_ResponseBuffer->SetData(header,QByteArray()); //set response header at last (no body->written by chunker)

}


/*!
	Returns HTTPRequest in binary format (header+body), compresses body and sets tags if needed
	
	/param pStatus		can return error: socket must abort operation...as canceled..
	/param pBuffer		returned HTTP request message (whole or part if chunked)
*/
void HTTPClientThread::GetRequestBuffer(Status &pStatus,QByteArray& pBuffer)
{
	if (m_RequestBuffer==NULL)
		return;

	if (m_bIsChunkedTransfer)
	{
		m_StreamHandler->GetChunk(pStatus,pBuffer,false,true);
		return;
	}
	
	m_RequestBuffer->GetData(pBuffer);
}

bool HTTPClientThread::IsChunkedTransfer()
{
	return m_bIsChunkedTransfer;
}
void HTTPClientThread::InitChunkedHttpStream(Status &status,const QHttpResponseHeader &header)
{
	status.setError(0);
	if (m_StreamHandler)
	{
		m_bIsChunkedTransfer=true;
		m_StreamHandler->SetResponseHeader(header);
		return;
	}

	status.setError(StatusCodeSet::ERR_HTTP_HANDLER_NOT_SUPPORT_CHUNK);
}



/*------------------------------------------------------------------------*/
/*								  SERVER MSG QUEUE						  */
/*------------------------------------------------------------------------*/


/*!
	Used by socket thread to add new message to server message queue, message is stored as HTTPRequest and decompressed if compression is detected
	/param pBuffer 
	/return ID of new message in the server msg queue
*/
int HTTPClientThread::WriteServerMsgQueue(const QHttpRequestHeader &header,const QByteArray& pbody)
{
	m_RWLock.lockForWrite();
	int nLastPos=m_ServerMessageBuffer.size(); 	//find maximum msg ID
	HTTPRequest tempMsg;
	tempMsg.SetData(header,pbody);
	m_ServerMessageBuffer.insert(nLastPos,tempMsg); //insert at last place new server message
	m_RWLock.unlock();
	return nLastPos;
}

/*!
	Used by main thread to fetch new message
	/param nServerMsgID id of message to fetch, if not found returns empty request HTTP header
	/param pMessage ref to new message, if not found returns empty request HTTP header
*/
void HTTPClientThread::GetServerMsg(int nServerMsgID,HTTPRequest& pHTTPMessage)
{
	m_RWLock.lockForRead();
	pHTTPMessage=m_ServerMessageBuffer.value(nServerMsgID,HTTPRequest()); //if not found return empty HTTP msg
	m_RWLock.unlock();
}


/*!
	SLOT: used by main thread to empty server message queue
	/param nServerMsgID id of message to delete, if -1 then clear whole buffer
*/
void HTTPClientThread::DeleteServerMsgQueue(int nServerMsgID)
{
	m_RWLock.lockForWrite();
	if (nServerMsgID==-1)
		m_ServerMessageBuffer.clear();//clear whole buffer
	else
		m_ServerMessageBuffer.remove(nServerMsgID); //remove message from que
	m_RWLock.unlock();
}


/*!
	SLOT: invoked by socket thread when new message arrives.
	Anaylize domain, redirect call to the handler with right domain, if no handler is registered for incoming msg, deletes msg from queue
	When handler process masg, msg is deleted from queue
	NOTE: only ONE HTTPHandler can be registered for one domain!
	/param nServerMsgID id of server message in queue
*/
void HTTPClientThread::ServerMessageReceived(int nServerMsgID)
{
	Status err;
	HTTPContext  ctx;
	HTTPResponse rep;

	//getMessage from queue
	HTTPRequest httpServerMsg=m_ServerMessageBuffer.value(nServerMsgID,HTTPRequest());
	if(httpServerMsg.GetBody()->size()==0)return; //no message found

	//find handler with right domain
	QString strDomain;
	HTTPUtil::GetDomain(httpServerMsg,strDomain);
	HTTPHandler *pHandler = NULL;
	for(int i=0; i<m_pLstHandlers.size(); i++)
	{
		if(m_pLstHandlers.at(i)->GetDomain() == strDomain){
			pHandler = m_pLstHandlers.at(i);
			break;
		}
	}

	//send server message for process to right handler. We dont care about status, context and response
	if(pHandler!=NULL)
		pHandler->HandleRequest(err,ctx,httpServerMsg,rep);
	
	//delete msg from queue
	DeleteServerMsgQueue(nServerMsgID);
}




/*------------------------------------------------------------------------*/
/*						  PRIVATE FUNCTIONS								  */
/*------------------------------------------------------------------------*/


/*!
	Used by main thread to invalidate buffers
*/
void HTTPClientThread:: InvalidateBuffers()
{
	m_ResponseBuffer=NULL;
	m_RequestBuffer=NULL;
}



/*!
	Called directly by slave socket to notify about connection errors while master thread was 
	not using socket thread

	If master thread is busy (locked) it will not raise error
	Sets flag: m_bSocketIsInErrorState: only disconect is allowed now
*/
void HTTPClientThread::SocketErrorDetected(int nErrorCode,QString rawText)
{
	//just fly by (but only if mutex is free: if master thread is free):
	if (m_Mutex.tryLock()) 
	{
		if (m_bSocketIsInErrorState || !IsConnected()) //if already error or not connect ignore..(socket probably empties event que from last communication)
		{
			m_Mutex.unlock(); 
			return;
		}
		else
		{
			m_bSocketIsInErrorState=true;				//this flag ensures that socket is blocked until destruction!
			m_Mutex.unlock(); 
			emit SocketReportedError(nErrorCode,rawText);
		}
	}
}

bool HTTPClientThread::SetDataTransferParametersAndCheckIsCancel(qint64 done,qint64 total, bool bRead)
{
	if (m_bIsChunkedTransfer)
	{
		total=m_StreamHandler->GetTotalStreamSize();
		if (bRead)
			done=m_StreamHandler->GetBytesProcessed()+done;
		else
			done=m_StreamHandler->GetBytesProcessed()-m_StreamHandler->GetDefaultChunkSize()+done;

		if (done>total)done=total;
	}

	QWriteLocker lock(&m_RWLock);
	m_Done=done;
	m_Total=total;
	return m_bCancel;
	//qDebug()<<"socket read:"<<bRead<<" done:"<<done<<" / total: "<<total;
}
void HTTPClientThread::GetDataTransferParameters(qint64 &done,qint64 &total)
{
	QReadLocker lock(&m_RWLock);
	done=m_Done;
	total=m_Total;
}

void HTTPClientThread::CancelOperation()
{
	QWriteLocker lock(&m_RWLock);
	m_bCancel=true;
}

bool HTTPClientThread::IsCanceled()
{
	QReadLocker lock(&m_RWLock);
	return m_bCancel;
}

