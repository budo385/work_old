#include "httpserver.h"
#include <QWriteLocker>
#include <QReadLocker>
#include "httpserversockethandler.h"
#include "common/common/loggerabstract.h"
#include "common/common/threadid.h"
#include <QSslConfiguration>
#include <QSslCertificate>
#include <QCoreApplication>
#include "trans/trans/config_trans.h"

/*!
	Constructor
*/
HTTPServer::HTTPServer(QObject *parent)
	: QTcpServer(parent),m_pIPKickBanManager(NULL),m_RWLock(QReadWriteLock::Recursive) //if somehow some signal is thrown back & fort between server and socket
{
	m_bUseKickBan=false;
	setMaxPendingConnections(10000); //10000 can be issued in 1 sec-> this is not relevant as we create our  QTCPSocket!!
}

/*!
	Destructor: stops server before destroy
*/
HTTPServer::~HTTPServer()
{
	if (m_pIPKickBanManager) delete m_pIPKickBanManager;
	StopServer();
}


/*------------------------------------------------------------------------*/
/*								  START / STOP							  */
/*------------------------------------------------------------------------*/



/*!
	Inits server based on parameters in pConnSettings
	
	\param		- pConnSettings			- http serve settings
	\param		- bUseKickBan			- if true then check every IP address against those two list, and enable dynamic IP kicking
	\param		- lstKickBanStatic		- static kickban list set by admin
	\param		- lstKickBanDynamic		- dynamic kickban list built by self learning

*/
void HTTPServer::InitServer(HTTPServerConnectionSettings pConnSettings,bool bUseKickBan,DbRecordSet *lstKickBanStatic,DbRecordSet *lstKickBanDynamic)
{	
	//set conn settings
	m_ConnSettings=pConnSettings;
	m_bUseKickBan=bUseKickBan;

	//KICK Ban protocol:
	if (m_bUseKickBan)
	{
		m_pIPKickBanManager = new IPKicker;
		m_pIPKickBanManager->Initialize(m_ConnSettings.m_nKickPeriodAfterIntrusion*60); //in minutes (10 is default)
		if(lstKickBanDynamic!=NULL)
			m_pIPKickBanManager->SetDynamicList(*lstKickBanDynamic);
		if(lstKickBanStatic!=NULL)
			m_pIPKickBanManager->SetStaticList(*lstKickBanStatic);
	}

	//qDebug()<<maxPendingConnections();
	connect(this,SIGNAL(acceptError(QAbstractSocket::SocketError)),this,SLOT(OnAcceptError(QAbstractSocket::SocketError)));
}

/*!
	Stops server, destroy all connections. Sequence:
	1. do not accept new connection, stop listener
	2. destroy socket one by one (be nice, wait for them to finish whatever they do)
	NOTE: only keep settings + registered handlers
*/
void HTTPServer::StopServer(){
	StopListen();
	CloseConnection(-1);
}


/*!
	Stops listening, do not accept any new connections
	NOTE: existing connections are still active
*/
void HTTPServer::StopListen()
{
	close();
}


/*!
	Starts listening, if failed or conn. settings not provided, returns error
	/param pStatus, 0 if OK
*/
void HTTPServer::StartListen(Status& pStatus, int nPort){
	
	//set handlers:
	//HTTPServerSocketHandler::SetRequestHandlers(m_pLstHandlers);

	//if already running, return error:
	if(isListening()){pStatus.setError(StatusCodeSet::ERR_HTTP_SERVER_ALREADY_RUNNING);return;}

	//use SSL: load certificates and set defaults...
	if(m_ConnSettings.m_bUseSSL)
	{
		CheckSSLCertificate(pStatus);
		if(!pStatus.IsOK())return;
	}

	//issue: even if server is not SSL this is needed to communicate push message via Windows Server
	InitSSL(pStatus);
	if(!pStatus.IsOK())return;
	
	//MB: dynamically change port....jupiiiii
	if (nPort!=-1)
	{
		m_ConnSettings.m_nPort=nPort;
	}

	//start the server
	if(!listen(QHostAddress(m_ConnSettings.m_strServerIP),m_ConnSettings.m_nPort))
	{
		pStatus.setError(StatusCodeSet::ERR_HTTP_SERVER_FAILED_TO_START,m_ConnSettings.m_strServerIP+";"+QVariant(m_ConnSettings.m_nPort).toString()+";"+errorString());
	}

 	else
	{
		//qDebug()<<"Server started listening at port: "<< m_ConnSettings.m_nPort<<" using SSL:"<<m_ConnSettings.m_bUseSSL;
		pStatus.setError(0);
	}

}




/*------------------------------------------------------------------------*/
/*						  INTERNAL THREAD HANDLERS						  */
/*------------------------------------------------------------------------*/


/*!
	Inhereted function from QTCPServer, client made connection, socket is created (socketDescriptor)
	/param socketDescriptor unique ID of socket
*/
void HTTPServer::incomingConnection(int socketDescriptor) 
{
	//qDebug()<<"incoming "<<socketDescriptor <<" thread: "<<ThreadIdentificator::GetCurrentThreadID();
	HTTPServerThread *pNewConnection = new HTTPServerThread(this,socketDescriptor,m_ConnSettings,GetHandlers(),m_pIPKickBanManager,m_ConnSettings.m_nIntrusionTries);
	pNewConnection->start();
} 


/*!
	Invoked by client thread itself, registers client thread in list. Called from thread constructor!
	NOTE: multithread access
	/param nSocketID socket ID
	/param pClientThread - pointer to client thread
*/
void HTTPServer::OnThreadCreated(int nThreadID,HTTPServerThread* pClientThread)
{
	QWriteLocker locker(&m_RWLock);
	m_ClientThreadList.insert(nThreadID,pClientThread);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTPServer: Inserting client thread %1 assign to socket %2 in list, number of active threads: %3").arg(nThreadID).arg(pClientThread->GetSocketID()).arg(m_ClientThreadList.size()));
}


/*!
	Invoked by client thread itself, unregisters client thread in list. Called from thread destructor!
	NOTE: multithread access
	/param nSocketID socket ID
*/

//already inside locker
void HTTPServer::UnRegisterClientThread(int nThreadID)
{
	//qDebug()<<"Client connection closed from socketID"<< nSocketID;
	//QReadWriteLock(m_RWLock);
	m_ClientThreadList.remove(nThreadID);
}

//already inside locker
int HTTPServer::GetNumberOfClientThreads()
{
	//QReadWriteLock(m_RWLock);
	QReadLocker locker(&m_RWLock);
	return m_ClientThreadList.size();
}

void HTTPServer::GetSocketList(ClientConnections &ClientThreadList)
{
	QReadLocker locker(&m_RWLock);
	ClientThreadList=m_ClientThreadList;
}

int HTTPServer::GetActiveSocketsCount()
{
	return m_ClientThreadList.size();
}

/*!
	Invoked by client thread itself, unregisters client thread in list. Called from thread destructor!
	NOTE: multithread access

	\param nSocketID socket ID
*/
void HTTPServer::OnThreadDestroyed(int nThreadID)
{
	QWriteLocker locker(&m_RWLock);
	m_ClientThreadList.remove(nThreadID);
	qDebug()<<"thread pool size"<<m_ClientThreadList.size();
	locker.unlock(); //unlock before emitting signal
	emit ClientThreadIsDead(nThreadID);
}



/*------------------------------------------------------------------------*/
/*								  SEND MESSAGE							  */
/*------------------------------------------------------------------------*/


/*!
	Sends HTTPRequest message to client (async-do not wait for response). 
	Do not check for errors or if client received msg. Multithread safe!
	/param nSocketID - socket to which server will send message, if -1 then broadcast to all connected clients!!
					   
					   Broadcasting message:
					   
					   1st Warning: this action can take long time, beacuse sending of msg will wait until client socket is
					   free for use (if currently processing client request, calling thread will be put on hold).
					   This precaution measure is only to protect request buffers to be overwritten when multiple threads
					   calling this function!
					   
					   2nd Warning: when iterating through lists of threads, list must be locked, this pratctically means
					   that any new connections will wait until broadcast sequence is over, or when destroying socket, socket will
					   live until access to list is fred!

	/param pRequest - HTTP request
*/

HTTPContext HTTPServer::GetThreadContext(int nThreadID)
{
	QReadLocker lock(&m_RWLock);

	HTTPServerThread *pThread= m_ClientThreadList.value(nThreadID,NULL);
	if (pThread)
		return pThread->GetConnectionContext();
	else
		return HTTPContext();
}


void HTTPServer::SendMessage(int nSocketID, HTTPRequest* pRequest)
{
	Status lastErr;
	
	//serialize access to client list
	m_RWLock.lockForRead();

	if(nSocketID==-1)//send to all clients
	{
		ClientConnectionsIterator i(m_ClientThreadList);
		while (i.hasNext()) {
			i.next();
			i.value()->SendData(lastErr,pRequest);
		}
    }
	else			//send to one
	{
		ClientConnectionsIterator i(m_ClientThreadList);
		while (i.hasNext()) 
		{
			i.next();
			if (nSocketID == i.value()->GetSocketID())
			{
				i.value()->SendData(lastErr,pRequest); //send message
				break;
			}
		}
	}

	//unlock
	m_RWLock.unlock();
}



/*!
	This is an overloaded member function, provided for convenience. 
	It behaves essentially like the above function but takes list of socket ID's.
	/param lstSocketID - list of socket ID's socket to which server will send message
	/param pRequest - HTTP request
*/

void HTTPServer::SendMessage(QList<int> lstSocketID, HTTPRequest* pRequest){
	Status lastErr;
	
	//serialize access to client list
	m_RWLock.lockForRead();

	ClientConnectionsIterator i(m_ClientThreadList);
	while (i.hasNext()) 
	{
		i.next();
		if (lstSocketID.contains(i.value()->GetSocketID()))
		{
			i.value()->SendData(lastErr,pRequest); //send message
		}
	}

	m_RWLock.unlock();
}
	

/*!
This is an overloaded member function, provided for convenience. 
It behaves essentially like the above function but takes list of socket ID's.
/param lstSocketID - list of socket ID's socket to which server will send message
/param pRequest - HTTP request
*/

void HTTPServer::SendMessageBroadCast(QByteArray* pBroadCastMsgBuffer)
{
	Status lastErr;

	//serialize access to client list
	m_RWLock.lockForRead();

	ClientConnectionsIterator i(m_ClientThreadList);
	while (i.hasNext()) {
		i.next();
		i.value()->SendDataBroadCast(lastErr,pBroadCastMsgBuffer);
	}

	//unlock
	m_RWLock.unlock();

}

/*------------------------------------------------------------------------*/
/*							  CLOSE CONNECTIONS							  */
/*------------------------------------------------------------------------*/

/*!
	Close connection of one or more client connections: destroys client 
	socket&thread in safe way, waits if any ongoing operation is taking place!
	/param nSocketID - socket or client to disconnect, if -1 then all!
*/
bool HTTPServer::CloseConnection(int nThreadID){

	//serialize access to client list
	QWriteLocker locker(&m_RWLock);
	if(nThreadID==-1)//send to all clients: force delete
	{
		ClientConnectionsIterator i(m_ClientThreadList);
		while (i.hasNext()) 
		{
			i.next();
			HTTPServerThread *pThread=i.value();
			CloseSocketThread(pThread);
		}
    }
	else
	{	
		HTTPServerThread *pThread=m_ClientThreadList.value(nThreadID,NULL);
		if (pThread)
		{
			CloseSocketThread(pThread);
		}
	}

	return true;
}


/*!
	This is an overloaded member function, provided for convenience. 
	It behaves essentially like the above function but takes list of socket ID's.
	\param lstSocketID - list of socket ID's socket for disconnect
*/
void HTTPServer::CloseConnection(QList<int> lstThreadID)
{
	Status lastErr;
	QWriteLocker locker(&m_RWLock);

	int nSize=lstThreadID.size();
	for (int i=0;i<nSize;++i)
	{
		HTTPServerThread *pThread=m_ClientThreadList.value(lstThreadID.at(i),NULL);
		CloseSocketThread(pThread);
	}
}

//FORCE socket close: deletion is unavoidable
void HTTPServer::CloseSocketThread(HTTPServerThread *pThread)
{
	if(pThread)
		pThread->ExecuteDestroyThread();
}



/*------------------------------------------------------------------------*/
/*								  IP BLACK LIST							  */
/*------------------------------------------------------------------------*/


/*!
	Get newly added IP addresses on kick list

	\param	- kick list
	\return	- false if list is not changed, lstAcess is empty
*/
bool HTTPServer::GetDynamicallyBuiltIPAccessList(DbRecordSet &lstAccess)
{
	if (NULL == m_pIPKickBanManager)
		return false;
	if(!m_pIPKickBanManager->IsListChanged()) return false;
	m_pIPKickBanManager->GetDynamicList(lstAccess);
	return true;
}



/*------------------------------------------------------------------------*/
/*							  CHECK CREDENTIALS							  */
/*------------------------------------------------------------------------*/

/*!
	Checks for path of SSL certificates, if not found, do not start SSL server
	\param pStatus - error
*/
void HTTPServer::CheckSSLCertificate(Status& pStatus)
{

	//if no SSL, then OK
	pStatus.setError(0);

	//CERTIFICATE
	//check if setted
	if(m_ConnSettings.m_strCertificateFile.isEmpty()||m_ConnSettings.m_strCertificateFile.isNull())
	{
		pStatus.setError(StatusCodeSet::ERR_NETWORK_GENERAL_MSG,tr("SSL certificates not found, server could not be started!"));
		return;
	}
	
	//check if file exists:
	QFileInfo testCert(m_ConnSettings.m_strCertificateFile);
	if(!testCert.exists())
	{
		pStatus.setError(StatusCodeSet::ERR_NETWORK_GENERAL_MSG,tr("SSL certificates not found, server could not be started!"));
		return;
	}


	//PRIVATE KEY

	//check if setted
	if(m_ConnSettings.m_strPrivateKeyFile.isEmpty()||m_ConnSettings.m_strPrivateKeyFile.isNull())
	{
		pStatus.setError(StatusCodeSet::ERR_NETWORK_GENERAL_MSG,tr("SSL certificates not found, server could not be started!"));
		return;
	}

	
	//check if file exists:
	QFileInfo testCert1(m_ConnSettings.m_strPrivateKeyFile);
	if(!testCert1.exists())
	{
		pStatus.setError(StatusCodeSet::ERR_NETWORK_GENERAL_MSG,tr("SSL certificates not found, server could not be started!"));
		return;
	}



}


void HTTPServer::InitSSL(Status& pStatus)
{

	//-----------------------------------
	//	LOAD CA root cert....if exists..
	//-----------------------------------
	// Load up root certificate for our web certificate.... it's not
	QFile rootCAFile(m_ConnSettings.m_strCACertificateFileBundle);
	QSslCertificate *rootCertificate=NULL;
	if (rootCAFile.open( QIODevice::ReadOnly ))
		rootCertificate = new QSslCertificate( &rootCAFile, QSsl::Pem	);
	rootCAFile.close();

	QSslConfiguration *sslConfig = new QSslConfiguration(QSslConfiguration::defaultConfiguration());

	QList<QSslCertificate> *caCerts = new QList<QSslCertificate>(
		sslConfig->caCertificates() );

	bool bCertOK=false;
	if(rootCertificate)
	{
		if (!rootCertificate->isNull())
		{
			caCerts->append( *rootCertificate );
			sslConfig->setCaCertificates( *caCerts );

			qDebug() << "CA Authority Certificate - Organization:" << rootCertificate->issuerInfo(QSslCertificate::Organization)
				<< "Common Name:"  <<  rootCertificate->issuerInfo(QSslCertificate::CommonName)
				<< "Country: " << rootCertificate->issuerInfo(QSslCertificate::CountryName);

			bCertOK=true;
		}
	}

	if (!bCertOK)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,QString("SSL Error: unable to load issuer root certificate, path was: %1").arg(m_ConnSettings.m_strCACertificateFileBundle));
	}


	//-----------------------------------
	//	SET local certificate 
	//-----------------------------------
	QFile localCertFile(m_ConnSettings.m_strCertificateFile);
	QSslCertificate *localCertificate;
	if (localCertFile.open( QIODevice::ReadOnly ))
		localCertificate = new QSslCertificate( &localCertFile, QSsl::Pem	);
	localCertFile.close();

	bCertOK=false;
	if(localCertificate)
	{
		if (!localCertificate->isNull())
		{
			qDebug() << "Local Certificate - Organization:" << localCertificate->subjectInfo(QSslCertificate::Organization)
				<< "Common Name:"  <<  localCertificate->subjectInfo(QSslCertificate::CommonName)
				<< "Country: " << localCertificate->subjectInfo(QSslCertificate::CountryName)
				<< "Expiry Date: " << localCertificate->expiryDate();
			bCertOK=true;
		}
	}
	if (!bCertOK)
	{
		pStatus.setError(1,"Failed to load local certificate! Check certificate format and path!");
		return;
	}
	

	sslConfig->setLocalCertificate(*localCertificate);
	sslConfig->setPeerVerifyMode(QSslSocket::VerifyNone); 
	sslConfig->setProtocol(QSsl::AnyProtocol); 

	//-----------------------------------
	//	SET private key
	//-----------------------------------
	QSslKey *privKey=NULL;
	QFile privFile(m_ConnSettings.m_strPrivateKeyFile);
	if (privFile.open( QIODevice::ReadOnly ))
		privKey = new QSslKey(&privFile,QSsl::Rsa,QSsl::Pem, QSsl::PrivateKey, SSL_PRIVATE_KEY);
	privFile.close();

	bCertOK=false;
	if (privKey)
	{
		if (!privKey->isNull())
		{
			sslConfig->setPrivateKey(*privKey);
			bCertOK=true;
		}
	}
	if (!bCertOK)
	{
		pStatus.setError(1,"Failed to load private key! Check password phrase, key format and path!");
		return;
	}


	//this enables same config for all new incoming connections:
	QSslConfiguration::setDefaultConfiguration( *sslConfig );
}

void HTTPServer::OnAcceptError( QAbstractSocket::SocketError err)
{
	qDebug()<<"Server reported error: "<<err;

}




