<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de">
<context>
    <name>FTPClient</name>
    <message>
        <location filename="ftpclient.cpp" line="267"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="ftpclient.cpp" line="251"/>
        <source>does not exist!</source>
        <translation>exitiert nicht!</translation>
    </message>
    <message>
        <location filename="ftpclient.cpp" line="258"/>
        <source>Error while reading file: </source>
        <translation>Fehler beim Lesen der Datei:</translation>
    </message>
    <message>
        <location filename="ftpclient.cpp" line="267"/>
        <source>is empty!</source>
        <translation>ist leer!</translation>
    </message>
    <message>
        <location filename="ftpclient.cpp" line="338"/>
        <source>Error while opening file: </source>
        <translation>Fehler beim Öffnen der Datei:</translation>
    </message>
    <message>
        <location filename="ftpclient.cpp" line="345"/>
        <source>Error while writing file: </source>
        <translation>Fehler beim Schreiben der Datei:</translation>
    </message>
</context>
<context>
    <name>HTTPClientSocketHandler</name>
    <message>
        <location filename="httpclientsockethandler.cpp" line="696"/>
        <source>Unknown reason</source>
        <translation>Unbekannter Grund</translation>
    </message>
</context>
<context>
    <name>HTTPServer</name>
    <message>
        <location filename="httpserver.cpp" line="479"/>
        <source>SSL certificates not found, server could not be started!</source>
        <translation>SSL-Zertifikate nicht gefunden, Server kann nciht gestartet werden!</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="htmlhttpserver.cpp" line="227"/>
        <source>WWW Document Directory:</source>
        <translation>WWW Dokumenten-Verzeichnis:</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="56"/>
        <source> does not exists!</source>
        <translation>existiert nicht!</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="71"/>
        <source>File: </source>
        <translation>Datei:</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="71"/>
        <source> can not be read!</source>
        <translation>kann nicht gelesen werden!</translation>
    </message>
    <message>
        <location filename="resthttpserver.cpp" line="231"/>
        <source>Schema Directory:</source>
        <translation>Schema-Verzeichnis:</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="56"/>
        <source>Resource Directory:</source>
        <translation>Ressourcen-Verzeichnis:</translation>
    </message>
    <message>
        <location filename="htmlhttpserver.cpp" line="227"/>
        <source> does not exist!</source>
        <translation> existiert nicht!</translation>
    </message>
</context>
<context>
    <name>QtSslSocket</name>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>QtSslSocket: Certificate required in server mode</source>
        <translation type="obsolete">QtSslSocket: Certificate required in server mode</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Private key required in server mode</source>
        <translation type="obsolete">Private key required in server mode</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Error creating SSL context, %1</source>
        <translation type="obsolete">Error creating SSL context, %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Invalid or empty cipher list, %1</source>
        <translation type="obsolete">Invalid or empty cipher list, %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Error loading certificate, %1</source>
        <translation type="obsolete">Error loading certificate, %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Error loading private key, %1</source>
        <translation type="obsolete">Error loading private key, %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Private key do not certificate public key, %1</source>
        <translation type="obsolete">Private key do not certificate public key, %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Failed to load the list of trusted CAs: %1</source>
        <translation type="obsolete">Failed to load the list of trusted CAs: %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Error creating SSL session, %1</source>
        <translation type="obsolete">Error creating SSL session, %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Error creating SSL session</source>
        <translation type="obsolete">Error creating SSL session</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Failed to write protocol data to BIO</source>
        <translation type="obsolete">Failed to write protocol data to BIO</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Failed to read from SSL, %1</source>
        <translation type="obsolete">Failed to read from SSL, %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>SSL error: %1</source>
        <translation type="obsolete">SSL error: %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>An unknown error occurred</source>
        <translation type="obsolete">An unknown error occurred</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Self signed certificate</source>
        <translation type="obsolete">Self signed certificate</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Self signed certificate in certificate chain</source>
        <translation type="obsolete">Self signed certificate in certificate chain</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Unable to decrypt certificate&apos;s signature</source>
        <translation type="obsolete">Unable to decrypt certificate&apos;s signature</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Certificate signature failure</source>
        <translation type="obsolete">Certificate signature failure</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>The certificate is not yet valid</source>
        <translation type="obsolete">The certificate is not yet valid</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>The certificate has expired</source>
        <translation type="obsolete">The certificate has expired</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Format error in certificate&apos;s notBefore field</source>
        <translation type="obsolete">Format error in certificate&apos;s notBefore field</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Format error in certificate&apos;s notAfter field</source>
        <translation type="obsolete">Format error in certificate&apos;s notAfter field</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Unable to get local issuer certificate</source>
        <translation type="obsolete">Unable to get local issuer certificate</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Unable to verify the first certificate</source>
        <translation type="obsolete">Unable to verify the first certificate</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Invalid CA certificate</source>
        <translation type="obsolete">Invalid CA certificate</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Unable to get issuer certificate</source>
        <translation type="obsolete">Unable to get issuer certificate</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Unable to decode issuer public key</source>
        <translation type="obsolete">Unable to decode issuer public key</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Path length constraint exceeded</source>
        <translation type="obsolete">Path length constraint exceeded</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Unsupported certificate purpose</source>
        <translation type="obsolete">Unsupported certificate purpose</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Certificate not trusted</source>
        <translation type="obsolete">Certificate not trusted</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Certificate rejected</source>
        <translation type="obsolete">Certificate rejected</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Subject issuer mismatch</source>
        <translation type="obsolete">Subject issuer mismatch</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Authority and subject key identifier mismatch</source>
        <translation type="obsolete">Authority and subject key identifier mismatch</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Authority and issuer serial number mismatch</source>
        <translation type="obsolete">Authority and issuer serial number mismatch</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Key usage does not include certificate signing</source>
        <translation type="obsolete">Key usage does not include certificate signing</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Out of memory</source>
        <translation type="obsolete">Zu wenig Speicher</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>Unknown error</source>
        <translation type="obsolete">Unbekannter Fehler</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>The SSL certificate has been verified</source>
        <translation type="obsolete">The SSL certificate has been verified</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>The SSL certificate is not valid until %1</source>
        <translation type="obsolete">The SSL certificate is not valid until %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>The SSL certificate expired on %1</source>
        <translation type="obsolete">The SSL certificate expired on %1</translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>, %1 </source>
        <translation type="obsolete">, %1 </translation>
    </message>
    <message>
        <location filename="rpcskeleton.cpp" line="55"/>
        <source>the host name &quot;%0&quot; doesn&apos;t match the host name in the certificate &quot;%1&quot;</source>
        <translation type="obsolete">the host name &quot;%0&quot; doesn&apos;t match the host name in the certificate &quot;%1&quot;</translation>
    </message>
</context>
</TS>
