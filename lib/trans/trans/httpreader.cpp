#include "httpreader.h"


/*!
	Constructor: inits QHttp object
	\param sync			- thread synchornizer object
	\param nTimeOut		- (msec) security feature: if QHTTP fails, this timer will save us from endless waiting

*/
HttpReader::HttpReader(ThreadSynchronizer *sync,int nTimeOut)
{
	m_nTimeOut=nTimeOut;
	m_ThreadSync=sync;
	m_bAuthRequired=false;
	ClearState();
	connect(&m_HttpReader,SIGNAL(requestFinished(int,bool)),this,SLOT(ReadingDone(int,bool)));
	connect(&m_HttpReader,SIGNAL(authenticationRequired ( const QString & , quint16 , QAuthenticator *  )),this,SLOT(OnAuthenticationRequired ( const QString & , quint16 , QAuthenticator *  )));
	connect(&m_HttpReader,SIGNAL(responseHeaderReceived ( const QHttpResponseHeader &  )),this,SLOT(OnResponseHeaderReceived ( const QHttpResponseHeader &  )));
	connect(&m_HttpReader,SIGNAL(readyRead ( const QHttpResponseHeader &  )),this,SLOT(OnReadyRead ( const QHttpResponseHeader &  )));
	connect(&m_HttpReader,SIGNAL(sslErrors ( const QList<QSslError> &  ) ),this,SLOT(OnSslErrors ( const QList<QSslError> & )));
}

HttpReader::~HttpReader()
{

}


/*!
	Invoked by external source, sends request
*/
void HttpReader::ReadURL(QString strUrl, bool bSSL,int nPort)
{
	m_bAuthRequired=false;
	m_strAuthRequiredHost="";
	if(m_HttpReader.hasPendingRequests()) //if already waiting for response, refuse to enter
	{
		ClearState();
		m_bErrorOccured=true;
		m_ThreadSync->ThreadRelease();
	}
	QUrl url(strUrl);
	m_HttpReader.setHost(url.host(),bSSL?QHttp::ConnectionModeHttps:QHttp::ConnectionModeHttp,nPort);
	m_ReqID=m_HttpReader.get(strUrl);
	m_nTimerID=startTimer(m_nTimeOut);
}

void HttpReader::OnSslErrors ( const QList<QSslError> & )
{
	m_HttpReader.ignoreSslErrors();
}

void HttpReader::OnReadyRead ( const QHttpResponseHeader & resp )
{
	m_RespBuffer+=m_HttpReader.readAll();
	emit ReadInProgress(m_RespBuffer.size(), m_nTotalSize);
}
void HttpReader::OnResponseHeaderReceived ( const QHttpResponseHeader &header  )
{
	//read total count:
	m_nTotalSize=header.value("Content-Length").toInt();
}

/*!
	Returns content if exists and status if error

	\param pContent	- content of page red by ReadURL()
	\return			- true if sucess
*/
bool HttpReader::GetResponse(QByteArray *pContent)
{
	*pContent=m_RespBuffer; 
	return !m_bErrorOccured;
}

/*!
	Invoked when page is readed or error occured
*/
void HttpReader::ReadingDone(int id,bool error)
{
	if(m_ReqID!=id) return;
	if(error)
	{
		//int err =m_HttpReader.error();
		ClearState();
		m_bErrorOccured=true;
		m_ThreadSync->ThreadRelease();
	}
	else
	{
		//done reading:
		m_bErrorOccured=false;
		m_ThreadSync->ThreadRelease();
	}
}


/*!
	Destroys this object
*/
void HttpReader::Destroy()
{
	deleteLater();
	m_ThreadSync->ThreadRelease();
}


/*!
	Security: timeout

	\param event	- timer event
*/
void HttpReader::timerEvent(QTimerEvent *event)
{
	//out timer is here, then fire Time out error, save our asses!!!
	if(event->timerId()==m_nTimerID)
	{
		if(m_HttpReader.state()==QHttp::Unconnected || m_HttpReader.state()==QHttp::HostLookup)
		{
			ClearState();
			m_bErrorOccured=true;
			m_ThreadSync->ThreadRelease();
		}
		else
			killTimer(m_nTimerID);
	}
}


/*!
	Clear state
*/
void HttpReader::ClearState()
{
	if(m_nTimerID!=-1)killTimer(m_nTimerID);
	m_HttpReader.clearPendingRequests();
	m_ReqID=-1;
	m_nTimerID=-1;
	m_bErrorOccured=false;
	m_RespBuffer.clear();
}



void HttpReader::OnAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth)
{
	m_bAuthRequired=true;
	m_strAuthRequiredHost=hostname;
	ClearState();
	m_bErrorOccured=false;
	m_ThreadSync->ThreadRelease();

}

void HttpReader::SetAuthentication(QAuthenticator *auth)
{
	m_HttpReader.setUser(auth->user(),auth->password());
}

void HttpReader::PostData( QString strURL, QByteArray postData, bool bSSL/*=false*/,int nPort/*=0*/ )
{
	m_bAuthRequired=false;
	m_strAuthRequiredHost="";
	if(m_HttpReader.hasPendingRequests()) //if already waiting for response, refuse to enter
	{
		ClearState();
		m_bErrorOccured=true;
		m_ThreadSync->ThreadRelease();
	}

	QUrl url(strURL);
	m_HttpReader.setHost(url.host(),bSSL?QHttp::ConnectionModeHttps:QHttp::ConnectionModeHttp,nPort);
	m_ReqID=m_HttpReader.post(strURL,postData);
	m_nTimerID=startTimer(m_nTimeOut);
}