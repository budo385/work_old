#ifndef HTTPCLIENTTHREAD_H
#define HTTPCLIENTTHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QtNetwork>


#include "common/common/status.h"
#include "httprequest.h"
#include "httpresponse.h"
#include "httphandler.h"
#include "httpclientconnectionsettings.h"
#include "httputil.h"
#include "httpcontext.h"
#include "httpserverping.h"
#include "httpstreamhandler.h"
#include "common/common/threadsync.h"


typedef QHash<int, HTTPRequest> ServerMsgBuffer;




/*!
    \class HTTPClientThread
    \brief HTTP client thread 
    \ingroup HTTPClient

	Can send messages to the server and can receive async. server messages.
	When created it runs separate (socket) thread in which HTTPClientSocketHandler object is handling events 
	from TCPSocket. Socket can be ordinary or SSL. Sending requests to the server can be synchronized 
	(calling thread is stopped until response or error) or async (calling thread only sends message without waiting for
	errors or response)

	Acess to all functions is serialized, so more then one thread can be access one global isntance of this object

	Use:
	- Connect() - establish connection to the server
	- Disconnect() - diconnects from server
	- SendData(HTTPRequest,HTTPResponse) 
		SYNC: sends HTTPRequest message to server. If compression is enabled message will be compressed 
		(DO NOT COMPRESS before calling). Server response is in HTTPResponse message.
		ASYNC: if HTTPResponse==NULL then HTTPRequest is sent to server and function returns immidatiely. 
	- RegisterServerMsgHandler(HTTPHandler *pHandler)
		Registers handler for incoming server messages. Server messages are stored in queue. 
		When new message arrives HTTPClientThread will invoke pHandler in list of handlers based on domain
		for which handler is registered. Server must set domain in HTTPRequest message that sends.
		After processing message is deleted from queue

		
	Important note:
	QWaitCondition can not be used because communication between threads is depending on emit SIGNAL method
	If two separate threads, this emit should by async, but sometimes it goes directly to slave thread SLOT and result is
	that calling thread is locked forever.
	Sending packets is designed to be executed in two steps (see SendData()):
	- write to socket (send signal to slave thread ro read outgoing buffers)
	- wait for server response (wait for buffer semaphore to be released)
	Sometimes, when calling slave thread, both steps are executed at once, this is handled fine, but just to mention that.
	Result of above behaviour is that we have developed custom Thread Sync mechamism:
	ThreadWait(), ThreadRelease() which handles this SYNC issues between threads much better.
	One last catch:
	In ClearSocketState() method of HTTPClientSocketHandler it is very IMPORTANT to put line 
	m_BufferSemaphore->release(1) at last place in method, reason is that this command acutally transfers 
	execution flow to calling thread which is in wait state (in SendData with command m_BufferSemaphore->acquire(1);)
*/

class HTTPClientThread : public QThread
{ 
	Q_OBJECT

	friend class HTTPClientSocketHandler;
public:

	HTTPClientThread();
	~HTTPClientThread();

    void run();

	//--------------------------------------
	//Main interface
	//--------------------------------------

	void Connect(Status& pStatus,HTTPClientConnectionSettings& pConnection);
	void Disconnect(Status& pStatus);
	void SendData(Status& pStatus, HTTPRequest* pHTTPRequest, HTTPResponse* pHTTPResponse=NULL,HTTPStreamHandler *pStreamHandler=NULL);
	void RegisterServerMsgHandler(HTTPHandler *pHandler){m_pLstHandlers.append(pHandler); };
	bool IsConnected();
	void CancelOperation();
	bool IsCanceled();
	void SetServerPingHandler(HTTPServerPing &ping){m_Ping=ping;};

signals: 
	void SocketConnect();
	void SocketDisconnect();
	void SocketDestroy();
	void SocketClientRequest();
	void SocketOperationInProgress(int,qint64,qint64);
	void SocketReportedError(int,QString);
	void SocketAbort();

public slots:
	void ServerMessageReceived(int);
	void SocketErrorDetected(int,QString);

protected:

	void							SetConnectionSettings(const HTTPClientConnectionSettings &pConnection);
	HTTPClientConnectionSettings	GetConnectionSettings(){return m_ConnSettings;};
	void				SetError(int nErrorCode, QString rawText="");
	Status&				GetError(){return m_LastError;};
	void				WriteResponseBufferChunked(Status &pStatus, const QHttpResponseHeader &header, const QByteArray &pBuffer, bool bLastPart=true);
	void				WriteResponseBuffer(Status &pStatus, const QHttpResponseHeader &header, const QByteArray &pBuffer);
	void				GetRequestBuffer(Status &err,QByteArray& pBuffer);
	void				InvalidateBuffers();				
	bool				IsChunkedTransfer();
	void				InitChunkedHttpStream(Status &status,const QHttpResponseHeader &header);
	int					WriteServerMsgQueue(const QHttpRequestHeader &header,const QByteArray& pbody);
	void				DeleteServerMsgQueue(int);
	void				GetServerMsg(int,HTTPRequest&);
	HTTPServerPing *	GetServerPing(){return &m_Ping;};
	void				SetConnected(bool bConnected);
	bool				SetDataTransferParametersAndCheckIsCancel(qint64 done,qint64 total, bool bRead);
	void				GetDataTransferParameters(qint64 &done,qint64 &total);

	qint64 m_Done,m_Total;

	

	/* ------------------private members------------------ */

	HTTPStreamHandler*					m_StreamHandler;
	//mutex locks	
	QMutex m_Mutex;					///< mutex
	QReadWriteLock m_RWLock;		///< RWlock for accessing shared data
	ThreadSynchronizer m_ThreadSync;///< for syncing between mastr & slave thread
	QWaitCondition m_waitCond;		///< special one for refreshing GUI
	HTTPServerPing m_Ping;			///< ping server

	bool m_bSocketIsInErrorState;		///<if true, no acces is allowed (only socket disconnect)

	//shared data members
	QSemaphore* m_BufferSemaphore;	///< resource to protect: response buffer when receiving message


	//Shared data betwenn main and socket thread 
	HTTPRequest *m_RequestBuffer;		///<use by socket for reading request when called in sync manner, warning: serialize access
	HTTPResponse *m_ResponseBuffer;		///<use by socket for writing response when called in sync manner, warning: serialize access
	ServerMsgBuffer m_ServerMessageBuffer; ///<used by socket for writing server messages (differs by ID), warning: serialize access
	HTTPClientConnectionSettings m_ConnSettings; ///<holds connection settings for socket
	Status m_LastError;			///< warning: not thread safe: serialize access
	bool m_bConnected;	
	bool m_bCancel;	
	bool m_bIsChunkedTransfer;

	//List of handlers of Server messages (probably only one)
	QList<HTTPHandler *>  m_pLstHandlers;

};


/*!
    \class HTTPClient
    \brief Main HTTP client module for accessing HTTP server. Multithread safe!
    \ingroup HTTPClient

	This is only wrapper for HTTPClientThread class. Public functions are:
	Connect, Disconnect, SendData, RegisterServerMsgHandler, SetConnectionSettings,
	GetConnectionSettings, IsConnected
	For details: \sa HTTPClientThread

*/

class HTTPClient : private HTTPClientThread{
public:
	HTTPClientThread::Connect;
	HTTPClientThread::Disconnect;
	HTTPClientThread::IsConnected;
	HTTPClientThread::SendData;
	HTTPClientThread::RegisterServerMsgHandler;
	HTTPClientThread::SetConnectionSettings;
	HTTPClientThread::GetConnectionSettings;
	HTTPClientThread::CancelOperation;
};



#endif //HTTPCLIENTTHREAD_H
