#include "httpserversockethandler.h"
#include "common/common/loggerabstract.h"
#include "config_trans.h"
#include <QSslSocket>
#include "httprequest.h"
#include "httpresponse.h"
#include "httputil.h"
#include "common/common/threadid.h"


/*!
	Constructor of server socket: creates socket for incoming connection
	\param pClientThread	- pointer to main thread / used to access shared data (buffers,etc..)
	\param ThreadSync		- shared ThreadSync for syncing threads (only used for sending msg to the client)
	\param parent			- QObject

*/
HTTPServerSocketHandler::HTTPServerSocketHandler(int nSocketDescriptor,bool bUseSSL,QObject *parent)
: QObject(parent),m_StreamHandler(NULL),m_bCloseSocketAfterResponse(false),m_nOrphanSocketTimeOut(0)
{

	m_nTimerID=-1;					
	m_SocketSemaphore= new QSemaphore(1); //for shared resource: socket!
	m_TCPSocket=NULL;
	m_ByteWritten=0;
	m_ByteToWrite=0;
	m_ByteWrittenToSocket=0;
	m_bAbortConnectionInProgress=false;
	m_datCreated=QDateTime::currentDateTime();
	//bool bUseSSL=m_ClientThread->GetServerSettings().m_bUseSSL;

#ifndef _DONT_USE_SSL
	QSslSocket *pSSLSocket;			///< holds pointer to SSL or  TCP socket
#endif

	//create socket:
#ifndef _DONT_USE_SSL
	if (bUseSSL)
	{
		m_TCPSocket=new QSslSocket(NULL);
		pSSLSocket=dynamic_cast<QSslSocket*>(m_TCPSocket); //cast it to SSL for SIGNAL connection
		connect(pSSLSocket, SIGNAL(sslErrors ( const QList<QSslError>&)), this, SLOT(OnSSLErrors(const QList<QSslError>&)));
		connect(pSSLSocket, SIGNAL(encryptedBytesWritten(qint64)), this, SLOT(OnBytesWritten(qint64)));
		pSSLSocket->setSocketDescriptor(nSocketDescriptor);
	}
	else
	{
#endif
		m_TCPSocket=new QTcpSocket (this);
		m_TCPSocket->setSocketDescriptor(nSocketDescriptor);
		connect(m_TCPSocket, SIGNAL(bytesWritten (qint64)), this, SLOT(OnBytesWritten(qint64)));
#ifndef _DONT_USE_SSL
	}
#endif

	//connect signals:
    // Connect the socket's signals to slots in our client implementation.
	//connect(m_TCPSocket, SIGNAL(connected()), this, SLOT(OnConnectionEstablished()));
	connect(m_TCPSocket, SIGNAL(readyRead()), this, SLOT(OnReadData()));
    connect(m_TCPSocket, SIGNAL(disconnected()), this, SLOT(OnConnectionClosed()));
    connect(m_TCPSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(ErrorHandler(QAbstractSocket::SocketError)));
	
#ifndef _DONT_USE_SSL
	if(bUseSSL && pSSLSocket) pSSLSocket->startServerEncryption(); //for SSL 
#endif

	m_ConnContext.m_nSocketID=nSocketDescriptor;
	m_ConnContext.m_strPeerIP=m_TCPSocket->peerAddress().toString();
	m_ConnContext.m_strPeerName=m_TCPSocket->peerName();		
	m_ConnContext.m_nPeerPort=m_TCPSocket->peerPort();

	m_nState=STATE_ACCEPTING_CONNECTION;
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTPServerSocketHandler: socket %1 assign to thread: %2").arg(nSocketDescriptor).arg(ThreadIdentificator::GetCurrentThreadID()));
	m_nTimerID=startTimer(HTTP_SERVER_ACCEPT_CONNECTION_TIMEOUT); //security option: client must send valid request in this period

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Socket: Incoming connection detected, thread %1, socket: %2").arg(ThreadIdentificator::GetCurrentThreadID()).arg(nSocketDescriptor));

}

/*!
	Destructor
*/
HTTPServerSocketHandler::~HTTPServerSocketHandler()
{
	if (m_StreamHandler){delete m_StreamHandler; m_StreamHandler=NULL;}

	//if somehow socket stay alive:
	if (m_TCPSocket!=NULL)
	{
		m_TCPSocket->blockSignals(true);
		m_TCPSocket->abort();
		m_TCPSocket->deleteLater(); 
		m_TCPSocket=NULL;
	}
	delete m_SocketSemaphore;
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_HTTP_SOCKET_CLOSED,QVariant(m_ConnContext.m_nSocketID).toString()+";"+m_ConnContext.m_strPeerIP);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Socket: closed, thread %1, socket: %2").arg(ThreadIdentificator::GetCurrentThreadID()).arg(m_ConnContext.m_nSocketID));
	//qDebug()<<"socket destroyed" <<m_ConnContext.m_nSocketID;
	emit DestroyThread();
}





/*------------------------------------------------------------------------*/
/*					 CONNECTING/DISCONNECTING							  */
/*------------------------------------------------------------------------*/


/*!
	Revoked by socket when socket is disconnected: if timeout error. error will be reported to ErrorHandler
*/
void HTTPServerSocketHandler::OnConnectionClosed()
{
		//kill streamer:
		if (m_StreamHandler){delete m_StreamHandler; m_StreamHandler=NULL;}

		//release socket:
		if (m_TCPSocket==NULL)
			return; //means that socket not exists any more: sometimes this is called twice??
		
		//destroy any signals:
		disconnect(m_TCPSocket, SIGNAL(readyRead()), this, SLOT(OnReadData()));
		disconnect(m_TCPSocket, SIGNAL(disconnected()), this, SLOT(OnConnectionClosed()));
		disconnect(m_TCPSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(ErrorHandler(QAbstractSocket::SocketError)));

		//schedule for deletion:
		m_TCPSocket->deleteLater();m_TCPSocket=NULL;//delete m_TCPSocket;

		//client is connecting/disconnecting before even making request...
		if (!m_bAbortConnectionInProgress)
			if(m_nState==STATE_ACCEPTING_CONNECTION || m_nState==STATE_NOT_CONNECTED)
			{
#if _DEBUG //17.10.2010: MB is annoyed with this message, leave only for test purpose
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"client not successfully connected.");
#endif
				KickBanIP_OnNTries(m_ConnContext.m_strPeerIP);
			}

		m_nState=STATE_NOT_CONNECTED; 
		//m_ClientThread->SetSocketState(STATE_NOT_CONNECTED);
		m_reqBuffer.clear();
		m_outgoingBuffer.clear();
		m_ByteWritten=0;
		m_ByteToWrite=0;
		m_ByteWrittenToSocket=0;
		if (m_SocketSemaphore->available()==0)m_SocketSemaphore->release(1);
		//emit DestroyThread();	//this will destroy thread back: in case when client is not disconected in proper manner: error
		SocketDestroy();
}

/*!
	Revoked by Main thread, initate destroy sequence, disconnect if connected, close socket, kill socket thread
*/
void HTTPServerSocketHandler::SocketDestroy()
{
	//qDebug()<<"Socket destroy signal: "<<m_ClientThread->GetSocketDescriptor();
	deleteLater();		//schedule this object for safe deletion
	if(m_nState!=STATE_NOT_CONNECTED && m_TCPSocket!=NULL) 
		m_TCPSocket->abort(); 
	if (m_SocketSemaphore->available()==0)m_SocketSemaphore->release(1); //just measure to clear all locked resources
	
	//QTimer::singleShot(0,this,SIGNAL(DestroyThread())); //emit destroy signal after all events...
	
}



/*!
	SECURITY feature: 
	- timeout from client connection to the first valid request
	- at this layer we dont know what is first request, in 99% it will be login (short)
	- so any sistem that has login, can set this timeout to relative short time (max 20sec)

*/
void HTTPServerSocketHandler::timerEvent(QTimerEvent *event)
{
	//out timer is here, then fire Time out error, save our asses!!!
	if(event->timerId()==m_nTimerID)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"client did not made valid HTTP request in given time period.");
		AbortConnection();
	}
}


/*------------------------------------------------------------------------*/
/*								  WRITING									*/
/*------------------------------------------------------------------------*/


/*!
	Revoked by Main thread: inform socket thread that new client request is ready for sending
	data in shared Main Thread (GetRequestBuffer())
	-> for sending server->client msg
*/

void HTTPServerSocketHandler::SocketWrite(QByteArray byteMsgSrv)
{
	//waits for socket to be released (reading in progress)
	if (!m_SocketSemaphore->tryAcquire(1,30000)) //if read>30sec, return error
	{
		m_LastError.setError(StatusCodeSet::ERR_HTTP_WRITE_FAILED_SOCKET_IN_USE);
		//m_ThreadSync->ThreadRelease(); //notify calling thread: its safe now to proceed coz buffer is processed
		return;
	}
	//m_SocketSemaphore->acquire(1);

	m_nState=STATE_WRITE;
	//m_ClientThread->SetSocketState(STATE_WRITE);

	//get user data, write to socket:
	//QByteArray clientRequest;
	//m_ClientThread->GetRequestBuffer(m_outgoingBuffer);
	m_outgoingBuffer=byteMsgSrv;
	if(m_outgoingBuffer.isNull()||m_outgoingBuffer.isEmpty())
	{	//empty request buffer!!!
		m_LastError.setError(StatusCodeSet::ERR_HTTP_INVALID_HTTP_MESSAGE);
	}
	else
	{
		m_ByteToWrite=m_outgoingBuffer.size();
		m_ByteWritten=0;
		m_ByteWrittenToSocket=0;
		WriteSocketPacket();
		//m_TCPSocket->write(m_outgoingBuffer);
	}
	
	//release socket for normal operations:
	//m_nState=STATE_READY;
	//m_ClientThread->SetSocketState(STATE_READY);
	//m_SocketSemaphore->release(1);
	//m_ThreadSync->ThreadRelease(); //notify calling thread: its safe now to proceed coz buffer is processed
}




/*------------------------------------------------------------------------*/
/*								  ERROR HANDLER							  */
/*------------------------------------------------------------------------*/

/*!
	Revoked by SOCKET, accepts any socket error. On any error closes connection and destroy itself
*/
void HTTPServerSocketHandler::ErrorHandler(QAbstractSocket::SocketError errSocket)
{
	//qDebug()<<"socket error:"<<errSocket << " desc:"<<m_TCPSocket->errorString();
	Status lastErr;
	HTTPUtil::MapSocketErrors(lastErr,errSocket,m_TCPSocket->errorString());
	m_LastError.setError(lastErr.getErrorCode(),lastErr.getErrorTextRaw());

	//emit DestroyThread();
	SocketDestroy();
	//QTimer::singleShot(0,this,SIGNAL(DestroyThread())); //emit destroy signal after all events...
}



/*------------------------------------------------------------------------*/
/*								  READING								  */
/*------------------------------------------------------------------------*/


/*!
	Revoked by SOCKET, accepts any data from socket and reads it
*/
void HTTPServerSocketHandler::OnReadData()
{

	//qDebug()<<ThreadIdentificator::GetCurrentThreadID()<<" this thread is trying to get something";

	if (m_nState!=STATE_READ)
	{  
		if (m_StreamHandler){delete m_StreamHandler; m_StreamHandler=NULL;} //precaution: should be deleted from previous stream...

		//first time we enter loop: m_Read_header should come first: if m_Read_header> longer then ->kick
		m_Read_header=QHttpRequestHeader();
		m_Read_nChunkReadIteration=0;
		m_Read_nTotalLength=0;
		m_Read_pRequestHandler=NULL;
		m_Read_bBlockReadStillProcessingLastChunk=false;
		if (!m_SocketSemaphore->tryAcquire(1,10000)) //if write>30sec, return error, should never happen
		{
			m_LastError.setError(StatusCodeSet::ERR_HTTP_READ_FAILED_SOCKET_IN_USE);
			AbortConnection(false); //happend..client and server blocked...resolve..by diconect
			return;
		}
		m_nState=STATE_READ;
		m_reqBuffer.clear();
	}

	if (m_Read_bBlockReadStillProcessingLastChunk) return;

	m_reqBuffer+=m_TCPSocket->readAll();
	m_Read_nChunkReadIteration++;

	//qDebug()<<ThreadIdentificator::GetCurrentThreadID()<<" this thread is red out something";
	/*
	QFile file("D:/stream_request.txt");
	if(file.open(QIODevice::Append))
	{
		QByteArray buf=m_reqBuffer;
		buf.append("\n\r----------------------------");
		file.write(buf);
		file.close();
	}
	*/

	if (m_Read_nChunkReadIteration==1)
	{
		int nHTTPType;// test HTTP m_Read_header
		int nHdrEnd=0;
		bool bCompleteHeader=HTTPUtil::CheckHTTPHeader(m_reqBuffer,nHTTPType,m_Read_nTotalLength,nHdrEnd); //try to extract m_Read_header

		if(!bCompleteHeader)
		{
			if (m_reqBuffer.size()>HTTP_HEADER_MAX_SIZE) //security option to prevent client to send garbage
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"client initial HTTP request exceeds maximum allowed size.");
				AbortConnection();
				return;
			}
			m_Read_nChunkReadIteration=0; //RESET COUNTER IN CASE OF INVALID HEADER
			return; 
		}

		if(nHTTPType!=HTTPUtil::HTTP_HEADER_REQUEST)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"client not made valid HTTP request.");
			AbortConnection();
			return;
		}
			
		m_Read_header = QHttpRequestHeader (m_reqBuffer.left(nHdrEnd));
		m_reqBuffer=m_reqBuffer.mid(nHdrEnd);

		//check if handler exists:
		//qDebug()<<"HTTP Server,request: "<<m_Read_header.toString();
		m_Read_pRequestHandler=HTTPUtil::GetRequestHandler(m_pLstHandlers,m_Read_header.path());
		if (m_Read_pRequestHandler==NULL)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"illegal domain requested.");
			AbortConnection(false); //don't kick, just close conn
			return;
		}
		//set socket timeout property:
		m_nOrphanSocketTimeOut=m_Read_pRequestHandler->m_nDefaultSocketTimeOut;

		if(m_nTimerID!=-1)
		{
			killTimer(m_nTimerID);
			m_nTimerID=-1;
		}

		//check if handler can initiate chunked receiving if client sends data in http chunks:
		if (m_Read_nTotalLength==-1)
		{
			//qDebug()<<m_Read_header.toString();
			HTTPRequest tmp_req(m_Read_header,QByteArray());
			HTTPResponse tmp_resp;
			Status err;
			if(!m_Read_pRequestHandler->InitChunkedHttpStream(err,m_ConnContext,tmp_req,tmp_resp,&m_StreamHandler))
			{
				if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_HANDLER_NOT_SUPPORT_CHUNK)
				{
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"handler does not support chunked transfer protocol.");
					AbortConnection();
					return;
				}
				if (err.getErrorCode()==StatusCodeSet::COM_HTTP_SEND_CONTINUE)
				{
					m_Read_bBlockReadStillProcessingLastChunk=true;
					SendResponse(err,tmp_resp);
					m_TCPSocket->flush(); //send GO to client at once!!-> dont change states...
					m_Read_bBlockReadStillProcessingLastChunk=false;
					return;
				}

				m_Read_bBlockReadStillProcessingLastChunk=true;
				SendResponse(err,tmp_resp);
				m_TCPSocket->flush();
				//qDebug()<<"tcp data flashed";
				m_reqBuffer.clear();
				if (m_nState!=STATE_WRITE)
				{
					m_nState=STATE_READY;
					if (m_SocketSemaphore->available()==0)m_SocketSemaphore->release(1); //just measure to clear all locked resources
				}
				return;
			}
		}
	}

	bool bEnd=HTTPUtil::CheckHTTPEndOfMessage(m_reqBuffer,m_Read_nTotalLength);
	if (m_Read_nTotalLength==-1 && m_StreamHandler) //chunked read: process chunk at once: bEnd mark last chunk
	{
		QByteArray chunk;
		bool bExtracted = HTTPUtil::ExtractHTTPChunk(m_reqBuffer,chunk);
		if (bExtracted)
		{
			Status err;
			m_StreamHandler->SetChunk(err,chunk,bEnd);
			if (!bEnd)
				bEnd=!err.IsOK(); //if error then END reading...close socket...but leave handler to process error: send response
			if (bEnd)
				m_reqBuffer.clear(); //if end, body is empty:  data is processed already
		}

		if (!bExtracted && bEnd) //if incoming msg can not be parsed...
		{
			Q_ASSERT(false);
			m_Read_bBlockReadStillProcessingLastChunk=true;
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"http message damaged, can not find end of chunked message.");
			AbortConnection();
			return;
		}
	}

	if(m_Read_pRequestHandler==NULL)
	{
		m_Read_bBlockReadStillProcessingLastChunk=true;
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"http message damaged.");
		AbortConnection();
		return;
	}

	//B.T.: added on 2013.01.28, limit total size of HTTP request
	if (m_reqBuffer.size()>HTTP_REQUEST_MAX_SIZE)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"http message is greater then 50Mb.");
		AbortConnection();
		return;
	}

	if (!bEnd) //not end of message, exit expect another
		return;

	m_Read_bBlockReadStillProcessingLastChunk=true;
	ProcessRequest(m_Read_pRequestHandler,m_Read_header,m_reqBuffer);


	//clear buffer-release socket
	m_reqBuffer.clear();
	if (m_nState!=STATE_WRITE)
	{
		m_nState=STATE_READY;
		if (m_SocketSemaphore->available()==0)m_SocketSemaphore->release(1); //just measure to clear all locked resources
	}

}

/*!
	When valid HTTP request is received, socket calls this function to process request.
	Handler is searched by domain in list of handlers held by HTTPServer. 
	If handler not found it will be nice to return something to client (error).
	If handler returns error, response will not be sent to client (used when client sends async messages)

	Chunked read: use m_StreamHandler and call this function after last chunk is received..
*/
void HTTPServerSocketHandler::ProcessRequest(HTTPHandler *pRequestHandler,QHttpRequestHeader &header, QByteArray &body)
{

#ifdef _DBG_SHOW_HTTP_PACKET_SIZE
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Incoming HTTP compressed chunk size: "+QVariant(m_reqBuffer.size()).toString());
#endif

		HTTPRequest requestMessage;
		HTTPResponse responseMessage;
		Status lastError;
		requestMessage.SetData(header,body); //decompression is done here if needed, not relevant for chunked http as body is probably empty
		m_reqBuffer.clear(); 
		
		//process request: expect response: if chunked then stremhandler object will be loaded, and request body empty..
		pRequestHandler->HandleRequest(lastError,m_ConnContext,requestMessage,responseMessage, &m_StreamHandler);
		SendResponse(lastError,responseMessage);
}


/*!
	Kicks IP on given timeperiod (seconds)
*/
void HTTPServerSocketHandler::KickBanIP_OnTimePeriod(QString strIP,int nTimePeriod)
{
	Q_ASSERT(false);
	Q_ASSERT(nTimePeriod>0);
	emit KickBanIP(strIP,nTimePeriod,0);
}
/*!
	Kicks IP on N tries: if again kicked, decresase counter-> set by HTTPServerConnectionSettings::m_nIntrusionTries
*/
void HTTPServerSocketHandler::KickBanIP_OnNTries(QString strIP)
{
	//Q_ASSERT(false);
	//qDebug()<<"Address "+strIP+" kicked";
	emit KickBanIP(strIP,0,0);
}

void HTTPServerSocketHandler::OnSSLErrors(const QList<QSslError>& lst)
{
#ifndef _DONT_USE_SSL
	int nSize=lst.count();
	for(int i=0;i<nSize;i++)
	{
		//client does not have to have cert nor server cert does have to be valid (expire)...

		if (lst.at(i).error()==QSslError::UnableToGetIssuerCertificate)
		{
			/*
			QSslConfiguration sslConfig = dynamic_cast<QSslSocket*>(m_TCPSocket)->sslConfiguration();
			QList<QSslCertificate> *caCerts = new QList<QSslCertificate>(sslConfig.caCertificates());
			int nSize=caCerts->size();
			bool bFound=false;
			for (int i=0;i<nSize;i++)
			{
				QString strMsg=QString("CA cert - Issuer info: %1, issue cn: %2, subject info: %3, subject cn: %4").arg(caCerts->at(i).issuerInfo(QSslCertificate::Organization).join("-")).arg(caCerts->at(i).issuerInfo(QSslCertificate::CommonName).join("-")).arg(caCerts->at(i).subjectInfo(QSslCertificate::Organization).join("-")).arg(caCerts->at(i).subjectInfo(QSslCertificate::CommonName).join("-"));
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,strMsg);
				if(caCerts->at(i).issuerInfo(QSslCertificate::Organization).indexOf("Daddy")>0)
				{
					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"SSL Error unable to get issue cert, but found in list");
					bFound=true;
					break;
				}
			}
			if (!bFound)
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"SSL Error unable to get issue cert, not found in list");
			}
			QSslCertificate localCert = dynamic_cast<QSslSocket*>(m_TCPSocket)->localCertificate();
			QString strMsg=QString("Used SSL cert - Issuer info: %1, issue cn: %2, subject info: %3, subject cn: %4").arg(localCert.issuerInfo(QSslCertificate::Organization).join("-")).arg(localCert.issuerInfo(QSslCertificate::CommonName).join("-")).arg(localCert.subjectInfo(QSslCertificate::Organization).join("-")).arg(localCert.subjectInfo(QSslCertificate::CommonName).join("-"));
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,strMsg);
			*/
		}

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"SSL Error: "+lst.at(i).errorString());
	}
	dynamic_cast<QSslSocket*>(m_TCPSocket)->ignoreSslErrors();
#endif
}

void HTTPServerSocketHandler::WriteSocketPacket(bool bDontChangeState)
{
	if (m_ByteWrittenToSocket<m_ByteToWrite && m_ByteWritten>=m_ByteWrittenToSocket)
	{
		//qint64 x=m_ByteWrittenToSocket;
		qint64 bytesToWrite=TCP_CHUNK_SIZE;
		if (m_ByteWritten+TCP_CHUNK_SIZE>m_ByteToWrite)
			bytesToWrite=m_ByteToWrite-m_ByteWrittenToSocket;
		const char *startOfData = m_outgoingBuffer.constData() + m_ByteWrittenToSocket;
		int nXstart=m_ByteWrittenToSocket;
		if(!bDontChangeState) //used for quick 100 continue response: keep previous state, just write 100 continue reponse header to client
			m_nState=STATE_WRITE;
		m_ByteWrittenToSocket=m_ByteWrittenToSocket+m_TCPSocket->write(startOfData,bytesToWrite);

		/*
		QFile file("D:/stream_response.txt");
		if(file.open(QIODevice::Append))
		{
			QByteArray buf=m_outgoingBuffer.mid(nXstart,bytesToWrite);
			buf.append("\n\r----------------------------");
			file.write(buf);
			file.close();
		}
		*/
		
	}

}

void HTTPServerSocketHandler::OnBytesWritten(qint64 bytes)
{
	m_ByteWritten+=bytes;
	//qDebug()<<ThreadIdentificator::GetCurrentThreadID()<<" bytes write total: "<<m_ByteWritten << " out of: "<<m_ByteToWrite <<" written to socket: "<<m_ByteWrittenToSocket;
	WriteSocketPacket();
	if (m_ByteWritten>=m_ByteToWrite && m_nState==STATE_WRITE)
	{
		m_outgoingBuffer.clear();
		if(m_StreamHandler) //is streaming: get another chunk: if error: abort connection, if something for write: write,else release socket
		{
			Status err;
			bool bEof=false;
			m_StreamHandler->GetChunk(err,m_outgoingBuffer); 
			//qDebug()<<ThreadIdentificator::GetCurrentThreadID()<<" again we are coming for streamer, new chunk size: "<<m_outgoingBuffer.size();
			if (!err.IsOK())
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Chunked send error: "+err.getErrorText());
				AbortConnection(false); //close connection but no kick
				return;
			}
			if (!m_outgoingBuffer.isEmpty())
			{
				m_ByteToWrite=m_outgoingBuffer.size();
				m_ByteWritten=0;
				m_ByteWrittenToSocket=0;
				WriteSocketPacket();
				return;
			}
		}
		if (m_StreamHandler){delete m_StreamHandler; m_StreamHandler=NULL;} //delete
		m_nState=STATE_READY;
		m_SocketSemaphore->release(1);
		if (m_bCloseSocketAfterResponse) //close it baby!
		{
			//emit DestroyThread();
			SocketDestroy();
		}
		//qDebug()<<ThreadIdentificator::GetCurrentThreadID()<<" thread is in READ state, destroy flag: "<<m_bCloseSocketAfterResponse;
	}
}

//in case of failure or client sending garbage, abort connection
void HTTPServerSocketHandler::AbortConnection(bool bKick)
{
	//to be safe: kill timer:
	if (m_nTimerID!=-1)
		killTimer(m_nTimerID);

	if (m_StreamHandler){delete m_StreamHandler; m_StreamHandler=NULL;} //precaution: should be deleted from previous stream...

	m_nTimerID=-1;
	m_bAbortConnectionInProgress=true;

	if(m_nState!=STATE_NOT_CONNECTED && m_TCPSocket!=NULL) 
		m_TCPSocket->abort(); 
	if (m_SocketSemaphore->available()==0)m_SocketSemaphore->release(1); //just measure to clear all locked resources
	m_nState=STATE_READY;
	if (bKick)
		KickBanIP_OnNTries(m_ConnContext.m_strPeerIP);
	//emit DestroyThread();
	SocketDestroy();
}

void HTTPServerSocketHandler::SendResponse(Status lastError,HTTPResponse &responseMessage)
{
	//return response if ordered
	m_bCloseSocketAfterResponse=false; //reset flag


	//B.T: begin streaming video right after buffer->ignore buffer content!!!
	//content-length must be set prior to this...
	if(lastError.getErrorCode()==StatusCodeSet::COM_HTTP_BEGIN_STREAM)
	{
		m_outgoingBuffer = responseMessage.toString().toLatin1(); //ignore..
		m_ByteWritten=0;
		m_ByteToWrite=0;
		m_ByteWrittenToSocket=0;
		//qDebug()<<responseMessage.toString();

		if(m_StreamHandler) //if streaming: get chunk
		{
			Status err;
			bool bEof=false;
			QByteArray dataFromStream;
			m_StreamHandler->GetChunk(err,dataFromStream); 
			if (!err.IsOK())
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Chunked send error: "+err.getErrorText());
				AbortConnection(false); //close connection but no kick
				return;
			}
			//qDebug()<<ThreadIdentificator::GetCurrentThreadID()<<" outgoing buffer size (header): "<<m_outgoingBuffer.size();
			m_outgoingBuffer = m_outgoingBuffer+dataFromStream;
			//qDebug()<<ThreadIdentificator::GetCurrentThreadID()<<" outgoing buffer size (header + body): "<<m_outgoingBuffer.size();
			if (!m_outgoingBuffer.isEmpty())
			{
				m_ByteToWrite=m_outgoingBuffer.size();
				m_ByteWritten=0;
				m_ByteWrittenToSocket=0;
				WriteSocketPacket();
				return;
			}
		}
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Stream send error");
		AbortConnection(false); //close connection but no kick
		return;
	}


	if(lastError.IsOK() || lastError.getErrorCode()==StatusCodeSet::COM_HTTP_KEEP_CONNECTION)
	{
		/*
		if (responseMessage.GetBody()->size()==0)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,444,"SENDING EMPTY BODY!!!!");
		}*/
		responseMessage.GetData(m_outgoingBuffer);
		responseMessage.GetBody()->clear(); //clean as not needed any more
		//qDebug()<<m_outgoingBuffer
		//write to socket:
		m_ByteWritten=0;
		m_ByteToWrite=m_outgoingBuffer.size();
		m_ByteWrittenToSocket=0;
		
#ifdef _DBG_SHOW_HTTP_HEADER
		//qDebug()<<m_outgoingBuffer.left(700);
		qDebug()<<responseMessage.toString();
#endif
		WriteSocketPacket();
		return;
	}


	
	//probably LBO redirect
	if(lastError.getErrorCode()==StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP)
	{
		/*
		if (responseMessage.GetBody()->size()==0)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,444,"SENDING EMPTY BODY!!!!");
		}
		*/
		responseMessage.GetData(m_outgoingBuffer);
		responseMessage.GetBody()->clear(); //clean as not needed any more
		//write to socket:
		m_ByteWritten=0;
		m_ByteToWrite=m_outgoingBuffer.size();
		m_ByteWrittenToSocket=0;
		m_bCloseSocketAfterResponse=true;
#ifdef _DBG_SHOW_HTTP_HEADER
		qDebug()<<responseMessage.toString();
		//qDebug()<<m_outgoingBuffer.left(700);
#endif
		WriteSocketPacket();
		return;
	}
	//if kick ban set to Black list & destroy this thread:
	if(lastError.getErrorCode()==StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN)
	{
		if (m_StreamHandler){delete m_StreamHandler; m_StreamHandler=NULL;} //delete
		//add to kick ban list for setted period (in error text)
		KickBanIP_OnTimePeriod(m_ConnContext.m_strPeerIP,QVariant(lastError.getErrorTextRaw()).toInt());
		//emit DestroyThread();
		SocketDestroy();
		return;
	}
	//kick client but give him chance to try his luck N times, before permanent kick
	if(lastError.getErrorCode()==StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN_NCOUNT)
	{
		if (m_StreamHandler){delete m_StreamHandler; m_StreamHandler=NULL;} //delete
		//add to kick ban list for N count
		KickBanIP_OnNTries(m_ConnContext.m_strPeerIP);
		//emit DestroyThread();
		SocketDestroy();
		return;
	}
	//close connection, kill this object if ordered
	if(lastError.getErrorCode()==StatusCodeSet::COM_HTTP_CLOSE_CONNECTION)
	{
		if (m_StreamHandler){delete m_StreamHandler; m_StreamHandler=NULL;} //delete
		//emit DestroyThread();
		SocketDestroy();
		return;
	}
	if(lastError.getErrorCode()==StatusCodeSet::COM_HTTP_SEND_CONTINUE)
	{
		/*
		if (responseMessage.GetBody()->size()==0)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,444,"SENDING EMPTY BODY!!!!");
		}
		*/
		responseMessage.GetData(m_outgoingBuffer);
		responseMessage.GetBody()->clear(); 
		m_ByteWritten=0;
		m_ByteToWrite=m_outgoingBuffer.size();
		m_ByteWrittenToSocket=0;
		WriteSocketPacket(true);
		return;
	}

	Q_ASSERT(false); //we never suppose to be here!!!

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SECURITY_HTTP_CONNECTION_FAILED,m_ConnContext.m_strPeerIP+";"+"response is not recognized, internal error, closing connection.");
	AbortConnection();
	return;

}