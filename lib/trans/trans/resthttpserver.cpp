#include "resthttpserver.h"
#include "common/common/config_version_sc.h"
#include "common/common/threadid.h"
#include "common/common/authenticator.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "common/common/logger.h"
#include "common/common/datahelper.h"

#include <QFile>
#include <QCoreApplication>
#include <QDir>
#include <QUrl>
#include <QTime>

#define SUBDIR_SERVICE_DESC "/webservices/rest/service_desc"
#define SUBDIR_SERVICE_SCHEMAS "/webservices/rest/schemas"


RestHTTPServer::RestHTTPServer(RpcSkeleton *pServerSkeletonSet,QString strWWWRootDir)
:RpcProtocol(RPC_PROTOCOL_TYPE_REST)
{

	if (strWWWRootDir.isEmpty())
		strWWWRootDir=QCoreApplication::applicationDirPath()+SUBDIR_SERVICE_SCHEMAS;
	m_strWWWRootDir=strWWWRootDir+SUBDIR_SERVICE_SCHEMAS;

	//pointers:
	m_ServerSkeletonSet=pServerSkeletonSet;
	Status err;

	/*
	m_ServerSkeletonSet->LoadServiceDescriptionData(err,QCoreApplication::applicationDirPath()+SUBDIR_SERVICE_DESC);
	if (!err.IsOK())
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,0,"Error while initializing webservices:"+err.getErrorText());
	*/
	LoadSchemas(err,m_strWWWRootDir);
	//if (!err.IsOK())
	//	ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,0,"Error while initializing webservices:"+err.getErrorText());

	//set domain:
	SetDomain(GetRPCDomain());
	m_datLastSave=QDateTime::currentDateTime();
}


void RestHTTPServer::HandleRequest(Status &err, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{
	err.setError(0);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Rest server: incoming request %1, thread: %2").arg(request.path()).arg(ThreadIdentificator::GetCurrentThreadID()));
		
	//qDebug()<<"----REST-----CALL-----------";
	//qDebug()<<"REST handler: "<<request.path();
	//qDebug()<<request.toString();
	//qDebug()<<"----------------------------";
	
	//QString strBody(request.GetBody()->constData());
	//qDebug()<<request.GetBody()->size();
 	//qDebug()<<strBody;

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1,"REST header: "+request.toString());
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1,"REST path: "+request.path());
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1,"REST body: "+strBody);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"REST request body: "+strBody);

	//g_Logger.FlushFileLog();

	if (request.path().right(12).toLower()=="/ping_server")
	{
		response.GetBody()->append("1");
		ResponseHandler(err,request,response);
		return;
	}


	//authenticate user:
	int nClientTimeZoneOffsetMinutes=0;
	if(!AuthenticateUser(err,ctx,request,response,nClientTimeZoneOffsetMinutes))
	{
		//qDebug()<<response.GetBody()->constData();
		return;
	}

	if(request.value("Accept-Encoding")=="deflate") //if request msg is compressed, use it back
		response.setValue("Content-Encoding","deflate");

	LockThreadActive();
	
	//intercept REQUEST URL: if schema is requested then pass it to schema server
	//else: rip off root URL part (http/dfgdg/rest and comprise new method as: METHOD+"/"+path
	QUrl url(request.path().toLower()); //strip query params if any...should not exists
	QString strPath=url.path();

	
	if (strPath.right(4)==".xsd" && strPath.left(QString("/rest/schemas").length())=="/rest/schemas")
	{
		QStringList lstPathParts=strPath.split("/");
		if (lstPathParts.size()>0)
		{
			QString schema=lstPathParts.at(lstPathParts.size()-1);
			HandleSchemaRequest(err,schema,request,response);
			UnLockThreadActive();
			return;
		}
	}

	if (strPath.left(5)=="/rest")
	{
		strPath=request.method().toUpper()+"/"+strPath.mid(6); //can be GET, GET/ or GET/contacts etc...
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_INVALID_REQUEST);
		ResponseHandler(err,request,response);
		return;
	}

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Rest server: start processing %1, thread: %2").arg(request.path()).arg(ThreadIdentificator::GetCurrentThreadID()));
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"Processing ("+ctx.m_strPeerIP+":"+QVariant(ctx.m_nSocketID).toString()+"/thread: "+QString::number(ThreadIdentificator::GetCurrentThreadID())+"): REST call:"+url.path());

	//qDebug()<<request.GetBody()->constData();
	//QTime timer_api;
	//timer_api.start();
	//qDebug()<<ctx.m_nSocketID<<" start: "<<request.path();
	m_ServerSkeletonSet->HandleRPC(err,request.GetBody(),response.GetBody(),strPath, nClientTimeZoneOffsetMinutes);
	//qDebug()<<ctx.m_nSocketID<<" end: "<<request.path()
	//int nElapsed = timer_api.elapsed();
	/*
	m_lck.lockForWrite();
	if (nElapsed>m_lstAPITiming.value(request.path(),0))
	{
		m_lstAPITiming[request.path()] = timer_api.elapsed();
	}
	if (m_datLastSave.secsTo(QDateTime::currentDateTime())>60)
	{
		//dump to file:
		QString strData;
		QHashIterator<QString,int> x(m_lstAPITiming);
		while (x.hasNext()) 
		{
			x.next();
			strData+=x.key()+": "+QString::number(x.value())+"\r\n";
		}
		if (!strData.isEmpty())
		{
			m_datLastSave=QDateTime::currentDateTime();
			DataHelper::WriteFile(QCoreApplication::applicationDirPath()+"/settings/api_timing.txt",strData.toLatin1());
		}
		m_lstAPITiming.clear();
	}
	m_lck.unlock();
	*/

	//qDebug()<<response.GetBody()->constData();

	if (!err.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1,"Error after REST call:"+url.path()+" err: "+err.getErrorText());
	}


	//qDebug()<<"error on rest rpc: "<<err.getErrorText();

	UnLockThreadActive();
	ResponseHandler(err,request,response);


	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Processed("+ctx.m_strPeerIP+":"+QVariant(ctx.m_nSocketID).toString()+"/thread: "+QString::number(ThreadIdentificator::GetCurrentThreadID())+"): "+strPath+" rest unzip: "+QVariant(response.GetBody()->size()).toString());

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Rest server: processed request %1, thread: %2").arg(request.path()).arg(ThreadIdentificator::GetCurrentThreadID()));

	//QString strBodyRes(request.GetBody()->constData());
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1,"REST body response: "+strBodyRes);
	/*
	if (!err.IsOK())
	{
		qDebug()<<"response error body: "<< response.GetBody()->constData();
		qDebug()<<"response error header: "<< response.toString();
	}
	*/
}

void RestHTTPServer::HandleSchemaRequest(Status &err, QString schemaName, HTTPRequest &request, HTTPResponse &response)
{
	if (!m_hshSchemas.contains(schemaName))
	{
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,404,"File not found");
		return;
	}
	response.SetBody(m_hshSchemas.value(schemaName));
	SetResponseHeader(request,response,200,"OK");
	err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
}


void RestHTTPServer::ResponseHandler(Status &err,HTTPRequest &request, HTTPResponse &response)
{
	switch(err.getErrorCode())
	{
	case 0:	// no error
		//if(request.value("Connection")!="Keep-Alive")
		//	err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,200,"OK");
		err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		return;
	case StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK:
		response.setValue("Allow",err.getErrorText());
		response.setValue("Access-Control-Allow-Methods",err.getErrorText());
		response.setValue("Access-Control-Allow-Headers",request.value("Access-Control-Request-Headers"));
		response.setValue("Access-Control-Max-Age",QString::number(1728000)); //20days
		SetResponseHeader(request,response,200,"OK");
		err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		return;
	
	case StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER: //special use-case for invalid authentication
		SetResponseHeader(request,response,401,"Unauthorized");
		err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		return;
	
	case StatusCodeSet::ERR_SYSTEM_KICKBAN:
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN,err.getErrorTextRaw());
		return;
	case StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL:
	case StatusCodeSet::ERR_XML_BODY_PARSE_FAIL:
	case StatusCodeSet::ERR_XML_PARAM_MISSING:
	case StatusCodeSet::ERR_XML_PARSE_FAIL_INVALID_XML:
		GenerateXMLError(err,request,response);
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,400,err.getErrorText());
		return;
	case StatusCodeSet::ERR_RPC_INVALID_REQUEST:
	case StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS:
		GenerateXMLError(err,request,response);
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,404,"File not found");
		return;

	default:
		//if(request.value("Connection")!="Keep-Alive")
		//	err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,500,"Internal Server Error");
		err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		return;
	}

}


void RestHTTPServer::SetResponseHeader(HTTPRequest &request,HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(nHTTPStatusCode,strHTTPCodeText);
	response.setValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setValue("Cache-Control","no-cache");
	response.setValue("Content-Type","text/xml");
	response.setValue("Access-Control-Allow-Origin","*");

	if (!request.value("Cookie").isEmpty())
		response.setValue("Set-Cookie",request.value("Cookie"));

}

void RestHTTPServer::LoadSchemas(Status &err,QString strFilePath)
{
	err.setError(0);
	m_hshSchemas.clear();

	//read all files resource directory:
	QDir dir(strFilePath);
	if (!dir.exists())
	{
		err.setError(1,QObject::tr("Schema Directory:")+strFilePath+QObject::tr(" does not exists!"));
		return ;
	}
	//find all subdirs and load 'em, then load all files inside:
	QFileInfoList lstFilesXML=dir.entryInfoList();
	int nSize2=lstFilesXML.size();

	for(int k=0;k<nSize2;++k)
	{
		if(lstFilesXML.at(k).completeSuffix()=="xsd")
		{
			QFile xmlFile(lstFilesXML.at(k).absoluteFilePath());
			if(!xmlFile.open(QIODevice::ReadOnly))
			{
				err.setError(1,QObject::tr("File: ")+lstFilesXML.at(k).absoluteFilePath()+QObject::tr(" can not be read!"));
				return;
			}
			//qDebug()<<lstFilesXML.at(k).fileName();
			m_hshSchemas[lstFilesXML.at(k).fileName()]=xmlFile.readAll();
			xmlFile.close();
		}
	}
}


void RestHTTPServer::GenerateXMLError(Status &err,HTTPRequest &request,HTTPResponse &response)
{
	RpcSkeletonMessageHandler rpc(this,request.GetBody(),response.GetBody(),0);
	rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());
}
