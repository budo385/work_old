#include "rpcskeletonmessagehandler.h"


/*! Constructor: makes instances of rpcrequest & rpcresponse message handlers
	fill them with buffers. When this object is destroyed msg instances are deleted
	automatically

	\param skeleton - pointer to skeleton object, must have GetRPCMessageHandler method 
	\param pBufRequest - request buffer, msg_in is init with this one
	\param pBufResponse - response buffer, msg_out is init with this one
*/
RpcSkeletonMessageHandler::RpcSkeletonMessageHandler(RpcProtocol *skeleton,QByteArray *pBufRequest,QByteArray *pBufResponse, int nClientTimeZoneOffsetMinutes)
{	
	skeleton->GetRPCMessageHandler(&msg_in);msg_in->SetBuffer(pBufRequest);msg_in->SetTimeZoneOffset(nClientTimeZoneOffsetMinutes);
	skeleton->GetRPCMessageHandler(&msg_out);msg_out->SetBuffer(pBufResponse);msg_out->SetTimeZoneOffset(nClientTimeZoneOffsetMinutes);
}



/*! Destructor: free memory */
RpcSkeletonMessageHandler::~RpcSkeletonMessageHandler(){
	delete msg_in; delete msg_out;
}


/*! Extracts NameSpace from method name from msg_in, or request buffer. 
	 NameSpace is string until first dot: "SrvMsg.Method" -> NameSpace="SrvMsg"
	\return NameSpace - If error then returns empty string.
*/
QString RpcSkeletonMessageHandler::GetRequestMethodNameSpace(){
	//get method name, test for error:
	Status err;
	QString strMethodName=msg_in->GetMethodName(err);if(!err.IsOK())return "";

	//find DOT:
	int npos=strMethodName.indexOf(".");
		if( npos<=0)return "";
	
	//return NameSpace:
	return strMethodName.left(npos);
}


/*! Extracts method name 
	 NameSpace is string until first dot: "SrvMsg.Method" -> NameSpace="SrvMsg"
	 Method is string after dot!
	\return NameSpace - If error then returns empty string.
*/
QString RpcSkeletonMessageHandler::GetSkeletonMethodName(){
	//get method name, test for error:
	Status err;
	QString strMethodName=msg_in->GetMethodName(err);if(!err.IsOK())return "";

	//find DOT:
	int npos=strMethodName.indexOf(".");
		if( npos<=0)return "";
	
	//return MethodName:
	return strMethodName.right(strMethodName.length()-npos-1);
}

