#ifndef HTMLHTTPSERVER_H
#define HTMLHTTPSERVER_H

#include "trans/trans/httphandler.h"
#include <QHash>
#include <QSemaphore>
#include <QDateTime>

class HtmlDocument
{
	public:
		HtmlDocument(QByteArray content=0,QDateTime date=QDateTime());
		QByteArray byteContent;
		QString strLastModified;
		QString strETag;
};

class HtmlHTTPServer : public HTTPHandler
{

public:
	HtmlHTTPServer(QString strWWWRootDir="",const QHash<QString,HtmlDocument> *lstWebPages=NULL);
	void HandleRequest(Status &err, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);
	void ReloadWebPages(Status &err){LoadDocuments(err,m_strWWWRootDir);};

protected:
	void			SetResponseHeader(HTTPRequest &request,HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText);
	void			LoadDocuments(Status &err,QString strFilePath);
	void			GenerateXMLError(Status &err,HTTPRequest &request,HTTPResponse &response);
	QString			ResolvePath(QString strPath, QString strReferer, bool &bRedirect);
	QString			CleanPath(QString strPath);

	QHash<QString,HtmlDocument> m_hshDocs;					//collection of all files used
	QString						m_strWWWRootDir;
	QSemaphore					*m_pSemLoadDocs;			//quick patch: document will be loaded at startup and that's it
};

#endif // HTMLHTTPSERVER_H
