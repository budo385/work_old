#ifndef SMTPSOCKET_H
#define SMTPSOCKET_H

#include <QObject>
#include <QTimerEvent>
#include "common/common/threadsync.h"
#include <QTcpSocket>
#include <QTextStream>


class SMTPSocket : public QObject
{
	Q_OBJECT

public:

	enum states
	{
		Init,
		Connected,
		Auth,
		User,
		Pass,
		Mail,
		Rcpt,
		Data,
		Body,
		Quit,
		Close
	};

	

	SMTPSocket(ThreadSynchronizer *ThreadSync);
	~SMTPSocket();

	void SetParameters(const QString &strMailServer, int nConnectionTimeOut, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc,  QString strUserName ="", QString strPassword="",const bool bBodyIsHtml=false, const bool bSendBodyAsFormatedMimeMessage=false,int nSmtpPort=0, bool bIsUTF8=false); 
	bool		m_bErrorOccured;
	QString		m_strLastError;

public slots:
	void SendMail(); 
	void Destroy();

private slots:
	void timerEvent(QTimerEvent *event);
	void errorReceived(QAbstractSocket::SocketError socketError);
	void disconnected();
	void readyRead();
	void connected();

private:
	void ClearState();

	QString m_strMessage;
	QString m_strMailServer;
	QString m_strFrom;
	QList<QString> m_lstRcpt;
	int nRcptIndex;

	QTcpSocket *m_socket;
	int			m_State;
	int			m_nSmtpPort;

	int			m_nTimerID;

	QTextStream *t;
	QString		response;

	QString		m_strUser;
	QString		m_strPass;
	bool		m_bIsUTF8;

	int m_nTimeOut;
	ThreadSynchronizer *m_ThreadSync;
	
};



#endif // SMTPSOCKET_H
