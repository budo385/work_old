#ifndef RPCSKELETONDISPATCHER_H
#define RPCSKELETONDISPATCHER_H


#include "common/common/status.h"
#include "rpcskeleton.h"
#include "rpcskeletonmessagehandler.h"


/*!
    \class RpcSkeletonDispatcher
    \brief The RpcSkeletonDispatcher class is a convenience RpcSkeleton subclass that implements HandleRPC
    \ingroup RPCModule

	Use: inherit this dispatcher and register skeleton objects in constructor. RPC call will be dispatched
	by NameSpace domain of each skeleton object.

*/

class RpcSkeletonDispatcher: public RpcSkeleton
{
public:
	RpcSkeletonDispatcher(int RPCType):RpcSkeleton(RPCType){};
	~RpcSkeletonDispatcher();
		
	//main RPC dispatcher:
	virtual bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nClientTimeZoneOffsetMinutes=0);

protected:
	QList<RpcSkeleton *> m_SkeletonList;	//< Used to store pointers to subskeletons in skeleton set

};


#endif //RPCSKELETONDISPATCHER_H
