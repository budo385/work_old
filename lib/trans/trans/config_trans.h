#ifndef CONFIG_TRANS_H
#define CONFIG_TRANS_H

#define SSL_PRIVATE_KEY								"SOKRATES(R)"
//#define SSL_PRIVATE_KEY								""
#define HTTP_CONNECTION_TIMEOUT_INTERVAL			150000			//milisecs	-> must be smaller then IS_ALIVE_INTERVAL
#define HTTP_SERVER_ACCEPT_CONNECTION_TIMEOUT		20000			//milisecs	-> time from connection established to the first valid processing
#define HTTP_HEADER_MAX_SIZE						52428800		//bytes     -> maximum size of first request: against onverflood buffers... //50Mb
#define HTTP_COMPRESSION_LEVEL						2				//from 0(no compression) to 9(highest compression), -1 is default
#define TCP_CHUNK_SIZE								524288			//if packet larger then this, chunk it in parts (bytes)
#define XML_MAXIMUM_BINARY_SIZE						81920000		//80Mb maximum binary size
#define HTTP_REQUEST_MAX_SIZE						52428800		//bytes     -> maximum size of first request: against onverflood buffers... //50Mb

//domains: REST protocol, domain define resource to use..interesting
#define DOMAIN_XML_RPC				"XMLRPC"
#define DOMAIN_SOAP_RPC				"SOAP"
#define DOMAIN_REST_RPC				"REST"
#define DOMAIN_HTTP_PING			"PING"
#define DOMAIN_HTTP_USER_STORAGE	"USER_STORAGE"
#define DOMAIN_HTTP_WEBSERVICE		"WEBSERVICE"
#define DOMAIN_HTTP_HTML			"HTML"
#define DOMAIN_HTTP_MULTIPART_UPLOAD "MULTIPART_UPLOAD"

#define SERVER_HTTP_DESCRIPTION		"Sokrates HTTP Server/Build: %1"//+APPLICATION_VERSION
#define CLIENT_HTTP_DESCRIPTION		"Sokrates HTTP Client/Build: %1"//+APPLICATION_VERSION


#ifdef _DEBUG
	//#define _DBG_SHOW_HTTP_PACKET_SIZE
#endif

#endif //CONFIG_TRANS_H