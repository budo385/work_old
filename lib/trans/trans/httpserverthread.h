#ifndef HTTPSERVERTHREAD_H 
#define HTTPSERVERTHREAD_H

#include <QThread>
#include "ipkicker.h"
#include "common/common/status.h"
#include "httpserversockethandler.h"
#include "httpserverconnectionsettings.h"
#include "httpcontext.h"
#include "httpserver.h"


/*!
    \class HTTPServerThread
    \brief Client thread in multithreaded HTTP server. Multithread safe!
    \ingroup HTTPServer

	Can send messages to the server and can receive async. server messages.
	When created it runs separate (socket) thread in which HTTPServerSocketHandler object is handling events 
	from TCPSocket. Socket can be ordinary or SSL. 

	Acess to all functions is serialized, so more then one thread can be access one global instance of this object
	Use:
		Create object with valid socketDescriptor, thread is automatically started, when server wishes to close connection
		simple destroy object (all pending operations will be finished first). If socket generates error it will autodestruct!
	SendData(HTTPRequest) ASYNC sending of data. 
		NOTE: calling thread is stopped until message is safely written to socket, to avoid multithread buffer overwrite
	emits SocketConnect when created and SocketDestroy when destroyed, signals that can be used to register handler

	NOTE: HTTPServerThread is uniquely defined by SocketID available through GetSocketDescriptor() or GetThreadID()
*/


class HTTPServer;

class HTTPServerThread :  public QThread //public ServerThread//
{ 
	Q_OBJECT

public:

	HTTPServerThread(HTTPServer	*pServer, int nSocketID, HTTPServerConnectionSettings &ConnSettings,QList<HTTPHandler *> lstHandlers,IPKicker* pIPAddressTest=NULL, int nDefaultConnectionAttemptsAllowed=0);
	~HTTPServerThread();

    void run();
	//--------------------------------------
	//For sending msg to the client
	//--------------------------------------
	void SendData(Status& pStatus, HTTPRequest* pHTTPRequest);
	void SendDataBroadCast(Status& pStatus, QByteArray* pBroadCastMsgBuffer);

	//--------------------------------------
	//Access to shared data between threads
	//--------------------------------------
	Status		GetLastError();
	HTTPContext GetConnectionContext();
	int			GetSocketState();
	QDateTime	GetSocketCreatedTime();
	int			GetOrphanSocketTimeOut(); //in minutes, 0-delete at once (default), >0 compare current datetime with m_datCreated
	int			GetSocketThreadID(){return m_nSocketThreadID;}
	int			GetSocketID(){return m_nSocketID;}


public slots:
	void ExecuteDestroyThread();
	void OnKickBanIP(QString strIP,int nBanTime,int nBanCounter);
		 
signals:
	void SocketDestroy();
	void ThreadDestroyed(int nThreadID);
	void SocketSendServerMessage(QByteArray);
	

private:
	HTTPServerSocketHandler			*m_pSocketHandler;
	IPKicker						*m_pIPAddressTest;						///< pointer for callback functions for registering client thread
	int								m_nSocketID;							///< socket ID
	int								m_nSocketThreadID;						///< socket thread ID
	HTTPServerConnectionSettings	m_ConnSettings;							///< connection settings for SERVER, can be used by socket
	int								m_nDefaultConnectionAttemptsAllowed;
	bool							m_bClosing;
	bool							m_bIPKicked;
	QList<HTTPHandler *>			m_pLstHandlers;			///< holds all handlers for all HTTP requests: this is common for all HTTP server threads in use
	HTTPServer						*m_pServer;
	QReadWriteLock					m_lck;
};





/*

//test: to assure max flow: spawn 2 new threads for socket:
class HTTPServerThreadSpawner :  public QThread 
{
	Q_OBJECT

public:

	HTTPServerThreadSpawner(HTTPServer *pServer,int nSocketID, HTTPServerConnectionSettings &ConnSettings,IPKicker* pIPAddressTest=NULL, int nDefaultConnectionAttemptsAllowed=0);
	~HTTPServerThreadSpawner();
	void run();

signals:
	void ThreadCreated(int nSocketID,HTTPServerThread*);
	void ThreadDestroyed(int nSocketID);

private:
	HTTPServer						*m_pServer;
	IPKicker						*m_pIPAddressTest;						///< pointer for callback functions for registering client thread
	int								m_nSocketID;							///< socket ID
	HTTPServerConnectionSettings	m_ConnSettings;							///< connection settings for SERVER, can be used by socket
	int								m_nDefaultConnectionAttemptsAllowed;
};

*/

#endif //HTTPSERVERTHREAD_H
