#ifndef PUSHSENDER_H
#define PUSHSENDER_H

#include <QThread>
#include "common/common/threadsync.h"
#include "pushnotification.h"


/*!
	\class PushSender

	Based on device type creates push object specific to device and wraps API through this simple interface
	Object will wait till server responds or until timeout
*/
class PushSender : public QThread
{
	Q_OBJECT

public:
	PushSender(int nDeviceType,int nTimeOut=10000);
	~PushSender();

	enum DeviceType
	{
		APPLE,
		ANDROID,
		WINDOWS,
		GOOGLE_CLOUD_MSG
	};

	bool	SendPushMessage(QString strDeviceID, QString strApplicationID, QString strMessage, int nMessageID, QStringList lstMessageCustomData, QString strPushSoundName="default", int nBadgeNumber=0);
	QString GetLastError();

signals:
	void SendPushMessageSignal();
	void Destroy();

private:
	void run();

	QString				m_strLastError;
	int					m_nDeviceType;
	int					m_nTimeOut;
	PushNotification	*m_Pusher;
	ThreadSynchronizer	m_ThreadSync;///< for syncing between mastr & slave thread

};

#endif // PUSHSENDER_H
