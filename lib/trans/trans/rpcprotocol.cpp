#include "rpcprotocol.h"
#include "xmlrpcmessage.h"
#include "restrpcmessage.h"

/*!
	Makes instances of RPC message object and returns in given pointer.
	What RPC message object will be instanced, it depends on m_nRPCType, setted in constructor:
	can be: RPC_PROTOCOL_TYPE_XML_RPC, RPC_PROTOCOL_TYPE_SOAP, etc..
	\param pRPCMessage pointer to pointer filled with address of new RPC message object
*/
void RpcProtocol::GetRPCMessageHandler(RpcMessage **pRPCMessage){

	switch(m_nRPCType)
	{
		case RPC_PROTOCOL_TYPE_XML_RPC:
			*pRPCMessage = new XmlRpcMessage; return;
		case RPC_PROTOCOL_TYPE_SOAP:
			Q_ASSERT_X(0,"SOAP not supportted","SOAP not supportted");return;
		case RPC_PROTOCOL_TYPE_REST:
			*pRPCMessage = new RestRpcMessage; return;
		default:
			Q_ASSERT_X(0,"Protocol not supportted","Protocol not supportted");return;
	}

}




 /*!
	Used by children to get right RPC domain (used in HTTP headers)
	\return domain
*/
QString RpcProtocol::GetRPCDomain()
{
	QString strDomain="";

	switch(m_nRPCType)
	{
		case RPC_PROTOCOL_TYPE_XML_RPC:
			strDomain=DOMAIN_XML_RPC;
			break;

		case RPC_PROTOCOL_TYPE_SOAP:
			strDomain=DOMAIN_SOAP_RPC;
			break;
		case RPC_PROTOCOL_TYPE_REST:
			strDomain=DOMAIN_REST_RPC;
			break;
		default:
			Q_ASSERT_X(0,"Protocol not supportted","Protocol not supportted");

	}

	return strDomain;
}

