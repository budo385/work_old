#include "restrpcvalue.h"
#include "xmlutil.h"



/* error */
void RestRpcValue::Serialize_Error(QByteArray *pBuffer,const int &nErrorCode, const QString &strErrorText)
{
	*pBuffer+="<ERROR>";

	QVariant nqvErrorCode((int)nErrorCode);
	Serialize_Integer(pBuffer,&nqvErrorCode,"ERROR_CODE"); 
	Serialize_String(pBuffer,&strErrorText,"ERROR_TEXT"); 

	*pBuffer+="</ERROR>";
}

//check if fault response, if not return ok status: only for client
void RestRpcValue::DeSerialize_Error(const QDomNode &parameter,Status& pStatus)
{
	pStatus.setError(0);
	if (parameter.toElement().tagName()!="ERROR")
		return;

	QDomNodeList nodes=parameter.childNodes();
	if (nodes.size()!=2)
		return;
	
	QVariant intVal;
	QString strText;
	Status err;
	DeSerialize_Integer(nodes.at(0),&intVal,pStatus);
	if (!pStatus.IsOK()) return;
	DeSerialize_String(nodes.at(1),&strText,pStatus);
	if (!pStatus.IsOK()) return;

	pStatus.setError(intVal.toInt(),strText);
}



void RestRpcValue::Serialize_String(QByteArray *pBuffer,const QString* pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=XmlUtil::xmlEncode(*pValue).toUtf8(); //escape special chars: \/& etc...+utf8 encode
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";
}

void RestRpcValue::Serialize_String(QByteArray *pBuffer,const QVariant* pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=XmlUtil::xmlEncode(pValue->toString()).toUtf8(); //escape special chars: \/& etc...+utf8 encode
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";
}

void RestRpcValue::DeSerialize_String(const QDomNode &parameter,QString* pValue,Status& pStatus,QString *strParamName)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}

	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QString();
	}
	else
	{
		//B.T. 2013-08-22 found that QDom already decode XML escape characters in process of parsing....
		*pValue=elm.text();
		//QString strT=elm.text();
		//QString strTX=XmlUtil::xmlDecode(elm.text().toUtf8());

		//*pValue=pValue->fromUtf8(XmlUtil::xmlDecode(elm.text().toUtf8())); //decode and convert UTF8
		//*pValue=pValue->fromUtf8(XmlUtil::xmlDecode(elm.text().toUtf8())); //decode and convert UTF8
	}
	if (strParamName)
		*strParamName=elm.tagName();
}

void RestRpcValue::Serialize_Bool(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=(pValue->toBool()==true ? "1": "0");
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}
void RestRpcValue::DeSerialize_Bool(const QDomNode &parameter,QVariant* pValue,Status& pStatus,QString *strParamName)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}
	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QVariant(QVariant::Bool);
	}
	else
	{
		*pValue=(elm.text().toLatin1()=="1" ? true: false);
	}
	if (strParamName)
		*strParamName=elm.tagName();

}

void RestRpcValue::Serialize_Integer(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=pValue->toString().toLatin1();
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}
void RestRpcValue::DeSerialize_Integer(const QDomNode &parameter,QVariant* pValue,Status& pStatus,QString *strParamName)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}
	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QVariant(QVariant::Int);
	}
	else
	{
		*pValue=elm.text().toInt();
	}
	if (strParamName)
		*strParamName=elm.tagName();

}


void RestRpcValue::Serialize_ULongLong(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=pValue->toString().toLatin1();
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}
void RestRpcValue::DeSerialize_ULongLong(const QDomNode &parameter,QVariant* pValue,Status& pStatus,QString *strParamName)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}
	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QVariant(QVariant::ULongLong);
	}
	else
	{
		*pValue=elm.text().toULongLong();
	}
	if (strParamName)
		*strParamName=elm.tagName();
}


void RestRpcValue::Serialize_Double(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=pValue->toString().toLatin1();
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}
void RestRpcValue::DeSerialize_Double(const QDomNode &parameter,QVariant* pValue,Status& pStatus,QString *strParamName)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}
	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QVariant(QVariant::Double);
	}
	else
	{
		*pValue=elm.text().toDouble();
	}
	if (strParamName)
		*strParamName=elm.tagName();

}

void RestRpcValue::Serialize_DateTime(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName, int nTimeZoneOffsetSeconds)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		if (nTimeZoneOffsetSeconds)
			*pBuffer+=pValue->toDateTime().addSecs(-nTimeZoneOffsetSeconds).toString(Qt::ISODate)+"Z"; //ISO 8601 YYYY-MM-DDTHH:MM:SS
		else
			*pBuffer+=pValue->toDateTime().toString(Qt::ISODate)+"Z"; //ISO 8601 YYYY-MM-DDTHH:MM:SS

	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}
void RestRpcValue::Serialize_DateTime(QByteArray *pBuffer,const QDateTime *pValue, const QString strParamName, int nTimeZoneOffsetSeconds)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";

		if (nTimeZoneOffsetSeconds)
			*pBuffer+=(pValue->addSecs(-nTimeZoneOffsetSeconds)).toString(Qt::ISODate)+"Z"; //ISO 8601 YYYY-MM-DDTHH:MM:SS
		else
			*pBuffer+=pValue->toString(Qt::ISODate)+"Z"; //ISO 8601 YYYY-MM-DDTHH:MM:SS
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}

void RestRpcValue::DeSerialize_DateTime(const QDomNode &parameter,QDateTime* pValue,Status& pStatus,QString *strParamName, int nTimeZoneOffsetSeconds)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}
	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QDateTime();
	}
	else
	{
		QString strText=elm.text(); //chop last Z for UTC mark
		if (strText.right(1).toLower()=="z")
			strText.chop(1);
		if (nTimeZoneOffsetSeconds)
			*pValue=QDateTime::fromString(strText,Qt::ISODate).addSecs(nTimeZoneOffsetSeconds);
		else
			*pValue=QDateTime::fromString(strText,Qt::ISODate);

	}
	if (strParamName)
		*strParamName=elm.tagName();

}


void RestRpcValue::Serialize_Date(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=(pValue->toDate()).toString(Qt::ISODate); //ISO 8601 YYYY-MM-DD
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}
void RestRpcValue::Serialize_Date(QByteArray *pBuffer,const QDate *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=pValue->toString(Qt::ISODate);
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}

void RestRpcValue::DeSerialize_Date(const QDomNode &parameter,QDate* pValue,Status& pStatus,QString *strParamName)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}
	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QDate();
	}
	else
	{
		*pValue=QDate::fromString(elm.text(),Qt::ISODate);
	}
	if (strParamName)
		*strParamName=elm.tagName();

}

void RestRpcValue::Serialize_Time(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=pValue->toTime().toString(Qt::ISODate); //ISO 8601 HH:MM:SS
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}
void RestRpcValue::Serialize_Time(QByteArray *pBuffer,const QTime *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=pValue->toString(Qt::ISODate);
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}

void RestRpcValue::DeSerialize_Time(const QDomNode &parameter,QTime* pValue,Status& pStatus,QString *strParamName)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}
	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QTime();
	}
	else
	{
		*pValue=QTime::fromString(elm.text(),Qt::ISODate);
	}
	if (strParamName)
		*strParamName=elm.tagName();

}

void RestRpcValue::Serialize_Binary(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=((QByteArray*)pValue)->toBase64(); 
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}
void RestRpcValue::Serialize_Binary(QByteArray *pBuffer,const QByteArray *pValue, const QString strParamName)
{
	if(!pValue->isNull())	
	{
		*pBuffer+="<"+strParamName+">";
		*pBuffer+=pValue->toBase64(); 
	}
	else
	{
		*pBuffer+="<"+strParamName+" xsi:nil=\"true\"/>";  //e.g. <MY_PARAM xsi:nil=true></MY_PARAM>
		return;
	}
	*pBuffer+="</"+strParamName+">";

}

void RestRpcValue::DeSerialize_Binary(const QDomNode &parameter,QByteArray* pValue,Status& pStatus,QString *strParamName)
{
	QDomElement elm=parameter.toElement();
	if (!elm.isElement())
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,elm.tagName());
		return;
	}
	QString strIsNull=elm.attribute("xsi:nil","").toLower();
	if (strIsNull=="true")
	{
		*pValue=QByteArray();
	}
	else
	{
		*pValue=QByteArray::fromBase64(elm.text().toLatin1());
	}
	if (strParamName)
		*strParamName=elm.tagName();
}

/*
Table is coded as:

<table>
	<row url="http:/rest/contact/BCNT_ID"> 
		<BCNT_ID> </BCNT_ID>
		<BCNT_LASTNAME> </BCNT_LASTNAME>
		<BCNT_ID> </BCNT_ID>
	</row>
	<row url="http:/rest/contact/BCNT_ID"> 
		<BCNT_ID> </BCNT_ID>
		<BCNT_LASTNAME> </BCNT_LASTNAME>
		<BCNT_ID> </BCNT_ID>
	</row>
</table>

strRowName				- if defined it will be used for tagName instead of row
strRowURLAttr			- url for each row details
strRowURLAttr_IDField	- field with id which will be appended to the strRowURLAttr, e.g.: http:sokrates.com/contacts/1

*/

void RestRpcValue::Serialize_DbRecordSet(QByteArray *pBuffer,const DbRecordSet *pValue, const QString strParamName, const QString strRowName, const QString strRowURLAttr,QString strRowURLAttr_IDField, int nTimeZoneOffsetSeconds)
{
	*pBuffer+="<"+strParamName+">";
	//*pBuffer+="<TABLE>";

	int nSize=pValue->getRowCount();
	int nSizeCol=pValue->getColumnCount();
	for(int j=0;j<nSize;j++)
	{
		if (!strRowName.isEmpty())
			*pBuffer+="<"+strRowName;
		else
			*pBuffer+="<row";

		if (!strRowURLAttr.isEmpty())
		{
			*pBuffer+=" url=\""+strRowURLAttr;
		}
		if (!strRowURLAttr_IDField.isEmpty())
		{
			QString strID;
			if (!pValue->getDataRef(j,strRowURLAttr_IDField).isNull())
				strID=pValue->getDataRef(j,strRowURLAttr_IDField).toString();
			*pBuffer+="/"+strID+"\"";
		}
		else if (!strRowURLAttr.isEmpty())
		{
			*pBuffer+="\""; //enclose "
		}

		
		*pBuffer+=">";


		for(int i=0;i<nSizeCol;i++)
		{
			int nColType=pValue->getColumnType(i);
			QString strColName=pValue->getColumnName(i);
			switch(nColType)
			{
			case QVariant::String:
				{
					Serialize_String(pBuffer,&pValue->getDataRef(j,i),strColName);
				}
				break;
			case QVariant::Int:
				{
					Serialize_Integer(pBuffer,&pValue->getDataRef(j,i),strColName);
				}
				break;
			case QVariant::ULongLong:
				{
					Serialize_ULongLong(pBuffer,&pValue->getDataRef(j,i),strColName);
				}
				break;
			case QVariant::Date:
				{
					Serialize_Date(pBuffer,&pValue->getDataRef(j,i).toDate(),strColName);
				}
				break;

			case QVariant::DateTime:
				{
					Serialize_DateTime(pBuffer,&pValue->getDataRef(j,i).toDateTime(),strColName,nTimeZoneOffsetSeconds);
				}
				break;
			case QVariant::Double:
				{
					Serialize_Double(pBuffer,&pValue->getDataRef(j,i),strColName);
				}
				break;

			case QVariant::Bool:
				{
					Serialize_Bool(pBuffer,&pValue->getDataRef(j,i),strColName);
				}
				break;
			case QVariant::ByteArray:
				{
					Serialize_Binary(pBuffer,&pValue->getDataRef(j,i),strColName);
				}
				break;
			case QVariant::Time:
				{
					Serialize_Time(pBuffer,&pValue->getDataRef(j,i).toTime(),strColName);
				}
				break;
			default:
				Q_ASSERT(0); //data type not supported!!

			}
		}

		if (!strRowName.isEmpty())
			*pBuffer+="</"+strRowName+">";
		else
			*pBuffer+="</row>";
	}

	//*pBuffer+="</TABLE>";
	*pBuffer+="</"+strParamName+">";
}

/*
	DbRecordSet must be defined with view schema before calling this funct
*/

void RestRpcValue::DeSerialize_DbRecordSet(const QDomNode &parameter,DbRecordSet* pValue,Status& pStatus,QString *strParamName, int nTimeZoneOffsetSeconds)
{
	/*
	QDomNodeList nodeTable=parameter.childNodes();
	qDebug()<<nodeTable.size();
	if (nodeTable.size()!=1)
	{
		pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,parameter.toElement().tagName());
		return;
	}
	*/

	//qDebug()<<parameter.nodeName();
	//qDebug()<<parameter.nodeValue();


	//QDomNodeList rows=nodeTable.at(0).childNodes();
	QDomNodeList rows=parameter.childNodes();
	int nSize=rows.size();
	if (nSize==0)
		return;
	pValue->addRow(nSize);
	int nSizeCol=pValue->getColumnCount();

	for(int j=0;j<nSize;j++)
	{
		QDomNodeList columns = rows.at(j).childNodes();

		//qDebug()<<columns.size();
		//qDebug()<<columns.at(0).toElement().tagName();
		
		//columns.size()
		if (nSizeCol!=columns.size()) //column count must match
		{
			pStatus.setError(StatusCodeSet::ERR_XML_PARAM_PARSE_FAIL,parameter.toElement().tagName());
			return;
		}

		for(int i=0;i<nSizeCol;i++)
		{
			QDomNode value=columns.at(i);
			int nColType=pValue->getColumnType(i);

			switch(nColType)
			{
			case QVariant::String:
				{
					QString strVal;
					DeSerialize_String(value,&strVal,pStatus);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,strVal);
				}
				break;
			case QVariant::Int:
				{
					QVariant val;
					DeSerialize_Integer(value,&val,pStatus);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,val);
				}
				break;
			case QVariant::ULongLong:
				{
					QVariant val;
					DeSerialize_ULongLong(value,&val,pStatus);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,val);
				}
				break;
			case QVariant::Date:
				{
					QDate val;
					DeSerialize_Date(value,&val,pStatus);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,val);
				}
				break;

			case QVariant::DateTime:
				{
					QDateTime val;
					DeSerialize_DateTime(value,&val,pStatus,NULL,nTimeZoneOffsetSeconds);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,val);
				}
				break;
			case QVariant::Double:
				{
					QVariant val;
					DeSerialize_Double(value,&val,pStatus);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,val);
				}
				break;

			case QVariant::Bool:
				{
					QVariant val;
					DeSerialize_Bool(value,&val,pStatus);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,val);
				}
				break;
			case QVariant::ByteArray:
				{
					QByteArray val;
					DeSerialize_Binary(value,&val,pStatus);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,val);
				}
				break;
			case QVariant::Time:
				{
					QTime val;
					DeSerialize_Time(value,&val,pStatus);
					if (!pStatus.IsOK()) return;
					pValue->setData(j,i,val);
				}
				break;

			default:
				Q_ASSERT(0); //data type not supported!!
			}
		}
	}


	if (strParamName)
		*strParamName=parameter.toElement().tagName();
}
