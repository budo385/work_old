#include "httpclientsockethandler.h"
#include "qhttp.h"
#include "config_trans.h"
#include <QCoreApplication>
#include "common/common/threadid.h"
#include "common/common/logger.h"

//extern Logger g_Logger;

//#include "qtsslsocket.h"
/*!
	Constructor
	\param pClientThread	- pointer to main thread / used to access shared data (buffers,etc..)
	\param pBufferSempahore - shared semaphore for serialize access to socket
	\param pThreadSync		- shared thread sync object
	\param parent			- QObject
	pStatus

*/
HTTPClientSocketHandler::HTTPClientSocketHandler(HTTPClientThread *pClientThread, QSemaphore *pBufferSempahore,ThreadSynchronizer *pThreadSync,QWaitCondition *waitCond,QObject *parent)
	: QObject(parent)
{

	m_bChunkedWriteAccepted=false;
	m_BufferSemaphore=pBufferSempahore;
	m_nState=STATE_NOT_CONNECTED;
	m_SocketSemaphore= new QSemaphore(1); //for shared resource: socket!
	m_ClientThread=pClientThread;
	m_ThreadSync=pThreadSync;
	m_waitCond=waitCond;

#ifndef _DONT_USE_SSL
	m_pSslSocket=NULL;
#endif
	m_TCPSocket=NULL;
	m_nTimerID=-1;
	m_ByteToWrite=0;
	m_ByteWritten=0;
	m_ByteWrittenToSocket=0;
}

/*!
	Destructor
*/
HTTPClientSocketHandler::~HTTPClientSocketHandler()
{
	//qDebug()<<"destroyed client socket";
	delete m_SocketSemaphore;
}




/*------------------------------------------------------------------------*/
/*								  CONNECTING							  */
/*------------------------------------------------------------------------*/

/*!
	Revoked by Main thread: tries to connect to the server based on connection settings 
	data in shared Main Thread (GetConnectionSettings())
*/
void HTTPClientSocketHandler::SocketConnect()
{

	//when this signal is received, this means that we have new conn settings:
	m_ConnSettings=m_ClientThread->GetConnectionSettings();


	//if already connected return error:
	if( m_nState!=STATE_NOT_CONNECTED){
		m_ClientThread->SetError(StatusCodeSet::ERR_HTTP_SOCKET_ALREADY_CONNECTED);
		m_ThreadSync->ThreadRelease(); //wake calling thread if he fell at sleep :)
		return;
	}
	
	//create socket:
	CreateSocket(m_ConnSettings.m_bUseSSL);
	
	//connect signals:
    // Connect the socket's signals to slots in our client implementation.
	connect(m_TCPSocket, SIGNAL(connected()), this, SLOT(OnConnectedToHost()));
    connect(m_TCPSocket, SIGNAL(readyRead()), this, SLOT(OnReadData()));
    connect(m_TCPSocket, SIGNAL(disconnected()), this, SLOT(OnConnectionClosed()));
    connect(m_TCPSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(ErrorHandler(QAbstractSocket::SocketError)));
	 
	//B.T. = limit connect time out to 10sec: to avoid long wait if server is down
	m_nTimerID=startTimer(m_ConnSettings.m_nConnectTimeOut*1000);	//try to save our asses if something goes wrong
	
	//qDebug()<<"trying to connect to host " <<m_ConnSettings.m_strServerIP<<":"<<m_ConnSettings.m_nPort;

	//set proxy if enabled
	if (m_ConnSettings.m_bUseProxy) m_TCPSocket->setProxy(m_ConnSettings.GetProxySettings());

#ifndef _DONT_USE_SSL
	if (m_ConnSettings.m_bUseSSL)
	{
		m_pSslSocket=dynamic_cast<QSslSocket*>(m_TCPSocket);
		Q_ASSERT(m_pSslSocket);
		connect(m_pSslSocket, SIGNAL(encrypted()), this, SLOT(OnSSLReady()));
		connect(m_pSslSocket, SIGNAL(sslErrors ( const QList<QSslError>&)), this, SLOT(OnSSLErrors(const QList<QSslError>&)));
		connect(m_pSslSocket, SIGNAL(encryptedBytesWritten(qint64)), this, SLOT(OnBytesWritten(qint64)));

		m_pSslSocket->connectToHostEncrypted(m_ConnSettings.m_strServerIP,m_ConnSettings.m_nPort);
		//m_pSslSocket->setProtocol(QSsl::TlsV1);
	}
	else
	{
#endif
		connect(m_TCPSocket, SIGNAL(bytesWritten (qint64)), this, SLOT(OnBytesWritten(qint64)));
		m_TCPSocket->connectToHost(m_ConnSettings.m_strServerIP,m_ConnSettings.m_nPort);
#ifndef _DONT_USE_SSL
	}
#endif

}

/*!
	Revoked by socket when socket is connected: if timeout error: error will be reported to ErrorHandler
*/
void HTTPClientSocketHandler::OnConnectedToHost()
{
	//qDebug()<<"socket connected";
	ClearSocketState(); //set state to ready!
}





/*------------------------------------------------------------------------*/
/*								  DISCONNECTING							  */
/*------------------------------------------------------------------------*/


/*!
	Revoked by Main thread: tries to disconnect from server: waits if socket is busy.
	If already disconnected returns with no error.
*/
void HTTPClientSocketHandler::SocketDisconnect()
{

	//if socket isn't connected return OK
	if( m_nState==STATE_NOT_CONNECTED||m_TCPSocket==NULL)
	{
		m_ClientThread->SetError(0);
		m_ThreadSync->ThreadRelease(); //notify calling thread 
		return;
	}
	
	//m_SocketSemaphore->acquire(1);		//waits for socket to be released (reading/writing in progress)
	m_TCPSocket->disconnectFromHost();		//send disconnect
	//m_SocketSemaphore->release(1); 		//release socket for normal operations:

}

/*!
	Revoked by socket when socket is disconnected: if timeout error. error will be reported to ErrorHandler
*/
void HTTPClientSocketHandler::OnConnectionClosed()
{

		//qDebug()<<"---------conn closed--------------";

		//release socket:
	    if (m_TCPSocket!=NULL)
		{
			m_TCPSocket->blockSignals(true);
			m_TCPSocket->deleteLater();//delete socket
			m_TCPSocket=NULL;
#ifndef _DONT_USE_SSL
			m_pSslSocket=NULL;
#endif
		}
		
		Status err;
		err.setError(StatusCodeSet::ERR_HTTP_HOST_CLOSED);
		ClearSocketState(err.getErrorCode());	//set error, if normal disconnect then ignore this error
		//qDebug()<<"Connection to host closed!";
}

/*!
	Revoked by Main thread, initate destroy sequence, disconnect if connected, close socket, kill socket thread
*/
void HTTPClientSocketHandler::SocketDestroy()
{
	
	//if(m_nState!=STATE_NOT_CONNECTED&&m_TCPSocket!=NULL) 
	//	m_TCPSocket->disconnectFromHost(); //disconnect it & destroy
	//release socket:
	if (m_TCPSocket!=NULL)
	{
		m_TCPSocket->blockSignals(true);
		m_TCPSocket->abort();
		m_TCPSocket->deleteLater();//delete socket
		m_TCPSocket=NULL;
#ifndef _DONT_USE_SSL
		m_pSslSocket=NULL;
#endif
	}
	deleteLater();		//schedule this object for safe deletion

//	else
	  //m_Condition->wakeAll(); //socket is dead, just notify calling thread that destroy op. is over!
	//wake calling thread
	//m_ThreadSync->ThreadRelease();//qDebug()<<"Socket: wake thread"; //notify calling thread
	if (m_SocketSemaphore->available()==0)m_SocketSemaphore->release(1);
	//ClearSocketState();
}




/*------------------------------------------------------------------------*/
/*								  WRITING								  */
/*------------------------------------------------------------------------*/


/*!
	Revoked by Main thread: inform socket thread that new client request is ready for sending
	data in shared Main Thread (GetRequestBuffer())
	\param bAsyncMode if false then reserve buffer semaphore to enable safe write of server response
*/

void HTTPClientSocketHandler::SocketWrite()
{
	//socket can be used by incoming server request: check if can write
	if (!m_SocketSemaphore->tryAcquire(1,30000)) //if read>30sec, return error
	{
		m_ClientThread->SetError(StatusCodeSet::ERR_HTTP_WRITE_FAILED_SOCKET_IN_USE);
		return;
	}
	//if socket isn't connected return error:
	if( m_nState==STATE_NOT_CONNECTED)
	{
		ClearSocketState(StatusCodeSet::ERR_HTTP_NOT_CONNECTED);
		return;
	}

	m_nState=STATE_WRITE;

	Status err;
	m_ClientThread->GetRequestBuffer(err,m_outgoingBuffer);
	if (m_outgoingBuffer.isEmpty())
	{
		m_ClientThread->SetError(StatusCodeSet::ERR_HTTP_REQUEST_BUFFER_INVALID);
		return;
	}
	if (!err.IsOK())
	{
		m_ClientThread->SetError(err.getErrorCode());
		return;
	}

	m_ByteWritten=0;
	m_ByteToWrite=m_outgoingBuffer.size();
	m_ByteWrittenToSocket=0;
	m_bChunkedWriteAccepted=false;
	WriteSocketPacket();
	m_nTimerID=startTimer(HTTP_CONNECTION_TIMEOUT_INTERVAL);	//for connection time-out
}





/*------------------------------------------------------------------------*/
/*								  ERROR HANDLER							  */	
/*------------------------------------------------------------------------*/

/*!
	Revoked by SOCKET, accepts any socket error. TimeOut Exception: if interval 
	between server packets is too long (only on READ)
	/param errSocket native TCPSocket error generated by socket (mapped to our codes)
*/
void HTTPClientSocketHandler::ErrorHandler(QAbstractSocket::SocketError errSocket)
{
		
	//set error:
	Status lastErr;
	HTTPUtil::MapSocketErrors(lastErr,errSocket,m_TCPSocket->errorString());

	//notify calling thread (only if not already busy)
	m_ClientThread->SocketErrorDetected(lastErr.getErrorCode(),lastErr.getErrorTextRaw());

	//clear socket:
	ClearSocketState(lastErr.getErrorCode(),false,false,lastErr.getErrorTextRaw());

}


/*!
	Clears socket state, any pending operations are aborted, locked resources are fred.
	Socket returned to readystate. Signals calling thread to wake and sets last error. 
	This is error recovering method!
	\param LastErrCode	- err code for client thread (def: no error)
	\param LastErrText  - err text for client thread (def: no error)
*/
void HTTPClientSocketHandler::ClearSocketState(int LastErrCode,bool bAfterRead,bool bAfterServerMsgReceived,QString rawErrorText)
{

		//set error to calling thread
		m_ClientThread->SetError(LastErrCode,rawErrorText);

		//inspect socket state/reset to READY or to NOT_CONNECTED:
		if(m_TCPSocket!=NULL)
			if(m_TCPSocket->state()==QAbstractSocket::ConnectedState)
				m_nState=STATE_READY;
			else
			{  //socket object is alive, but not connected, we are in error state, delete socket:
				ForceSocketDelete();
				m_nState=STATE_NOT_CONNECTED;
			}
		else
			m_nState=STATE_NOT_CONNECTED;
	
		//clears socket buffer	
		m_reqBuffer.clear();
		m_outgoingBuffer.clear();
		
		//kill any timers:
		if(m_nTimerID!=-1)killTimer(m_nTimerID);
		m_nTimerID=-1;		


		//set connected:
		m_ClientThread->SetConnected(!(m_nState==STATE_NOT_CONNECTED)); //sets true if connected


		//wake calling thread
		if (!bAfterServerMsgReceived)
		{
			m_ThreadSync->ThreadRelease();//qDebug()<<"Socket: wake thread"; //notify calling thread 
			//only if error occured or after read
			if (LastErrCode!=0 || bAfterRead)
			{	
				if(m_BufferSemaphore->available()==0)
				{
					m_BufferSemaphore->release(1);
					//qDebug()<<"B(-) thread: "<<+" socket: "<<m_TCPSocket->socketDescriptor()<<" pointer m_BufferSemaphore: "<<m_BufferSemaphore;;
				}
				m_waitCond->wakeAll();
			}
		}

		m_ByteWritten=0;
		m_ByteToWrite=0;
		m_ByteWrittenToSocket=0;
		m_bChunkedWriteAccepted=false;

		if (m_SocketSemaphore->available()==0)m_SocketSemaphore->release(1);
}




/*!
	Invoked by Write data in SYNC mode to prevent endless loop in case if server is not responding or if id does not return response as it has to!
	Note: overriden Q_OBJECT base class method invoked by its internal timer
*/
void HTTPClientSocketHandler::timerEvent(QTimerEvent *event){
	//out timer is here, then fire Time out error, save our asses!!!
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		//before killing connection, ping server: if ok then prolong timer
		//qDebug()<<"socket timeout try ping";
		if(m_ClientThread->GetServerPing()->Ping()) //ping ok
		{
			//qDebug()<<"ping ok";
			m_nTimerID=startTimer(HTTP_CONNECTION_TIMEOUT_INTERVAL);
			return;
		}
		
		//qDebug()<<"socket timeout definitive";
		ErrorHandler(QAbstractSocket::SocketTimeoutError);
	}
}




/*------------------------------------------------------------------------*/
/*								  READINIG								  */
/*------------------------------------------------------------------------*/


/*!
	Revoked by SOCKET, accepts any data from socket and reads it

	MO: If writing is in progress any data read on socket must be processed nand write aborted!!!
*/
void HTTPClientSocketHandler::OnReadData()
{
	if (m_nState==STATE_NOT_CONNECTED) 
	{
		m_ThreadSync->ThreadRelease();
		return;
	}

	if (m_nState!=STATE_READ)
	{	
		//qDebug()<<"thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" start read socket: "<<m_pSslSocket->socketDescriptor();
		m_Read_request_header=QHttpRequestHeader();
		m_Read_response_header=QHttpResponseHeader();
		m_Read_bBlockReadStillProcessingLastChunk=false;
		m_Read_nChunkReadIteration=0;
		m_Read_nTotalLength=0;
		m_Read_nHTTPType=0;

		if (!m_SocketSemaphore->tryAcquire(1,10000)) //if write>30sec, return error, should never happen
		{
			m_ClientThread->SetError(StatusCodeSet::ERR_HTTP_READ_FAILED_SOCKET_IN_USE);
			return;
		}

		//reset all:
		if(m_nTimerID!=-1)killTimer(m_nTimerID);	
		m_nTimerID=-1;				
		m_ByteWritten=0;
		m_ByteToWrite=0;
		m_ByteWrittenToSocket=0;
		m_bChunkedWriteAccepted=false;
		m_nState=STATE_READ;
		m_reqBuffer.clear();
	}

	if (m_Read_bBlockReadStillProcessingLastChunk) //read is blocked
	{
		return;
	}

	m_reqBuffer+=m_TCPSocket->readAll();
	//qDebug()<<"thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" socket: "<<m_pSslSocket->socketDescriptor()<<"reading :"<<m_reqBuffer.size()<<"-:"<<m_reqBuffer.left(30);
	m_Read_nChunkReadIteration++;


	if (m_Read_nChunkReadIteration==1)
	{
		int nHdrEnd=0;
		bool bCompleteHeader=HTTPUtil::CheckHTTPHeader(m_reqBuffer,m_Read_nHTTPType,m_Read_nTotalLength,nHdrEnd); //try to extract header

		if(!bCompleteHeader)
		{
			if (m_reqBuffer.size()>HTTP_HEADER_MAX_SIZE) //security option to prevent client to send garbage
			{
				ClearSocketState(StatusCodeSet::ERR_HTTP_INVALID_HTTP_MESSAGE);
				return;
			}
			return; 
		}

		if(m_Read_nHTTPType==HTTPUtil::HTTP_HEADER_INVALID)
		{
			ClearSocketState(StatusCodeSet::ERR_HTTP_INVALID_HTTP_MESSAGE);
			return;
		}

		if (m_Read_nHTTPType==HTTPUtil::HTTP_HEADER_RESPONSE)
			m_Read_response_header = QHttpResponseHeader (QString(m_reqBuffer.left(nHdrEnd)));
		else
			m_Read_request_header = QHttpRequestHeader (m_reqBuffer.left(nHdrEnd));

		m_reqBuffer=m_reqBuffer.mid(nHdrEnd);
		if (m_Read_nTotalLength==-1)
		{
			Status err;
			m_ClientThread->InitChunkedHttpStream(err,m_Read_response_header);
			if (!err.IsOK())
			{				
				m_Read_bBlockReadStillProcessingLastChunk=true;
				ClearSocketState(err.getErrorCode());
				return;
			}
		}
	}

	bool bChunkProcessedOK=true;
	bool bEnd=HTTPUtil::CheckHTTPEndOfMessage(m_reqBuffer,m_Read_nTotalLength);
	
	if(m_ClientThread->SetDataTransferParametersAndCheckIsCancel(m_reqBuffer.size(),m_Read_nTotalLength,true))
	{
		m_Read_bBlockReadStillProcessingLastChunk=true;
		OnAbort();
		return;
	}

	if (m_Read_nTotalLength==-1) //chunked read: process chunk at once: bEnd mark last chunk
	{
		//qDebug()<<"Chunked END of http detected thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" socket: "<<m_pSslSocket->socketDescriptor()<<"reading :"<<m_reqBuffer.size()<<"-:"<<m_reqBuffer.left(30);
		//qDebug()<<m_Read_response_header.toString();
		//qDebug()<<m_reqBuffer;

		Q_ASSERT(m_Read_nHTTPType==HTTPUtil::HTTP_HEADER_RESPONSE); //request to the client can not be chunked
		QByteArray chunk;
		bool bExtracted = HTTPUtil::ExtractHTTPChunk(m_reqBuffer,chunk);
		if (bExtracted)
		{
			Status err;
			//qDebug()<<"thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" write chunk no: "<<m_Read_nChunkReadIteration <<" socket: "<<m_pSslSocket->socketDescriptor();
			m_ClientThread->WriteResponseBufferChunked(err,m_Read_response_header,chunk,bEnd);
			if (!err.IsOK())
			{
				m_Read_bBlockReadStillProcessingLastChunk=true;
				ClearSocketState(err.getErrorCode());
				return;
			}
		}
	}
	
	if (!bEnd) //not end of message, exit expect another
		return;

	m_Read_bBlockReadStillProcessingLastChunk=true;
	
	bool bSrvMsgReceived=false;
	if (m_Read_nHTTPType==HTTPUtil::HTTP_HEADER_RESPONSE)
	{	
		if (m_Read_nTotalLength!=-1) //if not chunked then normal op: send whole buffer as one
		{
			//qDebug()<<"thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" write whole body "<<m_Read_nTotalLength<<" socket: "<<m_pSslSocket->socketDescriptor();
			Status err;
			m_ClientThread->WriteResponseBuffer(err,m_Read_response_header,m_reqBuffer);
			if (!err.IsOK())
			{
				ClearSocketState(err.getErrorCode());
				return;
			}
		}
	}
	else
	{  
		//request message received, store message to queue and signal main thread that new message is in queue
		int newServerMsgID=m_ClientThread->WriteServerMsgQueue(m_Read_request_header,m_reqBuffer);
		emit ServerMessageReceived(newServerMsgID);
		bSrvMsgReceived=true;
	}

	//clear buffer-release socket
	ClearSocketState(0,true,bSrvMsgReceived);
}





/*!
	WARNING: in error state, when socket hangs, we need to delete it, immidiatelly, causing no backfire signals
	WARNING: this is perfectly safe: socket signals are blocked, and socket is deleted in safe way.
*/
void HTTPClientSocketHandler::ForceSocketDelete()
{
	if(m_TCPSocket==NULL) return;
	m_TCPSocket->blockSignals(true); 	//DEACTIVATE SIGNALS (can be triggered in destructor of socket)

	//force socket deletion:
	m_TCPSocket->abort();
	m_TCPSocket->deleteLater();
	
	//init vars to NULL state
	m_TCPSocket=NULL;
#ifndef _DONT_USE_SSL
	m_pSslSocket=NULL;
#endif
}



/*!
	Override & create own socket from QTcpSocket class
*/
void HTTPClientSocketHandler::CreateSocket(bool bSSLSocket)
{
	//socket lives? Possible error: try to recover to delete old socket & create new/new connection
	if(m_TCPSocket!=NULL)
		ForceSocketDelete();

	//create socket:
#ifndef _DONT_USE_SSL
	if(bSSLSocket)
		//m_TCPSocket=new QtSslSocket(QtSslSocket::Client, NULL);
		m_TCPSocket=new QSslSocket(NULL);
	else
#endif
		m_TCPSocket=new QTcpSocket (this);
}

void HTTPClientSocketHandler::OnAbort()
{
	if (!m_TCPSocket)
		return;
	m_TCPSocket->blockSignals(true);
	m_TCPSocket->abort();	//disconnect
	m_TCPSocket->blockSignals(false);
/*
	//m_TCPSocket->connectToHost(m_ConnSettings.m_strServerIP,m_ConnSettings.m_nPort); //connect
#ifndef _DONT_USE_SSL
	if (m_ConnSettings.m_bUseSSL)
		m_pSslSocket->connectToHostEncrypted(m_ConnSettings.m_strServerIP,m_ConnSettings.m_nPort);
	else
#endif
		m_TCPSocket->connectToHost(m_ConnSettings.m_strServerIP,m_ConnSettings.m_nPort);
*/
	ClearSocketState(0,true);
}


void HTTPClientSocketHandler::OnSSLReady()
{
#ifndef _DONT_USE_SSL
	if(m_pSslSocket->isEncrypted())
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Encrypted SSL connection successfully started");
#endif
}

void HTTPClientSocketHandler::OnSSLErrors(const QList<QSslError>& lst)
{
#ifndef _DONT_USE_SSL
	int nSize=lst.count();
	for(int i=0;i<nSize;i++)
	{
		qDebug()<< "SSL Error no:"+QString::number(lst.at(i).error());
		qDebug()<< "SSL Error:"+lst.at(i).errorString();
		//if (lst.at(i).error()==QSslError::HostNameMismatch || lst.at(i).error()==QSslError::CertificateUntrusted  || lst.at(i).error()==QSslError::SelfSignedCertificate || lst.at(i).error()==QSslError::CertificateExpired)
		//{
			//m_pSslSocket->ignoreSslErrors();
		//	break;
		//}
		//ApplicationLogger::setApplicationLogger(&g_Logger);
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"SSL Error:"+lst.at(i).errorString());
	}

	m_pSslSocket->ignoreSslErrors();

#endif

}


void HTTPClientSocketHandler::OnBytesWritten(qint64 bytes)
{
	if (m_nState!=STATE_WRITE)	return;
	m_ByteWritten+=bytes;
	WriteSocketPacket();

	if (m_ByteWritten<m_ByteToWrite)
	{
		//reset timer if ON
		if(m_nTimerID!=-1)
		{
			killTimer(m_nTimerID);
			m_nTimerID=-1;	
			m_nTimerID=startTimer(HTTP_CONNECTION_TIMEOUT_INTERVAL);	//try to save our asses if something goes wrong
		}
		if(m_ClientThread->SetDataTransferParametersAndCheckIsCancel(m_ByteWritten,m_ByteToWrite,false))
		{
			OnAbort();
			return;
		}
	}
	else
	{
			if(m_ClientThread->SetDataTransferParametersAndCheckIsCancel(m_ByteToWrite,m_ByteToWrite,false))
			{
				OnAbort();
				return;
			}

			m_outgoingBuffer.clear(); 
			if (m_ClientThread->IsChunkedTransfer())
			{
				Status err;

				//---------------------chunked upload must be confirmed--------------------------!!!
				if (!m_bChunkedWriteAccepted)
				{
					err.setError(StatusCodeSet::ERR_HTTP_OPERATION_TIMEOUT); //as default
					m_TCPSocket->blockSignals(true);
					bool bServerAcceptedRequest=m_TCPSocket->waitForReadyRead(20000); //20seconds wait
					if (bServerAcceptedRequest)
					{
						bServerAcceptedRequest=false;
						QByteArray buffer=m_TCPSocket->readAll();
						if (!buffer.isEmpty())
						{
							int nHdrEnd=0;
							int nHttpType;
							qint64 nLength;
							bool bCompleteHeader=HTTPUtil::CheckHTTPHeader(buffer,nHttpType,nLength,nHdrEnd); //try to extract header
							if(bCompleteHeader && nHttpType==HTTPUtil::HTTP_HEADER_RESPONSE)
							{
								QHttpResponseHeader resp(QString(buffer.left(nHdrEnd)));
								buffer=buffer.mid(nHdrEnd);
								if (resp.statusCode()==100)
								{
									err.setError(0);
									bServerAcceptedRequest=true;
								}
								else
								{
									if (!buffer.isEmpty())
										err.setError(StatusCodeSet::ERR_HTTP_SERVER_REJECTED_REQUEST,QString(buffer));
									else
										err.setError(StatusCodeSet::ERR_HTTP_SERVER_REJECTED_REQUEST,tr("Unknown reason"));
								}
							}
						}
					}
					m_TCPSocket->blockSignals(false);

					if (!err.IsOK())
					{
						ClearSocketState(err.getErrorCode(),false,false,err.getErrorTextRaw());
						return;
					}
					m_bChunkedWriteAccepted=true;
				}
				//---------------------chunked upload must be confirmed--------------------------!!!


				m_ClientThread->GetRequestBuffer(err,m_outgoingBuffer);
				if (!err.IsOK())
				{
					ClearSocketState(err.getErrorCode());
					return;
				}
				if (!m_outgoingBuffer.isEmpty())
				{
					m_ByteToWrite=m_outgoingBuffer.size();
					m_ByteWritten=0;
					m_ByteWrittenToSocket=0;
					WriteSocketPacket();
					return;
				}
			}

			m_nState=STATE_READY;
			m_SocketSemaphore->release(1);
	}
}

void HTTPClientSocketHandler::WriteSocketPacket()
{

	if (m_ByteWrittenToSocket<m_ByteToWrite && m_ByteWritten>=m_ByteWrittenToSocket)
	{
		qint64 bytesToWrite=TCP_CHUNK_SIZE;
		if (m_ByteWritten+TCP_CHUNK_SIZE>m_ByteToWrite)
			bytesToWrite=m_ByteToWrite-m_ByteWrittenToSocket;
		const char *startOfData = m_outgoingBuffer.constData() + m_ByteWrittenToSocket;
		m_ByteWrittenToSocket=m_ByteWrittenToSocket+m_TCPSocket->write(startOfData,bytesToWrite);
	}

}