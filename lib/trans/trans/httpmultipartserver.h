#ifndef HTTPMULTIPARTSERVER_H
#define HTTPMULTIPARTSERVER_H


#include "trans/trans/httphandler.h"
#include <QHash>

class HTTPMultipartHandler
{
public:
	virtual bool HandleMessage(Status &err, QList<HTTPRequest> lstRequests, QByteArray *pBufResponse, QString strHTTPPath)=0;	
};



class HTTPMultipartServer : public HTTPHandler
{
public:
	HTTPMultipartServer(HTTPMultipartHandler *pHandler);
	void HandleRequest(Status &err, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);

protected:
	void			SetResponseHeader(HTTPRequest &request,HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText);
	void			ResponseHandler(Status &err,HTTPRequest &request, HTTPResponse &response);

	HTTPMultipartHandler *m_pHandler;

};

#endif // HTTPMULTIPARTSERVER_H
