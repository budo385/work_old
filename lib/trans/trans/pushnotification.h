#ifndef PUSHNOTIFICATION_H
#define PUSHNOTIFICATION_H

#include <QObject>
#include <QTimerEvent>
#include <QStringList>
#include "common/common/threadsync.h"

class PushNotification : public QObject
{
	Q_OBJECT

public:
	enum OS_TYPE
	{
		MOB_OS_MAC_OS=0,
		MOB_OS_ANDROID=1,
		MOB_OS_WIN=2,
		MOB_OS_GOOGLE_CLOUD=3

	};

	PushNotification(ThreadSynchronizer *sync,int nTimeOut=10000);
	~PushNotification();

	//implement these two:
	virtual void SendPushMessage()=0;
	virtual void Destroy()=0;
	
	void	SetMessageData(QString strPushToken1, QString strPushToken2, QString strMessage, int nMessageID, QStringList lstMessageCustomData, QString strPushSoundName="default", int nBadgeNumber=0);
	int		GetStatusCode(){return m_nStatusCode;};
	QString	GetStatusText(){return m_strStatusText;};

protected:
	virtual void OnTimeOut()=0; //implement this one
	void ResetTimeOut();

	void timerEvent(QTimerEvent *event);

	//message data:
	QString m_strPushToken1;
	QString m_strPushToken2;
	QString m_strMessage;
	int		m_nMessageID;
	QStringList m_lstMessageCustomData;
	QString m_strPushSoundName;
	int m_nBadgeNumber;

	//status data:
	int		m_nStatusCode;
	QString m_strStatusText;
	
	//thread sync
	int					m_nTimeOut;
	int					m_nTimerID;
	ThreadSynchronizer	*m_ThreadSync;

};

#endif // PUSHNOTIFICATION_H
