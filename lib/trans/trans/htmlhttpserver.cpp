#include "htmlhttpserver.h"
#include <QDir>
#include <QCoreApplication>

#include "common/common/config_version_sc.h"
#include "common/common/threadid.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "common/common/datahelper.h"
#include "trans/trans/httputil.h"
#include "common/common/sha256hash.h"
#include "common/common/logger.h"
#include "common/common/encryptedinifile.h"

#define SUBDIR_SERVICE_DOCS "/webservices/html"

//mini class to keep track of html documents
HtmlDocument::HtmlDocument(QByteArray content,QDateTime date)
{
	Sha256Hash Hasher;
	byteContent=content;
	strLastModified=QDateTime::currentDateTime().toString(Qt::ISODate);
	QByteArray hashByteArray=QString::number(content.size()).toLatin1()+":"+strLastModified.toLatin1();
	strETag=QString(Hasher.GetHash(hashByteArray).toBase64()); //hash = size+":"+date;
}


HtmlHTTPServer::HtmlHTTPServer(QString strWWWRootDir,const QHash<QString,HtmlDocument> *lstWebPages)
{
	Status err;
	if (strWWWRootDir.isEmpty())
	{
#ifdef _DEBUG //for all our servers with webservices..put 'em here:
		QDir dirMain(QCoreApplication::applicationDirPath()+"/..");
		if (dirMain.exists("appserver_mwreplyserver"))
		{
			m_strWWWRootDir=QCoreApplication::applicationDirPath()+"/../appserver_mwreplyserver"+SUBDIR_SERVICE_DOCS;
		}
		else if (dirMain.exists("appserver_john"))
		{
			m_strWWWRootDir=QCoreApplication::applicationDirPath()+"/../appserver_john"+SUBDIR_SERVICE_DOCS;
		}
		else if (dirMain.exists("appserver"))
		{
			m_strWWWRootDir=QCoreApplication::applicationDirPath()+"/../appserver"+SUBDIR_SERVICE_DOCS;
		}
		else
			m_strWWWRootDir=QCoreApplication::applicationDirPath()+SUBDIR_SERVICE_DOCS;

#else
		m_strWWWRootDir=QCoreApplication::applicationDirPath()+SUBDIR_SERVICE_DOCS;
#endif
	}
	else
		m_strWWWRootDir=strWWWRootDir+SUBDIR_SERVICE_DOCS;


	m_strWWWRootDir=QDir::cleanPath(m_strWWWRootDir);

	if (lstWebPages)
	{
		m_hshDocs = *lstWebPages;
	}
	else
	{
		LoadDocuments(err,m_strWWWRootDir);
		if (!err.IsOK())
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,0,"Error while initializing www service:"+err.getErrorText());
	}

	//set domain:
	SetDomain(DOMAIN_HTTP_HTML);

	m_pSemLoadDocs = new QSemaphore(1);
}


void HtmlHTTPServer::HandleRequest(Status &err, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{
	err.setError(0);

	//qDebug()<<"-----------------HTTP request----------------";
	//qDebug()<<request.toString();
	//qDebug()<<request.GetBody()->constData();
	//qDebug()<<"---------------------------------------------";

	//only allow GET
	
	if (request.method().toUpper()!="GET")
	{
		//qDebug()<<"Wrong method: "<<request.method().toUpper();
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,405,"Method Not Allowed");
		return;
	}
	

	//BT: to remove query string from path:
	QUrl url(request.path().toLower());
	QString strPath=url.path().toLower();


	bool bRedirect=false;

	// soap: enable this
	if (strPath.contains("/api"))
	{
		QString strSOAPAction=request.value("SOAPAction");
		QString strFile;
		if (strSOAPAction.isEmpty())
			strFile = strPath;
		else
			strFile = strSOAPAction;

		QFileInfo info(strFile);
		strFile = info.fileName().replace("\"","");

		
		
		QFile file_x(QCoreApplication::applicationDirPath()+"/"+strFile);
		if(file_x.open(QIODevice::ReadOnly))
		{
			QByteArray byteData = file_x.readAll();

			response.SetBody(byteData);
			SetResponseHeader(request,response,200,"OK");

			response.setValue("Content-Type","text/xml");
			err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			return;
		}
	}


	strPath=ResolvePath(strPath,request.value("Referer"),bRedirect);
	if (strPath.isEmpty())
	{
		err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		SetResponseHeader(request,response,404,"File not found");
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"WWW 404 detected, request was: " + url.path().toLower());
		return;
	}

	if (bRedirect)
	{
		SetResponseHeader(request,response,302,"");
		response.setValue("Location",CleanPath("/"+strPath));
		err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		return;
	}

	//BT: temporary for development: should be banned from release
	//if (strPath.indexOf("index.html")>=0)
	//{
	//	m_pSemLoadDocs->acquire();
		//LoadDocuments(err,m_strWWWRootDir);//BT: quick patch: reload all Documents:
	//	m_pSemLoadDocs->release();
	//}
	

	//304: testing
	QString strTestETag=request.value("If-None-Match");
	if (!strTestETag.isEmpty())
	{
		if (strTestETag==m_hshDocs.value(strPath).strETag)
		{
			//qDebug()<<"304: "<<strPath;
			SetResponseHeader(request,response,304,"");
			response.setValue("Last-Modified",m_hshDocs.value(strPath).strLastModified);
		
			err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"WWW processed: ("+ctx.m_strPeerIP+":"+QVariant(ctx.m_nSocketID).toString()+"): "+request.method().toUpper()+" "+strPath);
			return;
		}
	}
	QString strTestLastModify=request.value("If-Modified-Since");
	if (!strTestLastModify.isEmpty())
	{
		if (strTestLastModify==m_hshDocs.value(strPath).strLastModified)
		{
			//qDebug()<<"304: "<<strPath;
			SetResponseHeader(request,response,304,"");
			response.setValue("Last-Modified",m_hshDocs.value(strPath).strLastModified);
			err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"WWW processed: ("+ctx.m_strPeerIP+":"+QVariant(ctx.m_nSocketID).toString()+"): "+request.method().toUpper()+" "+strPath);
			return;
		}
	}

	response.SetBody(m_hshDocs.value(strPath).byteContent);
	SetResponseHeader(request,response,200,"OK");
	
	int nUseDeflate=-1; //0 use, -1 do not use;
	if(request.value("Accept-Encoding")=="deflate") //if request msg is compressed, use it back
		nUseDeflate=0;

	HTTPUtil::SetHeaderDataBasedOnFile(&response,m_strWWWRootDir+"/"+strPath,false,nUseDeflate); //deflate if above 2Kb
	response.setValue("Last-Modified",m_hshDocs.value(strPath).strLastModified);
	err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"WWW processed: ("+ctx.m_strPeerIP+":"+QVariant(ctx.m_nSocketID).toString()+"): "+request.method().toUpper()+" "+strPath);
	return;

}



void HtmlHTTPServer::SetResponseHeader(HTTPRequest &request,HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(nHTTPStatusCode,strHTTPCodeText);
	response.setValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setValue("Content-Type","text/plain");

	if (!strHTTPCodeText.isEmpty() && strHTTPCodeText!="OK")
	{
		response.GetBody()->append(QString("SOKRATES WWW SERVER ERROR: ")+strHTTPCodeText);
	}
}
//per directory: store relative path: doc or dir/doc.gg -> set type based on file type..etc...
void HtmlHTTPServer::LoadDocuments(Status &err,QString strFilePath)
{
	err.setError(0);
	m_hshDocs.clear();

	//read all files resource directory:
	QDir dir(strFilePath);
	if (!dir.exists())
	{
		err.setError(1,QObject::tr("WWW Document Directory:")+strFilePath+QObject::tr(" does not exist!"));
		return ;
	}

	DbRecordSet lstPaths,lstApps;
	DataHelper::GetFilesFromFolder(strFilePath,lstPaths,lstApps,true,true,strFilePath);

	//_DUMP(lstPaths);

	int nSize2=lstPaths.getRowCount();
	for(int k=0;k<nSize2;++k)
	{
		QFile xmlFile(strFilePath+"/"+lstPaths.getDataRef(k,"PATH").toString());
		QDateTime datLastModified=QFileInfo(strFilePath+"/"+lstPaths.getDataRef(k,"PATH").toString()).lastModified();
		if(!xmlFile.open(QIODevice::ReadOnly))
		{
			err.setError(1,QObject::tr("File: ")+lstPaths.getDataRef(k,"PATH").toString()+QObject::tr(" can not be read!"));
			return;
		}
		m_hshDocs["/"+lstPaths.getDataRef(k,"PATH").toString().toLower()]=HtmlDocument(xmlFile.readAll(),datLastModified);
		//qDebug()<<lstPaths.getDataRef(k,"PATH").toString().toLower()<<" - "<<m_hshDocs.value(lstPaths.getDataRef(k,"PATH").toString().toLower()).strETag;
		xmlFile.close();
	}
}



//try abs path, add index.htm, if not try referrer, if not 404
QString HtmlHTTPServer::ResolvePath(QString strPath, QString strReferrer, bool &bRedirect)
{
	bRedirect=false;
	if (m_hshDocs.contains(strPath))
		return strPath;
	if (m_hshDocs.contains(CleanPath(strPath+"/index.html")))
		{bRedirect=true;	return CleanPath(strPath+"/index.html");}
	if (m_hshDocs.contains(CleanPath(strPath+"/index.htm")))
		{bRedirect=true;	return CleanPath(strPath+"/index.htm");}

	//e.g.: http://wwwxx.com:9999/ or http://wwwxx.com:9999/webconnect or or http://wwwxx.com:9999/webconnect/index.html
	if (!strReferrer.isEmpty())
	{
		QStringList tokens=strReferrer.split("/",QString::SkipEmptyParts);
		if (tokens.size()<=2) return ""; //referrer is absolute

		tokens.removeFirst();
		tokens.removeFirst();

		QString strLastToken=tokens.at(tokens.size()-1);
		if (strLastToken.contains("."))
		{
			tokens.removeLast();
		}

		strPath = "/"+tokens.join("/")+"/"+strPath;
		strPath = CleanPath(strPath);
	}
	else
		return "";

	if (m_hshDocs.contains(strPath))
		return strPath;
	if (m_hshDocs.contains(CleanPath(strPath+"/index.html")))
	{bRedirect=true;	return CleanPath(strPath+"/index.html");}
	if (m_hshDocs.contains(CleanPath(strPath+"/index.htm")))
	{bRedirect=true;	return CleanPath(strPath+"/index.htm");}

	return "";
}


QString	HtmlHTTPServer::CleanPath(QString strPath)
{
	strPath=QDir::cleanPath(strPath);
	if (strPath.left(2)=="//")
		strPath=strPath.mid(1);
	if (strPath.right(2)=="//")
		strPath.chop(1);

	return strPath;
}