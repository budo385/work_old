/*!
	\class  HTTPResponse
	\brief  Provides complete information on a single HTTP response packet

	This class is a wrapper around QHttpResponseHeader, adding storage of HTTP body
	(not only header), and some other convenient access methods.
*/
#ifndef HTTPRESPONSE_H
#define HTTPRESPONSE_H

#include "qhttp.h"



class HTTPResponse : public QHttpResponseHeader
{
public:
	HTTPResponse(){};
    HTTPResponse(const QHttpResponseHeader &header, const QByteArray& body);
	~HTTPResponse();

	QByteArray *	GetBody(){	return &m_body; }
	void			SetBody(const QByteArray &body){	m_body = body; }
	void			GetData(QByteArray& binaryPackage);
	void			SetData(const QHttpResponseHeader &header, const QByteArray& body);
		
protected:
	QByteArray m_body;
};

#endif
