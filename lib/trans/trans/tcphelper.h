#ifndef TCPHELPER_H
#define TCPHELPER_H

#include <QObject>
#include <QString>

class TcpHelper : public QObject
{
public:
	TcpHelper(QObject *parent);
	~TcpHelper();
	
	static bool IsHostAddress(QString strIP);
	//static void ResolveWebAddressNetworkInterface(QString strIP, int nPort, bool bUseSSL);
	static QString ResolveLocalHostURLService(QString strIP, int nPort);

	//static QString s_sHostWebAddress;
};

#endif // TCPHELPER_H
