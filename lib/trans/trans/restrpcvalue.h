#ifndef RESTRPCVALUE_H
#define RESTRPCVALUE_H

#include <QDomNode>
#include <QString>
#include <QByteArray>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"

class RestRpcValue
{
public:

	//every serialize must have variant support, every de-serialize of data type which does not have isNull funct, must have variant support

	static void Serialize_String(QByteArray *pBuffer,const QString* pValue, const QString strParamName);
	static void Serialize_String(QByteArray *pBuffer,const QVariant* pValue, const QString strParamName);
	static void DeSerialize_String(const QDomNode &parameter,QString* pValue,Status& pStatus,QString *strParamName=NULL);

	static void Serialize_Bool(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName);
	static void DeSerialize_Bool(const QDomNode &parameter,QVariant* pValue,Status& pStatus,QString *strParamName=NULL);

	static void Serialize_Integer(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName);
	static void DeSerialize_Integer(const QDomNode &parameter,QVariant* pValue,Status& pStatus,QString *strParamName=NULL);

	static void Serialize_ULongLong(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName);
	static void DeSerialize_ULongLong(const QDomNode &parameter,QVariant* pValue,Status& pStatus,QString *strParamName=NULL);

	static void Serialize_Double(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName);
	static void DeSerialize_Double(const QDomNode &parameter,QVariant* pValue,Status& pStatus,QString *strParamName=NULL);

	static void Serialize_DateTime(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName, int nTimeZoneOffsetSeconds=0);
	static void Serialize_DateTime(QByteArray *pBuffer,const QDateTime *pValue, const QString strParamName, int nTimeZoneOffsetSeconds=0);
	static void DeSerialize_DateTime(const QDomNode &parameter,QDateTime* pValue,Status& pStatus,QString *strParamName=NULL, int nTimeZoneOffsetSeconds=0);

	static void Serialize_Date(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName);
	static void Serialize_Date(QByteArray *pBuffer,const QDate *pValue, const QString strParamName);
	static void DeSerialize_Date(const QDomNode &parameter,QDate* pValue,Status& pStatus,QString *strParamName=NULL);

	static void Serialize_Time(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName);
	static void Serialize_Time(QByteArray *pBuffer,const QTime *pValue, const QString strParamName);
	static void DeSerialize_Time(const QDomNode &parameter,QTime* pValue,Status& pStatus,QString *strParamName=NULL);

	static void Serialize_Binary(QByteArray *pBuffer,const QVariant *pValue, const QString strParamName);
	static void Serialize_Binary(QByteArray *pBuffer,const QByteArray *pValue, const QString strParamName);
	static void DeSerialize_Binary(const QDomNode &parameter,QByteArray* pValue,Status& pStatus,QString *strParamName=NULL);

	static void Serialize_DbRecordSet(QByteArray *pBuffer,const DbRecordSet *pValue, const QString strParamName, const QString strRowName="", const QString strRowURLAttr="",QString strRowURLAttr_IDField="",int nTimeZoneOffsetSeconds=0);
	static void DeSerialize_DbRecordSet(const QDomNode &parameter,DbRecordSet* pValue,Status& pStatus,QString *strParamName=NULL,int nTimeZoneOffsetSeconds=0);

	/* error */
	static void Serialize_Error(QByteArray *pBuffer,const int &nErrorCode, const QString &strErrorText);
	static void DeSerialize_Error(const QDomNode &parameter,Status& pStatus);

private:
	
};
/* TEST PARSER


#include "trans/trans/restrpcvalue.h"
#include <QDomDocument>

QByteArray test="<PARAMETERS>";
QString s1="Strikan Maleni";
RestRpcValue::Serialize_String(&test,&s1,"P1");
QVariant s2=QVariant(true);
RestRpcValue::Serialize_Bool(&test,&s2,"P2");
QVariant s3=QVariant(14.45);
RestRpcValue::Serialize_Double(&test,&s3,"P3");
QDateTime s4=QDateTime::currentDateTime();
RestRpcValue::Serialize_DateTime(&test,&s4,"P4");
QDate s5=QDate::currentDate();
RestRpcValue::Serialize_Date(&test,&s5,"P5");
QTime s6=QTime::currentTime();
RestRpcValue::Serialize_Time(&test,&s6,"P6");
QByteArray s7="ggggggggggggggg:;_>YX>X>Xyx<!\"#!#!#";
RestRpcValue::Serialize_Binary(&test,&s7,"P6");

test +="</PARAMETERS>";

QDomDocument doc;
doc.setContent(test);
QDomNodeList ListOfElements	= doc.elementsByTagName("PARAMETERS");
QDomNodeList allNodese=ListOfElements.at(0).childNodes();

qDebug()<<ListOfElements.size();
qDebug()<<allNodese.size();
QDomNode node=allNodese.at(1);

qDebug()<<node.toElement().text();


QString strTest;
Status err;
QVariant val;

RestRpcValue::DeSerialize_Bool(node,&val,err);
qDebug()<<val.toBool();

node=allNodese.at(2);
RestRpcValue::DeSerialize_Double(node,&val,err);
qDebug()<<val.toDouble();

node=allNodese.at(3);
QDateTime t1;
RestRpcValue::DeSerialize_DateTime(node,&t1,err);
qDebug()<<t1.toString();

node=allNodese.at(4);
QDate t2;
RestRpcValue::DeSerialize_Date(node,&t2,err);
qDebug()<<t2.toString();

node=allNodese.at(5);
QTime t3;
RestRpcValue::DeSerialize_Time(node,&t3,err);
qDebug()<<t3.toString();

node=allNodese.at(6);
QByteArray t4;
RestRpcValue::DeSerialize_Binary(node,&t4,err);
qDebug()<<t4;


//-----------------------recordset test----------------------------//

MainEntitySelectionController sel;
sel.Initialize(ENTITY_BUS_PERSON);
sel.ReloadData();

DbRecordSet  *data=sel.GetDataSource();

data->Dump();

QByteArray test1="<PARAMETERS xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
RestRpcValue::Serialize_DbRecordSet(&test1,data,"LIST_OF_PERSONS","PERSON","http://persons","BPER_ID");
test1 +="</PARAMETERS>";
qDebug()<<test1;

DbRecordSet data1;
data1.copyDefinition(*data);
QDomDocument doc_1;
doc_1.setContent(test1);
QDomNodeList ListOfElements_1	= doc_1.elementsByTagName("PARAMETERS");
QDomNodeList allNodese_1=ListOfElements_1.at(0).childNodes();
qDebug()<<ListOfElements_1.size();
qDebug()<<allNodese_1.size();
QDomNode node_1=allNodese_1.at(0);

QString parm1;
RestRpcValue::DeSerialize_DbRecordSet(node_1,&data1,err,&parm1);

data1.Dump();


//-----------------------recordset test----------------------------//

*/

#endif // RESTRPCVALUE_H
