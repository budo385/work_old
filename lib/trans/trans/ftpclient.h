#ifndef FTPCLIENT_H
#define FTPCLIENT_H

#include <QThread>
#include "qurlinfo.h"
#include <QMutex>
#include <QWaitCondition>
#include "ftpconnectionsettings.h"
#include "common/common/status.h"
#include "common/common/threadsync.h"

class FTPClient : public QThread
{
	Q_OBJECT

	friend class FTPClientSocketHandler;

public:
	FTPClient();
	~FTPClient();

	void run();

	//--------------------------------------
	//Main interface
	//--------------------------------------
	void Connect(Status& pStatus,FTPConnectionSettings& pConnection);
	void Disconnect(Status& pStatus);

	void SetConnectionSettings(FTPConnectionSettings &pConnection);
	FTPConnectionSettings GetConnectionSettings(){return m_ConnSettings;};

	void Dir_Change(Status& pStatus,QString strDir);
	void Dir_Create(Status& pStatus,QString strDir);
	void Dir_Remove(Status& pStatus,QString strDir);
	void Dir_List(Status& pStatus,QList<QUrlInfo> &lstDirContent);

	void File_Put(Status& pStatus,QString strFileFullPath);
	void File_Put(Status& pStatus,QByteArray &fileContent,QString strFileName);
	void File_Get(Status& pStatus,QString strFileName,QString strDestinationFileName);
	void File_Get(Status& pStatus,QByteArray &fileContent,QString strFileName);

	void File_Remove(Status& pStatus,QString strFileName);
	void File_Rename(Status& pStatus,QString strFileName,QString strNewFileName);

	void SendRawCommand(Status& pStatus, QString strCommand);


signals: 

	void SignalFTPConnect();
	void SignalFTPDisconnect();
	void SignalFTPDestroy();

	void SignalFTPDir_Change(QString strDir);
	void SignalFTPDir_Create(QString strDir);
	void SignalFTPDir_Remove(QString strDir);
	void SignalFTPDir_List();

	void SignalFTPFile_Put(QString strFileName);
	void SignalFTPFile_Get(QString strFileName);
	void SignalFTPFile_Remove(QString strFileName);
	void SignalFTPFile_Rename(QString strFileName,QString strNewFileName);

	void SignalFTPSendRawCommand(QString strCommand);

	void SignalFTPOperationInProgress(qint64 nDoneBytes, qint64 nTotalBytes);
	void SocketOperationInProgress(int nWaitTicks);

protected:

	//--------------------------------------
	//Access to shared data between threads
	//--------------------------------------

	//Status 
	void SetError(int nErrorCode, QString strErrorTxt="");
	Status& GetError(){return m_LastError;};

	//Sync buffers: response/request
	void WriteBuffer(QByteArray& pBuffer);						//socket thread only writes response: warning: because it uses external pointer this access is not 100% multithread safe
	void GetBuffer(QByteArray& pBuffer);
	void InvalidateBuffers();									//sets request/response pointers to NULL
	void SetDataTransferParameters(qint64 done,qint64 total);
	void GetDataTransferParameters(qint64 &done,qint64 &total);
	void SetDirList(QList<QUrlInfo> &lstDirContent);
	void GetDirList(QList<QUrlInfo> &lstDirContent);


	//sets socket status:
	void SetConnected(bool bConnected);
	bool IsConnected();




	/* ------------------private members------------------ */

	qint64 m_Done,m_Total;

	//mutex locks	
	QMutex m_Mutex;					///< mutex
	QReadWriteLock m_RWLock;		///< RWlock for accessing shared data
	ThreadSynchronizer m_ThreadSync;///< for syncing between mastr & slave thread
	QWaitCondition m_waitCond;		///< special one for refreshing GUI

	//bool m_bSocketIsInErrorState;		///<if true, no acces is allowed (only socket disconnect)

	//shared data members
	QSemaphore* m_BufferSemaphore;	///< resource to protect: response buffer when receiving message

	//Shared data betwenn main and socket thread 
	QByteArray *m_FileBuffer;
	QList<QUrlInfo> m_lstDirContent;
	FTPConnectionSettings m_ConnSettings;	///<holds connection settings for socket
	Status m_LastError;						///< warning: not thread safe: serialize access
	bool m_bConnected;	

};

#endif // FTPCLIENT_H
