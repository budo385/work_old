#ifndef HTTPSERVERSOCKETHANDLER_H
#define HTTPSERVERSOCKETHANDLER_H 

#include <QtNetwork>
#include <QThread>
#include "common/common/status.h"
#include "httphandler.h"
#include "httpcontext.h"
#include "httpclientconnectionsettings.h"

/*!
    \class HTTPServerSocketHandler
    \brief Holds SSL or ordinary TCP sockets, handles all client socket operation in separate thread
    \ingroup HTTPServer

	This object is used in separate client HTTPServerThread thread. Handles all server socket operations
	HTTPServerSocketHandler as child and HTTPServerThread as parent live in separate threads. 
	Communication between those two objects in separate threads is based on:
	- signal/slot technology 
	- shared conditon object 
	- semaphore (for locking out access to the socket, when reading to disable HTTPServerThread to write new message)
	- shared data buffers stored in HTTPServerThread - all access to the shared data is serialized 
*/
class HTTPServerSocketHandler : public QObject
{
      Q_OBJECT 

public:

	HTTPServerSocketHandler(int nSocketDescriptor,bool bUseSSL=false, QObject *parent = 0);
	~HTTPServerSocketHandler();

	/*! SocketState defines state of socket */
	enum SocketState 
	{
		STATE_NOT_CONNECTED,		///<Socket not connected
		STATE_ACCEPTING_CONNECTION,	///<Socket is connecting
		STATE_WRITE,				///<Socket is in writing mode
		STATE_READ,					///<Socket is in reading mode
		STATE_READY,				///<Socket is connected and ready for use
		STATE_CLOSING				///<Socket is in disconnecting state (deleteLater())
	};			

	Status				GetLastError(){return m_LastError;};
	HTTPContext			GetConnectionContext(){return m_ConnContext;};
	int					GetSocketState(){return m_nState;};
	void				SetRequestHandlers(QList<HTTPHandler *> pLstHandlers){m_pLstHandlers=pLstHandlers;};

	QDateTime			m_datCreated;
	int					m_nOrphanSocketTimeOut; //in minutes, 0-delete at once (default), >0 compare current datetime with m_datCreated

public slots:
	void SocketDestroy();
	void SocketWrite(QByteArray byteMsgSrv);

private slots:
	void OnReadData();
	void OnConnectionClosed();
	void OnBytesWritten(qint64 bytes);
	void ErrorHandler(QAbstractSocket::SocketError);
	void OnSSLErrors(const QList<QSslError>&);

signals:
	void DestroyThread();
	void KickBanIP(QString strIP,int nBanTime,int nBanCounter);

protected:
	void KickBanIP_OnTimePeriod(QString strIP,int nTimePeriod);
	void KickBanIP_OnNTries(QString strIP);
	void WriteSocketPacket(bool bDontChangeState=false);
	void ProcessRequest(HTTPHandler *pRequestHandler,QHttpRequestHeader &header, QByteArray &body);
	void SendResponse(Status lastError,HTTPResponse &responseMessage);
	void timerEvent(QTimerEvent *event);
	void AbortConnection(bool bKick=true);
	
	
	QList<HTTPHandler *>				m_pLstHandlers;			///< holds all handlers for all HTTP requests: this is common for all HTTP server threads in use
	Status								m_LastError;			///< last error from socket
	int									m_nTimerID;				///< timer ID for timeout errors
	QTcpSocket							*m_TCPSocket;			///< holds pointer to SSL or  TCP socket
	int									m_nState;				///< state of socket
	QByteArray							m_reqBuffer;			///< temporary buffer for accepting new server messages or responses
	QByteArray							m_outgoingBuffer;		///< buffer for outgoing messages/response: must be alive for chunked send
	QSemaphore							*m_SocketSemaphore;		///<Semaphore for syncing access to socket!
	bool								m_bAbortConnectionInProgress;
	HTTPStreamHandler*					m_StreamHandler;
	bool								m_bCloseSocketAfterResponse;
	HTTPContext							m_ConnContext;			///< connection context

	//write
	qint64								m_ByteToWrite;
	qint64								m_ByteWritten;
	qint64								m_ByteWrittenToSocket;

	//read
	qint64							m_Read_nChunkReadIteration;		//this is pure TCP chunk...no connection to the HTTP chunk data
	QHttpRequestHeader				m_Read_header;
	qint64							m_Read_nTotalLength;
	HTTPHandler*					m_Read_pRequestHandler;
	bool							m_Read_bBlockReadStillProcessingLastChunk;
};






#endif //HTTPSERVERSOCKETHANDLER_H
