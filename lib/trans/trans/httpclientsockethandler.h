#ifndef HTTPCLIENTSOCKETHANDLER_H
#define HTTPCLIENTSOCKETHANDLER_H

#include <QThread>

#include "common/common/status.h"
#include "httprequest.h"
#include "httpresponse.h"
#include "httphandler.h"
#include "httpclientconnectionsettings.h"
#include "httpclientconnection.h"
#include "httputil.h"
#include <QSslSocket>

//#include "qtsslsocket.h"



/*!
    \class HTTPClientSocketHandler
    \brief Holds SSL or ordinary TCP sockets, handles all client socket operation in separate thread
    \ingroup HTTPClient

	This object is used in separate client HTTPClientThread thread. Enables sending snyc or async messages.
	Also it can receive any incoming server message. NOTE: Socket is kept alive! 
	HTTPClientSocketHandler as child and HTTPClientThread as parent live in separate threads. 
	Communication between those two objects in separate threads is based on:
	- signal/slot technology (for singaling HTTPClientThread when new socket event arise (read/write/error)
	- shared conditon object (for stopping HTTPClientThread when in SYNC mode)
	- semaphore (for locking out access to the socket, when reading to disable HTTPClientThread to write new message)
	- shared data buffers stored in HTTPClientThread - all access to the shared data is serialized 
*/
class HTTPClientSocketHandler : public QObject
{
      Q_OBJECT 

public:

	HTTPClientSocketHandler(HTTPClientThread *pClientThread,QSemaphore *pBufferSempahore,ThreadSynchronizer *ThreadSync,QWaitCondition *waitCond,QObject *parent = 0);
	virtual ~HTTPClientSocketHandler();

	/*! SocketState defines state of socket */
	enum SocketState {
		STATE_NOT_CONNECTED=0,	///<Socket not connected
		STATE_WRITE,			///<Socket is in writing mode
		STATE_READ,				///<Socket is in reading mode
		STATE_READY				///<Socket is connected and ready for use
	};			


public slots:
	void SocketConnect();
	void SocketDisconnect();
	void SocketDestroy();
	void SocketWrite();


signals:
	void ServerMessageReceived(int);
	//void SocketErrorDetected(int); //emitted always when socket 

private slots:
	void OnReadData();
	void OnConnectedToHost();
	void OnConnectionClosed();
	void OnBytesWritten(qint64 bytes);
	void ErrorHandler(QAbstractSocket::SocketError);
	void OnAbort();
	void OnSSLReady();
	void OnSSLErrors(const QList<QSslError>&);

protected:

	void timerEvent(QTimerEvent *event);
	void ForceSocketDelete();
	virtual void CreateSocket(bool bSSLSocket);
	void WriteSocketPacket();

	//private func:
	void ClearSocketState(int LastErrCode=0,bool bAfterRead=false,bool bAfterServerMsgReceived=false,QString rawErrorText="");

	QTcpSocket *m_TCPSocket;			///< holds pointer to SSL or  TCP socket
#ifndef _DONT_USE_SSL
	QSslSocket *m_pSslSocket;			///< if SSL then not null
#endif

	int m_nState;						///< state of socket
	int m_nTimerID;						///< timer ID for timeout errors
	QByteArray m_outgoingBuffer;		///< buffer for outgoing messages: must be alive for chunked send
	QByteArray m_reqBuffer;				///< temporary buffer for accepting new server messages or responses
	HTTPClientConnectionSettings m_ConnSettings;	///<for storing current connection settings
	QSemaphore *m_SocketSemaphore;					///<Semaphore for syncing access to socket!
	//QTimer m_TimeOutException;					///<use to detecet timeout errors in scoket operations
	QWaitCondition *m_waitCond;



	// shared data between threads (socket and main):
	QSemaphore* m_BufferSemaphore;				///<resource to protect: response buffer when receiving message
	HTTPClientThread *m_ClientThread;			///<poiner to client thread shared data
	ThreadSynchronizer *m_ThreadSync;				///< for syncing between mastr & slave thread


	//write params:
	qint64 m_ByteToWrite;
	qint64 m_ByteWritten;
	qint64 m_ByteWrittenToSocket;
	bool   m_bChunkedWriteAccepted;

	//read params:
	qint64				m_Read_nChunkReadIteration;
	QHttpRequestHeader	m_Read_request_header;
	QHttpResponseHeader	m_Read_response_header;
	qint64				m_Read_nTotalLength;
	int					m_Read_nHTTPType;
	bool				m_Read_bBlockReadStillProcessingLastChunk;
	

};







#endif //HTTPCLIENTSOCKETHANDLER_H
