#include "httpmultipartserver.h"
#include "config_trans.h"

#include "common/common/threadid.h"
#include "httputil.h"
#include "common/common/logger.h"
extern Logger							g_Logger;


HTTPMultipartServer::HTTPMultipartServer(HTTPMultipartHandler *pHandler)
{
	m_pHandler=pHandler;
	SetDomain(DOMAIN_HTTP_MULTIPART_UPLOAD);
}



void HTTPMultipartServer::HandleRequest(Status &err, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{
	err.setError(0);

	qDebug()<<request.toString();
	qDebug()<<request.GetBody()->constData();

	QUrl url(request.path()); //strip query params if any...should not exists
	QString strPath=url.path();

	//strip my domain:
	strPath=strPath.replace(DOMAIN_HTTP_MULTIPART_UPLOAD,"");
	strPath=strPath.replace("//","/");

	if(request.value("Accept-Encoding")=="deflate") //if request msg is compressed, use it back
		response.setValue("Content-Encoding","deflate");

	QList<HTTPRequest> lstRequests;

	QString strContentType=request.value("Content-Type");
	//return error
	if (strContentType.toLower().indexOf("multipart")<0)
	{
		err.setError(StatusCodeSet::ERR_RPC_INVALID_REQUEST);
		ResponseHandler(err,request,response);
		return;
	}

	int nIdx=strContentType.toLower().indexOf("boundary");
	QString strDeLimiter="--"+strContentType.mid(nIdx+QString("boundary=").length()).trimmed();
	strDeLimiter.replace("\"","");

	//qDebug()<<request.GetBody()->constData();


	int nStartOfMsg=request.GetBody()->indexOf(strDeLimiter);
	if (nStartOfMsg<0)
	{
		err.setError(StatusCodeSet::ERR_RPC_INVALID_REQUEST);
		ResponseHandler(err,request,response);
		return;
	}
	nStartOfMsg +=strDeLimiter.length()+2;

	//qDebug()<<request.GetBody()->size();

	int nPrevIdx = nStartOfMsg;
	int nCurrIdx = request.GetBody()->indexOf(strDeLimiter,nPrevIdx);
	while (nCurrIdx>0)
	{
		int nHdrEnd = request.GetBody()->indexOf("\r\n\r\n",nPrevIdx);
		if(nHdrEnd > 0)
			nHdrEnd += 4;	
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_INVALID_REQUEST);
			ResponseHandler(err,request,response);
			return;
		}

		//qDebug()<<"size: "<<request.GetBody()->size();
		QByteArray header=request.GetBody()->mid(nPrevIdx,nHdrEnd-nPrevIdx);
		header.prepend("GET / HTTP/1.1\r\n");
		//qDebug()<<"size: "<<request.GetBody()->size();

		//parse 'name' and 'filename':
		QHttpRequestHeader requestHeader = QHttpRequestHeader (header);
		//qDebug()<<requestHeader.toString();
		QString strContentData=requestHeader.value("Content-Disposition");
		if (!strContentData.isEmpty())
		{
			QString strFileName;
			QString strFormName;
			qint64  nBytesTotal=0;
			HTTPUtil::ExtractFileInfoFromContentDisposition(strContentData,strFileName,nBytesTotal,strFormName);

			if (!strFileName.isEmpty())
				requestHeader.setValue("filename",strFileName);
			if (nBytesTotal>0)
				requestHeader.setValue("size",QString::number(nBytesTotal));
			if (!strFormName.isEmpty())
				requestHeader.setValue("name",strFormName);
		}


		//qDebug()<<requestHeader.toString();
		QByteArray body=request.GetBody()->mid(nHdrEnd,nCurrIdx-nHdrEnd-2); //to avoid loss
		//qDebug()<<"size: "<<request.GetBody()->size();
		//qDebug()<<body;
		
		HTTPRequest request_part(requestHeader, body);
		lstRequests.append(request_part);
		//qDebug()<<"size: "<<request.GetBody()->size();


		nPrevIdx =nCurrIdx+strDeLimiter.length()+2;
		nCurrIdx = request.GetBody()->indexOf(strDeLimiter,nPrevIdx);
	}
	

	m_pHandler->HandleMessage(err,lstRequests, response.GetBody(),strPath);
	ResponseHandler(err,request,response);
}


void HTTPMultipartServer::ResponseHandler(Status &err,HTTPRequest &request, HTTPResponse &response)
{
	switch(err.getErrorCode())
	{
	case 0:	// no error
		SetResponseHeader(request,response,200,"OK");
		err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		return;
	case StatusCodeSet::ERR_SYSTEM_KICKBAN:
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN,err.getErrorTextRaw());
		return;
	case StatusCodeSet::ERR_HTTP_MULTIPART_BAD_REQUEST:
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,400,"Bad Request");
		return;
	case StatusCodeSet::ERR_HTTP_MULTIPART_AUTHENTICATION_FAILED:
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,401,"Unauthorized");
		return;
	case StatusCodeSet::ERR_HTTP_FUNCTION_NOT_EXISTS:
		err.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		SetResponseHeader(request,response,404,"File not found");
		return;
	
	default:
		SetResponseHeader(request,response,500,"Internal Server Error: " +err.getErrorText());
		err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		return;
	}

}


void HTTPMultipartServer::SetResponseHeader(HTTPRequest &request,HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(nHTTPStatusCode,strHTTPCodeText);
	response.setValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg("2010.01"));
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setValue("Cache-Control","no-cache");
	response.setValue("Content-Type","text/xml");
	response.setValue("Access-Control-Allow-Origin","*");

	if (!request.value("Cookie").isEmpty())
		response.setValue("Set-Cookie",request.value("Cookie"));

}

