#include "httphostresolver.h"



//-------------------------------------------------------------------------
//					HOST LOOK UP
//-------------------------------------------------------------------------


/*!
	When new connection settings are given or IP address change, calculate new host name
	But dont wait for results, SLOT HostNameFetched will be invoked.
	If not found or err m_strHost=''
*/
void HTTPHostResolver::HostLookUp(QString IpAddress){
	QHostInfo::lookupHost(IpAddress, this, SLOT(HostNameFetched(QHostInfo))); //do not wait for results
	m_strHost="";	//reset host name

	//QHostInfo info = QHostInfo::fromName("");

	//qDebug()<<info.hostName();
}

/*!
	SLOT: invoked by host lookup mechanism, stores host name in the m_Host
*/

void HTTPHostResolver::HostNameFetched(QHostInfo host){
	if(host.error()==QHostInfo::NoError)
		m_strHost=host.hostName();
	else
		m_strHost="";

}
