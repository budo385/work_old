#ifndef SOAPRPCMESSAGE_H
#define SOAPRPCMESSAGE_H

#include "common/common/status.h"
#include "rpcmessage.h"
#include "qtsoap/qtsoap.h"
#include "xmlrpcvalue.h"


/*!
	\class SoapRpcMessage
	\brief class for compiling & parsing XML RPC call
	\ingroup SOAPModule

	Main class for generating SOAP message and parsing response. For every data type that must send through 
	XMLRPCMessage separate XMLRCPValue object must be created with Serialize/Deserialize functions.

	No data is stored inside object except string buffer containing message. 
	Supports NULL values: <nil/>
	Support basic QT variables

	Use:
	- SetBuffer() - sets pointer to external buffer in which SOAP stream will be written to or parsed from (to save one copy)

	sending XmlRPC:
	- AddParameter() - adds parameter in sequential order. First call sets first parameter, etc..
	- GenerateRPC() - generates RPC method based on previously setted parameters
	- GenerateFault() - generates SOAP fault message

	receiving XmlRPC:
	- GetParameterCount() - gets count of parameters in XmlRpc stream
	- CheckForFault() - checks if message contains fault. Call this prior to any parsing!
	- GetMethodName() - gets method name from xmlrpc stream
	- GetParameter() - gets parameter value from xmlrpc stream

*/

class SoapRpcMessage : public RpcMessage
{
public:
	SoapRpcMessage(); //:m_byteXMLMsg(NULL){};
	virtual ~SoapRpcMessage(){};


	/* For generating RPC request */
	/*-----------------------------*/
/*
	void GenerateRPC (QString strMethodName,Status& pStatus);
	void GenerateFault (int nFaultCode, QString strFaultText);
	void GenerateFault (int nFaultCode);

	bool AddParameter(QString*,QString strParamName="Param");	
	bool AddParameter(QVariant*,QString strParamName="Param");	
	bool AddParameter(int*,QString strParamName="Param");	
	bool AddParameter(bool*,QString strParamName="Param");	
	bool AddParameter(double*,QString strParamName="Param");	
	bool AddParameter(QDate*,QString strParamName="Param");		
	bool AddParameter(QDateTime*,QString strParamName="Param");	
	bool AddParameter(QTime*,QString strParamName="Param");		
	bool AddParameter(QByteArray*,QString strParamName="Param"); 
	bool AddParameter(DbRecordSet*,QString strParamName="Param"); 
*/

	/* For parsing RPC response */
	/*---------------------------*/
/*
	void GenerateResponse(Status& pStatus);
	int GetParameterCount();
	void CheckForFault (Status&);
	QString GetMethodName(Status&);

	void GetParameter(quint16 nPosition, QString*, Status& );
	void GetParameter(quint16 nPosition, QVariant*, Status& );
	void GetParameter(quint16 nPosition, int*, Status& );
	void GetParameter(quint16 nPosition, bool*, Status& );
	void GetParameter(quint16 nPosition, double*, Status& );
	void GetParameter(quint16 nPosition, QDate*, Status& );
	void GetParameter(quint16 nPosition, QDateTime*, Status& );
	void GetParameter(quint16 nPosition, QTime*, Status& );
	void GetParameter(quint16 nPosition, QByteArray*, Status& );
	void GetParameter(quint16 nPosition, DbRecordSet*, Status&);
*/


	/* buffer */
/*
	void SetBuffer(QByteArray *Buffer);
	QByteArray* GetBuffer(){return m_byteXMLMsg;};
	void ClearData(){m_byteXMLMsg->clear();m_SoapMsg.clear();}

*/
private:

	QtSoapStruct * DbRecordSetSerialize(DbRecordSet *pValue);
/*
	QByteArray *m_byteXMLMsg;	
	QtSoapMessage m_SoapMsg;
	QList<QtSoapSimpleType*> lstSOAP_SimpleTypes;
	QList<QtSoapArray*> lstSOAP_Structures;
	QList<QtSoapStruct*> lstSOAP_Arrays;
	XmlRpcValueString xmlValString;
	XmlRpcValueBool xmlValBool;
	XmlRpcValueInteger xmlValInteger;
	XmlRpcValueDouble xmlValDouble;
	XmlRpcValueDateTime xmlValDateTime;
	XmlRpcValueBinary xmlValBinary;
	XmlRpcValueFault xmlValFault;
	XmlRpcValueDbRecordSet xmlDbRecordSet;
	*/
};

#endif // SOAPRPCMESSAGE_H
