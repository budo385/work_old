#ifndef SKELETONRPCMESSAGEHANDLER_H
#define SKELETONRPCMESSAGEHANDLER_H


#include "rpcprotocol.h"
#include "rpcmessage.h"



/*!
    \class RpcSkeletonMessageHandler
    \brief Used as local helper object to make RPC message handling easier
    \ingroup RPCModule

	Makes instances of rpcrequest & rpcresponse message handlers based on RPC protocol type used
	fill them with buffers. When this object is destroyed msg instances are deleted
	automatically.
	Use msg handlers as public: SkeletonRpcMessageHandler.msg_in or SkeletonRpcMessageHandler.msg_out
*/

class RpcSkeletonMessageHandler
{

public:
	RpcSkeletonMessageHandler(RpcProtocol *skeleton,QByteArray *pBufRequest,QByteArray *pBufResponse, int nClientTimeZoneOffsetMinutes);
	~RpcSkeletonMessageHandler();

	QString GetRequestMethodNameSpace();
	QString GetSkeletonMethodName();

	RpcMessage *msg_in;			//<pointer to RPC message handler of client request
	RpcMessage *msg_out;		//<pointer to RPC message handler of response

};


#endif //SKELETONRPCMESSAGEHANDLER_H
