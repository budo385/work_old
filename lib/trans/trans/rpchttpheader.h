#ifndef RPCHTTPHEADER_H
#define RPCHTTPHEADER_H


#include <QHostInfo>
#include "rpcprotocol.h"
#include "httprequest.h"
#include "httpresponse.h"


/*!
    \class RpcHttpHeader
    \brief Used as local helper object to make RPC HTTP requests/response easier
    \ingroup RPCModule

	Makes instance of RpcHttpHeader init with specified protocol.
*/

class RpcHttpHeader
{

public:
	RpcHttpHeader():m_strHost(""),m_QStrDomain(""){};
	~RpcHttpHeader(){};
	
	void SetHTTPTags(QString strServerTag, QString strClientTag){m_strServerTag=strServerTag; m_strClientTag=strClientTag;}
	void InitRpcProtocol(RpcProtocol *prot);
	void InitHost(QString strHostIP, int nPort=80);

	void SetHttpRequestHeader(HTTPRequest& request);
	void SetHttpResponseHeader(HTTPResponse& response);
	void UseCompression(bool bUse=true){m_bUseCompress=bUse;}

private:
	QString m_strHost;
	QString m_QStrDomain;
	QString m_strServerTag; 
	QString m_strClientTag;
	bool	m_bUseCompress;
};


#endif //RPCHTTPHEADER_H
