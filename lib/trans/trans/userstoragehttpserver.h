#ifndef USERSTORAGEHTTPSERVER_H
#define USERSTORAGEHTTPSERVER_H

#include "trans/trans/httphandler.h"
#include <QReadWriteLock>
#include <QHash>
#include <QFileInfo>

class UserStorageHTTPServer : public HTTPHandler
{
public:

	enum MethodTypes
	{
		METHOD_TYPE_UNKNOWN,
		METHOD_TYPE_CLEAR,
		METHOD_TYPE_UPLOAD,
		METHOD_TYPE_DOWNLOAD
	};

	UserStorageHTTPServer();
	~UserStorageHTTPServer();

	virtual void	HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);
	virtual bool	InitChunkedHttpStream(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);
	bool			AddDirPath(QString strAlias,QString strDirPath);
	void			RemoveDirPath(QString strAlias);

protected:
	virtual bool	AuthenticateUser(QString strCookieValue,QString &strSessionID){return true;};
	virtual QString InitializeUserStorageDirectory(Status &status,QString strUserSubDir){return "";};
	void			SetResponseHeader(HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText);

	virtual bool	Method_Dispatcher(Status &status, QString strMethod, int nMethodType, QString strDirPath, QString strFileName, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler);
	virtual bool	Method_Upload(Status &status, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler,QString strDirPath, QString strFileName);
	virtual bool	Method_DownLoad(Status &status, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler,QString strDirPath, QString strFileName);
	virtual bool	Method_Clear(Status &status, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler,QString strDirPath, QString strFileName);

	virtual int		ResolveMethodType(QString strMethod);
	virtual void	GetFilePath(Status &status,int nMethodType,QString strSession,const HTTPRequest &request, const QUrl &url,QString &strAliasDir, QString &strDirPath,QString &strFileName, qint64 &nFileSizeForUpload);
	virtual void	CheckPermission(Status &pStatus,QString strSession,int nMethodType,QString strAliasDir,QString strDirPath,QString strFileName,qint64 nFileSizeForUpload){};
	virtual void	GetDirectoryPath(Status &status,const QUrl &url,QString strSession,QString &strDirPath);

	virtual QString GetFileName_Download(const QUrl &url);
	virtual QString GetFileName_Upload(const QUrl &url,const HTTPRequest &request,qint64 &nOriginalFileSize);

	virtual void	GetFileProperties(QFileInfo &fileinfo, QString &strETag, QString &strLastModified);
	
	QHash<QString,QString> m_hshDirPaths;
	QReadWriteLock  m_lckDirPaths;
};


#endif // USERSTORAGEHTTPSERVER_H
