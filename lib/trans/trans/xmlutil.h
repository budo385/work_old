#ifndef XMLUTIL_H
#define XMLUTIL_H

#include "common/common/dbrecordset.h"

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

/*!
	\class XmlUtil
	\brief XML parsing, encoding, and decoding and message handlers.
	\ingroup RPCModule

	Can be used as static: no state
*/

class XmlUtil 
{
	public:
		static void GetTagContent(QByteArray* pBuffer,QString strStartTag,QString strEndTag, int& nStartOffset,int& nLength,int nCount=0);
		static QString GetNextTag(QByteArray* pBuffer,int& nStartOffset,int nLength);
		static QString xmlEncode(const QString& );
		static QByteArray xmlDecode(const QByteArray& );
		static QByteArray fromBase64(QByteArray *data,quint64 nStart, quint64 nLength);
		static QByteArray ConvertRecordSet2ByteArray(DbRecordSet &data);
		static DbRecordSet ConvertByteArray2RecordSet(QByteArray &data);
		static QByteArray ConvertRecordSet2ByteArray_Fast(DbRecordSet &data);
		static DbRecordSet ConvertByteArray2RecordSet_Fast(QByteArray &data);

};

#endif // XMLUTIL_H
