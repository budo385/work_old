#include "ipaddress.h"

#include <QVariant>

//copy constructor
IpAddressAdv::IpAddressAdv(const IpAddressAdv &that)
{
	operator = (that);
}

//operator =
IpAddressAdv &IpAddressAdv::operator =(const IpAddressAdv &that)
{
	if(this != &that)
	{
		m_strIP	= that.m_strIP;
		m_nPort	= that.m_nPort;
		m_nTries=that.m_nTries;
	}
	return *this;
}

//operator ==
bool IpAddressAdv::operator ==(const IpAddressAdv &that)
{
	if(m_strIP	== that.m_strIP&&m_nPort	== that.m_nPort)
		return true;
	else
		return false;
}


/*!
	Stores IP address from string: xxx.xxx.xxx.xxx:port

	\param ip			- ip address
	\return 			- true if ip address is valid
*/
bool IpAddressAdv::SetIpAddress(QString ip)
{
	
	int nPos=ip.indexOf(":");
	if( nPos<0)
		m_strIP=ip;
	else
	{
		m_strIP=ip.left(nPos);
		m_nPort=ip.right(ip.length()-nPos-1).toInt();
	}

	//QHostAddress temp;//<- not good for address of type: sokra.dyn.com

	
	if(m_nPort<0||m_nPort>65535)
	{
		m_strIP="";m_nPort=0;return false;
	}
	else
		return true;

}


QString IpAddressAdv::GetIpAddress()
{
	if(m_nPort!=0)
		return m_strIP+":"+QVariant(m_nPort).toString();
	else
		return m_strIP;
}

bool IpAddressAdv::operator <(const IpAddressAdv &that) const
{
	return m_nTries<that.m_nTries;
}