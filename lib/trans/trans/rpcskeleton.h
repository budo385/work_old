#ifndef RPCSKELETON_H
#define RPCSKELETON_H

#include "rpcprotocol.h"
#include <QReadWriteLock>

/*!
    \class RpcSkeleton
    \brief Abstract skeleton class, type of skeleton is based on type of Rpc protocol in use
    \ingroup RPCModule

	Type of skeleton is defined by protocol type given in constructor and RPC message handler.
	Use:
	- when creating new skeleton object, inherit this one and initialize its constructor with 
	RPCProtocolType.
	- implement pure virtual HandleRPC function to handle RPC requests
	- set namespace qualificator for skeleton object( ussually name of object in business set)
	- use m_SkeletonList for skeleton sets to store and handle list of skeleton object or other skeleton sets

	- pRequestHeader -> if client has additional request information hidden in HTTP METHOD or URL path-> skeleton dispatcher or someone else must process this
*/

class RpcSkeleton:public RpcProtocol
{

public:
	RpcSkeleton(int pNumRPCProtocolType):RpcProtocol(pNumRPCProtocolType){};
	virtual ~RpcSkeleton(){};

	//pure virtual, RPC request pBufRequest as incoming stream is processed and response is returned in pBufResponse
	virtual bool	HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod,int nClientTimeZoneOffsetMinutes=0)=0;
	static void		LoadServiceDescriptionData(Status &err,const QString strFilePath);
	static void		EnableServiceDescription(bool bEnable){s_bUseDescription=bEnable;};
	virtual void	SetWebServiceURL(QString strURL);
	virtual QString	GetWebServiceURL();

protected:
	QString			GetNameSpace(){return m_strNameSpace; }
	void			SetNameSpace(const QString strValue){ m_strNameSpace = strValue; }
	virtual bool	TestNameSpace(const QString strRPCMethod){return (bool)(m_strNameSpace==strRPCMethod);}
	virtual bool	TestNameSpaceForRest(const QString strRPCMethod,QString &strComparePath,int &nResID,int &nParentResID, QString &strHttpMethod);
	QString			GetServiceDescription(const QString strServiceDescFileName,int nResID=-1, int nParentResID=-1);
	QString			GetServiceXmlNameSpace(const QString strSchemaName);

	QString			m_strNameSpace;					//< Name of skeleton object on server side
	QString			m_strWebServiceURLPath;			//< abs path for webservices: must be know to produce valid schemas, e.g. https://192.168.200.10/rest
	static QHash<QString,QByteArray> s_hshResData;	//< collection of all resource/all xml data used for webservices desc
	static bool		s_bUseDescription;
	QReadWriteLock  m_lckWebServiceURLPath;

};


#endif //RPCSKELETON_H
