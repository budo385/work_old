#include "rpchttpheader.h"


/*! Inits domain based on protocol

	\param prot - pointer to stub object, must have GetRPCMessageHandler method 
*/
void RpcHttpHeader::InitRpcProtocol(RpcProtocol *prot)
{	
	m_QStrDomain="/"+prot->GetRPCDomain();
	m_strHost="";
	m_bUseCompress=false;
}



/*! 
	Init host info for http request, if not init, then host tag will not be set

	\param strHostIP - based on Host Ip set Host tag in HTTP request header
*/
void RpcHttpHeader::InitHost(QString strHostIP, int nPort)
{
	QHostInfo info = QHostInfo::fromName(strHostIP);
	m_strHost=info.hostName();
	if (nPort!=80)
		m_strHost+=":"+QVariant(nPort).toString();
}


/*! 
	Adds RPC specific HTTP tags
	\param request - HTTP request header filled with right tags
*/
void RpcHttpHeader::SetHttpRequestHeader(HTTPRequest& request)
{
	//set request:
	request.setRequest("POST", m_QStrDomain);
	if(m_strHost!="")
		request.setValue("Host",m_strHost);
	request.setValue("User-agent",m_strClientTag); 
	request.setValue("Content-Type","text/xml");
	request.setValue("Connection","Keep-Alive");
	if (m_bUseCompress)
	{
		request.setValue("Accept-Encoding","deflate");
		request.setValue("Content-Encoding","deflate");
	}
	
}

/*! 
	Adds RPC specific HTTP tags
	\param response - HTTP response header filled with right tags
*/
void RpcHttpHeader::SetHttpResponseHeader(HTTPResponse& response)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(200,"OK");
	response.setValue("Server",m_strServerTag); 
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setValue("Cache-Control","no-cache");
	response.setValue("Content-Type","text/xml");
}





