#ifndef HTTPSERVERCONNECTIONSETTINGS_H
#define HTTPSERVERCONNECTIONSETTINGS_H



/*!
    \class HTTPServerConnectionSettings
    \brief Data placehoder for server connection settings, use by HTTPServer to start HTTPServer
    \ingroup HTTPServer
*/
class HTTPServerConnectionSettings
{

public:
	HTTPServerConnectionSettings():
	  m_strServerIP(QHostAddress::Any),
	  m_nPort(80),
	  m_bUseSSL(false),
	  m_bUseCompression(false),
	  m_nSSLNoPrivateKey(0),
	  m_nDefaultTimeOut(20){};
	~HTTPServerConnectionSettings(){};

	QString m_strServerIP;		///< server IP
	int m_nPort;				///< server port
	bool m_bUseSSL;				///< use SSL
	bool m_bUseCompression;		///< use Compression
	int m_nDefaultTimeOut;		///< default time out for all operations	 (in seconds)

	//for SSL:
	QString m_strCertificateFile;	///< certificate filename "c:\\server.cert"
	QString m_strCACertificateFileBundle;	///< certificate filename of CA authority (chain): "c:\\server.ca-bundle"
	QString m_strPrivateKeyFile;	///< server private key filename "c:\\server.pkey"
	int		m_nSSLNoPrivateKey;		///< 0 - use hardcoded priv. key for cert file, else use blank pass

	//Security:
	unsigned int m_nIntrusionTries;				///< Number of tries in which damaged http/rpc messages will be tolerated
	unsigned int m_nKickPeriodAfterIntrusion;	///< (seconds) kick period in which IP will be banned after m_nIntrusionTries



};
#endif //HTTPSERVERCONNECTIONSETTINGS_H
