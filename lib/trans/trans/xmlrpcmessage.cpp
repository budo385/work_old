
#include "xmlrpcmessage.h"
#include "xmlutil.h"
#include "trans/trans/config_trans.h"
#include "common/common/datahelper.h"


	/*! METHOD REQUEST*/
	static const QString METHODNAME_TAG = "<methodName>";
	static const QString METHODNAME_ETAG = "</methodName>";
	static const QString METHODCALL_TAG = "<methodCall>";
	static const QString METHODCALL_ETAG = "</methodCall>";

	static const QString PARAMS_TAG = "<params>";
	static const QString PARAMS_ETAG = "</params>";
	static const QString PARAM_TAG = "<param>";
	static const QString PARAM_ETAG = "</param>";

	/*! XML IDENTIFIER*/
	static const QString XMLRPC_SIGNATURE = "SOKRATES_XP Module 0.2";
	static const QString XML_VERSION ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";

	/*! XML RESPONSE*/
	static const QString METHODRESPONSE_TAG = "<methodResponse>";
	static const QString METHODRESPONSE_ETAG = "</methodResponse>";




/*! -------------------------MAIN INTERFACE--------------------------------*/

/*!
    Generates XMLRPC message which is stored in the data() pointer to external buffer
    \param strMethodName reference to output buffer
	\param pStatus		 error if method name='' or params=0 
*/
void XmlRpcMessage::GenerateRPC (QString strMethodName,Status& pStatus, QString strXmlBodyNamespaces){

	Q_ASSERT(m_byteXMLMsg!=NULL);//check if buffer is set

	//new test: total size can not exceed 80Mb:
	if (m_byteXMLMsg->size()>XML_MAXIMUM_BINARY_SIZE)
	{
		QString strSizeActual=DataHelper::GetFormatedFileSize((double)m_byteXMLMsg->size(),1);
		pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_SIZE_EXCEEDED,strSizeActual);
		return;
	}

	/* call must have method name && at least one parameter */
	if (strMethodName.isEmpty()||m_byteXMLMsg->size()==0)
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_GENERATE_FAIL);return;}


	/* build XML header: method name */
	m_byteXMLMsg->insert(0,XML_VERSION+METHODCALL_TAG+METHODNAME_TAG+strMethodName+METHODNAME_ETAG+PARAMS_TAG+"\r\n");

	/* close XML msg */
	*m_byteXMLMsg+="\r\n"+PARAMS_ETAG+METHODCALL_ETAG;
	pStatus.setError(0);

}


/*!
    Generates XMLRPC fault message: clears buffer
    \param nFaultCode error code
	\param strFaultText error text
*/
void XmlRpcMessage::GenerateFault (int nFaultCode, QString strFaultText){
	xmlValFault.Serialize(nFaultCode,strFaultText);

	/* build XML header: method response */
	m_byteXMLMsg->insert(0,XML_VERSION+METHODRESPONSE_TAG+"\r\n");

	/* build XML header: method response */
	*m_byteXMLMsg+="\r\n"+METHODRESPONSE_ETAG;
}


/*!
    Overloaded function: uses Status object to fetch error text
    \param nFaultCode error code
*/

void XmlRpcMessage::GenerateFault (int nFaultCode){

	Status err;
	err.setError(nFaultCode);
	xmlValFault.Serialize(err.getErrorCode(),err.getErrorTextRaw());

	m_byteXMLMsg->insert(0,XML_VERSION+METHODRESPONSE_TAG+"\r\n");
	
	*m_byteXMLMsg+="\r\n"+METHODRESPONSE_ETAG;
}



/*!
    Generates XMLRPC response message
    \param pStatus returned err inside or 0 if all OK

*/
void XmlRpcMessage::GenerateResponse (Status& pStatus, QString strXmlBodyNamespaces, QString strAddOnXmlData)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);//check if buffer is set

	/* call must have method name && at least one parameter : WRONG!!!*/
	//if (m_byteXMLMsg->size()==0)
	//	{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_GENERATE_FAIL);return;}

	/* build XML header: method response */
	m_byteXMLMsg->insert(0,XML_VERSION+METHODRESPONSE_TAG+"\r\n");

	/* build XML header: method response */
	*m_byteXMLMsg+="\r\n"+METHODRESPONSE_ETAG;

	pStatus.setError(0);
}


/*!
    Checks if XML message contains fault and returns it back:
    \param pStatus returned err inside or 0 if all OK
*/
void XmlRpcMessage::CheckForFault (Status& pStatus){
	int nFaultCode; 
	QString strFaultText;
	xmlValFault.DeSerialize(nFaultCode,strFaultText);
	pStatus.setError(nFaultCode,strFaultText);
}



/*!
    SetBuffer
	\param buffer reference to the external buffer

void XmlRpcMessage::SetBuffer(QByteArray& buffer){
		m_byteXMLMsg=&buffer;
		xmlValString.SetBuffer(m_byteXMLMsg);
		xmlValBool.SetBuffer(m_byteXMLMsg);
		xmlValInteger.SetBuffer(m_byteXMLMsg);
		xmlValDouble.SetBuffer(m_byteXMLMsg);
		xmlValDateTime.SetBuffer(m_byteXMLMsg);
		xmlValBinary.SetBuffer(m_byteXMLMsg);
		xmlDbRecordSet.SetBuffer(m_byteXMLMsg);
		xmlValFault.SetBuffer(m_byteXMLMsg);
}

*/

/*!
    SetBuffer
	\param buffer reference to the external buffer
*/
void XmlRpcMessage::SetBuffer(QByteArray* buffer){
		m_byteXMLMsg=buffer;
		xmlValString.SetBuffer(m_byteXMLMsg);
		xmlValBool.SetBuffer(m_byteXMLMsg);
		xmlValInteger.SetBuffer(m_byteXMLMsg);
		xmlValDouble.SetBuffer(m_byteXMLMsg);
		xmlValDateTime.SetBuffer(m_byteXMLMsg);
		xmlValBinary.SetBuffer(m_byteXMLMsg);
		xmlDbRecordSet.SetBuffer(m_byteXMLMsg);
		xmlValFault.SetBuffer(m_byteXMLMsg);
}



/*!
    Parameter count
	\return parameter count in buffer
*/
int XmlRpcMessage::GetParameterCount(){
	Q_ASSERT(m_byteXMLMsg!=NULL);
	return m_byteXMLMsg->count(PARAM_TAG.toLatin1());
	}

/*!
    Get Method name
	\param pStatus error if method TAGs doesnt exists
	\return method name
*/
QString XmlRpcMessage::GetMethodName(Status &pStatus){
	int nStart=0,nLength;
	XmlUtil::GetTagContent(m_byteXMLMsg,METHODNAME_TAG,METHODNAME_ETAG,nStart,nLength);
	if (nStart>=0&&nLength>=0)
	{
		pStatus.setError(0);
		return m_byteXMLMsg->mid(nStart,nLength);
	}
	else
	{
		pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
		return "";
	}
}





/*! -------------------------ADD PARAMETER--------------------------------*/



/*!
    Appends string at end of parameter list. If NULL sets <nil/>, 
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(QString* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);
	
	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlValString.Serialize(pValue);
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;
}

/*!
    Appends bool,int or double at end of parameter list
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(QVariant* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);

	switch(pValue->type())
	{
		case QVariant::Bool:

			(*m_byteXMLMsg)+=PARAM_TAG;
			xmlValBool.Serialize(pValue); 
			(*m_byteXMLMsg)+=PARAM_ETAG;
			break;

		case QVariant::Int:

			(*m_byteXMLMsg)+=PARAM_TAG;
			xmlValInteger.Serialize(pValue);
			(*m_byteXMLMsg)+=PARAM_ETAG;
			break;


		case QVariant::Double:

			(*m_byteXMLMsg)+=PARAM_TAG;
			xmlValDouble.Serialize(pValue); 
			(*m_byteXMLMsg)+=PARAM_ETAG;
			break;

		default:
			return false;
	};
	return true;
}




/*!
    Appends bool at end of parameter list
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(bool* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);

	QVariant var(*pValue);
	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlValBool.Serialize(&var); 
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;
}

/*!
    Appends int at end of parameter list
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(int* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);
	
	QVariant var(*pValue);
	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlValInteger.Serialize(&var);
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;
}

/*!
    Appends double at end of parameter list
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(double* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);

	QVariant var(*pValue);
	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlValDouble.Serialize(&var); 
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;
}





/*!
    Appends date at end of parameter list. If NULL sets <nil/>
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(QDate* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);

	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlValDateTime.Serialize(pValue); 
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;}

/*!
    Appends DateTime at end of parameter list. If NULL sets <nil/>
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(QDateTime* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);

	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlValDateTime.Serialize(pValue);
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;}

/*!
    Appends time at end of parameter list. If NULL sets <nil/>
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(QTime* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);


	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlValDateTime.Serialize(pValue);
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;}

/*!
    Appends binary at end of parameter list. If NULL sets <nil/>
	\param pValue value for xml convert
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(QByteArray* pValue,QString strParamName){
	Q_ASSERT(m_byteXMLMsg!=NULL);


	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlValBinary.Serialize(pValue);
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;}


/*!
    Appends record set at end of parameter list. If NULL see record set
	\param pValue value for xml convert
	Warning: can not use this parameter if using nested RecordSets->ASSERT will be fired
	Warning: on other side, accepting recordset must be defined before getting parameter
	\return false if type invalid
*/
bool XmlRpcMessage::AddParameter(DbRecordSet* pValue,QString strParamName, const QString strRowName, const QString strRowURLAttr,QString strRowURLAttr_IDField)
{
	Q_ASSERT(m_byteXMLMsg!=NULL);


	(*m_byteXMLMsg)+=PARAM_TAG;
	xmlDbRecordSet.SerializeFast(pValue);
	(*m_byteXMLMsg)+=PARAM_ETAG;
	return true;}




/*! -------------------------GET PARAMETER--------------------------------*/


/*!
    Gets STRING paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, QString* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	
	//deserialize value and return in pValue
	xmlValString.DeSerialize(nStart,nLength,pValue,pStatus);
};


/*!
    Gets INT, BOOL or DOUBLE paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, QVariant* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}


	switch(pValue->type())
	{
		case QVariant::Bool:
			xmlValBool.DeSerialize(nStart,nLength,pValue,pStatus); break;

		case QVariant::Int:
			xmlValInteger.DeSerialize(nStart,nLength,pValue,pStatus);break;

		case QVariant::Double:
			xmlValDouble.DeSerialize(nStart,nLength,pValue,pStatus);break;

		default:
			{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}
	};


};





/*!
    Gets BOOL  paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, bool* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

	QVariant var;
	xmlValBool.DeSerialize(nStart,nLength,&var,pStatus); 
	if(pStatus.IsOK())
		*pValue=var.toBool();
}



/*!
    Gets INT  paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, int* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

	QVariant var;
	xmlValInteger.DeSerialize(nStart,nLength,&var,pStatus);
	if(pStatus.IsOK())
		*pValue=var.toInt();
}




/*!
    Gets DOUBLE  paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, double* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

	QVariant var;
	xmlValDouble.DeSerialize(nStart,nLength,&var,pStatus);
	if(pStatus.IsOK())
		*pValue=var.toDouble();
}






/*!
    Gets DATE paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, QDate* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	
	//deserialize value and return in pValue
	xmlValDateTime.DeSerialize(nStart,nLength,pValue,pStatus);

}


/*!
    Gets TIME paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, QTime* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	
	//deserialize value and return in pValue
	xmlValDateTime.DeSerialize(nStart,nLength,pValue,pStatus);

};


/*!
    Gets DATETIME paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, QDateTime* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	
	//deserialize value and return in pValue
	xmlValDateTime.DeSerialize(nStart,nLength,pValue,pStatus);

};



/*!
    Gets BINARY paramter from specified position from xml stream and convert to value
	\param nPosition parameter postion (0...)
	\param pValue in which data will be stored
	\param nPosition parameter postion (0...)
	\param pStatus error if tag not exists or type is wrong
*/
void XmlRpcMessage::GetParameter(quint16 nPosition, QByteArray* pValue, Status& pStatus){
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);
	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	//extract param content at nPosition
	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	
	//deserialize value and return in pValue
	xmlValBinary.DeSerialize(nStart,nLength,pValue,pStatus);

};



/*!
    Gets RecordSet parameter from specified position from xml stream and convert to value
	
	\param nPosition parameter position (0...)
	\param pValue in which data will be stored
	\param pStatus error if tag not exists or type is wrong
	but pValue must be then defined to match incoming data, if not ASSERT or error in pStatus will be returned
	Warning: if false then pValue recordset will be destroyed and completly rebuilded

*/
void XmlRpcMessage::GetParameter(quint16 nPosition, DbRecordSet* pValue, Status& pStatus)
{
	int nStart=0,nLength=0;
	Q_ASSERT(m_byteXMLMsg!=NULL);

	//extract param content at nPosition
	//qDebug()<<m_byteXMLMsg->constData();

	if (m_byteXMLMsg->size()==0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL);return;} //if zero size abort

	XmlUtil::GetTagContent(m_byteXMLMsg,PARAM_TAG,PARAM_ETAG,nStart,nLength,nPosition);
	if(nStart<0||nLength<0)
	{
		pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"Pos:"+QVariant(nPosition).toString()+"L:"+QVariant(nLength).toString()+"Start:"+QVariant(nStart).toString()+"Buffer size: "+QVariant(m_byteXMLMsg->size()).toString()+"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;
		//qDebug()<<m_byteXMLMsg->constData();
	}
	
	//deserialize value and return in pValue
	//xmlDbRecordSet.DeSerialize(nStart,nLength,pValue,pStatus,pSkipColumDef);
	xmlDbRecordSet.DeSerializeFast(nStart,nLength,pValue,pStatus);
};






