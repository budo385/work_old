#ifndef RESTRPCMESSAGE_H
#define RESTRPCMESSAGE_H

#include "common/common/status.h"
#include "rpcmessage.h"
#include <QDomNode>
#include <QDomDocument>

class RestRpcMessage : public RpcMessage
{
public:

	RestRpcMessage():m_byteXMLMsg(NULL),m_nTimeZoneOffsetSeconds(0){};
	virtual ~RestRpcMessage(){};


	/* For generating RPC request */
	/*-----------------------------*/

	void GenerateRPC (QString strMethodName,Status& pStatus, QString strXmlBodyNamespaces="xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
	void GenerateFault (int nFaultCode, QString strFaultText);
	void GenerateFault (int nFaultCode);
	void SetTimeZoneOffset(int nTimeZoneOffsetMinutes){m_nTimeZoneOffsetSeconds=nTimeZoneOffsetMinutes*60;}

	bool AddParameter(QString*,QString strParamName="Param");	
	bool AddParameter(QVariant*,QString strParamName="Param");	
	bool AddParameter(int*,QString strParamName="Param");	
	bool AddParameter(qulonglong*,QString strParamName="Param");	
	bool AddParameter(bool*,QString strParamName="Param");	
	bool AddParameter(double*,QString strParamName="Param");	
	bool AddParameter(QDate*,QString strParamName="Param");		
	bool AddParameter(QDateTime*,QString strParamName="Param");	
	bool AddParameter(QTime*,QString strParamName="Param");		
	bool AddParameter(QByteArray*,QString strParamName="Param"); 
	bool AddParameter(DbRecordSet*,QString strParamName="Param", const QString strRowName="", const QString strRowURLAttr="",QString strRowURLAttr_IDField=""); 


	/* For parsing RPC response */
	/*---------------------------*/

	void GenerateResponse(Status& pStatus, QString strXmlBodyNamespaces="xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", QString strAddOnXmlData="");
	int GetParameterCount();
	void CheckForFault (Status&);
	QString GetMethodName(Status&);
	void StartResponseParsing(Status&); //before any response parse call, call this function: to enable DOM XML parsing methods

	void GetParameter(quint16 nPosition, QString*, Status& );
	void GetParameter(quint16 nPosition, QVariant*, Status& );
	void GetParameter(quint16 nPosition, int*, Status& );
	void GetParameter(quint16 nPosition, qulonglong*, Status& );
	void GetParameter(quint16 nPosition, bool*, Status& );
	void GetParameter(quint16 nPosition, double*, Status& );
	void GetParameter(quint16 nPosition, QDate*, Status& );
	void GetParameter(quint16 nPosition, QDateTime*, Status& );
	void GetParameter(quint16 nPosition, QTime*, Status& );
	void GetParameter(quint16 nPosition, QByteArray*, Status& );
	void GetParameter(quint16 nPosition, DbRecordSet*, Status&);

	/* buffer */
	
	void SetBuffer(QByteArray *Buffer);
	QByteArray* GetBuffer(){return m_byteXMLMsg;};
	void ClearData(){m_byteXMLMsg->clear(); m_XmlResponseBody.clear();}


private:
	bool GetParameterNode(QDomNode &node,quint16 nParamPosition);
	QDomDocument m_XmlResponseBody;
	QByteArray *m_byteXMLMsg;	/*!< for storing xml encoded parameters */
	int m_nTimeZoneOffsetSeconds;
	
};

#endif // RESTRPCMESSAGE_H
