#include "httprequest.h"
#include "config_trans.h"
#include "common/common/zipcompression.h"

HTTPRequest::HTTPRequest(const QHttpRequestHeader &header, const QByteArray& body)
{
	SetData(header,body);
}

HTTPRequest::HTTPRequest()
	: QHttpRequestHeader()
{
}

HTTPRequest::~HTTPRequest()
{
}

void HTTPRequest::SetData(const QHttpRequestHeader &header, const QByteArray& body)
{
	QHttpRequestHeader::operator=(header);

	if (value("Content-Encoding")=="deflate")
		ZipCompression::Inflate(body,m_body);
	else
		m_body=body;
}

void HTTPRequest::GetData(QByteArray& binaryPackage)
{
	if(value("Content-Encoding")=="deflate")
	{
		ZipCompression::Deflate(m_body,binaryPackage,HTTP_COMPRESSION_LEVEL);
		if (!hasKey("Transfer-Encoding"))
			setValue("Content-Length",QString::number(binaryPackage.size()));
		binaryPackage.insert(0,toString().toLatin1());
	}
	else
	{
		if (!hasKey("Transfer-Encoding"))
			setValue("Content-Length",QString::number(m_body.size()));
		binaryPackage=toString().toLatin1()+m_body;
	}
}

