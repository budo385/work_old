/*!
\class  IPAccessList
\brief  IP-chains like class to handle access list based on client's IP address
\ingroup IPModule
*/
#ifndef _IPACCESSLIST_H_
#define _IPACCESSLIST_H_

#include <QString>
#include <QList>
#include <QDateTime>

//possible matching results
#define IP_MATCH_ALLOWED	0
#define IP_MATCH_DENIED		1
#define IP_NO_MATCH			2

class IPAccessList
{
	class IPFilterEntry
	{
		public:
			IPFilterEntry();
			IPFilterEntry(const IPFilterEntry &that);
			void operator =(const IPFilterEntry &that);

			int CheckMatch(unsigned long nIP);

		public:
			unsigned long  m_nIP;
			unsigned short m_nFilterBits;
			bool		   m_bAllow;	
			QDateTime	   m_BanTime;

	};

public:
	IPAccessList();
	~IPAccessList();

	//void LoadAddEntry(QString strIP, int nFilterBits, bool bAllow, int nPos = -1);

	void AddEntry(QString strIP, int nFilterBits, bool bAllow, int nPos = -1,int nKickBanTime=0);
	void RemoveEntry(QString strIP);

	bool IsIPAllowed(QString strIP);

	static unsigned long IpStringToNum(QString strIP);

protected:
	QList<IPFilterEntry>	m_lstAccessList;	
};

#endif
