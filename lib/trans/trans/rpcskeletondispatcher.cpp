#include  "rpcskeletondispatcher.h"



/*!
	Destructor:
	Deletes all reserverd memory
*/
RpcSkeletonDispatcher::~RpcSkeletonDispatcher()
{
	qDeleteAll(m_SkeletonList);  //new faster way to delete all objects
	
	/*
		//release memory:
	for(int i=0;i<m_SkeletonList.size();++i)
		delete m_SkeletonList.at(i);
	*/

}


/*!
	Handles RPC call, dispatches to right skeleton object based on registered NameSpace of each skeleton:
	E.g.
	When method is invoked e.g.: "SrvMsgTxt.PrintMessage", all skeletons are searched by NameSpace="SrvMsgTxt", 
	if found call is redirected to that skeleton object, else, error is returned
	\param pBufRequest buffer with RPC request as binary stream of data
	\param pBufResponse buffer with RPC repsonse as binary stream of data
	NOTE: if async request then pBufResponse wil be empty
*/

bool RpcSkeletonDispatcher::HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod,int nClientTimeZoneOffsetMinutes)
{
	for(int i=0;i<m_SkeletonList.size();++i)
	{
		if(m_SkeletonList.at(i)->HandleRPC(err,pBufRequest,pBufResponse,strRPCMethod,nClientTimeZoneOffsetMinutes)) //if true then it is processed, else skip to another handler
			return true;
	}

	//no handler found to process method: error
	err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nClientTimeZoneOffsetMinutes);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);

	return false; //request not processed
}
