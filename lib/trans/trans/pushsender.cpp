#include "pushsender.h"
#include "applepushnotification.h"
#include "androidpushnotification.h"
#include "windowpushnotification.h"
#include "common/common/logger.h"

PushSender::PushSender(int nDeviceType,int nTimeOut)
{
	m_nDeviceType = nDeviceType;
	m_nTimeOut = nTimeOut;
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	start(); //start thread
	m_ThreadSync.ThreadWait();			//prepare for start
}

PushSender::~PushSender()
{
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"PushSender destructor start");

	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit Destroy();
	m_ThreadSync.ThreadWait();			//prepare for start


	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"PushSender destructor after push object destroyed, wait for slave thread");
	//stop thread gently:
	quit();
	wait();

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"PushSender destructor end");
}



//start Thread
void PushSender::run()
{
	switch (m_nDeviceType)
	{
	case APPLE:
		m_Pusher= new ApplePushNotification(&m_ThreadSync,m_nTimeOut);
		break;
	case ANDROID:
		m_Pusher= new AndroidPushNotification(&m_ThreadSync,m_nTimeOut);
		break;
	case WINDOWS:
		m_Pusher= new WindowPushNotification(&m_ThreadSync,m_nTimeOut);
		break;
	default:
	//	Q_ASSERT(false);
		return;
	}

	connect(this,SIGNAL(SendPushMessageSignal()),m_Pusher,SLOT(SendPushMessage()));
	connect(this,SIGNAL(Destroy()),m_Pusher,SLOT(Destroy()));
	m_ThreadSync.ThreadRelease();
	exec();
}




/*!
	Sends Push messages and waits to maximum timeout period
*/
bool PushSender::SendPushMessage(QString strDeviceID, QString strApplicationID, QString strMessage, int nMessageID, QStringList lstMessageCustomData, QString strPushSoundName, int nBadgeNumber)
{
	//execute order
	m_Pusher->SetMessageData(strDeviceID,strApplicationID,strMessage,nMessageID,lstMessageCustomData, strPushSoundName, nBadgeNumber);
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit SendPushMessageSignal();				
	m_ThreadSync.ThreadWait();			//wait for slave thread
	
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"PushSender::SendPushMessage returned from send");

	//get status (slave object is destroyed when this object dies)
	if (m_Pusher)
	{
		int nExitCode=m_Pusher->GetStatusCode();
		if (nExitCode==0)
			return true;
		else
			return false;
	}
	else
		return false;
}

QString PushSender::GetLastError()
{
	if (m_Pusher)
		return m_Pusher->GetStatusText();
	else
		return tr("Error creating push message object");
}





