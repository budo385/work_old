#include "httpresponse.h"
#include "config_trans.h"
#include "common/common/zipcompression.h"

HTTPResponse::HTTPResponse(const QHttpResponseHeader &header, const QByteArray& body)
{
	SetData(header,body);
}

HTTPResponse::~HTTPResponse()
{
}

void HTTPResponse::GetData(QByteArray& binaryPackage)
{
	if(value("Content-Encoding")=="deflate")
	{
		ZipCompression::Deflate(m_body,binaryPackage,HTTP_COMPRESSION_LEVEL);
		if (!hasKey("Transfer-Encoding"))
			setValue("Content-Length",QString::number(binaryPackage.size()));
		binaryPackage.insert(0,toString().toLatin1());
	}
	else
	{
		if (!hasKey("Transfer-Encoding"))
			setValue("Content-Length",QString::number(m_body.size()));
		binaryPackage=toString().toLatin1()+m_body;
	}
}

void HTTPResponse::SetData(const QHttpResponseHeader &header, const QByteArray& body)
{
	QHttpResponseHeader::operator=(header);

	if (value("Content-Encoding")=="deflate")
		ZipCompression::Inflate(body,m_body);
	else
		m_body=body;
}
