#ifndef IPKICKER_H
#define IPKICKER_H

#include <QMutex>
#include <QMutexLocker>
#include "ipdynamicaccesslist.h"



/*!
	\class IPKicker
	\brief Holds IP filter list, authenticate client by IP address, dynamic IP monitoring -> kicking
	\ingroup IPModule

	Multithread safe
	IP address can be kicked in these ways:
	- permament kick
	- with time period (on 1sec to N-hours)
	- with N tries to the permament kick (if some1 tries N-times, it will kicked)

	Use:
	- Manages two separate lists: fixed access list & dynamic IP kick ban list
	- Fixed list is set by administrator (no change), and dynamic is changed when KickBanIP() is called
	- Save lists into DB (only dynamic) if change occur.
	- Used in the HTTPServer, every app. server has own list (two sets ofl list)
	- Multithread safe

*/
class IPKicker 
{


public:

	IPKicker();

	//init 
	void Initialize(int nAfterCounterKickTime);
	bool IsListChanged(){return m_bChanged;}

	//static list
	void SetStaticList(DbRecordSet &lstAcess);
	void GetStaticList(DbRecordSet &lstAcess);

	//dynamic list
	void SetDynamicList(DbRecordSet &lstAcess);
	void GetDynamicList(DbRecordSet &lstAcess);

	//
	bool IsIPAllowed(QString strIP);
	void KickBanIP(QString strIP,int nBanTime=0,int nBanCounter=0);

private:
	IPDynamicAccessList m_IPAccessList;			//< Fixed access list object
	IPDynamicAccessList m_IPDynamicAccessList;	//< Dynamically created list of  banned IP's
	QMutex m_Mutex;								///< mutex
	bool m_bChanged;							///< if dynmic list is changed
};

#endif // IPKICKER_H

