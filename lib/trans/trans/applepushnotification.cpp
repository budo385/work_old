#include "applepushnotification.h"
#include <QVariant>
#include <QDateTime>
#include <QFile>
#include <QSslKey>
#include <QVariant>
#include <QDateTime>
#include <QTimer>
#include <QCoreApplication>
#include <QtEndian>
#include <QDateTime>

#include "stdint.h"		//B.T. TODO: exterminate!!
#include <time.h>		//B.T. TODO: exterminate!!
#include <WinSock2.h>	//B.T. TODO: exterminate!!

#include "common/common/logger.h"
#include "common/common/threadid.h"

QString ApplePushNotification::m_strPushServerURL="";
int	ApplePushNotification::m_nPushServerPort=0;
QString	ApplePushNotification::m_strFeedBackServerURL="";
int	ApplePushNotification::m_nFeedBackServerPort=0;
QString	ApplePushNotification::m_StrPeerName="";
QString	ApplePushNotification::m_strLocalCertificatePath="";
QString	ApplePushNotification::m_strPrivateKeyPath="";



ApplePushNotification::ApplePushNotification(ThreadSynchronizer *sync,int nTimeOut)
:PushNotification(sync,nTimeOut)
{
	m_pSSLSocket=NULL;
	m_bFeedBack=false;
}

ApplePushNotification::~ApplePushNotification()
{

}

void ApplePushNotification::Destroy()
{
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Apple Push Destroy start");

	if(m_pSSLSocket)
	{
		m_pSSLSocket->close();
		m_pSSLSocket->deleteLater();
		m_pSSLSocket=NULL;
	}
	deleteLater();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Apple Push Destroy done");
	m_ThreadSync->ThreadRelease();
}

void ApplePushNotification::SendPushMessage()
{
	if (m_pSSLSocket)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,QString("Apple Push: socket exist, use it to write another push"));
		socketEncrypted();
		return;
	}

	//check for paths:
	if (!QFile::exists(m_strLocalCertificatePath) || !QFile::exists(m_strPrivateKeyPath))
	{
		m_nStatusCode=1;
		m_strStatusText= QString("Failed to send Apple push notification. PushToken: %1. Error: %2").arg(m_strPushToken1).arg("SSL certificates for Apple push notification can not be located");
		m_ThreadSync->ThreadRelease();
		return;
	}

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,QString("Apple Push: create push msg, badge number %1").arg(m_nBadgeNumber));

	m_pSSLSocket = new QSslSocket();
	//connect(m_pSSLSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),this, SLOT(socketStateChanged(QAbstractSocket::SocketState)));
	connect(m_pSSLSocket, SIGNAL(encrypted()),this, SLOT(socketEncrypted()));
	connect(m_pSSLSocket, SIGNAL(connected()), this, SLOT(onConnected()));
	connect(m_pSSLSocket, SIGNAL(readyRead()), this, SLOT(readResponse()));
	connect(m_pSSLSocket, SIGNAL(sslErrors(const QList<QSslError>&)),this, SLOT(onSslErrors(const QList<QSslError> &)));
	connect(m_pSSLSocket, SIGNAL(disconnected()), this, SLOT(onSocketClosed()));


	//B.T: this is added in message_notifier constructor...
	QString strPathIssuer=QCoreApplication::applicationDirPath()+"/settings/entrust_2048_ca.cer";
	m_pSSLSocket->addCaCertificates(strPathIssuer);
	m_pSSLSocket->setLocalCertificate(m_strLocalCertificatePath);
	m_pSSLSocket->setPrivateKey(m_strPrivateKeyPath);
	m_pSSLSocket->ignoreSslErrors();
	/*
	QString strCert;
	
	strCert.append("Local Certificate - Organization:");
	strCert.append(m_pSSLSocket->localCertificate().subjectInfo(QSslCertificate::Organization).join("--"));
	strCert.append("Common Name:");
	strCert.append(m_pSSLSocket->localCertificate().subjectInfo(QSslCertificate::CommonName).join("--"));
	strCert.append("Country: ");
	strCert.append(m_pSSLSocket->localCertificate().subjectInfo(QSslCertificate::CountryName).join("--"));
	strCert.append("Expiry Date: ");
	strCert.append(m_pSSLSocket->localCertificate().expiryDate().toString("ddMMyyyy"));
	strCert.append("Issuer name: ");
	strCert.append(m_pSSLSocket->localCertificate().issuerInfo(QSslCertificate::CommonName).join("--"));
	strCert.append("Issuer org: ");
	strCert.append(m_pSSLSocket->localCertificate().issuerInfo(QSslCertificate::Organization).join("--"));

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,strCert);
	*/
	//qDebug() << "local certificate " << m_pSSLSocket->localCertificate().isValid() << m_pSSLSocket->localCertificate().subjectInfo(QSslCertificate::CommonName);
	//qDebug() << "private key " << m_pSSLSocket->privateKey().type() << m_pSSLSocket->privateKey().algorithm() << m_pSSLSocket->privateKey().isNull();

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,QString("Apple Push: trying to connect to the host %1").arg(m_strPushServerURL));

	//make connection, so help us good!
	if (m_bFeedBack)
	{
		m_pSSLSocket->connectToHostEncrypted(m_strFeedBackServerURL, m_nFeedBackServerPort, m_strPushServerURL);
	}
	else
	{
		m_pSSLSocket->connectToHostEncrypted(m_strPushServerURL, m_nPushServerPort, m_strPushServerURL);
	}

	m_nTimerID=startTimer(m_nTimeOut);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,QString("Apple Push: connecting to the host %1, ticking timeout").arg(m_strPushServerURL));

}


void ApplePushNotification::onConnected()
{
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,QString("Apple Push SSL Socket connected, PushToken: %1, thread: %2").arg(m_strPushToken1).arg(ThreadIdentificator::GetCurrentThreadID()));
	ResetTimeOut();
}

void ApplePushNotification::onSocketClosed()
{
	if(m_pSSLSocket)
	{
		m_pSSLSocket->disconnect();
		m_pSSLSocket->deleteLater();
		m_pSSLSocket=NULL;
	}
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,QString("Apple Push SSL Socket Closed, PushToken: %1, thread: %2").arg(m_strPushToken1).arg(ThreadIdentificator::GetCurrentThreadID()));
	m_ThreadSync->ThreadRelease();
}

int ApplePushNotification::parseBytes(int b1, int b2, int b3, int b4) 
{
	return ((b1 << 3 * 8) & 0xFF000000) | ((b2 << 2 * 8) & 0x00FF0000) | ((b3 << 1 * 8) & 0x0000FF00) | ((b4 << 0 * 8) & 0x000000FF);
}

void ApplePushNotification::readResponse()
{

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Apple Push SSL Socket read response");

	if (m_bFeedBack)
	{
		/*
		QByteArray bbb = m_pSSLSocket->readAll();

		qDebug() << "feedback returned bytes" << bbb;
		qDebug() << "feedback returned size" << bbb.size();

		int nMessages = bbb.size()/38;

		for (int i=0; i<nMessages; i++)
		{
			QByteArray deviceFeedback = bbb.mid(i*38, 38*(i+1));

			int timeNotWorking = parseBytes(deviceFeedback[0], deviceFeedback[1], deviceFeedback[2], deviceFeedback[3]);
			int nTokenLength = (deviceFeedback[5] & 0xFF);

			QByteArray tokenByn;
			for (int i=6; i< 38; i++)
			{
				tokenByn.append(deviceFeedback[i]);
			}

			qDebug() << "time not receiving "<< QDateTime::fromTime_t(timeNotWorking);
			qDebug() << "token hex "<<tokenByn.toHex();
			
		}
		*/
	}
	else
	{
		QByteArray bbb = m_pSSLSocket->read(6);
		int nCommand = bbb[0] & 0xFF;
		int nStatusCode = bbb[1] & 0xFF;
		int nIdentifier = qFromBigEndian<qint32>(parseBytes(bbb[2], bbb[3], bbb[4], bbb[5]));
		if (nIdentifier==m_nMessageID && nStatusCode==0)
		{
			nStatusCode=0;
			m_strStatusText="";
		}
		else
		{
			nStatusCode=nStatusCode;
			m_strStatusText=QString("Failed to send Apple push notification. PushToken: %1").arg(m_strPushToken1);
		}
		//m_pSSLSocket->disconnect();
		//m_pSSLSocket->deleteLater();
		//m_pSSLSocket=NULL;
	}
	ResetTimeOut();
	m_ThreadSync->ThreadRelease();
}

void ApplePushNotification::onSslErrors(const QList<QSslError> &error)
{
	m_nStatusCode=1;
	if (error.size()>0)
		m_strStatusText=QString("Failed to send Apple push notification. PushToken: %1. SSL error: %2").arg(m_strPushToken1).arg(error.at(0).errorString());
	if (m_pSSLSocket)
		m_pSSLSocket->disconnect();

	m_ThreadSync->ThreadRelease();
}


void ApplePushNotification::socketEncrypted()
{
	//qDebug()<< "Socket encrypted";

	if (m_bFeedBack)
	{
		/*
		QByteArray bbb = m_pSSLSocket->readAll();
		qDebug() << "feedback returned bytes" << bbb;
		qDebug() << "feedback returned size" << bbb.size();
		int nCommand = bbb[0] & 0xFF;
		int nStatusCode = bbb[1] & 0xFF;
		int nIdentifier = qFromBigEndian<qint32>(parseBytes(bbb[2], bbb[3], bbb[4], bbb[5]));
		*/
	}
	else
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Apple Push start to send Message, PushToken: "+m_strPushToken1);
		APNMessage *apnMessage = new APNMessage(this);
		QStringList dummy;
		apnMessage->sendMessage(m_pSSLSocket, m_nMessageID, m_strPushToken1, m_strMessage, m_nBadgeNumber, m_strPushSoundName, "ACT_KEY_CAPTION", "",dummy,"",m_lstMessageCustomData);
		
		//time out is 20 seconds:
		QTimer::singleShot(20000,this,SLOT(onSocketClosed()));
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Apple Push SSL Message Sent, waiting 20seconds, PushToken: "+m_strPushToken1);
	}
}




void ApplePushNotification::OnTimeOut()
{
	if (m_pSSLSocket)
	{
		if (m_pSSLSocket->state()==QAbstractSocket::UnconnectedState || m_pSSLSocket->state()==QAbstractSocket::HostLookupState || m_pSSLSocket->state()==QAbstractSocket::ConnectingState)
		{
			m_nStatusCode=1;
			m_strStatusText = QString("Failed to send Apple push notification. PushToken: %1. Error: %2").arg(m_strPushToken1).arg("timeout");
			//m_pSSLSocket->disconnect();
			m_ThreadSync->ThreadRelease();
		}
	}
}


void ApplePushNotification::goSocketClose()
{
	if (m_pSSLSocket)
		m_pSSLSocket->disconnect();
}




















int APNMessage::send_payload(QSslSocket *pSSLSocket, int nMessageIdentifier, const char *deviceTokenHex, const char *payloadBuff, size_t payloadLength)
{
	int rtn = 0;

	if (deviceTokenHex && payloadBuff && payloadLength)
	{
		//uint8_t command = 0; /* command number */
		//char binaryMessageBuff[sizeof(uint8_t) + sizeof(uint16_t) + DEVICE_BINARY_SIZE + sizeof(uint16_t) + MAXPAYLOAD_SIZE];

		// message format is, |COMMAND|TOKENLEN|TOKEN|PAYLOADLEN|PAYLOAD|
		/*char *binaryMessagePt = binaryMessageBuff;
		uint16_t networkOrderTokenLength = htons(DEVICE_BINARY_SIZE);
		uint16_t networkOrderPayloadLength = htons(payloadLength);*/
		uint8_t command = 1; /* command number */

		char binaryMessageBuff[sizeof(uint8_t) + sizeof(uint32_t) + sizeof(uint32_t) + sizeof(uint16_t) + DEVICE_BINARY_SIZE + sizeof(uint16_t) + MAXPAYLOAD_SIZE];

		/* message format is, |COMMAND|ID|EXPIRY|TOKENLEN|TOKEN|PAYLOADLEN|PAYLOAD| */
		char *binaryMessagePt = binaryMessageBuff;
		uint32_t whicheverOrderIWantToGetBackInAErrorResponse_ID = nMessageIdentifier;
		uint32_t networkOrderExpiryEpochUTC = htonl(time(NULL)+86400); // expire message if not delivered in 1 day
		uint16_t networkOrderTokenLength = htons(DEVICE_BINARY_SIZE);
		uint16_t networkOrderPayloadLength = htons(payloadLength);

		/* command */
		*binaryMessagePt++ = command;

		/* provider preference ordered ID */
		memcpy(binaryMessagePt, &whicheverOrderIWantToGetBackInAErrorResponse_ID, sizeof(uint32_t));
		binaryMessagePt += sizeof(uint32_t);

		/* expiry date network order */
		memcpy(binaryMessagePt, &networkOrderExpiryEpochUTC, sizeof(uint32_t));
		binaryMessagePt += sizeof(uint32_t);

		/* token length network order */
		memcpy(binaryMessagePt, &networkOrderTokenLength, sizeof(uint16_t));
		binaryMessagePt += sizeof(uint16_t);

		/* Convert the Device Token */
		int i = 0;
		int j = 0;
		int tmpi;
		char tmp[3];
		char deviceTokenBinary[DEVICE_BINARY_SIZE];
		while(i < strlen(deviceTokenHex))
		{
			if(deviceTokenHex[i] == ' ')
			{
				i++;
			}
			else
			{
				tmp[0] = deviceTokenHex[i];
				tmp[1] = deviceTokenHex[i + 1];
				tmp[2] = '\0';

				sscanf(tmp, "%x", &tmpi);
				deviceTokenBinary[j] = tmpi;

				i += 2;
				j++;
			}
		}

		/* device token */
		memcpy(binaryMessagePt, deviceTokenBinary, DEVICE_BINARY_SIZE);
		binaryMessagePt += DEVICE_BINARY_SIZE;

		/* payload length network order */
		memcpy(binaryMessagePt, &networkOrderPayloadLength, sizeof(uint16_t));
		binaryMessagePt += sizeof(uint16_t);

		/* payload */
		memcpy(binaryMessagePt, payloadBuff, payloadLength);
		binaryMessagePt += payloadLength;
		if (pSSLSocket->write(binaryMessageBuff, (binaryMessagePt - binaryMessageBuff)) > 0)
			rtn = 1;
	}

	//ssl_disconnect(sslcon);

	return rtn;
}

APNMessage::APNMessage(QObject *parent)
	: QObject(parent)
{
}

APNMessage::~APNMessage()
{
}

int APNMessage::sendMessage(QSslSocket *pSSLSocket, int nMessageIdentifier, QString strToken, QString strMessage /*= QString()*/, int nBadgeNumber /*= 0*/, QString	strSoundName /*= QString()*/, QString strActionKeyCaption /*= QString()*/, 
								QString strLocKey /*= QString()*/, QStringList lstLocArgs /*= QStringList()*/, QString strLaunchImage /*= QString()*/, QStringList lstCustomVals)
{
	QByteArray bytTmp = strToken.toUtf8();
	const char *m_cpDeviceTokenHex = bytTmp.constData();

	formatMessagePayload(strMessage, nBadgeNumber, strSoundName, strActionKeyCaption, strLocKey, lstLocArgs, strLaunchImage,lstCustomVals);
	return send_payload(pSSLSocket, nMessageIdentifier, m_cpDeviceTokenHex, m_messageBuff, strlen(m_messageBuff));
}

void APNMessage::formatMessagePayload(QString strMessage /*= QString()*/, int nBadgeNumber /*= 0*/, QString	strSoundName /*= QString()*/, QString strActionKeyCaption /*= QString()*/, 
									  QString strLocKey /*= QString()*/, QStringList lstLocArgs /*= QStringList()*/, QString strLaunchImage /*= QString()*/, QStringList lstCustomVals)
{
	//Compose payload.
	char tmpBuff[MAXPAYLOAD_SIZE];
	char badgenumBuff[3];

	//Aps values.
	strcpy(m_messageBuff, "{\"aps\":{");

	bool bSimpleAlert = true;
	bool bMessageHasLocalization = false;
	bool bHasAlert = false;
	if (bSimpleAlert)
	{
		if (!strMessage.isEmpty())
		{
			bHasAlert = true;
			addKeyValuePair(m_messageBuff, "alert", strMessage);
		}

		//strcat(m_messageBuff, "}");
	}
	else
	{
		strcat(m_messageBuff, "\"alert\":{");

		if (bMessageHasLocalization)
		{
			if (!strMessage.isEmpty())
			{
				bHasAlert = true;
				addKeyValuePair(m_messageBuff, "body", strMessage);
			}
		}
		else
		{
			bHasAlert = true;
			addKeyValuePair(m_messageBuff, "action-loc-key", strActionKeyCaption);
			strcat(m_messageBuff, ",");
			addKeyValuePair(m_messageBuff, "loc-args", lstLocArgs);
		}

		if (!strActionKeyCaption.isEmpty())
		{
			bHasAlert = true;
			if ((!strLocKey.isEmpty() || !strMessage.isEmpty()))
				strcat(m_messageBuff, ",");
			
			addKeyValuePair(m_messageBuff, "action-loc-key", strActionKeyCaption);
		}
		
		if (!strLaunchImage.isEmpty())
		{
			bHasAlert = true;
			if ((!strLocKey.isEmpty() || !strMessage.isEmpty() || !strActionKeyCaption.isEmpty()) && !strLaunchImage.isEmpty())
				strcat(m_messageBuff, ",");

			addKeyValuePair(m_messageBuff, "launch-image", strLaunchImage);
		}

		strcat(m_messageBuff, "}");
	}
	
	bool bHasBadge=false;
	if (nBadgeNumber>0)
	{
		bHasBadge=true;
		if (bHasAlert)
			strcat(m_messageBuff, ",");
	
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,QString("Apple Push: sending badge number %1").arg(nBadgeNumber));
		addKeyValuePair(m_messageBuff, "badge", nBadgeNumber);
	}

	if (strSoundName!="nosound")
	{
		if (!strSoundName.isEmpty())
		{
			if (bHasAlert || bHasBadge)
				strcat(m_messageBuff, ",");

			addKeyValuePair(m_messageBuff, "sound", strSoundName);
		}
	}
	
	
	strcat(m_messageBuff, "}");
	
	if(lstCustomVals.size()>0)
	{
		int i=0;
		do 
		{
			strcat(m_messageBuff, ",");
			if ((i+1)<lstCustomVals.size())
			{
				//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("ApplePush: adding custom val %1 - %2").arg(lstCustomVals.at(i)).arg(lstCustomVals.at(i+1)));
				addKeyValuePair(m_messageBuff, lstCustomVals.at(i), lstCustomVals.at(i+1));
			}
			i+=2;
		} while (i<lstCustomVals.size());
	}

	//Custom dictionary values.
/*	int i = 0;
	while(payload->dictKey[i] != NULL && i < 5)
	{
		sprintf(tmpBuff, "\"%s\":\"%s\"", payload->dictKey[i], payload->dictValue[i]);
		strcat(m_messageBuff, tmpBuff);
		if(i < 4 && payload->dictKey[i + 1] != NULL)
		{
			strcat(m_messageBuff, ",");
		}
		i++;
	}*/

	strcat(m_messageBuff, "}");

	qDebug() << "Message buffer" << m_messageBuff;
}

void APNMessage::addKeyValuePair(char *cPayload, QString strKey, QString strValue)
{
	char tmpBuff[TMP_BUFFER_SIZE];

	//const char* cKey = strKey.toStdString().c_str();

	QByteArray bytTmp = strKey.toUtf8();
	const char* cKey = bytTmp.constData();

	QByteArray bytTmp1 = strValue.toUtf8();
	const char* cValue = bytTmp1.constData();

	if ((bytTmp1.size()+bytTmp.size()+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, value=%2").arg(strKey).arg(strValue));
		return;
	}
	sprintf(tmpBuff, "\"%s\":\"%s\"", cKey, cValue);

	if ((strlen(tmpBuff)+strlen(cPayload)+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, value=%2, payload=%3").arg(strKey).arg(strValue).arg(QString(cPayload)));
		return;
	}
	strcat(cPayload, tmpBuff);
}

void APNMessage::addKeyValuePair(char *cPayload, QString strKey, int nValue)
{
	char tmpBuff[TMP_BUFFER_SIZE];

	QByteArray bytTmp = strKey.toUtf8();
	const char* cKey = bytTmp.constData();

	if ((bytTmp.size()+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, value=%2").arg(strKey).arg(nValue));
		return;
	}
	sprintf(tmpBuff, "\"%s\":%d", cKey, nValue);
	if ((strlen(tmpBuff)+strlen(cPayload)+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, value=%2, payload=%3").arg(strKey).arg(nValue).arg(QString(cPayload)));
		return;
	}
	strcat(cPayload, tmpBuff);
}

void APNMessage::addKeyValuePair(char *cPayload, QString strKey, QStringList lstValues)
{
	char tmpBuff[TMP_BUFFER_SIZE];

	QByteArray bytTmp = strKey.toUtf8();
	const char* cKey = bytTmp.constData();

	if ((bytTmp.size()+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, value=%2").arg(strKey).arg(lstValues.join(";")));
		return;
	}
	sprintf(tmpBuff, "\"%s\":[", cKey);
	if ((strlen(tmpBuff)+strlen(cPayload)+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, value=%2, payload=%3").arg(strKey).arg(lstValues.join(";")).arg(QString(cPayload)));
		return;
	}
	strcat(cPayload, tmpBuff);

	const char* dictValues[TMP_BUFFER_SIZE];
	int nLockArgsCount = lstValues.count();
	for (int i = 0; i < nLockArgsCount; i++)
	{
		QByteArray bytTmp1 = lstValues.value(i).toUtf8();
		dictValues[i] = bytTmp1.constData();
	}

	int i = 0;
	while(dictValues[i] != NULL)
	{
		sprintf(tmpBuff, "\"%s\"", dictValues[i]);
		if ((strlen(tmpBuff)+strlen(cPayload)+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, value=%2, payload=%3").arg(strKey).arg(lstValues.join(";")).arg(QString(cPayload)));
			return;
		}
		strcat(cPayload, tmpBuff);
		if(dictValues[i + 1] != NULL)
		{
			strcat(cPayload, ",");
		}
		i++;
	}
	strcat(cPayload, "]");
}

void APNMessage::addKeyValuePair(char *cPayload, QString strKey, QList<int> lstValues)
{
	char tmpBuff[TMP_BUFFER_SIZE];

	QByteArray bytTmp = strKey.toUtf8();
	const char* cKey = bytTmp.constData();

	if ((bytTmp.size()+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1").arg(strKey));
		return;
	}
	sprintf(tmpBuff, "\"%s\":[", cKey);
	if ((strlen(tmpBuff)+strlen(cPayload)+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, payload=%2").arg(strKey).arg(QString(cPayload)));
		return;
	}
	strcat(cPayload, tmpBuff);

	int nlstValuesCount = lstValues.count();
	for (int i = 0; i < nlstValuesCount; i++)
	{
		sprintf(tmpBuff, "%d", lstValues[i]);
		if ((strlen(tmpBuff)+strlen(cPayload)+10)>MAXPAYLOAD_SIZE)	//B.T. never out of > MAXPAYLOAD_SIZE
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_GENERAL,QString("Apple Push: incoming data is too large for push message: key=%1, payload=%2").arg(strKey).arg(QString(cPayload)));
			return;
		}
		strcat(cPayload, tmpBuff);
		if((i + 1) < nlstValuesCount)
		{
			strcat(cPayload, ",");
		}
	}
	strcat(cPayload, "]");
}
