#include "ipkicker.h"



IPKicker::IPKicker()
{
	m_bChanged=false;
}





/*------------------------------------------------------------------------*/
/*								  IP BLACK LIST							  */
/*------------------------------------------------------------------------*/


void IPKicker::Initialize(int nAfterCounterKickTime)
{
	QMutexLocker locker(&m_Mutex);
	m_IPDynamicAccessList.Initialize(nAfterCounterKickTime);
}

bool IPKicker::IsIPAllowed(QString strIP)
{
	QMutexLocker locker(&m_Mutex);
	return (m_IPAccessList.IsIPAllowed(strIP) && m_IPDynamicAccessList.IsIPAllowed(strIP));
}



void IPKicker::KickBanIP(QString strIP,int nBanTime,int nBanCounter)
{
	QMutexLocker locker(&m_Mutex);
	m_bChanged=true;
	m_IPDynamicAccessList.AddEntry(strIP,0,false,-1,nBanTime,nBanCounter);
}


void IPKicker::SetStaticList(DbRecordSet &lstAcess)
{
	QMutexLocker locker(&m_Mutex);
	m_IPAccessList.LoadIPAccessList(lstAcess);
}
void IPKicker::GetStaticList(DbRecordSet &lstAcess)
{
	QMutexLocker locker(&m_Mutex);
	m_IPAccessList.GetIPAccessList(lstAcess);
}


void IPKicker::SetDynamicList(DbRecordSet &lstAcess)
{
	QMutexLocker locker(&m_Mutex);
	m_IPDynamicAccessList.LoadIPAccessList(lstAcess);
}
void IPKicker::GetDynamicList(DbRecordSet &lstAcess)
{
	QMutexLocker locker(&m_Mutex);
	m_IPDynamicAccessList.GetIPAccessList(lstAcess);
}
