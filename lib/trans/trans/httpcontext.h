#ifndef TCPCONTEXT_H
#define TCPCONTEXT_H

#include <QString>

/*!
\class  HTTPContext
\ingroup HTTPModule
\brief  This class provides some context information for some client/server call

HTTPContext stores socket number and IP adress to provide context to upper level packet handler 
*/
class HTTPContext
{
public:

	HTTPContext():m_nSocketID(-1){};

	int m_nSocketID;
    QString	m_strPeerIP;
	int	m_nPeerPort;
	QString	m_strPeerName;

};

#endif // TCPCONTEXT_H

