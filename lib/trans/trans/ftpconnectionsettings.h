#ifndef FTPCONNECTIONSETTINGS_H
#define FTPCONNECTIONSETTINGS_H


#include <QString>
#include <QNetworkProxy>

/*!
	\class FTPConnectionSettings
	\brief Data placehoder for client connection settings, use by HTTPClientConnection to establish connection to server
	\ingroup FTPClient
*/
class FTPConnectionSettings 
{


public:
	FTPConnectionSettings():
			m_strServerIP(""),
			m_nPort(21),
			m_bUseProxy(false),
			m_bPassive(true)
	  {};

	  QString m_strFTPUserName;	///< custom name
	  QString m_strFTPPassword;	///< custom name
	  QString m_strCustomName;	///< custom name
	  QString m_strServerIP;	///< server IP
	  QString m_strStartDirectory;	///< ftp working directory
	  int m_nPort;				///< server port
	  bool m_bPassive;			///< passive mode

	  //PROXY:
	  bool m_bUseProxy;			///< use Proxy
	  QString m_strProxyUsername;
	  QString m_strProxyPassWord;
	  QString m_strProxyHostName;
	  int m_nProxyPort;
	  int m_nProxyType;

	  //return proxy object
	  QNetworkProxy GetProxySettings()
	  {
		  QNetworkProxy ProxySettings;	///< proxy host,ip,type, password, username, just to ease initialization
		  //fill proxy object:
		  ProxySettings.setType((QNetworkProxy::ProxyType)m_nProxyType);
		  ProxySettings.setHostName(m_strProxyHostName);
		  ProxySettings.setPort(m_nProxyPort);
		  ProxySettings.setUser(m_strProxyUsername);
		  ProxySettings.setPassword(m_strProxyPassWord);
		  return ProxySettings;
	  }

};


#endif // FTPCONNECTIONSETTINGS_H
