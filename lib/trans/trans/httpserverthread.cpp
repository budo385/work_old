#include "httpserverthread.h"
#include "common/common/loggerabstract.h"
#include "httputil.h"
#include "trans/trans/config_trans.h"
#include "common/common/threadid.h"

//quint64 nThreadsActive=0;

/*!
	Constructor: create socket parent object
*/
HTTPServerThread::HTTPServerThread(HTTPServer *pServer, int nSocketID, HTTPServerConnectionSettings &ConnSettings,QList<HTTPHandler *> lstHandlers, IPKicker* pIPAddressTest, int nDefaultConnectionAttemptsAllowed)
:m_pSocketHandler(NULL)
{
	m_pServer=pServer;
	m_bClosing=false;
	m_bIPKicked=false;
	m_pIPAddressTest=pIPAddressTest;
	m_nSocketID=nSocketID; 
	m_ConnSettings=ConnSettings;
	m_nDefaultConnectionAttemptsAllowed=nDefaultConnectionAttemptsAllowed;
	m_pLstHandlers=lstHandlers;

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Socket thread: creating, thread %1, socket: %2").arg(ThreadIdentificator::GetCurrentThreadID()).arg(m_nSocketID));
	/*
	m_lck.lockForWrite();
	nThreadsActive++;
	m_lck.unlock();
	*/
}


/*!

*/

//BT little change core:
HTTPServerThread::~HTTPServerThread()
{
	/*
	m_lck.lockForWrite();
	nThreadsActive--;
	qDebug()<<"still active threads: "<<nThreadsActive;
	m_lck.unlock()
	*/
}


/*!
	Socket thread main loop
*/
void HTTPServerThread::run()
{
	if (m_pIPAddressTest) //if NULL then IP testing is not available
	{
		if (!m_pIPAddressTest->IsIPAllowed(m_pSocketHandler->GetConnectionContext().m_strPeerIP))
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SECURITY_IP_BANNED,m_pSocketHandler->GetConnectionContext().m_strPeerIP);
			deleteLater(); //delete us: thread loop is not even running
			return; //do not start thread: just exit!!
		}
	}

	m_pSocketHandler = new HTTPServerSocketHandler(m_nSocketID,m_ConnSettings.m_bUseSSL);
	m_pSocketHandler->SetRequestHandlers(m_pLstHandlers);
	m_nSocketThreadID = ThreadIdentificator::GetCurrentThreadID();
	m_pServer->OnThreadCreated(m_nSocketThreadID,this); //register back this thread to server: 

	//connect main & this worker thread with signal/slot technology:
	connect(this,SIGNAL(SocketDestroy()),m_pSocketHandler,SLOT(SocketDestroy()),Qt::QueuedConnection);
	connect(this,SIGNAL(SocketSendServerMessage(QByteArray)),m_pSocketHandler,SLOT(SocketWrite(QByteArray)),Qt::QueuedConnection);
	connect(m_pSocketHandler,SIGNAL(KickBanIP(QString,int,int)),this,SLOT(OnKickBanIP(QString,int,int)),Qt::QueuedConnection);
	connect(m_pSocketHandler,SIGNAL(DestroyThread()),this,SLOT(ExecuteDestroyThread()),Qt::QueuedConnection); //destroy all

	exec(); //start event loop
}

/*!
	Send request to the client, in ASYNC manner: write and go!
	/param pStatus, 0 if OK
	/param pHTTPRequest client request

*/
void HTTPServerThread::SendData(Status& pStatus, HTTPRequest* pHTTPRequest)
{
	if (m_bClosing)
		return;

	QByteArray pBuffer;
	if (m_ConnSettings.m_bUseCompression)
		pHTTPRequest->setValue("content-encoding","deflate");
	pHTTPRequest->GetData(pBuffer);
	emit SocketSendServerMessage(pBuffer);	//emit signal that request is ready for send
}

void HTTPServerThread::SendDataBroadCast(Status& pStatus, QByteArray* pBroadCastMsgBuffer)
{
	if (m_bClosing)
		return;

	emit SocketSendServerMessage(*pBroadCastMsgBuffer);	//emit signal that request is ready for send
}


//BT: DO NOT DESTROY socket with delete() or explicit, use this API!!!-> as socket can be destroyed by itself and by garbage collector
//BT: exception: SERVER CLOSE: need to close all sockets before global objects are dead...
void HTTPServerThread::ExecuteDestroyThread()
{
	m_pSocketHandler=NULL;
	if (!m_bClosing) 
	{
		m_bClosing=true; 
		m_pServer->OnThreadDestroyed(m_nSocketThreadID);
	}

	if(isRunning())
	{
		quit();
		bool bOK=wait(1);		//wait 1msec
		if (!bOK)
		{
			QTimer::singleShot(0,this,SLOT(ExecuteDestroyThread())); 
		}
		else
			delete this;
	}
	else
		delete this;
}

void HTTPServerThread::OnKickBanIP(QString strIP,int nBanTime,int nBanCounter)
{
	if (m_bIPKicked)
		return;

	if (m_pIPAddressTest)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SECURITY_IP_KICKED,strIP);
		if (nBanTime==0 && nBanCounter==0)
			nBanCounter=m_nDefaultConnectionAttemptsAllowed;
		m_pIPAddressTest->KickBanIP(strIP,nBanTime,nBanCounter);
		m_bIPKicked=true;
	}

}

HTTPContext HTTPServerThread::GetConnectionContext()
{
	if (m_pSocketHandler)
		return m_pSocketHandler->GetConnectionContext();
	else
		return HTTPContext();
}

Status HTTPServerThread::GetLastError()
{
	if (m_pSocketHandler)
		return m_pSocketHandler->GetLastError();
	else
		return Status();
}

int HTTPServerThread::GetSocketState() 
{
	if (m_pSocketHandler)
		return m_pSocketHandler->GetSocketState();
	else
		return HTTPServerSocketHandler::STATE_NOT_CONNECTED;
}

QDateTime HTTPServerThread::GetSocketCreatedTime()
{
	if (m_pSocketHandler)
		return m_pSocketHandler->m_datCreated;
	else
		return QDateTime();
}
int	HTTPServerThread::GetOrphanSocketTimeOut()
{
	if (m_pSocketHandler)
		return m_pSocketHandler->m_nOrphanSocketTimeOut;
	else
		return 0;
}

