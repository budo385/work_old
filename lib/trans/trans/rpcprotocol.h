#ifndef RPCPROTOCOL_H
#define RPCPROTOCOL_H

#include "trans/trans/config_trans.h"
#include "rpcmessage.h"

//RPC PROTOCOL ID's:
#define RPC_PROTOCOL_TYPE_XML_RPC 0	
#define	RPC_PROTOCOL_TYPE_SOAP 1
#define RPC_PROTOCOL_TYPE_REST 2

/*!
    \class RpcProtocol
    \brief Abstract RPC class, inherited by stubs & skeletons
    \ingroup RPCModule

	Defines type of used RPCMessage protocol. Based on init parameter.
	can be: RPC_PROTOCOL_TYPE_XML_RPC, RPC_PROTOCOL_TYPE_SOAP, etc..
*/

class RpcProtocol
{
	friend class RpcSkeletonMessageHandler;	//class must have access to the GetRPCMessageHandler method
	friend class RpcStubMessageHandler;		//class must have access to the GetRPCMessageHandler method
	friend class RpcHttpHeader;				//class must have access to the GetRPCDomain method	

public:
	RpcProtocol(int pNumRPCProtocolType){m_nRPCType=pNumRPCProtocolType;};
	~RpcProtocol(){};

protected:

	void GetRPCMessageHandler(RpcMessage **m_RPCMessage);	//used by children to get right RPC message type
	QString GetRPCDomain();									//used by children to get right RPC domain (used in HTTP headers)
	//virtual void Init()=0;									//fake function to make this class abstract

	int m_nRPCType;							//< Type of XML based RPC protocol in use: XML-RPC,SOAP,etc..

};



#endif //RPCPROTOCOL_H
