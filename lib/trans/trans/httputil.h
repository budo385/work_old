#ifndef HTTPUTIL_H
#define HTTPUTIL_H	


#include "qftp.h"

#include "common/common/status.h"
#include "httprequest.h"
#include "httpresponse.h"
#include "httphandler.h"
#include "httpclientconnectionsettings.h"



/*!
    \class HTTPUtil
    \brief common util methods for HTTP operations. Use static member functions.
    \ingroup HTTPModule
*/
class HTTPUtil
{
public:

static bool				CheckHTTPHeader(const QByteArray& pIncomingBuffer, int& pHTTPType, qint64 &nContentLength, int &nHdrEnd);
static bool				CheckHTTPEndOfMessage(QByteArray& pIncomingBuffer, qint64 nContentLength);
static bool				ExtractHTTPChunk(QByteArray& pIncomingBuffer, QByteArray& pChunk);

static void				GetDomain(const QString &strPath, QString& strDomain);
static void				GetDomain(const HTTPRequest& pHTTPRequest, QString& strDomain);
static HTTPHandler *	GetRequestHandler(QList<HTTPHandler *> &lstHandlers,HTTPRequest& pRequest);
static HTTPHandler *	GetRequestHandler(QList<HTTPHandler *> &lstHandlers,const QString &strPath);
static void				MapSocketErrors(Status& pStatus, QAbstractSocket::SocketError errSocket,QString strSocketErrorDesc="");
static void				MapFTPSocketErrors(Status& pStatus, QFtp::Error errSocket);
static void				SetHeaderDataBasedOnFile(QHttpHeader *header, QString strFileName,bool bAddContentDisposition=true,int nDeflateIfAboveByte=0);
static bool				ExtractFileInfoFromContentDisposition(QString strContentDisposition,QString& strFileName, qint64& nFileSize,QString &strFormFieldName);


/*! HTTP Header Type defines type of receivied HTTP message */
	enum HTTPHeaderType {
			HTTP_HEADER_NOTCOMPLETE, ///<Header not completed
			HTTP_HEADER_REQUEST,	 ///<Header is request
			HTTP_HEADER_RESPONSE,	 ///<Header is respone
			HTTP_HEADER_INVALID		 ///<Header is damaged
		};



};



#endif //HTTPUTIL_H
