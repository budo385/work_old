#ifndef ANDROIDPUSHNOTIFICATION_H
#define ANDROIDPUSHNOTIFICATION_H

#include "qhttp.h"
#include <QSslError>
#include "pushnotification.h"

class AndroidPushNotification : public PushNotification
{
	Q_OBJECT

public:
	AndroidPushNotification(ThreadSynchronizer *sync,int nTimeOut=10000);
	~AndroidPushNotification();

	static	QString m_strGooglePushServerURL;
	static	QString m_strGoogleAuthenticationID;


public slots:
	void SendPushMessage(); //async method
	void Destroy();

private slots:
	void onRequestDone(bool bError);
	void onSslErrors(const QList<QSslError> &error);

private:
	QString JsonEncodeRecipients(const QStringList &lstRecipients);
	void OnTimeOut();
	QHttp *m_pHttp;
};

#endif // ANDROIDPUSHNOTIFICATION_H
