#include "httpfilestreamer.h"
#include <QFileInfo>

#include "trans/trans/config_trans.h"
#include "common/common/zipcompression.h"
#include "httputil.h"


HTTPFileStreamer::HTTPFileStreamer()
: HTTPStreamHandler(),m_bCompressionActive(false),m_nSetChunkCnt(0),m_nGetChunkCnt(0)
{

}

HTTPFileStreamer::~HTTPFileStreamer()
{
	Close();
	//qDebug()<<"Chunked:"<<"closed";
}

void HTTPFileStreamer::SetRequestHeader(const QHttpRequestHeader &header)
{
	m_reguest_header=header;
	if (m_reguest_header.value("content-encoding")=="deflate")
		m_bCompressionActive=true;

	m_nBytesTotal=0;
	QString strFileName,strFormName;
	HTTPUtil::ExtractFileInfoFromContentDisposition(header.value("Content-Disposition"),strFileName,m_nBytesTotal,strFormName);
}
void HTTPFileStreamer::SetResponseHeader(const QHttpResponseHeader &header)
{
	if (m_nSetChunkCnt>=1) //already set, so skip
		return;

	m_response_header=header;
	if (m_response_header.value("content-encoding")=="deflate")
		m_bCompressionActive=true;

	m_nBytesTotal=0;
	QString strFileName,strFormName;
	HTTPUtil::ExtractFileInfoFromContentDisposition(header.value("Content-Disposition"),strFileName,m_nBytesTotal,strFormName);
}



// read data by chunk, base64, zip if needed!???? how do I know?...then sent in format:
/*
ChunkSize\r\n
Chunk\r\n
*/
void HTTPFileStreamer::GetChunk(Status &pStatus, QByteArray &data,bool bInsertResponseHeaderInFirstChunk,bool bInsertRequestHeaderInFirstChunk)
{
	pStatus.setError(0);
	m_LastError=pStatus;

	//send only request header as first chunk if insert request is enabled: give client a chance to detect and abort if server rejects request:
	if (bInsertRequestHeaderInFirstChunk && m_nGetChunkCnt==0)
	{
		data=m_reguest_header.toString().toLatin1();
		m_nGetChunkCnt++;
		return;
	}

	if (!m_file.isOpen())
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Operation aborted. File handler is not open!"));
		m_LastError=pStatus;
		return;
	}
	
	if (m_file.bytesAvailable()==0)
	{
		data.clear();
		//data+="0\r\n"; //end of stream!!! -> additional CRLF
		return;
	}

	data=m_file.read(HTTP_FILE_CHUNK_SIZE);
	m_nBytesProcessed+=data.size();

	QByteArray zipped;
	//data=data.toBase64();
	if (m_bCompressionActive)
	{
		//qDebug()<<"compressing";
		ZipCompression::Deflate(data,zipped,HTTP_COMPRESSION_LEVEL);
		data=zipped;
		zipped.clear();
	}

	data=QString::number(data.size(), 16).toLatin1()+"\r\n"+data+"\r\n"; 

	if (m_file.bytesAvailable()==0)
	{
		data+="0  \r\n\r\n"; //end of stream!!!
		//Close(); //close file handler at last chunk
	}

	if (m_nGetChunkCnt==0 )
	{
		if(bInsertResponseHeaderInFirstChunk)		//add header at first getchunk:
			data=m_response_header.toString().toLatin1()+data;
		//else if (bInsertRequestHeaderInFirstChunk)	//add header at first getchunk:
		//	data=m_reguest_header.toString().toLatin1()+data;
	}

	
	m_nGetChunkCnt++;
	//qDebug()<<"Chunked:"<<m_nGetChunkCnt;
}


//data: is already chunk!: no size, no strip..as is!
// data is base64 encoded binary data: if header contains deflate: unzip first, then...
void HTTPFileStreamer::SetChunk(Status &pStatus, const QByteArray &data,bool bEof)
{
	pStatus.setError(0);
	m_LastError=pStatus;
	if (!m_file.isOpen())
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Operation aborted. File handler is not open!"));
		m_LastError=pStatus;
		return;
	}

	//QByteArray chunk=QByteArray::fromBase64(data);
	QByteArray chunk=data;
	if (m_bCompressionActive)
	{
		//qDebug()<<"decompressing";
		ZipCompression::Inflate(data,chunk);
	}
	m_nBytesProcessed+=chunk.size();

	if(m_file.write(chunk)==-1)
	{
		QFileInfo info(m_file.fileName());
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Error writing to file: %1!").arg(info.fileName()));
		m_LastError=pStatus;
		return;
	}
	m_nSetChunkCnt++;
}


void HTTPFileStreamer::InitFileOperation(Status &pStatus, QString strFileName, bool bWrite, bool bOverWrite)
{
	pStatus.setError(0);
	m_LastError=pStatus;
	m_nSetChunkCnt=0;
	m_nGetChunkCnt=0;
	m_nBytesProcessed=0;
	m_nBytesTotal=0;

	if(m_file.isOpen())
		m_file.close();

	m_file.setFileName(strFileName);
	if (m_file.exists() && bOverWrite==false && bWrite==true)
	{
		QFileInfo info(strFileName);
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Operation aborted. File %1 already exists!").arg(info.fileName()));
		m_LastError=pStatus;
		return;
	}
	if (!m_file.exists() && bWrite==false)
	{
		QFileInfo info(strFileName);
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Operation aborted. File %1 does not exists!").arg(info.fileName()));
		m_LastError=pStatus;
		return;
	}

	bool bOK=false;
	if (bWrite)
		bOK=m_file.open(QIODevice::WriteOnly);
	else
		bOK=m_file.open(QIODevice::ReadOnly);

	if (!bOK)
	{
		QFileInfo info(strFileName);
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Operation aborted. Failed to open file %1").arg(info.fileName()));
		m_LastError=pStatus;
		return;
	}

	return;

}
void HTTPFileStreamer::Close()
{
	if(m_file.isOpen())
		m_file.close();
}