#ifndef XMLRPCVALUE_H
#define XMLRPCVALUE_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"


	#define VALUE_TAG		"<value>"
	#define VALUE_ETAG      "</value>"
	#define BOOLEAN_TAG		"<boolean>"
	#define BOOLEAN_ETAG	"</boolean>"
	#define DOUBLE_TAG		"<double>"
	#define DOUBLE_ETAG		"</double>"
	#define I4_TAG			"<i4>"
	#define I4_ETAG			"</i4>"
	#define STRING_TAG		"<string>"
	#define STRING_ETAG		"</string>"
	#define DATETIME_TAG	"<dateTime.iso8601>"
	#define DATETIME_ETAG	"</dateTime.iso8601>"
	#define BASE64_TAG		"<base64>"
	#define BASE64_ETAG		"</base64>"

	#define ARRAY_TAG		"<array>"
	#define DATA_TAG		"<data>"
	#define DATA_ETAG		"</data>"
	#define ARRAY_ETAG		"</array>"

	#define STRUCT_TAG		"<struct>"
	#define MEMBER_TAG		"<member>"
	#define NAME_TAG		"<name>"
	#define NAME_ETAG		"</name>"
	#define MEMBER_ETAG		"</member>"
	#define STRUCT_ETAG		"</struct>"

	/*! NULL VALUE*/
	#define NULL_VALUE		"<nil/>"

	#define FAULT_TAG		"<fault>"
	#define FAULT_ETAG		"</fault>"
	#define FAULTCODE		"faultCode"
	#define FAULTSTRING		"faultString"


	#define DATA_TAG_LEN	6
	#define VALUE_ETAG_LEN	8
	#define ARRAY_ETAG_LEN	8
	#define I4_TAG_LEN	4
	#define I4_ETAG_LEN	5
	#define DATETIME_TAG_LEN	18
	#define DATETIME_ETAG_LEN	19
	#define DOUBLE_TAG_LEN	8
	#define DOUBLE_ETAG_LEN	9
	#define BOOLEAN_TAG_LEN		9
	#define BOOLEAN_ETAG_LEN	10
	#define BASE64_TAG_LEN		8
	#define BASE64_ETAG_LEN		9
	#define STRUCT_TAG_LEN		8
	#define STRUCT_ETAG_LEN		9
	#define MEMBER_ETAG_LEN     9
	#define	STRUCT_ETAG_LEN		9






/*!
    \class XmlRpcValueAbstract
    \brief abstract class for XMLRPC value, interface for Serialize/DeSerialize string to/from XML
    \ingroup XMLRPCModule

*/
class XmlRpcValueAbstract
{ 
public:
	XmlRpcValueAbstract():m_byteXMLMsg(NULL){}; //init buffer to NULL

	/*! serialize to/from XML */
	//void Serialize(); // warning: this is only to define interface (cant use abstract)
	//void DeSerialize(); // warning: this is only to define interface (cant use abstract)

	/*! buffer */
	void SetBuffer(QByteArray *pBuffer){m_byteXMLMsg=pBuffer;};
	void ClearData(){m_byteXMLMsg->clear();}

protected:
	QByteArray *m_byteXMLMsg;

};




/*!
    \class XmlRpcValueString
    \brief for Serialize/DeSerialize string to/from XML
    \ingroup XMLRPCModule

	Operates on external buffer to avoid string copy overhead
	Converts string to xml with escape char encoding (<,>,',",&)+ UTF8
*/
class XmlRpcValueString: public XmlRpcValueAbstract
{ 
public:
	/*! serialize to/from XML */
	void Serialize(QString* pValue);
	void Serialize(QVariant* pValue); //overloaded to avoid copy when fetched from recrodset
	void DeSerialize(int &nStartOffset, int nLength, QString* pValue,Status& pStatus);
};



/*!
    \class XmlRpcValueBool
    \brief for Serialize/DeSerialize boolean to/from XML
    \ingroup XMLRPCModule
*/
class XmlRpcValueBool: public XmlRpcValueAbstract
{ 
public:
	/*! serialize to/from XML */
	void Serialize(QVariant* pValue);
	void DeSerialize(int &nStartOffset, int nLength, QVariant* pValue,Status& pStatus);
};


/*!
    \class XmlRpcValueInteger
    \brief for Serialize/DeSerialize integer to/from XML
    \ingroup XMLRPCModule
*/
class XmlRpcValueInteger: public XmlRpcValueAbstract
{ 
public:
	/*! serialize to/from XML */
	void Serialize(QVariant* pValue);
	void DeSerialize(int &nStartOffset, int nLength, QVariant* pValue,Status& pStatus);
};

/*!
    \class XmlRpcValueDouble
    \brief for Serialize/DeSerialize double to/from XML
    \ingroup XMLRPCModule
*/
class XmlRpcValueDouble: public XmlRpcValueAbstract
{ 
public:
	/*! serialize to/from XML */
	void Serialize(QVariant* pValue);
	void DeSerialize(int &nStartOffset, int nLength, QVariant* pValue,Status& pStatus);
};


/*!
    \class XmlRpcValueDateTime
    \brief for Serialize/DeSerialize Date, Time and DateTime to/from XML
    \ingroup XMLRPCModule

	All three types are encoded in same TAG so there is no need to separate object, 
	based on input type information is filtered (Datetime is always transfered).
*/
class XmlRpcValueDateTime: public XmlRpcValueAbstract
{ 
public:
	XmlRpcValueDateTime():m_nTimeZoneOffsetSeconds(0){};
	void SetTimeZoneOffset(int nTimeZoneOffsetSeconds){m_nTimeZoneOffsetSeconds=nTimeZoneOffsetSeconds;};

	/*! serialize to/from XML */
	void Serialize(QDateTime* pValue);
	void Serialize(QDate* pValue);
	void Serialize(QTime* pValue);
	void Serialize(QVariant* pValue); //overloaded to avoid copy when fetched from recrodset

	void DeSerialize(int &nStartOffset, int nLength, QDateTime* pValue,Status& pStatus);
	void DeSerialize(int &nStartOffset, int nLength, QDate* pValue,Status& pStatus);
	void DeSerialize(int &nStartOffset, int nLength, QTime* pValue,Status& pStatus);

private:
	int m_nTimeZoneOffsetSeconds;
};



/*!
    \class XmlRpcValueBinary
    \brief for Serialize/DeSerialize binary data to/from XML
    \ingroup XMLRPCModule
*/
class XmlRpcValueBinary: public XmlRpcValueAbstract
{ 
public:
	/*! serialize to/from XML */
	void Serialize(QByteArray* pValue);
	void Serialize(QVariant* pValue); //overloaded to avoid copy when fetched from recrodset
	void DeSerialize(int &nStartOffset, int nLength, QByteArray* pValue,Status& pStatus);
};



/*!
    \class XmlRpcValueFault
    \brief for Serialize/DeSerialize error codes to/from XML
    \ingroup XMLRPCModule
*/
class XmlRpcValueFault: public XmlRpcValueAbstract
{ 
public:
	/*! serialize to/from XML */
	void Serialize(int nErrorCode, QString strErrorText);
	void DeSerialize(int &nErrorCode, QString &strErrorText);
	/*! buffer */
	void SetBuffer(QByteArray *pBuffer);

private:
	XmlRpcValueString xmlValString;
	XmlRpcValueInteger xmlValInteger;
};




/*!
    \class XmlRpcValueDbRecordSet
    \brief for Serialize/DeSerialize RecordSet to/from XML
    \ingroup XMLRPCModule
*/
class XmlRpcValueDbRecordSet: public XmlRpcValueAbstract
{ 
public:
	XmlRpcValueDbRecordSet():m_nTimeZoneOffsetSeconds(0){xmlValDateTime.SetTimeZoneOffset(0);};

	void SetTimeZoneOffset(int nTimeZoneOffsetSeconds)
		{m_nTimeZoneOffsetSeconds = nTimeZoneOffsetSeconds;xmlValDateTime.SetTimeZoneOffset(nTimeZoneOffsetSeconds);};


	/*! serialize to/from XML */
	void Serialize(DbRecordSet* pValue,bool pSkip=false);
	//void Serialize(QVariant* pValue,bool pSkip=false); //overloaded to avoid copy when fetched from recrodset
	void DeSerialize(int &nStartOffset, int nLength, DbRecordSet* pValue,Status& pStatus,bool pSkip=false);

	void SerializeFast(DbRecordSet* pValue);
	int DeSerializeFast(int &nStartOffset, int nLength, DbRecordSet* pValue,Status& pStatus,int nPreviousColumnDefinitonLength=0,DbRecordSet *pListDef=NULL);


	/*! buffer */
	void SetBuffer(QByteArray *pBuffer);

private:
	int m_nTimeZoneOffsetSeconds;

private:
	XmlRpcValueString xmlValString;
	XmlRpcValueBool xmlValBool;
	XmlRpcValueInteger xmlValInteger;
	XmlRpcValueDouble xmlValDouble;
	XmlRpcValueDateTime xmlValDateTime;
	XmlRpcValueBinary xmlValBinary;
};




#endif //XMLRPCVALUE_H



