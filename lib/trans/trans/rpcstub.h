#ifndef RPCSTUB_H
#define RPCSTUB_H

#include "rpcprotocol.h"

/*!
    \class RpcStub
    \brief Abstract stub class, type of stub is based on type of Rpc protocol in use
    \ingroup RPCModule

	Type of stub is defined by protocol type given in constructor and RPC message handler.
	Stub uses RPC message handler to compile RPC message and ConnectionHandler to send RPC message to server
	Use:
	- when creating new skeleton object, inherit this one and initialize its constructor with 
	RPCProtocolType. Based on this different RPC message will be in use
	
*/

class RpcStub : public RpcProtocol
{
public:
	RpcStub(int pNumRPCProtocolType):RpcProtocol(pNumRPCProtocolType){}; //m_Connection=connHandler;};
	virtual ~RpcStub(){}; //to properly enable destruction in virtual stubber !!

};


#endif //RPCSTUB_H
