#ifndef HTTPCLIENTCONNECTIONSETTINGS_H
#define HTTPCLIENTCONNECTIONSETTINGS_H

#include <QNetworkProxy>

/*!
    \class HTTPClientConnectionSettings
    \brief Data placehoder for client connection settings, use by HTTPClientConnection to establish connection to server
    \ingroup HTTPClient
*/
class HTTPClientConnectionSettings {


public:
	HTTPClientConnectionSettings():
		m_strServerIP(""),
		m_nPort(9999),
		m_bUseSSL(true),
		m_bUseCompression(true),
		m_nDatabaseID(0),			//90 seconds (m_nDefaultTimeOut)
		m_bUseProxy(false),
		m_strLookupURL(""),
		m_bUseDFO(true),
		m_nConnectTimeOut(20)
		{};
	~HTTPClientConnectionSettings(){};

	bool operator <(const HTTPClientConnectionSettings &that) const //B.T. for sorting aplhabetically
	{
		return QString::localeAwareCompare(m_strCustomName,that.m_strCustomName)<0;
	}



	QString m_strCustomName;	///< custom name

	QString m_strServerIP;		///< server IP
	int m_nPort;				///< server port

	bool m_bUseSSL;				///< use SSL
	bool m_bUseCompression;		///< use Compression
	int m_nDatabaseID;		///< default time out for all operations	 (in seconds)

	//PROXY:
	bool m_bUseProxy;			///< use Proxy
	QString m_strProxyUsername;
	QString m_strProxyPassWord;
	QString m_strProxyHostName;
	int m_nProxyPort;
	int m_nProxyType;
	int m_nConnectTimeOut;


	//Special fetures for LoadBalancing & DFO
	QString m_strLookupURL;		///< URL containg app. server IP address for DFO process
	bool m_bUseDFO;				///< use DFO error handler process at client in case of connection problems


	//return proxy object
	QNetworkProxy GetProxySettings()
	{
		QNetworkProxy ProxySettings;	///< proxy host,ip,type, password, username, just to ease initialization
		//fill proxy object:
		ProxySettings.setType((QNetworkProxy::ProxyType)m_nProxyType);
		ProxySettings.setHostName(m_strProxyHostName);
		ProxySettings.setPort(m_nProxyPort);
		ProxySettings.setUser(m_strProxyUsername);
		ProxySettings.setPassword(m_strProxyPassWord);
		return ProxySettings;
	}


};



#endif //HTTPCLIENTCONNECTIONSETTINGS_H
