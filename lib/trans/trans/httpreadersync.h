#ifndef HTTPREADERSYNC_H
#define HTTPREADERSYNC_H

#include <QThread>
#include "common/common/threadsync.h"
#include "httpreader.h"

/*!
	\class HttpReaderSync
	\brief HTTP reader for synchronized requests
	\ingroup HTTPClient

	Because QHttp object works in async mode, there is a need to read out content right away (sync)
*/

class HttpReaderSync : public QThread
{
	Q_OBJECT

public:
    HttpReaderSync(int nTimeOut=20000);
    ~HttpReaderSync();
	HttpReaderSync(const HttpReaderSync &that);
	HttpReaderSync &operator =(const HttpReaderSync &that); 

	bool ReadWebContent(QString pUrl, QByteArray *pContent,bool bSSL=false,int nPort=0);
	bool PostWebContent(QString pUrl, QByteArray pContentPost, QByteArray *pContentResponse,bool bSSL=false,int nPort=0);
	
signals:
	void ReadURL(QString page, bool, int);
	void PostData(QString,QByteArray,bool,int);
	void Destroy();
	void ReEmitAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth);
	void ReadInProgress(int,int);

private:
	void run();

	HttpReader *m_HttpReader;
	int m_nTimeOut;
	ThreadSynchronizer m_ThreadSync;///< for syncing between mastr & slave thread

    
};

#endif // HTTPREADERSYNC_H

