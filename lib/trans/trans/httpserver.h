 
#ifndef QTESTSRV_H
#define QTESTSRV_H

#include <QtNetwork>
#include <QTcpServer>
#include "httphandler.h"
#include "httprequest.h"
#include "ipkicker.h"
#include "httpserverthread.h"
#include "common/common/dbrecordset.h"

class HTTPServerThread;
typedef QHash<int,HTTPServerThread*> ClientConnections;
typedef QHashIterator<int,HTTPServerThread*> ClientConnectionsIterator;


/*!
\class  HTTPServer
\ingroup HTTPServer
\brief  Multithreaded HTTP server, dispatches HTTP requestes to registered handlers

Supports using SSL, client connections are kept alive, enables async sending of msg to the clients
Use:
	StartListen()	- starts listener
	StopListen()	- stops listener
	InitServer()    - inits server with server conn. data
	RegisterRequestHandler(HTTPHandler *pHandler) 
		Registers handler for incoming client requests. Every client has its own thread.
		When new message arrives HTTPServerThread will invoke pHandler in list of handlers based on domain
		for which handler is registered. Client must set domain in HTTPRequest message that sends.
	SendMessage()			- send message to list of clients or just one. HTTPRequest message is sent iny ASYNC manner.
	CloseConnection()		- closes client connection
	SetIPAccessList()		- sets list of allowed client IP addresses
	...
*/
class HTTPServer : public QTcpServer
{
	Q_OBJECT

public:
    HTTPServer(QObject *parent=0);
    ~HTTPServer();

	//--------------------------------------
	//Main interface
	//--------------------------------------

	//start/stop
	void InitServer(HTTPServerConnectionSettings,bool bUseKickBan=true,DbRecordSet *lstKickBanStatic=NULL,DbRecordSet *lstKickBanDynamic=NULL);
	void StopServer();

	void StopListen();
	void StartListen(Status& pStatus, int nPort=-1);



	//request handler magament
	void RegisterRequestHandler(HTTPHandler *pHandler){m_pLstHandlers.append(pHandler);};
	void ClearRequestHandlerList(){m_pLstHandlers.clear();};			//do not use while server is running...
	void GetSocketList(ClientConnections &ClientThreadList);
	int GetActiveSocketsCount();
	HTTPContext GetThreadContext(int nThreadID);

	//sending message to client:
	void SendMessage(int nSocketID, HTTPRequest* pRequest);
	void SendMessage(QList<int> lstSocketID, HTTPRequest* pRequest);
	void SendMessageBroadCast(QByteArray* pBroadCastMsgBuffer);


	//IP black list:
	IPKicker* GetIPKickBanList(){return m_pIPKickBanManager;};
	//bool UseKickBanList(){return m_bUseKickBan;};
	bool GetDynamicallyBuiltIPAccessList(DbRecordSet &lstAccess);


	//--------------------------------------
	//Thread use:
	//--------------------------------------

	//close connection:
	bool CloseConnection(int nThreadID);
	void CloseConnection(QList<int> lstThreadID);
	QList<HTTPHandler *>  GetHandlers(){return m_pLstHandlers;};
signals:
	void ClientThreadIsDead(int nThreadID);

	//for registering new thread/client connection
public slots:
	void OnThreadDestroyed(int nThread);
	void OnThreadCreated(int nThread,HTTPServerThread*);
	void OnAcceptError(QAbstractSocket::SocketError);

protected:
	//overloaded server function
	virtual void incomingConnection(int socketDescriptor); 

private:
	void	CloseSocketThread(HTTPServerThread *pThread);
	void	UnRegisterClientThread(int);
	int		GetNumberOfClientThreads();
	void	CheckSSLCertificate(Status& pStatus);
	void	InitSSL(Status& pStatus);
		

private:
	//mutex locks	
	//QMutex m_Mutex;							///< mutex
	QReadWriteLock m_RWLock;				///< RWlock for accessing client thread list
	//QReadWriteLock m_RWLock_IPAccessList;	///< RWlock for accessing IP access list


	HTTPServerConnectionSettings m_ConnSettings;	///<holds connection settings for server
	QList<HTTPHandler *>  m_pLstHandlers;			///<holds all handlers
	ClientConnections m_ClientThreadList;			///<list of current threads (pointer) indexed by socket id

	IPKicker *m_pIPKickBanManager;								///<kicker
	bool m_bUseKickBan;						
};

#endif // QTESTSRV_H

