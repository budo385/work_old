#ifndef NETWORK_CONNECTIONS_H
#define NETWORK_CONNECTIONS_H

#include "httpclientconnectionsettings.h"
#include "common/common/encryptedinifile.h"
#include "common/common/connectionslistabstract.h"

class NetworkConnections: public ConnectionsListAbstract
{
public:
	NetworkConnections();
	~NetworkConnections();

	NetworkConnections(const NetworkConnections &other) : ConnectionsListAbstract() { operator = (other); }
	void operator = (const NetworkConnections &other){
		if(this != &other){
			m_lstConnections = other.m_lstConnections;
		}
	}
	
	void Clear();
	bool Load(QString strFile, QString strPassword);
	bool Save(QString strFile, QString strPassword);

	//should be called after Load() call, before Save()
	QString GetPasswordHash();
	bool MatchUserPassword(QString strPassword);
	bool IsUserPasswordSet();
	bool SetUserPasswordHash(QString strPassHash);
	void ClearUserPassword();

public:
	QList<HTTPClientConnectionSettings> m_lstConnections;
	int	m_nCurSel;

protected:
	EncryptedIniFile m_objIni;

	QString GetHashString(QString strPassword);
};

#endif //NETWORK_CONNECTIONS_H
