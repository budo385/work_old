#ifndef _IPDYNAMIC_ACCESSLIST_H_
#define _IPDYNAMIC_ACCESSLIST_H_

#include <QString>
#include <QList>
#include <QDateTime>
#include "common/common/dbrecordset.h"


/*!
	\class  IPAccessList
	\brief  IP-chains like class to handle access list based on client's IP address
	\ingroup IPModule


	Supports kickban timeout period & counter:
	- IP can be banned in some time interval, after that it is removed from kick list
	- IP can be registered to allow N kick tries, after IP is kicked N times, counter drops to zero, 
	   IP is banned for permanent or for time defined by nAfterCounterKickTime

*/

class IPDynamicAccessList
{

	class IPFilterEntry
	{
	public:
		IPFilterEntry(unsigned long nIP=0,unsigned short m_nFilterBits=0,bool bAllow=true);
		
		IPFilterEntry(const IPFilterEntry &that);
		IPFilterEntry &operator =(const IPFilterEntry &that);

		int CheckMatch(unsigned long nIP);

	public:
		unsigned long  m_nIP;
		unsigned short m_nFilterBits;
		//int m_nFilterBits;
		bool		   m_bAllow;
		QDateTime	   m_datBanTime;
		int			   m_nCounter;
	};

public:

	enum IP_ALLOW
	{
		IP_MATCH_ALLOWED,
		IP_MATCH_DENIED,
		IP_NO_MATCH,
		IP_MATCH_ALLOWED_REMOVE_ENTRY
	};

	//IPDynamicAccessList(int nAfterCounterKickTime=0){m_nAfterCounterKickTime=nAfterCounterKickTime};

	void LoadIPAccessList(DbRecordSet &lstAcess);
	void GetIPAccessList(DbRecordSet &lstAcess);

	void Initialize(int nAfterCounterKickTime=0){m_nAfterCounterKickTime=nAfterCounterKickTime;}

	void AddEntry(QString strIP, int nFilterBits, bool bAllow, int nPos = -1, int nBanTime=0,int nBanCounter=0);
	void RemoveEntry(QString strIP);
	bool IsIPAllowed(QString strIP);

	static unsigned long IpStringToNum(QString strIP);

protected:
	int						m_nAfterCounterKickTime; //seconds for ban if counter drop to 0 for IP address,if =0 = permanent ban
	QList<IPFilterEntry>	m_lstAccessList;	
};

#endif

