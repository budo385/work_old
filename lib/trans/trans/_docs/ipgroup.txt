/**
 *
 * \defgroup IPModule IP Protocol 
 * 
 * \ingroup TransModule
 *
 * This module contains RPC based classes for generating and parsing RPC calls
 *
*/