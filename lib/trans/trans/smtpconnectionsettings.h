#ifndef SMTPCONNECTIONSETTINGS_H
#define SMTPCONNECTIONSETTINGS_H


/*!
	\class HTTPClientConnectionSettings
	\brief Data placehoder for client connection settings, use by HTTPClientConnection to establish connection to server
	\ingroup HTTPClient
*/
class SMTPConnectionSettings {

public:
	SMTPConnectionSettings(): m_nPort(25), m_bUseSSL(false), m_bUseSTLS(false) {};
	  ~SMTPConnectionSettings(){};

	  QString m_strCustomName;	///< custom name
	  QString m_strUserName;
	  QString m_strUserEmail;
	  QString m_strHost;
	  int m_nPort;				///< server port
	  QString m_strAccountName;
	  QString m_strAccountPassword;
	  bool m_bUseSSL;
      bool m_bUseSTLS;
};

#endif //SMTPCONNECTIONSETTINGS_H
