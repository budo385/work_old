#include "smtpsocket.h"
#include <QTcpSocket>
#include <QTime>
#include <QLocale>
//#include "common/common/datahelper.h"

SMTPSocket::SMTPSocket(ThreadSynchronizer *ThreadSync)
{
	m_ThreadSync=ThreadSync;
	m_nTimerID=-1;
	ClearState();

	m_socket = new QTcpSocket( this );
	connect( m_socket, SIGNAL( readyRead() ), this, SLOT( readyRead() ) );
	connect( m_socket, SIGNAL( connected() ), this, SLOT( connected() ) );
	connect( m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(errorReceived(QAbstractSocket::SocketError)));   
	connect(m_socket, SIGNAL(disconnected()), this, 	SLOT(disconnected()));
}

SMTPSocket::~SMTPSocket()
{
	delete t;
	delete m_socket;
}

//bBodyIsMessage = if true then all paramteres are ignored, and content inside body is sent as fully formatted MIME message
void SMTPSocket::SetParameters(const QString &strMailServer, int nConnectionTimeOut, const QString &from, const QString &to, const QString &subject, const QString &body, const QList<QString> &bcc,  QString strUserName, QString strPassword, const bool bBodyIsHtml, const bool bSendBodyAsFormatedMimeMessage,int nSmtpPort, bool bIsUTF8) 
{
	m_nSmtpPort=nSmtpPort;
	m_nTimeOut=nConnectionTimeOut;
	ClearState();

	//Generate date
	QTime myTime = QDateTime::currentDateTime().toUTC().time();
	QLocale myLocal = QLocale(QLocale::C);
	QString date = myLocal.toString(QDate::currentDate(), "ddd, dd MMM yyyy ");
	date += myLocal.toString(myTime, "hh:mm:ss");
	date += " +0000 (UTC)";
	

	if (bSendBodyAsFormatedMimeMessage)
	{
		m_strMessage = body;
		/*
		QByteArray buffer = m_strMessage.toUtf8();
		buffer.append("\r\n\r\n---------------new email-------------\r\n\r\n");
		DataHelper::WriteFile("D:\log_mime.txt",buffer);
		*/
	}
	else
	{
		//All about SMTP headers: 
		// to work must specify correct MIME type, to work on mail.sokrats.ch server header must be in order: subject, date, from to..+"\n" on end...
		//m_strMessage="\n";
		m_strMessage="";
		//m_strMessage.append("Date: Fri, 10 Sep 2010 08:43:35 +0200 \n");
		m_strMessage.append("Subject: " + subject + "\n");
		m_strMessage.append("Date: " + date + "\n");
		m_strMessage.append("From: " + from + "\n");
		m_strMessage.append("To: " + to + "\n");

		//m_strMessage.append("Date: " + date + "\n");

		if(bBodyIsHtml)
		{
			m_strMessage.append("MIME-Version: 1.0 \n");
			//m_strMessage.append("Content-Type: text/html; charset=\"iso-8859-1\"; format=flowed \n");
			m_strMessage.append("Content-Type: text/html; charset=\"iso-8859-1\" \n");
			m_strMessage.append("Content-Transfer-Encoding: 8bit \n");
		}
		else
		{
			m_strMessage.append("MIME-Version: 1.0 \n");
			m_strMessage.append("Content-Type: text/plain; charset=\"iso-8859-1\" \n");
			m_strMessage.append("Content-Transfer-Encoding: 8bit \n");

		}

		m_strMessage.append("\n");

		m_strMessage.append(body);
		m_strMessage.replace( QString::fromLatin1( "\n" ), QString::fromLatin1( "\r\n" ) );
		m_strMessage.replace( QString::fromLatin1( "\r\n.\r\n" ),QString::fromLatin1( "\r\n..\r\n" ) );
	}




	//qDebug()<<m_strMessage;

    //Add to adress
    m_lstRcpt = bcc;
    m_lstRcpt.insert(0, to); 
    nRcptIndex = 0;
	m_strFrom=from;
	m_strMailServer=strMailServer;

	m_strUser=strUserName;
	m_strPass=strPassword;
	m_bIsUTF8 =bIsUTF8;
}

void SMTPSocket::SendMail()
{
	m_State = Init;
	if (m_nSmtpPort<=0)
	{
		//MR: 25 is default port for unrestricted email reception, 587 is the default port for authenicating SMTP
		m_socket->connectToHost( m_strMailServer, m_strUser.isEmpty()? 25 : 587);
	}
	else
	{
		//MR: 25 is default port for unrestricted email reception, 587 is the default port for authenicating SMTP
		m_socket->connectToHost( m_strMailServer, m_nSmtpPort);
	}
	t = new QTextStream( m_socket );
	m_nTimerID=startTimer(m_nTimeOut);
}

void SMTPSocket::connected()
{
	m_State=Connected;
}

void SMTPSocket::timerEvent(QTimerEvent *event)
{
	//out timer is here, then fire Time out error, save our asses!!!
	if(event->timerId()==m_nTimerID)
	{
		if(m_State==Init)
		{
			ClearState();
			m_bErrorOccured=true;
			m_strLastError="Server time out!";
			m_ThreadSync->ThreadRelease();
		}
		else
			killTimer(m_nTimerID);
	}
}

void SMTPSocket::Destroy()
{
	deleteLater();
	m_ThreadSync->ThreadRelease();
}

void SMTPSocket::ClearState()
{
	if(m_nTimerID!=-1)killTimer(m_nTimerID);
	//m_socket->blockSignals(true);
	//m_socket->abort();
	//m_socket->blockSignals(false);
	m_nTimerID=-1;
	m_bErrorOccured=false;
	m_strLastError="";
}

void SMTPSocket::errorReceived(QAbstractSocket::SocketError socketError)
{
	if (m_strLastError=="OK")
	{
		m_bErrorOccured=false;
		m_ThreadSync->ThreadRelease();
		return;
	}

	ClearState();
	m_strLastError=m_socket->errorString();
	m_bErrorOccured=true;

	m_ThreadSync->ThreadRelease();
}

void SMTPSocket::disconnected()
{
	if (m_strLastError=="OK")
	{
		m_bErrorOccured=false;
		m_ThreadSync->ThreadRelease();
		return;
	}


	m_strLastError=m_socket->errorString();
	if (!m_strLastError.isEmpty())
	{
		m_bErrorOccured=true;
	}
	else if (m_State!=Close)
	{
		m_strLastError="Failed to send mail";
		m_bErrorOccured=true;
	}



	m_ThreadSync->ThreadRelease();
}


void SMTPSocket::readyRead()
{

    QString responseLine;
    do
    {
        responseLine = m_socket->readLine();
        response += responseLine;
    }
    while ( m_socket->canReadLine() && responseLine[3] != ' ' );

	//qDebug()<<responseLine;

	responseLine.truncate( 3 );


	if ( m_State == User && responseLine[0] == '5' )
	{
		//authentication not enabled: just skip it then
		m_State=Mail;
		responseLine="2";
		response="";

	}

    if ( m_State == Connected && responseLine[0] == '2' )
    {
        // banner was okay, let's go on
		*t << "HELO there\r\n";
		t->flush();
		if (!m_strUser.isEmpty())
			m_State=Auth;
		else
			m_State = Mail;
    }
	else if ( m_State == Auth && responseLine[0] == '2' )
	{
		*t << "auth login" << "\r\n";
		t->flush();
		m_State = User;
	}
	else if ( m_State == User && responseLine[0] == '3' )
	{
		*t << m_strUser.toLatin1().toBase64() << "\r\n";
		t->flush();
		m_State = Pass;
	}
	else if ( m_State == Pass && responseLine[0] == '3' )
	{
		*t << m_strPass.toLatin1().toBase64() << "\r\n";
		t->flush();
		m_State = Mail;
	}

    else if ( m_State == Mail && responseLine[0] == '2' )
    {
        // HELO response was okay (well, it has to be)

        *t << "MAIL FROM: <" << m_strFrom << ">\r\n";
		t->flush();
        m_State = Rcpt;
    }
    else if ( m_State == Rcpt && responseLine[0] == '2' )
    {
    	QString adress = m_lstRcpt[nRcptIndex];
        *t << "RCPT TO: <" << m_lstRcpt[nRcptIndex] << ">\r\n"; //r
        nRcptIndex++;
		t->flush();
        if (nRcptIndex >= m_lstRcpt.size())
        	m_State = Data;
    }
    else if ( m_State == Data && responseLine[0] == '2' )
    {
        *t << "DATA\r\n";
		t->flush();
        m_State = Body;
    }
    else if ( m_State == Body && responseLine[0] == '3' )
    {
		if (m_bIsUTF8)
		{
			t->setCodec("UTF-8");
			t->setAutoDetectUnicode(true);
			*t << m_strMessage << "\r\n.\r\n";
		}
		else
			*t << m_strMessage << "\r\n.\r\n";

		t->flush();
        m_State = Quit;
    }
    else if ( m_State == Quit && responseLine[0] == '2' )
    {
        *t << "QUIT\r\n";
		t->flush();
        // here, we just close.
        m_State = Close;
    }
    else if ( m_State == Close )
    {
		//qDebug()<<m_State;

		m_bErrorOccured=false;
		m_strLastError="OK";

		m_socket->disconnectFromHost();
		m_ThreadSync->ThreadRelease();
		return;
    }
    else
    {
		//qDebug()<<m_State;

		ClearState();
		m_bErrorOccured=true;
		m_strLastError="Unexpected reply from SMTP strMailServer:\n\n" + response;
		m_ThreadSync->ThreadRelease();
		return;
    }
    response = "";

	//qDebug()<<m_State;
}
