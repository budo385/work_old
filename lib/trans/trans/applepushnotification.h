#ifndef APPLEPUSHNOTIFICATION_H
#define APPLEPUSHNOTIFICATION_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QSslSocket>
#include "pushnotification.h"

#define DEVICE_BINARY_SIZE  32
#define MAXPAYLOAD_SIZE     256
#define TMP_BUFFER_SIZE     2048


class ApplePushNotification : public PushNotification
{
	Q_OBJECT

public:
	ApplePushNotification(ThreadSynchronizer *sync,int nTimeOut=10000);
	~ApplePushNotification();

	//-------------------PUBLIC DATA FOR EASY MANIPULATION:


	//void connectToAPN();
	//void connectToFeedBack();


	//common vars for all objects, but configurable from INI file:
	static	QString m_strPushServerURL;
	static	int		m_nPushServerPort;
	static	QString m_strFeedBackServerURL;
	static	int		m_nFeedBackServerPort;
	static	QString m_StrPeerName;

	//static	QString m_strCACertificatePath;
	static	QString m_strLocalCertificatePath;
	static	QString m_strPrivateKeyPath;




public slots:
	void SendPushMessage(); //async method
	void Destroy();

private slots:
	void onSocketClosed();
	void onConnected();
	void readResponse();
	void onSslErrors(const QList<QSslError> &error);
	void socketEncrypted();
	void goSocketClose();

private:
	int		parseBytes(int b1, int b2, int b3, int b4); 
	void	OnTimeOut();

	bool		m_bFeedBack;
	QSslSocket	*m_pSSLSocket;
};















typedef struct {
	/* The Message that is displayed to the user */
	char *message;

	/* The name of the Sound which will be played back */
	char *soundName;

	/* The Number which is plastered over the icon, 0 disables it */
	int badgeNumber;

	/* The Caption of the Action Key the user needs to press to launch the Application */
	char *actionKeyCaption;

	/* Custom Message Dictionary, which is accessible from the Application */
	char* dictKey[5];
	char* dictValue[5];
} Payload;

class APNMessage : public QObject
{
	Q_OBJECT

public:
	APNMessage(QObject *parent=0);
	~APNMessage();

	/* Used internally to send the payload */
	int sendMessage(QSslSocket *pSSLSocket, int nMessageIdentifier, QString strToken, QString strMessage = QString(), int nBadgeNumber = 0, QString	strSoundName = QString(), QString strActionKeyCaption = QString(), QString strLocKey = QString(), 
		QStringList lstLocArgs = QStringList(), QString strLaunchImage = QString(), QStringList lstCustomVals=QStringList());

	char m_messageBuff[MAXPAYLOAD_SIZE];

private:
	int send_payload(QSslSocket *pSSLSocket, int nMessageIdentifier, const char *deviceTokenHex, const char *payloadBuff, size_t payloadLength);
	void formatMessagePayload(QString strMessage = QString(), int nBadgeNumber = 0, QString	strSoundName = QString(), QString strActionKeyCaption = QString(), 
		QString strLocKey = QString(), QStringList lstLocArgs = QStringList(), QString strLaunchImage = QString(), QStringList lstCustomVals=QStringList());
	void addKeyValuePair(char *cPayload, QString strKey, QString strValue);
	void addKeyValuePair(char *cPayload, QString strKey, int nValue);
	void addKeyValuePair(char *cPayload, QString strKey, QStringList lstValues);
	void addKeyValuePair(char *cPayload, QString strKey, QList<int> lstValues);

};


#endif // APPLEPUSHNOTIFICATION_H
