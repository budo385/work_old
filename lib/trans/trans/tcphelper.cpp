#include "tcphelper.h"
#include <QNetworkInterface>
#include <QNetworkAddressEntry>
#include <QHostAddress>
#include <QTcpSocket>




TcpHelper::TcpHelper(QObject *parent)
	: QObject(parent)
{

}

TcpHelper::~TcpHelper()
{

}


bool TcpHelper::IsHostAddress(QString strIP)
{
	if (strIP=="localhost")
		return true;

	QHostAddress tesIP(strIP);
	QList<QHostAddress> lstAddress=	QNetworkInterface::allAddresses();

	int nSize=lstAddress.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstAddress.at(i)==tesIP)
			return true;
	}

	return false;
}

QString TcpHelper::ResolveLocalHostURLService(QString strIP, int nPort)
{
	if (strIP=="0.0.0.0" || strIP=="127.0.0.1" || strIP=="localhost")
	{
		QList<QNetworkInterface> lstInterafaces = QNetworkInterface ::allInterfaces();

		int nIdxLAN_Interface=-1;
		for (int i=0;i<lstInterafaces.size();i++)
		{
			//qDebug()<<lstInterafaces.at(i).humanReadableName();
			if (lstInterafaces.at(i).humanReadableName().indexOf("Local")>=0 || lstInterafaces.at(i).humanReadableName().indexOf("LAN")>=0)
			{
				//qDebug()<<lstInterafaces.at(i).humanReadableName();
				//qDebug()<<lstInterafaces.at(i).flags();

				if (lstInterafaces.at(i).flags() & QNetworkInterface::IsUp && lstInterafaces.at(i).flags() & QNetworkInterface::IsRunning && !(lstInterafaces.at(i).flags() & QNetworkInterface::IsLoopBack))
				{
					nIdxLAN_Interface=i;
					QList<QHostAddress> lstAddress = lstInterafaces.at(nIdxLAN_Interface).allAddresses();
					
					bool bFoundLocalIP=false;
					int nSize2 = lstAddress.size();
					for(int k=0;k<nSize2;++k)
					{
						QString strAddress=lstAddress.at(k).toString();
						if (strAddress.indexOf("192.168.") == 0 || strAddress.indexOf("10.") == 0 || strAddress.indexOf("172.16") == 0)
						{
							return strAddress;
							//bFoundLocalIP=true;
							//break;
						}
					}

					if (!bFoundLocalIP)
					{
						nIdxLAN_Interface=-1;
						continue;
					}

					break; //found
				}
			}
		}

		QList<QHostAddress> lstAddress;
		
		if (nIdxLAN_Interface>=0)
			lstAddress = lstInterafaces.at(nIdxLAN_Interface).allAddresses();
		else
			lstAddress=	QNetworkInterface::allAddresses();

		int nSize=lstAddress.size();

		for(int i=0;i<nSize;++i)
		{
			QString strAddress=lstAddress.at(i).toString();
			if (strAddress=="0.0.0.0" || strAddress=="127.0.0.1" || strAddress=="localhost")
				continue;

			if (lstAddress.at(i).protocol()==QAbstractSocket::IPv6Protocol)
				continue;

			if (strAddress.indexOf("192.168.") == 0 || strAddress.indexOf("10.") == 0 || strAddress.indexOf("172.16") == 0)
				return strAddress;
			else
				continue;


			return lstAddress.at(i).toString();

			//try to connect to it:
			
			QTcpSocket socket;
			socket.connectToHost(strAddress, nPort);
			socket.waitForConnected(10000);//5 sec
			if (socket.state() == QTcpSocket::ConnectedState)
			{
				return strAddress;
			}

		}
	}
	return strIP;
}


//IP address is 0.0.0.0 or localhost, try to get first 'outer' network interface and compile string for web access: http://IP:port
/*
void TcpHelper::ResolveWebAddressNetworkInterface(QString strIP, int nPort, bool bUseSSL)
{
	QString strResovledIP="";
	if (strIP=="0.0.0.0" || strIP=="127.0.0.1" || strIP=="localhost")
	{
	*/
		/*
		bool bResolved=false;
		QTcpSocket socket;
		socket.connectToHost("www.sokrates.ch", 80,QIODevice::ReadOnly);
		if (socket.waitForConnected(5000)) 
		{	
			qDebug()<<socket.localAddress().toString();
			qDebug()<<socket.peerAddress().toString();
		}
		*/
		//parse through interfaces, find's first ipv4 and set's�
/*
		QList<QNetworkInterface> lstInterfaces=QNetworkInterface::allInterfaces();
		int nSize=lstInterfaces.size();
		for(int i=0;i<nSize;++i)
		{
			qDebug()<<"if: "<<lstInterfaces.at(i).name()<<" name: "<<lstInterfaces.at(i).humanReadableName();
		
			QList<QNetworkAddressEntry>	 lstAddress=lstInterfaces.at(i).addressEntries();
			int nSize2=lstAddress.size();
			for(int k=0;k<nSize2;++k)
			{
				if (lstAddress.at(k).ip()!=QHostAddress("127.0.0.1"))
				{
					strIP=lstAddress.at(k).ip().toString();
					qDebug()<<" ip:" <<strIP;
					if (strResovledIP.isEmpty() && lstAddress.at(k).ip().protocol()==QAbstractSocket::IPv4Protocol)
						strResovledIP=strIP;
				}
			}
		}
*/
	/*
		QList<QHostAddress> lstAddress=	QNetworkInterface::allAddresses();
		nSize=lstAddress.size();
		for(int i=0;i<nSize;++i)
		{
			if (lstAddress.at(i)!=QHostAddress("127.0.0.1"))
			{
				strIP=lstAddress.at(i).toString();
				qDebug()<<strIP;
				//break;
			}
		}
	*/
/*
	}


	
	if (bUseSSL)
		s_sHostWebAddress="https://";
	else
		s_sHostWebAddress="http://";

	if (!strResovledIP.isEmpty())
		s_sHostWebAddress+=strResovledIP;
	else
		s_sHostWebAddress+=strIP;

	if (nPort!=80)
	{
		s_sHostWebAddress+=":"+QString::number(nPort);
	}
}*/