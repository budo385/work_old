#ifndef WINDOWPUSHNOTIFICATION_H
#define WINDOWPUSHNOTIFICATION_H

#include "qhttp.h"
#include <QSslError>
#include "pushnotification.h"

class WindowPushNotification : public PushNotification
{
	Q_OBJECT

public:
	WindowPushNotification(ThreadSynchronizer *sync,int nTimeOut=10000);
	~WindowPushNotification();

	enum WindowsPhonePushPriority{
		TileImmediately = 1,
		ToastImmediately = 2,
		RawImmediately = 3,
		TileWait450 = 11,
		ToastWait450 = 12,
		RawWait450 = 13,
		TileWait900 = 21,	
		ToastWait900 = 22,	
		RawWait900 = 23
	};


public slots:
	void SendPushMessage(); //async method
	void Destroy();

private slots:
	void onRequestDone(bool bError);
	void onSslErrors(const QList<QSslError> &error);

private:
	void OnTimeOut();
	QHttp *m_pHttp;
};

#endif // WINDOWPUSHNOTIFICATION_H
