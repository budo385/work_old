#ifndef HTTPSERVERPING_H
#define HTTPSERVERPING_H

//#include "httpreadersync.h"
#include "common/common/authenticator.h"

class HTTPServerPing //: protected HttpReaderSync
{

public:
	HTTPServerPing(int nTimeOut=10000);
	~HTTPServerPing();
	HTTPServerPing(const HTTPServerPing &that);
	HTTPServerPing &operator =(const HTTPServerPing &that); 

	void SetParameters(QString strIP,int nPort, bool bSSL,Authenticator &SessionCache);
	bool Ping();

private:
	Authenticator m_SessionCache;
	QString m_strIP;
	int m_nPort;
	bool m_bSSL;
	int m_nTimeOut;
};

#endif // HTTPSERVERPING_H
