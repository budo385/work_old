#ifndef HTTPREADER_H
#define HTTPREADER_H

#include <QObject>
#include <QThread>
#include "qhttp.h"
#include <QUrl>
#include <QBuffer>
#include <QMutex>
#include <QTimerEvent>
#include "common/common/threadsync.h"
#include <QDebug>
#include <QWaitCondition>
#include <QAuthenticator>





/*!
	\class HttpReader
	\brief Helper object for synchronized HTTP reading of HttpReaderSync
	\ingroup HTTPClient

	Because QHttp object works in async mode, there is a need to read out content right away (sync)
	Use:
	- Call GetPage() from another thread, wait until ThreadSynchronizer is free
	- get result with GetResponse()
*/

class HttpReader : public QObject
{
	Q_OBJECT

public:
    HttpReader(ThreadSynchronizer *sync,int nTimeOut=20000);
    ~HttpReader();

	bool GetResponse(QByteArray *pContent);
	bool IsAuthRequired(){return m_bAuthRequired;};
	QString GetAuthRequiredHost(){return m_strAuthRequiredHost;}
	void SetAuthentication(QAuthenticator *auth);
	void SetTimeOut(int nTimeOut){m_nTimeOut=nTimeOut;};

public slots:
	void ReadURL(QString strURL, bool bSSL=false,int nPort=0);
	void PostData(QString strURL, QByteArray postData, bool bSSL=false,int nPort=0);
	void Destroy();

private slots:
	void ReadingDone(int id,bool error);
	void OnAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth);
	void OnReadyRead ( const QHttpResponseHeader & resp ); 
	void OnSslErrors ( const QList<QSslError> & );
	void OnResponseHeaderReceived ( const QHttpResponseHeader &  );
	
signals:
	void ReadInProgress(int nCurrentSize, int nTotalSize);

private:
	void timerEvent(QTimerEvent *event);
	void ClearState();
    	

	QHttp m_HttpReader;
	QByteArray m_RespBuffer;
	bool m_bErrorOccured;
	int m_nTimeOut;
	int m_nTimerID;
	int m_ReqID;
	bool m_bAuthRequired;
	int m_nTotalSize;
	QString m_strAuthRequiredHost;

	ThreadSynchronizer *m_ThreadSync;

};

#endif // HTTPREADER_H

