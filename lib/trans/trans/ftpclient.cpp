#include "ftpclient.h"
#include <QCoreApplication>
#include <QEventLoop>
#include <QFile>
#include <QFileInfo>

#include "ftpclientsockethandler.h"

/*!
Constructor: starts socket thread
*/
FTPClient::FTPClient()
: QThread(NULL)
{
	m_BufferSemaphore= new QSemaphore(1); //for shared resource: buffer
	m_bConnected=false;

	InvalidateBuffers(); // sets buffers to NULL

	m_ThreadSync.ThreadSetForWait();
	start(); //start thread
	m_ThreadSync.ThreadWait(); //wait until thread is started

}


/*!
Destructor: stops socket thread
*/
FTPClient::~FTPClient()
{

	//m_ThreadSync.ThreadSetForWait();	//prepare thread for wait condition
	emit SignalFTPDestroy();				//signal socket to init auto-destruct sequence
	//m_ThreadSync.ThreadWait();			//wait until thread is stopped

	if(isRunning())
	{
		quit();		//stop thread
		wait();
	}

	delete m_BufferSemaphore;
}

/*!
Socket thread main loop
*/
void FTPClient::run()
{

	//create socket object that will live in thread //destroyed by SocketDisconnect() signal
	FTPClientSocketHandler* pSocketHandler=new FTPClientSocketHandler(this,m_BufferSemaphore,&m_ThreadSync,&m_waitCond);


	//connect main & this worker thread with signal/slot technology:
	connect(this,SIGNAL(SignalFTPConnect()),pSocketHandler,SLOT(OnFTPConnect()),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalFTPDisconnect()),pSocketHandler,SLOT(OnFTPDisconnect()),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalFTPDestroy()),pSocketHandler,SLOT(OnFTPDestroy()),Qt::QueuedConnection);

	connect(this,SIGNAL(SignalFTPDir_Change(QString)),pSocketHandler,SLOT(OnFTPDir_Change(QString)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalFTPDir_Create(QString)),pSocketHandler,SLOT(OnFTPDir_Create(QString)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalFTPDir_Remove(QString)),pSocketHandler,SLOT(OnFTPDir_Remove(QString)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalFTPDir_List()),pSocketHandler,SLOT(OnFTPDir_List()),Qt::QueuedConnection);

	connect(this,SIGNAL(SignalFTPFile_Put(QString)),pSocketHandler,SLOT(OnFTPFile_Put(QString)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalFTPFile_Get(QString)),pSocketHandler,SLOT(OnFTPFile_Get(QString)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalFTPFile_Remove(QString)),pSocketHandler,SLOT(OnFTPFile_Remove(QString)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalFTPFile_Rename(QString,QString)),pSocketHandler,SLOT(OnFTPFile_Rename(QString,QString)),Qt::QueuedConnection);

	connect(this,SIGNAL(SignalFTPSendRawCommand(QString)),pSocketHandler,SLOT(OnFTPSendRawCommand(QString)),Qt::QueuedConnection);

	m_ThreadSync.ThreadRelease();//release thread

	exec(); //start event loop
}


/*!
Connect to server based on given conn. settings. Error if already connected
/param pStatus, 0 if OK
/param pConnSettings contains connection settings
*/
void FTPClient::Connect(Status& pStatus,FTPConnectionSettings& pConnSettings)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); //lock access
	SetError(0); //reset error
	InvalidateBuffers();

	SetConnectionSettings(pConnSettings); 		//write conn. settings:
	if (pConnSettings.m_strServerIP.isEmpty())	//if empty fire TimeOut error at once
	{
		pStatus.setError(StatusCodeSet::ERR_HTTP_HOST_NOT_FOUND);
		return;
	}

	m_ThreadSync.ThreadSetForWait();		//prepare for wait
	emit SignalFTPConnect();	//send connect request:
	m_ThreadSync.ThreadWait();			//wait until something is come back:

	pStatus=GetError(); 	//check error:
	//because of wild threads, check state once again, if not connected set error:
	if(!IsConnected() && pStatus.IsOK())
		pStatus.setError(StatusCodeSet::ERR_HTTP_HOST_CLOSED);

	if (!pConnSettings.m_strStartDirectory.isEmpty())
	{
		locker.unlock();
		Dir_Change(pStatus,pConnSettings.m_strStartDirectory);
	}

}



/*!
Disconnect from server: wait for current operation
*/
void FTPClient::Disconnect(Status& pStatus)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); //lock access
	SetError(0); //reset error
	InvalidateBuffers();

	m_ThreadSync.ThreadSetForWait();		//prepare for wait
	emit SignalFTPDisconnect();				//send disconnect request:
	m_ThreadSync.ThreadWait();				//wait until something is come back:


	pStatus=GetError(); 		//check error:

	//if normal error (host disconnect, ignore),
	if(pStatus.getErrorCode()==StatusCodeSet::ERR_HTTP_HOST_CLOSED)
		pStatus.setError(0);

}


void FTPClient::Dir_Change(Status& pStatus,QString strDir)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();

	m_ThreadSync.ThreadSetForWait();		
	emit SignalFTPDir_Change(strDir);
	m_ThreadSync.ThreadWait();			

	pStatus=GetError(); 		
}


void FTPClient::Dir_Create(Status& pStatus,QString strDir)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();

	m_ThreadSync.ThreadSetForWait();		
	emit SignalFTPDir_Create(strDir);
	m_ThreadSync.ThreadWait();			

	pStatus=GetError(); 

}

void FTPClient::Dir_Remove(Status& pStatus,QString strDir)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();

	m_ThreadSync.ThreadSetForWait();		
	emit SignalFTPDir_Remove(strDir);
	m_ThreadSync.ThreadWait();			

	pStatus=GetError();

}
void FTPClient::Dir_List(Status& pStatus,QList<QUrlInfo> &lstDirContent)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();

	m_ThreadSync.ThreadSetForWait();		
	emit SignalFTPDir_List();
	m_ThreadSync.ThreadWait();			

	pStatus=GetError();
	GetDirList(lstDirContent);
}

void FTPClient::File_Put(Status& pStatus,QByteArray &fileContent,QString strFileName)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();
	m_FileBuffer=&fileContent;

	//m_ThreadSync.ThreadSetForWait();		
	//emit SignalFTPFile_Put(strFileName);


	m_BufferSemaphore->acquire(1);		
	emit SignalFTPFile_Put(strFileName);

	//---------------------------------------------------
	//				SYNC MODE
	//---------------------------------------------------

	if(m_BufferSemaphore->available()==0) //signal above may switch threads at once
	{
		int nWaitTicks=0; //can be used for progress bar;
		while (!m_waitCond.wait(&m_Mutex,500))//wait for socket to notify or wait 500ms, which comes first
		{
			//emit progress signal & process events (refresh GUI)
			//nWaitTicks++;
			//emit SocketOperationInProgress(nWaitTicks);
			qint64 nDone, nTotal;
			GetDataTransferParameters(nDone,nTotal);
			emit SignalFTPOperationInProgress(nDone,nTotal);
			QCoreApplication::instance()->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
			if(m_BufferSemaphore->available()==1) //when we exited wait() condition, socket maybe finish job, check it (buffers will be unlocked by socket)
				break; //exit loop if buffers unlocked
		}
	}


	Q_ASSERT(m_BufferSemaphore->available()==1); //buffer must be available;

	//one last signal:
	qint64 nDone, nTotal;
	GetDataTransferParameters(nDone,nTotal);
	emit SignalFTPOperationInProgress(nTotal,nTotal);
	QCoreApplication::instance()->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals


	pStatus=GetError();
}


void FTPClient::File_Put(Status& pStatus,QString strFileFullPath)
{
	QFile file(strFileFullPath);
	if (!file.exists())
	{
		QString strMsg=tr("File")+" "+strFileFullPath+" "+tr("does not exist!");
		pStatus.setError(1,strMsg);
		return;
	}

	if(!file.open(QIODevice::ReadOnly))
	{
		QString strMsg=tr("Error while reading file: ")+" "+strFileFullPath;
		pStatus.setError(1,strMsg);
		return;
	}

	QByteArray byteContent=file.readAll();
	file.close();
	if (byteContent.size()==0)
	{
		QString strMsg=tr("File")+" "+strFileFullPath+" "+tr("is empty!");
		pStatus.setError(1,strMsg);
		return;
	}

	QFileInfo fileInfo(strFileFullPath);
	File_Put(pStatus,byteContent,fileInfo.fileName());


}

void FTPClient::File_Get(Status& pStatus,QByteArray &fileContent,QString strFileName)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();
	m_FileBuffer=&fileContent;

	//m_ThreadSync.ThreadSetForWait();		
	//emit SignalFTPFile_Get(strFileName);
	//m_ThreadSync.ThreadWait();


	m_BufferSemaphore->acquire(1);		
	emit SignalFTPFile_Get(strFileName);

	//---------------------------------------------------
	//				SYNC MODE
	//---------------------------------------------------

	if(m_BufferSemaphore->available()==0) //signal above may switch threads at once
	{
		int nWaitTicks=0; //can be used for progress bar;
		while (!m_waitCond.wait(&m_Mutex,500))//wait for socket to notify or wait 200ms, which comes first
		{
			//emit progress signal & process events (refresh GUI)
			//nWaitTicks++;
			//emit SocketOperationInProgress(nWaitTicks);
			qint64 nDone, nTotal;
			GetDataTransferParameters(nDone,nTotal);
			emit SignalFTPOperationInProgress(nDone,nTotal);
			QCoreApplication::instance()->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals
			if(m_BufferSemaphore->available()==1) //when we exited wait() condition, socket maybe finish job, check it (buffers will be unlocked by socket)
				break; //exit loop if buffers unlocked
		}
	}

	Q_ASSERT(m_BufferSemaphore->available()==1); //buffer must be available;

	//one last signal:
	qint64 nDone, nTotal;
	GetDataTransferParameters(nDone,nTotal);
	emit SignalFTPOperationInProgress(nTotal,nTotal);
	QCoreApplication::instance()->processEvents(QEventLoop::ExcludeUserInputEvents);  //this will give time for GUI refresh/signals


	pStatus=GetError();

}


void FTPClient::File_Get(Status& pStatus,QString strFileName,QString strDestinationFileName)
{
	QByteArray byteContent;
	File_Get(pStatus,byteContent,strFileName);
	if (!pStatus.IsOK()) return;

	QFile file(strDestinationFileName);
	if(!file.open(QIODevice::WriteOnly))
	{
		QString strMsg=tr("Error while opening file: ")+" "+strDestinationFileName;
		pStatus.setError(1,strMsg);
		return;
	}

	if(file.write(byteContent)<0)
	{
		QString strMsg=tr("Error while writing file: ")+" "+strDestinationFileName;
		pStatus.setError(1,strMsg);
		file.close();
		return;
	}
	file.close();
}

void FTPClient::File_Remove(Status& pStatus,QString strFileName)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();

	m_ThreadSync.ThreadSetForWait();		
	emit SignalFTPFile_Remove(strFileName);
	m_ThreadSync.ThreadWait();			

	pStatus=GetError();

}
void FTPClient::File_Rename(Status& pStatus,QString strFileName,QString strNewFileName)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();

	m_ThreadSync.ThreadSetForWait();		
	emit SignalFTPFile_Rename(strFileName,strNewFileName);
	m_ThreadSync.ThreadWait();			

	pStatus=GetError();

}

void FTPClient::SendRawCommand(Status& pStatus, QString strCommand)
{
	//serialize access
	QMutexLocker locker(&m_Mutex); 
	SetError(0); //reset error
	InvalidateBuffers();

	m_ThreadSync.ThreadSetForWait();		
	emit SignalFTPSendRawCommand(strCommand);
	m_ThreadSync.ThreadWait();			

	pStatus=GetError();

}









/*------------------------------------------------------------------------*/
/*							CONNECTION SETTINGS							  */	
/*------------------------------------------------------------------------*/


/*!
Used by socket thread to signal error
\param pErrorCode 
\param pErrorTxt 
\param bUncoditional - if false then if error is already, error will not be overwritten!!! 

*/
void FTPClient::SetError(int pErrorCode,QString pErrorTxt)//,QString pErrorTxt,bool bUncoditional)
{
	QWriteLocker locker(&m_RWLock);
	//if(!bUncoditional && m_LastError.getErrorCode()!=0) return; //does not overwrite error if already exists (first is most important)
	m_LastError.setError(pErrorCode,pErrorTxt);//,pErrorTxt);

}


/*!
Used by main thread to set connection data for socket thread
\param pConnSettings 
*/
void FTPClient::	SetConnectionSettings(FTPConnectionSettings& pConnSettings)
{
	QWriteLocker locker(&m_RWLock);
	m_ConnSettings=pConnSettings;

}


/*!
Used by slave thread to set connected state
*/
void FTPClient::SetConnected(bool bConnected)
{
	QWriteLocker locker(&m_RWLock);
	m_bConnected=bConnected;
}


bool FTPClient::IsConnected()
{
	QReadLocker locker(&m_RWLock);
	return m_bConnected;
}




/*------------------------------------------------------------------------*/
/*						  REQUEST/RESPONSE BUFFERS						  */
/*------------------------------------------------------------------------*/


void FTPClient:: WriteBuffer(QByteArray& pBuffer)
{
	//check if external buffer is set
	QWriteLocker lock(&m_RWLock);

	if (m_FileBuffer!=NULL)	//just precaution measure!
		(*m_FileBuffer)=pBuffer;

}

/*!
	Returns file buffer
	param pBuffer -returned file
*/
void FTPClient::GetBuffer(QByteArray& pBuffer){
	//check if external buffer is set
	QReadLocker lock(&m_RWLock);

	if (m_FileBuffer==NULL)
	{	
		Q_ASSERT(false);
		pBuffer=QByteArray(); //return empty buffer
	}
	else
		pBuffer=(*m_FileBuffer);	

}

void FTPClient::SetDataTransferParameters(qint64 done,qint64 total)
{
	QWriteLocker lock(&m_RWLock);
	m_Done=done;
	m_Total=total;
}
void FTPClient::GetDataTransferParameters(qint64 &done,qint64 &total)
{
	QReadLocker lock(&m_RWLock);
	done=m_Done;
	total=m_Total;
}
void FTPClient::SetDirList(QList<QUrlInfo> &lstDirContent)
{
	QWriteLocker lock(&m_RWLock);
	m_lstDirContent=lstDirContent;
}
void FTPClient::GetDirList(QList<QUrlInfo> &lstDirContent)
{
	QReadLocker lock(&m_RWLock);
	lstDirContent=m_lstDirContent;

}






/*------------------------------------------------------------------------*/
/*						  PRIVATE FUNCTIONS								  */
/*------------------------------------------------------------------------*/


/*!
Used by main thread to invalidate buffers
*/
void FTPClient:: InvalidateBuffers(){

	QWriteLocker lock(&m_RWLock);

	m_FileBuffer=NULL;
	m_Done=0;
	m_Total=0;
	m_lstDirContent.clear();

}
