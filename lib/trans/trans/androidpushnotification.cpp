#include "androidpushnotification.h"
#include <QUrl>

QString AndroidPushNotification::m_strGooglePushServerURL="";
QString AndroidPushNotification::m_strGoogleAuthenticationID="";


AndroidPushNotification::AndroidPushNotification(ThreadSynchronizer *sync,int nTimeOut)
	:PushNotification(sync,nTimeOut)
{
	m_pHttp = new QHttp;
	connect(m_pHttp, SIGNAL(done(bool)),this, SLOT(onRequestDone(bool)));
	connect(m_pHttp, SIGNAL(sslErrors(const QList<QSslError>&)),this, SLOT(onSslErrors(const QList<QSslError> &)));
}

AndroidPushNotification::~AndroidPushNotification()
{

}

void AndroidPushNotification::SendPushMessage()
{
	if (m_lstMessageCustomData.size()<3)
	{
		m_nStatusCode=1;
		m_strStatusText="Internal error: no exact parameters in the custom list!";
		m_ThreadSync->ThreadRelease();
		return;
	}

	QStringList lstRecipients;
	lstRecipients<<m_strPushToken1;

	QString strReipients = JsonEncodeRecipients(lstRecipients);

	//NOTE: see here: http://developer.android.com/guide/google/gcm/gcm.html
    QString strMsgJSON = QString(
		"{"
			"\"registration_ids\" : [%1],"
            "\"data\" : {"
                "\"message\":\"%2\","
				"\"vibrate\":1,"
				"\"sound\":\"default\","
				"\"type\":\"%3\","
				"\"id\":\"%4\""
				"\"database\":\"%5\""
            "}"
        "}").arg(strReipients).arg(m_strMessage).arg(m_lstMessageCustomData.at(0)).arg(m_lstMessageCustomData.at(1)).arg(m_lstMessageCustomData.at(2));
 

	QUrl url(m_strGooglePushServerURL);
	if (url.scheme()=="https")
		m_pHttp->setHost(url.host(), QHttp::ConnectionModeHttps, 443);
	else
		m_pHttp->setHost(url.host(), QHttp::ConnectionModeHttp);


	// Add these headers to all requests
	QList<QPair<QString, QString> > lstHeaders;
	lstHeaders << QPair<QString, QString>("Host", url.host());
	lstHeaders << QPair<QString, QString>("Content-Type", "application/json");
	lstHeaders << QPair<QString, QString>("Authorization", QString("key=%1").arg(m_strGoogleAuthenticationID));

	QHttpRequestHeader httpRequest;
	httpRequest.setRequest("POST", m_strGooglePushServerURL); 
	httpRequest.setValues(lstHeaders);

	m_pHttp->request(httpRequest, strMsgJSON.toLocal8Bit());
	m_nTimerID=startTimer(m_nTimeOut); //security check
}



void AndroidPushNotification::Destroy()
{
	if(m_pHttp)
	{
		m_pHttp->close();
		m_pHttp->deleteLater();
		m_pHttp=NULL;
	}
	deleteLater();
	m_ThreadSync->ThreadRelease();
}


void AndroidPushNotification::onRequestDone(bool bError)
{
	if (!m_pHttp)
		return;

	m_nStatusCode=0;
	m_strStatusText="";
	
	if (bError) //if
	{
		m_nStatusCode=1;
		m_strStatusText=m_pHttp->errorString();
	}
	else
	{
		QByteArray arData = m_pHttp->readAll();
		QHttpResponseHeader	hdr =  m_pHttp->lastResponse();

		//authentication success, check if messages are ok:
		if (hdr.statusCode()==200)
		{
			if (arData.length()>0)
			{
				//look up for failure tag and if >0 then look up for error desc:
				/*
					{"multicast_id":6782339717028231855,"success":0,"failure":1,"canonical_ids":0,"results":[{"error":"InvalidRegistration"}]}
				*/
				int nPos=arData.indexOf("failure");
				if (nPos>0)
				{
					QString strCntFailures=arData.mid(arData.indexOf(":",nPos)+1, arData.indexOf(",",nPos)-arData.indexOf(":",nPos)-1).trimmed();
					bool bOK=false;
					int nFailures=strCntFailures.toInt(&bOK);
					if (bOK && nFailures>0)
					{
						m_nStatusCode=1;
						//extract error string:
						nPos=arData.indexOf("error");
						if (nPos>0)
						{
							m_strStatusText=arData.mid(arData.indexOf(":",nPos)+1, arData.indexOf("}",nPos)-arData.indexOf(":",nPos)-1).trimmed();
							m_strStatusText.replace("\"",""); //remove quotas
						}
					}
					/*
					else
					{
						//B.T. even if it is not parsed...just set error
						m_nStatusCode=1;
						m_strStatusText="Unknown error!";
					}
					*/
				}
			}
		}
		else if (hdr.statusCode()==401)
		{
			m_nStatusCode=1;
			m_strStatusText="Google authentication failed!";
		}
		else if (hdr.statusCode()!=400)
		{
			m_nStatusCode=1;
			m_strStatusText="Google failed to parse JSON request!";
		}
		else
		{
			m_nStatusCode=1;
			m_strStatusText="Google internal server error!";
		}
	}

	if (m_nStatusCode==1)
		if(m_strStatusText.isEmpty())
			m_strStatusText=QString("Failed to send push message to the Google server. PushToken: %1").arg(m_strPushToken1);
		else
			m_strStatusText=QString("Failed to send push message to the Google server. PushToken: %1. Server error: %2").arg(m_strPushToken1).arg(m_strStatusText);

	m_pHttp->close();
	m_ThreadSync->ThreadRelease();
}

void AndroidPushNotification::onSslErrors(const QList<QSslError> &error)
{
	m_nStatusCode=1;
	if (error.size()>0)
		m_strStatusText="Failed to send push notification, SSL error: "+error.at(0).errorString();
	if(m_pHttp)
	{
		m_pHttp->close();
	}
	m_ThreadSync->ThreadRelease();
}

void AndroidPushNotification::OnTimeOut()
{
	if(m_pHttp)
	{
		if (m_pHttp->state()==QHttp::Unconnected || m_pHttp->state()==QHttp::HostLookup || m_pHttp->state()==QHttp::Connecting)
		{
			m_nStatusCode=1;
			m_strStatusText = QString("Failed to send Apple push notification. PushToken: %1. Error: %2").arg(m_strPushToken1).arg("timeout");
			m_ThreadSync->ThreadRelease();
		}
	}
}


QString AndroidPushNotification::JsonEncodeRecipients(const QStringList &lstRecipients)
{
	QString strResult;
	int nCnt = lstRecipients.length();
	for(int i=0; i<nCnt; i++){
		if(!strResult.isEmpty())
			strResult += ",";
		strResult += "\"";
		strResult += lstRecipients[i];
		strResult += "\"";
	}
	return strResult;

}