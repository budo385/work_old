#include "windowpushnotification.h"
#include <QUrl>
#include "common/common/logger.h"


WindowPushNotification::WindowPushNotification(ThreadSynchronizer *sync,int nTimeOut)
	:PushNotification(sync,nTimeOut)
{
	m_pHttp = new QHttp;
	connect(m_pHttp, SIGNAL(done(bool)),this, SLOT(onRequestDone(bool)));
	connect(m_pHttp, SIGNAL(sslErrors(const QList<QSslError>&)),this, SLOT(onSslErrors(const QList<QSslError> &)));
}

WindowPushNotification::~WindowPushNotification()
{

}

void WindowPushNotification::SendPushMessage()
{
	if (m_lstMessageCustomData.size()<3)
	{
		m_nStatusCode=1;
		m_strStatusText="Internal error: no exact parameters in the custom list!";
		m_ThreadSync->ThreadRelease();
		return;
	}

	int nPriority = ToastImmediately;

	QString strMsg=m_lstMessageCustomData.at(0)+"-"+m_lstMessageCustomData.at(1)+"-"+m_lstMessageCustomData.at(2);
	QString strMsgXML = QString(
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
		"<wp:Notification xmlns:wp=\"WPNotification\">"
		"<wp:Toast>"
		"<wp:Text1>%1</wp:Text1>"
		"<wp:Text2>%2</wp:Text2>"
		"<wp:Param>/LoginPage.xaml?info=%3</wp:Param>"
		"</wp:Toast>"
		"</wp:Notification>").arg("MFootball").arg(m_strMessage).arg(strMsg);

	//qDebug()<<strMsgXML;
    
	QList<QPair<QString, QString> > lstHeaders;
	lstHeaders << QPair<QString, QString>("X-WindowsPhone-Target", "toast");
	lstHeaders << QPair<QString, QString>("X-NotificationClass", QString("%1").arg(nPriority));

	//m_strPushToken1.replace("http://","https://");

	QUrl url(m_strPushToken1);
	if (url.scheme()=="https")
		m_pHttp->setHost(url.host(), QHttp::ConnectionModeHttps, 443);
	else
		m_pHttp->setHost(url.host(), QHttp::ConnectionModeHttp);

	// Add these headers to all requests
	lstHeaders << QPair<QString, QString>("Host", url.host());
	lstHeaders << QPair<QString, QString>("Content-Type", "text/xml");
	lstHeaders << QPair<QString, QString>("Accept", "application/*");

	QHttpRequestHeader httpRequest;
	httpRequest.setRequest("POST", m_strPushToken1);
	httpRequest.setValues(lstHeaders);

	m_pHttp->request(httpRequest, strMsgXML.toLocal8Bit());
	m_nTimerID=startTimer(m_nTimeOut); //security check
}



void WindowPushNotification::Destroy()
{
	if(m_pHttp)
	{
		m_pHttp->close();
		m_pHttp->deleteLater();
		m_pHttp=NULL;
	}
	deleteLater();
	m_ThreadSync->ThreadRelease();
}


void WindowPushNotification::onRequestDone(bool bError)
{
	//qDebug()<<"response received";

	if (!m_pHttp)
		return;

	m_nStatusCode=0;
	m_strStatusText="";
	QByteArray arData;

	if (bError) //if
	{
		m_nStatusCode=1;
		m_strStatusText=m_pHttp->errorString();
	}
	else
	{
		arData = m_pHttp->readAll();
		QHttpResponseHeader	hdr =  m_pHttp->lastResponse();

		qDebug()<<arData;

		/*
		qDebug() << "Subscription status:" << hdr.allValues("X-SubscriptionStatus");
		qDebug() << "Message status:" << hdr.allValues("X-NotificationStatus");
		qDebug() << "Device status:" << hdr.allValues("X-DeviceConnectionStatus");
		*/
		//authentication success, check if messages are ok:
		if (hdr.statusCode()!=200)
		{
			m_nStatusCode=1;
			m_strStatusText = hdr.allValues("X-NotificationStatus").join(",");
			m_strStatusText.append(arData);
		}
	}

	if (m_nStatusCode==1)
	{
		if(m_strStatusText.isEmpty())
			m_strStatusText=QString("Failed to send push message to the Windows server. PushToken: %1").arg(m_strPushToken1);
		else
			m_strStatusText=QString("Failed to send push message to the Windows server. PushToken: %1. Server error: %2").arg(m_strPushToken1).arg(m_strStatusText);

		qDebug()<<"Push Error:";
		QString strBody(arData.constData());
		qDebug()<<strBody;
	}
	else
	{
		qDebug()<<"Push OK:";
		QString strBody(arData.constData());
		qDebug()<<strBody;
	}


	m_pHttp->close();
	m_pHttp=NULL;
	m_ThreadSync->ThreadRelease();
}

void WindowPushNotification::onSslErrors(const QList<QSslError> &lst)
{
	int nSize=lst.count();
	for(int i=0;i<nSize;i++)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_NETWORK_GENERAL_MSG,"Win Push SSL Error: "+lst.at(i).errorString());
	}


	m_nStatusCode=1;
	if (lst.size()>0)
		m_strStatusText="Failed to send push notification, SSL error: "+lst.at(0).errorString();
	if(m_pHttp)
	{
		m_pHttp->close();
	}
	m_ThreadSync->ThreadRelease();
}

void WindowPushNotification::OnTimeOut()
{
	if(m_pHttp)
	{
		if (m_pHttp->state()==QHttp::Unconnected || m_pHttp->state()==QHttp::HostLookup || m_pHttp->state()==QHttp::Connecting)
		{
			m_nStatusCode=1;
			m_strStatusText = QString("Failed to send Apple push notification. PushToken: %1. Error: %2").arg(m_strPushToken1).arg("timeout");
			m_ThreadSync->ThreadRelease();
		}
	}
}



