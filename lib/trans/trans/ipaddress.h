#ifndef IPADDRESS_H
#define IPADDRESS_H	

#include <QString>

/*!
    \class IpAddressAdv
    \brief data container for IP address
    \ingroup IPModule
*/
class IpAddressAdv
{
public:
	//constructors
	IpAddressAdv(){m_strIP="";m_nPort=0;m_nTries=0;};
	IpAddressAdv(const IpAddressAdv &that);

	//operators
	IpAddressAdv &operator =(const IpAddressAdv &that); 
	bool operator ==(const IpAddressAdv &that); 
	bool operator < (const IpAddressAdv &that) const; 

	//aux functions:
	bool SetIpAddress(QString ip);
	QString GetIpAddress();

	QString m_strIP;
	int m_nPort;
	int m_nTries;
};



#endif //IPADDRESS_H

