#include "ipaccesslist.h"

IPAccessList::IPFilterEntry::IPFilterEntry()
{
	m_nIP = 0;
	m_nFilterBits = 0;
	m_bAllow = true;
}

IPAccessList::IPFilterEntry::IPFilterEntry(const IPFilterEntry &that)
{
	operator =(that);
}

void IPAccessList::IPFilterEntry::operator =(const IPFilterEntry &that)
{
	m_nIP = that.m_nIP;
	m_nFilterBits = that.m_nFilterBits;
	m_bAllow = that.m_bAllow;

	Q_ASSERT(0 <= m_nFilterBits && m_nFilterBits <= 32);	//support only IPv4
}

int IPAccessList::IPFilterEntry::CheckMatch(unsigned long nIP)
{
	//prepare variables to be matched
	unsigned long nIPMask1 = m_nIP;
	unsigned long nIPMask2 = nIP;
	if(m_nFilterBits > 0)
	{
		int nMoveBits = 32 - m_nFilterBits;

		//lose the right-most bits that are not supposed to be matched
		nIPMask1 = nIPMask1 >> nMoveBits;
		nIPMask1 = nIPMask1 << nMoveBits;

		nIPMask2 = nIPMask2 >> nMoveBits;
		nIPMask2 = nIPMask2 << nMoveBits;
	}

	//check matching
	if(nIPMask1 == nIPMask2)
	{
		//match found
		if(m_bAllow)
			return IP_MATCH_ALLOWED;
		else
			return IP_MATCH_DENIED;
	}
	else
		return IP_NO_MATCH;	// IP does not match the filter
}

IPAccessList::IPAccessList()
{
}

IPAccessList::~IPAccessList()
{
}

unsigned long IPAccessList::IpStringToNum(QString strIP)
{
	unsigned long nIP = 0;

	int nPos1 = strIP.section('.', 0, 0).toInt();	//1st segment
	int nPos2 = strIP.section('.', 1, 1).toInt();	//2nd segment
	int nPos3 = strIP.section('.', 2, 2).toInt();	//3rd segment
	int nPos4 = strIP.section('.', 3, 3).toInt();	//4th segment
	
	nIP = (nPos1 << 24) + (nPos2 << 16) + (nPos3 << 8) + nPos4;

	return nIP;
}


void IPAccessList::AddEntry(QString strIP, int nFilterBits, bool bAllow, int nPos,int nKickBanTime)
{
	IPFilterEntry entry;
	entry.m_nIP = IpStringToNum(strIP);
	entry.m_nFilterBits = nFilterBits;
	entry.m_bAllow = bAllow;

	if(nPos < 0)
		m_lstAccessList.append(entry);
	else
		m_lstAccessList.insert(nPos, entry);
}




void IPAccessList::RemoveEntry(QString strIP)
{
	unsigned long nIP=IpStringToNum(strIP);
	
	for(int i=0;m_lstAccessList.size();++i)
	{
		if(m_lstAccessList.at(i).m_nIP==nIP)
		{
			m_lstAccessList.removeAt(i);
			return;
		}

	}
}


/*!
	Checks coming IP address to stored access list

	\param strIP		- ip address
	\return 			- true if ip address is allowed
*/
bool IPAccessList::IsIPAllowed(QString strIP)
{
	unsigned long nIP = IpStringToNum(strIP);

	int nMax = m_lstAccessList.size();
	for(int i=0; i<nMax; i++)
	{
		int nRes = m_lstAccessList[i].CheckMatch(nIP);
		if(IP_MATCH_ALLOWED == nRes)
			return true;
		if(IP_MATCH_DENIED == nRes)
			return false;
	}

	return true;	// we passed entire filter chain with no match, allow the IP
}

