#include "httpserverping.h"
#include <QTcpSocket>
#include <QSslSocket>
#include "trans/trans/config_trans.h"
#include "common/common/config_version_sc.h"
#include "httprequest.h"
#include "httpresponse.h"
#include "httputil.h"

HTTPServerPing::HTTPServerPing(int nTimeOut)
/*: HttpReaderSync(nTimeOut),*/:m_strIP("")
{
	m_nTimeOut=nTimeOut;
}

HTTPServerPing::~HTTPServerPing()
{

}

HTTPServerPing::HTTPServerPing(const HTTPServerPing &that)
{
	operator = (that);
}
HTTPServerPing &HTTPServerPing::operator =(const HTTPServerPing &that)
{
	if(this != &that)
	{
		m_SessionCache= that.m_SessionCache;
		m_strIP=that.m_strIP;
		m_nPort=that.m_nPort;
		m_bSSL=that.m_bSSL;
		m_nTimeOut=that.m_nTimeOut;
	}
	return *this;
}


void HTTPServerPing::SetParameters(QString strIP,int nPort, bool bSSL,Authenticator &SessionCache)
{
	m_SessionCache=SessionCache;
	m_strIP=strIP;
	m_bSSL=bSSL;
	m_nPort=nPort;
}
bool HTTPServerPing::Ping()
{
	if (m_strIP.isEmpty())
		return false;

	HTTPRequest httpRequest;
	httpRequest.setContentType("application/x-www-form-urlencoded");
	httpRequest.setValue("Connection","close");
	httpRequest.setValue("Host",m_strIP);
	httpRequest.setContentLength(0);

	QString strClientNonce=QString(Authenticator::GenerateRandomSequence(QString::number(m_nPort).toLatin1()).toHex()).toUtf8();
	QString strNonce=m_SessionCache.GenerateAuthenticationTokenForWebService(m_SessionCache.m_strUserName,m_SessionCache.m_strPassword,strClientNonce);
	QString strAuth="client_nonce=\""+strClientNonce+"\" nonce=\""+strNonce+"\"";
	httpRequest.setValue("auth",strAuth);

	QByteArray byteResponse;
	//QByteArray clientNonce,clientAuthToken; //32bytes long cnonce & auth token
	//m_SessionCache.GenerateAuthenticationToken(clientNonce,clientAuthToken);//create cnonce
	//urlencode: ?user=x&auth=y&mlid, mcode, parent_sess
	QString strURL=QString("/")+DOMAIN_HTTP_PING+"?";
	strURL+="user="+m_SessionCache.m_strUserName+"&";
	strURL+="parent_session="+QUrl::toPercentEncoding(m_SessionCache.m_SessionID); //will test if this session exists and running..

	//strURL=QUrl::toPercentEncoding(strURL);
	httpRequest.setRequest("GET",strURL);
	QByteArray requestData=httpRequest.toString().toLatin1();
	
	if (m_bSSL)
	{
		QSslSocket socket;
		socket.connectToHostEncrypted(m_strIP, m_nPort);
		socket.ignoreSslErrors();
		if (!socket.waitForEncrypted()) 
		{
			qDebug() << socket.errorString();
			return false;
		}
		socket.write(requestData);
		if(socket.waitForReadyRead(m_nTimeOut))
		{
			byteResponse=socket.readAll();
		}
		else
			return false;
	}
	else
	{
		QTcpSocket socket;
		socket.connectToHost(m_strIP, m_nPort);
		if (!socket.waitForConnected()) 
		{
			qDebug() << socket.errorString();
			return false;
		}
		socket.write(requestData);
		if(socket.waitForReadyRead(m_nTimeOut))
		{
			byteResponse=socket.readAll();
		}
		else
			return false;
	}

	int nType,nHdrEnd;
	qint64 nLength;
	HTTPUtil::CheckHTTPHeader(byteResponse,nType,nLength,nHdrEnd);
	if (nType!=HTTPUtil::HTTP_HEADER_RESPONSE)
		return false;

	QHttpResponseHeader header = QHttpResponseHeader(QString(byteResponse.left(nHdrEnd)));
	byteResponse=byteResponse.mid(nHdrEnd);
	HTTPResponse resp(header,byteResponse);

	if(resp.GetBody()->trimmed()=="OK" && header.statusCode()==200)
		return true;

	return false;
}
