#include "rpcstubmessagehandler.h"


/*! Constructor: makes instances of rpcrequest & rpcresponse message handlers
	fill them with buffers from HTTP messages. When this object is destroyed msg 
	instances are deleted automatically

	\param stub - pointer to stub object, must have GetRPCMessageHandler method 
*/
RpcStubMessageHandler::RpcStubMessageHandler(RpcProtocol *stub)
{	
	stub->GetRPCMessageHandler(&msg_in);msg_in->SetBuffer(HttpRequest.GetBody());
	stub->GetRPCMessageHandler(&msg_out);msg_out->SetBuffer(HttpResponse.GetBody());
}



/*! Destructor: free memory */
RpcStubMessageHandler::~RpcStubMessageHandler(){
	delete msg_in; delete msg_out;
}



