#ifndef RESTHTTPCLIENT_H
#define RESTHTTPCLIENT_H

#include "trans/trans/httpclientconnectionsettings.h"
#include "trans/trans/restrpcmessage.h"
#include "trans/trans/httpclientconnection.h"

class RestHTTPClient : public HTTPClientThread
{

public:
	RestHTTPClient();
	~RestHTTPClient();

public:

	enum RestLoginType
	{
		REST_TYPE_JAMES=0,
		REST_TYPE_COMMUNICATOR=1
	};
	//-----------------------------------------
	//Main interface general http sync client
	//-----------------------------------------
	void SetConnectionSettingsURL(QString strURL,int nConnectTimeOut=10);
	void SetConnectionSettings(QString strIP,int nPort,bool bUseSSL=true,int nConnectTimeOut=10);
	void SetConnectionSettings(HTTPClientConnectionSettings& pConnection);
	HTTPClientConnectionSettings GetConnectionSettings();
	void Connect(Status& pStatus);
	void Disconnect(Status& pStatus);
	void SendData(Status& pStatus, HTTPRequest* pHTTPRequest, HTTPResponse* pHTTPResponse=NULL);	//Implementation of SendData
	void ReConnect(Status& pStatus,QString strIP,int nPort);
	void SetDefaultHttpRequestHeader(HTTPRequest& request, QString strHttpMethod,QString strPath);

	//-----------------------------------------
	//REST
	//-----------------------------------------
	void RestClearBuffers();
	void RestStartSession(Status& pStatus,int nRestLoginType, QString strUserName,QString strPassword,QString &strSession);
	void RestGetCurrentSessionData(QString &strSession,int &nPersonID, int &nContactID);
	void RestEndSession(Status& pStatus,int nRestLoginType, QString strSession);
	void RestSend(Status& pStatus,QString strHttpMethod, QString strPath,QString strWebSession="",bool bSkipGenerateRPCRequest=false);
	void RestParseResponse(Status& pStatus);
	QByteArray* GetRestSendBody(){return m_HttpRequest.GetBody();};
	QByteArray* GetRestRecvBody(){return m_HttpResponse.GetBody();};
	RestRpcMessage	msg_send;			//<pointer to RPC message handler of client request
	RestRpcMessage	msg_recv;		//<pointer to RPC message handler of response

private:
	QString m_strHost;

	HTTPRequest		m_HttpRequest;	//<HTTP request message 
	HTTPResponse	m_HttpResponse;	//<HTTP response message

	//only for communicator login:
	int m_nPersonID;
	int m_nContactID;
	QString m_strSession;
};

#endif // RESTHTTPCLIENT_H
