#include "resthttpclient.h"
#include "common/common/authenticator.h"
#include "common/common/logger.h"
#include "common/common/datahelper.h"

RestHTTPClient::RestHTTPClient()
{
	msg_send.SetBuffer(m_HttpRequest.GetBody());
	msg_recv.SetBuffer(m_HttpResponse.GetBody());
}
RestHTTPClient::~RestHTTPClient()
{
	Status err;
	Disconnect(err);
}
void RestHTTPClient::RestClearBuffers()
{
	m_HttpRequest.GetBody()->clear();
	m_HttpResponse.GetBody()->clear();
	msg_send.ClearData();
	msg_recv.ClearData();
}

void RestHTTPClient::RestSend(Status& pStatus,QString strHttpMethod, QString strPath,QString strWebSession,bool bSkipGenerateRPCRequest)
{
	//generate xml body envelope
	if (!bSkipGenerateRPCRequest)
	{
		msg_send.GenerateRPC("",pStatus);
		if(!pStatus.IsOK())return;
	}

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RestSend/SetDefaultHttpRequestHeader is about to be executed"));

	//set def http header
	SetDefaultHttpRequestHeader(m_HttpRequest,strHttpMethod,strPath);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RestSend/SetDefaultHttpRequestHeader is executed"));
	if (strWebSession.isEmpty())
		strWebSession = m_strSession;

	if (!strWebSession.isEmpty())
	{
		m_HttpRequest.setValue("Cookie","web_session = "+strWebSession+";");
	}

	//qDebug()<<m_HttpRequest.toString();
	//qDebug()<<*(m_HttpRequest.GetBody());

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RestSend/SendData is about to be executed"));

	//response should be inside response buffer
	SendData(pStatus, &m_HttpRequest, &m_HttpResponse);
	if (!pStatus.IsOK())
	{
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RestSend/SendData is executed and failed, status text: %1, status code %2 ").arg(pStatus.getErrorText()).arg(pStatus.getErrorCode()));
		return;
	}

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RestSend/SendData is executed"));

	//password handler:
	/*
	int nStatusCode=m_HttpResponse.statusCode();
	if (nStatusCode==401 && !strUserName.isEmpty()) 
	{
		QString strWWWAuth=m_HttpResponse.value("WWW-Authenticate");
		QStringList lstTokens=strWWWAuth.split("\"",QString::SkipEmptyParts);
		if (lstTokens.size()>=2)
		{
			QString strServerNonce=lstTokens.at(1);
			QString strLastAuth=Authenticator::GenerateAuthenticationTokenForWebService(strUserName,strPassword,strServerNonce);
			strLastAuth="SokratesAuth user=\""+strUserName+"\" token=\""+strLastAuth+"\" nonce=\""+strServerNonce+"\"";
			m_HttpRequest.setValue("auth", strLastAuth);

			//send again:
			SendData(pStatus, &m_HttpRequest, &m_HttpResponse);
		}
	}
	*/

	int nStatusCode=m_HttpResponse.statusCode();
	if (nStatusCode==401)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

	Status errParsing;
	msg_recv.StartResponseParsing(errParsing);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RestSend/StartResponseParsing is executed, error parsing code: %1,  text: %2, nStatusCode =%3").arg(errParsing.getErrorCode()).arg(errParsing.getErrorText()).arg(nStatusCode));
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("RestSend/StartResponseParsing is executed, error pStatus code: %1,  text: %2, nStatusCode =%3").arg(pStatus.getErrorCode()).arg(pStatus.getErrorText()).arg(nStatusCode));

	//chek for REST error and HTTP errors:
	if (pStatus.IsOK())
	{
		if (nStatusCode!=200)
		{
			if (errParsing.IsOK())
			{
				//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("msg_recv.CheckForFault"));
				msg_recv.CheckForFault(pStatus);
				//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Done msg_recv.CheckForFault"));
			}
			else
			{
				if (!msg_recv.GetBuffer())
				{
					//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("msg_recv.GetBuffer()"));
					pStatus.setError(1,"HTTP error: "+QString::number(nStatusCode)+", text: "+msg_recv.GetBuffer()->left(1000));
				}
				else
				{
					//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("msg_recv.GetBuffer() IS NULL"));
					pStatus.setError(1,"Unknown HTTP error");
				}

				//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Done msg_recv.GetBuffer()"));
			}

			if (pStatus.IsOK())
			{
				pStatus.setError(1,"Error while trying to send request to the server");
			}

			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Return with status befor exit: %1").arg(pStatus.getErrorText()));
			return;
		}
	}

	//qDebug()<<msg_recv.GetBuffer()->constData();

	if (!errParsing.IsOK())
		pStatus=errParsing;

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Return with status on exit: %1").arg(pStatus.getErrorText()));
}

void RestHTTPClient::RestParseResponse(Status& pStatus)
{
	msg_recv.StartResponseParsing(pStatus);
}


void RestHTTPClient::SetConnectionSettingsURL(QString strURL,int nConnectTimeOut)
{
	QUrl url(strURL);
	int nPort=url.port();
	bool bUseSSL=false;
	if (url.scheme()=="https")
		bUseSSL=true;
	SetConnectionSettings(url.host(),nPort,bUseSSL,nConnectTimeOut);
}

void RestHTTPClient::SetConnectionSettings(QString strIP,int nPort,bool bUseSSL,int nConnectTimeOut)
{
	HTTPClientConnectionSettings settings;
	settings.m_strServerIP=strIP;
	settings.m_nPort=nPort;
	settings.m_bUseSSL=bUseSSL;
	settings.m_nConnectTimeOut=nConnectTimeOut;
	SetConnectionSettings(settings);
}
void RestHTTPClient::SetConnectionSettings(HTTPClientConnectionSettings& pConnSettings)
{
	//if connected/destroy connection:
	Status err;
	err.setError(0);
	if(IsConnected())
		HTTPClientThread::Disconnect(err);

	if (pConnSettings.m_nPort>0)
		m_strHost=pConnSettings.m_strServerIP+":"+QString::number(pConnSettings.m_nPort);
	else
		m_strHost=pConnSettings.m_strServerIP;

	HTTPClientThread::SetConnectionSettings(pConnSettings); //sets conn settings
}

HTTPClientConnectionSettings RestHTTPClient::GetConnectionSettings(){
	return HTTPClientThread::GetConnectionSettings(); //gets conn settings
}

void RestHTTPClient:: Connect(Status& pStatus)
{
	HTTPClientConnectionSettings tmpJBLinux=HTTPClientThread::GetConnectionSettings(); //je.. Linux
	HTTPClientThread::Connect(pStatus,tmpJBLinux);
}

void RestHTTPClient::ReConnect(Status& pStatus,QString strIP,int nPort){

	//if connected, broke connection:
	if(IsConnected())
		{ Disconnect(pStatus);if(!pStatus.IsOK())return;}
	
	//set new IP address:
	HTTPClientConnectionSettings conn=HTTPClientThread::GetConnectionSettings();
	conn.m_strServerIP=strIP;
	if(nPort!=0)conn.m_nPort=nPort;
	SetConnectionSettings(conn);

	//try to connect:
	Connect(pStatus);
}

void RestHTTPClient::Disconnect(Status& pStatus){
	HTTPClientThread::Disconnect(pStatus);
}

void RestHTTPClient::SendData(Status& pStatus, HTTPRequest* pHTTPRequest, HTTPResponse* pHTTPResponse)
{
	if(!IsConnected())
		{ Connect(pStatus);if(!pStatus.IsOK())return;}
	
	HTTPClientThread::SendData(pStatus,pHTTPRequest,pHTTPResponse);
}


void RestHTTPClient::SetDefaultHttpRequestHeader(HTTPRequest& request, QString strHttpMethod,QString strPath)
{
	//set request:
	request.setRequest(strHttpMethod, strPath);
	if(m_strHost!="")
		request.setValue("Host",m_strHost);
	request.setValue("User-agent","HTTP Client"); 
	request.setValue("Content-Type","text/xml");
	request.setValue("Connection","Keep-Alive");
	request.setValue("Accept-Encoding","deflate");
	request.setValue("Content-Encoding","deflate");
}


void RestHTTPClient::RestStartSession(Status& pStatus,int nRestLoginType, QString strUserName,QString strPassword,QString &strSession)
{
	QString strNonce=QString(Authenticator::GenerateRandomSequence(QString::number(QDateTime::currentDateTime().time().msec()).toLatin1()).toHex()).toUtf8();
	QString strAuthToken=Authenticator::GenerateAuthenticationTokenForWebService(strUserName,strPassword,strNonce);

	QString strBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	QString strPath;

	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strOldSession></strOldSession>";
	strBody +="<strUserName>"+strUserName+"</strUserName>";
	strBody +="<strAuthToken>"+strAuthToken+"</strAuthToken>";
	strBody +="<strClientNonce>"+strNonce+"</strClientNonce>";
	strBody +="<nProgCode>1</nProgCode>";
	strBody +="<strProgVer></strProgVer>";
	strBody +="<strClientID></strClientID>";
	strBody +="<strPlatform></strPlatform>";


	if (nRestLoginType==REST_TYPE_JAMES)
	{
		strBody +="</PARAMETERS>";
		strBody +="</REQUEST>";
		strPath = "/rest/service/login";
	}
	else if (nRestLoginType==REST_TYPE_COMMUNICATOR)
	{
		int nClientTimeZone=DataHelper::GetTimeZoneOffsetUTC(); //this is offset from UTC as reference for server.
		strBody +="<nUTCtimezoneOffsetMinutes>"+QString::number(nClientTimeZone)+"</nUTCtimezoneOffsetMinutes>";
		strBody +="</PARAMETERS>";
		strBody +="</REQUEST>";
		strPath = "/rest/server/login";
	}
	
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"RestStartSession body:"+strBody);


	msg_send.GetBuffer()->clear();
	msg_send.GetBuffer()->append(strBody);

	SetDefaultHttpRequestHeader(m_HttpRequest,"POST",strPath);
	SendData(pStatus, &m_HttpRequest, &m_HttpResponse);

	msg_send.GetBuffer()->clear();

	int nStatusCode=m_HttpResponse.statusCode();
	if (nStatusCode==401)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

	Status errParsing;
	msg_recv.StartResponseParsing(errParsing);

	//chek for REST error and HTTP errors:
	if (pStatus.IsOK())
	{
		if (nStatusCode!=200)
		{
			if (errParsing.IsOK())
			{
				msg_recv.CheckForFault(pStatus);
			}
			else
			{
				pStatus.setError(1,"HTTP error: "+QString::number(nStatusCode)+", text: "+msg_recv.GetBuffer()->left(1000));
			}

			if (pStatus.IsOK())
			{
				pStatus.setError(1,"Error while trying to send request to the server");
			}
			return;
		}
	}

	if (!errParsing.IsOK())
		pStatus=errParsing;
	else
	{
		if (msg_recv.GetParameterCount()<1)
		{
			pStatus.setError(1,"Session creation failed, parameter count mismatch!");
			return;
		}
		msg_recv.GetParameter(0,&strSession,pStatus);
		m_strSession=strSession;
		if (nRestLoginType==REST_TYPE_COMMUNICATOR)
		{
			msg_recv.GetParameter(1,&m_nContactID,pStatus);
			msg_recv.GetParameter(2,&m_nPersonID,pStatus);
		}
	}
}


void RestHTTPClient::RestEndSession(Status& pStatus,int nRestLoginType,QString strSession)
{

	QString strBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	QString strPath;

	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strSessionID>"+strSession+"</strSessionID>";

	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";

	if (nRestLoginType==REST_TYPE_JAMES)
	{
		strPath = "/rest/service/logout";
	}
	else if (nRestLoginType==REST_TYPE_COMMUNICATOR)
	{
		strPath = "/rest/server/logout";
	}

	msg_send.GetBuffer()->clear();
	msg_send.GetBuffer()->append(strBody);
	SetDefaultHttpRequestHeader(m_HttpRequest,"POST",strPath);
	SendData(pStatus, &m_HttpRequest, &m_HttpResponse);

}

void RestHTTPClient::RestGetCurrentSessionData( QString &strSession,int &nPersonID, int &nContactID )
{
	strSession=m_strSession;
	nPersonID=m_nPersonID;
	nContactID=m_nContactID;
}

