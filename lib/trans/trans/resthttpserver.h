#ifndef RESTHTTPSERVER_H
#define RESTHTTPSERVER_H

#include "trans/trans/httphandler.h"
#include "trans/trans/rpcprotocol.h"
#include "trans/trans/rpcskeleton.h"
#include <QHash>

class RestHTTPServer : public HTTPHandler, protected RpcProtocol
{

public:
	RestHTTPServer(RpcSkeleton *pServerSkeletonSet,QString strWWWRootDir="");
	void HandleRequest(Status &err, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);
	void SetWebServiceURL(QString strURL){m_ServerSkeletonSet->SetWebServiceURL(strURL+"/rest");}

protected:
	virtual bool	AuthenticateUser(Status &err,HTTPContext &ctx,HTTPRequest &request,HTTPResponse &response, int &nClientTimeZoneOffsetMinutes)=0;	
	virtual	void	LockThreadActive()=0;
	virtual	void	UnLockThreadActive()=0;

	void			HandleSchemaRequest(Status &err, QString schemaName, HTTPRequest &request, HTTPResponse &response);
	void			SetResponseHeader(HTTPRequest &request,HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText);
	void			ResponseHandler(Status &err,HTTPRequest &request, HTTPResponse &response);
	void			LoadSchemas(Status &err,QString strFilePath);
	void			GenerateXMLError(Status &err,HTTPRequest &request,HTTPResponse &response);
	
	RpcSkeleton *m_ServerSkeletonSet;
	QHash<QString,QByteArray> m_hshSchemas;	//< collection of all xsd files used for webservices desc
	QString						m_strWWWRootDir;
	QDateTime			m_datLastSave;
	QHash<QString,int>	m_lstAPITiming;
	//QReadWriteLock		m_lck;
};



#endif // RESTHTTPSERVER_H
