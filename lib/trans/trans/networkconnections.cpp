#include "networkconnections.h"
#include "common/common/sha256hash.h"

NetworkConnections::NetworkConnections()
{
	m_nCurSel = -1;
}

NetworkConnections::~NetworkConnections()
{
}

void NetworkConnections::Clear()
{
	m_lstConnections.clear();
}

bool NetworkConnections::Load(QString strFile, QString strPassword)
{
	Clear();

	m_objIni.SetPassword(strPassword);
	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	//load a list of network connections
	int nCount = 0;
	m_objIni.GetValue("NetworkConnections", "Count", nCount);
	for(int i=0; i<nCount; i++)
	{
		//load a single NetworkConnection info
		QString strKey, strName, strHost, strIPLookupURL;
		QString strProxyUser, strProxyPass, strProxyHost;
		int nPort, nSSL, nCompression, nDbID, nUseDFO;
		int nUseProxy, nProxyType, nProxyPort;

		strKey.sprintf("Name%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, strName);
	
		strKey.sprintf("Host%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, strHost);

		strKey.sprintf("Port%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, nPort, 1111);

		strKey.sprintf("UseSSL%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, nSSL, 1);

		strKey.sprintf("UseCompression%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, nCompression, 1);
		
		strKey.sprintf("DatabaseID%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, nDbID, 0);

		strKey.sprintf("IPLookupURL%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, strIPLookupURL);

		strKey.sprintf("UseDFO%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, nUseDFO, 0);

		strKey.sprintf("UseProxy%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, nUseProxy, 0);

		strKey.sprintf("ProxyPort%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, nProxyPort, 0);

		strKey.sprintf("ProxyType%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, nProxyType, 0);

		strKey.sprintf("ProxyUser%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, strProxyUser);

		strKey.sprintf("ProxyPassword%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, strProxyPass);

		strKey.sprintf("ProxyHost%d", i+1);
		m_objIni.GetValue("NetworkConnections", strKey, strProxyHost);

		HTTPClientConnectionSettings info;
		info.m_strCustomName = strName;
		info.m_strServerIP   = strHost;
		info.m_nPort		 = nPort;
		info.m_bUseSSL		 = nSSL;
		info.m_bUseCompression = nCompression;
		info.m_nDatabaseID	= nDbID;
		info.m_strLookupURL	 = strIPLookupURL;
		info.m_bUseDFO		 = nUseDFO;
		info.m_bUseProxy	 = nUseProxy;
		info.m_nProxyPort	 = nProxyPort;
		info.m_nProxyType	 = nProxyType;
		info.m_strProxyHostName = strProxyHost;
		info.m_strProxyUsername = strProxyUser;
		info.m_strProxyPassWord = strProxyPass;
		
		m_lstConnections.append(info);
	}

	m_objIni.GetValue("NetworkConnections", "CurrentConnection", m_nCurSel, -1);
	return true;
}

bool NetworkConnections::Save(QString strFile, QString strPassword)
{
	m_objIni.SetPassword(strPassword);
	m_objIni.SetPath(strFile);

	//fill the data inside
	m_objIni.RemoveSection("NetworkConnections");	//cleanup existing data

	QString strKey;

	int nCnt = m_lstConnections.size();
	m_objIni.SetValue("NetworkConnections", "Count", nCnt);

	for(int i=0; i<nCnt; i++)
	{
		//save a single NetworkConnection info
		strKey.sprintf("Name%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_strCustomName);
	
		strKey.sprintf("Host%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_strServerIP);

		strKey.sprintf("Port%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_nPort);

		strKey.sprintf("UseSSL%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_bUseSSL);

		strKey.sprintf("UseCompression%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_bUseCompression);
		
		strKey.sprintf("DatabaseID%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_nDatabaseID);

		strKey.sprintf("IPLookupURL%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_strLookupURL);

		strKey.sprintf("UseDFO%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_bUseDFO);

		strKey.sprintf("UseProxy%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_bUseProxy);

		strKey.sprintf("ProxyPort%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_nProxyPort);

		strKey.sprintf("ProxyType%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_nProxyType);

		strKey.sprintf("ProxyUser%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_strProxyUsername);

		strKey.sprintf("ProxyPassword%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_strProxyPassWord);

		strKey.sprintf("ProxyHost%d", i+1);
		m_objIni.SetValue("NetworkConnections", strKey, m_lstConnections[i].m_strProxyHostName);
	}

	m_objIni.SetValue("NetworkConnections", "CurrentConnection", m_nCurSel);

	return m_objIni.Save();
}

void NetworkConnections::ClearUserPassword()
{
	m_objIni.SetValue("UserInfo", "UsePassword", 0);
	m_objIni.SetValue("UserInfo", "PasswordHash", "");
}

QString NetworkConnections::GetPasswordHash()
{
	QString strPassHash;
	m_objIni.GetValue("UserInfo", "PasswordHash", strPassHash);
	return strPassHash;
}

bool NetworkConnections::MatchUserPassword(QString strPassword)
{
	if(IsUserPasswordSet())
	{
		//compare password hashes (converted to hex)
		QString strPassHash = GetPasswordHash();
		return strPassHash == GetHashString(strPassword);
	}
	return false;
}

bool NetworkConnections::IsUserPasswordSet()
{
	int nUsePass = 0;
	m_objIni.GetValue("UserInfo", "UsePassword", nUsePass, 0);
	return (nUsePass > 0);
}

bool NetworkConnections::SetUserPasswordHash(QString strPassHash)
{
	m_objIni.SetValue("UserInfo", "UsePassword", 1);
	m_objIni.SetValue("UserInfo", "PasswordHash", strPassHash);
	return true;
}

QString NetworkConnections::GetHashString(QString strPassword)
{
	QByteArray data;
	data.append(strPassword);

	//calculate hash value
	QByteArray hash;
	Sha256Hash Hasher;
	hash = Hasher.GetHash(data);

	//convert it into the string (binary to Base64)
	QString strResult;
	strResult = hash.toBase64();
	return strResult;
}

