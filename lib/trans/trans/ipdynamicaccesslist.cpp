#include "ipdynamicaccesslist.h"


//-------------------------------------------------------------------
//					IP entry
//-------------------------------------------------------------------


IPDynamicAccessList::IPFilterEntry::IPFilterEntry(unsigned long nIP,unsigned short m_nFilterBits,bool bAllow)
{
	m_nIP = nIP;
	m_nFilterBits = m_nFilterBits;
	m_bAllow = bAllow;
	m_datBanTime=QDateTime();
	m_nCounter=0;
	Q_ASSERT(m_nFilterBits <= 32);	//support only IPv4
}



IPDynamicAccessList::IPFilterEntry::IPFilterEntry(const IPFilterEntry &that)
{
	operator =(that);
}

IPDynamicAccessList::IPFilterEntry &IPDynamicAccessList::IPFilterEntry::operator =(const IPFilterEntry &that)
{
	m_nIP = that.m_nIP;
	m_nFilterBits = that.m_nFilterBits;
	m_bAllow = that.m_bAllow;
	m_datBanTime = that.m_datBanTime;
	m_nCounter = that.m_nCounter;

	return *this;
}



int IPDynamicAccessList::IPFilterEntry::CheckMatch(unsigned long nIP)
{
	//prepare variables to be matched
	unsigned long nIPMask1 = m_nIP;
	unsigned long nIPMask2 = nIP;
	if(m_nFilterBits > 0)
	{
		int nMoveBits = 32 - m_nFilterBits;

		//lose the right-most bits that are not supposed to be matched
		nIPMask1 = nIPMask1 >> nMoveBits;
		nIPMask1 = nIPMask1 << nMoveBits;

		nIPMask2 = nIPMask2 >> nMoveBits;
		nIPMask2 = nIPMask2 << nMoveBits;
	}

	//check matching
	if(nIPMask1 == nIPMask2)
	{
		//test time:
		if(!m_datBanTime.isNull())
		{
			//qDebug()<<m_datBanTime<<"  "<<QDateTime::currentDateTime();
			if(m_datBanTime>QDateTime::currentDateTime()) //until greater, we are in ban period
				return IP_MATCH_DENIED;
			else
				return IP_MATCH_ALLOWED_REMOVE_ENTRY; //allow & remove entry
		}
		else
		{
			//match found
			if(m_bAllow)
				return IP_MATCH_ALLOWED;
			else
				return IP_MATCH_DENIED;
		}
	}
	else
		return IP_NO_MATCH;	// IP does not match the filter
}





//-------------------------------------------------------------------
//					IP manager
//-------------------------------------------------------------------



unsigned long IPDynamicAccessList::IpStringToNum(QString strIP)
{
	unsigned long nIP = 0;

	int nPos1 = strIP.section('.', 0, 0).toInt();	//1st segment
	int nPos2 = strIP.section('.', 1, 1).toInt();	//2nd segment
	int nPos3 = strIP.section('.', 2, 2).toInt();	//3rd segment
	int nPos4 = strIP.section('.', 3, 3).toInt();	//4th segment

	nIP = (nPos1 << 24) + (nPos2 << 16) + (nPos3 << 8) + nPos4;

	return nIP;
}


/*!
	Add IP address to kick list.
	Warning if same address already exist in list with same filter bits, only counter is updated!

	\param strIP		- IP address
	\param nFilterBits	- filter bits (to add whole domain or set of addresses)
	\param bAllow		- true allow	
	\param nPos			- position in list to add entry, -1 to add at end
	\param nBanTime		- (seconds) Ban time, bAllow is set to false until BanTime expires, then it is removed from list
	\param nBanCounter	- number of times, before same IP can be kicked, after kick becomes valid (permanent or time limited)

*/
void IPDynamicAccessList::AddEntry(QString strIP, int nFilterBits, bool bAllow, int nPos, int nBanTime,int nBanCounter)
{
	IPFilterEntry entry;
	entry.m_nIP = IpStringToNum(strIP);
	entry.m_nFilterBits = nFilterBits;
	entry.m_bAllow = bAllow;
	entry.m_nCounter = nBanCounter;


	//FIND IF EXISTS:
	int nIdx=-1;
	int nSize=m_lstAccessList.size();
	for(int i=0;i<nSize;++i)
	{
		if(m_lstAccessList.at(i).m_nIP==entry.m_nIP && m_lstAccessList.at(i).m_nFilterBits==entry.m_nFilterBits)
		{
			nIdx=i;
			break;
		}
	}
	

	//OLD ENTRY:
	if(nIdx!=-1)
	{
		int nCount=m_lstAccessList.at(nIdx).m_nCounter;
		nCount--;
		if(nCount==0) //only when first time drops to zero
		{
			m_lstAccessList[nIdx].m_bAllow=false; //counter drop zero: kick IP
			if(m_nAfterCounterKickTime>0)
			{
				QDateTime datBanExpire=QDateTime::currentDateTime().addSecs(m_nAfterCounterKickTime);
				m_lstAccessList[nIdx].m_datBanTime=datBanExpire;
			}
			else
			{
				m_lstAccessList[nIdx].m_datBanTime=QDateTime();	//set to NULL/ permanent kick
			}

		}
		else
			if(nCount>0) m_lstAccessList[nIdx].m_nCounter=nCount; //if still >0 store back value, we will still tolreate kicking until counter >0
		
		return; //exist, we are done
	}

	//NEW ENTRY:
	//if time set, calc expire date, until then deny access
	if(nBanTime>0)
	{
		QDateTime datBanExpire=QDateTime::currentDateTime().addSecs(nBanTime);
		qDebug()<<datBanExpire;
		entry.m_datBanTime=datBanExpire;
		entry.m_bAllow = false;
	}
	else if(nBanCounter>0) //if counter set: find if already exists, if so, decrease counter, when to 0 -> add to permanent ban or 24hrs kick period
	{
		if(nBanCounter==1)
			entry.m_bAllow = false;				//false if only 1 time allowed
		else
			entry.m_bAllow = true;				//true until count drops to 0
		
		entry.m_nCounter = nBanCounter-1;		//set counter, drop count by 1 
	}


	if(nPos < 0)
		m_lstAccessList.append(entry);
	else
		m_lstAccessList.insert(nPos, entry);
}



/*!
	Remove IP address from kick list

	\param strIP		- IP address

*/
void IPDynamicAccessList::RemoveEntry(QString strIP)
{
	unsigned long nIP=IpStringToNum(strIP);

	int nSize=m_lstAccessList.size();
	for(int i=0;i<nSize;++i)
	{
		if(m_lstAccessList.at(i).m_nIP==nIP)
		{
			m_lstAccessList.removeAt(i);
			return;
		}

	}
}


/*!
	Checks coming IP address to stored access list

	\param strIP		- ip address
	\return 			- true if ip address is allowed
*/
bool IPDynamicAccessList::IsIPAllowed(QString strIP)
{
	unsigned long nIP = IpStringToNum(strIP);

	int nMax = m_lstAccessList.size();
	for(int i=0; i<nMax; i++)
	{
		int nRes = m_lstAccessList[i].CheckMatch(nIP);
		if(IP_MATCH_ALLOWED == nRes)
			return true;
		if(IP_MATCH_DENIED == nRes)
			return false;
		if(IP_MATCH_ALLOWED_REMOVE_ENTRY == nRes)
		{
			m_lstAccessList.removeAt(i); //remove as kick ban period expired
			return true;
		}
	}

	return true;	// we passed entire filter chain with no match, allow the IP
}



/*!
	Destroys any content inside & stored new list.
	Warning: order list by ORDER (asc) before calling)

	\param lstAcess		- list of IP addresses in TVIEW_CORE_IPACCESSLIST

*/
void IPDynamicAccessList::LoadIPAccessList(DbRecordSet &lstAcess)
{
	m_lstAccessList.clear();

	int nSize=lstAcess.getRowCount();
	int nIPIdx=lstAcess.getColumnIdx("CIPR_IP_ADDRESS");
	int nFilterdx=lstAcess.getColumnIdx("CIPR_FILTER_BITS");
	int nAllowdx=lstAcess.getColumnIdx("CIPR_ALLOW");
	for(int i=0;i<nSize;++i)
	{
		IPFilterEntry ip(lstAcess.getDataRef(i,nIPIdx).toUInt(),lstAcess.getDataRef(i,nFilterdx).toInt(),lstAcess.getDataRef(i,nAllowdx).toBool());
		m_lstAccessList.append(ip);
	}


}


/*!
	Returns inside list in  TVIEW_CORE_IPACCESSLIST format.
	Note: only retruns IP's that are banned! = bAllow=false

	\param lstAcess		- list of IP addresses in TVIEW_CORE_IPACCESSLIST

*/
void IPDynamicAccessList::GetIPAccessList(DbRecordSet &lstAcess)
{
	//lstAcess.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_IPACCESSLIST)); no access to db_core
	lstAcess.destroy();
	lstAcess.addColumn(QVariant::Int,"CIPR_ID");
	lstAcess.addColumn(QVariant::String,"CIPR_GLOBAL_ID");
	lstAcess.addColumn(QVariant::DateTime,"CIPR_DAT_LAST_MODIFIED");
	lstAcess.addColumn(QVariant::String,"CIPR_LIST_NAME");
	lstAcess.addColumn(QVariant::UInt,"CIPR_IP_ADDRESS");
	lstAcess.addColumn(QVariant::Int,"CIPR_FILTER_BITS");
	lstAcess.addColumn(QVariant::Int,"CIPR_ORDER");
	lstAcess.addColumn(QVariant::Int,"CIPR_ALLOW");


	int nIPIdx=lstAcess.getColumnIdx("CIPR_IP_ADDRESS");
	int nFilterdx=lstAcess.getColumnIdx("CIPR_FILTER_BITS");
	int nAllowdx=lstAcess.getColumnIdx("CIPR_ALLOW");
	int nOrderIdx=lstAcess.getColumnIdx("CIPR_ORDER");
	
	int nMax = m_lstAccessList.size();
	for(int i=0; i<nMax; i++)
	{
		if(!m_lstAccessList.at(i).m_bAllow)
		{
			lstAcess.addRow();
			lstAcess.setData(i,nIPIdx,(uint)m_lstAccessList.at(i).m_nIP);
			lstAcess.setData(i,nFilterdx,m_lstAccessList.at(i).m_nFilterBits);
			lstAcess.setData(i,nAllowdx,(int)m_lstAccessList.at(i).m_bAllow);
			lstAcess.setData(i,nOrderIdx,i);
		}
	}

}

