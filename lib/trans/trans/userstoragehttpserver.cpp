#include "userstoragehttpserver.h"
#include <QUrlQuery>
#include <QUrl>


#include "trans/trans/config_trans.h"
#include "common/common/config_version_sc.h"
#include "trans/trans/httpfilestreamer.h"
#include "common/common/datahelper.h"
#include "trans/trans/httputil.h"
#include "common/common/logger.h"
#include "common/common/sha256hash.h"

#define FILE_SIZE_MAX_FOR_DIRECT_DOWNLOAD 5*1024*1024 //5Mb


UserStorageHTTPServer::UserStorageHTTPServer()
{
	SetDomain(DOMAIN_HTTP_USER_STORAGE);
}

UserStorageHTTPServer::~UserStorageHTTPServer()
{

}


/*!
	Invoked by server, new  thread for user request.
	Request is authenticated, if authentication fails: kick, if session not found, return http ok, but error inside body.

	/param status			- returned status (defines how http server will handle response)
	/param ctx				- HTTP Context   - some HTTP/IP info on client
	/param request			- HTTP request	- client request
	/param response			- HTTP response - server response
	/param pStreamHandler	- object for stream, if stream was in progress, this object should already exist!

	Use of user_storage:
	1. user, token, nonce, parent_session are required parameters (query string)
	2. method: ClearStorage() (if no filename, then all storage!!!)
	5. method: DownLoadFile(), Content-Disposition: attachment; filename="ZIP_Lookup_QCW.png" 
	6. method: UploadFile(), Content-Disposition: attachment; filename="ZIP_Lookup_QCW.png" 
	7. HTTP method: does not matter: can be POST (clear,upload) or GET(download)

	8.1 filename for download
	8. overwrite: yes/no (for upload)
	9. Return codes: 200. OK for success (body: none), or 500 Internal Server Error (body: error desc)

	A): binary to base64? content type!?...still to research
	B) upload/download backup? security? overwrite yes/no? error return=?

*/
void UserStorageHTTPServer::HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{
	QUrlQuery url= QUrlQuery(QUrl(request.path()));
	QString method=url.queryItemValue("method").toLatin1(); 

	//qDebug()<<request.toString();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"UserStorageHTTPServer processing("+ctx.m_strPeerIP+":"+QVariant(ctx.m_nSocketID).toString()+"): Processing user_storage http operation: "+request.path());

	int nMethodType=ResolveMethodType(method);
	if (nMethodType==METHOD_TYPE_DOWNLOAD)
	{
		Q_ASSERT((*pStreamHandler)==NULL);
		if(InitChunkedHttpStream(status,ctx,request,response,pStreamHandler)) //init chunk object
		{
			//status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);  //if ok, return to close connection
			//err.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			//keep connection:
			return;
		}
		return; //else status and response is formed..
	}
	else if (nMethodType==METHOD_TYPE_UPLOAD)
	{
		HTTPFileStreamer *pChunker=dynamic_cast<HTTPFileStreamer*>((*pStreamHandler)); //job is done..return ok
		if(pChunker)
		{
			pChunker->Close();
			if(!(*pStreamHandler)->GetLastError().IsOK())
			{
				pChunker->DeleteFile();
				delete (*pStreamHandler); //destroy streamer
				(*pStreamHandler)=NULL;

				SetResponseHeader(response,500,"Internal Server Error");
				response.GetBody()->append((*pStreamHandler)->GetLastError().getErrorText());
				pChunker->DeleteFile();
				//status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
				return;
			}

			if (pChunker->GetFileSize()!=pChunker->GetTotalStreamSize()&& pChunker->GetTotalStreamSize()!=0)
			{
				pChunker->DeleteFile();
				delete (*pStreamHandler); //destroy streamer
				(*pStreamHandler)=NULL;

				SetResponseHeader(response,500,"Internal Server Error");
				response.GetBody()->append("Error: CRC checksum failed!");
				//pChunker->DeleteFile();
				//status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
				return;
			}
			delete (*pStreamHandler); //destroy streamer
			(*pStreamHandler)=NULL;

			SetResponseHeader(response,200,"OK");
			status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); 
		}
		else
		{
			SetResponseHeader(response,400,"Bad Request");
			//status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
			response.GetBody()->append("Bad Entry point!");
			return;
		}

		return;
	}
	else if (nMethodType==METHOD_TYPE_CLEAR)
	{
		SetResponseHeader(response,200,"OK");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); 
		return;
	}
	else
	{
		
		if ((*pStreamHandler))
		{
			delete (*pStreamHandler); //destroy streamer
			(*pStreamHandler)=NULL;
		}

		SetResponseHeader(response,400,"Bad Request");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
		response.GetBody()->append("Method is not recognized!");
		return;
	}
}



bool UserStorageHTTPServer::InitChunkedHttpStream(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{
	status.setError(0);

	//--------------------------------------------------------
	// AUTHENTICATE USER
	//--------------------------------------------------------
	QUrlQuery url= QUrlQuery(QUrl(request.path()));
	QUrl urlPath= QUrl(request.path());
	QString strMethod=url.queryItemValue("method").toLatin1(); 

	QString strSession;

	//strSession = url.queryItemValue("web_session").toLatin1();

	
	QString strCookie=request.value("Cookie");
	if (strCookie.isEmpty() || strCookie.indexOf("web_session")<0)
	{
		strCookie="web_session = "+url.queryItemValue("web_session").toLatin1()+";";
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("UserStorageHTTPServer authenticate session: %1").arg(strCookie));
	
	if(!AuthenticateUser(strCookie,strSession))
	{
		SetResponseHeader(response,401,"Unauthorized");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN_NCOUNT);
		return false;
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("UserStorageHTTPServer session: %1 authenticated!").arg(strCookie));

	//find method type: upload, download or clear
	//---------------------------------------------------
	int nMethodType=ResolveMethodType(strMethod);
	if(nMethodType==METHOD_TYPE_UNKNOWN)
	{
		SetResponseHeader(response,400,"Bad Request");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
		response.GetBody()->append("Method is not recognized!");
		return false;
	}

	//get file name and path
	//---------------------------------------------------
	QString strDirPath;
	QString strAliasDir;
	QString strFileName;
	qint64 nFileSizeForUpload;
	GetFilePath(status,nMethodType,strSession,request,urlPath,strAliasDir,strDirPath,strFileName,nFileSizeForUpload);
	if (!status.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("UserStorageHTTPServer GetFilePath error: %1").arg(status.getErrorText()));

		SetResponseHeader(response,500,"Internal Server Error");
		response.GetBody()->append(status.getErrorText());
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
		return false;
	}

	//check permission/access right on specific file/dir:
	//---------------------------------------------------
	CheckPermission(status,strSession,nMethodType,strAliasDir,strDirPath,strFileName,nFileSizeForUpload);
	if (!status.IsOK())
	{
		SetResponseHeader(response,500,"Internal Server Error");
		response.GetBody()->append(status.getErrorText());
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
		return false;
	}

	return Method_Dispatcher(status,strMethod,nMethodType,strDirPath,strFileName,request,response,pStreamHandler);
}



//--------------------------------------------------------
// RESPONSE HEADER
//--------------------------------------------------------

void UserStorageHTTPServer::SetResponseHeader(HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(nHTTPStatusCode,strHTTPCodeText);
	response.setValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
	response.setValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setContentType("text/plain");
}


bool UserStorageHTTPServer::Method_Dispatcher(Status &status, QString strMethod, int nMethodType, QString strDirPath, QString strFileName, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{
	switch (nMethodType)
	{
		case METHOD_TYPE_DOWNLOAD:
			return Method_DownLoad(status,request,response,pStreamHandler,strDirPath,strFileName);
		case METHOD_TYPE_UPLOAD:
			return Method_Upload(status,request,response,pStreamHandler,strDirPath,strFileName);
		case METHOD_TYPE_CLEAR:
			return Method_Clear(status,request,response,pStreamHandler,strDirPath,strFileName);
		default:
		{
			SetResponseHeader(response,400,"Bad Request");
			status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
			response.GetBody()->append("Method is not recognized!");
			return false;
		}
	}

	return false;
}

void UserStorageHTTPServer::GetDirectoryPath(Status &status,const QUrl &url,QString strSession,QString &strDirPath)
{
	//url: urlstorage/dir/dir/filename

	QFileInfo info(url.path());
	QString strSubDir=info.absolutePath();
	strSubDir=QUrl::fromPercentEncoding(strSubDir.mid(strSubDir.indexOf(DOMAIN_HTTP_USER_STORAGE)+QString(DOMAIN_HTTP_USER_STORAGE).length()+1).toLatin1());

	//based on session resolve path, url is not needed:
	QString strUserSubDir=DataHelper::EncodeSession2Directory(strSession);
	strDirPath=InitializeUserStorageDirectory(status,strUserSubDir);

	if (!strSubDir.isEmpty())
	{
		strDirPath+="/"+strSubDir;
	}
}

int UserStorageHTTPServer::ResolveMethodType(QString strMethod)
{
	if (strMethod.left(8)=="download")
	{
		return METHOD_TYPE_DOWNLOAD;
	}
	else if (strMethod.left(6)=="upload")
	{
		return METHOD_TYPE_UPLOAD;
	}
	else if (strMethod=="clear")
	{
		return METHOD_TYPE_CLEAR;
	}
	else
		return METHOD_TYPE_DOWNLOAD;
}

QString UserStorageHTTPServer::GetFileName_Download(const QUrl &url)
{
	//get filename from url path:
	//QString strpath=QUrl(QUrl::fromPercentEncoding(request.path().toLatin1())).path();
	QString strpath=url.path(QUrl::FullyDecoded);
	QFileInfo info(strpath);
	return info.fileName();
}

QString UserStorageHTTPServer::GetFileName_Upload(const QUrl &url,const HTTPRequest &request,qint64 &nOriginalFileSize)
{
	QString strFileName,strFormName;
	bool bOK=HTTPUtil::ExtractFileInfoFromContentDisposition(request.value("Content-Disposition"),strFileName,nOriginalFileSize,strFormName);
	if (!bOK)
		return "";
	else
		return strFileName;
}


//------------------------------------------------------------------------------------------------------
//			BASIC OPERATION IMPLEMENTATIONS: download,upload,clear
//------------------------------------------------------------------------------------------------------

bool UserStorageHTTPServer::Method_DownLoad(Status &status, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler,QString strDirPath, QString strFileName)
{
	QString strAbsFilePath=strDirPath+"/"+strFileName;
	

	QFileInfo info(strAbsFilePath);

	//does file exists:
	if (!info.exists())
	{
		status.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
		SetResponseHeader(response,404,"File not found");
		response.GetBody()->append("404: File not found");
		return false;
	}

	//get file properties:
	QString strETag;
	QString strLastModified;
	GetFileProperties(info,strETag,strLastModified);



	//304: testing
	QString strTestLastModify=request.value("If-Modified-Since");
	if (!strTestLastModify.isEmpty())
	{
		if (strTestLastModify==strLastModified)
		{
			//qDebug()<<"304: "<<strFileName;
			//qDebug()<<"304: "<<strLastModified;
			//qDebug()<<"304: "<<strTestLastModify;
			SetResponseHeader(response,304,"");
			response.setValue("Last-Modified",strLastModified);
			status.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			return false;
		}
	}

	

	QString strTestETag=request.value("If-None-Match");
	if (!strTestETag.isEmpty())
	{
		if (strTestETag==strETag)
		{
			//qDebug()<<"304: "<<strFileName;
			//qDebug()<<"304: "<<strLastModified;
			//qDebug()<<"304: "<<strTestETag;
			
			SetResponseHeader(response,304,"");
			response.setValue("Last-Modified",strLastModified);
			status.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			return false;
		}
	}
//	if (strFileName.indexOf("060508qn5")>=0)
//	{
		//qDebug()<<strAbsFilePath<<": "<<strLastModified;
		//qDebug()<<"If-Modified-Since: "<<strTestLastModify;
		//qDebug()<<"If-None-Match: "<<strTestETag;
//	}

	

	bool bClientAcceptCompressData=(request.value("Accept-Encoding").indexOf("deflate")>=0);

	//load file if ok:
	if (info.size()<FILE_SIZE_MAX_FOR_DIRECT_DOWNLOAD)
	{
		QFile in_file(strAbsFilePath);
		if(!in_file.open(QIODevice::ReadOnly))
		{
			SetResponseHeader(response,500,"Internal Server Error");
			response.GetBody()->append("Error: failed to open file: "+strFileName);
			status.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			return false;
		}
		QByteArray buffer=in_file.readAll();
		if (buffer.size()==0)
		{
			in_file.close();
			SetResponseHeader(response,500,"Internal Server Error");
			response.GetBody()->append("Error: failed to read from file: "+strFileName);
			status.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			return false;
		}

		response.SetBody(buffer);

		SetResponseHeader(response,200,"OK");
		//response.setValue("Pragma","no-cache"); //;MP noticed bug: probably client is caching by default on session level: instruct to bypass cache
		response.setValue("Cache-Control","no-cache"); //;MP noticed bug: probably client is caching by default on session level: instruct to bypass cache
		response.setValue("Expires","Sat, 26 Jul 1997 05:00:00 GMT"); //;MP noticed bug: probably client is caching by default on session level: instruct to bypass cache
		////This must be expires: as some clients does not test with 304
		
		int nUseDeflate=-1; //0 use, -1 do not use;
		if(request.value("Accept-Encoding")=="deflate") //if request msg is compressed, use it back
			nUseDeflate=0;

		HTTPUtil::SetHeaderDataBasedOnFile(&response,strAbsFilePath,false,nUseDeflate); //deflate if above 2Kb
		response.setValue("Last-Modified",strLastModified);
		if (!bClientAcceptCompressData)
			response.removeValue("Content-Encoding");
		status.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);

/*
		if (strFileName.indexOf("060508qn5")>=0)
		{
			qDebug()<<request.toString();
			qDebug()<<response.toString();
		}
*/
		return true;
	}


	HTTPFileStreamer *pChunker=new HTTPFileStreamer;
	pChunker->InitFileOperation(status,strDirPath+"/"+strFileName,false,false);
	if (!status.IsOK())
	{
		SetResponseHeader(response,500,"Internal Server Error");
		response.GetBody()->append(QString("Failed to initialize file transfer. Reason: %1").arg(status.getErrorText()));
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
		//qDebug()<<QString(*response.GetBody());
		return false;
	}

	//rebuild response header:
	SetResponseHeader(response,200,"OK");
	HTTPUtil::SetHeaderDataBasedOnFile(&response,strDirPath+"/"+strFileName,true,-1); //BT: temporary to enable file download
	response.setValue("Transfer-Encoding","chunked");
	if (!bClientAcceptCompressData)
		response.removeValue("Content-Encoding");

	pChunker->SetResponseHeader(response);
	(*pStreamHandler) = (HTTPStreamHandler*)pChunker; 

	return true;
}

bool UserStorageHTTPServer::Method_Upload(Status &status, HTTPRequest &request,HTTPResponse &response, HTTPStreamHandler **pStreamHandler, QString strDirPath,QString strFileName)
{
	//overwrite?
	QUrlQuery url= QUrlQuery(QUrl(request.path()));
	QString overwrite=url.queryItemValue("overwrite").toLatin1(); 
	bool bOverWrite=false;
	if (overwrite=="yes")
		bOverWrite=true;

	//send & cont?
	bool bSendContinue=false;
	if (request.value("Except")=="100-continue")
		bSendContinue=true;

	HTTPFileStreamer *pChunker=new HTTPFileStreamer;
	pChunker->InitFileOperation(status,strDirPath+"/"+strFileName,true,bOverWrite);
	if (!status.IsOK())
	{
		SetResponseHeader(response,500,"Internal Server Error");
		response.GetBody()->append(QString("Failed to initialize file transfer. Reason: %1").arg(status.getErrorText()));
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
		return false;
	}
	pChunker->SetRequestHeader(request);
	(*pStreamHandler) = (HTTPStreamHandler*)pChunker; 
	if (!bSendContinue)
		SetResponseHeader(response,200,"OK");
	else
	{
		SetResponseHeader(response,100,"Continue");
		status.setError(StatusCodeSet::COM_HTTP_SEND_CONTINUE);
		return false;
	}

	return true;
}

bool UserStorageHTTPServer::Method_Clear(Status &status, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler, QString strDirPath,QString strFileName)
{
	if (strFileName.isEmpty())
	{
		bool bOK=DataHelper::RemoveAllFromDirectory(strDirPath,true);
		if (!bOK)
		{
			SetResponseHeader(response,500,"Internal Server Error");
			status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
			response.GetBody()->append(QString("Failed to delete content from user storage directory %1").arg(strDirPath));
			return false;
		}
	}
	else
	{
		bool bOK=QFile::remove(strDirPath+"/"+strFileName);
		if (!bOK)
		{
			SetResponseHeader(response,500,"Internal Server Error");
			status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
			response.GetBody()->append(QString("Failed to delete file %1").arg(strFileName));
			return false;
		}
	}

	return true;
}

//returns false if already exists:
bool UserStorageHTTPServer::AddDirPath (QString strAlias,QString strDirPath)
{
	QWriteLocker lock(&m_lckDirPaths);

	if (m_hshDirPaths.contains(strAlias))
		return false;
	m_hshDirPaths[strAlias]=strDirPath;
	return true;
}

void UserStorageHTTPServer::RemoveDirPath (QString strAlias)
{
	QWriteLocker lock(&m_lckDirPaths);
	m_hshDirPaths.remove(strAlias);
}


void UserStorageHTTPServer::GetFileProperties(QFileInfo &fileinfo, QString &strETag, QString &strLastModified)
{
	Sha256Hash Hasher;
	strLastModified=fileinfo.lastModified().toString(Qt::ISODate);
	QByteArray hashByteArray=QString::number(fileinfo.size()).toLatin1()+":"+strLastModified.toLatin1();
	strETag=QString(Hasher.GetHash(hashByteArray).toBase64()); //hash = size+":"+date;
}

void UserStorageHTTPServer::GetFilePath(Status &status,int nMethodType, QString strSession,const HTTPRequest &request, const QUrl &url,QString &strAliasDir,QString &strDirPath,QString &strFileName, qint64 &nFileSizeForUpload)
{
	strAliasDir="";
	nFileSizeForUpload=0;

	//find target directory: user storage or path
	//---------------------------------------------------
	GetDirectoryPath(status,url,strSession,strDirPath);
	if (!status.IsOK())
		return;

	//find target filename
	//---------------------------------------------------
	if (nMethodType==METHOD_TYPE_DOWNLOAD || nMethodType==METHOD_TYPE_CLEAR)
	{
		strFileName=GetFileName_Download(url);
	}
	else
	{
		strFileName=GetFileName_Upload(url,request,nFileSizeForUpload);
	}

	if (strFileName.isEmpty())
	{
		status.setError(1,"Filename not specified!");
	}
}