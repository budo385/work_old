#include "pushnotification.h"

PushNotification::PushNotification(ThreadSynchronizer *sync,int nTimeOut)
{
	m_ThreadSync=sync;
	m_nTimeOut=nTimeOut;
	m_nTimerID=-1;	
	m_nStatusCode=0;
	m_strStatusText="";
}

PushNotification::~PushNotification()
{

}

//Note: lstMessageCustomData is list of pairs: <label><value>, e.g. "type","0","id","1"...
void PushNotification::SetMessageData(QString strPushToken1, QString strPushToken2, QString strMessage, int nMessageID, QStringList lstMessageCustomData, QString strPushSoundName, int nBadgeNumber)
{
	m_strPushToken1=strPushToken1;
	m_strPushToken2=strPushToken2;
	m_strMessage=strMessage;
	m_nMessageID=nMessageID;
	m_lstMessageCustomData = lstMessageCustomData;
	m_strPushSoundName = strPushSoundName;
	m_nBadgeNumber = nBadgeNumber;
}

void PushNotification::ResetTimeOut()
{
	if (m_nTimerID>0)
	{
		if(m_nTimerID!=-1)killTimer(m_nTimerID);
		m_nTimerID=-1;
	}
}

void PushNotification::timerEvent(QTimerEvent *event)
{
	//out timer is here, then fire Time out error, save our asses!!!
	if(event->timerId()==m_nTimerID)
	{
		OnTimeOut();
		killTimer(m_nTimerID);
		m_nTimerID=-1;
	}
}