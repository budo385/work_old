#include "xmlrpcvalue.h"
#include "xmlutil.h"



/*------------------------------------------------------------------------*/
/*								  FAULT VALUE							  */
/*------------------------------------------------------------------------*/

/*!
	Converts errorcode/text to xml, clears buffer!
    \param nErrorCode error code
    \param strErrorText error text
*/
void XmlRpcValueFault::Serialize(int nErrorCode, QString strErrorText){

	/* build XML header: method name */
	m_byteXMLMsg->clear();

	// manually construct FAULT STRUCTURE: TOFIX later can be automatic...
	*m_byteXMLMsg+=FAULT_TAG;
	*m_byteXMLMsg+=VALUE_TAG;
	*m_byteXMLMsg+=STRUCT_TAG;
	*m_byteXMLMsg+=MEMBER_TAG;
	*m_byteXMLMsg+=NAME_TAG;
	*m_byteXMLMsg+=FAULTCODE;
	*m_byteXMLMsg+=NAME_ETAG;

	//add error code:
	QVariant nqvErrorCode((int)nErrorCode);
	xmlValInteger.Serialize(&nqvErrorCode); 

	*m_byteXMLMsg+=MEMBER_ETAG;
	*m_byteXMLMsg+=MEMBER_TAG;
	*m_byteXMLMsg+=NAME_TAG;
	*m_byteXMLMsg+=FAULTSTRING;
	*m_byteXMLMsg+=NAME_ETAG;

	//add error text:
	xmlValString.Serialize(&strErrorText); 

	*m_byteXMLMsg+=MEMBER_ETAG;
	*m_byteXMLMsg+=STRUCT_ETAG;
	*m_byteXMLMsg+=VALUE_ETAG;
	*m_byteXMLMsg+=FAULT_ETAG;
}


/*!
	Checks if error exists and if so convert to errorcode/string
	Note: if error occur during parsing it will be also placed into errcode/text
    \param nErrorCode error code, 0 if no error in XML message
    \param strErrorText error text
*/
void XmlRpcValueFault::DeSerialize(int &nErrorCode, QString &strErrorText){

	//init to no error
	nErrorCode=StatusCodeSet::ERR_NONE;
	strErrorText="";
	int nStartOffset=0,nLength=0;
	Status statusTmp;

	//check if FAULT exists if not return clean
	if (m_byteXMLMsg->indexOf(FAULT_TAG)<0)return;


	//-----------------------------
	//parse error code tag
	//-----------------------------
	// find MEMBER tag:
	XmlUtil::GetTagContent(m_byteXMLMsg,MEMBER_TAG,MEMBER_ETAG,nStartOffset,nLength);
	if(nStartOffset<0||nLength<0)
	{
		//if not found return error
		nErrorCode=StatusCodeSet::ERR_XMLRPC_PARSE_FAIL;
		strErrorText="XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__);
		return;
	}

	//instruct for Integer deserialize of error code:
	QVariant nqvErrorCode(QVariant::Int);
	xmlValInteger.DeSerialize(nStartOffset,nLength,&nqvErrorCode,statusTmp);
	if(!statusTmp.IsOK())
	{
		nErrorCode=statusTmp.getErrorCode();
		strErrorText=statusTmp.getErrorTextRaw();
	}
	nErrorCode=nqvErrorCode.toInt();	


	//-----------------------------
	//parse error text tag
	//-----------------------------
	// find "2nd MEMBER tag:
	nStartOffset=0;nLength=0;
	XmlUtil::GetTagContent(m_byteXMLMsg,MEMBER_TAG,MEMBER_ETAG,nStartOffset,nLength,1);
	if(nStartOffset<0||nLength<0)
	{
		//if not found return error
		nErrorCode=StatusCodeSet::ERR_XMLRPC_PARSE_FAIL;
		strErrorText="XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__);
		return;
	}

	//instruct for Integer deserialize of error code:
	xmlValString.DeSerialize(nStartOffset,nLength,&strErrorText,statusTmp);
	if(!statusTmp.IsOK())
	{
		nErrorCode=statusTmp.getErrorCode();
		strErrorText=statusTmp.getErrorTextRaw();
		return;
	}


}


/*!
    SetBuffer
	\param buffer reference to the external buffer
*/

void XmlRpcValueFault::SetBuffer(QByteArray *pBuffer){
		m_byteXMLMsg=pBuffer;
		xmlValString.SetBuffer(m_byteXMLMsg);
		xmlValInteger.SetBuffer(m_byteXMLMsg);
}


																			
/*------------------------------------------------------------------------*/
/*								  STRING								  */
/*------------------------------------------------------------------------*/

/*!
	Converts string to xml with escape char encoding (<,>,',",&)+ UTF8
	XML data is append to the buffer
    \param pValue reference to string data

*/
void XmlRpcValueString::Serialize(QString *pValue){
		*m_byteXMLMsg+=VALUE_TAG; //no need to add type TAG because string is default type
	if(!pValue->isNull())	
		*m_byteXMLMsg+=XmlUtil::xmlEncode(*pValue).toUtf8(); //escape special chars: \/& etc...+utf8 encode
	else
		*m_byteXMLMsg+=NULL_VALUE;

	*m_byteXMLMsg+=VALUE_ETAG;

}

/*!
	Overloaded function, same as above.
    \param pValue reference to string data
*/
void XmlRpcValueString::Serialize(QVariant *pValue){
		*m_byteXMLMsg+=VALUE_TAG; //no need to add type TAG because string is default type
	if(!pValue->isNull())	
		*m_byteXMLMsg+=XmlUtil::xmlEncode(pValue->toString()).toUtf8(); //escape special chars: \/& etc...+utf8 encode
	else
		*m_byteXMLMsg+=NULL_VALUE;

	*m_byteXMLMsg+=VALUE_ETAG;
}



/*!
	Converts xml to \a pValue string with escape char encoding (<,>,',",&)+ UTF8
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content (-1 if end tag not found)
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
*/
void XmlRpcValueString::DeSerialize(int &nStartOffset, int nLength, QString *pValue,Status& pStatus){

	// find VALUE tag:
	XmlUtil::GetTagContent(m_byteXMLMsg,VALUE_TAG,VALUE_ETAG,nStartOffset,nLength);
	if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

	//extract content
	QByteArray byteValueContent =m_byteXMLMsg->mid(nStartOffset,nLength);


	if (byteValueContent.indexOf(NULL_VALUE)>=0) //test if NULL
		*pValue=QString(); //set to NULL;
	else
	{
		//report error if something inside (shouldnt be):
		//if (byteValueContent.indexOf("<")>=0)
		//	{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}
		//extract value
		*pValue=pValue->fromUtf8(XmlUtil::xmlDecode(byteValueContent)); //decode and convert UTF8

	}

	//update start offset to one char after last TAG:
	nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;
	pStatus.setError(StatusCodeSet::ERR_NONE); //reset err to 0
}



/*------------------------------------------------------------------------*/
/*								  BOOL									  */
/*------------------------------------------------------------------------*/


/*!
	Converts bool to xml
	XML data is append to the buffer
    \param pValue reference to data
*/
void XmlRpcValueBool::Serialize(QVariant *pValue){
	
	*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=BOOLEAN_TAG;
	
	if(!pValue->isNull())	
		*m_byteXMLMsg+=(pValue->toBool()==true ? "1": "0"); 
	else
		*m_byteXMLMsg+=NULL_VALUE;

	*m_byteXMLMsg+=BOOLEAN_ETAG;
	*m_byteXMLMsg+=VALUE_ETAG;
}


/*!
	Converts xml to bool
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content (-1 if end tag not found)
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
*/
void XmlRpcValueBool::DeSerialize(int &nStartOffset, int nLength, QVariant *pValue,Status& pStatus){

	// find VALUE tag:
	//XmlUtil::GetTagContent(m_byteXMLMsg,VALUE_TAG,VALUE_ETAG,nStartOffset,nLength);
	//	if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	// find BOOL tag:
	XmlUtil::GetTagContent(m_byteXMLMsg,BOOLEAN_TAG,BOOLEAN_ETAG,nStartOffset,nLength);
		if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}

		//extract content
	QByteArray byteValueContent =m_byteXMLMsg->mid(nStartOffset,nLength);


	if (byteValueContent.indexOf(NULL_VALUE)>=0) //test if NULL
		*pValue=QVariant(QVariant::Bool); //sets to NULL!
	else
	{
		//report error if something inside (shouldnt be):
		//if (byteValueContent.indexOf("<")>=0)
		//	{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}
		//extract value
		*pValue=(byteValueContent=="1" ? true: false);
	}

	//update start offset to one char after last TAG:
	nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;
	pStatus.setError(StatusCodeSet::ERR_NONE); //reset err to 0
}


/*------------------------------------------------------------------------*/
/*								  INTEGER								  */
/*------------------------------------------------------------------------*/


/*!
	Converts integer to xml
	XML data is append to the buffer
    \param pValue reference to data
*/
void XmlRpcValueInteger::Serialize(QVariant *pValue){
	
	*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=I4_TAG;
	
	if(!pValue->isNull())	
		*m_byteXMLMsg+=pValue->toString().toLatin1();
	else
		*m_byteXMLMsg+=NULL_VALUE;

	*m_byteXMLMsg+=I4_ETAG;
	*m_byteXMLMsg+=VALUE_ETAG;
}


/*!
	Converts xml to integer
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content (-1 if end tag not found)
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
*/
void XmlRpcValueInteger::DeSerialize(int &nStartOffset, int nLength, QVariant *pValue,Status& pStatus){

	// find VALUE tag:
	//XmlUtil::GetTagContent(m_byteXMLMsg,VALUE_TAG,VALUE_ETAG,nStartOffset,nLength);
	//	if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	// find I4 tag:
	XmlUtil::GetTagContent(m_byteXMLMsg,I4_TAG,I4_ETAG,nStartOffset,nLength);
		if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}


	//extract content
	QByteArray byteValueContent =m_byteXMLMsg->mid(nStartOffset,nLength);

	if (byteValueContent.indexOf(NULL_VALUE)>=0) //test if NULL
		*pValue=QVariant(QVariant::Int); //sets to NULL!
	else
		{
		//report error if something inside (shouldnt be):
		//if (byteValueContent.indexOf("<")>=0)
		//	{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}

		//extract value
		*pValue=byteValueContent.toInt();
		}

	//update start offset to one char after last TAG:
	nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;

	pStatus.setError(StatusCodeSet::ERR_NONE); //reset err to 0
}


/*------------------------------------------------------------------------*/
/*								  DOUBLE								  */
/*------------------------------------------------------------------------*/


/*!
	Converts double to xml
	XML data is append to the buffer
    \param pValue reference to data
*/
void XmlRpcValueDouble::Serialize(QVariant *pValue){
	
	*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=DOUBLE_TAG;
	
	if(!pValue->isNull())	
		*m_byteXMLMsg+=pValue->toString().toLatin1();
	else
		*m_byteXMLMsg+=NULL_VALUE;

	*m_byteXMLMsg+=DOUBLE_ETAG;
	*m_byteXMLMsg+=VALUE_ETAG;
}


/*!
	Converts xml to double
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content (-1 if end tag not found)
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
*/
void XmlRpcValueDouble::DeSerialize(int &nStartOffset, int nLength, QVariant *pValue,Status& pStatus){

	// find VALUE tag:
	//XmlUtil::GetTagContent(m_byteXMLMsg,VALUE_TAG,VALUE_ETAG,nStartOffset,nLength);
	//	if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	// find tag:
	XmlUtil::GetTagContent(m_byteXMLMsg,DOUBLE_TAG,DOUBLE_ETAG,nStartOffset,nLength);
		if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}

	//extract content
	QByteArray byteValueContent =m_byteXMLMsg->mid(nStartOffset,nLength);

	if (byteValueContent.indexOf(NULL_VALUE)>=0) //test if NULL
		*pValue=QVariant(QVariant::Double); //sets to NULL!
	else
		{
		//report error if something inside (shouldnt be):
	//	if (byteValueContent.indexOf("<")>=0)
		//	{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}
		//extract value
		*pValue=byteValueContent.toDouble();
		}
	
		//update start offset to one char after last TAG:
	nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;
	pStatus.setError(StatusCodeSet::ERR_NONE); //reset err to 0
}



/*------------------------------------------------------------------------*/
/*								  DATE TIME								  */
/*------------------------------------------------------------------------*/


/*!
	Converts datetime to xml (ISO 8601 YYYY-MM-DDTHH:MM:SS)
	XML data is append to the buffer
    \param pValue reference to data
*/
void XmlRpcValueDateTime::Serialize(QDateTime *pValue){
	
	*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=DATETIME_TAG;
	
	if(!pValue->isNull() && pValue->isValid())	
	{
		if (m_nTimeZoneOffsetSeconds)
			*m_byteXMLMsg+=(pValue->addSecs(-m_nTimeZoneOffsetSeconds)).toString(Qt::ISODate); //ISO 8601 YYYY-MM-DDTHH:MM:SS
		else
			*m_byteXMLMsg+=pValue->toString(Qt::ISODate); //ISO 8601 YYYY-MM-DDTHH:MM:SS

		//if (!bSkipUTCTransform)
		//	*m_byteXMLMsg+=DateTimeHandler::toUTC(*pValue).toString(Qt::ISODate); //ISO 8601 YYYY-MM-DDTHH:MM:SS
		//else
			
	}
	else
		*m_byteXMLMsg+=NULL_VALUE;

	*m_byteXMLMsg+=DATETIME_ETAG;
	*m_byteXMLMsg+=VALUE_ETAG;
}

/*!
	Converts date to xml with time=00
	XML data is append to the buffer
    \param pValue reference to data
*/
void XmlRpcValueDateTime::Serialize(QDate *pValue)
{
	QDateTime tmp=QDateTime(*pValue,QTime(0,0));
	Serialize(&tmp);
}


/*!
	Converts time to xml with date=curent
	XML data is append to the buffer
    \param pValue reference to data
*/
void XmlRpcValueDateTime::Serialize(QTime *pValue)
{
	QDateTime tmp=QDateTime(QDate::currentDate(),*pValue);
	Serialize(&tmp);
}


/*!
	Converts time to xml with date=curent
	XML data is append to the buffer
    \param pValue reference to data
*/
void XmlRpcValueDateTime::Serialize(QVariant *pValue)
{
	switch(pValue->type()){
		case QVariant::DateTime:
			{
				QDateTime tmp=pValue->toDateTime();
				Serialize(&tmp);
				break;
			}

		case QVariant::Date:
			{
				QDateTime tmp=QDateTime(pValue->toDate(),QTime(0,0));
				Serialize(&tmp);
				break;
			}
			
		case QVariant::Time: 
			{
				QDateTime tmp=QDateTime(QDate::currentDate(),pValue->toTime());
				Serialize(&tmp);
				break;
			}
		default:
			Q_ASSERT(false); //type not supported
	}
}


/*!
	Converts xml to Datetime
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content (-1 if end tag not found)
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
*/
void XmlRpcValueDateTime::DeSerialize(int &nStartOffset, int nLength, QDateTime *pValue,Status& pStatus){

	// find VALUE tag:
	//XmlUtil::GetTagContent(m_byteXMLMsg,VALUE_TAG,VALUE_ETAG,nStartOffset,nLength);
	//	if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

	// find DATETIME_TAG tag:
	XmlUtil::GetTagContent(m_byteXMLMsg,DATETIME_TAG,DATETIME_ETAG,nStartOffset,nLength);
		if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}

	//extract content
	QByteArray byteValueContent =m_byteXMLMsg->mid(nStartOffset,nLength);

	if (byteValueContent.indexOf(NULL_VALUE)>=0) //test if NULL
		*pValue=QDateTime(); //sets to NULL!
	else
		{
		//report error if something inside (shouldnt be):
		//if (byteValueContent.indexOf("<")>=0)
		//	{pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}

			//extract value
			//if (!bSkipUTCTransform)
			//	*pValue=DateTimeHandler::fromUTC(QDateTime::fromString(byteValueContent,Qt::ISODate));
			//else
			
			if (m_nTimeZoneOffsetSeconds)
				*pValue=QDateTime::fromString(byteValueContent,Qt::ISODate).addSecs(m_nTimeZoneOffsetSeconds);
			else
				*pValue=QDateTime::fromString(byteValueContent,Qt::ISODate);
		}

	//update start offset to one char after last TAG:
	nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;
	pStatus.setError(StatusCodeSet::ERR_NONE); //reset err to 0
}

/*!
	Converts xml to Date
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content (-1 if end tag not found)
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
*/
void XmlRpcValueDateTime::DeSerialize(int &nStartOffset, int nLength, QDate *pValue,Status& pStatus){

	QDateTime datTemp;
	DeSerialize(nStartOffset, nLength, &datTemp,pStatus);
	if (!datTemp.isNull())
		*pValue=datTemp.date(); //extract date part
	else
		*pValue=QDate(); //set to NULL
}

/*!
	Converts xml to Time
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content (-1 if end tag not found)
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
*/
void XmlRpcValueDateTime::DeSerialize(int &nStartOffset, int nLength, QTime *pValue,Status& pStatus){

	QDateTime datTemp;
	DeSerialize(nStartOffset, nLength, &datTemp,pStatus);
	if (!datTemp.isNull())
		*pValue=datTemp.time(); //extract time part
	else
		*pValue=QTime(); //set to NULL
}



/*------------------------------------------------------------------------*/
/*								  BINARY								  */
/*------------------------------------------------------------------------*/


/*!
	Converts binary to xml
	XML data is append to the buffer
    \param pValue reference to data
*/
void XmlRpcValueBinary::Serialize(QByteArray *pValue){
	
	*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=BASE64_TAG;
	
	if(!pValue->isNull())	
	{
//#ifndef QT_NO_DEBUG
//		QTime TestServerResponseTime;
//		TestServerResponseTime.start();
//		qDebug()<<"Base64: original size:"<<pValue->size();
//#endif
		*m_byteXMLMsg+=pValue->toBase64(); 
//#ifndef QT_NO_DEBUG
//		int nElapsed=TestServerResponseTime.elapsed();
//		qDebug()<<"Time Elapsed:"<<nElapsed<<" for base 64:"<<m_byteXMLMsg->size();
//#endif
	}
	else
		*m_byteXMLMsg+=NULL_VALUE;

	*m_byteXMLMsg+=BASE64_ETAG;
	*m_byteXMLMsg+=VALUE_ETAG;
}

/*!
	Overloaded function, same as above.
    \param pValue reference to data
*/
void XmlRpcValueBinary::Serialize(QVariant *pValue){
	
	*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=BASE64_TAG;
	
	if(!pValue->isNull())
	{
//#ifndef QT_NO_DEBUG
//		QTime TestServerResponseTime;
//		TestServerResponseTime.start();
//		qDebug()<<"Base64: original size:"<<((QByteArray*)pValue)->size();
//#endif
		*m_byteXMLMsg+=((QByteArray*)pValue)->toBase64(); 
//#ifndef QT_NO_DEBUG
//		int nElapsed=TestServerResponseTime.elapsed();
//		qDebug()<<"Time Elapsed:"<<nElapsed<<" for base 64:"<<m_byteXMLMsg->size();
//#endif
	}
	else
		*m_byteXMLMsg+=NULL_VALUE;

	*m_byteXMLMsg+=BASE64_ETAG;
	*m_byteXMLMsg+=VALUE_ETAG;
}



/*!
	Converts xml to binary
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content (-1 if end tag not found)
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
*/
void XmlRpcValueBinary::DeSerialize(int &nStartOffset, int nLength, QByteArray *pValue,Status& pStatus){

	// find VALUE tag:
	//XmlUtil::GetTagContent(m_byteXMLMsg,VALUE_TAG,VALUE_ETAG,nStartOffset,nLength);
	//	if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}
	// find tag:
	XmlUtil::GetTagContent(m_byteXMLMsg,BASE64_TAG,BASE64_ETAG,nStartOffset,nLength);
		if(nStartOffset<0||nLength<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_RPC_WRONG_DATA_TYPE);return;}

	//extract content BT: 06.10.2010!!! -> convert NILL to value..!?
	if (m_byteXMLMsg->mid(nStartOffset,nLength) == NULL_VALUE)
		*pValue=QByteArray(); //sets to NULL!
	else
		*pValue=XmlUtil::fromBase64(m_byteXMLMsg,nStartOffset,nLength);

	/*
	if (pValue->size()!=6)
		else
	{
		if (pValue->indexOf(NULL_VALUE)>=0)
			
		else
			*pValue=XmlUtil::fromBase64(m_byteXMLMsg,nStartOffset,nLength);
	}
	*/

	//update start offset to one char after last TAG:
	nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;
	pStatus.setError(StatusCodeSet::ERR_NONE); //reset err to 0
}




/*------------------------------------------------------------------------*/
/*								  DB RECORD SET							  */
/*------------------------------------------------------------------------*/



/*!
    SetBuffer
	\param buffer reference to the external buffer
*/

void XmlRpcValueDbRecordSet::SetBuffer(QByteArray *pBuffer){
		m_byteXMLMsg=pBuffer;
		xmlValString.SetBuffer(m_byteXMLMsg);
		xmlValBool.SetBuffer(m_byteXMLMsg);
		xmlValInteger.SetBuffer(m_byteXMLMsg);
		xmlValDouble.SetBuffer(m_byteXMLMsg);
		xmlValDateTime.SetBuffer(m_byteXMLMsg);
		xmlValBinary.SetBuffer(m_byteXMLMsg);
}

/*!
	Converts DbRecordSet into array of arrays (to xml)
	XML data is append to the buffer

	DBRecordSet:
	<struct>
	   <member>
		  <name>columns</name>
		  <value><array of column def></value>
		  </member>
	   <member>
		  <name>data</name>
		  <value><array of data></value>
		  </member>
	  </struct>

	 NOTE: if DbRecordSet is empty (NULL): send as string: <value><struct><nil/></struct></value>
    \param pValue reference to data
	\param pSkipColumDef - if 1 then expects that column defintion wil not be serialized,
	Warning: can not use this parameter if using nested RecordSets->ASSERT will be fired
	Warning: on other side, accepting recordset must be defined before getting parameter

*/
void XmlRpcValueDbRecordSet::Serialize(DbRecordSet *pValue,bool pBoolSkipColumnDefinition){

	
	int i,j,nDataType;
	QString strName, strAlias, strTable;
	QVariant* data;
	XmlRpcValueDbRecordSet xmlDbRecordSet; //for nested recordsets
	xmlDbRecordSet.SetBuffer(m_byteXMLMsg); //set buffer


	//check if empty: then set to NULL string
	int nColumnCount=pValue->getColumnCount();
	if(nColumnCount==0)
	{	
		*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=STRUCT_TAG;
		*m_byteXMLMsg+=NULL_VALUE;
		*m_byteXMLMsg+=STRUCT_ETAG;*m_byteXMLMsg+=VALUE_ETAG; 
		return;
	}

	
	//start struct sequence:
	*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=STRUCT_TAG;


	//------------------------------------------------------
	//column types:
	//------------------------------------------------------
	if(!pBoolSkipColumnDefinition)
	{	//start member+array:
		*m_byteXMLMsg+=MEMBER_TAG;*m_byteXMLMsg+=NAME_TAG;
		*m_byteXMLMsg+="columns";*m_byteXMLMsg+=NAME_ETAG;
		*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;


		for(i=0;i<nColumnCount;++i)
		{
			//get column data
			pValue->getColumn(i,nDataType,strName);//,strAlias,strTable);

			//start array for 1 column defintion:
			*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;
			
			QVariant tmp(nDataType);
			xmlValInteger.Serialize(&tmp);
			xmlValString.Serialize(&strName);
			//xmlValString.Serialize(&strAlias);
			//xmlValString.Serialize(&strTable);

			//end array for 1 column defintion:
			*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG;

		}
		//end array for all column defintions:
		*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG;*m_byteXMLMsg+=MEMBER_ETAG;

	}
	//------------------------------------------------------
	//data:
	//------------------------------------------------------

	//start member+array:

	//start member+array:
	*m_byteXMLMsg+=MEMBER_TAG;*m_byteXMLMsg+=NAME_TAG;
	*m_byteXMLMsg+="data";*m_byteXMLMsg+=NAME_ETAG;
	*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;

	int nRowCount=pValue->getRowCount();
	for(i=0;i<nRowCount;++i)
	{
		//start array for 1 row
		*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;


			for(j=0;j<nColumnCount;++j)
			{

				data=&(pValue->getDataRef(i,j));

				switch(data->type()){

					case QVariant::String:
						xmlValString.Serialize(data); break;

					case QVariant::Double:
						xmlValDouble.Serialize(data); break;

					case QVariant::Bool:
						xmlValBool.Serialize(data); break;

					case QVariant::Int:
						xmlValInteger.Serialize(data); break;

					case QVariant::ByteArray:
						xmlValBinary.Serialize(data); break;

					case QVariant::Date:
						xmlValDateTime.Serialize(data); break;

					case QVariant::DateTime:
						xmlValDateTime.Serialize(data); break;

					case QVariant::Time:
						xmlValDateTime.Serialize(data); break;

					default:
						//check for default data type:
						if( data->userType()==DbRecordSet::GetVariantType())
						{
							DbRecordSet childSet=(pValue->getDataRef(i,j)).value<DbRecordSet>();
							xmlDbRecordSet.Serialize(&childSet);
						}
						else
							Q_ASSERT(0); //data type not supported!!
					
				}

			}

			//end array for one row:
		*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG;
		}


	//end member+array:
	*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG; *m_byteXMLMsg+=MEMBER_ETAG;

	//end whole struct:
	*m_byteXMLMsg+=STRUCT_ETAG;*m_byteXMLMsg+=VALUE_ETAG;

}

/*!
	Overloaded function. Same as above
*/
//void XmlRpcValueDbRecordSet::Serialize(QVariant *pValue,bool pSkip){
//	Serialize((DbRecordSet*)pValue,pSkip);
//}




/*!
	Converts xml to DbRecordSet
	Note: first TAG in content must be <value>
	\param nStartOffset start offset of TAG content in buffer from 0
	\param nLength of content between PARAM tags!
	\param pValue parsed xml value is stored in \a pValue
	\param pStatus if content can not parsed or type mismatch
	\param pBoolSkipColumnDefinition - - if 1 then expects that incoming RecordSet doesnt have column defintion, 
	but pValue must be then defined to match incoming data, if not ASSERT or error in pStatus will be returned
	Warning: if false then pValue recordset will be destroyed and completly rebuilded
*/

void XmlRpcValueDbRecordSet::DeSerialize(int &nStartOffset, int nLength, DbRecordSet* pValue,Status& pStatus,bool pBoolSkipColumnDefinition){


//	int RowStart,RowLength;

	//for nested recordsets
	XmlRpcValueDbRecordSet xmlDbRecordSet;  
	xmlDbRecordSet.SetBuffer(m_byteXMLMsg); //set buffer

	//declare local vars:
	int nDataStart,nDataType,i,j,nEndColumnSetup,nEndRecordSet, nVaueStartPos;
	QString strName, strAlias, strTable, strTAG, strValue;
	QByteArray byteValue;
	DbRecordSet recValue;
	QVariant varTemp;
	Status err;
	QDate valueDate;
	QTime valueTime;
	QDateTime valueDateTime;



	//------------------------------------------------------
	//test if NULL:
	//------------------------------------------------------

	// find STRUCT tag:
	nDataStart=nStartOffset;nEndColumnSetup=nLength;
	XmlUtil::GetTagContent(m_byteXMLMsg,STRUCT_TAG,STRUCT_ETAG,nDataStart,nEndColumnSetup);
	if(nDataStart<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

	strValue=m_byteXMLMsg->mid(nDataStart,nEndColumnSetup);
	if(strValue==NULL_VALUE)
	{
			nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nDataStart)+VALUE_ETAG_LEN;	
			//nStartOffset+=nLength;
			pValue->destroy();
			pStatus.setError(0);
			return;
	}



	nEndRecordSet=nStartOffset+nLength;


	//------------------------------------------------------
	//column types:
	//------------------------------------------------------

	if (!pBoolSkipColumnDefinition)
	{

		//destroy any data inside:
		pValue->destroy();

		//find </member> of first member as outer limit, check for consistency
		nEndColumnSetup=m_byteXMLMsg->indexOf(MEMBER_ETAG,nStartOffset);
			if(nEndColumnSetup<0||nEndColumnSetup>(nStartOffset+nLength)){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

		//find <data> from start to 2nd member

		while(1)
		{	
			nDataStart=m_byteXMLMsg->indexOf(DATA_TAG,nStartOffset);
			if( nDataStart<0||nDataStart>nEndColumnSetup) 
				break;
			else
				nStartOffset=nDataStart+DATA_TAG_LEN;

			xmlValInteger.DeSerialize(nStartOffset,nLength,&varTemp,pStatus);if(!pStatus.IsOK())return;
			xmlValString.DeSerialize(nStartOffset,nLength,&strName,err);if(!pStatus.IsOK())return;
			//xmlValString.DeSerialize(nStartOffset,nLength,&strAlias,err);if(!pStatus.IsOK())return;
			//xmlValString.DeSerialize(nStartOffset,nLength,&strTable,err);if(!pStatus.IsOK())return;

			pValue->addColumn(varTemp.toInt(),strName);//,strAlias,strTable);
		}

		//up offest to point to data section:
		nStartOffset=nEndColumnSetup;

	}



	//------------------------------------------------------------------------------------------------------------
	//data: IMPORTANT: nStartOffset is crucial parameter, that is used to navigate trough XML message!!!
	//------------------------------------------------------------------------------------------------------------

	

	//find FIRST </data> of array (data holder) in data section:	
	nDataStart=m_byteXMLMsg->indexOf(DATA_TAG,nStartOffset);
	if( nDataStart<0||nDataStart>nEndRecordSet) 
		{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;} //error no data tag at all!
	else
		nStartOffset=nDataStart+DATA_TAG_LEN; //precisly align offset to char after <data>

	//find </member> of second member as outer limit, check for consistency
	nEndColumnSetup=m_byteXMLMsg->indexOf(MEMBER_ETAG,nStartOffset);
		if(nEndColumnSetup<0||nEndColumnSetup>nEndRecordSet){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}


	i=-1;j=-1; //init counters

	//process record set
	while(1)
	{	
		//find start of ROW:
		nDataStart=m_byteXMLMsg->indexOf(DATA_TAG,nStartOffset);
		if( nDataStart<0||nDataStart>nEndColumnSetup) 
			break;
		else
			nStartOffset=nDataStart+DATA_TAG_LEN; //ok we have found ROW:


		//add new row to record set (must be defined)
		pValue->addRow();
		i++;	//position 'i' counter to current row
		j=-1;	//reset column counter


		//process each column inside row inside recordset:
		while(1)
		{
		
			j++; //position 'j' counter to current column

			//find value tag, if err exit!
			nVaueStartPos=nStartOffset;
			strTAG=XmlUtil::GetNextTag(m_byteXMLMsg,nStartOffset,nLength);
			if(strTAG==DATA_ETAG)
				break; //new row found <array><data>---row 1 data---</data></array><array><data>---row 2 data---</data></array>
			else if (strTAG!=VALUE_TAG||strTAG=="")
				{pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

			//find tag after value, if string then it will found </value>
			//find tag after value, if NULL then skip to next
			strTAG=XmlUtil::GetNextTag(m_byteXMLMsg,nStartOffset,nLength);
			if(strTAG==NULL_VALUE)strTAG=XmlUtil::GetNextTag(m_byteXMLMsg,nStartOffset,nLength);
			if(strTAG==VALUE_ETAG)strTAG=STRING_TAG;

			//restore position to value:
			nStartOffset=nVaueStartPos;


			if(strTAG==BOOLEAN_TAG)
			{
				//nStartOffset is updated to char after </value>
				xmlValBool.DeSerialize(nStartOffset,nLength,&varTemp,pStatus);if(!pStatus.IsOK())return;
				pValue->setData(i,j,varTemp);
			}
			else if(strTAG==DOUBLE_TAG)
			{   //nStartOffset is updated to char after </value>
				xmlValDouble.DeSerialize(nStartOffset,nLength,&varTemp,pStatus);if(!pStatus.IsOK())return;
				pValue->setData(i,j,varTemp);
			}
			else if(strTAG==I4_TAG)
			{   //nStartOffset is updated to char after </value>
				xmlValInteger.DeSerialize(nStartOffset,nLength,&varTemp,pStatus);if(!pStatus.IsOK())return;
				pValue->setData(i,j,varTemp);
			}
			else if(strTAG==STRING_TAG)
			{	//nStartOffset is updated to char after </value>
				xmlValString.DeSerialize(nStartOffset,nLength,&strValue,pStatus);if(!pStatus.IsOK())return;
				pValue->setData(i,j,strValue);
			}
			else if(strTAG==DATETIME_TAG)
			{	
				//determine by column type: Time, Date, DateTime:
				nDataType=pValue->getColumnType(j);		//,nDataType,strName,strAlias,strTable);
				switch(nDataType)
				{
				case QVariant::Date:
					xmlValDateTime.DeSerialize(nStartOffset,nLength,&valueDate,pStatus);if(!pStatus.IsOK())return;
					pValue->setData(i,j,valueDate);
					break;
				case QVariant::Time:
					xmlValDateTime.DeSerialize(nStartOffset,nLength,&valueTime,pStatus);if(!pStatus.IsOK())return;
					pValue->setData(i,j,valueTime);
					break;
				default:
					xmlValDateTime.DeSerialize(nStartOffset,nLength,&valueDateTime,pStatus);if(!pStatus.IsOK())return;
					pValue->setData(i,j,valueDateTime);
				}
			}
			else if(strTAG==BASE64_TAG)
			{	//nStartOffset is updated to char after </value>
				xmlValBinary.DeSerialize(nStartOffset,nLength,&byteValue,pStatus);if(!pStatus.IsOK())return;
				pValue->setData(i,j,byteValue);
			}
			else if(strTAG==STRUCT_TAG)
			{	//nested recordset found...proceed...
				Q_ASSERT(!pBoolSkipColumnDefinition); //can not skip definition if nested recordsets in use!!!!
				//nStartOffset is updated to char after </value>			
				xmlDbRecordSet.DeSerialize(nStartOffset,nLength,&recValue,pStatus,false);if(!pStatus.IsOK())return;
				pValue->setData(i,j,recValue);
				//this is round.round bug bugzi: update end of our recordset, last /member was from this one:
				nEndColumnSetup=m_byteXMLMsg->indexOf(MEMBER_ETAG,nStartOffset);
					if(nEndColumnSetup<0||nEndColumnSetup>nEndRecordSet){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;}

			}
			else
			{
				pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return;

			}
		
		//end of column
		}


	//end of row
	}


	//update start offset to one char after last TAG:
	//nStartOffset=m_byteXMLMsg->indexOf(STRUCT_ETAG,nStartOffset)+QString(STRUCT_ETAG).length();
	nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;
	pStatus.setError(0);


}






/*!
	Converts DbRecordSet into array of arrays (to xml)
	XML data is append to the buffer

	DBRecordSet:
	----------------------------
	<struct>
		<member>
			<name>columns</name>
				<array><data>x:array of column def</data></array>
		</member>
		<member>
			<name>rows</name>
			<value><i4>no_of_rows</i4></value>
		</member>
		<member>
			<name>data</name>
			<value>x:array of data</value>
		</member>
	</struct>

	----------------------------
	<x:array of column def> (two array's: column type + columns names):
		<array><data><value><i4>col_1_type</i4></value><value><i4>col_2_type</i4></value>.........<value><i4>col_N_type</i4></value></array></data>
		<array><data><value><string>col_1_name</string></value><value><string>col_1_name</string></value>.........<value><string>col_N_name</string></value></array></data>

	----------------------------
	<x:array of data> (serialized by columns for easier parsing):
		<array><data><col1row1_data><col1row2_data><col1row3_data>.....<col1rowN_data></array></data>
		<array><data><col2row1_data><col2row2_data><col2row3_data>.....<col2rowN_data></array></data>
		....
		<array><data><colMrow1_data><colMrow2_data><colMrow3_data>.....<col2rowM_data></array></data>

	----------------------------
	<col1row1_data>: is standard xml value tag: (e.g. for string: <value><string>string</string></value>

	NOTE: if DbRecordSet is empty (NULL): send as string: <value><struct><nil/></struct></value>

	\param pValue reference to data

*/
void XmlRpcValueDbRecordSet::SerializeFast(DbRecordSet *pValue)
{


	int i,j,nDataType;
	QString strName, strAlias, strTable;
	QVariant* data;
	XmlRpcValueDbRecordSet xmlDbRecordSet; //for nested recordsets
	xmlDbRecordSet.SetBuffer(m_byteXMLMsg); //set buffer


	//check if empty: then set to NULL string
	int nColumnCount=pValue->getColumnCount();
	if(nColumnCount==0)
	{	
		*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=STRUCT_TAG;
		*m_byteXMLMsg+=NULL_VALUE;
		*m_byteXMLMsg+=STRUCT_ETAG;*m_byteXMLMsg+=VALUE_ETAG; 
		return;
	}


	//start struct sequence:
	*m_byteXMLMsg+=VALUE_TAG;*m_byteXMLMsg+=STRUCT_TAG;


	//------------------------------------------------------
	//column types:
	//------------------------------------------------------
	*m_byteXMLMsg+=MEMBER_TAG;*m_byteXMLMsg+=NAME_TAG;
	*m_byteXMLMsg+="columns";*m_byteXMLMsg+=NAME_ETAG;
	*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;

	*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;
	for(i=0;i<nColumnCount;++i)
	{
		//get column data
		nDataType=pValue->getColumnType(i);
		QVariant tmp(nDataType);
		xmlValInteger.Serialize(&tmp);
	}
	*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG;
	
	*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;
	for(i=0;i<nColumnCount;++i)
	{
		//get column data
		strName=pValue->getColumnName(i);
		xmlValString.Serialize(&strName);
	}
	*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG;


	//end array for all column definitions:
	*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG;*m_byteXMLMsg+=MEMBER_ETAG;

	//------------------------------------------------------
	//no of rows
	//------------------------------------------------------

	int nRowCount=pValue->getRowCount();
	*m_byteXMLMsg+=MEMBER_TAG;*m_byteXMLMsg+=NAME_TAG;
	*m_byteXMLMsg+="rows";*m_byteXMLMsg+=NAME_ETAG;
	*m_byteXMLMsg+=VALUE_TAG;
	*m_byteXMLMsg+=I4_TAG+QVariant(nRowCount).toString()+I4_ETAG;
	*m_byteXMLMsg+=VALUE_ETAG;
	*m_byteXMLMsg+=MEMBER_ETAG;


	//------------------------------------------------------
	//data:
	//------------------------------------------------------

	//start member+array:

	//start member+array:
	*m_byteXMLMsg+=MEMBER_TAG;*m_byteXMLMsg+=NAME_TAG;
	*m_byteXMLMsg+="data";*m_byteXMLMsg+=NAME_ETAG;
	*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;

	
	int nColCount=pValue->getColumnCount();
	int nDbRecordSetType=DbRecordSet::GetVariantType();

	if (nRowCount>0)
	{
	

		for(i=0;i<nColCount;++i)
		{
			//start array for column
			*m_byteXMLMsg+=ARRAY_TAG;*m_byteXMLMsg+=DATA_TAG;

			int nColType=pValue->getColumnType(i);


			switch(nColType)
			{
				case QVariant::String:
					for(j=0;j<nRowCount;++j)
					{
						data=&(pValue->getDataRef(j,i));
						xmlValString.Serialize(data);
					}
					break;
				case QVariant::Int:
					for(j=0;j<nRowCount;++j)
					{
						data=&(pValue->getDataRef(j,i));
						xmlValInteger.Serialize(data);
					}
					break;
				case QVariant::Date:
					for(j=0;j<nRowCount;++j)
					{
						//qDebug()<<"ser:"<<&pValue->getColumnName(i);
						xmlValDateTime.Serialize(&pValue->getDataRef(j,i).toDate());
					}
					break;

				case QVariant::DateTime:
					for(j=0;j<nRowCount;++j)
					{
						xmlValDateTime.Serialize(&pValue->getDataRef(j,i).toDateTime());
					}
					break;
				case QVariant::Double:
					for(j=0;j<nRowCount;++j)
					{
						data=&(pValue->getDataRef(j,i));
						xmlValDouble.Serialize(data);
					}
					break;

				case QVariant::Bool:
					for(j=0;j<nRowCount;++j)
					{
						data=&(pValue->getDataRef(j,i));
						xmlValBool.Serialize(data);
					}
					break;


				case QVariant::ByteArray:
					for(j=0;j<nRowCount;++j)
					{
						data=&(pValue->getDataRef(j,i));
						xmlValBinary.Serialize(data);
					}
					break;

				case QVariant::Time:
					for(j=0;j<nRowCount;++j)
					{
						xmlValDateTime.Serialize(&pValue->getDataRef(j,i).toTime());
					}
					break;


				default:
					if (nColType==nDbRecordSetType || nColType>=256)
					{
						for(j=0;j<nRowCount;++j)
						{
							DbRecordSet childSet=(pValue->getDataRef(j,i)).value<DbRecordSet>();
							xmlDbRecordSet.SerializeFast(&childSet);
						}
					}
					else
						Q_ASSERT(0); //data type not supported!!

				}

			//end array for column
			*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG; 
		}


	}

	//end array of all columns:
	*m_byteXMLMsg+=DATA_ETAG;*m_byteXMLMsg+=ARRAY_ETAG; *m_byteXMLMsg+=MEMBER_ETAG;

	//end whole struct:
	*m_byteXMLMsg+=STRUCT_ETAG;*m_byteXMLMsg+=VALUE_ETAG;

}






/*!
Converts xml to DbRecordSet
Note: first TAG in content must be <value>

\param nStartOffset start offset of TAG content in buffer from 0
\param nLength of content between PARAM tags!
\param pValue parsed xml value is stored in \a pValue
\param pStatus if content can not parsed or type mismatch

*/
int XmlRpcValueDbRecordSet::DeSerializeFast(int &nStartOffset, int nLength, DbRecordSet* pValue,Status& pStatus,int nPreviousColumnDefinitonLength,DbRecordSet *pListDef)
{

	pStatus.setError(0);

	//QByteArray b=m_byteXMLMsg->mid(nStartOffset,nLength);
	//qDebug()<<b;

	//	int RowStart,RowLength;

	//for nested recordsets
	XmlRpcValueDbRecordSet xmlDbRecordSet;  
	xmlDbRecordSet.SetBuffer(m_byteXMLMsg); //set buffer

	int nColumnDefinitonLength=0;

	//declare local vars:
	int nDataStart,nEndColumnSetup,nEndRecordSet;
	QString strName;
	DbRecordSet recValue;
	QVariant varTemp;
	Status err;



	//------------------------------------------------------
	//test if NULL:
	//------------------------------------------------------

	// find STRUCT tag:
	nDataStart=nStartOffset;nEndColumnSetup=nLength;
	XmlUtil::GetTagContent(m_byteXMLMsg,STRUCT_TAG,STRUCT_ETAG,nDataStart,nEndColumnSetup);
	if(nDataStart<0){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return nColumnDefinitonLength;}

	
	if (nEndColumnSetup==6) //if null
	{
		QString strValue=m_byteXMLMsg->mid(nDataStart,nEndColumnSetup);
		if(strValue==NULL_VALUE)
		{
			nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nDataStart)+VALUE_ETAG_LEN;	
			pValue->destroy();
			return nColumnDefinitonLength;
		}
	}


	nEndRecordSet=nStartOffset+nLength;


	//------------------------------------------------------
	//column types:
	//------------------------------------------------------

	//destroy any data inside:
	pValue->destroy();
	int nColCount=0;
	QList<int> lstTypes;

	//find </member> of first member as outer limit, check for consistency
	nEndColumnSetup=m_byteXMLMsg->indexOf(MEMBER_ETAG,nStartOffset);
	if(nEndColumnSetup<0||nEndColumnSetup>(nStartOffset+nLength)){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return nColumnDefinitonLength;}
	nColumnDefinitonLength=nEndColumnSetup-nStartOffset;

	//copy list definitions from incoming if both cond are met: length & supplied
	if (pListDef && nColumnDefinitonLength==nPreviousColumnDefinitonLength)
	{
		pValue->copyDefinition(*pListDef);
		nColCount=pValue->getColumnCount();
	} 
	else
	{
	
		//find <data> from start to 2nd member
		nDataStart=m_byteXMLMsg->indexOf(DATA_TAG,nStartOffset);
		nStartOffset=nDataStart+DATA_TAG_LEN;
		nDataStart=m_byteXMLMsg->indexOf(DATA_TAG,nStartOffset);
		nStartOffset=nDataStart+DATA_TAG_LEN;
		

		
		while(1)
		{	
			if(m_byteXMLMsg->indexOf(DATA_ETAG,nStartOffset)==nStartOffset)
				break;
			xmlValInteger.DeSerialize(nStartOffset,nLength,&varTemp,pStatus);if(!pStatus.IsOK())return nColumnDefinitonLength;
			if (varTemp.toInt()>=256) //B.T. after 2008 DbRecordset is registered sometimes as 256 sometimes as 257 so any above 256is good!
				varTemp=DbRecordSet::GetVariantType();
			lstTypes<<varTemp.toInt();
		}
		
		nColCount=lstTypes.size();
		
		nDataStart=m_byteXMLMsg->indexOf(DATA_TAG,nStartOffset);
		nStartOffset=nDataStart+DATA_TAG_LEN;
		for (int i=0;i<nColCount;++i)
		{
			xmlValString.DeSerialize(nStartOffset,nLength,&strName,err);if(!pStatus.IsOK())return nColumnDefinitonLength;
			pValue->addColumn(lstTypes.at(i),strName);//,strAlias,strTable);
		}
	}



	//up offest to point to data section:
	nStartOffset=nEndColumnSetup+MEMBER_ETAG_LEN;


	//------------------------------------------------------
	//rows:
	//------------------------------------------------------

	//find </member> of rows:
	int nEndRowSetup=m_byteXMLMsg->indexOf(MEMBER_ETAG,nStartOffset);
	if(nEndRowSetup<0||nEndRowSetup>(nStartOffset+nLength)){pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));return nColumnDefinitonLength;}

	//find <data> from start to 2nd member
	xmlValInteger.DeSerialize(nStartOffset,nLength,&varTemp,pStatus);if(!pStatus.IsOK())return nColumnDefinitonLength;
	int nRowCount=varTemp.toInt();

	//up offset to point to data section:
	nStartOffset=nEndRowSetup+MEMBER_ETAG_LEN;

	//------------------------------------------------------------------------------------------------------------
	//data: IMPORTANT: nStartOffset is crucial parameter, that is used to navigate trough XML message!!!
	//------------------------------------------------------------------------------------------------------------

	//if zero exit right away:
	if (nRowCount==0)
	{
		nStartOffset=m_byteXMLMsg->indexOf(STRUCT_ETAG,nStartOffset)+STRUCT_ETAG_LEN;
		nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;
		return nColumnDefinitonLength;
	}

	//create all rows:
	pValue->addRow(nRowCount);

	//find FIRST <data> of array (data holder) in data section:	
	nDataStart=m_byteXMLMsg->indexOf(DATA_TAG,nStartOffset);
	if( nDataStart<0||nDataStart>nEndRecordSet) 
	{	//error no data tag at all!
		pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
		return nColumnDefinitonLength;
	} 
	else
		nStartOffset=nDataStart+DATA_TAG_LEN; //precisely align offset to char after <data>



	int nDbRecordSetType=DbRecordSet::GetVariantType();


	for (int i=0;i<nColCount;++i)
	{

		//find FIRST <data> of array (data holder) in data section:	
		nDataStart=m_byteXMLMsg->indexOf(DATA_TAG,nStartOffset);
		if( nDataStart<0||nDataStart>nEndRecordSet) 
		{	//error no data tag at all!
			pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
			return nColumnDefinitonLength;
		} 
		else
			nStartOffset=nDataStart+DATA_TAG_LEN; //precisely align offset to char after <data>


		int nColType=pValue->getColumnType(i);


		switch(nColType)
		{
			
		case QVariant::String:
			{
				//Warning: when sending string there is no <string> tag just <value> as XMLRPC specifies:

				for(int j=0;j<nRowCount;++j)
				{
					int nValueStart=m_byteXMLMsg->indexOf(">",nStartOffset)+1;
					int nValueLength=m_byteXMLMsg->indexOf("</",nStartOffset)-nValueStart;
					if (nValueLength<0)
					{	//error: missing tags
						pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
						return nColumnDefinitonLength;
					} 
					QByteArray byteValueContent =m_byteXMLMsg->mid(nValueStart,nValueLength);
					if (byteValueContent.indexOf(NULL_VALUE)==-1) //test if NULL
						pValue->setData(j,i,QString::fromUtf8(XmlUtil::xmlDecode(byteValueContent)));
					nStartOffset=nValueStart+nValueLength+VALUE_ETAG_LEN;
				}
			}
			break;
		case QVariant::Int:
			{
				for(int j=0;j<nRowCount;++j)
				{
					int nValueStart=m_byteXMLMsg->indexOf(I4_TAG,nStartOffset)+I4_TAG_LEN;
					int nLength=m_byteXMLMsg->indexOf("</",nStartOffset)-nValueStart;
					if (nLength<0 || nValueStart>nEndRecordSet)
					{	//error: missing tags
						pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
						return nColumnDefinitonLength;
					} 
					QByteArray byteValueContent =m_byteXMLMsg->mid(nValueStart,nLength);
					if (byteValueContent.indexOf(NULL_VALUE)==-1) //test if NULL
						pValue->setData(j,i,byteValueContent.toInt());
					nStartOffset=nValueStart+nLength+VALUE_ETAG_LEN+I4_ETAG_LEN;
				}
			}
			break;
		case QVariant::Date:
			{

				for(int j=0;j<nRowCount;++j)
				{
					int nValueStart=m_byteXMLMsg->indexOf(DATETIME_TAG,nStartOffset)+DATETIME_TAG_LEN;
					int nLength=m_byteXMLMsg->indexOf("</",nStartOffset)-nValueStart;
					if (nLength<0 || nValueStart>nEndRecordSet)
					{	//error: missing tags
						pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
						return nColumnDefinitonLength;
					} 
					QByteArray byteValueContent =m_byteXMLMsg->mid(nValueStart,nLength);
					//qDebug()<<pValue->getColumnName()<<" "<<byteValueContent;
					if (byteValueContent.indexOf(NULL_VALUE)==-1) //test if NULL
					{
						pValue->setData(j,i,(QDateTime::fromString(byteValueContent,Qt::ISODate)).date());
					}
					nStartOffset=nValueStart+nLength+VALUE_ETAG_LEN+DATETIME_ETAG_LEN;
				}

			}
			break;

		case QVariant::DateTime:
			{

				for(int j=0;j<nRowCount;++j)
				{
					int nValueStart=m_byteXMLMsg->indexOf(DATETIME_TAG,nStartOffset)+DATETIME_TAG_LEN;
					int nLength=m_byteXMLMsg->indexOf("</",nStartOffset)-nValueStart;
					if (nLength<0 || nValueStart>nEndRecordSet)
					{	//error: missing tags
						pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
						return nColumnDefinitonLength;
					} 
					QByteArray byteValueContent =m_byteXMLMsg->mid(nValueStart,nLength);
					if (byteValueContent.indexOf(NULL_VALUE)==-1) //test if NULL
					{
						if (m_nTimeZoneOffsetSeconds)
							pValue->setData(j,i,(QDateTime::fromString(byteValueContent,Qt::ISODate).addSecs(m_nTimeZoneOffsetSeconds)));
						else
							pValue->setData(j,i,(QDateTime::fromString(byteValueContent,Qt::ISODate)));
					}
					nStartOffset=nValueStart+nLength+VALUE_ETAG_LEN+DATETIME_ETAG_LEN;
				}

			}
			break;
		case QVariant::Double:
			{
				for(int j=0;j<nRowCount;++j)
				{
					int nValueStart=m_byteXMLMsg->indexOf(DOUBLE_TAG,nStartOffset)+DOUBLE_TAG_LEN;
					int nLength=m_byteXMLMsg->indexOf("</",nStartOffset)-nValueStart;
					if (nLength<0 || nValueStart>nEndRecordSet)
					{	//error: missing tags
						pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
						return nColumnDefinitonLength;
					} 
					QByteArray byteValueContent =m_byteXMLMsg->mid(nValueStart,nLength);
					if (byteValueContent.indexOf(NULL_VALUE)==-1) //test if NULL
						pValue->setData(j,i,byteValueContent.toDouble());
					nStartOffset=nValueStart+nLength+VALUE_ETAG_LEN+DOUBLE_ETAG_LEN;
				}
			}
			break;

		case QVariant::Bool:
			{

				for(int j=0;j<nRowCount;++j)
				{
					int nValueStart=m_byteXMLMsg->indexOf(BOOLEAN_TAG,nStartOffset)+BOOLEAN_TAG_LEN;
					int nLength=m_byteXMLMsg->indexOf("</",nStartOffset)-nValueStart;
					if (nLength<0 || nValueStart>nEndRecordSet)
					{	//error: missing tags
						pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
						return nColumnDefinitonLength;
					} 
					QByteArray byteValueContent =m_byteXMLMsg->mid(nValueStart,nLength);
					if (byteValueContent.indexOf(NULL_VALUE)==-1) //test if NULL
						pValue->setData(j,i,(byteValueContent=="1" ? true: false));
					nStartOffset=nValueStart+nLength+VALUE_ETAG_LEN+BOOLEAN_ETAG_LEN;
				}
			}
			break;

		case QVariant::ByteArray:
			{

				for(int j=0;j<nRowCount;++j)
				{
					int nValueStart=m_byteXMLMsg->indexOf(BASE64_TAG,nStartOffset)+BASE64_TAG_LEN;
					int nLength=m_byteXMLMsg->indexOf("</",nStartOffset)-nValueStart;
					if (nLength<0 || nValueStart>nEndRecordSet)
					{	//error: missing tags
						pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
						return nColumnDefinitonLength;
					} 
					QByteArray byteValueContent =m_byteXMLMsg->mid(nValueStart,nLength);
					if (byteValueContent.indexOf(NULL_VALUE)==-1) //test if NULL
					{
						byteValueContent=QByteArray::fromBase64(byteValueContent);
						pValue->setData(j,i,byteValueContent);
					}
					nStartOffset=nValueStart+nLength+VALUE_ETAG_LEN+BASE64_ETAG_LEN;
				}

			}
			break;

		case QVariant::Time:
			{
				for(int j=0;j<nRowCount;++j)
				{
					int nValueStart=m_byteXMLMsg->indexOf(DATETIME_TAG,nStartOffset)+DATETIME_TAG_LEN;
					int nLength=m_byteXMLMsg->indexOf("</",nStartOffset)-nValueStart;
					if (nLength<0 || nValueStart>nEndRecordSet)
					{	//error: missing tags
						pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
						return nColumnDefinitonLength;
					} 
					QByteArray byteValueContent =m_byteXMLMsg->mid(nValueStart,nLength);
					if (byteValueContent.indexOf(NULL_VALUE)==-1) //test if NULL
						pValue->setData(j,i,(QDateTime::fromString(byteValueContent,Qt::ISODate)).time());
					nStartOffset=nValueStart+nLength+VALUE_ETAG_LEN+DATETIME_ETAG_LEN;
				}
			}
			break;
	
		default:
			if (nColType==nDbRecordSetType || nColType>=256) //B.T. after 2008 DbRecordset is registered sometimes as 256 sometimes as 257 so any above 256is good!
			{
				//optimized for column that have all same recordset, can get 2x speed
				DbRecordSet tmpDef;
				int nColumnDefinitonLengthChild;
				for(int j=0;j<nRowCount;++j)
				{
					if (j!=0)
					{
						xmlDbRecordSet.DeSerializeFast(nStartOffset,nLength,&recValue,pStatus,nColumnDefinitonLengthChild,&tmpDef);
					}
					else
					{
						nColumnDefinitonLengthChild=xmlDbRecordSet.DeSerializeFast(nStartOffset,nLength,&recValue,pStatus);
						tmpDef.copyDefinition(recValue);
					}
					if(!pStatus.IsOK())return nColumnDefinitonLength;
					//qDebug()<<pValue->getDataRef(j,i).userType();
					pValue->setData(j,i,recValue);
				}
			}
			else
				Q_ASSERT(0); //data type not supported!!

		}

		//find FIRST </array> 
		nDataStart=m_byteXMLMsg->indexOf(ARRAY_ETAG,nStartOffset);
		if( nDataStart<0||nDataStart>nEndRecordSet) 
		{	//error no data tag at all!
			pStatus.setError(StatusCodeSet::ERR_XMLRPC_PARSE_FAIL,"XML Failed at:"+QVariant(__LINE__).toString()+"in file:"+QString(__FILE__));
			return nColumnDefinitonLength;
		} 
		else
			nStartOffset=nDataStart+ARRAY_ETAG_LEN; //precisely align offset to char after <data>



	}


	//update offset to last char after:
	nStartOffset=m_byteXMLMsg->indexOf(VALUE_ETAG,nStartOffset)+VALUE_ETAG_LEN;

	return nColumnDefinitonLength;
}
