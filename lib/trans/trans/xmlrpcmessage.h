#ifndef XMLRPCMESSAGE_H
#define XMLRPCMESSAGE_H

#include "common/common/status.h"
#include "rpcmessage.h"
#include "xmlrpcvalue.h"



/*!
    \class XmlRpcMessage
    \brief class for compiling & parsing XML RPC call
    \ingroup XMLRPCModule

	Main class for generating XMLRPC message and parsing response. For every data type that must send through 
	XMLRPCMessage separate XMLRCPValue object must be created with Serialize/Deserialize functions.

	No data is stored inside object except string buffer containing message. 
	Supports NULL values: <nil/>
	Support basic QT variables

	Use:
	- SetBuffer() - sets pointer to external buffer in which XMLRPC stream will be written to or parsed from (to save one copy)

	sending XmlRPC:
	- AddParameter() - adds parameter in sequential order. First call sets first parameter, etc..
	- GenerateRPC() - generates RPC method based on previously setted parameters
	- GenerateFault() - generates XMLRPC fault message

	receiving XmlRPC:
	- GetParameterCount() - gets count of parameters in XmlRpc stream
	- CheckForFault() - checks if message contains fault. Call this prior to any parsing!
	- GetMethodName() - gets method name from xmlrpc stream
	- GetParameter() - gets parameter value from xmlrpc stream

*/
class XmlRpcMessage : public RpcMessage
{ 
public:

	XmlRpcMessage():m_byteXMLMsg(NULL){};
	virtual ~XmlRpcMessage(){};


	/* For generating RPC request */
	/*-----------------------------*/

	void GenerateRPC (QString strMethodName,Status& pStatus, QString strXmlBodyNamespaces="xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
	void GenerateFault (int nFaultCode, QString strFaultText);
	void GenerateFault (int nFaultCode);
	void SetTimeZoneOffset(int nTimeZoneOffsetMinutes){xmlValDateTime.SetTimeZoneOffset(nTimeZoneOffsetMinutes*60); xmlDbRecordSet.SetTimeZoneOffset(nTimeZoneOffsetMinutes*60);}

	bool AddParameter(QString*,QString strParamName="Param");	
	bool AddParameter(QVariant*,QString strParamName="Param");	
	bool AddParameter(int*,QString strParamName="Param");	
	bool AddParameter(bool*,QString strParamName="Param");	
	bool AddParameter(double*,QString strParamName="Param");	
	bool AddParameter(QDate*,QString strParamName="Param");		
	bool AddParameter(QDateTime*,QString strParamName="Param");	
	bool AddParameter(QTime*,QString strParamName="Param");		
	bool AddParameter(QByteArray*,QString strParamName="Param"); 
	bool AddParameter(DbRecordSet*,QString strParamName="Param", const QString strRowName="", const QString strRowURLAttr="",QString strRowURLAttr_IDField=""); 



	/* For parsing RPC response */
	/*---------------------------*/

	void GenerateResponse(Status& pStatus,  QString strXmlBodyNamespaces="xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", QString strAddOnXmlData="");
	int GetParameterCount();
	void CheckForFault (Status&);
	QString GetMethodName(Status&);
	void StartResponseParsing(Status&){};

	void GetParameter(quint16 nPosition, QString*, Status& );
	void GetParameter(quint16 nPosition, QVariant*, Status& );
	void GetParameter(quint16 nPosition, int*, Status& );
	void GetParameter(quint16 nPosition, bool*, Status& );
	void GetParameter(quint16 nPosition, double*, Status& );
	void GetParameter(quint16 nPosition, QDate*, Status& );
	void GetParameter(quint16 nPosition, QDateTime*, Status& );
	void GetParameter(quint16 nPosition, QTime*, Status& );
	void GetParameter(quint16 nPosition, QByteArray*, Status& );
	void GetParameter(quint16 nPosition, DbRecordSet*, Status&);
	
	/* buffer */
	void SetBuffer(QByteArray *Buffer);
	QByteArray* GetBuffer(){return m_byteXMLMsg;};
	void ClearData(){m_byteXMLMsg->clear();}


private:
	QByteArray *m_byteXMLMsg;	/*!< for storing xml encoded parameters */

	XmlRpcValueString xmlValString;
	XmlRpcValueBool xmlValBool;
	XmlRpcValueInteger xmlValInteger;
	XmlRpcValueDouble xmlValDouble;
	XmlRpcValueDateTime xmlValDateTime;
	XmlRpcValueBinary xmlValBinary;
	XmlRpcValueFault xmlValFault;
	XmlRpcValueDbRecordSet xmlDbRecordSet;


	//void ParseTagContent(int& nStartOffset,int& nLength,void *pValue);

};



#endif // XMLRPCMESSAGE_H
