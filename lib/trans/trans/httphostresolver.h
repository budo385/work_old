#ifndef HTTPRESOLVER_H
#define HTTPRESOLVER_H	

#include <QObject>
#include <QString>
#include <QHostInfo>

/*!
    \class HTTPHostResolver
    \brief asynchronous host resolving functionality (from IP->URL)
    \ingroup HTTPModule
*/
class HTTPHostResolver : private QObject
{ 
	Q_OBJECT

public:
	void HostLookUp(QString IpAddress);
	QString GetHostName(){return m_strHost;};

private slots:
	void HostNameFetched(QHostInfo);

private:
	QString m_strHost;
};



#endif //HTTPRESOLVER_H
