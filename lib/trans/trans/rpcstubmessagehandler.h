#ifndef STUBRPCMSGHANDLER_H
#define STUBRPCMSGHANDLER_H


#include "rpcprotocol.h"
#include "rpcmessage.h"
#include "httprequest.h"
#include "httpresponse.h"


/*!
    \class RpcStubMessageHandler
    \brief Used as local helper object to make RPC message handling easier
    \ingroup RPCModule

	Makes instances of rpcrequest & rpcresponse message handlers
	fill them with buffers from HTTP messages. When this object is destroyed msg 
	instances are deleted automatically
	Use msg handlers as public: StubRpcMessageHandler.msg_in or StubRpcMessageHandler.msg_out
*/

class RpcStubMessageHandler
{

public:
	RpcStubMessageHandler(RpcProtocol *stub);
	~RpcStubMessageHandler();

	RpcMessage *msg_in;			//<pointer to RPC message handler of client request
	RpcMessage *msg_out;		//<pointer to RPC message handler of response
	HTTPRequest HttpRequest;	//<HTTP request message 
	HTTPResponse HttpResponse;	//<HTTP response message 

};


#endif //STUBRPCMSGHANDLER_H
