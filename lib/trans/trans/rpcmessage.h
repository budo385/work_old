#ifndef RPCMESSAGE_H
#define RPCMESSAGE_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"


/*!
    \class RpcMessage
    \brief Abstract class for compiling & parsing any RPC call
    \ingroup RPCModule

	RpcMessage is abstract class which defines interface for converting any RPC call to predfined RPC stream.

	Use:

	sending XmlRPC:
	- AddParameter() - adds parameter in sequential order. First call sets first parameter, etc..
	- GenerateRPC() - generates RPC method based on previously setted parameters
	- GenerateFault() - generates XMLRPC fault message

	receiving XmlRPC:
	- GetParameterCount() - gets count of parameters in XmlRpc stream
	- CheckForFault() - checks if message contains fault. Call this prior to any parsing!
	- GetMethodName() - gets method name from xmlrpc stream
	- GetParameter() - gets parameter value from xmlrpc stream

*/
class RpcMessage
{ 
public:

	virtual ~RpcMessage(){};

	/* For generating RPC request */
	/*-----------------------------*/

	void virtual GenerateRPC (QString strMethodName,Status& pStatus, QString strXmlBodyNamespaces="xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"")=0;
	void virtual GenerateFault (int nFaultCode, QString strFaultText)=0;
	void virtual GenerateFault (int nFaultCode)=0;
	void virtual SetTimeZoneOffset(int nTimeZoneOffset)=0;

	bool virtual AddParameter(QString*,QString strParamName="Param")=0;	
	bool virtual AddParameter(QVariant*,QString strParamName="Param")=0;	
	bool virtual AddParameter(int*,QString strParamName="Param")=0;	
	bool virtual AddParameter(bool*,QString strParamName="Param")=0;	
	bool virtual AddParameter(double*,QString strParamName="Param")=0;	
	bool virtual AddParameter(QDate*,QString strParamName="Param")=0;		
	bool virtual AddParameter(QDateTime*,QString strParamName="Param")=0;	
	bool virtual AddParameter(QTime*,QString strParamName="Param")=0;		
	bool virtual AddParameter(QByteArray*,QString strParamName="Param")=0; 
	bool virtual AddParameter(DbRecordSet*,QString strParamName="Param", const QString strRowName="", const QString strRowURLAttr="",QString strRowURLAttr_IDField="")=0; 



	/* For parsing RPC response */
	/*---------------------------*/

	void	virtual GenerateResponse(Status& pStatus,  QString strXmlBodyNamespaces="xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", QString strAddOnXmlData="")=0;
	int		virtual GetParameterCount()=0;
	void	virtual CheckForFault (Status&)=0;
	QString virtual GetMethodName(Status&){return m_strMehodName;};
	void	virtual SetMethodName(QString strMethodName){m_strMehodName=strMethodName;};
	void	virtual StartResponseParsing(Status&)=0; //before any response parse call, call this function: to enable DOM XML parsing methods

	void virtual GetParameter(quint16 nPosition, QString*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, QVariant*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, int*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, bool*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, double*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, QDate*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, QDateTime*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, QTime*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, QByteArray*, Status& )=0;
	void virtual GetParameter(quint16 nPosition, DbRecordSet*, Status&)=0;

	/* buffer */
	void virtual		SetBuffer(QByteArray *Buffer)=0;
	virtual QByteArray* GetBuffer()=0;
	void virtual		ClearData()=0;

protected:
	QString m_strMehodName; //in format NameSpace.Method where NameSpace can consist of multiple parts separated with "."
};



#endif // RPCMESSAGE_H
