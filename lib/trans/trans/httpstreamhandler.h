#ifndef HTTPSTREAMHANDLER_H
#define HTTPSTREAMHANDLER_H

#include "common/common/status.h"
#include "qhttp.h"


class HTTPStreamHandler  
{
public:
	virtual ~HTTPStreamHandler(){};	//FIX: class with virtual functions, must have virt. destructor

	virtual void	SetRequestHeader(const QHttpRequestHeader &header){m_reguest_header=header;}
	virtual void	SetResponseHeader(const QHttpResponseHeader &header){m_response_header=header;}
	virtual void	GetChunk(Status &pStatus, QByteArray &data,bool bInsertResponseHeaderInFirstChunk=false,bool bInsertRequestHeaderInFirstChunk=false)=0;
	virtual void	SetChunk(Status &pStatus, const QByteArray &data,bool bEof=false)=0;
	Status&			GetLastError(){return m_LastError;};
	qint64			GetTotalStreamSize(){return m_nBytesTotal;};
	qint64			GetBytesProcessed(){return m_nBytesProcessed;};
	qint64			GetDefaultChunkSize(){return 0;};

protected:
	QHttpResponseHeader m_response_header;
	QHttpRequestHeader	m_reguest_header;
	Status				m_LastError;
	qint64				m_nBytesTotal;
	qint64				m_nBytesProcessed;
};

#endif // HTTPSTREAMHANDLER_H
