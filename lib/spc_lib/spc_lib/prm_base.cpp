#include "prm_base.h"

PRM_Base::PRM_Base(QWidget *parent)
	: QWidget(parent),
	m_pSelectionController(NULL)
{
}

PRM_Base::~PRM_Base()
{
	m_pSelectionController->unregisterObserver(this);
}


void PRM_Base::SetProjectTree(Selection_TreeBased *pTree)
{
	m_pSelectionController=pTree;
	m_pSelectionController->registerObserver(this);
}


void PRM_Base::updateObserver(ObsrPtrn_Subject* pSignalSource, int nMsgCode,int nMsgDetail,const QVariant val)
{
	//determine if our selection controller & right message:
	if(m_pSelectionController==pSignalSource)
		on_selectionChange(nMsgDetail);
}