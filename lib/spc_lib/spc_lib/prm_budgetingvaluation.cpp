#include "prm_budgetingvaluation.h"
//#define NOMINMAX
#include <qt_windows.h>
#include "common/common/csvimportfile.h"
#include <QProgressDialog>
#include <QInputDialog>

bool CsvProgress(unsigned long nUserData);

PRM_BudgetingValuation::PRM_BudgetingValuation(QWidget *parent)
	: PRM_Base(parent),
	m_bFakeFirstClickToPerDate(true),
	m_bFakeFirstClickToPerPeriod(true)
{
	ui.setupUi(this);

	//hide some stuff for later date
	ui.labelManagerInfo->hide();
	ui.btnRevenuesCollapse->hide();
	ui.btnCostsCollapse->hide();
	ui.frameProgress->hide();

	//initialization
	ui.dateFrom->hide();
	ui.dateTo->hide();
	ui.labelDateTo->hide();
	ui.radTotal->setChecked(true);
	ui.labelProjectTitle->setText("");
	ui.labelManagerInfo->setText("");

	//needed for presentation
	ui.dateFrom->setDate(QDate(2013,1,1));
	ui.dateTo->setDate(QDate(2013,12,31));
	connect(ui.dateFrom,SIGNAL(dateChanged(const QDate&)),this,SLOT(on_dateFrom_changed_cbk(const QDate&)));
	connect(ui.dateTo,SIGNAL(dateChanged(const QDate&)),this,SLOT(on_dateTo_changed_cbk(const QDate&)));

	//set button styling (colors)
	QString strStyleBudgetButton		= QString("QPushButton { background-color : rgb(%1, %2, %3); }").arg(0).arg(153).arg(204);
	QString strStyleAccruedButton		= QString("QLabel { background-color : rgb(%1, %2, %3); }").arg(255).arg(153).arg(153);
	QString strStyleSupplyButton		= QString("QLabel { background-color : rgb(%1, %2, %3); }").arg(255).arg(204).arg(153);
	QString strStyleRemainingButton		= QString("QPushButton { background-color : rgb(%1, %2, %3); }").arg(255).arg(255).arg(204);
	QString strStyleForecastButton		= QString("QLabel { background-color : rgb(%1, %2, %3); }").arg(102).arg(255).arg(204);
	QString strStyleDifferenceButton	= QString("QLabel { background-color : rgb(%1, %2, %3); }").arg(153).arg(255).arg(255);
	QString strStyleWorkFunctionsBtn	= QString("QPushButton { background-color : rgb(%1, %2, %3); }").arg(102).arg(204).arg(204);
	QString strStyleExpensesButton		= QString("QPushButton { background-color : rgb(%1, %2, %3); }").arg(102).arg(153).arg(153);

	ui.btnBudget->setStyleSheet(strStyleBudgetButton);
	ui.labelAccrued->setStyleSheet(strStyleAccruedButton);
	ui.labelSupply->setStyleSheet(strStyleSupplyButton);
	ui.btnRemaining->setStyleSheet(strStyleRemainingButton);
	ui.labelForecast->setStyleSheet(strStyleForecastButton);
	ui.labelDifference->setStyleSheet(strStyleDifferenceButton);
	ui.btnWorkFunctionsAndTime->setStyleSheet(strStyleWorkFunctionsBtn);
	ui.btnExpenses->setStyleSheet(strStyleExpensesButton);

	//set entry styling (colors)
	QString strStyleBudgetColEntries		= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(0).arg(153).arg(204);
	QString strStyleAccruedColEntries		= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(255).arg(153).arg(153);
	QString strStyleSupplyColEntries		= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(255).arg(204).arg(153);
	QString strStyleRemainingColEntries		= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(255).arg(255).arg(204);
	QString strStyleForecastColEntries		= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(102).arg(255).arg(204);
	QString strStyleDifferenceColEntries	= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(153).arg(255).arg(255);
	QString strStyleTotalEntries			= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(192).arg(192).arg(192);

	//custom colored entries
	QString strStyleBudgetCostsExpenses		= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(102).arg(153).arg(153);
	QString strStyleBudgetWorkAndTime		= QString("QLineEdit { background-color : rgb(%1, %2, %3); }").arg(102).arg(204).arg(204);

	//budget column colors
	ui.txtBudgetRevTotal->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetRevExpenses->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetRevExtExpenses->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetRevWork->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetCostsIntTotal->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetCostsExpenses->setStyleSheet(strStyleBudgetCostsExpenses);
	ui.txtBudgetCostsExtExpenses->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetWork->setStyleSheet(strStyleBudgetWorkAndTime);
	ui.txtBudgetTime->setStyleSheet(strStyleBudgetWorkAndTime);
	ui.txtBudgetBenOverall->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetBenWork->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetBenCostCovTotal->setStyleSheet(strStyleBudgetColEntries);
	ui.txtBudgetBenCostCovWork->setStyleSheet(strStyleBudgetColEntries);
	
	//accrued column colors
	ui.txtAccruedRevTotal->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedRevExpenses->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedRevExtExpenses->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedRevWork->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedCostsIntTotal->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedCostsExpenses->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedCostsExtExpenses->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedCostsWork->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedCostsTime->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedBenOverall->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedBenWork->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedBenCostCovTotal->setStyleSheet(strStyleAccruedColEntries);
	ui.txtAccruedBenCostCovWork->setStyleSheet(strStyleAccruedColEntries);

	//supply column colors
	ui.txtSupplyRevTotal->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyRevExpenses->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyRevExtExpenses->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyRevWork->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyCostsIntTotal->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyCostsExpenses->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyCostsExtExpenses->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyCostsWork->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyCostsTime->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyBenOverall->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyBenWork->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyBenCostCovTotal->setStyleSheet(strStyleSupplyColEntries);
	ui.txtSupplyBenCostCovWork->setStyleSheet(strStyleSupplyColEntries);

	//remaining column colors
	ui.txtRemainingRevTotal->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingRevExpenses->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingRevExtExpenses->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingRevWork->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingCostsIntTotal->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingCostsExpenses->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingCostsExtExpenses->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingCostsWork->setStyleSheet(strStyleBudgetWorkAndTime);
	ui.txtRemainingCostsTime->setStyleSheet(strStyleBudgetWorkAndTime);
	ui.txtRemainingBenOverall->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingBenWork->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingBenCostCovTotal->setStyleSheet(strStyleRemainingColEntries);
	ui.txtRemainingBenCostCovWork->setStyleSheet(strStyleRemainingColEntries);

	//forecast column colors
	ui.txtForecastRevTotal->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastRevExpenses->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastRevExtExpenses->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastRevWork->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastCostsIntTotal->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastCostsExpenses->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastCostsExtExpenses->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastCostsWork->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastCostsTime->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastBenOverall->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastBenWork->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastBenCostCovTotal->setStyleSheet(strStyleForecastColEntries);
	ui.txtForecastBenCostCovWork->setStyleSheet(strStyleForecastColEntries);
	
	//difference column colors
	ui.txtDifferenceRevTotal->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceRevExpenses->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceRevExtExpenses->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceRevWork->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceCostsIntTotal->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceCostsExpenses->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceCostsExtExpenses->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceCostsWork->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceCostsTime->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceBenOverall->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceBenWork->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceBenCostCovTotal->setStyleSheet(strStyleDifferenceColEntries);
	ui.txtDifferenceBenCostCovWork->setStyleSheet(strStyleDifferenceColEntries);

	//total fields
	ui.txtTotalsFullfillmentTotal->setStyleSheet(strStyleTotalEntries);
	ui.txtTotalsFullfillmentWork->setStyleSheet(strStyleTotalEntries);
	ui.txtTotalsRemainingTotal->setStyleSheet(strStyleTotalEntries);
	ui.txtTotalsRemainingWork->setStyleSheet(strStyleTotalEntries);
	ui.txtTotalsDifferenceTotal->setStyleSheet(strStyleTotalEntries);
	ui.txtTotalsDifferenceWork->setStyleSheet(strStyleTotalEntries);
	ui.txtTotalsContractProportion->setStyleSheet(strStyleTotalEntries);
	ui.txtTotalsContractProportionChange->setStyleSheet(strStyleTotalEntries);

	//load data for faking
	QSize size(300,100);
	QProgressDialog progress(tr("Initializing..."),tr("Cancel"),0,0,this);
	progress.setWindowTitle(tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	QString strFile = QCoreApplication::applicationDirPath() + "/bv.txt";
	Status status;
	CsvImportFile import;
	QStringList lstFormatPlain;
	QStringList lstFormatUtf8;
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	import.Load(status, strFile, m_dataFake, lstFormatPlain, lstFormatUtf8, true, CsvProgress, (unsigned long)&progress);
	if(!status.IsOK()){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Error", status.getErrorText());
		return;
	}
	QApplication::restoreOverrideCursor();
	int nFieldsCnt = m_dataFake.getColumnCount();
	Q_ASSERT(nFieldsCnt == 99);
}

PRM_BudgetingValuation::~PRM_BudgetingValuation()
{
}

//when project changes:
void PRM_BudgetingValuation::on_selectionChange(int nEntityRecordID)
{
	if(!m_pSelectionController)
		return;

	DbRecordSet rowSelectedRecord;
	int nProjID;
	QString strCode,strName;
	m_pSelectionController->GetSelection(nProjID,strCode,strName,rowSelectedRecord);
	//rowSelectedRecord.Dump();
	if (rowSelectedRecord.getRowCount()>0)
	{
		QString strTitle = strCode + " " + strName;
		ui.labelProjectTitle->setText(strTitle);

		recalculateData(QDate(), false, false);
	}
}

void PRM_BudgetingValuation::recalculateData(const QDate &date, bool bUseDate, bool bFromDate)
{
	if(!m_pSelectionController)
		return;

	//get index fields for searching into the fake data
	DbRecordSet rowSelectedRecord;
	int nProjID;
	QString strCode,strName;
	m_pSelectionController->GetSelection(nProjID,strCode,strName,rowSelectedRecord);
	//rowSelectedRecord.Dump();
	if (rowSelectedRecord.getRowCount()>0)
	{
		//get other index fields
		int nOldDateType = ui.radTotal->isChecked() ? 0 : ui.radPerDate->isChecked() ? 1 : 2;	//this is as defined in specifications
		int nDateType = ui.radTotal->isChecked() ? 0 : 1;	//this is as used in MB's export	
		int nCumulated = ui.chkIsCumulated->isChecked() ? 1 : 0;
		//converto string in this format "31.10.2012", "01.01.2013"
		QString strDateFrom = (bUseDate && bFromDate) ? date.toString("dd.MM.yyyy") : ui.dateFrom->date().toString("dd.MM.yyyy");
		QString strDateTo = (bUseDate && !bFromDate) ? date.toString("dd.MM.yyyy") : ui.dateTo->date().toString("dd.MM.yyyy");

		//QMessageBox::information(this, "Error", QString("Search params: code=[%1]\n\tdate type=[%2]\n\tcumulated=[%3]\n\tdate from=[%4]\n\tdate to=[%5]").arg(strCode).arg(nDateType).arg(nCumulated).arg(strDateFrom).arg(strDateTo));
		//QString strTxt = QString("%1\t%2\t%3\t%4\t%5\t").arg(strCode).arg(nDateType).arg(nCumulated).arg(strDateFrom).arg(strDateTo);
		//QInputDialog dlg;
		//dlg.setTextValue(strTxt);
		//dlg.exec();

	#ifdef _WIN32
		//fake request: 
		//Ugraditi delay kad kliknem da se pokaze delay i hourglass cursor prije nego se izcrtaju nove brojke. 
		//Delay nek bude jednu sekundu na projektima na level 4 (4 tocke u kodu) ili vise, a dvije sekunde za level 3.
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		QString strTmpCode = strCode;
		strTmpCode = strTmpCode.replace(".", "");
		int nDepth = strCode.length() - strTmpCode.length();
		int nDelayMsec = 500;
		if(nDepth < 4)
			nDelayMsec = 1000;
		::Sleep(nDelayMsec);
		QApplication::restoreOverrideCursor();
		//QMessageBox::information(this, "Error", QString("Level %1, delay: %2 (code: %3)").arg(nDepth).arg(nDelayMsec).arg(strCode));
	#endif

		//search exact project code
		m_dataFake.clearSelection();	//TOFIX not needed
		m_dataFake.find(0, strCode, false, false);		//select all matching codes
		int nCnt = m_dataFake.getSelectedCount();
		qDebug() << nCnt << "items selected by project code" << strCode;
		//QMessageBox::information(this, "Error", QString("Rows selected by project code [%1]: [%2]:\n%3").arg(strCode).arg(nCnt).arg(DumpSelectedRows()));
		m_dataFake.find(1, nDateType, false, true);		//subselect all matching date type
		nCnt = m_dataFake.getSelectedCount();
		qDebug() << nCnt << "items selected by data type" << nDateType;
		//QMessageBox::information(this, "Error", QString("Sub rows selected by date type [%1]: [%2]:\n%3").arg(nDateType).arg(nCnt).arg(DumpSelectedRows()));
		m_dataFake.find(2, nCumulated, false, true);	//subselect all matching cumulated field lines
		nCnt = m_dataFake.getSelectedCount();
		qDebug() << nCnt << "items selected by cumulated type" << nCumulated;
		//QMessageBox::information(this, "Error", QString("Sub rows selected by cumulated flag [%1]: [%2]:\n%3").arg(nCumulated).arg(nCnt).arg(DumpSelectedRows()));
		if(nDateType > 0){
			//QMessageBox::information(this, "Error", QString("Date From: [%1]").arg(strDateFrom));
			m_dataFake.find(3, strDateFrom, false, true);	//subselect all matching date from lines
			nCnt = m_dataFake.getSelectedCount();
			//QMessageBox::information(this, "Error", QString("Sub rows selected by date from [%1]: [%2]:\n%3").arg(strDateFrom).arg(nCnt).arg(DumpSelectedRows()));
			if(nOldDateType > 1){
				//QMessageBox::information(this, "Error", QString("Date To: [%1]").arg(strDateFrom));
				m_dataFake.find(4, strDateTo, false, true);	//subselect all matching date to lines
				nCnt = m_dataFake.getSelectedCount();
				//QMessageBox::information(this, "Error", QString("Sub rows selected by date to [%1]: [%2]:\n%3").arg(strDateTo).arg(nCnt).arg(DumpSelectedRows()));
			}
			else{
				//"per date", we must ignore all lines that have "date to" field non-empty
				m_dataFake.find(4, QString(""), false, true);	//subselect all matching date to lines
				nCnt = m_dataFake.getSelectedCount();
				//QMessageBox::information(this, "Error", QString("Sub rows selected after removing non-empty 'date period' rows: [%1]:\n%3").arg(nCnt).arg(DumpSelectedRows()));
			}
		}

		int nRow = m_dataFake.getSelectedRow();
		if(nRow >= 0)
			fillDataFromRow(nRow);
		//else
			//QMessageBox::information(this, "Error", QString("No data found"));
	}
}

void PRM_BudgetingValuation::fillDataFromRow(int nRow)
{
	//line 1
	ui.chkDoNotAddRevTotal		->setChecked(m_dataFake.getDataRef(nRow, 5).toInt() > 0);
	ui.txtBudgetRevTotal		->setText(m_dataFake.getDataRef(nRow, 6).toString());
	ui.txtAccruedRevTotal		->setText(m_dataFake.getDataRef(nRow, 7).toString());
	ui.txtSupplyRevTotal		->setText(m_dataFake.getDataRef(nRow, 8).toString());
	ui.txtRemainingRevTotal		->setText(m_dataFake.getDataRef(nRow, 9).toString());
	ui.txtForecastRevTotal		->setText(m_dataFake.getDataRef(nRow, 10).toString());
	ui.txtDifferenceRevTotal	->setText(m_dataFake.getDataRef(nRow, 11).toString());

	//line 2
	ui.chkDoNotAddRevExpenses	->setChecked(m_dataFake.getDataRef(nRow, 12).toInt() > 0);
	ui.txtBudgetRevExpenses		->setText(m_dataFake.getDataRef(nRow, 13).toString());
	ui.txtAccruedRevExpenses	->setText(m_dataFake.getDataRef(nRow, 14).toString());
	ui.txtSupplyRevExpenses		->setText(m_dataFake.getDataRef(nRow, 15).toString());
	ui.txtRemainingRevExpenses	->setText(m_dataFake.getDataRef(nRow, 16).toString());
	ui.txtForecastRevExpenses	->setText(m_dataFake.getDataRef(nRow, 17).toString());
	ui.txtDifferenceRevExpenses	->setText(m_dataFake.getDataRef(nRow, 18).toString());

	//line 3
	ui.chkDoNotAddRevExtExpenses	->setChecked(m_dataFake.getDataRef(nRow, 19).toInt() > 0);
	ui.txtBudgetRevExtExpenses		->setText(m_dataFake.getDataRef(nRow, 20).toString());
	ui.txtAccruedRevExtExpenses		->setText(m_dataFake.getDataRef(nRow, 21).toString());
	ui.txtSupplyRevExtExpenses		->setText(m_dataFake.getDataRef(nRow, 22).toString());
	ui.txtRemainingRevExtExpenses	->setText(m_dataFake.getDataRef(nRow, 23).toString());
	ui.txtForecastRevExtExpenses	->setText(m_dataFake.getDataRef(nRow, 24).toString());
	ui.txtDifferenceRevExtExpenses	->setText(m_dataFake.getDataRef(nRow, 25).toString());

	//line 4
	ui.chkDoNotAddRevWork			->setChecked(m_dataFake.getDataRef(nRow, 26).toInt() > 0);
	ui.txtBudgetRevWork				->setText(m_dataFake.getDataRef(nRow, 27).toString());
	ui.txtAccruedRevWork			->setText(m_dataFake.getDataRef(nRow, 28).toString());
	ui.txtSupplyRevWork				->setText(m_dataFake.getDataRef(nRow, 29).toString());
	ui.txtRemainingRevWork			->setText(m_dataFake.getDataRef(nRow, 30).toString());
	ui.txtForecastRevWork			->setText(m_dataFake.getDataRef(nRow, 31).toString());
	ui.txtDifferenceRevWork			->setText(m_dataFake.getDataRef(nRow, 32).toString());

	//line 5
	ui.txtBudgetCostsIntTotal		->setText(m_dataFake.getDataRef(nRow, 33).toString());
	ui.txtAccruedCostsIntTotal		->setText(m_dataFake.getDataRef(nRow, 34).toString());
	ui.txtSupplyCostsIntTotal		->setText(m_dataFake.getDataRef(nRow, 35).toString());
	ui.txtRemainingCostsIntTotal	->setText(m_dataFake.getDataRef(nRow, 36).toString());
	ui.txtForecastCostsIntTotal		->setText(m_dataFake.getDataRef(nRow, 37).toString());
	ui.txtDifferenceCostsIntTotal	->setText(m_dataFake.getDataRef(nRow, 38).toString());

	ui.chkDoNotAddCostsExpenses		->setChecked(m_dataFake.getDataRef(nRow, 39).toInt() > 0);
	ui.txtBudgetCostsExpenses		->setText(m_dataFake.getDataRef(nRow, 40).toString());
	ui.txtAccruedCostsExpenses		->setText(m_dataFake.getDataRef(nRow, 41).toString());
	ui.txtSupplyCostsExpenses		->setText(m_dataFake.getDataRef(nRow, 42).toString());
	ui.txtRemainingCostsExpenses	->setText(m_dataFake.getDataRef(nRow, 43).toString());
	ui.txtForecastCostsExpenses		->setText(m_dataFake.getDataRef(nRow, 44).toString());
	ui.txtDifferenceCostsExpenses	->setText(m_dataFake.getDataRef(nRow, 45).toString());

	ui.chkDoNotAddCostsExtExpenses		->setChecked(m_dataFake.getDataRef(nRow, 46).toInt() > 0);
	ui.txtBudgetCostsExtExpenses		->setText(m_dataFake.getDataRef(nRow, 47).toString());
	ui.txtAccruedCostsExtExpenses		->setText(m_dataFake.getDataRef(nRow, 48).toString());
	ui.txtSupplyCostsExtExpenses		->setText(m_dataFake.getDataRef(nRow, 49).toString());
	ui.txtRemainingCostsExtExpenses		->setText(m_dataFake.getDataRef(nRow, 50).toString());
	ui.txtForecastCostsExtExpenses		->setText(m_dataFake.getDataRef(nRow, 51).toString());
	ui.txtDifferenceCostsExtExpenses	->setText(m_dataFake.getDataRef(nRow, 52).toString());

	ui.chkDoNotAddCostsWork			->setChecked(m_dataFake.getDataRef(nRow, 53).toInt() > 0);
	ui.txtBudgetWork				->setText(m_dataFake.getDataRef(nRow, 54).toString());
	ui.txtAccruedCostsWork			->setText(m_dataFake.getDataRef(nRow, 55).toString());
	ui.txtSupplyCostsWork			->setText(m_dataFake.getDataRef(nRow, 56).toString());
	ui.txtRemainingCostsWork		->setText(m_dataFake.getDataRef(nRow, 57).toString());
	ui.txtForecastCostsWork			->setText(m_dataFake.getDataRef(nRow, 58).toString());
	ui.txtDifferenceCostsWork		->setText(m_dataFake.getDataRef(nRow, 59).toString());

	ui.chkDoNotAddCostsTime			->setChecked(m_dataFake.getDataRef(nRow, 60).toInt() > 0);
	ui.txtBudgetTime				->setText(m_dataFake.getDataRef(nRow, 61).toString());
	ui.txtAccruedCostsTime			->setText(m_dataFake.getDataRef(nRow, 62).toString());
	ui.txtSupplyCostsTime			->setText(m_dataFake.getDataRef(nRow, 63).toString());
	ui.txtRemainingCostsTime		->setText(m_dataFake.getDataRef(nRow, 64).toString());
	ui.txtForecastCostsTime			->setText(m_dataFake.getDataRef(nRow, 65).toString());
	ui.txtDifferenceCostsTime		->setText(m_dataFake.getDataRef(nRow, 66).toString());

	ui.txtBudgetBenOverall			->setText(m_dataFake.getDataRef(nRow, 61).toString());
	ui.txtAccruedBenOverall			->setText(m_dataFake.getDataRef(nRow, 62).toString());
	ui.txtSupplyBenOverall			->setText(m_dataFake.getDataRef(nRow, 63).toString());
	ui.txtRemainingBenOverall		->setText(m_dataFake.getDataRef(nRow, 64).toString());
	ui.txtForecastBenOverall		->setText(m_dataFake.getDataRef(nRow, 65).toString());
	ui.txtDifferenceBenOverall		->setText(m_dataFake.getDataRef(nRow, 66).toString());

	ui.txtBudgetBenWork			->setText(m_dataFake.getDataRef(nRow, 67).toString());
	ui.txtAccruedBenWork		->setText(m_dataFake.getDataRef(nRow, 68).toString());
	ui.txtSupplyBenWork			->setText(m_dataFake.getDataRef(nRow, 69).toString());
	ui.txtRemainingBenWork		->setText(m_dataFake.getDataRef(nRow, 70).toString());
	ui.txtForecastBenWork		->setText(m_dataFake.getDataRef(nRow, 71).toString());
	ui.txtDifferenceBenWork		->setText(m_dataFake.getDataRef(nRow, 72).toString());

	ui.txtBudgetBenCostCovTotal		->setText(m_dataFake.getDataRef(nRow, 73).toString());
	ui.txtAccruedBenCostCovTotal	->setText(m_dataFake.getDataRef(nRow, 74).toString());
	ui.txtSupplyBenCostCovTotal		->setText(m_dataFake.getDataRef(nRow, 75).toString());
	ui.txtRemainingBenCostCovTotal	->setText(m_dataFake.getDataRef(nRow, 76).toString());
	ui.txtForecastBenCostCovTotal	->setText(m_dataFake.getDataRef(nRow, 77).toString());
	ui.txtDifferenceBenCostCovTotal	->setText(m_dataFake.getDataRef(nRow, 78).toString());

	ui.txtBudgetBenCostCovWork		->setText(m_dataFake.getDataRef(nRow, 79).toString());
	ui.txtAccruedBenCostCovWork		->setText(m_dataFake.getDataRef(nRow, 80).toString());
	ui.txtSupplyBenCostCovWork		->setText(m_dataFake.getDataRef(nRow, 81).toString());
	ui.txtRemainingBenCostCovWork	->setText(m_dataFake.getDataRef(nRow, 82).toString());
	ui.txtForecastBenCostCovWork	->setText(m_dataFake.getDataRef(nRow, 83).toString());
	ui.txtDifferenceBenCostCovWork	->setText(m_dataFake.getDataRef(nRow, 84).toString());

	ui.txtTotalsFullfillmentTotal	->setText(m_dataFake.getDataRef(nRow, 85).toString());
	ui.txtTotalsRemainingTotal		->setText(m_dataFake.getDataRef(nRow, 86).toString());
	ui.txtTotalsDifferenceTotal		->setText(m_dataFake.getDataRef(nRow, 87).toString());
	ui.txtTotalsFullfillmentWork	->setText(m_dataFake.getDataRef(nRow, 88).toString());
	ui.txtTotalsRemainingWork		->setText(m_dataFake.getDataRef(nRow, 89).toString());
	ui.txtTotalsDifferenceWork		->setText(m_dataFake.getDataRef(nRow, 90).toString());
	ui.txtTotalsContractProportion	->setText(m_dataFake.getDataRef(nRow, 91).toString());
	ui.txtTotalsContractProportionChange	->setText(m_dataFake.getDataRef(nRow, 92).toString());
}

void PRM_BudgetingValuation::on_radTotal_toggled(bool bOn)
{
	if(!bOn) return;

	//hide date range widgets
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);

	ui.dateFrom->hide();
	ui.dateTo->hide();
	ui.labelDateTo->hide();

	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	recalculateData(QDate(), false, false);
}
	
void PRM_BudgetingValuation::on_radPerDate_toggled(bool bOn)
{
	if(!bOn) return;

	//show only "From" date widget
	ui.dateFrom->blockSignals(true);

	ui.dateFrom->show();
	ui.dateTo->hide();
	ui.labelDateTo->hide();

	if(m_bFakeFirstClickToPerDate){
		ui.dateFrom->setDate(QDate(2012,12,31));
		m_bFakeFirstClickToPerDate = false;
	}

	ui.dateFrom->blockSignals(false);

	recalculateData(QDate(), false, false);
}

void PRM_BudgetingValuation::on_radPeriods_toggled(bool bOn)
{
	if(!bOn) return;

	//show only "From" date widget
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);

	ui.dateFrom->show();
	ui.dateTo->show();
	ui.labelDateTo->show();

	if(m_bFakeFirstClickToPerPeriod){
		ui.dateFrom->setDate(QDate(2012,1,1));
		ui.dateTo->setDate(QDate(2012,12,31));
		m_bFakeFirstClickToPerPeriod = false;
	}

	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	recalculateData(QDate(), false, false);
}

void PRM_BudgetingValuation::on_chkIsCumulated_toggled(bool)
{
	recalculateData(QDate(), false, false);
}

void PRM_BudgetingValuation::on_dateFrom_changed_cbk(const QDate &date)
{
	recalculateData(date, true, true);
}
	
void PRM_BudgetingValuation::on_dateTo_changed_cbk(const QDate &date)
{
	recalculateData(date, true, false);
}

QString PRM_BudgetingValuation::DumpSelectedRows()
{
	QString strResult;
	int nStart = 0;
	int nRow = 0;
	while((nRow = m_dataFake.getSelectedRow(nStart)) >= 0){
		strResult += m_dataFake.getDataRef(nRow, 0).toString();
		strResult += "\t";
		strResult += m_dataFake.getDataRef(nRow, 1).toString();
		strResult += "\t";
		strResult += m_dataFake.getDataRef(nRow, 2).toString();
		strResult += "\t";
		strResult += m_dataFake.getDataRef(nRow, 3).toString();
		strResult += "\t";
		strResult += m_dataFake.getDataRef(nRow, 4).toString();
		strResult += "\t";
		strResult += m_dataFake.getDataRef(nRow, 5).toString();
		strResult += "\t";
		strResult += m_dataFake.getDataRef(nRow, 6).toString();
		strResult += "\n";
		nStart = nRow + 1;
	}
	return strResult;
}
