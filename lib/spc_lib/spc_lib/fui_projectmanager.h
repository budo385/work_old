#ifndef FUI_PROJECTMANAGER_H
#define FUI_PROJECTMANAGER_H

#include "generatedfiles/ui_fui_projectmanager.h"
#include "fui_collection/fui_collection/fuibase.h"

class FUI_ProjectManager : public FuiBase
{
	Q_OBJECT

public:
	FUI_ProjectManager(QWidget *parent = 0);
	~FUI_ProjectManager();

	enum SubWindows{
		PRM_BUDGETING=0
	};

private slots:
	void OnProjManagerClicked();

private:
	Ui::FUI_ProjectManager ui;

	Selection_TreeBased*	m_p_Selector;
	SPC_MainMenu*			m_p_Menu;

	int						m_nActiveSubWindow;
	QHash<int,QWidget*>		m_lstSubWindows;
};

#endif // FUI_PROJECTMANAGER_H
