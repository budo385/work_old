#include "fui_projectmanager.h"
#include "prm_budgetingvaluation.h"

FUI_ProjectManager::FUI_ProjectManager(QWidget *parent)
	: FuiBase(parent), m_nActiveSubWindow(-1)
{
	ui.setupUi(this);

	InitFui();
	InitializeFUIWidget(ui.splitter);

	m_p_Menu=ui.frameSpcMenu;
	m_p_Selector=ui.frameSelection;
	m_p_Selector->Initialize(ENTITY_BUS_PROJECT,false, true, true, false);	//init selection controller
	connect(m_p_Menu,SIGNAL(OnProjManagerClicked()),this,SLOT(OnProjManagerClicked()));

	OnProjManagerClicked();
}

FUI_ProjectManager::~FUI_ProjectManager()
{

}



//instance new budgeting eval wind
void FUI_ProjectManager::OnProjManagerClicked()
{
	if (m_nActiveSubWindow==PRM_BUDGETING)
		return;

	if (!m_lstSubWindows.contains(PRM_BUDGETING))
	{
		//create new
		QWidget *pWin = new PRM_BudgetingValuation();
		dynamic_cast<PRM_BudgetingValuation*>(pWin)->SetProjectTree(m_p_Selector);
		m_lstSubWindows[PRM_BUDGETING]=pWin;
	}

	QLayoutItem *pOld = ui.horizontalLayoutContent->takeAt(0);
	if (m_nActiveSubWindow<0)
		delete pOld;

	ui.horizontalLayoutContent->insertWidget(0,m_lstSubWindows[PRM_BUDGETING]);
}
