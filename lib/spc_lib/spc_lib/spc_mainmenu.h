#ifndef SPC_MENU_H
#define SPC_MENU_H

#include <QtWidgets/QFrame>
#include <QVBoxLayout>
#include <QPushButton>

class SPC_MainMenu : public QFrame
{
	Q_OBJECT

public:
	SPC_MainMenu(QWidget *parent);
	~SPC_MainMenu();

signals:
	void OnProjManagerClicked();


private:

protected:
	QVBoxLayout *m_vbox;
	QPushButton *m_btnProjWorkBench;
	QPushButton *m_btnBudgetingValuation;
	QPushButton *m_btnProjResources;
	QPushButton *m_btnReports;

};

#endif // SPC_MENU_H
