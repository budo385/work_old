#ifndef PRM_BUDGETINGVALUATION_H
#define PRM_BUDGETINGVALUATION_H

#include "prm_base.h"
#include "ui_prm_budgetingvaluation.h"

class PRM_BudgetingValuation : public PRM_Base
{
	Q_OBJECT

public:
	PRM_BudgetingValuation(QWidget *parent = 0);
	~PRM_BudgetingValuation();

protected:
	void on_selectionChange(int nEntityRecordID);	
	void recalculateData(const QDate &date, bool bUseDate, bool bFromDate);
	void fillDataFromRow(int nRow);
	QString DumpSelectedRows();

private slots:
	void on_chkIsCumulated_toggled(bool);
	void on_radTotal_toggled(bool);
	void on_radPerDate_toggled(bool);
	void on_radPeriods_toggled(bool);
	void on_dateFrom_changed_cbk(const QDate &date);
	void on_dateTo_changed_cbk(const QDate &date);

private:
	Ui::PRM_BudgetingValuation ui;
	DbRecordSet m_dataFake;
	bool m_bFakeFirstClickToPerDate;
	bool m_bFakeFirstClickToPerPeriod;
};

#endif // PRM_BUDGETINGVALUATION_H
