#ifndef PRM_BASE_H
#define PRM_BASE_H

#include <QWidget>
#include "bus_client/bus_client/selection_treebased.h"
#include "common/common/observer_ptrn.h"

class PRM_Base : public QWidget, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	PRM_Base(QWidget *parent);
	~PRM_Base();

	void SetProjectTree(Selection_TreeBased *pTree);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

protected:
	virtual void on_selectionChange(int nEntityRecordID){};	

	Selection_TreeBased*	m_pSelectionController;
	
};

#endif // PRM_BASE_H
