#include "spc_mainmenu.h"
#include "gui_core/gui_core/gui_helper.h"
#include <QScrollArea>
#include "gui_core/gui_core/thememanager.h"


SPC_MainMenu::SPC_MainMenu(QWidget *parent)
	: QFrame(parent)
{


	QVBoxLayout *vbox=new QVBoxLayout;
	vbox->setSpacing(0);
	vbox->setMargin(0);
	this->setLayout(vbox);


	QFrame *frame=new QFrame;
	QScrollArea* scroll=new QScrollArea;
	scroll->setBackgroundRole(QPalette::Dark);
	scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	scroll->setWidget(frame);
	scroll->setWidgetResizable(true);
	scroll->setVisible(false);

	m_vbox= new QVBoxLayout;
	m_vbox->setSpacing(0);
	m_vbox->setMargin(0);
	frame->setLayout(m_vbox);


	m_btnProjWorkBench  = new QPushButton;
	m_btnBudgetingValuation  = new QPushButton;
	m_btnProjResources  = new QPushButton;
	m_btnReports  = new QPushButton;

	m_btnProjWorkBench->setMaximumHeight(36);
	m_btnProjWorkBench->setMinimumHeight(36);
	m_btnBudgetingValuation->setMaximumHeight(36);
	m_btnBudgetingValuation->setMinimumHeight(36);
	m_btnProjResources->setMaximumHeight(36);
	m_btnProjResources->setMinimumHeight(36);
	m_btnReports->setMaximumHeight(36);
	m_btnReports->setMinimumHeight(36);

	GUI_Helper::CreateStyledButton(m_btnProjWorkBench,"",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Project Workbench")),15,ThemeManager::GetCEMenuButtonStyle(),10);
	GUI_Helper::CreateStyledButton(m_btnBudgetingValuation,"",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Budgeting & Valuation")),15,ThemeManager::GetCEMenuButtonStyle(),10);
	GUI_Helper::CreateStyledButton(m_btnProjResources,"",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Project Resources")),15,ThemeManager::GetCEMenuButtonStyle(),10);
	GUI_Helper::CreateStyledButton(m_btnReports,"",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Reporting")),15,ThemeManager::GetCEMenuButtonStyle(),10);

	m_vbox->addWidget(m_btnProjWorkBench);
	m_vbox->addWidget(m_btnBudgetingValuation);
	m_vbox->addWidget(m_btnProjResources);
	m_vbox->addSpacing(36);
	m_vbox->addWidget(m_btnReports);
	m_vbox->addStretch(1);
	vbox->addWidget(scroll);
	scroll->setVisible(true);

	connect(m_btnProjWorkBench,SIGNAL(clicked()),this,SIGNAL(OnProjManagerClicked()));
	//connect(m_btnBudgetingValuation,SIGNAL(clicked()),this,SIGNAL(ProjBudgetingValuationClicked()));
	//connect(m_btnProjResources,SIGNAL(clicked()),this,SIGNAL(ProjResourceClicked()));
	//connect(m_btnReports,SIGNAL(clicked()),this,SIGNAL(ReportingClicked()));

}

SPC_MainMenu::~SPC_MainMenu()
{

}
