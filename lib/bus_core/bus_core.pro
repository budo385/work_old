TEMPLATE	= lib
CONFIG		= qt warn_on release staticlib
HEADERS		= bus_core/entity_id_collection.h \
		  bus_core/fp_default.h \
		  bus_core/fpdf.h \
		  bus_core/fpsets.h \
		  bus_core/licenseaccessrights.h \
		  bus_core/licenseinfo.h \
		  bus_core/licensemodinfo.h \
		  bus_core/licensestreamreader.h \
		  bus_core/licensestreamwriter.h \
		  bus_core/sessionhandler.h \
		  bus_core/treelist.h
SOURCES		= bus_core/fp_default.cpp \
		  bus_core/fpdf.cpp \
		  bus_core/fpsets.cpp \
		  bus_core/licenseaccessrights.cpp \
		  bus_core/licenseinfo.cpp \
		  bus_core/licensemodinfo.cpp \
		  bus_core/licensestreamreader.cpp \
		  bus_core/licensestreamwriter.cpp \
		  bus_core/sessionhandler.cpp \
		  bus_core/treelist.cpp
INTERFACES	= 
TARGET		= bus_core1
INCLUDEPATH += ./bus_core/generatedfiles \
    ./bus_core/generatedfiles/debug \
    ./bus_core/../../../lib \
    ./bus_core/../../../../lib
