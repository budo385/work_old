#include "commgridviewhelper.h"

#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/datahelper.h"

void CommGridViewHelper::SetupSortRecordSet(DbRecordSet &lstSortColumnSetup)
{
	lstSortColumnSetup.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_GRID_COLUMN_SETUP));
	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Contact"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "CONTACT");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Project"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "PROJECT");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Date 1"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "DATE_1");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Date 2"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "DATE_2");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Status"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "STATUS");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Application type"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "APP_TYPE");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Subtype"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "SUBTYPE");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Media"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "MEDIA");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Originator"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "ORIGINATOR");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_HEADER_TEXT", tr("Owner"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_NAME", "OWNER");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_HEADER_TEXT", tr("Name"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_COLUMN_NAME", "SUBJECT");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_HEADER_TEXT", tr("Task Priority"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_COLUMN_NAME", "TASK_PRIORITY");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	lstSortColumnSetup.addRow();
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_HEADER_TEXT", tr("Task Type ID"));
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_COLUMN_NAME", "TASK_TYPE_ID");
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	lstSortColumnSetup.setData(lstSortColumnSetup.getRowCount()-1,"BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.
}

int CommGridViewHelper::CheckTask(DbRecordSet &recEntityRecord,int nRow/*=0*/)
{
	if (recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull())
		return NOT_SCHEDULED;

	bool bTaskActive = recEntityRecord.getDataRef(nRow, "BTKS_IS_TASK_ACTIVE").toBool();
	if (!bTaskActive)
		return NOT_SCHEDULED;

	int nTaskStatus = recEntityRecord.getDataRef(nRow, "BTKS_SYSTEM_STATUS").toInt();
	if (nTaskStatus == GlobalConstants::TASK_STATUS_COMPLETED || nTaskStatus == GlobalConstants::TASK_STATUS_CANCELLED)
		return NOT_SCHEDULED;

	int nStatus;
	bool bStartUnvalid			= recEntityRecord.getDataRef(nRow, "BTKS_START").isNull();
	bool bDueUnvalid			= recEntityRecord.getDataRef(nRow, "BTKS_DUE").isNull();
	//Check is start date reached.
	bool bStartPassed;
	if(!bStartUnvalid)
	{
		if(recEntityRecord.getDataRef(nRow, "BTKS_START").toDateTime() > QDateTime::currentDateTime())
			bStartPassed = false;
		else
			bStartPassed = true;
	}
	//Check is due date reached.
	bool bDuePassed;
	if (!bDueUnvalid)
	{
		if (recEntityRecord.getDataRef(nRow, "BTKS_DUE").toDateTime() > QDateTime::currentDateTime())
			bDuePassed = false;
		else
			bDuePassed = true;
	}

	//If start date invalid then it must at least be due now
	if	(bStartUnvalid)
	{
		if(bDueUnvalid)
			nStatus = SCHEDULED_DUENOW;
		else
		{
			if (bDuePassed)
				nStatus = SCHEDULED_OVERDUE;
			else
				nStatus = SCHEDULED_DUENOW;
		}

	}
	//If start valid - then it needs to be checked.
	else
	{
		//If start passed - due.
		if (bStartPassed)
		{
			if(bDueUnvalid)
				nStatus = SCHEDULED_DUENOW;
			else
			{
				if (bDuePassed)
					nStatus = SCHEDULED_OVERDUE;
				else
					nStatus = SCHEDULED_DUENOW;
			}
		}
		//If not yet passed then it can be only scheduled.
		else
			nStatus = SCHEDULED;
	}

	return nStatus;
}

int CommGridViewHelper::CheckItemDirection(int nCEType, DbRecordSet &recEntityRecord,int nRow)
{
	int nInOutDocument;
	//Check is it outgoing, incoming or document.
	if (nCEType == VOICECALL)
	{
		int nDirection = recEntityRecord.getDataRef(nRow, "BVC_DIRECTION").toInt();
		if (nDirection == 0 || nDirection == 2)
			nInOutDocument = OUTGOING;
		else
			nInOutDocument = INCOMING;
	}
	else if (nCEType == EMAIL)
	{
		if (recEntityRecord.getDataRef(nRow, "BEM_OUTGOING").toBool())
			nInOutDocument = OUTGOING;
		else
			nInOutDocument = INCOMING;
	}
	else
		nInOutDocument = INHOUSE;

	return nInOutDocument;
}

QString CommGridViewHelper::GetCommGridFourthIcon(bool bTaskIconExists, int nAppType, int nRowsInGrid, int nRow, QString &strPix4, DbRecordSet recEntityRecord, QHash<int,int> &hshRow2APPTYPE, int nInOutDocument, bool bFromCalendar /*= false*/)
{
	if (nInOutDocument==INHOUSE)
	{
		int AppID=recEntityRecord.getDataRef(nRow,"BDMD_APPLICATION_ID").toInt();
		//4-th icon sort hash.
		hshRow2APPTYPE.insert(nRowsInGrid, AppID);

		DbRecordSet lstApps; 
		Status err;
		if (GetApplicationIconRowID(AppID, lstApps, err)<0) //B.T. wtf!?? ==0 is valid data...
			strPix4 = ":Empty_icon.png";
		else
			strPix4 = QVariant(AppID).toString();
	}
	else
		strPix4 = GetAppTypePixmap(nAppType, nRowsInGrid, hshRow2APPTYPE);

	//If task picture exist and this is task set strPix4 to "-1" so that model takes the 4-th picture from task picture hash.
	if (bTaskIconExists)
		strPix4 = "-1";

	return strPix4;
}

QString CommGridViewHelper::GetAppTypePixmap(int nAppType, int nRowsInGrid, QHash<int,int> &hshRow2APPTYPE)
{
	//Email, document or voice call icon.
	QString strAppIcon = CommGridViewHelper::GetApplicationTypePixmap(nAppType);
	if		(nAppType == CommGridViewHelper::EMAIL)
	{
		hshRow2APPTYPE.insert(nRowsInGrid, CommGridViewHelper::EMAIL_SORT);
	}
	else if (nAppType == CommGridViewHelper::VOICECALL)
	{
		hshRow2APPTYPE.insert(nRowsInGrid, CommGridViewHelper::VOICECALL_SORT);
	}
	else
	{
		hshRow2APPTYPE.insert(nRowsInGrid, CommGridViewHelper::DOCUMENT_SORT);
	}

	return strAppIcon;
}

QString CommGridViewHelper::GetApplicationTypePixmap(int nAppType)
{
	QString strAppIcon;
	if		(nAppType == CommGridViewHelper::EMAIL)
	{
		strAppIcon = ":Outlook.png";
	}
	else if (nAppType == CommGridViewHelper::VOICECALL)
	{
		strAppIcon = ":SkypeBlue_32x32.png";
	}
	else
	{
		strAppIcon = ":Word32.png";
	}

	return strAppIcon;
}

QHash<int, QString> CommGridViewHelper::GetDocumentTypeIconNames()
{
	QHash<int, QString> hshDocIconNames;
	//BT issue 2313,2308
	//QPixmap docTypeIcon;
	//docTypeIcon.load(":Local_File_32.png");
	hshDocIconNames.insert(GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE, QString(":Local_File_32.png"));

	//	QPixmap docTypeIcon1;
	//docTypeIcon.load(":Address_32.png");
	hshDocIconNames.insert(GlobalConstants::DOC_TYPE_URL, QString(":Address_32.png"));

	//	QPixmap docTypeIcon2;
	//docTypeIcon.load(":Paper_Docs_32.png");
	hshDocIconNames.insert(GlobalConstants::DOC_TYPE_PHYSICAL, QString(":Paper_Docs_32.png"));

	//	QPixmap docTypeIcon3;
	//docTypeIcon.load(":Note_32.png");
	hshDocIconNames.insert(GlobalConstants::DOC_TYPE_NOTE, QString(":Note_32.png"));

	//	QPixmap docTypeIcon4;
	//docTypeIcon.load(":Internet_File_32.png");
	hshDocIconNames.insert(GlobalConstants::DOC_TYPE_INTERNET_FILE, QString(":Internet_File_32.png"));

	return hshDocIconNames;
}

//returns icon or empty if not fond
int CommGridViewHelper::GetApplicationIconRowID(int nApplicationID, DbRecordSet &lstApps, Status &err)
{
	GetDocApplicationsFromServer(lstApps, err);
	int nRowID=lstApps.find(0,nApplicationID,true);
	return nRowID;
}

QStringList CommGridViewHelper::CreateThirdColumnStrings(int nCEType, DbRecordSet &recEntityRecord,int nRow)
{
	//Make owner string.
	QString strDataOwner;
	//If this is a task then use task owner else use entity owner (Marin - issue #673).
	if (recEntityRecord.getDataRef(nRow, "BTKS_ID").toInt() > 0)
	{
		if (!recEntityRecord.getDataRef(nRow, "A2.BPER_LAST_NAME").toString().isEmpty())
			strDataOwner = recEntityRecord.getDataRef(nRow, "A2.BPER_LAST_NAME").toString();

		if(!strDataOwner.isEmpty() && !recEntityRecord.getDataRef(nRow, "A2.BPER_FIRST_NAME").toString().isEmpty())
			strDataOwner += " " + recEntityRecord.getDataRef(nRow, "A2.BPER_FIRST_NAME").toString();
		else if (!recEntityRecord.getDataRef(nRow, "A2.BPER_FIRST_NAME").toString().isEmpty())
			strDataOwner += recEntityRecord.getDataRef(nRow, "A2.BPER_FIRST_NAME").toString();

		if(!strDataOwner.isEmpty() && !recEntityRecord.getDataRef(nRow, "B1.BORG_NAME").toString().isEmpty())
			strDataOwner += ", " + recEntityRecord.getDataRef(nRow, "B1.BORG_NAME").toString();
		else if (!recEntityRecord.getDataRef(nRow, "B1.BORG_NAME").toString().isEmpty())
			strDataOwner += recEntityRecord.getDataRef(nRow, "B1.BORG_NAME").toString();
	}
	else
	{
		if (!recEntityRecord.getDataRef(nRow, "A.BPER_LAST_NAME").toString().isEmpty())
			strDataOwner = recEntityRecord.getDataRef(nRow, "A.BPER_LAST_NAME").toString();

		if(!strDataOwner.isEmpty() && !recEntityRecord.getDataRef(nRow, "A.BPER_FIRST_NAME").toString().isEmpty())
			strDataOwner += " " + recEntityRecord.getDataRef(nRow, "A.BPER_FIRST_NAME").toString();
		else if (!recEntityRecord.getDataRef(nRow, "A.BPER_FIRST_NAME").toString().isEmpty())
			strDataOwner += recEntityRecord.getDataRef(nRow, "A.BPER_FIRST_NAME").toString();

		if(!strDataOwner.isEmpty() && !recEntityRecord.getDataRef(nRow, "B.BORG_NAME").toString().isEmpty())
			strDataOwner += ", " + recEntityRecord.getDataRef(nRow, "B.BORG_NAME").toString();
		else if (!recEntityRecord.getDataRef(nRow, "B.BORG_NAME").toString().isEmpty())
			strDataOwner += recEntityRecord.getDataRef(nRow, "B.BORG_NAME").toString();
	}

	//Make contact string.
	QString strContactData;
	if (!recEntityRecord.getDataRef(nRow, "BCNT_LASTNAME").toString().isEmpty())
		strContactData = recEntityRecord.getDataRef(nRow, "BCNT_LASTNAME").toString();

	if (!strContactData.isEmpty() && !recEntityRecord.getDataRef(nRow, "BCNT_FIRSTNAME").toString().isEmpty())
		strContactData += " " + recEntityRecord.getDataRef(nRow, "BCNT_FIRSTNAME").toString();
	else if (!recEntityRecord.getDataRef(nRow, "BCNT_FIRSTNAME").toString().isEmpty())
		strContactData += recEntityRecord.getDataRef(nRow, "BCNT_FIRSTNAME").toString();

	if (!strContactData.isEmpty() && !recEntityRecord.getDataRef(nRow, "BCNT_ORGANIZATIONNAME").toString().isEmpty())
		strContactData += ", " + recEntityRecord.getDataRef(nRow, "BCNT_ORGANIZATIONNAME").toString();
	else if (!recEntityRecord.getDataRef(nRow, "BCNT_ORGANIZATIONNAME").toString().isEmpty())
		strContactData += recEntityRecord.getDataRef(nRow, "BCNT_ORGANIZATIONNAME").toString();

	//Make project string.
	QString strProjectData	= recEntityRecord.getDataRef(nRow, "BUSP_CODE").toString()	 + \
		" "	 + recEntityRecord.getDataRef(nRow, "BUSP_NAME").toString();
	if (recEntityRecord.getDataRef(nRow, "BUSP_CODE").isNull())
		strProjectData = "";

	QStringList list;
	list << strDataOwner << strContactData << strProjectData;

	return list;
}

QString	CommGridViewHelper::GetTaskPixmap(int nTask, int nRowsInGrid, QHash<int,int> &hshRow2STATUS)
{
	//Set task icon.
	QString strTaskIcon;
	if		(nTask == SCHEDULED)
	{
		strTaskIcon = ":OnHold32.png";
		hshRow2STATUS.insert(nRowsInGrid, SCHEDULED_SORT);
	}
	else if (nTask == SCHEDULED_DUENOW)
	{
		strTaskIcon = ":ToDo32.png";
		hshRow2STATUS.insert(nRowsInGrid, SCHEDULED_DUENOW_SORT);
	}
	else if (nTask == SCHEDULED_OVERDUE)
	{
		strTaskIcon = ":Overdue32.png";
		hshRow2STATUS.insert(nRowsInGrid, SCHEDULED_OVERDUE_SORT);
	}
	else
	{
		strTaskIcon = ":Empty_icon.png";
		hshRow2STATUS.insert(nRowsInGrid, EMPTY_SORT);
	}

	return strTaskIcon;
}

QString	CommGridViewHelper::GetCETypePixmap(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow, QHash<int,int> &hshRow2SUBTYPE)
{
	//Email, document or voice call icon.
	QString strCETypeIcon;
	if		(nCEType == EMAIL)
	{
		//_DUMP(recEntityRecord);
		if (recEntityRecord.getDataRef(nRow, "EMAIL_ATTACHMENT_COUNT").toInt() > 0)
		{
			strCETypeIcon = ":Attachment32.png";
			hshRow2SUBTYPE.insert(nRowsInGrid, SECOND_ICON_EMAIL_SORT);
		}
		else
		{
			strCETypeIcon = ":Email32.png";
			hshRow2SUBTYPE.insert(nRowsInGrid, SECOND_ICON_EMAIL_SORT);
		}
	}
	else if (nCEType == VOICECALL)
	{
		strCETypeIcon = ":Phone32.png";
		hshRow2SUBTYPE.insert(nRowsInGrid, SECOND_ICON_VOICECALL_SORT);
	}
	else
	{
		strCETypeIcon = ":Document32.png";
		hshRow2SUBTYPE.insert(nRowsInGrid, SECOND_ICON_DOCUMENT_SORT);
	}

	return strCETypeIcon;
}

QString CommGridViewHelper::GetInOutDocumentPixmap(int nInOutDocument, int nRowsInGrid, QHash<int,int> &hshRow2MEDIA)
{
	//Incoming, outgoing or document.
	QString strInOutIcon;
	if		(nInOutDocument == CommGridViewHelper::INCOMING)
	{
		strInOutIcon = ":Incoming32.png";
		hshRow2MEDIA.insert(nRowsInGrid, THIRD_ICON_INCOMING_SORT);
	}
	else if (nInOutDocument == CommGridViewHelper::OUTGOING)
	{
		strInOutIcon = ":Outgoing32.png";
		hshRow2MEDIA.insert(nRowsInGrid, THIRD_ICON_OUTGOING_SORT);
	}
	else
	{
		strInOutIcon = ":InHouse32.png";
		hshRow2MEDIA.insert(nRowsInGrid, THIRD_ICON_INHOUSE_SORT);
	}

	return strInOutIcon;
}
