#include "backupmanager.h"
#include "common/common/datahelper.h"
#include "common/common/config_version_sc.h"
#include "common/common/loggerabstract.h"
#include <QDir>
#include <QCoreApplication>
#include <QFile>

#define MUTEX_LOCK_TIMEOUT 20000


BackupManager::BackupManager()
:m_bShutDownDatabaseWhenBackup(false),m_RWMutex(QReadWriteLock::Recursive),m_bRestoreInProgress(false)
{


}

BackupManager::~BackupManager()
{
	bool bOK=m_DbActualizator.UnLoad();
	Q_ASSERT(bOK); //breaks if not ok
}


void BackupManager::SetBackupData(Status &pStatus,int nBackupFreq, QString strBackupPath, QString datBackupLastDate, QString strRestoreOnNextStart,int nBackupDay,QString strBackupTime, int nDeleteOldBackupsDay)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	m_nBackupFreq=nBackupFreq;		
	m_strBackupPath=strBackupPath;		
	m_DbActualizator.SetBackupPath(m_strBackupPath);
	m_datBackupLastDate=datBackupLastDate;		
	m_strRestoreOnNextStart=strRestoreOnNextStart;		
	m_strBackupTime=strBackupTime;
	m_nBackupDay=nBackupDay;
	m_nDeleteOldBackupsDay=nDeleteOldBackupsDay;

	m_RWMutex.unlock();
	emit SettingsChanged();
}

void BackupManager::SetBackupData(Status &pStatus,int nBackupFreq, QString strBackupPath, int nBackupDay,QString strBackupTime, int nDeleteOldBackupsDay)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	m_nBackupFreq=nBackupFreq;		
	m_strBackupPath=strBackupPath;		
	m_DbActualizator.SetBackupPath(m_strBackupPath);
	m_strBackupTime=strBackupTime;
	m_nBackupDay=nBackupDay;
	m_nDeleteOldBackupsDay=nDeleteOldBackupsDay;

	m_RWMutex.unlock();
	emit SettingsChanged();
}

void BackupManager::GetBackupData(Status &pStatus,int& nBackupFreq,QString& strBackupPath,QString& datBackupLastDate,QString& strRestoreOnNextStart,int& nBackupDay,QString& strBackupTime, int& nDeleteOldBackupsDay)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	nBackupFreq=m_nBackupFreq;		
	strBackupPath=m_strBackupPath;		
	datBackupLastDate=m_datBackupLastDate;		
	strRestoreOnNextStart=m_strRestoreOnNextStart;		
	strBackupTime=m_strBackupTime;
	nBackupDay=m_nBackupDay;
	nDeleteOldBackupsDay=m_nDeleteOldBackupsDay;

	m_RWMutex.unlock();
}

//delayed backup
//in process of backup: delete old ones if possible
void BackupManager::Backup(Status &pStatus)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	if (m_bRestoreInProgress)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		m_RWMutex.unlock();
		return;
	}
	m_RWMutex.unlock();
	emit BackupCommandIssued(-1);
}
//delayed restore
void BackupManager::Restore(Status &pStatus,QString strFileNameForRestore)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	m_strRestoreOnNextStart=strFileNameForRestore;
	m_bRestoreInProgress=true;
	m_RWMutex.unlock();
	emit RestoreCommandIssued(-1);
}

//called after setup backup data:
void BackupManager::Initialize(Status &pStatus,const DbConnectionSettings &ConnSettings,bool bShutDownDatabaseWhenBackup,bool bUseApplicationInstallDirForBackupDir)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	m_DbConnSettings = ConnSettings;
	m_bShutDownDatabaseWhenBackup=bShutDownDatabaseWhenBackup;

	//backup location:
	QDir bcpDir(m_strBackupPath);
	if (!bcpDir.exists() || m_strBackupPath.isEmpty())
	{
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"bcp path:"+m_strBackupPath);
		QString strAppDir;
		if (bUseApplicationInstallDirForBackupDir)
			strAppDir=QCoreApplication::applicationDirPath();
		else
			strAppDir=DataHelper::GetApplicationHomeDir();

		QDir bcpDirCreate(strAppDir);
		if (bcpDirCreate.exists("backup"))
			m_strBackupPath=strAppDir+"/backup";
		else if(bcpDirCreate.mkdir("backup"))
			m_strBackupPath=strAppDir+"/backup";
		else
		{
			pStatus.setError(1,QObject::tr("Can not determine backup directory path!"));
			m_RWMutex.unlock();
			return;
		}
	
		m_DbActualizator.SetBackupPath(m_strBackupPath);
	}

	//template's are in app sudir: they are database dumps->
	QDir templateDir(QCoreApplication::applicationDirPath());
	if (templateDir.exists("template_data"))
		m_strTemplatePath=QCoreApplication::applicationDirPath()+"/template_data";
	else
		m_strTemplatePath="";

	//load actualize dll:
	if(!m_DbActualizator.Initialize(m_DbConnSettings)) 
	{
		pStatus.setError(1,QObject::tr("Can not load db_actualize.dll!"));
		m_RWMutex.unlock();
		return;
	}

	//bring up connection:
	m_DbActualizator.InitConnection(pStatus,m_strBackupPath,m_bShutDownDatabaseWhenBackup);
	
	m_RWMutex.unlock();
}


void BackupManager::StartUpCheck(Status &pStatus)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	CloseDatabase();

	if (CheckDatabaseForRestore())
	{
		ExecRestore(pStatus);
		if (!pStatus.IsOK())
		{
			StartDatabase();
			m_RWMutex.unlock();
			return;
		}
	}

	QDateTime date;
	if (CheckDatabaseForBackup(date))
	{
		ExecBackup(pStatus);
		if (!pStatus.IsOK()) 
		{
			StartDatabase();
			m_RWMutex.unlock();
			return;
		}

	}

	if (CheckDatabaseForReorganization())
	{
		ExecReorganization(pStatus);
		if (!pStatus.IsOK())
		{
			StartDatabase();
			m_RWMutex.unlock();
			return;
		}
	}

	StartDatabase();
	m_RWMutex.unlock();
}


//based on currentdatetime, freq, day & time, determine next start in msec, 0 no start
//return mSEc
int BackupManager::GetNextBackupStart(Status &pStatus,QDateTime timStart)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return 0;
	}


	//reset start datetime to 0 seconds 
	int nSecondsToTarget=timStart.time().second();
	//qDebug()<<nSecondsToTarget;


	if (m_nBackupFreq==BCP_FREQ_NONE)
	{
		m_RWMutex.unlock();
		return 0;
	}


	//on whole hour
	if (m_nBackupFreq==BCP_FREQ_HOURLY)
	{
		QTime Target(timStart.time().addSecs(60*60)); //add 1hr
		Target.setHMS(Target.hour(),0,0); //reset to hour (e.g. 01:00)
		int nSec=abs(timStart.time().msecsTo(Target));
		m_RWMutex.unlock();
		return nSec;
		//return 60000;
	}

	if (m_nBackupFreq==BCP_FREQ_DAILY)
	{
		if (!m_strBackupTime.isEmpty())
		{
			QTime Target=QTime::fromString(m_strBackupTime,"hh:mm");
			//Target=Target.addSecs(nSecondsToTarget);
			if (Target.isValid())
			{
				int nSec=timStart.time().msecsTo(Target);
				if (nSec<0)
				{
					nSec=86400000+nSec;
				}
				m_RWMutex.unlock();
				return nSec;
			}
		}
	
		m_RWMutex.unlock();
		return 86400000; //next 24hrs
	}


	if (m_nBackupFreq==BCP_FREQ_WEEKLY)
	{

		int nDayOffset=7;
		if (m_nBackupDay>=0 && m_nBackupDay<=6)
		{
			int nCurrentDay=timStart.date().dayOfWeek()-1;		//0-MONDY 6-SUNDAY
			if (m_nBackupDay>=nCurrentDay)
				nDayOffset=m_nBackupDay-nCurrentDay;
			else
				nDayOffset=7-nCurrentDay-m_nBackupDay;
		}

		int nMsecOffset=0;
		if (!m_strBackupTime.isEmpty())
		{
			QTime Target=QTime::fromString(m_strBackupTime,"hh:mm");
			//Target=Target.addSecs(nSecondsToTarget);
			if (Target.isValid())
			{
				nMsecOffset=timStart.time().msecsTo(Target);
				if (nMsecOffset<0)
				{
					nMsecOffset=86400000+nMsecOffset;
					if (nDayOffset>0)	//if negative, decrease day->
						nDayOffset--;
				}
			}
		}

		int nOffset=86400000*nDayOffset+nMsecOffset;
		m_RWMutex.unlock();
		return nOffset; 
	}


	if (m_nBackupFreq==BCP_FREQ_MONTHLY)
	{

		QDate dateMonth=timStart.addMonths(1).date();
		QDateTime dateTarget=QDateTime(QDate(dateMonth.year(),dateMonth.month(),0)); //reset days =0
		int nDayOffset=timStart.daysTo(dateTarget);

		int nMsecOffset=0;
		if (!m_strBackupTime.isEmpty())
		{
			QTime Target=QTime::fromString(m_strBackupTime,"hh:mm");
			//Target=Target.addSecs(nSecondsToTarget);
			if (Target.isValid())
			{
				nMsecOffset=timStart.time().msecsTo(Target);
				if (nMsecOffset<0)
				{
					nMsecOffset=86400000-nMsecOffset;
					if (nDayOffset>0)	//if negative, decrease day->
						nDayOffset--;
				}
			}
		}

		int nOffset=86400000*nDayOffset+nMsecOffset;
		m_RWMutex.unlock();
		return nOffset; 
	}

	m_RWMutex.unlock();
	return 0;
}




//on startup, check if DB need reorganization:
bool BackupManager::CheckDatabaseForReorganization()
{
	Status err;

	int nDbVer=m_DbActualizator.GetDbVersion(err);
	if (!err.IsOK()) 
	{
		return false;
	}

	int nDbVerSPC=m_DbActualizator.GetDbVersion_SPC(err);
	if (!err.IsOK()) 
	{
		return false;
	}


	if ((nDbVer!=-1 && nDbVer!=QVariant(DATABASE_VERSION).toInt()) || (nDbVerSPC!=-1 && nDbVerSPC!=QVariant(DATABASE_VERSION_SPC).toInt()))
	{	
		return true;
	}
	

	return false;
}


//on every startup, check if DB need backup:
//Freq= 0- none, 1- monthly, 2- weekly, 3- daily FOR THICK
//Freq= 0- none, 1- monthly, 2- weekly, 3- daily, 4 -hourly  FOR THIN
//DAT: DD.MM.YYYY-HH.MM.SS
bool BackupManager::CheckDatabaseForBackup(QDateTime &datLastBcp)
{

	//date:
	QDateTime::fromString(m_datBackupLastDate,"dd.MM.yyyy-hh.mm.ss");
	if (!datLastBcp.isValid())
	{
		m_datBackupLastDate=QDateTime::currentDateTime().toString("dd.MM.yyyy-hh.mm.ss");
		return false;
	}

	//freq:
	if (m_nBackupFreq==0)
		return false;

	switch(m_nBackupFreq)
	{
	case 1:
		datLastBcp=datLastBcp.addMonths(1);
		break;
	case 2:
		datLastBcp=datLastBcp.addDays(7);
		break;
	case 3:
		datLastBcp=datLastBcp.addDays(1);
		break;
	case 4:
		datLastBcp=datLastBcp.addSecs(60*60);
		break;
	default:
		return false;
		break;
	}

	//exec backup
	if (datLastBcp<QDateTime::currentDateTime())
		return true;
	else
		return false;
}


bool BackupManager::CheckDatabaseForRestore()
{

	if (m_strRestoreOnNextStart.isEmpty())
		return false;
	else
		return true;

}


void BackupManager::ExecReorganization(Status &pStatus)
{

	if (m_bShutDownDatabaseWhenBackup)
		CloseDatabase();//close all connection, although actualize module can be connected


	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_MANAGER_REORGANIZE_START);
	m_DbActualizator.Organize(pStatus,false);
	if (pStatus.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_MANAGER_REORGANIZE_SUCCESS,QVariant(DATABASE_VERSION).toString());
	}

	if (m_bShutDownDatabaseWhenBackup)
		StartDatabase();

}

void BackupManager::ExecRestore(Status &pStatus)
{

	QString strRestore=m_strBackupPath+"/"+m_strRestoreOnNextStart;

	CloseDatabase();//close all connection, although actualize module can be connected


	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_MANAGER_RESTORE_START,strRestore);
	m_DbActualizator.Restore(pStatus,strRestore);

	//enable
	StartDatabase();

	//in event of restore, mark as fresh backup:
	if (pStatus.IsOK())
	{
		m_datBackupLastDate=QDateTime::currentDateTime().toString("dd.MM.yyyy-hh.mm.ss");
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_MANAGER_RESTORE_SUCCESS,strRestore);
		ClearDatabaseAfterRestore(pStatus);
	}

	m_strRestoreOnNextStart=""; //always reset flag
}


QString BackupManager::ExecBackup(Status &pStatus)
{
	if(!m_BackupLock.tryLock(MUTEX_LOCK_TIMEOUT)) //but only 1 thread at time, can exec Backup !
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return "";
	}

	if (m_bShutDownDatabaseWhenBackup)
		CloseDatabase();//close all connection, although actualize module can be connected
	

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_START);
	QString strBcpName=m_DbActualizator.Backup(pStatus);	//close all database when backup in THICK mode!!!!
	if (pStatus.IsOK())
	{
		m_datBackupLastDate=QDateTime::currentDateTime().toString("dd.MM.yyyy-hh.mm.ss");
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_BACKUP_SUCCESS,strBcpName);
	}

	//issue 2576: delete old backups:
	if (m_nDeleteOldBackupsDay>0)
	{
		Status err;
		m_DbActualizator.CleanOldBackups(err, m_nDeleteOldBackupsDay);
		if (!err.IsOK())
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,1,QString("Failed to delete old backups: %1").arg(err.getErrorText()));
		}
	}


	if (m_bShutDownDatabaseWhenBackup)
		StartDatabase();

	m_BackupLock.unlock();
	return strBcpName;
}


void BackupManager::LoadBackupList(Status &pStatus,DbRecordSet &lstBackups)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	QString strPath=m_strBackupPath;
	QString strDbName=m_DbConnSettings.m_strDbName;
	m_RWMutex.unlock();

	DbActualizator::LoadBackupList(pStatus,strPath,lstBackups,strDbName);
}


//---->load zipped files from template dir:
void BackupManager::LoadTemplateList(Status &pStatus,DbRecordSet &lstTemplates)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	if (m_strTemplatePath.isEmpty())
	{
		pStatus.setError(1,QObject::tr("Template Directory:")+m_strTemplatePath+QObject::tr(" does not exists!"));
		m_RWMutex.unlock();
		return;
	}
	QString strPath=m_strTemplatePath;
	m_RWMutex.unlock();
	DbActualizator::LoadBackupList(pStatus,strPath,lstTemplates); //load all dir content
	lstTemplates.sort(0);

/*

	lstTemplates.destroy();
	lstTemplates.addColumn(QVariant::String,"FILE");
	lstTemplates.addColumn(QVariant::DateTime,"DATE");


	//read all files from backup:
	QDir dir(m_strTemplatePath);
	if (!dir.exists())
	{
		pStatus.setError(1,QObject::tr("Template Directory:")+m_strTemplatePath+QObject::tr(" does not exists!"));
		return ;
	}

	//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isFile())
		{
			QString fileName=lstFiles.at(i).fileName();
			QString strOrderString;

			if(lstFiles.at(i).suffix()!="zip")
				continue;

			//order string:
			lstTemplates.addRow();
			lstTemplates.setData(lstTemplates.getRowCount()-1,0,fileName);
		}
	}

	lstTemplates.sort(0);
	*/
}




//called when user selects template database, schedule template for restore: restore db must have set temp, clear flag, and empty password
//TEMPLATENAME.zip
void BackupManager::RestoreFromTemplate(Status &pStatus,QString strTemplateFile)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	//read all files from backup:
	QDir dirTemplate(m_strTemplatePath);
	if (!dirTemplate.exists())
	{
		pStatus.setError(1,QObject::tr("Template Directory:")+m_strTemplatePath+QObject::tr(" does not exists!"));
		m_RWMutex.unlock();
		return ;
	}

	//read all files from backup:
	QDir dirBcp(m_strBackupPath);
	if (!dirBcp.exists())
	{
		pStatus.setError(1,QObject::tr("Backup Directory:")+m_strBackupPath+QObject::tr(" does not exists!"));
		m_RWMutex.unlock();
		return ;
	}

	//remove if exists in target:
	bool bOK=true;
	QFile bcpTemplate(m_strBackupPath+"/"+strTemplateFile);
	if (bcpTemplate.exists())
		bOK=bcpTemplate.remove();
	if (!bOK)
	{
		pStatus.setError(1,QObject::tr("Failed to create template copy:")+strTemplateFile);
		m_RWMutex.unlock();
		return;
	}

	//copy from source:
	QFile sourceTemplate(m_strTemplatePath+"/"+strTemplateFile);
	if (sourceTemplate.exists())
		bOK=sourceTemplate.copy(m_strBackupPath+"/"+strTemplateFile);
	if (!bOK)
	{
		pStatus.setError(1,QObject::tr("Failed to create template copy:")+strTemplateFile);
		m_RWMutex.unlock();
		return;
	}


	//set restore on next start:
	m_strRestoreOnNextStart=strTemplateFile;
	m_bRestoreInProgress=true;
	m_RWMutex.unlock();
	emit RestoreCommandIssued(-1);

	//reset flag:
	//SetAskForTemplateFlag(pStatus,0);
}



void BackupManager::LoadBackupFile(Status &pStatus, QString strBackupName,QByteArray &bcpContent)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	//read all files from backup:
	QDir dirBcp(m_strBackupPath);
	if (!dirBcp.exists())
	{
		pStatus.setError(1,QObject::tr("Backup Directory:")+m_strBackupPath+QObject::tr(" does not exists!"));
		m_RWMutex.unlock();
		return ;
	}
	bool bOK=true;
	QFile bcpFile(m_strBackupPath+"/"+strBackupName);
	if (!bcpFile.exists())
	{
		pStatus.setError(1,QObject::tr("Backup: ")+strBackupName+QObject::tr(" does not exists!"));
		m_RWMutex.unlock();
		return;
	}
	if(!bcpFile.open(QIODevice::ReadOnly))
	{
		pStatus.setError(1,QObject::tr("File: ")+m_strBackupPath+"/"+strBackupName+QObject::tr(" can not be read!"));
		m_RWMutex.unlock();
		return;
	}

	bcpContent=bcpFile.readAll();
	bcpFile.close();
	m_RWMutex.unlock();
}


void BackupManager::SaveBackupFile(Status &pStatus, QString strBackupName,QByteArray &bcpContent)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		m_RWMutex.unlock();
		return;
	}

	//read all files from backup:
	QDir dirBcp(m_strBackupPath);
	if (!dirBcp.exists())
	{
		pStatus.setError(1,QObject::tr("Backup Directory:")+m_strBackupPath+QObject::tr(" does not exists!"));
		m_RWMutex.unlock();
		return ;
	}
	bool bOK=true;
	QFile bcpFile(m_strBackupPath+"/"+strBackupName);
	if (bcpFile.exists())
	{
		pStatus.setError(1,QObject::tr("Backup: ")+strBackupName+QObject::tr(" already exists!"));
		m_RWMutex.unlock();
		return;
	}
	if(!bcpFile.open(QIODevice::WriteOnly))
	{
		pStatus.setError(1,QObject::tr("File: ")+m_strBackupPath+"/"+strBackupName+QObject::tr(" can not be written!"));
		m_RWMutex.unlock();
		return;
	}

	int nErr=bcpFile.write(bcpContent);
	if (nErr==-1)
	{
		pStatus.setError(1,QObject::tr("File: ")+m_strBackupPath+"/"+strBackupName+QObject::tr(" can not be written!"));
		m_RWMutex.unlock();
		return;
	}

	bcpFile.close();
	m_RWMutex.unlock();

}


void BackupManager::DeleteBackupFile(Status &pStatus, QString strBackupName)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		m_RWMutex.unlock();
		return;
	}

	//read all files from backup:
	QDir dirBcp(m_strBackupPath);
	if (!dirBcp.exists())
	{
		pStatus.setError(1,QObject::tr("Backup Directory:")+m_strBackupPath+QObject::tr(" does not exists!"));
		m_RWMutex.unlock();
		return ;
	}
	bool bOK=true;
	QFile bcpFile(m_strBackupPath+"/"+strBackupName);
	if (!bcpFile.exists())
	{
		pStatus.setError(1,QObject::tr("Backup: ")+strBackupName+QObject::tr(" does not exists!"));
		m_RWMutex.unlock();
		return;
	}
	if (!bcpFile.remove())
	{
		pStatus.setError(1,QObject::tr("Backup: ")+strBackupName+QObject::tr(" can not be deleted!"));
		m_RWMutex.unlock();
		return;
	}

	bcpFile.close();
	m_RWMutex.unlock();
	
}


void BackupManager::CheckForDemoDatabase(Status &pStatus, bool bEnableDemo, int nMaxSizeKb, int nWarnSizeKb)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	m_DbActualizator.CheckForDemoDatabase(pStatus,bEnableDemo,nMaxSizeKb,nWarnSizeKb);
	m_RWMutex.unlock();
}
void BackupManager::RecalcDemoDatabaseSize(Status &pStatus, int &nCurrSizeKb)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}
	m_DbActualizator.RecalcDemoDatabaseSize(pStatus,nCurrSizeKb);
	m_RWMutex.unlock();
}