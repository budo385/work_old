#include "picturehelpercore.h"
#include "db_core/db_core/dbsqltableview.h"


//Put in BoEntity before save: saves all pictures 
//pData must have BPIC_PICTURE col
//When deleting, just set PIC_ID=0, trigger will do da job, this will only update existing or add new ones...
//Probably special routine for cleaning DB is needed in future (unlinked PICS)
//Warning: lstData selected rows are important to get ID's back to the list
DbRecordSet PictureHelperCore::PreProcessSavePictureIfNeeded(DbRecordSet &lstData,QString strFieldPictureId)
{
	//get data for write:
	DbRecordSet lstPictureWrite;
	lstPictureWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));

	lstData.clearSelection();
	int nSize=lstData.getRowCount();
	for (int i =0;i<nSize;i++)
	{
		//check if picture exists: (if so, set as marked for write)
		if(lstData.getDataRef(i,"BPIC_PICTURE").toByteArray().size()>0) 
		{
			lstPictureWrite.addRow();
			lstPictureWrite.setData(lstPictureWrite.getRowCount()-1,"BPIC_ID",lstData.getDataRef(i,strFieldPictureId));
			lstPictureWrite.setData(lstPictureWrite.getRowCount()-1,"BPIC_PICTURE",lstData.getDataRef(i,"BPIC_PICTURE"));
			lstData.selectRow(i); //tagg row for write
		}

	}
	return lstPictureWrite;
}


void PictureHelperCore::PostProcessSavePictureIfNeeded(DbRecordSet &lstData,DbRecordSet &lstPictureWrite,QString strFieldPictureId)
{
	int nSize=lstData.getRowCount();
	if(lstPictureWrite.getRowCount()>0)
	{
		//copy back new ID's:
		int j=0;
		for (int i =0;i<nSize;i++)
		{
			if(lstData.isRowSelected(i))
			{
				lstData.setData(i,strFieldPictureId,lstPictureWrite.getDataRef(j,"BPIC_ID"));
				j++;
			}
		}
	}
}