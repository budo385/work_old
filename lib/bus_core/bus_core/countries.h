#ifndef COUNTRIES_H
#define COUNTRIES_H


#include "common/common/dbrecordset.h"
//#include "common/common/observer_ptrn.h"


/*!
	\class Countries
	\brief Hardcoced repository of all countries in all languages
	\ingroup Bus_Core

	Use static calls
*/

class Countries //:public ObsrPtrn_Subject
{

public:
	/*
	enum ObserverSignals				
	{
		LANGUAGE_CHANGE				//emits when lang changes		
	};
	*/

	static void SetLanguage(int nCurrentLanguage=0);
	static void GetCountries(DbRecordSet &list);
	static QString GetCountryFromISOCode(QString strCode);
	static QString GetCountryISOFromPredial(QString strPredial);
	static QString GetDsCodeFromCountry(QString strCountry);
	static QString GetDsCodeFromISO(QString strISOCode);
	static QString GetISOCodeFromCountry(QString strCountry);
	static QString GetISOCodeFromDs(QString strDsCode);
	static QString GetPredialFromISO(QString strISO);

private:
	static void GenerateCountryEnglish();
	static void GenerateCountryGerman();

	
};

#endif // COUNTRIES_H
