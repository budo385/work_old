<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de_CH">
<context>
    <name>CommGridViewHelper</name>
    <message>
        <location filename="commgridviewhelper.cpp" line="12"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="18"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="24"/>
        <source>Date 1</source>
        <translation>Datum 1</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="30"/>
        <source>Date 2</source>
        <translation>Datum 2</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="36"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="42"/>
        <source>Application type</source>
        <translation>Anwendungs-Typ</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="48"/>
        <source>Subtype</source>
        <translation>Untertyp</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="54"/>
        <source>Media</source>
        <translation>Medien</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="60"/>
        <source>Originator</source>
        <translation>Urheber</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="66"/>
        <source>Owner</source>
        <translation>Besitzer</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="72"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="78"/>
        <source>Task Priority</source>
        <translation>Aufgabenpriorität</translation>
    </message>
    <message>
        <location filename="commgridviewhelper.cpp" line="84"/>
        <source>Task Type ID</source>
        <translation>Aufgabentyp ID</translation>
    </message>
</context>
<context>
    <name>CommGridViewTableHelper</name>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="74"/>
        <source>by </source>
        <translation>von </translation>
    </message>
</context>
<context>
    <name>ContactTypeManager</name>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Address</source>
        <translation type="obsolete">Adresse</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Language</source>
        <translation type="obsolete">Sprache</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Salutation</source>
        <translation type="obsolete">Anrede</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Title</source>
        <translation type="obsolete">Titel</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Email Address</source>
        <translation type="obsolete">E-Mail-Adresse</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Internet</source>
        <translation type="obsolete">Internet</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Phone</source>
        <translation type="obsolete">Telefon</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Task</source>
        <translation type="obsolete">Aufgabe</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Phone Business</source>
        <translation type="obsolete">Geschäftstelefon</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Phone Private</source>
        <translation type="obsolete">Privattelefon</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Phone Mobile</source>
        <translation type="obsolete">Mobiltelefon</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Phone Private Mobile</source>
        <translation type="obsolete">Privat-Mobiltelefon</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Pager</source>
        <translation type="obsolete">Pager</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Fax</source>
        <translation type="obsolete">Fax</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Skype</source>
        <translation type="obsolete">Skype</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Email Business</source>
        <translation type="obsolete">E-Mail Geschäft</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Email Private</source>
        <translation type="obsolete">E-Mail privat</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Address Business</source>
        <translation type="obsolete">Geschäftsadresse</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Address Private</source>
        <translation type="obsolete">Privatadresse</translation>
    </message>
</context>
<context>
    <name>DbConnectionSetup</name>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Failed to create database! Reason:</source>
        <translation type="obsolete">Datenbank konnte nicht erzeugt werden! Grund:</translation>
    </message>
</context>
<context>
    <name>FormatAddress</name>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>First Name</source>
        <translation type="obsolete">Vorname</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Last Name</source>
        <translation type="obsolete">Nachname</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Middle Name</source>
        <translation type="obsolete">Zweiter Vorname</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Title</source>
        <translation type="obsolete">Titel</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Salutation</source>
        <translation type="obsolete">Anrede</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Language</source>
        <translation type="obsolete">Sprache</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Organization</source>
        <translation type="obsolete">Organisation</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Organization 2</source>
        <translation type="obsolete">Organisation 2</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Street</source>
        <translation type="obsolete">Strasse</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>ZIP/Postal Code</source>
        <translation type="obsolete">PLZ/Postleitzahl</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>City</source>
        <translation type="obsolete">Ort</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Country Code</source>
        <translation type="obsolete">Landescode</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Country</source>
        <translation type="obsolete">Land</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>PO BOX</source>
        <translation type="obsolete">Postfach</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>PO BOX ZIP</source>
        <translation type="obsolete">PLZ Postfach</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Office Code</source>
        <translation type="obsolete">Raumnummer</translation>
    </message>
</context>
<context>
    <name>ImportExportManagerAbstract</name>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Project import/export operation started.</source>
        <translation type="obsolete">Projekt-Datenaustausch gestartet.</translation>
    </message>
    <message>
        <location filename="commgridviewtablehelper.cpp" line="4368828"/>
        <source>Project import/export operation failed: </source>
        <translation type="obsolete">Projekt-Datenaustausch nicht erfolgreich: </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="801"/>
        <source>No files found for processing</source>
        <translation>keine Dateien zum Verarbeiten gefunden</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Project record </source>
        <translation type="obsolete">Projekt-Datensatz </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source> from file </source>
        <translation type="obsolete"> aus der Datei </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Project import/export operation - successfully processed file: </source>
        <translation type="obsolete">Projekt-Datenaustausch - erfolgreich verarbeitete Datei: </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>, failed records: </source>
        <translation type="obsolete">, nicht erfolgreich übertragene Datensätze: </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>, reason:</source>
        <translation type="obsolete">, Ursache:</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="852"/>
        <source>Some records/files failed, see log for details!</source>
        <translation>Fehler bei einzelnen Datensätzen/Dateen aufgetreten, siehe Protokoll für Einzelheiten! </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Project import/export operation ended.</source>
        <translation type="obsolete">Projekt-Datenaustausch beendet.</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>User import/export operation started.</source>
        <translation type="obsolete">Benutzer-Datenaustauch gestartet.</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>User record </source>
        <translation type="obsolete">Benutzer-Datensatz </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>User import/export operation ended.</source>
        <translation type="obsolete">Benutzer-Datenaustauch beendet.</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Debtor record </source>
        <translation type="obsolete">Debitoren-Datensatz </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Debtor import/export operation ended.</source>
        <translation type="obsolete">Debitoren-Datenaustausch beendet.</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Contact record </source>
        <translation type="obsolete">Kontakt-Datensatz </translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Operation can not be executed as project import/export operation is already started!</source>
        <translation type="obsolete">Aufgabe kann nicht ausgeführt werden da der Projekt-Datenaustausch schon gestartet wurde!</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Operation can not be executed as user import/export operation is already started!</source>
        <translation type="obsolete">Aufgabe kann nicht ausgeführt werden da der Benutzer-Datenaustausch schon gestartet wurde!</translation>
    </message>
</context>
<context>
    <name>NMRXManager</name>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Contact-Project</source>
        <translation type="obsolete">Kontakt-Projekt</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Contact-User</source>
        <translation type="obsolete">Kontakt-Benutzer</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Contact-Contact</source>
        <translation type="obsolete">Kontakt-Kontakt</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Project-Project</source>
        <translation type="obsolete">Projekt-Projekt</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>User-Contact</source>
        <translation type="obsolete">Benutzer-Kontakt</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>User-Project</source>
        <translation type="obsolete">Benutzer-Projekt</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Contact</source>
        <translation type="obsolete">Kontakt</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>User</source>
        <translation type="obsolete">Benutzer</translation>
    </message>
    <message>
        <location filename="importexportmanagerabstract.cpp" line="108"/>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="backupmanager.cpp" line="149"/>
        <source>Can not determine backup directory path!</source>
        <translation>Backup-Verzeichnispfad kann nicht bestimmt werden!</translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="167"/>
        <source>Can not load db_actualize.dll!</source>
        <translation>db_actualize.dll kann nicht geladen werden!</translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="18"/>
        <source>Restoring database from </source>
        <translation type="obsolete">Datenbank wiederherstellen aus </translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="623"/>
        <source>Template Directory:</source>
        <translation>Vorlagenverzeichnis:</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="337"/>
        <source> does not exists!</source>
        <translation> nicht gefunden!</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="239"/>
        <source>Template Data could not be fetched!</source>
        <translation type="obsolete">Vorlagendaten können nicht gelesen werden!</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="268"/>
        <source>Backup Directory:</source>
        <translation>Backup-Verzeichnis:</translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="655"/>
        <source>Failed to create template copy:</source>
        <translation>Vorlage konnte nicht kopiert werden:</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="84"/>
        <source>Unable to resolve function call to GetDatabaseCurrentVersion()</source>
        <translation>Aufruf von GetDatabaseCurrentVersion() misslungen</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="154"/>
        <source>Unable to resolve function call to InitializeDatabaseConnection()</source>
        <translation>Aufruf von InitializeDatabaseConnection() misslungen</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="174"/>
        <source>Unable to resolve function call to CloseDatabaseConnection()</source>
        <translation>Aufruf von CloseDatabaseConnection() misslungen</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="195"/>
        <source>Unable to resolve function call to Organize()</source>
        <translation>Aufruf von Organize() misslungen</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="219"/>
        <source>Unable to resolve function call to Backup()</source>
        <translation>Aufruf von Backup() misslungen</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="240"/>
        <source>Unable to resolve function call to Restore()</source>
        <translation>Aufruf zur Wiederherstellung nicht durchführbar</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="21"/>
        <source>Data directory:</source>
        <translation type="obsolete">Datenverzeichnis:</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="21"/>
        <source> could not be created!</source>
        <translation type="obsolete"> konnte nicht erzeugt werden!</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="21"/>
        <source>Failed to create fire.sql script</source>
        <translation type="obsolete">Script fire.sql konnte nicht erzeugt werden</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="87"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="88"/>
        <source>Set of global application options.</source>
        <translation>Set von globalen Anwendungsoptionen.</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="1"/>
        <source>:handwrite.png</source>
        <translation type="obsolete">:handwrite.png</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="258"/>
        <source>Appearance</source>
        <translation>Darstellung</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="101"/>
        <source>Person&apos;s application appearance settings.</source>
        <translation>Einstellungen persönliche Darstellung.</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="113"/>
        <source>Location &amp; Language</source>
        <translation>Standort &amp; Sprache</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="114"/>
        <source>Person&apos;s application location settings.</source>
        <translation>Einstellungen persönliche Anwendungen.</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="126"/>
        <source>Communication grid filter</source>
        <translation>Filter Kommunikationsliste</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="127"/>
        <source>Person&apos;s communication grid filter settings.</source>
        <translation>Einstellungen Filter persönliche Kommunikationsliste.</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="139"/>
        <source>Documents</source>
        <translation>Dokumente</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="153"/>
        <source>File Document Destination Configuration:</source>
        <translation>Konfiguration Dokumenten-Speicherort:</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="152"/>
        <source>Application Document Defaults</source>
        <translation>Defaults Dokumenten-Anwendungen</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="166"/>
        <source>Email Import</source>
        <translation>E-Mail-Import</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="297"/>
        <source>Communication</source>
        <translation>Kommunikation</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="193"/>
        <source>Outlook</source>
        <translation>Outlook</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="1"/>
        <source>Contact</source>
        <translation type="obsolete">Kontakt</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="271"/>
        <source>Default Organization</source>
        <translation>Default-Organisation</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="310"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="27"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="36"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="34"/>
        <source>Salutation</source>
        <translation>Anrede</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="33"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="37"/>
        <source>Email Address</source>
        <translation>E-Mail-Adresse</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="39"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="41"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="43"/>
        <source>Task</source>
        <translation>Aufgabe</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="96"/>
        <source>Phone Business Central</source>
        <translation>Geschäft Zentrale</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="97"/>
        <source>Phone Private</source>
        <translation>Privattelefon</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="98"/>
        <source>Phone Mobile</source>
        <translation>Mobiltelefon</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="99"/>
        <source>Phone Private Mobile</source>
        <translation>Privat-Mobiltelefon</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="100"/>
        <source>Pager</source>
        <translation>Pager</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="101"/>
        <source>Fax</source>
        <translation>Fax</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="102"/>
        <source>Skype</source>
        <translation>Skype</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="120"/>
        <source>Email Business</source>
        <translation>E-Mail Geschäft</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="121"/>
        <source>Email Private</source>
        <translation>E-Mail privat</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="137"/>
        <source>Address Business</source>
        <translation>Geschäftsadresse</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="138"/>
        <source>Address Private</source>
        <translation>Privatadresse</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="1"/>
        <source>Failed to create database! Reason:</source>
        <translation type="obsolete">Datenbank konnte nicht erzeugt werden! Grund:</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="30"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="31"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="32"/>
        <source>Middle Name</source>
        <translation>Zweiter Vorname</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="37"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="38"/>
        <source>Organization 2</source>
        <translation>Organisation 2</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="39"/>
        <source>Street</source>
        <translation>Strasse</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="40"/>
        <source>ZIP/Postal Code</source>
        <translation>PLZ/Postleitzahl</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="41"/>
        <source>City</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="42"/>
        <source>Country Code</source>
        <translation>Landescode</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="43"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="44"/>
        <source>PO BOX</source>
        <translation>Postfach</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="45"/>
        <source>PO BOX ZIP</source>
        <translation>PLZ Postfach</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="46"/>
        <source>Office Code</source>
        <translation>Raumnummer</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="30"/>
        <source>Contact-Project</source>
        <translation>Kontakt-Projekt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="36"/>
        <source>Contact-User</source>
        <translation>Kontakt-Benutzer</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="41"/>
        <source>Contact-Contact</source>
        <translation>Kontakt-Kontakt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="47"/>
        <source>Project-Project</source>
        <translation>Projekt-Projekt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="53"/>
        <source>User-Contact</source>
        <translation>Benutzer-Kontakt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="59"/>
        <source>User-Project</source>
        <translation>Benutzer-Projekt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="266"/>
        <source>Contact</source>
        <comment>_1</comment>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="272"/>
        <source>User</source>
        <comment>_1</comment>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="310"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="874"/>
        <source>Enter/Modify Project Main Data</source>
        <translation>Projekt-Stammdaten erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="889"/>
        <source>Synchronize Projects &amp; Debtors</source>
        <translation>Projekte &amp; Debitoren synchronisieren</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="904"/>
        <source>Import Projects</source>
        <translation>Import Projekte</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="919"/>
        <source>Export Projects</source>
        <translation>Export Projekte</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="934"/>
        <source>Administratrion Tools Access</source>
        <translation>Zugriff Administrationswerkzeuge</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="949"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="964"/>
        <source>Register Applications</source>
        <translation>Anwendungen registrieren</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="979"/>
        <source>Enter/Modify Users</source>
        <translation>Benutzer erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="994"/>
        <source>Enter/Moify all Types &amp; Adress-Schemas (in Menu Organization)</source>
        <translation>Typen &amp; Adress-Schemas erfassen/bearbeiten (im Menü Organisation)</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1009"/>
        <source>Enter/Modify Contacts</source>
        <translation>Kontakte erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1024"/>
        <source>Assign phone calls, emails and documents to projects and contacts</source>
        <translation>Zuweisen von Anrufen, E-Mails und Dokumenten zu Projekten und Kontakten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1039"/>
        <source>Enter/Modify Contact Group Trees</source>
        <translation>Kontakt-Gruppenbäume erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1054"/>
        <source>Register Templates (Docs &amp; Emails)</source>
        <translation>Vorlagen registrieren (Dokumente &amp; E-Mails)</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1371"/>
        <source>Project Manager</source>
        <translation>Projekt-Manager</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1372"/>
        <source>Project Manager Rights Set</source>
        <translation>Zugriffsrechtsset Projekt-Manager</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1383"/>
        <source>Administrator</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1384"/>
        <source>Administrator Rights Set</source>
        <translation>Zugriffsrechtsset Administrator</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1395"/>
        <source>Human Resources</source>
        <translation>Personalverwaltung</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1396"/>
        <source>Human Resources Rights Set</source>
        <translation>Zugriffsrechtsset Personalverwaltung</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1407"/>
        <source>Main Data</source>
        <translation>Stammdaten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1408"/>
        <source>Main Data Rights Set</source>
        <translation>Zugriffsrechtsset Stammdaten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1419"/>
        <source>Contact Manager</source>
        <translation>Kontaktverwalter</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1420"/>
        <source>Contact Manager Rights Set</source>
        <translation>Zugriffsrechtsset Kontaktverwalter</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1431"/>
        <source>Communicator</source>
        <translation>Communicator</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1432"/>
        <source>Communicator Rights Set</source>
        <translation>Zugriffsrechtsset Communicator</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1443"/>
        <source>Group Management</source>
        <translation>Gruppenverwaltung</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1444"/>
        <source>Group Management Rights Set</source>
        <translation>Zugriffsrechtsset Gruppenverwaltung</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1455"/>
        <source>Template Manager</source>
        <translation>Vorlagenverwalter</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1456"/>
        <source>Template Manager Rights Set</source>
        <translation>Zugriffsrechtsset Vorlagenverwalter</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="405"/>
        <source>Unable to resolve function call to CopyDatabase()</source>
        <translation>Aufruf von CopyDatabase() misslungen</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="323"/>
        <source>Program Updates</source>
        <translation>Programm-Updates</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="1"/>
        <source>Card Settings</source>
        <translation type="obsolete">Einstellungen Kontaktkarten</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="103"/>
        <source>Phone Business Direct</source>
        <translation>Geschäft direkt</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="104"/>
        <source>Phone Internal</source>
        <translation>Intern</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="35"/>
        <source>Short Salutation</source>
        <translation>Kurzanrede</translation>
    </message>
    <message>
        <location filename="formataddress.cpp" line="21"/>
        <source>Email Settings</source>
        <translation type="obsolete">Einstellungen E-Mail</translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="786"/>
        <source>Backup: </source>
        <translation>Backup: </translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="747"/>
        <source>File: </source>
        <translation>Datei: </translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="700"/>
        <source> can not be read!</source>
        <translation> kann nicht gelesen werden!</translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="733"/>
        <source> already exists!</source>
        <translation> besteht bereits!</translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="747"/>
        <source> can not be written!</source>
        <translation> kann nicht geschrieben werden!</translation>
    </message>
    <message>
        <location filename="backupmanager.cpp" line="786"/>
        <source> can not be deleted!</source>
        <translation> kann nicht gelöscht werden!</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="206"/>
        <source>Thunderbird</source>
        <translation>Thunderbird</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="336"/>
        <source>Card</source>
        <translation>Karte</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="349"/>
        <source>Email Out</source>
        <translation>E-Mail Ausgang</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="997"/>
        <source>Phones</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="1000"/>
        <source>Addresses</source>
        <translation>Adressen</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="1003"/>
        <source>Emails</source>
        <translation>E-Mails</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="1006"/>
        <source>Internet addresses</source>
        <translation>Internetadressen</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="1009"/>
        <source>Pictures</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="1015"/>
        <source>Debtor data</source>
        <translation>Debitorendaten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1069"/>
        <source>Load Project Lists</source>
        <translation>Projektliste laden</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1084"/>
        <source>Enter/Modify Private Project Lists</source>
        <translation>Persönliche Projektlisten erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1099"/>
        <source>Enter/Modify Public Project Lists</source>
        <translation>Öffentliche Projektlisten erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1114"/>
        <source>Modify Foreign Personal Settings</source>
        <translation>Fremde persönliche Einstellungen bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1467"/>
        <source>Project List Management</source>
        <translation>Projektlistenverwaltung</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1468"/>
        <source>Project List Management Rights Set</source>
        <translation>Zugriffsrechtsset Projektlistenverwaltung</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="307"/>
        <source>Contact</source>
        <comment>nmrx_dlg</comment>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="245"/>
        <source>Contact</source>
        <comment>Why</comment>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="45"/>
        <source>Presence</source>
        <translation>Anwesenheit</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="65"/>
        <source>Calendar Event-User</source>
        <translation>Kalendereintrag-Benutzer</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="72"/>
        <source>Calendar Event-Contact</source>
        <translation>Kalendereintrag-Kontakt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="79"/>
        <source>Calendar Event-Project</source>
        <translation>Kalendereintrag-Projekt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="86"/>
        <source>Calendar Event-Resource</source>
        <translation>Kalendereintrag-Ressource</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="322"/>
        <source>Resource</source>
        <translation>Ressource</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="317"/>
        <source>Calendar Event</source>
        <translation>Kalendereintrag</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="285"/>
        <source>Project</source>
        <comment>_1</comment>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="291"/>
        <source>Resource</source>
        <comment>_1</comment>
        <translation>Ressource</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1129"/>
        <source>Enter/Modify &apos;Category&apos; for Calendar Entries</source>
        <translation>Kategorie für Kalendereinträge erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1145"/>
        <source>Enter/Modify &apos;Show Time As&apos; for Calendar Entries</source>
        <translation>&apos;Zeit anzeigen als&apos; für Kalendereinträge erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1161"/>
        <source>Enter/Modify &apos;Category&apos; for Documents</source>
        <translation>Kategorie für Dokumente erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1177"/>
        <source>Enter/Modify assignment roles</source>
        <translation>Rollen für Zuweisungen erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1192"/>
        <source>Enter/Modify contact assignment roles</source>
        <translation>Rollen von Kontaktzuweisungen erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="47"/>
        <source>Calendar View</source>
        <translation>Kalenderansicht</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="362"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1208"/>
        <source>Open Calendar</source>
        <translation>Kalender öffnen</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1223"/>
        <source>Enter/Modify Calendar Entries</source>
        <translation>Kalendereinträge erfassen/bearbeiten</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1479"/>
        <source>Calendar Access</source>
        <translation>Kalenderzugriff</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1480"/>
        <source>Calendar Access Rights Set</source>
        <translation>Zugriffsrechtsset Kalender</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="337"/>
        <source>Error trying to delete old backups, backup Directory:</source>
        <translation>Fehler beim Löschen alter Backups, Backupverzeichnis:</translation>
    </message>
    <message>
        <location filename="nmrxmanager.cpp" line="94"/>
        <source>Project-Contact</source>
        <translation>Projekt-Kontakt</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="1018"/>
        <source>Contact Groups</source>
        <translation>Kontaktgruppen</translation>
    </message>
    <message>
        <location filename="dbactualizator.cpp" line="108"/>
        <source>Unable to resolve function call to GetDatabaseCurrentVersion_SPC()</source>
        <translation>Unable to resolve function call to GetDatabaseCurrentVersion_SPC()</translation>
    </message>
    <message>
        <location filename="optionsandsettingsorganizer.cpp" line="375"/>
        <source>Email In</source>
        <translation>E-Mail Eingang</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1239"/>
        <source>Assign Contacts</source>
        <translation>Kontakte zuweisen</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="1254"/>
        <source>Assign Projects</source>
        <translation>Projekte zuweisen</translation>
    </message>
    <message>
        <location filename="accessrightsorganizer.cpp" line="859"/>
        <source>Modify Personal Settings of Others</source>
        <translation>Persönliche Einstellungen anderer Benutzer bearbeiten</translation>
    </message>
    <message>
        <location filename="contacttypemanager.cpp" line="1021"/>
        <source>Custom Fields</source>
        <translation>Benutzerdefinierte Felder</translation>
    </message>
</context>
</TS>
