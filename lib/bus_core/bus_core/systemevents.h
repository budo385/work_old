#ifndef SYSTEM_EVENTS_H__
#define SYSTEM_EVENTS_H__

//
// Global list of all system types
// (values compatible to MB's)
//

#define ST_DOCUMENTS		0
#define ST_VOICECALLS		1
#define ST_EMAILS			2

//
// Global list of all system events
// (values compatible to MB's)
//

#define SE_DOCUMENTS_CREATED						1
#define SE_DOCUMENTS_OPENED							2
#define SE_DOCUMENTS_SENT							3
#define SE_DOCUMENTS_RECEIVED						4

#define SE_VOICECALLS_INC_ANSWERED					10
#define SE_VOICECALLS_OUT_INITIATED					11
#define SE_VOICECALLS_MISSED						12
#define SE_VOICECALLS_INC_ANSWERED_BY_OTHER			13
#define SE_VOICECALLS_CALL_BACK_REQUIRED			14
#define SE_VOICECALLS_CALL_BACK_DONE				15
#define SE_VOICECALLS_OUT_NO_ANSWER					16
#define SE_VOICECALLS_OUT_PARTNER_NOT_REACHED		17

#define SE_EMAILS_RECEIVED							30
#define SE_EMAILS_SENT								31
#define SE_EMAILS_FORWARDED							32
#define SE_EMAILS_ANSWERED							33

#endif //SYSTEM_EVENTS_H__
