#include "businessdatatransformer.h"
#include "contacttypemanager.h"
#include "common/common/authenticator.h"
#include "db_core/db_core/dbsqltableview.h"



//lstUser is filled from person data, can be defined or just added
//password can be generated or leaved empty (default)
//username=initials..!?
void BusinessDataTransformer::Person2User(DbRecordSet &lstPerson, DbRecordSet &lstUser,bool bDefineOutputList,bool bRandomPassword)
{

	//define out list if needed
	if (bDefineOutputList)
		lstUser.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USER));

	//if source empty, go away
	if (lstPerson.getRowCount()==0)
		return;


	//cache column _Idx, this is must for import of large recordsets..
	int nCntFirstName	=lstUser.getColumnIdx("CUSR_FIRST_NAME");
	int nCntLastName	=lstUser.getColumnIdx("CUSR_LAST_NAME");
	int nCntPersonID	=lstUser.getColumnIdx("CUSR_PERSON_ID");
	int nCntIsSystem	=lstUser.getColumnIdx("CUSR_IS_SYSTEM");
	int nCntIsLocal		=lstUser.getColumnIdx("CUSR_LOCAL_ACCESS_ONLY");
	int nCntPass		=lstUser.getColumnIdx("CUSR_PASSWORD");
	int nCntActive		=lstUser.getColumnIdx("CUSR_ACTIVE_FLAG");
	int nCntUsername	=lstUser.getColumnIdx("CUSR_USERNAME");

	int nPersID			=lstPerson.getColumnIdx("BPER_ID");
	int nPersFirstName	=lstPerson.getColumnIdx("BPER_FIRST_NAME");
	int nPersLastName	=lstPerson.getColumnIdx("BPER_LAST_NAME");
	int nPersInitials	=lstPerson.getColumnIdx("BPER_INITIALS");

	//copy data:
	QString strUsername;
	QByteArray bytePass;
	int nRow;
	int nSize=lstPerson.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		lstUser.addRow();
		nRow=lstUser.getRowCount()-1;
		lstUser.setData(nRow,nCntActive,1);			
		lstUser.setData(nRow,nCntIsSystem,0);			
		lstUser.setData(nRow,nCntIsLocal,0);
		lstUser.setData(nRow,nCntFirstName,lstPerson.getDataRef(nRow,nPersFirstName));
		lstUser.setData(nRow,nCntLastName,lstPerson.getDataRef(nRow,nPersLastName));
		lstUser.setData(nRow,nCntPersonID,lstPerson.getDataRef(nRow,nPersID));
		lstUser.setData(nRow,nCntUsername,lstPerson.getDataRef(nRow,nPersInitials));
		
		strUsername=lstUser.getDataRef(nRow,nCntUsername).toString();
		if (bRandomPassword)
		{
			bytePass=Authenticator::GenerateRandomPassword(8);
			bytePass=Authenticator::GeneratePassword(strUsername,bytePass); //salt & biber
			lstUser.setData(nRow,nCntPass,bytePass);
		}
		else
		{
			bytePass.clear();
			bytePass=Authenticator::GeneratePassword(strUsername,bytePass); //salt & biber
			lstUser.setData(nRow,nCntPass,bytePass);
		}
	}

}



//lstContact is filled from person data, can be defined or just added, picture is added only if BPIC_PICTURE field exists
//after this->assign new cont id to BPER_CONTACT_ID
void BusinessDataTransformer::Person2Contact(DbRecordSet &lstPerson, DbRecordSet &lstContact,bool bDefineOutputList,bool bCopyPicture,bool bDefinePureContactList)
{
	//define out list if needed
	if (bDefineOutputList)
	{
		if (!bDefinePureContactList)
		{
			lstContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_FUI));
			if (bCopyPicture)
				lstContact.addColumn(QVariant::ByteArray,"BPIC_PICTURE");
		}
		else
			lstContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT));
	}

	//if source empty, go away
	if (lstPerson.getRowCount()==0)
		return;


	//cache column _Idx, this is must for import of large recordsets..
	int nCntType		=lstContact.getColumnIdx("BCNT_TYPE");
	//int nCntAccess		=lstContact.getColumnIdx("BCNT_ACCESS");
	int nCntSupress		=lstContact.getColumnIdx("BCNT_SUPPRESS_MAILING");
	int nCntFirstName	=lstContact.getColumnIdx("BCNT_FIRSTNAME");
	int nCntLastName	=lstContact.getColumnIdx("BCNT_LASTNAME");
	int nCntBirthday	=lstContact.getColumnIdx("BCNT_BIRTHDAY");
	int nCntSex			=lstContact.getColumnIdx("BCNT_SEX");
	int nCntDesc		=lstContact.getColumnIdx("BCNT_DESCRIPTION");
	int nContPic		=lstContact.getColumnIdx("BPIC_PICTURE");
	int nContPicPreviev	=lstContact.getColumnIdx("BCNT_PICTURE");

	int nPersFirstName	=lstPerson.getColumnIdx("BPER_FIRST_NAME");
	int nPersLastName	=lstPerson.getColumnIdx("BPER_LAST_NAME");
	int nPersBirthday	=lstPerson.getColumnIdx("BPER_BIRTH_DATE");
	int nPersSex		=lstPerson.getColumnIdx("BPER_SEX");
	int nPersDesc		=lstPerson.getColumnIdx("BPER_DESCRIPTION");
	int nPersPic		=lstPerson.getColumnIdx("BPIC_PICTURE");
	int nPersPicPreview	=lstPerson.getColumnIdx("BPER_PICTURE");

	//copy data:
	int nRow;
	int nSize=lstPerson.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		lstContact.addRow();
		nRow=lstContact.getRowCount()-1;
		lstContact.setData(nRow,nCntType,ContactTypeManager::CM_TYPE_PERSON);			
		//lstContact.setData(nRow,nCntAccess,ContactTypeManager::ACC_RIGHT_PUBLIC_WRITE);
		lstContact.setData(nRow,nCntSupress,0);								
		lstContact.setData(nRow,nCntFirstName,lstPerson.getDataRef(nRow,nPersFirstName));
		lstContact.setData(nRow,nCntLastName,lstPerson.getDataRef(nRow,nPersLastName));
		lstContact.setData(nRow,nCntBirthday,lstPerson.getDataRef(nRow,nPersBirthday));
		lstContact.setData(nRow,nCntSex,lstPerson.getDataRef(nRow,nPersSex));
		lstContact.setData(nRow,nCntDesc,lstPerson.getDataRef(nRow,nPersDesc));
		if(bCopyPicture && nContPic!=-1 && nPersPic!=-1)
		{
			lstContact.setData(nRow,nContPic,lstPerson.getDataRef(nRow,nPersPic));
			lstContact.setData(nRow,nContPicPreviev,lstPerson.getDataRef(nRow,nPersPicPreview));
		}

	}



}

//lstPerson is filled from contact data, can be defined or just added, picture is added only if BPIC_PICTURE field exists
void BusinessDataTransformer::Contact2Person(DbRecordSet &lstContact, DbRecordSet &lstPerson,bool bDefineOutputList,bool bCopyPicture, DbRecordSet *plstOrg, DbRecordSet *pLstDept)
{


	//define out list if needed
	if (bDefineOutputList)
	{
		lstPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
		if (bCopyPicture)
			lstPerson.addColumn(QVariant::ByteArray,"BPIC_PICTURE");
	}

	//if source empty, go away
	if (lstContact.getRowCount()==0)
		return;


	//cache column _Idx, this is must for import of large recordsets..
	int nCntType		=lstContact.getColumnIdx("BCNT_TYPE");
	//int nCntAccess		=lstContact.getColumnIdx("BCNT_ACCESS");
	int nCntSupress		=lstContact.getColumnIdx("BCNT_SUPPRESS_MAILING");
	int nCntFirstName	=lstContact.getColumnIdx("BCNT_FIRSTNAME");
	int nCntLastName	=lstContact.getColumnIdx("BCNT_LASTNAME");
	int nCntBirthday	=lstContact.getColumnIdx("BCNT_BIRTHDAY");
	int nCntSex			=lstContact.getColumnIdx("BCNT_SEX");
	int nCntDesc		=lstContact.getColumnIdx("BCNT_DESCRIPTION");
	int nContPic		=lstContact.getColumnIdx("BPIC_PICTURE");
	int nContPicPreviev	=lstContact.getColumnIdx("BCNT_PICTURE");
	int nContID			=lstContact.getColumnIdx("BCNT_ID");

	int nPersFirstName	=lstPerson.getColumnIdx("BPER_FIRST_NAME");
	int nPersLastName	=lstPerson.getColumnIdx("BPER_LAST_NAME");
	int nPersBirthday	=lstPerson.getColumnIdx("BPER_BIRTH_DATE");
	int nPersSex		=lstPerson.getColumnIdx("BPER_SEX");
	int nPersDesc		=lstPerson.getColumnIdx("BPER_DESCRIPTION");
	int nPersPic		=lstPerson.getColumnIdx("BPIC_PICTURE");
	int nPersPicPreview	=lstPerson.getColumnIdx("BPER_PICTURE");
	int nPersContID		=lstPerson.getColumnIdx("BPER_CONTACT_ID");
	int nPersOcc		=lstPerson.getColumnIdx("BPER_OCCUPATION");
	int nPersActive		=lstPerson.getColumnIdx("BPER_ACTIVE_FLAG");
	int nPersDateEnter	=lstPerson.getColumnIdx("BPER_DATE_ENTERED");
	int nPersCode		=lstPerson.getColumnIdx("BPER_CODE");
	int nPersInitials	=lstPerson.getColumnIdx("BPER_INITIALS");

	//copy data:
	int nRow;
	int nSize=lstContact.getRowCount();
	QString strFirstName,strLastName,strInitials;
	for (int i=0;i<nSize;++i)
	{
		lstPerson.addRow();
		nRow=lstPerson.getRowCount()-1;
		lstPerson.setData(nRow,nPersOcc,(double)100);
		lstPerson.setData(nRow,nPersActive,1);
		lstPerson.setData(nRow,nPersDateEnter,QDate::currentDate());
		
	
		int nContactID=lstContact.getDataRef(nRow,nContID).toInt();
		lstPerson.setData(nRow,nPersContID,nContactID);

		//first && last name:
		strFirstName=lstContact.getDataRef(nRow,nCntFirstName).toString();
		strLastName=lstContact.getDataRef(nRow,nCntLastName).toString();
		QString strMiddleName=lstContact.getDataRef(nRow,"BCNT_MIDDLENAME").toString();
		strInitials="";
		if (!strFirstName.isEmpty())strInitials+=strFirstName.left(1).toUpper();
		if (!strLastName.isEmpty())strInitials+=strLastName.left(1).toUpper();
		if (!strMiddleName.isEmpty())strInitials+=strMiddleName.left(1).toUpper();
		strInitials=strInitials.trimmed();

		lstPerson.setData(nRow,nPersInitials,strInitials);	//empty string? is this ok?
	
		//TOFIX: how do we know what user wants to set as code?
		lstPerson.setData(nRow,nPersCode,strInitials+"_"+QVariant(nContactID).toString()+"_"+QVariant(i).toString());

		lstPerson.setData(nRow,nPersFirstName,strFirstName);
		lstPerson.setData(nRow,nPersLastName,strLastName);
		lstPerson.setData(nRow,nPersBirthday,lstContact.getDataRef(nRow,nCntBirthday));
		lstPerson.setData(nRow,nPersSex,lstContact.getDataRef(nRow,nCntSex));
		lstPerson.setData(nRow,nPersDesc,lstContact.getDataRef(nRow,nCntDesc));
		
		if(bCopyPicture && nContPic!=-1)
		{
			lstPerson.setData(nRow,nPersPic,lstContact.getDataRef(nRow,nContPic));
			lstPerson.setData(nRow,nPersPicPreview,lstContact.getDataRef(nRow,nContPicPreviev));
		}

		//try to find/match org by name:
		QString strContactOrg=lstContact.getDataRef(nRow,"BCNT_ORGANIZATIONNAME").toString();
		if (!strContactOrg.isEmpty() && plstOrg)
		{
			int nFound=plstOrg->find("BORG_NAME",strContactOrg,true);
			if (nFound>=0)
				lstPerson.setData(nRow,"BPER_ORGANIZATION_ID",plstOrg->getDataRef(nFound,"BORG_ID").toInt());
		}
		QString strContactDept=lstContact.getDataRef(nRow,"BCNT_DEPARTMENTNAME").toString();
		if (!strContactOrg.isEmpty() && pLstDept)
		{
			int nFound=pLstDept->find("BDEPT_NAME",strContactDept,true);
			if (nFound>=0)
				lstPerson.setData(nRow,"BPER_DEPARTMENT_ID",pLstDept->getDataRef(nFound,"BDEPT_ID").toInt());
		}
		
		lstPerson.setData(nRow,"BPER_OCCUPATION",(double)100);	//set to 100

	}



}
