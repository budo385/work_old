#ifndef DBACTUALIZATOR_H
#define DBACTUALIZATOR_H

#include <QLibrary>
#include "db_core/db_core/dbconnsettings.h"
#include "common/common/status.h"
#include "common/common/dbrecordset.h"


class DbActualizator 
{

public:
	DbActualizator();
	~DbActualizator();

	bool		Initialize(DbConnectionSettings &settings);
	bool		UnLoad();
	int			GetDbVersion(Status &pStatus);
	int			GetDbVersion_SPC(Status &pStatus);
	void		InitConnection(Status &pStatus,QString strBackupDirPath,bool bShutDownDatabase=false);
	void		DisConnect(Status &pStatus);
	void		Organize(Status &pStatus, bool bDropData=false);
	void		CopyDatabase(Status &pStatus, DbConnectionSettings &settingsTarget, bool bDestroyPrevious=false);
	QString		Backup(Status &pStatus);
	void		Restore(Status &pStatus,QString strRestoreFilePath);
	void		CleanOldBackups(Status &pStatus, int nDaysOld);
	void		SetBackupPath(QString strBackupDirPath){m_strBackupDirPath=strBackupDirPath;};
	static void LoadBackupList(Status &pStatus,QString strBackupPath,DbRecordSet &lstBackups,QString strDatabaseName="");
	void		CheckForDemoDatabase(Status &pStatus, bool bEnableDemo, int nMaxSizeKb, int nWarnSizeKb); 
	void		RecalcDemoDatabaseSize(Status &pStatus, int &nCurrSizeKb); 
	void		CreateReadOnlyUserForODBCAccess(Status &pStatus);
	void		ImportDataFromOracleExport(Status &pStatus,QString strDataFile, int &nProcessedInsertStatements, int &nProcessedSequenceUpdateStatements);
	void		ImportDataFromOmnisExport(Status &pStatus,QString strDataMigrationDir, int &nProcessedInsertStatements, bool bDropExisting=false);
	
	
	void		OmnisStartConvert(Status &pStatus,QString strInputDir,QString strOutputDir);
	bool		OmnisLoadFile(QString strInputFile, QByteArray &buffer);
	void		OmnisParse(QByteArray &buffer, QStringList &lstFile, QString strFlag);
	bool		OmnisCreateOutput(QString strOneTable, QString strTableName, QString strOutput_h, QByteArray &buffer_h, QString strOutput_cpp, QByteArray &buffer_cpp, QStringList &lstCons, QStringList &lstTriggers, QStringList &lstIndexes, QStringList &lstViews);
	void		TriggerRemoveEmptyLines(QString &strNew);
	bool		OmnisCreateProcedure(QString strOutput_file, QStringList &lstProcedures);
	bool		OmnisGlobalTempTables(QString strOutput_file, QStringList &lstTempTables);
	bool		OmnisGlobalTempTables_Triggers(QString strOutput_file, QStringList &lstTempTables);
	


private:
	QLibrary m_OrganizerDLL;
	DbConnectionSettings m_DbConnParams;
	QString m_strBackupDirPath;
	QHash<QString,QStringList> m_hshNumCols;
	QHash<QString,QStringList> m_hshStrCols;
	
};

#endif // DBACTUALIZATOR_H
