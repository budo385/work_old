#include "dbactualizator.h"
#include <QFileInfo>
#include <QDir>
#include <QStringList>

#include "common/common/datahelper.h"
#include "common/common/loggerabstract.h"

//typedef bool (*tOpenDatabaseConnection)(const char **,const char *,char *);
typedef bool (*tInitializeDatabaseConnection)(const char **,const char *,bool,char *);
typedef bool (*tCloseDatabaseConnection)(char *);
typedef bool (*tOrganizeDatabase)(char *,bool);
typedef bool (*tCopyDatabase)(char *,const char **,bool);
typedef bool (*tBackupDatabase)(char *,const char *szBackupPath,char *);
typedef bool (*tRestoreDatabase)(const char *,char *);
typedef int (*tGetDatabaseCurrentVersion)(char *);
typedef int (*tGetDatabaseCurrentVersion_SPC)(char *);
typedef bool (*tCheckForDemoDatabase)(char *,bool,int,int);
typedef bool (*tRecalcDemoDatabaseSize)(char *,int*);
typedef bool (*tCreateReadOnlyUserForODBCAccess)(char *);
typedef bool (*tImportDataFromOracleExport)(char *,const char *,int*,int*);
typedef bool (*tImportDataFromOmnisExport)(char *,const char *,int*,bool);



DbActualizator::DbActualizator()
{

}

DbActualizator::~DbActualizator()
{

}


bool DbActualizator::Initialize(DbConnectionSettings &settings)
{
	m_DbConnParams=settings;

	//load DLL (if not already):
	if (!m_OrganizerDLL.isLoaded())
	{
		m_OrganizerDLL.setFileName("db_actualize");
		if(!m_OrganizerDLL.load())
		{
			return false;
		}
	}

	//close any prev connection:
	Status err;
	DisConnect(err);

	return true;
}

bool DbActualizator::UnLoad() //before destructor: gently remove DB connection
{
	if (m_OrganizerDLL.isLoaded())
	{
		return m_OrganizerDLL.unload();
	}
	return true;
}



int DbActualizator::GetDbVersion(Status &pStatus)
{
	//read DB version:
	char szError[4096];
	tGetDatabaseCurrentVersion pfGetDatabaseCurrentVersion = (tGetDatabaseCurrentVersion) m_OrganizerDLL.resolve("GetDatabaseCurrentVersion");
	if (pfGetDatabaseCurrentVersion)
	{
		int nDbVersion=pfGetDatabaseCurrentVersion(szError);
		if (nDbVersion==-1)
		{
			pStatus.setError(1,QString(szError));
			return -1;
		}
		return nDbVersion;
	}
	else
	{
		pStatus.setError(1,QObject::tr("Unable to resolve function call to GetDatabaseCurrentVersion()"));
		return -1;

	}

}

int DbActualizator::GetDbVersion_SPC(Status &pStatus)
{
	//read DB version:
	char szError[4096];
	tGetDatabaseCurrentVersion_SPC pfGetDatabaseCurrentVersion = (tGetDatabaseCurrentVersion_SPC) m_OrganizerDLL.resolve("GetDatabaseCurrentVersion_SPC");
	if (pfGetDatabaseCurrentVersion)
	{
		int nDbVersion=pfGetDatabaseCurrentVersion(szError);
		if (nDbVersion==-1)
		{
			pStatus.setError(1,QString(szError));
			return -1;
		}
		return nDbVersion;
	}
	else
	{
		pStatus.setError(1,QObject::tr("Unable to resolve function call to GetDatabaseCurrentVersion_SPC()"));
		return -1;

	}

}



void DbActualizator::InitConnection(Status &pStatus,QString strBackupDirPath, bool bShutDownDatabase)
{
	m_strBackupDirPath=strBackupDirPath;

	//->init DBO with current connection:
	tInitializeDatabaseConnection pfOpenDatabaseConnection = (tInitializeDatabaseConnection) m_OrganizerDLL.resolve("InitializeDatabaseConnection");
	if (pfOpenDatabaseConnection)
	{

		//compile DB settings array:
		const char **pSettingsArray=NULL;
		QList<QByteArray> lstSettingsArray;
		pSettingsArray=new const char*[5];
		lstSettingsArray<<m_DbConnParams.m_strDbType.toLocal8Bit();
		pSettingsArray[0]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();
		lstSettingsArray<<m_DbConnParams.m_strDbHostName.toLocal8Bit();
		pSettingsArray[1]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();
		lstSettingsArray<<m_DbConnParams.m_strDbUserName.toLocal8Bit();
		pSettingsArray[2]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();
		lstSettingsArray<<m_DbConnParams.m_strDbPassword.toLocal8Bit();
		pSettingsArray[3]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();
		lstSettingsArray<<m_DbConnParams.m_strDbName.toLocal8Bit();
		pSettingsArray[4]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();

		char szError[4096];
		bool bOK=pfOpenDatabaseConnection(pSettingsArray,strBackupDirPath.toLocal8Bit().constData(),bShutDownDatabase,szError);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			delete [] pSettingsArray;
			return;
		}
		delete [] pSettingsArray;

	}
	else
	{
		pStatus.setError(1,QObject::tr("Unable to resolve function call to InitializeDatabaseConnection()"));
	}

}

void DbActualizator::DisConnect(Status &pStatus)
{
	char szError[4096];
	tCloseDatabaseConnection pfCloseDatabaseConnection = (tCloseDatabaseConnection) m_OrganizerDLL.resolve("CloseDatabaseConnection");
	if (pfCloseDatabaseConnection)
	{
		bool bOK=pfCloseDatabaseConnection(szError);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return;
		}
	}
	else
	{
		pStatus.setError(1,QObject::tr("Unable to resolve function call to CloseDatabaseConnection()"));
	}

}


void DbActualizator::Organize(Status &pStatus, bool bDropData)
{
	char szError[4096];
	tOrganizeDatabase pfOrganizeDatabase = (tOrganizeDatabase) m_OrganizerDLL.resolve("OrganizeDatabase");
	if (pfOrganizeDatabase)
	{
		bool bOK=pfOrganizeDatabase(szError,bDropData);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return;
		}
	}
	else
	{
		pStatus.setError(1,QObject::tr("Unable to resolve function call to Organize()"));
	}

}

QString DbActualizator::Backup(Status &pStatus)
{

	char szError[4096];
	char szBcpName[1024];
	tBackupDatabase pfBackupDatabase = (tBackupDatabase) m_OrganizerDLL.resolve("BackupDatabase");
	if (pfBackupDatabase)
	{
		bool bOK=pfBackupDatabase(szError,m_strBackupDirPath.toLocal8Bit().constData(),szBcpName);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return "";
		}
		else
			return QString(szBcpName);
	}
	else
	{
		pStatus.setError(1,QObject::tr("Unable to resolve function call to Backup()"));
	}

	return "";
}

void DbActualizator::Restore(Status &pStatus,QString strRestoreFilePath)
{
	char szError[4096];
	tRestoreDatabase pfRestoreDatabase = (tRestoreDatabase) m_OrganizerDLL.resolve("RestoreDatabase");
	if (pfRestoreDatabase)
	{
		bool bOK=pfRestoreDatabase(strRestoreFilePath.toLocal8Bit().constData(),szError);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return;
		}
	}
	else
	{
		pStatus.setError(1,QObject::tr("Unable to resolve function call to Restore()"));
	}


}


void DbActualizator::LoadBackupList(Status &pStatus,QString strBackupPath,DbRecordSet &lstBackups,QString strDbName)
{
	
	//just for firebird, muv out path chars:
	QFileInfo info(strDbName);
	strDbName=info.baseName();

	pStatus.setError(0);

	lstBackups.destroy();
	lstBackups.addColumn(QVariant::String,"FILE");
	lstBackups.addColumn(QVariant::DateTime,"DATE");
	lstBackups.addColumn(QVariant::Int,"SIZE");
	lstBackups.addColumn(QVariant::String,"SIZE_DISPLAY");


	//read all files from backup:
	QDir dir(strBackupPath);
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,"bcp path:"+strBackupPath);
	if (!dir.exists())
	{
		pStatus.setError(1,QObject::tr("Backup Directory:")+strBackupPath+QObject::tr(" does not exists!"));
		return ;
	}

	//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isFile())
		{
			QString fileName=lstFiles.at(i).fileName();
			QString strOrderString;

			if(lstFiles.at(i).suffix()!="zip")
				continue;

			QString strFileTestDb=fileName.left(strDbName.length());
			//QString strFileTest=fileName.mid(strDbName.length());
			//order string:
			QStringList lstParts=fileName.split("_",QString::SkipEmptyParts,Qt::CaseInsensitive);
			if (lstParts.size()<3)
				continue;
			
			strOrderString=lstParts.at(lstParts.size()-2);

			quint64 nSizeFile = lstFiles.at(i).size();

			if (!strDbName.isEmpty()) //if ignore, then load whole dir content
			{
				//QString strDb=lstParts.at(0);
				if (strFileTestDb.toUpper()==strDbName.toUpper())
				{
					lstBackups.addRow();
					lstBackups.setData(lstBackups.getRowCount()-1,0,fileName);
					lstBackups.setData(lstBackups.getRowCount()-1,1,QDateTime::fromString(strOrderString,"yyyyMMddHHmm"));
					lstBackups.setData(lstBackups.getRowCount()-1,2,(int)nSizeFile);
					lstBackups.setData(lstBackups.getRowCount()-1,3,DataHelper::GetFormatedFileSize((double)nSizeFile,2));
				}
			}
			else
			{
				lstBackups.addRow();
				lstBackups.setData(lstBackups.getRowCount()-1,0,fileName);
				lstBackups.setData(lstBackups.getRowCount()-1,1,QDateTime::fromString(strOrderString,"yyyyMMddHHmm"));
				lstBackups.setData(lstBackups.getRowCount()-1,2,(int)nSizeFile);
				lstBackups.setData(lstBackups.getRowCount()-1,3,DataHelper::GetFormatedFileSize((double)nSizeFile,2));
			}
		}
	}

	lstBackups.sort(1,1);
}

void DbActualizator::CleanOldBackups(Status &pStatus, int nDaysOld)
{

	QDateTime datTarget=QDateTime::currentDateTime().addDays(-nDaysOld);

	//just for firebird, muv out path chars:
	QFileInfo info(m_DbConnParams.m_strDbName);
	QString strDbName=info.baseName();

	pStatus.setError(0);

	//read all files from backup:
	QDir dir(m_strBackupDirPath);
	if (!dir.exists())
	{
		pStatus.setError(1,QObject::tr("Error trying to delete old backups, backup Directory:")+m_strBackupDirPath+QObject::tr(" does not exists!"));
		return ;
	}

	//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isFile())
		{
			QString fileName=lstFiles.at(i).fileName();
			QString strOrderString;

			if(lstFiles.at(i).suffix()!="zip")
				continue;

			QString strFileTestDb=fileName.left(strDbName.length());
			QStringList lstParts=fileName.split("_",QString::SkipEmptyParts,Qt::CaseInsensitive);
			if (lstParts.size()<3)
				continue;

			//delete:
			if (lstFiles.at(i).lastModified()<datTarget)
			{
				QFile::remove(lstFiles.at(i).absoluteFilePath());
			}
		}

	}
}



void DbActualizator::CopyDatabase(Status &pStatus, DbConnectionSettings &settingsTarget, bool bDestroyPrevious)
{
	pStatus.setError(0);
	char szError[4096];

	tCopyDatabase pfCopy = (tCopyDatabase) m_OrganizerDLL.resolve("CopyDatabase");
	if (pfCopy)
	{
		//compile DB settings array:
		const char **pSettingsArray=NULL;
		QList<QByteArray> lstSettingsArray;
		pSettingsArray=new const char*[5];
		lstSettingsArray<<settingsTarget.m_strDbType.toLocal8Bit();
		pSettingsArray[0]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();
		lstSettingsArray<<settingsTarget.m_strDbHostName.toLocal8Bit();
		pSettingsArray[1]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();
		lstSettingsArray<<settingsTarget.m_strDbUserName.toLocal8Bit();
		pSettingsArray[2]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();
		lstSettingsArray<<settingsTarget.m_strDbPassword.toLocal8Bit();
		pSettingsArray[3]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();
		lstSettingsArray<<settingsTarget.m_strDbName.toLocal8Bit();
		pSettingsArray[4]=lstSettingsArray.at(lstSettingsArray.size()-1).constData();

		bool bOK=pfCopy(szError,pSettingsArray,bDestroyPrevious);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			delete [] pSettingsArray;
			return;
		}
		delete [] pSettingsArray;
	}
	else
	{
		pStatus.setError(1,QObject::tr("Unable to resolve function call to CopyDatabase()"));
	}

}




void DbActualizator::CheckForDemoDatabase(Status &pStatus, bool bEnableDemo, int nMaxSizeKb, int nWarnSizeKb)
{
	pStatus.setError(0);
	char szError[4096];

	tCheckForDemoDatabase pfCheck = (tCheckForDemoDatabase) m_OrganizerDLL.resolve("CheckForDemoDatabase");
	if (pfCheck)
	{
		bool bOK=pfCheck(szError,bEnableDemo,nMaxSizeKb,nWarnSizeKb);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return;
		}
	}
}
void DbActualizator::RecalcDemoDatabaseSize(Status &pStatus, int &nCurrSizeKb)
{
	pStatus.setError(0);
	char szError[4096];

	tRecalcDemoDatabaseSize pfCheck = (tRecalcDemoDatabaseSize) m_OrganizerDLL.resolve("RecalcDemoDatabaseSize");
	if (pfCheck)
	{
		bool bOK=pfCheck(szError,&nCurrSizeKb);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return;
		}
	}
}


void DbActualizator::CreateReadOnlyUserForODBCAccess(Status &pStatus)
{
	pStatus.setError(0);
	char szError[4096];

	tCreateReadOnlyUserForODBCAccess pCreateODBC = (tCreateReadOnlyUserForODBCAccess) m_OrganizerDLL.resolve("CreateReadOnlyUserForODBCAccess");
	if (pCreateODBC)
	{
		bool bOK=pCreateODBC(szError);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return;
		}
	}
}

void DbActualizator::ImportDataFromOracleExport(Status &pStatus,QString strDataFile, int &nProcessedInsertStatements, int &nProcessedSequenceUpdateStatements)
{
	pStatus.setError(0);
	char szError[4096];

	tImportDataFromOracleExport pImportFromOracle = (tImportDataFromOracleExport) m_OrganizerDLL.resolve("ImportDataFromOracleExport");
	if (pImportFromOracle)
	{
		bool bOK=pImportFromOracle(szError,strDataFile.toLocal8Bit().constData(),&nProcessedInsertStatements, &nProcessedSequenceUpdateStatements);
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return;
		}
	}
}

void DbActualizator::ImportDataFromOmnisExport(Status &pStatus,QString strDataMigrationDir, int &nProcessedInsertStatements, bool bDropExisting)
{
	pStatus.setError(0);
	char szError[4096];

	tImportDataFromOmnisExport pImportFromOmnis = (tImportDataFromOmnisExport) m_OrganizerDLL.resolve("ImportDataFromOmnisExport");
	if (pImportFromOmnis)
	{
		bool bOK=pImportFromOmnis(szError,strDataMigrationDir.toLocal8Bit().constData(),&nProcessedInsertStatements,bDropExisting);
/*
		QFile status_file(QCoreApplication::applicationDirPath()+"/status.log");
		QString strStatus =QString("\nDbActualizator::ImportDataFromOmnisExport - after ImportDataFromOmnisExport, status: %1").arg(bOK);
		if(status_file.open(QIODevice::Append))
		{
			status_file.write(strStatus.toLatin1());
			status_file.close();
		}
*/
		if (!bOK)
		{
			pStatus.setError(1,QString(szError));
			return;
		}
	}
}


void DbActualizator::OmnisStartConvert(Status &pStatus,QString strInputDir,QString strOutputDir)
{
	// error handling
	pStatus.setError(0);
	bool bOK =true;

	// buffers & lists
	QByteArray bufferTables;
	QByteArray bufferCons;
	QByteArray bufferIndexes;
	QByteArray bufferTriggers;
	QByteArray bufferViews;
	QByteArray bufferProcedures;
	QByteArray bufferTempTables;
	QByteArray bufferTempTables_Triggers;
	QByteArray bufferCommonSPCSC_Triggers;

	QStringList lstTables;
	QStringList lstCons;
	QStringList lstIndexes;
	QStringList lstTriggers;
	QStringList lstViews;
	QStringList lstProcedures;
	QStringList lstTempTables;
	QStringList lstTempTables_Triggers;
	QStringList lstCommonSPCSC_Triggers;

	// output file names
	QByteArray buffer_h;
	QByteArray buffer_cpp;
	QString strOutput_h;
	QString strOutput_cpp;

	// general vars
	int nPos;
	QString strTemp;
	QString strTableName;


	// Load files: "Tables.SQL", "Triggers.SQL", "Cons.SQL", "Indexes.SQL", "Views.SQL, Procedures.SQL, GlobalTempTables.SQL, GlobalTempTables_Triggers.SQL"
	for(int i=0; i<9; i++)
	{
		switch(i){
			case 0:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\Tables.SQL", bufferTables);
			break;
			case 1:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\Triggers.SQL", bufferTriggers);
			break;
			case 2:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\Cons.SQL", bufferCons);
			break;
			case 3:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\Indexes.SQL", bufferIndexes);
			break;
			case 4:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\Views.SQL", bufferViews);
			break;
			case 5:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\Procedures.SQL", bufferProcedures);
			break;
			case 6:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\GlobalTempTables.SQL", bufferTempTables);
			case 7:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\GlobalTempTables_Triggers.SQL", bufferTempTables_Triggers);
			case 8:
				bOK = bOK & DbActualizator::OmnisLoadFile(strInputDir + "\\CommonSPCSC_Triggers.SQL", bufferCommonSPCSC_Triggers);
			break;
		}
	}

	if(!bOK){
		pStatus.setError(1,QString("All Required .SQL files (Tables,Triggers,Cons,Indexes,Views,Procedures) are not Present"));
		return;
	}

	// Parse input and create list collections
	OmnisParse(bufferTables, lstTables, "tables");
	OmnisParse(bufferCons, lstCons, "cons");
	OmnisParse(bufferIndexes, lstIndexes, "indexes");
	OmnisParse(bufferViews, lstViews, "views");
	OmnisParse(bufferTriggers, lstTriggers, "triggers");
	OmnisParse(bufferProcedures, lstProcedures, "procedures");
	OmnisParse(bufferTempTables, lstTempTables, "temptables");
	OmnisParse(bufferTempTables_Triggers, lstTempTables_Triggers, "globaltemp_triggers");
	OmnisParse(bufferCommonSPCSC_Triggers, lstCommonSPCSC_Triggers, "globaltemp_triggers");


	for (int i=0;i<lstTables.size();i++)
	{
		if (lstTables.at(i).indexOf("CREATE TABLE")>=0)	
		{	
			// extract TableName
			nPos = lstTables.at(i).indexOf("CREATE TABLE")+12;
			strTemp = lstTables.at(i).mid(nPos,-1).trimmed(); 
			nPos = strTemp.indexOf(" ");
			strTableName = strTemp.mid(0,nPos).trimmed();

			// skip tables with prefix: "f_cm_"
			if(strTableName.left(5).toLower()!="f_cm_" && strTableName.toLower()!="longs")   
			{
				// output file names
				strOutput_h = strOutputDir + "\\dbcreate_" + strTableName.toLower() + ".h";
				strOutput_cpp = strOutputDir + "\\dbcreate_" + strTableName.toLower() + ".cpp";

				if( !OmnisCreateOutput(lstTables[i],strTableName,strOutput_h,buffer_h,strOutput_cpp,buffer_cpp,lstCons,lstTriggers,lstIndexes,lstViews) )
				{
					// disk write error
					pStatus.setError(1,QString("Disk Write Error"));
					return;
				}
			}
		}
	}

	//Procedure write in one file: copy it to the DbSqlProcedureCreation/Init_GlobalTempTables: 
	if(!OmnisCreateProcedure(strOutputDir+"\\_spc_procedures.txt",lstProcedures))
	{
		pStatus.setError(1,QString("Write or Parse Error: OmnisCreateProcedure() failed"));
		return;
	}

	//Global Temp tables write in one file: copy it to the DbSqlProcedureCreation/Init_Firebird: 
	if(!OmnisGlobalTempTables(strOutputDir+"\\_spc_globaltemptables.txt",lstTempTables))
	{
		pStatus.setError(1,QString("Write or Parse Error: OmnisGlobalTempTables() failed"));
		return;
	}

	//Global Temp tables write in one file: copy it to the DbSqlProcedureCreation/Init_Firebird: 
	if(!OmnisGlobalTempTables_Triggers(strOutputDir+"\\_spc_globaltemptables_triggers.txt",lstTempTables_Triggers))
	{
		pStatus.setError(1,QString("Write or Parse Error: OmnisGlobalTempTables_Triggers()/_spc_globaltemptables_triggers failed"));
		return;
	}

	//use same funct:
	//Global Temp tables write in one file: copy it to the DbSqlProcedureCreation/Init_Firebird: 
	if(!OmnisGlobalTempTables_Triggers(strOutputDir+"\\_spc_commonSPCSC_triggers.txt",lstCommonSPCSC_Triggers))
	{
		pStatus.setError(1,QString("Write or Parse Error: OmnisGlobalTempTables_Triggers()/_spc_commonSPCSC_triggers failed"));
		return;
	}



}

bool DbActualizator::OmnisCreateProcedure(QString strOutput_file, QStringList &lstProcedures)
{
	QByteArray buffer;
	int nSize= lstProcedures.size();

	for (int i=0; i<nSize; i++)
	{
		int nPos1=lstProcedures.at(i).indexOf("Recreate Procedure",0,Qt::CaseInsensitive);
		int nPos2=lstProcedures.at(i).indexOf("(");
		if (nPos1<0 || nPos2<0)
			continue;

		nPos1+=QString("Recreate Procedure").length();
		QString strProcName=lstProcedures.at(i).mid(nPos1,nPos2-nPos1).trimmed();

		strProcName.replace("\\","");

		QString strNo=QString::number(i);
		for (int k=0;k<5;k++)
		{
			if (strNo.length()<5)
				strNo.prepend('0');
		}

		buffer.append("dboList[\""+strNo+","+strProcName+"\"]=\"");
		buffer.append(lstProcedures.at(i));
		buffer.append("\";\r\n\r\n");
	}

	QFile proc_file(strOutput_file);
	if(proc_file.open(QIODevice::WriteOnly))
	{
		proc_file.write(buffer);
		proc_file.close();
		return true;
	}
	else
		return false;
}


bool DbActualizator::OmnisGlobalTempTables(QString strOutput_file, QStringList &lstTempTables)
{
	QByteArray buffer;
	int nSize= lstTempTables.size();

	for (int i=0; i<nSize; i++)
	{
		int nPos1=lstTempTables.at(i).indexOf("CREATE GLOBAL TEMPORARY TABLE",0,Qt::CaseInsensitive);
		int nPos2=lstTempTables.at(i).indexOf("\\");
		if (nPos1<0 || nPos2<0)
			continue;

		nPos1+=QString("CREATE GLOBAL TEMPORARY TABLE").length();
		QString strTableName=lstTempTables.at(i).mid(nPos1,nPos2-nPos1).trimmed();

		buffer.append("dboList[\""+strTableName+"\"]=\"");
		buffer.append(lstTempTables.at(i));
		buffer.append("\";\r\n\r\n");
	}

	QFile proc_file(strOutput_file);
	if(proc_file.open(QIODevice::WriteOnly))
	{
		proc_file.write(buffer);
		proc_file.close();
		return true;
	}
	else
		return false;
}


bool DbActualizator::OmnisGlobalTempTables_Triggers(QString strOutput_file, QStringList &lstTriggers)
{
	QByteArray buffer;
	int nSize= lstTriggers.size();

	for (int i=0; i<nSize; i++)
	{
		//B.T. advanced algorithm:
		QString strTriggerName	= lstTriggers[i].split(" ").at(2); //CREATE TRIGGER TR_F_BATCH_JOBS_DELETE FOR F_BATCH_JOBS (3rd string)
		lstTriggers[i]			= lstTriggers[i].replace( QString("BEFORE INSERT OR UPDATE\\"), QString("BEFORE INSERT OR UPDATE \\") ); //add space after

		buffer.append("dboList[\""+strTriggerName+"\"]=\"");
		buffer.append(lstTriggers.at(i));
		buffer.append("\";\r\n\r\n");
	}

	QFile proc_file(strOutput_file);
	if(proc_file.open(QIODevice::WriteOnly))
	{
		proc_file.write(buffer);
		proc_file.close();
		return true;
	}
	else
		return false;
}



bool DbActualizator::OmnisCreateOutput(QString strOneTable, QString strTableName,QString strOutput_h, QByteArray &buffer_h, QString strOutput_cpp, QByteArray &buffer_cpp, QStringList &lstCons, QStringList &lstTriggers, QStringList &lstIndexes, QStringList &lstViews)
{
	// Create .h
	buffer_h.append("#ifndef DBCREATE_"+strTableName.toUpper()+"_H\r\n");
	buffer_h.append("#define DBCREATE_"+strTableName.toUpper()+"_H\r\n");  
	buffer_h.append("\r\n");
	buffer_h.append("#include \"dbsqltablecreation.h\"\r\n"); 
	buffer_h.append("#include \"db_core/db_core/dbtableiddefinition.h\"\r\n"); 
	buffer_h.append("\r\n\r\n");
	buffer_h.append("class DbCreate_"+strTableName.toUpper()+" : public DbSqlTableCreation\r\n");
	buffer_h.append("{\r\n");
	buffer_h.append("public: \r\n");
	buffer_h.append("\tDbCreate_"+strTableName.toUpper()+"():DbSqlTableCreation("+strTableName.toUpper()+"){};\r\n");
	buffer_h.append("\r\n\r\n");
	buffer_h.append("private:\r\n");
	buffer_h.append("\tvoid DefineFields(QStringList &lstSQL);\r\n");
	buffer_h.append("\tvoid DefineTriggers(SQLDBObjectCreate &dboList);\r\n");
	buffer_h.append("\tvoid DefineIndexes(SQLDBObjectCreate &dboList);\r\n");
	buffer_h.append("\tvoid DefineViews(SQLDBObjectCreate &dboList);\r\n");
	buffer_h.append("};\r\n");
	buffer_h.append("\r\n");
	buffer_h.append("#endif // "+strTableName.toUpper()+"_H\r\n");

	// Create .cpp
	buffer_cpp.append("#include \"dbcreate_"+strTableName.toLower()+".h\"\r\n");  // "dbcreate_bus_cm_contact.h"
    buffer_cpp.append("\r\n\r\n");
	buffer_cpp.append("//--------------------------------------------------\r\n");
	buffer_cpp.append("// ODBC - CREATE\r\n");
	buffer_cpp.append("//--------------------------------------------------\r\n");

	// implement methods
	QStringList lstCppMethod = strOneTable.split("\r\n");

	// DefineFields
	buffer_cpp.append("void DbCreate_"+strTableName.toUpper()+"::DefineFields(QStringList &lstSQL) \r\n");
	buffer_cpp.append("{\r\n");
	buffer_cpp.append("\t//Table schema statements for ODBC.\r\n");
	buffer_cpp.append("\tlstSQL.append(\" \t");
	buffer_cpp.append(lstCppMethod[0]+"\r\n");
	for (int i=1; i<lstCppMethod.size(); i++){
		
		if(i == lstCppMethod.size()-1){ // last line without newline
			buffer_cpp.append("\t\t\t\t\t\t\t\t\t\t"+lstCppMethod[i]);
		}
		else{
			buffer_cpp.append("\t\t\t\t\t\t\t\t\t\t"+lstCppMethod[i]+"\r\n");
		}
		
	}
	buffer_cpp.append("\t\");");
	buffer_cpp.append("\r\n\r\n");
	
	int nIdx;
	int nPos;
	int nStart;
	int nEnd;
	
	// DefineFields - constraints
	nIdx = 0;
	QString strLine;
	QString strKeys;

	// BT: 01.11.2012: skip all constraints...some problems occur...
	while(nIdx < lstCons.size())
	{
		nStart = lstCons.at(nIdx).indexOf(strTableName+" ");
		nEnd = lstCons.at(nIdx).indexOf("REFERENCES");
		
		if( nStart >= 0 && nStart<nEnd)
		{	
			buffer_cpp.append("\r\n\r\n\tlstSQL.append(\"\t\t");
			buffer_cpp.append("alter table "+strTableName.toUpper()+"\\\r\n");
			
			// extract foreign key name
			strLine = lstCons[nIdx];													  
			nStart = strLine.indexOf("ADD FOREIGN KEY");
			strKeys = strLine.right(strLine.size()-nStart);					  
			nStart = strKeys.indexOf("(");
			nEnd = strKeys.indexOf(")");
			strKeys = strKeys.mid(nStart+1,nEnd-nStart-1).trimmed();
			
			buffer_cpp.append("\t\t\t\t\t\t\t\t\t\tadd constraint "+strKeys.toUpper()+"_FK foreign key (\\\r\n");
			buffer_cpp.append("\t\t\t\t\t\t\t\t\t\t"+strKeys.toUpper()+")\\\r\n");

			// extract references name
			nStart = strLine.indexOf("REFERENCES");
			strKeys = strLine.right(strLine.size()-nStart);
			nStart = strKeys.indexOf(" ");
			nEnd = strKeys.length();
			strKeys = strKeys.mid(nStart+1,nEnd-nStart-1).trimmed();
			
			buffer_cpp.append("\t\t\t\t\t\t\t\t\t\treferences "+strKeys.toUpper()+"\" );");
		}
		nIdx++;
	}
	

	// END: DefineFields
	buffer_cpp.append("}\r\n\r\n");

	// DefineIndexes
	buffer_cpp.append("void DbCreate_"+strTableName.toUpper()+"::DefineIndexes(SQLDBObjectCreate &dboList) \r\n");
	buffer_cpp.append("{\r\n");
	buffer_cpp.append("\t\t//add index definition by its name (important).\r\n");

	nIdx = 0;
	QString strIndexName;
	while(nIdx < lstIndexes.size())
	{
		nStart = lstIndexes[nIdx].indexOf(strTableName+" ");  //F_Fakt_Plan_Log F_Fakt_Plan " "
		if(nStart>=0)
		{
			// extract index name
				// CREATE UNIQUE INDEX BJ_JOB_ID ON F_Batch_Jobs (BJ_JOB_ID,BJ_SEQUENCE);
				// CREATE INDEX BJ_SESSION_ID ON F_Batch_Jobs (BJ_SESSION_ID,BJ_SEQUENCE);
			nPos = lstIndexes.at(nIdx).indexOf("INDEX ");
			strIndexName = lstIndexes[nIdx].mid(nPos+5,-1).trimmed(); 
			nPos = strIndexName.indexOf(" ");
			strIndexName = strIndexName.left(nPos).trimmed().toUpper();	 

			// ensure this line is not inside IF statement
			nPos = lstIndexes.at(nIdx).indexOf("COMPUTED BY");
			if(nPos<0)
			{
				// prepend "IND_" to index name in list
				nPos = lstIndexes.at(nIdx).indexOf("INDEX ");
				nStart =  lstIndexes.at(nIdx).indexOf(" ",nPos);
				lstIndexes[nIdx].insert(nStart+1,"IND_");

				//buffer_cpp.append("\tdboList[\"IND_"+strIndexName+"\"] = \"create index IND_"+strIndexName+" on "+strTableName+" ("+strIndexName+")\";\r\n");
				buffer_cpp.append("\t\tdboList[\"IND_"+strIndexName+"\"] = \""+lstIndexes[nIdx]+"\";\r\n");
			}
		}
		nIdx++;
	}
	
	nIdx = 0;
	bool bOpenIfBracket = true;
	bool bCloseIfBracket = true;

	while(nIdx < lstIndexes.size())
	{
		nStart = lstIndexes.at(nIdx).indexOf(strTableName+" ");
		if(nStart>=0)
		{
			// extract index name
 			nPos = lstIndexes.at(nIdx).indexOf("INDEX ");
			strIndexName = lstIndexes[nIdx].mid(nPos+5,-1).trimmed(); 
			nPos = strIndexName.indexOf(" ");
 			strIndexName = strIndexName.left(nPos).trimmed().toUpper();	 

			// ensure this line will be written inside IF statement
			nPos = lstIndexes[nIdx].indexOf("COMPUTED BY");
			if(nPos >= 0)
			{
				// prepend "IND_" to index name in list
				nPos = lstIndexes.at(nIdx).indexOf("INDEX ");
				nStart =  lstIndexes.at(nIdx).indexOf(" ",nPos);
				lstIndexes[nIdx].insert(nStart+1,"IND_");
				
				if(bOpenIfBracket)
				{
					// print IF once
					buffer_cpp.append("\r\n\tif(m_DbType==DBTYPE_FIREBIRD)\r\n");
					buffer_cpp.append("\t{\r\n");
					bOpenIfBracket = false;
				}
				buffer_cpp.append("\t\tdboList[\"IND_"+strIndexName+"\"] = \""+lstIndexes[nIdx]+"\";\r\n");
			}
		}
		nIdx++;
	}
	if(!bOpenIfBracket){
		buffer_cpp.append("\r\n\t}\r\n");
		bCloseIfBracket = false;
	}
	// END: DefineIndexes
	buffer_cpp.append("\r\n}\r\n\r\n");

	
	// DefineViews
	buffer_cpp.append("void DbCreate_"+strTableName.toUpper()+"::DefineViews(SQLDBObjectCreate &dboList) \r\n");
	buffer_cpp.append("{\r\n");

	nIdx = 0;
	int nEnd2;
	QString strViewTable;
	QString strViewName;

	int nLengthTablename = strTableName.length();
	int nLengthViewtablename;
	int nLengthLst = lstViews.size();

	while(nIdx < nLengthLst)
	{	
		nPos = lstViews.at(nIdx).lastIndexOf("FROM ");
		nStart = lstViews.at(nIdx).indexOf(strTableName,nPos);
		
		// extract view_table_name: FROM ViewTableName
		if( nStart>0 && (nStart-5) == nPos)
		{

			// extract view_table_name: FROM ViewTableName
 			strLine = lstViews[nIdx];
 			nStart = strLine.lastIndexOf("FROM ");
 			strKeys = strLine.right(strLine.size()-nStart).trimmed();		
 			nStart = strKeys.indexOf(" ");
 			strKeys = strKeys.mid(nStart,-1).trimmed();	
 			nEnd = strKeys.indexOf(" ");
 			nEnd2 = strKeys.indexOf("\n");
 			nStart = 0;
 			if(nEnd2 < nEnd){
 				strViewTable = strKeys.mid(nStart,nEnd2-nStart).trimmed();
 			}
 			else{ strViewTable = strKeys.mid(nStart,nEnd-nStart).trimmed();
 			}
			nLengthViewtablename = strViewTable.length();
			
			// extract view_name: FROM ViewName
			strKeys = strLine.mid(11,-1).trimmed();	
			nEnd = strKeys.indexOf(" ");
			nStart = 0;
			strViewName = strKeys.mid(nStart,nEnd-nStart).trimmed();

			if(nLengthTablename == nLengthViewtablename) // because common problem: F_Fakt_Plan_Log vs F_Fakt_Plan
			{
				// print
				buffer_cpp.append("\r\n\r\n\tdboList[\""+strViewName+"\"] = \""+lstViews[nIdx]+"\";");
			}
			
		}
		nIdx++;
	}
	// END: DefineViews
	buffer_cpp.append("\r\n}\r\n\r\n");
		
	// DefineTriggers
	buffer_cpp.append("void DbCreate_"+strTableName.toUpper()+"::DefineTriggers(SQLDBObjectCreate &dboList) \r\n");
	buffer_cpp.append("{\r\n");
	buffer_cpp.append("\tif(m_DbType==DBTYPE_FIREBIRD)\r\n");
	buffer_cpp.append("\t{\r\n");
	
	nIdx = 0;
	QString strTriggerName;
	while(nIdx < lstTriggers.size())
	{	
		//B.T. advanced algorithm:
		strTriggerName				= lstTriggers[nIdx].split(" ").at(2); //CREATE TRIGGER TR_F_BATCH_JOBS_DELETE FOR F_BATCH_JOBS (3rd string)
		QString strTriggerTableName = lstTriggers[nIdx].split(" ").at(4);	 //CREATE TRIGGER TR_F_BATCH_JOBS_DELETE FOR F_BATCH_JOBS  (5th string)	
		
		if (strTriggerTableName.toLower()==strTableName.toLower()) //to skip lower/upper case problems
		{
			if (strTriggerName.indexOf("F_VERSICHERUNGSKATEGORIEN_DELETE")>=0) //FB can not take long names of views, triggers, etc...make exception
			{
				strTriggerName="TR_F_VERSICHERUNG_DELETE";
				lstTriggers[nIdx] = lstTriggers[nIdx].replace( QString("F_VERSICHERUNGSKATEGORIEN_DELETE"), QString("F_VERSICHERUNG_DELETE"));
			}

			lstTriggers[nIdx]			= lstTriggers[nIdx].replace( QString("BEFORE INSERT OR UPDATE\\"), QString("BEFORE INSERT OR UPDATE \\") ); //add space after
			buffer_cpp.append("\r\n\t\tdboList[\""+strTriggerName+"\"] = \""+lstTriggers[nIdx]+"\";");	
		}

		/*
		// extract trigger name
		strTriggerName = lstTriggers[nIdx];
		strTriggerName = strTriggerName.mid(15,-1).trimmed();
		nPos = strTriggerName.indexOf(" ");
		strTriggerName = strTriggerName.left(nPos).trimmed(); //TR_FF_Bausteine
		strTriggerName = strTriggerName.mid(3,-1);

		nStart = lstTriggers.at(nIdx).indexOf(strTableName+" ");
		nPos = lstTriggers.at(nIdx).indexOf("TRIGGER");

		if( nStart >= 0 && nStart>nPos )
		{
			
			// znaci iza BEFORE INSERT OR UPDATE stavi space pa sve ostalo
			if( lstTriggers.at(nIdx).indexOf("BEFORE INSERT OR UPDATE\\") >= 0){
				lstTriggers[nIdx] = lstTriggers[nIdx].replace( QString("BEFORE INSERT OR UPDATE\\"), QString("BEFORE INSERT OR UPDATE \\") );
			}
			
			buffer_cpp.append("\r\n\t\tdboList[\"TRG_"+strTriggerName+"\"] = \""+lstTriggers[nIdx]+"\";");	
		}
		*/

	  nIdx++;
	}	
	buffer_cpp.append("\r\n\t}\r\n");
	buffer_cpp.append("}\r\n");
	
	
	

	// Write to files, clear buffers
	QFile file_h(strOutput_h);
	if (!file_h.open(QIODevice::WriteOnly)) return false;	
	file_h.write(buffer_h);
	file_h.close();
	buffer_h.clear();

	QFile file_cpp(strOutput_cpp);
	if (!file_cpp.open(QIODevice::WriteOnly)) return false;	
	file_cpp.write(buffer_cpp);
	file_cpp.close();
	buffer_cpp.clear();
	
	return true;
}


bool DbActualizator::OmnisLoadFile(QString strInputFile, QByteArray &buffer)
{
	QFile file(strInputFile);	
	if (file.open(QIODevice::ReadOnly))
	{
		buffer = file.readAll();
		file.close();
		return true;
	}
	else{
		return false;
	}
}

void DbActualizator::OmnisParse(QByteArray &buffer, QStringList &lstFile, QString strFlag)
{
	QString strFile = buffer;
	QString* pFirstline = NULL;
	//QString* pLstRow =  NULL;
	int nPos=0;
	int nPos2=0;
	int nIdx=0;


	// lstConvertLongs needed by tables
	QStringList  lstConvertLongs = QStringList()
		<< "EFP_BESCHREIB"	
		<< "EFG_TEXT"	
		<< "EPB_TEXT"
		<< "EPP_BESCHREIB"
		<< "EAP_TEXT"
		<< "EPZ_TEXT"
		<< "EVK_TEXT"
		<< "FBS_TEXT1"
		<< "FBS_TEXT2"
		<< "FRP_TEXT1"
		<< "FRP_TEXT2"
		<< "FP_INFO"
		<< "PR_TEXT"
		<< "EAL_BEMERKUNGEN";

	QStringList  lstConvertBackToBinary = QStringList()
		<< "EUS_COLORS_TS"	
		<< "EUS_LOC_LIST"	
		<< "EUS_OL_EMAIL_FOLDERS_LIST"
		<< "EUS_PR_WINLIST"
		<< "EUS_REPSETUP_ESR"
		<< "EUS_REPSETUP_FAKT"
		<< "EUS_REPSETUP_HOCH"
		<< "EUS_REPSETUP_QUER"
		<< "EUS_STEMP_TAB"
		<< "EUS_TAPI_LIST"
		<< "EUS_TIMESHEET_US"
		<< "EUS_USER_MENU"
		<< "EUS_WRAPP_TAB";

	//can be NULL and must be integer
	QStringList  lstFKFields = QStringList()
		<< "BJ_TASK"	
		<< "BJ_RESULTS"
		<< "DFF_FAK_PIC"
		<< "DFF_PIC_HEAD"
		<< "PL_PLIST"
		<< "PL_SEL_KRIT"
		<< "PL_LST_SUBLISTS"
		<< "EDW_WT_LIST"
		<< "EDW_PT_LIST"
		<< "DF_HEADER"
		<< "DF_FOOTER"
		<< "DF_FOOTER"
		<< "EKB_PR_SEQUENCE"
		<< "EBM_FP_SEQUENCE"
		<< "EBM_PR_SEQUENCE"
		<< "FPF_EFK_SEQUENCE"
		<< "FPB_PR_SEQUENCE"
		<< "FPB_EAL_SEQUENCE"
		<< "FB_FP_SEQUENCE"
		<< "FB_PR_SEQUENCE";


	
	if(strFlag.toLower() == "tables")
	{	
		lstFile  = strFile.split(";"); 
		QString strTemp;
		QString strTableName;
		nIdx = 0;
		nPos = 0;
		

		

		pFirstline = &lstFile[0];
		//pLstRow = &lstFile[nIdx];

		// remove first line "--@"
		if( pFirstline->indexOf("--@")>= 0)//if( lstFile.at(0).indexOf("--@") >= 0 )
		{
			nPos = pFirstline->indexOf("CREATE TABLE");
			if(nPos>0)
				lstFile[0] = pFirstline->mid(nPos,-1);
			else
				lstFile.removeAt(0);
		}
		while(nIdx < lstFile.size())
		{	
			// trim lines
			lstFile[nIdx] = lstFile[nIdx].trimmed();

			// delete unnecessary lines
			if( lstFile.at(nIdx).size() < 1 || lstFile.at(nIdx).indexOf("DROP TABLE") >= 0 ) {
				lstFile.removeAt(nIdx);
			}
			else
			{	
				if( lstFile.at(nIdx).indexOf("CREATE TABLE") >= 0)
				{
					for (int i=0;i<lstConvertLongs.size();++i)
					{
						nPos=lstFile.at(nIdx).indexOf(lstConvertLongs[i]+" LONG");			// EFP_BESCHREIB LONG
						nPos2=lstFile.at(nIdx).indexOf(lstConvertLongs[i]+" NUMBER(9)"); 
						
						if(nPos>=0){ 
							lstFile[nIdx] = lstFile[nIdx].replace(QString(lstConvertLongs[i]+" LONG"),QString(lstConvertLongs[i]+" VARCHAR(8000)"));
						}
						if(nPos2>=0){
							lstFile[nIdx] = lstFile[nIdx].replace(QString(lstConvertLongs[i]+" NUMBER(9)"),QString(lstConvertLongs[i]+" VARCHAR(8000)"));
						}
					}


					for (int i=0;i<lstConvertBackToBinary.size();++i)
					{
						nPos=lstFile.at(nIdx).indexOf(lstConvertBackToBinary[i]+" LONG");			// EFP_BESCHREIB LONG
						nPos2=lstFile.at(nIdx).indexOf(lstConvertBackToBinary[i]+" NUMBER(9)"); 

						if(nPos>=0){ 
							lstFile[nIdx] = lstFile[nIdx].replace(QString(lstConvertBackToBinary[i]+" LONG"),QString(lstConvertBackToBinary[i]+" LONGVARBINARY"));
						}
						if(nPos2>=0){
							lstFile[nIdx] = lstFile[nIdx].replace(QString(lstConvertBackToBinary[i]+" NUMBER(9)"),QString(lstConvertBackToBinary[i]+" LONGVARBINARY"));
						}
					}

					
					// B.T. : 01.11.2012: keep all constraints later on, but remove from all at start, as import is blocked...
					// B.T. : all NOT NULL constraints remove!!!
					// replace "_SEQUENCE NUMBER(15)" with "_SEQUENCE INTEGER NOT NULL"
					if(lstFile.at(nIdx).indexOf(" NOT NULL") >= 0){
						lstFile[nIdx] = lstFile[nIdx].replace( QString(" NOT NULL"), QString("") );
					}

					// B.T. commented: let there be decimal..it will be mess if we change this now
					// replace "_SEQUENCE NUMBER(15)" with "_SEQUENCE INTEGER NOT NULL"
					if(lstFile.at(nIdx).indexOf("_SEQUENCE NUMBER(15)") >= 0){
						lstFile[nIdx] = lstFile[nIdx].replace( QString("_SEQUENCE NUMBER(15)"), QString("_SEQUENCE INTEGER") );
					}
					// replace "_SEQUENCE NUMBER(10)" with "_SEQUENCE INTEGER"
					if(lstFile.at(nIdx).indexOf("_SEQUENCE NUMBER(10)") >= 0){
						lstFile[nIdx] = lstFile[nIdx].replace( QString("_SEQUENCE NUMBER(10)"), QString("_SEQUENCE INTEGER") );
					}
					
					// replace "NUMBER(" with "DECIMAL("
					if( lstFile.at(nIdx).indexOf("NUMBER(") >= 0 ){
						lstFile[nIdx] = lstFile[nIdx].replace( QString("NUMBER("), QString("DECIMAL(") );
					}
					// replace "NUMBER," with "DECIMAL,"
					if( lstFile.at(nIdx).indexOf("NUMBER,") >= 0 ){
						lstFile[nIdx] = lstFile[nIdx].replace( QString("NUMBER,"), QString("DECIMAL,") );
					}
					// replace "VARCHAR2" with "VARCHAR"
					if( lstFile.at(nIdx).indexOf("VARCHAR2") >= 0 ){
						lstFile[nIdx] = lstFile[nIdx].replace( QString("VARCHAR2"), QString("VARCHAR") );
					}
					// replace "LONG RAW" with "BLOB"
					if( lstFile.at(nIdx).indexOf("LONG RAW") >= 0 ){
						lstFile[nIdx] = lstFile[nIdx].replace( QString("LONG RAW"), QString(" BLOB ") );
					}
					// replace " LONG " with " BLOB sub_type TEXT "
					if( lstFile.at(nIdx).indexOf(" LONG ") >= 0 ){
						lstFile[nIdx] = lstFile[nIdx].replace( QString(" LONG "), QString(" BLOB sub_type TEXT ") );	
					}
					// replace " DATE " with " DATETIME " 
					if( lstFile.at(nIdx).indexOf(" DATE ") >= 0 ){
						lstFile[nIdx] = lstFile[nIdx].replace( QString(" DATE "), QString(" DATETIME ") );	
					}


					//B.T.-------------MB: request: NO NULLS in DB-------------------------------------------------------------
					//B.T.-----------------------------------------------------------------------------------------------------
					//re-arrange all columns to have NULL's!!!1
					// calc TableName
					nPos = lstFile[nIdx].indexOf("CREATE TABLE")+12;
					strTemp = lstFile.at(nIdx).mid(nPos,-1).trimmed(); 
					nPos = strTemp.indexOf(" ");
					strTableName = strTemp.mid(0,nPos).trimmed().toUpper();


					//get all column names and detect types:
					
					QString strInput=lstFile.at(nIdx);
					strInput=strInput.replace("(15,","(15:");

					QStringList lstCols=strInput.split(",");
					QStringList lstNumericCols;
					QStringList lstStringCols;
					int nSizeCols=lstCols.size();
					for (int h=0;h<nSizeCols;h++)
					{
						QString strColFull=lstCols.at(h).toUpper();
						QString strCreateTable;
						if (strColFull.contains("CREATE TABLE"))
						{
							strCreateTable=strColFull.left(strColFull.indexOf("(")+1);
							QString str2=strColFull.mid(strColFull.indexOf("(")+1);

							strColFull=str2;
						}

						QString strColName= strColFull.left(strColFull.indexOf(" ")).simplified();


						//set NOT NULL
						if (!lstFKFields.contains(strColName) && !strColFull.contains("NOT NULL") && !strColFull.contains(" DATETIME ") &&  !strColFull.contains(" LONGVARBINARY") &&  !strColFull.contains(" BLOB"))
						{
							if (h!=nSizeCols-1)
							{
								strColFull.append(" NOT NULL");
							}
							else //if last then we have: DEF_AZAB_BIS DATE);
							{
								int nPos=strColFull.lastIndexOf(")");
								if (nPos>0 && nPos==strColFull.size()-1)
								{
									strColFull.chop(1);
									strColFull.append(" NOT NULL)");
								}
							}
						}

						//skip handling FK into triggers: can not be 0..leave it NULL
						if (lstFKFields.contains(strColName))
							strColFull=strColFull.replace("DECIMAL(9)","INTEGER");
						else
						{
							if (!strColFull.contains("_SEQUENCE"))
								if (strColFull.contains(" DECIMAL") || strColFull.contains(" INTEGER") || strColFull.contains("DECIMAL("))
								{
									lstNumericCols.append(strColName);
								}
								else if (strColFull.contains("VARCHAR(")) // || strColFull.contains(" BLOB "))
								{
									lstStringCols.append(strColName);
								}
						}
						

						if (h==0)
							lstCols[h]=strCreateTable+strColFull;
						else
							lstCols[h]=strColFull;
					}

					QString strTableCreate=lstCols.join(",");
					strTableCreate = strTableCreate.replace("(15:","(15,");
					lstFile[nIdx]=strTableCreate;
					if (lstNumericCols.size()!=0 || lstStringCols.size()!=0)
					{
						m_hshNumCols[strTableName]=lstNumericCols;
						m_hshStrCols[strTableName]=lstStringCols;
					}

					//B.T.-------------MB: request: NO NULLS in DB-------------------------------------------------------------
					//B.T.-----------------------------------------------------------------------------------------------------



					// add constraint for primary key, primary key is detected by substring "_SEQUENCE":  - constraint TableName_PK primary key (SequenceID) 
					if( lstFile.at(nIdx).indexOf("_SEQUENCE") >= 0)
					{
						// calculate "SequenceID"
						QString strSeqName = lstFile[nIdx];
						
						// cutting piece
						int nEndPos = strSeqName.indexOf("_SEQUENCE");
						QString strSeqPart = strSeqName.mid(nEndPos-10,10);

						// this
						int nBeginPos_1 = strSeqPart.lastIndexOf("(");
						int nBeginPos_2 = strSeqPart.lastIndexOf(",");
						nEndPos = strSeqPart.length();
						int nBeginPos;
						
						if(nBeginPos_2 <= 0) { // sequence name on first line after create command
							nBeginPos = nBeginPos_1;
						}
						else { // sequence name anywhere after ,
							if(nBeginPos_2 > nBeginPos_1){  // , > (
								nBeginPos = nBeginPos_2;
							}
							else{
								nBeginPos = nBeginPos_1;	
							} 
						}
						strSeqName = strSeqPart.mid(nBeginPos+1,nEndPos-nBeginPos).trimmed();
						
						// delete ')' from last line & append ','
						nPos = lstFile[nIdx].lastIndexOf(")");
						if(nPos>0){
							lstFile[nIdx] = lstFile[nIdx].left(nPos);	
							lstFile[nIdx].append(",");
						}
					

						
						// calc SequenceID -> hardcoded (15)
						lstFile[nIdx].append("\r\nconstraint "+strTableName+"_PK primary key ("+strSeqName+"_SEQUENCE))"); // _SEQUENCE
					}

					// add '\' for each line
					nPos = lstFile[nIdx].indexOf("\r\n");
					if(nPos > 0){ // Multiline "CREATE TABLE" command
						lstFile[nIdx] = lstFile[nIdx].replace( QString("\r\n"), QString("\\\r\n") );
					}
					else{ // single line "CREATE TABLE" command
						nPos = lstFile.at(nIdx).indexOf("(");
						lstFile[nIdx].insert(nPos+1,'\\');
					}
				}
			nIdx ++;
			}
		}
	}
	else if(strFlag.toLower() == "temptables")
	{	
		lstFile  = strFile.split("!!"); 
		QString strTemp;
		QString strTableName;
		nIdx = 0;
		nPos = 0;

		while(nIdx < lstFile.size())
		{	
			// trim lines
			lstFile[nIdx] = lstFile[nIdx].trimmed();

			// delete unnecessary lines
				if( lstFile.at(nIdx).indexOf("CREATE GLOBAL TEMPORARY TABLE") >= 0)
				{
					//replace all quotes
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\""), QString(""));

						// add '\' for each line
					nPos = lstFile[nIdx].indexOf("\r\n");
					if(nPos > 0){ // Multiline "CREATE TABLE" command
						lstFile[nIdx] = lstFile[nIdx].replace( QString("\r\n"), QString("\\\r\n") );
					}
					else{ // single line "CREATE TABLE" command
						nPos = lstFile.at(nIdx).indexOf("(");
						lstFile[nIdx].insert(nPos+1,'\\');
					}

					nIdx ++;
				}
				else
					lstFile.removeAt(nIdx);
				
				
			
		}
	}
	else if(strFlag.toLower() == "cons")
	{
		lstFile = strFile.split(";");
		nIdx = 0;
		nPos = 0;

		// remove first line "--@"
		pFirstline = &lstFile[0];
		if( pFirstline->indexOf("--@") >= 0 )
		{
			nPos = pFirstline->indexOf("ALTER TABLE");
			if(nPos>0)
				lstFile[0] = pFirstline->mid(nPos,-1);
			else
				lstFile.removeAt(0);
		}
		while(nIdx < lstFile.size())
		{	
			// trim lines
			lstFile[nIdx] = lstFile[nIdx].trimmed().toUpper();

			int nPos1= lstFile.at(nIdx).indexOf("ADD FOREIGN KEY (",0,Qt::CaseInsensitive);
			int nPos2= lstFile.at(nIdx).indexOf(")", nPos1);

			QString strRefName;
			if ((nPos1!=-1 && nPos2 != -1) && (nPos2>nPos1))
			{
				nPos1= nPos1 +QString("ADD FOREIGN KEY (").length();
				strRefName=lstFile.at(nIdx).mid(nPos1,nPos2-nPos1).trimmed();

				bool bSkip=false;
				for (int i=0;i<lstConvertBackToBinary.size();++i)
				{
					if (strRefName.indexOf(lstConvertBackToBinary.at(i))==0) //start of string
					{
						lstFile.removeAt(nIdx);
						bSkip=true;
						break;
					}
				}

				if (bSkip)
					continue;
			}


			// remove empty lines
			if( lstFile.at(nIdx).size() < 1 || lstFile.at(nIdx).indexOf("ADD PRIMARY KEY") >= 0){
				lstFile.removeAt(nIdx);
			}
			else{
				
				//replace "LONGR_FIELDS" s "ZS_BINARY_FIELDS"
				if(lstFile.at(nIdx).indexOf("LONGR_FIELDS") > 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("LONGR_FIELDS"), QString("ZS_BINARY_FIELDS") );
				}
				if(lstFile.at(nIdx).indexOf(" (RID)") > 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString(" (RID)"), QString(" (RID_BIN)") );
				}
		nIdx++;
			}
		}
	}
	else if(strFlag.toLower() == "indexes")  
	{
		lstFile = strFile.split(";");
		nIdx = 0;
		nPos = 0;

		// remove first line "--@"
		pFirstline = &lstFile[0];
		if( pFirstline->indexOf("--@") >= 0 ){
			nPos = pFirstline->indexOf("CREATE"); // CREATE INDEX, CREATE UNIQUE INDEX
			if(nPos>0)
				lstFile[0] = pFirstline->mid(nPos,-1);
			else
				lstFile.removeAt(0);
		}

		while(nIdx < lstFile.size())
		{	
			// trim lines
			lstFile[nIdx] = lstFile[nIdx].trimmed().toUpper();

			// remove empty lines
			if( lstFile.at(nIdx).size() < 1){
				lstFile.removeAt(nIdx);
			}
			else
			{
				if(lstFile.at(nIdx).indexOf("CREATE ") >= 0)
				{
					QString strStatement=lstFile.at(nIdx);
					/*
					if (strStatement.contains("EF_CI_MWST_DAT"))
					{
						qDebug()<<"dd";
					}*/

					// replace " (rpad" WITH " COMPUTED BY (rpad"
					if(strStatement.indexOf(" (RPAD") > 0)
					{
						QStringList lstFlds;

						int nPosFld=0;
						while (strStatement.indexOf("RPAD(",nPosFld)>0)
						{
							nPosFld=strStatement.indexOf("RPAD(",nPosFld);
							if (nPosFld>0)
							{
								nPosFld=nPosFld+QString("RPAD(").length();
								int nPosFldEnd=strStatement.indexOf(",",nPosFld);
								QString strFld=strStatement.mid(nPosFld,nPosFldEnd-nPosFld).trimmed();
								if (!strFld.contains("_CI_"))
									lstFlds.append(strFld);
							}
							else
								break;
						}

						nPosFld=0;
						while (strStatement.indexOf("LPAD(",nPosFld)>0)
						{
							nPosFld=strStatement.indexOf("LPAD(",nPosFld);
							if (nPosFld>0)
							{
								nPosFld=nPosFld+QString("LPAD(").length();
								int nPosFldEnd=strStatement.indexOf(",",nPosFld);
								QString strFld=strStatement.mid(nPosFld,nPosFldEnd-nPosFld).trimmed();
								if (!strFld.contains("_CI_"))
									lstFlds.append(strFld);
							}
							else
								break;
						}

						nPosFld=0;
						while (strStatement.indexOf("TO_CHAR(",nPosFld)>0)
						{
							nPosFld=strStatement.indexOf("TO_CHAR(",nPosFld);
							if (nPosFld>0)
							{
								nPosFld=nPosFld+QString("TO_CHAR(").length();
								int nPosFldEnd=strStatement.indexOf(",",nPosFld);
								QString strFld=strStatement.mid(nPosFld,nPosFldEnd-nPosFld).trimmed();
								if (!strFld.contains("_CI_"))
									lstFlds.append(strFld);
							}
							else
								break;
						}

						strStatement=strStatement.left(strStatement.indexOf(" (RPAD"))+" (";
						int nSizeFlds=lstFlds.size();
						for (int z=0;z<nSizeFlds;z++)
						{
							strStatement.append(lstFlds.at(z)+",");
						}
						strStatement.chop(1);
						strStatement+=")";
						lstFile[nIdx]=strStatement;
					}
					/*
					// replace "to_char" WITH "datetostr"
					if(lstFile.at(nIdx).indexOf("to_char") > 0)
					{
						lstFile[nIdx] = lstFile[nIdx].replace( QString("to_char"), QString("datetostr") );
					}
					// replace "'YYYYMMDDHH24MI'" WITH "'%d%m%Y%H%M'"
					if(lstFile.at(nIdx).indexOf("'YYYYMMDDHH24MI'") > 0)
					{
						lstFile[nIdx] = lstFile[nIdx].replace( QString("'YYYYMMDDHH24MI'"), QString("'%d%m%Y%H%M'") );
					}
					*/

					// Remove all XX_SEQUENCE fields from computed indexes
					//B.T. 07/2012/-> return SEQUENCE fields in all indexes!!!!
					//B.T. 20.08/2012/-> remove SEQUENCE fields in all indexes!!!!

					if(lstFile.at(nIdx).indexOf("_SEQUENCE") > 0) // && lstFile.at(nIdx).indexOf("(rpad" ) > 0)
					{
						// (rpad(EF_CI_MWST_DAT,1,' ')||lpad(EF_MWST,4,' ')||to_char(EF_FDATUM,'YYYYMMDDHH24MI'),EF_SEQUENCE);
						nPos = lstFile[nIdx].lastIndexOf("_SEQUENCE");
						nPos2 = lstFile[nIdx].lastIndexOf(',',nPos);
						
						if(nPos>0 && nPos2>0)
						{
							lstFile[nIdx] = lstFile[nIdx].left(nPos2).trimmed();
							lstFile[nIdx].append(")");
						}
					}	


				}	
			 nIdx++;
			}
		}
	}
	else if(strFlag.toLower() == "views")
	{
		lstFile = strFile.split("CREATE VIEW");
		nIdx = 0;
		nPos = 0;

		while(nIdx < lstFile.size())
		{
			// trim
			lstFile[nIdx] = lstFile[nIdx].trimmed().toUpper();;
			
			// remove empty lines
			if( lstFile.at(nIdx).size() < 1){
				lstFile.removeAt(nIdx);
			}
			else
			{
				lstFile[nIdx].prepend("CREATE VIEW ");	


				//---------------------------------B.T. remove all decode-------------------------------------
				QString strView = lstFile.at(nIdx); 

				int nPos1= strView.indexOf("DECODE(",0,Qt::CaseInsensitive);
				while(nPos1>0)
				{
					int nPos2 = strView.indexOf("\"",nPos1,Qt::CaseInsensitive);
					int nPos3 = strView.indexOf("\"",nPos2+1,Qt::CaseInsensitive);

					if (nPos3!=-1 && nPos2 != -1)
						if (nPos3>nPos2)
						{
							//skip remove decode for date fields :
							//B.T.:21.08.2012 decide: keep all date values as is..who bloody cares

							/*
							if(strView.mid(nPos1,nPos2-nPos1).indexOf("as date",Qt::CaseInsensitive)<0)
							{
							*/
								QString strNewCol=strView.mid(nPos2+1,nPos3-nPos2);
								strNewCol = strNewCol.replace(QString("\""),QString(""));
								strView.remove(nPos1,nPos3-nPos1+1);
								strView.insert(nPos1,strNewCol);
							/*
							}
							else //do not skip: just change to this: (case when FLD is null then '' else FLD END) "FLD"
							{
							
								QString strNewCol=strView.mid(nPos2+1,nPos3-nPos2);
								strNewCol = strNewCol.replace(QString("\""),QString(""));
								strNewCol = "(case when "+strNewCol+" is null then '' else " + strNewCol+ " end) \""+ strNewCol+"\"";
								strView.remove(nPos1,nPos3-nPos1+1);
								strView.insert(nPos1,strNewCol);
							//}
							*/
						}

						nPos1= strView.indexOf("DECODE(",nPos1+1,Qt::CaseInsensitive);
				}
				

				lstFile[nIdx] = strView;

				//---------------------------------B.T. remove all decode-------------------------------------

				//intercept decode:
				if( lstFile.at(nIdx).indexOf("DECODE(") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\r"), QString(" ") );
				}

				// replace newline with space
				if( lstFile.at(nIdx).indexOf("\r") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\r"), QString(" ") );
				}

				if( lstFile.at(nIdx).indexOf("\n") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\n"), QString(" ") );
				}
				// replace double space with single space
				if( lstFile.at(nIdx).indexOf("  ") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("  "), QString(" ") );
				}
				// replace " with \"
				if( lstFile.at(nIdx).indexOf("\"") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\""), QString("\\\"") );
				}
				// insert space for the word ASSELECT ?
				if(lstFile.at(nIdx).indexOf("AS\rSELECT") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("AS\rSELECT"), QString("AS SELECT") );
				}
				/*
				// insert newline before FROM
				if(lstFile.at(nIdx).indexOf("FROM ") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("FROM "), QString("\\\r\n\t\t\t\t\t\t\tFROM ") );
				}
				*/

				// insert ,\ and \n after [FROM TableName] and break joins in 2 lines
				if(lstFile.at(nIdx).indexOf("FROM ") >= 0)
				{
					int nBeginPos = lstFile[nIdx].indexOf("FROM ");
					nPos = lstFile[nIdx].indexOf(" ",nBeginPos+5);
					lstFile[nIdx].insert(nPos," \\\r\n\t\t\t\t\t\t\t");
					lstFile[nIdx] = lstFile[nIdx].trimmed();
					
					nBeginPos = lstFile[nIdx].lastIndexOf("LEFT OUTER JOIN ");
					lstFile[nIdx].insert(nBeginPos,"\\\r\n\t\t\t\t\t\t\t");
				}
				// insert slash at end for each command
				if(lstFile.at(nIdx).indexOf("\",") >= 0)
				{
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\","), QString("\",\\\r\n\t\t\t\t\t\t\t") );
				}
				
				// remove last 2 ',' (before and after 'FROM' at the end
				//nPos  = lstFile.at(nIdx).lastIndexOf("FROM"); 
				//nPos2 = lstFile.at(nIdx).lastIndexOf(",\\");
				//if(nPos>0)
				//{
					//lstFile[nIdx].remove(nPos,1);
				//}



 
			 nIdx++;
			}
		}	
	}
	else if(strFlag.toLower() == "procedures")
	{
		lstFile = strFile.split("Recreate procedure",QString::SkipEmptyParts,Qt::CaseInsensitive);
		nIdx = 0;
		nPos = 0;

		while(nIdx < lstFile.size())
		{
			// trim
			lstFile[nIdx] = lstFile[nIdx].toUpper();

			// remove empty lines
			if( lstFile.at(nIdx).size() < 1){
				lstFile.removeAt(nIdx);
			}
			else
			{
				lstFile[nIdx].replace("SET TERM !!;","");
				lstFile[nIdx].replace("SET TERM ;!!","");
				lstFile[nIdx].replace("!!","");

				if (lstFile[nIdx].contains("begin",Qt::CaseInsensitive))
				{
					lstFile[nIdx].prepend("RECREATE PROCEDURE ");	

					// replace newline with space
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\r\n\r\n\r\n"), QString(" ") );
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\r\n"), QString(" \\\r\n") );

					lstFile[nIdx] = lstFile[nIdx].replace( "ASDECLARE", "AS DECLARE"); //if merged accidentally
					lstFile[nIdx] = lstFile[nIdx].replace( "AS\\", "AS \\"); //if merged accidentally
				}

				nIdx++;
			}
		}	
	}
	else if(strFlag.toLower() == "triggers")
	{
		lstFile = strFile.split("CREATE OR REPLACE TRIGGER");
		nIdx = 0;
		nPos = 0;
		
		// remove first line "--@"
		pFirstline = &lstFile[0];
		if( pFirstline->indexOf("--@") >= 0 ){
			nPos = pFirstline->indexOf("CREATE OR REPLACE TRIGGER");
			if(nPos>0)
				lstFile[0] = lstFile[0].mid(nPos,-1);
			else
				lstFile.removeAt(0);
		}

		while(nIdx < lstFile.size())
		{
			// trim
			lstFile[nIdx] = lstFile[nIdx].trimmed().toUpper();;

			// remove empty lines
			if( lstFile.at(nIdx).size() < 1){
				lstFile.removeAt(nIdx);
			}
			else
			{
				lstFile[nIdx].prepend("CREATE OR REPLACE TRIGGER ");	

				//B.T. 31.10.2012: Do not allow NULL's to enter DB for strings, numbers and blob's:
				//---------------------------------------------------------------------------------
				QString strTrigger = lstFile[nIdx].toUpper();
				if(strTrigger.contains("BEFORE INSERT OR UPDATE")) //only replace whole trigger when before insert & update
				{
					QString strTriggerName = strTrigger.split(" ").at(4).trimmed().toUpper();
					QString strTriggerTableName = strTrigger.split(" ").at(10).trimmed().toUpper();	 
					strTrigger="CREATE TRIGGER "+strTriggerName+" FOR " +strTriggerTableName+" BEFORE INSERT OR UPDATE AS BEGIN \\\n";
					QStringList lstNumCols=m_hshNumCols.value(strTriggerTableName);
					QStringList lstStrCols=m_hshStrCols.value(strTriggerTableName);
					int nSizeNum=lstNumCols.size();
					int nSizeStr=lstStrCols.size();
					if (nSizeNum!=0 || nSizeStr!=0)
					{
						for(int k=0;k<nSizeNum;k++)
						{
							strTrigger += "IF(NEW."+lstNumCols.at(k)+" IS NULL) THEN NEW."+lstNumCols.at(k)+"=0;\\\n";
						}
				
						for(int k=0;k<nSizeStr;k++)
						{
							strTrigger += "IF(NEW."+lstStrCols.at(k)+" IS NULL) THEN NEW."+lstStrCols.at(k)+"='';\\\n";
						}
						strTrigger.append("END;");
						lstFile[nIdx]=strTrigger;
						nIdx++;
						continue;
					}
				}



				//B.T. 13.08.2012: To skip problems with indexes, remove empty space from database instead of NULL, and for datetime values, leave NULL as is and in view replace decode to work properly!!1
				//-----------------------------------------------------------------------
				if( lstFile.at(nIdx).indexOf("' ';") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("' ';"), QString("'';") );
				}

				while( lstFile.at(nIdx).indexOf("to_date(") >= 0)
				{
					QString strTrigger = lstFile.at(nIdx);
					int nPos = strTrigger.indexOf("to_date(");

					// this
					int nBeginPos = strTrigger.lastIndexOf("IF :NEW.",nPos);
					int nEndPos = strTrigger.indexOf("END IF;",nPos);

					if (nBeginPos>=0 && nEndPos>=0)
					{
						nEndPos=nEndPos+QString("END IF;").length();
						strTrigger=strTrigger.left(nBeginPos)+strTrigger.mid(nEndPos);
						lstFile[nIdx] =strTrigger;
					}
				}
				//-----------------------------------------------------------------------


				// replace "CREATE OR REPLACE " with "RECREATE"
				if( lstFile.at(nIdx).indexOf("CREATE OR REPLACE") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("CREATE OR REPLACE"), QString("CREATE") );
				}

				// replace "BEGIN" with "AS BEGIN" only first occurence
				if( lstFile.at(nIdx).indexOf("BEGIN") >= 0)
				{
					int nStart = lstFile.at(nIdx).indexOf("BEGIN");
					int nLength = QString("BEGIN").length();
					lstFile[nIdx] = lstFile[nIdx].replace( nStart, nLength,QString(" AS BEGIN ") );
				}
				// replace ":=" with "="
				if( lstFile.at(nIdx).indexOf(":=") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString(":="), QString("=") );
				}
				// replace ":NEW." with "NEW."
				if( lstFile.at(nIdx).indexOf(":NEW.") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString(":NEW."), QString("NEW.") );
				}
				// replace "IF NEW" with "IF (NEW"
				if( lstFile.at(nIdx).indexOf("IF NEW") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("IF NEW"), QString("IF (NEW") );
				}
				// replace "IS NULL THEN" with "IS NULL) THEN"
				if( lstFile.at(nIdx).indexOf("IS NULL THEN") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("IS NULL THEN"), QString("IS NULL) THEN") );
				}
				// replace "END IF;" with "" 
				if( lstFile.at(nIdx).indexOf("END IF;") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("END IF;"), QString("") );
				}
				// replace "/" with "" 
				if( lstFile.at(nIdx).indexOf("/") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("/"), QString("") );
				}
				// replace "SHOW ERRORS" with "" 
				if( lstFile.at(nIdx).indexOf("SHOW ERRORS") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("SHOW ERRORS"), QString("") );
				}
				// replace "to_date('01-JAN-1960','DD-MON-YYYY')" with "cast ('01' || '-january-' || '1960' as date)" 
				if( lstFile.at(nIdx).indexOf("to_date(\'01-JAN-1960\',\'DD-MON-YYYY\')") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("to_date(\'01-JAN-1960\',\'DD-MON-YYYY\')"), QString("cast (\'01\' || \'-january-\' || \'1960\' as date)") );
				}
				// replace " BEFORE INSERT OR UPDATE ON " with " FOR " 
				if( lstFile.at(nIdx).indexOf(" BEFORE INSERT OR UPDATE ON ") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString(" BEFORE INSERT OR UPDATE ON "), QString(" FOR ") );
				}
				// replace " AFTER INSERT OR UPDATE OR DELETE" with space after:
				if( lstFile.at(nIdx).indexOf("AFTER INSERT OR UPDATE OR DELETE") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("AFTER INSERT OR UPDATE OR DELETE"), QString("AFTER INSERT OR UPDATE OR DELETE ") );
				}

				// replace " FOR EACH ROW" with " BEFORE INSERT OR UPDATE" 
				if( lstFile.at(nIdx).indexOf(" FOR EACH ROW") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString(" FOR EACH ROW"), QString(" BEFORE INSERT OR UPDATE") );
				}

				// remove empty lines
				TriggerRemoveEmptyLines(lstFile[nIdx]);
				
				// add "\" 
				if( lstFile.at(nIdx).indexOf("\r\n") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\r\n"), QString("\\\r\n") );
				}
			 
			 nIdx++;
			}
		}	
	}
	else if(strFlag.toLower() == "globaltemp_triggers")
	{
		lstFile = strFile.split("CREATE TRIGGER");
		nIdx = 0;
		nPos = 0;

		while(nIdx < lstFile.size())
		{
			// trim
			lstFile[nIdx] = lstFile[nIdx].trimmed().toUpper();;

			// remove empty lines
			if( lstFile.at(nIdx).size() < 1){
				lstFile.removeAt(nIdx);
			}
			else
			{
				lstFile[nIdx].prepend("CREATE TRIGGER ");

				// remove empty lines
				TriggerRemoveEmptyLines(lstFile[nIdx]);

				// add "\" 
				if( lstFile.at(nIdx).indexOf("\r\n") >= 0){
					lstFile[nIdx] = lstFile[nIdx].replace( QString("\r\n"), QString("\\\r\n") );
				}

				nIdx++;
			}
		}	
	}


}

void DbActualizator::TriggerRemoveEmptyLines(QString &strNew) 
{
	int nStart = 0; // search from
	while(true)
	{
		nStart=strNew.indexOf("\r\n", nStart);
		if(nStart < 0)
			return;
		
		//search next chars for space or \r\n
		int nCharPos = nStart+2;
		while(nCharPos<strNew.length() && strNew.at(nCharPos)==' '){
			nCharPos++;
		}
		if(strNew.mid(nCharPos,2)=="\r\n"){ // second \r\n found
			strNew = strNew.remove(nStart+2,nCharPos-nStart);		
		}
		nStart += 2;
	}
}