#include "accessrightsorganizer.h"

#include <QObject>

AccessRightsOrganizer::AccessRightsOrganizer()
{
	//Initialize Access Rights. 
	InitializeAccessRights();
	//Initialize Access Right Sets. 
	InitializeAccessRightSets();
	//Put a list of Access Rights to each Access Right Set. 
	InitializeARSRights();
}

AccessRightsOrganizer::~AccessRightsOrganizer()
{
	qDeleteAll(m_hshAccessRights);
	m_hshAccessRights.clear();
	qDeleteAll(m_hshAccessRightSets);
	m_hshAccessRightSets.clear();
}

void AccessRightsOrganizer::InitializeAccessRights()
{
	DbRecordSet AccessRightRecordSet;

	//********************************************************************
	//**						OPEN_PERSON_FUI							**
	//********************************************************************
	AccessRightRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS));
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Open \"Person\" Windows");				//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, OPEN_PERSON_FUI);					//AR Code.

	m_hshAccessRights.insert(OPEN_PERSON_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					SEE_PERSONAL_RECORDS_DATA					**
	//********************************************************************
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See Personal records data");		//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_PERSONAL_RECORDS_DATA);			//AR Code.

	m_hshAccessRights.insert(SEE_PERSONAL_RECORDS_DATA, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						EDIT_PERSONAL_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit Personal records");			//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_PERSONAL_RECORDS);				//AR Code.

	m_hshAccessRights.insert(EDIT_PERSONAL_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						INSERT_PERSONAL_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Insert Personal records");			//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, INSERT_PERSONAL_RECORDS);				//AR Code.

	m_hshAccessRights.insert(INSERT_PERSONAL_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						DELETE_PERSONAL_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Delete Personal records");			//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, DELETE_PERSONAL_RECORDS);			//AR Code.

	m_hshAccessRights.insert(DELETE_PERSONAL_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						SEE_COMPANY_PERSONS						**
	//********************************************************************
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See only persons of one company");	//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_COMPANY_PERSONS);			//AR Code.

	m_hshAccessRights.insert(SEE_COMPANY_PERSONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				EDIT_INSERT_DELETE_COMPANY_PERSONS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit/insert/delete only persons of one company");	//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_INSERT_DELETE_COMPANY_PERSONS);					//AR Code.

	m_hshAccessRights.insert(EDIT_INSERT_DELETE_COMPANY_PERSONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						SEE_DEPARTMENT_PERSONS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See  only persons of one company");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_DEPARTMENT_PERSONS);								//AR Code.

	m_hshAccessRights.insert(SEE_DEPARTMENT_PERSONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**			EDIT_INSERT_DELETE_DEPARTMENT_PERSONS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit/insert/delete only persons of one company");	//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_INSERT_DELETE_DEPARTMENT_PERSONS);				//AR Code.

	m_hshAccessRights.insert(EDIT_INSERT_DELETE_DEPARTMENT_PERSONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					EMPLOYMENT_DATA_VISIBLE						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Employment Data Visible");							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EMPLOYMENT_DATA_VISIBLE);							//AR Code.

	m_hshAccessRights.insert(EMPLOYMENT_DATA_VISIBLE, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					GENERATE_USER_FROM_PERSON					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Generate User From Person");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, GENERATE_USER_FROM_PERSON);							//AR Code.

	m_hshAccessRights.insert(GENERATE_USER_FROM_PERSON, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**							OPEN_USER_FUI						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Open \"User\" Windows");							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, OPEN_USER_FUI);										//AR Code.

	m_hshAccessRights.insert(OPEN_USER_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						SEE_USER_RECORDS_DATA					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See User Records Data");							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_USER_RECORDS_DATA);								//AR Code.

	m_hshAccessRights.insert(SEE_USER_RECORDS_DATA, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						EDIT_USER_RECORDS						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit User Records");								//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_USER_RECORDS);									//AR Code.

	m_hshAccessRights.insert(EDIT_USER_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						INSERT_USER_RECORDS						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Insert User Records");								//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, INSERT_USER_RECORDS);								//AR Code.

	m_hshAccessRights.insert(INSERT_USER_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						DELETE_USER_RECORDS						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Delete User Records");								//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, DELETE_USER_RECORDS);								//AR Code.

	m_hshAccessRights.insert(DELETE_USER_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					SEE_ONLY_ONE_COMPANY_USERS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See only Users of one company");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_ONLY_ONE_COMPANY_USERS);							//AR Code.

	m_hshAccessRights.insert(SEE_ONLY_ONE_COMPANY_USERS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					GENERATE_PERSON_FROM_USER					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Generate Person From User");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, GENERATE_PERSON_FROM_USER);							//AR Code.

	m_hshAccessRights.insert(GENERATE_PERSON_FROM_USER, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						OPEN_ORGANIZATIONS_FUI					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Open \"Organizations\" Windows");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, OPEN_ORGANIZATIONS_FUI);								//AR Code.

	m_hshAccessRights.insert(OPEN_ORGANIZATIONS_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					SEE_ORGANIZATIONS_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See \"Organization\" Records");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_ORGANIZATIONS_RECORDS);							//AR Code.

	m_hshAccessRights.insert(SEE_ORGANIZATIONS_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					EDIT_ORGANIZATIONS_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit \"Organization\" Records");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_ORGANIZATIONS_RECORDS);							//AR Code.

	m_hshAccessRights.insert(EDIT_ORGANIZATIONS_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					INSERT_ORGANIZATIONS_RECORDS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Insert \"Organization\" Records");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, INSERT_ORGANIZATIONS_RECORDS);						//AR Code.

	m_hshAccessRights.insert(INSERT_ORGANIZATIONS_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					DELETE_ORGANIZATIONS_RECORDS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Delete \"Organization\" Records");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, DELETE_ORGANIZATIONS_RECORDS);						//AR Code.

	m_hshAccessRights.insert(DELETE_ORGANIZATIONS_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				SEE_ONLY_ONE_NODE_ORGANIZATIONS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See only \"Organization\" of one node");				//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_ONLY_ONE_NODE_ORGANIZATIONS);					//AR Code.

	m_hshAccessRights.insert(SEE_ONLY_ONE_NODE_ORGANIZATIONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**			EDIT_INSET_DELETE_ONE_NODE_ORGANIZATIONS			**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit/insert/delete only \"Organization\" of one node");	//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_INSET_DELETE_ONE_NODE_ORGANIZATIONS);			//AR Code.

	m_hshAccessRights.insert(EDIT_INSET_DELETE_ONE_NODE_ORGANIZATIONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**			DEPARTMENT_DATA_VISIBLE_IN_ORGANIZATION_FUI			**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Department Data Visible in Org Windows");				//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, DEPARTMENT_DATA_VISIBLE_IN_ORGANIZATION_FUI);		//AR Code.

	m_hshAccessRights.insert(DEPARTMENT_DATA_VISIBLE_IN_ORGANIZATION_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**		COST_CENTER_DATA_VISIBLE_IN_ORGANIZATION_FUI			**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Cost Center Data Visible in Org Windows");				//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, COST_CENTER_DATA_VISIBLE_IN_ORGANIZATION_FUI);		//AR Code.

	m_hshAccessRights.insert(COST_CENTER_DATA_VISIBLE_IN_ORGANIZATION_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**			MODIFY_DEPARTMENTS_IN_ORGANIZATION_FUI				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Modify Departments in Org Windows");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_DEPARTMENTS_IN_ORGANIZATION_FUI);				//AR Code.

	m_hshAccessRights.insert(MODIFY_DEPARTMENTS_IN_ORGANIZATION_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**			MODIFY_COST_CENTERS_IN_ORGANIZATION_FUI				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Modify Cost Centers in Org Windows");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_COST_CENTERS_IN_ORGANIZATION_FUI);			//AR Code.

	m_hshAccessRights.insert(MODIFY_COST_CENTERS_IN_ORGANIZATION_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						OPEN_DEPARTMENTS_FUI					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Open \"Departments\" Windows");							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, OPEN_DEPARTMENTS_FUI);								//AR Code.

	m_hshAccessRights.insert(OPEN_DEPARTMENTS_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						SEE_DEPARTMENTS_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See \"Departments\" Records");							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_DEPARTMENTS_RECORDS);							//AR Code.

	m_hshAccessRights.insert(SEE_DEPARTMENTS_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					EDIT_DEPARTMENTS_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit \"Departments\" Records");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_DEPARTMENTS_RECORDS);							//AR Code.

	m_hshAccessRights.insert(EDIT_DEPARTMENTS_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					INSERT_DEPARTMENTS_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Insert \"Departments\" Records");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, INSERT_DEPARTMENTS_RECORDS);							//AR Code.

	m_hshAccessRights.insert(INSERT_DEPARTMENTS_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					DELETE_DEPARTMENTS_RECORDS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Delete \"Departments\" Records");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, DELETE_DEPARTMENTS_RECORDS);							//AR Code.

	m_hshAccessRights.insert(DELETE_DEPARTMENTS_RECORDS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				SEE_ONLY_ONE_COMPANY_DEPARTMENTS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See only \"Departments\" of one company");			//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_ONLY_ONE_COMPANY_DEPARTMENTS);					//AR Code.

	m_hshAccessRights.insert(SEE_ONLY_ONE_COMPANY_DEPARTMENTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**		EDIT_INSERT_DELETE_ONE_COMPANY_DEPARTMENTS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit/insert/delete only \"Departments\" of one company");	//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_INSERT_DELETE_ONE_COMPANY_DEPARTMENTS);			//AR Code.

	m_hshAccessRights.insert(EDIT_INSERT_DELETE_ONE_COMPANY_DEPARTMENTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				SEE_ONLY_ONE_NODE_DEPARTMENTS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "See only \"Departments\" of one node");				//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SEE_ONLY_ONE_NODE_DEPARTMENTS);						//AR Code.

	m_hshAccessRights.insert(SEE_ONLY_ONE_NODE_DEPARTMENTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				EDIT_INSET_DELETE_ONE_NODE_DEPARTMENTS			**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Edit/insert/delete only \"Departments\" of one node");//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EDIT_INSET_DELETE_ONE_NODE_DEPARTMENTS);				//AR Code.

	m_hshAccessRights.insert(EDIT_INSET_DELETE_ONE_NODE_DEPARTMENTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**			COST_CENTER_DATA_VISIBLE_IN_DEPARTMENT_FUI			**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Cost Center Data Visible in Dept Windows");				//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, COST_CENTER_DATA_VISIBLE_IN_DEPARTMENT_FUI);			//AR Code.

	m_hshAccessRights.insert(COST_CENTER_DATA_VISIBLE_IN_DEPARTMENT_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**			MODIFIY_COST_CENTERS_IN_DEPARTMENT_FUI				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Modify Cost Centers in Dept Windows");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFIY_COST_CENTERS_IN_DEPARTMENT_FUI);				//AR Code.

	m_hshAccessRights.insert(MODIFIY_COST_CENTERS_IN_DEPARTMENT_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				OPEN_USER_ACCESS_RIGHTS_FUI						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Open \"User Access Rights\" Windows");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, OPEN_USER_ACCESS_RIGHTS_FUI);						//AR Code.

	m_hshAccessRights.insert(OPEN_USER_ACCESS_RIGHTS_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						MODIFIY_USER_ROLES						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Modify User Roles");								//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFIY_USER_ROLES);									//AR Code.

	m_hshAccessRights.insert(MODIFIY_USER_ROLES, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				OPEN_PERSONAL_ACCESS_RIGHTS_FUI					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Open \"Personal Access Rights\" Windows");				//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, OPEN_PERSONAL_ACCESS_RIGHTS_FUI);					//AR Code.

	m_hshAccessRights.insert(OPEN_PERSONAL_ACCESS_RIGHTS_FUI, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					MODIFIY_PERSON_ROLES						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Modify Person\'s Roles");							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFIY_PERSON_ROLES);								//AR Code.

	m_hshAccessRights.insert(MODIFIY_PERSON_ROLES, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**							MAY_LOG_IN							**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "May Log In");										//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MAY_LOG_IN);											//AR Code.

	m_hshAccessRights.insert(MAY_LOG_IN, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						NO_LOG_IN_BEFORE						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "No Login Before");									//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, NO_LOG_IN_BEFORE);									//AR Code.

	m_hshAccessRights.insert(NO_LOG_IN_BEFORE, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						NO_LOG_IN_AFTER							**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "No Login After");									//AR Name.
	AccessRightRecordSet.setData(0, 6, 2);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, NO_LOG_IN_AFTER);									//AR Code.

	m_hshAccessRights.insert(NO_LOG_IN_AFTER, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					MAY_SEE_SYSTEM_INFORMATION					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "May See System Information");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MAY_SEE_SYSTEM_INFORMATION);							//AR Code.

	m_hshAccessRights.insert(MAY_SEE_SYSTEM_INFORMATION, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					MAY_SEE_SESSIONS_INFORMATION				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "May See Sessions Information");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MAY_SEE_SESSIONS_INFORMATION);						//AR Code.

	m_hshAccessRights.insert(MAY_SEE_SESSIONS_INFORMATION, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						MAY_KILL_SESSIONS						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "May kill sessions");								//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MAY_KILL_SESSIONS);									//AR Code.

	m_hshAccessRights.insert(MAY_KILL_SESSIONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**							VIEW_OPTIONS						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "View Options");										//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, VIEW_OPTIONS);										//AR Code.

	m_hshAccessRights.insert(VIEW_OPTIONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						MODIFY_OPTIONS							**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Modify Options");									//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_OPTIONS);										//AR Code.

	m_hshAccessRights.insert(MODIFY_OPTIONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					VIEW_OWN_PERSONAL_SETTINGS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "View Own Personal Settings");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, VIEW_OWN_PERSONAL_SETTINGS);										//AR Code.

	m_hshAccessRights.insert(VIEW_OWN_PERSONAL_SETTINGS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					MODIFY_OWN_PERSONAL_SETTINGS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "Modify Own Personal Settings");						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_OWN_PERSONAL_SETTINGS);						//AR Code.

	m_hshAccessRights.insert(MODIFY_OWN_PERSONAL_SETTINGS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				VIEW_PERSONAL_SETTINGS_OF_OTHERS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, "View Personal Settings of Others");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, VIEW_PERSONAL_SETTINGS_OF_OTHERS);					//AR Code.

	m_hshAccessRights.insert(VIEW_PERSONAL_SETTINGS_OF_OTHERS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				MODIFY_PERSONAL_SETTINGS_OF_OTHERS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Modify Personal Settings of Others"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_PERSONAL_SETTINGS_OF_OTHERS);					//AR Code.

	m_hshAccessRights.insert(MODIFY_PERSONAL_SETTINGS_OF_OTHERS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();
	
	//********************************************************************
	//**					ENTER_MODIFY_PROJECT_MAIN_DATA				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify Project Main Data"));				//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_PROJECT_MAIN_DATA);						//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_PROJECT_MAIN_DATA, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					SYNCHRONIZE_PROJECTS_AND_DEBTORS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Synchronize Projects & Debtors"));				//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, SYNCHRONIZE_PROJECTS_AND_DEBTORS);					//AR Code.

	m_hshAccessRights.insert(SYNCHRONIZE_PROJECTS_AND_DEBTORS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						IMPORT_PROJECTS							**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Import Projects"));								//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, IMPORT_PROJECTS);									//AR Code.

	m_hshAccessRights.insert(IMPORT_PROJECTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						EXPORT_PROJECTS							**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Export Projects"));								//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, EXPORT_PROJECTS);									//AR Code.

	m_hshAccessRights.insert(EXPORT_PROJECTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					ADMINISTRATION_TOOLS_ACCESS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Administratrion Tools Access"));					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ADMINISTRATION_TOOLS_ACCESS);						//AR Code.

	m_hshAccessRights.insert(ADMINISTRATION_TOOLS_ACCESS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**							OPTIONS								**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Options"));										//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, OPTIONS);											//AR Code.

	m_hshAccessRights.insert(OPTIONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**					REGISTER_APPLICATIONS						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Register Applications"));						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, REGISTER_APPLICATIONS);								//AR Code.

	m_hshAccessRights.insert(REGISTER_APPLICATIONS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						ENTER_MODIFY_USERS						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify Users"));					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_USERS);									//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_USERS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**	ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU		**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Moify all Types & Adress-Schemas (in Menu Organization)")); //AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU);	//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						ENTER_MODIFY_CONTACTS					**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify Contacts"));						//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_CONTACTS);								//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_CONTACTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**		ASSIGN_CE_ENTITIES_TO_PROJECTS_AND_CONTACTS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Assign phone calls, emails and documents to projects and contacts"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ASSIGN_CE_ENTITIES_TO_PROJECTS_AND_CONTACTS);		//AR Code.

	m_hshAccessRights.insert(ASSIGN_CE_ENTITIES_TO_PROJECTS_AND_CONTACTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				ENTER_MODIFY_CONTACT_GROUP_TREES				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify Contact Group Trees"));				//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_CONTACT_GROUP_TREES);					//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_CONTACT_GROUP_TREES, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						REGISTER_TEMPLATES						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Register Templates (Docs & Emails)"));			//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, REGISTER_TEMPLATES);									//AR Code.

	m_hshAccessRights.insert(REGISTER_TEMPLATES, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**						LOAD_PROJECT_LISTS						**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Load Project Lists"));					//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, LOAD_PROJECT_LISTS);									//AR Code.

	m_hshAccessRights.insert(LOAD_PROJECT_LISTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				ENTER_MODIFY_PRIVATE_PROJECT_LISTS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify Private Project Lists"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_PRIVATE_PROJECT_LISTS);					//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_PRIVATE_PROJECT_LISTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				ENTER_MODIFY_PUBLIC_PROJECT_LISTS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify Public Project Lists"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_PUBLIC_PROJECT_LISTS);					//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_PUBLIC_PROJECT_LISTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				MODIFY_FOREIGN_PERSONAL_SETTINGS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Modify Foreign Personal Settings"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_FOREIGN_PERSONAL_SETTINGS);					//AR Code.

	m_hshAccessRights.insert(MODIFY_FOREIGN_PERSONAL_SETTINGS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				MODIFY_CALENDAR_CATEGORY				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify 'Category' for Calendar Entries"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_CALENDAR_CATEGORY);					//AR Code.

	m_hshAccessRights.insert(MODIFY_CALENDAR_CATEGORY, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();


	//********************************************************************
	//**				MODIFY_CALENDAR_SHOW_TIME_AS				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify 'Show Time As' for Calendar Entries"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_CALENDAR_SHOW_TIME_AS);					//AR Code.

	m_hshAccessRights.insert(MODIFY_CALENDAR_SHOW_TIME_AS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();


	//********************************************************************
	//**				MODIFY_DOCUMENTS_CATEGORY				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify 'Category' for Documents"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_DOCUMENTS_CATEGORY);					//AR Code.

	m_hshAccessRights.insert(MODIFY_DOCUMENTS_CATEGORY, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();


	//********************************************************************
	//**				MODIFY_NMRX_ROLES				**
	//********************************************************************
	AccessRightRecordSet.addRow();															//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);												//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);												//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify assignment roles"));		//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);													//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);													//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");													//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, MODIFY_NMRX_ROLES);					//AR Code.

	m_hshAccessRights.insert(MODIFY_NMRX_ROLES, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				ENTER_MODIFY_CONTACT_ASSIGNMENT_ROLE			**
	//********************************************************************
	AccessRightRecordSet.addRow();																//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);													//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);													//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify contact assignment roles"));	//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);														//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);														//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");														//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_CONTACT_ASSIGNMENT_ROLE);					//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_CONTACT_ASSIGNMENT_ROLE, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();


	//********************************************************************
	//**				OPEN_CALENDAR			**
	//********************************************************************
	AccessRightRecordSet.addRow();																//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);													//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);													//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Open Calendar"));							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);														//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);														//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");														//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, OPEN_CALENDAR);					//AR Code.

	m_hshAccessRights.insert(OPEN_CALENDAR, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				ENTER_MODIFY_CALENDAR			**
	//********************************************************************
	AccessRightRecordSet.addRow();																//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);													//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);													//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Enter/Modify Calendar Entries"));			//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);														//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);														//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");														//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ENTER_MODIFY_CALENDAR);					//AR Code.

	m_hshAccessRights.insert(ENTER_MODIFY_CALENDAR, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();


	//********************************************************************
	//**				ASSIGN_CONTACTS			**
	//********************************************************************
	AccessRightRecordSet.addRow();																//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);													//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);													//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Assign Contacts"));							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);														//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);														//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");														//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ASSIGN_CONTACTS);					//AR Code.

	m_hshAccessRights.insert(ASSIGN_CONTACTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**				ASSIGN_PROJECTS			**
	//********************************************************************
	AccessRightRecordSet.addRow();																//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);													//ARS ID (inserted later - from GUI).
	//AccessRightRecordSet.setData(0, 4, 0);													//AR Role ID (inserted later - from GUI).
	AccessRightRecordSet.setData(0, 5, QObject::tr("Assign Projects"));							//AR Name.
	AccessRightRecordSet.setData(0, 6, 1);														//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 1);														//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");														//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ASSIGN_PROJECTS);					//AR Code.

	m_hshAccessRights.insert(ASSIGN_PROJECTS, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

}


void AccessRightsOrganizer::InitializeAccessRightSets()
{
	DbRecordSet AccessRightSetRecordSet;

	//********************************************************************
	//**				PERSONAL_RECORDS_MAINTAINANCE					**
	//********************************************************************
	AccessRightSetRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCRSET));
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "Personal Records Maintenance");					//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "Personal Records Maintenance Rights Set");		//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, PERSONAL_RECORDS_MAINTAINANCE);					//ARS Code.

	m_hshAccessRightSets.insert(PERSONAL_RECORDS_MAINTAINANCE, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**				USER_RECORDS_MAINTAINANCE						**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "User Records Maintenance");						//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "User Records Maintenance Rights Set");			//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, USER_RECORDS_MAINTAINANCE);						//ARS Code.

	m_hshAccessRightSets.insert(USER_RECORDS_MAINTAINANCE, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**				ORGANIZATIONS_MAINTAINANCE						**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "Organizations Maintenance");						//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "Organizations Maintenance Rights Set");			//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, ORGANIZATIONS_MAINTAINANCE);						//ARS Code.

	m_hshAccessRightSets.insert(ORGANIZATIONS_MAINTAINANCE, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**					DEPARTMENTS_MAINTAINANCE					**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "Departments Maintenance");						//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "Departments Maintenance Rights Set");			//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, DEPARTMENTS_MAINTAINANCE);						//ARS Code.

	m_hshAccessRightSets.insert(DEPARTMENTS_MAINTAINANCE, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						ACCESS__RIGHTS							**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "Access Rights");									//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "Access Rights Rights Set");						//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, ACCESS__RIGHTS);									//ARS Code.

	m_hshAccessRightSets.insert(ACCESS__RIGHTS, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**							LOGIN								**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "Login");											//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "Login Rights Set");								//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, LOGIN);											//ARS Code.

	m_hshAccessRightSets.insert(LOGIN, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						SYSTEM_RIGHTS							**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "System Rights");									//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "System Rights Rights Set");						//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, SYSTEM_RIGHTS);									//ARS Code.

	m_hshAccessRightSets.insert(SYSTEM_RIGHTS, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**				OPTIONS_AND_PERSONAL_SETTINGS					**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "Options and Personal Settings");					//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "Options and Personal Settings Rights Set");		//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, OPTIONS_AND_PERSONAL_SETTINGS);					//ARS Code.

	m_hshAccessRightSets.insert(OPTIONS_AND_PERSONAL_SETTINGS, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						PROJECT_MANAGER							**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Project Manager"));							//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Project Manager Rights Set"));				//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, PROJECT_MANAGER);									//ARS Code.

	m_hshAccessRightSets.insert(PROJECT_MANAGER, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						ADMINISTRATOR							**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Administrator"));								//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Administrator Rights Set"));					//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, ADMINISTRATOR);									//ARS Code.

	m_hshAccessRightSets.insert(ADMINISTRATOR, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						HUMAN_RESOURCES							**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Human Resources"));							//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Human Resources Rights Set"));				//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, HUMAN_RESOURCES);									//ARS Code.

	m_hshAccessRightSets.insert(HUMAN_RESOURCES, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**							MAIN_DATA							**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Main Data"));									//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Main Data Rights Set"));						//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, MAIN_DATA);										//ARS Code.

	m_hshAccessRightSets.insert(MAIN_DATA, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						CONTACT_MANAGER							**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Contact Manager"));							//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Contact Manager Rights Set"));				//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, CONTACT_MANAGER);									//ARS Code.

	m_hshAccessRightSets.insert(CONTACT_MANAGER, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						COMMUNICATOR							**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Communicator"));								//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Communicator Rights Set"));			//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, COMMUNICATOR);									//ARS Code.

	m_hshAccessRightSets.insert(COMMUNICATOR, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						GROUP_MANAGEMENT						**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Group Management"));							//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Group Management Rights Set"));				//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, GROUP_MANAGEMENT);								//ARS Code.

	m_hshAccessRightSets.insert(GROUP_MANAGEMENT, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						TEMPLATE_MANAGER						**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Template Manager"));							//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Template Manager Rights Set"));				//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, TEMPLATE_MANAGER);								//ARS Code.

	m_hshAccessRightSets.insert(TEMPLATE_MANAGER, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**					PROJECT_LIST_MANAGEMENT						**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Project List Management"));			//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Project List Management Rights Set"));//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, PROJECT_LIST_MANAGEMENT);							//ARS Code.

	m_hshAccessRightSets.insert(PROJECT_LIST_MANAGEMENT, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**					CALENDAR_ACCESS						**
	//********************************************************************
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, QObject::tr("Calendar Access"));					//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, QObject::tr("Calendar Access Rights Set"));		//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 1);												//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 6, CALENDAR_ACCESS);									//ARS Code.

	m_hshAccessRightSets.insert(CALENDAR_ACCESS, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

}

void AccessRightsOrganizer::InitializeARSRights()
{
	//********************************************************************
	//**				PERSONAL_RECORDS_MAINTAINANCE					**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(OPEN_PERSON_FUI);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(SEE_PERSONAL_RECORDS_DATA);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(EDIT_PERSONAL_RECORDS);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(INSERT_PERSONAL_RECORDS);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(DELETE_PERSONAL_RECORDS);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(SEE_COMPANY_PERSONS);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(EDIT_INSERT_DELETE_COMPANY_PERSONS);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(SEE_DEPARTMENT_PERSONS);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(EDIT_INSERT_DELETE_DEPARTMENT_PERSONS);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(EMPLOYMENT_DATA_VISIBLE);
	m_hshAccessRightSets.value(PERSONAL_RECORDS_MAINTAINANCE)->AddAccessRight(GENERATE_USER_FROM_PERSON);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**					USER_RECORDS_MAINTAINANCE					**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(OPEN_USER_FUI);
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(SEE_USER_RECORDS_DATA);
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(EDIT_USER_RECORDS);
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(INSERT_USER_RECORDS);
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(DELETE_USER_RECORDS);
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(SEE_ONLY_ONE_COMPANY_USERS);
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(SEE_DEPARTMENT_PERSONS);
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(EDIT_INSERT_DELETE_DEPARTMENT_PERSONS);
	m_hshAccessRightSets.value(USER_RECORDS_MAINTAINANCE)->AddAccessRight(GENERATE_PERSON_FROM_USER);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**					ORGANIZATIONS_MAINTAINANCE					**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(OPEN_ORGANIZATIONS_FUI);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(SEE_ORGANIZATIONS_RECORDS);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(EDIT_ORGANIZATIONS_RECORDS);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(INSERT_ORGANIZATIONS_RECORDS);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(DELETE_ORGANIZATIONS_RECORDS);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(SEE_ONLY_ONE_NODE_ORGANIZATIONS);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(EDIT_INSET_DELETE_ONE_NODE_ORGANIZATIONS);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(DEPARTMENT_DATA_VISIBLE_IN_ORGANIZATION_FUI);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(COST_CENTER_DATA_VISIBLE_IN_ORGANIZATION_FUI);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(MODIFY_DEPARTMENTS_IN_ORGANIZATION_FUI);
	m_hshAccessRightSets.value(ORGANIZATIONS_MAINTAINANCE)->AddAccessRight(MODIFY_COST_CENTERS_IN_ORGANIZATION_FUI);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**					DEPARTMENTS_MAINTAINANCE					**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(OPEN_DEPARTMENTS_FUI);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(SEE_DEPARTMENTS_RECORDS);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(EDIT_DEPARTMENTS_RECORDS);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(INSERT_DEPARTMENTS_RECORDS);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(DELETE_DEPARTMENTS_RECORDS);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(SEE_ONLY_ONE_COMPANY_DEPARTMENTS);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(EDIT_INSERT_DELETE_ONE_COMPANY_DEPARTMENTS);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(SEE_ONLY_ONE_NODE_DEPARTMENTS);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(EDIT_INSET_DELETE_ONE_NODE_DEPARTMENTS);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(COST_CENTER_DATA_VISIBLE_IN_DEPARTMENT_FUI);
	m_hshAccessRightSets.value(DEPARTMENTS_MAINTAINANCE)->AddAccessRight(MODIFIY_COST_CENTERS_IN_DEPARTMENT_FUI);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						ACCESS__RIGHTS							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(ACCESS__RIGHTS)->AddAccessRight(OPEN_USER_ACCESS_RIGHTS_FUI);
	m_hshAccessRightSets.value(ACCESS__RIGHTS)->AddAccessRight(MODIFIY_USER_ROLES);
	m_hshAccessRightSets.value(ACCESS__RIGHTS)->AddAccessRight(OPEN_PERSONAL_ACCESS_RIGHTS_FUI);
	m_hshAccessRightSets.value(ACCESS__RIGHTS)->AddAccessRight(MODIFIY_PERSON_ROLES);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**							LOGIN								**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(LOGIN)->AddAccessRight(MAY_LOG_IN);
	m_hshAccessRightSets.value(LOGIN)->AddAccessRight(NO_LOG_IN_BEFORE);
	m_hshAccessRightSets.value(LOGIN)->AddAccessRight(NO_LOG_IN_AFTER);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**							SYSTEM_RIGHTS						**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(SYSTEM_RIGHTS)->AddAccessRight(MAY_SEE_SYSTEM_INFORMATION);
	m_hshAccessRightSets.value(SYSTEM_RIGHTS)->AddAccessRight(MAY_SEE_SESSIONS_INFORMATION);
	m_hshAccessRightSets.value(SYSTEM_RIGHTS)->AddAccessRight(MAY_KILL_SESSIONS);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**				OPTIONS_AND_PERSONAL_SETTINGS					**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(OPTIONS_AND_PERSONAL_SETTINGS)->AddAccessRight(VIEW_OPTIONS);
	m_hshAccessRightSets.value(OPTIONS_AND_PERSONAL_SETTINGS)->AddAccessRight(MODIFY_OPTIONS);
	m_hshAccessRightSets.value(OPTIONS_AND_PERSONAL_SETTINGS)->AddAccessRight(VIEW_OWN_PERSONAL_SETTINGS);
	m_hshAccessRightSets.value(OPTIONS_AND_PERSONAL_SETTINGS)->AddAccessRight(MODIFY_OWN_PERSONAL_SETTINGS);
	m_hshAccessRightSets.value(OPTIONS_AND_PERSONAL_SETTINGS)->AddAccessRight(VIEW_PERSONAL_SETTINGS_OF_OTHERS);
	m_hshAccessRightSets.value(OPTIONS_AND_PERSONAL_SETTINGS)->AddAccessRight(MODIFY_PERSONAL_SETTINGS_OF_OTHERS);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						PROJECT_MANAGER							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(PROJECT_MANAGER)->AddAccessRight(ENTER_MODIFY_PROJECT_MAIN_DATA);
	m_hshAccessRightSets.value(PROJECT_MANAGER)->AddAccessRight(SYNCHRONIZE_PROJECTS_AND_DEBTORS);
	m_hshAccessRightSets.value(PROJECT_MANAGER)->AddAccessRight(IMPORT_PROJECTS);
	m_hshAccessRightSets.value(PROJECT_MANAGER)->AddAccessRight(EXPORT_PROJECTS);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						ADMINISTRATOR							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(ADMINISTRATOR)->AddAccessRight(ADMINISTRATION_TOOLS_ACCESS);
	m_hshAccessRightSets.value(ADMINISTRATOR)->AddAccessRight(OPTIONS);
	m_hshAccessRightSets.value(ADMINISTRATOR)->AddAccessRight(REGISTER_APPLICATIONS);
	m_hshAccessRightSets.value(ADMINISTRATOR)->AddAccessRight(MODIFY_FOREIGN_PERSONAL_SETTINGS);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						HUMAN_RESOURCES							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(HUMAN_RESOURCES)->AddAccessRight(ENTER_MODIFY_USERS);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**							MAIN_DATA							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(MAIN_DATA)->AddAccessRight(ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU);
	m_hshAccessRightSets.value(MAIN_DATA)->AddAccessRight(MODIFY_CALENDAR_CATEGORY);
	m_hshAccessRightSets.value(MAIN_DATA)->AddAccessRight(MODIFY_CALENDAR_SHOW_TIME_AS);
	m_hshAccessRightSets.value(MAIN_DATA)->AddAccessRight(MODIFY_DOCUMENTS_CATEGORY);
	m_hshAccessRightSets.value(MAIN_DATA)->AddAccessRight(MODIFY_NMRX_ROLES);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						CONTACT_MANAGER							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(CONTACT_MANAGER)->AddAccessRight(ENTER_MODIFY_CONTACTS);
	m_hshAccessRightSets.value(CONTACT_MANAGER)->AddAccessRight(ENTER_MODIFY_CONTACT_ASSIGNMENT_ROLE);
	m_hshAccessRightSets.value(CONTACT_MANAGER)->AddAccessRight(ASSIGN_CONTACTS);
	m_hshAccessRightSets.value(CONTACT_MANAGER)->AddAccessRight(ASSIGN_PROJECTS);

	
	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						COMMUNICATOR							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(COMMUNICATOR)->AddAccessRight(ASSIGN_CE_ENTITIES_TO_PROJECTS_AND_CONTACTS);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						GROUP_MANAGEMENT						**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(GROUP_MANAGEMENT)->AddAccessRight(ENTER_MODIFY_CONTACT_GROUP_TREES);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						TEMPLATE_MANAGER						**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(TEMPLATE_MANAGER)->AddAccessRight(REGISTER_TEMPLATES);

	//********************************************************************
	//**					PROJECT_LIST_MANAGEMENT						**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(PROJECT_LIST_MANAGEMENT)->AddAccessRight(LOAD_PROJECT_LISTS);
	m_hshAccessRightSets.value(PROJECT_LIST_MANAGEMENT)->AddAccessRight(ENTER_MODIFY_PRIVATE_PROJECT_LISTS);
	m_hshAccessRightSets.value(PROJECT_LIST_MANAGEMENT)->AddAccessRight(ENTER_MODIFY_PUBLIC_PROJECT_LISTS);


	//********************************************************************
	//**					CALENDAR_ACCESS								**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(CALENDAR_ACCESS)->AddAccessRight(OPEN_CALENDAR);
	m_hshAccessRightSets.value(CALENDAR_ACCESS)->AddAccessRight(ENTER_MODIFY_CALENDAR);



	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);
}

AccessRight* AccessRightsOrganizer::GetAccessRight(int AccessRightID)
{
	return m_hshAccessRights.value(AccessRightID);
}

AccessRightSet* AccessRightsOrganizer::GetAccessRightSet(int AccessRightSetID)
{
	return m_hshAccessRightSets.value(AccessRightSetID);
}

QHash<int, AccessRightSet *>* AccessRightsOrganizer::GetAccessRightSetHash()
{
	return &m_hshAccessRightSets;
}

QHash<int, AccessRightSet *> AccessRightsOrganizer::GetSimpleAccessRightSetHash()
{
	QHash<int, AccessRightSet *> hshAccessRightSets;
	
	hshAccessRightSets.insert(PROJECT_MANAGER, m_hshAccessRightSets.value(PROJECT_MANAGER));
	hshAccessRightSets.insert(ADMINISTRATOR, m_hshAccessRightSets.value(ADMINISTRATOR));
	hshAccessRightSets.insert(HUMAN_RESOURCES, m_hshAccessRightSets.value(HUMAN_RESOURCES));
	hshAccessRightSets.insert(MAIN_DATA, m_hshAccessRightSets.value(MAIN_DATA));
	hshAccessRightSets.insert(CONTACT_MANAGER, m_hshAccessRightSets.value(CONTACT_MANAGER));
	hshAccessRightSets.insert(COMMUNICATOR, m_hshAccessRightSets.value(COMMUNICATOR));
	hshAccessRightSets.insert(GROUP_MANAGEMENT, m_hshAccessRightSets.value(GROUP_MANAGEMENT));
	hshAccessRightSets.insert(TEMPLATE_MANAGER, m_hshAccessRightSets.value(TEMPLATE_MANAGER));
	
	return hshAccessRightSets;
}
