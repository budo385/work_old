#include "formataddress.h"
#include <QStringList>
#include "bus_core/bus_core/countries.h"

FormatAddress::FormatAddress()
{

	//addr (for faster access):
	m_hshAdrFields["BCMA_FIRSTNAME"]=QObject::tr("First Name");
	m_hshAdrFields["BCMA_LASTNAME"]=QObject::tr("Last Name");
	m_hshAdrFields["BCMA_MIDDLENAME"]=QObject::tr("Middle Name");
	m_hshAdrFields["BCMA_TITLE"]=QObject::tr("Title");
	m_hshAdrFields["BCMA_SALUTATION"]=QObject::tr("Salutation");
	m_hshAdrFields["BCMA_SHORT_SALUTATION"]=QObject::tr("Short Salutation");
	m_hshAdrFields["BCMA_LANGUGAGE_CODE"]=QObject::tr("Language");
	m_hshAdrFields["BCMA_ORGANIZATIONNAME"]=QObject::tr("Organization");
	m_hshAdrFields["BCMA_ORGANIZATIONNAME_2"]=QObject::tr("Organization 2");
	m_hshAdrFields["BCMA_STREET"]=QObject::tr("Street");
	m_hshAdrFields["BCMA_ZIP"]=QObject::tr("ZIP/Postal Code");
	m_hshAdrFields["BCMA_CITY"]=QObject::tr("City");
	m_hshAdrFields["BCMA_COUNTRY_CODE"]=QObject::tr("Country Code");
	m_hshAdrFields["BCMA_COUNTRY_NAME"]=QObject::tr("Country");
	m_hshAdrFields["BCMA_POBOX"]=QObject::tr("PO BOX");
	m_hshAdrFields["BCMA_POBOXZIP"]=QObject::tr("PO BOX ZIP");
	m_hshAdrFields["BCMA_OFFICECODE"]=QObject::tr("Office Code");

	//for sorted view (only names):
	m_lstAdrFields.addColumn(QVariant::String,"COL_NAME");

	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("First Name"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Last Name"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Middle Name"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Title"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Salutation"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Short Salutation"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Language"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Organization"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Organization 2"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Street"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("ZIP/Postal Code"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("City"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Country Code"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Country"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("PO BOX"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("PO BOX ZIP"));
	m_lstAdrFields.addRow();m_lstAdrFields.setData(m_lstAdrFields.getRowCount()-1,0,QObject::tr("Office Code"));


	//def. schema:
	m_strDefaultSchema="[[BCMA_ORGANIZATIONNAME]]\n";
	m_strDefaultSchema+="[BCMA_FIRSTNAME] [BCMA_LASTNAME]\n";
	m_strDefaultSchema+="[[BCMA_STREET]]\n";
	m_strDefaultSchema+="[[BCMA_COUNTRY_CODE]-][[BCMA_ZIP] ][BCMA_CITY]";
}

FormatAddress::~FormatAddress()
{

}


//return list
void FormatAddress::GetAddressFieldLst(HshAdrFields &hshAdrFields)
{
	hshAdrFields=m_hshAdrFields;
}



//replaces schema with DB fields with Display field in current lang
QString FormatAddress::MapSchemaToDisplay(QString strSchemaDB)
{
	HshAdrFieldsIterator i(m_hshAdrFields);
	while(i.hasNext())
	{
		i.next();
		strSchemaDB=strSchemaDB.replace("["+i.key()+"]","["+i.value()+"]");
	}
	return strSchemaDB;
}


//replaces schema with DB fields from Display field in current lang
QString FormatAddress::MapDisplayToSchema(QString strSchemaDisplay)
{
	HshAdrFieldsIterator i(m_hshAdrFields);
	while(i.hasNext())
	{
		i.next();
		strSchemaDisplay=strSchemaDisplay.replace("["+i.value()+"]","["+i.key()+"]");
	}
	return strSchemaDisplay;
}



//format addresses based on values of addr fields inside rowAddress and given schema (in DB format)
//format address is stored inside field FORMATTEDADDRESS!
void FormatAddress::AddressFormat(DbRecordSet &lstAddress,QString strSchemaDB,bool bSkipFormattingIfExists)
{
	int nFormatAddrIdx=lstAddress.getColumnIdx("BCMA_FORMATEDADDRESS");

	//lstAddress.setColValue(lstAddress.getColumnIdx("BCMA_STREET"),"");
	//lstAddress.Dump();

	int nSize=lstAddress.getRowCount();
	for (int z=0;z<nSize;++z)
	{
		FormatAddressRow(lstAddress,z,strSchemaDB,nFormatAddrIdx,bSkipFormattingIfExists);
	}
	
}


//same as format address but every address is formatted separately with own schema
//size of lists must be same
void FormatAddress::AddressFormat(DbRecordSet &lstAddress,QStringList lstSchemaDB,bool bSkipFormattingIfExists)
{

	if (lstSchemaDB.size()!=lstAddress.getRowCount())
	{
		Q_ASSERT(false);		//error
		return;
	}


	int nFormatAddrIdx=lstAddress.getColumnIdx("BCMA_FORMATEDADDRESS");
	int nSize=lstAddress.getRowCount();
	for (int z=0;z<nSize;++z)
	{
		FormatAddressRow(lstAddress,z,lstSchemaDB.at(z),nFormatAddrIdx,bSkipFormattingIfExists);

	}

}



//Parse address fields from FormatAddress field, use schema
void FormatAddress::AddressParse(DbRecordSet &lstAddress,QString strSchemaDB)
{

	DbRecordSet lstParsedSchema;
	if(!ParseSchema(strSchemaDB,lstParsedSchema)) return; 

	//lstParsedSchema.Dump();
	
	int nFormatAddrIdx=lstAddress.getColumnIdx("BCMA_FORMATEDADDRESS");
	int nSize=lstAddress.getRowCount();
	for (int z=0;z<nSize;++z)
	{
		//get address
		QString strAddress=lstAddress.getDataRef(z,nFormatAddrIdx).toString();
		if(strAddress.isEmpty())continue;

		//parse:
		//recycle address to linefeeds:
		QStringList lstLines=strAddress.split("\n");

		int nCurrentLine=-1;
		QString strLine;
		int nSize1=lstParsedSchema.getRowCount();
		for (int i=0;i<nSize1;++i)
		{
			if(lstParsedSchema.getDataRef(i,0).toInt()>=lstLines.size())
				break;

			if(nCurrentLine!=lstParsedSchema.getDataRef(i,0).toInt())
			{
				nCurrentLine=lstParsedSchema.getDataRef(i,0).toInt();
				strLine=lstLines.at(nCurrentLine);
			}

			int nStartField,nLength;
			QString strField,strPrev,strAfter,strValue;

			strPrev=lstParsedSchema.getDataRef(i,"Prefix").toString();
			if(!strPrev.isEmpty())
				nStartField=strLine.indexOf(strPrev)+strPrev.length();
			else
				nStartField=0;

			strAfter=lstParsedSchema.getDataRef(i,"Sufix").toString();
			if(!strAfter.isEmpty())
				nLength=strLine.indexOf(strAfter)-nStartField;
			else
				nLength=strLine.length()-nStartField;
		


			strValue=strLine.mid(nStartField,nLength);
			strLine.remove(0,nStartField+nLength+strAfter.length());

			//store in schema
			lstParsedSchema.setData(i,"Value",strValue);

			//store in address:
			int nColIdx=lstAddress.getColumnIdx(lstParsedSchema.getDataRef(i,"Field").toString());
			if(nColIdx!=-1)
			{
				if (lstParsedSchema.getDataRef(i,"Field").toString()=="BCMA_COUNTRY_CODE") //issue 1772, code now contains ISO--transfer to DS-CODE
					strValue=Countries::GetISOCodeFromDs(strValue);
				lstAddress.setData(z,nColIdx,strValue);
			}

		}
	}


}



//Disolve schema into lines:
//Line,Prev, Field, After


bool FormatAddress::ParseSchema(QString strSchemaDB, DbRecordSet &lstParsed)
{

	//define list:
	lstParsed.destroy();
	lstParsed.addColumn(QVariant::Int,"Line");
	lstParsed.addColumn(QVariant::String,"Field");
	lstParsed.addColumn(QVariant::String,"Value");
	lstParsed.addColumn(QVariant::String,"Prefix");
	lstParsed.addColumn(QVariant::String,"Sufix");

	//recycle schema to linefeeds:
	QStringList lstLines=strSchemaDB.split("\n");

	QString strLine;
	//bool bValueExists;
	int nSize=lstLines.size();
	for (int i=0;i<nSize;++i)
	{
		//extract 1 line:
		strLine=lstLines.at(i);
		if(strLine.size()==0)
			continue;

		//no nested strings:
		do 
		{
			QString strField,strPrev,strAfter;
			
			//find before [
			int nPos1=strLine.indexOf("[");
			if(nPos1==-1) break; 
			strPrev=strLine.left(nPos1);

			//find content of [ ]
			int nPos2=strLine.indexOf("]");
			strField=strLine.mid(nPos1+1,nPos2-nPos1-1);
			if(strField.isEmpty())return false;

			//find after ]
			int nPos3=strLine.indexOf("[",nPos2+1);
			if(nPos3!=-1)
			{
				strAfter=strLine.mid(nPos2+1,nPos3-nPos2-1);
			}

			lstParsed.addRow();
			int nRow=lstParsed.getRowCount()-1;
			lstParsed.setData(nRow,0,i);
			lstParsed.setData(nRow,1,strField);
			lstParsed.setData(nRow,3,strPrev);
			lstParsed.setData(nRow,4,strAfter);

			if(nPos3!=-1)
				strLine.remove(0,nPos3);
			else
				strLine.remove(0,nPos2+1);


		} while(!strLine.isEmpty());

	}

	return true;
}






//format addresses:
void FormatAddress::FormatAddressRow(DbRecordSet &lstAddress, int z, QString strSchemaDB, int nFormatAddrIdx,bool bSkipFormattingIfExists)
{
	//skip if exists and set
	if(bSkipFormattingIfExists && !lstAddress.getDataRef(z,nFormatAddrIdx).toString().isEmpty())
	{
		//MB: if field is not empty and contains // then take as is, just convert // -> \n
		QString strAddr=lstAddress.getDataRef(z,nFormatAddrIdx).toString();
		strAddr.replace("//","\n");
		lstAddress.setData(z,nFormatAddrIdx,strAddr);
		return;
	}

	//replace all [DBfield] with DBValue
	HshAdrFieldsIterator i(m_hshAdrFields);
	while(i.hasNext())
	{
		i.next();

		if(lstAddress.getColumnIdx(i.key())==-1)  //if field is obsolote
			continue;

		if (lstAddress.getDataRef(z,i.key()).toString().isEmpty())
			strSchemaDB=strSchemaDB.replace("["+i.key()+"]","--EMPTY_FIELD--");
		else
		{
			QString strFieldValue=lstAddress.getDataRef(z,i.key()).toString(); 		//encode special chars:
			if (i.key()=="BCMA_COUNTRY_CODE") //issue 1772, code now contains ISO--transfer to DS-CODE
			{
				strFieldValue=Countries::GetDsCodeFromISO(strFieldValue);
			}
			
			strFieldValue.replace("[","--LEFT_BRACKET--");
			strFieldValue.replace("]","--RIGHT_BRACKET--");
			strSchemaDB=strSchemaDB.replace("["+i.key()+"]",strFieldValue);
		}
	}



	//find nested pairs: [] 
	int nLeft,nRight;
	nLeft=strSchemaDB.indexOf("[");
	while(nLeft>=0)
	{
		nRight=strSchemaDB.indexOf("]");
		if(nRight>nLeft)
		{
			QString strNested=strSchemaDB.mid(nLeft+1,nRight-nLeft-1);
			bool bErase=false;
			if(strNested.indexOf("--EMPTY_FIELD--")!=-1)
				bErase=true;

			if (bErase) //erase all inside nested if not found
				strSchemaDB=strSchemaDB.remove(nLeft,nRight-nLeft+1); //
			else	//just remove brackets:
			{
				strSchemaDB=strSchemaDB.remove(nLeft,1);
				strSchemaDB=strSchemaDB.remove(nRight-1,1);
			}
			nLeft=strSchemaDB.indexOf("[");	//new loop

		}

	}

	strSchemaDB.replace("--EMPTY_FIELD--",""); //remove empty fields
	strSchemaDB.replace("--LEFT_BRACKET--","[");
	strSchemaDB.replace("--RIGHT_BRACKET--","]");


	//remove trailing spaces, space before and empty lines:
	QStringList lstLines=strSchemaDB.split("\n",QString::SkipEmptyParts);
	strSchemaDB="";
	int nSize=lstLines.size();
	for (int i=0;i<nSize;++i)
	{
		QString strLine=lstLines.at(i);
		if (strLine.size()<4)
			strLine=strLine.simplified();

		strLine=strLine.trimmed();
		if (!strLine.isEmpty())
			strSchemaDB+=strLine+"\n";
	}

	strSchemaDB.chop(1); //remove last cr



	lstAddress.setData(z,nFormatAddrIdx,strSchemaDB);

}