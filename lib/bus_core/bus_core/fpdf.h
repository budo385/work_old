#ifndef FPOINTDEFFILE_H
#define FPOINTDEFFILE_H

// FPDF (function point definition file) is an .ini file that stores the several things:
//	- a list of available function points (FP)
//	- a tree that defines function point sets - FPS (for easier assignment of the group of settings to the user)
//	- a list of available modules and their assigned function point sets with default value specified for each FP
// One module defines one set of application permissions (for example "Business Edition")

//Internals:
//	- FP is defined

#include "treelist.h"

class FPDF 
{
public:
	FPDF();
	FPDF(const FPDF &other);
	void operator =(const FPDF &other);

	void Clear();
	bool IsEmpty();

	bool Load(const char *szFile);
	bool Save(const char *szFile);

	bool IsModified(){	return m_bModified; };

	bool AddFunctionPoint(const QString strName, const QString strDesc, int nCode, bool bActive = true, int nValue = 0, int nStatus = 1);
	bool AddFunctionPointSet(const QString strName, const QString strDesc, int nCode, int nStatus = 1, int nLevel = 0, int nType = ITEM_TYPE_FPS, int nValue = 0, bool bActive = true);

	bool AddModule(const QString strCode, const QString strName, const QString strDesc, int nCode, int nStatus, int nLevel = 0, int nType = ITEM_TYPE_MOD, int nValue = 0, bool bActive = true);
	bool CopyModule(int nIdx, const QString strCode, const QString strName);

	bool RemoveModule(int nIdx);

#ifdef _DEBUG
	void Dump();
	void AssertValid();
#endif

public:
	QString	 m_strFile;		//file name
	TreeList m_lstFP;		//list of function points
	TreeList m_lstFpSets;	//list of function point sets
	TreeList m_lstModSets;	//list of module sets
	
	bool m_bModified;	// "is dirty"
};

#endif	// FPOINTDEFFILE_H
