#include "fpsets.h"

bool FP_ITEM::operator == (const FP_ITEM &other)
{
	return (strName == other.strName && strCode == other.strCode);
}

void FP_ITEM::operator = (const FP_ITEM &other)
{
	nLevel	= other.nLevel;	
	nType	= other.nType;	
	nCode	= other.nCode;	
	strName	= other.strName;
	strCode	= other.strCode;
	strDesc	= other.strDesc;
	nStatus	= other.nStatus;
	nValue	= other.nValue;	
	bActive	= other.bActive;
}
