#ifndef REMINDERHELPERCORE_H
#define REMINDERHELPERCORE_H

#include "common/common/dbrecordset.h"
#define MAX_HR_BEFORE_EVENT 24
#define MAX_HR_AFTER_EVENT 24

class ReminderHelperCore
{
public:
	enum CommunicationEntityTypes
	{
		REMINDER_CATEGORY_CALENDAR=0,
		REMINDER_CATEGORY_TASK=1,
		REMINDER_CATEGORY_APPSERVER=2,
	};


	static void CreateDefaultCategorySettings(DbRecordSet &rowData);

private:
	
};

#endif // REMINDERHELPERCORE_H
