#include "usersessionmanager.h"
#include "common/common/config.h"
#include "common/common/datahelper.h"
#include <QDir>

int UserSessionManager::GetIsAliveInterval()
{
	return CLIENT_IS_ALIVE_INTERVAL;
}
int UserSessionManager::GetIsAliveCheckInterval()
{
	return CLIENT_IS_ALIVE_INTERVAL_CHECK;
}

void UserSessionManager::SetUserStorageDiskQuota(qint64 quota)
{
	m_nUserStorageServerDiskQuota=quota;
}
QString UserSessionManager::InitializeUserStorageDirectory(Status &status,QString strUserSubDir)
{
	status.setError(0);
	QString strUserDirectory=m_strUserStorageDirectoryPath+"/"+strUserSubDir;

	QDir dirUser(strUserDirectory);
	if (!dirUser.exists())
	{
		QDir dirMain(m_strUserStorageDirectoryPath); //must exists!
		if (!dirMain.exists())
		{
			status.setError(1,QString("Directory %1 does not exists! Service Unavailable!").arg(m_strUserStorageDirectoryPath));
			return "";
		}

		bool bOK=dirMain.mkdir(strUserSubDir);
		if (!bOK)
		{
			status.setError(1,QString("Failed to create user storage directory %1").arg(strUserSubDir));
			return "";
		}
	}

	return strUserDirectory;
}

bool UserSessionManager::IsEnoughDiskSpaceForUserStorage(qint64 nNewFileSize)
{
	//check total size (can be slow...):
	qint64 nCurrent=DataHelper::GetDiskUsage(m_strUserStorageDirectoryPath,true)+nNewFileSize;
	return nCurrent<m_nUserStorageServerDiskQuota;
	
	
}