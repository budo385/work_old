#ifndef COMMGRIDVIEWTABLEHELPER_H
#define COMMGRIDVIEWTABLEHELPER_H

#include <QObject>
#include <QHash>
#include <QDate>
#include <QTime>
#include "common/common/dbrecordset.h"
#include "bus_core/bus_core/commgridviewhelper.h"

class CommGridViewTableHelper : public QObject
{
	Q_OBJECT

public:
	CommGridViewTableHelper(QObject *parent=0);

	~CommGridViewTableHelper();

	virtual int GetLoggedPersonID()=0;
	virtual QString GetLoggedPersonInitials()=0;
	virtual void InitializeHelper()=0;

	void FirstColumnData(int nCEType, int &nRowsInGrid, DbRecordSet &recEntityRecord, int &nRow, bool &bSideBarMode);
	void SecondColumnData(int nCEType, DbRecordSet &recEntityRecord, int &nRow, int &nRowInGrid, bool &bSideBarMode);
	void ThirdColumnData(int nCEType, int &nRowsInGrid, DbRecordSet &recEntityRecord, int &nRow, bool &bSideBarMode);
	void FillRowHashes(int &nRow, int nCEType, DbRecordSet &recEntityRecord, int &nRowInList);
	void ClearHashes();
	void SortTableData(int nRowCount, int nGridType, DbRecordSet *recSortRecordSet = NULL, bool bForceSort = true);
	void CheckAndFixIcon3AndIcon4(DbRecordSet &recApplications,QHash<int, QString> &hshDocIconNames);

//private:
	//Data and Sorting hashes.
	QHash<int,int>				m_hshRow2CENT_ID;
	QHash<int,int>				m_hshRow2BTKS_ID;
	QHash<int,int>				m_hshRow2BTKS_SYSTEM_STATUS;
	QHash<int,int>				m_hshRow2BPER_ID;
	QHash<int,int>				m_hshRow2BCNT_ID;
	QHash<int,int>				m_hshRow2CENT_SYS;
	QHash<int,int>				m_hshRow2BUSP_ID;
	QHash<int,int>				m_hshRow2ENTITY_ID;
	QHash<int,int>				m_hshRow2BDMD_IS_CHECK_OUT;
	QHash<int,int>				m_hshRow2BEM_UNREAD_FLAG;
	QHash<int,int>				m_hshRow2BDMD_CHECK_OUT_USER_ID;
	QHash<int,QDate>			m_hshRow2DATE1;			//For sorting - first date shown in first column.
	QHash<int,QDate>			m_hshRow2DATE2;			//For sorting - second date shown in first column.
	QHash<int,int>				m_hshRow2STATUS;		//For sorting - first icon shown in first column (scheduled, not scheduled, ...).
	QHash<int,int>				m_hshRow2APPTYPE;		//For sorting - fourth icon shown in first column (application type).
	QHash<int,QString>			m_hshRow2ORIGINATOR;	//For sorting - originator shown in first column under dates for task.
	QHash<int,QString>			m_hshRow2OWNER;			//For sorting - owner shown in third column.
	QHash<int,QString>			m_hshRow2CONTACT;		//For sorting - contact shown in third column.
	QHash<int,QString>			m_hshRow2PROJECT;		//For sorting - project shown in third column.
	QHash<int,int>				m_hshRow2SUBTYPE;		//For sorting - second icon shown in first column (voice call, email, document).
	QHash<int,int>				m_hshRow2MEDIA;			//For sorting - third icon shown in first column (incoming, outgoing, inhouse).
	QHash<int,QTime>			m_hshRow2TIME1;			//For sorting - first date's time shown in first column.
	QHash<int,QTime>			m_hshRow2TIME2;			//For sorting - second date's time shown in first column.
	QHash<int,QString>			m_hshRow2SUBJECT_SORT;	//For sorting - first line shown in second column of pro view.
	QHash<int,int>				m_hshRow2BTKS_PRIORITY;	//For sorting - task priority.
	QHash<int,int>				m_hshRow2BTKS_TASK_TYPE_ID;	//For sorting - task type.

	//Sorting recordsets.
	DbRecordSet					m_recSortRecordSet;		//Sorting recordset.
	DbRecordSet					m_recSortByRecordset;	//Sorting recordset.

	//Hashes to pass to model;
	QHash<int,int>				m_hshRow2Color;
	QHash<int,QString>			m_hshRow2Pix1;
	QHash<int,QString>			m_hshRow2Pix2;
	QHash<int,QString>			m_hshRow2Pix3;
	QHash<int,QString>			m_hshRow2Pix4;
	QHash<int,QString>			m_hshRow2Date1;
	QHash<int,QString>			m_hshRow2Date2;
	QHash<int,QString>			m_hshRow2Autor;
	QHash<int,QString>			m_hshRow2Subject;
	QHash<int,QString>			m_hshRow2Descr;
	QHash<int,QString>			m_hshRow2SecondColDisplay;
	QHash<int,QString>			m_hshRow2Owner;
	QHash<int,QString>			m_hshRow2Contact;
	QHash<int,QString>			m_hshRow2Project;
	QHash<int,QString>			m_hshRow2TaskColor;
	QHash<int,QByteArray>		m_hshRow2TaskIcon;
	QHash<int,QByteArray>		m_hshRow2Pix4ByteArray;
	QHash<int,int>				m_hshRow2TaskPriority;

	CommGridViewHelper			*m_pCmGrdHelper;
};

#endif // COMMGRIDVIEWTABLEHELPER_H
