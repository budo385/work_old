#ifndef LICENSESTREAMWRITER_H
#define LICENSESTREAMWRITER_H

#define NOMINMAX
#include "common/common/rsakey.h"
#include <QString>

class LicenseStreamWriter
{
public:
	LicenseStreamWriter();
	~LicenseStreamWriter();

	void AttachSource(FILE *pInFile);
	void SetPrivateKey(const char *szKey);

	bool WriteBytes(void *pszBuffer, int nLen);
	bool WriteString(QString strData, int nLen);

	bool Write();	//flush data to file

protected:
	FILE *m_pOutFile;
	char m_szDecryptedData[4000];
	int	 m_nDataLen;
	RSA_Key	m_key;
};

#endif	// LICENSESTREAMREADER_H
