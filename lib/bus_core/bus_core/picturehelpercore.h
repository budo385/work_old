#ifndef PICTUREHELPERCORE_H
#define PICTUREHELPERCORE_H

#include "common/common/dbrecordset.h"
class PictureHelperCore 
{
public:
	static DbRecordSet PreProcessSavePictureIfNeeded(DbRecordSet &lstData,QString strFieldPictureId);
	static void PostProcessSavePictureIfNeeded(DbRecordSet &lstData,DbRecordSet &lstPictureWrite,QString strFieldPictureId);
};

#endif // PICTUREHELPERCORE_H
