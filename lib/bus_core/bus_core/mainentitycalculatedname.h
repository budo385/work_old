#ifndef MAINENTITYCALCULATEDNAME_H
#define MAINENTITYCALCULATEDNAME_H


#include "common/common/dbrecordset.h"


/*!
	\class MainEntityCalculatedName
	\brief Used to calculate name for person, contact,etc..
	\ingroup Bus_Core

*/

class MainEntityCalculatedName 
{

public:
    MainEntityCalculatedName();
    ~MainEntityCalculatedName();

	void Initialize (int nEntityID,DbRecordSet &rowData,int nCodeIdx=-1, int nNameIdx=-1);
	bool CanCalculate(){return m_bCanCalculate;};
	QString GetCalculatedName(DbRecordSet &rowData, int nCurrentRow=0); 
	void FillCalculatedNames(DbRecordSet &rowData, int nCalcName1_Idx,int nCalcName2_Idx=-1); 
	void InvalidateCalculatedNameFields(DbRecordSet &rowData, int nCurrentRow=0); 

	static QString GetCalculatedName(int nTableID,DbRecordSet &rowData, int nCurrentRow=0); 

private:
	//These functions describe rules how to assemble contact or project name
	static QString GetName_Hierarhical(QString strCode,QString strName);
	static QString GetName_Person(QString strLastName,QString strFirstName);
	static QString GetName_Contact(QString strCode,QString strName,QString strOrgName);

	int m_nEntityID;
	bool m_bCanCalculate;

	//Special names:
	int m_nFirstNameIdx;
	int m_nLastNameIdx;
	int m_nOrgNameIdx;
	int m_nDeptCodeIdx;
	int m_nDeptNameIdx;

	int m_nCodeIdx;
	int m_nNameIdx;


};

#endif // MAINENTITYCALCULATEDNAME_H
