#include "contacttypemanager.h"
#include "common/common/entity_id_collection.h"
#include "common/common/datahelper.h"
#include "db_core/db_core/dbsqltableview.h"
#include "formataddress.h"
#include "formatphone.h"
#include <QStringList>
#include "optionsandsettingsmanager.h"
#include "optionsandsettingsid.h"
#include "common/common/phonet.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "bus_core/bus_core/globalconstants.h"

/*!
	Get all type of types :)

	\param lstTypes - list of all embeded types
*/
void ContactTypeManager::GetEntityTypeList(DbRecordSet &lstTypes)
{
	lstTypes.destroy();
	lstTypes.addColumn(QVariant::Int,"ENTITY_TYPE_ID");
	lstTypes.addColumn(QVariant::String,"ENTITY_TYPE_NAME");

	//MB: 769..comment out
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_ADDRESS);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Address"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_LANGUAGE);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Language"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_SALUTATION);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Salutation"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_SHORT_SALUTATION);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Short Salutation"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_TITLE);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Title"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_EMAIL_ADDRESS);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Email Address"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_INTERNET);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Internet"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_PHONE);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Phone"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_TASK);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Task"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_CALENDAR_PRESENCE);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Presence"));
	lstTypes.addRow();
	lstTypes.setData(lstTypes.getRowCount()-1,0,TYPE_CALENDAR_CALENDAR_VIEW);lstTypes.setData(lstTypes.getRowCount()-1,1,QObject::tr("Calendar View"));
}


/*!
	Get all system types for one entity type

	\param lstTypes - list of all system types
*/
void ContactTypeManager::GetSystemTypeList(int nEntityTypeID,DbRecordSet &lstTypes)
{

	switch(nEntityTypeID)
	{
		case TYPE_PHONE:
			GetPhoneSystemTypes(lstTypes);
			break;
		case TYPE_EMAIL_ADDRESS:
			GetEmailSystemTypes(lstTypes);
			break;
		case TYPE_ADDRESS:
			GetAddressSystemTypes(lstTypes);
			break;
		default:
			//empty list:
			lstTypes.destroy();
			lstTypes.addColumn(QVariant::Int,"SYSTEM_TYPE_ID");
			lstTypes.addColumn(QVariant::String,"SYSTEM_TYPE_NAME");

			lstTypes.addRow();lstTypes.setData(0,0,SYSTEM_TYPE_EMPTY);lstTypes.setData(0,1,"");
			break;
	}

}



/*!
	Get phone system types

	\param lstTypes - list of all phone system types
*/
void ContactTypeManager::GetPhoneSystemTypes(DbRecordSet &lstTypes)
{
	lstTypes.destroy();
	lstTypes.addColumn(QVariant::Int,"SYSTEM_TYPE_ID");
	lstTypes.addColumn(QVariant::String,"SYSTEM_TYPE_NAME");

	lstTypes.addRow();lstTypes.setData(0,0,SYSTEM_TYPE_EMPTY);lstTypes.setData(0,1,"");
	lstTypes.addRow();lstTypes.setData(1,0,SYSTEM_PHONE_BUSINESS);lstTypes.setData(1,1,QObject::tr("Phone Business Central"));
	lstTypes.addRow();lstTypes.setData(2,0,SYSTEM_PHONE_PRIVATE);lstTypes.setData(2,1,QObject::tr("Phone Private"));
	lstTypes.addRow();lstTypes.setData(3,0,SYSTEM_PHONE_MOBILE);lstTypes.setData(3,1,QObject::tr("Phone Mobile"));
	lstTypes.addRow();lstTypes.setData(4,0,SYSTEM_PHONE_PRIVATE_MOBILE);lstTypes.setData(4,1,QObject::tr("Phone Private Mobile"));
	lstTypes.addRow();lstTypes.setData(5,0,SYSTEM_PAGER);lstTypes.setData(5,1,QObject::tr("Pager"));
	lstTypes.addRow();lstTypes.setData(6,0,SYSTEM_FAX);lstTypes.setData(6,1,QObject::tr("Fax"));
	lstTypes.addRow();lstTypes.setData(7,0,SYSTEM_SKYPE);lstTypes.setData(7,1,QObject::tr("Skype"));
	lstTypes.addRow();lstTypes.setData(8,0,SYSTEM_PHONE_DIRECT);lstTypes.setData(8,1,QObject::tr("Phone Business Direct"));
	lstTypes.addRow();lstTypes.setData(9,0,SYSTEM_PHONE_INTERNAL);lstTypes.setData(9,1,QObject::tr("Phone Internal"));
}


/*!
	Get email system types

	\param lstTypes - list of all email system types
*/
void ContactTypeManager::GetEmailSystemTypes(DbRecordSet &lstTypes)
{
	lstTypes.destroy();
	lstTypes.addColumn(QVariant::Int,"SYSTEM_TYPE_ID");
	lstTypes.addColumn(QVariant::String,"SYSTEM_TYPE_NAME");

	lstTypes.addRow();lstTypes.setData(0,0,SYSTEM_TYPE_EMPTY);lstTypes.setData(0,1,"");
	lstTypes.addRow();lstTypes.setData(1,0,SYSTEM_EMAIL_BUSINESS);lstTypes.setData(1,1,QObject::tr("Email Business"));
	lstTypes.addRow();lstTypes.setData(2,0,SYSTEM_EMAIL_PRIVATE);lstTypes.setData(2,1,QObject::tr("Email Private"));

}

/*!
	Get address system types

	\param lstTypes - list of all address system types
*/
void ContactTypeManager::GetAddressSystemTypes(DbRecordSet &lstTypes)
{
	lstTypes.destroy();
	lstTypes.addColumn(QVariant::Int,"SYSTEM_TYPE_ID");
	lstTypes.addColumn(QVariant::String,"SYSTEM_TYPE_NAME");

	lstTypes.addRow();lstTypes.setData(0,0,SYSTEM_TYPE_EMPTY);lstTypes.setData(0,1,"");
	lstTypes.addRow();lstTypes.setData(1,0,SYSTEM_ADDRESS_BUSINESS);lstTypes.setData(1,1,QObject::tr("Address Business"));
	lstTypes.addRow();lstTypes.setData(2,0,SYSTEM_ADDRESS_PRIVATE);lstTypes.setData(2,1,QObject::tr("Address Private"));

}


/*!
	Get address system types

	\param lstTypes			- list of all types
	\param lstSubData		- list of data with column with type id and BCMT_TYPE_NAME to fill
	\param nSubListTypeIDColIdx	- name of column with type id
	\param bAddressType		- if true then lstSubData contains address types
*/
void ContactTypeManager::AssignTypeName(DbRecordSet &lstTypes,DbRecordSet &lstSubData,QString strSubListTypeIDColIdx,bool bAddressType)
{

	int nSize=lstSubData.getRowCount();
	int nSubListTypeIDColIdx=lstSubData.getColumnIdx(strSubListTypeIDColIdx);
	int nSubListTypeNameColIdx=lstSubData.getColumnIdx("BCMT_TYPE_NAME");
	int nLstTypeNameColIdx=lstTypes.getColumnIdx("BCMT_TYPE_NAME");
	int nLstTypeIDColIdx=lstTypes.getColumnIdx("BCMT_ID");


	//if address
	if (bAddressType)
	{
		int nLstAddrTypeIDColIdx=-1;
		int nLstTypesColIdx=lstSubData.getColumnIdx("LST_TYPES");
				
		QString strTypeName;
		int nSize2;
		for(int i=0;i<nSize;++i)
		{
			DbRecordSet lstAddressTypes=lstSubData.getDataRef(i,nLstTypesColIdx).value<DbRecordSet>();
			if(nLstAddrTypeIDColIdx==-1)
				nLstAddrTypeIDColIdx=lstAddressTypes.getColumnIdx("BCMAT_TYPE_ID");
			
			strTypeName="";
			nSize2=lstAddressTypes.getRowCount();
			for(int k=0;k<nSize2;++k)
			{
				int nRowType=lstTypes.find(nLstTypeIDColIdx,lstAddressTypes.getDataRef(k,nLstAddrTypeIDColIdx),true);
				if (nRowType!=-1)
					strTypeName+=lstTypes.getDataRef(nRowType,nLstTypeNameColIdx).toString()+",";
			}
			lstSubData.setData(i,nSubListTypeNameColIdx,strTypeName.remove(strTypeName.size()-1,1)); //remove last ,
		}
	}
	else
	{
		//int nLstTypeIDColIdx=-1;
		//int nLstTypesColIdx=lstSubData.getColumnIdx("LST_TYPES");
		QString strTypeName;
		int nSize2;
		for(int i=0;i<nSize;++i)
		{
			int nTypeID=lstSubData.getDataRef(i,nSubListTypeIDColIdx).toInt();
			int nRowType=lstTypes.find(nLstTypeIDColIdx,nTypeID,true);
			if (nRowType!=-1)
				lstSubData.setData(i,nSubListTypeNameColIdx,lstTypes.getDataRef(nRowType,nLstTypeNameColIdx).toString());
		
		}


	}
	//lstSubData.Dump();
}


QVariant ContactTypeManager::GetDefaultTypeFromCache(int nEntityTypeID,DbRecordSet *data)
{
	QVariant varType(QVariant::Int); //NULL TYPE
	if(data==NULL)  return varType;

	//select all
	data->find(data->getColumnIdx("BCMT_ENTITY_TYPE"),nEntityTypeID);

	//select defaults:
	int nSelected=data->find(data->getColumnIdx("BCMT_IS_DEFAULT"),1,false,true,true);

	if(nSelected==1)
		return data->getDataRef(data->getSelectedRow(),"BCMT_ID");
	else
		return varType;
}


void ContactTypeManager::SetPhonets(DbRecordSet &lstContacts)
{
	int nSize=lstContacts.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strPhonet;
		if(!Phonet::ConvertString2Phonet(lstContacts.getDataRef(i,"BCNT_LASTNAME").toString(),strPhonet))
			strPhonet=lstContacts.getDataRef(i,"BCNT_LASTNAME").toString();
		lstContacts.setData(i,"BCNT_LASTNAME_PHONET",strPhonet);
		if(!Phonet::ConvertString2Phonet(lstContacts.getDataRef(i,"BCNT_FIRSTNAME").toString(),strPhonet))
			strPhonet=lstContacts.getDataRef(i,"BCNT_FIRSTNAME").toString();
		lstContacts.setData(i,"BCNT_FIRSTNAME_PHONET",strPhonet);
		if(!Phonet::ConvertString2Phonet(lstContacts.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString(),strPhonet))
			strPhonet=lstContacts.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString();
		lstContacts.setData(i,"BCNT_ORGANIZATIONNAME_PHONET",strPhonet);
		if(!Phonet::ConvertString2Phonet(lstContacts.getDataRef(i,"BCNT_LOCATION").toString(),strPhonet))
			strPhonet=lstContacts.getDataRef(i,"BCNT_LOCATION").toString();
		lstContacts.setData(i,"BCNT_LOCATION_PHONET",strPhonet);
	}
}




// converts list defined from TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT to TVIEW_BUS_CONTACT_FULL
// redefines lstContact 
void ContactTypeManager::ConvertContactListFromImport(DbRecordSet &lstContactTxtFormat,DbRecordSet &lstContact, QString strOrganizationNameForced,DbRecordSet *plstUsers)
{

	int nPersonCodeIdx=plstUsers->getColumnIdx("BPER_CODE");
	lstContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

	DbRecordSet lstPhones;
	lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE));
	lstPhones.addColumn(QVariant::String,"BCMT_TYPE_NAME");

	DbRecordSet lstEmails;
	lstEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL));
	lstEmails.addColumn(QVariant::String,"BCMT_TYPE_NAME");

	DbRecordSet lstInternet;
	lstInternet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET));
	lstInternet.addColumn(QVariant::String,"BCMT_TYPE_NAME");


	DbRecordSet lstAddress;
	lstAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
	DbRecordSet lstAddressTypes;
	lstAddressTypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT));
	lstAddress.addColumn(QVariant::String,"BCMT_TYPE_NAME");
	lstAddress.addColumn(DbRecordSet::GetVariantType(),"LST_TYPES");

	DbRecordSet lstJournal;
	lstJournal.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_JOURNAL));


	//define Idx to speed somewhat process:
	int nFirstNameColIdx=lstContact.getColumnIdx("BCNT_FIRSTNAME");
	int nTypeColIdx=lstContact.getColumnIdx("BCNT_TYPE");
	//int nAccessColIdx=lstContact.getColumnIdx("BCNT_ACCESS");
	int nSexColIdx=lstContact.getColumnIdx("BCNT_SEX");
	int nSupressColIdx=lstContact.getColumnIdx("BCNT_SUPPRESS_MAILING");

	int nOrgNameColIdx=lstContact.getColumnIdx("BCNT_ORGANIZATIONNAME");
	int nOrgShortNameColIdx=lstContact.getColumnIdx("BCNT_ORGANIZATIONNAME_2");
	int nLastNameColIdx=lstContact.getColumnIdx("BCNT_LASTNAME");
	int nLstPhonesIdx=lstContact.getColumnIdx("LST_PHONE");
	int nLstEmailsIdx=lstContact.getColumnIdx("LST_EMAIL");
	int nLstInternetIdx=lstContact.getColumnIdx("LST_INTERNET");
	int nLstAddressIdx=lstContact.getColumnIdx("LST_ADDRESS");
	int nLstJournalIdx=lstContact.getColumnIdx("LST_JOURNAL");

	int nLocation=lstContact.getColumnIdx("BCNT_LOCATION");
	int nCity=lstContactTxtFormat.getColumnIdx("BCMA_CITY");


	//merge all data
	//lstContact.merge(lstContactTxtFormat);

	//lstContactTxtFormat.Dump();

	int nRow=-1; //counter on exit list

	//loop to determine sublists: addr+phone+email+int+ // type + org
	int nSize=lstContactTxtFormat.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//------------------------------------------------
		//contact row (only if flag=0)
		//------------------------------------------------

		//check first flag:
		if (lstContactTxtFormat.getDataRef(i,0).toInt()==0) //CLC_IS_ADD_ON if 0 then add, else skip
		{
			lstContact.addRow();
			nRow++;

			DbRecordSet rowContact=lstContactTxtFormat.getRow(i);
			lstContact.assignRow(nRow,rowContact,true);

			//copy street to location:
			lstContact.setData(nRow,nLocation,lstContactTxtFormat.getDataRef(i,nCity).toString());

			//set default values:
			//lstContact.setData(nRow,nAccessColIdx,ContactTypeManager::ACC_RIGHT_PUBLIC_WRITE);		//public access right to contact
			if (lstContact.getDataRef(nRow,nSexColIdx).isNull()){	
				lstContact.setData(nRow,nSexColIdx,ContactTypeManager::SEX_MALE);					//public access right to contact
			}

			lstContact.setData(nRow,nSupressColIdx,0);												//public access right to contact

			if (lstContact.getDataRef(nRow,nFirstNameColIdx).toString().isEmpty() && lstContact.getDataRef(nRow,nLastNameColIdx).toString().isEmpty())
				lstContact.setData(nRow,nTypeColIdx,ContactTypeManager::CM_TYPE_ORGANIZATION);
			else
				lstContact.setData(nRow,nTypeColIdx,ContactTypeManager::CM_TYPE_PERSON);

			//set forced org only if organization is empty in import:
			//BCNT_ORGANIZATIONNAME
			if(lstContactTxtFormat.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty())
			{
				if(!strOrganizationNameForced.isEmpty() && lstContactTxtFormat.getDataRef(i,"BCMA_ORGANIZATIONNAME").toString().isEmpty())
					lstContact.setData(nRow,nOrgNameColIdx,strOrganizationNameForced);
				else
				{
					lstContact.setData(nRow,nOrgNameColIdx,lstContactTxtFormat.getDataRef(i,"BCMA_ORGANIZATIONNAME").toString());
					lstContact.setData(nRow,nOrgShortNameColIdx,lstContactTxtFormat.getDataRef(i,"BCMA_ORGANIZATIONNAME_2").toString());
				}
			}
			else
			{
				//org name = org name
				if(!strOrganizationNameForced.isEmpty())
					lstContact.setData(nRow,nOrgNameColIdx,strOrganizationNameForced);
				else
					lstContact.setData(nRow,nOrgNameColIdx,lstContactTxtFormat.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString());
			}



			//
		}
		else
		{
			if (nRow==-1)
			{
				//QMessageBox::information(NULL,QObject::tr("Invalid import file"),QObject::tr("Invalid import file. Please check values of first field. Import aborted!"));
				return;
			}
		}



		//------------------------------------------------
		//phones:
		//------------------------------------------------

		lstPhones.clear();
		QString strPhone;
		strPhone=lstContactTxtFormat.getDataRef(i,"CLC_PHONE_BUSINESS_CENTRAL").toString();
		if (!strPhone.isEmpty())
		{
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(nRow,"BCMP_NAME","Business");
		}

		strPhone=lstContactTxtFormat.getDataRef(i,"CLC_PHONE_BUSINESS_DIRECT").toString();
		if (!strPhone.isEmpty())
		{
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(nRow,"BCMP_NAME","Business");
		}
		strPhone=lstContactTxtFormat.getDataRef(i,"CLC_PHONE_MOBILE").toString();
		if (!strPhone.isEmpty())
		{
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(nRow,"BCMP_NAME","Mobile");
		}
		strPhone=lstContactTxtFormat.getDataRef(i,"CLC_PHONE_FAX").toString();
		if (!strPhone.isEmpty())
		{
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(nRow,"BCMP_NAME","Business Fax");
		}
		strPhone=lstContactTxtFormat.getDataRef(i,"CLC_PHONE_PRIVATE").toString();
		if (!strPhone.isEmpty())
		{
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(nRow,"BCMP_NAME","Home");
		}
		strPhone=lstContactTxtFormat.getDataRef(i,"CLC_PHONE_PRIVATE_MOBILE").toString();
		if (!strPhone.isEmpty())
		{
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(nRow,"BCMP_NAME","Mobile Private");
		}
		strPhone=lstContactTxtFormat.getDataRef(i,"CLC_PHONE_SKYPE").toString();
		if (!strPhone.isEmpty())
		{
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(nRow,"BCMP_NAME","Skype");
		}

		strPhone=lstContactTxtFormat.getDataRef(i,"CLC_PHONE_GENERIC").toString();
		if (!strPhone.isEmpty())
		{
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(nRow,"BCMP_NAME",lstContactTxtFormat.getDataRef(i,"CLC_PHONE_GENERIC_NAME").toString());
		}

		//merge phones:
		if (!lstContact.getDataRef(nRow,nLstPhonesIdx).isNull())
		{
			DbRecordSet lstPhonePrev=lstContact.getDataRef(nRow,nLstPhonesIdx).value<DbRecordSet>();
			lstPhones.merge(lstPhonePrev);
		}
		lstContact.setData(nRow,nLstPhonesIdx,lstPhones);





		//------------------------------------------------
		//emails:
		//------------------------------------------------
		lstEmails.clear();
		QString strEmail=lstContactTxtFormat.getDataRef(i,"CLC_EMAIL_ADDRESS").toString();
		if (!strEmail.isEmpty())
		{
			lstEmails.addRow();
			int nRow=lstEmails.getRowCount()-1;
			lstEmails.setData(nRow,"BCME_ADDRESS",strEmail);
			lstEmails.setData(nRow,"BCME_DESCRIPTION",lstContactTxtFormat.getDataRef(i,"CLC_EMAIL_ADDRESS_DESC").toString());
		}

		if (!lstContact.getDataRef(nRow,nLstEmailsIdx).isNull())
		{
			DbRecordSet lstPrev=lstContact.getDataRef(nRow,nLstEmailsIdx).value<DbRecordSet>();
			lstEmails.merge(lstPrev);
		}
		lstContact.setData(nRow,nLstEmailsIdx,lstEmails);


		//------------------------------------------------
		//internet:
		//------------------------------------------------
		lstInternet.clear();
		QString strInternet=lstContactTxtFormat.getDataRef(i,"CLC_WEB_ADDRESS").toString();
		if (!strInternet.isEmpty())
		{
			lstInternet.addRow();
			int nRow=lstInternet.getRowCount()-1;
			lstInternet.setData(nRow,"BCMI_ADDRESS",strInternet);
			lstInternet.setData(nRow,"BCMI_DESCRIPTION",lstContactTxtFormat.getDataRef(i,"CLC_WEB_ADDRESS_DESC").toString());
		}

		if (!lstContact.getDataRef(nRow,nLstInternetIdx).isNull())
		{
			DbRecordSet lstPrev=lstContact.getDataRef(nRow,nLstInternetIdx).value<DbRecordSet>();
			lstInternet.merge(lstPrev);
		}
		lstContact.setData(nRow,nLstInternetIdx,lstInternet);


		//------------------------------------------------
		//address:
		//------------------------------------------------
		lstAddress.clear();
		DbRecordSet rowAddress=lstContactTxtFormat.getRow(i);

		//rowAddress.Dump();

		//check if valid: must have at least street, city or country or formatted
		if (!(rowAddress.getDataRef(0,"BCMA_FIRSTNAME").toString().isEmpty() && 
			rowAddress.getDataRef(0,"BCMA_LASTNAME").toString().isEmpty() &&
			rowAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME").toString().isEmpty() &&
			rowAddress.getDataRef(0,"BCMA_STREET").toString().isEmpty() &&
			rowAddress.getDataRef(0,"BCMA_CITY").toString().isEmpty() &&
			rowAddress.getDataRef(0,"BCMA_ZIP").toString().isEmpty() 
			))
		{
			//copy first name & last name if empty:
			if (rowAddress.getDataRef(0,"BCMA_FIRSTNAME").toString().isEmpty())
				rowAddress.setData(0,"BCMA_FIRSTNAME",lstContact.getDataRef(nRow,nFirstNameColIdx));
			if (rowAddress.getDataRef(0,"BCMA_LASTNAME").toString().isEmpty())
				rowAddress.setData(0,"BCMA_LASTNAME",lstContact.getDataRef(nRow,nLastNameColIdx));

			lstAddress.merge(rowAddress);

			//lstAddress.Dump();
		}

		if (!lstContact.getDataRef(nRow,nLstAddressIdx).isNull())
		{
			DbRecordSet lstPrev=lstContact.getDataRef(nRow,nLstAddressIdx).value<DbRecordSet>();
			lstAddress.merge(lstPrev);
		}
		lstContact.setData(nRow,nLstAddressIdx,lstAddress);


		//------------------------------------------------
		//journal:
		//------------------------------------------------
		lstJournal.clear();
		DbRecordSet rowJournal=lstContactTxtFormat.getRow(i);

		//check if valid: must have at least street, city or country or formatted
		if (!(rowJournal.getDataRef(0,"BCMJ_PERSON_CODE").toString().isEmpty() && 
			rowJournal.getDataRef(0,"BCMJ_DATE").toString().isEmpty() &&
			rowJournal.getDataRef(0,"BCMJ_IS_PRIVATE").toString().isEmpty() 
			))
		{
			int nRowPers=plstUsers->find(nPersonCodeIdx,rowJournal.getDataRef(0,"BCMJ_PERSON_CODE"),true);
			if (nRowPers!=-1)
			{
				rowJournal.setData(0,"BCMJ_PERSON_ID",plstUsers->getDataRef(nRowPers,"BPER_ID"));
			}
			lstJournal.merge(rowJournal); //person can be null

		}
		if (!lstContact.getDataRef(nRow,nLstJournalIdx).isNull())
		{
			DbRecordSet lstPrev=lstContact.getDataRef(nRow,nLstJournalIdx).value<DbRecordSet>();
			lstJournal.merge(lstPrev);
		}
		lstContact.setData(nRow,nLstJournalIdx,lstJournal);

	}

	//parse CLC_GROUPS field...convert to LST_GROUPS
	ExtractGroups(lstContact);
}



//lstContact must be defined with TVIEW_BUS_CM_CONTACT_FULL and loaded with contacts from import source
//phones are formatted, types are set in NAME field->special mapper for every app.
//addresses are formatted if not already exists
//address country is set to CH :)
//on emails & internet set type based on name, if name is empty or non valid

//WARNING: before  entering plese set BCNT_TYPE to valid value 
void ContactTypeManager::PrepareContactListFromImport(DbRecordSet &lstContact, DbRecordSet *pTypeDataSet,DbRecordSet *recSettings, DbRecordSet *recOptions, int nPersonID,DbRecordSet *lstSchemas,bool bSkipPhoneTypeSet)
{
	FormatAddress m_AdrFormatter;
	FormatPhone m_PhoneFormatter;

	//loop through list, extract lists, prepare them
	int nPhoneLstIdx=lstContact.getColumnIdx("LST_PHONE");
	int nAddressLstIdx=lstContact.getColumnIdx("LST_ADDRESS");
	int nEmailLstIdx=lstContact.getColumnIdx("LST_EMAIL");
	int nInternetLstIdx=lstContact.getColumnIdx("LST_INTERNET");

	QVariant nDefaultAddresseType=GetDefaultType(ContactTypeManager::TYPE_ADDRESS,pTypeDataSet);
	QVariant nDefaultPhoneType=GetDefaultType(ContactTypeManager::TYPE_PHONE,pTypeDataSet);
	QVariant nDefaultEmailType=GetDefaultType(ContactTypeManager::TYPE_EMAIL_ADDRESS,pTypeDataSet);
	QVariant nDefaultItnernetType=GetDefaultType(ContactTypeManager::TYPE_INTERNET,pTypeDataSet);

	QVariant varBusinessType=GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS,pTypeDataSet);
	QVariant varBusinessFax=GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX,pTypeDataSet);
	QVariant varMobileType=GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE,pTypeDataSet);
	QVariant varHomeType=GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE,pTypeDataSet);
	QVariant varPager=GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PAGER,pTypeDataSet);
	QVariant varSkype=GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE,pTypeDataSet);
	QVariant varMobilePrivate=GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE,pTypeDataSet);

	//phone cols:
	DbRecordSet lstPhones;
	lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE));
	int nPhoneTypeCol=lstPhones.getColumnIdx("BCMP_TYPE_ID");
	int nPhoneNameCol=lstPhones.getColumnIdx("BCMP_NAME");
	int nPhoneCol=lstPhones.getColumnIdx("BCMP_FULLNUMBER");
	int nCountryCol=lstPhones.getColumnIdx("BCMP_COUNTRY");
	int nAreaCol=lstPhones.getColumnIdx("BCMP_AREA");
	int nLocalCol=lstPhones.getColumnIdx("BCMP_LOCAL");
	int nISOCol=lstPhones.getColumnIdx("BCMP_COUNTRY_ISO");
	int nSearchCol=lstPhones.getColumnIdx("BCMP_SEARCH");


	//address
	DbRecordSet lstAddressType;
	lstAddressType.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT));
	lstAddressType.addRow();
	lstAddressType.setData(0,"BCMAT_TYPE_ID",nDefaultAddresseType);

	QString strLangCode=GetTypeName(GetDefaultType(ContactTypeManager::TYPE_LANGUAGE,pTypeDataSet).toInt(),pTypeDataSet);

	OptionsAndSettingsManager settings;
	settings.SetSettingsDataSource(recSettings,nPersonID);
	settings.SetOptionsDataSource(recOptions);
	int nSchemaDefaultOrg=-1;
	int nSchemaDefaultPers=-1;
	nSchemaDefaultPers=settings.GetPersonSetting(CONTACT_DEF_ADDR_SCHEMA_PERSON).toInt();
	if (nSchemaDefaultPers<=0)
		nSchemaDefaultPers=settings.GetApplicationOption(APP_CONTACT_DEF_ADDR_SCHEMA_PERSON).toInt();
	nSchemaDefaultOrg=settings.GetPersonSetting(CONTACT_DEF_ADDR_SCHEMA_ORG).toInt();
	if (nSchemaDefaultOrg<=0)
		nSchemaDefaultOrg=settings.GetApplicationOption(APP_CONTACT_DEF_ADDR_SCHEMA_ORG).toInt();


	QTime time;
	time.start();

	int nSize=lstContact.getRowCount();
	for (int i=0;i<nSize;++i)
	{

		//---------------------------------------------------
		//email
		//---------------------------------------------------
		DbRecordSet lstEmails=lstContact.getDataRef(i,nEmailLstIdx).value<DbRecordSet>();
		//lstEmails.Dump();
		int nTypeCol=lstEmails.getColumnIdx("BCME_TYPE_ID");
		if (nTypeCol!=-1)
			lstEmails.setColValue(nTypeCol,nDefaultEmailType);
		lstEmails.setColValue(lstEmails.getColumnIdx("BCME_IS_DEFAULT"),0);
		if (lstEmails.getRowCount()>0)
			lstEmails.setData(0,"BCME_IS_DEFAULT",1); //set default to 1st
		lstContact.setData(i,nEmailLstIdx,lstEmails);

		//---------------------------------------------------
		//internet
		//---------------------------------------------------
		DbRecordSet lstInternet=lstContact.getDataRef(i,nInternetLstIdx).value<DbRecordSet>();
		//lstInternet.Dump();
		nTypeCol=lstInternet.getColumnIdx("BCMI_TYPE_ID");
		if (nTypeCol!=-1)
			lstInternet.setColValue(nTypeCol,nDefaultItnernetType);
		lstInternet.setColValue(lstInternet.getColumnIdx("BCMI_IS_DEFAULT"),0);
		if (lstInternet.getRowCount()>0)
			lstInternet.setData(0,"BCMI_IS_DEFAULT",1); //set default to 1st
		lstContact.setData(i,nInternetLstIdx,lstInternet);


		//---------------------------------------------------
		//address (set ch)
		//---------------------------------------------------
		DbRecordSet lstAddress=lstContact.getDataRef(i,nAddressLstIdx).value<DbRecordSet>();
		//lstAddress.Dump();
		int nCountryCode=lstAddress.getColumnIdx("BCMA_COUNTRY_CODE");
		int nCountryName=lstAddress.getColumnIdx("BCMA_COUNTRY_NAME");
		int nTypeList=lstAddress.getColumnIdx("LST_TYPES");
		//if (nCountryCode!=-1)
		//	lstAddress.setColValue(nCountryCode,"CH");
		//if (nCountryName!=-1)
		//	lstAddress.setColValue(nCountryName,"Schweiz");
		lstAddress.setColValue(lstAddress.getColumnIdx("BCMA_IS_DEFAULT"),0);
		if (lstAddress.getRowCount()>0)
			lstAddress.setData(0,"BCMA_IS_DEFAULT",1); //set default to 1st
		//lang:
		lstAddress.setColValue(lstAddress.getColumnIdx("BCMA_LANGUGAGE_CODE"),strLangCode);


		//format addresses:
		int nContType=lstContact.getDataRef(i,"BCNT_TYPE").toInt();
		//lstAddress.Dump();
		FormatAddressWithDefaultSchema(lstAddress,nContType,nSchemaDefaultPers,nSchemaDefaultOrg,nPersonID,lstSchemas); //format addresses only if not exists formatted address
		//lstAddress.Dump();

		//address types: fill list with default type:
		//lstAddressType.Dump();
		lstAddress.setColValue(nTypeList,lstAddressType);

		lstContact.setData(i,nAddressLstIdx,lstAddress);
		//lstAddress.Dump();
		//lstAddressType.Dump();

		//---------------------------------------------------
		//phones:
		//---------------------------------------------------
		DbRecordSet lstPhones=lstContact.getDataRef(i,nPhoneLstIdx).value<DbRecordSet>();
		//lstPhones.Dump();
		int nSize=lstPhones.getRowCount();
		for(int k=0;k<nSize;++k)
		{
			if (!bSkipPhoneTypeSet)
			{
				//determine type based on name
				if (lstPhones.getDataRef(k,nPhoneNameCol).toString()=="Business")
					lstPhones.setData(k,nPhoneTypeCol,varBusinessType);
				else if(lstPhones.getDataRef(k,nPhoneNameCol).toString()=="Business Fax")
					lstPhones.setData(k,nPhoneTypeCol,varBusinessFax);
				else if(lstPhones.getDataRef(k,nPhoneNameCol).toString()=="Mobile")
					lstPhones.setData(k,nPhoneTypeCol,varMobileType);
				else if(lstPhones.getDataRef(k,nPhoneNameCol).toString()=="Home")
					lstPhones.setData(k,nPhoneTypeCol,varHomeType);
				else if(lstPhones.getDataRef(k,nPhoneNameCol).toString()=="Pager")
					lstPhones.setData(k,nPhoneTypeCol,varPager);
				else if(lstPhones.getDataRef(k,nPhoneNameCol).toString()=="Mobile Private")
					lstPhones.setData(k,nPhoneTypeCol,varMobilePrivate);
				else if(lstPhones.getDataRef(k,nPhoneNameCol).toString()=="Skype")
				{
					//skip if skype, do not format:
					lstPhones.setData(k,nPhoneTypeCol,varSkype); //only set to search:
					lstPhones.setData(k,nSearchCol,lstPhones.getDataRef(k,nPhoneCol));
					continue;
				}
				else 
					lstPhones.setData(k,nPhoneTypeCol,nDefaultPhoneType);

				//if no type is defined, set defalt:
				if (lstPhones.getDataRef(k,nPhoneTypeCol).isNull())
					lstPhones.setData(k,nPhoneTypeCol,nDefaultPhoneType);
			}

			//calculate formated number:
			QString strPhone,strCountry,strLocal,strISO,strArea,strSearch;
			m_PhoneFormatter.SplitPhoneNumber(lstPhones.getDataRef(k,nPhoneCol).toString(),strCountry,strArea,strLocal,strSearch,strISO);
			lstPhones.setData(k,nPhoneCol,m_PhoneFormatter.FormatPhoneNumber(strCountry,strArea,strLocal));
			lstPhones.setData(k,nCountryCol,strCountry);
			lstPhones.setData(k,nAreaCol,strArea);
			lstPhones.setData(k,nLocalCol,strLocal);
			lstPhones.setData(k,nISOCol,strISO);
			lstPhones.setData(k,nSearchCol,strSearch);
		}

		lstPhones.setColValue(lstPhones.getColumnIdx("BCMP_IS_DEFAULT"),0);
		if (lstPhones.getRowCount()>0)
			lstPhones.setData(0,"BCMP_IS_DEFAULT",1); //set default to 1st

		//lstPhones.Dump();
		lstContact.setData(i,nPhoneLstIdx,lstPhones);


	}

	SetPhonets(lstContact);

}



/*!
	Extract groups info fill list lstContactGroups
	CLC_GROUPS field in format: //CLC_GROUPS = HBS::K.K.,HBS::K.R.{-},HBS::K.U.{1.01.2003-31.12.2003},HBS::K.V. .{ -31.12.2003},HBS::K.W.{1.01.2003-}
	lstContactGroups				 returned list in format: TVIEW_BUS_CM_GROUP+BGIT_EXTERNAL_CATEGORY

	\param RetOut_Data					- full contact list

*/
void ContactTypeManager::ExtractGroups(DbRecordSet &RetOut_Data)
{
	int nCntGroupsIdx=RetOut_Data.getColumnIdx("CLC_GROUPS");
	int nLstGroupsIdx=RetOut_Data.getColumnIdx("LST_GROUPS");


	//RetOut_Data.Dump();

	DbRecordSet lstContacGroup;
	lstContacGroup.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
	lstContacGroup.addColumn(QVariant::String,"BGIT_EXTERNAL_CATEGORY");



	int nSize=RetOut_Data.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstContacGroup.clear();
		//int nContactID=RetOut_Data.getDataRef(i,nCntIdx).toInt();
		//if (nContactID==0) continue;
		QString strgroups=RetOut_Data.getDataRef(i,nCntGroupsIdx).toString();
		if (strgroups.isEmpty()) continue;

		//parse CLC: (delimiter is coma)
		QStringList lstCategories=strgroups.split(",",QString::SkipEmptyParts);


		int nSize2=lstCategories.size();
		for(int k=0;k<nSize2;++k)
		{
			QString strGroupData=lstCategories.at(k).trimmed();
			QString strExternalCat;
			QDate from, to;

			//extract periods:
			int nPos1=strGroupData.indexOf("{");
			//issue 1611: can be without {}
			//if (nPos1==-1 ) 
			//	continue; //invalid group format, skip it
			lstContacGroup.addRow();
			int nRow=lstContacGroup.getRowCount()-1;


			if (nPos1>=0)
			{
				int nPos2=strGroupData.indexOf("}");
				if(nPos2<=nPos1) continue; //invalid entry
				QString strPeriod=strGroupData.mid(nPos1+1,nPos2-nPos1-1).trimmed();

				strExternalCat=strGroupData.left(nPos1).trimmed();

				int nPos3=strPeriod.indexOf("-");
				if (nPos3<0)continue; //invalid entry

				QString strFrom=strPeriod.left(nPos3);
				if (!strFrom.isEmpty())
					from=DataHelper::ParseDateString(strFrom);


				QString strTo=strPeriod.mid(nPos3+1,strPeriod.length()-nPos3-1);
				if (!strTo.isEmpty())
					to=DataHelper::ParseDateString(strTo);
			}
			else
				strExternalCat=strGroupData.trimmed();

			lstContacGroup.setData(nRow,"BGIT_EXTERNAL_CATEGORY",strExternalCat);
			lstContacGroup.setData(nRow,"BGCN_CMCA_VALID_FROM",from);
			lstContacGroup.setData(nRow,"BGCN_CMCA_VALID_TO",to);
		}

		//lstContacGroup.Dump();
		RetOut_Data.setData(i,nLstGroupsIdx,lstContacGroup);
	}

	//reset CLC_field
	RetOut_Data.setColValue(nCntGroupsIdx,"");
}



//gets default type from cache, if not in cache or no def type, return NULL variant
QVariant ContactTypeManager::GetDefaultType(int nEntityTypeID,DbRecordSet *data)
{
	QVariant varType(QVariant::Int); //NULL TYPE
	if(data==NULL){
		return varType;
	}

	//select all
	data->find(data->getColumnIdx("BCMT_ENTITY_TYPE"),nEntityTypeID);

	//select defaults:
	int nSelected=data->find(data->getColumnIdx("BCMT_IS_DEFAULT"),1,false,true,true);

	if(nSelected==1)
		return data->getDataRef(data->getSelectedRow(),"BCMT_ID");
	else
		return varType;
}

//based on system type, fetch user defined type:
QVariant ContactTypeManager::GetUserTypeBasedOnSystemType(int nEntityTypeID, int nSystemTypeID,DbRecordSet *data)
{
	QVariant varType(QVariant::Int); //NULL TYPE
	if(data==NULL){
		return varType;
	}

	//select all
	data->find(data->getColumnIdx("BCMT_ENTITY_TYPE"),nEntityTypeID);

	//select defaults:
	int nSelected=data->find(data->getColumnIdx("BCMT_SYSTEM_TYPE"),nSystemTypeID,false,true,true);
	if(nSelected==1)
		return data->getDataRef(data->getSelectedRow(),"BCMT_ID");
	else
		return varType;
}

QString ContactTypeManager::GetTypeName(int TypeID,DbRecordSet *data)
{
	if(data==NULL){
		return "";
	}

	//select all
	int nRow=data->find(data->getColumnIdx("BCMT_ID"),TypeID,true);
	if (nRow!=-1)
	{
		return data->getDataRef(nRow,"BCMT_TYPE_NAME").toString();
	}
	else
		return "";

}




bool ContactTypeManager::FormatAddressWithDefaultSchema(DbRecordSet &lstAddresses, int nContactType,int nSchemaDefaultPers,int nSchemaDefaultOrg, int nPersonID,DbRecordSet *lstSchemas)
{
	static FormatAddress m_AdrFormatter;
	int nSchema=-1;
	if (nContactType==ContactTypeManager::CM_TYPE_PERSON)
		nSchema=nSchemaDefaultPers;
	else
		nSchema=nSchemaDefaultOrg;


	if (nSchema<=0)
	{
		m_AdrFormatter.AddressFormat(lstAddresses,m_AdrFormatter.m_strDefaultSchema,true);
	}
	else
	{
		QString strSchema;
		//DbRecordSet *lstSchemas=m_SchemaHandler.GetDataSource();
		int nRow=lstSchemas->find(lstSchemas->getColumnIdx("BCMAS_ID"),nSchema,true);
		if(nRow!=-1)
			strSchema=lstSchemas->getDataRef(nRow,"BCMAS_SCHEMA").toString();
		else
			strSchema=m_AdrFormatter.m_strDefaultSchema;
		m_AdrFormatter.AddressFormat(lstAddresses,strSchema,true);
	}

	return true;
}


//creates empty & defined sub-lists for TVIEW_BUS_CONTACT_FULL defined row (not address types)
void ContactTypeManager::AddEmptyRowToFullContactList(DbRecordSet &data)
{
	data.addRow();
	int nRow=data.getRowCount()-1;

	DbRecordSet lstPhones;
	lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE_SELECT));
	DbRecordSet lstEmails;
	lstEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT));
	DbRecordSet lstInternet;
	lstInternet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET_SELECT));
	DbRecordSet lstAddress;
	lstAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT));
	//types: left empty
	//DbRecordSet lstAddressTypes;
	//lstAddressTypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES));
	DbRecordSet lstJournal;
	lstJournal.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_JOURNAL_SELECT));
	DbRecordSet lstGroups;
	lstGroups.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT));
	DbRecordSet lstPictures;
	lstPictures.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PICTURE));
	DbRecordSet lstCreditor;
	lstCreditor.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CREDITOR));
	DbRecordSet lstDebtor;
	lstDebtor.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_DEBTOR));

	data.setData(nRow,"LST_PHONE",lstPhones);
	data.setData(nRow,"LST_ADDRESS",lstAddress);
	data.setData(nRow,"LST_EMAIL",lstEmails);
	data.setData(nRow,"LST_INTERNET",lstInternet);
	data.setData(nRow,"LST_JOURNAL",lstJournal);
	data.setData(nRow,"LST_GROUPS",lstGroups);
	data.setData(nRow,"LST_PICTURES",lstPictures);
	data.setData(nRow,"LST_CREDITOR",lstCreditor);
	data.setData(nRow,"LST_DEBTOR",lstDebtor);

}

DbRecordSet ContactTypeManager::GetContactGroupsForCopy()
{
	DbRecordSet data;
	data.addColumn(QVariant::Int,"GROUP_ID");
	data.addColumn(QVariant::String,"GROUP_NAME");
	data.addRow();
	data.setData(0,0,QVariant(GROUP_PHONE).toInt());
	data.setData(0,1,QObject::tr("Phones"));
	data.addRow();
	data.setData(1,0,QVariant(GROUP_ADDRESS).toInt());
	data.setData(1,1,QObject::tr("Addresses"));
	data.addRow();
	data.setData(2,0,QVariant(GROUP_EMAIL).toInt());
	data.setData(2,1,QObject::tr("Emails"));
	data.addRow();
	data.setData(3,0,QVariant(GROUP_INTERNET).toInt());
	data.setData(3,1,QObject::tr("Internet addresses"));
	data.addRow();
	data.setData(4,0,QVariant(GROUP_PICTURE).toInt());
	data.setData(4,1,QObject::tr("Pictures"));
	//data.addRow();
	//data.setData(0,GROUP_CREDITOR);
	//data.setData(0,QObject::tr("Creditor data"));
	data.addRow();
	data.setData(5,0,QVariant(GROUP_DEBTOR).toInt());
	data.setData(5,1,QObject::tr("Debtor data"));
	data.addRow();
	data.setData(6,0,QVariant(GROUP_CONTACT_GROUP_ASSIGN).toInt());
	data.setData(6,1,QObject::tr("Contact Groups"));
	data.addRow();
	data.setData(7,0,QVariant(GROUP_CONTACT_CUSTOM_FIELDS).toInt());
	data.setData(7,1,QObject::tr("Custom Fields"));
	return data;
}

void ContactTypeManager::AssembleSQLQueryForSearch(QString &strSQLWhereJoin,QString &strSQLWhere,QString strField1_Name, int nDataTypeCustomFld1, QString strField1_Value1, QString strField1_Value2, QString strField2_Name, int nDataTypeCustomFld2, QString strField2_Value1, QString strField2_Value2, bool bIgnoreCase)
{
	DbView viewContact=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT);

	if (!strField1_Value1.isEmpty())
	{
		if (nDataTypeCustomFld1>=0)
		{
			switch (nDataTypeCustomFld1)
			{
			case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
				{
					strSQLWhereJoin=" INNER JOIN BUS_CUSTOM_BOOL ON BCB_RECORD_ID = BCNT_ID AND BCB_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField1_Name = "BCB_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_INT:
				{
					strSQLWhereJoin=" INNER JOIN BUS_CUSTOM_INT ON BCI_RECORD_ID = BCNT_ID AND BCI_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField1_Name = "BCI_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
				{
					strSQLWhereJoin=" INNER JOIN BUS_CUSTOM_FLOAT ON BCFL_RECORD_ID = BCNT_ID AND BCFL_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField1_Name = "BCFL_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
				{
					strSQLWhereJoin=" INNER JOIN BUS_CUSTOM_TEXT ON BCT_RECORD_ID = BCNT_ID AND BCT_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField1_Name = "BCT_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
				{
					strSQLWhereJoin=" INNER JOIN BUS_CUSTOM_TEXT ON BCLT_RECORD_ID = BCNT_ID AND BCLT_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField1_Name = "BCLT_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
				{
					strSQLWhereJoin=" INNER JOIN BUS_CUSTOM_DATE ON BCD_RECORD_ID = BCNT_ID AND BCD_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField1_Name = "BCD_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
				{
					strSQLWhereJoin=" INNER JOIN BUS_CUSTOM_DATETIME ON BCDT_RECORD_ID = BCNT_ID AND BCDT_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField1_Name = "BCDT_VALUE";
				}
				break;
			}

			strSQLWhere=GetFilterBasedOnFieldTypeCustomFld(strField1_Name,nDataTypeCustomFld1,strField1_Value1,strField1_Value2,bIgnoreCase);
		}
		else
		{
			if(strField1_Name.left(4)=="BCMA")
				strSQLWhereJoin=" INNER JOIN bus_cm_address ON BCMA_CONTACT_ID = BCNT_ID ";
			else if(strField1_Name.left(4)=="BCMP")
				strSQLWhereJoin=" INNER JOIN bus_cm_phone ON BCMP_CONTACT_ID = BCNT_ID ";
			else if(strField1_Name.left(4)=="BCMI")
				strSQLWhereJoin=" INNER JOIN bus_cm_internet ON BCMI_CONTACT_ID = BCNT_ID ";
			else if(strField1_Name.left(4)=="BCME")
				strSQLWhereJoin=" INNER JOIN bus_cm_email ON BCME_CONTACT_ID = BCNT_ID ";
			else if(strField1_Name.left(4)=="BGCN")
				strSQLWhereJoin=" INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID ";
			else if(strField1_Name.left(4)=="BCMD")
				strSQLWhereJoin=" INNER JOIN bus_cm_debtor ON BCMD_CONTACT_ID = BCNT_ID ";

			strSQLWhere=DataHelper::GetFilterBasedOnFieldType(viewContact,strField1_Name,strField1_Value1,strField1_Value2,bIgnoreCase);
		}


	}

	if (!strField2_Value1.isEmpty())
	{
		if (nDataTypeCustomFld2>=0)
		{
			switch (nDataTypeCustomFld2)
			{
			case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
				{
					strSQLWhereJoin+=" INNER JOIN BUS_CUSTOM_BOOL ON BCB_RECORD_ID = BCNT_ID AND BCB_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField2_Name = "BCB_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_INT:
				{
					strSQLWhereJoin+=" INNER JOIN BUS_CUSTOM_INT ON BCI_RECORD_ID = BCNT_ID AND BCI_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField2_Name = "BCI_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
				{
					strSQLWhereJoin+=" INNER JOIN BUS_CUSTOM_FLOAT ON BCFL_RECORD_ID = BCNT_ID AND BCFL_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField2_Name = "BCFL_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
				{
					strSQLWhereJoin+=" INNER JOIN BUS_CUSTOM_TEXT ON BCT_RECORD_ID = BCNT_ID AND BCT_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField2_Name = "BCT_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
				{
					strSQLWhereJoin+=" INNER JOIN BUS_CUSTOM_TEXT ON BCLT_RECORD_ID = BCNT_ID AND BCLT_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField2_Name = "BCLT_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
				{
					strSQLWhereJoin+=" INNER JOIN BUS_CUSTOM_DATE ON BCD_RECORD_ID = BCNT_ID AND BCD_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField2_Name = "BCD_VALUE";
				}
				break;
			case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
				{
					strSQLWhereJoin+=" INNER JOIN BUS_CUSTOM_DATETIME ON BCDT_RECORD_ID = BCNT_ID AND BCDT_TABLE_ID="+QString::number(BUS_CM_CONTACT);
					strField2_Name = "BCDT_VALUE";
				}
				break;
			}

			if (strSQLWhere.isEmpty())
				strSQLWhere=GetFilterBasedOnFieldTypeCustomFld(strField2_Name,nDataTypeCustomFld2,strField2_Value1,strField2_Value2,bIgnoreCase);
			else
				strSQLWhere+=" AND "+ GetFilterBasedOnFieldTypeCustomFld(strField2_Name,nDataTypeCustomFld2,strField2_Value1,strField2_Value2,bIgnoreCase);

		}
		else
		{

			if(strField2_Name.left(4)=="BCMA")
				strSQLWhereJoin+=" INNER JOIN bus_cm_address ON BCMA_CONTACT_ID = BCNT_ID ";
			else if(strField2_Name.left(4)=="BCMP")
				strSQLWhereJoin+=" INNER JOIN bus_cm_phone ON BCMP_CONTACT_ID = BCNT_ID ";
			else if(strField2_Name.left(4)=="BCMI")
				strSQLWhereJoin+=" INNER JOIN bus_cm_internet ON BCMI_CONTACT_ID = BCNT_ID ";
			else if(strField2_Name.left(4)=="BCME")
				strSQLWhereJoin+=" INNER JOIN bus_cm_email ON BCME_CONTACT_ID = BCNT_ID ";
			else if(strField2_Name.left(4)=="BGCN")
				strSQLWhereJoin=" INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID ";
			else if(strField2_Name.left(4)=="BCMD")
				strSQLWhereJoin=" INNER JOIN bus_cm_debtor ON BCMD_CONTACT_ID = BCNT_ID ";

			if (strSQLWhere.isEmpty())
				strSQLWhere=DataHelper::GetFilterBasedOnFieldType(viewContact,strField2_Name,strField2_Value1,strField2_Value2,bIgnoreCase);
			else
				strSQLWhere+=" AND "+ DataHelper::GetFilterBasedOnFieldType(viewContact,strField2_Name,strField2_Value1,strField2_Value2,bIgnoreCase);
		}
	}

}

void ContactTypeManager::SortContactList(DbRecordSet &lstContacts, int nSortOrder /* 0 -asc, 1- desc */)
{
	//store last:
	SortDataList lstSort;
	//asc
	if (nSortOrder==0)
	{
		lstSort<<SortData(lstContacts.getColumnIdx("BCNT_ORGANIZATIONNAME"),0);
		lstSort<<SortData(lstContacts.getColumnIdx("BCNT_LASTNAME"),0);
		lstSort<<SortData(lstContacts.getColumnIdx("BCNT_FIRSTNAME"),0);
	}
	else
	{
		lstSort<<SortData(lstContacts.getColumnIdx("BCNT_ORGANIZATIONNAME"),1);
		lstSort<<SortData(lstContacts.getColumnIdx("BCNT_LASTNAME"),1);
		lstSort<<SortData(lstContacts.getColumnIdx("BCNT_FIRSTNAME"),1);
	}

	lstContacts.sortMulti(lstSort);
}



//generate search string:
QString ContactTypeManager::GetFilterBasedOnFieldTypeCustomFld(QString strColName, int nDataType, QString strColValue1, QString strColValue2, bool bIgnoreCase)
{
	QString strSQLWhere;

	switch(nDataType)
	{
	case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
	case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
		{
			DataHelper::getDateSqlString(strSQLWhere, strColName, strColValue1, strColValue2);
			break;
		}
	case GlobalConstants::TYPE_CUSTOM_DATA_INT:
	case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
	case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
	strSQLWhere+=strColName+" = "+strColValue1;
		break;
	default:
		DataHelper::getCharacterSqlStringCustomFld(strSQLWhere, strColName, strColValue1, strColValue2, bIgnoreCase);
		break;
	}

	return strSQLWhere;

}
