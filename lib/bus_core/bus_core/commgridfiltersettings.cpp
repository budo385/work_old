#include "commgridfiltersettings.h"
#include "commfiltersettingsids.h"
#include "db_core/db_core/dbsqltableview.h"

CommGridFilterSettings::CommGridFilterSettings()
{
	m_recCommFilterSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
}

CommGridFilterSettings::~CommGridFilterSettings()
{

}

DbRecordSet CommGridFilterSettings::GetCommFilterSettings()
{
	m_recCommFilterSettings.clear();
	InitializeCommFilterSettings();
	return m_recCommFilterSettings;
}

DbRecordSet CommGridFilterSettings::GetCalendarTasksFilterSettings()
{
	m_recCommFilterSettings.clear();
	InitializeCalendarTaskFilterSettings();
	return m_recCommFilterSettings;
}

DbRecordSet CommGridFilterSettings::GetCalendarGridTasksFilterSettings()
{
	m_recCommFilterSettings.clear();
	InitializeCalendarGridTaskFilterSettings();
	return m_recCommFilterSettings;
}

void CommGridFilterSettings::InitializeCommFilterSettings()
{
	//********************************************************************
	//**				VIEW_SELECTION_GRID_ACTIVE						**
	//********************************************************************
	int row = 0;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, VIEW_SELECTION_GRID_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.
	
	//********************************************************************
	//**				SHOW_VOICE_CALLS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_UNASSIGNED);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				SHOW_VOICE_CALLS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_MISSED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				SHOW_VOICE_CALLS_SCHEDULED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_SCHEDULED);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_VOICE_CALLS_DUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_DUE);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_VOICE_CALLS_OVERDUE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_OVERDUE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_VOICE_CALLS_LAST					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_LAST);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						LAST_VOICE_CALLS_NUMBER					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_VOICE_CALLS_NUMBER);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_UNREAD						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_UNREAD);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_UNASSIGNED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_SCHEDULED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_SCHEDULED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_DUE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_DUE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_OVERDUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_OVERDUE);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_TEMPLATES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_TEMPLATES);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_LAST							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_LAST);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					LAST_EMAILS_NUMBER							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_EMAILS_NUMBER);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					DOCUMENTS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_UNASSIGNED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					DOCUMENTS_SCHEDULED							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_SCHEDULED);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_DUE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_DUE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_OVERDUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_OVERDUE);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_TEMPLATES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_TEMPLATES);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							DOCUMENTS_LAST						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_LAST);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					LAST_DOCUMENTS_NUMBER						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_DOCUMENTS_NUMBER);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						FILTER_BY_DATE_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_DATE_ACTIVE);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							FROM_DATE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FROM_DATE);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 4);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);							//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							TO_DATE								**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TO_DATE);							//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 4);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);							//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DATE_RANGE_SELECTOR						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DATE_RANGE_SELECTOR);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, -1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				FILTER_BY_ENTITY_TYPE_ACTIVE				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_ENTITY_TYPE_ACTIVE);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_DOCUMENTS							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_DOCUMENTS);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SHOW_EMAILS							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_VOICE_CALLS						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**			FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE); //Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						APPLICATION_TYPES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, APPLICATION_TYPES);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					FILTER_SEARCH_TEXT_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_SEARCH_TEXT_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SEARCH_IN_NAME						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_IN_NAME);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SEARCH_IN_DESCRIPTION						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_IN_DESCRIPTION);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SEARCH_TEXT							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_TEXT);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 2);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**						VIEW_SORT_CODE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, VIEW_SORT_CODE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 2);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				FILTER_BY_CE_ENTITY_TYPE_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_CE_ENTITY_TYPE_ACTIVE);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				SELECTED_COMBO_CE_ENTITY_TYPE_FILTER			**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SELECTED_COMBO_CE_ENTITY_TYPE_FILTER);//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_DOCUMENTS					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_DOCUMENTS);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_VOICE_CALLS				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_VOICE_CALLS);//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_EMAILS					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_EMAILS);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				FILTER_BY_TASK_TYPE_ACTIVE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_TASK_TYPE_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_PRIORITY_FROM							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_PRIORITY_FROM);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_PRIORITY_TO							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_PRIORITY_TO);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 9);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_TYPE_FILTER							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_TYPE_FILTER);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
}

void CommGridFilterSettings::InitializeCalendarTaskFilterSettings()
{
	//********************************************************************
	//**				VIEW_SELECTION_GRID_ACTIVE						**
	//********************************************************************
	int row = 0;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, VIEW_SELECTION_GRID_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.
	
	//********************************************************************
	//**				SHOW_VOICE_CALLS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_UNASSIGNED);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				SHOW_VOICE_CALLS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_MISSED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				SHOW_VOICE_CALLS_SCHEDULED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_SCHEDULED);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_VOICE_CALLS_DUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_DUE);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_VOICE_CALLS_OVERDUE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_OVERDUE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_VOICE_CALLS_LAST					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_LAST);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						LAST_VOICE_CALLS_NUMBER					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_VOICE_CALLS_NUMBER);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_UNREAD						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_UNREAD);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_UNASSIGNED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_SCHEDULED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_SCHEDULED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_DUE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_DUE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_OVERDUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_OVERDUE);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_TEMPLATES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_TEMPLATES);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_LAST							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_LAST);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					LAST_EMAILS_NUMBER							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_EMAILS_NUMBER);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					DOCUMENTS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_UNASSIGNED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					DOCUMENTS_SCHEDULED							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_SCHEDULED);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_DUE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_DUE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_OVERDUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_OVERDUE);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_TEMPLATES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_TEMPLATES);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							DOCUMENTS_LAST						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_LAST);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					LAST_DOCUMENTS_NUMBER						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_DOCUMENTS_NUMBER);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						FILTER_BY_DATE_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_DATE_ACTIVE);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							FROM_DATE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FROM_DATE);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 4);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);							//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							TO_DATE								**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TO_DATE);							//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 4);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);							//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DATE_RANGE_SELECTOR						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DATE_RANGE_SELECTOR);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, -1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				FILTER_BY_ENTITY_TYPE_ACTIVE				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_ENTITY_TYPE_ACTIVE);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_DOCUMENTS							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_DOCUMENTS);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SHOW_EMAILS							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_VOICE_CALLS						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**			FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE); //Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						APPLICATION_TYPES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, APPLICATION_TYPES);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					FILTER_SEARCH_TEXT_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_SEARCH_TEXT_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SEARCH_IN_NAME						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_IN_NAME);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SEARCH_IN_DESCRIPTION						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_IN_DESCRIPTION);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SEARCH_TEXT							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_TEXT);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 2);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**						VIEW_SORT_CODE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, VIEW_SORT_CODE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 2);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				FILTER_BY_CE_ENTITY_TYPE_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_CE_ENTITY_TYPE_ACTIVE);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				SELECTED_COMBO_CE_ENTITY_TYPE_FILTER			**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SELECTED_COMBO_CE_ENTITY_TYPE_FILTER);//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_DOCUMENTS					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_DOCUMENTS);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_VOICE_CALLS				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_VOICE_CALLS);//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_EMAILS					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_EMAILS);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				FILTER_BY_TASK_TYPE_ACTIVE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_TASK_TYPE_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_PRIORITY_FROM							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_PRIORITY_FROM);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_PRIORITY_TO							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_PRIORITY_TO);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 9);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_TYPE_FILTER							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_TYPE_FILTER);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
}

void CommGridFilterSettings::InitializeCalendarGridTaskFilterSettings()
{
	//********************************************************************
	//**				VIEW_SELECTION_GRID_ACTIVE						**
	//********************************************************************
	int row = 0;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, VIEW_SELECTION_GRID_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.
	
	//********************************************************************
	//**				SHOW_VOICE_CALLS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_UNASSIGNED);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				SHOW_VOICE_CALLS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_MISSED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				SHOW_VOICE_CALLS_SCHEDULED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_SCHEDULED);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_VOICE_CALLS_DUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_DUE);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_VOICE_CALLS_OVERDUE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_OVERDUE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_VOICE_CALLS_LAST					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS_LAST);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						LAST_VOICE_CALLS_NUMBER					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_VOICE_CALLS_NUMBER);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_UNREAD						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_UNREAD);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_UNASSIGNED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_SCHEDULED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_SCHEDULED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_DUE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_DUE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_EMAILS_OVERDUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_OVERDUE);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_TEMPLATES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_TEMPLATES);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SHOW_EMAILS_LAST							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS_LAST);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					LAST_EMAILS_NUMBER							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_EMAILS_NUMBER);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					DOCUMENTS_UNASSIGNED						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_UNASSIGNED);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					DOCUMENTS_SCHEDULED							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_SCHEDULED);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_DUE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_DUE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_OVERDUE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_OVERDUE);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DOCUMENTS_TEMPLATES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_TEMPLATES);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							DOCUMENTS_LAST						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DOCUMENTS_LAST);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					LAST_DOCUMENTS_NUMBER						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, LAST_DOCUMENTS_NUMBER);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 10);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						FILTER_BY_DATE_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_DATE_ACTIVE);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							FROM_DATE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FROM_DATE);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 4);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);							//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							TO_DATE								**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TO_DATE);							//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 4);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);							//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						DATE_RANGE_SELECTOR						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, DATE_RANGE_SELECTOR);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - datetime).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, -1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**				FILTER_BY_ENTITY_TYPE_ACTIVE				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_ENTITY_TYPE_ACTIVE);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_DOCUMENTS							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_DOCUMENTS);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SHOW_EMAILS							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_EMAILS);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						SHOW_VOICE_CALLS						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SHOW_VOICE_CALLS);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**			FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE); //Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**						APPLICATION_TYPES						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, APPLICATION_TYPES);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					FILTER_SEARCH_TEXT_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_SEARCH_TEXT_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SEARCH_IN_NAME						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_IN_NAME);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**					SEARCH_IN_DESCRIPTION						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_IN_DESCRIPTION);			//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_DATETIME", QByteArray());					//Value for date time.

	//********************************************************************
	//**							SEARCH_TEXT							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SEARCH_TEXT);						//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 2);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**						VIEW_SORT_CODE							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, VIEW_SORT_CODE);					//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 2);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 1);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				FILTER_BY_CE_ENTITY_TYPE_ACTIVE					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_CE_ENTITY_TYPE_ACTIVE);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				SELECTED_COMBO_CE_ENTITY_TYPE_FILTER			**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, SELECTED_COMBO_CE_ENTITY_TYPE_FILTER);//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_DOCUMENTS					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_DOCUMENTS);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_VOICE_CALLS				**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_VOICE_CALLS);//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				CE_ENTITY_TYPE_FILTER_EMAILS					**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, CE_ENTITY_TYPE_FILTER_EMAILS);	//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**				FILTER_BY_TASK_TYPE_ACTIVE						**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, FILTER_BY_TASK_TYPE_ACTIVE);		//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 1);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_PRIORITY_FROM							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_PRIORITY_FROM);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_PRIORITY_TO							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_PRIORITY_TO);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 0);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	m_recCommFilterSettings.setData(row, "BUSCS_VALUE"			, 9);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.

	//********************************************************************
	//**					TASK_TYPE_FILTER							**
	//********************************************************************
	row++;
	m_recCommFilterSettings.addRow();
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_ID"		, TASK_TYPE_FILTER);				//Settings id.
	m_recCommFilterSettings.setData(row, "BUSCS_SETTING_TYPE"	, 3);								//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary, 4 - date time).
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE"		, 0);								//Value for int or bool.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_STRING"	, "");								//Value for string.
	//m_recCommFilterSettings.setData(row, "BUSCS_VALUE_BYTE"	, QByteArray());					//Value for byte array.
}
