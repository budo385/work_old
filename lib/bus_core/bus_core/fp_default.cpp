//
// WARNING: This is a computer generated file. Do not edit!
//	Generated from: D:/SOKRATES.fpdf
//	Created on:     2013.02.06. 11:56:51
//

#include "fp_default.h"
#include <string.h>


int FindEntry(int nFP);

//
// Array with module info
//
typedef struct {
	int nCode;
	const char *szCode;
} MOD_INFO;

MOD_INFO g_arModInfo[COUNT_APP_MODULES] =
{

{APP_MODULE_SOKRATES_COMMUNICATOR_TEAM_EDITION, "SC-TE"},
{APP_MODULE_SOKRATES_COMMUNICATOR_PERSONAL_EDITION, "SC-PE"},
{APP_MODULE_SOKRATES_COMMUNICATOR_BUSINESS_EDITION, "SC-BE"},
{APP_MODULE_SOKRATES_COMMUNICATOR_ENTERPRISE_EDITION, "SC-EE"},
{APP_MODULE_SOKRATES_PROJECT_CONTROL_PM_PRO, "SPM-PRO"},
{APP_MODULE_SOKRATES_PROJECT_CONTROL_PRO, "SPC-PRO"},

};

//
// Array with Default Access Rights (per FP)
//
//
// NOTE: Array fields schema:
//	{code, can_use, value}
//

typedef struct {
	bool bCanUse;
	int nValue;
} FP_MOD_STATE;

typedef struct {
	int nCode;
	FP_MOD_STATE arState[COUNT_APP_MODULES];
} FP_ITEM1;

FP_ITEM1 g_arAccessRights[]=
{

{FP_USER_FUI,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_USER_NUMBER_LIMITATION,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PROJECT_FUI,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PROJECT_NUMBER_LIMITATION,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_TELEPHONY_SKYPE_CALL,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_TELEPHONY_SKYPE_CALL_TRANSFER,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_TELEPHONY_SKYPE_LISTEN_AND_ANSWER,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_TELEPHONY_SKYPE_CALL_REGISTRATION,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{1, 0}}},
{FP_TELEPHONY_OTHER_CALL_REGISTRATION,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{1, 0}}},
{FP_TELEPHONY_LIMITATION_OF_REGISTERED_PHONE_CALLS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{1, 0}}},
{FP_DM_INTERNET,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_DM_PAPER,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_DM_LOCAL,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_DM_ADDRESS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_DM_NOTE,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CONTACTS_FUI,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CONTACTS_NUMBER_LIMITATION,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_THIN_CLIENT_ACCESS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_THICK_CLIENT_ACCESS,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_DOCUMENT_TEMPLATES,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EMAIL_TEMPLATES,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_TASKS,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EMAILS_OUTLOOK,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EMAILS_VIEW,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_ACCESS_RIGHTS_FOR_SELECTED_USERS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_DEPARTMENT_FUI,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_ORGANIZATION_FUI,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_ORGANIZATION_NUMBER_LIMITATION,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_COST_CENTER_FUI,{{1, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_INVOICING_INFORMATION,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__INVOICE_ATTACHMENT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__CONTACT_DETAILS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__CONTACT_LIST,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__CONTACT_COMMUNICATION_DETAILS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__PROJECT_COMMUNICATION_DETAILS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__PROJECT_LIST,{{1, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__PROJECT_DETAILS,{{0, 0},{1, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__PERSON_DETAILS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__PERSONAL_COMMUNICATION_DETAILS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__LIST_OF_PERSONS,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__ORGANIZATIONS_LIST,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__ORGANIZATION_DETAILS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__CONTACT_PHONE_LIST,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_REPORT__CONTACT_ADDRESS_LIST,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_DEBTOR_DATA,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_IMPORT_CONTACTS_MANUALLY,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_CONTACTS_IMPORT__TEXT_,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_CONTACTS_IMPORT__OUTLOOK,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_CONTACTS_IMPORT__SKYPE,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_CONTACTS_IMPORT__TWIXTEL,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EXPORT_CONTACTS_MANUALLY,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EXPORT_CONTACTS_BACKGROUND,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_CONTACTS_EXPORT__TEXT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_IMPORT_PROJECTS_MANUALLY,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_IMPORT_PROJECTS_BACKGROUND,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_PROJECTS_IMPORT__TEXT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EXPORT_PROJECTS_MANUALLY,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EXPORT_PROJECTS_BACKTROUND,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_PROJECTS_EXPORT__TEXT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_IMPORT_EMAILS_MANUALLY,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_IMPORT_EMAILS_BACKGROUND,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_EMAILS_IMPORT__OUTLOOK,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_IMPORT_USERS_MANUALLY,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_IMPORT_USERS_BACKGROUND,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_USERS_IMPORT__TEXT,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EXPORT_USERS_MANUALLY,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EXPORT_USERS_BACKGROUND,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_FORMAT_USERS_EXPORT__TEXT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_SHOW_ORDER_BUTTON,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_EVALUATION_PERIOD,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_DISK_QUOTA_FOR_BLOBS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_LIGHT_ACCESS_RIGHTS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PROFESSIONAL_ACCESS_RIGHTS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CALENDAR__USE_PERSONAL_CALENDAR,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CALENDAR__VIEW_CONTACT_CALENDARS,{{0, 0},{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CALENDAR__VIEW_PROJECT_CALENDARS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CALENDAR__VIEW_RESOURCE_CALENDARS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_RESOURCE_FUI,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_RESERVATIONS__USER,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_RESERVATIONS__CONTACT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_RESERVATIONS__PROJECT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_RESERVATIONS__RESOURCE,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CALENDAR__NEW_ENTRIES,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_CALENDAR__BASIC_FUNCTIONALITY,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__BUDGETING_AND_VALUATION,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__VALUATIONS,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__PROJECT_WORKBENCH,{{0, 0},{0, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__DEFINE_PROJECT_TEMPLATES,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__USE_PROJECT_TEMPLATES,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__INVOICING,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__REPORTING,{{1, 100},{1, 100},{1, 100},{0, 0},{0, 0},{0, 0}}},
{FP_PC__SHORT_TIMESHEET,{{1, 0},{1, 0},{0, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__RESOURCE_MANAGEMENT_BY_PROJECT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__RESOURCE_MANAGEMENT_BY_PERSON,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__PROJECT_CONTROL_COMPACT,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__PROJECT_CONTROL_PRO,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},
{FP_PC__TIMESHEET_ENTRY,{{1, 0},{1, 0},{1, 0},{0, 0},{0, 0},{0, 0}}},

};


int ARDefault_GetModuleCode(const char *szCode)
{
	static int nCnt = sizeof(g_arModInfo)/sizeof(g_arModInfo[0]);
	for(int i=0; i<nCnt; i++)
		if(0 == strcmp(g_arModInfo[0].szCode, szCode))
			return i+1;
	return -1;
}


bool ARDefault_CanUseFP(int nMod, int nFP, int &nValue)
{
	int nFPIdx = FindEntry(nFP);
	if(nFPIdx >= 0){
		nValue = g_arAccessRights[nFPIdx].arState[nMod-1].nValue;
		return g_arAccessRights[nFPIdx].arState[nMod-1].bCanUse;
	}
	return false;
}


int FindEntry(int nFP)
{
	int nMax = sizeof(g_arAccessRights)/sizeof(g_arAccessRights[0]);
	int nIdx = -1;
	for(int i=0; i<nMax; i++){
		if(g_arAccessRights[i].nCode == nFP){
			nIdx = i;
			break;
		}
	}
	return nIdx;
}
