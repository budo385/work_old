#include "nmrxmanager.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/entity_id_collection.h"
#include "globalconstants.h"

void NMRXManager::DefineRolePairList(DbRecordSet &lstRolePair)
{
	//create list
	lstRolePair.destroy();
	lstRolePair.addColumn(QVariant::Int,"PAIR_ID");
	lstRolePair.addColumn(QVariant::Int,"M_TABLE_ID");
	lstRolePair.addColumn(QVariant::Int,"N_TABLE_ID");
	lstRolePair.addColumn(QVariant::String,"PAIR_NAME");
	lstRolePair.addColumn(QVariant::Int,"M_TABLE_TYPE_ID");  //type id: further describes entity (for CE-comm entity this is CENT_CE_TYPE_ID)
	lstRolePair.addColumn(QVariant::Int,"N_TABLE_TYPE_ID");  //type id: further describes entity (for CE-comm entity this is CENT_CE_TYPE_ID)

}

//gets all pair in format: Pair, N TableID, M Table ID
void NMRXManager::GetRolePairList(DbRecordSet &lstRolePair)
{

	DefineRolePairList(lstRolePair);

	lstRolePair.addRow();
	lstRolePair.setData(0,0,PAIR_CONTACT_PROJECT);
	lstRolePair.setData(0,1,BUS_CM_CONTACT);
	lstRolePair.setData(0,2,BUS_PROJECT);
	lstRolePair.setData(0,3,QObject::tr("Contact-Project"));

	lstRolePair.addRow();
	lstRolePair.setData(1,0,PAIR_CONTACT_PERSON);
	lstRolePair.setData(1,1,BUS_CM_CONTACT);
	lstRolePair.setData(1,2,BUS_PERSON);
	lstRolePair.setData(1,3,QObject::tr("Contact-User"));

	lstRolePair.addRow();lstRolePair.setData(2,0,PAIR_CONTACT_CONTACT);
	lstRolePair.setData(2,1,BUS_CM_CONTACT);
	lstRolePair.setData(2,2,BUS_CM_CONTACT);
	lstRolePair.setData(2,3,QObject::tr("Contact-Contact"));

	lstRolePair.addRow();
	lstRolePair.setData(3,0,PAIR_PROJECT_PROJECT);
	lstRolePair.setData(3,1,BUS_PROJECT);
	lstRolePair.setData(3,2,BUS_PROJECT);
	lstRolePair.setData(3,3,QObject::tr("Project-Project"));

	lstRolePair.addRow();
	lstRolePair.setData(4,0,PAIR_PERSON_CONTACT);
	lstRolePair.setData(4,1,BUS_PERSON);
	lstRolePair.setData(4,2,BUS_CM_CONTACT);
	lstRolePair.setData(4,3,QObject::tr("User-Contact"));

	lstRolePair.addRow();
	lstRolePair.setData(5,0,PAIR_PERSON_PROJECT);
	lstRolePair.setData(5,1,BUS_PERSON);
	lstRolePair.setData(5,2,BUS_PROJECT);
	lstRolePair.setData(5,3,QObject::tr("User-Project"));

	lstRolePair.addRow();
	lstRolePair.setData(6,0,PAIR_CALENDAR_PERSON);
	lstRolePair.setData(6,1,CE_COMM_ENTITY);
	lstRolePair.setData(6,2,BUS_PERSON);
	lstRolePair.setData(6,3,QObject::tr("Calendar Event-User"));
	lstRolePair.setData(6,4,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);

	lstRolePair.addRow();
	lstRolePair.setData(7,0,PAIR_CALENDAR_CONTACT);
	lstRolePair.setData(7,1,CE_COMM_ENTITY);
	lstRolePair.setData(7,2,BUS_CM_CONTACT);
	lstRolePair.setData(7,3,QObject::tr("Calendar Event-Contact"));
	lstRolePair.setData(7,4,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);

	lstRolePair.addRow();
	lstRolePair.setData(8,0,PAIR_CALENDAR_PROJECT);
	lstRolePair.setData(8,1,CE_COMM_ENTITY);
	lstRolePair.setData(8,2,BUS_PROJECT);
	lstRolePair.setData(8,3,QObject::tr("Calendar Event-Project"));
	lstRolePair.setData(8,4,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);

	lstRolePair.addRow();
	lstRolePair.setData(9,0,PAIR_CALENDAR_RESOURCE);
	lstRolePair.setData(9,1,CE_COMM_ENTITY);
	lstRolePair.setData(9,2,BUS_CAL_RESOURCE);
	lstRolePair.setData(9,3,QObject::tr("Calendar Event-Resource"));
	lstRolePair.setData(9,4,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);


	lstRolePair.addRow();
	lstRolePair.setData(10,0,PAIR_PROJECT_CONTACT);
	lstRolePair.setData(10,1,BUS_PROJECT);
	lstRolePair.setData(10,2,BUS_CM_CONTACT);
	lstRolePair.setData(10,3,QObject::tr("Project-Contact"));
}

DbRecordSet NMRXManager::GetPairRow(int nPair)
{
	//create list
	DbRecordSet lstRolePair;
	GetRolePairList(lstRolePair);
	//lstRolePair.Dump();

	int nRow=lstRolePair.find(0,nPair,true);
	if (nRow==-1)
	{
		Q_ASSERT(false);
		return lstRolePair;
	}
	lstRolePair.clearSelection();
	lstRolePair.selectRow(nRow);
	lstRolePair.deleteUnSelectedRows();
	return lstRolePair;

/*
	DefineRolePairList(lstRolePair);
	lstRolePair.addRow();

	switch(nPair)
	{
	case PAIR_CONTACT_PROJECT:
		lstRolePair.setData(0,0,PAIR_CONTACT_PROJECT);
		lstRolePair.setData(0,1,BUS_CM_CONTACT);
		lstRolePair.setData(0,2,BUS_PROJECT);
		lstRolePair.setData(0,3,QObject::tr("Contact-Project"));
		break;

	case PAIR_CONTACT_PERSON:
		lstRolePair.setData(0,1,BUS_CM_CONTACT);
		lstRolePair.setData(0,2,BUS_PERSON);
		lstRolePair.setData(0,3,QObject::tr("Contact-User"));
		break;

	case PAIR_CONTACT_CONTACT:
		lstRolePair.setData(0,0,PAIR_CONTACT_CONTACT);
		lstRolePair.setData(0,1,BUS_CM_CONTACT);
		lstRolePair.setData(0,2,BUS_CM_CONTACT);
		lstRolePair.setData(0,3,QObject::tr("Contact-Contact"));
	    break;
	case PAIR_PROJECT_PROJECT:
		lstRolePair.setData(0,0,PAIR_PROJECT_PROJECT);
		lstRolePair.setData(0,1,BUS_PROJECT);
		lstRolePair.setData(0,2,BUS_PROJECT);
		lstRolePair.setData(0,3,QObject::tr("Project-Project"));
	    break;
	case PAIR_PERSON_CONTACT:
		lstRolePair.setData(0,0,PAIR_PERSON_CONTACT);
		lstRolePair.setData(0,1,BUS_PERSON);
		lstRolePair.setData(0,2,BUS_CM_CONTACT);
		lstRolePair.setData(0,3,QObject::tr("User-Contact"));
		break;
	case PAIR_PERSON_PROJECT:
		lstRolePair.setData(0,0,PAIR_PERSON_PROJECT);
		lstRolePair.setData(0,1,BUS_PERSON);
		lstRolePair.setData(0,2,BUS_PROJECT);
		lstRolePair.setData(0,3,QObject::tr("User-Project"));
		break;
	default:
		Q_ASSERT(false);
	    break;
	}

	return lstRolePair;
	*/
}


//compile list for read: in format bus_nmrx_relation
DbRecordSet NMRXManager::GetPairListForRead(DbRecordSet lstPairs,int nTableID,int nTableRecordID)
{
	DbRecordSet lstRead;
	lstRead.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));


	int nTable1Idx=lstRead.getColumnIdx("BNMR_TABLE_1");
	int nTable2Idx=lstRead.getColumnIdx("BNMR_TABLE_2");
	int nKey1Idx=lstRead.getColumnIdx("BNMR_TABLE_KEY_ID_1");
	int nKey2Idx=lstRead.getColumnIdx("BNMR_TABLE_KEY_ID_2");

	//lstPairs.Dump();

	int nSize=lstPairs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstPairs.getDataRef(i,1).toInt()==nTableID)
		{
			lstRead.addRow();
			int nRow=lstRead.getRowCount()-1;
			lstRead.setData(nRow,nTable1Idx,nTableID);
			lstRead.setData(nRow,nTable2Idx,lstPairs.getDataRef(i,2));
			lstRead.setData(nRow,nKey1Idx,nTableRecordID);
			lstRead.setData(nRow,nKey2Idx,-1);
			//patch: if contact-contact or project-project then load other side too:
			if(lstPairs.getDataRef(i,2).toInt()==nTableID)
			{
				lstRead.addRow();
				int nRow=lstRead.getRowCount()-1;
				lstRead.setData(nRow,nTable1Idx,nTableID);
				lstRead.setData(nRow,nTable2Idx,nTableID);
				lstRead.setData(nRow,nKey1Idx,-1);
				lstRead.setData(nRow,nKey2Idx,nTableRecordID);
			}
		}
		else if (lstPairs.getDataRef(i,2).toInt()==nTableID)
		{
			lstRead.addRow();
			int nRow=lstRead.getRowCount()-1;
			lstRead.setData(nRow,nTable2Idx,nTableID);
			lstRead.setData(nRow,nTable1Idx,lstPairs.getDataRef(i,1));
			lstRead.setData(nRow,nKey2Idx,nTableRecordID);
			lstRead.setData(nRow,nKey1Idx,-1);
		}
	}

	//lstRead.Dump();

	return lstRead;
}






int NMRXManager::GetEntityIDFromTableID(int nTableID, int nTableTypeID)
{

	switch(nTableID)
	{
	case BUS_CM_CONTACT:
		return ENTITY_BUS_CONTACT;
		break;
	case BUS_PERSON:
		return ENTITY_BUS_PERSON;
		break;
	case BUS_PROJECT:
		return ENTITY_BUS_PROJECT;
	    break;
	case CE_COMM_ENTITY:
		if (nTableTypeID==GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART)
			return ENTITY_CALENDAR_EVENT_PART;
		else
			return -1;
		break;
	case BUS_CAL_RESOURCE:
		return ENTITY_CALENDAR_RESOURCES;
		break;
	default:
		Q_ASSERT(false);
		return -1;
	    break;
	}

	return -1;

}


void NMRXManager::GetIconAndTextFromTableID(int nTableID,QString &strIcon,QString &strText, int nTableTypeID)
{
	switch(nTableID)
	{
	case BUS_CM_CONTACT:
		{
			strIcon=":Contacts16.png";
			strText=QObject::tr("Contact","_1");
		}
		break;
	case BUS_PERSON:
		{
			strIcon=":User16.png";
			strText=QObject::tr("User","_1");
		}
		break;
	case BUS_PROJECT:
		{
			strIcon=":Projects16.png";
			strText=QObject::tr("Project","_1");
		}
		break;
	case CE_COMM_ENTITY:
		if (nTableTypeID==GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART)
		{
			strIcon=":CalendarEvent16.png";
			strText=QObject::tr("Project","_1");
		}
		break;
	case BUS_CAL_RESOURCE:
		{
			strIcon=":Resource16.png";
			strText=QObject::tr("Resource","_1");
		}
		break;
	default:
		Q_ASSERT(false);
		break;
	}

}

QString NMRXManager::GetEntityNameFromTableID(int nTableID, int nTableTypeID)
{

	switch(nTableID)
	{
	case BUS_CM_CONTACT:
		return QObject::tr("Contact","nmrx_dlg");
		break;
	case BUS_PERSON:
		return QObject::tr("User");
		break;
	case BUS_PROJECT:
		return QObject::tr("Project");
		break;
	case CE_COMM_ENTITY:
		if (nTableTypeID==GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART)
			return QObject::tr("Calendar Event");
		else
			return "";
		break;
	case BUS_CAL_RESOURCE:
		return QObject::tr("Resource");
		break;
	default:
		Q_ASSERT(false);
		break;
	}

	return "";
}

QString	NMRXManager::GetNameColumnFromTableID(int nTableID, int nTableTypeID)
{
	switch(nTableID)
	{
	case BUS_CM_CONTACT:
		return "BCNT_NAME";
		break;
	case BUS_PERSON:
		return "BPER_NAME";
		break;
	case BUS_PROJECT:
		return "BUSP_NAME";
		break;
	case BUS_CAL_RESOURCE:
		return "BRES_NAME";
		break;
	default:
		return "NAME";;
		break;
	}

	return "";
}


void NMRXManager::DefineDefaultRecordX(int nX_TableID,DbRecordSet &rowInvite)
{
	switch(nX_TableID)
	{
		case BUS_CAL_INVITE:
			{
				rowInvite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE));
				rowInvite.addRow();
				rowInvite.setData(0,"BCIV_INVITE",1);
				rowInvite.setData(0,"BCIV_IS_SENT",0);
				rowInvite.setData(0,"BCIV_STATUS",0);
				rowInvite.setData(0,"BCIV_BY_NOTIFICATION",0);
				rowInvite.setData(0,"BCIV_BY_EMAIL",1);
				rowInvite.setData(0,"BCIV_BY_SMS",0);
				return;
			}
		break;
	default:
		break;
	}

	return;
}

//from nmrx data to the marko list:  "ENTITY_TYPE", "ENTITY_ID", "ENTITY_PERSON_ID"
//note: tested only 2nd key and only for calendar use
void NMRXManager::ConvertToAssignmentData(DbRecordSet &lstResult, DbRecordSet &lstNMRXData)
{
	lstResult.destroy();
	lstResult.addColumn(QVariant::Int,"ENTITY_TYPE");
	lstResult.addColumn(QVariant::Int,"ENTITY_ID");
	lstResult.addColumn(QVariant::Int,"ENTITY_PERSON_ID");

	int nSize=lstNMRXData.getRowCount();
	lstResult.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		int nTableID=lstNMRXData.getDataRef(i,"BNMR_TABLE_2").toInt();
		switch(nTableID)
		{
		case BUS_PERSON:
			{
				lstResult.setData(i,"ENTITY_TYPE",GlobalConstants::TYPE_CAL_USER_SELECT);
				lstResult.setData(i,"ENTITY_ID",lstNMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2"));
				lstResult.setData(i,"ENTITY_PERSON_ID",lstNMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2"));
			}
			break;
		case BUS_CM_CONTACT:
			{
				lstResult.setData(i,"ENTITY_TYPE",GlobalConstants::TYPE_CAL_CONTACT_SELECT);
				lstResult.setData(i,"ENTITY_ID",lstNMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2"));
				lstResult.setData(i,"ENTITY_PERSON_ID",-1);
			}
			break;
		case BUS_PROJECT:
			{
				lstResult.setData(i,"ENTITY_TYPE",GlobalConstants::TYPE_CAL_PROJECT_SELECT);
				lstResult.setData(i,"ENTITY_ID",lstNMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2"));
				lstResult.setData(i,"ENTITY_PERSON_ID",-1);
			}
		    break;
		case BUS_CAL_RESOURCE:
			{
				lstResult.setData(i,"ENTITY_TYPE",GlobalConstants::TYPE_CAL_RESOURCE_SELECT);
				lstResult.setData(i,"ENTITY_ID",lstNMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2"));
				lstResult.setData(i,"ENTITY_PERSON_ID",-1);
			}
		    break;
		default:
			Q_ASSERT(false);
		    break;
		}
	}

}

void NMRXManager::ConvertToAssignmentDataFromCalendarPartData(DbRecordSet &lstCalData, DbRecordSet &lstPartData)
{
	lstCalData.addColumn(QVariant::Int,"ENTITY_TYPE");
	lstCalData.addColumn(QVariant::Int,"ENTITY_ID");
	lstCalData.addColumn(QVariant::Int,"ENTITY_PERSON_ID");

	int nSize=lstPartData.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstOneRowData;
		DbRecordSet lstNMRXPerson=lstPartData.getDataRef(i,"NMRX_PERSON_LIST").value<DbRecordSet>();
		NMRXManager::ConvertToAssignmentData(lstOneRowData, lstNMRXPerson);
		lstCalData.merge(lstOneRowData);

		DbRecordSet lstNMRXProject=lstPartData.getDataRef(i,"NMRX_PROJECT_LIST").value<DbRecordSet>();
		NMRXManager::ConvertToAssignmentData(lstOneRowData, lstNMRXProject);
		lstCalData.merge(lstOneRowData);

		DbRecordSet lstNMRXResources=lstPartData.getDataRef(i,"NMRX_RESOURCE_LIST").value<DbRecordSet>();
		NMRXManager::ConvertToAssignmentData(lstOneRowData, lstNMRXResources);
		lstCalData.merge(lstOneRowData);
	}

}

