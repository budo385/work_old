#ifndef ACCESSRIGHTSORGANIZER_H
#define ACCESSRIGHTSORGANIZER_H

#include <QHash>

#include "accessrightsid.h"
#include "accessright.h"
#include "accessrightset.h"

class AccessRightsOrganizer
{
public:
    AccessRightsOrganizer();
    ~AccessRightsOrganizer();

	AccessRight* GetAccessRight(int AccessRightID);
	AccessRightSet* GetAccessRightSet(int AccessRightSetID);
	QHash<int, AccessRightSet *>* GetAccessRightSetHash();
	QHash<int, AccessRightSet *> GetSimpleAccessRightSetHash();

	QHash<int, AccessRight *>		m_hshAccessRights;
	QHash<int, AccessRightSet *>	m_hshAccessRightSets;

private:
	void InitializeAccessRights();
	void InitializeAccessRightSets();
	void InitializeARSRights();


};

#endif // ACCESSRIGHTSORGANIZER_H
