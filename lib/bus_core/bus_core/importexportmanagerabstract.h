#ifndef IMPORTEXPORTMANAGERABSTRACT_H
#define IMPORTEXPORTMANAGERABSTRACT_H

#include <QMutex>
#include <QFileSystemWatcher>
#include <QReadWriteLock>

#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "trans/trans/ftpconnectionsettings.h"
#include "bus_interface/bus_interface/interface_businessserviceset.h"

#define TEMP_FTP_DIR_PROJECT "tmp_ftp_project"
#define TEMP_FTP_DIR_USER "tmp_ftp_user"
#define TEMP_FTP_DIR_DEBTOR "tmp_ftp_debtor"
#define TEMP_FTP_DIR_CONTACT "tmp_ftp_contact"

/*!
	\class ImportExportManagerAbstract
	\brief Base class for client & server backup manager
	\ingroup Bus_Core

	Multi thread safe
*/
class ImportExportManagerAbstract : public QObject
{
	Q_OBJECT

public:
	ImportExportManagerAbstract();
	~ImportExportManagerAbstract();

	enum ProcessType
	{
		PROCESS_TYPE_PROJECT,
		PROCESS_TYPE_USER,
		PROCESS_TYPE_CONTACT,
		PROCESS_TYPE_DEBTOR,
		PROCESS_TYPE_SPL

	};
	
	virtual void	Client_SaveSettings(Status &pStatus, DbRecordSet lstSettings);	//used from client
	virtual void	Client_StartTaskDelayed(Status &pStatus,int nTaskID=-1);		//used from client
	virtual bool	ReloadSettings(Status &pStatus);
	virtual int		GetGlobalPeriod(Status &pStatus);
	virtual void	GetSettings(Status &pStatus,DbRecordSet &settings);
	virtual void	SetBusinessServiceSet(Status &pStatus, Interface_BusinessServiceSet *pBusinessSet); 

	virtual void EnableFileWatcher(){m_FileWatcher.blockSignals(false);};
	virtual void DisableFileWatcher(){m_FileWatcher.blockSignals(true);};

signals:
	void	GlobalPeriodChanged(int nPeriodMin); 
	void	SignalStartTask(int nCallerID=-1,int nTaskID=-1); 

private slots:
	void	OnDirectoryChange(const QString & path);

protected:
	void	StartAllScheduledTasks(Status &pStatus, QString strLang);	
	void	StartProcessProject(Status &pStatus); 
	void	StartProcessUser(Status &pStatus); 
	void	StartProcessDebtor(Status &pStatus); 
	void	StartProcessContact(Status &pStatus, QString strLang);
	void	StartProcessStoredProjectList(Status &pStatus); 
	QString GetLastStatus(int nTaskID);

	//local funct:
	virtual void SaveSettings(Status &Ret_pStatus, DbRecordSet lstSettings);
	virtual void ReadSettings(Status &Ret_pStatus, DbRecordSet &Ret_lstSettings);
	virtual void ProcessProjects(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	virtual void ProcessUsers(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	virtual void ProcessDebtors(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	virtual	void ProcessContacts(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, QString strLang);
	virtual void ProcessStoredProjectLists(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	virtual void GetFTPSettings(Status &pStatus,int nFTPID,FTPConnectionSettings &conn);

	//FTP issue:
	static void		PreProcessExportFTP(Status &err,QString strLocalTempDir);
	static void		PostProcessExportFTP(Status &err,QString strLocalTempDir,FTPConnectionSettings FTPconn, QString strFTPDirectory,DbRecordSet &Ret_ProcessedFiles);
	static int		PreProcessImportFTP(Status &err,QString strLocalTempDir,FTPConnectionSettings FTPconn, QString strFTPDirectory,QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed);
	static void		PostProcessImportFTP(Status &err,QString strLocalTempDir,FTPConnectionSettings FTPconn, QString strFTPDirectory, QString strFileExtOfProcessed,DbRecordSet &Ret_ProcessedFiles);
	
	QReadWriteLock					m_RWMutex;				//data lock: only 1 can change, more then 1 thread can read imp/exp data
	bool							m_bFileWatcherEnabled;
	QFileSystemWatcher				m_FileWatcher;
private:
	QMutex							m_mtxProjects;	//operation lock: only 1 can execute process at same time
	QMutex							m_mtxUsers;		//operation lock: only 1 can execute process at same time
	QMutex							m_mtxDebtors;	//operation lock: only 1 can execute process at same time
	QMutex							m_mtxContacs;	//operation lock: only 1 can execute process at same time
	QMutex							m_mtxSPL;		//operation lock: only 1 can execute process at same time (stored project list)

	
	DbRecordSet						m_Settings;
	int								m_nPeriodProject;
	int								m_nPeriodStoredProjectList;
	int								m_nPeriodUser;
	int								m_nPeriodDebtor;
	int								m_nPeriodContact;
	Interface_BusinessServiceSet*	m_pBusinessSet;  //pointer to BO set!
	
};


#endif // IMPORTEXPORTMANAGERABSTRACT_H
