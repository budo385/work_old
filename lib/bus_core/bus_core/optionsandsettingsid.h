#ifndef OPTIONSANDSETTINGSID_H
#define OPTIONSANDSETTINGSID_H

/*!
\brief Definitions of Options and Personal Settings IDs
\ingroup Db

Header file with definitions for Options and Personal Settings IDs.

*/

//************************************************************************
//************************************************************************
//************************************************************************
//**					 Personal Settings ID's							**
//************************************************************************
//************************************************************************
//************************************************************************
#define SKIN															0
#define MAIN_APPLICATION_WINDOW_SIZE									1
#define MENU_WIDTH														2
#define GRID_WIDTH_FOR_FUI_WITH_GRID_ON_LEFT							3
#define CURRENT_COUNTRY_ISO_CODE														4 //Country predial

//Default 
#define PERSON_DEFAULT_DOCUMENT_TYPE									5
#define PERSON_DEFAULT_DOCUMENT_DIR										6
//Import settings.
#define IMPORT_EMAIL_OUTLOOK_FOLDERS									7

//COMM settings
#define COMM_SETTINGS_LISTEN_SKYPE										8

//More person settings.
//Default language
#define DEFAULT_LANGUAGE												9
#define CURRENT_LOCATION_NAME											10
#define CURRENT_LOCATION_AREA_CODE										11
//Appearance settings.
#define SHOW_HEADER														12
#define STARTING_MODULE													13

#define OUTLOOK_SETTINGS_SUBSCRIBED										14
#define OUTLOOK_SETTINGS_TIMER_MIN										15
#define OUTLOOK_SETTINGS_LAST_SCANNED									16
#define OUTLOOK_SETTINGS_SCAN_FOLDERS									17

//Contact data (visible):
#define CONTACT_DEF_ADDR_SCHEMA_PERSON									18
#define CONTACT_DEF_ADDR_SCHEMA_ORG										19
#define CONTACT_DEF_ACTUAL_LIST_VIEW									20
#define CONTACT_DEF_GROUP_LIST_VIEW										21
#define CONTACT_DEF_GROUP_TREE											22
#define CONTACT_DEF_GROUP_FOR_LOAD										23	//invisible
#define CONTACT_DEF_SETTINGS_SAVE_ON_EXIT								24

//APPEARENCE (invisible)
#define APPEAR_WINDOW_PROPERTIES										25  //list of left menu width+size(if separate window) <- includes main window size
#define APPEAR_CE_MENU_SECTIONS											26  //what section are hidden by default
#define CONTACT_DEF_GROUP_TREE_FOR_LOAD									27
#define CONTACT_DEF_ACTUAL_LIST_FOR_LOAD								28	
#define CONTACT_DEF_FILTER_VIEW											29
#define PROJECT_DEF_FILTER_VIEW											30
#define OUTLOOK_SETTINGS_WORKSTATIONS									31

//Appearance settings 2
#define SHOW_STATUS_BAR													32
#define SHOW_FULL_SCREEN												33
#define COMM_SETTINGS_TRANSFER_GROUP									34
#define SHOW_DOCUMENT_REVISION_HISTORY									35
#define SHOW_CLASSIFICATION_AND_DESC									36
#define IMPORT_CONTACT_OUTLOOK_FOLDERS									37
#define THICK_CHECK_FOR_UPDATES											38	
#define APPEAR_VIEW_MODE												39	
#define APPEAR_THEME													40	
#define APPEAR_SIDEBAR_LAST_POSITION									41	
#define AVATAR_STARTUP_LIST												42
#define OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN								43
#define QUICK_CONTACT_ADDRESS_FORMATING_LOCK							44
#define CONTACT_GROUP_EXCLUDE_ID										45

#define EMAIL_SETTINGS_SMTP_NAME										46
#define EMAIL_SETTINGS_SMTP_PORT										47
#define EMAIL_SETTINGS_USER_NAME										48
#define EMAIL_SETTINGS_USER_EMAIL										49
#define EMAIL_SETTINGS_SMTP_ACC_NAME									50
#define EMAIL_SETTINGS_SMTP_ACC_PASS									51
#define EMAIL_SETTINGS_SEND_COPY_CC										52
#define EMAIL_SETTINGS_SEND_COPY_BCC									53
#define EMAIL_SETTINGS_USE_AUTH											54

#define THUNDERBIRD_SETTINGS_SUBSCRIBED									55
#define THUNDERBIRD_SETTINGS_TIMER_MIN									56
#define THUNDERBIRD_SETTINGS_LAST_SCANNED								57
#define THUNDERBIRD_SETTINGS_SCAN_FOLDERS								58
#define THUNDERBIRD_SETTINGS_IMPORT_ONLY_KNOWN							59

#define IMPORT_EMAIL_THUNDERBIRD_FOLDERS								60
#define CONTACT_FAVORITES												61
#define MOBILE_LAST_TAPI_DEVICE_ID										62
#define COMM_SETTINGS_DEFAULT_SEL_TYPE									63
#define COMM_SETTINGS_DEFAULT_TAPI_DEV									64
#define COMM_SETTINGS_USE_DIAL_PREFIX									65
#define COMM_SETTINGS_DIAL_PREFIX_STR									66

#define PROJECT_SETTINGS_DEF_STORED_LIST								67
#define CONTACT_DISABLE_ZIP_LOOKUP										68
#define CALENDAR_VIEW_SETTINGS											69

#define REMINDER_CALENDAR_ENABLED										70
#define REMINDER_CALENDAR_SETTINGS										71
#define REMINDER_TASK_ENABLED											72
#define REMINDER_TASK_SETTINGS											73
#define REMINDER_APPSERVER_ENABLED										74
#define REMINDER_APPSERVER_SETTINGS										75

#define CONTACT_DEFAULT_ROLE											76
#define PERSON_DEFAULT_ROLE												77
#define TWO_SCREEN_REVERSED												78

#define CONTACT_SEARCH_DIALOG_TAB										79
#define EMAIL_IN_SETTINGS_ACCOUNTS										80
#define COMM_SETTINGS_DEFAULT_CALL_INCOMING								81
#define DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS						82

#define EMAIL_SETTINGS_USE_SSL											83
#define EMAIL_IN_SETTINGS_BLACKLIST										84

#define EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE							85
#define EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE							86	//0= no notification for new em ails, 1 = push notification for all new em ails,2 = push notification for ALLOWED_EMAIL_LIST
#define EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE					87	//0: salju push poruke bez messaga, samo sa badge brojem, i bez zvuka 1: salju push poruke bez messaga, samo sa badge brojem, ali sa "Bing" zvukom	2: salju se push poruke sa message i zvukom
#define EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_ALLOWED_EMAIL_LIST				88  //string: semicolon delimited list of incoming emails if EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE=2 that will trigger push message
#define EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE_EMAIL_ACCOUNT_LIST		89	//string: semicolon list of pairs delimited by ",": email_acc;0 e.g. trombe@gmail.com,1;ante@gmail.com,0
#define EMAIL_IN_SETTINGS_PUSH_ERROR_LAST_DATETIME						90	//if error push msg is sent in last 24hr do not send it again


//************************************************************************
//************************************************************************
//************************************************************************
//**						Options ID's								**
//************************************************************************
//************************************************************************
//************************************************************************
#define APP_DEFAULT_DOCUMENT_TYPE										0
#define APP_DEFAULT_DOCUMENT_DIR										1
#define APP_DEFAULT_DOCUMENT_NAME										2
#define APP_CONTACT_DEF_ADDR_SCHEMA_PERSON								3
#define APP_CONTACT_DEF_ADDR_SCHEMA_ORG									4
#define APP_CONTACT_DEF_ACTUAL_LIST_VIEW								5
#define APP_CONTACT_DEF_GROUP_LIST_VIEW									6
#define APP_CONTACT_DEF_GROUP_TREE										7
#define APP_PERSON_DOCUMENT_TYPE_PARSING_ON								8
#define COMM_OPTIONS_TRANSFER_GROUP										9
#define APP_DEFAULT_ORGANIZATION_ID										10
#define APP_CONTACT_GROUP_EXCLUDE_ID									11
#define APP_EXPORT_IMPORT_SHOW_OPTION_USE_UNICODE						12
#define PROJECT_OPTIONS_DEF_STORED_LIST									13
#define APP_FORMAT_CONTACT_ADDRESS_ALWAYS								14
#define CALENDAR_START_DAILY_WORK_TIME									15
#define CALENDAR_END_DAILY_WORK_TIME									16
#define PRESENCE_TYPE_PRELIMINARY										17
#define PRESENCE_TYPE_FINAL												18
#define PRESENCE_TYPE_AVAILABLE											19
#define CALENDAR_EMAIL_CONFIRMATION_ADDRESS								20

//************************************************************************
//************************************************************************
//************************************************************************
//**					OptionsAndSettingsSets							**
//************************************************************************
//************************************************************************
//************************************************************************
#define OPTIONS_SET														0		//This one is application options set. Others are person sets.
#define APPEARANCE														1
#define CURRENT_LOCATION												2
#define COMM_GRID_FILTER												3
#define SETTINGS_DOCUMENT_DEFAULTS										4
#define OPTIONS_DOCUMENT_DEFAULTS										5
#define IMPORT_SETTINGS													6
#define COMM_SETTINGS													7
#define OUTLOOK_SETTINGS												8
#define CONTACT_OPTIONS													9
#define CONTACT_SETTINGS_VISIBLE										10
#define CONTACT_SETTINGS_INVISIBLE										11
#define APPEARANCE_SETTINGS_INVISIBLE									12
#define ORGANIZATION_OPTIONS											13
#define PROJECT_SETTINGS_VISIBLE										14
#define COMM_SETTINGS_APP												15
#define PROGRAM_UPDATE_SETTINGS											16
#define AVATAR_SETTINGS													17
#define EMAIL_SETTINGS													18
#define THUNDERBIRD_SETTINGS											19
#define PROJECT_OPTIONS_APP												20
#define REMINDER_SETTINGS												21
#define CALENDAR_OPTIONS												22
#define EMAIL_IN_SETTINGS												23	

#endif // OPTIONSANDSETTINGSID_H
