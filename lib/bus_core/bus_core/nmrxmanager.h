#ifndef X_NMRXMANAGER_H
#define X_NMRXMANAGER_H


#include "common/common/dbrecordset.h"

/*!
	\class NMRXManager
	\brief Holds pairs for NMRX roles
	\ingroup Bus_Core

*/
class NMRXManager 
{

public:

	enum Pairs
	{
		PAIR_CONTACT_PROJECT=1, //start from 1
		PAIR_CONTACT_CONTACT,
		PAIR_CONTACT_PERSON,
		PAIR_PERSON_CONTACT,
		PAIR_PROJECT_PROJECT,
		PAIR_PERSON_PROJECT,
		PAIR_CALENDAR_PERSON,
		PAIR_CALENDAR_CONTACT,
		PAIR_CALENDAR_PROJECT,
		PAIR_CALENDAR_RESOURCE,
		PAIR_PROJECT_CONTACT
	};


	//get entity types:
	static void			DefineRolePairList(DbRecordSet &lstRolePair);
	static void			GetRolePairList(DbRecordSet &lstRolePair);
	static DbRecordSet	GetPairRow(int nPair);
	static DbRecordSet	GetPairListForRead(DbRecordSet lstPairs,int nTableID,int nTableRecordID);

	static int		GetEntityIDFromTableID(int nTableID, int nTableTypeID=-1);
	static QString	GetEntityNameFromTableID(int nTableID, int nTableTypeID=-1);
	static void		GetIconAndTextFromTableID(int nTableID,QString &strIcon,QString &strText, int nTableTypeID=-1);
	static void		DefineDefaultRecordX(int nX_TableID,DbRecordSet &rowInvite);
	static QString	GetNameColumnFromTableID(int nTableID, int nTableTypeID=-1);
	static void		ConvertToAssignmentData(DbRecordSet &lstResult, DbRecordSet &lstNMRXData);
	static void		ConvertToAssignmentDataFromCalendarPartData(DbRecordSet &lstCalData, DbRecordSet &lstPartData);

};

#endif // NMRXMANAGER_H
