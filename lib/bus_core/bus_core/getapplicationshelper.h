#ifndef GETAPPLICATIONSHELPER_H
#define GETAPPLICATIONSHELPER_H

#include <QObject>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"

class GetApplicationsHelper : public QObject
{
	Q_OBJECT

public:
	GetApplicationsHelper(QObject *parent);
	~GetApplicationsHelper();

	virtual void GetDocApplicationsFromServer(DbRecordSet &lstData, Status &err){};

private:
	
};

#endif // GETAPPLICATIONSHELPER_H
