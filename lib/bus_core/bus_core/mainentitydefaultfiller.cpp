#include "mainentitydefaultfiller.h"
#include "contacttypemanager.h"
#include "db_core/db_core/dbtableiddefinition.h"

MainEntityDefaultFiller::MainEntityDefaultFiller()
{

}

MainEntityDefaultFiller::~MainEntityDefaultFiller()
{

}

/*!
	Clear record: fill with NULL values, skips ID column

	\param lstRecords			- records
	\param nColumnIdxToSkip		- no col to skip in clear operation (default=0)
*/
void MainEntityDefaultFiller::ClearRecordSet(DbRecordSet &lstRecords,int nColumnIdxToSkip)
{
	//create dummy recordset:
	DbRecordSet lstEmpty;
	lstEmpty.copyDefinition(lstRecords);
	lstEmpty.addRow();

	//empty each row, preserve value to skip
	int nSize=lstRecords.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//save value:
		QVariant value=lstRecords.getDataRef(i,nColumnIdxToSkip);
		lstRecords.assignRow(lstEmpty); //empty list
		lstRecords.setData(i,nColumnIdxToSkip,value);
	}


}

/*!
	Sets default/mandatory values

	\param lstRecords				- records 
	\param bFillMandatoryFields		- if true, all mandatory fields will be filled, else only defaults
	\param bSkipIfAlreadyFilled		- if true, then default values will be inserted only if previous field value is NULL
*/
void MainEntityDefaultFiller::InsertDefaultValues(int nTableID,DbRecordSet &lstRecords, bool bFillMandatoryFields, bool bSkipIfAlreadyFilled, bool bClearIDField)
{
	switch(nTableID)
	{
	case BUS_CM_CONTACT: //no mandatory fields except type
		{
			int nSize=lstRecords.getRowCount();
			if (bSkipIfAlreadyFilled)
			{
				for(int i=0;i<nSize;++i)
				{
					if(bClearIDField && lstRecords.getDataRef(i,"BCNT_ID").isNull())lstRecords.setData(i,"BCNT_ID",0);							
					if(lstRecords.getDataRef(i,"BCNT_TYPE").isNull())lstRecords.setData(i,"BCNT_TYPE",ContactTypeManager::CM_TYPE_PERSON);							//set person as def
					//if(lstRecords.getDataRef(i,"BCNT_ACCESS").isNull())lstRecords.setData(i,"BCNT_ACCESS",ContactTypeManager::ACC_RIGHT_PUBLIC_WRITE);				//public access right to contact
					if(lstRecords.getDataRef(i,"BCNT_SEX").isNull())lstRecords.setData(i,"BCNT_SEX",ContactTypeManager::SEX_MALE);									//public access right to contact
					if(lstRecords.getDataRef(i,"BCNT_SUPPRESS_MAILING").isNull())lstRecords.setData(i,"BCNT_SUPPRESS_MAILING",0);									//set person as def
				}
			} 
			else
			{
				for(int i=0;i<nSize;++i)
				{
					if (bClearIDField)	lstRecords.setData(i,"BCNT_ID",0);				
					lstRecords.setData(i,"BCNT_TYPE",ContactTypeManager::CM_TYPE_PERSON);			//set person as def
					//lstRecords.setData(i,"BCNT_ACCESS",ContactTypeManager::ACC_RIGHT_PUBLIC_WRITE);//public access right to contact
					lstRecords.setData(i,"BCNT_SEX",ContactTypeManager::SEX_MALE);					//public access right to contact
					lstRecords.setData(i,"BCNT_SUPPRESS_MAILING",0);								//public access right to contact
				}
			}
		}
		break;
	case BUS_PROJECT:
		{
			int nSize=lstRecords.getRowCount();
			if (bSkipIfAlreadyFilled)
			{
				for(int i=0;i<nSize;++i)
				{
					if(bClearIDField && lstRecords.getDataRef(i,"BUSP_ID").isNull())lstRecords.setData(i,"BUSP_ID",0);							
					if(lstRecords.getDataRef(i,"BUSP_ACTIVE_FLAG").isNull())lstRecords.setData(i,"BUSP_ACTIVE_FLAG",1);									
					if(lstRecords.getDataRef(i,"BUSP_TEMPLATE_FLAG").isNull())lstRecords.setData(i,"BUSP_TEMPLATE_FLAG",0);
				}
			} 
			else
			{
				for(int i=0;i<nSize;++i)
				{
					if(bClearIDField )lstRecords.setData(i,"BUSP_ID",0);							
					lstRecords.setData(i,"BUSP_ACTIVE_FLAG",1);									
					lstRecords.setData(i,"BUSP_TEMPLATE_FLAG",0);
				}
			}
		}
		break;

	default:
		Q_ASSERT(false); //implement
	    break;
	}



}


//overrides only columns that have values inside lstDefaults,
//lstTarget and lstDefaults must have same size row,col and type (same lists
void MainEntityDefaultFiller::ApplyDefaultValues(DbRecordSet &lstTarget,DbRecordSet &lstDefaults)
{
	Q_ASSERT(lstDefaults.getRowCount()==lstTarget.getRowCount() && lstDefaults.getColumnCount()==lstTarget.getColumnCount());

	int nSize=lstTarget.getRowCount();
	int nColMax=lstTarget.getColumnCount();
	for(int i=0;i<nSize;++i)
	{
		DbRecordSet Row=lstDefaults.getRow(i);
		for(int j=0;j<nColMax;++j)
				if(!Row.getDataRef(0,j).isNull())
					lstTarget.setData(i,j,Row.getDataRef(0,j));
	}

}