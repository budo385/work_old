#include "useraccessright.h"
#include <QListIterator>
#include <QStringList>
#include "db_core/db_core/dbtableiddefinition.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "bus_core/bus_core/accuarcore.h"


//-----------------------------------------------------------
//					HELPER
//-----------------------------------------------------------


UserARData::UserARData(QHash<int, QString> ARHash,bool bIsSystemUser)
{
	m_hshARHash = ARHash;
	m_bIsSystemUser=bIsSystemUser;
}

UserARData::~UserARData()
{

}

/*!
Get access right value.

\param ARCode - Access right code.
\return QString - Value in string.
*/
QString UserARData::GetARValue(int ARCode)
{
	return m_hshARHash.value(ARCode,"");
}




//-----------------------------------------------------------
//					AC RIGHTS
//-----------------------------------------------------------

UserAccessRight::UserAccessRight()
{

}

UserAccessRight::~UserAccessRight()
{
	qDeleteAll(m_hshUserARHash);
	m_hshUserARHash.clear();
}



/*!
Sets AR record set for user, if exists, overwrite!

\param err			- loading AR from DB failed!!
\param UserID		- User ID.
\param IsSystemUser	- true if system user: no AR checking, always return true (everything is allowed)
*/
void UserAccessRight::SetUserARRecordSet(int UserID,DbRecordSet &ARRecordSet,bool bIsSystemUser)
{
	QWriteLocker locker (&m_RWMutex); //lock list
	AddUserARRecordSet(UserID,ARRecordSet,bIsSystemUser); //reload only if not already loaded
}


/*!
	Remove user recordset from memory.

	\param UserID		- User ID.
*/
void UserAccessRight::DeleteUserARRecordSet(int UserID)
{
	QWriteLocker locker (&m_RWMutex); //lock list

	if(!m_hshUserARHash.contains(UserID)) return; //user not in list
	delete(m_hshUserARHash.value(UserID));
	m_hshUserARHash.remove(UserID);
}





/*!
Test Access rights by it's code - for bool access rights.

\param ARCode - Access right code.
\return bool
*/
bool UserAccessRight::TestAR(int ARCode, bool bReturnOKIfNoExist)
{

	QReadLocker locker (&m_RWMutex); //lock list for read

	//get user record:
	int UserID=GetUserID();
	if(!m_hshUserARHash.contains(UserID)) return bReturnOKIfNoExist; //deny user access


	if(m_hshUserARHash.value(UserID)->m_bIsSystemUser) return true; //quick: this is sysadmin, give him all rights!

	//Get stored value.
	QString Value = m_hshUserARHash.value(UserID)->GetARValue(ARCode);

	//issue for MArko: existing users without AR defined must have default AR value = allow!????
	//for now, just change that if flag is set..
	if (bReturnOKIfNoExist && Value.isEmpty())
	{
		return true;
	}

	if (Value == "1")
		return true;
	else
		return false;
}

/*!
Test Access rights by it's code and integer value - for integer access rights.

\param ARCode	- Access right code.
\param IntValue - Integer access right value.
\return bool
*/
bool UserAccessRight::TestAR(int ARCode, int IntValue)
{
	QReadLocker locker (&m_RWMutex); //lock list for read

	int UserID=GetUserID();
	if(!m_hshUserARHash.contains(UserID)) return false; //deny user access

	if(m_hshUserARHash.value(UserID)->m_bIsSystemUser) return true; //quick: this is sysadmin, give him all rights!

	//Get stored value.
	QString Value = m_hshUserARHash.value(UserID)->GetARValue(ARCode);

	if (IntValue == QVariant(Value).toInt())
		return true;
	else
		return false;
}

/*!
Test Access rights by it's code and string value - for string access rights (project lists, ...).

\param ARCode		- Access right code.
\param StringValue	- String access right value.
\return bool
*/
bool UserAccessRight::TestAR(int ARCode, QString StringValue)
{
	QReadLocker locker (&m_RWMutex); //lock list for read

	int UserID=GetUserID();
	if(!m_hshUserARHash.contains(UserID)) return false; //deny user access

	if(m_hshUserARHash.value(UserID)->m_bIsSystemUser) return true; //quick: this is sysadmin, give him all rights!

	//Get stored value.
	QString Value = m_hshUserARHash.value(UserID)->GetARValue(ARCode);

	//Convert to list of values (e.g. for project lists).
	QStringList ValueList = Value.split(";");

	//Test is there that value.
	if (ValueList.contains(StringValue))
		return true;
	else
		return false;
}








//-----------------------------------------------------------
//					PRIVATE
//-----------------------------------------------------------



/*!
Add user recordset, loads from DB directly if not already loaded.
NOTE: lock all lists before accessing here

\param UserID		- User ID.
\param &ARRecordSet	- Access rights recordset (if exists, overwrites..., but check before....
\param IsSystemUser	- true if system user: no AR checking, always return true (everything is allowed)
*/
void UserAccessRight::AddUserARRecordSet(int UserID,DbRecordSet &ARRecordSet,bool bIsSystemUser)
{
	int LastARCode = -1;
	//Sort by AR Code.
	ARRecordSet.sort(9);

	//Access rights hash.
	QHash<int, QString> ARHash;

	//Get user code list.
	QList<int> ARCodeList = GetARCodes(ARRecordSet);

	//Loop through code and get AR value.
	QListIterator<int> i(ARCodeList);
	while (i.hasNext())
	{
		//Code.
		int ARCode = i.next();
		//Get AR value.
		QString Value = GetARValue(ARCode, ARRecordSet);
		ARHash.insert(ARCode, Value);
	}


	//if exist, overwrite
	if(m_hshUserARHash.contains(UserID))
	{
		delete(m_hshUserARHash.value(UserID));
		m_hshUserARHash.remove(UserID);
	}

	//set into hash
	m_hshUserARHash.insert(UserID, new UserARData(ARHash,bIsSystemUser));
}



QList<int> UserAccessRight::GetARCodes(DbRecordSet &ARRecordSet)
{
	QList<int> CodeList;

	int RowCount = ARRecordSet.getRowCount();
	for (int i = 0; i < RowCount; ++i)
	{
		if (!CodeList.contains(ARRecordSet.getDataRef(i, "CAR_CODE").toInt()))
			CodeList << ARRecordSet.getDataRef(i, "CAR_CODE").toInt();
	}

	return CodeList;
}

QString UserAccessRight::GetARValue(int ARCode, DbRecordSet &AccRRecordSet)
{
	//Define test recordset.
	DbRecordSet ARRecordSet;

	//Select rows with given code.
	AccRRecordSet.clearSelection();
	AccRRecordSet.find("CAR_CODE", ARCode);
	//Merge this rows into test recordset.
	ARRecordSet.copyDefinition(AccRRecordSet);
	ARRecordSet.merge(AccRRecordSet, true);

	//Get first row just to see the type.
	int RowCount = ARRecordSet.getRowCount();

	//First for bool.
	if (ARRecordSet.getDataRef(0, "CAR_TYPE").toInt() == 1)
	{
		QString Value = "0";

		//Loop through recordset and get value.
		for (int i = 0; i < RowCount; ++i)
		{
			if (ARRecordSet.getDataRef(i, "CAR_VALUE").toInt() == 1)
				Value = "1";
		}
		//Return value.
		return Value;
	}
	//Then integer (Get just first value - they should be the same).
	else if (ARRecordSet.getDataRef(0, "CAR_TYPE").toInt() == 0)
	{
		QString Value = ARRecordSet.getDataRef(0, "CAR_VALUE").toString();
		return Value;
	}
	//Then string.
	else
	{
		QString Value = "";
		QStringList Values;

		//Loop through recordset and get value.
		for (int i = 0;	i <	RowCount; ++i)
		{
			QString RowString = ARRecordSet.getDataRef(i, "CAR_VALUE_DESCR").toString();
			if (!RowString.isEmpty())
			{
				//Get string list (for semicolon separated values).
				QStringList stringList = RowString.split(";", QString::SkipEmptyParts);

				//Loop value and insert what is not already inserted.
				QListIterator<QString> j(stringList);
				while (j.hasNext())
				{
					QString strVal = j.next();
					if (!Values.contains(strVal))
						Values << strVal;
				}
			}
		}
		//Join all values in one string and return.
		Value = Values.join(";");
		return Value;
	}
}


//nDataRecordID can be -1 if insert mode is testing:
bool UserAccessRight::IsOperationAllowed(Status &Ret_pStatus,int nTableID, int nOperation,int nDataRecordID)
{
	Ret_pStatus.setError(0);
	if (nOperation==OP_INSERT) ////for insert not need to test lvl2
	{	
		Ret_pStatus.setError(0);
		bool bOk=IsOperationAllowed_Lvl_1(nTableID,nOperation);
		if (!bOk)
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_ACCESS_DENIED);
			return false;
		}
		return true;
	}

	QString strPrimaryKey=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);
	DbRecordSet lstTest;
	lstTest.addColumn(QVariant::Int,strPrimaryKey);
	lstTest.addRow();
	lstTest.setData(0,0,nDataRecordID);

	return IsOperationAllowed(Ret_pStatus,nTableID, nOperation,lstTest);
}

 //if only one is forbidden, returns false, and selects inside list
bool UserAccessRight::IsOperationAllowed(Status &Ret_pStatus,int nTableID, int nOperation,DbRecordSet &lstData)
{
	Ret_pStatus.setError(0);

	//LVL1 test: for table/person:
	bool bOk=IsOperationAllowed_Lvl_1(nTableID,nOperation);
	if (!bOk)
	{
		lstData.selectAll();
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_ACCESS_DENIED);
		return false;
	}

	if (nOperation==OP_INSERT) //for insert not need to test lvl2:
	{	
		return true;
	}

	//LVL2 test: for table/person/record:
	int nAccessLevelForTest=AccUARCore::ACC_LVL_READ;
	if (nOperation==OP_EDIT || nOperation==OP_INSERT)
		nAccessLevelForTest=AccUARCore::ACC_LVL_MODIFY;
	else if (nOperation==OP_DELETE)
		nAccessLevelForTest=AccUARCore::ACC_LVL_FULL;

	QString strPrimaryKey=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);
	
	DbRecordSet lstTest,lstResult;
	lstTest.addColumn(QVariant::Int,strPrimaryKey);
	lstTest.merge(lstData);

	TestUAR(Ret_pStatus,nTableID, GetPersonID(), nAccessLevelForTest, lstTest, lstResult,false);
	if (!Ret_pStatus.IsOK())
	{
		return false;
	}

	lstData.clearSelection();
	int nSize=lstResult.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		lstData.find(strPrimaryKey,lstResult.getDataRef(i,0).toInt(),true,false,false,true); //selects all found Id's
	}

	if (nSize>0)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_ACCESS_DENIED);
		return false;
	}
	
	return true;
}


//TEST from ACCESSRIGHTSID_H for current user!
bool UserAccessRight::IsOperationAllowed_Lvl_1(int nTableID, int nOperation)
{
	//LVL1: specific tables:

	switch(nTableID)
	{
	case BUS_CM_CONTACT:
		{
			if (nOperation==OP_EDIT || nOperation==OP_DELETE || nOperation==OP_INSERT)
				if(!TestAR(ENTER_MODIFY_CONTACTS))
					return false;
		}
		break;
	case BUS_PROJECT:
		{
			if (nOperation==OP_EDIT || nOperation==OP_DELETE  || nOperation==OP_INSERT)
				if(!TestAR(ENTER_MODIFY_PROJECT_MAIN_DATA))
					return false;
		}
		break;
	case BUS_PERSON:
		{
			if (nOperation==OP_READ)
				if(!TestAR(SEE_PERSONAL_RECORDS_DATA))
					return false;
			if (nOperation==OP_EDIT)
				if(!TestAR(EDIT_PERSONAL_RECORDS))
					return false;
			if (nOperation==OP_INSERT)
				if(!TestAR(INSERT_PERSONAL_RECORDS))
					return false;
			if (nOperation==OP_DELETE)
				if(!TestAR(DELETE_PERSONAL_RECORDS))
					return false;
		}
		break;
	case CORE_USER:
		{
			if (nOperation==OP_READ)
				if(!TestAR(SEE_USER_RECORDS_DATA))
					return false;
			if (nOperation==OP_EDIT)
				if(!TestAR(EDIT_USER_RECORDS))
					return false;
			if (nOperation==OP_INSERT)
				if(!TestAR(INSERT_USER_RECORDS))
					return false;
			if (nOperation==OP_DELETE)
				if(!TestAR(DELETE_USER_RECORDS))
					return false;
		}
		break;
	case BUS_DEPARTMENT:
		{
			if (nOperation==OP_READ)
				if(!TestAR(SEE_DEPARTMENTS_RECORDS))
					return false;
			if (nOperation==OP_EDIT)
				if(!TestAR(EDIT_DEPARTMENTS_RECORDS))
					return false;
			if (nOperation==OP_INSERT)
				if(!TestAR(INSERT_DEPARTMENTS_RECORDS))
					return false;
			if (nOperation==OP_DELETE)
				if(!TestAR(DELETE_DEPARTMENTS_RECORDS))
					return false;
		}
		break;
	case BUS_ORGANIZATION:
		{
			if (nOperation==OP_READ)
				if(!TestAR(SEE_ORGANIZATIONS_RECORDS))
					return false;
			if (nOperation==OP_EDIT)
				if(!TestAR(EDIT_ORGANIZATIONS_RECORDS))
					return false;
			if (nOperation==OP_INSERT)
				if(!TestAR(INSERT_ORGANIZATIONS_RECORDS))
					return false;
			if (nOperation==OP_DELETE)
				if(!TestAR(DELETE_ORGANIZATIONS_RECORDS))
					return false;
		}
		break;
	default:
		//Q_ASSERT(false); //LVL1 AR is not implemented here!!!
		break;
	}

	return true;
}


//SQL filter for read operation:
void UserAccessRight::SQLFilterRecords(int nTableID, QString &strSQL)
{
	bool bIsSystemUser=IsSystemUser(); //for automatic import/sync by server: 'system' user has ful right!
	if (bIsSystemUser)
		return;

	QString strPrimaryKey=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);
	QString strTableName=DbSqlTableDefinition::GetTableName(nTableID);
	int nPersonID=GetPersonID();

	QString strJoin =" LEFT OUTER JOIN CORE_ACC_USER_REC UAR_A ON UAR_A.CUAR_RECORD_ID="+strPrimaryKey+" AND UAR_A.CUAR_TABLE_ID="+QVariant(nTableID).toString()+" AND UAR_A.CUAR_PERSON_ID="+QVariant(nPersonID).toString();
	strJoin+=" LEFT OUTER JOIN CORE_ACC_USER_REC UAR_B ON UAR_B.CUAR_RECORD_ID="+strPrimaryKey+" AND UAR_B.CUAR_TABLE_ID="+QVariant(nTableID).toString()+" AND UAR_B.CUAR_PERSON_ID IS NULL ";

	int nPosWhere=strSQL.indexOf("WHERE",0,Qt::CaseInsensitive);
	int nPosOrderBy=strSQL.indexOf("ORDER",0,Qt::CaseInsensitive);
	
	//test if ORDER BY exists:
	QString strOrderBy;
	if (nPosOrderBy>=0)
	{
		strOrderBy=strSQL.mid(nPosOrderBy);
		strSQL=strSQL.left(nPosOrderBy);
	}

	//test if WHERE exists:
	if (nPosWhere<0)
		strSQL+=strJoin+" WHERE ";
	else
		strSQL=strSQL.left(nPosWhere)+strJoin+strSQL.mid(nPosWhere)+" AND ";

	strSQL+=" ((UAR_A.CUAR_RIGHT IS NULL AND UAR_B.CUAR_RIGHT IS NULL) OR";
	strSQL+=" (UAR_A.CUAR_RIGHT>0 OR UAR_A.CUAR_RIGHT IS NULL AND UAR_B.CUAR_RIGHT>0))";
	strSQL+=" "+strOrderBy;
	
	//qDebug()<<strSQL;
}