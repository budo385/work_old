#ifndef BUSINESSDATATRANSFORMER_H
#define BUSINESSDATATRANSFORMER_H

#include "common/common/dbrecordset.h"



/*!
	\class BusinessDataTransformer
	\brief Transforms data from business entities
	\ingroup Bus_Core

*/


class BusinessDataTransformer 
{
	

public:

	static void Person2User(DbRecordSet &lstPerson, DbRecordSet &lstUser,bool bDefineOutputList=true,bool bRandomPassword=false);
	static void Person2Contact(DbRecordSet &lstPerson, DbRecordSet &lstContact,bool bDefineOutputList=true,bool bCopyPicture=true,bool bDefinePureContactList=false);
	static void Contact2Person(DbRecordSet &lstContact, DbRecordSet &lstPerson,bool bDefineOutputList=true,bool bCopyPicture=true, DbRecordSet *plstOrg=NULL, DbRecordSet *pLstDept=NULL);


    
};

#endif // BUSINESSDATATRANSFORMER_H
