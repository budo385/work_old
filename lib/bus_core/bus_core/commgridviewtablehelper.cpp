#include "commgridviewtablehelper.h"
#include "commgridviewhelper.h"
#include "globalconstants.h"
#include "common/common/datahelper.h"
#include "db_core/db_core/dbsqltableview.h"

CommGridViewTableHelper::CommGridViewTableHelper(QObject *parent/*=0*/)
											   : QObject(parent)
{
	m_recSortRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_COMM_GRID_SORT_RECORDSET));
}

CommGridViewTableHelper::~CommGridViewTableHelper()
{

}

void CommGridViewTableHelper::FirstColumnData(int nCEType, int &nRowsInGrid, DbRecordSet &recEntityRecord, int &nRow, bool &bSideBarMode)
{
	//Locals.
	int nTask			= -1;
	int nInOutDocument	= -1;
	int nAppType		= -1;
	bool bTaskIconExists= false;
	QDateTime datFirstRow, datSecondRow;
	QString strFirstRow;
	QString strSecondRow; 
	QString strThirdRow;
	//Check scheduled time.
	nTask = m_pCmGrdHelper->CheckTask(recEntityRecord,nRow);

	//If not tasked then find appropriate strings. 
	if(nTask == m_pCmGrdHelper->NOT_SCHEDULED || (!recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull() && (recEntityRecord.getDataRef(nRow, "BTKS_IS_TASK_ACTIVE").toInt() == 0  || recEntityRecord.getDataRef(nRow, "BTKS_SYSTEM_STATUS").toInt() >= 3)))
	{
		if (nCEType == m_pCmGrdHelper->VOICECALL)
		{
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BVC_START").toDateTime();
			datSecondRow = recEntityRecord.getDataRef(nRow, "BVC_DURATION").toDateTime();
		}
		else if (nCEType == m_pCmGrdHelper->EMAIL)
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BEM_RECV_TIME").toDateTime();
		else
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BDMD_DAT_LAST_MODIFIED").toDateTime();

		m_hshRow2TaskPriority.insert(nRowsInGrid, -1);
	}
	//Else write start time, due time and who tasked it.
	else
	{
		//recEntityRecord.Dump();
		//In sidebar mode if task is due or overdue show due date if exists if not show start date. Issue #1970.
		if (bSideBarMode)
		{
			if (nTask>m_pCmGrdHelper->SCHEDULED)
			{
				if (recEntityRecord.getDataRef(nRow, "BTKS_DUE").toDateTime().isNull())
					datFirstRow	 = recEntityRecord.getDataRef(nRow, "BTKS_START").toDateTime();
				else
					datFirstRow	 = recEntityRecord.getDataRef(nRow, "BTKS_DUE").toDateTime();
			}
			else
				datFirstRow	 = recEntityRecord.getDataRef(nRow, "BTKS_START").toDateTime();

			//datFirstRow	 = recEntityRecord.getDataRef(nRow, "BTKS_START").toDateTime();
			datSecondRow = recEntityRecord.getDataRef(nRow, "BTKS_DUE").toDateTime();
			if (!recEntityRecord.getDataRef(nRow, "A1.BPER_INITIALS").toString().isEmpty())
				strThirdRow	 = tr("by ") + recEntityRecord.getDataRef(nRow, "A1.BPER_INITIALS").toString();
		}
		else
		{
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BTKS_START").toDateTime();
			datSecondRow = recEntityRecord.getDataRef(nRow, "BTKS_DUE").toDateTime();
			if (!recEntityRecord.getDataRef(nRow, "A1.BPER_INITIALS").toString().isEmpty())
				strThirdRow	 = tr("by ") + recEntityRecord.getDataRef(nRow, "A1.BPER_INITIALS").toString();
		}

		//Fill task and task sort hashes.
		m_hshRow2TaskColor.insert(nRowsInGrid, recEntityRecord.getDataRef(nRow, "BCMT_COLOR").toString());
		if (!recEntityRecord.getDataRef(nRow, "BCMT_PICTURE").toByteArray().isEmpty())
		{
			bTaskIconExists = true;
			m_hshRow2TaskIcon.insert(nRowsInGrid, recEntityRecord.getDataRef(nRow, "BCMT_PICTURE").toByteArray());
		}
		m_hshRow2TaskPriority.insert(nRowsInGrid, recEntityRecord.getDataRef(nRow, "BTKS_PRIORITY").toInt());
		m_hshRow2BTKS_PRIORITY.insert(nRowsInGrid, recEntityRecord.getDataRef(nRow, "BTKS_PRIORITY").toInt());
		m_hshRow2BTKS_TASK_TYPE_ID.insert(nRowsInGrid, recEntityRecord.getDataRef(nRow, "BTKS_TASK_TYPE_ID").toInt());
	}

	strFirstRow	 = datFirstRow.toString("d.M.yyyy h:mm");
	strSecondRow = datSecondRow.toString("d.M.yyyy h:mm");

	//Fill DATE 1 and DATE 2 sort cache.
	m_hshRow2DATE1.insert(nRowsInGrid, datFirstRow.date());
	m_hshRow2TIME1.insert(nRowsInGrid, datFirstRow.time());
	m_hshRow2DATE2.insert(nRowsInGrid, datSecondRow.date());
	m_hshRow2TIME2.insert(nRowsInGrid, datSecondRow.time());

	//Check is it outgoing, incoming or document.
	nInOutDocument = m_pCmGrdHelper->CheckItemDirection(nCEType, recEntityRecord,nRow);

	//Set application type.
	nAppType = nCEType;

	//Set icons.
	QString strPix1;
	if (nTask == m_pCmGrdHelper->NOT_SCHEDULED)
	{
		if (nCEType == m_pCmGrdHelper->DOCUMENT)
		{
			int nDocType = recEntityRecord.getDataRef(nRow, "BDMD_DOC_TYPE").toInt();
			if (nDocType == GlobalConstants::DOC_TYPE_INTERNET_FILE)
			{
				if (recEntityRecord.getDataRef(nRow, "BDMD_IS_CHECK_OUT").toBool())
				{
					if (!recEntityRecord.getDataRef(nRow, "BDMD_CHECK_OUT_USER_ID").isNull())
					{
						int nCheckedUserID = recEntityRecord.getDataRef(nRow, "BDMD_CHECK_OUT_USER_ID").toInt();
						//If this user checked out this file then show checked out icon.
						if (nCheckedUserID == GetLoggedPersonID())
						{
							strPix1 = ":Check_Out_32.png";
							m_hshRow2STATUS.insert(nRowsInGrid, m_pCmGrdHelper->CHECK_OUT_SORT);
						}
						//Else if checked out by someone else then use locked icon.
						else
						{
							strPix1 = ":Locked_32.png";
							m_hshRow2STATUS.insert(nRowsInGrid, m_pCmGrdHelper->LOCKED_SORT);
						}
					}
					//Empty icon.
					else
						strPix1 = m_pCmGrdHelper->GetTaskPixmap(nTask, nRowsInGrid, m_hshRow2STATUS);
				}
				//If document is template then put template flag.
				else if (recEntityRecord.getDataRef(nRow, "BDMD_TEMPLATE_FLAG").toBool())
				{
					strPix1 = ":Template_32.png";
					m_hshRow2STATUS.insert(nRowsInGrid, m_pCmGrdHelper->TEMPLATE_SORT);
				}
				else
					strPix1 = m_pCmGrdHelper->GetTaskPixmap(nTask, nRowsInGrid, m_hshRow2STATUS);
			}
			//If document is template then put template flag.
			else if (recEntityRecord.getDataRef(nRow, "BDMD_TEMPLATE_FLAG").toBool())
			{
				strPix1 = ":Template_32.png";
				m_hshRow2STATUS.insert(nRowsInGrid, m_pCmGrdHelper->TEMPLATE_SORT);
			}
			else
				strPix1 = m_pCmGrdHelper->GetTaskPixmap(nTask, nRowsInGrid, m_hshRow2STATUS);
		}
		else if (nCEType == m_pCmGrdHelper->EMAIL)
		{
			//If document is template then put template flag.
			bool bEmailIsTemplate = recEntityRecord.getDataRef(nRow, "BEM_TEMPLATE_FLAG").toBool();
			if (bEmailIsTemplate)
			{
				strPix1 = ":Template_32.png";
				m_hshRow2STATUS.insert(nRowsInGrid, m_pCmGrdHelper->TEMPLATE_SORT);
			}
			//If email and unread show unread icon but not template (issue #700).
			else if (recEntityRecord.getDataRef(nRow, "BEM_UNREAD_FLAG").toBool() && !recEntityRecord.getDataRef(nRow, "BEM_OUTGOING").toBool())
			{
				strPix1 = ":Unread_Email_32.png";
				m_hshRow2STATUS.insert(nRowsInGrid, m_pCmGrdHelper->UNREAD_EMAIL_SORT);
			}
			//Empty icon.
			else
				strPix1 = m_pCmGrdHelper->GetTaskPixmap(nTask, nRowsInGrid, m_hshRow2STATUS);
		}
		else if(nCEType == m_pCmGrdHelper->VOICECALL)
		{
			//If voice call missed show missed icon.
			if (recEntityRecord.getDataRef(nRow, "BVC_CALL_STATUS").toInt() == m_pCmGrdHelper->CALL_STATE_Missed)
			{
				strPix1 = ":Missed_Call_32.png";
				m_hshRow2STATUS.insert(nRowsInGrid, m_pCmGrdHelper->MISSED_CALL_SORT);
			}
			//Empty icon.
			else
				strPix1 = m_pCmGrdHelper->GetTaskPixmap(nTask, nRowsInGrid, m_hshRow2STATUS);
		}
		else
			strPix1 = m_pCmGrdHelper->GetTaskPixmap(nTask, nRowsInGrid, m_hshRow2STATUS);
	}
	else
		strPix1 = m_pCmGrdHelper->GetTaskPixmap(nTask, nRowsInGrid, m_hshRow2STATUS);

	QString strPix2 = m_pCmGrdHelper->GetCETypePixmap(nCEType, nRowsInGrid, recEntityRecord, nRow, m_hshRow2SUBTYPE);
	QString strPix3 = m_pCmGrdHelper->GetInOutDocumentPixmap(nInOutDocument, nRowsInGrid, m_hshRow2MEDIA);
	QString strPix4;
	m_pCmGrdHelper->GetCommGridFourthIcon(bTaskIconExists, nAppType, nRowsInGrid, nRow, strPix4, recEntityRecord, m_hshRow2APPTYPE, nInOutDocument);
	if (nInOutDocument==m_pCmGrdHelper->INHOUSE)
	{	
		int DocTypeID=recEntityRecord.getDataRef(nRow,"BDMD_DOC_TYPE").toInt();
		strPix3 = QVariant(DocTypeID).toString();
	}

	//Fill originator sort cache.
	m_hshRow2ORIGINATOR.insert(nRowsInGrid, strThirdRow);

	//Fill model hashes.
	m_hshRow2Color.insert(nRowsInGrid, nInOutDocument);
	m_hshRow2Pix1.insert(nRowsInGrid, strPix1);
	m_hshRow2Pix2.insert(nRowsInGrid, strPix2);
	m_hshRow2Pix3.insert(nRowsInGrid, strPix3);
	m_hshRow2Pix4.insert(nRowsInGrid, strPix4);
	m_hshRow2Date1.insert(nRowsInGrid, strFirstRow);
	m_hshRow2Date2.insert(nRowsInGrid, strSecondRow);
	m_hshRow2Autor.insert(nRowsInGrid, strThirdRow);
}

void CommGridViewTableHelper::SecondColumnData(int nCEType, DbRecordSet &recEntityRecord, int &nRow, int &nRowInGrid, bool &bSideBarMode)
{
	//Create widget and layout.
	QString strSubjectString = "";
	QString strDescrString = "";
	//Check does it have a task.
	if (recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull() || (!recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull() && (recEntityRecord.getDataRef(nRow, "BTKS_IS_TASK_ACTIVE").toInt() == 0  || recEntityRecord.getDataRef(nRow, "BTKS_SYSTEM_STATUS").toInt() >= 3)))
	{
		if (nCEType == m_pCmGrdHelper->EMAIL)
		{
			strSubjectString = recEntityRecord.getDataRef(nRow, "BEM_SUBJECT").toString();
			strDescrString	 = recEntityRecord.getDataRef(nRow, "CENT_TOOLTIP").toString();

			//strDescrString	 = recEntityRecord.getDataRef(nRow, "BEM_BODY").toString();
		}
		else if (nCEType == m_pCmGrdHelper->VOICECALL)
			strSubjectString = recEntityRecord.getDataRef(nRow, "BVC_DESCRIPTION").toString();
		//For documents.
		else if (nCEType == m_pCmGrdHelper->DOCUMENT)
		{
			double dblSizeb = recEntityRecord.getDataRef(nRow, "BDMD_SIZE").toDouble();
			//If there is document name then display it.
			strSubjectString = recEntityRecord.getDataRef(nRow, "BDMD_NAME").toString();  //B.T. doc name is always here
			if (bSideBarMode)
				strSubjectString += " " +  DataHelper::GetFormatedFileSize(dblSizeb, 2);
			else
				strSubjectString += "\n" +  DataHelper::GetFormatedFileSize(dblSizeb, 2);

			if (!recEntityRecord.getDataRef(nRow, "CET_ID").isNull())
				strSubjectString += ", " + recEntityRecord.getDataRef(nRow, "CET_CODE").toString() + " " + recEntityRecord.getDataRef(nRow, "CET_NAME").toString();

			strDescrString = recEntityRecord.getDataRef(nRow, "BDMD_DESCRIPTION").toString();
		}
	}
	else
	{
		//Marin wanted first subject of task then description. Since formats are different we will omit that
		//and hope he forget that.
		if (nCEType == m_pCmGrdHelper->EMAIL)
		{
			if(recEntityRecord.getDataRef(nRow, "BTKS_SUBJECT").toString().isEmpty())
				strSubjectString = recEntityRecord.getDataRef(nRow, "BEM_SUBJECT").toString();
			else
				strSubjectString = recEntityRecord.getDataRef(nRow, "BTKS_SUBJECT").toString();
		}
		else
			strSubjectString = recEntityRecord.getDataRef(nRow, "BTKS_SUBJECT").toString();

		if (bSideBarMode)
			strSubjectString = recEntityRecord.getDataRef(nRow, "A2.BPER_INITIALS").toString() + ": " + strSubjectString;

		if (!recEntityRecord.getDataRef(nRow, "BTKS_DESCRIPTION").isNull())
			strDescrString = recEntityRecord.getDataRef(nRow, "BTKS_DESCRIPTION").toString();
		else
		{
			//For email description is too big so skip it.
			if(nCEType == m_pCmGrdHelper->VOICECALL)
				strDescrString = recEntityRecord.getDataRef(nRow, "BTKS_DESCRIPTION").toString();
			else if(nCEType == m_pCmGrdHelper->DOCUMENT)
				strDescrString = recEntityRecord.getDataRef(nRow, "BDMD_NAME").toString();
		}
	}

	QString strSubjectTmp = strSubjectString;
	QString strDescrTmp	  = strDescrString;

	if (strSubjectTmp.startsWith("<html") || strSubjectTmp.startsWith("<!DOCTYPE"))
	{
		strSubjectTmp = DataHelper::ExtractTextFromHTML(strSubjectTmp);
	}

	if (strDescrTmp.startsWith("<html") || strDescrTmp.startsWith("<!DOCTYPE"))
	{
		strDescrTmp = DataHelper::ExtractTextFromHTML(strDescrTmp);
		//Trim spaces, newlines, etc. from start and and.
		strDescrTmp=strDescrTmp.trimmed();
		/*if(strDescrTmp.at(0)==QChar::fromAscii('\n'))
		{
			strDescrTmp=strDescrTmp.trimmed();
		}*/
	}

	QString strDisplay;
	if (strSubjectString.isEmpty())
		strDisplay = strDescrString;
	else
		strDisplay = strSubjectTmp + "\n" + strDescrTmp;

	//Fill model hashes.
	m_hshRow2SecondColDisplay.insert(nRowInGrid, strDisplay);
	m_hshRow2Subject.insert(nRowInGrid, strSubjectString);
	m_hshRow2Descr.insert(nRowInGrid, strDescrString);
	m_hshRow2SUBJECT_SORT.insert(nRowInGrid, strSubjectTmp);
}

void CommGridViewTableHelper::ThirdColumnData(int nCEType, int &nRowsInGrid, DbRecordSet &recEntityRecord, int &nRow, bool &bSideBarMode)
{
	QStringList list = m_pCmGrdHelper->CreateThirdColumnStrings(nCEType, recEntityRecord,nRow);

	//Sort hashes.
	m_hshRow2OWNER.insert(nRowsInGrid, list.value(0));
	m_hshRow2CONTACT.insert(nRowsInGrid, list.value(1));
	m_hshRow2PROJECT.insert(nRowsInGrid, recEntityRecord.getDataRef(nRow, "BUSP_CODE").toString());

	//Fill model caches.
	m_hshRow2Owner.insert(nRowsInGrid, list.value(0));
	m_hshRow2Project.insert(nRowsInGrid, list.value(2));

	//If voice call in sidebar add user initials.
	if (nCEType == m_pCmGrdHelper->VOICECALL && bSideBarMode)
	{
		QString strArrow;
		if(m_pCmGrdHelper->INCOMING == m_pCmGrdHelper->CheckItemDirection(nCEType, recEntityRecord,nRow))
		{
			strArrow = QChar(0x2190);
		}
		else
		{
			strArrow = QChar(0x2192);
		}

		QString strContact = GetLoggedPersonInitials() +" "+ strArrow +" "+ list.value(1);
		m_hshRow2Contact.insert(nRowsInGrid, strContact);
	}
	else
	{
		m_hshRow2Contact.insert(nRowsInGrid, list.value(1));
	}
}

//Fill hashes per row and returns date last modified item used for sorting.
void CommGridViewTableHelper::FillRowHashes(int &nRow, int nCEType, DbRecordSet &recEntityRecord, int &nRowInList)
{
	//Get items values from recordset.
	int nCENT_ID = recEntityRecord.getDataRef(nRowInList, "CENT_ID").toInt();
	int nBTKS_ID = recEntityRecord.getDataRef(nRowInList, "BTKS_ID").toInt();
	int nBTKS_SYSTEM_STATUS = recEntityRecord.getDataRef(nRowInList, "BTKS_SYSTEM_STATUS").toInt();
	int nBPER_ID = recEntityRecord.getDataRef(nRowInList, "A.BPER_ID").toInt();
	int nBCNT_ID = recEntityRecord.getDataRef(nRowInList, "BCNT_ID").toInt();
	int nCENT_SYS = recEntityRecord.getDataRef(nRowInList, "CENT_SYSTEM_TYPE_ID").toInt();
	int nBUSP_ID = recEntityRecord.getDataRef(nRowInList, "BUSP_ID").toInt();
	int nBDMD_IS_CHECK_OUT = 0;
	int nBDMD_CHECK_OUT_USER_ID = 0;
	int nBEM_UNREAD_FLAG = 0;

	int nENTITY_ID;
	if (nCEType == m_pCmGrdHelper->EMAIL)
	{
		nENTITY_ID	= recEntityRecord.getDataRef(nRowInList, "BEM_ID").toInt();
		nBEM_UNREAD_FLAG = recEntityRecord.getDataRef(nRowInList, "BEM_UNREAD_FLAG").toInt();
	}
	else if (nCEType == m_pCmGrdHelper->VOICECALL)
		nENTITY_ID	= recEntityRecord.getDataRef(nRowInList, "BVC_ID").toInt();
	else
	{
		nENTITY_ID	= recEntityRecord.getDataRef(nRowInList, "BDMD_ID").toInt();
		nBDMD_IS_CHECK_OUT = recEntityRecord.getDataRef(nRowInList, "BDMD_IS_CHECK_OUT").toInt();
		nBDMD_CHECK_OUT_USER_ID= recEntityRecord.getDataRef(nRowInList, "BDMD_CHECK_OUT_USER_ID").toInt();
	}

	//Fill hashes.
	m_hshRow2CENT_ID.insert(nRow, nCENT_ID);
	m_hshRow2BTKS_ID.insert(nRow, nBTKS_ID);
	m_hshRow2BTKS_SYSTEM_STATUS.insert(nRow, nBTKS_SYSTEM_STATUS);

	//If there is task then task owner is person id to be shown i owner sapne.
	if (!recEntityRecord.getDataRef(nRowInList, "BTKS_ID").isNull())
		m_hshRow2BPER_ID.insert(nRow, recEntityRecord.getDataRef(nRowInList, "A2.BPER_ID").toInt());
	else
		m_hshRow2BPER_ID.insert(nRow, nBPER_ID);

	m_hshRow2BCNT_ID.insert(nRow, nBCNT_ID);
	m_hshRow2CENT_SYS.insert(nRow, nCENT_SYS);
	m_hshRow2BUSP_ID.insert(nRow, nBUSP_ID);
	m_hshRow2ENTITY_ID.insert(nRow, nENTITY_ID);
	m_hshRow2BDMD_IS_CHECK_OUT.insert(nRow, nBDMD_IS_CHECK_OUT);
	m_hshRow2BEM_UNREAD_FLAG.insert(nRow, nBEM_UNREAD_FLAG);
	m_hshRow2BDMD_CHECK_OUT_USER_ID.insert(nRow, nBDMD_CHECK_OUT_USER_ID);
}

void CommGridViewTableHelper::ClearHashes()
{
	//Data and Sorting hashes.
	m_hshRow2CENT_ID.clear();
	m_hshRow2BTKS_ID.clear();
	m_hshRow2BTKS_SYSTEM_STATUS.clear();
	m_hshRow2BPER_ID.clear();
	m_hshRow2BCNT_ID.clear();
	m_hshRow2CENT_SYS.clear();
	m_hshRow2BUSP_ID.clear();
	m_hshRow2ENTITY_ID.clear();
	m_hshRow2BDMD_IS_CHECK_OUT.clear();
	m_hshRow2BEM_UNREAD_FLAG.clear();
	m_hshRow2BDMD_CHECK_OUT_USER_ID.clear();
	m_hshRow2DATE1.clear();			
	m_hshRow2DATE2.clear();			
	m_hshRow2STATUS.clear();		
	m_hshRow2APPTYPE.clear();		
	m_hshRow2ORIGINATOR.clear();	
	m_hshRow2OWNER.clear();			
	m_hshRow2CONTACT.clear();		
	m_hshRow2PROJECT.clear();		
	m_hshRow2SUBTYPE.clear();		
	m_hshRow2MEDIA.clear();			
	m_hshRow2TIME1.clear();			
	m_hshRow2TIME2.clear();
	m_hshRow2SUBJECT_SORT.clear();
	m_hshRow2BTKS_PRIORITY.clear();
	m_hshRow2BTKS_TASK_TYPE_ID.clear();

	//Hashes to pass to model;
	m_hshRow2Color.clear();
	m_hshRow2Pix1.clear();
	m_hshRow2Pix2.clear();
	m_hshRow2Pix3.clear();
	m_hshRow2Pix4.clear();
	m_hshRow2Date1.clear();
	m_hshRow2Date2.clear();
	m_hshRow2Autor.clear();
	m_hshRow2Subject.clear();
	m_hshRow2Descr.clear();
	m_hshRow2SecondColDisplay.clear();
	m_hshRow2Owner.clear();
	m_hshRow2Contact.clear();
	m_hshRow2Project.clear();
	m_hshRow2TaskColor.clear();
	m_hshRow2TaskIcon.clear();
	m_hshRow2TaskPriority.clear();
}

void CommGridViewTableHelper::SortTableData(int nRowCount, int nGridType, DbRecordSet *recSortRecordSet /*= NULL*/, bool bForceSort /*= true*/)
{
	//m_recSortRecordSet.Dump();

	//Clear sort recordset.
	m_recSortRecordSet.clear();
	m_recSortRecordSet.addRow(nRowCount);
	for(int i = 0; i < nRowCount; i++)
	{
		m_recSortRecordSet.setData(i, "ROW_IN_TABLE", i);
		m_recSortRecordSet.setData(i, "CONTACT", m_hshRow2CONTACT.value(i));
		m_recSortRecordSet.setData(i, "PROJECT", m_hshRow2PROJECT.value(i));
		m_recSortRecordSet.setData(i, "DATE_1", m_hshRow2DATE1.value(i));
		m_recSortRecordSet.setData(i, "DATE_2", m_hshRow2DATE2.value(i));
		m_recSortRecordSet.setData(i, "STATUS", m_hshRow2STATUS.value(i));
		m_recSortRecordSet.setData(i, "APP_TYPE", m_hshRow2APPTYPE.value(i));
		m_recSortRecordSet.setData(i, "SUBTYPE", m_hshRow2SUBTYPE.value(i));
		m_recSortRecordSet.setData(i, "MEDIA", m_hshRow2MEDIA.value(i));
		m_recSortRecordSet.setData(i, "ORIGINATOR", m_hshRow2ORIGINATOR.value(i));
		m_recSortRecordSet.setData(i, "OWNER", m_hshRow2OWNER.value(i));
		m_recSortRecordSet.setData(i, "TIME_1", m_hshRow2TIME1.value(i));
		m_recSortRecordSet.setData(i, "TIME_2", m_hshRow2TIME2.value(i));
		m_recSortRecordSet.setData(i, "SUBJECT", m_hshRow2SUBJECT_SORT.value(i)); //First line in second column - pro view.
		m_recSortRecordSet.setData(i, "TASK_PRIORITY", m_hshRow2BTKS_PRIORITY.value(i)); //Task priority.
		m_recSortRecordSet.setData(i, "TASK_TYPE_ID", m_hshRow2BTKS_TASK_TYPE_ID.value(i)); //Task type.
	}

	//Create default sort.
	SortDataList defaultSortList;
	SortData sortDate1(3 /*DATE_1*/,  1 /* = DESCENDING*/);
	SortData sortTime1(11 /*TIME_1*/, 1 /* = DESCENDING*/);
	SortData sortTime2(12 /*TIME_2*/, 1 /* = DESCENDING*/);
	SortData sortPriority(14 /*TASK_PRIORITY*/, 1 /* = DESCENDING*/);
	defaultSortList << sortDate1 << sortTime1;
	//For calendar grid (tasks) add also task priority sort as default.
	if(nGridType==m_pCmGrdHelper->CONTACT_GRID)
		defaultSortList << sortDate1 << sortTime1 << sortPriority;

	DbRecordSet recTmp;
	if (recSortRecordSet != NULL)
		recTmp = *recSortRecordSet;

	if (bForceSort)
	{
		if ((!recTmp.getRowCount()) && (!m_recSortByRecordset.getRowCount()))
			//Default sort - by date 1.
			m_recSortRecordSet.sortMulti(defaultSortList);
	}
	//Check is there something in sort recordset.
	if ( (!recTmp.getRowCount() && !m_recSortByRecordset.getRowCount()) || (bForceSort && !recTmp.getRowCount()))
		//Default sort - by date 1.
		m_recSortRecordSet.sortMulti(defaultSortList);
	//Sort by 
	else
	{
		SortDataList sortList;

		//If outside sort recordset is not null then sort on base of it.
		if (recTmp.getRowCount())
			m_recSortByRecordset = recTmp;

		int nSortRowCount = m_recSortByRecordset.getRowCount();
		for (int i = 0; i < nSortRowCount; i++)
		{
			//Get sort column name and order from recordset that came from outside.
			QString strSortColumn = m_recSortByRecordset.getDataRef(i, "BOGW_COLUMN_NAME").toString();
			int nSortOrder		  = m_recSortByRecordset.getDataRef(i, "BOGW_SORT_ORDER").toInt(); //0 - ASC, 1 - DESC;

			//Get sort column idx from sort recordset.
			int nColumnIDx		  = m_recSortRecordSet.getColumnIdx(strSortColumn);
			//Create sort data structure and place it in sort list.
			SortData sortData(nColumnIDx, nSortOrder);
			sortList << sortData;
		}

		//Check are dates used for sorting (if not add date1 for second sort).
		int nDate1Row = m_recSortByRecordset.find("BOGW_COLUMN_NAME", QString("DATE_1"), true);
		int nDate2Row = m_recSortByRecordset.find("BOGW_COLUMN_NAME", QString("DATE_2"), true);

		//If not used neither of dates to sort use date1 (and time1) to sort.
		if((nDate1Row<0 && nDate2Row<0))
		{
			sortList << sortDate1 <<  sortTime1;
		}
		//If date1 used and date2 not add time1
		else if (nDate1Row>=0 && nDate2Row<0)
			sortList << sortTime1;
		//If date2 used and date1 not add time2
		else if (nDate1Row<0 && nDate2Row>=0)
			sortList << sortTime2;
		//If both dates used check the order and add both times.
		else
		{
			if (nDate1Row > nDate2Row)
				sortList << sortTime2 << sortTime1;
			else
				sortList << sortTime1 << sortTime2;
		}

		//Sort recordset.
		m_recSortRecordSet.sortMulti(sortList);
	}

	//Create tmp hashes and reorder old ones.
	QHash<int, int> hshRow2CENT_ID,
		hshRow2BTKS_ID,
		hshRow2BTKS_SYSTEM_STATUS, 
		hshRow2BPER_ID,
		hshRow2BCNT_ID,
		hshRow2CENT_SYS,
		hshRow2BUSP_ID,
		hshRow2ENTITY_ID,
		hshRow2BDMD_IS_CHECK_OUT,
		hshRow2BEM_UNREAD_FLAG,
		hshRow2BDMD_CHECK_OUT_USER_ID,
		hshRow2STATUS,
		hshRow2APPTYPE,
		hshRow2SUBTYPE,
		hshRow2MEDIA;
	QHash<int, QDate> hshRow2DATE1,
		hshRow2DATE2;
	QHash<int, QTime> hshRow2TIME1,
		hshRow2TIME2;
	QHash<int, QString> hshRow2ORIGINATOR,
		hshRow2CONTACT,
		hshRow2PROJECT,
		hshRow2OWNER,
		hshRow2SUBJECT_SORT;
	QHash<int,int> hshRow2BTKS_PRIORITY,
		hshRow2BTKS_TASK_TYPE_ID;


	//Model hashes.
	QHash<int, int> hshRow2Color;
	QHash<int, QString> hshRow2Pix1,
		hshRow2Pix2,
		hshRow2Pix3,
		hshRow2Pix4,
		hshRow2Date1,
		hshRow2Date2,
		hshRow2Autor,
		hshRow2Subject,
		hshRow2Descr,
		hshRow2SecondColDisplay,
		hshRow2Owner,
		hshRow2Contact,
		hshRow2Project;
	QHash<int,QString> hshRow2TaskColor;
	QHash<int,QByteArray> hshRow2TaskIcon;
	QHash<int,int> hshRow2TaskPriorty;

	hshRow2CENT_ID	 = m_hshRow2CENT_ID;
	hshRow2BTKS_ID	 = m_hshRow2BTKS_ID;
	hshRow2BTKS_SYSTEM_STATUS = m_hshRow2BTKS_SYSTEM_STATUS;
	hshRow2BPER_ID	 = m_hshRow2BPER_ID;
	hshRow2BCNT_ID	 = m_hshRow2BCNT_ID;
	hshRow2CENT_SYS	 = m_hshRow2CENT_SYS;
	hshRow2BUSP_ID	 = m_hshRow2BUSP_ID;
	hshRow2ENTITY_ID = m_hshRow2ENTITY_ID;
	hshRow2BDMD_IS_CHECK_OUT = m_hshRow2BDMD_IS_CHECK_OUT;
	hshRow2BEM_UNREAD_FLAG = m_hshRow2BEM_UNREAD_FLAG;
	hshRow2BDMD_CHECK_OUT_USER_ID = m_hshRow2BDMD_CHECK_OUT_USER_ID;
	hshRow2STATUS	 = m_hshRow2STATUS;
	hshRow2APPTYPE	 = m_hshRow2APPTYPE;
	hshRow2CONTACT	 = m_hshRow2CONTACT;
	hshRow2PROJECT	 = m_hshRow2PROJECT;
	hshRow2DATE1	 = m_hshRow2DATE1;
	hshRow2DATE2	 = m_hshRow2DATE2;
	hshRow2SUBTYPE	 = m_hshRow2SUBTYPE;
	hshRow2MEDIA	 = m_hshRow2MEDIA;
	hshRow2ORIGINATOR= m_hshRow2ORIGINATOR;
	hshRow2OWNER	 = m_hshRow2OWNER;
	hshRow2TIME1	 = m_hshRow2TIME1;
	hshRow2TIME2	 = m_hshRow2TIME2;
	hshRow2SUBJECT_SORT		= m_hshRow2SUBJECT_SORT;
	hshRow2BTKS_PRIORITY	= m_hshRow2BTKS_PRIORITY;
	hshRow2BTKS_TASK_TYPE_ID= m_hshRow2BTKS_TASK_TYPE_ID;


	//Model hashes.
	hshRow2Color	= m_hshRow2Color;
	hshRow2Pix1		= m_hshRow2Pix1;
	hshRow2Pix2		= m_hshRow2Pix2;
	hshRow2Pix3		= m_hshRow2Pix3;
	hshRow2Pix4		= m_hshRow2Pix4;
	hshRow2Date1	= m_hshRow2Date1;
	hshRow2Date2	= m_hshRow2Date2;
	hshRow2Autor	= m_hshRow2Autor;
	hshRow2Subject	= m_hshRow2Subject;
	hshRow2Descr	= m_hshRow2Descr;
	hshRow2SecondColDisplay = m_hshRow2SecondColDisplay;
	hshRow2Owner	= m_hshRow2Owner;
	hshRow2Contact	= m_hshRow2Contact;
	hshRow2Project	= m_hshRow2Project;
	hshRow2TaskColor	= m_hshRow2TaskColor;
	hshRow2TaskIcon		= m_hshRow2TaskIcon;
	hshRow2TaskPriorty	= m_hshRow2TaskPriority;



	//Clear old hashes.
	m_hshRow2CENT_ID.clear();
	m_hshRow2BTKS_ID.clear();
	m_hshRow2BTKS_SYSTEM_STATUS.clear();
	m_hshRow2BPER_ID.clear();
	m_hshRow2BCNT_ID.clear();
	m_hshRow2CENT_SYS.clear();
	m_hshRow2BUSP_ID.clear();
	m_hshRow2ENTITY_ID.clear();
	m_hshRow2BDMD_IS_CHECK_OUT.clear();
	m_hshRow2BEM_UNREAD_FLAG.clear();
	m_hshRow2BDMD_CHECK_OUT_USER_ID.clear();
	m_hshRow2DATE1.clear();
	m_hshRow2DATE2.clear();
	m_hshRow2STATUS.clear();
	m_hshRow2APPTYPE.clear();
	m_hshRow2ORIGINATOR.clear();
	m_hshRow2CONTACT.clear();
	m_hshRow2PROJECT.clear();
	m_hshRow2SUBTYPE.clear();
	m_hshRow2MEDIA.clear();
	m_hshRow2OWNER.clear();
	m_hshRow2TIME1.clear();
	m_hshRow2TIME2.clear();
	m_hshRow2SUBJECT_SORT.clear();
	m_hshRow2SUBJECT_SORT.clear();
	m_hshRow2BTKS_PRIORITY.clear();
	m_hshRow2BTKS_TASK_TYPE_ID.clear();


	//Model hashes.
	m_hshRow2Color.clear();
	m_hshRow2Pix1.clear();
	m_hshRow2Pix2.clear();
	m_hshRow2Pix3.clear();
	m_hshRow2Pix4.clear();
	m_hshRow2Date1.clear();
	m_hshRow2Date2.clear();
	m_hshRow2Autor.clear();
	m_hshRow2Subject.clear();
	m_hshRow2Descr.clear();
	m_hshRow2SecondColDisplay.clear();
	m_hshRow2Owner.clear();
	m_hshRow2Contact.clear();
	m_hshRow2Project.clear();
	m_hshRow2TaskColor.clear();
	m_hshRow2TaskIcon.clear();
	m_hshRow2TaskPriority.clear();

	//Start reordering.
	for (int i = 0; i < nRowCount; ++i)
	{
		//Get old row number.
		int nOldRow = m_recSortRecordSet.getDataRef(i, "ROW_IN_TABLE").toInt();

		//Set values in hashes.
		m_hshRow2CENT_ID.insert(i, hshRow2CENT_ID.value(nOldRow));
		m_hshRow2BTKS_ID.insert(i, hshRow2BTKS_ID.value(nOldRow));
		m_hshRow2BTKS_SYSTEM_STATUS.insert(i, hshRow2BTKS_SYSTEM_STATUS.value(nOldRow));
		m_hshRow2BPER_ID.insert(i, hshRow2BPER_ID.value(nOldRow));
		m_hshRow2BCNT_ID.insert(i, hshRow2BCNT_ID.value(nOldRow));
		m_hshRow2CENT_SYS.insert(i, hshRow2CENT_SYS.value(nOldRow));
		m_hshRow2BUSP_ID.insert(i, hshRow2BUSP_ID.value(nOldRow));
		m_hshRow2ENTITY_ID.insert(i, hshRow2ENTITY_ID.value(nOldRow));
		m_hshRow2BDMD_IS_CHECK_OUT.insert(i, hshRow2BDMD_IS_CHECK_OUT.value(nOldRow));
		m_hshRow2BEM_UNREAD_FLAG.insert(i, hshRow2BEM_UNREAD_FLAG.value(nOldRow));
		m_hshRow2BDMD_CHECK_OUT_USER_ID.insert(i, hshRow2BDMD_CHECK_OUT_USER_ID.value(nOldRow));
		m_hshRow2DATE1.insert(i, hshRow2DATE1.value(nOldRow));
		m_hshRow2DATE2.insert(i, hshRow2DATE2.value(nOldRow));
		m_hshRow2STATUS.insert(i, hshRow2STATUS.value(nOldRow));
		m_hshRow2APPTYPE.insert(i, hshRow2APPTYPE.value(nOldRow));
		m_hshRow2ORIGINATOR.insert(i, hshRow2ORIGINATOR.value(nOldRow));
		m_hshRow2CONTACT.insert(i, hshRow2CONTACT.value(nOldRow));
		m_hshRow2PROJECT.insert(i, hshRow2PROJECT.value(nOldRow));
		m_hshRow2SUBTYPE.insert(i, hshRow2SUBTYPE.value(nOldRow));
		m_hshRow2MEDIA.insert(i, hshRow2MEDIA.value(nOldRow));
		m_hshRow2OWNER.insert(i, hshRow2OWNER.value(nOldRow));
		m_hshRow2TIME1.insert(i, hshRow2TIME1.value(nOldRow));
		m_hshRow2TIME2.insert(i, hshRow2TIME2.value(nOldRow));
		m_hshRow2SUBJECT_SORT.insert(i, hshRow2SUBJECT_SORT.value(nOldRow));
		m_hshRow2BTKS_PRIORITY.insert(i, hshRow2BTKS_PRIORITY.value(nOldRow));
		m_hshRow2BTKS_TASK_TYPE_ID.insert(i, hshRow2BTKS_TASK_TYPE_ID.value(nOldRow));

		//Set values in model hashes.
		m_hshRow2Color.insert(i, hshRow2Color.value(nOldRow));
		m_hshRow2Pix1.insert(i, hshRow2Pix1.value(nOldRow));
		m_hshRow2Pix2.insert(i, hshRow2Pix2.value(nOldRow));
		m_hshRow2Pix3.insert(i, hshRow2Pix3.value(nOldRow));
		m_hshRow2Pix4.insert(i, hshRow2Pix4.value(nOldRow));
		m_hshRow2Date1.insert(i, hshRow2Date1.value(nOldRow));
		m_hshRow2Date2.insert(i, hshRow2Date2.value(nOldRow));
		m_hshRow2Autor.insert(i, hshRow2Autor.value(nOldRow));
		m_hshRow2Subject.insert(i, hshRow2Subject.value(nOldRow));
		m_hshRow2Descr.insert(i, hshRow2Descr.value(nOldRow));
		m_hshRow2SecondColDisplay.insert(i, hshRow2SecondColDisplay.value(nOldRow));
		m_hshRow2Owner.insert(i, hshRow2Owner.value(nOldRow));
		m_hshRow2Contact.insert(i, hshRow2Contact.value(nOldRow));
		m_hshRow2Project.insert(i, hshRow2Project.value(nOldRow));
		m_hshRow2TaskColor.insert(i, hshRow2TaskColor.value(nOldRow));
		m_hshRow2TaskIcon.insert(i, hshRow2TaskIcon.value(nOldRow));
		m_hshRow2TaskPriority.insert(i, hshRow2TaskPriorty.value(nOldRow));

		//Set new row in sort recordset.
		m_recSortRecordSet.setData(i, "ROW_IN_TABLE", i);
	}
}

void CommGridViewTableHelper::CheckAndFixIcon3AndIcon4(DbRecordSet &recApplications,QHash<int, QString> &hshDocIconNames)
{
	QHash<int,QByteArray> hshApplicationIcons;

	int nRowCount = recApplications.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QByteArray bytePicture=recApplications.getDataRef(i, "BDMA_ICON").toByteArray();
		hshApplicationIcons.insert(recApplications.getDataRef(i, "BDMA_ID").toInt(), bytePicture);
	}

	int nCount=m_hshRow2Pix3.count();
	for (int i = 0; i<nCount; i++)
	{
		QString strPix3 = m_hshRow2Pix3.value(i);
		QString strPix4 = m_hshRow2Pix4.value(i);

		if (!strPix3.startsWith(":"))
		{
			int nIconID = QVariant(strPix3).toInt();
			m_hshRow2Pix3.insert(i,hshDocIconNames.value(nIconID));
		}

		m_hshRow2Pix4ByteArray.insert(i, NULL);
		if(!strPix4.startsWith(":"))
		{
			if (strPix4 == "-1")
			{
				//Do nothing and on client take m_hshRow2TaskIcon byte array to make 
			}
			else
			{
				int nIconID = QVariant(strPix4).toInt();
				if (!hshApplicationIcons.contains(nIconID))
					Q_ASSERT(false);

				m_hshRow2Pix4ByteArray.insert(i, hshApplicationIcons.value(nIconID));
			}
		}
	}
}
