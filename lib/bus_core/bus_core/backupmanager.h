#ifndef BACKUPMANAGER_H
#define BACKUPMANAGER_H

#include "common/common/status.h"
#include "bus_core/bus_core/dbactualizator.h"
#include "db_core/db_core/dbconnsettings.h"
#include <QReadWriteLock>


/*!
	\class BackupManager
	\brief Base class for client & server backup manager
	\ingroup Bus_Core

	Multi thread safe
*/
class BackupManager : public QObject
{
	Q_OBJECT

public:
	BackupManager();
	virtual ~BackupManager();

	enum BackupFreq
	{
		BCP_FREQ_NONE=0,
		BCP_FREQ_MONTHLY=1,
		BCP_FREQ_WEEKLY=2,
		BCP_FREQ_DAILY=3,
		BCP_FREQ_HOURLY=4
	};

	virtual void	Initialize(Status &pStatus,const DbConnectionSettings &ConnSettings, bool bShutDownDatabaseWhenBackup=false,bool bUseApplicationInstallDirForBackupDir=true);
	virtual void	StartUpCheck(Status &pStatus);
	virtual	void	Backup(Status &pStatus);				
	virtual	void	Restore(Status &pStatus,QString strFileNameForRestore);

	//set & get data:
	void	SetBackupData(Status &pStatus,int nBackupFreq, QString strBackupPath, QString datBackupLastDate, QString strRestoreOnNextStart,int nBackupDay,QString strBackupTime, int nDeleteOldBackupsDay);
	void	SetBackupData(Status &pStatus,int nBackupFreq, QString strBackupPath, int nBackupDay,QString strBackupTime, int nDeleteOldBackupsDay);
	void	GetBackupData(Status &pStatus,int& nBackupFreq,QString& strBackupPath,QString& datBackupLastDate,QString& strRestoreOnNextStart,int& nBackupDay,QString& strBackupTime, int& nDeleteOldBackupsDay);
		
	//template operations
	void	LoadTemplateList(Status &pStatus,DbRecordSet &lstTemplates);
	void	RestoreFromTemplate(Status &pStatus,QString strTemplateFile);

	//get backup, set backup, delete backup file
	void	LoadBackupFile(Status &pStatus, QString strBackupName,QByteArray &bcpContent);
	void	SaveBackupFile(Status &pStatus, QString strBackupName,QByteArray &bcpContent);
	void	DeleteBackupFile(Status &pStatus, QString strBackupName);
	void	LoadBackupList(Status &pStatus,DbRecordSet &lstBackups);
	int		GetNextBackupStart(Status &pStatus,QDateTime timStart=QDateTime::currentDateTime()); //B.T. issue 2490
	void	CheckForDemoDatabase(Status &pStatus, bool bEnableDemo, int nMaxSizeKb, int nWarnSizeKb); 
	void	RecalcDemoDatabaseSize(Status &pStatus, int &nCurrSizeKb); 


signals:
	void SettingsChanged();
	void RestoreCommandIssued(int nCallerID);
	void BackupCommandIssued(int nCallerID);

protected:
	bool CheckDatabaseForReorganization();
	bool CheckDatabaseForBackup(QDateTime &datLastBcp); //returns date of next scheduled backup, if true
	bool CheckDatabaseForRestore();
	virtual QString ExecBackup(Status &pStatus);
	virtual void	ExecRestore(Status &pStatus);
	virtual void	ExecReorganization(Status &pStatus);
	virtual void	ClearDatabaseAfterRestore(Status &pStatus)=0;		//implement this one
	virtual void	StartDatabase()=0;
	virtual void	CloseDatabase()=0;


	//DbSqlManager *m_DbManager;
	DbConnectionSettings m_DbConnSettings;
	DbActualizator m_DbActualizator;

	//BACKUP:
	int					m_nBackupFreq;		
	QString				m_strBackupPath;		
	QString				m_datBackupLastDate;		
	QString				m_strRestoreOnNextStart;		
	QString				m_strBackupTime;
	int					m_nBackupDay;
	QString				m_strTemplatePath;		
	bool				m_bShutDownDatabaseWhenBackup;
	QReadWriteLock		m_RWMutex;		//data lock: only 1 can change, more then 1 thread can read bcp data
	QMutex				m_BackupLock;	//operation lock: 1 thread is allowed to exec backup
	bool				m_bRestoreInProgress;
	int					m_nDeleteOldBackupsDay;


};

#endif // BACKUPMANAGER_H
