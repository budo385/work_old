#ifndef TREELIST_H
#define TREELIST_H

#include <QList>
#include "fpsets.h"

class TreeList : public QList<FP_ITEM>
{
	//class needed to sort the list
	class TreeListComparator 
	{
		public:
			bool operator ()(const FP_ITEM &a, const FP_ITEM &b) const;
	};

public:
	int FindByCode(int nCode);
	int FindNode(QList<int> lstPos);
	int FindNextSibling(int nIdx);
	int FindChild(int nIdx, int nRow);
	int FindParent(int nIdx);
	int GetChildCount(int nIdx);
	int GetChildCountRecursive(int nIdx);	//include grand children, ...
	bool GetNodePos(int nIdx, QList<int> &lstPos);

	bool RemoveNode(int nIdx);
	bool CopyNode(int nIdx, int nParent);
	bool CopyNode(TreeList &srcTree, int nIdx, int nParent);

	bool AddChild(int nIdx, FP_ITEM &item);

	void DumpPos(QList<int> lstPos);
	void DumpTree();

	void Sort();
};

#endif
