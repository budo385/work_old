#include "licensestreamreader.h"
//#include <openssl/err.h>
//#include <openssl/sha.h>
//#include <openssl/md5.h>
//#include <openssl/evp.h>

LicenseStreamReader::LicenseStreamReader()
{
	m_nDataLen = 0;
	m_pInFile = NULL;
}

void LicenseStreamReader::AttachSource(FILE *pInFile)
{
	m_pInFile = pInFile;
}

void LicenseStreamReader::SetPublicKey(const char *szKey)
{
	m_key.ReadPublicKeyFromBuffer(szKey, strlen(szKey));
}

bool LicenseStreamReader::Read()
{
	char szBuffer[256];	//NOTE: read buffer must be the size of a single block ?
	int nLen = fread(szBuffer, 1, sizeof(szBuffer), m_pInFile);
	if(nLen > 0)
	{
		//decrypt the contents
		int nLen2 = m_key.Verify(szBuffer, nLen, m_szDecryptedData+m_nDataLen);
		if(nLen2 > 0){
			m_nDataLen += nLen2;
			return true;
		}
	}
	return false;
}

bool LicenseStreamReader::ReadBytes(void *pszBuffer, int nLen)
{
	if(NULL == pszBuffer || nLen <= 0)
		return false;

	if(m_nDataLen < nLen)
		Read();

	if(m_nDataLen >= nLen)
	{
		//copy data
		memcpy(pszBuffer, m_szDecryptedData, nLen);	

		//cleanup consumed data from the buffer
		memmove(m_szDecryptedData, m_szDecryptedData+nLen, m_nDataLen-nLen);
		m_nDataLen -= nLen;

		return true;
	}

	return false;
}

bool LicenseStreamReader::ReadString(char *pszBuffer, int nLen)
{
	if(ReadBytes(pszBuffer, nLen)){
		pszBuffer[nLen] = '\0';
		return true;
	}
	return false;
}
