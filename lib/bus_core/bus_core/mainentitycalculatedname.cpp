#include "mainentitycalculatedname.h"
#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "db_core/db_core/dbsqltabledefinition.h"

MainEntityCalculatedName::MainEntityCalculatedName()
:m_bCanCalculate(false)
{
}

MainEntityCalculatedName::~MainEntityCalculatedName()
{

}

//nCodeIdx and nNameIdx are used for all other entities, if not supplied, it will not init
void MainEntityCalculatedName::Initialize(int nEntityID,DbRecordSet &rowData,int nCodeIdx, int nNameIdx)
{
	m_nEntityID=nEntityID;

	switch(nEntityID)
	{
	case ENTITY_BUS_CONTACT:
		{
			m_nNameIdx=nNameIdx;	//->BCNT_NAME
			m_nLastNameIdx=rowData.getColumnIdx("BCNT_LASTNAME");
			m_nFirstNameIdx=rowData.getColumnIdx("BCNT_FIRSTNAME");
			m_nOrgNameIdx=rowData.getColumnIdx("BCNT_ORGANIZATIONNAME");
			if (m_nOrgNameIdx !=-1 && m_nFirstNameIdx!=-1 && m_nLastNameIdx!=-1)
				m_bCanCalculate=true;
			else
				m_bCanCalculate=false;
		}
		break;
	case ENTITY_BUS_PERSON:
		{
			m_nNameIdx=nNameIdx;   //->BPER_NAME
			m_nLastNameIdx=rowData.getColumnIdx("BPER_LAST_NAME");
			m_nFirstNameIdx=rowData.getColumnIdx("BPER_FIRST_NAME");
			m_nDeptCodeIdx=rowData.getColumnIdx("BDEPT_CODE");
			m_nDeptNameIdx=rowData.getColumnIdx("BDEPT_NAME");
			
			if (m_nFirstNameIdx!=-1 && m_nLastNameIdx!=-1)
				m_bCanCalculate=true;
			else
				m_bCanCalculate=false;
		}
	    break;
	case ENTITY_CORE_USER:
		{
			m_nLastNameIdx=rowData.getColumnIdx("CUSR_LAST_NAME");
			m_nFirstNameIdx=rowData.getColumnIdx("CUSR_FIRST_NAME");
			if (m_nFirstNameIdx!=-1 && m_nLastNameIdx!=-1)
				m_bCanCalculate=true;
			else
				m_bCanCalculate=false;
		}
		break;
	default:
		{
			m_nCodeIdx=nCodeIdx;
			m_nNameIdx=nNameIdx;
			if (m_nCodeIdx!=-1 || m_nNameIdx!=-1) //one is enough
				m_bCanCalculate=true;
			else
				m_bCanCalculate=false;
		}

	    break;
	}


}

//if previously initialized, it will calculate names
//if nulls return empty string
QString MainEntityCalculatedName::GetCalculatedName(DbRecordSet &rowData, int nRow)
{
	//if not calculated, abort
	if (!m_bCanCalculate)
		return "";

	//if rows doesn't match:
	if (nRow>=rowData.getRowCount())
		return "";

	switch(m_nEntityID)
	{

	
	case ENTITY_BUS_PERSON:
		{
			if (m_nNameIdx!=-1) return rowData.getDataRef(nRow,m_nNameIdx).toString(); //if calc field exists, return it
			//Last Name, First Name
			//Second column: Dept Code,Dept Name
			if (rowData.getDataRef(nRow,m_nLastNameIdx).isNull()) return "";
			return rowData.getDataRef(nRow,m_nLastNameIdx).toString()+", "+rowData.getDataRef(nRow,m_nFirstNameIdx).toString();
		}
		break;

	case ENTITY_BUS_CONTACT: //Org,Last,First , or Org, or Last,First
		{
			if (m_nNameIdx!=-1) return rowData.getDataRef(nRow,m_nNameIdx).toString(); //if calc field exists, return it
			QString strName;
			QString strFirstName=rowData.getDataRef(nRow,m_nFirstNameIdx).toString();
			QString strLastName=rowData.getDataRef(nRow,m_nLastNameIdx).toString();
			QString strOrgName=rowData.getDataRef(nRow,m_nOrgNameIdx).toString();
			if (!strLastName.isEmpty())
				strName=strLastName;
			if (!strFirstName.isEmpty())
				strName+=" "+strFirstName.trimmed();
			if (!strOrgName.isEmpty()){
				if (strName.isEmpty())
					strName=strOrgName;
				else
					strName=strName+", "+strOrgName;
			}
/*
			if (rowData.getDataRef(nRow,m_nLastNameIdx).isNull() && rowData.getDataRef(nRow,m_nOrgNameIdx).isNull() && !rowData.getDataRef(nRow,m_nFirstNameIdx).isEmpty()) 
				strName=rowData.getDataRef(nRow,m_nFirstNameIdx).toString();
			else if(rowData.getDataRef(nRow,m_nOrgNameIdx).toString().isEmpty())
				strName=rowData.getDataRef(nRow,m_nLastNameIdx).toString()+QString::trimmed(" "+rowData.getDataRef(nRow,m_nFirstNameIdx).toString());
			else
				if (!rowData.getDataRef(nRow,m_nLastNameIdx).toString().isEmpty())
					strName=rowData.getDataRef(nRow,m_nLastNameIdx).toString()+QString::trimmed(" "+rowData.getDataRef(nRow,m_nFirstNameIdx).toString())+", "+rowData.getDataRef(nRow,m_nOrgNameIdx).toString();
				else
					strName=rowData.getDataRef(nRow,m_nOrgNameIdx).toString();
*/
			return strName;
		}
		break;

	case ENTITY_CORE_USER:
		if (rowData.getDataRef(nRow,m_nLastNameIdx).isNull()) return "";
		return rowData.getDataRef(nRow,m_nLastNameIdx).toString()+", "+rowData.getDataRef(nRow,m_nFirstNameIdx).toString();
		break;

	default:
		if (m_nCodeIdx!=-1 && m_nNameIdx!=-1)   //Code + Name for Tree (if got code)
			return rowData.getDataRef(nRow,m_nCodeIdx).toString()+"  "+rowData.getDataRef(nRow,m_nNameIdx).toString();
		else if (m_nNameIdx!=-1)
			return rowData.getDataRef(nRow,m_nNameIdx).toString();
		else
			return "";
	}

}

//if previously initialized, it will calculate names
void MainEntityCalculatedName::FillCalculatedNames(DbRecordSet &rowData, int nCalcName1_Idx,int nCalcName2_Idx)
{

	if (!m_bCanCalculate)
		return;



	switch(m_nEntityID)
	{


	case ENTITY_BUS_PERSON:
		{
			//Q_ASSERT(nCalcName2_Idx!=-1);
			QString strName;
			QString strDeptName;
			int nSize=rowData.getRowCount();
			for(int i=0;i<nSize;++i)
			{
				if (!rowData.getDataRef(i,m_nFirstNameIdx).toString().isEmpty())
				{
					strName=rowData.getDataRef(i,m_nLastNameIdx).toString()+", "+rowData.getDataRef(i,m_nFirstNameIdx).toString();
					if (strName.trimmed()==",")
						strName="";
				}
				else
				{
					strName=rowData.getDataRef(i,m_nLastNameIdx).toString();
				}
				
				if (nCalcName2_Idx!=-1)
				{
					if(!rowData.getDataRef(i,m_nDeptCodeIdx).toString().isEmpty() && m_nDeptCodeIdx!=-1 && m_nDeptNameIdx!=-1)
						strDeptName=rowData.getDataRef(i,m_nDeptCodeIdx).toString()+" "+rowData.getDataRef(i,m_nDeptNameIdx).toString();
					else
						strDeptName="";
					rowData.setData(i,nCalcName2_Idx,strDeptName);
				}
				rowData.setData(i,nCalcName1_Idx,strName);

			}
		}
		break;

	case ENTITY_BUS_CONTACT: //Org,Last,First , or Org, or Last,First
		{
			QString strName;
			int nSize=rowData.getRowCount();
			for(int nRow=0;nRow<nSize;++nRow)
			{
				strName.clear();
				QString strFirstName=rowData.getDataRef(nRow,m_nFirstNameIdx).toString();
				QString strLastName=rowData.getDataRef(nRow,m_nLastNameIdx).toString();
				QString strOrgName=rowData.getDataRef(nRow,m_nOrgNameIdx).toString();
				if (!strLastName.isEmpty())
					strName=strLastName;
				if (!strFirstName.isEmpty())
					strName+=" "+strFirstName.trimmed();
				if (!strOrgName.isEmpty()){
					if (strName.isEmpty())
						strName=strOrgName;
					else
						strName=strName+", "+strOrgName;
				}
/*
				if(rowData.getDataRef(nRow,m_nOrgNameIdx).toString().isEmpty())
					strName=rowData.getDataRef(nRow,m_nLastNameIdx).toString()+" "+rowData.getDataRef(nRow,m_nFirstNameIdx).toString();
				else
					if (!rowData.getDataRef(nRow,m_nLastNameIdx).toString().isEmpty())
						strName=rowData.getDataRef(nRow,m_nLastNameIdx).toString()+" "+rowData.getDataRef(nRow,m_nFirstNameIdx).toString()+", "+rowData.getDataRef(nRow,m_nOrgNameIdx).toString();
					else
						strName=rowData.getDataRef(nRow,m_nOrgNameIdx).toString();
*/
				rowData.setData(nRow,nCalcName1_Idx,strName);
			}

		}
		break;

	default:
		{
			if (m_nCodeIdx==-1 || m_nNameIdx==-1 || nCalcName1_Idx==-1)
				return;
			QString strName;
			int nSize=rowData.getRowCount();
			for(int nRow=0;nRow<nSize;++nRow)
			{
				QString strName=rowData.getDataRef(nRow,m_nCodeIdx).toString()+"  "+rowData.getDataRef(nRow,m_nNameIdx).toString();
				rowData.setData(nRow,nCalcName1_Idx,strName);
			}
		}
	}



}

//if record is changed, invalidate fields
void MainEntityCalculatedName::InvalidateCalculatedNameFields(DbRecordSet &rowData, int nRow)
{
	if (!m_bCanCalculate)
		return;


	//if rows doesn't match:
	if (nRow>=rowData.getRowCount())
		return;

	QVariant empty(QVariant::String);

	switch(m_nEntityID)
	{
		case ENTITY_BUS_PERSON:
			{
				rowData.setData(nRow,m_nLastNameIdx,empty);
			}
			break;

		case ENTITY_BUS_CONTACT: //Org,Last,First , or Org, or Last,First
			{
				rowData.setData(nRow,m_nLastNameIdx,empty);
				rowData.setData(nRow,m_nOrgNameIdx,empty);
			}
			break;
		case ENTITY_CORE_USER:
			rowData.setData(nRow,m_nLastNameIdx,empty);
			break;

		default:
			if (m_nCodeIdx!=-1)rowData.setData(nRow,m_nCodeIdx,empty);
			if (m_nNameIdx!=-1)rowData.setData(nRow,m_nNameIdx,empty);
	}


}

QString MainEntityCalculatedName::GetCalculatedName(int nTableID,DbRecordSet &rowData, int nCurrentRow)
{
	if (nCurrentRow<0 || nCurrentRow>rowData.getRowCount()-1)
		return "";

	switch(nTableID)
	{
	case BUS_PERSON:
		return GetName_Person(rowData.getDataRef(nCurrentRow,"BPER_LAST_NAME").toString(),rowData.getDataRef(nCurrentRow,"BPER_FIRST_NAME").toString());
		break;
	case BUS_CM_CONTACT:
		return GetName_Contact(rowData.getDataRef(nCurrentRow,"BCNT_LASTNAME").toString(),rowData.getDataRef(nCurrentRow,"BCNT_FIRSTNAME").toString(),rowData.getDataRef(nCurrentRow,"BCNT_ORGANIZATIONNAME").toString());
		break;
	case CORE_USER:
		return GetName_Person(rowData.getDataRef(nCurrentRow,"CUSR_LAST_NAME").toString(),rowData.getDataRef(nCurrentRow,"CUSR_FIRST_NAME").toString());
		break;
	default: //Hierarhical
		{
			QString strPrefix=DbSqlTableDefinition::GetTablePrefix(nTableID);
			return GetName_Hierarhical(rowData.getDataRef(nCurrentRow,strPrefix+"_CODE").toString(),rowData.getDataRef(nCurrentRow,strPrefix+"_NAME").toString());
		}
	}
}


QString MainEntityCalculatedName::GetName_Hierarhical(QString strCode, QString strName)
{
	if (!strName.isEmpty())
		return strCode+" "+strName;
	else
		return strCode;
}

QString MainEntityCalculatedName::GetName_Person(QString strLastName, QString strFirstName)
{
	if (!strFirstName.isEmpty())
		return strLastName+", "+strFirstName;
	else
		return strLastName;
}

QString MainEntityCalculatedName::GetName_Contact(QString strLastName,QString strFirstName,QString strOrgName)
{
	QString strName;
	if (!strFirstName.isEmpty())
	{
		if (strOrgName.isEmpty())
			return strLastName+" "+strFirstName;
		else
			return strLastName+" "+strFirstName+", "+strOrgName;
	}
	else if (!strLastName.isEmpty())
	{
		if (strOrgName.isEmpty())
			return strLastName;
		else
			return strLastName+", "+strOrgName;
	}
	else
	{
		return strOrgName;
	}
}