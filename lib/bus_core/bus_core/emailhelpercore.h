#ifndef EMAILHELPERCORE_H
#define EMAILHELPERCORE_H

#include "common/common/dbrecordset.h"

class EmailHelperCore
{
public:
	static QString	GetEmailFromName(QString strName);
	static QString	GetNameFromEmail(QString strEmail);
	static QString	GetFullEmailAddress(QString strName, QString strEmail, bool bAddSeparator=false);
	static void		LoadRecipients(DbRecordSet &lstContactEmails,DbRecordSet &recRecipients, QString strRecipients);
	static bool		ExtractNamesFromEmail(QString strEmail,QString &strFirstName,QString &strLastName);
	static void		ExtractMailsFromEmailRow(DbRecordSet &rowEmail,DbRecordSet &lstEmails);

private:
	
};

#endif // EMAILHELPERCORE_H
