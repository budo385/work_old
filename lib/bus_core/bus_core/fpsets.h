#ifndef FPSETS_H
#define FPSETS_H

#include <QString>

//
// Define a single item, no matter if it is a FP, FPS, or Module definition
// (hierarchy data is also stored within the structure)
//

//item type values
#define ITEM_TYPE_FP	1	// item is a Function Point
#define ITEM_TYPE_FPS	2	// item is a Function Point Set
#define ITEM_TYPE_MOD	3	// item is a Module

//item status values (access rights)
#define ITEM_STATUS_UNDEFINED	0	// not specified in this context
#define ITEM_STATUS_OFF			1	// user can not use this FP/FPS
#define ITEM_STATUS_ON			2	// user can use this FP/FPS
#define ITEM_STATUS_INHERITED	3	// access right for FP in FPS is inherited from FP 

typedef struct FP_ITEM {
	int			nLevel;		// defines hierarchy
	int			nType;		// one of the item type values (see above)
	int			nCode;		// unique ID number (used for FP and FPS)
	QString		strName;	// item name
	QString		strCode;	// code string (used only for Module item)
	QString		strDesc;	// description
	int			nStatus;	// one of the item status values (see above)
	int			nValue;		// FP specific value
	bool		bActive;	// display item within GUI list

	FP_ITEM(){};
	FP_ITEM(const FP_ITEM &other){ operator =(other); };
	bool operator == (const FP_ITEM &other);	//required for search operation
	void operator = (const FP_ITEM &other);	//required for copy operation
} FP_ITEM;

#endif // FPSETS_H
