#include "optionsandsettingsmanager.h"
#include "optionsandsettingsorganizer.h"
#include "db_core/db_core/dbsqltableview.h"
#include <QWriteLocker>
#include <QReadLocker>

OptionsAndSettingsManager::OptionsAndSettingsManager()
{
	m_recSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
	m_recOptions.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
}

OptionsAndSettingsManager::~OptionsAndSettingsManager()
{

}

void OptionsAndSettingsManager::SetSettingsDataSource(const DbRecordSet *recSettings, int nPersonID)
{
	QWriteLocker lock(&m_lckSettings);

	if (!recSettings)
		m_recSettings.clear();
	else
		m_recSettings	= *recSettings;
	m_nPersonID		= nPersonID;
	m_lstChangedSettingsID.clear();
	TestSettings();
	
}

void OptionsAndSettingsManager::SetOptionsDataSource(const DbRecordSet *recOptions)
{
	QWriteLocker lock(&m_lckOptions);

	if (!recOptions)
		m_recOptions.clear();
	else
		m_recOptions	= *recOptions;
	TestOptions();
}

void OptionsAndSettingsManager::GetDataSource(DbRecordSet &recOptions, DbRecordSet &recSettings)
{
	recSettings=m_recSettings;
	recOptions=m_recOptions;
}

QVariant OptionsAndSettingsManager::GetPersonSetting(int nSettingID)
{
	QReadLocker lock(&m_lckSettings);
	QVariant varSettingValue;
	int nRow = GetPersonSettingRowIdx(nSettingID);
	if (nRow<0) 
	{
		Q_ASSERT(false);		//settings not found
		return varSettingValue; //B.T. test if exists
	}

	//Get setting type.
	int nSettingType = m_recSettings.getDataRef(nRow, "BOUS_SETTING_TYPE").toInt();

	//Select value.
	//If bool or int.
	if (nSettingType == 0 || nSettingType == 1)
		varSettingValue = m_recSettings.getDataRef(nRow, "BOUS_VALUE");
	else if (nSettingType == 2)
		varSettingValue = m_recSettings.getDataRef(nRow, "BOUS_VALUE_STRING");
	else if (nSettingType == 3)
		varSettingValue = m_recSettings.getDataRef(nRow, "BOUS_VALUE_BYTE");
	else
		Q_ASSERT(false);

	return varSettingValue;
}

DbRecordSet OptionsAndSettingsManager::GetPersonSettingRow(int nSettingID)
{
	QReadLocker lock(&m_lckSettings);
	DbRecordSet empty;
	int nRow = GetPersonSettingRowIdx(nSettingID);
	if (nRow<0) 
	{
		Q_ASSERT(false);		//settings not found
		return empty; //B.T. test if exists
	}
	return m_recSettings.getRow(nRow);
}

DbRecordSet	OptionsAndSettingsManager::GetPersonSettingSet(int nSettingSetID)
{
	QWriteLocker lock(&m_lckSettings);

	//Q_ASSERT(!m_recSettings.getColumnCount());
	
	//Select records that have desired setting set.
	m_recSettings.clearSelection();
	m_recSettings.find("BOUS_SETTING_SET_ID", nSettingSetID);

	//If there is no that setting insert it.
	if (!m_recSettings.getSelectedCount())
	{
		OptionsAndSettingsOrganizer org;
		DbRecordSet	recSetting = org.GetSettingsBySetID(nSettingSetID);
		int nRowCount = recSetting.getRowCount();
		for (int i = 0; i < nRowCount; ++i)
			recSetting.setData(i, "BOUS_PERSON_ID", m_nPersonID);
		m_recSettings.merge(recSetting);

		//Select records that have desired setting set.
		m_recSettings.clearSelection();
		m_recSettings.find("BOUS_SETTING_SET_ID", nSettingSetID, false);
	}

	return m_recSettings.getSelectedRecordSet();
}

QVariant OptionsAndSettingsManager::GetApplicationOption(int nOptionID)
{
	QReadLocker lock(&m_lckOptions);
	QVariant varOptionValue;
	int nRow = GetApplicationOptionRowIdx(nOptionID);
	if (nRow<0) 
	{
		Q_ASSERT(false);		//settings not found
		return varOptionValue; //B.T. test if exists
	}


	//Get setting type.
	int nSettingType = m_recOptions.getDataRef(nRow, "BOUS_SETTING_TYPE").toInt();

	//Select value.
	//If bool or int.
	if (nSettingType == 0 || nSettingType == 1)
		varOptionValue = m_recOptions.getDataRef(nRow, "BOUS_VALUE");
	else if (nSettingType == 2)
		varOptionValue = m_recOptions.getDataRef(nRow, "BOUS_VALUE_STRING");
	else if (nSettingType == 3)
		varOptionValue = m_recOptions.getDataRef(nRow, "BOUS_VALUE_BYTE");
	else
		Q_ASSERT(false);

	return varOptionValue;
}

DbRecordSet	OptionsAndSettingsManager::GetApplicationOptionRow(int nOptionID)
{
	QReadLocker lock(&m_lckOptions);
	DbRecordSet empty;
	int nRow = GetApplicationOptionRowIdx(nOptionID);
	if (nRow<0) 
	{
		Q_ASSERT(false);		//settings not found
		return empty; //B.T. test if exists
	}
	return m_recOptions.getRow(nRow);


}

DbRecordSet	OptionsAndSettingsManager::GetApplicationOptionSet(int nSettingSetID)
{
	QWriteLocker lock(&m_lckOptions);

	m_recOptions.clearSelection();
	m_recOptions.find("BOUS_SETTING_SET_ID", nSettingSetID, false);

	return m_recOptions.getSelectedRecordSet();
}

void OptionsAndSettingsManager::TestOptions()
{
	//Load default options and settings.
	OptionsAndSettingsOrganizer OptSettOrganizer;
	DbRecordSet tmpOptions	= OptSettOrganizer.GetOptions();

	//Loop through options and check is some of them missing in the ones loaded from server.
	int nRowCount = tmpOptions.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nOptionID = tmpOptions.getDataRef(i, "BOUS_SETTING_ID").toInt();
		//This goes through cache and add option if it is not found.
		int nRow = GetApplicationOptionRowIdx(nOptionID);
		if (nRow<0)  //settings does not exits: add:
		{
			Q_ASSERT(false); //new application options added and database is not reorganized!!!!
			DbRecordSet rowNew= tmpOptions.getRow(i);
			m_recOptions.merge(rowNew);
		}
	}
}


void OptionsAndSettingsManager::TestSettings()
{
	//Load default options and settings.
	OptionsAndSettingsOrganizer OptSettOrganizer;
	DbRecordSet tmpSettings = OptSettOrganizer.GetPersonalSettings();

	//Now through setting and check is some of them missing in the ones loaded from server.
	int nRowCount = tmpSettings.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nSettingID = tmpSettings.getDataRef(i, "BOUS_SETTING_ID").toInt();
		//This goes through cache and add settin if it is not found.
		int nRow = GetPersonSettingRowIdx(nSettingID);
		if (nRow<0)  //settings does not exits: add:
		{
			DbRecordSet rowNew= tmpSettings.getRow(i);
			rowNew.setData(0, "BOUS_PERSON_ID", m_nPersonID);
			m_recSettings.merge(rowNew);
			m_lstChangedSettingsID << nSettingID;
		}
	}
}



void OptionsAndSettingsManager::SetPersonSetting(int nSettingID, QVariant varValue)
{
	QWriteLocker lock(&m_lckSettings);

	//Get setting and or if not exits create it.
	int nRow = GetPersonSettingRowIdx(nSettingID);
	if (nRow<0 || nRow>m_recSettings.getRowCount()-1) //check if all ok->should be ok, but check
	{
		Q_ASSERT(false);
		return;
	}

	//Remember changed settings so that it can be saved on logout.
	if (!m_lstChangedSettingsID.contains(nSettingID))
		m_lstChangedSettingsID << nSettingID;


	//Get setting type.
	int nSettingType = m_recSettings.getDataRef(nRow, "BOUS_SETTING_TYPE").toInt();

	//If bool or int.
	if (nSettingType == 0 || nSettingType == 1)
	{
		int val = varValue.toInt();
		m_recSettings.setData(nRow, "BOUS_VALUE", val);
	}
	else if (nSettingType == 2)
	{
		QString str = varValue.toString();
		m_recSettings.setData(nRow, "BOUS_VALUE_STRING", str);
	}
	else if (nSettingType == 3)
	{
		QByteArray byt = varValue.toByteArray();
		m_recSettings.setData(nRow, "BOUS_VALUE_BYTE", byt);
	}
	else
		Q_ASSERT(false);

}



void OptionsAndSettingsManager::SetApplicationOption(int nSettingID, QVariant varValue)
{

	QWriteLocker lock(&m_lckOptions);

	//Get setting and or if not exits create it.
	int nRow = GetApplicationOptionRowIdx(nSettingID);
	if (nRow<0 || nRow>m_recOptions.getRowCount()-1) //check if all ok->should be ok, but check
	{
		Q_ASSERT(false);
		return;
	}

	//Get setting type.
	int nSettingType = m_recOptions.getDataRef(nRow, "BOUS_SETTING_TYPE").toInt();

	//If bool or int.
	if (nSettingType == 0 || nSettingType == 1)
	{
		int val = varValue.toInt();
		m_recOptions.setData(nRow, "BOUS_VALUE", val);
	}
	else if (nSettingType == 2)
	{
		QString str = varValue.toString();
		m_recOptions.setData(nRow, "BOUS_VALUE_STRING", str);
	}
	else if (nSettingType == 3)
	{
		QByteArray byt = varValue.toByteArray();
		m_recOptions.setData(nRow, "BOUS_VALUE_BYTE", byt);
	}
	else
		Q_ASSERT(false);

}

int OptionsAndSettingsManager::GetPersonSettingRowIdx(int nSettingID)
{
	return m_recSettings.find("BOUS_SETTING_ID", nSettingID,true); //this find does not change internal state of recordset -> thread safe
}
int OptionsAndSettingsManager::GetApplicationOptionRowIdx(int nOptionID)
{
	return m_recOptions.find("BOUS_SETTING_ID", nOptionID,true); //this find does not change internal state of recordset -> thread safe
}