#include "calendarhelpercore.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "bus_core/bus_core/globalconstants.h"

void CalendarHelperCore::CreateInviteRecordsFromCalendarData(DbRecordSet &lstDataEvent,DbRecordSet &lstParts,DbRecordSet &lstPersons,DbRecordSet lstInviteBySC, DbRecordSet &lstInviteByEmail,DbRecordSet &lstInviteBySMS)
{
	lstInviteBySC.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_SELECT));
	lstInviteByEmail.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS));
	lstInviteBySMS.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS));

	if (lstDataEvent.getRowCount()==0)
		return;

	//get all nmrx person from all parts:
	int nSize=lstParts.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstNMRXPerson=lstParts.getDataRef(i,"NMRX_PERSON_LIST").value<DbRecordSet>();
		if (lstNMRXPerson.getColumnCount()==0)
			lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

		DbRecordSet lstInvite;
		lstInvite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_SELECT));

		ExtractInviteRecordsFromNMRXAssignment(lstNMRXPerson,lstInvite, lstPersons);

		lstInvite.setColValue("BCEP_ID",lstParts.getDataRef(i,"BCEP_ID").toInt());
		lstInvite.setColValue("BCEP_COMM_ENTITY_ID",lstParts.getDataRef(i,"BCEP_COMM_ENTITY_ID").toInt());
		lstInvite.setColValue("CENT_OWNER_ID",lstParts.getDataRef(i,"CENT_OWNER_ID"));

		int nSize2=lstInvite.getRowCount();
		for(int k=0;k<nSize2;k++)
		{
			DbRecordSet rowInvite=lstInvite.getRow(k);
			if (rowInvite.getDataRef(0,"BCIV_BY_NOTIFICATION").toInt()>0)
			{
				lstInviteBySC.merge(rowInvite);
			}
			if (rowInvite.getDataRef(0,"BCIV_BY_EMAIL").toInt()>0)
			{
				DbRecordSet row = lstParts.getRow(i);
				CopyDataFromCalendarDataToInviteRecord(lstDataEvent,row,lstInviteByEmail, rowInvite);
			}
			if (rowInvite.getDataRef(0,"BCIV_BY_SMS").toInt()>0)
			{
				DbRecordSet row = lstParts.getRow(i);
				CopyDataFromCalendarDataToInviteRecord(lstDataEvent,row,lstInviteBySMS, rowInvite);
			}
		}
	}
}

void CalendarHelperCore::CopyDataFromCalendarDataToInviteRecord(DbRecordSet &rowEvent,DbRecordSet &rowPart, DbRecordSet &Ret_lstData, DbRecordSet &lstInvitePartRecords)
{
	Ret_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS));

	if (lstInvitePartRecords.getRowCount()>0)
		Ret_lstData.merge(lstInvitePartRecords);
	else
		Ret_lstData.addRow();

	Ret_lstData.setColValue("BCEV_CATEGORY",rowEvent.getDataRef(0,"BCEV_CATEGORY"));
	Ret_lstData.setColValue("BCEV_ID",rowEvent.getDataRef(0,"BCEV_ID"));
	Ret_lstData.setColValue("BCEV_TITLE",rowEvent.getDataRef(0,"BCEV_TITLE"));
	//PART DATA:
	Ret_lstData.setColValue("CENT_CE_TYPE_ID",rowPart.getDataRef(0,"CENT_CE_TYPE_ID"));
	Ret_lstData.setColValue("CENT_OWNER_ID",rowPart.getDataRef(0,"CENT_OWNER_ID"));
	Ret_lstData.setColValue("BCEP_STATUS",rowPart.getDataRef(0,"BCEP_STATUS"));
	Ret_lstData.setColValue("BCEP_ID",rowPart.getDataRef(0,"BCEP_ID"));
	Ret_lstData.setColValue("BCEP_COMM_ENTITY_ID",rowPart.getDataRef(0,"BCEP_COMM_ENTITY_ID"));

	//OPTION:
	DbRecordSet lstOptions=rowPart.getDataRef(0,"OPTION_LIST").value<DbRecordSet>();
	if (lstOptions.getRowCount()>0)
	{
		//MB: what option to use!??? hehe, hes crazy like dynamite: use first one:
		Ret_lstData.setColValue("BCOL_ID",lstOptions.getDataRef(0,"BCOL_ID"));
		Ret_lstData.setColValue("BCOL_FROM",lstOptions.getDataRef(0,"BCOL_FROM"));
		Ret_lstData.setColValue("BCOL_TO",lstOptions.getDataRef(0,"BCOL_TO"));
		Ret_lstData.setColValue("BCOL_SUBJECT",lstOptions.getDataRef(0,"BCOL_SUBJECT"));
		Ret_lstData.setColValue("BCOL_LOCATION",lstOptions.getDataRef(0,"BCOL_LOCATION"));
		Ret_lstData.setColValue("BCOL_DESCRIPTION",lstOptions.getDataRef(0,"BCOL_DESCRIPTION"));
	}

	DbRecordSet lstNMRXPerson=rowPart.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
	if (lstNMRXPerson.getColumnCount()==0)
		lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	Ret_lstData.setColValue("CONTACT_ASSIGN_LIST",lstNMRXPerson);

}



//From NMRX table, extract INVITE record, test if invite must be sent, then create it
void CalendarHelperCore::ExtractInviteRecordsFromNMRXAssignment(DbRecordSet &lstNMRXPerson,DbRecordSet &RetNmrxDataForInvite, DbRecordSet &lstPersons)
{
	RetNmrxDataForInvite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_SELECT));
	if (lstNMRXPerson.getColumnIdx("X_RECORD")==-1)
		return;

	int nSize=lstNMRXPerson.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (lstNMRXPerson.getDataRef(i,"X_RECORD").isNull())
			continue;
		DbRecordSet rowInvite=lstNMRXPerson.getDataRef(i,"X_RECORD").value<DbRecordSet>();
		if (rowInvite.getRowCount()>0)
		{
			if (rowInvite.getDataRef(0,"BCIV_INVITE").toInt()>0 && rowInvite.getDataRef(0,"BCIV_IS_SENT").toInt()==0)
			{
				RetNmrxDataForInvite.merge(rowInvite);
				if (lstNMRXPerson.getDataRef(i,"BNMR_TABLE_2")==BUS_PERSON) //person+contact
				{
					int nPersonID=lstNMRXPerson.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt();
					RetNmrxDataForInvite.setData(RetNmrxDataForInvite.getRowCount()-1,"BPER_ID",nPersonID);
					int nRow=lstPersons.find("BPER_ID",nPersonID,true);
					if (nRow>=0)
					{
						RetNmrxDataForInvite.setData(RetNmrxDataForInvite.getRowCount()-1,"RECIPIENT_CONTACT_ID",lstPersons.getDataRef(nRow,"BPER_CONTACT_ID"));
					}
				}
				else//contact+person
				{
					int nContactID = lstNMRXPerson.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt();
					RetNmrxDataForInvite.setData(RetNmrxDataForInvite.getRowCount()-1,"RECIPIENT_CONTACT_ID",nContactID);
					int nRow=lstPersons.find("BPER_CONTACT_ID",nContactID,true);
					if (nRow>=0)
					{
						RetNmrxDataForInvite.setData(RetNmrxDataForInvite.getRowCount()-1,"BPER_ID",lstPersons.getDataRef(nRow,"BPER_ID"));
					}
				}
			}
		}
	}
}

//from rowInvite creates event data + parts+options+ nmrx assignments..
void CalendarHelperCore::CreateCalendarDataFromInviteData(DbRecordSet &rowInvite, DbRecordSet &lstDataEvent,DbRecordSet &lstParts,DbRecordSet &lstPersons, int nLoggedPersonID)
{
	lstDataEvent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
	lstParts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));
	DbRecordSet lstOptions;
	lstOptions.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_OPTIONS_SELECT));

	if (rowInvite.getRowCount()<1)
		return;

	lstDataEvent.addRow();
	//lstParts.addRow();
	lstOptions.addRow();

	lstDataEvent.setData(0,"BCEV_DAT_CREATED",QDateTime::currentDateTime());
	lstDataEvent.setData(0,"BCEV_IS_PRIVATE",0);
	lstDataEvent.setData(0,"BCEV_IS_INFORMATION",0);
	lstDataEvent.setData(0,"BCEV_IS_PRELIMINARY",0);
	
	AddEmptyPartRow(lstDataEvent,lstParts);

	//find owner by contact_id_organizer, else set to the creator of this this event..:
	int nOwnerContactID=rowInvite.getDataRef(0,"ORGANIZER_CONTACT_ID").toInt();
	int nRow=lstPersons.find("BPER_CONTACT_ID",nOwnerContactID);
	if (nRow>=0)
		lstParts.setData(0,"CENT_OWNER_ID",lstPersons.getDataRef(nRow,"BPER_ID").toInt());
	else
	{
		lstParts.setData(0,"CENT_OWNER_ID",nLoggedPersonID);
		nRow=lstPersons.find("BPER_ID",nLoggedPersonID);
		if (nRow>=0)
			nOwnerContactID=lstPersons.getDataRef(nRow,"BPER_CONTACT_ID").toInt();
	}

	//MAIN DATA:
	lstDataEvent.setData(0,"BCEV_CATEGORY",rowInvite.getDataRef(0,"BCEV_CATEGORY"));
	lstDataEvent.setData(0,"BCEV_TITLE",rowInvite.getDataRef(0,"BCEV_TITLE"));
	lstDataEvent.setData(0,"BCEV_FROM",rowInvite.getDataRef(0,"BCOL_FROM"));
	lstDataEvent.setData(0,"BCEV_TO",rowInvite.getDataRef(0,"BCOL_TO"));
	lstDataEvent.setData(0,"BCEV_DESCRIPTION",rowInvite.getDataRef(0,"BCOL_DESCRIPTION"));

	lstParts.setData(0,"BCEP_DESCRIPTION",rowInvite.getDataRef(0,"BCOL_DESCRIPTION"));
	lstParts.setData(0,"BCEP_STATUS",GlobalConstants::TYPE_CAL_PRELIMINARY);

	lstOptions.setData(0,"BCOL_FROM",rowInvite.getDataRef(0,"BCOL_FROM"));
	lstOptions.setData(0,"BCOL_TO",rowInvite.getDataRef(0,"BCOL_TO"));
	lstOptions.setData(0,"BCOL_SUBJECT",rowInvite.getDataRef(0,"BCOL_SUBJECT"));
	lstOptions.setData(0,"BCOL_LOCATION",rowInvite.getDataRef(0,"BCOL_LOCATION"));
	lstOptions.setData(0,"BCOL_DESCRIPTION",rowInvite.getDataRef(0,"BCOL_DESCRIPTION"));

	lstParts.setData(0,"OPTION_LIST",lstOptions);
}

void CalendarHelperCore::AddEmptyPartRow(DbRecordSet &rowEvent,DbRecordSet &lstParts)
{
	DbRecordSet rowPart;
	rowPart.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));
	rowPart.addRow();
	rowPart.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);

	DbRecordSet rowOption=CreateOptionForPart(rowEvent,rowPart); //1 part row = 1 option row always!!
	rowPart.setData(0,"OPTION_LIST",rowOption);

	DbRecordSet lstNMRXPerson;
	lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	rowPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);

	DbRecordSet lstNMRXProject;
	lstNMRXProject.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	rowPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);

	DbRecordSet lstNMRXResources;
	lstNMRXResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	rowPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResources);

	lstParts.merge(rowPart);
}


//create from defaults or copy all options from part: part must have 1 row at least
DbRecordSet CalendarHelperCore::CreateOptionForPart(DbRecordSet &rowEvent,DbRecordSet &rowPart)
{
	//TODO:
	DbRecordSet rowOption;
	rowOption.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_OPTIONS_SELECT));
	rowOption.addRow();

	if (rowEvent.getRowCount()>0 && rowPart.getRowCount()>0)
	{
		DbRecordSet lstOptions=rowPart.getDataRef(0,"OPTION_LIST").value<DbRecordSet>();
		if (lstOptions.getRowCount()>0)
		{
			CalendarHelperCore::CleanOptionListAfterCopy(lstOptions);
			rowOption=lstOptions;
		}
		else
		{
			QDateTime from=rowEvent.getDataRef(0,"BCEV_FROM").toDateTime();
			QDateTime to=rowEvent.getDataRef(0,"BCEV_TO").toDateTime();
			QString strTitle=rowEvent.getDataRef(0,"BCEV_TITLE").toString();
			QString strLocation=rowPart.getDataRef(0,"BCEP_DESCRIPTION").toString();

			rowOption.setData(0,"BCOL_FROM",from);
			rowOption.setData(0,"BCOL_TO",to);
			rowOption.setData(0,"BCOL_SUBJECT",strTitle);
			rowOption.setData(0,"BCOL_DESCRIPTION",strLocation);
		}
	}

	rowOption.setData(0,"BCOL_IS_ACTIVE",1);
	return rowOption;
}


void CalendarHelperCore::CleanOptionListAfterCopy(DbRecordSet &lstOptions)
{
	QVariant intNull(QVariant::Int);
	if (lstOptions.getRowCount()>0)
	{
		lstOptions.setColValue("BCOL_ID",intNull);
		int nSize1=lstOptions.getRowCount();
		for(int k=0;k<nSize1;k++)
		{
			DbRecordSet lstBreak=lstOptions.getDataRef(k,"BREAK_LIST").value<DbRecordSet>();
			if (lstBreak.getColumnIdx("BCBL_ID")>=0)
				lstBreak.setColValue("BCBL_ID",intNull);
			else
				lstBreak.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_BREAKS));
			lstOptions.setData(k,"BREAK_LIST",lstBreak);
		}
	}
	else
	{
		lstOptions.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));
	}
}

void CalendarHelperCore::CreateCalendarViewRecordset(DbRecordSet &recCalView, QString strViewName, int nOpenOnStartup, int nIsPublic, int nCalType, 
													 int nLoggedPersonID, int nSections/*=0*/, int nDateRange/*=2*/, QString strSortCode/*=""*/, int nViewTypeID/*=0*/, int nFilterByNoType/*=0*/, 
													 int nFilterByType/*=0*/, int nShowPreliminary/*=1*/, int nShowInformation/*=1*/, int nTimeScale/*=2*/,
													 QDateTime datScrollToTime/*=QDateTime(QDate::currentDate(), QTime(9,0))*/, QByteArray bytMultidayHeight/*=QByteArray()*/,
													 QByteArray bytFuiGeometry/*=QByteArray()*/, int nFuiPosX/*=0*/, int nFuiPosY/*=0*/, int nShowTasks/*=0*/, int nUpperColumnWidth/*=200*/)
{
	if (recCalView.getColumnCount()==0)
	{
		recCalView.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW));
	}
	if (recCalView.getRowCount()==0)
	{
		recCalView.addRow();
	}

	recCalView.setData(0, "BCALV_NAME", strViewName);
	recCalView.setData(0, "BCALV_OPEN_ON_STARTUP", nOpenOnStartup);			//No use for now.
	recCalView.setData(0, "BCALV_IS_PUBLIC", nIsPublic);					//Is public.
	recCalView.setData(0, "BCALV_TYPE", nCalType);							//Default type - for now.
	recCalView.setData(0, "BCALV_OWNER_ID", nLoggedPersonID);				//Logged user.
	recCalView.setData(0, "BCALV_SORT_CODE", strSortCode);					//Sort code.
	//recCalView.setData(0, "BCALV_VALUE_BYTE", NULL);						//Deprecated.
	if (nViewTypeID>0)
	{
		recCalView.setData(0, "BCALV_VIEW_TYPE_ID",		nViewTypeID);			//View tipe ID.
	}
	recCalView.setData(0, "BCALV_FILTER_BY_NOTYPE", nFilterByNoType);		//GUI checkbox.
	recCalView.setData(0, "BCALV_FILTER_BY_TYPE",	nFilterByType);			//GUI checkbox.
	recCalView.setData(0, "BCALV_SHOW_PRELIMINARY", nShowPreliminary);		//GUI checkbox.
	recCalView.setData(0, "BCALV_SHOW_INFORMATION", nShowInformation);		//GUI checkbox.
	recCalView.setData(0, "BCALV_TIME_SCALE",		nTimeScale);			//GUI timescale.
	recCalView.setData(0, "BCALV_DATE_RANGE",		nDateRange);			//GUI daterange.
	recCalView.setData(0, "BCALV_SCROLL_TO_TIME",	datScrollToTime);		//GUI scroll to time.
	recCalView.setData(0, "BCALV_MULTIDAY_HEIGHT",	bytMultidayHeight);		//GUI multiday calendar heigth.
	//recCalView.setData(0, "BCALV_COLUMN_WIDTH",		null);				//deprecated.
	recCalView.setData(0, "BCALV_FUI_GEOMETRY",		bytFuiGeometry);		//GUI spliter geometry.
	recCalView.setData(0, "BCALV_FUI_POSITION_X",	nFuiPosX);				//GUI pos X.
	recCalView.setData(0, "BCALV_FUI_POSITION_Y",	nFuiPosY);				//GUI pos Y.
	recCalView.setData(0, "BCALV_SHOW_TASKS",		nShowTasks);			//GUI is show tasks visible.
	recCalView.setData(0, "BCALV_SECTIONS",			nSections);				//GUI calendar sections visible, 0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER..
	recCalView.setData(0, "BCALV_OVERVIEWLEFTCOLWIDTH",	nUpperColumnWidth);	//Upper column width.
}

void CalendarHelperCore::AddEntityRowToEntityRecordset(DbRecordSet &recEntities, int nRow, int nENTITY_TYPE, int nENTITY_ID, int nENTITY_PERSON_ID, bool bIsSelected, int nSortCode)
{
	recEntities.setData(nRow, "BCVE_SORT_CODE", nSortCode);

	if (bIsSelected)
	{
		recEntities.setData(nRow, "BCVE_IS_SELECTED", 1);
	}
	else
	{
		recEntities.setData(nRow, "BCVE_IS_SELECTED", 0);
	}

	if(nENTITY_TYPE==GlobalConstants::TYPE_CAL_USER_SELECT)
	{
		//User has stored both user and contact data.
		recEntities.setData(nRow, "BCVE_CONTACT_ID", nENTITY_ID);
		recEntities.setData(nRow, "BCVE_USER_ID", nENTITY_PERSON_ID);
	}
	else if (nENTITY_TYPE==GlobalConstants::TYPE_CAL_PROJECT_SELECT)
	{
		recEntities.setData(nRow, "BCVE_PROJECT_ID", nENTITY_ID);
	}
	else if (nENTITY_TYPE==GlobalConstants::TYPE_CAL_CONTACT_SELECT)
	{
		recEntities.setData(nRow, "BCVE_CONTACT_ID", nENTITY_ID);
	}
	else if (nENTITY_TYPE==GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
	{
		recEntities.setData(nRow, "BCVE_RESOURCE_ID", nENTITY_ID);
	}
}

bool CalendarHelperCore::CheckEntityExistsInEntityRecordset(DbRecordSet &recEntities, int nENTITY_TYPE, int nENTITY_ID, int nENTITY_PERSON_ID)
{
	int nSelectedRowCount=-1;
	if(nENTITY_TYPE==GlobalConstants::TYPE_CAL_USER_SELECT)
	{
		nSelectedRowCount=recEntities.find("BCVE_USER_ID", nENTITY_PERSON_ID, false, false, false);
	}
	else if (nENTITY_TYPE==GlobalConstants::TYPE_CAL_PROJECT_SELECT)
	{
		nSelectedRowCount=recEntities.find("BCVE_PROJECT_ID", nENTITY_ID, false, false, false);
	}
	else if (nENTITY_TYPE==GlobalConstants::TYPE_CAL_CONTACT_SELECT)
	{
		nSelectedRowCount=recEntities.find("BCVE_CONTACT_ID", nENTITY_ID, false, false, false);
	}
	else if (nENTITY_TYPE==GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
	{
		nSelectedRowCount=recEntities.find("BCVE_RESOURCE_ID", nENTITY_ID, false, false, false);
	}

	if (nSelectedRowCount>0)
	{
		return true;
	}

	return false;
}

void CalendarHelperCore::RemoveEntityRowFromEntityRecordset(DbRecordSet &recEntities, int nENTITY_TYPE, int nENTITY_ID, int nENTITY_PERSON_ID)
{
	//Check does it exists, delete and reorder.
	if (CheckEntityExistsInEntityRecordset(recEntities, nENTITY_TYPE, nENTITY_ID, nENTITY_PERSON_ID))
	{
		recEntities.deleteSelectedRows();
		recEntities.sort("BCVE_SORT_CODE");
		int nRowCount=recEntities.getRowCount();
		for (int i=0; i<nRowCount; i++)
		{
			recEntities.setData(i, "BCVE_SORT_CODE", i);
		}
	}
}

void CalendarHelperCore::ReorderEntityRecordsetSortCode(DbRecordSet &recEntities)
{
	int nRowCount=recEntities.getRowCount();
	for (int i=0; i<nRowCount; i++)
	{
		if (i==0)
		{
			recEntities.setData(i, "BCVE_IS_SELECTED", 1);
		}
		else
		{
			recEntities.setData(i, "BCVE_IS_SELECTED", 0);
		}

		recEntities.setData(i, "BCVE_SORT_CODE", i);
	}
}

void CalendarHelperCore::ConvertSelectEntityRecordsetToInsertEntityRecordset(DbRecordSet &recEntities)
{
	//Set correct recordset for entities.
	DbRecordSet recCalendarEntitiesCoreTable;
	recCalendarEntitiesCoreTable.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW_ENTITIES));
	recCalendarEntitiesCoreTable.merge(recEntities);
	recEntities=recCalendarEntitiesCoreTable;
}
