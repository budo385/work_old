#ifndef CALENDARHELPERCORE_H
#define CALENDARHELPERCORE_H

#include "common/common/dbrecordset.h"
class CalendarHelperCore
{
public:
	static void			CreateInviteRecordsFromCalendarData(DbRecordSet &lstDataEvent,DbRecordSet &lstParts,DbRecordSet &lstPersons,DbRecordSet lstInviteBySC, DbRecordSet &lstInviteByEmail,DbRecordSet &lstInviteBySMS);
	static void			CopyDataFromCalendarDataToInviteRecord(DbRecordSet &rowEvent,DbRecordSet &rowPart, DbRecordSet &Ret_lstData, DbRecordSet &lstInvitePartRecords);
	static void			ExtractInviteRecordsFromNMRXAssignment(DbRecordSet &lstNMRXPerson,DbRecordSet &RetNmrxDataForInvite, DbRecordSet &lstPersons);
	static void			CreateCalendarDataFromInviteData(DbRecordSet &rowInvite, DbRecordSet &lstDataEvent,DbRecordSet &lstParts,DbRecordSet &lstPersons, int nLoggedPersonID);
	static void			AddEmptyPartRow(DbRecordSet &rowEvent,DbRecordSet &lstParts);
	static DbRecordSet	CreateOptionForPart(DbRecordSet &rowEvent,DbRecordSet &rowPart);
	static void			CleanOptionListAfterCopy(DbRecordSet &lstOptions);
	static void			CreateCalendarViewRecordset(DbRecordSet &recCalView, QString strViewName, int nOpenOnStartup, int nIsPublic, int nCalType, 
													int nLoggedPersonID, int nSections=0, int nDateRange=2, QString strSortCode="", int nViewTypeID=0, int nFilterByNoType=0, 
													int nFilterByType=0, int nShowPreliminary=1, int nShowInformation=1, int nTimeScale=2, 
													QDateTime datScrollToTime=QDateTime(QDate::currentDate(), QTime(9,0)), QByteArray bytMultidayHeight=QByteArray(),
													QByteArray bytFuiGeometry=QByteArray(), int nFuiPosX=0, int nFuiPosY=0, int nShowTasks=0, int nUpperColumnWidth=200);
	static void			AddEntityRowToEntityRecordset(DbRecordSet &recEntities, int nRow, int nENTITY_TYPE, int nENTITY_ID, int nENTITY_PERSON_ID, bool bIsSelected, int nSortCode);
	static bool			CheckEntityExistsInEntityRecordset(DbRecordSet &recEntities, int nENTITY_TYPE, int nENTITY_ID, int nENTITY_PERSON_ID);
	static void			RemoveEntityRowFromEntityRecordset(DbRecordSet &recEntities, int nENTITY_TYPE, int nENTITY_ID, int nENTITY_PERSON_ID);
	static void			ReorderEntityRecordsetSortCode(DbRecordSet &recEntities);
	static void			ConvertSelectEntityRecordsetToInsertEntityRecordset(DbRecordSet &recEntities);
};

#endif // CALENDARHELPERCORE_H
