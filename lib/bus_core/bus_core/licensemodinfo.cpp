#include "licensemodinfo.h"
#include <qDebug>

LicenseModInfo::LicenseModInfo()
{
	m_nCustomSolutionID			= 0;
	m_nNumLicenses				= 0;
	m_nLicensePlatformsSubcode	= 0;
	m_nModTreeSibling			= -1;
}

LicenseModInfo::LicenseModInfo(const LicenseModInfo &other)
{
	operator = (other);
}

void LicenseModInfo::operator =(const LicenseModInfo &other)
{
	if(this != &other)
	{
		m_strModuleName				= other.m_strModuleName;
		m_strCode					= other.m_strCode;
		m_strSubCode				= other.m_strSubCode;
		m_strName					= other.m_strName;
		m_strReportLine1			= other.m_strReportLine1;
		m_strReportLine2			= other.m_strReportLine2;
		m_nCustomSolutionID			= other.m_nCustomSolutionID;
		m_strMLIID					= other.m_strMLIID;
		m_nLicensingType			= other.m_nLicensingType;
		m_nNumLicenses				= other.m_nNumLicenses;
		m_nLicensePlatforms			= other.m_nLicensePlatforms;
		m_nLicensePlatformsSubcode	= other.m_nLicensePlatformsSubcode;
		m_dateCreated				= other.m_dateCreated;
		m_dateModified				= other.m_dateModified;
		m_dateExpiration			= other.m_dateExpiration;
		m_dateWarning 			    = other.m_dateWarning;
		m_bUseExpireDate			= other.m_bUseExpireDate;
		m_strModifiedBy				= other.m_strModifiedBy;
		m_strSerialNumber			= other.m_strSerialNumber;
		
		m_treeAR					= other.m_treeAR;
		m_nModTreeSibling			= other.m_nModTreeSibling;
		m_lstAR						= other.m_lstAR;
	}
}

void LicenseModInfo::AR_RefreshFlatListFromTree()
{
	m_lstAR.clear();

#ifdef _DEBUG
	AssertValid();
#endif

	//find our tree root node, based on the code string
	int nRootIdx = FindRootNode();
	if(nRootIdx >= 0)
	{
		//found tree root, now create flat list out of it
		//(push only FP lines, not FPS lines)
		int nLevel = m_treeAR.m_lstModSets[nRootIdx].nLevel;
		TreeList flatList;
		int nIdx = nRootIdx;
		do{
			if(m_treeAR.m_lstModSets[nIdx].nType == ITEM_TYPE_FP)
			{
				FP_ITEM item = m_treeAR.m_lstModSets[nIdx];

				//if necessary, resolve inherited status
				/*
				if(item[nIdx].nStatus == ITEM_STATUS_INHERITED){
					//search the same item in the FP list
					item.nStatus = m_fpdef.m_lstFP[item.nCode-1].nStatus;
				}
				*/

				//push into list
				flatList.push_back(item);
			}
			nIdx ++;
		}while(nIdx < m_treeAR.m_lstModSets.size() && m_treeAR.m_lstModSets[nIdx].nLevel > nLevel);

		//remove duplicates (prefer active FP instances)
		flatList.Sort();
		nIdx = 0;
		while(nIdx < flatList.size()-1)
		{
			if( /*flatList[nIdx].strName == flatList[nIdx+1].strName &&
				flatList[nIdx].nType == flatList[nIdx+1].nType*/
				flatList[nIdx].nCode == flatList[nIdx+1].nCode)
			{
				flatList.removeAt(nIdx);
			}
			else
				nIdx++;
		}

	#ifdef _DEBUG
		flatList.DumpTree();
	#endif

		//convert flat list to FP list format
		int nCount = flatList.size();
		for(int i=0; i<nCount; i++)
		{
			qDebug() << "list[" << i << "] = " << flatList[i].nCode << flatList[i].strName;

			FP_ITEM1 item;
			item.nCode   = flatList[i].nCode;
			item.bCanUse = (flatList[i].nStatus == ITEM_STATUS_ON || flatList[i].nStatus == ITEM_STATUS_INHERITED)? true : false;
			item.nValue  = (item.bCanUse)? flatList[i].nValue : 0;

			m_lstAR.append(item);
		}

	#ifdef _DEBUG
		AssertValid();
	#endif
	}
}
	
void LicenseModInfo::AR_RefreshTreeFromFlatList(FPDF &defaultFPDF)
{
	m_treeAR = defaultFPDF;	//first overwrite tree with default one (in case it was not initialized yet)

	int nRootIdx = FindRootNode();
	if(nRootIdx >= 0)
	{
		//TOFIX how to preserve ITEM_STATUS_INHERITED state here?
		//find each FP item	occurence within the tree and set its value
		int nLevel = m_treeAR.m_lstModSets[nRootIdx].nLevel;
		int nCount = m_lstAR.size();
		for(int i=0; i<nCount; i++)
		{
			//search entire mod tree (from the given root) for a current FP code
			int nIdx = nRootIdx;
			do{
				if( m_treeAR.m_lstModSets[nIdx].nType == ITEM_TYPE_FP &&
					m_treeAR.m_lstModSets[nIdx].nCode == m_lstAR[i].nCode)
				{
					//copy the data into the tree
					m_treeAR.m_lstModSets[nIdx].nStatus = (m_lstAR[i].bCanUse)? ITEM_STATUS_ON : ITEM_STATUS_OFF;
					m_treeAR.m_lstModSets[nIdx].nValue  = m_lstAR[i].nValue;
					break;	// FP found in the tree, proceed to next one
				}
				nIdx ++;
			}while(nIdx<m_treeAR.m_lstModSets.size() && m_treeAR.m_lstModSets[nIdx].nLevel > nLevel);
		}
	}

#ifdef _DEBUG
	AssertValid();
#endif
}

int LicenseModInfo::FindRootNode()
{
	//find our tree root node, based on the code string
	int nRootIdx = -1;
	int nSibling = 0;
	int nIdx = m_treeAR.m_lstModSets.FindChild(-1, nSibling);
	while(nIdx >= 0)
	{
		if( ITEM_TYPE_MOD == m_treeAR.m_lstModSets[nIdx].nType &&
			    m_strCode == m_treeAR.m_lstModSets[nIdx].strCode)
		{
			nRootIdx = nIdx;
			break;
		}
		//not found yet, keep looking
		nSibling ++;
		nIdx = m_treeAR.m_lstModSets.FindChild(-1, nSibling);
	}

	return nRootIdx;
}

#ifdef _DEBUG
void LicenseModInfo::Dump()
{
	qDebug() << "Code: " << m_strCode;
	qDebug() << "MLIID: " << m_strMLIID;

	int nCnt = m_lstAR.size();
	for(int i=0; i<nCnt; i++){
		QString strName;
		int nCode = m_lstAR[i].nCode;
		int nSubCnt = m_treeAR.m_lstFP.size();
		for(int j=0; j<nSubCnt; j++){
			if(m_treeAR.m_lstFP[j].nCode == nCode){
				strName = m_treeAR.m_lstFP[j].strName;
				break;
			}
		}

		qDebug() << "Code:" << nCode << "CanUse:" << m_lstAR[i].bCanUse  << "Value:" << m_lstAR[i].nValue << "Name:" << strName;
	}
	qDebug() << "Total FSs:" << nCnt;
}
#endif

#ifdef _DEBUG
void LicenseModInfo::AssertValid()
{
	//check for duplicate FP codes in a list!
	int nCnt = m_treeAR.m_lstFP.size();
	for(int i=0; i<nCnt; i++){
		for(int j=0; j<nCnt; j++){
			if(i != j)
				Q_ASSERT(m_treeAR.m_lstFP[i].nCode != m_treeAR.m_lstFP[j].nCode);
		}
	}

	//check for duplicate FP codes (2nd part)!
	nCnt = m_lstAR.size();
	for(int i=0; i<nCnt; i++){
		int nCode = m_lstAR[i].nCode;
		for(int j=0; j<nCnt; j++){
			if(i != j){
				Q_ASSERT(nCode != m_lstAR[j].nCode);
			}
		}
	}
}
#endif