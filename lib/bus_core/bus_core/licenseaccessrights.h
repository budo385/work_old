#ifndef LICENSEACCESSRIGHTS_H
#define LICENSEACCESSRIGHTS_H

#include <QString>
#include <QMap>
#include "licenseinfo.h"
#include "common/common/dbrecordset.h"
#include "licenseaccessrightsbase.h"

class LicenseAccessRights : public LicenseAccessRightsBase
{
public:
	LicenseAccessRights();
	~LicenseAccessRights();

	bool IsLoaded(){ return m_bLoaded; };
	bool LoadLicense(QString strFile);
	bool CanUseFP(QString strModule, QString strMLIID, int nFPCode, int &nValue);
	bool GetARList(QString strModule, QString strMLIID, LicenseModInfo **list = NULL);
	LicenseInfo &GetLicenseInfo(){	return m_info; };
	int GetUniqueLicenseID();
	int GetMaxLicenseCount(QString& strModuleCode, QString& strMLIID);
	void GetFPList(QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList);
	void GetModuleCodes(QHash<QString,QString>& lstModules); //<code,mlid> pairs

	int GetCustomSolutionID(){ return m_info.m_nCustomSolutionID; };

protected:
	int GetDataIdx(QString strModule, QString strMLIID);

protected:
	bool m_bLoaded;	
	LicenseInfo m_info;

	//map license info access by Module Code + MLIID Code key combination
	typedef QMap<QString, int> MapMLIID;
	typedef QMap<QString, MapMLIID> MapModule;

	MapModule m_mapMod;
};

#endif	// LICENSESTREAMREADER_H
