#include "formatphone.h"
#include "countries.h"

static QString m_strDefCountry="0041";
static QString m_strDefArea="056";

FormatPhone::FormatPhone()
{
}

FormatPhone::~FormatPhone()
{


}

//set defaults after login/settings change: 
void FormatPhone::SetDefaults(QString strCountry,QString strArea)
{
	m_strDefCountry = strCountry;
	m_strDefArea= strArea;
}



//format phone
QString FormatPhone::FormatPhoneNumber(QString strCountry,QString strArea,QString strLocal)
{
	//remove 00 from country:
	if (strCountry.indexOf("00")==0)
		strCountry.remove(0,2);

	//remove 0 from area:
	if (strArea.indexOf("0")==0)
		strArea.remove(0,1);

	//set as +XX (YY) LLL LLL:
	if (strCountry=="39" && strArea.left(1)!="3")		//hard fix for Italy: MB
		return "+"+strCountry+" 0"+strArea+" "+strLocal;
	else
		return "+"+strCountry+" (0)"+strArea+" "+strLocal;	
}


//check phone parts
bool FormatPhone::CheckPhone(QString strCountry,QString strArea,QString strLocal)
{
	bool bOK;
	int nTemp;
	nTemp=strCountry.toInt(&bOK);
	if (!bOK)return false;
	
	nTemp=strArea.toInt(&bOK);
	if (!bOK)return false;
	
	nTemp=strLocal.toInt(&bOK);
	if (!bOK)return false;

	return true;

}
//generate search number without leading zero's in area: 0041213344
QString FormatPhone::GenerateSearchNumber(QString strCountry,QString strArea,QString strLocal)
{
	if (strArea.indexOf("0")==0 && ((strCountry=="0039" && strArea.left(2)=="03") || strCountry!="0039")) //hard fix for italy: keep 0
		strArea.remove(0,1);

	QString strSearch=strCountry+strArea+strLocal.trimmed();

	//remove all chars non numbers:
	return FilterNonDigits(strSearch);
}



//split phone on parts, also returns formmated phone , replaces
void FormatPhone::SplitPhoneNumber(QString strPhone, QString &strCountry,QString &strArea,QString &strLocal,QString &strSearch,QString &strCountryISO)
{

	//if NULL or empty exit
	if (strPhone.isEmpty())
	{
		strArea="";
		strCountry="";
		strLocal="";
		strSearch="";
		return;
	}

	//-----------------------------------------
	//SKYPE SEARCH: find any alhpabethic char except + ( )
	//-----------------------------------------

	int nSize=strPhone.size();
	for(int i=0;i<nSize;++i)
	{
		//found letter-> it must be skype
		if (strPhone.at(i).isLetter() && strPhone.at(i)!='+' && strPhone.at(i)!='(' && strPhone.at(i)!=')')
		{
			strArea="";
			strCountry="";
			strLocal="";
			strSearch=strPhone;
			return;
		}
	}




	//-----------------------------------------
	//COUNTRY PARSER:
	//-----------------------------------------

	strPhone = strPhone.trimmed();				//cut of spaces

	//if first 00 get till first space?
	if (strPhone.indexOf("+")==0)
	{
		if (strPhone.indexOf("+",1)==1)
		{
			strPhone.remove(0,2);
			strPhone.insert(0,"00");
		}
		else
		{
			strPhone.remove(0,1);
			strPhone.insert(0,"00");
		}
	}


	strCountry=m_strDefCountry;

	//country found (two zero's):
	if (strPhone.left(2)=="00")
	{
		//get maximum number for country=5chars=00xxx:
		int nSpace=strPhone.left(5).indexOf(" ");
		if (nSpace!=-1)
		{
			strCountry=strPhone.left(nSpace);	//country is to first space
			strPhone.remove(0,nSpace);
		}
		else
		{
			//check for zero's, skip two leading:
			int nZero=strPhone.left(5).indexOf("0",2);
			if (nZero!=-1)
			{
				strCountry=strPhone.left(nZero);  //country is to first zero
				strPhone.remove(0,nZero);
			}
			else
			{
				//filter digits from right:
				int nCountryCodeLen=5;
				if (strPhone.length()>=5) //breaks when press + and less then 5
				{
					for (int i=4;i>1;--i)
					{
						if (!strPhone.at(i).isNumber())
						{
							nCountryCodeLen=i+1;
						}
					}
					if (nCountryCodeLen>2)
					{
						strCountry=strPhone.left(nCountryCodeLen);
						if (Countries::GetCountryISOFromPredial(strCountry).isEmpty())
						{
							strCountry.chop(1);
							nCountryCodeLen--;
						}

						strPhone.remove(0,nCountryCodeLen);
					}
				}
			}
		}
	}


	//-----------------------------------------
	//AREA PARSER:
	//-----------------------------------------
	//now muv to area code: can contain (0)21, (021) or just space deilimiter till local number: 021 xxx xxx
	//max chars=3
	strPhone=strPhone.trimmed();

	//get maximum number for country=5chars=00xxx:
	int nSpace=strPhone.indexOf(" ");
	if (nSpace!=-1)
	{
		//now filter non digits:
		strArea=strPhone.left(nSpace);
		strArea=FilterNonDigits(strArea);
		if (strArea.indexOf("0")!=0)
		{
			strArea=strArea.insert(0,"0");
		}
		strLocal=strPhone.remove(0,nSpace);
	}
	else
	{
		//try find  brackets: ")"
		int nBracketStart=strPhone.indexOf("(");
		if (nBracketStart!=-1)
		{
			int nBracketEnd=strPhone.indexOf(")");
			if (nBracketEnd!=-1)
			{
				if (nBracketEnd-nBracketStart==2)
				{
					if (strPhone.at(nBracketStart+1)=='0')
					{
						//take two after bracket:
						//now filter non digits:
						strArea=strPhone.mid(nBracketEnd+1,2);
						strArea=FilterNonDigits(strArea);
						if (strArea.indexOf("0")!=0)
						{
							strArea=strArea.insert(0,"0");
						}
						strLocal=strPhone.remove(0,nBracketEnd+3);
					}
					else
					{
						//take bracket (one number)
						//now filter non digits:
						strArea=strPhone.mid(nBracketStart+1,nBracketEnd-nBracketStart);
						strArea=FilterNonDigits(strArea);
						if (strArea.indexOf("0")!=0)
						{
							strArea=strArea.insert(0,"0");
						}
						strLocal=strPhone.remove(0,nBracketEnd);
					}
				}
				else
				{
					//take bracket (more numbers)
					//now filter non digits:
					strArea=strPhone.mid(nBracketStart+1,nBracketEnd-nBracketStart);
					strArea=FilterNonDigits(strArea);
					if (strArea.indexOf("0")!=0)
					{
						strArea=strArea.insert(0,"0");
					}
					strLocal=strPhone.remove(0,nBracketEnd);
				}
			}
			else
			{
				//take three:
				//now filter non digits:
				strArea=strPhone.left(3);
				strArea=FilterNonDigits(strArea);
				if (strArea.indexOf("0")!=0)
				{
					strArea=strArea.insert(0,"0");
				}
				strLocal=strPhone.remove(0,3);
			}
		}
		else
		{
			//take first 3 and get away:
			//now filter non digits:
			int nTake=3;
			if (strArea.indexOf("0")==0)
				strArea=strPhone.left(3);		//take 3 
			else
			{
				strArea=strPhone.left(2);		//take 2
				nTake=2;
			}

			strArea=FilterNonDigits(strArea);
			if (strArea.indexOf("0")!=0)
			{
				strArea=strArea.insert(0,"0");
			}
			strLocal=strPhone.remove(0,nTake);
		}
	}


	//check for non numbers (area is checked):
	strCountry=FilterNonDigits(strCountry);
	strLocal=FilterNonDigits(strLocal);

	//disolve local on two: or first three if unpair size:
	int nLocalSize=strLocal.size();
	int nUnPair=nLocalSize%2;
	int nJump=2;
	if (nUnPair)
		nJump=3;
	for (int i=0;i<nLocalSize;i++)
	{
		if (nJump<nLocalSize)
		{
			strLocal.insert(nJump," ");
			nJump+=3;
		}
		else
			break;
	}


	//combine search string:
	strSearch=GenerateSearchNumber(strCountry,strArea,strLocal);

	//check for country ISO code:
	strCountryISO=Countries::GetCountryISOFromPredial(strCountry);
	
}
		




//filter out non digits:
QString FormatPhone::FilterNonDigits(QString &strInput)
{
	QString strClean;

	//remove all chars non numbers:
	int nSize=strInput.size();
	for(int i=0;i<nSize;++i)
	{
		if (strInput.at(i).isNumber())
		{
			strClean+=strInput.at(i);
		}
	}

	return strClean;
}


void FormatPhone::ReFormatPhoneNumbers(DbRecordSet &lstPhones, bool bOnlyFormatSearch)
{

	//set phones:
	int nPhoneCol=lstPhones.getColumnIdx("BCMP_FULLNUMBER");
	int nCountryCol=lstPhones.getColumnIdx("BCMP_COUNTRY");
	int nAreaCol=lstPhones.getColumnIdx("BCMP_AREA");
	int nLocalCol=lstPhones.getColumnIdx("BCMP_LOCAL");
	int nISOCol=lstPhones.getColumnIdx("BCMP_COUNTRY_ISO");
	int nSearchCol=lstPhones.getColumnIdx("BCMP_SEARCH");
	int nSize=lstPhones.getRowCount();

	bool bIsDefaultSet=false;
	for(int k=0;k<nSize;k++)
	{
		//calculate formated number:
		QString strPhone,strCountry,strLocal,strISO,strArea,strSearch;
		SplitPhoneNumber(lstPhones.getDataRef(k,nPhoneCol).toString(),strCountry,strArea,strLocal,strSearch,strISO);
		if (!bOnlyFormatSearch)
		{
			if (!strArea.isEmpty() || !strLocal.isEmpty()) //skip if skype
				lstPhones.setData(k,nPhoneCol,FormatPhoneNumber(strCountry,strArea,strLocal));

			lstPhones.setData(k,nCountryCol,strCountry);
			lstPhones.setData(k,nAreaCol,strArea);
			lstPhones.setData(k,nLocalCol,strLocal);
			lstPhones.setData(k,nISOCol,strISO);
		}
		lstPhones.setData(k,nSearchCol,strSearch);

		//reset empty phone as non default:
		if (lstPhones.getDataRef(k,nPhoneCol).toString().isEmpty() && lstPhones.getDataRef(k,"BCMP_IS_DEFAULT").toInt()>0)
			lstPhones.setData(k,"BCMP_IS_DEFAULT",0);

		//check if default is set:
		if (lstPhones.getDataRef(k,"BCMP_IS_DEFAULT").toInt()>0)
			bIsDefaultSet=true;

		if (lstPhones.getDataRef(k,"BCMP_TYPE_ID").toInt()<=0)
		{
			QVariant empty(QVariant::Int);
			lstPhones.setData(k, "BCMP_TYPE_ID", empty); //do not allow non valid values
		}

	}

	//issue set default on first phone with type 
	if (!bIsDefaultSet)
	{
		for(int k=0;k<nSize;k++)
		{
			if (!lstPhones.getDataRef(k,"BCMP_TYPE_ID").isNull())
			{
				lstPhones.setData(k,"BCMP_IS_DEFAULT",1);
				bIsDefaultSet=true;
				break;
			}
		}
	}

	//if no phone has type and no def, set to first:
	if (lstPhones.getRowCount()>0 && !bIsDefaultSet)
	{
		lstPhones.setData(0,"BCMP_IS_DEFAULT",1);
	}

}