#ifndef ACCESSRIGHT_H
#define ACCESSRIGHT_H

#include "accessrightsid.h"
#include "common/common/dbrecordset.h"
#include "db_core/db_core/dbsqltableview.h"

class AccessRight
{
public:
    AccessRight();
	AccessRight(DbRecordSet &AccessRightRecordset);
	void operator =(const DbRecordSet &that); 
    ~AccessRight();

	void GetARRecordSet(DbRecordSet &ARRecordSet);
private:
	DbRecordSet m_recAccessRight; 
};

#endif // ACCESSRIGHT_H
