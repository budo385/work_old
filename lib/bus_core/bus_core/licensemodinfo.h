#ifndef LICENSEMODULEINFO_H
#define LICENSEMODULEINFO_H

#include <QDateTime>
#include "fpdf.h"

//TOFIX use same struct in C++ generation
typedef struct FP_ITEM1{
	int	 nCode;		// unique code
	bool bCanUse;	// can FP be used or not (access right)
	int	 nValue;	// value

	FP_ITEM1(){};
	FP_ITEM1(const FP_ITEM1 &other){	operator=(other); };
	void operator=(const FP_ITEM1 &other)
	{
		nCode	= other.nCode;		// unique code
		bCanUse	= other.bCanUse;	// can FP be used or not (access right)
		nValue	= other.nValue;		// value
	}
} FP_ITEM1;

class LicenseModInfo 
{
public:
	LicenseModInfo();
	LicenseModInfo(const LicenseModInfo &other);
	void operator =(const LicenseModInfo &other);

	void AR_RefreshFlatListFromTree();
	void AR_RefreshTreeFromFlatList(FPDF &defaultFPDF);
	int	 FindRootNode();

#ifdef _DEBUG
	void Dump();
	void AssertValid();
#endif

public:
	//internal data
	QString			m_strModuleName;	

	//data to be written into license
	QString			m_strCode;			//'type' store module code here
	QString			m_strSubCode;		//'subtype'
	QString			m_strName;
	QString			m_strReportLine1;
	QString			m_strReportLine2;
	int				m_nCustomSolutionID;
	QString			m_strMLIID;
	unsigned char   m_nLicensingType;
	unsigned int    m_nNumLicenses;
	short int		m_nLicensePlatforms;
	short int		m_nLicensePlatformsSubcode;
	QDateTime		m_dateCreated;
	QDateTime		m_dateModified;
	QDateTime		m_dateExpiration;
	QDateTime		m_dateWarning;
	bool			m_bUseExpireDate;
	QString			m_strModifiedBy;
	QString			m_strSerialNumber;
	QByteArray		m_arEncBlob;	//license + type + subtype + mliid
	
	QList<FP_ITEM1> m_lstAR;	//flat list with access rights
	FPDF			m_treeAR;	//keep access rights in a tree like this
	int				m_nModTreeSibling;
};

#endif	// LICENSEMODULEINFO_H
