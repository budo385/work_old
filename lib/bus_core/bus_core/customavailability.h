#ifndef CUSTOMAVAILABILITY_H
#define CUSTOMAVAILABILITY_H

#define CUSTOM_SOL_AS		1200	//"Anestethics schedule" custom solution
#define CUSTOM_SOL_MASTER	9999	//master custom solution key file (can view all custom solutions)

class CustomAvailability 
{
public:
	static void SetCustomSolutionID(int nID){ m_nCustomSolutionID = nID; };
	static int  GetCustomSolutionID(){ return m_nCustomSolutionID; };
	
	//test for solution availability
	static bool IsAvailable_AS_CalendarViewWizard()
	{
		//B.T. I can not test for ERB -> make your own keyfile with CustomSolution=1200
		/*
#ifdef _DEBUG
		return true;
#else*/
		return (CUSTOM_SOL_AS == m_nCustomSolutionID || CUSTOM_SOL_MASTER == m_nCustomSolutionID); 
//#endif
	}

protected:
	static int m_nCustomSolutionID;
};

#endif // CustomAvailability_H
