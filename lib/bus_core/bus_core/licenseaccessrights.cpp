#include "licenseaccessrights.h"
#include "fp_default.h"

LicenseAccessRights::LicenseAccessRights()
{
	m_bLoaded = false;
}

LicenseAccessRights::~LicenseAccessRights()
{
}

bool LicenseAccessRights::LoadLicense(QString strFile)
{
	//load license key file
	if(m_info.Load(strFile))
	{
		//
		// map the license contents for fast data access
		//
		m_mapMod.clear();
		
		for(int i=0; i<m_info.m_lstModInfo.size(); i++)
		{
			QString strCode  = m_info.m_lstModInfo[i].m_strCode;
			QString strMLIID = m_info.m_lstModInfo[i].m_strMLIID;

			//use existing submap or add the new one
			MapMLIID submap;
			if(m_mapMod.contains(strCode))
				submap = m_mapMod[strCode];
			
			submap[strMLIID] = i;		//update MLIID->index submap
			m_mapMod[strCode] = submap;	//store in the main map
		}

		m_bLoaded = true;
		return true;
	}
	return false;
}

int LicenseAccessRights::GetDataIdx(QString strModule, QString strMLIID)
{
	// get license array index from Mod + MLIID keys
	int nLicIdx = -1;
	if(m_mapMod.contains(strModule))
		if(m_mapMod[strModule].contains(strMLIID))
			nLicIdx = m_mapMod[strModule][strMLIID];

	return nLicIdx;
}

bool LicenseAccessRights::CanUseFP(QString strModule, QString strMLIID, int nFPCode, int &nValue)
{
	Q_ASSERT(IsLoaded());

	//get actual access right for this FP
	bool bFound = false;

	int nLicIdx = GetDataIdx(strModule, strMLIID);
	if(nLicIdx >= 0) // Module + MLIID section found
	{
		//TOFIX map this too? (FP code to index?)
		// search by all FP in this section
		int nCnt = m_info.m_lstModInfo[nLicIdx].m_lstAR.size();
		for(int i=0; i<nCnt; i++)
		{
			if(m_info.m_lstModInfo[nLicIdx].m_lstAR[i].nCode == nFPCode)
			{
				//printf("AR: FP found inside License\n");
				nValue = m_info.m_lstModInfo[nLicIdx].m_lstAR[i].nValue;
				return m_info.m_lstModInfo[nLicIdx].m_lstAR[i].bCanUse;
			}
		}
	}
	
	//if AR not found in the key file, look the default table
	int nModCode = ARDefault_GetModuleCode(strModule.toLatin1().constData());

	//get default access right for some FP
	//printf("AR: FP not found in license, search for default rights...\n");
	return ARDefault_CanUseFP(nModCode, nFPCode, nValue);
}

bool LicenseAccessRights::GetARList(QString strModule, QString strMLIID, LicenseModInfo **list)
{
	int nLicIdx = GetDataIdx(strModule, strMLIID);
	if(nLicIdx >= 0) // Module + MLIID section found
	{
		if(NULL != list)
			*list = &(m_info.m_lstModInfo[nLicIdx]);
		return true;
	}
	return false;
}



//-----------------------------------------------------------
//				EXTRA FUNCTIONS
//-----------------------------------------------------------

int LicenseAccessRights::GetUniqueLicenseID()
{
	Q_ASSERT(IsLoaded());
	return GetLicenseInfo().m_nLicenseID;
}

//returns -1 if module code & mlid combination does not exist
int LicenseAccessRights::GetMaxLicenseCount( QString& strModuleCode, QString& strMLIID)
{
	Q_ASSERT(IsLoaded());

	LicenseModInfo *list = NULL;
	if(!GetARList(strModuleCode, strMLIID, &list))
		return -1;
	else
		return list->m_nNumLicenses;
}



void LicenseAccessRights::GetFPList(QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList)
{

	//redefine result recordset
	RetOut_pList.destroy();
	RetOut_pList.addColumn(QVariant::Int, "FP_Code");
	RetOut_pList.addColumn(QVariant::Int, "FP_CanUse");
	RetOut_pList.addColumn(QVariant::Int, "FP_Value");

	#ifdef _DEBUG
		//m_info.Dump();
	#endif

	//build access right list for a given Module/MLIID combination
	for(int i=1; i<=MAX_FPS_ID; i++)
	{
		RetOut_pList.addRow();

		RetOut_pList.setData(i-1, 0, i);	//set code

		int nValue = 0;
		if(CanUseFP(strModuleCode, strMLIID, i, nValue))	
			RetOut_pList.setData(i-1, 1, 1);	//set access right - enabled
		else
			RetOut_pList.setData(i-1, 1, 0);	//set access right - disabled

		RetOut_pList.setData(i-1, 2, nValue);	//set value
	}
}




void LicenseAccessRights::GetModuleCodes(QHash<QString,QString>& lstModules)
{
	lstModules.clear();
	//LicenseInfo lic=GetLicenseInfo();
	int nSize=m_info.m_lstModInfo.count();
	for(int i=0;i<nSize;++i)
	{
		lstModules[m_info.m_lstModInfo.at(i).m_strCode]=m_info.m_lstModInfo.at(i).m_strMLIID;
	}
}