#ifndef SERVERCONTROLABSTRACT_H
#define SERVERCONTROLABSTRACT_H

#include "common/common/status.h"
#include <QString>
#include "common/common/dbrecordset.h"
#include "trans/trans/httpcontext.h"
#include "trans/trans/httpserver.h"
#include "bus_core/bus_core/serverinifile.h"


class ServerControlAbstract 
{
public:

	virtual ~ServerControlAbstract(){};

	//main API
	virtual void Start(Status &pStatus)=0;
	virtual void Stop()=0;
	virtual void Restart(Status &pStatus)=0;
	virtual void RestartDelayed(int nReStartAfterSec)=0;
	virtual void StopDelayed(int nStopAfterSec=0)=0;
	virtual bool IsRunning(){return false;}
	virtual void SaveSettings()=0;
	virtual HTTPContext GetThreadContext(int nThreadID)=0;
	virtual HTTPServer* GetHttpServer(){return NULL; };
	//demo
	virtual void TestDemoDatabaseSize(Status &pStatus,  bool &bFireWarningMsg, int &nCurrSizeKb, int &nMaxSizeKb)=0; 
	virtual ServerIniFile* GetIniFile()=0;
	virtual void ReloadWebPages(Status &pStatus)=0;

	virtual void SendErrorMessage(int nPersonID, QString strErrorText, int nErrorCode=1, QString strEmailAccount=""){};
	virtual void SendNewEmailNotification(int nPersonID, QString strEmailAccount,DbRecordSet &lstNewEmails){};
	virtual void SendPushNotification(int nPersonID, QString strMessage, QString strPushToken, int nOSType, QString strPushSoundName, int nBadgeNumber){};

private:
	
};

#endif // SERVERCONTROLABSTRACT_H
