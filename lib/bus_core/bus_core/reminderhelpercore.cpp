#include "reminderhelpercore.h"
#include "db_core/db_core/dbsqltableview.h"


void ReminderHelperCore::CreateDefaultCategorySettings(DbRecordSet &rowData)
{
	rowData.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_REMINDER_SETTINGS));

	rowData.addRow();
	rowData.setData(0,"REM_ONCE_BEFORE",30);
	rowData.setData(0,"REM_EVERY_N_BEFORE",0);
	rowData.setData(0,"REM_EVERY_N_AFTER",0);
	rowData.setData(0,"REM_DELETE_IF_OVERDUE",0);
	rowData.setData(0,"REM_SEND_BY_SOKRATES",1);
	rowData.setData(0,"REM_SEND_BY_EMAIL",0);
	rowData.setData(0,"REM_SEND_BY_SMS",0);
}