#ifndef ACCESSRIGHTSID_H
#define ACCESSRIGHTSID_H

/*!
\brief Definitions of Access Rights and Access Rights Sets IDs
\ingroup Db

Header file with definitions for Access Rights and Access Rights Sets IDs.

*/

//************************************************************************
//**					  Access Right Set ID's							**
//************************************************************************
#define PERSONAL_RECORDS_MAINTAINANCE									0
#define USER_RECORDS_MAINTAINANCE										1
#define ORGANIZATIONS_MAINTAINANCE										2
#define DEPARTMENTS_MAINTAINANCE										3
#define ACCESS__RIGHTS													4
#define LOGIN															5
#define SYSTEM_RIGHTS													6
#define OPTIONS_AND_PERSONAL_SETTINGS									7

//SIMPLE ACCESS RIGHTS SETS.
#define PROJECT_MANAGER													8
#define ADMINISTRATOR													9
#define HUMAN_RESOURCES													10
#define MAIN_DATA														11
#define CONTACT_MANAGER													12
#define COMMUNICATOR													13
#define GROUP_MANAGEMENT												14
#define TEMPLATE_MANAGER												15
#define PROJECT_LIST_MANAGEMENT											16
#define CALENDAR_ACCESS													17


//************************************************************************
//**						Access Right ID's							**
//************************************************************************
#define OPEN_PERSON_FUI													0 
#define SEE_PERSONAL_RECORDS_DATA										1
#define EDIT_PERSONAL_RECORDS											2
#define INSERT_PERSONAL_RECORDS											3
#define DELETE_PERSONAL_RECORDS											4
#define SEE_COMPANY_PERSONS												5
#define EDIT_INSERT_DELETE_COMPANY_PERSONS								6
#define SEE_DEPARTMENT_PERSONS											7
#define EDIT_INSERT_DELETE_DEPARTMENT_PERSONS							8
#define EMPLOYMENT_DATA_VISIBLE											9
#define GENERATE_USER_FROM_PERSON										10

#define OPEN_USER_FUI													11
#define SEE_USER_RECORDS_DATA											12
#define EDIT_USER_RECORDS												13
#define INSERT_USER_RECORDS												14
#define DELETE_USER_RECORDS												15
#define SEE_ONLY_ONE_COMPANY_USERS										16
#define GENERATE_PERSON_FROM_USER										17

#define OPEN_ORGANIZATIONS_FUI											18
#define SEE_ORGANIZATIONS_RECORDS										19
#define EDIT_ORGANIZATIONS_RECORDS										20
#define INSERT_ORGANIZATIONS_RECORDS									21
#define DELETE_ORGANIZATIONS_RECORDS									22
#define SEE_ONLY_ONE_NODE_ORGANIZATIONS									23
#define EDIT_INSET_DELETE_ONE_NODE_ORGANIZATIONS						24
#define DEPARTMENT_DATA_VISIBLE_IN_ORGANIZATION_FUI						25
#define COST_CENTER_DATA_VISIBLE_IN_ORGANIZATION_FUI					26
#define MODIFY_DEPARTMENTS_IN_ORGANIZATION_FUI							27
#define MODIFY_COST_CENTERS_IN_ORGANIZATION_FUI							28

#define OPEN_DEPARTMENTS_FUI											29
#define SEE_DEPARTMENTS_RECORDS											30
#define EDIT_DEPARTMENTS_RECORDS										31
#define INSERT_DEPARTMENTS_RECORDS										32
#define DELETE_DEPARTMENTS_RECORDS										33
#define SEE_ONLY_ONE_COMPANY_DEPARTMENTS								34
#define EDIT_INSERT_DELETE_ONE_COMPANY_DEPARTMENTS						35
#define SEE_ONLY_ONE_NODE_DEPARTMENTS									36
#define EDIT_INSET_DELETE_ONE_NODE_DEPARTMENTS							37
#define COST_CENTER_DATA_VISIBLE_IN_DEPARTMENT_FUI						38
#define MODIFIY_COST_CENTERS_IN_DEPARTMENT_FUI							39

#define OPEN_USER_ACCESS_RIGHTS_FUI										40
#define MODIFIY_USER_ROLES												41
#define OPEN_PERSONAL_ACCESS_RIGHTS_FUI									42
#define MODIFIY_PERSON_ROLES											43

#define MAY_LOG_IN														44
#define NO_LOG_IN_BEFORE												45
#define NO_LOG_IN_AFTER													46

#define MAY_SEE_SYSTEM_INFORMATION										47
#define MAY_SEE_SESSIONS_INFORMATION									48
#define MAY_KILL_SESSIONS												49

#define VIEW_OPTIONS													50
#define MODIFY_OPTIONS													51
#define VIEW_OWN_PERSONAL_SETTINGS										52
#define MODIFY_OWN_PERSONAL_SETTINGS									53
#define VIEW_PERSONAL_SETTINGS_OF_OTHERS								54
#define MODIFY_PERSONAL_SETTINGS_OF_OTHERS								55

//SIMPLE RIGHTS.
#define ENTER_MODIFY_PROJECT_MAIN_DATA									56
#define SYNCHRONIZE_PROJECTS_AND_DEBTORS								57
#define IMPORT_PROJECTS													58
#define EXPORT_PROJECTS													59
#define ADMINISTRATION_TOOLS_ACCESS										60
#define OPTIONS															61
#define REGISTER_APPLICATIONS											62
#define ENTER_MODIFY_USERS												63
#define ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU			64
#define ENTER_MODIFY_CONTACTS											65
#define ASSIGN_CE_ENTITIES_TO_PROJECTS_AND_CONTACTS						66
#define ENTER_MODIFY_CONTACT_GROUP_TREES								67
#define REGISTER_TEMPLATES												68

#define LOAD_PROJECT_LISTS												69
#define ENTER_MODIFY_PRIVATE_PROJECT_LISTS								70
#define ENTER_MODIFY_PUBLIC_PROJECT_LISTS								71

#define MODIFY_FOREIGN_PERSONAL_SETTINGS								72
#define MODIFY_CALENDAR_CATEGORY										73
#define MODIFY_CALENDAR_SHOW_TIME_AS									74
#define MODIFY_DOCUMENTS_CATEGORY										75
#define MODIFY_NMRX_ROLES												76
#define ENTER_MODIFY_CONTACT_ASSIGNMENT_ROLE							77

#define ENTER_MODIFY_CONTACT_ASSIGNMENT_ROLE							77
#define OPEN_CALENDAR													78
#define ENTER_MODIFY_CALENDAR											79
#define ASSIGN_CONTACTS													80
#define ASSIGN_PROJECTS													81



//************************************************************************
//**							DEFAULT ROLES							**
//************************************************************************
#define ADMINISTRATOR_ROLE												0
#define SECRETARY_BE_ROLE												1
#define PROJECT_MANAGER_BE_ROLE 										2
#define USER_BE_ROLE													3
#define USER_TE_ROLE													4

//************************************************************************
//**					Access Right Set Version ID's					**
//**					  (NOT IMPLEMENTED FOR NOW)						**
//************************************************************************
#define ARS_VERSION_0													0

#endif // ACCESSRIGHTSID_H
