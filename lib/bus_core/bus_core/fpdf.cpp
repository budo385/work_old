#include "fpdf.h"
#include "common/common/inifile.h"
#include <qDebug>

FPDF::FPDF()
{
	m_bModified = false;
}

FPDF::FPDF(const FPDF &other)
{
	operator = (other);
}

void FPDF::Clear()
{
	m_lstFP.clear();
	m_lstFpSets.clear();
	m_lstModSets.clear();
	m_bModified = false;
}

bool FPDF::IsEmpty()
{
	return (0 == m_lstFP.size() && 0 == m_lstFpSets.size() && 0 == m_lstModSets.size());
}

void FPDF::operator =(const FPDF &other)
{
	if(this != &other){
		m_lstFP			= other.m_lstFP;
		m_lstFpSets		= other.m_lstFpSets;
		m_lstModSets	= other.m_lstModSets;
		m_bModified		= other.m_bModified;
	}
}

bool FPDF::Load(const char *szFile)
{
	m_strFile = "";
	Clear();

	IniFile ini;
	ini.SetPath(szFile);
	if(!ini.Load())
		return false;

	//load a list of function points
	int nCount = 0;
	ini.GetValue("FunctionPoints", "Count", nCount);
	for(int i=0; i<nCount; i++)
	{
		//load a single function point info
		QString strKey, strName, strDesc;
		int nVisible, nValue, nStatus, nCode;

		strKey.sprintf("Name%d", i+1);
		ini.GetValue("FunctionPoints", strKey, strName);

		strKey.sprintf("Active%d", i+1);
		ini.GetValue("FunctionPoints", strKey, nVisible, 1);

		strKey.sprintf("Value%d", i+1);
		ini.GetValue("FunctionPoints", strKey, nValue, 0);

		strKey.sprintf("Status%d", i+1);
		ini.GetValue("FunctionPoints", strKey, nStatus, 0);

		strKey.sprintf("Code%d", i+1);
		ini.GetValue("FunctionPoints", strKey, nCode, -1);

		strKey.sprintf("Description%d", i+1);
		ini.GetValue("FunctionPoints", strKey, strDesc);

		AddFunctionPoint(strName, strDesc, nCode, nVisible, nValue, nStatus);
	}

	//load a list of function point sets
	nCount = 0;
	ini.GetValue("FunctionPointSets", "Count", nCount);
	for(int i=0; i<nCount; i++)
	{
		//load a single function point info
		QString strKey, strName, strDesc;
		int nLevel, nValue, nVisible, nType, nCode, nStatus;

		strKey.sprintf("Name%d", i+1);
		ini.GetValue("FunctionPointSets", strKey, strName);

		strKey.sprintf("Value%d", i+1);
		ini.GetValue("FunctionPointSets", strKey, nValue, 0);

		strKey.sprintf("Type%d", i+1);
		ini.GetValue("FunctionPointSets", strKey, nType);

		strKey.sprintf("Level%d", i+1);
		ini.GetValue("FunctionPointSets", strKey, nLevel, 0);

		strKey.sprintf("Active%d", i+1);
		ini.GetValue("FunctionPointSets", strKey, nVisible, 1);

		strKey.sprintf("Code%d", i+1);
		ini.GetValue("FunctionPointSets", strKey, nCode, -1);

		strKey.sprintf("Status%d", i+1);
		ini.GetValue("FunctionPointSets", strKey, nStatus, 0);

		strKey.sprintf("Description%d", i+1);
		ini.GetValue("FunctionPointSets", strKey, strDesc);

		AddFunctionPointSet(strName, strDesc, nCode, nStatus, nLevel, nType, nValue, nVisible);
	}

	//load a list of modules
	nCount = 0;
	ini.GetValue("Modules", "Count", nCount);
	for(int i=0; i<nCount; i++)
	{
		//load a single module info
		QString strKey, strCode, strName, strDesc;
		int nVisible, nLevel, nValue, nType, nCode, nStatus;

		strKey.sprintf("Name%d", i+1);
		ini.GetValue("Modules", strKey, strName);

		strKey.sprintf("Code%d", i+1);
		ini.GetValue("Modules", strKey, strCode);	

		strKey.sprintf("Value%d", i+1);
		ini.GetValue("Modules", strKey, nValue, 0);

		strKey.sprintf("Type%d", i+1);
		ini.GetValue("Modules", strKey, nType, 0);

		strKey.sprintf("Active%d", i+1);
		ini.GetValue("Modules", strKey, nVisible, 1);

		strKey.sprintf("Level%d", i+1);
		ini.GetValue("Modules", strKey, nLevel, 0);

		strKey.sprintf("Status%d", i+1);
		ini.GetValue("Modules", strKey, nStatus, 0);

		strKey.sprintf("NumCode%d", i+1);
		ini.GetValue("Modules", strKey, nCode, -1);

		strKey.sprintf("Description%d", i+1);
		ini.GetValue("Modules", strKey, strDesc);

		AddModule(strCode, strName, strDesc, nCode, nStatus, nLevel, nType, nValue, nVisible);
	}

	m_strFile = szFile;
#ifdef _DEBUG
	//m_lstModSets.DumpTree();
	AssertValid();
#endif
	m_bModified = false;
	return true;
}

bool FPDF::Save(const char *szFile)
{
	IniFile ini;
	ini.SetPath(szFile);

	//save a list of function points
	int nCount = m_lstFP.size();
	ini.SetValue("FunctionPoints", "Count", nCount);

	for(int i=0; i<nCount; i++)
	{
		//save a single FP info
		QString strKey;

		strKey.sprintf("Name%d", i+1);
		ini.SetValue("FunctionPoints", strKey, m_lstFP[i].strName);

		strKey.sprintf("Active%d", i+1);
		ini.SetValue("FunctionPoints", strKey, m_lstFP[i].bActive);

		strKey.sprintf("Value%d", i+1);
		ini.SetValue("FunctionPoints", strKey, m_lstFP[i].nValue);

		strKey.sprintf("Code%d", i+1);
		ini.SetValue("FunctionPoints", strKey, m_lstFP[i].nCode);

		strKey.sprintf("Status%d", i+1);
		ini.SetValue("FunctionPoints", strKey, (m_lstFP[i].nStatus == ITEM_STATUS_ON)? 1 : 0);

		strKey.sprintf("Description%d", i+1);
		ini.SetValue("FunctionPoints", strKey, m_lstFP[i].strDesc);
	}

	//save a list of function point sets
	nCount = m_lstFpSets.size();
	ini.SetValue("FunctionPointSets", "Count", nCount);

	for(int i=0; i<nCount; i++)
	{
		//save a single FP info
		QString strKey;

		strKey.sprintf("Name%d", i+1);
		ini.SetValue("FunctionPointSets", strKey, m_lstFpSets[i].strName);

		strKey.sprintf("Value%d", i+1);
		ini.SetValue("FunctionPointSets", strKey, m_lstFpSets[i].nValue);

		strKey.sprintf("Type%d", i+1);
		ini.SetValue("FunctionPointSets", strKey, m_lstFpSets[i].nType);

		strKey.sprintf("Level%d", i+1);
		ini.SetValue("FunctionPointSets", strKey, m_lstFpSets[i].nLevel);

		strKey.sprintf("Code%d", i+1);
		ini.SetValue("FunctionPointSets", strKey, m_lstFpSets[i].nCode);

		strKey.sprintf("Status%d", i+1);
		ini.SetValue("FunctionPointSets", strKey, m_lstFpSets[i].nStatus);

		strKey.sprintf("Active%d", i+1);
		ini.SetValue("FunctionPointSets", strKey, m_lstFpSets[i].bActive);

		strKey.sprintf("Description%d", i+1);
		ini.SetValue("FunctionPointSets", strKey, m_lstFpSets[i].strDesc);
	}
	
	//save a list of modules
	nCount = m_lstModSets.size();
	ini.SetValue("Modules", "Count", nCount);

	for(int i=0; i<nCount; i++)
	{
		//save a single module info
		QString strKey;

		strKey.sprintf("Name%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].strName);

		strKey.sprintf("Code%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].strCode);	

		strKey.sprintf("Value%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].nValue);

		strKey.sprintf("Type%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].nType);

		strKey.sprintf("NumCode%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].nCode);

		strKey.sprintf("Active%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].bActive);

		strKey.sprintf("Status%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].nStatus);

		strKey.sprintf("Level%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].nLevel);

		strKey.sprintf("Description%d", i+1);
		ini.SetValue("Modules", strKey, m_lstModSets[i].strDesc);
	}

	bool bSuccess = ini.Save();
	if(bSuccess){
		m_bModified = false;
		m_strFile = szFile;
	}
	
	return bSuccess;
}

bool FPDF::AddModule(const QString strCode, const QString strName, const QString strDesc, int nCode, int nStatus, int nLevel, int nType, int nValue, bool bActive)
{
	FP_ITEM item;
	item.strCode = strCode;
	item.strName = strName;
	item.nCode	 = nCode;
	item.nType	 = nType;
	item.bActive = bActive;
	item.nLevel  = nLevel;
	item.nValue	 = nValue;
	item.nStatus = nStatus;
	item.strDesc = strDesc;

	//add module into the list
	m_lstModSets.append(item);
	m_bModified = true;	// document modified
	return true;
}

bool FPDF::RemoveModule(int nIdx)
{
	//remove module definition
	m_lstModSets.removeAt(nIdx);
	m_bModified = true;	// document modified

	//TOFIX remove all children!!!
	return true;
}

bool FPDF::AddFunctionPoint(const QString strName, const QString strDesc, int nCode, bool bActive, int nValue, int nStatus)
{
	FP_ITEM item;
	item.strName  = strName;
	item.nCode	  = nCode;
	item.bActive  = bActive;
	item.nType	  = ITEM_TYPE_FP;
	item.nValue	  = nValue;
	item.nStatus  = (nStatus > 0)? ITEM_STATUS_ON : ITEM_STATUS_OFF;
	item.nLevel	  = 0;
	item.strDesc  = strDesc;

	if(m_lstFP.indexOf(item) < 0) //check for duplicate name
	{
		m_lstFP.append(item);
		m_bModified = true;	// document modified
		return true;
	}
	return false;
}

bool FPDF::AddFunctionPointSet(const QString strName, const QString strDesc, int nCode, int nStatus, int nLevel, int nType, int nValue, bool bActive)
{
	FP_ITEM item;
	item.strName	= strName;
	item.nCode		= nCode;
	item.bActive	= bActive;
	item.nType		= nType;
	item.nValue		= nValue;
	item.nLevel		= nLevel;
	item.nStatus    = nStatus;
	item.strDesc	= strDesc;

	m_lstFpSets.append(item);
	m_bModified = true;	// document modified
	return true;
}

bool FPDF::CopyModule(int nIdx, const QString strCode, const QString strName)
{
	int nSize = m_lstModSets.size();	// to know where will new module go
	if(m_lstModSets.CopyNode(nIdx, -1))	// copy under root node
	{
		m_lstModSets[nSize].strCode = strCode;
		m_lstModSets[nSize].strName = strName;
		return true;
	}
	return false;
}

#ifdef _DEBUG
void FPDF::AssertValid()
{
	//check for duplicate FP codes in a list!
	int nCnt = m_lstFP.size();
	for(int i=0; i<nCnt-1; i++){
		for(int j=i+1; j<nCnt; j++){
			if(m_lstFP[i].nCode == m_lstFP[j].nCode){
				qDebug() << "Two different FP items having the same code (" << m_lstFP[i].strName << "," << m_lstFP[j].strName << "), code=" << m_lstFP[j].nCode << "!";
				Q_ASSERT(false);
			}
		}
	}

	//check that FP in the FPS tree must always be a leaf node (no children)
	nCnt = m_lstFpSets.size();
	for(int i=0; i<nCnt-1; i++){
		if(ITEM_TYPE_FP == m_lstFpSets[i].nType){
			Q_ASSERT(m_lstFpSets[i].nLevel >= m_lstFpSets[i+1].nLevel);
		}
	}

	//find FP by name in other trees and make sure the code is the same
	nCnt = m_lstFP.size();
	int nFPSCnt = m_lstFpSets.size();
	int nMODCnt = m_lstModSets.size();
	for(int i=0; i<nCnt; i++){
		QString strName = m_lstFP[i].strName;
		int nCode = m_lstFP[i].nCode;

		//check all other trees
		for(int j=0; j<nFPSCnt; j++){
			if(strName == m_lstFpSets[j].strName){
				if(nCode != m_lstFpSets[j].nCode){
					qDebug() << "Item by name: " << strName << "has different code in different sets (code=" << nCode << "in set #" << i << ", code=" << m_lstFpSets[j].nCode << "in set #" << j << ")";
					//m_lstFpSets[j].nCode = nCode;
					Q_ASSERT(false);
				}
			}
		}
		for(int j=0; j<nMODCnt; j++){
			if(strName == m_lstModSets[j].strName){
				if(nCode != m_lstModSets[j].nCode){
					qDebug() << "Module by name: " << strName << "has different code in different sets (code=" << nCode << "in set #" << i << ", code=" << m_lstModSets[j].nCode << "in module set #" << j << ")";
					//m_lstModSets[j].nCode = nCode;
					Q_ASSERT(false);
				}
			}
		}
	}
}
#endif

#ifdef _DEBUG
void FPDF::Dump()
{
	int nCnt = m_lstFP.size();
	qDebug() << "FP list size:" << nCnt;

	for(int i=0; i<nCnt; i++){
		QString strName = m_lstFP[i].strName;
		int nCode = m_lstFP[i].nCode;
		qDebug() << "FP (" << i << "): " << "Code:" << nCode << "Name:" << strName;
	}
}
#endif
