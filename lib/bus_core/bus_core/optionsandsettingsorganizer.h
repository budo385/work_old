#ifndef OPTIONSANDSETTINGSORGANIZER_H
#define OPTIONSANDSETTINGSORGANIZER_H

#include "optionsandsettingsid.h"
#include "common/common/dbrecordset.h"

class OptionsAndSettingsOrganizer
{
public:
    OptionsAndSettingsOrganizer();
    ~OptionsAndSettingsOrganizer();

	DbRecordSet& GetOptionSets();
	DbRecordSet  GetApplicationOptionSets();
	DbRecordSet	 GetPersonalSettings();
	DbRecordSet  GetSinglePersonalSetting(int nSettingID);
	DbRecordSet  GetSettingsBySetID(int nSettingsSetID);
	DbRecordSet  GetOptions();
	DbRecordSet  GetSingleOption(int nOptionsID);

private:
    DbRecordSet m_recOptionsSet;
	DbRecordSet m_recOptions;
	DbRecordSet m_recSettings;

	void InitializeOptionsSets();
	void InitializeAppOptions();
	void InitializePersonSettings();
};

#endif // OPTIONSANDSETTINGSORGANIZER_H
