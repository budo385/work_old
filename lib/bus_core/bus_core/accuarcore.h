#ifndef ACCUARCORE_H
#define ACCUARCORE_H

#include "common/common/dbrecordset.h"

//User Access Right per record

class AccUARCore 
{
public:

	enum AccRightLevel //CUAR_RIGHT =  0=no right 1=Read Only 2=Read & Modify 3=Read, Modify & Delete & Modify and View  Access Rights. 
	{
		ACC_LVL_NO_RIGHT=0,
		ACC_LVL_READ=1,
		ACC_LVL_MODIFY=2,
		ACC_LVL_FULL=3
	};
	enum AccDisplayLevel
	{
		ACC_PRIVATE,
		ACC_OTHERS_CAN_READ,
		ACC_OTHERS_CAN_MODIFY,
		ACC_INDIVIDUAL
	};

	static void UARList2DisplayList(DbRecordSet *lstUAR=NULL, DbRecordSet *lstGAR=NULL);
	static void DisplayList2UAR(DbRecordSet *lstUAR=NULL, DbRecordSet *lstGAR=NULL);
	static void UpdateUARGroupDependentRights(DbRecordSet &lstGroupUsers,DbRecordSet &lstUAR, DbRecordSet &lstGAR, int nUARRow=-1);
	static void GenerateNewUARFromContacts(int nRecord, int nTableID,DbRecordSet &lstContacts, DbRecordSet &lstUAR, DbRecordSet &lstPersons);
	static void GenerateNewGARFromGroups(int nRecord, int nTableID,DbRecordSet &lstGroups, DbRecordSet &lstGAR);
	static void DefineDisplayUARList(DbRecordSet &lstUAR);
	static void DefineDisplayGARList(DbRecordSet &lstGAR);
	static void DefineGroupUserList(DbRecordSet &lstGroupUser);
	static void CopyGUAR(int nTableID,DbRecordSet lstOfRecordIDs, QString strRecordIdColName,const DbRecordSet &lstUAR_Source, const DbRecordSet &lstGAR_Source,DbRecordSet &lstUAR, DbRecordSet &lstGAR);
	static int	Record2DisplayLevel(int nLoggedPersonID,DbRecordSet &lstUAR, DbRecordSet &lstGAR);
	static void	DisplayLevel2Record(int nDisplayLevel,int nLoggedPersonID,int nRecordID,int nTableID,DbRecordSet &lstUAR, DbRecordSet &lstGAR);
	static bool	IsUserAllowedToModifyUAR(int nLoggedPersonID,DbRecordSet &lstUAR, DbRecordSet &lstGAR);
	static void	CreatePrivateUAR(int nLoggedPersonID,int nRecordID,int nTableID,DbRecordSet &lstUAR, DbRecordSet &lstGAR);





private:
	
};

#endif // ACCUARCORE_H
