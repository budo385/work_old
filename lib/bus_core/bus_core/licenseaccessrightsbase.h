#ifndef LICENSEACCESSRIGHTSBASE_H
#define LICENSEACCESSRIGHTSBASE_H



class LicenseAccessRightsBase
{
	public:
		virtual int GetMaxLicenseCount(QString& strModuleCode, QString& strMLIID)=0;
};

#endif	// LICENSEACCESSRIGHTSBASE_H