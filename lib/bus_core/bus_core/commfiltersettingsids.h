#ifndef COMMFILTERSETTINGSIDS_H
#define COMMFILTERSETTINGSIDS_H

//--------------------------------
// Fui stuff.
//--------------------------------
#define OPEN_ON_STARTUP													1  //Open on startup check box.
#define PUBLIC															2  //Is public check box.

//--------------------------------
// Grid.
//--------------------------------
//Voice call filter
#define VIEW_SELECTION_GRID_ACTIVE										3  //IS communication grid active.
#define SHOW_VOICE_CALLS_UNASSIGNED										4
#define SHOW_VOICE_CALLS_MISSED											5
#define SHOW_VOICE_CALLS_SCHEDULED										6
#define SHOW_VOICE_CALLS_DUE											7
#define SHOW_VOICE_CALLS_OVERDUE										8
#define SHOW_VOICE_CALLS_LAST											9
#define LAST_VOICE_CALLS_NUMBER											10
//Emails filter
#define SHOW_EMAILS_UNREAD												11
#define SHOW_EMAILS_UNASSIGNED											12
#define SHOW_EMAILS_SCHEDULED											13
#define SHOW_EMAILS_DUE													14
#define SHOW_EMAILS_OVERDUE												15
#define SHOW_EMAILS_TEMPLATES											16
#define SHOW_EMAILS_LAST												17
#define LAST_EMAILS_NUMBER												18
//Documents filter
#define DOCUMENTS_UNASSIGNED											19
#define DOCUMENTS_SCHEDULED												20
#define DOCUMENTS_DUE													21
#define DOCUMENTS_OVERDUE												22
#define DOCUMENTS_TEMPLATES												23
#define DOCUMENTS_LAST													24
#define LAST_DOCUMENTS_NUMBER											25

//--------------------------------
// Date filter group box.
//--------------------------------
#define FILTER_BY_DATE_ACTIVE											26
#define FROM_DATE														28
#define TO_DATE															29
#define DATE_RANGE_SELECTOR												30

//--------------------------------
// Entity type group box.
//--------------------------------
#define FILTER_BY_ENTITY_TYPE_ACTIVE									36
#define SHOW_DOCUMENTS													37
#define SHOW_EMAILS														38
#define SHOW_VOICE_CALLS												39

//--------------------------------
// Document application type group box.
//--------------------------------
#define FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE							40
#define APPLICATION_TYPES												41  //Application types to filter.

//--------------------------------
// Document application type group box.
//--------------------------------
#define FILTER_SEARCH_TEXT_ACTIVE										42
#define SEARCH_IN_NAME													43  //Application types to filter.
#define SEARCH_IN_DESCRIPTION											44  //Application types to filter.
#define SEARCH_TEXT														45  //Application types to filter.

//--------------------------------
// View sort code.
//--------------------------------
#define VIEW_SORT_CODE													46  //Application types to filter.

//--------------------------------
// CE entity type.
//--------------------------------
#define FILTER_BY_CE_ENTITY_TYPE_ACTIVE									47	//CE types (Document, voice call, emails).
#define SELECTED_COMBO_CE_ENTITY_TYPE_FILTER							48  //Selected combo box.
#define CE_ENTITY_TYPE_FILTER_DOCUMENTS									49  //Filter recordset for documents.
#define CE_ENTITY_TYPE_FILTER_VOICE_CALLS								50  //Filter recordset for voice calls.
#define CE_ENTITY_TYPE_FILTER_EMAILS									51  //Filter recordset for emails.

//--------------------------------
// Task type filters.
//--------------------------------
#define FILTER_BY_TASK_TYPE_ACTIVE										52	//Task type filter active.
#define TASK_PRIORITY_FROM												53	//Task type filter active.
#define TASK_PRIORITY_TO												54	//Task type filter active.
#define TASK_TYPE_FILTER												55	//Task type filter active.

#endif // COMMFILTERSETTINGSIDS_H
