#include "messagedispatcher.h"
#include <QWriteLocker>
#include <QReadLocker>


MessageDispatcher::MessageDispatcher(QObject *parent)
: ThreadSpawnedObject(parent)
{
	m_lstMessages.addColumn(QVariant::Double,"MSG_ID");
	m_lstMessages.addColumn(QVariant::DateTime,"MSG_CREATED");
	m_lstMessages.addColumn(QVariant::Int,"MSG_TYPE");
	m_lstMessages.addColumn(QVariant::Int,"MSG_CODE");
	m_lstMessages.addColumn(QVariant::String,"MSG_TEXT");
	m_lstMessages.addColumn(QVariant::DateTime,"MSG_NOTIFY_DATE");
	m_lstMessages.addColumn(QVariant::DateTime,"MSG_EXPIRE_DATE");
	//m_lstMessages.addColumn(QVariant::String,"MSG_TARGET_USERS"); //semicolon delimited stream of user id's---socket ids
	m_lstMessages.addColumn(QVariant::Int,"MSG_TIMER_ID");
	m_lstMessages.addColumn(QVariant::String,"MSG_TARGET_PERSON_IDS");
	//m_lstMessages.addColumn(QVariant::Int,"MSG_SENDER_PERSON_ID"); //msg will not be resent back to sender
	m_lstMessages.addColumn(QVariant::Int,"MSG_SENDER_SOCKET_ID"); //msg will not be resent back to sender
	
	
	m_nMsgID=0;
	connect(this,SIGNAL(NewMessageIncoming(int)),this,SLOT(NewMessage(int)),Qt::QueuedConnection);
	connect(this, SIGNAL(StartMsgTimer(int)), this, SLOT(OnStartTimer(int)));
}

MessageDispatcher::~MessageDispatcher()
{

}


//Sending of the message:
//Msg originator can be: logged person/user or application process: if person, msg is never sent back to him
//Msg can be sent to all logged persons or to specific ones (specified by person_id)
//Msg can be sent at once or at excat moment: specified by datNotifyDateTime
//Msg will expire at datExpireDateTime and will be deleted from memory

//nType - user defined
//nMessageCode - user defined -> Status.code
//strTextArguments - user defined -> Status.text
//lstTargetPersonIDs: if NULL or empty msg goes to all currently logged and is kept in memory for all that will log in until expire (if this is NULL then expire date should be set)
//nMessageID if set then it will overwrite old message if found
//datExpireDateTime - msg expire: and is deleted from memory, if NULL it will never expire
//datNotifyDateTime - exact time of msg delivery: msg will be delivered to logged users specified by lstTargetPersonIDs and will be kept in memory until datExpireDateTime
//nLoggedPersonID - person whos sends msg: msg will not be delivered to this one at all!!!!!


int MessageDispatcher::SendMessageX(int nType,int nMessageCode, QString strTextArguments,QStringList *lstTargetPersonIDs,int nLoggedPersonSocket, int nMessageID,QDateTime datExpireDateTime,QDateTime datNotifyDateTime)
{
	QWriteLocker lock(&m_mutex);

	//bool bDontSendMessage=false;
	int nLast=-1;
	
	if (nMessageID!=-1)
	{
		nLast=m_lstMessages.find("MSG_ID",nMessageID,true);
	}

	if (nLast==-1)
	{
		m_nMsgID++;
		m_lstMessages.addRow();
		nLast=m_lstMessages.getRowCount()-1;
		m_lstMessages.setData(nLast,"MSG_ID",m_nMsgID);
	}

	m_lstMessages.setData(nLast,"MSG_CREATED",QDateTime::currentDateTime());
	m_lstMessages.setData(nLast,"MSG_TYPE",nType);
	m_lstMessages.setData(nLast,"MSG_CODE",nMessageCode);
	m_lstMessages.setData(nLast,"MSG_TEXT",strTextArguments);
	m_lstMessages.setData(nLast,"MSG_NOTIFY_DATE",datNotifyDateTime);
	m_lstMessages.setData(nLast,"MSG_EXPIRE_DATE",datExpireDateTime);
	//m_lstMessages.setData(nLast,"MSG_SENDER_PERSON_ID",nLoggedPersonID);
	m_lstMessages.setData(nLast,"MSG_SENDER_SOCKET_ID",nLoggedPersonSocket);

	if (lstTargetPersonIDs)
		if (lstTargetPersonIDs->size()>0)
		{
			m_lstMessages.setData(nLast,"MSG_TARGET_PERSON_IDS",lstTargetPersonIDs->join(";"));
			//bDontSendMessage=true; //if ID person declared stop send message, only if socket is alive then send
		}

	//if (!bDontSendMessage)
	//{
		//signal to thread that message is here-> send it to users!
	if (datNotifyDateTime.isValid())
	{
		//if less and not started then start
		if (datNotifyDateTime>QDateTime::currentDateTime() && m_lstMessages.getDataRef(nLast,"MSG_TIMER_ID").toInt()==0)
		{
			emit StartMsgTimer(m_lstMessages.getDataRef(nLast,"MSG_ID").toInt()); //can not start timer from another thread
			return m_lstMessages.getDataRef(nLast,"MSG_ID").toInt();
		}

		return m_lstMessages.getDataRef(nLast,"MSG_ID").toInt();
	}

		//send message right away if not set other
	emit NewMessageIncoming(m_lstMessages.getDataRef(nLast,"MSG_ID").toInt());
	//}

	return m_lstMessages.getDataRef(nLast,"MSG_ID").toInt();
}

//on logout clears message imprint:
void MessageDispatcher::ClearUserSocketFromMessages(int nUserSocket)
{
	if (nUserSocket<=0)
		return;
	QWriteLocker lock(&m_mutex);

	int nRow=m_lstMessages.find("MSG_SENDER_SOCKET_ID",nUserSocket,true);
	if (nRow>=0)
	{
		m_lstMessages.setData(nRow,"MSG_SENDER_SOCKET_ID",-1);
	}

}
//if -1 then all
void MessageDispatcher::GetMessages(int nPersonID, DbRecordSet &lstMessages)
{
	ClearExpiredMessages();

	QWriteLocker lock(&m_mutex);

	int nSize=m_lstMessages.getRowCount();

	if (nPersonID==-1)
	{
		lstMessages=m_lstMessages;
		return;
	}

	lstMessages.copyDefinition(m_lstMessages);
	QString strUserID=QVariant(nPersonID).toString();

	
	for(int i=0;i<nSize;++i)
	{
		QStringList lstUsers=m_lstMessages.getDataRef(i,"MSG_TARGET_PERSON_IDS").toString().split(";");
		if (m_lstMessages.getDataRef(i,"MSG_TARGET_PERSON_IDS").toString().isEmpty()) //if no user specified then take msg and leave list intact
		{
			lstMessages.addRow();
			DbRecordSet row = m_lstMessages.getRow(i);
			lstMessages.assignRow(lstMessages.getRowCount()-1,row,true);
		}
		else if (lstUsers.indexOf(strUserID)>=0) //if user is identified then add, and remove from list, if list is empty then delete msg
		{
			lstMessages.addRow();
			DbRecordSet row = m_lstMessages.getRow(i);
			lstMessages.assignRow(lstMessages.getRowCount()-1,row,true);

			//if one user specified, mark for clear:
			if (lstUsers.indexOf(strUserID)>=0) 
			{
				int nRow=lstUsers.indexOf(strUserID);
				lstUsers. removeAt(nRow);
			}
			if (lstUsers.size()==0)
			{
				m_lstMessages.setData(i,"MSG_EXPIRE_DATE",QDateTime::currentDateTime());
			}
			m_lstMessages.setData(i,"MSG_TARGET_PERSON_IDS",lstUsers.join(";"));
		}
	}
	
}


void MessageDispatcher::ClearExpiredMessages()
{
	QWriteLocker lock(&m_mutex);
	int nSize=m_lstMessages.getRowCount();
	for(int i=nSize-1;i>=0;--i)
	{
		QDateTime dateExpired=m_lstMessages.getDataRef(i,"MSG_EXPIRE_DATE").toDateTime();
		if (dateExpired.isValid() && dateExpired <=QDateTime::currentDateTime())
		{
			m_lstMessages.deleteRow(i);
		}
	}

}




void MessageDispatcher::timerEvent ( QTimerEvent * event )
{
	//find message:
	int nTimerID=event->timerId();
	QWriteLocker lock(&m_mutex);
	int nSize=m_lstMessages.getRowCount();
	for(int i=nSize-1;i>=0;--i)
	{
		if (m_lstMessages.getDataRef(i,"MSG_TIMER_ID").toInt()==nTimerID)
		{
			m_lstMessages.setData(i,"MSG_TIMER_ID",0);
			killTimer(nTimerID);
			NewMessageIncoming(m_lstMessages.getDataRef(i,"MSG_ID").toInt());
		}
	}

}

void MessageDispatcher::OnStartTimer(int nMsgID)
{
	//find message:
	QWriteLocker lock(&m_mutex);
	int nSize=m_lstMessages.getRowCount();
	for(int i=nSize-1;i>=0;--i)
	{
		if (m_lstMessages.getDataRef(i,"MSG_ID").toInt()==nMsgID)
		{
			//set timer to fire message on specified time:
			QDateTime current=QDateTime::currentDateTime();
			int nSec=current.secsTo(m_lstMessages.getDataRef(i,"MSG_NOTIFY_DATE").toDateTime());
			int nTimerID=startTimer(nSec*1000);
			m_lstMessages.setData(i,"MSG_TIMER_ID",nTimerID);
			return;		
		}
	}
}