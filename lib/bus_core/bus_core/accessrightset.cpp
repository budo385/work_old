#include "accessrightset.h"

AccessRightSet::AccessRightSet()
{
	
}

AccessRightSet::AccessRightSet(DbRecordSet &AccessRightSetRecordSet)
{
	m_recAccessRightSet = AccessRightSetRecordSet;
}

AccessRightSet::AccessRightSet(AccessRightSet &AccRightSet)
{
	operator = (AccRightSet);
}

void AccessRightSet::operator =(const AccessRightSet &that)
{
	m_hshVersionAddAccessRights		= that.m_hshVersionAddAccessRights;
	m_hshVersionRemoveAccessRights	= that.m_hshVersionRemoveAccessRights;
	m_recAccessRightSet				= that.m_recAccessRightSet;
}

AccessRightSet::~AccessRightSet()
{
	
}

void AccessRightSet::AddAccessRight(int AccessRightID)
{
	m_hshVersionAddAccessRights << AccessRightID;
}

void AccessRightSet::RemoveAccessRight(int AccessRightID)
{
	m_hshVersionRemoveAccessRights << AccessRightID;
}

void AccessRightSet::GetARSRecordSet(DbRecordSet &ARSRecordSet)
{
	ARSRecordSet = m_recAccessRightSet;
}

void AccessRightSet::GetARSAddList(QVector<int> &ARSAddList)
{
	ARSAddList = m_hshVersionAddAccessRights;
}

void AccessRightSet::GetARSRemoveList(QVector<int> &ARSRemoveList)
{
	ARSRemoveList = m_hshVersionRemoveAccessRights;
}

