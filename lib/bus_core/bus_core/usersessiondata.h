#ifndef USERSESSIONDATA_H
#define USERSESSIONDATA_H

#include "common/common/dbrecordset.h"


/*!
    \class SessionData
    \brief User Session data container used inside UserSessionManager objects
    \ingroup Bus_Server

*/
class UserSessionData
{
public:
	//default constructor sets empty fields+activity date= current datetime
	UserSessionData(int nThread=0,int nStatus=0,QString strIP="",int SocketID=0):
		strSession(""),	
		strClientIP(strIP),
		nThreadID(nThread),
		nSocketID(SocketID),
		bStored(false),
		nStoredSessionID(0),
		nRequestStatus(nStatus),
		nRequestCounter(0),
		nUserID(0),
		bThreadActive(false),
		bIsSystemThread(false),
		bIsWebSession(false),
		datLastActivity(QDateTime::currentDateTime()),
		datLastSessionCheck(QDateTime::currentDateTime()),
		nClientTimeZoneOffsetMinutes(0){};
	~UserSessionData(){};


	QString		strSession;				///<session
	QDateTime	datLastActivity;		///<datetime of last client request
	QDateTime	datLastSessionCheck;	///<datetime of last DB session check
	int			nThreadID;				///<unqiue thread id 
	int			nSocketID;				///<unique socket ID when http
	bool		bStored;				///<true means that session record is under our control and stored in DB
	int			nRequestStatus;			///<status of client session: NORMAL, REAUTH, EXPIRED,...
	int			nRequestCounter;		///<how many times client has unsuccessfuly try to login
	QString		strClientIP;			///<client IP (only in remote ops)
	int			nStoredSessionID;		///<Session ID (number) stored in Database
	int			nUserID;	
	int			nPersonID;
	QString		strModuleCode; 
	QString		strMLIID;
	DbRecordSet rowUserData;			//whole user data information (last name, first, id,..)
	bool		bThreadActive;
	bool		bIsSystemThread;		//internal thread run by server only -> prevents garbage collector to clean it if no socket associated with it
	QString		strParentSession;		//for multi-client-connections: if empty then parent, else child: do not count license: if parent not found...return error
	bool		bIsWebSession;
	int			nClientTimeZoneOffsetMinutes;
};


typedef QHash<int, UserSessionData> UserSessionList;	///session list uniquely defined by key=ThreadID
typedef QHashIterator<int,UserSessionData> UserSessionListIterator;


#endif //USERSESSIONDATA_H
