#ifndef SERVERINIFILE_H
#define SERVERINIFILE_H

#include "common/common/inifile.h"
#include <QFileInfo>



/*!
	\class ServerIniFile
	\brief INI file reader for app. server
	\ingroup AppServer

	Application server INI file reader


*/
class ServerIniFile
{
public:
	ServerIniFile();
	~ServerIniFile();

	
	
	void Clear();
	bool Load(QString strFile, bool bCreateIfNotExists=true);
	bool Save(QString strFile="");

public:

	//data stored in the INI file
	QString m_strAppServerIPAddress;	// IP address
	int		m_nAppServerPort;			// port number
	int		m_nMaxConnections;			// maximum connections that app. server will accept
	int		m_nMaxDbConnections;		// maximum Database connections (if 0) then unlimited
	int		m_nMinDbConnections;		// min Database connections that are always open
	int		m_nAppServerRole;			// 1-normal XML-RPC, 2-HTML only requests, 3 - Job server
	QString m_strDBConnectionName;		// name of current database connection from DBAcesss.ini file
	int		m_nIsMasterServer;			// runs Global GarbageCollector
	QString	m_strDescriptor;			// app name
	int		m_nDiskUserStorageQuota;	// disk quota in GB
	int		m_nAutoStartService;		//
	QString m_strWebDocumentsPath;		// default app/webservices
	int		m_nLastErrorCode;	
	QString m_strLastErrorText;
	QString m_strLocalAppServerIP;
	QString m_strTempUserStorageDirectory;	//temp directory

	//EMAIL
	int		m_nEnableEmailSync;
	//SSL
	int		m_nSSLMode;					// accept only SSL(https) connections
	QString m_strSSLCertificateDir;		// path to the SSL certificates files (server.cert & server.pkey)
	int		m_nSSLNoPrivateKey;			// 0 - use hardcoded priv. key for cert file, else use blank pass

	//LBO&DFO
	int m_nLoadBalanceActive;
	int m_nAcceptDFConnections;

	//CUSTOM INFO
	QString m_strName;					// user defined name of app. server

	//SECURITY (KICK BAN)
	int m_bUseSecurityOptions;			// if 0 then all below has no meaning
	int m_nIntrusionTries;				// how many suspicios calla is allowed for one IP address before ban
	int m_nKickPeriodAfterIntrusion;	// after m_nIntrusionTries, ban IP to this time (seconds) or if 0 then ban for life
	QString m_strIPBlockListNamePreset;	// internal use: name of list with blocked IP address in DB, user should not chaneg this (set by admin)
	QString m_strIPBlockListNameDynamic;// internal use: name of list with blocked IP address in DB, user should not chaneg this (dynamically built)

	//LOGGER
	int m_nLogLevel;					
	int m_nLogMaxSize;					
	int m_nLogBufferSize;				
	int m_nLogTargetFile;				
	int m_nLogTargetConsole;				

	//BACKUP:
	int m_nBackupFreq;		
	QString m_strBackupPath;		
	QString m_datBackupLastDate;		
	QString m_strRestoreOnNextStart;		
	QString m_strBackupTime;
	int m_nBackupDay;
	int m_nDeleteOldBackupsDay;

	//UPDATE
	QString m_strUpdateTime;		 //HH:MM
	QString m_strWarningUpdateTime;	 //minutes before m_strUpdateTime

	//REST
	//int m_nRestEnableDesc;
	int m_nRestEnable;

	//WWW
	int m_nWWWEnable;
	QString m_strWWWRootAddress;

	//debug
	QString m_strLoggedEmails;	//MB 2014.03.18

	//PUSH DATA:
	QString m_strApplePushServerURL;
	int		m_nApplePushServerPort;
	QString m_strAppleFeedBackServerURL;
	int		m_nAppleFeedBackServerPort;
	QString m_strAppleRootCACertificateFile;
	QString m_strAppleWWDRCACertificateFile;
	QString m_strAppleLocalCertificateFile;
	QString m_strApplePrivateKeyFile;

	QString m_strGooglePushServerURL;
	QString m_strGoogleAuthenticationID;


	//SELFTEST
	QString m_strStartEmailAdd;
	QString m_strStartPassWordAdd;
	int m_nStartCounterAdd;
	int m_nCountUserAdd;
	QString m_strLastEmailAdded;
	QString m_strEmailServerAdd;
	int m_nEmailServerPortAdd;
	int m_nEmailServerIsImapAdd;
	int m_nEmailServerConnectionTypeAdd;
	int m_nTestSendingEmailsPeriodMinutes;
	QString m_strNextTestTime; //yyyyMMddHHmm
	int		m_nEmailServerSMTPPort_Test; //for sending SMTP mails
	QString m_strEmailServer_Test; //for sending SMTP mails

	
protected:
	IniFile m_objIni;
	bool CheckValues();
	bool SaveDefaults(QString strFile);
	QString m_strLastPathUsed;

};

#endif //SERVERINIFILE_H