#include "emailhelpercore.h"
#include "db_core/db_core/dbsqltableview.h"
#include <QStringList>

QString EmailHelperCore::GetEmailFromName(QString strName)
{
	//If string not contain "<" we will assume that is only email string (no name).
	if (!strName.contains("<"))
		return strName;

	strName.truncate(strName.indexOf(">"));
	strName.remove(0, strName.indexOf("<")+1);
	return strName;
}

QString EmailHelperCore::GetNameFromEmail(QString strEmail)
{
	QString str;
	if (!strEmail.contains("<"))
		return str;

	strEmail.truncate(strEmail.indexOf("<"));
	return strEmail;
}

QString EmailHelperCore::GetFullEmailAddress(QString strName, QString strEmail, bool bAddSeparator)
{
	if (!strName.isEmpty())
		strEmail = strName+" <" + strEmail + ">";

	if (bAddSeparator)
		strEmail+=";";
	return strEmail;
}

void EmailHelperCore::LoadRecipients(DbRecordSet &lstContactEmails,DbRecordSet &recRecipients, QString strRecipients)
{
	recRecipients.destroy();
	recRecipients.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));

	int nEmailIdx=lstContactEmails.getColumnIdx("BCME_ADDRESS");

	recRecipients.clear();

	QStringList lstCategories=strRecipients.split(";",QString::SkipEmptyParts);
	int nSize=lstCategories.size();
	for(int k=0;k<nSize;++k)
	{
		QString strEmailAll=lstCategories.at(k).trimmed();
		int nContactID=0,nEmailID=0;
		QString strName,strEmail;

		strEmail=GetEmailFromName(strEmailAll);
		strName=GetNameFromEmail(strEmailAll);

		if (strEmail.isEmpty() && strName.isEmpty())
			continue;

		int nRow=lstContactEmails.find(nEmailIdx,strEmail,true);
		if (nRow!=-1)
		{
			nContactID=lstContactEmails.getDataRef(nRow,"BCME_CONTACT_ID").toInt();
			nEmailID=lstContactEmails.getDataRef(nRow,"BCME_ID").toInt();
		}

		recRecipients.addRow();
		int nLastAdded=recRecipients.getRowCount()-1;
		recRecipients.setData(nLastAdded,"CONTACT_EMAIL",strEmail);
		recRecipients.setData(nLastAdded,"CONTACT_NAME",strName);
		recRecipients.setData(nLastAdded,"CONTACT_ID",nContactID);
		recRecipients.setData(nLastAdded,"EMAIL_ID",nEmailID);

	}
}

//accepts rowEmail in BUS_EMAIL format and returns lstEmails with 1 col = EMAIL
void EmailHelperCore::ExtractMailsFromEmailRow(DbRecordSet &rowEmail,DbRecordSet &lstEmails)
{
	lstEmails.destroy();
	lstEmails.addColumn(QVariant::String,"EMAIL");

	QString strFrom=rowEmail.getDataRef(0,"BEM_FROM").toString();
	QString strTo=rowEmail.getDataRef(0,"BEM_TO").toString();
	QString strBCC=rowEmail.getDataRef(0,"BEM_BCC").toString();
	QString strCC=rowEmail.getDataRef(0,"BEM_CC").toString();

	QStringList lstCategories=strFrom.split(";",QString::SkipEmptyParts);
	int nSize=lstCategories.size();
	for(int k=0;k<nSize;++k)
	{
		QString strEmailAll=lstCategories.at(k).trimmed();
		QString strEmail=GetEmailFromName(strEmailAll);
		lstEmails.addRow();
		lstEmails.setData(lstEmails.getRowCount()-1,0,strEmail);
	}
	lstCategories=strTo.split(";",QString::SkipEmptyParts);
	nSize=lstCategories.size();
	for(int k=0;k<nSize;++k)
	{
		QString strEmailAll=lstCategories.at(k).trimmed();
		QString strEmail=GetEmailFromName(strEmailAll);
		lstEmails.addRow();
		lstEmails.setData(lstEmails.getRowCount()-1,0,strEmail);
	}
	lstCategories=strBCC.split(";",QString::SkipEmptyParts);
	nSize=lstCategories.size();
	for(int k=0;k<nSize;++k)
	{
		QString strEmailAll=lstCategories.at(k).trimmed();
		QString strEmail=GetEmailFromName(strEmailAll);
		lstEmails.addRow();
		lstEmails.setData(lstEmails.getRowCount()-1,0,strEmail);
	}
	lstCategories=strCC.split(";",QString::SkipEmptyParts);
	nSize=lstCategories.size();
	for(int k=0;k<nSize;++k)
	{
		QString strEmailAll=lstCategories.at(k).trimmed();
		QString strEmail=GetEmailFromName(strEmailAll);
		lstEmails.addRow();
		lstEmails.setData(lstEmails.getRowCount()-1,0,strEmail);
	}


}

bool EmailHelperCore::ExtractNamesFromEmail(QString strEmail,QString &strFirstName,QString &strLastName)
{
	int nDotIdx=strEmail.indexOf(".");
	int nMonkeyIdx=strEmail.indexOf("@");
	if (nDotIdx<nMonkeyIdx && nDotIdx>0 && nMonkeyIdx>0)
	{
		strFirstName=strEmail.left(nDotIdx);
		if(strFirstName.length() > 0)
			strFirstName[0] = strFirstName[0].toUpper();	//capitalize
		strLastName=strEmail.mid(nDotIdx+1,nMonkeyIdx-nDotIdx-1);
		if(strLastName.length() > 0)
			strLastName[0] = strLastName[0].toUpper();	//capitalize

		return true;
	}
	else
		return false;
}
