#include "mainentityfilter.h"


//column names
MainEntityFilter::MainEntityFilter()
{
	/*
	m_nFilterCodePosition=-1;
	m_nFilterNamePosition=-1;
	m_nFilterIDPosition=-1;

	*/
	//define filters:
	m_lstFilters.addColumn(QVariant::Int,"FILTER_TYPE");
	m_lstFilters.addColumn(QVariant::String,"FILTER_DATA");
	m_lstFilters.addColumn(QVariant::String,"COLUMN_NAME");
	m_lstFilters.addColumn(QVariant::Int,"COLUMN_VALUE");
}

MainEntityFilter::MainEntityFilter(DbRecordSet lstFilterData)
{
	m_lstFilters=lstFilterData;
}

MainEntityFilter::~MainEntityFilter()
{

}


/*
void MainEntityFilter::SetFilterInfo(QString strIDIdx,QString strCodeIdx,QString strNameIdx)
{

	m_strCodeIdx=strCodeIdx;
	m_strNameIdx=strNameIdx;
	m_strIDIdx=strIDIdx;
}
*/


MainEntityFilter::MainEntityFilter(const MainEntityFilter &that)
{
	operator = (that);
}
MainEntityFilter &MainEntityFilter::operator =(const MainEntityFilter &that)
{
	if(this != &that)
	{
		m_lstFilters=that.m_lstFilters;
		//m_lstFilters.Dump();
		/*
		m_strCodeIdx=that.m_strCodeIdx;
		m_strNameIdx=that.m_strNameIdx;
		m_strIDIdx=that.m_strIDIdx;
		m_nFilterCodePosition=that.m_nFilterCodePosition;
		m_nFilterIDPosition=that.m_nFilterIDPosition;
		m_nFilterNamePosition=that.m_nFilterNamePosition;
		*/
	}
	return *this;
}


//only compares list of filters:
bool MainEntityFilter::operator ==(const MainEntityFilter &that)
{
	int nSize=m_lstFilters.getRowCount();
	if(nSize!=that.m_lstFilters.getRowCount()) return false;
	for(int i=0;i<nSize;++i)
	{
		if(m_lstFilters.getDataRef(i,0).toInt()!=that.m_lstFilters.getDataRef(i,0).toInt()) return false;
		if(m_lstFilters.getDataRef(i,1).toString()!=that.m_lstFilters.getDataRef(i,1).toString()) return false;
	}
	return true;
}

//returns conditions witohut WHERE!!!
QString MainEntityFilter::GetWhereStatement(DbRecordSet& FilterData, bool bAddWhereIfNotExists)
{
	if (FilterData.getColumnCount()==0)
		return "";

	int nSize=FilterData.getRowCount();
	QString strSqlWhere;

	
	for(int i=0;i<nSize;++i)
	{
		if(FilterData.getDataRef(i,0).toInt()==FILTER_SQL_WHERE)
		{
			if(i!=0)strSqlWhere+" AND ";
			strSqlWhere+=FilterData.getDataRef(i,1).toString();
			
		}
	}

	//if SQL does not have where add to start:
	if (bAddWhereIfNotExists && strSqlWhere.indexOf("WHERE",0,Qt::CaseInsensitive)==-1)
		strSqlWhere=strSqlWhere.prepend(" WHERE ");

	return	strSqlWhere;

}



/*!
	Clear filter if nPosition=-1, else removes entry from given position

	\param nPosition - -10 all, else at given position
*/
void MainEntityFilter::ClearFilter(int nPosition)
{
	if(nPosition==-10)
		m_lstFilters.clear();
	else
	{
		if(nPosition>0 && nPosition<m_lstFilters.getRowCount())
			m_lstFilters.deleteRow(nPosition);
	}
}


/*!
	Adds new filter or replaces old if nPosition is set

	\param nFilterType	- filter type
	\param strFilter	- filter text (can be where statement or ID,,..)
	\param nPosition	- -1 add new
	\return				- if filter is added returns filter position in list

*/
int MainEntityFilter::SetFilter(int nFilterType,QString strFilter, int nPosition)
{
	//add new row
	if(nPosition==-1)
	{
		m_lstFilters.addRow();
		nPosition=m_lstFilters.getRowCount()-1;
	}

	//store filter:
	m_lstFilters.setData(nPosition,0,nFilterType);
	m_lstFilters.setData(nPosition,1,strFilter);
	return nPosition;
}

/*!
	Same as above but stores filter in format COL=value.
	Only for SQL type.
	This is for easier filter ops. coz this is most common operation.

	\param strColumnName	- filter text (can be where statement or ID,,..)
	\param nColumnVal		- column name must match this value (int)
	\param nPosition		- -1 add new
	\return					- if filter is added returns filter position in list

*/
int MainEntityFilter::SetFilter(QString strColumnName, int nColumnVal,int nPosition)
{
	//add new row
	if(nPosition==-1)
	{
		m_lstFilters.addRow();
		nPosition=m_lstFilters.getRowCount()-1;
	}

	//store filter:
	QString strFilter=strColumnName+" = "+QVariant(nColumnVal).toString();
	m_lstFilters.setData(nPosition,0,FILTER_SQL_WHERE);
	m_lstFilters.setData(nPosition,1,strFilter);
	m_lstFilters.setData(nPosition,2,strColumnName);
	m_lstFilters.setData(nPosition,3,nColumnVal);

	return nPosition;
}

//from SQL filter types (COL_NAME(string)=value(int)), finds columns and check values inside record
//if all match returns true, else false
//inverse logic: returns always true, except when values are not matched
bool MainEntityFilter::DoesRecordMatchFilter(DbRecordSet& record)
{
	int nSize=m_lstFilters.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if(m_lstFilters.getDataRef(i,1).toInt()==FILTER_SQL_WHERE)
		{
				int nColIdx=record.getColumnIdx(m_lstFilters.getDataRef(i,2).toString());//get col idx inside record
				if(nColIdx!=-1)
				{
					if(record.getDataRef(0,nColIdx).toInt()!=m_lstFilters.getDataRef(i,3).toInt()) //compare value from filter & 
						return false;
				}
		}
	}

	return true;
}


/*
//most common filters:
void MainEntityFilter::ClearFilter_ID()
{
	if(m_nFilterIDPosition==-1) return;
	ClearFilter(m_nFilterIDPosition);
}
void MainEntityFilter::ClearFilter_Code()
{
	if(m_nFilterCodePosition==-1) return;
	ClearFilter(m_nFilterIDPosition);
}
void MainEntityFilter::ClearFilter_Name()
{
	if(m_nFilterNamePosition==-1) return;
	ClearFilter(m_nFilterIDPosition);
}




void MainEntityFilter::SetFilter_ID(int ID)
{
	m_nFilterIDPosition=SetFilter(FILTER_SQL_WHERE,m_strIDIdx+"="+QVariant(ID).toString(),m_nFilterIDPosition);
}

//WARNING: UTF 8 for umlauts!!!
void MainEntityFilter::SetFilter_Code(QString strCode)
{
	m_nFilterIDPosition=SetFilter(FILTER_SQL_WHERE,m_strCodeIdx+" LIKE '"+strCode.toUtf8()+"%'",m_nFilterIDPosition);
}

//WARNING: UTF 8 for umlauts!!!
void MainEntityFilter::SetFilter_Name(QString strName)
{
	m_nFilterIDPosition=SetFilter(FILTER_SQL_WHERE,m_strCodeIdx+" LIKE '"+strName.toUtf8()+"%'",m_nFilterIDPosition);
}


*/
