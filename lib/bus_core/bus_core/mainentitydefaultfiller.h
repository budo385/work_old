#ifndef MAINENTITYDEFAULTFILLER_H
#define MAINENTITYDEFAULTFILLER_H


#include <QString>
#include "common/common/dbrecordset.h"

/*!
	\class MainEntityDefaultFiller
	\brief Bo Entity for one record, 1, 1:1, 1:N, N:M
	\ingroup Bus_Core


	One point entry for all tables: hardcoded algorithms for filling default data

*/

class MainEntityDefaultFiller 
{
public:
    MainEntityDefaultFiller();
    ~MainEntityDefaultFiller();

	static void ClearRecordSet(DbRecordSet &lstRecords,int nColumnIdxToSkip=0);
	static void InsertDefaultValues(int nTableID,DbRecordSet &lstRecords, bool bFillMandatoryFields=false, bool bSkipIfAlreadyFilled=false, bool bClearIDField=false);
	static void ApplyDefaultValues(DbRecordSet &lstTarget,DbRecordSet &lstDefaults);

private:
    
};

#endif // MAINENTITYDEFAULTFILLER_H
