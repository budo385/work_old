#include "treelist.h"
#include <QtDebug> 

bool TreeList::TreeListComparator::operator ()(const FP_ITEM &a, const FP_ITEM &b) const
{
	bool bRes = false;

	//currently sort only by name (and 2nd key being Status)
	bRes = a.strName < b.strName;

	if(a.strName == b.strName)
	{
		if(a.nStatus == ITEM_STATUS_ON)
			bRes = false;
		else if(b.nStatus == ITEM_STATUS_ON)
			bRes = true;
		else if(a.nStatus == ITEM_STATUS_INHERITED)
			bRes = false;
		else if(b.nStatus == ITEM_STATUS_INHERITED)
			bRes = true;
	}
	
	//if(!m_bSortAsc)
	//	bRes = !bRes;

	return bRes;
}

int TreeList::FindByCode(int nCode)
{
	int nMax = size();
	for(int i=0; i<nMax; i++){
		if((*this)[i].nCode == nCode)
			return i;
	}
	return -1;
}

int TreeList::FindNode(QList<int> lstPos)
{
	//DumpPos(lstPos);
	//DumpTree();

	int nPos = -1;
	int nParent = -1;	//root

	//loop by tree depth levels
	int nCount = lstPos.size();
	for(int nLevel=0; nLevel<nCount; nLevel++)
	{
		int nIdx = lstPos[nLevel];

		//check if each child at this depth really exists 
		nPos = FindChild(nParent, nIdx);
		if(nPos < 0)
			return -1;	// child not found

		//prepare for the next level
		nParent  = nPos;
	}

	return nPos;	//index of the node in the list
}

int TreeList::FindChild(int nIdx, int nRow)
{
	if(-1 > nIdx || nIdx >= size())
		return -1;	//error - invalid parent node index

	int nChildLevel = 0;
	if(nIdx >= 0)
		nChildLevel = (*this)[nIdx].nLevel + 1;	//child level

	int nSibling = -1;
	int nPos = nIdx+1;
	int nSize = size();

	while(nPos<nSize)	//search forward until the end of array
	{
		int nLevel = (*this)[nPos].nLevel;

		if(nLevel < nChildLevel)
			return -1;			//error, no such child (branch terminated)
		else if(nLevel == nChildLevel){
			nSibling ++;			// one child found
			if(nRow == nSibling)
				return nPos;	// child found
		}

		nPos ++;
	}

	return -1;	// not found
}

int TreeList::FindNextSibling(int nIdx)
{
	if(0 > nIdx || nIdx >= size())
		return -1;	//error - invalid node index

	int nSiblingLevel = (*this)[nIdx].nLevel;

	int nPos = nIdx+1;
	int nCount = size();
	while(nPos<nCount)	//search forward until the end of array
	{
		int nLevel = (*this)[nPos].nLevel;

		if(nLevel < nSiblingLevel)
			return -1;		// error, no such sibling (branch terminated)
		else if(nLevel == nSiblingLevel)
			return nPos;	// sibling found

		nPos ++;
	}

	return -1;	//not found
}

//NOTE: -1 is valid index indicating root parent
int TreeList::GetChildCount(int nIdx)
{
	if(-1 > nIdx || nIdx >= size())
		return -1;	//error - invalid parent node index

	int nLevel = 0;
	if(nIdx >= 0)
		nLevel = (*this)[nIdx].nLevel + 1;

	int nChildren = 0;

	//find first child
	int nFirstChild = -1;
	int nCount = size();
	if(nIdx+1 < nCount && (*this)[nIdx+1].nLevel == nLevel)
	{
		nFirstChild = nIdx+1;
		nChildren ++;
	}
	else 
		return 0;
		
	//find next children
	int nNextChild = FindNextSibling(nFirstChild);
	while(nNextChild >= 0){
		nChildren ++;
		nNextChild = FindNextSibling(nNextChild);
	}

	return nChildren;
}

bool TreeList::RemoveNode(int nIdx)
{
	if(0 > nIdx || nIdx >= size())
		return false;	//error - invalid node index

	int nLevel = (*this)[nIdx].nLevel;

	//remove node along with all children
	removeAt(nIdx);

	while(nIdx < size())	//search forward until the end of array
	{
		if((*this)[nIdx].nLevel > nLevel)
		{
			removeAt(nIdx);
		}
		else
			break;	//sub-branch terminated
	}

	return true;	
}

bool TreeList::AddChild(int nIdx, FP_ITEM &item)
{
	if(0 > nIdx || nIdx >= size())
		return false;	//error - invalid node index

	int nLevel = (*this)[nIdx].nLevel;

	//find insert position (after the last child)
	nIdx ++;
	while(nIdx < size())	//search forward until the end of array
	{
		if((*this)[nIdx].nLevel > nLevel)
			nIdx ++;
		else
			break;	//sub-branch terminated
	}

	//fix item level
	item.nLevel = nLevel + 1;

	//insert the item
	insert(nIdx, item);

	return true;
}

int TreeList::FindParent(int nIdx)
{
	if(0 > nIdx || nIdx >= size())
		return -2;	//error - invalid node index (return root?)

	//search upward for a first item with one level up
	int nLevel = (*this)[nIdx].nLevel;
	
	nIdx --;
	while(nIdx >= 0)
	{
		if((*this)[nIdx].nLevel < nLevel)
			return nIdx;
		nIdx --;
	}

	return -1;	//parent must be root
}

bool TreeList::GetNodePos(int nIdx, QList<int> &lstPos)
{
	lstPos.clear();

	if(0 > nIdx || nIdx >= size())
		return false;	//error - invalid node index

	//go up until the root
	int nParent = nIdx;
	do{
		nParent = FindParent(nParent);

		// find relative position of a node within the parent's children
		int nCounter = 0;
		int nPos = FindChild(nParent, 0);
		while(nPos >= 0)
		{
			if (nPos == nIdx){
				lstPos.push_front(nCounter);
				break;
			}
			nCounter ++;
			nPos = FindNextSibling(nPos);
		}

		nIdx = nParent;

	} while(nParent >= 0);

	return true;
}

bool TreeList::CopyNode(int nIdx, int nParent)
{
	return CopyNode(*this, nIdx, nParent);
}

//copy entire branch from any source tree
bool TreeList::CopyNode(TreeList &srcTree, int nIdx, int nParent)
{
	if(0 > nIdx || nIdx >= srcTree.size() || -1 > nParent || nParent >= size())
		return false;	//error - invalid node index(es)

	//TOFIX parhaps this might be supproted, but not now - algorithm blocks
	//Q_ASSERT(this != &srcTree || nIdx != nParent);	//can not copy node to itself
	if(this == &srcTree && nIdx == nParent)
		return false;	//error - can not copy node to itself

	int nLevelParent = -1;	//root level
	if(nParent >= 0)
		nLevelParent = (*this)[nParent].nLevel;	//parent level

	//find insertion point (after the last child of a parent)
	int nPos = nParent + 1;
	while(nPos < size())	//search forward until the end of array
	{
		if((*this)[nPos].nLevel > nLevelParent)
			nPos ++;
		else
			break;	//sub-branch terminated
	}

	//copy the node and with its children (adjust levels)
	int nLevelSrc  = srcTree[nIdx].nLevel;
	int nLevelDiff = nLevelParent + 1 - nLevelSrc;

	int nPosSrc = nIdx;
	while(nPosSrc < srcTree.size())	//search forward until the end of array
	{
		if(srcTree[nPosSrc].nLevel > nLevelSrc || nPosSrc == nIdx){
			insert(nPos, srcTree[nPosSrc]);
			(*this)[nPos].nLevel += nLevelDiff;	//adjust level

			nPosSrc ++;
			nPos ++;

			//adjust when copying within the same tree
			if(this == &srcTree)
				if(nIdx > nParent && nParent >= 0)
					nPosSrc ++;
		}
		else
			break;	//sub-branch terminated
	}

	return true;
}

void TreeList::DumpPos(QList<int> lstPos)
{
	int nCount = lstPos.size();
	//qDebug() << "Dump tree position ( size =" << nCount << "):" << lstPos;
}

void TreeList::DumpTree()
{
	int nCount = size();
	//qDebug() << "Dump tree contents ( size =" << nCount << "):";
	for(int i=0; i<nCount; i++)
		qDebug() << "item(" << i << "):: level:" << this->at(i).nLevel << "code:" << this->at(i).nCode << " name:" <<this->at(i).strName;	
}

void TreeList::Sort()
{
	TreeListComparator cmp;
	qStableSort(begin(), end(), cmp);
}

//NOTE: -1 is valid index indicating root parent
int TreeList::GetChildCountRecursive(int nIdx)
{
	if(-1 > nIdx || nIdx >= size())
		return -1;	//error - invalid parent node index

	int nParentLevel = -1;
	if(nIdx >= 0)
		nParentLevel = (*this)[nIdx].nLevel;

	int nChildren = 0;

	//find first child
	int nChild = -1;
	int nCount = size();
	if(nIdx+1 < nCount && (*this)[nIdx+1].nLevel == nParentLevel+1)
		nChild = nIdx+1;
	else 
		return 0;
		
	//search next children and grand children
	while(nChild < nCount && (*this)[nChild].nLevel > nParentLevel)
	{
		nChildren ++;
		nChild ++;
	}

	return nChildren;
}
