#ifndef CONTACTTYPEMANAGER_H
#define CONTACTTYPEMANAGER_H

#include "common/common/dbrecordset.h"


/*!
	\class ContactTypeManager
	\brief Types of contact data: phones, addresses, etc..
	\ingroup Bus_Core


	Entity types is hardcoded structure, user can define own types inside one entity type.
	Every user type can ba assigned to one system type, this type is used to recognize type of item
	when importing from external system

*/
class ContactTypeManager 
{

public:

	enum ContactRecordTypeIDs
	{
		CM_TYPE_PERSON=1,		//avoid 0
		CM_TYPE_ORGANIZATION
	};

	enum ContactAccessRightType
	{
		ACC_RIGHT_PRIVATE=0,		//private
		ACC_RIGHT_PUBLIC_READ=1,	//public read
		ACC_RIGHT_PUBLIC_WRITE=2		//public write
	};

	enum ContactSex
	{
		SEX_MALE=0,		
		SEX_FEMALE=1,	
	};

	enum SubDataFilter //used on contact read/write to skip some subdata that is not needed in TVIEW_BUS_CONTACT_FULL
	{
		USE_SUBDATA_NONE=0,
		USE_SUBDATA_ALL=1,
		USE_SUBDATA_PICTURE=2,
		USE_SUBDATA_CREDITOR=4,
		USE_SUBDATA_DEBTOR=8,
		USE_SUBDATA_GROUP=16,
		USE_SUBDATA_JOURNAL=32,	
		USE_SUBDATA_CUSTOM_FIELDS=64,
	};

	enum TypeIDs
	{
		TYPE_ADDRESS=1,		//avoid 0
		TYPE_LANGUAGE,
		TYPE_SALUTATION,
		TYPE_TITLE,
		TYPE_APPOINTMENT,
		TYPE_DOCUMENTS,
		TYPE_EMAIL_ADDRESS,
		TYPE_EMAIL_MESSAGE,
		TYPE_INTERNET,
		TYPE_PHONE,
		TYPE_TASK,
		TYPE_SHORT_SALUTATION,
		TYPE_CALENDAR_PRESENCE,
		TYPE_CALENDAR_CALENDAR_VIEW
	};

	enum PhoneSystemTypes
	{
		SYSTEM_TYPE_EMPTY=0,			//used to mark as no type
		SYSTEM_PHONE_BUSINESS,
		SYSTEM_PHONE_PRIVATE,
		SYSTEM_PHONE_MOBILE,
		SYSTEM_PHONE_PRIVATE_MOBILE,
		SYSTEM_PAGER,
		SYSTEM_FAX,
		SYSTEM_SKYPE,
		SYSTEM_PHONE_DIRECT,	
		SYSTEM_PHONE_INTERNAL	
	};

	enum EmailSystemTypes
	{
		SYSTEM_EMAIL_BUSINESS=1,
		SYSTEM_EMAIL_PRIVATE
	};

	enum AddressSystemTypes
	{
		SYSTEM_ADDRESS_BUSINESS=1,
		SYSTEM_ADDRESS_PRIVATE
	};

	//Groups ID:
	enum ContactGroupsForCopy
	{
		GROUP_PHONE,			
		GROUP_ADDRESS,
		GROUP_EMAIL,
		GROUP_INTERNET,
		GROUP_PICTURE,
		GROUP_CREDITOR,
		GROUP_DEBTOR,
		GROUP_CONTACT_GROUP_ASSIGN,
		GROUP_CONTACT_CUSTOM_FIELDS

	};


	static QVariant GetDefaultTypeFromCache(int nEntityTypeID,DbRecordSet *lstCache);
	static void GetEntityTypeList(DbRecordSet &lstTypes);
	static void GetSystemTypeList(int nEntityTypeID,DbRecordSet &lstTypes);
	static void GetPhoneSystemTypes(DbRecordSet &lstTypes);
	static void GetEmailSystemTypes(DbRecordSet &lstTypes);
	static void GetAddressSystemTypes(DbRecordSet &lstTypes);
	static void AssignTypeName(DbRecordSet &lstTypes,DbRecordSet &lstSubData,QString strSubListTypeIDColIdx,bool bAddressType=false);
	static void SetPhonets(DbRecordSet &lstContacts);
	static DbRecordSet GetContactGroupsForCopy();

	static void ConvertContactListFromImport(DbRecordSet &lstContactTxtFormat,DbRecordSet &lstContact, QString strOrganizationNameForced,DbRecordSet *plstUsers);
	static void PrepareContactListFromImport(DbRecordSet &lstContact, DbRecordSet *pTypeDataSet,DbRecordSet *recSettings, DbRecordSet *recOptions,int nPersonID,DbRecordSet *lstSchemas, bool bSkipPhoneTypeSet=false);
	static void ExtractGroups(DbRecordSet &RetOut_Data);
	//special,type funct:
	static QVariant GetDefaultType(int nEntityTypeID,DbRecordSet *data);
	static QVariant GetUserTypeBasedOnSystemType(int nEntityTypeID, int nSystemTypeID,DbRecordSet *data);
	static QString GetTypeName(int TypeID,DbRecordSet *data);
	static void AddEmptyRowToFullContactList(DbRecordSet &data);
	static void AssembleSQLQueryForSearch(QString &strSQLWhereJoin,QString &strSQLWhere,QString strField1_Name, int nDataTypeCustomFld1, QString strField1_Value1, QString strField1_Value2, QString strField2_Name, int nDataTypeCustomFld2, QString strField2_Value1, QString strField2_Value2, bool bIgnoreCase=false);
	static QString GetFilterBasedOnFieldTypeCustomFld(QString strColName, int nDataType, QString strColValue1, QString strColValue2, bool bIgnoreCase=false);

	static void SortContactList(DbRecordSet &lstContacts, int nSortOrder=0 /* 0 -asc, 1- desc */);
private:
	static bool FormatAddressWithDefaultSchema(DbRecordSet &lstAddresses, int nContactType,int nSchemaDefaultPers,int nSchemaDefaultOrg,int nPersonID,DbRecordSet *lstSchemas);
    
};

#endif // CONTACTTYPEMANAGER_H
