#include "serverinifile.h"
#include "common/common/logger.h"

ServerIniFile::ServerIniFile()
{
}

ServerIniFile::~ServerIniFile()
{
}

void ServerIniFile::Clear()
{
	//m_lstConnections.clear();
}

//loads, creates if not exists, cheks values
bool ServerIniFile::Load(QString strFile, bool bCreateIfNotExists)
{
	m_strLastPathUsed=strFile;
	Clear();

	if(!QFileInfo(strFile).exists()&& bCreateIfNotExists)
		{return SaveDefaults(strFile);}

	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	// read INI values
	m_objIni.GetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.GetValue("Main", "AppServerPort",		m_nAppServerPort, 1111);
	m_objIni.GetValue("Main", "MaxConnections",		m_nMaxConnections, 2000);
	m_objIni.GetValue("Main", "MaxDbConnections",	m_nMaxDbConnections, 300);
	m_objIni.GetValue("Main", "MinDbConnections",	m_nMinDbConnections, 10);
	
	m_objIni.GetValue("Main", "AppServerRole",		m_nAppServerRole, 0);
	m_objIni.GetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.GetValue("Main", "IsMasterServer",		m_nIsMasterServer,1);
	m_objIni.GetValue("Main", "Descriptor",			m_strDescriptor);
	m_objIni.GetValue("Main", "AutoStartService",	m_nAutoStartService,1);
	m_objIni.GetValue("Main", "User Storage Disk Quota (GByte) (0 for unlimited)",			m_nDiskUserStorageQuota,10);
	m_objIni.GetValue("Main", "LastErrorCode",	m_nLastErrorCode,0);
	m_objIni.GetValue("Main", "LastErrorText",	m_strLastErrorText);
	m_objIni.GetValue("Main", "AppServerLocalIPAddress",	m_strLocalAppServerIP);
	m_objIni.GetValue("Main", "TempUserStorageDirectory",	m_strTempUserStorageDirectory);
	
		
	m_objIni.GetValue("EMAIL", "Enable Email Synchronization POP3",			m_nEnableEmailSync, 1);
	

	m_objIni.GetValue("SSL", "SSLMode",			m_nSSLMode, 0);
	m_objIni.GetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);
	m_objIni.GetValue("SSL", "Do not use Sokrates SSLPrivateKey",	m_nSSLNoPrivateKey,0);
	

	m_objIni.GetValue("DFO/LBO", "LoadBalanceActive",	m_nLoadBalanceActive, 0);
	m_objIni.GetValue("DFO/LBO", "AcceptDFConnections",m_nAcceptDFConnections, 1);
	
	m_objIni.GetValue("Security", "UseSecurityOptions",						m_bUseSecurityOptions,1);
	m_objIni.GetValue("Security", "Unsuccessful Connection Attempts Before Ban",		m_nIntrusionTries,3);
	m_objIni.GetValue("Security", "Default Ban Period (minutes) (0 for permanent)",		m_nKickPeriodAfterIntrusion,1440);
	m_objIni.GetValue("Security", "IPAccessListName_Preset",			m_strIPBlockListNamePreset);
	m_objIni.GetValue("Security", "IPAccessListName_Dynamic",			m_strIPBlockListNameDynamic);
	
	m_objIni.GetValue("Custom", "Name",				m_strName);

	m_objIni.GetValue("Logging", "Log Level",												m_nLogLevel,Logger::LOG_LEVEL_BASIC);
	m_objIni.GetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize,10);
	m_objIni.GetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize,64);
	m_objIni.GetValue("Logging", "Use File Log",		m_nLogTargetFile,1);
	m_objIni.GetValue("Logging", "Use Console Log",		m_nLogTargetConsole,1);
	
	m_objIni.GetValue("Backup", "Backup Frequency",		m_nBackupFreq,2);
	m_objIni.GetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.GetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.GetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.GetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.GetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
	m_objIni.GetValue("Backup", "Delete old Backup Files Older Than",m_nDeleteOldBackupsDay,0);
	

	m_objIni.GetValue("Update", "Update Time (hh:mm)",m_strUpdateTime);
	m_objIni.GetValue("Update", "Update Warning Time (min)",m_strWarningUpdateTime);

	m_objIni.GetValue("REST service", "Enable REST service",m_nRestEnable,0);
	//m_objIni.GetValue("REST service", "Enable REST service description",m_nRestEnableDesc,0);
	
	m_objIni.GetValue("WWW service", "Enable WWW service", m_nWWWEnable,0);
	m_objIni.GetValue("WWW service", "WebDocumentsPath",	m_strWebDocumentsPath);

	//debug
	m_objIni.GetValue("Debug", "logged_emails",	m_strLoggedEmails);

	m_objIni.GetValue("PushMessages", "ApplePushServerURL",m_strApplePushServerURL,"gateway.push.apple.com");
	m_objIni.GetValue("PushMessages", "ApplePushServerPort",m_nApplePushServerPort,2195);
	m_objIni.GetValue("PushMessages", "AppleFeedBackServerURL",m_strAppleFeedBackServerURL,"feedback.push.apple.com");
	m_objIni.GetValue("PushMessages", "AppleFeedBackServerPort",m_nAppleFeedBackServerPort,2196);
	m_objIni.GetValue("PushMessages", "AppleRootCaCertificateFile",m_strAppleRootCACertificateFile,"AppleIncRootCertificate.cer");
	m_objIni.GetValue("PushMessages", "AppleWWDRCACertificateFile",m_strAppleWWDRCACertificateFile,"AppleWWDRCA.cer");
	m_objIni.GetValue("PushMessages", "AppleLocalCertificateFile",m_strAppleLocalCertificateFile,"aps_prod_key_cert.pem");
	m_objIni.GetValue("PushMessages", "ApplePrivateKeyFile",m_strApplePrivateKeyFile,"aps_prod_key_noenc.pem");
	m_objIni.GetValue("PushMessages", "GooglePushServerURL",m_strGooglePushServerURL,"https://android.googleapis.com/gcm/send");
	m_objIni.GetValue("PushMessages", "GoogleAuthenticationID",m_strGoogleAuthenticationID,"AIzaSyCSJp2RUr9GwmYcc4wEyfEPVECXtTBj_l8");

	m_objIni.GetValue("Self Test Features", "StartEmailAdd (test procedure will add 4 digits at end, e.g. user0000, specify as 'user' or 'user@domain.com')",	m_strStartEmailAdd);
	m_objIni.GetValue("Self Test Features", "StartPassWordToAdd (test procedure will add 4 digits at end, e.g. pw0000)",	m_strStartPassWordAdd);
	m_objIni.GetValue("Self Test Features", "CountUserToAdd",		m_nCountUserAdd,0);
	m_objIni.GetValue("Self Test Features", "StartCounterToAdd (start digits from this value)",		m_nStartCounterAdd,0);
	m_objIni.GetValue("Self Test Features", "LastEmailAdded",	m_strLastEmailAdded);
	m_objIni.GetValue("Self Test Features", "EmailServerAdd",		m_strEmailServerAdd,"everxconnect.com");
	m_objIni.GetValue("Self Test Features", "EmailServerPortAdd",		m_nEmailServerPortAdd,110);
	m_objIni.GetValue("Self Test Features", "EmailServerIsImapAdd",		m_nEmailServerIsImapAdd,0);
	m_objIni.GetValue("Self Test Features", "EmailServerConnectionTypeAdd",		m_nEmailServerConnectionTypeAdd,0);
	m_objIni.GetValue("Self Test Features", "Test_SendingEmailsPeriodMinutes",	m_nTestSendingEmailsPeriodMinutes,0);
	m_objIni.GetValue("Self Test Features", "Test_NextTestTime",	m_strNextTestTime,"");
	m_objIni.GetValue("Self Test Features", "Test_EmailServerSMTPPort",	m_nEmailServerSMTPPort_Test,25);
	m_objIni.GetValue("Self Test Features", "Test_EmailServer",	m_strEmailServer_Test);
	
	
	return CheckValues();
}

bool ServerIniFile::Save(QString strFile)
{
	if (strFile.isEmpty())
		strFile=m_strLastPathUsed;

	m_objIni.SetPath(strFile);

	//fill the data inside
	m_objIni.RemoveSection("Main");		//cleanup existing data
	m_objIni.RemoveSection("Security");	
	m_objIni.RemoveSection("Logging");	
	m_objIni.RemoveSection("Backup");	
	m_objIni.RemoveSection("SSL");		
	m_objIni.RemoveSection("DFO/LBO");	
	m_objIni.RemoveSection("Update");	
	m_objIni.RemoveSection("Custom");	

	// write INI values
	m_objIni.SetValue("Main", "AppServerIPAddress", m_strAppServerIPAddress);
	m_objIni.SetValue("Main", "AppServerPort",		m_nAppServerPort);
	m_objIni.SetValue("Main", "MaxConnections",		m_nMaxConnections);
	m_objIni.SetValue("Main", "MaxDbConnections",	m_nMaxDbConnections);
	m_objIni.SetValue("Main", "MinDbConnections",	m_nMinDbConnections);
	m_objIni.SetValue("Main", "AppServerRole",		m_nAppServerRole);
	m_objIni.SetValue("Main", "DBConnectionName",	m_strDBConnectionName);
	m_objIni.SetValue("Main", "IsMasterServer",		m_nIsMasterServer);
	m_objIni.SetValue("Main", "Descriptor",			m_strDescriptor);
	m_objIni.SetValue("Main", "AutoStartService",	m_nAutoStartService);
	m_objIni.SetValue("Main", "User Storage Disk Quota (GByte) (0 for unlimited)",			m_nDiskUserStorageQuota);
	m_objIni.SetValue("Main", "LastErrorCode",	m_nLastErrorCode);
	m_objIni.SetValue("Main", "LastErrorText",	m_strLastErrorText);
	m_objIni.SetValue("Main", "AppServerLocalIPAddress",	m_strLocalAppServerIP);
	m_objIni.SetValue("Main", "TempUserStorageDirectory",	m_strTempUserStorageDirectory);

	m_objIni.SetValue("EMAIL", "Enable Email Synchronization POP3",			m_nEnableEmailSync);

	m_objIni.SetValue("SSL", "SSLMode",				m_nSSLMode);
	m_objIni.SetValue("SSL", "SSLCertificateDir",	m_strSSLCertificateDir);
	m_objIni.SetValue("SSL", "Do not use Sokrates SSLPrivateKey",	m_nSSLNoPrivateKey);

	m_objIni.SetValue("DFO/LBO", "LoadBalanceActive",	m_nLoadBalanceActive);
	m_objIni.SetValue("DFO/LBO", "AcceptDFConnections",	m_nAcceptDFConnections);
	
	m_objIni.SetValue("Security", "UseSecurityOptions",						m_bUseSecurityOptions);
	m_objIni.SetValue("Security", "Unsuccessful Connection Attempts Before Ban",		m_nIntrusionTries);
	m_objIni.SetValue("Security", "Default Ban Period (minutes) (0 for permanent)",		m_nKickPeriodAfterIntrusion);
	m_objIni.SetValue("Security", "IPAccessListName_Preset",			m_strIPBlockListNamePreset);
	m_objIni.SetValue("Security", "IPAccessListName_Dynamic",			m_strIPBlockListNameDynamic);

	m_objIni.SetValue("Custom", "Name",				m_strName);

	m_objIni.SetValue("Logging", "Log Level",												m_nLogLevel);
	m_objIni.SetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize);
	m_objIni.SetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize);
	m_objIni.SetValue("Logging", "Use File Log",		m_nLogTargetFile);
	m_objIni.SetValue("Logging", "Use Console Log",		m_nLogTargetConsole);

	m_objIni.SetValue("Backup", "Backup Frequency",		m_nBackupFreq);
	m_objIni.SetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.SetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.SetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.SetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.SetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
	m_objIni.SetValue("Backup", "Delete old Backup Files Older Than",m_nDeleteOldBackupsDay);

	m_objIni.SetValue("Update", "Update Time (hh:mm)",m_strUpdateTime);
	m_objIni.SetValue("Update", "Update Warning Time (min)",m_strWarningUpdateTime);

	m_objIni.SetValue("REST service", "Enable REST service",m_nRestEnable);
	//m_objIni.SetValue("REST service", "Enable REST service description",m_nRestEnableDesc);

	m_objIni.SetValue("WWW service", "Enable WWW service",m_nWWWEnable);
	m_objIni.SetValue("WWW service", "WebDocumentsPath",	m_strWebDocumentsPath);

	m_objIni.SetValue("PushMessages", "ApplePushServerURL",m_strApplePushServerURL);
	m_objIni.SetValue("PushMessages", "ApplePushServerPort",m_nApplePushServerPort);
	m_objIni.SetValue("PushMessages", "AppleFeedBackServerURL",m_strAppleFeedBackServerURL);
	m_objIni.SetValue("PushMessages", "AppleFeedBackServerPort",m_nAppleFeedBackServerPort);
	m_objIni.SetValue("PushMessages", "AppleRootCaCertificateFile",m_strAppleRootCACertificateFile);
	m_objIni.SetValue("PushMessages", "AppleWWDRCACertificateFile",m_strAppleWWDRCACertificateFile);
	m_objIni.SetValue("PushMessages", "ApplePrivateKeyFile",m_strApplePrivateKeyFile);
	m_objIni.SetValue("PushMessages", "AppleLocalCertificateFile",m_strAppleLocalCertificateFile);
	m_objIni.SetValue("PushMessages", "GooglePushServerURL",m_strGooglePushServerURL);
	m_objIni.SetValue("PushMessages", "GoogleAuthenticationID",m_strGoogleAuthenticationID);

	m_objIni.SetValue("Self Test Features", "StartEmailAdd (test procedure will add 4 digits at end, e.g. user0000, specify as 'user' or 'user@domain.com')",	m_strStartEmailAdd);
	m_objIni.SetValue("Self Test Features", "StartPassWordToAdd (test procedure will add 4 digits at end, e.g. pw0000)",	m_strStartPassWordAdd);
	m_objIni.SetValue("Self Test Features", "CountUserToAdd",		m_nCountUserAdd);
	m_objIni.SetValue("Self Test Features", "StartCounterToAdd (start digits from this value)",		m_nStartCounterAdd);
	m_objIni.SetValue("Self Test Features", "LastEmailAdded",	m_strLastEmailAdded);
	m_objIni.SetValue("Self Test Features", "EmailServerAdd",		m_strEmailServerAdd);
	m_objIni.SetValue("Self Test Features", "EmailServerPortAdd",		m_nEmailServerPortAdd);
	m_objIni.SetValue("Self Test Features", "EmailServerIsImapAdd",		m_nEmailServerIsImapAdd);
	m_objIni.SetValue("Self Test Features", "EmailServerConnectionTypeAdd",		m_nEmailServerConnectionTypeAdd);
	m_objIni.SetValue("Self Test Features", "Test_SendingEmailsPeriodMinutes",	m_nTestSendingEmailsPeriodMinutes);
	m_objIni.SetValue("Self Test Features", "Test_NextTestTime",	m_strNextTestTime);
	m_objIni.SetValue("Self Test Features", "Test_EmailServerSMTPPort",	m_nEmailServerSMTPPort_Test);
	m_objIni.SetValue("Self Test Features", "Test_EmailServer",	m_strEmailServer_Test);


	return m_objIni.Save();
}



bool ServerIniFile::CheckValues()
{
	if(m_nMaxConnections<=0) return false;
	if(m_nMaxDbConnections<=0) return false;
	if(m_nMinDbConnections<=0) return false;
	if(m_nAppServerRole<1) return false;
	if(m_nSSLMode<0 || m_nSSLMode>1) return false;
	if(m_nLoadBalanceActive<0 || m_nLoadBalanceActive>1) return false;
	if(m_nIsMasterServer<0 || m_nIsMasterServer>1) return false;
	if(m_nAutoStartService<0 || m_nAutoStartService>1) return false;
	if(m_bUseSecurityOptions<0 || m_bUseSecurityOptions>1) return false;
	if(m_nAcceptDFConnections<0 || m_nAcceptDFConnections>1) return false;
	if(m_nAppServerPort<0 || m_nAppServerPort>65536) return false;

	if(m_nDiskUserStorageQuota<0) return false;
	
	if(m_nLogBufferSize<0) return false;

	//reset to default log level:
	if(m_nLogLevel<0)m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	//if (nLevel>LOG_LEVEL_SPEED_TEST)nLevel=LOG_LEVEL_SPEED_TEST;
	//if(m_nLogLevel>Logger::LOG_LEVEL_DEBUG)m_nLogLevel=Logger::LOG_LEVEL_DEBUG;

	return true;
}



bool ServerIniFile::SaveDefaults(QString strFile)
{
	m_strAppServerIPAddress="0.0.0.0";
	m_nLastErrorCode=0;
	m_nAppServerPort=1111;
	m_nMaxConnections=2000;
	m_nMaxDbConnections=300;
	m_nMinDbConnections=10;
	m_nAppServerRole=1;
	m_nSSLMode=1;
	m_nLoadBalanceActive=0;
	m_nAcceptDFConnections=1;
	m_nIsMasterServer=1;
	m_nAutoStartService=1;
	m_nDeleteOldBackupsDay=0;
	m_nEnableEmailSync=1;
	m_nSSLNoPrivateKey=0;

	m_bUseSecurityOptions=1;
	m_nIntrusionTries=3;					// how many suspicios calla is allowed for one IP address before ban
	m_nKickPeriodAfterIntrusion=1440;		// 24hr

	m_nLogLevel=Logger::LOG_LEVEL_BASIC;
	m_nLogMaxSize=10;				
	m_nLogBufferSize=64;	
	m_nLogTargetFile=1;
	m_nLogTargetConsole=1;
	m_nBackupFreq=2;
	m_nBackupDay=0;
	m_nDiskUserStorageQuota=10;
	//m_nRestEnableDesc=0;
	m_nRestEnable=0;
	m_nWWWEnable=0;

	m_nCountUserAdd=0;
	m_nStartCounterAdd=0;
	m_nEmailServerPortAdd=110;
	m_nEmailServerIsImapAdd=0;
	m_nEmailServerConnectionTypeAdd=0;
	m_nTestSendingEmailsPeriodMinutes=0;
	m_nEmailServerSMTPPort_Test=25;

	m_strUpdateTime="00:00";
	m_strWarningUpdateTime="20";

	m_strApplePushServerURL = "gateway.push.apple.com";
	m_nApplePushServerPort = 2195;
	m_strAppleFeedBackServerURL = "feedback.push.apple.com";
	m_nAppleFeedBackServerPort = 2196;
	m_strAppleRootCACertificateFile = "AppleIncRootCertificate.cer";
	m_strAppleWWDRCACertificateFile = "AppleWWDRCA.cer";
	m_strAppleLocalCertificateFile = "aps_prod_key_cert.pem";
	m_strApplePrivateKeyFile = "aps_prod_key_noenc.pem";

	m_strGooglePushServerURL = "https://android.googleapis.com/gcm/send";
	m_strGoogleAuthenticationID = "AIzaSyCSJp2RUr9GwmYcc4wEyfEPVECXtTBj_l8";

	return Save(strFile);
}
