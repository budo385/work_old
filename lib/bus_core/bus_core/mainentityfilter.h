#ifndef MAINENTITYFILTER_H
#define MAINENTITYFILTER_H

#include "common/common/dbrecordset.h"


/*!
	\class MainEntitySelectionFilter
	\brief Filter used in selection controllers on client side
	\ingroup Bus_Core


	Every selection controller can have filter. 
	Filter can be (as defined in Marin mail 04.08.2006):
	- FILTER_SQL_WHERE			(one SQL WHERE condition, no WHERE statement as it can be more then 1 entry of this type)
	- FILTER_STORED_LIST		(ID of stored list from which records are shown: as stored list)
	- FILTER_CUSTOM_SELECTION	(contains ID of custom defined selection)
	- FILTER_CUSTOM_FILTER		(contains ID of custom defined filter)

	Filter can have more records of same type or of all types, all entries are executed with AND operation.
	



*/
class MainEntityFilter
{
public:
	MainEntityFilter();
	MainEntityFilter(DbRecordSet lstFilterData);
	
	~MainEntityFilter();

	//operators
	MainEntityFilter(const MainEntityFilter &that);
	MainEntityFilter &operator =(const MainEntityFilter &that); 
	bool operator ==(const MainEntityFilter &that); 

	//for all:
	void ClearFilter(int nPosition=-10);											   //clears all
	int SetFilter(int nFilterType,QString strFilter, int nPosition=-1);				   //set filter
	int SetFilter(QString strColumnName, int nColumnVal,int nPosition=-1); //set filter overloaded
	bool IsEmpty(){return !(bool)m_lstFilters.getRowCount();};

	DbRecordSet Serialize(){return m_lstFilters;};
	static QString GetWhereStatement(DbRecordSet& FilterData,bool bAddWhereIfNotExists=false);

	bool DoesRecordMatchFilter(DbRecordSet& record);

	enum FilterType
	{
		FILTER_SQL_WHERE,   //write where condition without WHERE
		FILTER_FROM_STORED_LIST,
		FILTER_CUSTOM_SELECTION,
		FILTER_CUSTOM_FILTER
	};


	/*
	//QUICK FILTER: by ID, CODE, NAME, can be used or not
	void SetFilterInfo(QString strIDIdx="",QString strCodeIdx="",QString strNameIdx="");

	//most common filters:
	void SetFilter_ID(int ID);
	void ClearFilter_ID();
	
	void SetFilter_Code(QString strCode);
	void ClearFilter_Code();
	
	void SetFilter_Name(QString strName);
	void ClearFilter_Name();
	
*/

	DbRecordSet& GetRecordSet(){ return m_lstFilters; }

private:
	DbRecordSet m_lstFilters;

	/*
	int m_nFilterIDPosition;
	int m_nFilterCodePosition;
	int m_nFilterNamePosition;

	QString m_strIDIdx;
	QString m_strCodeIdx;
	QString m_strNameIdx;

	*/
};

#endif // MAINENTITYFILTER_H
