#include "hierarchicalhelper.h"
#include <QChar>
#define HCT_DELIMITER  '.'


HierarchicalHelper::HierarchicalHelper()
{
}

HierarchicalHelper::~HierarchicalHelper()
{
}

void HierarchicalHelper::IncrementLastSegment(QString &strCode)
{
	//strip last delimiter
	if(strCode.length()>0 && '.' == strCode.at(strCode.length()-1))
		strCode = strCode.left(strCode.length()-1);

	//extract last segment
	QString strSegment = strCode.section('.', -1);
	strCode = strCode.left(strCode.length() - strSegment.length());

	bool ok;
	uint nSegment = strSegment.toUInt(&ok, 10);	//try to convert to numeric
	if(ok)
	{
		//code section is numerical, increment
		nSegment ++;
		QString strNewSegment = QString().setNum(nSegment);

		//append leading zeroes to match previous code
		int nLen = strSegment.size();
		if(strNewSegment.size() < nLen)
			strNewSegment = strNewSegment.rightJustified(nLen, '0');

		strCode += strNewSegment;
	}
	else
	{
		//code section is alpha
		if(strSegment.length()>0)
		{
			int nPos = strSegment.length()-1;
			while(nPos >= 0)
			{
				if(strSegment.at(nPos) < 127){
					QChar cNew = QChar(strSegment.at(nPos).toLatin1() + 1);
					if(cNew.isLetter()){
						strSegment[nPos] = cNew;
						if(nPos < strSegment.length())
						{
							//replace later characters with "A"
							for(int i=nPos+1; i<strSegment.length(); i++)
								strSegment[i] = 'A';
						}
						break;
					}
					else{
						nPos --;
						if(nPos < 0){
							strSegment += "A";
							break;
						}
					}
				}
			}
		}

		strCode += strSegment; 
	}

	strCode += ".";	// terminate the segment
}

void HierarchicalHelper::GenerateChildCode(QString &strCode)
{
	//terminate existing segment
	if( strCode.length()>0 &&
		strCode.at(strCode.length()-1) != '.')
		strCode += ".";

	//append new code segment (2 digits)
	strCode += "01.";
}

int	HierarchicalHelper::CalcCodeSegmentsCount(const QString &strCode)
{
	QString strParentTmp = strCode;
	if( strParentTmp.length()>0 &&
		strParentTmp.at(strParentTmp.length()-1) != '.')
		strParentTmp += ".";

	//count number of segments in the parent code
	int nFrom = 0;
	int nNumSegments = 0;
	int nPos = -1;
	while((nPos = strParentTmp.indexOf('.', nFrom)) >= 0){
		nNumSegments ++;
		nFrom = nPos+1;
	}

	return nNumSegments;
}

QString HierarchicalHelper::CutToTheNumberOfSegments(const QString &strCode, int nSegments)
{
	QString strRes = strCode;
	if( strRes.length()>0 &&
		strRes.at(strRes.length()-1) != '.')
		strRes += ".";

	//count number of segments in the parent code
	int nFrom = 0;
	int nCurSegment = 0;
	int nPos = -1;
	int nLastPos = -1;
	while(nCurSegment < nSegments)
	{
		nLastPos = nPos;

		nPos = strRes.indexOf('.', nFrom);
		if(nPos >= 0)
		{
			nCurSegment ++;
			nFrom = nPos+1;
			nLastPos = nPos;
		}
		else
		{
			break;
		}
	}

	if(nLastPos >= 0)
		strRes = strRes.left(nLastPos+1);
	else
		strRes = "";

	return strRes;
}


bool HierarchicalHelper::IsCodeDescendant(const QString &strCode, const QString &strAncestor)
{
	// empty code does not have parents
	if(strCode.isEmpty())
		return false;	

	// empty ancestor is parent to everyone
	if(strAncestor.isEmpty())
		return true;	

	if(strCode == strAncestor)
		return false;	// not a parent

	// descendant code should contain its ancestor's code
	int nPos = strCode.indexOf(strAncestor);
	return (nPos == 0);
}

bool HierarchicalHelper::IsCodeAncestor(const QString &strCode, const QString &strChild)
{
	return IsCodeDescendant(strChild, strCode);
}

bool HierarchicalHelper::IsCodeParent(const QString &strCode, const QString &strChild)
{
	if(!IsCodeAncestor(strCode, strChild))
		return false;

	// code is an ancestor, but is it a parent?
	QString strParent = CalcParentCode(strChild);
	return (strParent == strCode);
}

QString HierarchicalHelper::CalcParentCode(const QString &strCode)
{
	QString strParent;

	// no parent for empty code
	if(strCode.isEmpty())
		return strParent;

	strParent = strCode;

	// trim ending delimiter
	if(HCT_DELIMITER == strParent.at(strCode.length()-1))
		strParent = strParent.left(strCode.length()-1);

	// now cut last segment
	int nPos = strParent.lastIndexOf(HCT_DELIMITER);
	if(nPos >= 0)
		strParent = strParent.left(nPos+1);
	else
		strParent.clear();

	return strParent;
}

//select all children's in list of strParentCode
//return count
//strCodeColumnName is name of code column inside lstData
//list must be sorted by code
//nRowParent is row inside lstData where parent is 
int HierarchicalHelper::SelectChildren(const QString &strParentCode, DbRecordSet &lstData, QString strCodeColumnName, bool bDestroyPreviousSelection, int nRowParent)
{
	int nCodeIdx=lstData.getColumnIdx(strCodeColumnName);
	int nCodeSize=strParentCode.size();
	if (bDestroyPreviousSelection)
		lstData.clearSelection();
	int nSize=lstData.getRowCount();
	int nCount=0;
	if (nRowParent==-1)
		nRowParent=lstData.find(strCodeColumnName,strParentCode,true); //find parent
	if (nRowParent!=-1)
	{
		nRowParent++;
		for(int i=nRowParent;i<nSize;++i)
		{
			QString strChildCode=lstData.getDataRef(i,strCodeColumnName).toString().left(nCodeSize);
			if (strChildCode!=strParentCode) //M.R. told that left is faster then indexOf
				break;
			lstData.selectRow(i);
			nCount++;
		}
	}
	else
	{
		for(int i=0;i<nSize;++i) //traverse through whole list
		{
			QString strChildCode=lstData.getDataRef(i,strCodeColumnName).toString().left(nCodeSize);
			if (strChildCode!=strParentCode) //M.R. told that left is faster then indexOf
				continue; 
			lstData.selectRow(i);
			nCount++;
		}

	}
	return nCount;
}

int	HierarchicalHelper::SortedListFindParentNodeIdx(DbRecordSet &lstData, QString strCode, int nCodeColIdx, int nStartFrom)
{
	//QString strCodeParent = HierarchicalHelper::CalcParentCode(strCode);
	//calculate first segment of hierarchical code (or empty if the segment equals code)
	QString strCodeParent;
	int nPos = strCode.indexOf(".");
	if(nPos >= 0){
		strCodeParent = strCode.left(nPos+1);
		if(strCodeParent == strCode)
			strCodeParent = "";
	}

	//if the row has parent, it must be in one of the rows before it
	int nParentIdx  = -1;
	for(int j=nStartFrom; j>=0; j--){
		QString strCodeTmp = lstData.getDataRef(j, nCodeColIdx).toString();
		if(HierarchicalHelper::IsCodeAncestor(strCodeTmp, strCode)){
			nParentIdx = j; break;
		}
		else if(!strCodeParent.isEmpty() &&
				!strCodeTmp.startsWith(strCodeParent))
			break;	// reached different root branch
	}

	return nParentIdx;
}

int	HierarchicalHelper::SortedListFindPrevSiblingNodeIdx(DbRecordSet &lstData, QString strCode, int nCodeColIdx, int nStartFrom, int nParentIdx)
{
	if(nParentIdx < 0)
		nParentIdx  = HierarchicalHelper::SortedListFindParentNodeIdx(lstData, strCode, nCodeColIdx, nStartFrom);

	//QString strCodeParent = HierarchicalHelper::CalcParentCode(strCode);
	//calculate first segment of hierarchical code (or empty if the segment equals code)
	QString strCodeParent;
	int nPos = strCode.indexOf(".");
	if(nPos >= 0){
		strCodeParent = strCode.left(nPos+1);
		if(strCodeParent == strCode)
			strCodeParent = "";
	}

	//find previous sibling
	int nPrevSibIdx = -1;	// index of previous sibling node
	for(int j=nStartFrom; j>=0; j--){
		QString strCodeTmp = lstData.getDataRef(j, nCodeColIdx).toString();
		if(HierarchicalHelper::IsCodeAncestor(strCodeTmp, strCode)){
			break;
		}
		else if(strCodeParent.isEmpty() ||
				strCodeTmp.startsWith(strCodeParent))
		{
			//siblings are nodes who have the same parent
			int nSibParentIdx = HierarchicalHelper::SortedListFindParentNodeIdx(lstData, strCodeTmp, nCodeColIdx, j-1);
			if(nSibParentIdx == nParentIdx){
				nPrevSibIdx = j; 
				break;	// found first positive result, exiting
			}
		}
		else
			break;	// reached different root branch
	}
		
	return nPrevSibIdx;
}

void HierarchicalHelper::RemoveInvisibleTreeNodes(DbRecordSet &lstData, QString strCodeCol, QString strExpandedCol)
{
	int nCodeColIdx = lstData.getColumnIdx(strCodeCol);
	int nExpandedColIdx = lstData.getColumnIdx(strExpandedCol);

	int nCount = lstData.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		if(lstData.getDataRef(i, nExpandedColIdx).toInt() < 1)
		{
			//note is not expanded
			QString strCodeParent = lstData.getDataRef(i, nCodeColIdx).toString();

			//remove all child rows for this note
			for(int j=i+1; j<nCount; j++)
			{
				QString strCode = lstData.getDataRef(j, nCodeColIdx).toString();
				if(IsCodeDescendant(strCode, strCodeParent)){
					lstData.deleteRow(j);
					nCount --;
					j --;
				}
			}
		}
	}
}
