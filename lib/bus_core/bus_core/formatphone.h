#ifndef FORMATPHONE_H
#define FORMATPHONE_H

#include <QString>
#include "common/common/dbrecordset.h"
/*!
	\class FormatPhone
	\brief Use for formatting phone
	\ingroup Bus_Core

	Phone is formatted if allowed (by type):
	1. Originally entered phone number, e.g. �056 234 32 22�
	2. Automatically properly formatted phone number e.g. �0041 (0)56 923 99 23� (Fullnumber Format)
	3. Country predial code e.g. �0041�
	4. Area code e.g. �056�
	5. Local number e.g. �923 99 23�
	

*/

class FormatPhone 
{

public:
    FormatPhone();
    ~FormatPhone();

	static void SetDefaults(QString strCountry,QString strArea);

	QString FormatPhoneNumber(QString strCountry,QString strArea,QString strLocal);
	bool CheckPhone(QString strCountry,QString strArea,QString strLocal);			
	QString GenerateSearchNumber(QString strCountry,QString strArea,QString strLocal);
	void SplitPhoneNumber(QString strPhone, QString &strCountry,QString &strArea,QString &strLocal,QString &strSearch,QString &strCountryISO);

	void ReFormatPhoneNumbers(DbRecordSet &lstPhones, bool bOnlyFormatSearch=false);


private:
	void GetDefaults();
	QString FilterNonDigits(QString &strInput);



};

#endif // FORMATPHONE_H
