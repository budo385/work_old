#include "licenseinfo.h"
#include "licensestreamreader.h"
#include "licensestreamwriter.h"
#include "common/common/rsakey.h"
#include <qDebug>


#ifndef min
#define min(a,b) (((a)<(b))? (a):(b))
#endif



#define KEY_SIZE 2048	

char szPubKey[] = 
	"-----BEGIN RSA PUBLIC KEY-----\n"
	"MIIBCgKCAQEArN021y9M7fj6HE902QZVKvNT7iBMEDqL3EaS8ejz+8DxINonuGBL\n"
	"YNpKGVRQzbHDQmmOnGAyGapPvOGpZM2+ZN7Eg0Kw3eAgfVNJUDicjcwS/FP53qLX\n"
	"IKDXsoh5ByRX3sX67DR6ots7rnPtvHBWee1kaWEQ5sxFI8ZNF0162ByYnAGQSoxY\n"
	"iOeWBUPNRL/YJt09F3wmTWsK9kCSINCWOJWQtptnUy4NgpUz++f6DaLlieyuAYGW\n"
	"RIWeI0BrFzoC98luPAVf82ATuhNMITELfxp0v0aV56S69oMm5H6iaT4XolCUeTuE\n"
	"LDcXhO83kJ4ph1gOFfyKugiVJwpU/362mQIDAQAB\n"
	"-----END RSA PUBLIC KEY-----\n";

char szPrivKey[] = 
	"-----BEGIN RSA PRIVATE KEY-----\n"
	"MIIEowIBAAKCAQEArN021y9M7fj6HE902QZVKvNT7iBMEDqL3EaS8ejz+8DxINon\n"
	"uGBLYNpKGVRQzbHDQmmOnGAyGapPvOGpZM2+ZN7Eg0Kw3eAgfVNJUDicjcwS/FP5\n"
	"3qLXIKDXsoh5ByRX3sX67DR6ots7rnPtvHBWee1kaWEQ5sxFI8ZNF0162ByYnAGQ\n"
	"SoxYiOeWBUPNRL/YJt09F3wmTWsK9kCSINCWOJWQtptnUy4NgpUz++f6DaLlieyu\n"
	"AYGWRIWeI0BrFzoC98luPAVf82ATuhNMITELfxp0v0aV56S69oMm5H6iaT4XolCU\n"
	"eTuELDcXhO83kJ4ph1gOFfyKugiVJwpU/362mQIDAQABAoIBAFt189UoxqhGina+\n"
	"J5HIQvmlouukFPqlL2jNwmPSIH08WZhLNKbd9CP7eycpsFMU0b3qwbTFRNzFesjy\n"
	"EMEwBqOyJgFZIOFT+3yzu1+BhHuV1RnCiLseJ3qw/08L3BVGSmOpFpkxLw3JtysX\n"
	"VVrViTubufvzipDC1XYXLqS8rtl2cThQjNt4ZPxZ8016/SnrmdrOT1hDXXAno427\n"
	"t2MEkVcKYMMO/rTnbjFY/Laasv6GtHUAc/aF1fc1j7rZZUX1yMF913sdpx9cSkOD\n"
	"LBdGO+GNEJRM+iABMmwhFwBLAuTB7LLRP8htVRFuqd92JLO57tXF0OYUOXNTRwAg\n"
	"tAbzfTECgYEA1hBGab6BsHnOAa52nWS4IJ/fTQ7xezRte5My5fqXG2ULY9L5j5i+\n"
	"QGxWzHx5vaEOBO/Hsds4NOnmFnIXaOxOHY4M6GsxPAw6COq3nUQv1iwtkOskhEQP\n"
	"emVqM9pA4OjEYnc4ISeHq/H2Pjp4p3fq1LktaL/kOjrr9/3E/44elEUCgYEAzrqz\n"
	"z44MQzf4x7DgnSgWocRfJXk4gcWjO7MeSlQhoW084OMSqV9Fu6STg2LCjvSL0ZhQ\n"
	"buiawBccTtGolULAuxPp8xUD/nqtRshCj+sj9UtMnWpxkG6Z87G50Ckfoet2JVKu\n"
	"xJ8S5wZF0LBeTTlC8/HUCkhcqxKSPgfDJqcpwEUCgYEA1E9ABxETv9yPHcvTiTek\n"
	"B3eJGfyANRpCnBXanY504I7PRSqyqmFsEKljTV0HwbRDG1JWVWh/zrtNoWV3InF8\n"
	"6INC+iGWq1/VD5L8mdZPO7SFXP5bMXPvhg2fz93gCzHR1Xc5DGkQftF00TNdB3xA\n"
	"RMuE5iesTLSz5TqkcHKckaECgYAEDasa5bkd1EofeDtfOXsAHv6SmIbgHITv1bpB\n"
	"UNb7+E1aR3mFMqYGYNq3l8ci0G6xZGAnXI40N//JF9lLW+y1flV2+7GBWtxs595O\n"
	"PhkNZTWSXJTvdQu1uIVhLH0/SoOdhbqBhnDzcK8LwXmH4BiM+e/jZ7nRbOLLLpHH\n"
	"/z0VVQKBgCpPbuI8YlxQpIPGgUA/9vXV8utKU8ubkqv+zj+9+rCjXU4wWVvAVzR+\n"
	"oZNHPWaLL8rnX6vw1YTVrAdgHYI8L3oBUOWy5NAVAaqtFf3WCzn1LUj9hh9IP1Tp\n"
	"wD3IvHx7GGteINvjnRQkSnV9okpX/iGMe1rKZXz8rUIWIIL3SSiz\n"
	"-----END RSA PRIVATE KEY-----\n";

LicenseInfo::LicenseInfo()
{
	//init to some default values
	m_nLicenseVersion = 1000;	
	m_nLicenseID	= 0;
	m_nCustomSolutionID = 0;
	m_bIsCompany	= true;
	m_dateCreated	= QDateTime::currentDateTime();
	m_dateModified	= m_dateCreated;
	m_bModified		= false;

	strCopyRight =QString("SOKRATES")+QChar(174)+QString(" Copyright by Helix Business Soft AG, Switzerland");
}

LicenseInfo::LicenseInfo(const LicenseInfo &other)
{
	operator = (other);
}

void LicenseInfo::operator =(const LicenseInfo &other)
{
	if(this != &other){
		m_nLicenseVersion	= other.m_nLicenseVersion;
		m_nLicenseID		= other.m_nLicenseID;
		m_strCustomerID		= other.m_strCustomerID;
		m_nCustomSolutionID = other.m_nCustomSolutionID;
		m_strClientName		= other.m_strClientName;
		m_strReportLine1	= other.m_strReportLine1;
		m_strReportLine2	= other.m_strReportLine2;
		m_bIsCompany		= other.m_bIsCompany;
		m_dateCreated		= other.m_dateCreated;
		m_dateModified		= other.m_dateModified;
		m_bModified			= other.m_bModified;
	}
}

void LicenseInfo::Clear()
{
	m_bModified = false;
	m_nLicenseVersion = 1000;	
	m_nLicenseID	= 0;
	m_nCustomSolutionID = 0;
	m_bIsCompany	= true;
	m_dateCreated	= QDateTime::currentDateTime();
	m_dateModified	= m_dateCreated;
	m_strCustomerID.clear();
	m_strClientName.clear();
	m_strReportLine1.clear();
	m_strReportLine2.clear();
	m_lstModInfo.clear();
}

bool LicenseInfo::Load(QString strFile)
{
	Clear();

	FILE *pKeyFile = fopen(strFile.toLatin1().constData(), "rb");
	if(NULL != pKeyFile)
	{
		//1st field to read - copyright string
		char szBuffer[1024];
		int nRead = fread(szBuffer, strlen(strCopyRight.toLatin1()), 1, pKeyFile);
		if(nRead != 1 || 0 != memcmp(szBuffer, strCopyRight.toLatin1(), strlen(strCopyRight.toLatin1()))){
			fclose(pKeyFile);
			return false;		// no copyright string at the start of the file
		}

		//2nd field to read - format version number
		nRead = fread(&m_nLicenseVersion, 4, 1, pKeyFile);
		if(nRead != 1){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//
		// now comes RSA encrypted part - decrypt
		//
		LicenseStreamReader reader;
		reader.SetPublicKey(szPubKey);
		reader.AttachSource(pKeyFile);

		//3rd field to read - random number block (8 bytes)
		if(!reader.ReadBytes(szBuffer, 8)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//4th field to read - license id number - unsigned int (4 bytes)
		if(!reader.ReadBytes(&m_nLicenseID, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//5th field to read - customer ID string (12 bytes)
		if(!reader.ReadBytes(szBuffer, 12)){
			fclose(pKeyFile);
			return false;		// field not found
		}
		szBuffer[12] = '\0';
		m_strCustomerID.append(szBuffer);

		//6th field to read - custom solution ID number - unsigned int (4 bytes)
		if(!reader.ReadBytes(&m_nCustomSolutionID, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//7th field to read - client name  (100 bytes)
		if(!reader.ReadString(szBuffer, 100)){
			fclose(pKeyFile);
			return false;		// field not found
		}
		m_strClientName = QString::fromLocal8Bit(szBuffer);

		//8th report field string #1 (80 bytes)
		if(!reader.ReadString(szBuffer, 80)){
			fclose(pKeyFile);
			return false;		// field not found
		}
		m_strReportLine1 = QString::fromLocal8Bit(szBuffer);

		//9th report field string #2 (80 bytes)
		if(!reader.ReadString(szBuffer, 80)){
			fclose(pKeyFile);
			return false;		// field not found
		}
		m_strReportLine2 = QString::fromLocal8Bit(szBuffer);

		//10th field to read - "is a company" - boolean (1 byte)
		if(!reader.ReadBytes(&m_bIsCompany, 1)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//11th field to read - created date/time (4 bytes)
		time_t date = 0;
		if(!reader.ReadBytes(&date, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}
		m_dateCreated.setTime_t(date);

		//12th field to read - modified date/time (4 bytes)
		date = 0;
		if(!reader.ReadBytes(&date, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}
		m_dateModified.setTime_t(date);

		//13th field to read - number of module sections (4 bytes)
		int nNumModuleSections = 0;
		if(!reader.ReadBytes(&nNumModuleSections, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}
		
		//
		// Now read module sections
		//

		for(int i=0; i<nNumModuleSections; i++)
		{
			LicenseModInfo section;

			//1st module section field to read - module type string (10 bytes)
			if(!reader.ReadString(szBuffer, 10)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_strCode = QString::fromLocal8Bit(szBuffer);

			//2nd module section field to read - module subtype string (10 bytes)
			if(!reader.ReadString(szBuffer, 10)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_strSubCode = QString::fromLocal8Bit(szBuffer);

			//3rd module section field to read - module name string (100 bytes)
			if(!reader.ReadString(szBuffer, 100)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_strName = QString::fromLocal8Bit(szBuffer);

			//4th module section field to read - report line #1 string (80 bytes)
			if(!reader.ReadString(szBuffer, 80)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_strReportLine1 = QString::fromLocal8Bit(szBuffer);

			//5th module section field to read - report line #2 string (80 bytes)
			if(!reader.ReadString(szBuffer, 80)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_strReportLine2 = QString::fromLocal8Bit(szBuffer);

			//6th field to read - custom solution ID number (4 bytes)
			if(!reader.ReadBytes(&section.m_nCustomSolutionID, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//7th module section field to read - MLIID string (16 bytes)
			if(!reader.ReadString(szBuffer, 18)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_strMLIID = QString::fromLocal8Bit(szBuffer);

			//8th field to read - licencing type short number (2 bytes)
			if(!reader.ReadBytes(&section.m_nLicensingType, 1)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//9th field to read - number of licencses (4 bytes)
			if(!reader.ReadBytes(&section.m_nNumLicenses, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//10th field to read - licencse platforms code short number (2 bytes)
			if(!reader.ReadBytes(&section.m_nLicensePlatforms, 2)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//11th field to read - licencse platforms subcode short number (2 bytes)
			if(!reader.ReadBytes(&section.m_nLicensePlatformsSubcode, 2)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//12th field to read - created date/time (4 bytes)
			time_t date = 0;
			if(!reader.ReadBytes(&date, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_dateCreated.setTime_t(date);
		
			//13th field to read - modified date/time (4 bytes)
			date = 0;
			if(!reader.ReadBytes(&date, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_dateModified.setTime_t(date);

			//14th field to read - expired date/time (4 bytes)
			date = 0;
			if(!reader.ReadBytes(&date, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_dateExpiration.setTime_t(date);

			//15th field to read - expiration warning date/time (4 bytes)
			date = 0;
			if(!reader.ReadBytes(&date, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_dateWarning.setTime_t(date);

			//16th field to read - use licence expiration data - boolean (1 byte)
			if(!reader.ReadBytes(&section.m_bUseExpireDate, 1)){
				fclose(pKeyFile);
				return false;		// field not found
			}
		
			//17th field to read - 'modified by' string (5 bytes)
			if(!reader.ReadString(szBuffer, 5)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_strModifiedBy = QString::fromLocal8Bit(szBuffer);

			//18th field to read - serial number string (50 bytes)
			if(!reader.ReadString(szBuffer, 50)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_strSerialNumber = QString::fromLocal8Bit(szBuffer);

			//TOFIX how big is this?
			//19th field to read - encrypted blob (256 bytes)
			if(!reader.ReadString(szBuffer, 256)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_arEncBlob = QByteArray(szBuffer, 256);

			//20th field to read - number of FP sections (4 bytes)
			int nNumFPSections = 0;
			if(!reader.ReadBytes(&nNumFPSections, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//
			// Now read FP sections for a single module
			//
			for(int j=0; j<nNumFPSections; j++)
			{
				FP_ITEM1 item;

				//1st FP field to read - code number (4 bytes)
				if(!reader.ReadBytes(&item.nCode, 4)){
					fclose(pKeyFile);
					return false;		// field not found
				}
				
				//2nd FP field to read - can use (1 byte)
				if(!reader.ReadBytes(&item.bCanUse, 1)){
					fclose(pKeyFile);
					return false;		// field not found
				}
				
				//3rd FP field to read - value number (4 bytes)
				if(!reader.ReadBytes(&item.nValue, 4)){
					fclose(pKeyFile);
					return false;		// field not found
				}

				//store item in the list
				section.m_lstAR.append(item);
			}

			//some module section calculations before adding into tree
			section.AR_RefreshTreeFromFlatList(m_fpdf);	//refresh data before using it

			//calc module name from code (looking into current FPDF tree)
			int nCnt2 = m_fpdf.m_lstModSets.size();
			for(int k=0; k<nCnt2; k++)
			{
				if( m_fpdf.m_lstModSets[k].nType   == ITEM_TYPE_MOD &&
					m_fpdf.m_lstModSets[k].strCode == section.m_strCode)
				{
					section.m_strModuleName = m_fpdf.m_lstModSets[k].strName;
					break;
				}
			}
			
			//add section into the list
			m_lstModInfo.append(section);
		}

		fclose(pKeyFile);
		return true;
	}
	
	return false;
}

bool LicenseInfo::Save(QString strFile)
{
	FILE *pKeyFile = fopen(strFile.toLatin1().constData(), "wb");
	if(NULL != pKeyFile)
	{
		//1st field to write - copyright string
		int nWrite = fwrite(strCopyRight.toLatin1(), strlen(strCopyRight.toLatin1()), 1, pKeyFile);
		if(nWrite != 1){
			fclose(pKeyFile);
			return false;		// no copyright string at the start of the file
		}

		//2nd field to write - format version number
		nWrite = fwrite(&m_nLicenseVersion, 4, 1, pKeyFile);
		if(nWrite != 1){
			fclose(pKeyFile);
			return false;		// no copyright string at the start of the file
		}

		//
		// now comes RSA encrypted part - encrypt
		//
		LicenseStreamWriter writer;
		writer.SetPrivateKey(szPrivKey);
		writer.AttachSource(pKeyFile);

		char szBuffer[1024];

		//3rd field to write - random number block (8 bytes)
		OpenSSLHelper::Extern_RAND_bytes((unsigned char *)szBuffer, 8);		//generate fresh 8 random bytes
		if(!writer.WriteBytes(szBuffer, 8)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//4th field to write - license id number - unsigned int (4 bytes)
		if(!writer.WriteBytes(&m_nLicenseID, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//5th field to write - customer ID string (12 bytes)
		if(!writer.WriteString(m_strCustomerID, 12)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//6th field to write - custom solution ID number - unsigned int (4 bytes)
		if(!writer.WriteBytes(&m_nCustomSolutionID, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//7th field to write - client name  (100 bytes)
		if(!writer.WriteString(m_strClientName, 100)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//8th report field string #1 (80 bytes)
		if(!writer.WriteString(m_strReportLine1, 80)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//9th report field string #2 (80 bytes)
		if(!writer.WriteString(m_strReportLine2, 80)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//10th field to write - "is a company" - boolean (1 byte)
		if(!writer.WriteBytes(&m_bIsCompany, 1)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//11th field to write - created date/time (4 bytes)
		time_t date = m_dateCreated.toTime_t();
		if(!writer.WriteBytes(&date, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//12th field to write - modified date/time (4 bytes)
		date = m_dateModified.toTime_t();
		if(!writer.WriteBytes(&date, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}

		//13th field to write - number of module sections (4 bytes)
		int nNumModuleSections = m_lstModInfo.size();
		if(!writer.WriteBytes(&nNumModuleSections, 4)){
			fclose(pKeyFile);
			return false;		// field not found
		}
		
		//
		// Now write module sections
		//

		for(int i=0; i<nNumModuleSections; i++)
		{
			LicenseModInfo &section(m_lstModInfo[i]);

			//1st module section field to write - module type string (10 bytes)
			if(!writer.WriteString(section.m_strCode, 10)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//2nd module section field to write - module subtype string (10 bytes)
			if(!writer.WriteString(section.m_strSubCode, 10)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//3rd module section field to write - module name string (100 bytes)
			if(!writer.WriteString(section.m_strName, 100)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//4th module section field to write - report line #1 string (80 bytes)
			if(!writer.WriteString(section.m_strReportLine1, 80)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//5th module section field to write - report line #2 string (80 bytes)
			if(!writer.WriteString(section.m_strReportLine2, 80)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//6th field to write - custom solution ID number (4 bytes)
			if(!writer.WriteBytes(&section.m_nCustomSolutionID, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//7th module section field to write - MLIID string (16 bytes)
			if(!writer.WriteString(section.m_strMLIID, 18)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//8th field to write - licencing type short number (2 bytes)
			if(!writer.WriteBytes(&section.m_nLicensingType, 1)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//9th field to write - number of licencses (4 bytes)
			if(!writer.WriteBytes(&section.m_nNumLicenses, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//10th field to write - licencse platforms code short number (2 bytes)
			if(!writer.WriteBytes(&section.m_nLicensePlatforms, 2)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//11th field to write - licencse platforms subcode short number (2 bytes)
			if(!writer.WriteBytes(&section.m_nLicensePlatformsSubcode, 2)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//12th field to write - created date/time (4 bytes)
			time_t date = section.m_dateCreated.toTime_t();
			if(!writer.WriteBytes(&date, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}
		
			//13th field to write - modified date/time (4 bytes)
			date = section.m_dateModified.toTime_t();
			if(!writer.WriteBytes(&date, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//14th field to write - expired date/time (4 bytes)
			date = section.m_dateExpiration.toTime_t();
			if(!writer.WriteBytes(&date, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//15th field to write - expiration warning date/time (4 bytes)
			date = section.m_dateWarning.toTime_t();
			if(!writer.WriteBytes(&date, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//16th field to write - use licence expiration data - boolean (1 byte)
			if(!writer.WriteBytes(&section.m_bUseExpireDate, 1)){
				fclose(pKeyFile);
				return false;		// field not found
			}
		
			//17th field to write - 'modified by' string (5 bytes)
			if(!writer.WriteString(section.m_strModifiedBy, 5)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//18th field to write - serial number string (50 bytes)
			if(!writer.WriteString(section.m_strSerialNumber, 50)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//19th field to write - encrypted blob (256 bytes)
			// Encrypt combination of fields: num. licences + Module Type + Subtype + MLIID
			// unencrypted size = 42 bytes => encrypted size = 256 bytes (1 block size of the RSA key)
			memset(szBuffer, 0, 1024);
			memcpy(szBuffer, &section.m_nNumLicenses, 4);
			std::string strData = section.m_strCode.toLatin1().constData();
			memcpy(szBuffer+4, strData.c_str(), min(10, strlen(strData.c_str())));
			strData = section.m_strSubCode.toLatin1().constData();
			memcpy(szBuffer+14, strData.c_str(), min(10, strlen(strData.c_str())));
			strData = section.m_strMLIID.toLatin1().constData();
			memcpy(szBuffer+24, strData.c_str(), min(18, strlen(strData.c_str())));

			char szEncBuffer[1024];
			RSA_Key key;
			key.ReadPrivateKeyFromBuffer(szPrivKey, strlen(szPrivKey));
			int nSize = key.Sign(szBuffer, 42, szEncBuffer);
			Q_ASSERT(nSize == 256);	// 1 RSA block

			if(!writer.WriteBytes(szEncBuffer, 256)){
				fclose(pKeyFile);
				return false;		// field not found
			}
			section.m_arEncBlob = QByteArray(szEncBuffer, 256);	//refresh blob
		
			//20th field to write - number of FP sections (4 bytes)
			section.AR_RefreshFlatListFromTree();	//refresh data before using it
			int nNumFPSections = section.m_lstAR.size();
			if(!writer.WriteBytes(&nNumFPSections, 4)){
				fclose(pKeyFile);
				return false;		// field not found
			}

			//
			// Now write FP sections for a single module
			//
			for(int j=0; j<nNumFPSections; j++)
			{
				FP_ITEM1 &item(section.m_lstAR[j]);

				//qDebug() << item.nCode;

				//1st FP field to write - code number (4 bytes)
				if(!writer.WriteBytes(&item.nCode, 4)){
					fclose(pKeyFile);
					return false;		// field not found
				}
				
				//2nd FP field to write - code number (1 byte)
				if(!writer.WriteBytes(&item.bCanUse, 1)){
					fclose(pKeyFile);
					return false;		// field not found
				}
				
				//3rd FP field to write - value number (4 bytes)
				if(!writer.WriteBytes(&item.nValue, 4)){
					fclose(pKeyFile);
					return false;		// field not found
				}
			}
		}

		writer.Write();
		fclose(pKeyFile);
		return true;
	}
	
	return false;
}


#ifdef _DEBUG
void LicenseInfo::Dump()
{
	qDebug() << "Customer: " << m_strCustomerID;

	int nCnt = m_lstModInfo.size();
	for(int i=0; i<nCnt; i++){
		m_lstModInfo[i].Dump();
	}
}
#endif

#ifdef _DEBUG
void LicenseInfo::AssertValid()
{
	m_fpdf.AssertValid();

	int nCnt = m_lstModInfo.size();
	for(int i=0; i<nCnt; i++){
		m_lstModInfo[i].AssertValid();
	}
}
#endif