#ifndef LICENSESTREAMREADER_H
#define LICENSESTREAMREADER_H

#include "common/common/rsakey.h"

class LicenseStreamReader
{
public:
	LicenseStreamReader();

	void AttachSource(FILE *pInFile);
	void SetPublicKey(const char *szKey);

	bool ReadBytes(void *pszBuffer, int nLen);
	bool ReadString(char *pszBuffer, int nLen);

protected:
	bool Read();

protected:
	FILE *m_pInFile;
	char m_szDecryptedData[4000];
	int	 m_nDataLen;
	RSA_Key	m_key;
};

#endif	// LICENSESTREAMREADER_H
