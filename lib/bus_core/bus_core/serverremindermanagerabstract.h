#ifndef SERVERREMINDERMANAGERABSTRACT_H
#define SERVERREMINDERMANAGERABSTRACT_H

#include "common/common/dbrecordset.h"
#include "common/common/status.h"

class ServerReminderManagerAbstract 
{
public:

	virtual ~ServerReminderManagerAbstract(){};

	virtual void		CreateReminder(Status &Ret_pStatus,QString strSubject, QDateTime datDue,int nCategory, int nOwnerID, int nRecvContactID,int nTableID=-1, int nRecordID=-1, QString strData="",bool bOneShoot=false)=0;
	virtual void		GetAllActiveRemindersForUser(Status &Ret_pStatus,int nPersonID,DbRecordSet &RetReminders)=0;
	virtual void		DeleteReminders(Status &Ret_pStatus,DbRecordSet &lstReminders)=0;
	virtual void		PostPoneReminders(Status &Ret_pStatus,DbRecordSet &lstReminders, int nMinutes)=0;
	virtual void		RebuildReminderList()=0;
	virtual void		ReminderCleanUp()=0;
	virtual int			GetNextSpawnTimeSeconds()=0;
	virtual QDateTime	GetNextSpawnTimeDateTime()=0;

};


#endif // SERVERREMINDERMANAGERABSTRACT_H
