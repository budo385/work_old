#ifndef FORMATADDRESS_H
#define FORMATADDRESS_H

#include <QHash>
#include <QString>
#include "common/common/dbrecordset.h"


typedef QHash<QString, QString> HshAdrFields;	
typedef QHashIterator<QString,QString> HshAdrFieldsIterator;


/*!
	\class FormatAddress
	\brief Use for formatting address
	\ingroup Bus_Core

	Address is formatted from fields with given format schema.
	Address can be parsed using schema.
*/

class FormatAddress 
{


public:

	enum FormatOps
	{
		FORMAT_USING_USERDEFAULTS,					//reads schemas from user defaults
		FORMAT_USING_STORED_FORMATSCHEMA,			//format schema using BCMA_FORMATSCHEMA_ID inside list of addresses
		FORMAT_USING_HARDCODE_DEFAULTSCHEMA			//format schema using hardcoded schema
	};

    FormatAddress();
    ~FormatAddress();

	void AddressFormat(DbRecordSet &lstAddress,QString strSchemaDB,bool bSkipFormattingIfExists=false);
	void AddressFormat(DbRecordSet &lstAddress,QStringList lstSchemaDB,bool bSkipFormattingIfExists=false);
	void AddressParse(DbRecordSet &lstAddress,QString strSchemaDB);
	
	void GetAddressFieldLst(HshAdrFields &hshAdrFields);
	QString MapSchemaToDisplay(QString strSchemaDB);
	QString MapDisplayToSchema(QString strSchemaDisplay);

    
	HshAdrFields m_hshAdrFields;
	DbRecordSet m_lstAdrFields;   //same as above, just names
	QString m_strDefaultSchema;

private:
	void FormatAddressRow(DbRecordSet &lstAddress, int z, QString strSchema, int nFormatAddrIdx,bool bSkipFormattingIfExists);
	bool ParseSchema(QString strSchema, DbRecordSet &lstParsed);

};

#endif // FORMATADDRESS_H
