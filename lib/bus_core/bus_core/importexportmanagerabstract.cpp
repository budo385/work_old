#include "importexportmanagerabstract.h"
#include "trans/trans/ftpclient.h"
#include <QCoreApplication>
#include "common/common/loggerabstract.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include <QStringList>
#include <QDir>


#define MUTEX_LOCK_TIMEOUT 20000


ImportExportManagerAbstract::ImportExportManagerAbstract()
:m_RWMutex(QReadWriteLock::Recursive)
{
	m_bFileWatcherEnabled=true;
	m_pBusinessSet=NULL;
	m_nPeriodProject=0;
	m_nPeriodUser=0;
	m_nPeriodDebtor=0;
	m_nPeriodContact=0;
	m_nPeriodStoredProjectList=0;

}

ImportExportManagerAbstract::~ImportExportManagerAbstract()
{

}

void ImportExportManagerAbstract::SetBusinessServiceSet(Status &pStatus, Interface_BusinessServiceSet *pBusinessSet)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return;
	}
	m_pBusinessSet=pBusinessSet;
	m_RWMutex.unlock();
}

void ImportExportManagerAbstract::Client_SaveSettings(Status &pStatus, DbRecordSet lstSettings)
{
	SaveSettings(pStatus,lstSettings);
	if (!pStatus.IsOK())
		return;
	ReloadSettings(pStatus);
}


bool ImportExportManagerAbstract::ReloadSettings(Status &pStatus)
{
	pStatus.setError(0);
	int nPeriodOld=GetGlobalPeriod(pStatus);
	if (!pStatus.IsOK())
		return false;

	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return false;
	}

	disconnect(&m_FileWatcher,SIGNAL(directoryChanged( const QString & )),this,SLOT(OnDirectoryChange(const QString & )));

	ReadSettings(pStatus,m_Settings);
	if (!pStatus.IsOK())
	{
		m_RWMutex.unlock();
		return false;
	}

	if (m_Settings.getRowCount()!=1)
	{
		m_RWMutex.unlock();
		return false;
	}

	if (m_bFileWatcherEnabled)
	{
		//reset file watcher:
		QStringList lstFiles=m_FileWatcher.files();
		if (lstFiles.size()>0)
			m_FileWatcher.removePaths(lstFiles);

		//connect if needed:
		if (m_Settings.getDataRef(0,"CIE_SPLST_SCAN_TYPE").toInt()==1)
			m_FileWatcher.addPath(m_Settings.getDataRef(0,"CIE_SPLST_SOURCE_DIR").toString());
		if (m_Settings.getDataRef(0,"CIE_PROJ_SCAN_TYPE").toInt()==1)
			m_FileWatcher.addPath(m_Settings.getDataRef(0,"CIE_PROJ_SOURCE_DIR").toString());
		if (m_Settings.getDataRef(0,"CIE_USER_SCAN_TYPE").toInt()==1)
			m_FileWatcher.addPath(m_Settings.getDataRef(0,"CIE_USER_SOURCE_DIR").toString());
		if (m_Settings.getDataRef(0,"CIE_DEBT_SCAN_TYPE").toInt()==1)
			m_FileWatcher.addPath(m_Settings.getDataRef(0,"CIE_DEBT_SOURCE_DIR").toString());
		if (m_Settings.getDataRef(0,"CIE_CONT_SCAN_TYPE").toInt()==1)
			m_FileWatcher.addPath(m_Settings.getDataRef(0,"CIE_CONT_SOURCE_DIR").toString());

		connect(&m_FileWatcher,SIGNAL(directoryChanged( const QString & )),this,SLOT(OnDirectoryChange(const QString & )));
	}

	m_RWMutex.unlock();

	//if period changed, emit signal
	int nPeriod=GetGlobalPeriod(pStatus);
	if (!pStatus.IsOK())
		return false;
	if (nPeriodOld!=nPeriod)
	{
		emit GlobalPeriodChanged(nPeriod);
	}
	return true;
}


//if 0 then not needed, return period in minutes!
int ImportExportManagerAbstract::GetGlobalPeriod(Status &pStatus)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return 0;
	}

	if (m_Settings.getRowCount()!=1)
	{
		m_RWMutex.unlock();
		return 0;
	}


	int nPeriod=0;
	bool bStartSync=false;
	if (m_Settings.getDataRef(0,"CIE_PROJ_ENABLED").toInt()!=0)
		bStartSync=true;
	if (m_Settings.getDataRef(0,"CIE_USER_ENABLED").toInt()!=0)
		bStartSync=true;
	if (m_Settings.getDataRef(0,"CIE_DEBT_ENABLED").toInt()!=0)
		bStartSync=true;
	if (m_Settings.getDataRef(0,"CIE_CONT_ENABLED").toInt()!=0)
		bStartSync=true;
	if (m_Settings.getDataRef(0,"CIE_SPLST_ENABLED").toInt()!=0)
		bStartSync=true;

	if (!bStartSync)
	{
		m_RWMutex.unlock();
		return 0;
	}
	else
	{
		//check min & max value:
		int nPeriod= m_Settings.getDataRef(0,"CIE_GLOBAL_SCAN_PERIOD").toInt();
		m_RWMutex.unlock();
		if (nPeriod<5)
			return 5;
		if ( nPeriod>1440)
			return 1440;

		return nPeriod;
	}

	m_RWMutex.unlock();
	return 0;

}

//delayed action:
void ImportExportManagerAbstract::OnDirectoryChange(const QString & path)
{
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return;
	}

	if (m_Settings.getRowCount()!=1)
	{
		m_RWMutex.unlock();
		return;
	}

	Status err;

	if (path==m_Settings.getDataRef(0,"CIE_PROJ_SOURCE_DIR").toString() && m_Settings.getDataRef(0,"CIE_PROJ_SCAN_TYPE").toInt()==1)
	{
		emit SignalStartTask(-1,PROCESS_TYPE_PROJECT);
		//StartProcessProject(err);
		//m_RWMutex.unlock();
		//return;
	}
	if (path==m_Settings.getDataRef(0,"CIE_USER_SOURCE_DIR").toString() && m_Settings.getDataRef(0,"CIE_USER_SCAN_TYPE").toInt()==1)
	{
		emit SignalStartTask(-1,PROCESS_TYPE_USER);
		//StartProcessUser(err);
		//m_RWMutex.unlock();
		//return;
	}
	if (path==m_Settings.getDataRef(0,"CIE_DEBT_SOURCE_DIR").toString() && m_Settings.getDataRef(0,"CIE_DEBT_SCAN_TYPE").toInt()==1)
	{
		emit SignalStartTask(-1,PROCESS_TYPE_DEBTOR);
		//StartProcessDebtor(err);
		//m_RWMutex.unlock();
		//return;
	}
	if (path==m_Settings.getDataRef(0,"CIE_CONT_SOURCE_DIR").toString() && m_Settings.getDataRef(0,"CIE_CONT_SCAN_TYPE").toInt()==1)
	{
		emit SignalStartTask(-1,PROCESS_TYPE_CONTACT);
		//StartProcessContact(err);
		//m_RWMutex.unlock();
		//return;
	}
	if (path==m_Settings.getDataRef(0,"CIE_SPLST_SOURCE_DIR").toString() && m_Settings.getDataRef(0,"CIE_SPLST_SCAN_TYPE").toInt()==1)
	{
		emit SignalStartTask(-1,PROCESS_TYPE_SPL);
		//StartProcessContact(err);
		//m_RWMutex.unlock();
		//return;
	}

	m_RWMutex.unlock();

}

void ImportExportManagerAbstract::StartAllScheduledTasks(Status &pStatus, QString strLang)
{
	DbRecordSet recSettings;
	pStatus.setError(0);
	GetSettings(pStatus,recSettings);
	if (!pStatus.IsOK())
		 return;
	if (recSettings.getRowCount()!=1)
		return;
	int	nPeriodProject=m_nPeriodProject;
	int	nPeriodUser=m_nPeriodUser;
	int	nPeriodDebtor=m_nPeriodDebtor;
	int	nPeriodContact=m_nPeriodContact;
	int	nPeriodStoredProjectList=m_nPeriodStoredProjectList;

	

	//if enabled execute:
		if (recSettings.getDataRef(0,"CIE_SPLST_ENABLED").toInt()!=0 && recSettings.getDataRef(0,"CIE_SPLST_SCAN_TYPE").toInt()==0)
		{
			nPeriodStoredProjectList++;
			if (nPeriodStoredProjectList>=recSettings.getDataRef(0,"CIE_SPLST_SCAN_PERIOD").toInt())
			{
				StartProcessStoredProjectList(pStatus);
				nPeriodStoredProjectList=0;
			}
		}
		if (recSettings.getDataRef(0,"CIE_PROJ_ENABLED").toInt()!=0 && recSettings.getDataRef(0,"CIE_PROJ_SCAN_TYPE").toInt()==0)
		{
			nPeriodProject++;
			if (nPeriodProject>=recSettings.getDataRef(0,"CIE_PROJ_SCAN_PERIOD").toInt())
			{
				StartProcessProject(pStatus);
				nPeriodProject=0;
			}
		}
		if (recSettings.getDataRef(0,"CIE_USER_ENABLED").toInt()!=0 && recSettings.getDataRef(0,"CIE_USER_SCAN_TYPE").toInt()==0)
		{
			nPeriodUser++;
			if (nPeriodUser>=recSettings.getDataRef(0,"CIE_USER_SCAN_PERIOD").toInt())
			{
				StartProcessUser(pStatus);
				nPeriodUser=0;
			}
		}
		if (recSettings.getDataRef(0,"CIE_DEBT_ENABLED").toInt()!=0 && recSettings.getDataRef(0,"CIE_DEBT_SCAN_TYPE").toInt()==0)
		{
			nPeriodDebtor++;
			if (nPeriodDebtor>=recSettings.getDataRef(0,"CIE_DEBT_SCAN_PERIOD").toInt())
			{
				StartProcessDebtor(pStatus);
				nPeriodDebtor=0;
			}
		}
		if (recSettings.getDataRef(0,"CIE_CONT_ENABLED").toInt()!=0 && recSettings.getDataRef(0,"CIE_CONT_SCAN_TYPE").toInt()==0)
		{
			nPeriodContact++;
			if (nPeriodContact>=recSettings.getDataRef(0,"CIE_CONT_SCAN_PERIOD").toInt())
			{
				StartProcessContact(pStatus, strLang);
				nPeriodContact=0;
			}
		}

		if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
			return;
		}
		m_nPeriodProject=nPeriodProject;
		m_nPeriodUser=nPeriodUser;
		m_nPeriodDebtor=nPeriodDebtor;
		m_nPeriodContact=nPeriodContact;
		m_nPeriodStoredProjectList=nPeriodStoredProjectList;
		m_RWMutex.unlock();
}

void ImportExportManagerAbstract::Client_StartTaskDelayed(Status &pStatus,int nTaskID)
{
	pStatus.setError(0);
	emit SignalStartTask(-1,nTaskID);
}

void ImportExportManagerAbstract::GetSettings(Status &pStatus,DbRecordSet &settings)
{
	pStatus.setError(0);
	if(!m_RWMutex.tryLockForRead(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return;
	}
	settings=m_Settings;
	m_RWMutex.unlock();
}


void ImportExportManagerAbstract::StartProcessProject(Status &pStatus)
{
	if(!m_mtxProjects.tryLock(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return;
	}

	DbRecordSet lstErros;
	DbRecordSet lstFiles;

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_PROJECT_START);
	ProcessProjects(pStatus,lstFiles,lstErros);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		m_mtxProjects.unlock();
		return;
	}
	if (!pStatus.IsOK())
	{
		//save status:
		m_Settings.setData(0,"CIE_PROJ_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_PROJ_STATUS_ERROR",pStatus.getErrorText());
		m_Settings.setData(0,"CIE_PROJ_STATUS_LAST_FILE","");
		m_Settings.setData(0,"CIE_PROJ_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_PROJ_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_PROJECT_FAILED,pStatus.getErrorText());
		m_RWMutex.unlock();
		m_mtxProjects.unlock();
		return;
	}
	if (lstFiles.getRowCount()==0)
	{
		//save status:
		m_Settings.setData(0,"CIE_PROJ_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_PROJ_STATUS_ERROR",tr("No files found for processing"));
		//m_Settings.setData(0,"CIE_PROJ_STATUS_LAST_FILE",""); //last success file preserved!
		m_Settings.setData(0,"CIE_PROJ_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_PROJ_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_PROJECT_NO_PROCESS);
		m_RWMutex.unlock();
		m_mtxProjects.unlock();
		return;
	}
	
	//process errors:
	int nSize=lstErros.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strMsg=lstErros.getDataRef(i,1).toString()+";"+lstErros.getDataRef(i,0).toString()+";"+lstErros.getDataRef(i,3).toString();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_PROJECT_RECORD_FAILED,strMsg);
	}

	//process files:
	bool bError=false;
	int nProcessed=0, nFailed=0;
	QString strLastFile;
	nSize=lstFiles.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.getDataRef(i,1).toInt()==0)
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,3).toString()+";"+lstFiles.getDataRef(i,4).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_PROJECT_PROCESS_FILE_SUCCESS,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=lstFiles.getDataRef(i,3).toInt();
			nFailed=lstFiles.getDataRef(i,4).toInt();
			if (nFailed>0)
				bError=true;
		}
		else
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,2).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_PROJECT_PROCESS_FILE_FAILED,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=0;
			nFailed=0;
			bError=true;
		}
	}

	//save status:
	m_Settings.setData(0,"CIE_PROJ_STATUS_DATE",QDateTime::currentDateTime());
	if (bError)
		m_Settings.setData(0,"CIE_PROJ_STATUS_ERROR",tr("Some records/files failed, see log for details!"));
	else
		m_Settings.setData(0,"CIE_PROJ_STATUS_ERROR","OK");
	m_Settings.setData(0,"CIE_PROJ_STATUS_LAST_FILE",strLastFile);
	m_Settings.setData(0,"CIE_PROJ_STATUS_REC_OK",nProcessed);
	m_Settings.setData(0,"CIE_PROJ_STATUS_REC_FAIL",nFailed);
	Status err;
	SaveSettings(err,m_Settings);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_PROJECT_END);

	m_RWMutex.unlock();
	m_mtxProjects.unlock();
}




void ImportExportManagerAbstract::StartProcessStoredProjectList(Status &pStatus)
{
	if(!m_mtxSPL.tryLock(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return;
	}

	DbRecordSet lstErros;
	DbRecordSet lstFiles;

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_SPL_START);
	ProcessStoredProjectLists(pStatus,lstFiles,lstErros);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		m_mtxSPL.unlock();
		return;
	}
	if (!pStatus.IsOK())
	{
		
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_BUS_IMPORT_SKIPPED) //B.T. skip that do not write:
		{
			pStatus.setError(0);
			m_RWMutex.unlock();
			m_mtxSPL.unlock();
			return;
		}
		//save status:
		m_Settings.setData(0,"CIE_SPLST_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_SPLST_STATUS_ERROR",pStatus.getErrorText());
		m_Settings.setData(0,"CIE_SPLST_STATUS_LAST_FILE","");
		m_Settings.setData(0,"CIE_SPLST_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_SPLST_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_SPL_FAILED,pStatus.getErrorText());
		m_RWMutex.unlock();
		m_mtxSPL.unlock();
		return;
	}
	if (lstFiles.getRowCount()==0)
	{
		//save status:
		m_Settings.setData(0,"CIE_SPLST_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_SPLST_STATUS_ERROR",tr("No files found for processing"));
		//m_Settings.setData(0,"CIE_SPLST_STATUS_LAST_FILE",""); //last success file preserved!
		m_Settings.setData(0,"CIE_SPLST_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_SPLST_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_SPL_NO_PROCESS);
		m_RWMutex.unlock();
		m_mtxSPL.unlock();
		return;
	}

	//process errors:
	int nSize=lstErros.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strMsg=lstErros.getDataRef(i,1).toString()+";"+lstErros.getDataRef(i,0).toString()+";"+lstErros.getDataRef(i,3).toString();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_SPL_RECORD_FAILED,strMsg);
	}

	//process files:
	bool bError=false;
	int nProcessed=0, nFailed=0;
	QString strLastFile;
	nSize=lstFiles.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.getDataRef(i,1).toInt()==0)
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,3).toString()+";"+lstFiles.getDataRef(i,4).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_SPL_PROCESS_FILE_SUCCESS,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=lstFiles.getDataRef(i,3).toInt();
			nFailed=lstFiles.getDataRef(i,4).toInt();
			if (nFailed>0)
				bError=true;
		}
		else
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,2).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_SPL_PROCESS_FILE_FAILED,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=0;
			nFailed=0;
			bError=true;
		}
	}

	//save status:
	m_Settings.setData(0,"CIE_SPLST_STATUS_DATE",QDateTime::currentDateTime());
	if (bError)
		m_Settings.setData(0,"CIE_SPLST_STATUS_ERROR",tr("Some records/files failed, see log for details!"));
	else
		m_Settings.setData(0,"CIE_SPLST_STATUS_ERROR","OK");
	m_Settings.setData(0,"CIE_SPLST_STATUS_LAST_FILE",strLastFile);
	m_Settings.setData(0,"CIE_SPLST_STATUS_REC_OK",nProcessed);
	m_Settings.setData(0,"CIE_SPLST_STATUS_REC_FAIL",nFailed);
	Status err;
	SaveSettings(err,m_Settings);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_SPL_END);

	m_RWMutex.unlock();
	m_mtxSPL.unlock();
}



void ImportExportManagerAbstract::StartProcessUser(Status &pStatus)
{
	if(!m_mtxUsers.tryLock(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return;
	}


	DbRecordSet lstErros;
	DbRecordSet lstFiles;


	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_USER_START);
	ProcessUsers(pStatus,lstFiles,lstErros);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		m_mtxUsers.unlock();
		return;
	}
	if (!pStatus.IsOK())
	{
		//save status:
		m_Settings.setData(0,"CIE_USER_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_USER_STATUS_ERROR",pStatus.getErrorText());
		m_Settings.setData(0,"CIE_USER_STATUS_LAST_FILE","");
		m_Settings.setData(0,"CIE_USER_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_USER_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_USER_FAILED,pStatus.getErrorText());
		m_RWMutex.unlock();
		m_mtxUsers.unlock();
		return;
	}

	if (lstFiles.getRowCount()==0)
	{
		//save status:
		m_Settings.setData(0,"CIE_USER_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_USER_STATUS_ERROR",tr("No files found for processing"));
		//m_Settings.setData(0,"CIE_USER_STATUS_LAST_FILE","");
		m_Settings.setData(0,"CIE_USER_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_USER_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_USER_NO_PROCESS);
		m_RWMutex.unlock();
		m_mtxUsers.unlock();
		return;
	}

	//process errors:
	int nSize=lstErros.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strMsg=lstErros.getDataRef(i,1).toString()+";"+lstErros.getDataRef(i,0).toString()+";"+lstErros.getDataRef(i,3).toString();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_USER_RECORD_FAILED,strMsg);
	}

	//process files:
	bool bError=false;
	int nProcessed=0, nFailed=0;
	QString strLastFile;
	nSize=lstFiles.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.getDataRef(i,1).toInt()==0)
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,3).toString()+";"+lstFiles.getDataRef(i,4).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_USER_PROCESS_FILE_SUCCESS,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=lstFiles.getDataRef(i,3).toInt();
			nFailed=lstFiles.getDataRef(i,4).toInt();
			if (nFailed>0)
				bError=true;
		}
		else
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,2).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_USER_PROCESS_FILE_FAILED,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=0;
			nFailed=0;
			bError=true;
		}
	}

	//save status:
	m_Settings.setData(0,"CIE_USER_STATUS_DATE",QDateTime::currentDateTime());
	if (bError)
		m_Settings.setData(0,"CIE_PROJ_STATUS_ERROR",tr("Some records/files failed, see log for details!"));
	else
		m_Settings.setData(0,"CIE_USER_STATUS_ERROR","OK");
	m_Settings.setData(0,"CIE_USER_STATUS_LAST_FILE",strLastFile);
	m_Settings.setData(0,"CIE_USER_STATUS_REC_OK",nProcessed);
	m_Settings.setData(0,"CIE_USER_STATUS_REC_FAIL",nFailed);
	Status err;
	SaveSettings(err,m_Settings);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_USER_END);
	m_RWMutex.unlock();
	m_mtxUsers.unlock();
}

void ImportExportManagerAbstract::StartProcessDebtor(Status &pStatus)
{
	if(!m_mtxDebtors.tryLock(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return;
	}


	DbRecordSet lstErros;
	DbRecordSet lstFiles;

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_DEBTOR_START);
	ProcessDebtors(pStatus,lstFiles,lstErros);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		m_mtxDebtors.unlock();
		return;
	}
	if (!pStatus.IsOK())
	{
		//save status:
		m_Settings.setData(0,"CIE_DEBT_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_DEBT_STATUS_ERROR",pStatus.getErrorText());
		m_Settings.setData(0,"CIE_DEBT_STATUS_LAST_FILE","");
		m_Settings.setData(0,"CIE_DEBT_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_DEBT_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_DEBTOR_FAILED,pStatus.getErrorText());
		m_RWMutex.unlock();
		m_mtxDebtors.unlock();
		return;
	}

	if (lstFiles.getRowCount()==0)
	{
		//save status:
		m_Settings.setData(0,"CIE_DEBT_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_DEBT_STATUS_ERROR",tr("No files found for processing"));
		//m_Settings.setData(0,"CIE_DEBT_STATUS_LAST_FILE","");
		m_Settings.setData(0,"CIE_DEBT_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_DEBT_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_DEBTOR_NO_PROCESS);
		m_RWMutex.unlock();
		m_mtxDebtors.unlock();
		return;
	}

	//process errors:
	int nSize=lstErros.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strMsg=lstErros.getDataRef(i,1).toString()+";"+lstErros.getDataRef(i,0).toString()+";"+lstErros.getDataRef(i,3).toString();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_DEBTOR_RECORD_FAILED,strMsg);
	}

	//process files:
	bool bError=false;
	int nProcessed=0, nFailed=0;
	QString strLastFile;
	nSize=lstFiles.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.getDataRef(i,1).toInt()==0)
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,3).toString()+";"+lstFiles.getDataRef(i,4).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_DEBTOR_PROCESS_FILE_SUCCESS,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=lstFiles.getDataRef(i,3).toInt();
			nFailed=lstFiles.getDataRef(i,4).toInt();
			if (nFailed>0)
				bError=true;
		}
		else
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,2).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_DEBTOR_PROCESS_FILE_FAILED,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=0;
			nFailed=0;
			bError=true;
		}
	}

	//save status:
	m_Settings.setData(0,"CIE_DEBT_STATUS_DATE",QDateTime::currentDateTime());
	if (bError)
		m_Settings.setData(0,"CIE_PROJ_STATUS_ERROR",tr("Some records/files failed, see log for details!"));
	else
		m_Settings.setData(0,"CIE_DEBT_STATUS_ERROR","OK");

	m_Settings.setData(0,"CIE_DEBT_STATUS_LAST_FILE",strLastFile);
	m_Settings.setData(0,"CIE_DEBT_STATUS_REC_OK",nProcessed);
	m_Settings.setData(0,"CIE_DEBT_STATUS_REC_FAIL",nFailed);
	Status err;
	SaveSettings(err,m_Settings);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_DEBTOR_END);
	m_RWMutex.unlock();
	m_mtxDebtors.unlock();
}


void ImportExportManagerAbstract::StartProcessContact(Status &pStatus, QString strLang)
{
	if(!m_mtxContacs.tryLock(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		return;
	}


	DbRecordSet lstErros;
	DbRecordSet lstFiles;

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_CONTACT_START);
	ProcessContacts(pStatus,lstFiles,lstErros,strLang);
	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_MANAGER_BUSY);
		m_mtxContacs.unlock();
		return;
	}
	if (!pStatus.IsOK())
	{
		//save status:
		m_Settings.setData(0,"CIE_CONT_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_CONT_STATUS_ERROR",pStatus.getErrorText());
		m_Settings.setData(0,"CIE_CONT_STATUS_LAST_FILE","");
		m_Settings.setData(0,"CIE_CONT_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_CONT_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_CONTACT_FAILED,pStatus.getErrorText());
		m_RWMutex.unlock();
		m_mtxContacs.unlock();
		return;
	}

	if (lstFiles.getRowCount()==0)
	{
		//save status:
		m_Settings.setData(0,"CIE_CONT_STATUS_DATE",QDateTime::currentDateTime());
		m_Settings.setData(0,"CIE_CONT_STATUS_ERROR",tr("No files found for processing"));
		//m_Settings.setData(0,"CIE_CONT_STATUS_LAST_FILE","");
		m_Settings.setData(0,"CIE_CONT_STATUS_REC_OK",0);
		m_Settings.setData(0,"CIE_CONT_STATUS_REC_FAIL",0);
		Status err;
		SaveSettings(err,m_Settings);
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_CONTACT_NO_PROCESS);
		m_RWMutex.unlock();
		m_mtxContacs.unlock();
		return;
	}

	//process errors:
	int nSize=lstErros.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strMsg=lstErros.getDataRef(i,1).toString()+";"+lstErros.getDataRef(i,0).toString()+";"+lstErros.getDataRef(i,3).toString();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_CONTACT_RECORD_FAILED,strMsg);
	}

	//process files:
	bool bError=false;
	int nProcessed=0, nFailed=0;
	QString strLastFile;
	nSize=lstFiles.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.getDataRef(i,1).toInt()==0)
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,3).toString()+";"+lstFiles.getDataRef(i,4).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_CONTACT_PROCESS_FILE_SUCCESS,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=lstFiles.getDataRef(i,3).toInt();
			nFailed=lstFiles.getDataRef(i,4).toInt();
			if (nFailed>0)
				bError=true;
		}
		else
		{
			QString strMsg=lstFiles.getDataRef(i,0).toString()+";"+lstFiles.getDataRef(i,2).toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_IMP_EXP_CONTACT_PROCESS_FILE_FAILED,strMsg);
			strLastFile=lstFiles.getDataRef(i,0).toString();
			nProcessed=0;
			nFailed=0;
			bError=true;
		}
	}

	//save status:
	m_Settings.setData(0,"CIE_CONT_STATUS_DATE",QDateTime::currentDateTime());
	if (bError)
		m_Settings.setData(0,"CIE_PROJ_STATUS_ERROR",tr("Some records/files failed, see log for details!"));
	else
		m_Settings.setData(0,"CIE_CONT_STATUS_ERROR","OK");
	m_Settings.setData(0,"CIE_CONT_STATUS_LAST_FILE",strLastFile);
	m_Settings.setData(0,"CIE_CONT_STATUS_REC_OK",nProcessed);
	m_Settings.setData(0,"CIE_CONT_STATUS_REC_FAIL",nFailed);
	Status err;
	SaveSettings(err,m_Settings);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_IMP_EXP_CONTACT_END);
	m_RWMutex.unlock();
	m_mtxContacs.unlock();
}




//FTP issue:
//create temp dir and delete all..(must be executed on mutex..on one server...)
void ImportExportManagerAbstract::PreProcessExportFTP(Status &err,QString strLocalTempDir)
{
	err.setError(0);

	//create dir if not already:
	QDir dir(QCoreApplication::applicationDirPath());
	if(!dir.exists(strLocalTempDir))
	{
		if(!dir.mkdir(strLocalTempDir))
		{
			err.setError(StatusCodeSet::ERR_BUS_FTP_EI_FAIL_CREATE_TEMP_DIR,QDir::toNativeSeparators(QCoreApplication::applicationDirPath()+"/"+strLocalTempDir));
			return;
		}
	}

	//change dir:
	if(!dir.cd(strLocalTempDir))
	{	
		err.setError(StatusCodeSet::ERR_BUS_FTP_EI_FAIL_CHANGE_TO_TEMP_DIR,QDir::toNativeSeparators(QCoreApplication::applicationDirPath()+"/"+strLocalTempDir));
		return;
	}


	//get all files:
	QStringList lstFiles=dir.entryList(QDir::Files);
	int nSize=lstFiles.count();
	for(int i=0;i<nSize;++i)
	{
		if (!dir.remove(lstFiles.at(i)))
		{
			err.setError(StatusCodeSet::ERR_BUS_FTP_EI_FAIL_DELETE_FILE,QDir::toNativeSeparators(dir.absoluteFilePath(lstFiles.at(i))));
			return;
		}
	}
}

//prob only 1 file is in Ret_ProcessedFiles...upload it
void ImportExportManagerAbstract::PostProcessExportFTP(Status &err,QString strLocalTempDir,FTPConnectionSettings FTPconn, QString strFTPDirectory,DbRecordSet &Ret_ProcessedFiles)
{
	FTPClient client;
	client.Connect(err,FTPconn);
	if (!err.IsOK())return;

	if (!strFTPDirectory.isEmpty())
	{
		client.Dir_Change(err,strFTPDirectory);
		if (!err.IsOK())return;
	}

	//change dir:
	QDir dir(QCoreApplication::applicationDirPath());
	if(!dir.cd(strLocalTempDir))
	{	
		err.setError(StatusCodeSet::ERR_BUS_FTP_EI_FAIL_CHANGE_TO_TEMP_DIR,QDir::toNativeSeparators(QCoreApplication::applicationDirPath()+"/"+strLocalTempDir));
		return;
	}


	//-----------------------File put-----------------------------//
	int nSize=Ret_ProcessedFiles.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (Ret_ProcessedFiles.getDataRef(i,"FileOK").toInt()!=0)
			continue;

		QString strFile=Ret_ProcessedFiles.getDataRef(i,"FileName").toString();
		client.File_Put(err,strFile);
		if (!err.IsOK())return;
	}

	client.Disconnect(err);
}


//download all files from ftp dir using filter, prepare dir before
//returns no of files for processing, if 0 skip all
int ImportExportManagerAbstract::PreProcessImportFTP(Status &err,QString strLocalTempDir,FTPConnectionSettings FTPconn, QString strFTPDirectory,QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed)
{
	//prepare dir
	PreProcessExportFTP(err,strLocalTempDir);
	if (!err.IsOK())return 0;

	FTPClient client;
	client.Connect(err,FTPconn);
	if (!err.IsOK())return 0;

	if (!strFTPDirectory.isEmpty())
	{
		client.Dir_Change(err,strFTPDirectory);
		if (!err.IsOK())return 0;
	}

	//change dir:
	QDir dir(QCoreApplication::applicationDirPath());
	if(!dir.cd(strLocalTempDir))
	{	
		err.setError(StatusCodeSet::ERR_BUS_FTP_EI_FAIL_CHANGE_TO_TEMP_DIR,QDir::toNativeSeparators(QCoreApplication::applicationDirPath()+"/"+strLocalTempDir));
		return 0;
	}



	//get list of files on ftp:
	QStringList lstFilesToDownLoad;
	QList<QUrlInfo> lstDirContent;
	client.Dir_List(err,lstDirContent);
	if (!err.IsOK())return 0;

	int nSize=lstDirContent.count();
	for(int i=0;i<nSize;++i)
	{
		if (lstDirContent.at(i).isFile())
		{
			QString strFile=lstDirContent.at(i).name();
			if (!strFilePrefix.isEmpty())
				if(strFile.indexOf(strFilePrefix)!=0) 
					continue;

			if (!strFileExt.isEmpty()) //if extension is not at end:
				if(strFile.lastIndexOf(strFileExt)!=(strFile.length()-strFileExt.length())) 
					continue;

			if (!strFileExt.isEmpty()) //if processed extension is at end:
				if(strFile.lastIndexOf(strFileExtOfProcessed)==(strFile.length()-strFileExtOfProcessed.length())) 
					continue;

			lstFilesToDownLoad.append(strFile);
		}
	}


	//download them:
	int nSize2=lstFilesToDownLoad.count();
	for(int i=0;i<nSize2;++i)
	{
		client.File_Get(err,lstFilesToDownLoad.at(i),dir.absolutePath()+"/"+lstFilesToDownLoad.at(i));
		if (!err.IsOK())return 0;
	}

	client.Disconnect(err);

	return nSize2;
	
}

//after import change extensions on ftp
void ImportExportManagerAbstract::PostProcessImportFTP(Status &err,QString strLocalTempDir,FTPConnectionSettings FTPconn, QString strFTPDirectory, QString strFileExtOfProcessed,DbRecordSet &Ret_ProcessedFiles)
{
	FTPClient client;
	client.Connect(err,FTPconn);
	if (!err.IsOK())return;

	if (!strFTPDirectory.isEmpty())
	{
		client.Dir_Change(err,strFTPDirectory);
		if (!err.IsOK())return;
	}

	int nSize=Ret_ProcessedFiles.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (Ret_ProcessedFiles.getDataRef(i,"FileOK").toInt()!=0)
			continue;

		QString strOldFile=Ret_ProcessedFiles.getDataRef(i,"FileName").toString();
		//rename file - change extension to indicate file was processed
		QFileInfo info(strOldFile);
		strOldFile=info.fileName();
		QString strNewFile = info.baseName() + "." + strFileExtOfProcessed;

		if(strOldFile != strNewFile)
		{
			client.File_Remove(err,strNewFile);
			if (!err.IsOK())return;
			client.File_Rename(err,strOldFile,strNewFile);
			if (!err.IsOK())return;
		}
				
	}

	client.Disconnect(err);

}



void ImportExportManagerAbstract::ProcessProjects(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	if (!m_pBusinessSet)
	{
		Ret_pStatus.setError(1,"Business Set not available!");
		return;
	}

	DbRecordSet recSettings;
	GetSettings(Ret_pStatus,recSettings);
	if (!Ret_pStatus.IsOK())return;


	//check if local directory:
	if (recSettings.getDataRef(0,"CIE_PROJ_SOURCE")==0)
	{

		if (recSettings.getDataRef(0,"CIE_PROJ_IMPORT").toInt()>0)
		{
			m_pBusinessSet->BusImport->ProcessProjects_Import(Ret_pStatus,recSettings.getDataRef(0,"CIE_PROJ_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX_PROCESS").toString(),recSettings.getDataRef(0,"CIE_PROJ_SOURCE_DIR").toString(),Ret_ProcessedFiles,Ret_pErrors);
		}
		else
		{
			DbRecordSet ProjectsForExport;
			QByteArray dummy;
			int nLastN=0;
			if (recSettings.getDataRef(0,"CIE_PROJ_EXPORT_TYPE").toInt()==1)
				nLastN=recSettings.getDataRef(0,"CIE_PROJ_EXPORT_LAST_N").toInt();
			m_pBusinessSet->BusImport->ProcessProjects_Export(Ret_pStatus,recSettings.getDataRef(0,"CIE_PROJ_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX_PROCESS").toString(),recSettings.getDataRef(0,"CIE_PROJ_SOURCE_DIR").toString(),nLastN,recSettings.getDataRef(0,"CIE_PROJ_ADD_DATETIME").toInt(),Ret_ProcessedFiles,Ret_pErrors,ProjectsForExport,false,dummy, recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt());
		}
	}
	else //FTP
	{
		if (recSettings.getDataRef(0,"CIE_PROJ_IMPORT").toInt()>0) //import:
		{

			//get FTP
			FTPConnectionSettings conn;
			GetFTPSettings(Ret_pStatus,recSettings.getDataRef(0,"CIE_PROJ_SOURCE_FTP_ID").toInt(),conn);
			if (!Ret_pStatus.IsOK()) return;

			//prepare/download files for import:
			int nCnt=PreProcessImportFTP(Ret_pStatus,TEMP_FTP_DIR_PROJECT,conn,recSettings.getDataRef(0,"CIE_PROJ_SOURCE_FTP_DIR").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX_PROCESS").toString());
			if (!Ret_pStatus.IsOK()) return;

			if (nCnt==0)//->nothing to process
				return;

			QString strTargetDir=QCoreApplication::applicationDirPath()+"/"+TEMP_FTP_DIR_PROJECT;
			m_pBusinessSet->BusImport->ProcessProjects_Import(Ret_pStatus,recSettings.getDataRef(0,"CIE_PROJ_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX_PROCESS").toString(),strTargetDir,Ret_ProcessedFiles,Ret_pErrors);
			if (!Ret_pStatus.IsOK()) return;

			//mark processed files:
			PostProcessImportFTP(Ret_pStatus,TEMP_FTP_DIR_PROJECT,conn,recSettings.getDataRef(0,"CIE_PROJ_SOURCE_FTP_DIR").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX_PROCESS").toString(),Ret_ProcessedFiles);
			if (!Ret_pStatus.IsOK()) return;

		}
		else
		{
			//get FTP
			FTPConnectionSettings conn;
			GetFTPSettings(Ret_pStatus,recSettings.getDataRef(0,"CIE_PROJ_SOURCE_FTP_ID").toInt(),conn);
			if (!Ret_pStatus.IsOK()) return;

			//prepare/download files for import:
			PreProcessExportFTP(Ret_pStatus,TEMP_FTP_DIR_PROJECT);
			if (!Ret_pStatus.IsOK()) return;

			QString strTargetDir=QCoreApplication::applicationDirPath()+"/"+TEMP_FTP_DIR_PROJECT;
			DbRecordSet ProjectsForExport;
			QByteArray dummy;
			int nLastN=0;
			if (recSettings.getDataRef(0,"CIE_PROJ_EXPORT_TYPE").toInt()==1)
				nLastN=recSettings.getDataRef(0,"CIE_PROJ_EXPORT_LAST_N").toInt();
			m_pBusinessSet->BusImport->ProcessProjects_Export(Ret_pStatus,recSettings.getDataRef(0,"CIE_PROJ_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX_PROCESS").toString(),strTargetDir,nLastN,recSettings.getDataRef(0,"CIE_PROJ_ADD_DATETIME").toInt(),Ret_ProcessedFiles,Ret_pErrors,ProjectsForExport,false,dummy, recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt());
			if (!Ret_pStatus.IsOK()) return;

			//upload processed files:
			PostProcessExportFTP(Ret_pStatus,TEMP_FTP_DIR_PROJECT,conn,recSettings.getDataRef(0,"CIE_PROJ_SOURCE_FTP_DIR").toString(),Ret_ProcessedFiles);
			if (!Ret_pStatus.IsOK()) return;

		}

	}

}

void ImportExportManagerAbstract::ProcessUsers(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	if (!m_pBusinessSet)
	{
		Ret_pStatus.setError(1,"Business Set not available!");
		return;
	}
	DbRecordSet recSettings;
	GetSettings(Ret_pStatus,recSettings);
	if (!Ret_pStatus.IsOK())return;

	//get default role to create login account
	int nDefaultRole=-1;
	if (recSettings.getDataRef(0,"CIE_USER_DEF_ROLE_ID").toInt()>0)
		nDefaultRole=recSettings.getDataRef(0,"CIE_USER_DEF_ROLE_ID").toInt();

	//check if local directory:
	if (recSettings.getDataRef(0,"CIE_USER_SOURCE")==0)
	{
		if (recSettings.getDataRef(0,"CIE_USER_IMPORT").toInt()>0)
		{
			m_pBusinessSet->BusImport->ProcessUsers_Import(Ret_pStatus,recSettings.getDataRef(0,"CIE_USER_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString(),recSettings.getDataRef(0,"CIE_USER_SOURCE_DIR").toString(),Ret_ProcessedFiles,Ret_pErrors,nDefaultRole);
		}
		else
		{
			QByteArray dummy;
			DbRecordSet UsersForExport;
			int nLastN=0;
			if (recSettings.getDataRef(0,"CIE_USER_EXPORT_TYPE").toInt()==1)
				nLastN=recSettings.getDataRef(0,"CIE_USER_EXPORT_LAST_N").toInt();
			m_pBusinessSet->BusImport->ProcessUsers_Export(Ret_pStatus,recSettings.getDataRef(0,"CIE_USER_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString(),recSettings.getDataRef(0,"CIE_USER_SOURCE_DIR").toString(),nLastN,recSettings.getDataRef(0,"CIE_USER_ADD_DATETIME").toInt(),Ret_ProcessedFiles,Ret_pErrors,UsersForExport,false,dummy, recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt());
		}

	}
	else //FTP
	{
		if (recSettings.getDataRef(0,"CIE_USER_IMPORT").toInt()>0) //import:
		{

			//get FTP
			FTPConnectionSettings conn;
			GetFTPSettings(Ret_pStatus,recSettings.getDataRef(0,"CIE_USER_SOURCE_FTP_ID").toInt(),conn);
			if (!Ret_pStatus.IsOK()) return;

			//prepare/download files for import:
			int nCnt=PreProcessImportFTP(Ret_pStatus,TEMP_FTP_DIR_USER,conn,recSettings.getDataRef(0,"CIE_USER_SOURCE_FTP_DIR").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString());
			if (!Ret_pStatus.IsOK()) return;

			if (nCnt==0)//->nothing to process
				return;

			QString strTargetDir=QCoreApplication::applicationDirPath()+"/"+TEMP_FTP_DIR_USER;
			m_pBusinessSet->BusImport->ProcessUsers_Import(Ret_pStatus,recSettings.getDataRef(0,"CIE_USER_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString(),strTargetDir,Ret_ProcessedFiles,Ret_pErrors,nDefaultRole);
			if (!Ret_pStatus.IsOK()) return;

			//mark processed files:
			PostProcessImportFTP(Ret_pStatus,TEMP_FTP_DIR_USER,conn,recSettings.getDataRef(0,"CIE_USER_SOURCE_FTP_DIR").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString(),Ret_ProcessedFiles);
			if (!Ret_pStatus.IsOK()) return;

		}
		else
		{
			//get FTP
			FTPConnectionSettings conn;
			GetFTPSettings(Ret_pStatus,recSettings.getDataRef(0,"CIE_USER_SOURCE_FTP_ID").toInt(),conn);
			if (!Ret_pStatus.IsOK()) return;

			//prepare/download files for import:
			PreProcessExportFTP(Ret_pStatus,TEMP_FTP_DIR_USER);
			if (!Ret_pStatus.IsOK()) return;

			QString strTargetDir=QCoreApplication::applicationDirPath()+"/"+TEMP_FTP_DIR_USER;
			DbRecordSet ProjectsForExport;
			QByteArray dummy;
			int nLastN=0;
			if (recSettings.getDataRef(0,"CIE_USER_EXPORT_TYPE").toInt()==1)
				nLastN=recSettings.getDataRef(0,"CIE_USER_EXPORT_LAST_N").toInt();
			m_pBusinessSet->BusImport->ProcessUsers_Export(Ret_pStatus,recSettings.getDataRef(0,"CIE_USER_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString(),strTargetDir,nLastN,recSettings.getDataRef(0,"CIE_USER_ADD_DATETIME").toInt(),Ret_ProcessedFiles,Ret_pErrors,ProjectsForExport,false,dummy, recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt());
			if (!Ret_pStatus.IsOK()) return;

			//upload processed files:
			PostProcessExportFTP(Ret_pStatus,TEMP_FTP_DIR_USER,conn,recSettings.getDataRef(0,"CIE_USER_SOURCE_FTP_DIR").toString(),Ret_ProcessedFiles);
			if (!Ret_pStatus.IsOK()) return;

		}

	}
}
void ImportExportManagerAbstract::ProcessDebtors(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	if (!m_pBusinessSet)
	{
		Ret_pStatus.setError(1,"Business Set not available!");
		return;
	}
	DbRecordSet recSettings;
	GetSettings(Ret_pStatus,recSettings);
	if (!Ret_pStatus.IsOK())return;


	//check if local directory:
	if (recSettings.getDataRef(0,"CIE_DEBT_SOURCE")==0)
	{
		int nLastN=0;
		if (recSettings.getDataRef(0,"CIE_DEBT_EXPORT_TYPE").toInt()==1)
			nLastN=recSettings.getDataRef(0,"CIE_DEBT_EXPORT_LAST_N").toInt();

		m_pBusinessSet->BusImport->ProcessDebtors_Export(Ret_pStatus,recSettings.getDataRef(0,"CIE_DEBT_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_DEBT_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_DEBT_FILE_PREFIX_PROCESS").toString(),recSettings.getDataRef(0,"CIE_DEBT_SOURCE_DIR").toString(),nLastN,recSettings.getDataRef(0,"CIE_DEBT_ADD_DATETIME").toInt(),Ret_ProcessedFiles,Ret_pErrors,recSettings.getDataRef(0,"CIE_DEBT_ADDRESS_TYPE_ID").toInt(), recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt());
	}
	else //FTP
	{
		//get FTP
		FTPConnectionSettings conn;
		GetFTPSettings(Ret_pStatus,recSettings.getDataRef(0,"CIE_DEBT_SOURCE_FTP_ID").toInt(),conn);
		if (!Ret_pStatus.IsOK()) return;

		//prepare/download files for import:
		PreProcessExportFTP(Ret_pStatus,TEMP_FTP_DIR_DEBTOR);
		if (!Ret_pStatus.IsOK()) return;

		QString strTargetDir=QCoreApplication::applicationDirPath()+"/"+TEMP_FTP_DIR_DEBTOR;
		DbRecordSet ProjectsForExport;
		QByteArray dummy;
		int nLastN=0;
		if (recSettings.getDataRef(0,"CIE_DEBT_EXPORT_TYPE").toInt()==1)
			nLastN=recSettings.getDataRef(0,"CIE_DEBT_EXPORT_LAST_N").toInt();
		m_pBusinessSet->BusImport->ProcessDebtors_Export(Ret_pStatus,recSettings.getDataRef(0,"CIE_DEBT_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_DEBT_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_DEBT_FILE_PREFIX_PROCESS").toString(),strTargetDir,nLastN,recSettings.getDataRef(0,"CIE_DEBT_ADD_DATETIME").toInt(),Ret_ProcessedFiles,Ret_pErrors,0, recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt());
		if (!Ret_pStatus.IsOK()) return;

		//upload processed files:
		PostProcessExportFTP(Ret_pStatus,TEMP_FTP_DIR_DEBTOR,conn,recSettings.getDataRef(0,"CIE_DEBT_SOURCE_FTP_DIR").toString(),Ret_ProcessedFiles);
		if (!Ret_pStatus.IsOK()) return;
	}
}
void ImportExportManagerAbstract::ProcessContacts(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, QString strLang)
{
	if (!m_pBusinessSet)
	{
		Ret_pStatus.setError(1,"Business Set not available!");
		return;
	}
	DbRecordSet recSettings;
	GetSettings(Ret_pStatus,recSettings);
	if (!Ret_pStatus.IsOK())return;

	bool bSokratesHdr = true;
	bool bColumnTitles = false;
	bool bOutlookExport = false;

	QByteArray datHedFile;
	QString strHdrFile;
	if(strLang == "de"){
		if(bOutlookExport)
			strHdrFile = "Head_Cntct_Outlook_Ger.hed";
		else
			strHdrFile = "Head_Cntct_Comm_Ger.hed";
	}
	else{
		if(bOutlookExport)
			strHdrFile = "Head_Cntct_Outlook_Eng.hed";
		else
			strHdrFile = "Head_Cntct_Comm_Eng.hed";
	}
	QFile fileMap(QCoreApplication::applicationDirPath() + "/" + strHdrFile);
	if (fileMap.open(QIODevice::ReadOnly)){
		datHedFile = fileMap.readAll();
	}

	//check if local directory:
	if (recSettings.getDataRef(0,"CIE_CONT_SOURCE")==0)
	{
		if (recSettings.getDataRef(0,"CIE_CONT_IMPORT").toInt()>0)
		{
			m_pBusinessSet->BusImport->ProcessContacts_Import(Ret_pStatus,recSettings.getDataRef(0,"CIE_CONT_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX_PROCESS").toString(),recSettings.getDataRef(0,"CIE_CONT_SOURCE_DIR").toString(),Ret_ProcessedFiles,Ret_pErrors);
		}
		else
		{
			QByteArray dummy;
			DbRecordSet UsersForExport;
			int nLastN=0;
			if (recSettings.getDataRef(0,"CIE_CONT_EXPORT_TYPE").toInt()==1)
				nLastN=recSettings.getDataRef(0,"CIE_CONT_EXPORT_LAST_N").toInt();
			m_pBusinessSet->BusImport->ProcessContacts_Export(
				Ret_pStatus,
				recSettings.getDataRef(0,"CIE_CONT_FILE_EXT").toString(),
				recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX").toString(),
				recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX_PROCESS").toString(),
				recSettings.getDataRef(0,"CIE_CONT_SOURCE_DIR").toString(),
				nLastN,
				recSettings.getDataRef(0,"CIE_CONT_ADD_DATETIME").toInt(),
				Ret_ProcessedFiles,
				Ret_pErrors,
				UsersForExport,
				false,
				dummy,
				recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt(),
				bSokratesHdr,
				bColumnTitles,
				bOutlookExport,
				strLang,
				datHedFile);
		}

	}
	else //FTP
	{
		if (recSettings.getDataRef(0,"CIE_CONT_IMPORT").toInt()>0) //import:
		{

			//get FTP
			FTPConnectionSettings conn;
			GetFTPSettings(Ret_pStatus,recSettings.getDataRef(0,"CIE_CONT_SOURCE_FTP_ID").toInt(),conn);
			if (!Ret_pStatus.IsOK()) return;

			//prepare/download files for import:
			int nCnt=PreProcessImportFTP(Ret_pStatus,TEMP_FTP_DIR_CONTACT,conn,recSettings.getDataRef(0,"CIE_CONT_SOURCE_FTP_DIR").toString(),recSettings.getDataRef(0,"CIE_CONT_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX_PROCESS").toString());
			if (!Ret_pStatus.IsOK()) return;

			if (nCnt==0)//->nothing to process
				return;

			QString strTargetDir=QCoreApplication::applicationDirPath()+"/"+TEMP_FTP_DIR_CONTACT;
			m_pBusinessSet->BusImport->ProcessContacts_Import(Ret_pStatus,recSettings.getDataRef(0,"CIE_CONT_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX_PROCESS").toString(),strTargetDir,Ret_ProcessedFiles,Ret_pErrors);
			if (!Ret_pStatus.IsOK()) return;

			//mark processed files:
			PostProcessImportFTP(Ret_pStatus,TEMP_FTP_DIR_CONTACT,conn,recSettings.getDataRef(0,"CIE_CONT_SOURCE_FTP_DIR").toString(),recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX_PROCESS").toString(),Ret_ProcessedFiles);
			if (!Ret_pStatus.IsOK()) return;

		}
		else
		{
			//get FTP
			FTPConnectionSettings conn;
			GetFTPSettings(Ret_pStatus,recSettings.getDataRef(0,"CIE_CONT_SOURCE_FTP_ID").toInt(),conn);
			if (!Ret_pStatus.IsOK()) return;

			//prepare/download files for import:
			PreProcessExportFTP(Ret_pStatus,TEMP_FTP_DIR_CONTACT);
			if (!Ret_pStatus.IsOK()) return;

			QString strTargetDir=QCoreApplication::applicationDirPath()+"/"+TEMP_FTP_DIR_CONTACT;
			DbRecordSet ProjectsForExport;
			QByteArray dummy;
			int nLastN=0;
			if (recSettings.getDataRef(0,"CIE_CONT_EXPORT_TYPE").toInt()==1)
				nLastN=recSettings.getDataRef(0,"CIE_CONT_EXPORT_LAST_N").toInt();
			m_pBusinessSet->BusImport->ProcessContacts_Export(
				Ret_pStatus,
				recSettings.getDataRef(0,"CIE_CONT_FILE_EXT").toString(),
				recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX").toString(),
				recSettings.getDataRef(0,"CIE_CONT_FILE_PREFIX_PROCESS").toString(),
				strTargetDir,
				nLastN,
				recSettings.getDataRef(0,"CIE_CONT_ADD_DATETIME").toInt(),
				Ret_ProcessedFiles,Ret_pErrors,
				ProjectsForExport,
				false,
				dummy,
				recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt(),
				bSokratesHdr,
				bColumnTitles,
				bOutlookExport,
				strLang,
				datHedFile);
			if (!Ret_pStatus.IsOK()) return;

			//upload processed files:
			PostProcessExportFTP(Ret_pStatus,TEMP_FTP_DIR_CONTACT,conn,recSettings.getDataRef(0,"CIE_CONT_SOURCE_FTP_DIR").toString(),Ret_ProcessedFiles);
			if (!Ret_pStatus.IsOK()) return;
		}

	}
}



void ImportExportManagerAbstract::ProcessStoredProjectLists(Status &Ret_pStatus, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	if (!m_pBusinessSet)
	{
		Ret_pStatus.setError(1,"Business Set not available!");
		return;
	}

	DbRecordSet recSettings;
	GetSettings(Ret_pStatus,recSettings);
	if (!Ret_pStatus.IsOK())return;


	//check if local directory:
	if (recSettings.getDataRef(0,"CIE_SPLST_SOURCE")==0)
	{
		m_pBusinessSet->BusImport->ProcessStoredProjectList_Import(Ret_pStatus,recSettings.getDataRef(0,"CIE_SPLST_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_SPLST_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_SPLST_FILE_PREFIX_PROCESS").toString(),recSettings.getDataRef(0,"CIE_SPLST_SOURCE_DIR").toString(),Ret_ProcessedFiles,Ret_pErrors);
	}
	else //FTP
	{

		//get FTP
		FTPConnectionSettings conn;
		GetFTPSettings(Ret_pStatus,recSettings.getDataRef(0,"CIE_SPLST_SOURCE_FTP_ID").toInt(),conn);
		if (!Ret_pStatus.IsOK()) return;

		//prepare/download files for import:
		int nCnt=PreProcessImportFTP(Ret_pStatus,TEMP_FTP_DIR_PROJECT,conn,recSettings.getDataRef(0,"CIE_SPLST_SOURCE_FTP_DIR").toString(),recSettings.getDataRef(0,"CIE_SPLST_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_SPLST_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_SPLST_FILE_PREFIX_PROCESS").toString());
		if (!Ret_pStatus.IsOK()) return;

		if (nCnt==0)//->nothing to process
			return;

		QString strTargetDir=QCoreApplication::applicationDirPath()+"/"+TEMP_FTP_DIR_PROJECT;
		m_pBusinessSet->BusImport->ProcessStoredProjectList_Import(Ret_pStatus,recSettings.getDataRef(0,"CIE_SPLST_FILE_EXT").toString(),recSettings.getDataRef(0,"CIE_SPLST_FILE_PREFIX").toString(),recSettings.getDataRef(0,"CIE_SPLST_FILE_PREFIX_PROCESS").toString(),strTargetDir,Ret_ProcessedFiles,Ret_pErrors);
		if (!Ret_pStatus.IsOK()) return;

		//mark processed files:
		PostProcessImportFTP(Ret_pStatus,TEMP_FTP_DIR_PROJECT,conn,recSettings.getDataRef(0,"CIE_SPLST_SOURCE_FTP_DIR").toString(),recSettings.getDataRef(0,"CIE_SPLST_FILE_PREFIX_PROCESS").toString(),Ret_ProcessedFiles);
		if (!Ret_pStatus.IsOK()) return;

	}

}


void ImportExportManagerAbstract::SaveSettings(Status &Ret_pStatus, DbRecordSet lstSettings)
{	
	QString pLockResourceID;
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	DbRecordSet lstForDelete;
	m_pBusinessSet->ClientSimpleORM->Write(Ret_pStatus,CORE_IMPORT_EXPORT,lstSettings, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete);
}

void ImportExportManagerAbstract::ReadSettings(Status &Ret_pStatus, DbRecordSet &Ret_lstSettings)
{
	m_pBusinessSet->ClientSimpleORM->Read(Ret_pStatus,CORE_IMPORT_EXPORT,Ret_lstSettings);
}

void ImportExportManagerAbstract::GetFTPSettings(Status &pStatus,int nFTPID,FTPConnectionSettings &conn)
{
	if (!m_pBusinessSet)
	{
		pStatus.setError(1,"Business Set not available!");
		return;
	}

	DbRecordSet rec;
	m_pBusinessSet->ClientSimpleORM->Read(pStatus,CORE_FTP_SERVERS,rec,"WHERE CFTP_ID="+QVariant(nFTPID).toString());
	if (!pStatus.IsOK()) return;

	if (rec.getRowCount()!=1)
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_FTP_EI_FAIL_LOAD_SETTINGS);
		return;
	}

	conn.m_strServerIP=rec.getDataRef(0,"CFTP_HOST").toString();
	conn.m_strFTPUserName=rec.getDataRef(0,"CFTP_USER").toString();
	conn.m_strFTPPassword=rec.getDataRef(0,"CFTP_PASSWORD").toString();
	conn.m_nPort=rec.getDataRef(0,"CFTP_PORT").toInt();
	conn.m_bPassive=rec.getDataRef(0,"CFTP_IS_PASSIVE_MODE").toBool();
	conn.m_strStartDirectory=rec.getDataRef(0,"CFTP_START_DIRECTORY").toString();

	//proxy:
	conn.m_bUseProxy=rec.getDataRef(0,"CFTP_USE_PROXY").toBool();
	conn.m_strProxyHostName=rec.getDataRef(0,"CFTP_PROXY_HOST").toString();
	conn.m_strProxyUsername=rec.getDataRef(0,"CFTP_PROXY_USER").toString();
	conn.m_strProxyPassWord=rec.getDataRef(0,"CFTP_PROXY_PASSWORD").toString();
	conn.m_nProxyPort=rec.getDataRef(0,"CFTP_PROXY_PORT").toInt();
	conn.m_nProxyType=rec.getDataRef(0,"CFTP_PROXY_TYPE").toInt();

}

QString ImportExportManagerAbstract::GetLastStatus(int nTaskID)
{
	QString strPrefix;
	QString strMsg;

	switch(nTaskID)
	{
	case PROCESS_TYPE_PROJECT:
		strPrefix="PROJ";
		break;
	case PROCESS_TYPE_USER:
		strPrefix="USER";
		break;
	case PROCESS_TYPE_CONTACT:
		strPrefix="CONT";
		break;
	case PROCESS_TYPE_DEBTOR:
		strPrefix="DEBT";
		break;
	case PROCESS_TYPE_SPL:
		strPrefix="SPLST";
		break;
	}

	Status errTmp;
	DbRecordSet recSettings;
	GetSettings(errTmp,recSettings);
	if (errTmp.IsOK())
	{
		QFileInfo info(recSettings.getDataRef(0,"CIE_"+strPrefix+"_STATUS_LAST_FILE").toString());
		QString strfileName=info.fileName();

		strMsg =recSettings.getDataRef(0,"CIE_"+strPrefix+"_STATUS_REC_OK").toString()+";"+recSettings.getDataRef(0,"CIE_"+strPrefix+"_STATUS_REC_FAIL").toString()+";";
		strMsg +=recSettings.getDataRef(0,"CIE_"+strPrefix+"_STATUS_DATE").toDateTime().toString(Qt::SystemLocaleLongDate)+";";
		strMsg +=strfileName+";"+recSettings.getDataRef(0,"CIE_"+strPrefix+"_STATUS_ERROR").toString()+";";
	}

	return strMsg;
}