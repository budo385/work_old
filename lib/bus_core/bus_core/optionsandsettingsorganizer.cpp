#include "optionsandsettingsorganizer.h"

#include "db_core/db_core/dbsqltableview.h"

OptionsAndSettingsOrganizer::OptionsAndSettingsOrganizer()
{
	m_recOptionsSet.addColumn(QVariant::Int,	"OptionSetID");
	m_recOptionsSet.addColumn(QVariant::String, "OptionSetName");
	m_recOptionsSet.addColumn(QVariant::String, "OptionSetDescription");
	m_recOptionsSet.addColumn(QVariant::String, "OptionSetIcon");
	m_recOptionsSet.addColumn(QVariant::Bool,   "OptionSetHidden");
	m_recOptionsSet.addColumn(QVariant::Bool,   "ApplicationOptionSet");

	m_recOptions.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
	m_recSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));

	InitializeOptionsSets();
	InitializeAppOptions();
	InitializePersonSettings();
}

OptionsAndSettingsOrganizer::~OptionsAndSettingsOrganizer()
{
	
}

DbRecordSet& OptionsAndSettingsOrganizer::GetOptionSets()
{
	return m_recOptionsSet;
}

DbRecordSet OptionsAndSettingsOrganizer::GetApplicationOptionSets()
{
	m_recOptionsSet.clearSelection();
	m_recOptionsSet.find("ApplicationOptionSet", true);
	
	return m_recOptionsSet.getSelectedRecordSet();
}

DbRecordSet OptionsAndSettingsOrganizer::GetPersonalSettings()
{
	return m_recSettings;
}

DbRecordSet OptionsAndSettingsOrganizer::GetSinglePersonalSetting(int nSettingID)
{
	DbRecordSet recSetting = GetPersonalSettings();
	//_DUMP(recSetting);
	recSetting.find("BOUS_SETTING_ID", nSettingID);
	recSetting.deleteUnSelectedRows();

	return recSetting;
}

DbRecordSet OptionsAndSettingsOrganizer::GetSettingsBySetID(int nSettingsSetID)
{
	DbRecordSet recSetting = GetPersonalSettings();
	recSetting.find("BOUS_SETTING_SET_ID", nSettingsSetID);
	recSetting.deleteUnSelectedRows();

	return recSetting;
}

DbRecordSet OptionsAndSettingsOrganizer::GetOptions()
{
	return m_recOptions;
}

DbRecordSet OptionsAndSettingsOrganizer::GetSingleOption(int nOptionsID)
{
	DbRecordSet recOption = GetOptions();
	recOption.find("BOUS_SETTING_ID", nOptionsID);
	recOption.deleteUnSelectedRows();

	return recOption;
}

void OptionsAndSettingsOrganizer::InitializeOptionsSets()
{
	int row = 0;
	//********************************************************************
	//**						OPTIONS_SET								**
	//**					Aplication options set.						**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, OPTIONS_SET);
	m_recOptionsSet.setData(row, 1, QObject::tr("Application"));				//Option name.
	m_recOptionsSet.setData(row, 2, QObject::tr("Set of global application options."));	//Option description.
	m_recOptionsSet.setData(row, 3, ":handwrite.png");						//Option icon.
	m_recOptionsSet.setData(row, 4, true);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, true);												//Is application option set.

	row++;

	//********************************************************************
	//**					APPEARANCE									**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, APPEARANCE);
	m_recOptionsSet.setData(row, 1, QObject::tr("Appearance"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Person's application appearance settings."));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsAppearance.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					CURRENT_LOCATION							**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, CURRENT_LOCATION);
	m_recOptionsSet.setData(row, 1, QObject::tr("Location & Language"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Person's application location settings."));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsLocation.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					COMM_GRID_FILTER							**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, COMM_GRID_FILTER);
	m_recOptionsSet.setData(row, 1, QObject::tr("Communication grid filter"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Person's communication grid filter settings."));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsCommunication.png");
	m_recOptionsSet.setData(row, 4, true);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					DOCUMENT_DEFAULTS					**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, SETTINGS_DOCUMENT_DEFAULTS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Documents")); //issue 2675. changed name
	m_recOptionsSet.setData(row, 2, QObject::tr("File Document Destination Configuration:"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsDocument.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					OPTIONS_DOCUMENT_DEFAULTS					**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, OPTIONS_DOCUMENT_DEFAULTS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Application Document Defaults"));
	m_recOptionsSet.setData(row, 2, QObject::tr("File Document Destination Configuration:"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsDocument.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, true);												//Is application option set.

	row++;

	//********************************************************************
	//**					IMPORT_SETTINGS					**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, IMPORT_SETTINGS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Email Import"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Email Import"));
	m_recOptionsSet.setData(row, 3, ":handwrite.png");
	m_recOptionsSet.setData(row, 4, true);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	
	//********************************************************************
	//**					COMM_SETTINGS						**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, COMM_SETTINGS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Communication"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Communication"));
	m_recOptionsSet.setData(row, 3,":Icon_SettingsCommunication.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					OUTLOOK_SETTINGS						**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, OUTLOOK_SETTINGS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Outlook"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Outlook"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsOutlook.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					THUNDERBIRD_SETTINGS						**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, THUNDERBIRD_SETTINGS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Thunderbird"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Thunderbird"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsThunderbird.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					CONTACT_OPTIONS					**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, CONTACT_OPTIONS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Contact", "Why"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Contact", "Why"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsContact.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, true);												//Is application option set.

	row++;

	//********************************************************************
	//**					CONTACT_SETTINGS_VISIBLE					**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, CONTACT_SETTINGS_VISIBLE);
	m_recOptionsSet.setData(row, 1, QObject::tr("Contact", "Why"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Contact", "Why"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsContact.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					CONTACT_SETTINGS_INVISIBLE					**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, CONTACT_SETTINGS_INVISIBLE);
	m_recOptionsSet.setData(row, 1, QObject::tr("Contact", "Why"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Contact", "Why"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsContact.png");
	m_recOptionsSet.setData(row, 4, true);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					APPEARANCE_SETTINGS_INVISIBLE				**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, APPEARANCE_SETTINGS_INVISIBLE);
	m_recOptionsSet.setData(row, 1, QObject::tr("Appearance"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Appearance"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsAppearance.png");
	m_recOptionsSet.setData(row, 4, true);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					ORGANIZATION_OPTIONS				**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, ORGANIZATION_OPTIONS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Default Organization"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Default Organization"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsOrganization.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, true);												//Is application option set.

	row++;

	//********************************************************************
	//**					PROJECT_SETTINGS_VISIBLE					**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, PROJECT_SETTINGS_VISIBLE);
	m_recOptionsSet.setData(row, 1, QObject::tr("Project"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Project"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsProject.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					COMM_SETTINGS_APP							**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, COMM_SETTINGS_APP);
	m_recOptionsSet.setData(row, 1, QObject::tr("Communication"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Communication"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsCommunication.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, true);												//Is application option set.

	row++;

	//********************************************************************
	//**					PROJECT_OPTIONS_APP							**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, PROJECT_OPTIONS_APP);
	m_recOptionsSet.setData(row, 1, QObject::tr("Project"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Project"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsProject.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, true);												//Is application option set.

	row++;

	//********************************************************************
	//**					PROGRAM_UPDATE_SETTINGS						**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, PROGRAM_UPDATE_SETTINGS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Program Updates"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Program Updates"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsUpdate.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					AVATAR_SETTINGS						**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, AVATAR_SETTINGS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Card"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Card"));
	m_recOptionsSet.setData(row, 3, ":handwrite.png");
	m_recOptionsSet.setData(row, 4, true);	//hidden											//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**					EMAIL_SETTINGS						**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, EMAIL_SETTINGS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Email Out"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Email Out"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsEmail.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

	//********************************************************************
	//**						CALENDAR_OPTIONS						**
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, CALENDAR_OPTIONS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Calendar"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Calendar"));
	m_recOptionsSet.setData(row, 3, ":Calendar_Options.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, true);												//Is application option set.

	row++;

	//********************************************************************
	//**					EMAIL_IN_SETTINGS						    **
	//********************************************************************
	m_recOptionsSet.addRow();
	m_recOptionsSet.setData(row, 0, EMAIL_IN_SETTINGS);
	m_recOptionsSet.setData(row, 1, QObject::tr("Email In"));
	m_recOptionsSet.setData(row, 2, QObject::tr("Email In"));
	m_recOptionsSet.setData(row, 3, ":Icon_SettingsEmail.png");
	m_recOptionsSet.setData(row, 4, false);												//Option set hidden.
	m_recOptionsSet.setData(row, 5, false);												//Is application option set.

	row++;

}

void OptionsAndSettingsOrganizer::InitializeAppOptions()
{
	int row = 0;
	//********************************************************************
	//**				APP_DEFAULT_DOCUMENT_TYPE						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, OPTIONS_DOCUMENT_DEFAULTS);					//Settings set id.
	m_recOptions.setData(row, 4, APP_DEFAULT_DOCUMENT_TYPE);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "CH");									//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());							//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				APP_DEFAULT_DOCUMENT_DIR						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, OPTIONS_DOCUMENT_DEFAULTS);					//Settings set id.
	m_recOptions.setData(row, 4, APP_DEFAULT_DOCUMENT_DIR);						//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recOptions.setData(row, 7, 0);											//Value for int or bool.
	m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				APP_DEFAULT_DOCUMENT_NAME						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, OPTIONS_DOCUMENT_DEFAULTS);					//Settings set id.
	m_recOptions.setData(row, 4, APP_DEFAULT_DOCUMENT_NAME);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 1);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				APP_CONTACT_DEF_ADDR_SCHEMA_PERSON						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CONTACT_OPTIONS);								//Settings set id.
	m_recOptions.setData(row, 4, APP_CONTACT_DEF_ADDR_SCHEMA_PERSON);			//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				APP_CONTACT_DEF_ADDR_SCHEMA_ORG						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CONTACT_OPTIONS);								//Settings set id.
	m_recOptions.setData(row, 4, APP_CONTACT_DEF_ADDR_SCHEMA_ORG);				//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				APP_CONTACT_DEF_ACTUAL_LIST_VIEW						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CONTACT_OPTIONS);								//Settings set id.
	m_recOptions.setData(row, 4, APP_CONTACT_DEF_ACTUAL_LIST_VIEW);				//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**				APP_CONTACT_DEF_GROUP_LIST_VIEW						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CONTACT_OPTIONS);								//Settings set id.
	m_recOptions.setData(row, 4, APP_CONTACT_DEF_GROUP_LIST_VIEW);				//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				APP_CONTACT_DEF_GROUP_TREE						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CONTACT_OPTIONS);								//Settings set id.
	m_recOptions.setData(row, 4, APP_CONTACT_DEF_GROUP_TREE);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				APP_CONTACT_GROUP_EXCLUDE_ID						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CONTACT_OPTIONS);								//Settings set id.
	m_recOptions.setData(row, 4, APP_CONTACT_GROUP_EXCLUDE_ID);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//**************************************************************************
	//**				APP_PERSON_DOCUMENT_TYPE_PARSING_ON					**
	//**************************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, SETTINGS_DOCUMENT_DEFAULTS);				//Settings set id.
	m_recOptions.setData(row, 4, APP_PERSON_DOCUMENT_TYPE_PARSING_ON);			//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					COMM_OPTIONS_TRANSFER_GROUP					**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, COMM_SETTINGS_APP);							//Settings set id.
	m_recOptions.setData(row, 4, COMM_OPTIONS_TRANSFER_GROUP);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).


	row++;

	//********************************************************************
	//**					PROJECT_OPTIONS_DEF_STORED_LIST					**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, PROJECT_OPTIONS_APP);							//Settings set id.
	m_recOptions.setData(row, 4, PROJECT_OPTIONS_DEF_STORED_LIST);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).


	row++;

	//********************************************************************
	//**					ORGANIZATION_OPTIONS						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, ORGANIZATION_OPTIONS);							//Settings set id.
	m_recOptions.setData(row, 4, APP_DEFAULT_ORGANIZATION_ID);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//**************************************************************************
	//**				APP_PERSON_DOCUMENT_TYPE_PARSING_ON					**
	//**************************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, IMPORT_SETTINGS);								//Settings set id.
	m_recOptions.setData(row, 4, APP_EXPORT_IMPORT_SHOW_OPTION_USE_UNICODE);	//Settings id.
	m_recOptions.setData(row, 5, 0);											//Settings visible.
	m_recOptions.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CONTACT_DISABLE_ZIP_LOOKUP					**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CONTACT_OPTIONS);					//Settings set id.
	m_recOptions.setData(row, 4, CONTACT_DISABLE_ZIP_LOOKUP);			//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 0);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recOptions.setData(row, 9, );											//Value for byte array.
	//m_recOptions.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**				APP_FORMAT_CONTACT_ADDRESS_ALWAYS						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CONTACT_OPTIONS);								//Settings set id.
	m_recOptions.setData(row, 4, APP_FORMAT_CONTACT_ADDRESS_ALWAYS);			//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 0);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				CALENDAR_START_DAILY_WORK_TIME					**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CALENDAR_OPTIONS);							//Settings set id.
	m_recOptions.setData(row, 4, CALENDAR_START_DAILY_WORK_TIME);				//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recOptions.setData(row, 7, 1);											//Value for int or bool.
	m_recOptions.setData(row, 8, "08:00:00");									//Value for string (Switzerland).
	//m_recOptions.setData(row, 9, );											//Value for byte array.
	//m_recOptions.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				CALENDAR_END_DAILY_WORK_TIME					**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CALENDAR_OPTIONS);							//Settings set id.
	m_recOptions.setData(row, 4, CALENDAR_END_DAILY_WORK_TIME);				//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recOptions.setData(row, 7, 1);											//Value for int or bool.
	m_recOptions.setData(row, 8, "17:00:00");										//Value for string (Switzerland).
	//m_recOptions.setData(row, 9, );											//Value for byte array.
	//m_recOptions.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				PRESENCE_TYPE_PRELIMINARY						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CALENDAR_OPTIONS);							//Settings set id.
	m_recOptions.setData(row, 4, PRESENCE_TYPE_PRELIMINARY);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 65);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recOptions.setData(row, 9, );											//Value for byte array.
	//m_recOptions.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					PRESENCE_TYPE_FINAL							**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CALENDAR_OPTIONS);							//Settings set id.
	m_recOptions.setData(row, 4, PRESENCE_TYPE_FINAL);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 66);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recOptions.setData(row, 9, );											//Value for byte array.
	//m_recOptions.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					PRESENCE_TYPE_AVAILABLE						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CALENDAR_OPTIONS);							//Settings set id.
	m_recOptions.setData(row, 4, PRESENCE_TYPE_AVAILABLE);					//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recOptions.setData(row, 7, 67);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recOptions.setData(row, 9, );											//Value for byte array.
	//m_recOptions.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CALENDAR_EMAIL_CONFIRMATION_ADDRESS						**
	//********************************************************************
	m_recOptions.addRow();
	m_recOptions.setData(row, 3, CALENDAR_OPTIONS);							//Settings set id.
	m_recOptions.setData(row, 4, CALENDAR_EMAIL_CONFIRMATION_ADDRESS);		//Settings id.
	m_recOptions.setData(row, 5, 1);											//Settings visible.
	m_recOptions.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recOptions.setData(row, 7, 67);											//Value for int or bool.
	//m_recOptions.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recOptions.setData(row, 9, );											//Value for byte array.
	//m_recOptions.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
}
void OptionsAndSettingsOrganizer::InitializePersonSettings()
{
	int row = 0;
	//********************************************************************
	//**							SKIN								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);								//Settings set id.
	m_recSettings.setData(row, 4, SKIN);										//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary.
	m_recSettings.setData(row, 7, 0);										//Value for int or bool.
	//m_recSettings.setData(row, 8, QObject::tr("Default skin"));				//Value for string.
	//m_recSettings.setData(row, 9, QByteArray());							//Value for byte array.
	//m_recSettings.setData(row, 10, "Some skin");							//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				MAIN_APPLICATION_WINDOW_SIZE					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);								//Settings set id.
	m_recSettings.setData(row, 4, MAIN_APPLICATION_WINDOW_SIZE);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary.
	//m_recSettings.setData(row, 7, 0);										//Value for int or bool.
	m_recSettings.setData(row, 8, "800;600");									//Value for string.
	//m_recSettings.setData(row, 9, QByteArray());							//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						MENU_WIDTH								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);								//Settings set id.
	m_recSettings.setData(row, 4, MENU_WIDTH);								//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary.
	m_recSettings.setData(row, 7, 200);										//Value for int or bool.
	//m_recSettings.setData(row, 8, "800;600");								//Value for string.
	//m_recSettings.setData(row, 9, QByteArray());							//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**			GRID_WIDTH_FOR_FUI_WITH_GRID_ON_LEFT				**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);								//Settings set id.
	m_recSettings.setData(row, 4, GRID_WIDTH_FOR_FUI_WITH_GRID_ON_LEFT);		//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary.
	m_recSettings.setData(row, 7, 20);										//Value for int or bool.
	//m_recSettings.setData(row, 8, "800;600");								//Value for string.
	//m_recSettings.setData(row, 9, QByteArray());							//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						CURRENT_COUNTRY_ISO_CODE								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CURRENT_LOCATION);							//Settings set id.
	m_recSettings.setData(row, 4, CURRENT_COUNTRY_ISO_CODE);								//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary.
	//m_recSettings.setData(row, 7, 0);										//Value for int or bool.
	m_recSettings.setData(row, 8, "CH");										//Value for string (switzerland).
	//m_recSettings.setData(row, 9, QByteArray());							//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				PERSON_DEFAULT_DOCUMENT_TYPE					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, SETTINGS_DOCUMENT_DEFAULTS);				//Settings set id.
	m_recSettings.setData(row, 4, PERSON_DEFAULT_DOCUMENT_TYPE);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "CH");									//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());							//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				PERSON_DEFAULT_DOCUMENT_DIR						**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, OPTIONS_DOCUMENT_DEFAULTS);					//Settings set id.
	m_recSettings.setData(row, 4, PERSON_DEFAULT_DOCUMENT_DIR);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, QByteArray());								//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				IMPORT_EMAIL_OUTLOOK_FOLDERS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, IMPORT_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, IMPORT_EMAIL_OUTLOOK_FOLDERS);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				IMPORT_CONTACT_OUTLOOK_FOLDERS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, IMPORT_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, IMPORT_CONTACT_OUTLOOK_FOLDERS);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					COMM_SETTINGS_LISTEN_SKYPE					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, COMM_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, COMM_SETTINGS_LISTEN_SKYPE);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					COMM_SETTINGS_DEFAULT_CALL_INCOMING					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, COMM_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, COMM_SETTINGS_DEFAULT_CALL_INCOMING);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					COMM_SETTINGS_DEFAULT_SEL_TYPE					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, COMM_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, COMM_SETTINGS_DEFAULT_SEL_TYPE);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					COMM_SETTINGS_DEFAULT_TAPI_DEV					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, COMM_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, COMM_SETTINGS_DEFAULT_TAPI_DEV);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					COMM_SETTINGS_USE_DIAL_PREFIX					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, COMM_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, COMM_SETTINGS_USE_DIAL_PREFIX);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					COMM_SETTINGS_DIAL_PREFIX_STR					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, COMM_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, COMM_SETTINGS_DIAL_PREFIX_STR);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "0");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					COMM_SETTINGS_TRANSFER_GROUP					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, COMM_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, COMM_SETTINGS_TRANSFER_GROUP);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	
	//********************************************************************
	//**						DEFAULT_LANGUAGE						**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CURRENT_LOCATION);							//Settings set id.
	m_recSettings.setData(row, 4, DEFAULT_LANGUAGE);							//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CURRENT_LOCATION_NAME						**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CURRENT_LOCATION);							//Settings set id.
	m_recSettings.setData(row, 4, CURRENT_LOCATION_NAME);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				CURRENT_LOCATION_AREA_CODE						**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CURRENT_LOCATION);							//Settings set id.
	m_recSettings.setData(row, 4, CURRENT_LOCATION_AREA_CODE);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						SHOW_HEADER								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, SHOW_HEADER);									//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						STARTING_MODULE							**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, STARTING_MODULE);								//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					OUTLOOK_SETTINGS_SUBSCRIBED					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, OUTLOOK_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, OUTLOOK_SETTINGS_SUBSCRIBED);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, OUTLOOK_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					OUTLOOK_SETTINGS_TIMER_MIN					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, OUTLOOK_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, OUTLOOK_SETTINGS_TIMER_MIN);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 5);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

		//********************************************************************
	//**					OUTLOOK_SETTINGS_LAST_SCANNED					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, OUTLOOK_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, OUTLOOK_SETTINGS_LAST_SCANNED);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					OUTLOOK_SETTINGS_SCAN_FOLDERS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, OUTLOOK_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, OUTLOOK_SETTINGS_SCAN_FOLDERS);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	

	//********************************************************************
	//**					CONTACT_DEF_ADDR_SCHEMA_PERSON					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_ADDR_SCHEMA_PERSON);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**					CONTACT_DEF_ADDR_SCHEMA_ORG					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_ADDR_SCHEMA_ORG);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CONTACT_DEF_ACTUAL_LIST_VIEW					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_ACTUAL_LIST_VIEW);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CONTACT_DEF_GROUP_LIST_VIEW					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_GROUP_LIST_VIEW);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CONTACT_DEF_GROUP_TREE					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_GROUP_TREE);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**					CONTACT_DEF_SETTINGS_SAVE_ON_EXIT					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_SETTINGS_SAVE_ON_EXIT);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CONTACT_DEF_SETTINGS_SAVE_ON_EXIT					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_ACTUAL_LIST_FOR_LOAD);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	
	//********************************************************************
	//**					CONTACT_DEF_GROUP_FOR_LOAD					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_INVISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_GROUP_FOR_LOAD);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CONTACT_DEF_GROUP_TREE_FOR_LOAD					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_INVISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_GROUP_TREE_FOR_LOAD);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CONTACT_DEF_GROUP_TREE_FOR_EXCLUDE				**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_INVISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_GROUP_EXCLUDE_ID);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**					APPEAR_WINDOW_PROPERTIES					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE_SETTINGS_INVISIBLE);				//Settings set id.
	m_recSettings.setData(row, 4, APPEAR_WINDOW_PROPERTIES);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	///m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					APPEAR_CE_MENU_SECTIONS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE_SETTINGS_INVISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, APPEAR_CE_MENU_SECTIONS);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	///m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					CONTACT_DEF_FILTER_VIEW						**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEF_FILTER_VIEW);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	///m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					PROJECT_DEF_FILTER_VIEW						**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, PROJECT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, PROJECT_DEF_FILTER_VIEW);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	///m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					PROJECT_SETTINGS_DEF_STORED_LIST						**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, PROJECT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, PROJECT_SETTINGS_DEF_STORED_LIST);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	///m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	
	//********************************************************************
	//**					OUTLOOK_SETTINGS_WORKSTATIONS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, OUTLOOK_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, OUTLOOK_SETTINGS_WORKSTATIONS);			//Settings id.
	m_recSettings.setData(row, 5, 0);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**						SHOW_HEADER								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, SHOW_STATUS_BAR);								//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						SHOW_HEADER								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, SHOW_FULL_SCREEN);								//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						SHOW_DOCUMENT_REVISION_HISTORY								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, SHOW_DOCUMENT_REVISION_HISTORY);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						SHOW_CLASSIFICATION_AND_DESC								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, SHOW_CLASSIFICATION_AND_DESC);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						THICK_CHECK_FOR_UPDATES								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, PROGRAM_UPDATE_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, THICK_CHECK_FOR_UPDATES);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						APPEAR_VIEW_MODE								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, APPEAR_VIEW_MODE);							//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**						APPEAR_THEME								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, APPEAR_THEME);								//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**						APPEAR_LAST_POSITION								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, APPEAR_SIDEBAR_LAST_POSITION);				//Settings id.
	m_recSettings.setData(row, 5, 0);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	

	//********************************************************************
	//**					OUTLOOK_SETTINGS_TIMER_MIN					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, AVATAR_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, AVATAR_STARTUP_LIST);			//Settings id.
	m_recSettings.setData(row, 5, 0);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 5);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);	
	
	row++;

	//********************************************************************
	//**			QUICK_CONTACT_ADDRESS_FORMATING_LOCK				**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_INVISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, QUICK_CONTACT_ADDRESS_FORMATING_LOCK);		//Settings id.
	m_recSettings.setData(row, 5, 0);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);	

	row++;

	//********************************************************************
	//**					EMAIL_SETTINGS_SMTP_NAME					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_SMTP_NAME);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	
	//********************************************************************
	//**					EMAIL_SETTINGS_SMTP_PORT					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_SMTP_PORT);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 25);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					EMAIL_SETTINGS_USER_NAME					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_USER_NAME);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**					EMAIL_SETTINGS_USER_EMAIL					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_USER_EMAIL);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					EMAIL_SETTINGS_SMTP_ACC_NAME					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_SMTP_ACC_NAME);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					EMAIL_SETTINGS_SMTP_ACC_PASS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_SMTP_ACC_PASS);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					EMAIL_SETTINGS_USE_AUTH					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_USE_AUTH);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	
	//********************************************************************
	//**					EMAIL_SETTINGS_SEND_COPY_CC					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_SEND_COPY_CC);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					EMAIL_SETTINGS_SEND_COPY_BCC					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_SEND_COPY_BCC);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					EMAIL_SETTINGS_SEND_COPY_BCC					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_SETTINGS_USE_SSL);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	

	//********************************************************************
	//**					THUNDERBIRD_SETTINGS_SUBSCRIBED					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, THUNDERBIRD_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, THUNDERBIRD_SETTINGS_SUBSCRIBED);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					THUNDERBIRD_SETTINGS_IMPORT_ONLY_KNOWN					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, THUNDERBIRD_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, THUNDERBIRD_SETTINGS_IMPORT_ONLY_KNOWN);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					THUNDERBIRD_SETTINGS_TIMER_MIN					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, THUNDERBIRD_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, THUNDERBIRD_SETTINGS_TIMER_MIN);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 5);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

		//********************************************************************
	//**					THUNDERBIRD_SETTINGS_LAST_SCANNED					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, THUNDERBIRD_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, THUNDERBIRD_SETTINGS_LAST_SCANNED);			//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					THUNDERBIRD_SETTINGS_SCAN_FOLDERS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, THUNDERBIRD_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, THUNDERBIRD_SETTINGS_SCAN_FOLDERS);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	
	//********************************************************************
	//**				IMPORT_EMAIL_THUNDERBIRD_FOLDERS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, IMPORT_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, IMPORT_EMAIL_THUNDERBIRD_FOLDERS);				//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				CONTACT_FAVORITES								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_INVISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_FAVORITES);							//Settings id.
	m_recSettings.setData(row, 5, 0);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				MOBILE_LAST_TAPI_DEVICE_ID								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, COMM_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, MOBILE_LAST_TAPI_DEVICE_ID);					//Settings id.
	m_recSettings.setData(row, 5, 0);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**				CALENDAR_VIEW_SETTINGS								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE_SETTINGS_INVISIBLE);				//Settings set id.
	m_recSettings.setData(row, 4, CALENDAR_VIEW_SETTINGS);						//Settings id.
	m_recSettings.setData(row, 5, 0);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, -1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**				REMINDER_CALENDAR_ENABLED								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, REMINDER_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, REMINDER_CALENDAR_ENABLED);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				REMINDER_CALENDAR_SETTINGS								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, REMINDER_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, REMINDER_CALENDAR_SETTINGS);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	
	//********************************************************************
	//**				REMINDER_TASK_ENABLED								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, REMINDER_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, REMINDER_TASK_ENABLED);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				REMINDER_TASK_SETTINGS								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, REMINDER_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, REMINDER_TASK_SETTINGS);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				REMINDER_APPSERVER_ENABLED								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, REMINDER_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, REMINDER_APPSERVER_ENABLED);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				REMINDER_APPSERVER_SETTINGS								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, REMINDER_SETTINGS);							//Settings set id.
	m_recSettings.setData(row, 4, REMINDER_APPSERVER_SETTINGS);					//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 3);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				CONTACT_DEFAULT_ROLE							**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_DEFAULT_ROLE);						//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**				PERSON_DEFAULT_ROLE								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_VISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, PERSON_DEFAULT_ROLE);							//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 2);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				TWO_SCREEN_REVERSED								**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, APPEARANCE);									//Settings set id.
	m_recSettings.setData(row, 4, TWO_SCREEN_REVERSED);							//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**				CONTACT_SEARCH_DIALOG_TAB						**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, CONTACT_SETTINGS_INVISIBLE);					//Settings set id.
	m_recSettings.setData(row, 4, CONTACT_SEARCH_DIALOG_TAB);					//Settings id.
	m_recSettings.setData(row, 5, 0);											//Settings visible.
	m_recSettings.setData(row, 6, 0);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					EMAIL_IN_SETTINGS_ACCOUNTS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_IN_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_IN_SETTINGS_ACCOUNTS);		//Settings id.
	m_recSettings.setData(row, 5, 1);									//Settings visible.
	m_recSettings.setData(row, 6, 3);									//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**					EMAIL_IN_SETTINGS_BLACKLIST					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_IN_SETTINGS);						//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_IN_SETTINGS_BLACKLIST);		//Settings id.
	m_recSettings.setData(row, 5, 1);									//Settings visible.
	m_recSettings.setData(row, 6, 2);									//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;
	
	//********************************************************************
	//**					DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, SETTINGS_DOCUMENT_DEFAULTS);					//Settings set id.
	m_recSettings.setData(row, 4, DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS);	//Settings id.
	m_recSettings.setData(row, 5, 1);											//Settings visible.
	m_recSettings.setData(row, 6, 1);											//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	m_recSettings.setData(row, 7, 1);											//Value for int or bool.
	//m_recSettings.setData(row, 8, "");										//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );											//Value for byte array.
	//m_recSettings.setData(row, 10, 1);										//Person ID (if application option leave empty, else fill on person insert).

	row++;


	//********************************************************************
	//**		EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_IN_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE);			//Settings id.
	m_recSettings.setData(row, 5, 1);												//Settings visible.
	m_recSettings.setData(row, 6, 0);												//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);												//Value for int or bool.
	//m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );												//Value for byte array.
	//m_recSettings.setData(row, 10, 1);											//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**		EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_IN_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE);			//Settings id.
	m_recSettings.setData(row, 5, 1);												//Settings visible.
	m_recSettings.setData(row, 6, 0);												//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);												//Value for int or bool.
	//m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );												//Value for byte array.
	//m_recSettings.setData(row, 10, 1);											//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**		EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_IN_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE);	//Settings id.
	m_recSettings.setData(row, 5, 1);												//Settings visible.
	m_recSettings.setData(row, 6, 0);												//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);												//Value for int or bool.
	//m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );												//Value for byte array.
	//m_recSettings.setData(row, 10, 1);											//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**	EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_EMAIL_LIST					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_IN_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_ALLOWED_EMAIL_LIST);		//Settings id.
	m_recSettings.setData(row, 5, 1);												//Settings visible.
	m_recSettings.setData(row, 6, 2);												//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);												//Value for int or bool.
	//m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );												//Value for byte array.
	//m_recSettings.setData(row, 10, 1);											//Person ID (if application option leave empty, else fill on person insert).

	row++;

	//********************************************************************
	//**	EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE_EMAIL_ACCOUNT_LIST					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_IN_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE_EMAIL_ACCOUNT_LIST);		//Settings id.
	m_recSettings.setData(row, 5, 1);												//Settings visible.
	m_recSettings.setData(row, 6, 2);												//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);												//Value for int or bool.
	//m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );												//Value for byte array.
	//m_recSettings.setData(row, 10, 1);											//Person ID (if application option leave empty, else fill on person insert).

	row++;

		//********************************************************************
	//**	EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE_EMAIL_ACCOUNT_LIST					**
	//********************************************************************
	m_recSettings.addRow();
	m_recSettings.setData(row, 3, EMAIL_IN_SETTINGS);								//Settings set id.
	m_recSettings.setData(row, 4, EMAIL_IN_SETTINGS_PUSH_ERROR_LAST_DATETIME);		//Settings id.
	m_recSettings.setData(row, 5, 1);												//Settings visible.
	m_recSettings.setData(row, 6, 2);												//Settings type (0 - int, 1 - bool, 2 - string, 3 - binary).
	//m_recSettings.setData(row, 7, 0);												//Value for int or bool.
	//m_recSettings.setData(row, 8, "");											//Value for string (Switzerland).
	//m_recSettings.setData(row, 9, );												//Value for byte array.
	//m_recSettings.setData(row, 10, 1);											//Person ID (if application option leave empty, else fill on person insert).

	row++;
	
}
