#ifndef HIERARCHICALHELPER_H
#define HIERARCHICALHELPER_H

#include <QString>
#include "common/common/dbrecordset.h"


/*!
	\class HierarchicalHelper
	\brief Class for hierarchical data manipulation
	\ingroup Bus_Core

	Use:
*/

class HierarchicalHelper 
{
public:
    HierarchicalHelper();
    ~HierarchicalHelper();


	//find childrens:
	static int SelectChildren(const QString &strParentCode, DbRecordSet &lstData, QString strCodeColumnName, bool bDestroyPreviousSelection=true, int nRowParent=-1);

	//common from HCT:
	static bool IsCodeDescendant(const QString &strCode, const QString &strAncestor);
	static bool IsCodeAncestor(const QString &strCode, const QString &strChild);
	static bool IsCodeParent(const QString &strCode, const QString &strChild);
	static QString CalcParentCode(const QString &strCode);

	//generate new code:
	static void		IncrementLastSegment(QString &strCode);
	static void		GenerateChildCode(QString &strCode);
	static int		CalcCodeSegmentsCount(const QString &strCode);
	static QString	CutToTheNumberOfSegments(const QString &strCode, int nSegments);

	//work on the lists sorted by CODE ASC
	static int		SortedListFindParentNodeIdx(DbRecordSet &lstData, QString strCode, int nCodeColIdx, int nStartFrom);
	static int		SortedListFindPrevSiblingNodeIdx(DbRecordSet &lstData, QString strCode, int nCodeColIdx, int nStartFrom, int nParentIdx = -1);

	//work on the recordset as returned by tree view
	static void		RemoveInvisibleTreeNodes(DbRecordSet &lstData, QString strCodeCol, QString strExpandedCol);

private:
    
};

#endif // HIERARCHICALHELPER_H
