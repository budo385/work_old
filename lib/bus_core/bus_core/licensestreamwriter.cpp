#include "licensestreamwriter.h"
//#include <openssl/err.h>
//#include <openssl/sha.h>
//#include <openssl/md5.h>
//#include <openssl/evp.h>
#include <QString>

#ifndef min
#define min(a, b) (((a)<(b))?(a):(b))
#endif

LicenseStreamWriter::LicenseStreamWriter()
{
	m_nDataLen = 0;
	m_pOutFile = NULL;
}

LicenseStreamWriter::~LicenseStreamWriter()
{
}

void LicenseStreamWriter::AttachSource(FILE *pInFile)
{
	m_pOutFile = pInFile;
}

void LicenseStreamWriter::SetPrivateKey(const char *szKey)
{
	m_key.ReadPrivateKeyFromBuffer(szKey, strlen(szKey));
}

bool LicenseStreamWriter::Write()
{
	//encrypt the contents
	if(m_nDataLen > 0 && NULL != m_pOutFile)
	{
		char szBuffer[10024];
		int nLen = m_key.Sign(m_szDecryptedData, m_nDataLen, szBuffer);
		if(nLen > 0)
		{
			int nWrite = fwrite(szBuffer, 1, nLen, m_pOutFile);
			if(nWrite > 0){
				m_nDataLen = 0;
				return true;
			}
		}
	}
	return false;
}

bool LicenseStreamWriter::WriteBytes(void *pszBuffer, int nLen)
{
	if(NULL == pszBuffer || nLen <= 0)
		return false;

	//TOFIX what is > 0 but less than
	if(m_nDataLen + nLen > sizeof(m_szDecryptedData))
		Write();

	//append data at the end of the buffer
	memcpy(m_szDecryptedData + m_nDataLen, pszBuffer, nLen);
	m_nDataLen += nLen;

	return true;
}

bool LicenseStreamWriter::WriteString(QString strData, int nLen)
{
#ifndef WINCE
	//zero the buffer, to adapt for strings shorter that len (zero padding)
	char szBuffer[1024];
	memset(szBuffer, 0, min(nLen, 1024));
		
	//copy data into temp buffer (up to len bytes)
	//DO not modify this! Qt has a bug!
	std::string strTmp = strData.toStdString();
	const char *szData = strTmp.c_str();
	//BUG: const char *szData =strData.toLatin1().constData(); //return  \0 terminated data B.T.
	strncpy(szBuffer, szData, min(nLen, strlen(szData)));
	return WriteBytes(szBuffer, nLen);
#endif

#ifdef WINCE
	return true;
#endif
}
