#ifndef COMMGRIDVIEWHELPER_H
#define COMMGRIDVIEWHELPER_H

#include <QObject>
#include <QHash>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "bus_core/bus_core/globalconstants.h"

class CommGridViewHelper : public QObject
{
	Q_OBJECT

public:
	enum GridType			//Numbered because it is going to be written in DB (for filter view type).
	{
		DESKTOP_GRID = 0,
		CONTACT_GRID = 1,
		PROJECT_GRID = 2, 
		CALENDAR_GRID= 3
	};

	enum AppType
	{
		VOICECALL,
		EMAIL,
		DOCUMENT
	};

	enum IncomingOutgoing
	{
		INHOUSE					= 0,
		OUTGOING				= 1,
		INCOMING				= 2
	};

	enum TaskType
	{
		NOT_SCHEDULED		= 1,
		SCHEDULED			= 2,
		SCHEDULED_DUENOW	= 3,
		SCHEDULED_OVERDUE	= 4
	};

	enum FirstColumn2IconSortIdentificators
	{
		SECOND_ICON_VOICECALL_SORT	= 1,
		SECOND_ICON_EMAIL_SORT		= 2,
		SECOND_ICON_DOCUMENT_SORT	= 3
	};

	enum FirstColumn3IconSortIdentificators
	{
		THIRD_ICON_INCOMING_SORT = 1,
		THIRD_ICON_OUTGOING_SORT = 2,
		THIRD_ICON_INHOUSE_SORT	 = 3
	};

	enum FirstColumn1IconSortIdentificators
	{
		NOT_SCHEDULED_SORT		= 1,
		SCHEDULED_SORT			= 2,
		SCHEDULED_DUENOW_SORT	= 3,
		SCHEDULED_OVERDUE_SORT	= 4,
		EMPTY_SORT				= 5,
		INCOMING_SORT			= 6,
		OUTGOING_SORT			= 7,
		INHOUSE_SORT			= 8, 
		TEMPLATE_SORT			= 9,
		CHECK_OUT_SORT			= 10,
		LOCKED_SORT				= 11,
		UNREAD_EMAIL_SORT		= 12,
		MISSED_CALL_SORT		= 13
	};

	enum CallState
	{
		//intermediate states
		CALL_STATE_None			=0,
		CALL_STATE_Ringing		=1,
		CALL_STATE_Routing		=2,
		CALL_STATE_OnHold		=3,
		CALL_STATE_InProgress	=4,
		CALL_STATE_Forwarded	=5,
		//final states
		CALL_STATE_Finished		=6,
		CALL_STATE_Canceled		=7,
		CALL_STATE_Failed		=8,
		CALL_STATE_Missed		=9,
		CALL_STATE_Refused		=10
	};

	enum FirstColumn4IconSortIdentificators
	{
		VOICECALL_SORT			= -1,
		EMAIL_SORT				= -2,
		DOCUMENT_SORT			= -3
	};

	static void	SetupSortRecordSet(DbRecordSet &lstSortColumnSetup);

	static int CheckTask(DbRecordSet &recEntityRecord,int nRow=0);
	static int CheckItemDirection(int nCEType, DbRecordSet &recEntityRecord,int nRow);
	static QString GetAppTypePixmap(int nAppType, int nRowsInGrid, QHash<int,int> &hshRow2APPTYPE);
	static QString GetApplicationTypePixmap(int nAppType);
	static QHash<int, QString> GetDocumentTypeIconNames();

	//can not be static...must make instance...
	QString GetCommGridFourthIcon(bool bTaskIconExists, int nAppType, int nRowsInGrid, int nRow, QString &strPix4, DbRecordSet recEntityRecord, QHash<int,int> &hshRow2APPTYPE, int nInOutDocument, bool bFromCalendar = false);
	virtual void GetDocApplicationsFromServer(DbRecordSet &lstData, Status &err)=0;
	int GetApplicationIconRowID(int nApplicationID, DbRecordSet &lstApps, Status &err);

	static QString GetTaskPixmap(int nTask, int nRowsInGrid, QHash<int,int> &hshRow2STATUS);
	static QString GetCETypePixmap(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord,int nRow, QHash<int,int> &hshRow2SUBTYPE);
	static QString GetInOutDocumentPixmap(int nInOutDocument, int nRowsInGrid, QHash<int,int> &hshRow2MEDIA);
	static QStringList CreateThirdColumnStrings(int nCEType, DbRecordSet &recEntityRecord,int nRow);

private:
};

#endif // COMMGRIDVIEWHELPER_H
