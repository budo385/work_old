#include "accuarcore.h"
#include "db_core/db_core/dbsqltableview.h"

void AccUARCore::UARList2DisplayList(DbRecordSet *lstUAR, DbRecordSet *lstGAR)
{
	if (lstUAR)
	{
		if (lstUAR->getColumnIdx("CX_READ")==-1)
		{
			lstUAR->addColumn(QVariant::String,"BPER_CODE");
			lstUAR->addColumn(QVariant::String,"BPER_FIRST_NAME");
			lstUAR->addColumn(QVariant::String,"BPER_LAST_NAME");
			lstUAR->addColumn(QVariant::String,"BPER_NAME");
			lstUAR->addColumn(QVariant::Int,"CX_READ");
			lstUAR->addColumn(QVariant::Int,"CX_MODIFY");
			lstUAR->addColumn(QVariant::Int,"CX_FULL");
			lstUAR->addColumn(QVariant::Int,"BGIT_ID");
			lstUAR->addColumn(QVariant::Int,"BGIT_TREE_ID");
			lstUAR->addColumn(QVariant::String,"BGIT_CODE");
			lstUAR->addColumn(QVariant::String,"BGIT_NAME");
			lstUAR->addColumn(QVariant::String,"BGTR_NAME");
		}
		//parse CX:
		int nSize=lstUAR->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			switch(lstUAR->getDataRef(i,"CUAR_RIGHT").toInt())
			{
			case ACC_LVL_NO_RIGHT:
				{
					lstUAR->setData(i,"CX_READ",0);
					lstUAR->setData(i,"CX_MODIFY",0);
					lstUAR->setData(i,"CX_FULL",0);
				}
				break;
			case ACC_LVL_READ:
				{
					lstUAR->setData(i,"CX_READ",1);
					lstUAR->setData(i,"CX_MODIFY",0);
					lstUAR->setData(i,"CX_FULL",0);
				}
				break;
			case ACC_LVL_MODIFY:
				{
					lstUAR->setData(i,"CX_READ",1);
					lstUAR->setData(i,"CX_MODIFY",1);
					lstUAR->setData(i,"CX_FULL",0);
				}
				break;
			default:
				{
					lstUAR->setData(i,"CX_READ",1);
					lstUAR->setData(i,"CX_MODIFY",1);
					lstUAR->setData(i,"CX_FULL",1);
				}
				break;
			}
		}
	}

	if (lstGAR)
	{
		if (lstGAR->getColumnIdx("CX_READ")==-1)
		{
			lstGAR->addColumn(QVariant::Int,"CX_READ");
			lstGAR->addColumn(QVariant::Int,"CX_MODIFY");
			lstGAR->addColumn(QVariant::Int,"CX_FULL");
			lstGAR->addColumn(QVariant::Int,"CX_PROPAGATE");
			lstGAR->addColumn(QVariant::Int,"BGIT_ID");
			lstGAR->addColumn(QVariant::Int,"BGIT_TREE_ID");
			lstGAR->addColumn(QVariant::String,"BGIT_CODE");
			lstGAR->addColumn(QVariant::String,"BGIT_NAME");
			lstGAR->addColumn(QVariant::Int,"BGIT_LEVEL");
			lstGAR->addColumn(QVariant::String,"BGIT_ICON");
			lstGAR->addColumn(QVariant::Int,"BGIT_PARENT");
			lstGAR->addColumn(QVariant::String,"BGTR_NAME");
		}

		//parse CX:
		int nSize=lstGAR->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			switch(lstGAR->getDataRef(i,"CGAR_RIGHT").toInt())
			{
			case ACC_LVL_NO_RIGHT:
				{
					lstGAR->setData(i,"CX_READ",0);
					lstGAR->setData(i,"CX_MODIFY",0);
					lstGAR->setData(i,"CX_FULL",0);
				}
				break;
			case ACC_LVL_READ:
				{
					lstGAR->setData(i,"CX_READ",1);
					lstGAR->setData(i,"CX_MODIFY",0);
					lstGAR->setData(i,"CX_FULL",0);
				}
				break;
			case ACC_LVL_MODIFY:
				{
					lstGAR->setData(i,"CX_READ",1);
					lstGAR->setData(i,"CX_MODIFY",1);
					lstGAR->setData(i,"CX_FULL",0);
				}
				break;
			default:
				{
					lstGAR->setData(i,"CX_READ",1);
					lstGAR->setData(i,"CX_MODIFY",1);
					lstGAR->setData(i,"CX_FULL",1);
				}
				break;
			}
		}

		lstGAR->setColValue(lstGAR->getColumnIdx("CX_PROPAGATE"),0);
	}
}

//from chkboxes to the data:
void AccUARCore::DisplayList2UAR(DbRecordSet *lstUAR, DbRecordSet *lstGAR)
{
	if (lstUAR)
	{
		if (lstUAR->getColumnIdx("CX_READ")==-1)
		{
			lstUAR->addColumn(QVariant::String,"BPER_CODE");
			lstUAR->addColumn(QVariant::String,"BPER_FIRST_NAME");
			lstUAR->addColumn(QVariant::String,"BPER_LAST_NAME");
			lstUAR->addColumn(QVariant::String,"BPER_NAME");
			lstUAR->addColumn(QVariant::Int,"CX_READ");
			lstUAR->addColumn(QVariant::Int,"CX_MODIFY");
			lstUAR->addColumn(QVariant::Int,"CX_FULL");
			lstUAR->addColumn(QVariant::Int,"BGIT_ID");
			lstUAR->addColumn(QVariant::Int,"BGIT_TREE_ID");
			lstUAR->addColumn(QVariant::String,"BGIT_CODE");
			lstUAR->addColumn(QVariant::String,"BGIT_NAME");
			lstUAR->addColumn(QVariant::String,"BGTR_NAME");
		}

		//parse CX:
		int nSize=lstUAR->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nMaxRight=ACC_LVL_NO_RIGHT;
			if (lstUAR->getDataRef(i,"CX_READ").toInt()>0)
				nMaxRight=ACC_LVL_READ;
			if (lstUAR->getDataRef(i,"CX_MODIFY").toInt()>0)
				nMaxRight=ACC_LVL_MODIFY;
			if (lstUAR->getDataRef(i,"CX_FULL").toInt()>0)
				nMaxRight=ACC_LVL_FULL;
			lstUAR->setData(i,"CUAR_RIGHT",nMaxRight);
		}
	}


	if (lstGAR)
	{
		if (lstGAR->getColumnIdx("CX_READ")==-1)
		{
			lstGAR->addColumn(QVariant::Int,"CX_READ");
			lstGAR->addColumn(QVariant::Int,"CX_MODIFY");
			lstGAR->addColumn(QVariant::Int,"CX_FULL");
			lstGAR->addColumn(QVariant::Int,"CX_PROPAGATE");
			lstGAR->addColumn(QVariant::Int,"BGIT_ID");
			lstGAR->addColumn(QVariant::Int,"BGIT_TREE_ID");
			lstGAR->addColumn(QVariant::String,"BGIT_CODE");
			lstGAR->addColumn(QVariant::String,"BGIT_NAME");
			lstGAR->addColumn(QVariant::Int,"BGIT_LEVEL");
			lstGAR->addColumn(QVariant::String,"BGIT_ICON");
			lstGAR->addColumn(QVariant::Int,"BGIT_PARENT");
			lstGAR->addColumn(QVariant::String,"BGTR_NAME");
		}

		//parse CX:
		int nSize=lstGAR->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nMaxRight=ACC_LVL_NO_RIGHT;
			if (lstGAR->getDataRef(i,"CX_READ").toInt()>0)
				nMaxRight=ACC_LVL_READ;
			if (lstGAR->getDataRef(i,"CX_MODIFY").toInt()>0)
				nMaxRight=ACC_LVL_MODIFY;
			if (lstGAR->getDataRef(i,"CX_FULL").toInt()>0)
				nMaxRight=ACC_LVL_FULL;
			lstGAR->setData(i,"CGAR_RIGHT",nMaxRight);
		}
	}

	//lstGAR->setColValue(lstGAR->getColumnIdx("CX_PROPAGATE"),1);
}

//based on lstGAR CGAR_RIGHT, find all lstUAR that are groupDep and belong into group update max right
//if nUARRow=-1, traverse whole UAR list else just that row
//lstGroupUsers <BPER_ID,BPER_CODE,BPER_FIRST_NAME,BPER_LAST_NAME,BGIT_ID,BGIT_CODE,BGIT_NAME,BGIT_TREE_ID,BGTR_NAME,CGAR_RIGHT, BPER_NAME> 
void AccUARCore::UpdateUARGroupDependentRights(DbRecordSet &lstGroupUsers,DbRecordSet &lstUAR, DbRecordSet &lstGAR, int nUARRow)
{
	//lstGAR holds current CGAR_RIGHT->copy on CUAR_RIGHT for each user
	int nSize=lstGAR.getRowCount();
	int nSize1=lstGroupUsers.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nGroupID=lstGAR.getDataRef(i,"CGAR_GROUP_ID").toInt();
		int nRight=lstGAR.getDataRef(i,"CGAR_RIGHT").toInt();
		for(int k=0;k<nSize1;k++)
		{
			if (lstGroupUsers.getDataRef(k,"BGIT_ID").toInt()==nGroupID)
				lstGroupUsers.setData(k,"CGAR_RIGHT",nRight); //copy right to Ret_lstGroupUsers data
		}
	}

	//sort by Pers ID, Right: when user is found, just update from lowest record (with MAX RIGHTS):
	SortDataList lstSort;
	lstSort<<SortData(lstGroupUsers.getColumnIdx("BPER_ID"),0); //ASC
	lstSort<<SortData(lstGroupUsers.getColumnIdx("CGAR_RIGHT"),1); //DESC
	lstGroupUsers.sortMulti(lstSort);

	//GET GROUP RIGHTS FOR 1 USER:
	if (nUARRow!=-1)
	{
		int nPersID=lstUAR.getDataRef(nUARRow,"CUAR_PERSON_ID").toInt();
		int nRow=lstGroupUsers.find("BPER_ID",nPersID,true); 
		if (nRow>=0)
		{
			if (lstUAR.getDataRef(nUARRow,"CUAR_GROUP_DEPENDENT").toInt()==1) //only if group dep = 1
			{
				DbRecordSet row = lstGroupUsers.getRow(nRow);
				lstUAR.assignRow(nUARRow,row,true);
				lstUAR.setData(nUARRow,"CUAR_RIGHT",lstGroupUsers.getDataRef(nRow,"CGAR_RIGHT").toInt());
			}
			else
			{ //clear data: if gone from GAR->UAR
				lstUAR.setData(nUARRow,"BGIT_ID",0);
				lstUAR.setData(nUARRow,"BGIT_CODE","");
				lstUAR.setData(nUARRow,"BGIT_NAME","");
				lstUAR.setData(nUARRow,"BGIT_TREE_ID",0);
				lstUAR.setData(nUARRow,"BGTR_NAME","");
			}
		}
		else if (lstUAR.getDataRef(nUARRow,"CUAR_GROUP_DEPENDENT").toInt()==1) //if marked as group dep and not found: delete it
			lstUAR.deleteRow(nUARRow);
		return;
	}

	//UPDATE OR ADD NEW UAR RECORDS:
	int nPersID=-1;
	nSize=lstGroupUsers.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (nPersID!=lstGroupUsers.getDataRef(i,"BPER_ID").toInt())
		{
			nPersID=lstGroupUsers.getDataRef(i,"BPER_ID").toInt();
			nUARRow=lstUAR.find("CUAR_PERSON_ID",nPersID,true);
			if (nUARRow>=0)
			{
				if (lstUAR.getDataRef(nUARRow,"CUAR_GROUP_DEPENDENT").toInt()==1) //only if group dep = 1
				{
					DbRecordSet row = lstGroupUsers.getRow(i);
					lstUAR.assignRow(nUARRow,row,true);
					lstUAR.setData(nUARRow,"CUAR_RIGHT",lstGroupUsers.getDataRef(i,"CGAR_RIGHT").toInt());
				}
				else
				{ //clear data: if gone from GAR->UAR
					lstUAR.setData(nUARRow,"BGIT_ID",0);
					lstUAR.setData(nUARRow,"BGIT_CODE","");
					lstUAR.setData(nUARRow,"BGIT_NAME","");
					lstUAR.setData(nUARRow,"BGIT_TREE_ID",0);
					lstUAR.setData(nUARRow,"BGTR_NAME","");
				}
			}
			else
			{
				lstUAR.merge(lstGroupUsers.getRow(i));
				int nLast=lstUAR.getRowCount()-1;
				lstUAR.setData(nLast,"CUAR_PERSON_ID",nPersID);
				lstUAR.setData(nLast,"CUAR_GROUP_DEPENDENT",1);
				lstUAR.setData(nLast,"CUAR_RIGHT",lstGroupUsers.getDataRef(i,"CGAR_RIGHT").toInt());
			}
		}
	}
}

//create UAR FROM Contact and Persons cache
void AccUARCore::GenerateNewUARFromContacts(int nRecord, int nTableID,DbRecordSet &lstContacts, DbRecordSet &lstUAR, DbRecordSet &lstPersons)
{
	DefineDisplayUARList(lstUAR);

	int nSize=lstContacts.getRowCount();
	lstContacts.clearSelection();
	for(int i=0;i<nSize;i++)
	{
		int nRow=lstPersons.find("BPER_CONTACT_ID",lstContacts.getDataRef(i,"BCNT_ID").toInt(),true);
		if (nRow>=0)
		{
			lstUAR.addRow();
			int nLast=lstUAR.getRowCount()-1;
			lstUAR.setData(nLast,"CUAR_PERSON_ID",lstPersons.getDataRef(nRow,"BPER_ID").toInt());
			lstUAR.setData(nLast,"CUAR_GROUP_DEPENDENT",0);
			lstUAR.setData(nLast,"CUAR_RIGHT",AccUARCore::ACC_LVL_FULL);
			DbRecordSet row = lstPersons.getRow(nRow);
			lstUAR.assignRow(nLast,row,true);
		}
	}

	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_RECORD_ID"),nRecord);
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_TABLE_ID"),nTableID);
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_CHILD_INHERIT"),0);
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_CHILD_COMM_INHERIT"),0);
}

void AccUARCore::GenerateNewGARFromGroups(int nRecord, int nTableID,DbRecordSet &lstGroups, DbRecordSet &lstGAR)
{
	DefineDisplayGARList(lstGAR);

	lstGAR.merge(lstGroups);
	int nSize=lstGroups.getRowCount();
	for(int i=0;i<nSize;i++)
		lstGAR.setData(i,"CGAR_GROUP_ID",lstGAR.getDataRef(i,"BGIT_ID"));

	lstGAR.setColValue(lstGAR.getColumnIdx("CGAR_RECORD_ID"),nRecord);
	lstGAR.setColValue(lstGAR.getColumnIdx("CGAR_TABLE_ID"),nTableID);
	lstGAR.setColValue(lstGAR.getColumnIdx("CGAR_CHILD_INHERIT"),0);
	lstGAR.setColValue(lstGAR.getColumnIdx("CGAR_CHILD_COMM_INHERIT"),0);
	lstGAR.setColValue("CGAR_RIGHT",AccUARCore::ACC_LVL_FULL);
}

void AccUARCore::DefineDisplayUARList(DbRecordSet &lstUAR)
{
	lstUAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	lstUAR.addColumn(QVariant::String,"BPER_CODE");
	lstUAR.addColumn(QVariant::String,"BPER_FIRST_NAME");
	lstUAR.addColumn(QVariant::String,"BPER_LAST_NAME");
	lstUAR.addColumn(QVariant::String,"BPER_NAME");
	lstUAR.addColumn(QVariant::Int,"CX_READ");
	lstUAR.addColumn(QVariant::Int,"CX_MODIFY");
	lstUAR.addColumn(QVariant::Int,"CX_FULL");
	lstUAR.addColumn(QVariant::Int,"BGIT_ID");
	lstUAR.addColumn(QVariant::Int,"BGIT_TREE_ID");
	lstUAR.addColumn(QVariant::String,"BGIT_CODE");
	lstUAR.addColumn(QVariant::String,"BGIT_NAME");
	lstUAR.addColumn(QVariant::String,"BGTR_NAME");
}
void AccUARCore::DefineDisplayGARList(DbRecordSet &lstGAR)
{
	lstGAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_GROUP_REC));
	lstGAR.addColumn(QVariant::Int,"CX_READ");
	lstGAR.addColumn(QVariant::Int,"CX_MODIFY");
	lstGAR.addColumn(QVariant::Int,"CX_FULL");
	lstGAR.addColumn(QVariant::Int,"CX_PROPAGATE");
	lstGAR.addColumn(QVariant::Int,"BGIT_ID");
	lstGAR.addColumn(QVariant::Int,"BGIT_TREE_ID");
	lstGAR.addColumn(QVariant::String,"BGIT_CODE");
	lstGAR.addColumn(QVariant::String,"BGIT_NAME");
	lstGAR.addColumn(QVariant::Int,"BGIT_LEVEL");
	lstGAR.addColumn(QVariant::String,"BGIT_ICON");
	lstGAR.addColumn(QVariant::Int,"BGIT_PARENT");
	lstGAR.addColumn(QVariant::String,"BGTR_NAME");
}


//generate new UAR/GAR records from source and assigne whole UAR/GAR set of records to each new RECORD_ID found in list lstOfRecordIDs<strRecordIdColName>
//lstUAR_Source must have valid CUAR_ID and lstGAR_Source valid CGAR_ID
void AccUARCore::CopyGUAR(int nTableID,DbRecordSet lstOfRecordIDs, QString strRecordIdColName,const DbRecordSet &lstUAR_Source, const DbRecordSet &lstGAR_Source,DbRecordSet &lstUAR, DbRecordSet &lstGAR)
{
	lstUAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	lstGAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_GROUP_REC));

	int nSize=lstOfRecordIDs.getRowCount();
	int nSizeUAR=lstUAR_Source.getRowCount();
	int nSizeGAR=lstGAR_Source.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		//copy all UAR, set new  record ID
		//for each set parent = source ID except if group dep.
		for(int k=0;k<nSizeUAR;k++)
		{
			lstUAR.merge(lstUAR_Source.getRow(k));
			int nLast=lstUAR.getRowCount()-1;
			lstUAR.setData(nLast,"CUAR_RECORD_ID",lstOfRecordIDs.getDataRef(i,strRecordIdColName).toInt());
			if (lstUAR.getDataRef(nLast,"CUAR_GROUP_DEPENDENT").toInt()==0)
			{
				if (lstUAR_Source.getDataRef(k,"CUAR_PARENT_ID").toInt()>0)
					lstUAR.setData(nLast,"CUAR_PARENT_ID",lstUAR_Source.getDataRef(k,"CUAR_PARENT_ID").toInt()); //BT: if parent has parent, use that!!!
				else
					lstUAR.setData(nLast,"CUAR_PARENT_ID",lstUAR_Source.getDataRef(k,"CUAR_ID").toInt());
			}
			lstUAR.setData(nLast,"CUAR_ID",0); //reset ID
		}

		//copy all GAR, set new  record ID
		//for each set parent = source ID except if group dep.
		for(int k=0;k<nSizeGAR;k++)
		{
			lstGAR.merge(lstGAR_Source.getRow(k));
			int nLast=lstGAR.getRowCount()-1;
			lstGAR.setData(nLast,"CGAR_RECORD_ID",lstOfRecordIDs.getDataRef(i,strRecordIdColName).toInt());
			if (lstGAR_Source.getDataRef(k,"CGAR_PARENT_ID").toInt()>0)
				lstGAR.setData(nLast,"CGAR_PARENT_ID",lstGAR_Source.getDataRef(k,"CGAR_PARENT_ID").toInt()); //BT: if parent has parent, use that!!!
			else
				lstGAR.setData(nLast,"CGAR_PARENT_ID",lstGAR_Source.getDataRef(k,"CGAR_ID").toInt());

			lstGAR.setData(nLast,"CGAR_ID",0); //reset ID
		}

	}

	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_TABLE_ID"),nTableID);
	lstGAR.setColValue(lstGAR.getColumnIdx("CGAR_TABLE_ID"),nTableID);
}

//define special group/user list:
//BPER_ID,BPER_CODE,BPER_FIRST_NAME,BPER_LAST_NAME,BGIT_ID,BGIT_CODE,BGIT_NAME,BGIT_TREE_ID,BGTR_NAME,CGAR_RIGHT,BPER_NAME
void AccUARCore::DefineGroupUserList(DbRecordSet &lstGroupUser)
{
	lstGroupUser.destroy();
	lstGroupUser.addColumn(QVariant::Int,"BPER_ID");
	lstGroupUser.addColumn(QVariant::String,"BPER_CODE");
	lstGroupUser.addColumn(QVariant::String,"BPER_FIRST_NAME");
	lstGroupUser.addColumn(QVariant::String,"BPER_LAST_NAME");
	lstGroupUser.addColumn(QVariant::Int,"BGIT_ID");
	lstGroupUser.addColumn(QVariant::String,"BGIT_CODE");
	lstGroupUser.addColumn(QVariant::String,"BGIT_NAME");
	lstGroupUser.addColumn(QVariant::Int,"BGIT_LEVEL");
	lstGroupUser.addColumn(QVariant::String,"BGIT_ICON");
	lstGroupUser.addColumn(QVariant::Int,"BGIT_PARENT");
	lstGroupUser.addColumn(QVariant::Int,"BGIT_TREE_ID");
	lstGroupUser.addColumn(QVariant::String,"BGTR_NAME");
	lstGroupUser.addColumn(QVariant::Int,"CGAR_RIGHT");
	lstGroupUser.addColumn(QVariant::String,"BPER_NAME");
}

//based on records, returns DisplayLevel level:
//-private: if 2 UAR, 0 GAR and 1 UAR pers = rights=max, 1 UAR all=no right
//-others can read: if 2 UAR, 0 GAR and 1 UAR pers = rights=max, 1 UAR all=read
//-others can modify (full acccess right): no recs at all or 1 UAR, all=full
//-individual: all rest...
int AccUARCore::Record2DisplayLevel(int nLoggedPersonID,DbRecordSet &lstUAR, DbRecordSet &lstGAR)
{
	if (lstUAR.getRowCount()==0 && lstGAR.getRowCount()==0)
		return AccUARCore::ACC_OTHERS_CAN_MODIFY;
	if (lstUAR.getRowCount()==1)
		if (lstUAR.getDataRef(0,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_FULL && lstUAR.getDataRef(0,"CUAR_PERSON_ID").isNull())
			return AccUARCore::ACC_OTHERS_CAN_MODIFY;

	if (lstUAR.getRowCount()==2 && lstGAR.getRowCount()==0)
	{
		//at row 0-per 1-all
		if (lstUAR.getDataRef(0,"CUAR_PERSON_ID").toInt()==nLoggedPersonID && lstUAR.getDataRef(1,"CUAR_PERSON_ID").isNull() )
		{
			if (lstUAR.getDataRef(0,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_FULL && lstUAR.getDataRef(1,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_READ)
				return AccUARCore::ACC_OTHERS_CAN_READ;
			if (lstUAR.getDataRef(0,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_FULL && lstUAR.getDataRef(1,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_NO_RIGHT)
				return AccUARCore::ACC_PRIVATE;
		}//at row 0-all, 1-per
		else if (lstUAR.getDataRef(1,"CUAR_PERSON_ID").toInt()==nLoggedPersonID && lstUAR.getDataRef(0,"CUAR_PERSON_ID").isNull())
		{
			if (lstUAR.getDataRef(1,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_FULL && lstUAR.getDataRef(0,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_READ)
				return AccUARCore::ACC_OTHERS_CAN_READ;
			if (lstUAR.getDataRef(1,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_FULL && lstUAR.getDataRef(0,"CUAR_RIGHT").toInt()==AccUARCore::ACC_LVL_NO_RIGHT)
				return AccUARCore::ACC_PRIVATE;
		}
	}

	return AccUARCore::ACC_INDIVIDUAL;
}

void AccUARCore::DisplayLevel2Record(int nDisplayLevel,int nLoggedPersonID,int nRecordID,int nTableID,DbRecordSet &lstUAR, DbRecordSet &lstGAR)
{
	switch(nDisplayLevel)
	{
	case AccUARCore::ACC_PRIVATE:
		{
			lstGAR.clear();
			lstUAR.clear();
			lstUAR.addRow(2);
			lstUAR.setData(0,"CUAR_RIGHT",QVariant(AccUARCore::ACC_LVL_NO_RIGHT).toInt());
			lstUAR.setData(0,"CUAR_PERSON_ID",QVariant(QVariant::Int)); //null
			lstUAR.setData(1,"CUAR_RIGHT",QVariant(AccUARCore::ACC_LVL_FULL).toInt());
			lstUAR.setData(1,"CUAR_PERSON_ID",nLoggedPersonID); 
		}
		break;
	case AccUARCore::ACC_OTHERS_CAN_READ:
		{
			lstGAR.clear();
			lstUAR.clear();
			lstUAR.addRow(2);
			lstUAR.setData(0,"CUAR_RIGHT",QVariant(AccUARCore::ACC_LVL_READ).toInt());
			lstUAR.setData(0,"CUAR_PERSON_ID",QVariant(QVariant::Int)); //null
			lstUAR.setData(1,"CUAR_RIGHT",QVariant(AccUARCore::ACC_LVL_FULL).toInt());
			lstUAR.setData(1,"CUAR_PERSON_ID",nLoggedPersonID); 
		}
		break;
	case AccUARCore::ACC_OTHERS_CAN_MODIFY: //to enable clear of previous data:
		{
			lstGAR.clear();
			lstUAR.clear(); //set full access to all others: this row is just 
			lstUAR.addRow(1);
			lstUAR.setData(0,"CUAR_RIGHT",QVariant(AccUARCore::ACC_LVL_FULL).toInt());
			lstUAR.setData(0,"CUAR_PERSON_ID",QVariant(QVariant::Int)); //null
		}
		break;
	default:
		return; //leave as is if individual 
		break;
	}

	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_RECORD_ID"),nRecordID);
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_TABLE_ID"),nTableID);
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_CHILD_INHERIT"),0);
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_CHILD_COMM_INHERIT"),0);
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_GROUP_DEPENDENT"),0);
}



//test based on lstuar/gar if person is allowed to modify uar/gar
bool AccUARCore::IsUserAllowedToModifyUAR(int nLoggedPersonID,DbRecordSet &lstUAR, DbRecordSet &lstGAR)
{
	if (lstUAR.getRowCount()==0 && lstGAR.getRowCount()==0) //no protection: any1 can modify all
		return true;

	//UAR:
	int nRow=lstUAR.find("CUAR_PERSON_ID",0,true); //test all users
	if (nRow>=0)
	{
		if (lstUAR.getDataRef(nRow,"CUAR_RIGHT").toInt()>=ACC_LVL_FULL)
			return true;
	}
	nRow=lstUAR.find("CUAR_PERSON_ID",nLoggedPersonID,true); //test for logged user
	if (nRow>=0)
	{
		if (lstUAR.getDataRef(nRow,"CUAR_RIGHT").toInt()>=ACC_LVL_FULL)
			return true;
	}

	return false;
}


void AccUARCore::CreatePrivateUAR(int nLoggedPersonID,int nRecordID,int nTableID,DbRecordSet &lstUAR, DbRecordSet &lstGAR)
{
	lstUAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	lstGAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_GROUP_REC));
	AccUARCore::DisplayLevel2Record(ACC_PRIVATE,nLoggedPersonID,nRecordID,nTableID,lstUAR,lstGAR);
}

