#ifndef USERACCESSRIGHT_H
#define USERACCESSRIGHT_H

#include <QHash>
#include <QReadWriteLock>
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "bus_core/bus_core/accessrightsid.h"


/*!
\class UserAccessRight
\brief Global access rights container class. This is abstract class
\ingroup Bus_Core/AccessRights

Global access rights container class. Caches access rights for all logged users.
Multithread safe. If same user is logged twice, data is held in same hash object: be carefull when deleting


Use:
- inherit and implement sepcific DB access by implementing LoadARRecordSet() function
- implement GetUserID() which is different for server, thick client and thin client
- session manager is used to provide user context, so there is no need to send user id every time
- instance as global object on client and server side

*/
class UserARData;

class UserAccessRight
{
public:
	UserAccessRight();
	virtual ~UserAccessRight();

	enum Operation 
	{
		OP_READ,	
		OP_EDIT,			
		OP_INSERT,			
		OP_DELETE 
	};

	//before SQL execute, expand UAR to filter records that are not allowed to be read by current logged user!
	void SQLFilterRecords(int nTableID, QString &strSQL);

	//UAR system LVL2+1: uses UAR/GAR tables to test AR settings per person per record id for given operation (insert, edit, read, delete)
	bool IsOperationAllowed(Status &Ret_pStatus,int nTableID, int nOperation,DbRecordSet &lstData);
	bool IsOperationAllowed(Status &Ret_pStatus,int nTableID, int nOperation,int nDataRecordID=-1);

	//AR system LVL1: uses ACCESSRIGHTSID to test AR setting per user
	//init AR cache
	void SetUserARRecordSet(int UserID,DbRecordSet &ARRecordSet,bool bIsSystemUser=false);
	void DeleteUserARRecordSet(int UserID);
	bool TestAR(int ARCode, bool bReturnOKIfNoExist=false);
	bool TestAR(int ARCode, int IntValue);
	bool TestAR(int ARCode, QString StringValue);

protected:
	virtual int		GetUserID()=0;		//implement to get current user id for current thread
	virtual int		GetPersonID()=0;	//implement to get current person id for current thread
	virtual bool	IsSystemUser()=0;	//implement to get current person id for current thread
	virtual void	TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed=true)=0; //read/test UAR on DB

private:
	QList<int>		GetARCodes(DbRecordSet &ARRecordSet);
	QString			GetARValue(int ARCode, DbRecordSet &AccRRecordSet);
	void			AddUserARRecordSet(int UserID,DbRecordSet &AccRRecordSet,bool bIsSystemUser);
	bool			IsOperationAllowed_Lvl_1(int nTableID, int nOperation);

	QHash<int, UserARData*> m_hshUserARHash;    //< User to access right hash.
	QReadWriteLock m_RWMutex;
};



/*!
	\class UserARData
	\brief Used for Access Rights keeping.
	\ingroup Bus_Core

	Container class for holding single user access rights.
*/
class UserARData
{
public:
	UserARData(QHash<int, QString> ARHash,bool bIsSystemUser=false);
	~UserARData();

	QString GetARValue(int ARCode);
	bool m_bIsSystemUser;

private:
	QHash<int, QString>	m_hshARHash;			//< Access rights code to value hash.
};




#endif // USERACCESSRIGHT_H
