#ifndef MESSAGEDISPATCHER_H
#define MESSAGEDISPATCHER_H

#include <QObject>
#include <QReadWriteLock>
#include <QStringList>
#include <QTimerEvent>
#include "common/common/dbrecordset.h"
#include "common/common/threadspawner.h"

class MessageDispatcher : public ThreadSpawnedObject
{
	Q_OBJECT

public:
	MessageDispatcher(QObject *parent=NULL);
	virtual ~MessageDispatcher();

	int SendMessageX(int nType,int nMessageCode, QString strTextArguments,QStringList *lstTargetPersonIDs=NULL, int nLoggedPersonSocket=-1,int nMessageID=-1,QDateTime datExpireDateTime=QDateTime(),QDateTime datNotifyDateTime=QDateTime());
	void GetMessages(int nPersonID, DbRecordSet &lstMessages);
	void ClearUserSocketFromMessages(int nUserSocket);

signals:
	void NewMessageIncoming(int nMsgID);
	void StartMsgTimer(int nMsgID);

protected slots:
	virtual void NewMessage(int nMsgID)=0;
	void OnStartTimer(int);

protected:
	void timerEvent ( QTimerEvent * event ); 
	void ClearExpiredMessages();
	DbRecordSet m_lstMessages; //<MSG_CREATED, MSG_TYPE<0-info,1-warning,2-error>,MSG_TEXT,MSG_ARGS,MSG_EXPIRE_DATE,MSG_TARGET_USERS>
	QReadWriteLock m_mutex;
	double m_nMsgID;				//always incremental
};

#endif // MESSAGEDISPATCHER_H
