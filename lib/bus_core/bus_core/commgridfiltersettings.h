#ifndef COMMGRIDFILTERSETTINGS_H
#define COMMGRIDFILTERSETTINGS_H

#include "common/common/dbrecordset.h"

class CommGridFilterSettings
{
public:
	CommGridFilterSettings();
	~CommGridFilterSettings();

	DbRecordSet GetCommFilterSettings();
	DbRecordSet GetCalendarTasksFilterSettings();		//Used for calendar.
	DbRecordSet GetCalendarGridTasksFilterSettings();	//Used for calendar grid on left.

private:
	DbRecordSet m_recCommFilterSettings;

	void InitializeCommFilterSettings();
	void InitializeCalendarTaskFilterSettings();
	void InitializeCalendarGridTaskFilterSettings();
};

#endif // COMMGRIDFILTERSETTINGS_H
