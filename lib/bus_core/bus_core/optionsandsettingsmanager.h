#ifndef OPTIONSANDSETTINGSMANAGER_H
#define OPTIONSANDSETTINGSMANAGER_H

#include "common/common/dbrecordset.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include <QReadWriteLock>



/*!
	\class  OptionsAndSettingsManager
	\ingroup Bus_Core
	\brief  Thread Safe class for managing user/application options/settings
*/

class OptionsAndSettingsManager
{
public:
	OptionsAndSettingsManager();
	~OptionsAndSettingsManager();

	void		SetOptionsDataSource(const DbRecordSet *recOptions);
	void		SetSettingsDataSource(const DbRecordSet *recSettings, int nPersonID);
	void		GetDataSource(DbRecordSet &recOptions, DbRecordSet &recSettings);

	//User settings and application options.
	QVariant	GetPersonSetting(int nSettingID);
	DbRecordSet	GetPersonSettingRow(int nSettingID);
	DbRecordSet	GetPersonSettingSet(int nSettingSetID);
	void		SetPersonSetting(int nSettingID, QVariant varValue);


	QVariant	GetApplicationOption(int nOptionID);
	DbRecordSet	GetApplicationOptionRow(int nOptionID);
	DbRecordSet	GetApplicationOptionSet(int nSettingSetID);
	void		SetApplicationOption(int nSettingID, QVariant varValue);
	
	int			GetPersonID(){return m_nPersonID;};

protected:

	//Get's person setting and if it doesn't exist it inserts it in cache.
	int 		GetPersonSettingRowIdx(int nSettingID);
	int 		GetApplicationOptionRowIdx(int nOptionID);
	void		TestOptions();
	void		TestSettings();

	int			m_nPersonID;
	DbRecordSet m_recSettings;
	DbRecordSet m_recOptions;
	QList<int>	m_lstChangedSettingsID;
	QReadWriteLock m_lckSettings;
	QReadWriteLock m_lckOptions;

};

#endif // OPTIONSANDSETTINGSMANAGER_H
