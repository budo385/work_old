#ifndef USERSESSIONMANAGER_H
#define USERSESSIONMANAGER_H

#include "common/common/status.h"
#include "usersessiondata.h"
#include "trans/trans/httpcontext.h"
#include "licenseaccessrightsbase.h"



/*!
    \class UserSessionManager
    \brief Used for managing user sessions on server, abstract class
    \ingroup Bus_Server

	Defines interface for usermanager. Usermanagre is different for thin & thick mode (same interface).
	While in thin mode it works with multiple threads each representing one client, in thick mode, it only has one.

*/
class UserSessionManager
{

public:

	UserSessionManager(LicenseAccessRightsBase *pModuleLicenseManager){m_pModuleLicense=pModuleLicenseManager;};
	virtual ~UserSessionManager(){};

	//managing functions
	virtual void CreateSession(Status &pStatus,QString &strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID,bool bIsSystemThread=false,QString strParentSession="", int nClientTimeZoneOffsetMinutes=0)=0;
	virtual void CreateSessionForWebService(Status &pStatus,QString &strSession,DbRecordSet rowUserData, int nClientTimeZoneOffsetMinutes=0)=0;

	virtual void ValidateUserRequest(Status &pStatus,QString strSession, int &nClientTimeZoneOffsetMinutes, QString strClientIP="",bool IsLogin=false,int nSocketID=0)=0;
	virtual bool AuthenticateUser(QString strCookieValue,QString &strSessionID)=0;
	virtual void ValidateWebUserRequest(Status &pStatus,QString strSession,const HTTPContext &ctx, int &nClientTimeZoneOffsetMinutes)=0;

	virtual void DeleteSession(Status &pStatus, QString strSession)=0;
	virtual void DeleteSessionByThreadID(Status &pStatus,int nThreadID)=0;		
	virtual void InactiveSession(Status &pStatus,int nThreadID)=0;		
	virtual void UpdateSession(Status &pStatus,QString strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID)=0;

	//garbage collector functions
	virtual void DeleteExpiredUserSessions(Status &pStatus,UserSessionList &pListExpiredSessions)=0;
	virtual void DeleteOrphanUserSessions(Status &err,UserSessionList lstToDelete)=0;
	virtual void TestExpiredUserSessions(Status &err,UserSessionList &pListExpiredSessions)=0;
	virtual void GetSessionList(UserSessionList &pListSessions)=0; 
	//IsAlive funct (returns timeout):
	virtual int GetIsAliveInterval();
	virtual int GetIsAliveCheckInterval();
	virtual void ClientIsAlive(int nSocketID)=0;

	//context helper functions
	//virtual int GetUniqueUserContextID()=0;
	virtual QString GetSessionID()=0;
	virtual int GetUserID(QString strSession="")=0;
	virtual int GetSessionRecordID(QString strSession="")=0;
	virtual int GetPersonID(QString strSession="")=0;
	virtual int GetContactID(QString strSession="")=0;
	virtual bool IsSystemUser(QString strSession="")=0;
	virtual int GetSocketID(QString strSession="")=0;
	virtual QString GetClientIP(QString strSession="")=0;
	virtual void GetUserData(DbRecordSet &rowUserData,QString strSession="")=0;
	virtual void GetModuleInfo(QString& strModuleCode, QString& strMLIID)=0;
	virtual void LockThreadActive()=0;
	virtual void UnLockThreadActive()=0;
	virtual int GetSessionCount()=0;
	virtual QStringList GetSocketListForPersonID(int nPersonID)=0;
	virtual void ClearUserSessionStorage(QString strSession="")=0;
	virtual int	GetClientTimeZoneOffsetMinutes(QString strSession="")=0;

	void	SetUserStorageDiskQuota(qint64 quota);
	void	SetUserStorageDirectoryPath(QString strPath){m_strUserStorageDirectoryPath=strPath;}
	QString	GetUserStorageDirectoryPath(){return m_strUserStorageDirectoryPath;};
	QString InitializeUserStorageDirectory(Status &status,QString strUserSubDir);
	bool	IsEnoughDiskSpaceForUserStorage(qint64 nNewFileSize);

protected:
	LicenseAccessRightsBase *m_pModuleLicense;
	QString		m_strUserStorageDirectoryPath;
	qint64		m_nUserStorageServerDiskQuota;
};


#endif //USERSESSIONMANAGER_H


