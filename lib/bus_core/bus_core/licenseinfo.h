#ifndef LICENSEINFO_H
#define LICENSEINFO_H

#include <QList>
#include <QDateTime>
#include "licensemodinfo.h"
#include "fpdf.h"

class LicenseInfo
{
public:
	LicenseInfo();
	LicenseInfo(const LicenseInfo &other);
	void operator =(const LicenseInfo &other);

	void Clear();
	bool Load(QString strFile);
	bool Save(QString strFile);

#ifdef _DEBUG
	void Dump();
	void AssertValid();
#endif

public:
	//internal class data
	bool m_bModified;

	//global header data (readable in key file)
	int m_nLicenseVersion;

	//license header data (encrypted in key file)
	int m_nLicenseID;
	QString m_strCustomerID;
	int m_nCustomSolutionID;
	QString m_strClientName;
	QString m_strReportLine1;
	QString m_strReportLine2;
	bool	m_bIsCompany;
	QDateTime	m_dateCreated;
	QDateTime	m_dateModified;

	FPDF m_fpdf;	//default tree

	//per module data
	QList<LicenseModInfo>	m_lstModInfo;	//sections per module

	QString strCopyRight;
};

#endif	// LICENSEINFO_H
