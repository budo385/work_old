#include "MAPIExPCH.h"
#include "MAPIEx.h"
#include "Interface.h"
#include <ExchForm.h>
#include <vector>

CMAPIEx *g_mapi = NULL;

#define MAPI_CONTACTS_FOLDER_EN "Contacts"
#define MAPI_CONTACTS_FOLDER_DE "Kontakte"

int GetFolderCount_Recursive(LPMAPIFOLDER lpParentFolder);
bool ListFolders_Recursive(LPMAPIFOLDER lpParentFolder,int *nPIDs, char **szNames, int nParentIdx);

bool Initialize()
{
	g_mapi = new CMAPIEx;
	if(!CMAPIEx::Init(FALSE) || !g_mapi->Login() || !g_mapi->OpenMessageStore(NULL, MAPI_MODIFY)){
		g_mapi->Logout();
		delete g_mapi;
		g_mapi = NULL;
		CMAPIEx::Term();
		return false;
	}

	return true;
}

void Uninitialize()
{
	//g_mapi->Logout();
	if(g_mapi)
		delete g_mapi;
	g_mapi = NULL;
	CMAPIEx::Term();			
}

bool SendMail(const char *szFrom, 
			  const char *szFromEml,
			  int nToEmailCnt,
			  const char **ppszToEml, 
			  int nCCEmailCnt,
			  const char **ppszCCEml, 
			  int nBCCEmailCnt,
			  const char **ppszBCCEml, 
			  const char *szSubject, 
			  const char *szTxtBody,
			  const char *szRtfBody,
			  int nAttachmentCnt,
			  const char **ppszAttachmentNames,
			  const char **ppszAttachmentCIDs,
			  int *ppszAttachmentSize,
			  const char **ppszAttachmentData,
			  int nEntryBufSize,
			  int *pnEntrySize,
			  char *szEntryID,
			  int nPriority,
			  bool bShowForm,
			  bool bViewOnly)
{
	if(!g_mapi)
		return false;

	//*pnEntrySize = 0;
	//return true; // sent successfully

	if(g_mapi->OpenOutbox()) 
	{
		CMAPIMessage message;
		if(message.Create(g_mapi, nPriority)) 
		{
			if(szFrom)		message.SetSenderName(szFrom);
			if(szFromEml)	message.SetSenderEmail(szFromEml);
			if(szSubject)	message.SetSubject(szSubject);

			int i;

			//add recipients
			for(i=0; i<nToEmailCnt; i++)
				message.AddRecipient(ppszToEml[i], MAPI_TO);
			for(i=0; i<nCCEmailCnt; i++)
				message.AddRecipient(ppszCCEml[i], MAPI_CC);
			for(i=0; i<nBCCEmailCnt; i++)
				message.AddRecipient(ppszBCCEml[i], MAPI_BCC);

			if(szTxtBody && strlen(szTxtBody))	message.SetBody(szTxtBody);
			if(szRtfBody && strlen(szRtfBody))	message.SetRTF(szRtfBody);

			//add attachments
			for(i=0; i<nAttachmentCnt; i++)
				message.AddAttachment(ppszAttachmentData[i], ppszAttachmentSize[i], ppszAttachmentNames[i], ppszAttachmentCIDs[i]);

			message.FetchEntryID(); //should generate entry id
			
			if(NULL == szRtfBody || 0 == strlen(szRtfBody))//if(nAttachmentCnt > 0)
				message.ForceNonTnef();

			if(bShowForm)
			{
				//message.GetStringProperty(strData, PR_TRANSPORT_MESSAGE_HEADERS);
				int nRes = message.ShowFormNonModal(g_mapi, (bViewOnly) ? EXCHIVERB_OPEN : EXCHIVERB_OPEN);
				//int nRes = 1012;
				//int nRes1 = message.ShowForm(g_mapi);
				//if(nRes1 != 0)
				//	nRes = S_OK;
				
				//fill the entry ID info
				int nSize = message.GetEntryID()->cb;
				if(nEntryBufSize >= nSize) // buffer long enough
				{
					memcpy(szEntryID, message.GetEntryID()->lpb, nSize);
					if(pnEntrySize) *pnEntrySize = nSize;
				}
				else{
					if(pnEntrySize) *pnEntrySize = -nSize;	// this much is required (but not available in your buffer)
				}

				//required by #1805
				//g_mapi->DeleteMessage(message);

				return (S_OK == nRes);
			}
			else
			{
				if(message.Send())
				{
					//fill the entry ID info
					int nSize = message.GetEntryID()->cb;
					if(nEntryBufSize >= nSize) // buffer long enough
					{
						memcpy(szEntryID, message.GetEntryID()->lpb, nSize);
						if(pnEntrySize) *pnEntrySize = nSize;
					}
					else{
						if(pnEntrySize) *pnEntrySize = -nSize;	// this much is required (but not available in your buffer)
					}

					return true; // sent successfully
				}
			}
		}
	}

	return false;
}

bool DeleteMailByID(int nEntrySize, char *szEntryID)
{
	if(nEntrySize < 1 || NULL == szEntryID)
		return false;

	if(g_mapi->OpenRootFolder()) 
	{
		SBinary entry;
		entry.cb=nEntrySize;
		entry.lpb=new BYTE[entry.cb];
		memcpy(entry.lpb, szEntryID, entry.cb);

		CMAPIMessage message;
		if(message.Open(g_mapi, entry, 0)) 
		{
			g_mapi->DeleteMessage(message);
			delete [] entry.lpb;
			return true;
		}

		delete [] entry.lpb;
	}

	return false;
}

bool OpenMailByID(int nEntrySize, char *szEntryID, bool bModal)
{
	if(nEntrySize < 1 || NULL == szEntryID)
		return false;

	if(g_mapi->OpenRootFolder()) 
	{
		SBinary entry;
		entry.cb=nEntrySize;
		entry.lpb=new BYTE[entry.cb];
		memcpy(entry.lpb, szEntryID, entry.cb);

		CMAPIMessage message;
		if(message.Open(g_mapi, entry, 0)) 
		{
			int nRes = (bModal) ? message.ShowForm(g_mapi) : message.ShowFormNonModal(g_mapi);
			delete [] entry.lpb;

			return (S_OK == nRes);
		}

		delete [] entry.lpb;
	}

	return false;
}

bool OpenContacts()
{
	if( g_mapi->OpenRootFolder() && 
		(g_mapi->OpenSubFolder(MAPI_CONTACTS_FOLDER_DE) ||
		 g_mapi->OpenSubFolder(MAPI_CONTACTS_FOLDER_EN)) && 
		g_mapi->GetContents()) 
	{
		// sort by name (stored in PR_SUBJECT)
		g_mapi->SortContents(TABLE_SORT_ASCEND,PR_SUBJECT);
		return true;
	}
	return false;
}

EXPORT_API bool GetNextContact(char *szFirstName,
							   char *szLastName,
							   char *szEmail,
							   char *szAddrCity,
							   char *szAddrStreet,
							   char *szAddrCountry,
							   char *szAddrRegion,
							   char *szAddrZIP,
							   char *szPhoneBussiness1,
							   char *szPhoneBussiness2,
							   char *szPhoneHome,
							   char *szPhoneMobile,
							   char *szPhoneFax,
							   char *szWebsite,
							   char *szOrganization,
							   char *szCountry,
							   char *szCity,
							   char *szDescription,
							   char *szNamePrefix,
							   short  *nGender
								)
{
	CMAPIContact contact;
	if(g_mapi->GetNextContact(contact)) 
	{
		CString strFirstName;
		contact.GetPropertyString(strFirstName, PR_GIVEN_NAME);
		strncpy(szFirstName, strFirstName, 255); szFirstName[255] = '\0';

		CString strLastName;
		contact.GetPropertyString(strLastName, PR_SURNAME);
		strncpy(szLastName, strLastName, 255);	szLastName[255] = '\0';

		CString strEmail;
		contact.GetEmail(strEmail);
		strncpy(szEmail, strEmail, 255); szEmail[255] = '\0';

		CContactAddress address;
		contact.GetAddress(address,CContactAddress::BUSINESS);
		strncpy(szAddrCity, address.m_strCity, 255); szAddrCity[255] = '\0';
		strncpy(szAddrStreet, address.m_strStreet, 255); szAddrStreet[255] = '\0';
		strncpy(szAddrCountry, address.m_strCountry, 255); szAddrCountry[255] = '\0';
		strncpy(szAddrRegion, address.m_strStateOrProvince, 255); szAddrRegion[255] = '\0';
		strncpy(szAddrZIP, address.m_strPostalCode, 255); szAddrZIP[255] = '\0';

		CString strPhone;
		contact.GetPhoneNumber(strPhone,PR_BUSINESS_TELEPHONE_NUMBER);
		strncpy(szPhoneBussiness1, strPhone, 255); szPhoneBussiness1[255] = '\0';

		contact.GetPhoneNumber(strPhone,PR_BUSINESS2_TELEPHONE_NUMBER);
		strncpy(szPhoneBussiness2, strPhone, 255); szPhoneBussiness2[255] = '\0';

		contact.GetPhoneNumber(strPhone,PR_HOME_TELEPHONE_NUMBER);
		strncpy(szPhoneHome, strPhone, 255); szPhoneHome[255] = '\0';
		
		contact.GetPhoneNumber(strPhone,PR_MOBILE_TELEPHONE_NUMBER);
		strncpy(szPhoneMobile, strPhone, 255); szPhoneMobile[255] = '\0';

		contact.GetPhoneNumber(strPhone,PR_BUSINESS_FAX_NUMBER);
		strncpy(szPhoneFax, strPhone, 255); szPhoneFax[255] = '\0';

		if(strPhone.IsEmpty()){
			contact.GetPhoneNumber(strPhone,PR_PRIMARY_FAX_NUMBER);
			strncpy(szPhoneFax, strPhone, 255); szPhoneFax[255] = '\0';
		}

		contact.GetHomePage(strPhone);
		strncpy(szWebsite, strPhone, 255); szWebsite[255] = '\0';

		CString strCompany;
		contact.GetCompany(strCompany);
		strncpy(szOrganization, strCompany, 255); szOrganization[255] = '\0';

		CString strCountry;
		contact.GetCountry(strCountry);
		strncpy(szCountry, strCountry, 255); szCountry[255] = '\0';

		CString strText;
		contact.GetNotes(strText,FALSE);
		strncpy(szDescription, strText, 4000); szDescription[4000] = '\0';

		CString strCity;
		contact.GetCity(strCity);
		strncpy(szCity, strCity, 255); szCity[255] = '\0';

		contact.GetPropertyString(strText, PR_DISPLAY_NAME_PREFIX);
		strncpy(szNamePrefix, strText, 255); szNamePrefix[255] = '\0';

		if(!contact.GetPropertyShort(*nGender, PR_GENDER))
			*nGender = -1;

		return true;
	}

	return false;
}

int Email_GetFoldersCount()
{
	LPMAPIFOLDER lpMapiFolder = g_mapi->OpenRootFolder();
	if(!lpMapiFolder)
		return -1;

	return GetFolderCount_Recursive(lpMapiFolder);
}

int GetFolderCount_Recursive(LPMAPIFOLDER lpParentFolder)
{
	g_mapi->GetHierarchy(lpParentFolder);

	int nCount = 0;

	// list all child folders
	std::vector<LPMAPIFOLDER> folders; 
	CString strFolder;
	LPMAPIFOLDER lpFolder = g_mapi->GetNextSubFolder(strFolder, lpParentFolder);
	while(NULL != lpFolder)
	{
		nCount ++;
		folders.push_back(lpFolder);

		lpFolder = g_mapi->GetNextSubFolder(strFolder, lpParentFolder);
	}

	//now recurse
	unsigned int nChildren = folders.size();
	for(int i=0; i<nChildren; i++){
		nCount += GetFolderCount_Recursive(folders[i]);
	}

	//RELEASE(lpParentFolder);
	return nCount;
}

bool Email_FillFolderInfo(int *nPIDs, char **szNames)
{
	LPMAPIFOLDER lpMapiFolder = g_mapi->OpenRootFolder();
	if(!lpMapiFolder)
		return false;

	return ListFolders_Recursive(lpMapiFolder, nPIDs, szNames, -1);
}

bool ListFolders_Recursive(LPMAPIFOLDER lpParentFolder,int *nPIDs, char **szNames, int nParentIdx)
{
	g_mapi->GetHierarchy(lpParentFolder);

	int nChildIdx = nParentIdx;

	// list all child folders
	std::vector<LPMAPIFOLDER> folders;
	CString strFolder;
	LPMAPIFOLDER lpFolder = g_mapi->GetNextSubFolder(strFolder, lpParentFolder);
	while(NULL != lpFolder)
	{
		nChildIdx ++;
		folders.push_back(lpFolder);
		
		char *szEntry = szNames[nChildIdx];

		//copy data
		strncpy(szEntry, strFolder, 255); szEntry[255] = '\0';
		nPIDs[nChildIdx] = nParentIdx;

		lpFolder = g_mapi->GetNextSubFolder(strFolder, lpParentFolder);
	}

	//now recurse
	unsigned int nChildren = folders.size();
	for(int i=0; i<nChildren; i++){
		GetFolderCount_Recursive(folders[i]);
		if(!ListFolders_Recursive(folders[i], nPIDs, szNames, nParentIdx+1+i))
			return false;
	}

	//RELEASE(lpParentFolder);
	return true;
}

LPMAPIFOLDER Email_OpenOutlookFolder(int nCount, char **szNames)
{
	if(nCount < 1)
		return NULL;

	LPMAPIFOLDER lpMapiFolder = g_mapi->OpenRootFolder(false);
	if(!lpMapiFolder)
		return NULL;

	//find folder level by level
	LPMAPIFOLDER lpParentFolder = lpMapiFolder;
	int nLevel = 0;
	while(nLevel < nCount){
		g_mapi->GetHierarchy(lpParentFolder);

		CString strFolder;
		LPMAPIFOLDER lpFolder = g_mapi->GetNextSubFolder(strFolder, lpParentFolder);
		while(NULL != lpFolder && strFolder != szNames[nLevel]){
			lpFolder = g_mapi->GetNextSubFolder(strFolder, lpParentFolder);
		}
		LPMAPIFOLDER lpNewParent = lpFolder;
		if(!lpNewParent){
			RELEASE(lpParentFolder);
			return NULL;	//error, not found
		}
		
		RELEASE(lpParentFolder);
		lpParentFolder = lpNewParent;
		nLevel ++;
	}

	if(lpParentFolder){
		g_mapi->GetContents(lpParentFolder);
		g_mapi->SortContents(TABLE_SORT_ASCEND,PR_SUBJECT);
	}

	return lpParentFolder;
}

bool SendAppointment(const char *pszToEml, 
			  const char *szSubject, 
			  const char *szTxtBody,
			  const char *szICalendar)
{
	if(!g_mapi)
		return false;

	bool bOK=false;

	if(g_mapi->OpenOutbox()) 
	{
		CMAPIMessage message;
		if(message.Create(g_mapi, IMPORTANCE_NORMAL)) 
		{
			//if(szFrom)		message.SetSenderName(szFrom);
			//if(szFromEml)	message.SetSenderEmail(szFromEml);
			message.SetSubject(szSubject);
			message.AddRecipient(pszToEml, MAPI_TO);
			message.SetBody(szTxtBody);

			message.AddAttachment(szICalendar, strlen(szICalendar), "appointment.ics", NULL, "text/calendar");
			message.FetchEntryID(); //should generate entry id
			//message.ForceNonTnef();
			if(message.Send())
			{
				bOK=true;
				/*
				//fill the entry ID info
				int nSize = message.GetEntryID()->cb;
				if(nEntryBufSize >= nSize) // buffer long enough
				{
					memcpy(szEntryID, message.GetEntryID()->lpb, nSize);
					if(pnEntrySize) *pnEntrySize = nSize;
				}
				else{
					if(pnEntrySize) *pnEntrySize = -nSize;	// this much is required (but not available in your buffer)

					return true; // sent successfully
				}
				*/
			}
		}
	}
/*
	mapi.OpenCalendar();
	CMAPIAppointment appointment;
	//appointment.Create(mapi, nPriority);
	appointment.SetBody(strBody);
	//TOFIX appointment.SetSenderEmail(szFromEml);
	appointment.AddRecipient(strEmailTo, MAPI_TO);
	//TOFIX appointment.SetSubject
	appointment.Send();
*/
	return bOK;
}

