
#ifdef _USRDLL
  #define EXPORT_API extern "C" __declspec(dllexport)
#else
  #define EXPORT_API
#endif

#ifndef EXCHIVERB_OPEN
 #define EXCHIVERB_OPEN 0
#endif

//
// IMPORTANT:
//	Visual Studio shows a large number of memory leak warnings when the Qt program that uses this
//	(MFC-based) DLL is terminated, but you can safely ignore these according to the page:
//	http://doc.trolltech.com/solutions/4/qtwinmigrate/winmigrate-walkthrough.html
//
//
// Usage example:
// --------------------------------------------
// #include <windows.h>
// typedef bool (*tInitTest)();
// typedef bool (*tSendTest)(const char *, const char *, int, const char **, int, const char **, int, const char **, const char *,const char *,const char *,int, const char **, const char **, int *, const char **, int, int*, char *, int, bool, bool);
// ...
//	HINSTANCE hDll = LoadLibrary("MAPIEx.dll");
//	if(hDll){
//		tInitTest Initialize = (tInitTest)GetProcAddress(hDll, "Initialize");
//		if(!Initialize){
//			QMessageBox::information(NULL, "Info", "Error getting method!");
//			return;
//		}
//		if(!Initialize()){
///			QMessageBox::information(NULL, "Info", "Error initializing dll!");
//			return;
//		}
//
//		tSendTest SendMail = (tSendTest)GetProcAddress(hDll, "SendMail");
//		if(!SendMail){
//			QMessageBox::information(NULL, "Info", "Error getting method!");
//			return;
//		}
//
//		//array of pointers, use similar code for CC, BCC, attachments
//      char *szToEmails[5];
//		szToEmails[0] = "trumbic@sokrates.hr";
//		...
//		szToEmails[4] = "mperutov@sokrates.hr";
//
//		if(SendMail("Ja", "ja@ja.com", 5, szToEmails, ..., "<html><body><font size=2 color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;color:red'>Body</font></body></html>", ...))
//			QMessageBox::information(NULL, "Info", "OK");
//		else
//			QMessageBox::information(NULL, "Info", "Error loading dll!");
//
//		FreeLibrary(hDll);
//	}
//	else
//		QMessageBox::information(NULL, "Info", "Error loading dll!"); 
//
//----------------------------------------------

EXPORT_API bool Initialize();
EXPORT_API void Uninitialize();

EXPORT_API bool SendMail(const char *szFrom, 
						  const char *szFromEml, 
						  int nToEmailCnt,
						  const char **ppszToEml, 
						  int nCCEmailCnt,
						  const char **ppszCCEml, 
						  int nBCCEmailCnt,
						  const char **ppszBCCEml, 
						  const char *szSubject,
						  const char *szTxtBody,
						  const char *szRtfBody,
						  int nAttachmentCnt,
						  const char **ppszAttachmentNames,	// names
						  const char **ppszAttachmentCIDs,	// name in the attachment is embedded
						  int *ppszAttachmentSize,			// file sized
						  const char **ppszAttachmentData,	// file data
						  int nEntryBufSize,	// size of szEntryID buffer
						  int *pnEntrySize,		// returns actual size of data returned in szEntryID (if <0, returns required buffer size)
						  char *szEntryID,		// buffer to receive entry ID (binary data, NOT '\0' terminated)
						  int nPriority,		// IMPORTANCE_NORMAL|IMPORTANCE_HIGH|IMPORTANCE_LOW
						  bool bShowForm,
						  bool bViewOnly=false);

EXPORT_API bool SendAppointment(const char *pszToEml, 
			  const char *szSubject, 
			  const char *szTxtBody,
			  const char *szICalendar);

EXPORT_API bool OpenMailByID(int nEntrySize, char *szEntryID, bool bModal);

EXPORT_API bool DeleteMailByID(int nEntrySize, char *szEntryID);

// API 

EXPORT_API bool OpenContacts();

//note all buffers must be at least 256 chars long, description buffer must be 4k
EXPORT_API bool GetNextContact(char *szFirstName,
							   char *szLastName,
							   char *szEmail,
							   char *szAddrCity,
							   char *szAddrStreet,
							   char *szAddrCountry,
							   char *szAddrRegion,
							   char *szAddrZIP,
							   char *szPhoneBussiness1,
							   char *szPhoneBussiness2,
							   char *szPhoneHome,
							   char *szPhoneMobile,
							   char *szPhoneFax,
							   char *szWebsite,
							   char *szOrganization,
							   char *szCountry,
							   char *szCity,
							   char *szDescription,
							   char *szNamePrefix,
							   short  *nGender
								);

EXPORT_API int  Email_GetFoldersCount();
EXPORT_API bool Email_FillFolderInfo(int *nPIDs, char **szNames);

/*
EXPORT_API LPMAPIFOLDER Email_OpenOutlookFolder(int nCount, char **szNames);

EXPORT_API bool Email_GetNextMessage(char **pszSenderName, 
									 char **pszSenderEmail,
									 char **pszSubject,
									 char **pszRecvDate,
									 char **pszRecvDate,);
EXPORT_API bool Email_FreeMessageMem(int nCount, 
									 char **szNames);
*/