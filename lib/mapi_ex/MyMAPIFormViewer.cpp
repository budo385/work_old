// MyMAPIFormViewer.cpp: implementation of the CMyMAPIFormViewer class.
//
//////////////////////////////////////////////////////////////////////

#include "MAPIExPCH.h"
//#include "global.h"
//#include "FormViewer.h"
#include "MyMAPIFormViewer.h"
//#include "MessageListCtrl.h"

#define INITGUID
#define USES_IID_IMAPIAdviseSink
#define USES_IID_IMAPIMessageSite
#define USES_IID_IMAPIViewContext
#define USES_IID_IMAPIViewAdviseSink
#define USES_IID_IUnknown
#define USES_IID_IMessage
#define USES_IID_IMAPIForm
#define USES_IID_IPersistMessage

#include <initguid.h>
#include <mapiguid.h>

#define CHECKHRESMSG(hRes,szErrorMsg) (CheckHRes(hRes,szErrorMsg,__FILE__,__LINE__))
#define CHECKHRES(hRes) (CheckHRes(hRes,"",__FILE__,__LINE__))
BOOL CheckHRes(HRESULT hRes,char *szErrorMsg,char *szFile,int iLine);

///////////////////////////////////////////////////////////////////////////////
//    CopySBinary()
//
//    Parameters
//      
//    Purpose
//      Allocates a new SBinary and copies psbSrc into it
//      
STDMETHODIMP CopySBinary(LPSBinary psbDest,const LPSBinary psbSrc, LPVOID pParent)
{
    HRESULT     hRes = S_OK;

    psbDest -> cb = psbSrc -> cb;

    if (psbSrc -> cb)
    {
        if (pParent)
            hRes = MAPIAllocateMore(psbSrc -> cb, 
                                    pParent, (LPVOID *) & psbDest->lpb);
        else
            hRes = MAPIAllocateBuffer(psbSrc -> cb, 
                                      (LPVOID *) & psbDest->lpb);
		if (!FAILED(hRes))
			CopyMemory(psbDest->lpb,psbSrc->lpb,psbSrc -> cb);
    }

    return hRes;
}

BOOL CheckHRes(HRESULT hRes,char *szErrorMsg,char *szFile,int iLine)
{
	if (FAILED(hRes))
	{
		/*
		QString szMsg;
		szMsg.Format("%s\nhRes = %x\nIn file %s\nOn line %d",szErrorMsg,hRes,szFile,iLine);
		MessageBox(NULL,szMsg,"HResult failed check!",MB_OK);
		*/
	}
	return SUCCEEDED(hRes);
}

#include <exchform.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMyMAPIFormViewer::CMyMAPIFormViewer(
									 LPMDB lpMDB,
									 LPMAPISESSION lpMAPISession,
									 LPMAPIFOLDER lpFolder,
									 LPMESSAGE lpMessage,
									 LPSBinary lpMessageEID,
									 CMessageListCtrl *lpMessageListCtrl)
{
	//OutputDebugString("CMyMAPIFormViewer::CMyMAPIFormViewer\n");

	m_lpMAPISession = lpMAPISession;
	m_lpMAPISession->AddRef();

	m_lpMDB = lpMDB;
	m_lpMDB->AddRef();

	m_lpFolder = lpFolder;
	m_lpMessage = lpMessage;

	if (lpMessageEID)
	{
		CopySBinary(&m_MessageEID,lpMessageEID,NULL);
	}
	else
	{
		LPSPropValue lpProp;
		HRESULT hRes = HrGetOneProp(
			m_lpMessage,
			PR_ENTRYID,
			&lpProp);

		if(SUCCEEDED(hRes))
		{
			CopySBinary(&m_MessageEID,&lpProp->Value.bin,NULL);
			MAPIFreeBuffer(lpProp);
		}
		else{
			m_MessageEID.lpb = NULL;
			m_MessageEID.cb	= 0;
		}
	}
	//m_lpMessageListCtrl = lpMessageListCtrl;
	m_lpMessageListCtrl = NULL;

	m_lpFolder->AddRef();
	m_lpMessage->AddRef();
	//if (m_lpMessageListCtrl) m_lpMessageListCtrl->AddRef();
	

	m_lpPersistMessage = NULL;
	m_lpMapiFormAdviseSink = NULL;
	m_pulConnection = NULL;

	m_cRef = 1;
}

CMyMAPIFormViewer::~CMyMAPIFormViewer()
{
	//OutputDebugString("CMyMAPIFormViewer::~CMyMAPIFormViewer\n");

	MAPIFreeBuffer(m_MessageEID.lpb);
	UlRelease(m_lpPersistMessage);
	UlRelease(m_lpMessage);
	UlRelease(m_lpMapiFormAdviseSink);
	UlRelease(m_lpFolder);
	UlRelease(m_lpMDB);
	UlRelease(m_lpMAPISession);
	//if (m_lpMessageListCtrl) m_lpMessageListCtrl->Release();//this must be last!!!!!
}

STDMETHODIMP CMyMAPIFormViewer::QueryInterface (REFIID riid,
												 LPVOID * ppvObj)
{
	//OutputDebugString("CMyMAPIFormViewer::QueryInterface\n");
	*ppvObj = 0;
	if (riid == IID_IMAPIMessageSite)
	{
		*ppvObj = (IMAPIMessageSite *)this;
		AddRef();
		return S_OK;
	}
	if (riid == IID_IMAPIViewContext)
	{
		*ppvObj = (IMAPIViewContext *)this;
		AddRef();
		return S_OK;
	}
	if (riid == IID_IMAPIViewAdviseSink)
	{
		*ppvObj = (IMAPIViewAdviseSink *)this;
		AddRef();
		return S_OK;
	}
	if (riid == IID_IUnknown)
	{
		*ppvObj = (LPUNKNOWN)((IMAPIMessageSite *)this);
		AddRef();
		return S_OK;
	}
	return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG) CMyMAPIFormViewer::AddRef() 
{
	//OutputDebugString("CMyMAPIFormViewer::AddRef\n");
	InterlockedIncrement(&m_cRef);
	//sprintf(szString,"CMyMAPIFormViewer.m_cRef increased to %d.\n",m_cRef);
	////OutputDebugString(szString);
	return m_cRef; 
}

STDMETHODIMP_(ULONG) CMyMAPIFormViewer::Release() 
{
	//OutputDebugString("CMyMAPIFormViewer::Release\n");
	LONG lCount = InterlockedDecrement(&m_cRef);	
	//sprintf(szString,"CMyMAPIFormViewer.m_cRef decreased to %d.\n",m_cRef);
	////OutputDebugString(szString);
	if (!lCount)  delete this; 
	return lCount;
}

///////////////////////////////////////////////////////////////////////////////
// IMAPIMessageSite implementation
///////////////////////////////////////////////////////////////////////////////
STDMETHODIMP CMyMAPIFormViewer::GetSession (LPMAPISESSION FAR * ppSession)
{
	//OutputDebugString("CMyMAPIFormViewer::GetSession\n");
	*ppSession = m_lpMAPISession;
	(*ppSession)->AddRef();
	return S_OK;
}

STDMETHODIMP CMyMAPIFormViewer::GetStore (LPMDB FAR * ppStore)
{
	//OutputDebugString("CMyMAPIFormViewer::GetStore\n");
	*ppStore = m_lpMDB;
	(*ppStore)->AddRef();
	return S_OK;
}

STDMETHODIMP CMyMAPIFormViewer::GetFolder (LPMAPIFOLDER FAR * ppFolder)
{
	//OutputDebugString("CMyMAPIFormViewer::GetFolder\n");
	*ppFolder = m_lpFolder;
	m_lpFolder->AddRef();
	return S_OK;
}

STDMETHODIMP CMyMAPIFormViewer::GetMessage (LPMESSAGE FAR * ppmsg)
{
	//OutputDebugString("CMyMAPIFormViewer::GetMessage\n");
	HRESULT hRes = S_OK;
	if (m_lpMessage)
	{
		*ppmsg = m_lpMessage;
		m_lpMessage->AddRef();
	}
	else
	{
		*ppmsg = NULL;
		hRes = S_FALSE;
	}
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::GetFormManager (LPMAPIFORMMGR FAR * ppFormMgr)
{
	//OutputDebugString("CMyMAPIFormViewer::GetFormManager\n");
	HRESULT hRes = S_OK;

	hRes = MAPIOpenFormMgr(m_lpMAPISession,ppFormMgr);
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::NewMessage (ULONG fComposeInFolder,
											 LPMAPIFOLDER pFolderFocus,
											 LPPERSISTMESSAGE pPersistMessage,
											 LPMESSAGE FAR * ppMessage,
											 LPMAPIMESSAGESITE FAR * ppMessageSite,
											 LPMAPIVIEWCONTEXT FAR * ppViewContext)
{
	//OutputDebugString("CMyMAPIFormViewer::NewMessage\n");

	CMyMAPIFormViewer *lpMyMAPIFormViewer = NULL;

	HRESULT hRes = S_OK;
	if ((fComposeInFolder == FALSE) || !pFolderFocus) 
	{
		pFolderFocus = m_lpFolder;
	}
	hRes = m_lpFolder->CreateMessage(
		NULL,//IID
		NULL,//flags
		ppMessage);
	if (!CHECKHRES(hRes)) goto Cleanup;

	hRes = (*ppMessage)->SaveChanges(KEEP_OPEN_READWRITE);
	if (!CHECKHRES(hRes)) goto Cleanup;

	lpMyMAPIFormViewer = new CMyMAPIFormViewer(
		m_lpMDB,
		m_lpMAPISession,
		pFolderFocus,
		*ppMessage,
		NULL,
		m_lpMessageListCtrl);

	hRes = lpMyMAPIFormViewer->SetPersist(pPersistMessage);
	if (!CHECKHRES(hRes)) goto Cleanup;

	*ppMessageSite = lpMyMAPIFormViewer;

	hRes = (*ppMessageSite)->QueryInterface(IID_IMAPIViewContext,(LPVOID*) ppViewContext);
	if (!CHECKHRES(hRes)) goto Cleanup;



//	hRes = lpMyMAPIFormViewer->SetForm(lpForm);
//	if (!CHECKHRES(hRes)) goto Cleanup;



Cleanup:
//	UlRelease(lpForm);
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::CopyMessage (LPMAPIFOLDER /*pFolderDestination*/)
{
	//OutputDebugString("CMyMAPIFormViewer::CopyMessage\n");
	HRESULT hRes = S_OK;
	hRes = MAPI_E_NO_SUPPORT;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::MoveMessage (LPMAPIFOLDER /*pFolderDestination*/,
											  LPMAPIVIEWCONTEXT /*pViewContext*/,
											  LPCRECT /*prcPosRect*/)
{
	//OutputDebugString("CMyMAPIFormViewer::MoveMessage\n");
	HRESULT hRes = S_OK;
	hRes = MAPI_E_NO_SUPPORT;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::DeleteMessage (LPMAPIVIEWCONTEXT /*pViewContext*/,
												LPCRECT /*prcPosRect*/)
{
	//OutputDebugString("CMyMAPIFormViewer::DeleteMessage\n");
	HRESULT hRes = S_OK;
	hRes = MAPI_E_NO_SUPPORT;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::SaveMessage ()
{
	//OutputDebugString("CMyMAPIFormViewer::SaveMessage\n");
	HRESULT hRes = S_OK;

	hRes = m_lpPersistMessage->Save(
		m_lpMessage,
		TRUE);
	if (!CHECKHRES(hRes))
	{
		LPMAPIERROR lpErr = NULL;
		hRes = m_lpPersistMessage->GetLastError(hRes,0,&lpErr);
		CHECKHRESMSG(hRes,lpErr->lpszError);
		MAPIFreeBuffer(lpErr);
		goto Cleanup;
	}

	hRes = m_lpMessage->SaveChanges(KEEP_OPEN_READWRITE);
	if (!CHECKHRES(hRes)) goto Cleanup;

Cleanup:
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::SubmitMessage (ULONG /*ulFlags*/)
{
	//OutputDebugString("CMyMAPIFormViewer::SubmitMessage\n");
	HRESULT hRes = S_OK;

	hRes = m_lpPersistMessage->Save(
		m_lpMessage,
		TRUE);
	if (!CHECKHRES(hRes))
	{
		LPMAPIERROR lpErr = NULL;
		hRes = m_lpPersistMessage->GetLastError(hRes,0,&lpErr);
		CHECKHRESMSG(hRes,lpErr->lpszError);
		MAPIFreeBuffer(lpErr);
		goto Cleanup;
	}

	hRes = m_lpPersistMessage->HandsOffMessage();
	if (!CHECKHRES(hRes)) goto Cleanup;

	hRes = m_lpMessage->SubmitMessage(NULL);
	if (!CHECKHRES(hRes)) goto Cleanup;

//Is this needed?
	UlRelease(m_lpMessage);
	m_lpMessage = NULL;

Cleanup:
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::GetSiteStatus (LPULONG lpulStatus)
{
	//OutputDebugString("CMyMAPIFormViewer::GetSiteStatus\n");
	HRESULT hRes = S_OK;
	*lpulStatus = 
		VCSTATUS_NEW_MESSAGE |
		VCSTATUS_SAVE |
		VCSTATUS_SUBMIT;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::GetLastError(HRESULT /*hResult*/,
						  ULONG /*ulFlags*/,
						  LPMAPIERROR FAR * /*lppMAPIError*/)
{
	//OutputDebugString("CMyMAPIFormViewer::GetLastError\n");
	HRESULT hRes = S_OK;
	return hRes;
}

///////////////////////////////////////////////////////////////////////////////
// End IMAPIMessageSite implementation
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// IMAPIViewContext implementation
///////////////////////////////////////////////////////////////////////////////

STDMETHODIMP CMyMAPIFormViewer::SetAdviseSink(LPMAPIFORMADVISESINK pmvns)
{
	//OutputDebugString("CMyMAPIFormViewer::SetAdviseSink\n");
	HRESULT hRes = S_OK;
	UlRelease(m_lpMapiFormAdviseSink);
	if (pmvns)
	{
		m_lpMapiFormAdviseSink = pmvns;
		m_lpMapiFormAdviseSink->AddRef();
	}
	else
	{
		m_lpMapiFormAdviseSink = NULL;
	}
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::ActivateNext(ULONG ulDir,               
											  LPCRECT /*prcPosRect*/)
{
	//OutputDebugString("CMyMAPIFormViewer::ActivateNext\n");
	HRESULT hRes = S_OK;
	LPSBinary lpNextMessageEID = NULL;//do not free!
	LPMESSAGE lpNextMessage = NULL;
	LPPERSISTMESSAGE lpNextPersistMessage = NULL;
	LPMAPIFORM	lpMapiForm = NULL;
	LPMAPIVIEWADVISESINK lpMAPIViewAdviseSink = NULL;
	LPMAPIFORMMGR lpMAPIFormMgr = NULL;
	ULONG			ulObjType;
	ULONG			cValuesShow;
	LPSPropValue	lpspvaShow = NULL;

    enum {FLAGS,CLASS,STATUS,NUM_COLS};
	SizedSPropTagArray(NUM_COLS,sptaShowForm) = { NUM_COLS, { 
		PR_MESSAGE_FLAGS,
			PR_MESSAGE_CLASS,
			PR_MSG_STATUS}
    };

	//Without a view list control, we can't do 'next'
	if (!m_lpMessageListCtrl) goto Cleanup;

	if (!m_lpMDB) goto Cleanup;

	if (ulDir & VCDIR_NEXT)
	{
		//hRes = m_lpMessageListCtrl->GetNextMessageEID(&m_MessageEID,&lpNextMessageEID);
	}
	else
	if (ulDir & VCDIR_PREV)
	{
		//hRes = m_lpMessageListCtrl->GetPrevMessageEID(&m_MessageEID,&lpNextMessageEID);
	}
	if (hRes == MAPI_E_NOT_FOUND)
	{
		hRes = S_FALSE;
		goto Cleanup;
	}
	if (!CHECKHRES(hRes)) goto Cleanup;

	if (!lpNextMessageEID || !m_lpMapiFormAdviseSink) goto Cleanup;

	hRes = m_lpMDB->OpenEntry(
		lpNextMessageEID->cb, 
		(LPENTRYID) lpNextMessageEID->lpb, 
		NULL,//default interface
		MAPI_BEST_ACCESS, 
		&ulObjType, 
		(LPUNKNOWN*)&lpNextMessage);
	if (!CHECKHRES(hRes)) goto Cleanup;
	
	//update the current message pointer
	UlRelease(m_lpMessage);
	m_lpMessage = lpNextMessage;
	m_lpMessage->AddRef();
	
	//update the current message EID
	MAPIFreeBuffer(m_MessageEID.lpb);
	CopySBinary(&m_MessageEID,lpNextMessageEID,NULL);
	
	hRes = m_lpMessage->GetProps( 
		(LPSPropTagArray) &sptaShowForm,//property tag array
		NULL,//flags
		&cValuesShow, //Count of values returned
		&lpspvaShow);//Values returned
	if (!CHECKHRES(hRes)) goto Cleanup;
	
	//check to see if the current form can display the new message or find the needed form
	hRes = m_lpMapiFormAdviseSink->OnActivateNext(
		lpspvaShow[CLASS].Value.LPSZ,
		lpspvaShow[STATUS].Value.ul,//message status
		lpspvaShow[FLAGS].Value.ul,//message flags
		&lpNextPersistMessage);

	if (S_OK == hRes)//we can handle the message ourselves
	{
		if (lpNextPersistMessage)
		{
			//kill the old persist and use the new one
			UlRelease(m_lpPersistMessage);
			m_lpPersistMessage = lpNextPersistMessage;
			m_lpPersistMessage->AddRef();
		}
		else
		{
			//we use the current persist and dump it's old message
			hRes = m_lpPersistMessage->HandsOffMessage();
			if (!CHECKHRES(hRes)) goto Cleanup;
		}
		
		//load the new message
		hRes = m_lpPersistMessage->Load(
			(LPMAPIMESSAGESITE) this,
			m_lpMessage,
			lpspvaShow[STATUS].Value.ul,//message status
			lpspvaShow[FLAGS].Value.ul);//message flags
		if (!CHECKHRES(hRes)) goto Cleanup;
		
		//get a IMAPIForm interface to play with
		hRes = m_lpPersistMessage->QueryInterface(IID_IMAPIForm,(LPVOID*) &lpMapiForm);
		if (!CHECKHRES(hRes)) goto Cleanup;
		
		hRes = lpMapiForm->SetViewContext(
			(LPMAPIVIEWCONTEXT) this);
		if (!CHECKHRES(hRes)) goto Cleanup;
		
	}

	//We have to load the form from scratch
	else if (hRes == S_FALSE)
	{
		//get a IMAPIForm interface so we can shut down the old form
		hRes = m_lpPersistMessage->QueryInterface(IID_IMAPIForm,(LPVOID*) &lpMapiForm);
		if (!CHECKHRES(hRes)) goto Cleanup;
		
		hRes = lpMapiForm->ShutdownForm(SAVEOPTS_PROMPTSAVE);
		if (!CHECKHRES(hRes)) goto Cleanup;
		
		//remove the old advise sink
		if (m_pulConnection)
		{
			hRes = lpMapiForm->Unadvise(m_pulConnection);
			if (!CHECKHRES(hRes)) goto Cleanup;
			m_pulConnection = NULL;
		}

		UlRelease(lpMapiForm);
		UlRelease(m_lpPersistMessage);
		m_lpPersistMessage = NULL;


		//Load the new form
		hRes = GetFormManager(&lpMAPIFormMgr);
		if (!CHECKHRES(hRes)) goto Cleanup;
		
		hRes = lpMAPIFormMgr->LoadForm(
			0,//(ULONG) m_hWnd,
			0,//flags
			lpspvaShow[CLASS].Value.LPSZ,
			lpspvaShow[STATUS].Value.ul,//message status
			lpspvaShow[FLAGS].Value.ul,//message flags
			0,//parent folder
			(IMAPIMessageSite *) this,//message site
			lpNextMessage,
			(IMAPIViewContext *) this,//view context
			IID_IMAPIForm,//riid
			(LPVOID *) &lpMapiForm);
		if (!CHECKHRES(hRes)) goto Cleanup;
		
		hRes = SetForm(lpMapiForm);
		if (!CHECKHRES(hRes)) goto Cleanup;		
		
		RECT Rect;
		
		Rect.left = 0;
		Rect.right = 500;
		Rect.top = 0;
		Rect.bottom = 400;
		
		hRes = lpMapiForm->DoVerb(
			EXCHIVERB_OPEN,
			NULL,//view context
			NULL,//parent window
			NULL);//RECT structure with size
		if (hRes != S_OK)
		{
			hRes = lpMapiForm->DoVerb(
				EXCHIVERB_OPEN,
				NULL,//view context
				NULL,//parent window
				&Rect);//RECT structure with size
			if (!CHECKHRES(hRes)) goto Cleanup;
		}
	}
	if (!CHECKHRES(hRes)) goto Cleanup;
	
Cleanup:
	MAPIFreeBuffer(lpspvaShow);
	UlRelease(lpMAPIFormMgr);
	UlRelease(lpMAPIViewAdviseSink);
	UlRelease(lpMapiForm);
	UlRelease(lpNextPersistMessage);
	UlRelease(lpNextMessage);
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::GetPrintSetup(ULONG /*ulFlags*/,
											   LPFORMPRINTSETUP FAR * /*lppFormPrintSetup*/)
{
	//OutputDebugString("CMyMAPIFormViewer::GetPrintSetup\n");
	HRESULT hRes = S_OK;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::GetSaveStream(ULONG FAR * /*pulFlags*/,
											   ULONG FAR * /*pulFormat*/,
											   LPSTREAM FAR * /*ppstm*/)
{
	//OutputDebugString("CMyMAPIFormViewer::GetSaveStream\n");
	HRESULT hRes = S_OK;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::GetViewStatus(LPULONG lpulStatus)
{
	//OutputDebugString("CMyMAPIFormViewer::GetViewStatus\n");
	HRESULT hRes = S_OK;
	LPSBinary lpTestEID = NULL;

	*lpulStatus = NULL;
	*lpulStatus |= VCSTATUS_INTERACTIVE;
/*
	if (m_lpMessageListCtrl)
	{
		if (MAPI_E_NOT_FOUND != m_lpMessageListCtrl->GetNextMessageEID(&m_MessageEID,&lpTestEID))
			*lpulStatus |= VCSTATUS_NEXT;
		if (MAPI_E_NOT_FOUND != m_lpMessageListCtrl->GetPrevMessageEID(&m_MessageEID,&lpTestEID))
			*lpulStatus |= VCSTATUS_PREV;
	}
*/
	return hRes;
}

///////////////////////////////////////////////////////////////////////////////
// End IMAPIViewContext implementation
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// IMAPIViewAdviseSink implementation
///////////////////////////////////////////////////////////////////////////////

//Assuming we've advised on this form, we need to Unadvise it now, or it will never unload
STDMETHODIMP CMyMAPIFormViewer::OnShutdown()
{
	//OutputDebugString("CMyMAPIFormViewer::OnShutdown\n");
	HRESULT hRes = S_OK;
	LPMAPIFORM lpMapiForm = NULL;

	hRes = m_lpPersistMessage->QueryInterface( IID_IMAPIForm ,(LPVOID *)&lpMapiForm);
	if (!CHECKHRES(hRes)) goto Cleanup;

	hRes = lpMapiForm->Unadvise(m_pulConnection);
	if (!CHECKHRES(hRes)) goto Cleanup;

Cleanup:
	UlRelease(lpMapiForm);
	
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::OnNewMessage()
{
	//OutputDebugString("CMyMAPIFormViewer::OnNewMessage\n");
	HRESULT hRes = S_OK;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::OnPrint(
					 ULONG /*dwPageNumber*/,
					 HRESULT /*hrStatus*/)
{
	//OutputDebugString("CMyMAPIFormViewer::OnPrint\n");
	HRESULT hRes = S_OK;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::OnSubmitted()
{
	//OutputDebugString("CMyMAPIFormViewer::OnSubmitted\n");
	HRESULT hRes = S_OK;
	return hRes;
}

STDMETHODIMP CMyMAPIFormViewer::OnSaved()
{
	//OutputDebugString("CMyMAPIFormViewer::OnSaved\n");
	HRESULT hRes = S_OK;
	return hRes;
}

///////////////////////////////////////////////////////////////////////////////
// End IMAPIViewAdviseSink implementation
///////////////////////////////////////////////////////////////////////////////

//set the m_lpPersistMessage pointer and get an advise to play with
STDMETHODIMP CMyMAPIFormViewer::SetForm(LPMAPIFORM lpMapiForm)
{
	//OutputDebugString("CMyMAPIFormViewer::SetForm\n");
	HRESULT hRes = S_OK;

	UlRelease(m_lpPersistMessage);

	hRes = lpMapiForm->QueryInterface(IID_IPersistMessage,(LPVOID *)&m_lpPersistMessage);
	if (!CHECKHRES(hRes)) goto Cleanup;

	hRes = lpMapiForm->Advise(
		(LPMAPIVIEWADVISESINK) this,
		&m_pulConnection);
	if (!CHECKHRES(hRes)) goto Cleanup;

Cleanup:
	return hRes;

}

STDMETHODIMP CMyMAPIFormViewer::SetPersist(LPPERSISTMESSAGE lpPersistMessage)
{
	//OutputDebugString("CMyMAPIFormViewer::SetPersist\n");
	HRESULT hRes = S_OK;

	LPMAPIFORM lpMapiForm;

	UlRelease(m_lpPersistMessage);

	m_lpPersistMessage = lpPersistMessage;
	m_lpPersistMessage->AddRef();

	hRes = m_lpPersistMessage->QueryInterface( IID_IMAPIForm ,(LPVOID *)&lpMapiForm);
	if (!CHECKHRES(hRes)) goto Cleanup;

	hRes = lpMapiForm->Advise(
		(LPMAPIVIEWADVISESINK) this,
		&m_pulConnection);
	if (!CHECKHRES(hRes)) goto Cleanup;

Cleanup:
	UlRelease(lpMapiForm);
	return hRes;
}
