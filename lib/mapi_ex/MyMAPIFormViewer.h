// MyMAPIFormViewer.h: interface for the CMyMAPIFormViewer class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <MAPIFORM.H>

class CMessageListCtrl;

class CMyMAPIFormViewer : 
	public IMAPIMessageSite , 
	public IMAPIViewContext ,
	public IMAPIViewAdviseSink
{
public:
	CMyMAPIFormViewer(
		LPMDB lpMDB,
		LPMAPISESSION lpMAPISession,
		LPMAPIFOLDER lpFolder,
		LPMESSAGE lpMessage,
		LPSBinary lpMessageEID,
		CMessageListCtrl *lpMessageListCtrl);
	virtual ~CMyMAPIFormViewer();
public:
    STDMETHODIMP QueryInterface (REFIID riid, LPVOID * ppvObj);
	STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();
	
	STDMETHODIMP SetForm(LPMAPIFORM lpMapiForm);
	STDMETHODIMP SetPersist(LPPERSISTMESSAGE lpPersistMessage);

	STDMETHODIMP GetLastError(
		HRESULT hResult,
		ULONG ulFlags,
		LPMAPIERROR FAR * lppMAPIError);
	
	////////////////////////////////////////////////////////////
	// IMAPIMessageSite Functions
	////////////////////////////////////////////////////////////
	STDMETHODIMP GetSession (
		LPMAPISESSION FAR * ppSession);
	STDMETHODIMP GetStore (
		LPMDB FAR * ppStore);
	STDMETHODIMP GetFolder (
		LPMAPIFOLDER FAR * ppFolder);
	STDMETHODIMP GetMessage (
		LPMESSAGE FAR * ppmsg);
	STDMETHODIMP GetFormManager (
		LPMAPIFORMMGR FAR * ppFormMgr);
	STDMETHODIMP NewMessage (
		ULONG fComposeInFolder,
		LPMAPIFOLDER pFolderFocus,
		LPPERSISTMESSAGE pPersistMessage,
		LPMESSAGE FAR * ppMessage,
		LPMAPIMESSAGESITE FAR * ppMessageSite,
		LPMAPIVIEWCONTEXT FAR * ppViewContext);
	STDMETHODIMP CopyMessage (
		LPMAPIFOLDER pFolderDestination);
	STDMETHODIMP MoveMessage (
		LPMAPIFOLDER pFolderDestination,
		LPMAPIVIEWCONTEXT pViewContext,
		LPCRECT prcPosRect);
	STDMETHODIMP DeleteMessage (
		LPMAPIVIEWCONTEXT pViewContext,
		LPCRECT prcPosRect);
	STDMETHODIMP SaveMessage ();
	STDMETHODIMP SubmitMessage (
		ULONG ulFlags); 
	STDMETHODIMP GetSiteStatus (
		LPULONG lpulStatus);
	////////////////////////////////////////////////////////////
	// IMAPIViewAdviseSink Functions
	////////////////////////////////////////////////////////////
	STDMETHODIMP OnShutdown();
	STDMETHODIMP OnNewMessage();
	STDMETHODIMP OnPrint(
		ULONG dwPageNumber,
		HRESULT hrStatus);
	STDMETHODIMP OnSubmitted();
	STDMETHODIMP OnSaved();
	
	////////////////////////////////////////////////////////////
	// IMAPIViewContext Functions
	////////////////////////////////////////////////////////////
	STDMETHODIMP SetAdviseSink(
		LPMAPIFORMADVISESINK pmvns);
	STDMETHODIMP ActivateNext(
		ULONG ulDir,               
		LPCRECT prcPosRect); 
	STDMETHODIMP GetPrintSetup(
		ULONG ulFlags,
		LPFORMPRINTSETUP FAR * lppFormPrintSetup);
	STDMETHODIMP GetSaveStream(
		ULONG FAR * pulFlags,
		ULONG FAR * pulFormat,
		LPSTREAM FAR * ppstm);
	STDMETHODIMP GetViewStatus(
		LPULONG lpulStatus);
private :    
	LONG					m_cRef;
	LPMAPIFOLDER			m_lpFolder;
	LPMESSAGE				m_lpMessage;
	LPMAPIFORMADVISESINK	m_lpMapiFormAdviseSink;
	LPPERSISTMESSAGE		m_lpPersistMessage;
	CMessageListCtrl*		m_lpMessageListCtrl;
	SBinary					m_MessageEID;
	ULONG					m_pulConnection;
	LPMDB					m_lpMDB;
	LPMAPISESSION			m_lpMAPISession;
};