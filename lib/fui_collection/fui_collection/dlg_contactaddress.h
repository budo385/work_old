#ifndef DLG_CONTACTADDRESS_H
#define DLG_CONTACTADDRESS_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_contactaddress.h"
#include "gui_core/gui_core/gui_helper.h"
#include "gui_core/gui_core/guidatamanipulator.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "bus_client/bus_client/selection_combobox.h"
#include "dlg_contactaddressschemas.h"
#include "bus_core/bus_core/formataddress.h"



class Dlg_ContactAddress : public QDialog
{
    Q_OBJECT

public:
    Dlg_ContactAddress(QWidget *parent = 0);
    ~Dlg_ContactAddress();


	enum MODEMDEVCAPS
	{
		MODE_INSERT,
		MODE_EDIT,
		MODE_VIEW,
	};

	void SetRow(DbRecordSet &rowAddr,bool bEditMode,bool bSyncFlag);
	bool GetRow(DbRecordSet &rowAddr,bool &bSyncNames);
	void HideButtonBar();

	void RefreshDisplay();

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnCustomize_clicked();
	void on_btnFormat_clicked();
	void on_btnFillFields_clicked();
	//void on_btnFromInternet_clicked();
	void OnCountryEdit(const QString& text);
	void on_cmbCountry_currentIndexChanged(const QString &text);
	void on_cmbSalutation_currentIndexChanged(const QString & text);
	void on_cmbShortSalutation_currentIndexChanged(const QString & text);
	void on_lookupZIP_clicked();


private:

	Ui::Dlg_ContactAddressClass ui;

	GuiFieldManager *m_GuiFieldManagerMain;
	GuiDataManipulator m_DataManager;			//data manager for row
	DbRecordSet m_lstData;
	//DbRecordSet m_lstDataAddressTypes;

	bool m_bEditMode;

	Selection_ComboBox m_SalutationHandler;
	Selection_ComboBox m_ShortSalutationHandler;
	Selection_ComboBox m_TitleHandler;
	Selection_ComboBox m_LangHandler;
	Selection_ComboBox m_CountryHandler;

	Selection_ComboBox m_FormatAddressCombo;
	Selection_ComboBox m_ParseAddressCombo;
	FormatAddress m_FormatAddress;

	QAction* m_pActPastFromClipboard;

};

#endif // DLG_CONTACTADDRESS_H
