#ifndef COMMUNICATIONMANAGER_H
#define COMMUNICATIONMANAGER_H

#include <QString>
#include <QTimer>
#include <QObject>
#include "common/common/observer_ptrn.h"
#include <QProgressDialog>
#include "common/common/dbrecordset.h"
#include <QAuthenticator>
#include "common/common/status.h"

class CommunicationManager : public QObject, public ObsrPtrn_Observer
{
	Q_OBJECT

public:

	CommunicationManager();
	~CommunicationManager();

	void Initialize();
	void ClearData();
	void EndUserSession(); //clear all caches
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());


	//DOCUMENTS
	//--------------------------------------------------------
	bool PackAndSendDocuments(DbRecordSet &lstDocuments,QPoint *pos=NULL,int nProjectID=-1,int nContactID=-1);
	void EmailReply(DbRecordSet &recEmail,QPoint *pos=NULL);
	void EmailForward(DbRecordSet &recEmail,QPoint *pos=NULL);
	static void BatchReadAllCEMenuData();
	bool ProcessDocumentDrop(QList<QUrl> &lst, bool bCreateTemplates,bool bAcceptApplications,bool bIgnoreHttpURLs=false, QString strCategory="");
	void LoadDocuments();
	bool DropCommDataToProject(DbRecordSet &Data,int nProjectID);

	//EMAIL:
	//--------------------------------------------------------
	void OpenMailInExternalClient(int nEmailID,QByteArray byteExternID, QString strExternApp);
	DbRecordSet ProcessEmailContentFromOutlook(bool bLogOut=false);
	void AddEmailToSentMailList(int nEmailID, QByteArray byteOutlookID, int nCENT_ID);
	bool OpenMailInOutlookFromDatabaseRecord(int nEmailID);
		

	//TASK:
	//--------------------------------------------------------
	void SetTaskDone(DbRecordSet &lstTaskIDs);

public slots:
	void OnDocumentChanged(const QString & path);

private slots:
	void OnProcessOutlookEmails();
	void OnEmitAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth);
	void OnCheckInOutCompleted(Status err,int nDocumentID,QString strFilePath,DbRecordSet recRev,DbRecordSet recCheckOutInfo);

private:

	void RedefineSentMailList();
	bool OpenMailInOutlook(QByteArray byteExternID, QString strExternApp, bool bSupressMsg=false);
	bool CreateFileFromURL(QUrl lstSourceURLPdf,DbRecordSet recPathURL, bool bCreateTemplates, QString strCategory="");
	void ApplicationInstallFinder(int nDocTemplateID);
	void DetermineDocType(DbRecordSet &lstPaths, QList<QUrl> &lstSource);

	QTimer m_Timer;
	bool m_bTimerConnected;
	DbRecordSet m_lstSentEmails;					//global list of all sent emails<id, id>
	QHash<int,DbRecordSet> m_lstDocsForCheckOutAndOpen;
	QHash<int,int> m_lstDocumentsForOpenAfterUpload;
};


#endif // COMMUNICATIONMANAGER_H
