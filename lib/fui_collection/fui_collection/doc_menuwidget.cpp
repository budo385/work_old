#include "doc_menuwidget.h"
#include "gui_core/gui_core/gui_helper.h"
#include <QMimeData>
#include <QToolButton>
#include <QFileDialog>
#include <QAction>
#include "common/common/datahelper.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/styledpushbutton.h"
#include <QStatusBar>
#include <QHeaderView>
#include "fui_importcontact.h"
#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/documenthelper.h"
#include "fui_collection/fui_collection/dlg_rfparser.h"
#include "communicationmenu_base.h"
#include "bus_client/bus_client/emailhelper.h"

#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight					*g_AccessRight;				//global access right tester


#include "fui_dm_documents.h"

//global FUI manager:
#include "fuimanager.h"
extern FuiManager g_objFuiManager;					

//logged user:

 //global cache for user

//bo set
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	

//change manager:
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester



extern QString g_strLastDir_FileOpen;


Doc_MenuWidget::Doc_MenuWidget(QWidget *parent,bool bIgnoreViewMode)
	: QWidget(parent)
{
	ui.setupUi(this);
	m_bCloseOnSend=false;

	this->setAttribute(Qt::WA_DeleteOnClose, true);
	setAcceptDrops(true);

	m_lstProjects.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_lstContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_rowQCWContactDef.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

	//MAIN BUTTON
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR && !bIgnoreViewMode)
	{
		GUI_Helper::CreateStyledButton(ui.btnNewEntity,":Document24.png",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Documents & Applications")),15,ThemeManager::GetCEMenuButtonStyleLighter(),10);
		ui.btnNewEntity->setMinimumHeight(26);
		ui.btnHide->setMinimumHeight(26);
		ui.treeWidgetTemplate->setStyleSheet("* { font-size: 9px }"); 
		ui.treeWidgetApp->setStyleSheet("* { font-size: 9px }"); 
		ui.treeWidgetCheckedOutDocs->setStyleSheet("* { font-size: 9px }"); 
		ui.tabWidget->setMaximumHeight(150);
		ui.btnHide->setStyleSheet(ThemeManager::GetCEMenuButtonStyleLighter());
	}
	else
	{
		GUI_Helper::CreateStyledButton(ui.btnNewEntity,":Document32.png",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Documents")),15,ThemeManager::GetCEMenuButtonStyle(),10);
		ui.btnHide->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	}

	ui.btnHide->setFocusPolicy(Qt::NoFocus);
	ui.btnNewEntity->setFocusPolicy(Qt::NoFocus);

	//issue 1687: no more pie menu
	//connect(ui.btnNewEntity,SIGNAL(clicked()),this,SLOT(OnMenuButton()));
	//connect(&g_ThemeManager,SIGNAL(ThemeChanged()),this,SLOT(OnThemeChanged()));

	
	ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
	//GUI_Helper::CreateStyledButton(ui.btnHide,":arrow_up.png","",0,styleSectionButtonLvl0,0);
	connect(ui.btnHide,SIGNAL(clicked()),this,SLOT(OnHideTab()));


	//TEMPLATE TOOLBARS
	//connect(ui.btnNewFromTemplate, SIGNAL(clicked()), this, SLOT(OnNewFromTemplate()));
	//connect(ui.btnNewTask, SIGNAL(clicked()), this, SLOT(OnNewTask()));
	connect(ui.btnAddTemplate, SIGNAL(clicked()), this, SLOT(OnAddTemplate()));
	connect(ui.btnModifyTemplate, SIGNAL(clicked()), this, SLOT(OnModifyTemplate()));
	connect(ui.btnRemoveTemplate, SIGNAL(clicked()), this, SLOT(OnRemoveTemplate()));
	ui.btnAddTemplate->setIcon(QIcon(":handwrite.png"));
	ui.btnModifyTemplate->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnRemoveTemplate->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnModifyTemplate->setToolTip(tr("Modify Template"));
	ui.btnRemoveTemplate->setToolTip(tr("Delete Template"));
	ui.btnAddTemplate->setToolTip(tr("New Template"));

	//double click
	connect(ui.treeWidgetTemplate,SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)),this,SLOT(OnNewFromTemplate())); 
	connect(ui.treeWidgetApp,SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)),this,SLOT(OnNewFromApp())); 


	ui.btnNewFromTemplate->setIcon(QIcon(":Document16.png"));
	ui.btnNewFromTemplate->setToolTip(tr("New Document From Template"));

	ui.btnScheduleTask->setIcon(QIcon(":ToDo32.png"));
	ui.btnScheduleTask->setToolTip(tr("New Task From Template"));

	connect(ui.btnNewFromTemplate, SIGNAL(clicked()), this, SLOT(OnNewFromTemplate()));
	connect(ui.btnScheduleTask, SIGNAL(clicked()), this, SLOT(OnNewTask()));

	//APP TOOLBARS
	connect(ui.btnNewFromApp, SIGNAL(clicked()), this, SLOT(OnNewFromApp()));
	connect(ui.btnAddApp, SIGNAL(clicked()), this, SLOT(OnAddApp()));
	connect(ui.btnModifyApp, SIGNAL(clicked()), this, SLOT(OnModifyApp()));
	connect(ui.btnRemoveApp, SIGNAL(clicked()), this, SLOT(OnRemoveApp()));
	ui.btnNewFromApp->setIcon(QIcon(":Document16.png"));
	ui.btnNewFromApp->setToolTip(tr("New Document For Selected Application"));
	ui.btnAddApp->setIcon(QIcon(":handwrite.png"));
	ui.btnModifyApp->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnRemoveApp->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnModifyApp->setToolTip(tr("Modify/Create"));
	ui.btnRemoveApp->setToolTip(tr("Delete"));
	ui.btnAddApp->setToolTip(tr("Add"));

	connect(ui.btnPaths, SIGNAL(clicked()), this, SLOT(OnUserPaths()));
	ui.btnPaths->setIcon(QIcon(":handwithbook.png"));
	ui.btnPaths->setToolTip(tr("My Application Paths"));
	

	//REGISTER DOC:
	connect(ui.btnNewFromRegister, SIGNAL(clicked()), this, SLOT(OnRegister()));
	connect(ui.btnImport, SIGNAL(clicked()), this, SLOT(OnImport()));

	connect(ui.cmbCategory, SIGNAL( currentIndexChanged(const QString &)), this, SLOT(OnCategoryChanged(const QString)));


	//REGISTER CHK DOCS:
	connect(ui.btnCheckIn, SIGNAL(clicked()), this, SLOT(OnDocCheckIn()));


	//CHK DOCS TREE:
	ui.treeWidgetCheckedOutDocs->header()->hide();
	ui.treeWidgetCheckedOutDocs->setColumnCount(1);
	ui.treeWidgetCheckedOutDocs->setSelectionBehavior( QAbstractItemView::SelectRows);
	ui.treeWidgetCheckedOutDocs->setSelectionBehavior( QAbstractItemView::SelectItems);

	//TEMPLATE TREE:
	ui.treeWidgetTemplate->header()->hide();
	ui.treeWidgetTemplate->setColumnCount(1);
	ui.treeWidgetTemplate->setSelectionBehavior( QAbstractItemView::SelectRows);
	ui.treeWidgetTemplate->setSelectionBehavior( QAbstractItemView::SelectItems);


	//APP TREE:
	ui.treeWidgetApp->header()->hide();
	ui.treeWidgetApp->setColumnCount(2);
	ui.treeWidgetApp->setSelectionBehavior( QAbstractItemView::SelectRows);
	ui.treeWidgetApp->setSelectionBehavior( QAbstractItemView::SelectItems);


	//LOGGED USER:
	m_nLoggedID=g_pClientManager->GetPersonID();

	m_Category.Initialize(ENTITY_DOC_TEMPLATE_CATEGORY);

	//load all:
	LoadCategory();
	LoadTemplates();
	LoadApplications();
	LoadCheckedOutDocs();

	g_ChangeManager.registerObserver(this);

	//---------------------------------------- INIT CNTX MENU TEMPLATE--------------------------------//
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;

	
	pAction = new QAction(tr("New Document From Template"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnNewFromTemplate()));
	lstActions.append(pAction);

	//m_TemplateMenu.addAction(pAction);

	pAction = new QAction(tr("New Task From Template"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnNewTask()));
	lstActions.append(pAction);

	//m_TemplateMenu.addAction(pAction);
	


	pAction = new QAction(tr("Reload"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(ReLoadTemplates()));
	lstActions.append(pAction);

		SeparatorAct = new QAction(this);
		SeparatorAct->setSeparator(true);
		lstActions.append(SeparatorAct);
		
		if (g_AccessRight->TestAR(REGISTER_TEMPLATES))
		{
			pAction = new QAction(tr("Add Template"), this);
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnAddTemplate()));
			lstActions.append(pAction);
		}
		pAction = new QAction(tr("Modify Template"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnModifyTemplate()));
		lstActions.append(pAction);

		pAction = new QAction(tr("Delete Template"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnRemoveTemplate()));
		lstActions.append(pAction);
	


	//set cnxt menu:
	ui.treeWidgetTemplate->SetContextMenuActions(lstActions);

	lstActions.clear();
	
	//---------------------------------------- INIT CNTX MENU APP--------------------------------//

	pAction = new QAction(tr("New Document Using Application"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnNewFromApp()));
	lstActions.append(pAction);

	

	pAction = new QAction(tr("Reload"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(ReLoadApplications()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	if (g_AccessRight->TestAR(REGISTER_APPLICATIONS))
	{
		pAction = new QAction(tr("Add Application"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnAddApp()));
		lstActions.append(pAction);

	}
	pAction = new QAction(tr("Modify Application"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnModifyApp()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete Application"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnRemoveApp()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("View my application paths"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnUserPaths()));
	lstActions.append(pAction);

	//set cnxt menu:
	ui.treeWidgetApp->SetContextMenuActions(lstActions);

	//ui.btnNewFromTemplate->setMenu(&m_TemplateMenu);

	lstActions.clear();

		

	//---------------------------------------- INIT CNTX MENU CHK DOCS--------------------------------//

	pAction = new QAction(tr("Save/Check In Document"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDocCheckIn()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Reload"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(ReLoadCheckedOutDocs()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("Open Document"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDocOpen()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Open Document Details"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDocOpenFUI()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Unlock And Ignore All Local Changes"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDocCancelCheckOut()));
	lstActions.append(pAction);




	//set cnxt menu:
	ui.treeWidgetCheckedOutDocs->SetContextMenuActions(lstActions);


	connect(&m_PieMenu,SIGNAL(DocumentTypeSelected(int,int)),this,SLOT(OnDocumentTypeSelected(int,int)));



	//FUI:
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_DOCUMENT_TEMPLATES, nValue))
	{

		ui.tabWidget->removeTab(0);
		/*
		ui.tabWidget->widget(0)->setEnabled(false);
		ui.btnAddTemplate->setEnabled(false);
		ui.btnModifyTemplate->setEnabled(false);
		ui.btnRemoveTemplate->setEnabled(false);
		ui.btnNewFromTemplate->setEnabled(false);
		ui.btnScheduleTask->setEnabled(false);
		*/
	}

	if(!g_FunctionPoint.IsFPAvailable(FP_TASKS, nValue))
	{
		ui.btnScheduleTask->setEnabled(false);
	}

	//if no internet docs-> do not allow
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_INTERNET))
	{
		ui.tabWidget->widget(ui.tabWidget->count()-1)->setEnabled(false); //remove last
	}

	if (!g_AccessRight->TestAR(REGISTER_APPLICATIONS))
		ui.btnAddApp->setEnabled(false);

	if (!g_AccessRight->TestAR(REGISTER_TEMPLATES))
		ui.btnAddTemplate->setEnabled(false);


	QSize buttonSize(34, 34);
	StyledPushButton *pBtnNote		=	new StyledPushButton(this,":Note_32.png",":Note_32.png","","",0,0);
	StyledPushButton *pBtnAddress	=	new StyledPushButton(this,":Address_32.png",":Address_32.png","","",0,0);
	StyledPushButton *pBtnInternet	=	new StyledPushButton(this,":Internet_File_32.png",":Internet_File_32.png","","",0,0);
	StyledPushButton *pBtnLocal		=	new StyledPushButton(this,":Local_File_32.png",":Local_File_32.png","","",0,0);
	StyledPushButton *pBtnPaper		=	new StyledPushButton(this,":Paper_Docs_32.png",":Paper_Docs_32.png","","",0,0);

	//connect signals
	connect(pBtnNote,SIGNAL(clicked()),this,SLOT(OnNote()));
	connect(pBtnAddress,SIGNAL(clicked()),this,SLOT(OnAddress()));
	connect(pBtnInternet,SIGNAL(clicked()),this,SLOT(OnInternetFile()));
	connect(pBtnLocal,SIGNAL(clicked()),this,SLOT(OnLocalFile()));
	connect(pBtnPaper,SIGNAL(clicked()),this,SLOT(OnPaperDoc()));

	pBtnNote->setToolTip(tr("Note"));
	pBtnAddress->setToolTip(tr("Address"));
	pBtnInternet->setToolTip(tr("Internet File"));
	pBtnLocal->setToolTip(tr("Local File "));
	pBtnPaper->setToolTip(tr("Paper Document"));

	//size
	pBtnNote		->setMaximumSize(buttonSize);
	pBtnNote		->setMinimumSize(buttonSize);
	pBtnAddress		->setMaximumSize(buttonSize);
	pBtnAddress		->setMinimumSize(buttonSize);
	pBtnInternet	->setMaximumSize(buttonSize);
	pBtnInternet	->setMinimumSize(buttonSize);
	pBtnLocal		->setMaximumSize(buttonSize);
	pBtnLocal		->setMinimumSize(buttonSize);
	pBtnPaper		->setMaximumSize(buttonSize);
	pBtnPaper		->setMinimumSize(buttonSize);



	//Tool m_ProgressBar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnNote);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnInternet);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnLocal);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnAddress);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnPaper);
	buttonLayout->addStretch(1);

	buttonLayout->setContentsMargins(0,0,0,4);
	buttonLayout->setSpacing(0);

	ui.frameDetailToolBar->setLayout(buttonLayout);
	ui.frameDetailToolBar->setStyleSheet("QFrame#frameDetailToolBar "+ThemeManager::GetSidebarChapter_Bkg());
}

Doc_MenuWidget::~Doc_MenuWidget()
{
	g_ChangeManager.unregisterObserver(this);

}





/*
Qt::DropActions Doc_MenuWidget::supportedDropActions() const 
{
	return  Qt::CopyAction | Qt::MoveAction | Qt::IgnoreAction | Qt::LinkAction | Qt::ActionMask | Qt::TargetMoveAction;
}
*/


void Doc_MenuWidget::dragEnterEvent ( QDragEnterEvent * event )
{
	if (event->mimeData()->hasUrls())
		event->accept();

}

//if TAB= template then template, else doc, if not exe...brb....
void Doc_MenuWidget::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	if (!mime->hasUrls()) return;

	{
		QList<QUrl> lst =mime->urls();
		bool bDropOnTemplates=false;
		bool bAcceptApplications=false;
		if (event->pos().y()>ui.tabWidget->pos().y() && ui.tabWidget->currentIndex()==0 )
			bDropOnTemplates=true;
		if (event->pos().y()>ui.tabWidget->pos().y() && ui.tabWidget->currentIndex()==1 )
			bAcceptApplications=true;

		if (bAcceptApplications)
			if (!g_AccessRight->TestAR(REGISTER_APPLICATIONS))
				bAcceptApplications=false;

		if (bDropOnTemplates)
			if (!g_AccessRight->TestAR(REGISTER_TEMPLATES))
				bDropOnTemplates=false;

		if (!bDropOnTemplates && !bAcceptApplications)
		{
			event->ignore();
			return;
		}

		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT);
		this->activateWindow(); //bring on top_:
		QString strCategory=ui.cmbCategory->currentText();
		g_CommManager.ProcessDocumentDrop(lst,bDropOnTemplates,bAcceptApplications,false,strCategory);
	}
		
}

void Doc_MenuWidget::OnHideTab()
{
	bool bVisible=ui.tabWidget->isVisible();

	if (bVisible)
	{
		ui.btnHide->setIcon(QIcon(":Icon_Plus12.png"));
		ui.tabWidget->setVisible(false);
	}
	else
	{
		ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
		ui.tabWidget->setVisible(true);
	}
}


//load from server, rebuild tree
void Doc_MenuWidget::LoadTemplates(bool bReload)
{
	if (m_nLoggedID==0) return;

	DocumentHelper::GetDocTemplates(m_lstTemplates,bReload);
	m_lstTemplates.sort("BDMD_NAME"); //B.T. issue 2471
	

	ui.treeWidgetTemplate->blockSignals(true);
	ui.treeWidgetTemplate->clear();

	int nIDIdx=m_lstTemplates.getColumnIdx("BDMD_ID");
	int nNameIdx=m_lstTemplates.getColumnIdx("BDMD_NAME");
	int nTypeIdx=m_lstTemplates.getColumnIdx("BDMD_DOC_TYPE");
	int nDesc=m_lstTemplates.getColumnIdx("BDMD_DESCRIPTION");

	QString strCategory=ui.cmbCategory->currentText();

	QList<QTreeWidgetItem *> items;
	int nSize=m_lstTemplates.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//if category!='' and in template!='' then filter, else add...
		if (strCategory!=m_lstTemplates.getDataRef(i,"BDMD_CATEGORY").toString() && !m_lstTemplates.getDataRef(i,"BDMD_CATEGORY").toString().isEmpty() && !strCategory.isEmpty())
		{
			continue;
		}

		QString strNodeName=m_lstTemplates.getDataRef(i,nNameIdx).toString();
		int nID=m_lstTemplates.getDataRef(i,nIDIdx).toInt();
		QTreeWidgetItem *pRoot=new QTreeWidgetItem((QTreeWidget *)0,QStringList(strNodeName));
		pRoot->setData(0,Qt::UserRole,nID);
		pRoot->setToolTip(0,m_lstTemplates.getDataRef(i,nDesc).toString());

		//icon = doc type
		QPixmap pix=DocumentHelper::GetDocumentTypeIcon(m_lstTemplates.getDataRef(i,nTypeIdx).toInt());
		QIcon icon(pix);
		pRoot->setIcon(0,icon);

		items.append(pRoot);
	}
	ui.treeWidgetTemplate->insertTopLevelItems(0,items);
	ui.treeWidgetTemplate->blockSignals(false);

	
}

//load from server, rebuild tree
void Doc_MenuWidget::LoadApplications(bool bReload)
{
	if (m_nLoggedID==0) return;

	ui.treeWidgetApp->blockSignals(true);
	ui.treeWidgetApp->clear();

	DocumentHelper::GetDocApplications(m_lstApplications,bReload);


	

	int nIDIdx=m_lstApplications.getColumnIdx("BDMA_ID");
	int nCodeIdx=m_lstApplications.getColumnIdx("BDMA_CODE");
	int nNameIdx=m_lstApplications.getColumnIdx("BDMA_NAME");
	int nDesc=m_lstApplications.getColumnIdx("BDMA_DESCRIPTION");

	m_lstApplications.sort(nCodeIdx); //sort by code

	QList<QTreeWidgetItem *> items;
	int nSize=m_lstApplications.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//QString strNodeName=m_lstApplications.getDataRef(i,nCodeIdx).toString()+" "+m_lstApplications.getDataRef(i,nNameIdx).toString();
		QString strNodeName=m_lstApplications.getDataRef(i,nNameIdx).toString();
		int nID=m_lstApplications.getDataRef(i,nIDIdx).toInt();
		QStringList lst;
		lst<<""<<strNodeName;
		QTreeWidgetItem *pRoot=new QTreeWidgetItem((QTreeWidget *)0,lst);
		pRoot->setData(0,Qt::UserRole,nID);
		pRoot->setToolTip(0,m_lstApplications.getDataRef(i,nDesc).toString());
		pRoot->setToolTip(1,m_lstApplications.getDataRef(i,nDesc).toString());
		//set icon for application:
		QByteArray bytePicture=m_lstApplications.getDataRef(i,"BDMA_ICON").toByteArray();
		if (bytePicture.size()>0)
		{
			QPixmap pixMap;
			pixMap.loadFromData(bytePicture);
			QIcon icon(pixMap);
			pRoot->setIcon(0,icon);
		}

		// issue 1339
		if (m_lstApplications.getDataRef(i,"BDMA_NO_FILE_DOCUMENT").toInt()>0)
		{
			pRoot->setIcon(1,QIcon(":StartApp16g.png"));
		}
		else
		{
			pRoot->setIcon(1,QIcon(":Document_16b.png"));
		}


		pRoot->setData(0,Qt::UserRole,nID);
		items.append(pRoot);
	}
	ui.treeWidgetApp->insertTopLevelItems(0,items);
	ui.treeWidgetApp->header()->resizeSection(0,40);
	ui.treeWidgetApp->blockSignals(false);


}


void Doc_MenuWidget::LoadCheckedOutDocs(bool bReload)
{

	if (m_nLoggedID==0) return;

	ui.treeWidgetCheckedOutDocs->blockSignals(true);
	ui.treeWidgetCheckedOutDocs->clear();

	DocumentHelper::GetCheckedOutDocs(m_lstCheckedOutDocs,bReload);
	
	QList<bool> lstModified;
	DocumentHelper::TestCheckedOutDocsForModificiation(m_lstCheckedOutDocs,lstModified);

	int nIDIdx=m_lstCheckedOutDocs.getColumnIdx("BDMD_ID");
	int nNameIdx=m_lstCheckedOutDocs.getColumnIdx("BDMD_NAME");
	int nPathIdx=m_lstCheckedOutDocs.getColumnIdx("BDMD_DOC_PATH");

	QList<QTreeWidgetItem *> items;
	int nSize=m_lstCheckedOutDocs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strNodeName=m_lstCheckedOutDocs.getDataRef(i,nNameIdx).toString();
		int nID=m_lstCheckedOutDocs.getDataRef(i,nIDIdx).toInt();
		QTreeWidgetItem *pRoot=new QTreeWidgetItem((QTreeWidget *)0,QStringList(strNodeName));
		pRoot->setData(0,Qt::UserRole,nID);
		pRoot->setToolTip(0,m_lstCheckedOutDocs.getDataRef(i,nPathIdx).toString());

		if (lstModified.at(i)) //if modified
			pRoot->setIcon(0,QIcon(":CallS_Refused.png"));
		else
			pRoot->setIcon(0,QIcon(":Check_Out_16.png"));
			
		items.append(pRoot);
	}

	ui.treeWidgetCheckedOutDocs->insertTopLevelItems(0,items);
	ui.treeWidgetCheckedOutDocs->blockSignals(false);
}


void Doc_MenuWidget::LoadCategory(bool bReload)
{
	m_Category.ReloadData(bReload);

	int nIx=ui.cmbCategory->currentIndex();
	ui.cmbCategory->blockSignals(true);
	ui.cmbCategory->clear();

	DbRecordSet *lstData=m_Category.GetDataSource();
	int nSize=lstData->getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.cmbCategory->addItem(lstData->getDataRef(i,0).toString());
	}
	QString strLastCat=ui.cmbCategory->currentText();
	if (nIx!=-1)
		ui.cmbCategory->setCurrentIndex(nIx);
	else
		ui.cmbCategory->setCurrentIndex(0);


	ui.cmbCategory->blockSignals(false);
}


void Doc_MenuWidget::OnCategoryChanged(QString strCategory)
{
	//filter templates by category:
	LoadTemplates();
}


//-------------------------------------------------------------------------
//						TEMPLATES
//-------------------------------------------------------------------------




//go for new doc from template:
//template can be local or internet:
//if local all same, if internet:
void Doc_MenuWidget::OnNewFromTemplate()
{
	
	//get current template:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetTemplate->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstTemplates.find(0,nRecordID,true);
	if (nRow==-1)return;


	DbRecordSet row=m_lstTemplates.getRow(nRow);
	DbRecordSet rowDocData,recRevision;
	int nApplicationID=row.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
	int nRow1=m_lstApplications.find("BDMA_ID",nApplicationID,true);
	int nAppNoFile=0;
	if (nRow1!=-1)
		nAppNoFile=m_lstApplications.getDataRef(nRow1,"BDMA_NO_FILE_DOCUMENT").toInt();

	//-------------------------------------------------------
	//Create doc from template:
	//-------------------------------------------------------
	if(!DocumentHelper::CreateDocumentFromTemplate(row,rowDocData,recRevision,true,nAppNoFile))
		return;

	//clear revision (although document is saved locally-> path in rowDocData):
	//let user check in
	recRevision.clear();


	QString strFile=rowDocData.getDataRef(0,"BDMD_DOC_PATH").toString();
	int nDefaultDocType=rowDocData.getDataRef(0,"BDMD_DOC_TYPE").toInt();
	int nApplication=rowDocData.getDataRef(0,"BDMD_APPLICATION_ID").toInt();

	//if doc type = internet file, set as checked out
	if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
	{
		rowDocData.setData(0,"BDMD_IS_CHECK_OUT",1);
		rowDocData.setData(0,"BDMD_CHECK_OUT_USER_ID",g_pClientManager->GetPersonID());
		rowDocData.setData(0,"BDMD_CHECK_OUT_DATE",QDateTime::currentDateTime());
	}
	

	//if nota: then based on contact + project match []
	if (nDefaultDocType==GlobalConstants::DOC_TYPE_NOTE)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT);
		QString strBody=rowDocData.getDataRef(0,"BDMD_DESCRIPTION").toString();
		QString strNewBody;
		int nContactID=-1;
		int nProjectID=-1;
		if (m_lstContacts.getRowCount()>0)
		{
			int nIdx1=m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");
			nContactID=m_lstContacts.getDataRef(0,nIdx1).toInt();
		}
		if (m_lstProjects.getRowCount()>0)
		{
			int nIdx1=m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");
			nProjectID=m_lstProjects.getDataRef(0,nIdx1).toInt();
		}

		EmailHelper::CreateEmailFromTemplate(strBody,strNewBody,nContactID,nProjectID);
		rowDocData.setData(0,"BDMD_DESCRIPTION",strNewBody);
	}


	//open NoFile docuemnt:
	if (nAppNoFile)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT); //give me new data, if some1 is listening....
		DocumentHelper::SetDocumentDropDefaults(&m_lstContacts,&m_lstProjects,&m_rowQCWContactDef);
		DocumentHelper::OpenDocumentInExternalApp(rowDocData,true);
	}
	else
	{
		//write & open FUI in read mode
		WriteDocument(rowDocData,recRevision,true);

		//reload chek in docs:
		ReLoadCheckedOutDocs();
	}

	if (m_bCloseOnSend)
		close();

}



void Doc_MenuWidget::OnAddTemplate()
{
	m_PieMenu.CreateNewDocument(ui.btnAddTemplate,NULL,NULL,NULL,false,true);
}

void Doc_MenuWidget::OnModifyTemplate()
{
	
	//get selected cost center
	int nRecordID = -1;
	//get current item:
	QList<QTreeWidgetItem *> items=ui.treeWidgetTemplate->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EDIT, nRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(pFUI)pFUI->show();  //show FUI
	
}



void Doc_MenuWidget::OnRemoveTemplate()
{
	//get current item:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetTemplate->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	//warn
	int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete this record permanently from the database?"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES


	//delete
	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"BDMD_ID");
	lst.addRow();
	lst.setData(0,0,nRecordID);
	Status err;
	QString pLockResourceID;
	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	_SERVER_CALL(ClientSimpleORM->Delete(err,BUS_DM_DOCUMENTS,lst, pLockResourceID, lstStatusRows, pBoolTransaction))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}


	//remove item from tree:
	delete items.at(0);
	LoadCategory(true);
}




/*! Catches global cache events

	\param pSubject			- source of msg
	\param nMsgCode			- msg code
	\param val				- value sent from observer
*/
void Doc_MenuWidget::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}

	if(nMsgDetail==ENTITY_BUS_DM_DOCUMENTS)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			//always reload:
			LoadCategory(true);

			//CallBack to refresh displayed data:
			DbRecordSet lstNewData=val.value<DbRecordSet>();
			ui.treeWidgetTemplate->blockSignals(true);
			//set node to lstNewData
			if (lstNewData.getRowCount()>0)
			{
				if (lstNewData.getColumnIdx("BDMD_TEMPLATE_FLAG")>=0)
				{
					if (lstNewData.getDataRef(0,"BDMD_TEMPLATE_FLAG").toInt()>0) //only reload when new template mail is modified
					{
						//Reload data:
						LoadTemplates(true);

						QVariant varNode=lstNewData.getDataRef(0,"BDMD_ID");
						QTreeWidgetItem *item=SearchTree(ui.treeWidgetTemplate,varNode);
						if (item)
						{
							item->setSelected(true);
							ui.treeWidgetTemplate->setCurrentItem(item);
						}
					}
				}
				else
					LoadTemplates(true);

			}

			//reload checked:
			if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED || nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED)
			{
				ReLoadCheckedOutDocs();
			}



			ui.treeWidgetTemplate->blockSignals(false);
			return;
		}
		

	//for all
	if (nMsgCode==ChangeManager::GLOBAL_CHECK_IN_DOC ||nMsgCode==ChangeManager::GLOBAL_CHECK_OUT_DOC)
	{
		LoadCheckedOutDocs(); //already new data in cache
		//ReLoadCheckedOutDocs();
	}


	if(nMsgDetail==ENTITY_BUS_DM_APPLICATIONS)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			//CallBack to refresh displayed data:
			DbRecordSet lstNewData=val.value<DbRecordSet>();

			//Reload data:
			LoadApplications(true);

			ui.treeWidgetApp->blockSignals(true);
			//set node to lstNewData
			if (lstNewData.getRowCount()>0)
			{
				QVariant varNode=lstNewData.getDataRef(0,"BDMA_ID");
				QTreeWidgetItem *item=SearchTree(ui.treeWidgetApp,varNode);
				if (item)
				{
					item->setSelected(true);
					ui.treeWidgetApp->setCurrentItem(item);
				}
			}
			ui.treeWidgetApp->blockSignals(false);
			return;
		}



}




QTreeWidgetItem* Doc_MenuWidget::SearchTree(QTreeWidget* pTree, QVariant nUserValue)
{

	QTreeWidgetItem* item=NULL;

	int nSize=pTree->topLevelItemCount();
	for(int i=0;i<nSize;++i)
	{
		item=pTree->topLevelItem(i);
		//qDebug()<<item->data(0,Qt::UserRole).toInt();
		if (item->data(0,Qt::UserRole)==nUserValue)
			return item;
	}

	return NULL;
}


//-------------------------------------------------------------------------
//						APPLICATIONS
//-------------------------------------------------------------------------

/*
void Doc_MenuWidget::OnNewFromAppPie()
{
	m_PieMenu.SelectDocumentType(ui.btnNewFromApp,0);
}
*/


//app:
void Doc_MenuWidget::OnNewFromApp()
{
	//get current app:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetApp->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an application!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstApplications.find(0,nRecordID,true);
	if (nRow==-1)return;


	int nAppNoFile=m_lstApplications.getDataRef(nRow,"BDMA_NO_FILE_DOCUMENT").toInt();
	int nAppType=m_lstApplications.getDataRef(nRow,"BDMA_APPLICATION_TYPE").toInt();
	int nDefaultDocType;

	if (nAppNoFile>0 && nAppType!=GlobalConstants::DOC_TYPE_URL)
	{
		nDefaultDocType = GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
	}
	else
	{
		if (nAppType==GlobalConstants::APP_TYPE_FILE)
		{
			nDefaultDocType=DocumentHelper::AskForDocumentType();
			if (nDefaultDocType==-1)return;

			//int nResult=QMessageBox::question(NULL,tr("Choose Document Storage Location"),tr("Choose Document Storage Location:"),tr("  Save Reference(s) to Local File(s)  "),tr("  Store File Centrally on Internet  "),tr(" Cancel "),0,2);
			//if (nResult==0)
			//	nDefaultDocType = GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
			//else if (nResult==1)
			//	nDefaultDocType = GlobalConstants::DOC_TYPE_INTERNET_FILE;
			//else
			//	return; //exit
		}
		else 
		{
			nDefaultDocType=GlobalConstants::DOC_TYPE_URL;
		}
	}

		




	DbRecordSet row=m_lstApplications.getRow(nRow);
	int nApplication=row.getDataRef(0,"BDMA_ID").toInt();
	if (nApplication<=0)
		return;

	DbRecordSet rowDocData,recRevision;
	rowDocData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
	rowDocData.addRow();

	rowDocData.setData(0,"BDMD_NAME","New Document");
	rowDocData.setData(0,"BDMD_DOC_PATH","");
	rowDocData.setData(0,"BDMD_APPLICATION_ID",nApplication);
	rowDocData.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
	rowDocData.setData(0,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
	rowDocData.setData(0,"CENT_OWNER_ID",m_nLoggedID);
	rowDocData.setData(0,"BDMD_DOC_TYPE",nDefaultDocType);
	//rowDocData.setData(0,"CENT_IS_PRIVATE",0);
	rowDocData.setData(0,"BDMD_TEMPLATE_FLAG",0);


	//if got template and local or internet
	int nTemplateID=row.getDataRef(0,"BDMA_TEMPLATE_ID").toInt();
	if (nTemplateID>0)
	{
			//load template row:
			int nRow=m_lstTemplates.find(0,nTemplateID,true);
			if (nRow==-1) ReLoadTemplates();
			nRow=m_lstTemplates.find(0,nTemplateID,true);
			if (nRow==-1)
			{
				QMessageBox::warning(this,tr("Warning"),tr("Could not load template from application record!"));
				return;
			}


			//-------------------------------------------------------
			//Create doc from template:
			//-------------------------------------------------------


			DbRecordSet row=m_lstTemplates.getRow(nRow);

			if (nDefaultDocType==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
			{
				if(!DocumentHelper::CreateLocalDocumentFromTemplate(row,rowDocData,nAppNoFile))
					return;
			}
			else if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
			{
				if(!DocumentHelper::CreateInternetDocumentFromTemplate(row,rowDocData,recRevision,true))
					return;

				//clear revision:
				recRevision.clear();

				//automatically check out revision:
				if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
				{
					rowDocData.setData(0,"BDMD_IS_CHECK_OUT",1);
					rowDocData.setData(0,"BDMD_CHECK_OUT_USER_ID",g_pClientManager->GetPersonID());
					rowDocData.setData(0,"BDMD_CHECK_OUT_DATE",QDateTime::currentDateTime());
				}
			}
			else
			{
				if(!DocumentHelper::CreateDocumentFromTemplate(row,rowDocData,recRevision, true))
					return;
			}

	}


	//override application:
	rowDocData.setData(0,"BDMD_APPLICATION_ID",nApplication);

	//open NoFile docuemnt:
	if (nAppNoFile)
	{
		//get contacts:
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT); //give me new data, if some1 is listening....

		DocumentHelper::SetDocumentDropDefaults(&m_lstContacts,&m_lstProjects,&m_rowQCWContactDef);
		DocumentHelper::OpenDocumentInExternalApp(rowDocData,true);
	}
	else
		WriteDocument(rowDocData,recRevision,true);


	if (m_bCloseOnSend)
		close();

}


void Doc_MenuWidget::OnAddApp()
{
	g_objFuiManager.OpenFUI(MENU_DM_APPLICATIONS, true, false,FuiBase::MODE_INSERT);
}
void Doc_MenuWidget::OnModifyApp()
{
	//get selected cost center
	int nRecordID = -1;
	//get current item:
	QList<QTreeWidgetItem *> items=ui.treeWidgetApp->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an application!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_APPLICATIONS, true, false,FuiBase::MODE_EDIT, nRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(pFUI)pFUI->show();  //show FUI

}

void Doc_MenuWidget::OnRemoveApp()
{

	//get current item:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetApp->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an application!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	//warn
	int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete this record permanently from the database?"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES


	//delete
	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"BDMA_ID");
	lst.addRow();
	lst.setData(0,0,nRecordID);
	Status err;
	QString pLockResourceID;
	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	_SERVER_CALL(ClientSimpleORM->Delete(err,BUS_DM_APPLICATIONS,lst, pLockResourceID, lstStatusRows, pBoolTransaction))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}


	//remove item from tree:
	delete items.at(0);
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_DM_APPLICATIONS,QVariant(),this); 

}


//open empty doc
void Doc_MenuWidget::OnMenuButton()
{
	emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT);
	m_PieMenu.CreateNewDocument(ui.btnNewEntity,NULL,&m_lstContacts,&m_lstProjects,false,false);
}



//open file dialog, store path...
void Doc_MenuWidget::OnRegister()
{
	g_CommManager.LoadDocuments();

	/*
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.*";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Pick Existing Document"),
		strStartDir,
		strFilter);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	if(strFile.isEmpty())
		return;

	int nDefaultDocType=DocumentHelper::AskForDocumentType();
	if (nDefaultDocType==-1) return;

	//open DOC with defaults:
	DbRecordSet rowDocData;
	rowDocData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
	rowDocData.addRow();

	QFileInfo infoTarget(strFile);
	QString strBaseName=infoTarget.fileName();

	rowDocData.setData(0,"BDMD_DOC_PATH",strFile);
	rowDocData.setData(0,"BDMD_NAME",strBaseName);
	rowDocData.setData(0,"BDMD_DOC_TYPE",nDefaultDocType);

	//try resolve application by extension
	QString strExt=infoTarget.suffix();

	int nExtIdx=m_lstApplications.getColumnIdx("BDMA_EXTENSIONS");
	int nSizeApp=m_lstApplications.getRowCount();

	if (!strExt.isEmpty())
	{
		for (int k=0;k<nSizeApp;++k)
		{
			if (m_lstApplications.getDataRef(k,nExtIdx).toString().indexOf(strExt)>=0)
			{
				rowDocData.setData(0,"BDMD_APPLICATION_ID",m_lstApplications.getDataRef(k,0)); //set to app
				break;
			}
		}
	}


	DbRecordSet recRevision;
	if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
	{
		if(!DocumentHelper::CreateDocumentRevisionFromPath(recRevision,strFile,false,NULL,true)) //TEST TEST!!!!
		{
			QMessageBox::critical(NULL,tr("Error"),tr("Revision could not be loaded for file:")+strFile,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
	}
	
	//open FUI
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EMPTY, -1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_DM_Documents* pFuiDM=dynamic_cast<FUI_DM_Documents*>(pFUI);
	if (pFuiDM)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT); //give me new data, if some1 is listening....
		pFuiDM->SetDefaults(nDefaultDocType,&rowDocData,&m_lstContacts,&m_lstProjects,false,false,&recRevision);
	}
	if(pFUI)pFUI->show();  //show FUI
*/
}


void Doc_MenuWidget::OnUserPaths()
{
	g_objFuiManager.OpenFUI(MENU_DM_USER_PATHS, true, false,FuiBase::MODE_EMPTY);
}



//lstContacts= must have bcnt id, project must have busp_id
void Doc_MenuWidget::SetDefaults(DbRecordSet *lstContacts,DbRecordSet *lstProjects,DbRecordSet *rowQCWContact)
{
	m_lstContacts.clear();
	m_lstProjects.clear();
	m_rowQCWContactDef.clear();

	if (lstContacts)
	{
		//lstContacts->Dump();

		int nCntIdx=lstContacts->getColumnIdx("BCNT_ID");
		int nCntIdx1=m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);
	
		int nSize=lstContacts->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			m_lstContacts.addRow();
			m_lstContacts.setData(m_lstContacts.getRowCount()-1,nCntIdx1,lstContacts->getDataRef(i,nCntIdx));
		}
		
	}

	if (lstProjects)
	{

		int nCntIdx=lstProjects->getColumnIdx("BUSP_ID");
		int nCntIdx1=m_lstProjects.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		int nSize=lstProjects->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			m_lstProjects.addRow();
			m_lstProjects.setData(m_lstProjects.getRowCount()-1,nCntIdx1,lstProjects->getDataRef(i,nCntIdx));
		}

	}

	if (rowQCWContact)
	{
		if (rowQCWContact->getRowCount()>0)
			m_rowQCWContactDef=*rowQCWContact;
	}
}


/*

//imports docs: sets path, 
//lookup all applications with same extensions, assignes them
//set default type, sets name = name of file
void Doc_MenuWidget::RegisterDocumentPaths(DbRecordSet &lstPaths,int nIsTemplate)
{

	DbRecordSet lstRevisions;
	lstRevisions.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_REVISIONS));

	int nDefaultDocType=m_PieMenu.AskForDocumentType();
	if (nDefaultDocType==-1) return;

	DbRecordSet lstWrite;
	lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));

	int nExtIdx=m_lstApplications.getColumnIdx("BDMA_EXTENSIONS");
	int nSizeApp=m_lstApplications.getRowCount();
	int nSize=lstPaths.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		lstWrite.addRow();
		int nRow=lstWrite.getRowCount()-1;

		QString strFilePath=lstPaths.getDataRef(i,0).toString();
		lstWrite.setData(nRow,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
		lstWrite.setData(nRow,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
		lstWrite.setData(nRow,"CENT_OWNER_ID",m_nLoggedID);
		lstWrite.setData(nRow,"BDMD_NAME",lstPaths.getDataRef(i,1));
		lstWrite.setData(nRow,"BDMD_DOC_PATH",strFilePath);
		lstWrite.setData(nRow,"BDMD_DOC_TYPE",nDefaultDocType);
		lstWrite.setData(nRow,"BDMD_TEMPLATE_FLAG",nIsTemplate);
		lstWrite.setData(nRow,"BDMD_IS_PRIVATE",0);

		//try resolve application by extension
		QString strExt=lstPaths.getDataRef(i,2).toString();
		if (!strExt.isEmpty())
		{
			for (int k=0;k<nSizeApp;++k)
			{
				if (m_lstApplications.getDataRef(k,nExtIdx).toString().indexOf(strExt)>=0)
				{
					lstWrite.setData(nRow,"BDMD_APPLICATION_ID",m_lstApplications.getDataRef(k,0)); //set to app
					break;
				}
			}
		}


		if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		{
			DbRecordSet recRevision;
			if(!CommunicationManager::CreateDocumentRevisionFromPath(recRevision,strFilePath))
			{
				QMessageBox::critical(NULL,tr("Error"),tr("Revision could not be loaded for file:")+strFilePath,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			lstRevisions.merge(recRevision);
		}
	}

	if (lstWrite.getRowCount()==0)
		return;


	//write & open FUI
	WriteDocument(lstWrite,lstRevisions,false);

	
	//reload from server:
	if (nIsTemplate)
		LoadTemplates(true);
	
	//place info:
	//QString strMsg=
	//QMessageBox::information(this,tr("Information"),tr("Successfully registered {")+QVariant(nSize).toString()+tr("} document(s)!"));

}






//write app: code=name, usr paths...
void Doc_MenuWidget::RegisterApplicationPaths(DbRecordSet &lstPaths)
{
	DbRecordSet lstWrite;
	lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS));

	DbRecordSet lstWriteUserPath;
	lstWriteUserPath.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_USER_PATHS));

	int nSize=lstPaths.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		lstWrite.addRow();
		int nRow=lstWrite.getRowCount()-1;
		lstWriteUserPath.addRow();

		lstWrite.setData(nRow,"BDMA_CODE",lstPaths.getDataRef(i,1));
		lstWrite.setData(nRow,"BDMA_NAME",lstPaths.getDataRef(i,1));
		lstWriteUserPath.setData(nRow,"BDMU_APP_PATH",lstPaths.getDataRef(i,0));
	}

	//write:
	Status err;
	_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_APPLICATIONS,lstWrite))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	int nSize2=lstWrite.getRowCount();
	for (int i=0;i<nSize2;++i)
	{
		lstWriteUserPath.setData(i,"BDMU_OWNER_ID",m_nLoggedID);
		lstWriteUserPath.setData(i,"BDMU_APPLICATION_ID",lstWrite.getDataRef(0,0));
	}

	_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_USER_PATHS,lstWriteUserPath))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	LoadApplications(true);

}

*/


void Doc_MenuWidget::OnNewTask()
{

	
	//get current template:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetTemplate->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstTemplates.find(0,nRecordID,true);
	if (nRow==-1)return;


	//-------------------------------------------------------
	//Copy from template, assign all fields that are possible
	//-------------------------------------------------------

	DbRecordSet rowDocData,recRevision;
	DbRecordSet rowTemplate=m_lstTemplates.getRow(nRow);

	//-------------------------------------------------------
	//Create doc from template:
	//-------------------------------------------------------
	if(!DocumentHelper::CreateDocumentFromTemplate(rowTemplate,rowDocData,recRevision,true))
		return;


	QString strFile=rowDocData.getDataRef(0,"BDMD_DOC_PATH").toString();
	int nDefaultDocType=rowDocData.getDataRef(0,"BDMD_DOC_TYPE").toInt();
	int nApplication=rowDocData.getDataRef(0,"BDMD_APPLICATION_ID").toInt();

	//open FUI
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EMPTY, -1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_DM_Documents* pFuiDM=dynamic_cast<FUI_DM_Documents*>(pFUI);
	if (pFuiDM)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT); //give me new data, if some1 is listening....
		pFuiDM->SetDefaults(nDefaultDocType,&rowDocData,&m_lstContacts,&m_lstProjects,true,false,&recRevision);
	}
	if(pFUI)pFUI->show();  //show FUI


	//m_PieMenu.CreateNewDocument(ui.btnNewFromTemplate,&rowDocData,&m_lstContacts,&m_lstProjects,true,false);
}




void Doc_MenuWidget::OnDocumentTypeSelected(int nOperation,int nDocType)
{


	if (nOperation==0)
	{
		//OnNewFromApp(nDocType);
	}

}






bool Doc_MenuWidget::WriteDocument(DbRecordSet &lstDocs,DbRecordSet &lstRevisions,bool bOpenDoc)
{

	//if file size is greateer then allowed
	if(!DocumentHelper::TestDocumentRevisionsForMaximumSize(lstRevisions))
		return false;

	if (lstDocs.getRowCount()==0)
		return false;

	QString strFile=lstDocs.getDataRef(0,"BDMD_DOC_PATH").toString();
	int nApplication=lstDocs.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
	int nDocType=lstDocs.getDataRef(0,"BDMD_DOC_TYPE").toInt();


	//get contacts:
	emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT); //give me new data, if some1 is listening....

	//-------------------------------------------------------
	//Write
	//------------------------------------------------------
	DbRecordSet empty;
	Status err;
	DbRecordSet lstUAR,lstGAR;
	_SERVER_CALL(BusDocuments->Write(err,lstDocs,m_lstProjects,m_lstContacts,empty,lstRevisions,"",lstUAR,lstGAR))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return false;
	}



	//-------------------------------------------------------
	//Send signal for all to reload:
	//------------------------------------------------------
	QVariant varData;
	qVariantSetValue(varData,lstDocs);
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ENTITY_BUS_DM_DOCUMENTS,varData,this); 


	//-------------------------------------------------------
	//Open document:
	//-------------------------------------------------------
	if (bOpenDoc)
	{

		//get back id:
		int nRecordID=lstDocs.getDataRef(0,0).toInt();

		if (nDocType==GlobalConstants::DOC_TYPE_URL || nDocType==GlobalConstants::DOC_TYPE_NOTE) //if url or note do not open app, just go edit
		{
			int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EDIT, nRecordID);
			return true;
		}


		//open FUI
		int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_READ, nRecordID);


		if (nApplication<=0) return true;

		QString strAppPath;
		int nRow=m_lstApplications.find(0,nApplication,true);
		if (nRow==-1) return true;

		//uset advanced methoD:
		return DocumentHelper::OpenDocumentInExternalApp(lstDocs);

	}
	else
		return true;


}



//----------------------------------------------------------------------
//						CHK DOCS
//----------------------------------------------------------------------


void Doc_MenuWidget::OnDocCheckIn()
{
	//get current id:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetCheckedOutDocs->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an document!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstCheckedOutDocs.find(0,nRecordID,true);
	if (nRow==-1)return;
	
	DbRecordSet lstDocsForCheckIn;
	lstDocsForCheckIn.copyDefinition(m_lstCheckedOutDocs);
	lstDocsForCheckIn.merge(m_lstCheckedOutDocs.getRow(nRow));
	//int nDocID=m_lstCheckedOutDocs.getDataRef(nRow,"BDMD_ID").toInt();
	//QString strFilePath=m_lstCheckedOutDocs.getDataRef(nRow,"BDMD_DOC_PATH").toString();
	//DbRecordSet rowRev;
	DocumentHelper::CheckInIfPossible(lstDocsForCheckIn);
	//DocumentHelper::CheckInDocument(nDocID,strFilePath,rowRev,"",false,false,m_lstCheckedOutDocs.getDataRef(nRow,"BDMD_SET_TAG_FLAG").toInt()); //if success it will auto refresh

}
void Doc_MenuWidget::OnDocOpenFUI()
{
	//get current id:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetCheckedOutDocs->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an document!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstCheckedOutDocs.find(0,nRecordID,true);
	if (nRow==-1)return;
	int nDocID=m_lstCheckedOutDocs.getDataRef(nRow,"BDMD_ID").toInt();

	g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_READ, nDocID);

}
void Doc_MenuWidget::OnDocOpen()
{
	//get current id:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetCheckedOutDocs->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an document!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstCheckedOutDocs.find(0,nRecordID,true);
	if (nRow==-1)return;

	DbRecordSet rowDoc=m_lstCheckedOutDocs.getRow(nRow);

	DocumentHelper::OpenDocumentInExternalApp(rowDoc);
}

void Doc_MenuWidget::OnDocCancelCheckOut()
{
	//get current id:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetCheckedOutDocs->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an document!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstCheckedOutDocs.find(0,nRecordID,true);
	if (nRow==-1)return;
	int nDocID=m_lstCheckedOutDocs.getDataRef(nRow,"BDMD_ID").toInt();


	DocumentHelper::CancelCheckOutDocument(nDocID);  //if sucess it will auto refresh
}








bool Doc_MenuWidget::IsHidden()
{
	if (height()<=50)
		return true;
	else
		return false;
}

void Doc_MenuWidget::SetHidden(bool bHide)
{
	if (bHide)
	{
		ui.btnHide->setIcon(QIcon(":Icon_Plus12.png"));
		ui.tabWidget->setVisible(false);
	}
	else
	{
		ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
		ui.tabWidget->setVisible(true);
	}
}
void Doc_MenuWidget::SetCloseOnSend()
{
	m_bCloseOnSend=true;
	//delete on close
	this->setAttribute(Qt::WA_DeleteOnClose, true);
	m_PieMenu.SetCloseOnOpenDocument(this);
}


void Doc_MenuWidget::OnThemeChanged()
{
	ui.btnNewEntity->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	ui.btnHide->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	ui.frameDetailToolBar->setStyleSheet("QFrame#frameDetailToolBar "+ThemeManager::GetSidebarChapter_Bkg());
}


void Doc_MenuWidget::SetSideBarLayout()
{
	this->setWindowFlags(Qt::FramelessWindowHint);

	QVBoxLayout *layout=dynamic_cast<QVBoxLayout*>(this->layout());

	//HEADER:
	QFrame *pHeader= new QFrame(this);
	pHeader->setFrameShape(QFrame::NoFrame);
	pHeader->setMaximumHeight(30);
	pHeader->setMinimumHeight(30);
	pHeader->setObjectName("Header_bar");
	pHeader->setStyleSheet("QFrame#Header_bar "+ThemeManager::GetSidebarChapter_Bkg());

	QSize buttonSize_menu(13, 11);
	StyledPushButton *close		= new StyledPushButton(this,":Icon_CloseBlack.png",":Icon_CloseBlack.png","");
	close		->setMaximumSize(buttonSize_menu);
	close		->setMinimumSize(buttonSize_menu);
	close		->setToolTip(tr("Close"));

	QLabel *label = new QLabel(this);
	label->setStyleSheet("QLabel "+ThemeManager::GetSidebarChapter_Font());
	label->setText(tr("Document Manager"));

	QVBoxLayout *buttonLayout_menu_v = new QVBoxLayout;
	buttonLayout_menu_v->addWidget(close);
	buttonLayout_menu_v->addStretch(1);
	buttonLayout_menu_v->setSpacing(0);
	buttonLayout_menu_v->setContentsMargins(2,0,2,2);

	QHBoxLayout *buttonLayout_menu_h = new QHBoxLayout;
	
	buttonLayout_menu_h->addWidget(label);
	buttonLayout_menu_h->addStretch(1);
	buttonLayout_menu_h->addLayout(buttonLayout_menu_v);
	buttonLayout_menu_h->setSpacing(0);
	buttonLayout_menu_h->setContentsMargins(0,0,0,0);

	pHeader->setLayout(buttonLayout_menu_h);

	connect(close,SIGNAL(clicked()),this,SLOT(close()));


	//FOOTER:
	QStatusBar *statusBar = new QStatusBar(this);
	statusBar->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	statusBar->setObjectName("Status_bar");

	QFrame *pFooter= new QFrame(this);
	pFooter->setFrameShape(QFrame::NoFrame);
	pFooter->setMaximumHeight(20);
	pFooter->setMinimumHeight(20);

	QHBoxLayout *footer = new QHBoxLayout;
	footer->addWidget(statusBar);
	footer->setSpacing(0);
	footer->setContentsMargins(0,0,0,0);
	pFooter->setLayout(footer);

	pFooter->setStyleSheet("QFrame "+ThemeManager::GetSidebarChapter_Bkg());


	//GLOBAL BKG COLOR:
	this->setObjectName("MY_WIDGET");
	this->setStyleSheet("QWidget#MY_WIDGET "+ThemeManager::GetSidebar_Bkg()+ThemeManager::GetGlobalWidgetStyle());

	ui.btnNewEntity->setVisible(false);

	//SET IT
	layout->insertWidget(0,pHeader);
	layout->addWidget(pFooter);
}



void Doc_MenuWidget::mouseMoveEvent(QMouseEvent *event)
{
		move(event->globalPos() - m_dragPosition);
		event->accept();
} 

void Doc_MenuWidget::mouseReleaseEvent(QMouseEvent * event)
{
	QWidget::mouseReleaseEvent(event);
}


void Doc_MenuWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && event->y()<30) 
	{
		m_dragPosition = event->globalPos() - frameGeometry().topLeft();
		event->accept();
	}
	else
		m_dragPosition=QPoint(0,0);

} 



void Doc_MenuWidget::OnLocalFile()
{
	OpenFUI(GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE);
}
void Doc_MenuWidget::OnInternetFile()
{
	OpenFUI(GlobalConstants::DOC_TYPE_INTERNET_FILE);
}

void Doc_MenuWidget::OnNote()
{
	OpenFUI(GlobalConstants::DOC_TYPE_NOTE);
}
void Doc_MenuWidget::OnAddress()
{
	OpenFUI(GlobalConstants::DOC_TYPE_URL);
}
void Doc_MenuWidget::OnPaperDoc()
{
	OpenFUI(GlobalConstants::DOC_TYPE_PHYSICAL);
}


void Doc_MenuWidget::OpenFUI(int nDocType)
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EMPTY, -1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_DM_Documents* pFuiDM=dynamic_cast<FUI_DM_Documents*>(pFUI);
	if (pFuiDM)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT);
		pFuiDM->SetDefaults(nDocType,NULL,&m_lstContacts,&m_lstProjects);  
		pFuiDM->show();  //show FUI
	}

}

void Doc_MenuWidget::OnImport()
{
	Dlg_RFParser Dlg;
	Status err;
	if(Dlg.Initialize(err,"",true))
	{
		_CHK_ERR(err);
		if(Dlg.exec())
		{
			QString strPath;
			DbRecordSet lstApps,lstTemplates,lstInterfaces;
			Dlg.GetResult(lstApps,lstTemplates,lstInterfaces,strPath);
			QFileInfo filek(strPath);
			DocumentHelper::RegisterApplicationsAndTemplatesFromRFFile(lstApps,lstTemplates,lstInterfaces,filek.absolutePath());
		}

	}

}

void Doc_MenuWidget::SetCurrentTab()
{
	ui.tabWidget->setCurrentIndex(1);
}