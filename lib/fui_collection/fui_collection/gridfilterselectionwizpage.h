#ifndef GRIDFILTERSELECTIONWIZPAGE_H
#define GRIDFILTERSELECTIONWIZPAGE_H

#include "ui_gridfilterselectionwizpage.h"
#include "gui_core/gui_core/wizardpage.h"
#include "common/common/dbrecordset.h"
#include "commgridfilter.h"


class GridFilterSelectionWizPage : public WizardPage
{
	Q_OBJECT

public:
	GridFilterSelectionWizPage(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
	~GridFilterSelectionWizPage();
	void Initialize();
	void WriteToCache();
	void ReadFromCache();

private:
	Ui::GridFilterSelectionWizPageClass ui;

	void SetFilterDataToResultRecordSet(int nViewID, DbRecordSet &recFilterViewData);

	CommGridFilter *m_pCommGridFilter;
	int				m_nViewID;
	DbRecordSet		m_rFilterViews;		//Filter views.
	DbRecordSet		m_rFilterViewData;	//Filter views saved settings - checked, which one is used, etc.

private slots:
	void on_FilterValueChanged();
};

#endif // GRIDFILTERSELECTIONWIZPAGE_H
