#ifndef FUI_COREUSER_H
#define FUI_COREUSER_H

#include <QWidget>
#include "generatedfiles/ui_fui_coreuser.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"

/*!
	\class  FUI_CoreUser
	\brief  FUI Business Person
	\ingroup FUI_Collection
*/
class FUI_CoreUser : public FuiBase
{
	Q_OBJECT

public:
	FUI_CoreUser(QWidget *parent = 0);
	~FUI_CoreUser();

	void on_cmdInsert();
	void on_cmdCopy();
	QString GetFUIName();

protected:
	void DataPrepareForInsert(Status &err);
	void DataCheckBeforeWrite(Status &err);

private:
	Ui::FUI_CoreUserClass ui;
	GuiFieldManager *m_GuiFieldManagerMain;
	bool on_cmdOK();
	bool on_cmdDelete();
	void prepareCacheRecord(DbRecordSet* recNewData);

private slots:
	void on_groupBox_toggled(bool);
	void on_pushButton_clicked();
	void on_btnGeneratePassword_clicked();
	void on_txtPassword1_editingFinished();
	void on_txtPassword2_editingFinished();

private:
	void RefreshDisplay();
	void SetMode(int nMode);
	void IsPerson();

	//passwords:
	void LoadPassword();
	void ClearPassword();
	bool CheckPassword();
	bool m_bPassWordChanged;
	int m_nPasswordIdx;
	MainEntityCalculatedName m_CalcName;
};


#endif // FUI_COREUSER_H
