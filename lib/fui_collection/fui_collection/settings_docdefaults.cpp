#include "settings_docdefaults.h"
#include "communicationmanager.h"
#include <QFileDialog>
#include "common/common/datahelper.h"
#include "bus_client/bus_client/documenthelper.h"

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
extern QString g_strLastDir_FileOpen;

Settings_DocDefaults::Settings_DocDefaults(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
	:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

	//setup path buttons:
	ui.btnSelect->setIcon(QIcon(":SAP_Select.png"));
	ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnSelect->setToolTip(tr("Select Path"));
	ui.btnRemove->setToolTip(tr("Remove Path"));

	connect(ui.btnSelect,SIGNAL(clicked()),this,SLOT(OnSelectPath()));
	connect(ui.btnRemove,SIGNAL(clicked()),this,SLOT(OnRemovePath()));


	//read settings, set on window:

	//TYPE:
	switch(GetSettingValue(PERSON_DEFAULT_DOCUMENT_TYPE).toInt())
	{
	case 0:
		ui.rb0->setChecked(true);
		break;
	case 1:
		ui.rbAsk->setChecked(true);
		break;
	case 2:
		ui.rb1->setChecked(true);
		break;
	case 3:
		ui.rb2->setChecked(true);
	    break;
	}


	//if internet disabled return local:
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_INTERNET, nValue))
	{
		ui.rb2->setEnabled(false);
	}

	//else return internet:
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_LOCAL, nValue))
	{
		ui.rb1->setEnabled(false);
	}

	QString strPath=DocumentHelper::GetDefaultDocumentDirectory();
	ui.txtPath->setText(strPath);

	//if empty set to new, set dirty flag
	if (GetSettingValue(PERSON_DEFAULT_DOCUMENT_DIR).toString().isEmpty())
	{
		strPath+="_"+g_pClientManager->GetConnectionName();
		SetSettingValue(PERSON_DEFAULT_DOCUMENT_DIR, strPath);
	}

	ui.ckbSkipCheckOutWarning->setChecked(!GetSettingValue(DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS).toInt());
	
}

Settings_DocDefaults::~Settings_DocDefaults()
{

}

DbRecordSet Settings_DocDefaults::GetChangedValuesRecordSet()
{
	//save:
	int nDocTpye=0;
	if (ui.rbAsk->isChecked())
		nDocTpye=1;

	if (ui.rb1->isChecked())
		nDocTpye=2;

	if (ui.rb2->isChecked())
		nDocTpye=3;

	//only set if different:
	if (nDocTpye!=GetSettingValue(PERSON_DEFAULT_DOCUMENT_TYPE).toInt())
		SetSettingValue(PERSON_DEFAULT_DOCUMENT_TYPE, nDocTpye);

	QString strPath=ui.txtPath->toPlainText();

	strPath.replace("\\","/");
	if(strPath.lastIndexOf("\\")==strPath.length()-1)
		strPath.chop(1);

	if (strPath!=GetSettingValue(PERSON_DEFAULT_DOCUMENT_DIR).toString())
		SetSettingValue(PERSON_DEFAULT_DOCUMENT_DIR, strPath);

	if (ui.ckbSkipCheckOutWarning->isChecked())
		SetSettingValue(DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS, 0);
	else
		SetSettingValue(DOCUMENTS_WARN_ABOUT_CHECK_OUT_DOCUMENTS, 1);

	
	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

void Settings_DocDefaults::OnSelectPath()
{
	QString strStartDir=ui.txtPath->toPlainText();

	
	if (strStartDir.isEmpty())
	{
		strStartDir=DocumentHelper::GetDefaultDocumentDirectory();
	}
	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetMyDocumentsDir();
	}
	




	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Default Directory For Temporary Documents"),
		strStartDir);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath->setText(strFile);
	}
}

void Settings_DocDefaults::OnRemovePath()
{
	ui.txtPath->clear();
}