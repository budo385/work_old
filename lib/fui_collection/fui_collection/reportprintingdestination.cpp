#include "reportprintingdestination.h"
#include "report_definitions/report_definitions/reportregistration.h"
#include <QTableWidgetItem>
#include <QHeaderView>

ReportPrintingDestination::ReportPrintingDestination(int nWizardPageID, QString strPageTitle, QWidget *parent)
    : WizardPage(nWizardPageID, strPageTitle)
{
	m_recResultRecordSet.addColumn(QVariant::Int, "PrintDestination");
	m_recResultRecordSet.addRow();

	SetFinishBtnTitle(QString(tr("Print")));
}

ReportPrintingDestination::~ReportPrintingDestination()
{

}

void ReportPrintingDestination::Initialize()
{
	if (m_bInitialized)
		return;
	m_bInitialized = true;

	ui.setupUi(this);

	connect(ui.treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(CompleteChanged()));

	//Read from cache.
	ReadFromCache();

	ui.treeWidget->setColumnCount(1);
	ui.treeWidget->setRootIsDecorated(false);
	ui.treeWidget->header()->hide();
	ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	SetupItems();
}

void ReportPrintingDestination::SetupItems()
{
	QTreeWidgetItem *PreviewItem	= new QTreeWidgetItem(QStringList(tr("Print to screen")));
	QTreeWidgetItem *PrinterItem	= new QTreeWidgetItem(QStringList(tr("Send to printer")));
	QTreeWidgetItem *PdfItem		= new QTreeWidgetItem(QStringList(tr("Print to PDF")));

	m_lstPrintDestinations << PreviewItem << PrinterItem << PdfItem;

	PreviewItem->setIcon(0, QIcon(":gotopage.png"));
	PrinterItem->setIcon(0, QIcon(":print.png"));
	PdfItem->setIcon(0, QIcon(":pdf_icon.png"));

	ui.treeWidget->addTopLevelItem(PreviewItem);
	ui.treeWidget->addTopLevelItem(PrinterItem);
	ui.treeWidget->addTopLevelItem(PdfItem);

	QTreeWidgetItem *selItem = m_lstPrintDestinations.value(m_recResultRecordSet.getDataRef(0, 0).toInt());
	ui.treeWidget->setItemSelected(selItem, true);
}

void ReportPrintingDestination::CompleteChanged()
{
	if (ui.treeWidget->selectedItems().count() != 0)
	{
		QTreeWidgetItem *item = ui.treeWidget->selectedItems().first();
		int itemRow = ui.treeWidget->indexOfTopLevelItem(item);
		m_bComplete = true;
		if (itemRow==0)
			m_recResultRecordSet.setData(0, 0, ReportRegistration::REPORT_DESTINATION_PREVIEW);
		if (itemRow==1)
			m_recResultRecordSet.setData(0, 0, ReportRegistration::REPORT_DESTINATION_PRINTER);
		if (itemRow==2)
			m_recResultRecordSet.setData(0, 0, ReportRegistration::REPORT_DESTINATION_PDF);
	}
	else
		m_bComplete = false;

	emit completeStateChanged();
}