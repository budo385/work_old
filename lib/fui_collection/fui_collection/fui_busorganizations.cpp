#include "fui_busorganizations.h"
#include "fuimanager.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "common/common/entity_id_collection.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester


FUI_BusOrganizations::FUI_BusOrganizations(QWidget *parent)
	: FuiBase(parent)
{
	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);
	InitFui(ENTITY_BUS_ORGANIZATION, BUS_ORGANIZATION, dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	EnableHierarchyCodeCheck("BORG_CODE");//reload left menu after write
	ui.btnButtonBar->RegisterFUI(this);
	ui.frameSelection->Initialize(ENTITY_BUS_ORGANIZATION);	//init selection controller
	InitializeFUIWidget(ui.splitter);

	//init selection controllers for the other groups (multiple selection support)
	ui.frameDepartments->Initialize(ENTITY_BUS_DEPARTMENT, true,true);
	ui.frameCostCenters->Initialize(ENTITY_BUS_COST_CENTER, true,true);
	ui.framePersons->Initialize(ENTITY_BUS_PERSON, true,true);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_BUS_ORGANIZATION);
	m_GuiFieldManagerMain->RegisterField("BORG_CODE",ui.txtCode);
	m_GuiFieldManagerMain->RegisterField("BORG_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BORG_ACTIVE_FLAG",ui.chkActive);
	m_GuiFieldManagerMain->RegisterField("BORG_DESCRIPTION",ui.txtDescription);

	//set edit to false:
	SetMode(MODE_EMPTY);
	ui.txtName->setFocus();
}

FUI_BusOrganizations::~FUI_BusOrganizations()
{
	delete m_GuiFieldManagerMain;
}

//when programatically change data, call this:
void FUI_BusOrganizations::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();
}

//when changing state, call this:
void FUI_BusOrganizations::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameDetails->setEnabled(true);			//needed because of inverse state of one btn

		ui.txtCode->setEnabled(false);
		ui.txtName->setEnabled(false);
		ui.chkActive->setEnabled(false);
		ui.txtDescription->setEnabled(false);
		ui.btnEditDescription->setEnabled(false);
		
		ui.btnOpenCostCenters->setEnabled(false);
		ui.btnOpenDepartments->setEnabled(false);
		ui.frameSelection->setEnabled(true);

		ui.btnButtonBar->SetMode(nMode); //this is necessary to reeanble buttons!
	}
	else
	{
		ui.frameDetails->setEnabled(true);

		ui.txtCode->setEnabled(true);
		ui.txtName->setEnabled(true);
		ui.chkActive->setEnabled(true);
		ui.txtDescription->setEnabled(true);
		ui.btnEditDescription->setEnabled(true);

		ui.btnOpenCostCenters->setEnabled(true);
		ui.btnOpenDepartments->setEnabled(true);
		ui.frameSelection->setEnabled(false);

		//Set focus to Pers. No:
		ui.txtCode->setFocus(Qt::OtherFocusReason);
	}

	//these buttons are always enabled (MB changed his mind again)
	ui.btnOpenCostCenters->setEnabled(true);
	ui.btnOpenDepartments->setEnabled(true);
	ui.btnOpenPersons->setEnabled(true);
	ui.frameDepartments->setEnabled(true);
	ui.frameCostCenters->setEnabled(true);

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}


void FUI_BusOrganizations::on_selectionChange(int nEntityRecordID)
{
	FuiBase::on_selectionChange(nEntityRecordID);	//call base class

	//filter other data to this org id
	FilterAssigments(nEntityRecordID);
}

void FUI_BusOrganizations::on_cmdInsert()
{
	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_ORGANIZATION_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_ORGANIZATION, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed organizations: ")+QVariant(nCnt).toString()+tr("!\nCan not insert new organization!"));
			return;
		}
	}

	//filter other data to dummy record:
	FilterAssigments(-1);
	FuiBase::on_cmdInsert();
	ui.txtName->setFocus();
}


//switch to detail tab:
void FUI_BusOrganizations::on_cmdCopy()
{
	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_ORGANIZATION_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_ORGANIZATION, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed organizations: ")+QVariant(nCnt).toString()+tr("!\nCan not insert new organization!"));
			return;
		}
	}

	FuiBase::on_cmdCopy();
}

void FUI_BusOrganizations::on_btnOpenDepartments_clicked()
{
	//get selected department
	int nRecordID = -1;
	QString strCode, strName;
	DbRecordSet set;
	ui.frameDepartments->GetSelection(nRecordID, strCode, strName, set);

	//open FUI,but do not show until all defaults are set
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DEPARTMENTS, true,false,FuiBase::MODE_READ, nRecordID, true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(pFUI)
	{
		if(m_nEntityRecordID>0 && nRecordID==-1)//if valid ORG, load as default in new FUI
		{
			dynamic_cast<FuiBase *>(pFUI)->SetActualOrganization(m_nEntityRecordID);
		}
		pFUI->show();  //show FUI
	}
}

void FUI_BusOrganizations::on_btnOpenCostCenters_clicked()
{
	//get selected cost center
	int nRecordID = -1;
	QString strCode, strName;
	DbRecordSet set;
	ui.frameCostCenters->GetSelection(nRecordID, strCode, strName, set);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_COST_CENTERS, true, false,FuiBase::MODE_READ, nRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(pFUI)
	{
		if(m_nEntityRecordID>0 && nRecordID==-1)//if valid ORG, load as default in new FUI
		{
			dynamic_cast<FuiBase *>(pFUI)->SetActualOrganization(m_nEntityRecordID);
		}
		pFUI->show();  //show FUI
	}
}

void FUI_BusOrganizations::on_btnOpenPersons_clicked()
{
	//get selected person
	int nRecordID = -1;
	QString strCode, strName;
	DbRecordSet set;
	ui.framePersons->GetSelection(nRecordID, strCode, strName, set);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_PERSON, true, false,FuiBase::MODE_READ, nRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(pFUI)
	{
		if(m_nEntityRecordID>0 && nRecordID==-1)//if valid ORG, load as default in new FUI
		{
			dynamic_cast<FuiBase *>(pFUI)->SetActualOrganization(m_nEntityRecordID);
		}
		pFUI->show();  //show FUI
	}
	
}

void FUI_BusOrganizations::FilterAssigments(int nOrgID)
{
	if (nOrgID<=0)//clear grid:
	{
		ui.framePersons->GetDataSource()->clear();
		ui.framePersons->RefreshDisplay();
		ui.frameCostCenters->GetDataSource()->clear();
		ui.frameCostCenters->RefreshDisplay();
		ui.frameDepartments->GetDataSource()->clear();
		ui.frameDepartments->RefreshDisplay();
		return;
	}

	//set filters to empty on others:_
	ui.framePersons->ClearLocalFilter();
	ui.framePersons->GetLocalFilter()->SetFilter("BPER_ORGANIZATION_ID",nOrgID);
	ui.frameCostCenters->ClearLocalFilter();
	ui.frameCostCenters->GetLocalFilter()->SetFilter("BCTC_ORGANIZATION_ID",nOrgID);
	ui.frameDepartments->ClearLocalFilter();
	ui.frameDepartments->GetLocalFilter()->SetFilter("BDEPT_ORGANIZATION_ID",nOrgID);

	ui.framePersons->ReloadData();
	ui.frameCostCenters->ReloadData();
	ui.frameDepartments->ReloadData();
}

void FUI_BusOrganizations::on_btnEditDescription_clicked()
{
	QString strHtml = ui.txtDescription->toHtml();

	RichEditorDlg dlg;
	dlg.SetHtml(strHtml);
	if(0 != dlg.exec()){
		ui.txtDescription->setHtml(dlg.GetHtml());
	}
}

//get FUI name
QString FUI_BusOrganizations::GetFUIName()
{
	if(m_nEntityRecordID>0)
		return tr("Organization: ")+ui.frameSelection->GetCalculatedName(m_nEntityRecordID);
	else
		return tr("Organization");
}


void FUI_BusOrganizations::DataPrepareForInsert(Status &err)
{
	DataClear();
	m_lstData.addRow();
	m_lstData.setData(0,"BORG_ACTIVE_FLAG",1);
	m_lstData.setData(0,"BORG_ICON","");	
}
void FUI_BusOrganizations::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	if(m_lstData.getDataRef(0,"BORG_CODE").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Code is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"BORG_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Name is mandatory field!"));	return;	}
}