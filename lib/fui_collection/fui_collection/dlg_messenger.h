#ifndef DLG_MESSENGER_H
#define DLG_MESSENGER_H

#include <QtWidgets/QDialog>
#include <QToolButton>
#include <QStatusBar>
#include "generatedfiles/ui_dlg_messenger.h"
#include "common/common/observer_ptrn.h"



class Dlg_Messenger : public QDialog
{
    Q_OBJECT

public:
    Dlg_Messenger(QWidget *parent = 0);
    ~Dlg_Messenger();

	void Initialize(QToolButton *btnOpenMessenger, QStatusBar *pStatusBar=NULL);


public slots:
	void OnMessageLogged(int nMsgCode,int nMsgDetail,QString strVal);
	void OnShowMessenger();

private:
    Ui::Dlg_MessengerClass ui;

	QToolButton *m_btnOpenMessenger;
	QStatusBar *m_pStatusBar;


};

#endif // DLG_MESSENGER_H
