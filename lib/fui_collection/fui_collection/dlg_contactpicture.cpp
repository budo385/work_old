#include "dlg_contactpicture.h"
#include "gui_core/gui_core/thememanager.h"
Dlg_ContactPicture::Dlg_ContactPicture(QWidget *parent)
    : QDialog(parent),m_DataManager(&m_lstData)
{
	ui.setupUi(this);


	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PICTURE));
	m_lstData.addColumn(QVariant::ByteArray,"BPIC_PICTURE");

	m_GuiFieldManagerMain =new GuiFieldManager(&m_DataManager,DbSqlTableView::TVIEW_BUS_CM_PICTURE);

	//connect 
	m_GuiFieldManagerMain->RegisterField("BCMPC_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BCMPC_DATE",ui.txtDate);
	//m_GuiFieldManagerMain->RegisterField("BPIC_PICTURE",ui.pictureBig); //big pic
	m_GuiFieldManagerMain->RegisterField("BCMPC_TEXT",ui.frameEditor);

	//init pic, avoid asking for resize:
	ui.pictureBig->Initialize(PictureWidget::MODE_FULL,&m_DataManager,"BCMPC_PICTURE","BPIC_PICTURE","BCMPC_BIG_PICTURE_ID","BCMPC_ID");

	this->setWindowTitle(tr("Add a Picture"));//set dialog title

	//issue 1702:
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

Dlg_ContactPicture::~Dlg_ContactPicture()
{

	delete m_GuiFieldManagerMain;

}


void Dlg_ContactPicture::SetRow(DbRecordSet &row,bool bEditMode)
{
	m_bEditMode=bEditMode;
	m_lstData=row;

	//all enabled
	if(bEditMode)
	{
		ui.frameEditor->SetEditMode(true);
		ui.pictureBig->SetEditMode(true);
	}
	else
	{
		ui.frameEditor->SetEditMode(false);
		ui.pictureBig->SetEditMode(false);
		ui.txtDate->setEnabled(false);
		ui.txtName->setEnabled(false);
	}

	m_DataManager.EnableDataChangeTracking("BCMPC_ID");
	m_DataManager.ClearState();
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.pictureBig->RefreshDisplay();

	ui.txtName->setFocus(Qt::OtherFocusReason);
	
	ui.pictureBig->DisableNewWindow();
}


bool Dlg_ContactPicture::GetRow(DbRecordSet &row)
{
	row=m_lstData;
	return m_DataManager.IsChanged();
}




//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void Dlg_ContactPicture::on_btnOK_clicked()
{

	//if nothing changed abort:
	if(!m_DataManager.IsChanged())
		done(0);

	//only if edit mode recreate preview pic to 80x80:
	/*
	if(m_bEditMode)
	{
		//create preview picture
		QPixmap pixmap=ui.pictureBig->GetPixmap();
		PictureHelper::ResizePixMapToPreview(pixmap);
		QByteArray bufPicture;
		PictureHelper::ConvertPixMapToByteArray(bufPicture,pixmap);
		m_DataManager.ChangeData(0,"BCMPC_PICTURE",bufPicture);
	}
	*/

	done(1);
}


void Dlg_ContactPicture::on_btnCancel_clicked()
{
	done(0);
}





