#ifndef CALENDARMULTIDAYENTITYITEM_H
#define CALENDARMULTIDAYENTITYITEM_H

#include "calendarentityitem.h"
#include "calendarmultydaydisplaywidget.h"
#include "calendarmultidaygraphicsview.h"
#include <QGraphicsProxyWidget>
#include <QTextEdit>

class CalendarMultiDayEntityItem : public QGraphicsProxyWidget
{
	Q_OBJECT

public:
	CalendarMultiDayEntityItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, QDateTime startDateTime, 
		QDateTime endDateTime, QDateTime correctedStartDateTime, QDateTime correctedEndDateTime, QString strTitle, QString strLocation, 
		QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
		CalendarMultiDayGraphicsView *pCalendarMultiDayGraphicsView, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed, 
		QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
	~CalendarMultiDayEntityItem();

	void UpdateMultyDayEntityItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, QDateTime startDateTime, 
		QDateTime endDateTime, QDateTime correctedStartDateTime, QDateTime correctedEndDateTime, QString strTitle, QString strLocation, 
		QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
		int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed);

	void setVertFactor(int nHeightFactor){m_nHeightFactor = nHeightFactor;};
	void setVertOrder(int nVertOrder){m_nVertOrder = nVertOrder;};
	int getVertOrder(){return m_nVertOrder;};
	void getStartEndDateTime(QDateTime	&startDateTime, QDateTime &endDateTime);
	int getItemEntityID(){return m_nEntityID;};
	int getItemEntityType(){return m_nEnitiyType;};
	int getItemCentID(){return m_nCentID;};
	int getItemBcepID(){return m_nBcepID;};
	int getItemBcevID(){return m_nBcevID;};
	int getItemBCOL_ID(){return m_nBCOL_ID;};
	void SetSelected(bool bSelected = true);

public slots:
	void on_UpdateCalendar();
	void On_CnxtModify();
	void On_CnxtDelete();

protected:
	void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
	void dragLeaveEvent(QGraphicsSceneDragDropEvent *event);
	void dragMoveEvent(QGraphicsSceneDragDropEvent *event);
	void dropEvent(QGraphicsSceneDragDropEvent *event);
	void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent * event);
	bool viewportEvent(QEvent *event);

private:
	CalendarEntityItem::ResizeType isInResizeArea(const QPointF &pos);
	void CreateContextMenuActions(QList<QAction*> &lstActions);

	int				  m_nHeightFactor;
	int				  m_nVertOrder;
	QDateTime		  m_startDateTime;
	QDateTime		  m_endDateTime;
	QDateTime		  m_correctedStartDateTime;
	QDateTime		  m_correctedEndDateTime;
	QString			  m_strDisplayText;
	int				  m_nEntityID;
	int				  m_nEnitiyType;
	int				  m_nCentID;
	int				  m_nBcepID;
	int				  m_nBcevID;
	int				  m_nBCOL_ID;
	int				  m_nBcep_Status;
	int				  m_nBCEV_IS_INFORMATION;
	bool			  m_bIsProposed;
	QTextEdit		  *m_pTextEdit;
	QPoint			  m_ptItemPosition;
	CalendarMultiDayGraphicsView *m_pCalendarMultiDayGraphicsView;
	CalendarMultyDayDisplayWidget *m_pDisplayWidget; 
	bool			  m_isLeftHResizing;
	bool			  m_isRightHResizing;
	bool			  m_bIsMoving;
	int				  m_nHeight;
	int				  m_nWidth;
	QList<QAction*>	  m_lstActions;

signals:
	void ItemResizing(bool bIsResizing);
	void ItemResized(QGraphicsProxyWidget *thisItem, bool bLeftBorder);
	void ItemModify(QGraphicsProxyWidget *thisItem);
	void DropEntityOnItem(QGraphicsProxyWidget *thisItem, int nEntityID, int nEntityType, int nEntityPersonID);
	void ItemDelete(QGraphicsProxyWidget *thisItem);
	void ItemMoved(QGraphicsProxyWidget *thisItem); 
};

#endif // CALENDARMULTIDAYENTITYITEM_H
