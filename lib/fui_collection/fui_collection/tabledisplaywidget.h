#ifndef TABLEDISPLAYWIDGET_H
#define TABLEDISPLAYWIDGET_H

#include <QtWidgets/QTableWidget>
#include "calendargraphicsview.h"

class TableDisplayWidget : public QTableWidget
{
	Q_OBJECT

public:
	TableDisplayWidget(CalendarGraphicsView *pCalendarGraphicsView, int rows, int columns, bool bEntityItem = false, QWidget *parent = 0);

protected:
	bool viewportEvent(QEvent *event);

private:
	CalendarGraphicsView *m_pCalendarGraphicsView;

	bool m_bEntityItem;
};

#endif // TABLEDISPLAYWIDGET_H
