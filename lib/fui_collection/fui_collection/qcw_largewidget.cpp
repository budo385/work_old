#include "qcw_largewidget.h"
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager					g_ChangeManager;			//global message dispatcher
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "gui_core/gui_core/macros.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;


QCW_LargeWidget::QCW_LargeWidget(QWidget *parent)
	: QCW_Base(parent)
{
	ui.setupUi(this);

	ui.addSelected_toolButton_4->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_5->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_6->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_7->setIcon(QIcon(":CopyText16.png"));
	ui.btnAddressOrg2Pers->setIcon(QIcon(":Address_16.png"));
	ui.btnAddressOrg2Pers->setToolTip(tr("Copy organization address to the contact address"));

//	StyledPushButton *pBtnClear	= new StyledPushButton(this,":close.png",":close.png", "", QString("<html><b><font size=\"4\">%1 </font></b></html>").arg(tr("Clear All")),0,0);
	QPushButton *pClearAllButton = new QPushButton();
	pClearAllButton->setIcon(QIcon(":Delete16_qcw.png"));
	pClearAllButton->setText(tr("Clear All"));
	ui.largewidget_tabWidget->setCornerWidget(pClearAllButton);

	ui.framePersonOrganization->Initialize(ENTITY_CONTACT_ORGANIZATION, &m_recFullContactData, "BCNT_ORGANIZATIONNAME", false);
	
	connect(ui.leftSelector_frame,  SIGNAL(SelectionChanged(int, DbRecordSet)), this, SLOT(on_leftSelector_SelectionChanged(int, DbRecordSet)));
	connect(ui.leftSelector_frame,  SIGNAL(NewDataDropped(int, DbRecordSet)), this, SLOT(on_leftSelector_NewDataDropped(int, DbRecordSet)));
	connect(ui.leftSelector_frame,  SIGNAL(SetNames(QString,QString,QString)), this, SLOT(on_leftSelector_SetNames(QString,QString,QString)));
	connect(ui.leftSelector_frame,  SIGNAL(SetOrganization(QString)), this, SLOT(on_leftSelector_SetOrganization(QString)));
	connect(ui.contact_frameEditor, SIGNAL(textChanged()), this, SLOT(on_contact_frameEditor_textChanged()));
	connect(pClearAllButton, SIGNAL(clicked()), this, SLOT(on_ClearAll_clicked()));
	connect(ui.framePersonOrganization, SIGNAL(ComboTextChanged(const QString &)), this, SLOT(on_organization_lineEdit_textChanged(const QString &)));
	connect(ui.framePersonOrganization, SIGNAL(ComboTextEditFinish()), this, SLOT(on_organization_editingFinished()));
	SetFUIStyle();

	connect(ui.address_widget,SIGNAL(TownChanged(QString)),this,SLOT(OnTownChanged(QString)));

	ui.framePaymentConditions->Initialize(ENTITY_PAYMENT_CONDITIONS, &m_lstDebtor,"BCMD_PAYMENT_ID");
	m_GuiFieldManagerDebtor=new GuiFieldManager(&m_lstDebtor,DbSqlTableView::TVIEW_BUS_CM_DEBTOR);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_CUSTOMERCODE",ui.txtCustomerCode);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_DEBTORCODE",ui.txtDebtorCode);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_DEBTORACCOUNT",ui.txtDebtorAccount);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_DESCRIPTION",ui.frameDebtorDesc);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_INTERNAL_DEBTOR",ui.ckbInternalDebtor);

}

QCW_LargeWidget::~QCW_LargeWidget()
{
}

bool QCW_LargeWidget::Initialize(DbRecordSet recContactData)
{
	QCW_Base::Initialize(recContactData);
	//Setup left selector.
	ui.framePersonOrganization->RefreshDisplay();
	ui.leftSelector_frame->m_lstContacts = recContactData;	
	ui.leftSelector_frame->RefreshDisplay();
	ui.address_widget->SetLayout();
	m_GuiFieldManagerDebtor->RefreshDisplay();

	return true;
}

bool QCW_LargeWidget::Initialize(int nEntityRecordID)
{
	bool bOK=QCW_Base::Initialize(nEntityRecordID);
	if (!bOK) return false;

	ui.framePersonOrganization->RefreshDisplay();
	ui.leftSelector_frame->ShowContactGrid(false);
	m_GuiFieldManagerDebtor->RefreshDisplay();
	return true;
}

bool QCW_LargeWidget::Initialize(QString strDropText)
{
	//go insert mode
	Initialize(0);

	//init parse procedure
	ui.leftSelector_frame->SetDropText(strDropText);

	return true;
}

QString QCW_LargeWidget::GetSelectedText()
{
	return ui.leftSelector_frame->GetSelectedText();
}

void QCW_LargeWidget::SetDefaultValues(DbRecordSet &recContactData, QString strLeftSelectorText)
{
	//If no rows in recordset go insert mode.
	if (!recContactData.getRowCount())
		Initialize(0);
	//Else go insert mode but put existing data in fields.
	else
	{
		Initialize(0);
		m_recFullContactData = recContactData;
		SetupFrames();
	}		

	//init parse procedure
	ui.leftSelector_frame->SetDropText(strLeftSelectorText,true);
}

void QCW_LargeWidget::on_leftSelector_SelectionChanged(int nSelectedRow, DbRecordSet recContactData)
{
	//First return changed data to left selector.
	if (m_nSelectedRowInLeftSelector >= 0)
		ui.leftSelector_frame->m_lstContacts.assignRow(m_nSelectedRowInLeftSelector, m_recFullContactData);
	
	m_recFullContactData.clear();

	//Then get selected row.
	m_nSelectedRowInLeftSelector = nSelectedRow;
	m_recFullContactData = recContactData;

	SetupFrames();
}

void QCW_LargeWidget::on_leftSelector_NewDataDropped(int nDropType, DbRecordSet lstDroppedData)
{
	//_DUMP(lstDroppedData);
	//If nothing to add return.
	if (!lstDroppedData.getRowCount())
		return;

	if (nDropType == ENTITY_BUS_CONTACT)
	{
		if(m_nEntityRecordID > 0)
		{
			DbRecordSet recAddress	= lstDroppedData.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
			DbRecordSet recEmail	= lstDroppedData.getDataRef(0, "LST_EMAIL").value<DbRecordSet>();
			DbRecordSet recInternet = lstDroppedData.getDataRef(0, "LST_INTERNET").value<DbRecordSet>();
			DbRecordSet recPhones	= lstDroppedData.getDataRef(0, "LST_PHONE").value<DbRecordSet>();

			//_DUMP(recAddress);
			//_DUMP(recEmail);
			//_DUMP(recInternet);
			//_DUMP(recPhones);

			if (recPhones.getRowCount())
				ui.phone_widget->AddPhone(recPhones);
			if (recInternet.getRowCount())
				ui.internet_widget->AddInternetAddress(recInternet);
			if (recEmail.getRowCount())
				ui.email_widget->AddEmail(recEmail);
			if (recAddress.getRowCount())
				ui.address_widget->AddAddress(recAddress);
		}
		else
		{
			//Do not override this data with empty fields.
			CheckAdresses(lstDroppedData);
			CheckEmail(lstDroppedData);
			CheckInternetAdresses(lstDroppedData);
			CheckPhones(lstDroppedData);
			
			m_recFullContactData = lstDroppedData;
			SetupFrames(true);
		}
	}
	else if (nDropType == ENTITY_BUS_CM_PHONE)
	{
		ui.phone_widget->AddPhone(lstDroppedData);
	}
	else if (nDropType == ENTITY_BUS_CM_INTERNET)
	{
		ui.internet_widget->AddInternetAddress(lstDroppedData);
	}
	else if (nDropType == ENTITY_BUS_CM_ADDRESS)
	{
		ui.address_widget->AddAddress(lstDroppedData);
	}
	else if (nDropType == ENTITY_BUS_CM_EMAIL)
	{
		ui.email_widget->AddEmail(lstDroppedData);
	}
}

void QCW_LargeWidget::on_leftSelector_SetNames(QString strFirstName, QString strLastName, QString strMiddleName)
{
	ui.first_name_lineEdit->setText(strFirstName);
	ui.middle_name_lineEdit->setText(strMiddleName);
	ui.last_name_lineEdit->setText(strLastName);
}

void QCW_LargeWidget::on_leftSelector_SetOrganization(QString strOrganization)
{
	ui.framePersonOrganization->SetCurrentDisplayName(strOrganization);
}

void QCW_LargeWidget::SetupFrames(bool bCheckNames /*= false*/)
{
	SetupContactWidgets(&m_recFullContactData, bCheckNames);

	ui.address_widget->Initialize(&m_recFullContactData, this, 0);
	ui.email_widget->Initialize(&m_recFullContactData, this, 0);
	ui.internet_widget->Initialize(&m_recFullContactData, this, 0);
	ui.phone_widget->Initialize(&m_recFullContactData, this);
	ui.salutation_widget->Initialize(&m_recFullContactData, this, 0);
	ui.language_widget->Initialize(&m_recFullContactData, this, 0);
	ui.accessrights_widget->Initialize(&m_recFullContactData);
}

void QCW_LargeWidget::SetDataToRecordSet(QString strRecordSetFieldName, QString strValue)
{
	m_recFullContactData.setData(0, strRecordSetFieldName, strValue);
}

void QCW_LargeWidget::SetupContactWidgets(DbRecordSet *recFullContactData, bool bCheckNames /*= false*/)
{
	ui.first_name_lineEdit->blockSignals(true);
	ui.middle_name_lineEdit->blockSignals(true);
	ui.last_name_lineEdit->blockSignals(true);
	ui.location_lineEdit->blockSignals(true);

	//If last and first name exists and parser return some value don't show them, put old ones in recordset.
	if (!bCheckNames || (bCheckNames && ui.first_name_lineEdit->text().isEmpty()))
		ui.first_name_lineEdit->setText(recFullContactData->getDataRef(0, "BCNT_FIRSTNAME").toString());
	else
		SetDataToRecordSet("BCNT_FIRSTNAME", ui.first_name_lineEdit->text());
	if (!bCheckNames || (bCheckNames && ui.last_name_lineEdit->text().isEmpty()))
		ui.last_name_lineEdit->setText(recFullContactData->getDataRef(0, "BCNT_LASTNAME").toString());
	else
		SetDataToRecordSet("BCNT_LASTNAME", ui.last_name_lineEdit->text());
	ui.middle_name_lineEdit->setText(recFullContactData->getDataRef(0, "BCNT_MIDDLENAME").toString());
	ui.location_lineEdit->setText(recFullContactData->getDataRef(0, "BCNT_LOCATION").toString());
	ui.first_name_lineEdit->blockSignals(false);
	ui.middle_name_lineEdit->blockSignals(false);
	ui.last_name_lineEdit->blockSignals(false);
	ui.location_lineEdit->blockSignals(false);

	//More widget.
	ui.more_widget->Initialize(recFullContactData, this, 0);
	ui.contact_frameEditor->blockSignals(true);
	ui.contact_frameEditor->SetHtml(m_recFullContactData.getDataRef(0, "BCNT_DESCRIPTION").toString());
	ui.contact_frameEditor->blockSignals(false);
	if (m_recFullContactData.getDataRef(0, "BCNT_SEX").toInt() == 2)
		ui.unknown_radioButton->setChecked(true);
	else if (m_recFullContactData.getDataRef(0, "BCNT_SEX").toBool())
		ui.female_radioButton->setChecked(true);
	else
		ui.male_radioButton->setChecked(true);

	//BT: issue:
	ui.framePersonOrganization->SetCurrentDisplayName(recFullContactData->getDataRef(0, "BCNT_ORGANIZATIONNAME").toString(),true);
}

void QCW_LargeWidget::on_contact_frameEditor_textChanged()
{
	SetDataToRecordSet("BCNT_DESCRIPTION", ui.contact_frameEditor->GetHtml());
}

void QCW_LargeWidget::on_organization_lineEdit_textChanged(const QString & text)
{
	SetDataToRecordSet("BCNT_ORGANIZATIONNAME", text);
	ui.address_widget->SetOrganization(text);
}

void QCW_LargeWidget::on_first_name_lineEdit_textChanged(const QString & text)
{
	SetDataToRecordSet("BCNT_FIRSTNAME", text);
	ui.address_widget->SetFirstName(text);
}

void QCW_LargeWidget::on_middle_name_lineEdit_textChanged(const QString & text)
{
	SetDataToRecordSet("BCNT_MIDDLENAME", text);
	ui.address_widget->SetMiddleName(text);
}

void QCW_LargeWidget::on_last_name_lineEdit_textChanged(const QString & text)
{
	SetDataToRecordSet("BCNT_LASTNAME", text);
	ui.address_widget->SetLastName(text);
}

void QCW_LargeWidget::on_location_lineEdit_textChanged(const QString & text)
{
	SetDataToRecordSet("BCNT_LOCATION", text);
	ui.address_widget->SetTown(text);
}

void QCW_LargeWidget::on_male_radioButton_clicked(bool checked)
{
	if (checked)
		m_recFullContactData.setData(0, "BCNT_SEX", 0);
}

void QCW_LargeWidget::on_female_radioButton_clicked(bool checked)
{
	if (checked)
		m_recFullContactData.setData(0, "BCNT_SEX", 1);
}

void QCW_LargeWidget::on_unknown_radioButton_clicked(bool checked)
{
	if (checked)
		m_recFullContactData.setData(0, "BCNT_SEX", 2);
}

//Add selected part.
void QCW_LargeWidget::on_addSelected_toolButton_4_clicked()
{
	ui.framePersonOrganization->SetCurrentDisplayName(GetSelectedText());
}

void QCW_LargeWidget::on_addSelected_toolButton_5_clicked()
{
	ui.first_name_lineEdit->setText(GetSelectedText());
}

void QCW_LargeWidget::on_addSelected_toolButton_6_clicked()
{
	ui.last_name_lineEdit->setText(GetSelectedText());
}

void QCW_LargeWidget::on_addSelected_toolButton_7_clicked()
{
	ui.location_lineEdit->setText(GetSelectedText());
}

void QCW_LargeWidget::on_ok_pushButton_clicked()
{

	CheckDebtor(); //B.T. added

	if (m_nQCWMode == MODE_SINGLE)
	{

		int nFormatAddressAlways=g_pSettings->GetApplicationOption(APP_FORMAT_CONTACT_ADDRESS_ALWAYS).toInt();
		if (nFormatAddressAlways)
		{
			ui.address_widget->on_addressformat_toolButton_clicked();
		}

		DbRecordSet lstUAR,lstGAR;
		ui.accessrights_widget->GetUarListsForSave(lstUAR,lstGAR);

		int nOperation;
		if(!OnOK(nOperation,lstUAR,lstGAR))
			return;

		//m_recFullContactData.Dump();

		//notify cache:
		if (nOperation)
			ClientContactManager::NotifyCacheAfterContactWrite(false,m_recFullContactData,this);
		else
			ClientContactManager::NotifyCacheAfterContactWrite(true,m_recFullContactData,this);

		QVariant varData;
		qVariantSetValue(varData,m_recFullContactData);
		notifyObservers(ChangeManager::GLOBAL_QCW_WINDOW_FINISH_PROCESS,nOperation,varData,this);
	}
	else
	{
		//Selection changed slot is called to remember current changes.
		ui.leftSelector_frame->SlotSelectionChanged();
		QVariant varData;
		qVariantSetValue(varData,ui.leftSelector_frame->m_lstContacts);
		notifyObservers(ChangeManager::GLOBAL_QCW_WINDOW_FINISH_PROCESS,-1,varData,this);
	}

	close();
}

void QCW_LargeWidget::on_cancel_pushButton_clicked()
{
	OnCancel();
}

void QCW_LargeWidget::on_ClearAll_clicked()
{
	ClearAll();
}


void QCW_LargeWidget::on_organization_editingFinished()
{
//issue 2885: set new button
}

void QCW_LargeWidget::UpdateSalutationOnAdressFrame(QString strRecordSetFieldName, QString strValue)
{
	ui.address_widget->UpdateSalutation(strRecordSetFieldName, strValue);
}

void QCW_LargeWidget::OnTownChanged(QString strTown)
{
	if (ui.location_lineEdit->text().isEmpty())
		ui.location_lineEdit->setText(strTown);
}



void QCW_LargeWidget::on_btnAddressOrg2Pers_clicked()
{
	//issue 2885: set new button
	int nContactID=0;
	ui.framePersonOrganization->GetCurrentEntityRecord(nContactID);
	if (nContactID>0)
	{
		//issue 2274: when organization is selected then ask user if wanna copy address data from org to current address row...
		int nResult=QMessageBox::question(NULL,tr("Copy Address"),tr("Copy organization address to the contact address?"),tr("Yes"),tr("No"));
		if (nResult==0) 
		{
			DbRecordSet lstOfId;
			lstOfId.addColumn(QVariant::Int,"BCNT_ID");
			lstOfId.addRow();
			lstOfId.setData(0,0,nContactID);
			DbRecordSet lstAddressOrg;
			Status Ret_pStatus;
			g_pBoSet->app->BusContact->ReadContactAddress(Ret_pStatus, lstOfId, lstAddressOrg);
			_CHK_ERR(Ret_pStatus);
			if (lstAddressOrg.getRowCount()>0)
			{
				lstAddressOrg.find("BCMA_IS_DEFAULT",1);
				int nRow=lstAddressOrg.getSelectedRow();
				if (nRow<0)
					nRow=0;
				ui.address_widget->SetAddress(lstAddressOrg.getRow(nRow));
			}
		}
	}

}


void QCW_LargeWidget::CheckDebtor()
{
	//if customer code and debtor code is null and insert mode, then ignore data
	if (m_lstDebtor.getRowCount()>0)
	{
		if (m_lstDebtor.getDataRef(0,"BCMD_DEBTORCODE").toString().isEmpty() && m_lstDebtor.getDataRef(0,"BCMD_CUSTOMERCODE").toString().isEmpty() && m_nEntityRecordID==0)
		{
			m_lstDebtor.clear();
		}
	}

	if (m_lstDebtor.getRowCount()>0)
	{
		m_recFullContactData.setData(0,"LST_DEBTOR",m_lstDebtor);
	}
}


void QCW_LargeWidget::on_btnDebtorCode_clicked()
{

	Status err;
	QString strCode;
	_SERVER_CALL(BusContact->GetMaximumDebtorCode(err,strCode))
	_CHK_ERR(err);

	bool bOK;
	int nCode=strCode.toInt(&bOK);
	if (!bOK)
	{
		nCode=0;
	}

	if (m_lstDebtor.getRowCount()==0)
	{
		m_lstDebtor.addRow();
	}

	nCode++;
	ui.txtDebtorCode->setText(QVariant(nCode).toString());
	
}