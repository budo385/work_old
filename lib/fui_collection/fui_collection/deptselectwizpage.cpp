#include "deptselectwizpage.h"
#include "wizardregistration.h"
#include "common/common/entity_id_collection.h"
#include <QHash>
#include <QTreeWidget>
#include <QHeaderView>
#include <QTreeWidgetItem>
#include "gui_core/gui_core/universaltreewidget.h"
#include "gui_core/gui_core/macros.h"

//GLOBAL:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;		

DeptSelectWizPage::DeptSelectWizPage(int nWizardPageID, QString strPageTitle)
    : WizardPage(nWizardPageID, strPageTitle)
{
}

DeptSelectWizPage::~DeptSelectWizPage()
{
}

void DeptSelectWizPage::Initialize()
{
	//If already initialized return.
	if (m_bInitialized)
		return;

	m_bInitialized = true;

	ui.setupUi(this);

	//m_pParent->setWindowTitle(tr("Department selection"));
	//m_nWizardPageID = WIZARD_DEPARTMENT_SELECTION_PAGE;


	//Connect some signals.
	connect(ui.SelectedDepartments_treeWidget, SIGNAL(ItemNumberChanged()), this, SLOT(CompleteChanged()));

	//Set recordset 
	Status err;
	_SERVER_CALL(MainEntitySelector->ReadData(err, ENTITY_BUS_DEPARTMENT, DbRecordSet(), m_recDeptRecordSet))
	_CHK_ERR(err);

	//Read from cache, if there is something.
	ReadFromCache();

	//m_recResultRecordSet.Dump();
	//If cache recordset empty copy definition from bus department.
	if (m_recResultRecordSet.getRowCount() == 0)
		m_recResultRecordSet.copyDefinition(m_recDeptRecordSet);

	ui.DepartmentList_treeWidget->Initialize(&m_recDeptRecordSet, false, true, false, true);
	ui.SelectedDepartments_treeWidget->Initialize(&m_recResultRecordSet, true, false, false, true);

	//Set tree widgets look and feel.
	ui.DepartmentList_treeWidget->SetHeader();
	ui.SelectedDepartments_treeWidget->SetHeader();
	ui.DepartmentList_treeWidget->SetDropType(m_nWizardPageID);
	ui.SelectedDepartments_treeWidget->AddAcceptableDropTypes(m_nWizardPageID);
}

void DeptSelectWizPage::AddSelectedDepartments()
{
	//Get selected items.
	DbRecordSet RecordSet;
	ui.DepartmentList_treeWidget->GetDropValue(RecordSet);

	ui.SelectedDepartments_treeWidget->DropHandler(QModelIndex(), 0, RecordSet,NULL);
}

void DeptSelectWizPage::on_AddSelected_pushButton_clicked()
{
	AddSelectedDepartments();
}

void DeptSelectWizPage::on_AddAll_pushButton_clicked()
{
	ui.DepartmentList_treeWidget->selectAll();
	AddSelectedDepartments();
	ui.DepartmentList_treeWidget->clearSelection();
}

void DeptSelectWizPage::on_RemoveSelected_pushButton_clicked()
{
	QList<int> selectedRows = m_recResultRecordSet.getSelectedRows();

	if (!selectedRows.count())
		return;

	//Find row ID's.
	QList<int> selectedRowID;
	QListIterator<int> iter(selectedRows);
	while (iter.hasNext())
	{
		int row   = iter.next();
		int RowID = m_recResultRecordSet.getDataRef(row, 0).toInt();
		selectedRowID << RowID;
	}

	//Delete by row ID's.
	QListIterator<int> rowIter(selectedRowID);
	while (rowIter.hasNext())
	{
		int RowID_toDelete = rowIter.next();
		int Row_toDelete = m_recResultRecordSet.find(0, RowID_toDelete, true);
		if (Row_toDelete != -1)
			m_recResultRecordSet.deleteRow(Row_toDelete);
	}
	
	ui.SelectedDepartments_treeWidget->RefreshDisplay();
}

void DeptSelectWizPage::on_RemoveAll_pushButton_clicked()
{
	ui.SelectedDepartments_treeWidget->selectAll();
	on_RemoveSelected_pushButton_clicked();
}

void DeptSelectWizPage::CompleteChanged()
{
	if (ui.SelectedDepartments_treeWidget->model()->rowCount() > 0)
		m_bComplete = true;
	else	
		m_bComplete = false;

	completeStateChanged();
}
