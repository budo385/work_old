#include "options_docdefaults.h"
#include "communicationmanager.h"
#include <QFileDialog>
#include "common/common/datahelper.h"
#include "bus_client/bus_client/documenthelper.h"
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
extern QString g_strLastDir_FileOpen;


Options_DocDefaults::Options_DocDefaults(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
	:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

	//setup path buttons:
	ui.btnSelect->setIcon(QIcon(":SAP_Select.png"));
	ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnSelect->setToolTip(tr("Select Path"));
	ui.btnRemove->setToolTip(tr("Remove Path"));

	connect(ui.btnSelect,SIGNAL(clicked()),this,SLOT(OnSelectPath()));
	connect(ui.btnRemove,SIGNAL(clicked()),this,SLOT(OnRemovePath()));


	//read settings, set on window:

	//TYPE:
	switch(GetSettingValue(APP_DEFAULT_DOCUMENT_TYPE).toInt())
	{
	case 0:
		ui.rb0->setChecked(true);
		break;
	case 1:
		ui.rb1->setChecked(true);
		break;
	case 2:
		ui.rb2->setChecked(true);
		break;
	}


	//if internet disabled return local:
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_INTERNET, nValue))
	{
		ui.rb2->setEnabled(false);
	}

	//else return internet:
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_LOCAL, nValue))
	{
		ui.rb1->setEnabled(false);
	}


	QString strPath = GetSettingValue(APP_DEFAULT_DOCUMENT_DIR).toString();
	if (strPath.isEmpty())
		strPath = DocumentHelper::GetDefaultDocumentDirectory();
	
	ui.txtPath->setText(strPath);
	
	//if empty set to new, set dirty flag
	if (GetSettingValue(APP_DEFAULT_DOCUMENT_DIR).toString().isEmpty())
	{
		SetSettingValue(APP_DEFAULT_DOCUMENT_DIR, strPath);
	}

	//name:
	int nNameFormat=GetSettingValue(APP_DEFAULT_DOCUMENT_NAME).toInt();
	if (nNameFormat & 0x01)
		ui.ckbName->setChecked(true);
	if (nNameFormat & 0x02)
		ui.ckbDate->setChecked(true);
	if (nNameFormat & 0x04)
		ui.ckbLoggedUser->setChecked(true);

	ui.ckbParseType->setChecked(GetSettingValue(APP_PERSON_DOCUMENT_TYPE_PARSING_ON).toBool());
	
}

Options_DocDefaults::~Options_DocDefaults()
{

}

DbRecordSet Options_DocDefaults::GetChangedValuesRecordSet()
{
	//save:
	int nDocTpye=0;
	if (ui.rb1->isChecked())
		nDocTpye=1;

	if (ui.rb2->isChecked())
		nDocTpye=2;

	//only set if different:
	if (nDocTpye!=GetSettingValue(APP_DEFAULT_DOCUMENT_TYPE).toInt())
		SetSettingValue(APP_DEFAULT_DOCUMENT_TYPE, nDocTpye);

	QString strPath=ui.txtPath->toPlainText();

	strPath.replace("\\","/");
	if(strPath.lastIndexOf("\\")==strPath.length()-1)
		strPath.chop(1);

	if (strPath!=GetSettingValue(APP_DEFAULT_DOCUMENT_DIR).toString())
		SetSettingValue(APP_DEFAULT_DOCUMENT_DIR, strPath);


	//name format:
	int nNameFormat=0;
	if (ui.ckbName->isChecked())
		nNameFormat =nNameFormat | 0x01;
	if (ui.ckbDate->isChecked())
		nNameFormat =nNameFormat | 0x02;
	if (ui.ckbLoggedUser->isChecked())
		nNameFormat =nNameFormat | 0x04;

	if (nNameFormat!=GetSettingValue(APP_DEFAULT_DOCUMENT_NAME).toInt())
		SetSettingValue(APP_DEFAULT_DOCUMENT_NAME, nNameFormat);

	if (ui.ckbParseType->isChecked()!=GetSettingValue(APP_PERSON_DOCUMENT_TYPE_PARSING_ON).toBool())
		SetSettingValue(APP_PERSON_DOCUMENT_TYPE_PARSING_ON, ui.ckbParseType->isChecked());


	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

void Options_DocDefaults::OnSelectPath()
{
	QString strStartDir=ui.txtPath->toPlainText();


	if (strStartDir.isEmpty())
	{
		strStartDir=DocumentHelper::GetDefaultDocumentDirectory();
	}
	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetMyDocumentsDir();
	}





	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Default Directory For Temporary Documents"),
		strStartDir);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath->setText(strFile);
	}
}

void Options_DocDefaults::OnRemovePath()
{
	ui.txtPath->clear();
}
