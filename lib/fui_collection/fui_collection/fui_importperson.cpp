#include "fui_importperson.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "common/common/cliententitycache.h"
#include "gui_core/gui_core/gui_helper.h"
#include "common/common/csvimportfile.h"
#include "common/common/datahelper.h"
#include <QFileDialog>
#include <QProgressDialog>
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#ifndef min
#define min(a,b) (((a)<(b))? (a):(b))
#endif

//build list modes
#define BLM1_REPLACE_EXISTING	0
#define BLM1_UPDATE_EXISTING	1
#define BLM1_IGNORE_EXISTING	2
#define BLM1_SKIP_EXISTING		3
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
extern ClientEntityCache g_ClientCache;				//global client cache
extern QString g_strLastDir_FileOpen;


FUI_ImportPerson::FUI_ImportPerson(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	ui.cboImportSource->addItem(tr("Text File"));

	ui.radSkipExisting->setChecked(true);
	ui.radIgnoreExisting->hide(); //MB remove this mode (deleting it would hurt the code)

	ui.frameOrganization->Initialize(ENTITY_BUS_ORGANIZATION);
	ui.frameOrganization->SetSelectionButtonLabel(tr("Organization:"));

	//define data record
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON_IMPORT));

	ui.tableContacts->Initialize(&m_lstData);

	//define column headers
	DbRecordSet columns;
	ui.tableContacts->AddColumnToSetup(columns,"BPER_FIRST_NAME",tr("First Name"),120,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BPER_LAST_NAME",tr("Last Name"),120,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BORG_CODE",tr("Organization"),120,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BDEPT_CODE",tr("Department"),120,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BPER_CODE",tr("Employee Code"),120,false, "");
	ui.tableContacts->SetColumnSetup(columns);

	GUI_Helper::SetStyledButtonColorBkg(ui.btnBuildList, GUI_Helper::BTN_COLOR_YELLOW);
	GUI_Helper::SetStyledButtonColorBkg(ui.btnImportContacts, GUI_Helper::BTN_COLOR_YELLOW);

	//---------------------------------------
	//Init Role Combo.
	//---------------------------------------
	//Check what version is client.
	QString strEdition = g_pClientManager->GetIniFile()->m_strModuleCode;
	Status status;
	DbRecordSet recRoles;

	//!???.......this should be inside cache inside some global AR manager...u fucking noob...

	if("SC-TE" == strEdition)
		_SERVER_CALL(AccessRights->GetTERoles(status, recRoles))
	else
		_SERVER_CALL(AccessRights->GetBERoles(status, recRoles))
	//_CHK_ERR(status);
	
	//Fill role combo.
	ui.cboRoles->addItem("", -1);	//empty role
	int nRowCount = recRoles.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QString strRoleName = recRoles.getDataRef(i, "CROL_NAME").toString();
		int nRoleID = recRoles.getDataRef(i, "CROL_ID").toInt();
		ui.cboRoles->addItem(strRoleName, nRoleID);
		
		//Check is this role administrator role and store it for later - in personal edition all users are administrators.
		//if (recRoles.getDataRef(i, "CROL_CODE").toInt() == ADMINISTRATOR_ROLE)
		//	m_nAdminRoleID = nRoleID;
	}
}

FUI_ImportPerson::~FUI_ImportPerson()
{
}

void FUI_ImportPerson::on_btnBuildList_clicked()
{
	//
	// build person list from text file
	//
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                            g_strLastDir_FileOpen,
                                            tr("Import File (*.txt)"));
	if (!fileName.isEmpty())
	{
		QFileInfo fileInfo(fileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		//QString strFormat = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Users - Subformat: User Details";
		DbRecordSet data;
		Status status;
		CsvImportFile import;
	
		QStringList lstFormatPlain;
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Users - Subformat: User Details";
		QStringList lstFormatUtf8;
		
		import.Load(status, fileName, data, lstFormatPlain, lstFormatUtf8);
		if(!status.IsOK()){
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}
		int nFieldsCnt = data.getColumnCount();
		if(18 != nFieldsCnt){
			QMessageBox::information(this, "Error", tr("Document does not contain %1 fields for each record!").arg(18));
			return;
		}
		//data.Dump();

		//merge results with destination list
		int nCnt = data.getRowCount();
		for(int i=0; i<nCnt; i++)
		{
			m_lstData.addRow();
			int nRow = m_lstData.getRowCount() - 1;
			
			int nCol = 0;
			m_lstData.setData(nRow, "BPER_CODE",		        data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "GENERATE_CONTACT",         data.getDataRef(i, nCol++).toInt());
			m_lstData.setData(nRow, "BPER_CODE_EXT",            data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_FIRST_NAME",          data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_LAST_NAME",           data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_INITIALS",			data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BORG_CODE",                data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BDEPT_CODE",				data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_DATE_ENTERED",        DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BPER_DATE_LEFT",           DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BPER_ACTIVE_FLAG",         data.getDataRef(i, nCol++).toInt());
			m_lstData.setData(nRow, "BPER_SEX",					data.getDataRef(i, nCol++).toInt());
			m_lstData.setData(nRow, "BPER_DIRECT_PHONE",        data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_INSURANCE_ID",        data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_BIRTH_DATE",			DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BPER_OCCUPATION",          data.getDataRef(i, nCol++).toDouble());
			m_lstData.setData(nRow, "CLC_FUNCTION_CODE",		data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_DESCRIPTION",         data.getDataRef(i, nCol++).toString());
		}

	#ifdef _DEBUG
		//m_lstData.Dump();
	#endif
		m_lstData.selectAll();
		ui.tableContacts->RefreshDisplay();
		UpdateImportContactsButton();
	}
}

void FUI_ImportPerson::on_btnImportContacts_clicked()
{
	//get selected role
	int nRoleID = -1;
	int nRoleIdx = ui.cboRoles->currentIndex();
	if(nRoleIdx > 0){
		nRoleID = ui.cboRoles->itemData(nRoleIdx).toInt();
	}

	//update build list mode
	int nOperation = 0;
	if(ui.radReplaceExisting->isChecked())
		nOperation = BLM1_REPLACE_EXISTING;
	else if(ui.radUpdateExisting->isChecked())
		nOperation = BLM1_UPDATE_EXISTING;
	else if(ui.radIgnoreExisting->isChecked())
		nOperation = BLM1_IGNORE_EXISTING;
	else
		nOperation = BLM1_SKIP_EXISTING;

	//extract selected rows (under new view format)
	DbRecordSet lstSelected;
	lstSelected.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON_IMPORT));
	lstSelected.merge(m_lstData, true);

	int nRowsToImport = lstSelected.getRowCount();
	if(nRowsToImport < 1){
		QMessageBox::information(this, "", tr("No items have been selected for import!"));
		return;
	}

	//get org id:
	int nOrgIDForced = -1;
	DbRecordSet lstActOrg;
	ui.frameOrganization->GetCurrentEntityRecord(nOrgIDForced, lstActOrg);
	if(nOrgIDForced >= 0)
	{
		//lstActOrg.Dump();
		QString strOrgCodeForced = lstActOrg.getDataRef(0, "BORG_CODE").toString();
		if(!strOrgCodeForced.isEmpty()){
			int nColIdx = lstSelected.getColumnIdx("BORG_CODE");
			lstSelected.setColValue(nColIdx, strOrgCodeForced);
		}
	}

	//process data if custom role selected, eventum #2697:
	//"If a role is selected, every imported user should automatically get Login rights. The username has to be set to BPER_INITIALS from the import file. If the username is not unique, ad a running number to it (MB, MB1, MB2,..). If (!) the user has no role, he get's the selected one."

	//
	// import process
	//
	int nSize = lstSelected.getRowCount();

	QSize size(300,100);
	QProgressDialog progress(tr("Importing Users..."),tr("Cancel"),0,nSize+30,this);
	progress.setWindowTitle(tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	nSize = lstSelected.getRowCount();	//fix size (some rows were merged)
	progress.setMinimumSize(size);

	//update progress
	progress.setValue(10);
	qApp->processEvents();
	if (progress.wasCanceled())
		return;

	Status status;
	DbRecordSet lstWork;
	lstWork.copyDefinition(lstSelected);

	const int nChunkSize = 50;

	int nFailuresCnt = 0;
	int	nStart = -1;
	int nRowProgress = 0;

	int nCount = 0;
	while (1)
	{
		//prepare chunk
		lstWork.destroy();
		lstWork.copyDefinition(lstSelected);

		int nMax = min(nCount+nChunkSize, nSize);
		for(int i=nCount; i<nMax; i++){
			if(!lstWork.addRow()) break;
			DbRecordSet row = lstSelected.getRow(i);
			lstWork.assignRow(lstWork.getRowCount()-1, row);
		}
		if(lstWork.getRowCount() < 1)
			break;

		_SERVER_CALL(BusImport->ImportPersons(status, lstWork, nOperation, false, false, nRoleID))
		if(!status.IsOK()){
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}

		int nWorkSize = lstWork.getRowCount();
		//for(int i=0; i<nWorkSize; i++)
			//if(lstWork.getDataRef(i, "STATUS_CODE").toInt() > 0)
		//		nFailuresCnt++;

		nCount = nMax;

		//update progress
		progress.setValue(nCount);
		qApp->processEvents();
		if (progress.wasCanceled())
			return;
	}

	ui.tableContacts->RefreshDisplay();

	progress.setValue(nCount+15);
	qApp->processEvents();

	//notify observers: B.T. changed 18.03.2013: purge cache
	if (lstSelected.getRowCount()>0) //notify cache:
	{
		g_ClientCache.PurgeCache(ENTITY_BUS_PERSON);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_PERSON);
	}

	QMessageBox::information(this, "", tr("Done!"));
}

void FUI_ImportPerson::UpdateImportContactsButton()
{
	int nCnt = ui.tableContacts->GetDataSource()->getRowCount(); //
	ui.btnImportContacts->setEnabled((nCnt > 0));
}
