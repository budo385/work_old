#ifndef TABLE_CONTACTADDRESS_H
#define TABLE_CONTACTADDRESS_H

#include "gui_core/gui_core/universaltablewidgetex.h"
#include "table_contactaddresstypes.h"
#include "toolbar_edit.h"
#include <QCheckBox>
#include <QTextEdit>
#include "common/common/status.h"

class Table_ContactAddress : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_ContactAddress(QWidget *parent);
	void Initialize(DbRecordSet *plstData,DbRecordSet *pContactRecord,QTextEdit *pTxtAddress,Table_ContactAddressTypes *tableTypes,toolbar_edit *pToolBar,QCheckBox *syncPers,QCheckBox *snycOrg);
	void SetEditMode(bool bEdit=true);
	void AddRow(QString strTextAddressFormat="");
	void CheckDataBeforeWrite(Status &Ret_pStatus);

signals:
	void ContactDataChanged();
	void SignalDblClick();
	void SignalDblClickOver();

public slots:
	void RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);
	void OnContactChanged();
	void OpenDetailsFromContextMenu();

private slots:
	void SetDefault();
	void InsertRow();
	void OnCellDoubleClicked(int nRow,int nCol);
	void OnRowInserted(int nRow);
	void OnDeleted();
	void OnSelectionChanged();

protected:
	virtual QMimeData * mimeData ( const QList<QTableWidgetItem *> items ) const;

private:
	void BuildToolBar(toolbar_edit* pToolBar);
	void GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip);
	void CreateContextMenu();
	void OpenDetails(int nRow);
	void RefreshDefaultFlag();

	QTextEdit *m_pTxtAddress;
	Table_ContactAddressTypes *m_pTableTypes;
	toolbar_edit *m_toolbar;
	QCheckBox *m_syncPers;
	QCheckBox *m_syncOrg;

	int m_nDefaultColIdx;
	int m_nAdrTxtAddressIdx;	
	int m_nAdrIDColIdx;
	DbRecordSet *m_plstContact;
};



#endif // TABLE_CONTACTADDRESS_H
