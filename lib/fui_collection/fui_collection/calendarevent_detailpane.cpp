#include "calendarevent_detailpane.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/macros.h"
#include "common/common/datahelper.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "fui_collection/fui_collection/fui_calendar.h"
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;	

CalendarEvent_DetailPane::CalendarEvent_DetailPane(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ui.splitter->setStyleSheet("QSplitter::handle "+ThemeManager::GetSidebar_FooterHandle());

	
	m_lstDataCalEvent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
	m_lstDataCalEventPart.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));
	m_lstDataCalEventOption.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_OPTIONS_SELECT)); //+ BREAKS
	m_lstDataCalEventBreaks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_BREAKS));
	//m_lstDataCalEventPartTaskData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	
	m_GuiFieldManagerOption =new GuiFieldManager(&m_lstDataCalEventOption);
	m_GuiFieldManagerPart=new GuiFieldManager(&m_lstDataCalEventPart);

	ui.dateFrom->useAsDateTimeEdit(true);
	ui.dateTo->useAsDateTimeEdit(true);

	connect(ui.dateFrom,SIGNAL(focusOutSignal()),this,SLOT(OnDateTimeFromChanged()));
	connect(ui.dateTo,SIGNAL(focusOutSignal()),this,SLOT(OnDateTimeToChanged()));

	m_GuiFieldManagerOption->RegisterField("BCOL_LOCATION",ui.txtLocation);
	m_GuiFieldManagerOption->RegisterField("BCOL_DESCRIPTION",ui.frameEditor);
	m_GuiFieldManagerOption->RegisterField("BCOL_SUBJECT",ui.txtSubject);
	m_GuiFieldManagerOption->RegisterField("BCOL_FROM",ui.dateFrom);
	m_GuiFieldManagerOption->RegisterField("BCOL_TO",ui.dateTo);
	//m_GuiFieldManagerOption->RegisterField("BCOL_IS_ACTIVE",ui.ckbActive);


	//SAPNE's:
	ui.frameTemplate->Initialize(ENTITY_CALENDAR_EVENT_TEMPLATES,&m_lstDataCalEvent,"BCEV_TEMPLATE_ID");
	//ui.frameTemplate->GetButton(Selection_SAPNE::BUTTON_MODIFY)->setVisible(false);
	//ui.frameTemplate->GetButton(Selection_SAPNE::BUTTON_ADD)->setVisible(false);
	ui.frameTemplate->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setVisible(false);
	//ui.frameTemplate->GetButton(Selection_SAPNE::BUTTON_VIEW)->setVisible(false);
	ui.frameTemplate->registerObserver(this);

	ui.frameType->Initialize( ENTITY_CE_TYPES, &m_lstDataCalEventPart,"CENT_CE_TYPE_ID");
	ui.frameType->GetSelectionController()->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);
	ui.frameOwner->Initialize( ENTITY_BUS_PERSON,&m_lstDataCalEventPart, "CENT_OWNER_ID");

	ui.frameCategory->Initialize(ENTITY_CALENDAR_EVENT_TEMPLATE_CATEGORY,&m_lstDataCalEvent,"BCEV_CATEGORY",false);
	if(!g_AccessRight->TestAR(MODIFY_CALENDAR_CATEGORY))
		ui.frameCategory->DisableEntryText();

	ui.frameShowTimeAs->Initialize(ENTITY_BUS_CM_TYPES); //,&m_lstDataCalEventPart,"BCEP_PRESENCE_TYPE_ID",false);
	ui.frameShowTimeAs->GetSelectionController()->GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_CALENDAR_PRESENCE);
	ui.frameShowTimeAs->GetSelectionController()->ReloadData(); //activate cache
	if(!g_AccessRight->TestAR(MODIFY_CALENDAR_SHOW_TIME_AS))
		ui.frameShowTimeAs->DisableEntryText();

	//state:
	ui.cmbStates->addItem(tr("Fixed"),GlobalConstants::TYPE_CAL_FIXED);
	ui.cmbStates->addItem(tr("Preliminary"),GlobalConstants::TYPE_CAL_PRELIMINARY);
	ui.cmbStates->addItem(tr("Over"),GlobalConstants::TYPE_CAL_OVER);
	ui.cmbStates->addItem(tr("Cancelled"),GlobalConstants::TYPE_CAL_CANCELLED);
	ui.cmbStates->setCurrentIndex(0);

	//------------------NMRXs:
	
	QList<int> lst;
	lst<<NMRXManager::PAIR_CALENDAR_PERSON<<NMRXManager::PAIR_CALENDAR_CONTACT;
	lst<<NMRXManager::PAIR_CALENDAR_CONTACT;
	ui.widgetNMRXPerson->Initialize(tr("Assigned Contacts"),CE_COMM_ENTITY,lst,BUS_CM_CONTACT,BUS_PERSON,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART,-1,-1,BUS_CAL_INVITE);
	ui.widgetNMRXPerson->DisableRoleSwitch();
	ui.widgetNMRXPerson->SetReverseMode(false);
	ui.widgetNMRXPerson->SetLazyWriteMode();
	ui.widgetNMRXPerson->SetForbidDuplicateAssignments();
	ui.widgetNMRXPerson->SetUsePersonAssignAsContact();
	connect(ui.widgetNMRXPerson,SIGNAL(SendInvite(DbRecordSet)),this,SLOT(OnSendInvite(DbRecordSet)));
	connect(ui.widgetNMRXPerson,SIGNAL(InviteDataChanged()),this,SIGNAL(SignalInviteDataChanged()));
	connect(ui.widgetNMRXPerson,SIGNAL(OpenCalendar(int,DbRecordSet,int,DbRecordSet)),this,SLOT(OnOpenCalendar_Person(int,DbRecordSet,int,DbRecordSet)));
	
	lst.clear();
	lst<<NMRXManager::PAIR_CALENDAR_PROJECT;
	ui.widgetNMRXProject->Initialize(tr("Assigned Projects"),CE_COMM_ENTITY,lst,BUS_PROJECT,-1,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);
	ui.widgetNMRXProject->DisableRoleSwitch();
	ui.widgetNMRXProject->SetReverseMode(false);
	ui.widgetNMRXProject->SetLazyWriteMode();
	ui.widgetNMRXProject->SetForbidDuplicateAssignments();
	connect(ui.widgetNMRXProject,SIGNAL(OpenCalendar(int,DbRecordSet,int,DbRecordSet)),this,SLOT(OnOpenCalendar_Project(int,DbRecordSet,int,DbRecordSet)));

	lst.clear();
	lst<<NMRXManager::PAIR_CALENDAR_RESOURCE;
	ui.widgetNMRXResources->Initialize(tr("Reserved Resources"),CE_COMM_ENTITY,lst,BUS_CAL_RESOURCE,-1,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);
	ui.widgetNMRXResources->DisableRoleSwitch();
	ui.widgetNMRXResources->SetReverseMode(false);
	ui.widgetNMRXResources->SetLazyWriteMode();
	ui.widgetNMRXResources->SetForbidDuplicateAssignments();
	connect(ui.widgetNMRXResources,SIGNAL(OpenCalendar(int,DbRecordSet,int,DbRecordSet)),this,SLOT(OnOpenCalendar_Resources(int,DbRecordSet,int,DbRecordSet)));

	connect(ui.widgetNMRXPerson,SIGNAL(ContentChanged()),this,SLOT(OnNMRXPersonChanged()));
	connect(ui.widgetNMRXProject,SIGNAL(ContentChanged()),this,SLOT(OnNMRXProjectsChanged()));
	connect(ui.widgetNMRXResources,SIGNAL(ContentChanged()),this,SLOT(OnNMRXResourcesChanged()));


	//------------------breaks:
	ui.tableBreaks->Initialize(&m_lstDataCalEventBreaks,ui.frameBreakToolbar);
	ui.tableBreaks->SetEditMode(true);
	connect(ui.tableBreaks,SIGNAL(SignalRowInserted(int)),this,SLOT(OnBreakRowInserted(int)));
	connect(ui.tableBreaks,SIGNAL(SignalContentChanged()),this,SLOT(OnBreakDataChanged()));
	


	//------------------Option selector:
	m_lstDataCalEventOption.addRow();
	ui.frameRowSelector->Initialize(&m_lstDataCalEventOption,"Proposal:");
	connect(ui.frameRowSelector,SIGNAL(RowAdded(int,int)),this,SLOT(OnOptionRowAdded(int,int)));
	connect(ui.frameRowSelector,SIGNAL(RowDeleted(int)),this,SLOT(OnOptionRowDeleted(int)));
	connect(ui.frameRowSelector,SIGNAL(RowChanged(int,int)),this,SLOT(OnOptionRowChanged(int,int)));
	connect(ui.frameRowSelector,SIGNAL(Accepted(int)),this,SLOT(OnOptionAccepted(int)));

	//test:
	//m_lstDataCalEventPartTaskData.addRow();
	//ui.frameTask->m_pRecMain = &m_lstDataCalEventPartTaskData;
	//ui.frameTask->SetEnabled(true);


	connect(ui.frameEditor,SIGNAL(on_VerticalOpenClose_clicked(bool)),this,SLOT(OnEditor_VerticalOpenClose_clicked(bool)));
	ui.frameEditor->ShowVerticalStretchButton(true);
	ui.frameEditor->SetInverseVerticalStretchButton();

	ui.tabWidget->setCurrentIndex(3);

	//RefreshDisplay();
	
}

CalendarEvent_DetailPane::~CalendarEvent_DetailPane()
{


}


//option, part, main must contain at least 1 row
void CalendarEvent_DetailPane::SetData(DbRecordSet &lstDataCalEvent,DbRecordSet &lstDataCalEventPart,DbRecordSet &lstDataCalEventOption, DbRecordSet &lstNmrxPersonContact, DbRecordSet &lstNmrxProjects,DbRecordSet &lstNmrxResources)
{
	//lstDataCalEvent.Dump();
	//m_lstDataCalEvent.Dump();

	m_lstDataCalEvent=lstDataCalEvent;
	m_lstDataCalEventPart=lstDataCalEventPart;
	m_lstDataCalEventOption=lstDataCalEventOption;

	Q_ASSERT(m_lstDataCalEvent.getRowCount()==1);
	Q_ASSERT(m_lstDataCalEventPart.getRowCount()==1);
	Q_ASSERT(m_lstDataCalEventOption.getRowCount()>=1);

	QString strName=tr("Calendar Event");
	if (lstDataCalEvent.getRowCount()>0)
		strName=lstDataCalEvent.getDataRef(0,"BCEV_TITLE").toString();

	int nCentID=GetPartCommEntityID();
	ui.widgetNMRXPerson->Reload(strName,nCentID,true,&lstNmrxPersonContact);
	ui.widgetNMRXProject->Reload(strName,nCentID,true,&lstNmrxProjects);
	ui.widgetNMRXResources->Reload(strName,nCentID,true,&lstNmrxResources);

	//m_lstDataCalEventPartTaskData=lstTask;

	//show time as:
	if (lstDataCalEventPart.getRowCount()>0)
		ui.frameShowTimeAs->SetCurrentEntityRecord(lstDataCalEventPart.getDataRef(0, "BCEP_PRESENCE_TYPE_ID").toInt());
	else
		ui.frameShowTimeAs->Clear();

	//state:
	ui.cmbStates->setCurrentIndex(lstDataCalEventPart.getDataRef(0, "BCEP_STATUS").toInt());

	//option (try to keep current row):
	int nCurrentRow=ui.frameRowSelector->m_nCurrentRow;
	if (nCurrentRow<0 || nCurrentRow>=m_lstDataCalEventOption.getRowCount())
		nCurrentRow=0;

	m_GuiFieldManagerOption->SetCurrentRow(nCurrentRow);
	ui.frameRowSelector->Reset();
	ui.frameRowSelector->m_nCurrentRow=nCurrentRow;
	ui.frameRowSelector->RefreshLabel();
	LoadBreaksForOptionRow(nCurrentRow);

	if (lstDataCalEvent.getDataRef(0,"BCEV_TEMPLATE_FLAG")==1)
	{
		ui.frameCategory->setEnabled(true);
		ui.ckbNewCRP->setVisible(true);
	}
	else
	{
		ui.frameCategory->setEnabled(false);
		ui.ckbNewCRP->setVisible(false);
	}

	//m_lstDataCalEvent.Dump();
	RefreshDisplay();
}
void CalendarEvent_DetailPane::GetData(DbRecordSet &lstDataCalEvent,DbRecordSet &lstDataCalEventPart,DbRecordSet &lstDataCalEventOption, DbRecordSet &lstNmrxPersonContact, DbRecordSet &lstNmrxProjects,DbRecordSet &lstNmrxResources)
{
	//show time as:
	QVariant intNull(QVariant::Int);
	int nIDShowTimeAs;
	DbRecordSet rowShowTmeAs;
	if(ui.frameShowTimeAs->GetCurrentEntityRecord(nIDShowTimeAs,rowShowTmeAs))
		m_lstDataCalEventPart.setData(0, "BCEP_PRESENCE_TYPE_ID", nIDShowTimeAs);
	else
		m_lstDataCalEventPart.setData(0, "BCEP_PRESENCE_TYPE_ID", intNull);

	//nmrx:
	ui.widgetNMRXPerson->GetData(lstNmrxPersonContact);
	ui.widgetNMRXProject->GetData(lstNmrxProjects);
	ui.widgetNMRXResources->GetData(lstNmrxResources);

	//option & breaks:
	int nCurrentOptionRow=ui.frameRowSelector->m_nCurrentRow;
	SaveBreaksForOptionRow(ui.frameRowSelector->m_nCurrentRow);

	//task:
	//ui.frameTask->GetDataFromGUI();
	//lstTask=m_lstDataCalEventPartTaskData;

	//state:
	m_lstDataCalEventPart.setData(0,"BCEP_STATUS",ui.cmbStates->currentIndex());
	m_lstDataCalEventPart.setData(0,"BCEP_ASSIGN_NEW_CRP",(int)ui.ckbNewCRP->isChecked());

	lstDataCalEvent=m_lstDataCalEvent;
	lstDataCalEventPart=m_lstDataCalEventPart;
	lstDataCalEventOption=m_lstDataCalEventOption;

	

	//qDebug()<<(int)ui.ckbNewCRP->isChecked();
	//m_lstDataCalEventPart.Dump();
}
void CalendarEvent_DetailPane::SetEditMode(bool bEdit)
{
	ui.frameType->SetEditMode(bEdit);
	ui.frameTemplate->SetEditMode(bEdit);
	ui.frameCategory->SetEditMode(bEdit);
	ui.frameOwner->SetEditMode(bEdit);
	ui.frameShowTimeAs->SetEditMode(bEdit);
	ui.tableBreaks->SetEditMode(bEdit);
	ui.cmbStates->setEnabled(bEdit);
	m_GuiFieldManagerOption->SetEditMode(bEdit);
	m_GuiFieldManagerPart->SetEditMode(bEdit);
	ui.frameRowSelector->SetEditMode(bEdit);

	ui.widgetNMRXPerson->SetEditMode(bEdit);
	ui.widgetNMRXProject->SetEditMode(bEdit);
	ui.widgetNMRXResources->SetEditMode(bEdit);
}

void CalendarEvent_DetailPane::RefreshDisplay()
{
	ui.frameType->RefreshDisplay();
	ui.frameTemplate->RefreshDisplay();
	ui.frameCategory->RefreshDisplay();
	ui.frameOwner->RefreshDisplay();
	ui.frameShowTimeAs->RefreshDisplay();
	m_GuiFieldManagerOption->RefreshDisplay();
	m_GuiFieldManagerPart->RefreshDisplay();
	ui.tableBreaks->RefreshDisplay();
	OnBreakDataChanged();
	if (m_lstDataCalEvent.getRowCount()>0)
		ShowCkbNewCRP(m_lstDataCalEvent.getDataRef(0,"BCEV_TEMPLATE_FLAG").toBool());
}


void CalendarEvent_DetailPane::OnOptionRowAdded(int nPrevRow,int nCurrRow)
{
	if (nPrevRow>=0) //copy from previous
	{
		//m_lstDataCalEventOption.getRow(nPrevRow).Dump();
		DbRecordSet row = m_lstDataCalEventOption.getRow(nPrevRow);
		m_lstDataCalEventOption.assignRow(nCurrRow,row);
		m_lstDataCalEventOption.setData(nCurrRow,"BCOL_ID",QVariant(QVariant::Int)); //reset ID

		//if (m_lstDataCalEventOption.getDataRef(nPrevRow,"BCOL_IS_ACTIVE").toInt()==1)
		//	m_lstDataCalEventOption.setData(nCurrRow,"BCOL_IS_ACTIVE",0);
	}
}
void CalendarEvent_DetailPane::OnOptionRowDeleted(int nRow)
{
	//ok!
	Q_ASSERT(m_lstDataCalEventOption.getRowCount()!=0);
	m_lstDataCalEventBreaks.clear();
	ui.tableBreaks->RefreshDisplay();
}
void CalendarEvent_DetailPane::OnOptionRowChanged(int nPrevRow,int nCurrRow)
{
	//m_lstDataCalEventOption.Dump();

	m_GuiFieldManagerOption->SetCurrentRow(nCurrRow);
	m_GuiFieldManagerOption->RefreshDisplay();
	SaveBreaksForOptionRow(nPrevRow);
	LoadBreaksForOptionRow(nCurrRow);
}

void CalendarEvent_DetailPane::OnOptionAccepted(int nRow)
{
	//�ta sad?
	if (m_lstDataCalEventOption.getRowCount()<=1)
		return;

	QString strMsg =tr("Do you wish to accept this proposal and to discard all others (they will be deleted)?");
	int nResult=QMessageBox::question(this,tr("Warning"),strMsg,tr("Yes"),tr("No"));
	if (nResult!=0)
		return;

	//delete all others, reset counters:

	m_lstDataCalEventOption.selectAll();
	m_lstDataCalEventOption.selectRow(nRow,false);
	m_lstDataCalEventOption.deleteSelectedRows();
	m_GuiFieldManagerOption->SetCurrentRow(0);
	m_GuiFieldManagerOption->RefreshDisplay();

	ui.frameRowSelector->Reset();
	ui.frameRowSelector->m_nCurrentRow=0;
	ui.frameRowSelector->RefreshLabel();
	LoadBreaksForOptionRow(0);
}


void CalendarEvent_DetailPane::OnEditor_VerticalOpenClose_clicked(bool bShow)
{
	ui.tabWidget->setVisible(bShow);
}


int CalendarEvent_DetailPane::GetPartCommEntityID()
{
	int nCentID=0;
	if (m_lstDataCalEventPart.getRowCount()>0)
		nCentID=m_lstDataCalEventPart.getDataRef(0,"CENT_ID").toInt();
	return nCentID;
}
int CalendarEvent_DetailPane::GetPartID()
{
	int nPartID=0;
	if (m_lstDataCalEventPart.getRowCount()>0)
		nPartID=m_lstDataCalEventPart.getDataRef(0,"BCEP_ID").toInt();
	return nPartID;
}
int CalendarEvent_DetailPane::GetMainEventID()
{
	int nCalID=0;
	if (m_lstDataCalEvent.getRowCount()>0)
		nCalID=m_lstDataCalEvent.getDataRef(0,"BCEV_ID").toInt();
	return nCalID;
}


void CalendarEvent_DetailPane::SaveBreaksForOptionRow(int nOptionRow)
{
	if (nOptionRow<0 || nOptionRow>=m_lstDataCalEventOption.getRowCount())
		return;
	
	ui.tableBreaks->StoreCustomWidgetChanges();
	
	//m_lstDataCalEventBreaks.Dump();

	DbRecordSet lstBreaks=m_lstDataCalEventBreaks;
	if (lstBreaks.getColumnIdx("DURATION")!=-1)
		lstBreaks.removeColumn(lstBreaks.getColumnIdx("DURATION"));

	//lstBreaks.Dump();
	m_lstDataCalEventOption.setData(nOptionRow,"BREAK_LIST",lstBreaks);
}
void CalendarEvent_DetailPane::LoadBreaksForOptionRow(int nOptionRow)
{
	if (nOptionRow<0 || nOptionRow>=m_lstDataCalEventOption.getRowCount())
		return;

	m_lstDataCalEventBreaks=m_lstDataCalEventOption.getDataRef(nOptionRow,"BREAK_LIST").value<DbRecordSet>();
	if (m_lstDataCalEventBreaks.getColumnCount()==0)
		m_lstDataCalEventBreaks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_BREAKS));

	ui.tableBreaks->RefreshDisplay();
}

void CalendarEvent_DetailPane::OnNMRXPersonChanged()
{
	int nContacts=ui.widgetNMRXPerson->GetRowCountForAssignment(BUS_CM_CONTACT);
	int nPersons=ui.widgetNMRXPerson->GetRowCountForAssignment(BUS_PERSON);

	QString strTabTitle="";
	if (nContacts)
		strTabTitle+=tr("Contacts (")+QVariant(nContacts).toString()+")";
	else
		strTabTitle+=tr("Contacts");
	strTabTitle+=tr(" And ");
	if (nPersons)
		strTabTitle+=tr("Users (")+QVariant(nPersons).toString()+")";
	else
		strTabTitle+=tr("Users");

	ui.tabWidget->setTabText(0,strTabTitle);

}
void CalendarEvent_DetailPane::OnNMRXProjectsChanged()
{
	QString strTabTitle;
	int nProjects=ui.widgetNMRXProject->GetRowCountForAssignment(BUS_PROJECT);
	if (nProjects)
		strTabTitle+=tr("Projects (")+QVariant(nProjects).toString()+")";
	else
		strTabTitle+=tr("Projects");
	ui.tabWidget->setTabText(1,strTabTitle);

}
void CalendarEvent_DetailPane::OnNMRXResourcesChanged()
{
	QString strTabTitle;
	int nProjects=ui.widgetNMRXResources->GetRowCountForAssignment(BUS_CAL_RESOURCE);
	if (nProjects)
		strTabTitle+=tr("Resources (")+QVariant(nProjects).toString()+")";
	else
		strTabTitle+=tr("Resources");
	ui.tabWidget->setTabText(2,strTabTitle);
}

void CalendarEvent_DetailPane::AssignContacts(QList<int> &lst)
{
	ui.widgetNMRXPerson->AssignData(BUS_CM_CONTACT,lst,tr("assigned"));
}
void CalendarEvent_DetailPane::AssignUsers(QList<int> &lst)
{
	ui.widgetNMRXPerson->AssignData(BUS_PERSON,lst,tr("assigned"));
}
void CalendarEvent_DetailPane::AssignProject(QList<int> &lst)
{
	ui.widgetNMRXProject->AssignData(BUS_PROJECT,lst,tr("assigned"));
}
void CalendarEvent_DetailPane::AssignResources(QList<int> &lst)
{
	ui.widgetNMRXResources->AssignData(BUS_CAL_RESOURCE,lst,tr("assigned"));
}

int	CalendarEvent_DetailPane::GetCurrentOptionRow()
{
	return ui.frameRowSelector->m_nCurrentRow;
}


void CalendarEvent_DetailPane::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==ui.frameTemplate && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		emit SignalTemplateChanged(nMsgDetail);
	}
}

void CalendarEvent_DetailPane::OnSendInvite(DbRecordSet lstInvite)
{
	//event_id:
	int nEventID=m_lstDataCalEvent.getDataRef(0,"BCEV_ID").toInt();

	lstInvite.setColValue("BCEP_ID",m_lstDataCalEventPart.getDataRef(0,"BCEP_ID").toInt());
	lstInvite.setColValue("BCEP_COMM_ENTITY_ID",m_lstDataCalEventPart.getDataRef(0,"BCEP_COMM_ENTITY_ID").toInt());
	lstInvite.setColValue("CENT_OWNER_ID",m_lstDataCalEventPart.getDataRef(0,"CENT_OWNER_ID"));

	//->server call rows, event_id
	Status err;
	_SERVER_CALL(BusCalendar->SendInviteBySokrates(err,nEventID,lstInvite));
	_CHK_ERR(err);

}

void CalendarEvent_DetailPane::OnBreakRowInserted(int nRow) //by MB: every new break= from= option/break from
{
	int nOptionRow=GetCurrentOptionRow();
	if(nOptionRow>=0 && nOptionRow<m_lstDataCalEventOption.getRowCount())
	{
		QDateTime pFrom=m_lstDataCalEventOption.getDataRef(nOptionRow,"BCOL_FROM").toDateTime();
		QDateTime pTo=m_lstDataCalEventOption.getDataRef(nOptionRow,"BCOL_TO").toDateTime();
		if (pFrom.isValid() && pTo.isValid())
		{
			int nSeconds=pFrom.secsTo(pTo)/2;
			if(nSeconds>7.5*60)
			{
				nSeconds=nSeconds-7.5*60;
				pFrom=pFrom.addSecs(nSeconds);
				pFrom.setTime(QTime(pFrom.time().hour(),pFrom.time().minute()));
			}
			m_lstDataCalEventBreaks.setData(nRow,"BCBL_FROM",pFrom);
			m_lstDataCalEventBreaks.setData(nRow,"BCBL_TO",pFrom.addSecs(60*15));
		}
		else
		{
			m_lstDataCalEventBreaks.setData(nRow,"BCBL_FROM",QDateTime::currentDateTime());
			m_lstDataCalEventBreaks.setData(nRow,"BCBL_TO",QDateTime::currentDateTime().addSecs(60*15));
		}
	}
}

void CalendarEvent_DetailPane::SetCurrentOptionDates(QDateTime from,QDateTime to,bool bOverRide)
{
	int nOptionRow=GetCurrentOptionRow();
	if(nOptionRow>=0 && nOptionRow<m_lstDataCalEventOption.getRowCount())
	{
		QDateTime pFrom=m_lstDataCalEventOption.getDataRef(nOptionRow,"BCOL_FROM").toDateTime();
		QDateTime pTo=m_lstDataCalEventOption.getDataRef(nOptionRow,"BCOL_TO").toDateTime();

		if ((!pFrom.isValid() && !bOverRide) || bOverRide)
		{
			m_lstDataCalEventOption.setData(nOptionRow,"BCOL_FROM",from);
			ui.dateFrom->blockSignals(true);
			ui.dateFrom->setDateTime(from);
			ui.dateFrom->blockSignals(false);
		}
		if ((!pTo.isValid() && !bOverRide) || bOverRide)
		{
			m_lstDataCalEventOption.setData(nOptionRow,"BCOL_TO",to);
			ui.dateTo->blockSignals(true);
			ui.dateTo->setDateTime(to);
			ui.dateTo->blockSignals(false);
		}
	}

}



void CalendarEvent_DetailPane::OnOpenCalendar_Person(int,DbRecordSet lstContacts,int,DbRecordSet lstPersons)
{
	QDate from; 
	QDate to;

	DataHelper::GetCurrentWeek(m_lstDataCalEvent.getDataRef(0,"BCEV_FROM").toDateTime().date(),from, to);
	
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		//lstContacts.Dump();
		pFui->Initialize(from,to,NULL,NULL,&lstContacts);
	}
}
void CalendarEvent_DetailPane::OnOpenCalendar_Project(int,DbRecordSet lstProject,int,DbRecordSet)
{
	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(m_lstDataCalEvent.getDataRef(0,"BCEV_FROM").toDateTime().date(),from, to);
	
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->Initialize(from,to,NULL,&lstProject);
	}


}
void CalendarEvent_DetailPane::OnOpenCalendar_Resources(int,DbRecordSet lstRes,int,DbRecordSet)
{
	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(m_lstDataCalEvent.getDataRef(0,"BCEV_FROM").toDateTime().date(),from, to);
	
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->Initialize(from,to,NULL,NULL,NULL,&lstRes);
	}


}


void CalendarEvent_DetailPane::RefreshTitle(QString strTitle)
{
	ui.widgetNMRXPerson->SetMasterEntityCalcualtedName(strTitle);
	ui.widgetNMRXProject->SetMasterEntityCalcualtedName(strTitle);
	ui.widgetNMRXResources->SetMasterEntityCalcualtedName(strTitle);
}

void CalendarEvent_DetailPane::OnBreakDataChanged()
{
	QString strTabTitle;
	int nCnt=ui.tableBreaks->GetDataSource()->getRowCount();
	if (nCnt>0)
		strTabTitle+=tr("Breaks (")+QVariant(nCnt).toString()+")";
	else
		strTabTitle+=tr("Breaks");
	ui.tabWidget->setTabText(4,strTabTitle);

}

void CalendarEvent_DetailPane::ShowCkbNewCRP(bool bIsTemplateActivated)
{
	if(m_lstDataCalEventPart.getRowCount()==0) 
		return;

	ui.ckbNewCRP->setVisible(bIsTemplateActivated);
	if (bIsTemplateActivated)
	{
		ui.ckbNewCRP->setChecked(m_lstDataCalEventPart.getDataRef(0,"BCEP_ASSIGN_NEW_CRP").toBool());
		ui.frameCategory->setEnabled(true);
	}
	else
		ui.frameCategory->setEnabled(false);
}

void CalendarEvent_DetailPane::OnDateTimeFromChanged()
{
	QDateTime datEventFrom=ui.dateFrom->dateTime();
	emit DateTimeFromChanged(datEventFrom);
}
void CalendarEvent_DetailPane::OnDateTimeToChanged()
{
	QDateTime datEventTo=ui.dateTo->dateTime();
	emit DateTimeToChanged(datEventTo);
}

void CalendarEvent_DetailPane::SetOptionRow(int nOptionID)
{
	int nCurrentRow=m_lstDataCalEventOption.find("BCOL_ID",nOptionID,true);
	if (nCurrentRow>=0)
	{
		m_GuiFieldManagerOption->SetCurrentRow(nCurrentRow);
		ui.frameRowSelector->Reset();
		ui.frameRowSelector->m_nCurrentRow=nCurrentRow;
		ui.frameRowSelector->RefreshLabel();
		LoadBreaksForOptionRow(nCurrentRow);
		RefreshDisplay();
	}

}