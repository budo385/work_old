#include "assignedprojectsdlg.h"
#include "common/common/entity_id_collection.h"

AssignedProjectsDlg::AssignedProjectsDlg(DbRecordSet lstDataNMRXProjects, QWidget *parent, int nProjectCurrentID)
	: QDialog(parent)
{
	ui.setupUi(this);

	QApplication::setOverrideCursor(Qt::WaitCursor);

	//this can take a while
	ui.frmAssignedProjects->Initialize(ENTITY_BUS_PROJECT);	//init selection controller

	//NMRX Projects TABLE:
	*(ui.frmAssignedProjects->GetDataSource())=lstDataNMRXProjects; //copy list to table
	ui.frmAssignedProjects->RefreshDisplay();

	//select initial project
	if(nProjectCurrentID >= 0)
		ui.frmAssignedProjects->SetCurrentEntityRecord(nProjectCurrentID, false);

	QApplication::restoreOverrideCursor();
}

AssignedProjectsDlg::~AssignedProjectsDlg()
{
}

void AssignedProjectsDlg::on_btnOK_clicked()
{
	done(1);
}

void AssignedProjectsDlg::on_btnCancel_clicked()
{
	done(0);
}

int AssignedProjectsDlg::GetSelectedProject()
{
	int nProjectCurrentID = -1;
	QString strCode, strName;
	DbRecordSet lstEntityRecord;
	ui.frmAssignedProjects->GetSelection(nProjectCurrentID, strCode, strName, lstEntityRecord);

	return nProjectCurrentID;
}
