#include "fui_dm_userpaths.h"
#include "common/common/entity_id_collection.h"
#include <QFileDialog>

//globals:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;					
extern QString g_strLastDir_FileOpen;

//logger user:

 //global cache for user defualts


FUI_DM_UserPaths::FUI_DM_UserPaths(QWidget *parent)
: FuiBase(parent)
{

	//-------------------------
	//		INIT SIMPLE FUI
	//-------------------------
	ui.setupUi(this);	
	InitFui(ENTITY_BUS_DM_USER_PATHS,BUS_DM_USER_PATHS,NULL,ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);

	
	//setup path buttons:
	ui.btnSelect->setIcon(QIcon(":SAP_Select.png"));
	ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnSelect->setToolTip(tr("Select Path"));
	ui.btnRemove->setToolTip(tr("Remove Path"));
	connect(ui.btnSelect,SIGNAL(clicked()),this,SLOT(OnSelectPath()));
	connect(ui.btnRemove,SIGNAL(clicked()),this,SLOT(OnRemovePath()));


	//bring up field manager:
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData);
	m_GuiFieldManagerMain->RegisterField("BDMU_PRIORITY",ui.ckbPriority);
	m_GuiFieldManagerMain->RegisterField("BDMU_APP_PATH",ui.txtPath);

	//enable to use plain text:
	GuiField_TextEdit* pText=dynamic_cast<GuiField_TextEdit*>(m_GuiFieldManagerMain->GetFieldHandler("BDMU_APP_PATH"));
	if(pText!=NULL)pText->UsePlainText();


	ui.frameOwner->Initialize(ENTITY_BUS_PERSON);
	ui.frameApplication->Initialize( ENTITY_BUS_DM_APPLICATIONS,&m_lstData, "BDMU_APPLICATION_ID");

	//define list:
	m_lstUserPaths.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_USER_PATHS_SELECT));
	
	
	//---------------------------------------- INIT TABLE--------------------------------//
	ui.tableWidget->Initialize(&m_lstUserPaths);
	connect(ui.tableWidget,SIGNAL(SignalSelectionChanged()),this,SLOT(OnTableSelectionChanged()));
	DbRecordSet columns;
	ui.tableWidget->AddColumnToSetup(columns,"BDMA_NAME",tr("Application"),120,false);
	ui.tableWidget->AddColumnToSetup(columns,"BDMU_APP_PATH",tr("Path"),300,false);
	ui.tableWidget->SetColumnSetup(columns);
	
	//---------------------------------------- INIT CNTX MENU TABLE--------------------------------//
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;

	pAction = new QAction(tr("Delete Selected Records From Database"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDelete()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	//Select All:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), ui.tableWidget, SLOT(SortDialog()));
	lstActions.append(pAction);
	ui.tableWidget->SetContextMenuActions(lstActions);


	//reload applications from cache/server: to be safe:
	ui.frameApplication->GetSelectionController()->ReloadData();

	//---------------------------------------- INIT SAPNE ON LOGGED--------------------------------//

	//set edit to false:
	SetMode(MODE_EMPTY);


	//connect sapne on change:
	ui.frameOwner->registerObserver(this);

	//load data for logged user:
	int nLoggedID=g_pClientManager->GetPersonID();
	if (nLoggedID!=0)
		ui.frameOwner->SetCurrentEntityRecord(nLoggedID,true);
	OnOwnerChanged();

}



FUI_DM_UserPaths::~FUI_DM_UserPaths()
{
	delete m_GuiFieldManagerMain;
}

QString FUI_DM_UserPaths::GetFUIName()
{
	return tr("User Application Paths");
}


void FUI_DM_UserPaths::RefreshDisplay()
{
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frameOwner->RefreshDisplay();
	ui.frameApplication->RefreshDisplay();
}


void FUI_DM_UserPaths::SetMode(int nMode)
{

	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		m_GuiFieldManagerMain->SetEditMode(false);
		ui.btnSelect->setEnabled(false);
		ui.btnRemove->setEnabled(false);
		ui.tableWidget->setEnabled(true);
		ui.frameOwner->SetEditMode(true);
		ui.frameApplication->SetEditMode(false);
	}
	else
	{
		m_GuiFieldManagerMain->SetEditMode(true);
		ui.btnSelect->setEnabled(true);
		ui.btnRemove->setEnabled(true);
		ui.tableWidget->setEnabled(false);
		ui.frameOwner->SetEditMode(false);
		ui.frameApplication->SetEditMode(true);
		ui.frameApplication->setFocus(Qt::OtherFocusReason);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}

void FUI_DM_UserPaths::OnSelectPath()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.*";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Choose application"),
		strStartDir,
		strFilter);

	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath->setText(strFile);
	}

}

void FUI_DM_UserPaths::OnRemovePath()
{
	ui.txtPath->clear();
}


void FUI_DM_UserPaths::OnTableSelectionChanged()
{
	//load FUI data or clear:
	int nRow=m_lstUserPaths.getSelectedRow();
	if (nRow==-1) return;
	int nUserPathID=m_lstUserPaths.getDataRef(nRow,0).toInt();
	on_selectionChange(nUserPathID);
}


void FUI_DM_UserPaths::OnOwnerChanged()
{
	//load table, clear FUI:
	DbRecordSet data;
	int nPersonID;
	Status err;
	bool bSelected=ui.frameOwner->GetCurrentEntityRecord(nPersonID,data);
	if (bSelected)
	{
		_SERVER_CALL(BusDocuments->ReadUserPaths(err,nPersonID,m_lstUserPaths))
		//error:
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		//m_lstUserPaths.Dump();

		ui.tableWidget->RefreshDisplay();
		
		if (!ui.btnButtonBar->isEnabled())
			ui.btnButtonBar->setEnabled(true);

	}
	else
	{
		m_lstUserPaths.clear();
		ui.tableWidget->RefreshDisplay();
		ui.btnButtonBar->setEnabled(false);	//block whole window
	}

	on_selectionChange(0); //invalidate FUI
}


//user wants to delete from grid:
void FUI_DM_UserPaths::OnDelete()
{
	DbRecordSet lstForDelete;
	lstForDelete.addColumn(QVariant::Int,"BDMU_ID");
	lstForDelete.merge(m_lstUserPaths,true);
	if (lstForDelete.getRowCount()>0)
	{
		int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete the selected records {")+QVariant(lstForDelete.getRowCount()).toString()+tr("} from the database?"),tr("Yes"),tr("No"));
		if (nResult!=0) return; //only if YES


		Status err;
		QString pLockResourceID;
		DbRecordSet lstStatusRows;
		bool pBoolTransaction = true;
		_SERVER_CALL(ClientSimpleORM->Delete(err,BUS_DM_USER_PATHS,lstForDelete, pLockResourceID, lstStatusRows, pBoolTransaction))
		//error:
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		else
		{
			m_lstUserPaths.deleteSelectedRows();
			ui.tableWidget->RefreshDisplay();
			on_selectionChange(0);//invalidate FUI
		}
	}
}



void FUI_DM_UserPaths::DataWrite(Status &err)
{
	//copy Owner ID=
	DbRecordSet data;
	int nPersonID;
	bool bSelected=ui.frameOwner->GetCurrentEntityRecord(nPersonID,data);
	Q_ASSERT(bSelected);
	m_lstData.setColValue(m_lstData.getColumnIdx("BDMU_OWNER_ID"),nPersonID);

	Q_ASSERT(m_lstData.getRowCount()>0); //must have at least 1 row

	//check values: path+application id must be set
	if(m_lstData.getDataRef(0,"BDMU_APP_PATH").toString().isEmpty())
		{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,tr("Application path is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"BDMU_APPLICATION_ID").toInt()==0)
		{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,tr("Application is mandatory field!"));	return;	}

	m_lstData.getDataRef(0,"BDMU_APP_PATH").toString().replace("\\","/"); //save /


	FuiBase::DataWrite(err);
	if (err.IsOK())
	{
		_SERVER_CALL(BusDocuments->ReadUserPaths(err,nPersonID,m_lstUserPaths))
		//error:
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		if(m_lstData.getRowCount()>0)
			m_lstUserPaths.find(0,m_lstData.getDataRef(0,0).toInt());
		ui.tableWidget->RefreshDisplay();
	}
}




//delete from grid:
void FUI_DM_UserPaths::DataDelete(Status &err)
{
	FuiBase::DataDelete(err);
	if (err.IsOK())
	{
		int nRow=m_lstUserPaths.find(0,m_nEntityRecordID,true);
		if (nRow!=-1)
		{
			m_lstUserPaths.deleteRow(nRow);
			ui.tableWidget->RefreshDisplay();
		}
	}
}

void FUI_DM_UserPaths::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject == ui.frameOwner && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnOwnerChanged();
		return;
	}

	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);
}