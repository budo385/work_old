
#ifndef FUI_IMPORTEMAIL_H
#define FUI_IMPORTEMAIL_H

#include <QWidget>
#include "generatedfiles/ui_fui_importemail.h"
#include "fuibase.h"
#include <QProgressDialog>
#include "os_specific/os_specific/mapimanager.h"

class FUI_ImportEmail : public FuiBase
{
    Q_OBJECT

public:
    FUI_ImportEmail(QWidget *parent = 0);
    ~FUI_ImportEmail();

	QString GetFUIName(){ return tr("Import Emails"); }
	void SetDefaults(QString strDefaultContactEmail);
	void BuildImportListFromDrop(QByteArray &byteDrop, bool bAreTemplates=false, int nDefaultProjectID=-1);
	void OnGridFilledRecalculate(QSet<QString> &setEmails, int nDefaultProjectID=-1);

#ifdef _WIN32
	static void ReadEmails(Status status, QList<QByteArray> &lstEntryIDs, DbRecordSet &lstData, bool bAreTemplates = false, int nDefaultProjectID = -1, QProgressDialog *progress = NULL, bool bOnlyKnownContacts=false, bool bOnlyOutgoingEmails=false, CMAPIEx *pMapi = NULL);
#endif //#ifdef _WIN32

	static void ImportEmails(Status status, DbRecordSet &lstData, int &nSkippedOrReplacedCnt, bool bAddDupsAsNewRow, bool bSkipExisting = false, bool bSkipProgress=false);
	static void ClearInternalCache();
	static void FilterEmailsByContact(Status status, DbRecordSet &lstData, bool bSelectOnly = false);

	static bool IsDropEmail(QByteArray &byteDrop);
	static bool IsDropContact(QByteArray &byteDrop);
	static bool IsDropAppointment(QByteArray &byteDrop);
	bool FilterMatchRow(int nIdx);

	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

private:
	void EnableFilterWidgets(bool bEnable);
	
	void UpdateRowEmailContact(int nIdx);

	static QStringList ExtractEmails(QString &strSrc);
	static void UpdateContactsList(QStringList &lstSrc, int nRole, DbRecordSet &lstContactLinks, QString &strUnassignedLocal);
	static void FilterAttachmentRecords(DbRecordSet &lstAttachments);

	static QMap<QString, int> m_mapEmail2Contact;	//cache to convert email address to contact id

private:
    Ui::FUI_ImportEmailClass ui;

	DbRecordSet m_lstData;
	DbRecordSet m_lstProj;
	QList<QStringList> m_lstFolders;
	QList<QStringList> m_lstFoldersThunder;
	
	//filter data
	bool	m_bUseFilter;
	bool	m_bORMode;
	QDate	m_dateFilterFromDate;
	QDate	m_dateFilterToDate;
	QString	m_strFilterFromField;
	QString	m_strFilterToField;
	QString	m_strFilterCCField;
	QString	m_strFilterBCCField;
	QString	m_strFilterSubjectField;

private slots:
	void on_btnOpenEmail_clicked();
	void on_btnAssignProject_clicked();
	void on_btnImportData_clicked();
	void on_radUseFilter_toggled(bool);
	void on_radNoFilter_toggled(bool);
	void on_btnSelectSourceFolders_clicked();
	void on_btnBuildList_clicked();
	void OnTableDataSelectionChanged();
	void OnContactChanged();
	void OnContactSAPNEInsert();
	void OnProjectChanged();
	void OnSignalRefreshBoldRows();
	void on_cboImportSource_currentIndexChanged(int);
	void OnCloseWindow();
};

#endif // FUI_IMPORTEMAIL_H

