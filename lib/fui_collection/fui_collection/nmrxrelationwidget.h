#ifndef NMRXRELATIONWIDGET_H
#define NMRXRELATIONWIDGET_H

#include <QWidget>
#include <QString>
#include <QHBoxLayout>
#include "common/common/dbrecordset.h"
#include "table_nmrxrelation.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "selection_contactbutton_fui.h"
#include "common/common/status.h"


class NMRXRelationWidget : public QWidget
{
	Q_OBJECT

public:
    NMRXRelationWidget(QWidget *parent);
    ~NMRXRelationWidget();

	void Initialize(QString strTitle,int nMasterTableID,QList<int>lstPairs,int nAssigmentTableID_1,int nAssigmentTableID_2=-1,int nMasterTableTypeID_1=-1,int nTableTypeID_1=-1,int nTableTypeID_2=-1, int nX_TableID=-1);
	void SetReverseMode(bool bReverse){m_bReverse=bReverse;} //MB fucked up NMRX by reverse, set this flag to enable 'old' system
	void Reload(QString strMasterEntityCalcualtedName,int nMasterTableRecordID,bool bSkipRead=false,DbRecordSet *newRecord=NULL); //set mastertable_id=0 for special insert mode
	void WriteChangedRecords(Status &err,int nMasterTableRecordID); //only for lazy write emode
	void GetData(DbRecordSet &lstData); //only for lazy write emode
	void Invalidate();
	void DisableRoleSwitch(){m_bDisableRoleSwitch=true;}
	void SetLazyWriteMode(){m_bSkipWriteMode=true;}
	DbRecordSet GetFilterForRead(int m_nMasterTableRecordID);
	int GetRowCountForAssignment(int nTableID);
	void AssignData(int nAssigmentTableID,QList<int> &lstIDForAssign, QString strDefaultRoleName="");
	void GetSelectedData(int nAssigmentTableID,DbRecordSet &lstData, bool bAllRecords=false /* not only selected */);
	void SetEditMode(bool bEdit);
	void SetForbidDuplicateAssignments(){m_bForbidDuplicateAssignments=true;}
	void SetMasterEntityCalcualtedName(QString strName){m_strMasterEntityCalcualtedName=strName;}
	void SetUsePersonAssignAsContact(){m_bSetUsePersonAssignAsContact=true;}

	static bool TestListForDuplicateAssignments(int nAssigmentTableID,DbRecordSet &lstNMRXNewData, bool bShowMsgWarn=false);
	static bool GetInviteRecordsForNewInvite(DbRecordSet &lstNMRXData,DbRecordSet &RetNmrxDataForInvite, bool bIgnoreSelection=true);
	static void CreateAssignmentData(DbRecordSet &lstNMRXData,int nMasterTableID,int nAssigmentTableID,QList<int> &lstIDForAssign, QString strDefaultRoleName="",int nMasterTableRecordID=0,int nX_TableID=-1);

signals:
	void ContentChanged();
	void OpenCalendar(int,DbRecordSet,int,DbRecordSet);
	void SendInvite(DbRecordSet lstInvite);
	void InviteDataChanged();

private slots:
	void OnAssign1(int nDefaultID=-1);
	void OnAssign2(int nDefaultID=-1);
	void OnShow();
	void OnEdit();
	void OnDelete();
	void OnDeleteAll();
	void OnShowDetails();
	void OnShowCalendar();
	void OnSelectionChanged();
	void On_NewDataDropped(int,DbRecordSet);
	void OnInviteSend();
	

private:
    
	void RefreshNames(int nRow=-1);
	void EnableToolbar(bool bEnable);
	void BuildCacheLoadTable();
	//bool TestListForDuplicateAssignmentsPriv(int nAssigmentTableID,DbRecordSet &lstNMRXNewData, bool bShowMsgWarn=false);

	bool m_bForbidDuplicateAssignments;
	QString m_strMasterEntityCalcualtedName;
	int m_nMasterTableRecordID;
	int m_nMasterTableID,m_nAssigmentTableID_1,m_nAssigmentTableID_2;
	//QString m_strHtmlTag;
	//QString m_strEndHtmlTag;
	DbRecordSet m_lstData;
	Table_NMRXRelation *m_pTable;
	QHBoxLayout *m_toolbar;
	DbRecordSet m_lstPairs;
	MainEntitySelectionController m_EntityAssigment1;
	MainEntitySelectionController m_EntityAssigment2;
	Selection_ContactButton_FUI *m_pieMenu_toolButton;			//contact pie

	int m_nTable1Idx;
	int m_nTable2Idx;
	int m_nKey1Idx;
	int m_nKey2Idx;
	int m_nNameIdx_1;
	int m_nNameIdx_2;
	int m_nMasterTableTypeID_1;
	int m_nTableTypeID_1;
	int m_nTableTypeID_2;
	bool m_bSkipWriteMode;
	bool m_bDisableRoleSwitch;
	int m_nX_TableID;
	bool m_bEdit;
	bool m_bSetUsePersonAssignAsContact;
	bool m_bReverse;
	bool m_bReadOnlyMode;

	QPushButton *m_pBtnInviteSend;

};

#endif // NMRXRELATIONWIDGET_H
