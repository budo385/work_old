#ifndef CALENDAR_MENUWIDGET_H
#define CALENDAR_MENUWIDGET_H

#include <QWidget>
#include "generatedfiles/ui_calendar_menuwidget.h"

#include <QDragEnterEvent>
#include <QDropEvent>
#include <QAction>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QMenu>
#include "common/common/observer_ptrn.h"
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"

class Calendar_MenuWidget : public QWidget, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Calendar_MenuWidget(QWidget *parent = 0,bool bIgnoreViewMode=false);
	~Calendar_MenuWidget();

	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );

	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void SetDefaults(DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL, DbRecordSet *lstResources=NULL,QDateTime from=QDateTime(), QDateTime to=QDateTime());
	void SetScheduleFlag(){m_bScheduleTask=true;}
	void SetCloseOnSend();

	void DisableHide(){ui.btnHide->setVisible(false);};
	bool IsHidden();
	void SetHidden(bool bHide=true);
	void SetSideBarLayout();
public slots:
	void OnMenuButton();

signals:
	void NeedNewData(int nSubMenuType);		//emited always when new document is about to be created...to assign doc to contacts/projects later

private slots:
	
	void OnHideTab();

	//template:
	void OnNewFromTemplate();
	void OnNewTask();
	void OnAddTemplate();
	void OnModifyTemplate();
	void OnRemoveTemplate();
	void OnCategoryChanged(const QString strCategory);

	void ReLoadTemplates(){LoadTemplates(true);}

protected:
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent * event);

private:
	Ui::Calendar_MenuWidgetClass ui;

	void OnThemeChanged();
	void LoadTemplates(bool bReload=false);
	void LoadCategory(bool bReload=false);

	QTreeWidgetItem* SearchTree(QTreeWidget* pTree, QVariant nUserValue);

	DbRecordSet m_lstTemplates;
	DbRecordSet m_lstContacts; 
	DbRecordSet m_lstProjects;
	DbRecordSet m_lstResources;
	QDateTime m_datFrom;
	QDateTime m_datTo;
	int m_nLoggedID;

	bool m_bScheduleTask;
	MainEntitySelectionController m_Category;	
	QPoint m_dragPosition;
	bool m_bCloseOnSend;
	bool m_bCreateQuickTask;


};





#endif // CALENDAR_MENUWIDGET_H
