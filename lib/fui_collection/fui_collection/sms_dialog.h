#ifndef SMS_DIALOG_H
#define SMS_DIALOG_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_sms_dialog.h"
#include "os_specific/os_specific/skype/skypeinterface.h"
#include "bus_client/bus_client/accuserrecordselector.h"

class SMS_Dialog : public QDialog, public SkypeInterface
{
	Q_OBJECT

public:
	SMS_Dialog(QWidget *parent = 0);
	~SMS_Dialog();

	void SetNumber(QString &strNumber);
	void SetContact(int nID){ m_nContactID = nID; }
	void UpdateStats();

	bool SendSMS();

	//SKYPE INTERFACE:
	void OnSMSCreated(QString &strID){on_SMS_Created(strID);};
	
public:
	AccUserRecordSelector *uarWidget;

protected slots:
	void on_btnCancel_clicked();
	void on_btnSend_clicked();
	void on_btnSaveAndSend_clicked();
	void on_SMS_textChanged();
	void on_btnPickPhone_clicked();

private:
	Ui::SMS_DialogClass ui;
	QString m_strSMSID;
	int m_nContactID;

	void on_SMS_Created(QString &strID);
	void on_SMS_Failed(QString &strID, QString &strMsg);
	void on_SMS_Sent(QString &strID, QString &strMsg);

};

#endif // SMS_DIALOG_H
