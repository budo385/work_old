#ifndef DOCUMENT_PIEMENU_H
#define DOCUMENT_PIEMENU_H

#include <QObject>
#include "gui_core/gui_core/pie_menu/qtpiemenu.h"
#include "common/common/dbrecordset.h"


/*!
	\class  Document_PieMenu
	\brief  Start point to create new document: pop's up pie menu on pWidgetSource (center)
	\ingroup FUI_Collection
*/

class Document_PieMenu : public QObject
{
	Q_OBJECT

public:
	Document_PieMenu();
	~Document_PieMenu();


	void SelectDocumentType(QWidget *pWidgetSource, int nOperation);
	void CreateNewDocument(QWidget *pWidgetSource,DbRecordSet *rowDefault=NULL,DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL, bool bScheduleTask=false, bool bIsTemplate=false, DbRecordSet *recRevisionForInsert=NULL);
	void SetCloseOnOpenDocument(QWidget *parent){m_parent=parent;}

signals:
	void DocumentTypeSelected(int, int);

private slots:
	//PIE menu:
	void OnLocalFile();
	void OnInternetFile();
	void OnNote();
	void OnAddress();
	void OnPaperDoc();


private:
	QtPieMenu *m_pPieMenu;

	void OpenFUI(int nDocType);

	DbRecordSet m_rowDefault,m_lstProjects,m_lstContacts,m_recRevisionForInsert;
	bool m_bScheduleTask,m_bIsTemplate;

	int m_nOperation;
	bool m_bSelectionMode;
	QWidget *m_parent;



};

#endif // DOCUMENT_PIEMENU_H
