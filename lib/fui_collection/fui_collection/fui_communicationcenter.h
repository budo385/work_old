#ifndef FUI_COMMUNICATIONCENTER_H
#define FUI_COMMUNICATIONCENTER_H

#include <QWidget>
#include "generatedfiles/ui_fui_communicationcenter.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"


class FUI_CommunicationCenter : public FuiBase
{
    Q_OBJECT

public:
    FUI_CommunicationCenter(QWidget *parent = 0);
    ~FUI_CommunicationCenter();

	//void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	QString GetFUIName();
	void	SetCurrentTab(int nTabPane);
	void	DesktopStartupInitialization(DbRecordSet &recCurrentView);
	CommunicationMenu_Base* GetComMenu(){return m_p_CeMenu;};
	

public slots:
	void OnCEMenu_NeedNewData(int);
	void OnViewChanged(DbRecordSet &recCurrentView);

private slots:
	//void on_ComEntityGrid_executed(int nEntityID, int nEntitySysType, int nDocID);
	void on_PButton_clicked();
	void on_groupBoxCommName_clicked();
	void on_filterChanged(QString strFilterName);


private:
    Ui::FUI_CommunicationCenterClass ui;
	QString m_strTitle;
	CommunicationMenu_Base* m_p_CeMenu;


};

#endif // FUI_COMMUNICATIONCENTER_H
