#include "reportselectionwizpage.h"
#include "wizardregistration.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/wizardbase.h"
#include <QHash>
#include <QTreeWidget>
#include <QHeaderView>
#include <QTreeWidgetItem>
#include "gui_core/gui_core/universaltreewidget.h"

#ifndef WINCE
	#include "report_definitions/report_definitions/reportregistration.h"
#endif

ReportSelectionWizPage::ReportSelectionWizPage(int nWizardPageID, QString strPageTitle, QWidget *parent)
    : WizardPage(nWizardPageID, strPageTitle)
{
	m_ReportsRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_RPT_TABLE_VIEW));
	m_recResultRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_RPT_TABLE_VIEW));
}

ReportSelectionWizPage::~ReportSelectionWizPage()
{
}

void ReportSelectionWizPage::Initialize()
{
	if (m_bInitialized)
		return;
	ui.setupUi(this);
	m_bInitialized = true;

	//Connect selected reports and complete state changed.
	connect(ui.ReportList_treeWidget, SIGNAL(SignalSelectionChanged()), this, SLOT(CompleteChanged()));
	connect(ui.ReportList_treeWidget, SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)), this, SLOT(OnDblClick()));

	//The name of class says enough.
	BuildReportList();

	ui.ReportList_treeWidget->Initialize(&m_ReportsRecordSet, false, true, true, true);
	ui.ReportList_treeWidget->RefreshDisplay();
	m_recResultRecordSet.clear();
	//ui.SelectedReports_treeWidget->Initialize(&m_recResultRecordSet, true, false, false, true);
	//ui.SelectedReports_treeWidget->RefreshDisplay();
	//CompleteChanged();


	//Disable add all push button.
	//ui.AddAll_pushButton->setDisabled(true);

	//Set tree widgets look and feel.
	ui.ReportList_treeWidget->SetHeader();
	ui.ReportList_treeWidget->setRootIsDecorated(false);
	ui.ReportList_treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
	//ui.SelectedReports_treeWidget->SetHeader();
	//ui.SelectedReports_treeWidget->setRootIsDecorated(false);
	ui.ReportList_treeWidget->SetDropType(m_nWizardPageID);
	//ui.SelectedReports_treeWidget->AddAcceptableDropTypes(m_nWizardPageID);

	//Now that we know wizard page ID read from cache.
	ReadFromCache();

	//Select reports from cache.
	SelectReportsFromCache();
}

void ReportSelectionWizPage::SelectReportsFromCache()
{
	int nRowCount = m_recResultRecordSet.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nID = m_recResultRecordSet.getDataRef(i, 0).toInt();
		ui.ReportList_treeWidget->SelectItemByRowID(nID);
	}
	CompleteChanged();
}

QList<WizardPage*> ReportSelectionWizPage::GetNextPageCollectionVector()
{
	WizardRegistration wizReg(m_nNextPageCollection);
	QList<WizardPage*> tmpList = wizReg.m_hshWizardPageHash.value(m_nNextPageCollection)->GetPages();
	return tmpList;
}

void ReportSelectionWizPage::BuildReportList()
{
	//Get report hash.
#if WINCE
	QHash<int, QString> *hshReportIDToNameHash;
#else
	QHash<int, QString> hshReportIDToNameHash = ReportRegistration::GetReportData();
#endif //WINCE

	QHashIterator<int, QString> i(hshReportIDToNameHash);
	int k=0;
	while (i.hasNext()) 
	{
		i.next();
		int reportID = i.key();
		QString ReportName = i.value();
		m_ReportsRecordSet.addRow();
		m_ReportsRecordSet.setData(k, "RPT_ID",		reportID);
		m_ReportsRecordSet.setData(k, "RPT_CODE",	QVariant(reportID).toString());
		m_ReportsRecordSet.setData(k, "RPT_LEVEL",	1); //Level is 1 because of style sheet of report names in universaltreewidget.
		m_ReportsRecordSet.setData(k, "RPT_PARENT", 0);
		m_ReportsRecordSet.setData(k, "RPT_NAME",	ReportName);
		k++;
	}
}

void ReportSelectionWizPage::OnDblClick()
{
	CompleteChanged();
	emit goNextPage();
}


void ReportSelectionWizPage::CompleteChanged()
{
	DbRecordSet lstData;
	ui.ReportList_treeWidget->GetDropValue(lstData);
	m_recResultRecordSet=lstData;
	if (lstData.getRowCount()>0)
	{
		m_bComplete = true;
		int reportID = lstData.getDataRef(0,"RPT_ID").toInt();
		if (reportID == ReportRegistration::REPORT_CONTACT_LIST || reportID == ReportRegistration::REPORT_CONTACT_DETAILS)
			m_nNextPageCollection = WizardRegistration::CONTACT_SELECTION_WIZARD;
		if (reportID == ReportRegistration::REPORT_CONTACT_COMMUNICATION_DETAILS)
			m_nNextPageCollection = WizardRegistration::CONTACT_COMMUNICATION_REPORT_SELECTION_WIZARD;
		if (reportID == ReportRegistration::REPORT_PROJECT_LIST)
			m_nNextPageCollection = WizardRegistration::PROJECT_REPORT_SELECTION_WIZARD;
		if(reportID == ReportRegistration::REPORT_CONTACT_RELATIONS)
			m_nNextPageCollection = WizardRegistration::REPORT_CONTACT_RELATIONSHIPS;
	}
	else
	{
		m_bComplete = false;
	}

	completeStateChanged();
}
/*
void ReportSelectionWizPage::on_AddSelected_pushButton_clicked()
{
	dynamic_cast<WizardBase*>(m_pParent)->ClearPageHashFromThisPageUp(0);
	AddSelectedReports();
}

void ReportSelectionWizPage::on_AddAll_pushButton_clicked()
{
	ui.ReportList_treeWidget->selectAll();
	AddSelectedReports();
	ui.ReportList_treeWidget->clearSelection();
}

void ReportSelectionWizPage::on_RemoveSelected_pushButton_clicked()
{
	RemoveSelectedReports();
}

void ReportSelectionWizPage::on_RemoveAll_pushButton_clicked()
{
	ui.SelectedReports_treeWidget->selectAll();
	RemoveSelectedReports();
}
*/