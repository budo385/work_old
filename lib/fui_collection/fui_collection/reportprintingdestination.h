#ifndef REPORTPRINTINGDESTINATION_H
#define REPORTPRINTINGDESTINATION_H

#include <QWidget>
#include "ui_reportprintingdestination.h"
#include "gui_core/gui_core/wizardpage.h"


class ReportPrintingDestination : public WizardPage
{
    Q_OBJECT

public:
    ReportPrintingDestination(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
    ~ReportPrintingDestination();

	void Initialize();

private:
    Ui::ReportPrintingDestinationClass ui;

	void SetupItems();

	QList<QTreeWidgetItem *> m_lstPrintDestinations;

private slots:
	void CompleteChanged();
};

#endif // REPORTPRINTINGDESTINATION_H
