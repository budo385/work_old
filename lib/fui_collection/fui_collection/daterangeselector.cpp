#include "daterangeselector.h"
#include  <QMessageBox>

#include "common/common/datahelper.h"
#include "gui_core/gui_core/gui_helper.h"

DateRangeSelector::DateRangeSelector(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);

	m_bUseAsFocusOut = false;

	ui.cboRangeSize->addItem("-");
	ui.cboRangeSize->addItem(tr("Day"));
	ui.cboRangeSize->addItem(tr("Week"));
	ui.cboRangeSize->addItem(tr("Month"));
	ui.cboRangeSize->addItem(tr("Year"));
	ui.cboRangeSize->setCurrentIndex(0);

	ui.btnCurDateRange->setMaximumHeight(22);
	ui.btnPrev->setMaximumHeight(22);
	ui.btnNext->setMaximumHeight(22);
	ui.btnCurDateRange->setIcon(QIcon(":2downarrow.png"));
	ui.btnPrev->setIcon(QIcon(":1leftarrow.png"));
	ui.btnNext->setIcon(QIcon(":1rightarrow.png"));
	ui.btnCurDateRange->setFlat(true);
	ui.btnPrev->setFlat(true);
	ui.btnNext->setFlat(true);

	ui.dateFrom->setDate(QDate::currentDate());
	ui.dateTo->setDate(QDate::currentDate());

	connect(ui.dateFrom,SIGNAL(dateChanged(const QDate)),this, SLOT(OnDateChanged(const QDate)));
	connect(ui.dateTo,SIGNAL(dateChanged(const QDate)),this, SLOT(OnDateChanged(const QDate)));
	connect(ui.dateFrom,SIGNAL(focusOutSignal()),this, SLOT(OnFromDateFocusOut()));
	connect(ui.dateTo,SIGNAL(focusOutSignal()),this, SLOT(OnToDateFocusOut()));
	connect(ui.cboRangeSize, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboChanged(int)));
}

DateRangeSelector::~DateRangeSelector()
{
}

void DateRangeSelector::on_btnPrev_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nRangeIdx = ui.cboRangeSize->currentIndex();

	DataHelper::DateRangeSelectorPreviousNextButtonClicked(dateFrom, dateTo, nRangeIdx, -1);
	
	blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	if (m_bUseAsFocusOut)
		emit onFocusOut(dtFrom, dtTo, nRangeIdx); 
	else
		emit onDateChanged(dtFrom, dtTo, nRangeIdx); 
}

void DateRangeSelector::on_btnNext_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nRangeIdx = ui.cboRangeSize->currentIndex();

	DataHelper::DateRangeSelectorPreviousNextButtonClicked(dateFrom, dateTo, nRangeIdx, 1);
	
	blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	if (m_bUseAsFocusOut)
		emit onFocusOut(dtFrom, dtTo, nRangeIdx); 
	else
		emit onDateChanged(dtFrom, dtTo, nRangeIdx); 
}

void DateRangeSelector::on_btnCurDateRange_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nRangeIdx = ui.cboRangeSize->currentIndex();
	
	DataHelper::DateRangeSelectorCurrentButtonClicked(dateFrom, dateTo, nRangeIdx);

	//Disable or enable buttons if combo not empty.
	if (nRangeIdx>0)
	{
		ui.dateFrom->setDisabled(true);
		ui.dateTo->setDisabled(true);
		ui.btnNext->setDisabled(true);
		ui.btnPrev->setDisabled(true);
	}
	else
	{
		ui.dateFrom->setDisabled(false);
		ui.dateTo->setDisabled(false);
		ui.btnNext->setDisabled(false);
		ui.btnPrev->setDisabled(false);
	}

	blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	if (m_bUseAsFocusOut)
		emit onFocusOut(dtFrom, dtTo, nRangeIdx); 
	else
		emit onDateChanged(dtFrom, dtTo, nRangeIdx); 
}

void DateRangeSelector::Initialize(QDateTime &dateFrom, QDateTime &dateTo, int nRange)
{
	blockSignals(true);

	//Marin wanted this. Task #726.
	if (nRange > 0)
		ui.cboRangeSize->setCurrentIndex(nRange);
	else
	{
		ui.cboRangeSize->setCurrentIndex(nRange);
		ui.dateFrom->setDate(dateFrom.date());
		ui.dateTo->setDate(dateTo.date());
	}
	
	blockSignals(false);
}

void DateRangeSelector::GetDates(QDateTime &dtFrom, QDateTime &dtTo, int &nRange)
{
	nRange	= ui.cboRangeSize->currentIndex();
	dtFrom	= QDateTime(ui.dateFrom->date(), QTime(0,0,0));
	dtTo	= QDateTime(ui.dateTo->date(), QTime(23,59,59));
}

void DateRangeSelector::OnDateChanged(const QDate &)
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int   nRange	= ui.cboRangeSize->currentIndex();
	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	emit onDateChanged(dtFrom, dtTo, nRange); 
}

void DateRangeSelector::OnComboChanged(int nIdx)
{
	on_btnCurDateRange_clicked();
}

void DateRangeSelector::OnFromDateFocusOut()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int   nRange	= ui.cboRangeSize->currentIndex();
	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	emit onFocusOut(dtFrom, dtTo, nRange); 
}

void DateRangeSelector::OnToDateFocusOut()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int   nRange	= ui.cboRangeSize->currentIndex();

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));
	emit onFocusOut(dtFrom, dtTo, nRange); 
}
