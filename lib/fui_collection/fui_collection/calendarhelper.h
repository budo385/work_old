#ifndef CALENDARHELPER_H
#define CALENDARHELPER_H

#include "common/common/dbrecordset.h"
#include "common/common/status.h"

class CalendarHelper 
{
public:
	static void		GetCalendarTemplates(DbRecordSet &lstData,bool bReloadFromServer=false);
	static void		RecalcReservationList(QDateTime datStartDate, QDateTime datEndDate,DbRecordSet &Ret_Reservations,DbRecordSet &Ret_ReservationsRecalculated);
	static QString	GetCalendarStateNameFromID(int nStateID);
	static QString	GenerateSubjectID(DbRecordSet &rowData);
	static QString	ParseSubjectID(QString strSubject);
	static void		CreateInviteEmailFromTemplate(QString bodyTemplate, QString &NewBody, DbRecordSet &rowData,bool btoPercentEncoding=false);
	static QString	GetDefaultInviteSubject();
	static QString	GetDefaultInviteBody();
	static void		GetDefaultInviteSMS_BodyAndSubject(QString &strSubject,QString &strBody);
	static QString	GetDefaultLanguageName();
	

	static void		SendInvitationsBySMS(Status &err,DbRecordSet &lstData);
	static void		SendInvitations(Status &err,DbRecordSet &lstData);
	static bool		GenerateICalendar(DbRecordSet &lstData, QString &strBodyResult, QString &strSubject);
	static bool		GenerateVCalendar(DbRecordSet &lstData, QString &strBodyResult);
	static void		ParseCelandar(QString strEmailBody,DbRecordSet &data);
};

#endif // CALENDARHELPER_H
