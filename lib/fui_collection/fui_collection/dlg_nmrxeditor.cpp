#include "dlg_nmrxeditor.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "gui_core/gui_core/thememanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include <QKeyEvent>
#include <QCloseEvent>
#include "bus_client/bus_client/emailhelper.h"
#include "gui_core/gui_core/macros.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/formatphone.h"
#include "bus_client/bus_client/clientcontactmanager.h"

//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;	
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

Dlg_NMRXEditor::Dlg_NMRXEditor(QWidget *parent)
    : QDialog(parent),m_bSkipWriteMode(false),m_bReadOnlyMode(false)
{
	ui.setupUi(this);
	m_Layout=dynamic_cast<QVBoxLayout*>(this->layout()->itemAt(0)->layout()->itemAt(2));
	Q_ASSERT(m_Layout);

	m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">";
	m_strEndHtmlTag="</span></p></body></html>";


	QSize size(24,70);
	ui.btnReverse->setIcon(QIcon(":Switch.png"));
	ui.btnReverse->setIconSize(size);


	//QString	styleSectionButtonLvUp="QLabel {color:white} QPushButton { background-image:url(:Switch.png)}";
	//ui.btnReverse->setStyleSheet(styleSectionButtonLvUp);//Icon(QIcon(":Switch.png"));


	//define
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	//role:
	ui.frameRole->Initialize(ENTITY_NMRX_ROLES);


	//bring up field manager for description:
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData);
	m_GuiFieldManagerMain->RegisterField("BNMR_DESCRIPTION",ui.frameEditor);

	//set title
	this->setWindowTitle(tr("Define a Relationship"));

	ui.frameEditor->SetEmbeddedPixMode();

	QPixmap pix1(":sokrates_ico.png");
	ui.labBySC->setPixmap(pix1);
	ui.labBySC->setToolTip(tr("Send Invite By Sokrates"));
	ui.ckbBySokrates->setToolTip(tr("Send Invite By Sokrates"));

	QPixmap pix2(":Email16.png");
	ui.labByEmail->setPixmap(pix2);
	ui.labByEmail->setToolTip(tr("Send Invite By Email"));
	ui.ckbBySokrates->setToolTip(tr("Send Invite By Email"));
	
	QPixmap pix3(":Comm_SMS16.png");
	ui.labBySMS->setPixmap(pix3);
	ui.labBySMS->setToolTip(tr("Send Invite By SMS"));
	ui.ckbBySMS->setToolTip(tr("Send Invite By SMS"));

	ui.frameEmailTemplate->Initialize(ENTITY_BUS_EMAIL_TEMPLATES);
	ui.frameEmailAddress->Initialize(ENTITY_PERSON_EMAIL_ADDRESS_SELECTOR);
	//:default template per language, default mail of contact:
	int nDefaultEmailTemplateID=EmailHelper::GetDefaultEmailInviteTemplateForDefaultLanguage();
	ui.frameEmailTemplate->SetCurrentEntityRecord(nDefaultEmailTemplateID);

	ui.frameEmailAddress->GetButton(Selection_SAPNE::BUTTON_MODIFY)->setVisible(false);
	ui.frameEmailAddress->GetButton(Selection_SAPNE::BUTTON_ADD)->setVisible(false);
	ui.frameEmailAddress->GetButton(Selection_SAPNE::BUTTON_CONTACT_PIE)->setVisible(false);
	ui.frameEmailAddress->GetButton(Selection_SAPNE::BUTTON_VIEW)->setVisible(false);

	ui.frameEmailTemplate->GetButton(Selection_SAPNE::BUTTON_MODIFY)->setVisible(false);
	ui.frameEmailTemplate->GetButton(Selection_SAPNE::BUTTON_ADD)->setVisible(false);
	ui.frameEmailTemplate->GetButton(Selection_SAPNE::BUTTON_CONTACT_PIE)->setVisible(false);
	ui.frameEmailTemplate->GetButton(Selection_SAPNE::BUTTON_VIEW)->setVisible(false);

	connect(ui.frameSAPNE_M,SIGNAL(CurrentEntityRecordChanged(int)),this,SLOT(OnContactChanged(int)));

	//issue 1702:
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());

}

Dlg_NMRXEditor::~Dlg_NMRXEditor()
{
	delete m_GuiFieldManagerMain;

}



/*!
	Called after WRITE, refresh NAME column

	\param m_nMasterTableID		- master table id
	\param strMasterTableName	- master name to be display
	\param rowData				- row must be filled with data when insert (table_1,2,record_1 or 2 )
	\param nMode				- READ,INSERT, or EDIT

*/
void Dlg_NMRXEditor::SetRow(int nMasterTableID,int nMasterRecordID,QString strCalculatedMasterName,DbRecordSet &rowData,int nMode,QString strMasterName,QString strSlaveName,int nX_TableID,bool bSkipWriteMode, bool bDisableRoleSwitch, bool bMoveMasterSAPNEDown,bool bReadOnlyMode)
{
	m_bReadOnlyMode=bReadOnlyMode;
	m_strMasterName=strMasterName+":";
	m_strSlaveName=strSlaveName+":";
	m_lstData=rowData;
	m_bSkipWriteMode=bSkipWriteMode;
	m_nX_TableID=nX_TableID;

	if (bDisableRoleSwitch)
		ui.btnReverse->setVisible(false);

	if(!g_AccessRight->TestAR(MODIFY_DOCUMENTS_CATEGORY))
		ui.frameRole->DisableEntryText();


	//m_lstData.Dump();
	m_nMode=nMode;
	Q_ASSERT(m_lstData.getRowCount()==1); //must contain 1 row

	if (rowData.getDataRef(0,"BNMR_TABLE_1").toInt()==nMasterTableID &&rowData.getDataRef(0,"BNMR_TABLE_KEY_ID_1").toInt()==nMasterRecordID)
	{
		m_bMasterIs_M=true;
		m_nSlaveTableID=rowData.getDataRef(0,"BNMR_TABLE_2").toInt();
	}
	else
	{
		m_bMasterIs_M=false;
		m_nSlaveTableID=rowData.getDataRef(0,"BNMR_TABLE_1").toInt();
	}

	//save start spin position
	m_bMasterIs_M_Original=m_bMasterIs_M;

	//determine master (has label) & slave (has SAPNE)
	if (m_bMasterIs_M)
	{
		rowData.getData(0,"BNMR_TABLE_1",m_nMasterTableID);
		rowData.getData(0,"BNMR_TABLE_2",m_nSlaveTableID);
	}
	else
	{
		rowData.getData(0,"BNMR_TABLE_2",m_nMasterTableID);
		rowData.getData(0,"BNMR_TABLE_1",m_nSlaveTableID);
	}

	//set label:
	ui.label_N->setText(m_strHtmlTag+strCalculatedMasterName+m_strEndHtmlTag);

	//determnine sapne entity for slave:
	int nEntityID=NMRXManager::GetEntityIDFromTableID(m_nSlaveTableID);

	
	//set filter on roles, based on tables's id
	if (m_bMasterIs_M)
	{
		ui.frameSAPNE_M->Initialize(nEntityID,&m_lstData,"BNMR_TABLE_KEY_ID_2");
		ui.frameRole->GetSelectionController()->GetLocalFilter()->SetFilter("BNRO_TABLE_1",m_nMasterTableID);
		ui.frameRole->GetSelectionController()->GetLocalFilter()->SetFilter("BNRO_TABLE_2",m_nSlaveTableID);
		ui.labelMaster->setText(m_strMasterName);
		ui.labelSAPNE->setText(m_strSlaveName);

	}
	else
	{
		ui.frameSAPNE_M->Initialize(nEntityID,&m_lstData,"BNMR_TABLE_KEY_ID_1");
		ui.frameRole->GetSelectionController()->GetLocalFilter()->SetFilter("BNRO_TABLE_1",m_nSlaveTableID);
		ui.frameRole->GetSelectionController()->GetLocalFilter()->SetFilter("BNRO_TABLE_2",m_nMasterTableID);
		ui.labelSAPNE->setText(m_strMasterName);
		ui.labelMaster->setText(m_strSlaveName);
	}



	ui.frameRole->GetSelectionController()->ReloadData(); //apply filter:combo will be reloaded!
	//ui.frameRole->GetSelectionController()->GetDataSource()->Dump();


	//spin fields only if master is M
	if (!m_bMasterIs_M)
	{
		m_bMasterIs_M=true;
		QString strRoleName=m_lstData.getDataRef(0,"BNRO_ASSIGMENT_TEXT").toString();
		on_btnReverse_clicked();
		m_lstData.setData(0,"BNRO_ASSIGMENT_TEXT",strRoleName);
	}


	//if edit lock
	if (nMode==MODE_EDIT && !m_bSkipWriteMode)
	{
		int nID=m_lstData.getDataRef(0,"BNMR_ID").toInt();
		Q_ASSERT(nID!=0);
		Status err;
		_SERVER_CALL(BusNMRX->Lock(err,nID,m_strLockedRes))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
	}

	if(bMoveMasterSAPNEDown)
		on_btnReverse_clicked(); //on reverse
		//ReverseSAPNEs();


	//issue 2586: if project & contact and insert mode and project is up, then reverse 
	//if (m_nMode == MODE_INSERT && m_nMasterTableID==BUS_CM_CONTACT && m_nSlaveTableID==BUS_PROJECT)
	//{
	//	on_btnReverse_clicked(); //on reverse
	//}

	//set Mode:
	SetMode(nMode);

	int nX;
	if (!ui.frameSAPNE_M->GetCurrentEntityRecord(nX))
	{
		if(!ui.frameSAPNE_M->OpenSelector())
		{
			QTimer::singleShot(0,this,SLOT(on_btnCancel_clicked()));
		}
	}



	if (nX_TableID==BUS_CAL_INVITE)
	{
		DbRecordSet rowRecordX=rowData.getDataRef(0,"X_RECORD").value<DbRecordSet>();
		if (rowRecordX.getRowCount()==0)
		{
			NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
			rowData.setData(0,"X_RECORD",rowRecordX);
		}
		//rowRecordX.Dump();

		ui.ckbInvite->setChecked(rowRecordX.getDataRef(0,"BCIV_INVITE").toInt());
		ui.ckbBySokrates->setChecked(rowRecordX.getDataRef(0,"BCIV_BY_NOTIFICATION").toInt());
		ui.ckbByEmail->setChecked(rowRecordX.getDataRef(0,"BCIV_BY_EMAIL").toInt());
		ui.ckbBySMS->setChecked(rowRecordX.getDataRef(0,"BCIV_BY_SMS").toInt());

		//frame Email adress
		int nKey2ID=rowData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt();
		OnContactChanged(nKey2ID);

		//frame Email template
		int nEmailTemplateID=rowRecordX.getDataRef(0,"BCIV_EMAIL_TEMPLATE_ID").toInt();
		if(nEmailTemplateID>0)
			ui.frameEmailTemplate->SetCurrentEntityRecord(nEmailTemplateID);

		ui.frameInvite->setVisible(true);
	}
	else
		ui.frameInvite->setVisible(false);

	//if defined specify default role:
	if ((m_nMasterTableID == BUS_CM_CONTACT && m_nSlaveTableID == BUS_CM_CONTACT) || (m_nMasterTableID == BUS_CM_CONTACT && m_nSlaveTableID == BUS_PERSON))
	{
		QString strDefaultRole=g_pSettings->GetPersonSetting(CONTACT_DEFAULT_ROLE).toString();
		if (!strDefaultRole.isEmpty())
			ui.frameRole->SetCurrentDisplayName(strDefaultRole);
	}
	if ((m_nMasterTableID == BUS_PERSON && m_nSlaveTableID == BUS_CM_CONTACT) || (m_nMasterTableID == BUS_PERSON && m_nSlaveTableID == BUS_PERSON))
	{
		QString strDefaultRole=g_pSettings->GetPersonSetting(PERSON_DEFAULT_ROLE).toString();
		if (!strDefaultRole.isEmpty())
			ui.frameRole->SetCurrentDisplayName(strDefaultRole);
	}

	if(!g_AccessRight->TestAR(MODIFY_NMRX_ROLES))
		ui.frameRole->DisableEntryText();

	if(!g_AccessRight->TestAR(ENTER_MODIFY_CONTACT_ASSIGNMENT_ROLE)) //just disable c/c and c/u roles
		if ((m_nMasterTableID == BUS_CM_CONTACT && m_nSlaveTableID == BUS_CM_CONTACT) || (m_nMasterTableID == BUS_CM_CONTACT && m_nSlaveTableID == BUS_PERSON))
			ui.frameRole->DisableEntryText();


	ui.frameRole->setFocus();
}


//revolve:
void Dlg_NMRXEditor::on_btnReverse_clicked()
{

	m_Layout->removeWidget(ui.frameSAPNE_M);
	m_Layout->removeWidget(ui.label_N);

	m_bMasterIs_M=!m_bMasterIs_M;
	if (m_bMasterIs_M)
	{
		m_Layout->insertWidget(0,ui.label_N);
		m_Layout->addWidget(ui.frameSAPNE_M);
		//reload roles:
		ui.frameRole->GetSelectionController()->GetLocalFilter()->ClearFilter();
		ui.frameRole->GetSelectionController()->GetLocalFilter()->SetFilter("BNRO_TABLE_1",m_nMasterTableID);
		ui.frameRole->GetSelectionController()->GetLocalFilter()->SetFilter("BNRO_TABLE_2",m_nSlaveTableID);
		ui.labelMaster->setText(m_strMasterName);
		ui.labelSAPNE->setText(m_strSlaveName);

	}
	else
	{
		m_Layout->insertWidget(0,ui.frameSAPNE_M);
		m_Layout->addWidget(ui.label_N);
		//reload roles:
		ui.frameRole->GetSelectionController()->GetLocalFilter()->ClearFilter();
		ui.frameRole->GetSelectionController()->GetLocalFilter()->SetFilter("BNRO_TABLE_2",m_nMasterTableID);
		ui.frameRole->GetSelectionController()->GetLocalFilter()->SetFilter("BNRO_TABLE_1",m_nSlaveTableID);
		ui.labelSAPNE->setText(m_strMasterName);
		ui.labelMaster->setText(m_strSlaveName);
	}

	ui.frameRole->GetSelectionController()->ReloadData(); //apply filter:combo will be reloaded!
	ui.frameRole->Clear();

	RefreshDisplay();
}



void Dlg_NMRXEditor::RefreshDisplay()
{
	//refresh ACP:
	if (m_lstData.getRowCount()>0)
		ui.frameRole->SetCurrentEntityRecord(m_lstData.getDataRef(0, "BNMR_ROLE_ID").toInt());
	else
		ui.frameRole->Clear();

	//m_lstData.Dump();
	ui.frameSAPNE_M->RefreshDisplay();
	m_GuiFieldManagerMain->RefreshDisplay();
}


void Dlg_NMRXEditor::SetMode(int nMode)
{

	if(nMode==MODE_READ)
	{
		ui.frameRole->SetEditMode(false);
		ui.frameSAPNE_M->SetEditMode(false);
		ui.frameEditor->SetEditMode(false);
		ui.btnEdit->setEnabled(true);
		ui.btnDelete->setEnabled(true);
		ui.btnReverse->setEnabled(false);
		ui.btnOK->setEnabled(false);
	}
	else
	{
		ui.frameRole->SetEditMode(true);
		ui.frameSAPNE_M->SetEditMode(true);
		ui.frameEditor->SetEditMode(true);
		ui.btnEdit->setEnabled(false);
		ui.btnReverse->setEnabled(true);
		ui.btnOK->setEnabled(true);
		if (nMode!=MODE_EDIT)
			ui.btnDelete->setEnabled(false);  //delete enabled for edit: ensure unlock
	}

	m_nMode=nMode;
	RefreshDisplay();

}



void Dlg_NMRXEditor::on_btnOK_clicked()
{
	if (m_nMode=MODE_EDIT || m_nMode==MODE_INSERT)
	{
		QString strRoleName;
		QString strRole=ui.frameRole->GetCurrentDisplayName();
		/*
		if (strRole.isEmpty())
		{
			QMessageBox::warning(this,tr("Warning"),tr("Role must be set!"));
			return;
		}
		*/
		if (!m_bSkipWriteMode)
		{
			if (m_lstData.getDataRef(0,"BNMR_TABLE_KEY_ID_1").toInt()==0)
			{
				QMessageBox::warning(this,tr("Warning"),tr("Assignment must be set!"));
				return;
			}
		}

		if (m_lstData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt()==0)
		{
			QMessageBox::warning(this,tr("Warning"),tr("Assigment must be set!"));
			return;
		}


		//swap numbers if swap occur
		if (m_bMasterIs_M_Original!=m_bMasterIs_M)
		{
			//skip if calendar...calendar must be first always...(B.T. changed 12.03.2013)
			if (!(m_lstData.getDataRef(0,"BNMR_TABLE_1").toInt()==CE_COMM_ENTITY || m_lstData.getDataRef(0,"BNMR_TABLE_2").toInt()==CE_COMM_ENTITY))
			{
				int nOld=m_lstData.getDataRef(0,"BNMR_TABLE_1").toInt();
				m_lstData.setData(0,"BNMR_TABLE_1",m_lstData.getDataRef(0,"BNMR_TABLE_2").toInt());
				m_lstData.setData(0,"BNMR_TABLE_2",nOld);
				nOld=m_lstData.getDataRef(0,"BNMR_TABLE_KEY_ID_1").toInt();
				m_lstData.setData(0,"BNMR_TABLE_KEY_ID_1",m_lstData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt());
				m_lstData.setData(0,"BNMR_TABLE_KEY_ID_2",nOld);
			}
		}
		

		//save role? : determine if new role, added by ACP:
		QVariant intNull(QVariant::Int);
		DbRecordSet rowTaskType;
		int nID;
		if(ui.frameRole->GetCurrentEntityRecord(nID))
		{
			m_lstData.setData(0,"BNMR_ROLE_ID",nID);
			m_lstData.setData(0,"BNRO_NAME",ui.frameRole->GetCurrentDisplayName());
		}
		else
		{
			m_lstData.setData(0,"BNMR_ROLE_ID",intNull);
			m_lstData.setData(0,"BNRO_NAME","");
		}

		if (m_nX_TableID==BUS_CAL_INVITE) //save invite info:
		{
			DbRecordSet rowRecordX=m_lstData.getDataRef(0,"X_RECORD").value<DbRecordSet>();
			if (rowRecordX.getRowCount()==0)
			{
				NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
			}
			rowRecordX.setData(0,"BCIV_INVITE",(int)ui.ckbInvite->isChecked());
			rowRecordX.setData(0,"BCIV_BY_NOTIFICATION",(int)ui.ckbBySokrates->isChecked());
			rowRecordX.setData(0,"BCIV_BY_EMAIL",(int)ui.ckbByEmail->isChecked());
			rowRecordX.setData(0,"BCIV_BY_SMS",(int)ui.ckbBySMS->isChecked());

			int nTemplateID;
			ui.frameEmailTemplate->GetCurrentEntityRecord(nTemplateID);
			if (nTemplateID>0)
				rowRecordX.setData(0,"BCIV_EMAIL_TEMPLATE_ID",nTemplateID);
			else
				rowRecordX.setData(0,"BCIV_EMAIL_TEMPLATE_ID",QVariant(QVariant::Int));

			QString strAddress=ui.frameEmailAddress->GetCurrentDisplayName();
			rowRecordX.setData(0,"BCIV_EMAIL_ADDRESS",strAddress);


			m_lstData.setData(0,"X_RECORD",rowRecordX);
		}


		//m_lstData.Dump();
		//save relation:
		Status err;
		/*
		DbRecordSet rowDateForWrite;
		rowDateForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
		rowDateForWrite.merge(m_lstData);
		rowDateForWrite.setColValue("BNMR_TYPE",QVariant(GlobalConstants::NMRX_TYPE_ASSIGN).toInt());
		*/
		m_lstData.setColValue("BNMR_TYPE",QVariant(GlobalConstants::NMRX_TYPE_ASSIGN).toInt());
		if (!m_bSkipWriteMode)
		{

			_SERVER_CALL(BusNMRX->WriteSimple(err,m_lstData,m_strLockedRes,m_nX_TableID))
				if(!err.IsOK())
				{
					QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
					UnlockIfLocked();
					return;
				}
				//m_lstData.assignRow(0,rowDateForWrite,true); //return back data
		}
		//m_lstData.Dump();
	
		UnlockIfLocked();
		done(1); //saved!
	}
	else
		done(0); //as it was cancel

}




void Dlg_NMRXEditor::on_btnCancel_clicked()
{
	UnlockIfLocked();
	done(0);
}

void Dlg_NMRXEditor::on_btnEdit_clicked()
{
	if (m_bReadOnlyMode)
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}


	if (!m_bSkipWriteMode)
	{
		Q_ASSERT(m_lstData.getRowCount()==1);//must be 1 row!
		int nID=m_lstData.getDataRef(0,"BNMR_ID").toInt();
		Q_ASSERT(nID!=0);


		Status err;
		_SERVER_CALL(BusNMRX->Lock(err,nID,m_strLockedRes))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
	}
	
	SetMode(MODE_EDIT);
}


void Dlg_NMRXEditor::on_btnDelete_clicked()
{
	if (m_bReadOnlyMode)
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}

	if (!m_bSkipWriteMode)
	{

		Status err;
		Q_ASSERT(m_lstData.getRowCount()==1);//must be 1 row!

		//lock if not already
		if(m_strLockedRes.isEmpty())
		{
			int nID=m_lstData.getDataRef(0,"BNMR_ID").toInt();
			Q_ASSERT(nID!=0);

			_SERVER_CALL(BusNMRX->Lock(err,nID,m_strLockedRes))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
		}


		//delete it:
		_SERVER_CALL(BusNMRX->Delete(err,m_lstData))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//unlock if allowed:
		if(!m_strLockedRes.isEmpty())
		{
			_SERVER_CALL(BusNMRX->Unlock(err,m_strLockedRes))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			m_strLockedRes.clear();
		}

	}

	done(2); //deleted
}

void Dlg_NMRXEditor::UnlockIfLocked()
{
	if (m_bSkipWriteMode)
		return;
	
	if(m_nMode==MODE_EDIT && !m_strLockedRes.isEmpty())
	{
		Status err;
		_SERVER_CALL(BusNMRX->Unlock(err,m_strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		m_strLockedRes.clear();
	}
}


//close on ESC
void Dlg_NMRXEditor::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Escape)
	{
		on_btnCancel_clicked();
		return;
	}

	QDialog::keyPressEvent(event);
}

//close event interceptor
void Dlg_NMRXEditor::closeEvent(QCloseEvent *event)
{
	on_btnCancel_clicked(); 
	event->accept();
}

void Dlg_NMRXEditor::ReverseSAPNEs()
{
	m_Layout->removeWidget(ui.frameSAPNE_M);
	m_Layout->removeWidget(ui.label_N);
	m_Layout->insertWidget(0,ui.frameSAPNE_M);
	m_Layout->addWidget(ui.label_N);
	ui.labelSAPNE->setText(m_strMasterName);
	ui.labelMaster->setText(m_strSlaveName);
}

void Dlg_NMRXEditor::OnContactChanged(int nContactID) 
{
	if (m_lstData.getRowCount()>0)
	{
		if (nContactID>0)
		{
			if (ui.frameSAPNE_M->GetSelectionController()->GetSelectionEntity()==ENTITY_BUS_PERSON)
			{
				nContactID=ui.frameSAPNE_M->GetSelectionController()->GetContactIDFromPersonID(nContactID); //hard to understand but its true: person/contat dual nature
			}
			//ui.frameEmailAddress->SetCurrentEntityRecord(nContactID);
			ui.frameEmailAddress->GetSelectionController()->GetFilter()->ClearFilter();
			ui.frameEmailAddress->GetSelectionController()->GetFilter()->SetFilter("BCME_CONTACT_ID",nContactID);
			ui.frameEmailAddress->GetSelectionController()->ReloadData(true);
			
			DbRecordSet *pLstEmails=ui.frameEmailAddress->GetSelectionController()->GetDataSource();
			//pLstEmails->Dump();
			int nRow=pLstEmails->find("BCME_IS_DEFAULT",1,true);
			if (nRow>=0)
			{
				ui.frameEmailAddress->SetCurrentEntityRecord(pLstEmails->getDataRef(nRow,"BCME_ID").toInt());
			}
		}
		else
		{
			ui.frameEmailAddress->Clear();
		}

		//reset sms numbeR:
		DbRecordSet rowInvite=m_lstData.getDataRef(0,"X_RECORD").value<DbRecordSet>();
		if (rowInvite.getRowCount()>0)
			rowInvite.setData(0,"BCIV_SMS_NUMBERS","");
		m_lstData.setData(0,"X_RECORD",rowInvite); 
	}
	else
		ui.frameEmailAddress->Clear();
}


void Dlg_NMRXEditor::on_btnSelectSMS_clicked()
{
	int nContactID=0;
	if (m_lstData.getRowCount()==0)
		return;

	if (ui.frameSAPNE_M->GetSelectionController()->GetSelectionEntity()==ENTITY_BUS_PERSON)
	{
		nContactID=ui.frameSAPNE_M->GetSelectionController()->GetContactIDFromPersonID(nContactID); //hard to understand but its true: person/contact dual nature
	}

	nContactID=m_lstData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt();
	DbRecordSet rowInvite=m_lstData.getDataRef(0,"X_RECORD").value<DbRecordSet>();
	if (rowInvite.getRowCount()==0)
	{
		NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowInvite);
	}

	QString strPhones=rowInvite.getDataRef(0,"BCIV_SMS_NUMBERS").toString(); //delimited by ";"
	QStringList lstSelectedPhones=strPhones.split(";",QString::SkipEmptyParts);

	//Get phones
	DbRecordSet lstPhones;
	Status status;
	_SERVER_CALL(BusContact->ReadContactPhones(status, nContactID, lstPhones,DbSqlTableView::TVIEW_BUS_CM_PHONE_SELECT_EXT))
	_CHK_ERR(status);

	//only 1 phone
	QList<int> lstPhoneTypeMobile;
	lstPhoneTypeMobile<<ContactTypeManager::SYSTEM_PHONE_MOBILE<<ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE;

	if (ClientContactManager::SelectPhoneNumberOfContact(lstPhones,lstPhoneTypeMobile,lstSelectedPhones)) //only one
	{
		QString strPhoneSelected;
		if (lstSelectedPhones.size()>0);
		strPhoneSelected=lstSelectedPhones.at(0);

		FormatPhone PhoneFormatter;
		QString strCountry,strLocal,strISO,strArea,strSearch;
		PhoneFormatter.SplitPhoneNumber(strPhoneSelected,strCountry,strArea,strLocal,strSearch,strISO);
		rowInvite.setData(0,"BCIV_SMS_NUMBERS",strSearch);
		m_lstData.setData(0,"X_RECORD",rowInvite); //store back
	}
}