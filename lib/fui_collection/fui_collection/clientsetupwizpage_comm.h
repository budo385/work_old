#ifndef CLIENTSETUPWIZPAGE_COMM_H
#define CLIENTSETUPWIZPAGE_COMM_H

#include <QWidget>
#include "generatedfiles/ui_clientsetupwizpage_comm.h"
#include "gui_core/gui_core/wizardpage.h"

class ClientSetupWizPage_Comm : public WizardPage
{
	Q_OBJECT

public:
	ClientSetupWizPage_Comm(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
	~ClientSetupWizPage_Comm();
	void Initialize();
	//void resetPage(){};
	bool GetPageResult(DbRecordSet &RecordSet);

private slots:
	void on_btnSelectFolders_clicked();

private:
	Ui::ClientSetupWizPage_CommClass ui;
	int m_nIniLstRow;

	void GetFolderSetting(QList<QStringList> &lstFolders);
	void RefreshInfoLabel();
	QList<QStringList> m_lstOutlookFoldersScan;
};

#endif // CLIENTSETUPWIZPAGE_COMM_H
