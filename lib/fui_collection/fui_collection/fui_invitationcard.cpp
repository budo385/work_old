#include "FUI_InvitationCard.h"
#include <QBitmap>
#include <QMouseEvent>
#include <QPainter>
#include <QMenu>
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
//#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
//extern ClientOptionsAndSettingsManager *g_pSettings;
//#include "communicationmanager.h"
//extern CommunicationManager				g_CommManager;				//global comm manager
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "gui_core/gui_core/thememanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "gui_core/gui_core/macros.h"
#include "fui_collection/fui_collection/fui_calendarevent.h"

FUI_InvitationCard::FUI_InvitationCard(QWidget *parent)
	: FuiBase(parent)
{
	m_nEntityRecordID			= 0;
	m_nInviteMode_nPartID		= 0;
	m_nInviteMode_InviteID		= 0;
	m_nInviteMode_nContactId_ConfirmedBy	= 0;
	m_nInviteMode_nPersonID_ConfirmedBy		= 0;
	m_nInviteMode_nOwnerID		= 0;
	m_bInviteMode_bNotifyOwner	= false;

	ui.setupUi(this);

	ui.iconAccept->setPixmap(QPixmap(":Avatar_Accept.png"));
	ui.iconReject->setPixmap(QPixmap(":Avatar_Reject.png"));
	ui.iconImage->setPixmap(QPixmap(":CalendarEvent48.png"));

	connect(ui.iconAccept, SIGNAL(clicked()), this, SLOT(on_AcceptInvite()));
	connect(ui.iconReject, SIGNAL(clicked()), this, SLOT(on_RejectInvite()));

	//cache window bkg image
	m_imageBkg = QImage(ThemeManager::GetAvatarBkg()).convertToFormat(QImage::Format_ARGB32_Premultiplied);

	//create window mask
	QImage image(ThemeManager::GetAvatarBkg());
	QImage mask = image.createAlphaMask();
	QBitmap bmpMask = QBitmap::fromImage(mask);
    setMask(bmpMask);

	setWindowTitle(tr("Invitation Card"));

	g_ChangeManager.registerObserver(this);

	raise();
	activateWindow();
}

FUI_InvitationCard::~FUI_InvitationCard()
{
}

void FUI_InvitationCard::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
	else if(event->button() == Qt::RightButton) 
	{
		QAction *pActClose = new QAction(tr("Close"),this);
		connect(pActClose, SIGNAL(triggered()), this, SLOT(OnMenu_Close()));
		
		QMenu *menu = new QMenu();
		menu->addAction(pActClose);
		menu->popup(event->globalPos());

        event->accept();
    }
}

void FUI_InvitationCard::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPos() - m_dragPosition);
        event->accept();
    }
}

void FUI_InvitationCard::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
	painter.setBackgroundMode(Qt::TransparentMode);
	painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
	painter.drawImage(0, 0, m_imageBkg);
}

//#include "fuibase.h"
void FUI_InvitationCard::mouseDoubleClickEvent(QMouseEvent * event)
{
	//open Calendar Event FUI
	int nNewFUI=g_objFuiManager.OpenFUI(BUS_CAL_EVENT,true,false,FuiBase::MODE_INSERT);
	FUI_CalendarEvent* pFui=dynamic_cast<FUI_CalendarEvent*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->GoInviteAcceptMode(m_nEntityRecordID, m_nInviteMode_nPartID,m_nInviteMode_InviteID,g_pClientManager->GetPersonID(),g_pClientManager->GetPersonContactID(),m_nInviteMode_nOwnerID,true);
	}

	close();
}

void FUI_InvitationCard::SetLabel(QString &strTxt)
{
	ui.txtLabel->setText(strTxt);
	setWindowTitle(tr("Invitation Card: ") + strTxt);
}

void FUI_InvitationCard::SetTaskDate(QString &strTxt)
{
	ui.txtTaskDate->setText(strTxt);
}

void FUI_InvitationCard::SetOwnerInitials(QString &strTxt)
{
	ui.txtTaskInitials->setText(strTxt);
}

void FUI_InvitationCard::SetTaskDescTooltip(const QString &strTxt)
{
	ui.txtLabel->setToolTip(strTxt);
}

void FUI_InvitationCard::OnMenu_Close()
{
	close();
}

void FUI_InvitationCard::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}
}

void FUI_InvitationCard::OnThemeChanged()
{
	m_imageBkg = QImage(ThemeManager::GetAvatarBkg()).convertToFormat(QImage::Format_ARGB32_Premultiplied);

	//create window mask
	QImage image(ThemeManager::GetAvatarBkg());
	QImage mask = image.createAlphaMask();
	QBitmap bmpMask = QBitmap::fromImage(mask);
	setMask(bmpMask);
	this->update();
}

void FUI_InvitationCard::on_AcceptInvite()
{
	Status err;
	_SERVER_CALL(BusCalendar->SetInviteStatus(err,m_nEntityRecordID,m_nInviteMode_nPartID,m_nInviteMode_InviteID,m_nInviteMode_nContactId_ConfirmedBy,m_nInviteMode_nPersonID_ConfirmedBy,GlobalConstants::STATUS_CAL_INVITE_CONFIRMED,m_nInviteMode_nOwnerID,m_bInviteMode_bNotifyOwner))
	_CHK_ERR(err);
}

void FUI_InvitationCard::on_RejectInvite()
{
	Status err;
	_SERVER_CALL(BusCalendar->SetInviteStatus(err,m_nEntityRecordID,m_nInviteMode_nPartID,m_nInviteMode_InviteID,m_nInviteMode_nContactId_ConfirmedBy,m_nInviteMode_nPersonID_ConfirmedBy,GlobalConstants::STATUS_CAL_INVITE_REJECTED,m_nInviteMode_nOwnerID,m_bInviteMode_bNotifyOwner))
	_CHK_ERR(err);
}

void FUI_InvitationCard::SetInvitationInfo(int nCalEvID, int nPartID, int nOwnerID, int nInviteID, int nPersonID)
{
	m_nEntityRecordID = nCalEvID;
	m_nInviteMode_InviteID = nInviteID;
	m_nInviteMode_nPartID  = nPartID;
	m_nInviteMode_nOwnerID = nOwnerID;
	//TOFIX m_nInviteMode_nContactId_ConfirmedBy = 
	m_nInviteMode_nPersonID_ConfirmedBy = nPersonID;
}
