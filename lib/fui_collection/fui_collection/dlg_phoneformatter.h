#ifndef DLG_PHONEFORMATTER_H
#define DLG_PHONEFORMATTER_H

#include <QtWidgets/QDialog>
#include "ui_dlg_phoneformatter.h"
#include "bus_client/bus_client/selection_combobox.h"
#include "bus_core/bus_core/formatphone.h"


class Dlg_PhoneFormatter : public QDialog
{
    Q_OBJECT

public:
    Dlg_PhoneFormatter(QWidget *parent = 0);
    ~Dlg_PhoneFormatter();

	void SetRow(QString strPhone,QString strCountryISO="");
	void GetRow(QString &strPhone, QString &strCountry,QString &strArea,QString &strLocal,QString &strSearch,QString &strCountryISO);

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void OnPhoneChange();

private:
    Ui::Dlg_PhoneFormatterClass ui;

	Selection_ComboBox m_CountryHandler;
	FormatPhone m_FormatPhone;
};

#endif // DLG_PHONEFORMATTER_H
