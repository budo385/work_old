#include "optionsandsettingswidget.h"
#include "gui_core/gui_core/macros.h"
#include <QHeaderView>
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache; //global cache.
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "gui_core/gui_core/gui_helper.h"
#include "os_specific/os_specific/stylemanager.h"
#include "bus_core/bus_core/customavailability.h"

//******************************************************************************//
//						Options and settings widgets.							//
//******************************************************************************//
#include "appearancesettings.h"
#include "countrysettings.h"
#include "settings_docdefaults.h"
#include "options_docdefaults.h"
#include "settings_comm.h"
#include "settings_outlook.h"
#include "settings_thunderbird.h"
#include "settings_email.h"
#include "settings_email_out.h"
#include "settings_contacts.h"
#include "settings_projects.h"
#include "options_contacts.h"
#include "options_organization.h"
#include "settings_programupdates.h"
#include "options_calendar.h"

OptionsAndSettingsWidget::OptionsAndSettingsWidget(bool bIsOption, int nPersonID /*= -1*/, QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();
	//Set tree look.
	ui.treeWidget->header()->hide();
	ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	m_bIsOption = bIsOption;
	m_nPersonID = nPersonID;
	if (nPersonID < 0)
		m_pOptionsAndSettingsManager = NULL;
	else
	{
		//SetMode(FuiBase::MODE_EDIT);
		m_pOptionsAndSettingsManager = new OptionsAndSettingsManager();
		DbRecordSet recSettings;
		_SERVER_CALL(CoreServices->GetPersonalSettingsByPersonID(m_Status, m_nPersonID, recSettings));
		_CHK_ERR(m_Status);
		//_DUMP(recSettings);
		m_pOptionsAndSettingsManager->SetSettingsDataSource(&recSettings, m_nPersonID);

		if (m_nPersonID==g_pClientManager->GetPersonID())
		{
			//B.T. :refresh personal settings global object too:
			g_pSettings->SetSettingsDataSource(&recSettings,m_nPersonID);
		}

	}
	
	InitializeSelectionTree();

	//Application options.
	if (m_bIsOption)
		InitializeAppOptionsWidgets();
	//Personal settings.
	else
		InitializePersonalSettingsWidgets();

	//Insert widgets in stacked widget.
	QHashIterator<int, SettingsBase*> iter(m_hshOptionsSetIDToWidget);
	while(iter.hasNext())
	{
		iter.next();
		int optID = iter.key();
		SettingsBase *widget = iter.value();
		ui.stackedWidget->insertWidget(optID, widget);
	}

	//Connect tree selection to options widget.
 	connect(ui.treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(on_treeSelectionChanged()));

	//resize(600, height());
	//setWidth(600);

	//Select first item.
	if (ui.treeWidget->topLevelItemCount())
		ui.treeWidget->setCurrentItem(ui.treeWidget->topLevelItem(0));
	
}


OptionsAndSettingsWidget::~OptionsAndSettingsWidget()
{
	if (m_pOptionsAndSettingsManager)
		delete(m_pOptionsAndSettingsManager);
	m_pOptionsAndSettingsManager = NULL;
}

QString OptionsAndSettingsWidget::GetFUIName()
{
	if (m_bIsOption)
		return QString(tr("Options"));
	else
		return QString(tr("Settings"));
}

void OptionsAndSettingsWidget::InitializeAppOptionsWidgets()
{
	m_hshOptionsSetIDToWidget.insert(OPTIONS_DOCUMENT_DEFAULTS, new Options_DocDefaults(OPTIONS_DOCUMENT_DEFAULTS, m_bIsOption));
	m_hshOptionsSetIDToWidget.insert(CONTACT_OPTIONS, new Options_Contacts(CONTACT_OPTIONS, m_bIsOption));
	m_hshOptionsSetIDToWidget.insert(ORGANIZATION_OPTIONS, new Options_Organization(ORGANIZATION_OPTIONS, m_bIsOption));
	m_hshOptionsSetIDToWidget.insert(COMM_SETTINGS_APP, new Settings_Comm(COMM_SETTINGS_APP, m_bIsOption));
	m_hshOptionsSetIDToWidget.insert(PROJECT_OPTIONS_APP, new Settings_Projects(PROJECT_OPTIONS_APP, m_bIsOption));
	m_hshOptionsSetIDToWidget.insert(CALENDAR_OPTIONS, new options_calendar(CALENDAR_OPTIONS, m_bIsOption));
}

void OptionsAndSettingsWidget::InitializePersonalSettingsWidgets()
{
	m_hshOptionsSetIDToWidget.insert(APPEARANCE, new AppearanceSettings(APPEARANCE, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(CURRENT_LOCATION, new CountrySettings(CURRENT_LOCATION, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(SETTINGS_DOCUMENT_DEFAULTS, new Settings_DocDefaults(SETTINGS_DOCUMENT_DEFAULTS, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(COMM_SETTINGS, new Settings_Comm(COMM_SETTINGS, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(OUTLOOK_SETTINGS, new Settings_Outlook(OUTLOOK_SETTINGS, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(THUNDERBIRD_SETTINGS, new Settings_Thunderbird(THUNDERBIRD_SETTINGS, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(CONTACT_SETTINGS_VISIBLE, new Settings_Contacts(CONTACT_SETTINGS_VISIBLE, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(PROJECT_SETTINGS_VISIBLE, new Settings_Projects(PROJECT_SETTINGS_VISIBLE, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(PROGRAM_UPDATE_SETTINGS, new Settings_ProgramUpdates(PROGRAM_UPDATE_SETTINGS, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(EMAIL_SETTINGS, new Settings_Email(EMAIL_SETTINGS, m_bIsOption, m_pOptionsAndSettingsManager));
	m_hshOptionsSetIDToWidget.insert(EMAIL_IN_SETTINGS, new Settings_Email_Out(EMAIL_IN_SETTINGS, m_bIsOption, m_pOptionsAndSettingsManager));
}

void OptionsAndSettingsWidget::InitializeSelectionTree()
{
	//First get options sets;
	OptionsAndSettingsOrganizer OptionsOrganizer;
	DbRecordSet recOptionsSets(OptionsOrganizer.GetOptionSets());
	QList<QTreeWidgetItem*> itemsList;
	QSize sizeIcon(24,24);
	ui.treeWidget->setIconSize(sizeIcon);
	QSize sizeCol(340,32);
		
	int optionCount = recOptionsSets.getRowCount();
	for (int i = 0; i < optionCount; ++i)
	{
		//Check is option set hidden and is it option or setting.
		if(recOptionsSets.getDataRef(i, "OptionSetHidden").toBool() 
			|| recOptionsSets.getDataRef(i, "ApplicationOptionSet").toBool() != m_bIsOption)
			continue;
		
		//First row is application option set.
		QTreeWidgetItem *Item = new QTreeWidgetItem();
		int		OptionID  = recOptionsSets.getDataRef(i, 0).toInt();
		QString ItemName  = recOptionsSets.getDataRef(i, 1).toString();
		QString ItemDescr = recOptionsSets.getDataRef(i, 2).toString();
		QString ItemIcon  = recOptionsSets.getDataRef(i, 3).toString();
		Item->setSizeHint(0,sizeCol);
		Item->setIcon(0, QIcon(ItemIcon));
		Item->setData(0, Qt::DisplayRole, ItemName);
		Item->setData(0, Qt::UserRole, OptionID);
		Item->setToolTip(0, ItemDescr);

		if(OptionID==CALENDAR_OPTIONS)
		{
			if(CustomAvailability::IsAvailable_AS_CalendarViewWizard())
			{
				itemsList << Item;
			}
		}
		else
		{
			itemsList << Item;
		}
	}

	if (!itemsList.empty())
		ui.treeWidget->addTopLevelItems(itemsList);
}

void OptionsAndSettingsWidget::Reload()
{
	//Remove all from select tree.
	ui.treeWidget->blockSignals(true);
	ui.treeWidget->clear();

	//Remove all items from stacked widget.
	foreach(int OptionID, m_hshOptionsSetIDToWidget.keys())
		ui.stackedWidget->removeWidget(m_hshOptionsSetIDToWidget.value(OptionID));

	//Then delete options widget hash.
	qDeleteAll(m_hshOptionsSetIDToWidget);
	m_hshOptionsSetIDToWidget.clear();

	//Initialize all.
	InitializeSelectionTree();
	//Application options.
	if (m_bIsOption)
		InitializeAppOptionsWidgets();
	//Personal settings.
	else
		InitializePersonalSettingsWidgets();

	//Insert widgets in stacked widget.
	QHashIterator<int, SettingsBase*> iter(m_hshOptionsSetIDToWidget);
	while(iter.hasNext())
	{
		iter.next();
		int optID = iter.key();
		SettingsBase *widget = iter.value();
		ui.stackedWidget->insertWidget(optID, widget);
	}
	ui.treeWidget->blockSignals(false);

	//Select first item.
	ui.treeWidget->setCurrentItem(ui.treeWidget->topLevelItem(0));
}

void OptionsAndSettingsWidget::on_treeSelectionChanged()
{
	QList<QTreeWidgetItem*> selList(ui.treeWidget->selectedItems());

	if (selList.size()>0) //crash issue 1595
	{
		QTreeWidgetItem *item = selList.first();
		if (item)
		{
			int OptionID = item->data(0, Qt::UserRole).toInt();
			ui.stackedWidget->setCurrentWidget(m_hshOptionsSetIDToWidget.value(OptionID));
		}
	}

	//ui.stackedWidget->resize(300, ui.stackedWidget->height());
}

bool OptionsAndSettingsWidget::CanClose()
{
	DbRecordSet tmp = GetChangedOptionsOrSettingsRecordSet();
	//If there is something changed go to edit mode - so that it can not be closed but after saving or on cancel.
	//_DUMP(tmp);
	if (tmp.getRowCount())
	{
		m_nMode = MODE_EDIT;
		if (m_bUnconditionalClose)
		{
			//Check is the skin changed - it is so far the only setting that has to be handled this way on exit.
			if (tmp.find("BOUS_SETTING_ID", SKIN) && !m_pOptionsAndSettingsManager) 
				StyleManager::SetStyle(g_pSettings->GetPersonSetting(SKIN).toInt());
			
			return true;
		}
		
		QMessageBox::information(this, tr("Warning!"), tr("Save changed settings before closing of press Cancel button!"));
		return false;
	}
	else
	{
		m_nMode = MODE_EMPTY;
		return true;
	}
}

void OptionsAndSettingsWidget::on_selectionChange(int nEntityRecordID)
{
	if (m_pOptionsAndSettingsManager)
		return;
	
	FuiBase::on_selectionChange(nEntityRecordID);
}

DbRecordSet OptionsAndSettingsWidget::GetChangedOptionsOrSettingsRecordSet()
{
	DbRecordSet recChangedOptionsOrSettings;
	//Copy definitions from source.
	recChangedOptionsOrSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS)); //copyDefinition(*g_ClientCache.GetCache(ENTITY_APPLICATION_OPTIONS));

	//Go through options widgets and get changed recordsets.
	foreach(int OptionID, m_hshOptionsSetIDToWidget.keys())
		foreach(SettingsBase *widget, m_hshOptionsSetIDToWidget.values(OptionID))
		recChangedOptionsOrSettings.merge(widget->GetChangedValuesRecordSet());

	return recChangedOptionsOrSettings;
}

void OptionsAndSettingsWidget::on_Save_pushButton_clicked()
{
	//Get current option id for selecting later.
	QTreeWidgetItem *item = ui.treeWidget->currentItem();
	int nCurrentOptionID = item->data(0, Qt::UserRole).toInt();

	if (m_pOptionsAndSettingsManager)
	{
		DbRecordSet recSettings = GetChangedOptionsOrSettingsRecordSet();
		_SERVER_CALL(CoreServices->SavePersonalSettingsByPersonID(m_Status, m_nPersonID, recSettings));
		_CHK_ERR(m_Status);
		_DUMP(recSettings);


		if (m_nPersonID==g_pClientManager->GetPersonID())
		{
			//BT: save to global object also:
			g_pSettings->SavePersonSettings(m_Status,&recSettings);
		}
	}
	else
	{
		//Save settings.
		if (m_bIsOption)
			g_pSettings->SaveApplicationOptions(m_Status, &GetChangedOptionsOrSettingsRecordSet());
		else
			g_pSettings->SavePersonSettings(m_Status,&GetChangedOptionsOrSettingsRecordSet());
	}
	_CHK_ERR(m_Status);

	//Reload data.
	//Reload();

	//Select the one that was previously selected.
	/*int nItemCount = ui.treeWidget->topLevelItemCount();
	for (int i = 0; i < nItemCount; ++i)
	{
		QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);
		if (item->data(0, Qt::UserRole).toInt() == nCurrentOptionID)
			ui.treeWidget->setCurrentItem(item);
	}
	*/
	m_nMode = MODE_EMPTY;
	on_Cancel_pushButton_clicked();
}

void OptionsAndSettingsWidget::on_Cancel_pushButton_clicked()
{
	//Discard all changes and close fui (unconditionally).
	m_bUnconditionalClose = true;
	this->close();
}

//close event interceptor
void OptionsAndSettingsWidget::closeEvent(QCloseEvent *event)
{
	if (m_bUnconditionalClose)
		event->accept();
	else
	{
		if (CanClose())
			event->accept();
		else
			event->ignore();
	}
}
