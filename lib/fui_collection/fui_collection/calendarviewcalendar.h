#ifndef CALENDARVIEWCALENDAR_H
#define CALENDARVIEWCALENDAR_H

#include <QCalendarWidget>
#include <QDate>

class CalendarViewCalendar : public QCalendarWidget
{
	Q_OBJECT

public:
	CalendarViewCalendar(QWidget *parent);
	~CalendarViewCalendar();

	void SetSelectionRange(QDate from, QDate to);

private:
	QDate m_StartDate;
	QDate m_EndDate;

protected:
	void paintCell ( QPainter * painter, const QRect & rect, const QDate & date ) const;
};

#endif // CALENDARVIEWCALENDAR_H
