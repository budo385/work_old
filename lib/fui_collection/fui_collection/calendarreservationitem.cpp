#include "calendarreservationitem.h"

#include <QPen>
#include <QBrush>

#include "qmath.h"

#include "calendartableview.h"

CalendarReservationItem::CalendarReservationItem(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, QDateTime startDateTime, QDateTime endDateTime, 
												 CalendarGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent /*= 0*/)
	: QGraphicsItem(parent)
{
	m_nEntityID				= nEntityID;
	m_nEntityType			= nEntityType;
	m_nBCRS_ID				= nBCRS_ID;
	m_nBCRS_IS_POSSIBLE		= nBCRS_IS_POSSIBLE;
	m_startDateTime			= startDateTime;
	m_endDateTime			= endDateTime;
	m_pCalendarGraphicsView	= pCalendarGraphicsView;
	
	m_nHeight= 0;
	m_nWidth = 7;
	
	setZValue(3);
	//setCacheMode(ItemCoordinateCache);
}

CalendarReservationItem::~CalendarReservationItem()
{
	m_pCalendarGraphicsView	= NULL;
}

QRectF CalendarReservationItem::boundingRect() const
{
	return QRectF(0,0,m_nWidth, m_nHeight);
}

void CalendarReservationItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	painter->setRenderHint(QPainter::Antialiasing);
	
	//Sinusoida.
	QPen pen;
	if (m_nBCRS_IS_POSSIBLE)
		pen = QPen(QBrush(Qt::blue), 2);
	else
		pen = QPen(QBrush(Qt::red), 2);
	painter->setPen(pen);

	QPainterPath path;
	QVector<QPointF> vect;
	for (int i = 0; i<m_nHeight; i++)
	{
		qreal y = i;
		qreal x1 = (m_nWidth/2)*sin(0.4*y);
		qreal x = x1+m_rectItemRectangle.width()-(m_nWidth/2);
		vect << QPointF(x,y);
		//painter->drawPoint(x,y);
	}
	painter->drawLines(vect);
}

void CalendarReservationItem::on_UpdateCalendar()
{
	QRect rect = dynamic_cast<CalendarTableView*>(m_pCalendarGraphicsView->getTableView())->getItemRectangle(m_nEntityID, m_nEntityType, m_startDateTime, m_endDateTime);
	//QString s = m_startDateTime.toString();
	//QString ss = m_endDateTime.toString();
	m_rectItemRectangle.setWidth(rect.width());
	m_rectItemRectangle.setHeight(rect.height());
	m_nHeight = rect.height();
	setPos(rect.topLeft().x()+m_pCalendarGraphicsView->getVertHeaderWidth(), rect.topLeft().y()+m_pCalendarGraphicsView->getTotalHeaderHeight());
}
