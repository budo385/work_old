#ifndef COMMWORKBENCHWIDGET_H
#define COMMWORKBENCHWIDGET_H

#include <QWidget>
#include <QProcess>
#include "ui_commworkbenchwidget.h"
#include "common/common/observer_ptrn.h"
#include "document_piemenu.h"

class CommWorkBenchWidget : public QWidget, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	enum ReloadParameter
	{
		DEFAULT_RELOAD,
		CALENDAR_TASK_TYPE_RELOAD
	};

	CommWorkBenchWidget(QWidget *parent = 0, bool bSideBarMode = false);
	~CommWorkBenchWidget();
	
	void 			SetCurrentGridInSidebarMode(bool bCurrent);
	void 			Initialize(int nGridType, QWidget *pParentFUI, DbRecordSet *recEntities = NULL);
	void 			SetEntitiesRecordset(DbRecordSet *recEntities);
	void			DesktopStartupInitialization(int nLoggedPersonID, int nViewID);
	void 			SetData(int nEntityRecordID);
	
	Selection_SAPNE_FUI* GetContactSAPNE();
	void			RefreshContactItem(int nCEType, DbRecordSet &recEntityRecord);
	DbRecordSet*	GetEmailRecordSet();
	DbRecordSet*	GetVoiceCallRecordSet();
	DbRecordSet*	GetDocumentsRecordSet();
	void			ClearCommGrid();
	void			updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void			GetGridData(int& nGridType,int &nEntityRecordID);
	void			HideUserAndButtons(bool bHide = true);
	void			GetFilter(DbRecordSet &recFilterData);
	bool			IsSidebarMode(){return m_bSideBarMode;};
	void			SelectTask(int nCE_Type, int nID, bool bFromCalendar = false);
	void			ModifySelectedTask();
	DbRecordSet		GetSortRecordSet();

public slots:
	void			SetFilter(int nViewID, DbRecordSet recNewView, DbRecordSet recFilterViewData);
	void 			ReloadData(ReloadParameter reloadParameter = DEFAULT_RELOAD);
	void			ReloadViewWithoutSeverCall(bool bClearSelection = true);
	void 			SubMenuRequestedNewData();

private slots:
	void 			OnSelectionChanged(int nOwnerID, int nContactID, int nProjectID, int nEntitySysType, int nTaskID, QList<int> lstTaskList, int nEntityID, int nPolymorficID, int nBDMD_IS_CHECK_OUT, int nBDMD_CHECK_OUT_USER_ID, int nBTKS_SYSTEM_STATUS, QList<int> lstUnreadEmailsCENTIDs, QList<int> lstStartTaskEmailsCENT_IDs, QList<int> lstStartTaskPhoneCallsCENT_IDs);
	void 			on_openDoc_toolButton_clicked();
	void 			on_startTask_toolButton_clicked();
	void 			on_PostponeTask_toolButton_clicked();
	void 			on_taskDone_toolButton_clicked();
	void 			on_checkIn_toolButton_clicked();
	void 			on_Reply_toolButton_clicked();
	void 			on_Forward_toolButton_clicked();
	void 			on_PackAndSend_toolButton_clicked();
	void 			on_select_toolButton_clicked();
	void 			on_modify_toolButton_clicked();
	void 			on_clear_toolButton_clicked();
	void 			on_AddDoc_toolButton_clicked();
	void 			on_AddMail_toolButton_clicked();
	void 			on_AddVoiceCall_toolButton_clicked();
	void 			on_sort_toolButton_clicked();
	void			on_dateFilter_checkBox_toggled(bool bChecked);
	void 			on_showFilter_toolButton_clicked();
	void 			on_hideFilter_toolButton_clicked();

	void 			OnUserSAPNEChanged();
	void 			OnContactSAPNEChanged();
	void 			OnProjectSAPNEChanged();
	void 			OnOwnerSAPNEChanged();
	void			OnDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange, bool bResizeColumns);
	void 			on_viewSelect_toolButton_clicked();
	void 			on_viewSelect_comboBox_currentIndexChanged(int index);
	void 			on_refresh_pushButton_clicked();
	void			on_refresh2_toolButton_clicked();
	void			on_saveView_toolButton_clicked();
	void 			OnThemeChanged();
	void			OnCnxtMenuActionTriggered(int nActionType);

	void			OpenSplashScreen();

	void			PostPoneTasks(QDateTime datDueDateTime, QDateTime datStartDateTime);

private:
	Ui::CommWorkBenchWidgetClass ui;

	void 			SetSelectionOnNewItems(int nCE_Type, DbRecordSet &lstNewData, bool bFromCalendar = false);
	void 			ContactChanged(int nEntityRecordID, bool bFromReload = false);
	void 			ProjectChanged(int nEntityRecordID, bool bFromReload = false);
	void 			SetPerson(int nPersonID);
	void 			SetCalendar(bool bTaskTypeFilterSet = false);

	void 			FillWithNewContactData(int nContactID, int nCE_Type);
	void 			FillWithNewProjectData(int nProjectID, int nCE_Type);
	void 			FillWithNewOwnerData(int nProjectID, int nCE_Type);
	void 			GetSelectedDataForSAPNEAssignemnt(DbRecordSet &DataAssign, int nOperation);
	void 			GetDefaultData(DbRecordSet &lstContactsDef,DbRecordSet &lstProjectsDef);
	void 			InitializeHeader(int nGridType);
	void 			DisableHeader();
	void 			ClearHeader();
	bool 			ReloadViewSelector(int nViewID = -1, bool bFromDefaultSavedView = false);
	void 			LoadViewComboBox(int nViewID);
	void			SetFilterIntValue(int nValue, int nFilterSetting);
	void			SetFilterDateTimeValue(QDateTime datValue, int nFilterSetting);
	bool			GetFilterBoolValue(DbRecordSet recFilterViewData, int nFilterSetting);
	int				GetFilterIntValue(DbRecordSet recFilterViewData, int nFilterSetting);
	QString			GetFilterStringValue(DbRecordSet recFilterViewData, int nFilterSetting);
	QDateTime		GetFilterDateTimeValue(DbRecordSet recFilterViewData, int nFilterSetting);
	void 			FillInsertDefaultValues(DbRecordSet &recFilterSettings);
	void			SetupDateRangeSelector();
	void			UpdateRecordsetsWithChangedData(int nMsgCode, int nMsgDetail, const QVariant val);
	void			ReplaceDocumentData(DbRecordSet &recSource);

	int				m_nCurrentEntityID;				//Current entity ID (Person for Desktop, contact for Contact and project for Project fui).
	int 			m_nLoggedPersonID;				//Logged person ID.
	int 			m_nCurrentViewID;				//Current filter view ID.
	int				m_nGridType;					//Person, contact or project grid. See GridType enum. For view selection and view fui opening.
	DbRecordSet		m_recFilterViews;
	DbRecordSet		m_recFilterViewData;
	DbRecordSet		m_recEmail,
					m_recVoiceCall,
					m_recDocument;
	DbRecordSet		*m_recEntities;
	int 			m_nEntityID;
	int 			m_nTaskID;
	int 			m_nOwnerID;
	int 			m_nContactID; 
	int 			m_nProjectID;
	int 			m_nEntitySysType;
	QList<int>		m_lstTaskList;
	QList<int>		m_lstUnreadEmailsList;
	QList<int>		m_lstStartTaskEmailsCENT_IDs;
	QList<int>		m_lstStartTaskPhoneCallsCENT_IDs;
	int				m_nPolymorficID;
	Document_PieMenu m_PieMenu;
	DbRecordSet		m_lstSortColumnSetup;
	DbRecordSet		m_lstSortRecordSet;
	QString			m_strCurrentPersonInitials;
	QWidget			*m_pParentFUI;
	bool			m_bSideBarMode;
	bool			m_bIsCurrentFUIinSideBarMode;
	QProcess		m_procSplashScreen;
	bool			m_bStartSplashScreen;
	bool			m_bInitialized;

signals:
	void 			ContentChanged();
	void 			NeedNewData(int);
	void 			FilterChanged(QString strFilterName);
};

#endif // COMMWORKBENCHWIDGET_H
