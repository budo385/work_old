#include "maincommandbar.h"

MainCommandBar::MainCommandBar(QWidget *parent)
    : QFrame(parent)
{
	m_nMode=-1;
	ui.setupUi(this);

	//Set icons.
	ui.btnEdit->setIcon(QPixmap(":Edit24.png"));
	ui.btnInsert->setIcon(QPixmap(":Insert24.png"));
	ui.btnDelete->setIcon(QPixmap(":Delete24.png"));
	ui.btnCopy->setIcon(QPixmap(":Copy24.png"));

	QString style="QPushButton { font-size: 10pt; font-family: \"Arial\" }"; 
	this->setStyleSheet(style);
	m_pFuiBase = NULL;
}

void MainCommandBar::SetMode(int nMode)
{
	if(nMode==m_nMode) 
		return;				//if already in mode exit;
	else
		m_nMode=nMode;
	
	switch(nMode)
	{
		case MODE_READ:
			ui.btnEdit->setEnabled(true);
			ui.btnInsert->setEnabled(true);
			ui.btnDelete->setEnabled(true);
			ui.btnCopy->setEnabled(true);
			ui.btnOK->setEnabled(false);
			ui.btnCancel->setEnabled(false);
			break;

		case MODE_EDIT:
		case MODE_INSERT:

			ui.btnEdit->setEnabled(false);
			ui.btnInsert->setEnabled(false);
			ui.btnDelete->setEnabled(false);
			ui.btnCopy->setEnabled(false);
			ui.btnOK->setEnabled(true);
			ui.btnCancel->setEnabled(true);
			break;

		case MODE_EMPTY:

			ui.btnEdit->setEnabled(false);
			ui.btnInsert->setEnabled(true);
			ui.btnDelete->setEnabled(false);
			ui.btnCopy->setEnabled(false);
			ui.btnOK->setEnabled(false);
			ui.btnCancel->setEnabled(false);
			break;

	}
}


void MainCommandBar::SetButtonState(int nButton, bool bEnabled)
{
	switch(nButton)
	{
	case BUTTON_EDIT:
		ui.btnEdit->setEnabled(bEnabled);
		break;
	case BUTTON_INSERT:
		ui.btnInsert->setEnabled(bEnabled);
		break;
	case BUTTON_DELETE:
		ui.btnDelete->setEnabled(bEnabled);
	    break;
	case BUTTON_COPY:
		ui.btnCopy->setEnabled(bEnabled);
	    break;
	case BUTTON_OK:
		ui.btnOK->setEnabled(bEnabled);
		break;
	case BUTTON_CANCEL:
		ui.btnCancel->setEnabled(bEnabled);
		break;
	}
	

}

void MainCommandBar::SetButtonVisible(int nButton, bool bVisible)
{
	switch(nButton)
	{
	case BUTTON_EDIT:
		ui.btnEdit->setVisible(bVisible);
		break;
	case BUTTON_INSERT:
		ui.btnInsert->setVisible(bVisible);
		break;
	case BUTTON_DELETE:
		ui.btnDelete->setVisible(bVisible);
		break;
	case BUTTON_COPY:
		ui.btnCopy->setVisible(bVisible);
		break;
	case BUTTON_OK:
		ui.btnOK->setVisible(bVisible);
		break;
	case BUTTON_CANCEL:
		ui.btnCancel->setVisible(bVisible);
		break;
	}
}

void MainCommandBar::on_btnInsert_clicked()
{
	Q_ASSERT(NULL != m_pFuiBase);
	m_pFuiBase->on_cmdInsert();
}

void MainCommandBar::on_btnEdit_clicked()
{
	Q_ASSERT(NULL != m_pFuiBase);
	m_pFuiBase->on_cmdEdit();
}

void MainCommandBar::on_btnDelete_clicked()
{
	Q_ASSERT(NULL != m_pFuiBase);
	m_pFuiBase->on_cmdDelete();
}

void MainCommandBar::on_btnCopy_clicked()
{
	Q_ASSERT(NULL != m_pFuiBase);
	m_pFuiBase->on_cmdCopy();
}

void MainCommandBar::on_btnOK_clicked()
{
	Q_ASSERT(NULL != m_pFuiBase);
	m_pFuiBase->on_cmdOK();
}

void MainCommandBar::on_btnCancel_clicked()
{
	Q_ASSERT(NULL != m_pFuiBase);
	m_pFuiBase->on_cmdCancel();
}



QPushButton* MainCommandBar::GetButton(int nButton)
{
	switch(nButton)
	{
	case BUTTON_EDIT:
		return ui.btnEdit;
		break;
	case BUTTON_INSERT:
		return ui.btnInsert;
		break;
	case BUTTON_DELETE:
		return ui.btnDelete;
	    break;
	case BUTTON_COPY:
		return ui.btnCopy;
	    break;
	case BUTTON_OK:
		return ui.btnOK;
		break;
	case BUTTON_CANCEL:
		return ui.btnCancel;
		break;
	default:
		Q_ASSERT(false);
	    break;
	}
	
	return NULL;

}