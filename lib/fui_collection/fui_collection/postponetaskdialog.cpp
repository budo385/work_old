#include "postponetaskdialog.h"
#include <qDebug>

PostponeTaskDialog::PostponeTaskDialog(QDateTime &dateStart, QDateTime &dateDue, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	ui.pixConnect->setPixmap(QPixmap(":Connect.png"));

	ui.cboTimeAmount->addItem(QString("5 %1").arg(tr("min")));
	ui.cboTimeAmount->addItem(QString("10 %1").arg(tr("min")));
	ui.cboTimeAmount->addItem(QString("15 %1").arg(tr("min")));
	ui.cboTimeAmount->addItem(QString("30 %1").arg(tr("min")));
	ui.cboTimeAmount->addItem(QString("1 %1").arg(tr("h")));
	ui.cboTimeAmount->addItem(QString("2 %1").arg(tr("h")));
	ui.cboTimeAmount->addItem(QString("4 %1").arg(tr("h")));
	ui.cboTimeAmount->addItem(QString("6 %1").arg(tr("h")));
	ui.cboTimeAmount->addItem(QString("8 %1").arg(tr("h")));
	ui.cboTimeAmount->addItem(QString("1 %1").arg(tr("day")));
	ui.cboTimeAmount->addItem(QString("2 %1").arg(tr("days")));
	ui.cboTimeAmount->addItem(QString("1 %1").arg(tr("week")));
	ui.cboTimeAmount->addItem(QString("2 %1").arg(tr("weeks")));
	ui.cboTimeAmount->addItem(QString("3 %1").arg(tr("weeks")));
	ui.cboTimeAmount->addItem(QString("4 %1").arg(tr("weeks")));
	ui.cboTimeAmount->addItem(QString("1 %1").arg(tr("month")));
	ui.cboTimeAmount->addItem(QString("2 %1").arg(tr("months")));
	ui.cboTimeAmount->addItem(QString("6 %1").arg(tr("months")));
	ui.cboTimeAmount->addItem(QString("1 %1").arg(tr("year")));
	ui.cboTimeAmount->setCurrentIndex(-1);

	if(dateStart > QDateTime::currentDateTime())
		ui.chkShiftBoth->setChecked(true);

	ui.dateStart->useAsDateTimeEdit();
	ui.dateDue->useAsDateTimeEdit();

	//txtFromTime
	m_dateStartOrig = dateStart;
	m_dateDueOrig = dateDue;
	SetStartDate(dateStart);
	SetDueDate(dateDue);

	QDateTime dtNow = QDateTime::currentDateTime();
	if(dateDue.isValid())
		m_nDueStartDiff = dateStart.secsTo(dateDue);
	else
		m_nDueStartDiff = dateStart.secsTo(dtNow);
	qDebug() << "Date diff" << dateStart << "-" << dateDue << "is" << m_nDueStartDiff << "seconds";

	if(dateStart > dtNow){
		m_dateFrom = dateStart;
		RefdrawFromDate();
	}
	else{
		on_fromDate_UpdateTimer();
		connect(&m_fromUpdateTimer, SIGNAL(timeout()), this, SLOT(on_fromDate_UpdateTimer()));
		m_fromUpdateTimer.start(500);
	}

	connect(ui.cboTimeAmount, SIGNAL(currentIndexChanged(int)), this, SLOT(on_cboTimeAmount_selection(int)));
	connect(ui.dateStart, SIGNAL(dateTimeChanged(const QDateTime &)), this, SLOT(on_dateStart_changed(const QDateTime &)));
	connect(ui.dateDue, SIGNAL(dateTimeChanged(const QDateTime &)), this, SLOT(on_dateDue_changed(const QDateTime &)));
}

PostponeTaskDialog::~PostponeTaskDialog()
{
}

void PostponeTaskDialog::SetStartDate(const QDateTime &date)
{
	m_dateStart = date;
	ui.dateStart->blockSignals(true);
	ui.dateStart->setDateTime(date);
	ui.dateStart->blockSignals(false);
}

void PostponeTaskDialog::SetDueDate(const QDateTime &date)
{
	m_dateDue = date;
	ui.dateDue->blockSignals(true);
	ui.dateDue->setDateTime(date);
	ui.dateDue->blockSignals(false);
}

QDateTime PostponeTaskDialog::GetStartDate()
{
	return ui.dateStart->dateTime();
}

QDateTime PostponeTaskDialog::GetDueDate()
{
	return ui.dateDue->dateTime();
}

void PostponeTaskDialog::IncreaseDate(QDateTime &date, int nIndex, bool bForward)
{
	int nDirection = (bForward) ? 1 : -1;

	switch(nIndex){
		case 0:	//5 min
			date = date.addSecs(nDirection*5*60);
			break;
		case 1:	//10 min
			date = date.addSecs(nDirection*10*60);
			break;
		case 2:	//15 min
			date = date.addSecs(nDirection*15*60);
			break;
		case 3:	//30 min
			date = date.addSecs(nDirection*30*60);
			break;
		case 4:	//1 hour
			date = date.addSecs(nDirection*60*60);
			break;
		case 5:	//2 hours
			date = date.addSecs(nDirection*120*60);
			break;
		case 6:	//4 hours
			date = date.addSecs(nDirection*240*60);
			break;
		case 7:	//6 hours
			date = date.addSecs(nDirection*360*60);
			break;
		case 8:	//8 hours
			date = date.addSecs(nDirection*480*60);
			break;
		case 9:	//1 day
			date = date.addDays(nDirection*1);
			break;
		case 10:	//2 days
			date = date.addDays(nDirection*2);
			break;
		case 11: //1 week
			date = date.addDays(nDirection*7);
			break;
		case 12: //2 weeks
			date = date.addDays(nDirection*14);
			break;
		case 13: //3 weeks
			date = date.addDays(nDirection*21);
			break;
		case 14: //4 weeks
			date = date.addDays(nDirection*28);
			break;
		case 15: //1 month
			date = date.addMonths(nDirection*1);
			break;
		case 16: //2 months
			date = date.addMonths(nDirection*2);
			break;
		case 17: //6 months
			date = date.addMonths(nDirection*6);
			break;
		case 18: //1 year
			date = date.addYears(nDirection*1);
			break;
	}
}

void PostponeTaskDialog::on_cboTimeAmount_selection(int index)
{
	/*	Zdravo Miro,
Ovdje ti je algoritam:
So:= Start Datetime Original
Do:= Due Datetime Original
N:= Now Datetime
S:= Shift Period
C:= Combine Checkbox (true or false)
Sn:= New Start Datetime
Dn:= New Due Datetime

If Do=��
      Dn:= N + S
      If C 
          Sn:= Dn � S
	  End If
Else
      If Do<N
         Dn:= N + S
      Else
         Dn:= Do + S
      End If
      If C 
         If So=��
                Sn:= N
		 Else
                Sn:= Dn � (Do � So)
		 End If
      End If
End If

Pozdrav,
Marin
	*/
	QDateTime dateStart;
	QDateTime dateDue;

	if(!m_dateDueOrig.isValid())
	{
		dateDue = m_dateFrom;
		IncreaseDate(dateDue, index);

		dateStart = dateDue;
		if(ui.chkShiftBoth->isChecked())
			IncreaseDate(dateStart, index, false);
	}
	else
	{
		if(m_dateDueOrig < m_dateFrom)
		{
			dateDue = m_dateFrom;
			IncreaseDate(dateDue, index);
		}
		else{
			dateDue = m_dateDueOrig;
			IncreaseDate(dateDue, index);
		}

		if(ui.chkShiftBoth->isChecked()){
			if(!m_dateStartOrig.isValid())
				dateStart = m_dateFrom;
			else{
				dateStart = dateDue;
				dateStart = dateStart.addSecs(-1 * m_nDueStartDiff);
			}
		}
	}

	//set new dates
	if(ui.chkShiftBoth->isChecked())
		SetStartDate(dateStart);
	SetDueDate(dateDue);
}

void PostponeTaskDialog::on_dateStart_changed(const QDateTime &datetime)
{
	if(ui.chkShiftBoth->isChecked())
	{
		int nDiff = GetStartDate().secsTo(datetime);
		SetDueDate(GetDueDate().addSecs(nDiff));
	}
	else{
		if(GetStartDate() > GetDueDate())
			SetDueDate(GetStartDate());
	}
}

void PostponeTaskDialog::on_dateDue_changed(const QDateTime &datetime)
{
	if(ui.chkShiftBoth->isChecked())
	{
		int nDiff = GetDueDate().secsTo(datetime);
		SetStartDate(GetStartDate().addSecs(nDiff));
	}
	else{
		if(GetStartDate() > GetDueDate())
			SetStartDate(GetDueDate());
	}
}

void PostponeTaskDialog::on_btnOK_clicked()
{
	done(1);
	emit returnDates(GetDueDate(), GetStartDate());
}

void PostponeTaskDialog::on_btnCancel_clicked()
{
	done(0);
}

void PostponeTaskDialog::on_fromDate_UpdateTimer()
{
	//update time to the current
	QDateTime dtNow = QDateTime::currentDateTime();
	m_dateFrom = dtNow;

	RefdrawFromDate();
}

void PostponeTaskDialog::RefdrawFromDate()
{
	QString strFormat=QLocale::system().dateFormat(QLocale::ShortFormat) + " HH:mm:ss";
	ui.txtFromTime->setText(m_dateFrom.toString(strFormat));
}

