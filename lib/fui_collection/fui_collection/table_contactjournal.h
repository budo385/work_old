#ifndef TABLE_CONTACTJOURNAL_H
#define TABLE_CONTACTJOURNAL_H

#include <QTextEdit>
#include <QPushButton>
#include "toolbar_edit.h"
#include "gui_core/gui_core/universaltablewidgetex.h"


class Table_ContactJournal : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_ContactJournal(QWidget *parent);

	void Initialize(QTextEdit *m_pExtJournalEntry,QPushButton *btnEditJournalEntry,toolbar_edit *pToolBar);
	void Reload(int nContactID,bool bSkipRead=false,DbRecordSet *newRecord=NULL);
	void Invalidate();
public slots:
	void RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);

private slots:
	void OpenRTFEditor();		//when editing journal entry
	void OnEdit();
	void OnShow();
	void OnInsert();
	void OnDeleteSelected();
	void OnSelectionChanged();	
signals:
	void ContentChanged();
protected:
	void Data2CustomWidget(int nRow, int nCol);
private:
	void EnableToolbar(bool bEnable);
	void BuildToolBar(toolbar_edit* pToolBar);
	void CreateContextMenu();	
	
	QTextEdit *m_pExtJournalEntry;
	QPushButton *m_btnEditJournalEntry;
	int m_nJournalEntryIdx;  
	DbRecordSet m_lstJournal;
	int m_nContactID;
	toolbar_edit *m_toolbar;
};

#endif // TABLE_CONTACTJOURNAL_H

