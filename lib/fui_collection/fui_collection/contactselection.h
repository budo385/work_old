#ifndef CONTACTSELECTION_H
#define CONTACTSELECTION_H

#include <QtWidgets/QDialog>
#include "ui_contactselection.h"

#include "common/common/dbrecordset.h"

class ContactSelection : public QDialog
{
    Q_OBJECT

public:
    ContactSelection(QWidget *parent = 0);
    ~ContactSelection();

	void GetSelectedContacts(DbRecordSet &m_recToRecordSet,	DbRecordSet &m_recCCRecordSet, DbRecordSet &m_recBCCRecordSet);

private:
    Ui::ContactSelectionClass ui;

	DbRecordSet			m_recToRecordSet;
	DbRecordSet			m_recCCRecordSet;
	DbRecordSet			m_recBCCRecordSet;
	QFrame				*m_pContactWidget;

	void InitializeContacts();
	void GetMail(DbRecordSet &DroppedValue, DbRecordSet &recContainer);

private slots:
	void ToDataDroped(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event);
	void CCDataDroped(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event);
	void BCCDataDroped(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event);
	void on_AddTo_pushButton_clicked();
	void on_AddCC_pushButton_clicked();
	void on_AddBCC_pushButton_clicked();
	void on_Ok_pushButton_clicked();
	void on_Cancel_pushButton_clicked();
	void on_ToTable_SelectionChanged();
	void on_CCTable_SelectionChanged();
	void on_BCCTable_SelectionChanged();
	void on_selectToMail_pushButton_clicked();
	void on_selectCCMail_pushButton_clicked();
	void on_selectBCCMail_pushButton_clicked();
};

#endif // CONTACTSELECTION_H
