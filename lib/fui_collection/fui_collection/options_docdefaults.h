#ifndef OPTIONS_DOCDEFAULTS_H
#define OPTIONS_DOCDEFAULTS_H


#include <QWidget>
#include "generatedfiles/ui_options_docdefaults.h"
#include "settingsbase.h"


class Options_DocDefaults : public SettingsBase
{
	Q_OBJECT

public:
	Options_DocDefaults(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
	~Options_DocDefaults();

	DbRecordSet GetChangedValuesRecordSet();

	private slots:
		void OnSelectPath();
		void OnRemovePath();

private:
	Ui::Options_DocDefaultsClass ui;
};

#endif // OPTIONS_DOCDEFAULTS_H
