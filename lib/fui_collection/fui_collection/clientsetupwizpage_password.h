#ifndef CLIENTSETUPWIZPAGE_PASSWORD_H
#define CLIENTSETUPWIZPAGE_PASSWORD_H

#include "ui_clientsetupwizpage_password.h"
#include "gui_core/gui_core/wizardpage.h"


class ClientSetupWizPage_Password : public WizardPage
{
	Q_OBJECT

public:
	ClientSetupWizPage_Password(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
	~ClientSetupWizPage_Password();

	void Initialize();
	void resetPage();
	bool GetPageResult(DbRecordSet &RecordSet);

private slots:
	void on_txtPassword1_editingFinished();
	void on_txtPassword2_editingFinished();

private:
	Ui::ClientSetupWizPage_PasswordClass ui;

	//passwords:
	void LoadPassword();
	void ClearPassword();
	bool CheckPassword(QByteArray &newPassword);
	bool m_bPassWordChanged;
};

#endif // CLIENTSETUPWIZPAGE_PASSWORD_H
