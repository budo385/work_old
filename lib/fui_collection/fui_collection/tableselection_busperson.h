#ifndef TABLESELECTION_BUSPERSON_H
#define TABLESELECTION_BUSPERSON_H

#include <QObject>
#include "gui_core/gui_core/universaltablewidget.h"
#include "bus_core/bus_core/entity_id_collection.h"




/*!
	\class  TableSelection_BusPerson
	\brief  TableClass for BusPerson entity selection controller
	\ingroup GUICore

	Use:
	- initialize it with Initialize(), provide datasource
	- emits ReloadData() signal to reload entity data
	- if programatically reload, then RefreshDisplay()
*/
class TableSelection_BusPerson : public UniversalTableWidget
{
	Q_OBJECT

public:
    TableSelection_BusPerson(QWidget * parent);

	void Initialize(DbRecordSet * pData);
	int GetDropType(){return ENTITY_BUS_PERSON;};
	void CreateContextMenuActions(QList<QAction*>& lstActions);

private slots:
		void EmitReload(){emit ReloadData();};
signals:
		void ReloadData();


    
};

#endif // TABLESELECTION_BUSPERSON_H
