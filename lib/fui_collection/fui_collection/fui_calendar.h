#ifndef FUI_CALENDAR_H
#define FUI_CALENDAR_H

#include "fuibase.h"
#include "generatedfiles/ui_fui_calendar.h"

class Fui_Calendar : public FuiBase
{
	Q_OBJECT

public:
	Fui_Calendar(QWidget *parent = 0);
	~Fui_Calendar();

	void Initialize(QDate startDate, QDate endDate, DbRecordSet *recPersons=NULL, DbRecordSet *recProjects=NULL, DbRecordSet *recContacts=NULL, DbRecordSet *recResources=NULL, DbRecordSet *m_recEntitesFromPersonalSetting=NULL);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

protected:
	void showEvent(QShowEvent *event);

private:
	void InitializeCalendarWidgetAndGetCalendarItems(bool bForInsert = false, DbRecordSet recEntites = DbRecordSet(), bool bNoInitialize = false);
	//void InitializeCalendarWidget();
	void reorderEntitiesForAnesthesists();
	void GetCalendarData(int nRow, DbRecordSet &recEventParts, QList<int> &lstMultiPartEventsIDs, QDateTime &startDateTime, 
							QDateTime &endDateTime, int &nCentID, int &nBcepID, int &nBcevID, int &nBCOL_ID, int &nPersonID, 
							int &nContactID, QString &strTitle, QString &strLocation, QString &strColor, QPixmap &pixIcon, int &nBcep_Status, 
							int &nBCEV_IS_INFORMATION);
	//void GetCalendarItems(bool bForInsert = false, DbRecordSet recEntites = DbRecordSet());
	void GetMultiPartEvents(DbRecordSet recEventParts, QList<int> &lstMultiPartEventsIDs);
	void GetProposedEvents(DbRecordSet recEventParts, QList<int> &lstProposedEventPartIDs);
	void SortCalendarItems(int nEntityID, int nEntityType, int nEntityPersonID, DbRecordSet recEventParts, DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects, DbRecordSet recBreaks, bool bForInsert);
	void GetOwnerResProjParts(int nRow, DbRecordSet &recEventParts, int nCentID, int nContactID, int nBCOL_ID, QString &strOwner, QString &strResource, QString &strContact, QString &strProjects,
							QString &strDescription, QHash<int, qreal> &hshBrakeStart, QHash<int, qreal> &hshBrakeEnd, DbRecordSet &recContacts, 
							DbRecordSet &recResources, DbRecordSet &recProjects, DbRecordSet &recBreaks, DbRecordSet &recItemAssignedResources);
	void AddEvent(int nBcevID, int nEntityID, int nEntityType, int nEntityPersonID);
	void AddSingleDayEvent(int nRow, int nContactID, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID,
							QDateTime startDateTime, QDateTime endDateTime, QString strTitle, QString strLocation, QString strColor, 
							QPixmap pixIcon, DbRecordSet recEventParts, DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects, 
							DbRecordSet recBreaks, bool bForInsert, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed);
	void RemoveSingleDayEvent(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID);
	void RemoveMultiDayEvent(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID);
	void ModifyOneEvent(int nBcevID);
	void ModifySingleDayEvent(int nRow, DbRecordSet recEventParts, DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects, 
							DbRecordSet recBreaks, QList<int> lstMultiPartEventsIDs, QList<int> lstProposedEventPartIDs);
	void SplitMultiDayToSingleDays(int nRow, DbRecordSet recEventParts, DbRecordSet &recSplitedMultyDayEvent);
	void AddMultyDayEvent(bool bFromSingleDayEvent, int nRow, int nContactID, int nBCOL_ID, int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType, int nEntityPersonID, 
		QDateTime startDateTime, QDateTime endDateTime, QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, 
		DbRecordSet recEventParts, DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects, 
		DbRecordSet recBreaks, bool bForInsert, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed);
	void AddMultyDayToSingleDayCalendar(bool bFromSingleDayEvent, int nRow, DbRecordSet recEventParts, int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType, 
		QDateTime startDateTime, QDateTime endDateTime);
	void RemoveMultyDayFromSingleDayCalendar(int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType);
	void AddCalendarReservations(int nEntityID, int nEntityType, DbRecordSet recReservationsRecalculated);
	void AddCalendarTaskItems();
	void SplitMultiDayReservationToSingleDay(int nRow, DbRecordSet recReservationsRecalculated, DbRecordSet &recSplitedReservationsRecalculated);
	QString GetContactsForPart(int nCentID, DbRecordSet &recContacts, int nOwnerContactID, DbRecordSet &recItemAssignedResources);
	QString GetResourcesForPart(int nCentID, DbRecordSet &recResources, DbRecordSet &recItemAssignedResources);
	QString GetProjectsForPart(int nCentID, DbRecordSet &recProjects, DbRecordSet &recItemAssignedResources);
	void CreateBrakesHashes(int nBCOL_ID, QDateTime startDateTime, QDateTime endDateTime, DbRecordSet recBreaks, QHash<int, qreal> &hshBrakeStart, QHash<int, qreal> &hshBrakeEnd);
	bool EntityAlreadyExist(int nEntityID, int nEntityType);
	void addEntity(int nEntityID, int nEntityType, QString strName, int nPersonID = -1);
	void removeCurrentEntity();
	void enableDisableButtons();
	void setLabelsAndSapne();
	void ReorderEntitiesRecordSet();
	void SetEntityToSecondRow(int nDroppedEntityID, int nDroppedEntityType, int nDroppedEntityPersonID);
	void SetupDateIntervalLabel(int nRange);
	void GetFilterRecordset(DbRecordSet &recFilter);
	void LoadViewSettings(int nViewID = -1);
	void LoadViewComboBox(int nViewID);
	void CreateEntitiesRecordSets(DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects);
	void ReorderEntityRecordSets(int nCurrentEntityRow, int nEntityID, int nEntityType);

	Ui::Fui_CalendarClass ui;

	int					m_nDateRange;
	DbRecordSet			m_recEntites;			//Entities recordset used for display.
	DbRecordSet			m_recFixedEntites;		//Entities recordset - fixed keeps list of entities.
	DbRecordSet			m_recEntitesFromPersonalSetting; //Entities recordset - fixed keeps list of entities.
	int					m_nViewID;				//Current view ID.
	DbRecordSet			m_recCalendarViews;		//Views recordset - keeps list of views.
	//DbRecordSet			m_recCalendarViews;		//Views recordset - keeps list of views.
	QDateTime			m_datFrom;
	QDateTime			m_datTo;
	CalendarWidget::timeScale m_TimeScale;
	QTime				m_startScrollToTime;
	int					m_nCurentRow;			//Which entity in entity order recordset is now.
	SelectionPopup		m_dlgUserPopUp;
	SelectionPopup		m_dlgProjectPopUp;
	SelectionPopup		m_dlgContactPopUp;
	SelectionPopup		m_dlgResourcePopUp;
	bool				m_bResizeColumnsToFit;
	QPoint				m_ptFuiPosition;
	bool				m_bShowTasks;
	int					m_nCalendarViewSections;	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER.

private slots:
	void on_onDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange, bool bResizeColumns);
	void on_DateChanged(QDate date);

	void on_rangeFrameRangeTimeIntervalChanged(int nTimeScale);

	void on_delete_toolButton_clicked();
	void on_up_toolButton_clicked();
	void on_down_toolButton_clicked();
	void setDroppedEntityOnSecondRow(int nDroppedEntityID, int nDroppedEntityType, int nDroppedEntityPersonID);
	void on_showAssignations_toolButton_clicked();

	void on_addUser_toolButton_clicked();
	void on_addProject_toolButton_clicked();
	void on_addContact_toolButton_clicked();
	void on_addResource_toolButton_clicked();
	void on_reload_toolButton_clicked();
	void on_saveView_toolButton_clicked();
	void saveView(int nViewID, QString strViewName, bool bIsPublic);
	void on_viewSelect_comboBox_currentIndexChanged(int index);
	void on_deleteView_toolButton_clicked();

	void on_ItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void on_MultiDayItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void on_ItemMoved(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void on_MultyDayItemMoved(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void on_DeleteItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType);
	void on_ItemsDelete(DbRecordSet recSelectedItemsData);
	void on_MultyDayDeleteItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void on_ModifyItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void on_ViewItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void on_DropEntityOnItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, bool bDroppedOnFirstRow);
	
	void on_InsertNewItem(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType);
	void on_selectionChanged(QDateTime datSelectionFrom, QDateTime datSelectionTo, DbRecordSet recSelectedEntities);
	void on_openMultiDayEvent(QHash<int, int> hshMultiDayCentIDs, QHash<int, int> hshMultiDayBcepIDs, QHash<int, int> hshMultiDayBcevIDs);
	void on_ShowInformationEvents(bool bIsInformation);
	void on_ShowPreliminaryEvents(bool bIsPreliminary);
	void on_FilterTypeChanged(QList<int> lstTypeIDs);

	void on_TaskItemClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID);
	void on_TaskItemDoubleClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID);

	void on_showTasks_checkBox_stateChanged(int state);

	void on_defineAvailability(DbRecordSet recSelectedEntityItemsInLeftColumn);
	void on_markAsFinal(DbRecordSet recSelectedEntityItemsInLeftColumn);
	void on_markAsPreliminary(DbRecordSet recSelectedEntityItemsInLeftColumn);

	void on_dropEntityOnTable(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nDroppedEntityID, int nDroppedEntityType, int nDroppedEntityPersonID, QDateTime datSelectionFrom, QDateTime datSelectionTo);
};

#endif // FUI_CALENDAR_H
