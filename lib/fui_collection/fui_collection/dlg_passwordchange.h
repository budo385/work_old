#ifndef DLG_PASSWORDCHANGE_H
#define DLG_PASSWORDCHANGE_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_passwordchange.h"

class Dlg_PasswordChange : public QDialog
{
	Q_OBJECT

public:
	Dlg_PasswordChange(QWidget *parent = 0);
	~Dlg_PasswordChange();

	void SetUser(QString strUserName);

private slots:
	void on_txtPassword1_editingFinished();
	void on_txtPassword2_editingFinished();
	void on_btnCancel_clicked();
	void on_btnOK_clicked();

private:
	Ui::Dlg_PasswordChangeClass ui;

	//passwords:
	void LoadPassword();
	void ClearPassword();
	bool CheckPassword(QByteArray &newPassword);
	bool m_bPassWordChanged;
};

#endif // DLG_PASSWORDCHANGE_H
