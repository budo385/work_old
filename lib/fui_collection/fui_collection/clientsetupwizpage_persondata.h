#ifndef CLIENTSETUPWIZPAGE_PERSONDATA_H
#define CLIENTSETUPWIZPAGE_PERSONDATA_H

#include "gui_core/gui_core/wizardpage.h"
#include "ui_clientsetupwizpage_persondata.h"
#include "bus_core/bus_core/formatphone.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include <QMenu>
#include "gui_core/gui_core/guifieldmanager.h"


class ClientSetupWizPage_PersonData : public WizardPage
{
	Q_OBJECT

public:
	ClientSetupWizPage_PersonData(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
	~ClientSetupWizPage_PersonData();
	void Initialize();
	void resetPage();
	bool GetPageResult(DbRecordSet &RecordSet);

private:
	Ui::ClientSetupWizPage_PersonDataClass ui;

	void SetMenuEmailTypes(QMenu *menu);
	void SetMenuPhoneTypes(QMenu *menu);
	void FormatPhoneNumber(int nRow);
	QVariant GetTypeFromName(QString strName,int nEntity);
	void SetupPhone(QString strPhone, DbRecordSet *data, int nRow, bool bIsSkype=false);
	void SaveFromGUI();

	GuiFieldManager *m_GuiFieldManagerMain;
	QMenu m_MenuEmail_1;
	QMenu m_MenuEmail_2;
	QMenu m_MenuPhone_1;
	QMenu m_MenuPhone_2;
	QMenu m_MenuPhone_3;

	MainEntitySelectionController m_Types;
	FormatPhone m_PhoneFormatter;
	DbRecordSet m_lstData;
	DbRecordSet m_lstPhones;
	DbRecordSet m_lstEmails;
	DbRecordSet m_lstInternet;
	DbRecordSet m_lstAddress;

private slots:
	void on_btnAddress_clicked();
	void on_btnFormatPhone1_clicked();
	void on_btnFormatPhone2_clicked();
	void on_btnFormatPhone3_clicked();

	void OnMenuTriggerEmail1(QAction *action);
	void OnMenuTriggerEmail2(QAction *action);
	void OnMenuTriggerPhone1(QAction *action);
	void OnMenuTriggerPhone2(QAction *action);
	void OnMenuTriggerPhone3(QAction *action);

	void FieldChanged(const QString & text);
	void OnSetFocusDelayed();

};

#endif // CLIENTSETUPWIZPAGE_PERSONDATA_H
