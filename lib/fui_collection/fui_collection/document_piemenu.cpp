#include "document_piemenu.h"

#include "bus_core/bus_core/globalconstants.h"
#include <QIcon>
#include <QObject>
#include <QtWidgets/QMessageBox>
#include "fui_dm_documents.h"
#include "bus_client/bus_client/dlg_storagelocation.h"

//global FUI manager:
#include "fuimanager.h"
extern FuiManager g_objFuiManager;	

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester


Document_PieMenu::Document_PieMenu()
:m_parent(NULL)
{


	m_bSelectionMode=false;
	m_nOperation=-1;

	//build menu:
	m_pPieMenu = new QtPieMenu("Root menu", NULL, "Root menu");

	m_pPieMenu->insertItem(QIcon(":Local_File_32.png"),tr("Local File "),this,SLOT(OnLocalFile()));
	m_pPieMenu->insertItem(QIcon(":Internet_File_32.png"),tr("Internet File"),this,SLOT(OnInternetFile()));
	m_pPieMenu->insertItem(QIcon(":Note_32.png"),tr("Note"),this,SLOT(OnNote()));
	m_pPieMenu->insertItem(QIcon(":Address_32.png"),tr("Address"),this,SLOT(OnAddress()));
	m_pPieMenu->insertItem(QIcon(":Paper_Docs_32.png"),tr("Paper Document"),this,SLOT(OnPaperDoc()));
	m_pPieMenu->setOuterRadius(125);
	//m_pPieMenu->setInnerRadius(30);

	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_LOCAL, nValue))
	{
		m_pPieMenu->setItemEnabled(false,0);
	}
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_INTERNET, nValue))
	{
		m_pPieMenu->setItemEnabled(false,1);
	}
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_NOTE, nValue))
	{
		m_pPieMenu->setItemEnabled(false,2);
	}
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_ADDRESS, nValue))
	{
		m_pPieMenu->setItemEnabled(false,3);
	}
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_PAPER, nValue))
	{
		m_pPieMenu->setItemEnabled(false,4);
	}

}

Document_PieMenu::~Document_PieMenu()
{
	delete m_pPieMenu;
}




void Document_PieMenu::SelectDocumentType(QWidget *pWidgetSource, int nOperation)
{
	m_bSelectionMode=true;
	m_nOperation=nOperation;
	QPoint pos = pWidgetSource->mapToGlobal(QPoint(pWidgetSource->width()/2,15)); //somewhere on half:
	m_pPieMenu->popup(pos);
}


void Document_PieMenu::CreateNewDocument(QWidget *pWidgetSource,DbRecordSet *rowDefault,DbRecordSet *lstContacts,DbRecordSet *lstProjects, bool bScheduleTask, bool bIsTemplate, DbRecordSet *recRevisionForInsert)
{
	m_bSelectionMode=false;
	if (recRevisionForInsert)
		m_recRevisionForInsert=*recRevisionForInsert;

	if (rowDefault)
		m_rowDefault=*rowDefault;

	if (lstContacts)
		m_lstContacts=*lstContacts;

	if (lstProjects)
		m_lstProjects=*lstProjects;

	m_bScheduleTask=bScheduleTask;
	m_bIsTemplate=bIsTemplate;



	QPoint pos = pWidgetSource->mapToGlobal(QPoint(pWidgetSource->width()/2,15)); //somewhere on half:
	//m_pPieMenu->setParent(pWidgetSource);
	QCoreApplication::processEvents();
	m_pPieMenu->popup(pos);
}


void Document_PieMenu::OnLocalFile()
{
	if (m_bSelectionMode)
		emit DocumentTypeSelected(m_nOperation,GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE);
	else
		OpenFUI(GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE);
}
void Document_PieMenu::OnInternetFile()
{
	if (m_bSelectionMode)
		emit DocumentTypeSelected(m_nOperation,GlobalConstants::DOC_TYPE_INTERNET_FILE);
	else
		OpenFUI(GlobalConstants::DOC_TYPE_INTERNET_FILE);
}

void Document_PieMenu::OnNote()
{
	if (m_bSelectionMode)
		emit DocumentTypeSelected(m_nOperation,GlobalConstants::DOC_TYPE_NOTE);
	else
		OpenFUI(GlobalConstants::DOC_TYPE_NOTE);
}
void Document_PieMenu::OnAddress()
{
	if (m_bSelectionMode)
		emit DocumentTypeSelected(m_nOperation,GlobalConstants::DOC_TYPE_URL);
	else
		OpenFUI(GlobalConstants::DOC_TYPE_URL);
}
void Document_PieMenu::OnPaperDoc()
{
	if (m_bSelectionMode)
		emit DocumentTypeSelected(m_nOperation,GlobalConstants::DOC_TYPE_PHYSICAL);
	else
		OpenFUI(GlobalConstants::DOC_TYPE_PHYSICAL);
}



void Document_PieMenu::OpenFUI(int nDocType)
{

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EMPTY, -1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_DM_Documents* pFuiDM=dynamic_cast<FUI_DM_Documents*>(pFUI);
	if (pFuiDM)
	{
		pFuiDM->SetDefaults(nDocType,&m_rowDefault,&m_lstContacts,&m_lstProjects,m_bScheduleTask,m_bIsTemplate,&m_recRevisionForInsert);  
		pFuiDM->show();  //show FUI
	}

	if (m_parent && !m_bIsTemplate) //if template do not close whatever
	{
		m_parent->close();
	}
	
}

