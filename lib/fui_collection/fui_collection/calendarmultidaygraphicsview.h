#ifndef CALENDARMULTIDAYGRAPHICSVIEW_H
#define CALENDARMULTIDAYGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QtWidgets/QTableWidget>
#include "calendarwidget.h"
#include "calendargraphicsscene.h"
#include "calendarheadertableitem.h"
#include "calendarheaderitem.h"
#include "bus_client/bus_client/changemanager.h"
#include "common/common/entity_id_collection.h"

class TopLeftItem : public QGraphicsProxyWidget
{
	Q_OBJECT

public:
	TopLeftItem(int nHeight, QGraphicsView *pCalendarGraphicsView, QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
	~TopLeftItem();

public slots:
	void on_UpdateCalendar();

protected:

private:
	void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	bool isInResizeArea(const QPointF &pos);

	bool					m_isResizing;
	QGraphicsView			*m_pCalendarGraphicsView;
	Label					*m_pLabel;
	int						m_nHeight;
	int						m_nWidth;
	int						m_nMinWidth;

signals:
	void vertHeaderResizing(int nWidth);
	void vertHeaderResized(int nWidth);
};

class VerticalPersonItemDisplayWidget : public QFrame
{
	Q_OBJECT

public:
	VerticalPersonItemDisplayWidget(QString strEntityName, QWidget *parent=0);

	void setText(QString strText);
	void setSelected(bool bSelected);
	void resizeTable(int nWidth, int nHeight);

protected:
private:
	QTableWidgetItem *m_pItem1;
	QTableWidget *m_pTable;
};

class VerticalPersonItem : public QGraphicsProxyWidget , public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	VerticalPersonItem(int nRow, int nEntityID, int nEntityType, int nEntityPersonID, QString strText, QGraphicsView *pCalendarMultiDayGraphicsView, CalendarHeaderTableItem *pCalendarHeaderTableItem);
	~VerticalPersonItem();
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

	bool eventFilter(QObject *watched, QEvent *event);
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void setSelected(bool bSelected);

protected:
	void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

public slots:
	void on_UpdateCalendar();

private:
	int							 m_nRow;
	int							 m_nEntityID;
	int							 m_nEntityType;
	int							 m_nEntityPersonID;
	QString						 m_strText;
	QGraphicsView				 *m_pCalendarMultiDayGraphicsView;
	CalendarHeaderTableItem		 *m_pCalendarHeaderTableItem;
	VerticalPersonItemDisplayWidget *m_pWidg;
	bool						 m_bSelected;
};

class MultiDayCalendarHeaderItem : public CalendarHeaderItem
{
	Q_OBJECT
public:
	MultiDayCalendarHeaderItem(bool bIsInMultiDayCalendar, int nColumn, QString	strString, int nHeight, QGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent = 0, Qt::WindowFlags wFlags = 0);
	void ConnectSignals();

protected:
	void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
	void dragLeaveEvent(QGraphicsSceneDragDropEvent *event);
	void dragMoveEvent(QGraphicsSceneDragDropEvent *event);
	void dropEvent(QGraphicsSceneDragDropEvent *event);

public slots:
	void on_UpdateCalendar();
};

class CalendarHeaderTableModel : public QAbstractTableModel
{
public:
	CalendarHeaderTableModel(QObject *parent = 0);
	~CalendarHeaderTableModel();

	void Initialize(QDate startDate, int nDayInterval, DbRecordSet *recEntites);

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	int getRowForEntity(int nEntityID, int nEntityType);
	QDateTime getItemStartDateTime(QModelIndex &index);
	QDateTime getItemEndDateTime(QModelIndex &index);
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	//Item access methods.
	void getItem(int nEntityID, int nEntityType, QDateTime &dateTime, QModelIndex &Index);
	void getEntityForRow(int nRow, int nColumn, int &nEntityID, int &nEntityType, int &nEntityPersonID);

protected:
private:
	int getColumnForDate(QDate &date);

	QDate		m_startDate;
	int			m_nDayInterval;
	DbRecordSet *m_recEntites;		//Entities recordset.

//	CalendarTableModel *m_pCalendarTableModel;
};


class CalendarMultiDayGraphicsView : public QGraphicsView
{
	Q_OBJECT

public:
	CalendarMultiDayGraphicsView(CalendarWidget *pCalendarWidget, QWidget *parent = 0);
	~CalendarMultiDayGraphicsView();

	void Initialize(QDate &startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites, bool bResizeColumnsToFit = false);

	void AddDateHeader();
	CalendarHeaderTableItem* getTableView();
	CalendarHeaderTableModel* TableModel(){return m_pCalendarHeaderTableModel;};
	int getVertHeaderWidth();
	void setVertHeaderWidth(int nWidth);
	int getEntityCount();
	int getHeaderHeight();
	int getItemHeight(){return m_nRowHeightIncrement;};
	int getMultiHeaderHeight(){return m_pCalendarWidget->getMultiHeaderHeight();};
	int	getCurrentMultiHeaderHeight(){return m_pCalendarWidget->getCurrentMultiHeaderHeight();};
	int	getDateHeaderHeight(){return m_pCalendarWidget->getDateHeaderHeight();};
	void AddMultiDayCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, QDateTime startDateTime, QDateTime endDateTime, 
		QDateTime correctedStartDateTime, QDateTime correctedEndDateTime, QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, 
		QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, int nBcep_Status, 
		int nBCEV_IS_INFORMATION, bool bIsProposed, QGraphicsItem* &Item);
	void UpdateMultiDayCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime startDateTime, QDateTime endDateTime, 
		QDateTime correctedStartDateTime, QDateTime correctedEndDateTime, QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, 
		QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, int nBcep_Status, 
		int nBCEV_IS_INFORMATION, bool bIsProposed, QGraphicsItem* &Item);
	void RemoveCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QList<QGraphicsItem*> &Items);
	void AddMultiDayCalendarReservation(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, QDateTime startDateTime, 
		QDateTime endDateTime);
	void RemoveAllCalendarItems();
	void CheckForMultipleItemsInRow(QGraphicsItem *newItem);
	bool ItemExists(int nCentID, int nEntityID, int nEntityType, int nBCOL_ID);
	int  GetColumnWidth(){return m_nColumnWidth;};
	
	QMultiHash<int, QGraphicsItem*> GetMultiDayEventItems(){return m_lstMultiDayCalendarEntityItems;};

	void UpdateCalendarSignal();
	void UpdateCalendarSignalFromOutside();

	void SelectDeselectEntityItemsInLeftColumn(bool bSelected, int nEntityID, int nEntityType, int nEntityPersonID, QString nEntityName);

protected:
	void resizeEvent(QResizeEvent *event);
	void showEvent(QShowEvent *event);

public slots:
	void ResizeColumnsToFit(bool bForce = false);
	void on_UpdateCalendar();
	void on_scrollBar_valueChanged(int value);
	void on_horizontalScrollBarVisible(bool bVisible);
	void on_verticalScrollBarVisible(bool bVisible);
	void on_HscrollBar_valueChanged(int value);
	void on_VscrollBar_valueChanged(int value);
	void on_CustomVscrollBar_valueChanged(int value);
	void on_CustomHscrollBar_valueChanged(int value);
	void on_VscrollBar_rangeChanged(int min, int max);
	void on_HscrollBar_rangeChanged(int min, int max);
	void on_horizontalScrollBar_sliderMoved(int value);
	void on_verticalScrollBar_sliderMoved(int value);

	void on_columnResizedFromSimpleCalendar(int nColumn, int nWidth, bool bResizeAll);
	void on_columnResized(int nColumn, int nWidth, bool bResizeAll);

	void on_ItemModify(QGraphicsProxyWidget *thisItem);
	void on_DropEntityOnItem(QGraphicsProxyWidget *thisItem, int nEntityID, int nEntityType, int nEntityPersonID);
	void on_ItemDelete(QGraphicsProxyWidget *thisItem);
	void on_ItemResizing(QGraphicsProxyWidget *thisItem){};
	void on_ItemResized(QGraphicsProxyWidget *thisItem, bool bLeftBorder);
	void on_ItemMoved(QGraphicsProxyWidget *thisItem);
	void on_tableCellDoubleClicked(int row, int column);
	void on_SelectionChanged(QModelIndex topLeftSelectedIndex, QModelIndex bottomRightSelectedIndex);

	void on_defineAvailability();
	void on_markAsFinal();
	void on_markAsPreliminary();
	void on_selectAll();
	void on_deselectAll();

	void on_dropEntityOnTable(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nDroppedEntityID, 
		int nDroppedEntityType, int nDroppedEntityPersonID, QDateTime datSelectionFrom, QDateTime datSelectionTo);
	void on_dropEntityOnHeader(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nColumn);

private slots:
	void on_vertHeaderResized(int nWidth);

private:
	void CreateCalendarHeader();
	void CreateEntityItems();
	void DestroyCalendarHeader();
	void DestroyPersonItems();
	void PositionCornerScrollBarItems();
	void CreateCornerScrollBarItems();
	void PositionCustomScrollBars();
	void GetColidedItems(QGraphicsItem *Item, QList<QGraphicsItem*> &colidedItems, bool bThisItemOnly);
	void GetColidedItemsForRemove(QGraphicsItem *Item, QList<QGraphicsItem*> &colidedItems);
	void SortColidedItems(QList<QGraphicsItem*> &colidedItems);
	void GetItemsPos(QList<QGraphicsItem*> colidedItems);
	bool ItemsColide(QGraphicsItem *Item1, QGraphicsItem *Item2);
	void CheckForMultipleItemsInRowForRemove(QGraphicsItem *Item);
	void CheckItemDateTimeRange(QDateTime &startDateTime, QDateTime &endDateTime);
	bool AllColumnsAreInViewArea(int nViewportWidth);

	CalendarWidget			 *m_pCalendarWidget;
	CalendarHeaderTableModel *m_pCalendarHeaderTableModel;
	CalendarGraphicsScene	 *m_pGraphicsScene;
	CalendarHeaderTableItem	 *m_pCalendarHeaderTableView;
	QGraphicsProxyWidget	 *m_pBackgroundItem;
	QList<QGraphicsItem*>	 m_lstHeaderItems;
	QGraphicsProxyWidget	 *m_pLeftScrollBarItem;
	QGraphicsProxyWidget	 *m_pTopRightScrollBarItem;
	QGraphicsProxyWidget	 *m_pTopLeftScrollBarItem;
	QGraphicsProxyWidget	 *m_pRightDownScrollBarItem;
	QGraphicsProxyWidget	 *m_pTopLeftItem;
	QGraphicsProxyWidget	 *m_pHScrollBar;
	QScrollBar				 *m_pHScroll;
	QGraphicsProxyWidget	 *m_pVScrollBar;
	QScrollBar				 *m_pVScroll;
	DbRecordSet				 *m_recEntites;		//Entities recordset.
	QMultiHash<int, QGraphicsItem*> m_lstMultiDayCalendarEntityItems;
	//QHash<int, QGraphicsItem*> m_hshOrderOfItems;
	QList<QGraphicsItem*>	 m_lstMultiDayVerticalPersonHeaderItems;
	int						 m_nRowHeightIncrement;
	int						 m_nRowHeight;
	QMultiHash<int, QGraphicsItem*> m_lstCalendarMultiDayReservationItems;
	bool					 m_bResizeFromShowEvent;
	bool					 m_bFirstTimeShow;
	int						 m_nColumnWidth;
	int						 m_nEntityColumnWidth;
	DbRecordSet				 m_recSelectedEntityItemsInLeftColumn;

signals:
	void UpdateCalendar();
	void ItemModify(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void DropEntityOnItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, bool bDroppedOnFirstRow);
	void ItemDelete(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void MultiDayItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, 
		int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void MultiDayItemMoved(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, 
		int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void tableCellDoubleClicked(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType);
};

#endif // CALENDARMULTIDAYGRAPHICSVIEW_H
