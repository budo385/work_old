#ifndef QC_LANGUAGEFRAME_H
#define QC_LANGUAGEFRAME_H

#include <QWidget>
#include "ui_qc_languageframe.h"
#include "common/common/dbrecordset.h"

class QC_LanguageFrame : public QWidget
{
	Q_OBJECT

public:
	QC_LanguageFrame(QWidget *parent = 0);
	~QC_LanguageFrame();

	void Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType = 0 /*0-Large,1-small widget*/);

private:
	Ui::QC_LanguageFrameClass ui;

	void SetupGUIwidgets(DbRecordSet recData);
	void SetDataToRecordSet(QString strRecordSetFieldName, QString strValue);

	DbRecordSet *m_recFullContactData;
	DbRecordSet m_recLocalEntityRecordSet;
	DbRecordSet m_recAddressTypes;
	int			m_nParentType;
	QWidget		*m_pParentWidget;

private slots:
	void on_language_comboBox_editTextChanged(const QString &text);
};

#endif // QC_LANGUAGEFRAME_H
