#include "dlg_ftpservers.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "trans/trans/ftpclient.h"
#include <QtWidgets/QMessageBox>
#include "gui_core/gui_core/macros.h"

Dlg_FTPServers::Dlg_FTPServers(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_FTP_SERVERS));
	m_lstDataDeleted.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_FTP_SERVERS));
	connect(ui.lstConnections,SIGNAL(currentItemChanged ( QListWidgetItem *, QListWidgetItem * )  ),this,SLOT(OnItemChanged(QListWidgetItem *, QListWidgetItem *)));
	m_nCurrRow=-1;
	
	ui.cboProxyType->addItem(tr("Socks5 Proxy"),QNetworkProxy::Socks5Proxy);
	ui.cboProxyType->addItem(tr("Http  Proxy"),QNetworkProxy::HttpProxy);
	ui.tabWidget->setEnabled(false);
}

Dlg_FTPServers::~Dlg_FTPServers()
{
	if (!m_strLockedRes.isEmpty())
	{
		Status pStatus;
		bool bOK;
		_SERVER_CALL(ClientSimpleORM->UnLock(pStatus, CORE_FTP_SERVERS,bOK,m_strLockedRes))
			//_CHK_ERR(pStatus);
	}

}

//load server list,
bool Dlg_FTPServers::Initialize(QString strInitial)
{

	Status pStatus;
	_SERVER_CALL(ClientSimpleORM->Read(pStatus, CORE_FTP_SERVERS, m_lstData))
	_CHK_ERR_RET_BOOL_ON_FAIL(pStatus);

	//lock all reads:
	if (m_lstData.getRowCount()>0)
	{
		Status pStatus;
		DbRecordSet lstStatusRows;
		_SERVER_CALL(ClientSimpleORM->Lock(pStatus, CORE_FTP_SERVERS, m_lstData,m_strLockedRes, lstStatusRows))
		_CHK_ERR_RET_BOOL_ON_FAIL(pStatus);
	}

	m_lstData.sort("CFTP_NAME");

	//set on first:
	if (m_lstData.getRowCount()>0)
	{
		m_nCurrRow=0;
		if (!strInitial.isEmpty())
		{
			m_nCurrRow=m_lstData.find("CFTP_NAME",strInitial,true,false,true,true);
		}
	}

	RefreshDisplay();

	return true;
}



void Dlg_FTPServers::RefreshDisplay()
{
	//from data to list
	int nRowCurOld=ui.lstConnections->currentRow();
	ui.lstConnections->blockSignals(true);
	ui.lstConnections->clear();
	int nSize=m_lstData.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.lstConnections->addItem(m_lstData.getDataRef(i,"CFTP_NAME").toString());
	}
	ui.lstConnections->blockSignals(false);


	//save previous data:
	SaveData(nRowCurOld);

	//if current row -> refresh display:
	if (m_nCurrRow>=0 &&m_nCurrRow<ui.lstConnections->count())
	{
		ui.lstConnections->setCurrentRow(m_nCurrRow);
		ui.tabWidget->setEnabled(true);
	}
	else
	{
		m_nCurrRow=-1;
		ui.lstConnections->setCurrentRow(-1);
		ui.tabWidget->setEnabled(false);
	}

	//m_nCurrRow=nRowCur;

}


void Dlg_FTPServers::OnItemChanged(QListWidgetItem * current, QListWidgetItem * previous)
{
	if (previous)
	{
		SaveData(m_nCurrRow);
	}

	//set new data:
	m_nCurrRow=ui.lstConnections->currentRow();

	if (current && m_nCurrRow >=0)
	{
		ui.editHostName->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_HOST").toString());
		ui.editPort->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_PORT").toString());
		ui.editUser->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_USER").toString());
		ui.editPass->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_PASSWORD").toString());
		ui.editConnName->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_NAME").toString());
		ui.chkUsePassive->setChecked(m_lstData.getDataRef(m_nCurrRow,"CFTP_IS_PASSIVE_MODE").toInt());
		ui.editDir->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_START_DIRECTORY").toString());

		//proxy:
		ui.chkUseProxy->setChecked(m_lstData.getDataRef(m_nCurrRow,"CFTP_USE_PROXY").toInt());
		ui.editProxyHost->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_PROXY_HOST").toString());
		ui.editProxyPort->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_PROXY_PORT").toString());
		ui.editProxyUser->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_PROXY_USER").toString());
		ui.editProxyPass->setText(m_lstData.getDataRef(m_nCurrRow,"CFTP_PROXY_PASSWORD").toString());
		
		EnableProxyFields(ui.chkUseProxy->isChecked());

		int nIndex=ui.cboProxyType->findData(m_lstData.getDataRef(m_nCurrRow,"CFTP_PROXY_TYPE").toInt());
		ui.cboProxyType->setCurrentIndex(nIndex);

		ui.tabWidget->setEnabled(true);
	}

}

void Dlg_FTPServers::SaveData(int nRow)
{
	if (nRow<0 || nRow>m_lstData.getRowCount()-1)
		return;
	m_lstData.setData(nRow,"CFTP_HOST",ui.editHostName->text());
	m_lstData.setData(nRow,"CFTP_PORT",ui.editPort->text().toInt());
	m_lstData.setData(nRow,"CFTP_USER",ui.editUser->text());
	m_lstData.setData(nRow,"CFTP_PASSWORD",ui.editPass->text());
	m_lstData.setData(nRow,"CFTP_NAME",ui.editConnName->text());
	m_lstData.setData(nRow,"CFTP_IS_PASSIVE_MODE",(int)ui.chkUsePassive->isChecked());
	m_lstData.setData(nRow,"CFTP_START_DIRECTORY",ui.editDir->text());
	
	//proxy:
	m_lstData.setData(nRow,"CFTP_USE_PROXY",(int)ui.chkUseProxy->isChecked());
	m_lstData.setData(nRow,"CFTP_PROXY_HOST",ui.editProxyHost->text());
	m_lstData.setData(nRow,"CFTP_PROXY_PORT",ui.editProxyPort->text().toInt());
	m_lstData.setData(nRow,"CFTP_PROXY_USER",ui.editProxyUser->text());
	m_lstData.setData(nRow,"CFTP_PROXY_PASSWORD",ui.editProxyPass->text());
	m_lstData.setData(nRow,"CFTP_PROXY_TYPE",ui.cboProxyType->itemData(ui.cboProxyType->currentIndex()).toInt());

	//set back name:
	QListWidgetItem *item=ui.lstConnections->item(nRow);
	if (item)
		item->setText(ui.editConnName->text());
}

void Dlg_FTPServers::on_chkUseProxy_stateChanged(int nChecked)
{
	EnableProxyFields(nChecked);
}

void Dlg_FTPServers::EnableProxyFields(bool bEnable)
{
	ui.editProxyHost->setEnabled(bEnable);
	ui.editProxyPort->setEnabled(bEnable);
	ui.editProxyUser->setEnabled(bEnable);
	ui.editProxyPass->setEnabled(bEnable);
	ui.cboProxyType->setEnabled(bEnable);
}



void Dlg_FTPServers::on_btnSave_clicked()
{
	//save current:
	SaveData(m_nCurrRow);
	
	_DUMP(m_lstData)
	_DUMP(m_lstDataDeleted)

	//write & delete new
	DbRecordSet dummy;
	Status pStatus;
	_SERVER_CALL(ClientSimpleORM->Write(pStatus, CORE_FTP_SERVERS, m_lstData,m_strLockedRes,-1,0,m_lstDataDeleted))
	if (!pStatus.IsOK())
	{
		_CHK_ERR(pStatus);
		done(0); //cancel
		return;
	}

	

	done(1); //all ok
	return;

}
void Dlg_FTPServers::on_btnDelete_clicked()
{
	if (m_nCurrRow>=0)
	{
		if (m_lstData.getDataRef(m_nCurrRow,"CFTP_ID").toInt()>0)
		{
			m_lstDataDeleted.addRow();
			DbRecordSet row = m_lstData.getRow(m_nCurrRow);
			m_lstDataDeleted.assignRow(m_lstDataDeleted.getRowCount()-1,row);
		}

		m_lstData.deleteRow(m_nCurrRow);
		ui.lstConnections->setCurrentRow(-1);
		if (m_lstData.getRowCount()>0)
			m_nCurrRow=0;
		else
			m_nCurrRow=-1;
		RefreshDisplay();
	}
}
void Dlg_FTPServers::on_btnNew_clicked()
{
	m_lstData.addRow();
	int nRow=m_lstData.getRowCount()-1;
	m_lstData.setData(nRow,"CFTP_NAME",tr("New Connection"));
	m_lstData.setData(nRow,"CFTP_PORT",21);
	m_lstData.setData(nRow,"CFTP_IS_PASSIVE_MODE",1);
	m_lstData.setData(nRow,"CFTP_PROXY_TYPE",QNetworkProxy::Socks5Proxy);

	m_nCurrRow=m_lstData.getRowCount()-1;
	RefreshDisplay();
}

void Dlg_FTPServers::on_btnCancel_clicked()
{
	if (!m_strLockedRes.isEmpty())
	{
		Status pStatus;
		bool bOK;
		_SERVER_CALL(ClientSimpleORM->UnLock(pStatus, CORE_FTP_SERVERS,bOK,m_strLockedRes))
		//_CHK_ERR(pStatus);
	}
	


	done(0); //cancel
}


//test connection:
void Dlg_FTPServers::on_btnTest_clicked()
{

	//-----------------------Connecting-----------------------------//


	FTPConnectionSettings conn;
	conn.m_strServerIP=ui.editHostName->text();
	conn.m_strFTPUserName=ui.editUser->text();
	conn.m_strFTPPassword=ui.editPass->text();
	conn.m_nPort=ui.editPort->text().toInt();
	conn.m_bPassive=ui.chkUsePassive->isChecked();
	conn.m_strStartDirectory=ui.editDir->text();

	FTPClient client;
	Status err;
	client.Connect(err,conn);
	_CHK_ERR(err);

	QMessageBox::information(this,tr("Success"),tr("Successfully connected to host!"));

	//-----------------------Dir change /create-----------------------------//
	//if (!ui.editDir->text().isEmpty())
	//{
		//client.Dir_Change(err,ui.editDir->text());
		//_CHK_ERR(err);

		//QMessageBox::information(this,tr("Success"),tr("Successfully changed directory!"));
	//}

	client.Disconnect(err);
}
