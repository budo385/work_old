#include "personwidget.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "gui_core/gui_core/gui_helper.h"

/*!
Person widget. Person access rights control.

\param parent			- parent.
*/
PersonWidget::PersonWidget(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	SetFUIName(tr("Role To User Assignment"));

	//Define m_recInsertedRoleToPerson.
	//Organize recordset.
	m_recInsertedRoleToPerson.addColumn(QVariant::Int, "RowID");
	m_recInsertedRoleToPerson.addColumn(QVariant::Int, "PersonID");
	m_recInsertedRoleToPerson.addColumn(QVariant::Int, "RoleID");

	ui.Person_treeWidget->setAcceptDrops(true);
	ui.Role_treeWidget->setDragEnabled(true);
	ui.Role_treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
	
	Initialize();
	InitContextMenu();	
	
	connect(ui.Role_treeWidget, SIGNAL(itemSelectionChanged()),  this, SLOT(RoleSelectionChanged()));
	connect(ui.Person_treeWidget, SIGNAL(itemSelectionChanged()),  this, SLOT(PersonSelectionChanged()));
	connect(ui.Person_treeWidget, SIGNAL(ARSDropped(QTreeWidgetItem *, int)),  this, SLOT(on_RoleDropped(QTreeWidgetItem *, int)));
}

PersonWidget::~PersonWidget()
{
}

void PersonWidget::InitContextMenu()
{
	QAction *ActionDelete = new QAction(QIcon(":trash-16.png"), "Delete", this);
	connect(ActionDelete, SIGNAL(triggered()), this, SLOT(on_DeletePushButton_clicked()));
	m_lstRoleActions << ActionDelete;
	ui.Person_treeWidget->SetChildContextMenuActions(m_lstRoleActions);
}

void PersonWidget::Initialize()
{
	InitializeButtons();
	InitializeRecordSets();
	InitializePersonTree();
	InitializeRoleList();
}

void PersonWidget::InitializePersonTree()
{
	//Fist get all persons.
	int nPersonCount = m_recBUS_PERSON.getRowCount();
	for (int i = 0; i < nPersonCount; ++i)
	{
		QString PersonName		= m_recBUS_PERSON.getDataRef(i, "BPER_FIRST_NAME").toString();
		QString PersonLastName	= m_recBUS_PERSON.getDataRef(i, "BPER_LAST_NAME").toString();
		int		PersonRowID		= m_recBUS_PERSON.getDataRef(i, "BPER_ID").toInt();

		QTreeWidgetItem *PersonItem = new QTreeWidgetItem();
		QString DisplayData = PersonName + " " + PersonLastName;
		//Set display data.
		PersonItem->setData(0, Qt::UserRole,		PersonRowID);
		PersonItem->setData(0, Qt::DisplayRole,	DisplayData);
		PersonItem->setIcon(0, QIcon(":omnis_1797.png"));

		m_hshPersonIDToItem.insert(PersonRowID, PersonItem);
	}

	//Then get persons roles.
	int nPersRoleCount = m_recBUS_PERSONROLE.getRowCount();
	for (int i = 0; i < nPersRoleCount; ++i)
	{
		int nID		= m_recBUS_PERSONROLE.getDataRef(i, 0).toInt();
		int nPersID = m_recBUS_PERSONROLE.getDataRef(i, 3).toInt();
		int nRoleID = m_recBUS_PERSONROLE.getDataRef(i, 4).toInt();

		int nRoleRow = m_recCORE_ROLE.find(0, nRoleID, true);
		//Check does it contains
		if (nRoleRow < 0)
			continue;

		QString RoleName = m_recCORE_ROLE.getDataRef(nRoleRow, "CROL_NAME").toString();
		QString RoleDesc = m_recCORE_ROLE.getDataRef(nRoleRow, "CROL_ROLE_DESC").toString();
		int		RoleType = m_recCORE_ROLE.getDataRef(nRoleRow, "CROL_ROLE_TYPE").toInt();

		QTreeWidgetItem *RoleItem = new QTreeWidgetItem();
		QString DisplayData = RoleName; //RoleName + " " + RoleDesc;
		//Set display data.
		RoleItem->setData(0, Qt::UserRole,		nID);
		RoleItem->setData(1, Qt::UserRole,		nRoleID);
		RoleItem->setData(2, Qt::UserRole,		RoleType);
		RoleItem->setData(3, Qt::UserRole,		RoleDesc);
		RoleItem->setData(0, Qt::DisplayRole,	DisplayData);

		//If system role.
		if (RoleType)
			RoleItem->setIcon(0, QIcon(":MainDataCyBl.jpg"));
		else
			RoleItem->setIcon(0, QIcon(":OptionsCyBl.jpg"));

		if (m_hshPersonIDToItem.value(nPersID))
			m_hshPersonIDToItem.value(nPersID)->addChild(RoleItem);
	}

	ui.Person_treeWidget->addTopLevelItems(m_hshPersonIDToItem.values());
	ui.Person_treeWidget->expandAll();
}

void PersonWidget::InitializeRoleList()
{
	int nRoleCount = m_recCORE_ROLE.getRowCount();
	for (int i = 0; i < nRoleCount; ++i)
	{
		int		nRoleID	 = m_recCORE_ROLE.getDataRef(i, "CROL_ID").toInt();
		QString RoleName = m_recCORE_ROLE.getDataRef(i, "CROL_NAME").toString();
		QString RoleDesc = m_recCORE_ROLE.getDataRef(i, "CROL_ROLE_DESC").toString();
		int		RoleType = m_recCORE_ROLE.getDataRef(i, "CROL_ROLE_TYPE").toInt();

		QTreeWidgetItem *RoleItem = new QTreeWidgetItem();
		QString DisplayData = RoleName;
		//Set display data.
		RoleItem->setData(0, Qt::UserRole,		nRoleID);
		RoleItem->setData(1, Qt::UserRole,		RoleType);
		RoleItem->setData(2, Qt::UserRole,		RoleDesc);
		RoleItem->setData(0, Qt::DisplayRole,	DisplayData);
		//If system role.
		if (RoleType)
			RoleItem->setIcon(0, QIcon(":MainDataCyBl.jpg"));
		else
			RoleItem->setIcon(0, QIcon(":OptionsCyBl.jpg"));

		ui.Role_treeWidget->addTopLevelItem(RoleItem);
	}
}

void PersonWidget::InitializeButtons()
{
	ui.AssignPushButton	->setDisabled(true);
	ui.SavePushButton	->setDisabled(true);
	ui.CancelPushButton	->setDisabled(true);
	ui.DeletePushButton	->setDisabled(true);
}

void PersonWidget::InitializeRecordSets()
{
	//Get Persons.
	MainEntitySelectionController Person;
	Person.Initialize(ENTITY_BUS_PERSON);
	Person.ReloadData();
	m_recBUS_PERSON = *Person.GetDataSource();

	//_DUMP(m_recBUS_PERSON);

	//_SERVER_CALL(ClientSimpleORM->Read(m_Status, BUS_PERSON, m_recBUS_PERSON));
	//_CHK_ERR(m_Status);

	//Get Persons.
	_SERVER_CALL(ClientSimpleORM->Read(m_Status, BUS_PERSONROLE, m_recBUS_PERSONROLE));
	_CHK_ERR(m_Status);

	//Get Roles.
	QString strEdition = g_pClientManager->GetIniFile()->m_strModuleCode;

	if("SC-TE" == strEdition)
		_SERVER_CALL(AccessRights->GetTERoles(m_Status, m_recCORE_ROLE))
	else
		_SERVER_CALL(AccessRights->GetBERoles(m_Status, m_recCORE_ROLE))

	_CHK_ERR(m_Status);
	
//	QString strWhere = "WHERE CROL_ROLE_TYPE > 0";
//	_SERVER_CALL(ClientSimpleORM->Read(m_Status, CORE_ROLE, m_recCORE_ROLE, strWhere));
//	_CHK_ERR(m_Status);
//	_DUMP(m_recCORE_ROLE);
}

void PersonWidget::InsertRoleToPerson(int nPersonID, int nRoleID)
{
	//Lock role.
	if (!LockResource(nPersonID))
		return;

	//Check does this role already contains this ARS.
	if (CheckIsRoleInPerson(nPersonID, nRoleID))
		return;

	//First let's get it some fake ID (it will be negative number).
	int	nInsRoleRowID = -1;
	//Then check fake ID.
	while (m_recInsertedRoleToPerson.find(0, nInsRoleRowID, true) >= 0)
		nInsRoleRowID--;

	//Insert it to recordset.
	m_recInsertedRoleToPerson.addRow();
	int row = m_recInsertedRoleToPerson.getRowCount() - 1;
	m_recInsertedRoleToPerson.setData(row, 0, nInsRoleRowID);
	m_recInsertedRoleToPerson.setData(row, 1, nPersonID);
	m_recInsertedRoleToPerson.setData(row, 2, nRoleID);

	//Get recordset data and create item.
	int nRoleRow = m_recCORE_ROLE.find(0, nRoleID, true);

	QString RoleName = m_recCORE_ROLE.getDataRef(nRoleRow, "CROL_NAME").toString();
	QString RoleDesc = m_recCORE_ROLE.getDataRef(nRoleRow, "CROL_ROLE_DESC").toString();
	int		RoleType = m_recCORE_ROLE.getDataRef(nRoleRow, "CROL_ROLE_TYPE").toInt();

	QTreeWidgetItem *RoleItem = new QTreeWidgetItem();
	QString DisplayData = RoleName;
	//Set display data.
	RoleItem->setData(0, Qt::UserRole,		nInsRoleRowID);
	RoleItem->setData(1, Qt::UserRole,		nRoleID);
	RoleItem->setData(2, Qt::UserRole,		RoleType);
	RoleItem->setData(3, Qt::UserRole,		RoleDesc);
	RoleItem->setData(0, Qt::DisplayRole,	DisplayData);
	//If system role.
	if (RoleType)
		RoleItem->setIcon(0, QIcon(":MainDataCyBl.jpg"));
	else
		RoleItem->setIcon(0, QIcon(":OptionsCyBl.jpg"));

	//Add it to person.
	m_hshPersonIDToItem.value(nPersonID)->addChild(RoleItem);
	m_hshPersonIDToItem.value(nPersonID)->setExpanded(true);
	ui.Person_treeWidget->clearSelection();
	RoleItem->setSelected(true);

	//Change save state.
	ChangeSaveState();
}

void PersonWidget::ChangeSaveState()
{
	if (!m_hshDeletedRoleFromPerson.isEmpty() ||
		m_recInsertedRoleToPerson.getRowCount() > 0)
	{
		ui.SavePushButton->setEnabled(true);
		GUI_Helper::SetWidgetFontWeight(ui.SavePushButton, QFont::Bold);
		GUI_Helper::SetStyledButtonColorBkg(ui.SavePushButton, GUI_Helper::BTN_COLOR_GREEN, "white");
		ui.CancelPushButton->setEnabled(true);
		SetMode(FuiBase::MODE_EDIT);
	}
	else
	{
		ui.SavePushButton->setEnabled(false);
		ui.SavePushButton->setStyleSheet("");
		ui.CancelPushButton->setEnabled(false);
		SetMode(FuiBase::MODE_READ);
	}
}

bool PersonWidget::LockResource(int PersonID)
{
	//Check is this person already locked.
	if (m_hshLockingResourceHash.contains(PersonID))
		return true;

	//If nothing from above lock.
	QString LockResourceID;
	//Create locking recordset.
	int row = m_recBUS_PERSON.find(0, PersonID, true);
	DbRecordSet lockRecSet;
	lockRecSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
	lockRecSet.addRow();
	lockRecSet.setData(0, 0, PersonID);

	DbRecordSet lstStatusRows;
	_SERVER_CALL(ClientSimpleORM->Lock(m_Status, BUS_PERSON, lockRecSet, LockResourceID, lstStatusRows))
	if (!m_Status.IsOK())
	{
		QMessageBox::critical(this, "Locking error!", m_Status.getErrorText());
		return false;
	}

	m_hshLockingResourceHash.insert(PersonID, LockResourceID);
	return true;
}

void PersonWidget::UnLockResources()
{
	foreach(int PersonID, m_hshLockingResourceHash.keys())
	{
		QString lockRes = m_hshLockingResourceHash.value(PersonID);
		bool bUnlocked;
		_SERVER_CALL(ClientSimpleORM->UnLock(m_Status, BUS_PERSON, bUnlocked, lockRes))
		_CHK_ERR(m_Status);
		Q_ASSERT(bUnlocked);
	}

	m_hshLockingResourceHash.clear();
}

void PersonWidget::DeleteRoleFromPerson(QTreeWidgetItem *Item, QTreeWidgetItem *ParentItem)
{
	Q_ASSERT(Item);
	Q_ASSERT(ParentItem);
	int nPersonID		= ParentItem->data(0, Qt::UserRole).toInt();
	int nRoleToPersonID	= Item->data(0, Qt::UserRole).toInt();
	int nRoleID;

	//Lock role.
	if (!LockResource(nPersonID))
		return;

	//If inserted get access right set id (for later to delete).
	if (nRoleToPersonID < 0)
	{
		int r	= m_recInsertedRoleToPerson.find(0, nRoleToPersonID, true);
		//Delete it from insert recordset.
		m_recInsertedRoleToPerson.clearSelection();
		m_recInsertedRoleToPerson.selectRow(r);
		m_recInsertedRoleToPerson.deleteSelectedRows();
	}
	else //If not inserted access rights, then it is from before.
	{
		int r = m_recBUS_PERSONROLE.find(0, nRoleToPersonID, true);
		nRoleID = m_recBUS_PERSONROLE.getDataRef(r, 4).toInt();
		//Delete it from m_recBUS_PERSONROLE.
		m_recBUS_PERSONROLE.clearSelection();
		m_recBUS_PERSONROLE.selectRow(r);
		m_recBUS_PERSONROLE.deleteSelectedRows();
		//Insert person id to role id combination to delete.
		m_hshDeletedRoleFromPerson.insert(nPersonID, nRoleID);
	}

	//Take it from tree and delete it.
	ParentItem->takeChild(ParentItem->indexOfChild(Item));
	delete(Item);

	//Check save button.
	ChangeSaveState();
}

bool PersonWidget::CheckIsRoleInPerson(int nPersonID, int nRoleID)
{
	//Check is it already inserted before.
	m_recBUS_PERSONROLE.find(3, nPersonID);
	m_recBUS_PERSONROLE.find(4, nRoleID, false, true, true);
	if (m_recBUS_PERSONROLE.getSelectedCount() > 0)
		return true;

	//Check is it inserted recently.
	m_recInsertedRoleToPerson.find(1, nPersonID);
	m_recInsertedRoleToPerson.find(2, nRoleID, false, true, true);
	if (m_recInsertedRoleToPerson.getSelectedCount() > 0)
		return true;

	return false;
}

void PersonWidget::ReloadData()
{
	m_recCORE_ROLE.clear();
	m_recBUS_PERSON.clear();
	m_recBUS_PERSONROLE.clear();
	m_hshPersonIDToItem.clear();
	m_recInsertedRoleToPerson.clear();
	m_hshDeletedRoleFromPerson.clear();
	m_hshLockingResourceHash.clear();

	ui.Person_treeWidget->clear();
	ui.Role_treeWidget->clear();

	Initialize();
}

void PersonWidget::on_AssignPushButton_clicked()
{
	//Create selected roles recordset.
	DbRecordSet SelectedPersons;

	//Call dialog to get roles.
	MultiAssignDialog *PersonAssign = new MultiAssignDialog(&m_recBUS_PERSON);
	PersonAssign->SetTitle(tr("Assign Roles To Multiple Users"));
	if (PersonAssign->exec())
		SelectedPersons = PersonAssign->GetSelectedItems();

	//Get list of items and put them in a list.
	QList<QTreeWidgetItem*> selectedPersonItems;
	int selectedCount = SelectedPersons.getRowCount();
	for (int i = 0; i < selectedCount; ++i)
	{
		int PersonID = SelectedPersons.getDataRef(i, 0).toInt();
		selectedPersonItems << m_hshPersonIDToItem.value(PersonID);
	}

	foreach(QTreeWidgetItem *item, selectedPersonItems)
		on_RoleDropped(item, 0);

	//Delete dialog.
	delete(PersonAssign);
	return;
}

void PersonWidget::on_SavePushButton_clicked()
{
	int nInsertCount = m_recInsertedRoleToPerson.getRowCount();
	for (int i = 0; i < nInsertCount; ++i)
	{
		int nPersonID = m_recInsertedRoleToPerson.getDataRef(i, 1).toInt();
		int nRoleID   = m_recInsertedRoleToPerson.getDataRef(i, 2).toInt();
		_SERVER_CALL(AccessRights->InsertNewRoleToPerson(m_Status, nPersonID, nRoleID))
		_CHK_ERR(m_Status);
	}

	foreach(int nPersonID, m_hshDeletedRoleFromPerson.uniqueKeys())
	{
		foreach(int nRoleID, m_hshDeletedRoleFromPerson.values(nPersonID))
		{
			_SERVER_CALL(AccessRights->DeleteRoleFromPerson(m_Status, nPersonID, nRoleID))
			_CHK_ERR(m_Status);
		}
	}

	UnLockResources();
	ReloadData();
	SetMode(FuiBase::MODE_READ);
	ChangeSaveState();
}

void PersonWidget::on_CancelPushButton_clicked()
{
	if (QMessageBox::warning(this, "Cancel pressed!", "Do you want to discard changes!", QMessageBox::Ok, QMessageBox::Cancel) == QMessageBox::Cancel)
		return;
	
	UnLockResources();
	ReloadData();
	SetMode(FuiBase::MODE_READ);
	//g_objFuiManager.CloseCurrentFuiTab();
	ChangeSaveState();
}

void PersonWidget::on_DeletePushButton_clicked()
{
	//Get selected items list.
	QList<QTreeWidgetItem*> selectedItemsList = ui.Person_treeWidget->selectedItems();

	if (!selectedItemsList.count())
		return;
	
	QTreeWidgetItem *Item, *ParentItem;
	Item = selectedItemsList.first();
	ParentItem = Item->parent();

	DeleteRoleFromPerson(Item, ParentItem);
	ChangeSaveState();
}

void PersonWidget::RoleSelectionChanged()
{
	//Get selected items list.
	QList<QTreeWidgetItem*> selectedItemsList = ui.Role_treeWidget->selectedItems();

	if (!selectedItemsList.isEmpty())
	{
		ui.ARSDescr_textBrowser->clear();
		ui.ARSDescr_textBrowser->insertPlainText(selectedItemsList.first()->data(2, Qt::UserRole).toString());
		ui.AssignPushButton->setDisabled(false);
	}
	else
		ui.AssignPushButton->setDisabled(true);
}

void PersonWidget::PersonSelectionChanged()
{
	//Get selected items list.
	QList<QTreeWidgetItem*> selectedItemsList = ui.Person_treeWidget->selectedItems();

	if (selectedItemsList.count() != 0)
	{
		QTreeWidgetItem *item = selectedItemsList.first();
		if (!item->parent())
			ui.DeletePushButton->setDisabled(true);
		else
			ui.DeletePushButton->setDisabled(false);

		return;
	}
	
	ui.DeletePushButton->setDisabled(true);
}

void PersonWidget::on_RoleDropped(QTreeWidgetItem *Parent, int index)
{
	QList<int> SelectedRoleRowID = ui.Role_treeWidget->GetSelectedItems();

	int nPersonID = Parent->data(0, Qt::UserRole).toInt();

	//Insert dropped roles.
	foreach(int RoleID, SelectedRoleRowID)
		InsertRoleToPerson(nPersonID, RoleID);
}
