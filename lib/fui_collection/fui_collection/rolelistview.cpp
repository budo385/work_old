#include "rolelistview.h"
//#include "fui_collection/fui_collection/roledefinitionwidget.h"

/*!
Role list view, sub classed from QListView.

\param parent			- parent.
*/
RoleListView::RoleListView(QWidget *parent /*= 0*/)
	: QListView(parent)
{
	//Drag&Drop
	setAcceptDrops(false);
	setDragEnabled(true);

	//Selection
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectItems);

	//Tab navigation.
	setTabKeyNavigation(true);
}

RoleListView::~RoleListView()
{

}
