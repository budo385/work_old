#include "clientsetupwizpage_import.h"
#include "fui_importcontact.h"

#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:

ClientSetupWizPage_Import::ClientSetupWizPage_Import(int nWizardPageID, QString strPageTitle, QWidget *parent)
	: WizardPage(nWizardPageID, strPageTitle)
{

}

ClientSetupWizPage_Import::~ClientSetupWizPage_Import()
{

}

void ClientSetupWizPage_Import::Initialize()
{
	ui.setupUi(this);

	//ui.checkBox->setChecked(g_pClientManager->GetIniFile()->m_nHideLogin);
	m_bComplete = true;
	m_bInitialized = true;
}


void ClientSetupWizPage_Import::on_btnImportSykpe_Contact_clicked()
{
	
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
	FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->SetDefaultSource(tr("Skype Contacts"));
		pFui->show();  
	}
}

void ClientSetupWizPage_Import::on_btnImportOutlook_Contact_clicked()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
	FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->SetDefaultSource(tr("Outlook"));
		pFui->show();  
	}

}

void ClientSetupWizPage_Import::on_btnImportOutlook_Email_clicked()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_EMAIL,true);

}