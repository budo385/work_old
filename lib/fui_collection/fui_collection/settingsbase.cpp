#include "settingsbase.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

SettingsBase::SettingsBase(int nSettingsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
    : QWidget(parent)
{
	//Get settings set id.
	m_nSettingsSetID = nSettingsSetID;
	m_bOptionsSwitch = bOptionsSwitch;
	m_pOptionsAndSettingsManager = pOptionsAndSettingsManager;
	
	//Create local recordset.
	//m_recSettings.copyDefinition(*g_ClientCache.GetCache(ENTITY_APPLICATION_OPTIONS));
	m_recSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));

	if (m_pOptionsAndSettingsManager)
	{
		m_recSettings.merge(m_pOptionsAndSettingsManager->GetPersonSettingSet(nSettingsSetID));
	}
	else
	{
		//If application option.
		if (m_bOptionsSwitch)
			m_recSettings.merge(g_pSettings->GetApplicationOptionSet(nSettingsSetID));
		else
			m_recSettings.merge(g_pSettings->GetPersonSettingSet(nSettingsSetID));
	}

	//m_recSettings.Dump();

	m_bWholeSetChanged=false;
}

SettingsBase::~SettingsBase()
{
	
}

DbRecordSet SettingsBase::GetChangedValuesRecordSet()
{
	DbRecordSet returnRecordSet;
	returnRecordSet.copyDefinition(m_recSettings);

	if (!m_bWholeSetChanged)
	{
		m_recSettings.clearSelection();
		foreach(int ID, m_lstChangedValues)
		{
			int row = m_recSettings.find("BOUS_SETTING_ID", ID, true);
			m_recSettings.selectRow(row);
		}
	}
	//m_recSettings.getSelectedRecordSet().Dump();
	returnRecordSet.merge(m_recSettings, !m_bWholeSetChanged);

	return returnRecordSet;
}

int SettingsBase::GetPersonID()
{
	if (m_pOptionsAndSettingsManager)
		return m_pOptionsAndSettingsManager->GetPersonID();

	return -1;
}

void SettingsBase::SetIDChanged(int SettingsID)
{
	if (!m_lstChangedValues.contains(SettingsID))
		m_lstChangedValues << SettingsID;
}

void SettingsBase::SetChanged()
{
	m_bWholeSetChanged=true;
}


QVariant SettingsBase::GetSettingValue(int nSettingID)
{
	QVariant varSettingValue;

	int nRow = m_recSettings.find("BOUS_SETTING_ID", nSettingID, true);
	if (nRow == -1)
		nRow = InsertNonExistingSetting(nSettingID);
	Q_ASSERT(nRow!=-1);

	//Get setting type.
	int nSettingType = m_recSettings.getDataRef(nRow, "BOUS_SETTING_TYPE").toInt();

	//Select value.
	//If bool or int.
	if (nSettingType == 0 || nSettingType == 1)
		varSettingValue = m_recSettings.getDataRef(nRow, "BOUS_VALUE");
	else if (nSettingType == 2)
		varSettingValue = m_recSettings.getDataRef(nRow, "BOUS_VALUE_STRING");
	else if (nSettingType == 3)
		varSettingValue = m_recSettings.getDataRef(nRow, "BOUS_VALUE_BYTE");
	else
		Q_ASSERT(false);

	return varSettingValue;
}


void SettingsBase::SetSettingValue(int nSettingID,int nValue)
{
	//Get setting and or if not exits create it.
	int nRow = m_recSettings.find("BOUS_SETTING_ID", nSettingID, true);
	if (nRow == -1)
		nRow = InsertNonExistingSetting(nSettingID);
	Q_ASSERT(nRow!=-1);

	m_recSettings.setData(nRow, "BOUS_VALUE", nValue);

	SetIDChanged(nSettingID);
}

void SettingsBase::SetSettingValue(int nSettingID,QByteArray byteArray)
{
	//Get setting and or if not exits create it.
	int nRow = m_recSettings.find("BOUS_SETTING_ID", nSettingID, true);
	if (nRow == -1)
		nRow = InsertNonExistingSetting(nSettingID);
	Q_ASSERT(nRow!=-1);

	m_recSettings.setData(nRow, "BOUS_VALUE_BYTE", byteArray);

	SetIDChanged(nSettingID);
}

void SettingsBase::SetSettingValue(int nSettingID,QString strValue)
{
	//Get setting and or if not exits create it.
	int nRow = m_recSettings.find("BOUS_SETTING_ID", nSettingID, true);
	if (nRow == -1)
		nRow = InsertNonExistingSetting(nSettingID);
	Q_ASSERT(nRow!=-1);

	m_recSettings.setData(nRow, "BOUS_VALUE_STRING", strValue);

	SetIDChanged(nSettingID);
}

int SettingsBase::InsertNonExistingSetting(int nSettingID)
{
	int nRow = -1;
	if (!m_recSettings.getRowCount())
		m_recSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
	if (m_bOptionsSwitch)
		m_recSettings.merge(g_pSettings->GetApplicationOptionRow(nSettingID));
	else
		m_recSettings.merge(g_pSettings->GetPersonSettingRow(nSettingID));
	nRow = m_recSettings.find("BOUS_SETTING_ID", nSettingID, true);

	return nRow;
}
