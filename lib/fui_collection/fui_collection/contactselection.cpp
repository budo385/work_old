#include "contactselection.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "common/common/entity_id_collection.h"
#include "bus_client/bus_client/selection_contacts.h"
#include "gui_core/gui_core/universaltablewidget.h"
#include "gui_core/gui_core/macros.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "bus_client/bus_client/emailselectordialog.h"
extern BusinessServiceManager *g_pBoSet;		

ContactSelection::ContactSelection(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	m_recToRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	m_recCCRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	m_recBCCRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));

	//Table layout.
	ui.To_tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.To_tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	ui.CC_tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.CC_tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	ui.BCC_tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.BCC_tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	//Mail selection buttons.
	ui.selectToMail_pushButton->setIcon(QIcon(":Search_By_Phone_16.png"));
	ui.selectToMail_pushButton->setToolTip(tr("Select Alternative Email Address"));
	ui.selectCCMail_pushButton->setIcon(QIcon(":Search_By_Phone_16.png"));
	ui.selectCCMail_pushButton->setToolTip(tr("Select Alternative Email Address"));
	ui.selectBCCMail_pushButton->setIcon(QIcon(":Search_By_Phone_16.png"));
	ui.selectBCCMail_pushButton->setToolTip(tr("Select Alternative Email Address"));
	ui.selectToMail_pushButton->setDisabled(true);
	ui.selectCCMail_pushButton->setDisabled(true);
	ui.selectBCCMail_pushButton->setDisabled(true);
	
	//Window title.
	setWindowTitle(tr("Contact Selection"));

	InitializeContacts();
}

ContactSelection::~ContactSelection()
{
	delete(m_pContactWidget);
}

void ContactSelection::GetSelectedContacts(DbRecordSet &recToRecordSet,	DbRecordSet &recCCRecordSet, DbRecordSet &recBCCRecordSet)
{
	recToRecordSet	= m_recToRecordSet;
	recCCRecordSet	= m_recCCRecordSet;
	recBCCRecordSet	= m_recBCCRecordSet;
}

void ContactSelection::InitializeContacts()
{
	//determine selector type by trick: instante MainEntitySelectionController and get datatype
	MainEntitySelectionController tmp;
	tmp.Initialize(ENTITY_BUS_CONTACT);

	m_pContactWidget	= new Selection_Contacts;


	Selection_Contacts* p = dynamic_cast<Selection_Contacts*>(m_pContactWidget);

	dynamic_cast<Selection_Contacts*>(m_pContactWidget)->Initialize(false, false, true, true, true); //enable double click!
	
	ui.To_tableWidget-> Initialize(&m_recToRecordSet, true, false, false, true, 25, false, true); //Disable drop handler.
	ui.CC_tableWidget-> Initialize(&m_recCCRecordSet, true, false, false, true, 25, false, true);
	ui.BCC_tableWidget->Initialize(&m_recBCCRecordSet, true, false, false, true, 25, false, true);
	ui.To_tableWidget-> SetEditMode(true);
	ui.CC_tableWidget-> SetEditMode(true);
	ui.BCC_tableWidget->SetEditMode(true);
	ui.To_tableWidget-> AddAcceptableDropTypes(ENTITY_BUS_CONTACT);
	ui.CC_tableWidget-> AddAcceptableDropTypes(ENTITY_BUS_CONTACT);
	ui.BCC_tableWidget->AddAcceptableDropTypes(ENTITY_BUS_CONTACT);

	//TO column headers
	DbRecordSet ToColumns;
	ui.To_tableWidget->AddColumnToSetup(ToColumns, "CONTACT_NAME", tr("Contact"), 200, false, "");
	ui.To_tableWidget->AddColumnToSetup(ToColumns, "CONTACT_EMAIL", tr("Email"), 200, false, "");
	ui.To_tableWidget->SetColumnSetup(ToColumns);
	ui.To_tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

	//CC column headers
	DbRecordSet CCColumns;
	ui.CC_tableWidget->AddColumnToSetup(CCColumns, "CONTACT_NAME", tr("Contact"), 200, false, "");
	ui.CC_tableWidget->AddColumnToSetup(CCColumns, "CONTACT_EMAIL", tr("Email"), 200, false, "");
	ui.CC_tableWidget->SetColumnSetup(CCColumns);
	ui.CC_tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

	//BCC column headers
	DbRecordSet BCCColumns;
	ui.BCC_tableWidget->AddColumnToSetup(BCCColumns, "CONTACT_NAME", tr("Contact"), 200, false, "");
	ui.BCC_tableWidget->AddColumnToSetup(BCCColumns, "CONTACT_EMAIL", tr("Email"), 200, false, "");
	ui.BCC_tableWidget->SetColumnSetup(BCCColumns);
	ui.BCC_tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

	ui.Contact_stackedWidget->addWidget(m_pContactWidget);
	ui.Contact_stackedWidget->setCurrentWidget(m_pContactWidget);

	//Connect drop signals.
	connect(ui.To_tableWidget,	SIGNAL(DataDroped(QModelIndex, int, DbRecordSet&, QDropEvent*)), this, SLOT(ToDataDroped(QModelIndex, int, DbRecordSet&, QDropEvent*)));
	connect(ui.CC_tableWidget,  SIGNAL(DataDroped(QModelIndex, int, DbRecordSet&, QDropEvent*)), this, SLOT(CCDataDroped(QModelIndex, int, DbRecordSet&, QDropEvent*)));
	connect(ui.BCC_tableWidget, SIGNAL(DataDroped(QModelIndex, int, DbRecordSet&, QDropEvent*)), this, SLOT(BCCDataDroped(QModelIndex, int, DbRecordSet&, QDropEvent*)));

	connect(ui.To_tableWidget,  SIGNAL(itemSelectionChanged()), this, SLOT(on_ToTable_SelectionChanged()));
	connect(ui.CC_tableWidget,  SIGNAL(itemSelectionChanged()), this, SLOT(on_CCTable_SelectionChanged()));
	connect(ui.BCC_tableWidget, SIGNAL(itemSelectionChanged()), this, SLOT(on_BCCTable_SelectionChanged()));
}

void ContactSelection::GetMail(DbRecordSet &DroppedValue, DbRecordSet &recContainer)
{
	Status status;
	
	int nRowCount = DroppedValue.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QString strWhere = " WHERE BCME_IS_DEFAULT = 1";
		int nContactID = DroppedValue.getDataRef(i, 0).toInt();
		strWhere += " AND BCME_CONTACT_ID = " + QVariant(nContactID).toString();
		DbRecordSet recMailRecordSet;
		_SERVER_CALL(ClientSimpleORM->Read(status, BUS_CM_EMAIL, recMailRecordSet, strWhere))
		_CHK_ERR(status);

		if (recMailRecordSet.getRowCount() > 0)
		{
			int		nEmailID = recMailRecordSet.getDataRef(0, 0).toInt();
			QString strEmail = recMailRecordSet.getDataRef(0, "BCME_ADDRESS").toString();
			QString ContactName = dynamic_cast<MainEntitySelectionController*>(m_pContactWidget)->GetCalculatedName(nContactID);

			//Add to result recordset.
			recContainer.addRow();
			int nCurrentRow = recContainer.getRowCount() -1;
			recContainer.setData(nCurrentRow, 0, nContactID);
			recContainer.setData(nCurrentRow, 1, nEmailID);
			recContainer.setData(nCurrentRow, 2, ContactName);
			recContainer.setData(nCurrentRow, 3, strEmail);
		}
	}
}

void ContactSelection::ToDataDroped(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event)
{
	//DroppedValue.Dump();
	GetMail(DroppedValue, m_recToRecordSet);
	ui.To_tableWidget->RefreshDisplay();
}

void ContactSelection::CCDataDroped(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event)
{
	GetMail(DroppedValue, m_recCCRecordSet);
	ui.CC_tableWidget->RefreshDisplay();
}

void ContactSelection::BCCDataDroped(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event)
{
	GetMail(DroppedValue, m_recBCCRecordSet);
	ui.BCC_tableWidget->RefreshDisplay();
}

void ContactSelection::on_AddTo_pushButton_clicked()
{
	//Get contact recordset.
	DbRecordSet recSelectedContacts = *(dynamic_cast<MainEntitySelectionController*>(m_pContactWidget)->GetDataSource());

	//Get only selected ones.
	DbRecordSet SelectedRecodSet;
	SelectedRecodSet.copyDefinition(recSelectedContacts);
	SelectedRecodSet.merge(recSelectedContacts, true);

	GetMail(SelectedRecodSet, m_recToRecordSet);
	ui.To_tableWidget->RefreshDisplay();
}

void ContactSelection::on_AddCC_pushButton_clicked()
{
	//Get contact recordset.
	DbRecordSet recSelectedContacts = *(dynamic_cast<MainEntitySelectionController*>(m_pContactWidget)->GetDataSource());

	//Get only selected ones.
	DbRecordSet SelectedRecodSet;
	SelectedRecodSet.copyDefinition(recSelectedContacts);
	SelectedRecodSet.merge(recSelectedContacts, true);

	GetMail(SelectedRecodSet, m_recCCRecordSet);
	ui.CC_tableWidget->RefreshDisplay();
}

void ContactSelection::on_AddBCC_pushButton_clicked()
{
	//Get contact recordset.
	DbRecordSet recSelectedContacts = *(dynamic_cast<MainEntitySelectionController*>(m_pContactWidget)->GetDataSource());

	//Get only selected ones.
	DbRecordSet SelectedRecodSet;
	SelectedRecodSet.copyDefinition(recSelectedContacts);
	SelectedRecodSet.merge(recSelectedContacts, true);

	GetMail(SelectedRecodSet, m_recBCCRecordSet);
	ui.BCC_tableWidget->RefreshDisplay();
}

void ContactSelection::on_Ok_pushButton_clicked()
{
	accept();
}

void ContactSelection::on_Cancel_pushButton_clicked()
{
	reject();
}

void ContactSelection::on_ToTable_SelectionChanged()
{
	if (ui.To_tableWidget->selectedItems().count())
		ui.selectToMail_pushButton->setDisabled(false);
	else
		ui.selectToMail_pushButton->setDisabled(true);
}

void ContactSelection::on_CCTable_SelectionChanged()
{
	if (ui.CC_tableWidget->selectedItems().count())
		ui.selectCCMail_pushButton->setDisabled(false);
	else
		ui.selectCCMail_pushButton->setDisabled(true);
}

void ContactSelection::on_BCCTable_SelectionChanged()
{
	if (ui.BCC_tableWidget->selectedItems().count())
		ui.selectBCCMail_pushButton->setDisabled(false);
	else
		ui.selectBCCMail_pushButton->setDisabled(true);
}

void ContactSelection::on_selectToMail_pushButton_clicked()
{
	//Get contact ID.
	int nSelectedRowInRecordSet = ui.To_tableWidget->GetDataSource()->getSelectedRow();
	if (m_recToRecordSet.getDataRef(nSelectedRowInRecordSet, "CONTACT_ID").isNull())
		return;

	int nContactID = m_recToRecordSet.getDataRef(nSelectedRowInRecordSet, "CONTACT_ID").toInt();
	//DbRecordSet recMailRecordSet;
	//if (!GetSingleContacMails(nContactID, recMailRecordSet))
	//	return;

	QString strSelectedEmail;

	EmailSelectorDialog emailDialog(nContactID);
	if (emailDialog.exec())
	{
		QList<QString> lstData;
		lstData = emailDialog.getSelectedEmail();

		if (lstData.count()>0)
		{
			int nEmailID = QVariant(lstData.takeFirst()).toInt();
			QString strEmail = lstData.takeFirst();
			m_recToRecordSet.setData(nSelectedRowInRecordSet, "EMAIL_ID", nEmailID);
			m_recToRecordSet.setData(nSelectedRowInRecordSet, "CONTACT_EMAIL", strEmail);
			ui.To_tableWidget->RefreshDisplay();
		}
	}
}

void ContactSelection::on_selectCCMail_pushButton_clicked()
{
	//Get contact ID.
	int nSelectedRowInRecordSet = ui.CC_tableWidget->GetDataSource()->getSelectedRow();
	if (m_recCCRecordSet.getDataRef(nSelectedRowInRecordSet, "CONTACT_ID").isNull())
		return;

	int nContactID = m_recCCRecordSet.getDataRef(nSelectedRowInRecordSet, "CONTACT_ID").toInt();
	//DbRecordSet recMailRecordSet;
	//if (!GetSingleContacMails(nContactID, recMailRecordSet))
	//	return;

	QString strSelectedEmail;

	EmailSelectorDialog emailDialog(nContactID);
	if (emailDialog.exec())
	{
		QList<QString> lstData;
		lstData = emailDialog.getSelectedEmail();
		
		if (lstData.count()>0)
		{
			int nEmailID = QVariant(lstData.takeFirst()).toInt();
			QString strEmail = lstData.takeFirst();
			m_recCCRecordSet.setData(nSelectedRowInRecordSet, "EMAIL_ID", nEmailID);
			m_recCCRecordSet.setData(nSelectedRowInRecordSet, "CONTACT_EMAIL", strEmail);
			ui.CC_tableWidget->RefreshDisplay();
		}
	}
}

void ContactSelection::on_selectBCCMail_pushButton_clicked()
{
	//Get contact ID.
	int nSelectedRowInRecordSet = ui.BCC_tableWidget->GetDataSource()->getSelectedRow();
	if (m_recBCCRecordSet.getDataRef(nSelectedRowInRecordSet, "CONTACT_ID").isNull())
		return;

	int nContactID = m_recBCCRecordSet.getDataRef(nSelectedRowInRecordSet, "CONTACT_ID").toInt();
	//DbRecordSet recMailRecordSet;
	//if (!GetSingleContacMails(nContactID, recMailRecordSet))
	//	return;

	QString strSelectedEmail;

	EmailSelectorDialog emailDialog(nContactID);
	if (emailDialog.exec())
	{
		QList<QString> lstData;
		lstData = emailDialog.getSelectedEmail();

		if (lstData.count()>0)
		{
			int nEmailID = QVariant(lstData.takeFirst()).toInt();
			QString strEmail = lstData.takeFirst();
			m_recBCCRecordSet.setData(nSelectedRowInRecordSet, "EMAIL_ID", nEmailID);
			m_recBCCRecordSet.setData(nSelectedRowInRecordSet, "CONTACT_EMAIL", strEmail);
			ui.BCC_tableWidget->RefreshDisplay();
		}
	}
}
