#include "table_nmrxrelation.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "gui_core/gui_core/thememanager.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "bus_client/bus_client/emailselectordialog.h"
#include "bus_client/bus_client/phoneselectordlg.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/macros.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "bus_core/bus_core/formatphone.h"

#include <QHeaderView>
#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QToolButton>
#include <QTextEdit>
#include <QLabel>
#include <QMimeData>
#include "os_specific/os_specific/mime_types.h"
#include "trans/trans/xmlutil.h"

#include "gui_core/gui_core/macros.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

Table_NMRXRelation::Table_NMRXRelation(QWidget * parent)
:UniversalTableWidgetEx(parent)
{
}



//called after dark
void Table_NMRXRelation::Initialize(DbRecordSet *pData,int nMasterTableID,QList<int>lstPairs, int nX_TableID)
{
	m_nX_TableID=nX_TableID;

	int nCellHeight=30;

	UniversalTableWidgetEx::ColumnType nColType=UniversalTableWidgetEx::COL_TYPE_TEXT;
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
	{
		this->setStyleSheet(styleSheet()+" QTextEdit {font-weight:500;font-family:Arial; font-size:8pt}");
		nColType=UniversalTableWidgetEx::COL_TYPE_TEXT;
		//this->verticalHeader()->setDefaultSectionSize(47);
		nCellHeight=47;
	}

	//redefine table look:
	DbRecordSet columns;
	if (nX_TableID==BUS_CAL_INVITE)
	{
		AddColumnToSetup(columns,"NAME_2",QApplication::translate("Table_NMRXRelation","Name 1"),120,false,"",nColType);
		AddColumnToSetup(columns,"BNRO_NAME",QApplication::translate("Table_NMRXRelation","Role"),140,false,"",nColType);
		AddColumnToSetup(columns,"X_RECORD",QApplication::translate("Table_NMRXRelation","Invitation"),200,true,"",UniversalTableWidgetEx::COL_TYPE_CUSTOM_WIDGET);
		AddColumnToSetup(columns,"BNMR_DESCRIPTION",QApplication::translate("Table_NMRXRelation","Description"),170,false,"",UniversalTableWidgetEx::COL_TYPE_CUSTOM_WIDGET);

		m_nCol_X_Widget=2;
		m_nCol_Desc=3;
		nCellHeight=60;
	}
	else
	{
		if (lstPairs.at(0)>=NMRXManager::PAIR_CALENDAR_PERSON ) //special cases for calendar NMRX:
		{
			AddColumnToSetup(columns,"NAME_2",QApplication::translate("Table_NMRXRelation","Name 1"),120,false,"",nColType);
			AddColumnToSetup(columns,"BNRO_NAME",QApplication::translate("Table_NMRXRelation","Role"),140,false,"",nColType);
			AddColumnToSetup(columns,"BNMR_DESCRIPTION",QApplication::translate("Table_NMRXRelation","Description"),170,false,"",UniversalTableWidgetEx::COL_TYPE_CUSTOM_WIDGET);

			m_nCol_X_Widget=-1;
			m_nCol_Desc=2;
			nCellHeight=60;
		}
		else
		{
			AddColumnToSetup(columns,"NAME_1",QApplication::translate("Table_NMRXRelation","Name 1"),120,false,"",nColType);
			AddColumnToSetup(columns,"BNRO_NAME",QApplication::translate("Table_NMRXRelation","Role"),140,false,"",nColType);
			AddColumnToSetup(columns,"NAME_2",QApplication::translate("Table_NMRXRelation","Name 2"),120,false,"",nColType);
			AddColumnToSetup(columns,"BNMR_DESCRIPTION",QApplication::translate("Table_NMRXRelation","Description"),170,false,"",UniversalTableWidgetEx::COL_TYPE_CUSTOM_WIDGET);

			m_nCol_X_Widget=-1;
			m_nCol_Desc=3;
		}
	}

	//calendar:
	m_nAssignedTableID=-1;
	if (lstPairs.at(0)==NMRXManager::PAIR_CALENDAR_PERSON || lstPairs.at(0)==NMRXManager::PAIR_CALENDAR_CONTACT)
	{
		m_nAssignedTableID=BUS_CM_CONTACT;
	}
	if (lstPairs.at(0)==NMRXManager::PAIR_CALENDAR_PROJECT)
	{
		m_nAssignedTableID=BUS_PROJECT;
	}
	if (lstPairs.at(0)==NMRXManager::PAIR_CALENDAR_RESOURCE)
	{
		m_nAssignedTableID=BUS_CAL_RESOURCE;
	}


	

	//SetColumnSetup(columns);

	//set data source for table
	UniversalTableWidgetEx::Initialize(pData,&columns,false,true,nCellHeight);
	EnableDrop(true);

	m_nMasterTableID=nMasterTableID;
	m_nTable1=pData->getColumnIdx("BNMR_TABLE_1");
	m_nTable2=pData->getColumnIdx("BNMR_TABLE_2");
}




//dummy: if inserted: blue icon, else red
void Table_NMRXRelation::GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip)
{
	//set icon:
	int nTableID=-1;
	if(GetDataSource()->getDataRef(nRow,m_nTable1).toInt()==m_nMasterTableID)
		nTableID=GetDataSource()->getDataRef(nRow,m_nTable2).toInt();
	else
		nTableID=GetDataSource()->getDataRef(nRow,m_nTable1).toInt();

	QString strIcon;
	NMRXManager::GetIconAndTextFromTableID(nTableID,strIcon,strStatusTip); 
	RowIcon.addFile(strIcon);
}


void Table_NMRXRelation::dragEnterEvent ( QDragEnterEvent * event )
{
	//if (event->mimeData()->hasUrls())
	event->accept();

}

void Table_NMRXRelation::dragMoveEvent(QDragMoveEvent *event )
{
	event->acceptProposedAction();
}

void Table_NMRXRelation::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	//internal/external
	if (mime->hasFormat(SOKRATES_MIME_LIST))
	{
		int nEntityID;
		DbRecordSet lstDropped;

		//import emails:
		QByteArray arData = mime->data(SOKRATES_MIME_LIST);
		if(arData.size() > 0)
		{
			lstDropped=XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
			//lstDropped.Dump();
		}
		QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
		if(arDataType.size() > 0)
		{
			bool bOK;
			nEntityID=arDataType.toInt(&bOK);
			if (!bOK) nEntityID=-1;
		}

		if (nEntityID>=0 && lstDropped.getRowCount()>0)
		{
			emit NewDataDropped(nEntityID,lstDropped);
		}
		event->accept();
		return;
	}
	
	event->ignore();
}


void Table_NMRXRelation::Data2CustomWidget(int nRow, int nCol)
{
	if (nCol==m_nCol_X_Widget)
	{
		if (m_nX_TableID==BUS_CAL_INVITE)
		{
			Table_NMRXRelation_WidgetInvite *pX_Widget = dynamic_cast<Table_NMRXRelation_WidgetInvite *>(cellWidget(nRow,nCol));
			if (!pX_Widget)
			{
				pX_Widget = new Table_NMRXRelation_WidgetInvite(this,nRow);
				pX_Widget->setEnabled(m_bEdit);
				connect(pX_Widget,SIGNAL(DataChanged(int)),this,SLOT(On_X_WidgetChanged(int)));
				setCellWidget(nRow,nCol,pX_Widget);
			}

			DbRecordSet rowInvite=m_plstData->getDataRef(nRow,"X_RECORD").value<DbRecordSet>();
			Q_ASSERT(rowInvite.getRowCount()>0);
			if (rowInvite.getRowCount()>0)
			{
				//qDebug()<<"row refresh: "<<nRow<<" invite: "<<rowInvite.getDataRef(0,"BCIV_INVITE").toInt();
				pX_Widget->m_pInvited->setChecked(rowInvite.getDataRef(0,"BCIV_INVITE").toInt());
				pX_Widget->m_pInviteSent->setChecked(rowInvite.getDataRef(0,"BCIV_IS_SENT").toInt());
				pX_Widget->m_pStatus->setCurrentIndex(rowInvite.getDataRef(0,"BCIV_STATUS").toInt());
				pX_Widget->m_pBySokrates->setChecked(rowInvite.getDataRef(0,"BCIV_BY_NOTIFICATION").toInt());
				pX_Widget->m_pByEmail->setChecked(rowInvite.getDataRef(0,"BCIV_BY_EMAIL").toInt());
				pX_Widget->m_pBySMS->setChecked(rowInvite.getDataRef(0,"BCIV_BY_SMS").toInt());
			}
		}

	}
	else if (nCol==m_nCol_Desc) //multiline
	{
		QTextEdit *pText = dynamic_cast<QTextEdit *>(cellWidget(nRow,nCol));
		if (!pText)
		{
			pText = new QTextEdit;
			pText->setReadOnly(true);
			setCellWidget(nRow,nCol,pText);
		}
		pText->setHtml(m_plstData->getDataRef(nRow,"BNMR_DESCRIPTION").toString());
	}
}

void Table_NMRXRelation::CustomWidget2Data(int nRow, int nCol)
{
	if (nCol==m_nCol_X_Widget)
	{
		if (m_nX_TableID==BUS_CAL_INVITE)
		{
			Table_NMRXRelation_WidgetInvite *pX_Widget = dynamic_cast<Table_NMRXRelation_WidgetInvite *>(cellWidget(nRow,nCol));
			if (pX_Widget)
			{
				DbRecordSet rowInvite=m_plstData->getDataRef(nRow,"X_RECORD").value<DbRecordSet>();
				Q_ASSERT(rowInvite.getRowCount()>0);

				rowInvite.setData(0,"BCIV_INVITE",(int)pX_Widget->m_pInvited->isChecked());
				rowInvite.setData(0,"BCIV_IS_SENT",(int)pX_Widget->m_pInviteSent->isChecked());
				rowInvite.setData(0,"BCIV_STATUS",pX_Widget->m_pStatus->currentIndex());
				rowInvite.setData(0,"BCIV_BY_NOTIFICATION",(int)pX_Widget->m_pBySokrates->isChecked());
				rowInvite.setData(0,"BCIV_BY_EMAIL",(int)pX_Widget->m_pByEmail->isChecked());
				rowInvite.setData(0,"BCIV_BY_SMS",(int)pX_Widget->m_pBySMS->isChecked());

				m_plstData->setData(nRow,"X_RECORD",rowInvite);
			}
		}
	}
}


void Table_NMRXRelation::On_X_WidgetChanged(int nRow)
{
	CustomWidget2Data(nRow,m_nCol_X_Widget);
}


Table_NMRXRelation_WidgetInvite::Table_NMRXRelation_WidgetInvite(Table_NMRXRelation *parent,int nRow)
{
	m_Parent=parent;
	m_nRow=nRow;

	m_pInvited= new QCheckBox(tr("Invite"));
	m_pInviteSent= new QCheckBox(tr("Invitation Sent"));

	m_pStatus = new QComboBox;
	m_pStatus->setEditable(false);
	m_pStatus->addItem("");
	m_pStatus->addItem(tr("Confirmed"));
	m_pStatus->addItem(tr("Rejected"));
	m_pStatus->addItem(tr("Answered"));

	m_pBySokrates= new QCheckBox;
	m_pByEmail= new QCheckBox;
	m_pBySMS= new QCheckBox;

	connect(m_pInvited,SIGNAL(stateChanged(int)),this,SLOT(OnStateChanged(int)));
	connect(m_pInviteSent,SIGNAL(stateChanged(int)),this,SLOT(OnStateChanged(int)));
	connect(m_pBySokrates,SIGNAL(stateChanged(int)),this,SLOT(OnStateChanged(int)));
	connect(m_pByEmail,SIGNAL(stateChanged(int)),this,SLOT(OnStateChanged(int)));
	connect(m_pBySMS,SIGNAL(stateChanged(int)),this,SLOT(OnStateChanged(int)));


	QSize buttonSize(18,18);
	QLabel *btnBySokrates = new QLabel;
	btnBySokrates->setPixmap(QPixmap (":sokrates_ico.png"));

	//QToolButton *btnBySokrates = new QToolButton;
	//btnBySokrates->setIcon(QIcon(":Sokrates.ico"));
	btnBySokrates->setToolTip(tr("Send Invite By Sokrates"));
	//btnBySokrates->setAutoRaise(false);
	btnBySokrates->setMaximumSize(buttonSize);
	btnBySokrates->setMinimumSize(buttonSize);
	//btnBySokrates->setEnabled(false);

	//QToolButton *btnByEmail = new QToolButton;
	//btnByEmail->setIcon(QIcon(":Email16.png"));

	QLabel *btnByEmail = new QLabel;
	btnByEmail->setPixmap(QPixmap (":Email16.png"));

	btnByEmail->setToolTip(tr("Send Invite By Email"));
	//btnByEmail->setAutoRaise(true);
	btnByEmail->setMaximumSize(buttonSize);
	btnByEmail->setMinimumSize(buttonSize);
	//if (m_Parent->m_nAssignedTableID!=BUS_CM_CONTACT)
	//	btnByEmail->setEnabled(false);

	//QToolButton *btnBySMS = new QToolButton;
	///btnBySMS->setIcon(QIcon(":Comm_SMS16.png"));
	QLabel *btnBySMS = new QLabel;
	btnBySMS->setPixmap(QPixmap (":Comm_SMS16.png"));

	btnBySMS->setToolTip(tr("Send Invite By SMS"));
	//btnBySMS->setAutoRaise(true);
	btnBySMS->setMaximumSize(buttonSize);
	btnBySMS->setMinimumSize(buttonSize);
	//if (m_Parent->m_nAssignedTableID!=BUS_CM_CONTACT)
	//	btnBySMS->setEnabled(false);


	QHBoxLayout *hbox = new QHBoxLayout;

	hbox->addWidget(m_pInvited);
	hbox->addStretch();
	hbox->addWidget(m_pBySokrates);
	hbox->addWidget(btnBySokrates);
	hbox->addWidget(m_pByEmail);
	hbox->addWidget(btnByEmail);
	hbox->addWidget(m_pBySMS);
	hbox->addWidget(btnBySMS);
	hbox->setSpacing(1);
	hbox->setContentsMargins(0,0,0,0);

	//connect(btnBySokrates,SIGNAL(clicked()),this,SLOT(OnBySokrates()));
	//connect(btnByEmail,SIGNAL(clicked()),this,SLOT(OnByEmail()));
	//connect(btnBySMS,SIGNAL(clicked()),this,SLOT(OnBySMS()));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addLayout(hbox);
	vbox->addWidget(m_pInviteSent);
	vbox->addWidget(m_pStatus);
	vbox->setSpacing(1);
	vbox->setContentsMargins(3,0,3,0);
	this->setLayout(vbox);

}

Table_NMRXRelation_WidgetInvite::~Table_NMRXRelation_WidgetInvite()
{


}



