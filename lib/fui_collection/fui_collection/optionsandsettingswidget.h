#ifndef OPTIONSANDSETTINGSWIDGET_H
#define OPTIONSANDSETTINGSWIDGET_H

#include <QWidget>
#include "ui_optionsandsettingswidget.h"

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "fuibase.h"

#include "bus_core/bus_core/optionsandsettingsorganizer.h"
#include "settingsbase.h"

//******************************************************************************//

class OptionsAndSettingsWidget : public FuiBase
{
    Q_OBJECT

public:
    OptionsAndSettingsWidget(bool bIsOption, int nPersonID = -1, QWidget *parent = 0);
    ~OptionsAndSettingsWidget();

	QString GetFUIName();

private:
    Ui::OptionsAndSettingsWidgetClass ui;

	void InitializeAppOptionsWidgets();
	void InitializePersonalSettingsWidgets();
	void InitializeSelectionTree();
	void Reload();
	DbRecordSet GetChangedOptionsOrSettingsRecordSet();
	bool CanClose();
	void on_selectionChange(int nEntityRecordID);

	Status						m_Status;
	OptionsAndSettingsManager	*m_pOptionsAndSettingsManager;			//< Options and settings manager - for case that person is not logged one.
	int							m_nPersonID;							//< Person ID - when called normally it's -1.
	bool						m_bIsOption;							//< Options or personal settings switch.
	QHash<int, SettingsBase*>	m_hshOptionsSetIDToWidget;				//< Options (or personal settings) set ID to it's widget hash.
	
protected:
	virtual void closeEvent(QCloseEvent *event);

private slots:
	void on_treeSelectionChanged();
	void on_Save_pushButton_clicked();
	void on_Cancel_pushButton_clicked();
};

#endif // OPTIONSANDSETTINGSWIDGET_H
