#include "fui_buscostcenters.h"
#include "bus_client/bus_client/selectionpopup.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "common/common/entity_id_collection.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;


FUI_BusCostCenters::FUI_BusCostCenters(QWidget *parent)
    : FuiBase(parent)
{
	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);
	InitFui(ENTITY_BUS_COST_CENTER, BUS_COST_CENTER, dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	EnableHierarchyCodeCheck("BCTC_CODE");

	ui.btnButtonBar->RegisterFUI(this);
	ui.frameSelection->Initialize(ENTITY_BUS_COST_CENTER,true,true);	//init selection controller
	InitializeFUIWidget(ui.splitter);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	//Connect fields to the data:
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_BUS_COST_CENTER);
	m_GuiFieldManagerMain->RegisterField("BCTC_CODE",ui.txtCode);
	m_GuiFieldManagerMain->RegisterField("BCTC_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BCTC_ACTIVE_FLAG",ui.chkActive);
	m_GuiFieldManagerMain->RegisterField("BCTC_DESCRIPTION",ui.txtDescription);
	//---------------------------------------
	//init ACTUAL ORG SMR:
	//---------------------------------------
	ui.frameOrganization->Initialize(&m_lstData,"BCTC_ORGANIZATION_ID");
	SetActualOrganizationHandler(ui.frameOrganization,"BCTC_ORGANIZATION_ID");
	//set edit to false:
	SetMode(MODE_EMPTY);
	ui.txtName->setFocus();
}

FUI_BusCostCenters::~FUI_BusCostCenters()
{
	delete m_GuiFieldManagerMain;
}

//when programatically change data, call this:
void FUI_BusCostCenters::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frameOrganization->RefreshDisplay();
}

//when changing state, call this:
void FUI_BusCostCenters::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameSelection->setEnabled(true);
		ui.frameDetails->setEnabled(true);			//needed because of inverse state of one btn
		ui.frameOrganization->setEnabled(true);

		ui.txtCode->setEnabled(false);
		ui.txtName->setEnabled(false);
		ui.chkActive->setEnabled(false);
		ui.txtDescription->setEnabled(false);

		//ui.btnButtonBar->SetMode(nMode); //this is necessary to reeanble buttons!
	}
	else
	{
		ui.frameSelection->setEnabled(false);
		ui.frameDetails->setEnabled(true);
		ui.frameOrganization->setEnabled(false);

		ui.txtCode->setEnabled(true);
		ui.txtName->setEnabled(true);
		ui.chkActive->setEnabled(true);
		ui.txtDescription->setEnabled(true);

		//Set focus to Pers. No:
		ui.txtCode->setFocus(Qt::OtherFocusReason);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}


void FUI_BusCostCenters::on_cmdInsert()
{
	FuiBase::on_cmdInsert();
	ui.txtName->setFocus();
}


//set filters and refresh display for current Actual Org
void FUI_BusCostCenters::SetFiltersForCurrentActualOrganization(int nActualOrganizationID)
{
	//notify left selection (specific for each FUI):
	ui.frameSelection->ClearLocalFilter();
	ui.frameSelection->GetLocalFilter()->SetFilter("BCTC_ORGANIZATION_ID",nActualOrganizationID);
	ui.frameSelection->ReloadData();

}

//get FUI name
QString FUI_BusCostCenters::GetFUIName()
{
	if(m_nEntityRecordID>0)
		return tr("Cost Center: ")+ui.frameSelection->GetCalculatedName(m_nEntityRecordID);
	else
		return tr("Cost Center");
}

void FUI_BusCostCenters::DataPrepareForInsert(Status &err)
{
	DataClear();
	m_lstData.addRow();
	m_lstData.setData(0,"BCTC_ACTIVE_FLAG",1);
	m_lstData.setData(0,"BCTC_ICON","");	
}
void FUI_BusCostCenters::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	if(m_lstData.getDataRef(0,"BCTC_CODE").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Code is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"BCTC_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Name is mandatory field!"));	return;	}
}