#include "dlg_orderform.h"
#include <QDesktopServices>

//GLOBAL BO SET:
#include "bus_client/bus_client/clientmanager.h"
extern ClientManager *g_BoSet;	

Dlg_OrderForm::Dlg_OrderForm(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle(tr("Order Now"));
}

Dlg_OrderForm::~Dlg_OrderForm()
{

}


void Dlg_OrderForm::on_btnTrial_clicked()
{
	//open web
	QString strUrl=g_BoSet->GetOrderFormURL();
	QDesktopServices::openUrl(strUrl);
	done(1);

}
void Dlg_OrderForm::on_btnFull_clicked()
{
	//open web
	QString strUrl=g_BoSet->GetOrderFormURL();
	QDesktopServices::openUrl(strUrl);
	done(1);
}
void Dlg_OrderForm::on_btnCancel_clicked()
{

	done(1);
}


void Dlg_OrderForm::on_btnContinue_clicked()
{
	done(0);
}
