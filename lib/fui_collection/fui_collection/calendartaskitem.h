#ifndef CALENDARTASKITEM_H
#define CALENDARTASKITEM_H

#include <QGraphicsPixmapItem>
#include <QDateTime>
#include <QGraphicsView>
#include <QGraphicsObject>
#include <QObject>

class CalendarTaskItem : public QObject, public QGraphicsPixmapItem
{
	Q_OBJECT;
public:
	CalendarTaskItem(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, QPixmap Pixmap, int nEntityID, int nEntityType, int nTypeID, QGraphicsView *pCalendarGraphicsView, QGraphicsItem *parent = 0);
	~CalendarTaskItem();

public slots:
	void on_UpdateCalendar();

protected:
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

private:
	int m_nCENT_ID;
	int m_nBTKS_ID;
	int m_nCENT_SYSTEM_TYPE_ID;
	int m_nEntityID;
	int m_nEntityType;
	QDateTime m_datTaskDate;
	bool m_bIsStart;
	int m_nTypeID;
	QGraphicsView *m_pCalendarGraphicsView;

signals:
	void ItemClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID);
	void ItemDoubleClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID);
};

#endif // CALENDARTASKITEM_H