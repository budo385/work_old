#include "sidebarfui_desktop.h"
#include "gui_core/gui_core/thememanager.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt* g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;		

#include "bus_client/bus_client/commgridviewhelper_client.h"
#include "menuitems.h"

SideBarFui_Desktop::SideBarFui_Desktop(QWidget *parent)
	: SideBarFui_Base(parent)
{
	ui.setupUi(this);

	ui.labelComm->hide();

	m_nFuiTypeID=MENU_COMM_CENTER;
	setProperty("sidebar_fui",true);
	BuildMenu();
	//--------------------------
	//Set Grid.
	//--------------------------
	ui.widgetCommGrid->Initialize(CommGridViewHelper::DESKTOP_GRID, this);

	Status err;
	DbRecordSet lstData;
	_SERVER_CALL(BusCommunication->GetDefaultDesktopFilterViews(err,g_pClientManager->GetPersonID(),lstData))
	_CHK_ERR(err);
	if (lstData.getRowCount())
		ui.widgetCommGrid->DesktopStartupInitialization(g_pClientManager->GetPersonID(),lstData.getDataRef(0,"BUSCV_ID").toInt());

	g_ChangeManager.registerObserver(this);
}
	

SideBarFui_Desktop::~SideBarFui_Desktop()
{
	g_ChangeManager.unregisterObserver(this);

}

void SideBarFui_Desktop::SetCurrentFUI(bool bCurrent)
{
	m_bCurrentFUI=bCurrent;
	ui.widgetCommGrid->SetCurrentGridInSidebarMode(bCurrent);
}

void SideBarFui_Desktop::BuildMenu()
{
	SideBarFui_Base::BuildMenu();

	setStyleSheet("QFrame[sidebar_fui=\"true\"] "+ThemeManager::GetSidebar_Bkg() + " "+ThemeManager::GetGlobalWidgetStyle());
	ui.frameWorkSpace->setStyleSheet("QFrame#frameWorkSpace "+ThemeManager::GetSidebarChapter_Bkg());
	ui.labelTitle->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	ui.labelTitle->setAlignment(Qt::AlignLeft);
	ui.labelTitle->setText(tr("Workspace"));
	

	//ui.widgetCommGrid->setStyleSheet("QWidget {background-color: rgb(233,236,240)}");
}


QFrame* SideBarFui_Desktop::GetFrameCommToolBar()
{
	return ui.frameCommToolBar;
}
QFrame* SideBarFui_Desktop::GetFrameCommParent()
{
	return ui.frameCommParent;
}

DropZone_MenuWidget* SideBarFui_Desktop::GetDropZone_MenuWidget()
{
	return ui.widgetDropZone;
}

QLabel* SideBarFui_Desktop::GetFrameCommLabel()
{
	return ui.labelComm;
}

	

void SideBarFui_Desktop::GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strMail,int &nEmailReplyID,QString &strPhone,QString &strInternet)
{
	//get selection from grid:brb...
}

void SideBarFui_Desktop::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{

	if (pSubject==&g_ChangeManager)
	{
		if(nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
		{
			OnThemeChanged();
			return;
		}
	}
}

void SideBarFui_Desktop::OnThemeChanged()
{
	SideBarFui_Base::OnThemeChanged();

	setStyleSheet("QFrame[sidebar_fui=\"true\"] "+ThemeManager::GetSidebar_Bkg());
	ui.frameWorkSpace->setStyleSheet("QFrame#frameWorkSpace "+ThemeManager::GetSidebarChapter_Bkg());
	ui.labelTitle->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());

}