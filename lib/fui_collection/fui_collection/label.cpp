#include "label.h"

//strSyle = "QLabel {color:white; background: qlineargradient(x1:0, y1:0, x2:0.01,y2:0,stop:0 rgb(255,255,255), stop:1 rgb(128,128,128)); border-width:1px; border-style: outset ; \
//		  border-top-color: gray; border-left-color: gray; border-right-color: black; border-bottom-color: black; }";

Label::Label(Widgets widgetType, QWidget * parent /*= 0*/, Qt::WindowFlags f /*= 0*/)
	: QLabel(parent, f)
{
	QString strSyle;
	if (widgetType == HEADER_ITEM)
		strSyle = "QLabel {color:white; background: rgb(128,128,128); border-width:2px; border-style: outset; \
				  border-top-color: gray; border-left-color: gray; border-right-color: black; border-bottom-color: black; }";
	else if (widgetType == VERTICAL_ITEM)
		strSyle = "QLabel {color:white; background-color: rgb(128,128,128)}";
	else if (widgetType == BOTTOM_LEFT_ITEM)
		strSyle = "QLabel {color:white; background-color: rgb(128,128,128); border-width:1px; border-style: inset; \
				  border-top-color: white; border-left-color: rgb(128,128,128); border-right-color: rgb(128,128,128); border-bottom-color: rgb(128,128,128); }";
	else if (widgetType == TOP_LEFT_ITEM)
		strSyle = "QLabel {color:white; background-color: rgb(128,128,128); border-width:1px; border-style: inset; \
				  border-top-color: rgb(128,128,128); border-left-color: rgb(128,128,128); border-right-color: rgb(128,128,128); border-bottom-color: white; }";
	else if (widgetType == TOP_RIGHT_ITEM)
		strSyle = "QLabel {color:white; background-color: rgb(128,128,128); border-width:1px; border-style: inset; \
				  border-top-color: rgb(128,128,128); border-left-color: white; border-right-color: rgb(128,128,128); border-bottom-color: rgb(128,128,128); }";

	if (widgetType == TOP_RIGHT_ITEM)
		setPixmap(QPixmap(":Rescale.png"));

	setStyleSheet(strSyle);
}

Label::~Label()
{

}
