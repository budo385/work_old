#include "sidebarfui_project.h"
#include <QClipboard>
#include "gui_core/gui_core/thememanager.h"
#include "reportmanager.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "communicationactions.h"
#include "gui_core/gui_core/gui_helper.h"
#include "gui_core/gui_core/macros.h"
#include "fui_projects.h"
#include "fui_collection/fui_collection/fui_calendar.h"
#include "common/common/datahelper.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"
#include <QProgressDialog>

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "fuimanager.h"
extern FuiManager g_objFuiManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;				
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;

SideBarFui_Project::SideBarFui_Project(QWidget *parent)
: SideBarFui_Base(parent),m_pTxtActual(NULL),m_btnHideList(NULL),m_btnHideActual(NULL),m_bHideContactPaneOnDetailClickFirstTime(true),
m_Spacer(NULL),m_bCommGridInit(false),m_pTxt1(NULL),m_pTxt2(NULL)

{
	ui.setupUi(this);

	m_nFuiTypeID=MENU_PROJECTS;
	setProperty("sidebar_fui",true);
	m_RowProjectData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));
	BuildMenu();

	ui.frameActualParent->setVisible(false);

	g_objFuiManager.RegisterSideBarCommMenuSelector(MENU_PROJECTS,this);
	ui.txtDesc->setReadOnly(true);	
	g_ChangeManager.registerObserver(this);
	ui.stackedWidget->setCurrentIndex(0); //on detail
}

SideBarFui_Project::~SideBarFui_Project()
{
	g_objFuiManager.UnRegisterSideBarCommMenuSelector(MENU_PROJECTS,this);
	g_ChangeManager.unregisterObserver(this);
}

void SideBarFui_Project::SetCurrentFUI(bool bCurrent)
{
	m_bCurrentFUI=bCurrent;
	ui.widgetCommGrid->SetCurrentGridInSidebarMode(bCurrent);
	if (m_bCurrentFUI)
		QTimer::singleShot(0,this,SLOT(OnSetFindFocusDelayed()));
}

void SideBarFui_Project::BuildMenu()
{
	SideBarFui_Base::BuildMenu();


	//-----------------------------
	//LIST HEADER:
	//-----------------------------
	QSize buttonSize1(22, 22);
	//m_btnHideList= new StyledPushButton(":Icon_PulseMinus.mng","mng",true,this,"QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex(),"",0,0);
	m_btnHideList= new StyledPushButton(this,":Icon_Minus12.png",":Icon_Minus12.png","QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex(),"",0,0);
	m_btnHideList->hide();
	ui.labelComm->hide();

	//create buttons & labels:
	m_pTxt1		= new QLabel(this);
	m_pTxt1->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	m_pTxt1->setAlignment(Qt::AlignLeft);
	m_pTxt1->setText(tr("Project List"));

	QSize buttonSize(17, 17);
	StyledPushButton *pBtnAddTempl1	=	new StyledPushButton(this,":Icon_ProjectFromTempl16.png",":Icon_ProjectFromTempl16.png","","",0,0);
	StyledPushButton *pBtnAdd1		=	new StyledPushButton(this,":DbIcon_Insert16.png",":DbIcon_Insert16.png","","",0,0);
	StyledPushButton *pBtnPrintList	=	new StyledPushButton(this,":Icon_Printer16.png",":Icon_Printer16.png","","",0,0);

	//connect signals
	connect(m_btnHideList,SIGNAL(clicked()),this,SLOT(OnHideList()));
	connect(pBtnAdd1,SIGNAL(clicked()),this,SLOT(OnList_InsertClick()));
	connect(pBtnPrintList,SIGNAL(clicked()),this,SLOT(OnList_PrintClick()));
	connect(pBtnAddTempl1,SIGNAL(clicked()),this,SLOT(OnList_NewFromTemplate()));
	//size
	m_btnHideList->	setMaximumSize(buttonSize1);
	m_btnHideList->	setMinimumSize(buttonSize1);
	pBtnAdd1		->setMaximumSize(buttonSize);
	pBtnAdd1		->setMinimumSize(buttonSize);
	pBtnPrintList	->setMaximumSize(buttonSize);
	pBtnPrintList	->setMinimumSize(buttonSize);
	pBtnAddTempl1	->setMaximumSize(buttonSize);
	pBtnAddTempl1	->setMinimumSize(buttonSize);
	

	pBtnAddTempl1->setToolTip(tr("New From Template"));
	pBtnAdd1->setToolTip(tr("Add Project"));
	pBtnPrintList->setToolTip(tr("Print Selected Projects"));

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addWidget(m_btnHideList);
	buttonLayout->addSpacing(5);
	buttonLayout->addWidget(m_pTxt1);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnAdd1);
	buttonLayout->addWidget(pBtnAddTempl1);
	buttonLayout->addWidget(pBtnPrintList);
	buttonLayout->setContentsMargins(0,0,0,0);
	buttonLayout->setSpacing(2);
	ui.frameList->setLayout(buttonLayout);

	//-----------------------------
	//PROJECT:
	//-----------------------------
	ui.frameProject->Initialize(ENTITY_BUS_PROJECT,false, true, true, false);	//init selection controller
	ui.frameProject->registerObserver(this);
	
	//-----------------------------
	//ACTUAL HEADER:
	//-----------------------------
	//m_btnHideActual= new StyledPushButton(":Icon_PulsePlus.mng","mng",true,this,"QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex(),"",0,0);

	//create buttons & labels:
	m_pTxt2		= new QLabel(this);
	m_pTxt2->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	m_pTxt2->setAlignment(Qt::AlignLeft);
	m_pTxt2->setText(tr("Actual Project"));

	//QSize buttonSize(17, 17);
	StyledPushButton *pBtnAddTempl2	=	new StyledPushButton(this,":Icon_ProjectFromTempl16.png",":Icon_ProjectFromTempl16.png","","",0,0);
	StyledPushButton *pBtnAdd2		=	new StyledPushButton(this,":DbIcon_Insert16.png",":DbIcon_Insert16.png","","",0,0);
	StyledPushButton *pBtnEdit		=	new StyledPushButton(this,":DbIcon_Edit16.png",":DbIcon_Edit16.png","","",0,0);
	StyledPushButton *pBtnDelete	=	new StyledPushButton(this,":DbIcon_Delete16.png",":DbIcon_Delete16.png","","",0,0);
	StyledPushButton *pBtnView		=	new StyledPushButton(this,":DbIcon_View16.png",":DbIcon_View16.png","","",0,0);
	//StyledPushButton *pBtnPic		=	new StyledPushButton(this,":reddot.png",":reddot.png","","",0,0);


	//connect signals
//	connect(m_btnHideActual,SIGNAL(clicked()),this,SLOT(OnHideActual()));
	connect(pBtnAdd2,SIGNAL(clicked()),this,SLOT(OnList_InsertClick()));
	connect(pBtnEdit,SIGNAL(clicked()),this,SLOT(OnList_EditClick()));
	connect(pBtnDelete,SIGNAL(clicked()),this,SLOT(OnList_DeleteClick()));
	connect(pBtnView,SIGNAL(clicked()),this,SLOT(OnList_ViewClick()));
	connect(pBtnAddTempl2,SIGNAL(clicked()),this,SLOT(OnList_NewFromTemplate()));
	//connect(pBtnPic,SIGNAL(clicked()),this,SLOT(OnList_PicClick()));

	pBtnAdd2->setToolTip(tr("Add Project"));
	pBtnEdit->setToolTip(tr("Edit Project"));
	pBtnDelete->setToolTip(tr("Delete Project"));
	pBtnView->setToolTip(tr("View Project Detail"));
	pBtnAddTempl2->setToolTip(tr("New From Template"));

	//size
	//m_btnHideActual	->setMaximumSize(buttonSize1);
	//m_btnHideActual	->setMinimumSize(buttonSize1);
	pBtnAdd2		->setMaximumSize(buttonSize);
	pBtnAdd2		->setMinimumSize(buttonSize);
	pBtnEdit		->setMaximumSize(buttonSize);
	pBtnEdit		->setMinimumSize(buttonSize);
	pBtnDelete		->setMaximumSize(buttonSize);
	pBtnDelete		->setMinimumSize(buttonSize);
	pBtnView		->setMaximumSize(buttonSize);
	pBtnView		->setMinimumSize(buttonSize);
	pBtnAddTempl2	->setMaximumSize(buttonSize);
	pBtnAddTempl2	->setMinimumSize(buttonSize);
	//pBtnPic			->setMaximumSize(buttonSize);
	//pBtnPic			->setMinimumSize(buttonSize);


	QHBoxLayout *buttonLayout1 = new QHBoxLayout;
	buttonLayout1->addWidget(m_btnHideActual);
	buttonLayout1->addSpacing(5);
	buttonLayout1->addWidget(m_pTxt2);
	buttonLayout1->addStretch(1);
	buttonLayout1->addWidget(pBtnAdd2);
	buttonLayout1->addWidget(pBtnAddTempl2);
	buttonLayout1->addWidget(pBtnEdit);
	buttonLayout1->addWidget(pBtnDelete);
	buttonLayout1->addWidget(pBtnView);
	buttonLayout1->setContentsMargins(0,0,0,0);
	buttonLayout1->setSpacing(2);

	ui.frameActual->setLayout(buttonLayout1);
	ui.frameDetailToolBar->setVisible(false);
	ui.stackedWidget->setVisible(false);

	m_pTxtActual		= new QLabel(this);
	m_pTxtActual->setAlignment(Qt::AlignLeft);
	m_pTxtActual->setWordWrap(true);

	QHBoxLayout *txtlayout = new QHBoxLayout;
	txtlayout->addSpacing(3);
	txtlayout->addWidget(m_pTxtActual);
	txtlayout->addSpacing(3);
	txtlayout->setContentsMargins(0,0,0,0);
	txtlayout->setSpacing(0);
	ui.frameActualName->setLayout(txtlayout);

	//cntx menu:
	ui.frameActual->setContextMenuPolicy(Qt::CustomContextMenu);
	ui.frameActualName->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui.frameActual,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnCustomContextMenu(const QPoint &)));
	connect(ui.frameActualName,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnCustomContextMenu(const QPoint &)));
	m_pCntxReload = new QAction(tr("Reload Project Data"), this);
	connect(m_pCntxReload, SIGNAL(triggered()), this, SLOT(ReloadProjectData()));
	m_pCntxCopy = new QAction(tr("Copy Name to Clipboard"), this);
	connect(m_pCntxCopy, SIGNAL(triggered()), this, SLOT(CopyName2Clipboard()));


	//-----------------------------
	//DETAIL HEADER:
	//-----------------------------

	QSize buttonSize_detail(36, 36);
	StyledPushButton *pBtnQuickInfo		=	new StyledPushButton(this,":Projects24b_Mirror.png",":Projects24b_Mirror.png","","",0,0);
	StyledPushButton *pBtnCommGrid		=	new StyledPushButton(this,":Desktop32.png",":Desktop32.png","","",0,0);
	StyledPushButton *pBtnNMRXUsers		=	new StyledPushButton(this,":Icon_Contacts_24_Mirror.png",":Icon_Contacts_24_Mirror.png","","",0,0);
	StyledPushButton *pBtnCalendar		=	new StyledPushButton(this,":Calendar36_Miror.png",":Calendar36_Miror.png","","",0,0);

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
		pBtnCalendar->setVisible(false);

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_PROJECT_CALENDARS))
		pBtnCalendar->setVisible(false);

	//connect signals
	connect(pBtnQuickInfo,SIGNAL(clicked()),this,SLOT(OnDetail_QuickInfoClick()));
	connect(pBtnCommGrid,SIGNAL(clicked()),this,SLOT(OnDetail_CommGridClick()));
	connect(pBtnNMRXUsers,SIGNAL(clicked()),this,SLOT(OnDetail_NMRXUsersClick()));
	connect(pBtnCalendar,SIGNAL(clicked()),this,SLOT(OnDetail_CalendarClick()));

	pBtnQuickInfo->setToolTip(tr("Project Quick Info"));
	pBtnCommGrid->setToolTip(tr("Project Documents & Communication History"));
	pBtnNMRXUsers->setToolTip(tr("Assigned Users & Contacts"));
	pBtnCalendar->setToolTip(tr("Calendar"));

	//size
	pBtnQuickInfo		->setMaximumSize(buttonSize_detail);
	pBtnQuickInfo		->setMinimumSize(buttonSize_detail);
	pBtnCommGrid		->setMaximumSize(buttonSize_detail);
	pBtnCommGrid		->setMinimumSize(buttonSize_detail);
	pBtnNMRXUsers		->setMaximumSize(buttonSize_detail);
	pBtnNMRXUsers		->setMinimumSize(buttonSize_detail);
	pBtnCalendar		->setMaximumSize(buttonSize_detail);
	pBtnCalendar		->setMinimumSize(buttonSize_detail);

	//Tool m_ProgressBar.
	QHBoxLayout *buttonLayout_detail = new QHBoxLayout;
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnQuickInfo);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnCommGrid);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnNMRXUsers);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnCalendar);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->setContentsMargins(0,2,0,0);
	buttonLayout_detail->setSpacing(0);

	ui.frameDetailToolBar->setLayout(buttonLayout_detail);



	//-----------------------------
	//DETAIL :
	//-----------------------------

	connect(ui.widgetCommGrid,SIGNAL(NeedNewData(int)),this,SLOT(SubMenuRequestedNewData(int)));
	
	ui.frmLeader->Initialize(ENTITY_BUS_PERSON,&m_RowProjectData, "BUSP_LEADER_ID");
	ui.frmResponsible->Initialize(ENTITY_BUS_PERSON,&m_RowProjectData, "BUSP_RESPONSIBLE_ID");
	ui.frmOrganization->Initialize(ENTITY_BUS_ORGANIZATION,&m_RowProjectData, "BUSP_ORGANIZATION_ID");
	ui.frmDepartment->Initialize(ENTITY_BUS_DEPARTMENT,&m_RowProjectData, "BUSP_DEPARTMENT_ID");
	ui.frmLeader->SetEditMode(false);
	ui.frmResponsible->SetEditMode(false);
	ui.frmOrganization->SetEditMode(false);
	ui.frmDepartment->SetEditMode(false);
	ui.date_late_deadline->setEnabled(false);
	ui.date_start->setEnabled(false);
	ui.date_deadline->setEnabled(false);
	ui.date_complete->setEnabled(false);

	//assigments:
	QList<int> lst;
	lst<<NMRXManager::PAIR_CONTACT_PROJECT<<NMRXManager::PAIR_PERSON_PROJECT<<NMRXManager::PAIR_PROJECT_CONTACT;
	ui.widgetCommPerson->Initialize(tr("Assigned Users And Contacts"),BUS_PROJECT,lst,BUS_CM_CONTACT,BUS_PERSON);

	//ALL BKG STYLESHEETS:
	setStyleSheet("QFrame[sidebar_fui=\"true\"] "+ThemeManager::GetSidebar_Bkg()+ " "+ThemeManager::GetGlobalWidgetStyle());
	ui.frameProjectParent->setStyleSheet("QFrame#frameProjectParent "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameActualParent->setStyleSheet("QFrame#frameActualParent "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameActualName->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg()+" "+"QLabel "+ThemeManager::GetSidebarActualName_Font());
	ui.frameQuick_Bkg->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg());

	//ui.splitter->setStyleSheet("QSplitter::handle {image: url(:Theme_Blue_Temple_Sidebar_Footer_Handle.png)}  ");
	ui.splitter->setStyleSheet("QSplitter::handle "+ThemeManager::GetSidebar_FooterHandle());
	/*
	QString strStyle="QSplitter::handle "+ThemeManager::GetSidebar_CenterHandle();
	ui.splitter->setStyleSheet(strStyle);
	ui.splitter->handle(1)->splitter()->setMaximumHeight(5);
	ui.splitter->handle(1)->setMaximumHeight(5);
	int x=ui.splitter->handleWidth ();
	//ui.splitter->setHandleWidth (10);
	*/

	ui.stackedWidget->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());

	ui.frameActualParent->setMaximumHeight(76);
	OnDetail_CommGridClick();//set tab to project: issue 1238

}

void SideBarFui_Project::CopyName2Clipboard()
{
	QString strName=m_pTxtActual->text();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strName);
}

QFrame* SideBarFui_Project::GetFrameCommToolBar()
{
	return ui.frameCommToolBar;
}

DropZone_MenuWidget* SideBarFui_Project::GetDropZone_MenuWidget()
{
	return ui.widgetDropZone;
}

void SideBarFui_Project::GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strEmail,int &nEmailReplyID,QString &strPhone,QString &strInternet)
{
	if (m_nEntityRecordID>0 && m_RowProjectData.getRowCount()>0)
	{
		DbRecordSet xlstProject;
		xlstProject.addColumn(QVariant::Int,"BUSP_ID");
		xlstProject.addRow();
		xlstProject.setData(0,0,m_nEntityRecordID);

		*lstProjects=xlstProject;

		//if (ui.stackedWidget->currentIndex()==1)
		//{
			//issue 1817
			/*
			//which contact is actual:
			DbRecordSet xlstContacts;
			xlstContacts.addColumn(QVariant::Int,"BCNT_ID");
			DbRecordSet record;
			int nContactID;
			
			if (m_bCommGridInit)
				if(ui.widgetCommGrid->GetContactSAPNE()->GetCurrentEntityRecord(nContactID,record))
				{
					xlstContacts.addRow();
					xlstContacts.setData(0,0,nContactID);
				}
			*lstContacts=xlstContacts;
			*/
		//}
		//else

		//reset contact:
		DbRecordSet xlstContacts;
		xlstContacts.addColumn(QVariant::Int,"BCNT_ID");
		*lstContacts=xlstContacts;

		if (nSubMenuType==CommunicationMenu_Base::SUBMENU_EMAIL)
		{
				//project team: only for email:
				//get selected contacts:
				DbRecordSet lstSelectedContacts;
				if (ui.stackedWidget->currentIndex()==2) //only use selected if shown--
					ui.widgetCommPerson->GetSelectedData(BUS_CM_CONTACT,lstSelectedContacts);

				//if none then open wizard
				if (lstSelectedContacts.getRowCount()==0)
				{
					CommunicationActions::getProjectContacts(m_nEntityRecordID,lstSelectedContacts);
				}

				*lstContacts=lstSelectedContacts;
				//issue 1450: if project team then enable serial mode:
				SetSerialEmailMode();
		}

	}
}


void SideBarFui_Project::OnHideList()
{
	bool bVisible=ui.frameProject->IsSideBarVisible();
	if (bVisible)
	{
		ShowActualList(false);
	}
	else
	{
			ShowActualList(true);
	}


}
void SideBarFui_Project::OnHideActual()
{
	bool bVisible=ui.stackedWidget->isVisible();
	if (bVisible)
	{
		ShowActualFrame(false);
	}
	else
	{
		ShowActualFrame(true);
	}


}
void SideBarFui_Project::OnList_InsertClick()
{
	//B.T: disable this funny stuff: when insert go with new window
	//if current fui is selected to curr id, then pass to him, else open new FUI
	//FuiBase* pActive=g_objFuiManager.GetActiveFUI();
	//int nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_EMPTY);
	//if (g_objFuiManager.GetFUI(nNewFUI)==pActive) //do not send signal twice if it already active->fui_cont is linked to drop zone
	//	return;

	g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_INSERT);

}


void SideBarFui_Project::OnList_EditClick()
{
	if (m_nEntityRecordID>0)
	{
		//if current fui is selected to curr id, then pass to him, else open new FUI
		//int nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_READ,m_nEntityRecordID);
		//FuiBase* pActive=g_objFuiManager.GetActiveFUI();
		//if (g_objFuiManager.GetFUI(nNewFUI)==pActive) //do not send signal twice if it already active->fui_cont is linked to drop zone
		//	return;

		g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_EDIT,m_nEntityRecordID);
	}
}
void SideBarFui_Project::OnList_DeleteClick()
{
	if (m_nEntityRecordID>0)
	{
		int nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_READ,m_nEntityRecordID,true);
		QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
		FUI_Projects* pFUI=dynamic_cast<FUI_Projects*>(pFUIWidget);
		if (pFUI)
		{
			pFUI->on_cmdDelete();
			pFUI->close();
		}

		//if current fui is selected to curr id, then pass to him, else open new FUI
		//int nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_READ,m_nEntityRecordID,);
		//FuiBase* pActive=g_objFuiManager.GetActiveFUI();
		//if (g_objFuiManager.GetFUI(nNewFUI)==pActive) //do not send signal twice if it already active->fui_cont is linked to drop zone
		//	return;

		//g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_DELETE,m_nEntityRecordID);
	}
}
void SideBarFui_Project::OnList_ViewClick()
{
	if (m_nEntityRecordID>0)
	{
		g_objFuiManager.OpenFUI(MENU_PROJECTS,true,false,FuiBase::MODE_READ,m_nEntityRecordID,false);
	}
	//g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_READ,m_nEntityRecordID);
}

void SideBarFui_Project::OnList_NewFromTemplate()
{
	//if current fui is selected to curr id, then pass to him, else open new FUI
	FuiBase* pActive=g_objFuiManager.GetActiveFUI();
	int nNewFUI=-1;
	if (m_nEntityRecordID>0)
		nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_READ,m_nEntityRecordID);
	else
		nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS);

	if (g_objFuiManager.GetFUI(nNewFUI)==pActive) //do not send signal twice if it already active->fui_cont is linked to drop zone
		return;

	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_Projects* pFuiW=dynamic_cast<FUI_Projects*>(pFUI);
	if (pFuiW)
	{
		pFuiW->on_NewFromTemplate();
	}
}

void SideBarFui_Project::OnList_NewStructFromTemplate()
{
	//if current fui is selected to curr id, then pass to him, else open new FUI
	FuiBase* pActive=g_objFuiManager.GetActiveFUI();
	int nNewFUI=-1;
	if (m_nEntityRecordID>0)
		nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_READ,m_nEntityRecordID);
	else
		nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS);

	if (g_objFuiManager.GetFUI(nNewFUI)==pActive) //do not send signal twice if it already active->fui_cont is linked to drop zone
		return;

	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_Projects* pFuiW=dynamic_cast<FUI_Projects*>(pFUI);
	if (pFuiW)
	{
		pFuiW->on_NewStructFromTemplate();
	}
}


// when Conatct or Project selection changes, set SAPNE to selected values
void SideBarFui_Project::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	/*
	if(ui.frameProject==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_PROJECT_LIST_LOAD){
		on_ProjectListLoad();
	}
	else if(ui.frameProject==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_PROJECT_LIST_CREATE){
		on_ProjectListCreate();
	}
	else if(ui.frameProject==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_PROJECT_LIST_MODIFY){
		on_ProjectListModify();
	}
	else if(ui.frameProject==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_PROJECT_LIST_DELETE){
		on_ProjectListDelete();
	}
	*/
	if (pSubject==&g_ChangeManager)
	{
		if(nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
		{
			OnThemeChanged();
			return;
		}

		if(nMsgCode==ChangeManager::GLOBAL_SIDEBAR_FUI_PROJECT_ACTIVE && nMsgDetail>0) //change selection to contact if valid id
		{
			if (m_nEntityRecordID==nMsgDetail) //issue 1487
				return;
			m_nEntityRecordID=nMsgDetail;
			ui.frameProject->SetCurrentEntityRecord(m_nEntityRecordID,true); //if from distant FUI
			ReloadProjectData();
			return;
		}

		if(nMsgDetail!=ENTITY_BUS_PROJECT) return; //not our entity, exit

		//if EDIT mode, our FUI just inserted new id, assign back to FK link
		if(nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED || nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
		{
			DbRecordSet record=val.value<DbRecordSet>();
			if (record.getRowCount()>0 && record.getColumnIdx("BUSP_ID")>=0) //if valid format
			{
				if (m_nEntityRecordID==record.getDataRef(0,"BUSP_ID").toInt() && m_nEntityRecordID!=-1) //issue 1487: skip reloading comm data & other if same contact edited....
				{
					ReloadProjectData(false);
					return;
				}
				m_nEntityRecordID=record.getDataRef(0,"BUSP_ID").toInt();
				ui.frameProject->SetCurrentEntityRecord(m_nEntityRecordID,true); //if from distant FUI
				ReloadProjectData();
				return;
			}
		}

		if(nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED)
		{
			if(ui.frameProject->GetCalculatedName(m_nEntityRecordID).isEmpty())
			{
				m_nEntityRecordID=-1;
				ReloadProjectData();
				return;
			}
		}

		//reload selected:
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			ReloadProjectData();
		}

		return;
	}

	if (nMsgCode==SelectorSignals::SELECTOR_ON_DOUBLE_CLICK)
	{
		OnSelector_DoubleClick();
		return;
	}
	if(nMsgCode==SelectorSignals::SELECTOR_ON_SHOW_DETAILS)
	{
		OnSelector_ShowContactDetails();
		return;
	}
	if(nMsgCode==SelectorSignals::SELECTOR_ON_SHOW_DETAILS_NEW_WINDOW)
	{
		OnSelector_ShowContactDetails(true);
		return;
	}
/*//issue 1487
	if(nMsgCode==SelectorSignals::SELECTOR_DATA_CHANGED) //deleted
	{
		if(ui.frameProject->GetCalculatedName(m_nEntityRecordID).isEmpty())
		{
			m_nEntityRecordID=-1;
		}
		ReloadProjectData();
		return;
	}
	*/

	if(nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		if (m_nEntityRecordID==nMsgDetail) //issue 1487
			return;
		m_nEntityRecordID=nMsgDetail;
		ReloadProjectData();
		return;
	}
	else if(nMsgCode==SelectorSignals::SELECTOR_ON_INSERT)	{
		OnList_InsertClick();
		return;
	}
	else if(nMsgCode==SelectorSignals::SELECTOR_ON_DELETE){
		OnList_DeleteClick();
		return;
	}
	else if (nMsgCode==SelectorSignals::SELECTOR_ON_EDIT){
		OnList_EditClick();
		return;
	}
	else if (nMsgCode==SelectorSignals::SELECTOR_ON_INSERT_NEW_FROM_TEMPLATE)
	{
		OnList_NewFromTemplate();
		return;
	}
	else if (nMsgCode==SelectorSignals::SELECTOR_ON_INSERT_NEW_STRUCT_FROM_TEMPLATE)
	{
		OnList_NewStructFromTemplate();
		return;
	}
	else if(nMsgCode==SelectorSignals::SELECTOR_FIND_WIDGET_ENTER_PRESSED) 	//issue 2263: on enter on find widget open actual area!!!
	{
		ShowActualFrame(true);
		return;
	}

}

void SideBarFui_Project::ReloadProjectData(bool bReloadOtherData)
{
	QString strName=ui.frameProject->GetCalculatedName(m_nEntityRecordID);

	if (m_nEntityRecordID>0)
	{
		if (!ui.frameActualParent->isVisible()) //if first time make visible, show actual:
		{
			ui.frameActualParent->setVisible(true);
		}
		DbRecordSet lstNmrx1;
		DbRecordSet lstNmrx1Filter=ui.widgetCommPerson->GetFilterForRead(m_nEntityRecordID);

		Status err;
		_SERVER_CALL(BusProject->ReadProjectDataSideBar(err,m_nEntityRecordID,m_RowProjectData,lstNmrx1Filter,lstNmrx1))
		_CHK_ERR(err);

		if (bReloadOtherData)
		{
			if (m_bCommGridInit)
				ui.widgetCommGrid->SetData(m_nEntityRecordID);
		}
		ui.widgetCommPerson->Reload(strName,m_nEntityRecordID,true,&lstNmrx1);
	}
	else
	{
		if (m_bCommGridInit)
			ui.widgetCommGrid->ClearCommGrid();  
		ui.widgetCommPerson->Invalidate();
	}

	RefreshQuickInfo();
	RefreshActualName();

	//issue 1467:
	QTimer::singleShot(0,this,SLOT(OnScrollToCurrentItem()));
}


void SideBarFui_Project::SetActualText(QString strText)
{
	if (!m_pTxtActual)return;
	m_pTxtActual->setText(strText);

}


void SideBarFui_Project::OnDetail_QuickInfoClick()
{
	ui.stackedWidget->setCurrentIndex(0);
}
void SideBarFui_Project::OnDetail_CommGridClick()
{
	if(!m_bCommGridInit)
	{
		ui.widgetCommGrid->Initialize(CommGridViewHelper::PROJECT_GRID, this);
		if (m_nEntityRecordID > 0)
			ui.widgetCommGrid->SetData(m_nEntityRecordID);
		m_bCommGridInit=true;
	}
	ui.stackedWidget->setCurrentIndex(1);

}
void SideBarFui_Project::OnDetail_NMRXUsersClick()
{
	ui.stackedWidget->setCurrentIndex(2);
}
void SideBarFui_Project::OnDetail_CalendarClick()
{
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_PROJECT_CALENDARS))
	{
		return;
	}

	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(QDate::currentDate(),from, to);

	QString strName=ui.frameProject->GetCalculatedName(m_nEntityRecordID);
	
	DbRecordSet lstProjects;
	lstProjects.addColumn(QVariant::Int,"BUSP_ID");
	lstProjects.addColumn(QVariant::String,"BUSP_NAME");
	lstProjects.addRow();
	lstProjects.setData(0,0,m_nEntityRecordID);
	lstProjects.setData(0,1,strName);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		//lstContacts.Dump();
		pFui->Initialize(from,to,NULL,&lstProjects,NULL);
	}
}

//from address, phones, emails, sets data
void SideBarFui_Project::RefreshActualName()
{
	//NAME:
	QString strName;
	if (m_nEntityRecordID>=0)
		strName=ui.frameProject->GetCalculatedName(m_nEntityRecordID);
	SetActualText(strName);
	m_pTxtActual->setToolTip(strName);
}

//from address, phones, emails, sets data
void SideBarFui_Project::RefreshQuickInfo()
{
	ui.txtDesc->setHtml ("");

	QDate dateStart,dateDeadline,dateComplet,dateLateDeadline;
	if (m_RowProjectData.getRowCount()>0)
	{
		dateStart=m_RowProjectData.getDataRef(0,"BUSP_START_DATE").toDate();
		dateDeadline=m_RowProjectData.getDataRef(0,"BUSP_DEADLINE_DATE").toDate();
		dateComplet=m_RowProjectData.getDataRef(0,"BUSP_COMPLETION_DATE").toDate();
		dateLateDeadline=m_RowProjectData.getDataRef(0,"BUSP_LATEST_COMPLETION_DATE").toDate();
		ui.txtDesc->setHtml (m_RowProjectData.getDataRef(0,"BUSP_DESCRIPTION").toString());
	}

	ui.date_late_deadline->setDate(dateLateDeadline);
	ui.date_start->setDate(dateStart);
	ui.date_deadline->setDate(dateDeadline);
	ui.date_complete->setDate(dateComplet);


	ui.frmLeader->RefreshDisplay();
	ui.frmResponsible->RefreshDisplay();
	ui.frmOrganization->RefreshDisplay();
	ui.frmDepartment->RefreshDisplay();
}




void SideBarFui_Project::OnCustomContextMenu(const QPoint &pos)
{
	QMenu CnxtMenu(this);
	CnxtMenu.addAction(m_pCntxReload);
	CnxtMenu.addAction(m_pCntxCopy);
	CnxtMenu.exec(ui.frameActualName->mapToGlobal(pos));
}

QFrame* SideBarFui_Project::GetFrameCommParent()
{
	return ui.frameCommParent;
}

QLabel* SideBarFui_Project::GetFrameCommLabel()
{
	return ui.labelComm;
}

void SideBarFui_Project::OnSelector_DoubleClick()
{
	bool bShift = false;
	Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();
	if(Qt::ShiftModifier == (Qt::ShiftModifier & keys))
		bShift = true;

	if (m_nEntityRecordID>0){
		int nGID = g_objFuiManager.OpenFUI(MENU_PROJECTS,true,false,FuiBase::MODE_READ,m_nEntityRecordID);
		if(bShift){
				//minimize immediately to make it an avatar
				QWidget *pFui = g_objFuiManager.GetFUIWidget(nGID);
				if(pFui){
					((FUI_Projects *)pFui)->m_bHideOnMinimize = false; //we will hide ti below	
					pFui->showMinimized();
					QTimer::singleShot(0,pFui,SLOT(hide()));//B.T. delay hide
				}
		}
	}
}



void SideBarFui_Project::OnDropZone_NewDataDropped(int nDropType, DbRecordSet lstDroppedData)
{
	//if current fui is selected to curr id, then pass to him, else open new FUI
	FuiBase* pActive=g_objFuiManager.GetActiveFUI();
	int nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_EMPTY,m_nEntityRecordID,true);
	if (g_objFuiManager.GetFUI(nNewFUI)==pActive) //do not send signal twice if it already active->fui_cont is linked to drop zone
		return;

	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_Projects* pFuiW=dynamic_cast<FUI_Projects*>(pFUI);
	if (pFuiW)
	{
		if (pFuiW->GetEntityRecordID()!=m_nEntityRecordID)
		{
			pFuiW->on_selectionChange(m_nEntityRecordID);
		}
		g_objFuiManager.SetActiveFUI(pFuiW);
		pFuiW->OnCEMenu_NewDataDropped(nDropType,lstDroppedData);
		pFuiW->show();
	}
}

void SideBarFui_Project::OnScrollToCurrentItem()
{
	ui.frameProject->GetTreeWidget()->scrollToItem(ui.frameProject->GetTreeWidget()->currentItem());
}
void SideBarFui_Project::ShowActualFrameDetails(int nTabIdx)
{
	ShowActualFrame(true);
	ui.stackedWidget->setCurrentIndex(nTabIdx);
}
void SideBarFui_Project::OpenCurrentContactInNewWindow()
{
	OnList_ViewClick();
}

void SideBarFui_Project::ShowActualFrame(bool bShow,bool bSkipAutoCollapseOthers,bool bSkipInsertSpacer)
{
	bool bVisible=ui.stackedWidget->isVisible();
	if (!bVisible && !bShow || bVisible && bShow)
		return;

	if (!bShow)
	{
		m_btnHideActual->setIcon(":Icon_PulsePlus.mng");
		ui.frameDetailToolBar->setVisible(false);
		ui.stackedWidget->setVisible(false);

		if (!ui.frameProject->IsSideBarVisible() && !bSkipInsertSpacer)
		{
			if (!m_Spacer)
			{
				dynamic_cast<QVBoxLayout*>(layout())->insertStretch (1,1); 
				m_Spacer=layout()->itemAt(1);
			}
			else
			{
				m_Spacer->spacerItem()->changeSize(this->width(),5,QSizePolicy::Preferred,QSizePolicy::Expanding);
			}
		}
		ui.frameActualParent->setMaximumHeight(76);
	}
	else
	{
		ui.frameActualParent->setMaximumHeight(16777215);

		if (m_Spacer)
		{
			m_Spacer->spacerItem()->changeSize(this->width(),5,QSizePolicy::Preferred,QSizePolicy::Fixed);
		}

		//1850:
		//ShowActualList(false,true,true); //hide list if visible:

		m_btnHideActual->setIcon(":Icon_PulseMinus.mng");
		ui.frameDetailToolBar->setVisible(true);
		ui.stackedWidget->setVisible(true);


	}

	if (!bSkipAutoCollapseOthers)
	{
		bool b1=ui.stackedWidget->isVisible();
		bool b2=ui.frameProject->IsSideBarVisible();
		emit ChapterCollapsed(m_nFuiTypeID,1,(!b1 && !b2)); //notify parent to resize
	}

}

void SideBarFui_Project::ShowActualList(bool bShow,bool bSkipAutoCollapseOthers,bool bSkipInsertSpacer)
{
	bool bVisible=ui.frameProject->IsSideBarVisible();
	if (!bVisible && !bShow || bVisible && bShow)
		return;

	if (!bShow)
	{
		m_btnHideList->setIcon(":Icon_Plus12.png");
		ui.frameProject->SetSideBarVisible(false);

		if (!ui.stackedWidget->isVisible()  && !bSkipInsertSpacer)
		{
			if (!m_Spacer)
			{
				dynamic_cast<QVBoxLayout*>(layout())->insertStretch (1,1); 
				m_Spacer=layout()->itemAt(1);
			}
			else
			{
				m_Spacer->spacerItem()->changeSize(this->width(),5,QSizePolicy::Preferred,QSizePolicy::Expanding);
			}
		}
		ui.frameProjectParent->setMaximumHeight(91);
	}
	else
	{
		ui.frameProjectParent->setMaximumHeight(16777215);
		if (m_Spacer)
		{
			m_Spacer->spacerItem()->changeSize(this->width(),5,QSizePolicy::Preferred,QSizePolicy::Fixed);
			/*
			m_Spacer=layout()->takeAt(1);
			if (m_Spacer)
				delete m_Spacer;
			m_Spacer=NULL;
			*/
		}

		m_btnHideList->setIcon(":Icon_Minus12.png");
		ui.frameProject->SetSideBarVisible(true);
	}

	if (!bSkipAutoCollapseOthers)
	{
		bool b1=ui.stackedWidget->isVisible();
		bool b2=ui.frameProject->IsSideBarVisible();
		emit ChapterCollapsed(m_nFuiTypeID,0,(!b1 && !b2)); //notify parent to resize
	}

	//issue 1467:
	QTimer::singleShot(0,this,SLOT(OnScrollToCurrentItem()));
}


//issue 1273: 1st DZ, 2nd ACL, 3rd AC
bool SideBarFui_Project::HideVisibleFramesIfNeeded(int nFrameOpened)
{
	//if coordinates are > window height then do some hiding:
	if (GUI_Helper::IsWidgetOutOfScreen(this->topLevelWidget()))
	{
		//DZ:
		if (nFrameOpened!=2)
			if (!ui.widgetDropZone->IsHidden())
			{
				ui.widgetDropZone->SetHidden(true,true);
				return true;
			}
		//ACL:
		if (nFrameOpened!=0)
			if (ui.frameProject->IsSideBarVisible())
			{
				ShowActualList(false,true);
				return true;
			}
			//AC:
		if (nFrameOpened!=1)
			if (ui.stackedWidget->isVisible())
			{
				ShowActualFrame(false,true);
				return true;
			}
	}

	return false;
}



void SideBarFui_Project::OnSelector_ShowContactDetails(bool bNewWindow)
{
	int nNewFUI;
	if (bNewWindow)
		nNewFUI=g_objFuiManager.OpenFUI(MENU_PROJECTS,true,false,FuiBase::MODE_READ,m_nEntityRecordID,true);
	else
		nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_PROJECTS,FuiBase::MODE_READ,m_nEntityRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_Projects* pFuiW=dynamic_cast<FUI_Projects*>(pFUI);
	if (pFuiW)
	{
		pFuiW->SetCurrentTab(1);
		pFuiW->show();
	}
}


void SideBarFui_Project::OnThemeChanged()
{
	SideBarFui_Base::OnThemeChanged();
	if(m_btnHideList)m_btnHideList->setStyleSheet("QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex());
	if(m_pTxt1)m_pTxt1->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	if(m_btnHideActual)m_btnHideActual->setStyleSheet("QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex());
	if(m_pTxt2)m_pTxt2->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	setStyleSheet("QFrame[sidebar_fui=\"true\"] "+ThemeManager::GetSidebar_Bkg());
	ui.frameProjectParent->setStyleSheet("QFrame#frameProjectParent "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameActualParent->setStyleSheet("QFrame#frameActualParent "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameActualName->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg()+" "+"QLabel "+ThemeManager::GetSidebarActualName_Font());
	ui.frameQuick_Bkg->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg());

}

void SideBarFui_Project::OnList_PrintClick()
{
	DbRecordSet lstProjects;
	//ui.frameProject->GetSelection(lstProjects);
	//if (lstProjects.getRowCount()>0)
	//{//8
		//QList<DbRecordSet> lstData;
		//lstData.append(lstProjects);
		ReportManager Rpt;
		Rpt.PrintReport(ReportRegistration::REPORT_PROJECT_LIST); //,&lstData); ->MB requeste, whole wizard
	//}

}

void SideBarFui_Project::OnSetFindFocusDelayed()
{
	ui.frameProject->SetFocusOnFind();
}


