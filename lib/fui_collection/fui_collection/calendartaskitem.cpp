#include "calendartaskitem.h"
#include "calendartableview.h"
#include "calendargraphicsview.h"

CalendarTaskItem::CalendarTaskItem(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, QPixmap Pixmap, int nEntityID, int nEntityType, int nTypeID, QGraphicsView *pCalendarGraphicsView, QGraphicsItem *parent /*= 0*/)
	: QGraphicsPixmapItem(parent)
{
	m_nCENT_ID = nCENT_ID;
	m_nBTKS_ID = nBTKS_ID;
	m_nCENT_SYSTEM_TYPE_ID = nCENT_SYSTEM_TYPE_ID;
	m_datTaskDate = datTaskDate;
	m_bIsStart	  = bIsStart;
	m_nEntityID	  = nEntityID;
	m_nEntityType = nEntityType;
	m_nTypeID	  = nTypeID;
	m_pCalendarGraphicsView = pCalendarGraphicsView;
	setFlag(QGraphicsItem::ItemIsSelectable);
	setPixmap(Pixmap);
	setZValue(3);
}

CalendarTaskItem::~CalendarTaskItem()
{

}

void CalendarTaskItem::on_UpdateCalendar()
{
	QRect rect = dynamic_cast<CalendarTableView*>(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView())->getItemRectangle(m_nEntityID, m_nEntityType, m_datTaskDate, m_datTaskDate, true);
	setPos(rect.topLeft().x()+dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getVertHeaderWidth(), rect.topLeft().y()+dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTotalHeaderHeight());
}

void CalendarTaskItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	setSelected(false);
	emit ItemClicked(m_nCENT_ID, m_nBTKS_ID, m_nCENT_SYSTEM_TYPE_ID, m_datTaskDate, m_bIsStart, m_nEntityID, m_nEntityType, m_nTypeID);
}

void CalendarTaskItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	setSelected(false);
	emit ItemDoubleClicked(m_nCENT_ID, m_nBTKS_ID, m_nCENT_SYSTEM_TYPE_ID, m_datTaskDate, m_bIsStart, m_nEntityID, m_nEntityType, m_nTypeID);
}
