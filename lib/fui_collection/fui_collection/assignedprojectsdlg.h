#ifndef ASSIGNEDPROJECTSDLG_H
#define ASSIGNEDPROJECTSDLG_H

#include <QtWidgets/QDialog>
#include "ui_assignedprojectsdlg.h"

class AssignedProjectsDlg : public QDialog
{
	Q_OBJECT

public:
	AssignedProjectsDlg(DbRecordSet lstDataNMRXProjects, QWidget *parent = 0, int nProjectCurrentID = -1);
	~AssignedProjectsDlg();

	int GetSelectedProject();

private slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();

private:
	Ui::AssignedProjectsDlg ui;
};

#endif // ASSIGNEDPROJECTSDLG_H
