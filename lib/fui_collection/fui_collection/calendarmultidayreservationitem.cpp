#include "calendarmultidayreservationitem.h"

#include <QPen>
#include <QBrush>
#include "qmath.h"

CalendarMultiDayReservationItem::CalendarMultiDayReservationItem(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, QDateTime startDateTime, QDateTime endDateTime, 
																 CalendarMultiDayGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent /*= 0*/)
																 : QGraphicsItem(parent)
{
	m_nEntityID				= nEntityID;
	m_nEntityType			= nEntityType;
	m_nBCRS_ID				= nBCRS_ID;
	m_nBCRS_IS_POSSIBLE		= nBCRS_IS_POSSIBLE;
	m_startDateTime			= startDateTime;
	m_endDateTime			= endDateTime;
	m_pCalendarMultiDayGraphicsView	= pCalendarGraphicsView;

	m_nHeight= 7;
	m_nWidth = 0;

	setZValue(3);
}

CalendarMultiDayReservationItem::~CalendarMultiDayReservationItem()
{
	m_pCalendarMultiDayGraphicsView	= NULL;
}

QRectF CalendarMultiDayReservationItem::boundingRect() const
{
	return QRectF(0,0,m_nWidth, m_nHeight);
}

void CalendarMultiDayReservationItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	painter->setRenderHint(QPainter::Antialiasing);

	//Sinusoida.
	QPen pen;
	if (m_nBCRS_IS_POSSIBLE)
		pen = QPen(QBrush(Qt::blue), 1.8);
	else
		pen = QPen(QBrush(Qt::red), 1.8);
	painter->setPen(pen);

	QPainterPath path;
	QVector<QPointF> vect;
	for (int i = 0; i<m_nWidth; i++)
	{
		qreal x = i;
		qreal y1 = (m_nHeight/2)*sin(0.8*x);
		qreal y = y1+m_rectItemRectangle.height()-(m_nHeight/2);
		vect << QPointF(x,y);
	}
	painter->drawLines(vect);
}

void CalendarMultiDayReservationItem::on_UpdateCalendar()
{
	QRect rect = dynamic_cast<CalendarHeaderTableItem*>(m_pCalendarMultiDayGraphicsView->getTableView())->getItemRectangle(m_nEntityID, m_nEntityType, m_startDateTime, m_endDateTime);
	//QString s = m_startDateTime.toString();
	//QString ss = m_endDateTime.toString();
	m_rectItemRectangle.setWidth(rect.width());
	m_rectItemRectangle.setHeight(rect.height());
	m_nWidth = rect.width();
	//int n = m_pCalendarMultiDayGraphicsView->getMultiHeaderHeight();
	setPos(rect.topLeft().x()/*+m_pCalendarMultiDayGraphicsView->getVertHeaderWidth()*/, rect.topLeft().y()+m_pCalendarMultiDayGraphicsView->getCurrentMultiHeaderHeight());
}
