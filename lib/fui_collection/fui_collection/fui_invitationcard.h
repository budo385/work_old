#ifndef FUI_InvitationCard_H
#define FUI_InvitationCard_H

#include <QWidget>
#include "generatedfiles/ui_FUI_InvitationCard.h"
#include "dropzone_menuwidget.h"
#include "FuiBase.h"

class FUI_InvitationCard : public FuiBase
{
	Q_OBJECT

public:
	FUI_InvitationCard(QWidget *parent = 0);
	~FUI_InvitationCard();

	void SetLabel(QString &strTxt);
	void SetInvitationInfo(int nCalEvID, int nPartID, int nOwnerID, int nInviteID, int nPersonID);
	void SetTaskDate(QString &strTxt);
	void SetOwnerInitials(QString &strTxt);
	void SetTaskDescTooltip(const QString &strTxt);

	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

protected:
	void mouseDoubleClickEvent(QMouseEvent * event);
	void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);

protected:
	void OnThemeChanged();
    QPoint  m_dragPosition;
	QImage	m_imageBkg;
	QWidget *m_pParent;

	int m_nEntityRecordID;
	int m_nInviteMode_nPartID;
	int m_nInviteMode_InviteID;
	int m_nInviteMode_nContactId_ConfirmedBy;
	int m_nInviteMode_nPersonID_ConfirmedBy;
	int m_nInviteMode_nOwnerID;
	bool m_bInviteMode_bNotifyOwner;

private slots:
	void OnMenu_Close();
	void on_RejectInvite();
	void on_AcceptInvite();

private:
	Ui::FUI_InvitationCardClass ui;
};

#endif // FUI_InvitationCard_H
