#ifndef DEPTSELECTWIZPAGE_H
#define DEPTSELECTWIZPAGE_H

#include "generatedfiles/ui_deptselectwizpage.h"
#include "gui_core/gui_core/wizardpage.h"
#include "common/common/dbrecordset.h"

class DeptSelectWizPage : public WizardPage
{
    Q_OBJECT

public:
    DeptSelectWizPage(int nWizardPageID, QString strPageTitle);
    ~DeptSelectWizPage();
	void Initialize();
	

private:
    Ui::DeptSelectWizPageClass ui;

	void AddSelectedDepartments();

	DbRecordSet m_recDeptRecordSet;

private slots:
	void on_AddSelected_pushButton_clicked();
	void on_RemoveAll_pushButton_clicked();
	void on_RemoveSelected_pushButton_clicked();
	void on_AddAll_pushButton_clicked();
	void CompleteChanged();
};

#endif // DEPTSELECTWIZPAGE_H
