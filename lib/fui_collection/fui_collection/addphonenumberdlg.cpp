#include "addphonenumberdlg.h"
#include "bus_core/bus_core/formatphone.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "fui_collection/fui_collection/communicationactions.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/formatphone.h"

AddPhoneNumberDlg::AddPhoneNumberDlg(QString strNumber, QStringList lstPhoneTypes, int nSelectTypeIdx, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	ui.btnReformat->setIcon(QIcon(":Reformat.png"));
	
	ui.txtNumber->setText(strNumber);
	ui.comboBox->addItems(lstPhoneTypes);
	if(nSelectTypeIdx >= 0)
		ui.comboBox->setCurrentIndex(nSelectTypeIdx);
}


AddPhoneNumberDlg::~AddPhoneNumberDlg()
{
}

QString AddPhoneNumberDlg::GetNumber()
{
	return ui.txtNumber->text();
}
	
int	AddPhoneNumberDlg::GetTypeIdx()
{
	return ui.comboBox->currentIndex();
}

void AddPhoneNumberDlg::on_btnReformat_clicked()
{
	QString strNumber = GetNumber();

	//reformat number
	DbRecordSet recPhone;
	recPhone.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE));
	recPhone.addRow();
	recPhone.setData(0,"BCMP_FULLNUMBER", strNumber); //type=skype/TOFIX later for other interfaces:
	//recPhone.setData(0,"BCMP_TYPE_ID",	ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE));	//TOFIX
	FormatPhone phone;
	phone.ReFormatPhoneNumbers(recPhone);

	ui.txtNumber->setText(recPhone.getDataRef(0,"BCMP_FULLNUMBER").toString());
}

void AddPhoneNumberDlg::on_btnOK_clicked()
{
	done(1);
}

void AddPhoneNumberDlg::on_btnCancel_clicked()
{
	done(0);
}
