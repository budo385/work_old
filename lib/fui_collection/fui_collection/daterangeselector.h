#ifndef DATERANGESELECTOR_H
#define DATERANGESELECTOR_H

#include <QWidget>
#include "ui_daterangeselector.h"

class DateRangeSelector : public QWidget
{
    Q_OBJECT

public:
    DateRangeSelector(QWidget *parent = 0);
    ~DateRangeSelector();

	void Initialize(QDateTime &dateFrom, QDateTime &dateTo, int nRange);
	void GetDates(QDateTime &dtFrom, QDateTime &dtTo, int &nRange);
	void UseAsFocusOut(bool bUseAsFocusOut){m_bUseAsFocusOut = bUseAsFocusOut;};

private:
    Ui::DateRangeSelectorClass ui;

	bool m_bUseAsFocusOut;

signals:
	void onDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange);
	void onFocusOut(QDateTime &dtFrom, QDateTime &dtTo, int nRange);

private slots:
	void on_btnPrev_clicked();
	void on_btnNext_clicked();
	void on_btnCurDateRange_clicked();
	void OnDateChanged(const QDate &);
	void OnComboChanged(int nIdx);
	void OnFromDateFocusOut();
	void OnToDateFocusOut();
};

#endif // DATERANGESELECTOR_H
