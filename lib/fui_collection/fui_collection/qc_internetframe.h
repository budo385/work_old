#ifndef QC_INTERNETFRAME_H
#define QC_INTERNETFRAME_H

#include <QWidget>
#include "ui_qc_internetframe.h"
#include "common/common/dbrecordset.h"

class QC_InternetFrame : public QWidget
{
	Q_OBJECT

public:
	QC_InternetFrame(QWidget *parent = 0);
	~QC_InternetFrame();

	void Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType = 0 /*0-Large,1-small widget*/);
	void AddInternetAddress(DbRecordSet recInternet);

private:
	Ui::QC_InternetFrameClass ui;

	void SetupGUIwidgets(DbRecordSet recData);
	void SetDataToRecordSet(QString strRecordSetFieldName, QString strValue);
	void SetIntDataToRecordSet(QString strRecordSetFieldName, int nValue);

	DbRecordSet *m_recFullContactData;
	DbRecordSet m_recLocalEntityRecordSet;
	int			m_nCurrentRow;
	int			m_nParentType;
	QWidget		*m_pParentWidget;

private slots:
	void on_RowChanged(int nRow);
	void on_internet_lineEdit_textChanged(const QString &text);
	void on_description_lineEdit_textChanged(const QString &text);

	void on_addSelected_toolButton_clicked();
	void on_addSelected_toolButton_2_clicked();
	void on_isDefault_checkBox_stateChanged(int state);
};

#endif // QC_INTERNETFRAME_H
