#ifndef SETTINGS_PROJECTS_H
#define SETTINGS_PROJECTS_H

#include <QWidget>
#include "ui_settings_projects.h"
#include "settingsbase.h"

class Settings_Projects : public SettingsBase
{
	Q_OBJECT

public:
	Settings_Projects(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
	~Settings_Projects();

	DbRecordSet GetChangedValuesRecordSet();

protected:
	bool m_bIsOptionsPage;

private:
	Ui::Settings_ProjectsClass ui;

	void LoadViewSelector();
	void LoadStoredLists();
};

#endif // SETTINGS_PROJECTS_H
