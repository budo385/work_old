#include "fui_ce_types.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/picturehelper.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

static int CommEntityTypeFromComboIdx(int nCboIdx);

FUI_CE_Types::FUI_CE_Types(QWidget *parent)
    : FuiBase(parent)
{
	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);
	InitFui(ENTITY_CE_TYPES, CE_TYPE, dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	EnableHierarchyCodeCheck("CET_CODE");
	ui.frameSelection->Initialize(ENTITY_CE_TYPES);	//init selection controller
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_CE_TYPE);
	m_GuiFieldManagerMain->RegisterField("CET_CODE",ui.txtCode);
	m_GuiFieldManagerMain->RegisterField("CET_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("CET_MAPPED_NAME",ui.txtMapName);
	m_GuiFieldManagerMain->RegisterField("CET_DESCRIPTION",ui.txtDescription);

	//set edit to false:
	SetMode(MODE_EMPTY);

	ui.txtName->setFocus();

	ui.cboRootType->addItem(tr("Document types"));
	ui.cboRootType->addItem(tr("Email types"));
	ui.cboRootType->addItem(tr("Phone Call types"));
	ui.cboRootType->addItem(tr("Calendar Event types"));

	ui.btnModifyIcon->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnModifyIcon->setToolTip(tr("Modify Icon"));

	connect(ui.cboRootType, SIGNAL(currentIndexChanged(int)), this, SLOT(OnRootTypeComboChanged(int)));
	connect(ui.btnModifyIcon,SIGNAL(clicked()),this,SLOT(OnModifyIcon()));

	m_nCurRootTypeIdx = 0;
	OnRootTypeComboChanged(0);	// set initial filter
}

FUI_CE_Types::~FUI_CE_Types()
{
	delete m_GuiFieldManagerMain;
}

//when programatically change data, call this:
void FUI_CE_Types::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();
	RefreshIcon();
}

//when changing state, call this:
void FUI_CE_Types::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameSelection->setEnabled(true);
		ui.cboRootType->setEnabled(true);

		m_GuiFieldManagerMain->SetEditMode(false);

		//ui.frameDetails->setEnabled(false);

		ui.btnButtonBar->SetMode(nMode); //this is necessary to reeanble buttons!
		ui.btnModifyIcon->setEnabled(false);
	}
	else
	{
		ui.frameSelection->setEnabled(false);
		ui.cboRootType->setEnabled(false);

		m_GuiFieldManagerMain->SetEditMode(true);
		//ui.frameDetails->setEnabled(true);

		//Set focus
		ui.txtCode->setFocus(Qt::OtherFocusReason);
		ui.btnModifyIcon->setEnabled(true);
	}

	RefreshDisplay();
	FuiBase::SetMode(nMode);//set mode
}


bool FUI_CE_Types::on_cmdOK()
{
	m_lstData.setData(0,"CET_COMM_ENTITY_TYPE_ID", CommEntityTypeFromComboIdx(m_nCurRootTypeIdx));
	return FuiBase::on_cmdOK();
}

void FUI_CE_Types::on_cmdInsert()
{
	FuiBase::on_cmdInsert();
	ui.txtName->setFocus();
}

void FUI_CE_Types::OnRootTypeComboChanged(int nIndex)
{
	m_nCurRootTypeIdx = nIndex;

	//refilter the tree
	ui.frameSelection->ClearLocalFilter();
	ui.frameSelection->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",CommEntityTypeFromComboIdx(m_nCurRootTypeIdx));
	ui.frameSelection->ReloadData();

	//clean the main record
	m_nEntityRecordID = -1;
	ui.txtCode->setText("");
	ui.txtName->setText("");
	ui.txtDescription->SetHtml("");
}

int FUI_CE_Types::CommEntityTypeFromComboIdx(int nCboIdx)
{
	//map combo index to type ID
	switch(nCboIdx)
	{
		case 0:	return GlobalConstants::CE_TYPE_DOCUMENT;	//0;	//document
		case 1:	return GlobalConstants::CE_TYPE_EMAIL;		//2;	//email
		case 2:	return GlobalConstants::CE_TYPE_VOICE_CALL;	//1;	//voice call
		case 3:	return GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART;	
	}
	Q_ASSERT(false);	//unknown type
	return -1;
}


void FUI_CE_Types::SetDefaultType(int nType)
{
	switch(nType)
	{
	case GlobalConstants::CE_TYPE_DOCUMENT:
		ui.cboRootType->setCurrentIndex(0);
		OnRootTypeComboChanged(0);
		break;
	case GlobalConstants::CE_TYPE_EMAIL:
		ui.cboRootType->setCurrentIndex(1);
		OnRootTypeComboChanged(1);
		break;
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		ui.cboRootType->setCurrentIndex(2);
		OnRootTypeComboChanged(2);
	    break;
	case GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART:
		ui.cboRootType->setCurrentIndex(3);
		OnRootTypeComboChanged(3);
		break;
	}
}


//get FUI name
QString FUI_CE_Types::GetFUIName()
{
	if(!ui.txtName->text().isEmpty())
		return tr("Communication Categories: ") + ui.txtName->text();
	else
		return tr("Communication Categories");
}


void FUI_CE_Types::DataPrepareForInsert(Status &err)
{
	DataClear();
	m_lstData.addRow();
	m_lstData.setData(0,"CET_TYPE_NAME","");
	m_lstData.setData(0,"CET_ICON","");	
}
void FUI_CE_Types::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	if(m_lstData.getDataRef(0,"CET_CODE").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Code is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"CET_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Name is mandatory field!"));	return;	}
}

void FUI_CE_Types::OnModifyIcon()
{
	QPixmap pix;
	QString strFormat;
	bool bOK=PictureHelper::LoadPicture(pix,strFormat);
	if (!bOK || m_lstData.getRowCount()!=1)return;

	QByteArray picture;
	PictureHelper::ConvertPixMapToByteArray(picture,pix,strFormat);
	m_lstData.setData(0,"CET_ICON_BINARY",picture);
	ui.labelIcon->setPixmap(pix);
}

void FUI_CE_Types::RefreshIcon()
{
	if (m_lstData.getRowCount()==1)
	{
		QByteArray bytePicture=m_lstData.getDataRef(0,"CET_ICON_BINARY").toByteArray();
		QPixmap pixMap;
		pixMap.loadFromData(bytePicture);
		ui.labelIcon->setPixmap(pixMap);
	}
	else
	{
		ui.labelIcon->clear();
	}


}