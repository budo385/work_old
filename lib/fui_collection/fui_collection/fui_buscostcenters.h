#ifndef FUI_BUSCOSTCENTERS_H
#define FUI_BUSCOSTCENTERS_H

#include <QWidget>
#include "generatedfiles/ui_fui_buscostcenters.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"

class FUI_BusCostCenters : public FuiBase
{
    Q_OBJECT

public:
    FUI_BusCostCenters(QWidget *parent = 0);
    ~FUI_BusCostCenters();

	virtual void on_cmdInsert();
	void SetFiltersForCurrentActualOrganization(int nActualOrganizationID);
	QString GetFUIName();

protected:
	void DataPrepareForInsert(Status &err);
	void DataCheckBeforeWrite(Status &err);

private:
	void RefreshDisplay();
	void SetMode(int nMode);
    Ui::FUI_BusCostCentersClass ui;
	GuiFieldManager *m_GuiFieldManagerMain;
};

#endif // FUI_BUSCOSTCENTERS_H
