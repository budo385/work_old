#ifndef CALENDARVERTICALHEADERITEM_H
#define CALENDARVERTICALHEADERITEM_H

#include <QGraphicsProxyWidget>
#include "label.h"
#include "calendargraphicsview.h"
#include "calendartableview.h"

class CalendarVerticalHeaderItem : public QGraphicsProxyWidget
{
	Q_OBJECT

public:
	CalendarVerticalHeaderItem(int nRow, int nHeaderHeight, int nWidth, int nHeight, int nTimeScale, CalendarGraphicsView *pCalendarGraphicsView, QGraphicsProxyWidget *pCalendarTableView, QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0);
	~CalendarVerticalHeaderItem();

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

public slots:
	void on_UpdateCalendar();

private:
	CalendarGraphicsView	*m_pCalendarGraphicsView;
	QGraphicsProxyWidget	*m_pCalendarTableView;
	Label					*m_pLabel;
	int						m_nRow;
	int						m_nWidth;
	int						m_nHeight;
	int						m_nTimeScale;
	int						m_nHeaderHeight;
};

#endif // CALENDARVERTICALHEADERITEM_H
