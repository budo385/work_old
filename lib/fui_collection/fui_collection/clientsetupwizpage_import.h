#ifndef CLIENTSETUPWIZPAGE_IMPORT_H
#define CLIENTSETUPWIZPAGE_IMPORT_H

#include "gui_core/gui_core/wizardpage.h"
#include "ui_clientsetupwizpage_import.h"

class ClientSetupWizPage_Import : public WizardPage
{
	Q_OBJECT

public:
	ClientSetupWizPage_Import(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
	~ClientSetupWizPage_Import();

	void Initialize();
	void resetPage(){};
	bool GetPageResult(DbRecordSet &RecordSet){return true;};

private:
	Ui::ClientSetupWizPage_ImportClass ui;

private slots:
	void on_btnImportSykpe_Contact_clicked();
	void on_btnImportOutlook_Contact_clicked();
	void on_btnImportOutlook_Email_clicked();
		
};

#endif // CLIENTSETUPWIZPAGE_IMPORT_H
