#include "calendartableview.h"

#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QTextEdit>
#include <QTextBrowser>
#include <QStyleOptionViewItem>
#include <QApplication>
#include <QScrollBar>
#include <QGraphicsSceneDragDropEvent>

#include "calendargraphicsview.h"

void ScrollBar::showEvent(QShowEvent *event)
{
	emit scrollBarVisible();
}

void ScrollBar::hideEvent(QHideEvent *event)
{
	emit scrollBarHidden();
}

ItemDelegate::ItemDelegate (QAbstractTableModel *pTableModel, int nLeftOffSet /*= 15*/, QObject * parent /*= 0*/) : QItemDelegate(parent)
{
	m_pTableModel	 = pTableModel;
	m_nLeftOffSet	 = nLeftOffSet;
	m_nWorkStartHour = 8;
	m_nWorkEndHour	 = 17;
}

void ItemDelegate::AddMultiDayIDs(int nCentID, int nBcepID, int nBcevID)
{
	int nCount = m_hshMultiDayCentIDs.count();

	m_hshMultiDayCentIDs.insert(nCount, nCentID);
	m_hshMultiDayBcepIDs.insert(nCount, nBcepID);
	m_hshMultiDayBcevIDs.insert(nCount, nBcevID);
}

void ItemDelegate::GetMultiDayIDs(QHash<int, int> &hshMultiDayCentIDs, QHash<int, int> &hshMultiDayBcepIDs, QHash<int, int> &hshMultiDayBcevIDs)
{
	hshMultiDayCentIDs = m_hshMultiDayCentIDs;
	hshMultiDayBcepIDs = m_hshMultiDayBcepIDs;
	hshMultiDayBcevIDs = m_hshMultiDayBcevIDs;
}

void ItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	int nTimeScale = dynamic_cast<CalendarTableModel*>(m_pTableModel)->getTimeScale();
	//Background.
	if (index.row()/nTimeScale < m_nWorkStartHour || index.row()/nTimeScale >= m_nWorkEndHour)
	{
		painter->fillRect(option.rect, QBrush(QColor(255, 245,190)));
	}
	else
	{
		painter->fillRect(option.rect, QBrush(QColor(255, 255,215)));
	}
	
	if (option.state & QStyle::State_Selected)
	{
		painter->fillRect(option.rect, option.palette.highlight());
	}
	
	//Draw other lines.
	QPen pen;
	pen.setStyle(Qt::SolidLine);
	pen.setColor(QColor(234, 207,152));
	pen.setWidth(0.2);
	painter->setPen(pen);
	painter->drawLine(option.rect.topRight(), option.rect.bottomRight());

	if (index.row()%nTimeScale)
		pen.setStyle(Qt::DotLine);

	painter->setPen(pen);
	painter->drawLine(option.rect.topLeft(), option.rect.topRight());

	//Draw left gray area.
	QLinearGradient linearGrad(option.rect.topLeft().x(), option.rect.center().y(), option.rect.topRight().x()+m_nLeftOffSet, option.rect.center().y());
	linearGrad.setColorAt(0, Qt::white);

	if (dynamic_cast<CalendarTableModel*>(m_pTableModel)->CheckMultiDayIndex(index))
		linearGrad.setColorAt(0.01, QColor(255,0,0));
	else
		linearGrad.setColorAt(0.01, QColor(128,128,128));

	QRect rect1;
	if (m_pTableModel->columnCount()>26)
		rect1 = QRect(option.rect.x(),option.rect.y(), 4, option.rect.height());
	else
		rect1 = QRect(option.rect.x(),option.rect.y(), m_nLeftOffSet, option.rect.height());
	painter->fillRect(rect1, linearGrad);
	pen.setStyle(Qt::SolidLine);
	pen.setColor(Qt::black);
	pen.setWidth(1);
	painter->setPen(pen);
	if (m_pTableModel->columnCount()>26)
		painter->drawLine(option.rect.topLeft().x()+4, option.rect.topLeft().y(), option.rect.bottomLeft().x()+4, option.rect.bottomLeft().y());
	else
		painter->drawLine(option.rect.topLeft().x()+m_nLeftOffSet, option.rect.topLeft().y(), option.rect.bottomLeft().x()+m_nLeftOffSet, option.rect.bottomLeft().y());
}

CalendarTableView::CalendarTableView(CalendarWidget* pCalendarWidget, QGraphicsView* pCalendarGraphicsView, QAbstractTableModel *pCalendarTableModel, QWidget *parent)
	: QGraphicsProxyWidget()
{
	m_nRowHeight = pCalendarWidget->getRowHeight();
	m_nItemLeftOffSet = pCalendarWidget->getRowLeftOffset();

	m_pCalendarWidget = pCalendarWidget;
	m_pCalendarGraphicsView = pCalendarGraphicsView;
	m_pTable = new TableView(m_nItemLeftOffSet);

	//Locals.
	m_pModel = pCalendarTableModel;
	m_pTable->verticalHeader()->setMinimumSectionSize(m_nRowHeight);
	m_pTable->verticalHeader()->setDefaultSectionSize(m_nRowHeight); 

	m_pTable->horizontalHeader()->hide();
	m_pTable->verticalHeader()->hide();

	setWidget(m_pTable);

	ScrollBar *m_pHorizontalScrollBar = new ScrollBar();
	ScrollBar *m_pVerticalScrollBar = new ScrollBar();
	m_pTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	m_pTable->setHorizontalScrollBar(m_pHorizontalScrollBar);
	m_pTable->setVerticalScrollBar(m_pVerticalScrollBar);
	m_pTable->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	m_pTable->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	connect(m_pTable, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(on_doubleClicked(const QModelIndex &)));
	connect(m_pTable, SIGNAL(openMultiDayEvent(QModelIndex)), this, SIGNAL(openMultiDayEvent(QModelIndex)));

	connect(m_pHorizontalScrollBar, SIGNAL(scrollBarVisible()), this, SLOT(on_HScrollShow()));
	connect(m_pHorizontalScrollBar, SIGNAL(scrollBarHidden()), this, SLOT(on_HScrollHide()));
	connect(m_pVerticalScrollBar, SIGNAL(scrollBarVisible()), this, SLOT(on_VScrollShow()));
	connect(m_pVerticalScrollBar, SIGNAL(scrollBarHidden()), this, SLOT(on_VScrollHide()));

	setAcceptDrops(true);
	//setFlag(QGraphicsItem::ItemIsMovable, true);
	//setFlag(QGraphicsItem::ItemIsSelectable, true);
	//setFlag(QGraphicsItem::ItemIsFocusable, true);
	//setAcceptHoverEvents(true);
}

CalendarTableView::~CalendarTableView()
{
	m_pCalendarWidget		= NULL;
	m_pCalendarGraphicsView = NULL;
	m_pTable				= NULL;
	m_pModel				= NULL;
	m_pHorizontalScrollBar	= NULL;
	m_pVerticalScrollBar	= NULL;
}

void CalendarTableView::Initialize(QDate startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites)
{
	m_pTable->setModel(0);
	m_datStartDate = startDate;
	m_nDayInterval = nDayInterval;
	m_timeScale = scale;
	m_recEntites = recEntites;
	dynamic_cast<CalendarTableModel*>(m_pModel)->Initialize(m_datStartDate, m_nDayInterval, m_timeScale, m_recEntites);
	m_pTable->setModel(m_pModel);
	ItemDelegate *itemDelegate = new ItemDelegate(m_pModel, m_nItemLeftOffSet);
	m_pTable->setItemDelegate(itemDelegate);
	setPos(0, dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTotalHeaderHeight());
	//QDateTime scrollDateTime(startDate, QTime(6,0));
	//m_pTable->scrollTo(m_pModel->getItem(scrollDateTime), QAbstractItemView::PositionAtTop);
}

QRect CalendarTableView::getItemRectangle(int nEntityID, int nEntityType, QDateTime &startDateTime, QDateTime &endDateTime, bool bFromTaskItem /*= false*/)
{
	qreal nStartRemain, nEndRemain;
	QModelIndex startIndex, endIndex;
	dynamic_cast<CalendarTableModel*>(m_pModel)->getItem(nEntityID, nEntityType, startDateTime, startIndex, nStartRemain);
	dynamic_cast<CalendarTableModel*>(m_pModel)->getItem(nEntityID, nEntityType, endDateTime, endIndex, nEndRemain);
	QRect startItemRect = getItemRectangle(startIndex);
	QPoint startPoint;
	if (bFromTaskItem)
		startPoint = QPoint(startItemRect.topLeft().x(), startItemRect.topLeft().y()+m_nRowHeight*nStartRemain);
	else
		startPoint = QPoint(startItemRect.topLeft().x()+m_nItemLeftOffSet, startItemRect.topLeft().y()+m_nRowHeight*nStartRemain);
	
	QRect endItemRect = getItemRectangle(endIndex);
	QPoint endPoint(endItemRect.topRight().x(), endItemRect.topRight().y()+m_nRowHeight*nEndRemain);
	return QRect(startPoint, endPoint);
}

QRect CalendarTableView::getItemRectangle(QModelIndex &Index)
{
	return QRect(m_pTable->visualRect(Index));
}

QAbstractTableModel* CalendarTableView::Model()
{
	return m_pModel;
}

TableView* CalendarTableView::Table()
{
	return m_pTable;
}

void CalendarTableView::on_UpdateCalendar()
{
	int nHorizHeaderHeight = dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTotalHeaderHeight();
	int nVertHeaderHeight = m_pCalendarWidget->getVertHeaderWidthForLowerCalendar();
	setPos(nVertHeaderHeight, nHorizHeaderHeight);
	QRect rectViewport = dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->viewport()->rect();
	resize(rectViewport.width()-nVertHeaderHeight, rectViewport.height()-nHorizHeaderHeight);

	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->PositionCornerScrollBarItems();
}

void CalendarTableView::on_headerScrollBar_valueChanged(int value)
{
	if (dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getEntityCount()>1)
		return;

	m_pTable->horizontalScrollBar()->setValue(value);
	on_UpdateCalendar();
}


void CalendarTableView::on_HScrollShow()
{
	emit horizontalScrollBarVisible(true);
}

void CalendarTableView::on_HScrollHide()
{
	emit horizontalScrollBarVisible(false);
}

void CalendarTableView::on_VScrollShow()
{
	emit verticalScrollBarVisible(true);
}

void CalendarTableView::on_VScrollHide()
{
	emit verticalScrollBarVisible(false);
}

QRect CalendarTableView::getItemRectangleForHeader(int nColumn)
{
	int nUserCount = m_recEntites->getRowCount();
	if (!nUserCount)
		nUserCount = 1;
	int nTableColumn = nColumn*nUserCount;

	QModelIndex index = m_pModel->index(0, nTableColumn);
	QRect itemRect = m_pTable->visualRect(index);
	QRect rectangle(itemRect.topLeft().x(), itemRect.topLeft().y(), itemRect.width()*nUserCount, itemRect.height());
	return rectangle;
}

QRect CalendarTableView::getItemRectangleForVertHeader(int nRow)
{
	QModelIndex index = m_pModel->index(nRow, 0);
	return m_pTable->visualRect(index);
}

void CalendarTableView::getSelectionStartEndDateTime(QDateTime &startDateTime, QDateTime &endDateTime)
{
	startDateTime = dynamic_cast<CalendarTableModel*>(m_pModel)->getItemStartDateTime(m_pTable->selectionModel()->selectedIndexes().first());
	endDateTime = dynamic_cast<CalendarTableModel*>(m_pModel)->getItemEndDateTime(m_pTable->selectionModel()->selectedIndexes().first());
}

void CalendarTableView::setColumnWidth(int nColumn, int nWidth)
{
	int nUserCount = m_recEntites->getRowCount();
	for (int i = 0; i < nUserCount; i++)
	{
		m_pTable->setColumnWidth(nColumn*nUserCount+i, nWidth/nUserCount);
		m_pTable->horizontalHeader()->setDefaultSectionSize(nWidth/nUserCount);
	}
}

int CalendarTableView::getColumnWidth()
{
	return m_pTable->horizontalHeader()->defaultSectionSize();
}

void CalendarTableView::ItemResizing(bool bIsResizing)
{
	m_pTable->ItemIsResizing(bIsResizing);
}

void CalendarTableView::setColumnsDefaultWidth(int nColumnWidth)
{
	m_pTable->horizontalHeader()->setDefaultSectionSize(nColumnWidth);
	m_pTable->horizontalHeader()->resizeSections(QHeaderView::Interactive);
	m_pTable->update();
}

QDateTime CalendarTableView::GetTopLeftVisibleItem()
{
	QModelIndex index = m_pTable->indexAt(QPoint(10,10));
	if (!index.isValid())
		return QDateTime();
	QDateTime s = dynamic_cast<CalendarTableModel*>(m_pModel)->getItemStartDateTime(index);
	return s;
}
void CalendarTableView::SetTopLeftVisibleItem(QDateTime dateTime)
{
	//Scroll to start date at 6:00.
	QModelIndex ind = dynamic_cast<CalendarTableModel*>(m_pModel)->getItem(dateTime);
	//QString s = dateTime.toString();
	//Select index.
//	QItemSelectionModel *selModel = m_pTable->selectionModel();
//	selModel->setCurrentIndex(ind, QItemSelectionModel::Toggle);
//	m_pTable->setSelectionModel(selModel);
	//m_pTable->scrollTo(ind, QAbstractItemView::PositionAtBottom);
	m_pTable->scrollTo(ind, QAbstractItemView::PositionAtTop);
	//update();
}

void CalendarTableView::on_doubleClicked(const QModelIndex &index)
{
	emit cellDoubleClicked(index.row(), index.column());
}
