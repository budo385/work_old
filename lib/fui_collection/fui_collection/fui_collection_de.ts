<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_CH">
<context>
    <name></name>
    <message>
        <source>Close</source>
        <translation type="obsolete">Schliessen</translation>
    </message>
    <message>
        <source>Open on Startup</source>
        <translation type="obsolete">Beim Starten öffnen</translation>
    </message>
    <message>
        <source>Project Card: </source>
        <translation type="obsolete">Projekt-Karte: </translation>
    </message>
    <message>
        <source>Contact Card: </source>
        <translation type="obsolete">Kontakt-Karte: </translation>
    </message>
    <message>
        <source>Task Card: </source>
        <translation type="obsolete">Aufgaben-Karte: </translation>
    </message>
    <message>
        <source>Overdue</source>
        <translation type="obsolete">Überfällig</translation>
    </message>
    <message>
        <source>Due</source>
        <translation type="obsolete">Fällig</translation>
    </message>
    <message>
        <source>Done/Not Scheduled</source>
        <translation type="obsolete">Erledigt/Nicht geplant</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation type="obsolete">Vorgesehen</translation>
    </message>
    <message>
        <source>Do you really want to mark task as done?</source>
        <translation type="obsolete">Wollen Sie diese Aufgabe als erledigt markieren?</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
</context>
<context>
    <name>ARSToRoleAssignDialog</name>
    <message>
        <location filename="arstoroleassigndialog.cpp" line="+8"/>
        <source>Assign Selected Access Rights Sets To Multiple Roles</source>
        <translation>Ausgewählte Sets von Zugriffsrechten mehreren Rollen zuweisen</translation>
    </message>
</context>
<context>
    <name>ARSToRoleAssignDialogClass</name>
    <message>
        <location filename="arstoroleassigndialog.ui" line="+13"/>
        <source>Access Rights Sets To Role Assign Dialog</source>
        <translation>ARSToRoleAssignDialog</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>AddPhoneNumberDlg</name>
    <message>
        <location filename="addphonenumberdlg.ui" line="+14"/>
        <location line="+30"/>
        <source>Add Phone Number to Contact</source>
        <translation>Telephonnummer dem aktuellen Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="-22"/>
        <source>Phone Number</source>
        <translation>Telefonnummer</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>OK</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AdminTool</name>
    <message>
        <location filename="admintool.cpp" line="+94"/>
        <location line="+9"/>
        <location line="+512"/>
        <source>Monday</source>
        <translation>Montag</translation>
    </message>
    <message>
        <location line="-520"/>
        <location line="+9"/>
        <location line="+491"/>
        <location line="+22"/>
        <source>Tuesday</source>
        <translation>Dienstag</translation>
    </message>
    <message>
        <location line="-521"/>
        <location line="+9"/>
        <location line="+492"/>
        <location line="+22"/>
        <source>Wednesday</source>
        <translation>Mittwoch</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+9"/>
        <location line="+493"/>
        <location line="+22"/>
        <source>Thursday</source>
        <translation>Donnerstag</translation>
    </message>
    <message>
        <location line="-523"/>
        <location line="+9"/>
        <location line="+494"/>
        <location line="+22"/>
        <source>Friday</source>
        <translation>Freitag</translation>
    </message>
    <message>
        <location line="-524"/>
        <location line="+9"/>
        <location line="+495"/>
        <location line="+22"/>
        <source>Saturday</source>
        <translation>Samstag</translation>
    </message>
    <message>
        <location line="-525"/>
        <location line="+9"/>
        <location line="+13"/>
        <location line="+483"/>
        <location line="+22"/>
        <source>Sunday</source>
        <translation>Sonntag</translation>
    </message>
    <message>
        <location line="-74"/>
        <source>Download to my Computer</source>
        <translation>Auf lokalen Rechner herunterladen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Upload From Local Copy</source>
        <translation>Kopie vom lokalen Rechner hochladen</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>Select backup directory</source>
        <translation>Backup-Verzeichnis wählen</translation>
    </message>
    <message>
        <location line="+123"/>
        <location line="+30"/>
        <location line="+94"/>
        <location line="+19"/>
        <location line="+50"/>
        <location line="+8"/>
        <location line="+7"/>
        <location line="+16"/>
        <location line="+8"/>
        <location line="+7"/>
        <location line="+16"/>
        <location line="+8"/>
        <location line="+7"/>
        <location line="+17"/>
        <location line="+8"/>
        <location line="+7"/>
        <location line="+17"/>
        <location line="+8"/>
        <location line="+7"/>
        <location line="+38"/>
        <location line="+6"/>
        <location line="+86"/>
        <location line="+19"/>
        <location line="+208"/>
        <location line="+23"/>
        <location line="+78"/>
        <location line="+15"/>
        <location line="+235"/>
        <location line="+11"/>
        <location line="+11"/>
        <location line="+11"/>
        <location line="+12"/>
        <location line="+16"/>
        <location line="+74"/>
        <location line="+7"/>
        <location line="+54"/>
        <location line="+13"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-1251"/>
        <source>Database info could not be loaded</source>
        <translation>Datenbankinformationen können nicht geladen werden</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Failed to reset demo key</source>
        <translation>Demo-Schlüssel konnte nicht zurückgestellt werden</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location line="+81"/>
        <location line="+448"/>
        <location line="+46"/>
        <location line="+43"/>
        <location line="+682"/>
        <location line="+55"/>
        <source>Successfully processed: </source>
        <translation>Erfolgreich verarbeitet: </translation>
    </message>
    <message>
        <location line="-1274"/>
        <location line="+448"/>
        <location line="+46"/>
        <location line="+43"/>
        <location line="+682"/>
        <location line="+55"/>
        <source> records, failed to process: </source>
        <translation> Datensätze; Verarbeitung misslungen: </translation>
    </message>
    <message>
        <location line="-1242"/>
        <location line="+340"/>
        <location line="+231"/>
        <location line="+93"/>
        <location line="+444"/>
        <source>Directory can only be selected on server</source>
        <translation>Das richtige Verzeichnis kann nur auf dem Server selber ausgewählt werden</translation>
    </message>
    <message>
        <location line="-1089"/>
        <location line="+340"/>
        <location line="+231"/>
        <location line="+93"/>
        <location line="+444"/>
        <source>Select export/import directory</source>
        <translation>Import/Export-Verzeichnis wählen</translation>
    </message>
    <message>
        <location line="-898"/>
        <location line="+6"/>
        <source>You do not have sufficient access rights to perform this operation!</source>
        <translation>Kein Zugriffsrecht!</translation>
    </message>
    <message>
        <location line="-555"/>
        <location line="+230"/>
        <location line="+340"/>
        <location line="+38"/>
        <location line="+234"/>
        <location line="+94"/>
        <location line="+551"/>
        <location line="+21"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Backup process started on server, check out result in few minutes!</source>
        <translation type="obsolete">Auf dem Server wurde eine Sicherung gestartet, Sie haben in wenigen Minuten Zugriff auf die Sicherungsdatei!</translation>
    </message>
    <message>
        <location line="-1207"/>
        <location line="+84"/>
        <location line="+31"/>
        <location line="+31"/>
        <location line="+32"/>
        <location line="+32"/>
        <location line="+130"/>
        <location line="+227"/>
        <location line="+101"/>
        <location line="+250"/>
        <location line="+11"/>
        <location line="+11"/>
        <location line="+11"/>
        <location line="+12"/>
        <location line="+151"/>
        <source>Only scan period option is allowed when using FTP. Please change period.</source>
        <translation>Für FTP muss eine Prüfperiode definiert sein, &quot;automatisch&quot; ist nicht verfügbar. Bitte anpassen.</translation>
    </message>
    <message>
        <location line="-1045"/>
        <source>Project synchronization is enabled, but source directory is not specified!</source>
        <translation>Die Projektsynchronisation ist aktiviert, aber kein Verzeichnis definiert!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Project synchronization is enabled, but source ftp server/directory is not specified!</source>
        <translation>Die Projektsynchronisation ist aktiviert, aber kein ftp-Server/Verzeichnis definiert!</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>User synchronization is enabled, but source directory is not specified!</source>
        <translation>Die Benutzersynchronisation ist aktiviert, aber kein Verzeichnis definiert!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User synchronization is enabled, but source ftp server/directory is not specified!</source>
        <translation>Die Benutzersynchronisation ist aktiviert, aber kein ftp-Server/Verzeichnis definiert!</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Debtor synchronization is enabled, but source directory is not specified!</source>
        <translation>Die Debitorensynchronisation ist aktiviert, aber kein Verzeichnis definiert!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Debtor synchronization is enabled, but source ftp server/directory is not specified!</source>
        <translation>Die Debitorensynchronisation ist aktiviert, aber kein ftp-Server/Verzeichnis definiert!</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Contact synchronization is enabled, but source directory is not specified!</source>
        <translation>Die Kontaktsynchronisation ist aktiviert, aber kein Verzeichnis definiert!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Contact synchronization is enabled, but source ftp server/directory is not specified!</source>
        <translation>Die Kontaktsynchronisation ist aktiviert, aber kein ftp-Server/Verzeichnis definiert!</translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="obsolete">Download</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="obsolete">Upload</translation>
    </message>
    <message>
        <location line="-564"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+1372"/>
        <location line="+74"/>
        <source>Please select a backup file!</source>
        <translation>Bitte wählen Sie eine Backup-Datei!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Do you really want to delete the following backup file: </source>
        <translation>Wollen Sie die folgende Backup-Datei löschen: </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-1772"/>
        <source>Backup File</source>
        <translation>Backup-Datei</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date Created</source>
        <translation>Erstellungsdatum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>Grösse</translation>
    </message>
    <message>
        <source>File: </source>
        <translation type="obsolete">Datei: </translation>
    </message>
    <message>
        <source> can not be downloaded from server!</source>
        <translation type="obsolete"> kann nicht vom Server heruntergeladen werden!</translation>
    </message>
    <message>
        <source>File size can not be larger then 160Mb!</source>
        <translation type="obsolete">Datei darf 160MB Grösse nicht überschreiten!</translation>
    </message>
    <message>
        <source> can not be uploaded on server!</source>
        <translation type="obsolete"> kann nicht auf Datenbankserver hochgeladen werden!</translation>
    </message>
    <message>
        <location line="+409"/>
        <source>Backup process started on server. Server will notify when operation is completed!</source>
        <translation>Die Datensicherung wurde auf dem Server gestartet. Sie werden nach Abschluss benachrichtigt!</translation>
    </message>
    <message>
        <location line="+230"/>
        <source>Last created Backup is template!</source>
        <translation>Das letzte Backup ist die Datenbankvorlage!</translation>
    </message>
    <message>
        <location line="+340"/>
        <location line="+917"/>
        <source>Process started on server. Server will notify when operation is completed!</source>
        <translation>Vorgang auf dem Server gestartet. Sie werden nach Abschluss benachrichtigt!</translation>
    </message>
    <message>
        <location line="-879"/>
        <location line="+234"/>
        <location line="+94"/>
        <source>Process started on server. You will be notified when operation is completed!</source>
        <translation>Vorgang auf dem Server gestartet. Sie werden nach Abschluss benachrichtigt!</translation>
    </message>
    <message>
        <location line="-440"/>
        <source>Stored Project List synchronization is enabled, but source directory is not specified!</source>
        <translation>Die Synchronisation von gespeicherten Projektlisten ist aktiviert, aber kein Verzeichnis definiert!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Stored Project List synchronization is enabled, but source ftp server/directory is not specified!</source>
        <translation>Die Synchronisation von gespeicherten Projektlisten ist aktiviert, aber kein ftp-Server/Verzeichnis definiert!</translation>
    </message>
</context>
<context>
    <name>AdminToolClass</name>
    <message>
        <location filename="admintool.ui" line="+14"/>
        <source>Administrator Tools</source>
        <translation>Administrationswerkzeuge</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Database Managment</source>
        <translation>Datenbankverwaltung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Backup  location:</source>
        <translation>Backupverzeichnis:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Schedule Backup</source>
        <translation>Backup planen</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Hourly</source>
        <translation>Stündlich</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Weekly</source>
        <translation>Wöchentlich</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Every</source>
        <translation>Jeden</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>btnDay</source>
        <translation>btnDay</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Backup Now</source>
        <translation>Backup starten</translation>
    </message>
    <message>
        <location line="+321"/>
        <source>Restore</source>
        <translation>Restore</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Refresh Backup List</source>
        <translation>Backup-Liste aktualisieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore From Backup</source>
        <translation>Backup zurücklesen</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Template Database</source>
        <translation>Datenbank-Vorlage</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Ask For Template Database at Startup</source>
        <translation>Beim Starten nach Datenbankvorlage fragen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Start Setup Wizard On Startup</source>
        <translation>Konfiguration beim Starten ausführen</translation>
    </message>
    <message>
        <location line="-47"/>
        <source>Current Template:</source>
        <translation>Aktuelle Vorlage:</translation>
    </message>
    <message>
        <location line="+2759"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-2726"/>
        <source>Prepare Database As Template</source>
        <translation>Datenbank als Vorlage vorbereiten</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Automated Data Synchronization</source>
        <translation>Automatische Datensynchronisation</translation>
    </message>
    <message>
        <source>Project Import</source>
        <translation type="obsolete">Import Projekte</translation>
    </message>
    <message>
        <location line="+142"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>Period</source>
        <translation>Synchronisationsintervall</translation>
    </message>
    <message>
        <location line="-2223"/>
        <source>(min)</source>
        <translation>(min)</translation>
    </message>
    <message>
        <location line="+168"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>Automatic (on file change)</source>
        <translation>Automatisch (bei Änderungen)</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="obsolete">Quelle</translation>
    </message>
    <message>
        <source>FTP</source>
        <translation type="obsolete">FTP</translation>
    </message>
    <message>
        <location line="-1582"/>
        <location line="+569"/>
        <location line="+525"/>
        <source>Directory</source>
        <translation>Verzeichnis</translation>
    </message>
    <message>
        <location line="-1503"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <source>File Filter</source>
        <translation>Dateifilter</translation>
    </message>
    <message>
        <location line="-1606"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>File extension:</source>
        <translation>Dateierweiterung:</translation>
    </message>
    <message>
        <location line="-2118"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>File prefix:</source>
        <translation>Dateinamen-Präfix:</translation>
    </message>
    <message>
        <location line="-2118"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>File extension of processed files:</source>
        <translation>Dateierweiterung für verarbeitete Dateien:</translation>
    </message>
    <message>
        <source>User Import</source>
        <translation type="obsolete">Import Benutzer</translation>
    </message>
    <message>
        <source>Debtor Export</source>
        <translation type="obsolete">Export Debitoren</translation>
    </message>
    <message>
        <location line="-2424"/>
        <source>Export/Import Projects</source>
        <translation>Import/Export Projekte</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+524"/>
        <source>Enable Automatical Synchronization</source>
        <translation>Automatische Synchronisation aktivieren</translation>
    </message>
    <message>
        <location line="-2117"/>
        <location line="+524"/>
        <location line="+350"/>
        <location line="+219"/>
        <location line="+525"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location line="-1611"/>
        <location line="+266"/>
        <location line="+258"/>
        <location line="+266"/>
        <location line="+303"/>
        <location line="+266"/>
        <location line="+259"/>
        <location line="+266"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location line="-1603"/>
        <location line="+524"/>
        <location line="+566"/>
        <location line="+528"/>
        <source>All Records</source>
        <translation>Alle Datensätze</translation>
    </message>
    <message>
        <location line="-1605"/>
        <location line="+524"/>
        <location line="+566"/>
        <location line="+528"/>
        <source>Only Records Modified in The Last</source>
        <translation>Nur Datensätze, die in den letzten</translation>
    </message>
    <message>
        <location line="-2376"/>
        <location line="+784"/>
        <location line="+524"/>
        <location line="+566"/>
        <location line="+528"/>
        <source>days</source>
        <translation>Tagen geändert wurden</translation>
    </message>
    <message>
        <location line="-1595"/>
        <location line="+562"/>
        <location line="+532"/>
        <location line="+524"/>
        <location line="+433"/>
        <source>Last Status</source>
        <translation>Letzter Status</translation>
    </message>
    <message>
        <location line="-2010"/>
        <location line="+562"/>
        <location line="+532"/>
        <location line="+524"/>
        <location line="+433"/>
        <source>Last File Processed:</source>
        <translation>Letzte verarbeitete Datei:</translation>
    </message>
    <message>
        <location line="-2041"/>
        <location line="+562"/>
        <location line="+532"/>
        <location line="+524"/>
        <location line="+433"/>
        <source>Last Error:</source>
        <translation>Letzter Fehler:</translation>
    </message>
    <message>
        <location line="-2041"/>
        <location line="+562"/>
        <location line="+532"/>
        <location line="+524"/>
        <location line="+433"/>
        <source>Processed Records:</source>
        <translation>Verarbeitete Datensätze:</translation>
    </message>
    <message>
        <location line="-1997"/>
        <location line="+562"/>
        <location line="+532"/>
        <location line="+524"/>
        <location line="+433"/>
        <source>Process Now</source>
        <translation>Jetzt verarbeiten</translation>
    </message>
    <message>
        <location line="-2028"/>
        <source>Import/Export Users</source>
        <translation>Import/Export Benutzer</translation>
    </message>
    <message>
        <location line="+936"/>
        <source>Only Export Addresses of Type:</source>
        <translation>Nur Adressen eines Typs exportieren:</translation>
    </message>
    <message>
        <location line="-1530"/>
        <source>Scan Period:</source>
        <translation>Prüfperiode:</translation>
    </message>
    <message>
        <location line="+142"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>Start Every</source>
        <translation>Start alle</translation>
    </message>
    <message>
        <location line="-2102"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>Scan Periods</source>
        <translation>Prüfperioden</translation>
    </message>
    <message>
        <location line="-2089"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>Source/Target</source>
        <translation>Quelle/Ziel</translation>
    </message>
    <message>
        <location line="-2116"/>
        <location line="+2128"/>
        <source>Directory (server)</source>
        <translation>Verzeichnis (auf Server)</translation>
    </message>
    <message>
        <location line="-1901"/>
        <location line="+524"/>
        <location line="+570"/>
        <location line="+524"/>
        <source>Add Date/Time to Filename</source>
        <translation>Datum/Uhrzeit an Dateinamen hängen </translation>
    </message>
    <message>
        <location line="-1596"/>
        <location line="+562"/>
        <location line="+532"/>
        <location line="+524"/>
        <location line="+433"/>
        <source>Last Synchronization:</source>
        <translation>Letzte Synchronisation:</translation>
    </message>
    <message>
        <location line="-1989"/>
        <location line="+1094"/>
        <location line="+524"/>
        <location line="+433"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="-1418"/>
        <source>Export Debtors</source>
        <translation>Export Debitoren</translation>
    </message>
    <message>
        <location line="+1486"/>
        <source>Log</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Aktualisieren</translation>
    </message>
    <message>
        <location line="-2655"/>
        <source>Global Scan Period</source>
        <translation>Basis-Prüfperiode</translation>
    </message>
    <message>
        <location line="+285"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>FTP directory &amp;&amp; FTP conenction</source>
        <translation>FTP-Verzeichnis &amp;&amp; FTP-Verbindung</translation>
    </message>
    <message>
        <location line="-2121"/>
        <location line="+524"/>
        <location line="+569"/>
        <location line="+525"/>
        <location line="+510"/>
        <source>FTP target directory, leave empty if root, or absoluthe path, e.g.: /web/mydir</source>
        <translation>FTP Zielverzeichnis (leer für Rootverzeichnis, oder absoluter Pfad, z.B. /web/mydir)</translation>
    </message>
    <message>
        <location line="-2365"/>
        <source>Use Unicode</source>
        <translation>Unicode verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show Unicode Option</source>
        <translation>Unicode-Auswahloption zeigen</translation>
    </message>
    <message>
        <location line="+1632"/>
        <source>Export/Import Contacts</source>
        <translation>Kontakte Importieren/Exportieren</translation>
    </message>
    <message>
        <location line="-2065"/>
        <source>Delete old Backup Files Older Than</source>
        <translation>Backup-Dateien älter als</translation>
    </message>
    <message>
        <location line="+2589"/>
        <source>Import Stored Project Lists</source>
        <translation>Import gespeicherter Projektlisten</translation>
    </message>
    <message>
        <location line="-1220"/>
        <source>Default Role:</source>
        <translation>Default-Rolle:</translation>
    </message>
    <message>
        <location line="+1470"/>
        <source>File Filter (one file must have _Headers and another _Details in name after prefix)</source>
        <translation>Dateifilter (eine Datei muss _Headers und die andere _Details nach dem gleichen Präfix im Namen enthalten)</translation>
    </message>
</context>
<context>
    <name>App_MenuWidget</name>
    <message>
        <location filename="app_menuwidget.cpp" line="+58"/>
        <source>New Document For Selected Application</source>
        <translation>Neues Dokument für gewählte Anwendung</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Modify/Create</source>
        <translation>Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>My Application Paths</source>
        <translation>Meine Anwendungspfade</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>New Document Using Application</source>
        <translation>Neues Dokument für Anwendung</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Add Application</source>
        <translation>Anwendung hinzufügen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Modify Application</source>
        <translation>Anwendung bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete Application</source>
        <translation>Anwendung löschen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>View my application paths</source>
        <translation>Meine Anwendungspfade zeigen</translation>
    </message>
    <message>
        <location line="+222"/>
        <location line="+71"/>
        <location line="+75"/>
        <location line="+22"/>
        <location line="+9"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-177"/>
        <location line="+146"/>
        <location line="+22"/>
        <source>Please first select an application!</source>
        <translation>Zuerst Anwendung auswählen!</translation>
    </message>
    <message>
        <location line="-97"/>
        <source>Could not load template from application record!</source>
        <translation>Default-Vorlage für Anwendung kann nicht geladen werden!</translation>
    </message>
    <message>
        <location line="+106"/>
        <source>Do you really want to delete this record permanently from the database?</source>
        <translation>Wollen Sie diesen Datensatz endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+208"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-110"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Application Manager</source>
        <translation>Anwendungs-Manager</translation>
    </message>
</context>
<context>
    <name>App_MenuWidgetClass</name>
    <message>
        <location filename="app_menuwidget.ui" line="+14"/>
        <source>App_MenuWidget</source>
        <translation>App_MenuWidget</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>AppearanceSettings</name>
    <message>
        <location filename="appearancesettings.cpp" line="+36"/>
        <source>Default (no skin)</source>
        <translation>Default (kein Thema)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Windows</source>
        <translation>Windows</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Windows XP</source>
        <translation>Windows XP</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clean Look</source>
        <translation>Norwegian Wood</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plastique</source>
        <translation>Plastique</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Motif</source>
        <translation>Motif</translation>
    </message>
    <message>
        <source>Arthur</source>
        <translation type="obsolete">Arthur</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Windows Vista</source>
        <translation>Windows Vista</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Blue Temple</source>
        <translation>Blue Temple</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Grayhound</source>
        <translation>Grayhound</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Sidebar Mode</source>
        <translation>Sidebar-Modus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Professional Mode</source>
        <translation>Pro-Modus</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Cabernet</source>
        <translation>Cabernet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forest</source>
        <translation>Forest</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Smaragd</source>
        <translation>Smaragd</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Please log out and log in again to activate the new theme!</source>
        <translation>Bitte melden Sie sich ab und wieder neu an, um das neue Thema zu aktivieren!</translation>
    </message>
</context>
<context>
    <name>AppearanceSettingsClass</name>
    <message>
        <location filename="appearancesettings.ui" line="+14"/>
        <source>Appearance Settings</source>
        <translation>Einstellungen für Darstellung</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Select Skin:</source>
        <translation>Darstellung auswählen:</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Starting Module</source>
        <translation>Modul wird gestartet</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Contacts/Details</source>
        <translation>Kontakte/Details</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Contacts/Communication</source>
        <translation>Kontakte/Kommunikation</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Show Header</source>
        <translation>Kopfteil anzeigen</translation>
    </message>
    <message>
        <location line="-33"/>
        <source>View Options</source>
        <translation>Optionen sichtbar</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Show Status Bar</source>
        <translation>Statusleiste zeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Full Screen</source>
        <translation>Vollbild</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Select Theme:</source>
        <translation>Thema auswählen:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Two-Screen Configuration With No. 1 on the right</source>
        <translation>Konfiguration mit zwei Monitoren: Nr. 1 rechts</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Select View Mode:</source>
        <translation>Modus auswählen:</translation>
    </message>
</context>
<context>
    <name>AssignedProjectsDlg</name>
    <message>
        <location filename="assignedprojectsdlg.ui" line="+14"/>
        <source>Assigned Projects</source>
        <translation>Zugewiesene Projekte</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>CalViewSelectDialog</name>
    <message>
        <location filename="calviewselectdialog.cpp" line="+14"/>
        <source>Save View</source>
        <translation>Ansicht speichern</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Please fill the View Name field first!</source>
        <translation>Bitte geben Sie zuerst einen Namen für die Ansicht ein!</translation>
    </message>
</context>
<context>
    <name>CalViewSelectDialogClass</name>
    <message>
        <location filename="calviewselectdialog.ui" line="+32"/>
        <source>CalViewSelectDialog</source>
        <translation>CalViewSelectDialog</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>View Name</source>
        <translation>Name der Ansicht</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Public</source>
        <translation>Öffentlich</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>CalendarEntityItem</name>
    <message>
        <location filename="calendarentityitem.cpp" line="+409"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Modify</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>CalendarEvent_BreakTable</name>
    <message>
        <location filename="calendarevent_breaktable.cpp" line="+22"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>From</source>
        <translation>Von</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>To</source>
        <translation>Bis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Duration (min)</source>
        <translation>Dauer (Min.)</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Add New Break</source>
        <translation>Pause hinzufügen</translation>
    </message>
</context>
<context>
    <name>CalendarEvent_DetailPane</name>
    <message>
        <location filename="calendarevent_detailpane.cpp" line="+72"/>
        <source>Fixed</source>
        <translation>gültig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preliminary</source>
        <translation>vorläufig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Over</source>
        <translation>hat stattgefunden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancelled</source>
        <translation>abgebrochen</translation>
    </message>
    <message>
        <source>Assigned Users And Contacts</source>
        <translation type="obsolete">Zugewiesene Benutzer und Kontakte</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Assigned Projects</source>
        <translation>Zugewiesene Projekte</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Reserved Resources</source>
        <translation>Reservierte Ressourcen</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Calendar Event</source>
        <translation>Kalendereintrag</translation>
    </message>
    <message>
        <location line="+152"/>
        <source>Do you wish to accept this proposal and to discard all others (they will be deleted)?</source>
        <translation>Wollen Sie diesen Terminvorschlag annehmen und alle anderen verwerfen (bzw. löschen)?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Contacts (</source>
        <translation>Kontakte (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Contacts</source>
        <translation>Kontakte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source> And </source>
        <translation> und </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Users (</source>
        <translation>Benutzer (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Users</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Projects (</source>
        <translation>Projekte (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Resources (</source>
        <translation>Ressourcen (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Resources</source>
        <translation>Ressourcen</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+4"/>
        <location line="+4"/>
        <location line="+4"/>
        <source>assigned</source>
        <translation>zugewiesen</translation>
    </message>
    <message>
        <location line="-370"/>
        <source>Assigned Contacts</source>
        <translation>Zugewiesene Kontakte</translation>
    </message>
    <message>
        <location line="+517"/>
        <source>Breaks (</source>
        <translation>Pausen (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Breaks</source>
        <translation>Pausen</translation>
    </message>
</context>
<context>
    <name>CalendarEvent_DetailPaneClass</name>
    <message>
        <location filename="calendarevent_detailpane.ui" line="+14"/>
        <source>CalendarEvent_DetailPane</source>
        <translation>CalendarEvent_DetailPane</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Proposal</source>
        <translation>Vorschlag</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="obsolete">Aktiv</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Subject:</source>
        <translation>Betreff:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Location:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Contacts And Users</source>
        <translation>Kontakte und Benutzer</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Resources</source>
        <translation>Ressourcen</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Classification</source>
        <translation>Einteilung</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Template:</source>
        <translation>Vorlage:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Category:</source>
        <translation>Kategorie:</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>State:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Breaks</source>
        <translation>Pausen</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Reminder</source>
        <translation>Erinnerung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Remind Me</source>
        <translation>Erinnere mich</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Owner:</source>
        <translation>Besitzer:</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Show Time As:</source>
        <translation>Zeit anzeigen als:</translation>
    </message>
    <message>
        <location line="-541"/>
        <source>Use Actual Contacts/Projects/Resources</source>
        <translation>Aktuelle Kontakte/Projekte/Ressourcen verwenden</translation>
    </message>
</context>
<context>
    <name>CalendarEvent_Wizard</name>
    <message>
        <source>Assigned Users And Contacts</source>
        <translation type="obsolete">Zugewiesene Benutzer und Kontakte</translation>
    </message>
    <message>
        <location filename="calendarevent_wizard.cpp" line="+87"/>
        <source>Assigned Projects</source>
        <translation>Zugewiesene Projekte</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Reserved Resources</source>
        <translation>Reservierte Ressourcen</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Calendar Event</source>
        <translation>Kalendereintrag</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Assigned Contacts</source>
        <translation>Zugewiesene Kontakte</translation>
    </message>
</context>
<context>
    <name>CalendarEvent_WizardClass</name>
    <message>
        <location filename="calendarevent_wizard.ui" line="+14"/>
        <source>CalendarEvent_Wizard</source>
        <translation>CalendarEvent_Wizard</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Select a template using</source>
        <translation>Wählen Sie eine Vorlage mit</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>if you want to create a calendar event from a predefined example event:</source>
        <translation>wenn Sie einen Kalendereintrag aus einem vordefinierten Vorlagen-Eintrag erstellen möchten:</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Template:</source>
        <translation>Vorlage:</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Enter basic details for this calendar event in the header of this window and below.</source>
        <translation>Erfassen Sie die wichtigsten Einträge für den Kalendereintrag im oberen und unteren Teil dieses Fensters. </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Press &quot;Store &amp; Close&quot; to store it as simple event, or &quot;Next Step&quot; to continue defining the event.</source>
        <translation>Drücken Sie &quot;Speichern &amp; Schliessen&quot; um einen einfachen Kalendereintrag zu speichern, oder &quot;Weiter&quot; um den Eintrag zu präzisieren.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Location:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <source>Category:</source>
        <translation type="obsolete">Kategorie:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Show Time As:</source>
        <translation>Zeit anzeigen als:</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Do you want to be reminded before the event takes place? Activate &quot;Remind me&quot; and fill the options.</source>
        <translation>Wollen Sie an den Termin erinnert werden? Aktivieren Sie &quot;Erinnern&quot; und wählen Sie die passenden Optionen. </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remind Me</source>
        <translation>Erinnern</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Assign contacts and users to the event and optionally add a role and description.</source>
        <translation>Kontakte und Benutzer dem Kalendereintrag zuweisen und optional eine Rolle und Beschreibung zuweisen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Mark those assignees who have to be invited by email. The confirmation status can be set later or manually or automatically. Use the calendar icon to examine the calendar for the selected assignees.</source>
        <translation>Markieren Sie die Teilnehmer, die per E-Mail eingeladen werden. Der Bestätigungsstatus kann später manuell oder automatisch gesetzt werden. Der Kalender-Druckknopf öffnet den Kalender des gewählten Teilnehmers.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Assign projects to the event and optionally add a role and a description.</source>
        <translation>Projekte dem Eintrag zuweisen und optional eine Rolle und Beschreibung eintragen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+37"/>
        <source>Use the calendar icon to examine the calendar for the selected projects.</source>
        <translation>Der Kalender-Druckknopf öffnet den Kalender des gewählten Projekts.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Reserve resources for the event and optionally add a role and description.</source>
        <translation>Ressourcen für den Eintrag reservieren und optional eine Rolle und Beschreibung eintragen.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Define the breaks of the event.</source>
        <translation>Pausen für den Kalendereintrag definieren.</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Recurrence</source>
        <translation>Terminserie</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Schedule calendar event.</source>
        <translation>Kalendereintrag planen.</translation>
    </message>
</context>
<context>
    <name>CalendarMultiDayEntityItem</name>
    <message>
        <location filename="calendarmultidayentityitem.cpp" line="+377"/>
        <source>Modify</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>CalendarTaskWidget</name>
    <message>
        <location filename="calendartaskwidget.cpp" line="+22"/>
        <source>Tasks</source>
        <translation>Aufgaben</translation>
    </message>
</context>
<context>
    <name>CalendarTaskWidgetClass</name>
    <message>
        <location filename="calendartaskwidget.ui" line="+14"/>
        <source>CalendarTaskWidget</source>
        <translation>CalendarTaskWidget</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tasks</source>
        <translation>Aufgaben</translation>
    </message>
</context>
<context>
    <name>CalendarViewWidgetClass</name>
    <message>
        <location filename="calendarviewwidget.ui" line="+20"/>
        <source>CalendarViewWidget</source>
        <translation>CalendarViewWidget</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>1h</source>
        <translation>1 Std</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>30min</source>
        <translation>30 Min</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>15min</source>
        <translation>15 Min</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>10min</source>
        <translation>10 Min</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>6min</source>
        <translation>6 Min</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>5min</source>
        <translation>5 Min</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Filter Calendar Events with no Type</source>
        <translation>Kalendereinträge ohne Art ausblenden</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Filter Calendar Events By Type</source>
        <translation>Kalendereinträge nach Art filtern</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Show Preliminary Events</source>
        <translation>Vorläufige Einträge anzeigen</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Show Information Only Events</source>
        <translation>Informations-Einträge anzeigen</translation>
    </message>
</context>
<context>
    <name>Calendar_MenuWidget</name>
    <message>
        <location filename="calendar_menuwidget.cpp" line="+58"/>
        <location line="+9"/>
        <source>Calendar </source>
        <translation>Kalender</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Modify/Create</source>
        <translation>Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Create Calendar Event From Template</source>
        <translation>Kalendereintrag aus Vorlage erzeugen</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+32"/>
        <source>New Task From Template</source>
        <translation>Neue Aufgabe aus Vorlage</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Create Calendar Event Using Template</source>
        <translation>Kalendereintrag aus Vorlage erzeugen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Add Template</source>
        <translation>Vorlage hinzufügen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Modify Template</source>
        <translation>Vorlage bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete Template</source>
        <translation>Vorlage löschen</translation>
    </message>
    <message>
        <location line="+175"/>
        <location line="+61"/>
        <location line="+22"/>
        <location line="+9"/>
        <location line="+211"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-303"/>
        <location line="+61"/>
        <location line="+22"/>
        <location line="+220"/>
        <source>Please first select an template!</source>
        <translation>Vorlage zuerst auswählen!</translation>
    </message>
    <message>
        <location line="-211"/>
        <source>Do you really want to delete this record permanently from the database?</source>
        <translation>Wollen Sie diesen Datensatz endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+278"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Calendar Manager</source>
        <translation>Kalender-Manager</translation>
    </message>
</context>
<context>
    <name>Calendar_MenuWidgetClass</name>
    <message>
        <location filename="calendar_menuwidget.ui" line="+14"/>
        <source>Calendar_MenuWidget</source>
        <translation>Calendar_MenuWidget</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>CheckBoxWizPageClass</name>
    <message>
        <source>Selection</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>CheckBox</source>
        <translation type="obsolete">CheckBox</translation>
    </message>
</context>
<context>
    <name>ClientContactManager</name>
    <message>
        <source>Phones</source>
        <translation type="obsolete">Telefonnummern</translation>
    </message>
    <message>
        <source>Emails</source>
        <translation type="obsolete">E-Mails</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Adresse</translation>
    </message>
    <message>
        <source>Internet</source>
        <translation type="obsolete">Internet</translation>
    </message>
    <message>
        <source>Invalid import file</source>
        <translation type="obsolete">Ungültige Import-Datei</translation>
    </message>
    <message>
        <source>Invalid import file. Please check values of first field. Import aborted!</source>
        <translation type="obsolete">Ungültige Import-Datei. Überprüfen Sie die Werte im ersten Feld. Import abgebrochen!</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_Comm</name>
    <message>
        <location filename="clientsetupwizpage_comm.cpp" line="+49"/>
        <source>Outlook Integration disabled (OUTLOOK NOT INSTALLED)</source>
        <translation>Outlook® -Schnittstelle deaktiviert (Outlook® nicht installiert)</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>%1 folders selected</source>
        <translation>%1 Verzeichnisse ausgewählt</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>Skype Integration disabled (SKYPE NOT INSTALLED)</source>
        <translation>Skype®-Interface deaktiviert (Skype® ist nicht installiert)</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_CommClass</name>
    <message>
        <source>ClientSetupWizPage_Comm</source>
        <translation type="obsolete">ClientSetupWizPage_Comm</translation>
    </message>
    <message>
        <location filename="clientsetupwizpage_comm.ui" line="+25"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;STEP 3/3:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Enable Outlook &amp;amp; Skype Integration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Schritt 3/3:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Outlook &amp;amp; Skype - Integration aktivieren&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Outlook Integration (Automatic Email Import)</source>
        <translation>Outlook-Schnittstelle (automatischer Import von E-Mails)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Select Outlook Folders to be Scanned</source>
        <translation>Zu überwachende Outlook-Verzeichnisse auswählen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>9999; </source>
        <translation>9999; </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Scan Period (minutes):</source>
        <translation>Synchronisationsperiode (Minuten):</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Skype Integration</source>
        <translation>Skype-Schnittstelle</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Listen for Skype Calls</source>
        <translation>Skype-Anrufe empfangen</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_FirstStep</name>
    <message>
        <source>Welcome to SOKRATES® Communicator</source>
        <translation type="obsolete">Willkommen bei SOKRATES® Communicator</translation>
    </message>
    <message>
        <source>SOKRATES® &lt;i&gt;Communicator&lt;/i&gt; is your personal assistant for unified communication, collaboration, and information.</source>
        <translation type="obsolete">SOKRATES® &lt;i&gt;Communicator&lt;/i&gt; ist Ihr persönlicher Assistent für Unified Communication, Collaboration und Information.</translation>
    </message>
    <message>
        <location filename="clientsetupwizpage_firststep.cpp" line="+43"/>
        <source>&lt;br&gt;The following setup wizard will guide you trough a few simple installation steps.</source>
        <translation>&lt;br&gt;Der Installationsassistent wird Sie im Folgenden durch einige einfache Installationsschritte führen.</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Welcome to SOKRATES</source>
        <translation>Willkommen bei SOKRATES</translation>
    </message>
    <message>
        <location line="+0"/>
        <source> Communicator</source>
        <translation> Communicator</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>SOKRATES</source>
        <translation>SOKRATES</translation>
    </message>
    <message>
        <location line="+0"/>
        <source> &lt;i&gt;Communicator&lt;/i&gt; is your personal assistant for unified communication, collaboration, and information.</source>
        <translation> &lt;i&gt;Communicator&lt;/i&gt; ist Ihr persönlicher Assistent für vereinheitlichte Kommunikation, Zusammenarbeit und Information.</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_FirstStepClass</name>
    <message>
        <location filename="clientsetupwizpage_firststep.ui" line="+13"/>
        <source>ClientSetupWizPage_FirstStep</source>
        <translation>ClientSetupWizPage_FirstStep</translation>
    </message>
    <message>
        <location line="+57"/>
        <location line="+35"/>
        <location line="+35"/>
        <location line="+28"/>
        <location line="+40"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Show me the &quot;First Steps&quot; guide &amp;&amp; videos in my browser</source>
        <translation>&quot;Erste Schritte&quot; - Anleitung &amp;&amp; Videos in meinem Browser anzeigen</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_Import</name>
    <message>
        <location filename="clientsetupwizpage_import.cpp" line="+35"/>
        <source>Skype Contacts</source>
        <translation>Skype® Kontakte</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Outlook</source>
        <translation>Outlook®</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_ImportClass</name>
    <message>
        <location filename="clientsetupwizpage_import.ui" line="+13"/>
        <source>ClientSetupWizPage_Import</source>
        <translation>ClientSetupWizPage_Import</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;STEP 4/4:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;Import data&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Schritt 4/4:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;Daten importieren&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Import Contacts From Skype</source>
        <translation>Kontakte importieren aus Skype®</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Import Contacts From Outlook</source>
        <translation>Kontakte importieren aus Outlook®</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Import Emails From Outlook</source>
        <translation>E-Mails importieren aus Outlook®</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_Password</name>
    <message>
        <location filename="clientsetupwizpage_password.cpp" line="+49"/>
        <source>Change password</source>
        <translation>Passwort wechseln</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password not match!</source>
        <translation>Passwort stimmt nicht überein!</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_PasswordClass</name>
    <message>
        <source>ClientSetupWizPage_Password</source>
        <translation type="obsolete">ClientSetupWizPage_Password</translation>
    </message>
    <message>
        <location filename="clientsetupwizpage_password.ui" line="+25"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;STEP 2/3: &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Activate Login-Dialog and set Username &amp; Password. You can change it later in the menu File/Password.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;STEP 2/3: &lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Login-Dialog aktivieren und Benutzername und Passwort setzen. Kann jederzeit im Men&amp;uuml; Datei/Passwort ge&amp;auml;ndert werden.&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Activate Login-Dialog With Password Protection</source>
        <translation>Login-Fenster mit Passwortschutz aktivieren</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>(Old Password:)</source>
        <translation>(Altes Passwort:)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Confirm Password:</source>
        <translation>Passwort bestätigen:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <source>...Access...</source>
        <translation type="obsolete">...Zugriff...</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_PersonData</name>
    <message>
        <location filename="clientsetupwizpage_persondata.cpp" line="+66"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Format Phone</source>
        <translation>Telefonnummer formatieren</translation>
    </message>
</context>
<context>
    <name>ClientSetupWizPage_PersonDataClass</name>
    <message>
        <source>ClientSetupWizPage_PersonData</source>
        <translation type="obsolete">ClientSetupWizPage_PersonData</translation>
    </message>
    <message>
        <location filename="clientsetupwizpage_persondata.ui" line="+25"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;STEP 1/3:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Enter Your Personal Data&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Schritt 1/3:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Eingabe der persönlichen Stammdaten&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+46"/>
        <location line="+7"/>
        <source>Synchronize Names</source>
        <translation>Namen synchronisieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter Address</source>
        <translation>Adresse erfassen</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Formatted Address:</source>
        <translation>Formatierte Adresse:</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Personal Data</source>
        <translation>Persönlich</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Birthday:</source>
        <translation>Geburtstag:</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Ms.</source>
        <translation>Fr.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Mr.</source>
        <translation>Hr.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Nick Name:</source>
        <translation>Rufname:</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Last Name:</source>
        <translation>Nachname:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Location:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Middle Name:</source>
        <translation>Zweiter Vorname:</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Email Address</source>
        <translation>E-Mail-Adresse</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Web Page</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+19"/>
        <source>Email Choose V</source>
        <translation>E-Mail wählen V</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Phone Numbers</source>
        <translation>Telephonnummern</translation>
    </message>
    <message>
        <location line="+84"/>
        <location line="+16"/>
        <location line="+16"/>
        <source>Choose v</source>
        <translation>Auswahl V</translation>
    </message>
</context>
<context>
    <name>CommGridFilter</name>
    <message>
        <location filename="commgridfilter.cpp" line="+208"/>
        <source>Modify Filters</source>
        <translation>Filter bearbeiten</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Delete Filter</source>
        <translation>Filter löschen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Are you sure you want to delete the selected filter?</source>
        <translation>Wollen Sie wirklich den ausgewählten Filter endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Please insert filter name!</source>
        <translation>Filternamen erfassen!</translation>
    </message>
    <message>
        <location line="+260"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Hide All</source>
        <translation>Alle verstecken</translation>
    </message>
    <message>
        <source>Unread</source>
        <translation type="obsolete">Ungelesen</translation>
    </message>
    <message>
        <source>Unassigned</source>
        <translation type="obsolete">Nicht zugewiesen</translation>
    </message>
    <message>
        <source>Missed</source>
        <translation type="obsolete">Verpasst</translation>
    </message>
    <message>
        <source>Scheduled</source>
        <translation type="obsolete">Geplant</translation>
    </message>
    <message>
        <source>Due</source>
        <translation type="obsolete">Fällig</translation>
    </message>
    <message>
        <source>Overdue</source>
        <translation type="obsolete">Überfällig</translation>
    </message>
    <message>
        <source>Templates</source>
        <translation type="obsolete">Vorlagen</translation>
    </message>
    <message>
        <source>Last</source>
        <translation type="obsolete">Letzte</translation>
    </message>
    <message>
        <location line="+954"/>
        <source>Document types</source>
        <translation>Dokumentenarten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Email types</source>
        <translation>E-Mail-Typen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phone Call types</source>
        <translation>Anruf-Typen</translation>
    </message>
    <message>
        <location line="-444"/>
        <source>Filter warning</source>
        <translation>Filter-Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>View Selection and Show Filter groups can not be unchecked at the same time!</source>
        <translation>Entweder &quot;Vordefinierte Ansichten:Auswahl Ansichten&quot; oder &quot;Filter-Kriterien:Zeigen&quot; muss aktiviert sein!</translation>
    </message>
    <message>
        <location line="-921"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
</context>
<context>
    <name>CommGridFilterClass</name>
    <message>
        <location filename="commgridfilter.ui" line="+32"/>
        <source>Communication Grid Filter</source>
        <translation>Filter für Kommunikationsliste</translation>
    </message>
    <message>
        <location line="+423"/>
        <source>Show</source>
        <translation>Zeigen</translation>
    </message>
    <message>
        <location line="-232"/>
        <location line="+254"/>
        <source>Emails</source>
        <translation>E-Mails</translation>
    </message>
    <message>
        <location line="-241"/>
        <location line="+234"/>
        <source>Documents</source>
        <translation> Dokumente</translation>
    </message>
    <message>
        <location line="-260"/>
        <location line="+274"/>
        <source>Phone Calls</source>
        <translation>Telefonanrufe</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>Filter by Date</source>
        <translation>Filtern nach Datum</translation>
    </message>
    <message>
        <location line="-121"/>
        <source>Filter Documents by Application</source>
        <translation>Dokumente filtern nach Anwendung</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Search Text</source>
        <translation>Text suchen</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>in Name</source>
        <translation>in Namen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>in Description</source>
        <translation>in Beschreibung</translation>
    </message>
    <message>
        <location line="+371"/>
        <source>Communication List View</source>
        <translation>Kommunikations-Ansicht</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Open on Startup</source>
        <translation>Beim Starten öffnen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Public</source>
        <translation>Öffentlich</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Sort Code:</source>
        <translation>Sortiercode:</translation>
    </message>
    <message>
        <location line="-1057"/>
        <source>Predefined Selection</source>
        <translation>Vordefinierte Ansichten</translation>
    </message>
    <message>
        <location line="+341"/>
        <source>Filter criteria</source>
        <translation>Filter-Kriterien</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Choose The Filter Criteria:</source>
        <translation>Filterkriterien definieren:</translation>
    </message>
    <message>
        <location line="+322"/>
        <location line="+15"/>
        <source>Filter by Task Type</source>
        <translation>Filter nach Aufgabentyp</translation>
    </message>
    <message>
        <location line="-673"/>
        <source>View Selection</source>
        <translation>Auswahl Ansichten</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Check all items you want to see in the list:</source>
        <translation>In Liste anzuzeigende Elemente markieren:</translation>
    </message>
    <message>
        <location line="+423"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location line="+143"/>
        <location line="+30"/>
        <source>Filter by Type</source>
        <translation>Filtern nach Art</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Task Priority</source>
        <translation>Priorität</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Task Type:</source>
        <translation>Aufgaben-Typ:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Sort</source>
        <translation>Sortieren</translation>
    </message>
</context>
<context>
    <name>CommGridView</name>
    <message>
        <source>by</source>
        <translation type="obsolete">durch</translation>
    </message>
    <message>
        <location filename="commgridview.cpp" line="+774"/>
        <source>Unzip attachment and store document(s)?</source>
        <translation>Anhang entzippen und Dokument(e) speichern?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Successfully created %1 document(s)!</source>
        <translation>%1 Dokumen(e) erfolgreich erstellt!</translation>
    </message>
    <message>
        <source>by </source>
        <translation type="obsolete">durch </translation>
    </message>
    <message>
        <location line="+535"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Modify</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Save A Copy in ...</source>
        <translation>Kopie speichern unter ...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Check Out in ...</source>
        <translation>Dokument auschecken in ...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Load Document(s)...</source>
        <translation>Dokument(e) laden...</translation>
    </message>
    <message>
        <location line="-689"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Appointment import is not supported!</source>
        <translation>Import von Terminen wird nicht unterstützt!</translation>
    </message>
</context>
<context>
    <name>CommWorkBenchWidget</name>
    <message>
        <location filename="commworkbenchwidget.cpp" line="+171"/>
        <location line="+69"/>
        <source>Open Document</source>
        <translation>Dokument öffnen</translation>
    </message>
    <message>
        <location line="-65"/>
        <location line="+69"/>
        <source>Start Task</source>
        <translation>Aufgabe beginnen</translation>
    </message>
    <message>
        <location line="-65"/>
        <location line="+69"/>
        <source>Postpone Task</source>
        <translation>Aufgabe verschieben</translation>
    </message>
    <message>
        <location line="-65"/>
        <location line="+69"/>
        <source>Task Done / Email Read</source>
        <translation>Aufgabe erledigt / Email gelesen</translation>
    </message>
    <message>
        <location line="-66"/>
        <location line="+69"/>
        <location line="+48"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="-115"/>
        <location line="+69"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="-67"/>
        <location line="+69"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="-65"/>
        <location line="+68"/>
        <source>New Document</source>
        <translation>Neues Dokument</translation>
    </message>
    <message>
        <location line="-66"/>
        <location line="+68"/>
        <source>New Email</source>
        <translation>Neues E-Mail</translation>
    </message>
    <message>
        <location line="-66"/>
        <location line="+68"/>
        <source>New Phone Call</source>
        <translation>Neuer Telefonanruf</translation>
    </message>
    <message>
        <location line="-65"/>
        <location line="+68"/>
        <source>Check In</source>
        <translation>Einchecken</translation>
    </message>
    <message>
        <location line="-66"/>
        <location line="+68"/>
        <source>Reply</source>
        <translation>Antworten</translation>
    </message>
    <message>
        <location line="-66"/>
        <location line="+68"/>
        <source>Forward</source>
        <translation>Weiterleiten</translation>
    </message>
    <message>
        <location line="-66"/>
        <location line="+68"/>
        <source>Pack and Send</source>
        <translation>Packen und Versenden</translation>
    </message>
    <message>
        <location line="-63"/>
        <location line="+67"/>
        <source>Sort</source>
        <translation>Sortieren</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Communication &amp; Tasks</source>
        <translation>Kommunikation &amp; Aufgaben</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(Documents, Emails, Phone Calls)</source>
        <translation>(Dokumente, E-Mails, Anrufe)</translation>
    </message>
    <message>
        <location line="+1048"/>
        <location line="+32"/>
        <location line="+36"/>
        <location line="+227"/>
        <location line="+183"/>
        <location line="+16"/>
        <location line="+15"/>
        <location line="+28"/>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+130"/>
        <location line="+1372"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-2066"/>
        <location line="+32"/>
        <location line="+36"/>
        <source> items selected. Do you want to open them all?</source>
        <translation> Elemente ausgewählt. Alle öffnen?</translation>
    </message>
    <message>
        <location line="-68"/>
        <location line="+32"/>
        <location line="+36"/>
        <location line="+227"/>
        <location line="+269"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-564"/>
        <location line="+32"/>
        <location line="+36"/>
        <location line="+227"/>
        <location line="+269"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-281"/>
        <source>Do you really want to mark </source>
        <translation>Wollen Sie wirklich </translation>
    </message>
    <message>
        <source> task(s) as done?</source>
        <translation type="obsolete"> Aufgaben als erledigt markieren?</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>Please select documents for check in!</source>
        <translation>Bitte wählen Sie die einzucheckenden Dokumente aus!</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+15"/>
        <source>Please select emails first!</source>
        <translation>Bitte E-Mail(s) zuerst auswählen! </translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Please select documents to be attached to a new email before pressing Pack&amp;Send!</source>
        <translation>Dokumente für den E-Mail-Anhang zuerst auswählen, bevor Packen &amp; Versenden gestartet wird!</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Please first select an item!</source>
        <translation>Zuerst Element auswählen!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Do you really want to delete </source>
        <translation>Wollen Sie wirklich </translation>
    </message>
    <message>
        <location line="+0"/>
        <source> item(s) permanently from the database?</source>
        <translation> Element(e) endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="+130"/>
        <location line="+216"/>
        <source>Please select a filter first!</source>
        <translation>Zuerst Filter wählen!</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+49"/>
        <location line="+48"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>No Filter Selected</source>
        <translation>Kein Filter gewählt</translation>
    </message>
    <message>
        <source>Contact</source>
        <translation type="obsolete">Kontakt</translation>
    </message>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
    <message>
        <source>Date 1</source>
        <translation type="obsolete">Datum 1</translation>
    </message>
    <message>
        <source>Date 2</source>
        <translation type="obsolete">Datum 2</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Status</translation>
    </message>
    <message>
        <source>Application type</source>
        <translation type="obsolete">Anwendungstyp</translation>
    </message>
    <message>
        <source>Subtype</source>
        <translation type="obsolete">Untertyp</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">Medium</translation>
    </message>
    <message>
        <source>Originator</source>
        <translation type="obsolete">Urheber</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation type="obsolete">Besitzer</translation>
    </message>
    <message>
        <location line="-625"/>
        <source> task(s) as done</source>
        <translation> Aufgaben als erledigt markieren</translation>
    </message>
    <message>
        <location line="+4"/>
        <source> and </source>
        <translation> und </translation>
    </message>
    <message>
        <location line="+2"/>
        <source> email(s) as read</source>
        <translation> E-Mail(s) als gelesen markieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location line="-1477"/>
        <location line="+79"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Show Filter Frame</source>
        <translation>Filterbereich einblenden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Hide Filter Frame</source>
        <translation>Filterbereich ausblenden</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <location line="-67"/>
        <source>Remember View</source>
        <translation>Ansicht merken</translation>
    </message>
    <message>
        <location line="+3243"/>
        <source>Only Internet File type of documents can be saved! Document: %1 not saved.</source>
        <translation>Nur Internet-Datei-Dokumente können gespeichert werden! Dokument %1 nicht gespeichert.</translation>
    </message>
    <message>
        <source>Task Priority</source>
        <translation type="obsolete">Priorität</translation>
    </message>
    <message>
        <source>Task Type ID</source>
        <translation type="obsolete">ID Aufgabentyp</translation>
    </message>
    <message>
        <source>Setting up projects interface...</source>
        <translation type="obsolete">Projekt-Arbeitsumgebung wird konfiguriert...</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Filter is being applied...</source>
        <translation>Filterung wird durchgeführt...</translation>
    </message>
</context>
<context>
    <name>CommWorkBenchWidgetClass</name>
    <message>
        <location filename="commworkbenchwidget.ui" line="+26"/>
        <source>Communication Work Bench</source>
        <translation>Kommunikations-Workbench</translation>
    </message>
    <message>
        <location line="+395"/>
        <source>Contact:</source>
        <translation>Kontakt:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Project:</source>
        <translation>Projekt:</translation>
    </message>
    <message>
        <location line="-70"/>
        <source>Owner:</source>
        <translation>Besitzer:</translation>
    </message>
    <message>
        <location line="-230"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>View:</source>
        <translation>Ansicht:</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+32"/>
        <location line="+19"/>
        <location line="+265"/>
        <location line="+19"/>
        <location line="+19"/>
        <location line="+19"/>
        <location line="+19"/>
        <location line="+35"/>
        <location line="+19"/>
        <location line="+19"/>
        <location line="+35"/>
        <location line="+19"/>
        <location line="+19"/>
        <location line="+35"/>
        <location line="+19"/>
        <location line="+13"/>
        <location line="+35"/>
        <location line="+48"/>
        <location line="+19"/>
        <location line="+35"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-729"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
</context>
<context>
    <name>CommunicationActions</name>
    <message>
        <source>Send Email</source>
        <translation type="obsolete">E-Mail senden</translation>
    </message>
    <message>
        <source>Create New Document</source>
        <translation type="obsolete">Neues Dokument erstellen</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Contact does not have any web address!</source>
        <translation type="obsolete">Der Kontakt hat keine Webadresse!</translation>
    </message>
    <message>
        <source>This contact has no Skype(R) name defined. It is required for chatting!</source>
        <translation type="obsolete">Dieser Kontakt hat keinen Skype(R)-Namen erfasst. Dieser ist für Chats zwingend!</translation>
    </message>
</context>
<context>
    <name>CommunicationManager</name>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Speichern unter</translation>
    </message>
    <message>
        <source>The File </source>
        <translation type="obsolete">Datei </translation>
    </message>
    <message>
        <source> can not be written!</source>
        <translation type="obsolete"> kann nicht geschrieben werden!</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>File can not be written!</source>
        <translation type="obsolete">Datei kann nicht geschrieben werden!</translation>
    </message>
    <message>
        <source>Load Document</source>
        <translation type="obsolete">Dokument laden</translation>
    </message>
    <message>
        <source>File can not be loaded!</source>
        <translation type="obsolete">Datei kann nicht geladen werden!</translation>
    </message>
    <message>
        <location filename="communicationmanager.cpp" line="+726"/>
        <location line="+40"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Application is not set for this document. Operation aborted!</source>
        <translation type="obsolete">Dem Dokument ist keine Anwendung zugewiesen. Operation abgebrochen!</translation>
    </message>
    <message>
        <source>Unable to retrieve latest revision from server</source>
        <translation type="obsolete">Die neueste Version kann nicht vom Server geladen werden</translation>
    </message>
    <message>
        <source>Enter Document Name</source>
        <translation type="obsolete">Eingabe Dokumentenname</translation>
    </message>
    <message>
        <source>Enter Document Name:</source>
        <translation type="obsolete">Eingabe Dokumentenname:</translation>
    </message>
    <message>
        <source>Template file  </source>
        <translation type="obsolete">Vorlagendatei  </translation>
    </message>
    <message>
        <source> does not exists! Operation aborted!</source>
        <translation type="obsolete"> existiert nicht. Operation abgebrochen!</translation>
    </message>
    <message>
        <source>Copy Failed!</source>
        <translation type="obsolete">Kopieren misslungen!</translation>
    </message>
    <message>
        <source>Copy failed to location:  </source>
        <translation type="obsolete">Kopiervorgang misslungen zum Verzeichnis:  </translation>
    </message>
    <message>
        <source>. Operation aborted!</source>
        <translation type="obsolete">. Operation abgebrochen!</translation>
    </message>
    <message>
        <source>The Local File </source>
        <translation type="obsolete">Lokale Datei </translation>
    </message>
    <message>
        <source> is not visible on this computer and can not be opened!</source>
        <translation type="obsolete"> ist nicht sichtbar auf diesem Rechner und kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <source>Open Document</source>
        <translation type="obsolete">Dokument öffnen</translation>
    </message>
    <message>
        <source>Open Document:</source>
        <translation type="obsolete">Dokument öffnen:</translation>
    </message>
    <message>
        <source>Load Original And Modify</source>
        <translation type="obsolete">Original laden und bearbeiten</translation>
    </message>
    <message>
        <source>Load Copy to View Only</source>
        <translation type="obsolete">Kopie zur Ansicht öffnen</translation>
    </message>
    <message>
        <source> Cancel </source>
        <translation type="obsolete"> Abbrechen </translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <source>Check out not possible: Please check-in the document using &apos;Edit&apos; before you can open it!</source>
        <translation type="obsolete">Auschecken nicht möglich: Bitte checken Sie ein Dokument mittels &apos;Bearbeiten&apos; in die Datenbank ein bevor Sie es öffnen können!</translation>
    </message>
    <message>
        <location line="-235"/>
        <location line="+58"/>
        <location line="+5"/>
        <location line="+75"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+5"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-219"/>
        <location line="+58"/>
        <location line="+5"/>
        <location line="+75"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+5"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <source>Automated Temporary Storage mechanism failed to determine Document check out location. Choose path for check-out location?</source>
        <translation type="obsolete">Der Automatismus für die lokale Speicherung von Dokumenten konnte kein temporäres Verzeichnis für das Auschecken bestimmen. Verzeichnispfad auswählen?</translation>
    </message>
    <message>
        <location line="-156"/>
        <location line="+156"/>
        <source> document(s) from dropped items(s)</source>
        <translation> Dokument(e) aus hereingezogenen/r Datei(en)</translation>
    </message>
    <message>
        <source>Failed to create document from template! To create Local Or Internet Document, Template must be either Local or Internet document type!</source>
        <translation type="obsolete">Aus der Vorlage konnte kein Dokument erzeugt werden!</translation>
    </message>
    <message>
        <source>  Save Reference(s) to Local File(s)  </source>
        <translation type="obsolete">  Verweis auf lokale Datei(en) speichern  </translation>
    </message>
    <message>
        <source>File: </source>
        <translation type="obsolete">Datei: </translation>
    </message>
    <message>
        <source>File size can not be larger then 16Mb!</source>
        <translation type="obsolete">Datei darf 16MB Grösse nicht überschreiten!</translation>
    </message>
    <message>
        <source>Save Changes</source>
        <translation type="obsolete">Änderungen speichern</translation>
    </message>
    <message>
        <source>There are some checked out and modified documents. Do you want to check in all modified documents?</source>
        <translation type="obsolete">Es gibt lokal ausgecheckte Dokumente in Bearbeitung. Sollen alle momentan bearbeiteten Dokumente auf den Datenbankserver eingecheckt werden? </translation>
    </message>
    <message>
        <location line="-468"/>
        <source>Processed </source>
        <translation>Verarbeitet </translation>
    </message>
    <message>
        <source>Message does not have Entry ID!</source>
        <translation type="obsolete">Nachricht hat keine Eingabe-ID!</translation>
    </message>
    <message>
        <source>Error getting method from MAPI dll!</source>
        <translation type="obsolete">Fehler beim Zugriff auf MAPI-DLL!</translation>
    </message>
    <message>
        <source>Error opening email!</source>
        <translation type="obsolete">Fehler beim Öffnen einer E-Mail!</translation>
    </message>
    <message>
        <source>Outlook Mail Send function is only available on Windows!</source>
        <translation type="obsolete">Das Versenden von E-Mails über Outlook ist nur unter Windows verfügbar!</translation>
    </message>
    <message>
        <source>MAPI dll is not loaded, Outlook sent feature is unavailable!</source>
        <translation type="obsolete">Die Schnittstelle MAPI.DLL wurde nicht geladen: Das Versenden von E-Mails über Outlook ist nicht verfügbar!</translation>
    </message>
    <message>
        <source>Failed to Pack &amp; Send Documents</source>
        <translation type="obsolete">&apos;Packen &amp; Senden&apos; von Dokumenten misslungen</translation>
    </message>
    <message>
        <source>The Document </source>
        <translation type="obsolete">Das Dokument </translation>
    </message>
    <message>
        <source> can not be check out!</source>
        <translation type="obsolete"> kann nicht zur Bearbeitung ausgecheckt werden!</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation type="obsolete">Name: </translation>
    </message>
    <message>
        <source>Location: </source>
        <translation type="obsolete">Ort: </translation>
    </message>
    <message>
        <source>Failed to zip document </source>
        <translation type="obsolete">Packen (zippen) des Dokuments misslungen </translation>
    </message>
    <message>
        <location line="+249"/>
        <location line="+58"/>
        <location line="+156"/>
        <source>Create Template</source>
        <translation>Vorlage erzeugen</translation>
    </message>
    <message>
        <location line="-214"/>
        <location line="+58"/>
        <location line="+5"/>
        <location line="+151"/>
        <location line="+5"/>
        <source>Create </source>
        <translation>Erzeugen von </translation>
    </message>
    <message>
        <location line="-219"/>
        <location line="+58"/>
        <location line="+156"/>
        <source> template(s) from dropped items(s)</source>
        <translation> Vorlage(n) aus den registrierten Elementen</translation>
    </message>
    <message>
        <location line="-151"/>
        <location line="+156"/>
        <source>Create Documents</source>
        <translation>Dokumente erzeugen</translation>
    </message>
    <message>
        <source>Revision could not be loaded for file:</source>
        <translation type="obsolete">Version für folgende Datei konnte nicht geladen werden:</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Information</translation>
    </message>
    <message>
        <location line="-83"/>
        <source>Email: </source>
        <translation>E-Mail: </translation>
    </message>
    <message>
        <location line="+0"/>
        <source> is already assigned to the other project. Continue with operation and reassign it to the current project?</source>
        <translation> ist bereits einem anderen Projekt zugewiesen. Weiterfahren und neu stattdessen dem aktuellen Projekt zuweisen?</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+14"/>
        <source>Project Assignment</source>
        <translation>Projektzuweisung</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Voice Call is already assigned to the other project. Continue with operation and reassign it to the current project?</source>
        <translation>Der Anruf ist bereits einem anderen Projekt zugewiesen. Weiterfahren und ihn stattdessen neu dem aktuellen Projekt zuweisen?</translation>
    </message>
    <message>
        <source>Please Search Your Installation of </source>
        <translation type="obsolete">Bitte suchen Sie Ihre Installation von </translation>
    </message>
    <message>
        <source>Find Application Path Manually</source>
        <translation type="obsolete">Anwendungspfad manuell suchen</translation>
    </message>
    <message>
        <source>Download/Install Application</source>
        <translation type="obsolete">Anwendung herunterladen/installieren</translation>
    </message>
    <message>
        <source>Download/Install Viewer</source>
        <translation type="obsolete">Viewer herunterladen/installieren</translation>
    </message>
    <message>
        <location line="-170"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Search Application Installation</source>
        <translation type="obsolete">Installierte Anwendung suchen</translation>
    </message>
    <message>
        <source>Failed to load MAPIex.dll (error:%1)</source>
        <translation type="obsolete">Fehler beim Zugriff auf MAPI-DLL: %1</translation>
    </message>
    <message>
        <source>Check out not possible: Please check-in a document using &apos;Edit&apos; before you can open it!</source>
        <translation type="obsolete">Auschecken nicht möglich: Bitte checken Sie ein Dokument mittels &apos;Bearbeiten&apos; in die Datenbank ein bevor Sie es öffnen können!</translation>
    </message>
    <message>
        <source>Failed to check in document</source>
        <translation type="obsolete">Dokument kann nicht eingecheckt werden</translation>
    </message>
    <message>
        <source>Failed to check in document! Document Not Found!</source>
        <translation type="obsolete">Dokument kann nicht eingecheckt werden: Datei nicht gefunden!</translation>
    </message>
    <message>
        <source>  OK  </source>
        <translation type="obsolete">  OK  </translation>
    </message>
    <message>
        <source>  Discard Checked-Out Revision of Document  </source>
        <translation type="obsolete">  Ausgecheckte Dokumentenversion verwerfen  </translation>
    </message>
    <message>
        <location line="-340"/>
        <source>Start Processing Sent Emails From Outlook</source>
        <translation>Start der Verarbeitung der aus Outlook verschickten E-Mails</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>End Processing Sent Emails From Outlook</source>
        <translation>Ende der Verarbeitung der aus Outlook verschickten E-Mails</translation>
    </message>
    <message>
        <source>Open Document: </source>
        <translation type="obsolete">Offenes Dokument: </translation>
    </message>
    <message>
        <source>Transfer In Progress</source>
        <translation type="obsolete">Datenübertragung wird durchgeführt</translation>
    </message>
    <message>
        <source>Transferring document: </source>
        <translation type="obsolete">Übertragenes Dokument: </translation>
    </message>
    <message>
        <source> was not found!</source>
        <translation type="obsolete"> nicht gefunden!</translation>
    </message>
    <message>
        <source>Find Checked-out Document</source>
        <translation type="obsolete">Ausgechecktes Dokument suchen</translation>
    </message>
    <message>
        <source>Check In</source>
        <translation type="obsolete">Einchecken</translation>
    </message>
    <message>
        <location line="+183"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <source>PDF storage location:</source>
        <translation type="obsolete">Speicherort für PDF:</translation>
    </message>
    <message>
        <source>Download and Store pdf File</source>
        <translation type="obsolete">PDF-Datei herunterladen und speichern</translation>
    </message>
    <message>
        <source>Store Link to pdf File</source>
        <translation type="obsolete">Linke auf PDF-Datei speichern</translation>
    </message>
    <message>
        <location line="+215"/>
        <source>Failed to save:</source>
        <translation>Speicherung nicht möglich:</translation>
    </message>
    <message>
        <source>Pdf file can not be downloaded!</source>
        <translation type="obsolete">PDF-Datei kann nicht heruntergeladen werden!</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Connect to </source>
        <translation>Verbinden mit </translation>
    </message>
    <message>
        <location line="-267"/>
        <source>File storage location:</source>
        <translation>Speicherort Datei:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Download and Store File</source>
        <translation>Datei herunterladen und speichern</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Store Link to File</source>
        <translation>Verknüpfung auf Datei speichen</translation>
    </message>
    <message>
        <location line="+255"/>
        <source>File can not be downloaded!</source>
        <translation>Datei kann nicht heruntergeladen werden!</translation>
    </message>
    <message>
        <location line="-50"/>
        <source>Downloading document: </source>
        <translation>Dokument wird heruntergeladen:</translation>
    </message>
</context>
<context>
    <name>CommunicationMenu</name>
    <message>
        <source>Contact</source>
        <translation type="obsolete">Kontakt</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>No internet address found for selected contact(s)!</source>
        <translation type="obsolete">Für den oder die ausgewählten Kontakt(e) sind keine Internetadressen vorhanden!</translation>
    </message>
    <message>
        <location filename="communicationmenu.cpp" line="+119"/>
        <source>Phone Call</source>
        <translation>Telefonanruf</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
</context>
<context>
    <name>CommunicationMenu_Base</name>
    <message>
        <location filename="communicationmenu_base.cpp" line="+192"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+11"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-11"/>
        <location line="+11"/>
        <source>No internet address found for selected contact(s)!</source>
        <translation>Für den oder die ausgewählten Kontakt(e) sind keine Internetadressen vorhanden!</translation>
    </message>
</context>
<context>
    <name>ContactSelection</name>
    <message>
        <location filename="contactselection.cpp" line="+86"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location line="-67"/>
        <location line="+2"/>
        <location line="+2"/>
        <source>Select Alternative Email Address</source>
        <translation>Auswahl einer anderen E-Mail-Adresse</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Contact Selection</source>
        <translation>Auswahl Kontakte</translation>
    </message>
</context>
<context>
    <name>ContactSelectionClass</name>
    <message>
        <location filename="contactselection.ui" line="+13"/>
        <source>Contact Selection</source>
        <translation>Auswahl Kontakt</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>To:</source>
        <translation>An:</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+70"/>
        <location line="+70"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="-90"/>
        <source>CC:</source>
        <translation>CC:</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>BCC:</source>
        <translation>BCC:</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>CountrySettings</name>
    <message>
        <location filename="countrysettings.cpp" line="+17"/>
        <source>Select Country:</source>
        <translation>Land wählen:</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">English</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The selected language will be loaded at the next application start!</source>
        <translation>Die Sprache wird beim nächsten Programmstart aktiviert!</translation>
    </message>
</context>
<context>
    <name>CountrySettingsClass</name>
    <message>
        <location filename="countrysettings.ui" line="+19"/>
        <source>Country Settings</source>
        <translation>Ländereinstellungen</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Select Country:</source>
        <translation>Land wählen:</translation>
    </message>
    <message>
        <location line="-58"/>
        <source>Current Location</source>
        <translation>Aktueller Standort</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Location Name</source>
        <translation>Name des Standorts</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Area Code</source>
        <translation>Regionsvorwahl</translation>
    </message>
    <message>
        <location line="-113"/>
        <source>Default Language</source>
        <translation>Standardsprache</translation>
    </message>
</context>
<context>
    <name>DateRangeSelector</name>
    <message>
        <location filename="daterangeselector.cpp" line="+15"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
</context>
<context>
    <name>DateRangeSelector2</name>
    <message>
        <location filename="daterangeselector2.cpp" line="+103"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>3 Days</source>
        <translation>3 Tage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Business Days</source>
        <translation>Arbeitswoche</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Two Weeks</source>
        <translation>Zwei Wochen</translation>
    </message>
</context>
<context>
    <name>DateRangeSelector2Class</name>
    <message>
        <location filename="daterangeselector2.ui" line="+26"/>
        <source>Date Range Selector</source>
        <translation>Auswahl Zeitperiode</translation>
    </message>
    <message>
        <location line="+114"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="-50"/>
        <location line="+112"/>
        <source>d.M.yyyy</source>
        <translation>d.M.yyyy</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>DateRangeSelectorClass</name>
    <message>
        <location filename="daterangeselector.ui" line="+13"/>
        <source>Date Range Selector</source>
        <translation>Auswahl Zeitperiode</translation>
    </message>
    <message>
        <location line="+37"/>
        <location line="+38"/>
        <source>d.M.yyyy</source>
        <translation>d.M.yyyy</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <source>V</source>
        <translation type="obsolete">V</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
</context>
<context>
    <name>DefineAvailabilityDlgClass</name>
    <message>
        <location filename="DefineAvailabilityDlg.ui" line="+14"/>
        <source>Define Availability</source>
        <translation>Verfügbarkeit definieren</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Mo</source>
        <translation>Mo</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tu</source>
        <translation>Di</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>We</source>
        <translation>Mi</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Th</source>
        <translation>Do</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fr</source>
        <translation>Fr</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sa</source>
        <translation>Sa</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Su</source>
        <translation>So</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>DeptSelectWizPageClass</name>
    <message>
        <location filename="deptselectwizpage.ui" line="+19"/>
        <source>Deptartment Selection</source>
        <translation>Auswahl Abteilung</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
</context>
<context>
    <name>Dlg_AddrSchemaInfo</name>
    <message>
        <location filename="dlg_addrschemainfo.cpp" line="+9"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
</context>
<context>
    <name>Dlg_AddrSchemaInfoClass</name>
    <message>
        <location filename="dlg_addrschemainfo.ui" line="+24"/>
        <source>Address Schema</source>
        <translation>Adressschema</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>If some text should only be displayed if an address field is not empty, use nested square brackets.
Example: The following text &quot; - &quot; will not be displayed if the value of the related address field &quot;Country&quot; is empty:

Expression:  [[Country] - ]
Result:    &quot;CH - &quot; or &quot;&quot;

Only if &quot;Country&quot; has a valid value, the dependent string &quot; - &quot; will be shown in the formatted address.</source>
        <translation>Falls Text nur angezeigt werden soll, falls ein Adressfeld nicht leer ist, können verschachtelte eckige Klammern verwendet werden.
Beispiel: Der text &quot; - &quot; wird nicht angezeigt, falls der Wert des zugehörigen Adressfeldes &quot;Country&quot; leer ist:

Ausdruck:  [[Country] - ]
Resultat:    &quot;CH - &quot; oder &quot;&quot;

Nur falls &quot;Country&quot; einen gültigen Wert enthält, wird der davon abhängige Text &quot; - &quot; in der formatierten Adresse angezeigt.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactAddress</name>
    <message>
        <source>From Internet</source>
        <translation type="obsolete">Aus Internet</translation>
    </message>
    <message>
        <source>Find In TwixTel</source>
        <translation type="obsolete">Auf TwixTel suchen</translation>
    </message>
    <message>
        <location filename="dlg_contactaddress.cpp" line="+52"/>
        <source>Format Address</source>
        <translation>Adresse formatieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fill Fields From Address</source>
        <translation>Felder aus Adresse erzeugen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Modify Address Schema</source>
        <translation>Adressschema bearbeiten</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Enter Address</source>
        <translation>Adresse erfassen</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Not yet implemented</source>
        <translation type="obsolete">Noch nicht implementiert</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactAddressClass</name>
    <message>
        <location filename="dlg_contactaddress.ui" line="+13"/>
        <source>Contact Address</source>
        <translation>Kontaktadresse</translation>
    </message>
    <message>
        <location line="+497"/>
        <location line="+23"/>
        <source>PushButton</source>
        <translation>PushButton</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Middle Name:</source>
        <translation>Zweiter Vorname:</translation>
    </message>
    <message>
        <location line="-376"/>
        <source>Last Name:</source>
        <translation>Nachname:</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>City:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>ZIP/Postal Code:</source>
        <translation>PLZ/Postleitzahl:</translation>
    </message>
    <message>
        <location line="-164"/>
        <source>PO BOX:</source>
        <translation>Postfach:</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>State/Province:</source>
        <translation>Region:</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Organization 2:</source>
        <translation>Organisation 2:</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Salutation:</source>
        <translation>Anrede:</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Office Code:</source>
        <translation>Raumnummer:</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>PO BOX ZIP:</source>
        <translation>PLZ Postfach:</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Country:</source>
        <translation>Land:</translation>
    </message>
    <message>
        <location line="+217"/>
        <source>Street:</source>
        <translation>Strasse:</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <source>From Internet</source>
        <translation type="obsolete">Aus Internet</translation>
    </message>
    <message>
        <source>From TwixTel</source>
        <translation type="obsolete">Auf TwixTel suchen</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-261"/>
        <source>Synchronize Names</source>
        <translation>Namen synchronisieren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="-134"/>
        <source>Short Salutation:</source>
        <translation>Kurzanrede:</translation>
    </message>
    <message>
        <location line="-22"/>
        <source>Web Lookup ZIP/Town</source>
        <translation>PLZ/Ort im Web suchen</translation>
    </message>
    <message>
        <location line="+373"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactAddressSchemas</name>
    <message>
        <location filename="dlg_contactaddressschemas.cpp" line="+14"/>
        <source>Define Address Schema</source>
        <translation>Adressschema definieren</translation>
    </message>
    <message>
        <location line="+120"/>
        <location line="+5"/>
        <location line="+39"/>
        <location line="+100"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-144"/>
        <location line="+44"/>
        <source>Schema name is mandatory field</source>
        <translation>Name für Schema zwingend</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Invalid Schema, count of open/closed brackets does not match!</source>
        <translation>Schema ungültig, oder die eckigen Klammern sind falsch gesetzt!</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+36"/>
        <location line="+24"/>
        <location line="+49"/>
        <location line="+19"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Do you really want to delete the selected schema from the database</source>
        <translation>Wollen Sie wirklich das ausgewählte Schema endgültig aus der Datenbank löschen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactAddressSchemasClass</name>
    <message>
        <location filename="dlg_contactaddressschemas.ui" line="+16"/>
        <source>Contact Address Schemas</source>
        <translation>Adressschemas Kontakt</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>View:</source>
        <translation>Ansicht:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactBuildAll</name>
    <message>
        <source>Build Actual Contact List</source>
        <translation type="obsolete">Aktuelle Kontaktliste aufbauen</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactBuildAllClass</name>
    <message>
        <source>Build All Contacts</source>
        <translation type="obsolete">Alle Kontakte aufbauen</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Operation</source>
        <translation type="obsolete">Mengenoperation</translation>
    </message>
    <message>
        <source>Add To Contact List</source>
        <translation type="obsolete">Zu aktueller Kontaktliste dazufügen</translation>
    </message>
    <message>
        <source>Remove From Contact List</source>
        <translation type="obsolete">Aus Kontaktliste entfernen</translation>
    </message>
    <message>
        <source>Replace Contact List</source>
        <translation type="obsolete">Kontaktliste ersetzen</translation>
    </message>
    <message>
        <source>Intersect With Contact List</source>
        <translation type="obsolete">Schnittmenge mit Kontaktliste bilden</translation>
    </message>
    <message>
        <source>Build List</source>
        <translation type="obsolete">Liste aufbauen</translation>
    </message>
    <message>
        <source>Based on Search:</source>
        <translation type="obsolete">Suche:</translation>
    </message>
    <message>
        <source>Build All</source>
        <translation type="obsolete">Alle auflisten</translation>
    </message>
    <message>
        <source>Based on Group:</source>
        <translation type="obsolete">Gruppe:</translation>
    </message>
    <message>
        <source>Based on Selection:</source>
        <translation type="obsolete">Auswahl:</translation>
    </message>
    <message>
        <source>Based on Filter:</source>
        <translation type="obsolete">Filter:</translation>
    </message>
    <message>
        <source>Search 2:</source>
        <translation type="obsolete">Suche 2:</translation>
    </message>
    <message>
        <source>Search Field 2:</source>
        <translation type="obsolete">Suchfeld 2:</translation>
    </message>
    <message>
        <source>Search 1:</source>
        <translation type="obsolete">Suche 1:</translation>
    </message>
    <message>
        <source>Search Field 1:</source>
        <translation type="obsolete">Suchfeld 1:</translation>
    </message>
    <message>
        <source>Do not use Exclusion Group</source>
        <translation type="obsolete">Ausschlussgruppe nicht verwenden</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactJournal</name>
    <message>
        <location filename="dlg_contactjournal.cpp" line="+18"/>
        <source>Journal Entry</source>
        <translation>Journaleintrag</translation>
    </message>
    <message>
        <location line="+57"/>
        <location line="+60"/>
        <location line="+24"/>
        <location line="+20"/>
        <location line="+23"/>
        <location line="+10"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Person must be set!</source>
        <translation>Person muss eingetragen sein!</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactJournalClass</name>
    <message>
        <location filename="dlg_contactjournal.ui" line="+13"/>
        <source>Contact Journal</source>
        <translation>Kontaktjournal</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Is Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Person:</source>
        <translation>Person:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactPicture</name>
    <message>
        <location filename="dlg_contactpicture.cpp" line="+23"/>
        <source>Add a Picture</source>
        <translation>Bild hinzufügen</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactPictureClass</name>
    <message>
        <location filename="dlg_contactpicture.ui" line="+13"/>
        <source>Contact Picture</source>
        <translation>Bild zu Kontakt</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location line="-15"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactVcardImport</name>
    <message>
        <location filename="dlg_contactvcardimport.cpp" line="+62"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Format Phone</source>
        <translation>Telefonnummer formatieren</translation>
    </message>
    <message>
        <location line="+121"/>
        <source>To add data to existing contact, please select contact or exit edit/insert mode</source>
        <translation>Um zu einem bestehenden Kontakt etwas zuzuweisen, wählen Sie bitte zuerst einen Kontakt aus oder verlassen Sie den Bearbeitungs-/Einfügemodus</translation>
    </message>
    <message>
        <location filename="dlg_contactvcardimport.ui" line="+14"/>
        <source>Dlg_ContactVcardImport</source>
        <translation>Dlg_ContactVcardImport</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Create a new contact from the vCard</source>
        <translation>Neuen Kontakt aus einer vCard erzeugen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add data from the vCard to the actual contact</source>
        <translation>Daten aus der vCard dem aktuellen Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Phone Numbers</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+45"/>
        <location line="+45"/>
        <location line="+45"/>
        <source>Choose v</source>
        <translation>Auswahl V</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_CopyEntity</name>
    <message>
        <source>Select Groups For Copy</source>
        <translation type="obsolete">Auswahl Gruppen zum Kopieren</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="obsolete">Gruppenname</translation>
    </message>
</context>
<context>
    <name>Dlg_CopyEntityClass</name>
    <message>
        <source>Dlg_CopyEntity</source>
        <translation type="obsolete">Dlg_CopyEntity</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Select Groups For Copy</source>
        <translation type="obsolete">Auswahl Gruppen zum Kopieren</translation>
    </message>
</context>
<context>
    <name>Dlg_FTPServers</name>
    <message>
        <location filename="dlg_ftpservers.cpp" line="+19"/>
        <source>Socks5 Proxy</source>
        <translation>Socks5 Proxy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Http  Proxy</source>
        <translation>Http  Proxy</translation>
    </message>
    <message>
        <location line="+215"/>
        <source>New Connection</source>
        <translation>Neue Verbindung</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Success</source>
        <translation>Erfolgreich</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Successfully connected to host!</source>
        <translation>Erfolgreich mit Hostsystem verbunden!</translation>
    </message>
    <message>
        <source>Successfully changed directory!</source>
        <translation type="obsolete">Verzeichnis erfolgreich gewechselt!</translation>
    </message>
</context>
<context>
    <name>Dlg_FTPServersClass</name>
    <message>
        <location filename="dlg_ftpservers.ui" line="+13"/>
        <source>FTP Connections</source>
        <translation>FTP-Verbindungen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>FTP Connection Name:</source>
        <translation>FTP-Verbindungsname:</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>FTP Connection</source>
        <translation>FTP-Verbindung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Connection Name:</source>
        <translation>Verbindungsname:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter custom connection name here</source>
        <translation>Benutzerdefinierter Verbindungsname</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>FTP Host:</source>
        <translation>FTP Host:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+163"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location line="-114"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+160"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="-143"/>
        <source>Start Directory:</source>
        <translation>Startverzeichnis:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Passive mode</source>
        <translation>Passiv-Modus</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Use Proxy</source>
        <translation>Proxy verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Proxy type:</source>
        <translation>Proxy-Typ:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Host ID:</source>
        <translation>Host ID:</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Test Connection</source>
        <translation>Verbindung testen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_GridViews</name>
    <message>
        <location filename="dlg_gridviews.cpp" line="+20"/>
        <source>Configure Column Order</source>
        <translation>Spaltenreihenfolge konfigurieren</translation>
    </message>
    <message>
        <location line="+132"/>
        <location line="+49"/>
        <location line="+113"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+49"/>
        <source>Schema name is mandatory field</source>
        <translation>Name für Schema zwingend</translation>
    </message>
    <message>
        <location line="-26"/>
        <location line="+43"/>
        <location line="+26"/>
        <location line="+58"/>
        <location line="+21"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Do you really want to delete the selected view from the database</source>
        <translation>Wollen Sie wirklich die ausgewählte Anischt endgültig aus der Datenbank löschen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>Dlg_GridViewsClass</name>
    <message>
        <location filename="dlg_gridviews.ui" line="+13"/>
        <source>Grid Views</source>
        <translation>Listenansichten</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>View:</source>
        <translation>Ansicht:</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>/\</source>
        <translation>/\</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>\/</source>
        <translation>\/</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
</context>
<context>
    <name>Dlg_Messenger</name>
    <message>
        <location filename="dlg_messenger.cpp" line="+16"/>
        <source>Messenger</source>
        <translation>Messenger</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+67"/>
        <source>Show Logged Messages</source>
        <translation>Protokollierte Meldungen anzeigen</translation>
    </message>
    <message>
        <location line="-19"/>
        <source>You have unread messages</source>
        <translation>Sie haben ungelesene Meldungen</translation>
    </message>
</context>
<context>
    <name>Dlg_MessengerClass</name>
    <message>
        <location filename="dlg_messenger.ui" line="+13"/>
        <source>Messenger</source>
        <translation>Nachrichtenzentrum</translation>
    </message>
</context>
<context>
    <name>Dlg_NMRXEditor</name>
    <message>
        <location filename="dlg_nmrxeditor.cpp" line="+54"/>
        <source>Define a Relationship</source>
        <translation>Relation definieren</translation>
    </message>
    <message>
        <location line="+156"/>
        <location line="+274"/>
        <location line="+45"/>
        <location line="+32"/>
        <location line="+10"/>
        <location line="+10"/>
        <location line="+22"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-205"/>
        <location line="+7"/>
        <location line="+108"/>
        <location line="+29"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-144"/>
        <source>Assignment must be set!</source>
        <translation>Zuweisung muss gesetzt werden!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Assigment must be set!</source>
        <translation>Zuweisung muss gesetzt werden!</translation>
    </message>
    <message>
        <location line="-345"/>
        <location line="+1"/>
        <source>Send Invite By Sokrates</source>
        <translation>Einladung durch SOKRATES versenden</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+1"/>
        <source>Send Invite By Email</source>
        <translation>Einladung über E-Mail versenden</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+1"/>
        <source>Send Invite By SMS</source>
        <translation>Einladung über SMS versenden</translation>
    </message>
    <message>
        <location line="+442"/>
        <location line="+29"/>
        <source>You do not have sufficient access rights to perform this operation!</source>
        <translation>Kein Zugriffsrecht!</translation>
    </message>
</context>
<context>
    <name>Dlg_NMRXEditorClass</name>
    <message>
        <location filename="dlg_nmrxeditor.ui" line="+14"/>
        <source>NMRX Editor</source>
        <translation>Verknüpfungs-Editor</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Master:</source>
        <translation>Master:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Role:</source>
        <translation>Rolle:</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+29"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Email Template:</source>
        <translation>E-Mail-Vorlage:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Email Address:</source>
        <translation>E-Mail-Adresse:</translation>
    </message>
    <message>
        <location line="+229"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-254"/>
        <source>Invite</source>
        <translation>Einladen</translation>
    </message>
    <message>
        <location line="+169"/>
        <source>Select SMS</source>
        <translation>SMS wählen</translation>
    </message>
</context>
<context>
    <name>Dlg_OrderForm</name>
    <message>
        <source>Order Now</source>
        <translation type="obsolete">Jetzt bestellen</translation>
    </message>
</context>
<context>
    <name>Dlg_OrderFormClass</name>
    <message>
        <source>Order Form</source>
        <translation type="obsolete">Bestellformular</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;To start using program, you must order trial or full license:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Um das Programm verwenden zu können, müssen Sie eine Evaluationsversion oder eine lizensierte Vollversion bestellen:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Order Trial Version</source>
        <translation type="obsolete">Evaluationsversion bestellen</translation>
    </message>
    <message>
        <source>Order Full Version</source>
        <translation type="obsolete">Vollversion bestellen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600;&quot;&gt;To start using the application,  you have these options:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;a) Order a trial or a full licence and install it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;b) Continue and enter your access data manually&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;c) Cancel and start your purchased licence keyfile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600;&quot;&gt;Um weiterzufahren haben Sie folgende Möglichkeiten:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;a) Lizenz für Vollversion bestellen und installieren&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;b) Weiterfahren und erhaltene Verbindungsdaten von Hand konfigurieren&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;c) Abbrechen und erworbenen Lizenzschlüssel installieren&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="obsolete">Weiterfahren</translation>
    </message>
</context>
<context>
    <name>Dlg_PasswordChange</name>
    <message>
        <location filename="dlg_passwordchange.cpp" line="+20"/>
        <source>Change Password</source>
        <translation>Passwort wechseln</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password not match!</source>
        <translation>Passwort stimmt nicht überein!</translation>
    </message>
    <message>
        <location line="+64"/>
        <location line="+2"/>
        <source>Re-login</source>
        <translation>Erneut einloggen</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Password successfully changed, please restart application. Hide login dialog option will be effective on next application start.</source>
        <translation>Passwort erfolgreich geändert. Bitte starten Sie das Programm neu. Ab dem nächsten Programmstart wird das Loginfenster nicht mehr angezeigt.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Password successfully changed, please re-login</source>
        <translation>Passwort geändert, bitte neu einloggen</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>You must set password when changing username!</source>
        <translation>Bei einem Wechsel des Benutzernamens muss das Passwort eingegeben werden!</translation>
    </message>
</context>
<context>
    <name>Dlg_PasswordChangeClass</name>
    <message>
        <location filename="dlg_passwordchange.ui" line="+20"/>
        <source>Password Change</source>
        <translation>Passwort ändern</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Old Password:</source>
        <translation>Altes Passwort:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Confirm Password:</source>
        <translation>Passwort bestätigen:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Hide Login Prompt Dialog (only when password empty)</source>
        <translation>Loginfenster überspringen (nur falls Passwort leer ist)</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_PhoneFormatterClass</name>
    <message>
        <location filename="dlg_phoneformatter.ui" line="+21"/>
        <source>Format Phone</source>
        <translation>Telefonnummer formatieren</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Area Code:</source>
        <translation>Regionsvorwahl:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Country Predial:</source>
        <translation>Landesvorwahl:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Phone Details:</source>
        <translation>Vollständige Telephonnummer:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Local:</source>
        <translation>Lokale Nummer:</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_RFParser</name>
    <message>
        <location filename="dlg_rfparser.cpp" line="+62"/>
        <source>Load registration xml file:</source>
        <translation>XML-Registrierungdatei laden:</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>File </source>
        <translation>Datei </translation>
    </message>
    <message>
        <location line="+0"/>
        <source> does not exists!</source>
        <translation> existiert nicht!</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+18"/>
        <location line="+7"/>
        <source>File: </source>
        <translation>Datei: </translation>
    </message>
    <message>
        <location line="-30"/>
        <source> can not be read!</source>
        <translation> kann nicht gelesen werden!</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+18"/>
        <location line="+7"/>
        <source> can not be parsed: invalid xml format!</source>
        <translation> kann nicht analysiert werden: ungültiges XML-Format!</translation>
    </message>
    <message>
        <location line="+132"/>
        <source>Applications</source>
        <translation>Anwendungen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Templates</source>
        <translation>Vorlagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Interfaces</source>
        <translation>Schnittstellen</translation>
    </message>
</context>
<context>
    <name>Dlg_RFParserClass</name>
    <message>
        <location filename="dlg_rfparser.ui" line="+13"/>
        <source>Registration file parser</source>
        <translation>Parser für Registrierungsdatei</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Select items for registration in the SOKRATES® Communicator&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Auswahl von Elementen für die Registrierung in SOKRATES® Communicator&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Dlg_SerialEmailsMode</name>
    <message>
        <location filename="dlg_serialemailsmode.ui" line="+14"/>
        <source>Serial Email Letter</source>
        <translation>Serien-E-Mail</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Send to default email address</source>
        <translation>Nur an Standard-Emailadresse senden</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Send to all email addresses</source>
        <translation>An alle Emailadressen senden</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_StorageLocation</name>
    <message>
        <source>Document Management</source>
        <translation type="obsolete">Dokumentenmanagement</translation>
    </message>
    <message>
        <source>Save a reference to a local file. The file itself keeps being stored locally where it is (on your computer or on your local server). A double-click on this file name in Communicator will open the original file. Local files can only be used inside your own network (LAN) and are not accessible from other locations.</source>
        <translation type="obsolete">Sie können eine Referenz auf eine bestehendeDatei speichern. Dabei bleibt die Datei lokal gespeichert, wo sie ist(auf Ihrem Computer oder auf Ihrem lokalen Server). Ein Doppelklick aufdiese Datei in Communicator öffnet die Originaldatei. Lokale Dateienkönnen nur innerhalb des eigenen Netzwerkes (LAN) verwendet werden undsind (im Gegensatz zu Internet-Dateien) von anderen Standorten ausnicht verfügbar.</translation>
    </message>
    <message>
        <source>Save a complete file in the central database of SOKRATES(R) Communicator, on the SOKRATES (R) Application Server. These files are available everywhere, even from other location over the internet.</source>
        <translation type="obsolete">Save a complete file in the central database ofSOKRATES(R) Communicator, on the SOKRATES (R) Application Server. Thesefiles are available everywhere, even from other location over theinternet.</translation>
    </message>
</context>
<context>
    <name>Dlg_StorageLocationClass</name>
    <message>
        <source>Document Management</source>
        <translation type="obsolete">Dokumentenmanagement</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Choose Document Storage Location&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Speicherort für Dokument bestimmen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>  Save Reference(s) to Local File(s)  </source>
        <translation type="obsolete">  Verweis(e) auf lokale Datei(en) speichern  </translation>
    </message>
    <message>
        <source>  Store File Centrally on Internet  </source>
        <translation type="obsolete">  Datei zentral in (Internet-)Datenbank speichern  </translation>
    </message>
    <message>
        <source> Cancel </source>
        <translation type="obsolete"> Abbrechen </translation>
    </message>
</context>
<context>
    <name>Dlg_TemplateDb</name>
    <message>
        <source>Information</source>
        <translation type="obsolete">Information</translation>
    </message>
    <message>
        <source>Application will close now. Server will restart. Restart application in few minutes to start using new database.</source>
        <translation type="obsolete">Die Anwendung wird nun beendet, und der Server wird neu gestartet. Bitte starten Sie die Anwendung wieder in ein paar Minuten, um die neue Datenbank zu verwenden.</translation>
    </message>
</context>
<context>
    <name>Dlg_TemplateDbClass</name>
    <message>
        <source>Template Databases</source>
        <translation type="obsolete">Datenbankvorlagen</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Choose template database (pressing Cancel current database will be used). Later on you can start this wizard again in menu File/Admin Tools.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Vorlagendatanbank wählen (ABBRECHEN verwendet die aktuelle Datenbank). Dieser Assistent kann auch später im Menü Datei/Administrationswerkzeuge gestartet werden.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Current Template:</source>
        <translation type="obsolete">Aktuelle Vorlage:</translation>
    </message>
    <message>
        <source>Choose Template:</source>
        <translation type="obsolete">Vorlage wählen:</translation>
    </message>
</context>
<context>
    <name>Dlg_TrialPopUp</name>
    <message>
        <source>Order Now</source>
        <translation type="obsolete">Jetzt bestellen</translation>
    </message>
    <message>
        <source>Select keyfile or registration utility</source>
        <translation type="obsolete">Keyfile (Lizenzschlüssel) oder das Registrierungsprogramm auswählen</translation>
    </message>
    <message>
        <source>Your trial version of SOKRATES®  Communicator has expired! You can purchase a license keyfile for the unlimited version.</source>
        <translation type="obsolete">Ihre Evaluationsversion von SOKRATES®  Communicator  ist abgelaufen! Bitte erwerben Sie einen Lizenzschlüssel für eine unbeschränkte Vollersion.</translation>
    </message>
    <message>
        <source>This is a trial version of SOKRATES®  Communicator. You can use it for a </source>
        <translation type="obsolete">Dies ist eine Evaluationsversion von  SOKRATES®  Communicator. Sie dürfen Sie verwenden für einen Testzeitraum von   </translation>
    </message>
    <message>
        <source> day trial period or you can purchase a licence keyfile for the unlimited version.</source>
        <translation type="obsolete">   Tagen, oder jederzeit eine unbeschränkte Lizenz erwerben.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Please restart application, to start using registered version!</source>
        <translation type="obsolete">Die registrierte Anwendung ist nach einem Neustart aktiv!</translation>
    </message>
</context>
<context>
    <name>Dlg_TrialPopUpClass</name>
    <message>
        <source>Dlg_TrialPopUp</source>
        <translation type="obsolete">Dlg_TrialPopUp</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;This is a trial version of SOKRATES®  Communicator. You can use it for a 30 day trial period or you can purchase a licence keyfile for the unlimited version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Dies ist eine Evaluationsversion von SOKRATES®  Communicator. Sie dürfen sie für 30 Tage zu Evaluationszwecken verwenden, oder Sie können eine Lizenzdatei (Keyfile) für die uneingeschränkte Verwendung bestellen.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Order Trial</source>
        <translation type="obsolete">Evaluationsversion bestellen</translation>
    </message>
    <message>
        <source>Order Full Version</source>
        <translation type="obsolete">Vollversion bestellen</translation>
    </message>
    <message>
        <source>Register Purchased Keyfile</source>
        <translation type="obsolete">Erworbenes Keyfile (Lizenzschlüssel) registrieren</translation>
    </message>
    <message>
        <source>Continue With Trial</source>
        <translation type="obsolete">Mit Evaluation fortfahren</translation>
    </message>
    <message>
        <source>Trial</source>
        <translation type="obsolete">Evaluation</translation>
    </message>
</context>
<context>
    <name>Doc_MenuWidget</name>
    <message>
        <location filename="doc_menuwidget.cpp" line="+135"/>
        <source>Modify/Create</source>
        <translation>Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="-20"/>
        <location line="+77"/>
        <source>New Document From Template</source>
        <translation>Neues Dokument aus Vorlage</translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+80"/>
        <source>New Task From Template</source>
        <translation>Neue Aufgabe aus Vorlage</translation>
    </message>
    <message>
        <location line="-59"/>
        <source>My Application Paths</source>
        <translation>Meine Anwendungspfade</translation>
    </message>
    <message>
        <location line="+67"/>
        <location line="+37"/>
        <location line="+46"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location line="-73"/>
        <source>Add Template</source>
        <translation>Vorlage hinzufügen</translation>
    </message>
    <message>
        <location line="-111"/>
        <location line="+115"/>
        <source>Modify Template</source>
        <translation>Vorlage bearbeiten</translation>
    </message>
    <message>
        <location line="-114"/>
        <location line="+118"/>
        <source>Delete Template</source>
        <translation>Vorlage löschen</translation>
    </message>
    <message>
        <location line="-117"/>
        <source>New Template</source>
        <translation>Neue Vorlage</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>New Document Using Application</source>
        <translation>Neues Dokument für Anwendung</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Add Application</source>
        <translation>Anwendung hinzufügen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Modify Application</source>
        <translation>Anwendung bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete Application</source>
        <translation>Anwendung löschen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>View my application paths</source>
        <translation>Meine Anwendungspfade zeigen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Save/Check In Document</source>
        <translation>Dokument speichern/einchecken</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Open Document</source>
        <translation>Dokument öffnen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Open Document Details</source>
        <translation>Dokumentendaten öffnen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unlock And Ignore All Local Changes</source>
        <translation>Entsperren und Änderungen verwerfen</translation>
    </message>
    <message>
        <location line="+387"/>
        <location line="+104"/>
        <location line="+23"/>
        <location line="+9"/>
        <location line="+167"/>
        <location line="+75"/>
        <location line="+76"/>
        <location line="+22"/>
        <location line="+9"/>
        <location line="+324"/>
        <location line="+154"/>
        <location line="+27"/>
        <location line="+21"/>
        <location line="+22"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-1033"/>
        <location line="+104"/>
        <location line="+23"/>
        <location line="+682"/>
        <source>Please first select an template!</source>
        <translation>Vorlage zuerst auswählen!</translation>
    </message>
    <message>
        <location line="-673"/>
        <location line="+349"/>
        <source>Do you really want to delete this record permanently from the database?</source>
        <translation>Wollen Sie diesen Datensatz endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="-349"/>
        <location line="+349"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-349"/>
        <location line="+349"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-333"/>
        <location line="+349"/>
        <location line="+398"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-596"/>
        <location line="+151"/>
        <location line="+22"/>
        <source>Please first select an application!</source>
        <translation>Bitte zuerst eine Anwendung auswählen!</translation>
    </message>
    <message>
        <source> Cancel </source>
        <translation type="obsolete"> Abbrechen </translation>
    </message>
    <message>
        <location line="-98"/>
        <source>Could not load template from application record!</source>
        <translation>Default-Vorlage für Anwendung kann nicht geladen werden!</translation>
    </message>
    <message>
        <source>Pick Existing Document</source>
        <translation type="obsolete">Bestehendes Dokument auswählen</translation>
    </message>
    <message>
        <location line="+585"/>
        <location line="+27"/>
        <location line="+21"/>
        <location line="+22"/>
        <source>Please first select an document!</source>
        <translation>Dokument zuerst auswählen!</translation>
    </message>
    <message>
        <source>New Document</source>
        <translation type="obsolete">Neues Dokument</translation>
    </message>
    <message>
        <location line="-1596"/>
        <source>New Document For Selected Application</source>
        <translation>Neues Dokument für gewählte Anwendung</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Documents &amp; Applications</source>
        <translation>Dokumente &amp; Anwendungen</translation>
    </message>
    <message>
        <location line="+1735"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Document Manager</source>
        <translation>Dokumenten-Manager</translation>
    </message>
    <message>
        <location line="-1728"/>
        <source>Documents</source>
        <translation> Dokumente</translation>
    </message>
    <message>
        <location line="+289"/>
        <source>Note</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Internet File</source>
        <translation>Internet-Datei</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Local File </source>
        <translation>Lokale Datei </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paper Document</source>
        <translation>Papier-Dokument</translation>
    </message>
</context>
<context>
    <name>Doc_MenuWidgetClass</name>
    <message>
        <location filename="doc_menuwidget.ui" line="+19"/>
        <source>Document Menu</source>
        <translation>Menü Dokumente</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Templates</source>
        <translation>Vorlagen</translation>
    </message>
    <message>
        <location line="+134"/>
        <source>Applications</source>
        <translation>Anwendungen</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Register</source>
        <translation>Registrieren</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Register File</source>
        <translation>Datei registrieren</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Checked Out</source>
        <translation>Ausgecheckt</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Check In</source>
        <translation>Einchecken</translation>
    </message>
    <message>
        <location line="-230"/>
        <location line="+137"/>
        <location line="+116"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location line="-76"/>
        <source>Import Applications/Templates</source>
        <translation>Anwendungen/Vorlagen importieren</translation>
    </message>
</context>
<context>
    <name>Document_PieMenu</name>
    <message>
        <location filename="document_piemenu.cpp" line="+32"/>
        <source>Local File </source>
        <translation>Lokale Datei </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Internet File</source>
        <translation>Internet-Datei</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Note</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paper Document</source>
        <translation>Papier-Dokument</translation>
    </message>
    <message>
        <source>Choose Document Storage Location</source>
        <translation type="obsolete">Speicherort für Dokument bestimmen</translation>
    </message>
    <message>
        <source>Choose Document Storage Location:</source>
        <translation type="obsolete">Speicherort für Dokument bestimmen:</translation>
    </message>
    <message>
        <source>  Save Reference(s) to Local File(s)  </source>
        <translation type="obsolete">  Verweis(e) auf lokale Datei(en) speichern  </translation>
    </message>
    <message>
        <source>  Store File Centrally on Internet  </source>
        <translation type="obsolete">  Datei zentral in (Internet-)Datenbank speichern  </translation>
    </message>
    <message>
        <source> Cancel </source>
        <translation type="obsolete"> Abbrechen </translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>You do not have permission to save document either locally or on Internet!</source>
        <translation type="obsolete">Kein Zugriffsrecht um das Dokument lokal oder zentral (Datenbank, Internet) zu speichern!</translation>
    </message>
</context>
<context>
    <name>DropZone_MenuWidget</name>
    <message>
        <location filename="dropzone_menuwidget.cpp" line="+97"/>
        <source>Add Selected Text As Internet Address to the Contact</source>
        <translation>Markierten Text als Internetadresse dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Selected Text As Address to the Contact</source>
        <translation>Markierten Text als Adresse dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Selected Text As Email Address to the Contact</source>
        <translation>Markierten Text als E-Mail-Adresse dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Selected Text As Phone Number to the Contact</source>
        <translation>Markierten Text als Telefonnummer dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+606"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Picture storage location:</source>
        <translation>Bild-Speicherort:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Download Picture And Set As Contact Picture</source>
        <translation>Bild herunterladen und als Kontaktbild verwenden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Store Link/File As Document</source>
        <translation>Link/Datei als Dokument speichern</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Connect to </source>
        <translation>Verbinden mit </translation>
    </message>
    <message>
        <location line="-702"/>
        <source>Drop items here to assign them to a contact or project: Files, Other Contacts, Emails, Projects, vCards,...</source>
        <translation>Elemente hierherziehen und Kontakt oder Projekt zuweisen: Dateien, andere Kontakte, E-Mails, Projekte, vCards,...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste text here to assign it to the actual contact or project: Phone Numbers, Addresses, vCards, etc,</source>
        <translation>Text hier einfügen und aktuellem Kontakt oder Projekt zuweisen: Telefonnummern, Adressen, vCards, usw.,</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+6"/>
        <source>Drop Zone</source>
        <translation>Drop Zone</translation>
    </message>
    <message>
        <location line="+152"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Appointment import is not supported!</source>
        <translation>Import von Terminen wird nicht unterstützt!</translation>
    </message>
</context>
<context>
    <name>DropZone_MenuWidgetClass</name>
    <message>
        <location filename="dropzone_menuwidget.ui" line="+13"/>
        <source>DropZone</source>
        <translation>Drop Zone</translation>
    </message>
    <message>
        <source>Drop items here to assign them to a contact or project: Files, Other Contacts, Emails, Projects, vCards,...</source>
        <translation type="obsolete">Elemente hierherziehen und Kontakt oder Projekt zuweisen: Dateien, andere Kontakte, E-Mails, Projekte, vCards,...</translation>
    </message>
    <message>
        <source>Paste text here to assign it to the actual contact or project: Phone Numbers, Addresses, vCards, etc,:</source>
        <translation type="obsolete">Text hier einfügen und aktuellem Kontakt oder Projekt zuweisen: Telefonnummern, Adressen, vCards, usw.:</translation>
    </message>
</context>
<context>
    <name>EMailDialog</name>
    <message>
        <location filename="emaildialog.cpp" line="+87"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Attach File</source>
        <translation type="obsolete">Datei anhängen</translation>
    </message>
    <message>
        <source>Remove Selected Attachments</source>
        <translation type="obsolete">Ausgewählte Anhänge löschen</translation>
    </message>
    <message>
        <source>Save Attachment As</source>
        <translation type="obsolete">Anhang speichern unter</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Save &amp;&amp; Send</source>
        <translation>Speichern &amp;&amp; Senden</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Email Message</source>
        <translation>E-Mail-Nachricht</translation>
    </message>
    <message>
        <location line="+252"/>
        <location line="+125"/>
        <location line="+8"/>
        <location line="+29"/>
        <location line="+237"/>
        <location line="+62"/>
        <location line="+461"/>
        <location line="+95"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+95"/>
        <source>Template Email can not be scheduled!</source>
        <translation>E-Mail-Vorlage kann nicht geplant werden!</translation>
    </message>
    <message>
        <location line="-401"/>
        <source>Message Subject!</source>
        <translation>Betreff!</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Please enter message subject</source>
        <translation>Bitte geben Sie einen Betreff ein</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>(Subject)</source>
        <translation>(Betreff)</translation>
    </message>
    <message>
        <location line="-217"/>
        <source>The contents of this email is not yet actual, because the email is still being written or cancelled</source>
        <translation>Der Inhalt dieser Nachricht könnte nicht aktuell sein, da sie noch bearbeitet wird oder eventuell abgebrochen wurde</translation>
    </message>
    <message>
        <location line="-240"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+356"/>
        <source>Template Name is mandatory!</source>
        <translation>Vorlagenname ist zwingend!</translation>
    </message>
    <message>
        <source>Sending message failed</source>
        <translation type="obsolete">Nachricht konnte nicht versendet werden</translation>
    </message>
    <message>
        <source>Sending message failed. Do you wish to save email and exit without sending?</source>
        <translation type="obsolete">Die Nachricht konnte nicht vesendet werden. Wollen Sie sie speichern ohne sie zu versenden?</translation>
    </message>
    <message>
        <location line="-54"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <source>Choose application</source>
        <translation type="obsolete">Applikation wählen</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation type="obsolete">Auswahl Datei</translation>
    </message>
    <message>
        <location line="-760"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location line="+1685"/>
        <source>New Template</source>
        <translation>Neue Vorlage</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Failed to zip document </source>
        <translation type="obsolete">Packen (zippen) des Dokuments misslungen </translation>
    </message>
    <message>
        <location line="-1408"/>
        <source>The following contacts have no email address:</source>
        <translation>Folgende Kontakte haben keine E-Mail-Adresse:</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Continue sending the serial email or abort?</source>
        <translation>Mit Aussand weiterfahren oder abbrechen?</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Continue</source>
        <translation>Weiterfahren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>You need to define SMTP host in settings!</source>
        <translation>In den Einstellungen muss ein SMTP Hostname definiert werden!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error sending the mail!</source>
        <translation>Fehler beim E-Mail-Versand!</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>There were errors when sending the serial email to the following addresses:</source>
        <translation>Beim Versand des E-Mails sind Fehler bei folgenden Adressen aufgetreten:</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>All other emails have been sent successfully.</source>
        <translation>Alle anderen E-Mails wurden erfolgreich versendet.</translation>
    </message>
    <message>
        <source>Record is already locked by you, do you wish to discard old lock and to continue editing record?&apos;</source>
        <translation type="obsolete">Der Datensatz wird bereits von Ihnen gesperrt. Wollen Sie die bestehende Sperrung verwerfen und mit der Bearbeitung neu beginnen?</translation>
    </message>
    <message>
        <location line="+307"/>
        <source>This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?</source>
        <translation>Der Datensatz wird bereits von Ihnen gesperrt. Wollen Sie die bestehende Sperrung verwerfen und mit der Bearbeitung neu beginnen?</translation>
    </message>
</context>
<context>
    <name>EMailDialogClass</name>
    <message>
        <location filename="emaildialog.ui" line="+14"/>
        <location line="+103"/>
        <location line="+279"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Add Contacts</source>
        <translation>Kontakte hinzufügen</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>Subject:</source>
        <translation>Betreff:</translation>
    </message>
    <message>
        <source>Sent/Received Date:</source>
        <translation type="obsolete">Gesendet/Erhalten:</translation>
    </message>
    <message>
        <source>Is Private</source>
        <translation type="obsolete">Privat</translation>
    </message>
    <message>
        <location line="-258"/>
        <source>Project:</source>
        <translation>Projekt:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>Add Attachment</source>
        <translation>Anhang hinzufügen</translation>
    </message>
    <message>
        <location line="+225"/>
        <source>Is Template</source>
        <translation>Vorlage</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="-118"/>
        <source>Category:</source>
        <translation>Kategorie:</translation>
    </message>
    <message>
        <location line="-59"/>
        <source>Convert to Plain Text</source>
        <translation>Nur-Text-Format verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;html&gt;&lt;body style=&quot;margin: 0px; color: rgb(255, 0, 0);&quot;&gt;Non-Outlook users might not receive your attachment(s) correctly unless the message body is set to plain text!&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;body style=&quot;margin: 0px; color: rgb(255, 0, 0);&quot;&gt;Empfänger ohne Outlook können Ihre Anhänge u.U. nicht korrekt empfangen, wenn Sie nicht das Nur-Text-Format verwenden!&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Use Direct Mail</source>
        <translation>E-Mail-Client direkt senden</translation>
    </message>
    <message>
        <location line="-181"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>Fix HTML</source>
        <translation>HTML fixieren</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Plan Task</source>
        <translation>Aufgabe planen</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Template</source>
        <translation>Vorlage</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Use External Mail (Outlook®)</source>
        <translation type="unfinished">Externen E-Mail-Client verwenden (Outlook®)</translation>
    </message>
</context>
<context>
    <name>EmailSelectorDialog</name>
    <message>
        <source>Email</source>
        <translation type="obsolete">E-Mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Art</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Beschreibung</translation>
    </message>
</context>
<context>
    <name>EmailSelectorDialogClass</name>
    <message>
        <source>Email Selector</source>
        <translation type="obsolete">Auswahl E-Mail</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Abbrechen</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
</context>
<context>
    <name>Email_MenuWidget</name>
    <message>
        <location filename="email_menuwidget.cpp" line="+94"/>
        <source>Modify/Create</source>
        <translation>Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Send Email From Template</source>
        <translation>E-Mail aus Vorlage erzeugen</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+41"/>
        <source>New Task From Template</source>
        <translation>Neue Aufgabe aus Vorlage</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Send Email Using Template</source>
        <translation>E-Mail aus Vorlage erzeugen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Add Template</source>
        <translation>Vorlage hinzufügen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Modify Template</source>
        <translation>Vorlage bearbeiten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete Template</source>
        <translation>Vorlage löschen</translation>
    </message>
    <message>
        <location line="+90"/>
        <location line="+9"/>
        <source>Create Template</source>
        <translation>Vorlage erzeugen</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Create Template(s) or Register Email(s)</source>
        <translation>Vorlage(n) erzeugen oder E-Mail(s) registrieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>New Template(s) From Dropped Email(s)</source>
        <translation>Neue Vorlage(n) aus E-Mail(s)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Register Email(s)</source>
        <translation>E-Mail(s) registrieren</translation>
    </message>
    <message>
        <location line="+156"/>
        <location line="+59"/>
        <location line="+22"/>
        <location line="+9"/>
        <location line="+213"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-303"/>
        <location line="+59"/>
        <location line="+22"/>
        <location line="+222"/>
        <source>Please first select an template!</source>
        <translation>Vorlage zuerst auswählen!</translation>
    </message>
    <message>
        <location line="-213"/>
        <source>Do you really want to delete this record permanently from the database?</source>
        <translation>Wollen Sie diesen Datensatz endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="-237"/>
        <location line="+237"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-237"/>
        <location line="+237"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Send Email </source>
        <translation type="obsolete">E-Mail senden </translation>
    </message>
    <message>
        <location line="-465"/>
        <location line="+9"/>
        <source>Email </source>
        <translation>E-Mail </translation>
    </message>
    <message>
        <location line="+740"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Email Manager</source>
        <translation>E-Mail-Manager</translation>
    </message>
    <message>
        <location line="-546"/>
        <source>Do you want to replace the existing template(s) (pressing No adds a new template)?</source>
        <translation>Wollen Sie die bestehende(n) Vorlage(n) ersetzen (Nein fügt eine neue Vorlage hinzu)?</translation>
    </message>
</context>
<context>
    <name>Email_MenuWidgetClass</name>
    <message>
        <location filename="email_menuwidget.ui" line="+13"/>
        <source>Email Menu</source>
        <translation>Menü E-Mails</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Templates</source>
        <translation>Vorlagen</translation>
    </message>
    <message>
        <location line="+143"/>
        <source>Register</source>
        <translation>Registrieren</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Register Email Message</source>
        <translation>E-Mail-Nachricht registrieren</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>FUI_BusCostCenters</name>
    <message>
        <location filename="fui_buscostcenters.cpp" line="+113"/>
        <source>Cost Center: </source>
        <translation>Kostenstelle:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cost Center</source>
        <translation>Kostenstelle</translation>
    </message>
</context>
<context>
    <name>FUI_BusCostCentersClass</name>
    <message>
        <location filename="fui_buscostcenters.ui" line="+13"/>
        <source>Cost Centers</source>
        <translation>Kostenstellen</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
</context>
<context>
    <name>FUI_BusDepartments</name>
    <message>
        <location filename="fui_busdepartments.cpp" line="+191"/>
        <source>Department: </source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Department</source>
        <translation>Abteilung</translation>
    </message>
</context>
<context>
    <name>FUI_BusOrganizations</name>
    <message>
        <location filename="fui_busorganizations.cpp" line="+267"/>
        <source>Organization: </source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="-141"/>
        <location line="+28"/>
        <source>You have reached the limit to the number of allowed organizations: </source>
        <translation>Maximale Anzahl zulässiger Abteilungen erreicht:</translation>
    </message>
    <message>
        <location line="-28"/>
        <location line="+28"/>
        <source>!
Can not insert new organization!</source>
        <translation>!
Neue Organisation kann nicht erfasst werden!</translation>
    </message>
</context>
<context>
    <name>FUI_BusOrganizationsClass</name>
    <message>
        <location filename="fui_busorganizations.ui" line="+13"/>
        <source>Organizations</source>
        <translation>Organisationen</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Edi&amp;t</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Departments</source>
        <translation>Abteilungen</translation>
    </message>
    <message>
        <location line="+65"/>
        <location line="+77"/>
        <location line="+77"/>
        <source>Open Structure in New Window</source>
        <translation>Struktur in neuem Fenster öffnen</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Cost Centers</source>
        <translation>Kostenstellen</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Persons</source>
        <translation>Personen</translation>
    </message>
</context>
<context>
    <name>FUI_BusPerson</name>
    <message>
        <location filename="fui_busperson.cpp" line="+322"/>
        <source>New password set</source>
        <translation>Neues Passwort gesetzt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password: </source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password not match!</source>
        <translation>Passwort stimmt nicht überein!</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>User: </source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+54"/>
        <source>You have reached the limit to the number of allowed users!
Can not insert new user!</source>
        <translation>Maximale Anzahl zulässiger Benutzer erreicht:
Kein weiterer Benutzer kann mehr erfasst werden!</translation>
    </message>
    <message>
        <location line="-229"/>
        <location line="+19"/>
        <location line="+138"/>
        <location line="+30"/>
        <location line="+11"/>
        <location line="+13"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-192"/>
        <source>You must set a password when you change the username!</source>
        <translation>Bei einem Wechsel des Benutzernamens muss ein Passwort eingegeben werden!</translation>
    </message>
    <message>
        <location line="-19"/>
        <source>User role must be selected!</source>
        <translation>Benutzerrolle auswählen!</translation>
    </message>
    <message>
        <location line="+157"/>
        <location line="+30"/>
        <location line="+11"/>
        <location line="+13"/>
        <source>You do not have sufficient access rights to perform this operation!</source>
        <translation>Kein Zugriffsrecht!</translation>
    </message>
    <message>
        <location line="+261"/>
        <source>(none)</source>
        <translation>(none)</translation>
    </message>
</context>
<context>
    <name>FUI_BusPersonClass</name>
    <message>
        <source>Test2</source>
        <translation type="obsolete">Test2</translation>
    </message>
    <message>
        <location filename="fui_busperson.ui" line="+206"/>
        <source>Department:</source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Ms.</source>
        <translation>Fr.</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>Entered:</source>
        <translation>Eintritt:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Last Name:</source>
        <translation>Nachname:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Initials:</source>
        <translation>Initialen:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Pers. No.:</source>
        <translation>Personalnr.:</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Left:</source>
        <translation>Austritt:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Mr.</source>
        <translation>Hr.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Direct Phone:</source>
        <translation>Tel. direkt:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Contact:</source>
        <translation>Kontakt:</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Login Account</source>
        <translation>Login-Konto</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Has Login Account</source>
        <translation>Login-Konto</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Confirm Password:</source>
        <translation>Passwort bestätigen:</translation>
    </message>
    <message>
        <location line="-67"/>
        <source>Switch To Account Details</source>
        <translation>Wechseln zu Kontendetails</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Generate Password</source>
        <translation>Passwort generieren</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>Personal</source>
        <translation>Persönlich</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Birth Day:</source>
        <translation>Geburtstag:</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Insurance No:</source>
        <translation>Versicherungsnummer:</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Occupation</source>
        <translation>Beschäftigung</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Occupation (%):</source>
        <translation>Beschäftigungsgrad (%):</translation>
    </message>
    <message>
        <location line="-579"/>
        <source>Role:</source>
        <translation>Rolle:</translation>
    </message>
    <message>
        <location line="-386"/>
        <source>Show All Users</source>
        <translation>Zeige alle Benutzer</translation>
    </message>
    <message>
        <location line="+691"/>
        <source>Modify Personal Settings</source>
        <translation>Persönliche Einstellungen bearbeiten</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Stored Project List</source>
        <translation>Gespeicherte Projektlisten</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Default Project List:</source>
        <translation>Standard-Projekstliste:</translation>
    </message>
    <message>
        <source>Reservations</source>
        <translation type="obsolete">Reservierungen</translation>
    </message>
</context>
<context>
    <name>FUI_CE_Types</name>
    <message>
        <location filename="fui_ce_types.cpp" line="+39"/>
        <source>Document types</source>
        <translation>Dokumentenarten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Email types</source>
        <translation>Email-Typen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Modify Icon</source>
        <translation>Icon bearbeiten</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>Communication Categories: </source>
        <translation>Kommunikationskategorien:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Communication Categories</source>
        <translation>Kommunikationskategorien</translation>
    </message>
    <message>
        <location line="-132"/>
        <source>Phone Call types</source>
        <translation>Anruf-Typen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Calendar Event types</source>
        <translation>Kalendereintrag-Typen</translation>
    </message>
</context>
<context>
    <name>FUI_CE_TypesClass</name>
    <message>
        <source>FUI_CE_Types</source>
        <translation type="obsolete">FUI_CE_Types</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="obsolete">Beschreibung:</translation>
    </message>
    <message>
        <location filename="fui_ce_types.ui" line="+89"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="-19"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Mapped Name:</source>
        <translation>Namensabbildung:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Icon:</source>
        <translation>Icon:</translation>
    </message>
</context>
<context>
    <name>FUI_CalendarEvent</name>
    <message>
        <location filename="fui_calendarevent.cpp" line="+42"/>
        <source>Calendar Event</source>
        <translation>Kalendereintrag</translation>
    </message>
    <message>
        <location line="+267"/>
        <location line="+52"/>
        <source>Show Wizard</source>
        <translation>Assistenten zeigen</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Wizard Mode On/Off</source>
        <translation>Assistenzmodus Ein/Aus</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Store &amp;&amp; Close</source>
        <translation>Speichern &amp;&amp; Schliessen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Wizard Mode</source>
        <translation>Assistenzmodus</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+22"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Next Step</source>
        <translation type="obsolete">Weiter</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>STEPS:</source>
        <translation>Schritte:</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+121"/>
        <location line="+23"/>
        <source>Basic</source>
        <translation>Basis</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Reminder</source>
        <translation>Erinnerung</translation>
    </message>
    <message>
        <source>User &amp; Contacts</source>
        <translation type="obsolete">Kontakte &amp; Benutzer</translation>
    </message>
    <message>
        <location line="-209"/>
        <source>Continue...</source>
        <translation>Weiter...</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Contacts</source>
        <comment>nabijem</comment>
        <translation>Kontakte</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+124"/>
        <location line="+40"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="-163"/>
        <location line="+125"/>
        <location line="+45"/>
        <source>Resources</source>
        <translation>Ressourcen</translation>
    </message>
    <message>
        <location line="-169"/>
        <location line="+126"/>
        <location line="+50"/>
        <source>Breaks</source>
        <translation>Pausen</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+77"/>
        <source>Recurrence</source>
        <translation>Terminserie</translation>
    </message>
    <message>
        <location line="-259"/>
        <location line="+128"/>
        <location line="+61"/>
        <location line="+78"/>
        <source>Task</source>
        <translation>Aufgabe</translation>
    </message>
    <message>
        <location line="-266"/>
        <location line="+129"/>
        <location line="+66"/>
        <source>Multipart</source>
        <translation>Mehrteiler</translation>
    </message>
    <message>
        <location line="+53"/>
        <location line="+234"/>
        <source>Add Calendar Part</source>
        <translation>Kalendereintrags-Teil hinzufügen</translation>
    </message>
    <message>
        <location line="-107"/>
        <location line="+55"/>
        <source>Part </source>
        <translation>Teil</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Do you really want to delete part %1?</source>
        <translation>Wollen Sie wirklich Teil %1 löschen?</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+10"/>
        <location line="+196"/>
        <location line="+884"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-1090"/>
        <location line="+206"/>
        <location line="+101"/>
        <location line="+783"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-1090"/>
        <location line="+206"/>
        <location line="+101"/>
        <location line="+783"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-1080"/>
        <source>Last Calendar Part can not be deleted</source>
        <translation>Ein einzelner Teil eines Kalendereintrags darf nicht gelöscht werden</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Delete Calendar Part</source>
        <translation>Eintrags-Teil löschen</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Owner</source>
        <translation>Besitzer</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>Event From and To Dates are not properly set!</source>
        <translation>Von-/Bis-Daten des Kalendereintrags sind nicht korrekt gesetzt!</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Part %1, Option %2, From and To Dates are not properly set!</source>
        <translation>Teil %1, Option %2: Von- und Bis-Daten sind nicht korrekt gesetzt!</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>There are event parts lying outside the main event time period. Should the main time period be adjusted automatically?</source>
        <translation>Teile des Kalendereintrags liegen ausserhalb der Dauer des Gesamteintrags. Soll die Periode des Haupteintrags automatisch angepasst werden? </translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Part %1,Option %2, Break row %3 From and To Dates are out of range of option!</source>
        <translation>Teil %1, Option %2, Pause Zeile %3: Von- und Bis-Daten sind nicht korrekt gesetzt!</translation>
    </message>
    <message>
        <location line="+410"/>
        <location line="+4"/>
        <location line="+2"/>
        <location line="+149"/>
        <location line="+2"/>
        <location line="+2"/>
        <source>assigned</source>
        <translation>zugewiesen</translation>
    </message>
    <message>
        <location line="+191"/>
        <source>Do you really want to discard all data and create calendar event from template?</source>
        <translation>Wollen Sie wirklich alle Daten verwerfen und einen neuen Eintrag aus einer Vorlage erstellen?</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Please Confirm Sending Invitations:</source>
        <translation>Bestätigen Sie, dass Einladungen verschickt werden sollen:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Send all unsent invitations</source>
        <translation>Alle noch offenen Einladungen versenden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Return to Edit Calendar Data</source>
        <translation>Zurück zur Bearbeitung des Kalendereintrages</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Accept</source>
        <translation>Annehmen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reject</source>
        <translation>Ablehnen</translation>
    </message>
    <message>
        <location line="-1545"/>
        <location line="+35"/>
        <source>Contacts</source>
        <translation>Kontakte</translation>
    </message>
    <message>
        <location line="+455"/>
        <source>This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?</source>
        <translation>Der Datensatz wird bereits von Ihnen gesperrt. Wollen Sie die bestehende Sperrung verwerfen und mit der Bearbeitung neu beginnen?</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Date To must be at least five minutes bigger than Date From!</source>
        <translation>Das Ende muss mindestens fünf Minuten nach dem Startzeitpunkt liegen!</translation>
    </message>
</context>
<context>
    <name>FUI_CalendarEventClass</name>
    <message>
        <location filename="fui_calendarevent.ui" line="+14"/>
        <source>FUI_CalendarEvent</source>
        <translation>FUI_CalendarEvent</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Calendar Event</source>
        <translation>Kalendereintrag</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Is Template</source>
        <translation>Vorlage</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Dates</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Date Modified:</source>
        <translation>Letzte Änderung:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Date Created:</source>
        <translation>Erstellt am:</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information Only</source>
        <translation>Nur informativ</translation>
    </message>
</context>
<context>
    <name>FUI_CalendarViewEditor</name>
    <message>
        <location filename="fui_calendarvieweditor.cpp" line="+31"/>
        <source>Calendar Views</source>
        <translation>Kalender-Ansichten</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>3 Days</source>
        <translation>3 Tage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Business Days</source>
        <translation>Arbeitswoche</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Two Weeks</source>
        <translation>Zwei Wochen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+260"/>
        <source>No views selected!</source>
        <translation>Keine Ansicht ausgewählt!</translation>
    </message>
    <message>
        <location line="-242"/>
        <location line="+68"/>
        <location line="+409"/>
        <location line="+544"/>
        <source>You must show at least one calendar in the view!</source>
        <translation>Sie müssen zumindest einen Kalender in der Ansicht anzeigen lassen!</translation>
    </message>
    <message>
        <location line="-969"/>
        <source>View name must not be empty!</source>
        <translation>Der Name der Ansicht darf nicht leer sein!</translation>
    </message>
    <message>
        <location line="+432"/>
        <source>Do you want to create a new view for each selected item?</source>
        <translation>Wollen Sie eine neue Ansicht für jeden gewählten Eintrag erzeugen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+353"/>
        <source>Operation In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Syntax error: line does not contain the function name!</source>
        <translation>Fehler: Die Zeile enthält keinen Funktionsnamen!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Syntax error: unknown function name: %1!</source>
        <translation>Fehler: Unbekannter Funktionsname: %1!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Syntax error: parameter list not closed in line: %1!</source>
        <translation>Fehler: Parameterliste nicht geschlossen in Zeile: %1!</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+13"/>
        <location line="+8"/>
        <location line="+8"/>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+13"/>
        <location line="+8"/>
        <location line="+8"/>
        <source>Syntax error: Invalid function parameter count in line: %1!</source>
        <translation>Fehler: Ungültige Anzahl Parameter in Zeile: %1!</translation>
    </message>
    <message>
        <location line="-75"/>
        <location line="+37"/>
        <location line="+13"/>
        <source>Syntax error: Invalid function parameter value in line: %1!</source>
        <translation>Fehler: Ungültiger Parameterwert in Zeile: %1!</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Script error: Unsupported function: %1!</source>
        <translation>Fehler: Nicht unterstützte Funktion: %1!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Script error: %1!</source>
        <translation>Fehler: %1!</translation>
    </message>
    <message>
        <location line="+85"/>
        <location line="+22"/>
        <location line="+34"/>
        <location line="+38"/>
        <source>Calendar view %1 not found!</source>
        <translation>Kalenderansicht %1 nicht gefunden!</translation>
    </message>
    <message>
        <location line="+169"/>
        <source>Project list %1 not found!</source>
        <translation>Projektliste %1 nicht gefunden!</translation>
    </message>
</context>
<context>
    <name>FUI_CalendarViewEditorClass</name>
    <message>
        <location filename="fui_calendarvieweditor.ui" line="+24"/>
        <source>Calendar Views:</source>
        <translation>Kalenderansichten:</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Calendar View</source>
        <translation>Kalenderansicht</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Time Period:</source>
        <translation>Zeittaum:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Show Overview Calendar</source>
        <translation>Kalenderübersicht anzeigen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show Detail Calendar</source>
        <translation>Detailkalender anzeigen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Public</source>
        <translation>Öffentlich</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Write to Selected Calendar Views</source>
        <translation>In selektierte Kalenderansichten eintragen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Create New Calendar View</source>
        <translation>Neue Kalenderansicht erzeugen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete Selected Calendar Views</source>
        <translation>Selektierte Kalenderansichten löschen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Selected Calendar Views</source>
        <translation>Selektierte Kalenderansichten</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Add Projects</source>
        <translation>Projekte hinzufügen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove Projects</source>
        <translation>Projekte entfernen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add Contacts</source>
        <translation>Kontakte hinzufügen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove Contacts</source>
        <translation>Kontakte entfernen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add Resources</source>
        <translation>Ressourcen hinzufügen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove Resources</source>
        <translation>Ressourcen entfernen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove Users</source>
        <translation>Benutzer entfernen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add Users</source>
        <translation>Benutzer hinzufügen</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Selected Calendar View:</source>
        <translation>Selektierte Kalenderansicht:</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Move Up</source>
        <translation>Nach oben</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Move Down</source>
        <translation>Nach unten</translation>
    </message>
    <message>
        <location line="-71"/>
        <source>Regenerate Views</source>
        <translation>Ansichten neu erstellen</translation>
    </message>
</context>
<context>
    <name>FUI_CeEventType</name>
    <message>
        <location filename="fui_ceeventtype.cpp" line="+47"/>
        <source>Document types</source>
        <translation>Dokumentenarten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Email types</source>
        <translation>E-Mail-Typen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phone Call types</source>
        <translation>Anruf-Typen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Document Created</source>
        <translation>Dokument erzeugt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Document Opened</source>
        <translation>Dokument geöffnet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Document Sent</source>
        <translation>Dokument gesendet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Document Received</source>
        <translation>Dokument erhalten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incoming Call Answered</source>
        <translation>Eingehenden Anruf beantwortet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incoming Call Initiated</source>
        <translation>Eingehenden Anruf gestartet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call Missed</source>
        <translation>Anruf verpasst</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incoming Call Answered By Someone Else</source>
        <translation>Eingehender Anruf durch andere entgegengenommen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call Back Required</source>
        <translation>Rückruf erforderlich</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call Back Done</source>
        <translation>Rückruf erledigt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Outgoing Call No Answer</source>
        <translation>Ausgehender Anruf ohne Antwort</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Outgoing Call Partner Not Reached</source>
        <translation>Angerufenen nicht erreicht</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Email Received</source>
        <translation>E-Mail erhalten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Email Sent</source>
        <translation>E-Mail gesendet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Email Forwarded</source>
        <translation>E-Mail weitergeleitet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Email Answered</source>
        <translation>E-Mail beantwortet</translation>
    </message>
    <message>
        <location line="+124"/>
        <source>Communication Event Types: </source>
        <translation>Kommunikationsereignis-Typen:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Communication Event Types</source>
        <translation>Kommunikationsereignis-Typen</translation>
    </message>
</context>
<context>
    <name>FUI_CeEventTypeClass</name>
    <message>
        <source>FUI_CeEventType</source>
        <translation type="obsolete">FUI_CeEventType</translation>
    </message>
    <message>
        <location filename="fui_ceeventtype.ui" line="+150"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Priority:</source>
        <translation>Priorität:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>High</source>
        <translation>Hoch</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Medium</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Low</source>
        <translation>Niedrig</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Generate Default Events Types</source>
        <translation>Default-Ereignistypen generieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Direction:</source>
        <translation>Richtung:</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Outgoing</source>
        <translation>Ausgehend</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Incoming</source>
        <translation>Eingehend</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>System Event:</source>
        <translation>Systemereignis:</translation>
    </message>
</context>
<context>
    <name>FUI_CommunicationCenter</name>
    <message>
        <location filename="fui_communicationcenter.cpp" line="+22"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
</context>
<context>
    <name>FUI_CommunicationCenterClass</name>
    <message>
        <source>FUI_CommunicationCenter</source>
        <translation type="obsolete">FUI_CommunicationCenter</translation>
    </message>
</context>
<context>
    <name>FUI_ContactType</name>
    <message>
        <location filename="fui_contacttype.cpp" line="+100"/>
        <source>Types: </source>
        <translation>Arten:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Types</source>
        <translation>Arten</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+11"/>
        <location line="+12"/>
        <location line="+34"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-57"/>
        <location line="+11"/>
        <location line="+12"/>
        <source>You do not have sufficient access rights to perform this operation!</source>
        <translation>Der Datensatz wird bereits von Ihnen gesperrt. Wollen Sie die bestehende Sperrung verwerfen und mit der Bearbeitung neu beginnen?</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?</source>
        <translation>Der Datensatz wird bereits von Ihnen gesperrt. Wollen Sie die bestehende Sperrung verwerfen und mit der Bearbeitung neu beginnen?</translation>
    </message>
</context>
<context>
    <name>FUI_ContactTypeClass</name>
    <message>
        <source>FUI_ContactType</source>
        <translation type="obsolete">FUI_ContactType</translation>
    </message>
</context>
<context>
    <name>FUI_Contacts</name>
    <message>
        <location filename="fui_contacts.cpp" line="+257"/>
        <source>Assigned Projects</source>
        <translation>Zugewiesene Projekte</translation>
    </message>
    <message>
        <location line="+480"/>
        <location line="+113"/>
        <source>!
Can not insert new contact!</source>
        <translation>Neuer Kontakt kann nicht eingefügt werden!</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="+721"/>
        <location line="+7"/>
        <location line="+34"/>
        <location line="+12"/>
        <location line="+54"/>
        <location line="+41"/>
        <location line="+282"/>
        <location line="+16"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-672"/>
        <location line="+977"/>
        <location line="+193"/>
        <location line="+170"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-1524"/>
        <source>Address(es):</source>
        <translation>Adresse(n):</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Internet Address(es):</source>
        <translation>Internet-Adresse(n):</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Phone(s):</source>
        <translation>Telefonnummern:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Can not exit Contact Window while editing group tree!</source>
        <translation>Kontaktfenster kann nicht geschlossen werden solange der Gruppenbaum noch bearbeitet wird!</translation>
    </message>
    <message>
        <location line="-1109"/>
        <source>Assigned Users And Contacts</source>
        <translation>Zugewiesene Benutzer und Kontakte</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tree</source>
        <translation>Baum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>From</source>
        <translation>Von</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>To</source>
        <translation>Bis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact Groups</source>
        <translation>Kontaktgruppen</translation>
    </message>
    <message>
        <location line="+464"/>
        <location line="+113"/>
        <source>You have reached the limit to the number of allowed contacts: </source>
        <translation>Maximale Anzahl zulässiger Kontakte erreicht:</translation>
    </message>
    <message>
        <location line="+359"/>
        <source>Email Address(es):</source>
        <translation>E-Mail-Adresse(n):</translation>
    </message>
    <message>
        <location line="+301"/>
        <source>Projects (</source>
        <translation>Projekte (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Contacts (</source>
        <translation>Kontakte (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Contacts</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source> And </source>
        <translation> und </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Users (</source>
        <translation>Benutzer (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Users</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location line="+2"/>
        <source> Documents (</source>
        <translation> Dokumente (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source> E-mails (</source>
        <translation> E-Mails (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source> Voice Calls (</source>
        <translation> Anrufe (</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Journals (</source>
        <translation>Journaleinträge (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Journal</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location line="+460"/>
        <source>There are no selected rows in the list!</source>
        <translation>Keine ausgewählten Zeilen in der Liste!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save as...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Text file (*.txt)</source>
        <translation>Textdatei (*.txt)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failed to open destination file!</source>
        <translation>Zieldatei kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <source>Contact can not be modified by other users!</source>
        <translation type="obsolete">Der Kontakt darf nicht durch andere Benutzer geändert werden!</translation>
    </message>
    <message>
        <source>Contact can not be viewed by other users!</source>
        <translation type="obsolete">Der Kontakt darf nicht durch andere Benutzer sichtbar sein!</translation>
    </message>
    <message>
        <source>Invalid Debtor Code, can not resolve next Debtor Code!</source>
        <translation type="obsolete">Ungültige Debitorennummer: Nächster Vorschlag kann nicht bestimmt werden!</translation>
    </message>
    <message>
        <location line="-393"/>
        <source>Error while fetching saved data. Please refresh contact data!</source>
        <translation>Fehler beim Laden gespeicherter Daten. Bitte aktualisieren Sie Ihre Ansicht der Kontaktdaten!</translation>
    </message>
    <message>
        <location line="-776"/>
        <source>Contact: </source>
        <translation>Kontakt:</translation>
    </message>
    <message>
        <location line="+1836"/>
        <source>Contact already exists in database:</source>
        <translation>Der Kontakt besteht schon in der Datenbank:</translation>
    </message>
    <message>
        <location line="-1681"/>
        <location line="+1512"/>
        <location line="+170"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-1682"/>
        <location line="+1512"/>
        <location line="+170"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-2382"/>
        <source>Copy Internet Address</source>
        <translation>Internet-Adresse in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Address</source>
        <translation>Adresse in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Email</source>
        <translation>E-Mail in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Phone</source>
        <translation>Telefonnummer in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Use Entry Assistant</source>
        <translation>Eingabeassistenten verwenden</translation>
    </message>
    <message>
        <location line="+1998"/>
        <source>Please select contact(s) for serial email</source>
        <translation>Bitte selektieren Sie die Kontakte für das Serien-E-Mail</translation>
    </message>
    <message>
        <location line="-1319"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Shall the new organization name automatically be changed in all assigned persons, too?</source>
        <translation>Wollen Sie den neuen Organisationsnamen automatisch allen zugewiesenen Personen zuweisen lassen?</translation>
    </message>
    <message>
        <location line="+1512"/>
        <source>You changed the address of %1. Do you want this change to be made automatically to %2 addresses of people in this organization?</source>
        <translation>Die Adresse von %1 wurde geändert. Wollen Sie diese Änderung auch automatisch in allen passenden Adressen (%2) von Personen dieser Organisation nachtragen?</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>User aborted!</source>
        <translation>Abbruch durch Benutzer!</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Operation Aborted</source>
        <translation>Ausführung abgebrochen</translation>
    </message>
    <message>
        <location line="-2358"/>
        <source>Contact Calendar Entries</source>
        <translation>Einträge in Kontaktkalender</translation>
    </message>
</context>
<context>
    <name>FUI_ContactsClass</name>
    <message>
        <source>FUI_Contacts</source>
        <translation type="obsolete">FUI_Contacts</translation>
    </message>
    <message>
        <source>Communication With Contact</source>
        <translation type="obsolete">Kommunikation</translation>
    </message>
    <message>
        <source>Contact:</source>
        <translation type="obsolete">Kontakt:</translation>
    </message>
    <message>
        <location filename="fui_contacts.ui" line="+2608"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="-2448"/>
        <source>Show Quick Info</source>
        <translation>Überblick anzeigen</translation>
    </message>
    <message>
        <location line="+154"/>
        <source>1 Email Address(es):</source>
        <translation>1 E-Mail-Adresse(n):</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>0 Address(es):</source>
        <translation>0 Adresse(n):</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>0 Internet Address(es):</source>
        <translation>0 Internet-Adresse(n):</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location line="-187"/>
        <source>3 Phone(s):</source>
        <translation>3 Telefonnummer(n):</translation>
    </message>
    <message>
        <source>Persons And Contacts</source>
        <translation type="obsolete">Personen und Kontakte</translation>
    </message>
    <message>
        <location line="+310"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Journal</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Journal Entry:</source>
        <translation>Journaleintrag:</translation>
    </message>
    <message>
        <location line="+136"/>
        <source>Person</source>
        <translation>Person</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Middle Name:</source>
        <translation>Zweiter Vorname:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Nick Name:</source>
        <translation>Rufname:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Ms.</source>
        <translation>Fr.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Last Name:</source>
        <translation>Nachname:</translation>
    </message>
    <message>
        <location line="+74"/>
        <location line="+137"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="-115"/>
        <source>Mr.</source>
        <translation>Hr.</translation>
    </message>
    <message>
        <source>Synchronize names</source>
        <translation type="obsolete">Namen synchronisieren</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Foundation Day:</source>
        <translation>Gründungsdatum:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Organization Short Name:</source>
        <translation>Kurzbezeichnung Organisation:</translation>
    </message>
    <message>
        <location line="+153"/>
        <source>Location:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Phones</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Formatted Address:</source>
        <translation>Formatierte Adresse:</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>eMail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location line="+365"/>
        <source>External Code:</source>
        <translation>Externer Code:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Ancient Last Name:</source>
        <translation>Früherer Nachname:</translation>
    </message>
    <message>
        <source>Access Rights</source>
        <translation type="obsolete">Zugriffsrechte</translation>
    </message>
    <message>
        <source>Private Contact</source>
        <translation type="obsolete">Privatkontakt</translation>
    </message>
    <message>
        <location line="-170"/>
        <source>Department:</source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Function:</source>
        <translation>Funktion:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Profession:</source>
        <translation>Beruf:</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Birthday:</source>
        <translation>Geburtstag:</translation>
    </message>
    <message>
        <location line="+291"/>
        <source>Contact Owner:</source>
        <translation>Besitzer:</translation>
    </message>
    <message>
        <location line="-396"/>
        <source>Supress Mailing</source>
        <translation>In Mailings unterdrücken</translation>
    </message>
    <message>
        <location line="+463"/>
        <source>Groups</source>
        <translation>Gruppen</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Pictures</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location line="+326"/>
        <source>Group List</source>
        <translation>Gruppenliste</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+133"/>
        <source>View:</source>
        <translation>Ansicht:</translation>
    </message>
    <message>
        <location line="-97"/>
        <location line="+151"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location line="-124"/>
        <source>Show Only Active</source>
        <translation>Nur Aktive zeigen</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Detailed Actual Contact List</source>
        <translation>Detaillierte aktuelle Kontaktliste</translation>
    </message>
    <message>
        <location line="-2568"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="+407"/>
        <source>Documents &amp;&amp; Assignments</source>
        <translation>Dokumente und Beziehungen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Users And Contacts</source>
        <translation>Benutzer und Kontakte</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Detail Data</source>
        <translation>Details</translation>
    </message>
    <message>
        <location line="+404"/>
        <location line="+167"/>
        <source>Synchronize Names</source>
        <translation>Namen synchronisieren</translation>
    </message>
    <message>
        <location line="+880"/>
        <source>Change Period In Which Contact Is Active In Group</source>
        <translation>Gültigkeitsperiode der Gruppenzuweisung bearbeiten</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Debtor</source>
        <translation>Debitor</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Debtor Account:</source>
        <translation>Debitorenkonto:</translation>
    </message>
    <message>
        <source>Invoice Text:</source>
        <translation type="obsolete">Fakturatext:</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location line="-157"/>
        <source>Debtor Code:</source>
        <translation>Debitorencode:</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Customer Code:</source>
        <translation>Kundencode:</translation>
    </message>
    <message>
        <location line="+573"/>
        <source>Export Serial Letters</source>
        <translation>Serienbriefexport</translation>
    </message>
    <message>
        <location line="-514"/>
        <source>Propose</source>
        <translation>Vorschlag</translation>
    </message>
    <message>
        <location line="-612"/>
        <source>Do not Synchronize Emails</source>
        <translation>E-Mails nicht synchronisieren</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Mark Synchronized Emails as Private</source>
        <translation>Synchronisierte E-Mails als &quot;privat&quot; markieren</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Outgoing Email are Private</source>
        <translation>Ausgehende E-Mails sind privat</translation>
    </message>
    <message>
        <location line="+1066"/>
        <source>Send Serial Email</source>
        <translation>Serien-E-Mail versenden</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <source>Private</source>
        <translation type="obsolete">Privat</translation>
    </message>
    <message>
        <source>Others can View</source>
        <translation type="obsolete">Für andere sichtbar</translation>
    </message>
    <message>
        <source>Others can Write</source>
        <translation type="obsolete">Von anderen modifizierbar</translation>
    </message>
    <message>
        <location line="-612"/>
        <source>Reservations</source>
        <translation>Reservierungen</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Internal Debtor</source>
        <translation>Interner Debitor</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Payment Period:</source>
        <translation>Zahlfrist (in Tagen):</translation>
    </message>
    <message>
        <location line="-520"/>
        <source>Record Recorded On:</source>
        <translation>Erfasst am:</translation>
    </message>
    <message>
        <location line="+637"/>
        <source>Custom Fields</source>
        <translation>Benutzerdefinierte Felder</translation>
    </message>
</context>
<context>
    <name>FUI_CoreUser</name>
    <message>
        <location filename="fui_coreuser.cpp" line="+126"/>
        <location line="+55"/>
        <source>Built-in account</source>
        <translation>Systemkonto</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>This is built-in account, you can not modify all settings!</source>
        <translation>Dies ist ein nicht modifizierbares Systemkonto!</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>You can not delete built-in account!</source>
        <translation>Ein Systemkonto kann nicht gelöscht werden!</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>New password set</source>
        <translation>Neues Passwort gesetzt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password: </source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password not match!</source>
        <translation>Passwort stimmt nicht überein!</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Login Account: </source>
        <translation>Login-Konto:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Login Account</source>
        <translation>Login-Konto</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+26"/>
        <source>You have reached the limit to the number of allowed users!
Can not insert new user!</source>
        <translation>Maximale Anzahl zulässiger Benutzer erreicht:Kein weiterer Benutzer kann mehr erfasst werden!</translation>
    </message>
    <message>
        <location line="-172"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>You must set password when changing username!</source>
        <translation>Bei einem Wechsel des Benutzernamens muss das Passwort eingegeben werden!</translation>
    </message>
</context>
<context>
    <name>FUI_CoreUserClass</name>
    <message>
        <source>FUI_CoreUser</source>
        <translation type="obsolete">FUI_CoreUser</translation>
    </message>
    <message>
        <location filename="fui_coreuser.ui" line="+89"/>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Last Name:</source>
        <translation>Nachname:</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>System User</source>
        <translation>Systembenutzer</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Local Access Only</source>
        <translation>Nur lokaler Zugriff</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Confirm Password:</source>
        <translation>Passwort bestätigen:</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Generate</source>
        <translation>Generieren</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Is User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Switch To User Details</source>
        <translation>Zu Benutzerdetails wechseln</translation>
    </message>
</context>
<context>
    <name>FUI_CustomFields</name>
    <message>
        <location filename="fui_customfields.cpp" line="+19"/>
        <source>Contacts</source>
        <translation>Kontakte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Boolean</source>
        <translation>Logisch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Integer</source>
        <translation>Ganzzahl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Float</source>
        <translation>Fliesskommazahl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Character(200)</source>
        <translation>Zeichen(200)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Datetime</source>
        <translation>Zeitpunkt</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Optional</source>
        <translation>Optional</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mandatory</source>
        <translation>Pflicht</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mandatory With Radio Buttons</source>
        <translation>Zwingend aus Optionsschaltflächen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Default Value</source>
        <translation>Defaultwert</translation>
    </message>
    <message>
        <location line="+405"/>
        <source>Radio button label</source>
        <translation>Feldbezeichnung für Optionsfeld</translation>
    </message>
    <message>
        <location line="-394"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Custom Fields</source>
        <translation>Benutzerdefinierte Felder</translation>
    </message>
    <message>
        <location line="+115"/>
        <location line="+82"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="-80"/>
        <location line="+84"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>Some selection predefined values can not be converted to the specified data type. Operation aborted!</source>
        <translation>Einige vordefinierte Werte können nicht ins gewünschte Datenformat umgewandelt werden. Ausführung abgebrochen!</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>When data type is boolean then only two predefined values are allowed</source>
        <translation>Bei logischen Werten sind nur zwei vordefinierte Werte zulässig</translation>
    </message>
    <message>
        <location filename="fui_customfields.ui" line="+14"/>
        <source>FUI_CustomFields</source>
        <translation>FUI_CustomFields</translation>
    </message>
    <message>
        <source>Field is part of:</source>
        <translation type="obsolete">Feld ist Bestandteil von:</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <source>Data type:</source>
        <translation type="obsolete">Datentyp:</translation>
    </message>
    <message>
        <source>Selection type:</source>
        <translation type="obsolete">Auswahltyp:</translation>
    </message>
    <message>
        <source>Sort code:</source>
        <translation type="obsolete">Sortiercode:</translation>
    </message>
    <message>
        <source>Group:</source>
        <translation type="obsolete">Gruppe:</translation>
    </message>
    <message>
        <source>Selection predefined values:</source>
        <translation type="obsolete">Auswahl vordefinierter Werte:</translation>
    </message>
    <message>
        <location filename="fui_customfields.cpp" line="-79"/>
        <source>If selection is mandatory then exactly two selectable values must be defined for bool data type. Operation aborted!</source>
        <translation>Wenn die Auswahl Pflicht ist, dann müssen für logische Felder genau zwei Werte vorgegeben werden. Ausführung abgebrochen!</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>If selection is mandatory then selection values must be defined. Operation aborted!</source>
        <translation>Wenn die Auswahl Pflicht ist, dann müssen Werte vorgegeben werden. Ausführung abgebrochen!</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>You can not use optional selection with bool data type!</source>
        <translation>Für logische Felder können keine optionalen Auswahlen vorgegeben werden!</translation>
    </message>
    <message>
        <location line="-373"/>
        <source>Radio Button Label</source>
        <translation>Feldbezeichnung für Optionsfeld</translation>
    </message>
    <message>
        <location filename="fui_customfields.ui" line="-13"/>
        <source>Table:</source>
        <translation>Tabelle:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Data Type:</source>
        <translation>Datentyp:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Value Selection:</source>
        <translation>Auswahlwerte:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Sort Code:</source>
        <translation>Sortiercode:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Field Group:</source>
        <translation>Feldgruppe:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Predefined Values for Selection:</source>
        <translation>Vordefinierte Werte für Auswahl:</translation>
    </message>
</context>
<context>
    <name>FUI_DM_Applications</name>
    <message>
        <location filename="fui_dm_applications.cpp" line="+21"/>
        <source>Modify Application Icon</source>
        <translation>Anwendungs-Icon ändern</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>External Applications</source>
        <translation>Externe Anwendungen</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Application code is mandatory field!</source>
        <translation>Anwendungscode zwingend!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Application name is mandatory field!</source>
        <translation>Anwendungsname zwingend!</translation>
    </message>
</context>
<context>
    <name>FUI_DM_ApplicationsClass</name>
    <message>
        <source>FUI_DM_Applications</source>
        <translation type="obsolete">FUI_DM_Applications</translation>
    </message>
    <message>
        <location filename="fui_dm_applications.ui" line="+237"/>
        <source>Used for Documents of Type</source>
        <translation>Verwendet für Dokumente der Art</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Local/Internet Files</source>
        <translation>Lokale/Internet-Datei</translation>
    </message>
    <message>
        <location line="-37"/>
        <source>  (e.g. &quot;txt,prn&quot;)</source>
        <translation>  (z.B. &quot;txt,prn&quot;)</translation>
    </message>
    <message>
        <location line="-129"/>
        <source>Icon:</source>
        <translation>Icon:</translation>
    </message>
    <message>
        <location line="+233"/>
        <source>Template:</source>
        <translation>Vorlage:</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Open Parameters:</source>
        <translation>Öffnungs-Parameter:</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>File Extensions:</source>
        <translation>Dateierweiterungen:</translation>
    </message>
    <message>
        <location line="-146"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
    <message>
        <location line="+335"/>
        <source>Read-Only</source>
        <translation>Schreibgeschützt</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Template &apos;Install Application&apos;:</source>
        <translation>Vorlage &apos;Anwendung installieren&apos;:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Do Not Create a Document</source>
        <translation>Kein Dokument erzeugen</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Template &apos;Install Viewer&apos;:</source>
        <translation>Vorlage &apos;Viewer installieren&apos;:</translation>
    </message>
    <message>
        <location line="-80"/>
        <source>Use % Encoding</source>
        <translation>%-Kodierung verwenden</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Set Read-Only Flag for Copies</source>
        <translation>Schreibschutz bei Kopien setzen</translation>
    </message>
</context>
<context>
    <name>FUI_DM_Documents</name>
    <message>
        <location filename="fui_dm_documents.cpp" line="+69"/>
        <source>Create Document From Selected Template</source>
        <translation>Dokument aus gewählter Vorlage erzeugen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select Path</source>
        <translation>Pfad wählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Path</source>
        <translation>Pfad entfernen</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Assigned Contacts</source>
        <translation>Zugewiesene Kontakte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Assigned Projects</source>
        <translation>Zugewiesene Projekte</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Check In Date</source>
        <translation>Eincheck-Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Revision Size)</source>
        <translation>Revisions-Grösse)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Revision Tag</source>
        <translation>Versionsmarkierung</translation>
    </message>
    <message>
        <source>Person</source>
        <translation type="obsolete">Benutzer</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Revision Tag</source>
        <translation>Versionsmarkierung setzen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save Copy of Document Revision</source>
        <translation>Kopie der Dokumentenversion speichern</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Open Copy of Document Revision</source>
        <translation>Kopie der Dokumentenversion öffnen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Replace Current Text with Revision</source>
        <translation>Aktuellen Text ersetzen durch Revision</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete Revision</source>
        <translation>Version löschen</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Documents</source>
        <translation> Dokumente</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Document: </source>
        <translation>Dokument:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Document</source>
        <translation> Dokument</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Select Document</source>
        <translation>Auswahl Dokument </translation>
    </message>
    <message>
        <location line="+355"/>
        <source>Document name is mandatory field!</source>
        <translation>Dokumentenname zwingend!</translation>
    </message>
    <message>
        <location line="-27"/>
        <location line="+102"/>
        <source>Operation aborted!</source>
        <translation>Operation abgebrochen!</translation>
    </message>
    <message>
        <source>Classification</source>
        <translation type="obsolete">Einteilung</translation>
    </message>
    <message>
        <location line="+297"/>
        <source>Checked-Out for Modification (Locked)</source>
        <translation>Ausgecheckt zur Bearbeitung (gesperrt)</translation>
    </message>
    <message>
        <location line="-449"/>
        <location line="+527"/>
        <location line="+11"/>
        <location line="+9"/>
        <location line="+111"/>
        <location line="+6"/>
        <location line="+36"/>
        <location line="+28"/>
        <location line="+36"/>
        <location line="+104"/>
        <location line="+12"/>
        <location line="+8"/>
        <location line="+6"/>
        <location line="+5"/>
        <location line="+8"/>
        <location line="+10"/>
        <location line="+93"/>
        <location line="+406"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-889"/>
        <source>Please Check Out Document first!</source>
        <translation>Dokument zuerst auschecken!</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Document is already checked out by another user. Check in and overwrite any changes that this user made?</source>
        <translation>Das Dokument ist bereits von einem anderen Benutzer zur Bearbeitung ausgecheckt worden. Wollen Sie das Dokument trotzdem einchecken und damit alle Änderungen des anderen Benutzers verwerfen?</translation>
    </message>
    <message>
        <location line="-538"/>
        <location line="+538"/>
        <location line="+9"/>
        <location line="+111"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-658"/>
        <location line="+538"/>
        <location line="+9"/>
        <location line="+111"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-111"/>
        <source>Overwrite previously checked in document?</source>
        <translation>Zuvor eingechecktes Dokument überschreiben?</translation>
    </message>
    <message>
        <location line="+158"/>
        <source>Revision TAG</source>
        <translation>Versionsmarkierung</translation>
    </message>
    <message>
        <location line="-47"/>
        <source>Document is already checked out by another user. Check out as read only</source>
        <translation>Das Dokument ist bereits von einem anderen Benutzer zur Bearbeitung ausgecheckt worden und kann nur als Kopie zum Lesen ausgecheckt werden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Document already checked out!</source>
        <translation>Das Dokument ist bereits von einem anderen Benutzer zur Bearbeitung ausgecheckt worden!</translation>
    </message>
    <message>
        <source>Please select one revision!</source>
        <translation type="obsolete">Version auswählen!</translation>
    </message>
    <message>
        <source>Automated Temporary Storage mechanism failed to determine Document check out location. Choose path for check-out location?</source>
        <translation type="obsolete">Der Automatismus für die lokale Speicherung von Dokumenten konnte kein temporäres Verzeichnis für das Auschecken bestimmen. Verzeichnispfad auswählen?</translation>
    </message>
    <message>
        <source>Please select one template!</source>
        <translation type="obsolete">Vorlage auswählen!</translation>
    </message>
    <message>
        <location line="+216"/>
        <location line="+8"/>
        <location line="+6"/>
        <location line="+5"/>
        <location line="+8"/>
        <location line="+10"/>
        <source>Failed to load from template!</source>
        <translation>Vorlage kann nicht geladen werden!</translation>
    </message>
    <message>
        <location line="-217"/>
        <location line="+28"/>
        <location line="+36"/>
        <location line="+246"/>
        <location line="+406"/>
        <source>Please first select an revision!</source>
        <translation>Version zuerst auswählen!</translation>
    </message>
    <message>
        <location line="-1926"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+824"/>
        <source>Local File Reference</source>
        <translation>Verweis auf lokale Datei</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Paper Document Location</source>
        <translation>Standort Papier-Dokument</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Note</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Internet File</source>
        <translation>Internet-Datei</translation>
    </message>
    <message>
        <location line="+463"/>
        <source>Please first select an template!</source>
        <translation>Vorlage zuerst auswählen!</translation>
    </message>
    <message>
        <location line="-433"/>
        <source>Unlocked</source>
        <translation>Entsperrt</translation>
    </message>
    <message>
        <location line="+691"/>
        <source> Contact(s)</source>
        <translation> Kontakt(e)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source> Contact(s) (</source>
        <translation> Kontakt(e) (</translation>
    </message>
    <message>
        <location line="+20"/>
        <source> Project(s)</source>
        <translation> Projekt(e)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source> Project(s) (</source>
        <translation> Projekt(e) (</translation>
    </message>
    <message>
        <source>Some revisions are not copied because size is greater then %1. You have to manually save old revision and check in as new revision!</source>
        <translation type="obsolete">Einige Revisionen können nicht kopiert werden, weil Ihre Grösse %1 übersteigt. Sie müssen die alte Revision von Hand speichern und als neue Revision wieder einchecken!</translation>
    </message>
    <message>
        <location line="-1150"/>
        <source>This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?</source>
        <translation>Der Datensatz wird bereits von Ihnen gesperrt. Wollen Sie die bestehende Sperrung verwerfen und mit der Bearbeitung neu beginnen?</translation>
    </message>
</context>
<context>
    <name>FUI_DM_DocumentsClass</name>
    <message>
        <source>FUI_DM_Documents</source>
        <translation type="obsolete">FUI_DM_Documents</translation>
    </message>
    <message>
        <location filename="fui_dm_documents.ui" line="+174"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="-35"/>
        <source>Local File Reference</source>
        <translation>Verweis auf lokale Datei</translation>
    </message>
    <message>
        <source>Is Private</source>
        <translation type="obsolete">Privat</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Is Template</source>
        <translation>Vorlage</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Dates</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Date Modified:</source>
        <translation>Letzte Änderung:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Date Created:</source>
        <translation>Erstellt am:</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Document Path:</source>
        <translation>Dokumentenpfad:</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+442"/>
        <source>Open Parameter:</source>
        <translation>Öffnungs-Parameter:</translation>
    </message>
    <message>
        <location line="-402"/>
        <source>Location:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Check Out As Read Only</source>
        <translation>Nur zum Lesen auschecken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Check Out Read Only</source>
        <translation>Nur zum Lesen auschecken</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Set TAG when checking document</source>
        <translation>Versionsmarkierung setzen beim Einchecken des Dokuments</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set Tag When Check In</source>
        <translation>Versionsmarkierung beim Einchecken setzen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>When checking out, ask for location. Otherwise document will be automatically saved in application temporary folder.</source>
        <translation>Beim Auschecken nach Pfad fragen. Ansonsten wird Dokument automatisch im temporären Anwendungsverzeichnis gespeichert.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ask Check Out  Location</source>
        <translation>Beim Auschecken nach Speicherort fragen</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Checked Out By:</source>
        <translation>Ausgecheckt von:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Checked Out Date:</source>
        <translation>Ausgecheckt am:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Checked Out Path:</source>
        <translation>Auscheckpfad:</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>   Check In</source>
        <translation>   Einchecken</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>   Check Out</source>
        <translation>   Auschecken</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Document</source>
        <translation>Dokument öffnen</translation>
    </message>
    <message>
        <source>Classification And Description</source>
        <translation type="obsolete">Einteilung und Beschreibung</translation>
    </message>
    <message>
        <location line="+296"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Application:</source>
        <translation>Anwendung:</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Template:</source>
        <translation>Vorlage:</translation>
    </message>
    <message>
        <source>Assignments</source>
        <translation type="obsolete">Zuweisungen</translation>
    </message>
    <message>
        <location line="-267"/>
        <source>Owner:</source>
        <translation>Besitzer:</translation>
    </message>
    <message>
        <location line="-882"/>
        <source>File Name:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+828"/>
        <source>Category:</source>
        <translation>Kategorie:</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Contact(s)</source>
        <translation>Kontakt(e)</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Projects(s)</source>
        <translation>Projekt(e)</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Classification</source>
        <translation>Einteilung</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Revision History</source>
        <translation>Versions-History</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Rights</source>
        <translation>Rechte</translation>
    </message>
    <message>
        <location line="-1075"/>
        <source>Document</source>
        <translation> Dokument</translation>
    </message>
    <message>
        <location line="+1132"/>
        <source>Task Details</source>
        <translation>Aufgabe</translation>
    </message>
</context>
<context>
    <name>FUI_DM_UserPaths</name>
    <message>
        <location filename="fui_dm_userpaths.cpp" line="+32"/>
        <source>Select Path</source>
        <translation>Pfad wählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Path</source>
        <translation>Pfad entfernen</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Delete Selected Records From Database</source>
        <translation>Ausgewählte Datensätze aus Datenbank löschen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>S&amp;ort</source>
        <translation>&amp;Sortieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>User Application Paths</source>
        <translation>Benutzer-Anwendungspfade</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Choose application</source>
        <translation>Applikation wählen</translation>
    </message>
    <message>
        <location line="+43"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-54"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Do you really want to delete the selected records {</source>
        <translation>Wollen Sie wirklich die ausgewählten Datensätze endgültig aus der Datenbank löschen {</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Application path is mandatory field!</source>
        <translation>Anwendungspfad zwingend!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Application is mandatory field!</source>
        <translation>Anwendung zwingend!</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>} from the database?</source>
        <translation>} aus der Datenbank?</translation>
    </message>
</context>
<context>
    <name>FUI_DM_UserPathsClass</name>
    <message>
        <source>FUI_DM_UserPaths</source>
        <translation type="obsolete">FUI_DM_UserPaths</translation>
    </message>
    <message>
        <location filename="fui_dm_userpaths.ui" line="+127"/>
        <source>Priority</source>
        <translation>Priorität</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Application path:</source>
        <translation>Anwendungspfad:</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>Application:</source>
        <translation>Anwendung:</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>Owner:</source>
        <translation>Besitzer:</translation>
    </message>
</context>
<context>
    <name>FUI_DebtorPayments</name>
    <message>
        <location filename="fui_debtorpayments.cpp" line="+34"/>
        <source>Payment Conditions</source>
        <translation>Zahlungskonditionen</translation>
    </message>
    <message>
        <location filename="fui_debtorpayments.ui" line="+14"/>
        <source>FUI_DebtorPayments</source>
        <translation>FUI_DebtorPayments</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Payment Period (in Days):</source>
        <translation>Zahlfrist (in Tagen):</translation>
    </message>
</context>
<context>
    <name>FUI_DocumentManagerClass</name>
    <message>
        <source>FUI_DocumentManager</source>
        <translation type="obsolete">FUI_DocumentManager</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;List of all &lt;span style=&quot; font-size:8pt;&quot;&gt;Documents in system &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;List of all &lt;span style=&quot; font-size:8pt;&quot;&gt;Documents in system &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="obsolete">Einfügen</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Bearbeiten</translation>
    </message>
    <message>
        <source>Open/Execute</source>
        <translation type="obsolete">Öffnen/Ausführen</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <source>Contact:</source>
        <translation type="obsolete">Kontakt:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="obsolete">Beschreibung:</translation>
    </message>
    <message>
        <source>Project:</source>
        <translation type="obsolete">Projekt:</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="obsolete">Art:</translation>
    </message>
    <message>
        <source>Filter Documents by selected contacts</source>
        <translation type="obsolete">Dokumente nach ausgewählten Kontakten filtern</translation>
    </message>
    <message>
        <source>Filters Documents by time range</source>
        <translation type="obsolete">Dokumente nach Zeitperiode filtern</translation>
    </message>
    <message>
        <source>Filter Documents by selected projects</source>
        <translation type="obsolete">Dokumente nach ausgewählten Projekten filtern</translation>
    </message>
    <message>
        <source>Filter Documents by selected types</source>
        <translation type="obsolete">Dokumenten nach ausgewählten Dokumentenarten filtern</translation>
    </message>
    <message>
        <source>Refresh Now</source>
        <translation type="obsolete">Jetzt aktualisieren</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Events for selected document&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Events for selected document&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Time Range</source>
        <translation type="obsolete">Zeitperiode</translation>
    </message>
    <message>
        <source>Per Date:</source>
        <translation type="obsolete">Per Datum:</translation>
    </message>
    <message>
        <source>Last Week</source>
        <translation type="obsolete">Letzte Woche</translation>
    </message>
    <message>
        <source>Last Month</source>
        <translation type="obsolete">Letzter Monat</translation>
    </message>
    <message>
        <source>Projects</source>
        <translation type="obsolete">Projekte</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="obsolete">Kontakte</translation>
    </message>
    <message>
        <source>Build Actual Contact List</source>
        <translation type="obsolete">Aktuelle Kontaktliste aufbauen</translation>
    </message>
    <message>
        <source>Interfaces</source>
        <translation type="obsolete">Schnittstellen</translation>
    </message>
    <message>
        <source>CE Types</source>
        <translation type="obsolete">Kommunikationsereignis-Typen</translation>
    </message>
</context>
<context>
    <name>FUI_ImportAppointment</name>
    <message>
        <source>From</source>
        <translation type="obsolete">Von</translation>
    </message>
    <message>
        <source>To</source>
        <translation type="obsolete">An</translation>
    </message>
    <message>
        <source>Subject</source>
        <translation type="obsolete">Betreff</translation>
    </message>
    <message>
        <source>Received/Sent</source>
        <translation type="obsolete">Gesendet/Erhalten</translation>
    </message>
    <message>
        <source>Projects</source>
        <translation type="obsolete">Projekte</translation>
    </message>
    <message>
        <source>Unassigned</source>
        <translation type="obsolete">Nicht zugewiesen</translation>
    </message>
    <message>
        <location filename="fui_importappointment.cpp" line="+189"/>
        <source>Importing Appointments...</source>
        <translation>Kalendereinträge importieren...</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Operation In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Operation stopped by user request!</source>
        <translation>Operation durch Benutzer abgebrochen!</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>No items have been selected for import!</source>
        <translation>Keine Daten für den Import ausgewählt!</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>%1 existing Appointments have been skipped!</source>
        <translation>%1 bestehende(r) Kalendereintrag(-einträge) wurde(n) übersprungen!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 existing Appointments have been replaced!</source>
        <translation>%1 bestehende(r) Kalendereintrag(-einträge) wurde(n) ersetzt!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message does not have Entry ID!</source>
        <translation>Nachricht hat keine ID!</translation>
    </message>
    <message>
        <location line="+473"/>
        <source>Do you want to assign project &quot;%1 %2&quot; to the selected Appointment(s)?</source>
        <translation>Wollen Sie das Projekt  &quot;%1 %2&quot; den ausgewählten Kalendereinträgen zuweisen?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to assign project &quot;%1 %2&quot; to the listed Appointment(s)?</source>
        <translation>Wollen Sie das Projekt  &quot;%1 %2&quot; den ausgewählten Kalendereinträgen zuweisen?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Question</source>
        <translation>Frage</translation>
    </message>
    <message>
        <location filename="fui_importappointment.h" line="+19"/>
        <source>Import Calendar Events</source>
        <translation>Kalendereinträge importieren</translation>
    </message>
    <message>
        <location filename="fui_importappointment.cpp" line="-666"/>
        <location line="+22"/>
        <source>Participant</source>
        <translation>Teilnehmer</translation>
    </message>
    <message>
        <location line="-16"/>
        <location line="+9"/>
        <source>Organizer</source>
        <translation>Organisator</translation>
    </message>
    <message>
        <location line="-245"/>
        <source>From Date</source>
        <translation>Von Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>To Date</source>
        <translation>Bis Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
</context>
<context>
    <name>FUI_ImportAppointmentClass</name>
    <message>
        <location filename="fui_importappointment.ui" line="+14"/>
        <source>Import Appointment</source>
        <translation>Kalendereintrag importieren</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>1. Select Source:</source>
        <translation>1. Quelle bestimmen:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>To import Appointments from Outlook(R), drag them with your mouse from Outlook and drop them onto the list below or onto the Drop Zone!</source>
        <translation>Sie können Kalendereinträge aus Outlook(R) importieren, indem Sie sie mit der Maus direkt aus Outlook(R) per Drag&amp;Drop auf die untenstehende Liste oder auf die Drop Zone ziehen!</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>To Date:</source>
        <translation>Bis Datum:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>From Date:</source>
        <translation>Von Datum:</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Select Source Folders</source>
        <translation>Quellvereichnis(se) bestimmen</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>2. Build List From Source</source>
        <translation>2. Liste aus Quelle aufbauen</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Assign Calendar Events</source>
        <translation>Kalendereinträge zuweisen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>To assign Calendar Events to projects, select them and select a project using the first pushbutton (Select) below:</source>
        <translation>Wählen Sie ein bestehendes Projekt mit dem Icon AUSWAHL, um es den selektierten Kalendereinträgen zuzuweisen:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Projects:</source>
        <translation>Projekte:</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Open Selected Calendar Event</source>
        <translation>Ausgewählten Kalendereintrag öffnen</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Overwrite Existing Calendar Events</source>
        <translation>Bestehenden Kalendereintrag überschreiben</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>3. Import Selected Calendar Events</source>
        <translation>3. Ausgewählte Kalendereinträge importieren</translation>
    </message>
</context>
<context>
    <name>FUI_ImportContact</name>
    <message>
        <location filename="fui_importcontact.cpp" line="+89"/>
        <location line="+136"/>
        <source>Outlook</source>
        <translation>Outlook</translation>
    </message>
    <message>
        <location line="-134"/>
        <location line="+147"/>
        <source>Skype Contacts</source>
        <translation>Skype Kontakte</translation>
    </message>
    <message>
        <location line="-145"/>
        <location line="+166"/>
        <source>Text File</source>
        <translation>Textdatei</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+294"/>
        <source>TwixTel CD</source>
        <translation>TwixTel CD</translation>
    </message>
    <message>
        <location line="-249"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Street</source>
        <translation>Strasse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Town</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phone numbers</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+114"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Text file (*.txt)</source>
        <translation>Textdatei (*.txt)</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Document does not contain %1 fields for each record!</source>
        <translation>Das Dokument enhält nicht %1 Felder für jeden Datensatz!</translation>
    </message>
    <message>
        <source>TwixTel query...</source>
        <translation type="obsolete">TwixTel-Abfrage...</translation>
    </message>
    <message>
        <location line="-28"/>
        <location line="+474"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-473"/>
        <location line="+474"/>
        <source>Operation In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <source>No records found!</source>
        <translation type="obsolete">Kein Datensatz gefunden!</translation>
    </message>
    <message>
        <location line="-313"/>
        <source>User aborted!</source>
        <translation>Abbruch durch Benutzer!</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>No items have been selected for import!</source>
        <translation>Keine Zeilen für Import ausgewählt!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Importing Contacts...</source>
        <translation>Kontakte werden importiert...</translation>
    </message>
    <message>
        <location line="+165"/>
        <source>Import Successful</source>
        <translation>Import erfolgreich</translation>
    </message>
    <message>
        <location line="-677"/>
        <location line="+889"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Error getting method from MAPI dll!</source>
        <translation type="obsolete">Fehler beim Zugriff auf MAPI-DLL!</translation>
    </message>
    <message>
        <location line="-851"/>
        <source>Loading Contacts From File...</source>
        <translation>Kontakte werden aus Datei geladen...</translation>
    </message>
    <message>
        <location line="+762"/>
        <source>vCard</source>
        <translation>vCard</translation>
    </message>
    <message>
        <source>Listing Contacts...</source>
        <translation type="obsolete">Kontakte auflisten...</translation>
    </message>
    <message>
        <source>There are no folders selected!</source>
        <translation type="obsolete">Kein Verzeichnis gewählt!</translation>
    </message>
    <message>
        <location line="-129"/>
        <source>%1 contacts imported, %2 contacts skipped.</source>
        <translation>%1 Kontakte importiert, %2 Kontakte übersprungen.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 contacts imported.</source>
        <translation>%1 Kontakte importiert.</translation>
    </message>
    <message>
        <location line="-782"/>
        <source>Modify</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+941"/>
        <source>No items have been selected for edit!</source>
        <translation>Keine Elemente zum Bearbeiten ausgewählt!</translation>
    </message>
    <message>
        <location line="-835"/>
        <source>Could not import contacts from Outlook(R). Please drag your contacts with your mouse from Outlook(R) and drop them to the (yet empty) contact list in this contact import window.</source>
        <translation>Es konnten keine Kontakte aus Outlook(R)importiert werden. Bitte ziehen Sie Ihre Kontakte mit der Maus direktaus Outlook(R) auf die (noch leere) Kontaktliste in diesemKontakt-Importfenster.</translation>
    </message>
    <message>
        <location line="+889"/>
        <source>No contacts in Skype(R) found. Please make sure you use an actual version of Skype(R)!</source>
        <translation>Es konnten keine Kontakte aus Skype(R)importiert werden. Bitte stellen Sie sicher, dass Sie eine aktuelleVersion von Skype(R) installiert haben!</translation>
    </message>
    <message>
        <location filename="fui_importcontact.h" line="+20"/>
        <source>Import Contacts</source>
        <translation>Kontakte importieren</translation>
    </message>
    <message>
        <location filename="fui_importcontact.cpp" line="-314"/>
        <source>Import stopped at contact #%1. Error: </source>
        <translation>Import abgebrochen bei Kontakt #%1. Fehler: </translation>
    </message>
</context>
<context>
    <name>FUI_ImportContactClass</name>
    <message>
        <location filename="fui_importcontact.ui" line="+13"/>
        <source>Import Contact</source>
        <translation>Kontakte importieren</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>1. Select Source:</source>
        <translation>1. Quelle bestimmen:</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Build All</source>
        <translation>Alle auflisten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use Filter</source>
        <translation>Filter benutzen</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last Name:</source>
        <translation>Nachname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Town:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>2. Build List From Source</source>
        <translation>2. Liste aus Quelle aufbauen</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Handling of Duplicates:</source>
        <translation>Vorgehen bei Duplikaten:</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Replace Existing</source>
        <translation>Bestehende ersetzen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update Existing</source>
        <translation>Bestehende aktualisieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Ignore Existing</source>
        <translation>Bestehende ignorieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Skip if Exists</source>
        <translation>Bestehende überspringen</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>3. Import Selected Contacts</source>
        <translation>3. Ausgewählte Kontakte importieren</translation>
    </message>
    <message>
        <location line="-256"/>
        <source>Select Source Folders</source>
        <translation>Quellvereichnis(se) bestimmen</translation>
    </message>
</context>
<context>
    <name>FUI_ImportContactContactRel</name>
    <message>
        <location filename="fui_importcontactcontactrel.cpp" line="+25"/>
        <source>Text File</source>
        <translation>Textdatei</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Contact #1 First Name</source>
        <translation>Kontakt #1 Vorname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact #1 Last Name</source>
        <translation>Kontakt #1 Nachname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact #1 Organization</source>
        <translation>Kontakt #1 Organisation</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact #2 First Name</source>
        <translation>Kontakt #2 Vorname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact #2 Last Name</source>
        <translation>Kontakt #2 Nachname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact #2 Organization</source>
        <translation>Kontakt #2 Organisation</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import File (*.txt)</source>
        <translation>Datei importieren (*.txt)</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Document does not contain %1 fields for each record!</source>
        <translation>Das Dokument enhält nicht %1 Felder für jeden Datensatz!</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>No items have been selected for import!</source>
        <translation>Keine Daten für den Import ausgewählt!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Importing Records...</source>
        <translation>Datensätze werden importiert...</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Operation In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <location filename="fui_importcontactcontactrel.h" line="+18"/>
        <source>Import Contact-Contact Relationships</source>
        <translation>Beziehunungen zwischen Kontakten importieren</translation>
    </message>
</context>
<context>
    <name>FUI_ImportContactContactRelClass</name>
    <message>
        <location filename="fui_importcontactcontactrel.ui" line="+14"/>
        <source>Import Contact-Contact Relationships</source>
        <translation>Beziehunungen zwischen Kontakten importieren</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>1. Select Source:</source>
        <translation>1. Quelle bestimmen:</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>2. Build List From Source</source>
        <translation>2. Liste aus Quelle aufbauen</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>3. Import Selected Records</source>
        <translation>3. Ausgewählte Datensätze importieren</translation>
    </message>
</context>
<context>
    <name>FUI_ImportEmail</name>
    <message>
        <location filename="fui_importemail.cpp" line="+71"/>
        <source>Outlook</source>
        <translation>Outlook</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>From</source>
        <translation>Von</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>To</source>
        <translation>An</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Subject</source>
        <translation>Betreff</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Received/Sent</source>
        <translation>Gesendet/Erhalten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unassigned</source>
        <translation>Nicht zugewiesen</translation>
    </message>
    <message>
        <location line="+219"/>
        <source>There are no folders selected!</source>
        <translation>Keine Verzeichnisse ausgewählt!</translation>
    </message>
    <message>
        <source>Listing Emails...</source>
        <translation type="obsolete">E-Mails überwachen...</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Operation In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Importing Emails...</source>
        <translation>E-Mails werden importiert...</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Operation stopped by user request!</source>
        <translation>Operation durch Benutzer abgebrochen!</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>No items have been selected for import!</source>
        <translation>Keine Zeilen für Import ausgewählt!</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message does not have Entry ID!</source>
        <translation>Nachricht besitzt keine Entry ID!</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+57"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Error opening email!</source>
        <translation>Fehler beim Öffnen einer E-Mail!</translation>
    </message>
    <message>
        <location line="+747"/>
        <source>Do you want to assign project &quot;%1 %2&quot; to the selected email(s)?</source>
        <translation>Wollen Sie das Projekt  &quot;%1 %2&quot; den ausgewählten E-Mails zuweisen?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to assign project &quot;%1 %2&quot; to the listed email(s)?</source>
        <translation>Wollen Sie das Projekt  &quot;%1 %2&quot; den aufgeführten E-Mails zuweisen?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Question</source>
        <translation>Frage</translation>
    </message>
    <message>
        <location filename="fui_importemail.h" line="+19"/>
        <source>Import Emails</source>
        <translation>E-Mails importieren</translation>
    </message>
    <message>
        <location filename="fui_importemail.cpp" line="-1290"/>
        <source>Thunderbird</source>
        <translation>Thunderbird</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>Thunderbird email client is not installed!</source>
        <translation>Der Thunderbird-E-Mail-Client ist nicht installiert!</translation>
    </message>
    <message>
        <location line="+226"/>
        <source>%1 existing emails have been skipped!</source>
        <translation>%1 bestehende E-Mails wurden übersprungen!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 existing emails have been replaced!</source>
        <translation>%1 bestehende E-Mails wurden ersetzt!</translation>
    </message>
    <message>
        <location line="+1140"/>
        <source>Attachment size limits reached. &apos;%1� Skipped!</source>
        <oldsource>Attachment size limits reached. &apos;%1 Skipped!</oldsource>
        <translation type="unfinished">Maximale Grösse für Anhänge erreicht. &apos;%1 Übersprungen!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Attachment Skipped � Read Info.txt</source>
        <oldsource>Attachment Skipped  Read Info.txt</oldsource>
        <translation type="unfinished">Anhang übersprungen  Info.txt lesen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Attachment %1 are not imported, maximum size is exceeded</source>
        <translation>Anhang %1 nicht importiert, maximale Grösse überschritten</translation>
    </message>
</context>
<context>
    <name>FUI_ImportEmailClass</name>
    <message>
        <location filename="fui_importemail.ui" line="+13"/>
        <source>Import Email</source>
        <translation>E-Mails importieren</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>1. Select Source:</source>
        <translation>1. Quelle bestimmen:</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Build All</source>
        <translation>Alle auflisten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use Filter</source>
        <translation>Filter benutzen</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Search text in &quot;Subject&quot;:</source>
        <translation>Text in &quot;Betreff&quot; suchen:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Search text in &quot;From&quot;:</source>
        <translation>Text in &quot;Von&quot; suchen:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Search text in &quot;CC&quot;:</source>
        <translation>Text in &quot;CC&quot; suchen:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Search text in &quot;To&quot;:</source>
        <translation>Text in &quot;An&quot; suchen:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>To Date:</source>
        <translation>Bis Datum:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Search text in &quot;BCC&quot;:</source>
        <translation>Text in &quot;BCC&quot; suchen:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>From Date:</source>
        <translation>Von Datum:</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Select Source Folders</source>
        <translation>Quellvereichnis(se) bestimmen</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>OR</source>
        <translation>ODER</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>AND</source>
        <translation>UND</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>2. Build List From Source</source>
        <translation>2. Liste aus Quelle aufbauen</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>The following email addresses have not been recognized an can not be assigned to a contact.
Please use the first icon to the right of the list (Select) to select an existing contact for the actual email address.
Use the fourth icon (Add) to create a new contact from the selected email address.</source>
        <oldsource>The following email addresses have not been recognized an can not be assigned to a contact.
Please use the first icon to the right of the list (Select) to select an existing contact for the actual email address.
Use the fourth icon (Add) to create a new contact from the selected email address.</oldsource>
        <translation type="unfinished">Die folgenden E-Mail-Adressen konnten keinem Kontakt zugeordnet werden.
Verwenden Sie das erste Icon auf der rechten Seite der Liste (Auswahl), um einen bestehenden Kontakt der aktuellen E-Mail-Adresse zuzuordnen.
Mit dem vierten Icon (Hinzufügen) können Sie aus der aktuellen E-Mail-Adresse einen neuen Kontakt erzeugen.</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>Projects:</source>
        <translation>Projekte:</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Open Selected Email</source>
        <translation>Ausgewähltes E-Mail öffnen</translation>
    </message>
    <message>
        <location line="-180"/>
        <source>Unassigned Emails:</source>
        <translation>E-Mails ohne Zuweisung:</translation>
    </message>
    <message>
        <location line="+273"/>
        <source>3. Import Selected Emails</source>
        <translation>3. Ausgewählte E-Mails importieren</translation>
    </message>
    <message>
        <location line="-304"/>
        <source>List of Unknown Email Addresses</source>
        <translation>Unbekannte E-Mail-Adressen</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Assign Emails to Projects</source>
        <translation>E-Mails Projekten zuweisen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>To assign emails to projects, select them and select a project using the first pushbutton (Select) below:</source>
        <translation>Wählen Sie ein bestehendes Projekt mit dem Icon AUSWAHL, um es den selektierten E-Mails zuzuweisen:</translation>
    </message>
    <message>
        <location line="-515"/>
        <source>To import emails from Outlook(R), drag them with your mouse from Outlook and drop them onto the list below or onto the Drop Zone!</source>
        <translation>Sie können E-Mails aus Outlook(R) importieren, indem Sie sie mit der Maus direkt aus Outlook(R) per Drag&amp;Drop auf die untenstehende Liste oder auf die Drop Zone ziehen!</translation>
    </message>
    <message>
        <location line="+648"/>
        <source>Overwrite Existing Emails</source>
        <translation>Bestehende E-Mails überschreiben</translation>
    </message>
</context>
<context>
    <name>FUI_ImportPerson</name>
    <message>
        <location filename="fui_importperson.cpp" line="+34"/>
        <source>Text File</source>
        <translation>Textdatei</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Department</source>
        <translation>Abteilung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Employee Code</source>
        <translation>Personalnummer</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import File (*.txt)</source>
        <translation>Datei importieren (*.txt)</translation>
    </message>
    <message>
        <source>Document does not contain 51 fields for each record!</source>
        <translation type="obsolete">Das Dokument enhält nicht 51 Felder für jeden Datensatz!</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Document does not contain %1 fields for each record!</source>
        <translation>Das Dokument enhält nicht %1 Felder für jeden Datensatz!</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>No items have been selected for import!</source>
        <translation>Keine Zeilen für den Import ausgewählt!</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Importing Users...</source>
        <translation>Benutzer werden importiert...</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Operation In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <location filename="fui_importperson.h" line="+18"/>
        <source>Import Users</source>
        <translation>Benutzer importieren</translation>
    </message>
    <message>
        <location filename="fui_importperson.cpp" line="+72"/>
        <source>Done!</source>
        <translation>Erledigt!</translation>
    </message>
</context>
<context>
    <name>FUI_ImportPersonClass</name>
    <message>
        <location filename="fui_importperson.ui" line="+14"/>
        <source>Import Person</source>
        <translation>Benutzer importieren</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>1. Select Source:</source>
        <translation>1. Quelle bestimmen:</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>2. Build List From Source</source>
        <translation>2. Liste aus Quelle aufbauen</translation>
    </message>
    <message>
        <source>Create Users</source>
        <translation type="obsolete">Benutzer erzeugen</translation>
    </message>
    <message>
        <source>Create Contacts</source>
        <translation type="obsolete">Kontakte erzeugen</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Handling of Duplicates:</source>
        <translation>Vorgehen bei Duplikaten:</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Replace Existing</source>
        <translation>Bestehende ersetzen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update Existing</source>
        <translation>Bestehende aktualisieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Ignore Existing</source>
        <translation>Bestehende ignorieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Skip if Exists</source>
        <translation>Bestehende überspringen</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>3. Import Selected Users</source>
        <translation>3. Ausgewählte Benutzer importieren</translation>
    </message>
    <message>
        <location line="-257"/>
        <source>Role:</source>
        <translation>Rolle:</translation>
    </message>
</context>
<context>
    <name>FUI_ImportProject</name>
    <message>
        <location filename="fui_importproject.cpp" line="+38"/>
        <source>Text File</source>
        <translation>Textdatei</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Project Code</source>
        <translation>Projektcode</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Project Name</source>
        <translation>Projektname</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import File (*.txt)</source>
        <translation>Datei importieren (*.txt)</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Document does not contain %1 fields for each record!</source>
        <translation>Das Dokument enhält nicht %1 Felder für jeden Datensatz!</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>No items have been selected for import!</source>
        <translation>Keine Zeilen für Import ausgewählt!</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Importing Projects...</source>
        <translation>Projekte werden importiert...</translation>
    </message>
    <message>
        <location line="-252"/>
        <location line="+252"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-251"/>
        <location line="+252"/>
        <source>Operation In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <location line="-253"/>
        <source>Loading/parsing CSV file...</source>
        <translation>Laden und Parsieren der CSV-Datei...</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>You have reached the limit to the number of allowed projects!
Can not insert new projects!</source>
        <translation>Maximale Anzahl zulässiger Projekte erreicht:
Kein weiteres Projekt kann mehr erfasst werden!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>You will reach the limit to the number of allowed projects!
Try selecting smaller number of projects!</source>
        <translation>Zu viele Projekte ausgewählt! 
Die maximale Anzahl zulässiger Projekte würde überschritten!</translation>
    </message>
    <message>
        <location filename="fui_importproject.h" line="+16"/>
        <source>Import Projects</source>
        <translation>Projekte importieren</translation>
    </message>
</context>
<context>
    <name>FUI_ImportProjectClass</name>
    <message>
        <location filename="fui_importproject.ui" line="+13"/>
        <source>Import Project</source>
        <translation>Projekte importieren</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>1. Select Source:</source>
        <translation>1. Quelle bestimmen:</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>2. Build List From Source</source>
        <translation>2. Liste aus Quelle aufbauen</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Handling of Duplicates:</source>
        <translation>Vorgehen bei Duplikaten:</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Replace Existing</source>
        <translation>Bestehende ersetzen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update Existing</source>
        <translation>Bestehende aktualisieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Ignore Existing</source>
        <translation>Bestehende ignorieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Skip if Exists</source>
        <translation>Bestehende überspringen</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>3. Import Selected Projects</source>
        <translation>3. Ausgewählte Projekte importieren</translation>
    </message>
</context>
<context>
    <name>FUI_ImportUserContactRel</name>
    <message>
        <location filename="fui_importusercontactrel.cpp" line="+25"/>
        <source>Text File</source>
        <translation>Textdatei</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Contact First Name</source>
        <translation>Kontakt Vorname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact Last Name</source>
        <translation>Kontakt Nachname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact Organization</source>
        <translation>Kontakt Organisation</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User First Name</source>
        <translation>Benutzer Vorname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Last Name</source>
        <translation>Benutzer Nachname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Code</source>
        <translation>Benutzer Personalnummer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Initials</source>
        <translation>Benutzer Initialen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import File (*.txt)</source>
        <translation>Datei importieren (*.txt)</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Document does not contain %1 fields for each record!</source>
        <translation>Das Dokument enhält nicht %1 Felder für jeden Datensatz!</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>No items have been selected for import!</source>
        <translation>Keine Daten für den Import ausgewählt!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Importing Records...</source>
        <translation>Datensätze werden importiert...</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Operation In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <location filename="fui_importusercontactrel.h" line="+18"/>
        <source>Import User-Contact Relationships</source>
        <translation>Beziehunungen zwischen Kontakten und Benutzern importieren</translation>
    </message>
</context>
<context>
    <name>FUI_ImportUserContactRelClass</name>
    <message>
        <location filename="fui_importusercontactrel.ui" line="+14"/>
        <source>Import User-Contact Relationships</source>
        <translation>Beziehunungen zwischen Kontakten und Benutzern importieren</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>1. Select Source:</source>
        <translation>1. Quelle bestimmen:</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>2. Build List From Source</source>
        <translation>2. Liste aus Quelle aufbauen</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>3. Import Selected Records</source>
        <translation>3. Ausgewählte Datensätze importieren</translation>
    </message>
</context>
<context>
    <name>FUI_InvitationCard</name>
    <message>
        <location filename="fui_invitationcard.cpp" line="+52"/>
        <source>Invitation Card</source>
        <translation>Einladungskarte</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Invitation Card: </source>
        <translation>Einladungskarte:</translation>
    </message>
</context>
<context>
    <name>FUI_InvitationCardClass</name>
    <message>
        <location filename="fui_invitationcard.ui" line="+19"/>
        <source>SOKRATES Card</source>
        <translation>SOKRATES Karte</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Start Task</source>
        <translation>Aufgabe beginnen</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>image</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>task date</source>
        <translation>Datum Aufgabe</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>AS</source>
        <translation>AS</translation>
    </message>
</context>
<context>
    <name>FUI_NMRXRoles</name>
    <message>
        <source>Assignment Role Definitions: </source>
        <translation type="obsolete">Definitionen von Rollen:</translation>
    </message>
    <message>
        <source>Assignment Role Definitions</source>
        <translation type="obsolete">Definitionen von Rollen</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
</context>
<context>
    <name>FUI_NMRXRolesClass</name>
    <message>
        <source>FUI_NMRXRoles</source>
        <translation type="obsolete">FUI_NMRXRoles</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="obsolete">Beschreibung:</translation>
    </message>
</context>
<context>
    <name>FUI_PBXConfig</name>
    <message>
        <location filename="fui_pbxconfig.cpp" line="+40"/>
        <source>Online</source>
        <translation>Online</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Account</source>
        <translation>Konto</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phone Number</source>
        <translation>Telefonnummer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forwarded</source>
        <translation>Weitergeleitet</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Phone account</source>
        <translation>Telephonkonto</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command</source>
        <translation>Kommando</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Foward To</source>
        <translation>Weiterleiten an</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Frequence</source>
        <translation>Periodizität</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Valid From</source>
        <translation>Gültig ab</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Valid To</source>
        <translation>Gültig bis</translation>
    </message>
    <message>
        <location filename="fui_pbxconfig.h" line="+16"/>
        <source>Call Forwarding</source>
        <translation>Anrufweiterleitung</translation>
    </message>
</context>
<context>
    <name>FUI_PBXConfigClass</name>
    <message>
        <location filename="fui_pbxconfig.ui" line="+16"/>
        <source>PBX Configuration</source>
        <translation>Konfiguration Telefonzentrasle</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Forward Calls</source>
        <translation>Anrufe weiterleiten</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Forward CallsTo:</source>
        <translation>Anrufe weiterleiten an:</translation>
    </message>
    <message>
        <location line="+106"/>
        <location line="+383"/>
        <source>Skype-In Phone Number:</source>
        <translation>Skype-In Telefonnummer:</translation>
    </message>
    <message>
        <location line="-376"/>
        <source>Local Skype Account:</source>
        <translation>Lokales Skype Konto:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Registration</source>
        <translation>Registrierung</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Read Local Skype Account</source>
        <translation>Lokales Skype Konto lesen</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Register Now</source>
        <translation>Jetzt registrieren</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>Registered</source>
        <translation>Registriert</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unregistered</source>
        <translation>Unregistriert</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Forwarded To:</source>
        <translation>Weitergeleitet an:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Skype Account:</source>
        <translation>Skype Konto:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Forward Account Access:</source>
        <translation>Zugriff auf Weiterleitungskonto:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Public</source>
        <translation>Öffentlich</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>&lt;b&gt;Registered Phone Accounts&lt;b&gt;</source>
        <translation>&lt;b&gt;Registrierte Telefonkonten&lt;b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Scheduler</source>
        <translation>Scheduler</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>Phone Account:</source>
        <translation>Telefonkonto:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Once</source>
        <translation>Einmal</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Every Day</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Workdays</source>
        <translation>Werktage</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Weekends</source>
        <translation>Wochenenden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Mondays</source>
        <translation>Montags</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Tuesdays</source>
        <translation>Dienstags</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Wednesdays</source>
        <translation>Mittwochs</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Thursdays</source>
        <translation>Donnerstags</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Fridays</source>
        <translation>Freitags</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Saturdays</source>
        <translation>Samstags</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sundays</source>
        <translation>Sonntags</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Valid From Date:</source>
        <translation>Gültig ab:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Forward To:</source>
        <translation>Weiterleiten an:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Time:</source>
        <translation>Zeit:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Valid To Date:</source>
        <translation>Gültig bis:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Frequence:</source>
        <translation>Periodizität:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Insert</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>FUI_Projects</name>
    <message>
        <location filename="fui_projects.cpp" line="+399"/>
        <source>Project: </source>
        <translation>Projekt:</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+187"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location line="-452"/>
        <source>Assigned Users And Contacts</source>
        <translation>Zugewiesene Benutzer und Kontakte</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Contact Calendar Entries</source>
        <translation>Einträge in Kontaktkalender</translation>
    </message>
    <message>
        <location line="+136"/>
        <location line="+41"/>
        <source>You have reached the limit to the number of allowed projects!
Can not insert new project!</source>
        <translation>Maximale Anzahl zulässiger Projekte erreicht:Kein weiteres Projekt kann mehr erfasst werden!</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>Do you want to proceed to edit project details?</source>
        <translation>Wollen Sie damit fortfahren, die Projektdaten zu bearbeiten?</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Contacts (</source>
        <translation>Kontakte (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Contacts</source>
        <translation>Kontakte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source> And </source>
        <translation> und </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Users (</source>
        <translation>Benutzer (</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Users</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="-180"/>
        <source>No template selected!</source>
        <translation>Keine Vorlage ausgewählt!</translation>
    </message>
    <message>
        <source>Loading Data...</source>
        <translation type="obsolete">Daten werden geladen...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Read In Progress</source>
        <translation type="obsolete">Operation wird ausgeführt</translation>
    </message>
    <message>
        <source>Saving Data...</source>
        <translation type="obsolete">Daten werden gespeichern...</translation>
    </message>
</context>
<context>
    <name>FUI_ProjectsClass</name>
    <message>
        <location filename="fui_projects.ui" line="+14"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location line="+271"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Leader:</source>
        <translation>Leiter:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Responsible:</source>
        <translation>Verantwortlicher:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Department:</source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Order Date:</source>
        <translation>Auftragsdatum:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Offer Valid Until:</source>
        <translation>Offerte gültig bis:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Contract Date:</source>
        <translation>Vertragsdatum:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Offer Date:</source>
        <translation>Offertdatum:</translation>
    </message>
    <message>
        <location line="-166"/>
        <source>Fixed Fee:</source>
        <translation>Pauschalsumme:</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Time Budget:</source>
        <translation>Zeitbudget:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Cost Budget:</source>
        <translation>Aufwandbudget:</translation>
    </message>
    <message>
        <location line="+227"/>
        <source>Latest Deadline:</source>
        <translation>Letzter Termin:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Deadline:</source>
        <translation>Termin:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Completion Date:</source>
        <translation>Fertigstellung:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Start Date:</source>
        <translation>Startdatum:</translation>
    </message>
    <message>
        <location line="-632"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Is Template</source>
        <translation>Vorlage</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Documents &amp;&amp; Assignments</source>
        <translation>Dokumente und Beziehungen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Users And Contacts</source>
        <translation>Benutzer und Kontakte</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Detail Data</source>
        <translation>Details</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Budget</source>
        <translation>Budget</translation>
    </message>
    <message>
        <location line="+132"/>
        <source>Dates</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location line="+255"/>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Rights</source>
        <translation>Rechte</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Reservations</source>
        <translation>Reservierungen</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Custom Fields</source>
        <translation>Benutzerdefinierte Felder</translation>
    </message>
</context>
<context>
    <name>FUI_RoleDefinition</name>
    <message>
        <location filename="fui_roledefinition.cpp" line="+58"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert new role</source>
        <translation>Neue Rolle einfügen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Rename role</source>
        <translation>Rolle umbenennen</translation>
    </message>
    <message>
        <location line="+848"/>
        <source>Locking error!</source>
        <translation>Fehler aufgrund einer Datensatzsperrung!</translation>
    </message>
    <message>
        <location line="-890"/>
        <source>Role Definition</source>
        <translation>Rollen-Definition</translation>
    </message>
</context>
<context>
    <name>FUI_RoleDefinitionClass</name>
    <message>
        <source>FUI_RoleDefinition</source>
        <translation type="obsolete">FUI_RoleDefinition</translation>
    </message>
    <message>
        <location filename="fui_roledefinition.ui" line="+160"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Save Changes</source>
        <translation type="obsolete">Änderungen speichern</translation>
    </message>
    <message>
        <source>Assign Access Rights Sets to Multiple Roles</source>
        <translation type="obsolete">Sets von Zugriffsrechten mehreren Rollen zuweisen</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>Insert New Role</source>
        <translation>Neue Rolle einfügen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="-120"/>
        <source>List of Access Rights Sets:</source>
        <translation>Liste der Sets von Zugriffsrechten:</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>List of Roles:</source>
        <translation>Rollenliste:</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Role Description:</source>
        <translation>Rollenbeschreibung:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>System Role</source>
        <translation>System-Rolle</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Business Visible</source>
        <translation>Auf Applikationsebene sichtbar</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Access Right Point Description:</source>
        <translation>Beschreibung Zugriffsrecht:</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Access Right Points:</source>
        <translation>Zugriffsrechte:</translation>
    </message>
    <message>
        <location line="-17"/>
        <location line="+9"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Assign Selected Access Rights Sets to Multiple Roles</source>
        <translation>Ausgewählte Zugriffsrechten mehreren Rollen zuweisen</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Bearbeiten</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Insert or Modify Roles</source>
        <translation>Rollen erstellen oder bearbeiten</translation>
    </message>
</context>
<context>
    <name>FUI_TaskRemindersClass</name>
    <message>
        <location filename="fui_taskreminders.ui" line="+19"/>
        <source>SideBarFui_Desktop</source>
        <translation>SideBarFui_Desktop</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+62"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
<context>
    <name>FUI_VoiceCall</name>
    <message>
        <location filename="fui_voicecall.cpp" line="+27"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call</source>
        <translation>Anrufen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Document</source>
        <translation> Dokument</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Phone Call</source>
        <translation>Telefonanruf</translation>
    </message>
</context>
<context>
    <name>FUI_VoiceCallCenter</name>
    <message>
        <location filename="fui_voicecallcenter.cpp" line="+397"/>
        <location line="+105"/>
        <source>Call Now!</source>
        <translation>Jetzt anrufen!</translation>
    </message>
    <message>
        <location line="-104"/>
        <location line="+1"/>
        <location line="+18"/>
        <location line="+16"/>
        <location line="+12"/>
        <location line="+42"/>
        <source>Hangup</source>
        <translation>Auflegen</translation>
    </message>
    <message>
        <location line="-72"/>
        <source>Accept Call</source>
        <translation>Anruf entgegennehmen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Redirect To...</source>
        <translation>Weiterleiten an...</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Hold</source>
        <translation>Halten</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+30"/>
        <source>Connect To...</source>
        <translation>Verbinden mit...</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Connect Now</source>
        <translation>Jetzt verbinden</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Resume</source>
        <translation>Wieder aufnehmen</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Call Back Now!</source>
        <translation>Jetzt zurückrufen!</translation>
    </message>
    <message>
        <location line="+504"/>
        <source>Phone Call Center - Call status: </source>
        <translation>Telefon-Zentrale - Anrufstatus: </translation>
    </message>
    <message>
        <location line="-22"/>
        <source>Phone Call Center</source>
        <translation>Telefon-Zentrale</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>You have reached the limit to the number of allowed calls stored!
Can not save this call!</source>
        <translation>Maximale Anzahl zulässiger Anrufe erreicht:
Dieser Anruf kann nicht gespeichert werden!</translation>
    </message>
    <message>
        <location line="+515"/>
        <source>Assign Project To Contact</source>
        <translation>Projekt zu Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Assign Project To Contact?</source>
        <translation>Projekt zu Kontakt zuweisen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location line="+204"/>
        <location line="+31"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-31"/>
        <location line="+31"/>
        <source>Invalid time!</source>
        <translation>Ungültige Zeit!</translation>
    </message>
    <message>
        <location line="+318"/>
        <source>Status: Offline</source>
        <translation>Status: Offline</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Status: Online</source>
        <translation>Status: Online</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Status: Away</source>
        <translation>Status: Abwesend</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Status: Not Available</source>
        <translation>Status: Nicht verfügbar</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Status: Do Not Disturb!</source>
        <translation>Status: Bitte nicht stören!</translation>
    </message>
    <message>
        <location line="+400"/>
        <location line="+22"/>
        <source>No contact selected!</source>
        <translation>Kein Kontakt ausgewählt!</translation>
    </message>
    <message>
        <location line="-1678"/>
        <source>You must first define a start time!</source>
        <translation>Zuerst muss eine Startzeit erfasst werden!</translation>
    </message>
    <message>
        <location line="+1293"/>
        <source>Incoming Phone Call</source>
        <translation>Eingehender Anruf</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Outgoing Phone Call</source>
        <translation>Ausgehender Anruf</translation>
    </message>
    <message>
        <location line="-267"/>
        <location line="+332"/>
        <source>You must enter Skype Number!</source>
        <translation>Bitte Skype Nummer eingeben!</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>The chat-window can only be used for contacts with a Skype-name!</source>
        <translation>Das Chat-Fenster kann nur für Kontakte mit einer Skype-Adresse geöffnet werden!</translation>
    </message>
    <message>
        <location line="-1377"/>
        <location line="+2"/>
        <source>Phone Call From %1 For %2: %3</source>
        <translation>Anruf von %1 für %2: %3</translation>
    </message>
    <message>
        <location line="-699"/>
        <source>Phone Call</source>
        <translation>Anruf</translation>
    </message>
    <message>
        <location line="+346"/>
        <location line="+64"/>
        <location line="+46"/>
        <source>You don&apos;t have access rights to transfer the call!</source>
        <translation>Kein Zugriffsrecht für das Weiterleiten von Anrufen!</translation>
    </message>
    <message>
        <location line="+1309"/>
        <source>Could not open requested TAPI device!</source>
        <translation>Ausgewählte TAPI-Telephonschnittstelle kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <source>TAPI Error!</source>
        <translation type="obsolete">TAPI (Telephonie)-Fehler!</translation>
    </message>
    <message>
        <location line="+425"/>
        <source>TAPI Error: %1!</source>
        <translation>TAPI (Telephonie)-Fehler: %1!</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Phone Line Occupied!</source>
        <translation>Telefonlinie besetzt!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Phone Operation Failed!</source>
        <translation>Problem bei Telefoniefunktion!</translation>
    </message>
</context>
<context>
    <name>FUI_VoiceCallCenterClass</name>
    <message>
        <location filename="fui_voicecallcenter.ui" line="+20"/>
        <source>Voice Call Center</source>
        <translation>Telefonzentrale</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Phone Number:</source>
        <translation>Telefonnummer:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Number</source>
        <translation>Nummer</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Contact:</source>
        <translation>Kontakt:</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Project:</source>
        <translation>Projekt:</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="obsolete">Kontakte</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Assigned Projects</source>
        <translation>Zugewiesene Projekte</translation>
    </message>
    <message>
        <location line="-173"/>
        <source>Interface:</source>
        <translation>Schnittstelle:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>interface</source>
        <translation>Schnittstelle</translation>
    </message>
    <message>
        <location line="+797"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="-79"/>
        <source>Start Time:</source>
        <translation>Startzeit:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>End Time:</source>
        <translation>Endzeit:</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Duration (h:m:s):</source>
        <translation>Dauer (h:m.s):</translation>
    </message>
    <message>
        <location line="+154"/>
        <source>Save &amp;&amp; Close</source>
        <translation>Speichern &amp;&amp; Schliessen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-496"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Schedule Task</source>
        <translation>Aufgabe planen</translation>
    </message>
    <message>
        <location line="-680"/>
        <source>Incoming</source>
        <translation>Eingehend</translation>
    </message>
    <message>
        <location line="-47"/>
        <source>Outgoing</source>
        <translation>Ausgehend</translation>
    </message>
    <message>
        <source>Is Private</source>
        <translation type="obsolete">Privat</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Other Phone Numbers of This Contact</source>
        <translation>Andere Telefonnummern dieses Kontakts</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Find Contact For This Phone Number</source>
        <translation>Kontakt zur Telefonnummer suchen</translation>
    </message>
    <message>
        <location line="+622"/>
        <source>&lt;b&gt;Conference Calls:&lt;/b&gt;&lt;i&gt;To add one more contact to the actual call, simply start a second call!&lt;/i&gt;</source>
        <translation>&lt;b&gt;Konferenzen:&lt;/b&gt;&lt;i&gt; Sie können jetzt auch weitere Kontakte anrufen, um eine Telephonkonferenz zu starten!&lt;/i&gt;</translation>
    </message>
    <message>
        <location line="-254"/>
        <source>Open Chat Window</source>
        <translation>Chat-Fenster öffnen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Send SMS</source>
        <translation>SMS senden</translation>
    </message>
    <message>
        <location line="-471"/>
        <source>Phone Call</source>
        <translation>Telefonanruf</translation>
    </message>
    <message>
        <location line="+941"/>
        <source>Rights:</source>
        <translation>Rechte:</translation>
    </message>
    <message>
        <location line="-187"/>
        <source>Call Details</source>
        <translation>Anrufprotokoll</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Task Details</source>
        <translation>Aufgabe</translation>
    </message>
    <message>
        <location line="-819"/>
        <source>Add Phone Number to Contact</source>
        <translation>Telephonnummer dem aktuellen Kontakt zuweisen</translation>
    </message>
</context>
<context>
    <name>FUI_VoiceCallClass</name>
    <message>
        <location filename="fui_voicecall.ui" line="+16"/>
        <source>Voice Call</source>
        <translation>Anruf</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Outgoing</source>
        <translation>Ausgehend</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Phone Number:</source>
        <translation>Telefonnummer:</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Duration (h:m:s):</source>
        <translation>Dauer (h:m.s):</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Start Time:</source>
        <translation>Startzeit:</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Interface:</source>
        <translation>Schnittstelle:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Incoming</source>
        <translation>Eingehend</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Project:</source>
        <translation>Projekt:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Contact:</source>
        <translation>Kontakt:</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>End Time:</source>
        <translation>Endzeit:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Forwarded</source>
        <translation>Weitergeleitet</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Schedule Follow-Up</source>
        <translation>Folgeaufgabe planen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
</context>
<context>
    <name>FuiBase</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="fuibase.cpp" line="+498"/>
        <location line="+37"/>
        <location line="+403"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-442"/>
        <source>Do you really want to delete this record permanently from the database?</source>
        <translation>Wollen Sie diesen Datensatz endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+440"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-440"/>
        <location line="+440"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <source>Function SetActiveGroupVisible is not implemented in this FUI!</source>
        <translation type="obsolete">Function SetActiveGroupVisible is not implemented in this FUI!</translation>
    </message>
    <message>
        <location line="-132"/>
        <source>Do you really want to delete element &quot;%1 %2&quot; from the database?</source>
        <translation>Wollen Sie  &quot;%1 %2&quot; endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>
%1 subelement(s) will be deleted as well!</source>
        <translation>
%1 Unterelement(e) wird/werden ebenfalls gelöscht!</translation>
    </message>
    <message>
        <location line="-274"/>
        <source>Load Data before copy!</source>
        <translation>Vor dem Kopieren muss ein Datensatz geladen sein!</translation>
    </message>
    <message>
        <location line="+277"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <location line="-318"/>
        <source>Do you really want to delete &apos;</source>
        <translation>Wollen Sie &apos;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&apos; permanently from the database?</source>
        <translation>&apos; endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <source>Record is already locked by you, do you wish to discard old lock and to continue editing record?&apos;</source>
        <translation type="obsolete">Der Datensatz wird bereits von Ihnen gesperrt. Wollen Sie die bestehende Sperrung verwerfen und mit der Bearbeitung neu beginnen?</translation>
    </message>
    <message>
        <location line="+444"/>
        <source>This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?</source>
        <translation>Der Datensatz wird bereits von Ihnen gesperrt. Wollen Sie die bestehende Sperrung verwerfen und mit der Bearbeitung neu beginnen?</translation>
    </message>
</context>
<context>
    <name>FuiManager</name>
    <message>
        <location filename="fuimanager.cpp" line="+764"/>
        <source> in edit or insert mode!</source>
        <translation> im Bearbeitungs- oder Einfügemodus!</translation>
    </message>
    <message>
        <location line="+134"/>
        <source>Ctrl+F4</source>
        <translation>Ctrl+F4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close Tab</source>
        <translation>Register schliessen</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+36"/>
        <source>Maximize Window</source>
        <translation>Fenster maximieren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Minimize Window</source>
        <translation>Fenster verkleinern</translation>
    </message>
    <message>
        <location line="-375"/>
        <source>Can not close window in edit or insert mode!</source>
        <translation>Das Fenster kann im Bearbeitungs-/Einfügemodus nicht geschlossen werden!</translation>
    </message>
    <message>
        <location line="-49"/>
        <source>Setting up Calendar Event Interface...</source>
        <translation>Kalendereintrag wird vorbereitet...</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Setting up Calendar Interface...</source>
        <translation>Kalender wird vorbereitet...</translation>
    </message>
    <message>
        <location line="+234"/>
        <source>Can not close window </source>
        <translation>Fenster kann nicht geschlossen werden </translation>
    </message>
    <message>
        <location line="-392"/>
        <source>Setting up Projects Interface...</source>
        <translation>Projekt-Arbeitsumgebung wird konfiguriert...</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Setting up Contacts Interface...</source>
        <translation>Kontakt-Arbeitsumgebung wird konfiguriert...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Setting up Personal Desktop Interface...</source>
        <translation>Persönlicher Desktop wird konfiguriert...</translation>
    </message>
</context>
<context>
    <name>Fui_Calendar</name>
    <message>
        <location filename="fui_calendar.cpp" line="+1917"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Resource</source>
        <translation>Ressource</translation>
    </message>
    <message>
        <location line="-1836"/>
        <source>Hide Calendar</source>
        <translation>Kalender ausblenden</translation>
    </message>
    <message>
        <location line="-52"/>
        <source> Calendar</source>
        <translation>SOKRATES® Kalender</translation>
    </message>
    <message>
        <source>Hour</source>
        <translation type="obsolete">Stunde</translation>
    </message>
    <message>
        <source>Half Hour</source>
        <translation type="obsolete">Halbe Stunde</translation>
    </message>
    <message>
        <source>Quarter Hour</source>
        <translation type="obsolete">Viertelstunge</translation>
    </message>
    <message>
        <source>Ten Minutes</source>
        <translation type="obsolete">Zehn Minuten</translation>
    </message>
    <message>
        <source>Six Minutes</source>
        <translation type="obsolete">Sechs Minuten</translation>
    </message>
    <message>
        <source>Five Minutes</source>
        <translation type="obsolete">Fünf Minuten</translation>
    </message>
    <message>
        <location line="+2279"/>
        <location line="+25"/>
        <location line="+11"/>
        <source>Delete Event</source>
        <translation>Eintrag löschen</translation>
    </message>
    <message>
        <location line="-36"/>
        <location line="+36"/>
        <source>Do you really want to delete this item?</source>
        <translation>Wollen Sie den Eintrag wirklich löschen?</translation>
    </message>
    <message>
        <location line="+290"/>
        <source>Monday</source>
        <translation>Montag</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tuesday</source>
        <translation>Dienstag</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Wednesday</source>
        <translation>Mittwoch</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Thursday</source>
        <translation>Donnerstag</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Friday</source>
        <translation>Freitag</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Saturday</source>
        <translation>Samstag</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sunday</source>
        <translation>Sonntag</translation>
    </message>
    <message>
        <source>Week </source>
        <translation type="obsolete">Woche </translation>
    </message>
    <message>
        <location line="-2561"/>
        <source>Reload Calendar</source>
        <translation>Kalender neu laden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Calendar View</source>
        <translation>Kalenderansicht speichern</translation>
    </message>
    <message>
        <location line="+2562"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>January</source>
        <translation>Januar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>February</source>
        <translation>Februar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>March</source>
        <translation>März</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>April</source>
        <translation>April</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>May</source>
        <translation>Mai</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>June</source>
        <translation>Juni</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>July</source>
        <translation>Juli</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>August</source>
        <translation>August</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>September</source>
        <translation>September</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>October</source>
        <translation>Oktober</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>November</source>
        <translation>November</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>December</source>
        <translation>Dezember</translation>
    </message>
    <message>
        <location line="-354"/>
        <source>Do you really want to delete </source>
        <translation>Wollen Sie wirklich </translation>
    </message>
    <message>
        <location line="+2"/>
        <source> items?</source>
        <translation>Einträge löschen?</translation>
    </message>
    <message>
        <location line="-2232"/>
        <source>Show Assignations</source>
        <translation>Zuweisungen anzeigen</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Delete Selected Calendar View</source>
        <translation>Ausgewählte Kalenderansicht speichern</translation>
    </message>
    <message>
        <location line="+2140"/>
        <location line="+1207"/>
        <location line="+13"/>
        <location line="+32"/>
        <location line="+15"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-1267"/>
        <source>Please select one View first!</source>
        <translation>Bitte zuerst Ansicht wählen!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete View</source>
        <translation>Ansicht löschen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Do you really want to delete this view?</source>
        <translation>Wollen Sie diese Ansicht wirklich löschen?</translation>
    </message>
    <message>
        <location line="+1007"/>
        <location line="+385"/>
        <source>Owner</source>
        <translation>Besitzer</translation>
    </message>
    <message>
        <location line="-383"/>
        <location line="+21"/>
        <location line="+21"/>
        <location line="+22"/>
        <location line="+321"/>
        <location line="+22"/>
        <location line="+19"/>
        <location line="+20"/>
        <source>assigned</source>
        <translation>zugewiesen</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+360"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-3738"/>
        <source>SOKRATES</source>
        <translation>SOKRATES</translation>
    </message>
</context>
<context>
    <name>Fui_CalendarClass</name>
    <message>
        <location filename="fui_calendar.ui" line="+14"/>
        <source>Fui_Calendar</source>
        <translation>Fui_Calendar</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>User Calendar</source>
        <translation>Benutzer-Kalender</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Project Calendar</source>
        <translation>Projekt-Kalender</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Contact Calendar</source>
        <translation>Kontakt-Kalender</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Resource Calendar</source>
        <translation>Ressourcen-Kalender</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+17"/>
        <location line="+11"/>
        <location line="+7"/>
        <location line="+129"/>
        <location line="+23"/>
        <location line="+7"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-187"/>
        <location line="+35"/>
        <location line="+235"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
    <message>
        <source>Time Interval:</source>
        <translation type="obsolete">Zeitintervall:</translation>
    </message>
    <message>
        <location line="-354"/>
        <source>Show Tasks</source>
        <translation>Aufgaben anzeigen</translation>
    </message>
    <message>
        <location line="+304"/>
        <source>Select View</source>
        <translation>Ansicht auswählen</translation>
    </message>
</context>
<context>
    <name>GridFilterSelectionWizPageClass</name>
    <message>
        <location filename="gridfilterselectionwizpage.ui" line="+13"/>
        <source>GridFilterSelectionWizPage</source>
        <translation>GridFilterSelectionWizPage</translation>
    </message>
</context>
<context>
    <name>ImportCenter</name>
    <message>
        <location filename="importcenter.cpp" line="+66"/>
        <source>Skype Contacts</source>
        <translation>Skype Kontakte</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Outlook</source>
        <translation>Outlook</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Text File</source>
        <translation>Textdatei</translation>
    </message>
</context>
<context>
    <name>ImportCenterClass</name>
    <message>
        <location filename="importcenter.ui" line="+13"/>
        <source>Import Center</source>
        <translation>Import-Zentrale</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Import all data from various sources&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Alle Daten aus verschiedenen Quellen importieren&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Import Contacts From Txt File</source>
        <translation>Kontakte importieren aus Textdatei</translation>
    </message>
    <message>
        <location line="-37"/>
        <source>Import Contacts From Skype</source>
        <translation>Kontakte importieren aus Skype</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Import Contacts From Outlook</source>
        <translation>Kontakte importieren aus Outlook</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Import Projects From Txt File</source>
        <translation>Projekt importieren aus Textdatei</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Import Emails From Outlook</source>
        <translation>E-Mails importieren aus Outlook</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Finish</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location line="-121"/>
        <source>Picture</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Import data&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Daten importieren&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>MainCommandBarClass</name>
    <message>
        <source>MainCommandBar</source>
        <translation type="obsolete">MainCommandBar</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="obsolete">&amp;Bearbeiten</translation>
    </message>
    <message>
        <source>&amp;Insert</source>
        <translation type="obsolete">E&amp;infügen</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="obsolete">&amp;Löschen</translation>
    </message>
    <message>
        <source>Co&amp;py</source>
        <translation type="obsolete">&amp;Kopieren</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="maincommandbar.ui" line="+69"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Insert</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>MainEntitySelectionController</name>
    <message>
        <source>Code</source>
        <translation type="obsolete">Code</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>Dept. Name</source>
        <translation type="obsolete">Abt.-Name</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Benutzername</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Art</translation>
    </message>
    <message>
        <source>Role NM Pairs</source>
        <translation type="obsolete">Rolle in Paarbeziehung</translation>
    </message>
    <message>
        <source>Contact</source>
        <translation type="obsolete">Kontakt</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Application Name</source>
        <translation type="obsolete">Applikationsname</translation>
    </message>
    <message>
        <source>Template Name</source>
        <translation type="obsolete">Vorlagenname</translation>
    </message>
    <message>
        <source>Tree</source>
        <translation type="obsolete">Baum</translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="obsolete">Gruppe</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation type="obsolete">Gültig ab</translation>
    </message>
    <message>
        <source>Valid To</source>
        <translation type="obsolete">Gültig bis</translation>
    </message>
</context>
<context>
    <name>MultiAssignDialogClass</name>
    <message>
        <location filename="multiassigndialog.ui" line="+13"/>
        <source>Multiple Assignment</source>
        <translation>Mehrfachzuweisung</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
</context>
<context>
    <name>NMRXRelationWidget</name>
    <message>
        <location filename="nmrxrelationwidget.cpp" line="+143"/>
        <location line="+137"/>
        <source>Edit Assignment</source>
        <translation>Zuweisung bearbeiten</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+132"/>
        <source>Show Assignment</source>
        <translation>Zuweisung anzeigen</translation>
    </message>
    <message>
        <location line="-131"/>
        <source>Remove Assignment</source>
        <translation>Zuweisung entfernen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+148"/>
        <source>Show Details</source>
        <translation>Zeige Details</translation>
    </message>
    <message>
        <location line="-118"/>
        <source>Assign </source>
        <translation>zuweisen </translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Del</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Remove All Assignments</source>
        <translation>Alle Zuweisungen entfernen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>S&amp;ort</source>
        <translation>&amp;Sortieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location line="+107"/>
        <location line="+549"/>
        <location line="+16"/>
        <location line="+10"/>
        <location line="+41"/>
        <location line="+16"/>
        <location line="+10"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-545"/>
        <location line="+82"/>
        <location line="+66"/>
        <location line="+76"/>
        <location line="+61"/>
        <location line="+13"/>
        <location line="+62"/>
        <location line="+66"/>
        <location line="+11"/>
        <location line="+59"/>
        <location line="+68"/>
        <location line="+619"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-1101"/>
        <location line="+142"/>
        <source>User is not a Contact! No assignment possible!</source>
        <translation>Der Benutzer ist nicht als Kontakt erfasst. Keine Zuweisung möglich!</translation>
    </message>
    <message>
        <location line="+959"/>
        <source>Assignment already exists!</source>
        <translation>Die Zuweisung besteht bereits!</translation>
    </message>
    <message>
        <location line="-885"/>
        <location line="+62"/>
        <location line="+77"/>
        <location line="+127"/>
        <source>Please select an assignment!</source>
        <translation>Wählen Sie eine Zuweisung aus!</translation>
    </message>
    <message>
        <location line="-791"/>
        <source>Remove Selected Assignment</source>
        <translation>Ausgewählte Zuweisung löschen</translation>
    </message>
    <message>
        <location line="-128"/>
        <location line="+18"/>
        <source> Assign</source>
        <translation> zuweisen</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+25"/>
        <source>Send Invitations Now!</source>
        <translation>Einladung(en) jetzt senden!</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Show Assignee Calendar</source>
        <translation>Zugewiesenen Kalender anzeigen</translation>
    </message>
    <message>
        <location line="+1432"/>
        <source>Please confirm invitation send</source>
        <translation>Versenden von Einladungen bestätigen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Send Invitation to the selected user/contact only</source>
        <translation>Einladung nur an ausgewählte(n) Benutzer/Kontakt(e) senden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Send Invitation to all</source>
        <translation>Einladung an alle senden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-1072"/>
        <location line="+148"/>
        <location line="+137"/>
        <location line="+141"/>
        <location line="+70"/>
        <source>You do not have sufficient access rights to perform this operation!</source>
        <translation>Kein Zugriffsrecht!</translation>
    </message>
</context>
<context>
    <name>OptionsAndSettingsWidget</name>
    <message>
        <location filename="optionsandsettingswidget.cpp" line="+260"/>
        <source>Save changed settings before closing of press Cancel button!</source>
        <translation>Vor dem Schliessen müssen die geänderten Einstellungen gespeichert werden, oder andernfalls muss abgebrochen werden!</translation>
    </message>
    <message>
        <location line="-149"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Warning!</source>
        <translation>Warnung!</translation>
    </message>
</context>
<context>
    <name>OptionsAndSettingsWidgetClass</name>
    <message>
        <source>OptionsAndSettingsWidget</source>
        <translation type="obsolete">OptionsAndSettingsWidget</translation>
    </message>
    <message>
        <location filename="optionsandsettingswidget.ui" line="+113"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>Options_Contacts</name>
    <message>
        <location filename="options_contacts.cpp" line="+29"/>
        <location line="+11"/>
        <source>Modify View</source>
        <translation>Ansicht definieren</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+8"/>
        <source>Modify Address Schema</source>
        <translation>Adressschema bearbeiten</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>Options_ContactsClass</name>
    <message>
        <source>Options_Contacts</source>
        <translation type="obsolete">Options_Contacts</translation>
    </message>
    <message>
        <location filename="options_contacts.ui" line="+199"/>
        <source>Contact Address Formatting</source>
        <translation>Kontaktadresse formatieren</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Default Address Schema for Persons:</source>
        <translation>Default-Adressschema für Personen:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Default Address Schema for Organizations:</source>
        <translation>Default-Adressschema für Organisationen:</translation>
    </message>
    <message>
        <location line="-252"/>
        <source>Views</source>
        <translation>Ansichten</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Default Group List View:</source>
        <translation>Default-Gruppenansicht:</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Default Actual Contact List View:</source>
        <translation>Default-Ansicht für aktuelle Kontaktliste:</translation>
    </message>
    <message>
        <location line="-65"/>
        <source>Default Group Tree:</source>
        <translation>Default-Gruppenbaum:</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Exclude Contact Group:</source>
        <translation>Kontaktgruppe ausschliessen:</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Disable ZIP-Code Lookup using Web Service</source>
        <translation>Web-PLZ-Suche über Webservice deaktivieren</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Always Format Contact Address</source>
        <translation>Kontaktadresse automatisch formatieren</translation>
    </message>
</context>
<context>
    <name>Options_DocDefaults</name>
    <message>
        <location filename="options_docdefaults.cpp" line="+19"/>
        <source>Select Path</source>
        <translation>Pfad wählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Path</source>
        <translation>Pfad entfernen</translation>
    </message>
    <message>
        <location line="+136"/>
        <source>Default Directory For Temporary Documents</source>
        <translation>Default-Verzeichnis für temporäre Dokumente</translation>
    </message>
</context>
<context>
    <name>Options_DocDefaultsClass</name>
    <message>
        <source>Options_DocDefaults</source>
        <translation type="obsolete">Options_DocDefaults</translation>
    </message>
    <message>
        <location filename="options_docdefaults.ui" line="+37"/>
        <source>Document Storage Location</source>
        <translation type="unfinished">Fragen nach Speicherort: Lokal / Internet (Datenbank)</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Store File Documents Locally</source>
        <translation>Dokumente lokal speichern</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Store File Documents on Internet</source>
        <translation>Dokumente im Internet (Datenbank) speichern</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Default Directory For Temporary Documents (e.g. &apos;C:/Temp/Docs&apos;):</source>
        <translation>Defaultverzeichnis für temporäre Dokumente (z.B. &apos;C:/Temp/Docs&apos;):</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Document Name Formatting</source>
        <translation>Formatierung von Dokumentennamen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Logged User</source>
        <translation>Angemeldeter Benutzer</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Template Name</source>
        <translation>Vorlagenname</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>New Document Name Format NAME DATE USER: (e.g. &apos;MyDocument_01012008_MB&apos;)</source>
        <translation>Formatierung für Namen neuer Dokumente NAME DATUM BENUTZER: (z.B. &apos;MyDocument_01012008_MB&apos;)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enable parsing directory paths as document types</source>
        <translation>Dokumententypen aus Pfadnamen bestimmen</translation>
    </message>
    <message>
        <location line="-187"/>
        <source>Ask For File Document Storage: Local⁄Internet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Options_Organization</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
</context>
<context>
    <name>Options_OrganizationClass</name>
    <message>
        <source>Options_Organization</source>
        <translation type="obsolete">Options_Organization</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <source>Code:</source>
        <translation type="obsolete">Code:</translation>
    </message>
    <message>
        <location filename="options_organization.ui" line="+45"/>
        <source>Default Organization:</source>
        <translation>Default-Organisation:</translation>
    </message>
</context>
<context>
    <name>OutlookFolderPickerDlg</name>
    <message>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <source>Error getting method from MAPI dll!</source>
        <translation type="obsolete">Fehler beim Zugriff auf MAPI-DLL!</translation>
    </message>
</context>
<context>
    <name>OutlookFolderPickerDlgClass</name>
    <message>
        <source>Select Outlook Folder</source>
        <translation type="obsolete">Outlook-Verzeichnis wählen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Ok</translation>
    </message>
    <message>
        <source>Remember Setting</source>
        <translation type="obsolete">Einstellungen speichern</translation>
    </message>
</context>
<context>
    <name>PersonWidget</name>
    <message>
        <source>Assign roles to multiple Users</source>
        <translation type="obsolete">Rollen mehreren Benutzern zuweisen</translation>
    </message>
    <message>
        <location filename="personwidget.cpp" line="+24"/>
        <source>Role To User Assignment</source>
        <translation>Rolle einem Benutzer zuweisen</translation>
    </message>
    <message>
        <location line="+364"/>
        <source>Assign Roles To Multiple Users</source>
        <translation>Rollen mehreren Benutzern zuweisen</translation>
    </message>
</context>
<context>
    <name>PersonWidgetClass</name>
    <message>
        <location filename="personwidget.ui" line="+13"/>
        <source>Person Role Definition</source>
        <translation>Rollendefinition für Person</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Delete Role</source>
        <translation>Rolle löschen</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Save Changes</source>
        <translation>Änderungen speichern</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Role Description:</source>
        <translation>Rollenbeschreibung:</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>List of Roles:</source>
        <translation>Liste der Rollen:</translation>
    </message>
    <message>
        <location line="-118"/>
        <location line="+110"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location line="-85"/>
        <source>Assign Selected Roles To Multiple Users</source>
        <translation>Ausgewählte Rollen mehreren Benutzern zuweisen</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>List of Users:</source>
        <translation>Benutzerliste:</translation>
    </message>
</context>
<context>
    <name>PhoneSelectorDlg</name>
    <message>
        <source>Phone</source>
        <translation type="obsolete">Telefonnummer</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Art</translation>
    </message>
</context>
<context>
    <name>PhoneSelectorDlgClass</name>
    <message>
        <source>Select Phone</source>
        <translation type="obsolete">Telefonnummer wählen</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>PostponeTaskDialog</name>
    <message>
        <location filename="postponetaskdialog.cpp" line="+10"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>min</source>
        <translation>Min.</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>h</source>
        <translation>Std.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>days</source>
        <translation>Tage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>weeks</source>
        <translation>Wochen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <source>months</source>
        <translation>Monate</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>year</source>
        <translation>Jahr</translation>
    </message>
</context>
<context>
    <name>PostponeTaskDialogClass</name>
    <message>
        <location filename="postponetaskdialog.ui" line="+14"/>
        <source>Postpone a Task</source>
        <translation>Aufgabe verschieben</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start Date/Time:</source>
        <translation>Beginn:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Due Date/Time:</source>
        <translation>Fällig:</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Shift Task by:</source>
        <translation>Aufgabe verschieben um:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>QCW_Base</name>
    <message>
        <location filename="qcw_base.cpp" line="+281"/>
        <source>Contact Entry Assistant</source>
        <translation>Kontakt-Eingabeassistent</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Contact already exists in database:</source>
        <translation>Der Kontakt besteht schon in der Datenbank:</translation>
    </message>
    <message>
        <location line="-273"/>
        <location line="+274"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-274"/>
        <location line="+274"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="-274"/>
        <location line="+274"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-274"/>
        <source>You changed the address of %1. Do you want this change to be made automatically to %2 addresses of people in this organization?</source>
        <translation>Die Adresse von %1 wurde geändert. Wollen Sie diese Änderung auch automatisch in allen passenden Adressen (%2) von Personen dieser Organisation nachtragen?</translation>
    </message>
</context>
<context>
    <name>QCW_LargeWidget</name>
    <message>
        <source>Quick Entry Contact</source>
        <translation type="obsolete">Kontakt-Eingabeassistent</translation>
    </message>
    <message>
        <source>Contact Entry Assistant</source>
        <translation type="obsolete">Kontakt-Eingabeassistent</translation>
    </message>
    <message>
        <location filename="qcw_largewidget.cpp" line="+21"/>
        <source>Copy organization address to the contact address</source>
        <translation>Adresse aus Organisation übernehmen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear All</source>
        <translation>Alles löschen</translation>
    </message>
    <message>
        <location line="+388"/>
        <source>Copy Address</source>
        <translation>Adresse in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Copy organization address to the contact address?</source>
        <translation>Adresse aus Organisation übernehmen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>QCW_LargeWidgetClass</name>
    <message>
        <location filename="qcw_largewidget.ui" line="+20"/>
        <source>QCW_LargeWidget</source>
        <translation></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+91"/>
        <location line="+39"/>
        <location line="+29"/>
        <location line="+22"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-140"/>
        <source>Male</source>
        <translation>Herr</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Female</source>
        <translation>Frau</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Middle Name</source>
        <translation>Zweiter Vorname</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>More</source>
        <translation>Mehr</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+262"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-248"/>
        <source>Debtor</source>
        <translation>Debitor</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Customer Code:</source>
        <translation>Kundencode:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Internal Debtor</source>
        <translation>Interner Debitor</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Debtor Code:</source>
        <translation>Debitorencode:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Propose</source>
        <translation>Vorschlag</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Debtor Account:</source>
        <translation>Debitorenkonto:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Payment Period:</source>
        <translation>Zahlfrist (in Tagen):</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
</context>
<context>
    <name>QCW_LeftSelector</name>
    <message>
        <location filename="qcw_leftselector.cpp" line="+45"/>
        <source>Drop items here to assign them to a contact or project: Files, Other Contacts, Emails, Projects, vCards,...</source>
        <translation>Elemente hierherziehen und Kontakt oder Projekt zuweisen: Dateien, andere Kontakte, E-Mails, Projekte, vCards,...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste text here to assign it to the actual contact or project: Phone Numbers, Addresses, vCards, etc,</source>
        <translation>Text hier einfügen und aktuellem Kontakt oder Projekt zuweisen: Telefonnummern, Adressen, vCards, usw.,</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Add Selected Text As Internet Address to the Contact</source>
        <translation>Markierten Text als Internetadresse dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Selected Text As Address to the Contact</source>
        <translation>Markierten Text als Adresse dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Selected Text As Email Address to the Contact</source>
        <translation>Markierten Text als E-Mail-Adresse dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Selected Text As Phone Number to the Contact</source>
        <translation>Markierten Text als Telefonnummer dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Selected Text As Contact Name</source>
        <translation>Markierten Text als Name dem Kontakt zuweisen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Selected Text As Contact Organization</source>
        <translation>Markierten Text als Organisation dem Kontakt zweisen</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Clear List</source>
        <translation>Liste leeren</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Remove Selected Contacts From List</source>
        <translation>Gewählte Kontakte aus Liste entfernen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>S&amp;ort</source>
        <translation>&amp;Sortieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location line="-33"/>
        <source>Analyze the selected text as form data</source>
        <translation>Ausgewählten Text als Formulardaten analysieren</translation>
    </message>
</context>
<context>
    <name>QCW_LeftSelectorClass</name>
    <message>
        <location filename="qcw_leftselector.ui" line="+13"/>
        <source>QCW_LeftSelector</source>
        <translation></translation>
    </message>
    <message>
        <location line="+148"/>
        <source>1.</source>
        <translation>1.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Copy your contact data from emails, websites etc. to the text field below.</source>
        <translation>Kopieren Sie Ihre Kontaktdaten aus E-Mails, Webseiten usw. ins untenstehende Feld.</translation>
    </message>
    <message>
        <location line="+290"/>
        <source>2.</source>
        <translation>2.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Press this button to automatically analyze the contact data and to distribute it to the fields on the right!</source>
        <translation>Automatische Analyse der Kontaktdaten im Textfeld und Verteilung auf die einzelnen Felder!</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
</context>
<context>
    <name>QCW_SmallWidget</name>
    <message>
        <source>Contact Entry Assistant</source>
        <translation type="obsolete">Kontakt-Eingabeassistent</translation>
    </message>
    <message>
        <location filename="qcw_smallwidget.cpp" line="+20"/>
        <source>Copy organization address to the contact address</source>
        <translation>Adresse aus Organisation übernehmen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear All</source>
        <translation>Alles löschen</translation>
    </message>
    <message>
        <location line="+367"/>
        <source>Copy Address</source>
        <translation>Adresse in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Copy organization address to the contact address?</source>
        <translation>Adresse aus Organisation übernehmen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>QCW_SmallWidgetClass</name>
    <message>
        <location filename="qcw_smallwidget.ui" line="+19"/>
        <source>QCW_SmallWidget</source>
        <translation></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+42"/>
        <location line="+43"/>
        <location line="+55"/>
        <location line="+45"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-132"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Middle Name</source>
        <translation>Zweiter Vorname</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Male</source>
        <translation>Herr</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Female</source>
        <translation>Frau</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Communication</source>
        <translation>Kommunikation</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>More</source>
        <translation>Mehr</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>QC_AccessRightsFrameClass</name>
    <message>
        <location filename="qc_accessrightsframe.ui" line="+32"/>
        <source>QC_AccessRightsFrame</source>
        <translation></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Access Rights</source>
        <translation>Zugriffsrechte</translation>
    </message>
    <message>
        <source>Private Contact</source>
        <translation type="obsolete">Privatkontakt</translation>
    </message>
    <message>
        <source>Others can view</source>
        <translation type="obsolete">Für andere sichtbar</translation>
    </message>
    <message>
        <source>Others can write</source>
        <translation type="obsolete">Von anderen modifizierbar</translation>
    </message>
    <message>
        <source>Private</source>
        <translation type="obsolete">Privat</translation>
    </message>
    <message>
        <source>Others can View</source>
        <translation type="obsolete">Für andere sichtbar</translation>
    </message>
    <message>
        <source>Others can Write</source>
        <translation type="obsolete">Von anderen modifizierbar</translation>
    </message>
</context>
<context>
    <name>QC_AddressFrameClass</name>
    <message>
        <location filename="qc_addressframe.ui" line="+32"/>
        <source>QC_AddressFrame</source>
        <translation></translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+123"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+49"/>
        <location line="+29"/>
        <location line="+49"/>
        <location line="+49"/>
        <location line="+49"/>
        <location line="+49"/>
        <location line="+49"/>
        <location line="+29"/>
        <location line="+78"/>
        <location line="+38"/>
        <location line="+49"/>
        <location line="+49"/>
        <location line="+35"/>
        <location line="+91"/>
        <location line="+48"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-712"/>
        <source>First/Middle Name</source>
        <translation>Vorname/2. Vorname</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Organization Name</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Organization Name 2</source>
        <translation>Organisation 2</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Street</source>
        <translation>Strasse</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>PO Box / PO Zip</source>
        <translation>Postfach (+PLZ)</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>ZIP / Town</source>
        <translation>PLZ / Ort</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>State</source>
        <translation>Region</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Country Code / Name</source>
        <translation>Landescode / Name</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>Formatted Address</source>
        <translation>Formatierte Adresse</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Format Address</source>
        <translation>Adresse formatieren</translation>
    </message>
    <message>
        <location line="-336"/>
        <source>Web Lookup ZIP/Town</source>
        <translation>PLZ/Ort im Web suchen</translation>
    </message>
    <message>
        <location line="-481"/>
        <source>Default Address</source>
        <translation>Default-Adresse</translation>
    </message>
</context>
<context>
    <name>QC_EmailFrameClass</name>
    <message>
        <location filename="qc_emailframe.ui" line="+26"/>
        <source>QC_EmailFrame</source>
        <translation></translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+54"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Default Email</source>
        <translation>Default-E-Mail</translation>
    </message>
</context>
<context>
    <name>QC_EntitySelector</name>
    <message>
        <location filename="qc_entityselector.cpp" line="+11"/>
        <source>of</source>
        <translation>von</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+5"/>
        <source>/</source>
        <translation>/</translation>
    </message>
</context>
<context>
    <name>QC_EntitySelectorClass</name>
    <message>
        <location filename="qc_entityselector.ui" line="+31"/>
        <source>QC_EntitySelector</source>
        <translation></translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+38"/>
        <location line="+25"/>
        <location line="+25"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QC_InternetFrameClass</name>
    <message>
        <location filename="qc_internetframe.ui" line="+32"/>
        <source>QC_InternetFrame</source>
        <translation></translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+48"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Default Internet</source>
        <translation>Default Internet</translation>
    </message>
</context>
<context>
    <name>QC_LanguageFrameClass</name>
    <message>
        <location filename="qc_languageframe.ui" line="+26"/>
        <source>QC_LanguageFrame</source>
        <translation></translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
</context>
<context>
    <name>QC_MoreFrameClass</name>
    <message>
        <location filename="qc_moreframe.ui" line="+25"/>
        <source>QC_MoreFrame</source>
        <translation></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Nickname</source>
        <translation>Rufname</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Birthday</source>
        <translation>Geburtstag</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Department</source>
        <translation>Abteilung</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+26"/>
        <location line="+26"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Profession</source>
        <translation>Beruf</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Language Code</source>
        <translation>Sprachcode</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>No Emails</source>
        <translation>Keine E-Mails</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>External Code</source>
        <translation>Externer Code</translation>
    </message>
</context>
<context>
    <name>QC_PhoneFrameClass</name>
    <message>
        <location filename="qc_phoneframe.ui" line="+32"/>
        <source>QC_PhoneFrame</source>
        <translation></translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Phone Numbers</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Business Central</source>
        <translation>Geschäft Zentrale</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+34"/>
        <location line="+52"/>
        <location line="+34"/>
        <location line="+52"/>
        <location line="+34"/>
        <location line="+52"/>
        <location line="+34"/>
        <location line="+52"/>
        <location line="+34"/>
        <location line="+52"/>
        <location line="+34"/>
        <location line="+52"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-446"/>
        <source>Business Direct</source>
        <translation>Geschäft Direkt</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Business Mobile</source>
        <translation>Geschäft Mobile</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Fax</source>
        <translation>Fax</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Private Mobile</source>
        <translation>Privat Mobile</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Skype User Name</source>
        <translation>Skype Benutzername</translation>
    </message>
    <message>
        <location line="-469"/>
        <location line="+86"/>
        <location line="+86"/>
        <location line="+86"/>
        <location line="+86"/>
        <location line="+86"/>
        <source>Format Phone Number</source>
        <translation>Telefonnummer formatieren</translation>
    </message>
    <message>
        <location line="-406"/>
        <location line="+86"/>
        <location line="+86"/>
        <location line="+86"/>
        <location line="+86"/>
        <location line="+86"/>
        <source>Default Phone</source>
        <translation>Default-Telefonnummer</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="wizardregistration.cpp" line="+31"/>
        <source>Select Report</source>
        <translation>Auswahl Auswertung</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Department Selection</source>
        <translation>Auswahl Abteilung</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+13"/>
        <location line="+55"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+13"/>
        <source>Select report destination</source>
        <translation>Auswahl Druckdestination</translation>
    </message>
    <message>
        <location line="-104"/>
        <location line="+68"/>
        <location line="+16"/>
        <location line="+17"/>
        <source>Select Contacts</source>
        <translation>Auswahl Kontakte</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Personal Data</source>
        <translation>Persönlich</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Communication Data</source>
        <translation>Kommunikation</translation>
    </message>
    <message>
        <source>Failed to load MAPIex.dll (error:%1)</source>
        <translation type="obsolete">Fehler beim Zugriff auf MAPI-DLL: %1</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Information</translation>
    </message>
    <message>
        <source>Application will close now. Server will restart. Restart application in few minutes to start using new database.</source>
        <translation type="obsolete">Die Anwendung wird nun beendet, und der Server wird neu gestartet. Bitte starten Sie die Anwendung wieder in ein paar Minuten, um die neue Datenbank zu verwenden.</translation>
    </message>
    <message>
        <location filename="communicationactions.cpp" line="+169"/>
        <location line="+261"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-261"/>
        <location line="+261"/>
        <source>Contact does not have any web address!</source>
        <translation>Für den oder die ausgewählten Kontakt(e) sind keine Internetadressen vorhanden!</translation>
    </message>
    <message>
        <source>Selector</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>User Selector</source>
        <translation type="obsolete">Benutzerauswahl</translation>
    </message>
    <message>
        <source>Contact Selector</source>
        <translation type="obsolete">Kontaktauswahl</translation>
    </message>
    <message>
        <source>Project Selector</source>
        <translation type="obsolete">Projektauswahl</translation>
    </message>
    <message>
        <location filename="startupwizard.cpp" line="+52"/>
        <source>Re-login</source>
        <translation>Neu anmelden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password successfully changed, please re-login</source>
        <translation>Passwort geändert, bitte neu anmelden</translation>
    </message>
    <message>
        <source>Select backup directory</source>
        <translation type="obsolete">Backup-Verzeichnis wählen</translation>
    </message>
    <message>
        <source>External Code</source>
        <translation type="obsolete">Externer Code</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Beschreibung</translation>
    </message>
    <message>
        <source>Ancient Last Name</source>
        <translation type="obsolete">Ehemaliger Nachname</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation type="obsolete">Nachname</translation>
    </message>
    <message>
        <source>First Name</source>
        <translation type="obsolete">Vorname</translation>
    </message>
    <message>
        <source>Middle Name</source>
        <translation type="obsolete">Zweiter Vorname</translation>
    </message>
    <message>
        <source>Department</source>
        <translation type="obsolete">Abteilung</translation>
    </message>
    <message>
        <source>Day of birth</source>
        <translation type="obsolete">Geburtstag</translation>
    </message>
    <message>
        <source>Organization</source>
        <translation type="obsolete">Organisation</translation>
    </message>
    <message>
        <source>Organization Short Name</source>
        <translation type="obsolete">Kurzname der Organisation</translation>
    </message>
    <message>
        <source>Organization Foundation Date</source>
        <translation type="obsolete">Gründungsdatum</translation>
    </message>
    <message>
        <source>Profession</source>
        <translation type="obsolete">Beruf</translation>
    </message>
    <message>
        <source>Function</source>
        <translation type="obsolete">Funktion</translation>
    </message>
    <message>
        <source>Male/Female</source>
        <translation type="obsolete">Mann/Frau</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="obsolete">Ort</translation>
    </message>
    <message>
        <source>Valid From</source>
        <translation type="obsolete">Gültig ab</translation>
    </message>
    <message>
        <source>Valid To</source>
        <translation type="obsolete">Gültig bis</translation>
    </message>
    <message>
        <source>Phones</source>
        <translation type="obsolete">Telefonnummern</translation>
    </message>
    <message>
        <source>Emails</source>
        <translation type="obsolete">E-Mails</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Adresse</translation>
    </message>
    <message>
        <source>Internet</source>
        <translation type="obsolete">Internet</translation>
    </message>
    <message>
        <source>Any Phone Number</source>
        <translation type="obsolete">eine Telephonnummer</translation>
    </message>
    <message>
        <source>Any Net Address</source>
        <translation type="obsolete">eine Internetadresse</translation>
    </message>
    <message>
        <location filename="sidebarfui_base.cpp" line="+169"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Invalid import file</source>
        <translation type="obsolete">Ungültige Import-Datei</translation>
    </message>
    <message>
        <source>Invalid import file. Please check values of first field. Import aborted!</source>
        <translation type="obsolete">Ungültige Import-Datei. Überprüfen Sie die Werte im ersten Feld. Import abgebrochen!</translation>
    </message>
    <message>
        <location filename="calendarhelper.cpp" line="+49"/>
        <location filename="communicationactions.cpp" line="-274"/>
        <location filename="table_contacttype.cpp" line="+290"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Password not match!</source>
        <translation type="obsolete">Passwort stimmt nicht überein!</translation>
    </message>
    <message>
        <source>Hide All</source>
        <translation type="obsolete">Alle verstecken</translation>
    </message>
    <message>
        <source>Unread</source>
        <translation type="obsolete">Ungelesen</translation>
    </message>
    <message>
        <source>Unassigned</source>
        <translation type="obsolete">Nicht zugewiesen</translation>
    </message>
    <message>
        <source>Missed</source>
        <translation type="obsolete">Verpasst</translation>
    </message>
    <message>
        <source>Scheduled</source>
        <translation type="obsolete">Geplant</translation>
    </message>
    <message>
        <source>Due</source>
        <translation type="obsolete">Fällig</translation>
    </message>
    <message>
        <source>Templates</source>
        <translation type="obsolete">Vorlagen</translation>
    </message>
    <message>
        <source>Last</source>
        <translation type="obsolete">Letzte</translation>
    </message>
    <message>
        <location filename="communicationactions.cpp" line="-64"/>
        <location line="+259"/>
        <source>Send Email</source>
        <translation>E-Mail senden</translation>
    </message>
    <message>
        <location line="-210"/>
        <location line="+265"/>
        <source>Create New Document</source>
        <translation>Neues Dokument erstellen</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation type="obsolete">Speichern unter</translation>
    </message>
    <message>
        <source>The File </source>
        <translation type="obsolete">Datei </translation>
    </message>
    <message>
        <source>Template file  </source>
        <translation type="obsolete">Vorlagendatei  </translation>
    </message>
    <message>
        <source>Copy Failed!</source>
        <translation type="obsolete">Kopieren misslungen!</translation>
    </message>
    <message>
        <source>The Local File </source>
        <translation type="obsolete">Lokale Datei </translation>
    </message>
    <message>
        <source>Load Original And Modify</source>
        <translation type="obsolete">Original laden und bearbeiten</translation>
    </message>
    <message>
        <source>Load Copy to View Only</source>
        <translation type="obsolete">Kopie zur Ansicht öffnen</translation>
    </message>
    <message>
        <source> Cancel </source>
        <translation type="obsolete"> Abbrechen </translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <source>Check out not possible: Please check-in a document using &apos;Edit&apos; before you can open it!</source>
        <translation type="obsolete">Auschecken nicht möglich: Bitte checken Sie ein Dokument mittels &apos;Bearbeiten&apos; in die Datenbank ein bevor Sie es öffnen können!</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <source>Automated Temporary Storage mechanism failed to determine Document check out location. Choose path for check-out location?</source>
        <translation type="obsolete">Der Automatismus für die lokale Speicherung von Dokumenten konnte kein temporäres Verzeichnis für das Auschecken bestimmen. Verzeichnispfad auswählen?</translation>
    </message>
    <message>
        <source>Failed to create document from template! To create Local Or Internet Document, Template must be either Local or Internet document type!</source>
        <translation type="obsolete">Aus der Vorlage konnte kein Dokument erzeugt werden!</translation>
    </message>
    <message>
        <source>File: </source>
        <translation type="obsolete">Datei: </translation>
    </message>
    <message>
        <source>Save Changes</source>
        <translation type="obsolete">Änderungen speichern</translation>
    </message>
    <message>
        <source>There are some checked out and modified documents. Do you want to check in all modified documents?</source>
        <translation type="obsolete">Es gibt lokal ausgecheckte Dokumente in Bearbeitung. Sollen alle momentan bearbeiteten Dokumente auf den Datenbankserver eingecheckt werden? </translation>
    </message>
    <message>
        <source>Processed </source>
        <translation type="obsolete">Verarbeitet </translation>
    </message>
    <message>
        <source>Error getting method from MAPI dll!</source>
        <translation type="obsolete">Fehler beim Zugriff auf MAPI-DLL!</translation>
    </message>
    <message>
        <source>Error opening email!</source>
        <translation type="obsolete">Fehler beim Öffnen einer E-Mail!</translation>
    </message>
    <message>
        <source>Outlook Mail Send function is only available on Windows!</source>
        <translation type="obsolete">Das Versenden von E-Mails über Outlook ist nur unter Windows verfügbar!</translation>
    </message>
    <message>
        <source>Email: </source>
        <translation type="obsolete">E-Mail: </translation>
    </message>
    <message>
        <source> is already assigned to the other project. Continue with operation and reassign it to the current project?</source>
        <translation type="obsolete"> ist bereits einem anderen Projekt zugewiesen. Weiterfahren und neu stattdessen dem aktuellen Projekt zuweisen?</translation>
    </message>
    <message>
        <source>Project Assignment</source>
        <translation type="obsolete">Projektzuweisung</translation>
    </message>
    <message>
        <source>Voice Call is already assigned to the other project. Continue with operation and reassign it to the current project?</source>
        <translation type="obsolete">Der Anruf ist bereits einem anderen Projekt zugewiesen. Weiterfahren und ihn stattdessen neu dem aktuellen Projekt zuweisen?</translation>
    </message>
    <message>
        <location filename="qcw_leftselector.cpp" line="+45"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <source>From Internet</source>
        <translation type="obsolete">Aus Internet</translation>
    </message>
    <message>
        <source>Find In TwixTel</source>
        <translation type="obsolete">Auf TwixTel suchen</translation>
    </message>
    <message>
        <source>Format Address</source>
        <translation type="obsolete">Adresse formatieren</translation>
    </message>
    <message>
        <source>Fill Fields From Address</source>
        <translation type="obsolete">Felder aus Adresse erzeugen</translation>
    </message>
    <message>
        <source>Modify Address Schema</source>
        <translation type="obsolete">Adressschema bearbeiten</translation>
    </message>
    <message>
        <source>Enter Address</source>
        <translation type="obsolete">Adresse erfassen</translation>
    </message>
    <message>
        <location filename="table_contactaddresstypes.cpp" line="+143"/>
        <source>Address type must be set!</source>
        <translation>Ein Adresstyp muss erfasst werden!</translation>
    </message>
    <message>
        <source>Not yet implemented</source>
        <translation type="obsolete">Noch nicht implementiert</translation>
    </message>
    <message>
        <source>Schema name is mandatory field</source>
        <translation type="obsolete">Name für Schema zwingend</translation>
    </message>
    <message>
        <source>Invalid Schema, count of open/closed brackets does not match!</source>
        <translation type="obsolete">Schema ungültig, oder die eckigen Klammern sind falsch gesetzt!</translation>
    </message>
    <message>
        <source>Do you really want to delete the selected schema from the database</source>
        <translation type="obsolete">Wollen Sie wirklich das ausgewählte Schema endgültig aus der Datenbank löschen</translation>
    </message>
    <message>
        <source>Build Actual Contact List</source>
        <translation type="obsolete">Aktuelle Kontaktliste aufbauen</translation>
    </message>
    <message>
        <source>Journal Entry</source>
        <translation type="obsolete">Journaleintrag</translation>
    </message>
    <message>
        <source>Add a Picture</source>
        <translation type="obsolete">Bild hinzufügen</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="obsolete">Gruppenname</translation>
    </message>
    <message>
        <source>Configure Column Order</source>
        <translation type="obsolete">Spaltenreihenfolge konfigurieren</translation>
    </message>
    <message>
        <source>Do you really want to delete the selected view from the database</source>
        <translation type="obsolete">Wollen Sie wirklich die ausgewählte Anischt endgültig aus der Datenbank löschen</translation>
    </message>
    <message>
        <source>Define a Relationship</source>
        <translation type="obsolete">Relation definieren</translation>
    </message>
    <message>
        <source>Change Password</source>
        <translation type="obsolete">Passwort wechseln</translation>
    </message>
    <message>
        <source>Select keyfile or registration utility</source>
        <translation type="obsolete">Keyfile (Lizenzschlüssel) oder das Registrierungsprogramm auswählen</translation>
    </message>
    <message>
        <source>Modify/Create</source>
        <translation type="obsolete">Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="obsolete">Hinzufügen</translation>
    </message>
    <message>
        <source>My Application Paths</source>
        <translation type="obsolete">Meine Anwendungspfade</translation>
    </message>
    <message>
        <source>Do you really want to delete this record permanently from the database?</source>
        <translation type="obsolete">Wollen Sie diesen Datensatz endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <source>Please first select an application!</source>
        <translation type="obsolete">Zuerst Anwendung auswählen!</translation>
    </message>
    <message>
        <source>Could not load template from application record!</source>
        <translation type="obsolete">Default-Vorlage für Anwendung kann nicht geladen werden!</translation>
    </message>
    <message>
        <source>Pick Existing Document</source>
        <translation type="obsolete">Bestehendes Dokument auswählen</translation>
    </message>
    <message>
        <source>Please first select an document!</source>
        <translation type="obsolete">Dokument zuerst auswählen!</translation>
    </message>
    <message>
        <source>Local File </source>
        <translation type="obsolete">Lokale Datei </translation>
    </message>
    <message>
        <source>Internet File</source>
        <translation type="obsolete">Internet-Datei</translation>
    </message>
    <message>
        <source>Note</source>
        <translation type="obsolete">Notiz</translation>
    </message>
    <message>
        <source>Paper Document</source>
        <translation type="obsolete">Papier-Dokument</translation>
    </message>
    <message>
        <source>Create Template(s) or Register Email(s)</source>
        <translation type="obsolete">Vorlage(n) erzeugen oder E-Mail(s) registrieren</translation>
    </message>
    <message>
        <source>New Template(s) From Dropped Email(s)</source>
        <translation type="obsolete">Neue Vorlage(n) aus E-Mail(s)</translation>
    </message>
    <message>
        <source>Register Email(s)</source>
        <translation type="obsolete">E-Mail(s) registrieren</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Speichern</translation>
    </message>
    <message>
        <source>The contents of this email is not yet actual, because the email is still being written or cancelled</source>
        <translation type="obsolete">Der Inhalt dieser Nachricht könnte nicht aktuell sein, da sie noch bearbeitet wird oder eventuell abgebrochen wurde</translation>
    </message>
    <message>
        <source>Sending message failed. Do you wish to save email and exit without sending?</source>
        <translation type="obsolete">Die Nachricht konnte nicht vesendet werden. Wollen Sie sie speichern ohne sie zu versenden?</translation>
    </message>
    <message>
        <source>Template Email can not be scheduled!</source>
        <translation type="obsolete">E-Mail-Vorlagen können nicht als Aufgaben definiert werden!</translation>
    </message>
    <message>
        <source>Choose application</source>
        <translation type="obsolete">Applikation wählen</translation>
    </message>
    <message>
        <source>Attachment with this name already exists!</source>
        <translation type="obsolete">Ein Anhang dieses Namens existiert schon!</translation>
    </message>
    <message>
        <source>Please select one attachment!</source>
        <translation type="obsolete">Anhang wählen!</translation>
    </message>
    <message>
        <source>Cost Center: </source>
        <translation type="obsolete">Kostenstelle:</translation>
    </message>
    <message>
        <source>Cost Center</source>
        <translation type="obsolete">Kostenstelle</translation>
    </message>
    <message>
        <source>Department: </source>
        <translation type="obsolete">Abteilung:</translation>
    </message>
    <message>
        <source>Organization: </source>
        <translation type="obsolete">Organisation:</translation>
    </message>
    <message>
        <source>New password set</source>
        <translation type="obsolete">Neues Passwort gesetzt</translation>
    </message>
    <message>
        <source>Password: </source>
        <translation type="obsolete">Passwort:</translation>
    </message>
    <message>
        <source>User: </source>
        <translation type="obsolete">Benutzer:</translation>
    </message>
    <message>
        <source>User</source>
        <translation type="obsolete">Benutzer</translation>
    </message>
    <message>
        <source>Document types</source>
        <translation type="obsolete">Dokumentenarten</translation>
    </message>
    <message>
        <source>Email types</source>
        <translation type="obsolete">E-Mail-Typen</translation>
    </message>
    <message>
        <source>Communication Categories: </source>
        <translation type="obsolete">Kommunikationskategorien: </translation>
    </message>
    <message>
        <source>Communication Categories</source>
        <translation type="obsolete">Kommunikationskategorien</translation>
    </message>
    <message>
        <source>Tree</source>
        <translation type="obsolete">Baum</translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="obsolete">Gruppe</translation>
    </message>
    <message>
        <source>From</source>
        <translation type="obsolete">Von</translation>
    </message>
    <message>
        <source>To</source>
        <translation type="obsolete">Bis</translation>
    </message>
    <message>
        <source>Contact Groups</source>
        <translation type="obsolete">Kontaktgruppen</translation>
    </message>
    <message>
        <source>Contact: </source>
        <translation type="obsolete">Kontakt:</translation>
    </message>
    <message>
        <source>Phone(s):</source>
        <translation type="obsolete">Telefonnummern:</translation>
    </message>
    <message>
        <source>Can not exit Contact Window while editing group tree!</source>
        <translation type="obsolete">Kontaktfenster kann nicht geschlossen werden solange der Gruppenbaum noch bearbeitet wird!</translation>
    </message>
    <message>
        <source>Types: </source>
        <translation type="obsolete">Arten:</translation>
    </message>
    <message>
        <source>Types</source>
        <translation type="obsolete">Arten</translation>
    </message>
    <message>
        <source>Built-in account</source>
        <translation type="obsolete">Systemkonto</translation>
    </message>
    <message>
        <source>This is built-in account, you can not modify all settings!</source>
        <translation type="obsolete">Dies ist ein nicht modifizierbares Systemkonto!</translation>
    </message>
    <message>
        <source>You can not delete built-in account!</source>
        <translation type="obsolete">Ein Systemkonto kann nicht gelöscht werden!</translation>
    </message>
    <message>
        <source>Login Account: </source>
        <translation type="obsolete">Login-Konto:</translation>
    </message>
    <message>
        <source>Login Account</source>
        <translation type="obsolete">Login-Konto</translation>
    </message>
    <message>
        <source>Modify Application Icon</source>
        <translation type="obsolete">Anwendungs-Icon ändern</translation>
    </message>
    <message>
        <source>Application code is mandatory field!</source>
        <translation type="obsolete">Anwendungscode zwingend!</translation>
    </message>
    <message>
        <source>Application name is mandatory field!</source>
        <translation type="obsolete">Anwendungsname zwingend!</translation>
    </message>
    <message>
        <source>Create Document From Selected Template</source>
        <translation type="obsolete">Dokument aus gewählter Vorlage erzeugen</translation>
    </message>
    <message>
        <source>Select Path</source>
        <translation type="obsolete">Pfad wählen</translation>
    </message>
    <message>
        <source>Remove Path</source>
        <translation type="obsolete">Pfad entfernen</translation>
    </message>
    <message>
        <source>Assigned Contacts</source>
        <translation type="obsolete">Zugewiesene Kontakte</translation>
    </message>
    <message>
        <source>Check In Date</source>
        <translation type="obsolete">Eincheck-Datum</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>Revision Tag</source>
        <translation type="obsolete">Versionsmarkierung</translation>
    </message>
    <message>
        <source>Select Document</source>
        <translation type="obsolete">Auswahl Dokument </translation>
    </message>
    <message>
        <source>Document name is mandatory field!</source>
        <translation type="obsolete">Dokumentenname zwingend!</translation>
    </message>
    <message>
        <source>Please Check Out Document first!</source>
        <translation type="obsolete">Dokument zuerst auschecken!</translation>
    </message>
    <message>
        <source>Document is already checked out by another user. Check in and overwrite any changes that this user made?</source>
        <translation type="obsolete">Das Dokument ist bereits von einem anderen Benutzer zur Bearbeitung ausgecheckt worden. Wollen Sie das Dokument trotzdem einchecken und damit alle Änderungen des anderen Benutzers verwerfen?</translation>
    </message>
    <message>
        <source>Overwrite previously checked in document?</source>
        <translation type="obsolete">Zuvor eingechecktes Dokument überschreiben?</translation>
    </message>
    <message>
        <source>Document is already checked out by another user. Check out as read only</source>
        <translation type="obsolete">Das Dokument ist bereits von einem anderen Benutzer zur Bearbeitung ausgecheckt worden und kann nur als Kopie zum Lesen ausgecheckt werden</translation>
    </message>
    <message>
        <source>Document already checked out!</source>
        <translation type="obsolete">Das Dokument ist bereits von einem anderen Benutzer zur Bearbeitung ausgecheckt worden!</translation>
    </message>
    <message>
        <source>Please first select an revision!</source>
        <translation type="obsolete">Version zuerst auswählen!</translation>
    </message>
    <message>
        <source>Failed to load from template!</source>
        <translation type="obsolete">Vorlage kann nicht geladen werden!</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="obsolete">Anwendung</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="obsolete">Pfad</translation>
    </message>
    <message>
        <source>Do you really want to delete the selected records {</source>
        <translation type="obsolete">Wollen Sie wirklich die ausgewählten Datensätze endgültig aus der Datenbank löschen {</translation>
    </message>
    <message>
        <source>Application path is mandatory field!</source>
        <translation type="obsolete">Anwendungspfad zwingend!</translation>
    </message>
    <message>
        <source>Application is mandatory field!</source>
        <translation type="obsolete">Anwendung zwingend!</translation>
    </message>
    <message>
        <source>Street</source>
        <translation type="obsolete">Strasse</translation>
    </message>
    <message>
        <source>Town</source>
        <translation type="obsolete">Ort</translation>
    </message>
    <message>
        <source>Country</source>
        <translation type="obsolete">Land</translation>
    </message>
    <message>
        <source>Phone numbers</source>
        <translation type="obsolete">Telefonnummern</translation>
    </message>
    <message>
        <source>Organization:</source>
        <translation type="obsolete">Organisation:</translation>
    </message>
    <message>
        <source>Subject</source>
        <translation type="obsolete">Betreff</translation>
    </message>
    <message>
        <source>Received/Sent</source>
        <translation type="obsolete">Gesendet/Erhalten</translation>
    </message>
    <message>
        <source>Projects</source>
        <translation type="obsolete">Projekte</translation>
    </message>
    <message>
        <source>Employee Code</source>
        <translation type="obsolete">Personalnummer</translation>
    </message>
    <message>
        <source>Project Code</source>
        <translation type="obsolete">Projektcode</translation>
    </message>
    <message>
        <source>Project Name</source>
        <translation type="obsolete">Projektname</translation>
    </message>
    <message>
        <source>Assignment Role Definitions: </source>
        <translation type="obsolete">Definitionen von Rollen:</translation>
    </message>
    <message>
        <source>Assignment Role Definitions</source>
        <translation type="obsolete">Definitionen von Rollen</translation>
    </message>
    <message>
        <source>Online</source>
        <translation type="obsolete">Online</translation>
    </message>
    <message>
        <source>Account</source>
        <translation type="obsolete">Konto</translation>
    </message>
    <message>
        <source>Phone Number</source>
        <translation type="obsolete">Telefonnummer</translation>
    </message>
    <message>
        <source>Forwarded</source>
        <translation type="obsolete">Weitergeleitet</translation>
    </message>
    <message>
        <source>Phone account</source>
        <translation type="obsolete">Telephonkonto</translation>
    </message>
    <message>
        <source>Command</source>
        <translation type="obsolete">Kommando</translation>
    </message>
    <message>
        <source>Foward To</source>
        <translation type="obsolete">Weiterleiten an</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Zeit</translation>
    </message>
    <message>
        <source>Frequence</source>
        <translation type="obsolete">Periodizität</translation>
    </message>
    <message>
        <source>Project: </source>
        <translation type="obsolete">Projekt:</translation>
    </message>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
    <message>
        <source>Call</source>
        <translation type="obsolete">Anrufen</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="obsolete"> Dokument</translation>
    </message>
    <message>
        <source>Assign Project To Contact</source>
        <translation type="obsolete">Projekt zu Kontakt zuweisen</translation>
    </message>
    <message>
        <source>Assign Project To Contact?</source>
        <translation type="obsolete">Projekt zu Kontakt zuweisen?</translation>
    </message>
    <message>
        <source>Load Data before copy!</source>
        <translation type="obsolete">Vor dem Kopieren muss ein Datensatz geladen sein!</translation>
    </message>
    <message>
        <source>Can not close window in edit or insert mode!</source>
        <translation type="obsolete">Das Fenster kann im Bearbeitungs-/Einfügemodus nicht geschlossen werden!</translation>
    </message>
    <message>
        <source>Can not close window </source>
        <translation type="obsolete">Fenster kann nicht geschlossen werden </translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="obsolete">Code</translation>
    </message>
    <message>
        <source>Dept. Name</source>
        <translation type="obsolete">Abteilung</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Benutzername</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Art</translation>
    </message>
    <message>
        <source>Role NM Pairs</source>
        <translation type="obsolete">Rolle in Paarbeziehung</translation>
    </message>
    <message>
        <source>Application Name</source>
        <translation type="obsolete">Applikationsname</translation>
    </message>
    <message>
        <source>Template Name</source>
        <translation type="obsolete">Vorlagenname</translation>
    </message>
    <message>
        <source>Edit Assignment</source>
        <translation type="obsolete">Zuweisung bearbeiten</translation>
    </message>
    <message>
        <source>Show Assignment</source>
        <translation type="obsolete">Zuweisung anzeigen</translation>
    </message>
    <message>
        <source>Remove Assignment</source>
        <translation type="obsolete">Zuweisung entfernen</translation>
    </message>
    <message>
        <source>Show Details</source>
        <translation type="obsolete">Zeige Details</translation>
    </message>
    <message>
        <source>Assign </source>
        <translation type="obsolete">Zuweisen </translation>
    </message>
    <message>
        <source>Please select an asignment!</source>
        <translation type="obsolete">Wählen Sie eine Zuweisung aus!</translation>
    </message>
    <message>
        <source>Default Directory For Temporary Documents</source>
        <translation type="obsolete">Default-Verzeichnis für temporäre Dokumente</translation>
    </message>
    <message>
        <source>Save changed settings before closing of press Cancel button!</source>
        <translation type="obsolete">Vor dem Schliessen müssen die geänderten Einstellungen gespeichert werden, oder andernfalls muss abgebrochen werden!</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>Actual Organization:</source>
        <translation type="obsolete">Aktuelle Organisation:</translation>
    </message>
    <message>
        <source>Communicate</source>
        <translation type="obsolete">Kommunikation</translation>
    </message>
    <message>
        <source>Phone Call</source>
        <translation type="obsolete">Telefonanruf</translation>
    </message>
    <message>
        <source>Do you really want to delete the selected contacts {</source>
        <translation type="obsolete">Wollen Sie wirklich die ausgewählten Kontakte endgültig aus der Datenbank löschen {</translation>
    </message>
    <message>
        <source>Go View Mode</source>
        <translation type="obsolete">In Ansichtsmodus wechseln</translation>
    </message>
    <message>
        <source>Modify Tree</source>
        <translation type="obsolete">Baum bearbeiten</translation>
    </message>
    <message>
        <source>Do you really want to delete this tree permanently from the database? Warning: all groups inside will be deleted!</source>
        <translation type="obsolete">Wollen Sie diesen Baum endgültig aus der Datenbank löschen? Warnung: Alle dazugehörigen Gruppen und deren Zuweisungen gehen verloren!</translation>
    </message>
    <message>
        <source>Do you really want to delete selected groups from the database?</source>
        <translation type="obsolete">Wollen Sie die ausgewählten Gruppe endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <source>Invalid format: column count must be inside [1-4]!</source>
        <translation type="obsolete">Ungültiges Format: 1-4 Spalten müssen vorhanden sein!</translation>
    </message>
    <message>
        <source>Open In New Window</source>
        <translation type="obsolete">In neuem Fenster öffnen</translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="obsolete">Daten</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="obsolete">Titel</translation>
    </message>
    <message>
        <source>ZIP Code</source>
        <translation type="obsolete">Postleitzahl</translation>
    </message>
    <message>
        <source>City</source>
        <translation type="obsolete">Ort</translation>
    </message>
    <message>
        <source>Region</source>
        <translation type="obsolete">Kanton</translation>
    </message>
    <message>
        <source>PO BOX</source>
        <translation type="obsolete">Postfach</translation>
    </message>
    <message>
        <source>PO BOX ZIP</source>
        <translation type="obsolete">PLZ Postfach</translation>
    </message>
    <message>
        <source>Office Code</source>
        <translation type="obsolete">Raumnummer</translation>
    </message>
    <message>
        <source>Default Type</source>
        <translation type="obsolete">Default-Art</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Datum</translation>
    </message>
    <message>
        <source>Is Private</source>
        <translation type="obsolete">Privat</translation>
    </message>
    <message>
        <source>Person</source>
        <translation type="obsolete">Person</translation>
    </message>
    <message>
        <source>Please select journal entry!</source>
        <translation type="obsolete">Auswahl Journaleintrag!</translation>
    </message>
    <message>
        <source>Do you really want to delete the selected journal entries {</source>
        <translation type="obsolete">Wollen Sie wirklich die ausgewählten Journaleinträge endgültig aus der Datenbank löschen {</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="obsolete">Telefonnummer</translation>
    </message>
    <message>
        <source>Country Predial</source>
        <translation type="obsolete">Landesvorwahl</translation>
    </message>
    <message>
        <source>Area Predial</source>
        <translation type="obsolete">Regionsvorwahl</translation>
    </message>
    <message>
        <source>Local Number</source>
        <translation type="obsolete">Lokale Nummer</translation>
    </message>
    <message>
        <source>Picture</source>
        <translation type="obsolete">Bild</translation>
    </message>
    <message>
        <source>System Type</source>
        <translation type="obsolete">System-Typ</translation>
    </message>
    <message>
        <source>Format Phone</source>
        <translation type="obsolete">Telefonnummer formatieren</translation>
    </message>
    <message>
        <source>Name 1</source>
        <translation type="obsolete">Name 1</translation>
    </message>
    <message>
        <source>Role</source>
        <translation type="obsolete">Rolle</translation>
    </message>
    <message>
        <source>Name 2</source>
        <translation type="obsolete">Name 2</translation>
    </message>
    <message>
        <source>Assigment Text</source>
        <translation type="obsolete">Zuweisungsbeschreibung</translation>
    </message>
    <message>
        <source>New Document</source>
        <translation type="obsolete">Neues Dokument</translation>
    </message>
    <message>
        <source>New Document For Selected Application</source>
        <translation type="obsolete">Neues Dokument für gewählte Anwendung</translation>
    </message>
    <message>
        <source>Drop Zone</source>
        <translation type="obsolete">Drop Zone</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation type="obsolete">Auswahl Datei</translation>
    </message>
    <message>
        <source>Local File Reference</source>
        <translation type="obsolete">Verweis auf lokale Datei</translation>
    </message>
    <message>
        <source>Paper Document Location</source>
        <translation type="obsolete">Standort Papier-Dokument</translation>
    </message>
    <message>
        <source>Setting up personal desktop interface...</source>
        <translation type="obsolete">Persönlicher Desktop wird konfiguriert...</translation>
    </message>
    <message>
        <source>Failed to check in document</source>
        <translation type="obsolete">Dokument kann nicht eingecheckt werden</translation>
    </message>
    <message>
        <source>Failed to check in document! Document Not Found!</source>
        <translation type="obsolete">Dokument kann nicht eingecheckt werden: Datei nicht gefunden!</translation>
    </message>
    <message>
        <source>  OK  </source>
        <translation type="obsolete">  OK  </translation>
    </message>
    <message>
        <source>  Discard Checked-Out Revision of Document  </source>
        <translation type="obsolete">  Ausgecheckte Dokumentenversion verwerfen  </translation>
    </message>
    <message>
        <source>Start Processing Sent Emails From Outlook</source>
        <translation type="obsolete">Start der Verarbeitung der aus Outlook verschickten E-Mails</translation>
    </message>
    <message>
        <source>End Processing Sent Emails From Outlook</source>
        <translation type="obsolete">Ende der Verarbeitung der aus Outlook verschickten E-Mails</translation>
    </message>
    <message>
        <source>Select export/import directory</source>
        <translation type="obsolete">Import/Export-Verzeichnis wählen</translation>
    </message>
    <message>
        <location filename="wizardregistration.cpp" line="+9"/>
        <location line="+30"/>
        <source>Select Projects for Export</source>
        <translation>Projekte für den Export auswählen</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Select Users for Export</source>
        <translation>Benutzer wählen für Export</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select File for User export (specify full file path, including custom filename)</source>
        <translation>Datei für den Benutzerexport auswählen (mit voller Pfadangabe inkl. Dateiname)</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Select File for Project export (specify full file path, including custom filename)</source>
        <translation>Datei für den Projektexport auswählen (mit voller Pfadangabe inkl. Dateiname)</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Select Filter</source>
        <translation>Filter auswählen</translation>
    </message>
    <message>
        <source>Documents &amp; Applications</source>
        <translation type="obsolete">Dokumente &amp; Anwendungen</translation>
    </message>
    <message>
        <location line="-58"/>
        <source>First Steps</source>
        <translation>Erste Schritte</translation>
    </message>
    <message>
        <source>Web Site</source>
        <translation type="obsolete">Webseite</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Select File for Contact Export (specify full file path, including custom filename)</source>
        <translation>Datei-Auswahl für Kontakt-Export (vollen Pfad inklusive Dateiname eintragen) </translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Select Contacts for Export</source>
        <translation>Kontakte für Export auswählen</translation>
    </message>
    <message>
        <location filename="fui_busperson.cpp" line="-56"/>
        <location line="+17"/>
        <location filename="fui_coreuser.cpp" line="+194"/>
        <source>Last Name is mandatory field!</source>
        <translation>Nachname obligatorisch!</translation>
    </message>
    <message>
        <location filename="qcw_base.cpp" line="+248"/>
        <source>Organization Name is mandatory field!</source>
        <translation>Organisation obligatorisch!</translation>
    </message>
    <message>
        <location filename="wizardregistration.cpp" line="-18"/>
        <location line="+10"/>
        <location line="+10"/>
        <source>Use Unicode for Export</source>
        <translation>Bei Export Unicode verwenden</translation>
    </message>
    <message>
        <location filename="communicationactions.cpp" line="+102"/>
        <source>Create Email Task</source>
        <translation>E-Mail-Aufgabe erzeugen</translation>
    </message>
    <message>
        <location filename="qcw_base.cpp" line="-8"/>
        <source>You must enter at least Last Name or First Name of Contact!</source>
        <translation>Geben Sie bitte Nach- oder Vorname des Kontaktes ein!</translation>
    </message>
    <message>
        <location filename="admintool.cpp" line="+790"/>
        <source>Save Backup File</source>
        <translation>Backup-Datei speichern</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Load Backup File</source>
        <translation>Backupdatei laden</translation>
    </message>
    <message>
        <location filename="communicationactions.cpp" line="+160"/>
        <source>Send Serial Email</source>
        <translation>Serien-E-Mail versenden</translation>
    </message>
    <message>
        <location line="-449"/>
        <source>This contact has no Skype(R) name defined. It is required for chatting!</source>
        <translation>Dieser Kontakt hat keinen Skype(R)-Namen erfasst. Dieser ist für Chats zwingend!</translation>
    </message>
    <message>
        <location filename="fui_buscostcenters.cpp" line="+14"/>
        <location filename="fui_busdepartments.cpp" line="+15"/>
        <location filename="fui_busorganizations.cpp" line="+128"/>
        <location filename="fui_ce_types.cpp" line="+146"/>
        <location filename="fui_ceeventtype.cpp" line="+8"/>
        <location filename="fui_projects.cpp" line="+450"/>
        <source>Code is mandatory field!</source>
        <translation>Ein Code ist obligatorisch!</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="fui_busdepartments.cpp" line="+3"/>
        <location filename="fui_busorganizations.cpp" line="+3"/>
        <location filename="fui_ce_types.cpp" line="+3"/>
        <location filename="fui_ceeventtype.cpp" line="+3"/>
        <location filename="fui_customfields.cpp" line="+108"/>
        <location filename="fui_projects.cpp" line="+3"/>
        <source>Name is mandatory field!</source>
        <translation>Ein Vorname ist obligatorisch!</translation>
    </message>
    <message>
        <location filename="fui_busperson.cpp" line="-10"/>
        <source>Pers. No. is mandatory field!</source>
        <translation>Die Personalnummer ist obligatorisch!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Initials is mandatory field!</source>
        <translation>Das Visum ist obligatorisch!</translation>
    </message>
    <message>
        <location line="+14"/>
        <location filename="fui_coreuser.cpp" line="+7"/>
        <source>Username is mandatory field!</source>
        <translation>Der Benutzername ist obligatorisch!</translation>
    </message>
    <message>
        <location filename="fui_coreuser.cpp" line="+3"/>
        <source>Password must be set!</source>
        <translation>Passwort muss eingetragen sein!</translation>
    </message>
    <message>
        <location filename="table_contactaddress.cpp" line="+347"/>
        <source>Address is mandatory field!</source>
        <translation>Eine Adresse ist obligatorisch!</translation>
    </message>
    <message>
        <location filename="table_contactemail.cpp" line="+142"/>
        <source>Email address is mandatory field!</source>
        <translation>Eine E-Mail-Adresse ist obligatorisch!</translation>
    </message>
    <message>
        <location filename="table_contactinternet.cpp" line="+164"/>
        <source>Internet address is mandatory field!</source>
        <translation>Eine Internetadresse ist obligatorisch!</translation>
    </message>
    <message>
        <location filename="table_contactphone.cpp" line="+317"/>
        <source>Phone number is mandatory field!</source>
        <translation>Eine Telefonnummer ist obligatorisch!</translation>
    </message>
    <message>
        <location filename="table_contacttype.cpp" line="+28"/>
        <source>Type Name is mandatory field!</source>
        <translation>Der Typ-Name ist obligatorisch!</translation>
    </message>
    <message>
        <location filename="sidebarfui_base.cpp" line="+89"/>
        <source>Application Manager</source>
        <translation>Anwendungs-Manager</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Calendar Manager</source>
        <translation>Kalender-Manager</translation>
    </message>
    <message>
        <location filename="sms_dialog.cpp" line="+283"/>
        <source>Acknowledgement</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The SMS has been transferred to Skype(C) to be sent from there. You will check its delivery it in Skype&apos;s event protocol.</source>
        <translation>Die SMS wurde zum Senden nach Skype(C) übertragen. Sie können das Versenden im Ereignisprotokoll von Skype überprüfen.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error sending an SMS message (%1)!</source>
        <translation>Fehler beim Senden einer SMS-Nachricht (%1)!</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>SMS message sent (%1)!</source>
        <translation>SMS-Nachricht gesendet (%1)!</translation>
    </message>
    <message>
        <location filename="wizardregistration.cpp" line="-69"/>
        <location line="+10"/>
        <location line="+3"/>
        <location line="+10"/>
        <location line="+3"/>
        <location line="+52"/>
        <location line="+3"/>
        <location line="+10"/>
        <location line="+3"/>
        <location line="+3"/>
        <location line="+17"/>
        <location line="+10"/>
        <location line="+3"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location filename="calendarhelper.cpp" line="+88"/>
        <source>Fixed</source>
        <translation>endgültig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preliminary</source>
        <translation>vorläufig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Over</source>
        <translation>hat stattgefunden</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancelled</source>
        <translation>abgesagt</translation>
    </message>
    <message>
        <location filename="fui_calendarvieweditor.cpp" line="-515"/>
        <source>Regenerating Views...</source>
        <translation>Ansichten werden neu erstellt...</translation>
    </message>
    <message>
        <location filename="commgridview.cpp" line="-278"/>
        <source>Too many items found. List restricted to 4000 items. Please redefine your search!</source>
        <translation>Zu viele Suchergebnisse. Die Ergebnisliste wird auf 4000 Einträge begrenzt. Bitte grenzen Sie Ihre Suche genauer ein!</translation>
    </message>
</context>
<context>
    <name>RecipientsTableModel</name>
    <message>
        <source>To:</source>
        <translation type="obsolete">An:</translation>
    </message>
    <message>
        <source>CC:</source>
        <translation type="obsolete">CC:</translation>
    </message>
    <message>
        <source>BCC:</source>
        <translation type="obsolete">BCC:</translation>
    </message>
</context>
<context>
    <name>RecipientsTableView</name>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <source>To:</source>
        <translation type="obsolete">An:</translation>
    </message>
    <message>
        <source>CC:</source>
        <translation type="obsolete">CC:</translation>
    </message>
    <message>
        <source>BCC:</source>
        <translation type="obsolete">BCC:</translation>
    </message>
</context>
<context>
    <name>ReportManager</name>
    <message>
        <location filename="reportmanager.cpp" line="+32"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Report not available, check your license!</source>
        <translation>Auswertung nicht verfügbar, bitte prüfen Sie Ihre Lizenzierung!</translation>
    </message>
</context>
<context>
    <name>ReportPreviewer</name>
    <message>
        <location filename="reportpreviewer.cpp" line="+22"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Print to PDF</source>
        <translation>In PDF-Datei ausgeben</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Go to first page</source>
        <translation>Springe zu erster Seite</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go to previous page</source>
        <translation>Springe zu vorhergehender Seite</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go to next page</source>
        <translation>Springe zu nächster Seite</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go to last page</source>
        <translation>Springe zur letzten Seite</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go to specified page</source>
        <translation>Springe zu wählbarer Seite</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Close report</source>
        <translation>Auswertung schliessen</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Go to page</source>
        <translation>Springe zu Seite</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page number:</source>
        <translation>Seitenzahl:</translation>
    </message>
    <message>
        <location line="-217"/>
        <source>Reporting</source>
        <translation>Auswertungen</translation>
    </message>
</context>
<context>
    <name>ReportPreviewerClass</name>
    <message>
        <location filename="reportpreviewer.ui" line="+16"/>
        <source>Report Previewer</source>
        <translation>Auswertungs-Vorschau</translation>
    </message>
</context>
<context>
    <name>ReportPrintingDestination</name>
    <message>
        <location filename="reportprintingdestination.cpp" line="+43"/>
        <source>Print to screen</source>
        <translation>Ausgabe auf Bildschirm</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Send to printer</source>
        <translation>Ausgabe an Drucker</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print to PDF</source>
        <translation>Ausgabe in PDF-Datei</translation>
    </message>
    <message>
        <location line="-33"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
</context>
<context>
    <name>ReportPrintingDestinationClass</name>
    <message>
        <location filename="reportprintingdestination.ui" line="+16"/>
        <source>Report Printing Destination</source>
        <translation>Druckdestination</translation>
    </message>
</context>
<context>
    <name>ReportSelectionWizPageClass</name>
    <message>
        <location filename="reportselectionwizpage.ui" line="+13"/>
        <source>Report Selection</source>
        <translation>Auswahl Auswertungen</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation type="obsolete">&gt;&gt;</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
    <message>
        <source>&lt;&lt;</source>
        <translation type="obsolete">&lt;&lt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>RoleInputDialog</name>
    <message>
        <location filename="roleinputdialog.cpp" line="+25"/>
        <source>Role Name</source>
        <translation>Rollenname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Role Description</source>
        <translation>Rollenbeschreibung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System Role</source>
        <translation>System-Rolle</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Business visible</source>
        <translation>Auf Applikationsebene sichtbar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Role</source>
        <translation>Neue Rolle</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Access Rights</source>
        <translation>Zugriffsrechte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Role Name field empty!
Please insert role name field.</source>
        <translation>Rollenname leer!
Bitte geben Sie einen Rollennamen ein.</translation>
    </message>
</context>
<context>
    <name>RoleInputDialogClass</name>
    <message>
        <location filename="roleinputdialog.ui" line="+25"/>
        <source>Role Input</source>
        <translation>Eingabe Rolle</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>System Role</source>
        <translation>System-Rolle</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Business visible</source>
        <translation>Auf Applikationsebene sichtbar</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Role Name:</source>
        <translation>Rollenname:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Role Description:</source>
        <translation>Rollenbeschreibung:</translation>
    </message>
</context>
<context>
    <name>SMS_Dialog</name>
    <message>
        <location filename="sms_dialog.cpp" line="-230"/>
        <source>%1 letters typed (%2 messages)</source>
        <translation>%1 Zeichen eingegeben (%2 Nachrichten)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>You must enter Phone Number!</source>
        <translation>Bitte geben Sie eine Telefonnummer ein!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid Phone Number!</source>
        <translation>Ungültige Telefonummer!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Message is empty!</source>
        <translation>Nachricht ist leer!</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>The SMS has been transferred to Skype(C) to be sent from there. You will check its delivery it in Skype&apos;s event protocol.</source>
        <translation>Das SMS wurde zum Senden nach Skype(C) übertragen. Sie können das Versenden im Ereignisprotokoll von Skype überprüfen.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error sending an SMS message (%1)!</source>
        <translation>Fehler beim Senden einer SMS-Nachricht (%1)!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>SMS message sent (%1)!</source>
        <translation>SMS-Nachricht nicht gesendet (%1)!</translation>
    </message>
    <message>
        <location line="-15"/>
        <source>Acknowledgement</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+21"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Done!</source>
        <translation>Erledigt!</translation>
    </message>
</context>
<context>
    <name>SMS_DialogClass</name>
    <message>
        <location filename="sms_dialog.ui" line="+14"/>
        <source>Send SMS message</source>
        <translation>SMS-Nachricht senden</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>Message:</source>
        <translation>Nachricht:</translation>
    </message>
    <message>
        <source>Phone number:</source>
        <translation type="obsolete">Telefonnummer:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Send</source>
        <translation>Senden</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-111"/>
        <source>SMS</source>
        <translation>SMS</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Phone Number:</source>
        <translation>Telefonnummer:</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Save &amp;&amp; Send</source>
        <translation>Speichern &amp;&amp; Senden</translation>
    </message>
</context>
<context>
    <name>SelectionPopupClass</name>
    <message>
        <source>Select an Item</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>Selection_ACP</name>
    <message>
        <source>Select</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>Remove Assignment</source>
        <translation type="obsolete">Zuweisung entfernen</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
</context>
<context>
    <name>Selection_ACPClass</name>
    <message>
        <source>Selection_ACP</source>
        <translation type="obsolete">Selection_ACP</translation>
    </message>
</context>
<context>
    <name>Selection_ActualOrganization</name>
    <message>
        <source>Actual Organization:</source>
        <translation type="obsolete">Aktuelle Organisation:</translation>
    </message>
</context>
<context>
    <name>Selection_ActualOrganization_FUI</name>
    <message>
        <location filename="selection_actualorganization_fui.cpp" line="+97"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No Assignment Made!</source>
        <translation>Keine bestehende Zuweisung!</translation>
    </message>
</context>
<context>
    <name>Selection_ContactButton</name>
    <message>
        <source>Email</source>
        <translation type="obsolete"> E-Mail</translation>
    </message>
    <message>
        <source>Phone Call</source>
        <translation type="obsolete">Anruf</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="obsolete"> Dokument</translation>
    </message>
    <message>
        <source>Internet</source>
        <translation type="obsolete">Internet</translation>
    </message>
    <message>
        <source>Communicate</source>
        <translation type="obsolete">Kommunikation</translation>
    </message>
    <message>
        <source>Chat</source>
        <translation type="obsolete">Chat</translation>
    </message>
    <message>
        <source>SMS</source>
        <translation type="obsolete">SMS</translation>
    </message>
</context>
<context>
    <name>Selection_Contacts</name>
    <message>
        <source>Show Contact Details</source>
        <translation type="obsolete">Kontakt-Details anzeigen</translation>
    </message>
    <message>
        <source>Show Contact Details In New Window</source>
        <translation type="obsolete">Kontakt-Details in neuem Fenster anzeigen</translation>
    </message>
    <message>
        <source>Delete Selected Contacts From Database</source>
        <translation type="obsolete">Ausgewählte Kontakte aus der Datenbank löschen</translation>
    </message>
    <message>
        <source>Clear List</source>
        <translation type="obsolete">Liste leeren</translation>
    </message>
    <message>
        <source>Remove Selected Contacts From List</source>
        <translation type="obsolete">Ausgewählte Kontakte aus der Liste entfernen</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <source>Remove Duplicates From List</source>
        <translation type="obsolete">Doubletten aus Liste entfernen</translation>
    </message>
    <message>
        <source>S&amp;ort</source>
        <translation type="obsolete">&amp;Sortieren</translation>
    </message>
    <message>
        <source>CTRL+S</source>
        <translation type="obsolete">CTRL+S</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Do you really want to delete the selected contacts {</source>
        <translation type="obsolete">Wollen Sie wirklich die ausgewählten Kontakte endgültig aus der Datenbank löschen {</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <source>Build Contact List</source>
        <translation type="obsolete">Kontaktliste aufbauen</translation>
    </message>
    <message>
        <source>Create Organization(s) From Person(s)</source>
        <translation type="obsolete">Organisation(en) zu Person(en) erzeugen</translation>
    </message>
    <message>
        <source>Find Duplicates in Database</source>
        <translation type="obsolete">Doubletten in Datenbank suchen</translation>
    </message>
    <message>
        <source>No person contact selected!</source>
        <translation type="obsolete">Kein Personen-Kontakt ausgewählt!</translation>
    </message>
    <message>
        <source>Copying Contacts...</source>
        <translation type="obsolete">Kontakte werden kopiert...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Copy In Progress</source>
        <translation type="obsolete">Kopie wird durchgeführt</translation>
    </message>
    <message>
        <source> contact successfully copied!</source>
        <translation type="obsolete"> Kontakt erfolgreich kopiert!</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Information</translation>
    </message>
    <message>
        <source>No duplicate contact found!</source>
        <translation type="obsolete">Keine Kontakt-Doubletten gefunden!</translation>
    </message>
    <message>
        <source>} from the database?</source>
        <translation type="obsolete">} aus der Datenbank?</translation>
    </message>
</context>
<context>
    <name>Selection_ContactsClass</name>
    <message>
        <source>Selection_Contacts</source>
        <translation type="obsolete">Selection_Contacts</translation>
    </message>
</context>
<context>
    <name>Selection_EditTemplateTree</name>
    <message>
        <source>Code</source>
        <translation type="obsolete">Code</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
</context>
<context>
    <name>Selection_EditTemplateTreeClass</name>
    <message>
        <source>Edit Items</source>
        <translation type="obsolete">Einträge bearbeiten</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>Selection_GroupTree</name>
    <message>
        <source>Add New Tree</source>
        <translation type="obsolete">Neuen Baum hinzufügen</translation>
    </message>
    <message>
        <source>Rename Tree</source>
        <translation type="obsolete">Baum umbenennen</translation>
    </message>
    <message>
        <source>Delete Tree</source>
        <translation type="obsolete">Baum löschen</translation>
    </message>
    <message>
        <source>Reload Trees</source>
        <translation type="obsolete">Bäume neu laden</translation>
    </message>
    <message>
        <source>Add Group</source>
        <translation type="obsolete">Gruppe hinzufügen</translation>
    </message>
    <message>
        <source>Rename Group</source>
        <translation type="obsolete">Gruppe umbenennen</translation>
    </message>
    <message>
        <source>Delete Group</source>
        <translation type="obsolete">Gruppe löschen</translation>
    </message>
    <message>
        <source>Add Selected </source>
        <translation type="obsolete">Ausgewählte  </translation>
    </message>
    <message>
        <source> to Group</source>
        <translation type="obsolete"> zur Gruppe hinzufügen</translation>
    </message>
    <message>
        <source>Remove Selected </source>
        <translation type="obsolete">Ausgewählte  </translation>
    </message>
    <message>
        <source> From Group</source>
        <translation type="obsolete"> aus der Gruppe entfernen</translation>
    </message>
    <message>
        <source>Replace Group </source>
        <translation type="obsolete">Gruppen-</translation>
    </message>
    <message>
        <source> With Selected Records</source>
        <translation type="obsolete"> durch ausgewählte Kontakte ersetzen  </translation>
    </message>
    <message>
        <source>Intersect Selected </source>
        <translation type="obsolete">Schnittmenge der ausgewählten </translation>
    </message>
    <message>
        <source> With Group Records</source>
        <translation type="obsolete"> mit Gruppenkontakten</translation>
    </message>
    <message>
        <source>Add Whole Actual </source>
        <translation type="obsolete">Gesamte aktuelle </translation>
    </message>
    <message>
        <source> List to Group</source>
        <translation type="obsolete">liste zur Gruppe hinzufügen</translation>
    </message>
    <message>
        <source> With Whole Actual </source>
        <translation type="obsolete"> durch gesamte </translation>
    </message>
    <message>
        <source> List</source>
        <translation type="obsolete">liste ersetzen</translation>
    </message>
    <message>
        <source>External Category</source>
        <translation type="obsolete">Externe Kategorie</translation>
    </message>
    <message>
        <source>Import Sub-groups</source>
        <translation type="obsolete">Untergruppen importieren</translation>
    </message>
    <message>
        <source>Refresh Data</source>
        <translation type="obsolete">Daten aktualisieren</translation>
    </message>
    <message>
        <source>Expand All Nodes</source>
        <translation type="obsolete">Alle Zweige öffnen</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Please Select Tree For Edit!</source>
        <translation type="obsolete">Auswahl Baum zur Bearbeitung!</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Go View Mode</source>
        <translation type="obsolete">In Ansichtsmodus wechseln</translation>
    </message>
    <message>
        <source>Modify Tree</source>
        <translation type="obsolete">Baum bearbeiten</translation>
    </message>
    <message>
        <source>Enter Tree Name</source>
        <translation type="obsolete">Baumnamen eingeben</translation>
    </message>
    <message>
        <source>Tree Name:</source>
        <translation type="obsolete">Baumname:</translation>
    </message>
    <message>
        <source>Do you really want to delete this tree permanently from the database? Warning: all groups inside will be deleted!</source>
        <translation type="obsolete">Wollen Sie diesen Baum endgültig aus der Datenbank löschen? Warnung: Alle dazugehörigen Gruppen und deren Zuweisungen gehen verloren!</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <source>Group Code And Name Must Be Specified!</source>
        <translation type="obsolete">Gruppencode und -name müssen erfasst sein!</translation>
    </message>
    <message>
        <source>Enter New Tree Name</source>
        <translation type="obsolete">Neuen Baumnamen erfassen</translation>
    </message>
    <message>
        <source>Enter Group External Category</source>
        <translation type="obsolete">Externe Kategorie für Gruppe erfassen</translation>
    </message>
    <message>
        <source>External Category:</source>
        <translation type="obsolete">Externe Kategorie:</translation>
    </message>
    <message>
        <source>Import Groups</source>
        <translation type="obsolete">Gruppen importieren</translation>
    </message>
    <message>
        <source>Import File (*.txt)</source>
        <translation type="obsolete">Datei importieren (*.txt)</translation>
    </message>
    <message>
        <source>Invalid format: column count must be inside [1-4]!</source>
        <translation type="obsolete">Ungültiges Format: 1-4 Spalten müssen vorhanden sein!</translation>
    </message>
    <message>
        <source>Startup: Load in Actual Contact List</source>
        <translation type="obsolete">Bei Programmstart: In aktuelle Kontaktliste laden</translation>
    </message>
    <message>
        <source>Set Group For Load In Actual List (</source>
        <translation type="obsolete">Gruppe zum Laden in die aktuelle Kontaktliste (</translation>
    </message>
    <message>
        <source>Do you really want to delete selected groups from the database?</source>
        <translation type="obsolete">Wollen Sie die ausgewählten Gruppe endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <source>Startup: Load in Actual Contact List (</source>
        <translation type="obsolete">Bei Programmstart: In aktuelle Kontaktliste laden (</translation>
    </message>
    <message>
        <source>Contact</source>
        <translation type="obsolete">Kontakt</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="obsolete">Kontakte</translation>
    </message>
    <message>
        <source>Record</source>
        <translation type="obsolete">Datensatz</translation>
    </message>
    <message>
        <source>Records</source>
        <translation type="obsolete">Datensätze</translation>
    </message>
</context>
<context>
    <name>Selection_GroupTreeClass</name>
    <message>
        <source>Selection_GroupTree</source>
        <translation type="obsolete">Selection_GroupTree</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
</context>
<context>
    <name>Selection_NM_SAPNE</name>
    <message>
        <source>Modify/Create</source>
        <translation type="obsolete">Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>Remove Assignment</source>
        <translation type="obsolete">Zuweisung entfernen</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="obsolete">Hinzufügen</translation>
    </message>
    <message>
        <source>Open In New Window</source>
        <translation type="obsolete">In neuem Fenster öffnen</translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="obsolete">Daten</translation>
    </message>
</context>
<context>
    <name>Selection_NM_SAPNEClass</name>
    <message>
        <source>Selection_NM_SAPNE</source>
        <translation type="obsolete">Selection_NM_SAPNE</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Assigned NM Records&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Zugewiesene Verknüpfungen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Selection_SAPNE</name>
    <message>
        <source>Modify/Create</source>
        <translation type="obsolete">Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>Remove Assignment</source>
        <translation type="obsolete">Zuweisung entfernen</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="obsolete">Hinzufügen</translation>
    </message>
    <message>
        <source>View Details</source>
        <translation type="obsolete">Zeige Details</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>No Assignment Made!</source>
        <translation type="obsolete">Keine bestehende Zuweisung!</translation>
    </message>
</context>
<context>
    <name>Selection_SAPNEClass</name>
    <message>
        <source>Selection_SAPNE</source>
        <translation type="obsolete">Selection_SAPNE</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;label&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;Bezeichnung&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Selection_SAPNE_FUI</name>
    <message>
        <location filename="selection_sapne_fui.cpp" line="+134"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No Assignment Made!</source>
        <translation>Keine bestehende Zuweisung!</translation>
    </message>
</context>
<context>
    <name>Selection_ScheduleButton</name>
    <message>
        <source>Schedule Follow-Up</source>
        <translation type="obsolete">Folgeaufgabe planen</translation>
    </message>
    <message>
        <source>Communicate</source>
        <translation type="obsolete">Kommunikation</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="obsolete"> Dokument</translation>
    </message>
    <message>
        <source>Local File </source>
        <translation type="obsolete">Lokale Datei  </translation>
    </message>
    <message>
        <source>Internet File</source>
        <translation type="obsolete">Internet-Datei</translation>
    </message>
    <message>
        <source>Note</source>
        <translation type="obsolete">Notiz</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Adresse</translation>
    </message>
    <message>
        <source>Paper Document</source>
        <translation type="obsolete">Papier-Dokument</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="obsolete">E-Mail</translation>
    </message>
    <message>
        <source>Phone Call</source>
        <translation type="obsolete">Telefonanruf</translation>
    </message>
</context>
<context>
    <name>Selection_TableBasedClass</name>
    <message>
        <source>Selection_TableBased</source>
        <translation type="obsolete">Selection_TableBased</translation>
    </message>
</context>
<context>
    <name>Selection_TreeBased</name>
    <message>
        <source>&amp;Reload</source>
        <translation type="obsolete">Neu l&amp;aden</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation type="obsolete">&amp;Suchen</translation>
    </message>
    <message>
        <source>&amp;Details</source>
        <translation type="obsolete">&amp;Details</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation type="obsolete">&amp;Neu</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation type="obsolete">Ins</translation>
    </message>
    <message>
        <source>New From Template</source>
        <translation type="obsolete">Neu aus Vorlage</translation>
    </message>
    <message>
        <source>New Substructure From Template</source>
        <translation type="obsolete">Neue Unterstruktur aus Vorlage erzeugen</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="obsolete">&amp;Kopieren</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation type="obsolete">&amp;Verschieben</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="obsolete">&amp;Bearbeiten</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="obsolete">&amp;Löschen</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="obsolete">Del</translation>
    </message>
</context>
<context>
    <name>Selection_TreeBasedClass</name>
    <message>
        <source>Selection</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
</context>
<context>
    <name>Settings_CommClass</name>
    <message>
        <location filename="settings_comm.ui" line="+14"/>
        <source>Communication Settings</source>
        <translation>Einstellungen Kommunikation</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Listen for Skype Calls</source>
        <translation>Skype-Anrufe empfangen</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Default Group for Call Transfer</source>
        <translation>Default-Gruppe für Anrufvermittlung</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Default for Outgoing Phone Calls:</source>
        <translation>Default für ausgehende Anrufe:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Skype</source>
        <translation>Skype</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>TAPI</source>
        <translation>TAPI</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Default TAPI Line:</source>
        <translation>Default TAPI-Linie:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use Dial-Out Prefix:</source>
        <translation>Amtsvorwahl:</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Default Phone Call: Incoming</source>
        <translation>Default Anruf: Eingehend</translation>
    </message>
</context>
<context>
    <name>Settings_Contacts</name>
    <message>
        <location filename="settings_contacts.cpp" line="+27"/>
        <location line="+11"/>
        <source>Modify View</source>
        <translation>Ansicht definieren</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+8"/>
        <source>Modify Address Schema</source>
        <translation>Adressschema bearbeiten</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>%1 folders selected</source>
        <translation>%1 Verzeichnis ausgewählt</translation>
    </message>
</context>
<context>
    <name>Settings_ContactsClass</name>
    <message>
        <source>Settings_Contacts</source>
        <translation type="obsolete">Settings_Contacts</translation>
    </message>
    <message>
        <location filename="settings_contacts.ui" line="+242"/>
        <source>Contact Address Formatting</source>
        <translation>Kontaktadresse formatieren</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Default Address Schema for Persons:</source>
        <translation>Default-Adressschema für Personen:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Default Address Schema for Organizations:</source>
        <translation>Default-Adressschema für Organisationen:</translation>
    </message>
    <message>
        <location line="-295"/>
        <source>Views</source>
        <translation>Ansichten</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>Default View for Contact Communications List:</source>
        <translation>Default-Ansicht für Kontakt-Kommunikationsliste:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Save Last Selected View/Tree On Program Exit</source>
        <translation>Letzte verwendete Ansicht/Baum merken</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Build Complete Contact List After Startup</source>
        <translation>Vollständige Kontaktliste beim Login aufbauen</translation>
    </message>
    <message>
        <location line="-72"/>
        <source>Default Actual Contact List View:</source>
        <translation>Default-Ansicht für aktuelle Kontaktliste:</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Default Group List View:</source>
        <translation>Default-Gruppenansicht:</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Default Group Tree:</source>
        <translation>Default-Gruppenbaum:</translation>
    </message>
    <message>
        <location line="+359"/>
        <source>Select Outlook Folders to be Scanned</source>
        <translation>Zu überwachende Outlook-Verzeichnisse auswählen</translation>
    </message>
    <message>
        <location line="-209"/>
        <source>Exclude Contact Group:</source>
        <translation>Kontaktgruppe ausschliessen:</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Contact Assignment Default Roles</source>
        <translation>Default-Rolle für Kontaktzuweisung</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Default Role For Contact Assignment:</source>
        <translation>Default-Rolle für Kontaktzuweisung:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Default Role For Person Assignment:</source>
        <translation>Default-Rolle für Benutzerzuweisung:</translation>
    </message>
</context>
<context>
    <name>Settings_DocDefaults</name>
    <message>
        <location filename="settings_docdefaults.cpp" line="+21"/>
        <source>Select Path</source>
        <translation>Pfad wählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Path</source>
        <translation>Pfad entfernen</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Default Directory For Temporary Documents</source>
        <translation>Default-Verzeichnis für temporäre Dokumente</translation>
    </message>
</context>
<context>
    <name>Settings_DocDefaultsClass</name>
    <message>
        <source>Settings_DocDefaults</source>
        <translation type="obsolete">Settings_DocDefaults</translation>
    </message>
    <message>
        <location filename="settings_docdefaults.ui" line="+161"/>
        <source>Document Storage Location</source>
        <translation>Speicherort für Dokumente</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Store File Documents Locally</source>
        <translation>Dokumente lokal speichern</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Store File Documents on Internet</source>
        <translation type="unfinished">Fragen nach Speicherort: Lokal / Internet (Datenbank)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Default Directory For Temporary Documents (e.g. &apos;C:/Temp/Docs&apos;):</source>
        <translation>Defaultverzeichnis für temporäre Dokumente (z.B. &apos;C:/Temp/Docs&apos;):</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Skip Warning for Checked Out Documents</source>
        <translation>Warnung betreffend ausgecheckter Dokumente überspringen</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Ask For File Document Storage: Local⁄Internet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings_Email</name>
    <message>
        <location filename="settings_email.cpp" line="+24"/>
        <source>NONE</source>
        <translation>kein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>OpenTLS</source>
        <translation>OpenTLS</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SSL/TLS</source>
        <translation>SSL/TLS</translation>
    </message>
</context>
<context>
    <name>Settings_EmailClass</name>
    <message>
        <location filename="settings_email.ui" line="+20"/>
        <source>Outlook Settings</source>
        <translation>Einstellungen Outlook</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Outgoing Server (SMTP)</source>
        <translation>Ausgangs-Server (SMTP)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Name/port:</source>
        <translation>Name/Port:</translation>
    </message>
    <message>
        <source>99999; </source>
        <translation type="obsolete">99999;  </translation>
    </message>
    <message>
        <location line="+22"/>
        <source>SMTP requires Authentication</source>
        <translation>SMTP mit Authentifizierung</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Account Name:</source>
        <translation>Konto-Name:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Email:</source>
        <translation>E-Mail:</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Send Copies of Serial Emails:</source>
        <translation>Kopie versendeter Serien-Emails senden:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>CC:</source>
        <translation>CC:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>BCC:</source>
        <translation>BCC:</translation>
    </message>
    <message>
        <location line="-115"/>
        <source>99999</source>
        <translation>99999</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Security:</source>
        <translation>Sicherheit:</translation>
    </message>
</context>
<context>
    <name>Settings_EmailOutClass</name>
    <message>
        <location filename="settings_email_out.ui" line="+20"/>
        <source>Outlook Settings</source>
        <translation>Einstellungen Outlook</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Account</source>
        <translation>Konto</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Email:</source>
        <translation>E-Mail:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+43"/>
        <location line="+44"/>
        <location line="+25"/>
        <location line="+25"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Email Provider</source>
        <translation>E-Mail-Provider</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Preset:</source>
        <translation>Voreinstellung:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Incoming Server</source>
        <translation>Eingangsserver</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Name/port:</source>
        <translation>Name/Port:</translation>
    </message>
    <message>
        <source>99999; </source>
        <translation type="obsolete">99999; </translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Protocol:</source>
        <translation>Protokoll:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>POP3</source>
        <translation>POP3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>IMAP</source>
        <translation>IMAP</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Security:</source>
        <translation>Sicherheit:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>User Name:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Test Connection</source>
        <translation>Verbindung testen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Only Download Emails From Known Sender</source>
        <translation>Nur E-Mails von bekannten Absendern herunterladen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Check every (min):</source>
        <translation>Abrufintervall (in Min.):</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Download E-Mails After:</source>
        <translation>E-Mails herunterladen nach:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Original emails remain on mail server!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Originamails verbleiben auf dem Server!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="-356"/>
        <source>Active Account</source>
        <translation>E-Mail-Konto aktiv</translation>
    </message>
    <message>
        <location line="+209"/>
        <location line="+111"/>
        <location line="+24"/>
        <source>99999</source>
        <translation>99999</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Delete Emails After Days (0-never):</source>
        <translation>E-Mails nach n Tagen löschen (0=nie):</translation>
    </message>
</context>
<context>
    <name>Settings_Email_Out</name>
    <message>
        <location filename="settings_email_out.cpp" line="+38"/>
        <source>NONE</source>
        <translation>kein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>OpenTLS</source>
        <translation>OpenTLS</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SSL/TLS</source>
        <translation>SSL/TLS</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OTHER</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location line="+281"/>
        <source>Check time was too low!
Now increased to the minimal value (2 min).</source>
        <translation>Prüfintervall zu kurz!
Auf Minimalwert gesetzt (2 Min.).</translation>
    </message>
    <message>
        <location line="+129"/>
        <source>Connection failed: %1!</source>
        <translation>Keine Verbindung: %1!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connection success!</source>
        <translation>Verbindung erfolgreich!</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Failed to connect to POP3 host</source>
        <translation>Keine Verbindung zum POP3-Host</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+14"/>
        <location line="+20"/>
        <location line="+23"/>
        <source>POP3 error: </source>
        <translation>PO3 Fehler: </translation>
    </message>
    <message>
        <location line="-48"/>
        <source>Failed to arrange TLS connection</source>
        <translation>Fehler beim Herstellen einer TLS-Verbindung</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Failed to establish TLS connection: </source>
        <translation>Fehler beim Herstellen einer TLS-Verbindung: </translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Failed sending USER command</source>
        <translation>Fehler beim Senden des Befehls USER</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+14"/>
        <location line="+90"/>
        <source>Invalid user name or password</source>
        <translation>Benutzername oder Passwort ungültig</translation>
    </message>
    <message>
        <location line="-96"/>
        <source>Failed sending PASS command</source>
        <translation>Fehler beim Senden des Befehls PASS</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Failed to connect to IMAP host</source>
        <translation>Keine Verbindung zum IMAP-Host</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>IMAP error: </source>
        <translation>IMAP Fehler:</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Failed to arrange TLS connection to IMAP host</source>
        <translation>Fehler beim Herstellen einer TLS-Verbindung zum IMAP Host</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>TLS error: </source>
        <translation>TLS Fehler: </translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Failed to establish TLS connection to IMAP host: </source>
        <translation>Fehler beim Herstellen einer TLS-Verbindung zum IMAP Host: </translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Login error: </source>
        <translation>Fehler beim Login: </translation>
    </message>
    <message>
        <source>Check time for Google account was too low!
Now increased to the minimal value (15 min).</source>
        <translation type="obsolete">Abrufperiode für E-Mails zu kurz! auf 15 Min. geändert.</translation>
    </message>
    <message>
        <location line="-356"/>
        <source>Check time for Google account was too low!
Now increased to the minimal value (12 min).</source>
        <translation type="unfinished">Abrufperiode für E-Mails zu kurz! auf 15 Min. geändert. {12 ?}</translation>
    </message>
</context>
<context>
    <name>Settings_Outlook</name>
    <message>
        <location filename="settings_outlook.cpp" line="+229"/>
        <source>%1 folders selected</source>
        <translation>%1 Verzeichnis(se) ausgewählt</translation>
    </message>
</context>
<context>
    <name>Settings_OutlookClass</name>
    <message>
        <location filename="settings_outlook.ui" line="+20"/>
        <source>Outlook Settings</source>
        <translation>Einstellungen Outlook</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Automatically Import New Emails From The Outlook Installation on the Actual Workstation</source>
        <translation>Neue E-Mails auf diesem Rechner automatisch aus Outlook importieren</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Last Scanned:</source>
        <translation>Letzte Synchronisation:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>9999; </source>
        <translation>9999; </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Scan Period (minutes):</source>
        <translation>Synchronisieren nach (Minuten):</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Select Outlook Folders to be Scanned</source>
        <translation>Zu überwachende Outlook-Verzeichnisse auswählen</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Synchonize Only Emails From Known Contacts</source>
        <translation>Nur E-Mails von bekannten Kontakten synchronisieren</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Reset Folders</source>
        <translation>Verzeichnisauswahl zurücksetzen</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Synchronize Outgoing Emails Only</source>
        <translation>Nur ausgehende E-Mails synchronisieren</translation>
    </message>
</context>
<context>
    <name>Settings_ProgramUpdatesClass</name>
    <message>
        <location filename="settings_programupdates.ui" line="+13"/>
        <source>Settings_ProgramUpdates</source>
        <translation>Settings_ProgramUpdates</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enable Check for Updates</source>
        <translation>Auf verfügbare Updates prüfen</translation>
    </message>
</context>
<context>
    <name>Settings_Projects</name>
    <message>
        <location filename="settings_projects.cpp" line="+110"/>
        <source>(none)</source>
        <translation>(none)</translation>
    </message>
</context>
<context>
    <name>Settings_ProjectsClass</name>
    <message>
        <source>Settings_Projects</source>
        <translation type="obsolete">Settings_Projects</translation>
    </message>
    <message>
        <location filename="settings_projects.ui" line="+21"/>
        <source>Views</source>
        <translation>Ansichten</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Default View for Project Communications List:</source>
        <translation>Default-Ansicht für Projekt-Kommunikationsliste:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Stored Project List</source>
        <translation>Gespeicherte Projektliste</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Default Project List:</source>
        <translation>Standard-Projektliste:</translation>
    </message>
</context>
<context>
    <name>Settings_Thunderbird</name>
    <message>
        <location filename="settings_thunderbird.cpp" line="+155"/>
        <source>%1 folders selected</source>
        <translation>%1 Verzeichnis(se) ausgewählt</translation>
    </message>
</context>
<context>
    <name>Settings_ThunderbirdClass</name>
    <message>
        <location filename="settings_thunderbird.ui" line="+20"/>
        <source>Thunderbird Settings</source>
        <translation>Einstellungen Thunderbird</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Automatically Import New Emails From The Thunderbird Installation on the Actual Workstation</source>
        <translation>Neue E-Mails auf diesem Rechner automatisch aus Thunderbird importieren</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Last Scanned:</source>
        <translation>Letzte Synchronisation:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>9999; </source>
        <translation>9999; </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Scan Period (minutes):</source>
        <translation>Synchronisieren alle (Minuten):</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Synchonize Only Emails From Known Contacts</source>
        <translation>Nur E-Mails von bekannten Kontakten synchronisieren</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Select Thunderbird Folders to be Scanned</source>
        <translation> Zu überwachende Thunderbird-Verzeichnisse auswählen</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Synchronize Outgoing Emails Only</source>
        <translation>Nur ausgehende E-Mails synchronisieren</translation>
    </message>
</context>
<context>
    <name>SideBarFui_Base</name>
    <message>
        <location filename="sidebarfui_base.cpp" line="-157"/>
        <source>Communication</source>
        <translation>Kommunikation</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>Email Manager</source>
        <translation>E-Mail-Manager</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Make Phone Call</source>
        <translation>Anrufen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Document Manager</source>
        <translation>Dokumenten-Manager</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Send SMS</source>
        <translation>SMS senden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Application Manager</source>
        <translation>Anwendungs-Manager</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Calendar Manager</source>
        <translation>Kalender-Manager</translation>
    </message>
</context>
<context>
    <name>SideBarFui_Contact</name>
    <message>
        <location filename="sidebarfui_contact.cpp" line="+108"/>
        <source>Contact List</source>
        <translation>Kontaktliste</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+56"/>
        <source>Add Contact</source>
        <translation>Kontakt erfassen</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Clear List</source>
        <translation>Liste leeren</translation>
    </message>
    <message>
        <source>Actual Contact</source>
        <translation type="obsolete">Aktiver Kontakt</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Edit Contact</source>
        <translation>Kontakt bearbeiten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete Contact</source>
        <translation>Kontakt löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>View Contact Detail</source>
        <translation>Kontakt-Details zeigen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show/Hide Contact Picture</source>
        <translation>Bild zeigen/verstecken</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Reload Contact Data</source>
        <translation>Kontaktdaten neu laden</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Contact Quick Info</source>
        <translation>Quick Info Kontakt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact Documents &amp; Communication History</source>
        <translation>Kontakt-Dokumente &amp; Kommunikationsprotokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Assigned Users &amp; Contacts</source>
        <translation>Zugewiesene Benutzer &amp; Kontakte</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+93"/>
        <source>Assigned Projects</source>
        <translation>Zugewiesene Projekte</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Copy Internet Address</source>
        <translation>Internet-Adresse in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Address</source>
        <translation>Adresse in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Email</source>
        <translation>E-Mail in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Phone</source>
        <translation>Telefonnummer in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Assigned Users And Contacts</source>
        <translation>Zugewiesene Benutzer und Kontakte</translation>
    </message>
    <message>
        <location line="+755"/>
        <source>Do you really want to open %1 cards?</source>
        <translation>Wollen Sie wirklich %1 Kontaktkarten öffnen?</translation>
    </message>
    <message>
        <location line="-992"/>
        <source>Print Selected Contacts</source>
        <translation>Ausgewählte Kontakte drucken</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Print Contact Detail</source>
        <translation>Kontakt-Details drucken</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Copy Name to Clipboard</source>
        <translation>Namen in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Groups</source>
        <translation>Gruppen</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Tree</source>
        <translation>Baum</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>From</source>
        <translation>Von</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>To</source>
        <translation>Bis</translation>
    </message>
    <message>
        <location line="+1211"/>
        <location line="+12"/>
        <location line="+54"/>
        <location line="+41"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>Error while fetching saved data. Please refresh contact data!</source>
        <translation>Fehler beim Laden gespeicherter Daten. Bitte aktualisieren Sie Ihre Ansicht der Kontaktdaten!</translation>
    </message>
    <message>
        <location line="-1385"/>
        <source>Active Subject</source>
        <translation>Aktiver Kontakt</translation>
    </message>
</context>
<context>
    <name>SideBarFui_ContactClass</name>
    <message>
        <location filename="sidebarfui_contact.ui" line="+14"/>
        <source>SideBarFui_Contact</source>
        <translation>SideBarFui_Contact</translation>
    </message>
    <message>
        <location line="+698"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Change Period In Which Contact Is Active In Group</source>
        <translation>Gültigkeitsperiode der Gruppenzuweisung bearbeiten</translation>
    </message>
</context>
<context>
    <name>SideBarFui_Desktop</name>
    <message>
        <location filename="sidebarfui_desktop.cpp" line="+59"/>
        <source>Workspace</source>
        <translation>Workspace</translation>
    </message>
</context>
<context>
    <name>SideBarFui_DesktopClass</name>
    <message>
        <location filename="sidebarfui_desktop.ui" line="+19"/>
        <source>SideBarFui_Desktop</source>
        <translation>SideBarFui_Desktop</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+62"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
<context>
    <name>SideBarFui_Project</name>
    <message>
        <location filename="sidebarfui_project.cpp" line="+80"/>
        <source>Project List</source>
        <translation>Projektbaum</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+50"/>
        <source>Add Project</source>
        <translation>Projekt erfassen</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Actual Project</source>
        <translation>Aktuelles Projekt</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Edit Project</source>
        <translation>Projekt bearbeiten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete Project</source>
        <translation>Projekt löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>View Project Detail</source>
        <translation>Projekt-Details zeigen</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Reload Project Data</source>
        <translation>Projektdaten aktualisieren</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Project Quick Info</source>
        <translation>Quick Info Projekt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Project Documents &amp; Communication History</source>
        <translation>Project Documents &amp; Communication History</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Assigned Users &amp; Contacts</source>
        <translation>Zugewiesene Benutzer &amp; Kontakte</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Assigned Users And Contacts</source>
        <translation>Zugewiesene Benutzer und Kontakte</translation>
    </message>
    <message>
        <location line="-189"/>
        <source>Print Selected Projects</source>
        <translation>Ausgewählte Pfojekte drucken</translation>
    </message>
    <message>
        <source>Loading Data...</source>
        <translation type="obsolete">Daten werden geladen...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Saving Data...</source>
        <translation type="obsolete">Daten werden gespeichert...</translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+55"/>
        <source>New From Template</source>
        <translation>Neu aus Vorlage</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Copy Name to Clipboard</source>
        <translation>Namen in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
</context>
<context>
    <name>SideBarFui_ProjectClass</name>
    <message>
        <location filename="sidebarfui_project.ui" line="+13"/>
        <source>SideBarFui_Project</source>
        <translation>SideBarFui_Project</translation>
    </message>
    <message>
        <location line="+275"/>
        <source>Start Date:</source>
        <translation>Startdatum:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Deadline:</source>
        <translation>Termin:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Completion Date:</source>
        <translation>Fertigstellung:</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Latest Deadline:</source>
        <translation>Letzter Termin:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Leader:</source>
        <translation>Leiter:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Responsible:</source>
        <translation>Verantwortlicher:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Department:</source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizFileSelection</name>
    <message>
        <source>Select directory</source>
        <translation type="obsolete">Verzeichnis wählen</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation type="obsolete">Datei wählen</translation>
    </message>
    <message>
        <source>Text file (*.txt)</source>
        <translation type="obsolete">Textdatei (*.txt)</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizFileSelectionClass</name>
    <message>
        <source>SimpleSelectionWizFileSelection</source>
        <translation type="obsolete">SimpleSelectionWizFileSelection</translation>
    </message>
    <message>
        <source>File Path:</source>
        <translation type="obsolete">Dateipfad:</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizFileSelectionExp</name>
    <message>
        <source>Select directory</source>
        <translation type="obsolete">Verzeichnis wählen</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation type="obsolete">Datei wählen</translation>
    </message>
    <message>
        <source>Text file (*.txt)</source>
        <translation type="obsolete">Textdatei (*.txt)</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizFileSelectionExpClass</name>
    <message>
        <source>SimpleSelectionWizFileSelectionExp</source>
        <translation type="obsolete">SimpleSelectionWizFileSelectionExp</translation>
    </message>
    <message>
        <source>File Path:</source>
        <translation type="obsolete">Dateipfad:</translation>
    </message>
    <message>
        <source>Include SOKRATES(R) Header</source>
        <translation type="obsolete">SOKRATES®-Kopfzeile einfügen</translation>
    </message>
    <message>
        <source>Add Column Headers</source>
        <translation type="obsolete">Spaltenüberschriften hinzufügen</translation>
    </message>
    <message>
        <source>Export for Outlook(R)</source>
        <translation type="obsolete">Export nach Outlook®</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizPageClass</name>
    <message>
        <source>Selection Page</source>
        <translation type="obsolete">Auswahl Seite</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation type="obsolete">&gt;&gt;</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
    <message>
        <source>&lt;&lt;</source>
        <translation type="obsolete">&lt;&lt;</translation>
    </message>
</context>
<context>
    <name>StoredProjListsDlgClass</name>
    <message>
        <source>Stored Project Lists</source>
        <translation type="obsolete">Gespeicherte Projektlisten</translation>
    </message>
    <message>
        <source>Select a stored project list:</source>
        <translation type="obsolete">Projektliste auswählen:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>StoredProjListsEditor</name>
    <message>
        <source>Project Lists</source>
        <translation type="obsolete">Projektlisten</translation>
    </message>
    <message>
        <source>Actual Project Lists</source>
        <translation type="obsolete">Aktuelle Projektliste</translation>
    </message>
    <message>
        <source>Load:</source>
        <translation type="obsolete">Laden:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <source>Loading Data...</source>
        <translation type="obsolete">Daten werden geladen...</translation>
    </message>
    <message>
        <source>Read In Progress</source>
        <translation type="obsolete">Operation wird ausgeführt</translation>
    </message>
</context>
<context>
    <name>Table_ContactAddress</name>
    <message>
        <location filename="table_contactaddress.cpp" line="-310"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Middle Name</source>
        <translation>Zweiter Vorname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Street</source>
        <translation>Strasse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZIP Code</source>
        <translation>Postleitzahl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>City</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Region</source>
        <translation>Kanton</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PO BOX</source>
        <translation>Postfach</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PO BOX ZIP</source>
        <translation>PLZ Postfach</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Office Code</source>
        <translation>Raumnummer</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Default Type</source>
        <translation>Standard-Typ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Set as Default</source>
        <translation>Als &amp;Standard setzen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open In New Window</source>
        <translation>In neuem Fenster öffnen</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>Add New Address</source>
        <translation>&amp;Neue Adresse hinzufügen</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation type="obsolete">Ins</translation>
    </message>
    <message>
        <source>&amp;Delete Selection</source>
        <translation type="obsolete">Auswahl &amp;löschen</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="obsolete">Del</translation>
    </message>
    <message>
        <source>Delete &amp;All</source>
        <translation type="obsolete">&amp;Alles löschen</translation>
    </message>
    <message>
        <source>CTRL+S</source>
        <translation type="obsolete">CTRL+S</translation>
    </message>
    <message>
        <source>Duplicate &amp;Selection</source>
        <translation type="obsolete">Auswahl &amp;Duplizieren</translation>
    </message>
    <message>
        <source>CTRL+D</source>
        <translation type="obsolete">CTRL+D</translation>
    </message>
    <message>
        <source>&amp;Select All</source>
        <translation type="obsolete">&amp;Alles auswählen</translation>
    </message>
    <message>
        <source>CTRL+A</source>
        <translation type="obsolete">CTRL+A</translation>
    </message>
    <message>
        <source>D&amp;eSelect All</source>
        <translation type="obsolete">Auswahl &amp;aufheben</translation>
    </message>
    <message>
        <source>CTRL+E</source>
        <translation type="obsolete">CTRL+E</translation>
    </message>
    <message>
        <source>S&amp;ort</source>
        <translation type="obsolete">&amp;Sortieren</translation>
    </message>
</context>
<context>
    <name>Table_ContactEmail</name>
    <message>
        <location filename="table_contactemail.cpp" line="-112"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Default Type</source>
        <translation>Default-Art</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Send Email</source>
        <translation>E-Mail senden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Create/Edit Email Type</source>
        <translation>E-Mail-Art erfassen/bearbeiten</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Set as Default</source>
        <translation>Als &amp;Default setzen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Add New Email</source>
        <translation>Neue E-Mail-Adresse hinzufügen</translation>
    </message>
</context>
<context>
    <name>Table_ContactInternet</name>
    <message>
        <location filename="table_contactinternet.cpp" line="-134"/>
        <source>Internet Address</source>
        <translation>Internet-Adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Default Type</source>
        <translation>Default-Art</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Open Website</source>
        <translation>Webseite öffnen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Create/Edit Internet Type</source>
        <translation>Internet-Typ erfassen/bearbeiten</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Set as Default</source>
        <translation>Als &amp;Default setzen</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Add New Internet</source>
        <translation>Internet-Adresse hinzufügen</translation>
    </message>
</context>
<context>
    <name>Table_ContactJournal</name>
    <message>
        <location filename="table_contactjournal.cpp" line="+32"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Is Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Person</source>
        <translation>Person</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Add New Entry</source>
        <translation>&amp;Neuen Eintrag hinzufügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Show Entry</source>
        <translation>Eintrag anzeigen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Edit Entry</source>
        <translation>Eintrag &amp;bearbeiten</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete Selected Journal Entries From Database</source>
        <translation>Ausgewählte Journaleinträge aus Datenbank löschen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>S&amp;ort</source>
        <translation>&amp;Sortieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location line="+38"/>
        <location line="+183"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-125"/>
        <location line="+73"/>
        <location line="+45"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-118"/>
        <location line="+73"/>
        <source>Please select journal entry!</source>
        <translation>Auswahl Journaleintrag!</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Add Journal Entry</source>
        <translation>Journaleintrag hinzufügen</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Do you really want to delete the selected journal entries {</source>
        <translation>Wollen Sie wirklich die ausgewählten Journaleinträge endgültig aus der Datenbank löschen {</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>} from the database?</source>
        <translation>} aus der Datenbank?</translation>
    </message>
</context>
<context>
    <name>Table_ContactPhone</name>
    <message>
        <location filename="table_contactphone.cpp" line="-282"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phone</source>
        <translation>Telefonnummer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Country Predial</source>
        <translation>Landesvorwahl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Area Predial</source>
        <translation>Regionsvorwahl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Local Number</source>
        <translation>Lokale Nummer</translation>
    </message>
    <message>
        <source>Default Type</source>
        <translation type="obsolete">Default-Art</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>Format Phone Number</source>
        <translation>Telefonnummer formatieren</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Set as Default</source>
        <translation>Als &amp;Default setzen</translation>
    </message>
    <message>
        <location line="-15"/>
        <source>Make Phone Call</source>
        <translation>Anrufen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Create/Edit Phone Type</source>
        <translation>Anruftyp erfassen/bearbeiten</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Can not format phone of this type!</source>
        <translation>Telefonnummern dieses Typs können nicht formatiert werden!</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Add New Phone</source>
        <translation>Telefonnummer hinzufügen</translation>
    </message>
    <message>
        <location line="-167"/>
        <source>Default Phone</source>
        <translation>Default-Telefonnummer</translation>
    </message>
</context>
<context>
    <name>Table_ContactPicture</name>
    <message>
        <location filename="table_contactpicture.cpp" line="+24"/>
        <source>Picture</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Default Type</source>
        <translation>Default-Art</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>&amp;Set as Default</source>
        <translation>Als &amp;Default setzen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open In New Window</source>
        <translation>In neuem Fenster öffnen</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Add New Picture</source>
        <translation>Bild hinzufügen</translation>
    </message>
</context>
<context>
    <name>Table_ContactType</name>
    <message>
        <location filename="table_contacttype.cpp" line="-294"/>
        <location line="+72"/>
        <location line="+19"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
    <message>
        <location line="-90"/>
        <location line="+74"/>
        <location line="+19"/>
        <source>System Type</source>
        <translation>System-Typ</translation>
    </message>
    <message>
        <location line="-92"/>
        <location line="+76"/>
        <location line="+19"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location line="-94"/>
        <location line="+77"/>
        <location line="+19"/>
        <source>Icon</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+80"/>
        <location line="+20"/>
        <source>Invite Email Template</source>
        <translation>E-Mail-Vorlage für Termineinladung</translation>
    </message>
    <message>
        <location line="-80"/>
        <source>Default Type</source>
        <translation>Default-Typ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>&amp;Set as Default</source>
        <translation>Als &amp;Default setzen</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Format Phone</source>
        <translation>Telefonnummer formatieren</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Add New Type</source>
        <translation>Neuen Typ hinzufügen</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>Only one user defined type can have the selected system type!</source>
        <translation>Nur ein benutzerdefinierter Typ kann den ausgwählten Systemtyp zugewiesen haben!</translation>
    </message>
</context>
<context>
    <name>Table_ContactsGrid</name>
    <message>
        <location filename="table_contactsgrid.cpp" line="+77"/>
        <source>Modify View</source>
        <translation>Ansicht definieren</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Show Contact Details</source>
        <translation>Kontakt-Details anzeigen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Contact Details In New Window</source>
        <translation>Kontakt-Details anzeigen in neuem Fenster</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete Selected Contacts From Database</source>
        <translation>Ausgewählte Kontakte aus Datenbank löschen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Clear List</source>
        <translation>Liste löschen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Remove Selected Contacts From List</source>
        <translation>Ausgewählte Kontakte aus Liste löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>S&amp;ort</source>
        <translation>&amp;Sortieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <source>Reading Detailed Contact List...</source>
        <translation type="obsolete">Detaillierte aktuelle Kontaktliste wird eingelesen...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Read In Progress</source>
        <translation type="obsolete">Operation wird ausgeführt</translation>
    </message>
    <message>
        <location line="+241"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Do you really want to delete the selected contacts (%1) from the database?</source>
        <translation>Wollen Sie wirklich die ausgewählten Kontakte (%1) endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <source>} from the database?</source>
        <translation type="obsolete">} aus der Datenbank?</translation>
    </message>
</context>
<context>
    <name>Table_ContactsGroup</name>
    <message>
        <location filename="table_contactsgroup.cpp" line="+85"/>
        <source>Modify View</source>
        <translation>Ansicht definieren</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Show Contact Details</source>
        <translation>Kontakt-Details anzeigen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show Contact Details In New Window</source>
        <translation>Kontakt-Details anzeigen in neuem Fenster anzeigen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete Selected Contacts From Database</source>
        <translation>Ausgewählte Kontakte aus der Datenbank löschen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Remove All Contacts From Group</source>
        <translation>Alle Kontakte aus der Liste entfernen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Remove Selected Contacts From Group</source>
        <translation>Ausgewählte Kontakte aus der Liste entfernen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Add Selected Contacts To Actual Contact List</source>
        <translation>Ausgewählte Kontakte zur aktuellen Kontaktliste hinzufügen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Enter Period In Which Contact Is Active In Group</source>
        <translation>Gültigkeitsperiode der Gruppenzuweisung bearbeiten</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Select All</source>
        <translation>&amp;Alles auswählen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>S&amp;ort</source>
        <translation>&amp;Sortieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location line="+184"/>
        <location line="+47"/>
        <location line="+14"/>
        <location line="+13"/>
        <location line="+12"/>
        <location line="+62"/>
        <location line="+151"/>
        <location line="+39"/>
        <location line="+27"/>
        <location line="+63"/>
        <location line="+56"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-345"/>
        <location line="+29"/>
        <location line="+13"/>
        <location line="+111"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="-153"/>
        <source>Please select group first!</source>
        <translation>Wählen Sie zuerst eine Gruppe aus!</translation>
    </message>
    <message>
        <location line="+153"/>
        <source>Do you really want to delete the selected contacts (%1) from the database?</source>
        <translation>Wollen Sie wirklich die ausgewählten Kontakte (%1) endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="+174"/>
        <source>Reading Group Contact List...</source>
        <translation>Detaillierte Gruppenliste wird eingelesen...</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Read In Progress</source>
        <translation>Operation wird ausgeführt</translation>
    </message>
    <message>
        <location line="-299"/>
        <source>Can not drop contact onto the group because either group is not selected inside the Group Tree or Show Active flag is checked!</source>
        <translation>Der Kontakt konnte nicht auf die Gruppe gezogen werden. Mögliche Ursachen: Die Gruppe ist entweder innerhalb des Gruppenbaumes nicht richtig selektiert worden oder &quot;Nur Aktive zeigen&quot; ist aktiviert!</translation>
    </message>
    <message>
        <source>} from the database?</source>
        <translation type="obsolete">} aus der Datenbank?</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>You do not have sufficient access rights to perform this operation!</source>
        <translation>Kein Zugriffsrecht!</translation>
    </message>
</context>
<context>
    <name>Table_CustomFieldWidget</name>
    <message>
        <location filename="table_customfields.cpp" line="+572"/>
        <location line="+969"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
</context>
<context>
    <name>Table_CustomFields</name>
    <message>
        <location line="-1516"/>
        <source>Custom Field</source>
        <translation>Benutzerdefinierte Felder</translation>
    </message>
</context>
<context>
    <name>Table_NMRXRelation</name>
    <message>
        <location filename="table_nmrxrelation.cpp" line="+55"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>Name 1</source>
        <translation>Name 1</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>Role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Name 2</source>
        <translation>Name 2</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+12"/>
        <location line="+11"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="-24"/>
        <source>Invitation</source>
        <translation>Einladung</translation>
    </message>
</context>
<context>
    <name>Table_NMRXRelation_WidgetInvite</name>
    <message>
        <location line="+208"/>
        <source>Invite</source>
        <translation>Einladen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invitation Sent</source>
        <translation>Einladung gesendet</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed</source>
        <translation>Bestätigt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rejected</source>
        <translation>Abgelehnt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Answered</source>
        <translation>Beantwortet</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Send Invite By Sokrates</source>
        <translation>Einladung durch SOKRATES versenden</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Send Invite By Email</source>
        <translation>Einladung über E-Mail versenden</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Send Invite By SMS</source>
        <translation>Einladung über SMS versenden</translation>
    </message>
</context>
<context>
    <name>Table_NMRXRoles</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>Assigment Text</source>
        <translation type="obsolete">Zuweisungsbeschreibung</translation>
    </message>
</context>
<context>
    <name>Table_SelectionContacts</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
</context>
<context>
    <name>Table_SelectionTableBased</name>
    <message>
        <source>&amp;Reload</source>
        <translation type="obsolete">Neu l&amp;aden</translation>
    </message>
    <message>
        <source>S&amp;ort</source>
        <translation type="obsolete">&amp;Sortieren</translation>
    </message>
    <message>
        <source>CTRL+S</source>
        <translation type="obsolete">CTRL+S</translation>
    </message>
</context>
<context>
    <name>TaskDialog</name>
    <message>
        <location filename="taskdialog.cpp" line="+283"/>
        <source>Task owner must be set!</source>
        <translation>Besitzer der Aufgabe muss eingetragen sein!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Task originator must be set!</source>
        <translation>Urheber der Aufgabe muss eingetragen sein!</translation>
    </message>
</context>
<context>
    <name>TaskDialogClass</name>
    <message>
        <location filename="taskdialog.ui" line="+20"/>
        <source>Task</source>
        <translation>Aufgabe</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Scheduled</source>
        <translation>Geplant</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>In Work</source>
        <translation>In Bearbeitung</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Completed</source>
        <translation>Beendet</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>No Status</source>
        <translation>Kein Status</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Subject:</source>
        <translation>Betreff:</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Start Date/Time:</source>
        <translation>Beginn:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Task Owner:</source>
        <translation>Besitzer der Aufgabe:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Due Date/Time:</source>
        <translation>Fällig:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Originator:</source>
        <translation>Urheber:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Completion Date/Time:</source>
        <translation>Beendet:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Task Type:</source>
        <translation>Aufgaben-Typ:</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-321"/>
        <source>Priority:</source>
        <translation>Priorität:</translation>
    </message>
</context>
<context>
    <name>TaskWidget</name>
    <message>
        <location filename="taskwidget.cpp" line="+374"/>
        <source>Task owner must be set!</source>
        <translation>Besitzer der Aufgabe muss eingetragen sein!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Task originator must be set!</source>
        <translation>Urheber der Aufgabe muss eingetragen sein!</translation>
    </message>
</context>
<context>
    <name>TaskWidgetClass</name>
    <message>
        <location filename="taskwidget.ui" line="+20"/>
        <source>Task</source>
        <translation>Aufgabe</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Schedule Task</source>
        <translation>Aufgabe planen</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Subject:</source>
        <translation>Betreff:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Scheduled</source>
        <translation>Geplant</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Priority:</source>
        <translation>Priorität:</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>In Work</source>
        <translation>In Bearbeitung</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Completed</source>
        <translation>Beendet</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>No Status</source>
        <translation>Kein Status</translation>
    </message>
    <message>
        <location line="-196"/>
        <source>Originator:</source>
        <translation>Urheber:</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Task Type:</source>
        <translation>Aufgaben-Typ:</translation>
    </message>
    <message>
        <location line="-146"/>
        <source>Task Owner:</source>
        <translation>Besitzer der Aufgabe:</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Completion Date/Time:</source>
        <translation>Beendet:</translation>
    </message>
    <message>
        <location line="-146"/>
        <source>Start Date/Time:</source>
        <translation>Beginn:</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Due Date/Time:</source>
        <translation>Fällig:</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Notify Originator</source>
        <translation>Urheber benachrichtigen</translation>
    </message>
</context>
<context>
    <name>ThirdColumnDelegate</name>
    <message>
        <location filename="commgridview.h" line="+76"/>
        <source>Owner:</source>
        <translation>Besitzer:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Contact:</source>
        <translation>Kontakt:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Project:</source>
        <translation>Projekt:</translation>
    </message>
</context>
<context>
    <name>ToolBar_DateSelectorClass</name>
    <message>
        <source>Select Date</source>
        <translation type="obsolete">Auswahl Datum</translation>
    </message>
    <message>
        <source>Current Date</source>
        <translation type="obsolete">Aktuelles Datum</translation>
    </message>
    <message>
        <source>Per Date</source>
        <translation type="obsolete">Per Datum</translation>
    </message>
</context>
<context>
    <name>TreeViewDialogClass</name>
    <message>
        <source>Tree View</source>
        <translation type="obsolete">Baumansicht</translation>
    </message>
    <message>
        <source>Select tree level:</source>
        <translation type="obsolete">Stufe für Baum wählen:</translation>
    </message>
    <message>
        <source>Build Stored List</source>
        <translation type="obsolete">Gespeicherte Liste aufbauen</translation>
    </message>
    <message>
        <source>Select stored list:</source>
        <translation type="obsolete">Gespeicherte Liste auswählen:</translation>
    </message>
    <message>
        <source>Build Tree</source>
        <translation type="obsolete">Baum aufbauen</translation>
    </message>
</context>
<context>
    <name>TypeColumnDelegate</name>
    <message>
        <source>To:</source>
        <translation type="obsolete">An:</translation>
    </message>
    <message>
        <source>CC:</source>
        <translation type="obsolete">CC:</translation>
    </message>
    <message>
        <source>BCC:</source>
        <translation type="obsolete">BCC:</translation>
    </message>
</context>
<context>
    <name>UserWidget</name>
    <message>
        <location filename="userwidget.cpp" line="+383"/>
        <source>Assign roles to multiple Login Accounts</source>
        <translation>Rolle mehreren Login-Kontos zuweisen</translation>
    </message>
    <message>
        <location filename="userwidget.h" line="+31"/>
        <source>Login Account Access Rights</source>
        <translation>Zugriffsrechte Login-Konto</translation>
    </message>
</context>
<context>
    <name>UserWidgetClass</name>
    <message>
        <source>UserWidget</source>
        <translation type="obsolete">UserWidget</translation>
    </message>
    <message>
        <location filename="userwidget.ui" line="+33"/>
        <source>List of Users:</source>
        <translation>Benutzerliste:</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Select Users</source>
        <translation>Auswahl Benutzer</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete Role</source>
        <translation>Rolle löschen</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>List of Roles:</source>
        <translation>Rollenliste:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Role Description:</source>
        <translation>Rollenbeschreibung:</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Save Changes</source>
        <translation>Änderungen speichern</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>VerticalPersonItem</name>
    <message>
        <location filename="calendarmultidaygraphicsview.cpp" line="+307"/>
        <source>Define Availability</source>
        <translation>Verfügbarkeit definieren</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Mark as Final</source>
        <translation>Als &quot;endgültig&quot; kennzeichnen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Mark as Preliminary</source>
        <translation>Als &quot;vorläufig&quot; kennzeichnen</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+23"/>
        <source>Select All</source>
        <translation>Alle selektieren</translation>
    </message>
    <message>
        <location line="-20"/>
        <location line="+24"/>
        <source>Deselect All</source>
        <translation>Alle deselektieren</translation>
    </message>
</context>
<context>
    <name>VoiceCallRedirectDlgClass</name>
    <message>
        <source>Voice Call Redirection</source>
        <translation type="obsolete">Anrufumleitung</translation>
    </message>
    <message>
        <location filename="voicecallredirectdlg.ui" line="+45"/>
        <source>Select a contact to transfer the call to. When you press OK, you will be connected the the contact and you can talk to him or her before transferring the caller.
Ask the called contact to hang up the phone, his/her phone will ring immediately when you press Connect and he or her will get the transferred caller on line.</source>
        <oldsource>Select a contact to transfer the call to. When you press OK, you will be connected the the contact and you can talk to him or her before transferring the caller.
Ask the called contact to hang up the phone, his/her phone will ring immediately when you press Connect and he or her will get the transferred caller on line.</oldsource>
        <translation type="unfinished">Wählen Sie einen Kontakt für die Weitervermittlung des Anrufes. Nach dem Drücken von OK werden zunächst Sie mit diesem Kontakt verbunden.
Bitten Sie ihn aufzulegen. Sobald Sie &quot;Verbinden&quot; drücken, wird sein Telefon klingeln mit dem Anrufer an der Leitung.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Contact:</source>
        <translation>Kontakt:</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Phone Number:</source>
        <translation>Telefonnummer:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Interface:</source>
        <translation>Schnittstelle:</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-213"/>
        <source>Voice Call Transfer</source>
        <translation>Anrufvermittlung</translation>
    </message>
</context>
<context>
    <name>fui_avatar</name>
    <message>
        <location filename="fui_avatar.cpp" line="+148"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open on Startup</source>
        <translation>Beim Starten öffnen</translation>
    </message>
    <message>
        <source>Card: </source>
        <translation type="obsolete">Karte: </translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Project Card: </source>
        <translation>Projekt-Karte: </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Contact Card: </source>
        <translation>Kontakt-Karte: </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Task Card: </source>
        <translation>Aufgaben-Karte: </translation>
    </message>
    <message>
        <source>Todo!</source>
        <translation type="obsolete">Zu erledigen!</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Overdue</source>
        <translation>Überfällig</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Due</source>
        <translation>Fällig</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Pending</source>
        <translation>Vorgesehen</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>Do you really want to mark task as done?</source>
        <translation>Wollen Sie diese Aufgabe als erledigt markieren?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location line="-143"/>
        <source>Done/Not Scheduled</source>
        <translation>Erledigt/Nicht geplant</translation>
    </message>
</context>
<context>
    <name>fui_avatarClass</name>
    <message>
        <location filename="fui_avatar.ui" line="+19"/>
        <source>SOKRATES Card</source>
        <translation>SOKRATES Karte</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>image</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>task date</source>
        <translation>Datum Aufgabe</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>AS</source>
        <translation>AS</translation>
    </message>
    <message>
        <location line="-116"/>
        <source>Start Task</source>
        <translation>Aufgabe beginnen</translation>
    </message>
    <message>
        <location line="+132"/>
        <source>Task Done / Email Read</source>
        <translation>Aufgabe erledigt / Email gelesen</translation>
    </message>
</context>
<context>
    <name>fui_busdepartmentsClass</name>
    <message>
        <location filename="fui_busdepartments.ui" line="+13"/>
        <source>Departments</source>
        <translation>Abteilungen</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Edi&amp;t</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cost Center:</source>
        <translation>Kostenstelle:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Persons</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Open Structure in New Window</source>
        <translation>Struktur in neuem Fenster öffnen</translation>
    </message>
</context>
<context>
    <name>fui_resources</name>
    <message>
        <location filename="fui_resources.cpp" line="+174"/>
        <source>Resources</source>
        <translation>Ressourcen</translation>
    </message>
    <message>
        <location line="-133"/>
        <source>Resource Calendar Entries</source>
        <translation>Einträge im Ressourcen-Kalender</translation>
    </message>
</context>
<context>
    <name>fui_resourcesClass</name>
    <message>
        <location filename="fui_resources.ui" line="+58"/>
        <source>Show All Resources</source>
        <translation>Alle Ressourcen zeigen</translation>
    </message>
    <message>
        <location line="+153"/>
        <source>Valid From:</source>
        <translation>Gültig ab:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Location:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Valid To:</source>
        <translation>Gültig bis:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Calendar Reservations</source>
        <translation>Kalenderreservierungen</translation>
    </message>
</context>
<context>
    <name>options_calendar</name>
    <message>
        <location filename="options_calendar.ui" line="+14"/>
        <source>options_calendar</source>
        <translation>options_calendar</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>AS Start Daily Working Time</source>
        <translation>AS Beginn Arbeitstag</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>AS End Daily Working Time</source>
        <translation>AS Ende Arbeitstag</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Presence Type for &quot;preliminary&quot;</source>
        <translation>Anwesenheitsart für &quot;vorläufig&quot;</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Presence Type for &quot;final&quot;</source>
        <translation>Anwesenheitsart für &quot;endgültig&quot;</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Presence Type for &quot;available&quot;</source>
        <translation>Anwesenheitsart für &quot;verfügbar&quot;</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Confirmation Email Address:</source>
        <translation>Adresse für Bestätigungs-E-Mail:</translation>
    </message>
</context>
<context>
    <name>qc_salutationframeClass</name>
    <message>
        <location filename="qc_salutationframe.ui" line="+26"/>
        <source>qc_salutationframe</source>
        <translation></translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Salutation</source>
        <translation>Anrede</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+57"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Short Salutation</source>
        <translation>Kurzanrede</translation>
    </message>
</context>
<context>
    <name>toolbar_edit</name>
    <message>
        <location filename="toolbar_edit.cpp" line="+14"/>
        <source>Modify/Create</source>
        <translation>Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
</context>
<context>
    <name>toolbar_editClass</name>
    <message>
        <source>toolbar_edit</source>
        <translation type="obsolete">toolbar_edit</translation>
    </message>
    <message>
        <location filename="toolbar_edit.ui" line="+14"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
</context>
<context>
    <name>tree_test_serverClass</name>
    <message>
        <source>Test Server</source>
        <translation type="obsolete">Test Server</translation>
    </message>
    <message>
        <source>Time:</source>
        <translation type="obsolete">Zeit:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <source>Code:</source>
        <translation type="obsolete">Code:</translation>
    </message>
    <message>
        <source>Number of nodes:</source>
        <translation type="obsolete">Telefonnummer:</translation>
    </message>
    <message>
        <source>Level:</source>
        <translation type="obsolete">Rolle:</translation>
    </message>
    <message>
        <source>Build project list</source>
        <translation type="obsolete">Gespeicherte Liste aufbauen</translation>
    </message>
    <message>
        <source>Load stored list</source>
        <translation type="obsolete">Gespeicherte Liste aufbauen</translation>
    </message>
</context>
</TS>
