#ifndef TABLE_CONTACTTYPE_H
#define TABLE_CONTACTTYPE_H

#include "gui_core/gui_core/universaltablewidgetex.h"
#include "toolbar_edit.h"
#include "common/common/status.h"

class Table_ContactType : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_ContactType(QWidget * parent);

	void Initialize(DbRecordSet *plstData,toolbar_edit *pToolBar);
	void SetEntityType(int nEntityType,bool bAddFormatting, bool bAddSystemType,bool bAddEmailTemplate=false);
	void SetEditMode(bool bEdit=true);
	void CheckDataBeforeWrite(Status &Ret_pStatus);

private slots:
	void OnSetDefault();
	void OnComboIndexChanged(int nIndex);
	void OnRowInserted(int nRow);
	void OnDeleted();
	void OnCellDoubleClicked(int nRow,int nCol);
	void OnColorChanged(const QColor &);

protected:
	void Data2CustomWidget(int nRow, int nCol);
	void CustomWidget2Data(int nRow, int nCol);
	void GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip);	
signals:
	void EmitSetAsDefault(int);
private:
	void CreateContextMenu();
	void BuildToolBar(toolbar_edit* pToolBar);
	void RefreshDefaultFlag();
	int m_nDefaultColIdx;
	toolbar_edit *m_toolbar;
	DbRecordSet m_lstSystemTypes;
	int m_nCol_System;
	int m_nCol_Color;
	int m_nCol_Icon;
	int m_nCol_EmailTemplate;
};

#endif // TABLE_CONTACTTYPE_H
