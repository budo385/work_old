#ifndef TABLE_CONTACTADDRESSTYPES_H
#define TABLE_CONTACTADDRESSTYPES_H

#include "gui_core/gui_core/universaltablewidgetex.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "common/common/observer_ptrn.h"
#include "common/common/status.h"

class Table_ContactAddressTypes : public UniversalTableWidgetEx, ObsrPtrn_Observer
{

public:
	Table_ContactAddressTypes(QWidget *parent);

	void Initialize();
	void SetAddressRow(DbRecordSet *pRowAddress,int nCurrentRow=0);
	void CheckDataBeforeWrite(Status &Ret_pStatus);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	

private slots:
	void OnCellEdited(int nRow, int nCol);

private:
	DbRecordSet						*m_pRowAddress;
	int								m_nCurrentAddrRow; //current row inside m_pRowAddress
	DbRecordSet						m_lstTypes;
	int								m_nTypeCheckColIdx;
	DbRecordSet						*m_AddressTypes;
	MainEntitySelectionController	m_AddressTypeHandler;
	SortDataList					m_lstDefSort;		
};
	
#endif // TABLE_CONTACTADDRESSTYPES_H
