#include "fui_roledefinition.h"

#include "roleinputdialog.h"
#include "arstoroleassigndialog.h"
#include "gui_core/gui_core/gui_helper.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

FUI_RoleDefinition::FUI_RoleDefinition(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	m_bEditMode = false;
	
	SetFUIName(tr("Role Definition"));
	
	//Organize recordset.
	m_recInsertedARSToRole.addColumn(QVariant::Int, "RowID");
	m_recInsertedARSToRole.addColumn(QVariant::Int, "RoleID");
	m_recInsertedARSToRole.addColumn(QVariant::Int, "ARSID");

	InitializeButtons();

	InitContextMenu();

	//Get roles and access rights sets.
	InitializeARSWidget();
	InitializeTreeWidget();

	ui.ARS_treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

	ui.Role_treeWidget->setAcceptDrops(true);
	ui.ARS_treeWidget->setDragEnabled(true);

	connect(ui.Role_treeWidget,		SIGNAL(itemSelectionChanged()),					this, SLOT(RoleSelectionChanged()));
	connect(ui.Role_treeWidget,		SIGNAL(ARSDropped(QTreeWidgetItem *, int)),		this, SLOT(on_ARSDropped(QTreeWidgetItem *, int)));
	connect(ui.Role_treeWidget,		SIGNAL(FocusIn()),								this, SLOT(on_TreeFocusIn()));
	connect(ui.Role_treeWidget,		SIGNAL(FocusOut()),								this, SLOT(on_TreeFocusOut()));
	connect(ui.ARS_ProperyBrowser,	SIGNAL(ValueChanged(int, QVariant, QString)),	this, SLOT(ARSValueChanged(int, QVariant, QString)));
	connect(ui.ARS_treeWidget,		SIGNAL(itemSelectionChanged()),					this, SLOT(ARSSelectionChanged()));
}

FUI_RoleDefinition::~FUI_RoleDefinition()
{

}

void FUI_RoleDefinition::InitContextMenu()
{
	//Action delete.
	QAction *ActionDelete = new QAction(QIcon(":trash-16.png"), tr("Delete"), this);
	connect(ActionDelete, SIGNAL(triggered()), this, SLOT(on_DeletePushButton_clicked()));
	//Action insert new role.
	QAction *ActionInsertRole = new QAction(QIcon(":filenew.png"), tr("Insert new role"), this);
	connect(ActionInsertRole, SIGNAL(triggered()), this, SLOT(on_InsertRolePushButton_clicked()));
	//Action rename role.
	QAction *RenameRoleAction = new QAction(QIcon(":brush.png"), tr("Rename role"), this);
	connect(RenameRoleAction, SIGNAL(triggered()), this, SLOT(on_RenameRole()));

	m_lstARSAction	<< ActionDelete;
	m_lstRoleAction << RenameRoleAction << ActionDelete;
	m_lstTreeAction << ActionInsertRole;

	ui.Role_treeWidget->SetChildContextMenuActions(m_lstARSAction);
	ui.Role_treeWidget->SetParentContextMenuActions(m_lstRoleAction);
	ui.Role_treeWidget->SetWidgetContextMenuActions(m_lstTreeAction);
}

void FUI_RoleDefinition::InitializeButtons()
{
	ui.DeletePushButton->setEnabled(false);
	ui.SavePushButton->setEnabled(false);
	ui.SavePushButton->setStyleSheet("");
	ui.CancelPushButton->setEnabled(false);
	
	ui.editPushButton->setEnabled(true);
	GUI_Helper::SetWidgetFontWeight(ui.editPushButton, QFont::Bold);
	GUI_Helper::SetStyledButtonColorBkg(ui.editPushButton, GUI_Helper::BTN_COLOR_GREEN, "white");
	ui.ARS_treeWidget->setDisabled(true);
	ui.InsertRolePushButton->setDisabled(true);
	ui.ARS_ProperyBrowser->setEnabled(false);
	
	ui.BusVisibleCheckBox->setDisabled(true);
	ui.SysRoleCheckBox->setDisabled(true);

	ui.AssignToRolesPushButton->setDisabled(true);

	ui.Role_treeWidget->setContextMenuPolicy(Qt::PreventContextMenu);
}

void FUI_RoleDefinition::InitializeTreeWidget()
{
	m_RoleItemsHash.clear();
	//Get ROLES.
	//Check what version is client.
	QString strEdition = g_pClientManager->GetIniFile()->m_strModuleCode;

	if("SC-TE" == strEdition)
		_SERVER_CALL(AccessRights->GetTERoles(m_Status, m_recCORE_ROLE))
	else
		_SERVER_CALL(AccessRights->GetBERoles(m_Status, m_recCORE_ROLE))

	_CHK_ERR(m_Status);
	
//	_SERVER_CALL(AccessRights->GetRoles(m_Status, m_recCORE_ROLE))
//	_CHK_ERR(m_Status);

	int nRoleCount = m_recCORE_ROLE.getRowCount();
	for (int i = 0; i < nRoleCount; ++i)
	{
		QString RoleName = m_recCORE_ROLE.getDataRef(i, "CROL_NAME").toString();
		QString RoleDesc = m_recCORE_ROLE.getDataRef(i, "CROL_ROLE_DESC").toString();
		int RowID		 = m_recCORE_ROLE.getDataRef(i, "CROL_ID").toInt();
		int RoleType	 = m_recCORE_ROLE.getDataRef(i, "CROL_ROLE_TYPE").toInt();

		QTreeWidgetItem *Item = new QTreeWidgetItem();
		QString DisplayData = RoleName; //RoleName + " " + RoleDesc;
		//Set display data.
		Item->setData(0, Qt::UserRole,		RowID);
		Item->setData(1, Qt::UserRole,		RoleType);
		Item->setData(2, Qt::UserRole,		RoleDesc);
		Item->setData(0, Qt::DisplayRole,	DisplayData);
		
		//If system role.
		if (RoleType)
			Item->setIcon(0, QIcon(":MainDataCyBl.jpg"));
		else
			Item->setIcon(0, QIcon(":OptionsCyBl.jpg"));

		m_RoleItemsHash.insert(RowID, Item);
	}

	//Get CORE_ACCRSETROLE.
	_SERVER_CALL(ClientSimpleORM->Read(m_Status, CORE_ACCRSETROLE, m_recCORE_ACCRSETROLE))
	_CHK_ERR(m_Status);

	_SERVER_CALL(ClientSimpleORM->Read(m_Status, CORE_ACCESSRIGHTS, m_recCORE_ACCESSRIGHTS))
	_CHK_ERR(m_Status);

	TranslateAR(m_recCORE_ACCESSRIGHTS);


	//Get roles access rights.
	QHashIterator<int, QTreeWidgetItem*> iter(m_RoleItemsHash);
	while (iter.hasNext())
	{
		iter.next();
		int RoleRowID = iter.key();
		QTreeWidgetItem *RoleItem = iter.value();
		
		DbRecordSet tmpARRec;
		m_recCORE_ACCRSETROLE.find(3, RoleRowID);
		tmpARRec.copyDefinition(m_recCORE_ACCRSETROLE);
		tmpARRec.merge(m_recCORE_ACCRSETROLE, true);

		int nARCount = tmpARRec.getRowCount();
		for (int j = 0; j < nARCount; ++j)
		{
			//Get ID.
			int ARRowId	   = tmpARRec.getDataRef(j, 0).toInt();
			int row = tmpARRec.find(0, ARRowId, true);
			int arsID = tmpARRec.getDataRef(row, 4).toInt();
			m_recCORE_ACCRSET.clearSelection();
			m_recCORE_ACCRSET.find(0, arsID);
			DbRecordSet tmpARSRecordSet;
			tmpARSRecordSet.copyDefinition(m_recCORE_ACCRSET);
			tmpARSRecordSet.merge(m_recCORE_ACCRSET, true);

			QString ARName = tmpARSRecordSet.getDataRef(0, 3).toString();
			QString ARDesc = tmpARSRecordSet.getDataRef(0, 4).toString();
			int ARType	   = tmpARSRecordSet.getDataRef(0, 5).toInt();

			QTreeWidgetItem *ARSItem = new QTreeWidgetItem();
			QString DispData = ARName; //ARName + " " + ARDesc;
			//Set display data.
			ARSItem->setData(0, Qt::UserRole,		ARRowId);
			ARSItem->setData(1, Qt::UserRole,		ARType);
			ARSItem->setData(2, Qt::UserRole,		ARDesc);
			ARSItem->setData(0, Qt::DisplayRole,	DispData);

			if (ARType)
				ARSItem->setIcon(0, QIcon(":handwrite.png"));
			else
				ARSItem->setIcon(0, QIcon(":handwithbook.png"));

			RoleItem->addChild(ARSItem);
		}
	}

	//Setup tree.
	ui.Role_treeWidget->addTopLevelItems(m_RoleItemsHash.values());
}

void FUI_RoleDefinition::InitializeARSWidget()
{
	_SERVER_CALL(AccessRights->GetAccessRightsSets(m_Status, m_recCORE_ACCRSET))
	_CHK_ERR(m_Status);

	TranslateARSet(m_recCORE_ACCRSET);

	QList<QTreeWidgetItem*> ARList;
		
	int nARCount = m_recCORE_ACCRSET.getRowCount();
	for (int i = 0; i < nARCount; ++i)
	{
		QString ARName = m_recCORE_ACCRSET.getDataRef(i, 3).toString();
		QString ARDesc = m_recCORE_ACCRSET.getDataRef(i, 4).toString();
		int RowId	   = m_recCORE_ACCRSET.getDataRef(i, 0).toInt();
		int ARType	   = m_recCORE_ACCRSET.getDataRef(i, 5).toInt();
		int ARCode	   = m_recCORE_ACCRSET.getDataRef(i, 6).toInt();

		QTreeWidgetItem *Item = new QTreeWidgetItem();
		QString DisplayData = ARName; // + " " + ARDesc;
		//Set display data.
		Item->setData(0, Qt::UserRole,		RowId);
		Item->setData(1, Qt::UserRole,		ARType);
		Item->setData(0, Qt::DisplayRole,	DisplayData);

		if (ARType)
			Item->setIcon(0, QIcon(":handwrite.png"));
		else
			Item->setIcon(0, QIcon(":handwithbook.png"));

		ARList << Item;
	}

	ui.ARS_treeWidget->addTopLevelItems(ARList);
}

void FUI_RoleDefinition::RenameRole(int RoleID, QString RoleName, QString RoleDescr, int RoleType)
{
	RoleInputDialog *RoleInput = new RoleInputDialog(&RoleName, &RoleDescr, &RoleType, false);
	if (RoleInput->exec())
	{
		//If not already renamed, not deleted and not a new role.
		if (!m_lstRenamedRoles.contains(RoleID) && RoleID >= 0)
			m_lstRenamedRoles << RoleID;

		QTreeWidgetItem *roleItem = m_RoleItemsHash.value(RoleID);
		roleItem->setData(0, Qt::DisplayRole,	RoleName);
		roleItem->setData(1, Qt::UserRole,		RoleType);
		roleItem->setData(2, Qt::UserRole,		RoleDescr);
	}

	delete(RoleInput);
	ChangeSaveState();
}

void FUI_RoleDefinition::InsertRole(QString RoleName, QString RoleDescr, int RoleType)
{
	//First let's get it some fake ID (it will be negative number).
	int	nRoleID = -1;
	while (m_lstInsertedRoles.contains(nRoleID))
		nRoleID--;

	//Then we make item and put it in tree.
	QTreeWidgetItem *Item = new QTreeWidgetItem();
	QString DisplayData = RoleName;
	//Set display data.
	Item->setData(0, Qt::UserRole,		nRoleID);
	Item->setData(1, Qt::UserRole,		RoleType);
	Item->setData(2, Qt::UserRole,		RoleDescr);
	Item->setData(0, Qt::DisplayRole,	DisplayData);

	//If system role.
	if (RoleType)
		Item->setIcon(0, QIcon(":MainDataCyBl.jpg"));
	else
		Item->setIcon(0, QIcon(":OptionsCyBl.jpg"));

	//Add it to tree.
	ui.Role_treeWidget->addTopLevelItem(Item);
	//Insert in role items hash.
	m_RoleItemsHash.insert(nRoleID, Item);
	//Insert in role insert list.
	m_lstInsertedRoles << nRoleID;

	ui.Role_treeWidget->clearSelection();
	Item->setSelected(true);

	ChangeSaveState();
}

void FUI_RoleDefinition::InsertARSToRole(int RoleID, int ARSRowID)
{
	//Lock role.
	if (!LockResource(RoleID))
		return;

	//Check does this role already contains this ARS.
	if (CheckIsARSinRole(RoleID, ARSRowID))
		return;

	//First let's get it some fake ID (it will be negative number).
	int	nARSIDRowID = -1;
	//Then check fake ID.
	while (m_recInsertedARSToRole.find(0, nARSIDRowID, true) >= 0)
		nARSIDRowID--;

	//Get access right set data from it's recordset.
	int rowInARSRecordSet = m_recCORE_ACCRSET.find(0, ARSRowID, true);
	QString ARName = m_recCORE_ACCRSET.getDataRef(rowInARSRecordSet, 3).toString();
	QString ARDesc = m_recCORE_ACCRSET.getDataRef(rowInARSRecordSet, 4).toString();
	int ARType	   = m_recCORE_ACCRSET.getDataRef(rowInARSRecordSet, 5).toInt();

	m_recInsertedARSToRole.addRow();
	int row = m_recInsertedARSToRole.getRowCount() - 1;
	m_recInsertedARSToRole.setData(row, 0, nARSIDRowID);
	m_recInsertedARSToRole.setData(row, 1, RoleID);
	m_recInsertedARSToRole.setData(row, 2, ARSRowID);

	//Then create access rights recordset (for property browser).
	CreateAccessRightsRecordSetForInsertedARS(RoleID, ARSRowID);
	
	//Add it to tree.
	QTreeWidgetItem *ARSItem = new QTreeWidgetItem();
	QString DispData = ARName;
	//Set display data.
	ARSItem->setData(0, Qt::UserRole,		nARSIDRowID);
	ARSItem->setData(1, Qt::UserRole,		ARType);
	ARSItem->setData(2, Qt::UserRole,		ARDesc);
	ARSItem->setData(0, Qt::DisplayRole,	DispData);
	if (ARType)
		ARSItem->setIcon(0, QIcon(":handwrite.png"));
	else
		ARSItem->setIcon(0, QIcon(":handwithbook.png"));
	//Ass ARS to role, expand and select ARS.
	m_RoleItemsHash.value(RoleID)->addChild(ARSItem);
	m_RoleItemsHash.value(RoleID)->setExpanded(true);
	ui.Role_treeWidget->clearSelection();
	ARSItem->setSelected(true);
	
	//Change save state.
	ChangeSaveState();
}

void FUI_RoleDefinition::DeleteARS(QTreeWidgetItem *Item, QTreeWidgetItem *ParentItem)
{
	Q_ASSERT(Item);
	Q_ASSERT(ParentItem);
	int RoleID	= ParentItem->data(0, Qt::UserRole).toInt();
	int AccRID	= Item->data(0, Qt::UserRole).toInt();
	int ARSID;

	//Lock role.
	if (!LockResource(RoleID))
		return;

	//If inserted get access right set id (for later to delete).
	if (AccRID < 0)
	{
		int r = m_recInsertedARSToRole.find(0, AccRID, true);
		ARSID = m_recInsertedARSToRole.getDataRef(r, 2).toInt();
	}
	else //If not inserted access rights, then it is from before.
	{
		int r = m_recCORE_ACCRSETROLE.find(0, AccRID, true);
		ARSID = m_recCORE_ACCRSETROLE.getDataRef(r, 4).toInt();
	}

	//Remove from inserted ARS to role recordset.
	m_recInsertedARSToRole.clearSelection();
	m_recInsertedARSToRole.find(0, AccRID);
	m_recInsertedARSToRole.find(1, RoleID, false, true, true);
	if (m_recInsertedARSToRole.deleteSelectedRows() == 0) //If not inserted than delete it.
	{
		m_hshDeletedARSFromRole.insert(RoleID, ARSID);
	}
	
	m_recCORE_ACCRSETROLE.find(3, RoleID);
	m_recCORE_ACCRSETROLE.find(4, ARSID, false, true, true);
	m_recCORE_ACCRSETROLE.deleteSelectedRows();

	m_recCORE_ACCESSRIGHTS.clearSelection();
	m_recCORE_ACCESSRIGHTS.find(3, ARSID);
	m_recCORE_ACCESSRIGHTS.find(4, RoleID, false, true, true);
	QList<int> ARRowList = m_recCORE_ACCESSRIGHTS.getSelectedRows();

	QList<int> ARRowIDList;
	foreach(int row, ARRowList)
		ARRowIDList << m_recCORE_ACCESSRIGHTS.getDataRef(row, 0).toInt();

	DbRecordSet tmp; 
	tmp.copyDefinition(m_recCORE_ACCESSRIGHTS);
	tmp.merge(m_recCORE_ACCESSRIGHTS, true);
	m_recCORE_ACCESSRIGHTS.deleteSelectedRows();

	
	//Empty access rights recordset.
	foreach(int rowID, ARRowIDList)
	{
		if(m_lstChangedAccessRight.indexOf(rowID) >= 0)
			m_lstChangedAccessRight.removeAt(m_lstChangedAccessRight.indexOf(rowID));
	}

	//Finally remove from tree.
	ParentItem->takeChild(ParentItem->indexOfChild(Item));
	delete(Item);

	ChangeSaveState();
}

void FUI_RoleDefinition::DeleteRole(QTreeWidgetItem *Item)
{
	Q_ASSERT(Item);
	int RoleID	= Item->data(0, Qt::UserRole).toInt();

	//Lock role.
	if (!LockResource(RoleID))
		return;

	//First remove children.
	QList<QTreeWidgetItem*> childrenList = Item->takeChildren();
	QListIterator<QTreeWidgetItem*> iter(childrenList);
	while (iter.hasNext())
		DeleteARS(iter.next(), Item);
	
	//Remove from all kind of lists.
	//If not inserted than delete it.
	if(m_lstInsertedRoles.indexOf(RoleID) >= 0)
		m_lstInsertedRoles.removeAt(m_lstInsertedRoles.indexOf(RoleID));
	else
		m_lstDeletedRoles << RoleID;
	
	if(m_lstRenamedRoles.contains(RoleID))
		m_lstRenamedRoles.removeAt(m_lstRenamedRoles.indexOf(RoleID));
	
	//Remove from recordsets.
	if (m_recCORE_ROLE.find(0, RoleID, true) >= 0)
		m_recCORE_ROLE.deleteRow(m_recCORE_ROLE.find(0, RoleID, true));
	m_recCORE_ACCRSETROLE.find(3, RoleID, false, false, true);
	m_recCORE_ACCRSETROLE.deleteSelectedRows();
	
	//And from tree.
	ui.Role_treeWidget->takeTopLevelItem(ui.Role_treeWidget->indexOfTopLevelItem(Item));
	delete(Item);
}

void FUI_RoleDefinition::CreateAccessRightsRecordSetForInsertedARS(int RoleID, int ARSID)
{
	//Get ARS Code.
	int row = m_recCORE_ACCRSET.find(0, ARSID, true);
	int ARSCode = m_recCORE_ACCRSET.getDataRef(row, 6).toInt();

	//Get access right set.
	AccessRightSet *ARSet = m_AROrganizer.GetAccessRightSet(ARSCode);

	//Get access rights list.
	QVector<int> ARAddList;
	ARSet->GetARSAddList(ARAddList);

	//Get fake row ID.
	QVariant varARRowID;
	m_recCORE_ACCESSRIGHTS.GetMinimum(0, varARRowID);
	int ARRowID = varARRowID.toInt();
	//It must be negative number if not 0 (it will be decreased lated).
	if (ARRowID >= 0)
		ARRowID = 0;

	//Loop through list.
	QVectorIterator<int> iter(ARAddList);
	while (iter.hasNext())
	{
		int ARID = iter.next();
		DbRecordSet ARRecordSet;
		AccessRight *ARight = m_AROrganizer.GetAccessRight(ARID);

		//Decrease temporary row id.
		ARRowID--;

		ARight->GetARRecordSet(ARRecordSet);
		ARRecordSet.setData(0, 0, ARRowID);	//Access right tmp row ID.
		ARRecordSet.setData(0, 3, ARSID);	//Access right set ID (also temporary one).
		ARRecordSet.setData(0, 4, RoleID);	//Role ID.

		if(m_recCORE_ACCESSRIGHTS.getRowCount() == 0)
			m_recCORE_ACCESSRIGHTS.copyDefinition(ARRecordSet);
		m_recCORE_ACCESSRIGHTS.merge(ARRecordSet);
	}
}

void FUI_RoleDefinition::on_RenameRole()
{
	//Only one item can be selected.
	QTreeWidgetItem	*Item;
	Item = ui.Role_treeWidget->selectedItems().first();
	int		RoleID	 = Item->data(0,Qt::UserRole).toInt();
	QString RoleName = Item->data(0, Qt::DisplayRole).toString();
	QString RoleDesc = Item->data(2, Qt::UserRole).toString();
	int		RoleType = Item->data(1, Qt::UserRole).toInt();

	RenameRole(RoleID, RoleName, RoleDesc, RoleType);

	//Just to refresh description field.
	ui.Role_treeWidget->clearSelection();
	Item->setSelected(true);
}

void FUI_RoleDefinition::on_SavePushButton_clicked()
{
	//First save new roles.
	foreach(int RoleID, m_lstInsertedRoles)
	{
		QTreeWidgetItem *RoleItem = m_RoleItemsHash.value(RoleID);
		int RID;
		QString RoleName = RoleItem->data(0, Qt::DisplayRole).toString();
		QString RoleDesc = RoleItem->data(2, Qt::UserRole).toString();
		int		RoleType = RoleItem->data(1, Qt::UserRole).toInt();
		_SERVER_CALL(AccessRights->InsertNewRole(m_Status, RID, RoleName, RoleDesc, RoleType))
		_CHK_ERR(m_Status);

		//Map tmp ID to real one (for ARS insertion).
		m_hshTmpToInsertedRoleID.insert(RoleID, RID);
	}

	//Second rename any role that need to be renamed.
	foreach(int nRoleID, m_lstRenamedRoles)
	{
		QTreeWidgetItem *RoleItem = m_RoleItemsHash.value(nRoleID);
		QString RoleName = RoleItem->data(0, Qt::DisplayRole).toString();
		QString RoleDesc = RoleItem->data(2, Qt::UserRole).toString();
		_SERVER_CALL(AccessRights->RenameRole(m_Status, nRoleID, RoleName, RoleDesc))
		_CHK_ERR(m_Status);
	}

	//Delete roles.
	foreach(int roleID, m_lstDeletedRoles)
	{
		_SERVER_CALL(AccessRights->DeleteRole(m_Status, roleID))
		_CHK_ERR(m_Status);
	}

	//Insert ARS to role.
	int InsARSToRoleCount = m_recInsertedARSToRole.getRowCount();
	for(int i = 0; i < InsARSToRoleCount; ++i)
	{
		int nroleID = m_recInsertedARSToRole.getDataRef(i, 1).toInt();
		//Check is this role newly added.
		if (nroleID < 0)
			nroleID = m_hshTmpToInsertedRoleID.value(nroleID);
		int nARSID = m_recInsertedARSToRole.getDataRef(i, 2).toInt();
		_SERVER_CALL(AccessRights->InsertNewAccRSetToRole(m_Status, nroleID, nARSID))
		_CHK_ERR(m_Status);
	}

	//Delete ARS from role.
	foreach(int rID, m_hshDeletedARSFromRole.uniqueKeys())
	{
		foreach(int arsID, m_hshDeletedARSFromRole.values(rID))
		{
			_SERVER_CALL(AccessRights->DeleteAccRSetFromRole(m_Status, rID, arsID))
			_CHK_ERR(m_Status);
		}
	}

	SaveChangedAccessRights();
	UnLockResources();

	g_pClientManager->ReloadAccessRights();

	ReloadData();
	m_bEditMode = false;
	ChangeSaveState();
}

void FUI_RoleDefinition::RoleSelectionChanged()
{
	//Get selected items list.
	QList<QTreeWidgetItem*> selectedItemsList = ui.Role_treeWidget->selectedItems();
	
	//If nothing selected (when we delete all) disable delete button and return.
	if (selectedItemsList.isEmpty())
	{
		ui.ARS_ProperyBrowser->Initialize(&DbRecordSet());
		ui.DeletePushButton->setEnabled(false);
		return;
	}
	
	//Only one item can be selected.
	QTreeWidgetItem	*Item;
	Item = selectedItemsList.first();
	//Enable push button.
	if (m_bEditMode)
		ui.DeletePushButton->setEnabled(true);

	DbRecordSet tmpRec;
	
	//Check is it ARS.
	if (Item->parent())
	{
		int RoleID	= Item->parent()->data(0, Qt::UserRole).toInt();
		QString RoleType= Item->parent()->data(1, Qt::UserRole).toString();
		QString RoleDescr = Item->parent()->data(2, Qt::UserRole).toString();
		int ItemID	= Item->data(0, Qt::UserRole).toInt();
		QString ARSDescr = Item->data(2, Qt::UserRole).toString();
		int ARSID;

		//Set check boxes.
		SetRoleType(RoleType);
		//Set description fields.
		ui.RoleDescr_textBrowser->clear();
		ui.RoleDescr_textBrowser->insertPlainText(RoleDescr);
		ui.ARSDescr_textBrowser->clear();
		ui.ARSDescr_textBrowser->insertPlainText(ARSDescr);

		//Find access right set.
		int row		= m_recCORE_ACCRSETROLE.find(0, ItemID, true);
		if (row > -1)
			ARSID	= m_recCORE_ACCRSETROLE.getDataRef(row, 4).toInt();
		else
		{
			row		= m_recInsertedARSToRole.find(0, ItemID, true);
			ARSID	= m_recInsertedARSToRole.getDataRef(row, 2).toInt();
		}
		
		m_recCORE_ACCESSRIGHTS.clearSelection();
		m_recCORE_ACCESSRIGHTS.find(4, RoleID);
		m_recCORE_ACCESSRIGHTS.find(3, ARSID, false, true, true);
		tmpRec.copyDefinition(m_recCORE_ACCESSRIGHTS);
		tmpRec.merge(m_recCORE_ACCESSRIGHTS, true);
		//Initialize browser.
		ui.ARS_ProperyBrowser->Initialize(&tmpRec);
	}
	//If role.
	else
	{
		QList<QTreeWidgetItem*> ChildItems;
		//Get children items.
		int childCount = Item->childCount();
		for (int i = 0; i < childCount; ++i)
			ChildItems << Item->child(i);

		int RoleID	= Item->data(0, Qt::UserRole).toInt();
		QString RoleDescr = Item->data(2, Qt::UserRole).toString();
		QString RoleType= Item->data(1, Qt::UserRole).toString();
		tmpRec.copyDefinition(m_recCORE_ACCESSRIGHTS);
		m_recCORE_ACCESSRIGHTS.find(4, RoleID);
		tmpRec.merge(m_recCORE_ACCESSRIGHTS, true);

		//Set check boxes.
		SetRoleType(RoleType);
		//Set description fields.
		ui.RoleDescr_textBrowser->clear();
		ui.RoleDescr_textBrowser->insertPlainText(RoleDescr);
		ui.ARSDescr_textBrowser->clear();

		//Initialize browser.
		ui.ARS_ProperyBrowser->Initialize(&tmpRec);
	}
}

void FUI_RoleDefinition::ARSSelectionChanged()
{
	if (!m_bEditMode)
		return;

	//Get selected items list.
	QList<QTreeWidgetItem*> selectedItemsList = ui.ARS_treeWidget->selectedItems();

	//If nothing selected (when we delete all) disable assign button and return.
	if (selectedItemsList.isEmpty())
		ui.AssignToRolesPushButton->setDisabled(true);
	else
		ui.AssignToRolesPushButton->setDisabled(false);
}

void FUI_RoleDefinition::ARSValueChanged(int RowID, QVariant PropertyValue, QString  PropertyStrValue)
{
	//Set data to recordset.
	int row = m_recCORE_ACCESSRIGHTS.find(0, RowID, true);

	int RoleID = m_recCORE_ACCESSRIGHTS.getDataRef(row, 4).toInt();
	//Lock role.
	if (!LockResource(RoleID))
	{
		//Select item once more so that it updates the changed value in property browser.
		QTreeWidgetItem *item = ui.Role_treeWidget->selectedItems().first();
		item->setSelected(true);
		return;
	}
	
	m_recCORE_ACCESSRIGHTS.setData(row, 7, PropertyValue.toInt());
	m_recCORE_ACCESSRIGHTS.setData(row, 8, PropertyStrValue);

	//Insert in changed values list.
	if (!m_lstChangedAccessRight.contains(RowID))
		m_lstChangedAccessRight << RowID;
	
	ChangeSaveState();
}

void FUI_RoleDefinition::on_InsertRolePushButton_clicked()
{
	QString RoleName;
	QString RoleDescr;
	int		RoleType;

	RoleInputDialog *RoleInput = new RoleInputDialog(&RoleName, &RoleDescr, &RoleType, true);
	if (RoleInput->exec())
		InsertRole(RoleName, RoleDescr, RoleType);

	delete(RoleInput);
	return;
}

void FUI_RoleDefinition::on_CancelPushButton_clicked()
{
	if (QMessageBox::warning(this, "Cancel pressed!", "Do you want to discard changes!", QMessageBox::Ok, QMessageBox::Cancel) == QMessageBox::Cancel)
		return;

	UnLockResources();
	ReloadData();
	SetMode(FuiBase::MODE_READ);
	m_bEditMode = false;
	//g_objFuiManager.CloseCurrentFuiTab();
}

void FUI_RoleDefinition::on_DeletePushButton_clicked()
{
	//Only one item can be selected.
	QTreeWidgetItem	*Item, *ParentItem;
	Item = ui.Role_treeWidget->selectedItems().first();
	ParentItem = Item->parent();

	//Check is it ARS.
	if (ParentItem)
		DeleteARS(Item, ParentItem);
	//If role.
	else
		DeleteRole(Item);

	ChangeSaveState();
}

void FUI_RoleDefinition::ChangeSaveState()
{
	if (!m_lstInsertedRoles.isEmpty()		|| !m_lstChangedAccessRight.isEmpty()		||
		!m_lstDeletedRoles.isEmpty()		|| !m_lstRenamedRoles.isEmpty()				||
		!m_hshDeletedARSFromRole.isEmpty()	|| m_recInsertedARSToRole.getRowCount() > 0 ||
		m_bEditMode)
	{
		ui.SavePushButton->setEnabled(true);
		GUI_Helper::SetWidgetFontWeight(ui.SavePushButton, QFont::Bold);
		GUI_Helper::SetStyledButtonColorBkg(ui.SavePushButton, GUI_Helper::BTN_COLOR_GREEN, "white");
		ui.CancelPushButton->setEnabled(true);
		ui.editPushButton->setEnabled(false);
		ui.editPushButton->setStyleSheet("");
		ui.InsertRolePushButton->setEnabled(true);
		ui.ARS_ProperyBrowser->setEnabled(true);
		ui.AssignToRolesPushButton->setEnabled(true);
		ui.ARS_treeWidget->setEnabled(true);
		ui.Role_treeWidget->setContextMenuPolicy(Qt::DefaultContextMenu);
		//Change fuibase mode.
		SetMode(FuiBase::MODE_EDIT);
	}
	else
	{
		ui.editPushButton->setEnabled(true);
		ui.SavePushButton->setEnabled(false);
		ui.SavePushButton->setStyleSheet("");
		ui.CancelPushButton->setEnabled(false);
		ui.InsertRolePushButton->setEnabled(false);
		ui.ARS_ProperyBrowser->setEnabled(false);
		ui.DeletePushButton->setEnabled(false);
		ui.AssignToRolesPushButton->setEnabled(false);
		ui.ARS_treeWidget->setEnabled(false);
		ui.Role_treeWidget->setContextMenuPolicy(Qt::PreventContextMenu);
		//Change fuibase mode.
		SetMode(FuiBase::MODE_READ);
	}
}

void FUI_RoleDefinition::on_ARSDropped(QTreeWidgetItem *parent, int index)
{
	QList<int> SelectedARSRowID	   = ui.ARS_treeWidget->GetSelectedItems();

	int RoleID = parent->data(0, Qt::UserRole).toInt();

	//Insert dropped AR sets.
	foreach(int ARSRowID, SelectedARSRowID)
		InsertARSToRole(RoleID, ARSRowID);
}

bool FUI_RoleDefinition::CheckIsARSinRole(int RoleID, int ARSID)
{
	//Check is it already inserted before.
	m_recCORE_ACCRSETROLE.find(3, RoleID);
	m_recCORE_ACCRSETROLE.find(4, ARSID, false, true, true);
	if (m_recCORE_ACCRSETROLE.getSelectedCount() > 0)
		return true;

	//Check is it inserted recently.
	m_recInsertedARSToRole.find(1, RoleID);
	m_recInsertedARSToRole.find(2, ARSID, false, true, true);
	if (m_recInsertedARSToRole.getSelectedCount() > 0)
		return true;

	return false;
}

void FUI_RoleDefinition::SetRoleType(QString &RoleType)
{
	if (RoleType == "1")
	{
		ui.BusVisibleCheckBox->setCheckState(Qt::Checked);
		ui.SysRoleCheckBox->setCheckState(Qt::Checked);
	}
	else if (RoleType == "0")
	{
		ui.BusVisibleCheckBox->setCheckState(Qt::Unchecked);
		ui.SysRoleCheckBox->setCheckState(Qt::Checked);
	}
	else
	{
		ui.BusVisibleCheckBox->setCheckState(Qt::Unchecked);
		ui.SysRoleCheckBox->setCheckState(Qt::Unchecked);
	}
}

void FUI_RoleDefinition::SaveChangedAccessRights()
{
	m_recCORE_ACCESSRIGHTS.clearSelection();

	if (m_lstChangedAccessRight.isEmpty())
		return;

	foreach(int AccRID, m_lstChangedAccessRight)
	{
		//Get row.
		int row = m_recCORE_ACCESSRIGHTS.find(0, AccRID, true);
		//Check is access right ID new one or is just a modification of existing one.
		//If true then find it and put new inserted role ID.
		if (AccRID < 0)
		{
			Q_ASSERT(row >= 0); //If not something is wrong.
			int RoleID = m_recCORE_ACCESSRIGHTS.getDataRef(row, 4).toInt();
			//Replace this fake role ID with real one.
			if (RoleID < 0)
				RoleID = m_hshTmpToInsertedRoleID.value(RoleID);
			m_recCORE_ACCESSRIGHTS.setData(row, 4, RoleID);
		}
		
		//Select changed row.
		m_recCORE_ACCESSRIGHTS.selectRow(row);
	}

	DbRecordSet ChangedARRecordSet;
	ChangedARRecordSet.copyDefinition(m_recCORE_ACCESSRIGHTS);
	ChangedARRecordSet.merge(m_recCORE_ACCESSRIGHTS, true);

	_SERVER_CALL(AccessRights->ChangeAccessRights(m_Status, ChangedARRecordSet))
	_CHK_ERR(m_Status);
}

void FUI_RoleDefinition::ReloadData()
{
	ui.Role_treeWidget->clear();
	ui.ARS_treeWidget->clear();
	m_recCORE_ROLE.clear();
	m_recCORE_ACCRSETROLE.clear();
	m_recCORE_ACCRSET.clear();
	m_recCORE_ACCESSRIGHTS.clear();
	m_lstInsertedRoles.clear();
	m_lstDeletedRoles.clear();
	m_lstRenamedRoles.clear();
	m_hshTmpToInsertedRoleID.clear();
	m_recInsertedARSToRole.clear();
	m_hshDeletedARSFromRole.clear();
	m_lstChangedAccessRight.clear();

	InitializeButtons();

	//Get roles and access rights sets.
	InitializeARSWidget();
	InitializeTreeWidget();

	//Clear text boxea.
	ui.RoleDescr_textBrowser->clear();
	ui.ARSDescr_textBrowser->clear();
}

bool FUI_RoleDefinition::LockResource(int RoleID)
{
	//Check is this newly inserted role.
	if (RoleID < 0)
		return true;

	//Check is this role already locked.
	if (m_hshLockingResourceHash.contains(RoleID))
		return true;

	//If nothing from above lock.
	QString LockResourceID;
	//Create locking recordset.
	int row = m_recCORE_ROLE.find(0, RoleID, true);
	DbRecordSet lockRecSet;
	lockRecSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ROLE));
	lockRecSet.addRow();
	lockRecSet.setData(0, 0, RoleID);

	DbRecordSet lstStatusRows;
	_SERVER_CALL(ClientSimpleORM->Lock(m_Status, CORE_ROLE, lockRecSet, LockResourceID, lstStatusRows))
	if (!m_Status.IsOK())
	{
		QMessageBox::critical(this, tr("Locking error!"), m_Status.getErrorText());
		return false;
	}
		
	m_hshLockingResourceHash.insert(RoleID, LockResourceID);
	return true;
}

void FUI_RoleDefinition::UnLockResources()
{
	foreach(int roleID, m_hshLockingResourceHash.keys())
	{
		QString lockRes = m_hshLockingResourceHash.value(roleID);
		bool bUnlocked;
		_SERVER_CALL(ClientSimpleORM->UnLock(m_Status, CORE_ROLE, bUnlocked, lockRes))
		_CHK_ERR(m_Status);
		Q_ASSERT(bUnlocked);
	}

	m_hshLockingResourceHash.clear();
}

void FUI_RoleDefinition::on_editPushButton_clicked()
{
	m_bEditMode = true;
	ChangeSaveState();
}

void FUI_RoleDefinition::on_AssignToRolesPushButton_clicked()
{
	//Create selected roles recordset.
	DbRecordSet SelectedRoles;

	//Call dialog to get roles.
	ARSToRoleAssignDialog *RoleAssign = new ARSToRoleAssignDialog(&m_recCORE_ROLE);
	if (RoleAssign->exec())
		SelectedRoles = RoleAssign->GetSelectedRoles();
	
	//Get list of items and put them in a list.
	QList<QTreeWidgetItem*> selectedRoleItems;
	int selectedCount = SelectedRoles.getRowCount();
	for (int i = 0; i < selectedCount; ++i)
	{
		int roleID = SelectedRoles.getDataRef(i, 0).toInt();
		selectedRoleItems << m_RoleItemsHash.value(roleID);
	}

	foreach(QTreeWidgetItem *item, selectedRoleItems)
		on_ARSDropped(item, 0);

	//Delete dialog.
	delete(RoleAssign);
	return;
}

void FUI_RoleDefinition::on_TreeFocusIn()
{
	if (!m_bEditMode)
		return;

	ui.DeletePushButton->setEnabled(true);
}

void FUI_RoleDefinition::on_TreeFocusOut()
{
	//ui.DeletePushButton->setEnabled(false);
}


//mamicu mu jebem, nemoj da spremas stringove u bazu, vidi sta sad moram da radim
void FUI_RoleDefinition::TranslateAR(DbRecordSet &lstData)
{
	//check if right list
	if (lstData.getColumnIdx("CAR_CODE")<0 || lstData.getColumnIdx("CAR_NAME")<0)
		return;

	int nSize=lstData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nCarCode=lstData.getDataRef(i,"CAR_CODE").toInt();

		AccessRight *pArObject=m_AROrganizer.m_hshAccessRights.value(nCarCode,NULL);
		if (pArObject)
		{
			DbRecordSet recArSet;
			pArObject->GetARRecordSet(recArSet);
			if (recArSet.getRowCount()>0)
			{
				lstData.setData(i,"CAR_NAME",recArSet.getDataRef(0,5));
			}
		}
	}

}

void FUI_RoleDefinition::TranslateARSet(DbRecordSet &lstData)
{
	//check if right list
	if (lstData.getColumnIdx("CAST_NAME")<0 || lstData.getColumnIdx("CAST_ACCRSET_DESC")<0 || lstData.getColumnIdx("CAST_CODE")<0)
		return;

	int nSize=lstData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nCarCode=lstData.getDataRef(i,"CAST_CODE").toInt();

		AccessRightSet *pArObject=m_AROrganizer.m_hshAccessRightSets.value(nCarCode,NULL);
		if (pArObject)
		{
			DbRecordSet recArSet;
			pArObject->GetARSRecordSet(recArSet);
			if (recArSet.getRowCount()>0)
			{
				lstData.setData(i,"CAST_NAME",recArSet.getDataRef(0,3));
				lstData.setData(i,"CAST_ACCRSET_DESC",recArSet.getDataRef(0,4));
			}
		}
	}
}