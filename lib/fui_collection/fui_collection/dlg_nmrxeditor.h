#ifndef DLG_NMRXEDITOR_H
#define DLG_NMRXEDITOR_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_nmrxeditor.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include <QVBoxLayout>



/*!
	\class  Dlg_NMRXEditor
	\brief  Dialog for editing, viewing and saving NM
	\ingroup Fui_collection


	- init with NM pair, set master, load if edit, go edit now


*/


class Dlg_NMRXEditor : public QDialog
{
    Q_OBJECT

public:
    Dlg_NMRXEditor(QWidget *parent = 0);
    ~Dlg_NMRXEditor();

	//Modes:
	enum Mode 
	{
		MODE_READ=0,	
		MODE_EDIT=1,			
		MODE_INSERT=2,			
		MODE_EMPTY=3	
	};

	void SetRow(int nMasterTableID,int nMasterRecordID,QString strCalculatedMasterName,DbRecordSet &rowData,int nMode,QString strMasterName,QString strSlaveName,int nX_TableID=-1,bool bSkipWriteMode=false, bool bDisableRoleSwitch=false,bool bMoveMasterSAPNEDown=false,bool bReadOnlyMode=false);
	void GetRow(DbRecordSet &rowData){rowData=m_lstData;}

private slots:
	void on_btnReverse_clicked();
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	//void on_btnNew_clicked(){};
	void on_btnEdit_clicked();
	void on_btnDelete_clicked();
	void OnContactChanged(int);
	void on_btnSelectSMS_clicked();

private:

	void keyPressEvent(QKeyEvent* event);
	void closeEvent(QCloseEvent *event);
	void UnlockIfLocked();
	void RefreshDisplay();
	void SetMode(int nMode);
	void ReverseSAPNEs();

	QString m_strLockedRes;
	int m_nMode;
	DbRecordSet m_lstData;

	QString			m_strHtmlTag;
	QString			m_strEndHtmlTag;
	GuiFieldManager *m_GuiFieldManagerMain;
	bool			m_bMasterIs_M,m_bMasterIs_M_Original;
	QVBoxLayout		*m_Layout;
	int				m_nMasterTableID, m_nSlaveTableID;
	QString			m_strMasterName;
	QString			m_strSlaveName;
	bool			m_bSkipWriteMode;
	int				m_nX_TableID;
	bool			m_bReadOnlyMode;

	Ui::Dlg_NMRXEditorClass ui;
};

#endif // DLG_NMRXEDITOR_H
