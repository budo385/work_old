#include "table_contactaddresstypes.h"
#include <QHeaderView>
#include "bus_client/bus_client/clientcontactmanager.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbsqltableview.h"

Table_ContactAddressTypes::Table_ContactAddressTypes(QWidget *parent)
: UniversalTableWidgetEx(parent),m_pRowAddress(NULL)
{

}

//to enable load from global cache: out from constructor
void Table_ContactAddressTypes::Initialize()
{
	//init datasource: BUS_CM_TYPES table + check column
	m_AddressTypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_AddressTypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_ADDRESS);
	m_AddressTypeHandler.ReloadData();
	m_lstTypes=*m_AddressTypeHandler.GetDataSource(); //copy as we must add one more column;
	m_lstTypes.addColumn(QVariant::Int,"IS_CHECKED");

	m_nTypeCheckColIdx=m_lstTypes.getColumnIdx("IS_CHECKED");

	m_lstDefSort<<SortData(m_nTypeCheckColIdx,1); //desc
	m_lstDefSort<<SortData(m_lstTypes.getColumnIdx("BCMT_TYPE_NAME")); //asc

	DbRecordSet columns;
	AddColumnToSetup(columns,"IS_CHECKED","",20,true, "",COL_TYPE_CHECKBOX);
	AddColumnToSetup(columns,"BCMT_TYPE_NAME","",130,false, "",COL_TYPE_TEXT);
	UniversalTableWidgetEx::Initialize(&m_lstTypes,&columns,true,true,18);
	//hide headers:
	horizontalHeader()->hide();
	verticalHeader()->hide();
	setShowGrid(false);
}


//LST_TYPES -> show, if nCurrentRow>0 then points to the row inside list pRowAddress
void Table_ContactAddressTypes::SetAddressRow(DbRecordSet *pRowAddress,int nCurrentRow)
{
	m_nCurrentAddrRow=nCurrentRow;
	m_pRowAddress=pRowAddress;
	if (m_pRowAddress==NULL)
	{
		m_lstTypes.setColValue(m_nTypeCheckColIdx,0); //reset all
		m_lstTypes.sortMulti(m_lstDefSort);
		RefreshDisplay();
		return;
	}
	DbRecordSet lstTypes=m_pRowAddress->getDataRef(m_nCurrentAddrRow,"LST_TYPES").value<DbRecordSet>();
	if (lstTypes.getColumnCount()==0 || lstTypes.getColumnIdx("BCMT_TYPE_NAME")==-1) //define if not
		lstTypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT));
	
	if (lstTypes.getRowCount()==0)//insert default type
	{
		int nTypeID=ClientContactManager::GetDefaultType(ContactTypeManager::TYPE_ADDRESS).toInt();
		if(nTypeID>0)
		{
			lstTypes.addRow();
			lstTypes.setData(0,"BCMAT_ADDRESS_ID",pRowAddress->getDataRef(0,"BCMA_ID")); //address id
			lstTypes.setData(0,"BCMAT_TYPE_ID",nTypeID); //type id
			int nX=m_lstTypes.find("BCMT_ID",nTypeID,true);
			if (nX>=0)
				lstTypes.setData(0,"BCMT_TYPE_NAME",m_lstTypes.getDataRef(nX,"BCMT_TYPE_NAME").toString()); //type name
		}
	}
	//store back (if defaults added):
	m_pRowAddress->setData(m_nCurrentAddrRow,"LST_TYPES",lstTypes);

	//refresh display:
	//LST_TYPES is in TVIEW_BUS_CM_ADDRESS_TYPES_SELECT, datasource TVIEW_BUS_CM_TYPES
	m_lstTypes.setColValue(m_nTypeCheckColIdx,0); //reset all
	int nSize=lstTypes.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nTypeID=lstTypes.getDataRef(i,"BCMAT_TYPE_ID").toInt();
		int nRow=m_lstTypes.find("BCMT_ID",nTypeID,true);
		if(nRow>=0)
			m_lstTypes.setData(nRow,m_nTypeCheckColIdx,1);
	}
	m_lstTypes.sortMulti(m_lstDefSort);
	RefreshDisplay();
}

//on change, save types at once
void Table_ContactAddressTypes::OnCellEdited(int nRow, int nCol)
{
	UniversalTableWidgetEx::OnCellEdited(nRow,nCol);

	if (m_pRowAddress==NULL)
		return;

	if (nCol==0) //check button toggled:
	{
		int nTypeID=m_lstTypes.getDataRef(nRow,"BCMT_ID").toInt();
		DbRecordSet lstTypes=m_pRowAddress->getDataRef(m_nCurrentAddrRow,"LST_TYPES").value<DbRecordSet>();
		int nRowAddrType=lstTypes.find("BCMAT_TYPE_ID",nTypeID,true);
		if (m_lstTypes.getDataRef(nRow,"IS_CHECKED").toInt()>0)
		{
			if(nRowAddrType<0)//if not in LST_TYPES, add new row
			{
				lstTypes.addRow();
				nRowAddrType=lstTypes.getRowCount()-1;
			}
			lstTypes.setData(nRowAddrType,"BCMAT_ADDRESS_ID",m_pRowAddress->getDataRef(m_nCurrentAddrRow,"BCMA_ID")); //address id
			lstTypes.setData(nRowAddrType,"BCMAT_TYPE_ID",nTypeID); //type id
			lstTypes.setData(nRowAddrType,"BCMT_TYPE_NAME",m_lstTypes.getDataRef(nRow,"BCMT_TYPE_NAME"));
		}
		else
		{
			if(nRowAddrType>=0) //if uncheck delete it:
				lstTypes.deleteRow(nRowAddrType);
		}

		//lstTypes.Dump();

		//store back (if defaults added):
		m_pRowAddress->setData(m_nCurrentAddrRow,"LST_TYPES",lstTypes);

		emit SignalDataChanged(nRow,nCol);
	}

}


//When Types are changed in the cache, use this to refresh content
void Table_ContactAddressTypes::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if(nMsgCode==SelectorSignals::SELECTOR_DATA_CHANGED)
	{
		m_lstTypes=*m_AddressTypeHandler.GetDataSource(); //copy as we must add one more column;
		m_lstTypes.addColumn(QVariant::Bool,"IS_CHECKED");
		SetAddressRow(m_pRowAddress);
	}
}

void Table_ContactAddressTypes::CheckDataBeforeWrite(Status &Ret_pStatus)
{
	if(m_lstTypes.getRowCount()==m_lstTypes.countColValue(m_nTypeCheckColIdx,false) && m_lstTypes.getRowCount()!=0)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Address type must be set!"));	
		return;
	}
}