#ifndef ROLEDEFINTIONTREEWIDGET_H
#define ROLEDEFINTIONTREEWIDGET_H

#include "universaltreewidget.h"

class RoleDefintionTreeWidget : public UniversalTreeWidget
{
public:
	RoleDefintionTreeWidget(QWidget *parent = NULL) : UniversalTreeWidget(parent) {};
private:
	QTreeWidgetItem* SetItemData(DbRecordSet &DataRecordSet, QTreeWidgetItem *Item = NULL);
};

#endif // ROLEDEFINTIONTREEWIDGET_H
