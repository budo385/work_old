#ifndef TaskDialog_H
#define TaskDialog_H

#include <QtWidgets/QDialog>
#include "ui_taskdialog.h"
#include "common/common/observer_ptrn.h"
#include "common/common/status.h"

/*!
	\class  TaskDialog
	\brief  Schedule tasks
	\ingroup FUICollection

	Use:
	- INSERT: fill m_recTask, with task=active, status, etc.. to enable it
	- EDIT: if Task ID != NULL, chkbox is disabled: chkbox is checked if task active
	- MODE: if read mode, all controls can be enabled/disabled
*/

class TaskDialog : public QDialog, public ObsrPtrn_Observer
{
    Q_OBJECT

public:
    TaskDialog(QWidget *parent = 0);
    ~TaskDialog();

	DbRecordSet m_recTask;
	DbRecordSet *m_pRecMain;

	void SetDefaultTaskType(int nTaskSystemType);
	void GetDataFromGUI();
	void SetGUIFromData();
	void ClearTaskData();
	void SetEnabled(bool bEnable=true);
	bool isChecked();
	void Hide();
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());	
	void CheckTaskRecordBeforeSave(Status &err);
		
signals:
	void ScheduleTaskToggle();

private:
	void SetInfoVisible(bool bVisible = true); //automatically visible if task created, else on check button

    Ui::TaskDialogClass ui;

	int m_nTaskSystemType;
	bool m_bEditMode;

private slots:
	void OnTaskTypeChanged();
	void OnTaskCompleted(bool bChecked);
	void on_btnOK_clicked();
	void on_btnCancel_clicked();
};

#endif // TaskDialog_H
