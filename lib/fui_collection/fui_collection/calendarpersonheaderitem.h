#ifndef CALENDARPERSONHEADERITEM_H
#define CALENDARPERSONHEADERITEM_H

#include <QGraphicsProxyWidget>
#include "label.h"

#include "calendargraphicsview.h"

class PersonHeaderDisplayWidget : public QFrame
{
	Q_OBJECT

public:
	PersonHeaderDisplayWidget(QIcon icon,  QString strText, int nHeight, CalendarGraphicsView *pCalendarGraphicsView, QWidget *parent = 0);
	~PersonHeaderDisplayWidget();

protected:

private:
	CalendarGraphicsView *m_pCalendarGraphicsView;

};

class CalendarPersonHeaderItem : public QGraphicsProxyWidget
{
	Q_OBJECT

public:
	CalendarPersonHeaderItem(int nColumn, QIcon icon,  QString strText, int nHeight, 
		CalendarGraphicsView *pCalendarGraphicsView, QGraphicsProxyWidget *pCalendarTableView, CalendarTableModel *pCalendarTableModel, 
		QGraphicsItem * parent = 0, Qt::WindowFlags wFlags = 0);
	~CalendarPersonHeaderItem();

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

public slots:
	void on_UpdateCalendar();

private:
	CalendarGraphicsView *m_pCalendarGraphicsView;
	QGraphicsProxyWidget *m_pCalendarTableView;
	CalendarTableModel	 *m_pCalendarTableModel;
	Label				 *m_pLabel;
	QTableWidget		 *m_pTable;
	int					 m_nColumn;
	int					 m_nHeight;
};

#endif // CALENDARPERSONHEADERITEM_H
