#ifndef USERWIDGET_H
#define USERWIDGET_H

#include <QWidget>
#include <QHash>
#include <QMenu>

#include "generatedfiles/ui_userwidget.h"

#include "artreewidget.h"
#include "multiassigndialog.h"
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "fuibase.h"

/*!
\class  UserWidget.
\ingroup FUICore/AccessRights
\brief  User manipulation. 

User and his role adding, deleting and renaming widget.
*/
class UserWidget : public FuiBase
{
    Q_OBJECT

public:
    UserWidget(QWidget *parent = 0);
    ~UserWidget();

	QString GetFUIName(){return tr("Login Account Access Rights");};

private:
	Ui::UserWidgetClass ui;
	
	void InitContextMenu();

	void Initialize();
	void InitializeButtons();
	void InitializeRecordSets();
	void InitializeUserTree();
	void InitializeRoleList();
	void InsertRoleToUser(int nUserID, int nRoleID);
	void DeleteRoleFromUser(QTreeWidgetItem *Item, QTreeWidgetItem *ParentItem);
	bool CheckIsRoleInUser(int nUserID, int nRoleID);
	void ChangeSaveState();
	bool LockResource(int UserID);
	void UnLockResources();
	void ReloadData();

	Status							m_Status;
	DbRecordSet						m_recCORE_ROLE;					//< Roles recordset.
	DbRecordSet						m_recCORE_USER;					//< User recordset.
	DbRecordSet						m_recCORE_USERROLE;				//< User to role recordset.
	QHash<int, QTreeWidgetItem*>	m_hshUserIDToItem;				//< User ID to tree item hash.
	//Changes.
	DbRecordSet						m_recInsertedRoleToUser;		//< Inserted roles to user recordset.
	QMultiHash<int, int>			m_hshDeletedRoleFromUser;		//< Deleted roles from user hash (role id, user id).
	//Lock resource list.
	QHash<int,QString>				m_hshLockingResourceHash;		//< User id to lock resource hash.
	//Context actions.
	QList<QAction*>					m_lstRoleActions;

	private slots:
		void			on_AssignPushButton_clicked();
		void			on_SavePushButton_clicked();
		void			on_CancelPushButton_clicked();
		void			on_DeletePushButton_clicked();
		void			RoleSelectionChanged();
		void			UserSelectionChanged();
		void			on_RoleDropped(QTreeWidgetItem *Parent, int index);
};

#endif // USERWIDGET_H
