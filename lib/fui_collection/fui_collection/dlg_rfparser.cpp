#include "dlg_rfparser.h"
#include "db_core/db_core/dbsqltableview.h"
#include <QFileDialog>
#include <QHeaderView>
#include <QDomDocument>
extern QString g_strLastDir_FileOpen;


Dlg_RFParser::Dlg_RFParser(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	m_bApp=true;
	m_bTemp=true;
	m_bInter=true;
	m_lstApplications.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS));
	m_lstApplications.addColumn(QVariant::String,"TEMPLATE_NAME");
	m_lstApplications.addColumn(QVariant::String,"TEMPLATE_PATH");
	m_lstApplications.addColumn(QVariant::Int,"TEMPLATE_TYPE");
	m_lstApplications.addColumn(QVariant::String,"APP_PATH");
	
	m_lstTemplates.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
	m_lstTemplates.addColumn(QVariant::String,"FILE_PATH");

	m_lstInterfaces.destroy();
	m_lstInterfaces.addColumn(QVariant::String,"INTERFACE_CODE");
	m_lstInterfaces.addColumn(QVariant::String,"INTERFACE_NAME");
	m_lstInterfaces.addColumn(QVariant::String,"DATA_PATH");
	m_lstInterfaces.addColumn(QVariant::String,"APP_PATH");
	

	connect(ui.treeWidget,SIGNAL(itemChanged(QTreeWidgetItem *, int)),this,SLOT(OnItemChanged(QTreeWidgetItem *, int)));
}

Dlg_RFParser::~Dlg_RFParser()
{

}

//if file path empty: open file dialog is open
//err-> error, bool= false if user cancel's
bool Dlg_RFParser::Initialize(Status &err,QString strFilePath, bool bForceFileSelectionDlg)
{
	err.setError(0);
	//-------------------------------------------------------------
	//				LOAD XML
	//-------------------------------------------------------------
	QDomDocument xmldoc_RF; 
	if(strFilePath.isEmpty() || bForceFileSelectionDlg)
	{
		QString strStartDir=QCoreApplication::applicationDirPath(); 
		QString strFilter="SC_Registration.xml";
		if (!QFile::exists(strStartDir+"/"+strFilter) || bForceFileSelectionDlg)
		{
			//open file dialog:
			if (g_strLastDir_FileOpen.isEmpty())
				g_strLastDir_FileOpen=strStartDir;
			else
				strStartDir=g_strLastDir_FileOpen;
			strFilePath = QFileDialog::getOpenFileName(
				NULL,
				tr("Load registration xml file:"),
				strStartDir,
				strFilter);
			if (!strFilePath.isEmpty())
			{
				QFileInfo fileInfo(strFilePath);
				g_strLastDir_FileOpen=fileInfo.absolutePath();
			}
			if(strFilePath.isEmpty())
				return false;
		}
		else
		{
			strFilePath=strStartDir+"/"+strFilter;
		}
	}


	QFile RFFile(strFilePath);
	if (!RFFile.exists())
	{
		err.setError(1,tr("File ")+ strFilePath+ tr(" does not exists!"));
		return true;
	}
	if(!RFFile.open(QIODevice::ReadOnly))
	{
		err.setError(1,tr("File: ")+strFilePath+tr(" can not be read!"));
		return true;
	}
	if(!xmldoc_RF.setContent(&RFFile))
	{
		err.setError(1,tr("File: ")+strFilePath+tr(" can not be parsed: invalid xml format!"));
		RFFile.close();
		return true;
	}
	RFFile.close();

	m_strRFFilePath=strFilePath;


	//parse by name: set tree items:
	//-------------------------------------------------------------
	//				PARSE XML
	//-------------------------------------------------------------


	QDomNodeList ListOfElements	= xmldoc_RF.elementsByTagName("Portable_Applications");
	if (ListOfElements.count()==0)
	{
		err.setError(1,tr("File: ")+strFilePath+tr(" can not be parsed: invalid xml format!"));
		return true;
	}

	QDomNodeList allNodese=ListOfElements.at(0).childNodes();
	if (allNodese.count()!=3)
	{		
		err.setError(1,tr("File: ")+strFilePath+tr(" can not be parsed: invalid xml format!"));
		return true;
	}

	m_lstApplications.clear();
	QDomNode NodeApp = allNodese.at(0);

	if (!NodeApp.isNull())
	{
		QDomNodeList ListOfElements	= NodeApp.childNodes(); //get all app entries
		int nSize=ListOfElements.count();
		for(int k=0;k<nSize;k++)
		{
			QDomNode Node = ListOfElements.at(k);

			QDomNodeList childListOfElements = Node.childNodes(); //get all app entries
			int nSize2=childListOfElements.count();
			m_lstApplications.addRow();
			int nRow=m_lstApplications.getRowCount()-1;
			for(int i=0;i<nSize2;i++)
			{
					QDomNode  childNode = childListOfElements.at(i);
					QDomElement Element =  childNode.toElement();
					QString name = Element.tagName();

					if (name=="Name")
						m_lstApplications.setData(nRow, "BDMA_NAME",Element.text()); 
					if (name=="Description")
						m_lstApplications.setData(nRow, "BDMA_DESCRIPTION",Element.text());
					if (name=="Application_Path")
						m_lstApplications.setData(nRow, "APP_PATH",Element.text());
					if (name=="Code")
						m_lstApplications.setData(nRow, "BDMA_CODE",Element.text());
					if (name=="File_Extensions")
						m_lstApplications.setData(nRow, "BDMA_EXTENSIONS",Element.text());
					if (name=="Parameters")
						m_lstApplications.setData(nRow, "BDMA_OPEN_PARAMETER",Element.text());
					if (name=="Template_Name")
						m_lstApplications.setData(nRow, "TEMPLATE_NAME",Element.text());
					if (name=="Template_Path")
						m_lstApplications.setData(nRow, "TEMPLATE_PATH",Element.text());
					if (name=="Template_Type")
						m_lstApplications.setData(nRow, "TEMPLATE_TYPE",QVariant(Element.text()).toInt());
					if (name=="Type")
						m_lstApplications.setData(nRow, "BDMA_APPLICATION_TYPE",QVariant(Element.text()).toInt());
					if (name=="Read_Only")
						m_lstApplications.setData(nRow, "BDMA_READ_ONLY",QVariant(Element.text()).toInt());
					if (name=="Use_Percent_Coding")
						m_lstApplications.setData(nRow, "BDMA_ENCODE_PARAMETERS",QVariant(Element.text()).toInt());
					if (name=="Do_Not_Create_Doc")
						m_lstApplications.setData(nRow, "BDMA_NO_FILE_DOCUMENT",QVariant(Element.text()).toInt());
					if (name=="Set_Copies_To_Read_Only")
						m_lstApplications.setData(nRow, "BDMA_SET_READ_ONLY_FLAG",QVariant(Element.text()).toInt());
			}
		}
	}


	m_lstTemplates.clear();
	QDomNode NodeTemplate = allNodese.at(1);

	if (!NodeTemplate.isNull())
	{
		QDomNodeList ListOfElements	= NodeTemplate.childNodes(); //get all app entries
		int nSize=ListOfElements.count();
		for(int k=0;k<nSize;k++)
		{
			QDomNode Node = ListOfElements.at(k);

			QDomNodeList childListOfElements = Node.childNodes(); //get all app entries
			int nSize2=childListOfElements.count();
			m_lstTemplates.addRow();
			int nRow=m_lstTemplates.getRowCount()-1;
			m_lstTemplates.setData(nRow,"BDMD_TEMPLATE_FLAG",1);
			for(int i=0;i<nSize2;i++)
			{
				QDomNode  childNode = childListOfElements.at(i);
				QDomElement Element =  childNode.toElement();
				QString name = Element.tagName();

				if (name=="Template_Name")
					m_lstTemplates.setData(nRow, "BDMD_NAME",Element.text()); 
				if (name=="Filename")
					m_lstTemplates.setData(nRow, "BDMD_DOC_PATH",Element.text());
				//if (name=="Is_Private")
				//	m_lstTemplates.setData(nRow, "CENT_IS_PRIVATE",QVariant(Element.text()).toInt());
				if (name=="Description")
					m_lstTemplates.setData(nRow, "BDMD_DESCRIPTION",Element.text());
				if (name=="File_Path")
					m_lstTemplates.setData(nRow, "FILE_PATH",Element.text());
				if (name=="Storage")
					m_lstTemplates.setData(nRow, "BDMD_DOC_TYPE",QVariant(Element.text()).toInt());
				
			}
		}
	}


	m_lstInterfaces.clear();
	QDomNode NodeInterface = allNodese.at(2);
	if (!NodeInterface.isNull())
	{
		QDomNodeList ListOfElements	= NodeInterface.childNodes(); //get all app entries
		int nSize=ListOfElements.count();
		for(int k=0;k<nSize;k++)
		{
			QDomNode Node = ListOfElements.at(k);
			qDebug()<<Node.nodeName();
			QDomNodeList childListOfElements = Node.childNodes(); //get all app entries
			int nSize2=childListOfElements.count();
			m_lstInterfaces.addRow();
			int nRow=m_lstInterfaces.getRowCount()-1;
			m_lstInterfaces.setData(nRow, "INTERFACE_NAME",Node.nodeName()); 
			for(int i=0;i<nSize2;i++)
			{
				QDomNode  childNode = childListOfElements.at(i);
				QDomElement Element =  childNode.toElement();
				QString name = Element.tagName();
				if (name=="Interface_Code")
					m_lstInterfaces.setData(nRow, "INTERFACE_CODE",Element.text()); 
				if (name=="Data_Path")
					m_lstInterfaces.setData(nRow, "DATA_PATH",Element.text());
				if (name=="Application_Path")
					m_lstInterfaces.setData(nRow, "APP_PATH",Element.text());
			}
		}
	}


	m_parentItem_Apps = new QTreeWidgetItem();
	m_parentItem_Templates = new QTreeWidgetItem();
	m_parentItem_Interfaces = new QTreeWidgetItem();
	m_parentItem_Apps->setData(0,Qt::DisplayRole,tr("Applications"));
	m_parentItem_Templates->setData(0,Qt::DisplayRole,tr("Templates"));
	m_parentItem_Interfaces->setData(0,Qt::DisplayRole,tr("Interfaces"));
	m_parentItem_Apps->setFlags( Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled); 
	m_parentItem_Templates->setFlags( Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled); 
	m_parentItem_Interfaces->setFlags( Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled); 
	m_parentItem_Apps->setCheckState(0,Qt::Checked);
	m_parentItem_Templates->setCheckState(0,Qt::Checked);
	m_parentItem_Interfaces->setCheckState(0,Qt::Checked);

	

	//-------------------------------------------------------------
	//				BUILD TREE
	//-------------------------------------------------------------

	ui.treeWidget->setColumnCount(2);
	ui.treeWidget->setColumnWidth(0,200);
	ui.treeWidget->setColumnWidth(1,300);
	ui.treeWidget->header()->hide();
	ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	//Application childs:
	QList<QTreeWidgetItem*> itemsList;
	int nSize = m_lstApplications.getRowCount();
	for (int i = 0; i < nSize; ++i)
	{
		//First row is application option set.
		QTreeWidgetItem *Item = new QTreeWidgetItem();
		Item->setData(0, Qt::DisplayRole, m_lstApplications.getDataRef(i,"BDMA_NAME").toString());
		Item->setData(1, Qt::DisplayRole, m_lstApplications.getDataRef(i,"BDMA_DESCRIPTION").toString());
		Item->setToolTip(0, m_lstApplications.getDataRef(i,"APP_PATH").toString());
		Item->setFlags( Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled); 
		Item->setCheckState(0,Qt::Checked);
		itemsList << Item;
	}
	if (itemsList.count()>0)
	{
		m_parentItem_Apps->addChildren(itemsList);
		ui.treeWidget->addTopLevelItem(m_parentItem_Apps);
		ui.treeWidget->expandItem(m_parentItem_Apps);
	}

	//template childs:
	itemsList.clear();
	nSize = m_lstTemplates.getRowCount();
	for (int i = 0; i < nSize; ++i)
	{
		//First row is application option set.
		QTreeWidgetItem *Item = new QTreeWidgetItem();
		Item->setData(0, Qt::DisplayRole, m_lstTemplates.getDataRef(i,"BDMD_NAME").toString());
		Item->setData(1, Qt::DisplayRole, m_lstTemplates.getDataRef(i,"BDMD_DESCRIPTION").toString());
		Item->setToolTip(0, m_lstTemplates.getDataRef(i,"FILE_PATH").toString());
		Item->setFlags( Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled); 
		Item->setCheckState(0,Qt::Checked);
		itemsList << Item;
	}
	if (itemsList.count()>0)
	{
		m_parentItem_Templates->addChildren(itemsList);
		ui.treeWidget->addTopLevelItem(m_parentItem_Templates);
		ui.treeWidget->expandItem(m_parentItem_Templates);
	}

		

	//interface childs:
	itemsList.clear();
	nSize = m_lstInterfaces.getRowCount();
	for (int i = 0; i < nSize; ++i)
	{
		//First row is application option set.
		QTreeWidgetItem *Item = new QTreeWidgetItem();
		Item->setData(0, Qt::DisplayRole, m_lstInterfaces.getDataRef(i,"INTERFACE_CODE").toString());
		if (!m_lstInterfaces.getDataRef(i,"DATA_PATH").toString().isEmpty())
			Item->setData(1, Qt::DisplayRole, m_lstInterfaces.getDataRef(i,"DATA_PATH").toString());		
		else
			Item->setData(1, Qt::DisplayRole, m_lstInterfaces.getDataRef(i,"APP_PATH").toString());
		Item->setFlags( Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled); 
		Item->setCheckState(0,Qt::Checked);
		itemsList << Item;
	}
	if (itemsList.count()>0)
	{
		m_parentItem_Interfaces->addChildren(itemsList);
		ui.treeWidget->addTopLevelItem(m_parentItem_Interfaces);
		ui.treeWidget->expandItem(m_parentItem_Interfaces);
	}
	return true;

}

void Dlg_RFParser::GetResult(DbRecordSet &lstApps,DbRecordSet &lstTemplates,DbRecordSet &lstInterfaces,QString &strRFFilePath)
{
	strRFFilePath=m_strRFFilePath;

	int nSize=m_parentItem_Apps->childCount();
	m_lstApplications.clearSelection();
	for(int i=0;i<nSize;i++)
	{
		if (m_parentItem_Apps->child(i)->checkState(0)==Qt::Checked)
		{
			m_lstApplications.selectRow(i);
		}
	}
	m_lstApplications.deleteUnSelectedRows();
	lstApps=m_lstApplications;

	nSize=m_parentItem_Templates->childCount();
	m_lstTemplates.clearSelection();
	for(int i=0;i<nSize;i++)
	{
		if (m_parentItem_Templates->child(i)->checkState(0)==Qt::Checked)
		{
			m_lstTemplates.selectRow(i);
		}
	}
	m_lstTemplates.deleteUnSelectedRows();
	lstTemplates=m_lstTemplates;

	
	nSize=m_parentItem_Interfaces->childCount();
	m_lstInterfaces.clearSelection();
	for(int i=0;i<nSize;i++)
	{
		if (m_parentItem_Interfaces->child(i)->checkState(0)==Qt::Checked)
		{
			m_lstInterfaces.selectRow(i);
		}
	}
	m_lstInterfaces.deleteUnSelectedRows();
	lstInterfaces=m_lstInterfaces;
}

void Dlg_RFParser::on_btnCancel_clicked()
{
	done(0);
}

void Dlg_RFParser::on_btnOK_clicked()
{
	done(1);
}


void Dlg_RFParser::OnItemChanged(QTreeWidgetItem *item, int col)
{
	if (item == m_parentItem_Apps)
	{
		if (m_bApp!=(item->checkState(0)==Qt::Checked))
		{
			m_bApp=!m_bApp;
			int nSize=item->childCount();
			for(int i=0;i<nSize;i++)
			{
				if (m_bApp)
					item->child(i)->setCheckState(0,Qt::Checked);
				else
					item->child(i)->setCheckState(0,Qt::Unchecked);
			}
		}
	}

	if (item == m_parentItem_Templates)
	{
		if (m_bTemp!=(item->checkState(0)==Qt::Checked))
		{
			m_bTemp=!m_bTemp;
			int nSize=item->childCount();
			for(int i=0;i<nSize;i++)
			{
				if (m_bTemp)
					item->child(i)->setCheckState(0,Qt::Checked);
				else
					item->child(i)->setCheckState(0,Qt::Unchecked);
			}
		}
	}

	if (item == m_parentItem_Interfaces)
	{
		if (m_bInter!=(item->checkState(0)==Qt::Checked))
		{
			m_bInter=!m_bInter;
			int nSize=item->childCount();
			for(int i=0;i<nSize;i++)
			{
				if (m_bInter)
					item->child(i)->setCheckState(0,Qt::Checked);
				else
					item->child(i)->setCheckState(0,Qt::Unchecked);
			}
		}
	}

}
