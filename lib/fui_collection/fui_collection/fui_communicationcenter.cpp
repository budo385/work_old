#include "fui_communicationcenter.h"	
#include "bus_core/bus_core/globalconstants.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "common/common/entity_id_collection.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"

//GLOBALS:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:

FUI_CommunicationCenter::FUI_CommunicationCenter(QWidget *parent)
: FuiBase(parent)//,m_StyleMenuManager(NULL)
{
	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);
	InitFui();

	m_strTitle=tr("Desktop");

	m_p_CeMenu=ui.frameCommToolBar;

	//helpers used to recalculate custom fields
	//m_contactController.Initialize(ENTITY_BUS_CONTACT);
	//m_projectController.Initialize(ENTITY_BUS_PROJECT);

	//ui.lblPicture1->setPixmap(QPixmap(":DmPic2.png"));


	//connect CE menu/E grid:
	connect(ui.frameCommToolBar,SIGNAL(NeedNewData(int)),this,SLOT(OnCEMenu_NeedNewData(int)));
	connect(ui.CommWorkBench_widget,SIGNAL(NeedNewData(int)),this,SLOT(OnCEMenu_NeedNewData(int)));

	//--------------------------
	//Set Grid.
	//--------------------------
	ui.CommWorkBench_widget->Initialize(CommGridViewHelper::DESKTOP_GRID, this);

	connect(ui.CommWorkBench_widget, SIGNAL(FilterChanged(QString)), this, SLOT(on_filterChanged(QString)));
	//ui.CommWorkBench_widget->SetData(g_pClientManager->GetPersonID());

	//drop handler from CE menu re-rout to CE grid
	//ui.CommWorkBench_widget->SetDropHandlerWidget(ui.frameCommToolBar->m_menuDoc);


}

FUI_CommunicationCenter::~FUI_CommunicationCenter()
{
	//if (m_StyleMenuManager) delete m_StyleMenuManager;
}

//when clicked if not loaded, load:
void FUI_CommunicationCenter::on_groupBoxCommName_clicked()
{
	/*
	//based on checkbook:
	if (ui.groupBoxCommName->isChecked())
	{
		ui.groupBoxCommName->setMinimumHeight(123);
		//ui.stackedWidget_2->setVisible(true);	
	}
	else
	{
		ui.groupBoxCommName->setMinimumHeight(25);
		//ui.stackedWidget_2->setVisible(false);
	}
	*/
}



//get FUI name:
QString FUI_CommunicationCenter::GetFUIName()
{
	return m_strTitle;
}


void FUI_CommunicationCenter::on_PButton_clicked()
{
	int personID = g_pClientManager->GetPersonID();
	Status status;
	DbRecordSet recEmail, recVoiceCall, recDocument;
/*
	_SERVER_CALL(BusCommunication->GetPersonCommData(status, recEmail, recVoiceCall, recDocument, personID))
	_CHK_ERR(status);
	ui.CommWorkBench_widget->Initialize(recEmail, recVoiceCall, recDocument);
*/
}

/*
void FUI_CommunicationCenter::on_ComEntityGrid_executed(int nEntityID, int nEntitySysType, int nDocID)
{
	switch(nEntitySysType)
	{
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		{
			//voice calls
			int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CALL, true, false);
			FUI_VoiceCall* pFUI = dynamic_cast<FUI_VoiceCall *>(g_objFuiManager.GetFUIWidget(nFUI));
			if(NULL == pFUI) return;	//someone closed the FUI for this call

			pFUI->SetViewMode(true);
		}
		break;

	case GlobalConstants::CE_TYPE_DOCUMENT:
		{
			g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_READ, nDocID);
		}
	    break;
	case GlobalConstants::CE_TYPE_EMAIL:
		 break;
	default:
		Q_ASSERT(false);
	    break;
	}
}
*/



void FUI_CommunicationCenter::OnCEMenu_NeedNewData(int nSubMenuType)
{
	//which contact is actual:
	DbRecordSet lstContacts;
	lstContacts.addColumn(QVariant::Int,"BCNT_ID");
	DbRecordSet record;
	int nContactID;
	if(ui.CommWorkBench_widget->GetContactSAPNE()->GetCurrentEntityRecord(nContactID,record))
	{
		lstContacts.addRow();
		lstContacts.setData(0,0,nContactID);
	}
	ui.frameCommToolBar->SetDefaults(&lstContacts);
	//if empty: defaults will be loaded
	ui.frameCommToolBar->SetDefaultMail("");
	ui.frameCommToolBar->SetDefaultPhone("");
	ui.frameCommToolBar->SetDefaultInternet("");
		

}



void FUI_CommunicationCenter::SetCurrentTab(int nTabPane)
{
	//ui.tabWidget->setCurrentIndex(nTabPane);
}





void FUI_CommunicationCenter::DesktopStartupInitialization(DbRecordSet &recCurrentView)
{
	ui.CommWorkBench_widget->DesktopStartupInitialization(g_pClientManager->GetPersonID(),recCurrentView.getDataRef(0,"BUSCV_ID").toInt());
	OnViewChanged(recCurrentView);
}

void FUI_CommunicationCenter::OnViewChanged(DbRecordSet &recCurrentView)
{


	//m_strTitle=ClientContactManager::GetFirstLettersFromString(g_pClientManager->GetPersonName());
	m_strTitle = g_pClientManager->GetPersonInitials();
	m_strTitle += ": "+recCurrentView.getDataRef(0,"BUSCV_NAME").toString();
	
	//init fui name change:
	RefreshFuiName();

}

void FUI_CommunicationCenter::on_filterChanged(QString strFilterName)
{
	int nCurr1=g_objFuiManager.GetCurrentCommGridGID();
	int nCurr2=g_objFuiManager.GetFuiID(this);
	m_strTitle = /*g_pClientManager->GetPersonInitials() + ": " + */ strFilterName;
	RefreshFuiName();
}
