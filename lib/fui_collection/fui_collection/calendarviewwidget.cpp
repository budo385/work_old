#include "calendarviewwidget.h"
#include "gui_core/gui_core/thememanager.h"
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/globalconstants.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

CalendarViewWidget::CalendarViewWidget(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);
	ui.calendarWidget->setFirstDayOfWeek(Qt::Monday);

	ui.labelTitle->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	ui.labelTitle->setAlignment(Qt::AlignLeft);
	ui.label_1->setStyleSheet("QLabel {color:white}");
	ui.label_2->setStyleSheet("QLabel {color:white}");
	ui.label_3->setStyleSheet("QLabel {color:white}");
	ui.label_4->setStyleSheet("QLabel {color:white}");
	ui.label_5->setStyleSheet("QLabel {color:white}");
	ui.label_6->setStyleSheet("QLabel {color:white}");

	ui.frameTypes->GetTreeWidget()->SetItemsUserCheckable(true);
	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_TYPE_SELECT));
	ui.frameTypes->GetTreeWidget()->SetIconColumnDataSource(lstData.getColumnIdx("CET_ICON_BINARY"));
	ui.frameTypes->Initialize(ENTITY_CE_TYPES);
	ui.frameTypes->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);
	ui.frameTypes->ReloadData();

	connect(ui.calendarWidget,SIGNAL(selectionChanged()),this,SLOT(OnCalendarSelectionChanged()));
	connect(ui.slider,SIGNAL(valueChanged(int)),this,SLOT(OnSliderValueChanged(int)));
	connect(ui.frameTypes->GetTreeWidget(),SIGNAL(ItemCheckStateChanged(QList<QTreeWidgetItem*>)),this,SLOT(OnTypeItemCheckStateChanged(QList<QTreeWidgetItem*>)));
	connect(ui.ckbShowPreliminary,SIGNAL(stateChanged(int)),this,SLOT(OnInformationChanged(int)));
	connect(ui.ckbShowInfo,SIGNAL(stateChanged(int)),this,SLOT(OnPreliminaryChanged(int)));
	connect(ui.ckbFilter,SIGNAL(stateChanged(int)),this,SLOT(OnFilterByTypeChanged(int)));
	connect(ui.ckbFilterNoType,SIGNAL(stateChanged(int)),this,SLOT(OnFilterByNoType(int)));

	setStyleSheet("QFrame#rangeFrame"+ThemeManager::GetSidebarChapter_Bkg());
}

CalendarViewWidget::~CalendarViewWidget()
{
	
}

void CalendarViewWidget::SetDateRange(QDate from, QDate to)
{
	ui.calendarWidget->blockSignals(true);
	ui.calendarWidget->SetSelectionRange(from,to);
	ui.calendarWidget->blockSignals(false);
}

void CalendarViewWidget::setSlider(int nSliderMinute)
{
	if (nSliderMinute == 1)
		ui.slider->setValue(0);
	else if (nSliderMinute == 2)
		ui.slider->setValue(1);
	else if (nSliderMinute == 4)
		ui.slider->setValue(2);
	else if (nSliderMinute == 6)
		ui.slider->setValue(3);
	else if (nSliderMinute == 10)
		ui.slider->setValue(4);
	else if (nSliderMinute == 12)
		ui.slider->setValue(5);
}

void CalendarViewWidget::GetData(int &nRangeMinute, DbRecordSet &recCheckedItems, bool &bFilterByType, bool &bShowPreliminary, bool &bShowInformation,bool &bFilterByNoType)
{
	nRangeMinute=GetSliderMinute(ui.slider->value());
	ui.frameTypes->GetTreeWidget()->GetCheckedItems(recCheckedItems,true);
	bFilterByType=(bool)(ui.ckbFilter->checkState()==Qt::Checked);
	bShowPreliminary=(bool)(ui.ckbShowPreliminary->checkState()==Qt::Checked);
	bShowInformation=(bool)(ui.ckbShowInfo->checkState()==Qt::Checked);
	bFilterByNoType=(bool)(ui.ckbFilterNoType->checkState()==Qt::Checked);
}

void CalendarViewWidget::SetData(int nRangeMinute, DbRecordSet recCheckedItems, bool bFilterByType, bool bShowPreliminary, bool bShowInformation, bool bFilterByNoType, QDate from, QDate to)
{
	this->blockSignals(true);
	ui.ckbFilter->blockSignals(true);
	ui.ckbFilterNoType->blockSignals(true);

	ui.frameTypes->blockSignals(true);

	setSlider(nRangeMinute);
	ui.ckbFilter->setChecked(bFilterByType);
	ui.ckbShowPreliminary->setChecked(bShowPreliminary);
	ui.ckbShowInfo->setChecked(bShowInformation);
	ui.ckbFilterNoType->setChecked(bFilterByNoType);
	
	if (!recCheckedItems.getRowCount())
		ui.frameTypes->GetTreeWidget()->SetItemsChecked(recCheckedItems, true /*force to check all*/);
	else
		ui.frameTypes->GetTreeWidget()->SetItemsChecked(recCheckedItems);

	SetDateRange(from, to);

	ui.ckbFilter->blockSignals(false);
	ui.ckbFilterNoType->blockSignals(false);
	ui.frameTypes->blockSignals(false);
	this->blockSignals(false);
}

void CalendarViewWidget::OnCalendarSelectionChanged()
{
	emit DateChanged(ui.calendarWidget->selectedDate());
}
void CalendarViewWidget::OnSliderValueChanged(int value)
{
	emit RangeChanged(GetSliderMinute(ui.slider->value()));
}
void CalendarViewWidget::OnTypeItemCheckStateChanged(QList<QTreeWidgetItem*> lstData)
{
	if (!ui.ckbFilter->isChecked())
		return;

	QList<int> lstIDs;
	int nSize=lstData.size();
	for(int i=0;i<nSize;i++)
	{
		int nRowID = lstData.at(i)->data(0, Qt::UserRole).toInt();
		lstIDs.append(nRowID);
	}

	emit FilterTypeChanged(lstIDs);
}

void CalendarViewWidget::OnFilterByTypeChanged(int value)
{
	QList<int> lstIDs;
	if (value==Qt::Unchecked)
	{
		emit FilterTypeChanged(lstIDs);
		ui.frameTypes->setEnabled(false);
	}
	else
	{
		ui.ckbFilterNoType->blockSignals(true);
		ui.ckbFilterNoType->setChecked(false);
		ui.ckbFilterNoType->blockSignals(false);

		ui.frameTypes->setEnabled(true);
		DbRecordSet recCheckedItems;
		ui.frameTypes->GetTreeWidget()->GetCheckedItems(recCheckedItems,true);
		if (recCheckedItems.getRowCount()>0)
		{
			int nSize=recCheckedItems.getRowCount();
			for(int i=0;i<nSize;i++)
			{
				lstIDs.append(recCheckedItems.getDataRef(i,"CHECKED_ITEM_ID").toInt());
			}

			emit FilterTypeChanged(lstIDs);
		}
	}
}
void CalendarViewWidget::OnFilterByNoType(int value)
{
	if (value==Qt::Unchecked)
	{
		emit FilterByNoType(false);
	}
	else
	{
		emit FilterByNoType(true);
		ui.ckbFilter->blockSignals(true);
		ui.ckbFilter->setChecked(false);
		ui.frameTypes->setEnabled(false);
		ui.ckbFilter->blockSignals(false);
	}
}

void CalendarViewWidget::OnInformationChanged(int value)
{
	emit ShowInformationEvents(value==Qt::Checked);
}
void CalendarViewWidget::OnPreliminaryChanged(int value)
{
	emit ShowPreliminaryEvents(value==Qt::Checked);
}





int CalendarViewWidget::GetSliderMinute(int nSliderValue)
{
	
	switch(nSliderValue)
	{
	case 0:
		return 1;	//Hour.
		break;
	case 1:
		return 2;	//Half hour.
		break;
	case 2:
		return 4;	//Quater hour.
	    break;
	case 3:
		return 6;	//Ten minutes.
	    break;
	case 4:
		return 10;	//Six minutes.
		break;
	default:
		return 12;	//Five minutes.
	    break;
	}

	return 2;
}

int CalendarViewWidget::SetSliderMinute(int nMinute)
{
	switch(nMinute)
	{
	case 60:
		return 0;
		break;
	case 30:
		return 1;
		break;
	case 15:
		return 2;
	    break;
	case 10:
		return 3;
	    break;
	case 6:
		return 4;
		break;
	default:
		return 5;
	    break;
	}

	return 0;
}
