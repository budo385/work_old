#include "qc_internetframe.h"
#include "common/common/entity_id_collection.h"
#include <QtWidgets/QMessageBox>
#include "qcw_largewidget.h"
#include "qcw_smallwidget.h"
#include "gui_core/gui_core/thememanager.h"

QC_InternetFrame::QC_InternetFrame(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	ui.addSelected_toolButton->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_2->setIcon(QIcon(":CopyText16.png"));

	connect(ui.selector_widget, SIGNAL(RowChanged(int)), this, SLOT(on_RowChanged(int)));
}

QC_InternetFrame::~QC_InternetFrame()
{

}

void QC_InternetFrame::Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType /*= 0 0-Large,1-small widget*/)
{
	m_nParentType = nParentType;
	m_pParentWidget = ParentWidget;
	m_recFullContactData = recFullContactData;

	ui.selector_widget->Initialize(ENTITY_BUS_CM_INTERNET, m_recFullContactData);
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

void QC_InternetFrame::AddInternetAddress(DbRecordSet recInternet)
{
	if (ui.internet_lineEdit->text().isEmpty())
		ui.selector_widget->addEntity(recInternet);
	else
		ui.selector_widget->addEntityNoOverride(recInternet);
}

void QC_InternetFrame::SetupGUIwidgets(DbRecordSet recData)
{
	ui.internet_lineEdit->setText(recData.getDataRef(0, "BCMI_ADDRESS").toString());
	ui.description_lineEdit->setText(recData.getDataRef(0, "BCMI_DESCRIPTION").toString());
}

void QC_InternetFrame::SetDataToRecordSet(QString strRecordSetFieldName, QString strValue)
{
	m_recLocalEntityRecordSet.setData(m_nCurrentRow, strRecordSetFieldName, strValue);
	m_recFullContactData->setData(0, "LST_INTERNET", m_recLocalEntityRecordSet);
}

void QC_InternetFrame::SetIntDataToRecordSet(QString strRecordSetFieldName, int nValue)
{
	m_recLocalEntityRecordSet.setData(m_nCurrentRow, strRecordSetFieldName, nValue);
	m_recFullContactData->setData(0, "LST_INTERNET", m_recLocalEntityRecordSet);
}

void QC_InternetFrame::on_RowChanged(int nRow)
{
	DbRecordSet tmpRowLocalEntityRecordSet;
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_INTERNET").value<DbRecordSet>();

	tmpRowLocalEntityRecordSet.copyDefinition(m_recLocalEntityRecordSet);
	if (nRow < 0)
	{
		tmpRowLocalEntityRecordSet.addRow();
	}
	else
	{
		m_recLocalEntityRecordSet.clearSelection();
		_DUMP(m_recLocalEntityRecordSet);
		m_recLocalEntityRecordSet.selectRow(nRow);
		tmpRowLocalEntityRecordSet.merge(m_recLocalEntityRecordSet, true);
	}

	m_nCurrentRow = nRow;

	SetupGUIwidgets(tmpRowLocalEntityRecordSet);
}

void QC_InternetFrame::on_internet_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMI_ADDRESS", text);
}

void QC_InternetFrame::on_description_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMI_DESCRIPTION", text);
}

void QC_InternetFrame::on_addSelected_toolButton_clicked()
{
	if (m_nParentType == 0)
		ui.internet_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.internet_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_InternetFrame::on_addSelected_toolButton_2_clicked()
{
	if (m_nParentType == 0)
		ui.description_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.description_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_InternetFrame::on_isDefault_checkBox_stateChanged(int state)
{
	if (state == Qt::Checked)
	{
		int nRowCount = m_recLocalEntityRecordSet.getRowCount();
		for (int i=0; i < nRowCount; i++)
			m_recLocalEntityRecordSet.setData(i, "BCMI_IS_DEFAULT", 0);

		SetIntDataToRecordSet("BCMI_IS_DEFAULT", 1);
	}
	else
		SetIntDataToRecordSet("BCMI_IS_DEFAULT", 0);
}
