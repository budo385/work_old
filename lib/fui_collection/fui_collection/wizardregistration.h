#ifndef WIZARDREGISTRATION_H
#define WIZARDREGISTRATION_H

#include <QHash>

//Base class.
#include "gui_core/gui_core/wizardbase.h"

class WizardRegistration
{
public:
	//--------------------------------------------------------------------------
	//DEFINE WIZARD PAGES IN ENTITY_ID_COLLECTION.H!!!!!!!!! -> BECAUSE OF CACHE.
	//--------------------------------------------------------------------------

	//Here you define wizards.
	enum Wizards
	{
		REPORT_WIZARD,
		DEPARTMENT_SELECTION_WIZARD,
		CONTACT_SELECTION_WIZARD,
		STARTUP_WIZARD,
		PROJECT_EXPORT_SELECTION_WIZARD,
		USER_EXPORT_SELECTION_WIZARD,
		PROJECT_REPORT_SELECTION_WIZARD,
		CONTACT_COMMUNICATION_REPORT_SELECTION_WIZARD,
		CONTACT_EXPORT_SELECTION_WIZARD,
		CONTACT_SELECTION_WIZARD_SIMPLE,
		REPORT_DESTINATION_WIZARD,
		REPORT_CONTACT_RELATIONSHIPS
	};
	
	WizardRegistration(int nWizardID);
    ~WizardRegistration();

	QHash<int, WizardPagesCollection*> m_hshWizardPageHash;		//< Wizard ID to page collection.
};

#endif // WIZARDREGISTRATION_H
