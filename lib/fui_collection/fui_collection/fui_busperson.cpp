#include "fui_busperson.h"
#include "bus_core/bus_core/businessdatatransformer.h"
#include "gui_core/gui_core/dateselectordlg.h"
#include "common/common/entity_id_collection.h"
#include "common/common/authenticator.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "gui_core/gui_core/gui_helper.h"
#include "bus_core/bus_core/useraccessright.h"
#include "qcw_base.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "optionsandsettingswidget.h"

//GLOBALS:
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;				

bool Call_OnLoadFullPicture(int nPicID, DbRecordSet &rowPic); //in fui contacts

FUI_BusPerson::FUI_BusPerson(QWidget *parent)
    : FuiBase(parent)
{


	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);
	ui.frameSelection->Initialize(ENTITY_BUS_PERSON,true,true); //skip loading data, coz this is under filter =ACO
	InitFui(ENTITY_BUS_PERSON,BUS_PERSON,dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);
	m_CalcName.Initialize(ENTITY_BUS_PERSON,m_lstData);
	//ui.widgetReservations->Initialize(BUS_PERSON);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_BUS_PERSON);
	m_GuiFieldManagerMain->RegisterField("BPER_CODE",ui.txtCode);
	m_GuiFieldManagerMain->RegisterField("BPER_INITIALS",ui.txtInitials);
	m_GuiFieldManagerMain->RegisterField("BPER_FIRST_NAME",ui.txtFirstName);
	m_GuiFieldManagerMain->RegisterField("BPER_LAST_NAME",ui.txtLastName);
	m_GuiFieldManagerMain->RegisterField("BPER_DATE_ENTERED",ui.dateEntered);
	m_GuiFieldManagerMain->RegisterField("BPER_DATE_LEFT",ui.dateLeft);
	m_GuiFieldManagerMain->RegisterField("BPER_ACTIVE_FLAG",ui.chkActive);
	m_GuiFieldManagerMain->RegisterField("BPER_INSURANCE_ID",ui.txtInsurance);
	m_GuiFieldManagerMain->RegisterField("BPER_BIRTH_DATE",ui.dateBirth);	
	m_GuiFieldManagerMain->RegisterField("BPER_OCCUPATION",ui.dblOccupation);
	
	QList<QRadioButton*> lstRadioButons;
	lstRadioButons << ui.btnMale << ui.btnFemale;
	QList<QVariant> lstValues;
	lstValues << 0 << 1;
	m_GuiFieldManagerMain->RegisterField("BPER_SEX",new GuiField_RadioButton("BPER_SEX",lstRadioButons,lstValues,&m_lstData,&DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON)));
	
	//group contact:
	m_GuiFieldManagerMain->RegisterField("BPER_DIRECT_PHONE",ui.txtDirectPhone);
	ui.frameContact->EnableInsertButton();
	ui.frameContact->registerObserver(this);

	m_GuiFieldManagerMain->RegisterField("BPER_DESCRIPTION",ui.frameEditor);
	m_GuiFieldManagerUser =new GuiFieldManager(&m_lstCoreUser,DbSqlTableView::TVIEW_CORE_USER);
	m_GuiFieldManagerUser->RegisterField("CUSR_USERNAME",ui.txtUserName);
	m_nPasswordIdx=m_lstCoreUser.getColumnIdx("CUSR_PASSWORD");
	//init custom picture control
	ui.picture->Initialize(PictureWidget::MODE_PREVIEW_WITH_BUTTONS,&m_lstData,"BPER_PICTURE","BPIC_PICTURE","BPER_BIG_PICTURE_ID","BPER_ID");
	ui.picture->SetLoadFullPictureCallBack(Call_OnLoadFullPicture);
	ui.picture->setFocusPolicy(Qt::ClickFocus);
	ui.frameDepartment->Initialize(ENTITY_BUS_DEPARTMENT,&m_lstData, "BPER_DEPARTMENT_ID");
	ui.frameContact->Initialize(ENTITY_BUS_CONTACT,&m_lstData, "BPER_CONTACT_ID");

	//---------------------------------------
	//init ACTUAL ORG SMR:
	//---------------------------------------
	ui.frameOrganization->Initialize(&m_lstData,"BPER_ORGANIZATION_ID");
	SetActualOrganizationHandler(ui.frameOrganization,"BPER_ORGANIZATION_ID");

	ui.tabWidget->setCurrentIndex(0); //set to first
	SetMode(MODE_EMPTY);

	ui.frameEditor->SetEmbeddedPixMode();

	//---------------------------------------
	//Init Role Combo.
	//---------------------------------------
	//Check what version is client.
	m_strEdition = g_pClientManager->GetIniFile()->m_strModuleCode;
	Status status;
	DbRecordSet recRoles;

	//!???.......this should be inside cache inside some global AR manager...u fucking noob...

	if("SC-TE" == m_strEdition)
		_SERVER_CALL(AccessRights->GetTERoles(status, recRoles))
	else
		_SERVER_CALL(AccessRights->GetBERoles(status, recRoles))
	//_CHK_ERR(status);
	
	//Fill role combo.
	int nRowCount = recRoles.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QString strRoleName = recRoles.getDataRef(i, "CROL_NAME").toString();
		int nRoleID = recRoles.getDataRef(i, "CROL_ID").toInt();
		ui.Role_comboBox->addItem(strRoleName, nRoleID);
		
		//Check is this role administrator role and store it for later - in personal edition all users are administrators.
		if (recRoles.getDataRef(i, "CROL_CODE").toInt() == ADMINISTRATOR_ROLE)
			m_nAdminRoleID = nRoleID;
	}

	if(!g_AccessRight->TestAR(MODIFY_FOREIGN_PERSONAL_SETTINGS))
	{
		ui.btnPersonalSettings->setVisible(false);
		return;
	}

	LoadStoredLists();

	//ui.tabWidget->removeTab(ui.tabWidget->count()-1); //remove reservations

}

FUI_BusPerson::~FUI_BusPerson()
{
	delete m_GuiFieldManagerMain;
}



//when programatically change data, call this:
void FUI_BusPerson::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();
	m_GuiFieldManagerUser->RefreshDisplay();
	ui.frameOrganization->RefreshDisplay();
	ui.frameDepartment->RefreshDisplay();
	ui.frameContact->RefreshDisplay();
	
	//PASSWORD refresh
	ui.btnSwitchToUser->setEnabled(false); //disable by default
	if(GetMode()==MODE_READ ||GetMode()==MODE_EDIT)
		LoadPassword();
	else
	{
		ui.groupBoxPassword->setChecked(false);
		ClearPassword();
	}

	ui.picture->RefreshDisplay();
	
}



//when changing state, call this:
void FUI_BusPerson::SetMode(int nMode)
{

	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		//ui.frameDetails->setEnabled(false);
		m_GuiFieldManagerMain->SetEditMode(false);
		ui.frameEditor->SetEditMode(false);
		m_GuiFieldManagerUser->SetEditMode(false);
		ui.frameDepartment->SetEditMode(false);
		ui.frameContact->SetEditMode(false);
		ui.picture->SetEditMode(false);
		ui.groupBoxPassword->setCheckable(false);
		ui.btnGeneratePassword->setEnabled(false);
		ui.txtPassword1->setEnabled(false);
		ui.txtPassword2->setEnabled(false);
		ui.frameSelection->setEnabled(true);
		ui.txtDirectPhone->setDragEnabled(false);
		ui.picture->SetEditMode(false); //enable picture
		ui.frameEditor->SetEditMode(false);
		ui.Role_comboBox->setEnabled(false);
		//ui.btnPersonalSettings->setEnabled(false);
		ui.cboDefaultStoredList->setEnabled(false);
		//ui.widgetReservations->SetEditMode(false);
	}
	else
	{
		m_GuiFieldManagerMain->SetEditMode(true);
		ui.frameDepartment->SetEditMode(true);
		ui.frameContact->SetEditMode(true);
		ui.picture->SetEditMode(true);
		ui.frameSelection->setEnabled(false);
		m_GuiFieldManagerUser->SetEditMode(true);
		ui.groupBoxPassword->setCheckable(true);
		ui.btnGeneratePassword->setEnabled(true);
		ui.txtPassword1->setEnabled(true);
		ui.txtPassword2->setEnabled(true);
		//ui.btnPersonalSettings->setEnabled(true);
		ui.cboDefaultStoredList->setEnabled(true);
		//ui.widgetReservations->SetEditMode(true);


		ui.txtDirectPhone->setDragEnabled(true);
		ui.picture->SetEditMode(true); //enable picture
		//RTF:
		ui.frameEditor->SetEditMode(true);
		//ui.btnEditPicture->setEnabled(true);

		//Set focus to Pers. No:
		ui.txtCode->setFocus(Qt::OtherFocusReason);

		if(m_strEdition != "SC-PE")
			ui.Role_comboBox->setEnabled(true);
	}


	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
	m_bPassWordChanged=false; //whwn FUI change state, clear flag
}




/*!
	Called after WRITE, refresh NAME column

	\param recNewData	- one row record with entity data: NOTE: ID key column must at index=0
*/
void FUI_BusPerson::prepareCacheRecord(DbRecordSet* recNewData)
{
	//fill name (calculated field):)
	DbRecordSet newCacheRecord;
	newCacheRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON_SELECTION));
	newCacheRecord.merge(*recNewData);

	//recNewData->Dump();

	//calculate PERS NAME:
	QString strName=m_CalcName.GetCalculatedName(*recNewData);
	newCacheRecord.setData(0,"BPER_NAME",strName);

	//calculate DEPT NAME based on ID (we do not have in insert mode dept code, name):
	QString strDeptName;
	if(newCacheRecord.getDataRef(0,"BPER_DEPARTMENT_ID").toInt()>0)
		strDeptName=ui.frameDepartment->GetSelectionController()->GetCalculatedName(newCacheRecord.getDataRef(0,"BPER_DEPARTMENT_ID").toInt());
	newCacheRecord.setData(0,"BPER_DEPT_NAME",strDeptName);

	if (recNewData->getColumnIdx("USER_ID")>0)
	{
		if (recNewData->getDataRef(0,"USER_ID").toInt()>0)
			newCacheRecord.setData(0,"IS_LOGIN_ACC",1);
		else
			newCacheRecord.setData(0,"IS_LOGIN_ACC",0);
	}

	*recNewData=newCacheRecord;
}




//---------------------------------------------------------------------------
//					PASSWORDS:
//---------------------------------------------------------------------------

//load dummy password:
void FUI_BusPerson::LoadPassword()
{
	ClearPassword(); //reset

	//set chkbox:
	if(m_lstCoreUser.getRowCount()>0) //edit && read mode if loaded set chkbox if exists
	{
		bool bIsUSer=(bool)m_lstCoreUser.getDataRef(0,"CUSR_ID").toInt();
		ui.groupBoxPassword->setChecked(bIsUSer);
		ui.btnSwitchToUser->setEnabled(bIsUSer);
		if(bIsUSer)
		{
			ui.txtPassword1->setText("xxxyyyzzz");
			ui.txtPassword2->setText("xxxyyyzzz");
		}
	}
	else
		ui.groupBoxPassword->setChecked(false);
}

//load dummy password:
void FUI_BusPerson::ClearPassword()
{
	ui.txtPassword1->clear();
	ui.txtPassword2->clear();
}
void FUI_BusPerson::on_txtPassword2_editingFinished()
{
	//set dirty flag only if different from DB:
	if(ui.txtPassword2->text()!="xxxyyyzzz")
		m_bPassWordChanged=true;
}
void FUI_BusPerson::on_txtPassword1_editingFinished()
{
	//set dirty flag only if different from DB:
	if(ui.txtPassword1->text()!="xxxyyyzzz")
		m_bPassWordChanged=true;
}


void FUI_BusPerson::on_btnGeneratePassword_clicked()
{
	QByteArray pass =Authenticator::GenerateRandomPassword(8);
	QString strPass(pass);
	ui.txtPassword1->setText(strPass);
	ui.txtPassword2->setText(strPass);
	QMessageBox::information(this,tr("New password set"),tr("Password: ")+strPass,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
	m_bPassWordChanged=true;
}



//before writing check if password is ok:
bool FUI_BusPerson::CheckPassword()
{

	if (ui.txtPassword2->text()!=ui.txtPassword1->text())
	{
		QMessageBox::warning(this,tr("Error"),tr("Password not match!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return false;
	}
	else
	{
		if(m_bPassWordChanged || ui.txtPassword2->text().isEmpty()) //if pass changed or set empty:
		{
			QString strUserName=m_lstCoreUser.getDataRef(0,"CUSR_USERNAME").toString();
			QByteArray pass;
			if(m_lstData.getDataRef(0,"BPER_FPERSON_SEQ").toInt()==0)
				pass=Authenticator::GeneratePassword(strUserName,ui.txtPassword2->text());
			else
				pass=ui.txtPassword2->text().toLatin1().toBase64(); //for old SPC users (B.T. 2013.02.06)

			m_lstCoreUser.setData(0,m_nPasswordIdx,pass);
		}

		return true;
	}
}



//check passwords:
bool FUI_BusPerson::on_cmdOK()
{
	DbRecordSet rowUserBeforeSave=m_lstCoreUser;

	//Add role to person.
	bool bRoleNotSelected = ui.Role_comboBox->currentText().isEmpty();
	if(m_strEdition != "SC-PE")
	{
		if (bRoleNotSelected)
		{
			QMessageBox::warning(this,tr("Warning"),tr("User role must be selected!"));
			ui.Role_comboBox->setFocus();
			return false;
		}
	}

	//user is added, edited or deleted:
	if(m_lstCoreUser.getRowCount()>0)
	{
		int nUserID=m_lstCoreUser.getDataRef(0,"CUSR_ID").toInt();

		//if groupbox flag is set:
		if(ui.groupBoxPassword->isChecked())
		{
			if(!CheckPassword()) return false; //password check failed!
			
			//if password not changed & user is for logged user, set password:
			if (!m_bPassWordChanged && g_pClientManager->GetPersonID()==m_nEntityRecordID && g_pClientManager->GetLoggedUserName()!=ui.txtUserName->text()) 
			{
				QMessageBox::warning(this,tr("Warning"),tr("You must set a password when you change the username!"));
				return false;
			}

			if(nUserID==0) //insert  prepare def. data:
			{
				m_lstCoreUser.setData(0,"CUSR_FIRST_NAME",ui.txtFirstName->text());
				m_lstCoreUser.setData(0,"CUSR_LAST_NAME",ui.txtLastName->text());
				m_lstCoreUser.setData(0,"CUSR_ACTIVE_FLAG",1);
				m_lstCoreUser.setData(0,"CUSR_IS_SYSTEM",0);
				m_lstCoreUser.setData(0,"CUSR_LOCAL_ACCESS_ONLY",0);
			}
		}
		else //flag is cleared: delete password data
		{

			m_lstCoreUser.clear(); //mark for clear;
			ClearPassword();
		}
	}

	//save personal list:
	int nProjectListID=ui.cboDefaultStoredList->itemData(ui.cboDefaultStoredList->currentIndex()).toInt();
	if (nProjectListID==0)
		m_lstData.setData(0,"BPER_DEF_PROJECT_LIST_ID",QVariant(QVariant::Int));
	else
		m_lstData.setData(0,"BPER_DEF_PROJECT_LIST_ID",nProjectListID);

	
	//execute:
	if (!FuiBase::on_cmdOK())
	{
		m_lstCoreUser=rowUserBeforeSave; //specific
		return false;
	}

	return true;
}

//when password chkbox:
void FUI_BusPerson::on_groupBoxPassword_toggled(bool)
{
	if(m_nMode==MODE_EDIT || m_nMode==MODE_INSERT)
	{
		//just check if empty record, then insert one:
		if(ui.groupBoxPassword->isChecked())
			if(m_lstCoreUser.getRowCount()==0)
			{
				m_lstCoreUser.addRow();
				m_lstCoreUser.setData(0,"CUSR_ACTIVE_FLAG",1);	
				m_lstCoreUser.setData(0,"CUSR_IS_SYSTEM",0);			
				m_lstCoreUser.setData(0,"CUSR_LOCAL_ACCESS_ONLY",0); 
			}
	}
}

void FUI_BusPerson::on_btnSwitchToUser_clicked()
{
	//if row count zero, exit
	if(m_lstCoreUser.getRowCount()==0)
		return;

	//get ID, if 0 exit
	int nUserID=m_lstCoreUser.getDataRef(0,"CUSR_ID").toInt();
	if(nUserID==0)
		return;

	//open FUI with user data:
	int nNewFUI = g_objFuiManager.OpenFUI(MENU_USER, true, false, FuiBase::MODE_READ, nUserID);

}


//set filters and refresh display for current Actual Org
void FUI_BusPerson::SetFiltersForCurrentActualOrganization(int nActualOrganizationID)
{
	//notify left selection (specific for each FUI):
	if (!ui.ckbShowAll->isChecked())
	{
		ui.frameSelection->ClearLocalFilter();
		ui.frameSelection->GetLocalFilter()->SetFilter("BPER_ORGANIZATION_ID",nActualOrganizationID);
		ui.frameSelection->ReloadData();
	}

	//notify all other selection assignment controllers
	ui.frameDepartment->GetSelectionController()->ClearLocalFilter();
	ui.frameDepartment->GetSelectionController()->GetLocalFilter()->SetFilter("BDEPT_ORGANIZATION_ID",nActualOrganizationID);
	ui.frameDepartment->SetActualOrganization(nActualOrganizationID);

}

//get FUI name
QString FUI_BusPerson::GetFUIName()
{
	if(m_nEntityRecordID>0)
		return tr("User: ")+ui.frameSelection->GetCalculatedName(m_nEntityRecordID);
	else
		return tr("User");
}

void FUI_BusPerson::OnContactSAPNEInsert()
{
	//new Contact is about to be inserted, fetch new FUI:
	FuiBase* pFUI=dynamic_cast<FuiBase*>(ui.frameContact->GetLastFUIOpen());
	if(pFUI==NULL) return;

	QCW_Base *pQCW=dynamic_cast<QCW_Base*>(pFUI);
	if(pQCW==NULL) return;


	//get person record, if not 1 row, abort
	if(m_lstData.getRowCount()!=1)
		return;

	//try to insert default values

	//---------------------------------------------
	//contact data:
	//---------------------------------------------
	DbRecordSet dataContact;
	BusinessDataTransformer::Person2Contact(m_lstData,dataContact);
	if (dataContact.getRowCount()>0)
		dataContact.setData(0,"BCNT_ORGANIZATIONNAME",ui.frameOrganization->GetCurrentActualOrganizationName());

	DbRecordSet recContactFull;
	recContactFull.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	ContactTypeManager::AddEmptyRowToFullContactList(recContactFull);
	recContactFull.assignRow(0,dataContact,true);

	//insert data:
	pQCW->SetDefaultValues(recContactFull,"");
}


void FUI_BusPerson::on_cmdInsert()
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_USERS))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return;
	}

	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_USER_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_PERSON, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		nCnt++; //<- hidden user counts
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed users!\nCan not insert new user!"));
			return;
		}
	}
	FuiBase::on_cmdInsert();
}

bool FUI_BusPerson::on_cmdEdit()
{

	if(!g_AccessRight->TestAR(ENTER_MODIFY_USERS))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return false;
	}

	return FuiBase::on_cmdEdit();
}

bool FUI_BusPerson::on_cmdDelete()
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_USERS))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return false;
	}

	return  FuiBase::on_cmdDelete();
}


//switch to detail tab:
void FUI_BusPerson::on_cmdCopy()
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_USERS))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return;
	}

	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_USER_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_PERSON, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		nCnt++; //<- hidden user cou
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed users!\nCan not insert new user!"));
			return;
		}
	}

	FuiBase::on_cmdCopy();
}

void FUI_BusPerson::on_selectionChange(int nEntityRecordID)
{
	//Select role in role combo.
	if (nEntityRecordID > -1)
	{
		DbRecordSet recRoleList;
		Status status;
		_SERVER_CALL(AccessRights->GetPersonRoleList(status, recRoleList, nEntityRecordID))
		_CHK_ERR(status);
		if (recRoleList.getRowCount()>0)
		{
			int nRoleID = recRoleList.getDataRef(0, "CROL_ID").toInt();
			int nIndex = ui.Role_comboBox->findData(nRoleID);
			ui.Role_comboBox->setCurrentIndex(nIndex);
		}
		else
			ui.Role_comboBox->setCurrentIndex(-1);
	}

	FuiBase::on_selectionChange(nEntityRecordID);
	
	int nProjectListID=0;
	if (m_lstData.getRowCount()>0)
		nProjectListID=m_lstData.getDataRef(0,"BPER_DEF_PROJECT_LIST_ID").toInt();
	ui.cboDefaultStoredList->setCurrentIndex(ui.cboDefaultStoredList->findData(nProjectListID));
}

void FUI_BusPerson::on_ckbShowAll_stateChanged(int)
{
	if (ui.ckbShowAll->isChecked())
	{
		ui.frameSelection->ClearLocalFilter();
		ui.frameSelection->ReloadData();
	}
	else
	{
		ui.frameSelection->ClearLocalFilter();
		ui.frameSelection->GetLocalFilter()->SetFilter("BPER_ORGANIZATION_ID",ui.frameOrganization->GetCurrentActualOrganizationID());
		ui.frameSelection->ReloadData();
	}
}

void FUI_BusPerson::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);
	if (pSubject==ui.frameContact && nMsgCode==SelectorSignals::SELECTOR_ON_AFTER_OPEN_INSERT_HANDLER)
	{
		OnContactSAPNEInsert();
		return;
	}
	else if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_STORED_PROJECT_LISTS_UPDATED)
	{
		//reload the stored project lists list
		LoadStoredLists();
		return;
	}
	else if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
	{
		on_ckbShowAll_stateChanged(1);
		return;
	}
}

void FUI_BusPerson::DataPrepareForInsert(Status &err)
{
	DataClear();
	m_lstData.addRow();
	//m_lstCoreUser.addRow();

	m_lstData.setData(0,"BPER_OCCUPATION",(double)100);	//set to 100
	m_lstData.setData(0,"BPER_ACTIVE_FLAG",1);	//set active by default
	m_lstData.setData(0,"BPER_SEX",0);			//set male
	m_lstData.setData(0,"BPER_DATE_ENTERED",QDate::currentDate()); //set enter date to today

	//m_lstCoreUser.setData(0,"CUSR_ACTIVE_FLAG",1);	
	//m_lstCoreUser.setData(0,"CUSR_IS_SYSTEM",0);			
	//m_lstCoreUser.setData(0,"CUSR_LOCAL_ACCESS_ONLY",0); 
}

void FUI_BusPerson::DataClear()
{
	m_lstData.clear();
	m_lstCoreUser.clear();
	//ui.widgetReservations->Clear();
}

void FUI_BusPerson::DataDefine()
{
	DbSqlTableDefinition::GetKeyData(m_nTableID,m_TableData);
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
	m_lstData.addColumn(QVariant::ByteArray,"BPIC_PICTURE");
	m_lstData.addColumn(QVariant::Int,"USER_ID");

	m_lstCoreUser.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USER));
}

void FUI_BusPerson::DataRead(Status &err,int nRecordID)
{
	DbRecordSet recRoleList;
	_SERVER_CALL(BusPerson->ReadData(err,nRecordID,m_lstData,m_lstCoreUser,recRoleList))
	if(err.IsOK())
	{
		//ui.widgetReservations->Reload(err,nRecordID);
		//if (!err.IsOK())
		//	return;

		m_lstDataCache=m_lstData;
		m_lstCoreUserCache=m_lstCoreUser;
		if (recRoleList.getRowCount()>0)
		{
			int nRoleID = recRoleList.getDataRef(0, "CROL_ID").toInt();
			int nIndex = ui.Role_comboBox->findData(nRoleID);
			ui.Role_comboBox->setCurrentIndex(nIndex);
		}
		else
			ui.Role_comboBox->setCurrentIndex(-1);
	}
}

void FUI_BusPerson::DataWrite(Status &err)
{
	//Get new role.
	int nRoleId;
	if (m_strEdition != "SC-PE")
		nRoleId = ui.Role_comboBox->itemData(ui.Role_comboBox->currentIndex()).toInt();
	else
		nRoleId = m_nAdminRoleID;

	_SERVER_CALL(BusPerson->WriteData(err,m_lstData,m_lstCoreUser,m_strLockedRes,nRoleId));
	if (err.IsOK())
	{
		int nRecordID=m_lstData.getDataRef(0,"BPER_ID").toInt();
		//ui.widgetReservations->Write(err,nRecordID);
		m_strLockedRes="";
		m_lstDataCache=m_lstData;
		m_lstCoreUserCache=m_lstCoreUser;
	}
}

void FUI_BusPerson::DataPrepareForCopy(Status &err)
{
	//read big picture for main record, reset id's
	ClientContactManager::LoadBigPictureList(err,m_lstData,"BPER_BIG_PICTURE_ID");
	if (!err.IsOK()) return;

	m_lstData.setColValue(m_lstData.getColumnIdx("BPER_ID"),0);
	m_lstData.setColValue(m_lstData.getColumnIdx("BPER_BIG_PICTURE_ID"),0);
	m_lstData.setColValue(m_lstData.getColumnIdx("USER_ID"),0);

	m_lstCoreUser.setColValue(m_lstData.getColumnIdx("CUSR_PERSON_ID"),0);
	m_lstCoreUser.setColValue(m_lstData.getColumnIdx("CUSR_ID"),0);

	//DbRecordSet data;
	//ui.widgetReservations->GetData(data);
	//data.setColValue("BCRS_ID",QVariant(QVariant::Int));
	//ui.widgetReservations->Reload(err,-1,&data);
}

void FUI_BusPerson::DataUnlock(Status &err)
{
	FuiBase::DataUnlock(err);
	if (err.IsOK() && m_nMode==MODE_EDIT)
	{
		m_lstCoreUser=m_lstCoreUserCache;
		//if (m_nEntityRecordID>0)
		//	ui.widgetReservations->Reload(err,m_nEntityRecordID); //instead of cache just reload widget...
		//else
		//	ui.widgetReservations->Clear();
	}
}


void FUI_BusPerson::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);


	//DataForWrite.debugPrintInfo();
	if(m_lstData.getDataRef(0,"BPER_LAST_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Last Name is mandatory field!"));	return;	}

	//B.T. issue 2572 on 29.02.2012
	//if(m_lstData.getDataRef(0,"BPER_FIRST_NAME").toString().isEmpty())
	//{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("First Name is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"BPER_CODE").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Pers. No. is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"BPER_INITIALS").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Initials is mandatory field!"));	return;	}


	if (m_lstCoreUser.getRowCount()>0)
	{
		//DataForWrite.debugPrintInfo();
		if(m_lstCoreUser.getDataRef(0,"CUSR_LAST_NAME").toString().isEmpty())
		{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Last Name is mandatory field!"));	return;	}

		//B.T. issue 2572 on 29.02.2012
		//if(m_lstCoreUser.getDataRef(0,"CUSR_FIRST_NAME").toString().isEmpty())
		//{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("First Name is mandatory field!"));	return;	}

		if(m_lstCoreUser.getDataRef(0,"CUSR_USERNAME").toString().isEmpty())
		{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Username is mandatory field!"));	return;	}

	//	if(m_lstCoreUser.getDataRef(0,"CUSR_PASSWORD").toString().isEmpty())
	//	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Password must be set!"));	return;	}
	}

}


void FUI_BusPerson::on_btnPersonalSettings_clicked()
{
	if (m_nEntityRecordID>0)
	{
		g_objFuiManager.OpenFUI(MENU_PERSONAL_SETTINGS,true,false,FuiBase::MODE_READ,m_nEntityRecordID);
	}
}

void FUI_BusPerson::LoadStoredLists()
{
	ui.cboDefaultStoredList->clear();

	Status status;
	DbRecordSet recLists;
	_SERVER_CALL(StoredProjectLists->GetListNames(status, recLists /*,g_pClientManager->GetPersonID()*/)) //Project grid id
	if (!status.IsOK()) //no error from construcotr...
		return;

	//Fill combo.
	int nRowCount = recLists.getRowCount();
	if(nRowCount > 0)
	{
		//empty list entry
		ui.cboDefaultStoredList->addItem(tr("(none)"), 0);

		for(int i = 0; i < nRowCount; i++)
		{
			int ViewID = recLists.getDataRef(i, "BSPL_ID").toInt();
			QString strViewName = recLists.getDataRef(i, "BSPL_NAME").toString();
			ui.cboDefaultStoredList->addItem(strViewName, ViewID);
		}
	}
}

void FUI_BusPerson::SetFUIStyle()
{
	FuiBase::SetFUIStyle();
}