#include "fui_projects.h"
#include "fuimanager.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "bus_client/bus_client/generatecodedlg.h"
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "bus_client/bus_client/selection_edittemplatetree.h"
#include <QtWidgets/QMessageBox>
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "gui_core/gui_core/macros.h"
#include "fui_calendar.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"
 //defines FP codes
#include "communicationactions.h"

#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/gui_helper.h"
#include "common/common/datahelper.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "gui_core/gui_core/thememanager.h"
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager


FUI_Projects::FUI_Projects(QWidget *parent, bool bOpenedAsAvatar /*= false*/)
    : FuiBase(parent),m_pCalendarButton(NULL)
{
	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);

	m_bHideOnMinimize = true;
	m_bOpenedAsAvatar = bOpenedAsAvatar;

	//SIDEBAR engaged:
	m_p_CeMenu=ui.frameCommToolBar;
	m_p_Selector=ui.frameSelection;
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
	{
		m_p_CeMenu=g_objFuiManager.GetSideBarCommMenuSelector(MENU_PROJECTS);
		if (m_p_CeMenu)
			m_p_Selector=dynamic_cast<Selection_TreeBased*>(m_p_CeMenu->GetSelector());

		if (!m_p_CeMenu)
			m_p_CeMenu=ui.frameCommToolBar;
		else
			ui.frameCommToolBar->setVisible(false);
		if (!m_p_Selector)
		{
			m_p_Selector=ui.frameSelection;
			ui.frameSelection->Initialize(ENTITY_BUS_PROJECT,false, true, true, false);	//init selection controller
		}
		else
		{
			connect(m_p_Selector,SIGNAL(destroyed()),this,SLOT(On_SelectionControllerSideBarDestroyed()));
			ui.frameSelection->setVisible(false);
		}
		m_bSideBarModeView=true;

		resize(640,480);
	}
	else
	{
		ui.frameSelection->Initialize(ENTITY_BUS_PROJECT,false, true, true, false);	//init selection controller
		resize(1200,700);
	}

	InitFui(ENTITY_BUS_PROJECT, BUS_PROJECT,dynamic_cast<MainEntitySelectionController*>(m_p_Selector),ui.btnButtonBar);
	EnableHierarchyCodeCheck("BUSP_CODE");
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);
	ui.widgetReservations->Initialize(BUS_PROJECT);

	ui.tableCustomFields->Initialize(&m_lstCustomFields);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------

	//Connect fields to the data:
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_BUS_PROJECT);
	m_GuiFieldManagerMain->RegisterField("BUSP_CODE",ui.txtCode);
	m_GuiFieldManagerMain->RegisterField("BUSP_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BUSP_START_DATE",ui.txtStartDate);
	m_GuiFieldManagerMain->RegisterField("BUSP_DEADLINE_DATE",ui.txtDeadlineDate);
	m_GuiFieldManagerMain->RegisterField("BUSP_OFFER_DATE",ui.txtOfferDate);
	m_GuiFieldManagerMain->RegisterField("BUSP_OFFER_VALID_DATE",ui.txtOfferValidDate);
	m_GuiFieldManagerMain->RegisterField("BUSP_ORDER_DATE",ui.txtOrderDate);
	m_GuiFieldManagerMain->RegisterField("BUSP_COMPLETION_DATE",ui.txtCompletionDate);
	m_GuiFieldManagerMain->RegisterField("BUSP_LATEST_COMPLETION_DATE",ui.txtLatestCompletionDate);
	m_GuiFieldManagerMain->RegisterField("BUSP_CONTRACT_DATE",ui.txtContractDate);
	m_GuiFieldManagerMain->RegisterField("BUSP_ACTIVE_FLAG",ui.chkActive);
	m_GuiFieldManagerMain->RegisterField("BUSP_DESCRIPTION",ui.frameDesc);
	m_GuiFieldManagerMain->RegisterField("BUSP_LOCATION",ui.frameLocation);
	m_GuiFieldManagerMain->RegisterField("BUSP_TIME_BUDGET",ui.txtTimeBudget);
	m_GuiFieldManagerMain->RegisterField("BUSP_FIXED_FEE",ui.txtFixedFee);
	m_GuiFieldManagerMain->RegisterField("BUSP_COST_BUDGET",ui.txtCostBudget);
	m_GuiFieldManagerMain->RegisterField("BUSP_TEMPLATE_FLAG",ui.chkIsTemplate);
	
	ui.frmLeader->Initialize(ENTITY_BUS_PERSON,&m_lstData, "BUSP_LEADER_ID");
	ui.frmResponsible->Initialize(ENTITY_BUS_PERSON,&m_lstData, "BUSP_RESPONSIBLE_ID");
	ui.frmOrganization->Initialize(ENTITY_BUS_ORGANIZATION,&m_lstData, "BUSP_ORGANIZATION_ID");
	ui.frmDepartment->Initialize(ENTITY_BUS_DEPARTMENT,&m_lstData, "BUSP_DEPARTMENT_ID");

	//---------------------------------------
	//init ACTUAL ORG SMR:
	//---------------------------------------
	ui.frmActualOrganization->Initialize();
	SetActualOrganizationHandler(ui.frmActualOrganization,"BUSP_ORGANIZATION_ID");

	//register this FUI as observer of ORG (when ORG changes set FILTER on Dept):
	ui.frmOrganization->registerObserver(this);
	
	//Set Icon on RTF EDIT

	EnableHierarchyCodeCheck("BUSP_CODE"); //check for reload

	//assigments:
	QList<int> lst;
	lst<<NMRXManager::PAIR_CONTACT_PROJECT<<NMRXManager::PAIR_PERSON_PROJECT<<NMRXManager::PAIR_PROJECT_CONTACT;
	ui.widgetCommPerson->Initialize(tr("Assigned Users And Contacts"),BUS_PROJECT,lst,BUS_CM_CONTACT,BUS_PERSON);
	connect(ui.widgetCommPerson,SIGNAL(ContentChanged()),this,SLOT(OnNMRXContactsChanged()));


	//set edit to false:
	SetMode(MODE_EMPTY);
	//ui.txtName->setFocus();

	ui.frameDesc->SetEmbeddedPixMode();
	ui.frameLocation->SetEmbeddedPixMode();

	//--------------------------------------------------
	//		Contact/Communication tab.
	//--------------------------------------------------
	ui.contactGrid->Initialize(CommGridViewHelper::PROJECT_GRID, this);

	//CE signals
	connect(ui.contactGrid,SIGNAL(NeedNewData(int)),this,SLOT(OnCEMenu_NeedNewData(int)));
	connect(m_p_CeMenu,SIGNAL(NeedNewData(int)),this,SLOT(OnCEMenu_NeedNewData(int)));
	connect(m_p_CeMenu->m_menuDropZone,SIGNAL(NewDataDropped(int,DbRecordSet)),this,SLOT(OnCEMenu_NewDataDropped(int,DbRecordSet)));

	m_pWndAvatar = NULL;

	m_pCalendarButton =	new StyledPushButton(this,":CalendarEvent24.png",":CalendarEvent24.png","","",0,0);
	m_pCalendarButton->setMinimumSize(QSize(24,24));
	m_pCalendarButton->setMaximumSize(QSize(24,24));
	m_pCalendarButton->setToolTip(tr("Contact Calendar Entries"));
	ui.btnButtonBar->GetButtonLayout()->insertWidget(4,m_pCalendarButton);
	connect(m_pCalendarButton,SIGNAL(clicked()),this,SLOT(OnBtnCalendar_Click()));
	m_pCalendarButton->setEnabled(false);

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
		m_pCalendarButton->setVisible(false);

	if (!g_FunctionPoint.IsFPAvailable(FP_RESERVATIONS__PROJECT))
	{
		ui.tabGroups->widget(TAB_DETAIL_RESERVATIONS)->setVisible(false);
		ui.tabGroups->removeTab(TAB_DETAIL_RESERVATIONS);
	}


	


	if (!m_bSideBarModeView)
	{
		QTimer::singleShot(0,this,SLOT(OnSetFindFocusDelayed()));
	}

	connect(ui.tabWidgetData,SIGNAL(currentChanged(int)),this,SLOT(OnTabWidgetData_currentChanged(int)));
	OnTabWidgetData_currentChanged(ui.tabWidgetData->currentIndex());
}

FUI_Projects::~FUI_Projects()
{
	delete m_GuiFieldManagerMain;
}

//when programatically change data, call this:
void FUI_Projects::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frmLeader->RefreshDisplay();
	ui.frmResponsible->RefreshDisplay();
	ui.frmOrganization->RefreshDisplay();
	ui.frmDepartment->RefreshDisplay();
	ui.frmActualOrganization->RefreshDisplay();
	ui.tableCustomFields->RefreshDisplay();
}

//when changing state, call this:
void FUI_Projects::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameSelection->setEnabled(true);
		//ui.frameDetails->setEnabled(true);			//needed because of inverse state of one btn

		//ui.frmActualOrganization->setEnabled(true);
		m_GuiFieldManagerMain->SetEditMode(false);		//all widgets

		//SAPNE
		ui.frmLeader->SetEditMode(false);
		ui.frmResponsible->SetEditMode(false);
		ui.frmOrganization->SetEditMode(false);
		ui.frmDepartment->SetEditMode(false);
		ui.arSelector->SetEditMode(false);
		ui.tabWidgetComm->setEnabled(true);
		//ui.btnButtonBar->SetMode(nMode); //this is necessary to reeanble buttons!
		ui.widgetReservations->SetEditMode(false);
		ui.tableCustomFields->SetEditMode(false);
	}
	else
	{
		ui.tabWidgetComm->setEnabled(false);
		ui.frameSelection->setEnabled(false);
		ui.arSelector->SetEditMode(true);

		//ui.frmActualOrganization->setEnabled(true);
		m_GuiFieldManagerMain->SetEditMode(true);		//all widgets

		//SAPNE
		ui.frmLeader->SetEditMode(true);
		ui.frmResponsible->SetEditMode(true);
		ui.frmOrganization->SetEditMode(true);
		ui.frmDepartment->SetEditMode(true);
		ui.widgetReservations->SetEditMode(true);
		ui.tableCustomFields->SetEditMode(true);

		//Set focus to Pers. No:
		ui.txtCode->setFocus(Qt::OtherFocusReason);

	}


	if(nMode==MODE_INSERT || nMode==MODE_EMPTY)
	{
		
		ui.widgetCommPerson->Invalidate();
		ui.contactGrid->ClearCommGrid();	//if insert, clear grid
	}

	if (m_pCalendarButton)
	{
		if (nMode==MODE_EDIT || nMode==MODE_READ)
		{
			bool bEnableCalendar=true;
			if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_PROJECT_CALENDARS))
			{
				bEnableCalendar=false;
			}
			m_pCalendarButton->setEnabled(bEnableCalendar);
		}
		else
			m_pCalendarButton->setEnabled(false);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}


void FUI_Projects::on_cmdInsert()
{
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_PROJECT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_PROJECT_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_PROJECT, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed projects!\nCan not insert new project!"));
			return;
		}
	}

	FuiBase::on_cmdInsert();
	ui.tabWidgetData->setCurrentIndex(1);
	ui.txtName->setFocus();

	//Set default ORG=ACO
	int nOrgID=ui.frmActualOrganization->GetCurrentActualOrganizationID();
	ui.frmOrganization->SetCurrentEntityRecord(nOrgID);
}

//switch to detail tab:
bool FUI_Projects::on_cmdEdit()
{
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_PROJECT,UserAccessRight::OP_EDIT,m_nEntityRecordID))
		_CHK_ERR_RET_BOOL_ON_FAIL(err);

	ui.tabWidgetData->setCurrentIndex(1);
	return FuiBase::on_cmdEdit();
}

//switch to detail tab:
void FUI_Projects::on_cmdCopy()
{
	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_PROJECT_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_PROJECT, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed projects!\nCan not insert new project!"));
			return;
		}
	}

	ui.tabWidgetData->setCurrentIndex(1);
	FuiBase::on_cmdCopy();
}

bool FUI_Projects::on_cmdOK()
{
	int nModePrevious=m_nMode;

	//make sure code ends with delimiter
	QString strCode = m_lstData.getDataRef(0, "BUSP_CODE").toString();
	if (strCode.right(1) != "."){
		strCode += ".";
		m_lstData.setData(0, "BUSP_CODE", strCode);
	}
	
	bool bOK =FuiBase::on_cmdOK();

	//only after insert:
	if (bOK && nModePrevious==MODE_INSERT)
	{
		QString strName=m_p_Selector->GetCalculatedName(m_nEntityRecordID);
		ui.widgetCommPerson->Reload(strName,m_nEntityRecordID,true);
		//Grid.
		ui.contactGrid->SetData(m_nEntityRecordID);
	}

	return bOK;
}

bool FUI_Projects::on_cmdDelete()
{
	Status err; 
	if(!g_AccessRight->IsOperationAllowed(err,BUS_PROJECT,UserAccessRight::OP_DELETE,m_lstData))
		_CHK_ERR_RET_BOOL_ON_FAIL(err);

	return FuiBase::on_cmdDelete();
}

//set filters and refresh display for current Actual Org
void FUI_Projects::SetFiltersForCurrentActualOrganization(int nActualOrganizationID)
{
}

//get FUI name for combo box
QString FUI_Projects::GetFUIName()
{
	/*
	int nRecordID = 0;
	QString strCode, strName;
	DbRecordSet lstData;
	m_p_Selector->GetSelection(nRecordID, strCode, strName, lstData);
	if(nRecordID>0)
	*/
	QString strName=MainEntityCalculatedName::GetCalculatedName(BUS_PROJECT,m_lstData);
	if (!strName.isEmpty())
		return tr("Project: ")+ strName; //m_p_Selector->GetCalculatedName(nRecordID);
	else
		return tr("Project");
}

// when ORG changes, set filter on Dept
void FUI_Projects::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);

	//IF ORG
	if(pSubject==ui.frmOrganization && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		ui.frmDepartment->GetSelectionController()->ClearLocalFilter();
		ui.frmDepartment->GetSelectionController()->GetLocalFilter()->SetFilter("BDEPT_ORGANIZATION_ID",nMsgDetail);
	}
	/* B.T. 2008: issue 1201, transfered into Fui_base
	else if(m_pSelectionController==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_INSERT_NEW_FROM_TEMPLATE){
		Op_NewFromTemplate();
	}
	else if(m_pSelectionController==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_INSERT_NEW_STRUCT_FROM_TEMPLATE){
		Op_NewStructFromTemplate();
	}
	*/
	/*
	else if(m_pSelectionController==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_PROJECT_LIST_LOAD){
		on_ProjectListLoad();
	}
	else if(m_pSelectionController==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_PROJECT_LIST_CREATE){
		on_ProjectListCreate();
	}
	else if(m_pSelectionController==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_PROJECT_LIST_MODIFY){
		on_ProjectListModify();
	}
	else if(m_pSelectionController==pSubject && nMsgCode==SelectorSignals::SELECTOR_ON_PROJECT_LIST_DELETE){
		on_ProjectListDelete();
	}
	*/
}

void FUI_Projects::on_NewFromTemplate(bool bCopyCE)
{
	//calculate code of a selected item
	QString strParentCode;
	DbRecordSet *cacheData= g_ClientCache.GetCache(m_nEntityID);
	if(cacheData)
	{
		if(m_nEntityRecordID >= 0)
		{
			int nIdCol = cacheData->getColumnIdx(m_TableData.m_strPrimaryKey);
			Q_ASSERT(nIdCol >= 0);
			int nCodeCol = cacheData->getColumnIdx(m_strCodeField);
			Q_ASSERT(nCodeCol >= 0);

			int nParentIdx = cacheData->find(nIdCol, m_nEntityRecordID, true);
			//Q_ASSERT(nParentIdx >= 0);
			if(nParentIdx >= 0)
				cacheData->getData(nParentIdx, nCodeCol, strParentCode);
		}
	}

	//STEP 1: pick new code and a template source
	generatecodedlg dlg;
	dlg.m_strFldPrefix  = m_strTablePrefix;
	dlg.m_nEntityType   = m_nEntityID;
	dlg.SetParentCode(strParentCode);
	if(0 == cacheData->getRowCount())
		dlg.SetProposedCode("P.");	//default
	else
		dlg.SetProposedCode(strParentCode);
	dlg.EnableNameInput();
	dlg.EnableTemplateInput();
	int nResult=dlg.exec();
	if(nResult)
	{
		//STEP 2: user can edit selected nodes in another three-based dialog
		DbRecordSet lstRecords;
		lstRecords.addColumn(QVariant::Int,		"BUSP_ID");
		lstRecords.addColumn(QVariant::String,	"BUSP_CODE");
		lstRecords.addColumn(QVariant::Int,		"BUSP_LEVEL");
		lstRecords.addColumn(QVariant::Int,		"BUSP_PARENT");
		lstRecords.addColumn(QVariant::Int,		"BUSP_HASCHILDREN");
		lstRecords.addColumn(QVariant::Int,		"BUSP_ICON");
		lstRecords.addColumn(QVariant::Int,		"BUSP_MAINPARENT_ID");
		lstRecords.addColumn(QVariant::String,	"BUSP_NAME");
		lstRecords.addColumn(QVariant::Int,		"BUSP_STYLE");
		lstRecords.addColumn(QVariant::Int,		"BUSP_ACTIVE_FLAG");
		lstRecords.addColumn(QVariant::Int,		"TEMPLATE_ID");
		lstRecords.addColumn(QVariant::String,	"TEMPLATE_CODE");

		QString strTemplateCode,strName;
		int nTemplateID = dlg.GetTemplateData(strTemplateCode,strName);
		if(nTemplateID < 1){
			if(dlg.IsUseTemplateSelected()){
				QMessageBox::information(NULL, "Error", tr("No template selected!"));
				return;
			}

			//else create project without template
			if (m_lstData.getRowCount()>0)
			{
				//reset ID for the new record to be generated
				m_lstData.clear();
				m_lstData.addRow();

				//add new info
				m_lstData.setData(0,m_strCodeField, dlg.m_strProposedCode);
				m_lstData.setData(0,m_strNameField, dlg.m_strProposedName);
				SetMode(MODE_INSERT);
			}
			return;
		}

		QString strTopCode = dlg.m_strProposedCode;

		lstRecords.addRow();
		lstRecords.setData(0, "BUSP_LEVEL", 1);
		lstRecords.setData(0, "BUSP_CODE", "");	//code is relative to the top code
		lstRecords.setData(0, "BUSP_NAME", dlg.m_strProposedName);
		lstRecords.setData(0, "BUSP_ACTIVE_FLAG", 1);
		lstRecords.setData(0, "TEMPLATE_ID", nTemplateID);

		//add row for each template sub
		if(nTemplateID>0 && cacheData)
		{
			int nCount = HierarchicalHelper::SelectChildren(strTemplateCode, *cacheData, "BUSP_CODE");
			int nIdx = cacheData->getSelectedRow();
			while(nIdx >= 0)
			{
				lstRecords.addRow();
				int nPos = lstRecords.getRowCount()-1;

				QString strCode = cacheData->getDataRef(nIdx, "BUSP_CODE").toString();
				strCode = strCode.right(strCode.length()-strTemplateCode.length());

				lstRecords.setData(nPos, "BUSP_LEVEL", 1);
				lstRecords.setData(nPos, "BUSP_CODE", strCode);
				lstRecords.setData(nPos, "BUSP_NAME", cacheData->getDataRef(nIdx, "BUSP_NAME").toString());
				lstRecords.setData(nPos, "BUSP_ACTIVE_FLAG", 1);
				lstRecords.setData(nPos, "TEMPLATE_ID", cacheData->getDataRef(nIdx, "BUSP_ID").toInt());

				nIdx = cacheData->getSelectedRow(nIdx+1);
			}
		}

		//show dialog with the data
		Selection_EditTemplateTree dlg1;
		dlg1.SetData(lstRecords, "BUSP_CODE", "BUSP_NAME");
		if(dlg1.exec())
		{
			//get survivors
			lstRecords = dlg1.m_lstData;

			//fix all the codes (add selected prefix)
			int i;
			int nMax = lstRecords.getRowCount();
			for(i=0; i<nMax; i++)
			{
				QString strCode = strTopCode + lstRecords.getDataRef(i, "BUSP_CODE").toString();
				lstRecords.setData(i, "BUSP_CODE", strCode);
			}

			Status status;
			_SERVER_CALL(BusProject->NewProjectsFromTemplate(status, lstRecords, bCopyCE))
			if(!status.IsOK()){
				QMessageBox::information(NULL, "Error", status.getErrorText());
				return;
			}

			//add new items into the cache

			//if has lines:
			if (lstRecords.getRowCount()>0)
			{
				MainEntityCalculatedName namer;
				namer.Initialize(ENTITY_BUS_PROJECT,lstRecords);
				int nSize=lstRecords.getRowCount();
				lstRecords.clearSelection();

				for(int i=0;i<nSize;++i){
					if (lstRecords.getDataRef(i,0).isNull())
						lstRecords.selectRow(i);
				}

				lstRecords.deleteSelectedRows();
				if (lstRecords.getRowCount()>0) //notify cache:
				{
					g_ClientCache.ModifyCache(ENTITY_BUS_PROJECT,lstRecords,DataHelper::OP_ADD,this);

					if(QMessageBox::Yes == QMessageBox::question(NULL, tr("Project"), tr("Do you want to proceed to edit project details?"), QMessageBox::Yes|QMessageBox::No))
					{
						//edit first of the inserted records
						int nRecordID = lstRecords.getDataRef(0, "BUSP_ID").toInt();
						m_pSelectionController->SetCurrentEntityRecord(nRecordID);
						on_selectionChange(nRecordID);
						on_cmdEdit();
					}
				}
			}
		}
	}
	else
	{
		if (m_bSideBarModeView) //issue 1461
		{
			close(); //close fui
		}

	}
}

void FUI_Projects::on_NewStructFromTemplate()
{
	on_NewFromTemplate(false);
}

void FUI_Projects::prepareCacheRecord(DbRecordSet *recNewData)
{
	//TO FIX: copy all lists:

	//fill name (calculated field):)
	DbRecordSet newCacheRecord;
	newCacheRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
	newCacheRecord.merge(*recNewData);

	//QString strName=m_CalcName.GetCalculatedName(*recNewData);
	//newCacheRecord.setData(0,"BCNT_NAME",strName);

	*recNewData=newCacheRecord;
}



//reload 
void FUI_Projects::on_selectionChange(int nEntityRecordID)
{
	if(nEntityRecordID==m_nEntityRecordID) return; //if already loaded exit
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_PROJECT,UserAccessRight::OP_READ,nEntityRecordID))
		_CHK_ERR(err);

	FuiBase::on_selectionChange(nEntityRecordID);

	//get contact name:
	QString strName=m_p_Selector->GetCalculatedName(nEntityRecordID);
	ui.widgetCommPerson->Reload(strName,nEntityRecordID);


	//Grid - complex filter or not.
	if (!m_bOpenedAsAvatar)
		ui.contactGrid->SetData(nEntityRecordID);
	else
		m_bOpenedAsAvatar = false;
}

void FUI_Projects::ReloadFromAvatar()
{
	ui.contactGrid->SetData(m_nEntityRecordID);
}

void FUI_Projects::OnNMRXContactsChanged()
{
	int nContacts=ui.widgetCommPerson->GetRowCountForAssignment(BUS_CM_CONTACT);
	int nPersons=ui.widgetCommPerson->GetRowCountForAssignment(BUS_PERSON);

	QString strTabTitle="";
	if (nContacts)
		strTabTitle+=tr("Contacts (")+QVariant(nContacts).toString()+")";
	else
		strTabTitle+=tr("Contacts");
	strTabTitle+=tr(" And ");
	if (nPersons)
		strTabTitle+=tr("Users (")+QVariant(nPersons).toString()+")";
	else
		strTabTitle+=tr("Users");

	ui.tabWidgetComm->setTabText(1,strTabTitle);

}

void FUI_Projects::SaveCommGridChBoxFilterState(int nState, int nSettingID)
{
	//Get state.
	bool bState = false;
	if (nState==Qt::Checked)
		bState = true;

	//Write.
	g_pSettings->SetPersonSetting(nSettingID, bState);
}

//0 - comm grid,
//1 - details
void FUI_Projects::SetCurrentTab(int nTabPane, int nDetailTab)
{
	ui.tabWidgetData->setCurrentIndex(nTabPane);
	OnTabWidgetData_currentChanged(nTabPane);
	if (nTabPane==0)
	{
		if (nDetailTab>=0)
			ui.tabWidgetComm->setCurrentIndex(nDetailTab);
	}
	else
	{
		if (nDetailTab>=0)
			ui.tabGroups->setCurrentIndex(nDetailTab);
	}
}

void FUI_Projects::GetCurrentTab(int &nTabPane, int &nDetailTab, int &nCommTab)
{
	nTabPane	= ui.tabWidgetData->currentIndex();
	nDetailTab	= ui.tabGroups->currentIndex();
	nCommTab	= ui.tabWidgetComm->currentIndex();
}

void FUI_Projects::OnCEMenu_NeedNewData(int nSubTypeMenu)
{
	if (!g_objFuiManager.IsActiveWindowCurrentFUI(this) && m_bSideBarModeView) 
		return;

	m_p_CeMenu->SetDefaultMail("");
	m_p_CeMenu->SetDefaultPhone("");
	m_p_CeMenu->SetDefaultInternet("");



	//which contact is actual:
	DbRecordSet lstProject;
	lstProject.addColumn(QVariant::Int,"BUSP_ID");

	if (m_nEntityRecordID>0)
	{
		lstProject.addRow();
		lstProject.setData(0,0,m_nEntityRecordID);
		//g_CommManager.SetDocumentDropDefaults(NULL,&lstProject);
	}



	/*
	switch(ui.tabWidgetData->currentIndex())
	{
	case 0:
		{
*/
			//if (ui.tabWidgetComm->currentIndex()==0)
			//{
				//issue: 1817: do not assign contact
				/*
				//which contact is actual:
				DbRecordSet lstContacts;
				lstContacts.addColumn(QVariant::Int,"BCNT_ID");
				DbRecordSet record;
				int nContactID;
				if(ui.contactGrid->GetContactSAPNE()->GetCurrentEntityRecord(nContactID,record))
				{
					lstContacts.addRow();
					lstContacts.setData(0,0,nContactID);
				}
				m_p_CeMenu->SetDefaults(&lstContacts,&lstProject);
				*/
			//	return;
			//}
			//else //project team:
			//{
			//reset contact:

				if (m_nEntityRecordID>0 && nSubTypeMenu==CommunicationMenu_Base::SUBMENU_EMAIL)
				{
					//get selected contacts:
					DbRecordSet lstSelectedContacts;
					ui.widgetCommPerson->GetSelectedData(BUS_CM_CONTACT,lstSelectedContacts);

					//if none then open wizard
					if (lstSelectedContacts.getRowCount()==0)
					{
						CommunicationActions::getProjectContacts(m_nEntityRecordID,lstSelectedContacts);
					}

					//issue 1450: if project team then enable serial mode:
					m_p_CeMenu->SetSerialEmailMode();
					//regardless of result, set defaults
					m_p_CeMenu->SetDefaults(&lstSelectedContacts,&lstProject);
					return;
				}
				
			//}
/*
		}
		break;
	case 1:
		if (m_nEntityRecordID>0)
		{
			m_p_CeMenu->SetDefaults(NULL,&lstProject);
			return;
		}
		break;
	}

	*/
	m_p_CeMenu->SetDefaults(NULL,&lstProject);
}

void FUI_Projects::OnCEMenu_NewDataDropped(int nEntityID,DbRecordSet lstData)
{
	if (!g_objFuiManager.IsActiveWindowCurrentFUI(this) && m_bSideBarModeView) 
		return;

	switch(nEntityID)
	{

	case ENTITY_COMM_GRID_DATA: //if grid data dropped
		{
			if (m_nEntityRecordID>0)
			{
				if(g_CommManager.DropCommDataToProject(lstData,m_nEntityRecordID))
				{
					ui.contactGrid->ReloadData();
				}
			}
		}
		break;
	case ENTITY_BUS_CONTACT:	//if contact->nmrx
		{
			QList<int> lstID;
			int nSize=lstData.getRowCount();
			for(int i=0;i<nSize;++i)
			{
				lstID<<lstData.getDataRef(i,"BCNT_ID").toInt();
			}
			ui.widgetCommPerson->AssignData(BUS_CM_CONTACT,lstID);
			ui.tabWidgetData->setCurrentIndex(0);
			ui.tabWidgetComm->setCurrentIndex(1);
			//notify observers: issue 1479
			DbRecordSet rec=m_lstData;
			prepareCacheRecord(&rec);
			QVariant varData;
			qVariantSetValue(varData,rec);
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED,ENTITY_BUS_PROJECT,varData,this); 

		}
		break;
	case ENTITY_BUS_PERSON:	//if contact->nmrx
		{
			QList<int> lstID;
			int nSize=lstData.getRowCount();
			for(int i=0;i<nSize;++i)
			{
				lstID<<lstData.getDataRef(i,"BPER_ID").toInt();
			}
			ui.widgetCommPerson->AssignData(BUS_PERSON,lstID);
			ui.tabWidgetData->setCurrentIndex(0);
			ui.tabWidgetComm->setCurrentIndex(1);
			//notify observers: issue 1479
			DbRecordSet rec=m_lstData;
			prepareCacheRecord(&rec);
			QVariant varData;
			qVariantSetValue(varData,rec);
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED,ENTITY_BUS_PROJECT,varData,this); 
		}
		break;
	}
}

void FUI_Projects::changeEvent ( QEvent * event)
{
	if(event->type() == QEvent::WindowStateChange)
	{
		//now check to see if the window is being minimised
        if( isMinimized() )
		{
			if(NULL == m_pWndAvatar)
			{
				//calculate position for the new window
				QPoint pt = geometry().topRight();
				pt.setX(pt.x() - 169);

				m_pWndAvatar = new fui_avatar(AVATAR_PROJECT, this);
				m_pWndAvatar->move(pt);
			}

			QString strTxt = ui.txtCode->text();
			strTxt +=  " ";
			strTxt +=  ui.txtName->text();
			m_pWndAvatar->SetLabel(strTxt);
			m_pWndAvatar->SetRecord(m_nEntityRecordID);

			m_pWndAvatar->show();

			if (m_bSideBarModeView)
				g_objFuiManager.ClearActiveFUI(this);

			if(m_bHideOnMinimize)
				QTimer::singleShot(0,this,SLOT(hide()));//B.T. delay hide
			m_bHideOnMinimize = true; //reset
		}
	}
}

void FUI_Projects::closeEvent(QCloseEvent *event)
{
	FuiBase::closeEvent(event);

	if(m_pWndAvatar){
		m_pWndAvatar->close();
		m_pWndAvatar = NULL;
	}
}


//issue: 1153, selection controller is destroyed before FUI, disable it:
void FUI_Projects::On_SelectionControllerSideBarDestroyed()
{
	m_p_Selector=NULL;
	m_pSelectionController=NULL;
}

void FUI_Projects::OnSetFindFocusDelayed()
{
	ui.frameSelection->SetFocusOnFind();
}

void FUI_Projects::DataDefine()
{
	FuiBase::DataDefine();
	m_lstCustomFields.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS_DATA));
}

void FUI_Projects::DataPrepareForInsert(Status &err)
{
	DataClear();
	m_lstData.addRow();
	m_lstData.setData(0,"BUSP_ACTIVE_FLAG",1);
	m_lstData.setData(0,"BUSP_ICON","");	

	//for custom fields, special procedure:
	_SERVER_CALL(BusCustomFields->ReadUserCustomDataForInsert(err,BUS_PROJECT,m_lstCustomFields));
}

void FUI_Projects::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	if(m_lstData.getDataRef(0,"BUSP_CODE").toString().isEmpty())
		{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Code is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"BUSP_NAME").toString().isEmpty())
		{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Name is mandatory field!"));	return;	}

	ui.widgetReservations->DataCheckBeforeWrite(err);
	ui.tableCustomFields->StoreCustomWidgetChanges(); //ensure that all changes are stored properly in the record
}

void FUI_Projects::DataClear()
{
	FuiBase::DataClear();
	ui.arSelector->Invalidate();
	ui.widgetReservations->Clear();
	m_lstCustomFields.clear();
}


void FUI_Projects::DataRead(Status &err,int nRecordID)
{
	//read batch:
	DbRecordSet lstNmrx1;
	DbRecordSet lstNmrx1Filter=ui.widgetCommPerson->GetFilterForRead(nRecordID);
	DbRecordSet lstUAR,lstGAR;

	_SERVER_CALL(BusProject->ReadData(err,nRecordID,m_lstData,lstNmrx1Filter,lstNmrx1,lstUAR,lstGAR,m_lstCustomFields));
	if(err.IsOK())
	{
		ui.widgetReservations->Reload(err,nRecordID);
		if (!err.IsOK())
			return;

		m_lstDataCache=m_lstData;
		QString strName=MainEntityCalculatedName::GetCalculatedName(BUS_PROJECT,m_lstData);
		ui.widgetCommPerson->Reload(strName,nRecordID,true,&lstNmrx1);
		ui.arSelector->Load(nRecordID,BUS_PROJECT,&lstUAR,&lstGAR);
		ui.arSelector->SetRecordDisplayName(strName);
	}
}

void FUI_Projects::DataWrite(Status &err)
{
	DbRecordSet lstUAR,lstGAR;
	ui.arSelector->Save(m_nEntityRecordID,BUS_PROJECT,&lstUAR,&lstGAR); //get ar rows for save

	_SERVER_CALL(BusProject->WriteData(err,m_lstData,m_strLockedRes,lstUAR,lstGAR,m_lstCustomFields));
	if (err.IsOK())
	{
		m_strLockedRes="";
		m_lstDataCache=m_lstData;
		//re-init UAR selector (if after insert)
		m_nEntityRecordID=m_lstData.getDataRef(0,m_TableData.m_strPrimaryKey).toInt();
		ui.widgetReservations->Write(err,m_nEntityRecordID);
		QString strName=MainEntityCalculatedName::GetCalculatedName(BUS_PROJECT,m_lstData);
		ui.arSelector->Load(m_nEntityRecordID,BUS_PROJECT,&lstUAR,&lstGAR);
		ui.arSelector->SetRecordDisplayName(strName);
	}
}

void FUI_Projects::DataPrepareForCopy(Status &err)
{
	FuiBase::DataPrepareForCopy(err);
	ui.arSelector->Invalidate();

	DbRecordSet data;
	ui.widgetReservations->GetData(data);
	data.setColValue("BCRS_ID",QVariant(QVariant::Int));
	ui.widgetReservations->Reload(err,-1,&data);
	ui.tableCustomFields->ResetRecordIDs();
}

void FUI_Projects::DataUnlock(Status &err)
{
	FuiBase::DataUnlock(err);

	ui.arSelector->CancelChanges();
	ui.arSelector->SetRecordDisplayName(m_p_Selector->GetCalculatedName(m_nEntityRecordID));
	if (m_nEntityRecordID>0)
		ui.widgetReservations->Reload(err,m_nEntityRecordID); //instead of cache just reload widget...
	else
		ui.widgetReservations->Clear();
}

void FUI_Projects::OnBtnCalendar_Click()
{
	if (m_nEntityRecordID<=0)
		return;

	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(QDate::currentDate(),from, to);

	QString strName=m_p_Selector->GetCalculatedName(m_nEntityRecordID);

	DbRecordSet lstProjects;
	lstProjects.addColumn(QVariant::Int,"BUSP_ID");
	lstProjects.addColumn(QVariant::String,"BUSP_NAME");
	lstProjects.addRow();
	lstProjects.setData(0,0,m_nEntityRecordID);
	lstProjects.setData(0,1,strName);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		//lstContacts.Dump();
		pFui->Initialize(from,to,NULL,&lstProjects,NULL);
	}
}

void FUI_Projects::OnTabWidgetData_currentChanged(int nCurrentTab)
{
	if (nCurrentTab==TAB_MAIN_COMM)
		ui.btnButtonBar->setEnabled(false);
	else
		ui.btnButtonBar->setEnabled(true);

}