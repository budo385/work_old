#ifndef DLG_GRIDVIEWS_H
#define DLG_GRIDVIEWS_H

#include <QtWidgets/QDialog>
#include <QtWidgets/QMessageBox>
#include "common/common/dbrecordset.h"
#include "generatedfiles/ui_dlg_gridviews.h"
#include "bus_core/bus_core/formataddress.h"
#include "bus_client/bus_client/selection_gridviews.h"
#include "dlg_addrschemainfo.h"



class Dlg_GridViews : public QDialog
{
	Q_OBJECT

public:
	Dlg_GridViews(QWidget *parent = 0);
	~Dlg_GridViews();

	//Modes:
	enum Mode 
	{
		MODE_READ=0,	
		MODE_EDIT=1,			
		MODE_INSERT=2,			
		MODE_EMPTY=3	
	};

	enum GridID
	{
		GRID_ACTUAL_CONTACT_GRID,
		GRID_GROUP_CONTACT_GRID
	};

	bool Initialize(QString strView, int nGridID);
	QString GetCurrentViewName();

private slots:
		void on_btnAdd_clicked();
		void on_btnRemove_clicked();
		void on_btnUp_clicked();
		void on_btnDown_clicked();

		void on_btnCancel_clicked();
		void on_btnOK_clicked();
		void on_btnNew_clicked();
		void on_btnEdit_clicked();
		void on_btnDelete_clicked();
		void OnViewChanged(int nIndex);


private:

	void RefreshCache(QString oldViewName,DbRecordSet& newData);
	void RefreshDisplay();
	void SetMode(int nMode);

	QString m_strLockedRes;
	int m_nMode;
	int m_nGridID;

	DbRecordSet m_lstColumnDataGrid;

	Ui::Dlg_GridViewsClass ui;

	Selection_GridViews m_ComboHandler;			//super handler: connected to cache

};
#endif // DLG_GRIDVIEWS_H
