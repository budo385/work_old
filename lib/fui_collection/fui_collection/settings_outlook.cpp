#include "settings_outlook.h"
#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#include "os_specific/os_specific/mapimanager.h"

#include "bus_client/bus_client/changemanager.h"
extern ChangeManager					g_ChangeManager;			//global message dispatcher
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

void FolderList2String(const QList<QStringList> &lstData, QString &strResult);
void String2FolderList(QString strData, QList<QStringList> &lstResult);


Settings_Outlook::Settings_Outlook(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
   	:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

#ifdef _WIN32
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Update Some LastScan data");

	//find/prepare Ini row (per-connection settings)
	m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstOutlookSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	if(-1 == m_nIniLstRow){
		g_pClientManager->GetIniFile()->m_lstOutlookSync.addRow();
		m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstOutlookSync.getRowCount()-1;
		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_ConnectionName", g_pClientManager->GetConnectionName());
	}

	if(g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool())	//FIX: #2588 (do not access MAPI if not used!!!!)
	{
		QStringList lstPersEmails;
		MapiManager::GetPersonalEmails(lstPersEmails, MapiManager::InitGlobalMapi());
		QString strOurEmail = (lstPersEmails.size() > 0)? lstPersEmails.at(0) : "";

		if(!strOurEmail.isEmpty())
		{
			strOurEmail  = ";" + strOurEmail;
			strOurEmail += ";";

			QString strAllowedEmails = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_AllowedProfiles").toString(); //GetSettingValue(OUTLOOK_SETTINGS_WORKSTATIONS).toString();
			if( g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool() &&
				strAllowedEmails.indexOf(strOurEmail) >= 0 ) //GetSettingValue(OUTLOOK_SETTINGS_SUBSCRIBED).toBool()
			{
				ui.chkImport->setChecked(true);

				if( g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_OnlyKnownEmails").toBool()) //GetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN).toBool()
					ui.chkOnlyKnown->setChecked(true);
				else
					ui.chkOnlyKnown->setChecked(false);

				if( g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_OnlyOutgoingEmails").toBool()) //GetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN).toBool()
					ui.chkOutgoingOnly->setChecked(true);
				else
					ui.chkOutgoingOnly->setChecked(false);
			}
			else{
				ui.chkImport->setChecked(false);
				ui.chkOnlyKnown->setChecked(false);
				ui.chkOutgoingOnly->setChecked(false);
			}
		}
		else{
			ui.chkImport->setChecked(false);
			ui.chkOnlyKnown->setEnabled(false);
			ui.chkOutgoingOnly->setEnabled(false);
		}
	}
	else{
		ui.chkImport->setChecked(false);
		ui.chkOnlyKnown->setEnabled(false);
		ui.chkOutgoingOnly->setEnabled(false);
	}

	ui.txtScanPeriod->setText(QString("%1").arg( g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt())); //GetSettingValue(OUTLOOK_SETTINGS_TIMER_MIN).toString()

	QDateTime dtLastScan = QDateTime::fromString(g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_LastScan").toString(), "dd.MM.yyyy hh:mm:ss"); //GetSettingValue(OUTLOOK_SETTINGS_LAST_SCANNED).toString()
	ui.dateLastScanned->setDateTime(dtLastScan);

	GetFolderSetting(m_lstOutlookFoldersScan);
	RefreshInfoLabel();

	m_bScanDateChanged = false;
	connect(ui.dateLastScanned, SIGNAL(dateTimeChanged(const QDateTime&)), this, SLOT(on_scannedDateTime_changed(const QDateTime &)));
#endif
}

Settings_Outlook::~Settings_Outlook()
{
}

DbRecordSet Settings_Outlook::GetChangedValuesRecordSet()
{
#ifdef _WIN32
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Update Scan info");

	//save always to prevent bugs:
	if (ui.chkImport->isChecked() != g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool()) //GetSettingValue(OUTLOOK_SETTINGS_SUBSCRIBED).toBool()
	{
		//m_recSettings.Dump();
		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_Enabled", ui.chkImport->isChecked()? true : false); //SetSettingValue(OUTLOOK_SETTINGS_SUBSCRIBED, ui.chkImport->isChecked()? true : false);
	}

	if (ui.chkOnlyKnown->isChecked() != g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_OnlyKnownEmails").toBool()) //GetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN).toBool()
	{
		//m_recSettings.Dump();
		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_OnlyKnownEmails", ui.chkOnlyKnown->isChecked()? true : false);//SetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN, ui.chkOnlyKnown->isChecked()? true : false);
	}

	if (ui.chkOutgoingOnly->isChecked() != g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_OnlyOutgoingEmails").toBool()) //GetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN).toBool()
	{
		//m_recSettings.Dump();
		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_OnlyOutgoingEmails", ui.chkOutgoingOnly->isChecked()? true : false);//SetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN, ui.chkOnlyKnown->isChecked()? true : false);
	}

	//refresh list of allowed Outlook profiles
	if(ui.chkImport->isChecked())	//FIX: #2588 (do not access MAPI if not used!!!!)
	{
		QStringList lstPersEmails;
		MapiManager::GetPersonalEmails(lstPersEmails, MapiManager::InitGlobalMapi());
		QString strOurEmail = (lstPersEmails.size() > 0)? lstPersEmails.at(0) : "";

		if(!strOurEmail.isEmpty())
		{
			QString strSearchEmail  = ";" + strOurEmail;
			strSearchEmail += ";";

			QString strAllowedEmails = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_AllowedProfiles").toString(); //GetSettingValue(OUTLOOK_SETTINGS_WORKSTATIONS).toString();
			if(strAllowedEmails.indexOf(strSearchEmail) >= 0)
			{
				//remove email from allowed if required
				if(!ui.chkImport->isChecked())
					strAllowedEmails.replace(strSearchEmail, ";");
			}
			else
			{
				//add email to allowed if required
				if(ui.chkImport->isChecked()){
					if(strAllowedEmails.isEmpty())
						strAllowedEmails += ";";
					strAllowedEmails += strOurEmail;
					strAllowedEmails += ";";
				}
			}
			g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_AllowedProfiles", strAllowedEmails); //SetSettingValue(OUTLOOK_SETTINGS_WORKSTATIONS, strAllowedEmails); 
		}
	}

	QList<QStringList> lstFolders;
	GetFolderSetting(lstFolders);
	if(m_lstOutlookFoldersScan != lstFolders){
		//QByteArray arData;
		//OutlookFolderPickerDlg::FolderList2Binary(m_lstOutlookFoldersScan, arData);
		QString strData;
		FolderList2String(m_lstOutlookFoldersScan, strData);
		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_ScanFolders", strData);//SetSettingValue(OUTLOOK_SETTINGS_SCAN_FOLDERS, strData); //arData
	}

	if(m_bScanDateChanged)
	{
		QDateTime dtLastOutlookScan = ui.dateLastScanned->dateTime();	// notify timer directly
		QString strNewDate = dtLastOutlookScan.toString("dd.MM.yyyy hh:mm:ss");
		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_LastScan", strNewDate); //SetSettingValue(OUTLOOK_SETTINGS_LAST_SCANNED, strNewDate);
	}
	
	if (ui.txtScanPeriod->text().toInt() != g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt()) //GetSettingValue(OUTLOOK_SETTINGS_TIMER_MIN).toInt()
	{
		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_TimerMin", ui.txtScanPeriod->text().toInt()); //SetSettingValue(OUTLOOK_SETTINGS_TIMER_MIN, ui.txtScanPeriod->text().toInt());

		//re-start timer with new period
		//if(g_pSettings->GetPersonSetting(OUTLOOK_SETTINGS_SUBSCRIBED).toBool())
		{
			int nPeriod = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt(); //GetSettingValue(OUTLOOK_SETTINGS_TIMER_MIN).toInt();
			if(nPeriod > 0)
			{
				nPeriod *= 60 * 1000;	// min to ms
				g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_SUBSCRIPTION_TIMER_OUTLOOK_CHANGED,nPeriod);
			}
		}
	}
	//if not checked stop timer:
	if (!ui.chkImport->isChecked()) 
	{
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_SUBSCRIPTION_TIMER_OUTLOOK_CHANGED,0);
	}

	g_pClientManager->GetIniFile()->Save(); //#2154

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Update LastScan Done");
#endif

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

void Settings_Outlook::on_btnSelectFolders_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Select Outlook folders");

	OutlookFolderPickerDlg dlg(false, false, false);
	dlg.SetSelection(m_lstOutlookFoldersScan);
	if(dlg.exec()){
		m_lstOutlookFoldersScan = dlg.GetSelection();
		RefreshInfoLabel();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Select Outlook folders Done");
}

void Settings_Outlook::GetFolderSetting(QList<QStringList> &lstFolders)
{
	//load outlook folder selection setting
	//QByteArray  arData  = GetSettingValue(OUTLOOK_SETTINGS_SCAN_FOLDERS).toByteArray();
	//OutlookFolderPickerDlg::Binary2FolderList(arData, lstFolders);
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Get Outlook folder setting");

	QString strData = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_ScanFolders").toString();
	String2FolderList(strData, lstFolders);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Get Outlook folder setting Done");
}

void Settings_Outlook::RefreshInfoLabel()
{
	//refresh info
	ui.labelFolders->setText(tr("%1 folders selected").arg(m_lstOutlookFoldersScan.count()));
}

void Settings_Outlook::on_scannedDateTime_changed(const QDateTime &dateTime)
{
	m_bScanDateChanged = true;
}

void Settings_Outlook::on_chkImport_clicked(bool bChecked)
{
	//MB request: first time usage - set current date instead of standard "1.1.2000 00:00:00"
	if(bChecked){
		if(ui.dateLastScanned->dateTime() == QDateTime(QDate(2000,1,1), QTime(0,0,0))){
			ui.dateLastScanned->setDateTime(QDateTime::currentDateTime());
			m_bScanDateChanged = true;
		}
		ui.chkOnlyKnown->setEnabled(true);
		ui.chkOutgoingOnly->setEnabled(true);
	}
}

void FolderList2String(const QList<QStringList> &lstData, QString &strResult)
{
	strResult = "";
	int nSize = lstData.size();
	for(int i=0; i<nSize; i++)
	{
		const QStringList &lstItem = lstData[i];
		QString strItem = "\"";
		int nSegments = lstItem.size();
		for(int j=0; j<nSegments; j++)
		{
			strItem += lstItem[j];
			if(j < (nSegments-1))
				strItem += "/";
		}
		strItem += "\"";
		//if(i < (nSize-1))
			strItem += ";";
		strResult += strItem;
	}
}

void String2FolderList(QString strData, QList<QStringList> &lstResult)
{
	lstResult.clear();
	int nPos = -1;
	while((nPos = strData.indexOf(";")) >= 0)
	{
		QString strItem = strData.left(nPos);
		strData = strData.mid(nPos+1);

		//strip quotes
		strItem = strItem.mid(1, strItem.size()-2);

		//now process the item
		QStringList lstSegments;
		while((nPos = strItem.indexOf("/")) >= 0)
		{
			QString strSegment = strItem.left(nPos);
			strItem = strItem.mid(nPos+1);
			lstSegments << strSegment;
		}
		if(!strItem.isEmpty())
			lstSegments << strItem;	// last segment
		lstResult << lstSegments;
	}
}

void Settings_Outlook::on_btnResetFolders_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Reset Outlook folders");

	m_lstOutlookFoldersScan.clear();
	RefreshInfoLabel();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Setings Window: Reset Outlook folders Done");
}
