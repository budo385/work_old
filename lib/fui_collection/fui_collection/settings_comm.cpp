#include "settings_comm.h"
#include "os_specific/os_specific/tapi/TapiLine.h"

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

Settings_Comm::Settings_Comm(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
	:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	m_bIsOptionsPage = bOptionsSwitch;

	ui.setupUi(this);

	if(!bOptionsSwitch){
		ui.chkListenSkype->setChecked(GetSettingValue(COMM_SETTINGS_LISTEN_SKYPE).toBool());
	}
	else{
		ui.chkListenSkype->hide();	// only in "Personal Settings" page
		ui.cboTAPI->hide();		// only in "Personal Settings" page
		ui.txtDialPfx->hide();	// only in "Personal Settings" page
		ui.chkDialPfx->hide();	// only in "Personal Settings" page
		ui.chkDefaultIncoming->hide();	// only in "Personal Settings" page
		ui.label_3->hide();	
	}

	//set Group SAPNE:
	ui.frameGroup->Initialize(ENTITY_BUS_GROUP_ITEMS);
	ui.frameGroup->GetButton(Selection_SAPNE_FUI::BUTTON_REMOVE)->setVisible(false);
	ui.frameGroup->GetButton(Selection_SAPNE_FUI::BUTTON_ADD)->setVisible(false);
	ui.frameGroup->GetButton(Selection_SAPNE_FUI::BUTTON_MODIFY)->setVisible(false);

	int nGroupID = GetGroupSetting();
	ui.frameGroup->SetCurrentEntityRecord(nGroupID);

	if(!m_bIsOptionsPage)
	{
		if(GetSettingValue(COMM_SETTINGS_DEFAULT_SEL_TYPE).toInt() == 1)
			ui.radTAPI->setChecked(true);
		else
			ui.radSkype->setChecked(true);

#ifdef _WIN32
		//fill the combo box with available TAPI devices
		CTapiLine lineTAPI;
		int nCnt = lineTAPI.GetDeviceCount();
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("TAPI Settings page: detected total of %1 TAPI devices").arg(nCnt));
		for(int i=0; i<nCnt; i++)
		{
			QString strDeviceName;
			bool bVoiceCalls = false;
			if(lineTAPI.GetDeviceInfo(i, strDeviceName, bVoiceCalls)){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("TAPI Settings page: got device info, name [%1], voice=%2").arg(strDeviceName).arg(bVoiceCalls));
				if(bVoiceCalls){
					ui.cboTAPI->addItem(strDeviceName);
				}
			}
		}
		//now try to selected our device
		QString strSelDevice = GetSettingValue(COMM_SETTINGS_DEFAULT_TAPI_DEV).toString();
		if(!strSelDevice.isEmpty()){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("TAPI Settings page: try to select default device, name [%1]").arg(strSelDevice));
			int nIdx = ui.cboTAPI->findText(strSelDevice);
			if(nIdx >= 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("TAPI Settings page: device found at combo index: %1").arg(nIdx));
				ui.cboTAPI->setCurrentIndex(nIdx);
			}
			else{
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("TAPI Settings page: failed to find default device"));
			}
		}

		if(GetSettingValue(COMM_SETTINGS_USE_DIAL_PREFIX).toBool())
			ui.chkDialPfx->setChecked(true);
		on_chkDialPfx_clicked();

		ui.txtDialPfx->setText(GetSettingValue(COMM_SETTINGS_DIAL_PREFIX_STR).toString());
#endif

		if(GetSettingValue(COMM_SETTINGS_DEFAULT_CALL_INCOMING).toInt() == 1)
			ui.chkDefaultIncoming->setChecked(true);
	}
}

Settings_Comm::~Settings_Comm()
{
}

DbRecordSet Settings_Comm::GetChangedValuesRecordSet()
{
	//save:
	if(!m_bIsOptionsPage){
		if (ui.chkListenSkype->isChecked() != GetSettingValue(COMM_SETTINGS_LISTEN_SKYPE).toBool())
			SetSettingValue(COMM_SETTINGS_LISTEN_SKYPE, ui.chkListenSkype->isChecked()? true : false);
	}

	int nGroupID = -1;
	ui.frameGroup->GetCurrentEntityRecord(nGroupID);
	if(GetGroupSetting() != nGroupID)
		SetGroupSetting(nGroupID);

	if(!m_bIsOptionsPage)
	{
		int nSelType = (ui.radSkype->isChecked())? 0 : 1;
		if(GetSettingValue(COMM_SETTINGS_DEFAULT_SEL_TYPE).toInt() != nSelType)
			SetSettingValue(COMM_SETTINGS_DEFAULT_SEL_TYPE, nSelType);

		if(GetSettingValue(COMM_SETTINGS_DEFAULT_TAPI_DEV).toString() != ui.cboTAPI->currentText())
			SetSettingValue(COMM_SETTINGS_DEFAULT_TAPI_DEV, ui.cboTAPI->currentText());

		bool bUsePfx = (ui.chkDialPfx->isChecked())? true : false;
		if(GetSettingValue(COMM_SETTINGS_USE_DIAL_PREFIX).toBool() != bUsePfx)
			SetSettingValue(COMM_SETTINGS_USE_DIAL_PREFIX, bUsePfx);

		if(GetSettingValue(COMM_SETTINGS_DIAL_PREFIX_STR).toString() != ui.txtDialPfx->text())
			SetSettingValue(COMM_SETTINGS_DIAL_PREFIX_STR, ui.txtDialPfx->text());

		int nCallIncloming = (ui.chkDefaultIncoming->isChecked())? 1 : 0;
		if(GetSettingValue(COMM_SETTINGS_DEFAULT_CALL_INCOMING).toInt() != nCallIncloming)
			SetSettingValue(COMM_SETTINGS_DEFAULT_CALL_INCOMING, nCallIncloming);
	}

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

int Settings_Comm::GetGroupSetting()
{
	if(!m_bIsOptionsPage)
		return GetSettingValue(COMM_SETTINGS_TRANSFER_GROUP).toInt();
	else
		return GetSettingValue(COMM_OPTIONS_TRANSFER_GROUP).toInt();
}

void Settings_Comm::SetGroupSetting(int nGroupID)
{
	if(!m_bIsOptionsPage)
		SetSettingValue(COMM_SETTINGS_TRANSFER_GROUP, nGroupID);
	else
		SetSettingValue(COMM_OPTIONS_TRANSFER_GROUP, nGroupID);
}

void Settings_Comm::on_chkDialPfx_clicked()
{
	if(ui.chkDialPfx->isChecked())
		ui.txtDialPfx->setEnabled(true);
	else
		ui.txtDialPfx->setEnabled(false);
}
