#ifndef LANGUAGEWARNINGDLG_H
#define LANGUAGEWARNINGDLG_H

#include <QDialog>
#include "ui_languagewarningdlg.h"

class LanguageWarningDlg : public QDialog
{
	Q_OBJECT

public:
	LanguageWarningDlg(QString strUL, QString strDL, QWidget *parent = 0);
	~LanguageWarningDlg();

private:
	Ui::LanguageWarningDlgClass ui;
};

#endif // LANGUAGEWARNINGDLG_H
