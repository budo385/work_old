#ifndef IMPORTCENTER_H
#define IMPORTCENTER_H

#include <QWidget>
#include "generatedfiles/ui_importcenter.h"

class ImportCenter : public QDialog
{
	Q_OBJECT

public:
	ImportCenter(QWidget *parent = 0);
	~ImportCenter();

private slots:
	void on_btnImportSykpe_Contact_clicked();
	void on_btnImportOutlook_Contact_clicked();
	void on_btnImportOutlook_Email_clicked();
	void on_btnImportTxt_Contact_clicked();
	void on_btnImportTxt_Project_clicked();
	void on_btnFinish_clicked();
	void Activate();
private:
	Ui::ImportCenterClass ui;
};

#endif // IMPORTCENTER_H
