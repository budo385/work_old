#include "qcw_leftselector.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include <QTextDocumentFragment>
#include <QHeaderView>
#include "db_core/db_core/dbsqltableview.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "common/common/datahelper.h"
#include <QUrl>
#include <QMimeData>
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/thememanager.h"

#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager



/*
//global FUI manager:
#include "fuimanager.h"
extern FuiManager g_objFuiManager;


 //defines FP codes
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester


*/


QCW_LeftSelector::QCW_LeftSelector(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);
	setAcceptDrops(true);

	//this->setAttribute(Qt::WA_DeleteOnClose, true);
	ui.textEdit->setAcceptDrops(true);


	ui.labelPic->setToolTip(tr("Drop items here to assign them to a contact or project: Files, Other Contacts, Emails, Projects, vCards,..."));
	ui.textEdit->setToolTip(tr("Paste text here to assign it to the actual contact or project: Phone Numbers, Addresses, vCards, etc,"));

	//connect(&g_ThemeManager,SIGNAL(ThemeChanged()),this,SLOT(OnThemeChanged()));


	ui.labelPic->setPixmap(QPixmap(ThemeManager::GetCEDropZonePicture()));

	ui.btnInternet->setIcon(QIcon(":Earth16.png"));
	ui.btnAddress->setIcon(QIcon(":Adress16.png"));
	ui.btnEmail->setIcon(QIcon(":Email16.png"));
	ui.btnPhone->setIcon(QIcon(":Phone16.png"));
	ui.btnContact->setIcon(QIcon(":Icon_Person24.png"));
	ui.btnHouse->setIcon(QIcon(":Organization16.png"));
	ui.btnFormData->setIcon(QIcon(":RunAnalyze.png"));

	ui.btnParse->setIconSize(QSize(48,32));
	ui.btnParse->setIcon(QIcon(":RunAnalyze.png"));

	ui.btnInternet->setToolTip(tr("Add Selected Text As Internet Address to the Contact"));
	ui.btnAddress->setToolTip(tr("Add Selected Text As Address to the Contact"));
	ui.btnEmail->setToolTip(tr("Add Selected Text As Email Address to the Contact"));
	ui.btnPhone->setToolTip(tr("Add Selected Text As Phone Number to the Contact"));
	ui.btnContact->setToolTip(tr("Add Selected Text As Contact Name"));
	ui.btnHouse->setToolTip(tr("Add Selected Text As Contact Organization"));
	ui.btnFormData->setToolTip(tr("Analyze the selected text as form data"));
	


	//QString	strFrameLayout="QFrame#frameBkg { border-width: 4px; color: white; border-image:url(:DropZone_BG.png) 4px 4px 4px 4px stretch stretch }";
	ui.frameBkg->setStyleSheet(ThemeManager::GetCEDropZoneBkgStyle());

	//setup Table widget:

	//---------------------------------------- INIT CNTX MENU TABLE--------------------------------//
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;



	pAction = new QAction(tr("Clear List"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteTable()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Remove Selected Contacts From List"), this);
	pAction->setShortcut(tr("Del"));
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteSelection()));
	lstActions.append(pAction);
	ui.tableWidget->addAction(pAction);

	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	//Select All:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), ui.tableWidget, SLOT(SortDialog()));
	lstActions.append(pAction);


	//---------------------------------------- INIT CNTX MENU TABLE--------------------------------//

	m_lstContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	DbRecordSet columns;
	UniversalTableWidget::AddColumnToSetup(columns,"","",35,false); //issue 1634: dummy datasource: all handled manually add numbers inside grid
	UniversalTableWidget::AddColumnToSetup(columns,"BCNT_NAME",QObject::tr("Contact"),200,false);
	ui.tableWidget->SetContextMenuActions(lstActions);
	ui.tableWidget->Initialize(&m_lstContacts,&columns); 
	ui.tableWidget->SetEditMode(true); //this is ok
	ui.tableWidget->EnableDrag(true,ENTITY_BUS_CONTACT);
	//ui.tableWidget->InitContactGrid(this); //custom init (for drop handlers)
	//set last column streched:
	ui.tableWidget->horizontalHeader()->setStretchLastSection(true);
	ui.tableWidget->verticalHeader()->setDefaultSectionSize(15);
	ui.tableWidget->setStyleSheet("QTableWidgetItem {font-size:8pt}");

	//Find thing.
	ui.Find_widget->Initialize(ui.tableWidget);

	connect(ui.tableWidget,SIGNAL(SignalSelectionChanged()),this,SLOT(SlotSelectionChanged()));  //MB requested key navigation, will trigger event twice, but...


	//ui.frameBkgBig->setStyleSheet(QString("QFrame#frameBkgBig { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }").arg(":Theme_Blue_Temple_CEDropZoneBkg.png"));


	//set yellow frames + red labels:

	QString strFontRed=QString("<html><body style=\" font-family:Arial; font-size:18pt; font-weight:800; font-style:normal; text-decoration:none;\">\
								<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:18pt; font-weight:800; font-style:normal; color:%2;\">%1</span></p>\
								</body></html>");
	
	ui.labelStep1->setText(strFontRed.arg("1.").arg("red"));
	ui.labelStep2->setText(strFontRed.arg("2.").arg("red"));


	ui.frameStep1->setStyleSheet("QFrame {background-color: rgb(255,255,204)}");
	ui.frameStep2->setStyleSheet("QFrame {background-color: rgb(255,255,204)}");

	//Hide buttons. Issue #1451.
	ui.frameButton->setVisible(false);

	g_ChangeManager.registerObserver(this);
}

QCW_LeftSelector::~QCW_LeftSelector()
{

}



//when text is changed
void QCW_LeftSelector::OnTextChanged()
{
	///QString strTextDrop=ui.textEdit->toPlainText();
	//ProcessTextDrop(strTextDrop); //process only if it was empty before...
}




void QCW_LeftSelector::ProcessAddress(QString strTextDrop)
{
	DbRecordSet lstContact;
	m_Manager.Parse_Text(strTextDrop,lstContact);
	DbRecordSet lstData=lstContact.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();

	//issue 1348:
	if (lstData.getRowCount()==0)
	{
		lstData.addRow();
		lstData.setData(0,"BCMA_FORMATEDADDRESS",strTextDrop);

		DbRecordSet lstAddressTypes;
		lstAddressTypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT));
		lstData.setData(0,"LST_TYPES",lstAddressTypes);
		_DUMP(lstData);
	}

	emit NewDataDropped(ENTITY_BUS_CM_ADDRESS,lstData);
}

void QCW_LeftSelector::ProcessEMail(QString strTextDrop)
{
	DbRecordSet lstContact;
	m_Manager.Parse_Text(strTextDrop,lstContact);
	DbRecordSet lstData=lstContact.getDataRef(0,"LST_EMAIL").value<DbRecordSet>();
	emit NewDataDropped(ENTITY_BUS_CM_EMAIL,lstData);

}
void QCW_LeftSelector::ProcessPhone(QString strTextDrop)
{
	DbRecordSet lstContact;
	m_Manager.Parse_Text(strTextDrop,lstContact);
	DbRecordSet lstData=lstContact.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
	emit NewDataDropped(ENTITY_BUS_CM_PHONE,lstData);


}
void QCW_LeftSelector::ProcessInternet(QString strTextDrop)
{
	DbRecordSet lstContact;
	m_Manager.Parse_Text(strTextDrop,lstContact);
	DbRecordSet lstData=lstContact.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
	emit NewDataDropped(ENTITY_BUS_CM_INTERNET,lstData);

}


void QCW_LeftSelector::on_btnContact_clicked()
{
	//? first/last
	DbRecordSet lstContact;
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	m_Manager.Parse_Text(strSelected,lstContact);
	DbRecordSet lstData=lstContact.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();

	if (lstData.getRowCount()>0)
	{
		emit SetNames(lstData.getDataRef(0,"BCMA_FIRSTNAME").toString(),lstData.getDataRef(0,"BCMA_LASTNAME").toString(),lstData.getDataRef(0,"BCMA_MIDDLENAME").toString());
	}

}
void QCW_LeftSelector::on_btnHouse_clicked()
{
	//? organiz
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	emit SetOrganization(strSelected);
}
void QCW_LeftSelector::on_btnInternet_clicked()
{
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	ProcessInternet(strSelected);
}

void QCW_LeftSelector::on_btnAddress_clicked()
{
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	ProcessAddress(strSelected);
}

void QCW_LeftSelector::on_btnEmail_clicked()
{
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	ProcessEMail(strSelected);


}
void QCW_LeftSelector::on_btnPhone_clicked()
{
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	ProcessPhone(strSelected);
}


void QCW_LeftSelector::OnThemeChanged()
{
	ui.labelPic->setPixmap(QPixmap(ThemeManager::GetCEDropZonePicture()));
	ui.frameBkg->setStyleSheet(ThemeManager::GetCEDropZoneBkgStyle());
}


//user selected cell with dbl click:
void QCW_LeftSelector::SlotSelectionChanged()
{
	int nRow = m_lstContacts.getSelectedRow();
	emit SelectionChanged(nRow, m_lstContacts.getSelectedRecordSet());
}

//refresh content in table widget:
void QCW_LeftSelector::RefreshDisplay()
{
	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,m_lstContacts);
	filler.FillCalculatedNames(m_lstContacts,m_lstContacts.getColumnIdx("BCNT_NAME"));

	ui.tableWidget->SortList();
	SlotSelectionChanged();
}


//do d'action & fire cache event
void QCW_LeftSelector::OnDeleteSelection()
{
	ui.tableWidget->DeleteSelection();
}


//do d'action & fire cache event
void QCW_LeftSelector::OnDeleteTable()
{
	ui.tableWidget->DeleteTable();
}



void QCW_LeftSelector::on_btnParse_clicked()
{
	DbRecordSet lstContact;
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	if (strSelected.isEmpty())
		strSelected=ui.textEdit->toPlainText();

	m_Manager.Parse_Text(strSelected,lstContact);
	emit NewDataDropped(ENTITY_BUS_CONTACT,lstContact);

}
void QCW_LeftSelector::on_btnRefresh_clicked()
{

}
void QCW_LeftSelector::on_btnExport_clicked()
{

}


QString QCW_LeftSelector::GetSelectedText()
{
	return ui.textEdit->textCursor().selection().toPlainText();
}



void QCW_LeftSelector::ShowContactGrid(bool bShow)
{
	ui.splitter->widget(1)->setVisible(bShow);
	
	//ui.frameButton->setVisible(bShow);
	/*
	//ui.tableWidget->setVisible(bShow);
	//ui.Find_widget->setVisible(bShow);
	ui.btnRefresh->setVisible(bShow);
	ui.btnExport->setVisible(bShow);
	*/
	
}




void QCW_LeftSelector::dragEnterEvent ( QDragEnterEvent * event )
{
	//if (event->mimeData()->hasUrls())
	event->accept();

}

void QCW_LeftSelector::dragMoveEvent(QDragMoveEvent *event )
{
	event->acceptProposedAction();
}



//issue 1276
//if TAB= template then template, else doc, if not exe...brb....
void QCW_LeftSelector::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();
	this->activateWindow(); //bring on top_:

	DbRecordSet lstContact;
	//------------------------------------------------
	//DOCS && Vcard ->import window or doc popup
	//------------------------------------------------
	if (mime->hasUrls())
	{
		QList<QUrl> lst =mime->urls();
		DbRecordSet lstPaths;
		DbRecordSet lstApps;
		DataHelper::ParseFilesFromURLs(lst,lstPaths,lstApps);
		if (lstPaths.getRowCount()==0 && lstApps.getRowCount()==0)
			return;

		//if HTTP drop then process
		int nSelCnt=lstPaths.find(2,QString("HTTP"));
		if (nSelCnt>0)
		{
			//A: find vcards: vcf
			int nSize=lstPaths.getRowCount();
			for(int i=0;i<nSize;++i)
			{
				if (lstPaths.getDataRef(i,1).toString().right(4)==".vcf")
				{
					//process:
					m_Manager.CreateFullContactFromvCardData(lst.at(0).toString(),true,lstContact);
					if (lstContact.getRowCount()>0)
						emit NewDataDropped(ENTITY_BUS_CONTACT,lstContact);
					return;
				}
			}
		}

		//sort out vcf files:
		DbRecordSet lstVCard;
		lstVCard.copyDefinition(lstPaths);
		lstPaths.find(2,QString("vcf"));
		lstVCard.merge(lstPaths,true);
		lstPaths.deleteSelectedRows(); //remove those from drop->go to vcf drop
		if (lstVCard.getRowCount()>0)
		{
			m_Manager.CreateFullContactFromvCardFile(lstVCard,lstContact);
			if (lstContact.getRowCount()>0)
				emit NewDataDropped(ENTITY_BUS_CONTACT,lstContact);
			return;
		}
	}

	//------------------------------------------------
	// Text drop (last)
	//------------------------------------------------
	if (mime->hasFormat("text/plain"))
	{
		ui.textEdit->blockSignals(true);
		ui.textEdit->setPlainText(mime->text());
		ui.textEdit->blockSignals(false);
		event->accept();
		QString strTextDrop=mime->text();
		//VCARD:
		if (strTextDrop.indexOf("BEGIN:VCARD")!=-1)
		{
			//process:
			m_Manager.CreateFullContactFromvCardData(strTextDrop,false,lstContact);
			if (lstContact.getRowCount()>0)
				emit NewDataDropped(ENTITY_BUS_CONTACT,lstContact);
		}
		//return;
	}


	//default process:
	QWidget::dropEvent(event);
}

//set text and parse
void QCW_LeftSelector::SetDropText(QString strText,bool bskipParse)
{
	ui.textEdit->setText(strText);
	if (!bskipParse)
		on_btnParse_clicked();
}

void QCW_LeftSelector::on_btnFormData_clicked()
{
	DbRecordSet lstContact;
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	if (strSelected.isEmpty())
		strSelected=ui.textEdit->toPlainText();

	m_Manager.Parse_TextForm(strSelected,lstContact);
	emit NewDataDropped(ENTITY_BUS_CONTACT,lstContact);


}

void QCW_LeftSelector::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}
}