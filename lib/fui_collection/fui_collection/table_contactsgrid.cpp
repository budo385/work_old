#include "table_contactsgrid.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "dlg_gridviews.h"
#include "gui_core/gui_core/gui_helper.h"
#include <QProgressDialog>
#include "bus_core/bus_core/mainentityfilter.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "fuibase.h"

#include "common/common/datahelper.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;				//global cache
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager	*g_pBoSet;		

#include "common/common/logger.h"
extern Logger g_Logger;

//GLOBALS:
#include "fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:


Table_ContactsGrid::Table_ContactsGrid(QWidget *parent)
:UniversalTableWidgetEx(parent)
{

}

Table_ContactsGrid::~Table_ContactsGrid()
{

}

//called after dark
void Table_ContactsGrid::Initialize(QWidget *pContactFUI,QComboBox *pComboViews,QPushButton *btnEditView,Table_SubContactData *pTableSub,QSplitter *splitterSub,QPushButton *btnRefresh)
{

	m_pContactFUI=pContactFUI;
	Q_ASSERT(m_pContactFUI);

	m_btnRefresh=btnRefresh;


	//set date:
	m_datActualContactListBuild=QDateTime::currentDateTime();
	m_datLastBuild=QDateTime::fromString("1944-10-11 00-00-00","yyyy-MM-dd HH-mm-ss");
	m_bVisible=false;

	m_nGridID=Dlg_GridViews::GRID_ACTUAL_CONTACT_GRID;;

	//init data source:
	//MainEntitySelectionController::Initialize(ENTITY_BUS_CONTACT);	//init data source
	m_lstSubListData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	m_lstData.copyDefinition(m_lstSubListData);
	m_lstSubListData.addRow();


	//set data source for table, enable drop
	UniversalTableWidgetEx::Initialize(&m_lstData);
	EnableDrag(true,ENTITY_BUS_CONTACT,":Contacts32.png");				//drop type is full contact list

	//controls:
	m_pComboViews=pComboViews;
	m_btnEditView=btnEditView;
	m_pTableSub=pTableSub;
	m_splitterSub=splitterSub;

	m_pTableSub->Initialize(&m_lstSubListData,NULL,true,true,100);
	m_pTableSub->verticalHeader()->hide();

	m_btnEditView->setIcon(QIcon(":SAP_Select.png"));
	m_btnEditView->setToolTip(tr("Modify View"));

	//redefine table look, load views:
	//COMBO:
	m_ComboHandler.Initialize(ENTITY_BUS_OPT_GRID_VIEWS);
	m_ComboHandler.SetDataForCombo(m_pComboViews,"BOGW_VIEW_NAME");
	m_ComboHandler.SetDistinctDisplay();
	m_ComboHandler.GetLocalFilter()->SetFilter("BOGW_GRID_ID",m_nGridID);
	bool bOK=m_ComboHandler.ReloadData();							
	//if(!bOK) return false;		

	//connect signals:
	connect(m_pComboViews,SIGNAL(currentIndexChanged(int)),this,SLOT(on_ViewChanged(int)));
	connect(m_btnEditView,SIGNAL(clicked()),this,SLOT(OpenViewEditor()));
	connect(btnRefresh,SIGNAL(clicked()),this,SLOT(OnRefreshContent()));
	connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnShowDetails()));
	connect(this,SIGNAL(SignalSelectionChanged()),this,SLOT(OnSelectionChanged()));


	//---------------------------------------- INIT CNTX MENU TABLE--------------------------------//
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;

	pAction = new QAction(tr("Show Contact Details"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDetails()));
	lstActions.append(pAction);


	pAction = new QAction(tr("Show Contact Details In New Window"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDetailsNewWindow()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete Selected Contacts From Database"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDelete()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("Clear List"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteTable()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Remove Selected Contacts From List"), this);
	pAction->setShortcut(tr("Del"));
	connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteSelection()));
	lstActions.append(pAction);
	this->addAction(pAction);


	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	//Select All:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), this, SLOT(OpenSortDialog()));

	lstActions.append(pAction);
	this->addAction(pAction);
	
	SetContextMenuActions(lstActions);


	//---------------------------LOAD VIEW FROM SETTINGS---------------------------
	QString nGridView=g_pSettings->GetPersonSetting(CONTACT_DEF_ACTUAL_LIST_VIEW).toString();
	if (nGridView.isEmpty())
		nGridView=g_pSettings->GetApplicationOption(APP_CONTACT_DEF_ACTUAL_LIST_VIEW).toString();

	m_pComboViews->blockSignals(true);
	m_ComboHandler.SetCurrentIndexFromName(nGridView);
	on_ViewChanged(m_pComboViews->currentIndex());
	m_pComboViews->blockSignals(false);
}

//table changed state test it
void Table_ContactsGrid::OnSelectionChanged()
{
	//refresh entry if one row is selected
	if(m_plstData->getSelectedCount()==1)
	{
		int nCurrentRow=m_plstData->getSelectedRow();
		DbRecordSet row = m_plstData->getRow(nCurrentRow);
		m_lstSubListData.assignRow(0,row);
	}
	else
	{
		m_lstSubListData.clear();
		m_lstSubListData.addRow();
	}

	m_pTableSub->RefreshDisplay();
}

void Table_ContactsGrid::OpenViewEditor()
{

	QString strViewName=m_pComboViews->currentText();
	Dlg_GridViews dlg;
	dlg.Initialize(strViewName,m_nGridID);

	if(0 != dlg.exec())
	{
		strViewName=dlg.GetCurrentViewName();
		//set to new view, block signals, coz we have manual reload
		m_pComboViews->blockSignals(true);
		m_ComboHandler.SetCurrentIndexFromName(strViewName);
		m_pComboViews->blockSignals(false);
	}
	//whatever always reload
	on_ViewChanged(m_pComboViews->currentIndex());

}

//when user changes view, destroy grid, rebuild, if -1, use default view from contactmanager
void Table_ContactsGrid::on_ViewChanged(int nIndex)
{
	DbRecordSet lstSetup;
	DbRecordSet lstSetupSub;
	lstSetup.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_GRID_COLUMN_SETUP));
	lstSetupSub.copyDefinition(lstSetup);

	if (nIndex>=0)
	{
		//extract all setup cols from combo:
		QString strViewName=m_pComboViews->currentText();
		DbRecordSet *pData=m_ComboHandler.GetDataSource();
		//pData->Dump();
		pData->find(pData->getColumnIdx("BOGW_VIEW_NAME"),strViewName);
		lstSetup.merge(*pData,true);
	}
	else
	{
		ClientContactManager::GetDefaultContactGridSetup(lstSetup);
	}

	
	//rebuild d' grid:
	
	//_DUMP(lstSetup)

	int nColNameIdx=lstSetup.getColumnIdx("BOGW_COLUMN_NAME");
	int nColPos=lstSetup.getColumnIdx("BOGW_COLUMN_POSITION");
	lstSetup.sort(nColPos);	//sort by pos: bcnt's will go first LST_ last...
	int nSize=lstSetup.getRowCount();
	//int nSubListStart=nSize;
	lstSetup.clearSelection();
	
	for (int i=0;i<nSize;++i)
	{
		if(lstSetup.getDataRef(i,nColNameIdx).toString().indexOf("LST_")==0)
		{
			lstSetupSub.addRow();
			DbRecordSet row = lstSetup.getRow(i);
			lstSetupSub.assignRow(lstSetupSub.getRowCount()-1,row);
			lstSetup.selectRow(i);
		}
	}

	

	lstSetup.deleteSelectedRows();

	//mark all cols nonedit:
	lstSetup.setColValue(lstSetup.getColumnIdx("BOGW_EDITABLE"),false);


	//if no sublists, hide down table, set splitter:
	if (lstSetupSub.getRowCount()==0)
		m_pTableSub->setVisible(false);
	else
		m_pTableSub->setVisible(true);

	//reassign lists, hide if needed
	SetColumnSetup(lstSetup);
	m_pTableSub->SetColumnSetup(lstSetupSub);


	//sort:
	SortMultiColumn(lstSetup);

}

//When left short list is rebuilt, send date to this one (content will be rebuilt if needed)
void Table_ContactsGrid::OnActualListRebuilt()
{
	m_datActualContactListBuild=QDateTime::currentDateTime();
	ReloadIfNeed();
}

//When tab is shown, call this (content will be rebuilt if needed)
void Table_ContactsGrid::SetFlagVisibile(bool bVisible)
{
	m_bVisible=bVisible;
	ReloadIfNeed();
}


void Table_ContactsGrid::ReloadIfNeed()
{
	int nChunkSize=150;
	if(m_datLastBuild>=m_datActualContactListBuild)
		return;

	if (m_bVisible)
	{
		//clear data
		DbRecordSet oldData=m_lstData;
		m_lstData.clear();
		RefreshDisplay(-1,true);

		FuiBase *pFui=dynamic_cast<FuiBase*>(m_pContactFUI);
		if (pFui==NULL) 
			return;
		//get actual list from cache
		DbRecordSet *dataCached=pFui->getSelectionController()->GetDataSource();
		if(dataCached==NULL)
			return;

		int nSize=dataCached->getRowCount();
		if (nSize==0)
			return;


		//filter out only new ones:
		DbRecordSet lstNewData;
		ClientContactManager::CompareLists(*dataCached,oldData,&m_lstData,&lstNewData);
		nSize=lstNewData.getRowCount();
		if (nSize==0)
		{
			RefreshDisplay(-1,true);
			return;
		}

		Status err;
		DbRecordSet lstContactDetails;
		ClientContactManager::ReadContactDetails(err,lstNewData,lstContactDetails);
		if (!err.IsOK())return;

		m_lstData.merge(lstContactDetails);
		RefreshDisplay(-1,true); //call virtual funct to refresh display on selector widget
		m_datLastBuild=QDateTime::currentDateTime();
	}

}


void Table_ContactsGrid::OnRefreshContent()
{
	m_lstData.clear();
	m_datLastBuild=QDateTime::fromString("1944-10-11 00-00-00","yyyy-MM-dd HH-mm-ss");
	ReloadIfNeed();
}

void Table_ContactsGrid::OnShowDetails()
{
	int nRow=m_lstData.getSelectedRow();
	if (nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BCNT_ID").toInt();
		emit ShowContactDetails(nContactID);
	}

}

void Table_ContactsGrid::OnShowDetailsNewWindow()
{
	int nRow=m_lstData.getSelectedRow();
	if (nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BCNT_ID").toInt();
		g_objFuiManager.OpenFUI(MENU_CONTACTS,true,false,FuiBase::MODE_READ,nContactID);
		//emit ShowContactDetails(nContactID);
	}
}

void Table_ContactsGrid::OnDelete()
{

	DbRecordSet DataForDelete;
	DataForDelete.addColumn(QVariant::Int,"BCNT_ID");
	DataForDelete.merge(m_lstData,true);

	if (DataForDelete.getRowCount()>0)
	{
		Status err;

		int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete the selected contacts (%1) from the database?").arg(QVariant(DataForDelete.getRowCount()).toString()),tr("Yes"),tr("No"));
		if (nResult!=0) return; //only if YES

		_SERVER_CALL(BusContact->DeleteContacts(err,DataForDelete,true))
		//error:
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			if (err.getErrorCode()==StatusCodeSet::ERR_BUS_ACESS_REFUSED_ON_DELETE) //reload:
			{
				OnRefreshContent();
			}
			return;
		}

		m_lstData.deleteSelectedRows();
		RefreshDisplay(-1,true);

		//emit global:
		g_ClientCache.ModifyCache(ENTITY_BUS_CONTACT,DataForDelete,DataHelper::OP_REMOVE);

	}
}

void Table_ContactsGrid::GetSelectedContacts(DbRecordSet &lstContacts)
{
	lstContacts.copyDefinition(m_lstData);
	lstContacts.merge(m_lstData,true);
}

void Table_ContactsGrid::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	UniversalTableWidgetEx::RefreshDisplay(nRow,bApplyLastSortModel);
	OnSelectionChanged();
}


//create multiline
void Table_ContactsGrid::Data2CustomWidget(int nRow, int nCol)
{
	int nColType=m_lstColumnSetup.getDataRef(nCol,"BOGW_COLUMN_TYPE").toInt();

	//if (nColType==COL_TYPE_MULTILINE_TEXT)
	//{
		QTextEdit *pText = dynamic_cast<QTextEdit *>(cellWidget(nRow,nCol));
		if (!pText)
		{
			pText = new QTextEdit;
			pText->setReadOnly(true);
			setCellWidget(nRow,nCol,pText);
		}
		pText->setHtml(m_plstData->getDataRef(nRow,m_lstColumnMapping[nCol]).toString());
	//}
}