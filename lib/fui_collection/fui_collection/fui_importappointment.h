
#ifndef FUI_IMPORTAppointment_H
#define FUI_IMPORTAppointment_H

#include <QWidget>
#include "generatedfiles/ui_fui_importAppointment.h"
#include "fuibase.h"
#include <QProgressDialog>
#include "os_specific/os_specific/mapimanager.h"

class FUI_ImportAppointment : public FuiBase
{
    Q_OBJECT

public:
    FUI_ImportAppointment(QWidget *parent = 0);
    ~FUI_ImportAppointment();

	QString GetFUIName(){ return tr("Import Calendar Events"); }
	void SetDefaults(QString strDefaultContactAppointment);
	void BuildImportListFromDrop(QByteArray &byteDrop, bool bAreTemplates=false, int nDefaultProjectID=-1);
	void OnGridFilledRecalculate(QSet<QString> &setAppointments, int nDefaultProjectID=-1);

#ifdef _WIN32
	static void ReadAppointments(Status status, QList<QByteArray> &lstEntryIDs, DbRecordSet &lstData, bool bAreTemplates = false, int nDefaultProjectID = -1, QProgressDialog *progress = NULL, bool bOnlyKnownContacts=false, CMAPIEx *pMapi = NULL);
#endif
	static void ImportAppointments(Status status, DbRecordSet &lstData, int &nSkippedOrReplacedCnt, bool bAddDupsAsNewRow, bool bSkipExisting = false, bool bSkipProgress=false);
	static void ClearInternalCache();
	static void FilterAppointmentsByContact(Status status, DbRecordSet &lstData);

	static bool IsDropAppointment(QByteArray &byteDrop);
	static bool IsDropContact(QByteArray &byteDrop);
	bool FilterMatchRow(int nIdx);

	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

private:
	void EnableFilterWidgets(bool bEnable);
	
	void UpdateRowAppointmentContact(int nIdx);

	static QStringList ExtractAppointments(QString &strSrc);
	static void UpdateContactsList(QStringList &lstSrc, int nRole, DbRecordSet &lstContactLinks, QString &strUnassignedLocal);

	static QMap<QString, int> m_mapAppointment2Contact;	//cache to convert Appointment address to contact id

private:
    Ui::FUI_ImportAppointmentClass ui;

	DbRecordSet m_lstData;
	DbRecordSet m_lstDataCalEventPart;
	DbRecordSet m_lstDataCalEventPartTaskData;
	DbRecordSet m_lstProj;
	QList<QStringList> m_lstFolders;
	
	//filter data
	QDate	m_dateFilterFromDate;
	QDate	m_dateFilterToDate;

private slots:
	void on_btnOpenAppointment_clicked();
	void on_btnAssignProject_clicked();
	void on_btnImportData_clicked();
	void on_btnSelectSourceFolders_clicked();
	void on_btnBuildList_clicked();
	void OnTableDataSelectionChanged();
	void OnContactChanged();
	void OnContactSAPNEInsert();
	void OnProjectChanged();
	void OnSignalRefreshBoldRows();
};

#endif // FUI_IMPORTAppointment_H

