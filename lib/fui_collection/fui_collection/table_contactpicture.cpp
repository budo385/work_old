#include "table_contactpicture.h"
#include "gui_core/gui_core/macros.h"
#include "common/common/entity_id_collection.h"
#include "dlg_contactpicture.h"
#include "trans/trans/xmlutil.h"
#include "os_specific/os_specific/mime_types.h"
#include "common/common/status.h"
#include "gui_core/gui_core/picturehelper.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager* g_pBoSet;

Table_ContactPicture::Table_ContactPicture(QWidget * parent)
:UniversalTableWidgetEx(parent),m_toolbar(NULL)
{
}

//called after dark
void Table_ContactPicture::Initialize(DbRecordSet *plstData,toolbar_edit *pToolBar)
{
	BuildToolBar(pToolBar);
	//setup columns
	DbRecordSet columns;
	AddColumnToSetup(columns,"BCMPC_PICTURE",tr("Picture"),120,false, "",COL_TYPE_CUSTOM_WIDGET);
	AddColumnToSetup(columns,"BCMPC_DATE",tr("Date"),100,false, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMPC_NAME",tr("Name"),100,false, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMPC_TEXT",tr("Description"),100,true, "",COL_TYPE_CUSTOM_WIDGET); //set as editable, coz text is doing better with readonly flag

	//set data source for table, enable drag
	UniversalTableWidgetEx::Initialize(plstData,&columns,false,true,45);
	EnableDrag(true,ENTITY_BUS_BIG_PICTURE);
	CreateContextMenu();

	//store idx columns for faster access
	m_nDefaultColIdx=m_plstData->getColumnIdx("BCMPC_IS_DEFAULT");

	connect(this,SIGNAL(SignalRowsDeleted()),this,SLOT(OnDeleted()));
	connect(this,SIGNAL(SignalRowInserted(int)),this,SLOT(OnRowInserted(int)));
	connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
}


//dummy: if inserted: blue icon, else red
void Table_ContactPicture::GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip)
{
	//if Default then show checked icon:
	if(m_plstData->getDataRef(nRow,m_nDefaultColIdx).toInt()!=0)
	{
		RowIcon.addFile(":checked.png");
		strStatusTip=tr("Default Type");
	}
	else
		RowIcon=QIcon(); //empty icon
}

//create custom cntx menu at end of existing one:
void Table_ContactPicture::CreateContextMenu()
{
	QList<QAction*> lstActions;
	CreateDefaultContextMenuActions(lstActions);

	//Add separator
	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.prepend(SeparatorAct);
	//Insert new row:
	QAction* pAction = new QAction(tr("&Set as Default"), this);
	pAction->setIcon(QIcon(":checked.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(SetDefault()));
	lstActions.prepend(pAction);
	//Open Details:
	pAction = new QAction(tr("Open In New Window"), this);
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(OpenDetailsFromContextMenu()));
	lstActions.prepend(pAction); //insert at 2nd place

	SetContextMenuActions(lstActions);
}

//invoked by cnt menu, set default flag on selected row
void Table_ContactPicture::SetDefault()
{
	int nRow=m_plstData->getSelectedRow();
	if(nRow!=-1)
	{
		//set default flag:
		m_plstData->setColValue(m_nDefaultColIdx,0);
		m_plstData->setData(nRow,m_nDefaultColIdx,1);
		RefreshDisplay();
	}
}


//opens dialog with details & big picture
void Table_ContactPicture::OpenDetails(int nRow)
{
	Status err;
	DbRecordSet rowPicture;
	rowPicture.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
	rowPicture.addRow();

	bool bBigPictureLoaded=!m_plstData->getDataRef(nRow,"BPIC_PICTURE").isNull();
	int nRecordID=m_plstData->getDataRef(nRow,"BCMPC_ID").toInt();
	if(!bBigPictureLoaded && nRecordID!=0) //if flag is false and ID is valid, load from server
	{
		Status err;
		int nBigPicId=m_plstData->getDataRef(nRow,"BCMPC_BIG_PICTURE_ID").toInt();
		_SERVER_CALL(BusContact->ReadBigPicture(err,nBigPicId,rowPicture))
		_CHK_ERR(err)
		m_plstData->setData(nRow,"BPIC_PICTURE",rowPicture.getDataRef(0,"BPIC_PICTURE")); //save picture
	}

	if (!m_bEdit)
	{
		//open details:
		Dlg_ContactPicture *dlg = new Dlg_ContactPicture(NULL);
		DbRecordSet row = m_plstData->getRow(nRow);
		dlg->SetRow(row,m_bEdit);
		dlg->setWindowModality(Qt::NonModal);
		dlg->show();
	}
	else
	{
		//open details:
		Dlg_ContactPicture dlg;
		DbRecordSet row = m_plstData->getRow(nRow);
		dlg.SetRow(row,m_bEdit);
		int nRes=dlg.exec();
		if(nRes!=0 && m_bEdit)
		{
			DbRecordSet newData;
			dlg.GetRow(newData);
			m_plstData->assignRow(nRow,newData);		//set new data back
			RefreshDisplay(nRow);
		}
	}
}


//on insert row, insert into datasource, open details
void Table_ContactPicture::InsertRow()
{
	UniversalTableWidgetEx::InsertRow();
	OpenDetails(m_plstData->getRowCount()-1);
}

void Table_ContactPicture::OnRowInserted(int nRow)
{
	m_plstData->setData(nRow,"BCMPC_ID",0);//reset ID to 0, if copy
	RefreshDefaultFlag();
}

//double click move focus, and opens detail dialog
void Table_ContactPicture::OnCellDoubleClicked(int nRow,int nCol)
{
	m_plstData->clearSelection();
	m_plstData->selectRow(nRow);
	RefreshDisplay();
	OpenDetails(nRow);
}


//open in new window
void Table_ContactPicture::OpenDetailsFromContextMenu()
{
	int nRowSelected=m_plstData->getSelectedRow();
	if (nRowSelected>=0)
	{
		OpenDetails(nRowSelected);
	}
}

void Table_ContactPicture::BuildToolBar(toolbar_edit * pToolBar)
{
	m_toolbar=pToolBar;
	pToolBar->GetBtnAdd()->setText(tr("Add New Picture"));
	connect(pToolBar->GetBtnAdd(), SIGNAL(clicked()), this, SLOT(InsertRow()));
	connect(pToolBar->GetBtnSelect(), SIGNAL(clicked()), this, SLOT(OpenDetailsFromContextMenu()));
	connect(pToolBar->GetBtnModify(), SIGNAL(clicked()), this, SLOT(OpenDetailsFromContextMenu()));
	connect(pToolBar->GetBtnClear(), SIGNAL(clicked()), this, SLOT(DeleteSelection()));

}
void Table_ContactPicture::SetEditMode(bool bEdit)
{
	UniversalTableWidgetEx::SetEditMode(bEdit);
	if(m_toolbar)m_toolbar->setEnabled(bEdit);
}

//when drag: send selected items + type
QMimeData * Table_ContactPicture::mimeData( const QList<QTableWidgetItem *> items ) const
{
	QMimeData *mimeData=QTableWidget::mimeData(items);
	if (mimeData)
	{
		DbRecordSet lstPicture;
		lstPicture.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
		//lstPicture.addRow();

		//load big pics inside:
		int nSize=m_plstData->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			if (!m_plstData->isRowSelected(i)) continue;
			bool bBigPictureLoaded=!m_plstData->getDataRef(i,"BPIC_PICTURE").isNull();
			int nRecordID=m_plstData->getDataRef(i,"BCMPC_ID").toInt();
			if(!bBigPictureLoaded && nRecordID!=0) //if flag is false and ID is valid, load from server
			{
				DbRecordSet rowPicture;
				Status err;
				int nBigPicId=m_plstData->getDataRef(i,"BCMPC_BIG_PICTURE_ID").toInt();
				_SERVER_CALL(BusContact->ReadBigPicture(err,nBigPicId,rowPicture))
				_CHK_ERR_NO_RET(err)
				if (!err.IsOK())
					return mimeData;
				m_plstData->setData(i,"BPIC_PICTURE",rowPicture.getDataRef(0,"BPIC_PICTURE")); //save picture
			}
		}

		DbRecordSet lstSelectedItems=m_plstData->getSelectedRecordSet();
		if (lstSelectedItems.getRowCount()>0)
		{
			lstPicture.merge(lstSelectedItems); //only big pic
			//lstPicture.Dump();
			QByteArray byteListData=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstSelectedItems);
			mimeData->setData(SOKRATES_MIME_LIST,byteListData);
			mimeData->setData(SOKRATES_MIME_DROP_TYPE,QVariant(ENTITY_BUS_BIG_PICTURE).toString().toLatin1()); //pass unique type
		}
	}
	return mimeData;
}


void Table_ContactPicture::CheckDataBeforeWrite(Status &Ret_pStatus)
{
	Ret_pStatus.setError(0);
}

void Table_ContactPicture::RefreshDefaultFlag()
{
	int nCountDefs=m_plstData->countColValue(m_nDefaultColIdx,1);
	if(nCountDefs==0 && m_plstData->getRowCount()>0)				//set first to be default if default is deleted!
		m_plstData->setData(0,m_nDefaultColIdx,1);
	else if (nCountDefs>1) //if default copied
	{
		int nRowFirstDef=m_plstData->find(m_nDefaultColIdx,1,true);
		if (nRowFirstDef>=0)
		{
			m_plstData->setColValue(m_nDefaultColIdx,0);
			m_plstData->setData(nRowFirstDef,m_nDefaultColIdx,1);
		}
	}
}

//create multiline and pic:
void Table_ContactPicture::Data2CustomWidget(int nRow, int nCol)
{
	if (nCol==0) //picture
	{
		QLabel *pPic = dynamic_cast<QLabel *>(cellWidget(nRow,nCol));
		if (!pPic)
		{
			pPic = new QLabel;
			pPic->setScaledContents(false); //scales image..
			setCellWidget(nRow,nCol,pPic);
		}
		QPixmap pixMap;
		pixMap.loadFromData(m_plstData->getDataRef(nRow,"BCMPC_PICTURE").toByteArray());
		pPic->setPixmap(pixMap);
	}
	else if (nCol==3) //multiline
	{
		QTextEdit *pText = dynamic_cast<QTextEdit *>(cellWidget(nRow,nCol));
		if (!pText)
		{
			pText = new QTextEdit;
			pText->setReadOnly(true);
			setCellWidget(nRow,nCol,pText);
		}
		pText->setHtml(m_plstData->getDataRef(nRow,"BCMPC_TEXT").toString());
	}
}

void Table_ContactPicture::OnDeleted()
{
	RefreshDefaultFlag();
}

