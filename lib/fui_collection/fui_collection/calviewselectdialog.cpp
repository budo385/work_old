#include "calviewselectdialog.h"

#include <QtWidgets/QMessageBox>

CalViewSelectDialog::CalViewSelectDialog(int nViewID, QString strViewName, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	m_nViewID = nViewID;
	m_strViewName = strViewName;
	ui.viewName_lineEdit->setText(m_strViewName);

	setWindowTitle(tr("Save View"));

	ui.public_checkBox->setChecked(true);
}

CalViewSelectDialog::~CalViewSelectDialog()
{

}

void CalViewSelectDialog::on_save_pushButton_clicked()
{
	QString strViewName = ui.viewName_lineEdit->text();
	bool bIsPublic = ui.public_checkBox->isChecked();
	if (strViewName.isEmpty())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please fill the View Name field first!"));
		return;
	}
	
	if (m_nViewID > 0)
	{
		if (m_strViewName == strViewName)
			emit returnData(m_nViewID, m_strViewName, bIsPublic);
		else
			emit returnData(-1, strViewName, bIsPublic);
	}
	else
		emit returnData(-1, strViewName, bIsPublic);
	
	done(QDialog::Accepted);
}

void CalViewSelectDialog::on_cancel_pushButton_clicked()
{
	done(QDialog::Rejected);
}
