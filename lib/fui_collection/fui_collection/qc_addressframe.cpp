#include "qc_addressframe.h"
#include "common/common/entity_id_collection.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/countries.h"
#include "bus_client/bus_client/httplookup.h"

#define NM_TEMP_ID "NM_TEMP_ID"			//name of temporar column for random generate ID
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "gui_core/gui_core/thememanager.h"

#include "qcw_largewidget.h"
#include "qcw_smallwidget.h"


QC_AddressFrame::QC_AddressFrame(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	m_bLayoutSet = false;

	ui.simple_radioButton->setChecked(true);

	ui.full_radioButton->setIcon(QIcon(":AdrLarge16.png"));
	ui.simple_radioButton->setIcon(QIcon(":AdrSmall16.png"));
	ui.addSelected_toolButton_4->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_2->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_3->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_5->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_6->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_7->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_8->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_9->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_10->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_11->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_12->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_13->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_14->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_15->setIcon(QIcon(":CopyText16.png"));
	ui.addressformat_toolButton->setIcon(QIcon(":Reformat2.png"));
	ui.lockIcon_label->setPixmap(QPixmap(":Lock16.png"));

	ui.container_frame->setFixedWidth(265);

	m_CountryHandler.Initialize(ENTITY_COUNTRIES);
	m_CountryHandler.SetDataForCombo(ui.cmbCountry,"COUNTRY");//,,"DS-CODE","BCMA_COUNTRY_CODE", m_pDataManager);
	m_CountryHandler.RefreshDisplay();

	ui.contacttype_tableWidget->Initialize();
	ui.contacttype_tableWidget->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
	ui.contacttype_tableWidget->SetEditMode(true);

	connect(ui.selector_widget, SIGNAL(RowChanged(int)), this, SLOT(on_RowChanged(int)));

	ui.lookupZIP->setIcon(QIcon(":Search_In_Web24.png"));
	if(g_pSettings->GetApplicationOption(CONTACT_DISABLE_ZIP_LOOKUP).toBool())
		ui.lookupZIP->setEnabled(false);
}

QC_AddressFrame::~QC_AddressFrame()
{
	m_recFullContactData = NULL;
}

void QC_AddressFrame::Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType /*= 0 0-Large,1-small widget*/)
{
	m_nParentType = nParentType;
	m_pParentWidget = ParentWidget;
	m_recFullContactData = recFullContactData;
	//Set widget layout.
	SetLayout();
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
	ui.selector_widget->Initialize(ENTITY_BUS_CM_ADDRESS, m_recFullContactData, m_pParentWidget);
}

void QC_AddressFrame::SetAddress(DbRecordSet recAddress)
{
	m_recLocalEntityRecordSet.assignRow(m_nCurrentRow,recAddress);
	SetupGUIwidgets(recAddress);
}

void QC_AddressFrame::AddAddress(DbRecordSet recAddress)
{
	if (ui.formatedaddress_textEdit->toPlainText().isEmpty())
		ui.selector_widget->addEntity(recAddress);
	else
		ui.selector_widget->addEntityNoOverride(recAddress);
}

void QC_AddressFrame::SetLayout()
{
	//If in large widget formated address and address type goes to right.
	if (!m_bLayoutSet)
	{
		if (m_nParentType == 1)
		{
			ui.container_frame->setHidden(true);
		}
		else
		{
			/*
			ui.formatedaddress_textEdit->setFixedWidth(100);
			ui.contacttype_tableWidget->setFixedWidth(100);
			ui.formattedaddress_label->setFixedWidth(30);
			*/
			//First take the three buttons on left.
			QLayoutItem *AddSelectedButton = ui.frame_3->layout()->takeAt(ui.frame_3->layout()->indexOf(ui.addSelected_toolButton_15));
			QLayoutItem *FormatAddressButton = ui.frame_3->layout()->takeAt(ui.frame_3->layout()->indexOf(ui.addressformat_toolButton));
			QLayoutItem *LockFrame = ui.frame_3->layout()->takeAt(ui.frame_3->layout()->indexOf(ui.lockIcon_frame));

			//Buttons vertical layout.
			QVBoxLayout *vLayout = new QVBoxLayout();
			vLayout->addSpacing(40);
			vLayout->addItem(AddSelectedButton);
			vLayout->addItem(FormatAddressButton);
			vLayout->addStretch(10);
			vLayout->setContentsMargins( 0, 0, 0, 0);
			vLayout->setSpacing(4);

			//Then create the rest.
			QLayoutItem *FormatedAddressLabel = ui.frame_3->layout()->takeAt(ui.frame_3->layout()->indexOf(ui.formattedaddress_label));
			QLayoutItem *FormatedAddressTextEdit = ui.frame_3->layout()->takeAt(ui.frame_3->layout()->indexOf(ui.formatedaddress_textEdit));
			QLayoutItem *AddressTypeWidget = ui.frame_3->layout()->takeAt(ui.frame_3->layout()->indexOf(ui.contacttype_tableWidget));

			QHBoxLayout *lockHorizLayout = new QHBoxLayout();
			lockHorizLayout->addItem(FormatedAddressLabel);
			//lockHorizLayout->addStretch(1);
			lockHorizLayout->addItem(LockFrame);
			lockHorizLayout->setSpacing(4);
			lockHorizLayout->setContentsMargins( 0, 0, 0, 0);

			QVBoxLayout *vLayout1 = new QVBoxLayout();
			vLayout1->addSpacing(15);
			vLayout1->addItem(lockHorizLayout);
			vLayout1->addItem(FormatedAddressTextEdit);
			vLayout1->addItem(AddressTypeWidget);
			vLayout1->addStretch(100);
			vLayout1->setContentsMargins( 0, 0, 0, 0);
			vLayout1->setSpacing(4);

			QHBoxLayout *horizLayout = new QHBoxLayout();
			horizLayout->addItem(vLayout);
			horizLayout->addItem(vLayout1);
			horizLayout->setContentsMargins( 0, 0, 0, 0);
			horizLayout->setSpacing(0);

			ui.container_frame->setLayout(horizLayout);
			ui.container_frame->layout()->setContentsMargins( 4, 4, 4, 4);
			ui.container_frame->layout()->setSpacing(0);
		}

		m_bLayoutSet = true;
	}
}

void QC_AddressFrame::SetFirstName(QString strValue)
{
	ui.firstname_lineEdit->setText(strValue);
}

void QC_AddressFrame::SetMiddleName(QString strValue)
{
	ui.middlename_lineEdit->setText(strValue);
}

void QC_AddressFrame::SetLastName(QString strValue)
{
	ui.lastname_lineEdit->setText(strValue);
}

void QC_AddressFrame::SetOrganization(QString strValue)
{
	ui.organizationname_lineEdit->setText(strValue);
}

void QC_AddressFrame::SetTown(QString strValue)
{
	ui.town_lineEdit->setText(strValue);
}

void QC_AddressFrame::UpdateSalutation(QString strRecordSetFieldName, QString strValue)
{
	int nRowCount=m_recLocalEntityRecordSet.getRowCount();
	for (int i=0; i<nRowCount; i++)
	{
		m_recLocalEntityRecordSet.setData(i, strRecordSetFieldName, strValue);
	}
}

void QC_AddressFrame::SetupGUIwidgets(DbRecordSet recData)
{
	ui.title_lineEdit->setText(recData.getDataRef(0, "BCMA_TITLE").toString());
	ui.firstname_lineEdit->setText(recData.getDataRef(0, "BCMA_FIRSTNAME").toString());
	ui.middlename_lineEdit->setText(recData.getDataRef(0, "BCMA_MIDDLENAME").toString());
	ui.lastname_lineEdit->setText(recData.getDataRef(0, "BCMA_LASTNAME").toString());
	ui.organizationname_lineEdit->setText(recData.getDataRef(0, "BCMA_ORGANIZATIONNAME").toString());
	ui.organizationname_lineEdit_2->setText(recData.getDataRef(0, "BCMA_ORGANIZATIONNAME_2").toString());
	ui.street_lineEdit->setText(recData.getDataRef(0, "BCMA_STREET").toString());
	ui.pobox_lineEdit->setText(recData.getDataRef(0, "BCMA_POBOX").toString());
	ui.pozip_lineEdit->setText(recData.getDataRef(0, "BCMA_POBOXZIP").toString());
	ui.zip_lineEdit->setText(recData.getDataRef(0, "BCMA_ZIP").toString());
	ui.town_lineEdit->setText(recData.getDataRef(0, "BCMA_CITY").toString());
	ui.state_lineEdit->setText(recData.getDataRef(0, "BCMA_REGION").toString());
	//ui.cmbCountry->blockSignals(true);
	//ui.cmbCountry->setEditText(recData.getDataRef(0, "BCMA_COUNTRY_NAME").toString());
	//ui.cmbCountry->blockSignals(false);
	ui.countrycode_lineEdit->setText(Countries::GetDsCodeFromISO(recData.getDataRef(0, "BCMA_COUNTRY_CODE").toString())); //BT: must be AFTER country name: issue 1526   	
	if (ui.countrycode_lineEdit->text().isEmpty())
	{
		ui.countrycode_lineEdit->setText(g_pSettings->GetPersonSetting(CURRENT_COUNTRY_ISO_CODE).toString());
	}

	ui.formatedaddress_textEdit->setText(recData.getDataRef(0, "BCMA_FORMATEDADDRESS").toString());
	ui.lock_checkBox->setChecked(g_pSettings->GetPersonSetting(QUICK_CONTACT_ADDRESS_FORMATING_LOCK).toBool());

	//types:
	ui.contacttype_tableWidget->SetAddressRow(&m_recLocalEntityRecordSet,m_nCurrentRow);
	connect(ui.contacttype_tableWidget,SIGNAL(SignalDataChanged(int,int)),this,SLOT(OnTypeChanged(int,int)));

	
}

void QC_AddressFrame::SetDataToRecordSet(QString strRecordSetFieldName, QString strValue)
{
	if (m_nParentType == 0)
		dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->AddressChanged(m_nCurrentRow, strRecordSetFieldName, strValue);
	else if (m_nParentType == 1)
		dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->AddressChanged(m_nCurrentRow, strRecordSetFieldName, strValue);

	//m_recLocalEntityRecordSet.Dump();
	//DbRecordSet lstAddressType=m_recLocalEntityRecordSet.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
	//lstAddressType.Dump();

	m_recLocalEntityRecordSet.setData(m_nCurrentRow, strRecordSetFieldName, strValue);
	m_recFullContactData->setData(0, "LST_ADDRESS", m_recLocalEntityRecordSet);
}

void QC_AddressFrame::SetIntDataToRecordSet(QString strRecordSetFieldName, int nValue)
{
	m_recLocalEntityRecordSet.setData(m_nCurrentRow, strRecordSetFieldName, nValue);
	m_recFullContactData->setData(0, "LST_ADDRESS", m_recLocalEntityRecordSet);
}

void QC_AddressFrame::FormatAddressTextEdit(bool bForce /*= false*/)
{
	if (ui.lock_checkBox->isChecked() && !bForce)
		return;
	
	DbRecordSet data;
	data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
	data.merge(m_recLocalEntityRecordSet.getRow(m_nCurrentRow));

	ClientContactManager m_ContactMan;
	m_ContactMan.FormatAddressWithDefaultSchema(data,ContactTypeManager::CM_TYPE_PERSON,ClientContactManager::FORMAT_USING_USERDEFAULTS,false); //format address
	ui.formatedaddress_textEdit->setText(data.getDataRef(0, "BCMA_FORMATEDADDRESS").toString());
}

void QC_AddressFrame::on_RowChanged(int nRow)
{
	DbRecordSet tmpRowLocalEntityRecordSet;
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

/*
	m_recLocalEntityRecordSet.Dump();
	for (int i=0;i<m_recLocalEntityRecordSet.getRowCount();i++)
	{
		DbRecordSet lstTypes=m_recLocalEntityRecordSet.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
		lstTypes.Dump();
	}
*/
	

	tmpRowLocalEntityRecordSet.copyDefinition(m_recLocalEntityRecordSet);
	if (nRow < 0)
	{
		tmpRowLocalEntityRecordSet.addRow();
	}
	else
	{
		m_recLocalEntityRecordSet.clearSelection();
		m_recLocalEntityRecordSet.selectRow(nRow);
		tmpRowLocalEntityRecordSet.merge(m_recLocalEntityRecordSet, true);
	}

	m_nCurrentRow = nRow;

	SetupGUIwidgets(tmpRowLocalEntityRecordSet);
}

void QC_AddressFrame::on_title_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_TITLE", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_firstname_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_FIRSTNAME", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_middlename_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_MIDDLENAME", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_lastname_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_LASTNAME", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_organizationname_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_ORGANIZATIONNAME", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_organizationname_lineEdit_2_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_ORGANIZATIONNAME_2", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_street_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_STREET", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_pobox_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_POBOX", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_pozip_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_POBOXZIP", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_zip_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_ZIP", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_town_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_CITY", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_state_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_REGION", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_countrycode_lineEdit_textChanged(const QString &text)
{
	if (text.isEmpty())
	{
		//refresh display:
		ui.cmbCountry->blockSignals(true);
		ui.cmbCountry->setCurrentIndex(-1);
		ui.cmbCountry->blockSignals(false);
		return;
	}

	//find ISO code:
	int nRow=m_CountryHandler.GetDataSource()->find(3,text,true);

	//refresh display:
	ui.cmbCountry->blockSignals(true);
	ui.cmbCountry->setCurrentIndex(nRow);
	ui.cmbCountry->blockSignals(false);
	
	QString strCountryName = ui.cmbCountry->itemText(nRow);

	SetDataToRecordSet("BCMA_COUNTRY_CODE", Countries::GetISOCodeFromDs(text));
	SetDataToRecordSet("BCMA_COUNTRY_NAME", strCountryName);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_cmbCountry_editTextChanged(const QString &text)
{
	if (text.isEmpty())
	{
		//refresh display:
		ui.cmbCountry->blockSignals(true);
		ui.cmbCountry->setCurrentIndex(-1);
		ui.cmbCountry->blockSignals(false);
		ui.countrycode_lineEdit->blockSignals(true);
		ui.countrycode_lineEdit->setText("");
		ui.countrycode_lineEdit->blockSignals(false);
		SetDataToRecordSet("BCMA_COUNTRY_CODE","");
		SetDataToRecordSet("BCMA_COUNTRY_NAME","");
		return;
	}

	//find ISO code:
	int nRow=m_CountryHandler.GetDataSource()->find(0,text,true);

	QString strISOCode="";
	if (nRow>=0)
	{
		strISOCode=m_CountryHandler.GetDataSource()->getDataRef(nRow, "ISO").toString();
		ui.countrycode_lineEdit->blockSignals(true);
		ui.countrycode_lineEdit->setText(m_CountryHandler.GetDataSource()->getDataRef(nRow, "DS-CODE").toString());
		ui.countrycode_lineEdit->blockSignals(false);
	}

	SetDataToRecordSet("BCMA_COUNTRY_CODE",strISOCode);
	SetDataToRecordSet("BCMA_COUNTRY_NAME", text);
	FormatAddressTextEdit();
}

void QC_AddressFrame::on_formatedaddress_textEdit_textChanged()
{
	SetDataToRecordSet("BCMA_FORMATEDADDRESS", ui.formatedaddress_textEdit->toPlainText());
}

void QC_AddressFrame::on_simple_radioButton_toggled(bool checked)
{
	ui.title_label->setHidden(checked);
	ui.title_lineEdit->setHidden(checked);
	ui.addSelected_toolButton_4->setHidden(checked);
	ui.pobox_pozip_label->setHidden(checked);
	ui.pobox_lineEdit->setHidden(checked);
	ui.pozip_lineEdit->setHidden(checked);
	ui.addSelected_toolButton_8->setHidden(checked);
	ui.addSelected_toolButton_9->setHidden(checked);
	ui.state_label->setHidden(checked);
	ui.state_lineEdit->setHidden(checked);
	ui.addSelected_toolButton_12->setHidden(checked);
}

void QC_AddressFrame::on_lock_checkBox_stateChanged(int state)
{
	if (state == Qt::Checked)
		g_pSettings->SetPersonSetting(QUICK_CONTACT_ADDRESS_FORMATING_LOCK, true);
	else
		g_pSettings->SetPersonSetting(QUICK_CONTACT_ADDRESS_FORMATING_LOCK, false);
}

void QC_AddressFrame::on_addressformat_toolButton_clicked()
{
	FormatAddressTextEdit(true);
}


//Add selected part.
void QC_AddressFrame::on_addSelected_toolButton_4_clicked()
{
	if (m_nParentType == 0)
		ui.title_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.title_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_clicked()
{
	if (m_nParentType == 0)
		ui.firstname_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.firstname_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_2_clicked()
{
	if (m_nParentType == 0)
		ui.middlename_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.middlename_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_3_clicked()
{
	if (m_nParentType == 0)
		ui.lastname_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.lastname_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_5_clicked()
{
	if (m_nParentType == 0)
		ui.organizationname_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.organizationname_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_6_clicked()
{
	if (m_nParentType == 0)
		ui.organizationname_lineEdit_2->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.organizationname_lineEdit_2->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_7_clicked()
{
	if (m_nParentType == 0)
		ui.street_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.street_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_9_clicked()
{
	if (m_nParentType == 0)
		ui.pobox_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.pobox_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_8_clicked()
{
	if (m_nParentType == 0)
		ui.pozip_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.pozip_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_10_clicked()
{
	if (m_nParentType == 0)
		ui.zip_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.zip_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_11_clicked()
{
	if (m_nParentType == 0)
		ui.town_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.town_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_12_clicked()
{
	if (m_nParentType == 0)
		ui.state_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.state_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_13_clicked()
{
	if (m_nParentType == 0)
		ui.countrycode_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.countrycode_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_14_clicked()
{
	if (m_nParentType == 0)
		ui.cmbCountry->setEditText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.cmbCountry->setEditText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_addSelected_toolButton_15_clicked()
{
	if (m_nParentType == 0)
		ui.formatedaddress_textEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.formatedaddress_textEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_AddressFrame::on_lookupZIP_clicked()
{
	QString strZIP = ui.zip_lineEdit->text();
	QString strTown = ui.town_lineEdit->text();
	QString strCountry = ui.countrycode_lineEdit->text();
	bool bZipWasEmpty = strZIP.isEmpty();
	bool bCountryWasEmpty = strCountry.isEmpty();

	if(HttpLookup::WebLookupZIPTown(strZIP, strTown, strCountry))
	{
		if(bZipWasEmpty)
			ui.zip_lineEdit->setText(strZIP);
		else
			ui.town_lineEdit->setText(strTown);
		if(bCountryWasEmpty){
			ui.countrycode_lineEdit->setText(strCountry);
			on_countrycode_lineEdit_textChanged(strCountry);
		}
	}
}

void QC_AddressFrame::OnTypeChanged(int,int)
{

	/*
	//qDebug()<<"OnTypeChanged started";
	m_recLocalEntityRecordSet.Dump();
	for (int i=0;i<m_recLocalEntityRecordSet.getRowCount();i++)
	{
		DbRecordSet lstTypes=m_recLocalEntityRecordSet.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
		lstTypes.Dump();
	}
	*/
	
	//just save to main record changes...
	m_recFullContactData->setData(0, "LST_ADDRESS", m_recLocalEntityRecordSet);

	//qDebug()<<"OnTypeChanged end";
}

void QC_AddressFrame::on_isDefault_checkBox_stateChanged(int state)
{
	if (state == Qt::Checked)
	{
		int nRowCount = m_recLocalEntityRecordSet.getRowCount();
		for (int i=0; i < nRowCount; i++)
			m_recLocalEntityRecordSet.setData(i, "BCMA_IS_DEFAULT", 0);

		SetIntDataToRecordSet("BCMA_IS_DEFAULT", 1);
	}
	else
		SetIntDataToRecordSet("BCMA_IS_DEFAULT", 0);
}
