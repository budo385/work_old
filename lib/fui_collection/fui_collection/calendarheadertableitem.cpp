#include "calendarheadertableitem.h"

#include <QHeaderView>
#include <QMimeData>
#include <QGraphicsSceneDragDropEvent>

#include "calendartableview.h"
#include "calendarmultidaygraphicsview.h"

HeaderTableItemDelegate::HeaderTableItemDelegate(QObject * parent /*= 0*/) : QItemDelegate(parent)
{

}

void HeaderTableItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	bool bAllowSelection = true;
	if ((index.row()<1))
		bAllowSelection = false;
	
	//QItemDelegate::paint(painter, option, index);
	QPointF p1(option.rect.bottomLeft().x()+option.rect.width()/24*8, option.rect.bottomLeft().y());
	QPointF p2(option.rect.topRight().x()-option.rect.width()/24*8, option.rect.topRight().y());
	QPointF p3(option.rect.topLeft().x()+option.rect.width()/24*8, option.rect.topRight().y());
	QPointF p4(option.rect.bottomRight().x()-option.rect.width()/24*8, option.rect.bottomRight().y());
	QRectF rect1(option.rect.topLeft(), p1);
	QRectF rect2(p2, option.rect.bottomRight());
	QRectF rect3(p3, p4);

	painter->fillRect(rect3, QBrush(QColor(255, 255,215)));
	painter->fillRect(rect1, QBrush(QColor(255, 245,190)));
	painter->fillRect(rect2, QBrush(QColor(255, 245,190)));
	
	//Don't mark selected first and last row.
	if ((option.state & QStyle::State_Selected) && bAllowSelection)
	{
		painter->fillRect(option.rect, option.palette.highlight());
	}


	//Draw lines.
	QPen pen;
	pen.setStyle(Qt::SolidLine);
	pen.setColor(QColor(234, 207,152));
	pen.setWidth(0.2);
	QFont font;
	font.setBold(true);
	painter->setPen(pen);
	painter->setFont(font);

	for (int i = 0; i < 24; ++i)
	{
		if (!(i%2) && i)
		{
			if (!index.row())
			{
				qreal x = option.rect.x()+option.rect.width()*i/24;
				qreal y = option.rect.topLeft().y()+10;
				qreal y1 = option.rect.bottomRight().y();
				painter->drawLine(x, y, x, y1);
				//painter->drawLine(option.rect.x()+option.rect.width()*i/24, option.rect.topLeft().y()+10, option.rect.x()+option.rect.width()*i/24, option.rect.bottomRight().y());
				QRect rectBounding;
				int nHourVerticalOffSet = 1;
				//Draw hour.
				QFont font;
				font.setPointSize(6);
				font.setStyleStrategy(QFont::PreferAntialias);
				painter->setFont(font);
				QPointF topLeft(option.rect.x()+option.rect.width()*i/24-option.rect.width()/24, option.rect.y());
				QPointF bottomRight(option.rect.x()+option.rect.width()*i/24+option.rect.width()/24, option.rect.y()+10);
				QRectF textRect(topLeft, bottomRight);

				//If selected change font color.
				if ((option.state & QStyle::State_Selected) && bAllowSelection)
					pen.setColor(QColor(255,255,255));
				else
					pen.setColor(QColor(160,130,0));

				painter->setPen(pen);
				
				//If row 0 draw text.
				if (!index.row())
					painter->drawText(textRect, Qt::AlignCenter, QVariant(i).toString());
				
				pen.setColor(QColor(234, 207,152));
				painter->setPen(pen);
			}
			else
			{
				qreal x = option.rect.x()+option.rect.width()*i/24;
				qreal y = option.rect.topLeft().y();
				qreal y1 = option.rect.bottomRight().y();
				painter->drawLine(x, y, x, y1);
				//painter->drawLine(option.rect.x()+option.rect.width()*i/24, option.rect.topLeft().y(), option.rect.x()+option.rect.width()*i/24, option.rect.bottomRight().y());
			}
		}
		else
		{
			qreal x = option.rect.x()+option.rect.width()*i/24;
			qreal y = option.rect.topLeft().y();
			qreal y1 = option.rect.bottomRight().y();
			painter->drawLine(x, y, x, y1);
			//painter->drawLine(option.rect.x()+option.rect.width()*i/24, option.rect.topLeft().y(), option.rect.x()+option.rect.width()*i/24, option.rect.bottomRight().y());
		}
	}
	
	painter->drawLine(option.rect.x()+option.rect.width()-1, option.rect.topLeft().y(), option.rect.x()+option.rect.width()-1, option.rect.bottomRight().y());

	QPen pen1;
	pen1.setStyle(Qt::SolidLine);
	pen1.setColor(QColor(234, 207,152));
	pen1.setWidth(0.2);
	painter->setPen(pen1);
	painter->drawLine(option.rect.bottomLeft(), option.rect.bottomRight());

}

CalendarHeaderTableItem::CalendarHeaderTableItem(QGraphicsView* pCalendarMultiDayGraphicsView, QAbstractTableModel	*pCalendarHeaderTableModel, QGraphicsItem * parent /*= 0*/, Qt::WindowFlags wFlags /*= 0*/)
	: QGraphicsProxyWidget(parent, wFlags)
{
	m_pCalendarMultiDayGraphicsView = pCalendarMultiDayGraphicsView;
	m_nItemLeftOffSet = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getVertHeaderWidth();
	m_pTable = new TableView(m_nItemLeftOffSet, true);
	//m_pTable->setSelectionMode(QAbstractItemView::SingleSelection);
	//m_pTable->setSelectionBehavior(QAbstractItemView::SingleSelection);

	HeaderTableItemDelegate *item = new HeaderTableItemDelegate();
	m_pTable->setItemDelegate(item);
	m_pModel = pCalendarHeaderTableModel;
	m_pTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	m_pTable->horizontalHeader()->hide();
	m_pTable->verticalHeader()->hide();
	m_pTable->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	setWidget(m_pTable);

	ScrollBar *HorizontalScrollBar = new ScrollBar();
	ScrollBar *VerticalScrollBar = new ScrollBar();

	m_pTable->setHorizontalScrollBar(HorizontalScrollBar);
	m_pTable->setVerticalScrollBar(VerticalScrollBar);
	m_pTable->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	m_pTable->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

	setAcceptHoverEvents(true);

	connect(HorizontalScrollBar, SIGNAL(scrollBarVisible()), this, SLOT(on_HScrollShow()));
	connect(HorizontalScrollBar, SIGNAL(scrollBarHidden()), this, SLOT(on_HScrollHide()));
	connect(VerticalScrollBar, SIGNAL(scrollBarVisible()), this, SLOT(on_VScrollShow()));
	connect(VerticalScrollBar, SIGNAL(scrollBarHidden()), this, SLOT(on_VScrollHide()));
	connect(m_pTable, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(on_doubleClicked(const QModelIndex &)));
}

CalendarHeaderTableItem::~CalendarHeaderTableItem()
{
	m_pCalendarMultiDayGraphicsView = NULL;
	m_pTable						= NULL;
	m_pModel						= NULL;
}

void CalendarHeaderTableItem::Initialize()
{
	m_pTable->setModel(0);

	if (dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getEntityCount() > 1)
		m_pTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	else
		m_pTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	m_pTable->setModel(m_pModel);
	
	//Set default row height and first row height
	m_pTable->verticalHeader()->setDefaultSectionSize(30);
	m_pTable->verticalHeader()->setStretchLastSection(true);
	m_pTable->verticalHeader()->hide();
	m_pTable->setRowHeight(0,14);
}

void CalendarHeaderTableItem::setColumnWidth(int nColumn, int nWidth)
{
	m_pTable->setColumnWidth(nColumn, nWidth);
	m_pTable->horizontalHeader()->setDefaultSectionSize(nWidth);
}

void CalendarHeaderTableItem::setRowHeight(int nRow, int nHeight)
{
	m_pTable->setRowHeight(nRow, nHeight);
}

QRect CalendarHeaderTableItem::getItemRectangle(int nEntityID, int nEntityType, QDateTime &startDateTime, QDateTime &endDateTime)
{
	m_nItemLeftOffSet = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getVertHeaderWidth();
	//qreal nStartRemain, nEndRemain;
	QModelIndex startIndex, endIndex;
	QTime StartTime(0,0);
	QTime EndTime(23,59, 59, 99);
	qreal n = (qreal)startDateTime.time().secsTo(StartTime)/86400;
	qreal q = (qreal)endDateTime.time().secsTo(EndTime)/86400;
	dynamic_cast<CalendarHeaderTableModel*>(m_pModel)->getItem(nEntityID, nEntityType, startDateTime, startIndex);
	dynamic_cast<CalendarHeaderTableModel*>(m_pModel)->getItem(nEntityID, nEntityType, endDateTime, endIndex);
	QRect startItemRect = getItemRectangle(startIndex);
	qreal startOffset = -n*startItemRect.width();
	QPoint startPoint(startItemRect.topLeft().x()+m_nItemLeftOffSet+startOffset, startItemRect.topLeft().y());
	QRect endItemRect = getItemRectangle(endIndex);
	qreal endOffset = -q*endItemRect.width();
	QPoint endPoint(endItemRect.topRight().x()+m_nItemLeftOffSet+endOffset, endItemRect.bottomRight().y());

	return QRect(startPoint, endPoint);
}

QRect CalendarHeaderTableItem::getItemRectangle(QModelIndex &Index)
{
	return QRect(m_pTable->visualRect(Index));
}

QRect CalendarHeaderTableItem::getItemRectangleForHeader(int nColumn)
{
	QModelIndex index = m_pModel->index(0, nColumn);
	QRect itemRect = m_pTable->visualRect(index);
	QRect rectangle(itemRect.topLeft().x(), itemRect.topLeft().y(), itemRect.width(), itemRect.height());
	return rectangle;
}

QRect CalendarHeaderTableItem::getItemRectangleForVertPersonHeader(int nRow)
{
	QModelIndex index = m_pModel->index(nRow, 0);
	QRect itemRect = m_pTable->visualRect(index);
	QRect rectangle(itemRect.topLeft().x(), itemRect.topLeft().y(), itemRect.width(), itemRect.height());
	return rectangle;
}

void CalendarHeaderTableItem::resizeColumn(int nColumn, int nWidth, bool bResizeAll)
{
	if (bResizeAll)
	{
		int nColumnCount = m_pModel->columnCount();
		for (int i = 0; i < nColumnCount; ++i)
		{
			m_pTable->setColumnWidth(i, nWidth);
			m_pTable->horizontalHeader()->setDefaultSectionSize(nWidth);
		}
	}
	else
	{
		m_pTable->setColumnWidth(nColumn, nWidth);
	}
}

void CalendarHeaderTableItem::setColumnsDefaultWidth(int nColumnWidth)
{
	m_pTable->horizontalHeader()->setDefaultSectionSize(nColumnWidth);
	m_pTable->horizontalHeader()->resizeSections(QHeaderView::Interactive);
	m_pTable->update();
}

void CalendarHeaderTableItem::on_UpdateCalendar()
{
	int nVertHeaderWidth = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getVertHeaderWidth();
	//QRect rectViewport = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->viewport()->rect();
	if (dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getEntityCount() > 1)
	{
		setPos(nVertHeaderWidth, dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getDateHeaderHeight());
		resize(m_pCalendarMultiDayGraphicsView->viewport()->rect().width()-nVertHeaderWidth, m_pCalendarMultiDayGraphicsView->viewport()->rect().height()-dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getDateHeaderHeight());
		m_pTable->resize(m_pCalendarMultiDayGraphicsView->viewport()->rect().width()-nVertHeaderWidth, m_pCalendarMultiDayGraphicsView->viewport()->rect().height()-dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getDateHeaderHeight());
		//m_pTable->setRowHeight(1, m_pCalendarMultiDayGraphicsView->viewport()->rect().height()-dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getDateHeaderHeight());
	}
	else
	{
		setPos(nVertHeaderWidth, 0);
		resize(m_pCalendarMultiDayGraphicsView->viewport()->rect().width()-nVertHeaderWidth, dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getMultiHeaderHeight());
		m_pTable->resize(m_pCalendarMultiDayGraphicsView->viewport()->rect().width()-nVertHeaderWidth, m_pCalendarMultiDayGraphicsView->viewport()->rect().height());
		//m_pTable->setRowHeight(0, m_pCalendarMultiDayGraphicsView->viewport()->rect().height());
	}
}

void CalendarHeaderTableItem::on_scrollBar_valueChanged(int value)
{
	if (dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getEntityCount()==1)
		m_pTable->horizontalScrollBar()->setValue(value);
}

void CalendarHeaderTableItem::on_HScrollShow()
{
	emit horizontalScrollBarVisible(true);
}

void CalendarHeaderTableItem::on_HScrollHide()
{
	emit horizontalScrollBarVisible(false);
}

void CalendarHeaderTableItem::on_VScrollShow()
{
	emit verticalScrollBarVisible(true);
}

void CalendarHeaderTableItem::on_VScrollHide()
{
	emit verticalScrollBarVisible(false);
}

void CalendarHeaderTableItem::on_doubleClicked(const QModelIndex &index)
{
	emit cellDoubleClicked(index.row(), index.column());
}

void CalendarHeaderTableItem::testDragDrop(QGraphicsSceneDragDropEvent *event)
{
	const QMimeData *mimeData = event->mimeData();
	QStringList lstData = mimeData->text().split(";");
	if (lstData.value(0) == "entity_item")
	{
		QModelIndex selectedCell = m_pTable->indexAt(event->pos().toPoint());
		int nEntityID, nEntityType, nEntityPersonID;
		dynamic_cast<CalendarHeaderTableModel*>(m_pModel)->getEntityForRow(selectedCell.row(), selectedCell.column(), nEntityID, nEntityType, nEntityPersonID);

		int nDraggedEntityID = lstData.value(1).toInt();
		int nDraggedEntityType = lstData.value(2).toInt();
		int nDraggedEntityPersonID = lstData.value(3).toInt();

		qDebug() << nEntityType << " " << nEntityID;

		if (nEntityID==-1 || nEntityType==-1 || (nDraggedEntityID==nEntityID && nDraggedEntityType==nEntityType))
			event->ignore();
		else
			event->accept();
	}
	else
		event->ignore();
}

void CalendarHeaderTableItem::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	testDragDrop(event);
}

void CalendarHeaderTableItem::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{

}

void CalendarHeaderTableItem::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
	testDragDrop(event);
}

void CalendarHeaderTableItem::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	QModelIndex selectedCell	= m_pTable->indexAt(event->pos().toPoint());
	QDateTime datSelectionFrom	= dynamic_cast<CalendarHeaderTableModel*>(m_pModel)->getItemStartDateTime(selectedCell);
	QDateTime datSelectionTo	= dynamic_cast<CalendarHeaderTableModel*>(m_pModel)->getItemEndDateTime(selectedCell);
	int nEntityID, nEntityType, nEntityPersonID;
	dynamic_cast<CalendarHeaderTableModel*>(m_pModel)->getEntityForRow(selectedCell.row(), selectedCell.column(), nEntityID, nEntityType, nEntityPersonID);

	const QMimeData *mimeData = event->mimeData();
	QStringList lstData = mimeData->text().split(";");
	int nDraggedEntityID = lstData.value(1).toInt();
	int nDraggedEntityType = lstData.value(2).toInt();
	int nDraggedEntityPersonID = lstData.value(3).toInt();

	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->on_dropEntityOnTable(nDraggedEntityID, nDraggedEntityType, nDraggedEntityPersonID, nEntityID, nEntityType, 
		nEntityPersonID, datSelectionFrom, datSelectionTo);
}
