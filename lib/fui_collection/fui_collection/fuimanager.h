#ifndef FUIMANAGER_H
#define FUIMANAGER_H

#include <QWidget>
#include <QObject>
#include <QStackedWidget>
#include <QComboBox>
#include <QTabWidget>
#include <QMap>
#include <QList>
#include "fuibase.h"
#include "common/common/observer_ptrn.h"
#include "communicationmenu_base.h"
#include "menuitems.h"

class FuiManager : public QObject, public ObsrPtrn_Observer
{
	Q_OBJECT

	class FuiInfo {
		public:
			FuiInfo(){
				pFuiWidget = NULL;
				pTabWidget = NULL;
				pFuiPlaceholder = NULL;
				nFP = -1;
				nGID = -1;
			};

			FuiInfo(const FuiInfo &other){
				operator = (other);
			};

			bool operator == (const FuiInfo &other){
				return (pFuiWidget == other.pFuiWidget);
			}

			void operator = (const FuiInfo &other){
				pFuiWidget = other.pFuiWidget;
				pTabWidget = other.pTabWidget;
				pFuiPlaceholder = other.pFuiPlaceholder;
				nFP = other.nFP;
				nGID = other.nGID;
			};

		public:
			QWidget *pFuiWidget;			// widget
			QTabWidget *pTabWidget;			// parent widget
			QWidget *pFuiPlaceholder;	    // tab's parent
			int nFP;						// Function Point code
			int	nGID;						// unique ID for an object
	};

public:
    FuiManager();
    ~FuiManager();

	void Initialize(QWidget	*pMainWnd, QStackedWidget *pWorkAreaWnd, QMenu *pWindowMenu);

	int	OpenFUI(int nFuiCode, bool bSeparateWindow = false, bool bForceNewTab = false, int nMode = FuiBase::MODE_EMPTY, int nRecordID=-1,bool bDontShow=false, bool bOpenedAsAvatar=false);
	bool CloseFUI(int nGID, bool bUnconditional = false);
	bool CloseAllFUIWindows(bool bUnconditional = true);
	QWidget *GetFUIWidget(int nGID);
	FuiBase *GetFUI(int nGID);
	int GetFuiID(FuiBase *pWidget);
	bool ActivateFUI(int nGID);
	int GetCurrentFP(){return m_nCurMainFP;}
	int GetCurrentCommGridGID(){return m_nCurCommGridFUI;}
	bool IsInstanceAlive(int nFuiCode);
	bool IsStandAloneWidget(QWidget *pWidget);
	QWidget* GetCurrentActiveTabFUI();
	
	int FindFuiByType(int nTypeID, int nTaskID = -1);
	void RefreshFUI(int nGID, bool bActivate=false);

	//helpers
	QString GetFuiTitle(int nCode);
	
	//---------------SIDEBAR-------------------------------//
	bool IsActiveWindowFUI(int nFuiType);
	bool IsActiveWindowCurrentFUI(FuiBase *current);
	int	OpenSideBarModeFUI(int nFuiCode, int nMode = FuiBase::MODE_EMPTY, int nRecordID=-1,bool bDontShow=false);
	void SetActiveFUI(FuiBase *pFuiActive);
	FuiBase* GetActiveFUI(){return m_pFuiActive;};
	void ClearActiveFUI(FuiBase *pFuiActive);
	void RegisterSideBarCommMenuSelector(int nFuiType,CommunicationMenu_Base *pCommMenu);
	void UnRegisterSideBarCommMenuSelector(int nFuiType,CommunicationMenu_Base *pCommMenu);
	CommunicationMenu_Base* GetSideBarCommMenuSelector(int nFuiType);
	//opening QCW:
	FuiBase* OpenQCWWindow(QWidget *parent,int nContactID=-1,DbRecordSet *lstContacts=NULL,QString strDropText="",DbRecordSet *recDefaultContact=NULL);
	//---------------SIDEBAR-------------------------------//

	//event handlers
	void OnFuiComboChanged(int nIndex);
	void OnFuiTabSelected(int nIndex);
	void OnFuiWindowClose(QWidget *pWindow);

	bool m_bSkipComboUpdate;

	static int FromEntityToMenuID(int nEntityID);

	void InstallEventFilterOnAllContainers(QObject *pFilter);
	void DeInstallEventFilterOnAllContainers(QObject *pFilter);
	//----------------------------------------
	//	UPDATE OBSERVER 
	//----------------------------------------
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

signals:
	void TabWantsToMaximizeToFullScreen();
	void FuiChanged();

public slots:
	bool CloseCurrentFuiTab(bool bUnconditional = false);
	void MaximizeToFullScreen();
	void OnFuiWindowMenuTriggered(QAction *action, bool bChecked);

protected:
	QWidget *CreateFuiWidget(int nFuiType, QWidget *pParent, bool bOpenedAsAvatar=false);
	int FindFUIWidget(int nGID);
	void SetupTabIcons(QTabWidget *pTabWidget);
	void SetLastCommGrid(int nFui, int nFuiType);

protected:
	int m_nGIDCounter;		// unique IDs generator
	int m_nCurMainFP;		// identify FP type curently open witin the main window
	int m_nCurCommGridFUI;	// identify FP of Comm grid type curently open witin the main window
	FuiBase *m_pFuiActive;	//last active FUI
	QHash<int,CommunicationMenu_Base*> m_hashSideBarCommMenu;

	QMap<int, QWidget *>    m_mapFuiWnd;	//maps FP to Fui holder Window 
	QMap<int, QTabWidget *> m_mapTabWnd;	//maps FP to Tab Widget (within the Fui holder window)
	QList<FuiInfo>			m_lstFUI;		//list of FUI details (in exact order as displayed in combo box)
	QList<QAction *>		m_lstFUIMenu;	//actions from the FUI menu

protected:
	QWidget			*m_pMainWnd;	
	QStackedWidget	*m_pWorkAreaWnd;	// placeholder window to display FUIs
	QAction			*m_pActClose;
	QAction			*m_pActMaximize;
	bool			m_bMaximized;
	QMenu			*m_pWindowMenu;
	bool			m_bOpenedAsAvatar;
private:
	int				m_nRecordID;
};

//overrides action to send its own pointer when triggered
class QActionEx : public QAction
{
	Q_OBJECT

public:
	QActionEx(const QString &text, QObject *parent = NULL) : QAction(text, parent){}

signals:
	void triggered_ex(QAction *action, bool bChecked); 

public slots:
	void OnTriggered(bool bChecked){ emit triggered_ex(this, bChecked);	}
};

#endif // FUIMANAGER_H



