#include "calendarviewcalendar.h"
#include <QBrush>
#include <QPainter>
#include <QDebug>

CalendarViewCalendar::CalendarViewCalendar(QWidget *parent)
	: QCalendarWidget(parent)
{

}

CalendarViewCalendar::~CalendarViewCalendar()
{

}

void CalendarViewCalendar::SetSelectionRange(QDate from, QDate to)
{
	m_StartDate=from;
	m_EndDate=to;
	setSelectedDate(from);
	//repaint();
	updateCells();
};

//get our private selection range and select dates
void CalendarViewCalendar::paintCell ( QPainter * painter, const QRect & rect, const QDate & date ) const
{
	if (m_StartDate.isValid() && m_EndDate.isValid())
	{
		if (date>=m_StartDate && date<=m_EndDate)
		{
			painter->save();
			//qDebug()<<"0";
			painter->fillRect(rect, Qt::cyan);
			//qDebug()<<"1";
			painter->setPen(Qt::black);
			//qDebug()<<"2";
			painter->drawText(rect, Qt::AlignCenter, QString::number(date.day()));
			//qDebug()<<"3";
			painter->restore();
			//qDebug()<<"4";
			
/*
			painter->setPen(QPen(Qt::cyan));
			painter->setBrush(QBrush(Qt::transparent));
			painter->drawRect(rect.adjusted(0, 0, -1, -1));
			//painter->drawText(rect, Qt::AlignCenter, QString::number(date.day()));

			QBrush brush = QBrush(Qt::SolidPattern);
			brush.setColor(Qt::cyan);
			painter->setBrush(brush);
			painter->fillRect(rect,brush);

*/			
			return;
		}
	}

	QCalendarWidget::paintCell(painter,rect,date);
}
