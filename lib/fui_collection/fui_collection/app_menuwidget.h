#ifndef APP_MENUWIDGET_H
#define APP_MENUWIDGET_H

#include <QWidget>
#include "generatedfiles/ui_app_menuwidget.h"
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QUrl>
#include "common/common/observer_ptrn.h"
#include "common/common/dbrecordset.h"


class App_MenuWidget : public QWidget, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	App_MenuWidget(QWidget *parent = 0,bool bIgnoreViewMode=false);
	~App_MenuWidget();

	//Qt::DropActions supportedDropActions() const;
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void SetDefaults(DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL,DbRecordSet *rowQCWContact=NULL);
	//void DisableHide(){ui.btnHide->setVisible(false);};
	//void ProcessDocumentDrop(QList<QUrl> &lst, bool bCreateTemplates=false,bool bAcceptApplications=false);
	void SetCloseOnSend();
	//bool IsHidden();
	//void SetHidden(bool bHide=true);
	void SetSideBarLayout();


signals:
	void NeedNewData(int nSubMenuType);		//emited always when new document is about to be created...to assign doc to contacts/projects later

private slots:
	//void OnMenuButton();
	//void OnDocumentTypeSelected(int nOperation,int nDocType);
	//void OnHideTab();

	//app:
	//void OnNewFromAppPie();
	void OnNewFromApp();
	void OnAddApp();
	void OnModifyApp();
	void OnRemoveApp();
	void OnUserPaths();

	//reload
	void LoadApplications(bool bReload=false);
	void ReLoadApplications(){LoadApplications(true);}
	void LoadTemplates(bool bReload=false);
	void ReLoadTemplates(){LoadTemplates(true);}

protected:
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent * event);

private:
	Ui::App_MenuWidgetClass ui;
	QTreeWidgetItem* SearchTree(QTreeWidget* pTree, QVariant nUserValue);
	bool WriteDocument(DbRecordSet &lstDocs,DbRecordSet &lstRevisions,bool bOpenDoc=false);

	DbRecordSet m_lstTemplates;
	DbRecordSet m_lstApplications;
	DbRecordSet m_lstContacts; 
	DbRecordSet m_lstProjects; 
	DbRecordSet m_rowQCWContactDef;					//default QCW list (must have at least 1 row
	int m_nLoggedID;
	bool m_bCloseOnSend;
	QPoint m_dragPosition;
};



#endif // APP_MENUWIDGET_H
