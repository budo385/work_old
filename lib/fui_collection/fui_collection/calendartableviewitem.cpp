#include "calendartableviewitem.h"

CalendarTableViewItem::CalendarTableViewItem(CalendarGraphicsView *pCalendarGraphicsView)
	: QGraphicsProxyWidget()
{
	setZValue(1);
}

CalendarTableViewItem::~CalendarTableViewItem()
{
	delete(m_pCalendarTableView);
	m_pCalendarTableView = NULL;
}

void CalendarTableViewItem::Initialize(QDate &startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites)
{
	m_pCalendarTableView->Initialize(startDate, nDayInterval, scale, recEntites);
}

void CalendarTableViewItem::setDateInterval(QDate &startDate, int nDayInterval)
{

}

void CalendarTableViewItem::setTimeScale(CalendarWidget::timeScale scale)
{

}

void CalendarTableViewItem::setPersonsHash(QHash<int, QString> hshPersons)
{

}

QRect CalendarTableViewItem::getItemRectangle(QDateTime &startDateTime, QDateTime &endDateTime)
{
	return QRect();
}

QRect CalendarTableViewItem::getItemRectangle(QModelIndex &Index)
{
	return QRect();

}

QRect CalendarTableViewItem::getItemRectangleForHeader(int nColumn)
{
	return QRect();

}

void CalendarTableViewItem::getSelectionStartEndDateTime(QDateTime &startDateTime, QDateTime &endDateTime)
{

}

CalendarTableModel* CalendarTableViewItem::Model()
{
	return dynamic_cast<CalendarTableModel*>(m_pCalendarTableView->Model());
}

void CalendarTableViewItem::on_resizeEvent()
{
	
}
