#include "qcw_base.h"
#include "common/common/entity_id_collection.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "bus_core/bus_core/formatphone.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;					//global access to Bo services
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;


QCW_Base::QCW_Base(QWidget *parent)
	:FuiBase(parent)
{
	InitFui(ENTITY_BUS_CONTACT);

	m_recFullContactData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

	m_nSelectedRowInLeftSelector = -1;
	m_strLockResourceID = QString();
	
	m_bSideBarModeView = true; //BT issue 1275???
	m_nEntityRecordID = -1;
	m_lstDebtor.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_DEBTOR));
}

QCW_Base::~QCW_Base()
{
	if (!m_strLockResourceID.isNull())
		UnlockContactNoRet(m_strLockResourceID);
}

bool QCW_Base::Initialize(DbRecordSet recContactData)
{	
	//Set widget mode.
	m_nQCWMode = MODE_MULTIPLE;
	return true;
}

bool QCW_Base::Initialize(int nEntityRecordID)
{
	//Set widget mode.
	m_nQCWMode = MODE_SINGLE;
	m_nEntityRecordID = nEntityRecordID;
	m_recFullContactData.clear();

	InitializeSingleContact(m_recFullContactData, nEntityRecordID, m_strLockResourceID);
	if (m_recFullContactData.getRowCount()==0)
	{		
		return false;
	}

	SetupFrames();
	return true;
}

void QCW_Base::GetDefaultValues(DbRecordSet &recContactData, int &nMode)
{
	recContactData=m_recFullContactData;
	nMode=m_nQCWMode; 
}

void QCW_Base::SetFUIStyle()
{
	FuiBase::SetFUIStyle();
	//return;
}

void QCW_Base::ClearAll()
{
	m_recFullContactData.clear();
	ContactTypeManager::AddEmptyRowToFullContactList(m_recFullContactData);
	m_lstDebtor.clear();
	m_lstDebtor.addRow();

	if (m_nEntityRecordID > 0)
		m_recFullContactData.setData(0, "BCNT_ID", m_nEntityRecordID);

	SetupFrames();
}

void QCW_Base::UnlockContact(QString &strLockResourceID)
{
	Status status;
	bool bUnlocked;
	_SERVER_CALL(ClientSimpleORM->UnLock(status, BUS_CM_CONTACT, bUnlocked, strLockResourceID));
	_CHK_ERR(status);
	strLockResourceID = QString();
}

void QCW_Base::UnlockContactNoRet(QString &strLockResourceID)
{
	Status status;
	bool bUnlocked;
	_SERVER_CALL(ClientSimpleORM->UnLock(status, BUS_CM_CONTACT, bUnlocked, strLockResourceID));
	_CHK_ERR_NO_RET(status);
	strLockResourceID = QString();
}

void QCW_Base::InitializeSingleContact(DbRecordSet &recFullContactData, int &nEntityRecordID, QString &strLockResourceID)
{
	if (nEntityRecordID <= 0)
	{
		recFullContactData.clear();
		ContactTypeManager::AddEmptyRowToFullContactList(recFullContactData);
		m_lstDebtor.clear();
		m_lstDebtor.addRow();
	}
	else
	{
		QString strWhere = " BCNT_ID = " + QVariant(nEntityRecordID).toString();
		recFullContactData.clear();

		//Lock.
		//Create locking recordset.
		DbRecordSet lockRecSet;
		lockRecSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT));
		lockRecSet.addRow();
		lockRecSet.setData(0, 0, nEntityRecordID);

		Status status;
		DbRecordSet lstStatusRows;
		_SERVER_CALL(ClientSimpleORM->Lock(status, BUS_CM_CONTACT, lockRecSet, strLockResourceID, lstStatusRows));
		_CHK_ERR_NO_RET(status);

		if (status.IsOK())
		{
			MainEntityFilter Filter;
			Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
			DbRecordSet recFilter(Filter.Serialize());

			//Get data for contacts.
			_SERVER_CALL(BusContact->ReadData(status, recFilter, recFullContactData,ContactTypeManager::USE_SUBDATA_DEBTOR));
			_CHK_ERR_NO_RET(status);

			if (recFullContactData.getRowCount()>0) //issue 2534:
				m_lstAddressOrganizationchangePropagate=recFullContactData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();

			m_lstDebtor=recFullContactData.getDataRef(0,"LST_DEBTOR").value<DbRecordSet>();
		}
		else
		{
			recFullContactData.clear();
			ContactTypeManager::AddEmptyRowToFullContactList(recFullContactData);
		}
	}
}

bool QCW_Base::OnOK(int &nOperation,DbRecordSet &lstUAR,DbRecordSet &lstGAR)
{
	DbRecordSet recTmp = m_recFullContactData;

	recTmp.Dump();

	if (m_nQCWMode == MODE_SINGLE)
	{
		Status status;
		CleanUpBeforeSave(status);
		_CHK_ERR_NO_RET(status);
		if(!status.IsOK())
		{
			m_recFullContactData = recTmp;
			return false;
		}
		//_CHK_ERR_RET_BOOL_ON_FAIL(status);

		if(!CheckDuplicates())
			return false;
	
		m_nEntityRecordID = m_recFullContactData.getDataRef(0, "BCNT_ID").toInt();
		if (m_nEntityRecordID<=0)
			nOperation=0; //insert
		else
			nOperation=1; //edit


		bool bGoForUpdateOfAddressAfterWrite=false;

		//issue 2534:
		if (nOperation && m_recFullContactData.getRowCount()>0)
		{
			if (m_recFullContactData.getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION)
			{
				//compare address fields:
				DbRecordSet lstAddress=m_recFullContactData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();

				//just compare 1st address:
				if (lstAddress.getRowCount()==1 && m_lstAddressOrganizationchangePropagate.getRowCount()==1)
				{
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_ORGANIZATIONNAME").toString()!=lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_ORGANIZATIONNAME_2").toString()!=lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME_2").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_STREET").toString()!=lstAddress.getDataRef(0,"BCMA_STREET").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_POBOX").toString()!=lstAddress.getDataRef(0,"BCMA_POBOX").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_POBOXZIP").toString()!=lstAddress.getDataRef(0,"BCMA_POBOXZIP").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_ZIP").toString()!=lstAddress.getDataRef(0,"BCMA_ZIP").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_CITY").toString()!=lstAddress.getDataRef(0,"BCMA_CITY").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_REGION").toString()!=lstAddress.getDataRef(0,"BCMA_REGION").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_COUNTRY_CODE").toString()!=lstAddress.getDataRef(0,"BCMA_COUNTRY_CODE").toString())
						bGoForUpdateOfAddressAfterWrite=true;
					if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_COUNTRY_NAME").toString()!=lstAddress.getDataRef(0,"BCMA_COUNTRY_NAME").toString())
						bGoForUpdateOfAddressAfterWrite=true;
				}
			}
		}

			

		//DbRecordSet lstDebtors=m_recFullContactData.getDataRef(0,"LST_DEBTOR").value<DbRecordSet>();
		//_DUMP(lstDebtors);

		//DbRecordSet lstAddress=m_recFullContactData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
		//DbRecordSet lstEmails=m_recFullContactData.getDataRef(0,"LST_EMAIL").value<DbRecordSet>();
		//DbRecordSet lstInternet=m_recFullContactData.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
		//DbRecordSet lstPhones=m_recFullContactData.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
		//_DUMP(lstAddress);
		//DbRecordSet tmp = lstAddress.getDataRef(0, "LST_TYPES").value<DbRecordSet>();
		//DbRecordSet tmp1 = lstAddress.getDataRef(1, "LST_TYPES").value<DbRecordSet>();
		//_DUMP(tmp);
		//_DUMP(tmp1);
		//_DUMP(lstEmails)
		//_DUMP(lstInternet)
		//_DUMP(lstPhones)
		//_DUMP(m_recFullContactData)
		//DbRecordSet lstUAR;
		//lstUAR.Dump();
		//lstGAR.Dump();

		_SERVER_CALL(BusContact->WriteData(status, m_recFullContactData,ContactTypeManager::USE_SUBDATA_DEBTOR,m_strLockResourceID,lstUAR,lstGAR));
		_CHK_ERR_RET_BOOL_ON_FAIL(status);

		m_strLockResourceID.clear();

		//issue 2534:
		if (bGoForUpdateOfAddressAfterWrite)
		{
			//find how many persons have same organization and have unlocked address???
			DbRecordSet lstAddressIDs;
			_SERVER_CALL(BusContact->GetAllContactsOfOrganizationByAddress(status, m_nEntityRecordID,m_lstAddressOrganizationchangePropagate,lstAddressIDs));
			_CHK_ERR_RET_BOOL_ON_FAIL(status);

			//ask user
			int nPersonsOfOrg=lstAddressIDs.getRowCount();
			if (nPersonsOfOrg>0)
			{
				QString strOrgName=m_recFullContactData.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString();
				int nResult=QMessageBox::question(this,tr("Warning"),QString(tr("You changed the address of %1. Do you want this change to be made automatically to %2 addresses of people in this organization?")).arg(strOrgName).arg(nPersonsOfOrg),tr("Yes"),tr("No"));
				if (nResult==0) 
				{
					//if ok, then go
					DbRecordSet lstAddress=m_recFullContactData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
					//_DUMP(lstAddress);
					_SERVER_CALL(BusContact->UpdateContactsOfOrganizationByAddress(status, lstAddressIDs, lstAddress));
					_CHK_ERR_RET_BOOL_ON_FAIL(status);
				}
			}
		}
	}
	
	return true;
}

void QCW_Base::OnCancel()
{
	if (!m_strLockResourceID.isNull())
		UnlockContact(m_strLockResourceID);

	close();
}

QString QCW_Base::GetFUIName()
{
	return tr("Contact Entry Assistant");
}

void QCW_Base::SalutationChanged(QString strRecordSetFieldName, QString strValue)
{
	DbRecordSet tmpAddress = m_recFullContactData.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

	int nRowCount = tmpAddress.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		tmpAddress.setData(i, strRecordSetFieldName, strValue);

	UpdateSalutationOnAdressFrame(strRecordSetFieldName, strValue);

	m_recFullContactData.setData(0, "LST_ADDRESS", tmpAddress);
}

void QCW_Base::LanguageChanged(QString strRecordSetFieldName, QString strValue)
{
	DbRecordSet tmpAddress = m_recFullContactData.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

	int nRowCount = tmpAddress.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		tmpAddress.setData(i, strRecordSetFieldName, strValue);

	//_DUMP(tmpAddress);
	m_recFullContactData.setData(0, "LST_ADDRESS", tmpAddress);
}

void QCW_Base::AddressChanged(int nCurrentRow, QString strRecordSetFieldName, QString strValue)
{
	DbRecordSet tmpAddress = m_recFullContactData.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

	tmpAddress.setData(nCurrentRow, strRecordSetFieldName, strValue);
	
	//_DUMP(tmpAddress);
	m_recFullContactData.setData(0, "LST_ADDRESS", tmpAddress);
}

void QCW_Base::AddressTypeChanged(int nCurrentRow, DbRecordSet rowTypes)
{
	DbRecordSet tmpAddress = m_recFullContactData.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

	tmpAddress.setData(nCurrentRow, "LST_TYPES", rowTypes);

	m_recFullContactData.setData(0, "LST_ADDRESS", tmpAddress);
}

void QCW_Base::AddressAdded(DbRecordSet recAddress)
{
	//Setup language and salutation for all the same.
	int nRowCount = recAddress.getRowCount();
	QString strSalutation;
	QString strShortSalutation;
	QString strLanguage;
	if (nRowCount > 1)
	{
		strSalutation = recAddress.getDataRef(0, "BCMA_SALUTATION").toString();
		strShortSalutation = recAddress.getDataRef(0, "BCMA_SHORT_SALUTATION").toString();
		strLanguage = recAddress.getDataRef(0, "BCMA_LANGUGAGE_CODE").toString();
	}

	for (int i = 0; i < nRowCount; i++)
	{
		recAddress.setData(i, "BCMA_SALUTATION", strSalutation);
		recAddress.setData(i, "BCMA_SHORT_SALUTATION", strShortSalutation);
		recAddress.setData(i, "BCMA_LANGUGAGE_CODE", strLanguage);
	}

	//Then set it to full recordset.
	m_recFullContactData.setData(0, "LST_ADDRESS", recAddress);
}

//BT refill with default data:
void QCW_Base::CleanUpBeforeSave(Status &status)
{
	FormatPhone m_PhoneFormatter;

	m_recFullContactData.setData(0,"BCNT_TYPE",ContactTypeManager::CM_TYPE_PERSON);			//set person as def
	if (m_recFullContactData.getDataRef(0,"BCNT_LASTNAME").toString().isEmpty() &&
		m_recFullContactData.getDataRef(0,"BCNT_FIRSTNAME").toString().isEmpty() &&
		m_recFullContactData.getDataRef(0,"BCNT_MIDDLENAME").toString().isEmpty() &&
		!m_recFullContactData.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString().isEmpty())
	{
		m_recFullContactData.setData(0,"BCNT_TYPE",ContactTypeManager::CM_TYPE_ORGANIZATION);	//org if names are empty and org is full
	}
	m_recFullContactData.setData(0,"BCNT_OWNER_ID",g_pClientManager->GetPersonID());			//owner is logged user


	//remove empty rows from emails/phones/internet/address
	DbRecordSet lstAddress;
	lstAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT));
	lstAddress.merge(m_recFullContactData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>());

	int nSize=lstAddress.getRowCount();
	for(int i=nSize-1;i>=0;--i)
	{
		if (lstAddress.getDataRef(i,"BCMA_FIRSTNAME").toString().isEmpty() &&
			lstAddress.getDataRef(i,"BCMA_LASTNAME").toString().isEmpty() &&
			lstAddress.getDataRef(i,"BCMA_MIDDLENAME").toString().isEmpty() &&
			lstAddress.getDataRef(i,"BCMA_ORGANIZATIONNAME").toString().isEmpty() &&
			lstAddress.getDataRef(i,"BCMA_FORMATEDADDRESS").toString().isEmpty())
		{
			lstAddress.deleteRow(i);
		}
		else
		{
			//clean duplicate types:
			DbRecordSet lstAddressType=lstAddress.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
			lstAddressType.removeDuplicates(lstAddressType.getColumnIdx("BCMAT_TYPE_ID"));
			lstAddress.setData(i,"LST_TYPES",lstAddressType);
		}
		//DbRecordSet lstAddressType=lstAddress.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
		//lstAddressType.Dump();

	}
	lstAddress.find("BCMA_IS_DEFAULT",1);
	if (lstAddress.getSelectedCount()!=1)
	{
		lstAddress.setColValue("BCMA_IS_DEFAULT",0);
		if (lstAddress.getRowCount()>0) //set 1st as default
		{
			lstAddress.setData(0,"BCMA_IS_DEFAULT",1);
		}
	}

	//lst address 
	DbRecordSet lstEmails;
	lstEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT));
	lstEmails.merge(m_recFullContactData.getDataRef(0,"LST_EMAIL").value<DbRecordSet>());

	nSize=lstEmails.getRowCount();
	for(int i=nSize-1;i>=0;--i)
	{
		if (lstEmails.getDataRef(i,"BCME_ADDRESS").toString().isEmpty() &&
			lstEmails.getDataRef(i,"BCME_DESCRIPTION").toString().isEmpty())
		{
			lstEmails.deleteRow(i);
		}
	}
	lstEmails.find("BCME_IS_DEFAULT",1);
	if (lstEmails.getSelectedCount()!=1)
	{
		lstEmails.setColValue("BCME_IS_DEFAULT",0);
		if (lstEmails.getRowCount()>0) //set 1st as default
		{
			lstEmails.setData(0,"BCME_IS_DEFAULT",1);
		}
	}

 	//lstEmails.Dump();
	DbRecordSet lstInternet;
	lstInternet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET_SELECT));
	lstInternet.merge(m_recFullContactData.getDataRef(0,"LST_INTERNET").value<DbRecordSet>());

	nSize=lstInternet.getRowCount();
	for(int i=nSize-1;i>=0;--i)
	{
		if (lstInternet.getDataRef(i,"BCMI_ADDRESS").toString().isEmpty() &&
			lstInternet.getDataRef(i,"BCMI_DESCRIPTION").toString().isEmpty())
		{
			lstInternet.deleteRow(i);
		}
	}
	lstInternet.find("BCMI_IS_DEFAULT",1);
	if (lstInternet.getSelectedCount()!=1)
	{
		lstInternet.setColValue("BCMI_IS_DEFAULT",0);
		if (lstInternet.getRowCount()>0) //set 1st as default
		{
			lstInternet.setData(0,"BCMI_IS_DEFAULT",1);
		}
	}

	DbRecordSet lstPhones;
	lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE_SELECT));
	lstPhones.merge(m_recFullContactData.getDataRef(0,"LST_PHONE").value<DbRecordSet>());

	nSize=lstPhones.getRowCount();
	for(int i=nSize-1;i>=0;--i)
	{
		if (lstPhones.getDataRef(i,"BCMP_FULLNUMBER").toString().isEmpty())//&&
			//lstPhones.getDataRef(i,"BCMP_LOCAL").toString().isEmpty()) //BT: removed, coz phone is not stored if this is active
		{
			lstPhones.deleteRow(i);
		}
	}

	lstPhones.find("BCMP_IS_DEFAULT",1);
	if (lstPhones.getSelectedCount()!=1)
	{
		lstPhones.setColValue("BCMP_IS_DEFAULT",0);
		if (lstPhones.getRowCount()>0) //set 1st as default
		{
			lstPhones.setData(0,"BCMP_IS_DEFAULT",1);
		}
	}



	//set phones:
	m_PhoneFormatter.ReFormatPhoneNumbers(lstPhones);

	m_recFullContactData.setData(0,"LST_EMAIL",lstEmails);
	m_recFullContactData.setData(0,"LST_PHONE",lstPhones);
	m_recFullContactData.setData(0,"LST_INTERNET",lstInternet);
	m_recFullContactData.setData(0,"LST_ADDRESS",lstAddress);

	//Check that last name or organization is inserted.
	if(m_recFullContactData.getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_PERSON)
	{
		//if(m_recFullContactData.getDataRef(0,"BCNT_LASTNAME").toString().isEmpty())
		//	status.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Last Name is mandatory field!"));
		
		if(m_recFullContactData.getDataRef(0,"BCNT_LASTNAME").toString().isEmpty() && m_recFullContactData.getDataRef(0,"BCNT_FIRSTNAME").toString().isEmpty())
		{status.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("You must enter at least Last Name or First Name of Contact!"));	return;	}

		//if(DataForWrite.getDataRef(0,"BCNT_FIRSTNAME").toString().isEmpty())
		//{Ret_pStatus.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("First Name is mandatory field!"));	return;	}
	}
	else //ORG:
	{
		if(m_recFullContactData.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString().isEmpty())
			status.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Organization Name is mandatory field!"));
	}





}


bool QCW_Base::CheckDuplicates()
{
	Status status;

	//set phonets:
	ContactTypeManager::SetPhonets(m_recFullContactData);
	Status Ret_pStatus;
	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_PHONETS));
	lstData.merge(m_recFullContactData);
	DbRecordSet ret_data;
	_SERVER_CALL(BusContact->FindDuplicates(status,lstData,ret_data));
	_CHK_ERR_RET_BOOL_ON_FAIL(status);
	if (ret_data.getRowCount()>0)
	{
		QString strMsg=tr("Contact already exists in database:")+" "+ret_data.getDataRef(0,"BCNT_NAME").toString()+". Continue with save?";
		int nResult=QMessageBox::question(NULL,tr("Warning"),strMsg,tr("Yes"),tr("No"));
		if (nResult==0) 
		{
			return false;
		}
	}

	return true;
}

void QCW_Base::CheckAdresses(DbRecordSet &lstDroppedData)
{
	//Take new addresses from dropped data list.
	DbRecordSet recNewAddresses	= lstDroppedData.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

	//Check old addresses - if formated address empty delete that row.
	DbRecordSet oldAddressesNotToDelete = m_recFullContactData.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
	oldAddressesNotToDelete.clearSelection();
	int nCount = oldAddressesNotToDelete.getRowCount();
	for (int i = 0; i < nCount; ++i)
	{
		if (oldAddressesNotToDelete.getDataRef(i, "BCMA_FORMATEDADDRESS").toString().isEmpty())
			oldAddressesNotToDelete.selectRow(i);
	}
	//Now delete rows with no formated address.
	oldAddressesNotToDelete.deleteSelectedRows();
	
	//If no rows remained return.
	if (oldAddressesNotToDelete.getRowCount() == 0)
		return;

	//Now merge old with new addresses and set it back to recordset.
	if (recNewAddresses.getRowCount())
		oldAddressesNotToDelete.merge(recNewAddresses);

	//Set address back to dropped data list.
	lstDroppedData.setData(0, "LST_ADDRESS", oldAddressesNotToDelete);
}

void QCW_Base::CheckEmail(DbRecordSet &lstDroppedData)
{
	//Take new emails from dropped data list.
	DbRecordSet recNewEmails = lstDroppedData.getDataRef(0, "LST_EMAIL").value<DbRecordSet>();

	//Check old emails - if empty delete that row.
	DbRecordSet oldEmailsNotToDelete = m_recFullContactData.getDataRef(0, "LST_EMAIL").value<DbRecordSet>();
	oldEmailsNotToDelete.clearSelection();
	int nCount = oldEmailsNotToDelete.getRowCount();
	for (int i = 0; i < nCount; ++i)
	{
		if (oldEmailsNotToDelete.getDataRef(i, "BCME_ADDRESS").toString().isEmpty())
			oldEmailsNotToDelete.selectRow(i);
	}
	//Now delete rows with no emails.
	oldEmailsNotToDelete.deleteSelectedRows();

	//If no rows remained return.
	if (oldEmailsNotToDelete.getRowCount() == 0)
		return;

	//Now merge old with new emails and set it back to recordset.
	if (recNewEmails.getRowCount())
		oldEmailsNotToDelete.merge(recNewEmails);

	//Set emails back to dropped data list.
	lstDroppedData.setData(0, "LST_EMAIL", oldEmailsNotToDelete);
}

void QCW_Base::CheckInternetAdresses(DbRecordSet &lstDroppedData)
{
	//Take new internet addresses from dropped data list.
	DbRecordSet recNewInternetAdresses = lstDroppedData.getDataRef(0, "LST_INTERNET").value<DbRecordSet>();

	//Check old internet addresses - if empty delete that row.
	DbRecordSet oldInternetAdressesNotToDelete = m_recFullContactData.getDataRef(0, "LST_INTERNET").value<DbRecordSet>();
	oldInternetAdressesNotToDelete.clearSelection();
	int nCount = oldInternetAdressesNotToDelete.getRowCount();
	for (int i = 0; i < nCount; ++i)
	{
		if (oldInternetAdressesNotToDelete.getDataRef(i, "BCMI_ADDRESS").toString().isEmpty())
			oldInternetAdressesNotToDelete.selectRow(i);
	}
	//Now delete rows with no internet addresses.
	oldInternetAdressesNotToDelete.deleteSelectedRows();

	//If no rows remained return.
	if (oldInternetAdressesNotToDelete.getRowCount() == 0)
		return;

	//Now merge old with new internet addresses and set it back to recordset.
	if (recNewInternetAdresses.getRowCount())
		oldInternetAdressesNotToDelete.merge(recNewInternetAdresses);

	//Set internet addresses back to dropped data list.
	lstDroppedData.setData(0, "LST_INTERNET", oldInternetAdressesNotToDelete);
}

void QCW_Base::CheckPhones(DbRecordSet &lstDroppedData)
{
	//Take new emails from dropped data list.
	DbRecordSet recNewPhones = lstDroppedData.getDataRef(0, "LST_PHONE").value<DbRecordSet>();
	//Check old emails - if empty delete that row.
	DbRecordSet oldPhonesNotToDelete = m_recFullContactData.getDataRef(0, "LST_PHONE").value<DbRecordSet>();
	
	//If new exist override old (if it exist).
	//Business central phone.
	int nRowNew = recNewPhones.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), true);
	int nRowOld = oldPhonesNotToDelete.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), true);
	if (nRowNew>=0)
	{
		if (nRowOld>=0)
			oldPhonesNotToDelete.setData(nRowOld, "BCMP_FULLNUMBER", recNewPhones.getDataRef(nRowNew, "BCMP_FULLNUMBER").toString());
		else
			oldPhonesNotToDelete.merge(recNewPhones.getRow(nRowNew));
	}

	//Business direct phone.
	nRowNew = recNewPhones.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT), true);
	nRowOld = oldPhonesNotToDelete.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT), true);
	if (nRowNew>=0)
	{
		if (nRowOld>=0)
			oldPhonesNotToDelete.setData(nRowOld, "BCMP_FULLNUMBER", recNewPhones.getDataRef(nRowNew, "BCMP_FULLNUMBER").toString());
		else
			oldPhonesNotToDelete.merge(recNewPhones.getRow(nRowNew));
	}

	//Business mobile phone.
	nRowNew = recNewPhones.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), true);
	nRowOld = oldPhonesNotToDelete.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), true);
	if (nRowNew>=0)
	{
		if (nRowOld>=0)
			oldPhonesNotToDelete.setData(nRowOld, "BCMP_FULLNUMBER", recNewPhones.getDataRef(nRowNew, "BCMP_FULLNUMBER").toString());
		else
			oldPhonesNotToDelete.merge(recNewPhones.getRow(nRowNew));
	}

	//Fax.
	nRowNew = recNewPhones.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), true);
	nRowOld = oldPhonesNotToDelete.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), true);
	if (nRowNew>=0)
	{
		if (nRowOld>=0)
			oldPhonesNotToDelete.setData(nRowOld, "BCMP_FULLNUMBER", recNewPhones.getDataRef(nRowNew, "BCMP_FULLNUMBER").toString());
		else
			oldPhonesNotToDelete.merge(recNewPhones.getRow(nRowNew));
	}

	//Private phone.
	nRowNew = recNewPhones.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), true);
	nRowOld = oldPhonesNotToDelete.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), true);
	if (nRowNew>=0)
	{
		if (nRowOld>=0)
			oldPhonesNotToDelete.setData(nRowOld, "BCMP_FULLNUMBER", recNewPhones.getDataRef(nRowNew, "BCMP_FULLNUMBER").toString());
		else
			oldPhonesNotToDelete.merge(recNewPhones.getRow(nRowNew));
	}

	//Private mobile phone.
	nRowNew = recNewPhones.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), true);
	nRowOld = oldPhonesNotToDelete.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), true);
	if (nRowNew>=0)
	{
		if (nRowOld>=0)
			oldPhonesNotToDelete.setData(nRowOld, "BCMP_FULLNUMBER", recNewPhones.getDataRef(nRowNew, "BCMP_FULLNUMBER").toString());
		else
			oldPhonesNotToDelete.merge(recNewPhones.getRow(nRowNew));
	}
	
	//Skype.
	nRowNew = recNewPhones.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), true);
	nRowOld = oldPhonesNotToDelete.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), true);
	if (nRowNew>=0)
	{
		if (nRowOld>=0)
			oldPhonesNotToDelete.setData(nRowOld, "BCMP_FULLNUMBER", recNewPhones.getDataRef(nRowNew, "BCMP_FULLNUMBER").toString());
		else
			oldPhonesNotToDelete.merge(recNewPhones.getRow(nRowNew));
	}
	
	//Set internet addresses back to dropped data list.
	lstDroppedData.setData(0, "LST_PHONE", oldPhonesNotToDelete);
}
