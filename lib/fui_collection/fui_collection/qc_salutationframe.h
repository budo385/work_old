#ifndef QC_SALUTATIONFRAME_H
#define QC_SALUTATIONFRAME_H

#include <QWidget>
#include "ui_qc_salutationframe.h"
#include "common/common/dbrecordset.h"

class qc_salutationframe : public QWidget
{
	Q_OBJECT

public:
	qc_salutationframe(QWidget *parent = 0);
	~qc_salutationframe();

	void Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType = 0 /*0-Large,1-small widget*/);

private:
	Ui::qc_salutationframeClass ui;

	void SetupGUIwidgets(DbRecordSet recData);
	void SetDataToRecordSet(QString strRecordSetFieldName, QString strValue);

	DbRecordSet *m_recFullContactData;
	DbRecordSet m_recLocalEntityRecordSet;
	DbRecordSet m_recAddressTypes;
	int			m_nParentType;
	QWidget		*m_pParentWidget;

private slots:
	void on_salutation_comboBox_editTextChanged(const QString &text);
	void on_shortsalutation_comboBox_editTextChanged(const QString &text);
	void on_addSelected_toolButton_clicked();
	void on_addSelected_toolButton_2_clicked();
};

#endif // QC_SALUTATIONFRAME_H
