#ifndef FUI_BUSPERSON_H
#define FUI_BUSPERSON_H

#include <QWidget>
#include "generatedfiles/ui_fui_busperson.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"

/*!
	\class  FUI_BusPerson
	\brief  FUI Business Person
	\ingroup FUI_Collection
*/
class FUI_BusPerson : public FuiBase
{
    Q_OBJECT

public:
    FUI_BusPerson(QWidget *parent = 0);
    ~FUI_BusPerson();

	bool on_cmdOK();
	void on_cmdInsert();
	void on_cmdCopy();
	bool on_cmdEdit();
	bool on_cmdDelete();
	void on_selectionChange(int nEntityRecordID);	//note: if 0 then it will invalidate FUI: go empty mode
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	QString GetFUIName();

protected:
	void DataWrite(Status &err);
	void DataPrepareForInsert(Status &err);
	void DataPrepareForCopy(Status &err);
	void DataClear();
	void DataDefine();
	void DataRead(Status &err,int nRecordID);
	void DataUnlock(Status &err);
	void DataCheckBeforeWrite(Status &err);
	void SetFUIStyle();

private:
    Ui::FUI_BusPersonClass ui;
	//Gui maanger:
	GuiFieldManager *m_GuiFieldManagerMain;
	GuiFieldManager *m_GuiFieldManagerUser;
	void prepareCacheRecord(DbRecordSet* recNewData);
	void SetFiltersForCurrentActualOrganization(int nActualOrganizationID);
	
	void RefreshDisplay();
	void SetMode(int nMode);

private slots:
	void on_btnSwitchToUser_clicked();
	void on_groupBoxPassword_toggled(bool);
	void on_btnGeneratePassword_clicked();
	void on_txtPassword2_editingFinished();
	void on_txtPassword1_editingFinished();
	void OnContactSAPNEInsert();
	void on_ckbShowAll_stateChanged(int);
	void on_btnPersonalSettings_clicked();

private:

	DbRecordSet m_lstCoreUser;
	DbRecordSet m_lstCoreUserCache;
	//passwords:
	void LoadPassword();
	void ClearPassword();
	bool CheckPassword();
	void LoadStoredLists();
	bool	m_bPassWordChanged;
	int		m_nPasswordIdx;
	QString m_strEdition;
	int		m_nAdminRoleID;

	MainEntityCalculatedName m_CalcName;
};

#endif // FUI_BUSPERSON_H

