#include "calendarmultydaydisplaywidget.h"

#include <QPainter>
#include <QEvent>
#include <QHelpEvent>
#include <QToolTip>
#include <QHeaderView>
#include <QVBoxLayout>
#include "calendarentitydisplaywidget.h"

TableMultiDayDisplayWidget::TableMultiDayDisplayWidget(QGraphicsView *pCalendarMultiDayGraphicsView, int rows, int columns, QWidget *parent /*= 0*/)
:QTableWidget(rows, columns, parent)
{
	m_pCalendarMultiDayGraphicsView = pCalendarMultiDayGraphicsView;
}

bool TableMultiDayDisplayWidget::viewportEvent(QEvent *event)
{
	if (event->type() == QEvent::ToolTip) 
	{
		QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);

		QString strToolTip = QString("<html><body style=\" font-family:Arial; text-decoration:none;\">");
		for (int i=0; i<rowCount();i++)
		{
			//If in area for general tooltip, display general tooltip.
			if (i==(rowCount()-1))
			{
//				QTableWidgetItem *ii=item(i, 0);
				if (!dynamic_cast<QTextEdit*>(cellWidget(i, 0))->toPlainText().isEmpty())
					strToolTip+=dynamic_cast<QTextEdit*>(cellWidget(i, 0))->toHtml();
				//if (!item(i, 0)->data(Qt::DisplayRole).toString().isEmpty())
//					strToolTip+=QString("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; \">%1</span></p>").arg(item(i, 0)->data(Qt::DisplayRole).toString());
			}
			else
				strToolTip+=item(i, 0)->data(Qt::UserRole).toString();
		}
		strToolTip+=QString("</body></html>");
		QPoint pos = helpEvent->globalPos();
		QToolTip::showText(pos, strToolTip);
		return true;
	} 
	else
		return QTableWidget::viewportEvent(event);
}

CalendarMultyDayDisplayWidget::CalendarMultyDayDisplayWidget(int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType, 
		QDateTime startDateTime, QDateTime endDateTime, QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, 
		QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
		CalendarMultiDayGraphicsView *pCalendarMultiDayGraphicsView, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed, 
		QWidget *parent /*= 0*/)
	: QFrame(parent)
{
	m_pCalendarGraphicsView = pCalendarMultiDayGraphicsView;
	m_bSelected = false;

	QString strToolTip;
	if (!strTitle.isEmpty())
		strToolTip+=QString("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; \">%1</span></p>").arg(strTitle);
	if (!strLocation.isEmpty())
		strToolTip+=QString("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; \">%1</span></p>").arg(strLocation);
	//Don't show owner in tooltip.
	//if (!strOwner.isEmpty())
	//	strToolTip+=QString("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; \">%1</span></p>").arg(strOwner);
	if (!strResource.isEmpty())
		strToolTip+=QString("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; \">%1</span></p>").arg(strResource);
	if (!strContact.isEmpty())
		strToolTip+=QString("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; \">%1</span></p>").arg(strContact);
	if (!strProjects.isEmpty())
		strToolTip+=QString("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; \">%1</span></p>").arg(strProjects);

	
	m_item1 = NULL;
	m_pCalendarGraphicsView = pCalendarMultiDayGraphicsView;
	if(!strColor.isEmpty())
	{
		m_typeColor = QColor(strColor);
		strColor = strColor.prepend("'");
		strColor = strColor.append("'");
	}
	else
	{
		strColor = "white";
		m_typeColor = QColor(Qt::white);
	}

	m_strColor = strColor;

	//Title row will alway be.
	m_item1 = new QTableWidgetItem();
	m_item1->setData(Qt::UserRole, strToolTip);

	//Description row.
	m_item2 = new QTextEdit();
	m_item2->setHtml(strDescription);

	//Create table widget.
	m_table = new TableMultiDayDisplayWidget(m_pCalendarGraphicsView, 2, 1);
	m_table->horizontalHeader()->hide();
	m_table->horizontalHeader()->setStretchLastSection(true);
	m_table->verticalHeader()->setStretchLastSection(true);
	m_table->verticalHeader()->setMinimumSectionSize(6);
	m_table->verticalHeader()->hide();
	m_table->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
	m_table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_table->setShowGrid(false);
	m_table->setSelectionMode(QAbstractItemView::NoSelection);
	m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_strBorderStyle = "solid";
	m_strBorderColor = "gray";
	m_strBorderWidth = "1";
	if (nBcep_Status == GlobalConstants::TYPE_CAL_PRELIMINARY)
	{
		m_strBorderStyle = "dashed";
		m_strBorderWidth = "1.3";
	}
	else if (nBcep_Status == GlobalConstants::TYPE_CAL_CANCELLED)
	{
		m_strBorderStyle = "dashed";
		m_strBorderColor = "red";
		m_strBorderWidth = "1.3";
	}
	else if (bIsProposed)
	{
		m_strBorderStyle = "dotted";
		m_strBorderWidth = "1.3";
	}
	else if (m_bSelected)
	{
		m_strBorderColor = "red";
	}
	else
	{
		m_strBorderStyle = "solid";
	}

	m_strTableStyle = QString(" QTableWidget {border: %1 %2 %3; border-radius: 0px; background: %4;}");
	QString strTableStyle = m_strTableStyle.arg(m_strBorderWidth).arg(m_strBorderStyle).arg(m_strBorderColor).arg(m_strColor);
	m_table->setStyleSheet(strTableStyle);

	//Set items in table.
	m_table->setItem(0,0,m_item1);
	m_table->setCellWidget(1,0,m_item2);

	QVBoxLayout *layout = new QVBoxLayout();
	layout->setSizeConstraint(QLayout::SetMaximumSize);
	layout->setContentsMargins(0,0,0,0);
	layout->setSpacing(0);
	layout->addWidget(m_table);
	setLayout(layout);
	setMinimumHeight(6);
	setMaximumHeight(6);
}

CalendarMultyDayDisplayWidget::~CalendarMultyDayDisplayWidget()
{

}

void CalendarMultyDayDisplayWidget::SetSelected(bool bSelected /*= true*/)
{
	m_bSelected = bSelected;
	
	QString strTableStyle;
	if (m_bSelected)
		strTableStyle = m_strTableStyle.arg("3").arg(m_strBorderStyle).arg("red").arg(m_strColor);
	else
		strTableStyle = m_strTableStyle.arg(m_strBorderWidth).arg(m_strBorderStyle).arg(m_strBorderColor).arg(m_strColor);
	
	m_table->setStyleSheet(strTableStyle);
}
