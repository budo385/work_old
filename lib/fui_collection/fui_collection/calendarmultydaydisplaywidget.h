#ifndef CALENDARMULTYDAYDISPLAYWIDGET_H
#define CALENDARMULTYDAYDISPLAYWIDGET_H

#include <QtWidgets/QFrame>
#include <QScrollArea>
#include <QIcon>
#include <QTextEdit>
#include "calendarmultidaygraphicsview.h"

class TableMultiDayDisplayWidget : public QTableWidget
{
	Q_OBJECT

public:
	TableMultiDayDisplayWidget(QGraphicsView *pCalendarMultiDayGraphicsView, int rows, int columns, QWidget *parent = 0);

protected:
	bool viewportEvent(QEvent *event);

private:
	QGraphicsView *m_pCalendarMultiDayGraphicsView;
};

class CalendarMultyDayDisplayWidget : public QFrame
{
	Q_OBJECT

public:
	CalendarMultyDayDisplayWidget(int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType, QDateTime startDateTime, QDateTime endDateTime, 
		QString strTitle, QString strLocation, QString strColor, 
		QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
		CalendarMultiDayGraphicsView *pCalendarMultiDayGraphicsView, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed, 
		QWidget *parent = 0);
	~CalendarMultyDayDisplayWidget();

	void SetSelected(bool bSelected = true);

private:
	QColor			  m_typeColor;
	QTableWidgetItem *m_item1;
	QTextEdit *m_item2;
	TableMultiDayDisplayWidget *m_table;
	CalendarMultiDayGraphicsView *m_pCalendarGraphicsView;
	bool			  m_bSelected;
	QString			  m_strTableStyle;
	QString			  m_strBorderStyle;
	QString			  m_strBorderColor;
	QString			  m_strBorderWidth;
	QString			  m_strColor;
};

#endif // CALENDARMULTYDAYDISPLAYWIDGET_H
