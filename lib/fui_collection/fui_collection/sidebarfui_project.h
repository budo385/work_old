#ifndef SIDEBARFUI_PROJECT_H
#define SIDEBARFUI_PROJECT_H

#include "sidebarfui_base.h"
#include "generatedfiles/ui_sidebarfui_project.h"
#include "common/common/observer_ptrn.h"
#include "gui_core/gui_core/styledpushbutton.h"

class SideBarFui_Project : public SideBarFui_Base, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	SideBarFui_Project(QWidget *parent = 0);
	~SideBarFui_Project();

	void SetCurrentFUI(bool bCurrent);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	QWidget* GetSelector(){return ui.frameProject;};
	bool HideVisibleFramesIfNeeded(int nFrameOpened /* 0 -ACL, 1 -AC, 2-DP */);
	void ShowActualFrameDetails(int nTabIdx);
	void OpenCurrentContactInNewWindow();

protected:
	QFrame* GetFrameCommToolBar();
	QFrame* GetFrameCommParent();
	QLabel* GetFrameCommLabel();
	DropZone_MenuWidget* GetDropZone_MenuWidget();
	void GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strEmail,int &nEmailReplyID,QString &strPhone,QString &strInternet);
	void ShowActualFrame(bool bShow,bool bSkipAutoCollapseOthers=false,bool bSkipInsertSpacer=false);
	void ShowActualList(bool bShow,bool bSkipAutoCollapseOthers=false,bool bSkipInsertSpacer=false);

public slots:
	void OnDetail_CalendarClick();

protected slots:
	void BuildMenu();
	void OnDropZone_NewDataDropped(int nDropType, DbRecordSet lstDroppedData);
	void OnScrollToCurrentItem();

private slots:

	void ReloadProjectData(bool bReloadOtherData=true);
	void CopyName2Clipboard();

	void OnHideList();
	void OnHideActual();

	void OnList_InsertClick();
	void OnList_EditClick();
	void OnList_DeleteClick();
	void OnList_ViewClick();
	void OnList_NewStructFromTemplate();
	void OnList_NewFromTemplate();
	void OnList_PrintClick();

	void OnDetail_QuickInfoClick();
	void OnDetail_CommGridClick();
	void OnDetail_NMRXUsersClick();
	void OnCustomContextMenu(const QPoint &);
	void OnSetFindFocusDelayed();

private:
	void SetActualText(QString strText);
	void RefreshQuickInfo();
	void RefreshActualName();
	void OnSelector_DoubleClick();
	void OnSelector_ShowContactDetails(bool bNewWindow=false);
	void OnThemeChanged();

	DbRecordSet m_RowProjectData;
	bool m_bShowPicture;
	QLabel *m_pTxt1;
	QLabel *m_pTxt2;
	StyledPushButton *m_btnHideList;
	StyledPushButton *m_btnHideActual;
	QLabel *m_pTxtActual;
	bool m_bHideContactPaneOnDetailClickFirstTime;
	QAction *m_pCntxReload;
	QAction *m_pCntxCopy;
	QLayoutItem *m_Spacer;
	bool m_bCommGridInit;
	Ui::SideBarFui_ProjectClass ui;
};

#endif // SIDEBARFUI_PROJECT_H
