#ifndef CALENDARWIDGET_H
#define CALENDARWIDGET_H

#include <QWidget>
#include <QSplitter>
#include <QGraphicsView>
#include <QDateTime>

#include <QDate>
#include <QAbstractTableModel>
#include <QGraphicsProxyWidget>
#include <QTableView>
#include <QtWidgets/QTableWidget>
#include <QStackedLayout>

#include "common/common/dbrecordset.h"
#include "bus_core/bus_core/globalconstants.h"

class TopRightItem : public QGraphicsProxyWidget
{
	Q_OBJECT
public:
protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
private:
signals:
	void ResizeColumnsToFit(bool bForce);
};

class TableView : public QTableView
{
	Q_OBJECT

public:
	TableView(int nItemLeftOffSet, bool bIsHeaderTable = false, QWidget *parent = 0);
	void ItemIsResizing(bool bItemIsResizing){m_bItemIsResizing = bItemIsResizing;};
protected:
	void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
	void mouseDoubleClickEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

private slots:

private:
	bool m_bItemIsResizing;
	bool m_bStartSelection;
	QModelIndex m_topLeftSelectedIndex;
	QModelIndex m_bottomRightSelectedIndex;
	int m_nItemLeftOffSet;
	bool m_bIsHeaderTable;

signals:
	void SelectionChanged(QModelIndex topLeftSelectedIndex, QModelIndex bottomRightSelectedIndex);
	void openMultiDayEvent(QModelIndex index);
};

class CalendarWidget : public QWidget
{
	Q_OBJECT

public:
	enum timeScale
	{
		HOUR		= 1,
		HALF_HOUR	= 2,
		QUATER_HOUR	= 4,
		TEN_MINUTES	= 6,
		SIX_MINUTES	= 10,
		FIVE_MINUTES= 12
	};

	CalendarWidget(QWidget *parent = 0);
	~CalendarWidget();

	void Initialize(QDate startDate, QDate endDate, CalendarWidget::timeScale scale, DbRecordSet *recEntites, bool bResizeColumnsToFit = true, QTime scrollToTime = QTime(6,0), int nCalendarViewSections = 0);
	
	int		getVertHeaderWidthForUpperCalendar();
	int		getVertHeaderWidthForLowerCalendar();
	void	setVertHeaderWidth(int nVertHeaderWidth){m_nVertHeaderWidth = nVertHeaderWidth;};
	//QHash<int, QString> getPersonsHash(){return m_hshEntityes;};
	int		getPersonHeaderHeight();
	int		getDateHeaderHeight();
	QDate	getStartDate();
	int		getDateInterval();
	int		getMultiHeaderHeight();
	int		getCurrentMultiHeaderHeight();
	CalendarWidget::timeScale getTimeScale();
	QAbstractTableModel* TableModel();
	QAbstractTableModel* HeaderTableModel();
	int		getRowHeight(){return m_nRowHeight;}
	int		getRowLeftOffset(){return m_nItemLeftOffSet;};
	int		getCalendarColumnWidth();
	int		getMultiDayCalendarColumnWidth();

	void	setTimeScale(int nTimeScale);
	int		getEntityCount(){return m_recEntites->getRowCount();};

	QDateTime GetTopLeftVisibleItem();
	void SetTopLeftVisibleItem(QDateTime dateTime);

	void UpdateCalendarFromFui();
	void SelectEntityItems(int nEntityID, int nEntityType);

	//Splitter.
	QByteArray	GetSpliterState();
	void		SetSpliterState(QByteArray spliterState);
	int			getCalendarViewSections(){return m_nCalendarViewSections;};

public slots:
	void AddCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, QDateTime startDateTime, QDateTime endDateTime, 
						 QString strTitle, QString strLocation, QString strColor, 
						 QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
						 QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, bool bForInsert, int nBcep_Status, 
						 int nBCEV_IS_INFORMATION, bool bIsProposed, DbRecordSet recItemAssignedResources);
	void UpdateCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime startDateTime, QDateTime endDateTime, QString strTitle, 
						QString strLocation, QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, 
						QString strProjects, QString strDescription, QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, 
						int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed, DbRecordSet recItemAssignedResources);
	void AddMultiDay(bool bFromSingleDayEvent, int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType, QDateTime startDateTime, 
						QDateTime endDateTime);
	void RemoveMultiDay(int nCentID, int nBcepID, int nBcevID);
	void UpdateMultiDay(int nCentID, int nBcepID, int nBcevID, QDateTime startDateTime, QDateTime endDateTime);
	void ReorderMultiDay();
	void AddCalendarReservation(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, QDateTime startDateTime, 
								QDateTime endDateTime);
	void RemoveSingleDayCalendarItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID);
	void RemoveMultiDayEventItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID);
	void AddMultiDayCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, QDateTime startDateTime, 
							QDateTime endDateTime, QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, 
							QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, bool bForInsert, 
							int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed, DbRecordSet recItemAssignedResources);
	void AddCalendarTaskItems(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, QPixmap Pixmap, int nEntityID, int nEntityType, int nTypeID);
	void getSelectedItemDateTime(QDateTime &startDateTime, QDateTime &endDateTime);
	void on_ItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, 
						int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void on_MultiDayItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, 
		int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void on_DeleteItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType);
	void on_DeleteItemMultyDay(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void on_ModifyItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void on_ViewItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void on_tableCellDoubleClicked(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType);
	void on_openMultiDayEvent(QModelIndex index);
	void on_splitterMoved(int pos, int index);
	void on_defineAvailability(DbRecordSet recSelectedEntityItemsInLeftColumn);
	void on_markAsFinal(DbRecordSet recSelectedEntityItemsInLeftColumn);
	void on_markAsPreliminary(DbRecordSet recSelectedEntityItemsInLeftColumn);
	void on_dropEntityOnTable(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nDroppedEntityID, 
		int nDroppedEntityType, int nDroppedEntityPersonID, QDateTime datSelectionFrom, QDateTime datSelectionTo);

private:
	void GetCorrectedDateTimes(QDateTime &correctedStartDateTime, QDateTime &correctedEndDateTime);
	bool IsEntityInItem(int nEntityID, int nEntityType, DbRecordSet recItemAssignedResources);

	QGraphicsView			 *m_pCalendarGraphicsView;
	QGraphicsView			 *m_pCalendarMultiDayGraphicsView;
	QSplitter				 *m_pSplitter;	
	QBoxLayout				 *m_pLayout;

	CalendarWidget::timeScale m_timeScale;
	QDate					 m_startDate;
	int						 m_nDayInterval;
	DbRecordSet				 *m_recEntites;		//Entities recordset.
	DbRecordSet				 m_recMultiDayEvents;		//Entities recordset.
	QHash<QGraphicsItem*, DbRecordSet> m_hshItemToResources;
	QHash<QGraphicsItem*, DbRecordSet> m_hshMultiDayItemToResources;
	QByteArray				 m_splitterState;

	int						 m_nHeaderHeight;
	int						 m_nVertHeaderWidth;
	int						 m_nPersonHeaderHeight;
	int						 m_nColumnWidth;
	int						 m_nRowHeight;
	int						 m_nItemLeftOffSet;
	int						 m_nCalendarViewSections;	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER.

signals:
	void MultiDayItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void ItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void MultiDayItemMoved(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void ItemMoved(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void DeleteItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType);
	void MultiDayDeleteItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void ModifyItem(int nItemID, int nBcepID, int nBcevID, int nBCOL_ID);
	void ViewItem(int nItemID, int nBcepID, int nBcevID, int nBCOL_ID);
	void DropEntityOnItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, bool bDroppedOnFirstRow);
	void tableCellDoubleClicked(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType);
	void selectionChanged(QDateTime datSelectionFrom, QDateTime datSelectionTo, DbRecordSet recSelectedEntities);
	void openMultiDayEvent(QHash<int, int> hshMultiDayCentIDs, QHash<int, int> hshMultiDayBcepIDs, QHash<int, int> hshMultiDayBcevIDs);
	void ItemsDelete(DbRecordSet recSelectedItemsData);
	void TaskItemClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID);
	void TaskItemDoubleClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID);
	void defineAvailability(DbRecordSet recSelectedEntityItemsInLeftColumn);
	void markAsFinal(DbRecordSet recSelectedEntityItemsInLeftColumn);
	void markAsPreliminary(DbRecordSet recSelectedEntityItemsInLeftColumn);
	void dropEntityOnTable(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nDroppedEntityID, int nDroppedEntityType, int nDroppedEntityPersonID, QDateTime datSelectionFrom, QDateTime datSelectionTo);
};

#endif // CALENDARWIDGET_H
