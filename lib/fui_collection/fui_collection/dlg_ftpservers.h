#ifndef DLG_FTPSERVERS_H
#define DLG_FTPSERVERS_H

#include <QtWidgets/QDialog>
#include "ui_dlg_ftpservers.h"
#include "common/common/dbrecordset.h"
#include <QListWidgetItem>

class Dlg_FTPServers : public QDialog
{
	Q_OBJECT

public:
	Dlg_FTPServers(QWidget *parent = 0);
	~Dlg_FTPServers();

	bool Initialize(QString strInitial); //if failed, never open
	void GetServers(DbRecordSet &lstData, int &nCurrent){lstData=m_lstData;nCurrent=m_nCurrRow;}

private slots:
	void OnItemChanged(QListWidgetItem * current, QListWidgetItem * previous);
	void on_chkUseProxy_stateChanged(int);
	void on_btnSave_clicked();
	void on_btnDelete_clicked();
	void on_btnNew_clicked();
	void on_btnCancel_clicked();
	void on_btnTest_clicked();

private:
	
	void SaveData(int nRow);
	void RefreshDisplay();
	void EnableProxyFields(bool bEnable = true);

	Ui::Dlg_FTPServersClass ui;
	DbRecordSet m_lstData;
	DbRecordSet m_lstDataDeleted;
	int m_nCurrRow;
	QString m_strLockedRes;
	
};

#endif // DLG_FTPSERVERS_H
