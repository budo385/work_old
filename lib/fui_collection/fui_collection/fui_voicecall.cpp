#include "fui_voicecall.h"

#include "fui_voicecallcenter.h"
#include "gui_core/gui_core/pie_menu/qtpiemenu.h"
#include "gui_core/gui_core/gui_helper.h"
#include "document_piemenu.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_core/bus_core/commgridviewhelper.h"
#include "common/common/datahelper.h"

//GLOBALS:
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "fuimanager.h"
extern FuiManager g_objFuiManager;	

FUI_VoiceCall::FUI_VoiceCall(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui(ENTITY_BUS_VOICECALLS,BUS_VOICECALLS,NULL, NULL); //FUI base

	//build menu:
	m_pPieMenu = new QtPieMenu("Root menu", this, "Root menu");
	m_pPieMenu->insertItem(QIcon(":Email32.png"),tr("Email"),this,SLOT(OnScheduleEmail()));
	m_pPieMenu->insertItem(QIcon(":Phone32.png"),tr("Call"),this,SLOT(OnScheduleCall()));
	m_pPieMenu->insertItem(QIcon(":Document32.png"),tr("Document"),this,SLOT(OnScheduleDoc()));
	m_pPieMenu->setOuterRadius(100);

	ui.btnStartTimeSetCurrent->setIcon(QIcon(":clock.png"));
	ui.btnEndTimeSetCurrent->setIcon(QIcon(":clock.png"));

	//define list:
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	m_recVoiceLogData.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_VOICECALLS));
	m_recVoiceLogData.copyDefinition(set, false);
	m_recVoiceLogData.addRow();

	//init voice call log 'FUI'
	ui.frameUser->Initialize( ENTITY_BUS_PERSON, &m_recVoiceLogData,"CENT_OWNER_ID" );

	ui.frameContact->Initialize( ENTITY_BUS_CONTACT,&m_recVoiceLogData, "BVC_CONTACT_ID" );
	ui.frameContact->EnableInsertButton();

	ui.frmSelectedProject->Initialize( ENTITY_BUS_PROJECT, &m_recVoiceLogData,"BVC_PROJECT_ID" );

	ui.frmType->Initialize( ENTITY_CE_TYPES,&m_recVoiceLogData, "CENT_CE_TYPE_ID" );
	ui.frmType->GetSelectionController()->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",GlobalConstants::CE_TYPE_VOICE_CALL);

	GUI_Helper::SetButtonTextColor(ui.btnSheduleCall, QColor(255,255,255));
	GUI_Helper::SetWidgetFontWeight(ui.btnSheduleCall, QFont::DemiBold);
	GUI_Helper::SetStyledButtonColorBkg(ui.btnSheduleCall, GUI_Helper::BTN_COLOR_GREEN_1);
	ui.btnSheduleCall->setIcon(QIcon(":piemenu.png"));
}

FUI_VoiceCall::~FUI_VoiceCall()
{
}

QString FUI_VoiceCall::GetFUIName()
{
	return tr("Phone Call");
}

void FUI_VoiceCall::RefreshDisplay()
{
	if(m_recVoiceLogData.getDataRef(0, "BVC_DIRECTION").toInt() == 1)
		ui.radIncoming->setChecked(1);
	else if(m_recVoiceLogData.getDataRef(0, "BVC_DIRECTION").toInt() == 0)
		ui.radOutgoing->setChecked(1);
	else
		ui.radForwarded->setChecked(1);

	if(m_recVoiceLogData.getDataRef(0, "BVC_DEVICE_CODE").isNull())
		ui.cboInterface->setCurrentIndex(1);	//"Other"
	else
		ui.cboInterface->setCurrentIndex(0);	//"Skype"

	QDateTime dtStart;
	m_recVoiceLogData.getData(0, "BVC_START", dtStart);
	ui.txtCallStartTime->setText(dtStart.toString("hh:mm:ss"));

	QDateTime dtEnd;
	m_recVoiceLogData.getData(0, "BVC_END", dtEnd);
	ui.txtCallEndTime->setText(dtEnd.toString("hh:mm:ss"));

	//ui.txtCallDate->m_lineEdit->setText(dtStart.toString("dd.MM.yyyy"));

	//calc call duration
	if( !m_recVoiceLogData.getDataRef(0, "BVC_START").isNull() &&
		!m_recVoiceLogData.getDataRef(0, "BVC_END").isNull())
	{
		int nDiffSecs = dtStart.secsTo(dtEnd);
		int nSec = nDiffSecs % 60;
		int nMin = nDiffSecs / 60;
		int nHour = nMin / 60;
		nMin  = nMin % 60;
		ui.txtCallDuration->setText(QString().sprintf("%02d:%02d:%02d", nHour, nMin, nSec));
	}
	else
		ui.txtCallDuration->setText("");

	//int nPrivate = m_recVoiceLogData.getDataRef(0, "CENT_IS_PRIVATE").toInt();
	//ui.chkPrivateCall->setCheckState((nPrivate > 0)? Qt::Checked : Qt::Unchecked);

	ui.frmEditor->SetHtml(m_recVoiceLogData.getDataRef(0, "BVC_DESCRIPTION").toString());

	int nCallStatus = m_recVoiceLogData.getDataRef(0, "BVC_CALL_STATUS").toInt();
	
	//map state to combo box index
	if(CommGridViewHelper::CALL_STATE_Finished == nCallStatus)
		ui.cboCallStatus->setCurrentIndex(0);
	else if(CommGridViewHelper::CALL_STATE_Canceled == nCallStatus)
		ui.cboCallStatus->setCurrentIndex(1);
	else if(CommGridViewHelper::CALL_STATE_Failed == nCallStatus)
		ui.cboCallStatus->setCurrentIndex(2);
	else if(CommGridViewHelper::CALL_STATE_Missed == nCallStatus)
		ui.cboCallStatus->setCurrentIndex(3);
	else if(CommGridViewHelper::CALL_STATE_Refused == nCallStatus)
		ui.cboCallStatus->setCurrentIndex(4);
	else
		ui.cboCallStatus->setCurrentIndex(0);

	//TOFIX add all phones for the selected contact (if there is one)
	ui.cboPhoneNumber->clear();
	ui.cboPhoneNumber->addItem(m_recVoiceLogData.getDataRef(0, "BVC_CALLER_ID").toString());

	//update gui
	ui.frameContact->RefreshDisplay();
	ui.frmSelectedProject->RefreshDisplay();
	ui.frmType->RefreshDisplay();
	ui.frameTask->SetGUIFromData();
	ui.frameUser->RefreshDisplay();
}

void FUI_VoiceCall::SetViewMode(bool bView)
{
	ui.btnVoiceCall_Edit->setVisible(bView);	//only in view mode
	SetMode(bView? MODE_READ : MODE_EDIT);
}

void FUI_VoiceCall::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.radIncoming->setEnabled(false);
		ui.radOutgoing->setEnabled(false);
		ui.radForwarded->setEnabled(false);
		ui.txtCallStartTime->setEnabled(false);
		ui.txtCallEndTime->setEnabled(false);
		ui.txtCallDate->setEnabled(false);
		ui.txtCallDuration->setEnabled(false);
		ui.chkPrivateCall->setEnabled(false);
		ui.frmEditor->setEnabled(false);
		ui.cboCallStatus->setEnabled(false);
		ui.cboPhoneNumber->setEnabled(false);
		ui.cboInterface->setEnabled(false);
		ui.frameContact->setEnabled(false);
		ui.frmSelectedProject->setEnabled(false);
		ui.frmType->setEnabled(false);
		ui.btnStartTimeSetCurrent->setEnabled(false);
		ui.btnEndTimeSetCurrent->setEnabled(false);
	}
	else
	{
		ui.radIncoming->setEnabled(true);
		ui.radOutgoing->setEnabled(true);
		ui.radForwarded->setEnabled(true);
		ui.txtCallStartTime->setEnabled(true);
		ui.txtCallEndTime->setEnabled(true);
		ui.txtCallDate->setEnabled(true);
		ui.txtCallDuration->setEnabled(true);
		ui.chkPrivateCall->setEnabled(true);
		ui.frmEditor->setEnabled(true);
		ui.cboCallStatus->setEnabled(true);
		ui.cboPhoneNumber->setEnabled(true);
		ui.cboInterface->setEnabled(true);
		ui.frameContact->setEnabled(true);
		ui.frmSelectedProject->setEnabled(true);
		ui.frmType->setEnabled(true);
		ui.btnStartTimeSetCurrent->setEnabled(true);
		ui.btnEndTimeSetCurrent->setEnabled(true);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}

void FUI_VoiceCall::on_btnVoiceCall_Edit_clicked()
{
	//enter edit mode
	ui.btnVoiceCall_Edit->setEnabled(false);
	ui.btnVoiceCall_OK->setEnabled(true);
	ui.btnVoiceCall_Cancel->setEnabled(true);

	SetMode(MODE_EDIT);
}

void FUI_VoiceCall::on_btnVoiceCall_OK_clicked()
{
	/*
	UpdateDataFromGui();

	//save the attached task (if exists)
	Status status;
	ui.frameTask->GetDataFromGUI();

	bool bTaskActive = ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0;	
	if(bTaskActive)
	{
		//write task record
		_SERVER_CALL(ClientSimpleORM->Write(status, BUS_TASKS, ui.frameTask->m_recTask))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}

		//initialize task ID
		m_recVoiceLogData.setData(0, "CENT_TASK_ID", ui.frameTask->m_recTask.getDataRef(0, "BTKS_ID").toInt());
		//m_recData.Dump();
	}

	//write voice call record
	_SERVER_CALL(VoiceCallCenter->WriteCall(status, m_recVoiceLogData,""))
	if(!status.IsOK())
	{
		QString strErr = status.getErrorText();
		QMessageBox::information(NULL, "", strErr);
	}
	else{
		//refresh cache and close
		g_ClientCache.ModifyCache(ENTITY_BUS_VOICECALLS, m_recVoiceLogData, DataHelper::OP_ADD, this); 
		g_objFuiManager.CloseFUI(g_objFuiManager.GetFuiID(this));
	}
	*/
}

void FUI_VoiceCall::on_btnVoiceCall_Cancel_clicked()
{
	//close fui
	g_objFuiManager.CloseFUI(g_objFuiManager.GetFuiID(this)); 
}

void FUI_VoiceCall::OnScheduleCall()
{
	int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false);
	FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
	if(NULL == pFUI) return;	//someone closed the FUI for this call

	UpdateDataFromGui();
	pFUI->SetShowTask(m_recVoiceLogData);
}

void FUI_VoiceCall::OnScheduleEmail()
{
	QMessageBox::information(NULL, "", "TODO");
}

void FUI_VoiceCall::OnScheduleDoc()
{

	//schedule call for what contacts->from SAPNE?


	//for docs:
	Document_PieMenu pie;
	pie.CreateNewDocument(ui.btnSheduleCall,NULL,NULL,NULL,true);

}

void FUI_VoiceCall::on_btnSheduleCall_clicked()
{
	m_pPieMenu->popup(ui.btnSheduleCall->mapToGlobal(QPoint(0,0)));
}

void FUI_VoiceCall::on_btnStartTimeSetCurrent_clicked()
{
	ui.txtCallStartTime->setText(QTime::currentTime().toString("hh:mm:ss")); 
	//set current date
//	ui.txtCallDate->m_lineEdit->setText(QDate::currentDate().toString("dd.MM.yyyy"));

	UpdateDataFromGui();
}

void FUI_VoiceCall::on_btnEndTimeSetCurrent_clicked()
{
	ui.txtCallEndTime->setText(QTime::currentTime().toString("hh:mm:ss")); 
	UpdateDataFromGui();

	//RecalcDuration();

	//calc call duration
	if( !m_recVoiceLogData.getDataRef(0, "BVC_START").isNull() &&
		!m_recVoiceLogData.getDataRef(0, "BVC_END").isNull())
	{
		QDateTime dtStart;
		m_recVoiceLogData.getData(0, "BVC_START", dtStart);
		QDateTime dtEnd;
		m_recVoiceLogData.getData(0, "BVC_END", dtEnd);

		int nDiffSecs = dtStart.secsTo(dtEnd);
		int nSec = nDiffSecs % 60;
		int nMin = nDiffSecs / 60;
		int nHour = nMin / 60;
		nMin  = nMin % 60;
		ui.txtCallDuration->setText(QString().sprintf("%02d:%02d:%02d", nHour, nMin, nSec));
	}
	else
		ui.txtCallDuration->setText("");
}

void FUI_VoiceCall::UpdateDataFromGui()
{
	if(ui.radIncoming->isChecked())
		m_recVoiceLogData.getDataRef(0, "BVC_DIRECTION") = QVariant(1);
	else
		m_recVoiceLogData.getDataRef(0, "BVC_DIRECTION") = QVariant(0);

	if(ui.cboInterface->currentIndex() == 0)	//"Skype"
		m_recVoiceLogData.getDataRef(0, "BVC_DEVICE_CODE") = QVariant(1);
	else
		m_recVoiceLogData.getDataRef(0, "BVC_DEVICE_CODE") = QVariant(QVariant::Int);

	QString strDateTime;// = ui.txtCallDate->m_lineEdit->text();
	strDateTime += " ";
	strDateTime += ui.txtCallStartTime->text();

	QDateTime dtStart;
	dtStart = QDateTime::fromString(strDateTime, "dd.MM.yyyy hh:mm:ss");
	m_recVoiceLogData.setData(0, "BVC_START", dtStart);

//	strDateTime = ui.txtCallDate->m_lineEdit->text();
	strDateTime += " ";
	strDateTime += ui.txtCallEndTime->text();

	QDateTime dtEnd;
	dtEnd = QDateTime::fromString(strDateTime, "dd.MM.yyyy hh:mm:ss");
	m_recVoiceLogData.setData(0, "BVC_END", dtEnd);

	//calc call duration
	int nDiffSecs = dtStart.secsTo(dtEnd);
	m_recVoiceLogData.setData(0, "BVC_DURATION", nDiffSecs);

	/*
	if(ui.chkPrivateCall->isChecked())
		m_recVoiceLogData.setData(0, "CENT_IS_PRIVATE", 1);
	else
		m_recVoiceLogData.setData(0, "CENT_IS_PRIVATE", 0);
	*/

	QString strHtml = ui.frmEditor->GetHtml();
	m_recVoiceLogData.setData(0, "BVC_DESCRIPTION", strHtml);

	int nIdx = ui.cboCallStatus->currentIndex();
	
	//map state to combo box index
	int nCallStatus = CommGridViewHelper::CALL_STATE_Finished;
	if(0 == nIdx)
		nCallStatus = CommGridViewHelper::CALL_STATE_Finished;
	else if(1 == nIdx)
		nCallStatus = CommGridViewHelper::CALL_STATE_Canceled;
	else if(2 == nIdx)
		nCallStatus = CommGridViewHelper::CALL_STATE_Failed;
	else if(3 == nIdx)
		nCallStatus = CommGridViewHelper::CALL_STATE_Missed;
	else if(4 == nIdx)
		nCallStatus = CommGridViewHelper::CALL_STATE_Refused;

	m_recVoiceLogData.setData(0, "BVC_CALL_STATUS", nCallStatus);
	m_recVoiceLogData.setData(0, "BVC_CALLER_ID", ui.cboPhoneNumber->currentText());
}

void FUI_VoiceCall::SetData(DbRecordSet &recVoice, DbRecordSet &recTask)
{
	m_recVoiceLogData.clear();
	m_recVoiceLogData.merge(recVoice);
	ui.frameTask->m_recTask = recTask;
	RefreshDisplay();
}
