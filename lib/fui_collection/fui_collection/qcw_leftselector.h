#ifndef QCW_LEFTSELECTOR_H
#define QCW_LEFTSELECTOR_H

#include <QtWidgets/QFrame>
#include "generatedfiles/ui_qcw_leftselector.h"
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "common/common/observer_ptrn.h"

class QCW_LeftSelector : public QFrame, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	QCW_LeftSelector(QWidget *parent = 0);
	~QCW_LeftSelector();

	void RefreshDisplay();
	QString GetSelectedText();
	DbRecordSet m_lstContacts; //source of grid: TVIEW_BUS_CONTACT_FULL
	void ShowContactGrid(bool bShow);
	
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void dragMoveEvent(QDragMoveEvent *event );
	void SetDropText(QString strText,bool bskipParse=false);

public slots:
	void SlotSelectionChanged();

signals:
	void NewDataDropped(int nDropType, DbRecordSet lstDroppedData);			//emited always when soemthing is dropped on drop zone
	void SelectionChanged(int nSelectedRow, DbRecordSet lstSelectedRow);
	void SetNames(QString strFirstName, QString strLastName, QString strMiddleName);
	void SetOrganization(QString strOrganization);

private slots:
	void OnTextChanged();
	void OnDeleteSelection();
	void OnDeleteTable();

	//btns:
	void on_btnContact_clicked();
	void on_btnHouse_clicked();
	void on_btnInternet_clicked();
	void on_btnAddress_clicked();
	void on_btnEmail_clicked();
	void on_btnPhone_clicked();

	void on_btnParse_clicked();
	void on_btnRefresh_clicked();
	void on_btnExport_clicked();
	void on_btnFormData_clicked();
	



private:
	Ui::QCW_LeftSelectorClass ui;
	void OnThemeChanged();
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void ProcessAddress(QString strTextDrop);
	void ProcessEMail(QString strTextDrop);
	void ProcessPhone(QString strTextDrop);
	void ProcessInternet(QString strTextDrop);

	ClientContactManager m_Manager;

};

#endif // QCW_LEFTSELECTOR_H
