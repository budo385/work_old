#include "countrysettings.h"
#include "bus_core/bus_core/countries.h"
#include "bus_core/bus_core/formatphone.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include <QtWidgets/QMessageBox>
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

CountrySettings::CountrySettings(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
    : SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);


	//Set look and feel.
	ui.Country_label->setText(tr("Select Country:"));

	//Init widgets.
	InitializeWidgets();
}

CountrySettings::~CountrySettings()
{
	
}

void CountrySettings::InitializeWidgets()
{
	//Country combo box.
	DbRecordSet recCountriesRecordSet;
	Countries::GetCountries(recCountriesRecordSet);

	int countryCount = recCountriesRecordSet.getRowCount();
	for (int i = 0; i < countryCount; ++i)
	{
		QString CountryName = recCountriesRecordSet.getDataRef(i, 0).toString();
		QVariant Predial	= recCountriesRecordSet.getDataRef(i, "ISO");
		ui.Country_comboBox->addItem(CountryName, Predial);
	}

	QVariant Predial = GetSettingValue(CURRENT_COUNTRY_ISO_CODE);
	//qDebug()<<Predial.toString();
	int index = ui.Country_comboBox->findData(Predial);
	ui.Country_comboBox->setCurrentIndex(index);

	//Location name.
	QString strLocationName = GetSettingValue(CURRENT_LOCATION_NAME).toString();
	ui.LocationName_lineEdit->setText(strLocationName);
	
	//Area code.
	QString strAreaCode = GetSettingValue(CURRENT_LOCATION_AREA_CODE).toString();
	ui.AreaCode_lineEdit->setText(strAreaCode);

	//Default language.
	QStringList lstDefaultLanguage;
	lstDefaultLanguage << "English" << "Deutsch";
	ui.DefaultLanguage_comboBox->blockSignals(true);
	ui.DefaultLanguage_comboBox->addItems(lstDefaultLanguage);

	int nCurrentDefaultLanguage = GetSettingValue(DEFAULT_LANGUAGE).toInt();
	ui.DefaultLanguage_comboBox->setCurrentIndex(nCurrentDefaultLanguage);

/*
	if(g_pClientManager->GetIniFile()->m_strLangCode == "de")
		ui.DefaultLanguage_comboBox->setCurrentIndex(1);
	else
		ui.DefaultLanguage_comboBox->setCurrentIndex(0);
	ui.DefaultLanguage_comboBox->blockSignals(false);
*/	
}

DbRecordSet CountrySettings::GetChangedValuesRecordSet()
{
	//Country predial
	int index = ui.Country_comboBox->currentIndex();
	if (ui.Country_comboBox->itemData(index).toString() != GetSettingValue(CURRENT_COUNTRY_ISO_CODE).toString())
		SetSettingValue(CURRENT_COUNTRY_ISO_CODE, ui.Country_comboBox->itemData(index).toString());

	//Location name.
	if (ui.LocationName_lineEdit->text() != GetSettingValue(CURRENT_LOCATION_NAME).toString())
		SetSettingValue(CURRENT_LOCATION_NAME, ui.LocationName_lineEdit->text());

	//Area code.
	if (ui.AreaCode_lineEdit->text() != GetSettingValue(CURRENT_LOCATION_AREA_CODE).toString())
		SetSettingValue(CURRENT_LOCATION_AREA_CODE, ui.AreaCode_lineEdit->text());

	//set country/ predial:
	FormatPhone::SetDefaults(GetSettingValue(CURRENT_COUNTRY_ISO_CODE).toString(),GetSettingValue(CURRENT_LOCATION_AREA_CODE).toString());


	//Default language.
	index = ui.DefaultLanguage_comboBox->currentIndex();
	if (ui.DefaultLanguage_comboBox->currentIndex() != GetSettingValue(DEFAULT_LANGUAGE).toInt())
		SetSettingValue(DEFAULT_LANGUAGE, ui.DefaultLanguage_comboBox->currentIndex());
	
	bool bLoggedUser=(GetPersonID()==g_pClientManager->GetPersonID());
	if (GetPersonID()<=0)
		bLoggedUser=true;
	if (bLoggedUser)
	{
		if(index == 1)
			g_pClientManager->GetIniFile()->m_strLangCode = "de";
		else
			g_pClientManager->GetIniFile()->m_strLangCode = "en";
	}

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

void CountrySettings::on_DefaultLanguage_comboBox_currentIndexChanged(int index)
{
	QMessageBox::information(NULL, tr("Information"), tr("The selected language will be loaded at the next application start!"));
}
