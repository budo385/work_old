#ifndef SETTINGSBASE_H
#define SETTINGSBASE_H

#include <QWidget>
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "bus_core/bus_core/optionsandsettingsmanager.h"
#include "common/common/dbrecordset.h"

class SettingsBase : public QWidget
{
    Q_OBJECT

public:
    SettingsBase(int nSettingsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
    ~SettingsBase();

	virtual DbRecordSet GetChangedValuesRecordSet();
	int GetPersonID();

protected:
	void		SetIDChanged(int SettingsID);
	void		SetChanged();
	QVariant	GetSettingValue(int nSettingID);
	void		SetSettingValue(int nSettingID, int nValue);
	void		SetSettingValue(int nSettingID, QByteArray byteArray);
	void		SetSettingValue(int nSettingID, QString strValue);
	int			InsertNonExistingSetting(int nSettingID);

	DbRecordSet					m_recSettings;				//< This widget recordset.
	int							m_nSettingsSetID;			//< Id of set
	OptionsAndSettingsManager	*m_pOptionsAndSettingsManager;
	bool						m_bWholeSetChanged;			//< if true, write back all settings
	bool						m_bOptionsSwitch;			//< Is this options or settings widget.

private:
	QList<int>					m_lstChangedValues;			//< Changed values list (list of setting id's).
};

#endif // SETTINGSBASE_H
