#ifndef COUNTRYSETTINGS_H
#define COUNTRYSETTINGS_H

#include <QWidget>
#include "ui_countrysettings.h"

#include "settingsbase.h"

class CountrySettings : public SettingsBase
{
    Q_OBJECT

public:
    CountrySettings(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
    ~CountrySettings();

private slots:
	void on_DefaultLanguage_comboBox_currentIndexChanged(int index);

private:
    Ui::CountrySettingsClass ui;

	void InitializeWidgets();
	DbRecordSet GetChangedValuesRecordSet();
};

#endif // COUNTRYSETTINGS_H
