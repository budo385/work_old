#ifndef CALENDARRESERVATIONITEM_H
#define CALENDARRESERVATIONITEM_H

#include <QObject>
#include <QGraphicsItem>
#include "calendargraphicsview.h"

class CalendarReservationItem : public QObject, public QGraphicsItem
{
	Q_OBJECT

public:
	CalendarReservationItem(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, QDateTime startDateTime, QDateTime endDateTime, CalendarGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent = 0);
	~CalendarReservationItem();

	QRectF boundingRect() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

public slots:
	void on_UpdateCalendar();

private:
	int				  	m_nEntityID;
	int				  	m_nEntityType;
	int				  	m_nBCRS_ID;
	int				  	m_nBCRS_IS_POSSIBLE;
	QDateTime		  	m_startDateTime;
	QDateTime		  	m_endDateTime;
	QRect			  	m_rectItemRectangle;
	int				  	m_nHeight;
	qreal			  	m_nWidth;
	CalendarGraphicsView *m_pCalendarGraphicsView;
};

#endif // CALENDARRESERVATIONITEM_H
