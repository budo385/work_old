#ifndef TASKWIDGET_H
#define TASKWIDGET_H

#include <QWidget>
#include "ui_taskwidget.h"
#include "common/common/observer_ptrn.h"
#include "common/common/status.h"

/*!
	\class  TaskWidget
	\brief  Schedule tasks
	\ingroup FUICollection

	Use:
	- INSERT: fill m_recTask, with task=active, status, etc.. to enable it
	- EDIT: if Task ID != NULL, chkbox is disabled: chkbox is checked if task active
	- MODE: if read mode, all controls can be enabled/disabled
*/

class TaskWidget : public QWidget, public ObsrPtrn_Observer
{
    Q_OBJECT

public:
    TaskWidget(QWidget *parent = 0);
    ~TaskWidget();

	DbRecordSet m_recTask;
	DbRecordSet *m_pRecMain;

	void ServerReload();

	void SetDefaultTaskType(int nTaskSystemType);
	void GetDataFromGUI();
	void SetGUIFromData();
	void ClearTaskData();
	void SetEnabled(bool bEnable=true);
	bool isChecked();
	void SetChecked(bool bChecked=true);
	void Hide();
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());	
	void CheckTaskRecordBeforeSave(Status &err);
	void SetAlwaysExpanded(bool bExpanded=true);

signals:
	void ScheduleTaskToggle();

private:
	void SetInfoVisible(bool bVisible = true); //automatically visible if task created, else on check button

    Ui::TaskWidgetClass ui;

	int m_nTaskSystemType;
	bool m_bEditMode;
	bool m_bAlwaysExpanded;

private slots:
	void on_chkScheduledTask_clicked();
	void OnTaskTypeChanged();
	void OnTaskCompleted(bool bChecked);
};

#endif // TASKWIDGET_H
