#ifndef REPORTPREVIEWER_H
#define REPORTPREVIEWER_H

#include <QWidget>
#include <QScrollArea>
#include <QToolBar>
#include <QMenuBar>
#include <QInputDialog>
#include <QComboBox>
#include <QLabel>
#ifndef WINCE
#include <QPrintDialog>
#endif //WINCE
#include <QFileDialog>
#include <QVariant>

//Form header.
#include "generatedfiles/ui_reportpreviewer.h"

//Base class
#include "fuibase.h"

//Report printer.
#include "report_core/report_core/rptprinter.h"

//Wizard.
//#include "gui_core/gui_core/wizardbase.h"
#include "gui_core/gui_core/richeditordlg.h"

class ReportPreviewer :public FuiBase
{
    Q_OBJECT

public:
    ReportPreviewer(QWidget *parent = 0);
    ~ReportPreviewer();

	QString GetFUIName();

private:
    Ui::ReportPreviewerClass ui;

	void PrintPreview();
	void DeleteCurrentReport();

	QString			m_strReportFileName;
	QString			m_strPdfFileName;
	//QList<QPicture*> m_lstPictureList;
	QList<QPixmap*> m_lstPictureList;
	double			m_nScaleFactor;
	QComboBox		*m_cbZoom;
	QLabel			*m_PictureLabel;
	int				m_nCurrentPicture;
	QList<DbRecordSet> m_lstPageRecordSet;								//< Pages results recordset list.

private slots:
	void on_CloseReportButton_clicked();
	void on_GoToPageButton_clicked();
	void on_LastPageButton_clicked();
	void on_NextPageButton_clicked();
	void on_PreviousPageButton_clicked();
	void on_FirstPageButton_clicked();
	void on_PrintPdfButton_clicked();
	void on_PrintButton_clicked();
	void on_OpenButton_clicked();
};

#endif // REPORTPREVIEWER_H
