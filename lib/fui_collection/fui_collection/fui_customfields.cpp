#include "fui_customfields.h"
#include "bus_core/bus_core/globalconstants.h"

//bo set
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	

FUI_CustomFields::FUI_CustomFields(QWidget *parent)
	: FuiBase(parent)
{
	ui.setupUi(this);

	ui.frameSelection->Initialize(ENTITY_CUSTOM_FIELDS,false,true); //skip loading data, coz this is under filter =ACO
	InitFui(ENTITY_CUSTOM_FIELDS, BUS_CUSTOM_FIELDS,dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);

	//fill dropdown's:
	ui.cmbTable->addItem(tr("Contacts"),BUS_CM_CONTACT);
	ui.cmbTable->addItem(tr("Projects"),BUS_PROJECT);

	ui.cmbDataType->blockSignals(true);
	ui.cmbDataType->addItem(tr("Boolean"),GlobalConstants::TYPE_CUSTOM_DATA_BOOL);
	ui.cmbDataType->addItem(tr("Integer"),GlobalConstants::TYPE_CUSTOM_DATA_INT);
	ui.cmbDataType->addItem(tr("Float"),GlobalConstants::TYPE_CUSTOM_DATA_FLOAT);
	ui.cmbDataType->addItem(tr("Character(200)"),GlobalConstants::TYPE_CUSTOM_DATA_TEXT);
	ui.cmbDataType->addItem(tr("Text"),GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT);
	ui.cmbDataType->addItem(tr("Date"),GlobalConstants::TYPE_CUSTOM_DATA_DATE);
	ui.cmbDataType->addItem(tr("Datetime"),GlobalConstants::TYPE_CUSTOM_DATA_DATETIME);
	ui.cmbDataType->blockSignals(false);

	ui.cmbSelection->blockSignals(true);
	ui.cmbSelection->addItem(tr("None"),GlobalConstants::TYPE_CUSTOM_SELECTION_NONE);
	ui.cmbSelection->addItem(tr("Optional"),GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL);
	ui.cmbSelection->addItem(tr("Mandatory"),GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY);
	ui.cmbSelection->addItem(tr("Mandatory With Radio Buttons"),GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON);
	ui.cmbSelection->blockSignals(false);
	

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData, DbSqlTableView::TVIEW_BUS_CAL_RESOURCE);
	m_GuiFieldManagerMain->RegisterField("BCF_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BCF_SORT",ui.txtSort);
	m_GuiFieldManagerMain->RegisterField("BCF_GROUP",ui.txtGroup);

	//--------------------------------------------------
	//		SELECTIONS
	//--------------------------------------------------
	m_lstSelections.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_SELECTIONS));

	DbRecordSet columns;
	UniversalTableWidgetEx::AddColumnToSetup(columns,"BCS_VALUE",tr("Value"),200,true);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"BCS_IS_DEFAULT",tr("Default Value"),80,true,"",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"BCS_RADIO_BUTTON_LABEL",tr("Radio Button Label"),200);
	ui.tableSelections->Initialize(&m_lstSelections,&columns);

	QSize buttonSize(22, 22);
	ui.btnAdd		->setMaximumSize(buttonSize);
	ui.btnAdd		->setMinimumSize(buttonSize);
	ui.btnDelete	->setMaximumSize(buttonSize);
	ui.btnDelete	->setMinimumSize(buttonSize);
	ui.btnAdd->setIcon(QIcon(":handwrite.png"));
	ui.btnDelete->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnDelete->setToolTip(tr("Delete"));
	ui.btnAdd->setToolTip(tr("Add"));

	connect(ui.btnAdd, SIGNAL(clicked()), ui.tableSelections, SLOT(InsertRow()));
	connect(ui.btnDelete, SIGNAL(clicked()), ui.tableSelections, SLOT(DeleteSelection()));
	QList<QAction*> lstActions;
	ui.tableSelections->CreateDefaultContextMenuActions(lstActions);
	ui.tableSelections->SetContextMenuActions(lstActions);
	connect(ui.tableSelections,SIGNAL(SignalDataChanged(int,int)),this,SLOT(OnSelectionTableDataChanged(int,int)));
	connect(ui.tableSelections,SIGNAL(SignalRowInserted(int)),this,SLOT(OnSelectionTableRowInserted(int)));

	SetMode(MODE_EMPTY);
	on_selectionDataChange();

}

FUI_CustomFields::~FUI_CustomFields()
{

}


QString FUI_CustomFields::GetFUIName()
{
	return tr("Custom Fields");
}

void FUI_CustomFields::RefreshDisplay()
{
	m_GuiFieldManagerMain->RefreshDisplay();
}



//when changing state, call this:
void FUI_CustomFields::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameSelection->setEnabled(true);
		m_GuiFieldManagerMain->SetEditMode(false);
		ui.cmbTable->setEnabled(false);
		ui.cmbDataType->setEnabled(false);
		ui.cmbSelection->setEnabled(false);
		ui.tableSelections->SetEditMode(false);
		ui.btnAdd->setEnabled(false);
		ui.btnDelete->setEnabled(false);
	}
	else
	{
		ui.frameSelection->setEnabled(false);
		m_GuiFieldManagerMain->SetEditMode(true);
		ui.cmbTable->setEnabled(true);
		//ui.cmbSelection->setEnabled(true);
		if (nMode==MODE_INSERT)
		{
			ui.cmbDataType->setEnabled(true);
			
		}

		RefreshSelectionComboEdit();
		RefreshSelectionTableEdit();


	}

	if (nMode==MODE_EMPTY)
	{
		m_lstData.clear();
		m_lstSelections.clear();
		ui.tableSelections->RefreshDisplay();
		ui.cmbSelection->setCurrentIndex(0);
		ui.cmbDataType->setCurrentIndex(0);
	}


	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}

//prepare for insert: payment period=0
void FUI_CustomFields::DataPrepareForInsert(Status &err)
{
	FuiBase::DataPrepareForInsert(err);
	m_lstData.setData(0,"BCF_TABLE_ID",BUS_CM_CONTACT);
	m_lstData.setData(0,"BCF_DATA_TYPE",GlobalConstants::TYPE_CUSTOM_DATA_BOOL);
	m_lstData.setData(0,"BCF_SELECTION_TYPE",GlobalConstants::TYPE_CUSTOM_SELECTION_NONE);

	ui.cmbTable->setCurrentIndex(ui.cmbTable->findData(m_lstData.getDataRef(0,"BCF_TABLE_ID").toInt()));
	ui.cmbDataType->setCurrentIndex(ui.cmbDataType->findData(m_lstData.getDataRef(0,"BCF_DATA_TYPE").toInt()));
	ui.cmbSelection->setCurrentIndex(ui.cmbSelection->findData(m_lstData.getDataRef(0,"BCF_SELECTION_TYPE").toInt()));
}

//check if name is entered:
void FUI_CustomFields::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	if(m_lstData.getDataRef(0,"BCF_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Name is mandatory field!"));	return;	}
}


//reads full view record based on given Primary key
void FUI_CustomFields::DataRead(Status &err,int nRecordID)
{
	_SERVER_CALL(BusCustomFields->Read(err,nRecordID,m_lstData,m_lstSelections))

	//m_lstData.Dump();
	//m_lstSelections.Dump();
	
	ui.tableSelections->RefreshDisplay();

	if (err.IsOK() && m_lstData.getRowCount()>0)
	{
		m_lstDataCache=m_lstData;
		
		//set values of combo's:
		ui.cmbTable->setCurrentIndex(ui.cmbTable->findData(m_lstData.getDataRef(0,"BCF_TABLE_ID").toInt()));
		ui.cmbDataType->setCurrentIndex(ui.cmbDataType->findData(m_lstData.getDataRef(0,"BCF_DATA_TYPE").toInt()));
		ui.cmbSelection->setCurrentIndex(ui.cmbSelection->findData(m_lstData.getDataRef(0,"BCF_SELECTION_TYPE").toInt()));

		RefreshSelectionComboEdit();
		RefreshSelectionTableEdit();
	}



}

void FUI_CustomFields::prepareCacheRecord(DbRecordSet *recNewData)
{

	DbRecordSet newCacheRecord;
	newCacheRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS_SELECT));
	newCacheRecord.merge(*recNewData);

	if (newCacheRecord.getRowCount()==1)
	{
		if (newCacheRecord.getDataRef(0,"BCF_TABLE_ID").toInt()==BUS_CM_CONTACT)
			newCacheRecord.setData(0,"TABLE_NAME",tr("Contact"));
		else
			newCacheRecord.setData(0,"TABLE_NAME",tr("Project"));
	}

	*recNewData=newCacheRecord;

}

void FUI_CustomFields::DataUnlock(Status &err)
{
	FuiBase::DataUnlock(err);
	if (err.IsOK())
	{
		DataRead(err,m_nEntityRecordID); //RELOAD DATA..WHO KNOWs what is changed
	}
}


void FUI_CustomFields::DataWrite(Status &err)
{
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	DbRecordSet lstForDelete;

	//set values of combo's:
	m_lstData.setData(0,"BCF_TABLE_ID",ui.cmbTable->itemData(ui.cmbTable->currentIndex()));
	m_lstData.setData(0,"BCF_DATA_TYPE",ui.cmbDataType->itemData(ui.cmbDataType->currentIndex()));
	m_lstData.setData(0,"BCF_SELECTION_TYPE",ui.cmbSelection->itemData(ui.cmbSelection->currentIndex()));

	if (m_lstData.getDataRef(0,"BCF_SELECTION_TYPE").toInt()==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
	{
		m_lstSelections.clear();
	}

	if(!CheckSelectionValues())
	{
		//QMessageBox::warning(this, tr("Warning"), tr("Some selection predefined values can not be converted to the specified data type. Operation aborted!"));
		err.setError(1,tr("Some selection predefined values can not be converted to the specified data type. Operation aborted!"));
		return;
	}

	//check if mandatory and mand with radio then if selections = empty then disable
	if (m_lstData.getDataRef(0,"BCF_DATA_TYPE").toInt()==GlobalConstants::TYPE_CUSTOM_DATA_BOOL && (m_lstData.getDataRef(0,"BCF_SELECTION_TYPE").toInt()==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON || m_lstData.getDataRef(0,"BCF_SELECTION_TYPE").toInt()==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY))
	{
		if (m_lstSelections.getRowCount()!=2)
		{
			err.setError(1,tr("If selection is mandatory then exactly two selectable values must be defined for bool data type. Operation aborted!"));
			return;
		}
	}


	//check if mandatory and mand with radio then if selections = empty then disable
	if (m_lstSelections.getRowCount()==0 && (m_lstData.getDataRef(0,"BCF_SELECTION_TYPE").toInt()==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY || m_lstData.getDataRef(0,"BCF_SELECTION_TYPE").toInt()==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON))
	{
		err.setError(1,tr("If selection is mandatory then selection values must be defined. Operation aborted!"));
		return;
	}


	//m_lstData.Dump();
	//m_lstSelections.Dump();

	_SERVER_CALL(BusCustomFields->Write(err,m_lstData,m_lstSelections,m_strLockedRes))
	//_SERVER_CALL(ClientSimpleORM->Write(err,m_nTableID,m_lstData,m_strLockedRes, nQueryView, nSkipLastColumns, lstForDelete))
	if (err.IsOK())
	{
		m_strLockedRes.clear();
		m_lstDataCache=m_lstData;
	}
}

//just fill out name fields...
void FUI_CustomFields::on_selectionDataChange()
{
	DbRecordSet *pData=ui.frameSelection->GetDataSource();
	int nSize=pData->getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (pData->getDataRef(i,"BCF_TABLE_ID").toInt()==BUS_CM_CONTACT)
		{
			pData->setData(i,"TABLE_NAME",tr("Contact"));
		}
		else
		{
			pData->setData(i,"TABLE_NAME",tr("Project"));
		}
	}


	ui.frameSelection->RefreshDisplay();
	
}

void FUI_CustomFields::on_cmbDataType_currentIndexChanged(int nIdx)
{
	RefreshSelectionComboEdit();
}


void FUI_CustomFields::on_cmbSelection_currentIndexChanged(int nIdx)
{
	RefreshSelectionTableEdit();
}

//allow only 1 default value:
void FUI_CustomFields::OnSelectionTableDataChanged(int row,int col)
{
	if (col==1) //display col
	{
		int nVal=m_lstSelections.getDataRef(row,"BCS_IS_DEFAULT").toInt();
		m_lstSelections.setColValue("BCS_IS_DEFAULT",0);
		m_lstSelections.setData(row,"BCS_IS_DEFAULT",nVal);
		ui.tableSelections->RefreshDisplay();
	}
	
}

void FUI_CustomFields::OnSelectionTableRowInserted(int row)
{
	//if bool then allow only two rows:
	if (ui.cmbDataType->itemData(ui.cmbDataType->currentIndex())==GlobalConstants::TYPE_CUSTOM_DATA_BOOL)
	{
		if (m_lstSelections.getRowCount()>2)
		{
			QMessageBox::warning(this, tr("Warning"), tr("When data type is boolean then only two predefined values are allowed"));
			m_lstSelections.deleteRow(row);
			ui.tableSelections->RefreshDisplay();
		}
	}
}


//before write check if sel values are ok:
bool FUI_CustomFields::CheckSelectionValues()
{
	int nDataType=ui.cmbDataType->itemData(ui.cmbDataType->currentIndex()).toInt();
	int nSize=m_lstSelections.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (m_lstSelections.getDataRef(i,"BCS_VALUE").toString().isEmpty())
		{
			return false;
		}

		switch (nDataType)
		{
			case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
			{
				bool bOK;
				m_lstSelections.getDataRef(i,"BCS_VALUE").toString().toInt(&bOK);
				if (!bOK)
					return false;

				//if(!m_lstSelections.getDataRef(i,"BCS_VALUE").canConvert(QVariant::Bool))
				//	return false;
			}
			break;
			case GlobalConstants::TYPE_CUSTOM_DATA_INT:
			{
				bool bOK;
				m_lstSelections.getDataRef(i,"BCS_VALUE").toString().toInt(&bOK);
				if (!bOK)
					return false;

//				if(!m_lstSelections.getDataRef(i,"BCS_VALUE").canConvert(QVariant::Int))
//					return false;
			}
			break;
			case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
			{
				bool bOK;
				m_lstSelections.getDataRef(i,"BCS_VALUE").toString().toFloat(&bOK);
				if (!bOK)
					return false;
			}
			break;
			case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
			{
				QDate testDate=m_lstSelections.getDataRef(i,"BCS_VALUE").toDate();
				if (!testDate.isValid())
					return false;
			}
			break;
			case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
			{
				QDateTime testDate=m_lstSelections.getDataRef(i,"BCS_VALUE").toDateTime();
				if (!testDate.isValid())
					return false;
			}
			break;
		}
	}

	return true;
}


void FUI_CustomFields::RefreshSelectionComboEdit()
{
	int nDataType=ui.cmbDataType->itemData(ui.cmbDataType->currentIndex()).toInt();
	if (nDataType>=GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT)
	{
		ui.cmbSelection->setCurrentIndex(0);//to none
		ui.cmbSelection->setEnabled(false);
		m_lstSelections.clear();
		ui.tableSelections->RefreshDisplay();
	}
	else
	{
		ui.cmbSelection->setEnabled(true);
	}
	
}
void FUI_CustomFields::RefreshSelectionTableEdit()
{
	int nSelType=ui.cmbSelection->itemData(ui.cmbSelection->currentIndex()).toInt();
	
	if (nSelType!=GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
	{
		int nDataType=ui.cmbDataType->itemData(ui.cmbDataType->currentIndex()).toInt();
		if (nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL && nDataType==GlobalConstants::TYPE_CUSTOM_DATA_BOOL)
		{
			QMessageBox::information(this,tr("Information"),tr("You can not use optional selection with bool data type!"));
			ui.cmbSelection->setCurrentIndex(0);

			ui.tableSelections->SetEditMode(false);
			ui.btnAdd->setEnabled(false);
			ui.btnDelete->setEnabled(false);

			m_lstSelections.clear();
			ui.tableSelections->RefreshDisplay();
			return;
		}
		ui.tableSelections->SetEditMode(true);
		ui.btnAdd->setEnabled(true);
		ui.btnDelete->setEnabled(true);
	}
	else
	{
		ui.tableSelections->SetEditMode(false);
		ui.btnAdd->setEnabled(false);
		ui.btnDelete->setEnabled(false);

		m_lstSelections.clear();
		ui.tableSelections->RefreshDisplay();
	}
		
	if (nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
	{
		DbRecordSet columns;
		ui.tableSelections->GetColumnSetup(columns);
		if (columns.getRowCount()!=3)
		{
			UniversalTableWidgetEx::AddColumnToSetup(columns,"BCS_RADIO_BUTTON_LABEL",tr("Radio button label"),200);
			ui.tableSelections->SetColumnSetup(columns);
		}
	}
	else
	{
		DbRecordSet columns;
		ui.tableSelections->GetColumnSetup(columns);
		if (columns.getRowCount()!=2)
		{
			columns.deleteRow(columns.getRowCount()-1);
			ui.tableSelections->SetColumnSetup(columns);
		}
	}
}
	
