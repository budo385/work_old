#ifndef FUI_AVATAR_H
#define FUI_AVATAR_H

#include <QWidget>
#include "generatedfiles/ui_fui_avatar.h"
#include "dropzone_menuwidget.h"

#define AVATAR_PROJECT				0
#define AVATAR_CONTACT				1
#define AVATAR_TASK_EMAIL			2
#define AVATAR_TASK_CALL			3
#define AVATAR_TASK_ADDRESS			4
#define AVATAR_TASK_INTERNET_FILE	5
#define AVATAR_TASK_LOCAL_FILE		6
#define AVATAR_TASK_NOTE			7
#define AVATAR_TASK_PAPER_DOCS		8

class fui_avatar : public QWidget, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	fui_avatar(int nType, QWidget *parent = 0);
	~fui_avatar();

	void SetRecord(int nID);
	void SetPicture(QPixmap &picture);
	void SetLabel(QString &strTxt);
	QString EncodeState(); //to store/restore in settings
	
	void SetTaskID(int nID){ m_nTaskID = nID; };
	void SetTaskDate(QString &strTxt);
	void SetTaskState(int nState);
	void SetTaskOwnerInitials(QString &strTxt);
	void SetTaskDescTooltip(const QString &strTxt);
	void SetTaskDoneMark();

	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

protected:
	void mouseDoubleClickEvent(QMouseEvent * event);
	void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);

protected:
	void OnThemeChanged();
    QPoint  m_dragPosition;
	QImage	m_imageBkg;
	QWidget *m_pParent;
	int		m_nAvatarType;
	int		m_nRecordID;
	int		m_nTaskID;
	DropZone_MenuWidget *m_pDropZone;
	bool	m_bFirstTimeReloadFromAvatar;
	bool	m_bTaskDone;

public slots:
	void OnMenu_Close();
	void OnMenu_Startup();
	void onTaskRun_clicked();
	void onTaskDone_clicked();

private:
	Ui::fui_avatarClass ui;
};

#endif // FUI_AVATAR_H
