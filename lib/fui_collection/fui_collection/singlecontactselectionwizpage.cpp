#include "singlecontactselectionwizpage.h"

//GLOBAL:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;		

SingleContactSelectionWizPage::SingleContactSelectionWizPage(int nWizardPageID, QString strPageTitle, QWidget *parent /*= 0*/)
    : SimpleSelectionWizPage(nWizardPageID, strPageTitle)
{

}

SingleContactSelectionWizPage::~SingleContactSelectionWizPage()
{

}

void SingleContactSelectionWizPage::Initialize()
{
	//-------------------------------
	//Standard Initialize stuff.
	//-------------------------------
	if (m_bInitialized)
		return;
	m_bInitialized = true;
	//-------------------------------
	
	ReadFromCache();
	//Connect selected reports and complete state changed.
	//connect(ui.Selected_treeWidget, SIGNAL(ItemNumberChanged()), this, SLOT(CompleteChanged()));
	
	//Set recordset 
	Status err;
	//_SERVER_CALL(MainEntitySelector->ReadData(err, ENTITY_BUS_CONTACT, DbRecordSet(), m_recSelectTreeRecordSet))

	//ui.Selection_treeWidget->Initialize(&m_recSelectTreeRecordSet, false, true, false, true);
	//ui.Selected_treeWidget->Initialize(&m_recResultRecordSet, true, false, false, true);

	//Add the rest.
}

void SingleContactSelectionWizPage::CompleteChanged()
{

}
