#ifndef FUI_CUSTOMFIELDS_H
#define FUI_CUSTOMFIELDS_H

#include "fuibase.h"
#include "ui_fui_customfields.h"
#include "gui_core/gui_core/guifieldmanager.h"

class FUI_CustomFields : public FuiBase
{
	Q_OBJECT

public:
	FUI_CustomFields(QWidget *parent = 0);
	~FUI_CustomFields();

	QString GetFUIName();
	void on_selectionDataChange();

private slots:
	void on_cmbSelection_currentIndexChanged(int);
	void on_cmbDataType_currentIndexChanged(int);
	void OnSelectionTableDataChanged(int row,int col);
	void OnSelectionTableRowInserted(int row);

protected:
	
	void DataCheckBeforeWrite(Status &err);
	void DataPrepareForInsert(Status &err);
	void DataRead(Status &err,int nRecordID);
	void DataWrite(Status &err);
	void DataUnlock(Status &err);
	void prepareCacheRecord(DbRecordSet *recNewData);

private:
	Ui::FUI_CustomFields ui;
	GuiFieldManager *m_GuiFieldManagerMain;

	void RefreshSelectionComboEdit();
	void RefreshSelectionTableEdit();

	bool CheckSelectionValues();
	void RefreshDisplay();
	void SetMode(int nMode);
	DbRecordSet m_lstSelections;
};

#endif // FUI_CUSTOMFIELDS_H
