#ifndef DLG_RFPARSER_H
#define DLG_RFPARSER_H

#include <QtWidgets/QDialog>
#include <QTreeWidgetItem>
#include "generatedfiles/ui_dlg_rfparser.h"
#include "common/common/dbrecordset.h"
#include "common/common/status.h"

class Dlg_RFParser : public QDialog
{
	Q_OBJECT

public:
	Dlg_RFParser(QWidget *parent = 0);
	~Dlg_RFParser();

	bool Initialize(Status &err,QString strFilePath="", bool bForceFileSelectionDlg=false);
	void GetResult(DbRecordSet &lstApps,DbRecordSet &lstTemplates,DbRecordSet &lstInterfaces,QString &strRFFilePath);

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void OnItemChanged(QTreeWidgetItem *, int);

private:
	Ui::Dlg_RFParserClass ui;
	DbRecordSet m_lstApplications;
	DbRecordSet m_lstTemplates;
	DbRecordSet m_lstInterfaces;
	QString m_strRFFilePath;

	QTreeWidgetItem *m_parentItem_Apps;
	QTreeWidgetItem *m_parentItem_Templates;
	QTreeWidgetItem *m_parentItem_Interfaces;
	bool m_bApp,m_bTemp,m_bInter;
};

#endif // DLG_RFPARSER_H
