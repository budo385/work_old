#include "dlg_serialemailsmode.h"

Dlg_SerialEmailsMode::Dlg_SerialEmailsMode(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
}

Dlg_SerialEmailsMode::~Dlg_SerialEmailsMode()
{
}

void Dlg_SerialEmailsMode::on_btnOK_clicked()
{
	done(1);
}
	
void Dlg_SerialEmailsMode::on_btnCancel_clicked()
{
	done(0);
}

bool Dlg_SerialEmailsMode::GetUseAllEmails()
{
	return ui.radioAllEmails->isChecked();
}
