#include "fui_pbxconfig.h"

FUI_PBXConfig::FUI_PBXConfig(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	m_lstFwdContact1.addColumn(QVariant::Int, "BVC_CONTACT_ID");
	m_lstFwdContact1.addRow();

	m_lstFwdContact2 = m_lstFwdContact1;
	m_lstFwdContact3 = m_lstFwdContact1;
	m_lstFwdContact4 = m_lstFwdContact1;
	m_lstFwdContact5 = m_lstFwdContact1;
	m_lstFwdContact6 = m_lstFwdContact1;
	m_lstFwdContact7 = m_lstFwdContact1;
	m_lstFwdContact8 = m_lstFwdContact1;
	m_lstFwdContact9 = m_lstFwdContact1;

	ui.frameOrganization->Initialize( ENTITY_BUS_CONTACT, &m_lstFwdContact1, "BVC_CONTACT_ID" );
	ui.frameOrganization_2->Initialize( ENTITY_BUS_CONTACT, &m_lstFwdContact2, "BVC_CONTACT_ID" );
	ui.frameOrganization_3->Initialize( ENTITY_BUS_CONTACT, &m_lstFwdContact3, "BVC_CONTACT_ID" );
	ui.frameOrganization_4->Initialize( ENTITY_BUS_CONTACT,&m_lstFwdContact4,  "BVC_CONTACT_ID" );
	ui.frameOrganization_5->Initialize( ENTITY_BUS_CONTACT, &m_lstFwdContact5, "BVC_CONTACT_ID" );
	ui.frameOrganization_6->Initialize( ENTITY_BUS_CONTACT, &m_lstFwdContact6,"BVC_CONTACT_ID" );
	ui.frameOrganization_7->Initialize( ENTITY_BUS_CONTACT, &m_lstFwdContact7, "BVC_CONTACT_ID" );
	ui.frameOrganization_8->Initialize( ENTITY_BUS_CONTACT, &m_lstFwdContact8, "BVC_CONTACT_ID" );
	ui.frameOrganization_9->Initialize( ENTITY_BUS_CONTACT,&m_lstFwdContact9,  "BVC_CONTACT_ID" );


	//define column headers
	m_lstGrid1.addColumn(QVariant::Bool, "BCNT_ONLINE");
	m_lstGrid1.addColumn(QVariant::String, "BCNT_FIRSTNAME");
	m_lstGrid1.addColumn(QVariant::String, "BCMA_ORGANIZATIONNAME");
	m_lstGrid1.addColumn(QVariant::ByteArray, "BCMA_STREET");
	ui.tableView_2->Initialize(&m_lstGrid1);

	DbRecordSet columns;
	ui.tableView_2->AddColumnToSetup(columns,"BCNT_ONLINE",tr("Online"),80,true, "");
	ui.tableView_2->AddColumnToSetup(columns,"BCNT_FIRSTNAME",tr("Account"),150,true, "");
	ui.tableView_2->AddColumnToSetup(columns,"BCMA_ORGANIZATIONNAME",tr("Phone Number"),150,true, "");
	ui.tableView_2->AddColumnToSetup(columns,"BCMA_STREET",tr("Forwarded"),250,true, "");
	ui.tableView_2->SetColumnSetup(columns);

	m_lstGrid2.addColumn(QVariant::String, "BCNT_ACCOUNT");
	m_lstGrid2.addColumn(QVariant::String, "BCMA_COMMAND");
	m_lstGrid2.addColumn(QVariant::String, "BCMA_FORWARD_TO");
	m_lstGrid2.addColumn(QVariant::String, "BCMA_TIME");
	m_lstGrid2.addColumn(QVariant::String, "BCMA_FREQUENCE");
	m_lstGrid2.addColumn(QVariant::String, "BCMA_VALID_FROM");
	m_lstGrid2.addColumn(QVariant::String, "BCMA_VALID_TO");
	ui.tableView->Initialize(&m_lstGrid2);

	columns.clear();
	ui.tableView->AddColumnToSetup(columns,"BCNT_ACCOUNT",tr("Phone account"),130,true, "");
	ui.tableView->AddColumnToSetup(columns,"BCMA_COMMAND",tr("Command"),80,true, "");
	ui.tableView->AddColumnToSetup(columns,"BCMA_FORWARD_TO",tr("Foward To"),100,true, "");
	ui.tableView->AddColumnToSetup(columns,"BCMA_TIME",tr("Time"),80,true, "");
	ui.tableView->AddColumnToSetup(columns,"BCMA_FREQUENCE",tr("Frequence"),80,true, "");
	ui.tableView->AddColumnToSetup(columns,"BCMA_VALID_FROM",tr("Valid From"),80,true, "");
	ui.tableView->AddColumnToSetup(columns,"BCMA_VALID_TO",tr("Valid To"),80,true, "");
	ui.tableView->SetColumnSetup(columns);

	//set icons
	ui.toolButton_3->setIcon(QIcon(":SAP_Select.png"));
	ui.toolButton->setIcon(QIcon(":SAP_Modify.png"));
	ui.toolButton_2->setIcon(QIcon(":SAP_Clear.png"));
}

FUI_PBXConfig::~FUI_PBXConfig()
{
}

