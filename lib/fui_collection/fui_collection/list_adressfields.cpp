#include "list_adressfields.h"

List_AdressFields::List_AdressFields(QWidget *parent)
	: ListWidget(parent)
{

}

List_AdressFields::~List_AdressFields()
{

}

//get drop value inside brackets:
void List_AdressFields::GetDropValue(DbRecordSet &values, bool bSelectItemChildren) const
{
	values.copyDefinition(m_lstData);
	values.merge(m_lstData,true);

	for(int i=0;i<values.getRowCount();++i)
	{
		values.setData(i,0,"["+values.getDataRef(i,0).toString()+"]");
	}

}
