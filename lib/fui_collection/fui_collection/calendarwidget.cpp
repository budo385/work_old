#include "calendarwidget.h"

#include <QVBoxLayout>
#include <QRect>

#include "calendargraphicsview.h"
#include "calendarmultidaygraphicsview.h"
#include "calendartableview.h"
#include "calendarmultidayentityitem.h"

#include "bus_core/bus_core/customavailability.h"
#include "db_core/db_core/dbsqltableview.h"

void TopRightItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

}

void TopRightItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	emit ResizeColumnsToFit(true);
}

TableView::TableView(int nItemLeftOffSet, bool bIsHeaderTable /*= false*/, QWidget *parent /*= 0*/)
{
	m_bItemIsResizing = false;
	m_bStartSelection = false;
	m_nItemLeftOffSet = nItemLeftOffSet;
	m_bIsHeaderTable = bIsHeaderTable;

	setFrameStyle(QFrame::NoFrame);
	setAutoScroll(true);
	setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	setShowGrid(false);
}

void TableView::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
	/*
	if(m_bItemIsResizing)
		return;

	QItemSelectionRange selRange = selected.first();
	QModelIndex topLeft = selRange.topLeft();
	QModelIndex bottomRight = selRange.bottomRight();

	QItemSelectionRange deselRange;
	if (deselected.count())
	{
		deselRange = deselected.first();
		QModelIndex topLeft1;
		QModelIndex bottomRight1;
		if (deselRange.topLeft().isValid())
		{
			topLeft1 = deselRange.topLeft();
			qDebug() << 2 << topLeft1.row() << topLeft1.column();
		}
		if (deselRange.bottomRight().isValid())
		{
			bottomRight1 = deselRange.bottomRight();
			qDebug() << 3 << bottomRight1.row() << bottomRight1.column();
		}
	}
	qDebug() << 1 << topLeft.row() << topLeft.column() << bottomRight.row() << bottomRight.column();

*/
	//emit SelectionChanged(selected, deselected);
}

void TableView::mouseDoubleClickEvent(QMouseEvent *event)
{
	QModelIndex index = indexAt(event->pos());
	if (!index.isValid()) 
		return QAbstractItemView::mouseDoubleClickEvent(event);

	QPersistentModelIndex persistent = index;

	if (((visualRect(index).x()<event->pos().x()) && ((visualRect(index).x()+m_nItemLeftOffSet)>event->pos().x())) && !m_bIsHeaderTable)
		emit openMultiDayEvent(persistent);
	else
		emit doubleClicked(persistent);
}

void TableView::mousePressEvent(QMouseEvent *event)
{
	QTableView::mousePressEvent(event);
	QModelIndex index = indexAt(event->pos());
	if (index.isValid())
	{
		m_bStartSelection = true;
		m_topLeftSelectedIndex = index;
	}
}

void TableView::mouseReleaseEvent(QMouseEvent *event)
{
	QTableView::mouseReleaseEvent(event);
	QModelIndex index = indexAt(event->pos());
	if (m_bStartSelection)
	{
		m_bStartSelection = false;
		if (index.isValid() && index != m_topLeftSelectedIndex)
		{
			m_bottomRightSelectedIndex = index;
			emit SelectionChanged(m_topLeftSelectedIndex, m_bottomRightSelectedIndex);
		}
	}
}

CalendarWidget::CalendarWidget(QWidget *parent /*= 0*/)
	: QWidget(parent)
{
	m_recMultiDayEvents.addColumn(QVariant::Int, "ROW");
	m_recMultiDayEvents.addColumn(QVariant::Int, "COLUMN");
	m_recMultiDayEvents.addColumn(QVariant::Int, "CENT_ID");
	m_recMultiDayEvents.addColumn(QVariant::Int, "BCEP_ID");
	m_recMultiDayEvents.addColumn(QVariant::Int, "BCEV_ID");
	m_recMultiDayEvents.addColumn(QVariant::Int, "ENTITY_ID");
	m_recMultiDayEvents.addColumn(QVariant::Int, "ENTITY_TYPE");

	m_recEntites = NULL;

	m_nVertHeaderWidth				= 80;
	m_nHeaderHeight					= 27;
	m_nPersonHeaderHeight			= 27;
	m_nRowHeight					= 20;
	m_nItemLeftOffSet				= 10;
	m_nCalendarViewSections			= -1;
	
	m_pCalendarMultiDayGraphicsView = new CalendarMultiDayGraphicsView(this);
	m_pCalendarGraphicsView			= new CalendarGraphicsView(this);
	m_pSplitter						= new QSplitter(Qt::Vertical);
	connect(m_pSplitter, SIGNAL(splitterMoved(int, int)), this, SLOT(on_splitterMoved(int, int)));
	m_pLayout						= new QBoxLayout(QBoxLayout::TopToBottom);
	m_pLayout->setContentsMargins(0, 0, 0, 0);
	setLayout(m_pLayout);

	connect(m_pCalendarGraphicsView, SIGNAL(UpdateMultiDayCalendar()),		m_pCalendarMultiDayGraphicsView, SLOT(on_UpdateCalendar()));
	connect(m_pCalendarGraphicsView, SIGNAL(columnResized(int, int, bool)), m_pCalendarMultiDayGraphicsView, SLOT(on_columnResizedFromSimpleCalendar(int, int, bool)));
	connect(m_pCalendarGraphicsView, SIGNAL(scrollBar_valueChanged(int)),	m_pCalendarMultiDayGraphicsView, SLOT(on_scrollBar_valueChanged(int)));
	connect(m_pCalendarGraphicsView, SIGNAL(ItemResized(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)), this, SLOT(on_ItemResized(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)));
	connect(m_pCalendarGraphicsView, SIGNAL(ItemMoved(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)), this, SIGNAL(ItemMoved(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)));
	connect(m_pCalendarGraphicsView, SIGNAL(ItemsDelete(DbRecordSet)), this, SIGNAL(ItemsDelete(DbRecordSet)));
	connect(m_pCalendarGraphicsView, SIGNAL(ItemModify(int, int, int, int)), this, SLOT(on_ModifyItem(int, int, int, int)));
	connect(m_pCalendarGraphicsView, SIGNAL(ItemView(int, int, int, int)), this, SLOT(on_ViewItem(int, int, int, int)));

	//connect(m_pCalendarGraphicsView, SIGNAL(DropEntityOnItem(int, int, int, int, int, int, int, bool)), this, SIGNAL(DropEntityOnItem(int, int, int, int, int, int, int, bool)));
	connect(m_pCalendarGraphicsView, SIGNAL(ItemDelete(int, int, int, int, int, int)), this, SLOT(on_DeleteItem(int, int, int, int, int, int)));
	connect(m_pCalendarGraphicsView, SIGNAL(tableCellDoubleClicked(QDateTime, QDateTime, int, int)), this, SLOT(on_tableCellDoubleClicked(QDateTime, QDateTime, int, int)));
	connect(m_pCalendarGraphicsView, SIGNAL(selectionChanged(QDateTime, QDateTime, DbRecordSet)), this, SIGNAL(selectionChanged(QDateTime, QDateTime, DbRecordSet)));
	connect(m_pCalendarGraphicsView, SIGNAL(openMultiDayEvent(QModelIndex)), this, SLOT(on_openMultiDayEvent(QModelIndex)));
	connect(m_pCalendarGraphicsView, SIGNAL(TaskItemClicked(int, int, int, QDateTime, bool, int, int, int)), this, SIGNAL(TaskItemClicked(int, int, int, QDateTime, bool, int, int, int)));
	connect(m_pCalendarGraphicsView, SIGNAL(TaskItemDoubleClicked(int, int, int, QDateTime, bool, int, int, int)), this, SIGNAL(TaskItemDoubleClicked(int, int, int, QDateTime, bool, int, int, int)));
	
	//connect(m_pCalendarMultiDayGraphicsView, SIGNAL(scrollBar_valueChanged(int)), m_pCalendarGraphicsView, SLOT(on_scrollBar_valueChanged(int)));
	connect(m_pCalendarMultiDayGraphicsView, SIGNAL(ItemModify(int, int, int, int)), this, SLOT(on_ModifyItem(int, int, int, int)));
	connect(m_pCalendarMultiDayGraphicsView, SIGNAL(DropEntityOnItem(int, int, int, int, int, int, int, bool)), this, SIGNAL(DropEntityOnItem(int, int, int, int, int, int, int, bool)));
	connect(m_pCalendarMultiDayGraphicsView, SIGNAL(ItemDelete(int, int, int, int)), this, SLOT(on_DeleteItemMultyDay(int, int, int, int)));
	connect(m_pCalendarMultiDayGraphicsView, SIGNAL(tableCellDoubleClicked(QDateTime, QDateTime, int, int)), this, SLOT(on_tableCellDoubleClicked(QDateTime, QDateTime, int, int)));
	connect(m_pCalendarMultiDayGraphicsView, SIGNAL(MultiDayItemResized(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)), this, SLOT(on_MultiDayItemResized(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)));
	connect(m_pCalendarMultiDayGraphicsView, SIGNAL(MultiDayItemMoved(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)), this, SIGNAL(MultiDayItemMoved(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)));
}

CalendarWidget::~CalendarWidget()
{
	m_recEntites					= NULL;
	m_pCalendarGraphicsView			= NULL;
	m_pCalendarMultiDayGraphicsView	= NULL;
	m_pSplitter						= NULL;
	m_pLayout						= NULL;
}

void CalendarWidget::Initialize(QDate startDate, QDate endDate, CalendarWidget::timeScale scale, DbRecordSet *recEntites, bool bResizeColumnsToFit /*= true*/, QTime scrollToTime /*= QTime(6,0)*/, int nCalendarViewSections /*= 0*/)
{
	//How it looks and feel.
	if (m_nCalendarViewSections>=0)
	{
		if (m_nCalendarViewSections==1)
		{
			m_pLayout->removeWidget(m_pCalendarMultiDayGraphicsView);
		}
		else if (m_nCalendarViewSections==2)
		{
			m_pLayout->removeWidget(m_pCalendarGraphicsView);
		}
		else
		{
			m_pLayout->removeWidget(m_pSplitter);
		}
	}

	if (nCalendarViewSections==1)
	{
		m_pLayout->addWidget(m_pCalendarMultiDayGraphicsView);
	}
	else if (nCalendarViewSections==2)
	{
		m_pLayout->addWidget(m_pCalendarGraphicsView);
	}
	else
	{

		m_pSplitter->addWidget(m_pCalendarMultiDayGraphicsView);
		m_pSplitter->addWidget(m_pCalendarGraphicsView);
		m_pSplitter->setStretchFactor(1, 10);
		if (m_splitterState.isEmpty())
		{
			QList<int> splitterSizes;
			splitterSizes<<80<<400;
			m_pSplitter->setSizes(splitterSizes);
		}
		else
		{
			m_pSplitter->restoreState(m_splitterState);
		}
		m_pLayout->addWidget(m_pSplitter);
	}


	QTime timer;
	timer.start();
	qDebug() << " /+++++++++++++ CalendarWidget::Initialize() " << timer.elapsed();

	m_timeScale			= scale;
	m_startDate			= startDate;
	m_nDayInterval		= startDate.daysTo(endDate)+1;
	m_recEntites		= recEntites;
	m_hshItemToResources.clear();
	m_hshMultiDayItemToResources.clear();
	m_nCalendarViewSections=nCalendarViewSections;
	m_nVertHeaderWidth = getVertHeaderWidthForLowerCalendar();

	//	qDebug() << " /+++++++++++++ CalendarWidget::Initialize(2) " << timer.elapsed();
	//	qDebug() << " /+++++++++++++ CalendarWidget::Initialize(3) " << timer.elapsed();

	if (m_nCalendarViewSections==1)
	{
		dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->Initialize(m_startDate, m_nDayInterval, m_timeScale, m_recEntites, bResizeColumnsToFit);
	}
	else if (m_nCalendarViewSections==2)
	{
		dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->Initialize(m_startDate, m_nDayInterval, m_timeScale, m_recEntites, bResizeColumnsToFit, scrollToTime);
	}
	else
	{
		dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->Initialize(m_startDate, m_nDayInterval, m_timeScale, m_recEntites, bResizeColumnsToFit, scrollToTime);
		dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->Initialize(m_startDate, m_nDayInterval, m_timeScale, m_recEntites, bResizeColumnsToFit);
	}

	if (bResizeColumnsToFit)
	{
		if (m_nCalendarViewSections==1)
		{
			dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->ResizeColumnsToFit(bResizeColumnsToFit);
		}
		else if (m_nCalendarViewSections==2)
		{
			dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->ResizeColumnsToFit(bResizeColumnsToFit);
		}
		else
		{
			dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->ResizeColumnsToFit(bResizeColumnsToFit);
			dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->ResizeColumnsToFit(bResizeColumnsToFit);
		}
	}
/*	qDebug() << " /+++++++++++++ CalendarWidget::Initialize(4) " << timer.elapsed();
	qDebug() << " /+++++++++++++ CalendarWidget::Initialize(5) " << timer.elapsed();
	qDebug() << " /+++++++++++++ CalendarWidget::Initialize(6) " << timer.elapsed();*/
}

int CalendarWidget::getVertHeaderWidthForUpperCalendar()
{
	if (m_recEntites==NULL)
		return 0;

	if (m_nCalendarViewSections==1)
		return 	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getVertHeaderWidth();
	else
		return m_nVertHeaderWidth;
}

int CalendarWidget::getVertHeaderWidthForLowerCalendar()
{
	return m_nVertHeaderWidth;
}

int CalendarWidget::getPersonHeaderHeight()
{
	if (m_recEntites->getRowCount() >1)
		return m_nPersonHeaderHeight;
	else
		return 0;
}

int CalendarWidget::getDateHeaderHeight()
{
	return m_nHeaderHeight;
}

QDate CalendarWidget::getStartDate()
{
	return m_startDate;
}

int	CalendarWidget::getDateInterval()
{
	return m_nDayInterval;
}

int CalendarWidget::getMultiHeaderHeight()
{
	return getDateHeaderHeight();
}

int CalendarWidget::getCurrentMultiHeaderHeight()
{
	if (m_recEntites->getRowCount() >1)
		return getMultiHeaderHeight();
	else
		return 0;
}

CalendarWidget::timeScale CalendarWidget::getTimeScale()
{
	return m_timeScale;
}

QAbstractTableModel* CalendarWidget::TableModel()
{
	return dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->Model();
}

QAbstractTableModel* CalendarWidget::HeaderTableModel()
{
	return dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->TableModel();
}

int CalendarWidget::getCalendarColumnWidth()
{
	return dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->GetColumnWidth();
}

int CalendarWidget::getMultiDayCalendarColumnWidth()
{
	return dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->GetColumnWidth();
}

void CalendarWidget::setTimeScale(int nTimeScale)
{
	m_timeScale = (CalendarWidget::timeScale)nTimeScale;
	dynamic_cast<CalendarTableModel*>(TableModel())->setTimeScale(m_timeScale);
	dynamic_cast<CalendarTableView*>(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView())->Table()->setModel(0);
	dynamic_cast<CalendarTableView*>(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView())->Table()->setModel(dynamic_cast<CalendarTableModel*>(TableModel()));
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->DestroyVerticalHeader();
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->CreateVerticalHeader();
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->UpdateCalendarSignal();
	
	//dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->Initialize(m_startDate, m_nDayInterval, m_timeScale, &m_recEntites);
}

QDateTime CalendarWidget::GetTopLeftVisibleItem()
{
	return dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->GetTopLeftVisibleItem();
}

void CalendarWidget::SetTopLeftVisibleItem(QDateTime dateTime)
{
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->SetTopLeftVisibleItem(dateTime);
}

void CalendarWidget::UpdateCalendarFromFui()
{
	if (m_nCalendarViewSections==1)
	{
		dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->UpdateCalendarSignal();
	}
	else if (m_nCalendarViewSections==2)
	{
		dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->UpdateCalendarSignal();
	}
	else
	{
		dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->UpdateCalendarSignal();
		dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->UpdateCalendarSignal();
	}

	ReorderMultiDay();
}

void CalendarWidget::SelectEntityItems(int nEntityID, int nEntityType)
{
//#ifndef _DEBUG
	if(!CustomAvailability::IsAvailable_AS_CalendarViewWizard())
	{
		return;
	}
//#endif

	QHashIterator<QGraphicsItem*, DbRecordSet> i(m_hshItemToResources);
	while (i.hasNext()) 
	{
		i.next();
		CalendarEntityItem *item = dynamic_cast<CalendarEntityItem*>(i.key());
		if (!item)
			continue;

		item->SetSelected(false);
		
		//Select even first row.
		//if (item->getItemEntityID()==nEntityID && item->getItemEntityType()==nEntityType)
		//	continue;
		
		DbRecordSet recTmp = i.value();
		if (IsEntityInItem(nEntityID, nEntityType, recTmp))
			item->SetSelected();
	}

	QHashIterator<QGraphicsItem*, DbRecordSet> j(m_hshMultiDayItemToResources);
	qDebug()<<m_hshMultiDayItemToResources.count();
	while (j.hasNext()) 
	{
		j.next();
		CalendarMultiDayEntityItem *item = dynamic_cast<CalendarMultiDayEntityItem*>(j.key());
		item->SetSelected(false);

		//Select even first row.
		//if (item->getItemEntityID()==nEntityID && item->getItemEntityType()==nEntityType)
		//	continue;

		DbRecordSet recTmp = j.value();
		if (IsEntityInItem(nEntityID, nEntityType, recTmp))
			item->SetSelected();
	}
}

QByteArray CalendarWidget::GetSpliterState()
{
	return m_pSplitter->saveState();
}

void CalendarWidget::SetSpliterState(QByteArray spliterState)
{
	if (m_nCalendarViewSections!=0)
		return;

	m_pSplitter->restoreState(spliterState);
}

void CalendarWidget::AddCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, QDateTime startDateTime, 
					 QDateTime endDateTime, QString strTitle, QString strLocation, QString strColor, 
					 QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
					 QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, bool bForInsert, int nBcep_Status, 
					 int nBCEV_IS_INFORMATION, bool bIsProposed, DbRecordSet recItemAssignedResources)
{
	QGraphicsItem *item = NULL;
	
	//Insert if normal adding or if from insert (from observer pattern) check does it exists.
	if ((!bForInsert) || (bForInsert && (!dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->ItemExists(nCentID, nEntityID, nEntityType, nBCOL_ID))))
		dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->AddCalendarItem(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, strLocation, strColor, 
									pixIcon, strOwner, strResource, strContact, strProjects, strDescription, hshBrakeStart, hshBrakeEnd, nBcep_Status, 
									nBCEV_IS_INFORMATION, bIsProposed, item);

	m_hshItemToResources.insert(item, recItemAssignedResources);
}

void CalendarWidget::UpdateCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime startDateTime, 
									 QDateTime endDateTime, QString strTitle, QString strLocation, QString strColor, 
									 QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
									 QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, int nBcep_Status, 
									 int nBCEV_IS_INFORMATION, bool bIsProposed, DbRecordSet recItemAssignedResources)
{
	QGraphicsItem *item = NULL;
	
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->UpdateCalendarItem(nCentID, nBcepID, nBcevID, nBCOL_ID, startDateTime, endDateTime, strTitle, strLocation, strColor, 
		pixIcon, strOwner, strResource, strContact, strProjects, strDescription, hshBrakeStart, hshBrakeEnd, nBcep_Status, nBCEV_IS_INFORMATION, 
		bIsProposed, item);

	m_hshItemToResources.insert(item, recItemAssignedResources);

	QDateTime correctedStartDateTime = startDateTime;
	QDateTime correctedEndDateTime = endDateTime;
	GetCorrectedDateTimes(correctedStartDateTime, correctedEndDateTime);

	QGraphicsItem *multiDayItem = NULL;
	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->UpdateMultiDayCalendarItem(nCentID, nBcepID, nBcevID, nBCOL_ID, startDateTime,
		endDateTime, correctedStartDateTime, correctedEndDateTime, strTitle, strLocation, strColor, pixIcon, strOwner, strResource, strContact, strProjects, strDescription, nBcep_Status, 
		nBCEV_IS_INFORMATION, bIsProposed, multiDayItem);
	m_hshMultiDayItemToResources.insert(multiDayItem, recItemAssignedResources);
	
	if (startDateTime.date() != endDateTime.date())
		UpdateMultiDay(nCentID, nBcepID, nBcevID, startDateTime, endDateTime);
}

void CalendarWidget::AddMultiDay(bool bFromSingleDayEvent, int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType, QDateTime startDateTime, QDateTime endDateTime)
{
	QModelIndex startIndex;
	qreal remain;
	dynamic_cast<CalendarTableModel*>(TableModel())->getItem(nEntityID, nEntityType, startDateTime, startIndex, remain);
	int nStartRow = startIndex.row();
	int nColum = startIndex.column();

	QModelIndex endIndex;
	dynamic_cast<CalendarTableModel*>(TableModel())->getItem(nEntityID, nEntityType, endDateTime, endIndex, remain);
	int nEndRow = endIndex.row();

	for (int i = nStartRow; i <= nEndRow; i++)
	{
		QModelIndex ind = dynamic_cast<CalendarTableModel*>(TableModel())->index(i, nColum);
		if (!bFromSingleDayEvent)
		{
			dynamic_cast<CalendarTableModel*>(TableModel())->AddMultiDayIndex(ind);
			m_recMultiDayEvents.addRow();
			int nRow = m_recMultiDayEvents.getRowCount()-1;
			m_recMultiDayEvents.setData(nRow, "ROW", i);
			m_recMultiDayEvents.setData(nRow, "COLUMN", nColum);
			m_recMultiDayEvents.setData(nRow, "CENT_ID", nCentID);
			m_recMultiDayEvents.setData(nRow, "BCEP_ID", nBcepID);
			m_recMultiDayEvents.setData(nRow, "BCEV_ID", nBcevID);
			m_recMultiDayEvents.setData(nRow, "ENTITY_ID", nEntityID);
			m_recMultiDayEvents.setData(nRow, "ENTITY_TYPE", nEntityType);
		}
	}
}

void CalendarWidget::RemoveMultiDay(int nCentID, int nBcepID, int nBcevID)
{
	DbRecordSet tmp;
	tmp.copyDefinition(m_recMultiDayEvents);
	m_recMultiDayEvents.clearSelection();
	m_recMultiDayEvents.find("CENT_ID", nCentID);
	tmp.merge(m_recMultiDayEvents, true);
	m_recMultiDayEvents.deleteSelectedRows();
	int nRowCount = tmp.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nRow = tmp.getDataRef(i, "ROW").toInt();
		int nColumn = tmp.getDataRef(i, "COLUMN").toInt();
		//Check is this row and column assigned to some other multi day event.
		m_recMultiDayEvents.clearSelection();
		m_recMultiDayEvents.find("ROW", nRow);
		m_recMultiDayEvents.find("COLUMN", nColumn, false, true, true);
		if (!m_recMultiDayEvents.getSelectedCount())
		{
			QModelIndex ind = dynamic_cast<CalendarTableModel*>(TableModel())->index(nRow, nColumn);
			dynamic_cast<CalendarTableModel*>(TableModel())->RemoveMultiDayIndex(ind);
		}
	}
}

void CalendarWidget::UpdateMultiDay(int nCentID, int nBcepID, int nBcevID, QDateTime startDateTime, QDateTime endDateTime)
{
	int nCounter = 0;
	QHash<int, int> hshEntityID;
	QHash<int, int> hshEntityType;
	int nRowCount = m_recMultiDayEvents.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nRowCentID = m_recMultiDayEvents.getDataRef(i, "CENT_ID").toInt();

		if (nRowCentID == nCentID)
		{
			bool bInsertEntity = true;
			int nRowEntityID = m_recMultiDayEvents.getDataRef(i, "ENTITY_ID").toInt();
			int nRowEntityType = m_recMultiDayEvents.getDataRef(i, "ENTITY_TYPE").toInt();

			QList<int> nCounterForEntityType;
			nCounterForEntityType = hshEntityType.keys(nRowEntityType);
			QListIterator<int> iter(nCounterForEntityType);
			while (iter.hasNext())
			{
				int nCounterKey = iter.next();
				if (hshEntityID.value(nCounterKey) == nRowEntityID)
					bInsertEntity = false;
			}

			if (bInsertEntity)
			{
				hshEntityID.insert(nCounter, nRowEntityID);
				hshEntityType.insert(nCounter, nRowEntityType);
				nCounter++;
			}
		}
	}

	RemoveMultiDay(nCentID, nBcepID, nBcevID);

	int nHashCount = hshEntityType.count();
	for (int i = 0; i < nHashCount; i++)
	{
		int nRowEntityID = hshEntityID.value(i);
		int nRowEntityType = hshEntityType.value(i);
		
		int nRowCount = startDateTime.daysTo(endDateTime)+1;
		for (int j = 0; j < nRowCount; j++)
		{
			QDateTime rowStartDateTime;
			QDateTime rowEndDateTime;
			if (j == 0)
				rowStartDateTime = startDateTime;
			else
				rowStartDateTime = QDateTime(startDateTime.date().addDays(j), QTime(0,0));

			if (j == (nRowCount-1))
				rowEndDateTime = endDateTime;
			else
				rowEndDateTime = QDateTime(startDateTime.date().addDays(j), QTime(23,59, 59));

			AddMultiDay(false, nCentID, nBcepID, nBcevID, nRowEntityID, nRowEntityType, rowStartDateTime, rowEndDateTime);
		}
	}
	
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView()->update();
}

void CalendarWidget::ReorderMultiDay()
{
	QMultiHash<int, QGraphicsItem*> lstMultiDayCalendarEntityItems = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->GetMultiDayEventItems();
	QHashIterator<int, QGraphicsItem*> i(lstMultiDayCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		CalendarMultiDayEntityItem *item = dynamic_cast<CalendarMultiDayEntityItem*>(i.value());
		int nCentID = item->getItemCentID();
		int nBcepID = item->getItemBcepID();
		int nBcevID = item->getItemBcevID();
		QDateTime startDateTime, endDateTime;
		item->getStartEndDateTime(startDateTime, endDateTime);
		if (startDateTime!=endDateTime)
		{
			UpdateMultiDay(nCentID, nBcepID, nBcevID, startDateTime, endDateTime);
		}
	}
}

void CalendarWidget::AddCalendarReservation(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, QDateTime startDateTime, QDateTime endDateTime)
{
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->AddCalendarReservation(nEntityID, nEntityType, nBCRS_ID, nBCRS_IS_POSSIBLE, startDateTime, endDateTime);
	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->AddMultiDayCalendarReservation(nEntityID, nEntityType, nBCRS_ID, nBCRS_IS_POSSIBLE, startDateTime, endDateTime);
}

void CalendarWidget::RemoveSingleDayCalendarItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	QList<QGraphicsItem*> Items;
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->RemoveCalendarItem(nCENT_ID, nBcepID, nBcevID, nBCOL_ID, Items);
	QListIterator<QGraphicsItem*> i(Items);
	while (i.hasNext())
	{
		m_hshItemToResources.remove(i.next());
	}
	
	QList<QGraphicsItem*> multiDayItems;
	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->RemoveCalendarItem(nCENT_ID, nBcepID, nBcevID, nBCOL_ID, multiDayItems);
	QListIterator<QGraphicsItem*> j(multiDayItems);
	while (j.hasNext())
	{
		m_hshMultiDayItemToResources.remove(j.next());
	}
}	

void CalendarWidget::RemoveMultiDayEventItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	RemoveMultiDay(nCENT_ID, nBcepID, nBcevID);
	QList<QGraphicsItem*> Items;
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->RemoveCalendarItem(nCENT_ID, nBcepID, nBcevID, nBCOL_ID, Items);
	QListIterator<QGraphicsItem*> i(Items);
	while (i.hasNext())
	{
		m_hshItemToResources.remove(i.next());
	}
	
	QList<QGraphicsItem*> multiDayItems;
	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->RemoveCalendarItem(nCENT_ID, nBcepID, nBcevID, nBCOL_ID, multiDayItems);
	QListIterator<QGraphicsItem*> j(multiDayItems);
	while (j.hasNext())
	{
		m_hshMultiDayItemToResources.remove(j.next());
	}
}

void CalendarWidget::AddMultiDayCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, QDateTime startDateTime, QDateTime endDateTime, 
											 QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, QString strOwner, 
											 QString strResource, QString strContact, QString strProjects, QString strDescription, bool bForInsert, 
											 int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed, DbRecordSet recItemAssignedResources)
{
	//Insert if normal adding or if from insert (from observer pattern) check does it exists.
	if ((!bForInsert) || (bForInsert && (!dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->ItemExists(nCentID, 
		nEntityID, nEntityType, nBCOL_ID))))
	{
		QDateTime correctedStartDateTime = startDateTime;
		QDateTime correctedEndDateTime	 = endDateTime;
		GetCorrectedDateTimes(correctedStartDateTime, correctedEndDateTime);

		//QString sss = startDateTime.toString();
		//QString sssss = endDateTime.toString();

		//QString s = correctedStartDateTime.toString();
		//QString ss = correctedEndDateTime.toString();

		QGraphicsItem *item = NULL;
		dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->AddMultiDayCalendarItem(nCentID, nBcepID, nBcevID, nBCOL_ID, 
		nEntityID, nEntityType, startDateTime, endDateTime, correctedStartDateTime, correctedEndDateTime, strTitle, strLocation, strColor, pixIcon, strOwner, strResource, strContact, strProjects, 
		strDescription, nBcep_Status, nBCEV_IS_INFORMATION, bIsProposed, item);

		m_hshMultiDayItemToResources.insert(item, recItemAssignedResources);
		//_DUMP(recItemAssignedResources);
	}
}

void CalendarWidget::AddCalendarTaskItems(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, QPixmap Pixmap, int nEntityID, int nEntityType, int nTypeID)
{
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->AddCalendarTaskItems(nCENT_ID, nBTKS_ID, nCENT_SYSTEM_TYPE_ID, datTaskDate, bIsStart, Pixmap, nEntityID, nEntityType, nTypeID);
}

void CalendarWidget::getSelectedItemDateTime(QDateTime &startDateTime, QDateTime &endDateTime)
{
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getSelectedItemDateTime(startDateTime, endDateTime);
}

void CalendarWidget::on_ItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, 
											int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo)
{
	emit ItemResized(datSelectionFrom, datSelectionTo, nEntityID, nEntityType, nCentID, nBcepID, nBcevID, nBCOL_ID, datItemFrom, datItemTo);
}

void CalendarWidget::on_MultiDayItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, 
									int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo)
{
	emit MultiDayItemResized(datSelectionFrom, datSelectionTo, nEntityID, nEntityType, nCentID, nBcepID, nBcevID, nBCOL_ID, datItemFrom, datItemTo);
}

void CalendarWidget::on_DeleteItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType)
{
	emit DeleteItem(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType);
}

void CalendarWidget::on_DeleteItemMultyDay(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	emit MultiDayDeleteItem(nCentID, nBcepID, nBcevID, nBCOL_ID);
}

void CalendarWidget::on_ModifyItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	emit ModifyItem(nCentID, nBcepID, nBcevID, nBCOL_ID);
}

void CalendarWidget::on_ViewItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	emit ViewItem(nCentID, nBcepID, nBcevID, nBCOL_ID);
}


void CalendarWidget::on_tableCellDoubleClicked(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType)
{
	emit tableCellDoubleClicked(datSelectionFrom, datSelectionTo, nEntityID, nEntityType);
}

void CalendarWidget::on_openMultiDayEvent(QModelIndex index)
{
	DbRecordSet tmp;
	tmp.copyDefinition(m_recMultiDayEvents);
	int nColumn = index.column();
	int nRow	= index.row();
	m_recMultiDayEvents.clearSelection();
	m_recMultiDayEvents.find("ROW", nRow);
	m_recMultiDayEvents.find("COLUMN", nColumn, false, true, true);
	tmp.merge(m_recMultiDayEvents, true);
	
	QHash<int, int> hshMultiDayCentIDs;
	QHash<int, int> hshMultiDayBcepIDs;
	QHash<int, int> hshMultiDayBcevIDs; 
	int nRowCount = tmp.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		hshMultiDayCentIDs.insert(i, tmp.getDataRef(i, "CENT_ID").toInt());
		hshMultiDayBcepIDs.insert(i, tmp.getDataRef(i, "BCEP_ID").toInt());
		hshMultiDayBcevIDs.insert(i, tmp.getDataRef(i, "BCEV_ID").toInt());
	}
	
	emit openMultiDayEvent(hshMultiDayCentIDs, hshMultiDayBcepIDs, hshMultiDayBcevIDs); 
}

void CalendarWidget::on_splitterMoved(int pos, int index)
{
	m_splitterState=m_pSplitter->saveState();
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->UpdateCalendarSignalFromOutside();
	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->UpdateCalendarSignalFromOutside();
}

void CalendarWidget::on_defineAvailability(DbRecordSet recSelectedEntityItemsInLeftColumn)
{
	emit defineAvailability(recSelectedEntityItemsInLeftColumn);
}

void CalendarWidget::on_markAsFinal(DbRecordSet recSelectedEntityItemsInLeftColumn)
{
	emit markAsFinal(recSelectedEntityItemsInLeftColumn);
}

void CalendarWidget::on_markAsPreliminary(DbRecordSet recSelectedEntityItemsInLeftColumn)
{
	emit markAsPreliminary(recSelectedEntityItemsInLeftColumn);
}

void CalendarWidget::on_dropEntityOnTable(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nDroppedEntityID, 
														int nDroppedEntityType, int nDroppedEntityPersonID, QDateTime datSelectionFrom, QDateTime datSelectionTo)
{
	emit dropEntityOnTable(nDraggedEntityID, nDraggedEntityType, nDraggedEntityPersonID, nDroppedEntityID, nDroppedEntityType, nDroppedEntityPersonID, datSelectionFrom, datSelectionTo);
}

void CalendarWidget::GetCorrectedDateTimes(QDateTime &correctedStartDateTime, QDateTime &correctedEndDateTime)
{
	if (correctedStartDateTime.date() < m_startDate)
		correctedStartDateTime = QDateTime(m_startDate, QTime(0,0));

	if (correctedEndDateTime.date() > m_startDate.addDays(m_nDayInterval-1))
		correctedEndDateTime = QDateTime(m_startDate.addDays(m_nDayInterval-1), QTime(23,59,59));
}

bool CalendarWidget::IsEntityInItem(int nEntityID, int nEntityType, DbRecordSet recItemAssignedResources)
{
	//_DUMP(recItemAssignedResources);
	QString strSearchColumn;
	if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
		strSearchColumn = "BPER_CONTACT_ID";
	else if(nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT)
		strSearchColumn = "BCNT_ID";
	else if(nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
		strSearchColumn = "BUSP_ID";
	else if(nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
		strSearchColumn = "BRES_ID";

	recItemAssignedResources.clearSelection();
	if(recItemAssignedResources.find(strSearchColumn, nEntityID, false)>0)
		return true;
	
	return false;
}	
