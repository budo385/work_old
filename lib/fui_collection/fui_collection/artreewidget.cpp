#include "artreewidget.h"

#include <QHeaderView>
#include <QContextMenuEvent>

ARTreeWidget::ARTreeWidget(QWidget *parent)
    : QTreeWidget(parent)
{
	setColumnCount(1);
	header()->hide();
	setSelectionBehavior(QAbstractItemView::SelectItems);
	//setAcceptDrops(true);
}

ARTreeWidget::~ARTreeWidget()
{

}

QList<int> ARTreeWidget::GetSelectedItems()
{
	QList<QTreeWidgetItem*> SelectedItemsList = selectedItems();
	QList<int> SelectedARSIDList;
	QListIterator<QTreeWidgetItem*> iter(SelectedItemsList);
	while (iter.hasNext())
	{
		QTreeWidgetItem *Item = iter.next();
		SelectedARSIDList << Item->data(0, Qt::UserRole).toInt();
	}
	return SelectedARSIDList;
}

void ARTreeWidget::SetParentContextMenuActions(QList<QAction *> ParentActions)
{
	m_ParentActions = ParentActions;
}

void ARTreeWidget::SetChildContextMenuActions(QList<QAction *> ChildActions)
{
	m_ChildActions = ChildActions;
}

void ARTreeWidget::SetWidgetContextMenuActions(QList<QAction *> WidgetActions)
{
	m_WidgetActions = WidgetActions;
}

bool ARTreeWidget::dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action)
{
	//It must be dropped on a role.
	if (!parent)
	{
		emit RoleDropped();
		return false;
	}
	if (parent->parent())
		return false;

	emit ARSDropped(parent, index);
	return true;
}

void ARTreeWidget::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu cntxtMenu(this);

	QPoint position = event->pos();
	QTreeWidgetItem *clickedItem = itemAt(position);

	//If clicked on widget.
	if (!clickedItem)
		cntxtMenu.addActions(m_WidgetActions);
	//If clicked on parent.
	else if (!clickedItem->parent())
		cntxtMenu.addActions(m_ParentActions);
	//If clicked on child.
	else
		cntxtMenu.addActions(m_ChildActions);
	
	cntxtMenu.exec(event->globalPos());
}

void ARTreeWidget::focusInEvent(QFocusEvent *event)
{
	emit FocusIn();
}

void ARTreeWidget::focusOutEvent(QFocusEvent *event)
{
	emit FocusOut();
}
