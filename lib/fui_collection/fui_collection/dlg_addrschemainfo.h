#ifndef DLG_ADDRSCHEMAINFO_H
#define DLG_ADDRSCHEMAINFO_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_addrschemainfo.h"


class Dlg_AddrSchemaInfo : public QDialog
{
    Q_OBJECT

public:
    Dlg_AddrSchemaInfo(QWidget *parent = 0);
    ~Dlg_AddrSchemaInfo();

private:
    Ui::Dlg_AddrSchemaInfoClass ui;

	private slots:
	void on_btnOK_clicked();
};

#endif // DLG_ADDRSCHEMAINFO_H
