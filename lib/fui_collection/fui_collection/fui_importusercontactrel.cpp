#include "fui_importusercontactrel.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "common/common/cliententitycache.h"
#include "gui_core/gui_core/gui_helper.h"
#include "common/common/csvimportfile.h"
#include "common/common/datahelper.h"
#include <QFileDialog>
#include <QProgressDialog>

#ifndef min
#define min(a,b) (((a)<(b))? (a):(b))
#endif

extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
extern ClientEntityCache g_ClientCache;				//global client cache
extern QString g_strLastDir_FileOpen;


FUI_ImportUserContactRel::FUI_ImportUserContactRel(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	ui.cboImportSource->addItem(tr("Text File"));

	//define data record
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_USER_CONTACT_RELATIONSHIP_IMPORT));

	ui.tableContacts->Initialize(&m_lstData);

	//define column headers
	DbRecordSet columns;
	ui.tableContacts->AddColumnToSetup(columns,"BCNT_FIRSTNAME",tr("Contact First Name"),150,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BCNT_LASTNAME",tr("Contact Last Name"),150,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BCNT_ORGANIZATIONNAME",tr("Contact Organization"),150,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BPER_FIRST_NAME",tr("User First Name"),150,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BPER_LAST_NAME",tr("User Last Name"),150,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BPER_CODE",tr("User Code"),150,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BPER_INITIALS",tr("User Initials"),150,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BUCR_ROLE",tr("Role"),150,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BUCR_DESCRIPTION",tr("Description"),150,false, "");
	ui.tableContacts->SetColumnSetup(columns);

	GUI_Helper::SetStyledButtonColorBkg(ui.btnBuildList, GUI_Helper::BTN_COLOR_YELLOW);
	GUI_Helper::SetStyledButtonColorBkg(ui.btnImportContacts, GUI_Helper::BTN_COLOR_YELLOW);
}

FUI_ImportUserContactRel::~FUI_ImportUserContactRel()
{
}

void FUI_ImportUserContactRel::on_btnBuildList_clicked()
{
	//
	// build UserContactRel list from text file
	//
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                            g_strLastDir_FileOpen,
                                            tr("Import File (*.txt)"));
	if (!fileName.isEmpty())
	{
		QFileInfo fileInfo(fileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		//QString strFormat = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: User-Contact-Relations - Subformat: default";
		DbRecordSet data;
		Status status;
		CsvImportFile import;
	
		QStringList lstFormatPlain;
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: User-Contact-Relations - Subformat: default";
		QStringList lstFormatUtf8;
				
		import.Load(status, fileName, data, lstFormatPlain, lstFormatUtf8, true);
		if(!status.IsOK()){
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}
		int nFieldsCnt = data.getColumnCount();
		if(9 != nFieldsCnt){
			QMessageBox::information(this, "Error", tr("Document does not contain %1 fields for each record!").arg(9));
			return;
		}
		//data.Dump();

		//merge results with destination list
		int nCnt = data.getRowCount();
		for(int i=0; i<nCnt; i++)
		{
			m_lstData.addRow();
			int nRow = m_lstData.getRowCount() - 1;
			
			int nCol = 0;
			m_lstData.setData(nRow, "BCNT_FIRSTNAME",		    data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BCNT_LASTNAME",			data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BCNT_ORGANIZATIONNAME",    data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_FIRST_NAME",          data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_LAST_NAME",           data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_CODE",				data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BPER_INITIALS",            data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUCR_ROLE",				data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUCR_DESCRIPTION",			data.getDataRef(i, nCol++).toString());
		}

	#ifdef _DEBUG
		//m_lstData.Dump();
	#endif
		m_lstData.selectAll();
		ui.tableContacts->RefreshDisplay();
		UpdateImportContactsButton();
	}
}

void FUI_ImportUserContactRel::on_btnImportContacts_clicked()
{
	//extract selected rows (under new view format)
	DbRecordSet lstSelected;
	lstSelected.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_USER_CONTACT_RELATIONSHIP_IMPORT));
	lstSelected.merge(m_lstData, true);

	int nRowsToImport = lstSelected.getRowCount();
	if(nRowsToImport < 1){
		QMessageBox::information(this, "", tr("No items have been selected for import!"));
		return;
	}

	//
	// import process
	//
	int nSize = lstSelected.getRowCount();

	QSize size(300,100);
	QProgressDialog progress(tr("Importing Records..."),tr("Cancel"),0,nSize+30,this);
	progress.setWindowTitle(tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	nSize = lstSelected.getRowCount();	//fix size (some rows were merged)
	progress.setMinimumSize(size);

	//update progress
	progress.setValue(10);
	qApp->processEvents();
	if (progress.wasCanceled())
		return;

	Status status;
	DbRecordSet lstWork;
	lstWork.copyDefinition(lstSelected);

	const int nChunkSize = 50;

	int nFailuresCnt = 0;
	int	nStart = -1;
	int nRowProgress = 0;

	int nCount = 0;
	while (1)
	{
		//prepare chunk
		lstWork.destroy();
		lstWork.copyDefinition(lstSelected);

		int nMax = min(nCount+nChunkSize, nSize);
		for(int i=nCount; i<nMax; i++){
			if(!lstWork.addRow()) break;
			DbRecordSet row = lstSelected.getRow(i);
			lstWork.assignRow(lstWork.getRowCount()-1, row);
		}
		if(lstWork.getRowCount() < 1)
			break;

		_SERVER_CALL(BusImport->ImportUserContactRelationships(status, lstWork))
		if(!status.IsOK()){
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}

		int nWorkSize = lstWork.getRowCount();
		//for(int i=0; i<nWorkSize; i++)
			//if(lstWork.getDataRef(i, "STATUS_CODE").toInt() > 0)
		//		nFailuresCnt++;

		nCount = nMax;

		//update progress
		progress.setValue(nCount);
		qApp->processEvents();
		if (progress.wasCanceled())
			return;
	}

	ui.tableContacts->RefreshDisplay();

	progress.setValue(nCount+15);
	qApp->processEvents();
}

void FUI_ImportUserContactRel::UpdateImportContactsButton()
{
	int nCnt = ui.tableContacts->GetDataSource()->getRowCount(); //
	ui.btnImportContacts->setEnabled((nCnt > 0));
}
