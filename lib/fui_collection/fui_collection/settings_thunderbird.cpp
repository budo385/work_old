#include "settings_thunderbird.h"
#include "os_specific/os_specific/outlookfolderpickerdlg.h"

#include "bus_client/bus_client/changemanager.h"
extern ChangeManager					g_ChangeManager;			//global message dispatcher
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

void FolderList2String(const QList<QStringList> &lstData, QString &strResult);
void String2FolderList(QString strData, QList<QStringList> &lstResult);

Settings_Thunderbird::Settings_Thunderbird(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
   	:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

	//find/prepare Ini row (per-connection settings)
	m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	if(-1 == m_nIniLstRow){
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.addRow();
		m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getRowCount()-1;
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_ConnectionName", g_pClientManager->GetConnectionName());
	}

	if(g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool() ) //GetSettingValue(THUNDERBIRD_SETTINGS_SUBSCRIBED).toBool()
	{
		ui.chkImport->setChecked(true);

		if( g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_OnlyKnownEmails").toBool() ) //GetSettingValue(THUNDERBIRD_SETTINGS_IMPORT_ONLY_KNOWN).toBool()
			ui.chkOnlyKnown->setChecked(true);
		else
			ui.chkOnlyKnown->setChecked(false);

		if( g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_OnlyOutgoingEmails").toBool()) //GetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN).toBool()
			ui.chkOutgoingOnly->setChecked(true);
		else
			ui.chkOutgoingOnly->setChecked(false);
	}
	else{
		ui.chkImport->setChecked(false);
		ui.chkOnlyKnown->setChecked(false);
		ui.chkOutgoingOnly->setChecked(false);
	}

	ui.txtScanPeriod->setText(QString("%1").arg(g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt())); //GetSettingValue(THUNDERBIRD_SETTINGS_TIMER_MIN).toString()

	QDateTime dtLastScan = QDateTime::fromString( g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_LastScan").toString(), "dd.MM.yyyy hh:mm:ss"); //GetSettingValue(THUNDERBIRD_SETTINGS_LAST_SCANNED).toString()
	ui.dateLastScanned->setDateTime(dtLastScan);

	GetFolderSetting(m_lstThunderbirdFoldersScan);
	RefreshInfoLabel();

	m_bScanDateChanged = false;
	connect(ui.dateLastScanned, SIGNAL(dateTimeChanged(const QDateTime&)), this, SLOT(on_scannedDateTime_changed(const QDateTime &)));
}

Settings_Thunderbird::~Settings_Thunderbird()
{
}

DbRecordSet Settings_Thunderbird::GetChangedValuesRecordSet()
{
	//save always to prevent bugs:
	if (ui.chkImport->isChecked() != g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_Enabled").toBool()) //GetSettingValue(THUNDERBIRD_SETTINGS_SUBSCRIBED).toBool()
	{
		//m_recSettings.Dump();
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_Enabled", ui.chkImport->isChecked()? true : false); //SetSettingValue(THUNDERBIRD_SETTINGS_SUBSCRIBED, ui.chkImport->isChecked()? true : false);
	}

	if (ui.chkOnlyKnown->isChecked() != g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_OnlyKnownEmails").toBool()) //GetSettingValue(THUNDERBIRD_SETTINGS_IMPORT_ONLY_KNOWN).toBool()
	{
		//m_recSettings.Dump();
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_OnlyKnownEmails", ui.chkOnlyKnown->isChecked()? true : false); //SetSettingValue(THUNDERBIRD_SETTINGS_IMPORT_ONLY_KNOWN, ui.chkOnlyKnown->isChecked()? true : false);
	}

	if (ui.chkOutgoingOnly->isChecked() != g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_OnlyOutgoingEmails").toBool()) //GetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN).toBool()
	{
		//m_recSettings.Dump();
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_OnlyOutgoingEmails", ui.chkOutgoingOnly->isChecked()? true : false);//SetSettingValue(OUTLOOK_SETTINGS_IMPORT_ONLY_KNOWN, ui.chkOnlyKnown->isChecked()? true : false);
	}

	QList<QStringList> lstFolders;
	GetFolderSetting(lstFolders);
	if(m_lstThunderbirdFoldersScan != lstFolders){
		//QByteArray arData;
		//OutlookFolderPickerDlg::FolderList2Binary(m_lstThunderbirdFoldersScan, arData);
		QString strData;
		FolderList2String(m_lstThunderbirdFoldersScan, strData);
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_ScanFolders", strData);//SetSettingValue(THUNDERBIRD_SETTINGS_SCAN_FOLDERS, strData); //arData

		//reset the offsets setting if the list of folders changed
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_LastScanFileOffsets", "");
	}

	if(m_bScanDateChanged)
	{
		QDateTime dtLastThunderbirdScan = ui.dateLastScanned->dateTime();	// notify timer directly
		QString strNewDate = dtLastThunderbirdScan.toString("dd.MM.yyyy hh:mm:ss");
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_LastScan", strNewDate);//SetSettingValue(THUNDERBIRD_SETTINGS_LAST_SCANNED, strNewDate);

		//reset the offsets setting if the scan date changed
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_LastScanFileOffsets", "");
	}
	
	if (ui.txtScanPeriod->text().toInt() != g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt()) //GetSettingValue(THUNDERBIRD_SETTINGS_TIMER_MIN).toInt()
	{
		g_pClientManager->GetIniFile()->m_lstThunderbirdSync.setData(m_nIniLstRow, "Sync_TimerMin", ui.txtScanPeriod->text().toInt());//SetSettingValue(THUNDERBIRD_SETTINGS_TIMER_MIN, ui.txtScanPeriod->text().toInt());

		//re-start timer with new period
		//if(g_pSettings->GetPersonSetting(Thunderbird_SETTINGS_SUBSCRIBED).toBool())
		{
			int nPeriod = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt(); //GetSettingValue(THUNDERBIRD_SETTINGS_TIMER_MIN).toInt();
			if(nPeriod > 0)
			{
				nPeriod *= 60 * 1000;	// min to ms
				g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_SUBSCRIPTION_TIMER_THUNDERBIRD_CHANGED,nPeriod);
			}
		}
	}
	//if not checked stop timer:
	if (!ui.chkImport->isChecked()) 
	{
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_SUBSCRIPTION_TIMER_THUNDERBIRD_CHANGED,0);
	}

	g_pClientManager->GetIniFile()->Save(); //#2154

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

void Settings_Thunderbird::on_btnSelectFolders_clicked()
{
	OutlookFolderPickerDlg dlg(true, false, false);
	dlg.SetSelection(m_lstThunderbirdFoldersScan);
	if(dlg.exec()){
		m_lstThunderbirdFoldersScan = dlg.GetSelection();
		RefreshInfoLabel();
	}
}

void Settings_Thunderbird::GetFolderSetting(QList<QStringList> &lstFolders)
{
	//load Thunderbird folder selection setting
	//QByteArray  arData  = GetSettingValue(THUNDERBIRD_SETTINGS_SCAN_FOLDERS).toByteArray();
	//OutlookFolderPickerDlg::Binary2FolderList(arData, lstFolders);

	QString strData = g_pClientManager->GetIniFile()->m_lstThunderbirdSync.getDataRef(m_nIniLstRow, "Sync_ScanFolders").toString();
	String2FolderList(strData, lstFolders);
}

void Settings_Thunderbird::RefreshInfoLabel()
{
	//refresh info
	ui.labelFolders->setText(tr("%1 folders selected").arg(m_lstThunderbirdFoldersScan.count()));
}

void Settings_Thunderbird::on_scannedDateTime_changed(const QDateTime &dateTime)
{
	m_bScanDateChanged = true;
}

void Settings_Thunderbird::on_chkImport_clicked(bool bChecked)
{
	//MB request: first time usage - set current date instead of standard "1.1.2000 00:00:00"
	if(bChecked){
		if(ui.dateLastScanned->dateTime() == QDateTime(QDate(2000,1,1), QTime(0,0,0))){
			ui.dateLastScanned->setDateTime(QDateTime::currentDateTime());
			m_bScanDateChanged = true;
		}
	}
}
