#ifndef MAINCOMMANDBAR_H
#define MAINCOMMANDBAR_H

#include <QWidget>
#include <QtWidgets/QFrame>

#include "fuibase.h"
#include "./generatedfiles/ui_maincommandbar.h"



/*!
	\class  MainCommandBar
	\ingroup GUICore_FUI
	\brief  Main command bar

	Use:
		- used by FuiBase
		- connect signals to own method
		- for each button click, must check access rights, do operation, call back SetMode() if success

*/
class MainCommandBar : public QFrame 
{
	//friend class FuiBase;	//class must have access to the Buttons

    Q_OBJECT

public:
    MainCommandBar(QWidget *parent = 0);
    //~MainCommandBar();

	//Modes:
	enum Mode 
	{
		MODE_READ=0,	
		MODE_EDIT=1,			
		MODE_INSERT=2,			
		MODE_EMPTY=3

	};

	enum Button
	{
		BUTTON_EDIT,			
		BUTTON_INSERT,	
		BUTTON_DELETE,	
		BUTTON_COPY,		
		BUTTON_OK,
		BUTTON_CANCEL
	};


	QHBoxLayout* GetButtonLayout(){return ui.hboxLayout;};

	void RegisterFUI(FuiBase* pFuiBase){m_pFuiBase=pFuiBase;}
	void SetMode(int nMode=MODE_READ);

	void SetButtonState(int nButton, bool bEnabled);
	void SetButtonVisible(int nButton, bool bVisible);

	QPushButton* GetButton(int nButton);

public:
    Ui::MainCommandBarClass ui;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnCopy_clicked();
	void on_btnDelete_clicked();
	void on_btnInsert_clicked();
	void on_btnEdit_clicked();


private:
	FuiBase* m_pFuiBase;
	//nMode
	int m_nMode;
};

#endif // MAINCOMMANDBAR_H



