#ifndef CALENDARGRAPHICSVIEW_H
#define CALENDARGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QtWidgets/QTableWidget>

#include "calendarwidget.h"
#include "calendargraphicsscene.h"
//#include "calendartableview.h"
#include "calendarheaderitem.h"

class CalendarDateHeaderItem : public CalendarHeaderItem
{
	Q_OBJECT
public:
	CalendarDateHeaderItem(bool bIsInMultiDayCalendar, int nColumn, QString	strString, int nHeight, QGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent = 0, Qt::WindowFlags wFlags = 0);
	void ConnectSignals();
public slots:
	void on_UpdateCalendar();
};

class CalendarTableModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	CalendarTableModel(QObject *parent = 0);
	~CalendarTableModel();

	void Initialize(QDate startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites);

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	QVariant headerPersonData(int section) const;
	int headerEntityType(int section) const;

	//Access methods.
	void setDateInterval(QDate &startDate, int nDayInterval);
	QDate getStartDate(){return m_startDate;};
	int	 getDateInterval();
	void setTimeScale(CalendarWidget::timeScale scale);
	CalendarWidget::timeScale getTimeScale(){return m_timeScale;};
	DbRecordSet* getEntityRecordSet(){return m_recEntites;};
	QDateTime getItemStartDateTime(QModelIndex &index);
	QDateTime getItemEndDateTime(QModelIndex &index);

	//Item access methods.
	void getEntityForCell(int nRow, int nColumn, int &nEntityID, int &nEntityType, int &nEntityPersonID);
	void getItem(int nEntityID, int nEntityType, QDateTime dateTime, QModelIndex &Index, qreal &remain);
	QModelIndex getItem(QDateTime &dateTime);
	void AddMultiDayIndex(QModelIndex index){m_lstSelectedIndexes << index;};
	void RemoveMultiDayIndex(QModelIndex index){m_lstSelectedIndexes.removeAll(index);};
	bool CheckMultiDayIndex(QModelIndex index){return m_lstSelectedIndexes.contains(index);};

	int getColumnForDate(int nEntityID, int nEntityType, QDate &date);

private:
	void getRowForTime(QTime &time, int &nRow, qreal &Remain);
	QDate getDateForColumn(int &nColumn);
	QTime getStartTimeForRow(int nRow);
	QTime getEndTimeForRow(int nRow);

	CalendarWidget::timeScale	m_timeScale;
	QDate		m_startDate;
	int			m_nDayInterval;
	DbRecordSet *m_recEntites;		//Entities recordset.
	QList<QModelIndex> m_lstSelectedIndexes;
};

class CalendarGraphicsView : public QGraphicsView
{
	Q_OBJECT

public:
	CalendarGraphicsView(CalendarWidget *pCalendarWidget, QWidget *parent = 0);
	~CalendarGraphicsView();

	void Initialize(QDate startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites, bool bResizeColumnsToFit = false, QTime scrollToTime = QTime(6,0));
	CalendarTableModel *Model(){return m_pCalendarTableModel;};

	//For outside.
	int  getEntityCount(){return m_recEntites->getRowCount();}
	void getSelectedItemDateTime(QDateTime &startDateTime, QDateTime &endDateTime);
	int  GetColumnWidth(){return m_nColumnWidth;};
	int  GetMultiDayColumnWidth();
	QDateTime GetTopLeftVisibleItem();
	void SetTopLeftVisibleItem(QDateTime dateTime);
	bool ItemExists(int nCentID, int nEntityID, int nEntityType, int nBCOL_ID);
	
	//Used internally.
	CalendarGraphicsScene* getScene();
	int getVertHeaderWidth(){return m_pCalendarWidget->getVertHeaderWidthForLowerCalendar();};
	QGraphicsProxyWidget* getTableView();
	int getTotalHeaderHeight();
	void showLeftDownItem(bool bShow = true);
	void PositionCornerScrollBarItems();
	void CreateVerticalHeader();
	void DestroyVerticalHeader();
	void UpdateCalendarSignal();
	void UpdateCalendarSignalFromOutside();

	void CheckForMultipleItemsInColumn(QGraphicsItem *newItem);

private:
	void CreateHeader();
	void DestroyHeader();
	void CreateCalendarHeader();
	void CreatePersonHeader();
	void DestroyCalendarHeader();
	void DestroyPersonHeader();
	void GetItemsPosAndWidth(QList<QGraphicsItem*> colidedItems);
	void GetColidedItems(QGraphicsItem *Item, QList<QGraphicsItem*> &colidedItems, bool bThisItemOnly);
	void GetColidedItemsForRemove(QGraphicsItem *itemForRemove, QList<QGraphicsItem*> &colidedItems);
	bool ItemsColide(QGraphicsItem *Item1, QGraphicsItem *Item2);
	void CheckForMultipleItemsInColumnForRemove(QGraphicsItem *Item);
	void SortColidedItems(QList<QGraphicsItem*> &colidedItems);
	void CreateCornerScrollBarItems();
	void PositionCustomScrollBars();
	void CalculateColumnWidth();
	bool AllColumnsAreInViewArea(int nViewportWidth);
	void UnselectOneItem(QGraphicsItem* Item);
	void UnselectAllItems();

	CalendarWidget			*m_pCalendarWidget;
	CalendarTableModel		*m_pCalendarTableModel;
	CalendarGraphicsScene	*m_pGraphicsScene;
	QGraphicsProxyWidget	*m_pCalendarTableView;
	QGraphicsProxyWidget	*m_pBackgroundItem;
	QGraphicsProxyWidget	*m_pLeftDownScrollBarItem;
	QGraphicsProxyWidget	*m_pRightDownScrollBarItem;
	QGraphicsProxyWidget	*m_pTopRightScrollBarItem;
	QGraphicsProxyWidget	*m_pTopLeftScrollBarItem;
	QGraphicsProxyWidget	*m_pHScrollBar;
	QGraphicsProxyWidget	*m_pVScrollBar;
	QScrollBar				*m_pHScroll;
	QScrollBar				*m_pVScroll;
	QList<QGraphicsItem*>	m_lstHeaderItems;
	QList<QGraphicsItem*>	m_lstPersonHeaderItems;
	QList<QGraphicsItem*>	m_lstVerticalHeaderItems;
	QMultiHash<int, QGraphicsItem*> m_lstCalendarEntityItems;
	QMultiHash<int, QGraphicsItem*> m_lstCalendarReservationItems;
	QMultiHash<int, QGraphicsItem*> m_lstCalendarTaskItems;
	QList<QGraphicsItem*>	m_lstSelectedItems;
	DbRecordSet				*m_recEntites;		//Entities recordset.
	int						m_nColumnWidth;
	int						m_nRowHeight;
	int						m_nItemLeftOffSet;
	bool					m_bColumnsManualResized;
	bool					m_bResizeColumnsToFit;
	bool					m_bResizeFromShowEvent;
	bool					m_bFirstTimeShow;
	QDateTime				m_ScrollToDateTime;

public slots:
	void AddCalendarItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, QDateTime &startDateTime, QDateTime &endDateTime, 
							QString strTitle, QString strLocation, QString strColor, 
							QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
							QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, int nBcep_Status, int nBCEV_IS_INFORMATION, 
							bool bIsProposed, QGraphicsItem* &Item);
	void UpdateCalendarItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime &startDateTime, QDateTime &endDateTime, 
							QString strTitle, QString strLocation, QString strColor, 
							QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
							QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, int nBcep_Status, int nBCEV_IS_INFORMATION, 
							bool bIsProposed, QGraphicsItem* &Item);
	void RemoveCalendarItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID, QList<QGraphicsItem*> &Items);
	void AddCalendarReservation(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, QDateTime startDateTime, 
								QDateTime endDateTime);
	//void RemoveCalendarItem(int nItemID);
	void AddCalendarTaskItems(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, QPixmap Pixmap, int nEntityID, int nEntityType, int nTypeID);
	void RemoveAllCalendarItems();
	void on_VscrollBar_valueChanged(int value);
	void on_HscrollBar_valueChanged(int value);
	void on_CustomVscrollBar_valueChanged(int value);
	void on_CustomHscrollBar_valueChanged(int value);
	void on_VscrollBar_rangeChanged(int min, int max);
	void on_HscrollBar_rangeChanged(int min, int max);
	void on_horizontalScrollBar_sliderMoved(int value);
	void on_verticalScrollBar_sliderMoved(int value);

	void on_columnResized(int nColumn, int nWidth, bool bResizeAll);
	void on_horizontalScrollBarVisible(bool bVisible);
	void on_verticalScrollBarVisible(bool bVisible);
	void on_ItemResized(QGraphicsProxyWidget *thisItem);
	void on_ItemResizing(bool bIsResizing);
	void on_ItemModify(QGraphicsProxyWidget *thisItem);
	void on_ItemView(QGraphicsProxyWidget *thisItem);
	
	void on_DropEntityOnItem(QGraphicsProxyWidget *thisItem, int nEntityID, int nEntityType, int nEntityPersonID);
	void on_ItemDelete(QGraphicsProxyWidget *thisItem);
	void on_ItemMoved(QGraphicsProxyWidget *thisItem);
	void on_tableCellDoubleClicked(int row, int column);
	void on_SelectionChanged(QModelIndex topLeftSelectedIndex, QModelIndex bottomRightSelectedIndex);
	void AddEntityToSelectionRecordset(int nEntityID, int nEntityType, int nEntityPersonID, QHash<int, int> &hshAddedEntityID, QHash<int, int> &hshAddedEntityType, DbRecordSet &recSelectedEntites);
	void ResizeColumnsToFit(bool bForce = false);
	void on_ItemSelected(QGraphicsProxyWidget *thisItem);

protected:
	void resizeEvent(QResizeEvent *event);
	void showEvent(QShowEvent *event);

signals:
	void UpdateCalendar();
	void UpdateMultiDayCalendar();
	void columnResized(int nColumn, int nWidth, bool bResizeAll);
	void scrollBar_valueChanged(int value);
	void ItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void ItemMoved(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo);
	void ItemModify(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void ItemView(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID);
	void DropEntityOnItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID);
	void ItemDelete(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType);
	void ItemsDelete(DbRecordSet recSelectedItemsData);
	void tableCellDoubleClicked(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType);
	void selectionChanged(QDateTime datSelectionFrom, QDateTime datSelectionTo, DbRecordSet recSelectedEntities);
	void openMultiDayEvent(QModelIndex index);
	void TaskItemClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID);
	void TaskItemDoubleClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID);
};

#endif // CALENDARGRAPHICSVIEW_H
