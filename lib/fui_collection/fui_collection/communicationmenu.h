#ifndef COMMUNICATIONMENU_H
#define COMMUNICATIONMENU_H


#include "communicationmenu_base.h"
#include "common/common/observer_ptrn.h"
/*!
	\class  CommunicationMenu
	\ingroup FUI_Collection
	\brief  CE menu

	Use:
	- init, set sizes for vertical splitter
	- connect NeedNewData(), fill with defaults with SetDefaults()
*/

class CommunicationMenu : public CommunicationMenu_Base, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
    CommunicationMenu(QWidget* parent=0);
    ~CommunicationMenu();

	void SetFUIParent(int nFUI);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	bool ProcessKeyPressEventFromMainWindow( QKeyEvent * event ); 
private:
	void OnThemeChanged();


};

#endif // COMMUNICATIONMENU_H


