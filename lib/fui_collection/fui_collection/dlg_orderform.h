#ifndef DLG_ORDERFORM_H
#define DLG_ORDERFORM_H

#include <QDialog>
#include "generatedfiles/ui_dlg_orderform.h"

class Dlg_OrderForm : public QDialog
{
	Q_OBJECT

public:
	Dlg_OrderForm(QWidget *parent = 0);
	~Dlg_OrderForm();

private slots:
	void on_btnTrial_clicked();
	void on_btnFull_clicked();
	void on_btnCancel_clicked();
	void on_btnContinue_clicked();

private:
	Ui::Dlg_OrderFormClass ui;
};

#endif // DLG_ORDERFORM_H
