#include "roledefintiontreewidget.h"

QTreeWidgetItem* RoleDefintionTreeWidget::SetItemData(DbRecordSet &DataRecordSet, QTreeWidgetItem *Item /*= NULL*/)
{
	if (!Item)
		Item = new QTreeWidgetItem;

	QString Name		= DataRecordSet.getDataRef(0, 3).toString();
	QString Description = DataRecordSet.getDataRef(0, 4).toString();
	int RowId			= DataRecordSet.getDataRef(0, 0).toInt();
	int ParentId		= DataRecordSet.getDataRef(0, 1).toInt();
	int RoleType		= DataRecordSet.getDataRef(0, 2).toInt();
	QString icon		= DataRecordSet.getDataRef(0, 5).toInt();

	//Set display data.
	Item->setData(0, Qt::UserRole,		RowId);			//RowID.
	Item->setData(1, Qt::UserRole,		ParentId);		//Parent ID.
	Item->setData(2, Qt::UserRole,		RoleType);		//Role type (business or system).
	Item->setData(0, Qt::DisplayRole,	Name);			//Code.
	Item->setData(1, Qt::DisplayRole,	Description	);	//Description.
	Item->setIcon(0, CreateIcon(icon));					//Set icon.

	return Item;
}
