#include "taskwidget.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "db_core/db_core/dbsqltableview.h"


#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester



TaskWidget::TaskWidget(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);
	m_pRecMain = NULL;

	m_recTask.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	m_recTask.addRow();

	this->setMaximumHeight(25); //B.T. to avoid flip-flop in layout when opening widget

	ui.frameTaskType->Initialize(ENTITY_BUS_CM_TYPES);
	ui.frameTaskType->GetSelectionController()->GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_TASK);
	ui.frameTaskType->GetSelectionController()->ReloadData(); //activate cache

	m_nTaskSystemType=GlobalConstants::TASK_TYPE_NO_TYPE;

	//enable datetime edit
	ui.dateStart->useAsDateTimeEdit();
	ui.dateDue->useAsDateTimeEdit();
	ui.dateCompleted->useAsDateTimeEdit();

	ui.frameOwner		->Initialize(ENTITY_BUS_PERSON,&m_recTask,  "BTKS_OWNER");
	ui.frameOriginator	->Initialize(ENTITY_BUS_PERSON,&m_recTask, "BTKS_ORIGINATOR");

	ui.dateStart		->setCalendarPopup(true);
	ui.dateDue			->setCalendarPopup(true);
	ui.dateCompleted	->setCalendarPopup(true);

	m_bEditMode=false;
	SetInfoVisible(false);

	m_bAlwaysExpanded = false;

	ui.frameTaskType->registerObserver(this);
	connect(ui.radCompleted, SIGNAL(clicked(bool)), this, SLOT(OnTaskCompleted(bool)));

	ui.frameOwner->registerObserver(this);
	ui.frameOriginator->registerObserver(this);

	ui.frmDescription ->SetEmbeddedPixMode();

	ui.cboPriority->addItem("0");
	ui.cboPriority->addItem("1");
	ui.cboPriority->addItem("2");
	ui.cboPriority->addItem("3");
	ui.cboPriority->addItem("4");
	ui.cboPriority->addItem("5");
	ui.cboPriority->addItem("6");
	ui.cboPriority->addItem("7");
	ui.cboPriority->addItem("8");
	ui.cboPriority->addItem("9");

	//FUI:
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_TASKS, nValue))
	{
		this->SetEnabled(false);
	}
}

TaskWidget::~TaskWidget()
{
}

void TaskWidget::GetDataFromGUI()
{
	Q_ASSERT(m_recTask.getRowCount()==1); //security check

	m_recTask.setData(0, "BTKS_SUBJECT", ui.txtSubject->text());
	m_recTask.setData(0, "BTKS_START",	 ui.dateStart->dateTime());
	m_recTask.setData(0, "BTKS_DUE",	 ui.dateDue->dateTime());
	m_recTask.setData(0, "BTKS_COMPLETED",   ui.dateCompleted->dateTime());
	m_recTask.setData(0, "BTKS_DESCRIPTION", ui.frmDescription->GetHtml());
	m_recTask.setData(0, "BTKS_PRIORITY", ui.cboPriority->currentIndex());

	//TASK TYPE:
	QVariant intNull(QVariant::Int);
	DbRecordSet rowTaskType;
	int nTaskTypeID;
	if(ui.frameTaskType->GetCurrentEntityRecord(nTaskTypeID,rowTaskType))
		m_recTask.setData(0, "BTKS_TASK_TYPE_ID", nTaskTypeID);
	else
		m_recTask.setData(0, "BTKS_TASK_TYPE_ID", intNull);

	int nStatus = 0;	//no status
	if(ui.radScheduled->isChecked())
		nStatus = 1;
	else if(ui.radInWork->isChecked())
		nStatus = 2;
	else if(ui.radCompleted->isChecked())
		nStatus = 3;
	else if(ui.radCancelled->isChecked())
		nStatus = 4;
	m_recTask.setData(0, "BTKS_SYSTEM_STATUS", nStatus);

	m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE", ui.chkScheduledTask->isChecked()? 1 : 0);

	m_recTask.setData(0, "BTKS_NOTIFY_ORIGINATOR", ui.chkNotifyOriginator->isChecked()? 1 : 0);

	//MB request: change owner when task closed
	if(nStatus >= 3)
	{
		//propagate task owner to item owner
		if(m_pRecMain){
			int nOwnerID = m_recTask.getDataRef(0, "BTKS_OWNER").toInt();
			m_pRecMain->setData(0, "CENT_OWNER_ID", nOwnerID);
		}
	}
}

void TaskWidget::SetGUIFromData()
{
	Q_ASSERT(m_recTask.getRowCount()==1); //security check

	//TASK TYPE:
	ui.frameTaskType->SetCurrentEntityRecord(m_recTask.getDataRef(0, "BTKS_TASK_TYPE_ID").toInt());

	bool bShowTaskPage=false;

	//NOTE: modus operandi:
	//a) if task ID valid, chk box can not changed
	if(m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0)
	{
		bShowTaskPage=true;
		ui.chkScheduledTask->setChecked(1);	
	}
	else
		ui.chkScheduledTask->setChecked(0);	

	//if Task exist, chkbox can not be switched off/on: task page is visible
	if(m_recTask.getDataRef(0, "BTKS_ID").toInt() > 0)
	{
		bShowTaskPage=true;
		//ui.chkScheduledTask->setCheckable(false); //if edit mode, disable chkbox, show task
	}
	else
	{
		//if insert mode: set default values:
		m_recTask.setData(0, "BTKS_SYSTEM_TYPE", m_nTaskSystemType);
		m_recTask.setData(0, "BTKS_SYSTEM_STATUS", 1);		//set "scheduled"
		m_recTask.setData(0, "BTKS_START", QDateTime::currentDateTime());	 //his active
		m_recTask.setData(0, "BTKS_OWNER", g_pClientManager->GetPersonID());		
		m_recTask.setData(0, "BTKS_ORIGINATOR", g_pClientManager->GetPersonID());	
	}

	int nStatus = m_recTask.getDataRef(0, "BTKS_SYSTEM_STATUS").toInt();
	if(1 == nStatus)
		ui.radScheduled->setChecked(1);
	else if(2 == nStatus)
		ui.radInWork->setChecked(1);
	else if(3 == nStatus)
		ui.radCompleted->setChecked(1);
	else if(4 == nStatus)
		ui.radCancelled->setChecked(1);
	else
		ui.radNoStatus->setChecked(1);

	bool bNotifyOriginator = m_recTask.getDataRef(0, "BTKS_NOTIFY_ORIGINATOR").toBool();
	if(bNotifyOriginator)
		ui.chkNotifyOriginator->setChecked(1);

	//	ui.chkScheduledTask->setEnabled(ui.frameX->isEnabled());	//only enabled if frame enabled

	ui.txtSubject->setText(m_recTask.getDataRef(0, "BTKS_SUBJECT").toString());
	ui.dateStart->setDateTime(m_recTask.getDataRef(0, "BTKS_START").toDateTime());
	ui.dateDue->setDateTime(m_recTask.getDataRef(0, "BTKS_DUE").toDateTime());
	ui.dateCompleted->setDateTime(m_recTask.getDataRef(0, "BTKS_COMPLETED").toDateTime());
	ui.frmDescription->SetHtml(m_recTask.getDataRef(0, "BTKS_DESCRIPTION").toString());
	ui.cboPriority->setCurrentIndex(m_recTask.getDataRef(0, "BTKS_PRIORITY").isNull() ? 5 : m_recTask.getDataRef(0, "BTKS_PRIORITY").toInt());

	//based on data, show or hide page:
	SetInfoVisible(bShowTaskPage);

	ui.frameOwner		->RefreshDisplay();
	ui.frameOriginator	->RefreshDisplay();
}

void TaskWidget::SetEnabled(bool bEnable)
{
	if(!g_FunctionPoint.IsFPAvailable(FP_TASKS))
		return;

	//ui.frameX->setEnabled(bEnable);

	m_bEditMode=bEnable;
	ui.txtSubject->setEnabled(bEnable);
	ui.dateStart->setEnabled(bEnable);
	ui.dateDue->setEnabled(bEnable);
	ui.dateCompleted->setEnabled(bEnable);
	ui.frmDescription->SetEditMode(bEnable);
	ui.radScheduled->setEnabled(bEnable);
	ui.radInWork->setEnabled(bEnable);
	ui.radCompleted->setEnabled(bEnable);
	ui.radCancelled->setEnabled(bEnable);
	ui.radNoStatus->setEnabled(bEnable);
	ui.frameOwner->setEnabled(bEnable);
	ui.frameOriginator->setEnabled(bEnable);
	ui.frameTaskType->setEnabled(bEnable);
	ui.cboPriority->setEnabled(bEnable);

	/*
	if (!bEnable)
	{
		ui.chkScheduledTask->setCheckable(false);	
	}
	else
	{
		if(m_recTask.getDataRef(0, "BTKS_ID").toInt() == 0)
			ui.chkScheduledTask->setCheckable(true);
	}
	*/
}

void TaskWidget::SetInfoVisible(bool bVisible)
{
	//if(!g_FunctionPoint.IsFPAvailable(FP_TASKS))
	//	return;

	if (bVisible)
	{
		if(m_bAlwaysExpanded)
			setMaximumHeight(65280); //parent frame
		else
			setMaximumHeight(280); //parent frame
		ui.chkScheduledTask->setMinimumHeight(280);
		ui.frameX->setVisible(true);	
		m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE",1);
	}
	else
	{
		m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE",0);

		if(m_bAlwaysExpanded){
			//TOFIX disable the data
			return;
		}

		setMaximumHeight(25); //parent frame
		ui.chkScheduledTask->setMinimumHeight(25);
		ui.frameX->setVisible(false);
		
	}
}

void TaskWidget::on_chkScheduledTask_clicked()
{
	//if(!g_FunctionPoint.IsFPAvailable(FP_TASKS))
	//	return;

	//if non edit mode: restore state
	if (!m_bEditMode)
	{
		ui.chkScheduledTask->blockSignals(true);
		ui.chkScheduledTask->setChecked(!ui.chkScheduledTask->isChecked());
		ui.chkScheduledTask->blockSignals(false);
		return;
	}
	
	//
	//based on checkbook:
	SetInfoVisible(ui.chkScheduledTask->isChecked());

	/*
	if (ui.chkScheduledTask->isChecked())
	{
		ui.chkScheduledTask->setMinimumHeight(240);
		ui.frameX->setVisible(true);	
		m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE",1);
	}
	else
	{
		ui.chkScheduledTask->setMinimumHeight(25);
		ui.frameX->setVisible(false);
		m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE",0);
	}
	*/

	emit ScheduleTaskToggle();

	//SetInfoVisible(ui.chkScheduledTask->isChecked());
}

void TaskWidget::ClearTaskData()
{
	ui.frameTaskType->Clear();

	m_recTask.destroy();
	m_recTask.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	m_recTask.addRow();
}

void TaskWidget::Hide()
{
	ui.chkScheduledTask->setChecked(false);
	SetInfoVisible(false);
}

void TaskWidget::SetDefaultTaskType(int nTaskSystemType)
{
	m_nTaskSystemType=nTaskSystemType;
}

bool TaskWidget::isChecked()
{
	return ui.chkScheduledTask->isChecked();
}

void TaskWidget::SetChecked(bool bChecked)
{
	ui.chkScheduledTask->setChecked(bChecked);
	SetInfoVisible(bChecked);
}

void TaskWidget::OnTaskTypeChanged()
{
	if(ui.txtSubject->text().isEmpty()){
		ui.txtSubject->setText(ui.frameTaskType->GetCurrentDisplayName());
	}
}

void TaskWidget::OnTaskCompleted(bool bChecked)
{
	//on completed, set completion date to current
	ui.dateCompleted->setDateTime(QDateTime::currentDateTime());
}

void TaskWidget::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject == ui.frameTaskType && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnTaskTypeChanged();
		return;
	}
	else if((pSubject == ui.frameOwner) || (pSubject == ui.frameOriginator))
	{
		//if the settings are different for owner and originator, check the "notify originator"
		int nOwnerID = 0;
		int nOriginatorID = -1;
		ui.frameOwner->GetCurrentEntityRecord(nOwnerID);
		ui.frameOriginator->GetCurrentEntityRecord(nOriginatorID);
		if(nOwnerID != nOriginatorID)
			ui.chkNotifyOriginator->setChecked(true);
	}
}

void TaskWidget::ServerReload()
{

}

//issue 2352: owner & originator must be set:
void TaskWidget::CheckTaskRecordBeforeSave(Status &err)
{
	err.setError(0);
	if (m_recTask.getRowCount()<1)
		return;

	if (m_recTask.getDataRef(0,"BTKS_OWNER").toInt()==0)
	{
		err.setError(1,tr("Task owner must be set!"));
		return;
	}
	if (m_recTask.getDataRef(0,"BTKS_ORIGINATOR").toInt()==0)
	{
		err.setError(1,tr("Task originator must be set!"));
		return;
	}
	return;
}

void TaskWidget::SetAlwaysExpanded(bool bExpanded)
{
	m_bAlwaysExpanded = bExpanded;
	if(bExpanded)
	{
		setMaximumHeight(65080); //parent frame
		ui.chkScheduledTask->setMinimumHeight(280);
		ui.frameX->setVisible(true);
	}
		//SetInfoVisible(true);
}
