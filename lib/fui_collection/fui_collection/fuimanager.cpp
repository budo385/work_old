#include "fuimanager.h"
#include "gui_core/gui_core/qfuiholderwidget.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QSplashScreen>
#include <QProcess>

//supported FUIs
#include "personwidget.h"
#include "userwidget.h"

//#include "roledefinitionwidget.h"
#include "fui_roledefinition.h"
//#include "boentitytestfui.h"
#include "fui_busperson.h"
#include "fui_busorganizations.h"
#include "fui_buscostcenters.h"
#include "fui_contacts.h"
#include "fui_coreuser.h"
#include "fui_contacttype.h"
#include "fui_busdepartments.h"
#include "reportpreviewer.h"
#include "fui_communicationcenter.h"
#include "fui_voicecallcenter.h"
#include "fui_voicecall.h"
#include "fui_ce_types.h"
#include "fui_ceeventtype.h"
#include "fui_projects.h"
#include "fui_importcontact.h"
#include "fui_importperson.h"
#include "fui_importproject.h"
#include "fui_importemail.h"
#include "fui_importappointment.h"
#include "fui_ImportUserContactRel.h"
#include "fui_ImportContactContactRel.h"
#include "fui_pbxconfig.h"
#include "optionsandsettingswidget.h"
#include "emaildialog.h"
#include "fui_dm_applications.h"
#include "fui_dm_userpaths.h"
#include "fui_dm_documents.h"
#include "commgridfilter.h"
#include "qcw_smallwidget.h"
#include "qcw_largewidget.h"
#include "fui_resources.h"
#include "fui_calendarevent.h"
#include "fui_calendar.h"
#include "fui_invitationcard.h"
#include "fui_calendarvieweditor.h"
#include "fui_debtorpayments.h"
#include "spc_lib/spc_lib/fui_projectmanager.h"
#include "fui_customfields.h"

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;				//global access right tester

 //defines FP codes
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester


int GetMainMenuCount();
void OnFuiClose(QWidget *pWidget, QWidget *pMainWnd);

FuiManager::FuiManager()
{
	m_pMainWnd		= NULL;
	m_pWorkAreaWnd	= NULL;
	m_pActClose		= NULL;
	m_pActMaximize	= NULL;
	m_pWindowMenu	= NULL;
	m_pFuiActive	= NULL;

	m_bMaximized=false;

	m_nCurMainFP = -1;
	m_nCurCommGridFUI = -1;
	m_nGIDCounter = 0;
	m_bSkipComboUpdate = false;
	m_nRecordID = -1;
}

FuiManager::~FuiManager()
{
}

void FuiManager::Initialize(QWidget	*pMainWnd, QStackedWidget *pWorkAreaWnd, QMenu *pWindowMenu)
{
	m_pMainWnd		= pMainWnd;
	m_pWorkAreaWnd	= pWorkAreaWnd;
	m_pWindowMenu	= pWindowMenu;
	
	Q_ASSERT(m_pWindowMenu);			//just to be sure
}

int	FuiManager::OpenFUI(int nFuiType, bool bSeparateWindow, bool bForceNewTab, int nMode, int nRecordID,bool bDontShow, bool bOpenedAsAvatar/*=false*/)
{
	bool bFuiAlreadyExists = false;

	QTabWidget *pTabWnd = NULL;
	QWidget *pFuiWnd = NULL;
	QString strTitle;
	FuiBase* pBase = NULL;
	int nTabCount = -1;
	m_nRecordID = nRecordID;

	FuiInfo info;
	info.nFP = nFuiType;
	info.nGID = m_nGIDCounter ++;

	if(bSeparateWindow)
	{
		//Ctrl + click creates FUI in a separate window
		pFuiWnd = CreateFuiWidget(nFuiType, NULL, bOpenedAsAvatar);
		if(NULL == pFuiWnd)
			return -1;	// error - failed to create new window with the FUI
		pBase = dynamic_cast<FuiBase*>(pFuiWnd);
		Q_ASSERT(pBase!=NULL);
		if (pBase==NULL)		//error - bot FUI Base!
			return -1;
		strTitle=pBase->GetFUIName();

		//Get FUI position and move it.
		pFuiWnd->move(dynamic_cast<FuiBase*>(pFuiWnd)->GetFuiPosition());

		info.pFuiWidget = pFuiWnd;	//separate window
		pFuiWnd->setWindowTitle(strTitle);
			
		//add to the FUI combo box

		QActionEx *pMenuAction = new QActionEx(strTitle, this);
		connect(pMenuAction, SIGNAL(triggered(bool)), pMenuAction, SLOT(OnTriggered(bool)));
		connect(pMenuAction, SIGNAL(triggered_ex(QAction *, bool)), this, SLOT(OnFuiWindowMenuTriggered(QAction *,bool)));
		m_lstFUIMenu.append(pMenuAction);
		m_pWindowMenu->addAction(pMenuAction);

		//detect when FUI in separate window is closed
		//pBase = dynamic_cast<FuiBase*>(pFuiWnd);
		if(pBase){
			pBase->RegisterCloseFn(OnFuiClose, m_pMainWnd);
			pBase->m_nFuiType = nFuiType; //store FUI type
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("FUI opened as a separate window: %1").arg(strTitle));
	}
	else
	{
		//FIX: hide previous active window to prevent drawing artefacts
		if(m_nCurMainFP >= 0 &&	m_nCurMainFP != nFuiType){
			QTabWidget *pTabWnd1 = m_mapTabWnd[m_nCurMainFP];
			pTabWnd1->hide();
		}

		if(m_mapFuiWnd.find(nFuiType) != m_mapFuiWnd.end())
			pFuiWnd = m_mapFuiWnd[nFuiType];

		if(NULL != pFuiWnd){
			bFuiAlreadyExists = true;
			//qDebug("MENU: FUI already exists");
		}

		if(NULL == pFuiWnd)	//FUI not already created
		{
			//qDebug("MENU: Creating new FUI holder widget");

			pFuiWnd = new QFuiHolderWidget(m_pWorkAreaWnd);
			m_pWorkAreaWnd->addWidget(pFuiWnd);

			//update window map
			m_mapFuiWnd[nFuiType] = pFuiWnd;
		}

		info.pFuiPlaceholder = pFuiWnd;
	}

	Q_ASSERT(pFuiWnd != NULL);

	if(!bSeparateWindow){
		pFuiWnd->setVisible(false);
		m_pWorkAreaWnd->setCurrentWidget(pFuiWnd);
	}

	if(!bFuiAlreadyExists && !bSeparateWindow)
	{
		QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
		sizePolicy1.setHorizontalStretch(1);
		sizePolicy1.setVerticalStretch(1);
		sizePolicy1.setHeightForWidth(pFuiWnd->sizePolicy().hasHeightForWidth());
		pFuiWnd->setSizePolicy(sizePolicy1);
		pFuiWnd->setSizeIncrement(QSize(1, 1));
	}

	if(!bSeparateWindow)
	{
		QHBoxLayout *hboxLayout = NULL;

		if(!bFuiAlreadyExists)
		{
			hboxLayout = new QHBoxLayout(pFuiWnd);
			hboxLayout->setSpacing(0);
			hboxLayout->setMargin(0);
			hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
		}

		if(m_mapTabWnd.find(nFuiType) != m_mapTabWnd.end())
			pTabWnd = m_mapTabWnd[nFuiType];

		if(!bFuiAlreadyExists)
		{
			//qDebug("MENU: New tab widget created");
			Q_ASSERT(pFuiWnd->children().size() < 2);	//fui placeholder should be the only child (with layout)

			pTabWnd = new QTabWidget;
			pTabWnd->setVisible(false);
			//pTabWnd->setUpdatesEnabled(false);
			SetupTabIcons(pTabWnd);
			hboxLayout->addWidget(pTabWnd);

			QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
			sizePolicy2.setHorizontalStretch(1);
			sizePolicy2.setVerticalStretch(1);
			sizePolicy2.setHeightForWidth(pFuiWnd->sizePolicy().hasHeightForWidth());
			pTabWnd->setSizePolicy(sizePolicy2);
			pTabWnd->setSizeIncrement(QSize(1, 1));

			//update tab map
			m_mapTabWnd[nFuiType] = pTabWnd;

			//we must keep track when some window becomes active
			m_pMainWnd->connect(pTabWnd, SIGNAL(currentChanged(int)), m_pMainWnd, SLOT(OnFuiTabSelected(int)));
		}
		
		info.pTabWidget = pTabWnd;

		nTabCount = pTabWnd->count();
		if(bForceNewTab || (0 == nTabCount))
		{
			QWidget *pSubFuiWnd = CreateFuiWidget(nFuiType, pTabWnd->parentWidget());
			if(NULL == pFuiWnd)
				return -1;	// error - failed to create new window with the FUI
			pBase =	dynamic_cast<FuiBase*>(pSubFuiWnd);
			Q_ASSERT(pBase!=NULL);
			if (pBase==NULL)		//error - not FUI Base!
				return -1;

			//B.T.: get FUI title from sokrates/MenuItem.cpp or from fuibase itself
			strTitle=pBase->GetFUIName();

			if(NULL != pSubFuiWnd)
			{
				//qDebug("MENU: New FUI created");

				QActionEx *pMenuAction = new QActionEx(strTitle, this);
				connect(pMenuAction, SIGNAL(triggered(bool)), pMenuAction, SLOT(OnTriggered(bool)));
				connect(pMenuAction, SIGNAL(triggered_ex(QAction *, bool)), this, SLOT(OnFuiWindowMenuTriggered(QAction *, bool)));
				m_lstFUIMenu.append(pMenuAction);
				m_pWindowMenu->addAction(pMenuAction);

				//pSubFuiWnd->setVisible(false);
				pTabWnd->addTab(pSubFuiWnd, strTitle);
		
				QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
				sizePolicy2.setHorizontalStretch(1);
				sizePolicy2.setVerticalStretch(1);
				sizePolicy2.setHeightForWidth(pSubFuiWnd->sizePolicy().hasHeightForWidth());
				pSubFuiWnd->setSizePolicy(sizePolicy2);
				pSubFuiWnd->setSizeIncrement(QSize(1, 1));
				
				pTabWnd->setCurrentWidget(pSubFuiWnd);
				//pSubFuiWnd->setVisible(true);

				info.pFuiWidget = pSubFuiWnd;
				//pBase =	dynamic_cast<FuiBase*>(pSubFuiWnd);
				if(pBase)
					pBase->m_nFuiType = nFuiType; //store FUI type
			}
		}
		else
		{
			//refresh combo position for currently active tab
			FuiInfo info;
			info.pFuiWidget = pTabWnd->currentWidget();
			int nIdx2 = m_lstFUI.indexOf(info);

			pBase =	dynamic_cast<FuiBase*>(info.pFuiWidget);
		}
	}

	if(!bSeparateWindow)
	{
		m_nCurMainFP = nFuiType;	//remember current ID
		pTabWnd->setVisible(true);
		//pTabWnd->setUpdatesEnabled(true); //B.T: speeds up start by 300ms
	}

	Q_ASSERT(NULL != pBase);

	if(pBase)
	{
		//register observer for new FUI:
		pBase->registerObserver(this);
		pBase->SetFUIName(strTitle); //sets default FUI name from left menu

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("FUI opened as an embedded window: %1").arg(strTitle));

		//now set the required mode for the FUI window
		switch (nMode){
			case FuiBase::MODE_READ:
				if(nRecordID >= 0)
					pBase->on_selectionChange(nRecordID);
				else
					pBase->on_cmdInsert();

				break;
			case FuiBase::MODE_EDIT:
				if(nRecordID >= 0)
				{
					pBase->on_selectionChange(nRecordID);
					pBase->on_cmdEdit();
				}
				else
					pBase->on_cmdInsert();

				break;
			case FuiBase::MODE_INSERT:
				pBase->on_cmdInsert();
				break;
			case FuiBase::MODE_DELETE:
				{
					pBase->on_selectionChange(nRecordID);
					pBase->on_cmdDelete();
				}
				break;
			case FuiBase::MODE_EMPTY:	
				break;	// do nothing
			default:
				Q_ASSERT(FALSE);	//mode not supported
		}
	}

	//SHOW FUI at end of preprocessing (reading)
	//if allowed show now, this can be used to prepare FUI (fill with defaults in background, then show it later...
	if(!bDontShow)pFuiWnd->show();

	if(!bFuiAlreadyExists || (bFuiAlreadyExists && (bForceNewTab || (0 == nTabCount))))
		m_lstFUI.append(info);
	else
	{
		//reinit GID by searching for the FUI in the list
		info.nGID = GetFuiID(pBase);
	}

	emit FuiChanged();

	SetLastCommGrid(info.nGID,nFuiType);
	return info.nGID; // return unique ID
}

QWidget *FuiManager::CreateFuiWidget(int nFuiType, QWidget *pParent, bool bOpenedAsAvatar/*=false*/)
{
	switch (nFuiType){
		case MENU_PROJECTS:
			if(g_FunctionPoint.IsFPAvailable(FP_PROJECT_FUI)){
				//start splash
				g_pClientManager->StartProgressDialog(tr("Setting up Projects Interface..."));
				QWidget *pFUI = new FUI_Projects(pParent, bOpenedAsAvatar); 
				g_pClientManager->StopProgressDialog();
				return pFUI;
			}
			else
				return NULL;
		case MENU_PERSON_ACCESS_RIGHTS:
			return new PersonWidget(pParent);
		case MENU_USER_ACCESS_RIGHTS:
			return new UserWidget(pParent);
		case MENU_ROLE_DEFINITION:
			return new FUI_RoleDefinition(pParent);
		case MENU_PERSON:
			if(g_FunctionPoint.IsFPAvailable(FP_USER_FUI))
				return new FUI_BusPerson(pParent);
			else
				return NULL;
		case MENU_ORGANIZATIONS:
			if(g_FunctionPoint.IsFPAvailable(FP_ORGANIZATION_FUI))
				return new FUI_BusOrganizations(pParent);
			else
				return NULL;
		case MENU_DEPARTMENTS:
			if(g_FunctionPoint.IsFPAvailable(FP_DEPARTMENT_FUI))
				return new FUI_BusDepartments(pParent);
			else
				return NULL;
		case MENU_COST_CENTERS:
			if(g_FunctionPoint.IsFPAvailable(FP_COST_CENTER_FUI))
				return new FUI_BusCostCenters(pParent);
			else
				return NULL;
		case MENU_USER:
				if(g_FunctionPoint.IsFPAvailable(FP_USER_FUI))
					return new FUI_CoreUser(pParent);
				else
					return NULL;
		case MENU_CONTACT_TYPES:
			return new FUI_ContactType(pParent);
		case MENU_CONTACTS:
			if(g_FunctionPoint.IsFPAvailable(FP_CONTACTS_FUI)){
				//start splash
				g_pClientManager->StartProgressDialog(tr("Setting up Contacts Interface..."));

				QWidget *pFUI = new FUI_Contacts(pParent, bOpenedAsAvatar); 

				g_pClientManager->StopProgressDialog();
				return pFUI;
			}
			else
				return NULL;
		case MENU_REPORTS_PREVIEW:
			return new ReportPreviewer(pParent);
		case MENU_COMM_CENTER:
			{
				//start splash
				g_pClientManager->StartProgressDialog(tr("Setting up Personal Desktop Interface..."));

				QWidget *pFUI = new FUI_CommunicationCenter(pParent); 

				g_pClientManager->StopProgressDialog();
				return pFUI;
			}
		case MENU_COMM_VOICE_CENTER:
			return new FUI_VoiceCallCenter(pParent);
		case MENU_COMM_VOICE_CALL:
			return new FUI_VoiceCall(pParent);
		//case MENU_COMM_DOC_MANAGER:
		//	return new FUI_DocumentManager(pParent);
		case MENU_COMM_CE_TYPES:
			return new FUI_CE_Types(pParent);
		case MENU_COMM_CE_EVENT_TYPES:
			return new FUI_CeEventType(pParent);
		case MENU_IMPORT_CONTACT:
			if(g_FunctionPoint.IsFPAvailable(FP_IMPORT_CONTACTS_MANUALLY))
				return new FUI_ImportContact(pParent);
			else
				return NULL;
		case MENU_IMPORT_PERSON:
			if(g_FunctionPoint.IsFPAvailable(FP_IMPORT_USERS_MANUALLY))
				return new FUI_ImportPerson(pParent);
			else
				return NULL;
		case MENU_IMPORT_PROJECT:
			if(g_FunctionPoint.IsFPAvailable(FP_IMPORT_PROJECTS_MANUALLY))
				return new FUI_ImportProject(pParent);
			else
				return NULL;
		case MENU_IMPORT_EMAIL:
			if(g_FunctionPoint.IsFPAvailable(FP_IMPORT_EMAILS_MANUALLY))
				return new FUI_ImportEmail(pParent);
			else
				return NULL;
		case MENU_IMPORT_APPOINTMENT:
			//TOFIX if(g_FunctionPoint.IsFPAvailable(FP_IMPORT_EMAILS_MANUALLY))
				return new FUI_ImportAppointment(pParent);
			//TOFIX else
			//TOFIX		return NULL;
		case MENU_IMPORT_USER_CONTACT_RELATIONSHIP:
			return new FUI_ImportUserContactRel(pParent);
		case MENU_IMPORT_CONTACT_CONTACT_RELATIONSHIP:
			return new FUI_ImportContactContactRel(pParent);
		case MENU_PBX_CONFIG:
			return new FUI_PBXConfig(pParent);
		//case MENU_NMRX_ROLE:
		//	return new FUI_NMRXRoles(pParent);
		case MENU_OPTIONS:
			return new OptionsAndSettingsWidget(true, m_nRecordID, pParent);
		case MENU_PERSONAL_SETTINGS:
			return new OptionsAndSettingsWidget(false, m_nRecordID, pParent);
		case MENU_EMAIL:
			if(g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW))
				return new EMailDialog(pParent);
			else
				return NULL;
		case MENU_DM_APPLICATIONS:
			return new FUI_DM_Applications(pParent);
		case MENU_DM_USER_PATHS:
			return new FUI_DM_UserPaths(pParent);
		case MENU_DM_DOCUMENTS:
			return new FUI_DM_Documents(pParent);
		case COMM_GRID_FILTER_WIDGET_DESKTOP:
			return new CommGridFilter(COMM_GRID_FILTER_WIDGET_DESKTOP, pParent);
		case COMM_GRID_FILTER_WIDGET_CONTACT:
			return new CommGridFilter(COMM_GRID_FILTER_WIDGET_CONTACT, pParent);
		case COMM_GRID_FILTER_WIDGET_PROJECT:
			return new CommGridFilter(COMM_GRID_FILTER_WIDGET_PROJECT, pParent);
		case COMM_GRID_FILTER_WIDGET_CALENDAR:
			return new CommGridFilter(COMM_GRID_FILTER_WIDGET_CALENDAR, pParent);
			//issue: 2374+2371: small QCW gone...
		case MENU_QCW_LARGE:
		case MENU_QCW_SMALL:
			return new QCW_LargeWidget(pParent);
		//case MENU_QCW_SMALL:
		//	return new QCW_SmallWidget(pParent);
		case MENU_RESOURCES:
			if(g_FunctionPoint.IsFPAvailable(FP_RESOURCE_FUI))
				return new fui_resources(pParent);	
			else
				return NULL;
		case MENU_CALENDAR_EVENT:
			{
				if(!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
					return NULL;

				if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
					return NULL;

				//QApplication::setOverrideCursor(Qt::WaitCursor);
				g_pClientManager->StartProgressDialog(tr("Setting up Calendar Event Interface..."));
				QWidget *pFUI = new FUI_CalendarEvent(pParent); 
				g_pClientManager->StopProgressDialog();
				//QApplication::restoreOverrideCursor();
				return pFUI;
			}
		case MENU_CALENDAR:
			{
				g_pClientManager->StartProgressDialog(tr("Setting up Calendar Interface..."));
				QWidget *pFUI = new Fui_Calendar(pParent); 
				g_pClientManager->StopProgressDialog();
				return pFUI;
			}
		case MENU_INVITATION_CARD:
			return new FUI_InvitationCard(pParent);
		case MENU_CALENDAR_VIEWS:
			return new FUI_CalendarViewEditor(pParent);
		case MENU_PAYMENT_CONDITIONS:
			return new FUI_DebtorPayments(pParent);	
		case MENU_SPC_PROJECT_MANAGER:
			return new FUI_ProjectManager(pParent);	
		case MENU_CUSTOM_FIELDS:
			return new FUI_CustomFields(pParent);				
		default:
			Q_ASSERT(false);
	}

	return NULL;	//error: unknown FUI
}

bool FuiManager::CloseCurrentFuiTab(bool bUnconditional /*= false*/)
{
	if(m_nCurMainFP > 0)	//already in the same FUI
	{
		//destroy current FUI tab
		QTabWidget *pTabWnd = m_mapTabWnd[m_nCurMainFP];
		int nIdx = pTabWnd->currentIndex();
		if(nIdx >= 0)
		{
			QWidget *pFui = pTabWnd->widget(nIdx);

			//check can close
			FuiBase* pBase = dynamic_cast<FuiBase*>(pFui);
			if(pBase)
			{
				if (!bUnconditional)
				{
					if(!pBase->CanClose())
					{
						QMessageBox::information(m_pMainWnd, "", tr("Can not close window in edit or insert mode!"));
						return false;
					}
				}
			}

			//remove this item from history list and Fui combo box
			FuiInfo info;
			info.pFuiWidget = pFui;
			int nIdx2 = m_lstFUI.indexOf(info);
			if(nIdx2 >= 0)
			{
				if (m_lstFUI[nIdx2].nGID==m_nCurCommGridFUI)
					m_nCurCommGridFUI=-1;

				m_lstFUI.removeAt(nIdx2);
				m_pWindowMenu->removeAction(m_lstFUIMenu[nIdx2]);
				m_lstFUIMenu.removeAt(nIdx2);
			}

			pTabWnd->widget(nIdx)->deleteLater();
			pTabWnd->removeTab(nIdx);
		}
		else
		{
			Q_ASSERT(pTabWnd->count() == 0);
		}

		return true;
	}

	return false;
}

void FuiManager::OnFuiComboChanged(int nIndex)
{
	if(m_bSkipComboUpdate)
		return;

	//track and reactivate the selected FUI
	if(nIndex >= 0 && nIndex < m_lstFUI.size())
	{
		if(NULL == m_lstFUI[nIndex].pFuiPlaceholder)
		{
			//FUI in a separate window
			Q_ASSERT(NULL != m_lstFUI[nIndex].pFuiWidget);
			QWidget *pFUIWidget = m_lstFUI[nIndex].pFuiWidget;
			pFUIWidget->activateWindow();
			if(pFUIWidget->isMinimized())
				pFUIWidget->showNormal();
		}
		else
		{
			//FUI in a tab, activate proper tab
			m_pWorkAreaWnd->setCurrentWidget(m_lstFUI[nIndex].pFuiPlaceholder);
			QTabWidget *pTabWnd = m_mapTabWnd[m_lstFUI[nIndex].nFP];
			pTabWnd->show();
			pTabWnd->setCurrentWidget(m_lstFUI[nIndex].pFuiWidget);

			m_nCurMainFP = m_lstFUI[nIndex].nFP;	//refresh info
			SetLastCommGrid(m_lstFUI[nIndex].nGID,m_lstFUI[nIndex].nFP);
		}
	}

	emit FuiChanged();
}

void FuiManager::OnFuiTabSelected(int nIndex)
{
	//when tab window becomes active select proper FUI name inside the combo box
	if(m_nCurMainFP > 0)
	{
		QTabWidget *pTabWnd = m_mapTabWnd[m_nCurMainFP];
		QWidget *pFuiWnd = pTabWnd->currentWidget();

		//refreshcurrent index in the combo to match current Fui
		FuiInfo info;
		info.pFuiWidget = pFuiWnd;
		int nIdx2 = m_lstFUI.indexOf(info);
		if(nIdx2 >= 0)
			SetLastCommGrid(m_lstFUI[nIdx2].nGID,m_lstFUI[nIdx2].nFP);
	}
}

void FuiManager::OnFuiWindowClose(QWidget *pWindow)
{
	//remove this item from history list and Fui combo box
	FuiInfo info;
	info.pFuiWidget = pWindow;
	int nIdx2 = m_lstFUI.indexOf(info);
	if(nIdx2 >= 0)
	{
		int nID = m_lstFUI[nIdx2].nFP;
		QTabWidget *pTabWnd = m_lstFUI[nIdx2].pTabWidget;
		//TOFIX? clear maps

		if (m_lstFUI[nIdx2].nGID==m_nCurCommGridFUI) //B.T: fur marko
		{

			QTabWidget *pTabWnd = m_mapTabWnd[m_nCurMainFP];
			if (pTabWnd)
			{
				QWidget *pFuiWnd = pTabWnd->currentWidget();

				//refreshcurrent index in the combo to match current Fui
				FuiInfo info;
				info.pFuiWidget = pFuiWnd;
				int nIdx2 = m_lstFUI.indexOf(info);
				if(nIdx2 >= 0)
					SetLastCommGrid(m_lstFUI[nIdx2].nGID,m_lstFUI[nIdx2].nFP);
				else
					m_nCurCommGridFUI=-1;
			}
			else
					m_nCurCommGridFUI=-1;
		}

		m_lstFUI.removeAt(nIdx2);

		m_pWindowMenu->removeAction(m_lstFUIMenu[nIdx2]);
		m_lstFUIMenu.removeAt(nIdx2);


	}
}

int FuiManager::FindFUIWidget(int nGID)
{
	int nCnt = m_lstFUI.size();
	for(int i=0; i<nCnt; i++)
	{
		if(m_lstFUI[i].nGID == nGID)
			return i;
	}

	return -1;	// failed to find FUI
}

int FuiManager::GetFuiID(FuiBase *pWidget)
{
	int nCnt = m_lstFUI.size();
	for(int i=0; i<nCnt; i++)
		if(m_lstFUI[i].pFuiWidget == pWidget)
			return m_lstFUI[i].nGID;

	return -1;	// failed to find FUI
}

QWidget *FuiManager::GetFUIWidget(int nGID)
{
	int nIdx = FindFUIWidget(nGID);
	if(nIdx >= 0)
		return m_lstFUI[nIdx].pFuiWidget;

	return NULL;	// failed to find FUI
}

FuiBase *FuiManager::GetFUI(int nGID)
{
	//get Widget
	QWidget* pFUIWidget=GetFUIWidget(nGID);
	Q_ASSERT(pFUIWidget!=NULL);
	if(pFUIWidget==NULL) return NULL;

	//dynamic cast:
	FuiBase* pFUIBase=dynamic_cast<FuiBase*>(pFUIWidget);
	Q_ASSERT(pFUIBase!=NULL);

	return pFUIBase;
}

bool FuiManager::CloseFUI(int nGID, bool bUnconditional)
{
	//find widget
	int nIdx = FindFUIWidget(nGID);
	if(nIdx < 0)
		return false;

	int nType = -1;

	//check can close
	FuiBase* pBase = dynamic_cast<FuiBase*>(m_lstFUI[nIdx].pFuiWidget);
	if(pBase)
	{
		nType = pBase->m_nFuiType;

		if(bUnconditional)
			pBase->m_bUnconditionalClose = true;
		else
		{
			if(!pBase->CanClose())
			{
				QString strFUIName=pBase->GetFUIName();
				QMessageBox::information(m_pMainWnd, "", tr("Can not close window ") + strFUIName + tr(" in edit or insert mode!"));
				return false;
			}
		}
	}

	//bool bOK;

	if(m_lstFUI[nIdx].pTabWidget == NULL)
	{
		//widget is in a separate window
		m_lstFUI[nIdx].pFuiWidget->close();
		//m_lstFUI[nIdx].pFuiWidget->deleteLater();
	}
	else
	{
		//widget is in a tab
		int nTabIdx = m_lstFUI[nIdx].pTabWidget->indexOf(m_lstFUI[nIdx].pFuiWidget);
		Q_ASSERT(nTabIdx >= 0);
		if(nTabIdx >= 0)
		{
			//QWidget *pWidget=m_lstFUI[nIdx].pTabWidget->widget(nTabIdx);
			m_lstFUI[nIdx].pTabWidget->removeTab(nTabIdx);
			//m_lstFUI[nIdx].pFuiWidget->deleteLater();
			//QWidget *p1=m_lstFUI[nIdx].pFuiWidget;
			m_lstFUI[nIdx].pFuiWidget->close();
		}

		//m_lstFUI[nIdx].pFuiWidget->close();
		//if(m_nCurMainFP == m_lstFUI[nIdx].nFP)
	}


	m_lstFUI.erase(m_lstFUI.begin() + nIdx);

	m_pWindowMenu->removeAction(m_lstFUIMenu[nIdx]);
	m_lstFUIMenu.removeAt(nIdx);

	if(nType >= 0){
		//clean widget map TOFIX only if no other instances of the same type?
		QMap<int, QWidget *>::iterator it = m_mapFuiWnd.find(nType);
		if(it != m_mapFuiWnd.end())
			m_mapFuiWnd.erase(it);
	}

	emit FuiChanged();

	return true;
}

bool FuiManager::CloseAllFUIWindows(bool bUnconditional)
{
	while(m_lstFUI.size()){
		if(!CloseFUI(m_lstFUI[0].nGID, bUnconditional)){
			ActivateFUI(m_lstFUI[0].nGID);	//position on the FUI that failed to close
			return false;
		}
	}

	return true;
}

int	FuiManager::FromEntityToMenuID(int nEntityID)
{
	switch(nEntityID){
		case ENTITY_BUS_PERSON:					return MENU_PERSON;
		case ENTITY_BUS_ORGANIZATION:			return MENU_ORGANIZATIONS;
		case ENTITY_BUS_DEPARTMENT:				return MENU_DEPARTMENTS;
		case ENTITY_BUS_COST_CENTER:			return MENU_COST_CENTERS;
		case ENTITY_BUS_CONTACT:				return MENU_CONTACTS;
		case ENTITY_CE_TYPES:					return MENU_COMM_CE_TYPES;
		case ENTITY_BUS_PROJECT:				return MENU_PROJECTS;
		case ENTITY_BUS_DM_APPLICATIONS:		return MENU_DM_APPLICATIONS;
		case ENTITY_BUS_DM_TEMPLATES:			return MENU_DM_DOCUMENTS;
		case ENTITY_CALENDAR_RESOURCES:			return MENU_RESOURCES;
		case ENTITY_PAYMENT_CONDITIONS:			return MENU_PAYMENT_CONDITIONS;
		case ENTITY_CUSTOM_FIELDS:				return MENU_CUSTOM_FIELDS;

		//TOFIX case ENTITY_CE_EVENT_TYPES:				return MENU_COMM_CE_EVENT_TYPES;
		default:
			Q_ASSERT(false);
	}
	return -1;
}

//Catches global signals:
//when FUI changes state, rename FUI name in the combo box
void FuiManager::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	//nMsgDetail = FuiBase::GetMode();
	//val = FuiBase::GetFUIName();
	if(nMsgCode==FuiBase::FUI_CHANGED_NAME)
	{
		int nCnt = m_lstFUI.size();
		for(int i=0; i<nCnt; i++)
			if(dynamic_cast<ObsrPtrn_Subject*>(m_lstFUI[i].pFuiWidget) == pSubject)
			{
				//set new name of FUI in combo
				
				if(nMsgDetail==FuiBase::MODE_EDIT || nMsgDetail==FuiBase::MODE_INSERT)
				{
					m_lstFUIMenu[i]->setIcon(QIcon(":handwrite.png"));
				}
				else
				{
					m_lstFUIMenu[i]->setIcon(QIcon());
				}
				
				m_lstFUIMenu[i]->setText(val.toString()); 

				//set new name of FUI in a tab or window title
				if(NULL != m_lstFUI[i].pTabWidget)
				{
					QTabWidget *pTabWnd = m_lstFUI[i].pTabWidget;
					int nIdx = pTabWnd->indexOf(m_lstFUI[i].pFuiWidget);
					if(nIdx >= 0)
						pTabWnd->setTabText(nIdx, val.toString());
				}
				else if(NULL != m_lstFUI[i].pFuiWidget)
				{
					m_lstFUI[i].pFuiWidget->setWindowTitle(val.toString());
				}


			}
	}
}

//when creating new TAB add close button:
void FuiManager::SetupTabIcons(QTabWidget *pTabWidget)
{
	//will be destroyed as childs:
	if(m_pActClose==NULL)
	{
		QKeySequence key1(tr("Ctrl+F4"));
		m_pActClose = new QAction(tr("Close Tab"),m_pMainWnd);
		m_pActClose->setIcon(QIcon(":close.png"));
		m_pActClose->setShortcut(key1);
		connect(m_pActClose, SIGNAL(triggered()), this, SLOT(CloseCurrentFuiTab()));

		m_pActMaximize = new QAction(tr("Maximize Window"),m_pMainWnd);
		m_pActMaximize->setIcon(QIcon(":Fullsize.png"));
		connect(m_pActMaximize, SIGNAL(triggered()), this, SLOT(MaximizeToFullScreen()));
	}

	QSize buttonSize(24, 24);
	QWidget *container= new QWidget(pTabWidget);

	QToolButton *pBtnClose = new QToolButton(container); //parent is necessary to be TAB...
	pBtnClose->setMaximumSize(buttonSize);
	pBtnClose->setAutoRaise(true);
	pBtnClose->setDefaultAction(m_pActClose);

	QToolButton *pBtnMaximize = new QToolButton(container); //parent is necessary to be TAB...
	pBtnMaximize->setMaximumSize(buttonSize);
	pBtnMaximize->setAutoRaise(true);
	pBtnMaximize->setDefaultAction(m_pActMaximize);
	
	QHBoxLayout *vbox = new QHBoxLayout;
	vbox->addWidget(pBtnMaximize);
	vbox->addWidget(pBtnClose);
	vbox->addStretch(1);
	vbox->setMargin(0);
	vbox->setSpacing(0);
	container->setLayout(vbox);

	//add as corner widget :
	pTabWidget->setCornerWidget(container);
}

//re-emit this signal (widget must be child):
void FuiManager::MaximizeToFullScreen()
{
	//change icon
	if (m_bMaximized)
	{
		m_pActMaximize->setToolTip(tr("Maximize Window"));
		m_pActMaximize->setIcon(QIcon(":Fullsize.png"));
		m_bMaximized=false;
	}
	else
	{
		m_pActMaximize->setToolTip(tr("Minimize Window"));
		m_pActMaximize->setIcon(QIcon(":Smallsize.png"));
		m_bMaximized=true;
	}

	emit TabWantsToMaximizeToFullScreen();
}

void FuiManager::OnFuiWindowMenuTriggered(QAction *action, bool bChecked)
{
	//find action index in the list
	int nIdx = 0;
	int nCount = m_lstFUIMenu.size();
	for(int i=0; i<nCount; i++){
		if(action == m_lstFUIMenu[i]){
			nIdx = i;
			break;
		}
	}

	//handle action
	Q_ASSERT(nIdx >= 0);
	if(nIdx >= 0)
		OnFuiComboChanged(nIdx);
}

bool FuiManager::ActivateFUI(int nGID)
{
	//find widget
	int nIdx = FindFUIWidget(nGID);
	if(nIdx < 0)
		return false;

	OnFuiComboChanged(nIdx);
	return true;
}



bool FuiManager::IsInstanceAlive(int nFuiCode)
{
	int nSize=m_lstFUI.size();
	for(int i=0;i<nSize;++i)
	{
		if (m_lstFUI.at(i).nFP==nFuiCode)
			return true;
	}

	return false;
}

void FuiManager::SetLastCommGrid(int nFui, int nFuiType)
{
	if (nFuiType==MENU_COMM_CENTER || nFuiType==MENU_PROJECTS || nFuiType==MENU_CONTACTS)
	{
		m_nCurCommGridFUI=nFui;
	}	
}

bool FuiManager::IsStandAloneWidget(QWidget *pWidget)
{
	FuiInfo info;
	info.pFuiWidget = pWidget;
	int nIdx2 = m_lstFUI.indexOf(info);
	if (nIdx2 >= 0)
		return (NULL == m_lstFUI[nIdx2].pTabWidget);
	else
		return false;
}

QWidget* FuiManager::GetCurrentActiveTabFUI()
{
	if(m_nCurMainFP > 0)	//already in the same FUI
	{
		//destroy current FUI tab
		QTabWidget *pTabWnd = m_mapTabWnd[m_nCurMainFP];
		int nIdx = pTabWnd->currentIndex();
		if(nIdx >= 0)
			return pTabWnd->widget(nIdx);
	}
	return NULL;
}




void FuiManager::InstallEventFilterOnAllContainers(QObject *pFilter)
{
//BT: event filter does not work on widget parent: events are propagated from children (e.g button) to the parrent..
	/*
	m_pMainWnd->setEnabled(false);
	//m_pMainWnd->installEventFilter(pFilter);

	int nSize=m_lstFUI.size();
	for(int i=0;i<nSize;++i)
	{
		//m_lstFUI.at(i).pFuiWidget->installEventFilter(pFilter);
		m_lstFUI.at(i).pFuiWidget->setEnabled(false);
	}
*/

	/*
	//m_pMainWnd->installEventFilter(pFilter);
	m_pMainWnd->setEnabled(false);



	QMapIterator<int, QWidget *> i(m_mapFuiWnd);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->setEnabled(false);
		//i.value()->installEventFilter(pFilter);
	} 
	*/
}
void FuiManager::DeInstallEventFilterOnAllContainers(QObject *pFilter)
{
//BT: event filter does not work on widget parent: events are propagated from children (e.g button) to the parrent..

	/*
	//m_pMainWnd->removeEventFilter(pFilter);
	m_pMainWnd->setEnabled(true);

	int nSize=m_lstFUI.size();
	for(int i=0;i<nSize;++i)
	{
		//m_lstFUI.at(i).pFuiWidget->removeEventFilter(pFilter);
		m_lstFUI.at(i).pFuiWidget->setEnabled(true);
	}
	*/
	/*
	//m_pMainWnd->removeEventFilter(pFilter);
	m_pMainWnd->setEnabled(true);

	QMapIterator<int, QWidget *> i(m_mapFuiWnd);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->setEnabled(true);
		//i.value()->removeEventFilter(pFilter);
	} 
	*/

}


bool FuiManager::IsActiveWindowFUI(int nFuiType)
{
	//FuiBase *fui=dynamic_cast<FuiBase*>(QApplication::activeWindow());
	if (m_pFuiActive)
		if (m_pFuiActive->m_nFuiType==nFuiType)
			return true;

	return false;

}
bool FuiManager::IsActiveWindowCurrentFUI(FuiBase *current)
{
	//FuiBase *fui=dynamic_cast<FuiBase*>(QApplication::activeWindow());
	if (m_pFuiActive==current)
			return true;

	return false;
}
//if nRecordID is valid and mode is empty: use existing active fui if record=record
int	FuiManager::OpenSideBarModeFUI(int nFuiCode, int nMode, int nRecordID,bool bDontShow)
{
	//FuiBase *fui=dynamic_cast<FuiBase*>(QApplication::activeWindow());
	bool bUseActiveFui=false;
	if (m_pFuiActive)
		if (m_pFuiActive->m_nFuiType==nFuiCode && m_pFuiActive->m_bSideBarModeView)
		{
			if (m_pFuiActive->GetMode()==FuiBase::MODE_EDIT || m_pFuiActive->GetMode()==FuiBase::MODE_INSERT)
				return GetFuiID(m_pFuiActive); //issue 1171: use current fui always
			else
				bUseActiveFui = true;

			//if active fui has selected nRecordID and in edit mode, and edit mode is requested, use it
			if (!bUseActiveFui && nRecordID>0 && m_pFuiActive->GetMode()==FuiBase::MODE_EDIT && m_pFuiActive->m_nEntityRecordID==nRecordID)
			{
				return GetFuiID(m_pFuiActive); //use
			}

		}
	



	if (bUseActiveFui) //only if non insert or non edit mode
	{
			switch (nMode)
			{
				case FuiBase::MODE_READ:
					if(nRecordID >= 0)
						m_pFuiActive->on_selectionChange(nRecordID);
					else
						m_pFuiActive->on_cmdInsert();

					break;
				case FuiBase::MODE_EDIT:
					if(nRecordID >= 0)
					{
						m_pFuiActive->on_selectionChange(nRecordID);
						m_pFuiActive->on_cmdEdit();
					}
					else
						m_pFuiActive->on_cmdInsert();

					break;
				case FuiBase::MODE_INSERT:
					m_pFuiActive->on_cmdInsert();
					break;
				case FuiBase::MODE_DELETE:
					{
						m_pFuiActive->on_selectionChange(nRecordID);
						m_pFuiActive->on_cmdDelete();
					}
			
					break;

				case FuiBase::MODE_EMPTY:	
					break;	// do nothing
				default:
					Q_ASSERT(FALSE);	//mode not supported
			}

		return GetFuiID(m_pFuiActive);
	}
	else
	{
		return OpenFUI(nFuiCode,true,false,nMode,nRecordID,bDontShow);
	}

}

void FuiManager::SetActiveFUI(FuiBase *pFuiActive)
{
	m_pFuiActive=pFuiActive;
}

//to be sure that closed fui cleans:
void FuiManager::ClearActiveFUI(FuiBase *pFuiActive)
{
	if (pFuiActive==m_pFuiActive)
		m_pFuiActive=NULL;
}


void FuiManager::RegisterSideBarCommMenuSelector(int nFuiType,CommunicationMenu_Base *pCommMenu)
{
	m_hashSideBarCommMenu[nFuiType]=pCommMenu;
}
CommunicationMenu_Base* FuiManager::GetSideBarCommMenuSelector(int nFuiType)
{
	return m_hashSideBarCommMenu.value(nFuiType,NULL);
}

void FuiManager::UnRegisterSideBarCommMenuSelector(int nFuiType,CommunicationMenu_Base *pCommMenu)
{
	//only if it is actual registered selector for that type of fui:
	if (m_hashSideBarCommMenu.value(nFuiType,NULL)==pCommMenu)
		m_hashSideBarCommMenu.remove(nFuiType);
}


//depending on resoultion, open QCW->must connect signals..
FuiBase* FuiManager::OpenQCWWindow(QWidget *parent, int nContactID,DbRecordSet *lstContacts,QString strDropText,DbRecordSet *recDefaultContact)
{
	//get available space
	QRect desktopRec=QApplication::desktop()->availableGeometry(parent);

	ObsrPtrn_Observer *pParentObserver=dynamic_cast<ObsrPtrn_Observer*>(parent);
	//Q_ASSERT(pParentObserver);//parent must be observer

	FuiBase *pQCW=NULL;
	bool bSmallInUse=false;

	//issue: 2374+2371: small QCW gone...

	//if (desktopRec.width()<=950 || desktopRec.height()<=700)
	//{
	//	pQCW=GetFUI(OpenFUI(MENU_QCW_SMALL,true,false,FuiBase::MODE_EMPTY,-1,true));
	//	bSmallInUse=true;
	//}
	//else
	//	pQCW=GetFUI(OpenFUI(MENU_QCW_SMALL,true,false,FuiBase::MODE_EMPTY,-1,true));
		pQCW=GetFUI(OpenFUI(MENU_QCW_LARGE,true,false,FuiBase::MODE_EMPTY,-1,true));

	//if failed

	if (!pQCW)
		return NULL;

	//set mode + data:
	bool bOK=true;
	if (lstContacts)
	{
		bOK=dynamic_cast<QCW_Base*>(pQCW)->Initialize(*lstContacts);
	}
	else if (recDefaultContact)
	{
		dynamic_cast<QCW_Base*>(pQCW)->SetDefaultValues(*recDefaultContact,strDropText);
	}
	else if (!strDropText.isEmpty())
	{
		bOK=dynamic_cast<QCW_Base*>(pQCW)->Initialize(strDropText);
	}
	else
	{
		bOK=dynamic_cast<QCW_Base*>(pQCW)->Initialize(nContactID);
	}

	if (!bOK)
	{
		//probably wrong contact ID!??
		//Q_ASSERT(false); 
		pQCW->destroy();
		return NULL;
	}


	pQCW->updateGeometry();
	if (pParentObserver)
		pQCW->registerObserver(pParentObserver);
	pQCW->show();
	return pQCW; //dynamic_cast<ObsrPtrn_Subject*>(pQCW); //qcw must be subject
}

int FuiManager::FindFuiByType(int nTypeID, int nTaskID)
{
	int nSize=m_lstFUI.size();
	for(int i=0;i<nSize;++i)
	{
		if(m_lstFUI.at(i).nFP == nTypeID)
		{
			if(nTaskID >= 0)	//match task ID too?
			{
				QWidget *pFui=m_lstFUI.at(i).pFuiWidget;

				bool bTaskMatch = false;
				if(nTypeID == MENU_COMM_VOICE_CENTER)
				{
					if(((FUI_VoiceCallCenter *)pFui)->GetTaskID() == nTaskID)
						bTaskMatch = true;
				}
				else if(nTypeID == MENU_EMAIL)
				{
					if(((EMailDialog *)pFui)->GetTaskID() == nTaskID)
						bTaskMatch = true;
				}
				else if(nTypeID == MENU_DM_DOCUMENTS)
				{
					if(((FUI_DM_Documents *)pFui)->GetTaskID() == nTaskID)
						bTaskMatch = true;
				}
				if(bTaskMatch)
					return m_lstFUI.at(i).nGID;	//matched
			}
			else
				return m_lstFUI.at(i).nGID;	//matched
		}
	}

	return -1;	//unmatched
}

void FuiManager::RefreshFUI(int nGID, bool bActivate)
{
	FuiBase *pFui = GetFUI(nGID);
	if(pFui){
		//reload data
		bool bVisible = pFui->isVisible();
		pFui->on_selectionChange(pFui->GetEntityRecordID());
		if(!bVisible)
			pFui->hide();
		else if(bActivate)
			ActivateFUI(nGID);

		//refresh avatars if created
		if(NULL != pFui->m_pWndAvatar)
		{
			if(NULL != dynamic_cast<FUI_VoiceCallCenter*>(pFui))
				((FUI_VoiceCallCenter *)pFui)->RefreshAvatarData(true);
			else if(NULL != dynamic_cast<EMailDialog*>(pFui))
				((EMailDialog *)pFui)->RefreshAvatarData(true);
			else if(NULL != dynamic_cast<FUI_DM_Documents*>(pFui))
				((FUI_DM_Documents *)pFui)->RefreshAvatarData(true);
		}
	}
}
