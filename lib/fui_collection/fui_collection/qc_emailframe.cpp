#include "qc_emailframe.h"
#include "common/common/entity_id_collection.h"
#include "qcw_largewidget.h"
#include "qcw_smallwidget.h"
#include "gui_core/gui_core/thememanager.h"

QC_EmailFrame::QC_EmailFrame(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	ui.addSelected_toolButton->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_2->setIcon(QIcon(":CopyText16.png"));

	connect(ui.selector_widget, SIGNAL(RowChanged(int)), this, SLOT(on_RowChanged(int)));
}

QC_EmailFrame::~QC_EmailFrame()
{

}

void QC_EmailFrame::Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType /*= 0 0-Large,1-small widget*/)
{
	m_nParentType = nParentType;
	m_pParentWidget = ParentWidget;
	m_recFullContactData = recFullContactData;

	ui.selector_widget->Initialize(ENTITY_BUS_CM_EMAIL, m_recFullContactData);
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

void QC_EmailFrame::AddEmail(DbRecordSet recEmail)
{
	if (ui.email_lineEdit->text().isEmpty())
		ui.selector_widget->addEntity(recEmail);
	else
		ui.selector_widget->addEntityNoOverride(recEmail);
}

void QC_EmailFrame::SetupGUIwidgets(DbRecordSet recData)
{
	ui.email_lineEdit->setText(recData.getDataRef(0, "BCME_ADDRESS").toString());
	ui.description_lineEdit->setText(recData.getDataRef(0, "BCME_DESCRIPTION").toString());
	if (recData.getDataRef(0, "BCME_IS_DEFAULT").toBool())
		ui.isDefault_checkBox->setCheckState(Qt::Checked);
	else
		ui.isDefault_checkBox->setCheckState(Qt::Unchecked);
}

void QC_EmailFrame::SetDataToRecordSet(QString strRecordSetFieldName, QString strValue)
{
	m_recLocalEntityRecordSet.setData(m_nCurrentRow, strRecordSetFieldName, strValue);
	m_recFullContactData->setData(0, "LST_EMAIL", m_recLocalEntityRecordSet);
}

void QC_EmailFrame::SetIntDataToRecordSet(QString strRecordSetFieldName, int nValue)
{
	m_recLocalEntityRecordSet.setData(m_nCurrentRow, strRecordSetFieldName, nValue);
	m_recFullContactData->setData(0, "LST_EMAIL", m_recLocalEntityRecordSet);
}

void QC_EmailFrame::on_RowChanged(int nRow)
{
	DbRecordSet tmpRowLocalEntityRecordSet;
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_EMAIL").value<DbRecordSet>();

	tmpRowLocalEntityRecordSet.copyDefinition(m_recLocalEntityRecordSet);
	if (nRow < 0)
	{
		tmpRowLocalEntityRecordSet.addRow();
	}
	else
	{
		m_recLocalEntityRecordSet.clearSelection();
		m_recLocalEntityRecordSet.selectRow(nRow);
		tmpRowLocalEntityRecordSet.merge(m_recLocalEntityRecordSet, true);
	}

	m_nCurrentRow = nRow;

	SetupGUIwidgets(tmpRowLocalEntityRecordSet);
}

void QC_EmailFrame::on_email_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCME_ADDRESS", text);
}

void QC_EmailFrame::on_description_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCME_DESCRIPTION", text);
}

void QC_EmailFrame::on_addSelected_toolButton_clicked()
{
	if (m_nParentType == 0)
		ui.email_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.email_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_EmailFrame::on_addSelected_toolButton_2_clicked()
{
	if (m_nParentType == 0)
		ui.description_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.description_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_EmailFrame::on_isDefault_checkBox_stateChanged(int state)
{
	if (state == Qt::Checked)
	{
		int nRowCount = m_recLocalEntityRecordSet.getRowCount();
		for (int i=0; i < nRowCount; i++)
			m_recLocalEntityRecordSet.setData(i, "BCME_IS_DEFAULT", 0);

		SetIntDataToRecordSet("BCME_IS_DEFAULT", 1);
	}
	else
		SetIntDataToRecordSet("BCME_IS_DEFAULT", 0);
}
