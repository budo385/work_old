#include "roletreeview.h"
#include "fui_collection/fui_collection/roleinputdialog.h"
//#include "fui_collection/fui_collection/roledefinitionwidget.h"

RoleTreeView::RoleTreeView(QWidget *parent)
    : QTreeView(parent)
{
	//Header
	QHeaderView *Header = new QHeaderView(Qt::Horizontal);
	Header->setResizeMode(QHeaderView::Stretch);
	Header->hide();
	setHeader(Header);

	//Drag&Drop
	setAcceptDrops(true);
	setDragEnabled(true);
	setDropIndicatorShown(true);

	//Selection
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectItems);

	//Tab navigation.
	setTabKeyNavigation(true);

	//Role and AccRghts context menu.
	CreateRolesContextMenuActions(m_lstRoleActions);
	CreateAccRightsContextMenuActions(m_lstAccRightsActions);

	setContextMenuPolicy(Qt::DefaultContextMenu);
}

RoleTreeView::~RoleTreeView()
{

}

void RoleTreeView::dragMoveEvent(QDragMoveEvent *event)
{
	//Get drag index.
	QModelIndex index = indexAt(event->pos());
	//Select item under cursor.
	this->selectionModel()->select(index, QItemSelectionModel::SelectCurrent);

	//Can move only Roles not Persons.
	if (!index.parent().isValid())
		event->accept();
	else
		event->ignore();
}

void RoleTreeView::dragEnterEvent(QDragEnterEvent *event)
{
	//Check what are we dragging in.
	if (event->mimeData()->hasFormat("helix/accrightsitem"))
		event->accept();
	else
		event->ignore();
}

void RoleTreeView::dropEvent(QDropEvent *event)
{
	if (!event->mimeData()->hasFormat("helix/accrightsitem"))
	{
		event->ignore();
		return;
	}

	if (event->source() == this)
	{
		///Get destination index.
		QModelIndex index = indexAt(event->pos());

		//Check is index valid.
		if (!index.isValid())
		{
			event->ignore();
			return;
		}

		//Get destination Item.
		DbRecordSetItem *Item = static_cast<DbRecordSetItem*>(index.internalPointer());
		//Get parent children RowID list.
		QList<int> list = Item->GetChildrenRowIDList();

		//Get selected(dropped) index.
		QModelIndex SelectedIndex = this->selectionModel()->currentIndex();
		//Get Item.
		DbRecordSetItem *SelectedItem = static_cast<DbRecordSetItem*>(SelectedIndex.internalPointer());
		//Get dropped item RowID.
		int childRowID = SelectedItem->GetRowId();

		//If there is already that item in list Parent (that role in person) ignore drop.
		if (list.contains(childRowID) || index.parent().isValid())
		{
			event->ignore();
			return;
		}

		//Set dropped item to model.
		(static_cast<RoleDefTreeModel*> (model()))->SetDropedItem(SelectedItem);
		model()->dropMimeData(event->mimeData(), event->dropAction(), 0, 0, index);
		event->accept();
	}
	else
	{
		///Get destination index.
		QModelIndex index = indexAt(event->pos());
		//Check is index valid.
		if (!index.isValid())
		{
			event->ignore();
			return;
		}
		//Get destination Item.
		DbRecordSetItem *Item = static_cast<DbRecordSetItem*>(index.internalPointer());
		//Get parent children RowID list.
		QList<int> list = Item->GetChildrenRowIDList();

		//Get access right set list view.
		QListView *ListView = (static_cast<QListView*> (event->source()));
		//Get access right set selected item index.
		QModelIndex AccRSetIndex = ListView->selectionModel()->currentIndex();
		//Get access right set item.
		
		//TOFIX: BT:NERADDDDDDDDDDDDDDDDDDDDDDDDDDDDDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
		//TOFIX: BT:NERADDDDDDDDDDDDDDDDDDDDDDDDDDDDDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
		//TOFIX: BT:NERADDDDDDDDDDDDDDDDDDDDDDDDDDDDDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
		DbRecordSetItem *AccRSetItem = NULL; //static_cast<AccRightsSetListModel*>(ListView->model())->GetItemsHash()->value(AccRSetIndex.row());
//		DbRecordSetItem *AccRSetItem = static_cast<DbRecordSetItem*>(AccRSetIndex.internalPointer());
		int childRowID = AccRSetItem->GetRowId();

		//If there is already that item in list Parent (that role in person) ignore drop.
		if (list.contains(childRowID))
		{
			event->ignore();
			return;
		}

		//Set dropped item to model.
		(static_cast<RoleDefTreeModel*> (model()))->SetDropedItem(AccRSetItem);
		model()->dropMimeData(event->mimeData(), event->dropAction(), 0, 0, index);
		event->accept();
	}

	event->ignore();
}

void RoleTreeView::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu CnxtMenu(this);
	QModelIndex index = this->indexAt(event->pos());

	//If clicked somewhere no context.
	if (!index.isValid())
		return;

	m_indContextMenuItemIndex = QPersistentModelIndex(index);

	//If clicked on Role Item.
	if (index.parent().isValid())
	{
		int nSize=m_lstAccRightsActions.size();
		for(int i=0;i<nSize;++i)
			CnxtMenu.addAction(m_lstAccRightsActions.at(i));
	}
	else
	{
		int nSize=m_lstRoleActions.size();
		for(int i=0;i<nSize;++i)
			CnxtMenu.addAction(m_lstRoleActions.at(i));
	}

	CnxtMenu.exec(event->globalPos());
}

void RoleTreeView::CreateRolesContextMenuActions(QList<QAction*>& lstActions)
{
	//Rename Role:
	QAction* pAction = new QAction(tr("&Rename"), this);
	pAction->setData(QVariant(true));	//means that is enabled only in view mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(RenameRole()));

	lstActions.append(pAction);
	this->addAction(pAction);

	//Add separator
	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	//Delete item:
	pAction = new QAction(tr("&Delete"), this);
	pAction->setData(QVariant(true));	//means that is enabled only in view mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteItem()));

	lstActions.append(pAction);
	this->addAction(pAction);
}

void RoleTreeView::CreateAccRightsContextMenuActions(QList<QAction*>& lstActions)
{
	//Delete item:
	QAction* pAction = new QAction(tr("&Delete"), this);
	pAction->setData(QVariant(true));	//means that is enabled only in view mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteItem()));

	lstActions.append(pAction);
	this->addAction(pAction);
}

void RoleTreeView::RenameRole()
{
	//Find item.
	DbRecordSetItem *RoleItem = static_cast<DbRecordSetItem*>(m_indContextMenuItemIndex.internalPointer());

	//Get item data.
	QString RoleName  = RoleItem->data(0).toString();
	QString RoleDescr = RoleItem->data(1).toString();
	int		RoleType  = RoleItem->data(2).toInt();

	//Call input dialog.
	RoleInputDialog *RoleInput = new RoleInputDialog(&RoleName, &RoleDescr, &RoleType, false);
	RoleInput->exec();

	//Wait until we come back from dialog.
	if (RoleInput->Rejected)
	{
		delete(RoleInput);
		selectionModel()->clear();
		return;
	}

	//Set Name and Description to model, then call SetData.
	static_cast<RoleDefTreeModel*>(model())->SetRoleName(RoleName);
	static_cast<RoleDefTreeModel*>(model())->SetRoleDescription(RoleDescr);
	//Rename.
	static_cast<RoleDefTreeModel*>(model())->setData(m_indContextMenuItemIndex, QVariant(), Qt::EditRole);

	//Clear selection.
	selectionModel()->clear();

	//Delete dialog.
	delete(RoleInput);
	return;
}

void RoleTreeView::DeleteItem()
{
	//Remove rows
	static_cast<RoleDefTreeModel*>(model())->removeRows(m_indContextMenuItemIndex.row(), 1, m_indContextMenuItemIndex);
	selectionModel()->clear();
}
