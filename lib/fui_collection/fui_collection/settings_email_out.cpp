#if defined(WIN32)
#  define NOMINMAX
#endif

#include "settings_email_out.h"
#include "os_specific/os_specific/mailmanager.h"
#include "trans/trans/xmlutil.h"
#include "os_specific/os_specific/mailpp/src/mailpp/mailpp.h"
#include "os_specific/os_specific/mimepp/src/mimepp/mimepp.h"
#include <QtWidgets/QMessageBox>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include "common/common/rsakey.h"

#if defined(WIN32)
#  define WIN32_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#  define strcasecmp _stricmp
#endif

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

QString GenerateAccUID();

Settings_Email_Out::Settings_Email_Out(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
   	:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

	ui.previous_toolButton->setIcon(QIcon(":1leftarrow.png"));
	ui.next_toolButton->setIcon(QIcon(":1rightarrow.png"));
	ui.delete_toolButton->setIcon(QIcon(":Delete16_qcw.png"));
	ui.add_toolButton->setIcon(QIcon(":Insert16_qcw.png"));

	ui.cboSecurity->addItem(tr("NONE"),	   EML_SECURITY_NONE);
	ui.cboSecurity->addItem(tr("OpenTLS"), EML_SECURITY_STARTTLS);
	ui.cboSecurity->addItem(tr("SSL/TLS"), EML_SECURITY_SSLTLS);
	
	//provider definitions
	m_lstProviders.append(EmailProvider("GMail", "pop.gmail.com", EML_TYPE_POP3, 995, EML_SECURITY_SSLTLS));
	m_lstProviders.append(EmailProvider("Yahoo", "plus.pop.mail.yahoo.com", EML_TYPE_POP3, 995, EML_SECURITY_SSLTLS));
	m_lstProviders.append(EmailProvider("Hotmail", "pop3.live.com", EML_TYPE_POP3, 995, EML_SECURITY_SSLTLS));
	m_lstProviders.append(EmailProvider("gmx.de", "pop.gmx.net", EML_TYPE_POP3, 995, EML_SECURITY_SSLTLS));
	m_lstProviders.append(EmailProvider("web.de", "pop3.web.de", EML_TYPE_POP3, 995, EML_SECURITY_SSLTLS));
	m_lstProviders.append(EmailProvider("AOL", "pop.aim.com", EML_TYPE_POP3, 995, EML_SECURITY_SSLTLS));

	ui.cboProviders->addItem(tr("OTHER"));
	int nCnt = m_lstProviders.length();
	for(int i=0; i<nCnt; i++)
		ui.cboProviders->addItem(m_lstProviders[i].m_strName);

	//fetch info
	QByteArray arData = GetSettingValue(EMAIL_IN_SETTINGS_ACCOUNTS).toByteArray();
	m_recAccounts = XmlUtil::ConvertByteArray2RecordSet_Fast(arData);

	//position to the first page
	int nPages = m_recAccounts.getRowCount();
	m_nCurrentPage = 0;
	if(m_nCurrentPage >= nPages)
		m_nCurrentPage --;

	m_bChanged = false;

	SetupCurrentPage();
}

Settings_Email_Out::~Settings_Email_Out()
{
}

DbRecordSet Settings_Email_Out::GetChangedValuesRecordSet()
{
	UpdateDataFromPage();

	if (m_bChanged)	{
		QByteArray arData = XmlUtil::ConvertRecordSet2ByteArray_Fast(m_recAccounts);
		SetSettingValue(EMAIL_IN_SETTINGS_ACCOUNTS, arData);
	}
	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

void Settings_Email_Out::on_previous_toolButton_clicked()
{
	if(m_nCurrentPage > 0){
		UpdateDataFromPage();
		m_nCurrentPage--;
		SetupCurrentPage();
	}
}

void Settings_Email_Out::on_next_toolButton_clicked()
{
	int nCount = m_recAccounts.getRowCount();
	if(nCount > 0){
		if(m_nCurrentPage < nCount-1){
			UpdateDataFromPage();
			m_nCurrentPage++;
			SetupCurrentPage();
		}
	}
}

void Settings_Email_Out::on_delete_toolButton_clicked()
{
	if(m_nCurrentPage >= 0 && m_nCurrentPage < m_recAccounts.getRowCount()){
		m_recAccounts.deleteRow(m_nCurrentPage);
		//calculate new row
		if(m_nCurrentPage >= m_recAccounts.getRowCount())
			m_nCurrentPage--;	
		SetupCurrentPage();
		m_bChanged = true;
	}
}

void Settings_Email_Out::on_add_toolButton_clicked()
{
	UpdateDataFromPage();
	if(m_recAccounts.getRowCount() < 1){
		//ensure that the recordset is defined	
		m_recAccounts.destroy();
		m_recAccounts.addColumn(QVariant::String, "POP3_EMAIL");
		m_recAccounts.addColumn(QVariant::String, "POP3_HOST");
		m_recAccounts.addColumn(QVariant::String, "POP3_PORT");
		m_recAccounts.addColumn(QVariant::String, "POP3_USER");
		m_recAccounts.addColumn(QVariant::String, "POP3_PASS");
		m_recAccounts.addColumn(QVariant::Int, "POP3_USE_SSL");
		m_recAccounts.addColumn(QVariant::Int, "POP3_KNOWN_CONTACT_ONLY");
		m_recAccounts.addColumn(QVariant::Int, "POP3_CHECK_EVERY_MIN");
		m_recAccounts.addColumn(QVariant::DateTime, "POP3_LAST_CHECK");
		m_recAccounts.addColumn(QVariant::DateTime, "POP3_LAST_MESSAGE");
		m_recAccounts.addColumn(QVariant::Int, "POP3_SERVER_TYPE");
		m_recAccounts.addColumn(QVariant::Int, "POP3_ACCOUNT_ACTIVE");
		m_recAccounts.addColumn(QVariant::Int, "POP3_DEL_EMAIL_AFTER_DAYS");
		m_recAccounts.addColumn(QVariant::Int, "POP3_FULL_SCAN_MODE");	//should POP3 message scanner scan linearly or abort at first message UID that exists in teh database
		m_recAccounts.addColumn(QVariant::String, "POP3_ACC_UID");	//unique ID for this account (needs to be unique only locally within a set of accounts for a single person)
		m_recAccounts.addColumn(QVariant::String, "ERROR_EMAIL_UID");	//UID that failed last time we did import
		m_recAccounts.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");	//log with recent issues with this account
	}
	m_recAccounts.addRow();
	m_nCurrentPage = m_recAccounts.getRowCount()-1;

	//set defaults here
	m_recAccounts.setData(m_nCurrentPage, "POP3_EMAIL", "");
	m_recAccounts.setData(m_nCurrentPage, "POP3_HOST", "");
	m_recAccounts.setData(m_nCurrentPage, "POP3_PORT", "110");
	m_recAccounts.setData(m_nCurrentPage, "POP3_USER", "");
	m_recAccounts.setData(m_nCurrentPage, "POP3_PASS", "");
	m_recAccounts.setData(m_nCurrentPage, "POP3_USE_SSL", 1);
	m_recAccounts.setData(m_nCurrentPage, "POP3_KNOWN_CONTACT_ONLY", 0);
	m_recAccounts.setData(m_nCurrentPage, "POP3_CHECK_EVERY_MIN", 12);
	m_recAccounts.setData(m_nCurrentPage, "POP3_LAST_CHECK", QDateTime::currentDateTime());
	m_recAccounts.setData(m_nCurrentPage, "POP3_LAST_MESSAGE", QDateTime::currentDateTime());
	m_recAccounts.setData(m_nCurrentPage, "POP3_SERVER_TYPE", EML_TYPE_POP3);
	
	//B.T.: check if exists new flds to avoid crash:
	//------------------------------------------------------------------
	if (m_recAccounts.getColumnIdx("POP3_ACCOUNT_ACTIVE")<0)
		m_recAccounts.addColumn(QVariant::Int, "POP3_ACCOUNT_ACTIVE");
	if (m_recAccounts.getColumnIdx("POP3_DEL_EMAIL_AFTER_DAYS")<0)
		m_recAccounts.addColumn(QVariant::Int, "POP3_DEL_EMAIL_AFTER_DAYS");
	if (m_recAccounts.getColumnIdx("POP3_FULL_SCAN_MODE")<0)
		m_recAccounts.addColumn(QVariant::Int, "POP3_FULL_SCAN_MODE");
	if (m_recAccounts.getColumnIdx("POP3_ACC_UID")<0)
		m_recAccounts.addColumn(QVariant::String, "POP3_ACC_UID");
	if (m_recAccounts.getColumnIdx("ERROR_EMAIL_UID")<0)
		m_recAccounts.addColumn(QVariant::String, "ERROR_EMAIL_UID");
	if (m_recAccounts.getColumnIdx("ACC_ERROR_MESSAGES")<0)
		m_recAccounts.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");
	//------------------------------------------------------------------
	
	m_recAccounts.setData(m_nCurrentPage, "POP3_ACCOUNT_ACTIVE", 1);
	m_recAccounts.setData(m_nCurrentPage, "POP3_DEL_EMAIL_AFTER_DAYS", 0);
	m_recAccounts.setData(m_nCurrentPage, "POP3_FULL_SCAN_MODE", 1);			//should POP3 message scanner scan linearly or abort at first message UID that exists in teh database
	m_recAccounts.setData(m_nCurrentPage, "POP3_ACC_UID", GenerateAccUID());	//unique ID for this account (needs to be unique only locally within a set of accounts for a single person)

	//init GUI
	SetupCurrentPage();

	m_bChanged = true;
}

void Settings_Email_Out::SetupCurrentPage()
{
	int nAccCount = m_recAccounts.getRowCount();
	if(m_nCurrentPage >= 0 && m_nCurrentPage < nAccCount)
	{
		ui.lineEmail->setText(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_EMAIL").toString());
		ui.txtEmail->setText(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_EMAIL").toString());
		ui.txtPop3Host->setText(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_HOST").toString());
		ui.txtPop3Port->setText(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_PORT").toString());
		ui.txtPop3User->setText(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_USER").toString());
		ui.txtPop3Pass->setText(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_PASS").toString());
		ui.cboSecurity->setCurrentIndex(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_USE_SSL").toInt()-1);

		//these settings were added afterwards
		if(m_recAccounts.getColumnIdx("POP3_KNOWN_CONTACT_ONLY")>=0){
			ui.chkKnownSenderOnly->setChecked(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_KNOWN_CONTACT_ONLY").toInt() == 1 ? true : false);
			ui.txtCheckMin->setText(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_CHECK_EVERY_MIN").toString());
		}
		else{
			m_recAccounts.addColumn(QVariant::Int, "POP3_KNOWN_CONTACT_ONLY");
			m_recAccounts.addColumn(QVariant::Int, "POP3_CHECK_EVERY_MIN");
			m_recAccounts.addColumn(QVariant::DateTime, "POP3_LAST_CHECK");
			m_recAccounts.setColValue("POP3_KNOWN_CONTACT_ONLY", 0);
			m_recAccounts.setColValue("POP3_CHECK_EVERY_MIN", 10);
			m_recAccounts.setColValue("POP3_LAST_CHECK", QDateTime::currentDateTime());
		}

		if(m_recAccounts.getColumnIdx("POP3_SERVER_TYPE")>=0){
			if(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_SERVER_TYPE").toInt() == EML_TYPE_POP3)
				ui.radioPOP3->setChecked(true);
			else
				ui.radioIMAP->setChecked(true);
		}
		else{
			m_recAccounts.addColumn(QVariant::Int, "POP3_SERVER_TYPE");
			m_recAccounts.setColValue("POP3_SERVER_TYPE", EML_TYPE_POP3);
			ui.radioPOP3->setChecked(true);
		}

		//add newer field if needed
		if(m_recAccounts.getColumnIdx("POP3_LAST_MESSAGE")<0){
			m_recAccounts.addColumn(QVariant::DateTime, "POP3_LAST_MESSAGE");
		}
		else{
			ui.dtEmailsAfter->setDateTime(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_LAST_MESSAGE").toDateTime());
		}
		if(m_recAccounts.getColumnIdx("POP3_ACCOUNT_ACTIVE")<0){
			m_recAccounts.addColumn(QVariant::Int, "POP3_ACCOUNT_ACTIVE");
			m_recAccounts.setColValue("POP3_ACCOUNT_ACTIVE", 1);
		}
		ui.chkIsActive->setChecked(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_ACCOUNT_ACTIVE").toInt() > 0);

		if(m_recAccounts.getColumnIdx("POP3_DEL_EMAIL_AFTER_DAYS")<0){
			m_recAccounts.addColumn(QVariant::Int, "POP3_DEL_EMAIL_AFTER_DAYS");
			m_recAccounts.setColValue("POP3_DEL_EMAIL_AFTER_DAYS", 0);
		}
		ui.txtDelEmailAfterDays->setText(m_recAccounts.getDataRef(m_nCurrentPage, "POP3_DEL_EMAIL_AFTER_DAYS").toString());

		ui.txtCurAccPos->setText(QString("(%1/%2)").arg(m_nCurrentPage+1).arg(nAccCount));
	}
	else{
		//clear form
		ui.lineEmail->setText("");
		ui.txtEmail->setText("");
		ui.txtPop3Host->setText("");
		ui.txtPop3Port->setText("");
		ui.txtPop3User->setText("");
		ui.txtPop3Pass->setText("");
		ui.cboSecurity->setCurrentIndex(0);
		ui.chkKnownSenderOnly->setChecked(false);
		ui.txtCheckMin->setText("12");
		ui.radioPOP3->setChecked(true);
		ui.dtEmailsAfter->setDateTime(QDateTime());
		ui.chkIsActive->setChecked(false);
		ui.txtCurAccPos->setText("");
		ui.cboProviders->setCurrentIndex(0);
		ui.txtDelEmailAfterDays->setText("0");
	}

	UpdateWidgetStates();
}

void Settings_Email_Out::UpdateDataFromPage()
{
	if(m_nCurrentPage >= 0 && m_nCurrentPage < m_recAccounts.getRowCount())
	{
		bool bEmailDateChanged = ui.dtEmailsAfter->dateTime() != m_recAccounts.getDataRef(m_nCurrentPage, "POP3_LAST_MESSAGE").toDateTime();

		if( ui.txtEmail->text()		!= m_recAccounts.getDataRef(m_nCurrentPage, "POP3_EMAIL").toString() ||
			ui.txtPop3Host->text()	!= m_recAccounts.getDataRef(m_nCurrentPage, "POP3_HOST").toString() ||
			ui.txtPop3Port->text()	!= m_recAccounts.getDataRef(m_nCurrentPage, "POP3_PORT").toString() ||
			ui.txtPop3User->text()	!= m_recAccounts.getDataRef(m_nCurrentPage, "POP3_USER").toString() ||
			ui.txtPop3Pass->text()	!= m_recAccounts.getDataRef(m_nCurrentPage, "POP3_PASS").toString() ||
			ui.cboSecurity->currentIndex() != (m_recAccounts.getDataRef(m_nCurrentPage, "POP3_USE_SSL").toInt()-1) ||
			ui.chkKnownSenderOnly->isChecked() != ((m_recAccounts.getDataRef(m_nCurrentPage, "POP3_KNOWN_CONTACT_ONLY").toInt() == 1) ? true : false) ||
			ui.txtCheckMin->text()	!= m_recAccounts.getDataRef(m_nCurrentPage, "POP3_CHECK_EVERY_MIN").toString() ||
			bEmailDateChanged ||
			ui.radioPOP3->isChecked() != ((m_recAccounts.getDataRef(m_nCurrentPage, "POP3_SERVER_TYPE").toInt() == EML_TYPE_POP3) ? true : false) ||
			ui.chkIsActive->isChecked()  != ((m_recAccounts.getDataRef(m_nCurrentPage, "POP3_ACCOUNT_ACTIVE").toInt() == 1) ? true : false) ||
			ui.txtDelEmailAfterDays->text() != m_recAccounts.getDataRef(m_nCurrentPage, "POP3_DEL_EMAIL_AFTER_DAYS").toString())
		{
			m_bChanged = true;
		}

		//B.T.: check if exists new flds to avoid crash:
		//------------------------------------------------------------------
		if (m_recAccounts.getColumnIdx("POP3_ACCOUNT_ACTIVE")<0)
			m_recAccounts.addColumn(QVariant::Int, "POP3_ACCOUNT_ACTIVE");
		if (m_recAccounts.getColumnIdx("POP3_DEL_EMAIL_AFTER_DAYS")<0)
			m_recAccounts.addColumn(QVariant::Int, "POP3_DEL_EMAIL_AFTER_DAYS");
		if (m_recAccounts.getColumnIdx("POP3_FULL_SCAN_MODE")<0)
			m_recAccounts.addColumn(QVariant::Int, "POP3_FULL_SCAN_MODE");
		if (m_recAccounts.getColumnIdx("POP3_ACC_UID")<0)
			m_recAccounts.addColumn(QVariant::String, "POP3_ACC_UID");
		if (m_recAccounts.getColumnIdx("ERROR_EMAIL_UID")<0)
			m_recAccounts.addColumn(QVariant::String, "ERROR_EMAIL_UID");
		if (m_recAccounts.getColumnIdx("ACC_ERROR_MESSAGES")<0)
			m_recAccounts.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");
		//------------------------------------------------------------------

		m_recAccounts.setData(m_nCurrentPage, "POP3_EMAIL", ui.txtEmail->text());
		m_recAccounts.setData(m_nCurrentPage, "POP3_HOST", ui.txtPop3Host->text());
		m_recAccounts.setData(m_nCurrentPage, "POP3_PORT", ui.txtPop3Port->text());
		m_recAccounts.setData(m_nCurrentPage, "POP3_USER", ui.txtPop3User->text());
		m_recAccounts.setData(m_nCurrentPage, "POP3_PASS", ui.txtPop3Pass->text());
		m_recAccounts.setData(m_nCurrentPage, "POP3_USE_SSL", ui.cboSecurity->currentIndex()+1);
		m_recAccounts.setData(m_nCurrentPage, "POP3_KNOWN_CONTACT_ONLY", ui.chkKnownSenderOnly->isChecked()? 1:0);
		m_recAccounts.setData(m_nCurrentPage, "POP3_CHECK_EVERY_MIN", ui.txtCheckMin->text().toInt());
		m_recAccounts.setData(m_nCurrentPage, "POP3_LAST_MESSAGE", ui.dtEmailsAfter->dateTime());
		m_recAccounts.setData(m_nCurrentPage, "POP3_SERVER_TYPE", ui.radioPOP3->isChecked() ? EML_TYPE_POP3 : EML_TYPE_IMAP);
		m_recAccounts.setData(m_nCurrentPage, "POP3_ACCOUNT_ACTIVE", ui.chkIsActive->isChecked() ? 1 : 0);
		m_recAccounts.setData(m_nCurrentPage, "POP3_DEL_EMAIL_AFTER_DAYS", ui.txtDelEmailAfterDays->text().toInt());

		//this can be PITA when trying to debug ASAP
#ifndef _DEBUG
		//minimal time check is 2 minutes
		bool bGoogleMail = ui.txtEmail->text().endsWith("google.com") || ui.txtEmail->text().endsWith("gmail.com");
		if(bGoogleMail && ui.txtCheckMin->text().toInt() < 12){
			m_recAccounts.setData(m_nCurrentPage, "POP3_CHECK_EVERY_MIN", 12);
			ui.txtCheckMin->setText(QString("%1").arg(12));
			QMessageBox::information(NULL, "", tr("Check time for Google account was too low!\nNow increased to the minimal value (12 min)."));
		}
		else if(ui.txtCheckMin->text().toInt() < 2){
			m_recAccounts.setData(m_nCurrentPage, "POP3_CHECK_EVERY_MIN", 2);
			ui.txtCheckMin->setText(QString("%1").arg(2));
			QMessageBox::information(NULL, "", tr("Check time was too low!\nNow increased to the minimal value (2 min)."));
		}
#endif

		if(bEmailDateChanged){
			//must reset this flag when date changes, so that the importer gets a chance to import old emails (possibly placed before already imported ones)
			m_recAccounts.setData(m_nCurrentPage, "POP3_FULL_SCAN_MODE", 1);	//should POP3 message scanner scan linearly or abort at first message UID that exists in teh database
		}
	}
}

void Settings_Email_Out::UpdateWidgetStates()
{
	bool bValidAccIdx = (m_nCurrentPage >= 0 && m_nCurrentPage < m_recAccounts.getRowCount());
	bool bEnable = bValidAccIdx && ui.chkIsActive->isChecked();

	ui.chkIsActive->setEnabled(bValidAccIdx);

	ui.txtEmail->setEnabled(bEnable);
	ui.txtPop3Host->setEnabled(bEnable);
	ui.txtPop3Port->setEnabled(bEnable);
	ui.txtPop3User->setEnabled(bEnable);
	ui.txtPop3Pass->setEnabled(bEnable);
	ui.radioPOP3->setEnabled(bEnable);
	ui.radioIMAP->setEnabled(bEnable);
	ui.cboSecurity->setEnabled(bEnable);
	ui.chkKnownSenderOnly->setEnabled(bEnable);
	ui.txtCheckMin->setEnabled(bEnable);
	ui.dtEmailsAfter->setEnabled(bEnable);
	ui.txtDelEmailAfterDays->setEnabled(bEnable);
	ui.cboProviders->setEnabled(bEnable);
	ui.btnTestConnection->setEnabled(bEnable);

	//command widgets
	ui.previous_toolButton->setDisabled(0 == m_nCurrentPage);
	ui.next_toolButton->setDisabled(m_nCurrentPage >= m_recAccounts.getRowCount()-1);
	ui.delete_toolButton->setDisabled(m_nCurrentPage >= m_recAccounts.getRowCount());
}

void Settings_Email_Out::on_cboProviders_currentIndexChanged(int index)
{
	if(index > 0)	//not "OTHER"
	{
		ui.txtPop3Host->setText(m_lstProviders[index-1].m_strServer);
		ui.txtPop3Port->setText(QString("%1").arg(m_lstProviders[index-1].m_nPort));
		ui.radioPOP3->setChecked(EML_TYPE_POP3 == m_lstProviders[index-1].m_nType);
		ui.cboSecurity->setCurrentIndex(m_lstProviders[index-1].m_nSecurity-1);
	}
}

//on protocol toggle, set default port values based on security
void Settings_Email_Out::on_radioPOP3_toggled(bool bOn)
{
	int nSecurity = ui.cboSecurity->currentIndex()+1;
	int nPort=-1;
	if(bOn){
		//set default port for POP3
		if(EML_SECURITY_NONE == nSecurity)
			nPort = EML_DEFAULT_PORT_POP3;
		else
			nPort = EML_DEFAULT_PORT_POP3_SSL;
	}
	else{
		//set default port for IMAP
		if(EML_SECURITY_NONE == nSecurity)
			nPort = EML_DEFAULT_PORT_IMAP;
		else
			nPort = EML_DEFAULT_PORT_IMAP_SSL;
	}
	ui.txtPop3Port->setText(QString("%1").arg(nPort));
}

//on security changed, set default port based on protocol type
void Settings_Email_Out::on_cboSecurity_currentIndexChanged(int index)
{
	bool bPOP3 = ui.radioPOP3->isChecked();
	int nSecurity = index+1;
	int nPort=-1;
	if(bPOP3){
		//set default port for POP3
		if(EML_SECURITY_NONE == nSecurity)
			nPort = EML_DEFAULT_PORT_POP3;
		else
			nPort = EML_DEFAULT_PORT_POP3_SSL;
	}
	else{
		//set default port for IMAP
		if(EML_SECURITY_NONE == nSecurity)
			nPort = EML_DEFAULT_PORT_IMAP;
		else
			nPort = EML_DEFAULT_PORT_IMAP_SSL;
	}
	ui.txtPop3Port->setText(QString("%1").arg(nPort));
}

//when typed port matches some default port, adjust security type
void Settings_Email_Out::on_txtPop3Port_textChanged( const QString & )
{
	int nPort = ui.txtPop3Port->text().toInt();
	if( EML_DEFAULT_PORT_POP3 == nPort ||
		EML_DEFAULT_PORT_IMAP == nPort)
	{
		ui.cboSecurity->setCurrentIndex(EML_SECURITY_NONE-1);
	}
	else if(EML_DEFAULT_PORT_POP3_SSL == nPort ||
			EML_DEFAULT_PORT_IMAP_SSL == nPort)
	{
		ui.cboSecurity->setCurrentIndex(EML_SECURITY_SSLTLS-1);	
	}
}

//update user name from email (they are equal by default)
void Settings_Email_Out::on_txtEmail_textChanged( const QString & )
{
	ui.txtPop3User->setText(ui.txtEmail->text());
}

void Settings_Email_Out::on_btnTestConnection_clicked()
{
	//test connection
	bool bPOP3 = ui.radioPOP3->isChecked();
	int nSecurity = ui.cboSecurity->currentIndex()+1;
	int nPort = ui.txtPop3Port->text().toInt();
	QString strHost = ui.txtPop3Host->text();
	QString strUser = ui.txtPop3User->text();
	QString strPass = ui.txtPop3Pass->text();
	
	QString strError;
	if(!TestConnection(bPOP3, strHost, nPort, nSecurity, strUser, strPass, strError))
		QMessageBox::warning(NULL, "", tr("Connection failed: %1!").arg(strError));
	else
		QMessageBox::information(NULL, "", tr("Connection success!"));
}

int verify_callback2(X509_STORE_CTX *,void *)
{
	return 1;	//do not terminate
}

class myTraceOutput : public mailpp::TraceOutput
{
public:
	virtual void Send (const char *buffer, unsigned length)
	{
#ifdef _DEBUG
		qDebug() << "POP3 >" << buffer;
#endif
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "SMTP > " + QString(buffer));
	};

	virtual void Receive (const char *buffer, unsigned length)
	{
#ifdef _DEBUG
		qDebug() << "POP3 <" << buffer;
#endif
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "SMTP < " + QString(buffer));
	};
};

bool Settings_Email_Out::TestConnection(bool bPOP3, const QString &strHost, int nPort, int nSecurity, const QString &strUser, const QString &strPass, QString &strError)
{
	bool bUseSTLS = (nSecurity == EML_SECURITY_STARTTLS);
	bool bUseSSL = bUseSTLS || (nSecurity == EML_SECURITY_SSLTLS);
	bool bWait    = !bUseSSL || bUseSTLS;
	
	if(bPOP3)
	{
		//init POP3 client
		myTraceOutput tracer;
		mailpp::Pop3Client client;
		client.SetTraceOutput(&tracer, false);

		if(client.Connect(strHost.toLocal8Bit().constData(), nPort, bWait) < 0){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to connect to POP3 host.");
			strError = tr("Failed to connect to POP3 host");
			return false;
		}
		//connected, check the reply code as well
		if(bWait && mailpp::Pop3Client::OK != client.ReplyCode()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
			strError = tr("POP3 error: ") + client.ErrorMessage();
			return false;
		}

		//initiate SSL if needed
		if(bUseSTLS)
		{
			if(client.Stls() < 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to arrange TLS connection to POP3 host.");
				strError = tr("Failed to arrange TLS connection");
				return false;
			}
			if(mailpp::Pop3Client::OK != client.ReplyCode()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
				strError = tr("POP3 error: ") + client.ErrorMessage();
				client.Quit();
				client.Disconnect();
				return false;
			}
		}
		if(bUseSSL)
		{
			//ignore error about hostname mismatch within the certificate
		#if 1
			SSL_CTX	*ctx = SSL_CTX_new(SSLv23_client_method());	//TLSv1_client_method
			SSL *ssl = NULL;
			if(ctx){
				SSL_CTX_set_options(ctx, SSL_OP_ALL);
				SSL_CTX_set_cert_verify_callback(ctx, verify_callback2, NULL);
				ssl = SSL_new(ctx);
			}
			else{
				char szBuf[1000];
				ERR_error_string(ERR_get_error(), szBuf);
				strError = tr("POP3 error: ") + szBuf;
				client.Disconnect();
				return false;
			}
			if( client.ConnectTlsOpenSSL(strHost.toLocal8Bit().constData(), ssl, true) < 0 &&
				mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
		#else
			if( client.ConnectTls(strHost.toLocal8Bit().constData(), true) < 0 &&
				mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
		#endif
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Failed to establish TLS connection to POP3 host (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));
				strError = tr("Failed to establish TLS connection: ") + client.ErrorMessage();

				char szBuf[1000];
				ERR_error_string(ERR_get_error(), szBuf);
				qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";

				client.Disconnect();
				return false;
			}
			if(mailpp::Pop3Client::OK != client.ReplyCode()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
				strError = tr("POP3 error: ") + client.ErrorMessage();
				client.Quit();
				client.Disconnect();
				return false;
			}
		}

		//login
		if(client.User(strUser.toLocal8Bit().constData()) < 0){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to send POP3 command (USER).");
			strError = tr("Failed sending USER command");
			client.Disconnect();
			return false;
		}
		if(mailpp::Pop3Client::OK != client.ReplyCode()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
			strError = tr("Invalid user name or password");
			client.Quit();
			client.Disconnect();
			return false;
		}

		if(client.Pass(strPass.toLocal8Bit().constData()) < 0){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to send POP3 command (PASS).");
			strError = tr("Failed sending PASS command");
			client.Disconnect();
			return false;
		}
		if(mailpp::Pop3Client::OK != client.ReplyCode()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
			strError = tr("Invalid user name or password");
			client.Quit();
			client.Disconnect();
			return false;
		}

		//success
		client.Quit();
		client.Disconnect();
		return true;
	}
	else
	{
		//IMAP testing
		// Create an IMAP4 client
		mailpp::Imap4Client client;
		mailpp::StdTraceOutput traceOut;
		client.SetTraceOutput(&traceOut, false);

		// Open connection to the server
		if(client.Connect(strHost.toLocal8Bit().constData(), nPort, bWait) < 0){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to connect to IMAP host.");
			strError = tr("Failed to connect to IMAP host");
			return false;
		}

		//initiate SSL if needed
		if(bUseSTLS)
		{
			if(client.Starttls() < 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,"Failed to arrange TLS connection to IMAP host.");
				strError = tr("Failed to arrange TLS connection to IMAP host");
				client.Disconnect();
				return false;
			}
			if(mailpp::Imap4Client::OK != client.CommandStatus()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("IMAP error: %1").arg(client.ErrorMessage()));
				strError = tr("IMAP error: ") + client.ErrorMessage();
				client.Logout();
				client.Disconnect();
				return false;
			}
		}
		if(bUseSSL)
		{
			//ignore error about hostname mismatch within the certificate
		#if 1
			SSL_CTX	*ctx = SSL_CTX_new(SSLv23_client_method());	//TLSv1_client_method
			SSL *ssl = NULL;
			if(ctx){
				SSL_CTX_set_options(ctx, SSL_OP_ALL);
				SSL_CTX_set_cert_verify_callback(ctx, verify_callback2, NULL);
				ssl = SSL_new(ctx);
			}
			else{
				char szBuf[1000];
				ERR_error_string(ERR_get_error(), szBuf);
				strError = tr("TLS error: ") + szBuf;
				client.Disconnect();
				return false;
			}
			if( client.ConnectTlsOpenSSL(strHost.toLocal8Bit().constData(), ssl, true) < 0 &&
				mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
		#else
			if( client.ConnectTls(strPOP3.toLocal8Bit().constData(), true) < 0 &&
				mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
		#endif
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Failed to establish TLS connection to IMAP host (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));

				char szBuf[1000];
				ERR_error_string(ERR_get_error(), szBuf);
				qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";

				strError = tr("Failed to establish TLS connection to IMAP host: ") + client.ErrorMessage();
				client.Disconnect();
				return false;
			}
		}

		// Send LOGIN command
		if( client.Login(strUser.toLocal8Bit().constData(), strPass.toLocal8Bit().constData()) < 0)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Login error (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));
			strError = tr("Login error: ") + client.ErrorMessage();
			client.Disconnect();
			return false;
		}
		if(mailpp::Imap4Client::OK != client.CommandStatus()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Login error (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));
			strError = tr("Invalid user name or password");
			client.Logout();
			client.Disconnect();
			return false;
		}

		//success
		client.Logout();
		client.Disconnect();
		return true;
	}
}

void Settings_Email_Out::on_chkIsActive_clicked(bool bChecked)
{
	UpdateWidgetStates();
}

QString GenerateAccUID()
{
	char szBuffer[1024];
	//3rd field to write - random number block (16 bytes)
	OpenSSLHelper::Extern_RAND_bytes((unsigned char *)szBuffer, 16);		//generate fresh 8 random bytes
	return QString(QByteArray::fromRawData(szBuffer, 16).toBase64());
}
