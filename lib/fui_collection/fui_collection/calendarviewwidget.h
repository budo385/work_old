#ifndef CALENDARVIEWWIDGET_H
#define CALENDARVIEWWIDGET_H

#include <QtWidgets/QFrame>
#include <QTreeWidgetItem>
#include <QList>

#include "generatedfiles/ui_calendarviewwidget.h"

/*
	Modus Operandi:
		- on calendar load call GetData to filter data and set range min
		- on every data from/to change call SetDateRange
		- catch all signals, see desc for each
*/

class CalendarViewWidget : public QFrame
{
	Q_OBJECT

public:
	CalendarViewWidget(QWidget *parent = 0);
	~CalendarViewWidget();

	void SetDateRange(QDate from, QDate to);
	void GetData(int &nRangeMinute, DbRecordSet &recCheckedItems, bool &bFilterByType, bool &bShowPreliminary, bool &bShowInformation, bool &bFilterByNoType);
	void SetData(int nRangeMinute, DbRecordSet recCheckedItems, bool bFilterByType, bool bShowPreliminary, bool bShowInformation, bool bFilterByNoType, QDate from, QDate to);
	void setSlider(int nSliderMinute);

signals:
	void DateChanged(QDate date /* current date changed: move from to into this week */);
	void RangeChanged(int nValue /* in minutes: from 60min to 5min */);
	void FilterTypeChanged(QList<int> /* list of id's of current type CET_TYPES by which u filter events, if empty, then filter is off */);
	void FilterByNoType(bool);
	void ShowInformationEvents(bool /* if 1 show only events with BCEV_IS_INFORMATION=1*/);
	void ShowPreliminaryEvents(bool /* if 1 show only events with BCEV_IS_PRELIMINARY=1*/);

private slots:
	void OnCalendarSelectionChanged();
	void OnSliderValueChanged(int);
	void OnTypeItemCheckStateChanged(QList<QTreeWidgetItem*>);
	void OnFilterByTypeChanged(int);
	void OnInformationChanged(int);
	void OnPreliminaryChanged(int);
	void OnFilterByNoType(int);


private:
	int GetSliderMinute(int nSliderValue);
	int SetSliderMinute(int nMinute);

	Ui::CalendarViewWidgetClass ui;

};

#endif // CALENDARVIEWWIDGET_H
