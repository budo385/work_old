//#include "mapi_ex/Interface.h"
#include "emaildialog.h"
#include "bus_client/bus_client/emailhelper.h"
#include "bus_core/bus_core/emailhelpercore.h"
#include "bus_client/bus_client/documenthelper.h"
#include "bus_core/bus_core/commgridviewhelper.h"
#include "bus_core/bus_core/globalconstants.h"
#include "gui_core/gui_core/richeditwidget.h"
#include "contactselection.h"
#include "common/common/datahelper.h"
#include "os_specific/os_specific/mapimanager.h"
#include "trans/trans/smtpconnectionsettings.h"
#include "os_specific/os_specific/mailmanager.h"
#include "bus_client/bus_client/changemanager.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "common/common/zipcompression.h"
#include "commgridview.h"
#include "bus_core/bus_core/accuarcore.h"
#include "gui_core/gui_core/scrollablemsgbox.h"
#include "fui_collection/fui_collection/fui_importemail.h"
//#ifndef min
//#define min(a,b) (((a)<(b))? (a):(b))
//#endif


#include "gui_core/gui_core/thememanager.h"

#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "communicationmanager.h"
extern CommunicationManager	g_CommManager;				
#include "fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt* g_pClientManager;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester



#include <QInputDialog>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFile>


typedef bool (*FN_Call_CreateEmailFromTemplate)(QString bodyTemplate, QString &NewBody, int nContactID, int nProjectID);
bool Call_CreateEmailFromTemplate(QString bodyTemplate, QString &NewBody, int nContactID, int nProjectID)
{
	return EmailHelper::CreateEmailFromTemplate(bodyTemplate,NewBody,nContactID,nProjectID);
}

EMailDialog::EMailDialog(QWidget *parent)
    : FuiBase(parent),m_bOpenFromTemplate(false)
{
	m_CID_ModifyEmailBodyInProgress=false;
	m_bSerialEmailMode = false;
	m_bSerialUseAllContactEmails = false;
	m_nProjectID = -1;
	ui.setupUi(this);
	m_PersonCache.Initialize(ENTITY_BUS_PERSON);
	m_PersonCache.ReloadData();
	m_ContactCache.Initialize(ENTITY_BUS_CONTACT);
	m_ContactCache.ReloadData();

	ui.TextEdit_widget->EnableHtmlFixing(true);

	ui.grupIsTemplate->setMaximumHeight(25); //B.T. to avoid flip-flop in layout when opening widget

	InitFui(ENTITY_BUS_EMAILS,BUS_EMAIL,NULL,ui.btnButtonBar); //FUI base
	EnableAccessRightCheck();
	EnableWindowCloseOnCancelOrOK();

	ui.btnButtonBar->RegisterFUI(this);
	ui.radExternalMail->setText(QString("Use External Mail (Outlook")+QChar(174)+QString(")"));

	//disable all not needed buttons as caller is responsible to set right mode: insert or edit
	//special: insert->setdefaults, edit->execute tasks
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_COPY)->setVisible(false);
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_INSERT)->setVisible(false);
	//ui.btnButtonBar->GetButton(ManCommandBar::BUTTON_EDIT)->setVisible(false);

	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_OK)->setText(tr("Save"));


	ui.frameTask->SetDefaultTaskType(GlobalConstants::TASK_TYPE_SCHEDULED_EMAIL);
	ui.frameTask->SetAlwaysExpanded();

	//Default email type is html (for now).
	//m_nEmailType = 1;


	connect(ui.btnAddAttachment,SIGNAL(clicked()),ui.listAttachemnts,SLOT(AddNewAttachment()));
	connect(ui.frameTask,SIGNAL(ScheduleTaskToggle()),this,SLOT(OnScheduleTaskToggle()));


	//----------------------------attach cntx menu---------------------------------


	//bring up field manager:
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData);
	m_GuiFieldManagerMain->RegisterField("BEM_FROM",ui.From_lineEdit);
	m_GuiFieldManagerMain->RegisterField("BEM_SUBJECT",ui.Subject_lineEdit);
	//m_GuiFieldManagerMain->RegisterField("CENT_IS_PRIVATE",ui.ckbTemplateIsPrivate);
	m_GuiFieldManagerMain->RegisterField("BEM_TEMPLATE_NAME",ui.txtTemplateName);
	m_GuiFieldManagerMain->RegisterField("BEM_RECV_TIME",ui.dateTimeEdit);
	m_GuiFieldManagerMain->RegisterField("BEM_TEMPLATE_DESCRIPTION",ui.txtTemplateDesc);
	//m_GuiFieldManagerMain->RegisterField("BEM_BODY",ui.TextEdit_widget->GetTextWidget());
	m_GuiFieldManagerMain->RegisterField("BEM_FIX_HTML",ui.chkFixHTML);

	ui.dateTimeEdit->setCalendarPopup(false);
	ui.dateTimeEdit->useAsDateTimeEdit();
	
	//SAPNE
	ui.frameProject->Initialize( ENTITY_BUS_PROJECT, &m_lstData,"BEM_PROJECT_ID");
	ui.frameType->Initialize( ENTITY_CE_TYPES,&m_lstData, "CENT_CE_TYPE_ID");
	ui.frameType->GetSelectionController()->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",GlobalConstants::CE_TYPE_EMAIL);

	ui.frameCategory->Initialize(ENTITY_EMAIL_TEMPLATE_CATEGORY,&m_lstData,"BEM_CATEGORY",false);

	//Create recordset definition.
	m_recToRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	m_recCCRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	m_recBCCRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	
	m_lstContactLink.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));

	//m_bIsExecuteMode=false;


	//Set default data (sender contact name and email - if it exists).
	//SetPersonData();
	connect( ui.TextEdit_widget->GetTextWidget(),SIGNAL(textChanged()),this,SLOT(OnBodyChanged()));
	m_bBodyChanged=false;


	//add second button:
	m_buttonSaveSend= new QPushButton(tr("Save && Send"));

	QFont font=m_buttonSaveSend->font();
	font.setPointSize(10);
	m_buttonSaveSend->setFont(font);
	QSize size(130,27);
	m_buttonSaveSend->setFixedSize(130,27);
	QSizePolicy policy(QSizePolicy::Preferred,QSizePolicy::Fixed);
	m_buttonSaveSend->setSizePolicy(policy);
	ui.btnButtonBar->GetButtonLayout()->insertWidget(5,m_buttonSaveSend);


	connect(m_buttonSaveSend,SIGNAL(clicked()),this,SLOT(OnSaveSend()));
	m_bSaveSendClicked=false;

	connect(ui.TextEdit_widget, SIGNAL(EditViewSwitched()), this, SLOT(OnEditViewSwitched()));

	ui.frameTask->m_pRecMain = &m_lstData;	//attach main record

	//set edit to false:
	SetMode(MODE_EMPTY);

	ui.labelOutlook->setVisible(false);	 //initial state

	//issue 1396:
	//ui.labelDocType->setText(QString(HTML_START_F1)+tr("Email")+HTML_END_F1);
	ui.labelDocType->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	ui.labelDocType->setAlignment(Qt::AlignLeft);
	ui.labelDocType->setText(tr("Email"));

	ui.labelIcon->setPixmap(QPixmap(":Email48Shadow.png"));
	ui.frame->setStyleSheet("QFrame#frame "+ThemeManager::GetSidebarChapter_Bkg());
	ui.label_2->setStyleSheet("QLabel {color:white}");
	ui.label_5->setStyleSheet("QLabel {color:white}");
	ui.arSelector->setStyleSheet("QLabel {color:white}"); //issue 1953

	ui.radDirectMail->setChecked(true);
	
#ifdef _WIN32
	if(MapiManager::LoadMapiDll())
		ui.radExternalMail->setChecked(true);
	else
		ui.radExternalMail->setEnabled(false);
#endif

	m_lstCIDInfo.addColumn(QVariant::String,  "CID");
	m_lstCIDInfo.addColumn(QVariant::String,  "Path");
	m_lstCIDInfo.addColumn(QVariant::Int,	  "IsTemporary");

	SetFUIStyle();
	//ui.Address_tableWidget->setStyleSheet(ThemeManager::GetTableViewBkg());

	connect(ui.TextEdit_widget,SIGNAL(on_VerticalOpenClose_clicked(bool)),this,SLOT(OnEditor_VerticalOpenClose_clicked(bool)));
	ui.TextEdit_widget->ShowVerticalStretchButton(true);

	//ui.AddContacts_pushButton->setVisible(false);

	//update tab widget check icon on task changes
	updateTaskTabCheckbox();
	connect(ui.frameTask, SIGNAL(ScheduleTaskToggle()),this,SLOT(updateTaskTabCheckbox()));
	updateTemplateTabCheckbox();
	connect(ui.grupIsTemplate, SIGNAL(clicked()),this,SLOT(updateTemplateTabCheckbox()));
}

EMailDialog::~EMailDialog()
{
	//QByteArray body=ui.TextEdit_widget->GetHtml().toLocal8Bit().constData();
	//CommunicationManager::SaveFileContent(body,"D:/mail_body_htmlcontrol.html");

	delete m_GuiFieldManagerMain;
}

QString EMailDialog::GetFUIName()
{
	//if (m_nMode==MODE_EDIT)
		return tr("Email Message");
	//else
	//	return tr("Send Email Message");
}


void EMailDialog::RefreshDisplay()
{
	ui.frameCategory->RefreshDisplay();
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frameType->RefreshDisplay();
	ui.frameProject->RefreshDisplay();
	ui.frameTask->SetGUIFromData();
	updateTaskTabCheckbox();
	
	//update template status:_
	if (m_lstData.getRowCount()>0)
	{
		//m_lstData.Dump();
		QString str = m_lstData.getDataRef(0,"BEM_BODY").toString();
		
		if(m_lstData.getDataRef(0,"BEM_TEMPLATE_FLAG").toInt()==1)
			ui.grupIsTemplate->setChecked(true);
		else
			ui.grupIsTemplate->setChecked(false);

		//note: this must be set before filling the HTML
		bool bFixateHTML = (m_lstData.getDataRef(0,"BEM_FIX_HTML").toInt() > 0);
		if(bFixateHTML){
			ui.chkFixHTML->setChecked(true);
			ui.TextEdit_widget->EnableHtmlFixing(false);	//do not fix HTML in "fix(ate) HTML" mode
			if(!ui.TextEdit_widget->IsHTMLDisplayed()){
				ui.TextEdit_widget->m_bEmitSignal = false;
				ui.TextEdit_widget->SwitchFromHTMLToNormalView();	//bad name, in this case it switches to HTML
				ui.TextEdit_widget->m_bEmitSignal = true;
			}
			on_chkFixHTML_clicked();

			QTimer::singleShot(0, this, SLOT(FixHtmlViewLater()));
		}
		else{
			ui.chkFixHTML->setChecked(false);
			ui.TextEdit_widget->EnableHtmlFixing(true);
			ui.chkFixHTML->hide();	//not visible when outside of "raw HTML" editor
		}
		qDebug() << "Email RTF widget switched to HTML view #1:" << ui.TextEdit_widget->IsHTMLDisplayed();

		ui.TextEdit_widget->GetTextWidget()->blockSignals(true);

		QString strBody = m_lstData.getDataRef(0,"BEM_BODY").toString();
		if(bFixateHTML || m_lstData.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
		{
			ui.TextEdit_widget->SetPlainText(strBody);
			if(bFixateHTML)
				ui.TextEdit_widget->m_bTextIsHTML = true;
		}
		else
			ui.TextEdit_widget->SetHtml(strBody);

		qDebug() << "Email RTF widget set HTML content #1:" << (m_lstData.getDataRef(0,"BEM_EMAIL_TYPE").toInt()!=GlobalConstants::EMAIL_TYPE_PLAIN_TEXT);

		QTextCursor cur=ui.TextEdit_widget->GetTextWidget()->textCursor();

		cur.movePosition(QTextCursor::Start,QTextCursor::MoveAnchor);
		ui.TextEdit_widget->GetTextWidget()->setTextCursor(cur);
		ui.TextEdit_widget->GetTextWidget()->ensureCursorVisible();

		ui.TextEdit_widget->GetTextWidget()->blockSignals(false);
	}
	else
	{
		ui.grupIsTemplate->setChecked(false);
		ui.TextEdit_widget->SetHtml("");

		qDebug() << "Email RTF widget set HTML content #2";
	}

	on_grupIsTemplate_clicked();
	on_chkPlainText_clicked();
	updateTemplateTabCheckbox();
}


void EMailDialog::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		m_GuiFieldManagerMain->SetEditMode(false);
		ui.TextEdit_widget->SetEditMode(false);
		ui.txtTemplateDesc->SetEditMode(false);
		ui.frameProject->SetEditMode(false);
		ui.frameType->SetEditMode(false);
		ui.frameTask->SetEnabled(false);
		ui.grupIsTemplate->setEnabled(false);
		ui.AddContacts_pushButton->setEnabled(false);
		ui.Address_tableWidget->SetEditMode(false);
		ui.btnAddAttachment->setEnabled(false);
		ui.listAttachemnts->SetEditMode(false);
		m_buttonSaveSend->setEnabled(false);
		ui.frameCategory->SetEditMode(false);
		ui.arSelector->SetEditMode(false);
	}
	else
	{
		m_GuiFieldManagerMain->SetEditMode(true);
		ui.TextEdit_widget->SetEditMode(true);

		//issue 2667: if edit mode disable change of subject and body: if not task and not template
		//check if columns exist to be sure
		if (m_lstData.getColumnIdx("CENT_TASK_ID")>0 && m_lstData.getColumnIdx("BEM_TEMPLATE_FLAG")>0 && m_lstData.getColumnIdx("BEM_IS_SYNC_IN_PROGRESS")>0)
		{
			if(m_lstData.getDataRef(0,"CENT_ID").toInt()>0) //ensure that it's really edit mode
			{
				if (m_lstData.getDataRef(0,"CENT_TASK_ID").toInt()==0 && m_lstData.getDataRef(0,"BEM_TEMPLATE_FLAG").toInt()==0 && m_lstData.getDataRef(0,"BEM_IS_SYNC_IN_PROGRESS").toInt()==0)
				{
					ui.TextEdit_widget->SetEditMode(false);
					ui.Subject_lineEdit->setDisabled(true);
				}
			}
		}
		
		ui.txtTemplateDesc->SetEditMode(true);
		ui.frameProject->SetEditMode(true);
		ui.frameType->SetEditMode(true);
		ui.frameTask->SetEnabled(true);
		ui.grupIsTemplate->setEnabled(true);
		ui.AddContacts_pushButton->setEnabled(true);
		ui.Address_tableWidget->SetEditMode(true);
		ui.btnAddAttachment->setEnabled(true);
		ui.listAttachemnts->SetEditMode(true);
		ui.frameCategory->SetEditMode(true);
		m_buttonSaveSend->setEnabled(true);
		ui.Subject_lineEdit->setFocus();
		ui.arSelector->SetEditMode(true);
	}

	if(!g_FunctionPoint.IsFPAvailable(FP_CONTACTS_FUI))
		ui.AddContacts_pushButton->setEnabled(false);

	ui.dateTimeEdit->setEnabled(false);

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();

}

/*
Opus Dei:
1) recContacts - 1st contact default mail is set to TO, others to BCC
2) if strEmail is set, it will be picked up from TO contact
3) project is is assign to SAPNE
4) template id: mail is read, if recontact>0, addresses are reset, else if reply id is set, addresses are fetched from ce_link from reply mail: swithc to<->from
5) if reply id is only set: load ce link, switch addresses
*/


/* Must be called right after OpenFui. If not call ClearData(), or the result will be unpredictable and you'll suffer the consequences. */
void EMailDialog::SetDefaults(DbRecordSet recContacts, QString strEmail, int nProjectID, int nTemplateEmailID, bool bScheduleTask,bool bSetAsTemplate, int nEmailReplyID,QStringList *lstZippedFiles,bool bForwardMail)
{
	m_CID_ModifyEmailBodyInProgress=true;
	DbRecordSet lstContactEmails;
	DbRecordSet recReplyEmail;
	DbRecordSet recReplyTask,recReplyAttachments;
	bool bLoadEmailAddresses=true;


	//determine one contact:
	int nContactID=-1;
	if (recContacts.getRowCount()>0)
	{
		//get contact & project from data:
		QString strCntID;
		if(recContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2")>=0)
			strCntID="BNMR_TABLE_KEY_ID_2";
		else if (recContacts.getColumnIdx("BCNT_ID")!=-1)
			strCntID="BCNT_ID";

		if (!strCntID.isEmpty())
			nContactID=recContacts.getDataRef(0,strCntID).toInt();
	}


	//Template: load from DB, copy+ add recContact fields
	if (nTemplateEmailID!=-1)
	{
		on_selectionChange(nTemplateEmailID);
		//_DUMP_FILE(m_lstData.getDataRef(0,"BEM_BODY").toByteArray());
		on_cmdCopy();
		//_DUMP_FILE(m_lstData.getDataRef(0,"BEM_BODY").toByteArray());
		if (m_nMode!=MODE_INSERT) on_cmdInsert(); //failed
		ClearTemplateFields();
		m_bOpenFromTemplate=true;
		//only if not reply/forward
		if (nEmailReplyID==-1) //issue 1875: use template && !m_bSerialEmailMode)
		{
			QString strNewBody;
			if(m_bSerialEmailMode)
			{
				//issue #2199
				//evaluate only project and user data brackets -> multiple contacts will be used so skip their evaluation
				EmailHelper::CreateEmailFromTemplate(m_lstData.getDataRef(0,"BEM_BODY").toString(),strNewBody,-1,nProjectID);
			}
			else
			{
				//evaluate email from template - fill brackets for contact info and other
				EmailHelper::CreateEmailFromTemplate(m_lstData.getDataRef(0,"BEM_BODY").toString(),strNewBody,nContactID,nProjectID);
			}
			m_lstData.getDataRef(0,"BEM_BODY")=strNewBody;
		}
	}
	else
	{
		on_cmdInsert();
	}


	//prepare mail (reply or forward, or only)
	if (nEmailReplyID!=-1)
	{
		PrepareReplyForwardMail(nEmailReplyID,!bForwardMail);
	}
	else if (recContacts.getRowCount()>0)
	{
		DbRecordSet recContactsWithoutEmail;
		PrepareMailToSendToContacts(recContacts, recContactsWithoutEmail, strEmail);

		//check if there were contacts with no emails defined
		if(recContactsWithoutEmail.getRowCount() > 0)
		{
			//_DUMP(recContactsWithoutEmail);
			QString strMsg = tr("The following contacts have no email address:");
			int nCount = recContactsWithoutEmail.getRowCount();
			for(int i=0; i<nCount; i++){
				strMsg += "\n\"";
				strMsg += recContactsWithoutEmail.getDataRef(i, "BCNT_NAME").toString();
				strMsg += "\"";
			}

			strMsg += "\n\n";
			strMsg += tr("Continue sending the serial email or abort?"); //

			/*
			QMessageBox msgBox;
			QPushButton *pBtnContinue = msgBox.addButton (tr("Continue"), QMessageBox::YesRole);
			QPushButton *pBtnAbort   = msgBox.addButton (tr("Abort"), QMessageBox::NoRole);
			msgBox.setText(strMsg);
			if (msgBox.clickedButton() == pBtnAbort)
			*/
			ScrollableMsgBox msgBox;
			msgBox.SetButtonInfo(0, tr("Continue"), QMessageBox::YesRole);
			msgBox.SetButtonInfo(1, tr("Abort"), QMessageBox::NoRole);
			msgBox.SetMessage(strMsg);
			msgBox.setWindowTitle(tr("Warning"));
			int nRes = msgBox.exec();
			if (nRes != QMessageBox::YesRole) 
			{
				g_objFuiManager.CloseFUI(g_objFuiManager.GetFuiID(this));
				return;
			}
		}
	}
	
	//TOFIX:
	//clear duplicates:
	//m_recToRecordSet.removeDuplicates(m_recToRecordSet.getColumnIdx("CONTACT_ID"));
	//m_recBCCRecordSet.removeDuplicates(m_recToRecordSet.getColumnIdx("CONTACT_ID"));
	//m_recCCRecordSet.removeDuplicates(m_recToRecordSet.getColumnIdx("CONTACT_ID"));
	//m_recToRecordSet.Dump();
	ui.Address_tableWidget->ClearAll();

	ui.Address_tableWidget->AppendContacts(m_recToRecordSet, m_recCCRecordSet, m_recBCCRecordSet);

	//Project:
	if (nProjectID > 0)
	{
		ui.frameProject->SetCurrentEntityRecord(nProjectID);
	}

	//if sender is empty then set it to logged user...
	if (m_lstData.getDataRef(0,"BEM_FROM").toString().isEmpty())
	{
		SetPersonData();
	}

	//set as template:
	if (bSetAsTemplate)
	{
		m_lstData.setData(0,"BEM_TEMPLATE_FLAG",1);
	}

	//if schedule task:
	if (bScheduleTask)
	{
		ui.frameTask->m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE", 1);	 //his active
	}

	//always outgoing:
	m_lstData.setData(0,"BEM_OUTGOING",1);


	//once again refresh:
	EmailHelper::UnpackCIDToTemp(m_lstCIDInfo,m_lstData,m_lstAttachments);
	ui.listAttachemnts->SetData(m_lstAttachments);
	ui.listAttachemnts->AppendFiles(lstZippedFiles);

	//_DUMP(m_lstData);
	//_DUMP(m_lstCIDInfo);
	//_DUMP(m_lstAttachments);

	//MB: email with image inside should not be forced to be plain text
	//if(ui.listAttachemnts->IsCIDAttachmentExists())
	//	ui.chkPlainText->setCheckState(Qt::Checked);

	RefreshDisplay();
	m_CID_ModifyEmailBodyInProgress=false;
	
	ui.Subject_lineEdit->setFocus();

	//should we disable switching to RTF editor (allowing only editing of raw HTML)
	if(m_lstData.getDataRef(0,"BEM_FIX_HTML").toInt() > 0){
		if(!ui.TextEdit_widget->IsHTMLDisplayed()){
			ui.TextEdit_widget->m_bEmitSignal = false;
			ui.TextEdit_widget->SwitchFromHTMLToNormalView();	//bad name, in this case it switches to HTML
			ui.TextEdit_widget->m_bEmitSignal = true;
		}
		on_chkFixHTML_clicked();
	}
	else
		ui.chkFixHTML->hide();	//not visible when outside of "raw HTML" editor
	qDebug() << "Email RTF widget switched to HTML view #2:" << ui.TextEdit_widget->IsHTMLDisplayed();
}

bool EMailDialog::SendMail(DbRecordSet &recError)
{
#ifdef _DEBUG
	qDebug() << "Send mail TO";
	m_recToRecordSet.Dump();
	qDebug() << "CC";
	m_recCCRecordSet.Dump();
	qDebug() << "BCC";
	m_recBCCRecordSet.Dump();
	qDebug() << "(Serial mode) CC copy:" << g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_CC).toString();
	qDebug() << "(Serial mode) BCC copy:" << g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_BCC).toString();
#endif

	bool bOK=false;
	bool bExternalClient = ui.radExternalMail->isChecked();
	if(bExternalClient)
	{
		//try Outlook MAPI
		bOK=OutlookSendMailByDLL();
		if (!bOK)
		{
			return MailManager::SendMailDefaultClient(m_lstData,m_recToRecordSet,m_recCCRecordSet,m_recBCCRecordSet);
		}
	}
	else
	{
		SMTPConnectionSettings conn;
		conn.m_strHost				= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_NAME).toString();
		conn.m_nPort				= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_PORT).toInt();
		conn.m_strUserName			= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USER_NAME).toString();
		conn.m_strUserEmail			= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USER_EMAIL).toString();
		conn.m_strAccountName		= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_ACC_NAME).toString();
		conn.m_strAccountPassword	= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_ACC_PASS).toString();

		//#define EML_SECURITY_NONE	  1
		//#define EML_SECURITY_STARTTLS 2
		//#define EML_SECURITY_SSLTLS	  3
		int nSSLType = g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USE_SSL).toInt();
		if(nSSLType > 1){
			conn.m_bUseSSL = true;
			if(2 == nSSLType)
				conn.m_bUseSTLS = true;
		}

		if(conn.m_strHost.isEmpty()){
			QMessageBox::warning(this,tr("Warning"), tr("You need to define SMTP host in settings!"));
			return false;
		}

		//_DUMP(m_recToRecordSet);
		bOK = MailManager::SendMailDirectly(recError, m_lstData,conn,m_lstAttachments,m_recToRecordSet,m_recCCRecordSet,m_recBCCRecordSet,Call_CreateEmailFromTemplate,g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_CC).toString(),g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_BCC).toString(),g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USE_AUTH).toBool(),m_nProjectID,m_bSerialEmailMode);
		if(!bOK){
			qDebug() << "Show error message";
			QMessageBox::warning(this,tr("Warning"), tr("Error sending the mail!"));
		}
		else if(recError.getRowCount() > 0){
			qDebug() << "Show list of failed emails";
			//show list of failed emails
			//_DUMP(recError);
			QString strMsg = tr("There were errors when sending the serial email to the following addresses:");
			int nCount = recError.getRowCount();
			for(int i=0; i<nCount; i++){
				strMsg += "\n";
				if(!recError.getDataRef(i, "CONTACT_NAME").toString().isEmpty()){
					strMsg += "\"";
					strMsg += recError.getDataRef(i, "CONTACT_NAME").toString();
					strMsg += "\" ";
				}
				strMsg += "<";
				strMsg += recError.getDataRef(i, "CONTACT_EMAIL").toString();
				strMsg += ">";
			}

			strMsg += "\n\n";
			strMsg += tr("All other emails have been sent successfully.");

			//QMessageBox::warning(this,tr("Warning"), strMsg);
			ScrollableMsgBox msgBox;
			msgBox.setModal(true);
			msgBox.SetButtonInfo(0, tr("OK"), QMessageBox::YesRole);
			msgBox.HideButton(1);
			msgBox.SetMessage(strMsg);
			msgBox.setWindowTitle(tr("Warning"));
			msgBox.exec();
		}
	}
	
	return bOK;
}

void EMailDialog::on_AddContacts_pushButton_clicked()
{
	//Create tmp recordsets.
	DbRecordSet recTo, recCC, recBCC;
	recTo.copyDefinition(m_recToRecordSet);
	recCC.copyDefinition(m_recToRecordSet);
	recBCC.copyDefinition(m_recToRecordSet);
	
	//Get contact selection dialog.
	ContactSelection contSel;
	if (contSel.exec())
	{
		contSel.GetSelectedContacts(recTo, recCC, recBCC);
		//Append to table widget.
		ui.Address_tableWidget->AppendContacts(recTo, recCC, recBCC);
	}
}

void EMailDialog::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Escape)
	{
		on_cmdCancel();
		return;
	}

	FuiBase::keyPressEvent(event);
}



void EMailDialog::SetPersonData()
{
	//m_nPersonID = g_pClientManager->GetPersonID();

	Status status;

	if (g_pClientManager->GetPersonContactID() == 0)
		return;
	
	m_strPersonContactID = QVariant(g_pClientManager->GetPersonContactID()).toString();

	DbRecordSet tmp;
	g_pClientManager->GetUserContactData(tmp);
	if (tmp.getRowCount()==0 || tmp.getColumnCount()==0)
		return;

	QString strName  = tmp.getDataRef(0, "BCME_NAME").toString();
	QString strEmail = tmp.getDataRef(0, "BCME_ADDRESS").toString();


	//Add it to form.
	if (m_lstData.getRowCount()>0)
	{
		m_lstData.setData(0,"BEM_FROM",EmailHelperCore::GetFullEmailAddress(strName,strEmail,false));
	}
}


void EMailDialog::SetRecipientString(DbRecordSet &recRecipients, QString &strRecipients)
{
	int nRowCount = recRecipients.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		QString strRecipientName  = recRecipients.getDataRef(i, "CONTACT_NAME").toString();
		QString strRecipientEmail = recRecipients.getDataRef(i, "CONTACT_EMAIL").toString();
		
		//Set return string.
		strRecipients += EmailHelperCore::GetFullEmailAddress(strRecipientName,strRecipientEmail,true);
	}
}



void EMailDialog::WriteTo_CE_CONTACT_LINK(DbRecordSet &recRecipients, int nType /*1=From 2=To 3=CC 4=BCC*/)
{
	DbRecordSet tmpCntLink;
	tmpCntLink.copyDefinition(m_lstContactLink);

	//Define recordset.
	int nRowCount = recRecipients.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nContactID = recRecipients.getDataRef(i, "CONTACT_ID").toInt();

		//If contact ID is null then skip.
		if (nContactID<=0)
			continue;
		tmpCntLink.addRow();
		int nRow = tmpCntLink.getRowCount() - 1;
		tmpCntLink.setData(nRow, "BNMR_TABLE_KEY_ID_2", nContactID);
		tmpCntLink.setData(nRow, "BNMR_SYSTEM_ROLE", nType);
	}

	//remove duplicate  contacts from role:
	tmpCntLink.removeDuplicates(tmpCntLink.getColumnIdx("BNMR_TABLE_KEY_ID_2"));


	m_lstContactLink.merge(tmpCntLink);
}




//------------------------------------------------------------------------------
//							SIMPLE FUI OPS
//------------------------------------------------------------------------------



void EMailDialog::DataPrepareForInsert(Status &err)
{
	FuiBase::DataPrepareForInsert(err);

	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_EDIT)->setVisible(false);		//if going insert mode (from outside), skipo

	//Set sending date time.
	//m_lstData.setData(0, "BEM_RECV_TIME", QDateTime::currentDateTime());
	//Set email type - text, html or both.
	m_lstData.setData(0, "BEM_EMAIL_TYPE", GlobalConstants::EMAIL_TYPE_HTML);//m_nEmailType);


	//set owner=logged----->>>
	int nLoggedID=g_pClientManager->GetPersonID();
	if (nLoggedID!=0)
		m_lstData.setData(0,"CENT_OWNER_ID",nLoggedID);
	m_lstData.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_EMAIL);

	//m_lstData.setData(0,"CENT_IS_PRIVATE",0);

	//issue: 1462:
	ui.arSelector->Invalidate();
	DbRecordSet recContactData;
	g_pClientManager->GetUserContactData(recContactData);
	if (recContactData.getRowCount()>0)
	{
		if (recContactData.getDataRef(0,"BCNT_OUT_MAIL_AS_PRIV").toInt()>0)
			ui.arSelector->SetAccessDisplayLevel(AccUARCore::ACC_PRIVATE);    //as before: set private for logged user
	}

	m_lstData.setData(0,"BEM_OUTGOING",1);
	
	m_bBodyChanged=false;
	

	//disable tasks:
	ui.frameTask->ClearTaskData();
	m_lstTaskCache=ui.frameTask->m_recTask;

	//for insert: set from= logged user
	SetPersonData();
	
}



void EMailDialog::DataClear()
{
	m_lstData.clear();

	m_lstContactLink.clear();
	m_lstAttachments.clear();

	m_recToRecordSet.clear();
	m_recCCRecordSet.clear();
	m_recBCCRecordSet.clear();
	ui.listAttachemnts->ClearAll();


	//tasks:
	ui.frameTask->ClearTaskData();
	m_lstTaskCache.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	m_lstTaskCache.addRow();
	m_bBodyChanged=false;
	m_bSaveSendClicked=false;
	ui.arSelector->Invalidate();
}

void EMailDialog::DataDefine()
{
	DbSqlTableDefinition::GetKeyData(m_nTableID,m_TableData);
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_COMM_ENTITY));
}


void EMailDialog::DataRead(Status &err,int nRecordID)
{
	DbRecordSet lstContactEmails;
	DbRecordSet lstUAR,lstGAR;

	_SERVER_CALL(BusEmail->Read(err,nRecordID,m_lstData,m_lstContactLink,m_lstTaskCache,lstContactEmails,m_lstAttachments,lstUAR,lstGAR))
	if (err.IsOK())
	{
		_DUMP(m_lstData);
		//_DUMP(m_lstCIDInfo);
		//_DUMP(m_lstAttachments);

		//if read flag is unset, set it, fire global signal:
		if (m_lstData.getRowCount()>0)
		{

			//unread flag
			if (m_lstData.getDataRef(0,"BEM_UNREAD_FLAG").toInt()>0)
			{
				_SERVER_CALL(BusEmail->SetEmailRead(err,m_lstData.getDataRef(0,"BEM_COMM_ENTITY_ID").toInt()))
				//Issue  #817.
				//if (err.IsOK())
				//	g_ClientCache.ModifyCache(ENTITY_BUS_EMAILS,m_lstData,DataHelper::OP_EDIT);
			}

			//if flag=not snyc'd, try to sync
			
			if (m_lstData.getDataRef(0,"BEM_IS_SYNC_IN_PROGRESS").toInt()>0)
			{
				bool bFireWarning=true;
				DbRecordSet lstProcessedEmails=g_CommManager.ProcessEmailContentFromOutlook();
				if (lstProcessedEmails.getColumnIdx("BEM_ID")!=-1)
				{
					int nRow=lstProcessedEmails.find("BEM_ID",m_lstData.getDataRef(0,"BEM_ID").toInt(),true);
					if (nRow!=-1)
					{
						bFireWarning=false;
						//update content (re-read from server):
						_SERVER_CALL(BusEmail->Read(err,nRecordID,m_lstData,m_lstContactLink,m_lstTaskCache,lstContactEmails,m_lstAttachments,lstUAR,lstGAR))
					}
				}

				//if not synced, warn user:
				if (bFireWarning)
					QMessageBox::warning(this,tr("Warning"),tr("The contents of this email is not yet actual, because the email is still being written or cancelled"));

			}

			ui.arSelector->Load(m_lstData.getDataRef(0,"BEM_ID").toInt(),BUS_EMAIL,&lstUAR,&lstGAR);
			ui.arSelector->SetRecordDisplayName(m_lstData.getDataRef(0,"BEM_SUBJECT").toString());
		}
	
		if (m_lstTaskCache.getRowCount()>0)
		{
			ui.frameTask->m_recTask=m_lstTaskCache; //store into cache & tasker
		}

		if (m_lstData.getRowCount()>0)
		{
			Data2Emails(lstContactEmails,m_lstData.getDataRef(0,"BEM_TO").toString(),m_lstData.getDataRef(0,"BEM_CC").toString(),m_lstData.getDataRef(0,"BEM_BCC").toString());
			ui.Address_tableWidget->AppendContacts(m_recToRecordSet, m_recCCRecordSet, m_recBCCRecordSet);
		}

		m_bBodyChanged=false;

		if(!m_CID_ModifyEmailBodyInProgress)
			EmailHelper::UnpackCIDToTemp(m_lstCIDInfo,m_lstData,m_lstAttachments);
		ui.listAttachemnts->SetData(m_lstAttachments);
		RefreshDisplay();

		//_DUMP(m_lstCIDInfo);
		//_DUMP(m_lstAttachments);

		if( 0 == m_lstData.getDataRef(0,"BEM_TEMPLATE_FLAG").toInt() &&
			!m_lstData.getDataRef(0, "BEM_RECV_TIME").toDateTime().isNull())
		{
			ShowMailSendRadios(false); //hidden for already send emails
		}
	}


}

void EMailDialog::DataLock(Status &err)
{
	int nCommEntityID=m_lstData.getDataRef(0,"CENT_ID").toInt();
	Q_ASSERT(nCommEntityID!=0);

	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"CENT_ID");
	lst.addRow();
	lst.setData(0,0,nCommEntityID);
	DbRecordSet lstStatusRows;
	_SERVER_CALL(ClientSimpleORM->Lock(err,CE_COMM_ENTITY,lst,m_strLockedRes, lstStatusRows))
		if (!err.IsOK())
			if (err.getErrorCode()==StatusCodeSet::ERR_SQL_LOCKED_BY_ANOTHER)
			{
				QString strMsg=err.getErrorTextRaw();
				int nPos=strMsg.indexOf(" - ");
				if (nPos>=0)
				{
					QString strPersCode=strMsg.left(nPos);
					//test if this is moa
					if(strPersCode==g_pClientManager->GetPersonCode())
					{

						int nResult=QMessageBox::question(this,tr("Warning"),tr("This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?"),tr("Yes"),tr("No"));
						if (nResult==0)
						{
							//unlock record on server:
							Status err_temp;
							bool bUnlocked=false;
							_SERVER_CALL(ClientSimpleORM->UnLockByRecordID(err_temp,CE_COMM_ENTITY,lst,bUnlocked))
								if (!err_temp.IsOK() || !bUnlocked)
								{
									return; //return with old lock error
								}
								else
								{
									err.setError(0); //clear error and return
									_SERVER_CALL(ClientSimpleORM->Lock(err,CE_COMM_ENTITY,lst,m_strLockedRes, lstStatusRows))
									return;
								}

						}

					}
				}
			}

}
void EMailDialog::DataDelete(Status &err)
{
	int nCommEntityID=m_lstData.getDataRef(0,"CENT_ID").toInt();
	Q_ASSERT(nCommEntityID!=0);

	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"CENT_ID");
	lst.addRow();
	lst.setData(0,0,nCommEntityID);
	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	_SERVER_CALL(ClientSimpleORM->Delete(err,CE_COMM_ENTITY,lst,m_strLockedRes, lstStatusRows, pBoolTransaction))
	if (err.IsOK())
		DataClear();

}

//write & send msg
void EMailDialog::DataWrite(Status &err)
{
	if (ui.grupIsTemplate->isChecked())
		m_lstData.setData(0,"BEM_TEMPLATE_FLAG",1);
	else
		m_lstData.setData(0,"BEM_TEMPLATE_FLAG",0);


	//if template and template name empty warning:
	if (ui.grupIsTemplate->isChecked() && m_lstData.getDataRef(0,"BEM_TEMPLATE_NAME").toString().isEmpty())
	{
		err.setError(1,tr("Template Name is mandatory!"));
		return;
	}

		//save body:
	if (m_bBodyChanged)
	{
		QString strBody;
		if(m_lstData.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT ||
			 ui.chkPlainText->checkState() == Qt::Checked)
		{
			strBody = ui.TextEdit_widget->GetPlainText();
			m_lstData.setData(0, "BEM_EMAIL_TYPE", GlobalConstants::EMAIL_TYPE_PLAIN_TEXT); //force plaintext
		}
		else{
			bool bFixateHTML = (m_lstData.getDataRef(0,"BEM_FIX_HTML").toInt() > 0) || ui.chkFixHTML->isChecked();
			if(bFixateHTML)
				strBody = ui.TextEdit_widget->GetPlainText();
			else{
				strBody = ui.TextEdit_widget->GetHtml(true);
				if(ui.TextEdit_widget->IsHtmlFixingEnabled())
					RichEditWidget::PostprocessFixRtfEmail(strBody);
			}
		}

		m_lstData.setData(0,"BEM_BODY",strBody);
	}
	else
	{
		//check if text-only requested and we have HTML
		if(ui.chkPlainText->checkState() == Qt::Checked &&
			m_lstData.getDataRef(0,"BEM_EMAIL_TYPE").toInt() != GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
		{
			//conversion required
			QString strBody = ui.TextEdit_widget->GetPlainText();
			m_lstData.setData(0,"BEM_BODY",strBody);
			m_lstData.setData(0, "BEM_EMAIL_TYPE", GlobalConstants::EMAIL_TYPE_PLAIN_TEXT); //force plaintext
		}
	}

	//_DUMP(m_lstCIDInfo);
	//_DUMP(m_lstAttachments);

	if(!ui.listAttachemnts->GetData(m_lstAttachments,true))
		return;

	//_DUMP(m_lstCIDInfo);
	//_DUMP(m_lstAttachments);

	EmailHelper::UpdateEmailImageCID(m_lstCIDInfo,m_lstData,m_lstAttachments);

	//_DUMP(m_lstCIDInfo);
	//_DUMP(m_lstAttachments);

	//-----------------------------------------------
	//			CONVERT & CHECK DATA
	//-----------------------------------------------

	//Clear recordsets.
	m_recToRecordSet.clear();
	m_recCCRecordSet.clear();
	m_recBCCRecordSet.clear();
	//Get recipients from recipients table.
	ui.Address_tableWidget->GetRecipients(m_recToRecordSet, m_recCCRecordSet, m_recBCCRecordSet);

	//Convert data:
	QString strTo,strCC,strBCC;
	Emails2Data(strTo,strCC,strBCC);

	m_lstData.setData(0,"BEM_TO",strTo);
	m_lstData.setData(0,"BEM_CC",strCC);
	m_lstData.setData(0,"BEM_BCC",strBCC);

	//check data:
	//if(m_lstData.getDataRef(0,"BEM_FROM").toString().isEmpty())
	//	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,tr("From is mandatory field!"));	return;	}

	//if(strTo.isEmpty())
	//	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,tr("To is mandatory field!"));	return;	}


	DbRecordSet rowTask;
	if (ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt()>0)
	{
		ui.frameTask->GetDataFromGUI(); //fill mrectask
		ui.frameTask->CheckTaskRecordBeforeSave(err);
		if (!err.IsOK())
			return;
		rowTask=ui.frameTask->m_recTask;
	}

	//-----------------------------------------------
	//			SEND MESSAGE
	//-----------------------------------------------

	bool bMsgSucessSend=true;
	bool bMsgActuallySent=false;

	//if subject is empty, warn
	if (ui.Subject_lineEdit->text().isEmpty())
	{
		QString strSubject = QInputDialog::getText(this, tr("Message Subject!"), tr("Please enter message subject"), QLineEdit::Normal,	tr("(Subject)"));
		ui.Subject_lineEdit->setText(strSubject);
	}



	DbRecordSet recSerialErrors;
	if (m_bSaveSendClicked)
	{
		//m_lstData.Dump();
		bMsgSucessSend=SendMail(recSerialErrors);
		bMsgActuallySent=true;
		if (!bMsgSucessSend)
		{
			bMsgActuallySent=false;
		}

		//mark task as done if was active::
		if (bMsgActuallySent && ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt()>0 && rowTask.getRowCount()>0)
		{
			rowTask.setData(0, "BTKS_SYSTEM_STATUS", GlobalConstants::TASK_STATUS_COMPLETED);
			rowTask.setData(0, "BTKS_COMPLETED", QDateTime::currentDateTime());
			if (rowTask.getDataRef(0, "BTKS_OWNER").isNull())
				rowTask.setData(0, "BTKS_OWNER", g_pClientManager->GetPersonID());
		}

		//set date & flag for sync
		if (bMsgActuallySent)
		{
			//m_lstData.Dump();
			ui.dateTimeEdit->setDateTime(QDateTime::currentDateTime());
			m_lstData.setData(0,"BEM_IS_SYNC_IN_PROGRESS",1);

			//set datetime: issue 2267: do not change recv_time: only when send
			m_lstData.setData(0,"BEM_RECV_TIME",QDateTime::currentDateTime());
		}
		if (ui.radDirectMail->isChecked())
		{
			m_lstData.setData(0,"BEM_IS_SYNC_IN_PROGRESS",0);
		}
	}

	if(ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt()>0 && rowTask.getRowCount()>0)
	{
		if(GlobalConstants::TASK_STATUS_COMPLETED == rowTask.getDataRef(0, "BTKS_SYSTEM_STATUS").toInt())
		{
			//#2707: remove avatar for this task if it exists
			EMailDialog::RemoveAvatar(AVATAR_TASK_EMAIL, m_lstData.getDataRef(0, "BEM_ID").toInt());
		}
	}

	//set datetime: issue 2267: do not change recv_time: only when send or when NEW mail
	if (m_nEntityRecordID<=0)
		m_lstData.setData(0,"BEM_RECV_TIME",QDateTime::currentDateTime());

	//if task: add as owner (issue 1975)
	if (rowTask.getRowCount()>0)
	{
		//first delete old ones with role:
		m_lstContactLink.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_EMAIL_TASK_OWNER);
		m_lstContactLink.deleteSelectedRows();

		int nOwnerID=rowTask.getDataRef(0,"BTKS_OWNER").toInt();
		DbRecordSet recPerson;
		m_PersonCache.GetEntityRecord(nOwnerID,recPerson);
		if (recPerson.getRowCount()>0)
		{
			int nContactID=recPerson.getDataRef(0,"BPER_CONTACT_ID").toInt();
			if (nContactID>0)
				if(m_lstContactLink.find("BNMR_TABLE_KEY_ID_2",nContactID,true)<0)
				{
					m_lstContactLink.addRow();
					m_lstContactLink.setData(m_lstContactLink.getRowCount()-1,"BNMR_TABLE_KEY_ID_2",nContactID);
					m_lstContactLink.setData(m_lstContactLink.getRowCount()-1,"BNMR_SYSTEM_ROLE",GlobalConstants::CONTACT_LINK_ROLE_EMAIL_TASK_OWNER);
					//_DUMP(m_lstContactLink);
				}
		}
	}


	qDebug() << "Get GAR, UAR";

	//-----------------------------------------------
	//			WRITE TO DB
	//-----------------------------------------------
	DbRecordSet lstUAR,lstGAR;
	ui.arSelector->Save(m_nEntityRecordID,BUS_EMAIL,&lstUAR,&lstGAR); //get ar rows for save

	qDebug() << "Write to DB";

	if(m_bSerialEmailMode && m_nEntityRecordID!=0)	//serial email with template
	{
		//test:
		if (!ui.From_lineEdit->text().isEmpty())
		{
			//First write FROM to CE_CONTACT_LINK.
			if (m_strPersonContactID.isEmpty()) //logged = FROM
				m_strPersonContactID = QVariant(g_pClientManager->GetPersonContactID()).toString();
		}

		int nContactIDIdx=m_recToRecordSet.getColumnIdx("CONTACT_ID");
		int nEmailIdx=m_recToRecordSet.getColumnIdx("CONTACT_EMAIL");
		int nEmailErrIdx = recSerialErrors.getColumnIdx("CONTACT_EMAIL");

		DbRecordSet lstContactsOrig = m_lstContactLink;
		
		//define
		DbRecordSet lstData;
		DbRecordSet set;
		set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
		lstData.copyDefinition(set, false);
		set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
		lstData.copyDefinition(set, false);
		lstData.addColumn(QVariant::String, "PROJECT_NAME");
		lstData.addColumn(QVariant::String, "PROJECT_CODE");
		lstData.addColumn(QVariant::String, "EMAIL_UNASSIGNED");
		lstData.addColumn(DbRecordSet::GetVariantType(), "ATTACHMENTS");
		lstData.addColumn(DbRecordSet::GetVariantType(), "CONTACTS");
		lstData.addColumn(DbRecordSet::GetVariantType(), "UAR_TABLE"); //BT: 29.01.2009: added for GUAR
		lstData.addColumn(DbRecordSet::GetVariantType(), "GAR_TABLE");
		lstData.addColumn(QVariant::Int, "ATTACHMENTS_SIZE");

		int nSizeTo=m_recToRecordSet.getRowCount();
		for(int i=0; i<nSizeTo; i++)
		{
			//store to database only for those emails that did not fail
			QString strEmail = m_recToRecordSet.getDataRef(i, nEmailIdx).toString();
			int nRes = recSerialErrors.find(nEmailErrIdx, strEmail, true);
			if(nRes >= 0)
				continue;	//skip saving this one

			lstData.addRow();
			int nCurRow = lstData.getRowCount()-1;
			lstData.assignRow(nCurRow, m_lstData, true);	//copy original mail

			//validate square brackets for this contact
			int nContactID = m_recToRecordSet.getDataRef(i, nContactIDIdx).toInt();
			QString strBody  = lstData.getDataRef(nCurRow,"BEM_BODY").toString();
			QString strNewBody;
			Call_CreateEmailFromTemplate(strBody, strNewBody, nContactID, m_nProjectID);

			//store new body for saving
			lstData.setData(nCurRow,"BEM_BODY", strNewBody);

			//owerwrite "TO" field
			lstData.setData(nCurRow,"BEM_TO", m_recToRecordSet.getDataRef(i, nEmailIdx).toString());

			//_DUMP(lstData);

			//now fake the contact links (WriteTo_CE_CONTACT_LINK writes directly to m_lstContactLink)
			m_lstContactLink.clear();
			DbRecordSet tmp;
			tmp.copyDefinition(m_recToRecordSet);
			tmp.addRow();
			tmp.setData(0, "CONTACT_ID", QVariant(m_strPersonContactID).toInt());
			WriteTo_CE_CONTACT_LINK(tmp, GlobalConstants::CONTACT_LINK_ROLE_FROM);
			DbRecordSet row = m_recToRecordSet.getRow(i);
			WriteTo_CE_CONTACT_LINK(row, GlobalConstants::CONTACT_LINK_ROLE_TO);
			WriteTo_CE_CONTACT_LINK(m_recCCRecordSet, GlobalConstants::CONTACT_LINK_ROLE_CC);

			lstData.setData(nCurRow, "CONTACTS", m_lstContactLink);
		}

		//restore original contact link list
		m_lstContactLink = lstContactsOrig;

		qDebug() << "Save a DB copy for each serial mail address (" << nSizeTo << " items)";

		//save all mails (will use chunks internally)
		int nSkippedOrReplacedCnt = 0;
		FUI_ImportEmail::ImportEmails(err, lstData, nSkippedOrReplacedCnt, true, false, false);
		_CHK_ERR_NO_MSG(err); //TOFIX what if only one fails

	}
	else	//other
	{
		//_DUMP(m_lstData);
		//_DUMP(m_lstCIDInfo);
		//_DUMP(m_lstAttachments);

		qDebug() << "Write single mail copy to DB";

		_SERVER_CALL(BusEmail->Write(err,m_lstData,m_lstContactLink,rowTask,m_lstAttachments,m_strLockedRes,lstUAR,lstGAR))
		if (err.IsOK())
		{
			qDebug() << "Write single mail copy to DB - OK";

			//_DUMP(m_lstCIDInfo);
			//_DUMP(m_lstAttachments);

			m_strLockedRes.clear();

			if (rowTask.getRowCount()>0)
			{
				ui.frameTask->m_recTask=rowTask;	//return d'
				m_lstTaskCache=rowTask;
			}

			//if mail sent, add to list
			if (bMsgActuallySent && !ui.radDirectMail->isChecked())
			{
				g_CommManager.AddEmailToSentMailList(m_lstData.getDataRef(0,"BEM_ID").toInt(),m_lstData.getDataRef(0,"BEM_EXT_ID").toByteArray(),m_lstData.getDataRef(0,"CENT_ID").toInt());
			}
			//re-init UAR selector (if after insert)
			ui.arSelector->Load(m_lstData.getDataRef(0,"BEM_ID").toInt(),BUS_EMAIL,&lstUAR,&lstGAR);
			ui.arSelector->SetRecordDisplayName(m_lstData.getDataRef(0,"BEM_SUBJECT").toString());
		}
		else
		{
			qDebug() << "Write single mail copy to DB - not OK";

			EmailHelper::UnpackCIDToTemp(m_lstCIDInfo,m_lstData,m_lstAttachments); //return body back..
			ui.listAttachemnts->SetData(m_lstAttachments);
		}
	}

	qDebug() << "Write to DB done";
}

void EMailDialog::DataUnlock(Status &err)
{
	FuiBase::DataUnlock(err);
	if (!err.IsOK()) return;
	if (m_lstTaskCache.getRowCount()>0)
		ui.frameTask->m_recTask=m_lstTaskCache;			//restore from cache
	else
		ui.frameTask->ClearTaskData();
	ui.arSelector->CancelChanges();
	if (m_lstData.getRowCount()>0)
		ui.arSelector->SetRecordDisplayName(m_lstData.getDataRef(0,"BEM_SUBJECT").toString());
}



//from DB to recipient table, updates->to, bcc, cc records
void EMailDialog::Data2Emails(DbRecordSet &lstContactEmails,QString strTo,QString strCC,QString strBCC)
{
	//load data:
	EmailHelperCore::LoadRecipients(lstContactEmails,m_recToRecordSet,strTo);
	EmailHelperCore::LoadRecipients(lstContactEmails,m_recCCRecordSet,strCC);
	EmailHelperCore::LoadRecipients(lstContactEmails,m_recBCCRecordSet,strBCC);
}

//updates-> m_lstContactLink
void EMailDialog::Emails2Data(QString &strTo,QString &strCC,QString &strBCC)
{
	SetRecipientString(m_recToRecordSet, strTo);
	SetRecipientString(m_recCCRecordSet, strCC);
	SetRecipientString(m_recBCCRecordSet, strBCC);

	m_lstContactLink.clear();

	//test:
	if (!ui.From_lineEdit->text().isEmpty())
	{
		//First write FROM to CE_CONTACT_LINK.
		if (m_strPersonContactID.isEmpty()) //logged = FROM
			m_strPersonContactID = QVariant(g_pClientManager->GetPersonContactID()).toString();
	}
	
	DbRecordSet tmp;
	tmp.copyDefinition(m_recToRecordSet);
	tmp.addRow();
	tmp.setData(0, "CONTACT_ID", QVariant(m_strPersonContactID).toInt());

	
	WriteTo_CE_CONTACT_LINK(tmp, GlobalConstants::CONTACT_LINK_ROLE_FROM);
//
	//all others:
	WriteTo_CE_CONTACT_LINK(m_recToRecordSet, GlobalConstants::CONTACT_LINK_ROLE_TO);
	WriteTo_CE_CONTACT_LINK(m_recCCRecordSet, GlobalConstants::CONTACT_LINK_ROLE_CC);
	WriteTo_CE_CONTACT_LINK(m_recBCCRecordSet, GlobalConstants::CONTACT_LINK_ROLE_BCC);


	//m_lstContactLink.Dump();


}


//execute scheduled mail:
void EMailDialog::Execute (int nEmailID)
{
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_EDIT)->setVisible(false);		//if going insert mode (from outside), skipo
	//m_bIsExecuteMode=true;
	on_selectionChange(nEmailID);

	//if ok go edit:
	if (m_nEntityRecordID!=0)
	{
		if(!on_cmdEdit()) return;
		//clear template data (if it was from template, now its cleared):
		ClearTemplateFields();
	}
}


void EMailDialog::on_grupIsTemplate_clicked()
{


	//based on checkbook:
	if (ui.grupIsTemplate->isChecked())
	{
		if (ui.frameTask->m_recTask.getDataRef(0,"BTKS_IS_TASK_ACTIVE").toInt()>0)
		{
			QMessageBox::warning(this,tr("Warning"),tr("Template Email can not be scheduled!"));
			ui.grupIsTemplate->setChecked(false);
			return;
		}

		if (ui.grupIsTemplate->maximumHeight()<160)
			ui.grupIsTemplate->setMaximumHeight(1600);
		ui.grupIsTemplate->setMinimumHeight(160);
		ui.frameX->setVisible(true);	
		m_buttonSaveSend->setEnabled(false);
	}
	else
	{
		ui.grupIsTemplate->setMinimumHeight(25);
		ui.frameX->setVisible(false);
		if(m_nMode==MODE_EDIT || m_nMode==MODE_INSERT)
			m_buttonSaveSend->setEnabled(true);
	}
}


bool EMailDialog::CanClose()
{
	return (m_bUnconditionalClose ||(m_nMode != MODE_EDIT));
}






void EMailDialog::DataPrepareForCopy(Status &err)
{
	err.setError(0);
	QVariant intNull(QVariant::Int);

	//clear ID fields:
	m_lstData.setColValue(m_lstData.getColumnIdx(m_TableData.m_strPrimaryKey),intNull);
	m_lstData.setColValue(m_lstData.getColumnIdx("CENT_ID"),intNull);
	m_lstData.setColValue(m_lstData.getColumnIdx("CENT_TASK_ID"),intNull);
	m_lstData.setColValue(m_lstData.getColumnIdx("BEM_COMM_ENTITY_ID"),intNull);
	ui.frameTask->m_recTask.setColValue(ui.frameTask->m_recTask.getColumnIdx("BTKS_ID"),intNull);
	m_lstContactLink.setColValue(m_lstContactLink.getColumnIdx("BNMR_TABLE_KEY_ID_2"),intNull);
	
	m_lstAttachments.setColValue(m_lstAttachments.getColumnIdx("BEA_ID"),intNull);
	m_lstAttachments.setColValue(m_lstAttachments.getColumnIdx("BEA_EMAIL_ID"),intNull);
	
	m_lstTaskCache=ui.frameTask->m_recTask;
	m_bBodyChanged=false;
	ui.arSelector->Invalidate();
}


void EMailDialog::ClearTemplateFields()
{
	if (m_lstData.getRowCount()>0)
	{
		m_lstData.setData(0,"BEM_TEMPLATE_FLAG",0);
		m_lstData.setData(0,"BEM_TEMPLATE_NAME","");
		m_lstData.setData(0,"BEM_TEMPLATE_DESCRIPTION","");

		m_lstData.setData(0,"CENT_OWNER_ID",g_pClientManager->GetPersonID());
		QByteArray tmpEmpty;
		m_lstData.setData(0,"BEM_EXT_ID",tmpEmpty);
		m_lstData.setData(0,"BEM_EXT_APP_DB_INSTANCE","");
		m_lstData.setData(0,"BEM_EXT_APP","");
		m_lstData.setData(0,"BEM_EXT_ID_CHKSUM","");
	}
}

bool EMailDialog::OutlookSendMailByDLL()
{
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_OUTLOOK, nValue))
	{
		return false;
	}

#ifdef _WIN32
	_DUMP(m_lstAttachments);

	//Open Outlook SENT form:
	bool bPlain=(m_lstData.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT);
	return MapiManager::OpenMailInOutlookFromRecord(m_lstData,m_lstAttachments,m_recToRecordSet,m_recCCRecordSet,m_recBCCRecordSet,bPlain);
#else
	return false;
#endif
}



void EMailDialog::OnScheduleTaskToggle()
{
	if (ui.frameTask->m_recTask.getDataRef(0,"BTKS_IS_TASK_ACTIVE").toInt()>0 && ui.grupIsTemplate->isChecked() )
	{
		QMessageBox::warning(this,tr("Warning"),tr("Template Email can not be scheduled!"));
		ui.frameTask->Hide();
		return;
	}
}



void EMailDialog::OnBodyChanged()
{
	m_bBodyChanged=true;
}



//on save send:
void EMailDialog::OnSaveSend()
{
	m_bSaveSendClicked=true;
	on_cmdOK();
}



/*
Reply
-----
1. The send email popup window (with the CE Menu elements) is opened exactly as it is done in the pie menu. The user can send an email with or without template.
2. The standard email popup window is opened with the From: contact from the selected email in the To: field, the subject has a "Re:" (translateable) added in front and the original email body is appended to the new email (after the template body if used). The cursor is on the beginning of the text.
3. Normal email handling...

Forward
-------
Same as Reply, but no recipient is in the To: field --> a contact must be selected or an email address manually entered.
*/

void EMailDialog::PrepareReplyForwardMail(int nEmailReplyID, bool bReply)
{

	//clear TO & FROM:
	ui.From_lineEdit->clear();
	ui.Address_tableWidget->ClearAll();
	m_recToRecordSet.clear();
	m_recCCRecordSet.clear();
	m_recBCCRecordSet.clear();

	//reply&forward data
	DbRecordSet lstContactEmails,recContacts;
	DbRecordSet recReplyEmail;
	DbRecordSet recReplyTask,recReplyAttachments;
	DbRecordSet lstUAR,lstGAR;

	Status err;
	_SERVER_CALL(BusEmail->Read(err,nEmailReplyID,recReplyEmail,recContacts,recReplyTask,lstContactEmails,recReplyAttachments,lstUAR,lstGAR))
	_CHK_ERR(err);
	if (recReplyEmail.getRowCount()==0) 
		return;

	//BODY:
	QTextEdit txtEdit;
	QString strBody=m_lstData.getDataRef(0,"BEM_BODY").toString();
	if(m_lstData.getDataRef(0,"BEM_EMAIL_TYPE").toInt()!=GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
	{
		txtEdit.setHtml(strBody);
		txtEdit.append(recReplyEmail.getDataRef(0,"BEM_BODY").toString());
		strBody=txtEdit.toHtml();
	}
	else
	{
		strBody.append(recReplyEmail.getDataRef(0,"BEM_BODY").toString());
	}
	

	//strBody+="<br>"+recReplyEmail.getDataRef(0,"BEM_BODY").toString();
	m_lstData.setData(0,"BEM_BODY",strBody);
	//UpdateEmailImageCID();

	//SUBJECT:
	QString strSubject=m_lstData.getDataRef(0,"BEM_SUBJECT").toString();
	if (bReply)
		strSubject="Re: "+recReplyEmail.getDataRef(0,"BEM_SUBJECT").toString();
	else
		strSubject="Fwd: "+recReplyEmail.getDataRef(0,"BEM_SUBJECT").toString();
	m_lstData.setData(0,"BEM_SUBJECT",strSubject);


//	lstContactEmails.Dump();

	//generate FROM
	DbRecordSet lstToDummy;
	QString strTO=recReplyEmail.getDataRef(0,"BEM_TO").toString(); //delimited text
	EmailHelperCore::LoadRecipients(lstContactEmails,lstToDummy,strTO);

	//issue 2684: ignore from, always use logged user as from
	//QString strFROM;
	//m_strPersonContactID="";
	//if (lstToDummy.getRowCount()>0)
	//{
		//strFROM=EmailHelperCore::GetFullEmailAddress(lstToDummy.getDataRef(0,"CONTACT_NAME").toString(),lstToDummy.getDataRef(0,"CONTACT_EMAIL").toString(),false);
		//m_strPersonContactID=lstToDummy.getDataRef(0,"CONTACT_ID").toString();
		//lstToDummy.deleteRow(0);
	//}

	//QString strOriginalFROM;


	//generate TO
	QString strOldFrom=recReplyEmail.getDataRef(0,"BEM_FROM").toString();
	if (!strOldFrom.isEmpty())
	{
		//find email in contact list: set id
		int nContactID=-1;
		int nProjectID=-1;
		QString strContact=EmailHelperCore::GetNameFromEmail(strOldFrom);
		QString strEmail=EmailHelperCore::GetEmailFromName(strOldFrom);

		//set email for from contact:
		int nSizeE=lstContactEmails.getRowCount();
		strEmail=strEmail.toLower();
		strEmail=strEmail.replace(";","").trimmed(); //remove sep if exists
		for(int i=0;i<nSizeE;++i)
		{
			if (lstContactEmails.getDataRef(i,"BCME_ADDRESS").toString().toLower().trimmed()==strEmail)
			{
				nContactID=lstContactEmails.getDataRef(i,"BCME_CONTACT_ID").toInt();
				break;
			}
		}
		//strOriginalFROM=strEmail;
		
		//if reply email has project, assign to it
		nProjectID=recReplyEmail.getDataRef(0,"BEM_PROJECT_ID").toInt();
		if (nProjectID>0)
		{
			ui.frameProject->SetCurrentEntityRecord(nProjectID);
		}
		else
		{
			//if contact found: set SAPNE project to his projects (if one)
			Status err;
			DbRecordSet lstProjects;
			_SERVER_CALL(BusContact->ReadNMRXProjects(err,nContactID,lstProjects))
			_CHK_ERR_NO_RET(err);

			if (err.IsOK() && lstProjects.getRowCount()==1)
			{
				nProjectID=lstProjects.getDataRef(0,"BUSP_ID").toInt();
				ui.frameProject->SetCurrentEntityRecord(nProjectID);
			}
		}


		DbRecordSet rowTo;
		rowTo.copyDefinition(m_recToRecordSet);
		rowTo.addRow();
		rowTo.setData(0,"CONTACT_NAME",strContact);
		rowTo.setData(0,"CONTACT_EMAIL",strEmail);
		rowTo.setData(0,"CONTACT_ID",nContactID);
		lstToDummy.insertRow(0);
		lstToDummy.assignRow(0,rowTo);

		//lstToDummy.Dump();

		if (nProjectID==0)
			nProjectID=-1;

		QString strNewBody;
		EmailHelper::CreateEmailFromTemplate(m_lstData.getDataRef(0,"BEM_BODY").toString(),strNewBody,nContactID,nProjectID);
		m_lstData.getDataRef(0,"BEM_BODY")=strNewBody;
		//UpdateEmailImageCID();

		m_nProjectID = nProjectID;
	}

	lstToDummy.Dump();

	//set TO & FROM:

	//issue 2684: ignore from, always use logged user as from
	//FROM is always logged user:
	SetPersonData();
	//m_lstData.setData(0,"BEM_FROM",strFROM);

	if (bReply) //if forward leave as is:
	{
		m_recToRecordSet=lstToDummy;
		//m_recToRecordSet.Dump();
		//qDebug()<<recReplyEmail.getDataRef(0,"BEM_CC").toString();
		EmailHelperCore::LoadRecipients(lstContactEmails,m_recCCRecordSet,recReplyEmail.getDataRef(0,"BEM_CC").toString());
		EmailHelperCore::LoadRecipients(lstContactEmails,m_recBCCRecordSet,recReplyEmail.getDataRef(0,"BEM_BCC").toString());
		//m_recCCRecordSet.Dump();
	}

	//issue 2684: ignore from, always use logged user as from, now check if logged user is appearing somewhere in TO, CCC or BCC listst, if so then delete him:
	int nContactFromID=m_strPersonContactID.toInt();
	int nRow=m_recToRecordSet.find("CONTACT_ID",nContactFromID,true);
	if (nRow>=0)
		m_recToRecordSet.deleteRow(nRow);
	nRow=m_recCCRecordSet.find("CONTACT_ID",nContactFromID,true);
	if (nRow>=0)
		m_recCCRecordSet.deleteRow(nRow);
	nRow=m_recBCCRecordSet.find("CONTACT_ID",nContactFromID,true);
	if (nRow>=0)
		m_recBCCRecordSet.deleteRow(nRow);

	//issue 2684:  now find if there is any double occurrence of email addresses
	m_recToRecordSet.removeDuplicates(m_recToRecordSet.getColumnIdx("CONTACT_EMAIL"));
	m_recCCRecordSet.removeDuplicates(m_recCCRecordSet.getColumnIdx("CONTACT_EMAIL"));
	m_recBCCRecordSet.removeDuplicates(m_recBCCRecordSet.getColumnIdx("CONTACT_EMAIL"));

}

//send email to contact id's (defaults) 
void EMailDialog::PrepareMailToSendToContacts(DbRecordSet &recContacts, DbRecordSet &recContactsWithoutEmail, QString strDefaultEmailForFirstContact)
{
	//clear TO:
	ui.Address_tableWidget->ClearAll();
	m_recToRecordSet.clear();
	m_recCCRecordSet.clear();
	m_recBCCRecordSet.clear();


	//recContactsWithoutEmail.clear();
	//recContactsWithoutEmail.copyDefinition(m_recToRecordSet);

	if(recContacts.getRowCount()==0) 
		return;

	DbRecordSet lstContactEmails;
	DbRecordSet lstContactWithoutEmails;
	lstContactWithoutEmails.addColumn(QVariant::Int,"BCNT_ID");

	//check list:
	QString strCntID="BCNT_ID";
	if(recContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2")>=0)
		strCntID="BNMR_TABLE_KEY_ID_2";
	else if (recContacts.getColumnIdx("BCNT_ID")==-1)
		return;

	//read all emails (only defaults in general) from server:
	Status err;
	QString strWhere;
	if(!m_bSerialUseAllContactEmails)
		strWhere = " AND BCME_IS_DEFAULT=1";
	_SERVER_CALL(ClientSimpleORM->ReadFromParentIDs(err, BUS_CM_EMAIL, lstContactEmails, "BCME_CONTACT_ID",recContacts,strCntID,"",strWhere))
	_CHK_ERR(err);
	if (lstContactEmails.getRowCount()==0)
		return;

	//define temp list
	DbRecordSet tmp;
	tmp.copyDefinition(lstContactEmails);
	int nContactIdx=lstContactEmails.getColumnIdx("BCME_CONTACT_ID");
	int nContactIdx_1=recContacts.getColumnIdx("BCNT_ID");
	if (nContactIdx_1==-1)
		nContactIdx_1=recContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");
	if (nContactIdx_1!=-1 && nContactIdx!=-1)
	{

		//First row is TO recipient.
		int nRowCount = recContacts.getRowCount();
		for (int i = 0; i < nRowCount; ++i)
		{
			//find contact mails, copy 2 tmp
			int nContactID = recContacts.getDataRef(i, nContactIdx_1).toInt();
			lstContactEmails.find(nContactIdx,nContactID);
			tmp.clear();
			tmp.merge(lstContactEmails,true); //get only selected //preferably only one

			//lstContactEmails.Dump();
			//tmp.Dump();

			if (tmp.getRowCount()>0)
			{
				if(m_bSerialUseAllContactEmails)	
				{
					//special serial mode where we send message to all emails of the selected contacts
					Q_ASSERT(m_bSerialEmailMode);

					int nEmailCnt = tmp.getRowCount();
					for(int j=0; j<nEmailCnt; j++)
					{
						m_recToRecordSet.addRow();
						int nRow=m_recToRecordSet.getRowCount()-1;
						m_recToRecordSet.setData(nRow, "CONTACT_ID",	nContactID);
						m_recToRecordSet.setData(nRow, "EMAIL_ID",		tmp.getDataRef(j, "BCME_ID").toInt());
						//m_recToRecordSet.setData(nRow, "CONTACT_NAME",	tmp.getDataRef(j, "BCME_NAME").toString());
						m_recToRecordSet.setData(nRow, "CONTACT_EMAIL", tmp.getDataRef(j, "BCME_ADDRESS").toString());
					}
				}
				else
				{
					//If first row and strEmail is already sent then add all you need.
					if (!i || m_bSerialEmailMode)
					{
						m_recToRecordSet.addRow();
						int nRow=m_recToRecordSet.getRowCount()-1;
						m_recToRecordSet.setData(nRow, "CONTACT_ID",	nContactID);
						m_recToRecordSet.setData(nRow, "EMAIL_ID",		tmp.getDataRef(0, "BCME_ID").toInt());
						//m_recToRecordSet.setData(nRow, "CONTACT_NAME",	tmp.getDataRef(0, "BCME_NAME").toString());
						if (strDefaultEmailForFirstContact.isEmpty())
							m_recToRecordSet.setData(nRow, "CONTACT_EMAIL", tmp.getDataRef(0, "BCME_ADDRESS").toString());
						else
							m_recToRecordSet.setData(nRow, "CONTACT_EMAIL", strDefaultEmailForFirstContact);
					}
					//Else add to BCC.
					else
					{
						m_recBCCRecordSet.addRow();
						int nRow=m_recBCCRecordSet.getRowCount()-1;
						m_recBCCRecordSet.setData(nRow, "CONTACT_ID",	nContactID);
						m_recBCCRecordSet.setData(nRow, "EMAIL_ID",		tmp.getDataRef(0, "BCME_ID").toInt());
						//m_recBCCRecordSet.setData(nRow, "CONTACT_NAME",	tmp.getDataRef(0, "BCME_NAME").toString());
						m_recBCCRecordSet.setData(nRow, "CONTACT_EMAIL",tmp.getDataRef(0, "BCME_ADDRESS").toString());
					}
				}
			}
			else
			{
				lstContactWithoutEmails.addRow();
				lstContactWithoutEmails.setData(lstContactWithoutEmails.getRowCount()-1, 0,	nContactID);
			}
		}
	}

	FillNamesInToCCBCCRecords();

	//recContactsWithoutEmail = DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION)
	m_ContactCache.GetEntityRecords(lstContactWithoutEmails,0,recContactsWithoutEmail);

	//if sender is empty then set it to logged user...
	if (m_lstData.getDataRef(0,"BEM_FROM").toString().isEmpty())
	{
		SetPersonData();
	}
}


void EMailDialog::on_chkPlainText_clicked()
{
	//show Outlook warning label
	if(ui.listAttachemnts->IsCIDAttachmentExists())
	{
		if(ui.chkPlainText->checkState() == Qt::Checked)
			ui.labelOutlook->setVisible(false);
		else
			ui.labelOutlook->setVisible(true);
	}
	else
		ui.labelOutlook->setVisible(false);
}

void EMailDialog::CreateQuickTask(Status &err, DbRecordSet &rowTask)
{
	//use local task row:
	if (rowTask.getRowCount()==0)
	{
		rowTask=ui.frameTask->m_recTask;
	}

	if (ui.grupIsTemplate->isChecked())
		m_lstData.setData(0,"BEM_TEMPLATE_FLAG",1);
	else
		m_lstData.setData(0,"BEM_TEMPLATE_FLAG",0);


	//if template and template name empty warning:
	if (ui.grupIsTemplate->isChecked() && m_lstData.getDataRef(0,"BEM_TEMPLATE_NAME").toString().isEmpty())
	{
		m_lstData.setData(0,"BEM_TEMPLATE_NAME",tr("New Template"));
		//err.setError(1,tr("Template Name is mandatory!"));
		//return;
	}



	//-----------------------------------------------
	//			CONVERT & CHECK DATA
	//-----------------------------------------------

	//Clear recordsets.
	m_recToRecordSet.clear();
	m_recCCRecordSet.clear();
	m_recBCCRecordSet.clear();
	//Get recipients from recipients table.
	ui.Address_tableWidget->GetRecipients(m_recToRecordSet, m_recCCRecordSet, m_recBCCRecordSet);

	//Convert data:
	QString strTo,strCC,strBCC;
	Emails2Data(strTo,strCC,strBCC);

	m_lstData.setData(0,"BEM_TO",strTo);
	m_lstData.setData(0,"BEM_CC",strCC);
	m_lstData.setData(0,"BEM_BCC",strBCC);

	
	//set datetime:
	m_lstData.setData(0,"BEM_RECV_TIME",QDateTime::currentDateTime());



	//-----------------------------------------------
	//			WRITE TO DB
	//-----------------------------------------------
	DbRecordSet lstUAR,lstGAR;
	_SERVER_CALL(BusEmail->Write(err,m_lstData,m_lstContactLink,rowTask,m_lstAttachments,m_strLockedRes, lstUAR,lstGAR))
}



DbRecordSet *EMailDialog::GetTaskRecordSet()
{
	return &(ui.frameTask->m_recTask);
}

void  EMailDialog::SetSerialMailingMode(bool bUseAllContactEmails)
{
	m_bSerialEmailMode = true;
	m_bSerialUseAllContactEmails = bUseAllContactEmails;

	//force direct mailing mode
	ui.radDirectMail->setChecked(true);
	ui.radExternalMail->setEnabled(false);
}

void EMailDialog::SetEmailData(DbRecordSet &recData)
{
	Q_ASSERT(recData.getRowCount() == 1);

	if(recData.getRowCount() > 0){
		m_lstAttachments = recData.getDataRef(0, "ATTACHMENTS").value<DbRecordSet>();
		m_lstContactLink = recData.getDataRef(0, "CONTACTS").value<DbRecordSet>();
		m_lstData.clear();
		m_lstData.merge(recData);

		//TO:
		QString strTmp = recData.getDataRef(0, "BEM_TO").toString();
		QStringList list = strTmp.split(";", QString::SkipEmptyParts);
		int i;
		for(i=0; i<list.size(); i++){
			m_recToRecordSet.addRow();
			int nIdx = m_recToRecordSet.getRowCount() - 1;

			//split name + email
			QString strEntry = list[i];
			QString strName;
			int nPos = strEntry.indexOf("<");
			if(nPos > 0){
				strName  = strEntry.left(nPos);
				strEntry = strEntry.right(strEntry.length()-nPos-1);
			}
			nPos = strEntry.indexOf(">");
			if(nPos > 0)
				strEntry = strEntry.left(nPos);

			m_recToRecordSet.setData(nIdx, "CONTACT_EMAIL", strEntry);
			m_recToRecordSet.setData(nIdx, "CONTACT_NAME", strName);
			m_recToRecordSet.setData(nIdx, "RECIPIENT_TYPE", 0);
		}

		//CC
		strTmp = recData.getDataRef(0, "BEM_CC").toString();
		list = strTmp.split(";", QString::SkipEmptyParts);
		for(i=0; i<list.size(); i++){
			m_recCCRecordSet.addRow();
			int nIdx = m_recCCRecordSet.getRowCount() - 1;

			//split name + email
			QString strEntry = list[i];
			QString strName;
			int nPos = strEntry.indexOf("<");
			if(nPos > 0){
				strName  = strEntry.left(nPos);
				strEntry = strEntry.right(strEntry.length()-nPos-1);
			}
			nPos = strEntry.indexOf(">");
			if(nPos > 0)
				strEntry = strEntry.left(nPos);

			m_recCCRecordSet.setData(nIdx, "CONTACT_EMAIL", strEntry);
			m_recCCRecordSet.setData(nIdx, "CONTACT_NAME", strName);
			m_recCCRecordSet.setData(nIdx, "RECIPIENT_TYPE", 1);
		}

		//BCC
		strTmp = recData.getDataRef(0, "BEM_BCC").toString();
		list = strTmp.split(";", QString::SkipEmptyParts);
		for(i=0; i<list.size(); i++){
			m_recBCCRecordSet.addRow();
			int nIdx = m_recBCCRecordSet.getRowCount() - 1;

			//split name + email
			QString strEntry = list[i];
			QString strName;
			int nPos = strEntry.indexOf("<");
			if(nPos > 0){
				strName  = strEntry.left(nPos);
				strEntry = strEntry.right(strEntry.length()-nPos-1);
			}
			nPos = strEntry.indexOf(">");
			if(nPos > 0)
				strEntry = strEntry.left(nPos);

			m_recBCCRecordSet.setData(nIdx, "CONTACT_EMAIL", strEntry);
			m_recBCCRecordSet.setData(nIdx, "CONTACT_NAME", strName);
			m_recBCCRecordSet.setData(nIdx, "RECIPIENT_TYPE", 2);
		}

		EmailHelper::UnpackCIDToTemp(m_lstCIDInfo,m_lstData,m_lstAttachments);
		ui.listAttachemnts->SetData(m_lstAttachments);
		ui.Address_tableWidget->AppendContacts(m_recToRecordSet, m_recCCRecordSet, m_recBCCRecordSet);
		RefreshDisplay();

		//should we disable switching to RTF editor (allowing only editing of raw HTML)
		if(m_lstData.getDataRef(0,"BEM_FIX_HTML").toInt() > 0){
			if(!ui.TextEdit_widget->IsHTMLDisplayed()){
				ui.TextEdit_widget->m_bEmitSignal = false;
				ui.TextEdit_widget->SwitchFromHTMLToNormalView();	//bad name, in this case it switches to HTML
				ui.TextEdit_widget->m_bEmitSignal = true;
			}
			on_chkFixHTML_clicked();
		}
		else
			ui.chkFixHTML->hide();	//not visible when outside of "raw HTML" editor

		qDebug() << "Email RTF widget switched to HTML view #3:" << ui.TextEdit_widget->IsHTMLDisplayed();
	}
}



void EMailDialog::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if(nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}

	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);

}

void EMailDialog::OnThemeChanged()
{
	ui.labelDocType->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	ui.frame->setStyleSheet("QFrame#frame "+ThemeManager::GetSidebarChapter_Bkg());
}

void EMailDialog::changeEvent(QEvent * event)
{
	QWidget::changeEvent(event);

	if(event->type() == QEvent::WindowStateChange)
	{
		//now check to see if the window is being minimised
        if( isMinimized() )
		{
			//only for tasks
			Status status;
			ui.frameTask->GetDataFromGUI();
			bool bTaskActive = ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0;
			if(bTaskActive)
			{
				if(NULL == m_pWndAvatar)
				{
					//calculate initial position for the new window
					QPoint pt = geometry().topRight();
					pt.setX(pt.x() - 169);

					m_pWndAvatar = new fui_avatar(AVATAR_TASK_EMAIL, this);
					m_pWndAvatar->move(pt);
				}

				RefreshAvatarData();
				m_pWndAvatar->show();

				if (m_bSideBarModeView)
					g_objFuiManager.ClearActiveFUI(this);

				if(m_bHideOnMinimize)
					QTimer::singleShot(0,this,SLOT(hide()));//B.T. delay hide
				m_bHideOnMinimize = true; //reset
			}
		}
	}
}

void EMailDialog::closeEvent(QCloseEvent *event)
{
	FuiBase::closeEvent(event);

	if(m_pWndAvatar){
		m_pWndAvatar->close();
		m_pWndAvatar = NULL;
	}
}

QString EMailDialog::GetTaskDate(int nCEType, DbRecordSet &recEntityRecord, DbRecordSet &recTaskRecord, int nRow/*=0*/)
{
	//calc task date - copied from commgridview.cpp:1360
	QDateTime datFirstRow;

	//If not tasked then find appropriate strings. 
	int nTask = CommGridViewHelper::CheckTask(recTaskRecord, nRow);
	if( nTask == CommGridViewHelper::NOT_SCHEDULED || 
		(!recTaskRecord.getDataRef(nRow, "BTKS_ID").isNull() && (recTaskRecord.getDataRef(nRow, "BTKS_IS_TASK_ACTIVE").toInt() == 0  || recTaskRecord.getDataRef(nRow, "BTKS_SYSTEM_STATUS").toInt() >= 3)))
	{
		if (nCEType == CommGridViewHelper::VOICECALL)
		{
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BVC_START").toDateTime();
		}
		else if (nCEType == CommGridViewHelper::EMAIL)
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BEM_RECV_TIME").toDateTime();
		else
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BDMD_DAT_LAST_MODIFIED").toDateTime();
	}
	else	//Else write start time, due time and who tasked it.
	{
		if(nTask == CommGridViewHelper::SCHEDULED_OVERDUE ||
			nTask == CommGridViewHelper::SCHEDULED_DUENOW)
		{
			//issue #2116 return due date for due/overdue tasks
			datFirstRow	 = recTaskRecord.getDataRef(nRow, "BTKS_DUE").toDateTime();
		}
		else
		{
			//recEntityRecord.Dump();
			datFirstRow	 = recTaskRecord.getDataRef(nRow, "BTKS_START").toDateTime();
		}
	}

	QString strFirstRow	= datFirstRow.toString("d.M.yyyy h:mm");
	return strFirstRow;
}

QString EMailDialog::GetPersonInitials(int nPersonID)
{
	QString res;

	DbRecordSet lstData;
	lstData.addColumn(QVariant::String, "BPER_INITIALS");
	QString strSql = QString("SELECT BPER_INITIALS FROM BUS_PERSON WHERE BPER_ID=%1") .arg(nPersonID);

	Status err;
	_SERVER_CALL(ClientSimpleORM->ExecuteSQL(err, strSql, lstData));
	if (err.IsOK()){
		if(lstData.getRowCount() > 0)
			res = lstData.getDataRef(0, 0).toString();
	}

	return res;
}

QString EMailDialog::GetTaskAssignedContact(int nContactID)
{
	QString res;
	DbRecordSet lstData;
	QString strSql = QString("SELECT BCNT_FIRSTNAME,BCNT_LASTNAME, BCNT_ORGANIZATIONNAME FROM BUS_CM_CONTACT WHERE BCNT_ID=%1") .arg(nContactID);

	Status err;
	_SERVER_CALL(ClientSimpleORM->ExecuteSQL(err, strSql, lstData));
	if (err.IsOK()){
		if(lstData.getRowCount() > 0){
			//first name
			res = lstData.getDataRef(0, 0).toString();
			if(!res.isEmpty()) res += " ";
			//last name
			res += lstData.getDataRef(0, 1).toString();

			//organization in a separate line
			if(!res.isEmpty() && !lstData.getDataRef(0, 2).toString().isEmpty()) res += "\n";
			res += lstData.getDataRef(0, 2).toString();
		}
	}

	return res;
}

int EMailDialog::GetTaskID()
{
	if(ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0)
		return ui.frameTask->m_recTask.getDataRef(0, "BTKS_ID").toInt();

	return -1;
}

void EMailDialog::RefreshAvatarData(bool bReload)
{
	if(bReload){
		QString strWhere="WHERE BTKS_ID="+QVariant(GetTaskID()).toString();
		Status status;
		_SERVER_CALL(ClientSimpleORM->Read(status, BUS_TASKS, ui.frameTask->m_recTask, strWhere));
		ui.frameTask->SetGUIFromData();
	}

	QString strTxt = ui.frameTask->m_recTask.getDataRef(0, "BTKS_SUBJECT").toString();
	QString strContact;
	int nRows = m_lstContactLink.getRowCount();
	if(nRows > 0){
		//m_lstContactLink.Dump();
		int nIdx = -1;
		int nRole = (m_lstData.getDataRef(0,"BEM_OUTGOING").toInt() > 0) ? GlobalConstants::CONTACT_LINK_ROLE_TO : GlobalConstants::CONTACT_LINK_ROLE_FROM;
		for(int i=0; i<nRows; i++)
		{
			if(nRole == m_lstContactLink.getDataRef(i, "BNMR_SYSTEM_ROLE").toInt())
			{
				strContact = EMailDialog::GetTaskAssignedContact(m_lstContactLink.getDataRef(i, "BNMR_TABLE_KEY_ID_2").toInt());
				break;
			}
		}
	}
	QString strLabel = strContact;
	strLabel += "\n";
	strLabel += strTxt;
	m_pWndAvatar->SetLabel(strLabel);

	QString strDesc = ui.frameTask->m_recTask.getDataRef(0, "BTKS_DESCRIPTION").toString();
	m_pWndAvatar->SetTaskDescTooltip(strDesc);

	QString strDate = EMailDialog::GetTaskDate(CommGridViewHelper::EMAIL, m_lstData, ui.frameTask->m_recTask);
	m_pWndAvatar->SetTaskDate(strDate);

	QString strOwner = EMailDialog::GetPersonInitials(ui.frameTask->m_recTask.getDataRef(0, "BTKS_OWNER").toInt());
	m_pWndAvatar->SetTaskOwnerInitials(strOwner);

	int nTask = CommGridViewHelper::CheckTask(ui.frameTask->m_recTask);
	m_pWndAvatar->SetTaskState(nTask);

	m_pWndAvatar->SetRecord(m_nEntityRecordID);

	m_pWndAvatar->SetTaskID(ui.frameTask->m_recTask.getDataRef(0, "BTKS_ID").toInt());
}


void EMailDialog::OnEditor_VerticalOpenClose_clicked(bool bSmaller)
{
	if (!bSmaller)
	{
		ui.Address_tableWidget->setVisible(false);
		ui.AddContacts_pushButton->setVisible(false);
		ui.btnAddAttachment->setVisible(false);
		ui.listAttachemnts->setVisible(false);
		ui.label->setVisible(false);
		ui.Subject_lineEdit->setVisible(false);
		ui.radExternalMail->setVisible(false);
		ui.radDirectMail->setVisible(false);
		
		ui.chkPlainText->setVisible(false);
		ui.labelOutlook->setVisible(false);
		//ui.xxx_spacer->setVisible(false);
		ui.label_10->setVisible(false);
		ui.frameCategory->setVisible(false);
		//ui.grupIsTemplate->setVisible(false);
		//ui.frameTask->setVisible(false);
		ui.tabWidget->setTabEnabled(1,false);
		ui.tabWidget->setTabEnabled(2,false);
	}
	else
	{
		ui.Address_tableWidget->setVisible(true);
		ui.AddContacts_pushButton->setVisible(true);
		ui.btnAddAttachment->setVisible(true);
		ui.listAttachemnts->setVisible(true);
		ui.label->setVisible(true);
		ui.Subject_lineEdit->setVisible(true);
		ui.radExternalMail->setVisible(true);
		ui.radDirectMail->setVisible(true);

		ui.chkPlainText->setVisible(true);
		ui.labelOutlook->setVisible(true);
		//ui.xxx_spacer->setVisible(true);
		ui.label_10->setVisible(true);
		ui.frameCategory->setVisible(true);
		//ui.grupIsTemplate->setVisible(true);
		//ui.frameTask->setVisible(true);
		ui.tabWidget->setTabEnabled(1,true);
		ui.tabWidget->setTabEnabled(2,true);
	}
}

void EMailDialog::ShowMailSendRadios(bool bShow)
{
	ui.radExternalMail->setVisible(bShow);
	ui.radDirectMail->setVisible(bShow);
}

void EMailDialog::SetEmailData(QString strSubject, QString strBody,int nOwnerID,QString strOwnerEmailFrom)
{
	ui.Subject_lineEdit->setText(strSubject);
	ui.TextEdit_widget->GetTextWidget()->blockSignals(true);
	ui.TextEdit_widget->SetPlainText(strBody);
	ui.TextEdit_widget->GetTextWidget()->blockSignals(false);
	m_lstData.setData(0,"BEM_EMAIL_TYPE", GlobalConstants::EMAIL_TYPE_PLAIN_TEXT);
	//m_lstData.setData(0,"BEM_BODY").toString());

	if (nOwnerID>0)
	{
		m_strPersonContactID=QString::number(nOwnerID);
		//Add it to form.
		if (m_lstData.getRowCount()>0)
		{
			m_lstData.setData(0,"BEM_FROM",EmailHelperCore::GetFullEmailAddress("",strOwnerEmailFrom,false));
		}
	}

	//should we disable switching to RTF editor (allowing only editing of raw HTML)
	if(m_lstData.getDataRef(0,"BEM_FIX_HTML").toInt() > 0){
		if(!ui.TextEdit_widget->IsHTMLDisplayed()){
			ui.TextEdit_widget->m_bEmitSignal = false;
			ui.TextEdit_widget->SwitchFromHTMLToNormalView();	//bad name, in this case it switches to HTML
			ui.TextEdit_widget->m_bEmitSignal = true;
		}
		on_chkFixHTML_clicked();
	}
	else
		ui.chkFixHTML->hide();	//not visible when outside of "raw HTML" editor

	qDebug() << "Email RTF widget switched to HTML view #4:" << ui.TextEdit_widget->IsHTMLDisplayed();
}


void EMailDialog::FillNamesInToCCBCCRecords()
{
	//BT fix: get names:
	DbRecordSet lstToNames;
	DbRecordSet lstBCCNames;
	DbRecordSet lstCCNames;
	m_ContactCache.GetEntityRecords(m_recToRecordSet,m_recToRecordSet.getColumnIdx("CONTACT_ID"),lstToNames);
	m_ContactCache.GetEntityRecords(m_recBCCRecordSet,m_recBCCRecordSet.getColumnIdx("CONTACT_ID"),lstBCCNames);
	m_ContactCache.GetEntityRecords(m_recCCRecordSet,m_recCCRecordSet.getColumnIdx("CONTACT_ID"),lstCCNames);

	int nRowCount = m_recToRecordSet.getRowCount();
	int nNameCnt=lstToNames.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		if (i<nNameCnt)
			m_recToRecordSet.setData(i, "CONTACT_NAME",	lstToNames.getDataRef(i, "BCNT_NAME"));
	}
	nRowCount = m_recBCCRecordSet.getRowCount();
	nNameCnt=lstBCCNames.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		if (i<nNameCnt)
			m_recBCCRecordSet.setData(i, "CONTACT_NAME",	lstBCCNames.getDataRef(i, "BCNT_NAME"));
	}
	nRowCount = m_recCCRecordSet.getRowCount();
	nNameCnt=lstCCNames.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		if (i<nNameCnt)
			m_recCCRecordSet.setData(i, "CONTACT_NAME",	lstCCNames.getDataRef(i, "BCNT_NAME"));
	}
}

void EMailDialog::on_chkFixHTML_clicked()
{
	if(ui.chkFixHTML->isChecked())
	{
		//disable switch to RTF editor, only raw HTML editor can be used
		ui.TextEdit_widget->GetShowHtmlButton()->setEnabled(false);
		ui.TextEdit_widget->EnableHtmlFixing(false);	//do not fix HTML in "fix(ate) HTML" mode
	}
	else
	{
		ui.TextEdit_widget->GetShowHtmlButton()->setEnabled(true);
		ui.TextEdit_widget->EnableHtmlFixing(true);
	}
}

//"Fix HTML" check box is visible only when the RTF widget is in "Raw HTML" editor mode
void EMailDialog::OnEditViewSwitched()
{
	if(ui.TextEdit_widget->IsHTMLDisplayed())
		ui.chkFixHTML->show();
	else
		ui.chkFixHTML->hide();
}

void EMailDialog::FixHtmlViewLater()
{
	if(m_lstData.getRowCount()>0)
	{
		if(m_lstData.getDataRef(0,"BEM_FIX_HTML").toInt() > 0){
			ui.chkFixHTML->setChecked(true);
			ui.TextEdit_widget->EnableHtmlFixing(false);	//do not fix HTML in "fix(ate) HTML" mode
			if(!ui.TextEdit_widget->IsHTMLDisplayed()){
				ui.TextEdit_widget->m_bEmitSignal = false;
				ui.TextEdit_widget->SwitchFromHTMLToNormalView();	//bad name, in this case it switches to HTML
				ui.TextEdit_widget->m_bEmitSignal = true;
			}
			on_chkFixHTML_clicked();

			QString strBody = m_lstData.getDataRef(0,"BEM_BODY").toString();
			ui.TextEdit_widget->SetPlainText(strBody);
			ui.TextEdit_widget->m_bTextIsHTML = true;
			ui.TextEdit_widget->m_bHTMLIsDisplayed = true;
			ui.TextEdit_widget->RefreshHtmlHighlight();

			qDebug() << "Email RTF widget switched to HTML view #5:" << ui.TextEdit_widget->IsHTMLDisplayed();
		}
		else{
			ui.chkFixHTML->setChecked(false);
			ui.TextEdit_widget->EnableHtmlFixing(true);
			ui.chkFixHTML->hide();	//not visible when outside of "raw HTML" editor
		}
	}
}


void EMailDialog::updateTaskTabCheckbox()
{
	//customize 2nd tab title to simulate icon
	ui.tabWidget->setTabIcon(1, (ui.frameTask->isChecked())? QIcon(":/checkbox_checked.png") : QIcon(":/checkbox_unchecked.png"));
}

void EMailDialog::updateTemplateTabCheckbox()
{
	//customize 2nd tab title to simulate icon
	ui.tabWidget->setTabIcon(2, (ui.grupIsTemplate->isChecked())? QIcon(":/checkbox_checked.png") : QIcon(":/checkbox_unchecked.png"));
}

void EMailDialog::RemoveAvatar(int nType, int nRecordID)
{
	QString strAvatars = g_pSettings->GetPersonSetting(AVATAR_STARTUP_LIST).toString();
	//this is to reduce "losing last avatar" effects, caused by strAvatarsNew code bug
	if(!strAvatars.isEmpty() && !strAvatars.endsWith(";")) 
		strAvatars += ";";	//MR: list must be terminated

	if(!strAvatars.isEmpty())
	{
		bool bModified = false;
		QString strAvatarsNew;
		int nPos = strAvatars.indexOf(";");
		while(nPos >= 0)
		{
			QString strSegment = strAvatars.mid(0, nPos);
			if(!strSegment.isEmpty())
			{
				//decode avatar data "avatar_type,record_id:left,top"
				int nPos1 = strSegment.indexOf(",");
				if(nPos1 > 0)
				{
					int nEntityType=-1;
					int nCurType  = strSegment.mid(0, nPos1).toInt();
					int nLeft  = 0;
					int nTop   = 0;
					int nRecID = -1;
					int nPos2  = strSegment.indexOf(":", nPos1+1);
					if(nPos2 >= 0){
						nRecID = strSegment.mid(nPos1+1, nPos2-nPos1-1).toInt();
						int nPos3 = strSegment.indexOf(",", nPos2+1);
						if(nPos3 >= 0){
							nLeft  = strSegment.mid(nPos2+1, nPos3-nPos2-1).toInt();
							nTop   = strSegment.mid(nPos3+1).toInt();
						}
					}
													
					if( nType == nCurType &&
						nRecordID == nRecID)
					{
						//avatar match found, remove it
						bModified = true;
					}
					else{
						//keep the avatar
						strAvatarsNew=strAvatarsNew+";"+strSegment;
					}
				}
			}

			//keep going
			strAvatars.remove(0, nPos+1);
			nPos = strAvatars.indexOf(";");
		}

		//save new avatar settings:
		if(bModified){
			if(!strAvatarsNew.isEmpty()) strAvatarsNew += ";";	//MR: list must be terminated
			g_pSettings->SetPersonSetting(AVATAR_STARTUP_LIST,strAvatarsNew);
		}
	}
}
