#ifndef SIDEBARFUI_DESKTOP_H
#define SIDEBARFUI_DESKTOP_H

#include "sidebarfui_base.h"
#include "generatedfiles/ui_sidebarfui_desktop.h"

class SideBarFui_Desktop : public SideBarFui_Base, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	SideBarFui_Desktop(QWidget *parent = 0);
	~SideBarFui_Desktop();

	void SetCurrentFUI(bool bCurrent);
	bool HideVisibleFramesIfNeeded(int nFrameOpened /* 0 -ACL, 1 -AC, 2-DP */){return true;};
	
	//bool sidebar_fui(){return true;};
	//void Setsidebar_fui(bool bSet){};

protected:
	QFrame* GetFrameCommToolBar();
	QFrame* GetFrameCommParent();
	QLabel* GetFrameCommLabel();
	DropZone_MenuWidget* GetDropZone_MenuWidget();
	void GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strMail,int &nEmailReplyID,QString &strPhone,QString &strInternet);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

protected slots:
	void BuildMenu();	

private:
	void OnThemeChanged();
	Ui::SideBarFui_DesktopClass ui;
};

#endif // SIDEBARFUI_DESKTOP_H
