#ifndef FUI_CalendarViewEditor_H
#define FUI_CalendarViewEditor_H

#include <QWidget>
#include "ui_fui_CalendarViewEditor.h"
#include "common/common/dbrecordset.h"
#include "fuibase.h"

class FUI_CalendarViewEditor : public FuiBase
{
    Q_OBJECT

public:
    FUI_CalendarViewEditor(QWidget *parent = 0);
    ~FUI_CalendarViewEditor();

private:
	Ui::FUI_CalendarViewEditorClass ui;

protected:
	QFrame *m_pSelectorWidget;
	DbRecordSet m_recCalViewTypes;

	static void GetDataFromEntity(int nEntityCode, int &nEntityType, QString &strIdField, QString &strNameField, QString &strPersonIdField);
	void RemoveCalendarViewItemTypes(int nEntityCode);
	void AddCalendarViewItemTypes(int nEntityCode);
	void AddEntitiesToCalendarView(DbRecordSet recEntities, int nCalViewID);
	void RemoveEntitiesFromCalendarView(DbRecordSet recEntities, int nCalViewID);
	bool GetProjectList(QString strProjectList, DbRecordSet &recListData, QString &strErrMsg);
	bool GetContactList(QString strContactList, DbRecordSet &recListData, QString &strErrMsg);
	bool GetProjectEntityList(QString strProjectList, DbRecordSet &recEntities, QString &strErrMsg);
	bool GetContactEntityList(QString strContactList, DbRecordSet &recEntities, QString &strErrMsg);
	void RepackToEntityList(const DbRecordSet &recListData, DbRecordSet &recEntities, bool bProjectList);
	int	 GetCalendarViewID(QString strName, QString &strErrMsg);

	void ExecuteScript(QString strScriptContent);

	//internal script implementation
	bool CalendarView_Create(QString &strErrMsg, QString strName, int nTimePeriodCode, QString strCalViewType, bool bShowOverviewCalendar, bool bShowDetailCalendar, bool bIsPublic);
	bool CalendarView_Delete(QString &strErrMsg, QString strName);
	bool CalendarView_AddProjects(QString &strErrMsg, QString strName, QString strProjectList);
	bool CalendarView_AddContacts(QString &strErrMsg, QString strName, QString strContactList);
	bool CalendarViewGroup_CreateFromProjects(QString &strErrMsg, QString strNamePrefix, int nTimePeriodCode, QString strCalViewType, bool bShowOverviewCalendar, bool bShowDetailCalendar, bool bIsPublic, QString strProjectList);
	bool CalendarViewGroup_CreateFromContacts(QString &strErrMsg, QString strNamePrefix, int nTimePeriodCode, QString strCalViewType, bool bShowOverviewCalendar, bool bShowDetailCalendar, bool bIsPublic, QString strContactList);
	bool CalendarViewGroup_Delete(QString &strErrMsg, QString strNamePrefix);
	bool CalendarViewGroup_AddProjects(QString &strErrMsg, QString strNamePrefix, QString strProjectList);
	bool CalendarViewGroup_AddContacts(QString &strErrMsg, QString strNamePrefix, QString strContactList);

private slots:
	void on_btnWriteToCalendarViews_clicked();
    void on_btnCreateCalendarView_clicked();
	void on_btnDeleteSelectedViews_clicked();
	void on_btnAddProjects_clicked();
    void on_btnRemoveProjects_clicked();
    void on_btnAddContacts_clicked();
    void on_btnRemoveContacts_clicked();
    void on_btnAddResources_clicked();
    void on_btnRemoveResources_clicked();
    void on_btnRemoveUsers_clicked();
    void on_btnAddUsers_clicked();
	void on_btnMoveUp_clicked();
	void on_btnMoveDown_clicked();
	void OnViewSelectionChanged();
	void on_btnRegenerateViewsAnesthesists_clicked();

protected:
	void OnViewListModified();
};

#endif // FUI_CalendarViewEditor_H
