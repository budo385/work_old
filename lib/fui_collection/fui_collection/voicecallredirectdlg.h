#ifndef VOICECALLREDIRECTDLG_H
#define VOICECALLREDIRECTDLG_H

#include <QtWidgets/QDialog>
#include "ui_voicecallredirectdlg.h"
#include "common/common/observer_ptrn.h"

class VoiceCallRedirectDlg : public QDialog, public ObsrPtrn_Observer
{
    Q_OBJECT

public:
    VoiceCallRedirectDlg(QWidget *parent = 0);
    ~VoiceCallRedirectDlg();

	QString m_strNumber;
	void LoadPhoneCombo(DbRecordSet &lstPhones);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

private:
    Ui::VoiceCallRedirectDlgClass ui;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnExtSearch_clicked();
	void LoadContactData();
	void OnShowContactDetails(int nContactID);
};

#endif // VOICECALLREDIRECTDLG_H
