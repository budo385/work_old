#include "table_contactsgroup.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "dlg_gridviews.h"
#include "gui_core/gui_core/gui_helper.h"
#include "common/common/datahelper.h"
#include "fuibase.h"
#include "gui_core/gui_core/dlg_enterperiod.h"
#include "fui_contacts.h"
#include <QProgressDialog>
#include "bus_core/bus_core/mainentityfilter.h"
#include "db_core/db_core/dbsqltabledefinition.h"


//GLOBALS:
#include "common/common/cliententitycache.h"
#include "bus_interface/bus_interface/businessservicemanager.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
extern ClientEntityCache g_ClientCache;				//global cache
extern BusinessServiceManager *g_pBoSet;		//Bo set
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;


Table_ContactsGroup::Table_ContactsGroup(QWidget *parent)
:UniversalTableWidgetEx(parent)
{
	m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">";
	m_strEndHtmlTag="</span></p></body></html>";
}

Table_ContactsGroup::~Table_ContactsGroup()
{

}

//called after dark
void Table_ContactsGroup::Initialize(QWidget *pContactFUI,Selection_GroupTree* pSelectionGrouTree,QComboBox *pComboViews,QPushButton *btnEditView,Table_SubContactData *pTableSub,QSplitter *splitterSub,QPushButton *btnRefresh,QLabel *labelGroupName,QCheckBox *pbtnShowActive)
{

	m_nCurrentGroupID=-1;
	m_bIsCurrentGroupCumulated=false;

	m_pLabelGroupName=labelGroupName;
	m_btnRefresh=btnRefresh;

	m_pbtnShowActive=pbtnShowActive;
	m_pContactFUI=pContactFUI;
	m_pSelectionGrouTree=pSelectionGrouTree;
	Q_ASSERT(m_pSelectionGrouTree);
	Q_ASSERT(m_pContactFUI);

	m_nGridID=Dlg_GridViews::GRID_GROUP_CONTACT_GRID;

	//init data source:
	//MainEntitySelectionController::Initialize(ENTITY_BUS_CONTACT);	//init data source
	m_lstSubListData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	m_lstData.copyDefinition(m_lstSubListData);
	m_lstSubListData.addRow();

	//set data source for table, enable drop
	UniversalTableWidgetEx::Initialize(&m_lstData);
	EnableDrag(true,ENTITY_BUS_CONTACT,":Contacts32.png");				//drop type is full contact list
	EnableDrop(true,ENTITY_BUS_CONTACT);
	SetEditMode(true); //to accept drops?

	//controls:
	m_pComboViews=pComboViews;
	m_btnEditView=btnEditView;
	m_pTableSub=pTableSub;
	m_splitterSub=splitterSub;

	//set sub grid:
	m_pTableSub->Initialize(&m_lstSubListData,NULL,true,true,100);
	m_pTableSub->verticalHeader()->hide();
	
	m_btnEditView->setIcon(QIcon(":SAP_Select.png"));
	m_btnEditView->setToolTip(tr("Modify View"));

	//redefine table look, load views:
	//COMBO:
	m_ComboHandler.Initialize(ENTITY_BUS_OPT_GRID_VIEWS);
	m_ComboHandler.SetDataForCombo(m_pComboViews,"BOGW_VIEW_NAME");
	m_ComboHandler.SetDistinctDisplay();
	m_ComboHandler.GetLocalFilter()->SetFilter("BOGW_GRID_ID",m_nGridID);
	bool bOK=m_ComboHandler.ReloadData();							
	//if(!bOK) return false;		

	m_pbtnShowActive->setChecked(true);	//always checked

	//connect signals:
	//m_pComboViews->setCurrentIndex(-1); //force reload on index change
	connect(m_pComboViews,SIGNAL(currentIndexChanged(int)),this,SLOT(on_ViewChanged(int)));
	connect(m_btnEditView,SIGNAL(clicked()),this,SLOT(OpenViewEditor()));
	connect(m_btnRefresh,SIGNAL(clicked()),this,SLOT(OnRefreshContent()));
	connect(m_pbtnShowActive,SIGNAL(clicked()),this,SLOT(OnRefreshContent()));
	connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnShowDetails()));
	connect(this,SIGNAL(SignalSelectionChanged()),this,SLOT(OnSelectionChanged()));
	connect(this,SIGNAL(SignalDataDroped(int,DbRecordSet &,QDropEvent *)),this,SLOT(OnDataDroped(int,DbRecordSet &,QDropEvent *)));
	
	//----------------------------------------------------------
	//					GROUP OPERATIONS
	//----------------------------------------------------------

	m_pSelectionGrouTree->registerObserver(this);
	m_pLabelGroupName->setText("");

	//----------------------------------------------------------
	//					CNTX MENU
	//----------------------------------------------------------

	//create Cnxt Menu actions:
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions; 

	pAction = new QAction(tr("Show Contact Details"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDetails()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Show Contact Details In New Window"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDetailsNewWindow()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete Selected Contacts From Database"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDelete()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("Remove All Contacts From Group"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnRemoveContactsAll()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Remove Selected Contacts From Group"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnRemoveContacts()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Add Selected Contacts To Actual Contact List"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnAddContactsToActual()));
	lstActions.append(pAction);


	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("Enter Period In Which Contact Is Active In Group"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnEnterPeriod()));
	lstActions.append(pAction);

	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("&Select All"), this);
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), this, SLOT(SelectAll()));
	lstActions.append(pAction);

	//Sort
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), this, SLOT(OpenSortDialog()));

	lstActions.append(pAction);
	this->addAction(pAction);


	SetContextMenuActions(lstActions);

	//---------------------------LOAD VIEW FROM SETTINGS---------------------------
	QString nGridView=g_pSettings->GetPersonSetting(CONTACT_DEF_GROUP_LIST_VIEW).toString();
	if (nGridView.isEmpty())
		nGridView=g_pSettings->GetApplicationOption(APP_CONTACT_DEF_GROUP_LIST_VIEW).toString();


	m_pComboViews->blockSignals(true);
	m_ComboHandler.SetCurrentIndexFromName(nGridView);
	on_ViewChanged(m_pComboViews->currentIndex());
	m_pComboViews->blockSignals(false);
	
}

//table changed state test it
void Table_ContactsGroup::OnSelectionChanged()
{
	//refresh entry if one row is selected
	if(m_plstData->getSelectedCount()==1)
	{
		int nCurrentRow=m_plstData->getSelectedRow();
		DbRecordSet row = m_plstData->getRow(nCurrentRow);
		m_lstSubListData.assignRow(0,row);
	}
	else
	{
		m_lstSubListData.clear();
		m_lstSubListData.addRow();
	}

	m_pTableSub->RefreshDisplay();

}

void Table_ContactsGroup::OpenViewEditor()
{

	QString strViewName=m_pComboViews->currentText();
	Dlg_GridViews dlg;
	dlg.Initialize(strViewName,m_nGridID);

	if(0 != dlg.exec())
	{
		strViewName=dlg.GetCurrentViewName();
		//set to new view, block signals, coz we have manual reload
		m_pComboViews->blockSignals(true);
		m_ComboHandler.SetCurrentIndexFromName(strViewName);
		m_pComboViews->blockSignals(false);
	}
	//whatever always reload
	on_ViewChanged(m_pComboViews->currentIndex());

}

//when user changes view, destroy grid, rebuild, if -1, use default view from contactmanager
void Table_ContactsGroup::on_ViewChanged(int nIndex)
{
	DbRecordSet lstSetup;
	DbRecordSet lstSetupSub;
	lstSetup.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_GRID_COLUMN_SETUP));
	lstSetupSub.copyDefinition(lstSetup);


	if (nIndex>=0)
	{
		//extract all setup cols from combo:
		QString strViewName=m_pComboViews->currentText();
		DbRecordSet *pData=m_ComboHandler.GetDataSource();
		//pData->Dump();
		pData->find(pData->getColumnIdx("BOGW_VIEW_NAME"),strViewName);
		lstSetup.merge(*pData,true);
	}
	else
	{
		ClientContactManager::GetDefaultContactGridSetup(lstSetup);
	}

	//lstSetup.Dump();

	//rebuild d' grid:
	//lstSetup.Dump();
	int nColNameIdx=lstSetup.getColumnIdx("BOGW_COLUMN_NAME");
	int nColPos=lstSetup.getColumnIdx("BOGW_COLUMN_POSITION");
	lstSetup.sort(nColPos);	//sort by name: bcnt's will go first LST_ last...
	int nSize=lstSetup.getRowCount();
	//int nSubListStart=nSize;
	lstSetup.clearSelection();
	for (int i=0;i<nSize;++i)
	{
		if(lstSetup.getDataRef(i,nColNameIdx).toString().indexOf("LST_")==0)
		{
			lstSetupSub.addRow();
			DbRecordSet row = lstSetup.getRow(i);
			lstSetupSub.assignRow(lstSetupSub.getRowCount()-1,row);
			lstSetup.selectRow(i);
		}
	}
	lstSetup.deleteSelectedRows();

	//mark all cols nonedit:
	lstSetup.setColValue(lstSetup.getColumnIdx("BOGW_EDITABLE"),false);


	//if no sublists, hide down table, set splitter:
	if (lstSetupSub.getRowCount()==0)
		m_pTableSub->setVisible(false);
	else
		m_pTableSub->setVisible(true);

	//reassign lists, hide if needed
	SetColumnSetup(lstSetup);
	m_pTableSub->SetColumnSetup(lstSetupSub);


	//sort:
	SortMultiColumn(lstSetup);
}



//for group selector (only he can pop up):
//re
void Table_ContactsGroup::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	Status err;

	//if (pSubject!=m_pSelectionGrouTree)return;
	
	DbRecordSet lstData=val.value<DbRecordSet>();
	if (lstData.getRowCount()==0) return;

	

	//if selection change:
	if (nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		//if trying to read same group
		if (m_nCurrentGroupID==nMsgDetail && ((lstData.getRowCount()>1 && m_bIsCurrentGroupCumulated) ||(lstData.getRowCount()==1 && !m_bIsCurrentGroupCumulated)) )
			return;

		FUI_Contacts *pFui=dynamic_cast<FUI_Contacts*>(m_pContactFUI);
		if (pFui) 
		{
			if(pFui->GetMainTabWidget()->currentIndex()!=1)
			{
				pFui->GetMainTabWidget()->setCurrentIndex(1);
			}
		}


		m_nCurrentGroupID=nMsgDetail;
		if (lstData.getRowCount()>1)
			m_bIsCurrentGroupCumulated=true;
		else
			m_bIsCurrentGroupCumulated=false;

		//_DUMP(lstData);

		m_lstCurrentGroups=lstData;
		//m_lstCurrentGroups.Dump();


		//clear lists
		//m_lstDataOriginal.clear();
		m_lstData.clear();

		//can be invalid: -1:
		if(m_nCurrentGroupID!=-1)
		{
			//read all NM assoc table:
			DbRecordSet lstNewData;
			_SERVER_CALL(BusGroupTree->ReadGroupContent(err,ENTITY_BUS_CONTACT,m_lstCurrentGroups,lstNewData))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			ReadGroupContent(lstNewData);		//da big read

		}
		
		//UpdateListContentIfAccumulated();
		RefreshDisplay();
		

	}


	//ignore other messages:
	if (nMsgCode<Selection_GroupTree::GROUP_ADD_SELECTED)
		return;

	//ignore all if cumulated:
	//if (m_pbtnShowActive->isChecked())
	//{
	//	QMessageBox::warning(this,tr("Warning"),tr("No action allowed on group content while it shows cumulated data! Select only one group!"));
	//	return;
	//}

	//Warning: lstData: is in short actual list format (name, id, et.c..)

	DbRecordSet lstGroupContent;
	lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
	int nGroupIdx=lstGroupContent.getColumnIdx("BGCN_ITEM_ID");

	//list is in contact format: must contain BCNT_ID field
	int nContactIdx=lstData.getColumnIdx("BCNT_ID");
	lstData.setColumn(nContactIdx,QVariant::Int,"BGCN_CONTACT_ID");
	Q_ASSERT(nContactIdx!=-1);
	
	switch(nMsgCode)
	{
	case Selection_GroupTree::GROUP_ADD_SELECTED:
	case Selection_GroupTree::GROUP_ADD_WHOLE_LIST:
		{
			lstGroupContent.merge(lstData);
			lstGroupContent.setColValue(nGroupIdx,nMsgDetail);
			//lstGroupContent.Dump();
			_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nMsgDetail,DataHelper::OP_ADD,lstGroupContent))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			
		}
		break;
	case Selection_GroupTree::GROUP_REMOVE_SELECTED:
		{
			lstGroupContent.merge(lstData);
			lstGroupContent.setColValue(nGroupIdx,nMsgDetail);
			//lstGroupContent.Dump();
			_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nMsgDetail,DataHelper::OP_REMOVE,lstGroupContent))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
		}
		break;
	case Selection_GroupTree::GROUP_REPLACE_SELECTED:
	case Selection_GroupTree::GROUP_REPLACE_WHOLE_LIST:
		{
			lstGroupContent.merge(lstData);
			lstGroupContent.setColValue(nGroupIdx,nMsgDetail);
			_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nMsgDetail,DataHelper::OP_REPLACE,lstGroupContent))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
		}
	    break;
	case Selection_GroupTree::GROUP_INTERSECT_SELECTED:
		{
			lstGroupContent.merge(lstData);
			lstGroupContent.setColValue(nGroupIdx,nMsgDetail);
			_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nMsgDetail,DataHelper::OP_INTERSECT,lstGroupContent))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
		}
	    break;
	default:
		Q_ASSERT(false);
	    break;
	}

	//Warning: lstGroupContent is now in BUS_CM_GRUOP definition: now we must reload from server all new data that not exists already in list:
	DbRecordSet lstNewData;
	nContactIdx=lstGroupContent.getColumnIdx("BGCN_CONTACT_ID");

	ClientContactManager::CompareLists(lstGroupContent,m_lstData,NULL,&lstNewData,nContactIdx,0);

	m_lstData.deleteUnSelectedRows(); //keep only matches, those are unselected
	m_lstData.clearSelection();


	if (lstNewData.getRowCount()>0)
	{
		ReadGroupContent(lstNewData);	///auto add on end
	}
	

	//if cumulated: remove duplos & sort
	//UpdateListContentIfAccumulated();
	RefreshDisplay();

	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_GROUP_DATA_INSIDE_CONTACT_DETAILS,0);

}


//reload data:
void Table_ContactsGroup::OnRefreshContent()
{

	//switch to GROUP conent:



	Status err;
	
	//ignore all if cumulated:
	//if (m_pbtnShowActive->isChecked())
	//{
	//	QMessageBox::warning(this,tr("Warning"),tr("No action allowed on group content while it shows cumulated data! Select only one group!"));
	//	return;
	//}
	if (m_nCurrentGroupID==-1 || m_lstCurrentGroups.getRowCount()==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select group first!"));
		return;
	}

	//QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	DbRecordSet lstNewData;
	_SERVER_CALL(BusGroupTree->ReadGroupContent(err,ENTITY_BUS_CONTACT,m_lstCurrentGroups,lstNewData))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}


	m_lstData.clear();
	ReadGroupContent(lstNewData);	///auto add on end
	//UpdateListContentIfAccumulated();
	RefreshDisplay();
	return;
}



//accepts drops from group/actual grid+tree of groups
void Table_ContactsGroup::OnDataDroped(int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
{
	int nSize=DroppedValue.getRowCount();
	if(nSize==0 || m_nCurrentGroupID==-1 || m_pbtnShowActive->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Can not drop contact onto the group because either group is not selected inside the Group Tree or Show Active flag is checked!"));
		event->ignore();
		return;
	}
	//cant drop on itself
	if(event->source()==this)
	{
		event->ignore();
		return;
	}
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CONTACT_GROUP_TREES))
	{
		event->ignore();
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}

	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);


	QVariant varData;
	qVariantSetValue(varData,DroppedValue);
	updateObserver(NULL,Selection_GroupTree::GROUP_ADD_SELECTED,m_nCurrentGroupID,varData);
}


//shows only active:
void Table_ContactsGroup::OnRefreshActive()
{
	int nValidFrom=m_lstData.getColumnIdx("BGCN_CMCA_VALID_FROM");
	int nValidTo=m_lstData.getColumnIdx("BGCN_CMCA_VALID_TO");
	QDate today=QDate::currentDate();

	if (m_pbtnShowActive->isChecked())
	{

		//find active:
		int nSize=m_lstData.getRowCount();
		m_lstData.clearSelection();
		for(int i=0;i<nSize;++i)
		{
			QDate from=m_lstData.getDataRef(i,nValidFrom).toDate();
			QDate to=m_lstData.getDataRef(i,nValidTo).toDate();
			
			if (from.isNull() && to.isNull())
				continue;

			if (from.isNull() && !to.isNull())
			{
				if (to>=today)
					continue;
				else
				{
					m_lstData.selectRow(i);
					continue;
				}
			}

			if (!from.isNull() && to.isNull())
			{
				if (from<=today)
					continue;
				else
				{
					m_lstData.selectRow(i);
					continue;
				}
			}

			if (from<=today && to>=today)
			{
				continue;
			}
			else
			{
				m_lstData.selectRow(i);
				continue;
			}
		}

		m_lstData.deleteSelectedRows();
	}
}



void Table_ContactsGroup::OnShowDetails()
{
	int nRow=m_lstData.getSelectedRow();
	if (nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BCNT_ID").toInt();
		emit ShowContactDetails(nContactID);
	}
	
}

void Table_ContactsGroup::OnShowDetailsNewWindow()
{
	int nRow=m_lstData.getSelectedRow();
	if (nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BCNT_ID").toInt();
		g_objFuiManager.OpenFUI(MENU_CONTACTS,true,false,FuiBase::MODE_READ,nContactID);
		//emit ShowContactDetails(nContactID);
	}

}


void Table_ContactsGroup::OnDelete()
{

	DbRecordSet DataForDelete;
	DataForDelete.addColumn(QVariant::Int,"BCNT_ID");
	DataForDelete.merge(m_lstData,true);

	if (DataForDelete.getRowCount()>0)
	{
		Status err;

		int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete the selected contacts (%1) from the database?").arg(QVariant(DataForDelete.getRowCount()).toString()),tr("Yes"),tr("No"));
		if (nResult!=0) return; //only if YES

		_SERVER_CALL(BusContact->DeleteContacts(err,DataForDelete,true))
		//error:
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			if (err.getErrorCode()==StatusCodeSet::ERR_BUS_ACESS_REFUSED_ON_DELETE) //reload:
			{
				OnRefreshContent();
			}

			return;
		}

		m_lstData.deleteSelectedRows();
		RefreshDisplay();

		//emit global:
		g_ClientCache.ModifyCache(ENTITY_BUS_CONTACT,DataForDelete,DataHelper::OP_REMOVE,this);

	}


}


void Table_ContactsGroup::OnRemoveContactsAll()
{
	//Status err;
	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

//	m_lstData.Dump();

	DbRecordSet lstGroupContactID; //not need to send id's->group id is enough!
	lstGroupContactID.addColumn(QVariant::Int,"BGCN_CONTACT_ID");
	//lstGroupContactID.merge(m_lstData);

	//_DUMP(m_lstCurrentGroups);
	_SERVER_CALL(BusGroupTree->RemoveGroupContentItem(err,ENTITY_BUS_CONTACT,lstGroupContactID,m_lstCurrentGroups))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	m_lstData.clear();
	RefreshDisplay();

}

void Table_ContactsGroup::OnRemoveContacts()
{
	//Status err;
	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	int nSel=m_lstData.getSelectedCount();

	DbRecordSet lstGroupContactID; //send only ID's
	lstGroupContactID.addColumn(QVariant::Int,"BGCN_CONTACT_ID");
	lstGroupContactID.merge(m_lstData,true);

	//_DUMP(lstGroupContactID);
	_SERVER_CALL(BusGroupTree->RemoveGroupContentItem(err,ENTITY_BUS_CONTACT,lstGroupContactID,m_lstCurrentGroups))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	nSel=m_lstData.getSelectedCount();
	m_lstData.deleteSelectedRows();
	nSel=m_lstData.getSelectedCount();

	RefreshDisplay();

}



void Table_ContactsGroup::OnAddContactsToActual()
{
	FuiBase *pFui=dynamic_cast<FuiBase*>(m_pContactFUI);
	if (pFui==NULL) 
		return;
	//get actual list from cache
	DbRecordSet *dataCached=pFui->getSelectionController()->GetDataSource();
	if(dataCached==NULL)
		return;

	dataCached->merge(m_lstData,true);
	pFui->getSelectionController()->RefreshDisplay();
}


void Table_ContactsGroup::OnEnterPeriod()
{

	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);


	//enter dialog:
	Dlg_EnterPeriod Dlg;
	int nRow=m_lstData.getSelectedRow();
	if (nRow==-1) return;

	Dlg.setPeriod(m_lstData.getDataRef(nRow,"BGCN_CMCA_VALID_FROM").toDate(),m_lstData.getDataRef(nRow,"BGCN_CMCA_VALID_TO").toDate());
	if(Dlg.exec())
	{
		QDate from,to;
		Dlg.getPeriod(from,to);

		DbRecordSet lstGroupContent;
		lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
		lstGroupContent.merge(m_lstData,true);		//M.B.: 720
		lstGroupContent.setColValue(lstGroupContent.getColumnIdx("BGCN_CMCA_VALID_FROM"),from);
		lstGroupContent.setColValue(lstGroupContent.getColumnIdx("BGCN_CMCA_VALID_TO"),to);

		//DbRecordSet row=m_lstData.getRow(nRow);


		//
		//Status err;
		_SERVER_CALL(BusGroupTree->WriteGroupContentData(err,ENTITY_BUS_CONTACT,lstGroupContent))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		}
		OnRefreshContent();
		//RefreshDisplayRow(nRow);
	}
}



//reads group content based on given group/cont assoc: lstNewData (BUS_CM_GROUP)
void Table_ContactsGroup::ReadGroupContent(DbRecordSet lstNewData)
{

	//read by chunks
	int nSize=lstNewData.getRowCount();
	int nChunkSize=150;

	int nItemIDIdx_1=lstNewData.getColumnIdx("BGCN_ITEM_ID");
	int nValidFromIdx_1=lstNewData.getColumnIdx("BGCN_CMCA_VALID_FROM");
	int nValidToIdx_1=lstNewData.getColumnIdx("BGCN_CMCA_VALID_TO");
	int nPKIdx_1=lstNewData.getColumnIdx("BGCN_ID");
	int nContIdx_1=lstNewData.getColumnIdx("BGCN_CONTACT_ID");


	//do not load duplicate contact ID's (if cumulated):
	if (m_bIsCurrentGroupCumulated)
		lstNewData.removeDuplicates(nContIdx_1);



	//make filter, chunked read...
	MainEntityFilter filter;
	DbRecordSet lstChunkRead;
	Status err;
	int nCount=0;
	QString strWhereIn,strWhere;
	QSize size(300,100);
	g_pClientManager->HideCommunicationInProgress(true);
	QProgressDialog progress(tr("Reading Group Contact List..."),tr("Cancel"),0,nSize,this);
	progress.setWindowTitle(tr("Read In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//2.4s before pops up
	//qApp->processEvents();
	//READ Contact data at 100records chunks:
	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		int nOffset=nCount;
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstNewData,"BGCN_CONTACT_ID",strWhereIn,nCount,nChunkSize);
		if (nCount<0) break;
		strWhere="BCNT_ID IN "+strWhereIn;
		filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
		_SERVER_CALL(BusContact->ReadData(err,filter.Serialize(),lstChunkRead))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			g_pClientManager->HideCommunicationInProgress(false);
			return;
		}
		filter.ClearFilter();

		//----------------------------------------------------
		//prepare chunked list: copy all fields:
		//----------------------------------------------------
		int nSize2			=lstChunkRead.getRowCount();
		int nRow;
		for(int i=0;i<nSize2;++i)
		{
			nRow=lstChunkRead.find(0,lstNewData.getDataRef(nOffset+i,nContIdx_1),true);
			if (nRow!=-1){
				DbRecordSet row = lstNewData.getRow(nOffset+i);
				lstChunkRead.assignRow(nRow,row,true);
			}
			else
			{
				//Q_ASSERT(false);	//ACEESS RIGHTS-> some contact will not be shown inside group!
				continue;
			}
		}
		m_lstData.merge(lstChunkRead);
		if (nCount>=nChunkSize)//hide progress for short operations
		{
			progress.setValue(nCount);
			qApp->processEvents();
			if (progress.wasCanceled())
			{
				g_pClientManager->HideCommunicationInProgress(false);
				return;
			}
		}
	}

	//hide progress for short operations
	if (nCount>=nChunkSize)
	{
		progress.setValue(nSize);
		qApp->processEvents();
	}


	g_pClientManager->HideCommunicationInProgress(false);
}



void Table_ContactsGroup::GetSelectedContacts(DbRecordSet &lstContacts)
{
	lstContacts.copyDefinition(m_lstData);
	lstContacts.merge(m_lstData,true);
}


void Table_ContactsGroup::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	OnRefreshActive();
	UniversalTableWidgetEx::RefreshDisplay(-1,true); //do d' upper:

	if (m_nCurrentGroupID==-1)
		m_pLabelGroupName->setText("");
	else
	{
		QString strName=m_pSelectionGrouTree->GetCalculatedName(m_nCurrentGroupID);
		if (m_bIsCurrentGroupCumulated)
		{
			QString strChildrens=QVariant(m_lstCurrentGroups.getRowCount()-1).toString();
			m_pLabelGroupName->setText(m_strHtmlTag+strName+"<font color=\"#0000FF\"> ("+strChildrens+")</font>"+m_strEndHtmlTag);
		}
		else
		{
			m_pLabelGroupName->setText(m_strHtmlTag+strName+m_strEndHtmlTag);
		}
	}

	OnSelectionChanged();
}


//create multiline
void Table_ContactsGroup::Data2CustomWidget(int nRow, int nCol)
{
	int nColType=m_lstColumnSetup.getDataRef(nCol,"BOGW_COLUMN_TYPE").toInt();

	//if (nColType==COL_TYPE_MULTILINE_TEXT)
	//{
		QTextEdit *pText = dynamic_cast<QTextEdit *>(cellWidget(nRow,nCol));
		if (!pText)
		{
			pText = new QTextEdit;
			pText->setReadOnly(true);
			setCellWidget(nRow,nCol,pText);
		}
		pText->setHtml(m_plstData->getDataRef(nRow,m_lstColumnMapping[nCol]).toString());
	//}
}