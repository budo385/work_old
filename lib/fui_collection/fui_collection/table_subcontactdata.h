#ifndef TABLE_SUBCONTACTDATA_H
#define TABLE_SUBCONTACTDATA_H

#include "gui_core/gui_core/universaltablewidgetex.h"

class Table_SubContactData : public UniversalTableWidgetEx
{
	Q_OBJECT

public:

	enum ColumnType
	{
		COL_TYPE_LIST=4,
		COL_TYPE_MULTILINE_TEXT=5
	};

	Table_SubContactData(QWidget *parent);
	~Table_SubContactData();

private slots:
	void OnListSelectionChanged();

protected:
	void Data2CustomWidget(int nRow, int nCol);


private:
	
};

#endif // TABLE_SUBCONTACTDATA_H
