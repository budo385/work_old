#ifndef QC_ENTITYSELECTOR_H
#define QC_ENTITYSELECTOR_H

#include <QWidget>
#include "ui_qc_entityselector.h"
#include "common/common/dbrecordset.h"

class QC_EntitySelector : public QWidget
{
	Q_OBJECT

public:
	QC_EntitySelector(QWidget *parent = 0);
	~QC_EntitySelector();

	void Initialize(int nEntityCollectionID, DbRecordSet *recFullContactData, QWidget *ParentWidget = NULL, int nParentType = 0 /*0-Large,1-small widget*/);
	void addEntity(DbRecordSet recEntityData);
	void addEntityNoOverride(DbRecordSet recEntityData);

private:
	Ui::QC_EntitySelectorClass ui;

	int			m_nEntityCollectionID;
	int			m_nPageCount;
	int			m_nCurrentPage;
	DbRecordSet *m_recFullContactData;
	DbRecordSet m_recLocalEntityRecordSet;
	QString		m_strOfString;
	QWidget		*m_pParentWidget;

	void SetupLineEdit();
	void AppendOrUpdateEmailDescr(DbRecordSet &recEmail, DbRecordSet recNewRecordset);

private slots:
	void on_previous_toolButton_clicked();
	void on_next_toolButton_clicked();
	void on_add_toolButton_clicked(DbRecordSet *recRecordset = NULL);
	void on_delete_toolButton_clicked();

signals:
	void RowChanged(int nRow);
};

#endif // QC_ENTITYSELECTOR_H
