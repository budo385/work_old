#include "calendarhelper.h"
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/entity_id_collection.h"
#include "common/common/datahelper.h"
#include "fui_collection/fui_collection/emaildialog.h"
#include "fui_collection/fui_collection/menuitems.h"
#include "common/common/rsakey.h"
#include "os_specific/os_specific/mapimanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_client/bus_client/emailhelper.h"
#include "bus_core/bus_core/contacttypemanager.h"

#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;

void SendSkypeSMS(const QString &strPhone, const QString &strMessage);


//load from cache and server, store in cache
void CalendarHelper::GetCalendarTemplates(DbRecordSet &lstData,bool bReloadFromServer)
{
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));

	if(g_pClientManager->GetPersonID()==0) return;

	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_CALENDAR_EVENT_TEMPLATES);
		if (plstCacheData)
		{
			lstData=*plstCacheData;
			return;
		}
	}

	Status err;
	_SERVER_CALL(BusCalendar->ReadTemplates(err,lstData))
	if (!err.IsOK())
	{
		QMessageBox::critical(NULL,(QString)QObject::tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//Store in Cache:
	g_ClientCache.SetCache(ENTITY_CALENDAR_EVENT_TEMPLATES,lstData);

}


//Ret_Reservations: is in format of BUS_CAL_RESERVATION
//Ret_ReservationsRecalculated: is in simple format: ID,DATETIME_FROM,DATETIME_TO,FLAG_IS_POSSIBLE
void CalendarHelper::RecalcReservationList(QDateTime datStartDate, QDateTime datEndDate,DbRecordSet &Ret_Reservations,DbRecordSet &Ret_ReservationsRecalculated)
{
	Ret_ReservationsRecalculated.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_BUS_CAL_RESERVATION));

	//resort 'em:
	int nSize=Ret_Reservations.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (Ret_Reservations.getDataRef(i,"BCRS_IS_FREE").toInt()>0) //use as is but expand from start->end
		{
			Ret_ReservationsRecalculated.addRow();
			int nRow=Ret_ReservationsRecalculated.getRowCount()-1;
			Ret_ReservationsRecalculated.setData(nRow,"BCRS_ID",Ret_Reservations.getDataRef(i,"BCRS_ID"));
			Ret_ReservationsRecalculated.setData(nRow,"BCRS_IS_POSSIBLE",Ret_Reservations.getDataRef(i,"BCRS_IS_POSSIBLE"));
			Ret_ReservationsRecalculated.setData(nRow,"BCRS_IS_FREE",Ret_Reservations.getDataRef(i,"BCRS_IS_FREE"));
			Ret_ReservationsRecalculated.setData(nRow,"DATE_TO",Ret_Reservations.getDataRef(i,"BCRS_FREE_TO"));
			Ret_ReservationsRecalculated.setData(nRow,"DATE_FROM",Ret_Reservations.getDataRef(i,"BCRS_FREE_FROM"));
		}
		else
		{
			int nWeekDay=Ret_Reservations.getDataRef(i,"BCRS_WEEKDAY").toInt();

			int nDifferenceSecs=Ret_Reservations.getDataRef(i,"BCRS_WEEKDAY_TIME_FROM").toDateTime().secsTo(Ret_Reservations.getDataRef(i,"BCRS_WEEKDAY_TIME_TO").toDateTime());
			if (nDifferenceSecs<0)
				nDifferenceSecs=60*60;
			else if (nDifferenceSecs>60*60*24)
				nDifferenceSecs=60*60*24;

			QTime startTime=Ret_Reservations.getDataRef(i,"BCRS_WEEKDAY_TIME_FROM").toDateTime().time();
			//QTime endTime=Ret_Reservations.getDataRef(i,"BCRS_WEEKDAY_TIME_TO").toDateTime().time();
			//bool bSpanOverTwoDay=false;
			//if (startTime.secsTo(endTime)<0)
			//	bSpanOverTwoDay=true; //means that day offset is -1

			qDebug()<<datStartDate;
			qDebug()<<datEndDate;
			QDateTime start_period=datStartDate;
			QDateTime end_period=datStartDate;
			//search first occurrence: then each is 7days further..
			int nDayinWeek=datStartDate.date().dayOfWeek()-1; //from 0:monday -6:sunday
			int nDiff=nWeekDay-nDayinWeek;
			if (nDiff<0)
				nDiff=7+nWeekDay-nDayinWeek;
			//if (nDiff>0 && bSpanOverTwoDay)
			//	nDiff--;

			while (start_period.addDays(nDiff)<=datEndDate)
			{
				start_period=start_period.addDays(nDiff);
				start_period=QDateTime(start_period.date(),startTime);
				end_period=start_period.addSecs(nDifferenceSecs);
				//QDateTime(start_period.date(),endTime);
				qDebug()<<start_period;
				qDebug()<<end_period;

				//add row:
				Ret_ReservationsRecalculated.addRow();
				int nRow=Ret_ReservationsRecalculated.getRowCount()-1;
				Ret_ReservationsRecalculated.setData(nRow,"BCRS_ID",Ret_Reservations.getDataRef(i,"BCRS_ID"));
				Ret_ReservationsRecalculated.setData(nRow,"BCRS_IS_POSSIBLE",Ret_Reservations.getDataRef(i,"BCRS_IS_POSSIBLE"));
				Ret_ReservationsRecalculated.setData(nRow,"BCRS_IS_FREE",Ret_Reservations.getDataRef(i,"BCRS_IS_FREE"));
				Ret_ReservationsRecalculated.setData(nRow,"DATE_FROM",start_period);
				Ret_ReservationsRecalculated.setData(nRow,"DATE_TO",end_period);
				nDiff=7; //only first time align, then always 7days diff
			}
		}
	}
}


//part+invite_id+contact_id: separated by "-"
QString	CalendarHelper::GetCalendarStateNameFromID(int nStateID)
{
	switch (nStateID)
	{
	case GlobalConstants::TYPE_CAL_FIXED:
		return QObject::tr("Fixed");
	case GlobalConstants::TYPE_CAL_PRELIMINARY:
		return QObject::tr("Preliminary");
	case GlobalConstants::TYPE_CAL_OVER:
		return QObject::tr("Over");
	case GlobalConstants::TYPE_CAL_CANCELLED:
		return QObject::tr("Cancelled");
	default:
		return "";
	}

	return "";
}

//part+invite_id+contact_id: separated by "-"
QString	CalendarHelper::GenerateSubjectID(DbRecordSet &rowData)
{
	if (rowData.getRowCount()==0)
		return "";
	return rowData.getDataRef(0,"BCEP_ID").toString()+"-"+rowData.getDataRef(0,"BCIV_ID").toString()+"-"+rowData.getDataRef(0,"RECIPIENT_CONTACT_ID").toString();
}

QString CalendarHelper::ParseSubjectID(QString strSubject)
{
	//strSubject += " ID: ["+strID+"]";
	int nPos=strSubject.indexOf("ID: [");
	if (nPos<0)
		return "";
	nPos +=5;
	int nPos2=strSubject.indexOf("]",nPos);
	if (nPos2<0)
		return "";
	return strSubject.mid(nPos,nPos2-nPos);
}

//replace all: rowData contains all ID's & data...
/*
1. [Cal_Inv_Title]
2. [Cal_Inv_Subject]
3. [Cal_Inv_From]
4. [Cal_Inv_To]
5. [Cal_Inv_Location]
6. [Cal_Inv_Description]
7. [Cal_Inv_Type]   -> from cache, then extract type name if not null
8. [Cal_Inv_Category]
9. [Cal_Inv_State]  -> case: fixed, prelim
10. [Cal_Inv_Owner] -> from cache: first_last
*/
void CalendarHelper::CreateInviteEmailFromTemplate(QString bodyTemplate, QString &NewBody, DbRecordSet &rowData,bool btoPercentEncoding)
{
	NewBody.replace("[Cal_Inv_Title]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"BCEV_TITLE").toString()) : rowData.getDataRef(0,"BCEV_TITLE").toString());
	NewBody.replace("[Cal_Inv_Subject]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"BCOL_SUBJECT").toString()) : rowData.getDataRef(0,"BCOL_SUBJECT").toString());
	NewBody.replace("[Cal_Inv_From]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"BCOL_FROM").toDateTime().toString("MM.dd.yyyy")) : rowData.getDataRef(0,"BCOL_FROM").toDateTime().toString("MM.dd.yyyy"));
	NewBody.replace("[Cal_Inv_To]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"BCOL_TO").toDateTime().toString("MM.dd.yyyy")) : rowData.getDataRef(0,"BCOL_TO").toDateTime().toString("MM.dd.yyyy"));
	NewBody.replace("[Cal_Inv_Location]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"BCOL_LOCATION").toString()) : rowData.getDataRef(0,"BCOL_LOCATION").toString());
	NewBody.replace("[Cal_Inv_Description]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"BCOL_DESCRIPTION").toString()) : rowData.getDataRef(0,"BCOL_DESCRIPTION").toString());
	NewBody.replace("[Cal_Inv_Category]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"BCEV_CATEGORY").toString()) : rowData.getDataRef(0,"BCEV_CATEGORY").toString());

	QString strState=GetCalendarStateNameFromID(rowData.getDataRef(0,"BCEP_STATUS").toInt());
	NewBody.replace("[Cal_Inv_State]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(strState) : strState);

	NewBody.replace("[Cal_Inv_Owner]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"OWNER_NAME").toString()) : rowData.getDataRef(0,"OWNER_NAME").toString());
	NewBody.replace("[Cal_Inv_Type]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(rowData.getDataRef(0,"TYPE_NAME").toString()) : rowData.getDataRef(0,"TYPE_NAME").toString());

}

QString	CalendarHelper::GetDefaultInviteSubject()
{
	return "Invitation to [Cal_Inv_Title]";
}
QString	CalendarHelper::GetDefaultInviteBody()
{
	QString strBody="[Cal_Inv_Owner] invites you to the following event:";
	strBody +="\n\r";
	strBody +="Title: [Cal_Inv_Title]";
	strBody +="\n\r";
	strBody +="Subject: [Cal_Inv_Subject]";
	strBody +="\n\r";
	strBody +="When: From [Cal_Inv_From] to [Cal_Inv_To]";
	strBody +="\n\r";
	strBody +="Description: [Cal_Inv_Description]";
	strBody +="\n\r";
	return strBody;
}




//lstData in format:  TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS
//create body, set subject, trlibaba lan, create com data, send email
void CalendarHelper::SendInvitations(Status &err,DbRecordSet &lstData)
{
	//lstData.Dump();
	err.setError(0);
	int nSize=lstData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet rowInvite=lstData.getRow(i);

		QString strSubject;
		QString strBody;
		//try template first:
		if (rowInvite.getDataRef(0,"BCIV_EMAIL_TEMPLATE_ID").toInt()>0)
		{
			strBody=rowInvite.getDataRef(0,"EMAIL_TEMPLATE_BODY").toString();
			strSubject=rowInvite.getDataRef(0,"EMAIL_TEMPLATE_SUBJECT").toString();
		}
		if (strSubject.isEmpty())
			strSubject=GetDefaultInviteSubject();
		if (strBody.isEmpty())
			strBody=GetDefaultInviteBody();

		EmailHelper::CreateEmailFromTemplate(strBody,strBody,rowInvite.getDataRef(0,"RECIPIENT_CONTACT_ID").toInt());
		CreateInviteEmailFromTemplate(strBody,strBody,rowInvite);
		CreateInviteEmailFromTemplate(strSubject,strSubject,rowInvite);
		
		//Add ID at end of subject:
		QString strID=GenerateSubjectID(rowInvite);
		strSubject += " ID: ["+strID+"]";
		rowInvite.setData(0,"BCIV_SUBJECT_ID",strID);


		//embed body inside iCal: and use subject

		GenerateICalendar(rowInvite,strBody, strSubject);

		//rowInvite.Dump();

		//QString strSubject=rowInvite.getDataRef(i,"BCOL_SUBJECT").toString();


#ifdef _WIN32
		//MR: create email, save as comm, send to Outlook...
		DbRecordSet lstContactsToSend;
		lstContactsToSend.addColumn(QVariant::Int,"BCNT_ID");
		lstContactsToSend.addRow();
		lstContactsToSend.setData(0,0,rowInvite.getDataRef(0,"ORGANIZER_CONTACT_ID").toInt());

		bool bOK=MapiManager::SendAppointment(strBody, strSubject, QString(), rowInvite.getDataRef(0,"RECIPIENT_MAIL").toString());

		//save back data:
		lstData.assignRow(i,rowInvite);
		//rowInvite.Dump();
		//save each e-mail as CE record: ??
#endif
	}

	//lstData.Dump();

	//save all invite record: new OUID/Subject ID
	_SERVER_CALL(BusCalendar->WriteInviteRecordsAfterSend(err,lstData));

	/*
	//save all mails
	QString strLockRes;
	DbRecordSet lstSchedule, lstAttachments, lstContactLinks;
	bool bSkipExisting = true;
	bool bAddDupsAsNewRow = false;
	int nSkippedOrReplacedCnt = 0;
	_SERVER_CALL(BusEmail->WriteMultiple(err, lstData, lstContactLinks, lstSchedule, lstAttachments, strLockRes, bSkipExisting, nSkippedOrReplacedCnt, bAddDupsAsNewRow))
	*/
}


//lstData in TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS:
//store BCIV_ID somewhere inside calendar body, return generated UID inside OUTLOOK_UID field
//MIME: text/calendar
bool CalendarHelper::GenerateICalendar(DbRecordSet &lstData, QString &strBodyResult, QString &strSubject)
{
/*
BEGIN:VCALENDAR
PRODID:-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN
VERSION:2.0
METHOD:REQUEST
BEGIN:VEVENT
ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:rajcic@sokrates.hr
ORGANIZER:MAILTO:infoline@sokrates.ch
DTSTART:20090730T070000Z
DTEND:20090730T073000Z
TRANSP:OPAQUE
SEQUENCE:0
UID:040000008200E00074C5B7101A82E0080000000010CE8B925910CA010000000000000000100
 0000007B05852E2E14446BCB8EBFC0A64373C
DTSTAMP:20090729T123343Z
DESCRIPTION:Zeit: Donnerstag\, 30. Juli 2009 09:00-09:30 (GMT+01:00)
  Amsterdam\, Berlin\, Bern\, Rom\, Stockholm\,
  Wien.\n\n*~*~*~*~*~*~*~*~*~*\n\n\n
SUMMARY:Demo Termin
PRIORITY:5
X-MICROSOFT-CDO-IMPORTANCE:1
CLASS:PUBLIC
BEGIN:VALARM
TRIGGER:-PT15M
ACTION:DISPLAY
DESCRIPTION:Reminder
END:VALARM
END:VEVENT
END:VCALENDAR
*/
/*
	//BCOL_LOCATION
*/
	strBodyResult  = "";
	strBodyResult += "BEGIN:VCALENDAR\r\n";
	//strBodyResult += "PRODID:-//Helix Business Soft AG//Sokrates Communicator 1.0//EN\r\n";
	strBodyResult += "PRODID:-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN\r\n";
	strBodyResult += "VERSION:2.0\r\n";
	strBodyResult += "METHOD:REQUEST\r\n";
	strBodyResult += "BEGIN:VEVENT\r\n";

	strBodyResult += "ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:";
	QString strDesc = lstData.getDataRef(0, "RECIPIENT_MAIL").toString();
	strBodyResult += strDesc;
	strBodyResult += "\r\n";

	strBodyResult += "ORGANIZER:MAILTO:";
	strDesc = lstData.getDataRef(0, "ORGANIZER_MAIL").toString();
	strBodyResult += strDesc;
	strBodyResult += "\r\n";

	strBodyResult += "DTSTART:";
	QDateTime dtTmp = lstData.getDataRef(0, "BCOL_FROM").toDateTime();
	strDesc = dtTmp.toString("yyyyMMddThhmmssZ");
	strBodyResult += strDesc;
	strBodyResult += "\r\n";

	strBodyResult += "DTEND:";
	dtTmp = lstData.getDataRef(0, "BCOL_TO").toDateTime();
	strDesc = dtTmp.toString("yyyyMMddThhmmssZ");
	strBodyResult += strDesc;
	strBodyResult += "\r\n";

	strBodyResult += "TRANSP:OPAQUE\r\n";
	strBodyResult += "SEQUENCE:0\r\n";

	//TOFIX generate this field, convert to hex
	const char hex2char[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};   
	unsigned char szBuffer[100];
	OpenSSLHelper::Extern_RAND_bytes((unsigned char *)szBuffer, 50);
	QString strData;
	for(int i=0; i<50; i++){
		char szBuffer1[10];
		unsigned char b = szBuffer[i];
		int nNibbleHigh = (b & 0xF0) >> 4;
		int nNibbleLow  = (b & 0x0F);
		sprintf(szBuffer1, "%c%c", hex2char[nNibbleHigh], hex2char[nNibbleLow]);
		strData += szBuffer1;
	}
	lstData.setData(0, "BCIV_OUID", strData);

	strBodyResult += "UID:";
	strDesc = strData;
	strDesc = strDesc.replace("\r\n", "\r\n ");	//multiple text lines must be indented
	strBodyResult += strDesc;
	strBodyResult += "\r\n";

	strBodyResult += "DTSTAMP:";
	dtTmp = QDateTime::currentDateTime();
	strDesc = dtTmp.toString("yyyyMMddThhmmssZ");
	strBodyResult += strDesc;
	strBodyResult += "\r\n";

	//keep this one empty, fill the one at the botton (VEVENT related)
	strBodyResult += "DESCRIPTION:When:\\nWhere:\\n\\n*~*~*~*~*~*~*~*~*~*\\n\\n\r\n  ";
	//TOFIX add location here
	strDesc = lstData.getDataRef(0, "BCOL_DESCRIPTION").toString();
	strDesc = DataHelper::ExtractTextFromHTML(strDesc);
	strDesc = strDesc.replace("\r\n", "\\n");
	strDesc = strDesc.replace("\n", "\\n");
	strBodyResult += strDesc;
	strBodyResult += "\r\n";

	strBodyResult += "SUMMARY:"; 
	strDesc = strSubject; //lstData.getDataRef(0, "BCOL_SUBJECT").toString();
	strDesc = strDesc.replace("\r\n", "\r\n ");	//multiple text lines must be indented
	strBodyResult += strDesc;
	strBodyResult += "\r\n";

	strBodyResult += "PRIORITY:5\r\n";
	strBodyResult += "X-MICROSOFT-CDO-IMPORTANCE:1\r\n";
	strBodyResult += "CLASS:PUBLIC\r\n";
	strBodyResult += "BEGIN:VALARM\r\n";
	strBodyResult += "TRIGGER:-PT15M\r\n";
	strBodyResult += "ACTION:DISPLAY\r\n";

	strBodyResult += "DESCRIPTION:Reminder\r\n";

	strBodyResult += "END:VALARM\r\n";
	strBodyResult += "END:VEVENT\r\n";
	strBodyResult += "END:VCALENDAR\r\n";

#if 0 //#ifdef _DEBUG
	strBodyResult = "BEGIN:VCALENDAR\r\n";
	strBodyResult += "PRODID:-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN\r\n";
	strBodyResult += "VERSION:2.0\r\n";
	strBodyResult += "METHOD:REQUEST\r\n";
	strBodyResult += "BEGIN:VEVENT\r\n";
	strBodyResult += "ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:rajcic@sokrates.hr\r\n";
	strBodyResult += "ORGANIZER:MAILTO:infoline@sokrates.ch\r\n";
	strBodyResult += "DTSTART:20090730T070000Z\r\n";
	strBodyResult += "DTEND:20090730T073000Z\r\n";
	strBodyResult += "TRANSP:OPAQUE\r\n";
	strBodyResult += "SEQUENCE:0\r\n";
	strBodyResult += "UID:040000008200E00074C5B8101A82E0080000000010CE8B925910CA010000000000000000100\r\n";
	strBodyResult += " 0000007B05852E2E14446BCB8EBFC0A64373C\r\n";
	strBodyResult += "DTSTAMP:20090729T123343Z\r\n";
	strBodyResult += "DESCRIPTION:Zeit: Donnerstag\\, 30. Juli 2009 09:00-09:30 (GMT+01:00)\r\n";
	strBodyResult += "  Amsterdam\\, Berlin\\, Bern\\, Rom\\, Stockholm\\,\r\n";
	strBodyResult += "  Wien.\\n\\n*~*~*~*~*~*~*~*~*~*\\n\\n\\n\r\n";
	strBodyResult += "SUMMARY:Demo Termin\r\n";
	strBodyResult += "PRIORITY:5\r\n";
	strBodyResult += "X-MICROSOFT-CDO-IMPORTANCE:1\r\n";
	strBodyResult += "CLASS:PUBLIC\r\n";
	strBodyResult += "BEGIN:VALARM\r\n";
	strBodyResult += "TRIGGER:-PT15M\r\n";
	strBodyResult += "ACTION:DISPLAY\r\n";
	strBodyResult += "DESCRIPTION:Reminder\r\n";
	strBodyResult += "END:VALARM\r\n";
	strBodyResult += "END:VEVENT\r\n";
	strBodyResult += "END:VCALENDAR\r\n";
#endif

	return true;
}

//lstData in TVIEW_BUS_CAL_INVITE_FIELDS:
//store BCIV_ID somewhere inside calendar body, return generated UID inside OUTLOOK_UID field
//MIME: text/x-vCalendar
bool CalendarHelper::GenerateVCalendar(DbRecordSet &lstData, QString &strBodyResult)
{
	//TOFIX not used?
	return false;
}

void CalendarHelper::SendInvitationsBySMS(Status &err,DbRecordSet &lstData)
{
	//lstData.Dump();

	err.setError(0);
	int nSize=lstData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet rowInvite=lstData.getRow(i);

		QString strSubject;
		QString strBody;
		GetDefaultInviteSMS_BodyAndSubject(strSubject,strBody);
		CreateInviteEmailFromTemplate(strBody,strBody,rowInvite);
		CreateInviteEmailFromTemplate(strSubject,strSubject,rowInvite);
		
		//Add ID at end of subject:
		//QString strID=GenerateSubjectID(rowInvite);
		//strSubject += " ID: ["+strID+"]";
		//rowInvite.setData(0,"BCIV_SUBJECT_ID",strID);

		QString strSMS = strSubject;
		strSMS += "\n";
		strSMS += strBody;

		QString strPhone=lstData.getDataRef(i,"BCIV_SMS_NUMBERS").toString(); //"091432143124312"; //TOFIX
		if (strPhone.isEmpty())
			continue;

		//send through skype interface sms with subject/body to recipient_contact_id....
		SendSkypeSMS(strPhone, strSMS);
	}

	//save all invite record: new OUID/Subject ID
	_SERVER_CALL(BusCalendar->WriteInviteRecordsAfterSend(err,lstData,true));

}

void CalendarHelper::GetDefaultInviteSMS_BodyAndSubject(QString &strSubject,QString &strBody)
{
	//based on default lang code, get body:
	if (GetDefaultLanguageName()=="Deutch")
	{
		strSubject="Einladung zu [Cal_Inv_Title] von [Cal_Inv_Owner]:";

		strBody="[Cal_Inv_Subject]";
		strBody +="\n\r";
		strBody +="Wann: von [Cal_Inv_From] bis [Cal_Inv_To]";
		strBody +="\n\r";
		strBody +="Wo: [Cal_Inv_Location]";
		strBody +="\n\r";
		strBody +="Beschreibung: [Cal_Inv_Description]";
		strBody +="\n\r";
		return;

	}
	else
	{
		strSubject="Invitation to [Cal_Inv_Title] from [Cal_Inv_Owner]:";

		strBody="[Cal_Inv_Subject]";
		strBody +="\n\r";
		strBody +="When: From [Cal_Inv_From] to [Cal_Inv_To]";
		strBody +="\n\r";
		strBody +="Where: [Cal_Inv_Location]";
		strBody +="\n\r";
		strBody +="Description: [Cal_Inv_Description]";
		strBody +="\n\r";
		return;
	}
}

QString CalendarHelper::GetDefaultLanguageName()
{
	//find default template ID:
	QString strTypeName;
	MainEntitySelectionController cacheTypes;
	cacheTypes.Initialize(ENTITY_BUS_CM_TYPES);
	cacheTypes.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_LANGUAGE);
	cacheTypes.ReloadData();
	DbRecordSet *pLstTypes=cacheTypes.GetDataSource();
	//find def lang then extract email template id:	
	for (int i=0;i<pLstTypes->getRowCount();i++)
	{
		if (pLstTypes->getDataRef(i,"BCMT_IS_DEFAULT").toInt()>0)
			strTypeName=pLstTypes->getDataRef(i,"BCMT_TYPE_NAME").toInt();
	}

	return strTypeName;
}

/*
BCEV_TITLE ...summary mislim
//ovo pazi na UTC i GMT vremena: (stavi u lokalnio
BCOL_FROM
BCOL_TO
BCOL_LOCATION LOCATIOn
UID->BCIV_OUID
*/

void CalendarHelper::ParseCelandar(QString strEmailBody,DbRecordSet &data /*TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS*/ )
{



}