#include "calendarmultidaygraphicsview.h"

#include <QScrollBar>
#include "calendarmultidayentityitem.h"
#include "calendarmultidayreservationitem.h"
#include "db_core/db_core/dbsqltableview.h"

#include <QVBoxLayout>
#include <QDrag>
#include <QMimeData>
#include <QHeaderView>
#include <QToolTip>
#include <QGraphicsSceneHoverEvent>
#include <QtWidgets/QMessageBox>

#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;

TopLeftItem::TopLeftItem(int nHeight, QGraphicsView *pCalendarGraphicsView, QGraphicsItem *parent /*= 0*/, Qt::WindowFlags wFlags /*= 0*/)
: QGraphicsProxyWidget(parent, wFlags)
{
	m_nWidth = 0;
	m_nHeight = nHeight;
	m_nMinWidth = 30;
	m_pCalendarGraphicsView = pCalendarGraphicsView;
	m_nWidth = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarGraphicsView)->getVertHeaderWidth();
	m_pLabel = new Label(Label::HEADER_ITEM);
	m_pLabel->setMinimumHeight(m_nHeight);
	m_pLabel->setMaximumHeight(m_nHeight);
	m_isResizing = false;

	setWidget(m_pLabel);
	setZValue(11);
}

TopLeftItem::~TopLeftItem()
{
	m_pLabel = NULL;
	m_pCalendarGraphicsView = NULL;
}

void TopLeftItem::on_UpdateCalendar()
{
}

void TopLeftItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
	if (isInResizeArea(event->pos()))
		setCursor(Qt::SplitHCursor);
	else
		setCursor(Qt::ArrowCursor);
}

void TopLeftItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	if (m_isResizing)
	{
		m_nWidth = event->pos().x();
		int n = mapToItem(this, event->pos()).x();
		m_nWidth = n;
		if (m_nWidth < m_nMinWidth)
			m_nWidth = m_nMinWidth;
		resize(m_nWidth, m_nHeight);
		emit vertHeaderResized(m_nWidth);
	}
}

void TopLeftItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && isInResizeArea(event->pos()))
		m_isResizing = true;
	else
		QGraphicsProxyWidget::mousePressEvent(event);
}

void TopLeftItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && m_isResizing)
	{
		m_isResizing = false;
		emit vertHeaderResized(m_nWidth);
	}
	else
		QGraphicsProxyWidget::mouseReleaseEvent(event);
}

bool TopLeftItem::isInResizeArea(const QPointF &pos)
{
	int nWidth = this->widget()->width();
	if (((nWidth - 5) < pos.x()) && (pos.x() < nWidth))
		return true;
	else
		return false;
}

VerticalPersonItemDisplayWidget::VerticalPersonItemDisplayWidget(QString strEntityName, QWidget *parent/*=0*/)
: QFrame(parent)
{
	QString strSyle = "QFrame {color:white; background-color: rgb(128,128,128)}";
	setStyleSheet(strSyle);

	m_pItem1 = new QTableWidgetItem(strEntityName);
	m_pItem1->setFlags(Qt::ItemIsSelectable);

	//Create table widget.
	m_pTable = new QTableWidget(1, 1);
	m_pTable->horizontalHeader()->hide();
	m_pTable->horizontalHeader()->setStretchLastSection(true);
	m_pTable->verticalHeader()->hide();
	m_pTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pTable->setShowGrid(false);
	m_pTable->setSelectionMode(QAbstractItemView::MultiSelection);
	m_pTable->setEditTriggers(QAbstractItemView::NoEditTriggers);

	m_pTable->setItem(0,0,m_pItem1);

	QVBoxLayout *layout = new QVBoxLayout();
	layout->setContentsMargins(0,0,0,0);
	layout->addWidget(m_pTable);
	layout->setSizeConstraint(QLayout::SetMaximumSize);
	setLayout(layout);
}

void VerticalPersonItemDisplayWidget::setText(QString strText)
{
	m_pItem1->setText(strText);
}

void VerticalPersonItemDisplayWidget::setSelected(bool bSelected)
{
	if (bSelected)
	{
		QString strSyle = "QFrame {color:black; background-color:white )}";
		setStyleSheet(strSyle);
	}
	else
	{
		QString strSyle = "QFrame {color:white; background-color: rgb(128,128,128)}";
		setStyleSheet(strSyle);
	}
}

void VerticalPersonItemDisplayWidget::resizeTable(int nWidth, int nHeight)
{
	m_pTable->setMaximumWidth(nWidth);
	m_pTable->setMaximumHeight(nHeight);
}

VerticalPersonItem::VerticalPersonItem(int nRow, int nEntityID, int nEntityType, int nEntityPersonID, QString strText, QGraphicsView *pCalendarMultiDayGraphicsView, CalendarHeaderTableItem *pCalendarHeaderTableItem)
{
	m_nRow		= nRow;
	m_nEntityID = nEntityID;
	m_nEntityType= nEntityType;
	m_nEntityPersonID= nEntityPersonID;
	m_strText	= strText;
	m_pCalendarMultiDayGraphicsView = pCalendarMultiDayGraphicsView;
	m_pCalendarHeaderTableItem		= pCalendarHeaderTableItem;
	m_bSelected=false;

	m_pWidg = new VerticalPersonItemDisplayWidget(strText);
	setWidget(m_pWidg);
	setZValue(11);
	//setAcceptHoverEvents(true);
	setToolTip(strText);
	connect(pCalendarMultiDayGraphicsView, SIGNAL(UpdateCalendar()), this, SLOT(on_UpdateCalendar()));
	g_ChangeManager.registerObserver(this); 
}

VerticalPersonItem::~VerticalPersonItem()
{
	g_ChangeManager.unregisterObserver(this); 
}

void VerticalPersonItem::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail/*=0*/,const QVariant val/*=QVariant()*/)
{
	if(nMsgDetail==ENTITY_BUS_PERSON && m_nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT && nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
	{
		DbRecordSet rec = val.value<DbRecordSet>();
		if (m_nEntityPersonID==rec.getDataRef(0, "BPER_ID").toInt())
		{
			m_pWidg->setText(rec.getDataRef(0, "BPER_NAME").toString());
		}
	}
	if(nMsgDetail==ENTITY_BUS_PROJECT && m_nEntityType==GlobalConstants::TYPE_CAL_PROJECT_SELECT && nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
	{
		DbRecordSet rec = val.value<DbRecordSet>();
		if (m_nEntityID==rec.getDataRef(0, "BUSP_ID").toInt())
		{
			m_pWidg->setText(rec.getDataRef(0, "BUSP_NAME").toString());
		}
	}
	if(nMsgDetail==ENTITY_BUS_CONTACT && m_nEntityType==GlobalConstants::TYPE_CAL_CONTACT_SELECT && nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
	{
		DbRecordSet rec = val.value<DbRecordSet>();
		if (m_nEntityID==rec.getDataRef(0, "BCNT_ID").toInt())
		{
			m_pWidg->setText(rec.getDataRef(0, "BCNT_NAME").toString());
		}
	}
	if(nMsgDetail==ENTITY_CALENDAR_RESOURCES && m_nEntityType==GlobalConstants::TYPE_CAL_RESOURCE_SELECT && nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
	{
		DbRecordSet rec = val.value<DbRecordSet>();
		if (m_nEntityID==rec.getDataRef(0, "BRES_ID").toInt())
		{
			m_pWidg->setText(rec.getDataRef(0, "BRES_NAME").toString());
		}
	}
}

bool VerticalPersonItem::eventFilter(QObject *watched, QEvent *event)
{
	if (watched == m_pWidg) 
	{
		if (event->type() == QEvent::ToolTip) 
		{
			QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
			QPoint pos = helpEvent->globalPos();
			QToolTip::showText(pos, m_strText);

			return true;
		} 
		else
			return QGraphicsProxyWidget::eventFilter(watched, event);
	}
		
	return QGraphicsProxyWidget::eventFilter(watched, event);
}

void VerticalPersonItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget /*= 0*/)
{
	QGraphicsProxyWidget::paint(painter, option, widget);
	
	painter->setPen(Qt::white);
	if (m_nRow==1)
		painter->drawLine(option->rect.topLeft().x(), option->rect.topLeft().y(), option->rect.topRight().x(), option->rect.topRight().y());
	
	painter->drawLine(option->rect.bottomLeft().x(), option->rect.bottomLeft().y(), option->rect.bottomRight().x(), option->rect.bottomRight().y());
}

void VerticalPersonItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() != Qt::LeftButton) {
		event->ignore();
		return;
	}
}

void VerticalPersonItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		m_bSelected=!m_bSelected;
		m_pWidg->setSelected(m_bSelected);
		dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->SelectDeselectEntityItemsInLeftColumn(m_bSelected, m_nEntityID, m_nEntityType, m_nEntityPersonID, m_strText);
	}
}

void VerticalPersonItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	QDrag *drag = new QDrag(event->widget());
	QMimeData *mimeData = new QMimeData;
	QString strData = QString("entity_item")+";"+QVariant(m_nEntityID).toString()+";"+QVariant(m_nEntityType).toString()+";"+QVariant(m_nEntityPersonID).toString()+";"+QVariant(m_nRow).toString();
	mimeData->setText(strData);
	drag->setMimeData(mimeData);
	QPixmap pix;
	if (m_nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
		pix = QPixmap(":Contacts16.png");
	if (m_nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT)
		pix = QPixmap(":Contacts16.png");
	if (m_nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
		pix = QPixmap(":Projects16.png");
	if (m_nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
		pix = QPixmap(":Resource16.png");

	drag->setPixmap(pix);
	drag->exec();
}

void VerticalPersonItem::setSelected(bool bSelected)
{
	if (m_bSelected==bSelected)
		return;
	
	m_bSelected=bSelected;
	m_pWidg->setSelected(m_bSelected);
	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->SelectDeselectEntityItemsInLeftColumn(m_bSelected, m_nEntityID, m_nEntityType, m_nEntityPersonID, m_strText);
}

void VerticalPersonItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	QMenu CnxtMenu;

	if (!m_bSelected)
	{
		QAction* pAction4 = new QAction(tr("Select All"), this);
		connect(pAction4, SIGNAL(triggered()), dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView), SLOT(on_selectAll()));
		CnxtMenu.addAction(pAction4);
		QAction* pAction5 = new QAction(tr("Deselect All"), this);
		connect(pAction5, SIGNAL(triggered()), dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView), SLOT(on_deselectAll()));
		CnxtMenu.addAction(pAction5);

		CnxtMenu.exec(event->screenPos());
	}
	else
	{
		QAction* pAction = new QAction(tr("Define Availability"), this);
		connect(pAction, SIGNAL(triggered()), dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView), SLOT(on_defineAvailability()));
		CnxtMenu.addAction(pAction);

		QAction* pAction1 = new QAction(tr("Mark as Final"), this);
		connect(pAction1, SIGNAL(triggered()), dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView), SLOT(on_markAsFinal()));
		CnxtMenu.addAction(pAction1);

		QAction* pAction2 = new QAction(tr("Mark as Preliminary"), this);
		connect(pAction2, SIGNAL(triggered()), dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView), SLOT(on_markAsPreliminary()));
		CnxtMenu.addAction(pAction2);

		QAction* pAction3 = new QAction(tr("Select All"), this);
		connect(pAction3, SIGNAL(triggered()), dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView), SLOT(on_selectAll()));
		CnxtMenu.addAction(pAction3);

		QAction* pAction6 = new QAction(tr("Deselect All"), this);
		connect(pAction6, SIGNAL(triggered()), dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView), SLOT(on_deselectAll()));
		CnxtMenu.addAction(pAction6);

		CnxtMenu.exec(event->screenPos());
	}
}

void VerticalPersonItem::on_UpdateCalendar()
{
	QRect rect;
	rect = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getTableView()->getItemRectangleForVertPersonHeader(m_nRow);
	setPos(0, rect.topLeft().y()+dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getHeaderHeight());
	m_pWidg->resize(dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getVertHeaderWidth()-1, rect.height());
	m_pWidg->resizeTable(dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getVertHeaderWidth()-1, rect.height());
	resize(dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarMultiDayGraphicsView)->getVertHeaderWidth()-1, rect.height());
}

MultiDayCalendarHeaderItem::MultiDayCalendarHeaderItem(bool bIsInMultiDayCalendar, int nColumn, QString	strString, int nHeight, QGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent /*= 0*/, Qt::WindowFlags wFlags /*= 0*/)
:CalendarHeaderItem(bIsInMultiDayCalendar, nColumn, strString, nHeight, pCalendarGraphicsView, parent, wFlags)
{
}

void MultiDayCalendarHeaderItem::ConnectSignals()
{
	connect(dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarGraphicsView), SIGNAL(UpdateCalendar()), this, SLOT(on_UpdateCalendar()));
	connect(this, SIGNAL(columnResized(int, int, bool)), dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarGraphicsView), SLOT(on_columnResized(int, int, bool)));
}

void MultiDayCalendarHeaderItem::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	const QMimeData *mimeData = event->mimeData();
	QStringList lstData = mimeData->text().split(";");
	if (lstData.value(0) == "entity_item")
		event->accept();
	else
		event->ignore();
}

void MultiDayCalendarHeaderItem::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{

}

void MultiDayCalendarHeaderItem::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{

}

void MultiDayCalendarHeaderItem::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	const QMimeData *mimeData = event->mimeData();
	QStringList lstData = mimeData->text().split(";");
	int nDraggedEntityID = lstData.value(1).toInt();
	int nDraggedEntityType = lstData.value(2).toInt();
	int nDraggedEntityPersonID = lstData.value(3).toInt();

	dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarGraphicsView)->on_dropEntityOnHeader(nDraggedEntityID, nDraggedEntityType, nDraggedEntityPersonID, m_nColumn);
}

void MultiDayCalendarHeaderItem::on_UpdateCalendar()
{
	QRect rect = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarGraphicsView)->getTableView()->getItemRectangleForHeader(m_nColumn);
	setPos(rect.topLeft().x()+dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarGraphicsView)->getVertHeaderWidth(), 0);
	resize(rect.width()-1, m_nHeight);
}

CalendarHeaderTableModel::CalendarHeaderTableModel(QObject *parent /*= 0*/) : QAbstractTableModel(parent)
{
	m_recEntites = NULL;
}

CalendarHeaderTableModel::~CalendarHeaderTableModel()
{
	m_recEntites = NULL;
}

void CalendarHeaderTableModel::Initialize(QDate startDate, int nDayInterval, DbRecordSet *recEntites)
{
	m_startDate		= startDate;
	m_nDayInterval  = nDayInterval;
	m_recEntites	= recEntites;
}

QVariant CalendarHeaderTableModel::data(const QModelIndex &index, int role /*= Qt::DisplayRole*/) const
{
	return QVariant();
}

int CalendarHeaderTableModel::rowCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
	return m_recEntites->getRowCount()+2;
}

int CalendarHeaderTableModel::columnCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
	return m_nDayInterval;
}

int CalendarHeaderTableModel::getRowForEntity(int nEntityID, int nEntityType)
{
	m_recEntites->clearSelection();
	m_recEntites->find("ENTITY_ID", nEntityID);
	m_recEntites->find("ENTITY_TYPE", nEntityType, false, true, true);
	int nRow = m_recEntites->getSelectedRow();
	return nRow+1;
}

QDateTime CalendarHeaderTableModel::getItemStartDateTime(QModelIndex &index)
{
	QDate date = m_startDate.addDays(index.column());
	QTime time(0, 0);
	return QDateTime(date, time);
}

QDateTime CalendarHeaderTableModel::getItemEndDateTime(QModelIndex &index)
{
	QDate date = m_startDate.addDays(index.column());
	QTime time(23, 59, 59);
	return QDateTime(date, time);
}

QVariant CalendarHeaderTableModel::headerData(int section, Qt::Orientation orientation, int role /*= Qt::DisplayRole*/) const
{
	if (role == Qt::DisplayRole)
	{
		if(orientation == Qt::Horizontal)
		{
			//QString s = m_startDate.toString();
			return m_startDate.addDays(section);
		}
		else
			return section+1;
	}

	return QVariant();
}

void CalendarHeaderTableModel::getItem(int nEntityID, int nEntityType, QDateTime &dateTime, QModelIndex &Index)
{
	int nRow = getRowForEntity(nEntityID, nEntityType);
	QDate dtTmp = dateTime.date();
	int nColumn = getColumnForDate(dtTmp);

	Index = index(nRow, nColumn);
}

void CalendarHeaderTableModel::getEntityForRow(int nRow, int nColumn, int &nEntityID, int &nEntityType, int &nEntityPersonID)
{
	if (nRow == 0 || m_recEntites->getRowCount() < nRow)
	{
		nEntityType = -1;
		nEntityID = -1;
		nEntityPersonID = -1;
		return;
	}
	nEntityType = m_recEntites->getDataRef(nRow-1, "ENTITY_TYPE").toInt();
	nEntityID = m_recEntites->getDataRef(nRow-1, "ENTITY_ID").toInt();
	nEntityPersonID = m_recEntites->getDataRef(nRow-1, "ENTITY_PERSON_ID").toInt();
}

int CalendarHeaderTableModel::getColumnForDate(QDate &date)
{
	return m_startDate.daysTo(date);
}

CalendarMultiDayGraphicsView::CalendarMultiDayGraphicsView(CalendarWidget *pCalendarWidget, QWidget *parent)
	: QGraphicsView(parent)
{
	m_pCalendarWidget			= pCalendarWidget;
	m_pCalendarHeaderTableModel = new CalendarHeaderTableModel();
	m_pGraphicsScene			= new CalendarGraphicsScene();
	setScene(m_pGraphicsScene);

	m_nRowHeightIncrement = 10;
	m_nEntityColumnWidth = 200;
	m_nRowHeight = 30;
	m_bResizeFromShowEvent = false;
	m_bFirstTimeShow = true;
	m_recEntites=NULL;
	m_recSelectedEntityItemsInLeftColumn.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));

	//Background item.
	Label *label = new Label(Label::VERTICAL_ITEM);
	m_pBackgroundItem = new QGraphicsProxyWidget();
	m_pBackgroundItem->setWidget(label);
	m_pBackgroundItem->setZValue(0);
	m_pBackgroundItem->setPos(0,0);
	m_pBackgroundItem->setMinimumHeight(viewport()->rect().height());
	m_pBackgroundItem->setMaximumHeight(viewport()->rect().height());
	m_pBackgroundItem->setMinimumWidth(viewport()->rect().width());
	m_pBackgroundItem->setMaximumWidth(viewport()->rect().width());
	m_pGraphicsScene->addItem(m_pBackgroundItem);

	//Multi day header.
	m_pCalendarHeaderTableView = new CalendarHeaderTableItem(this, m_pCalendarHeaderTableModel);
	m_pGraphicsScene->addItem(m_pCalendarHeaderTableView);
	m_pCalendarHeaderTableView->setZValue(1);

	//Scroll bars.
	m_pHScroll = new QScrollBar(Qt::Horizontal);
	m_pVScroll = new QScrollBar(Qt::Vertical);
	m_pHScrollBar = m_pGraphicsScene->addWidget(m_pHScroll);
	m_pVScrollBar = m_pGraphicsScene->addWidget(m_pVScroll);
	m_pHScrollBar->setZValue(3);
	m_pVScrollBar->setZValue(3);
	m_pHScrollBar->hide();
	//m_pVScrollBar->hide();
	connect(m_pHScroll, SIGNAL(valueChanged(int)), this, SLOT(on_CustomHscrollBar_valueChanged(int)));
	connect(m_pVScroll, SIGNAL(valueChanged(int)), this, SLOT(on_CustomVscrollBar_valueChanged(int)));

	//Create corner items.
	CreateCornerScrollBarItems();
	m_pRightDownScrollBarItem->hide();
	connect(m_pCalendarHeaderTableView, SIGNAL(horizontalScrollBarVisible(bool)), this, SLOT(on_horizontalScrollBarVisible(bool)));
	connect(m_pCalendarHeaderTableView, SIGNAL(verticalScrollBarVisible(bool)), this, SLOT(on_verticalScrollBarVisible(bool)));
	connect(m_pCalendarHeaderTableView->Table()->horizontalScrollBar(), SIGNAL(rangeChanged(int, int)), this, SLOT(on_HscrollBar_rangeChanged(int, int)));
	connect(m_pCalendarHeaderTableView->Table()->verticalScrollBar(), SIGNAL(rangeChanged(int, int)), this, SLOT(on_VscrollBar_rangeChanged(int, int)));
	connect(m_pCalendarHeaderTableView->Table()->horizontalScrollBar(), SIGNAL(sliderMoved(int)), this, SLOT(on_horizontalScrollBar_sliderMoved(int)));
	connect(m_pCalendarHeaderTableView->Table()->verticalScrollBar(), SIGNAL(sliderMoved(int)), this, SLOT(on_verticalScrollBar_sliderMoved(int)));

	connect(m_pCalendarHeaderTableView->Table()->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(on_VscrollBar_valueChanged(int)));
	connect(m_pCalendarHeaderTableView->Table()->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(on_HscrollBar_valueChanged(int)));
	connect(this, SIGNAL(UpdateCalendar()), m_pCalendarHeaderTableView, SLOT(on_UpdateCalendar()));

	connect(m_pCalendarHeaderTableView->Table(), SIGNAL(SelectionChanged(QModelIndex, QModelIndex)), this, SLOT(on_SelectionChanged(QModelIndex, QModelIndex)));
	connect(m_pCalendarHeaderTableView, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(on_tableCellDoubleClicked(int, int)));
	
	setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
}

CalendarMultiDayGraphicsView::~CalendarMultiDayGraphicsView()
{
	m_pGraphicsScene		= NULL;
	m_pBackgroundItem		= NULL;
	m_pLeftScrollBarItem	= NULL;
	m_pTopRightScrollBarItem= NULL;
	m_pTopLeftScrollBarItem	= NULL;
	m_pRightDownScrollBarItem= NULL;
	m_pTopLeftItem			= NULL;
	m_pHScrollBar			= NULL;
	m_pHScroll				= NULL;
	m_pVScrollBar			= NULL;
	m_pVScroll				= NULL;
	m_recEntites			= NULL;
}

void CalendarMultiDayGraphicsView::Initialize(QDate &startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites, bool bResizeColumnsToFit /*= false*/)
{
	m_recSelectedEntityItemsInLeftColumn.clear();
	DestroyCalendarHeader();
	DestroyPersonItems();
	RemoveAllCalendarItems();

	//Initialize table view.
	m_recEntites = recEntites;
	m_pCalendarHeaderTableModel->Initialize(startDate, nDayInterval, m_recEntites);
	m_pCalendarHeaderTableView->Initialize();

	//Create headers.
	CreateCalendarHeader();
	CreateEntityItems();

	PositionCustomScrollBars();
	PositionCornerScrollBarItems();

	//Resize columns to fit.
	if (bResizeColumnsToFit)
		ResizeColumnsToFit();
	
	//Draw changes.
	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::AddDateHeader()
{
	CreateCalendarHeader();
	m_pCalendarHeaderTableView->Table()->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	emit UpdateCalendar();
}

CalendarHeaderTableItem* CalendarMultiDayGraphicsView::getTableView()
{
	return m_pCalendarHeaderTableView;
}

int CalendarMultiDayGraphicsView::getVertHeaderWidth()
{
	if (m_pCalendarWidget->getCalendarViewSections()==0)
	{
		return m_pCalendarWidget->getVertHeaderWidthForUpperCalendar();
	}
	else
	{
		return m_nEntityColumnWidth;
	}
}

void CalendarMultiDayGraphicsView::setVertHeaderWidth(int nWidth)
{
	m_nEntityColumnWidth = nWidth;
}

int CalendarMultiDayGraphicsView::getEntityCount()
{
	if (!m_recEntites)
		return 0;
	
	return m_recEntites->getRowCount();
}

int CalendarMultiDayGraphicsView::getHeaderHeight()
{
	if (m_recEntites->getRowCount()>1)
		return getMultiHeaderHeight();
	else
		return 0;
}

void CalendarMultiDayGraphicsView::AddMultiDayCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, 
				QDateTime startDateTime, QDateTime endDateTime, QDateTime correctedStartDateTime, QDateTime correctedEndDateTime, QString strTitle, 
				QString strLocation, QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, 
				QString strProjects, QString strDescription, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed, QGraphicsItem* &Item)
{
	//CheckItemDateTimeRange(startDateTime, endDateTime);
	CalendarMultiDayEntityItem *item = new CalendarMultiDayEntityItem(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, startDateTime, 
		endDateTime, correctedStartDateTime, correctedEndDateTime, strTitle, strLocation, strColor, pixIcon, strOwner, strResource, strContact, 
		strProjects, strDescription, this, nBcep_Status, nBCEV_IS_INFORMATION, bIsProposed);
	m_lstMultiDayCalendarEntityItems.insert(nCentID, item);

	Item = item;
	//CheckForMultipleItemsInRow(item);
	m_pGraphicsScene->addItem(item);
	connect(item, SIGNAL(ItemResized(QGraphicsProxyWidget *, bool)), this, SLOT(on_ItemResized(QGraphicsProxyWidget *, bool)));
	connect(item, SIGNAL(ItemModify(QGraphicsProxyWidget *)), this, SLOT(on_ItemModify(QGraphicsProxyWidget *)));
	connect(item, SIGNAL(ItemDelete(QGraphicsProxyWidget *)), this, SLOT(on_ItemDelete(QGraphicsProxyWidget *)));
	connect(item, SIGNAL(DropEntityOnItem(QGraphicsProxyWidget *, int, int, int)), this, SLOT(on_DropEntityOnItem(QGraphicsProxyWidget *, int, int, int)));
	connect(item, SIGNAL(ItemMoved(QGraphicsProxyWidget *)), this, SLOT(on_ItemMoved(QGraphicsProxyWidget *)));
	connect(this, SIGNAL(UpdateCalendar()), item, SLOT(on_UpdateCalendar()));
	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::UpdateMultiDayCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime startDateTime, QDateTime endDateTime, 
	QDateTime correctedStartDateTime, QDateTime correctedEndDateTime, QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, 
	QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, int nBcep_Status, 
	int nBCEV_IS_INFORMATION, bool bIsProposed, QGraphicsItem* &Item)
{
	QHashIterator<int, QGraphicsItem*> i(m_lstMultiDayCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		if (i.key() == nCentID)
		{
			CalendarMultiDayEntityItem *item = dynamic_cast<CalendarMultiDayEntityItem*>(i.value());
			int nEntityID = item->getItemEntityID();
			int nEntityType = item->getItemEntityType();
			int nItemBCOL_ID = item->getItemBCOL_ID();
			if (nItemBCOL_ID == nBCOL_ID)
			{
				item->UpdateMultyDayEntityItem(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, startDateTime, endDateTime, 
					correctedStartDateTime, correctedEndDateTime, strTitle, strLocation, strColor, pixIcon, strOwner, strResource, strContact, 
					strProjects, strDescription, nBcep_Status, nBCEV_IS_INFORMATION, bIsProposed);
				Item = item;
			}
		}
	}

	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::RemoveCalendarItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QList<QGraphicsItem*> &Items)
{
	QHashIterator<int, QGraphicsItem*> i(m_lstMultiDayCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		if (i.key() == nCentID)
		{
			CalendarMultiDayEntityItem *item = dynamic_cast<CalendarMultiDayEntityItem*>(i.value());
			int nItemBCOL_ID = item->getItemBCOL_ID();
			if (nItemBCOL_ID == nBCOL_ID)
			{
				Items << item;
				m_lstMultiDayCalendarEntityItems.remove(i.key(), i.value());
				m_pGraphicsScene->removeItem(item);
				delete(item);
			}
		}
	}

	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::AddMultiDayCalendarReservation(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, 
									QDateTime startDateTime, QDateTime endDateTime)
{
	CalendarMultiDayReservationItem *item = new CalendarMultiDayReservationItem(nEntityID, nEntityType, nBCRS_ID, nBCRS_IS_POSSIBLE, startDateTime, endDateTime, this);
	m_pGraphicsScene->addItem(item);
	m_lstCalendarMultiDayReservationItems.insert(nBCRS_ID, item);
	connect(this, SIGNAL(UpdateCalendar()), dynamic_cast<CalendarMultiDayReservationItem*>(item), SLOT(on_UpdateCalendar()));

	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::RemoveAllCalendarItems()
{
	//Remove calendar items.
	QHashIterator<int, QGraphicsItem*> i(m_lstMultiDayCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		QGraphicsItem *item = i.value();
		m_pGraphicsScene->removeItem(item);
		//delete(item);
	}

	//Remove calendar reservations.
	QHashIterator<int, QGraphicsItem*> j(m_lstCalendarMultiDayReservationItems);
	while (j.hasNext()) 
	{
		j.next();
		QGraphicsItem *resItem = j.value();
		m_pGraphicsScene->removeItem(resItem);
		//delete(resItem);
	}

	qDeleteAll(m_lstMultiDayCalendarEntityItems);
	m_lstMultiDayCalendarEntityItems.clear();
	qDeleteAll(m_lstCalendarMultiDayReservationItems);
	m_lstCalendarMultiDayReservationItems.clear();
}

void CalendarMultiDayGraphicsView::ResizeColumnsToFit(bool bForce /*= false*/)
{
	if (m_recEntites->getRowCount()<=1 && !bForce)
		return;

	//If columns are shorter than view width resize to fit.
	int nColumnWidth = dynamic_cast<CalendarHeaderTableItem*>(m_pCalendarHeaderTableView)->Table()->columnWidth(0);
	int nColumnsWidth = nColumnWidth*dynamic_cast<CalendarHeaderTableItem*>(m_pCalendarHeaderTableView)->Model()->columnCount();
	int nViewPortWidth = viewport()->rect().width()-getVertHeaderWidth()-m_pVScroll->width();

	m_nColumnWidth = (viewport()->rect().width()-getVertHeaderWidth()-m_pVScroll->width())/(dynamic_cast<CalendarHeaderTableItem*>(m_pCalendarHeaderTableView)->Model()->columnCount());
	on_columnResized(0, m_nColumnWidth, true);
}

void CalendarMultiDayGraphicsView::resizeEvent(QResizeEvent *event)
{
	m_pGraphicsScene->setSceneRect(viewport()->rect());

	m_pGraphicsScene->setSceneRect(viewport()->rect());
	m_pBackgroundItem->setMinimumHeight(viewport()->rect().height());
	m_pBackgroundItem->setMaximumHeight(viewport()->rect().height());
	m_pBackgroundItem->setMinimumWidth(viewport()->rect().width());
	m_pBackgroundItem->setMaximumWidth(viewport()->rect().width());
	
	//Multi person header item.
	if (m_recEntites==NULL)
		return;

	if (m_recEntites->getRowCount()> 1)
	{
		m_pCalendarHeaderTableView->setPos(m_pCalendarWidget->getVertHeaderWidthForUpperCalendar(), m_pCalendarWidget->getDateHeaderHeight());
		m_pCalendarHeaderTableView->resize(viewport()->rect().width()-m_pCalendarWidget->getVertHeaderWidthForUpperCalendar(), viewport()->rect().height()-m_pCalendarWidget->getDateHeaderHeight());
		m_pCalendarHeaderTableView->Table()->resize(viewport()->rect().width()-m_pCalendarWidget->getVertHeaderWidthForUpperCalendar(), viewport()->rect().height()-m_pCalendarWidget->getDateHeaderHeight());
		//m_pCalendarHeaderTableView->Table()->setRowHeight(0, viewport()->rect().height()-m_pCalendarWidget->getDateHeaderHeight());
	}
	else
	{
		m_pCalendarHeaderTableView->setPos(m_pCalendarWidget->getVertHeaderWidthForUpperCalendar(), 0);
		m_pCalendarHeaderTableView->resize(viewport()->rect().width()-m_pCalendarWidget->getVertHeaderWidthForUpperCalendar(),viewport()->rect().height());
		m_pCalendarHeaderTableView->Table()->resize(viewport()->rect().width()-m_pCalendarWidget->getVertHeaderWidthForUpperCalendar(), viewport()->rect().height());
		//m_pCalendarHeaderTableView->Table()->setRowHeight(0, viewport()->rect().height());
	}

	PositionCustomScrollBars();
	PositionCornerScrollBarItems();
	
	if ((m_bResizeFromShowEvent && m_bFirstTimeShow))
	{
		m_bFirstTimeShow = false;
		m_bResizeFromShowEvent = false;	
		ResizeColumnsToFit(true);
	}
	else if (AllColumnsAreInViewArea(event->size().width()) && (event->oldSize().width()!=event->size().width()))
		ResizeColumnsToFit(true);
}

void CalendarMultiDayGraphicsView::showEvent(QShowEvent *event)
{
	m_bResizeFromShowEvent = true;
}

void CalendarMultiDayGraphicsView::on_UpdateCalendar()
{
	m_pCalendarHeaderTableView->on_UpdateCalendar();
}

void CalendarMultiDayGraphicsView::on_scrollBar_valueChanged(int value)
{
	m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->setValue(value);
}

void CalendarMultiDayGraphicsView::on_HscrollBar_valueChanged(int value)
{
	m_pHScroll->setValue(value);
	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::on_VscrollBar_valueChanged(int value)
{
	m_pVScroll->setValue(value);
	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::on_CustomVscrollBar_valueChanged(int value)
{
	m_pCalendarHeaderTableView->Table()->verticalScrollBar()->setValue(value);
}

void CalendarMultiDayGraphicsView::on_CustomHscrollBar_valueChanged(int value)
{
	m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->setValue(value);
}

void CalendarMultiDayGraphicsView::on_horizontalScrollBarVisible(bool bVisible)
{
	if(bVisible)
	{
		m_pLeftScrollBarItem->show();
		m_pHScrollBar->show();
		if (m_pVScrollBar->isVisible())
			m_pRightDownScrollBarItem->show();
		else
			m_pRightDownScrollBarItem->hide();
	}
	else
	{
		m_pLeftScrollBarItem->hide();
		m_pRightDownScrollBarItem->hide();
		m_pHScrollBar->hide();
	}
	PositionCustomScrollBars();
}

void CalendarMultiDayGraphicsView::on_verticalScrollBarVisible(bool bVisible)
{
	if(bVisible)
	{
		m_pTopRightScrollBarItem->show();
		m_pVScrollBar->show();
		if (m_pHScrollBar->isVisible())
			m_pRightDownScrollBarItem->show();
		else
			m_pRightDownScrollBarItem->hide();
	}
	else
	{
		m_pTopRightScrollBarItem->hide();
		m_pRightDownScrollBarItem->hide();
		m_pVScrollBar->hide();
	}
	PositionCustomScrollBars();
}

void CalendarMultiDayGraphicsView::on_VscrollBar_rangeChanged(int min, int max)
{
	m_pVScroll->resize(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->width(), m_pCalendarHeaderTableView->Table()->verticalScrollBar()->height());
	m_pVScroll->setRange(min, max);
	m_pVScroll->setPageStep(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->pageStep());
}

void CalendarMultiDayGraphicsView::on_HscrollBar_rangeChanged(int min, int max)
{
	m_pHScroll->resize(m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->width(), m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->height());
	m_pHScroll->setRange(min, max);
	m_pHScroll->setPageStep(m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->pageStep());
}

void CalendarMultiDayGraphicsView::on_horizontalScrollBar_sliderMoved(int value)
{
	m_pHScroll->setValue(value);
}

void CalendarMultiDayGraphicsView::on_verticalScrollBar_sliderMoved(int value)
{
	m_pVScroll->setValue(value);
}

void CalendarMultiDayGraphicsView::on_columnResizedFromSimpleCalendar(int nColumn, int nWidth, bool bResizeAll)
{
	if (m_recEntites==NULL)
		return;
	if (m_recEntites->getRowCount() > 1)
		return;

	m_pCalendarHeaderTableView->resizeColumn(nColumn, nWidth, bResizeAll);

	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::on_columnResized(int nColumn, int nWidth, bool bResizeAll)
{
	if (bResizeAll)
	{
		int nColumnCount = m_pCalendarHeaderTableView->Model()->columnCount();
		for (int i = 0; i < nColumnCount; ++i)
			m_pCalendarHeaderTableView->setColumnWidth(i, nWidth);
	}
	else
	{
		m_pCalendarHeaderTableView->setColumnWidth(nColumn, nWidth);
	}

	m_nColumnWidth = nWidth;

	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::on_ItemModify(QGraphicsProxyWidget *thisItem)
{
	int nCentID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBCOL_ID();
	emit ItemModify(nCentID, nBcepID, nBcevID, nBCOL_ID);
}

void CalendarMultiDayGraphicsView::on_DropEntityOnItem(QGraphicsProxyWidget *thisItem, int nEntityID, int nEntityType, int nEntityPersonID)
{
	int nCentID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBCOL_ID();
	int nDropEntityID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemEntityID();
	int nDropEntityType = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemEntityType();
	int nDropItemRow = m_pCalendarHeaderTableModel->getRowForEntity(nDropEntityID, nDropEntityType)-1;
	bool bDroppedOnFirstRow = false;
	if(!nDropItemRow)
		bDroppedOnFirstRow = true;
	emit DropEntityOnItem(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, nEntityPersonID, bDroppedOnFirstRow);
}

void CalendarMultiDayGraphicsView::on_ItemDelete(QGraphicsProxyWidget *thisItem)
{
	int nCentID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBCOL_ID();
	emit ItemDelete(nCentID, nBcepID, nBcevID, nBCOL_ID);
}

void CalendarMultiDayGraphicsView::on_ItemResized(QGraphicsProxyWidget *thisItem, bool bLeftBorder)
{
	QRectF rect0 = thisItem->boundingRect();
	QRectF rectScene = thisItem->mapRectToScene(rect0);
	QRectF rectl = m_pCalendarHeaderTableView->mapRectFromScene(rectScene);

	QModelIndex topLeftIndex = m_pCalendarHeaderTableView->Table()->indexAt(rectl.topLeft().toPoint());
	QModelIndex bottomRightIndex = m_pCalendarHeaderTableView->Table()->indexAt(QPoint(rectl.bottomRight().x()-1, rectl.bottomRight().y()-1));

	if (!bottomRightIndex.isValid())
	{
		int nColumn = m_pCalendarHeaderTableView->Model()->columnCount()-1;
		int nRow = m_pCalendarHeaderTableView->Table()->rowAt(rectl.bottomRight().y());
		bottomRightIndex = m_pCalendarHeaderTableView->Model()->index(nRow, nColumn);
	}

	QDateTime datSelectionFrom = m_pCalendarHeaderTableModel->getItemStartDateTime(topLeftIndex);
	QDateTime datSelectionTo = m_pCalendarHeaderTableModel->getItemEndDateTime(bottomRightIndex);

	QRect startItemRect = m_pCalendarHeaderTableView->Table()->visualRect(topLeftIndex);
	QRect entItemRect = m_pCalendarHeaderTableView->Table()->visualRect(bottomRightIndex);

	QDateTime datItemFrom, datItemTo;
	dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getStartEndDateTime(datItemFrom, datItemTo);

	if (bLeftBorder)
	{
		qreal nRatio = (rectl.topLeft().x() - startItemRect.topLeft().x())/startItemRect.width();
		int nHour = nRatio*24;
		datSelectionFrom.setTime(QTime(nHour, 0));
		datSelectionTo = datItemTo;
	}
	else
	{
		qreal nRatio = (entItemRect.bottomRight().x() - rectl.bottomRight().x())/entItemRect.width();
		int nHour = 24 - nRatio*24;
		datSelectionTo.setTime(QTime(nHour, 0));
		datSelectionFrom = datItemFrom;
	}

	int nEntityID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemEntityID();
	int nEntityType = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemEntityType();
	int nCentID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBCOL_ID();

	QString s = datSelectionFrom.toString();
	QString ss = datSelectionTo.toString();
	QString sss = datItemFrom.toString();
	QString ssss = datItemTo.toString();

	if (!datSelectionFrom.isValid() || !datSelectionTo.isValid())
	{
		emit UpdateCalendar();
		return;
	}

	emit MultiDayItemResized(datSelectionFrom, datSelectionTo, nEntityID, nEntityType, nCentID, nBcepID, nBcevID, nBCOL_ID, datItemFrom, datItemTo);
	on_ItemResizing(false);
}

void CalendarMultiDayGraphicsView::on_ItemMoved(QGraphicsProxyWidget *thisItem)
{
	QRectF rect0 = thisItem->boundingRect();
	QRectF rectScene = thisItem->mapRectToScene(rect0);
	QRectF rectl = m_pCalendarHeaderTableView->mapRectFromScene(rectScene);

	QModelIndex topLeftIndex = m_pCalendarHeaderTableView->Table()->indexAt(rectl.topLeft().toPoint());
	QModelIndex bottomRightIndex = m_pCalendarHeaderTableView->Table()->indexAt(QPoint(rectl.bottomRight().x()-1, rectl.bottomRight().y()-1));

	if (!bottomRightIndex.isValid())
	{
		int nColumn = m_pCalendarHeaderTableView->Model()->columnCount()-1;
		int nRow = m_pCalendarHeaderTableView->Table()->rowAt(rectl.bottomRight().y());
		bottomRightIndex = m_pCalendarHeaderTableView->Model()->index(nRow, nColumn);
	}

	QDateTime datSelectionFrom = m_pCalendarHeaderTableModel->getItemStartDateTime(topLeftIndex);
	QDateTime datSelectionTo = m_pCalendarHeaderTableModel->getItemEndDateTime(bottomRightIndex);

	QRect startItemRect = m_pCalendarHeaderTableView->Table()->visualRect(topLeftIndex);
	QRect entItemRect = m_pCalendarHeaderTableView->Table()->visualRect(bottomRightIndex);

	QDateTime datItemFrom, datItemTo;
	dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getStartEndDateTime(datItemFrom, datItemTo);

	qreal nRatio = (rectl.topLeft().x() - startItemRect.topLeft().x())/startItemRect.width();
	int nHour = nRatio*24;
	datSelectionFrom.setTime(QTime(nHour, 0));

	int nEntityID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemEntityID();
	int nEntityType = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemEntityType();
	int nCentID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarMultiDayEntityItem*>(thisItem)->getItemBCOL_ID();

	QString s = datSelectionFrom.toString();
	QString ss = datSelectionTo.toString();
	QString sss = datItemFrom.toString();
	QString ssss = datItemTo.toString();

	if (!datSelectionFrom.isValid() || !datSelectionTo.isValid())
	{
		emit UpdateCalendar();
		return;
	}

	emit MultiDayItemMoved(datSelectionFrom, datSelectionTo, nEntityID, nEntityType, nCentID, nBcepID, nBcevID, nBCOL_ID, datItemFrom, datItemTo);
	//on_ItemResizing(false);
}

void CalendarMultiDayGraphicsView::on_tableCellDoubleClicked(int row, int column)
{
	QModelIndex selectedCell	= m_pCalendarHeaderTableModel->index(row, column);
	QDateTime datSelectionFrom	= m_pCalendarHeaderTableModel->getItemStartDateTime(selectedCell);
	QDateTime datSelectionTo	= m_pCalendarHeaderTableModel->getItemEndDateTime(selectedCell);
	int nEntityID, nEntityType, nEntityPersonID;
	m_pCalendarHeaderTableModel->getEntityForRow(row, column, nEntityID, nEntityType, nEntityPersonID);
	
	if (nEntityType < 0 || nEntityID < 0)
		return;

	emit tableCellDoubleClicked(datSelectionFrom, datSelectionTo, nEntityID, nEntityType);
}

void CalendarMultiDayGraphicsView::on_SelectionChanged(QModelIndex topLeftSelectedIndex, QModelIndex bottomRightSelectedIndex)
{
	if (topLeftSelectedIndex.row()>bottomRightSelectedIndex.row())
	{
		QModelIndex tmpIndex = topLeftSelectedIndex;
		topLeftSelectedIndex = bottomRightSelectedIndex;
		bottomRightSelectedIndex = tmpIndex;
	}
	
	QDateTime datSelectionFrom	= m_pCalendarHeaderTableModel->getItemStartDateTime(topLeftSelectedIndex);
	QDateTime datSelectionTo	= m_pCalendarHeaderTableModel->getItemEndDateTime(bottomRightSelectedIndex);
	int nEntityID, nEntityType, nEntityPersonID;
	m_pCalendarHeaderTableModel->getEntityForRow(topLeftSelectedIndex.row(), topLeftSelectedIndex.column(), nEntityID, nEntityType, nEntityPersonID);

	if (nEntityType < 0 || nEntityID < 0)
		return;

	//QString s = datSelectionFrom.toString();
	//QString ss = datSelectionTo.toString();

	emit tableCellDoubleClicked(datSelectionFrom, datSelectionTo, nEntityID, nEntityType);
}

void CalendarMultiDayGraphicsView::on_defineAvailability()
{
	if (m_recSelectedEntityItemsInLeftColumn.getRowCount()==0)
		return;
	
	m_pCalendarWidget->on_defineAvailability(m_recSelectedEntityItemsInLeftColumn);
}

void CalendarMultiDayGraphicsView::on_markAsFinal()
{
	if (m_recSelectedEntityItemsInLeftColumn.getRowCount()==0)
		return;

	m_pCalendarWidget->on_markAsFinal(m_recSelectedEntityItemsInLeftColumn);
}

void CalendarMultiDayGraphicsView::on_markAsPreliminary()
{
	if (m_recSelectedEntityItemsInLeftColumn.getRowCount()==0)
		return;

	m_pCalendarWidget->on_markAsPreliminary(m_recSelectedEntityItemsInLeftColumn);
}

void CalendarMultiDayGraphicsView::on_selectAll()
{
	QListIterator<QGraphicsItem*> i(m_lstMultiDayVerticalPersonHeaderItems);
	while (i.hasNext())
	{
		dynamic_cast<VerticalPersonItem*>(i.next())->setSelected(true);
	}
}

void CalendarMultiDayGraphicsView::on_deselectAll()
{
	QListIterator<QGraphicsItem*> i(m_lstMultiDayVerticalPersonHeaderItems);
	while (i.hasNext())
	{
		dynamic_cast<VerticalPersonItem*>(i.next())->setSelected(false);
	}
}

void CalendarMultiDayGraphicsView::on_dropEntityOnTable(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nDroppedEntityID, 
						  int nDroppedEntityType, int nDroppedEntityPersonID, QDateTime datSelectionFrom, QDateTime datSelectionTo)
{
	m_pCalendarWidget->on_dropEntityOnTable(nDraggedEntityID, nDraggedEntityType, nDraggedEntityPersonID, nDroppedEntityID, nDroppedEntityType, nDroppedEntityPersonID, datSelectionFrom, datSelectionTo);
}

void CalendarMultiDayGraphicsView::on_dropEntityOnHeader(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nColumn)
{
	QModelIndex selectedCell	= m_pCalendarHeaderTableModel->index(1, nColumn);
	QDateTime datSelectionFrom	= m_pCalendarHeaderTableModel->getItemStartDateTime(selectedCell);
	QDateTime datSelectionTo	= m_pCalendarHeaderTableModel->getItemEndDateTime(selectedCell);
	int nDroppedEntityID, nDroppedEntityType, nDroppedEntityPersonID;
	m_pCalendarHeaderTableModel->getEntityForRow(1, nColumn, nDroppedEntityID, nDroppedEntityType, nDroppedEntityPersonID);

	m_pCalendarWidget->on_dropEntityOnTable(nDraggedEntityID, nDraggedEntityType, nDraggedEntityPersonID, nDroppedEntityID, nDroppedEntityType, nDroppedEntityPersonID, datSelectionFrom, datSelectionTo);
}

void CalendarMultiDayGraphicsView::on_vertHeaderResized(int nWidth)
{
	setVertHeaderWidth(nWidth);
	PositionCornerScrollBarItems();
	PositionCustomScrollBars();

	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::CreateCalendarHeader()
{
	if (m_recEntites->getRowCount()<=1)
		return;

	//Create column header items.
	int nColumnCount = m_pCalendarWidget->getDateInterval();
	for (int i = 0; i < nColumnCount; i++)
	{
		int nHeight = m_pCalendarWidget->getDateHeaderHeight();
		QString strString = m_pCalendarHeaderTableModel->headerData(i, Qt::Horizontal).toDate().toString("ddd dd.MM.yyyy");
		MultiDayCalendarHeaderItem *item = new MultiDayCalendarHeaderItem(true, i, strString, nHeight, this);
		item->ConnectSignals();
		item->setZValue(12);
		m_pGraphicsScene->addItem(item);
		m_lstHeaderItems << item;
	}
}

void CalendarMultiDayGraphicsView::CreateEntityItems()
{
	if (m_recEntites->getRowCount()<=1)
		return;

	int nRowCount = m_recEntites->getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nEntityID = m_recEntites->getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = m_recEntites->getDataRef(i, "ENTITY_TYPE").toInt();
		int nEntityPersonID = m_recEntites->getDataRef(i, "ENTITY_PERSON_ID").toInt();
		QString strName = m_recEntites->getDataRef(i, "ENTITY_NAME").toString();
		int nRow = m_pCalendarHeaderTableModel->getRowForEntity(nEntityID, nEntityType);
		VerticalPersonItem *item = new VerticalPersonItem(nRow, nEntityID, nEntityType, nEntityPersonID, strName, this, getTableView());
		item->setToolTip(strName);
		m_pGraphicsScene->addItem(item);
		m_lstMultiDayVerticalPersonHeaderItems << item;
	}
}

void CalendarMultiDayGraphicsView::DestroyCalendarHeader()
{
	int nListCount = m_lstHeaderItems.count();
	for (int i = 0; i < nListCount; ++i)
	{
		m_pGraphicsScene->removeItem(m_lstHeaderItems.value(i));
		delete(m_lstHeaderItems.value(i));
	}
	m_lstHeaderItems.clear();
}

void CalendarMultiDayGraphicsView::DestroyPersonItems()
{
	int nListCount = m_lstMultiDayVerticalPersonHeaderItems.count();
	for (int i = 0; i < nListCount; ++i)
	{
		m_pGraphicsScene->removeItem(m_lstMultiDayVerticalPersonHeaderItems.value(i));
		delete(m_lstMultiDayVerticalPersonHeaderItems.value(i));
	}
	m_lstMultiDayVerticalPersonHeaderItems.clear();
}

void CalendarMultiDayGraphicsView::PositionCornerScrollBarItems()
{
	m_pLeftScrollBarItem->setPos(0, 0);
	m_pLeftScrollBarItem->setMinimumHeight(viewport()->rect().height());
	m_pLeftScrollBarItem->setMaximumHeight(viewport()->rect().height());
	m_pLeftScrollBarItem->setMinimumWidth(getVertHeaderWidth());
	m_pLeftScrollBarItem->setMaximumWidth(getVertHeaderWidth());

	m_pRightDownScrollBarItem->setPos(viewport()->rect().width()-m_pCalendarHeaderTableView->Table()->verticalScrollBar()->rect().width(),viewport()->rect().height()-m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->rect().height());
	m_pRightDownScrollBarItem->setMinimumHeight(viewport()->rect().height()-m_pCalendarHeaderTableView->Table()->verticalScrollBar()->rect().height()-getHeaderHeight());
	m_pRightDownScrollBarItem->setMaximumHeight(viewport()->rect().height()-m_pCalendarHeaderTableView->Table()->verticalScrollBar()->rect().height()-getHeaderHeight());
//	m_pRightDownScrollBarItem->setMinimumHeight(m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->rect().height());
//	m_pRightDownScrollBarItem->setMaximumHeight(m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->rect().height());
	m_pRightDownScrollBarItem->setMinimumWidth(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->rect().width());
	m_pRightDownScrollBarItem->setMaximumWidth(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->rect().width());

	if (m_recEntites->getRowCount()>1)
	{
		m_pTopRightScrollBarItem->show();
		m_pTopRightScrollBarItem->setPos(viewport()->rect().width()-m_pCalendarHeaderTableView->Table()->verticalScrollBar()->rect().width(),0);
		m_pTopRightScrollBarItem->setMinimumHeight(getDateHeaderHeight());
		m_pTopRightScrollBarItem->setMaximumHeight(getDateHeaderHeight());
		m_pTopRightScrollBarItem->setMinimumWidth(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->rect().width());
		m_pTopRightScrollBarItem->setMaximumWidth(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->rect().width());
		
		m_pTopLeftItem->show();
		m_pTopLeftItem->setPos(0,0);
		m_pTopLeftItem->resize(getVertHeaderWidth(), getDateHeaderHeight());
/*		m_pTopLeftItem->setMinimumHeight(getDateHeaderHeight());
		m_pTopLeftItem->setMaximumHeightHeight(getDateHeaderHeight());
		m_pTopLeftItem->setMinimumWidth(m_pCalendarHeaderTableView->getVertHeaderWidth());
		m_pTopLeftItem->setMaximumWidth(m_pCalendarHeaderTableView->getVertHeaderWidth());*/
	}
	else
	{
		m_pTopLeftItem->hide();
		m_pTopRightScrollBarItem->hide();
	}
}

void CalendarMultiDayGraphicsView::CreateCornerScrollBarItems()
{
	Label *label = new Label(Label::BOTTOM_LEFT_ITEM);
	Label *label1 = new Label(Label::BOTTOM_RIGHT_ITEM);
	Label *label2 = new Label(Label::TOP_RIGHT_ITEM);
	int nHeight = m_pCalendarWidget->getDateHeaderHeight();
	
	m_pLeftScrollBarItem = new QGraphicsProxyWidget();
	m_pRightDownScrollBarItem = new QGraphicsProxyWidget();
	m_pTopRightScrollBarItem = new TopRightItem();
	connect(dynamic_cast<TopRightItem*>(m_pTopRightScrollBarItem), SIGNAL(ResizeColumnsToFit(bool)), this, SLOT(ResizeColumnsToFit(bool)));
	m_pTopLeftItem = new TopLeftItem(nHeight, this);
	connect(m_pTopLeftItem, SIGNAL(vertHeaderResized(int)), this, SLOT(on_vertHeaderResized(int)));

	m_pLeftScrollBarItem->setWidget(label);
	m_pRightDownScrollBarItem->setWidget(label1);
	m_pTopRightScrollBarItem->setWidget(label2);
	
	m_pLeftScrollBarItem->setZValue(10);
	m_pRightDownScrollBarItem->setZValue(10);
	m_pTopRightScrollBarItem->setZValue(13);
	m_pTopLeftItem->setZValue(13);
	
	m_pGraphicsScene->addItem(m_pLeftScrollBarItem);
	m_pGraphicsScene->addItem(m_pRightDownScrollBarItem);
	m_pGraphicsScene->addItem(m_pTopRightScrollBarItem);
	m_pGraphicsScene->addItem(m_pTopLeftItem);
}

void CalendarMultiDayGraphicsView::PositionCustomScrollBars()
{
	//Horizontal scroll bar.
	m_pHScroll->resize(m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->width(), m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->height());
	m_pHScroll->setRange(m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->minimum(), m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->maximum());
	m_pHScroll->setPageStep(m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->pageStep());
	m_pHScrollBar->setPos(getVertHeaderWidth(), frameRect().bottomLeft().y()-m_pCalendarHeaderTableView->Table()->horizontalScrollBar()->frameGeometry().height());

	m_pVScroll->resize(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->width(), m_pCalendarHeaderTableView->Table()->verticalScrollBar()->height());
	m_pVScroll->setRange(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->minimum(), m_pCalendarHeaderTableView->Table()->verticalScrollBar()->maximum());
	m_pVScroll->setPageStep(m_pCalendarHeaderTableView->Table()->verticalScrollBar()->pageStep());
	m_pVScrollBar->setPos(frameRect().topRight().x()-m_pCalendarHeaderTableView->Table()->verticalScrollBar()->frameGeometry().width(), getHeaderHeight());
}

void CalendarMultiDayGraphicsView::CheckForMultipleItemsInRow(QGraphicsItem *newItem)
{
	QList<QGraphicsItem*> colidedItems;
	GetColidedItems(newItem, colidedItems, false);
	SortColidedItems(colidedItems);
	GetItemsPos(colidedItems);
}

bool CalendarMultiDayGraphicsView::ItemExists(int nCentID, int nEntityID, int nEntityType, int nBCOL_ID)
{
	QList<QGraphicsItem*> lstItems;
	lstItems = m_lstMultiDayCalendarEntityItems.values(nCentID);
	QListIterator<QGraphicsItem*> iter(lstItems);
	while(iter.hasNext())
	{
		QGraphicsItem* item = iter.next();
		int nItemEntityID = dynamic_cast<CalendarMultiDayEntityItem*>(item)->getItemEntityID();
		int nItemEntityType = dynamic_cast<CalendarMultiDayEntityItem*>(item)->getItemEntityType();
		int nItemBCOL_ID = dynamic_cast<CalendarMultiDayEntityItem*>(item)->getItemBCOL_ID();

		if (nItemEntityType == nEntityType && nItemEntityID == nEntityID && nItemBCOL_ID == nBCOL_ID)
			return true;
	}

	return false;
}

void CalendarMultiDayGraphicsView::UpdateCalendarSignal()
{
	DestroyCalendarHeader();
	DestroyPersonItems();
	//Create headers.
	CreateCalendarHeader();
	CreateEntityItems();

	ResizeColumnsToFit();
}

void CalendarMultiDayGraphicsView::UpdateCalendarSignalFromOutside()
{
	emit UpdateCalendar();
}

void CalendarMultiDayGraphicsView::SelectDeselectEntityItemsInLeftColumn(bool bSelected, int nEntityID, int nEntityType, int nEntityPersonID, QString nEntityName)
{
	if (bSelected)
	{
		m_recSelectedEntityItemsInLeftColumn.addRow();
		int nRow = m_recSelectedEntityItemsInLeftColumn.getRowCount()-1;
		m_recSelectedEntityItemsInLeftColumn.setData(nRow, "ENTITY_ID", nEntityID);
		m_recSelectedEntityItemsInLeftColumn.setData(nRow, "ENTITY_NAME", nEntityName);
		m_recSelectedEntityItemsInLeftColumn.setData(nRow, "ENTITY_TYPE", nEntityType);
		m_recSelectedEntityItemsInLeftColumn.setData(nRow, "ENTITY_PERSON_ID", nEntityPersonID);
	}
	else
	{
		m_recSelectedEntityItemsInLeftColumn.clearSelection();
		m_recSelectedEntityItemsInLeftColumn.find("ENTITY_ID", nEntityID);
		m_recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE", nEntityType, false, true, true);
		m_recSelectedEntityItemsInLeftColumn.deleteSelectedRows();
	}
}

void CalendarMultiDayGraphicsView::GetColidedItems(QGraphicsItem *Item, QList<QGraphicsItem*> &colidedItems, bool bThisItemOnly)
{
	int nEntityID = dynamic_cast<CalendarMultiDayEntityItem*>(Item)->getItemEntityID();
	int nEntityType = dynamic_cast<CalendarMultiDayEntityItem*>(Item)->getItemEntityType();
	QDateTime startDateTime, endDateTime;
	dynamic_cast<CalendarMultiDayEntityItem*>(Item)->getStartEndDateTime(startDateTime, endDateTime);
	QRect newRect = m_pCalendarHeaderTableView->getItemRectangle(nEntityID, nEntityType, startDateTime, endDateTime);
	int nItemRow = m_pCalendarHeaderTableModel->getRowForEntity(nEntityID, nEntityType);

	//First get collided items.
	QHashIterator<int, QGraphicsItem *> i(m_lstMultiDayCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		QDateTime itemStartDateTime, itemEndDateTime;
		CalendarMultiDayEntityItem *item = dynamic_cast<CalendarMultiDayEntityItem*>(i.value());
		int nThisEntityID = item->getItemEntityID();
		int nThisEntityType = item->getItemEntityType();
		item->getStartEndDateTime(itemStartDateTime, itemEndDateTime);
		QRect itemRect = m_pCalendarHeaderTableView->getItemRectangle(nThisEntityID, nThisEntityType, itemStartDateTime, itemEndDateTime);
		int nThisItemRow = m_pCalendarHeaderTableModel->getRowForEntity(nThisEntityID, nThisEntityType);

		//int ii = item->getItemEntityID();
		//int jj = item->getItemEntityType();
		//QString ss = itemStartDateTime.date().toString();
		//QString sss = startDateTime.date().toString();

		//Check.
		if (nItemRow == nThisItemRow)
		{
			if (/*itemStartDateTime.date() == startDateTime.date() && */nEntityID == item->getItemEntityID() && nEntityType == item->getItemEntityType())
			{
				//if (newRect.intersects(itemRect))
				if (newRect.intersected(itemRect).height()>1)
				{
					if (!colidedItems.contains(item))
					{
						colidedItems << item;
						if (!bThisItemOnly)
							GetColidedItems(item, colidedItems, false);
					}
				}
			}
		}
	}
}

void CalendarMultiDayGraphicsView::GetColidedItemsForRemove(QGraphicsItem *Item, QList<QGraphicsItem*> &colidedItems)
{
	GetColidedItems(Item, colidedItems, false);
	colidedItems.removeAll(Item);
}

void CalendarMultiDayGraphicsView::SortColidedItems(QList<QGraphicsItem*> &colidedItems)
{
	DbRecordSet recColidedItems;
	recColidedItems.addColumn(QVariant::Int, "ORDER");
	recColidedItems.addColumn(QVariant::DateTime, "START_DATETIME");
	recColidedItems.addColumn(QVariant::Int, "DURATION");
	int nCount = 0;	
	QListIterator<QGraphicsItem*> j(colidedItems);
	while (j.hasNext())
	{
		CalendarMultiDayEntityItem *colItem = dynamic_cast<CalendarMultiDayEntityItem*>(j.next());
		QDateTime itemStartDateTime, itemEndDateTime;
		colItem->getStartEndDateTime(itemStartDateTime, itemEndDateTime);
		recColidedItems.addRow();
		recColidedItems.setData(nCount, "ORDER", nCount);
		recColidedItems.setData(nCount, "START_DATETIME", itemStartDateTime);
		recColidedItems.setData(nCount, "DURATION", itemStartDateTime.time().secsTo(itemEndDateTime.time()));
		nCount++;
	}

	SortDataList defaultSortList;
	SortData sortDate1(1 /*START_DATETIME*/,  0 /* = ASCENDING*/);
	SortData sortTime1(2 /*DURATION*/, 1 /* = ASCENDING*/);
	defaultSortList << sortDate1 << sortTime1;
	recColidedItems.sortMulti(defaultSortList);
	//_DUMP(recColidedItems);

	QList<QGraphicsItem*> tmpColidedItems;
	int nRowCount = recColidedItems.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nOrder = recColidedItems.getDataRef(i, "ORDER").toInt();
		tmpColidedItems.append(colidedItems.value(nOrder));
	}
	colidedItems.clear();
	colidedItems = tmpColidedItems;
}

void CalendarMultiDayGraphicsView::GetItemsPos(QList<QGraphicsItem*> colidedItems)
{
	int nColumnCount;
	int nMaxVOrder = 0;
	int nColidedItemsCount = colidedItems.count();
	for (int i = 0; i < nColidedItemsCount; i++)
	{
		int nItemVOrder = 0;
		QList<int> lstPossibleVOrder;
		QList<int> lstNotPossibleVOrder;
		CalendarMultiDayEntityItem *Item = dynamic_cast<CalendarMultiDayEntityItem*>(colidedItems.value(i));
		//If first item place it first.
		if (!i)
		{
			nColumnCount = 1;
			lstPossibleVOrder << nItemVOrder;
		}
		else
		{
			for (int j = 0; j < i; j++)
			{
				CalendarMultiDayEntityItem *previousItem = dynamic_cast<CalendarMultiDayEntityItem*>(colidedItems.value(j));
				int nPreviousItemHOrder = previousItem->getVertOrder();

				if (ItemsColide(Item, previousItem))
				{
					int nNewItemHOrder = nPreviousItemHOrder+1;
					nItemVOrder = nNewItemHOrder;
					//Put previous item horizontal position in not possible list and add new one.
					lstNotPossibleVOrder << nPreviousItemHOrder;
					lstPossibleVOrder << nItemVOrder;
				}
				else
				{
					lstPossibleVOrder << nItemVOrder;
				}
			}

		}

		int nVOrder=0;
		bool bFirstIn = true;
		int nPossibleHOrder = lstPossibleVOrder.count();
		for (int i = 0; i < nPossibleHOrder; i++)
		{
			if (!lstNotPossibleVOrder.contains(lstPossibleVOrder.value(i)))
			{
				if (bFirstIn)
				{
					nVOrder = lstPossibleVOrder.value(i);
					bFirstIn = false;
				}
				else
				{
					if (lstPossibleVOrder.value(i) < nVOrder)
						nVOrder = lstPossibleVOrder.value(i);
				}
			}
		}
		
		if (nVOrder > nMaxVOrder)
			nMaxVOrder = nVOrder;

		Item->setVertOrder(nVOrder);
	}

	if (colidedItems.count() > 1)
	{
		int nEntityID = dynamic_cast<CalendarMultiDayEntityItem*>(colidedItems.first())->getItemEntityID();
		int nEntityType = dynamic_cast<CalendarMultiDayEntityItem*>(colidedItems.first())->getItemEntityType();
		int nRow = m_pCalendarHeaderTableModel->getRowForEntity(nEntityID,nEntityType);
		if (((nMaxVOrder+1)*m_nRowHeightIncrement)>m_nRowHeight)
			m_pCalendarHeaderTableView->setRowHeight(nRow, (nMaxVOrder+1)*m_nRowHeightIncrement);
		else
			m_pCalendarHeaderTableView->setRowHeight(nRow, m_nRowHeight);
	}
}

bool CalendarMultiDayGraphicsView::ItemsColide(QGraphicsItem *Item1, QGraphicsItem *Item2)
{
	int nEntityID1 = dynamic_cast<CalendarMultiDayEntityItem*>(Item1)->getItemEntityID();
	int nEntityType1 = dynamic_cast<CalendarMultiDayEntityItem*>(Item1)->getItemEntityType();
	QDateTime startDateTime1, endDateTime1;
	dynamic_cast<CalendarMultiDayEntityItem*>(Item1)->getStartEndDateTime(startDateTime1, endDateTime1);
	QRect item1Rect = m_pCalendarHeaderTableView->getItemRectangle(nEntityID1, nEntityType1, startDateTime1, endDateTime1);

	int nEntityID2 = dynamic_cast<CalendarMultiDayEntityItem*>(Item2)->getItemEntityID();
	int nEntityType2 = dynamic_cast<CalendarMultiDayEntityItem*>(Item2)->getItemEntityType();
	QDateTime itemStartDateTime2, itemEndDateTime2;
	dynamic_cast<CalendarMultiDayEntityItem*>(Item2)->getStartEndDateTime(itemStartDateTime2, itemEndDateTime2);
	QRect item2Rect = m_pCalendarHeaderTableView->getItemRectangle(nEntityID2, nEntityType2, itemStartDateTime2, itemEndDateTime2);


	//Check.
	if (/*itemStartDateTime2.date() == startDateTime1.date() && */nEntityID1 == nEntityID2 && nEntityType1 == nEntityType2)
	{
		//if (newRect.intersects(itemRect))
		if (item1Rect.intersected(item2Rect).height()>1)
			return true;
	}

	return false;
}

void CalendarMultiDayGraphicsView::CheckForMultipleItemsInRowForRemove(QGraphicsItem *Item)
{
	QList<QGraphicsItem*> colidedItems;
	GetColidedItemsForRemove(Item, colidedItems);
	SortColidedItems(colidedItems);
	GetItemsPos(colidedItems);
/*
	CalendarMultiDayEntityItem *itemForRemove = dynamic_cast<CalendarMultiDayEntityItem*>(Item);
	int nEntityID = itemForRemove->getItemEntityID();
	int nEntityType = itemForRemove->getItemEntityType();
	QDateTime startDate, endDate;
	itemForRemove->getStartEndDateTime(startDate, endDate);
	QRect removeItemRect = m_pCalendarHeaderTableView->getItemRectangle(nEntityID, nEntityType, startDate, endDate);

	int nHeightFactor = 1;
	int nVertOrder = 0;

	QList<CalendarMultiDayEntityItem*> colidedItems;
	QHashIterator<int, QGraphicsItem *> i(m_lstMultiDayCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();

		CalendarMultiDayEntityItem *item = dynamic_cast<CalendarMultiDayEntityItem*>(i.value());
		if (item == itemForRemove)
			continue;

		QDateTime itemStartDateTime, itemEndDateTime;
		item->getStartEndDateTime(itemStartDateTime, itemEndDateTime);
		QRect itemRect = m_pCalendarHeaderTableView->getItemRectangle(nEntityID, nEntityType, itemStartDateTime, itemEndDateTime);

		//Check.
		if (nEntityID == item->getItemEntityID() && nEntityType == item->getItemEntityType())
		{
			if (removeItemRect.intersects(itemRect))
			{
				colidedItems << item;
				int nItemVertOrder = item->getVertOrder();
				if (nItemVertOrder>nVertOrder)
					nVertOrder= nItemVertOrder;
			}
		}
	}

	if (nVertOrder)
		nVertOrder--;

	int nItemsCount = colidedItems.count();
	for (int i = 0; i < nItemsCount; i++)
	{
		colidedItems.value(i)->setVertFactor(nItemsCount);
		colidedItems.value(i)->setVertOrder(nVertOrder);
		nVertOrder--;
	}
	
	int nRow = m_pCalendarHeaderTableModel->getRowForEntity(nEntityID,nEntityType);
	if (nItemsCount>6)
		m_pCalendarHeaderTableView->setRowHeight(nRow, nItemsCount*m_nRowHeightIncrement);
	else
		m_pCalendarHeaderTableView->setRowHeight(nRow, 30);
*/
}

void CalendarMultiDayGraphicsView::CheckItemDateTimeRange(QDateTime &startDateTime, QDateTime &endDateTime)
{
	QDate modelStartDate = m_pCalendarWidget->getStartDate();
	int nInterval = m_pCalendarWidget->getDateInterval();

	if (startDateTime.date() < modelStartDate)
		startDateTime = QDateTime(modelStartDate, QTime(0,0));
	
	QDate modelEndDate = modelStartDate.addDays(nInterval-1);

	if (endDateTime.date() > modelEndDate)
		endDateTime = QDateTime(modelEndDate, QTime(23,59, 59));
}

bool CalendarMultiDayGraphicsView::AllColumnsAreInViewArea(int nViewportWidth)
{
	int nColumncount = m_pCalendarHeaderTableView->Model()->columnCount();
	int nEntitesCount = 1;
	if (m_recEntites->getRowCount()>0)
		nEntitesCount = m_recEntites->getRowCount();

	int nAllColumnsWidth = m_nColumnWidth*nColumncount;

	int nViewPort = nViewportWidth-getVertHeaderWidth()-m_pVScroll->width();
	if (nViewPort >= nAllColumnsWidth)
		return true;
	else
		return false;
}
