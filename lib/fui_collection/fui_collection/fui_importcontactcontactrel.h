#ifndef FUI_ImportContactContactRel_H
#define FUI_ImportContactContactRel_H

#include <QWidget>
#include "generatedfiles/ui_FUI_ImportContactContactRel.h"
#include "fuibase.h"

class FUI_ImportContactContactRel : public FuiBase
{
    Q_OBJECT

public:
    FUI_ImportContactContactRel(QWidget *parent = 0);
    ~FUI_ImportContactContactRel();

private:
    Ui::FUI_ImportContactContactRelClass ui;
	QString GetFUIName(){return tr("Import Contact-Contact Relationships");};
	void UpdateImportContactsButton();

	DbRecordSet m_lstData;

private slots:
	void on_btnImportContacts_clicked();
	void on_btnBuildList_clicked();
};

#endif // FUI_ImportContactContactRel_H
