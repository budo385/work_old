#include "clientsetupwizpage_firststep.h"
#include <QDesktopServices>
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager* g_pBoSet;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "gui_core/gui_core/thememanager.h"

ClientSetupWizPage_FirstStep::ClientSetupWizPage_FirstStep(int nWizardPageID, QString strPageTitle, QWidget *parent)
: WizardPage(nWizardPageID, strPageTitle)
{
	//ui.setupUi(this);
	m_bWebHelpOpen=false;

	setMinimumSize(QSize(600,450));

}

ClientSetupWizPage_FirstStep::~ClientSetupWizPage_FirstStep()
{

}


void ClientSetupWizPage_FirstStep::Initialize()
{
	ui.setupUi(this);
	
	//set pics and bkg:

	QString strTitle = tr("Welcome to SOKRATES")+QChar(174)+tr(" Communicator");

	ui.labelTitle->setText(	QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
								<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:18pt; font-weight:800; color:%1;\">%2</span></p>\
								</body></html>").arg("black").arg(strTitle));

	ui.labelTitle->setAlignment(Qt::AlignCenter);
	ui.labelPic_1->setPixmap(QPixmap(":DesktopIcon128_Mirror.png"));
	ui.labelPic_2->setPixmap(QPixmap(":Contacts128_Mirror.png"));
	ui.labelPic_3->setPixmap(QPixmap(":Projects128.png"));

	QString strMsg=tr("SOKRATES")+QChar(174)+tr(" <i>Communicator</i> is your personal assistant for unified communication, collaboration, and information.");
	strMsg+=tr("<br>The following setup wizard will guide you trough a few simple installation steps.");

	ui.labelInfo->setWordWrap(true);
	ui.labelInfo->setText(	QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
									<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:13pt; font-weight:600; color:%1;\">%2</span></p>\
									</body></html>").arg("black").arg(strMsg));

	//bkg_
	QString strBkg=ThemeManager::GetToolbarBkgStyle();
	strBkg.replace("frameToolBar","frameParent");
	ui.frameParent->setStyleSheet(strBkg);
	ui.frameIntro->setStyleSheet("QFrame#frameIntro {background-color: white}");
	ui.checkBox->setStyleSheet("QCheckBox  {font-weight:600;font-style:italic;font-family:Arial; font-size:13pt; color:black}");

	ui.checkBox->setChecked(true);

	m_bComplete = true;
	emit completeStateChanged();
	m_bInitialized = true;


}



void ClientSetupWizPage_FirstStep::on_NextButtonClicked()
{
	if (!ui.checkBox->isChecked() || m_bWebHelpOpen) //if not checked or already visited
		return;
	
	QString strHelp;
	if(g_pClientManager->GetIniFile()->m_strLangCode == "de")
	{
		strHelp = "http://www.sokrates-communicator.com/de/firststeps/index.html";
	}
	else
	{
		strHelp = "http://www.sokrates-communicator.com/en/firststeps/index.html";
	}

	m_bWebHelpOpen=true;

	QDesktopServices::openUrl(strHelp); 
}