#include "qclickablelabel.h"

QClickableLabel::QClickableLabel(QWidget *parent)
	: QLabel(parent)
{
	setMouseTracking( true );
}

QClickableLabel::~QClickableLabel()
{

}

void QClickableLabel::mousePressEvent ( QMouseEvent * event )
{
	emit clicked();
}

