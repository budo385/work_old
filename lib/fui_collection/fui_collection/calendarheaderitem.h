#ifndef CALENDARHEADERITEM_H
#define CALENDARHEADERITEM_H

#include <QGraphicsProxyWidget>
#include <QGraphicsView>
#include "label.h"

class CalendarHeaderItem : public QGraphicsProxyWidget
{
	Q_OBJECT

public:
	CalendarHeaderItem(bool bIsInMultiDayCalendar, int nColumn, QString	strString, int nHeight, QGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent = 0, Qt::WindowFlags wFlags = 0);
	~CalendarHeaderItem();

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
	virtual void ConnectSignals() = 0;

public slots:
	virtual void on_UpdateCalendar() = 0;

protected:
	void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

	bool isInResizeArea(const QPointF &pos);

	bool					m_bIsInMultiDayCalendar;
	bool					m_isResizing;
	QGraphicsView			*m_pCalendarGraphicsView;
	Label					*m_pLabel;
	int						m_nColumn;
	int						m_nHeight;
	int						m_nWidth;
	int						m_nMinWidth;
	QString					m_strString;

signals:
	void columnResized(int nColumn, int nWidth, bool bResizeAll);
};

#endif // CALENDARHEADERITEM_H
