#ifndef DLG_SERIALEMAILSMODE_H
#define DLG_SERIALEMAILSMODE_H

#include <QtWidgets/QDialog>
#include "ui_dlg_serialemailsmode.h"

class Dlg_SerialEmailsMode : public QDialog
{
	Q_OBJECT

public:
	Dlg_SerialEmailsMode(QWidget *parent = 0);
	~Dlg_SerialEmailsMode();

	bool GetUseAllEmails();

private:
	Ui::Dlg_SerialEmailsMode ui;

private slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();
};

#endif // DLG_SERIALEMAILSMODE_H
