#include "fui_dm_documents.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/picturehelper.h"
#include "bus_core/bus_core/mainentitydefaultfiller.h"
#include "bus_core/bus_core/globalconstants.h"
#include "gui_core/gui_core/gui_helper.h"
#include <QLocale>
#include <QInputDialog>
#include <QFileDialog>
#include "bus_client/bus_client/documenthelper.h"
#include "emaildialog.h"
#include "commgridview.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "common/common/datahelper.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"

#include "fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
extern QString g_strLastDir_FileOpen;
#include "bus_client/bus_client/clientdownloadmanager.h"
extern ClientDownloadManager *g_DownloadManager;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;	

FUI_DM_Documents::FUI_DM_Documents(QWidget *parent)
: FuiBase(parent),m_bDisableRefresh(false),m_bNoteDocument(false)
{
	connect(g_DownloadManager,SIGNAL(CheckInOutCompleted(Status,int,QString,DbRecordSet,DbRecordSet)),this,SLOT(OnCheckInOutCompleted(Status,int,QString,DbRecordSet,DbRecordSet)));

	m_lstProjects.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_lstContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_lstRevisions.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_REVISIONS_SELECT));
	m_recCheckOutInfo.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO));
	m_recInsertedDocumentRevision.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_REVISIONS));

	//-------------------------
	//		INIT SIMPLE FUI
	//-------------------------
	ui.setupUi(this);	
	InitFui(ENTITY_BUS_DM_DOCUMENTS,BUS_DM_DOCUMENTS,NULL,ui.btnButtonBar); //FUI base
	EnableAccessRightCheck();
	EnableWindowCloseOnCancelOrOK();
	ui.btnButtonBar->RegisterFUI(this);

	ui.frameTask->SetDefaultTaskType(GlobalConstants::TASK_TYPE_SCHEDULED_DOC);

	//disable insert:
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_INSERT)->setVisible(false);

	m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">";
	m_strEndHtmlTag="</span></p></body></html>";


	//GUI_Helper::SetButtonTextIcon(ui.btnCheckIn,tr("Check In"),":redo.png");
	ui.btnCheckIn->setIcon(QIcon(":redo.png"));
	ui.btnCheckOut->setIcon(QIcon(":editundo.png"));

	//setup path buttons:
	ui.btnSaveFromTemplate->setIcon(QIcon(":paste.png"));
	ui.btnSelect->setIcon(QIcon(":SAP_Select.png"));
	ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnSaveFromTemplate->setToolTip(tr("Create Document From Selected Template"));
	ui.btnSelect->setToolTip(tr("Select Path"));
	ui.btnRemove->setToolTip(tr("Remove Path"));
	connect(ui.btnSelect,SIGNAL(clicked()),this,SLOT(OnSelectPath()));
	connect(ui.btnRemove,SIGNAL(clicked()),this,SLOT(OnRemovePath()));
	connect(ui.btnSaveFromTemplate,SIGNAL(clicked()),this,SLOT(OnSaveFromTemplate()));

	//bring up field manager:
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData);
	m_GuiFieldManagerMain->RegisterField("BDMD_DAT_CREATED",ui.dateCreated);
	m_GuiFieldManagerMain->RegisterField("BDMD_DAT_LAST_MODIFIED",ui.dateModified);
	m_GuiFieldManagerMain->RegisterField("BDMD_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BDMD_DOC_PATH",ui.txtPath);
	//m_GuiFieldManagerMain->RegisterField("BDMD_OPEN_PARAMETER",ui.txtOpenParameter);
	//m_GuiFieldManagerMain->RegisterField("BDMD_OPEN_PARAMETER",ui.txtOpenParamInternetFile);
	m_GuiFieldManagerMain->RegisterField("BDMD_LOCATION",ui.txtLocation);
	//m_GuiFieldManagerMain->RegisterField("BDMD_DESCRIPTION",ui.frameEditor->GetTextWidget());
	m_GuiFieldManagerMain->RegisterField("BDMD_TEMPLATE_FLAG",ui.ckbIsTemplate);
	//m_GuiFieldManagerMain->RegisterField("CENT_IS_PRIVATE",ui.ckbIsPrivate);
	m_GuiFieldManagerMain->RegisterField("BDMD_SET_TAG_FLAG",ui.ckbSetTag);


	m_GuiFieldManagerMain->RegisterField("BDMD_READ_ONLY",ui.ckbReadOnly);
	

	

	//extra:
	//ui.ckbReadOnlyFlag->setVisible(true);

	//register 2 fields:
	//m_GuiFieldManagerMain->RegisterField("BDMD_DESCRIPTION",ui.frameNoteEditor->GetTextWidget());
	

	
	//enable to use plain text:
	GuiField_TextEdit* pText=dynamic_cast<GuiField_TextEdit*>(m_GuiFieldManagerMain->GetFieldHandler("BDMD_DOC_PATH"));
	if(pText!=NULL)pText->UsePlainText();
	pText=dynamic_cast<GuiField_TextEdit*>(m_GuiFieldManagerMain->GetFieldHandler("BDMD_LOCATION"));
	if(pText!=NULL)pText->UsePlainText();


	//reload all patterns from cache/server to be safe:
	MainEntitySelectionController cacheLoader;
	QList<int> lstIDs;
	lstIDs<<ENTITY_BUS_DM_APPLICATIONS<<ENTITY_CE_TYPES<<ENTITY_BUS_DM_TEMPLATES<<ENTITY_DOC_TEMPLATE_CATEGORY;
	cacheLoader.BatchReadEntityData(lstIDs);

	//init patterns
	ui.frameTemplate->Initialize(ENTITY_BUS_DM_TEMPLATES,&m_lstData,"BDMD_TEMPLATE_ID");
	ui.frameApplication->Initialize( ENTITY_BUS_DM_APPLICATIONS, &m_lstData,"BDMD_APPLICATION_ID");
	ui.frameType->Initialize( ENTITY_CE_TYPES, &m_lstData,"CENT_CE_TYPE_ID");
	ui.frameOwner->Initialize( ENTITY_BUS_PERSON,&m_lstData, "CENT_OWNER_ID");
	ui.frameType->GetSelectionController()->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);

	ui.frameContacts->Initialize(ENTITY_BUS_CONTACT,tr("Assigned Contacts"));
	ui.frameProjects->Initialize(ENTITY_BUS_PROJECT,tr("Assigned Projects"));
	ui.frameContacts->registerObserver(this);
	ui.frameProjects->registerObserver(this);
	ui.frameCategory->Initialize(ENTITY_DOC_TEMPLATE_CATEGORY,&m_lstData,"BDMD_CATEGORY",false);
	if(!g_AccessRight->TestAR(MODIFY_DOCUMENTS_CATEGORY))
		ui.frameCategory->DisableEntryText();

	//--------------------------------------------------------
	//Document revisions:
	//--------------------------------------------------------
	ui.tableWidget->Initialize(&m_lstRevisions,false,false,true,true,25,true);
	DbRecordSet columns;
	ui.tableWidget->AddColumnToSetup(columns,"BDMR_DAT_CHECK_IN",tr("Check In Date"),140,false);
	ui.tableWidget->AddColumnToSetup(columns,"SIZE",tr("Revision Size)"),80,false);
	ui.tableWidget->AddColumnToSetup(columns,"BDMR_NAME",tr("Name"),180,false);
	ui.tableWidget->AddColumnToSetup(columns,"BDMR_REVISION_TAG",tr("Revision Tag"),100,false);
	ui.tableWidget->AddColumnToSetup(columns,"USER_NAME",tr("User"),200,false);
	
	ui.tableWidget->SetColumnSetup(columns);


	//------------------------------------- INIT CNTX MENU--------------------------------//
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;

	pAction = new QAction(tr("Reload"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(on_btnRefresh_clicked()));
	lstActions.append(pAction);

	m_pRevSetTag = new QAction(tr("Set Revision Tag"), this);
	connect(m_pRevSetTag, SIGNAL(triggered()), this, SLOT(OnSetRevisionTag()));
	lstActions.append(m_pRevSetTag);

	m_pRevSaveCopy = new QAction(tr("Save Copy of Document Revision"), this);
	connect(m_pRevSaveCopy, SIGNAL(triggered()), this, SLOT(OnGetRevision()));
	lstActions.append(m_pRevSaveCopy);

	m_pRevOpenCopy = new QAction(tr("Open Copy of Document Revision"), this);
	connect(m_pRevOpenCopy, SIGNAL(triggered()), this, SLOT(OnOpenRevision()));
	lstActions.append(m_pRevOpenCopy);

	m_pRevReplaceCurrent = new QAction(tr("Replace Current Text with Revision"), this);
	connect(m_pRevReplaceCurrent, SIGNAL(triggered()), this, SLOT(OnOnReplaceCurrentRevision()));
	lstActions.append(m_pRevReplaceCurrent);

	

	pAction = new QAction(tr("Delete Revision"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteRevision()));
	lstActions.append(pAction);


	ui.tableWidget->SetContextMenuActions(lstActions);

	//------------------------------------- INIT CNTX MENU--------------------------------//



	ui.frameTask->m_pRecMain = &m_lstData;	//attach main record

	
	//set to first doc as default:
	//set edit to false:
	SetMode(MODE_EMPTY);

	connect(ui.frameNoteEditor,SIGNAL(on_PrintIcon_clicked()),this,SLOT(OnPrint()));


	//ui.frameBkg()
	ui.frameBkg->setStyleSheet("QFrame#frameBkg "+ThemeManager::GetSidebarChapter_Bkg());


	ui.labelDocType->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	ui.labelDocType->setAlignment(Qt::AlignLeft);

		//issue 1329
	ui.labelName->setStyleSheet("QLabel {color:white}");
	ui.labelFile->setStyleSheet("QLabel {color:white}");
	

	ui.frameNoteEditor->ShowVerticalStretchButton(true);
	ui.frameEditor->ShowVerticalStretchButton(true);
	
	ui.frameNoteEditor->SetSmallBoolean(false); //Dirty to change Herzog layout.
	OnNoteEditor_VerticalOpenClose_clicked(false); //Dirty to change Herzog layout.
	
	connect(ui.frameNoteEditor,SIGNAL(on_VerticalOpenClose_clicked(bool)),this,SLOT(OnNoteEditor_VerticalOpenClose_clicked(bool)));
	connect(ui.frameEditor,SIGNAL(on_VerticalOpenClose_clicked(bool)),this,SLOT(OnEditor_VerticalOpenClose_clicked(bool)));

	ui.frameNoteEditor	->SetEmbeddedPixMode();
	ui.frameEditor		->SetEmbeddedPixMode();

	//...
	ui.tabWidgetMain->setCurrentIndex(0);
	ui.frameTask->SetAlwaysExpanded();
	updateTaskTabCheckbox();
	connect(ui.frameTask,SIGNAL(ScheduleTaskToggle()),this,SLOT(updateTaskTabCheckbox()));

}


FUI_DM_Documents::~FUI_DM_Documents()
{
	delete m_GuiFieldManagerMain;
}

QString FUI_DM_Documents::GetFUIName()
{
	return tr("Documents");
}


void FUI_DM_Documents::RefreshDisplay()
{
	ui.frameCategory->RefreshDisplay();
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frameType->RefreshDisplay();
	ui.frameTemplate->RefreshDisplay();
	ui.frameApplication->RefreshDisplay();
	ui.frameTask->SetGUIFromData();
	ui.frameOwner->RefreshDisplay();
	
	RefreshRevisions(false); //refresh revisions


	//refresh pages for doc type:
	if (m_lstData.getRowCount()>0)
	{
		switch(m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt())
		{
		case GlobalConstants::DOC_TYPE_PHYSICAL:
			PreparePhysicalDocument(true);
			break;
		case GlobalConstants::DOC_TYPE_NOTE:
			ui.frameNoteEditor->SetInverseVerticalStretchButton();
			PrepareNoteDocument(true);
		    break;
		case GlobalConstants::DOC_TYPE_INTERNET_FILE:
			PrepareInternetDocument(true);
		    break;
		case GlobalConstants::DOC_TYPE_URL:
			PrepareURLDocument(true);
			break;
		default:
			PrepareLocalDocument(true);
		    break;
		}

		ui.frameNoteEditor->SetHtml(m_lstData.getDataRef(0,"BDMD_DESCRIPTION").toString());
		ui.frameEditor->SetHtml(m_lstData.getDataRef(0,"BDMD_DESCRIPTION").toString());
		ui.txtOpenParameter->setText(m_lstData.getDataRef(0,"BDMD_OPEN_PARAMETER").toString());
		ui.txtOpenParamInternetFile->setText(m_lstData.getDataRef(0,"BDMD_OPEN_PARAMETER").toString());

		//only set filename:
		QString strFileNameSource=m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString();
		QFileInfo info(strFileNameSource);
		ui.txtFileName->setText(info.fileName());
		setWindowTitle(tr("Document: ")+ui.txtName->text());
	}
	else
		setWindowTitle(tr("Document"));

	RefreshButtonState();
	updateTaskTabCheckbox();
}


void FUI_DM_Documents::SetMode(int nMode)
{

	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		m_GuiFieldManagerMain->SetEditMode(false);
		ui.btnSelect->setEnabled(false);
		ui.btnRemove->setEnabled(false);
		ui.btnSaveFromTemplate->setEnabled(false);
		ui.frameEditor->SetEditMode(false);
		ui.frameTemplate->SetEditMode(false);
		ui.frameApplication->SetEditMode(false);
		ui.frameType->SetEditMode(false);
		ui.frameTask->SetEnabled(false);
		ui.txtOpenParameter->setEnabled(false);
		ui.txtOpenParamInternetFile->setEnabled(false);
		ui.frameContacts->SetEditMode(false);
		ui.frameProjects->SetEditMode(false);
		ui.frameOwner->SetEditMode(false);
		ui.frameNoteEditor->SetEditMode(false);
		ui.frameCategory->SetEditMode(false);
		ui.txtFileName->setEnabled(false);
		ui.arSelector->SetEditMode(false);
		m_pRevReplaceCurrent->setEnabled(false);
	}
	else
	{
		m_GuiFieldManagerMain->SetEditMode(true);
		ui.btnSelect->setEnabled(true);
		ui.btnRemove->setEnabled(true);
		ui.btnSaveFromTemplate->setEnabled(true);
		ui.frameEditor->SetEditMode(true);
		ui.frameTemplate->SetEditMode(true);
		ui.frameApplication->SetEditMode(true);
		ui.frameType->SetEditMode(true);
		ui.frameTask->SetEnabled(true);
		ui.txtOpenParameter->setEnabled(true);
		ui.txtOpenParamInternetFile->setEnabled(true);
		ui.frameContacts->SetEditMode(true);
		ui.frameProjects->SetEditMode(true);
		ui.frameOwner->SetEditMode(true);
		ui.frameNoteEditor->SetEditMode(true);
		ui.frameCategory->SetEditMode(true);
		ui.arSelector->SetEditMode(true);
	
		if (!m_bDocCheckedOut) //only if not checked
			ui.txtFileName->setEnabled(true);
		else
			ui.txtFileName->setEnabled(false);
		m_pRevReplaceCurrent->setEnabled(true);
		ui.txtName->setFocus(Qt::OtherFocusReason);
	}

	if (nMode==MODE_INSERT)
		ui.btnCheckOut->setEnabled(false);
	else
		ui.btnCheckOut->setEnabled(true);


	ui.dateModified->setEnabled(false);

	ui.ckbReadOnly->setEnabled(true); //always on:

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}


void FUI_DM_Documents::OnSelectPath()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.*";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Select Document"),
		strStartDir,
		strFilter);

	if(!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		//set name if empty:
		ui.txtPath->setText(strFile);
		QFileInfo infoTarget(strFile);
		QString strBaseName=infoTarget.fileName();
		if (ui.txtName->text().isEmpty())
		{
			ui.txtName->setText(strBaseName);
		}

		int nAppID=DocumentHelper::GetApplicationFromExtension(strFile).toInt();
		if (nAppID)
		{
			ui.frameApplication->SetCurrentEntityRecord(nAppID);
		}
	
	}
}

void FUI_DM_Documents::OnRemovePath()
{
	ui.txtPath->clear();
}

void FUI_DM_Documents::DataPrepareForInsert(Status &err)
{
	FuiBase::DataPrepareForInsert(err);

	m_strOriginalNote.clear();

	//set date created, modified=
	m_lstData.setData(0,"BDMD_DAT_CREATED",QDateTime::currentDateTime());

	//set owner=logged----->>>
	int nLoggedID=g_pClientManager->GetPersonID();
	if (nLoggedID!=0)
		m_lstData.setData(0,"CENT_OWNER_ID",nLoggedID);

	m_lstData.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
	//m_lstData.setData(0,"CENT_IS_PRIVATE",0);

	//set to normal doc as default:
	m_lstData.setData(0,"BDMD_DOC_TYPE",GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE);
	//PrepareLocalDocument(true);

	m_lstContacts.clear();
	m_lstProjects.clear();

	//disable tasks:
	ui.frameTask->ClearTaskData();
	m_lstTaskCache=ui.frameTask->m_recTask;
	ui.arSelector->Invalidate();
}

void FUI_DM_Documents::DataPrepareForCopy(Status &err)
{
	err.setError(0);
	QVariant intNull(QVariant::Int);

	//clear ID fields:
	m_lstData.setColValue(m_lstData.getColumnIdx(m_TableData.m_strPrimaryKey),intNull);
	m_lstData.setColValue(m_lstData.getColumnIdx("CENT_ID"),intNull);
	m_lstData.setColValue(m_lstData.getColumnIdx("CENT_TASK_ID"),intNull);
	m_lstData.setColValue(m_lstData.getColumnIdx("BDMD_COMM_ENTITY_ID"),intNull);
	ui.frameTask->m_recTask.setColValue(ui.frameTask->m_recTask.getColumnIdx("BTKS_ID"),intNull);
	m_lstContacts.setColValue(m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_1"),intNull);
	m_lstProjects.setColValue(m_lstProjects.getColumnIdx("BNMR_TABLE_KEY_ID_1"),intNull);
	m_lstTaskCache=ui.frameTask->m_recTask;
	ui.arSelector->Invalidate();
}

//lstContacts,lstProjects either in CELC/CELP format or only have BCNT_ID
void FUI_DM_Documents::SetDefaults(int nDocType,DbRecordSet *rowDefault,DbRecordSet *lstContacts,DbRecordSet *lstProjects, bool bScheduleTask, bool bIsTemplate, DbRecordSet *recRevisionForInsert)
{
	//go insert mode
	on_cmdInsert();

	bool bDefaultsSet=false; //skip checkin
	if (rowDefault)
	{
		if (rowDefault->getRowCount()>0)
		{
			MainEntityDefaultFiller::ApplyDefaultValues(m_lstData,*rowDefault);
			bDefaultsSet=true;
		}
	}

	//set type:
	m_lstData.setData(0,"BDMD_DOC_TYPE",nDocType);

	//if template:
	if (bIsTemplate)
		m_lstData.setData(0,"BDMD_TEMPLATE_FLAG",1);
	else
		m_lstData.setData(0,"BDMD_TEMPLATE_FLAG",0);
	

	//if revision:
	if (recRevisionForInsert)
	{
		if (recRevisionForInsert->getRowCount()>0)
		{
			m_recInsertedDocumentRevision=*recRevisionForInsert;
			//refresh revisions:
			m_lstRevisions.clear();
			m_lstRevisions.merge(m_recInsertedDocumentRevision);
		}
	}


	if (lstContacts)
	{
		int nCntID=-1;
		if (lstContacts->getColumnIdx("BNMR_TABLE_KEY_ID_2")>=0)
		{
			m_lstContacts=*lstContacts;
			
		}
		else
		{
			m_lstContacts.clear();
			int nSize=lstContacts->getRowCount();
			for(int i=0;i<nSize;++i)
			{
				m_lstContacts.addRow();
				m_lstContacts.setData(i,"BNMR_TABLE_KEY_ID_2",lstContacts->getDataRef(i,0));
			}
			//nCntID=0;
		}
		nCntID=m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");

		if (nCntID!=-1)
		{
			//m_lstContacts.Dump();
			ui.frameContacts->SetList(m_lstContacts,nCntID);
		}
		
	}

	if (lstProjects)
	{
		int nID=-1;
		if (lstProjects->getColumnIdx("BNMR_TABLE_KEY_ID_2")>=0)
		{
			m_lstProjects=*lstProjects;
		}
		else
		{
			m_lstProjects.clear();
			int nSize=lstProjects->getRowCount();
			for(int i=0;i<nSize;++i)
			{
				m_lstProjects.addRow();
				m_lstProjects.setData(i,"BNMR_TABLE_KEY_ID_2",lstProjects->getDataRef(i,0));
			}
		}
		nID=m_lstProjects.getColumnIdx("BNMR_TABLE_KEY_ID_2");

		if (nID!=-1)
		{
			ui.frameProjects->SetList(m_lstProjects,nID);
		}

		
	}


	//if schedule task:
	if (bScheduleTask)
	{
		ui.frameTask->m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE", 1);	 //his active
		//set active tab to task details
		ui.tabWidget->setCurrentIndex(1);
		updateTaskTabCheckbox();
	}


	//once again:
	RefreshDisplay();
	ui.txtName->setFocus(Qt::OtherFocusReason);

	//if internet or local, open check in:
	if (!bDefaultsSet)
	{
		if (nDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		{
			on_btnCheckIn_clicked();
		}
		if (nDocType==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
		{
			OnSelectPath();
		}
	}

}

void FUI_DM_Documents::DataClear()
{
	m_lstData.clear();

	m_lstProjects.clear();
	m_lstContacts.clear();
	m_lstRevisions.clear();
	m_recCheckOutInfo.clear();

	//tasks:
	ui.frameTask->ClearTaskData();
	m_lstTaskCache.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	m_lstTaskCache.addRow();


	ui.frameContacts->Clear();
	ui.frameProjects->Clear();
	ui.arSelector->Invalidate();
}

void FUI_DM_Documents::DataDefine()
{
	DbSqlTableDefinition::GetKeyData(m_nTableID,m_TableData);
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
}


void FUI_DM_Documents::DataRead(Status &err,int nRecordID)
{
	DbRecordSet lstUAR,lstGAR;

	_SERVER_CALL(BusDocuments->Read(err,nRecordID,m_lstData,m_lstProjects,m_lstContacts,m_lstTaskCache,m_lstRevisions,m_recCheckOutInfo,lstUAR,lstGAR))
	if (err.IsOK() && m_lstData.getRowCount()>0)
	{
		m_strOriginalNote=m_lstData.getDataRef(0,"BDMD_DESCRIPTION").toString();
		if (m_lstTaskCache.getRowCount()>0)
		{
			ui.frameTask->m_recTask=m_lstTaskCache; //store into cache & tasker
		}
		int nCntID=m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		int nProjectID=m_lstProjects.getColumnIdx("BNMR_TABLE_KEY_ID_2");

		ui.frameContacts->SetList(m_lstContacts,nCntID);
		ui.frameProjects->SetList(m_lstProjects,nProjectID);
		m_lstDataCache=m_lstData;
		ui.arSelector->Load(m_lstData.getDataRef(0,"BDMD_ID").toInt(),BUS_DM_DOCUMENTS,&lstUAR,&lstGAR);
		ui.arSelector->SetRecordDisplayName(m_lstData.getDataRef(0,"BDMD_NAME").toString());
	}
}


void FUI_DM_Documents::DataLock(Status &err)
{
	int nCommEntityID=m_lstData.getDataRef(0,"CENT_ID").toInt();
	Q_ASSERT(nCommEntityID!=0);

	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"CENT_ID");
	lst.addRow();
	lst.setData(0,0,nCommEntityID);
	DbRecordSet lstStatusRows;
	_SERVER_CALL(ClientSimpleORM->Lock(err,CE_COMM_ENTITY,lst,m_strLockedRes, lstStatusRows))
		if (!err.IsOK())
			if (err.getErrorCode()==StatusCodeSet::ERR_SQL_LOCKED_BY_ANOTHER)
			{
				QString strMsg=err.getErrorTextRaw();
				int nPos=strMsg.indexOf(" - ");
				if (nPos>=0)
				{
					QString strPersCode=strMsg.left(nPos);
					//test if this is moa
					if(strPersCode==g_pClientManager->GetPersonCode())
					{

						int nResult=QMessageBox::question(this,tr("Warning"),tr("This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?"),tr("Yes"),tr("No"));
						if (nResult==0)
						{
							//unlock record on server:
							Status err_temp;
							bool bUnlocked=false;
							_SERVER_CALL(ClientSimpleORM->UnLockByRecordID(err_temp,CE_COMM_ENTITY,lst,bUnlocked))
								if (!err_temp.IsOK() || !bUnlocked)
								{
									return; //return with old lock error
								}
								else
								{
									err.setError(0); //clear error and return
									_SERVER_CALL(ClientSimpleORM->Lock(err,CE_COMM_ENTITY,lst,m_strLockedRes, lstStatusRows))
										return;
								}

						}

					}
				}
			}

}
void FUI_DM_Documents::DataDelete(Status &err)
{
	int nCommEntityID=m_lstData.getDataRef(0,"CENT_ID").toInt();
	Q_ASSERT(nCommEntityID!=0);


	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"CENT_ID");
	lst.addRow();
	lst.setData(0,0,nCommEntityID);
	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	_SERVER_CALL(ClientSimpleORM->Delete(err,CE_COMM_ENTITY,lst,m_strLockedRes, lstStatusRows, pBoolTransaction))
	if (err.IsOK())
		DataClear();

}
void FUI_DM_Documents::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	QString strPath=m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString();
	if (ui.ckbIsTemplate->isChecked())
	{
		if (!DocumentHelper::TestForOfficeTemplateFiles(strPath))
		{
			err.setError(1,tr("Operation aborted!"));
			return;
		}
	}
}

void FUI_DM_Documents::DataWrite(Status &err)
{
	QString strPath=m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString();

	//m_lstData.Dump();
	if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_NOTE)
		m_lstData.setData(0,"BDMD_DESCRIPTION",ui.frameNoteEditor->GetHtml());
	else
		m_lstData.setData(0,"BDMD_DESCRIPTION",ui.frameEditor->GetHtml());

	if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		m_lstData.setData(0,"BDMD_OPEN_PARAMETER",ui.txtOpenParamInternetFile->text());
	else
		m_lstData.setData(0,"BDMD_OPEN_PARAMETER",ui.txtOpenParameter->text());

	//set new name only if not check out (only for internet):
	if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		if (!m_bDocCheckedOut && !ui.txtFileName->text().isEmpty())
			m_lstData.setData(0,"BDMD_DOC_PATH",ui.txtFileName->text());

	if(m_lstData.getDataRef(0,"BDMD_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,tr("Document name is mandatory field!"));	return;	}

	DbRecordSet rowTask;
	if (ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt()>0)
	{
		ui.frameTask->GetDataFromGUI(); //fill mrectask
		ui.frameTask->CheckTaskRecordBeforeSave(err);
		if (!err.IsOK())
			return;
		rowTask=ui.frameTask->m_recTask;
		//rowTask.Dump();
	}
	//get lists:
	GetLinkListsFromSAPNE();
	//if task: add as owner (issue 1975)
	if (rowTask.getRowCount()>0)
	{
		//first delete old ones with role:
		m_lstContacts.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_DOC_TASK_OWNER);
		m_lstContacts.deleteSelectedRows();

		int nOwnerID=rowTask.getDataRef(0,"BTKS_OWNER").toInt();
		DbRecordSet recPerson;
		ui.frameOwner->GetSelectionController()->GetEntityRecord(nOwnerID,recPerson);
		if (recPerson.getRowCount()>0)
		{
			int nContactID=recPerson.getDataRef(0,"BPER_CONTACT_ID").toInt();
			if (nContactID>0)
				if(m_lstContacts.find("BNMR_TABLE_KEY_ID_2",nContactID,true)<0)
				{
					m_lstContacts.addRow();
					m_lstContacts.setData(m_lstContacts.getRowCount()-1,"BNMR_TABLE_KEY_ID_2",nContactID);
					m_lstContacts.setData(m_lstContacts.getRowCount()-1,"BNMR_SYSTEM_ROLE",GlobalConstants::CONTACT_LINK_ROLE_DOC_TASK_OWNER);
				}
		}

		//#2707: remove avatar for this task if it exists
		if(GlobalConstants::TASK_STATUS_COMPLETED == rowTask.getDataRef(0, "BTKS_SYSTEM_STATUS").toInt())
		{	
			int nType = AVATAR_TASK_NOTE;
			int nDocType = m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt();
			if(GlobalConstants::DOC_TYPE_INTERNET_FILE == nDocType)
				nType = AVATAR_TASK_INTERNET_FILE;
			else if(GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE == nDocType)
				nType = AVATAR_TASK_LOCAL_FILE;
			else if(GlobalConstants::DOC_TYPE_PHYSICAL == nDocType)
				nType = AVATAR_TASK_PAPER_DOCS;

			EMailDialog::RemoveAvatar(nType, m_lstData.getDataRef(0, "BDMD_ID").toInt());
		}
	}

	bool bScheduleUpload=false;

	//_DUMP(m_recInsertedDocumentRevision);
	//only if insert, copy, insert first revision, if edit-> user manually checks in
	if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_INTERNET_FILE)
	{

		if (m_nMode==MODE_EDIT)
		{
			m_recInsertedDocumentRevision.clear();
		}
		else
		{
			//content is empty and file exists: schedule for upload...
			if (m_recInsertedDocumentRevision.getDataRef(0,"BDMR_CONTENT").toByteArray().size()==0 && QFile::exists(strPath))
			{
				bScheduleUpload=true;
				m_recInsertedDocumentRevision.clear(); //as there is no need 
			}
			else
			{
				if(!DocumentHelper::TestDocumentRevisionsForMaximumSize(m_recInsertedDocumentRevision))
				{
					err.setError(StatusCodeSet::ERR_GENERAL,tr("Operation aborted!"));
					return;
				}
			}
		}
	}
	else if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_NOTE)
	{
		m_recInsertedDocumentRevision.clear();
		CreateNoteRevisionIfNeeded(m_recInsertedDocumentRevision);
	}


	//QString strFile=m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString();
	DbRecordSet lstUAR,lstGAR;
	ui.arSelector->Save(m_lstData.getDataRef(0,"BDMD_ID").toInt(),BUS_DM_DOCUMENTS,&lstUAR,&lstGAR); //get ar rows for save

	_SERVER_CALL(BusDocuments->Write(err,m_lstData,m_lstProjects,m_lstContacts,rowTask,m_recInsertedDocumentRevision,m_strLockedRes,lstUAR,lstGAR))
	if (err.IsOK())
	{
		if (m_lstTaskCache.getRowCount()>0)
		{
			ui.frameTask->m_recTask=m_lstTaskCache; //store into cache & tasker
		}
		int nCntID=m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		int nProjectID=m_lstProjects.getColumnIdx("BNMR_TABLE_KEY_ID_2");

		ui.frameContacts->SetList(m_lstContacts,nCntID);
		ui.frameProjects->SetList(m_lstProjects,nProjectID);

		m_strLockedRes.clear();
		m_lstDataCache=m_lstData;

		if (rowTask.getRowCount()>0)
		{
			ui.frameTask->m_recTask=rowTask;	//return d'
			m_lstTaskCache=rowTask;
		}

		//get apps
		int nApp=m_lstData.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
		if (nApp>0)
		{
			DbRecordSet lstApplications;
			DocumentHelper::GetDocApplications(lstApplications,false);
			int nRow=lstApplications.find(0,nApp,true);
			if (nRow!=-1)
			{
				//check if app has got template
				if (lstApplications.getDataRef(nRow,"BDMA_TEMPLATE_ID").toInt()<=0)
				{
					//if not then assign this one->save->reload cache or empty
					DbRecordSet rowApp=lstApplications.getRow(nRow);
					rowApp.setData(0,"BDMA_TEMPLATE_ID",m_lstData.getDataRef(0,"BDMD_ID"));
					Status err;
					QString pLockResourceID;
					int nQueryView = -1;
					int nSkipLastColumns = 0; 
					DbRecordSet lstForDelete;
					_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_APPLICATIONS,rowApp, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
					_CHK_ERR(err);
					g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_DM_APPLICATIONS); 
				}
			}
		}
			
		ui.arSelector->Load(m_lstData.getDataRef(0,"BDMD_ID").toInt(),BUS_DM_DOCUMENTS,&lstUAR,&lstGAR);
		ui.arSelector->SetRecordDisplayName(m_lstData.getDataRef(0,"BDMD_NAME").toString());

		if (bScheduleUpload)
		{
			g_DownloadManager->ScheduleCheckInDocument(m_lstData.getDataRef(0,"BDMD_ID").toInt(),strPath,"",true,m_nMode==MODE_EDIT,ui.ckbSetTag->isChecked());
		}

		/*
		if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_NOTE)
		{
			//reload revs:
			m_recInsertedDocumentRevision.clear();
			RefreshRevisions(true);
		}
		*/
	}

}

void FUI_DM_Documents::DataUnlock(Status &err)
{
	FuiBase::DataUnlock(err);
	if (!err.IsOK()) return;
	if (m_lstTaskCache.getRowCount()>0)
		ui.frameTask->m_recTask=m_lstTaskCache;			//restore from cache
	else
		ui.frameTask->ClearTaskData();
	ui.arSelector->CancelChanges();
	if (m_lstData.getRowCount()>0)
		ui.arSelector->SetRecordDisplayName(m_lstData.getDataRef(0,"BDMD_NAME").toString());
}





void FUI_DM_Documents::Execute (int nDocumentID)
{
	//m_bIsExecuteMode=true;
	on_selectionChange(nDocumentID);

	//if ok go edit:
	if (m_nEntityRecordID!=0)
	{
		int nDocType=m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt();

		//if internet if not template: check out
		if (nDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		{
			if (m_lstRevisions.getRowCount()>0)
			{
				on_btnCheckOut_clicked();						//automatically check out document
			}
			else  //if internet if template not empty and revision is empty: save from template
			{
				OnSaveFromTemplate();
			}
			RefreshRevisions(true);			//reload from server
			//on_cmdEdit();					//go edit mode
		}


		//if local then if template, copy from it
		if(nDocType==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
			if (m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString().isEmpty() && m_lstData.getDataRef(0,"BDMD_TEMPLATE_ID").toInt()!=0)
			{
				if(!on_cmdEdit()) return;
				OnSaveFromTemplate();
			}



		//only files and url can be opened in application:
		if(nDocType==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE || nDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE || nDocType==GlobalConstants::DOC_TYPE_URL)
			OpenDocumentInApplication();

	}
}


void FUI_DM_Documents::PrepareLocalDocument(bool checked)
{
	if (checked)
	{
		ui.stackedWidget->setCurrentIndex(0);
		
		ui.btnSelect->setVisible(true);
		ui.btnSaveFromTemplate->setVisible(true);
		ui.labelDocPath->setText("Document Path:");
		ui.frameApplication->setVisible(true);
		ui.labelApp->setVisible(true);
		QPixmap pix;
		pix.load(":Local_File_48.png");
		ui.labelIconDocType->setPixmap(pix);

		ui.labelDocType->setText(tr("Local File Reference"));

		//m_GuiFieldManagerMain->RegisterField("BDMD_DOC_PATH",ui.txtFileName);		//overwrite any prev registration
		ui.txtFileName->setVisible(false);
		ui.labelFile->setVisible(false);
	}

}

void FUI_DM_Documents::PrepareURLDocument(bool checked)
{
	if (checked)
	{
		ui.stackedWidget->setCurrentIndex(0);
		
		ui.btnSelect->setVisible(false);
		ui.btnSaveFromTemplate->setVisible(false);
		ui.labelDocPath->setText("Address:");
		ui.frameApplication->setVisible(true);
		ui.labelApp->setVisible(true);

		QPixmap pix;
		pix.load(":Address_48.png");
		ui.labelIconDocType->setPixmap(pix);
		ui.labelDocType->setText(tr("Address"));
		ui.txtFileName->setVisible(false);
		ui.labelFile->setVisible(false);

	}
}


void FUI_DM_Documents::PreparePhysicalDocument(bool checked)
{
	if (checked)
	{
		ui.stackedWidget->setCurrentIndex(1);
		
		ui.frameApplication->setVisible(false);
		ui.labelApp->setVisible(false);
		QPixmap pix;
		pix.load(":Paper_Docs_48.png");
		ui.labelIconDocType->setPixmap(pix);
		ui.labelDocType->setText(tr("Paper Document Location"));
		ui.txtFileName->setVisible(false);
		ui.labelFile->setVisible(false);

	}

}
void FUI_DM_Documents::PrepareNoteDocument(bool checked)
{	
	if (checked)
	{
		ui.stackedWidget->setCurrentIndex(3);
		ui.stackedWidget->setMaximumHeight(16777215);
		ui.groupBoxDetails->setMaximumHeight(16777215);
		ui.frameEditor->setVisible(false);
	
		ui.frameApplication->setVisible(false);
		ui.labelApp->setVisible(false);


		QPixmap pix;
		pix.load(":Note_48.png");
		ui.labelIconDocType->setPixmap(pix);
		ui.labelDocType->setText(tr("Note"));
		ui.txtFileName->setVisible(false);
		ui.labelFile->setVisible(false);

		m_bNoteDocument=true;
		//issue: 2673: this is not good now: leave it
		//ui.tabWidget->removeTab(0);
		ui.tabWidget->setCurrentIndex(2);

		//m_pRevSetTag->setVisible(false);
		m_pRevSaveCopy->setVisible(false);
	}

}
void FUI_DM_Documents::PrepareInternetDocument(bool checked)
{
	if (checked)
	{
		ui.stackedWidget->setCurrentIndex(2);
		
		ui.frameApplication->setVisible(true);
		ui.labelApp->setVisible(true);
		QPixmap pix;
		pix.load(":Internet_File_48.png");
		ui.labelIconDocType->setPixmap(pix);
		ui.labelDocType->setText(tr("Internet File"));
		//m_GuiFieldManagerMain->RegisterField("BDMD_DOC_PATH",ui.txtFileName);		//overwrite any prev registration
		ui.txtFileName->setVisible(true);
		ui.labelFile->setVisible(true);
		m_pRevReplaceCurrent->setVisible(false);
	}

}


bool FUI_DM_Documents::CanClose()
{
	return (m_bUnconditionalClose ||(m_nMode != MODE_EDIT));
}


//fill table, refresh icon
void FUI_DM_Documents::RefreshRevisions(bool bReloadFromServer)
{
	//m_lstRevisions.Dump();
		
	//reload from server if needed:
	if (bReloadFromServer && m_nEntityRecordID>0)
	{
		Status err;
		_SERVER_CALL(BusDocuments->ReloadRevisions(err,m_nEntityRecordID,m_recCheckOutInfo,m_lstRevisions));
		_CHK_ERR(err);
	}

	//Status:
	QString strCheckOutStatus=m_strHtmlTag+tr("Unlocked")+m_strEndHtmlTag;
	QString strCheckOutUser="";
	QString strCheckOutToolTip;
	QString strCheckOutDate="";
	QString strCheckOutPath="";
	QString strIcon=":Check_In_16.png";
	m_bDocCheckedOut=false;
	//ui.btnCheckOut->setEnabled(true);
		
	if (m_recCheckOutInfo.getRowCount()>0)
	{
		if (m_recCheckOutInfo.getDataRef(0,"BDMD_IS_CHECK_OUT").toInt()>0)
		{
			//QString strDateTimeFormat=QLocale::system().dateFormat(QLocale::ShortFormat)+" HH:mm:ss";
			strCheckOutStatus=m_strHtmlTag+tr("Checked-Out for Modification (Locked)")+m_strEndHtmlTag;
			strCheckOutUser=m_strHtmlTag+m_recCheckOutInfo.getDataRef(0,"BPER_LAST_NAME").toString()+", "+m_recCheckOutInfo.getDataRef(0,"BPER_FIRST_NAME").toString()+m_strEndHtmlTag;
			strCheckOutDate=m_strHtmlTag+m_recCheckOutInfo.getDataRef(0,"BDMD_CHECK_OUT_DATE").toDateTime().toString(Qt::LocaleDate)+m_strEndHtmlTag;
			strCheckOutPath=m_strHtmlTag+m_recCheckOutInfo.getDataRef(0,"BDMD_DOC_PATH").toString()+m_strEndHtmlTag;
			//strCheckOutToolTip=m_strHtmlTag+tr("Document is checked out by: ")+m_recCheckOutInfo.getDataRef(0,"BPER_LAST_NAME").toString()+", "+m_recCheckOutInfo.getDataRef(0,"BPER_FIRST_NAME").toString()+tr(" at ")+m_recCheckOutInfo.getDataRef(0,"BDMD_CHECK_OUT_DATE").toDateTime().toString(strDateTimeFormat)+m_strEndHtmlTag;
			//icon:
			strIcon=":Check_Out_16.png";
			m_bDocCheckedOut=true;
			//ui.btnCheckOut->setEnabled(false);
		}
	}

	//if insert and name empty: set name of file: && application from extension:
	if (m_nMode==MODE_INSERT && m_lstRevisions.getRowCount()>0)
	{
		QString strFileName=m_lstRevisions.getDataRef(0,"BDMR_NAME").toString();
		if (m_lstData.getDataRef(0,"BDMD_NAME").toString().isEmpty())
		{
			m_lstData.setData(0,"BDMD_NAME",strFileName);
			ui.txtName->setText(strFileName);
		}
		if (m_lstData.getDataRef(0,"BDMD_APPLICATION_ID").toInt()==0)
		{
			m_lstData.setData(0,"BDMD_APPLICATION_ID",DocumentHelper::GetApplicationFromExtension(strFileName));
			ui.frameApplication->RefreshDisplay();
		}
	}

	ui.labelCheckOutUser->setText(strCheckOutUser);
	ui.labelCheckOutUser->setToolTip(strCheckOutUser);

	ui.labelIcon->setPixmap(QIcon(strIcon).pixmap(16,16));
	
	ui.labelPath->setText(strCheckOutPath);
	ui.labelPath->setToolTip(strCheckOutPath);
	
	ui.labelDate->setText(strCheckOutDate);
	ui.labelStatus->setText(strCheckOutStatus);

	//set sizes:_

	int nSize=m_lstRevisions.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		double nSize=m_lstRevisions.getDataRef(i,"BDMR_SIZE").toDouble();
		QString strSize=DataHelper::GetFormatedFileSize(nSize);
		m_lstRevisions.setData(i,"SIZE",strSize);
	}



	//Display:
	m_lstRevisions.sort(m_lstRevisions.getColumnIdx("BDMR_DAT_CHECK_IN"),1);
	ui.tableWidget->RefreshDisplay();

	if (m_nMode==MODE_EDIT || m_nMode==MODE_INSERT) //only in this mode change name is allowed
	{
		if (m_bDocCheckedOut)
			ui.txtFileName->setEnabled(false);
		else
			ui.txtFileName->setEnabled(true);
	}

}


void FUI_DM_Documents::on_btnRefresh_clicked()
{
	RefreshRevisions(true);
}



void FUI_DM_Documents::on_btnCheckIn_clicked()
{
	//no check in, if not check out
	if(m_nEntityRecordID>0 && !m_bDocCheckedOut && m_lstRevisions.getRowCount()>0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please Check Out Document first!"));
		return;
	}

	//if some1 else check out, ask to overwrite: only ADMIN:
	bool bOverWrite=false;
	if (m_bDocCheckedOut)
	{
		if (g_pClientManager->GetPersonID()!=m_recCheckOutInfo.getDataRef(0,"BDMD_CHECK_OUT_USER_ID").toInt())
		{
			//warn
			int nResult=QMessageBox::question(this,tr("Warning"),tr("Document is already checked out by another user. Check in and overwrite any changes that this user made?"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
			bOverWrite=true;
		}
	}

	//if insert & already check in: overwrite:
	if (m_nMode==MODE_INSERT && m_lstRevisions.getRowCount()>0)
	{
		int nResult=QMessageBox::question(this,tr("Warning"),tr("Overwrite previously checked in document?"),tr("Yes"),tr("No"));
		if (nResult!=0) return; //only if YES
	}

	QString strTag="";
//if (ui.ckbSetTag->isChecked())
//{
//		//ask for tag
//		strTag=QInputDialog::getText(this,tr("Revision TAG"),tr("Revision TAG"));
//	}


	//if edit or view, only then save:
	if (m_nMode!=MODE_INSERT)
	{
		m_bDisableRefresh=true;
		g_DownloadManager->ScheduleCheckInDocument(m_lstData.getDataRef(0,"BDMD_ID").toInt(),m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString(),"",true,m_nMode==MODE_EDIT,ui.ckbSetTag->isChecked());
		//check in:
		/*
		if(!DocumentHelper::CheckInDocument(m_nEntityRecordID,m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString(),m_recInsertedDocumentRevision,strTag,true,m_nMode==MODE_EDIT,ui.ckbSetTag->isChecked()))
		{
			m_bDisableRefresh=false;
			RefreshRevisions(true);  //if error, refresh revisions...not insert..so ok
			return;
		}
		*/
		m_bDisableRefresh=false;
		return;
	}
	else //if insert, ask for location, create revision:
	{
		//ask for doc:
		int nSize;
		QString strPath="";
		if(!DocumentHelper::CreateDocumentRevisionFromPath(m_recInsertedDocumentRevision,strPath,true,&nSize,true))
			return;

		m_recInsertedDocumentRevision.setData(0,"BDMR_DOCUMENT_ID",m_nEntityRecordID);
		m_lstData.setData(0,"BDMD_DOC_PATH",strPath);
		m_lstData.setData(0,"BDMD_SIZE",nSize);

		if (ui.txtFileName->text().isEmpty())
		{
			//only set filename:
			QString strFileNameSource=m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString();
			QFileInfo info(strFileNameSource);
			ui.txtFileName->setText(info.fileName());
		}

		//_DUMP(m_lstData);
	}


	//refresh revisions:
	if (m_nMode==MODE_INSERT)
	{
		m_lstRevisions.clear();
	}

	//m_recInsertedDocumentRevision.Dump();

	if (m_recInsertedDocumentRevision.getRowCount()>0)
	{
		//temporary record:
		DbRecordSet rowRevision;
		rowRevision.copyDefinition(m_lstRevisions);
		rowRevision.merge(m_recInsertedDocumentRevision);
		rowRevision.setData(0,"USER_NAME",g_pClientManager->GetPersonName());

		//rowRevision.Dump();
		m_lstRevisions.merge(rowRevision);
	}

	//clear check in data:
	QVariant intNull(QVariant::Int);
	if (m_recCheckOutInfo.getRowCount()>0)
	{
		
		m_recCheckOutInfo.setData(0,"BDMD_IS_CHECK_OUT",0);
		m_recCheckOutInfo.setData(0,"BDMD_CHECK_OUT_USER_ID",intNull);
	}

	//clear m_lstdata:
	m_lstData.setData(0,"BDMD_IS_CHECK_OUT",0);
	m_lstData.setData(0,"BDMD_CHECK_OUT_USER_ID",intNull);
	m_lstData.setData(0,"BDMD_CHECK_OUT_DATE",QVariant(QVariant::DateTime));

	


	RefreshRevisions(false);

	//issue 1968:
	RefreshButtonState();

}


void FUI_DM_Documents::on_btnCheckOut_clicked()
{
	
	if(m_nEntityRecordID<=0)return; //if invalid doc, skip

	bool bReadOnly=ui.ckbReadOnly->isChecked();

	//if already check out, ask for read only
	if (m_bDocCheckedOut)
	{
		if (g_pClientManager->GetPersonID()!=m_recCheckOutInfo.getDataRef(0,"BDMD_CHECK_OUT_USER_ID").toInt())
		{
			//warn
			int nResult=QMessageBox::question(this,tr("Warning"),tr("Document is already checked out by another user. Check out as read only"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
			bReadOnly=true;
		}
		else
		{
			QMessageBox::warning(this,tr("Warning"),tr("Document already checked out!"));
			return;
		}
	}

	m_bDisableRefresh=true;
	DbRecordSet recDocument;//=m_lstRevisions;
	//QString strCheckOutLocation=DocumentHelper::CheckOutDocument(m_nEntityRecordID,m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString(),bReadOnly,recDocument,m_recCheckOutInfo,ui.ckbAskForLocation->isChecked(),m_nMode==MODE_EDIT);
	g_DownloadManager->ScheduleCheckOutDocument(m_nEntityRecordID,m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString(),bReadOnly,ui.ckbAskForLocation->isChecked(),m_nMode==MODE_EDIT);
	m_bDisableRefresh=false;


	/*
	if (strCheckOutLocation.isEmpty())
		return;

	//get back check out info:
	if (m_recCheckOutInfo.getRowCount()>0)
		m_lstData.assignRow(0,m_recCheckOutInfo,true);

	RefreshRevisions(false);

	//issue 1968:
	RefreshButtonState();
	*/

}



void FUI_DM_Documents::OnSetRevisionTag()
{

	int nRowSelected=m_lstRevisions.getSelectedRow();
	if (nRowSelected==-1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an revision!"));
		return;
	}

	//ask for tag
	QString strTag=QInputDialog::getText(this,tr("Revision TAG"),tr("Revision TAG"));
	m_lstRevisions.setData(nRowSelected,"BDMR_REVISION_TAG",strTag);

	int nRevID=m_lstRevisions.getDataRef(nRowSelected,0).toInt();
	
	//save:
	Status err;
	_SERVER_CALL(BusDocuments->UpdateRevision(err,nRevID,strTag))
	_CHK_ERR(err);

	ui.tableWidget->RefreshDisplay();
	return;

}

void FUI_DM_Documents::OnGetRevision()
{
	if (m_nMode==MODE_INSERT)
		return;

	int nRowSelected=m_lstRevisions.getSelectedRow();
	if (nRowSelected==-1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an revision!"));
		return;
	}
	int nRevID=m_lstRevisions.getDataRef(nRowSelected,0).toInt();


	g_DownloadManager->ScheduleCheckOutDocument(m_nEntityRecordID,"",true,true,true,true,"",nRevID);

/*
	//save:
	DbRecordSet recDocument;
	Status err;
	_SERVER_CALL(BusDocuments->CheckOutRevision(err,nRevID,recDocument))
	_CHK_ERR(err);
	

	if (recDocument.getRowCount()>0)
	{
		//save:
		QString strPath=DocumentHelper::GetCheckOutDocumentSavePath(recDocument.getDataRef(0,"BDMR_NAME").toString(),false);
		if (strPath.isEmpty())
			return;
		DocumentHelper::SaveFileContent(recDocument.getDataRef(0,"BDMR_CONTENT").toByteArray(),strPath,recDocument.getDataRef(0,"BDMR_IS_COMPRESSED").toInt());
	}
*/

}

void FUI_DM_Documents::OnOpenRevision()
{
	if (m_nMode==MODE_INSERT)
		return;

	int nRowSelected=m_lstRevisions.getSelectedRow();
	if (nRowSelected==-1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an revision!"));
		return;
	}
	int nRevID=m_lstRevisions.getDataRef(nRowSelected,0).toInt();


	if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_NOTE)
	{
		//get rev from server
		DbRecordSet recRev;
		Status err;
		_SERVER_CALL(BusDocuments->CheckOutRevision(err,nRevID,recRev))
		_CHK_ERR(err);
		if (recRev.getRowCount()!=1) 
			return;
		QString strHtml=QString::fromUtf8(qUncompress(recRev.getDataRef(0,"BDMR_CONTENT").toByteArray()));
		//Open in RTF dialog:
		RichEditorDlg dlg;
		dlg.SetHtml(strHtml);
		dlg.exec();


		return;
	}

	//g_DownloadManager->ScheduleCheckOutDocument(m_nEntityRecordID,"",true,false,true,true,"",nRevID);


	QString strParams=m_lstData.getDataRef(0,"BDMD_OPEN_PARAMETER").toString();
	int nAppID=m_lstData.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
	QString strAppParameters="";

	DbRecordSet recData;
	recData.addColumn(QVariant::Bool,"bReadOnly");
	recData.addColumn(QVariant::String,"strDocPath");
	recData.addColumn(QVariant::String,"strDefaultDirPath");
	recData.addColumn(QVariant::Int,"nAppID");
	recData.addColumn(QVariant::String,"strParams");
	recData.addColumn(QVariant::String,"strAppParameters");
	recData.addColumn(QVariant::Int,"nRevisionID");

	recData.addRow();
	recData.setData(0,"bReadOnly",true);
	recData.setData(0,"strDocPath","");
	recData.setData(0,"strDefaultDirPath","");
	recData.setData(0,"nAppID",nAppID);
	recData.setData(0,"strParams",strParams);
	recData.setData(0,"strAppParameters",strAppParameters);
	recData.setData(0,"nRevisionID",nRevID);

	//_DUMP(recData);
	QVariant varData;
	qVariantSetValue(varData,recData);
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_DOCUMENT_CHECK_OUT_AND_OPEN,m_nEntityRecordID,varData);
/*

	//save:
	DbRecordSet recDocument;
	Status err;
	_SERVER_CALL(BusDocuments->CheckOutRevision(err,nRevID,recDocument))
	_CHK_ERR(err);


	if (recDocument.getRowCount()>0)
	{
		//find check out directory+path:
		QString strFile=recDocument.getDataRef(0,"BDMR_NAME").toString();
		QString strPath=DocumentHelper::GetTempDirectoryForAutomaticStorage(); //ATS mechanism
		if (strPath.isEmpty())
		{
			//ask user to manually enter path, if not cancel check out
			int nResult=QMessageBox::question(NULL,tr("Warning"),tr("Automated Temporary Storage mechanism failed to determine Document check out location. Choose path for check-out location?"),tr("Yes"),tr("No"));
			if (nResult==0) 
				strPath=DocumentHelper::GetCheckOutDocumentSavePath(strFile,false);
			else
				return;
		}
		else
		{
			strPath=strPath+"/"+strFile;
		}
		if (strPath.isEmpty())
			return;

		//save:
		DocumentHelper::SaveFileContent(recDocument.getDataRef(0,"BDMR_CONTENT").toByteArray(),strPath,recDocument.getDataRef(0,"BDMR_IS_COMPRESSED").toInt());

		//open:
		QString strParams=m_lstData.getDataRef(0,"BDMD_OPEN_PARAMETER").toString();
		int nAppID=m_lstData.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
		DocumentHelper::OpenDocumentInExternalApp_Shell(nAppID,strPath,strParams); 

	}
*/
}


//if template SAPNE is filled
void FUI_DM_Documents::OnSaveFromTemplate()
{
	int nTemplateID;
	DbRecordSet rowTemplate;
	if(!ui.frameTemplate->GetCurrentEntityRecord(nTemplateID,rowTemplate))
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}

	DbRecordSet rowRevision;
	if (rowTemplate.getRowCount()==1)
	{

		if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
		{
			if(!DocumentHelper::CreateLocalDocumentFromTemplate(rowTemplate,m_lstData))
			{
				QMessageBox::warning(this,tr("Warning"),tr("Failed to load from template!"));
				return;
			}
		}
		else if (m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		{
			if (m_nMode==MODE_EDIT) 
			{
				QMessageBox::warning(this,tr("Warning"),tr("Failed to load from template!"));
				return;
			}
			DbRecordSet rowDocData;
			if(!DocumentHelper::CreateInternetDocumentFromTemplate(rowTemplate,rowDocData,rowRevision,true))
			{
				QMessageBox::warning(this,tr("Warning"),tr("Failed to load from template!"));
				return;
			}
			if (!DocumentHelper::CheckInDocument(m_nEntityRecordID,rowDocData.getDataRef(0,"BDMD_DOC_PATH").toString(),rowRevision,"",false,m_nMode==MODE_EDIT,m_lstData.getDataRef(0,"BDMD_SET_TAG_FLAG").toInt()))
			{
				QMessageBox::warning(this,tr("Warning"),tr("Failed to load from template!"));
				return;
			}
		}
		else
		{
			if(!DocumentHelper::CreateDocumentFromTemplate(rowTemplate,m_lstData,rowRevision))
			{
				QMessageBox::warning(this,tr("Warning"),tr("Failed to load from template!"));
				return;
			}

		}


		RefreshDisplay();
	}
	else
		QMessageBox::warning(this,tr("Warning"),tr("Failed to load from template!"));
}



//before save: get lists from sapne's:
void FUI_DM_Documents::GetLinkListsFromSAPNE()
{
	DbRecordSet lstTemp;
	
	//contacts:
	ui.frameContacts->GetList(lstTemp);
	lstTemp.setColumn(0,QVariant::Int,"BNMR_TABLE_KEY_ID_2");

	//extract task owners & ignore them:
	int nSize=m_lstContacts.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_lstContacts.getDataRef(i,"BNMR_SYSTEM_ROLE").toInt()!=GlobalConstants::CONTACT_LINK_ROLE_DOC_TASK_OWNER)
			continue;
		int nRow=lstTemp.find(0,m_lstContacts.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt(),true);
		if (nRow>=0)
		{
			lstTemp.deleteRow(nRow);
		}
	}

	m_lstContacts.clear();
	m_lstContacts.merge(lstTemp);
	//set role to contacts to owner
	m_lstContacts.setColValue(m_lstContacts.getColumnIdx("BNMR_SYSTEM_ROLE"),GlobalConstants::CONTACT_LINK_ROLE_DOC_OWNER);

	//projects:
	ui.frameProjects->GetList(lstTemp);
	lstTemp.setColumn(0,QVariant::Int,"BNMR_TABLE_KEY_ID_2");
	m_lstProjects.clear();
	m_lstProjects.merge(lstTemp);

}



void FUI_DM_Documents::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Escape)
	{
		on_cmdCancel();
		return;
	}

	FuiBase::keyPressEvent(event);
}



void FUI_DM_Documents::OpenDocumentInApplication()
{

	//-------------------------------------------------------
	//Open Application with document:
	//-------------------------------------------------------
	QString strFile=m_lstData.getDataRef(0,"BDMD_DOC_PATH").toString();
	int nApplication=m_lstData.getDataRef(0,"BDMD_APPLICATION_ID").toInt();

	DocumentHelper::OpenDocumentInExternalApp_Shell(nApplication,strFile,"");
}


void FUI_DM_Documents::on_btnOpenDocument_clicked()
{
	//open doc:
	if (m_nEntityRecordID<=0 || m_lstData.getRowCount()==0)
		return;

	
	DocumentHelper::OpenDocumentInExternalApp(m_lstData);
}


void FUI_DM_Documents::OnDeleteRevision()
{
	if (m_nEntityRecordID<=0 || m_lstData.getRowCount()!=1)
	{
		return;
	}

	bool bLock=true;
	if (m_nMode==MODE_EDIT)
		bLock=false;

	int nSelectedRow=m_lstRevisions.getSelectedRow();
	if (nSelectedRow==-1)
	{		
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an revision!"));
		return;
	}

	int nRevisionID=m_lstRevisions.getDataRef(nSelectedRow,"BDMR_ID").toInt();

	Status err;
	_SERVER_CALL(BusDocuments->DeleteRevision(err,m_nEntityRecordID,nRevisionID,bLock))
	_CHK_ERR(err);

	m_lstRevisions.deleteSelectedRows();
	ui.tableWidget->RefreshDisplay();

}



void FUI_DM_Documents::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if(nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}


	if (pSubject == ui.frameContacts && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnAssignedContactChanged();
		return;
	}
	if (pSubject == ui.frameProjects && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnAssignedProjectChanged();
		return;
	}

	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);

	//reload if ours:
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_CHECK_IN_DOC ||nMsgCode==ChangeManager::GLOBAL_CHECK_OUT_DOC)
	{
		if (nMsgDetail==m_nEntityRecordID && !m_bDisableRefresh)
		{
			RefreshRevisions(true); //reload check out status
		}
	}


}




void FUI_DM_Documents::OnPrint()
{
	if (m_lstData.getRowCount()==0)
		return;


	QString strContact;
	DbRecordSet lstContacts;
	ui.frameContacts->GetList(lstContacts);

	if (lstContacts.getRowCount()>0)
		strContact=lstContacts.getDataRef(0,"BCNT_NAME").toString();

	QString strProject;
	DbRecordSet lstProjects;
	ui.frameProjects->GetList(lstProjects);

	if (lstProjects.getRowCount()>0)
		strProject=lstProjects.getDataRef(0,"BUSP_CODE").toString()+" "+lstProjects.getDataRef(0,"BUSP_NAME").toString();

	//person:
	MainEntitySelectionController sel;
	sel.Initialize(ENTITY_BUS_PERSON);

	QString strPerson;
	DbRecordSet recPers;
	int iID=m_lstData.getDataRef(0,"CENT_OWNER_ID").toInt();
	sel.GetEntityRecord(iID,recPers);
	if (recPers.getRowCount()>0)
		recPers.getData(0,"BPER_NAME",strPerson);



	QStringList list;

	list<<ui.txtName->text();
	list<<strContact;
	list<<strProject;
	list<<ui.dateCreated->text();
	list<<strPerson;

	//qDebug()<<list;
	//0 - contact name?
	//1 - date
	//2 - person

	ui.frameNoteEditor->SetupPrintingHeader(list);
}


void FUI_DM_Documents::on_cmdCopy()
{
	m_lstRevisions.clear();
	FuiBase::on_cmdCopy();
}

void FUI_DM_Documents::OnAssignedContactChanged()
{
	DbRecordSet lstData;
	ui.frameContacts->GetList(lstData);
	int nCnt=lstData.getRowCount();

	QString strTabTitle=tr(" Contact(s)");
	if (nCnt)
		strTabTitle=tr(" Contact(s) (")+QVariant(nCnt).toString()+")";

	if (m_bNoteDocument)
		ui.tabWidget->setTabText(0,strTabTitle);
	else
		ui.tabWidget->setTabText(1,strTabTitle);

}

DbRecordSet *FUI_DM_Documents::GetTaskRecordSet()
{
	return &(ui.frameTask->m_recTask);
}

void FUI_DM_Documents::OnAssignedProjectChanged()
{
	DbRecordSet lstData;
	ui.frameProjects->GetList(lstData);
	int nCnt=lstData.getRowCount();

	QString strTabTitle=tr(" Project(s)");
	if (nCnt)
		strTabTitle=tr(" Project(s) (")+QVariant(nCnt).toString()+")";

	if (m_bNoteDocument)
		ui.tabWidget->setTabText(1,strTabTitle);
	else
		ui.tabWidget->setTabText(2,strTabTitle);


}


void FUI_DM_Documents::OnThemeChanged()
{
	ui.frameBkg->setStyleSheet("QFrame#frameBkg "+ThemeManager::GetSidebarChapter_Bkg());
	ui.labelDocType->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
}

void FUI_DM_Documents::changeEvent(QEvent * event)
{
	QWidget::changeEvent(event);

	if(event->type() == QEvent::WindowStateChange)
	{
		//now check to see if the window is being minimised
        if( isMinimized() )
		{
			//only for tasks
			Status status;
			ui.frameTask->GetDataFromGUI();
			bool bTaskActive = ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0;
			if(bTaskActive)
			{
				if(NULL == m_pWndAvatar)
				{
					//calculate initial position for the new window
					QPoint pt = geometry().topRight();
					pt.setX(pt.x() - 169);

					int nAvatarType = AVATAR_TASK_INTERNET_FILE;
					switch(m_lstData.getDataRef(0,"BDMD_DOC_TYPE").toInt())
					{
					case GlobalConstants::DOC_TYPE_PHYSICAL:
						nAvatarType = AVATAR_TASK_LOCAL_FILE;
						break;
					case GlobalConstants::DOC_TYPE_NOTE:
						nAvatarType = AVATAR_TASK_NOTE;
						break;
					case GlobalConstants::DOC_TYPE_INTERNET_FILE:
						nAvatarType = AVATAR_TASK_INTERNET_FILE;
						break;
					case GlobalConstants::DOC_TYPE_URL:
						nAvatarType = AVATAR_TASK_ADDRESS;
						break;
					}
					m_pWndAvatar = new fui_avatar(nAvatarType, this);
					m_pWndAvatar->move(pt);
				}

				RefreshAvatarData();
				m_pWndAvatar->show();

				if (m_bSideBarModeView)
					g_objFuiManager.ClearActiveFUI(this);

				if(m_bHideOnMinimize)
					QTimer::singleShot(0,this,SLOT(hide()));//B.T. delay hide
				m_bHideOnMinimize = true; //reset
			}
		}
	}
}

void FUI_DM_Documents::closeEvent(QCloseEvent *event)
{
	FuiBase::closeEvent(event);

	if(m_pWndAvatar){
		m_pWndAvatar->close();
		m_pWndAvatar = NULL;
	}
}


//issue 1826
void FUI_DM_Documents::SetSimpleMode()
{
	ui.groupBoxDetails->setVisible(false);
	ui.frameCategory->setVisible(false);
	ui.label_10->setVisible(false);
	ui.frameOwner->setVisible(false);
	ui.labelBirthDay_12->setVisible(false);
	ui.frameTask->setVisible(false);
	if (m_bNoteDocument)
		ui.tabWidget->setCurrentIndex(2);
	else
		ui.tabWidget->setCurrentIndex(0);
	qApp->processEvents();
	resize(600,300);
}


void FUI_DM_Documents::RefreshButtonState()
{
	//issue 1968:
	if (m_bDocCheckedOut)
	{
		ui.btnCheckOut->setEnabled(false);
		ui.btnCheckIn->setEnabled(true);
		GUI_Helper::SetStyledButtonColorBkg(ui.btnCheckIn, GUI_Helper::BTN_COLOR_YELLOW);
		ui.btnCheckOut->setStyleSheet("");
	}
	else
	{
		if (m_nMode==MODE_INSERT)
		{
			ui.btnCheckIn->setEnabled(false);
			ui.btnCheckIn->setStyleSheet("");
			ui.btnCheckOut->setStyleSheet("");
			ui.btnCheckOut->setEnabled(false); 
		}
		else
		{
			
			ui.btnCheckOut->setEnabled(true);
			ui.btnCheckIn->setEnabled(false);
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCheckOut, GUI_Helper::BTN_COLOR_YELLOW);
			ui.btnCheckIn->setStyleSheet("");
		}
	}
}

int FUI_DM_Documents::GetTaskID()
{
	if(ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0)
		return ui.frameTask->m_recTask.getDataRef(0, "BTKS_ID").toInt();

	return -1;
}

void FUI_DM_Documents::RefreshAvatarData(bool bReload)
{
	if(bReload){
		QString strWhere="WHERE BTKS_ID="+QVariant(GetTaskID()).toString();
		Status status;
		_SERVER_CALL(ClientSimpleORM->Read(status, BUS_TASKS, ui.frameTask->m_recTask, strWhere));
		ui.frameTask->SetGUIFromData();
	}

	QString strTxt = ui.frameTask->m_recTask.getDataRef(0, "BTKS_SUBJECT").toString();
	QString strContact;
	if(m_lstContacts.getRowCount() > 0){
		strContact = EMailDialog::GetTaskAssignedContact(m_lstContacts.getDataRef(0, "BNMR_TABLE_KEY_ID_2").toInt());
	}
	QString strLabel = strContact;
	strLabel += "\n";
	strLabel += strTxt;
	m_pWndAvatar->SetLabel(strLabel);
	m_pWndAvatar->SetRecord(m_nEntityRecordID);

	m_pWndAvatar->SetTaskID(ui.frameTask->m_recTask.getDataRef(0, "BTKS_ID").toInt());

	QString strDesc = ui.frameTask->m_recTask.getDataRef(0, "BTKS_DESCRIPTION").toString();
	m_pWndAvatar->SetTaskDescTooltip(strDesc);

	QString strDate = EMailDialog::GetTaskDate(CommGridViewHelper::DOCUMENT, m_lstData, ui.frameTask->m_recTask);
	m_pWndAvatar->SetTaskDate(strDate);

	QString strOwner = EMailDialog::GetPersonInitials(ui.frameTask->m_recTask.getDataRef(0, "BTKS_OWNER").toInt());
	m_pWndAvatar->SetTaskOwnerInitials(strOwner);

	int nTask = CommGridViewHelper::CheckTask(ui.frameTask->m_recTask);
	m_pWndAvatar->SetTaskState(nTask);
}


void FUI_DM_Documents::OnNoteEditor_VerticalOpenClose_clicked(bool bSmaller)
{
	if (!bSmaller)
	{
		ui.groupBox_2->setVisible(false);
		ui.groupBox_3->setVisible(false);
		ui.frameCategory->setVisible(false);
		ui.label_10->setVisible(false);
		ui.frameOwner->setVisible(false);
		ui.labelBirthDay_12->setVisible(false);
		ui.frameTask->setVisible(false);
		ui.tabWidget->setVisible(false);
	}
	else
	{
		ui.groupBox_2->setVisible(true);
		ui.groupBox_3->setVisible(true);
		ui.frameCategory->setVisible(true);
		ui.label_10->setVisible(true);
		ui.frameOwner->setVisible(true);
		ui.labelBirthDay_12->setVisible(true);
		ui.frameTask->setVisible(true);
		ui.tabWidget->setVisible(true);
	}
}
void FUI_DM_Documents::OnEditor_VerticalOpenClose_clicked(bool bSmaller)
{
	if (!bSmaller)
	{
		ui.groupBoxDetails->setVisible(false);
		ui.frameCategory->setVisible(false);
		ui.label_10->setVisible(false);
		ui.frameOwner->setVisible(false);
		ui.labelBirthDay_12->setVisible(false);
		ui.frameTask->setVisible(false);
	}
	else
	{
		ui.groupBoxDetails->setVisible(true);
		ui.frameCategory->setVisible(true);
		ui.label_10->setVisible(true);
		ui.frameOwner->setVisible(true);
		ui.labelBirthDay_12->setVisible(true);
		ui.frameTask->setVisible(true);
	}
}

void FUI_DM_Documents::OnCheckInOutCompleted(Status err,int nDocumentID,QString strFilePath,DbRecordSet recRev,DbRecordSet recCheckOutInfo)
{
	if (recCheckOutInfo.getRowCount()==0)
		return;
	if (recCheckOutInfo.getDataRef(0,"BDMD_ID").toInt()!=m_nEntityRecordID)
		return;
	if (!err.IsOK())
	{
		RefreshRevisions(true);  //if error, refresh revisions...not insert..so ok
		return;
	}
	m_recCheckOutInfo=recCheckOutInfo;
	if (m_recCheckOutInfo.getRowCount()>0)
		m_lstData.assignRow(0,recCheckOutInfo,true);
	RefreshRevisions(true);
	RefreshButtonState();
}


void FUI_DM_Documents::CreateNoteRevisionIfNeeded(DbRecordSet &rowRevision)
{
	rowRevision.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_REVISIONS));
	QString strContent=m_lstData.getDataRef(0,"BDMD_DESCRIPTION").toString();
	if (m_strOriginalNote==m_lstData.getDataRef(0,"BDMD_DESCRIPTION").toString())
		return;

	rowRevision.addRow();
	rowRevision.setData(0,"BDMR_CHECK_IN_USER_ID",g_pClientManager->GetPersonID());
	QByteArray content=qCompress(strContent.toUtf8(),1);
	rowRevision.setData(0,"BDMR_CONTENT",content);   //compress text content
	rowRevision.setData(0,"BDMR_NAME",ui.txtName->text());
	rowRevision.setData(0,"BDMR_DAT_CHECK_IN",QDateTime::currentDateTime());
	rowRevision.setData(0,"BDMR_SIZE",strContent.size());
	rowRevision.setData(0,"BDMR_IS_COMPRESSED",1);
}

void FUI_DM_Documents::OnOnReplaceCurrentRevision()
{
	if (m_nMode!=MODE_EDIT)
		return;

	int nRowSelected=m_lstRevisions.getSelectedRow();
	if (nRowSelected==-1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an revision!"));
		return;
	}
	int nRevID=m_lstRevisions.getDataRef(nRowSelected,0).toInt();

	//get rev from server
	DbRecordSet recRev;
	Status err;
	_SERVER_CALL(BusDocuments->CheckOutRevision(err,nRevID,recRev))
		_CHK_ERR(err);
	if (recRev.getRowCount()!=1) 
		return;
	QString strHtml=QString::fromUtf8(qUncompress(recRev.getDataRef(0,"BDMR_CONTENT").toByteArray()));
	//ui.frameEditor->SetHtml(strHtml);
	m_lstData.setData(0,"BDMD_DESCRIPTION",strHtml);
	RefreshDisplay();
}


void FUI_DM_Documents::updateTaskTabCheckbox()
{
	//customize 2nd tab title to simulate icon
	ui.tabWidgetMain->setTabIcon(1, (ui.frameTask->isChecked())? QIcon(":/checkbox_checked.png") : QIcon(":/checkbox_unchecked.png"));
}


