#include "commworkbenchwidget.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/globalconstants.h"
#include "fui_voicecall.h"
#include "fui_voicecallcenter.h"
#include "fui_dm_documents.h"
#include "emaildialog.h"
#include "postponetaskdialog.h"
#include "commgridfilter.h"
#include "bus_core/bus_core/optionsandsettingsorganizer.h"
#include "bus_core/bus_core/commfiltersettingsids.h"
#include "bus_core/bus_core/commgridfiltersettings.h"
#include "gui_core/gui_core/multisortcolumn.h"
#include "sidebarfui_base.h"
#include "communicationactions.h"
#include "bus_client/bus_client/documenthelper.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"
//GLOBALS:

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/clientdownloadmanager.h"
extern ClientDownloadManager *g_DownloadManager;

#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager

CommWorkBenchWidget::CommWorkBenchWidget(QWidget *parent, bool bSideBarMode /*= false*/)
	: QWidget(parent),m_bInitialized(false)
{
	m_bSideBarMode = false;
	if (bSideBarMode)
		m_bSideBarMode = true;
	else if (ThemeManager::GetViewMode() == ThemeManager::VIEW_SIDEBAR)
		m_bSideBarMode = true;
	
	ui.setupUi(this);

	m_bStartSplashScreen = false;
	
	m_pParentFUI = NULL;
	m_nCurrentViewID = -1;
	m_recFilterViews.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW));
	m_recFilterViewData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));

	//Header look and feel.
	m_nTaskID = 0;
	m_nLoggedPersonID = g_pClientManager->GetPersonID();
	m_nCurrentEntityID = m_nLoggedPersonID;

	//User SAPNE.
	ui.User_frame->Initialize(ENTITY_BUS_PERSON);
	ui.User_frame->SetCurrentEntityRecord(m_nLoggedPersonID);
	//DbRecordSet recTVIEW_BUS_PERSON_SELECTION;
	//ui.User_frame->GetCurrentEntityRecord(m_nCurrentEntityID, recTVIEW_BUS_PERSON_SELECTION);
	//recTVIEW_BUS_PERSON_SELECTION.Dump();
	m_strCurrentPersonInitials = g_pClientManager->GetPersonInitials(); 	//recTVIEW_BUS_PERSON_SELECTION.getDataRef(0, "BPER_INITIALS").toString();
	ui.User_frame->registerObserver(this);

	//Owner SAPNE.
	ui.Owner_frame->Initialize(ENTITY_BUS_PERSON);
	ui.Owner_frame->registerObserver(this);

	//Contact SAPNE.
	ui.Contact_frame->Initialize(ENTITY_BUS_CONTACT);
	ui.Contact_frame->registerObserver(this);

	//Project SAPNE.
	ui.Project_frame->Initialize(ENTITY_BUS_PROJECT);
	ui.Project_frame->registerObserver(this);

	//Date range selector.
	connect(ui.dateFilter_frame,SIGNAL(onDateChanged(QDateTime&, QDateTime&, int, bool)),this,SLOT(OnDateChanged(QDateTime&, QDateTime&, int, bool)));

	//Auto raise buttons.
	ui.pieMenu_toolButton->setAutoRaise(true);
	ui.openDoc_toolButton->setAutoRaise(true);
	ui.startTask_toolButton->setAutoRaise(true);
	ui.PostponeTask_toolButton->setAutoRaise(true);
	ui.taskDone_toolButton->setAutoRaise(true);
	ui.select_toolButton->setAutoRaise(true);
	ui.modify_toolButton->setAutoRaise(true);
	ui.clear_toolButton->setAutoRaise(true);
	ui.AddDoc_toolButton->setAutoRaise(true);
	ui.AddMail_toolButton->setAutoRaise(true);
	ui.AddVoiceCall_toolButton->setAutoRaise(true);
	ui.checkIn_toolButton->setAutoRaise(true);
	ui.Reply_toolButton->setAutoRaise(true);
	ui.Forward_toolButton->setAutoRaise(true);
	ui.sort_toolButton->setAutoRaise(true);
	ui.PackAndSend_toolButton->setAutoRaise(true);
	ui.showFilter_toolButton->setAutoRaise(true);
	ui.hideFilter_toolButton->setAutoRaise(true);
	ui.refresh2_toolButton->setAutoRaise(true);
	
	//Set buttons and icon sizes.
	QSize IconSize;
	if(m_bSideBarMode)
		IconSize = QSize(16,16);
	else
		IconSize = QSize(32,32);
	
	ui.pieMenu_toolButton->setMinimumSize(IconSize);
	ui.openDoc_toolButton->setMinimumSize(IconSize);
	ui.startTask_toolButton->setMinimumSize(IconSize);
	ui.PostponeTask_toolButton->setMinimumSize(IconSize);
	ui.taskDone_toolButton->setMinimumSize(IconSize);
	ui.select_toolButton->setMinimumSize(IconSize);
	ui.modify_toolButton->setMinimumSize(IconSize);
	ui.clear_toolButton->setMinimumSize(IconSize);
	ui.AddDoc_toolButton->setMinimumSize(IconSize);
	ui.AddMail_toolButton->setMinimumSize(IconSize);
	ui.AddVoiceCall_toolButton->setMinimumSize(IconSize);
	ui.checkIn_toolButton->setMinimumSize(IconSize);
	ui.Reply_toolButton->setMinimumSize(IconSize);
	ui.Forward_toolButton->setMinimumSize(IconSize);
	ui.PackAndSend_toolButton->setMinimumSize(IconSize);
	ui.showFilter_toolButton->setMinimumSize(IconSize);
	ui.refresh2_toolButton->setMinimumSize(IconSize);
	
	ui.pieMenu_toolButton->setIconSize(IconSize);
	ui.select_toolButton->setIconSize(IconSize);
	ui.modify_toolButton->setIconSize(IconSize);
	ui.clear_toolButton->setIconSize(IconSize);
	ui.AddDoc_toolButton->setIconSize(IconSize);
	ui.AddMail_toolButton->setIconSize(IconSize);
	ui.AddVoiceCall_toolButton->setIconSize(IconSize);
	ui.checkIn_toolButton->setIconSize(IconSize);
	ui.Reply_toolButton->setIconSize(IconSize);
	ui.Forward_toolButton->setIconSize(IconSize);
	ui.PackAndSend_toolButton->setIconSize(IconSize);
	ui.showFilter_toolButton->setIconSize(IconSize);
	ui.refresh2_toolButton->setIconSize(IconSize);

	ui.refresh_pushButton->setMinimumSize(QSize(32,32));
	ui.refresh_pushButton->setMaximumSize(QSize(32,32));
	ui.refresh_pushButton->setIconSize(QSize(32,32));
	ui.refresh_pushButton->setText("");
	ui.refresh_pushButton->setIcon(QIcon(":reload32.png"));
	ui.refresh_pushButton->setToolTip(tr("Refresh"));
	ui.refresh_pushButton->setFlat(true);

	ui.saveView_toolButton->setMinimumSize(QSize(24,24));
	ui.saveView_toolButton->setIconSize(QSize(24,24));
	ui.saveView_toolButton->setText("");
	ui.saveView_toolButton->setIcon(QIcon(":DefaultView24.png"));
	ui.saveView_toolButton->setToolTip(tr("Remember View"));
	ui.saveView_toolButton->setAutoRaise(true);


	//Set button icons.
	if(m_bSideBarMode)
	{
		ui.pieMenu_toolButton->setIcon(QIcon(":PieMenu16.png"));
		//Open doc. tool button.
		ui.openDoc_toolButton->setIconSize(IconSize);
		ui.openDoc_toolButton->setIcon(QIcon(":OpenDoc16.png"));
		ui.openDoc_toolButton->setToolTip(tr("Open Document"));
		//Start task.
		ui.startTask_toolButton->setIcon(QIcon(":StartTask16.png"));
		ui.startTask_toolButton->setIconSize(IconSize);
		ui.startTask_toolButton->setToolTip(tr("Start Task"));
		//Postpone task.
		ui.PostponeTask_toolButton->setIcon(QIcon(":Postpone_16.png"));
		ui.PostponeTask_toolButton->setIconSize(IconSize);
		ui.PostponeTask_toolButton->setToolTip(tr("Postpone Task"));
		//Done task.
		ui.taskDone_toolButton->setIcon(QIcon(":TaskDone16.png"));
		ui.taskDone_toolButton->setIconSize(IconSize);
		ui.taskDone_toolButton->setToolTip(tr("Task Done / Email Read"));

		ui.select_toolButton->setIcon(QIcon(":View16.png"));
		ui.select_toolButton->setToolTip(tr("View"));
		ui.modify_toolButton->setIcon(QIcon(":Edit16.png"));
		ui.modify_toolButton->setToolTip(tr("Edit"));
		ui.clear_toolButton->setIcon(QIcon(":Delete16.png"));
		ui.clear_toolButton->setToolTip(tr("Delete"));

		//These buttons are hidden and not need 24 pix icons.
		ui.AddDoc_toolButton->setIcon(QIcon(":Document32bPLUS.png"));
		ui.AddDoc_toolButton->setToolTip(tr("New Document"));
		ui.AddMail_toolButton->setIcon(QIcon(":Email32PLUS.png"));
		ui.AddMail_toolButton->setToolTip(tr("New Email"));
		ui.AddVoiceCall_toolButton->setIcon(QIcon(":Phone32PLUS.png"));
		ui.AddVoiceCall_toolButton->setToolTip(tr("New Phone Call"));

		ui.checkIn_toolButton->setIcon(QIcon(":Check_In_16.png"));
		ui.checkIn_toolButton->setToolTip(tr("Check In"));
		ui.Reply_toolButton->setIcon(QIcon(":Email_Reply_16.png"));
		ui.Reply_toolButton->setToolTip(tr("Reply"));
		ui.Forward_toolButton->setIcon(QIcon(":Email_Forward_16.png"));
		ui.Forward_toolButton->setToolTip(tr("Forward"));
		ui.PackAndSend_toolButton->setIcon(QIcon(":Email_Attach_16.png"));
		ui.PackAndSend_toolButton->setToolTip(tr("Pack and Send"));
		//Marin wanted this icon to be 24,24 and other 16,16.
		ui.sort_toolButton->setIconSize(QSize(24,24));
		ui.sort_toolButton->setMinimumSize(QSize(24,24));
		ui.sort_toolButton->setIcon(QIcon(":Sort_24.png"));
		ui.sort_toolButton->setToolTip(tr("Sort"));

		ui.filter_frame->setVisible(false);

		ui.spacer->changeSize(8, 46, QSizePolicy::Fixed);
		ui.spacer_3->changeSize(8, 46, QSizePolicy::Fixed);

		ui.showFilter_toolButton->setIconSize(QSize(24,24));
		ui.showFilter_toolButton->setMinimumSize(QSize(24,24));
		ui.showFilter_toolButton->setIcon(QIcon(":Eye.png"));
		ui.showFilter_toolButton->setToolTip(tr("Show Filter Frame"));
		
		ui.hideFilter_toolButton->setIconSize(QSize(24,24));
		ui.hideFilter_toolButton->setMinimumSize(QSize(24,24));
		ui.hideFilter_toolButton->setIcon(QIcon(":Eye_OK.png"));
		ui.hideFilter_toolButton->setToolTip(tr("Hide Filter Frame"));

		ui.refresh2_toolButton->setIconSize(QSize(24,24));
		ui.refresh2_toolButton->setMinimumSize(QSize(24,24));
		ui.refresh2_toolButton->setIcon(QIcon(":reload32.png"));
		ui.refresh2_toolButton->setToolTip(tr("Refresh"));
	}
	else
	{
		ui.pieMenu_toolButton->setIcon(QIcon(":PieMenu_big.png"));
		//Open doc. tool button.
		ui.openDoc_toolButton->setIconSize(IconSize);
		ui.openDoc_toolButton->setIcon(QIcon(":OpenDoc.png"));
		ui.openDoc_toolButton->setToolTip(tr("Open Document"));
		//Start task.
		ui.startTask_toolButton->setIcon(QIcon(":StartTask.png"));
		ui.startTask_toolButton->setIconSize(IconSize);
		ui.startTask_toolButton->setToolTip(tr("Start Task"));
		//Postpone task.
		ui.PostponeTask_toolButton->setIcon(QIcon(":Postpone32.png"));
		ui.PostponeTask_toolButton->setIconSize(IconSize);
		ui.PostponeTask_toolButton->setToolTip(tr("Postpone Task"));
		//Done task.
		ui.taskDone_toolButton->setIcon(QIcon(":Task_Done.png"));
		ui.taskDone_toolButton->setIconSize(IconSize);
		ui.taskDone_toolButton->setToolTip(tr("Task Done / Email Read"));

		ui.select_toolButton->setIcon(QIcon(":View32.png"));
		ui.select_toolButton->setToolTip(tr("View"));
		ui.modify_toolButton->setIcon(QIcon(":Edit32.png"));
		ui.modify_toolButton->setToolTip(tr("Edit"));
		ui.clear_toolButton->setIcon(QIcon(":Delete32.png"));
		ui.clear_toolButton->setToolTip(tr("Delete"));

		ui.AddDoc_toolButton->setIcon(QIcon(":Document32bPLUS.png"));
		ui.AddDoc_toolButton->setToolTip(tr("New Document"));
		ui.AddMail_toolButton->setIcon(QIcon(":Email32PLUS.png"));
		ui.AddMail_toolButton->setToolTip(tr("New Email"));
		ui.AddVoiceCall_toolButton->setIcon(QIcon(":Phone32PLUS.png"));
		ui.AddVoiceCall_toolButton->setToolTip(tr("New Phone Call"));

		ui.checkIn_toolButton->setIcon(QIcon(":Check_In_32.png"));
		ui.checkIn_toolButton->setToolTip(tr("Check In"));
		ui.Reply_toolButton->setIcon(QIcon(":Email_Reply_32.png"));
		ui.Reply_toolButton->setToolTip(tr("Reply"));
		ui.Forward_toolButton->setIcon(QIcon(":Email_Forward_32.png"));
		ui.Forward_toolButton->setToolTip(tr("Forward"));
		ui.PackAndSend_toolButton->setIcon(QIcon(":Email_Attach_32.png"));
		ui.PackAndSend_toolButton->setToolTip(tr("Pack and Send"));
		ui.sort_toolButton->setIcon(QIcon(":Sort_32.png"));
		ui.sort_toolButton->setIconSize(QSize(32,32));
		ui.sort_toolButton->setMinimumSize(QSize(32,32));
		ui.sort_toolButton->setToolTip(tr("Sort"));

		ui.filter_frame->setVisible(true);
		ui.showFilter_toolButton->setVisible(false);
		ui.hideFilter_toolButton->setVisible(false);
		ui.refresh2_toolButton->setVisible(false);
	}

	//Set title label.
	QString strTitleText1 = tr("Communication & Tasks");
	QString strTitleText2 = tr("(Documents, Emails, Phone Calls)");
	QString strHtmlText = "<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:11pt; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt; font-weight:bold; \">" + strTitleText1 + " </span>" + strTitleText2 + "</body></html>";
	ui.TitleLabel->setText(strHtmlText);

	//Set pie menu contact to no contact.
	ui.pieMenu_toolButton->SetEntityRecordID(0);

	//Disable header buttons.
	DisableHeader();

	//Filter selector.
	ui.viewSelect_toolButton->setAutoRaise(true);
	ui.viewSelect_toolButton->setIconSize(QSize(32,32));
	ui.viewSelect_toolButton->setIcon(QIcon(":View32.png"));
	ui.viewSelect_toolButton->setToolTip(tr("View"));

	//Setup sorting recordset.
	CommGridViewHelper::SetupSortRecordSet(m_lstSortColumnSetup);

	//Set layout for side bar view.
	if(m_bSideBarMode)
	{
		setMinimumWidth(300);
		
		//Hide title frame.
		ui.Title_frame->hide();
		
		//Set grid layout horizontal spacing to 0.
		QGridLayout *gridLayout = (dynamic_cast<QGridLayout *>(layout()));
		gridLayout->setHorizontalSpacing(0);
		
		//Set filter frame geometry.
		ui.filter_frame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		ui.filter_frame->setMinimumWidth(298);
		ui.filter_frame->setMaximumWidth(16777215);
		ui.filter_frame->setMinimumHeight(88);
		ui.filter_frame->setMaximumHeight(88);
		ui.filter_frame->layout()->setSpacing(0);
		gridLayout = (dynamic_cast<QGridLayout *>(ui.filter_frame->layout()));
		QSpacerItem *spacerIt = new QSpacerItem(100, 10, QSizePolicy::Expanding);
		gridLayout->addItem(spacerIt, 1, 1);
		
		ui.viewSelect_toolButton->resize(24,24);
		ui.viewSelect_toolButton->setIconSize(QSize(24,24));

		ui.refresh_pushButton->setMinimumSize(QSize(24,24));
		ui.refresh_pushButton->setMaximumSize(QSize(24,24));
		ui.refresh_pushButton->setIconSize(QSize(24,24));
		ui.refresh_pushButton->setIcon(QIcon(":reload24.png"));

		//Make view selection combo shorter.
		ui.viewSelect_comboBox->setMaximumHeight(18);
		ui.viewSelect_comboBox->setMinimumWidth(100);

		//Take sapne frame from layout and stick it to the bottom of the widget.
		QLayoutItem *layoutItem = layout()->takeAt(2);
		QHBoxLayout *newLayout = new QHBoxLayout();
		layoutItem->widget()->setMaximumHeight(70);
		newLayout->addItem(layoutItem);
		newLayout->setContentsMargins( 0, 0, 0, 0);
		ui.sidebarSapne_frame->setLayout(newLayout);

		//Set layout horizontal spacing to 0.
		gridLayout->setHorizontalSpacing(0);

		//Hide buttons.
		ui.AddDoc_toolButton->hide();
		ui.AddMail_toolButton->hide();
		ui.AddVoiceCall_toolButton->hide();

		//User selection.
		ui.User_label->hide();
		ui.User_frame->hide();

		//Buttons frame spacers setup.
		ui.buttons_frame->setMinimumHeight(40);
		ui.buttons_frame->setMaximumHeight(40);
		//ui.buttons_frame->setContentsMargins( 2, 0, 0, 0); commented in issue #1985.
		
		ui.refresh_pushButton->setMinimumWidth(50);

		QSpacerItem *spacer = ui.buttons_frame->layout()->itemAt(5)->spacerItem();
		spacer->changeSize(10, 10);
		spacer = ui.buttons_frame->layout()->itemAt(9)->spacerItem();
		spacer->changeSize(10, 10);
		spacer = ui.buttons_frame->layout()->itemAt(13)->spacerItem();
		spacer->changeSize(10, 10);
		spacer = ui.buttons_frame->layout()->itemAt(17)->spacerItem();
		spacer->changeSize(0, 10);


		//BT: 
		//ui.filter_frame->setStyleSheet("QFrame#filter_frame {background-color: rgb(233,236,240)} QLabel {font-size: 9px} QLineEdit {font-size: 9px} QComboBox {font-size: 9px} QDateEdit {font-size: 9px}");
		ui.filter_frame->setStyleSheet("QFrame#filter_frame {background-color: rgb(233,236,240)} * {font-size: 9px}");
		ui.sapne_frame->setStyleSheet("QFrame#sapne_frame {background-color: rgb(233,236,240)}");
		ui.buttons_frame->setStyleSheet("QFrame#buttons_frame {background-color: rgb(233,236,240)}");

		ui.filter_frame->setFrameShape(QFrame::WinPanel);
		ui.sapne_frame->setFrameShape(QFrame::WinPanel);
		ui.buttons_frame->setFrameShape(QFrame::WinPanel);
	}
	else
	{
		setMinimumWidth(715);
		ui.buttons_frame->setMinimumHeight(50);
		ui.buttons_frame->setMaximumHeight(50);
//		ui.filter_frame->setMinimumWidth(399);
//		ui.filter_frame->setMaximumWidth(399);
		ui.sidebarSapne_frame->hide();
	}

	//Connect stuff.
	connect(ui.CommGrid_widget, SIGNAL(SectionClicked(int, int, int, int, int, QList<int>, int, int, int, int, int, QList<int>, QList<int>, QList<int>)), this, SLOT(OnSelectionChanged(int, int, int, int, int, QList<int>, int, int, int, int, int, QList<int>, QList<int>, QList<int>)));
	connect(ui.CommGrid_widget, SIGNAL(SectionDoubleClicked()), this, SLOT(on_openDoc_toolButton_clicked()));
	connect(ui.CommGrid_widget, SIGNAL(CnxtMenuActionTriggered(int)), this, SLOT(OnCnxtMenuActionTriggered(int)));
	

	g_ChangeManager.registerObserver(this); 
}

CommWorkBenchWidget::~CommWorkBenchWidget()
{
	m_pParentFUI = NULL;
	g_ChangeManager.unregisterObserver(this); 
}

void CommWorkBenchWidget::SetCurrentGridInSidebarMode(bool bCurrent)
{
	m_bIsCurrentFUIinSideBarMode = bCurrent;
}

Selection_SAPNE_FUI* CommWorkBenchWidget::GetContactSAPNE()
{
	return ui.Contact_frame;
	//return ui.CommGridHeader_widget->GetContactSAPNE();
}

void CommWorkBenchWidget::Initialize(int nGridType, QWidget *pParentFUI, DbRecordSet *recEntities /*= NULL*/)
{
	m_bInitialized=true;
	m_nGridType = nGridType;
	m_pParentFUI = 	pParentFUI;
	m_recEntities = recEntities;
	bool bReloadFromDefaultSavedView = false;

	if (m_nGridType == CommGridViewHelper::DESKTOP_GRID)
	{
		m_recEmail.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW));
		m_recVoiceCall.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
		m_recDocument.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW));
	}
	else if (m_nGridType == CommGridViewHelper::CONTACT_GRID)
	{
		m_recEmail.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW));
		m_recVoiceCall.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
		m_recDocument.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW));
	}
	else
	{
		m_recEmail.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW));
		m_recVoiceCall.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
		m_recDocument.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW));
	}

	m_recEmail.addColumn(QVariant::Int, "EMAIL_ATTACHMENT_COUNT");
	
	if (m_nGridType == CommGridViewHelper::CONTACT_GRID)
	{
		//First load views to see if there is saved view.
		if (!ReloadViewSelector(-1))
			return;
		
		int nSavedDefaultView = g_pSettings->GetPersonSetting(CONTACT_DEF_FILTER_VIEW).toInt();
		//If there is not that view than set default view to -1.
		if (ui.viewSelect_comboBox->findData(nSavedDefaultView) < 0)
			g_pSettings->SetPersonSetting(CONTACT_DEF_FILTER_VIEW, -1);
		else
		{
			bReloadFromDefaultSavedView = true;
			m_nCurrentViewID = nSavedDefaultView;
		}
		//If no view loaded then fill default values to filter (show all).
		if(m_nCurrentViewID < 0)
			FillInsertDefaultValues(m_recFilterViewData);
	}
	else if (m_nGridType == CommGridViewHelper::PROJECT_GRID)
	{
		//First load views to see if there is saved view.
		if (!ReloadViewSelector(-1))
			return;
		int nSavedDefaultView = g_pSettings->GetPersonSetting(PROJECT_DEF_FILTER_VIEW).toInt();
		//If there is not that view than set default view to -1.
		if (ui.viewSelect_comboBox->findData(nSavedDefaultView) < 0)
			g_pSettings->SetPersonSetting(PROJECT_DEF_FILTER_VIEW, -1);
		else	
		{
			bReloadFromDefaultSavedView = true;
			m_nCurrentViewID = nSavedDefaultView;
		}
		//If no view loaded then fill default values to filter (show all).
		if(m_nCurrentViewID < 0)
			FillInsertDefaultValues(m_recFilterViewData);
	}
	
	//If in side bar mode and in desktop, hide eye of the tiger and show filter frame.
	if (m_nGridType == CommGridViewHelper::DESKTOP_GRID && m_bSideBarMode)
	{
		ui.filter_frame->setVisible(true);
		ui.showFilter_toolButton->setVisible(false);
		ui.hideFilter_toolButton->setVisible(false);
		ui.refresh2_toolButton->setVisible(false);
	}

	//Hide save view button in destop grid.
	if (m_nGridType == CommGridViewHelper::DESKTOP_GRID)
	{
		ui.saveView_toolButton->setVisible(false);
	}

	if (nGridType == CommGridViewHelper::CALENDAR_GRID)
	{
		ui.filter_frame->hide();
		///ui.showFilter_toolButton->setVisible(false);
	}
	
	InitializeHeader(nGridType);
	if (!ReloadViewSelector(m_nCurrentViewID, bReloadFromDefaultSavedView))
		return;
}

void CommWorkBenchWidget::SetEntitiesRecordset(DbRecordSet *recEntities)
{
	m_recEntities = recEntities;
	ReloadData();
}

void CommWorkBenchWidget::DesktopStartupInitialization(int nLoggedPersonID, int nViewID)
{
	m_nCurrentEntityID = nLoggedPersonID;
	ui.viewSelect_comboBox->setCurrentIndex(ui.viewSelect_comboBox->findData(nViewID));
}

void CommWorkBenchWidget::SetData(int nEntityRecordID)
{
	if (m_nGridType == CommGridViewHelper::DESKTOP_GRID)
		SetPerson(nEntityRecordID);
	else if (m_nGridType == CommGridViewHelper::CONTACT_GRID)
		ContactChanged(nEntityRecordID);
	else if (m_nGridType == CommGridViewHelper::PROJECT_GRID)
		ProjectChanged(nEntityRecordID);
	else if (m_nGridType == CommGridViewHelper::CALENDAR_GRID)
		SetCalendar();
}

void CommWorkBenchWidget::SelectTask(int nCE_Type, int nID, bool bFromCalendar /*= false*/)
{
	DbRecordSet lstSelected;
	switch(nCE_Type)
	{
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		lstSelected.addColumn(QVariant::Int, "BVC_ID");
		lstSelected.addRow();
		lstSelected.setData(0, "BVC_ID", nID);
		break;
	case GlobalConstants::CE_TYPE_EMAIL:
		lstSelected.addColumn(QVariant::Int, "BEM_ID");
		lstSelected.addRow();
		lstSelected.setData(0, "BEM_ID", nID);
		break;
	default:
		lstSelected.addColumn(QVariant::Int, "BDMD_ID");
		lstSelected.addRow();
		lstSelected.setData(0, "BDMD_ID", nID);
		break;
	}

	SetSelectionOnNewItems(nCE_Type, lstSelected, bFromCalendar);
}

void CommWorkBenchWidget::ModifySelectedTask()
{
	on_modify_toolButton_clicked();
}

DbRecordSet CommWorkBenchWidget::GetSortRecordSet()
{
	return m_lstSortRecordSet;
}

void CommWorkBenchWidget::SetFilter(int nViewID, DbRecordSet recNewView, DbRecordSet recFilterViewData)
{
	m_nCurrentViewID = nViewID;

	ReloadParameter relParam = DEFAULT_RELOAD;

	if (!ReloadViewSelector(nViewID))
		return;
	
	ui.viewSelect_comboBox->blockSignals(true);
	ui.viewSelect_comboBox->setCurrentIndex(ui.viewSelect_comboBox->findData(nViewID));
	ui.viewSelect_comboBox->blockSignals(false);
	
	if (m_nGridType == CommGridViewHelper::CALENDAR_GRID)
	{
		int nRow = recFilterViewData.find("BUSCS_SETTING_ID", FILTER_BY_TASK_TYPE_ACTIVE, true);
		if (nRow >= 0)
		{
			int nFilterByTaskType = recFilterViewData.getDataRef(nRow, "BUSCS_VALUE").toInt();
			if (nFilterByTaskType)
			{
				CommGridFilterSettings filterSettings;
				m_recFilterViewData = filterSettings.GetCalendarGridTasksFilterSettings();

				//Task type filter active.
				nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", FILTER_BY_TASK_TYPE_ACTIVE, true);
				Q_ASSERT(nRow >= 0);
				m_recFilterViewData.setData(nRow, "BUSCS_VALUE", nFilterByTaskType);
				
				//Priority from.
				nRow = recFilterViewData.find("BUSCS_SETTING_ID", TASK_PRIORITY_FROM, true);
				int nTaskPriorityFrom = 0;
				if (nRow >= 0)
					nTaskPriorityFrom = recFilterViewData.getDataRef(nRow, "BUSCS_VALUE").toInt();

				nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", TASK_PRIORITY_FROM, true);
				Q_ASSERT(nRow >= 0);
				m_recFilterViewData.setData(nRow, "BUSCS_VALUE", nTaskPriorityFrom);

				//Priority to.
				nRow = recFilterViewData.find("BUSCS_SETTING_ID", TASK_PRIORITY_TO, true);
				int nTaskPriorityTo = 9;
				if (nRow >= 0)
					nTaskPriorityTo = recFilterViewData.getDataRef(nRow, "BUSCS_VALUE").toInt();

				nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", TASK_PRIORITY_TO, true);
				Q_ASSERT(nRow >= 0);
				m_recFilterViewData.setData(nRow, "BUSCS_VALUE", nTaskPriorityTo);

				//Task types.
				nRow = recFilterViewData.find("BUSCS_SETTING_ID", TASK_TYPE_FILTER, true);
				QByteArray bytTypes;
				if (nRow >= 0)
					bytTypes = recFilterViewData.getDataRef(nRow, "BUSCS_VALUE_BYTE").toByteArray();

				nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", TASK_TYPE_FILTER, true);
				Q_ASSERT(nRow >= 0);
				m_recFilterViewData.setData(nRow, "BUSCS_VALUE_BYTE", bytTypes);

				relParam = CALENDAR_TASK_TYPE_RELOAD;
			}
		}
		else
			return;
	}
	else
	{
		m_recFilterViewData = recFilterViewData;
		SetupDateRangeSelector();
	}
	
	//Issue #2119.
	QTimer::singleShot(2000, this, SLOT(OpenSplashScreen()));
	m_bStartSplashScreen = true;
	ReloadData(relParam);
	m_bStartSplashScreen = false;
	g_pClientManager->StopProgressDialog();
	
	emit FilterChanged(m_strCurrentPersonInitials + ": " + ui.viewSelect_comboBox->currentText());
}

void CommWorkBenchWidget::GetFilter(DbRecordSet &recFilterData)
{
	recFilterData.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_COMMGRIDFILTER_WIZARD_VIEW));
	recFilterData.addRow();
	recFilterData.setData(0, "VIEW_ID", m_nCurrentViewID);
	QByteArray byte = XmlUtil::ConvertRecordSet2ByteArray_Fast(m_recFilterViewData);
	recFilterData.setData(0, "VIEW_DATA_RECORDSET", byte);

}

void CommWorkBenchWidget::ContactChanged(int nEntityRecordID, bool bFromReload /*= false*/)
{
	m_nCurrentEntityID = nEntityRecordID;

	//_DUMP(m_recFilterViewData);

	//Get data.
	Status status;
	m_recEmail.clear();
	m_recVoiceCall.clear();
	m_recDocument.clear();

	//Check has this contact some stored view.
	if (!bFromReload)
	{
		int nViewID;
		_SERVER_CALL(BusCommunication->GetGridViewForContact(status, nViewID, m_nCurrentEntityID))
		_CHK_ERR(status);

		if (nViewID>0)
			ReloadViewSelector(nViewID, true);
		else
		{
			FillInsertDefaultValues(m_recFilterViewData);
			ReloadViewSelector(-1);
		}
	}

	_SERVER_CALL(BusCommunication->GetContactCommData(status, m_recEmail, m_recDocument, m_recVoiceCall, nEntityRecordID, m_recFilterViewData))
	_CHK_ERR(status);

	//CHECK FP FOR EMAIL.
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW))
		m_recEmail.clear();

	//Initialize grid.
	ui.CommGrid_widget->Initialize(&m_recEmail, &m_recDocument, &m_recVoiceCall, &m_lstSortRecordSet, m_nGridType);

	emit ContentChanged();
}

void CommWorkBenchWidget::ProjectChanged(int nEntityRecordID, bool bFromReload /*= false*/)
{
	m_nCurrentEntityID = nEntityRecordID;

	//_DUMP(m_recFilterViewData);

	//Get data.
	Status status;
	m_recEmail.clear();
	m_recVoiceCall.clear();
	m_recDocument.clear();

	//Check has this project some stored view.
	if (!bFromReload)
	{
		int nViewID;
		_SERVER_CALL(BusCommunication->GetGridViewForProject(status, nViewID, m_nCurrentEntityID))
			_CHK_ERR(status);

		if (nViewID>0)
			ReloadViewSelector(nViewID, true);
		else
		{
			FillInsertDefaultValues(m_recFilterViewData);
			ReloadViewSelector(-1);
		}
	}

	_SERVER_CALL(BusCommunication->GetProjectCommData(status, m_recEmail, m_recDocument, m_recVoiceCall, nEntityRecordID, m_recFilterViewData))
	_CHK_ERR(status);

	//CHECK FP FOR EMAIL.
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW))
		m_recEmail.clear();
	
	//Initialize grid.
	ui.CommGrid_widget->Initialize(&m_recEmail, &m_recDocument, &m_recVoiceCall, &m_lstSortRecordSet, m_nGridType);

	emit ContentChanged();
}

void CommWorkBenchWidget::SetPerson(int nPersonID)
{
	m_nCurrentEntityID = nPersonID;

	//Get grid data.
	m_recEmail.clear();
	m_recVoiceCall.clear();
	m_recDocument.clear();
	bool bRowsStripped=false;
	Status sts;
	if (nPersonID > 0)
	{
		QApplication::setOverrideCursor(Qt::WaitCursor);
		
		if(m_recFilterViewData.getRowCount())
		{
			_SERVER_CALL(BusCommunication->GetPersonCommData(sts, bRowsStripped, m_recEmail, m_recDocument, m_recVoiceCall, nPersonID, m_recFilterViewData))
			_CHK_ERR_NO_RET(sts);
			if (!sts.IsOK())
			{
				QApplication::restoreOverrideCursor();
				return;
			}
		}
		
		//CHECK FP FOR EMAIL.
		if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW))
			m_recEmail.clear();
		
		//Initialize grid.
		ui.CommGrid_widget->Initialize(&m_recEmail, &m_recDocument, &m_recVoiceCall, &m_lstSortRecordSet, m_nGridType, true, bRowsStripped);

		QApplication::restoreOverrideCursor();
	}
}

void CommWorkBenchWidget::SetCalendar(bool bTaskTypeFilterSet /*= false*/)
{
	Q_ASSERT(m_recEntities);
	
	//_DUMP(m_recEmail);

	m_recEmail.clear();
	m_recVoiceCall.clear();
	m_recDocument.clear();
	DbRecordSet recFilter;
	CommGridFilterSettings filterSettings;
	recFilter = filterSettings.GetCalendarGridTasksFilterSettings();
	//_DUMP(m_recEmail);

	QApplication::setOverrideCursor(Qt::WaitCursor);

	//QTime timer;
	//timer.start();

	Status err;
	if (bTaskTypeFilterSet)
		_SERVER_CALL(BusCommunication->GetCalendarGridCommData(err, m_recEmail, m_recDocument, m_recVoiceCall, *m_recEntities, m_recFilterViewData))
	else
		_SERVER_CALL(BusCommunication->GetCalendarGridCommData(err, m_recEmail, m_recDocument, m_recVoiceCall, *m_recEntities, recFilter))
	

	//qDebug() << "poslije GetCalendarGridCommData" << timer.elapsed();
	//_DUMP(m_recEmail);

	_CHK_ERR_NO_RET(err);
	if (!err.IsOK())
	{
		QApplication::restoreOverrideCursor();
		return;
	} 
	
	//CHECK FP FOR EMAIL.
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW))
		m_recEmail.clear();

	//_DUMP(m_recEmail);

	//qDebug() << "prije initialize" << timer.elapsed();
	//Initialize grid.
	ui.CommGrid_widget->Initialize(&m_recEmail, &m_recDocument, &m_recVoiceCall, &m_lstSortRecordSet, m_nGridType);

	//qDebug() << "poslije initialize" << timer.elapsed();
	QApplication::restoreOverrideCursor();
}

void CommWorkBenchWidget::ReloadData(ReloadParameter reloadParameter /*= DEFAULT_RELOAD*/)
{
	ClearHeader();

	if (m_nGridType == CommGridViewHelper::DESKTOP_GRID)
		SetPerson(m_nCurrentEntityID);
	else if (m_nGridType == CommGridViewHelper::CONTACT_GRID)
		ContactChanged(m_nCurrentEntityID, true);
	else if (m_nGridType == CommGridViewHelper::PROJECT_GRID)
		ProjectChanged(m_nCurrentEntityID, true);
	else if (m_nGridType == CommGridViewHelper::CALENDAR_GRID)
	{
		if (reloadParameter == CALENDAR_TASK_TYPE_RELOAD)
			SetCalendar(true);
		else
			SetCalendar();
	}
	
	emit ContentChanged();
}

void CommWorkBenchWidget::ReloadViewWithoutSeverCall(bool bClearSelection /*= true*/)
{
	ClearHeader();

	//_DUMP(m_recEmail);
	//_DUMP(m_recVoiceCall);
	//_DUMP(m_recDocument);

	//Initialize grid.
	ui.CommGrid_widget->Initialize(&m_recEmail, &m_recDocument, &m_recVoiceCall, &m_lstSortRecordSet, m_nGridType, bClearSelection);
}

void CommWorkBenchWidget::RefreshContactItem(int nCEType, DbRecordSet &recEntityRecord)
{
	ui.CommGrid_widget->RefreshContactItem(nCEType, recEntityRecord);
}

DbRecordSet* CommWorkBenchWidget::GetEmailRecordSet()
{
	return &m_recEmail;
}

DbRecordSet* CommWorkBenchWidget::GetVoiceCallRecordSet()
{
	return &m_recVoiceCall;
}

DbRecordSet* CommWorkBenchWidget::GetDocumentsRecordSet()
{
	return &m_recDocument;
}

void CommWorkBenchWidget::ClearCommGrid()
{
	m_nCurrentEntityID = -1;

	//Clear grid data.
	m_recEmail.clear();
	m_recVoiceCall.clear();
	m_recDocument.clear();
	
	//Initialize grid with empty recordsets.
	ui.CommGrid_widget->Initialize(&m_recEmail, &m_recDocument, &m_recVoiceCall, &m_lstSortRecordSet, m_nGridType);

	//Clear selection in grid header.
	//ui.CommGridHeader_widget->DisableHeader();

	emit ContentChanged();
}


/*! Catches global cache events: when document or mail or voice call is modified, or deleted->reload grid!!!

	\param pSubject			- source of msg
	\param nMsgCode			- msg code
	\param val				- value sent from observer
*/
void CommWorkBenchWidget::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (!m_bInitialized) //in sidebar mode when grid is dormant, ignore all observer signals
		return;

	//selectors:
	if (pSubject == ui.User_frame && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnUserSAPNEChanged();
		return;
	}

	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}

	if (pSubject == ui.Owner_frame && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnOwnerSAPNEChanged();
		return;
	}
	if (pSubject == ui.Contact_frame && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnContactSAPNEChanged();
		return;
	}
	if (pSubject == ui.Project_frame && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnProjectSAPNEChanged();
		return;
	}
	if (pSubject == ui.User_frame && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnUserSAPNEChanged();
		return;
	}


	if(nMsgDetail==ENTITY_BUS_DM_DOCUMENTS)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			UpdateRecordsetsWithChangedData(nMsgCode, nMsgDetail, val);
			return;
		}

	if(nMsgDetail==ENTITY_BUS_EMAILS)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			UpdateRecordsetsWithChangedData(nMsgCode, nMsgDetail, val);
			return;
		}

	if(nMsgDetail==ENTITY_BUS_VOICECALLS)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			UpdateRecordsetsWithChangedData(nMsgCode, nMsgDetail, val);
			return;
		}

	//make sensitive to check in/out:
	if (nMsgCode==ChangeManager::GLOBAL_CHECK_OUT_DOC ||nMsgCode==ChangeManager::GLOBAL_CHECK_IN_DOC)
	{
		DbRecordSet temp;
		temp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS));
		temp.addRow();
		temp.setData(0,"BDMD_ID",nMsgDetail);
		QVariant varData;
		qVariantSetValue(varData,temp);
		UpdateRecordsetsWithChangedData(ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED, ENTITY_BUS_DM_DOCUMENTS, varData);
		return;
	}
}

void CommWorkBenchWidget::on_viewSelect_toolButton_clicked()
{
	int nFuiID;
	if (m_nGridType == CommGridViewHelper::DESKTOP_GRID)
		nFuiID = COMM_GRID_FILTER_WIDGET_DESKTOP;
	else if (m_nGridType == CommGridViewHelper::CONTACT_GRID)
		nFuiID = COMM_GRID_FILTER_WIDGET_CONTACT;
	else if (m_nGridType == CommGridViewHelper::PROJECT_GRID)
		nFuiID = COMM_GRID_FILTER_WIDGET_PROJECT;
	else if (m_nGridType == CommGridViewHelper::CALENDAR_GRID)
		nFuiID = COMM_GRID_FILTER_WIDGET_CALENDAR;

	//So far only for desktop
	int nGID = g_objFuiManager.OpenFUI(nFuiID, true, false);

	int nCurrentViewID = QVariant(ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex())).toInt();
	dynamic_cast<CommGridFilter*>(g_objFuiManager.GetFUIWidget(nGID))->SetCurrentView(nCurrentViewID, m_recFilterViewData, m_nLoggedPersonID);
	connect(dynamic_cast<CommGridFilter*>(g_objFuiManager.GetFUIWidget(nGID)), SIGNAL(SetFilter(int, DbRecordSet, DbRecordSet)), this, SLOT(SetFilter(int, DbRecordSet, DbRecordSet)));
}

void CommWorkBenchWidget::on_viewSelect_comboBox_currentIndexChanged(int index)
{
	m_nCurrentViewID = ui.viewSelect_comboBox->itemData(index).toInt();
	if (!ReloadViewSelector(m_nCurrentViewID))
		return;	
	SetupDateRangeSelector();
	ReloadData();

	emit FilterChanged(m_strCurrentPersonInitials + ": " + ui.viewSelect_comboBox->currentText());
}

void CommWorkBenchWidget::on_refresh_pushButton_clicked()
{
	ReloadData();
}

void CommWorkBenchWidget::on_refresh2_toolButton_clicked()
{
	ReloadData();
}

void CommWorkBenchWidget::on_saveView_toolButton_clicked()
{
	if (m_nCurrentEntityID <= 0 || m_nCurrentViewID <= 0)
		return;

	QString strSQL;
		
	if (m_nGridType == CommGridViewHelper::CONTACT_GRID)
	{
		strSQL=QString("UPDATE BUS_CM_CONTACT SET BCNT_DEFAULT_GRID_VIEW_ID = %1 WHERE BCNT_ID = %2").arg(m_nCurrentViewID).arg(m_nCurrentEntityID);
	}
	else if (m_nGridType == CommGridViewHelper::PROJECT_GRID)
	{
		strSQL=QString("UPDATE BUS_PROJECTS SET BUSP_DEFAULT_GRID_VIEW_ID = %1 WHERE BUSP_ID = %2").arg(m_nCurrentViewID).arg(m_nCurrentEntityID);
	}
	else
		return;
		

	Status err;
	DbRecordSet lstRet;
	_SERVER_CALL(ClientSimpleORM->ExecuteSQL(err,strSQL,lstRet));
	_CHK_ERR(err);

}

void CommWorkBenchWidget::OnThemeChanged()
{
	ui.CommGrid_widget->setStyleSheet(ThemeManager::GetTableViewBkg());
}

//when new data comes, select it on CE grid
void CommWorkBenchWidget::SetSelectionOnNewItems(int nCE_Type, DbRecordSet &lstNewData, bool bFromCalendar /*= false*/)
{
	DbRecordSet *lstEntity=NULL;

	DbRecordSet *pLstDataEmails=GetEmailRecordSet();
	DbRecordSet *pLstDataVoiceCalls=GetVoiceCallRecordSet();
	DbRecordSet *pLstDataDocs=GetDocumentsRecordSet();

	QString strIDName;
	switch(nCE_Type)
	{
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		pLstDataVoiceCalls->clearSelection();
		lstEntity=GetVoiceCallRecordSet();
		strIDName="BVC_ID";
		break;
	case GlobalConstants::CE_TYPE_EMAIL:
		pLstDataEmails->clearSelection();
		lstEntity=GetEmailRecordSet();
		strIDName="BEM_ID";
	    break;
	default:
		pLstDataDocs->clearSelection();
		lstEntity=GetDocumentsRecordSet();
		strIDName="BDMD_ID";
	    break;
	}

	if (bFromCalendar)
	{
		pLstDataVoiceCalls->clearSelection();
		pLstDataEmails->clearSelection();
		pLstDataDocs->clearSelection();
	}

	Q_ASSERT(lstEntity);

	//------------------SET SELECTION ON NEW ITEMS-----------------------

	//loop by ID:
	int nIDIdxNew=lstNewData.getColumnIdx(strIDName);
	int nIDIdxGrid=lstEntity->getColumnIdx(strIDName);

//	lstEntity->Dump();

	if (nIDIdxGrid!=-1 && nIDIdxNew!=-1)
	{
		int nSize=lstNewData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			int nRow=lstEntity->find(nIDIdxGrid,lstNewData.getDataRef(i,nIDIdxNew),true);
			if (nRow!=-1)
			{
				lstEntity->selectRow(nRow);
			}
		}

		if (lstEntity->getSelectedCount())
			ui.CommGrid_widget->SelectGridFromSourceRecordSet();
	}

}

void CommWorkBenchWidget::GetGridData(int& nGridType,int &nEntityRecordID)
{
	nGridType=m_nGridType;
	nEntityRecordID=m_nCurrentEntityID;
}

void CommWorkBenchWidget::HideUserAndButtons(bool bHide /*= true*/)
{
	ui.User_frame->setVisible(!bHide);
	ui.User_label->setVisible(!bHide);
	ui.buttons_frame->setVisible(!bHide);
}

void CommWorkBenchWidget::OnSelectionChanged(int nOwnerID, int nContactID, int nProjectID, int nEntitySysType, int nTaskID, QList<int> lstTaskList, 
	int nEntityID, int nPolymorficID, int nBDMD_IS_CHECK_OUT, int nBDMD_CHECK_OUT_USER_ID, int nBTKS_SYSTEM_STATUS, QList<int> lstUnreadEmailsBEM_IDs, 
	QList<int> lstStartTaskEmailsCENT_IDs, QList<int> lstStartTaskPhoneCallsCENT_IDs)
{
	//Check is anything selected - if not clear header.
	if (nEntityID < 0)
	{
		ClearHeader();
		return;
	}

	//store all selection data:
	m_nTaskID = nTaskID;
	m_nOwnerID=nOwnerID;
	m_nContactID=nContactID; 
	m_nProjectID=nProjectID;
	m_nEntityID = nEntityID;
	m_nEntitySysType=nEntitySysType;
	m_nPolymorficID=nPolymorficID;
	m_lstTaskList.clear();
	m_lstUnreadEmailsList.clear();
	m_lstTaskList=lstTaskList;
	m_lstUnreadEmailsList = lstUnreadEmailsBEM_IDs;
	m_lstStartTaskEmailsCENT_IDs = lstStartTaskEmailsCENT_IDs;
	m_lstStartTaskPhoneCallsCENT_IDs = lstStartTaskPhoneCallsCENT_IDs;

	bool bStartTask = false;
	if ((lstStartTaskEmailsCENT_IDs.count() + lstStartTaskPhoneCallsCENT_IDs.count()) > 0)
		bStartTask = true;
	
	//Enable select, modify, delete.
	if (nEntityID > 0)
	{
		ui.select_toolButton->setDisabled(false);
		ui.clear_toolButton->setDisabled(false);
		ui.modify_toolButton->setDisabled(false);
		ui.Owner_frame->setDisabled(false);
		ui.Project_frame->setDisabled(false);
		ui.Contact_frame->setDisabled(false);
	}
	else
	{
		ui.select_toolButton->setDisabled(true);
		ui.clear_toolButton->setDisabled(true);
		ui.modify_toolButton->setDisabled(true);
		ui.Owner_frame->setDisabled(true);
		ui.Project_frame->setDisabled(true);
		ui.Contact_frame->setDisabled(true);
	}

	//,true is for block signals.
	ui.Owner_frame->SetCurrentEntityRecord(nOwnerID, true);
	ui.Contact_frame->SetCurrentEntityRecord(nContactID, true);
	ui.Project_frame->SetCurrentEntityRecord(nProjectID, true);


	ui.pieMenu_toolButton->SetEntityRecordID(nContactID);

	
	switch(nEntitySysType)
	{
	case(GlobalConstants::CE_TYPE_DOCUMENT):
		{
			ui.openDoc_toolButton->setEnabled(true);
			ui.PackAndSend_toolButton->setEnabled(true);

			ui.Reply_toolButton->setEnabled(false);
			ui.Forward_toolButton->setEnabled(false);
			
			//If document checked out then enable check in.
			if(nBDMD_IS_CHECK_OUT == 1)
			{
				if(nBDMD_CHECK_OUT_USER_ID == m_nLoggedPersonID)
					ui.checkIn_toolButton->setEnabled(true);
				else
					ui.checkIn_toolButton->setEnabled(false);
			}
			else
				ui.checkIn_toolButton->setEnabled(false);
			
			//If task exists and not completed.
			if (nTaskID)
			{
				//ui.startTask_toolButton->setEnabled(true);
				ui.PostponeTask_toolButton->setEnabled(true);
				if (nBTKS_SYSTEM_STATUS < 3)
					ui.taskDone_toolButton->setDisabled(false);
				else
					ui.taskDone_toolButton->setDisabled(true);
			}
			else
			{
				//ui.startTask_toolButton->setEnabled(false);
				ui.PostponeTask_toolButton->setEnabled(false);
				ui.taskDone_toolButton->setDisabled(true);
			}
		}
		break;
	case(GlobalConstants::CE_TYPE_VOICE_CALL):
		{
			ui.openDoc_toolButton->setEnabled(true);
			ui.checkIn_toolButton->setEnabled(false);
			ui.Reply_toolButton->setEnabled(false);
			ui.Forward_toolButton->setEnabled(false);
			ui.PackAndSend_toolButton->setEnabled(false);
			
			//Start task logic.
			if ((nTaskID && nBTKS_SYSTEM_STATUS < 3) || bStartTask)
			{
				//ui.startTask_toolButton->setEnabled(true);
				ui.PostponeTask_toolButton->setEnabled(true);
			}
			else
			{
				//ui.startTask_toolButton->setEnabled(false);
				ui.PostponeTask_toolButton->setEnabled(false);
			}
			
			//If task exists.
			if (nTaskID && nBTKS_SYSTEM_STATUS < 3)
			{
				if (nBTKS_SYSTEM_STATUS < 3)
					ui.taskDone_toolButton->setDisabled(false);
				else
					ui.taskDone_toolButton->setDisabled(true);
			}
			else
				ui.taskDone_toolButton->setDisabled(true);

		}
		break;
	case(GlobalConstants::CE_TYPE_EMAIL):
		{
			ui.openDoc_toolButton->setEnabled(true);
			ui.checkIn_toolButton->setEnabled(false);
			ui.Reply_toolButton->setEnabled(true);
			ui.Forward_toolButton->setEnabled(true);
			ui.PackAndSend_toolButton->setEnabled(false);
			ui.taskDone_toolButton->setDisabled(true);

			//Start task logic.
			if ((nTaskID && nBTKS_SYSTEM_STATUS < 3) || bStartTask)
			{
				//ui.startTask_toolButton->setEnabled(true);
				ui.PostponeTask_toolButton->setEnabled(true);
			}
			else
			{
				//ui.startTask_toolButton->setEnabled(false);
				ui.PostponeTask_toolButton->setEnabled(false);
			}
			
			//If task exists.
			if (nTaskID && nBTKS_SYSTEM_STATUS < 3)
			{
				if (nBTKS_SYSTEM_STATUS < 3)
					ui.taskDone_toolButton->setDisabled(false);
				else
					ui.taskDone_toolButton->setDisabled(true);
			}
			else
				ui.taskDone_toolButton->setDisabled(true);
		}
		break;
	default:
		Q_ASSERT(false);
		break;
	}

	//If selected task list has multiple selections allow task done button.
	if (lstTaskList.count() > 0 || lstUnreadEmailsBEM_IDs.count() > 0)
		ui.taskDone_toolButton->setDisabled(false);
	
	//CHECK FP.
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW))
	{
		ui.AddMail_toolButton->setEnabled(false);
		ui.PackAndSend_toolButton->setEnabled(false);
		ui.Reply_toolButton->setEnabled(false);
		ui.Forward_toolButton->setEnabled(false);
	}
}

void CommWorkBenchWidget::on_openDoc_toolButton_clicked()
{
	//issue #1688
	bool bShift = false;
	Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();
	if(Qt::ShiftModifier == (Qt::ShiftModifier & keys))
		bShift = true;

	DbRecordSet *pLstData=GetVoiceCallRecordSet();
	int nRow=pLstData->getSelectedRow();
	if (nRow!=-1)
	{
		int nCount=pLstData->getSelectedCount();
		if (nCount>1)
		{
			int nResult=QMessageBox::question(this,tr("Warning"),QVariant(nCount).toString()+tr(" items selected. Do you want to open them all?"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
		}

		DbRecordSet lstData=pLstData->getSelectedRecordSet();
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			//open one by one:
			int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false,FuiBase::MODE_EMPTY,-1,true);
			FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
			if(NULL == pFUI) return;	//someone closed the FUI for this call
			pFUI->LoadCallData(lstData.getDataRef(i,"BVC_ID").toInt());
			pFUI->show();

			//for tasks, show as "Task Card"
			if(bShift && m_nTaskID > 0){
				pFUI->showMinimized();
			}
		}
	}


	//open doc in app:
	pLstData=GetDocumentsRecordSet();

	nRow=pLstData->getSelectedRow();
	if (nRow!=-1)
	{
		int nCount=pLstData->getSelectedCount();
		if (nCount>1)
		{
			int nResult=QMessageBox::question(this,tr("Warning"),QVariant(nCount).toString()+tr(" items selected. Do you want to open them all?"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
		}

		DbRecordSet lstData=pLstData->getSelectedRecordSet();
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			DbRecordSet rowDoc=lstData.getRow(i);

			//for tasks, show as "Task Card"
			if(bShift && m_nTaskID > 0){
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_READ, rowDoc.getDataRef(0,"BDMD_ID").toInt(),true);
				QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_DM_Documents* pFUI=dynamic_cast<FUI_DM_Documents*>(pFUIWidget);
				if (pFUI)
				{
					pFUI->show();  //show FUI
					pFUI->showMinimized();
				}
			}
			else
				DocumentHelper::OpenDocumentInExternalApp(rowDoc);
		}
	}


	//open email in app:
	pLstData=GetEmailRecordSet();

	nRow=pLstData->getSelectedRow();
	if (nRow!=-1)
	{
		int nCount=pLstData->getSelectedCount();
		if (nCount>1)
		{
			int nResult=QMessageBox::question(this,tr("Warning"),QVariant(nCount).toString()+tr(" items selected. Do you want to open them all?"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
		}

		

		bool bReloadGrid=false;
		DbRecordSet lstData=pLstData->getSelectedRecordSet();
		int nSize=lstData.getRowCount();
		QList<int> lstSelectedCENT_IDs;
		//_DUMP(lstData);
		for(int i=0;i<nSize;++i)
		{
			int nCENT_ID = lstData.getDataRef(0,"CENT_ID").toInt();
			lstSelectedCENT_IDs << nCENT_ID;

			//for tasks, show as "Task Card"
			if(bShift && m_nTaskID > 0){
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_READ, lstData.getDataRef(0,"BEM_ID").toInt(),true);
				QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
				EMailDialog* pFUI=dynamic_cast<EMailDialog*>(pFUIWidget);
				if (pFUI)
				{
					pFUI->show();  //show FUI
					pFUI->showMinimized();
				}
			}
			else
			{
				QByteArray byteExternID=lstData.getDataRef(i,"BEM_EXT_ID").toByteArray();
				QString strExtApp=lstData.getDataRef(i,"BEM_EXT_APP").toString();
				int nEmailID=lstData.getDataRef(i,"BEM_ID").toInt();
				g_CommManager.OpenMailInExternalClient(nEmailID,byteExternID,strExtApp);
				
				//mark as READ (issue: 540):
				//unread flag is set, then set:
				if (lstData.getDataRef(i,"BEM_UNREAD_FLAG").toInt()>0)
				{
					Status err;
					_SERVER_CALL(BusEmail->SetEmailRead(err, nCENT_ID))
					if (err.IsOK())
					{
						int nRow = pLstData->find("CENT_ID", nCENT_ID, true);
						pLstData->setData(nRow, "BEM_UNREAD_FLAG", 0);

						//UpdateRecordsetsWithChangedData(ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED, ENTITY_BUS_EMAILS, pLstData);

						ui.CommGrid_widget->Initialize(GetEmailRecordSet(), GetDocumentsRecordSet(), GetVoiceCallRecordSet(), &m_lstSortRecordSet, m_nGridType);
					}
				}
			}
		}
		
		//Select previous selected rows.
		ui.CommGrid_widget->SelectBySourceRecordSetRows(lstSelectedCENT_IDs);
	}

}

void CommWorkBenchWidget::on_startTask_toolButton_clicked()
{
	//Issue #1456.
	bool bForceSilentCreate = false;
	bool bStartTask = false;
	bool bNothingSelected=false;
	if ((m_lstStartTaskEmailsCENT_IDs.count() + m_lstStartTaskPhoneCallsCENT_IDs.count()) > 0)
		bStartTask = true;
	//If there is one email and one phone call task then force silent.
	if (((m_lstStartTaskEmailsCENT_IDs.count() + m_lstStartTaskPhoneCallsCENT_IDs.count()) == 2) && (m_lstStartTaskEmailsCENT_IDs.count() == 1))
		bForceSilentCreate = true;
	
	//issue 2077: if nothing selected (ignore documents):
	if (GetEmailRecordSet()->getSelectedCount()==0 && GetVoiceCallRecordSet()->getSelectedCount()==0)
		bNothingSelected = true;

	//Issue #1456.
	if (bStartTask)
	{
		int nEmailCount = m_lstStartTaskEmailsCENT_IDs.count();
		int nVoiceCallCount = m_lstStartTaskPhoneCallsCENT_IDs.count();

		//Emails.
		if (nEmailCount > 0)
		{
			DbRecordSet recEmailsStartTask;
			recEmailsStartTask.copyDefinition(m_recEmail);
			for (int i = 0; i < nEmailCount; i++)
			{
				recEmailsStartTask.addRow();
				int nRow = m_recEmail.find("CENT_ID", m_lstStartTaskEmailsCENT_IDs.value(i), true);
				if (nRow >= 0){
					DbRecordSet row = m_recEmail.getRow(nRow);
					recEmailsStartTask.assignRow(recEmailsStartTask.getRowCount()-1, row);
				}
			}
			
			CommunicationActions::createEmailQuickTask(recEmailsStartTask, &ui.startTask_toolButton->mapToGlobal(QPoint(0,0)), bForceSilentCreate);
		}
		//Voice Calls.
		if (nVoiceCallCount > 0)
		{
			DbRecordSet recVoiceCallsStartTask;
			recVoiceCallsStartTask.copyDefinition(m_recVoiceCall);
			for (int i = 0; i < nVoiceCallCount; i++)
			{
				recVoiceCallsStartTask.addRow();
				int nRow = m_recVoiceCall.find("CENT_ID", m_lstStartTaskPhoneCallsCENT_IDs.value(i), true);
				if (nRow >= 0){
					DbRecordSet row = m_recVoiceCall.getRow(nRow);
					recVoiceCallsStartTask.assignRow(recVoiceCallsStartTask.getRowCount()-1, row);
				}
			}

			CommunicationActions::createPhoneQuickTask(recVoiceCallsStartTask, &ui.startTask_toolButton->mapToGlobal(QPoint(0,0)), bForceSilentCreate);
		}

		return;
	}

	//issue 2077:
	if (bNothingSelected)
	{
		//if ce grid is part of contact->set contact, and if part of project grid, set project id:
		DbRecordSet lstProjectAssigned;
		DbRecordSet lstContactAssigned;
		lstProjectAssigned.addColumn(QVariant::Int,"BUSP_ID");
		lstContactAssigned.addColumn(QVariant::Int,"BCNT_ID");
		if(m_nGridType == CommGridViewHelper::PROJECT_GRID && m_nCurrentEntityID>0)
		{
			lstProjectAssigned.addRow();
			lstProjectAssigned.setData(0,0,m_nCurrentEntityID);
		}
		else if (m_nGridType == CommGridViewHelper::CONTACT_GRID && m_nCurrentEntityID>0)
		{
			lstContactAssigned.addRow();
			lstContactAssigned.setData(0,0,m_nCurrentEntityID);
		}
		
		//create new task as note document:
		int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EMPTY,-1,true);
		QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
		FUI_DM_Documents* pFUI=dynamic_cast<FUI_DM_Documents*>(pFUIWidget);
		if (pFUI)
		{
			pFUI->SetDefaults(GlobalConstants::DOC_TYPE_NOTE,NULL,&lstContactAssigned,&lstProjectAssigned,true);
		}
		if(pFUI)pFUI->show();  //show FUI
		return;
	}

	//Issue #1456.
	if (!bStartTask)
	{
		switch(m_nEntitySysType)
		{
		case GlobalConstants::CE_TYPE_VOICE_CALL:
			{
				//voice calls
				int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false,FuiBase::MODE_EMPTY,-1,true);
				FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
				if(NULL == pFUI) return;	//someone closed the FUI for this call
				pFUI->LoadCallData(m_nPolymorficID);
				pFUI->show();
			}
			break;

		case GlobalConstants::CE_TYPE_DOCUMENT:
			{
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EMPTY,-1,true);
				QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_DM_Documents* pFUI=dynamic_cast<FUI_DM_Documents*>(pFUIWidget);
				if (pFUI)
				{
					pFUI->Execute(m_nPolymorficID);
				}
				if(pFUI)pFUI->show();  //show FUI
			}
			break;

		case GlobalConstants::CE_TYPE_EMAIL:
			{
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_EMPTY,-1,true);
				QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
				EMailDialog* pFUI=dynamic_cast<EMailDialog*>(pFUIWidget);
				if (pFUI)
				{
					pFUI->Execute(m_nPolymorficID);
				}
				if(pFUI)pFUI->show();  //show FUI
			}
			break;

		default:
			Q_ASSERT(false);
			break;
		}
	}
}

void CommWorkBenchWidget::on_PostponeTask_toolButton_clicked()
{
	QDateTime datDueDate, datStartDate;
	ui.CommGrid_widget->GetMinTaskDueAndStartTime(m_nTaskID, m_lstTaskList, datDueDate, datStartDate);
	PostponeTaskDialog dlg(datStartDate, datDueDate);
	connect(&dlg, SIGNAL(returnDates(QDateTime, QDateTime)), this, SLOT(PostPoneTasks(QDateTime, QDateTime)));
	dlg.exec();
}

void CommWorkBenchWidget::on_taskDone_toolButton_clicked()
{
	bool bReload = true;
	int nSize=m_lstTaskList.size();
	int nUnreadSize=m_lstUnreadEmailsList.size();

	QString strDialogWarning = tr("Do you really want to mark ");
	if (nSize > 0)
		strDialogWarning += QVariant(nSize).toString() + tr(" task(s) as done");
	if (nUnreadSize > 0)
	{
		if (nSize > 0)
			strDialogWarning += tr(" and ");

		strDialogWarning += QVariant(nUnreadSize).toString() + tr(" email(s) as read");
	}
	strDialogWarning += tr("?");

	int nResult=QMessageBox::question(this, tr("Warning"), strDialogWarning, tr("Yes"), tr("No"));
	if (nResult!=0) return; //only if YES

	DbRecordSet lstTasks;
	lstTasks.addColumn(QVariant::Int,"BTKS_ID");
	for(int i=0;i<nSize;++i)
	{
		lstTasks.addRow();
		lstTasks.setData(lstTasks.getRowCount()-1,0,m_lstTaskList.at(i));
	}
	if (lstTasks.getRowCount()>0)
	{
		//[#927] Note: Re: Time in "Task Done" Phone Call Tasks set to 00:00
		//When a phone call task is marked as "Done" AND the start time is 00:00, 
		//then set the date and start time to the actual date time. The end time remains empty.
		//--> it is done on server.
		g_CommManager.SetTaskDone(lstTasks);
	}

	//Mark unread emails read.
	DbRecordSet lstEmails;
	lstEmails.addColumn(QVariant::Int, "BEM_ID");
	for(int i = 0; i < nUnreadSize; ++i)
	{
		int nBEM_ID = m_lstUnreadEmailsList.at(i);
		lstEmails.addRow();
		lstEmails.setData(lstEmails.getRowCount()-1, 0, nBEM_ID);

		//Mark email read in source recordset (in case there is no need to reload data - just mark emails read).
		DbRecordSet *plstEmails = GetEmailRecordSet();
		int nRow = plstEmails->find("BEM_ID", nBEM_ID, true);

		if (nRow>=0)
			plstEmails->setData(nRow, "BEM_UNREAD_FLAG", 0);
	}

	Status status;
	_SERVER_CALL(BusCommunication->SetEmailsRead(status, lstEmails))
	_CHK_ERR_NO_RET(status);
	//Check are there any tasks selected or you just want to mark emails read. If just emails then don't reload (issue #817).
	if (status.IsOK() && lstEmails.getRowCount())
	{
		if (!m_lstTaskList.size())
			bReload = false;
	}

	//------------------------------------------------------
	//Send signal for all to reload:
	//------------------------------------------------------
	//refresh grid:
	if (bReload)
	{
		ReloadData();
		//QVariant varData;
		//qVariantSetValue(varData,temp);
		//g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED,ENTITY_BUS_EMAILS,); 
	}
	else
	{
		ui.CommGrid_widget->Initialize(GetEmailRecordSet(), GetDocumentsRecordSet(), GetVoiceCallRecordSet(), &m_lstSortRecordSet, m_nGridType);
		DisableHeader();
	}
}


void CommWorkBenchWidget::on_select_toolButton_clicked()
{
	//issue #1688
	bool bShift = false;
	Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();
	if(Qt::ShiftModifier == (Qt::ShiftModifier & keys))
		bShift = true;

	switch(m_nEntitySysType)
	{
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		{
			//voice calls
			int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false,FuiBase::MODE_EMPTY,-1,true);
			FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
			if(NULL == pFUI) return;	//someone closed the FUI for this call
			pFUI->LoadCallData(m_nPolymorficID);
			pFUI->show();
			
			//for tasks, show as "Task Card"
			if(bShift && m_nTaskID > 0){
				pFUI->showMinimized();
			}
		}
		break;

	case GlobalConstants::CE_TYPE_DOCUMENT:
		{
			int nFUI = g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_READ, m_nPolymorficID);
			FUI_DM_Documents* pFUI = dynamic_cast<FUI_DM_Documents *>(g_objFuiManager.GetFUIWidget(nFUI));
			//for tasks, show as "Task Card"
			if(bShift && m_nTaskID > 0){
				pFUI->showMinimized();
			}
		}
		break;
	case GlobalConstants::CE_TYPE_EMAIL:
		{
			int nFUI = g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_READ, m_nPolymorficID);
			EMailDialog* pFUI = dynamic_cast<EMailDialog *>(g_objFuiManager.GetFUIWidget(nFUI));
			//for tasks, show as "Task Card"
			if(bShift && m_nTaskID > 0){
				pFUI->showMinimized();
			}

			//Select previously selected record.
			QList<int> lstSelectedCENT_ID;
			lstSelectedCENT_ID << m_nEntityID;
			
			//Set email unread - change it in the view (it will not be reloaded from fui above - issue #817).
			DbRecordSet *pLstData = GetEmailRecordSet();
			int nRow = pLstData->find("CENT_ID", m_nEntityID, true);
			pLstData->setData(nRow, "BEM_UNREAD_FLAG", 0);
			ui.CommGrid_widget->Initialize(GetEmailRecordSet(), GetDocumentsRecordSet(), GetVoiceCallRecordSet(), &m_lstSortRecordSet, m_nGridType);
			//DisableHeader();

			//Select previously selected record.
			ui.CommGrid_widget->SelectBySourceRecordSetRows(lstSelectedCENT_ID);
		}
		break;
	default:
		Q_ASSERT(false);
		break;
	}

}

void CommWorkBenchWidget::on_modify_toolButton_clicked()
{
	switch(m_nEntitySysType)
	{
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		{
			//QMessageBox::warning(this,"Not Implemented","Not Implemented");

			//voice calls
			int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false,FuiBase::MODE_EMPTY,-1,true);
			FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
			if(NULL == pFUI) return;	//someone closed the FUI for this call
			pFUI->LoadCallData(m_nPolymorficID);
			pFUI->show();
		}
		break;

	case GlobalConstants::CE_TYPE_DOCUMENT:
		{
			g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EDIT, m_nPolymorficID);
		}
		break;
	case GlobalConstants::CE_TYPE_EMAIL:
		{
			g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_EDIT, m_nPolymorficID);
			
			//Set email unread - change it in the view (it will not be reloaded from fui above - issue #817).
			DbRecordSet *pLstData = GetEmailRecordSet();
			int nRow = pLstData->find("CENT_ID", m_nEntityID, true);
			pLstData->setData(nRow, "BEM_UNREAD_FLAG", 0);
			ui.CommGrid_widget->Initialize(GetEmailRecordSet(), GetDocumentsRecordSet(), GetVoiceCallRecordSet(), &m_lstSortRecordSet, m_nGridType);

			//DisableHeader();
		}
		break;
	default:
		Q_ASSERT(false);
		break;
	}

}
void CommWorkBenchWidget::on_checkIn_toolButton_clicked()
{
	DbRecordSet lstDataDocs=GetDocumentsRecordSet()->getSelectedRecordSet();

	if(lstDataDocs.getRowCount()>0)
	{
		DocumentHelper::CheckInIfPossible(lstDataDocs);
	}
	else
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Please select documents for check in!"));
	}

}
void CommWorkBenchWidget::on_Reply_toolButton_clicked()
{
	DbRecordSet lstData=GetEmailRecordSet()->getSelectedRecordSet();

	if(lstData.getRowCount()>0)
	{
		QPoint piePos=ui.Reply_toolButton->mapToGlobal(QPoint(10,10));
		g_CommManager.EmailReply(lstData,&piePos); //test:
		//CommunicationActions::createEmailQuickTask(lstData,&piePos);
	}
	else
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Please select emails first!"));
	}

}
void CommWorkBenchWidget::on_Forward_toolButton_clicked()
{
	DbRecordSet lstData=GetEmailRecordSet()->getSelectedRecordSet();

	if(lstData.getRowCount()>0)
	{
		QPoint piePos=ui.Forward_toolButton->mapToGlobal(QPoint(10,10));
		g_CommManager.EmailForward(lstData,&piePos);
	}
	else
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Please select emails first!"));
	}
}


void CommWorkBenchWidget::on_PackAndSend_toolButton_clicked()
{
	DbRecordSet lstDataDocs=GetDocumentsRecordSet()->getSelectedRecordSet();

	if(lstDataDocs.getRowCount()>0)
	{
		QPoint piePos=ui.PackAndSend_toolButton->mapToGlobal(QPoint(10,10));
		
		int nGridType,nRecID,nProjID=-1,nContID=-1;
		GetGridData(nGridType,nRecID);
		
		//nContID=m_nContactID;
		nContID=m_nCurrentEntityID; //issue 2310
		nProjID=m_nProjectID;
		if (m_nContactID==1 && nGridType==CommGridViewHelper::CONTACT_GRID)
			nContID=nRecID;
		if (m_nContactID==1 && nGridType==CommGridViewHelper::PROJECT_GRID)
			nProjID=nRecID;

		g_CommManager.PackAndSendDocuments(lstDataDocs,&piePos,nProjID,nContID); //send current id from selected row if none then use current grid
	}
	else
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Please select documents to be attached to a new email before pressing Pack&Send!"));
	}

}

void CommWorkBenchWidget::on_clear_toolButton_clicked()
{

	//get all lists:
	DbRecordSet *pLstDataEmails=GetEmailRecordSet();
	DbRecordSet *pLstDataVoiceCalls=GetVoiceCallRecordSet();
	DbRecordSet *pLstDataDocs=GetDocumentsRecordSet();

	int nSelectedEmail=pLstDataEmails->getSelectedCount();
	int nSelectedVC=pLstDataVoiceCalls->getSelectedCount();
	int nSelectedDoc=pLstDataDocs->getSelectedCount();

	int nSelected=nSelectedDoc+nSelectedEmail+nSelectedVC;


	if (nSelected==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an item!"));
		return;
	}

	//ask dummy:
	int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete ")+QVariant(nSelected).toString()+tr(" item(s) permanently from the database?"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES

	//make deletion list:
	DbRecordSet lstForDelete, lstEmailsForDelete, lstVoiceCallsForDelete, lstDocumentsForDelete;
	lstForDelete.addColumn(QVariant::Int,"CENT_ID");
	lstEmailsForDelete.addColumn(QVariant::Int,"BEM_ID");
	lstVoiceCallsForDelete.addColumn(QVariant::Int,"BVC_ID");
	lstDocumentsForDelete.addColumn(QVariant::Int,"BDMD_ID");
	
	lstForDelete.merge(*pLstDataEmails,true);
	lstForDelete.merge(*pLstDataVoiceCalls,true);
	lstForDelete.merge(*pLstDataDocs,true);
	
	lstEmailsForDelete.merge(*pLstDataEmails,true);
	lstVoiceCallsForDelete.merge(*pLstDataVoiceCalls,true);
	lstDocumentsForDelete.merge(*pLstDataDocs,true);

	//delete & lock, return if err
	Status err;
	QString pLockResourceID;
	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	_SERVER_CALL(ClientSimpleORM->Delete(err,CE_COMM_ENTITY,lstForDelete, pLockResourceID, lstStatusRows, pBoolTransaction))
	_CHK_ERR(err);


	//reload this crap, send notify signals:

	//------------------------------------------------------
	//Send signal for all to reload:
	//------------------------------------------------------
	if (nSelectedDoc>0)
	{
		QVariant varData;
		qVariantSetValue(varData,lstDocumentsForDelete);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED,ENTITY_BUS_DM_DOCUMENTS,varData,this); 
	}

	if (nSelectedVC>0)
	{
		QVariant varData;
		qVariantSetValue(varData,lstVoiceCallsForDelete);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED,ENTITY_BUS_VOICECALLS,varData,this); 
	}

	if (nSelectedEmail>0)
	{
		QVariant varData;
		qVariantSetValue(varData,lstEmailsForDelete);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED,ENTITY_BUS_EMAILS,varData,this); 
	}

	pLstDataEmails->deleteSelectedRows();
	pLstDataVoiceCalls->deleteSelectedRows();
	pLstDataDocs->deleteSelectedRows();

	//refresh grid:
	ReloadViewWithoutSeverCall();
	//ReloadData();
}

void CommWorkBenchWidget::on_AddDoc_toolButton_clicked()
{

	DbRecordSet lstContactsDef,lstProjectsDef;
	GetDefaultData(lstContactsDef,lstProjectsDef);
	m_PieMenu.CreateNewDocument(this,NULL,&lstContactsDef,&lstProjectsDef,false,false);
}

void CommWorkBenchWidget::on_AddMail_toolButton_clicked()
{
	DbRecordSet lstContactsDef,lstProjectsDef;
	GetDefaultData(lstContactsDef,lstProjectsDef);

	//voice calls
	int nFUI = g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_EMPTY,-1,true);
	EMailDialog* pFUI = dynamic_cast<EMailDialog *>(g_objFuiManager.GetFUIWidget(nFUI));
	if(NULL == pFUI) return;	//someone closed the FUI for this call

	int nCntID=-1,nProjID=-1;
	if (lstProjectsDef.getRowCount()>0)
		nProjID=lstProjectsDef.getDataRef(0,0).toInt();
	pFUI->SetDefaults(lstContactsDef, "", nProjID);
	pFUI->show();


}

void CommWorkBenchWidget::on_AddVoiceCall_toolButton_clicked()
{
	DbRecordSet lstContactsDef,lstProjectsDef;
	GetDefaultData(lstContactsDef,lstProjectsDef);

	//voice calls
	int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false,FuiBase::MODE_EMPTY,-1,true);
	FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
	if(NULL == pFUI) return;	//someone closed the FUI for this call

	int nCntID=-1,nProjID=-1;
	if (lstProjectsDef.getRowCount()>0)
		nProjID=lstProjectsDef.getDataRef(0,0).toInt();
	if (lstContactsDef.getRowCount()>0)
		nCntID=lstContactsDef.getDataRef(0,0).toInt();
	pFUI->SetDefaultContact(nCntID,"",nProjID);
	pFUI->show();
}

void CommWorkBenchWidget::on_sort_toolButton_clicked()
{
	MultiSortColumn dlgSorter;

	//init
	dlgSorter.SetColumnSetup(m_lstSortColumnSetup, m_lstSortRecordSet);

	//show:
	if(dlgSorter.exec()>0)
	{
		m_lstSortRecordSet = dlgSorter.GetResultRecordSet();
		//_DUMP(m_lstSortRecordSet);
		ui.CommGrid_widget->SortTable(&m_lstSortRecordSet, true);
//		ReloadData();
	}
}

void CommWorkBenchWidget::on_dateFilter_checkBox_toggled(bool bChecked)
{
	//First check that some filter is already selected.
	if (!m_recFilterViewData.getRowCount())
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Please select a filter first!"));
		ui.dateFilter_checkBox->blockSignals(true);
		if (!bChecked)
		{
			ui.dateFilter_checkBox->setCheckState(Qt::Checked);
			ui.dateFilter_frame->setDisabled(false);
		}
		else
		{
			ui.dateFilter_checkBox->setCheckState(Qt::Unchecked);
			ui.dateFilter_frame->setDisabled(true);
		}
		ui.dateFilter_checkBox->blockSignals(false);
		return;
	}
	
	if (!bChecked)
	{
		ui.dateFilter_frame->setDisabled(true);
		SetFilterIntValue(0, FILTER_BY_DATE_ACTIVE);
	}
	else
	{
		ui.dateFilter_frame->setDisabled(false);
		SetFilterIntValue(1, FILTER_BY_DATE_ACTIVE);
	}
	ReloadData();
}

void CommWorkBenchWidget::on_showFilter_toolButton_clicked()
{
	if (m_nGridType == CommGridViewHelper::CALENDAR_GRID)
	{
		on_viewSelect_toolButton_clicked();
	}
	else
	{
		ui.filter_frame->setVisible(true);
		ui.buttons_frame->setVisible(false);
	}
}

void CommWorkBenchWidget::on_hideFilter_toolButton_clicked()
{
	ui.filter_frame->setVisible(false);
	ui.buttons_frame->setVisible(true);
}

void CommWorkBenchWidget::OnUserSAPNEChanged()
{
	if (!(m_nGridType == CommGridViewHelper::DESKTOP_GRID))
		return;

	//Get new person initials.
	DbRecordSet recTVIEW_BUS_PERSON_SELECTION;
	ui.User_frame->GetCurrentEntityRecord(m_nCurrentEntityID, recTVIEW_BUS_PERSON_SELECTION);
	m_strCurrentPersonInitials = recTVIEW_BUS_PERSON_SELECTION.getDataRef(0, "BPER_INITIALS").toString();
	if (m_strCurrentPersonInitials.isEmpty())
		m_strCurrentPersonInitials = QString(recTVIEW_BUS_PERSON_SELECTION.getDataRef(0,"BPER_LAST_NAME").toString().at(0)) + QString(recTVIEW_BUS_PERSON_SELECTION.getDataRef(0,"BPER_FIRST_NAME").toString().at(0));

	InitializeHeader(m_nGridType);
	if (m_nCurrentViewID>=0)
		ReloadViewSelector(m_nCurrentViewID);
	ReloadData();

	emit FilterChanged(m_strCurrentPersonInitials + ": " + ui.viewSelect_comboBox->currentText());
}

//Design by issue: 369, improved by issue 491 (multi assignment + owner assignment)
//Reload whole grid if error!
//Reload whole grid if owner changed!
//in any other case: soft update
void CommWorkBenchWidget::OnContactSAPNEChanged()
{

	ui.Contact_frame->blockSignals(true); //prevent backfire

	//get assignment:
	int nAssignedID,nOperation;
	DbRecordSet record;
	if(ui.Contact_frame->GetCurrentEntityRecord(nAssignedID,record)) //add new assignment
		nOperation=1;
	else
		nOperation=0;

	//get selected data from GRID
	DbRecordSet DataAssign;
	GetSelectedDataForSAPNEAssignemnt(DataAssign,nOperation);

	//server call:
	Status err;
	_SERVER_CALL(BusCommunication->ChangeEntityAssigment(err,0,nAssignedID,nOperation,DataAssign))
	if (!err.IsOK())
	{	
		QMessageBox::critical(this,(QString)tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		ui.Contact_frame->blockSignals(false);
		ReloadData();
		return;
	}

	//reset current contact info:
	if (nOperation)
		m_nContactID=nAssignedID;
	else
		m_nContactID=-1;

	ui.Contact_frame->blockSignals(false);

	//refresh data on grid, with new contact id:
	FillWithNewContactData(m_nContactID,GlobalConstants::CE_TYPE_DOCUMENT);
	FillWithNewContactData(m_nContactID,GlobalConstants::CE_TYPE_EMAIL);
	FillWithNewContactData(m_nContactID,GlobalConstants::CE_TYPE_VOICE_CALL);
}




//Design by issue: 369, improved by issue 491 (multi assignment + owner assignment)
//Reload whole grid if error!
//Reload whole grid if owner changed!
//in any other case: soft update
void CommWorkBenchWidget::OnProjectSAPNEChanged()
{

	ui.Project_frame->blockSignals(true); //prevent backfire

	//get assignment:
	int nAssignedID,nOperation;
	DbRecordSet record;
	if(ui.Project_frame->GetCurrentEntityRecord(nAssignedID,record)) //add new assignment
		nOperation=1;
	else
		nOperation=0;

	//get selected data from GRID
	DbRecordSet DataAssign;
	GetSelectedDataForSAPNEAssignemnt(DataAssign,nOperation);

	//server call:
	Status err;
	_SERVER_CALL(BusCommunication->ChangeEntityAssigment(err,1,nAssignedID,nOperation,DataAssign))
	if (!err.IsOK())
	{	
		QMessageBox::critical(this,(QString)tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		ui.Project_frame->blockSignals(false);
		ReloadData();
		return;
	}

	//reset current contact info:
	if (nOperation)
		m_nProjectID=nAssignedID;
	else
		m_nProjectID=-1;

	ui.Project_frame->blockSignals(false);

	//refresh data on grid, with new contact id:
	FillWithNewProjectData(m_nProjectID,GlobalConstants::CE_TYPE_DOCUMENT);
	FillWithNewProjectData(m_nProjectID,GlobalConstants::CE_TYPE_EMAIL);
	FillWithNewProjectData(m_nProjectID,GlobalConstants::CE_TYPE_VOICE_CALL);
}



//Design by issue: 369, improved by issue 491 (multi assignment + owner assignment)
//Reload whole grid if error!
//Reload whole grid if owner changed!
//in any other case: soft update
void CommWorkBenchWidget::OnOwnerSAPNEChanged()
{

	ui.Owner_frame->blockSignals(true); //prevent backfire

	//get assignment:
	int nAssignedID,nOperation;
	DbRecordSet record;
	if(ui.Owner_frame->GetCurrentEntityRecord(nAssignedID,record)) //add new assignment
		nOperation=1;
	else
		nOperation=0;

	//get selected data from GRID
	DbRecordSet DataAssign;
	GetSelectedDataForSAPNEAssignemnt(DataAssign,nOperation);

	//server call:
	Status err;
	_SERVER_CALL(BusCommunication->ChangeEntityAssigment(err,2,nAssignedID,nOperation,DataAssign))
	if (!err.IsOK())
	{	
		QMessageBox::critical(this,(QString)tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		ui.Owner_frame->blockSignals(false);
		ReloadData();
		return;
	}

	//reset current contact info:
	if (nOperation)
		m_nOwnerID=nAssignedID;
	else
		m_nOwnerID=-1;

	ui.Owner_frame->blockSignals(false);
	ReloadData();
	//refresh data on grid, with new contact id:
	//FillWithNewOwnerData(m_nOwnerID,GlobalConstants::CE_TYPE_DOCUMENT);
	//FillWithNewOwnerData(m_nOwnerID,GlobalConstants::CE_TYPE_EMAIL);
	//FillWithNewOwnerData(m_nOwnerID,GlobalConstants::CE_TYPE_VOICE_CALL);

}

void CommWorkBenchWidget::OnDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange, bool bResizeColumns)
{
	if(!m_recFilterViewData.getRowCount())
	{
		QMessageBox::information(NULL, tr("No Filter Selected"), tr("Please select a filter first!"));
		return;
	}

	SetFilterDateTimeValue(dtFrom, FROM_DATE);
	SetFilterDateTimeValue(dtTo, TO_DATE);
	SetFilterIntValue(nRange, DATE_RANGE_SELECTOR);

	//_DUMP(m_recFilterViewData);

	ReloadData();
}

void CommWorkBenchWidget::FillWithNewContactData(int nContactID, int nCE_Type)
{
	DbRecordSet pLstData;
	int nMsgDetail;
	
	switch(nCE_Type)
	{
	case GlobalConstants::CE_TYPE_DOCUMENT:
		pLstData=GetDocumentsRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_DM_DOCUMENTS;
		break;
	case GlobalConstants::CE_TYPE_EMAIL:
		pLstData=GetEmailRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_EMAILS;
		break;
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		pLstData=GetVoiceCallRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_VOICECALLS;
		break;
	}


	int nSize=pLstData.getRowCount();
	if (nSize==0) return;


	//get from cache
	QString strFName;
	QString strLName;
	QString strOrg;
	if (m_nContactID>0)
	{
		DbRecordSet *pLstDataCache=g_ClientCache.GetCache(ENTITY_BUS_CONTACT);
		if (pLstDataCache==NULL) return;
		int nRow=pLstDataCache->find("BCNT_ID",m_nContactID,true);
		if (nRow==-1) return;
		strFName=pLstDataCache->getDataRef(nRow,"BCNT_FIRSTNAME").toString();
		strLName=pLstDataCache->getDataRef(nRow,"BCNT_LASTNAME").toString();
		strOrg=pLstDataCache->getDataRef(nRow,"BCNT_ORGANIZATIONNAME").toString();
	}


	for(int nRow=0;nRow<nSize;++nRow)
	{
		if (m_nContactID>=0)
		{
			pLstData.setData(nRow,"BCNT_ID",m_nContactID);
			pLstData.setData(nRow,"BCNT_LASTNAME",strFName);
			pLstData.setData(nRow,"BCNT_FIRSTNAME",strLName);
			pLstData.setData(nRow,"BCNT_ORGANIZATIONNAME",strOrg);
		}
		else
		{
			pLstData.setData(nRow,"BCNT_ID",QVariant(QVariant::Int));
			pLstData.setData(nRow,"BCNT_LASTNAME","");
			pLstData.setData(nRow,"BCNT_FIRSTNAME","");
			pLstData.setData(nRow,"BCNT_ORGANIZATIONNAME","");
		}
	}	


	//pLstData.selectAll(); //select all for refresh:
	int nMsgCode = ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED;
	QVariant varData;
	qVariantSetValue(varData,pLstData);
	UpdateRecordsetsWithChangedData(nMsgCode, nMsgDetail, varData);

	//RefreshContactItem(nCE_Type, pLstData);
}

void CommWorkBenchWidget::FillWithNewProjectData(int nProjectID, int nCE_Type)
{
	DbRecordSet pLstData;
	int nMsgDetail;

	switch(nCE_Type)
	{
	case GlobalConstants::CE_TYPE_DOCUMENT:
		pLstData=GetDocumentsRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_DM_DOCUMENTS;
		break;
	case GlobalConstants::CE_TYPE_EMAIL:
		pLstData=GetEmailRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_EMAILS;
		break;
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		pLstData=GetVoiceCallRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_VOICECALLS;
		break;
	}

	int nSize=pLstData.getRowCount();
	if (nSize==0) return;

	QString strCode;
	QString strName;
	if (m_nProjectID>0)
	{
		//get from cache
		DbRecordSet *pLstDataCache=g_ClientCache.GetCache(ENTITY_BUS_PROJECT);
		if (pLstDataCache==NULL) return;
		int nRow=pLstDataCache->find("BUSP_ID",m_nProjectID,true);
		if (nRow==-1) return;
		strCode=pLstDataCache->getDataRef(nRow,"BUSP_CODE").toString();
		strName=pLstDataCache->getDataRef(nRow,"BUSP_NAME").toString();
	}
	
	for(int nRow=0;nRow<nSize;++nRow)
	{
		if (m_nProjectID>0)
		{
			pLstData.setData(nRow,"BUSP_ID",m_nProjectID);
			pLstData.setData(nRow,"BUSP_CODE",strCode);
			pLstData.setData(nRow,"BUSP_NAME",strName);
		}
		else
		{
			pLstData.setData(nRow,"BUSP_ID",QVariant(QVariant::Int));
			pLstData.setData(nRow,"BUSP_CODE","");
			pLstData.setData(nRow,"BUSP_NAME","");
		}
	}

	//pLstData.selectAll(); //select all for refresh:
	//RefreshContactItem(nCE_Type, pLstData);
	
	int nMsgCode = ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED;
	QVariant varData;
	qVariantSetValue(varData,pLstData);
	UpdateRecordsetsWithChangedData(nMsgCode, nMsgDetail, varData);
}


void CommWorkBenchWidget::FillWithNewOwnerData(int nProjectID, int nCE_Type)
{
	DbRecordSet pLstData;
	int nMsgDetail;

	switch(nCE_Type)
	{
	case GlobalConstants::CE_TYPE_DOCUMENT:
		pLstData=GetDocumentsRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_DM_DOCUMENTS;
		break;
	case GlobalConstants::CE_TYPE_EMAIL:
		pLstData=GetEmailRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_EMAILS;
		break;
	case GlobalConstants::CE_TYPE_VOICE_CALL:
		pLstData=GetVoiceCallRecordSet()->getSelectedRecordSet();
		nMsgDetail = ENTITY_BUS_VOICECALLS;
		break;
	}

	int nSize=pLstData.getRowCount();
	if (nSize==0) return;


	QString strFName;
	QString strLName;
	if (m_nOwnerID>0)
	{

		//get from cache
		DbRecordSet *pLstDataCache=g_ClientCache.GetCache(ENTITY_BUS_PERSON);
		if (pLstDataCache==NULL) return;
		int nRow=pLstDataCache->find("BPER_ID",m_nOwnerID,true);
		if (nRow==-1) return;
		strFName=pLstDataCache->getDataRef(nRow,"BPER_FIRST_NAME").toString();
		strLName=pLstDataCache->getDataRef(nRow,"BPER_LAST_NAME").toString();
	}



	for(int nRow=0;nRow<nSize;++nRow)
	{	
		if (m_nOwnerID>0)
		{
			pLstData.setData(nRow,"A.BPER_ID",m_nOwnerID);
			pLstData.setData(nRow,"A.BPER_FIRST_NAME",strFName);
			pLstData.setData(nRow,"A.BPER_LAST_NAME",strLName);
		}
		else
		{
			pLstData.setData(nRow,"A.BPER_ID",QVariant(QVariant::Int));
			pLstData.setData(nRow,"A.BPER_FIRST_NAME","");
			pLstData.setData(nRow,"A.BPER_LAST_NAME","");
		}
	}

	//pLstData.selectAll(); //select all for refresh:
	//RefreshContactItem(nCE_Type, pLstData);
	
	int nMsgCode = ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED;
	QVariant varData;
	qVariantSetValue(varData,pLstData);
	UpdateRecordsetsWithChangedData(nMsgCode, nMsgDetail, varData);
}



void CommWorkBenchWidget::GetSelectedDataForSAPNEAssignemnt(DbRecordSet &DataAssign, int nOperation)
{
	//one list is going on server:
	DataAssign.destroy();
	DataAssign.addColumn(QVariant::Int,"CENT_ID");
	DataAssign.addColumn(QVariant::Int,"TYPE_ID");
	DataAssign.addColumn(QVariant::Int,"ID");
	DataAssign.addColumn(QVariant::Int,"ROLE_ID");
	DataAssign.addColumn(QVariant::Int,"BUSP_ID"); //B.T. added because of issue 2535: when removing project or contact I must know their ID 
	DataAssign.addColumn(QVariant::Int,"BCNT_ID");

	//get selected rows:
	DbRecordSet pLstDataEmails=GetEmailRecordSet()->getSelectedRecordSet();
	DbRecordSet pLstDataVoiceCalls=GetVoiceCallRecordSet()->getSelectedRecordSet();
	DbRecordSet pLstDataDocuments=GetDocumentsRecordSet()->getSelectedRecordSet();


	if(nOperation) //add new assignment
	{
		int nSize=pLstDataDocuments.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			DataAssign.addRow();
			int nRow=DataAssign.getRowCount()-1;
			DataAssign.setData(nRow,0,pLstDataDocuments.getDataRef(i,"CENT_ID"));
			DataAssign.setData(nRow,1,QVariant(GlobalConstants::CE_TYPE_DOCUMENT).toInt());
			DataAssign.setData(nRow,2,pLstDataDocuments.getDataRef(i,"BDMD_ID"));
			DataAssign.setData(nRow,3,QVariant(GlobalConstants::CONTACT_LINK_ROLE_DOC_OWNER).toInt());
			DataAssign.setData(nRow,4,pLstDataDocuments.getDataRef(i,"BUSP_ID"));
			DataAssign.setData(nRow,5,pLstDataDocuments.getDataRef(i,"BCNT_ID"));
		}
		nSize=pLstDataEmails.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			int nOutgoing=pLstDataEmails.getDataRef(i,"BEM_OUTGOING").toInt();
			int nRole;
			if (nOutgoing)
				nRole=GlobalConstants::CONTACT_LINK_ROLE_TO;
			else
				nRole=GlobalConstants::CONTACT_LINK_ROLE_FROM;

			DataAssign.addRow();
			int nRow=DataAssign.getRowCount()-1;
			DataAssign.setData(nRow,0,pLstDataEmails.getDataRef(i,"CENT_ID"));
			DataAssign.setData(nRow,1,QVariant(GlobalConstants::CE_TYPE_EMAIL).toInt());
			DataAssign.setData(nRow,2,pLstDataEmails.getDataRef(i,"BEM_ID"));
			DataAssign.setData(nRow,3,nRole);
			DataAssign.setData(nRow,4,pLstDataEmails.getDataRef(i,"BUSP_ID"));
			DataAssign.setData(nRow,5,pLstDataEmails.getDataRef(i,"BCNT_ID"));
		}
		nSize=pLstDataVoiceCalls.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			DataAssign.addRow();
			int nRow=DataAssign.getRowCount()-1;
			DataAssign.setData(nRow,0,pLstDataVoiceCalls.getDataRef(i,"CENT_ID"));
			DataAssign.setData(nRow,1,QVariant(GlobalConstants::CE_TYPE_VOICE_CALL).toInt());
			DataAssign.setData(nRow,2,pLstDataVoiceCalls.getDataRef(i,"BVC_ID"));
			DataAssign.setData(nRow,4,pLstDataVoiceCalls.getDataRef(i,"BUSP_ID"));
			DataAssign.setData(nRow,5,pLstDataVoiceCalls.getDataRef(i,"BCNT_ID"));
			//vc does not have role
		}
	}
	else //removal (role is not of essence)
	{
		int nSize=pLstDataDocuments.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			DataAssign.addRow();
			int nRow=DataAssign.getRowCount()-1;
			DataAssign.setData(nRow,0,pLstDataDocuments.getDataRef(i,"CENT_ID"));
			DataAssign.setData(nRow,1,QVariant(GlobalConstants::CE_TYPE_DOCUMENT).toInt());
			DataAssign.setData(nRow,2,pLstDataDocuments.getDataRef(i,"BDMD_ID"));
			DataAssign.setData(nRow,4,pLstDataDocuments.getDataRef(i,"BUSP_ID"));
			DataAssign.setData(nRow,5,pLstDataDocuments.getDataRef(i,"BCNT_ID"));
		}
		nSize=pLstDataEmails.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			DataAssign.addRow();
			int nRow=DataAssign.getRowCount()-1;
			DataAssign.setData(nRow,0,pLstDataEmails.getDataRef(i,"CENT_ID"));
			DataAssign.setData(nRow,1,QVariant(GlobalConstants::CE_TYPE_EMAIL).toInt());
			DataAssign.setData(nRow,2,pLstDataEmails.getDataRef(i,"BEM_ID"));
			DataAssign.setData(nRow,4,pLstDataEmails.getDataRef(i,"BUSP_ID"));
			DataAssign.setData(nRow,5,pLstDataEmails.getDataRef(i,"BCNT_ID"));
		}
		nSize=pLstDataVoiceCalls.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			DataAssign.addRow();
			int nRow=DataAssign.getRowCount()-1;
			DataAssign.setData(nRow,0,pLstDataVoiceCalls.getDataRef(i,"CENT_ID"));
			DataAssign.setData(nRow,1,QVariant(GlobalConstants::CE_TYPE_VOICE_CALL).toInt());
			DataAssign.setData(nRow,2,pLstDataVoiceCalls.getDataRef(i,"BVC_ID"));
			DataAssign.setData(nRow,4,pLstDataVoiceCalls.getDataRef(i,"BUSP_ID"));
			DataAssign.setData(nRow,5,pLstDataVoiceCalls.getDataRef(i,"BCNT_ID"));
		}
	}

}

void CommWorkBenchWidget::GetDefaultData(DbRecordSet &lstContactsDef,DbRecordSet &lstProjectsDef)
{
	int nGrid,nRecID;
	GetGridData(nGrid,nRecID);

	lstContactsDef.destroy();
	lstProjectsDef.destroy();

	lstContactsDef.addColumn(QVariant::Int,"BCNT_ID");
	lstProjectsDef.addColumn(QVariant::Int,"BUSP_ID");

	switch(nGrid)
	{
	case CommGridViewHelper::CONTACT_GRID:
		if (nRecID!=-1)
		{
			lstContactsDef.addRow();
			lstContactsDef.setData(0,0,nRecID);
		}
		break;
	case CommGridViewHelper::PROJECT_GRID:
		if (nRecID!=-1)
		{
			lstProjectsDef.addRow();
			lstProjectsDef.setData(0,0,nRecID);
		}
		break;
	}
}

void CommWorkBenchWidget::InitializeHeader(int nGridType)
{
	m_nGridType = nGridType;
	if (m_nGridType == CommGridViewHelper::DESKTOP_GRID)
		ui.User_frame->GetCurrentEntityRecord(m_nLoggedPersonID);
	else
	{
		ui.User_frame->setHidden(true);
		ui.User_label->setHidden(true);
	}

	DisableHeader();
}

void CommWorkBenchWidget::DisableHeader()
{
	m_nEntityID		= -1;
	m_nTaskID		= -1;
	m_nOwnerID		= -1;
	m_nContactID	= -1; 
	m_nProjectID	= -1;
	m_nEntitySysType= -1;
	m_nPolymorficID	= -1;

	//Disable SAPNES.
	ui.Owner_frame	->setDisabled(true);
	ui.Contact_frame->setDisabled(true);
	ui.Project_frame->setDisabled(true);
	ui.dateFilter_frame->setDisabled(true);
	ui.Owner_frame	->Clear(true);
	ui.Contact_frame->Clear(true);
	ui.Project_frame->Clear(true);

	//Pie menu
	ui.pieMenu_toolButton->setDisabled(true);
	//Open button.
	ui.openDoc_toolButton->setDisabled(true);
	//Start button.
	//ui.startTask_toolButton->setDisabled(true);
	//Postpone task.
	ui.PostponeTask_toolButton->setDisabled(true);
	//Task done button.
	ui.taskDone_toolButton->setDisabled(true);
	//Check in.
	ui.checkIn_toolButton->setDisabled(true);
	//Reply, Forward, Pack and send.
	ui.Reply_toolButton->setDisabled(true);
	ui.Forward_toolButton->setDisabled(true);
	ui.PackAndSend_toolButton->setDisabled(true);
	//Disable select and modify.
	ui.select_toolButton->setDisabled(true);
	ui.clear_toolButton->setDisabled(true);
	ui.modify_toolButton->setDisabled(true);
}

void CommWorkBenchWidget::ClearHeader()
{
	//Disable SAPNES.
	ui.Owner_frame	->setDisabled(true);
	ui.Contact_frame->setDisabled(true);
	ui.Project_frame->setDisabled(true);
	ui.Owner_frame	->Clear(true);
	ui.Contact_frame->Clear(true);
	ui.Project_frame->Clear(true);

	//Pie menu
	ui.pieMenu_toolButton->setDisabled(true);
	//Open button.
	ui.openDoc_toolButton->setDisabled(true);
	//Start button.
	//ui.startTask_toolButton->setDisabled(true);
	ui.startTask_toolButton->setEnabled(true); //issue 2077: always ON
	//Postpone task.
	ui.PostponeTask_toolButton->setDisabled(true);
	//Task done button.
	ui.taskDone_toolButton->setDisabled(true);
	//Reply, Forward, Pack and send.
	ui.Reply_toolButton->setDisabled(true);
	ui.Forward_toolButton->setDisabled(true);
	ui.PackAndSend_toolButton->setDisabled(true);
	//Disable select and modify.
	ui.select_toolButton->setDisabled(true);
	ui.clear_toolButton->setDisabled(true);
	ui.modify_toolButton->setDisabled(true);
}

bool CommWorkBenchWidget::ReloadViewSelector(int nViewID /*= -1*/, bool bFromDefaultSavedView /*= false*/)
{
	m_recFilterViews.clear();
	Status status;
	//Also adjust eye of the tiger icon. Issue #1964.
	if (nViewID <= 0)
	{
		_SERVER_CALL(BusCommunication->GetCommFilterViews(status, m_recFilterViews, m_nLoggedPersonID, m_nGridType))
		ui.showFilter_toolButton->setIcon(QIcon(":Eye.png"));
	}
	else
	{
		_SERVER_CALL(BusCommunication->GetCommFilterViewDataAndViews(status, m_recFilterViewData, m_recFilterViews, nViewID, m_nGridType))
		ui.showFilter_toolButton->setIcon(QIcon(":Eye_Active.png"));

		int nViewRow = m_recFilterViews.find("BUSCV_ID", nViewID, true);
		if (nViewRow>=0)
		{
			QByteArray byteView = m_recFilterViews.getDataRef(nViewRow, "BUSCV_COMMGRIDVIEW_SORT").toByteArray();
			m_lstSortRecordSet = XmlUtil::ConvertByteArray2RecordSet_Fast(byteView);
			//_DUMP(m_lstSortRecordSet);
		}
	}

	if (!status.IsOK())
	{
		return false;
	}
	
	//Load views in combo.
	LoadViewComboBox(nViewID);

	//Setup date range selector if from default saved contact or project view.
//	if (bFromDefaultSavedView)
		SetupDateRangeSelector();

//		_DUMP(m_recFilterViews);
//		_DUMP(m_recFilterViewData);

	return true;


	//Load views in combo.
	//LoadViewComboBox(nViewID);
}

void CommWorkBenchWidget::LoadViewComboBox(int nViewID)
{
	ui.viewSelect_comboBox->blockSignals(true);
	//Fill combo.
	ui.viewSelect_comboBox->clear();
	int nRowCount = m_recFilterViews.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		int ViewID = m_recFilterViews.getDataRef(i, "BUSCV_ID").toInt();
		QString strViewName = m_recFilterViews.getDataRef(i, "BUSCV_NAME").toString();
		ui.viewSelect_comboBox->addItem(strViewName, ViewID);
	}
	//Set current item.
	ui.viewSelect_comboBox->setCurrentIndex(ui.viewSelect_comboBox->findData(nViewID));
	ui.viewSelect_comboBox->blockSignals(false);
}

void CommWorkBenchWidget::SetFilterIntValue(int nValue, int nFilterSetting)
{
	//_DUMP(m_recFilterViewData);
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		m_recFilterViewData.setData(nRow, "BUSCS_VALUE", nValue);
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		Q_ASSERT(nTmpRow >= 0);
		tmp.setData(nTmpRow, "BUSCS_VALUE", nValue);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		m_recFilterViewData.merge(tmp, true);
	}
}

void CommWorkBenchWidget::SetFilterDateTimeValue(QDateTime datValue, int nFilterSetting)
{
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		m_recFilterViewData.setData(nRow, "BUSCS_VALUE_DATETIME", datValue);
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		Q_ASSERT(nTmpRow >= 0);
		tmp.setData(nTmpRow, "BUSCS_VALUE_DATETIME", datValue);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		m_recFilterViewData.merge(tmp, true);
	}
}

bool CommWorkBenchWidget::GetFilterBoolValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	bool bValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		bValue = m_recFilterViewData.getDataRef(m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true), "BUSCS_VALUE").toBool();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		bValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE").toBool();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return bValue;
}

int CommWorkBenchWidget::GetFilterIntValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	int nValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		nValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE").toInt();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		nValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE").toInt();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return nValue;
}

QString CommWorkBenchWidget::GetFilterStringValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	QString strValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		strValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE_STRING").toString();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		strValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE_STRING").toString();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return strValue;
}

QDateTime CommWorkBenchWidget::GetFilterDateTimeValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	QDateTime datValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		datValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE_DATETIME").toDateTime();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		datValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE_DATETIME").toDateTime();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return datValue;
}

void CommWorkBenchWidget::FillInsertDefaultValues(DbRecordSet &recFilterSettings)
{
	//Fill default recordset data.
	recFilterSettings.clear();
	CommGridFilterSettings filterSettings;
	recFilterSettings = filterSettings.GetCommFilterSettings();

	if (m_nGridType == CommGridViewHelper::CONTACT_GRID)
	{
		recFilterSettings.setData(recFilterSettings.find("BUSCS_SETTING_ID", FILTER_BY_ENTITY_TYPE_ACTIVE, true), "BUSCS_VALUE", 1);
	}
	else if (m_nGridType == CommGridViewHelper::PROJECT_GRID)
	{
		recFilterSettings.setData(recFilterSettings.find("BUSCS_SETTING_ID", FILTER_BY_ENTITY_TYPE_ACTIVE, true), "BUSCS_VALUE", 1);
	}
}

void CommWorkBenchWidget::SetupDateRangeSelector()
{
	//_DUMP(m_recFilterViewData);
	bool bDateFilterUsed = GetFilterBoolValue(m_recFilterViewData, FILTER_BY_DATE_ACTIVE);
	QDateTime datFrom	 = GetFilterDateTimeValue(m_recFilterViewData, FROM_DATE);
	QDateTime datTo		 = GetFilterDateTimeValue(m_recFilterViewData, TO_DATE);
	int nRange			 = GetFilterIntValue(m_recFilterViewData, DATE_RANGE_SELECTOR);

	//If some range selected use the range not the stored date.
	if (nRange > 0)
	{
		QDate dateFrom, dateTo;
		switch(nRange){
			case 1: // "Day"
				dateFrom = QDate::currentDate();
				dateTo = dateFrom; 
				break;
			case 2: // "Week" (current week)
				{
					dateFrom = QDate::currentDate();
					int nWeekday = dateFrom.dayOfWeek();
					dateFrom = dateFrom.addDays(-nWeekday+1); 
					dateTo = dateFrom; 
					dateTo = dateTo.addDays(6);
				}
				break;
			case 3: // "Month"
				{
					dateFrom = QDate::currentDate();
					if(dateFrom.day() > 1)
						dateFrom = dateFrom.addDays(-(dateFrom.day()-1));
					dateTo = dateFrom;
					int nDaysInMonth = dateFrom.daysInMonth();
					dateTo = dateTo.addDays(nDaysInMonth-1);
				}
				break;
			case 4: // "Year" - move to the start of the this year
				dateFrom = QDate::currentDate();
				dateFrom = QDate(dateFrom.year(), 1, 1);
				dateTo = QDate(dateFrom.year(), 12, 31); 
				break;
		}

		datFrom = QDateTime(dateFrom, QTime(0,0,0));
		datTo	= QDateTime(dateTo, QTime(23,59,59));

		SetFilterDateTimeValue(datFrom, FROM_DATE);
		SetFilterDateTimeValue(datTo, TO_DATE);
	}

	ui.dateFilter_checkBox->blockSignals(true);
	ui.dateFilter_checkBox->setChecked(bDateFilterUsed);
	ui.dateFilter_checkBox->blockSignals(false);
	ui.dateFilter_frame->blockSignals(true);
	ui.dateFilter_frame->setEnabled(bDateFilterUsed);
	ui.dateFilter_frame->Initialize(datFrom, datTo, nRange);
	ui.dateFilter_frame->blockSignals(false);
}

void CommWorkBenchWidget::UpdateRecordsetsWithChangedData(int nMsgCode, int nMsgDetail, const QVariant val)
{
	//On insert just set selection on new data->
	//On delete remove item from grid.

	bool bReloaded = false;
	if(nMsgDetail==ENTITY_BUS_DM_DOCUMENTS)
	{
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED)
		{
			//if (g_objFuiManager.GetCurrentCommGridGID() == g_objFuiManager.GetFuiID(static_cast<FuiBase*>(m_pParentFUI)))
			if ((m_bSideBarMode && m_bIsCurrentFUIinSideBarMode) || (g_objFuiManager.GetCurrentCommGridGID() == g_objFuiManager.GetFuiID(static_cast<FuiBase*>(m_pParentFUI))))
			{
				//Issue #1929.
				//ReloadData();
				//If we are in contact or project grid, add new data regardless of filter.
				//if (m_nGridType > 0)
				if (1)
				{
					DbRecordSet lstAddedData=val.value<DbRecordSet>();
					DbRecordSet *lstDocs=GetDocumentsRecordSet();
					
					Q_ASSERT(lstAddedData.getRowCount() > 0);

					//Select deleted items.
					lstDocs->clearSelection();
					lstAddedData.clearSelection();
					int nRowCount = lstAddedData.getRowCount();
					if (lstDocs->getRowCount()>0 && lstDocs->getColumnCount()>0) //BT: if no document, lstDocs is empty
					{
						
						for (int i = 0; i < nRowCount; ++i)
						{
							int nBDMD_ID = lstAddedData.getDataRef(i, "BDMD_ID").toInt();
							if (lstDocs->find("BDMD_ID", nBDMD_ID, false, false, false) > 0)
								lstAddedData.selectRow(i);
						}
					}

					//Make new recordset with only BEM_ID and merge lstAddedData (Added emails recordset) - only BEM_ID's go back to server.
					DbRecordSet lstAddedBDMD_IDs;
					lstAddedBDMD_IDs.addColumn(QVariant::Int, "BDMD_ID");
					lstAddedBDMD_IDs.merge(lstAddedData);

					//If we selected the same number of rows we inserted than do nothing, 
					//else load all new data in grid (regardless of the filter).
					if (lstDocs->getSelectedCount() > nRowCount || lstDocs->getSelectedCount() < nRowCount)
					{
						Status status;
						DbRecordSet tmp;
						_SERVER_CALL(BusCommunication->GetCommGridDataByIDs(status, tmp, lstAddedBDMD_IDs, m_nGridType, ENTITY_BUS_DM_DOCUMENTS));
						lstDocs->merge(tmp);
						ReloadViewWithoutSeverCall();
					}

					SetSelectionOnNewItems(GlobalConstants::CE_TYPE_DOCUMENT, lstAddedBDMD_IDs);
					return;
				}
			}

			DbRecordSet lstNewData=val.value<DbRecordSet>();
			SetSelectionOnNewItems(GlobalConstants::CE_TYPE_DOCUMENT,lstNewData);
			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED)
		{
			DbRecordSet lstDelData=val.value<DbRecordSet>();
			DbRecordSet *lstDocs=GetDocumentsRecordSet();
			if (lstDocs->getRowCount()>0 && lstDocs->getColumnCount()>0) //BT: if no document, lstDocs is empty
			{
				Q_ASSERT(lstDelData.getRowCount() > 0);

				//Select deleted items.
				lstDocs->clearSelection();
				int nRowCount = lstDelData.getRowCount();

				for (int i = 0; i < nRowCount; ++i)
				{
					int nBDMD_ID = lstDelData.getDataRef(i, "BDMD_ID").toInt();
					lstDocs->find("BDMD_ID",nBDMD_ID, false, false, false);
				}
				//Delete selected.
				if (lstDocs->deleteSelectedRows())
					ReloadViewWithoutSeverCall();
			}
			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED || nMsgCode==ChangeManager::GLOBAL_CHECK_OUT_DOC || nMsgCode==ChangeManager::GLOBAL_CHECK_IN_DOC)
		{
			DbRecordSet lstEditedData=val.value<DbRecordSet>();

			DbRecordSet *lstDocs=GetDocumentsRecordSet();
			Q_ASSERT(lstEditedData.getRowCount() > 0);
			
			//Select deleted items.
			lstDocs->clearSelection();
			lstEditedData.clearSelection();
			if (lstDocs->getRowCount()>0 && lstDocs->getColumnCount()>0) //BT: if no document, lstDocs is empty
			{
				int nRowCount = lstEditedData.getRowCount();
				for (int i = 0; i < nRowCount; ++i)
				{
					int nBDMD_ID = lstEditedData.getDataRef(i, "BDMD_ID").toInt();
					if (lstDocs->find("BDMD_ID", nBDMD_ID, false, false, false) > 0)
						lstEditedData.selectRow(i);
				}
			
				lstEditedData.deleteUnSelectedRows();
				lstDocs->deleteSelectedRows();
				int nEditedRowsCount = lstEditedData.getRowCount();
				
				//If there is some data to reload reload it.
				if (nEditedRowsCount)
				{
					Status status;
					DbRecordSet tmp;
					_SERVER_CALL(BusCommunication->GetCommGridDataByIDs(status, tmp, lstEditedData, m_nGridType, ENTITY_BUS_DM_DOCUMENTS))
					lstDocs->merge(tmp);
					ReloadViewWithoutSeverCall(false /*Clear selection = false*/);
				}

				SetSelectionOnNewItems(GlobalConstants::CE_TYPE_DOCUMENT,lstEditedData);
			}
			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			ReloadData();
			DbRecordSet lstNewData=val.value<DbRecordSet>();
			SetSelectionOnNewItems(GlobalConstants::CE_TYPE_DOCUMENT,lstNewData);
			return;
		}
	}

	if(nMsgDetail==ENTITY_BUS_EMAILS)
	{
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED)
		{
			//if (g_objFuiManager.GetCurrentCommGridGID() == g_objFuiManager.GetFuiID(static_cast<FuiBase*>(m_pParentFUI)))
			if ((m_bSideBarMode && m_bIsCurrentFUIinSideBarMode) || (g_objFuiManager.GetCurrentCommGridGID() == g_objFuiManager.GetFuiID(static_cast<FuiBase*>(m_pParentFUI))))
			{
				//Issue #1929.
				//ReloadData();
				//If we are in contact or project grid, add new data regardless of filter.
				//if (m_nGridType > 0)
				if (1)
				{
					DbRecordSet lstAddedData=val.value<DbRecordSet>();
					DbRecordSet *lstEmails=GetEmailRecordSet();
					Q_ASSERT(lstAddedData.getRowCount() > 0);

					//Select deleted items.
					lstEmails->clearSelection();
					lstAddedData.clearSelection();
					int nRowCount = lstAddedData.getRowCount();
					if (lstEmails->getRowCount()>0 && lstEmails->getColumnCount()>0) //BT: if no data, lstEmails is empty
					{
						for (int i = 0; i < nRowCount; ++i)
						{
							int nBEM_ID = lstAddedData.getDataRef(i, "BEM_ID").toInt();
							if (lstEmails->find("BEM_ID", nBEM_ID, false, false, false) > 0)
								lstAddedData.selectRow(i);
						}
					}
					
					//Make new recordset with only BEM_ID and merge lstAddedData (Added emails recordset) - only BEM_ID's go back to server.
					DbRecordSet lstAddedBEM_IDs;
					lstAddedBEM_IDs.addColumn(QVariant::Int, "BEM_ID");
					lstAddedBEM_IDs.merge(lstAddedData);

					//If we selected the same number of rows we inserted than do nothing, 
					//else load all new data in grid (regardless of the filter).
					if (lstEmails->getSelectedCount() > nRowCount || lstEmails->getSelectedCount() < nRowCount)
					{
						Status status;
						DbRecordSet tmp;
						_SERVER_CALL(BusCommunication->GetCommGridDataByIDs(status, tmp, lstAddedBEM_IDs/*lstAddedData*/, m_nGridType, ENTITY_BUS_EMAILS))
						lstEmails->merge(tmp);
						ReloadViewWithoutSeverCall();
					}

					SetSelectionOnNewItems(GlobalConstants::CE_TYPE_EMAIL,lstAddedBEM_IDs);
					return;
				}
			}

			DbRecordSet lstNewData=val.value<DbRecordSet>();
			SetSelectionOnNewItems(GlobalConstants::CE_TYPE_EMAIL,lstNewData);
			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED)
		{
			DbRecordSet lstDelData=val.value<DbRecordSet>();

			DbRecordSet *lstEmails=GetEmailRecordSet();
			if (lstEmails->getRowCount()>0 && lstEmails->getColumnCount()>0)//BT: if no data, lstEmails is empty
			{
				Q_ASSERT(lstDelData.getRowCount() > 0);
				//Select deleted items.
				lstEmails->clearSelection();
				int nRowCount = lstDelData.getRowCount();
				for (int i = 0; i < nRowCount; ++i)
				{
					int nBEM_ID = lstDelData.getDataRef(i, "BEM_ID").toInt();
					lstEmails->find("BEM_ID", nBEM_ID, false, false, false);
				}
				//Delete selected.
				if (lstEmails->deleteSelectedRows())
					ReloadViewWithoutSeverCall();
			}

			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
		{
			DbRecordSet lstEditedData=val.value<DbRecordSet>();

			DbRecordSet *lstEmails = GetEmailRecordSet();
			Q_ASSERT(lstEditedData.getRowCount() > 0);

			//Select edited items.
			lstEmails->clearSelection();
			lstEditedData.clearSelection();
			if (lstEmails->getRowCount()>0 && lstEmails->getColumnCount()>0)//BT: if no data, lstEmails is empty
			{
				int nRowCount = lstEditedData.getRowCount();
				for (int i = 0; i < nRowCount; ++i)
				{
					int nBEM_ID = lstEditedData.getDataRef(i, "BEM_ID").toInt();
					if (lstEmails->find("BEM_ID", nBEM_ID, false, false, false) > 0)
						lstEditedData.selectRow(i);
				}
				lstEditedData.deleteUnSelectedRows();
				lstEmails->deleteSelectedRows();
				int nEditedRowsCount = lstEditedData.getRowCount();

				//If there is some data to reload reload it.
				if (nEditedRowsCount)
				{
					Status status;
					DbRecordSet tmp;
					_SERVER_CALL(BusCommunication->GetCommGridDataByIDs(status, tmp, lstEditedData, m_nGridType, ENTITY_BUS_EMAILS))
					lstEmails->merge(tmp);
					ReloadViewWithoutSeverCall(false /*Clear selection = false*/);
					//ReloadViewWithoutSeverCall();
				}

				SetSelectionOnNewItems(GlobalConstants::CE_TYPE_EMAIL,lstEditedData);
			}
			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			ReloadData();
			DbRecordSet lstNewData=val.value<DbRecordSet>();
			SetSelectionOnNewItems(GlobalConstants::CE_TYPE_EMAIL,lstNewData);
			return;
		}
	}

	if(nMsgDetail==ENTITY_BUS_VOICECALLS)
	{
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED)
		{
			//if (g_objFuiManager.GetCurrentCommGridGID() == g_objFuiManager.GetFuiID(static_cast<FuiBase*>(m_pParentFUI)))
			if ((m_bSideBarMode && m_bIsCurrentFUIinSideBarMode) || (g_objFuiManager.GetCurrentCommGridGID() == g_objFuiManager.GetFuiID(static_cast<FuiBase*>(m_pParentFUI))))
			{
				//Issue #1929.
				//ReloadData();
				//If we are in contact or project grid, add new data regardless of filter.
				//if (m_nGridType > 0)
				if (1)
				{
					DbRecordSet lstAddedData=val.value<DbRecordSet>();

					DbRecordSet *lstVoiceCalls=GetVoiceCallRecordSet();
					Q_ASSERT(lstAddedData.getRowCount() > 0);

					//Select deleted items.
					lstVoiceCalls->clearSelection();
					lstAddedData.clearSelection();
					int nRowCount = lstAddedData.getRowCount();
					if (lstVoiceCalls->getRowCount()>0 && lstVoiceCalls->getColumnCount()>0)//BT: if no data, lstVoiceCalls is empty
					{
						for (int i = 0; i < nRowCount; ++i)
						{
							int nBVC_ID = lstAddedData.getDataRef(i, "BVC_ID").toInt();
							if (lstVoiceCalls->find("BVC_ID", nBVC_ID, false, false, false) > 0)
								lstAddedData.selectRow(i);
						}
					}
					//Make new recordset with only BVC_ID and merge lstAddedData (Added emails recordset) - only BVC_ID's go back to server.
					DbRecordSet lstAddedBVC_IDs;
					lstAddedBVC_IDs.addColumn(QVariant::Int, "BVC_ID");
					lstAddedBVC_IDs.merge(lstAddedData);

					//If we selected the same number of rows we inserted than do nothing, 
					//else load all new data in grid (regardless of the filter).
					if (lstVoiceCalls->getSelectedCount() > nRowCount || lstVoiceCalls->getSelectedCount() < nRowCount)
					{
						Status status;
						DbRecordSet tmp;
						_SERVER_CALL(BusCommunication->GetCommGridDataByIDs(status, tmp, lstAddedBVC_IDs/*lstAddedData*/, m_nGridType, ENTITY_BUS_VOICECALLS))
						lstVoiceCalls->merge(tmp);
						ReloadViewWithoutSeverCall();
					}

					SetSelectionOnNewItems(GlobalConstants::CE_TYPE_VOICE_CALL, lstAddedBVC_IDs);
					return;
				}
			}

			DbRecordSet lstNewData=val.value<DbRecordSet>();
			SetSelectionOnNewItems(GlobalConstants::CE_TYPE_VOICE_CALL,lstNewData);
			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED)
		{
			DbRecordSet lstDelData=val.value<DbRecordSet>();
			DbRecordSet *lstVoiceCalls=GetVoiceCallRecordSet();
			if (lstVoiceCalls->getRowCount()>0 && lstVoiceCalls->getColumnCount()>0)
			{
				Q_ASSERT(lstDelData.getRowCount() > 0);
				//Select deleted items.
				lstVoiceCalls->clearSelection();
				int nRowCount = lstDelData.getRowCount();
				for (int i = 0; i < nRowCount; ++i)
				{
					int nBVC_ID = lstDelData.getDataRef(i, "BVC_ID").toInt();
					lstVoiceCalls->find("BVC_ID",nBVC_ID, false, false, false);
				}
				//Delete selected.
				if (lstVoiceCalls->deleteSelectedRows())
					ReloadViewWithoutSeverCall();
			}
			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
		{
			DbRecordSet lstEditedData=val.value<DbRecordSet>();

			DbRecordSet *lstVoiceCalls = GetVoiceCallRecordSet();
			//lstVoiceCalls->Dump();
			Q_ASSERT(lstEditedData.getRowCount() > 0);

			//Select deleted items.
			lstVoiceCalls->clearSelection();
			lstEditedData.clearSelection();
			if (lstVoiceCalls->getRowCount()>0 && lstVoiceCalls->getColumnCount()>0)//BT: if no data, lstVoiceCalls is empty
			{

				int nRowCount = lstEditedData.getRowCount();
				for (int i = 0; i < nRowCount; ++i)
				{
					int nBVC_ID = lstEditedData.getDataRef(i, "BVC_ID").toInt();
					if (lstVoiceCalls->find("BVC_ID", nBVC_ID, false, false, false) > 0)
						lstEditedData.selectRow(i);
				}
				lstEditedData.deleteUnSelectedRows();
				//lstVoiceCalls->Dump();
				lstVoiceCalls->deleteSelectedRows();
				//lstVoiceCalls->Dump();
				int nEditedRowsCount = lstEditedData.getRowCount();

				//If there is some data to reload reload it.
				if (nEditedRowsCount)
				{
					Status status;
					DbRecordSet tmp;
					_SERVER_CALL(BusCommunication->GetCommGridDataByIDs(status, tmp, lstEditedData, m_nGridType, ENTITY_BUS_VOICECALLS))
					//lstVoiceCalls->Dump();
					//_DUMP(tmp);
					lstVoiceCalls->merge(tmp);
					//lstVoiceCalls->Dump();

					//ReloadViewWithoutSeverCall();
					ReloadViewWithoutSeverCall(false /*Clear selection = false*/);
				}

				SetSelectionOnNewItems(GlobalConstants::CE_TYPE_VOICE_CALL,lstEditedData);
			}
			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			ReloadData();
			DbRecordSet lstNewData=val.value<DbRecordSet>();
			SetSelectionOnNewItems(GlobalConstants::CE_TYPE_VOICE_CALL,lstNewData);
			return;
		}
	}

/*
	//make sensitive to check in/out:
	if (nMsgCode==ChangeManager::GLOBAL_CHECK_OUT_DOC ||nMsgCode==ChangeManager::GLOBAL_CHECK_IN_DOC)
	{
		DbRecordSet *lstDocs=GetDocumentsRecordSet();
		if(lstDocs->find("BDMD_ID",nMsgDetail,true)!=-1);	//if found in grid, reload:
		ReloadData();
		return;
	}
	*/
}

void CommWorkBenchWidget::ReplaceDocumentData(DbRecordSet &recSource)
{
	//int nRowCount
}


void CommWorkBenchWidget::SubMenuRequestedNewData()
{ 
	emit NeedNewData(CommunicationMenu_Base::SUBMENU_CEGRID);
}

void CommWorkBenchWidget::OnCnxtMenuActionTriggered(int nActionType)
{
	switch(nActionType)
	{
	case CommGridView::CNXT_OPEN:
		on_openDoc_toolButton_clicked();
		break;
	case CommGridView::CNXT_VIEW:
		on_select_toolButton_clicked();
		break;
	case CommGridView::CNXT_MODIFY:
		on_modify_toolButton_clicked();
	    break;
	case CommGridView::CNXT_DELETE:
		on_clear_toolButton_clicked();
	    break;
	case CommGridView::CNXT_SAVECOPY:
		{
			//open doc in app:
			DbRecordSet *pLstData=GetDocumentsRecordSet();
			DbRecordSet lstDocs=pLstData->getSelectedRecordSet();
			QString strDir=DocumentHelper::GetCheckOutDocumentDirectoryPath();
			if (strDir.isEmpty())
				return;
			int nSize=lstDocs.getRowCount();
			for(int i=0;i<nSize;i++)
			{
				//DocumentHelper::OpenDocumentInExternalApp(lstDocs.getRow(i),false,strDir,1);
				//issue 1959:
				QString strDocPath=lstDocs.getDataRef(i,"BDMD_DOC_PATH").toString();
				int nDocID=lstDocs.getDataRef(i,"BDMD_ID").toInt();
				int nDocType=lstDocs.getDataRef(i,"BDMD_DOC_TYPE").toInt();

				if (nDocType!=GlobalConstants::DOC_TYPE_INTERNET_FILE)
				{
					QMessageBox::warning(this,tr("Warning"),QString(tr("Only Internet File type of documents can be saved! Document: %1 not saved.")).arg(lstDocs.getDataRef(i,"BDMD_NAME").toString()));
					continue;
				}

				//DbRecordSet rowRev,rowCheckOut;
				//DocumentHelper::CheckOutDocument(nDocID,strDocPath,true,rowRev,rowCheckOut,false,false,false,strDir);
				g_DownloadManager->ScheduleCheckOutDocument(nDocID,strDocPath,true,false,false,false,strDir);
			}
		}
		break;
	case CommGridView::CNXT_CHECKOUT:
		{
			//open doc in app:
			DbRecordSet *pLstData=GetDocumentsRecordSet();
			DbRecordSet lstDocs=pLstData->getSelectedRecordSet();
			QString strDir=DocumentHelper::GetCheckOutDocumentDirectoryPath();
			if (strDir.isEmpty())
				return;
			int nSize=lstDocs.getRowCount();
			for(int i=0;i<nSize;i++){
				DbRecordSet row = lstDocs.getRow(i);
				DocumentHelper::OpenDocumentInExternalApp(row,false,strDir,0);
			}
		}
		break;
	case CommGridView::CNXT_LAODDOCUMENTS:
		{
			SubMenuRequestedNewData();
			g_CommManager.LoadDocuments();
		}
		break;

	default:
	    break;
	}




}

void CommWorkBenchWidget::OpenSplashScreen()
{
	if (m_bStartSplashScreen)
		g_pClientManager->StartProgressDialog(tr("Filter is being applied..."));
}

void CommWorkBenchWidget::PostPoneTasks(QDateTime datDueDateTime, QDateTime datStartDateTime)
{
	QString s = datDueDateTime.toString();
	QString ss = datStartDateTime.toString();

	DbRecordSet recTaskIDList;
	recTaskIDList.addColumn(QVariant::Int, "BTKS_ID");
	for (int i = 0; i < m_lstTaskList.size(); i++)
	{
		recTaskIDList.addRow();
		recTaskIDList.setData(i, "BTKS_ID", m_lstTaskList.at(i));
	}

	Status status;
	_SERVER_CALL(BusCommunication->PostPoneTasks(status, recTaskIDList, datDueDateTime, datStartDateTime))
	_CHK_ERR(status);

	ReloadData();
}
