#include "table_contactphone.h"
#include "dlg_phoneformatter.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "fui_collection/fui_collection/fui_contacttype.h"
#include "fui_collection/fui_collection/fui_contacts.h"
#include "menuitems.h"

#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "common/common/cliententitycache.h"
extern ClientEntityCache				g_ClientCache;				//global client cache


Table_ContactPhone::Table_ContactPhone(QWidget * parent)
:UniversalTableWidgetEx(parent),m_toolbar(NULL),m_pActMakeAct(NULL),m_pActOpenType(NULL)
{
}



//called after dark
void Table_ContactPhone::Initialize(DbRecordSet *plstData,toolbar_edit *pToolBar, QWidget *pContactFUI)
{
	BuildToolBar(pToolBar);

	m_pContactFUI=pContactFUI;

	m_TypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_PHONE);
	m_TypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_TypeHandler.ReloadData();

	//setup columns
	DbRecordSet columns;
	AddColumnToSetup(columns,"",tr("Type"),100,true, "",COL_TYPE_CUSTOM_WIDGET);
	AddColumnToSetup(columns,"BCMP_FULLNUMBER",tr("Phone"),140,true, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMP_NAME",tr("Name"),100,true, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMP_COUNTRY",tr("Country Predial"),100,true, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMP_AREA",tr("Area Predial"),100,true, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMP_LOCAL",tr("Local Number"),100,true, "",COL_TYPE_TEXT,"");
	//set data source for table, enable drag
	UniversalTableWidgetEx::Initialize(plstData,&columns);
	EnableDrag(true,ENTITY_BUS_CM_PHONE);
	CreateContextMenu();
	//store idx columns for faster access
	m_nTypeColIdx=m_plstData->getColumnIdx("BCMP_TYPE_ID");
	m_nDefaultColIdx=m_plstData->getColumnIdx("BCMP_IS_DEFAULT");
	m_nPhoneColIdx=m_plstData->getColumnIdx("BCMP_FULLNUMBER");
	m_nAreaColIdx=m_plstData->getColumnIdx("BCMP_AREA");
	m_nCuntryColIdx=m_plstData->getColumnIdx("BCMP_COUNTRY");
	m_nLocalColIdx=m_plstData->getColumnIdx("BCMP_LOCAL");

	connect(&m_TypeHandler,SIGNAL(SignalReloadCombo()),this,SLOT(OnComboDataReload()));
	connect(this,SIGNAL(SignalRowInserted(int)),this,SLOT(OnRowInserted(int)));
	connect(this,SIGNAL(SignalRowsDeleted()),this,SLOT(OnDeleted()));
}



//dummy: if inserted: blue icon, else red
void Table_ContactPhone::GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip)
{
	//if Default then show checked icon:
	if(m_plstData->getDataRef(nRow,m_nDefaultColIdx).toInt()!=0)
	{
		RowIcon.addFile(":checked.png");
		strStatusTip=tr("Default Phone");
	}
	else
		RowIcon=QIcon(); //empty icon
}


//create custom cntx menu at end of existing one:
void Table_ContactPhone::CreateContextMenu()
{
	QList<QAction*> lstActions;
	CreateDefaultContextMenuActions(lstActions);

	//Add separator
	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.prepend(SeparatorAct);
	//----------
	m_pActMakeAct = new QAction(tr("Make Phone Call"), this);
	m_pActMakeAct->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(m_pActMakeAct, SIGNAL(triggered()), this, SLOT(OnMakeAction()));
	lstActions.prepend(m_pActMakeAct);
	//----------
	m_pActOpenType = new QAction(tr("Create/Edit Phone Type"), this);
	m_pActOpenType->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(m_pActOpenType, SIGNAL(triggered()), this, SLOT(OnOpenTypeFUI()));
	lstActions.prepend(m_pActOpenType);

	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.prepend(SeparatorAct);
	//set as def
	QAction* pAction = new QAction(tr("&Set as Default"), this);
	pAction->setIcon(QIcon(":checked.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnSetDefault()));
	lstActions.prepend(pAction);
	//format
	pAction = new QAction(tr("Format Phone Number"), this);
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnFormatPhoneNumber()));
	lstActions.prepend(pAction);



	SetContextMenuActions(lstActions);
}

//invoked by cnt menu, set default flag on selected row
void Table_ContactPhone::OnSetDefault()
{
	int nRow=m_plstData->getSelectedRow();
	if(nRow!=-1)
	{
		//set default flag:
		m_plstData->setColValue(m_nDefaultColIdx,0);
		m_plstData->setData(nRow,m_nDefaultColIdx,1);
		RefreshDisplay();
	}
}

//cell edited:
void Table_ContactPhone::OnCellEdited(int nRow, int nCol)
{

	//process upper stream:
	UniversalTableWidgetEx::OnCellEdited(nRow,nCol);
	nCol=m_lstColumnMapping[nCol];	//col inside recordset
	QString strPhone,strArea,strCountry,strSearch,strISO,strLocal;

	//check if format allowed: get type
	int nPhoneTypeID=m_plstData->getDataRef(nRow,"BCMP_TYPE_ID").toInt();
	if (nPhoneTypeID!=-1)
	{
		DbRecordSet *plstTypes=m_TypeHandler.GetDataSource();
		if (plstTypes==NULL)
			return;
		int nPhoneTypeRow=plstTypes->find(0,nPhoneTypeID,true);
		if (nPhoneTypeRow!=-1)
		{
			int nAllowed=plstTypes->getDataRef(nPhoneTypeRow,"BCMT_IS_FORMATTING_ALLOWED").toInt();
			if (nAllowed==0)
			{
				//just generate search string:
				m_plstData->setData(nRow,"BCMP_SEARCH",m_plstData->getDataRef(nRow,m_nPhoneColIdx).toString());
				return;
			}
		}
			
	}

	//if type is allowed for formatting, split phone
	if(nCol==m_nPhoneColIdx)
	{
		m_FormatPhone.SplitPhoneNumber(m_plstData->getDataRef(nRow,nCol).toString(),strCountry,strArea,strLocal,strSearch,strISO);
		m_plstData->setData(nRow,"BCMP_AREA",strArea);
		m_plstData->setData(nRow,"BCMP_COUNTRY",strCountry);
		m_plstData->setData(nRow,"BCMP_LOCAL",strLocal);
		m_plstData->setData(nRow,"BCMP_SEARCH",strSearch);
		m_plstData->setData(nRow,"BCMP_COUNTRY_ISO",strISO);
		RefreshDisplay(nRow);
	}


	//if edited, then format back:
	if(nCol==m_nAreaColIdx || nCol==m_nCuntryColIdx || nCol==m_nLocalColIdx)
	{
		strPhone=m_FormatPhone.FormatPhoneNumber(m_plstData->getDataRef(nRow,"BCMP_COUNTRY").toString(),m_plstData->getDataRef(nRow,"BCMP_AREA").toString(),m_plstData->getDataRef(nRow,"BCMP_LOCAL").toString());
		m_plstData->setData(nRow,m_nPhoneColIdx,strPhone);
		RefreshDisplay(nRow);
	}

}

void Table_ContactPhone::OnFormatPhoneNumber()
{

	QString strPhone,strArea,strCountry,strSearch,strISO,strLocal;
	Dlg_PhoneFormatter dlg;

	//determine row:
	int nRow=m_plstData->getSelectedRow();
	if(nRow==-1 || m_plstData->getSelectedCount()>1)
		return;

	//check if format allowed: get type
	int nPhoneTypeID=m_plstData->getDataRef(nRow,"BCMP_TYPE_ID").toInt();
	if (nPhoneTypeID!=-1)
	{
		DbRecordSet *plstTypes=m_TypeHandler.GetDataSource();
		if (plstTypes==NULL)
			return;
		int nPhoneTypeRow=plstTypes->find(0,nPhoneTypeID,true);
		if (nPhoneTypeRow!=-1)
		{
			int nAllowed=plstTypes->getDataRef(nPhoneTypeRow,"BCMT_IS_FORMATTING_ALLOWED").toInt();
			if (nAllowed==0)
			{
				//just generate search string:
				QMessageBox::warning(this,tr("Warning"),tr("Can not format phone of this type!"));
				return;
			}
		}

	}
	

	dlg.SetRow(m_plstData->getDataRef(nRow,m_nPhoneColIdx).toString(),m_plstData->getDataRef(nRow,"BCMP_COUNTRY_ISO").toString());
	int nRes=dlg.exec();
	if (nRes)
	{
		dlg.GetRow(strPhone,strCountry,strArea,strLocal,strSearch,strISO);
		m_plstData->setData(nRow,m_nPhoneColIdx,strPhone);
		m_plstData->setData(nRow,"BCMP_AREA",strArea);
		m_plstData->setData(nRow,"BCMP_COUNTRY",strCountry);
		m_plstData->setData(nRow,"BCMP_LOCAL",strLocal);
		m_plstData->setData(nRow,"BCMP_SEARCH",strSearch);
		m_plstData->setData(nRow,"BCMP_COUNTRY_ISO",strISO);
		RefreshDisplay(nRow);
	}
}


void Table_ContactPhone::BuildToolBar(toolbar_edit * pToolBar)
{
	m_toolbar=pToolBar;
	pToolBar->GetBtnAdd()->setText(tr("Add New Phone"));
	connect(pToolBar->GetBtnAdd(), SIGNAL(clicked()), this, SLOT(InsertRow()));
	pToolBar->GetBtnSelect()->setVisible(false);
	pToolBar->GetBtnModify()->setVisible(false);
	connect(pToolBar->GetBtnClear(), SIGNAL(clicked()), this, SLOT(DeleteSelection()));
}



void Table_ContactPhone::SetEditMode(bool bEdit)
{
	UniversalTableWidgetEx::SetEditMode(bEdit);
	if(m_toolbar)m_toolbar->setEnabled(bEdit);
	
	//always on:
	if (m_pActMakeAct)m_pActMakeAct->setEnabled(true);
	if (m_pActOpenType)m_pActOpenType->setEnabled(true);
}


void Table_ContactPhone::Data2CustomWidget(int nRow, int nCol)
{
	QComboBox *pComboType = dynamic_cast<QComboBox *>(cellWidget(nRow,nCol));
	if (!pComboType)
	{
		pComboType = new QComboBox;
		pComboType->setEditable(false);
		pComboType->setProperty("combo_row",nRow); //to identify sender
		connect(pComboType,SIGNAL(currentIndexChanged(int)),this,SLOT(OnComboIndexChanged(int)));  //connect on change signals:
		m_TypeHandler.ReloadCombo(pComboType);
		pComboType->setEnabled(m_bEdit);
		setCellWidget(nRow,nCol,pComboType);
	}
	m_TypeHandler.SetComboSelection(pComboType,m_plstData->getDataRef(nRow,m_nTypeColIdx));
}

void Table_ContactPhone::OnComboIndexChanged(int nIndex)
{
	QComboBox *pComboType = dynamic_cast<QComboBox *>(sender());
	if (pComboType)
	{
		int nRow=pComboType->property("combo_row").toInt();
		Q_ASSERT(nRow>=0 && nRow <m_plstData->getRowCount()); //row must be inside data
		m_plstData->setData(nRow,"BCMP_TYPE_ID",pComboType->itemData(pComboType->currentIndex()));
		OnCustomWidgetCellClicked(nRow,0);
	}
}
//when data is changed from cache or new type added: reload all combo's
void Table_ContactPhone::OnComboDataReload()
{
	if (!m_plstData)
		return;

	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		QComboBox *pComboType = dynamic_cast<QComboBox *>(cellWidget(i,0)); //col type is first
		if (pComboType)
		{
			m_TypeHandler.ReloadCombo(pComboType);
			m_TypeHandler.SetComboSelection(pComboType,m_plstData->getDataRef(i,m_nTypeColIdx));
		}
	}
}


void Table_ContactPhone::OnRowInserted(int nRow)
{
	m_plstData->setData(nRow,"BCMP_ID",0);//reset ID to 0, if copy
	if(m_plstData->getDataRef(nRow,"BCMP_TYPE_ID").isNull())	//only if empty
		m_plstData->setData(nRow,"BCMP_TYPE_ID",ContactTypeManager::GetDefaultTypeFromCache(ContactTypeManager::TYPE_PHONE,g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES)));	//reset to default

	RefreshDefaultFlag();
}

void Table_ContactPhone::CheckDataBeforeWrite(Status &Ret_pStatus)
{
	int nTestCol=m_plstData->getColumnIdx("BCMP_FULLNUMBER");
	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if(m_plstData->getDataRef(i,nTestCol).toString().isEmpty())
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Phone number is mandatory field!"));	
			return;	
		}
	}

	//re-arrange all phones-> set search string, ISO code:
	//int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strPhone,strCountry,strLocal,strISO,strArea,strSearch;
		m_FormatPhone.SplitPhoneNumber(m_plstData->getDataRef(i,"BCMP_FULLNUMBER").toString(),strCountry,strArea,strLocal,strSearch,strISO);
		m_plstData->setData(i,"BCMP_SEARCH",strSearch);
		m_plstData->setData(i,"BCMP_COUNTRY_ISO",strISO);
	}
}


//when deleting row, make sure to have at least one flag
void Table_ContactPhone::RefreshDefaultFlag()
{
	int nCountDefs=m_plstData->countColValue(m_nDefaultColIdx,1);
	if(nCountDefs==0 && m_plstData->getRowCount()>0)				//set first to be default if default is deleted!
		m_plstData->setData(0,m_nDefaultColIdx,1);
	else if (nCountDefs>1) //if default copied
	{
		int nRowFirstDef=m_plstData->find(m_nDefaultColIdx,1,true);
		if (nRowFirstDef>=0)
		{
			m_plstData->setColValue(m_nDefaultColIdx,0);
			m_plstData->setData(nRowFirstDef,m_nDefaultColIdx,1);
		}
	}
}

void Table_ContactPhone::OnDeleted()
{
	RefreshDefaultFlag();
}


QString Table_ContactPhone::GetDefaultEntry()
{
	int nRow=m_plstData->find(m_nDefaultColIdx,1,true);
	if (nRow!=-1)
		return m_plstData->getDataRef(nRow,"BCMP_SEARCH").toString();
	return "";
}


void Table_ContactPhone::ClearEmptyEntries()
{
	m_plstData->clearSelection();
	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if(m_plstData->getDataRef(i,"BCMP_FULLNUMBER").toString().isEmpty())
			m_plstData->selectRow(i);
	}
	DeleteSelection();
}

void Table_ContactPhone::OnMakeAction()
{
	//determine row:
	int nRow=m_plstData->getSelectedRow();
	if(nRow==-1 || m_plstData->getSelectedCount()>1)
		return;

	//
	QString strPhoneNumber=m_plstData->getDataRef(nRow,"BCMP_SEARCH").toString();
	
	dynamic_cast<FUI_Contacts*>(m_pContactFUI)->m_p_CeMenu->SetDefaultPhone(strPhoneNumber);
	dynamic_cast<FUI_Contacts*>(m_pContactFUI)->m_p_CeMenu->OnPhoneCall();

}
void Table_ContactPhone::OnOpenTypeFUI()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CONTACT_TYPES,true,false,FuiBase::MODE_READ,-1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_ContactType* pFuiW=dynamic_cast<FUI_ContactType*>(pFUI);
	if (pFuiW)
	{
		pFuiW->on_selectionChange(ContactTypeManager::TYPE_PHONE);
		pFuiW->on_cmdEdit();
	}
	pFUI->show();
}
