#ifndef CALENDAREVENT_BREAKTABLE_H
#define CALENDAREVENT_BREAKTABLE_H

#include "gui_core/gui_core/universaltablewidgetex.h"
#include "table_contactaddresstypes.h"
#include "toolbar_edit.h"
#include "common/common/status.h"

class CalendarEvent_BreakTable : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	CalendarEvent_BreakTable(QWidget *parent);

	void Initialize(DbRecordSet *plstData,toolbar_edit *pToolBar);
	void SetEditMode(bool bEdit=true);

public slots:
	void RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);

private slots:
	void RecalcDurations();
	void OnFromFocusOutSignal();

protected:
	void Data2CustomWidget(int nRow, int nCol);
	void CustomWidget2Data(int nRow, int nCol);
	void GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip);	

private:
	void BuildToolBar(toolbar_edit* pToolBar);
	toolbar_edit *m_toolbar;
	
};

#endif // CALENDAREVENT_BREAKTABLE_H
