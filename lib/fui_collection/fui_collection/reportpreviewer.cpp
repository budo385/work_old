#include "reportpreviewer.h"

#include "contactselection.h"
#include "emaildialog.h"
#include "fuimanager.h"

#include "wizardregistration.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QPrintDialog>
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
extern QString g_strLastDir_FileOpen;

ReportPreviewer::ReportPreviewer(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	ui.OpenButton->setIcon(QIcon(":openfile.png"));
	ui.OpenButton->setToolTip(tr("Open"));
	ui.OpenButton->setShortcut(Qt::CTRL+Qt::Key_O);
	
	ui.PrintButton->setIcon(QIcon(":print.png"));
	ui.PrintButton->setToolTip(tr("Print"));
	ui.PrintButton->setShortcut(Qt::CTRL+Qt::Key_P);
	ui.PrintButton->setDisabled(true);
	
	ui.PrintPdfButton->setIcon(QIcon(":pdf_icon.png"));
	ui.PrintPdfButton->setToolTip(tr("Print to PDF"));
	ui.PrintPdfButton->setDisabled(true);
	
	ui.FirstPageButton->setIcon(QIcon(":firstpage.png"));
	ui.FirstPageButton->setToolTip(tr("Go to first page"));
	ui.FirstPageButton->setShortcut(Qt::CTRL+Qt::Key_Home);
	ui.FirstPageButton->setDisabled(true);
	
	ui.PreviousPageButton->setIcon(QIcon(":prevpage.png"));
	ui.PreviousPageButton->setToolTip(tr("Go to previous page"));
	ui.PreviousPageButton->setShortcut(Qt::CTRL+Qt::Key_Left);
	ui.PreviousPageButton->setDisabled(true);
	
	ui.NextPageButton->setIcon(QIcon(":nextpage.png"));
	ui.NextPageButton->setToolTip(tr("Go to next page"));
	ui.NextPageButton->setShortcut(Qt::CTRL+Qt::Key_Right);
	ui.NextPageButton->setDisabled(true);
	
	ui.LastPageButton->setIcon(QIcon(":lastpage.png"));
	ui.LastPageButton->setToolTip(tr("Go to last page"));
	ui.LastPageButton->setShortcut(Qt::CTRL+Qt::Key_End);
	ui.LastPageButton->setDisabled(true);

	ui.GoToPageButton->setIcon(QIcon(":gotopage.png"));
	ui.GoToPageButton->setToolTip(tr("Go to specified page"));
	ui.GoToPageButton->setShortcut(Qt::CTRL+Qt::Key_G);
	ui.GoToPageButton->setDisabled(true);

	ui.CloseReportButton->setIcon(QIcon(":close.png"));
	ui.CloseReportButton->setToolTip(tr("Close report"));
	ui.CloseReportButton->setDisabled(true);

	m_strPdfFileName = "";
	m_nCurrentPicture = 0;
	m_nScaleFactor = 1;
	m_PictureLabel = new QLabel;
	m_PictureLabel->setBackgroundRole(QPalette::Base);
	m_PictureLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	m_PictureLabel->setScaledContents(true);

	//Set label as scroll widget.
	ui.ScrollArea->setBackgroundRole(QPalette::Dark);
	ui.ScrollArea->setWidget(m_PictureLabel);
/*	ui.ScrollArea->setFrameShape(QFrame::Panel);
	ui.ScrollArea->setFrameStyle(QFrame::Sunken);
	ui.ScrollArea->setLineWidth(2);
*/
	//Open selecting wizard on startup.
	RefreshFuiName();
	on_OpenButton_clicked();
}

ReportPreviewer::~ReportPreviewer()
{
	delete(m_PictureLabel);
}

QString ReportPreviewer::GetFUIName()
{
	return QString(tr("Reporting"));
}

void ReportPreviewer::PrintPreview()
{
#ifndef WINCE
	//Set current picture to be the first.
	m_nCurrentPicture = 0;

	//Send to preview widget to print.
	RptPrinter Rpt;
	//if (!Rpt.InitalizeReport(m_strReportFileName, m_lstPageRecordSet))
	//	return;
	
	QSize PreviewPageSize = Rpt.GetReportPreviewPageSize();

	//Set member vars.
	bool hasMorePages = true;

	//While has more pages
	while (hasMorePages)
	{
		//QPicture *pix = new QPicture;
		QPixmap *pix = new QPixmap(PreviewPageSize);
		pix->fill();
		hasMorePages = Rpt.printReportPage(pix);
		m_lstPictureList << pix;
	}

	//Set first picture on label and adjust size (if you don't adjust size, you won't see a thing).
	//m_PictureLabel->setPicture(*m_lstPictureList.value(m_nCurrentPicture));
	m_PictureLabel->setPixmap(*m_lstPictureList.value(m_nCurrentPicture));
	m_PictureLabel->adjustSize();

	ui.PrintButton->setDisabled(false);
	ui.PrintPdfButton->setDisabled(false);
	ui.FirstPageButton->setDisabled(false);
	ui.NextPageButton->setDisabled(false);
	ui.PreviousPageButton->setDisabled(false);
	ui.LastPageButton->setDisabled(false);
	ui.GoToPageButton->setDisabled(false);
	ui.CloseReportButton->setDisabled(false);
#endif //WINCE
}

void ReportPreviewer::DeleteCurrentReport()
{
	if (!m_lstPictureList.isEmpty())
	{
		qDeleteAll(m_lstPictureList);
		m_lstPictureList.clear();
	}
}

void ReportPreviewer::on_OpenButton_clicked()
{
	WizardRegistration wizReg(WizardRegistration::REPORT_WIZARD);
	WizardBase wiz(WizardRegistration::REPORT_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);
	
	int h = size().height();
	int w = size().width();
	wiz.resize(w*.80, h*.80);
	
	//If wizard accepted.
	if (wiz.exec())
		wiz.GetWizPageRecordSetList(m_lstPageRecordSet);
	else return;

	QDir dir(QCoreApplication::applicationDirPath()); 
	QString strDir;

	DbRecordSet selReport = m_lstPageRecordSet.first();
	m_strReportFileName = selReport.getDataRef(0, 0).toString() + ".xml";


#ifdef _DEBUG
	{
		dir.cdUp();
		dir.cdUp();
		dir.cdUp();
		strDir = dir.absolutePath();
		m_strReportFileName = strDir + "/lib/report_definitions/report_definitions/" + m_strReportFileName;
	}
#else
	{
		strDir = dir.absolutePath();
		m_strReportFileName = strDir + "/report_definitions/" + m_strReportFileName;
	}
#endif

	//QMessageBox::information(NULL,m_strReportFileName,m_strReportFileName);

	if (m_strReportFileName.isNull())
		return;

	//Delete previous pages of report then print.
	DeleteCurrentReport();

	DbRecordSet PrintDestinationRecordSet = m_lstPageRecordSet.last();
	int Destination = PrintDestinationRecordSet.getDataRef(0, 0).toInt();
	//PDF
	if (Destination == 2)
	{
		on_PrintPdfButton_clicked();
	}
	else if (Destination == 1)
	{
		on_PrintButton_clicked();
	}
	else
		PrintPreview();
}

void ReportPreviewer::on_PrintButton_clicked()
{
#ifndef WINCE
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	QPrinter *printer = new QPrinter();
	QPrintDialog print_dialog(printer, this);
	if (print_dialog.exec() == QPrintDialog::Accepted)
		printer->setFullPage(true);
	else
	{
		QApplication::restoreOverrideCursor();
		return;
	}

	RptPrinter Rpt;
	//if (!Rpt.InitalizeReport(m_strReportFileName, m_lstPageRecordSet))
	//	return;
	Rpt.PrintReportToPrinter(printer);

	QApplication::restoreOverrideCursor();
#endif //WINCE
}

void ReportPreviewer::on_PrintPdfButton_clicked()
{
#ifndef WINCE
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	m_strPdfFileName = QFileDialog::getSaveFileName(this, "Enter PDF file name", g_strLastDir_FileOpen, "PDF files (*.pdf)");
	if (m_strPdfFileName.isNull())
	{
		QApplication::restoreOverrideCursor();
		return;
	}
	if (!m_strPdfFileName.isEmpty())
	{
		QFileInfo fileInfo(m_strPdfFileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}

	//Check file extension.
	int extensionIndex = m_strPdfFileName.lastIndexOf(".");
	QString extension = m_strPdfFileName;
	extension.remove(0, extensionIndex);
	if (extension != ".pdf")
		m_strPdfFileName.append(".pdf");

	//PRINT TO PDF.
	RptPrinter Rpt;
	//if (!Rpt.InitalizeReport(m_strReportFileName, m_lstPageRecordSet))
	//	return;

	QPrinter *printer = new QPrinter();
	printer->setOutputFormat(QPrinter::PdfFormat);
	//printer->setPageSize(QPrinter::A4);
	//printer->setOrientation(QPrinter::Landscape);
	printer->setFullPage(true);
	printer->setOutputFileName(m_strPdfFileName);	

	Rpt.PrintReportToPrinter(printer);

	QApplication::restoreOverrideCursor();
#endif //WINCE
}

void ReportPreviewer::on_FirstPageButton_clicked()
{
	m_nCurrentPicture = 0;
	//m_PictureLabel->setPicture(*m_lstPictureList.value(m_nCurrentPicture));
	m_PictureLabel->setPixmap(*m_lstPictureList.value(m_nCurrentPicture));
}

void ReportPreviewer::on_PreviousPageButton_clicked()
{
	if(m_nCurrentPicture > 0)
		m_nCurrentPicture--;
	else
		return;

	//m_PictureLabel->setPicture(*m_lstPictureList.value(m_nCurrentPicture));
	m_PictureLabel->setPixmap(*m_lstPictureList.value(m_nCurrentPicture));
}

void ReportPreviewer::on_NextPageButton_clicked()
{
	if(m_nCurrentPicture < m_lstPictureList.count() -1 )
		m_nCurrentPicture++;
	else
		return;

	//m_PictureLabel->setPicture(*m_lstPictureList.value(m_nCurrentPicture));
	m_PictureLabel->setPixmap(*m_lstPictureList.value(m_nCurrentPicture));
}

void ReportPreviewer::on_LastPageButton_clicked()
{
	m_nCurrentPicture = m_lstPictureList.count() - 1;
	//m_PictureLabel->setPicture(*m_lstPictureList.value(m_nCurrentPicture));
	m_PictureLabel->setPixmap(*m_lstPictureList.value(m_nCurrentPicture));
}

void ReportPreviewer::on_GoToPageButton_clicked()
{
	bool Ok = false;
	int PictureNumber = QInputDialog::getInt( this, tr( "Go to page" ), tr( "Page number:" ), 1, 1, m_lstPictureList.count(), 1, &Ok );
	if (Ok) 
	{
		m_nCurrentPicture = PictureNumber - 1;
		//m_PictureLabel->setPicture(*m_lstPictureList.value(m_nCurrentPicture));
		m_PictureLabel->setPixmap(*m_lstPictureList.value(m_nCurrentPicture));
	}
	else
		return;
}

void ReportPreviewer::on_CloseReportButton_clicked()
{
	DeleteCurrentReport();

	m_PictureLabel->clear();
	m_PictureLabel->adjustSize();

	ui.PrintButton->setDisabled(true);
	ui.PrintPdfButton->setDisabled(true);
	ui.FirstPageButton->setDisabled(true);
	ui.NextPageButton->setDisabled(true);
	ui.PreviousPageButton->setDisabled(true);
	ui.LastPageButton->setDisabled(true);
	ui.GoToPageButton->setDisabled(true);
	ui.CloseReportButton->setDisabled(true);
}