#include "os_specific/os_specific/mapimanager.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "fui_importemail.h"
#include "gui_core/gui_core/gui_helper.h"
#include <QInputDialog>
#include <QtWidgets/QMessageBox>
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/datahelper.h"
#include "bus_core/bus_core/globalconstants.h"
#include "communicationmanager.h"
#include "qcw_base.h"
#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#include "os_specific/os_specific/mailmanager.h"
#include "emaildialog.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/accuarcore.h"

//GLOBALS:
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#ifndef min
#define min(a,b) (((a)<(b))? (a):(b))
#endif

#define MAX_ATTACHMENT_LIST_SIZE 16777216

QMap<QString, int> FUI_ImportEmail::m_mapEmail2Contact;
QMutex g_mutexEmail2Cont;

typedef bool (*FN_FILTER_MATCH_ROW)(unsigned long nCallerObject,int nRow);
bool Call_FilterMatchRow(unsigned long nCallerObject,int nRow)
{
	FUI_ImportEmail *pDlg = (FUI_ImportEmail *)nCallerObject;
	Q_ASSERT(NULL != pDlg);
	if(pDlg)
	{
		return pDlg->FilterMatchRow(nRow);
	}
	return false;
}


FUI_ImportEmail::FUI_ImportEmail(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	ui.label1->setStyleSheet("* {font-weight:800; font-family:MS Shell Dlg 2; font-size:12pt}");

	ui.radNoFilter->setChecked(true);
	EnableFilterWidgets(false);

	m_bORMode=true;
	ui.radOR->setChecked(true);

	ui.txtFilterFromDate->setDate(QDate::currentDate());
	ui.txtFilterToDate->setDate(QDate::currentDate());

	ui.cboImportSource->addItem(tr("Outlook"));
	ui.cboImportSource->addItem(tr("Thunderbird"));

	//define list:
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	m_lstData.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
	m_lstData.copyDefinition(set, false);
	m_lstData.addColumn(QVariant::String, "PROJECT_NAME");
	m_lstData.addColumn(QVariant::String, "PROJECT_CODE");
	m_lstData.addColumn(QVariant::String, "EMAIL_UNASSIGNED");
	m_lstData.addColumn(DbRecordSet::GetVariantType(), "ATTACHMENTS");
	m_lstData.addColumn(DbRecordSet::GetVariantType(), "CONTACTS");
	m_lstData.addColumn(DbRecordSet::GetVariantType(), "UAR_TABLE");
	m_lstData.addColumn(DbRecordSet::GetVariantType(), "GAR_TABLE");
	m_lstData.addColumn(QVariant::Int, "ATTACHMENTS_SIZE");
	

	ui.tableData->Initialize(&m_lstData, true);
	ui.tableData->SetEditMode(true);
	connect(ui.tableData,SIGNAL(SignalSelectionChanged()),this,SLOT(OnTableDataSelectionChanged()));
	ui.tableData->m_pParent = this;
	
	ui.frameProject->Initialize(ENTITY_BUS_PROJECT);
	ui.frameProject->registerObserver(this);

	ui.frameContact->Initialize(ENTITY_BUS_CONTACT);
	ui.frameContact->EnableInsertButton();
	ui.frameContact->registerObserver(this);
	connect(ui.tableData, SIGNAL(SignalRefreshBoldRows()), this, SLOT(OnSignalRefreshBoldRows()));



	//define column headers
	DbRecordSet columns;
	ui.tableData->AddColumnToSetup(columns,"BEM_FROM",tr("From"),100,false, "");
	ui.tableData->AddColumnToSetup(columns,"BEM_TO",tr("To"),100,false, "");
	ui.tableData->AddColumnToSetup(columns,"BEM_SUBJECT",tr("Subject"),180,false, "");
	ui.tableData->AddColumnToSetup(columns,"BEM_RECV_TIME",tr("Received/Sent"),150,false, "");
	ui.tableData->AddColumnToSetup(columns,"PROJECT_NAME",tr("Projects"),180,false, "");
	ui.tableData->AddColumnToSetup(columns,"EMAIL_UNASSIGNED",tr("Unassigned"),180,false, "");
	ui.tableData->SetColumnSetup(columns);

	GUI_Helper::SetStyledButtonColorBkg(ui.btnBuildList, GUI_Helper::BTN_COLOR_YELLOW);
	GUI_Helper::SetStyledButtonColorBkg(ui.btnImportData, GUI_Helper::BTN_COLOR_YELLOW);

	//load outlook folder selection setting
	QByteArray  arData = g_pSettings->GetPersonSetting(IMPORT_EMAIL_OUTLOOK_FOLDERS).toByteArray();
	OutlookFolderPickerDlg::Binary2FolderList(arData, m_lstFolders);
	int nCnt = m_lstFolders.count();
	if(0 == nCnt)
	{
		//fill default folders
		QStringList lstSub;
		lstSub.append("Inbox");
		m_lstFolders.append(lstSub);

		lstSub.clear();
		lstSub.append("Sent Items");
		m_lstFolders.append(lstSub);

		lstSub.clear();
		lstSub.append("Posteingang");
		m_lstFolders.append(lstSub);

		lstSub.clear();
		lstSub.append("Gesendete Objekte");
		m_lstFolders.append(lstSub);
	}

	arData = g_pSettings->GetPersonSetting(IMPORT_EMAIL_THUNDERBIRD_FOLDERS).toByteArray();
	OutlookFolderPickerDlg::Binary2FolderList(arData, m_lstFoldersThunder);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - constructor done"));
}

FUI_ImportEmail::~FUI_ImportEmail()
{
	m_mapEmail2Contact.clear(); //FIX: this fixes problem of stale data in the cache

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - destructor"));
}

void FUI_ImportEmail::on_btnSelectSourceFolders_clicked()
{
	bool bThunderbird = false;
	if(1 == ui.cboImportSource->currentIndex())
		bThunderbird = true;

	OutlookFolderPickerDlg dlg(bThunderbird);
	dlg.SetSelection((bThunderbird)? m_lstFoldersThunder : m_lstFolders);
	if(dlg.exec())
	{
		QByteArray arData;
			if(bThunderbird)
			{
				m_lstFoldersThunder = dlg.GetSelection();
				if(dlg.StoreSettings(arData))
					g_pSettings->SetPersonSetting(IMPORT_EMAIL_THUNDERBIRD_FOLDERS, arData);
			}
			else
			{
				m_lstFolders = dlg.GetSelection();
				if(dlg.StoreSettings(arData))
					g_pSettings->SetPersonSetting(IMPORT_EMAIL_OUTLOOK_FOLDERS, arData);
			}
	}
}


void FUI_ImportEmail::OnTableDataSelectionChanged()
{
	//TOFIX set project code for multiple selected table data rows ? (or empty if all rows do not have same project)
	//ui.frameProject->SetCurrentRow(m_lstData.getSelectedRow());
	//ui.frameProject->RefreshDisplay();
}

void FUI_ImportEmail::EnableFilterWidgets(bool bEnable)
{
	ui.txtFilterFromDate		->setEnabled(bEnable);
	ui.txtFilterToDate			->setEnabled(bEnable);
	ui.txtFilterFromField		->setEnabled(bEnable);
	ui.txtFilterToField			->setEnabled(bEnable);
	ui.txtFilterCCField			->setEnabled(bEnable);
	ui.txtFilterBCCField		->setEnabled(bEnable);
	ui.txtFilterSubjectField	->setEnabled(bEnable);
	ui.radOR					->setEnabled(bEnable);
	ui.radAND					->setEnabled(bEnable);
}

void FUI_ImportEmail::on_radNoFilter_toggled(bool bChecked)
{
	EnableFilterWidgets(!bChecked);
}

void FUI_ImportEmail::on_radUseFilter_toggled(bool bChecked)
{
	EnableFilterWidgets(bChecked);
}

bool FUI_ImportEmail::FilterMatchRow(int nIdx)
{
	if(!m_bUseFilter)
		return true;

	//match date
	QDateTime dtSentRecv = m_lstData.getDataRef(nIdx, "BEM_RECV_TIME").toDateTime();
	if(dtSentRecv.isValid()){
		if(m_dateFilterFromDate.isValid())
			if(dtSentRecv.date() < m_dateFilterFromDate)
				return false;	//no match
		if(m_dateFilterToDate.isValid())
			if(dtSentRecv.date() > m_dateFilterToDate)
				return false;	//no match
	}

	int nMatchFieldsCnt = 0;
	int nEmptyFieldsCnt = 0;

	if(!m_strFilterFromField.isEmpty()){
		QString strField = m_lstData.getDataRef(nIdx, "BEM_FROM").toString();
		if(strField.indexOf(m_strFilterFromField, 0, Qt::CaseInsensitive) < 0){
			if(!m_bORMode)	return false;	// no match
		}
		else
			nMatchFieldsCnt ++;
	}
	else
		nEmptyFieldsCnt++;
	if(!m_strFilterToField.isEmpty()){
		QString strField = m_lstData.getDataRef(nIdx, "BEM_TO").toString();
		if(strField.indexOf(m_strFilterToField, 0, Qt::CaseInsensitive) < 0){
			if(!m_bORMode)	return false;	// no match
		}
		else
			nMatchFieldsCnt ++;
	}
	else
		nEmptyFieldsCnt++;
	if(!m_strFilterCCField.isEmpty()){
		QString strField = m_lstData.getDataRef(nIdx, "BEM_CC").toString();
		if(strField.indexOf(m_strFilterCCField, 0, Qt::CaseInsensitive) < 0){
			if(!m_bORMode)	return false;	// no match
		}
		else
			nMatchFieldsCnt ++;
	}
	else
		nEmptyFieldsCnt++;
	if(!m_strFilterBCCField.isEmpty()){
		QString strField = m_lstData.getDataRef(nIdx, "BEM_BCC").toString();
		if(strField.indexOf(m_strFilterBCCField, 0, Qt::CaseInsensitive) < 0){
			if(!m_bORMode)	return false;	// no match
		}
		else
			nMatchFieldsCnt ++;
	}
	else
		nEmptyFieldsCnt++;
	if(!m_strFilterSubjectField.isEmpty()){
		QString strField = m_lstData.getDataRef(nIdx, "BEM_SUBJECT").toString();
		if(strField.indexOf(m_strFilterSubjectField, 0, Qt::CaseInsensitive) < 0){
			if(!m_bORMode)	return false;	// no match
		}
		else
			nMatchFieldsCnt ++;
	}
	else
		nEmptyFieldsCnt++;

	//when using OR mode at least one of "from" "to" "CC" "BCC" "subject" fields must match
	if(m_bORMode && (nMatchFieldsCnt < 1) && (nEmptyFieldsCnt < 5))
		return false;

	return true;	// match, survived all tests
}

void FUI_ImportEmail::on_btnBuildList_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - build list operation"));

	//refresh filter
	m_bUseFilter = ui.radUseFilter->isChecked();
	if(m_bUseFilter)
	{
		m_bORMode			 = ui.radOR->isChecked();
		m_dateFilterFromDate = ui.txtFilterFromDate->date();
		m_dateFilterToDate	 = ui.txtFilterToDate->date();
		m_strFilterFromField = ui.txtFilterFromField->text();
		m_strFilterToField	 = ui.txtFilterToField->text();
		m_strFilterCCField	 = ui.txtFilterCCField->text();
		m_strFilterBCCField	 = ui.txtFilterBCCField->text();
		m_strFilterSubjectField	 = ui.txtFilterSubjectField->text();
	}

	if(0 == ui.cboImportSource->currentIndex())
	{
#ifdef _WIN32
		int nEmailsImported;
		QSet<QString> setEmails;
		MapiManager::ImportEmails(m_lstData,m_lstFolders,nEmailsImported,setEmails,Call_FilterMatchRow,(unsigned long)this, MapiManager::InitGlobalMapi());
		m_lstData.selectAll();
		OnGridFilledRecalculate(setEmails);
		ui.tableData->RefreshDisplay();
#endif

	}
	else if(1 == ui.cboImportSource->currentIndex())
	{
		//
		// Thunderbird email
		//
		if(!MailManager::IsThunderbirdInstalled()){
			QMessageBox::information(NULL, "Info", tr("Thunderbird email client is not installed!"));
			return;
		}

		int nFoldersCnt = m_lstFoldersThunder.count();
		if(nFoldersCnt < 1){
			QMessageBox::information(this, "Error", tr("There are no folders selected!"));
			return;
		}


		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

		QSet<QString> setEmails;
		MailManager::GetThunderbirdMails(m_lstData,m_lstFoldersThunder,setEmails,false,Call_FilterMatchRow,(unsigned long)this);

		m_lstData.selectAll();
		OnGridFilledRecalculate(setEmails);
		ui.tableData->RefreshDisplay();

		QApplication::restoreOverrideCursor();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - build list operation done"));
}

void FUI_ImportEmail::ImportEmails(Status status, DbRecordSet &lstData, int &nSkippedOrReplacedCnt, bool bAddDupsAsNewRow, bool bSkipExisting, bool bSkipProgress)
{
	qDebug() << "FUI_ImportEmail::ImportEmails";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - import operation"));

	//
	// import process
	//
	int nSize = lstData.getRowCount();
	if(nSize < 1) return;

	QProgressDialog *pDlg = NULL;
	if (!bSkipProgress)
	{
		QSize size(300,100);
		pDlg = new QProgressDialog(tr("Importing Emails..."),tr("Cancel"),0,nSize+30,NULL);
		pDlg->setWindowTitle(tr("Operation In Progress"));
		pDlg->setMinimumSize(size);
		pDlg->setWindowModality(Qt::WindowModal);
		pDlg->setMinimumDuration(1200);//1.2s before pops up
	}

	DbRecordSet lstNotifyObserver;
	lstNotifyObserver.addColumn(QVariant::Int,"BEM_ID");

	const int nChunkSize = 30;
	int nCount = 0;
	int nFailuresCnt = 0;
	int nStart = -1;
	int nRowProgress = 0;

	qDebug() << "Import Email Start";

	
	while(nCount<nSize)
	{
		int nAttachmentChunkSize=0;

		DbRecordSet lstChunk; //BT: always redefine chunk list!!!
		lstChunk.copyDefinition(lstData);
		if (lstChunk.getColumnIdx("ATTACHMENTS")>=0)
			lstChunk.removeColumn(lstChunk.getColumnIdx("ATTACHMENTS")); //BT = remove this, coz it will go on server

		DbRecordSet lstAttachments;
		lstAttachments.addColumn(DbRecordSet::GetVariantType(), "ATT_LIST");
		DbRecordSet lstContactLinks;
		lstContactLinks.addColumn(DbRecordSet::GetVariantType(), "CONTACT_LIST");

		//prepare chunk
		lstChunk.clear();

		qDebug() << "Import Email New Chunk";

		int nMax = min(nCount+nChunkSize, nSize);
		for(int i=nCount; i<nMax; i++)
		{
			nAttachmentChunkSize+=lstData.getDataRef(i, "ATTACHMENTS_SIZE").toInt();
			if (nAttachmentChunkSize>=MAX_ATTACHMENT_LIST_SIZE)
			{
				if (i==nCount) //if only 1 mail is bigger then otribi attachments until <MAX_ATTACHMENT_LIST_SIZE
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - start trying to truncate attachment of %1 bytes for mail %2").arg(nAttachmentChunkSize).arg(lstData.getDataRef(i,"BEM_SUBJECT").toString()));
					DbRecordSet lstAttachmentOneEmail=lstData.getDataRef(i, "ATTACHMENTS").value<DbRecordSet>();
					FilterAttachmentRecords(lstAttachmentOneEmail);
					Q_ASSERT(lstAttachmentOneEmail.getStoredByteSize()<MAX_ATTACHMENT_LIST_SIZE); 
					nAttachmentChunkSize=lstAttachmentOneEmail.getStoredByteSize(); //add new value
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - attachment truncated to the %1 bytes").arg(nAttachmentChunkSize));
				}
				else
					break; //if limit is reached, exit loop and send chunk as is, lstattachments should be smaller then MAX_ATTACHMENT_LIST_SIZE
			}

			lstChunk.merge(lstData.getRow(i));
			//append attachment
			lstAttachments.addRow();
			DbRecordSet row1 = lstData.getDataRef(i, "ATTACHMENTS").value<DbRecordSet>();
			lstAttachments.setData(lstAttachments.getRowCount()-1, 0, row1);

#ifdef _DEBUG
			DbRecordSet tmp = lstData.getDataRef(i, "ATTACHMENTS").value<DbRecordSet>();
			int nAttCnt = tmp.getRowCount();
			for(int j=0; j<nAttCnt; j++)
				qDebug() << "Attachment" << j << "Size" << tmp.getDataRef(j, "BEA_CONTENT").toByteArray().size() << "Name" << tmp.getDataRef(j, "BEA_NAME").toString();
#endif

			lstContactLinks.addRow();
			DbRecordSet row = lstData.getDataRef(i, "CONTACTS").value<DbRecordSet>();
			lstContactLinks.setData(lstContactLinks.getRowCount()-1, 0, row);
		}

		Q_ASSERT(lstChunk.getRowCount()!=0); //must be >0

		//if(lstChunk.getRowCount() < 1)
		//	break;

		Q_ASSERT(lstAttachments.getRowCount() == lstChunk.getRowCount());

		//update progress
		if(pDlg) 
		{
			pDlg->setValue(nCount + lstChunk.getRowCount());
			qApp->processEvents();
		}

		if (pDlg)
		{
			if (pDlg->wasCanceled()){
				QMessageBox::information(NULL, "Info", tr("Operation stopped by user request!"));
				return;
			}
		}

		//BT added: set owner to logged user:
		if (g_pClientManager->GetPersonID()!=0)
			lstChunk.setColValue(lstChunk.getColumnIdx("CENT_OWNER_ID"),g_pClientManager->GetPersonID());
		//BT added: set mail type:
		lstChunk.setColValue(lstChunk.getColumnIdx("CENT_SYSTEM_TYPE_ID"),GlobalConstants::CE_TYPE_EMAIL);

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - prepare to send data chunk %1 rows, %3 bytes (total %2), attachment list size: %4").arg(lstChunk.getRowCount()).arg(nSize).arg(lstChunk.getStoredByteSize()).arg(lstAttachments.getStoredByteSize()));

		int nChunkSizePre = lstChunk.getRowCount();

		QString strLockRes;
		DbRecordSet lstSchedule;
		_SERVER_CALL(BusEmail->WriteMultiple(status, lstChunk, lstContactLinks, lstSchedule, lstAttachments, strLockRes, bSkipExisting, nSkippedOrReplacedCnt, bAddDupsAsNewRow))
		_CHK_ERR_NO_MSG(status);

		int nChunkSize = lstChunk.getRowCount();
		Q_ASSERT(nChunkSizePre == nChunkSize);

		//now write email IDs back into the  main list
		for(int z=0; z<nChunkSize; z++){
			int nEmailID = lstChunk.getDataRef(z, "BEM_ID").toInt();
			//some emails may be skipped (already existing in the database, ID=0 (NULL))
			//Q_ASSERT(nEmailID > 0);
			//BT:nEmailID=0 for skipped emails...
			lstData.setData(nCount + z, "BEM_ID", nEmailID);
		}
		
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - chunk sent success"));

		//proceed to next chunk
		nCount += nChunkSizePre;

		if (lstChunk.getRowCount()>0)
			lstNotifyObserver.merge(lstChunk);
	}

	qDebug() << "Import Email End";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - Import Email End"));

	//B.T.: notify CE grid about new emails in system:
	//------------------------------------------------------
	//Send signal for all to reload:
	//------------------------------------------------------
	if (!bSkipProgress && lstNotifyObserver.getRowCount()>0)
	{
		QVariant varData;
		qVariantSetValue(varData,lstNotifyObserver);
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - Import Email Before Notify"));
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ENTITY_BUS_EMAILS,varData,NULL); 
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - Import Email After Notify"));
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - Import Email prepare for exit"));

	if(pDlg) pDlg->setValue(nCount+15);
	if(pDlg) pDlg->close();

	qDebug() << "Import Email Exit";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - Import Email Exit"));
}

void FUI_ImportEmail::on_btnImportData_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - import data click"));

	//extract selected rows
	DbRecordSet lstSelected;
	lstSelected.copyDefinition(m_lstData);
	lstSelected.merge(m_lstData, true);
	//lstSelected.Dump();
	if(lstSelected.getRowCount() < 1){
		QMessageBox::information(this, "", tr("No items have been selected for import!"));
		return;
	}

	int nSkippedOrReplacedCnt = 0;
	bool bSkipExisting = !ui.chkOverwrite->isChecked();
	Status status; 
	ImportEmails(status, lstSelected, nSkippedOrReplacedCnt, bSkipExisting);
	_CHK_ERR(status);
	ui.tableData->RefreshDisplay();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - import data done"));

	if(nSkippedOrReplacedCnt > 0)
	{
		QString strMsg;
		if(bSkipExisting)
			strMsg = QString(tr("%1 existing emails have been skipped!")).arg(nSkippedOrReplacedCnt);
		else
			strMsg = QString(tr("%1 existing emails have been replaced!")).arg(nSkippedOrReplacedCnt);
		QMessageBox::information(this, tr("Info"), strMsg);
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - close window"));
	
	//QTimer::singleShot(0,this,SLOT(OnCloseWindow()));
	g_objFuiManager.CloseFUI(g_objFuiManager.GetFuiID(this));
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - close window done"));
}

void FUI_ImportEmail::on_btnAssignProject_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - Assign project click"));

	int nCol = m_lstData.getColumnIdx("BEM_PROJECT_ID");
	int nNameCol = m_lstData.getColumnIdx("PROJECT_NAME");
	int nCodeCol = m_lstData.getColumnIdx("PROJECT_CODE");

	int nEntityRecordID = -1;
	DbRecordSet record;
	if(ui.frameProject->GetCurrentEntityRecord(nEntityRecordID, record)){
		m_lstData.setColValue(nCol, nEntityRecordID, true);	// only to selected items
		m_lstData.setColValue(nNameCol, record.getDataRef(0, "BUSP_NAME").toString(), true);	// only to selected items
		m_lstData.setColValue(nCodeCol, record.getDataRef(0, "BUSP_CODE").toString(), true);	// only to selected items
	}
	else{
		m_lstData.setColValue(nCol, QVariant(QVariant::Int), true);			// only to selected items
		m_lstData.setColValue(nNameCol, QVariant(QVariant::String), true);	// only to selected items
		m_lstData.setColValue(nCodeCol, QVariant(QVariant::String), true);	// only to selected items
	}
	//record.Dump();
	
	ui.tableData->RefreshDisplay();
}

void FUI_ImportEmail::on_btnOpenEmail_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - Open email click"));

	int nFocusedRow = ui.tableData->row(ui.tableData->currentItem());
	if(nFocusedRow>=0)
	{
		QString strApp = m_lstData.getDataRef(nFocusedRow, "BEM_EXT_APP").toString();
		if("Outlook" == strApp)
		{
#ifdef _WIN32
			//find and open this email in outlook form using Entry ID
			QByteArray arData;
			m_lstData.getData(nFocusedRow, "BEM_EXT_ID", arData);

			if(arData.length() < 1){
				QMessageBox::information(this, tr("Error"), tr("Message does not have Entry ID!"));
				return;
			}

			if(!MapiManager::OpenMailInOutlook(arData))
			{
				QMessageBox::information(this, tr("Info"), tr("Error opening email!"));
			}
#endif
		}
		else if("Thunderbird" == strApp)
		{
			QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

			int nNewFUI=g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_INSERT,-1,true);
			QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
			EMailDialog* pFUI=dynamic_cast<EMailDialog*>(pFUIWidget);
			if (pFUI)
			{
				DbRecordSet row = m_lstData.getRow(nFocusedRow);
				pFUI->SetEmailData(row);
				pFUI->show();  //show FUI
			}

			QApplication::restoreOverrideCursor();
		}
	}
}

void FUI_ImportEmail::OnContactChanged()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - contact changed"));

	//get selected unassigned email
	QString strEmail;
	int nCurSel = ui.cboUnassignedEmails->currentIndex();
	if(nCurSel >= 0)
		strEmail = ui.cboUnassignedEmails->itemText(nCurSel);
	if(strEmail.isEmpty())
		return;

	//get selected contact ID 
	int nContactID = -1;
	DbRecordSet record;
	if(!ui.frameContact->GetCurrentEntityRecord(nContactID, record))
		return;

	//record.Dump();

	//update cache
	m_mapEmail2Contact[strEmail] = nContactID;

	//now make another run to update unassigned list
	//calculate "EMAIL_UNASSIGNED", "CONTACTS" field for individual rows
	bool bAnyChange = false;
	int nCnt = m_lstData.getRowCount();
	int i;
	for(i=0; i<nCnt; i++)
	{
		QString strUnassigned;
		m_lstData.getData(i, "EMAIL_UNASSIGNED", strUnassigned);
		
		//split emails into the list and remove email name from each email
		int nIdxFound = -1;
		QStringList lstLocalEmls = strUnassigned.split(";", QString::SkipEmptyParts);
		int nLocEmls = lstLocalEmls.count();
		int j;
		for(j=0; j<nLocEmls; j++){
			if(lstLocalEmls[j] == strEmail)	{
				nIdxFound = j;
				break;
			}
		}
		if(nIdxFound >= 0)
		{
			bAnyChange = true;

			//update contacts list
			DbRecordSet lstContactLinks;
			m_lstData.getData(i, "CONTACTS", lstContactLinks);
			lstContactLinks.addRow();
			int nRow = lstContactLinks.getRowCount()-1;
			lstContactLinks.setData(nRow, "BNMR_TABLE_KEY_ID_2", nContactID);
			
			//recalc role
			QString strEmailFrom, strEmailTo, strEmailCC, strEmailBCC, strAll;
			m_lstData.getData(i, "BEM_FROM", strEmailFrom);
			m_lstData.getData(i, "BEM_TO",   strEmailTo);
			m_lstData.getData(i, "BEM_CC",   strEmailCC);
			m_lstData.getData(i, "BEM_BCC",  strEmailBCC);

			//CELC_LINK_ROLE_ID - 1=From 2=To 3=CC 4=BCC for emails
			QString strEmail2(strEmail);
			strEmail2.insert(0, "<");
			strEmail2 += ">";

			int nLinkRole = 0;
			if(strEmailFrom.indexOf(strEmail2) >= 0)
				nLinkRole = 1;
			else if(strEmailTo.indexOf(strEmail2) >= 0)
				nLinkRole = 2;
			else if(strEmailCC.indexOf(strEmail2) >= 0)
				nLinkRole = 3;
			else if(strEmailBCC.indexOf(strEmail2) >= 0)
				nLinkRole = 4;

			if(nLinkRole > 0)
				lstContactLinks.setData(nRow, "BNMR_SYSTEM_ROLE", nLinkRole);

			m_lstData.setData(i, "CONTACTS", lstContactLinks);

			//update unassigned field - remove current email from it
			QString strUnassignedLocal;
			for(j=0; j<nLocEmls; j++)
			{
				if(j != nIdxFound){
					QString strEmail = lstLocalEmls[j];
					if(!strUnassignedLocal.isEmpty())
						strUnassignedLocal += ";";
					strUnassignedLocal += strEmail;
				}
				m_lstData.setData(i, "EMAIL_UNASSIGNED", strUnassignedLocal);
			}
		}	// nIdxFound > 0
	}	// for

	if(bAnyChange)
		ui.tableData->RefreshDisplay();

	//remove this email from combo with unassigned
	if(nCurSel >= 0){
		ui.cboUnassignedEmails->removeItem(nCurSel);
		ui.cboUnassignedEmails->setCurrentIndex(0);
	}

	//now clear contact info for next "victim"
	ui.frameContact->Clear();

/*
	//permanently add the email address to the contact
	DbRecordSet recContactEmail;
	recContactEmail.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL));
	recContactEmail.addRow();
	recContactEmail.setData(0, "BCME_CONTACT_ID", nContactID);
	recContactEmail.setData(0, "BCME_ADDRESS", strEmail);
	recContactEmail.setData(0, "BCME_NAME", record.getDataRef(0, "BCNT_NAME").toString());
	recContactEmail.setData(0, "BCME_IS_DEFAULT", 0);
	
	Status status;
	_SERVER_CALL(BusContact->AddContactEmail(status, recContactEmail))
	_CHK_ERR(status);
*/
}

void FUI_ImportEmail::OnContactSAPNEInsert()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - contact inserted"));

	//new Contact is about to be inserted, fetch new FUI:
	FuiBase* pFUI=dynamic_cast<FuiBase*>(ui.frameContact->GetLastFUIOpen());
	if(pFUI==NULL) return;

	//register observer
	pFUI->registerObserver(this);

	QCW_Base *pQCW=dynamic_cast<QCW_Base*>(pFUI);
	if(pQCW==NULL) return;

	//get data holder:
	//BoClientEntitySet* pBoEntity=static_cast<BoClientEntitySet*>(pFUI->GetBoEntity());
	//Q_ASSERT(pBoEntity!=NULL);


	//---------------------------------------------
	//Email Address
	//---------------------------------------------
	QString strEmail=ui.cboUnassignedEmails->currentText();
	if(!strEmail.isEmpty())
	{
		//---------------------------------------------
		//Contact name:  find pattern: name.lastname@
		//---------------------------------------------

		DbRecordSet dataContact;
		dataContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_FUI));

		int nDotIdx=strEmail.indexOf(".");
		int nMonkeyIdx=strEmail.indexOf("@");
		if (nDotIdx<nMonkeyIdx && nDotIdx>0 && nMonkeyIdx>0)
		{
			QString strFirstName=strEmail.left(nDotIdx);
			if(strFirstName.length() > 0)
				strFirstName[0] = strFirstName[0].toUpper();	//capitalize
			QString strLastName=strEmail.mid(nDotIdx+1,nMonkeyIdx-nDotIdx-1);
			if(strLastName.length() > 0)
				strLastName[0] = strLastName[0].toUpper();	//capitalize
			//new contact data (name):

			dataContact.addColumn(QVariant::ByteArray,"BPIC_PICTURE");
			dataContact.addRow();
			dataContact.setData(0,"BCNT_FIRSTNAME",strFirstName);
			dataContact.setData(0,"BCNT_LASTNAME",strLastName);

			//pBoEntity->InsertDefaultValues(dataContact,BoClientEntity_BusContact::GROUP_MAIN);
		}

		DbRecordSet data;
		data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL));
		data.addRow();
		data.setData(0,"BCME_ADDRESS",strEmail);
		//pBoEntity->InsertDefaultValues(data,BoClientEntity_BusContact::GROUP_EMAIL);

		//set rows:
		DbRecordSet recContactFull;
		recContactFull.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
		ContactTypeManager::AddEmptyRowToFullContactList(recContactFull);
		if (dataContact.getRowCount()>0)
			recContactFull.assignRow(0,dataContact,true);
		
		recContactFull.setData(0,"LST_EMAIL",data); //set email

		//extract bode for first email that has this address
		QString strMsgBody;
		QString strEmail1 = "<";
		strEmail1 += strEmail;
		strEmail1 += ">";
		int nCnt = m_lstData.getRowCount();
		for(int i=0; i<nCnt; i++)
		{
			QString strEmailFrom, strEmailTo, strEmailCC, strEmailBCC;
			m_lstData.getData(i, "BEM_FROM", strEmailFrom);
			m_lstData.getData(i, "BEM_TO",   strEmailTo);
			m_lstData.getData(i, "BEM_CC",   strEmailCC);
			m_lstData.getData(i, "BEM_BCC",  strEmailBCC);

			if( strEmailFrom.indexOf(strEmail1) >= 0 ||
				strEmailTo.indexOf(strEmail1) >= 0 ||
				strEmailCC.indexOf(strEmail1) >= 0 ||
				strEmailBCC.indexOf(strEmail1) >= 0)
			{
				m_lstData.getData(i, "BEM_BODY", strMsgBody);
				break;
			}
		}
		
		//insert data:
		pQCW->SetDefaultValues(recContactFull, strMsgBody);
	}

	//refresh FUI, setactivegroupvisible to Phones:
	//pFUI->RefreshDisplay();
}

void FUI_ImportEmail::SetDefaults(QString strDefaultContactEmail)
{
	ui.radUseFilter->setChecked(true);
	ui.txtFilterFromField->setText(strDefaultContactEmail);
	ui.txtFilterToField->setText(strDefaultContactEmail);
	ui.txtFilterCCField->setText(strDefaultContactEmail);
	ui.txtFilterBCCField->setText(strDefaultContactEmail);
	
	ui.txtFilterFromDate->setDate(QDate(1900,1,1));
	ui.txtFilterToDate->setDate(QDate::currentDate());
}


void FUI_ImportEmail::OnGridFilledRecalculate(QSet<QString> &setEmails, int nDefaultProjectID)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - recalculate list"));

	//get contact ID for each email not already in cache (if exists)
	DbRecordSet lstEmails;
	lstEmails.addColumn(QVariant::Int, "BCME_CONTACT_ID");
	lstEmails.addColumn(QVariant::String, "BUS_CM_EMAIL");

	int nPosList = 0;
	QList<QString> lstUnknown = setEmails.toList();
	int nCntUnk = lstUnknown.count();
	int i;
	for(i=0; i<nCntUnk; i++)
	{
		//check if this email is already resolved in the cache
		QMap<QString, int>::iterator It = m_mapEmail2Contact.find(lstUnknown[i]);
		if(It == m_mapEmail2Contact.end())
		{
			lstEmails.addRow();
			lstEmails.setData(nPosList, 1, lstUnknown[i]);
			nPosList ++;
		}
	}

	Status status;
	_SERVER_CALL(BusCommunication->GetContactFromEmail(status, lstEmails))
	_CHK_ERR(status);

	QList<QString> lstUnassigned;

	//refresh cache
	nCntUnk = lstEmails.getRowCount();
	for(i=0; i<nCntUnk; i++)
	{
		if(!lstEmails.getDataRef(i, 0).isNull()){
			m_mapEmail2Contact[lstEmails.getDataRef(i, 1).toString()] = lstEmails.getDataRef(i, 0).toInt();
		}
		else
			lstUnassigned.append(lstEmails.getDataRef(i, 1).toString());
	}

	//calculate "EMAIL_UNASSIGNED", "CONTACTS" field for individual rows
	int nCnt = m_lstData.getRowCount();
	for(i=0; i<nCnt; i++)
	{
		UpdateRowEmailContact(i);
	}

	//fill combo box
	//ui.cboUnassignedEmails->clear(); //TOFIX: if dropping on already filled list, do not delete previous results
	nCnt = lstUnassigned.size();
	for(i=0; i<nCnt; i++){
		if(ui.cboUnassignedEmails->findText(lstUnassigned[i]) < 0)
			ui.cboUnassignedEmails->addItem(lstUnassigned[i]);
	}

	ui.tableData->RefreshDisplay();

	//set template flag to 0
	int nTemplateCol = m_lstData.getColumnIdx("BEM_TEMPLATE_FLAG");
	m_lstData.setColValue(nTemplateCol, 0);

	//mark unread messages in bold
	//B.T. improved:
	int nCount = m_lstData.getRowCount();
	OnSignalRefreshBoldRows();

	QString strCode, strName;
	int nProjID;
	if(nDefaultProjectID >= 0)	// override project setting
	{
		MainEntitySelectionController cacheLoader;
		cacheLoader.Initialize(ENTITY_BUS_PROJECT);
		DbRecordSet record;
		cacheLoader.GetEntityRecord(nDefaultProjectID, record);
		if(record.getRowCount()>0)
		{
			strCode = record.getDataRef(0, "BUSP_CODE").toString();
			strName = record.getDataRef(0, "BUSP_NAME").toString();
			nProjID = record.getDataRef(0, "BUSP_ID").toInt();

			int nCol = m_lstData.getColumnIdx("BEM_PROJECT_ID");
			int nCodeCol = m_lstData.getColumnIdx("PROJECT_CODE");
			int nNameCol = m_lstData.getColumnIdx("PROJECT_NAME");
			m_lstData.setColValue(nCodeCol, strCode);
			m_lstData.setColValue(nNameCol, strName);
			m_lstData.setColValue(nCol, nProjID);
		}
	}
	else
	{
		for(i=0; i<nCount; i++)
		{
			//if there is a single contact (other than us) and it has single related project
			//attach the project directly to this email
			DbRecordSet lstContactLinks = m_lstData.getDataRef(i, "CONTACTS").value<DbRecordSet>();
			int nCntContacts = lstContactLinks.getRowCount();
			if(nCntContacts > 0)
			{
				bool bOutgoing = (m_lstData.getDataRef(i, "BEM_OUTGOING").toInt() > 0);
				//lstContactLinks.Dump();
				int nCnt = 0;
				int nIdx = -1;
				for(int j=0; j<nCntContacts; j++)
				{
					int nRole = lstContactLinks.getDataRef(j, "BNMR_SYSTEM_ROLE").toInt();
					if(bOutgoing && nRole == GlobalConstants::CONTACT_LINK_ROLE_TO){
						nCnt ++;
						nIdx = j;
					}
					else if(!bOutgoing && nRole == GlobalConstants::CONTACT_LINK_ROLE_FROM){
						nCnt ++;
						nIdx = j;
					}
				}

				if(nCnt == 1)	// only one contact found for this email
				{
					//check related projects for this contact
					Status status;
					DbRecordSet lstProjects;
					int nContactID=lstContactLinks.getDataRef(nIdx, "BNMR_TABLE_KEY_ID_2").toInt();
					_SERVER_CALL(BusContact->ReadNMRXProjects(status,nContactID,lstProjects))
					_CHK_ERR(status);

					//if only one project, attach the project directly to the mail
				#ifdef _DEBUG
					//lstContactLinks.Dump();
					//lstProjects.Dump();
				#endif
					if(lstProjects.getRowCount() == 1)
					{
						m_lstData.setData(i, "PROJECT_CODE", lstProjects.getDataRef(0, "BUSP_CODE").toString());
						m_lstData.setData(i, "PROJECT_NAME", lstProjects.getDataRef(0, "BUSP_NAME").toString());
						m_lstData.setData(i, "BEM_PROJECT_ID", lstProjects.getDataRef(0, "BUSP_ID").toInt());
					}
				}
			}
		}
	}

	ui.tableData->RefreshDisplay();
}

void FUI_ImportEmail::UpdateRowEmailContact(int nIdx)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - update contact email"));

	QString strEmailFrom, strEmailTo, strEmailCC, strEmailBCC;
	m_lstData.getData(nIdx, "BEM_FROM", strEmailFrom);
	m_lstData.getData(nIdx, "BEM_TO",   strEmailTo);
	m_lstData.getData(nIdx, "BEM_CC",   strEmailCC);
	m_lstData.getData(nIdx, "BEM_BCC",  strEmailBCC);
	
	//split emails into the list and remove email name from each email
	QStringList lstLocalEmlFrom = ExtractEmails(strEmailFrom);
	QStringList lstLocalEmlTo   = ExtractEmails(strEmailTo);
	QStringList lstLocalEmlCC   = ExtractEmails(strEmailCC);
	QStringList lstLocalEmlBCC  = ExtractEmails(strEmailBCC);

	//now check if the emails are known or unnassigned
	DbRecordSet lstContactLinks;
	lstContactLinks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	QString strUnassignedLocal;

	//CELC_LINK_ROLE_ID - 1=From 2=To 3=CC 4=BCC for emails
	UpdateContactsList(lstLocalEmlFrom, 1, lstContactLinks, strUnassignedLocal);
	UpdateContactsList(lstLocalEmlTo,	2, lstContactLinks, strUnassignedLocal);
	UpdateContactsList(lstLocalEmlCC,	3, lstContactLinks, strUnassignedLocal);
	UpdateContactsList(lstLocalEmlBCC,	4, lstContactLinks, strUnassignedLocal);

	m_lstData.setData(nIdx, "CONTACTS", lstContactLinks);
	m_lstData.setData(nIdx, "EMAIL_UNASSIGNED", strUnassignedLocal);
}

void FUI_ImportEmail::UpdateContactsList(QStringList &lstSrc, int nRole, DbRecordSet &lstContactLinks, QString &strUnassignedLocal)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - update contact list"));

	int nCount = lstSrc.count();
	for(int j=0; j<nCount; j++)
	{
		QString strEmail = lstSrc[j];

		//check if this email is already resolved in the cache
		QMap<QString, int>::iterator It = m_mapEmail2Contact.find(strEmail);
		if(It == m_mapEmail2Contact.end())
		{
			if(!strUnassignedLocal.isEmpty())
				strUnassignedLocal += ";";
			strUnassignedLocal += strEmail;
		}
		else
		{
			//email is resolved, store contact ID
			lstContactLinks.addRow();
			int nRow = lstContactLinks.getRowCount()-1;
			lstContactLinks.setData(nRow, "BNMR_TABLE_KEY_ID_2", m_mapEmail2Contact[strEmail]);

			strEmail.insert(0, "<");
			strEmail += ">";

			lstContactLinks.setData(nRow, "BNMR_SYSTEM_ROLE", nRole);
		}
	}
}

QStringList FUI_ImportEmail::ExtractEmails(QString &strSrc)
{
	//split emails into the list and remove email name from each email
	QStringList lstLocalEmls = strSrc.split(";", QString::SkipEmptyParts);
	int nLocEmls = lstLocalEmls.count();
	int j;
	for(j=0; j<nLocEmls; j++)
	{
		QString strEntry = lstLocalEmls[j];
		int nPos = strEntry.indexOf("<");
		if(nPos > 0)
			strEntry = strEntry.right(strEntry.length()-nPos-1);
		nPos = strEntry.indexOf(">");
		if(nPos > 0)
			strEntry = strEntry.left(nPos);
		lstLocalEmls[j] = strEntry;
	}

	return lstLocalEmls;
}

bool FUI_ImportEmail::IsDropEmail(QByteArray &byteDrop)
{
	int nPos = byteDrop.indexOf(QByteArray("IPM.Note"));
	return (nPos >= 0);
}

bool FUI_ImportEmail::IsDropContact(QByteArray &byteDrop)
{
	int nPos = byteDrop.indexOf(QByteArray("IPM.Contact"));
	return (nPos >= 0);
}

bool FUI_ImportEmail::IsDropAppointment(QByteArray &byteDrop)
{
	int nPos = byteDrop.indexOf(QByteArray("IPM.Appointment"));
	return (nPos >= 0);
}

void FUI_ImportEmail::BuildImportListFromDrop(QByteArray &byteDrop, bool bAreTemplates, int nDefaultProjectID)
{
	qDebug() << "FUI_ImportEmail::BuildImportListFromDrop";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Build import list from drop"));

#ifdef _WIN32
	//hide parts of the window
	ui.frame->hide();

	//if default project, show it
	if(nDefaultProjectID >= 0)
		ui.frameProject->SetCurrentEntityRecord(nDefaultProjectID, true);

	//fill the grid with email
	if(byteDrop.size() > 0)
	{

		m_bUseFilter = false;
		QList<QByteArray> lstEntryIDs;
		MapiManager::GetEntryIDsFromDrop(byteDrop, lstEntryIDs);
		QSet<QString> setEmails;
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MapiManager::GetEntryIDsFromDrop: entries found in %1 bytedrop").arg(lstEntryIDs.size()));

		MapiManager::GenerateEmailsFromDrop(m_lstData,lstEntryIDs,setEmails,Call_FilterMatchRow,(unsigned long)this, MapiManager::InitGlobalMapi());
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("FUI_ImportEmail::BuildImportListFromDrop found %1 rows in list").arg(m_lstData.getRowCount()));
		m_lstData.selectAll();
		OnGridFilledRecalculate(setEmails, nDefaultProjectID);
		ui.tableData->RefreshDisplay();

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Email import list displayed"));
	}

#endif //#ifdef _WIN32
}

#ifdef _WIN32
void FUI_ImportEmail::ReadEmails(Status status, QList<QByteArray> &lstEntryIDs, DbRecordSet &lstData, bool bAreTemplates, int nDefaultProjectID, QProgressDialog *progress, bool bOnlyKnownContacts, bool bOnlyOutgoingEmails, CMAPIEx *pMapi)
{
	qDebug() << "FUI_ImportEmail::ReadEmails";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmails(OnlyKnown=%1, OnlyOutgoing=%2)").arg(bOnlyKnownContacts).arg(bOnlyOutgoingEmails));

	int nCnt = lstEntryIDs.count();
	QSet<QString> setEmails;
	if(!MapiManager::ReadEmailsFromEntryIDs(lstEntryIDs,setEmails,lstData,bAreTemplates,progress, pMapi, bOnlyOutgoingEmails))
		return;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmails - read emails"));

	//TOFIX code similar to OnGridFilledRecalculate
	QString strCode, strName;
	int nProjID;
	if(nDefaultProjectID >= 0)	// override project setting
	{
		MainEntitySelectionController cacheLoader;
		cacheLoader.Initialize(ENTITY_BUS_PROJECT);
		DbRecordSet record;
		cacheLoader.GetEntityRecord(nDefaultProjectID, record);
		if(record.getRowCount()>0)
		{
			strCode = record.getDataRef(0, "BUSP_CODE").toString();
			strName = record.getDataRef(0, "BUSP_NAME").toString();
			nProjID = record.getDataRef(0, "BUSP_ID").toInt();

			int nCol = lstData.getColumnIdx("BEM_PROJECT_ID");
			int nCodeCol = lstData.getColumnIdx("PROJECT_CODE");
			int nNameCol = lstData.getColumnIdx("PROJECT_NAME");
			lstData.setColValue(nCodeCol, strCode);
			lstData.setColValue(nNameCol, strName);
			lstData.setColValue(nCol, nProjID);
		}
	}

	//get contact ID for each email not already in cache (if exists)
	if(!bAreTemplates)
	{
		DbRecordSet lstEmails;
		lstEmails.addColumn(QVariant::Int, "BCME_CONTACT_ID");
		lstEmails.addColumn(QVariant::String, "BUS_CM_EMAIL");

		int nPosList = 0;
		QList<QString> lstUnknown = setEmails.toList();
		int nCntUnk = lstUnknown.count();
		int i;
		for(i=0; i<nCntUnk; i++)
		{
			//check if this email is already resolved in the cache
			QMap<QString, int>::iterator It = m_mapEmail2Contact.find(lstUnknown[i]);
			if(It == m_mapEmail2Contact.end())
			{
				lstEmails.addRow();
				lstEmails.setData(nPosList, 1, lstUnknown[i]);
				nPosList ++;
			}
		}

		_SERVER_CALL(BusCommunication->GetContactFromEmail(status, lstEmails))
		_CHK_ERR_NO_MSG(status);	// no msg due to multithreading

		QList<QString> lstUnassigned;

		//refresh cache
		nCntUnk = lstEmails.getRowCount();
		for(i=0; i<nCntUnk; i++)
		{
			if(!lstEmails.getDataRef(i, 0).isNull()){
				g_mutexEmail2Cont.lock();
				m_mapEmail2Contact[lstEmails.getDataRef(i, 1).toString()] = lstEmails.getDataRef(i, 0).toInt();
				g_mutexEmail2Cont.unlock();
			}
			else
				lstUnassigned.append(lstEmails.getDataRef(i, 1).toString());
		}

		Q_ASSERT(nCnt == lstData.getRowCount());

		//for each email
		//TOFIX code similar to UpdateRowEmailContact
		//calculate "EMAIL_UNASSIGNED", "CONTACTS" field for individual rows
		for(i=0; i<nCnt; i++)
		{
			QString strEmailFrom, strEmailTo, strEmailCC, strEmailBCC;
			lstData.getData(i, "BEM_FROM", strEmailFrom);
			lstData.getData(i, "BEM_TO",   strEmailTo);
			lstData.getData(i, "BEM_CC",   strEmailCC);
			lstData.getData(i, "BEM_BCC",  strEmailBCC);

			//split emails into the list and remove email name from each email
			QStringList lstLocalEmlFrom = ExtractEmails(strEmailFrom);
			QStringList lstLocalEmlTo   = ExtractEmails(strEmailTo);
			QStringList lstLocalEmlCC   = ExtractEmails(strEmailCC);
			QStringList lstLocalEmlBCC  = ExtractEmails(strEmailBCC);

			//now check if the emails are known or unnassigned
			DbRecordSet lstContactLinks;
			lstContactLinks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
			QString strUnassignedLocal;

			//CELC_LINK_ROLE_ID - 1=From 2=To 3=CC 4=BCC for emails
			UpdateContactsList(lstLocalEmlFrom, 1, lstContactLinks, strUnassignedLocal);
			UpdateContactsList(lstLocalEmlTo,	2, lstContactLinks, strUnassignedLocal);
			UpdateContactsList(lstLocalEmlCC,	3, lstContactLinks, strUnassignedLocal);
			UpdateContactsList(lstLocalEmlBCC,	4, lstContactLinks, strUnassignedLocal);

			lstData.setData(i, "CONTACTS", lstContactLinks);
			lstData.setData(i, "EMAIL_UNASSIGNED", strUnassignedLocal);

			if(bOnlyKnownContacts){
				QString strSubject = lstData.getDataRef(i, "BEM_SUBJECT").toString();
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmails: filter email '%1' by contacts (from=[%2],to=[%3])").arg(strSubject).arg(strEmailFrom).arg(strEmailTo));
			}

			//TOFIX if there is a single contact? and it has single project
			int nCntContacts = lstContactLinks.getRowCount();
			if(nCntContacts > 0)
			{
				//lstContactLinks.Dump();
				bool bOutgoing = (lstData.getDataRef(i, "BEM_OUTGOING").toInt() > 0);
				int nCnt1 = 0;
				int nIdx = -1;
				for(int j=0; j<nCntContacts; j++)
				{
					int nRole = lstContactLinks.getDataRef(j, "BNMR_SYSTEM_ROLE").toInt();
					if(bOutgoing && nRole == GlobalConstants::CONTACT_LINK_ROLE_TO){
						g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmails: filter by contacts, found valid 'To' contact for outgoing mail"));
						nCnt1 ++;
						nIdx = j;
					}
					else if(!bOutgoing && nRole == GlobalConstants::CONTACT_LINK_ROLE_FROM){
						g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmails: filter by contacts, found valid 'From' contact for incoming mail"));
						nCnt1 ++;
						nIdx = j;
					}
				}
				if(bOnlyKnownContacts && nCnt1 < 1)
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmails: remove email row %1 (no known contacts): OutgoingMail=%2").arg(i).arg(bOutgoing));

					//delete this email row, it's not from at least one known contact
					lstData.deleteRow(i);
					i --; nCnt --;
					continue;
				}
				
				// only when no default project setting
				if(nDefaultProjectID < 0 && nCnt1 == 1)	// only one contact found for this email
				{
					//check related projects for this contact
					Status status;
					DbRecordSet lstProjects;
					int nContactID=lstContactLinks.getDataRef(nIdx, "BNMR_TABLE_KEY_ID_2").toInt();
					_SERVER_CALL(BusContact->ReadNMRXProjects(status,nContactID,lstProjects))
					_CHK_ERR_NO_MSG(status);	// no msg due to multithreading

					//if only one project, attach the project directly to the mail
					if(lstProjects.getRowCount() == 1)
					{
						lstData.setData(i, "PROJECT_CODE", lstProjects.getDataRef(0, "BUSP_CODE").toString());
						lstData.setData(i, "PROJECT_NAME", lstProjects.getDataRef(0, "BUSP_NAME").toString());
						lstData.setData(i, "BEM_PROJECT_ID", lstProjects.getDataRef(0, "BUSP_ID").toInt());
					}
				}
			}
			else
			{
				if(bOnlyKnownContacts)
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmails: remove email row %d (no contacts whatsoever)").arg(i));

					//delete this email row, it's not from at least one known contact
					lstData.deleteRow(i);
					i --; nCnt --;
					continue;
				}
			}
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmails - done"));
}

#endif //#ifdef _WIN32

void FUI_ImportEmail::OnProjectChanged()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - project changed"));

	//get current code/name
	QString strCode, strName;
	int nEntityRecordID = -1;
	DbRecordSet record;
	if(ui.frameProject->GetCurrentEntityRecord(nEntityRecordID, record)){
		//record.Dump();
		strName = record.getDataRef(0, "BUSP_NAME").toString();
		strCode = record.getDataRef(0, "BUSP_CODE").toString();
	}

	//ask user
	QString strMsg;
	int nSelected = m_lstData.getSelectedCount();
	if(nSelected > 0)
		strMsg = tr("Do you want to assign project \"%1 %2\" to the selected email(s)?").arg(strCode).arg(strName);
	else
		strMsg = tr("Do you want to assign project \"%1 %2\" to the listed email(s)?").arg(strCode).arg(strName);

	int nResult = QMessageBox::question(this, tr("Question"), strMsg, QMessageBox::Yes|QMessageBox::No);
	if(QMessageBox::Yes != nResult)
		return;

	//set data
	int nCol = m_lstData.getColumnIdx("BEM_PROJECT_ID");
	int nNameCol = m_lstData.getColumnIdx("PROJECT_NAME");
	int nCodeCol = m_lstData.getColumnIdx("PROJECT_CODE");

	m_lstData.setColValue(nCol, 
		(nEntityRecordID >= 0)? nEntityRecordID : QVariant(QVariant::Int),
		(nSelected > 0)? true : false);	// only to selected items?

	m_lstData.setColValue(nNameCol, 
		(nEntityRecordID >= 0)? strName : QVariant(QVariant::String),
		(nSelected > 0)? true : false);	// only to selected items?

	m_lstData.setColValue(nCodeCol, 
		(nEntityRecordID >= 0)? strCode : QVariant(QVariant::String),
		(nSelected > 0)? true : false);	// only to selected items?
	
	ui.tableData->RefreshDisplay();
}

void FUI_ImportEmail::OnSignalRefreshBoldRows()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - refresh bold signal"));

	//mark unread messages in bold
	//B.T. improved:
	ui.tableData->ClearAllBoldFonts();
	ui.tableData->setUpdatesEnabled(false);
	ui.tableData->blockSignals(true);
	int nCount = m_lstData.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		if(m_lstData.getDataRef(i, "BEM_UNREAD_FLAG").toInt() > 0)
			ui.tableData->SetRowBold(i);
	}
	ui.tableData->setUpdatesEnabled(true);
	ui.tableData->blockSignals(false);
}

void FUI_ImportEmail::ClearInternalCache()
{
	m_mapEmail2Contact.clear();
}

void FUI_ImportEmail::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - observer update"));

	if (pSubject == ui.frameProject && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnProjectChanged();
		return;
	}
	if (pSubject == ui.frameContact && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnContactChanged();
		return;
	}
	if (pSubject == ui.frameContact && nMsgCode==SelectorSignals::SELECTOR_ON_AFTER_OPEN_INSERT_HANDLER)
	{
		OnContactSAPNEInsert();
		return;
	}

	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - observer update done"));
	return; //already handled by OnContactChanged


	//QCW:
	if (nMsgCode==ChangeManager::GLOBAL_QCW_WINDOW_FINISH_PROCESS)
	{
		DbRecordSet lstData = val.value<DbRecordSet>();	//TVIEW_BUS_CONTACT_FULL view
		if (lstData.getRowCount()==1)
		{
			int nContactID = lstData.getDataRef(0,"BCNT_ID").toInt();
			DbRecordSet lstEmails = lstData.getDataRef(0, "LST_EMAIL").value<DbRecordSet>();

			//update email cache, remove this emails from "unassigned" list
			//
			//m_mapEmail2Contact[lstEmails.getDataRef(i,"BCME_ADDRESS").toString()] = nContactID;

			//TOFIX similar to OnContactChanged()

			//get selected unassigned email
			QString strEmail;
			int nCurSel = ui.cboUnassignedEmails->currentIndex();
			if(nCurSel >= 0)
				strEmail = ui.cboUnassignedEmails->itemText(nCurSel);
			if(strEmail.isEmpty())
				return;
		
			//update cache
			g_mutexEmail2Cont.lock();
			m_mapEmail2Contact[strEmail] = nContactID;
			g_mutexEmail2Cont.unlock();

			//now make another run to update unassigned list
			//calculate "EMAIL_UNASSIGNED", "CONTACTS" field for individual rows
			bool bAnyChange = false;
			int nCnt = m_lstData.getRowCount();
			int i;
			for(i=0; i<nCnt; i++)
			{
				QString strUnassigned;
				m_lstData.getData(i, "EMAIL_UNASSIGNED", strUnassigned);
				
				//split emails into the list and remove email name from each email
				int nIdxFound = -1;
				QStringList lstLocalEmls = strUnassigned.split(";", QString::SkipEmptyParts);
				int nLocEmls = lstLocalEmls.count();
				int j;
				for(j=0; j<nLocEmls; j++){
					if(lstLocalEmls[j] == strEmail)	{
						nIdxFound = j;
						break;
					}
				}
				if(nIdxFound >= 0)
				{
					bAnyChange = true;

					//update contacts list
					DbRecordSet lstContactLinks;
					m_lstData.getData(i, "CONTACTS", lstContactLinks);
					lstContactLinks.addRow();
					int nRow = lstContactLinks.getRowCount()-1;
					lstContactLinks.setData(nRow, "BNMR_TABLE_KEY_ID_2", nContactID);
					
					//recalc role
					QString strEmailFrom, strEmailTo, strEmailCC, strEmailBCC, strAll;
					m_lstData.getData(i, "BEM_FROM", strEmailFrom);
					m_lstData.getData(i, "BEM_TO",   strEmailTo);
					m_lstData.getData(i, "BEM_CC",   strEmailCC);
					m_lstData.getData(i, "BEM_BCC",  strEmailBCC);

					//CELC_LINK_ROLE_ID - 1=From 2=To 3=CC 4=BCC for emails
					QString strEmail2(strEmail);
					strEmail2.insert(0, "<");
					strEmail2 += ">";

					int nLinkRole = 0;
					if(strEmailFrom.indexOf(strEmail2) >= 0)
						nLinkRole = 1;
					else if(strEmailTo.indexOf(strEmail2) >= 0)
						nLinkRole = 2;
					else if(strEmailCC.indexOf(strEmail2) >= 0)
						nLinkRole = 3;
					else if(strEmailBCC.indexOf(strEmail2) >= 0)
						nLinkRole = 4;

					if(nLinkRole > 0)
						lstContactLinks.setData(nRow, "BNMR_SYSTEM_ROLE", nLinkRole);

					m_lstData.setData(i, "CONTACTS", lstContactLinks);

					//update unassigned field - remove current email from it
					QString strUnassignedLocal;
					for(j=0; j<nLocEmls; j++)
					{
						if(j != nIdxFound){
							QString strEmail = lstLocalEmls[j];
							if(!strUnassignedLocal.isEmpty())
								strUnassignedLocal += ";";
							strUnassignedLocal += strEmail;
						}
						m_lstData.setData(i, "EMAIL_UNASSIGNED", strUnassignedLocal);
					}
				}	// nIdxFound > 0
			}	// for

			if(bAnyChange)
				ui.tableData->RefreshDisplay();

			//remove this email from combo with unassigned
			if(nCurSel >= 0){
				ui.cboUnassignedEmails->removeItem(nCurSel);
				ui.cboUnassignedEmails->setCurrentIndex(0);
			}

			//now clear contact info for next "victim"
			ui.frameContact->Clear();
		}
	}
}

void FUI_ImportEmail::FilterEmailsByContact(Status status, DbRecordSet &lstData, bool bSelectOnly)
{
	qDebug() << "FUI_ImportEmail::FilterEmailsByContact";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - filter mails by contact"));

	int nRows = lstData.getRowCount();
	for(int i=0; i<nRows; i++)
	{
		//lstData.Dump();

		//extract contacts for this email
		DbRecordSet lstContactLinks;
		lstData.getData(i, "CONTACTS", lstContactLinks);

		int nContactCnt = lstContactLinks.getRowCount();
		for(int j=0; j<nContactCnt; j++)
		{
			int nContactID = lstContactLinks.getDataRef(j, "BNMR_TABLE_KEY_ID_2").toInt();
			//lstContactLinks.setData(nRow, "CELC_LINK_ROLE_ID", nLinkRole);

			//get contact flags: BCNT_DO_NOT_SYNC_MAIL, BCNT_SYNC_MAIL_AS_PRIV
			DbRecordSet lstContactInfo;
			QString strWhere= QString(" WHERE BCNT_ID=%1").arg(nContactID);
			MainEntityFilter filter;
			filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
			_SERVER_CALL(BusContact->ReadData(status, filter.Serialize(), lstContactInfo));
			_CHK_ERR_NO_MSG(status);	// no msg due to multithreading

			//now use flags to filter the list
			if( lstContactInfo.getRowCount() > 0)
			{
				if(lstContactInfo.getDataRef(0, "BCNT_DO_NOT_SYNC_MAIL").toInt() > 0)
				{
					if(bSelectOnly)
					{
						lstData.selectRow(i);
					}
					else
					{
						//remove the row from the recordset
						lstData.deleteRow(i);
						i --; nRows --;
					}
				}
				else if(lstContactInfo.getDataRef(0, "BCNT_SYNC_MAIL_AS_PRIV").toInt() > 0)
				{
					//mark as private before importing
					//lstData.setData(i, "CENT_IS_PRIVATE", 1);
					DbRecordSet lstUAR,lstGAR;
					AccUARCore::CreatePrivateUAR(g_pClientManager->GetPersonID(),lstData.getDataRef(i,"BEM_ID").toInt(),BUS_EMAIL,lstUAR,lstGAR);
					lstData.setData(i, "UAR_TABLE", lstUAR);
					lstData.setData(i, "GAR_TABLE", lstGAR);
					//lstUAR.Dump();
				}
			}
		}
	}
}

void FUI_ImportEmail::on_cboImportSource_currentIndexChanged(int nIdx)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - import source combo changed"));

	if(0 == nIdx){
		ui.labelOutlookDropOnly->show();

		ui.radNoFilter		->hide();	
		ui.radUseFilter		->hide();
		ui.label			->hide();
		ui.txtFilterFromDate->hide();
		ui.label_2			->hide();
		ui.txtFilterToDate	->hide();
		ui.label_4			->hide();
		ui.txtFilterFromField	->hide();
		ui.label_5			->hide();
		ui.txtFilterToField	->hide();
		ui.label_6			->hide();
		ui.txtFilterCCField	->hide();
		ui.label_7			->hide();
		ui.txtFilterBCCField->hide();
		ui.label_8			->hide();
		ui.txtFilterSubjectField ->hide();
		ui.btnSelectSourceFolders->hide();
		ui.radOR			->hide();
		ui.radAND			->hide();
		ui.btnBuildList		->hide();
	}
	else{
		ui.labelOutlookDropOnly->hide();

		ui.radNoFilter		->show();	
		ui.radUseFilter		->show();
		ui.label			->show();
		ui.txtFilterFromDate->show();
		ui.label_2			->show();
		ui.txtFilterToDate	->show();
		ui.label_4			->show();
		ui.txtFilterFromField	->show();
		ui.label_5			->show();
		ui.txtFilterToField	->show();
		ui.label_6			->show();
		ui.txtFilterCCField	->show();
		ui.label_7			->show();
		ui.txtFilterBCCField->show();
		ui.label_8			->show();
		ui.txtFilterSubjectField ->show();
		ui.btnSelectSourceFolders->show();
		ui.radOR			->show();
		ui.radAND			->show();
		ui.btnBuildList		->show();
	}
}


//for one mail filters biggest attachment until sum <16mb
void FUI_ImportEmail::FilterAttachmentRecords(DbRecordSet &lstAttachments)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - filter attachment records"));

	while(lstAttachments.getStoredByteSize()>MAX_ATTACHMENT_LIST_SIZE)
	{
		int nMaxAttach=0;
		int nRowWithBiggestAttachment=-1;
		int nSize=lstAttachments.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nAttchSize=lstAttachments.getDataRef(i,"BEA_CONTENT").toByteArray().size();
			if (nAttchSize>nMaxAttach)
			{
				nMaxAttach=nAttchSize;
				nRowWithBiggestAttachment=i;
			}
		}
		if (nRowWithBiggestAttachment==-1)
		{
			Q_ASSERT(false); //this means that list >40Mb and no attachments!?
			return;
		}
		QString strName = lstAttachments.getDataRef(nRowWithBiggestAttachment, "BEA_NAME").toString();
		//QByteArray datNewContents = qCompress(tr("Attachment '%1� is too big (%d MB), Skipped!")).arg(strName).arg(nAttSizeMB).toLatin1());
		QByteArray datNewContents = tr("Attachment size limits reached. '%1� Skipped!").arg(strName).toLatin1();
		datNewContents = qCompress(datNewContents,1);
		lstAttachments.setData(nRowWithBiggestAttachment, "BEA_CONTENT", datNewContents);
		//overwrite name
		strName = tr("Attachment Skipped � Read Info.txt");
		lstAttachments.setData(nRowWithBiggestAttachment, "BEA_NAME", strName);

		g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,0,QString(tr("Attachment %1 are not imported, maximum size is exceeded")).arg(lstAttachments.getDataRef(nRowWithBiggestAttachment,"BEA_NAME").toString()));
	}
}

void FUI_ImportEmail::OnCloseWindow()
{
	g_objFuiManager.CloseFUI(g_objFuiManager.GetFuiID(this));
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - close window done"));
}
