#ifndef ADMINTOOL_H
#define ADMINTOOL_H

#include <QtWidgets/QDialog>
#include <QMenu>
#include "generatedfiles/ui_admintool.h"
#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "bus_client/bus_client/selection_combobox.h"
#include "bus_client/bus_client/clientbackupmanager.h"
#include "bus_client/bus_client/clientimportexportmanager.h"

class AdminTool : public QDialog
{
	Q_OBJECT

public:
	AdminTool(QWidget *parent = 0);
	~AdminTool();

	bool Initialize(); //if failed, never open

private slots:
	void on_btnBackup_clicked();
	void on_btnRestore_clicked();
	void on_btnRefresh_clicked();
	void on_btnSelectBackup_clicked();
	void OnMenuTriggerWeek(QAction *action);
	void OnMenuTriggerMonth(QAction *action);
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_groupBoxSchedule_clicked();
	void on_btnTemplate_clicked();
	void on_ckbOldBackup_stateChanged(int);

	
	void on_ckbEnable_Project_stateChanged(int);
	void on_rbScanPeriod_Project_toggled(bool);
	void on_rbLastN_Project_toggled(bool);
	void on_btnSelectPath_Project_clicked();
	void on_btnProcess_Project_clicked();
	void on_btnSelectFTP_Project_clicked();
	void on_rbDir_Project_toggled(bool);
		
	void on_ckbEnable_User_stateChanged(int);
	void on_rbScanPeriod_User_toggled(bool);
	void on_rbLastN_User_toggled(bool);
	void on_btnSelectPath_User_clicked();
	void on_btnProcess_User_clicked();
	void on_btnSelectFTP_User_clicked();
	void on_rbDir_User_toggled(bool);

	void on_ckbEnable_Debtor_stateChanged(int);
	void on_rbScanPeriod_Debtor_toggled(bool);
	void on_rbLastN_Debtor_toggled(bool);
	void on_btnSelectPath_Debtor_clicked();
	void on_btnProcess_Debtor_clicked();
	void on_btnSelectFTP_Debtor_clicked();
	void on_rbDir_Debtor_toggled(bool);

	void on_ckbEnable_Contact_stateChanged(int);
	void on_rbScanPeriod_Contact_toggled(bool);
	void on_rbLastN_Contact_toggled(bool);
	void on_btnSelectPath_Contact_clicked();
	void on_btnProcess_Contact_clicked();
	void on_btnSelectFTP_Contact_clicked();
	void on_rbDir_Contact_toggled(bool);

	void on_ckbEnable_SPL_stateChanged(int);
	void on_rbScanPeriod_SPL_toggled(bool);
	void on_btnSelectPath_SPL_clicked();
	void on_btnProcess_SPL_clicked();
	void on_btnSelectFTP_SPL_clicked();
	void on_rbDir_SPL_toggled(bool);

/*
	void on_btnSelectHeadersFile_clicked();
	void on_btnSelectDetailsFile_clicked();
	void on_btnImportStoredProjLists_clicked();
*/
	void OnBackup_Download();
	void OnBackup_Upload();
	void OnBackup_Delete();

	void OnCommunicationServerMessageRecieved(DbRecordSet rowMsg);

private:
	Ui::AdminToolClass ui;

	void SetupDayFromAction(QString strText,QPushButton *btnDay);
	void SetupDayFromAction(int nDayInWeek,QPushButton *btnDay);
	int GetDayFromButton(QPushButton *btnDay);
	void SaveDatabaseOptions();
	bool SaveImportExportOptions();
	void RefreshEnabledWidgets_Project();
	void RefreshEnabledWidgets_User();
	void RefreshEnabledWidgets_Debtor();
	void RefreshEnabledWidgets_Contact();
	void RefreshEnabledWidgets_SPL();
	bool ReloadFTPServers();
	void SaveFTPID();
	void LoadFTPID();
	void CheckOnLoad();
	void SaveRoleID();
	void LoadRoleID();


	QMenu m_MenuDaysMonth;
	QMenu m_MenuDaysWeek;

	int m_nBackupFreq;		
	QString m_strBackupPath;		
	QString m_datBackupLastDate;		
	QString m_strRestoreOnNextStart;		
	QString m_strBackupTime;
	int m_nBackupDay;
	int m_nDeleteOldBackupsDay;

	QString m_strTemplateName;
	int m_nAskForTemplate;
	int m_nStartStartUpWizard;

	DbRecordSet m_lstBackups;
	DbRecordSet m_lstImportExportSettings;
	GuiFieldManager *m_GuiFieldManagerImportExport;
	GuiDataManipulator *m_pData;
	Selection_ComboBox m_AddressTypeHandler;
	ClientBackupManager *m_pBackupManager;
	ClientImportExportManager *m_ImportExportManager;	//import/exsport manager

};

#endif // ADMINTOOL_H

