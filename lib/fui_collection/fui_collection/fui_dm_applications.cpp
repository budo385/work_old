#include "fui_dm_applications.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/picturehelper.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;			

FUI_DM_Applications::FUI_DM_Applications(QWidget *parent)
	: FuiBase(parent)
{

	//-------------------------
	//		INIT SIMPLE FUI
	//-------------------------
	ui.setupUi(this);	
	InitFui(ENTITY_BUS_DM_APPLICATIONS,BUS_DM_APPLICATIONS,NULL,ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);

	//setup path buttons:
	ui.btnModifyIcon->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnModifyIcon->setToolTip(tr("Modify Application Icon"));
	
	connect(ui.btnModifyIcon,SIGNAL(clicked()),this,SLOT(OnModifyIcon()));

	//bring up field manager:
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData);
	m_GuiFieldManagerMain->RegisterField("BDMA_CODE",ui.txtCode);
	m_GuiFieldManagerMain->RegisterField("BDMA_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BDMA_EXTENSIONS",ui.txtFileExtensions);
	m_GuiFieldManagerMain->RegisterField("BDMA_OPEN_PARAMETER",ui.txtOpen);
	//m_GuiFieldManagerMain->RegisterField("BDMA_OPEN_PARAMETER",ui.txtOpen);
	m_GuiFieldManagerMain->RegisterField("BDMA_READ_ONLY",ui.ckbReadOnly);
	m_GuiFieldManagerMain->RegisterField("BDMA_NO_FILE_DOCUMENT",ui.ckbNoFile);
	m_GuiFieldManagerMain->RegisterField("BDMA_DESCRIPTION",ui.frameEditor);
	m_GuiFieldManagerMain->RegisterField("BDMA_ENCODE_PARAMETERS",ui.ckbEncodeParams);
	m_GuiFieldManagerMain->RegisterField("BDMA_SET_READ_ONLY_FLAG",ui.ckbReadOnlyCopy);
	
	
	//Type:
	QList<QRadioButton*> lstRadioButons;
	lstRadioButons << ui.rb0 << ui.rb1;
	QList<QVariant> lstValues;
	lstValues << GlobalConstants::APP_TYPE_FILE << GlobalConstants::APP_TYPE_URL;
	m_GuiFieldManagerMain->RegisterField("BDMA_APPLICATION_TYPE",new GuiField_RadioButton("BDMA_APPLICATION_TYPE",lstRadioButons,lstValues,&m_lstData));


	ui.frameTemplate->Initialize(ENTITY_BUS_DM_TEMPLATES,&m_lstData,"BDMA_TEMPLATE_ID");
	ui.frameTemplateAppInstall->Initialize(ENTITY_BUS_DM_TEMPLATES,&m_lstData,"BDMA_INSTALL_APP_TEMPL_ID");
	ui.frameTemplateViewInstall->Initialize(ENTITY_BUS_DM_TEMPLATES,&m_lstData,"BDMA_INSTALL_VIEWER_TEMPL_ID");


	//set edit to false:
	SetMode(MODE_EMPTY);
}



FUI_DM_Applications::~FUI_DM_Applications()
{
	delete m_GuiFieldManagerMain;
}

QString FUI_DM_Applications::GetFUIName()
{
	return tr("External Applications");
}


void FUI_DM_Applications::RefreshDisplay()
{
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frameTemplate->RefreshDisplay();
	ui.frameTemplateAppInstall->RefreshDisplay();
	ui.frameTemplateViewInstall->RefreshDisplay();
	RefreshIcon();
}


void FUI_DM_Applications::SetMode(int nMode)
{

	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		m_GuiFieldManagerMain->SetEditMode(false);
		ui.frameTemplate->SetEditMode(false);
		ui.frameTemplateAppInstall->SetEditMode(false);
		ui.frameTemplateViewInstall->SetEditMode(false);
		ui.btnModifyIcon->setEnabled(false);
		ui.frameEditor->SetEditMode(false);
	}
	else
	{
		m_GuiFieldManagerMain->SetEditMode(true);
		ui.frameTemplate->SetEditMode(true);
		ui.frameTemplateAppInstall->SetEditMode(true);
		ui.frameTemplateViewInstall->SetEditMode(true);
		ui.btnModifyIcon->setEnabled(true);
		ui.frameEditor->SetEditMode(true);
		ui.txtCode->setFocus(Qt::OtherFocusReason);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}




//attach icon:
void FUI_DM_Applications::OnModifyIcon()
{
	QPixmap pix;
	QString strFormat;
	bool bOK=PictureHelper::LoadPicture(pix,strFormat);
	if (!bOK || m_lstData.getRowCount()!=1)return;
	
	QByteArray picture;
	PictureHelper::ConvertPixMapToByteArray(picture,pix,strFormat);
	m_lstData.setData(0,"BDMA_ICON",picture);
	ui.labelIcon->setPixmap(pix);
}

void FUI_DM_Applications::RefreshIcon()
{
	if (m_lstData.getRowCount()==1)
	{
		QByteArray bytePicture=m_lstData.getDataRef(0,"BDMA_ICON").toByteArray();
		QPixmap pixMap;
		pixMap.loadFromData(bytePicture);
		ui.labelIcon->setPixmap(pixMap);
	}
	else
	{
		ui.labelIcon->clear();
	}
	

}


void FUI_DM_Applications::DataWrite(Status &err)
{
	//check values: path+application id must be set
	if(m_lstData.getDataRef(0,"BDMA_CODE").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,tr("Application code is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"BDMA_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,tr("Application name is mandatory field!"));	return;	}

	_SERVER_CALL(BusDocuments->WriteApplication(err,m_lstData,m_strLockedRes)); 
	if (err.IsOK())
	{
		m_strLockedRes.clear();
		m_lstDataCache=m_lstData;
	}
}


bool FUI_DM_Applications::on_cmdOK()
{
	if(FuiBase::on_cmdOK())
	{
		close();
		return true;
	}
	else
		return false;
}

void FUI_DM_Applications::on_cmdCancel()
{
	FuiBase::on_cmdCancel();
	close();
}


bool FUI_DM_Applications::on_cmdDelete()
{
	if(FuiBase::on_cmdDelete())
	{
		close();
		return true;
	}
	else
		return false;
}


bool FUI_DM_Applications::CanClose()
{
	return (m_bUnconditionalClose ||(m_nMode != MODE_EDIT));
}


void FUI_DM_Applications::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Escape)
	{
		on_cmdCancel();
		return;
	}

	FuiBase::keyPressEvent(event);
}



void FUI_DM_Applications::DataPrepareForInsert(Status &err)
{
	FuiBase::DataPrepareForInsert(err);
	m_lstData.setData(0,"BDMA_APPLICATION_TYPE",GlobalConstants::APP_TYPE_FILE);
	m_lstData.setData(0,"BDMA_SET_READ_ONLY_FLAG",1);
}
