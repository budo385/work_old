#include "voicecallredirectdlg.h"

//global message dispatcher
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;	
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;


VoiceCallRedirectDlg::VoiceCallRedirectDlg(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	ui.frameSelection->Initialize();
	connect(ui.frameSelection,SIGNAL(ShowContactDetails(int)),this,SLOT(OnShowContactDetails(int)));

	ui.frameContact->Initialize(ENTITY_BUS_CONTACT);
	ui.frameContact->GetButton(Selection_SAPNE_FUI::BUTTON_SELECT)->hide();
	ui.frameContact->GetButton(Selection_SAPNE_FUI::BUTTON_REMOVE)->hide();

	ui.btnExtSearch->setIcon(QIcon(":EarthSearch.png"));
	ui.btnExtSearch->hide();

	//TOFIX dynamic list?
	ui.cboInterface->addItem("Skype");
	ui.cboInterface->addItem("Other");
	ui.cboInterface->setCurrentIndex(0);

	ui.cboPhoneNumber->setFocus();
	ui.frameContact->registerObserver(this);


	//first try personal setting, then options
	int nGroupID = g_pSettings->GetPersonSetting(COMM_SETTINGS_TRANSFER_GROUP).toInt();
	if(nGroupID <= 0)
		nGroupID = g_pSettings->GetApplicationOption(COMM_OPTIONS_TRANSFER_GROUP).toInt();

	if(nGroupID > 0)
		ui.frameSelection->SelectGroupContacts(nGroupID);
}

VoiceCallRedirectDlg::~VoiceCallRedirectDlg()
{
}

void VoiceCallRedirectDlg::on_btnExtSearch_clicked()
{
	QMessageBox::information(NULL, "", "Not implemented yet!");
}

void VoiceCallRedirectDlg::on_btnOK_clicked()
{
	m_strNumber = ui.cboPhoneNumber->currentText();
	accept();
}

void VoiceCallRedirectDlg::on_btnCancel_clicked()
{
	reject();
}

void VoiceCallRedirectDlg::LoadContactData()
{
	//reload phones:
	Status err;
	int nContactId;
	DbRecordSet lstPhones;
	DbRecordSet lstRead;
	DbRecordSet rowCurrentContact;
	if(ui.frameContact->GetCurrentEntityRecord(nContactId,rowCurrentContact))
	{
		_SERVER_CALL(BusContact->ReadContactPhones(err,nContactId,lstPhones))//to fix: one call
		if (!err.IsOK()) return;
	}

	//load phones:
	LoadPhoneCombo(lstPhones);
}

void VoiceCallRedirectDlg::LoadPhoneCombo(DbRecordSet &lstPhones)
{
	ui.cboPhoneNumber->blockSignals(true);
	ui.cboPhoneNumber->clear();
	int nSize=lstPhones.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.cboPhoneNumber->addItem(lstPhones.getDataRef(i,"BCMP_SEARCH").toString(),lstPhones.getDataRef(i,"BCMP_FULLNUMBER"));
	}

	//set current phone to default
	int nDefRow=lstPhones.find(lstPhones.getColumnIdx("BCMP_IS_DEFAULT"),1,true);
	if (nDefRow!=-1)
	{
		ui.cboPhoneNumber->setCurrentIndex(nDefRow);
	}

	ui.cboPhoneNumber->blockSignals(false);
}

void VoiceCallRedirectDlg::OnShowContactDetails(int nContactID)
{
	//new contact selected at left-side selector widget (propagate changes)
	ui.frameContact->SetCurrentEntityRecord(nContactID);
}


void VoiceCallRedirectDlg::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	//selectors:
	if (pSubject == ui.frameContact && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		LoadContactData();
		return;
	}
}