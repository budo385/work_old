#include "fui_calendarvieweditor.h"

#include "bus_client/bus_client/selection_tablebased.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/calendarhelpercore.h"
#include "common/common/datahelper.h"
#include "trans/trans/xmlutil.h"
#include <QInputDialog>
#include "bus_core/bus_core/globalconstants.h"
#include "bus_core/bus_core/customavailability.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/selectionpopup.h"
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger
#include <QProgressDialog>

#include <QMap>

FUI_CalendarViewEditor::FUI_CalendarViewEditor(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();
	g_ChangeManager.registerObserver(this);

	SetFUIName(tr("Calendar Views"));

	//init roles list
	m_pSelectorWidget = new Selection_TableBased;
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->Initialize(ENTITY_CALENDAR_VIEW, false, false, true, true, true); //enable double click!
	ui.Views_stackedWidget->addWidget(m_pSelectorWidget);
	ui.Views_stackedWidget->setCurrentWidget(m_pSelectorWidget);
	connect(m_pSelectorWidget, SIGNAL(SelectionChanged()), this, SLOT(OnViewSelectionChanged()));

	//init time period combo
	ui.cboTimePeriod->addItem("-", DataHelper::UNDEFINED);
	ui.cboTimePeriod->addItem(tr("Day"), DataHelper::DAY);
	ui.cboTimePeriod->addItem(tr("3 Days"), DataHelper::THREE_DAYS);
	ui.cboTimePeriod->addItem(tr("Business Days"), DataHelper::BUSS_DAYS);
	ui.cboTimePeriod->addItem(tr("Week"), DataHelper::WEEK);
	ui.cboTimePeriod->addItem(tr("Two Weeks"), DataHelper::TWO_WEEK);
	ui.cboTimePeriod->addItem(tr("Month"), DataHelper::MONTH);
	ui.cboTimePeriod->addItem(tr("Year"), DataHelper::YEAR);
	ui.cboTimePeriod->setCurrentIndex(0);

	//init types combo
	MainEntitySelectionController TypeHandler;
	TypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_CALENDAR_CALENDAR_VIEW);
	TypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	TypeHandler.ReloadData();
	m_recCalViewTypes = *TypeHandler.GetDataSource();
	//m_recCalViewTypes.Dump();
	int nRowCount = m_recCalViewTypes.getRowCount();
	for(int i=0; i<nRowCount; i++){
		ui.cboType->addItem(m_recCalViewTypes.getDataRef(i, "BCMT_TYPE_NAME").toString(), m_recCalViewTypes.getDataRef(i, "BCMT_ID").toInt());
	}

	OnViewSelectionChanged();

#ifndef _DEBUG
	if(!CustomAvailability::IsAvailable_AS_CalendarViewWizard()){
		ui.btnRegenerateViewsAnesthesists->hide();
	}
#endif
}

FUI_CalendarViewEditor::~FUI_CalendarViewEditor()
{
}

void FUI_CalendarViewEditor::on_btnWriteToCalendarViews_clicked()
{
	//modify selected views
	//get selected views in the view list
	int nEntityRecordID;
	QString strCode;
	QString strName;
	DbRecordSet lstEntityRecord;
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->GetSelection(nEntityRecordID, strCode, strName, lstEntityRecord);
	//lstEntityRecord.Dump();
	int nSelectedCnt = lstEntityRecord.getRowCount();
	if(nSelectedCnt <= 0){
		QMessageBox::information(NULL, "", tr("No views selected!"));
		return;
	}

	//get parameters from GUI
	int nTimePeriodIdx = ui.cboTimePeriod->currentIndex();
	int nTimePeriodCode = (nTimePeriodIdx >= 0) ? ui.cboTimePeriod->itemData(nTimePeriodIdx).toInt() : DataHelper::UNDEFINED;
	
	int nCalViewTypeIdx = ui.cboType->currentIndex();
	int nCalViewTypeCode = (nCalViewTypeIdx >= 0) ? m_recCalViewTypes.getDataRef(nCalViewTypeIdx, "BCMT_ID").toInt() : 0;

	bool bShowOverviewCalendar = (ui.chkShowOverviewCal->checkState()==Qt::Checked);
	bool bShowDetailCalendar = (ui.chkShowDetailCal->checkState()==Qt::Checked);
	int nSections = (bShowOverviewCalendar &&  bShowDetailCalendar) ? 0 : (bShowOverviewCalendar)? 1 : 2;	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER

	bool bIsPublic = (ui.chkIsPublic->checkState()==Qt::Checked);

	if(!bShowOverviewCalendar  && !bShowDetailCalendar){
		QMessageBox::information(NULL, "", tr("You must show at least one calendar in the view!"));
		return;
	}

	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	//for each selected view
	for(int i=0; i<nSelectedCnt; i++)
	{
		int nCalViewID = lstEntityRecord.getDataRef(i, "BCALV_ID").toInt();

		DbRecordSet recCalendarView, recCalendarEntities, recCalendarTypeIDs;
		Status err;
		_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCalendarView, recCalendarEntities, recCalendarTypeIDs, nCalViewID));
		_CHK_ERR(err);

		//override the settings
		recCalendarView.setData(0, "BCALV_IS_PUBLIC", QVariant(bIsPublic).toInt());	//Is public.
		recCalendarView.setData(0, "BCALV_TYPE", nCalViewTypeCode);		
		recCalendarView.setData(0, "BCALV_SECTIONS", nSections);
		recCalendarView.setData(0, "BCALV_DATE_RANGE", nTimePeriodCode);

		_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nCalViewID, recCalendarView, recCalendarEntities, recCalendarTypeIDs));
		_CHK_ERR(err);
	}

	int nSelectSingleRecordID = -1;
	if(1 == nSelectedCnt)
		nSelectSingleRecordID=lstEntityRecord.getDataRef(0, "BCALV_ID").toInt();

	//reload calendar views list
	dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->ReloadData(true);

	//restore selection if only one item was selected
	if(nSelectSingleRecordID >= 0){
		dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->SetCurrentEntityRecord(nSelectSingleRecordID);
		
		//reload the list of the view fields
		OnViewSelectionChanged();
	}

	OnViewListModified();
}

void FUI_CalendarViewEditor::on_btnCreateCalendarView_clicked()
{
	//create a new view in the database

	//get view data
	QString strViewName = ui.txtName->text();
	if(strViewName.isEmpty()){
		QMessageBox::information(NULL, "", tr("View name must not be empty!"));
		return;
	}

	//get parameters from GUI
	int nTimePeriodIdx = ui.cboTimePeriod->currentIndex();
	int nTimePeriodCode = (nTimePeriodIdx >= 0) ? ui.cboTimePeriod->itemData(nTimePeriodIdx).toInt() : DataHelper::UNDEFINED;
	
	int nCalViewTypeIdx = ui.cboType->currentIndex();
	int nCalViewTypeCode = (nCalViewTypeIdx >= 0) ? m_recCalViewTypes.getDataRef(nCalViewTypeIdx, "BCMT_ID").toInt() : 0;

	bool bShowOverviewCalendar = (ui.chkShowOverviewCal->checkState()==Qt::Checked);
	bool bShowDetailCalendar = (ui.chkShowDetailCal->checkState()==Qt::Checked);
	bool bIsPublic = (ui.chkIsPublic->checkState()==Qt::Checked);

	if(!bShowOverviewCalendar  && !bShowDetailCalendar){
		QMessageBox::information(NULL, "", tr("You must show at least one calendar in the view!"));
		return;
	}
	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	int nSections = (bShowOverviewCalendar &&  bShowDetailCalendar) ? 0 : (bShowOverviewCalendar)? 1 : 2;	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER

	DbRecordSet recView, recCalEntities, recCalTypeIDs;
	CalendarHelperCore::CreateCalendarViewRecordset(recView, strViewName, 0, QVariant(bIsPublic).toInt(), nCalViewTypeCode, nLoggedPersonID, nSections, nTimePeriodCode);

	//now create a new view
	int nViewID = -1;
	Status err;
	_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nViewID, recView, recCalEntities, recCalTypeIDs));
	_CHK_ERR(err);

	//reload calendar views list
	dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->ReloadData(true);

	//select new view within the reloaded list
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->SetCurrentEntityRecord(nViewID);

	//reload the list of the view fields
	OnViewSelectionChanged();

	OnViewListModified();
}

void FUI_CalendarViewEditor::on_btnAddProjects_clicked()
{
	AddCalendarViewItemTypes(ENTITY_BUS_PROJECT);
}

void FUI_CalendarViewEditor::on_btnRemoveProjects_clicked()
{
	RemoveCalendarViewItemTypes(ENTITY_BUS_PROJECT);
}

void FUI_CalendarViewEditor::on_btnAddContacts_clicked()
{
	AddCalendarViewItemTypes(ENTITY_BUS_CONTACT);
}

void FUI_CalendarViewEditor::on_btnRemoveContacts_clicked()
{
	RemoveCalendarViewItemTypes(ENTITY_BUS_CONTACT);
}

void FUI_CalendarViewEditor::on_btnAddResources_clicked()
{
	AddCalendarViewItemTypes(ENTITY_CALENDAR_RESOURCES);
}

void FUI_CalendarViewEditor::on_btnRemoveResources_clicked()
{
	RemoveCalendarViewItemTypes(ENTITY_CALENDAR_RESOURCES);
}

void FUI_CalendarViewEditor::on_btnRemoveUsers_clicked()
{
	RemoveCalendarViewItemTypes(ENTITY_BUS_PERSON);
}

void FUI_CalendarViewEditor::on_btnAddUsers_clicked()
{
	AddCalendarViewItemTypes(ENTITY_BUS_PERSON);
}

void FUI_CalendarViewEditor::OnViewSelectionChanged()
{
	//get view list selection
	int nEntityRecordID;
	QString strCode;
	QString strName;
	DbRecordSet lstEntityRecord;
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->GetSelection(nEntityRecordID, strCode, strName, lstEntityRecord);
	//lstEntityRecord.Dump();

	//rebuild the tree view on the right
	ui.lstSelectedView->clear();

	int nSelectedCnt = lstEntityRecord.getRowCount();
	if(1 == nSelectedCnt)
	{
		ui.lstSelectedView->setEnabled(true);
		ui.btnMoveUp->setEnabled(true);
		ui.btnMoveDown->setEnabled(true);

		//selected calendar view
		int nCalViewID = -1;
		lstEntityRecord.getData(0, "BCALV_ID", nCalViewID);

		//logged person parameter
		int nLoggedPersonID = g_pClientManager->GetPersonID();

		//now fetch the big chunk describing all calendar views available for current user (+ public ones)
		DbRecordSet recCalendarView, recCalendarEntities, recCalTypeIDs;
		Status err;
		_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCalendarView, recCalendarEntities, recCalTypeIDs, nCalViewID));
		_CHK_ERR(err);

//		_DUMP(recCalendarEntities);

		//init main fields
		ui.txtName->setText(recCalendarView.getDataRef(0, "BCALV_NAME").toString());

		int nCalTypeCode = recCalendarView.getDataRef(0, "BCALV_TYPE").toInt();
		int nCalTypeIdx = ui.cboType->findData(QVariant(nCalTypeCode));
		ui.cboType->setCurrentIndex(nCalTypeIdx);

		int nTimePeriodCode = recCalendarView.getDataRef(0, "BCALV_DATE_RANGE").toInt();
		int nTimeItemIdx = ui.cboTimePeriod->findData(QVariant(nTimePeriodCode));
		ui.cboTimePeriod->setCurrentIndex(nTimeItemIdx);

		int nCalSections = recCalendarView.getDataRef(0, "BCALV_SECTIONS").toInt();	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER 
		ui.chkShowOverviewCal->setCheckState(nCalSections < 2 ? Qt::Checked : Qt::Unchecked);
		ui.chkShowDetailCal->setCheckState(nCalSections != 1 ? Qt::Checked : Qt::Unchecked);
		ui.chkIsPublic->setCheckState(recCalendarView.getDataRef(0, "BCALV_IS_PUBLIC").toInt() > 0 ? Qt::Checked : Qt::Unchecked);

		//fill the list view
		int nRowCount = recCalendarEntities.getRowCount();
		for(int i=0; i<nRowCount; i++)
		{
			QStringList lstLabels;

			if (recCalendarEntities.getDataRef(i,"BCVE_USER_ID").toInt()>0)
			{
				lstLabels << recCalendarEntities.getDataRef(i, "BPER_NAME").toString();
			}
			else if (recCalendarEntities.getDataRef(i,"BCVE_PROJECT_ID").toInt()>0)
			{
				lstLabels << recCalendarEntities.getDataRef(i, "BUSP_NAME").toString();
			}
			else if (recCalendarEntities.getDataRef(i,"BCVE_CONTACT_ID").toInt()>0)
			{
				lstLabels << recCalendarEntities.getDataRef(i, "BCNT_NAME").toString();
			}
			else if (recCalendarEntities.getDataRef(i,"BCVE_RESOURCE_ID").toInt()>0)
			{
				lstLabels << recCalendarEntities.getDataRef(i, "BRES_NAME").toString();
			}

			QTreeWidgetItem *item = new QTreeWidgetItem(lstLabels);
			ui.lstSelectedView->addTopLevelItem(item);
		}
	}
	else
	{
		//init main fields
		ui.txtName->setText("");
		ui.cboTimePeriod->setCurrentIndex(-1);
		ui.cboType->setCurrentIndex(-1);
		ui.chkShowOverviewCal->setCheckState(Qt::Checked);
		ui.chkShowDetailCal->setCheckState(Qt::Checked);
		ui.chkIsPublic->setCheckState(Qt::Checked);

		ui.lstSelectedView->setEnabled(false);
		ui.btnMoveUp->setEnabled(false);
		ui.btnMoveDown->setEnabled(false);
	}
}

void FUI_CalendarViewEditor::on_btnDeleteSelectedViews_clicked()
{
	//get selected views in the view list
	int nEntityRecordID;
	QString strCode;
	QString strName;
	DbRecordSet lstEntityRecord;
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->GetSelection(nEntityRecordID, strCode, strName, lstEntityRecord);
	//lstEntityRecord.Dump();
	int nSelectedCnt = lstEntityRecord.getRowCount();
	if(nSelectedCnt <= 0){
		QMessageBox::information(NULL, "", tr("No views selected!"));
		return;
	}

	//for each selected view
	for(int i=0; i<nSelectedCnt; i++)
	{
		int nCalViewID = -1;
		lstEntityRecord.getData(i, "BCALV_ID", nCalViewID);
		qDebug() << "Delete Clandar View ID=" << nCalViewID;

		Status err;
		_SERVER_CALL(BusCalendar->DeleteCalFilterView(err, nCalViewID));
		_CHK_ERR(err);
	}

	//reload calendar views list
	dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->ReloadData(true);

	OnViewListModified();
}

void FUI_CalendarViewEditor::GetDataFromEntity(int nEntityCode, int &nEntityType, QString &strIdField, QString &strNameField, QString &strPersonIdField)
{
	switch(nEntityCode){
		case ENTITY_BUS_PROJECT:
			nEntityType = GlobalConstants::TYPE_CAL_PROJECT_SELECT;
			strIdField = "BUSP_ID";
			strNameField = "BUSP_NAME";
			break;
		case ENTITY_BUS_PERSON:
			nEntityType = GlobalConstants::TYPE_CAL_USER_SELECT;
			strIdField = "BPER_CONTACT_ID";
			strPersonIdField = "BPER_ID";
			strNameField = "BPER_NAME";
			break;
		case ENTITY_CALENDAR_RESOURCES:
			nEntityType = GlobalConstants::TYPE_CAL_RESOURCE_SELECT;
			strIdField = "BRES_ID";
			strNameField = "BRES_NAME";
			break;
		case ENTITY_BUS_CONTACT:
			nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;
			strIdField = "BCNT_ID";
			strNameField = "BCNT_NAME";
			break;
		default:
			Q_ASSERT(false);
	}
}

void FUI_CalendarViewEditor::RemoveCalendarViewItemTypes(int nEntityCode)
{
	//get db field names for this entity type
	int nEntityType;
	QString strIdField;
	QString strNameField;
	QString strPersonIdField;
	GetDataFromEntity(nEntityCode, nEntityType, strIdField, strNameField, strPersonIdField);

	//select entity items to be deleted from selected views
	SelectionPopup dlg;
	dlg.Initialize(nEntityCode, true);
	int nResult=dlg.OpenSelector();
	if(nResult==0)
		return;

	int nRecID;
	DbRecordSet lstData;
	dlg.GetSelectedData(nRecID,lstData);
	int nRowCount = lstData.getRowCount();
	if(nRowCount < 1)
		return;
	
	//_DUMP(lstData);

	//get selected views in the view list
	int nEntityRecordID;
	QString strCode;
	QString strName;
	DbRecordSet lstEntityRecord;
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->GetSelection(nEntityRecordID, strCode, strName, lstEntityRecord);
	int nSelectedViewsCnt = lstEntityRecord.getRowCount();

	//_DUMP(lstEntityRecord);

	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	//if selection, apply to selected views
	for (int i=0; i<nSelectedViewsCnt; i++)
	{
		int nCalViewID= lstEntityRecord.getDataRef(i, "BCALV_ID").toInt();

		DbRecordSet recCalendarView, recCalendarEntities, recCalendarTypeIDs;
		Status err;
		_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCalendarView, recCalendarEntities, recCalendarTypeIDs, nCalViewID));
		_CHK_ERR(err);

		//Set correct recordset for entities.
		CalendarHelperCore::ConvertSelectEntityRecordsetToInsertEntityRecordset(recCalendarEntities);

		//_DUMP(recCalendarEntities);

		//for each selected item
		int nSelectedItems = lstData.getRowCount();
		for(int j=0; j<nSelectedItems; j++)
		{
			//QString strEntityName=lstData.getDataRef(j, strNameField).toString();
			int nEntityID = lstData.getDataRef(j, strIdField).toInt();
			int nEntityPersonID=(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT) ? lstData.getDataRef(j, strPersonIdField).toInt() : -1;

			CalendarHelperCore::RemoveEntityRowFromEntityRecordset(recCalendarEntities, nEntityType, nEntityID, nEntityPersonID);
		}

		//_DUMP(recCalendarEntities);

		_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nCalViewID, recCalendarView, recCalendarEntities, recCalendarTypeIDs));
		_CHK_ERR(err);
	}

	if(nSelectedViewsCnt > 0)
	{
		//reload the list of the view fields
		OnViewSelectionChanged();
	}

	OnViewListModified();
}

void FUI_CalendarViewEditor::AddCalendarViewItemTypes(int nEntityCode)
{
	//get db field names for this entity type
	int nEntityType;
	QString strIdField;
	QString strNameField;
	QString strPersonIdField;
	GetDataFromEntity(nEntityCode, nEntityType, strIdField, strNameField, strPersonIdField);

	//select entity items to be inserted into selected views
	SelectionPopup dlg;
	dlg.Initialize(nEntityCode,true);
	int nResult=dlg.OpenSelector();
	if(nResult==0)
		return;

	int nRecID;
	DbRecordSet lstData;
	dlg.GetSelectedData(nRecID,lstData);
	int nRowCount = lstData.getRowCount();
	if(nRowCount < 1)
		return;
	
	//_DUMP(lstData);

	//get selected views in the view list
	int nEntityRecordID;
	QString strCode;
	QString strName;
	DbRecordSet lstEntityRecord;
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->GetSelection(nEntityRecordID, strCode, strName, lstEntityRecord);
	int nSelectedViewsCnt = lstEntityRecord.getRowCount();

	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	int nSelectSingleRecordID = -1;

	//if selection, apply to selected views
	if(nSelectedViewsCnt > 0)
	{
		for (int i=0; i<nSelectedViewsCnt; i++)
		{
			int nCalViewID= lstEntityRecord.getDataRef(i, "BCALV_ID").toInt();

			DbRecordSet recCalendarView, recCalendarEntities, recCalendarTypeIDs;
			Status err;
			_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCalendarView, recCalendarEntities, recCalendarTypeIDs, nCalViewID));
			_CHK_ERR(err);

			//Set correct recordset for entities.
			CalendarHelperCore::ConvertSelectEntityRecordsetToInsertEntityRecordset(recCalendarEntities);

			//_DUMP(recCalendarEntities);

			//for each selected item
			int nSelectedItems = lstData.getRowCount();
			for(int j=0; j<nSelectedItems; j++)
			{
				//QString strEntityName=lstData.getDataRef(j, strNameField).toString();
				int nEntityID = lstData.getDataRef(j, strIdField).toInt();
				int nEntityPersonID=(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT) ? lstData.getDataRef(j, strPersonIdField).toInt() : -1;

				if (!CalendarHelperCore::CheckEntityExistsInEntityRecordset(recCalendarEntities, nEntityType, nEntityID, nEntityPersonID))
				{
					recCalendarEntities.addRow();
					int nLastRow=recCalendarEntities.getRowCount()-1;
					int nSortCode=0;
					if (nLastRow>0)
					{
						nSortCode=recCalendarEntities.getDataRef(nLastRow-1, "BCVE_SORT_CODE").toInt() + 1;
					}
					CalendarHelperCore::AddEntityRowToEntityRecordset(recCalendarEntities, nLastRow, nEntityType, nEntityID, nEntityPersonID, false, nSortCode);
				}
			}

			//_DUMP(recCalendarEntities);

			_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nCalViewID, recCalendarView, recCalendarEntities, recCalendarTypeIDs));
			_CHK_ERR(err);
			//_DUMP(recCalendarEntities);

			if(1 == nSelectedViewsCnt)
				nSelectSingleRecordID = nCalViewID;
		}
	}
	else
	{
		//create new views with the selection included

		//get view data
		QString strViewNamePrefix = ui.txtName->text();
	
		//get parameters from GUI
		int nTimePeriodIdx = ui.cboTimePeriod->currentIndex();
		int nTimePeriodCode = (nTimePeriodIdx >= 0) ? ui.cboTimePeriod->itemData(nTimePeriodIdx).toInt() : DataHelper::UNDEFINED;
		
		int nCalViewTypeIdx = ui.cboType->currentIndex();
		int nCalViewTypeCode = (nCalViewTypeIdx >= 0) ? m_recCalViewTypes.getDataRef(nCalViewTypeIdx, "BCMT_ID").toInt() : 0;

		bool bShowOverviewCalendar = (ui.chkShowOverviewCal->checkState()==Qt::Checked);
		bool bShowDetailCalendar = (ui.chkShowDetailCal->checkState()==Qt::Checked);
		bool bIsPublic = (ui.chkIsPublic->checkState()==Qt::Checked);

		if(!bShowOverviewCalendar  && !bShowDetailCalendar){
			QMessageBox::information(NULL, "", tr("You must show at least one calendar in the view!"));
			return;
		}

		bool bMultipleRecords = false;
		if(nRowCount > 1){
			//ask only if multiple records selected
			int nResult = QMessageBox::question(NULL, "", tr("Do you want to create a new view for each selected item?"), tr("Yes"),tr("No"));
			bMultipleRecords = (nResult==0);
		}

		//logged person parameter
		int nLoggedPersonID = g_pClientManager->GetPersonID();
		int nSections = (bShowOverviewCalendar &&  bShowDetailCalendar) ? 0 : (bShowOverviewCalendar)? 1 : 2;	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER

		if(bMultipleRecords)	//one new view for each selected item
		{
			//for each selected item
			int nSelectedItems = lstData.getRowCount();
			for(int j=0; j<nSelectedItems; j++)
			{
				QString strNameValue=lstData.getDataRef(j, strNameField).toString();
				QString strViewName = strViewNamePrefix + " " + strNameValue;

				//fill view record
				DbRecordSet recView, recCalEntities, recCalTypeIDs;
				CalendarHelperCore::CreateCalendarViewRecordset(recView, strViewName, 0, QVariant(bIsPublic).toInt(), nCalViewTypeCode, nLoggedPersonID, nSections, nTimePeriodCode);

				//fill in the entities field within the settings field
				int nEntityID = lstData.getDataRef(j, strIdField).toInt();
				int nEntityPersonID=(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT) ? lstData.getDataRef(j, strPersonIdField).toInt() : -1;

				//Set correct recordset for entities.
				recCalEntities.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW_ENTITIES));
				recCalEntities.addRow();
				CalendarHelperCore::AddEntityRowToEntityRecordset(recCalEntities, 0, nEntityType, nEntityID, nEntityPersonID, true, 0);

				int nViewID = -1;
				Status err;
				_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nViewID, recView, recCalEntities, recCalTypeIDs));
				_CHK_ERR(err);

				if(1 == nSelectedViewsCnt)
					nSelectSingleRecordID = nViewID;
			}
		}
		else
		{
			//one new view with all selected items
			QString strViewName = strViewNamePrefix;

			//fill view record
			DbRecordSet recView, recCalEntities, recCalTypeIDs;
			CalendarHelperCore::CreateCalendarViewRecordset(recView, strViewName, 0, QVariant(bIsPublic).toInt(), nCalViewTypeCode, nLoggedPersonID, nSections, nTimePeriodCode);
			recCalEntities.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW_ENTITIES));

			int nSelectedItems = lstData.getRowCount();
			for(int j=0; j<nSelectedItems; j++)
			{
				recCalEntities.addRow();

				int nEntityID = lstData.getDataRef(j, strIdField).toInt();
				int nEntityPersonID=(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT) ? lstData.getDataRef(j, strPersonIdField).toInt() : -1;

				bool bSelected = (j==0)?true:false;

				//Set correct recordset for entities.
				CalendarHelperCore::AddEntityRowToEntityRecordset(recCalEntities, j, nEntityType, nEntityID, nEntityPersonID, bSelected, j);
			}

			int nViewID = -1;
			Status err;
			_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nViewID, recView, recCalEntities, recCalTypeIDs));
			_CHK_ERR(err);

			if(1 == nSelectedViewsCnt)
				nSelectSingleRecordID = nViewID;
		}
	}

	//reload calendar views list
	//FIX: causes the selection on the left side to be lost
	//dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->ReloadData(true);

	//restore selection if only one item was selected
	if(nSelectSingleRecordID >= 0){
		dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->SetCurrentEntityRecord(nSelectSingleRecordID);
		
		//reload the list of the view fields
		OnViewSelectionChanged();
	}

	OnViewListModified();
}

void FUI_CalendarViewEditor::AddEntitiesToCalendarView(DbRecordSet recEntities, int nCalViewID)
{
	DbRecordSet recCalendarView, recCalendarEntities, recCalendarTypeIDs;
	Status err;
	_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCalendarView, recCalendarEntities, recCalendarTypeIDs, nCalViewID));
	_CHK_ERR(err);

	//Set correct recordset for entities.
	CalendarHelperCore::ConvertSelectEntityRecordsetToInsertEntityRecordset(recCalendarEntities);

	//for each selected item
	int nSelectedItems = recEntities.getRowCount();
	for(int j=0; j<nSelectedItems; j++)
	{
		int nEntityID = recEntities.getDataRef(j, "ENTITY_ID").toInt();
		int nEntityType = recEntities.getDataRef(j, "ENTITY_TYPE").toInt();
		int nEntityPersonID=(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT) ? recEntities.getDataRef(j, "ENTITY_PERSON_ID").toInt() : -1;

		if (!CalendarHelperCore::CheckEntityExistsInEntityRecordset(recCalendarEntities, nEntityType, nEntityID, nEntityPersonID))
		{
			recCalendarEntities.addRow();
			int nLastRow=recCalendarEntities.getRowCount()-1;
			int nSortCode=0;
			if (nLastRow>0)
			{
				nSortCode=recCalendarEntities.getDataRef(nLastRow-1, "BCVE_SORT_CODE").toInt() + 1;
			}
			CalendarHelperCore::AddEntityRowToEntityRecordset(recCalendarEntities, nLastRow, nEntityType, nEntityID, nEntityPersonID, false, nSortCode);
		}
	}

	_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nCalViewID, recCalendarView, recCalendarEntities, recCalendarTypeIDs));
	_CHK_ERR(err);
}

void FUI_CalendarViewEditor::RemoveEntitiesFromCalendarView(DbRecordSet recEntities, int nCalViewID)
{
	DbRecordSet recCalendarView, recCalendarEntities, recCalendarTypeIDs;
	Status err;
	_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCalendarView, recCalendarEntities, recCalendarTypeIDs, nCalViewID));
	_CHK_ERR(err);

	//Set correct recordset for entities.
	CalendarHelperCore::ConvertSelectEntityRecordsetToInsertEntityRecordset(recCalendarEntities);

	//for each selected item
	int nSelectedItems = recEntities.getRowCount();
	for(int j=0; j<nSelectedItems; j++)
	{
		int nEntityID = recEntities.getDataRef(j, "ENTITY_ID").toInt();
		int nEntityType = recEntities.getDataRef(j, "ENTITY_TYPE").toInt();
		int nEntityPersonID=(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT) ? recEntities.getDataRef(j, "ENTITY_PERSON_ID").toInt() : -1;

		CalendarHelperCore::RemoveEntityRowFromEntityRecordset(recCalendarEntities, nEntityType, nEntityID, nEntityPersonID);
	}

	_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nCalViewID, recCalendarView, recCalendarEntities, recCalendarTypeIDs));
	_CHK_ERR(err);
}

void FUI_CalendarViewEditor::on_btnMoveUp_clicked()
{
	//get view entries selection
	int nSelectedEntityIdx = ui.lstSelectedView->indexOfTopLevelItem(ui.lstSelectedView->currentItem());
	QModelIndexList lstSelected = ui.lstSelectedView->selectionModel()->selectedIndexes();
	if(lstSelected.isEmpty())
		return;
	qSort(lstSelected);

	//get view list selection
	int nEntityRecordID;
	QString strCode;
	QString strName;
	DbRecordSet lstEntityRecord;
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->GetSelection(nEntityRecordID, strCode, strName, lstEntityRecord);
	//lstEntityRecord.Dump();

	int nSelectedCnt = lstEntityRecord.getRowCount();
	if(1 == nSelectedCnt)
	{
		//selected calendar view
		int nCalViewID = lstEntityRecord.getDataRef(0, "BCALV_ID").toInt();

		//logged person parameter
		int nLoggedPersonID = g_pClientManager->GetPersonID();

		//now fetch the big chunk describing all calendar views available for current user (+ public ones)
		DbRecordSet recCalendarView, recCalendarEntities, recCalendarTypeIDs;
		Status err;
		_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCalendarView, recCalendarEntities, recCalendarTypeIDs, nCalViewID));
		_CHK_ERR(err);

		//Set correct recordset for entities.
		CalendarHelperCore::ConvertSelectEntityRecordsetToInsertEntityRecordset(recCalendarEntities);
		
		int nRows = lstSelected.length();
		for(int i=0; i<nRows; i++)
		{
			//move row up
			int nSelectedEntityIdx = lstSelected.at(i).row();
			if(nSelectedEntityIdx > 0)
			{
				recCalendarEntities.insertRow(nSelectedEntityIdx-1);
				DbRecordSet row = recCalendarEntities.getRow(nSelectedEntityIdx+1);
				recCalendarEntities.assignRow(nSelectedEntityIdx-1, row);
				recCalendarEntities.deleteRow(nSelectedEntityIdx+1);
			}
		}

		//Reorder sort codes
		CalendarHelperCore::ReorderEntityRecordsetSortCode(recCalendarEntities);

		_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nCalViewID, recCalendarView, recCalendarEntities, recCalendarTypeIDs));
		_CHK_ERR(err);

		qDebug() << "Updated Calendar View ID=" << nCalViewID;

		//reload the list of the view fields
		OnViewSelectionChanged();

		//select item
		int nNewPos = nSelectedEntityIdx-1;
		if(nNewPos < 0)
			nNewPos = 0;
		ui.lstSelectedView->setCurrentItem(ui.lstSelectedView->topLevelItem(nNewPos));

		OnViewListModified();
	}
}
	
void FUI_CalendarViewEditor::on_btnMoveDown_clicked()
{
	int nTotalEntitiesCnt = ui.lstSelectedView->topLevelItemCount();
	int nSelectedEntityIdx = ui.lstSelectedView->indexOfTopLevelItem(ui.lstSelectedView->currentItem());
	//if(nSelectedEntityIdx >= nTotalEntitiesCnt-1)
	//	return;	// no space "down"
	QModelIndexList lstSelected = ui.lstSelectedView->selectionModel()->selectedIndexes();
	if(lstSelected.isEmpty())
		return;
	qSort(lstSelected);

	//get view list selection
	int nEntityRecordID;
	QString strCode;
	QString strName;
	DbRecordSet lstEntityRecord;
	dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->GetSelection(nEntityRecordID, strCode, strName, lstEntityRecord);
	//lstEntityRecord.Dump();

	int nSelectedCnt = lstEntityRecord.getRowCount();
	if(1 == nSelectedCnt)
	{
		//selected calendar view
		int nCalViewID = lstEntityRecord.getDataRef(0, "BCALV_ID").toInt();

		//logged person parameter
		int nLoggedPersonID = g_pClientManager->GetPersonID();

		//now fetch the big chunk describing all calendar views available for current user (+ public ones)
		DbRecordSet recCalendarView, recCalendarEntities, recCalendarTypeIDs;
		Status err;
		_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCalendarView, recCalendarEntities, recCalendarTypeIDs, nCalViewID));
		_CHK_ERR(err);

		//Set correct recordset for entities.
		CalendarHelperCore::ConvertSelectEntityRecordsetToInsertEntityRecordset(recCalendarEntities);

		int nRows = lstSelected.length();
		for(int i=nRows-1; i>=0; i--)
		{
			//move row down
			int nSelectedEntityIdx = lstSelected.at(i).row();
			if(nSelectedEntityIdx < nTotalEntitiesCnt-1)
			{
				recCalendarEntities.insertRow(nSelectedEntityIdx+2);
				DbRecordSet row = recCalendarEntities.getRow(nSelectedEntityIdx);
				recCalendarEntities.assignRow(nSelectedEntityIdx+2, row);
				recCalendarEntities.deleteRow(nSelectedEntityIdx);
			}
		}

		//Reorder sort codes
		CalendarHelperCore::ReorderEntityRecordsetSortCode(recCalendarEntities);

		_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nCalViewID, recCalendarView, recCalendarEntities, recCalendarTypeIDs));
		_CHK_ERR(err);

		qDebug() << "Updated Calendar View ID=" << nCalViewID;

		//reload the list of the view fields
		OnViewSelectionChanged();

		//select item
		int nNewPos = nSelectedEntityIdx+1;
		if(nNewPos >= nTotalEntitiesCnt)
			nNewPos = nSelectedEntityIdx;
		ui.lstSelectedView->setCurrentItem(ui.lstSelectedView->topLevelItem(nNewPos));

		OnViewListModified();
	}
}

void FUI_CalendarViewEditor::OnViewListModified()
{
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CALENDAR_VIEW_LIST_RELOAD, ENTITY_CALENDAR_EVENT);
}

//list of supported functions
#define FN_CalendarView_Create					1
#define FN_CalendarView_Delete					2
#define FN_CalendarView_AddProjects				3
#define FN_CalendarView_AddContacts				4
#define FN_CalendarViewGroup_CreateFromProjects	5
#define FN_CalendarViewGroup_CreateFromContacts	6
#define FN_CalendarViewGroup_Delete				7
#define FN_CalendarViewGroup_AddProjects		8
#define FN_CalendarViewGroup_AddContacts		9

// implements interpreter for dummy script language:
// - supports one command call per line
// - supports comments ";" (this char should not eb part of the command params)
// - does not support advanced stuff such as loops, variables, ...
//
void FUI_CalendarViewEditor::ExecuteScript(QString strScriptContent)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Execute Script"));

	//map function name to some number
	static QMap<QString, int> mapFunction2Idx;
	if(mapFunction2Idx.empty())
	{
		mapFunction2Idx.insert("CalendarView_Create",					FN_CalendarView_Create);
		mapFunction2Idx.insert("CalendarView_Delete",					FN_CalendarView_Delete);
		mapFunction2Idx.insert("CalendarView_AddProjects",				FN_CalendarView_AddProjects);
		mapFunction2Idx.insert("CalendarView_AddContacts",				FN_CalendarView_AddContacts);
		mapFunction2Idx.insert("CalendarViewGroup_CreateFromProjects",	FN_CalendarViewGroup_CreateFromProjects);
		mapFunction2Idx.insert("CalendarViewGroup_CreateFromContacts",	FN_CalendarViewGroup_CreateFromContacts);
		mapFunction2Idx.insert("CalendarViewGroup_Delete",				FN_CalendarViewGroup_Delete);
		mapFunction2Idx.insert("CalendarViewGroup_AddProjects",			FN_CalendarViewGroup_AddProjects);
		mapFunction2Idx.insert("CalendarViewGroup_AddContacts",			FN_CalendarViewGroup_AddContacts);
	}

	//map time period input (english only) to the period code
	static QMap<QString, int> mapTimePeriod2Code;
	if(mapTimePeriod2Code.empty())
	{
		mapTimePeriod2Code.insert("-",				DataHelper::UNDEFINED);
		mapTimePeriod2Code.insert("Day",			DataHelper::DAY);
		mapTimePeriod2Code.insert("Business Days",	DataHelper::BUSS_DAYS);
		mapTimePeriod2Code.insert("Week",			DataHelper::WEEK);
		mapTimePeriod2Code.insert("Two Weeks",		DataHelper::TWO_WEEK);
		mapTimePeriod2Code.insert("Month",			DataHelper::MONTH);
		mapTimePeriod2Code.insert("Year",			DataHelper::YEAR);
	}

	if(strScriptContent.isEmpty())
		return;

	//now start parsing line by line
	strScriptContent.replace("\r\n", "\n");
	QStringList lstLines = strScriptContent.split("\n", QString::SkipEmptyParts);

	int nLineCnt = lstLines.count();
	QSize size(300,100);
	QProgressDialog progress(QObject::tr("Regenerating Views..."), QString(),0,nLineCnt,this);
	progress.setWindowTitle(tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setMinimumDuration(600);

	for(int i=0; i<nLineCnt; i++)
	{
		progress.setValue(i);
		qApp->processEvents();

		QString &strLine = lstLines[i];
		strLine = strLine.trimmed();

		//STEP 1: strip comments
		//TOFIX this code needs to be more complex to account for ; being part of some string parameter
		int nCommentPos = strLine.indexOf(";");
		if(nCommentPos >= 0){
			strLine = strLine.left(nCommentPos);
			strLine = strLine.trimmed();
		}

		//if comment line or empty line
		if(strLine.isEmpty())
			continue;

		//STEP 2: extract function name
		int nParamsPos = strLine.indexOf("(");
		if(nParamsPos <= 0)	{
			QMessageBox::information(NULL, "", tr("Syntax error: line does not contain the function name!"));
			return;
		}
		
		QString strFunction = strLine.left(nParamsPos);
		strFunction = strFunction.trimmed();
		if(!mapFunction2Idx.contains(strFunction)) {
			QMessageBox::information(NULL, "", tr("Syntax error: unknown function name: %1!").arg(strFunction));
			return;
		}

		//extract parameters
		QString strParameters = strLine.mid(nParamsPos+1);
		nParamsPos = strParameters.lastIndexOf(")");
		if(nParamsPos <= 0)	{
			QMessageBox::information(NULL, "", tr("Syntax error: parameter list not closed in line: %1!").arg(i+1));
			return;
		}
		strParameters = strParameters.left(nParamsPos);

		//split parameter list and trim spaces
		QStringList lstParams = strParameters.split(",", QString::SkipEmptyParts);
		int nParamCnt = lstParams.count();
		for(int j=0; j<nParamCnt; j++){
			lstParams[j] = lstParams[j].trimmed();
			//if needed, unquote param value
			if( !lstParams.isEmpty() &&
				lstParams[j].at(0) == '\"' && 
				lstParams[j].at(lstParams[j].size()-1) == '\"')
			{
				lstParams[j] = lstParams[j].mid(1);
				lstParams[j].chop(1);
			}
		}

		//execute function in this line
		QString strErrMsg;
		bool bFnSuccess = true;
		bool bOK = true;
		int nTimePeriodCode = -1;
		int nFunctionCode = mapFunction2Idx[strFunction];
		switch(nFunctionCode){

			case FN_CalendarView_Create:
				if(nParamCnt != 6){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				if(!mapTimePeriod2Code.contains(lstParams[1])){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter value in line: %1!").arg(i+1));
					return;
				}
				nTimePeriodCode = mapTimePeriod2Code[lstParams[1]];
				bFnSuccess = CalendarView_Create(strErrMsg, lstParams[0], nTimePeriodCode, lstParams[2], lstParams[3].toInt(&bOK, 10)>0, lstParams[4].toInt(&bOK, 10)>0, lstParams[5].toInt(&bOK, 10)>0);
				break;
			
			case FN_CalendarView_Delete:
				if(nParamCnt != 1){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				bFnSuccess = CalendarView_Delete(strErrMsg, lstParams[0]);
				break;
			
			case FN_CalendarView_AddProjects:
				if(nParamCnt != 2){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				bFnSuccess = CalendarView_AddProjects(strErrMsg, lstParams[0], lstParams[1]);
				break;
			
			case FN_CalendarView_AddContacts:
				if(nParamCnt != 2){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				bFnSuccess = CalendarView_AddContacts(strErrMsg, lstParams[0], lstParams[1]);
				break;
			
			case FN_CalendarViewGroup_CreateFromProjects:
				if(nParamCnt != 7){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				if(!mapTimePeriod2Code.contains(lstParams[1])){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter value in line: %1!").arg(i+1));
					return;
				}
				nTimePeriodCode = mapTimePeriod2Code[lstParams[1]];
				bFnSuccess = CalendarViewGroup_CreateFromProjects(strErrMsg, lstParams[0], nTimePeriodCode, lstParams[2], lstParams[3].toInt(&bOK, 10)>0, lstParams[4].toInt(&bOK, 10)>0, lstParams[5].toInt(&bOK, 10)>0, lstParams[6]);
				break;
			
			case FN_CalendarViewGroup_CreateFromContacts:
				if(nParamCnt != 7){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				if(!mapTimePeriod2Code.contains(lstParams[1])){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter value in line: %1!").arg(i+1));
					return;
				}
				nTimePeriodCode = mapTimePeriod2Code[lstParams[1]];
				bFnSuccess = CalendarViewGroup_CreateFromContacts(strErrMsg, lstParams[0], nTimePeriodCode, lstParams[2], lstParams[3].toInt(&bOK, 10)>0, lstParams[4].toInt(&bOK, 10)>0, lstParams[5].toInt(&bOK, 10)>0, lstParams[6]);
				break;
			
			case FN_CalendarViewGroup_Delete:
				if(nParamCnt != 1){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				bFnSuccess = CalendarViewGroup_Delete(strErrMsg, lstParams[0]);
				break;
			
			case FN_CalendarViewGroup_AddProjects:
				if(nParamCnt != 2){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				bFnSuccess = CalendarViewGroup_AddProjects(strErrMsg, lstParams[0], lstParams[1]);
				break;
			
			case FN_CalendarViewGroup_AddContacts:
				if(nParamCnt != 2){
					QMessageBox::information(NULL, "", tr("Syntax error: Invalid function parameter count in line: %1!").arg(i+1));
					return;
				}
				bFnSuccess = CalendarViewGroup_AddContacts(strErrMsg, lstParams[0], lstParams[1]);
				break;
			
			default:
				QMessageBox::information(NULL, "", tr("Script error: Unsupported function: %1!").arg(strFunction));
				return;
		}	// end switch

		//break on error
		if(!bFnSuccess)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script error: %1)").arg(strErrMsg));
			QMessageBox::information(NULL, "", tr("Script error: %1!").arg(strErrMsg));
			return;
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script done"));

	//refresh FUI window
	//reload calendar views list
	dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->ReloadData(true);
	OnViewListModified();
}

//internal script API implementation
bool FUI_CalendarViewEditor::CalendarView_Create(QString &strErrMsg, QString strName, int nTimePeriodCode, QString strCalViewType, bool bShowOverviewCalendar, bool bShowDetailCalendar, 
												 bool bIsPublic)
{
	if(!bShowOverviewCalendar  && !bShowDetailCalendar){
		strErrMsg = tr("You must show at least one calendar in the view!");
		return false;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarView_Create (\"%1\")").arg(strName));

	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	//find calendar view type code by its name
	int nCalViewTypeIdx = m_recCalViewTypes.find("BCMT_TYPE_NAME", strCalViewType, true);	//find first match by name
	int nCalViewTypeCode = (nCalViewTypeIdx >= 0) ? m_recCalViewTypes.getDataRef(nCalViewTypeIdx, "BCMT_ID").toInt() : 0;

	//you can not have "show none" value here
	int nSections = (bShowOverviewCalendar &&  bShowDetailCalendar) ? 0 : (bShowOverviewCalendar)? 1 : 2;	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER

	DbRecordSet recView, recCalEntities, recCalTypeIDs;
	CalendarHelperCore::CreateCalendarViewRecordset(recView, strName, 0, QVariant(bIsPublic).toInt(), nCalViewTypeCode, nLoggedPersonID, nSections, nTimePeriodCode);

	//now create a new view
	int nViewID = -1;
	Status err;
	_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nViewID, recView, recCalEntities, recCalTypeIDs));
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return false;
	}

	return true;
}

bool FUI_CalendarViewEditor::CalendarView_Delete(QString &strErrMsg, QString strName)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarView_Delete (\"%1\")").arg(strName));

	int nCalViewID = GetCalendarViewID(strName, strErrMsg);
	if(!strErrMsg.isEmpty()){
		return false;	//error fetching the data
	}
	if(nCalViewID < 0){
		return true;	//already deleted, that's OK
	}
	
	qDebug() << "Delete Clandar View ID=" << nCalViewID;
	Status err;
	_SERVER_CALL(BusCalendar->DeleteCalFilterView(err, nCalViewID));
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return false;
	}
	return true;
}

bool FUI_CalendarViewEditor::CalendarView_AddProjects(QString &strErrMsg, QString strName, QString strProjectList)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarView_AddProjects (\"%1\", \"%2\")").arg(strName).arg(strProjectList));

	//get project list sorted by code as entity list
	DbRecordSet recEntities;
	if(!GetProjectEntityList(strProjectList, recEntities, strErrMsg))
		return false;

	int nCalViewID = GetCalendarViewID(strName, strErrMsg);
	if(!strErrMsg.isEmpty()){
		return false;	//error fetching the data
	}
	if(nCalViewID < 0){
		strErrMsg = tr("Calendar view %1 not found!").arg(strName);
		return false;	//view does not exist
	}

	AddEntitiesToCalendarView(recEntities, nCalViewID);
	return true;
}

bool FUI_CalendarViewEditor::CalendarView_AddContacts(QString &strErrMsg, QString strName, QString strContactList)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarView_AddContacts (\"%1\", \"%2\")").arg(strName).arg(strContactList));

	//get project list sorted by code as entity list
	DbRecordSet recEntities;
	if(!GetContactEntityList(strContactList, recEntities, strErrMsg))
		return false;

	int nCalViewID = GetCalendarViewID(strName, strErrMsg);
	if(!strErrMsg.isEmpty()){
		return false;	//error fetching the data
	}
	if(nCalViewID < 0){
		strErrMsg = tr("Calendar view %1 not found!").arg(strName);
		return false;	//view does not exist
	}

	AddEntitiesToCalendarView(recEntities, nCalViewID);
	return true;
}

bool FUI_CalendarViewEditor::CalendarViewGroup_CreateFromProjects(QString &strErrMsg, QString strNamePrefix, int nTimePeriodCode, QString strCalViewType, bool bShowOverviewCalendar, bool bShowDetailCalendar, bool bIsPublic, QString strProjectList)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarViewGroup_CreateFromProjects (\"%1\", \"%2\")").arg(strNamePrefix).arg(strProjectList));

	DbRecordSet recProjectListData;
	if(!GetProjectList(strProjectList, recProjectListData, strErrMsg))
		return false;

	//for each project in the project list
	int nCount = recProjectListData.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		//create a calendar view with a single project in it
		QString strName = strNamePrefix;
		//strName += " ";	//prefix already contains the divider
		strName += recProjectListData.getDataRef(i, "BUSP_NAME").toString();

		if(!CalendarView_Create(strErrMsg, strName, nTimePeriodCode, strCalViewType, bShowOverviewCalendar, bShowDetailCalendar,  bIsPublic))
			return false;
		
		//get ID of the new view
		int nCalViewID = GetCalendarViewID(strName, strErrMsg);
		if(!strErrMsg.isEmpty()){
			return false;	//error fetching the data
		}
		if(nCalViewID < 0){
			strErrMsg = tr("Calendar view %1 not found!").arg(strName);
			return false;	//view does not exist
		}

		//add single entity into the new view
		DbRecordSet recEntity;
		RepackToEntityList(recProjectListData.getRow(i), recEntity, true);
		AddEntitiesToCalendarView(recEntity, nCalViewID);
	}
	return true;
}

bool FUI_CalendarViewEditor::CalendarViewGroup_CreateFromContacts(QString &strErrMsg, QString strNamePrefix, int nTimePeriodCode, QString strCalViewType, bool bShowOverviewCalendar, bool bShowDetailCalendar, bool bIsPublic, QString strContactList)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarViewGroup_CreateFromProjects (\"%1\", \"%2\")").arg(strNamePrefix).arg(strContactList));

	DbRecordSet recContactListData;
	if(!GetContactList(strContactList, recContactListData, strErrMsg))
		return false;

	//for each project in the project list
	int nCount = recContactListData.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		//create a calendar view with a single contact in it
		QString strName = strNamePrefix;
		//strName += " ";	//prefix already contains the divider
		strName += recContactListData.getDataRef(i, "BCNT_NAME").toString();

		if(!CalendarView_Create(strErrMsg, strName, nTimePeriodCode, strCalViewType, bShowOverviewCalendar, bShowDetailCalendar,  bIsPublic))
			return false;

		//get ID of the new view
		int nCalViewID = GetCalendarViewID(strName, strErrMsg);
		if(!strErrMsg.isEmpty()){
			return false;	//error fetching the data
		}
		if(nCalViewID < 0){
			strErrMsg = tr("Calendar view %1 not found!").arg(strName);
			return false;	//view does not exist
		}

		//add single entity into the new view
		DbRecordSet recEntity;
		RepackToEntityList(recContactListData.getRow(i), recEntity, false);
		AddEntitiesToCalendarView(recEntity, nCalViewID);
	}
	return true;
}

bool FUI_CalendarViewEditor::CalendarViewGroup_Delete(QString &strErrMsg, QString strNamePrefix)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarViewGroup_Delete (\"%1\")").arg(strNamePrefix));

	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	//fetch the big chunk describing all calendar views available for current user (+ public ones)
	DbRecordSet recCalendarViews;
	Status err;
	_SERVER_CALL(BusCalendar->GetCalFilterViews(err, recCalendarViews, nLoggedPersonID, -1));	//-1 -> ignore the cal type (all my views)
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return false;
	}

	int nRowCnt = recCalendarViews.getRowCount();
	for(int i=0; i<nRowCnt; i++)
	{
		//check if view name starts with given prefix
		if(recCalendarViews.getDataRef(i, "BCALV_NAME").toString().startsWith(strNamePrefix))
		{
			int nCalViewID = -1;
			recCalendarViews.getData(i, "BCALV_ID", nCalViewID);
			qDebug() << "Delete Clandar View ID=" << nCalViewID;

			_SERVER_CALL(BusCalendar->DeleteCalFilterView(err, nCalViewID));
			if(!err.IsOK()){
				strErrMsg = err.getErrorText();
				return false;
			}
		}
	}
	return true;
}

bool FUI_CalendarViewEditor::CalendarViewGroup_AddProjects(QString &strErrMsg, QString strNamePrefix, QString strProjectList)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarViewGroup_AddProjects (\"%1\", \"%2\")").arg(strNamePrefix).arg(strProjectList));

	//get project list sorted by code as entity list
	DbRecordSet recEntities;
	if(!GetProjectEntityList(strProjectList, recEntities, strErrMsg))
		return false;

	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	//fetch the big chunk describing all calendar views available for current user (+ public ones)
	DbRecordSet recCalendarViews;
	Status err;
	_SERVER_CALL(BusCalendar->GetCalFilterViews(err, recCalendarViews, nLoggedPersonID, -1));	//-1 -> ignore the cal type (all my views)
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return false;
	}

	int nRowCnt = recCalendarViews.getRowCount();
	for(int i=0; i<nRowCnt; i++)
	{
		//check if view name starts with prefix
		if(recCalendarViews.getDataRef(i, "BCALV_NAME").toString().startsWith(strNamePrefix))
		{
			//modify a single selected view
			int nCalViewID = -1;
			recCalendarViews.getData(i, "BCALV_ID", nCalViewID);

			AddEntitiesToCalendarView(recEntities, nCalViewID);
		}
	}
	return true;
}

bool FUI_CalendarViewEditor::CalendarViewGroup_AddContacts(QString &strErrMsg, QString strNamePrefix, QString strContactList)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Script call: CalendarViewGroup_AddContacts (\"%1\", \"%2\")").arg(strNamePrefix).arg(strContactList));

	//get project list sorted by code as entity list
	DbRecordSet recEntities;
	if(!GetContactEntityList(strContactList, recEntities, strErrMsg))
		return false;

	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	//fetch the big chunk describing all calendar views available for current user (+ public ones)
	DbRecordSet recCalendarViews;
	Status err;
	_SERVER_CALL(BusCalendar->GetCalFilterViews(err, recCalendarViews, nLoggedPersonID, -1));	//-1 -> ignore the cal type (all my views)
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return false;
	}

	int nRowCnt = recCalendarViews.getRowCount();
	for(int i=0; i<nRowCnt; i++)
	{
		//check if view name starts with prefix
		if(recCalendarViews.getDataRef(i, "BCALV_NAME").toString().startsWith(strNamePrefix))
		{
			//modify a single selected view
			int nCalViewID = -1;
			recCalendarViews.getData(i, "BCALV_ID", nCalViewID);

			AddEntitiesToCalendarView(recEntities, nCalViewID);
		}
	}
	return true;
}

void FUI_CalendarViewEditor::on_btnRegenerateViewsAnesthesists_clicked()
{
	//� - \xC3\x9C  (veliko U sa umlautom)
	//� - \xC3\xBC  (malo U sa umlautom)
	//� - \xC3\xA4  (malo a sa umlautom)

	const char szScript[] = " \
CalendarView_Delete(\"- OP Planung\"); \n\
CalendarView_Create(\"- OP Planung\", \"Week\", \"Service Plan\", 1,0,1); \n\
CalendarView_AddProjects(\"- OP Planung\", \"OP-Liste\"); \n\
\n\
CalendarView_Delete(\"- \xC3\x9C""bersicht OP's\"); \n\
CalendarView_Create(\"- \xC3\x9C""bersicht OP's\", \"Week\", \"Overview Operating Rooms\", 1,0,1); \n\
CalendarView_AddProjects(\"- \xC3\x9C""bersicht OP's\", \"OP-Liste\"); \n\
\n\
CalendarView_Delete(\"- Verf\xC3\xBCgbarkeitsplan\"); \n\
CalendarView_Create(\"- Verf\xC3\xBCgbarkeitsplan\", \"Week\", \"Service Plan\", 1,0,1); \n\
CalendarView_AddContacts(\"- Verf\xC3\xBCgbarkeitsplan\", \"An\xC3\xA4sthesie::A.S.PL. Planung\"); \n\
\n\
CalendarView_Delete(\"- \xC3\x9C""bersicht An\xC3\xA4sthesisten\"); \n\
CalendarView_Create(\"- \xC3\x9C""bersicht An\xC3\xA4sthesisten\", \"Week\", \"Overview Anesthetists\", 1,0,1); \n\
CalendarView_AddContacts(\"- \xC3\x9C""bersicht An\xC3\xA4sthesisten\", \"An\xC3\xA4sthesie::A.S.PL. Planung\"); \n\
\n\
CalendarViewGroup_Delete(\"OP \"); \n\
CalendarViewGroup_CreateFromProjects(\"OP \", \"Week\", \"Service Plan\", 1,0,1,\"OP-Liste\"); \n\
CalendarViewGroup_AddContacts(\"OP \", \"An\xC3\xA4sthesie::A.S.PL. Planung\"); \n\
\n\
CalendarViewGroup_Delete(\"A \"); \n\
CalendarViewGroup_CreateFromContacts(\"A \", \"Week\", \"Plan Anesthetist\", 1,0,1,\"An\xC3\xA4sthesie::A.S.PL. Planung\"); \n\
CalendarViewGroup_AddProjects(\"A \", \"OP-Liste\");";

	QString strScript = QString::fromUtf8(szScript);
	ExecuteScript(strScript);
}

bool FUI_CalendarViewEditor::GetProjectList(QString strProjectList, DbRecordSet &recListData, QString &strErrMsg)
{
	//fetch content of the desired project list
	Status err;
	DbRecordSet recLists;
	_SERVER_CALL(StoredProjectLists->GetListNames(err, recLists)) //Project grid id
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return false;
	}
	int nListIdx = recLists.find("BSPL_NAME", strProjectList, true, false, false);	//find first
	if(nListIdx < 0){
		strErrMsg = tr("Project list %1 not found!").arg(strProjectList);
		return false;
	}
	int nListID = recLists.getDataRef(nListIdx, "BSPL_ID").toInt();
	bool bIsSelection;
	QString strSelectionXML;
	bool bIsFilter;
	QString strFilterXML;
	bool bGetDescOnly = false;
	_SERVER_CALL(StoredProjectLists->GetListData(err, nListID, recListData, bIsSelection, strSelectionXML, bIsFilter, strFilterXML, bGetDescOnly));
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return false;
	}
	recListData.sort("BUSP_CODE");
	return true;
}

bool FUI_CalendarViewEditor::GetProjectEntityList(QString strProjectList, DbRecordSet &recEntities, QString &strErrMsg)
{
	DbRecordSet recProjectListData;
	if(!GetProjectList(strProjectList, recProjectListData, strErrMsg))
		return false;
	RepackToEntityList(recProjectListData, recEntities, true);
	return true;
}

bool FUI_CalendarViewEditor::GetContactList(QString strContactList, DbRecordSet &recListData, QString &strErrMsg)
{
	//fetch content of the desired project list
	Status err;
	_SERVER_CALL(BusContact->ReadContactsFromGroup(err, strContactList, recListData));
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return false;
	}
	return true;
}

void FUI_CalendarViewEditor::RepackToEntityList(const DbRecordSet &recListData, DbRecordSet &recEntities, bool bProjectList)
{
	//prepare result in the form needed by AddEntitiesToCalendarView
	int nEntityType = (bProjectList)? GlobalConstants::TYPE_CAL_PROJECT_SELECT : GlobalConstants::TYPE_CAL_CONTACT_SELECT;
	int nIdColumnIdx = (bProjectList)? recListData.getColumnIdx("BUSP_ID") : recListData.getColumnIdx("BGCN_CONTACT_ID");
	Q_ASSERT(nIdColumnIdx >= 0);

	recEntities.clear();
	recEntities.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	int nCount = recListData.getRowCount();
	for(int i=0; i<nCount; i++){
		recEntities.addRow();
		recEntities.setData(i, "ENTITY_TYPE", nEntityType);
		recEntities.setData(i, "ENTITY_ID",	  recListData.getDataRef(i, nIdColumnIdx).toInt());
	}
}

bool FUI_CalendarViewEditor::GetContactEntityList(QString strContactList, DbRecordSet &recEntities, QString &strErrMsg)
{
	DbRecordSet recContactListData;
	if(!GetContactList(strContactList, recContactListData, strErrMsg))
		return false;

	RepackToEntityList(recContactListData, recEntities, false);
	return true;
}

int	 FUI_CalendarViewEditor::GetCalendarViewID(QString strName, QString &strErrMsg)
{
	//logged person parameter
	int nLoggedPersonID = g_pClientManager->GetPersonID();

	//fetch the big chunk describing all calendar views available for current user (+ public ones)
	DbRecordSet recCalendarViews;
	Status err;
	_SERVER_CALL(BusCalendar->GetCalFilterViews(err, recCalendarViews, nLoggedPersonID, -1));	//-1 -> ignore the cal type (all my views)
	if(!err.IsOK()){
		strErrMsg = err.getErrorText();
		return -1;
	}

	//find description record for this specific calendar view
	int nRowIdx = recCalendarViews.find("BCALV_NAME", strName, true, false, false);	//find first
	if (nRowIdx < 0){
		return -1;
	}
	
	int nCalViewID = -1;
	recCalendarViews.getData(nRowIdx, "BCALV_ID", nCalViewID);
	return nCalViewID;
}
