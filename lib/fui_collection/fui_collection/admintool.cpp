#include "admintool.h"
#include <QFileDialog>
#include "common/common/datahelper.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "dlg_ftpservers.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "common/common/authenticator.h"
#include "trans/trans/config_trans.h"
#include "common/common/config_version_sc.h"
#include "gui_core/gui_core/thememanager.h"

#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight					*g_AccessRight;				//global access right tester
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
extern QString g_strLastDir_FileOpen;

#include "bus_client/bus_client/clientdownloadmanager.h"
extern ClientDownloadManager		*g_DownloadManager;


#define HTML_START_F1	"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:11pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:9pt; font-weight:bold; font-style:italic;\">"
#define HTML_END_F1		"</span></p></body></html>"


AdminTool::AdminTool(QWidget *parent)
: QDialog(parent)
{
	ui.setupUi(this);
	m_GuiFieldManagerImportExport=NULL;
	m_pData=NULL;
	ui.dateLastSync_Project->useAsDateTimeEdit();
	m_pBackupManager=NULL;
	m_pBackupManager=static_cast<ClientBackupManager*>(g_pClientManager->GetBackupManager());
	if (m_pBackupManager==NULL)
	{
		ui.tabWidget->widget(0)->setEnabled(false);
	}
	m_ImportExportManager=NULL;
	m_ImportExportManager=static_cast<ClientImportExportManager*>(g_pClientManager->GetImportExportManager());
	if (m_ImportExportManager==NULL)
	{
		ui.tabWidget->widget(1)->setEnabled(false);
	}


	if(!g_AccessRight->TestAR(IMPORT_PROJECTS))
	{
		ui.rbImport_Project->setEnabled(false);
	}
	if(!g_AccessRight->TestAR(EXPORT_PROJECTS))
	{
		ui.rbExport_Project->setEnabled(false);	
	}
	//if both then all disable:
	if(!ui.rbImport_Project->isEnabled() && !ui.rbExport_Project->isEnabled())
	{
		ui.tabWidgetExport->widget(0)->setEnabled(false);
	}

	if(!g_AccessRight->TestAR(SYNCHRONIZE_PROJECTS_AND_DEBTORS))
	{
		ui.tabWidgetExport->widget(0)->setEnabled(false);
		ui.tabWidgetExport->widget(2)->setEnabled(false);

	}

	//issue 1702:
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());

	connect(g_pClientManager, SIGNAL(CommunicationServerMessageRecieved(DbRecordSet)),this,SLOT(OnCommunicationServerMessageRecieved(DbRecordSet)));
	

}


bool AdminTool::Initialize()
{
	//reload FTP's:
	if(!ReloadFTPServers())
		return false;


	//days:
	m_MenuDaysWeek.addAction(tr("Monday"));
	m_MenuDaysWeek.addAction(tr("Tuesday"));
	m_MenuDaysWeek.addAction(tr("Wednesday"));
	m_MenuDaysWeek.addAction(tr("Thursday"));
	m_MenuDaysWeek.addAction(tr("Friday"));
	m_MenuDaysWeek.addAction(tr("Saturday"));
	m_MenuDaysWeek.addAction(tr("Sunday"));

	//days:
	m_MenuDaysMonth.addAction(tr("Monday"));
	m_MenuDaysMonth.addAction(tr("Tuesday"));
	m_MenuDaysMonth.addAction(tr("Wednesday"));
	m_MenuDaysMonth.addAction(tr("Thursday"));
	m_MenuDaysMonth.addAction(tr("Friday"));
	m_MenuDaysMonth.addAction(tr("Saturday"));
	m_MenuDaysMonth.addAction(tr("Sunday"));


	ui.btnDayWeek->setMenu(&m_MenuDaysWeek);
	//ui.btnDayMonth->setMenu(&m_MenuDaysMonth);

	ui.dateStartDay->useAsTimeEdit();
	//ui.dateStartMonth->useAsTimeEdit();
	ui.dateStartWeek->useAsTimeEdit();

	ui.dateStartDay->setTime(QTime(1,0));
	//ui.dateStartMonth->setTime(QTime(1,0));
	ui.dateStartWeek->setTime(QTime(1,0));
	SetupDayFromAction(tr("Sunday"),ui.btnDayWeek);
	//SetupDayFromAction(tr("Sunday"),ui.btnDayMonth);

	ui.dateStartDay->setDisplayFormat("hh:mm");
	//ui.dateStartMonth->setDisplayFormat("hh:mm");
	ui.dateStartWeek->setDisplayFormat("hh:mm");


	ui.btnSelectBackup->setIcon(QIcon(":SAP_Select.png"));
	ui.txtPath->setEnabled(false);

	//if on server allow dir choose:
	if (g_pClientManager->IsClientIPMatchHost())
	{
		ui.btnSelectBackup->setEnabled(true);
		ui.txtPath->setEnabled(true);
	}
	else
		ui.btnSelectBackup->setEnabled(false);



	Status err;
	_SERVER_CALL(ServerControl->GetBackupData(err,m_nBackupFreq,m_strBackupPath,m_datBackupLastDate,m_strRestoreOnNextStart,m_nBackupDay,m_strBackupTime,m_nDeleteOldBackupsDay))
	_CHK_ERR_RET_BOOL_ON_FAIL(err);

	if (m_nBackupFreq!=ClientBackupManager::BCP_FREQ_NONE)
	{
		ui.groupBoxSchedule->setChecked(true);

		switch(m_nBackupFreq)
		{
		case ClientBackupManager::BCP_FREQ_HOURLY:
			ui.rbtnHour->setChecked(true);
			break;
		case ClientBackupManager::BCP_FREQ_DAILY:
			{
				ui.rbtnDay->setChecked(true);
				if (!m_strBackupTime.isEmpty())
				{
					QTime time=QTime::fromString(m_strBackupTime,"hh:mm");
					if (time.isValid())
						ui.dateStartDay->setTime(time);
				}


			}
			break;
		case ClientBackupManager::BCP_FREQ_WEEKLY:
			{
				ui.rbtnWeek->setChecked(true);
				if (!m_strBackupTime.isEmpty())
				{
					QTime time=QTime::fromString(m_strBackupTime,"hh:mm");
					if (time.isValid())
						ui.dateStartWeek->setTime(time);
				}

				SetupDayFromAction(m_nBackupDay,ui.btnDayWeek);
			}
			break;
			/*
			case ClientBackupManager::BCP_FREQ_MONTHLY:
			{
			ui.rbtnMonth->setChecked(true);
			if (!m_strBackupTime.isEmpty())
			{
			QTime time=QTime::fromString(m_strBackupTime,"hh:mm");
			if (time.isValid())
			ui.dateStartMonth->setTime(time);
			}
			SetupDayFromAction(m_nBackupDay,ui.btnDayMonth);
			}
			break;
			*/
		default:
			ui.rbtnDay->setChecked(true);
			break;
		}

		if (m_nDeleteOldBackupsDay>0)
		{
			ui.ckbOldBackup->setChecked(true);
			ui.txtOldBackupDay->setValue(m_nDeleteOldBackupsDay);
		}
		else
		{
			ui.txtOldBackupDay->setValue(0);
			ui.ckbOldBackup->setChecked(false);
		}
	}
	else
	{
		ui.groupBoxSchedule->setChecked(false);
	}

	ui.txtPath->setText(m_strBackupPath);


	//get template data:
	_SERVER_CALL(ServerControl->GetTemplateData(err,m_strTemplateName,m_nAskForTemplate,m_nStartStartUpWizard))
	_CHK_ERR_RET_BOOL_ON_FAIL(err);
	
	ui.ckbAskForTemplate->setChecked(m_nAskForTemplate);
	ui.txtTemplate->setText(m_strTemplateName);
	ui.ckbStartWizard->setChecked(m_nStartStartUpWizard);

	//if thin not possible to select:

	//temporary:
	//ui.ckbAskForTemplate->setEnabled(false);

	connect(&m_MenuDaysWeek,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerWeek(QAction *)));
	//connect(&m_MenuDaysMonth,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerMonth(QAction *)));

	_SERVER_CALL(ServerControl->LoadBackupList(err,m_lstBackups))
	_CHK_ERR_NO_RET(err);

	ui.tableWidget->Initialize(&m_lstBackups,false,false,true,true,25,true);
	DbRecordSet lstSetup;
	ui.tableWidget->AddColumnToSetup(lstSetup,"FILE",tr("Backup File"),360,false);
	ui.tableWidget->AddColumnToSetup(lstSetup,"DATE",tr("Date Created"),150,false);
	ui.tableWidget->AddColumnToSetup(lstSetup,"SIZE_DISPLAY",tr("Size"),100,false);
	ui.tableWidget->SetColumnSetup(lstSetup);
	ui.tableWidget->RefreshDisplay();


	//if system-> he can set template name
	ui.btnTemplate->setVisible(false);
	if (g_pClientManager->GetLoggedUserName()=="system")
	{
		ui.txtTemplate->setEnabled(true);
		ui.btnTemplate->setVisible(true);
	}


	//LOAD IMPORT/EXPORT settings
	if(m_ImportExportManager)
	{
		m_ImportExportManager->ReloadSettings(err);
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}
	if(m_ImportExportManager)
	{
		m_ImportExportManager->GetSettings(err,m_lstImportExportSettings);
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}


	//_DUMP(m_lstImportExportSettings);

	
	QList<QRadioButton*> lstRadioButons;
	QList<QVariant> lstValues;

	ui.btnSelectPath_Project->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectFTP_Project->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectPath_User->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectFTP_User->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectPath_Debtor->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectFTP_Debtor->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectPath_Contact->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectFTP_Contact->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectPath_SPL->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectFTP_SPL->setIcon(QIcon(":SAP_Select.png"));


	//connect fields:
	m_pData= new GuiDataManipulator(&m_lstImportExportSettings);
	m_GuiFieldManagerImportExport =new GuiFieldManager(m_pData);
	m_GuiFieldManagerImportExport->RegisterField("CIE_GLOBAL_SCAN_PERIOD",ui.txtGlobalScanPeriod);

	CheckOnLoad();
	
	m_GuiFieldManagerImportExport->RegisterField("CIE_USE_UNICODE",ui.ckb_UseUnicode);
	ui.ckb_ShowUnicodeOption->setChecked(g_pSettings->GetApplicationOption(APP_EXPORT_IMPORT_SHOW_OPTION_USE_UNICODE).toBool());
	

	//-------------projects--------------------
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_ENABLED",ui.ckbEnable_Project);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbImport_Project << ui.rbExport_Project;
	lstValues << 1 << 0;
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_IMPORT",new GuiField_RadioButton("CIE_PROJ_IMPORT",lstRadioButons,lstValues,&m_lstImportExportSettings));
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbScanPeriod_Project << ui.rbScanAuto_Project;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_SCAN_TYPE",new GuiField_RadioButton("CIE_PROJ_SCAN_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_SCAN_PERIOD",ui.txtPeriod_Project);
	lstRadioButons.clear();lstValues.clear();
	lstRadioButons << ui.rbDir_Project << ui.rbFTP_Project;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_SOURCE",new GuiField_RadioButton("CIE_PROJ_SOURCE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_SOURCE_DIR",ui.txtPath_Project);
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_SOURCE_FTP_DIR",ui.txtFTPDir_Project);
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_FILE_EXT",ui.txtFileExt_Project);
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_FILE_PREFIX",ui.txtFilePrefix_Project);
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_FILE_PREFIX_PROCESS",ui.txtFileProcessedExt_Project);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbExportAll_Project << ui.rbLastN_Project;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_EXPORT_TYPE",new GuiField_RadioButton("CIE_PROJ_EXPORT_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_EXPORT_LAST_N",ui.txtLastN_Project);
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_ADD_DATETIME",ui.ckbAddDate_Project);
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_STATUS_ERROR",ui.txtLastError_Project);
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_STATUS_LAST_FILE",ui.txtLastFile_Project);
	m_GuiFieldManagerImportExport->RegisterField("CIE_PROJ_STATUS_DATE",ui.dateLastSync_Project);

	ui.dateLastSync_Project->setEnabled(false);
	ui.txtLastFile_Project->setEnabled(false);
	ui.txtLastError_Project->setEnabled(false);




	//-------------users--------------------
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_ENABLED",ui.ckbEnable_User);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbImport_User << ui.rbExport_User;
	lstValues << 1 << 0;
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_IMPORT",new GuiField_RadioButton("CIE_USER_IMPORT",lstRadioButons,lstValues,&m_lstImportExportSettings));
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbScanPeriod_User << ui.rbScanAuto_User;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_SCAN_TYPE",new GuiField_RadioButton("CIE_USER_SCAN_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_SCAN_PERIOD",ui.txtPeriod_User);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbDir_User << ui.rbFTP_User;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_SOURCE",new GuiField_RadioButton("CIE_USER_SOURCE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_SOURCE_DIR",ui.txtPath_User);
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_SOURCE_FTP_DIR",ui.txtFTPDir_User);
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_FILE_EXT",ui.txtFileExt_User);
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_FILE_PREFIX",ui.txtFilePrefix_User);
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_FILE_PREFIX_PROCESS",ui.txtFileProcessedExt_User);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbExportAll_User << ui.rbLastN_User;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_EXPORT_TYPE",new GuiField_RadioButton("CIE_USER_EXPORT_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_EXPORT_LAST_N",ui.txtLastN_User);
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_ADD_DATETIME",ui.ckbAddDate_User);
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_STATUS_ERROR",ui.txtLastError_User);
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_STATUS_LAST_FILE",ui.txtLastFile_User);
	m_GuiFieldManagerImportExport->RegisterField("CIE_USER_STATUS_DATE",ui.dateLastSync_User);

	ui.dateLastSync_User->setEnabled(false);
	ui.txtLastFile_User->setEnabled(false);
	ui.txtLastError_User->setEnabled(false);


	//---------------------------------------
	//Init Role Combo.
	//---------------------------------------
	//Check what version is client.
	QString strEdition = g_pClientManager->GetIniFile()->m_strModuleCode;
	Status status;
	DbRecordSet recRoles;

	//!???.......this should be inside cache inside some global AR manager...u fucking noob...

	if("SC-TE" == strEdition)
		_SERVER_CALL(AccessRights->GetTERoles(status, recRoles))
	else
		_SERVER_CALL(AccessRights->GetBERoles(status, recRoles))
	//_CHK_ERR(status);

	//Fill role combo.
	ui.cboRoles->addItem("", -1);	//empty role
	int nRowCount = recRoles.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QString strRoleName = recRoles.getDataRef(i, "CROL_NAME").toString();
		int nRoleID = recRoles.getDataRef(i, "CROL_ID").toInt();
		ui.cboRoles->addItem(strRoleName, nRoleID);

		//Check is this role administrator role and store it for later - in personal edition all users are administrators.
		//if (recRoles.getDataRef(i, "CROL_CODE").toInt() == ADMINISTRATOR_ROLE)
		//	m_nAdminRoleID = nRoleID;
	}

	



	//-------------debtors--------------------
	m_AddressTypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_AddressTypeHandler.SetDataForCombo(ui.cmbAddressType,"BCMT_TYPE_NAME","BCMT_ID","CIE_DEBT_ADDRESS_TYPE_ID",m_pData);
	m_AddressTypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_ADDRESS);
	m_AddressTypeHandler.ReloadData();

	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_ENABLED",ui.ckbEnable_Debtor);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbImport_Debtor << ui.rbExport_Debtor;
	lstValues << 1 << 0;
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_IMPORT",new GuiField_RadioButton("CIE_DEBT_IMPORT",lstRadioButons,lstValues,&m_lstImportExportSettings));
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbScanPeriod_Debtor << ui.rbScanAuto_Debtor;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_SCAN_TYPE",new GuiField_RadioButton("CIE_DEBT_SCAN_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_SCAN_PERIOD",ui.txtPeriod_Debtor);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbDir_Debtor << ui.rbFTP_Debtor;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_SOURCE",new GuiField_RadioButton("CIE_DEBT_SOURCE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_SOURCE_DIR",ui.txtPath_Debtor);
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_SOURCE_FTP_DIR",ui.txtFTPDir_Debtor);
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_FILE_EXT",ui.txtFileExt_Debtor);
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_FILE_PREFIX",ui.txtFilePrefix_Debtor);
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_FILE_PREFIX_PROCESS",ui.txtFileProcessedExt_Debtor);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbExportAll_Debtor << ui.rbLastN_Debtor;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_EXPORT_TYPE",new GuiField_RadioButton("CIE_DEBT_EXPORT_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_EXPORT_LAST_N",ui.txtLastN_Debtor);
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_ADD_DATETIME",ui.ckbAddDate_Debtor);
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_STATUS_ERROR",ui.txtLastError_Debtor);
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_STATUS_LAST_FILE",ui.txtLastFile_Debtor);
	m_GuiFieldManagerImportExport->RegisterField("CIE_DEBT_STATUS_DATE",ui.dateLastSync_Debtor);

	ui.dateLastSync_Debtor->setEnabled(false);
	ui.txtLastFile_Debtor->setEnabled(false);
	ui.txtLastError_Debtor->setEnabled(false);

	ui.rbExport_Debtor->setChecked(true);
	ui.rbImport_Debtor->setEnabled(false);



	//-------------contacts--------------------
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_ENABLED",ui.ckbEnable_Contact);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbImport_Contact << ui.rbExport_Contact;
	lstValues << 1 << 0;
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_IMPORT",new GuiField_RadioButton("CIE_CONT_IMPORT",lstRadioButons,lstValues,&m_lstImportExportSettings));
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbScanPeriod_Contact << ui.rbScanAuto_Contact;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_SCAN_TYPE",new GuiField_RadioButton("CIE_CONT_SCAN_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_SCAN_PERIOD",ui.txtPeriod_Contact);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbDir_Contact << ui.rbFTP_Contact;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_SOURCE",new GuiField_RadioButton("CIE_CONT_SOURCE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_SOURCE_DIR",ui.txtPath_Contact);
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_SOURCE_FTP_DIR",ui.txtFTPDir_Contact);
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_FILE_EXT",ui.txtFileExt_Contact);
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_FILE_PREFIX",ui.txtFilePrefix_Contact);
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_FILE_PREFIX_PROCESS",ui.txtFileProcessedExt_Contact);
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_STATUS_ERROR",ui.txtLastError_Contact);
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_STATUS_LAST_FILE",ui.txtLastFile_Contact);
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_STATUS_DATE",ui.dateLastSync_Contact);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbExportAll_Contact << ui.rbLastN_Contact;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_EXPORT_TYPE",new GuiField_RadioButton("CIE_CONT_EXPORT_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_EXPORT_LAST_N",ui.txtLastN_Contact);
	m_GuiFieldManagerImportExport->RegisterField("CIE_CONT_ADD_DATETIME",ui.ckbAddDate_Contact);
	ui.dateLastSync_Contact->setEnabled(false);
	ui.txtLastFile_Contact->setEnabled(false);
	ui.txtLastError_Contact->setEnabled(false);



	//-------------spl--------------------
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_ENABLED",ui.ckbEnable_SPL);

	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbScanPeriod_SPL << ui.rbScanAuto_SPL;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_SCAN_TYPE",new GuiField_RadioButton("CIE_SPLST_SCAN_TYPE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_SCAN_PERIOD",ui.txtPeriod_SPL);
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.rbDir_SPL << ui.rbFTP_SPL;
	lstValues << 0 << 1;
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_SOURCE",new GuiField_RadioButton("CIE_SPLST_SOURCE",lstRadioButons,lstValues,&m_lstImportExportSettings));
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_SOURCE_DIR",ui.txtPath_SPL);
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_SOURCE_FTP_DIR",ui.txtFTPDir_SPL);
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_FILE_EXT",ui.txtFileExt_SPL);
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_FILE_PREFIX",ui.txtFilePrefix_SPL);
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_FILE_PREFIX_PROCESS",ui.txtFileProcessedExt_SPL);
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_STATUS_ERROR",ui.txtLastError_SPL);
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_STATUS_LAST_FILE",ui.txtLastFile_SPL);
	m_GuiFieldManagerImportExport->RegisterField("CIE_SPLST_STATUS_DATE",ui.dateLastSync_SPL);

	ui.dateLastSync_SPL->setEnabled(false);
	ui.txtLastFile_SPL->setEnabled(false);
	ui.txtLastError_SPL->setEnabled(false);


	//-----------------------------------FTP
	//
	//-----------------------------------FTP


	//reload data:

	m_GuiFieldManagerImportExport->RefreshDisplay();
	LoadFTPID();
	LoadRoleID();

	RefreshEnabledWidgets_Project();
	RefreshEnabledWidgets_User();
	RefreshEnabledWidgets_Debtor();
	RefreshEnabledWidgets_Contact();
	RefreshEnabledWidgets_SPL();
	//check access right
	//


	//create Cnxt Menu actions:
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions; 

	pAction = new QAction(tr("Download to my Computer"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnBackup_Download()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Upload From Local Copy"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnBackup_Upload()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnBackup_Delete()));
	lstActions.append(pAction);
	ui.tableWidget->SetContextMenuActions(lstActions);

	ui.widget_ServerLog->Initialize();

	return true;
}


AdminTool::~AdminTool()
{
	if (m_GuiFieldManagerImportExport) delete m_GuiFieldManagerImportExport;
	if (m_pData) delete m_pData;
}

void AdminTool::on_groupBoxSchedule_clicked()
{
	if (ui.groupBoxSchedule->isChecked())
	{
		//if nothing set hour
		if (!ui.rbtnHour->isChecked() && !ui.rbtnDay->isChecked() && !ui.rbtnWeek->isChecked())
		{
			ui.rbtnHour->setChecked(true);
		}
	}
}


void AdminTool::SetupDayFromAction(QString strText,QPushButton *btnDay)
{
	btnDay->setText(strText);
	int nDayInWeek=0;
	if (strText==tr("Tuesday"))
		nDayInWeek=1;
	if (strText==tr("Wednesday"))
		nDayInWeek=2;
	if (strText==tr("Thursday"))
		nDayInWeek=3;
	if (strText==tr("Friday"))
		nDayInWeek=4;
	if (strText==tr("Saturday"))
		nDayInWeek=5;
	if (strText==tr("Sunday"))
		nDayInWeek=6;

	btnDay->setProperty("DAY_IN_WEEK",nDayInWeek);

}

void AdminTool::SetupDayFromAction(int nDayInWeek,QPushButton *btnDay)
{

	QString strText=tr("Monday");
	if (nDayInWeek==1)
		strText=tr("Tuesday");
	if (nDayInWeek==2)
		strText=tr("Wednesday");
	if (nDayInWeek==3)
		strText=tr("Thursday");
	if (nDayInWeek==4)
		strText=tr("Friday");
	if (nDayInWeek==5)
		strText=tr("Saturday");
	if (nDayInWeek==6)
		strText=tr("Sunday");

	btnDay->setText(strText);
	btnDay->setProperty("DAY_IN_WEEK",nDayInWeek);

}

int AdminTool::GetDayFromButton(QPushButton *btnDay)
{
	return btnDay->property("DAY_IN_WEEK").toInt();
}




void AdminTool::on_btnBackup_clicked()
{
	Status err;
	if(m_pBackupManager)
		m_pBackupManager->Backup(err);

	if (err.getErrorCode()==1 && err.getErrorText()=="User Abort!")
		return;
	_CHK_ERR(err);

	if (g_pClientManager->IsThinClient())
		QMessageBox::information(this,tr("Information"),tr("Backup process started on server. Server will notify when operation is completed!"));
	else
		on_btnRefresh_clicked();
}

void AdminTool::on_btnRestore_clicked()
{
	int nRow=m_lstBackups.getSelectedRow();
	if(nRow<0) return;
	QString strRestore=m_lstBackups.getDataRef(nRow,0).toString();
	Status err;
	if(m_pBackupManager)m_pBackupManager->Restore(err,strRestore);
	_CHK_ERR(err);
}

void AdminTool::on_btnRefresh_clicked()
{
	m_lstBackups.clear();

	Status err;
	_SERVER_CALL(ServerControl->LoadBackupList(err,m_lstBackups))
	_CHK_ERR_NO_RET(err);

	ui.tableWidget->RefreshDisplay();
}


void AdminTool::OnMenuTriggerWeek(QAction *action)
{
	SetupDayFromAction(action->text(),ui.btnDayWeek);
}

void AdminTool::OnMenuTriggerMonth(QAction *action)
{
	//SetupDayFromAction(action->text(),ui.btnDayMonth);
}


void AdminTool::on_btnSelectBackup_clicked()
{
	QString strStartDir=ui.txtPath->toPlainText();

	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetApplicationHomeDir();
	}

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Select backup directory"),
		strStartDir);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath->setText(strFile);
		m_strBackupPath=strFile;
		on_btnRefresh_clicked();
	}

	Status pStatus;
	_SERVER_CALL(ServerControl->SetBackupData(pStatus,m_nBackupFreq,m_strBackupPath,m_nBackupDay,m_strBackupTime,m_nDeleteOldBackupsDay))
	_CHK_ERR(pStatus);
}


void AdminTool::SaveDatabaseOptions()
{

	//if nothing changed abort:
	//save to global:
	if (!ui.groupBoxSchedule->isChecked())
	{
		m_nBackupFreq=ClientBackupManager::BCP_FREQ_NONE;
	}
	else
	{
		if (ui.rbtnHour->isChecked())
		{
			m_nBackupFreq=ClientBackupManager::BCP_FREQ_HOURLY;

		}
		if (ui.rbtnDay->isChecked())
		{
			m_nBackupFreq=ClientBackupManager::BCP_FREQ_DAILY;
			m_strBackupTime=ui.dateStartDay->time().toString("hh:mm");

		}
		if (ui.rbtnWeek->isChecked())
		{
			m_nBackupFreq=ClientBackupManager::BCP_FREQ_WEEKLY;
			m_strBackupTime=ui.dateStartWeek->time().toString("hh:mm");
			m_nBackupDay=GetDayFromButton(ui.btnDayWeek);
		}
		/*
		if (ui.rbtnMonth->isChecked())
		{
			m_nBackupFreq=ClientBackupManager::BCP_FREQ_MONTHLY;
			m_strBackupTime=ui.dateStartMonth->time().toString("hh:mm");
			m_nBackupDay=GetDayFromButton(ui.btnDayMonth);
		}
		*/

		if (ui.ckbOldBackup->isChecked())
		{
			m_nDeleteOldBackupsDay=ui.txtOldBackupDay->value();
		}
		else
		{
			m_nDeleteOldBackupsDay=0;
		}

	}


	//get old:
	int nBackupFreq,nBackupDay,nDeleteOldBackupsDay;
	QString strBackupPath,datBackupLastDate,strRestoreOnNextStart,strBackupTime;

	Status err;
	_SERVER_CALL(ServerControl->GetBackupData(err,nBackupFreq,strBackupPath,datBackupLastDate,strRestoreOnNextStart,nBackupDay,strBackupTime,nDeleteOldBackupsDay))
	_CHK_ERR_NO_RET(err);

	qDebug()<<datBackupLastDate;

	//get template data:
	m_nAskForTemplate=ui.ckbAskForTemplate->isChecked();
	m_nStartStartUpWizard=ui.ckbStartWizard->isChecked();
	m_strTemplateName=ui.txtTemplate->text();
	m_strBackupPath=ui.txtPath->toPlainText();
	_SERVER_CALL(ServerControl->SetTemplateData(err,m_strTemplateName,m_nAskForTemplate,m_nStartStartUpWizard))
	_CHK_ERR_NO_RET(err);
	_SERVER_CALL(ServerControl->SetBackupData(err,m_nBackupFreq,m_strBackupPath,m_nBackupDay,m_strBackupTime,m_nDeleteOldBackupsDay))
	_CHK_ERR_NO_RET(err);

	if ((nBackupFreq!=m_nBackupFreq || nBackupDay!=m_nBackupDay || m_strBackupTime!=strBackupTime) && !g_pClientManager->IsThinClient())
	{
		if(m_pBackupManager)m_pBackupManager->ThickModeRestartTimer(); //restart timer only if settings are changed
	}

	g_pSettings->SetApplicationOption(APP_EXPORT_IMPORT_SHOW_OPTION_USE_UNICODE,ui.ckb_ShowUnicodeOption->isChecked());
}

void AdminTool::on_btnOK_clicked()
{
	if (!SaveImportExportOptions())
		return;

	SaveDatabaseOptions();

	done(1);
}


void AdminTool::on_btnCancel_clicked()
{
	done(0);
}

void AdminTool::on_btnTemplate_clicked()
{

	//get db version:
	Status err;
	DbRecordSet lstData;
	_SERVER_CALL(ClientSimpleORM->Read(err,CORE_DATABASE_INFO,lstData))
	_CHK_ERR(err);

	//update: ask for startup, ask for template, reset demo key:
	if (lstData.getRowCount()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Database info could not be loaded"));
		return;
	}


	QByteArray key;
	//key=lstData.getDataRef(0,"CDI_SETUP_WIZARD_WIZARD_INFO").toByteArray();
	QDateTime lastModif,datStart;
	int nSize,nLocked;
	int nDbVersion;
	QString strAppVersion;
/*
	if(!Authenticator::Demo_ParseKey(key,strAppVersion,nDbVersion,lastModif,nSize,nLocked,datStart)) //ignore if locked!!!<-template db will be reorganized!
	{
		QMessageBox::warning(this,tr("Warning"),tr("Failed to reset demo key"));
		return;
	}
	QFileInfo sokrates(QCoreApplication::applicationDirPath()+"/sokrates.exe");
	if (sokrates.exists())
	{
		lastModif=sokrates.lastModified();
		nSize=sokrates.size();
	}
*/
	//client will reset those values:
	strAppVersion="";
	lastModif=QDateTime();
	nSize=0;
	if (!Authenticator::Demo_CreateKey(key,strAppVersion,DATABASE_VERSION,lastModif,nSize,0,QDateTime()))
	{
		QMessageBox::warning(this,tr("Warning"),tr("Failed to reset demo key"));
		return;
	}

	//set
	lstData.setData(0,"CDI_TEMPLATE_DB_USED",ui.txtTemplate->text());
	lstData.setData(0,"CDI_ASK_FOR_TEMPLATE_DB",0);
	lstData.setData(0,"CDI_START_SETUP_WIZARD",1);
	lstData.setData(0,"CDI_SETUP_WIZARD_WIZARD_INFO",key);

	//set controls:
	ui.ckbAskForTemplate->setChecked(false);
	ui.ckbStartWizard->setChecked(true);

	//save:
	QString pLockResourceID;
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	DbRecordSet lstForDelete;
	_SERVER_CALL(ClientSimpleORM->Write(err,CORE_DATABASE_INFO,lstData, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
	_CHK_ERR(err);

	on_btnBackup_clicked();
	QMessageBox::information(this,tr("Information"),tr("Last created Backup is template!"));
	QApplication::closeAllWindows(); //exit & logout
}

void AdminTool::on_ckbOldBackup_stateChanged(int)
{
	if (ui.ckbOldBackup->isChecked())
	{
		ui.txtOldBackupDay->setEnabled(true);
	}
	else
	{
		ui.txtOldBackupDay->setEnabled(false);
	}
}


void AdminTool::RefreshEnabledWidgets_Project()
{
	if (ui.ckbEnable_Project->isChecked())
	{
		ui.groupBox_Project_1->setEnabled(true);
		ui.groupBox_Project_2->setEnabled(true);
		ui.groupBox_Project_3->setEnabled(true);
		ui.groupBox_Project_4->setEnabled(true);
		ui.groupBox_Project_5->setEnabled(true);
		ui.btnProcess_Project->setEnabled(true);
	}
	else
	{
		ui.groupBox_Project_1->setEnabled(false);
		ui.groupBox_Project_2->setEnabled(false);
		ui.groupBox_Project_3->setEnabled(false);
		ui.groupBox_Project_4->setEnabled(false);
		ui.groupBox_Project_5->setEnabled(false);
		ui.btnProcess_Project->setEnabled(false);
	}

	if (ui.rbScanPeriod_Project->isChecked())
	{
		ui.txtPeriod_Project->setEnabled(true);
	}
	else
	{
		ui.txtPeriod_Project->setEnabled(false);
	}
	

	if (ui.rbLastN_Project->isChecked())
	{
		ui.txtLastN_Project->setEnabled(true);
	}
	else
	{
		ui.txtLastN_Project->setEnabled(false);
	}


	QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_STATUS_REC_FAIL").toString();
	ui.labelStatus_Project->setText(HTML_START_F1+strMsg+HTML_END_F1);
}

void AdminTool::on_ckbEnable_Project_stateChanged(int)
{
	RefreshEnabledWidgets_Project();
}

void AdminTool::on_rbScanPeriod_Project_toggled(bool)
{
	if (ui.rbFTP_Project->isChecked() && ui.rbScanAuto_Project->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_Project->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_PROJ_SCAN_TYPE",0);
		return;
	}

	RefreshEnabledWidgets_Project();
}

void AdminTool::on_rbLastN_Project_toggled(bool)
{
	RefreshEnabledWidgets_Project();
}


void AdminTool::on_btnSelectPath_Project_clicked()
{
	if (!g_pClientManager->IsClientIPMatchHost())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Directory can only be selected on server"));
		return;
	}
	

	QString strStartDir=ui.txtPath->toPlainText();

	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetApplicationHomeDir();
	}

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Select export/import directory"),
		strStartDir);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath_Project->setText(strFile);
	}

}

bool AdminTool::SaveImportExportOptions()
{

	m_lstImportExportSettings.setData(0,"CIE_PROJ_SOURCE_DIR",ui.txtPath_Project->toPlainText());
	m_lstImportExportSettings.setData(0,"CIE_USER_SOURCE_DIR",ui.txtPath_User->toPlainText());
	m_lstImportExportSettings.setData(0,"CIE_DEBT_SOURCE_DIR",ui.txtPath_Debtor->toPlainText());
	m_lstImportExportSettings.setData(0,"CIE_CONT_SOURCE_DIR",ui.txtPath_Contact->toPlainText());
	m_lstImportExportSettings.setData(0,"CIE_SPLST_SOURCE_DIR",ui.txtPath_SPL->toPlainText());
	
	SaveFTPID(); //save from combos->data
	SaveRoleID();

	//check settings: issue: 1269
	if(m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_ENABLED").toInt()!=0)
	{
		if (ui.rbDir_Project->isChecked())
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_SOURCE_DIR").toString().isEmpty())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Project synchronization is enabled, but source directory is not specified!"));
				return false; 
			}
		}
		else
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_SOURCE_FTP_ID").toInt()<=0)
			{
				QMessageBox::warning(this,tr("Warning"),tr("Project synchronization is enabled, but source ftp server/directory is not specified!"));
				return false; 
			}

			//if auto file watcher then abort (on fto only scan period):
			if (ui.rbScanAuto_Project->isChecked())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
				ui.rbScanPeriod_Project->setChecked(true);
				m_lstImportExportSettings.setData(0,"CIE_PROJ_SCAN_TYPE",0);
				return false;
			}
		}
	}

	
	//check settings: issue: 1269
	if(m_lstImportExportSettings.getDataRef(0,"CIE_USER_ENABLED").toInt()!=0)
	{
		if (ui.rbDir_User->isChecked())
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_USER_SOURCE_DIR").toString().isEmpty())
			{
				QMessageBox::warning(this,tr("Warning"),tr("User synchronization is enabled, but source directory is not specified!"));
				return false; 
			}
		}
		else
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_USER_SOURCE_FTP_ID").toInt()<=0)
			{
				QMessageBox::warning(this,tr("Warning"),tr("User synchronization is enabled, but source ftp server/directory is not specified!"));
				return false; 
			}

			//if auto file watcher then abort (on fto only scan period):
			if (ui.rbScanAuto_User->isChecked())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
				ui.rbScanPeriod_User->setChecked(true);
				m_lstImportExportSettings.setData(0,"CIE_USER_SCAN_TYPE",0);
				return false;
			}

		}
	}
	if(m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_ENABLED").toInt()!=0)
	{

		if (ui.rbDir_Debtor->isChecked())
		{
			//check settings: issue: 1269
			if (m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_SOURCE_DIR").toString().isEmpty())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Debtor synchronization is enabled, but source directory is not specified!"));
				return false; 
			}
		}
		else
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_SOURCE_FTP_ID").toInt()<=0)
			{
				QMessageBox::warning(this,tr("Warning"),tr("Debtor synchronization is enabled, but source ftp server/directory is not specified!"));
				return false; 
			}

			//if auto file watcher then abort (on fto only scan period):
			if (ui.rbScanAuto_Debtor->isChecked())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
				ui.rbScanPeriod_Debtor->setChecked(true);
				m_lstImportExportSettings.setData(0,"CIE_DEBT_SCAN_TYPE",0);
				return false;
			}

		}
	}

	if (m_lstImportExportSettings.getDataRef(0,"CIE_CONT_ENABLED").toInt()!=0)
	{
	
		//check settings: issue: 1269
		if (ui.rbDir_Contact->isChecked())
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_CONT_SOURCE_DIR").toString().isEmpty())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Contact synchronization is enabled, but source directory is not specified!"));
				return false; 
			}
		}
		else
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_CONT_SOURCE_FTP_ID").toInt()<=0)
			{
				QMessageBox::warning(this,tr("Warning"),tr("Contact synchronization is enabled, but source ftp server/directory is not specified!"));
				return false; 
			}

			//if auto file watcher then abort (on fto only scan period):
			if (ui.rbScanAuto_Contact->isChecked())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
				ui.rbScanPeriod_Contact->setChecked(true);
				m_lstImportExportSettings.setData(0,"CIE_CONT_SCAN_TYPE",0);
				return false;
			}

		}
	}

	if (m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_ENABLED").toInt()!=0)
	{

		//check settings: issue: 1269
		if (ui.rbDir_SPL->isChecked())
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_SOURCE_DIR").toString().isEmpty())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Stored Project List synchronization is enabled, but source directory is not specified!"));
				return false; 
			}
		}
		else
		{
			if (m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_SOURCE_FTP_ID").toInt()<=0)
			{
				QMessageBox::warning(this,tr("Warning"),tr("Stored Project List synchronization is enabled, but source ftp server/directory is not specified!"));
				return false; 
			}

			//if auto file watcher then abort (on fto only scan period):
			if (ui.rbScanAuto_SPL->isChecked())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
				ui.rbScanPeriod_SPL->setChecked(true);
				m_lstImportExportSettings.setData(0,"CIE_SPLST_SCAN_TYPE",0);
				return false;
			}

		}
	}

	//m_lstImportExportSettings.Dump();

	//save
	Status err;
	_SERVER_CALL(ServerControl->SaveImportExportSettings(err,m_lstImportExportSettings))
	_CHK_ERR_RET_BOOL_ON_FAIL(err);

	//load
	if(m_ImportExportManager)
	{
		m_ImportExportManager->ReloadSettings(err);
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
		m_ImportExportManager->GetSettings(err,m_lstImportExportSettings);
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}
	m_GuiFieldManagerImportExport->RefreshDisplay();

	//if thick restart timer:
	if(m_ImportExportManager)m_ImportExportManager->ThickModeRestartTimer();

	return true;

}


void AdminTool::on_btnProcess_Project_clicked()
{
	if(!g_AccessRight->TestAR(EXPORT_PROJECTS) && ui.rbExport_Project->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}

	if(!g_AccessRight->TestAR(IMPORT_PROJECTS) && ui.rbImport_Project->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}



	if(!SaveImportExportOptions())
		return;


	Status err;
	_SERVER_CALL(ServerControl->StartProcessProject(err))
	_CHK_ERR(err);
	if (g_pClientManager->IsThinClient())
	{
		QMessageBox::information(this,tr("Information"),tr("Process started on server. Server will notify when operation is completed!"));
		return;
	}
	if(m_ImportExportManager)
	{
		m_ImportExportManager->ReloadSettings(err);
		_CHK_ERR(err);
		m_ImportExportManager->GetSettings(err,m_lstImportExportSettings);
		_CHK_ERR(err);
	}

	m_GuiFieldManagerImportExport->RefreshDisplay();
	RefreshEnabledWidgets_Project();

	//clear cache:
	g_ClientCache.PurgeCache(ENTITY_BUS_PROJECT);
	//fire reload signal to all selectors:
	QVariant empty;
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_PROJECT,empty);

	//QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_STATUS_REC_FAIL").toString();
	//QMessageBox::information(this,tr("Information"),strMsg);

}



void AdminTool::on_btnProcess_User_clicked()
{
	if(!SaveImportExportOptions())
		return;


	Status err;
	_SERVER_CALL(ServerControl->StartProcessUser(err))
	_CHK_ERR(err);
	if (g_pClientManager->IsThinClient())
	{
		QMessageBox::information(this,tr("Information"),tr("Process started on server. You will be notified when operation is completed!"));
		return;
	}
	if(m_ImportExportManager)
	{
		m_ImportExportManager->ReloadSettings(err);
		_CHK_ERR(err);
		m_ImportExportManager->GetSettings(err,m_lstImportExportSettings);
		_CHK_ERR(err);
	}
	m_GuiFieldManagerImportExport->RefreshDisplay();
	RefreshEnabledWidgets_User();

	//clear cache:
	g_ClientCache.PurgeCache(ENTITY_BUS_PERSON);
	//fire reload signal to all selectors:
	QVariant empty;
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_PERSON,empty);

	//QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_USER_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_USER_STATUS_REC_FAIL").toString();
	//QMessageBox::information(this,tr("Information"),strMsg);

}

void AdminTool::on_ckbEnable_User_stateChanged(int)
{
	RefreshEnabledWidgets_User();
}

void AdminTool::on_rbScanPeriod_User_toggled(bool)
{
	if (ui.rbFTP_User->isChecked() && ui.rbScanAuto_User->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_User->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_USER_SCAN_TYPE",0);
		return;
	}

	RefreshEnabledWidgets_User();
}

void AdminTool::on_rbLastN_User_toggled(bool)
{
	RefreshEnabledWidgets_User();
}


void AdminTool::on_btnSelectPath_User_clicked()
{
	if (!g_pClientManager->IsClientIPMatchHost())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Directory can only be selected on server"));
		return;
	}


	QString strStartDir=ui.txtPath->toPlainText();

	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetApplicationHomeDir();
	}

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Select export/import directory"),
		strStartDir);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath_User->setText(strFile);
	}

}



void AdminTool::RefreshEnabledWidgets_User()
{
	if (ui.ckbEnable_User->isChecked())
	{
		ui.groupBox_User_1->setEnabled(true);
		ui.groupBox_User_2->setEnabled(true);
		ui.groupBox_User_3->setEnabled(true);
		ui.groupBox_User_4->setEnabled(true);
		ui.groupBox_User_5->setEnabled(true);
		ui.groupBox_User_7->setEnabled(true);
		ui.btnProcess_User->setEnabled(true);
	}
	else
	{
		ui.groupBox_User_1->setEnabled(false);
		ui.groupBox_User_2->setEnabled(false);
		ui.groupBox_User_3->setEnabled(false);
		ui.groupBox_User_4->setEnabled(false);
		ui.groupBox_User_5->setEnabled(false);
		ui.groupBox_User_7->setEnabled(false);
		ui.btnProcess_User->setEnabled(false);
	}

	if (ui.rbScanPeriod_User->isChecked())
	{
		ui.txtPeriod_User->setEnabled(true);
	}
	else
	{
		ui.txtPeriod_User->setEnabled(false);
	}


	if (ui.rbLastN_User->isChecked())
	{
		ui.txtLastN_User->setEnabled(true);
	}
	else
	{
		ui.txtLastN_User->setEnabled(false);
	}


	QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_USER_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_USER_STATUS_REC_FAIL").toString();
	ui.labelStatus_User->setText(HTML_START_F1+strMsg+HTML_END_F1);
}


void AdminTool::RefreshEnabledWidgets_Debtor()
{
	if (ui.ckbEnable_Debtor->isChecked())
	{
		ui.groupBox_Debtor_1->setEnabled(true);
		ui.groupBox_Debtor_2->setEnabled(true);
		ui.groupBox_Debtor_3->setEnabled(true);
		ui.groupBox_Debtor_4->setEnabled(true);
		ui.groupBox_Debtor_5->setEnabled(true);
		ui.btnProcess_Debtor->setEnabled(true);
	}
	else
	{
		ui.groupBox_Debtor_1->setEnabled(false);
		ui.groupBox_Debtor_2->setEnabled(false);
		ui.groupBox_Debtor_3->setEnabled(false);
		ui.groupBox_Debtor_4->setEnabled(false);
		ui.groupBox_Debtor_5->setEnabled(false);
		ui.btnProcess_Debtor->setEnabled(false);
	}

	if (ui.rbScanPeriod_Debtor->isChecked())
	{
		ui.txtPeriod_Debtor->setEnabled(true);
	}
	else
	{
		ui.txtPeriod_Debtor->setEnabled(false);
	}


	if (ui.rbLastN_Debtor->isChecked())
	{
		ui.txtLastN_Debtor->setEnabled(true);
	}
	else
	{
		ui.txtLastN_Debtor->setEnabled(false);
	}


	QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_STATUS_REC_FAIL").toString();
	ui.labelStatus_Debtor->setText(HTML_START_F1+strMsg+HTML_END_F1);
}


void AdminTool::RefreshEnabledWidgets_Contact()
{
	if (ui.ckbEnable_Contact->isChecked())
	{
		ui.groupBox_Contact_1->setEnabled(true);
		ui.groupBox_Contact_2->setEnabled(true);
		ui.groupBox_Contact_3->setEnabled(true);
		ui.groupBox_Contact_4->setEnabled(true);
		ui.btnProcess_Contact->setEnabled(true);
	}
	else
	{
		ui.groupBox_Contact_1->setEnabled(false);
		ui.groupBox_Contact_2->setEnabled(false);
		ui.groupBox_Contact_3->setEnabled(false);
		ui.groupBox_Contact_4->setEnabled(false);
		ui.btnProcess_Contact->setEnabled(false);
	}

	if (ui.rbScanPeriod_Contact->isChecked())
	{
		ui.txtPeriod_Contact->setEnabled(true);
	}
	else
	{
		ui.txtPeriod_Contact->setEnabled(false);
	}


	if (ui.rbLastN_Contact->isChecked())
	{
		ui.txtLastN_Contact->setEnabled(true);
	}
	else
	{
		ui.txtLastN_Contact->setEnabled(false);
	}

	QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_CONT_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_CONT_STATUS_REC_FAIL").toString();
	ui.labelStatus_Contact->setText(HTML_START_F1+strMsg+HTML_END_F1);
}



void AdminTool::on_btnProcess_Debtor_clicked()
{
	if(!SaveImportExportOptions())
		return;


	Status err;
	_SERVER_CALL(ServerControl->StartProcessDebtor(err));
	_CHK_ERR(err);
	if (g_pClientManager->IsThinClient())
	{
		QMessageBox::information(this,tr("Information"),tr("Process started on server. You will be notified when operation is completed!"));
		return;
	}
	if(m_ImportExportManager)
	{
		m_ImportExportManager->ReloadSettings(err);
		_CHK_ERR(err);
		m_ImportExportManager->GetSettings(err,m_lstImportExportSettings);
		_CHK_ERR(err);
	}
	m_GuiFieldManagerImportExport->RefreshDisplay();
	RefreshEnabledWidgets_Debtor();

	//QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_STATUS_REC_FAIL").toString();
	//QMessageBox::information(this,tr("Information"),strMsg);
}

void AdminTool::on_ckbEnable_Debtor_stateChanged(int)
{
	RefreshEnabledWidgets_Debtor();
}

void AdminTool::on_rbScanPeriod_Debtor_toggled(bool)
{
	if (ui.rbFTP_Debtor->isChecked() && ui.rbScanAuto_Debtor->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_Debtor->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_DEBT_SCAN_TYPE",0);
		return;
	}
	RefreshEnabledWidgets_Debtor();
}

void AdminTool::on_rbLastN_Debtor_toggled(bool)
{
	RefreshEnabledWidgets_Debtor();
}

void AdminTool::on_rbLastN_Contact_toggled(bool)
{
	RefreshEnabledWidgets_Contact();
}


void AdminTool::on_btnSelectPath_Debtor_clicked()
{
	if (!g_pClientManager->IsClientIPMatchHost())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Directory can only be selected on server"));
		return;
	}


	QString strStartDir=ui.txtPath->toPlainText();

	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetApplicationHomeDir();
	}

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Select export/import directory"),
		strStartDir);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath_Debtor->setText(strFile);
	}

}




void AdminTool::on_btnProcess_Contact_clicked()
{
	if(!SaveImportExportOptions())
		return;


	Status err;
	_SERVER_CALL(ServerControl->StartProcessContact(err));
	_CHK_ERR(err);
	if (g_pClientManager->IsThinClient())
	{
		QMessageBox::information(this,tr("Information"),tr("Process started on server. You will be notified when operation is completed!"));
		return;
	}
	if(m_ImportExportManager)
	{
		m_ImportExportManager->ReloadSettings(err);
		_CHK_ERR(err);
		m_ImportExportManager->GetSettings(err,m_lstImportExportSettings);
		_CHK_ERR(err);
	}
	m_GuiFieldManagerImportExport->RefreshDisplay();
	RefreshEnabledWidgets_Contact();

	//clear cache:
	g_ClientCache.PurgeCache(ENTITY_BUS_CONTACT);
	//fire reload signal to all selectors:
	QVariant empty;
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_CONTACT,empty);

	//QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_CONT_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_CONT_STATUS_REC_FAIL").toString();
	//QMessageBox::information(this,tr("Information"),strMsg);

}

void AdminTool::on_ckbEnable_Contact_stateChanged(int)
{
	RefreshEnabledWidgets_Contact();
}

void AdminTool::on_rbScanPeriod_Contact_toggled(bool)
{
	if (ui.rbFTP_Contact->isChecked() && ui.rbScanAuto_Contact->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_Contact->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_CONT_SCAN_TYPE",0);
		return;
	}

	RefreshEnabledWidgets_Contact();
}



void AdminTool::on_btnSelectPath_Contact_clicked()
{
	if (!g_pClientManager->IsClientIPMatchHost())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Directory can only be selected on server"));
		return;
	}


	QString strStartDir=ui.txtPath->toPlainText();

	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetApplicationHomeDir();
	}

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Select export/import directory"),
		strStartDir);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath_Contact->setText(strFile);
	}

}



bool AdminTool::ReloadFTPServers()
{

	//load ftp combos:
	Status pStatus;
	DbRecordSet lstFTPs;
	_SERVER_CALL(ClientSimpleORM->Read(pStatus, CORE_FTP_SERVERS, lstFTPs))
	_CHK_ERR_RET_BOOL_ON_FAIL(pStatus);

	ui.cmbFTP_Project->clear();
	ui.cmbFTP_Contact->clear();
	ui.cmbFTP_Debtor->clear();
	ui.cmbFTP_User->clear();
	ui.cmbFTP_SPL->clear();


	int nSize=lstFTPs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.cmbFTP_Project->addItem(lstFTPs.getDataRef(i,"CFTP_NAME").toString(),lstFTPs.getDataRef(i,"CFTP_ID").toInt());
		ui.cmbFTP_Contact->addItem(lstFTPs.getDataRef(i,"CFTP_NAME").toString(),lstFTPs.getDataRef(i,"CFTP_ID").toInt());
		ui.cmbFTP_Debtor->addItem(lstFTPs.getDataRef(i,"CFTP_NAME").toString(),lstFTPs.getDataRef(i,"CFTP_ID").toInt());
		ui.cmbFTP_User->addItem(lstFTPs.getDataRef(i,"CFTP_NAME").toString(),lstFTPs.getDataRef(i,"CFTP_ID").toInt());
		ui.cmbFTP_SPL->addItem(lstFTPs.getDataRef(i,"CFTP_NAME").toString(),lstFTPs.getDataRef(i,"CFTP_ID").toInt());
	}

	return true;
	
}

void AdminTool::on_btnSelectFTP_Project_clicked()
{
	Dlg_FTPServers dlg;
	if(!dlg.Initialize(ui.cmbFTP_Project->currentText()))
		return;

	if(dlg.exec())
	{
		DbRecordSet lstData;
		int nRow;
		dlg.GetServers(lstData,nRow);
		int nFTPID=lstData.getDataRef(nRow,0).toInt();
		SaveFTPID();
		ReloadFTPServers();
		LoadFTPID();
		ui.cmbFTP_Project->setCurrentIndex(ui.cmbFTP_Project->findData(nFTPID));
	}
}

void AdminTool::on_btnSelectFTP_Contact_clicked()
{
	Dlg_FTPServers dlg;
	if(!dlg.Initialize(ui.cmbFTP_Debtor->currentText()))
		return;

	if(dlg.exec())
	{
		DbRecordSet lstData;
		int nRow;
		dlg.GetServers(lstData,nRow);
		int nFTPID=lstData.getDataRef(nRow,0).toInt();
		SaveFTPID();
		ReloadFTPServers();
		LoadFTPID();
		ui.cmbFTP_Project->setCurrentIndex(ui.cmbFTP_Project->findData(nFTPID));
	}
}

void AdminTool::on_btnSelectFTP_User_clicked()
{
	Dlg_FTPServers dlg;
	if(!dlg.Initialize(ui.cmbFTP_User->currentText()))
		return;

	if(dlg.exec())
	{
		DbRecordSet lstData;
		int nRow;
		dlg.GetServers(lstData,nRow);
		int nFTPID=lstData.getDataRef(nRow,0).toInt();
		SaveFTPID();
		ReloadFTPServers();
		LoadFTPID();
		ui.cmbFTP_Project->setCurrentIndex(ui.cmbFTP_Project->findData(nFTPID));
	}
}

void AdminTool::on_btnSelectFTP_Debtor_clicked()
{
	Dlg_FTPServers dlg;
	if(!dlg.Initialize(ui.cmbFTP_Debtor->currentText()))
		return;

	if(dlg.exec())
	{
		DbRecordSet lstData;
		int nRow;
		dlg.GetServers(lstData,nRow);
		int nFTPID=lstData.getDataRef(nRow,0).toInt();
		SaveFTPID();
		ReloadFTPServers();
		LoadFTPID();
		ui.cmbFTP_Project->setCurrentIndex(ui.cmbFTP_Project->findData(nFTPID));
	}
}




void AdminTool::SaveFTPID()
{
	if (m_pData->GetDataSource()->getRowCount()>0)
	{
		if (ui.cmbFTP_Project->currentIndex()==-1 || ui.cmbFTP_Project->count()==0 || ui.rbDir_Project->isChecked())
			m_pData->GetDataSource()->setData(0,"CIE_PROJ_SOURCE_FTP_ID",QVariant(QVariant::Int)); //set to null
		else
			m_pData->GetDataSource()->setData(0,"CIE_PROJ_SOURCE_FTP_ID",ui.cmbFTP_Project->itemData(ui.cmbFTP_Project->currentIndex()).toInt());
	
		if (ui.cmbFTP_User->currentIndex()==-1 || ui.cmbFTP_User->count()==0 || ui.rbDir_User->isChecked())
			m_pData->GetDataSource()->setData(0,"CIE_USER_SOURCE_FTP_ID",QVariant(QVariant::Int)); //set to null
		else
			m_pData->GetDataSource()->setData(0,"CIE_USER_SOURCE_FTP_ID",ui.cmbFTP_User->itemData(ui.cmbFTP_User->currentIndex()).toInt());

		if (ui.cmbFTP_Debtor->currentIndex()==-1 || ui.cmbFTP_Debtor->count()==0 || ui.rbDir_Debtor->isChecked())
			m_pData->GetDataSource()->setData(0,"CIE_DEBT_SOURCE_FTP_ID",QVariant(QVariant::Int)); //set to null
		else
			m_pData->GetDataSource()->setData(0,"CIE_DEBT_SOURCE_FTP_ID",ui.cmbFTP_Debtor->itemData(ui.cmbFTP_Debtor->currentIndex()).toInt());

		if (ui.cmbFTP_Contact->currentIndex()==-1 || ui.cmbFTP_Contact->count()==0 || ui.rbDir_Contact->isChecked())
			m_pData->GetDataSource()->setData(0,"CIE_CONT_SOURCE_FTP_ID",QVariant(QVariant::Int)); //set to null
		else
			m_pData->GetDataSource()->setData(0,"CIE_CONT_SOURCE_FTP_ID",ui.cmbFTP_Contact->itemData(ui.cmbFTP_Contact->currentIndex()).toInt());

		if (ui.cmbFTP_SPL->currentIndex()==-1 || ui.cmbFTP_SPL->count()==0 || ui.rbDir_SPL->isChecked())
			m_pData->GetDataSource()->setData(0,"CIE_SPLST_SOURCE_FTP_ID",QVariant(QVariant::Int)); //set to null
		else
			m_pData->GetDataSource()->setData(0,"CIE_SPLST_SOURCE_FTP_ID",ui.cmbFTP_SPL->itemData(ui.cmbFTP_SPL->currentIndex()).toInt());

	}
}

void AdminTool::SaveRoleID()
{
	if (m_pData->GetDataSource()->getRowCount()>0)
	{
		if (ui.cboRoles->currentIndex()==-1 || ui.cboRoles->count()==0)
			m_pData->GetDataSource()->setData(0,"CIE_USER_DEF_ROLE_ID",QVariant(QVariant::Int)); //set to null
		else
			m_pData->GetDataSource()->setData(0,"CIE_USER_DEF_ROLE_ID",ui.cboRoles->itemData(ui.cboRoles->currentIndex()).toInt());
	}

}

void AdminTool::LoadRoleID()
{
	int nIndex=0;
	if (m_pData->GetDataSource()->getRowCount()>0)
		nIndex=ui.cboRoles->findData(m_pData->GetDataSource()->getDataRef(0,"CIE_USER_DEF_ROLE_ID").toInt());
	ui.cboRoles->setCurrentIndex(nIndex);
}


void AdminTool::LoadFTPID()
{
	//FTP combo:
	int nIndex=0;
	if (m_pData->GetDataSource()->getRowCount()>0)
		nIndex=ui.cmbFTP_Project->findData(m_pData->GetDataSource()->getDataRef(0,"CIE_PROJ_SOURCE_FTP_ID").toInt());
	ui.cmbFTP_Project->setCurrentIndex(nIndex);

	//FTP combo:
	nIndex=0;
	if (m_pData->GetDataSource()->getRowCount()>0)
		nIndex=ui.cmbFTP_User->findData(m_pData->GetDataSource()->getDataRef(0,"CIE_USER_SOURCE_FTP_ID").toInt());
	ui.cmbFTP_User->setCurrentIndex(nIndex);

	//FTP combo:
	nIndex=0;
	if (m_pData->GetDataSource()->getRowCount()>0)
		nIndex=ui.cmbFTP_Debtor->findData(m_pData->GetDataSource()->getDataRef(0,"CIE_DEBT_SOURCE_FTP_ID").toInt());
	ui.cmbFTP_Debtor->setCurrentIndex(nIndex);

	//FTP combo:
	nIndex=0;
	if (m_pData->GetDataSource()->getRowCount()>0)
		nIndex=ui.cmbFTP_Contact->findData(m_pData->GetDataSource()->getDataRef(0,"CIE_CONT_SOURCE_FTP_ID").toInt());
	ui.cmbFTP_Contact->setCurrentIndex(nIndex);

	//FTP combo:
	nIndex=0;
	if (m_pData->GetDataSource()->getRowCount()>0)
		nIndex=ui.cmbFTP_SPL->findData(m_pData->GetDataSource()->getDataRef(0,"CIE_SPLST_SOURCE_FTP_ID").toInt());
	ui.cmbFTP_SPL->setCurrentIndex(nIndex);
}



//do not allow file watcher in same time:
void AdminTool::on_rbDir_Project_toggled(bool)
{
	if (ui.rbFTP_Project->isChecked() && ui.rbScanAuto_Project->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_Project->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_PROJ_SCAN_TYPE",0);
	}
}

//do not allow file watcher in same time:
void AdminTool::on_rbDir_Debtor_toggled(bool)
{
	if (ui.rbFTP_Debtor->isChecked() && ui.rbScanAuto_Debtor->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_Debtor->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_DEBT_SCAN_TYPE",0);
	}
}

//do not allow file watcher in same time:
void AdminTool::on_rbDir_Contact_toggled(bool)
{
	if (ui.rbFTP_Contact->isChecked() && ui.rbScanAuto_Contact->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_Contact->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_CONT_SCAN_TYPE",0);
	}
}

//do not allow file watcher in same time:
void AdminTool::on_rbDir_User_toggled(bool)
{
	if (ui.rbFTP_User->isChecked() && ui.rbScanAuto_User->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_User->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_USER_SCAN_TYPE",0);
	}
}


//do not allow file watcher in same time:
void AdminTool::on_rbDir_SPL_toggled(bool)
{
	if (ui.rbFTP_SPL->isChecked() && ui.rbScanAuto_SPL->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_Project->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_SPLST_SCAN_TYPE",0);
	}
}



void AdminTool::OnBackup_Download()
{
	if (!g_pClientManager->IsThinClient())
		return;

	DbRecordSet row=m_lstBackups.getSelectedRecordSet();
	if (row.getRowCount()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select a backup file!"));
		return;
	}

	QString strStartDir=DataHelper::GetMyDocumentsDir();
	QString strName=row.getDataRef(0,"FILE").toString();
	QString strFilter =strName; 

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;

	QFileDialog file(this,QObject::tr("Save Backup File"),strStartDir,strFilter);
	file.selectFile(strName);
	file.setFileMode(QFileDialog::AnyFile); 
	file.setAcceptMode(QFileDialog::AcceptSave);
	QString strFile;
	if(file.exec())
	{
		if (file.selectedFiles().count()>0)
			strFile=file.selectedFiles().at(0);
	}
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	if(strFile.isEmpty())
		return;
	
	g_DownloadManager->ScheduleDownload(strFile,true,true);
}
void AdminTool::OnBackup_Upload()
{
	if (!g_pClientManager->IsThinClient())
		return;

	QString strStartDir=DataHelper::GetMyDocumentsDir();
	QString strFilter ="*.zip";
	//QString strFilter ="*.*";
	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName
		(
		NULL,
		QObject::tr("Load Backup File"),
		strStartDir,
		strFilter
		);

	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}

	//load picture into ByteArray
	if(strFile.isEmpty())
		return;

	g_DownloadManager->ScheduleUpload(strFile,true,true);

	//on_btnRefresh_clicked();
}
void AdminTool::OnBackup_Delete()
{	
	DbRecordSet row=m_lstBackups.getSelectedRecordSet();
	if (row.getRowCount()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select a backup file!"));
		return;
	}

	QString strName =row.getDataRef(0,"FILE").toString();
	QString	strMsg=tr("Do you really want to delete the following backup file: ")+ strName+"?";

	int nResult=QMessageBox::question(this,tr("Warning"),strMsg,tr("Yes"),tr("No"));
	if (nResult!=0)
		return; //only if YES

	Status err;
	_SERVER_CALL(ServerControl->DeleteBackupFile(err,strName))
	_CHK_ERR(err)

	on_btnRefresh_clicked();
}

void AdminTool::CheckOnLoad()
{
	if (m_lstImportExportSettings.getRowCount()!=1)
		return;

	if (m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_SCAN_TYPE").toInt()==1 && m_lstImportExportSettings.getDataRef(0,"CIE_PROJ_SOURCE").toInt()==1)
		m_lstImportExportSettings.setData(0,"CIE_PROJ_SCAN_TYPE",0);
	if (m_lstImportExportSettings.getDataRef(0,"CIE_USER_SCAN_TYPE").toInt()==1 && m_lstImportExportSettings.getDataRef(0,"CIE_USER_SOURCE").toInt()==1)
		m_lstImportExportSettings.setData(0,"CIE_USER_SCAN_TYPE",0);
	if (m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_SCAN_TYPE").toInt()==1 && m_lstImportExportSettings.getDataRef(0,"CIE_DEBT_SOURCE").toInt()==1)
		m_lstImportExportSettings.setData(0,"CIE_DEBT_SCAN_TYPE",0);
	if (m_lstImportExportSettings.getDataRef(0,"CIE_CONT_SCAN_TYPE").toInt()==1 && m_lstImportExportSettings.getDataRef(0,"CIE_CONT_SOURCE").toInt()==1)
		m_lstImportExportSettings.setData(0,"CIE_CONT_SCAN_TYPE",0);
	if (m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_SCAN_TYPE").toInt()==1 && m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_SOURCE").toInt()==1)
		m_lstImportExportSettings.setData(0,"CIE_SPLST_SCAN_TYPE",0);

}

//manager gets msg, shows, refresh all after this:
void AdminTool::OnCommunicationServerMessageRecieved(DbRecordSet rowMsg)
{
	if (rowMsg.getRowCount()>0)
	{
		if (rowMsg.getDataRef(0,"MSG_CODE").toInt()==StatusCodeSet::MSG_SYSTEM_BACKUP_SUCCESS)
		{
			on_btnRefresh_clicked();
		}
	}
}





void AdminTool::on_ckbEnable_SPL_stateChanged(int)
{
	RefreshEnabledWidgets_SPL();
}

void AdminTool::on_rbScanPeriod_SPL_toggled(bool)
{
	if (ui.rbFTP_SPL->isChecked() && ui.rbScanAuto_SPL->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Only scan period option is allowed when using FTP. Please change period."));
		ui.rbScanPeriod_Project->setChecked(true);
		m_lstImportExportSettings.setData(0,"CIE_SPLST_SCAN_TYPE",0);
		return;
	}

	RefreshEnabledWidgets_SPL();
}

void AdminTool::on_btnSelectPath_SPL_clicked()
{
	if (!g_pClientManager->IsClientIPMatchHost())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Directory can only be selected on server"));
		return;
	}


	QString strStartDir=ui.txtPath_SPL->toPlainText();

	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetApplicationHomeDir();
	}

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Select export/import directory"),
		strStartDir);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath_SPL->setText(strFile);
	}

}



void AdminTool::on_btnProcess_SPL_clicked()
{
	
	/*
	if(!g_AccessRight->TestAR(EXPORT_PROJECTS) && ui.rbExport_Project->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}

	if(!g_AccessRight->TestAR(IMPORT_PROJECTS) && ui.rbImport_Project->isChecked())
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}
	*/

	if(!SaveImportExportOptions())
		return;


	Status err;
	_SERVER_CALL(ServerControl->StartProcessStoredProjectList(err))
		_CHK_ERR(err);
	if (g_pClientManager->IsThinClient())
	{
		QMessageBox::information(this,tr("Information"),tr("Process started on server. Server will notify when operation is completed!"));
		return;
	}
	if(m_ImportExportManager)
	{
		m_ImportExportManager->ReloadSettings(err);
		_CHK_ERR(err);
		m_ImportExportManager->GetSettings(err,m_lstImportExportSettings);
		_CHK_ERR(err);
	}

	m_GuiFieldManagerImportExport->RefreshDisplay();
	RefreshEnabledWidgets_SPL();

	//clear cache:
	//g_ClientCache.PurgeCache(ENTITY_BUS_PROJECT);
	//fire reload signal to all selectors:
	//QVariant empty;
	//g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_PROJECT,empty);

	QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_STATUS_REC_FAIL").toString();
	QMessageBox::information(this,tr("Information"),strMsg);

}


void AdminTool::on_btnSelectFTP_SPL_clicked()
{
	Dlg_FTPServers dlg;
	if(!dlg.Initialize(ui.cmbFTP_SPL->currentText()))
		return;

	if(dlg.exec())
	{
		DbRecordSet lstData;
		int nRow;
		dlg.GetServers(lstData,nRow);
		int nFTPID=lstData.getDataRef(nRow,0).toInt();
		SaveFTPID();
		ReloadFTPServers();
		LoadFTPID();
		ui.cmbFTP_SPL->setCurrentIndex(ui.cmbFTP_SPL->findData(nFTPID));
	}
}



void AdminTool::RefreshEnabledWidgets_SPL()
{
	if (ui.ckbEnable_SPL->isChecked())
	{
		ui.groupBox_SPL_1->setEnabled(true);
		ui.groupBox_SPL_2->setEnabled(true);
		ui.groupBox_SPL_3->setEnabled(true);
		ui.groupBox_SPL_4->setEnabled(true);
		ui.btnProcess_SPL->setEnabled(true);
	}
	else
	{
		ui.groupBox_SPL_1->setEnabled(false);
		ui.groupBox_SPL_2->setEnabled(false);
		ui.groupBox_SPL_3->setEnabled(false);
		ui.groupBox_SPL_4->setEnabled(false);
		ui.btnProcess_SPL->setEnabled(false);
	}

	if (ui.rbScanPeriod_SPL->isChecked())
	{
		ui.txtPeriod_SPL->setEnabled(true);
	}
	else
	{
		ui.txtPeriod_SPL->setEnabled(false);
	}

	QString strMsg=tr("Successfully processed: ")+m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_STATUS_REC_OK").toString()+tr(" records, failed to process: ")+m_lstImportExportSettings.getDataRef(0,"CIE_SPLST_STATUS_REC_FAIL").toString();
	ui.labelStatus_SPL->setText(HTML_START_F1+strMsg+HTML_END_F1);
}

/*
void AdminTool::on_btnSelectHeadersFile_clicked()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.dat";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;

	QString strHdrFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Choose Stored Project Lists Headers file"),
		strStartDir,
		strFilter);

	ui.txtHeadersFile->setText(strHdrFile);
}
	
void AdminTool::on_btnSelectDetailsFile_clicked()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.dat";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;

	QString strDetailFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Choose Stored Project Lists Details file"),
		strStartDir,
		strFilter);

	ui.txtDetailsFile->setText(strDetailFile);
}

void AdminTool::on_btnImportStoredProjLists_clicked()
{
	QString strHdrFile = ui.txtHeadersFile->text();
	QString strDetailFile = ui.txtDetailsFile->text();

	if(strHdrFile.isEmpty() || strDetailFile.isEmpty()){
		QMessageBox::information(this, "Error", tr("You must select both import files!"));
		return;
	}

	//Load both files into the byte array object
	QFile fileHdr(strHdrFile);
	if(!fileHdr.open(QIODevice::ReadOnly)){
		QMessageBox::information(this, "Error", QString(tr("Failed to load header file:\n%1!")).arg(strHdrFile));
		return;
	}
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	QByteArray datHeader = fileHdr.readAll();

	QFile fileDetail(strDetailFile);
	if(!fileDetail.open(QIODevice::ReadOnly)){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Error", QString(tr("Failed to load detail file:\n%1!")).arg(strHdrFile));
		return;
	}
	QByteArray datDetail = fileDetail.readAll();

	Status status;
	_SERVER_CALL(BusImport->ProcessStoredProjectList_Import(status,datHeader, datDetail))
	if(!status.IsOK()){
		QApplication::restoreOverrideCursor();
		QMessageBox::information(this, "Error", status.getErrorText());
		return;
	}
	QApplication::restoreOverrideCursor();
}
*/