#ifndef CALENDARTABLEVIEWITEM_H
#define CALENDARTABLEVIEWITEM_H

#include <QGraphicsProxyWidget>

#include "calendargraphicsview.h"
#include "calendartableview.h"

class CalendarTableViewItem : public QGraphicsProxyWidget
{
	Q_OBJECT

public:
	CalendarTableViewItem(CalendarGraphicsView *pCalendarGraphicsView);
	~CalendarTableViewItem();

	void	Initialize(QDate &startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites);

	void	setDateInterval(QDate &startDate, int nDayInterval);
	void	setTimeScale(CalendarWidget::timeScale scale);
	void	setPersonsHash(QHash<int, QString> hshPersons);
	QRect	getItemRectangle(QDateTime &startDateTime, QDateTime &endDateTime);
	QRect	getItemRectangle(QModelIndex &Index);
	QRect	getItemRectangleForHeader(int nColumn);
	void	getSelectionStartEndDateTime(QDateTime &startDateTime, QDateTime &endDateTime);

	CalendarTableModel* Model();

public slots:
	void on_resizeEvent();

private:
	CalendarTableView *m_pCalendarTableView;
	
};

#endif // CALENDARTABLEVIEWITEM_H
