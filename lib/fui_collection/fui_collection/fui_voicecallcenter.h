#ifndef FUI_VOICECALLCENTER_H
#define FUI_VOICECALLCENTER_H

#include <QWidget>
#include "generatedfiles/ui_fui_voicecallcenter.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "os_specific/os_specific/skype/skypeinterface.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "os_specific/os_specific/tapi/TapiLine.h"

class FUI_VoiceCallCenter : public FuiBase, public SkypeInterface
{
	//friend class SkypeParser;

	Q_OBJECT

public:
    FUI_VoiceCallCenter(QWidget *parent = 0);
    ~FUI_VoiceCallCenter();

	//SKYPE INTERFACE:
	void SetWindowState(int nState);
	void SetCallID(int nID);
	void SetCaller(QString strNumber);
	void SetCallState(QString strStatus);
	void OnUserResolved(int nTypeData,QString strHandle,QVariant varData);
	void OnChatResolved_Name(QString strName);
	void Wnd_ActivateWindow(){this->activateWindow();};
	void Wnd_SetWindowFlags(Qt::WindowFlags type){this->setWindowFlags(type);};

	void AllowPhoneInput(bool bAllow = true);
	void StartCall(QString strNumber, QString strFormattedNum);
	void EndCall();
	void LoadCallData(int nDbRecordID);
	void on_selectionChange(int nEntityRecordID);
	void SetShowTask(DbRecordSet &recData, bool bActive = true);
	QString GetFUIName();
	void UpdateDataFromGui();
	void RefreshDisplay();
	void OnRemoteNumberKnown(QString strNumber, int nInterface);
	
	DbRecordSet *GetTaskRecordSet();

	int GetTaskID();
	void RefreshAvatarData(bool bReload = false);

	void FilterContacts(QString strNumber, int nInterface, bool bFromManualSearch=false);
	virtual void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

	void RecalcDuration(bool bCurTime = false);
	void LoadPhoneCombo(DbRecordSet lstPhones);
	void SetDefaultContact(int nContactID,QString strPhoneDefault="",int nProjectID=-1);
	QTime ParseTime(QString strTime);

protected:
	virtual void changeEvent (QEvent * event);
	virtual void closeEvent(QCloseEvent *event);

public:
	DbRecordSet m_recData;

	DbRecordSet m_recPhoneTypes;	//for type id -> name

	//phones info grid columns
	QStringList m_lstPhonesFmt;		//formatted phones
	QStringList m_lstPhones;
	QStringList m_lstPhoneNames;
	QStringList m_lstPhoneTypes;	//converted from "BCMP_TYPE_ID"
	QList<int> m_lstPhoneTypeIDs;


#ifdef _WIN32
	//TAPI device IDs listed in the combo
	std::vector<int> m_lstTAPIDevIDs;
	CTapiLine m_lineTAPI;
#endif

	bool m_bHideOnMinimize;

protected:
	//int m_nWndState;
	//int	m_nCallID;
	//int	m_nCallConferenceID;
	int m_bCallOnHold;
	//int m_nCallState;
	bool m_bForwarded;
	bool m_bIncoming;
	bool m_bCallWasEstablished;
	
	bool m_bWaitForOnHold;
	QString m_strRedirectTo;

	//current number in two formats
	QString	m_strUnfmtNumber;
	QString	m_strFmtNumber;

	//Skype info for the remote user
	QString	m_strFullName;	// remote name
	QString	m_strBirthday;
	QString	m_strSex;
	QString	m_strCountry;
	QString	m_strProvince;
	QString	m_strCity;
	QString	m_strPhoneHome;
	QString	m_strPhoneOffice;
	QString	m_strPhoneMobile;
	QString	m_strHomepage;
	QString	m_strAbout;

	//
	QString	m_strLastSkypeStatUser;
	QString	m_strLastSkypeStatStatus;
	//QString	m_strStatus;
	QTime	m_callRingingTime;
	QTimer *m_pDurationTimer;
	QtPieMenu *m_pPieMenu;
	ClientContactManager m_ContactMan;
	DbRecordSet rowContactPicture;
	void LoadContactData(bool bClickedOnGrid=false);
	void LoadContactDataOnProjectChange();
	void CheckForProjectContactAssigment();

	void SetFUIStyle(){}; //BT override defaut style to enable special font buttons

	QTimer *m_pTapiTimer;

	DbRecordSet m_lstDataNMRXProjects;	//projects of a current contact

private:
    Ui::FUI_VoiceCallCenterClass ui;
	bool m_bSkipBackUpdate;
	//SKYPE INTERFACE:
	void OnUserResolved_FullName(QString strNumber, QString strFullName);
	void OnUserResolved_Birthday(QString strNumber, QString strData);
	void OnUserResolved_Sex(QString strNumber, QString strData);
	void OnUserResolved_Country(QString strNumber, QString strData);
	void OnUserResolved_Province(QString strNumber, QString strData);
	void OnUserResolved_City(QString strNumber, QString strData);
	void OnUserResolved_PhoneHome(QString strNumber, QString strData);
	void OnUserResolved_PhoneOffice(QString strNumber, QString strData);
	void OnUserResolved_PhoneMobile(QString strNumber, QString strData);
	void OnUserResolved_Homepage(QString strNumber, QString strData);
	void OnUserResolved_About(QString strNumber, QString strData);
	void OnUserResolved_Status(QString strNumber, QString strData);
	void OnThemeChanged();
	void BuildPhoneCache(DbRecordSet &lstPhones);

private slots:
	void on_btnEndTimeSetCurrent_clicked();
	void on_btnStartTimeSetCurrent_clicked();
	void on_btnSave_clicked();
	void on_btnCancel_clicked();
	void on_btnCallAction3_clicked();
	void on_btnCallAction2_clicked();
	void on_btnCallAction1_clicked();
	void on_btnScheduleTask_clicked();
	void on_btnPickPhone_clicked();
	void on_btnSearchContact_clicked();
	void on_btnOpenChat_clicked();
	void on_btnOpenSMS_clicked();
	void OnCallDurationTimer();
	void OnTAPICheckTimer();
	void OnContactSAPNEInsert();
	void on_btnAssignProjects_clicked();
	void on_btnInsertNewPhone_clicked();

	void OnScheduleCall();
	void OnScheduleEmail();
	void OnScheduleDoc(int nDocType);

	void OnCallStartTimeEdited();
	void OnCallEndTimeEdited();

	void UpdateSkypeStatus();
	
	void OnPrint();

	void continueRedirect();
	void StopVoicemail();
	void updateTaskTabCheckbox();
	void updateContactInfo();

};

#endif // FUI_VOICECALLCENTER_H
