#include "fui_importproject.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "common/common/cliententitycache.h"
#include "gui_core/gui_core/gui_helper.h"
#include "common/common/csvimportfile.h"
#include "common/common/datahelper.h"
#include <QFileDialog>
#include <QProgressDialog>

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;


#ifndef min
#define min(a,b) (((a)<(b))? (a):(b))
#endif

//build list modes
#define BLM2_REPLACE_EXISTING	0
#define BLM2_UPDATE_EXISTING	1
#define BLM2_IGNORE_EXISTING	2
#define BLM2_SKIP_EXISTING		3

extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
extern ClientEntityCache g_ClientCache;				//global client cache
extern QString g_strLastDir_FileOpen;


FUI_ImportProject::FUI_ImportProject(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	ui.cboImportSource->addItem(tr("Text File"));

	ui.radSkipExisting->setChecked(true);

	//define data record
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_IMPORT));
	m_lstData.addColumn(QVariant::String, "CALC_DEBTOR_CODES");
	m_lstData.addColumn(QVariant::String, "CALC_ROLES");


	ui.tableContacts->Initialize(&m_lstData);

	//define column headers
	DbRecordSet columns;
	ui.tableContacts->AddColumnToSetup(columns,"BUSP_CODE",tr("Project Code"),200,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BUSP_NAME",tr("Project Name"),450,false, "");
	ui.tableContacts->SetColumnSetup(columns);
	ui.tableContacts->SetEditMode(true);
	//delete some context menu actions
	QList<QAction*>&lstActions = ui.tableContacts->GetContextMenuActions();
	lstActions.removeAt(4);
	lstActions.removeAt(0);

	ui.frameOrganization->Initialize(ENTITY_BUS_ORGANIZATION);
	ui.frameOrganization->SetSelectionButtonLabel(tr("Organization:"));


	GUI_Helper::SetStyledButtonColorBkg(ui.btnBuildList, GUI_Helper::BTN_COLOR_YELLOW);
	GUI_Helper::SetStyledButtonColorBkg(ui.btnImportProjects, GUI_Helper::BTN_COLOR_YELLOW);

	m_Per.Initialize(ENTITY_BUS_PERSON);
	m_Per.ReloadData();
	m_Org.Initialize(ENTITY_BUS_ORGANIZATION);
	m_Org.ReloadData();
	m_Dep.Initialize(ENTITY_BUS_DEPARTMENT);
	m_Dep.ReloadData();
}

FUI_ImportProject::~FUI_ImportProject()
{
}

bool CsvProgress(unsigned long nUserData)
{
	QProgressDialog *pDlg = (QProgressDialog *)nUserData;
	Q_ASSERT(NULL != pDlg);
	
	if(pDlg){
		pDlg->setValue(1);
		qApp->processEvents();
		if (pDlg->wasCanceled())
			return false;	// abort
	}
	return true;
}

void FUI_ImportProject::on_btnBuildList_clicked()
{
	//
	// build person list from text file
	//
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                            g_strLastDir_FileOpen,
                                            tr("Import File (*.txt)"));
	if (!fileName.isEmpty())
	{
		QFileInfo fileInfo(fileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		QSize size(300,100);
		QProgressDialog progress(tr("Loading/parsing CSV file..."),tr("Cancel"),0,0,this);
		progress.setWindowTitle(tr("Operation In Progress"));
		progress.setMinimumSize(size);
		progress.setWindowModality(Qt::WindowModal);
		progress.setMinimumDuration(1200);//1.2s before pops up

		//QString strFormat = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects - Subformat: Project Main Short";
		DbRecordSet data;
		Status status;
		CsvImportFile import;

		QStringList lstFormatPlain;
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects - Subformat: Project Main Short";
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 2.00 - Format: Projects - Subformat: Project Main Short";
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 3.00 - Format: Projects - Subformat: Project Main Short";
		QStringList lstFormatUtf8;

		import.Load(status, fileName, data, lstFormatPlain, lstFormatUtf8, false, CsvProgress, (unsigned long)&progress);
		if(!status.IsOK()){
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}
		//data.Dump();

		bool bIsVersion2 = ("~~~SOKRATES Data Exchange - Version: 2.00 - Format: Projects - Subformat: Project Main Short" == import.GetHeaderLine());
		bool bIsVersion3 = ("~~~SOKRATES Data Exchange - Version: 3.00 - Format: Projects - Subformat: Project Main Short" == import.GetHeaderLine());

		//update progress
		progress.setValue(1);
		qApp->processEvents();
		if (progress.wasCanceled())
			return;

		int nFieldsCnt = data.getColumnCount();
		int nMandatoryCnt = (bIsVersion2)? 23 : 21;
		if (bIsVersion3)
		{
			nMandatoryCnt=40;
		}
		if(nMandatoryCnt != nFieldsCnt){
			QMessageBox::information(this, "Error", tr("Document does not contain %1 fields for each record!").arg(nMandatoryCnt));
			return;
		}
		//data.Dump();

		//merge results with destination list
		int nCnt = data.getRowCount();
		for(int i=0; i<nCnt; i++)
		{
			//update progress
			progress.setValue(1);
			qApp->processEvents();
			if (progress.wasCanceled())
				return;

			m_lstData.addRow();
			int nRow = m_lstData.getRowCount() - 1;
			
			int nCol = 0;
			m_lstData.setData(nRow, "BUSP_CODE",				data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUSP_NAME",				data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUSP_START_DATE",          DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BUSP_DEADLINE_DATE",       DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BUSP_COMPLETION_DATE",     DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BUSP_LATEST_COMPLETION_DATE",  DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BUSP_OFFER_DATE",			DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BUSP_OFFER_VALID_DATE",    DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BUSP_ORDER_DATE",          DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BUSP_CONTRACT_DATE",       DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
			m_lstData.setData(nRow, "BUSP_LEADER_CODE",			data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUSP_RESPONSIBLE_CODE",    data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUSP_DESCRIPTION",         data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUSP_ACTIVE_FLAG",			data.getDataRef(i, nCol++).toInt());
			m_lstData.setData(nRow, "BUSP_TEMPLATE_FLAG",       data.getDataRef(i, nCol++).toInt());
			m_lstData.setData(nRow, "BUSP_LOCATION",			data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUSP_TIME_BUDGET",         data.getDataRef(i, nCol++).toDouble());
			m_lstData.setData(nRow, "BUSP_FIXED_FEE",           data.getDataRef(i, nCol++).toDouble());
			m_lstData.setData(nRow, "BUSP_COST_BUDGET",         data.getDataRef(i, nCol++).toDouble());
			m_lstData.setData(nRow, "BUSP_ORGANIZATION_CODE",   data.getDataRef(i, nCol++).toString());
			m_lstData.setData(nRow, "BUSP_DEPARTMENT_CODE",     data.getDataRef(i, nCol++).toString());

			if (bIsVersion2 || bIsVersion3)
			{
				m_lstData.setData(nRow, "CALC_DEBTOR_CODES",	data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "CALC_ROLES",			data.getDataRef(i, nCol++).toString());
			}
			
			if (bIsVersion3)
			{
				m_lstData.setData(nRow, "BUSP_STATUS_CODE",			data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_AREA_CODE",			data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_SECTOR_CODE",			data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_INDUSTRY_CODE",		data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_SERVICE_TYPE_CODE",	data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_PROCESS_CODE",		data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_RATE_CAT_CODE",		data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_EXP_RATE_CAT_CODE",	data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_PLAN_COST_CAT_CODE",	data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_TYPE_CODE",			data.getDataRef(i, nCol++).toString());
				m_lstData.setData(nRow, "BUSP_DEF_ACTIVITY_CODE",	data.getDataRef(i, nCol++).toString());
				
				m_lstData.setData(nRow, "BUSP_INVOICING_TYPE",	data.getDataRef(i, nCol++).toInt());
				m_lstData.setData(nRow, "BUSP_PRODUCTIVE",		data.getDataRef(i, nCol++).toInt());
				m_lstData.setData(nRow, "BUSP_NOTCHARGEABLE",	data.getDataRef(i, nCol++).toInt());
				m_lstData.setData(nRow, "BUSP_INVOICEABLE",		data.getDataRef(i, nCol++).toInt());
				m_lstData.setData(nRow, "BUSP_ABSENTTIME",		data.getDataRef(i, nCol++).toInt());
				m_lstData.setData(nRow, "BUSP_HOLIDAYS",		data.getDataRef(i, nCol++).toInt());
			}
			
		}

		//m_lstData.Dump();
		m_lstData.selectAll();
		ui.tableContacts->RefreshDisplay();
		UpdateImportProjectsButton();
		ui.tableContacts->setFocus();
	}
}

void FUI_ImportProject::UpdateImportProjectsButton()
{
	int nCnt = ui.tableContacts->GetDataSource()->getRowCount(); //
	ui.btnImportProjects->setEnabled((nCnt > 0));
}

void FUI_ImportProject::on_btnImportProjects_clicked()
{
	//update build list mode
	int nOperation = 0;
	if(ui.radReplaceExisting->isChecked())
		nOperation = BLM2_REPLACE_EXISTING;
	else if(ui.radUpdateExisting->isChecked())
		nOperation = BLM2_UPDATE_EXISTING;
	else if(ui.radIgnoreExisting->isChecked())
		nOperation = BLM2_IGNORE_EXISTING;
	else
		nOperation = BLM2_SKIP_EXISTING;

	//extract selected rows (under new view format)
	DbRecordSet lstSelected;
	lstSelected.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));
	lstSelected.addColumn(QVariant::String, "CALC_DEBTOR_CODES");
	lstSelected.addColumn(QVariant::String, "CALC_ROLES");

	lstSelected.addColumn(QVariant::String, "BUSP_STATUS_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_AREA_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_SECTOR_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_INDUSTRY_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_SERVICE_TYPE_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_PROCESS_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_RATE_CAT_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_EXP_RATE_CAT_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_PLAN_COST_CAT_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_TYPE_CODE");
	lstSelected.addColumn(QVariant::String, "BUSP_DEF_ACTIVITY_CODE");

	lstSelected.merge(m_lstData, true);

	int nProjsToImport = lstSelected.getRowCount();
	if(nProjsToImport < 1){
		QMessageBox::information(this, "", tr("No items have been selected for import!"));
		return;
	}

	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_PROJECT_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_PROJECT, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed projects!\nCan not insert new projects!"));
			return;
		}
		else if ((nCnt + nProjsToImport) > nValue)
		{
			QMessageBox::information(NULL, "", tr("You will reach the limit to the number of allowed projects!\nTry selecting smaller number of projects!"));
			return;
		}
	}

	//get org id:
	int nOrgIDForced = -1;
	ui.frameOrganization->GetCurrentEntityRecord(nOrgIDForced);

	//convert BUSP_ORGANIZATION_CODE/BUSP_DEPARTMENT_CODE/BUSP_LEADER_CODE/BUSP_RESPONSIBLE_CODE
	//			 to BUSP_ORGANIZATION_ID/BUSP_DEPARTMENT_ID/...
	int nColDepCode = m_Dep.GetDataSource()->getColumnIdx("BDEPT_CODE");	Q_ASSERT(nColDepCode >= 0);
	int nColDepID   = m_Dep.GetDataSource()->getColumnIdx("BDEPT_ID");		Q_ASSERT(nColDepID >= 0);
	int nColOrgCode = m_Org.GetDataSource()->getColumnIdx("BORG_CODE");		Q_ASSERT(nColDepCode >= 0);
	int nColOrgID   = m_Org.GetDataSource()->getColumnIdx("BORG_ID");		Q_ASSERT(nColDepID >= 0);
	int nColPerCode = m_Per.GetDataSource()->getColumnIdx("BPER_CODE");		Q_ASSERT(nColDepCode >= 0);
	int nColPerID   = m_Per.GetDataSource()->getColumnIdx("BPER_ID");		Q_ASSERT(nColDepID >= 0);

	QString strCode;
	int nRow;
	int nStart = 0;
	int nHandled = 0;
	while(-1 != (nStart = m_lstData.getSelectedRow(nStart+1))){
		lstSelected.setData(nHandled, "BUSP_ACTIVE_FLAG", 1);	//mark as active

		//convert from department code to ID
		strCode = m_lstData.getDataRef(nStart, "BUSP_DEPARTMENT_CODE").toString();
		if(!strCode.isEmpty()){
			nRow = m_Dep.GetDataSource()->find(nColDepCode, strCode, true);
			if(nRow >= 0)
				lstSelected.setData(nHandled, "BUSP_DEPARTMENT_ID", m_Dep.GetDataSource()->getDataRef(nRow, nColDepID).toInt());
		}

		//convert from org code to ID
		strCode = m_lstData.getDataRef(nStart, "BUSP_ORGANIZATION_CODE").toString();
		if(!strCode.isEmpty()){
			nRow = m_Org.GetDataSource()->find(nColOrgCode, strCode, true);
			if(nRow >= 0)
				lstSelected.setData(nHandled, "BUSP_ORGANIZATION_ID", m_Org.GetDataSource()->getDataRef(nRow, nColOrgID).toInt());
		}
		else if(nOrgIDForced >= 0){
			//for empty code fields, set the global settings
			lstSelected.setData(nHandled, "BUSP_ORGANIZATION_ID", nOrgIDForced);
		}


		//convert from leader/responsible code to ID
		strCode = m_lstData.getDataRef(nStart, "BUSP_LEADER_CODE").toString();
		if(!strCode.isEmpty()){
			nRow = m_Per.GetDataSource()->find(nColPerCode, strCode, true);
			if(nRow >= 0)
				lstSelected.setData(nHandled, "BUSP_LEADER_ID", m_Per.GetDataSource()->getDataRef(nRow, nColPerID).toInt());
		}
		strCode = m_lstData.getDataRef(nStart, "BUSP_RESPONSIBLE_CODE").toString();
		if(!strCode.isEmpty()){
			nRow = m_Per.GetDataSource()->find(nColPerCode, strCode, true);
			if(nRow >= 0)
				lstSelected.setData(nHandled, "BUSP_RESPONSIBLE_ID", m_Per.GetDataSource()->getDataRef(nRow, nColPerID).toInt());
		}

		nHandled ++;
	}


	//
	// import process
	//
	int nSize = lstSelected.getRowCount();

	QSize size(300,100);
	QProgressDialog progress(tr("Importing Projects..."),tr("Cancel"),0,nSize+30,this);
	progress.setWindowTitle(tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	nSize = lstSelected.getRowCount();	//fix size (some rows were merged)
	progress.setMinimumSize(size);

	//update progress
	progress.setValue(10);
	qApp->processEvents();
	if (progress.wasCanceled())
		return;

	Status status;
	DbRecordSet lstWork;
	lstWork.copyDefinition(lstSelected);

	const int nChunkSize = 50;

	int nFailuresCnt = 0;
		nStart = -1;
	int nRowProgress = 0;

	int nCount = 0;
	while (1)
	{
		//prepare chunk
		lstWork.destroy();
		lstWork.copyDefinition(lstSelected);

		int nMax = min(nCount+nChunkSize, nSize);
		for(int i=nCount; i<nMax; i++){
			if(!lstWork.addRow()) break;
			DbRecordSet row = lstSelected.getRow(i);
			lstWork.assignRow(lstWork.getRowCount()-1, row);
		}
		if(lstWork.getRowCount() < 1)
			break;

		_SERVER_CALL(BusImport->ImportProjects(status, lstWork, nOperation))
		if(!status.IsOK()){
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}

		//lstWork.Dump();

		//merge status fields back to original list (only selected items were imported) and refresh grid display
		int nHandled = 0;
		while(-1 != (nStart = m_lstData.getSelectedRow(nStart+1))) 
		{
			//update main list
			//if(lstWork.getDataRef(nHandled, "STATUS_CODE").toInt() > 0)
			//	nFailuresCnt++;

			//update ID and other needed fields
			lstSelected.setData(nRowProgress, "BUSP_ID", lstWork.getDataRef(nHandled, "BUSP_ID").toInt());
			lstSelected.setData(nRowProgress, "BUSP_LEVEL", lstWork.getDataRef(nHandled, "BUSP_LEVEL").toInt());
			lstSelected.setData(nRowProgress, "BUSP_PARENT", lstWork.getDataRef(nHandled, "BUSP_PARENT").toInt());
			lstSelected.setData(nRowProgress, "BUSP_HASCHILDREN", lstWork.getDataRef(nHandled, "BUSP_HASCHILDREN").toInt());

			nRowProgress ++;	//total progress
			nHandled ++;		//progress in this chunk
			if(nHandled >= nChunkSize)
				break;
		}

		int nWorkSize = lstWork.getRowCount();
		//for(int i=0; i<nWorkSize; i++)
			//if(lstWork.getDataRef(i, "STATUS_CODE").toInt() > 0)
		//		nFailuresCnt++;

		nCount = nMax;

		//update progress
		progress.setValue(nCount);
		qApp->processEvents();
		if (progress.wasCanceled())
			return;
	}

	ui.tableContacts->RefreshDisplay();

	progress.setValue(nCount+15);
	qApp->processEvents();

	//add new items into the cache
	DbRecordSet lstActual;
	lstActual.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
	lstActual.merge(lstSelected);
	//lstActual.Dump();


	//B.T. 2013: purge all and force project reload on all controllers:
	g_ClientCache.PurgeCache(ENTITY_BUS_PROJECT);
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_PROJECT); 

}
