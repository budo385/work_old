#ifndef QCW_SMALLWIDGET_H
#define QCW_SMALLWIDGET_H

#include <QWidget>
#include "ui_qcw_smallwidget.h"
#include "qcw_base.h"

class QCW_SmallWidget : public QCW_Base
{
	Q_OBJECT

public:
	QCW_SmallWidget(QWidget *parent = 0);
	~QCW_SmallWidget();

	bool Initialize(DbRecordSet recContactData);
	bool Initialize(int nEntityRecordID);
	bool Initialize(QString strDropText);
	void SetDefaultValues(DbRecordSet &recContactData, QString strLeftSelectorText);

	QString GetSelectedText();

private:
	Ui::QCW_SmallWidgetClass ui;

	//Set values to widgets. Called from Initialize(...).
	void SetupFrames(bool bCheckNames = false);

	void SetDataToRecordSet(QString strRecordSetFieldName, QString strValue);
	void SetupContactWidgets(DbRecordSet *recFullContactData, bool bCheckNames = false);

private slots:
	//Only one record must be in this recordset.
	void on_leftSelector_SelectionChanged(int nSelectedRow, DbRecordSet recContactData);
	void on_leftSelector_NewDataDropped(int nDropType, DbRecordSet lstDroppedData);
	void on_leftSelector_SetNames(QString strFirstName, QString strLastName, QString strMiddleName);
	void on_leftSelector_SetOrganization(QString strOrganization);

	void on_contact_frameEditor_textChanged();
	void on_organization_lineEdit_textChanged(const QString & text);
	void on_organization_editingFinished();
	void on_first_name_lineEdit_textChanged(const QString & text);
	void on_middle_name_lineEdit_textChanged(const QString & text);
	void on_last_name_lineEdit_textChanged(const QString & text);
	void on_location_lineEdit_textChanged(const QString & text);
	void on_male_radioButton_clicked(bool checked = false);
	void on_female_radioButton_clicked(bool checked = false);
	void on_unknown_radioButton_clicked(bool checked = false);
	void on_btnAddressOrg2Pers_clicked();

	//Add selected part.
	void on_addSelected_toolButton_4_clicked();
	void on_addSelected_toolButton_5_clicked();
	void on_addSelected_toolButton_6_clicked();
	void on_addSelected_toolButton_7_clicked();

	//Save - Cancel.
	void on_ok_pushButton_clicked();
	void on_cancel_pushButton_clicked();

	//Clear all.
	void on_ClearAll_clicked();
	void UpdateSalutationOnAdressFrame(QString strRecordSetFieldName, QString strValue);
	void OnTownChanged(QString);
};

#endif // QCW_SMALLWIDGET_H
