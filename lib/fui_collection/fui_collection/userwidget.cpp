#include "userwidget.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
//#include "bus_client/bus_client/servermessagehandlerset.h"
//#include "trans/trans/httpclientconnectionsettings.h"
extern BusinessServiceManager *g_pBoSet;

#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:

/*!
User access right control widget.

\param parent			- parent.
*/
UserWidget::UserWidget(QWidget *parent)
    : FuiBase(parent)
{
	ui.setupUi(this);
	InitFui();

	//Define m_recInsertedRoleToUser.
	//Organize recordset.
	m_recInsertedRoleToUser.addColumn(QVariant::Int, "RowID");
	m_recInsertedRoleToUser.addColumn(QVariant::Int, "PersonID");
	m_recInsertedRoleToUser.addColumn(QVariant::Int, "RoleID");

	ui.User_treeWidget->setAcceptDrops(true);
	ui.Role_treeWidget->setDragEnabled(true);
	ui.Role_treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

	Initialize();
	InitContextMenu();	

	connect(ui.Role_treeWidget, SIGNAL(itemSelectionChanged()),  this, SLOT(RoleSelectionChanged()));
	connect(ui.User_treeWidget, SIGNAL(itemSelectionChanged()),  this, SLOT(UserSelectionChanged()));
	connect(ui.User_treeWidget, SIGNAL(ARSDropped(QTreeWidgetItem *, int)),  this, SLOT(on_RoleDropped(QTreeWidgetItem *, int)));
}

UserWidget::~UserWidget()
{
}

void UserWidget::Initialize()
{
	InitializeButtons();
	InitializeRecordSets();
	InitializeUserTree();
	InitializeRoleList();
}

void UserWidget::InitializeUserTree()
{
	//Fist get all users.
	int nUserCount = m_recCORE_USER.getRowCount();
	for (int i = 0; i < nUserCount; ++i)
	{
		QString UserName		= m_recCORE_USER.getDataRef(i, 3).toString();
		QString UserLastName	= m_recCORE_USER.getDataRef(i, 4).toString();
		int		UserRowID		= m_recCORE_USER.getDataRef(i, 0).toInt();

		QTreeWidgetItem *UserItem = new QTreeWidgetItem();
		QString DisplayData = UserName + " " + UserLastName;
		//Set display data.
		UserItem->setData(0, Qt::UserRole,		UserRowID);
		UserItem->setData(0, Qt::DisplayRole,	DisplayData);
		UserItem->setIcon(0, QIcon(":omnis_1797.png"));

		m_hshUserIDToItem.insert(UserRowID, UserItem);
	}

	//Then get users roles.
	int nPersRoleCount = m_recCORE_USERROLE.getRowCount();
	for (int i = 0; i < nPersRoleCount; ++i)
	{
		int nID		= m_recCORE_USERROLE.getDataRef(i, 0).toInt();
		int nPersID = m_recCORE_USERROLE.getDataRef(i, 3).toInt();
		int nRoleID = m_recCORE_USERROLE.getDataRef(i, 4).toInt();

		int nRoleRow = m_recCORE_ROLE.find(0, nRoleID, true);
		if (nRoleRow < 0)
			continue;
		
		QString RoleName = m_recCORE_ROLE.getDataRef(nRoleRow, 3).toString();
		QString RoleDesc = m_recCORE_ROLE.getDataRef(nRoleRow, 4).toString();
		int		RoleType = m_recCORE_ROLE.getDataRef(nRoleRow, 5).toInt();

		QTreeWidgetItem *RoleItem = new QTreeWidgetItem();
		QString DisplayData = RoleName; //RoleName + " " + RoleDesc;
		//Set display data.
		RoleItem->setData(0, Qt::UserRole,		nID);
		RoleItem->setData(1, Qt::UserRole,		nRoleID);
		RoleItem->setData(2, Qt::UserRole,		RoleType);
		RoleItem->setData(3, Qt::UserRole,		RoleDesc);
		RoleItem->setData(0, Qt::DisplayRole,	DisplayData);

		//If system role.
		if (RoleType)
			RoleItem->setIcon(0, QIcon(":MainDataCyBl.jpg"));
		else
			RoleItem->setIcon(0, QIcon(":OptionsCyBl.jpg"));

		m_hshUserIDToItem.value(nPersID)->addChild(RoleItem);
	}

	ui.User_treeWidget->addTopLevelItems(m_hshUserIDToItem.values());
}

void UserWidget::InitializeRoleList()
{
	int nRoleCount = m_recCORE_ROLE.getRowCount();
	for (int i = 0; i < nRoleCount; ++i)
	{
		int		nRoleID	 = m_recCORE_ROLE.getDataRef(i, 0).toInt();
		QString RoleName = m_recCORE_ROLE.getDataRef(i, 3).toString();
		QString RoleDesc = m_recCORE_ROLE.getDataRef(i, 4).toString();
		int		RoleType = m_recCORE_ROLE.getDataRef(i, 5).toInt();

		QTreeWidgetItem *RoleItem = new QTreeWidgetItem();
		QString DisplayData = RoleName;
		//Set display data.
		RoleItem->setData(0, Qt::UserRole,		nRoleID);
		RoleItem->setData(1, Qt::UserRole,		RoleType);
		RoleItem->setData(2, Qt::UserRole,		RoleDesc);
		RoleItem->setData(0, Qt::DisplayRole,	DisplayData);
		//If system role.
		if (RoleType)
			RoleItem->setIcon(0, QIcon(":MainDataCyBl.jpg"));
		else
			RoleItem->setIcon(0, QIcon(":OptionsCyBl.jpg"));

		ui.Role_treeWidget->addTopLevelItem(RoleItem);
	}
}

void UserWidget::InitializeButtons()
{
	ui.AssignPushButton	->setDisabled(true);
	ui.SavePushButton	->setDisabled(true);
	ui.CancelPushButton	->setDisabled(true);
	ui.DeletePushButton	->setDisabled(true);
}

void UserWidget::InitializeRecordSets()
{
	//Get Users.
	_SERVER_CALL(ClientSimpleORM->Read(m_Status, CORE_USER, m_recCORE_USER))
	if(!m_Status.IsOK())
	{
		qDebug() << m_Status.getErrorText();
		return;
	}
	else
		qDebug() << "Users retrieved.";

	//Get Users.
	_SERVER_CALL(ClientSimpleORM->Read(m_Status, CORE_USERROLE, m_recCORE_USERROLE))
	if(!m_Status.IsOK())
	{
		qDebug() << m_Status.getErrorText();
		return;
	}
	else
		qDebug() << "Users roles retrieved.";

	//Get Roles.
	QString strWhere = "WHERE CROL_ROLE_TYPE < 2";
	_SERVER_CALL(ClientSimpleORM->Read(m_Status, CORE_ROLE, m_recCORE_ROLE, strWhere))
	if(!m_Status.IsOK())
	{
		qDebug() << m_Status.getErrorText();
		return;
	}
	else
		qDebug() << "Roles retrieved.";

	//m_recCORE_USER.Dump();
	//m_recCORE_USERROLE.Dump();
	//m_recCORE_ROLE.Dump();
}

void UserWidget::InsertRoleToUser(int nUserID, int nRoleID)
{
	//Lock role.
	if (!LockResource(nUserID))
		return;

	//Check does this role already contains this ARS.
	if (CheckIsRoleInUser(nUserID, nRoleID))
		return;

	//First let's get it some fake ID (it will be negative number).
	int	nInsRoleRowID = -1;
	//Then check fake ID.
	while (m_recInsertedRoleToUser.find(0, nInsRoleRowID, true) >= 0)
		nInsRoleRowID--;

	//Insert it to recordset.
	m_recInsertedRoleToUser.addRow();
	int row = m_recInsertedRoleToUser.getRowCount() - 1;
	m_recInsertedRoleToUser.setData(row, 0, nInsRoleRowID);
	m_recInsertedRoleToUser.setData(row, 1, nUserID);
	m_recInsertedRoleToUser.setData(row, 2, nRoleID);

	//Get recordset data and create item.
	int nRoleRow = m_recCORE_ROLE.find(0, nRoleID, true);

	QString RoleName = m_recCORE_ROLE.getDataRef(nRoleRow, 3).toString();
	QString RoleDesc = m_recCORE_ROLE.getDataRef(nRoleRow, 4).toString();
	int		RoleType = m_recCORE_ROLE.getDataRef(nRoleRow, 5).toInt();

	QTreeWidgetItem *RoleItem = new QTreeWidgetItem();
	QString DisplayData = RoleName;
	//Set display data.
	RoleItem->setData(0, Qt::UserRole,		nInsRoleRowID);
	RoleItem->setData(1, Qt::UserRole,		nRoleID);
	RoleItem->setData(2, Qt::UserRole,		RoleType);
	RoleItem->setData(3, Qt::UserRole,		RoleDesc);
	RoleItem->setData(0, Qt::DisplayRole,	DisplayData);
	//If system role.
	if (RoleType)
		RoleItem->setIcon(0, QIcon(":MainDataCyBl.jpg"));
	else
		RoleItem->setIcon(0, QIcon(":OptionsCyBl.jpg"));

	//Add it to user.
	m_hshUserIDToItem.value(nUserID)->addChild(RoleItem);
	m_hshUserIDToItem.value(nUserID)->setExpanded(true);
	ui.User_treeWidget->clearSelection();
	RoleItem->setSelected(true);

	//Change save state.
	ChangeSaveState();
}

void UserWidget::ChangeSaveState()
{
	if (!m_hshDeletedRoleFromUser.isEmpty() ||
		m_recInsertedRoleToUser.getRowCount() > 0)
	{
		ui.SavePushButton->setEnabled(true);
		ui.CancelPushButton->setEnabled(true);
		SetMode(FuiBase::MODE_EDIT);
	}
	else
	{
		ui.SavePushButton->setEnabled(false);
		ui.CancelPushButton->setEnabled(false);
		SetMode(FuiBase::MODE_READ);
	}
}

bool UserWidget::LockResource(int UserID)
{
	//Check is this user already locked.
	if (m_hshLockingResourceHash.contains(UserID))
		return true;

	//If nothing from above lock.
	QString LockResourceID;
	//Create locking recordset.
	int row = m_recCORE_USER.find(0, UserID, true);
	DbRecordSet lockRecSet;
	lockRecSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USER));
	lockRecSet.addRow();
	lockRecSet.setData(0, 0, UserID);

	DbRecordSet lstStatusRows;
	_SERVER_CALL(ClientSimpleORM->Lock(m_Status, CORE_USER, lockRecSet, LockResourceID, lstStatusRows))
	if (!m_Status.IsOK())
	{
		QMessageBox::critical(this, "Locking error!", m_Status.getErrorText());
		return false;
	}

	m_hshLockingResourceHash.insert(UserID, LockResourceID);
	return true;
}

void UserWidget::UnLockResources()
{
	foreach(int UserID, m_hshLockingResourceHash.keys())
	{
		QString lockRes = m_hshLockingResourceHash.value(UserID);
		bool bUnlocked;
		_SERVER_CALL(ClientSimpleORM->UnLock(m_Status, CORE_USER, bUnlocked, lockRes))
		Q_ASSERT(bUnlocked);
	}

	m_hshLockingResourceHash.clear();
}

void UserWidget::DeleteRoleFromUser(QTreeWidgetItem *Item, QTreeWidgetItem *ParentItem)
{
	Q_ASSERT(Item);
	Q_ASSERT(ParentItem);
	int nUserID		= ParentItem->data(0, Qt::UserRole).toInt();
	int nRoleToUserID	= Item->data(0, Qt::UserRole).toInt();
	int nRoleID;

	//Lock role.
	if (!LockResource(nUserID))
		return;

	//If inserted get access right set id (for later to delete).
	if (nRoleToUserID < 0)
	{
		int r	= m_recInsertedRoleToUser.find(0, nRoleToUserID, true);
		//Delete it from insert recordset.
		m_recInsertedRoleToUser.clearSelection();
		m_recInsertedRoleToUser.selectRow(r);
		m_recInsertedRoleToUser.deleteSelectedRows();
	}
	else //If not inserted access rights, then it is from before.
	{
		int r = m_recCORE_USERROLE.find(0, nRoleToUserID, true);
		nRoleID = m_recCORE_USERROLE.getDataRef(r, 4).toInt();
		//Delete it from m_recCORE_USERROLE.
		m_recCORE_USERROLE.clearSelection();
		m_recCORE_USERROLE.selectRow(r);
		m_recCORE_USERROLE.deleteSelectedRows();
		//Insert user id to role id combination to delete.
		m_hshDeletedRoleFromUser.insert(nUserID, nRoleID);
	}

	//Take it from tree and delete it.
	ParentItem->takeChild(ParentItem->indexOfChild(Item));
	delete(Item);

	//Check save button.
	ChangeSaveState();
}

bool UserWidget::CheckIsRoleInUser(int nUserID, int nRoleID)
{
	//Check is it already inserted before.
	m_recCORE_USERROLE.find(3, nUserID);
	m_recCORE_USERROLE.find(4, nRoleID, false, true, true);
	if (m_recCORE_USERROLE.getSelectedCount() > 0)
		return true;

	//Check is it inserted recently.
	m_recInsertedRoleToUser.find(1, nUserID);
	m_recInsertedRoleToUser.find(2, nRoleID, false, true, true);
	if (m_recInsertedRoleToUser.getSelectedCount() > 0)
		return true;

	return false;
}

void UserWidget::ReloadData()
{
	m_recCORE_ROLE.clear();
	m_recCORE_USER.clear();
	m_recCORE_USERROLE.clear();
	m_hshUserIDToItem.clear();
	m_recInsertedRoleToUser.clear();
	m_hshDeletedRoleFromUser.clear();
	m_hshLockingResourceHash.clear();

	ui.User_treeWidget->clear();
	ui.Role_treeWidget->clear();

	Initialize();

}

void UserWidget::InitContextMenu()
{
	QAction *ActionDelete = new QAction(QIcon(":trash-16.png"), "Delete", this);
	connect(ActionDelete, SIGNAL(triggered()), this, SLOT(on_DeletePushButton_clicked()));
	m_lstRoleActions << ActionDelete;
	ui.User_treeWidget->SetChildContextMenuActions(m_lstRoleActions);
}

void UserWidget::on_AssignPushButton_clicked()
{
	//Create selected roles recordset.
	DbRecordSet SelectedUsers;

	//Call dialog to get roles.
	MultiAssignDialog *UserAssign = new MultiAssignDialog(&m_recCORE_USER);
	UserAssign->SetTitle(tr("Assign roles to multiple Login Accounts"));
	if (UserAssign->exec())
		SelectedUsers = UserAssign->GetSelectedItems();

	//Get list of items and put them in a list.
	QList<QTreeWidgetItem*> selectedUserItems;
	int selectedCount = SelectedUsers.getRowCount();
	for (int i = 0; i < selectedCount; ++i)
	{
		int UserID = SelectedUsers.getDataRef(i, 0).toInt();
		selectedUserItems << m_hshUserIDToItem.value(UserID);
	}

	foreach(QTreeWidgetItem *item, selectedUserItems)
		on_RoleDropped(item, 0);

	//Delete dialog.
	delete(UserAssign);
	return;
}

void UserWidget::on_SavePushButton_clicked()
{
	int nInsertCount = m_recInsertedRoleToUser.getRowCount();
	for (int i = 0; i < nInsertCount; ++i)
	{
		int nUserID = m_recInsertedRoleToUser.getDataRef(i, 1).toInt();
		int nRoleID   = m_recInsertedRoleToUser.getDataRef(i, 2).toInt();
		_SERVER_CALL(AccessRights->InsertNewRoleToUser(m_Status, nUserID, nRoleID))
		if(!m_Status.IsOK())
		{
			qDebug() << m_Status.getErrorText();
			return;
		}
		else
			qDebug() << "Role inserted.";
	}

	foreach(int nUserID, m_hshDeletedRoleFromUser.uniqueKeys())
	{
		foreach(int nRoleID, m_hshDeletedRoleFromUser.values(nUserID))
		{
			_SERVER_CALL(AccessRights->DeleteRoleFromUser(m_Status, nUserID, nRoleID))
			if(!m_Status.IsOK())
			{
				qDebug() << m_Status.getErrorText();
				return;
			}
			else
				qDebug() << "Role deleted.";
		}
	}

	UnLockResources();
	ReloadData();
	ChangeSaveState();
}

void UserWidget::on_CancelPushButton_clicked()
{
	if (QMessageBox::warning(this, "Cancel pressed!", "Do you want to discard changes!", QMessageBox::Ok, QMessageBox::Cancel) == QMessageBox::Cancel)
		return;

	UnLockResources();
	ReloadData();
	SetMode(FuiBase::MODE_READ);
	//g_objFuiManager.CloseCurrentFuiTab();
}

void UserWidget::on_DeletePushButton_clicked()
{
	//Get selected items list.
	QList<QTreeWidgetItem*> selectedItemsList = ui.User_treeWidget->selectedItems();

	if (!selectedItemsList.count())
		return;

	QTreeWidgetItem *Item, *ParentItem;
	Item = selectedItemsList.first();
	ParentItem = Item->parent();

	DeleteRoleFromUser(Item, ParentItem);
}

void UserWidget::RoleSelectionChanged()
{
	//Get selected items list.
	QList<QTreeWidgetItem*> selectedItemsList = ui.Role_treeWidget->selectedItems();

	if (!selectedItemsList.isEmpty())
	{
		ui.ARSDescr_textBrowser->clear();
		ui.ARSDescr_textBrowser->insertPlainText(selectedItemsList.first()->data(2, Qt::UserRole).toString());
		ui.AssignPushButton->setDisabled(false);
	}
	else
		ui.AssignPushButton->setDisabled(true);
}

void UserWidget::UserSelectionChanged()
{
	//Get selected items list.
	QList<QTreeWidgetItem*> selectedItemsList = ui.User_treeWidget->selectedItems();

	if (selectedItemsList.count() != 0)
	{
		QTreeWidgetItem *item = selectedItemsList.first();
		if (!item->parent())
			ui.DeletePushButton->setDisabled(true);
		else
			ui.DeletePushButton->setDisabled(false);

		return;
	}

	ui.DeletePushButton->setDisabled(true);
}

void UserWidget::on_RoleDropped(QTreeWidgetItem *Parent, int index)
{
	QList<int> SelectedRoleRowID = ui.Role_treeWidget->GetSelectedItems();

	int nUserID = Parent->data(0, Qt::UserRole).toInt();

	//Insert dropped roles.
	foreach(int RoleID, SelectedRoleRowID)
		InsertRoleToUser(nUserID, RoleID);
}
