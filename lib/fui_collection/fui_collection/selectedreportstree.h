#ifndef SELECTEDREPORTSTREE_H
#define SELECTEDREPORTSTREE_H

#include "gui_core/gui_core/universaltreewidget.h"

// Class just for report selection wizard page - allows only one report to be dropped in it.

class SelectedReportsTree : public UniversalTreeWidget
{
	Q_OBJECT

public:
	SelectedReportsTree(QWidget *parent = 0);
	~SelectedReportsTree();

	virtual void DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);

private:
signals:
	void ItemNumberChanged();
};

#endif // SELECTEDREPORTSTREE_H
