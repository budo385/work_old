#ifndef CLIENTSETUPWIZPAGE_FIRSTSTEP_H
#define CLIENTSETUPWIZPAGE_FIRSTSTEP_H

#include "ui_clientsetupwizpage_firststep.h"
#include "gui_core/gui_core/wizardpage.h"

class ClientSetupWizPage_FirstStep : public WizardPage
{
	Q_OBJECT

public:
	ClientSetupWizPage_FirstStep(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
	~ClientSetupWizPage_FirstStep();

	void Initialize();
	bool GetPageResult(DbRecordSet &RecordSet){return true;};
	void on_NextButtonClicked();

private:
	Ui::ClientSetupWizPage_FirstStepClass ui;
	bool m_bWebHelpOpen;
};

#endif // CLIENTSETUPWIZPAGE_FIRSTSTEP_H
