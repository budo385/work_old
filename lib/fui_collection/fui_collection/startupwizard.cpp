#include "startupwizard.h"
#include "wizardregistration.h"
#include "common/common/dbrecordset.h"
#include <QtWidgets/QMessageBox>
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager* g_pBoSet;
#include "bus_client/bus_client/clientbackupmanager.h"
#include "importcenter.h"
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

StartupWizard::StartupWizard()
{

}

StartupWizard::~StartupWizard()
{

}


//wizard:
//returns true: if client needs relogin:
bool StartupWizard::StartWizard()
{
	QList<DbRecordSet> RecordSetList;

	WizardRegistration wizReg(WizardRegistration::STARTUP_WIZARD);
	WizardBase wiz(WizardRegistration::STARTUP_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);
	wiz.SetRetryMode();

	//If wizard accepted.
	if (wiz.exec())
	{
		wiz.GetWizPageRecordSetList(RecordSetList);

	
		//write data on server:
		Status err;
		_SERVER_CALL(BusContact->CreateDefaultPerson(err,RecordSetList.at(1).getDataRef(0,0).toInt()))
		_CHK_ERR_RET_BOOL_ON_FAIL(err);

		//enable wizard start:
		g_pClientManager->GetIniFile()->m_nStartImportWizard=1;
		
		//if password change:
		if (RecordSetList.at(2).getDataRef(0,0).toBool())
		{
			QMessageBox::information(NULL,QObject::tr("Re-login"),QObject::tr("Password successfully changed, please re-login"));
			return true;
		}
		else
			return false;
	}

	return false;
}


void StartupWizard::ResetOnStartUpFlag()
{
	Status err;
	QString strTemplateName;
	int nAskForTemplate,nStartStartUpWizard;
	ClientBackupManager *pBackupManager=static_cast<ClientBackupManager*>(g_pClientManager->GetBackupManager());
	if (pBackupManager)
	{
		_SERVER_CALL(ServerControl->GetTemplateData(err,strTemplateName,nAskForTemplate,nStartStartUpWizard))
		_CHK_ERR(err);
		_SERVER_CALL(ServerControl->SetTemplateData(err,strTemplateName,0,0))
	}

}



void StartupWizard::StartImportWizard()
{
	//pop.-up import wizard if not pop.uped alredy:
	if (g_pClientManager->GetIniFile()->m_nStartImportWizard==1)
	{
		QDialog *pDialog= new ImportCenter;
		pDialog->setModal(false);
		pDialog->show();
		g_pClientManager->GetIniFile()->m_nStartImportWizard=0;
	}
}