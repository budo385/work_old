#ifndef FUI_IMPORTUserContactRel_H
#define FUI_IMPORTUserContactRel_H

#include <QWidget>
#include "generatedfiles/ui_fui_importUserContactRel.h"
#include "fuibase.h"

class FUI_ImportUserContactRel : public FuiBase
{
    Q_OBJECT

public:
    FUI_ImportUserContactRel(QWidget *parent = 0);
    ~FUI_ImportUserContactRel();

private:
    Ui::FUI_ImportUserContactRelClass ui;
	QString GetFUIName(){return tr("Import User-Contact Relationships");};
	void UpdateImportContactsButton();

	DbRecordSet m_lstData;

private slots:
	void on_btnImportContacts_clicked();
	void on_btnBuildList_clicked();
};

#endif // FUI_IMPORTUserContactRel_H
