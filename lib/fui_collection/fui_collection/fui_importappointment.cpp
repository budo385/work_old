#include "os_specific/os_specific/mapimanager.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "fui_importappointment.h"
#include "gui_core/gui_core/gui_helper.h"
#include <QInputDialog>
#include <QtWidgets/QMessageBox>
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/datahelper.h"
#include "bus_core/bus_core/globalconstants.h"
#include "communicationmanager.h"
#include "qcw_base.h"
#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#include "os_specific/os_specific/mailmanager.h"
#include "emaildialog.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/accuarcore.h"
#include "fui_collection/fui_collection/calendarhelper.h"

//GLOBALS:
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#ifdef _WIN32
 //#include <windows.h>
 //#include "os_specific/os_specific/mapi/mapiex.h"
#endif

#ifndef min
#define min(a,b) (((a)<(b))? (a):(b))
#endif

#define MAX_ATTACHMENT_LIST_SIZE 16777216

QMap<QString, int> FUI_ImportAppointment::m_mapAppointment2Contact;
QMutex g_mutexAppointment2Cont;

typedef bool (*FN_FILTER_MATCH_ROW)(unsigned long nCallerObject,int nRow);
bool Appointment_FilterMatchRow(unsigned long nCallerObject,int nRow)
{
	FUI_ImportAppointment *pDlg = (FUI_ImportAppointment *)nCallerObject;
	Q_ASSERT(NULL != pDlg);
	if(pDlg)
	{
		return pDlg->FilterMatchRow(nRow);
	}
	return false;
}

FUI_ImportAppointment::FUI_ImportAppointment(QWidget *parent)
	: FuiBase(parent), m_lstFolders()
{
	ui.setupUi(this);
	InitFui();

	ui.label1->setStyleSheet("* {font-weight:800; font-family:MS Shell Dlg 2; font-size:12pt}");

	//define list:
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
	m_lstDataCalEventPart.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));
	m_lstDataCalEventPartTaskData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));

	ui.tableData->Initialize(&m_lstData, true);
	ui.tableData->SetEditMode(true);
	connect(ui.tableData,SIGNAL(SignalSelectionChanged()),this,SLOT(OnTableDataSelectionChanged()));
	//ui.tableData->m_pParent = this;
	
	ui.frameProject->Initialize(ENTITY_BUS_PROJECT);
	ui.frameProject->registerObserver(this);

//	ui.frameContact->Initialize(ENTITY_BUS_CONTACT);
//	ui.frameContact->EnableInsertButton();
//	ui.frameContact->registerObserver(this);
//	connect(ui.tableData, SIGNAL(SignalRefreshBoldRows()), this, SLOT(OnSignalRefreshBoldRows()));


	//define column headers
	DbRecordSet columns;
	ui.tableData->AddColumnToSetup(columns,"BCEV_FROM",tr("From Date"),100,false, "");
	ui.tableData->AddColumnToSetup(columns,"BCEV_TO",tr("To Date"),100,false, "");
	ui.tableData->AddColumnToSetup(columns,"BCEV_TITLE",tr("Title"),180,false, "");
	ui.tableData->AddColumnToSetup(columns,"BCEV_DESCRIPTION",tr("Description"),250,false, "");
	ui.tableData->SetColumnSetup(columns);

	GUI_Helper::SetStyledButtonColorBkg(ui.btnBuildList, GUI_Helper::BTN_COLOR_YELLOW);
	GUI_Helper::SetStyledButtonColorBkg(ui.btnImportData, GUI_Helper::BTN_COLOR_YELLOW);

	//init default date range
	QDateTime now = QDateTime::currentDateTime();
	QDateTime yearAgo = now.addYears(-1);
	ui.txtFilterFromDate->setDateTime(yearAgo);
	ui.txtFilterToDate->setDateTime(now);

	//load outlook folder selection setting
	QByteArray  arData = g_pSettings->GetPersonSetting(IMPORT_EMAIL_OUTLOOK_FOLDERS).toByteArray();
	OutlookFolderPickerDlg::Binary2FolderList(arData, m_lstFolders);
	int nCnt = m_lstFolders.count();
	if(0 == nCnt)
	{
		//fill default folders
		QStringList lstSub;
		lstSub.append("Personal Folders");
		lstSub.append("Calendar");
		m_lstFolders.append(lstSub);
	}
}

FUI_ImportAppointment::~FUI_ImportAppointment()
{
	m_mapAppointment2Contact.clear(); //FIX: this fixes problem of stale data in the cache
}

void FUI_ImportAppointment::on_btnSelectSourceFolders_clicked()
{
	OutlookFolderPickerDlg dlg(false);
	dlg.SetSelection(m_lstFolders);
	if(dlg.exec())
	{
		QByteArray arData;
		m_lstFolders = dlg.GetSelection();
		if(dlg.StoreSettings(arData))
			g_pSettings->SetPersonSetting(IMPORT_EMAIL_OUTLOOK_FOLDERS, arData);
	}
}


void FUI_ImportAppointment::OnTableDataSelectionChanged()
{
	//TOFIX set project code for multiple selected table data rows ? (or empty if all rows do not have same project)
	//ui.frameProject->SetCurrentRow(m_lstData.getSelectedRow());
	//ui.frameProject->RefreshDisplay();
}

bool FUI_ImportAppointment::FilterMatchRow(int nIdx)
{
	//match event start date
	QDateTime dtSentRecv = m_lstData.getDataRef(nIdx, "BCEV_FROM").toDateTime();
	if(dtSentRecv.isValid()){
		if(m_dateFilterFromDate.isValid())
			if(dtSentRecv.date() < m_dateFilterFromDate)
				return false;	//no match
		if(m_dateFilterToDate.isValid())
			if(dtSentRecv.date() > m_dateFilterToDate)
				return false;	//no match
	}
	return true;	// match, survived all tests
}

void FUI_ImportAppointment::on_btnBuildList_clicked()
{
	//refresh filter
	m_dateFilterFromDate = ui.txtFilterFromDate->date();
	m_dateFilterToDate	 = ui.txtFilterToDate->date();

#ifdef _WIN32
	int nAppointmentsImported;
	QSet<QString> setAppointments;
	MapiManager::ListAppointments(m_lstData,m_lstFolders,nAppointmentsImported,setAppointments,Appointment_FilterMatchRow,(unsigned long)this, MapiManager::InitGlobalMapi());
	m_lstData.selectAll();
	OnGridFilledRecalculate(setAppointments);
	ui.tableData->RefreshDisplay();
#endif
}

void FUI_ImportAppointment::ImportAppointments(Status status, DbRecordSet &lstData, int &nSkippedOrReplacedCnt, bool bAddDupsAsNewRow, bool bSkipExisting, bool bSkipProgress)
{
	qDebug() << "FUI_ImportAppointment::ImportAppointments";

#ifdef _WIN32
	//
	// import process
	//
	int nSize = lstData.getRowCount();
	if(nSize < 1) return;

	QProgressDialog *pDlg = NULL;
	if (!bSkipProgress)
	{
		QSize size(300,100);
		pDlg = new QProgressDialog(tr("Importing Appointments..."),tr("Cancel"),0,nSize+30,NULL);
		pDlg->setWindowTitle(tr("Operation In Progress"));
		pDlg->setMinimumSize(size);
		pDlg->setWindowModality(Qt::WindowModal);
		pDlg->setMinimumDuration(1200);//1.2s before pops up
	}

	DbRecordSet lstNotifyObserver;
	lstNotifyObserver.addColumn(QVariant::Int,"BEM_ID");

	const int nChunkSize = 30;
	int nCount = 0;
	int nFailuresCnt = 0;
	int nStart = -1;
	int nRowProgress = 0;

	qDebug() << "Import Appointment Start";

	CMAPIEx* pMapi = MapiManager::InitGlobalMapi();
	if(!pMapi || !pMapi->OpenRootFolder())
		return;

	while(nCount<nSize)
	{
		int nAttachmentChunkSize=0;

		DbRecordSet lstChunk; //BT: always redefine chunk list!!!
		lstChunk.copyDefinition(lstData);
		if (lstChunk.getColumnIdx("ATTACHMENTS")>=0)
			lstChunk.removeColumn(lstChunk.getColumnIdx("ATTACHMENTS")); //BT = remove this, coz it will go on server

		//BT: commented: other list is used.
		//DbRecordSet lstContactLinks;
		//lstContactLinks.addColumn(DbRecordSet::GetVariantType(), "CONTACT_LIST");

		int nMax = min(nCount+nChunkSize, nSize);
		for(int i=nCount; i<nMax; i++)
		{
			lstChunk.merge(lstData.getRow(i));

			//lstContactLinks.addRow();
			//DbRecordSet rec=lstData.getDataRef(i, "CONTACTS").value<DbRecordSet>();
			//rec.Dump();
			//lstContactLinks.setData(lstContactLinks.getRowCount()-1, 0, lstData.getDataRef(i, "CONTACTS").value<DbRecordSet>());
		}
		nCount += lstChunk.getRowCount();

		//update progress
		if(pDlg) 
		{
			pDlg->setValue(nCount);
			qApp->processEvents();
		}

		if (pDlg)
		{
			if (pDlg->wasCanceled()){
				QMessageBox::information(NULL, "Info", tr("Operation stopped by user request!"));
				return;
			}
		}

		//BT added: set owner to logged user:
		if (g_pClientManager->GetPersonID()!=0)
			lstChunk.setColValue(lstChunk.getColumnIdx("CENT_OWNER_ID"),g_pClientManager->GetPersonID());
		//BT added: set mail type:
		lstChunk.setColValue(lstChunk.getColumnIdx("CENT_SYSTEM_TYPE_ID"),GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);

//		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportAppointments - prepare to send data chunk %1 rows, %3 bytes (total %2), attachment list size: %4").arg(lstChunk.getRowCount()).arg(nSize).arg(lstChunk.getStoredByteSize()).arg(lstAttachments.getStoredByteSize()));

		QString strLockRes;
		DbRecordSet lstSchedule;
		//_SERVER_CALL(BusCalendar->WriteEvent(status,m_lstData,m_lstDataCalEventPart,m_lstDataCalEventPartTaskData,strLockRes));
		//_CHK_ERR_NO_MSG(status);

		//_SERVER_CALL(BusEmail->WriteMultiple(status, lstChunk, lstContactLinks, lstSchedule, lstAttachments, strLockRes, bSkipExisting, nSkippedOrReplacedCnt, bAddDupsAsNewRow))
		//_CHK_ERR_NO_MSG(status);


		//import appointment parts
		//test if this is an appointment reply
		//extract entry ID
		nMax = lstChunk.getRowCount();
		for(int j=0; j<nMax; j++)
		{
			SBinary entryID;
			entryID.lpb = (LPBYTE)lstChunk.getDataRef(j, "BEM_EXT_ID").toByteArray().constData();
			entryID.cb	= lstChunk.getDataRef(j, "BEM_EXT_ID").toByteArray().size();
			
			CMAPIAppointment appointment;
			if(appointment.Open(pMapi, entryID))
			{
				QString strMessageClass;
				appointment.GetMessageClass(strMessageClass);
				//qDebug() << "Message Class:" << strMessageClass;

				//PARSE event:
				DbRecordSet set;
				set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS));
				set.addRow();

				//TOFIX move mapi part here for more encapsulation -> bool MapiManager::ReadAppointment(SBinary &entryID, QString &strMessageClass, DbRecordSet &set);

				if(strMessageClass.startsWith("IPM.Schedule.Meeting.Resp.Pos"))
					set.setData(0, "BCIV_STATUS", GlobalConstants::STATUS_CAL_INVITE_CONFIRMED);
				else if(strMessageClass.startsWith("IPM.Schedule.Meeting.Resp.Neg"))
					set.setData(0, "BCIV_STATUS", GlobalConstants::STATUS_CAL_INVITE_REJECTED);

				//extract contacts for this Appointment
				DbRecordSet lstAllRecipients;//I need all recipients in list + organizer store in 
				lstAllRecipients.addColumn(QVariant::Int,"BNMR_TABLE_KEY_ID_2");

				DbRecordSet lstContactLinks;
				lstData.getData(j, "CONTACTS", lstContactLinks);

				//BT: zasto je nedefinirana?
				//lstContactLinks.Dump();

				if (lstContactLinks.getColumnCount()>0 && lstContactLinks.getRowCount()>0)
				{
					lstContactLinks.addColumn(QVariant::String,"BNRO_NAME");

					//get organizer and recipient email+id?
					if(strMessageClass.startsWith("IPM.Schedule.Meeting.Resp."))
					{
						int nRow=lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_FROM,true);
						if (nRow>=0)
						{
							set.setData(0,"RECIPIENT_CONTACT_ID",lstContactLinks.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
							lstContactLinks.setColValue("BNRO_NAME",tr("Participant"));
						}
						nRow=lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_TO,true);
						if (nRow>=0)
						{
							set.setData(0,"ORGANIZER_CONTACT_ID",lstContactLinks.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
							lstContactLinks.setData(nRow,"BNRO_NAME",tr("Organizer"));
						}
					}
					else
					{
						int nRow=lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_FROM,true);
						if (nRow>=0)
						{
							set.setData(0,"ORGANIZER_CONTACT_ID",lstContactLinks.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
							lstContactLinks.setData(nRow,"BNRO_NAME",tr("Organizer"));
						}
						nRow=lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_TO,true);
						if (nRow>=0)
						{
							set.setData(0,"RECIPIENT_CONTACT_ID",lstContactLinks.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
							lstContactLinks.find("BNMR_SYSTEM_ROLE",(int)GlobalConstants::CONTACT_LINK_ROLE_TO);
							lstContactLinks.setColValue("BNRO_NAME",tr("Participant"),true);
						}
					}
				}

				//lstContactLinks.Dump();

				set.setData(0,"CONTACT_ASSIGN_LIST",lstContactLinks); //??? who is organizer....??

				QString strID=CalendarHelper::ParseSubjectID(lstData.getDataRef(j, "BEM_SUBJECT").toString());
				set.setData(0, "BCIV_SUBJECT_ID", strID);
				set.setData(0, "RECIPIENT_MAIL", lstData.getDataRef(j, "BEM_FROM").toString());
				QString strBody=lstData.getDataRef(j, "BEM_BODY").toString();
				set.setData(0, "BCIV_ANSWER_TEXT", lstData.getDataRef(j, "BEM_BODY").toString());

				//qDebug()<<strBody;

				QString strData;
				appointment.GetSubject(strData);
				set.setData(0, "BCEV_TITLE", strData);
				set.setData(0, "BCOL_SUBJECT", strData);

				appointment.GetLocation(strData);
				set.setData(0, "BCOL_LOCATION", strData);
				appointment.GetVCalendarUID(strData);
				set.setData(0, "BCIV_OUID", strData);
				
				appointment.GetStartDate(strData);
				QDateTime dateTime=QDateTime::fromString(strData,"MM/dd/yyyy hh:mm:ss AP");
				set.setData(0, "BCOL_FROM", dateTime);
				//qDebug()<<dateTime.toString("dd.MM.yyyy hh:mm:ss");
				
				appointment.GetEndDate(strData);
				dateTime=QDateTime::fromString(strData,"MM/dd/yyyy hh:mm:ss AP");
				//qDebug()<<dateTime.toString("dd.MM.yyyy hh:mm:ss");
				set.setData(0, "BCOL_TO", dateTime);

				//CalendarHelper::ParseCelandar(strBody,set);
				//set.Dump();

				if(strMessageClass.startsWith("IPM.Schedule.Meeting.Resp."))
				{
					//NOTE: being here proves the entry is an appointment response message:
					//IPM.Schedule.Meeting.Resp.Neg
					//IPM.Schedule.Meeting.Resp.Pos
					//IPM.Schedule.Meeting.Resp.Tent
										
					Status status;
					_SERVER_CALL(BusCalendar->WriteInviteReplyStatus(status, set));
					//FIX: this skips the important code below _CHK_ERR_NO_MSG(status);
				}
				else if (strMessageClass.startsWith("IPM.Schedule.Meeting.Request"))
				{
					Status status;
					_SERVER_CALL(BusCalendar->WriteInviteReplyStatus(status, set,1));

				}
				else if (strMessageClass.startsWith("IPM.Schedule.Meeting.Canceled"))
				{
					Status status;
					_SERVER_CALL(BusCalendar->WriteInviteReplyStatus(status, set,2));
				}
			}
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportAppointments - chunk sent success"));

		if (lstChunk.getRowCount()>0)
			lstNotifyObserver.merge(lstChunk);
	}

	qDebug() << "Import Appointment End";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportAppointments - Import Appointment End"));

	//B.T.: notify CE grid about new Appointments in system:
	//------------------------------------------------------
	//Send signal for all to reload:
	//------------------------------------------------------
	if (!bSkipProgress && lstNotifyObserver.getRowCount()>0)
	{
		QVariant varData;
		qVariantSetValue(varData,lstNotifyObserver);
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportAppointments - Import Appointment Before Notify"));
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ENTITY_BUS_EMAILS,varData,NULL); 
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportAppointments - Import Appointment After Notify"));
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportAppointments - Import Appointment prepare for exit"));

	if(pDlg) pDlg->setValue(nCount+15);
	if(pDlg) pDlg->close();

#endif //#ifdef _WIN32

	qDebug() << "Import Appointment Exit";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportAppointments - Import Appointment Exit"));
}

void FUI_ImportAppointment::on_btnImportData_clicked()
{
	//extract selected rows
	DbRecordSet lstSelected;
	lstSelected.copyDefinition(m_lstData);
	lstSelected.merge(m_lstData, true);
	//lstSelected.Dump();
	if(lstSelected.getRowCount() < 1){
		QMessageBox::information(this, "", tr("No items have been selected for import!"));
		return;
	}

	int nSkippedOrReplacedCnt = 0;
	bool bSkipExisting = !ui.chkOverwrite->isChecked();
	Status status; 
	ImportAppointments(status, lstSelected, nSkippedOrReplacedCnt, bSkipExisting);
	_CHK_ERR(status);
	ui.tableData->RefreshDisplay();

	if(nSkippedOrReplacedCnt > 0)
	{
		QString strMsg;
		if(bSkipExisting)
			strMsg = QString(tr("%1 existing Appointments have been skipped!")).arg(nSkippedOrReplacedCnt);
		else
			strMsg = QString(tr("%1 existing Appointments have been replaced!")).arg(nSkippedOrReplacedCnt);
		QMessageBox::information(this, tr("Info"), strMsg);
	}

	g_objFuiManager.CloseFUI(g_objFuiManager.GetFuiID(this));
}

void FUI_ImportAppointment::on_btnAssignProject_clicked()
{
	int nCol = m_lstData.getColumnIdx("BEM_PROJECT_ID");
	int nNameCol = m_lstData.getColumnIdx("PROJECT_NAME");
	int nCodeCol = m_lstData.getColumnIdx("PROJECT_CODE");

	int nEntityRecordID = -1;
	DbRecordSet record;
	if(ui.frameProject->GetCurrentEntityRecord(nEntityRecordID, record)){
		m_lstData.setColValue(nCol, nEntityRecordID, true);	// only to selected items
		m_lstData.setColValue(nNameCol, record.getDataRef(0, "BUSP_NAME").toString(), true);	// only to selected items
		m_lstData.setColValue(nCodeCol, record.getDataRef(0, "BUSP_CODE").toString(), true);	// only to selected items
	}
	else{
		m_lstData.setColValue(nCol, QVariant(QVariant::Int), true);			// only to selected items
		m_lstData.setColValue(nNameCol, QVariant(QVariant::String), true);	// only to selected items
		m_lstData.setColValue(nCodeCol, QVariant(QVariant::String), true);	// only to selected items
	}
	//record.Dump();
	
	ui.tableData->RefreshDisplay();
}

void FUI_ImportAppointment::on_btnOpenAppointment_clicked()
{
	int nFocusedRow = ui.tableData->row(ui.tableData->currentItem());
	if(nFocusedRow>=0)
	{
		QString strApp = m_lstData.getDataRef(nFocusedRow, "BEM_EXT_APP").toString();
		if("Outlook" == strApp)
		{
#ifdef _WIN32
			//find and open this Appointment in outlook form using Entry ID
			QByteArray arData;
			m_lstData.getData(nFocusedRow, "BEM_EXT_ID", arData);

			if(arData.length() < 1){
				QMessageBox::information(this, tr("Error"), tr("Message does not have Entry ID!"));
				return;
			}
			MapiManager::OpenMailInOutlook(arData);
#endif
		}
	}
}

void FUI_ImportAppointment::OnContactChanged()
{
/*
	//permanently add the Appointment address to the contact
	DbRecordSet recContactAppointment;
	recContactAppointment.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_Appointment));
	recContactAppointment.addRow();
	recContactAppointment.setData(0, "BCME_CONTACT_ID", nContactID);
	recContactAppointment.setData(0, "BCME_ADDRESS", strAppointment);
	recContactAppointment.setData(0, "BCME_NAME", record.getDataRef(0, "BCNT_NAME").toString());
	recContactAppointment.setData(0, "BCME_IS_DEFAULT", 0);
	
	Status status;
	_SERVER_CALL(BusContact->AddContactAppointment(status, recContactAppointment))
	_CHK_ERR(status);
*/
}

void FUI_ImportAppointment::OnContactSAPNEInsert()
{
}

void FUI_ImportAppointment::SetDefaults(QString strDefaultContactAppointment)
{
	ui.txtFilterFromDate->setDate(QDate(1900,1,1));
	ui.txtFilterToDate->setDate(QDate::currentDate());
}


void FUI_ImportAppointment::OnGridFilledRecalculate(QSet<QString> &setAppointments, int nDefaultProjectID)
{
	//get contact ID for each Appointment not already in cache (if exists)
	DbRecordSet lstAppointments;
	lstAppointments.addColumn(QVariant::Int, "BCME_CONTACT_ID");
	lstAppointments.addColumn(QVariant::String, "BUS_CM_EMAIL");

	int nPosList = 0;
	QList<QString> lstUnknown = setAppointments.toList();
	int nCntUnk = lstUnknown.count();
	int i;
	for(i=0; i<nCntUnk; i++)
	{
		//check if this Appointment is already resolved in the cache
		QMap<QString, int>::iterator It = m_mapAppointment2Contact.find(lstUnknown[i]);
		if(It == m_mapAppointment2Contact.end())
		{
			lstAppointments.addRow();
			lstAppointments.setData(nPosList, 1, lstUnknown[i]);
			nPosList ++;
		}
	}

	Status status;
	_SERVER_CALL(BusCommunication->GetContactFromEmail(status, lstAppointments))
	_CHK_ERR(status);

	QList<QString> lstUnassigned;

	//refresh cache
	nCntUnk = lstAppointments.getRowCount();
	for(i=0; i<nCntUnk; i++)
	{
		if(!lstAppointments.getDataRef(i, 0).isNull()){
			m_mapAppointment2Contact[lstAppointments.getDataRef(i, 1).toString()] = lstAppointments.getDataRef(i, 0).toInt();
		}
		else
			lstUnassigned.append(lstAppointments.getDataRef(i, 1).toString());
	}

	ui.tableData->RefreshDisplay();

	//set template flag to 0
	int nTemplateCol = m_lstData.getColumnIdx("BEM_TEMPLATE_FLAG");
	m_lstData.setColValue(nTemplateCol, 0);

	//mark unread messages in bold
	//B.T. improved:
	int nCount = m_lstData.getRowCount();
	OnSignalRefreshBoldRows();

	QString strCode, strName;
	int nProjID;
	if(nDefaultProjectID >= 0)	// override project setting
	{
		MainEntitySelectionController cacheLoader;
		cacheLoader.Initialize(ENTITY_BUS_PROJECT);
		DbRecordSet record;
		cacheLoader.GetEntityRecord(nDefaultProjectID, record);
		if(record.getRowCount()>0)
		{
			strCode = record.getDataRef(0, "BUSP_CODE").toString();
			strName = record.getDataRef(0, "BUSP_NAME").toString();
			nProjID = record.getDataRef(0, "BUSP_ID").toInt();

			int nCol = m_lstData.getColumnIdx("BEM_PROJECT_ID");
			int nCodeCol = m_lstData.getColumnIdx("PROJECT_CODE");
			int nNameCol = m_lstData.getColumnIdx("PROJECT_NAME");
			m_lstData.setColValue(nCodeCol, strCode);
			m_lstData.setColValue(nNameCol, strName);
			m_lstData.setColValue(nCol, nProjID);
		}
	}
	else
	{
		for(i=0; i<nCount; i++)
		{
			//if there is a single contact (other than us) and it has single related project
			//attach the project directly to this Appointment
			DbRecordSet lstContactLinks = m_lstData.getDataRef(i, "CONTACTS").value<DbRecordSet>();
			int nCntContacts = lstContactLinks.getRowCount();
			if(nCntContacts > 0)
			{
				bool bOutgoing = (m_lstData.getDataRef(i, "BEM_OUTGOING").toInt() > 0);
				//lstContactLinks.Dump();
				int nCnt = 0;
				int nIdx = -1;
				for(int j=0; j<nCntContacts; j++)
				{
					int nRole = lstContactLinks.getDataRef(j, "BNMR_SYSTEM_ROLE").toInt();
					if(bOutgoing && nRole == GlobalConstants::CONTACT_LINK_ROLE_TO){
						nCnt ++;
						nIdx = j;
					}
					else if(!bOutgoing && nRole == GlobalConstants::CONTACT_LINK_ROLE_FROM){
						nCnt ++;
						nIdx = j;
					}
				}

				if(nCnt == 1)	// only one contact found for this Appointment
				{
					//check related projects for this contact
					Status status;
					DbRecordSet lstProjects;
					int nContactID=lstContactLinks.getDataRef(nIdx, "BNMR_TABLE_KEY_ID_2").toInt();
					_SERVER_CALL(BusContact->ReadNMRXProjects(status,nContactID,lstProjects))
					_CHK_ERR(status);

					//if only one project, attach the project directly to the mail
					if(lstProjects.getRowCount() == 1)
					{
						m_lstData.setData(i, "PROJECT_CODE", lstProjects.getDataRef(0, "BUSP_CODE").toString());
						m_lstData.setData(i, "PROJECT_NAME", lstProjects.getDataRef(0, "BUSP_NAME").toString());
						m_lstData.setData(i, "BEM_PROJECT_ID", lstProjects.getDataRef(0, "BUSP_ID").toInt());
					}
				}
			}
		}
	}

	ui.tableData->RefreshDisplay();
}

void FUI_ImportAppointment::UpdateRowAppointmentContact(int nIdx)
{
	QString strAppointmentFrom, strAppointmentTo, strAppointmentCC, strAppointmentBCC;
	m_lstData.getData(nIdx, "BEM_FROM", strAppointmentFrom);
	m_lstData.getData(nIdx, "BEM_TO",   strAppointmentTo);
	m_lstData.getData(nIdx, "BEM_CC",   strAppointmentCC);
	m_lstData.getData(nIdx, "BEM_BCC",  strAppointmentBCC);
	
	//split Appointments into the list and remove Appointment name from each Appointment
	QStringList lstLocalEmlFrom = ExtractAppointments(strAppointmentFrom);
	QStringList lstLocalEmlTo   = ExtractAppointments(strAppointmentTo);
	QStringList lstLocalEmlCC   = ExtractAppointments(strAppointmentCC);
	QStringList lstLocalEmlBCC  = ExtractAppointments(strAppointmentBCC);

	//now check if the Appointments are known or unnassigned
	DbRecordSet lstContactLinks;
	lstContactLinks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	QString strUnassignedLocal;

	//CELC_LINK_ROLE_ID - 1=From 2=To 3=CC 4=BCC for Appointments
	UpdateContactsList(lstLocalEmlFrom, 1, lstContactLinks, strUnassignedLocal);
	UpdateContactsList(lstLocalEmlTo,	2, lstContactLinks, strUnassignedLocal);
	UpdateContactsList(lstLocalEmlCC,	3, lstContactLinks, strUnassignedLocal);
	UpdateContactsList(lstLocalEmlBCC,	4, lstContactLinks, strUnassignedLocal);

	m_lstData.setData(nIdx, "CONTACTS", lstContactLinks);
	m_lstData.setData(nIdx, "Appointment_UNASSIGNED", strUnassignedLocal);
}

void FUI_ImportAppointment::UpdateContactsList(QStringList &lstSrc, int nRole, DbRecordSet &lstContactLinks, QString &strUnassignedLocal)
{
	int nCount = lstSrc.count();
	for(int j=0; j<nCount; j++)
	{
		QString strAppointment = lstSrc[j];

		//check if this Appointment is already resolved in the cache
		QMap<QString, int>::iterator It = m_mapAppointment2Contact.find(strAppointment);
		if(It == m_mapAppointment2Contact.end())
		{
			if(!strUnassignedLocal.isEmpty())
				strUnassignedLocal += ";";
			strUnassignedLocal += strAppointment;
		}
		else
		{
			//Appointment is resolved, store contact ID
			lstContactLinks.addRow();
			int nRow = lstContactLinks.getRowCount()-1;
			lstContactLinks.setData(nRow, "BNMR_TABLE_KEY_ID_2", m_mapAppointment2Contact[strAppointment]);

			strAppointment.insert(0, "<");
			strAppointment += ">";

			lstContactLinks.setData(nRow, "BNMR_SYSTEM_ROLE", nRole);
		}
	}
}

QStringList FUI_ImportAppointment::ExtractAppointments(QString &strSrc)
{
	//split Appointments into the list and remove Appointment name from each Appointment
	QStringList lstLocalEmls = strSrc.split(";", QString::SkipEmptyParts);
	int nLocEmls = lstLocalEmls.count();
	int j;
	for(j=0; j<nLocEmls; j++)
	{
		QString strEntry = lstLocalEmls[j];
		int nPos = strEntry.indexOf("<");
		if(nPos > 0)
			strEntry = strEntry.right(strEntry.length()-nPos-1);
		nPos = strEntry.indexOf(">");
		if(nPos > 0)
			strEntry = strEntry.left(nPos);
		lstLocalEmls[j] = strEntry;
	}

	return lstLocalEmls;
}

bool FUI_ImportAppointment::IsDropAppointment(QByteArray &byteDrop)
{
	int nPos = byteDrop.indexOf(QByteArray("IPM.Note"));
	return (nPos >= 0);
}

bool FUI_ImportAppointment::IsDropContact(QByteArray &byteDrop)
{
	int nPos = byteDrop.indexOf(QByteArray("IPM.Contact"));
	return (nPos >= 0);
}

void FUI_ImportAppointment::BuildImportListFromDrop(QByteArray &byteDrop, bool bAreTemplates, int nDefaultProjectID)
{
	qDebug() << "FUI_ImportAppointment::BuildImportListFromDrop";

	//hide parts of the window
	ui.frame->hide();

	//if default project, show it
	if(nDefaultProjectID >= 0)
		ui.frameProject->SetCurrentEntityRecord(nDefaultProjectID, true);

#ifdef _WIN32
	//fill the grid with Appointment
	if(byteDrop.size() > 0)
	{
	#ifdef _DEBUG
	#if 0
		QFile file("D:\\aaa.txt");
		if(file.open(QIODevice::WriteOnly))
			file.write(byteDrop);
	#endif 
	#endif

		QList<QByteArray> lstEntryIDs;
		MapiManager::GetEntryIDsFromDrop(byteDrop, lstEntryIDs);
		QSet<QString> setAppointments;

		MapiManager::GenerateEmailsFromDrop(m_lstData,lstEntryIDs,setAppointments,Appointment_FilterMatchRow,(unsigned long)this, MapiManager::InitGlobalMapi());

		m_lstData.selectAll();
		OnGridFilledRecalculate(setAppointments, nDefaultProjectID);
		ui.tableData->RefreshDisplay();
	}
#endif	//#ifdef _WIN32
}

#ifdef _WIN32
void FUI_ImportAppointment::ReadAppointments(Status status, QList<QByteArray> &lstEntryIDs, DbRecordSet &lstData, bool bAreTemplates, int nDefaultProjectID, QProgressDialog *progress, bool bOnlyKnownContacts, CMAPIEx *pMapi)
{
	qDebug() << "FUI_ImportAppointment::ReadAppointments";

	int nCnt = lstEntryIDs.count();
	QSet<QString> setAppointments;
	if(!MapiManager::ReadEmailsFromEntryIDs(lstEntryIDs,setAppointments,lstData,bAreTemplates,progress, pMapi))
		return;

	//TOFIX code similar to OnGridFilledRecalculate
	QString strCode, strName;
	int nProjID;
	if(nDefaultProjectID >= 0)	// override project setting
	{
		MainEntitySelectionController cacheLoader;
		cacheLoader.Initialize(ENTITY_BUS_PROJECT);
		DbRecordSet record;
		cacheLoader.GetEntityRecord(nDefaultProjectID, record);
		if(record.getRowCount()>0)
		{
			strCode = record.getDataRef(0, "BUSP_CODE").toString();
			strName = record.getDataRef(0, "BUSP_NAME").toString();
			nProjID = record.getDataRef(0, "BUSP_ID").toInt();

			int nCol = lstData.getColumnIdx("BEM_PROJECT_ID");
			int nCodeCol = lstData.getColumnIdx("PROJECT_CODE");
			int nNameCol = lstData.getColumnIdx("PROJECT_NAME");
			lstData.setColValue(nCodeCol, strCode);
			lstData.setColValue(nNameCol, strName);
			lstData.setColValue(nCol, nProjID);
		}
	}

	//get contact ID for each Appointment not already in cache (if exists)
	if(!bAreTemplates)
	{
		DbRecordSet lstAppointments;
		lstAppointments.addColumn(QVariant::Int, "BCME_CONTACT_ID");
		lstAppointments.addColumn(QVariant::String, "BUS_CM_Appointment");

		int nPosList = 0;
		QList<QString> lstUnknown = setAppointments.toList();
		int nCntUnk = lstUnknown.count();
		int i;
		for(i=0; i<nCntUnk; i++)
		{
			//check if this Appointment is already resolved in the cache
			QMap<QString, int>::iterator It = m_mapAppointment2Contact.find(lstUnknown[i]);
			if(It == m_mapAppointment2Contact.end())
			{
				lstAppointments.addRow();
				lstAppointments.setData(nPosList, 1, lstUnknown[i]);
				nPosList ++;
			}
		}

		_SERVER_CALL(BusCommunication->GetContactFromEmail(status, lstAppointments))
		_CHK_ERR_NO_MSG(status);	// no msg due to multithreading

		QList<QString> lstUnassigned;

		//refresh cache
		nCntUnk = lstAppointments.getRowCount();
		for(i=0; i<nCntUnk; i++)
		{
			if(!lstAppointments.getDataRef(i, 0).isNull()){
				g_mutexAppointment2Cont.lock();
				m_mapAppointment2Contact[lstAppointments.getDataRef(i, 1).toString()] = lstAppointments.getDataRef(i, 0).toInt();
				g_mutexAppointment2Cont.unlock();
			}
			else
				lstUnassigned.append(lstAppointments.getDataRef(i, 1).toString());
		}

		//TOFIX code similar to UpdateRowAppointmentContact
		//calculate "Appointment_UNASSIGNED", "CONTACTS" field for individual rows
		for(i=0; i<nCnt; i++)
		{
			QString strAppointmentFrom, strAppointmentTo, strAppointmentCC, strAppointmentBCC;
			lstData.getData(i, "BEM_FROM", strAppointmentFrom);
			lstData.getData(i, "BEM_TO",   strAppointmentTo);
			lstData.getData(i, "BEM_CC",   strAppointmentCC);
			lstData.getData(i, "BEM_BCC",  strAppointmentBCC);

			//split Appointments into the list and remove Appointment name from each Appointment
			QStringList lstLocalEmlFrom = ExtractAppointments(strAppointmentFrom);
			QStringList lstLocalEmlTo   = ExtractAppointments(strAppointmentTo);
			QStringList lstLocalEmlCC   = ExtractAppointments(strAppointmentCC);
			QStringList lstLocalEmlBCC  = ExtractAppointments(strAppointmentBCC);

			//now check if the Appointments are known or unnassigned
			DbRecordSet lstContactLinks;
			lstContactLinks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
			QString strUnassignedLocal;

			//CELC_LINK_ROLE_ID - 1=From 2=To 3=CC 4=BCC for Appointments
			UpdateContactsList(lstLocalEmlFrom, 1, lstContactLinks, strUnassignedLocal);
			UpdateContactsList(lstLocalEmlTo,	2, lstContactLinks, strUnassignedLocal);
			UpdateContactsList(lstLocalEmlCC,	3, lstContactLinks, strUnassignedLocal);
			UpdateContactsList(lstLocalEmlBCC,	4, lstContactLinks, strUnassignedLocal);

			lstData.setData(i, "CONTACTS", lstContactLinks);
			lstData.setData(i, "Appointment_UNASSIGNED", strUnassignedLocal);

			//TOFIX if there is a single contact? and it has single project
			int nCntContacts = lstContactLinks.getRowCount();
			if(nCntContacts > 0)
			{
				//lstContactLinks.Dump();
				bool bOutgoing = (lstData.getDataRef(i, "BEM_OUTGOING").toInt() > 0);
				int nCnt1 = 0;
				int nIdx = -1;
				for(int j=0; j<nCntContacts; j++)
				{
					int nRole = lstContactLinks.getDataRef(j, "BNMR_SYSTEM_ROLE").toInt();
					if(bOutgoing && nRole == GlobalConstants::CONTACT_LINK_ROLE_TO){
						nCnt1 ++;
						nIdx = j;
					}
					else if(!bOutgoing && nRole == GlobalConstants::CONTACT_LINK_ROLE_FROM){
						nCnt1 ++;
						nIdx = j;
					}
				}
				if(bOnlyKnownContacts && nCnt1 < 1)
				{
					//delete this Appointment row, it's not from at least one known contact
					lstData.deleteRow(i);
					i --; nCnt --;
					continue;
				}

				// only when no default project setting
				if(nDefaultProjectID < 0 && nCnt1 == 1)	// only one contact found for this Appointment
				{
					//check related projects for this contact
					Status status;
					DbRecordSet lstProjects;
					int nContactID=lstContactLinks.getDataRef(nIdx, "BNMR_TABLE_KEY_ID_2").toInt();
					_SERVER_CALL(BusContact->ReadNMRXProjects(status,nContactID,lstProjects))
					_CHK_ERR_NO_MSG(status);	// no msg due to multithreading

					//if only one project, attach the project directly to the mail
					if(lstProjects.getRowCount() == 1)
					{
						lstData.setData(i, "PROJECT_CODE", lstProjects.getDataRef(0, "BUSP_CODE").toString());
						lstData.setData(i, "PROJECT_NAME", lstProjects.getDataRef(0, "BUSP_NAME").toString());
						lstData.setData(i, "BEM_PROJECT_ID", lstProjects.getDataRef(0, "BUSP_ID").toInt());
					}
				}
			}
			else
			{
				if(bOnlyKnownContacts)
				{
					//delete this Appointment row, it's not from at least one known contact
					lstData.deleteRow(i);
					i --; nCnt --;
					continue;
				}
			}
		}
	}
}
#endif //#ifdef _WIN32

void FUI_ImportAppointment::OnProjectChanged()
{
	//get current code/name
	QString strCode, strName;
	int nEntityRecordID = -1;
	DbRecordSet record;
	if(ui.frameProject->GetCurrentEntityRecord(nEntityRecordID, record)){
		//record.Dump();
		strName = record.getDataRef(0, "BUSP_NAME").toString();
		strCode = record.getDataRef(0, "BUSP_CODE").toString();
	}

	//ask user
	QString strMsg;
	int nSelected = m_lstData.getSelectedCount();
	if(nSelected > 0)
		strMsg = tr("Do you want to assign project \"%1 %2\" to the selected Appointment(s)?").arg(strCode).arg(strName);
	else
		strMsg = tr("Do you want to assign project \"%1 %2\" to the listed Appointment(s)?").arg(strCode).arg(strName);

	int nResult = QMessageBox::question(this, tr("Question"), strMsg, QMessageBox::Yes|QMessageBox::No);
	if(QMessageBox::Yes != nResult)
		return;

	//set data
	int nCol = m_lstData.getColumnIdx("BEM_PROJECT_ID");
	int nNameCol = m_lstData.getColumnIdx("PROJECT_NAME");
	int nCodeCol = m_lstData.getColumnIdx("PROJECT_CODE");

	m_lstData.setColValue(nCol, 
		(nEntityRecordID >= 0)? nEntityRecordID : QVariant(QVariant::Int),
		(nSelected > 0)? true : false);	// only to selected items?

	m_lstData.setColValue(nNameCol, 
		(nEntityRecordID >= 0)? strName : QVariant(QVariant::String),
		(nSelected > 0)? true : false);	// only to selected items?

	m_lstData.setColValue(nCodeCol, 
		(nEntityRecordID >= 0)? strCode : QVariant(QVariant::String),
		(nSelected > 0)? true : false);	// only to selected items?
	
	ui.tableData->RefreshDisplay();
}

void FUI_ImportAppointment::OnSignalRefreshBoldRows()
{
	//mark unread messages in bold
	//B.T. improved:
	ui.tableData->ClearAllBoldFonts();
	ui.tableData->setUpdatesEnabled(false);
	ui.tableData->blockSignals(true);
	int nCount = m_lstData.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		if(m_lstData.getDataRef(i, "BEM_UNREAD_FLAG").toInt() > 0)
			ui.tableData->SetRowBold(i);
	}
	ui.tableData->setUpdatesEnabled(true);
	ui.tableData->blockSignals(false);
}

void FUI_ImportAppointment::ClearInternalCache()
{
	m_mapAppointment2Contact.clear();
}

void FUI_ImportAppointment::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject == ui.frameProject && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		OnProjectChanged();
		return;
	}

	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);

	return; //already handled by OnContactChanged

	//QCW:
	if (nMsgCode==ChangeManager::GLOBAL_QCW_WINDOW_FINISH_PROCESS)
	{
	}
}

void FUI_ImportAppointment::FilterAppointmentsByContact(Status status, DbRecordSet &lstData)
{
	qDebug() << "FUI_ImportAppointment::FilterAppointmentsByContact";

	int nRows = lstData.getRowCount();
	for(int i=0; i<nRows; i++)
	{
		//lstData.Dump();

		//extract contacts for this Appointment
		DbRecordSet lstContactLinks;
		lstData.getData(i, "CONTACTS", lstContactLinks);

		int nContactCnt = lstContactLinks.getRowCount();
		for(int j=0; j<nContactCnt; j++)
		{
			int nContactID = lstContactLinks.getDataRef(j, "BNMR_TABLE_KEY_ID_2").toInt();
			//lstContactLinks.setData(nRow, "CELC_LINK_ROLE_ID", nLinkRole);

			//get contact flags: BCNT_DO_NOT_SYNC_MAIL, BCNT_SYNC_MAIL_AS_PRIV
			DbRecordSet lstContactInfo;
			QString strWhere= QString(" WHERE BCNT_ID=%1").arg(nContactID);
			MainEntityFilter filter;
			filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
			_SERVER_CALL(BusContact->ReadData(status, filter.Serialize(), lstContactInfo));
			_CHK_ERR_NO_MSG(status);	// no msg due to multithreading

			//now use flags to filter the list
			if( lstContactInfo.getRowCount() > 0)
			{
				if(lstContactInfo.getDataRef(0, "BCNT_DO_NOT_SYNC_MAIL").toInt() > 0)
				{
					//remove the row from the recordset
					lstData.deleteRow(i);
					i --; nRows --;
				}
				else if(lstContactInfo.getDataRef(0, "BCNT_SYNC_MAIL_AS_PRIV").toInt() > 0)
				{
					//mark as private before importing
					//lstData.setData(i, "CENT_IS_PRIVATE", 1);
					DbRecordSet lstUAR,lstGAR;
					AccUARCore::CreatePrivateUAR(g_pClientManager->GetPersonID(),lstData.getDataRef(i,"BEM_ID").toInt(),BUS_EMAIL,lstUAR,lstGAR);
					lstData.setData(i, "UAR_TABLE", lstUAR);
					lstData.setData(i, "GAR_TABLE", lstGAR);
					//lstUAR.Dump();
				}
			}
		}
	}
}

