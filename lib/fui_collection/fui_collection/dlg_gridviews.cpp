#include "dlg_gridviews.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/thememanager.h"


//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services

#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;				//global cache


Dlg_GridViews::Dlg_GridViews(QWidget *parent)
: QDialog(parent)
{
	ui.setupUi(this);

	this->setWindowTitle(tr("Configure Column Order"));//set dialog title

	//issue 1702:
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());

	
}



Dlg_GridViews::~Dlg_GridViews()
{

}


//init dialog, return false if failed:
bool Dlg_GridViews::Initialize(QString strView, int nGridID)
{
	m_nGridID=nGridID;

	//LIST:
	//enable for drag set list of addr fields:
	ui.lstAddressFields->setAcceptDrops(false);
	ui.lstAddressFields->setDragEnabled(true);
	ui.lstAddressFields->setDropIndicatorShown(true);
	DbRecordSet lstFields;
	ClientContactManager::GetFullContactGridSetup(lstFields);
	ui.lstAddressFields->SetData(lstFields);
	ui.lstAddressFields->setSelectionMode( QAbstractItemView::ExtendedSelection); //multi selection

	//TABLE:
	ui.tableWidget->Initialize(&m_lstColumnDataGrid,true);

	//COMBO:
	m_ComboHandler.Initialize(ENTITY_BUS_OPT_GRID_VIEWS);
	m_ComboHandler.SetDataForCombo(ui.cmbViews,"BOGW_VIEW_NAME");
	m_ComboHandler.SetDistinctDisplay();
	m_ComboHandler.GetLocalFilter()->SetFilter("BOGW_GRID_ID",nGridID);
	bool bOK=m_ComboHandler.ReloadData(true); //always refresh from server
	if(!bOK) return false;						//server failed to response

	
	//connect signals:
	ui.cmbViews->setCurrentIndex(-1); //reset index
	connect(ui.cmbViews,SIGNAL(currentIndexChanged(int)),this,SLOT(OnViewChanged(int)));

	//set mode:
	SetMode(MODE_READ);
	
	//find strView:
	m_ComboHandler.SetCurrentIndexFromName(strView);
	

	return true;

}





//when changing state, call this:
void Dlg_GridViews::SetMode(int nMode)
{

	m_nMode=nMode;

	if(m_nMode==MODE_READ || m_nMode==MODE_EMPTY)
	{
		ui.txtViewName->setVisible(false);
		ui.cmbViews->setVisible(true);
		ui.btnEdit->setEnabled(true);
		ui.btnNew->setEnabled(true);
		ui.btnDelete->setEnabled(true);
		ui.btnAdd->setEnabled(false);
		ui.btnRemove->setEnabled(false);
		ui.btnUp->setEnabled(false);
		ui.btnDown->setEnabled(false);
		ui.tableWidget->SetEditMode(false);

	}
	else
	{
		if(m_nMode==MODE_INSERT)
		{
			m_lstColumnDataGrid.clear();
			ui.tableWidget->RefreshDisplay();
		}
		else
		{
			//set view name
			DbRecordSet Data;
			int nIndex=m_ComboHandler.GetSelection(Data);
			if(nIndex>=0)
				ui.txtViewName->setText(Data.getDataRef(0,"BOGW_VIEW_NAME").toString());
		}
		ui.txtViewName->setVisible(true);
		ui.cmbViews->setVisible(false);
		ui.btnEdit->setEnabled(false);
		ui.btnNew->setEnabled(false);
		ui.btnDelete->setEnabled(false);
		ui.btnAdd->setEnabled(true);
		ui.btnRemove->setEnabled(true);
		ui.btnUp->setEnabled(true);
		ui.btnDown->setEnabled(true);
		ui.txtViewName->setFocus(Qt::OtherFocusReason);
		ui.tableWidget->SetEditMode(true);
	}

}






//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void Dlg_GridViews::on_btnOK_clicked()
{
	//try write:
	if(m_nMode==MODE_EDIT)
	{
		//if edit mode, take row from combo, fill, send, send locked, if ok, empty lock, refresh combo...
		int nIndex=ui.cmbViews->currentIndex();
		if(nIndex>=0) 
		{
			QString schemaName=ui.txtViewName->text();
			if(schemaName.isEmpty())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Schema name is mandatory field"));
				return;
			}

			//prepare record:
			QString strOldName=ui.cmbViews->currentText();
			
			DbRecordSet lstDataForWrite;
			lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_GRID_VIEWS));
			lstDataForWrite.merge(m_lstColumnDataGrid);

			lstDataForWrite.setColValue(lstDataForWrite.getColumnIdx("BOGW_ID"),0);
			lstDataForWrite.setColValue(lstDataForWrite.getColumnIdx("BOGW_VIEW_NAME"),schemaName);
			lstDataForWrite.setColValue(lstDataForWrite.getColumnIdx("BOGW_GRID_ID"),m_nGridID);
			//set row position:
			for (int i=0;i<lstDataForWrite.getRowCount();++i)
				lstDataForWrite.setData(i,"BOGW_COLUMN_POSITION",i);

			//send for write with lock
			Status err;
			_SERVER_CALL(BusGridViews->Write(err,lstDataForWrite,m_strLockedRes,strOldName,m_nGridID))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			m_strLockedRes.clear();
			
			DbRecordSet* pData=m_ComboHandler.GetDataSource();
			//delete old schema, add new:
			pData->find(pData->getColumnIdx("BOGW_VIEW_NAME"),strOldName);
			pData->deleteSelectedRows();
			pData->merge(lstDataForWrite);

			//refresh cache:
			RefreshCache(strOldName,lstDataForWrite);

			m_ComboHandler.RefreshDisplay();
			m_ComboHandler.SetCurrentIndexFromName(schemaName);
		}
	}

	//try insert:
	if(m_nMode==MODE_INSERT)
	{
	
		QString schemaName=ui.txtViewName->text();
		if(schemaName.isEmpty())
		{
			QMessageBox::warning(this,tr("Warning"),tr("Schema name is mandatory field"));
			return;
		}
		DbRecordSet lstDataForWrite;
		lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_GRID_VIEWS));
		lstDataForWrite.merge(m_lstColumnDataGrid);
		lstDataForWrite.setColValue(lstDataForWrite.getColumnIdx("BOGW_VIEW_NAME"),schemaName);
		lstDataForWrite.setColValue(lstDataForWrite.getColumnIdx("BOGW_GRID_ID"),m_nGridID);
		//set row position:
		for (int i=0;i<lstDataForWrite.getRowCount();++i)
			lstDataForWrite.setData(i,"BOGW_COLUMN_POSITION",i);

		//send for write with lock
		Status err;
		_SERVER_CALL(BusGridViews->Write(err,lstDataForWrite,m_strLockedRes,"",m_nGridID))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//refresh cache:
		DbRecordSet* pData=m_ComboHandler.GetDataSource();
		pData->merge(lstDataForWrite);
		RefreshCache("",lstDataForWrite);

		m_ComboHandler.RefreshDisplay();
		m_ComboHandler.SetCurrentIndexFromName(schemaName);
	}


	done(1);
}


void Dlg_GridViews::on_btnCancel_clicked()
{
	if(m_nMode==MODE_EDIT && !m_strLockedRes.isEmpty())
	{
		Status err;
		_SERVER_CALL(BusGridViews->Unlock(err,m_strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		m_strLockedRes.clear();
		SetMode(MODE_READ);	//lock released
		OnViewChanged(ui.cmbViews->currentIndex()); //reload content
		return;
	}

	if(m_nMode==MODE_INSERT)
	{
		SetMode(MODE_READ);	//lock released
		OnViewChanged(ui.cmbViews->currentIndex()); //reload content
		return;
	}


	done(0);
}

//when index changes, reload data
void Dlg_GridViews::OnViewChanged(int nIndex)
{
	if(nIndex>=0)
	{
		//select all with same name, show them, sort by position:
		DbRecordSet* pData=m_ComboHandler.GetDataSource();
		QString strOldName=ui.cmbViews->currentText();
		//delete old schema, add new:
		pData->find(pData->getColumnIdx("BOGW_VIEW_NAME"),strOldName);
		m_lstColumnDataGrid.clear();
		m_lstColumnDataGrid.merge(*pData,true);
		m_lstColumnDataGrid.sort(m_lstColumnDataGrid.getColumnIdx("BOGW_COLUMN_POSITION"));
	}
	else
	{
		m_lstColumnDataGrid.clear();
	}

	ui.tableWidget->RefreshDisplay();
}



void Dlg_GridViews::on_btnNew_clicked()
{
	SetMode(MODE_INSERT);
}

void Dlg_GridViews::on_btnEdit_clicked()
{
	int nIndex=ui.cmbViews->currentIndex();
	if(nIndex>=0) //lock
	{
		Status err;
		_SERVER_CALL(BusGridViews->Lock(err,m_ComboHandler.GetDataSource()->getDataRef(nIndex,"BOGW_VIEW_NAME").toString(),m_strLockedRes,m_nGridID))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		SetMode(MODE_EDIT);	//lock obtained
	}
}

void Dlg_GridViews::on_btnDelete_clicked()
{
	int nIndex=ui.cmbViews->currentIndex();
	if(nIndex>=0) 
	{
		int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete the selected view from the database"),tr("Yes"),tr("No"));
		if (nResult!=0) return; //only if YES

		QString strViewName=ui.cmbViews->currentText();

		Status err;
		_SERVER_CALL(BusGridViews->Delete(err,strViewName,m_nGridID))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//refresh cache:
		DbRecordSet Data;
		
		DbRecordSet* pData=m_ComboHandler.GetDataSource();
		//delete old schema, add new:
		pData->find(pData->getColumnIdx("BOGW_VIEW_NAME"),strViewName);
		Data.copyDefinition(*pData);
		Data.merge(*pData,true);
		pData->deleteSelectedRows();

		m_lstColumnDataGrid.clear();
		ui.tableWidget->RefreshDisplay();

		m_ComboHandler.RefreshToGlobalCache(MainEntitySelectionController::CACHE_UPDATE_DATA_DELETED,false,&Data);
		m_ComboHandler.RefreshDisplay();
		OnViewChanged(ui.cmbViews->currentIndex()); //reload content
	}


}





void Dlg_GridViews::on_btnAdd_clicked()
{
	DbRecordSet lstData;
	ui.lstAddressFields->GetDropValue(lstData);
	m_lstColumnDataGrid.merge(lstData);
	ui.tableWidget->RefreshDisplay();
}

void Dlg_GridViews::on_btnRemove_clicked()
{
	ui.tableWidget->DeleteSelection();
}

void Dlg_GridViews::on_btnUp_clicked()
{
	ui.tableWidget->MoveRowUp();
}

void Dlg_GridViews::on_btnDown_clicked()
{
	ui.tableWidget->MoveRowDown();
}



QString Dlg_GridViews::GetCurrentViewName()
{
	return ui.cmbViews->currentText();
}


void Dlg_GridViews::RefreshCache(QString oldViewName,DbRecordSet& newData)
{
	//get from cache:
	DbRecordSet *pCache=g_ClientCache.GetCache(ENTITY_BUS_OPT_GRID_VIEWS);
	if (pCache && !oldViewName.isEmpty())
	{
		//remove old:
		pCache->find(pCache->getColumnIdx("BOGW_GRID_ID"),m_nGridID);
		pCache->find(pCache->getColumnIdx("BOGW_VIEW_NAME"),oldViewName,false,true,true,true);
		DbRecordSet lstOld;
		lstOld.copyDefinition(*pCache);
		lstOld.merge(*pCache,true);
		m_ComboHandler.RefreshToGlobalCache(MainEntitySelectionController::CACHE_UPDATE_DATA_DELETED,false,&lstOld);
	}
	
	//send add signal:
	m_ComboHandler.RefreshToGlobalCache(MainEntitySelectionController::CACHE_UPDATE_DATA_INSERTED,false,&newData);
}