#include "qc_entityselector.h"
#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/thememanager.h"
#include "qcw_base.h"

QC_EntitySelector::QC_EntitySelector(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	m_strOfString = tr("of");
	ui.lineEdit->setAlignment(Qt::AlignHCenter);
	ui.lineEdit->setReadOnly(true);

	ui.previous_toolButton->setIcon(QIcon(":1leftarrow.png"));
	ui.next_toolButton->setIcon(QIcon(":1rightarrow.png"));
	ui.delete_toolButton->setIcon(QIcon(":Delete16_qcw.png"));
	ui.add_toolButton->setIcon(QIcon(":Insert16_qcw.png"));
}

QC_EntitySelector::~QC_EntitySelector()
{

}

void QC_EntitySelector::Initialize(int nEntityCollectionID, DbRecordSet *recFullContactData, QWidget *ParentWidget /*= NULL*/, int nParentType /*= 0 -Large,1-small widget*/)
{
	m_pParentWidget			= ParentWidget;
	m_nEntityCollectionID	= nEntityCollectionID;
	m_recFullContactData	= recFullContactData;
	
	if (m_nEntityCollectionID == ENTITY_BUS_CM_EMAIL)
	{
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_EMAIL").value<DbRecordSet>();
		m_strOfString = tr("/");
}
	else if (m_nEntityCollectionID == ENTITY_BUS_CM_INTERNET)
	{
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_INTERNET").value<DbRecordSet>();
		m_strOfString = tr("/");
	}
	else if (m_nEntityCollectionID == ENTITY_BUS_CM_ADDRESS)
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
	else
		Q_ASSERT(false);
	
	//_DUMP(m_recLocalEntityRecordSet);
	m_nPageCount = m_recLocalEntityRecordSet.getRowCount();

	if (m_nPageCount<=0)
	{
		on_add_toolButton_clicked();
		return;
	}

	m_nCurrentPage = 1;

	SetupLineEdit();
	emit RowChanged(m_nCurrentPage-1);
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

void QC_EntitySelector::addEntity(DbRecordSet recEntityData)
{
	DbRecordSet recData = recEntityData;
	on_add_toolButton_clicked(&recData);
}

void QC_EntitySelector::addEntityNoOverride(DbRecordSet recEntityData)
{
	DbRecordSet recData = recEntityData;
	on_add_toolButton_clicked();
	on_add_toolButton_clicked(&recData);
}

void QC_EntitySelector::SetupLineEdit()
{
	QString strLineEdit;
	strLineEdit = QVariant(m_nCurrentPage).toString() + " " + m_strOfString + " " + QVariant(m_nPageCount).toString();
	ui.lineEdit->setText(strLineEdit);

	//Enable/Disable buttons.
	if (m_nCurrentPage <= 1)
		ui.previous_toolButton->setDisabled(true);
	else
		ui.previous_toolButton->setDisabled(false);
	
	if ((m_nCurrentPage+1 > m_nPageCount) || (m_nCurrentPage == 1 && m_nPageCount == 1))
		ui.next_toolButton->setDisabled(true);
	else
		ui.next_toolButton->setDisabled(false);

	if((m_nCurrentPage == 0) || (m_nCurrentPage == 1 && m_nPageCount ==1))
		ui.delete_toolButton->setDisabled(true);
	else
		ui.delete_toolButton->setDisabled(false);
}

void QC_EntitySelector::on_previous_toolButton_clicked()
{
	m_nCurrentPage--;
	SetupLineEdit();
	emit RowChanged(m_nCurrentPage-1);
}

void QC_EntitySelector::on_next_toolButton_clicked()
{
	m_nCurrentPage++;
	SetupLineEdit();
	emit RowChanged(m_nCurrentPage-1);
}

void QC_EntitySelector::AppendOrUpdateEmailDescr(DbRecordSet &recEmail, DbRecordSet recNewRecordset)
{
	bool	bEmailExists		= false;
	QString strNewEmail			= recNewRecordset.getDataRef(0, "BCME_ADDRESS").toString();
	QString strNewDescription	= recNewRecordset.getDataRef(0, "BCME_DESCRIPTION").toString();
	int		nExistingEmailCount = recEmail.getRowCount();
	for (int i=0; i < nExistingEmailCount; i++)
	{
		if (recEmail.getDataRef(i, "BCME_ADDRESS").toString() == strNewEmail)
		{
			bEmailExists = true;
			if (recEmail.getDataRef(i, "BCME_DESCRIPTION").toString().isEmpty())
				recEmail.setData(i, "BCME_DESCRIPTION", strNewDescription);
		}
	}

	//Now, if email don't exist simply add new email. Else check is there description and add it if don't exists.
	if (!bEmailExists)
		recEmail.assignRow(m_nCurrentPage-1, recNewRecordset);
}

void QC_EntitySelector::on_add_toolButton_clicked(DbRecordSet *recRecordset /*= NULL*/)
{
	//Get new and clean copy of recordset and delete row.
	bool bRowAdded = true;
	
	if (m_nEntityCollectionID == ENTITY_BUS_CM_EMAIL)
	{
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_EMAIL").value<DbRecordSet>();
		if (recRecordset)
		{
			AppendOrUpdateEmailDescr(m_recLocalEntityRecordSet, *recRecordset);
			bRowAdded = false;
		}
		else
			m_recLocalEntityRecordSet.addRow();
		m_recFullContactData->setData(0, "LST_EMAIL", m_recLocalEntityRecordSet);
	}
	else if (m_nEntityCollectionID == ENTITY_BUS_CM_INTERNET)
	{
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_INTERNET").value<DbRecordSet>();
		if (recRecordset)
		{
			m_recLocalEntityRecordSet.assignRow(m_nCurrentPage-1, *recRecordset);
			bRowAdded = false;
		}
		else
			m_recLocalEntityRecordSet.addRow();
		m_recFullContactData->setData(0, "LST_INTERNET", m_recLocalEntityRecordSet);
	}
	else if (m_nEntityCollectionID == ENTITY_BUS_CM_ADDRESS)
	{
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
		if (recRecordset)
		{
			m_recLocalEntityRecordSet.assignRow(m_nCurrentPage-1, *recRecordset);
			bRowAdded = false;
		}
		else
		{
			m_recLocalEntityRecordSet.addRow();
		}

		dynamic_cast<QCW_Base*>(m_pParentWidget)->AddressAdded(m_recLocalEntityRecordSet);
	}
	
	if (bRowAdded)
	{
		m_nPageCount++;
		m_nCurrentPage = m_nPageCount;
	}

	SetupLineEdit();
	emit RowChanged(m_nCurrentPage-1);
}

void QC_EntitySelector::on_delete_toolButton_clicked()
{
	//Get new and clean copy of recordset and delete row.
	if (m_nEntityCollectionID == ENTITY_BUS_CM_EMAIL)
	{
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_EMAIL").value<DbRecordSet>();
		m_recLocalEntityRecordSet.deleteRow(m_nCurrentPage-1);
		m_recFullContactData->setData(0, "LST_EMAIL", m_recLocalEntityRecordSet);
	}
	else if (m_nEntityCollectionID == ENTITY_BUS_CM_INTERNET)
	{
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_INTERNET").value<DbRecordSet>();
		m_recLocalEntityRecordSet.deleteRow(m_nCurrentPage-1);
		m_recFullContactData->setData(0, "LST_INTERNET", m_recLocalEntityRecordSet);
	}
	else if (m_nEntityCollectionID == ENTITY_BUS_CM_ADDRESS)
	{
		m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
		m_recLocalEntityRecordSet.deleteRow(m_nCurrentPage-1);
		dynamic_cast<QCW_Base*>(m_pParentWidget)->AddressAdded(m_recLocalEntityRecordSet);
		//m_recFullContactData->setData(0, "LST_ADDRESS", m_recLocalEntityRecordSet);
	}

	if (m_nCurrentPage > 1)
	{
		m_nCurrentPage--;
		m_nPageCount--;
	}
	else
		m_nPageCount--;

	SetupLineEdit();
	emit RowChanged(m_nCurrentPage-1);
}
