#include "settings_programupdates.h"

Settings_ProgramUpdates::Settings_ProgramUpdates(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);
	ui.ckbUpdates->setChecked(GetSettingValue(THICK_CHECK_FOR_UPDATES).toBool());
}

Settings_ProgramUpdates::~Settings_ProgramUpdates()
{

}


DbRecordSet Settings_ProgramUpdates::GetChangedValuesRecordSet()
{
	if (ui.ckbUpdates->isChecked()!=GetSettingValue(THICK_CHECK_FOR_UPDATES).toBool())
		SetSettingValue(THICK_CHECK_FOR_UPDATES, ui.ckbUpdates->isChecked());

	return SettingsBase::GetChangedValuesRecordSet();
}