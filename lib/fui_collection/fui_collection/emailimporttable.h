#ifndef EMAILIMPORTTABLE_H
#define EMAILIMPORTTABLE_H

#include "gui_core/gui_core/universaltablewidget.h"
class FUI_ImportEmail;

class EmailImportTable : public UniversalTableWidget
{
    Q_OBJECT

public:
    EmailImportTable(QWidget *parent = 0);
    ~EmailImportTable();

public:
	FUI_ImportEmail	*m_pParent;

protected:
	//DnD support
	virtual void dragMoveEvent ( QDragMoveEvent * event);
	virtual void dragEnterEvent ( QDragEnterEvent * event);
	virtual void dropEvent(QDropEvent *event);


};

#endif // EMAILIMPORTTABLE_H
