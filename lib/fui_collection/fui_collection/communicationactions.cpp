#include "communicationactions.h"
#include <QtWidgets/QMessageBox>
#include "common/common/dbrecordset.h"
#include "fui_voicecallcenter.h"
#include <QDesktopServices>
#include "emaildialog.h"

#include "doc_menuwidget.h"
#include "email_menuwidget.h"
#include "sms_dialog.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include <QApplication>
#include <QRect>
#include <QDesktopWidget>
#include "wizardregistration.h"
#include "taskdialog.h"
#include "bus_core/bus_core/globalconstants.h"
#include "os_specific/os_specific/skypemanager.h"
#include "common/common/datahelper.h"
#include "dlg_serialemailsmode.h"

#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache; //global cache:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:

/*
CommunicationActions::CommunicationActions(QObject *parent)
	: QObject(parent)
{

}

CommunicationActions::~CommunicationActions()
{

}
*/

//load email FUI, with given contact/email
void CommunicationActions::sendEmail(int nContactID,QString strDefaultEmail,int nProjectID,QPoint *pos,QStringList *lstPackSendPaths,int nEmailReplayID, bool IsForwardMode,bool bScheduleTask)
{

	Email_MenuWidget *menuDoc= new Email_MenuWidget( NULL,true); 
	menuDoc->DisableHide();
	
	//set defaults:
	if (lstPackSendPaths)
		menuDoc->SetPackSendMode(lstPackSendPaths);

	if (bScheduleTask)
		menuDoc->SetScheduleFlag();
	menuDoc->SetDefaultMail(strDefaultEmail,nEmailReplayID,IsForwardMode);
	menuDoc->SetCloseOnSend();


	DbRecordSet lstContacts;
	lstContacts.addColumn(QVariant::Int,"BCNT_ID");
	DbRecordSet lstProjects;
	lstProjects.addColumn(QVariant::Int,"BUSP_ID");

	//set contact:
	if (nContactID>0)
	{
		lstContacts.addRow();
		lstContacts.setData(0,0,nContactID);
	}
	//set project:
	if (nProjectID>0)
	{
		lstProjects.addRow();
		lstProjects.setData(0,0,nProjectID);
	}
		
	menuDoc->SetDefaults(&lstContacts,&lstProjects);
	menuDoc->SetSideBarLayout(); //issue 1387

	if(pos)
	{
		//avoid to go off screen: issue 1387
		QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(menuDoc));
		if ((pos->x()+menuDoc->geometry().width())>(desktopRec.x()+desktopRec.width()))
			pos->setX(pos->x()-menuDoc->geometry().width());
		menuDoc->move(*pos);
	}

	//set title, go go
	menuDoc->setWindowTitle(QObject::tr("Send Email"));
	menuDoc->show();

}


//load FUI voicecall+contact+default phone
void CommunicationActions::callPhone(int nContactID,QString strDefaultPhone,int nProjectID)
{

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false);//,FuiBase::MODE_READ, nRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);

	//only if loaded contact, open voice calls
	if (nContactID>0)
	{
		FUI_VoiceCallCenter *pVoiceCall=dynamic_cast<FUI_VoiceCallCenter *>(pFUI);
		if (pVoiceCall)
		{
			pVoiceCall->SetDefaultContact(nContactID,strDefaultPhone,nProjectID);
		}
	}


	if(pFUI)pFUI->show();  //show FUI

}


void CommunicationActions::sendDocument(int nContactID,QString strDefaultDoc,QPoint *pos)
{
	Doc_MenuWidget *menuDoc= new Doc_MenuWidget( NULL,true);
	menuDoc->DisableHide();
	menuDoc->SetCloseOnSend();

	//set contact:
	if (nContactID>0)
	{
		DbRecordSet lstContacts;
		lstContacts.addColumn(QVariant::Int,"BCNT_ID");
		lstContacts.addRow();
		lstContacts.setData(0,0,nContactID);
		menuDoc->SetDefaults(&lstContacts);
	}

	if(pos)
		menuDoc->move(*pos);

	//set title, go go
	menuDoc->setWindowTitle(QObject::tr("Create New Document"));
	menuDoc->show();

}

void CommunicationActions::openWeb(int nContactID,QString strDefaultInternet)
{
	//load contact data, send internet if default is empty
	if (nContactID>0 && strDefaultInternet.isEmpty())
	{
		Status err;
		DbRecordSet rowContactData;
		_SERVER_CALL(BusContact->ReadContactDefaults(err,nContactID,rowContactData))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,QObject::tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		if (rowContactData.getRowCount()>0)
		{
			strDefaultInternet=rowContactData.getDataRef(0,"BCMI_ADDRESS").toString();
		}

	}

	if (strDefaultInternet.isEmpty())
	{
		QMessageBox::information(NULL,QObject::tr("Warning"),QObject::tr("Contact does not have any web address!"));
		return;
	}
	


	//issue nr: 911
	//if (strDefaultInternet.indexOf("http://")==0)
		QDesktopServices::openUrl(strDefaultInternet.trimmed());
	//else
	//	QDesktopServices::openUrl("http://"+strDefaultInternet);

}

void CommunicationActions::openChat(int nContactID)
{
	//get contact numbers
	Status err;
	DbRecordSet lstPhones;
	DbRecordSet lstPicture,lstDataNMRXContacts,lstDataNMRXProjects;
	_SERVER_CALL(BusContact->ReadContactDataForVoiceCall(err,nContactID,lstPhones,lstPicture,lstDataNMRXContacts,lstDataNMRXProjects))
	if (!err.IsOK()) return;

	//load phone types
	MainEntitySelectionController TypeHandler;
	TypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_PHONE);
	TypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	TypeHandler.ReloadData();
	DbRecordSet recPhoneTypes = *TypeHandler.GetDataSource();

	QString strNumber;

	//find skype number
	int nSize=lstPhones.getRowCount();
	for(int i=0;i<nSize;++i){
		int nRow = recPhoneTypes.find("BCMT_ID", lstPhones.getDataRef(i,"BCMP_TYPE_ID").toString(), true);
		if(nRow >= 0){
			if(ContactTypeManager::SYSTEM_SKYPE == recPhoneTypes.getDataRef(nRow,"BCMT_SYSTEM_TYPE").toInt())
			{
				strNumber = lstPhones.getDataRef(i,"BCMP_SEARCH").toString();
				break;
			}
		}
	}

	if(!strNumber.isEmpty()){
		QString strCmd = QString().sprintf("CHAT CREATE %s", strNumber.toLatin1().constData());
		SkypeManager::SendCommand(strCmd.toLatin1().constData());
	}
	else{
		QMessageBox::information(NULL, "", QObject::tr("This contact has no Skype(R) name defined. It is required for chatting!"));
	}
}

void CommunicationActions::openSMS(int nContactID)
{
	//get contact numbers
	Status err;
	DbRecordSet lstPhones;
	DbRecordSet lstPicture,lstDataNMRXContacts,lstDataNMRXProjects;
	_SERVER_CALL(BusContact->ReadContactDataForVoiceCall(err,nContactID,lstPhones,lstPicture,lstDataNMRXContacts,lstDataNMRXProjects))
	if (!err.IsOK()) return;

	//load phone types
	MainEntitySelectionController TypeHandler;
	TypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_PHONE);
	TypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	TypeHandler.ReloadData();
	DbRecordSet recPhoneTypes = *TypeHandler.GetDataSource();

	QString strNumber;

	//find mobile number
	int nSize=lstPhones.getRowCount();
	for(int i=0;i<nSize;++i){
		int nRow = recPhoneTypes.find("BCMT_ID", lstPhones.getDataRef(i,"BCMP_TYPE_ID").toString(), true);
		if(nRow >= 0){
			if(ContactTypeManager::SYSTEM_PHONE_MOBILE == recPhoneTypes.getDataRef(nRow,"BCMT_SYSTEM_TYPE").toInt())
			{
				strNumber = lstPhones.getDataRef(i,"BCMP_SEARCH").toString();
				break;
			}
		}
	}

	if(strNumber.isEmpty())
	{
		//find mobile private number as an alternative
		for(int i=0;i<nSize;++i){
			int nRow = recPhoneTypes.find("BCMT_ID", lstPhones.getDataRef(i,"BCMP_TYPE_ID").toString(), true);
			if(nRow >= 0){
				if(ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE == recPhoneTypes.getDataRef(nRow,"BCMT_SYSTEM_TYPE").toInt())
				{
					strNumber = lstPhones.getDataRef(i,"BCMP_SEARCH").toString();
					break;
				}
			}
		}
	}

	SMS_Dialog dlg;
	dlg.SetContact(nContactID);
	dlg.SetNumber(strNumber);
	dlg.exec();
}



//open contact selection wizard, get contacts, if false returns: error or user canceled
bool CommunicationActions::getProjectContacts(int nProjectID, DbRecordSet &lstContacts)
{
	//load contacts for project
	Status err;
	DbRecordSet lstContacts_All;
	_SERVER_CALL(BusContact->ReadNMRXContactsFromProject(err,nProjectID,lstContacts_All))
	_CHK_ERR_RET_BOOL_ON_FAIL(err)

	//set as default for list:
	g_ClientCache.SetCache(WIZARD_CONTACT_SELECTION_PAGE,lstContacts_All);

	//open wizard
	QList<DbRecordSet> RecordSetList;
	WizardRegistration wizReg(WizardRegistration::CONTACT_SELECTION_WIZARD_SIMPLE);
	WizardBase wiz(WizardRegistration::CONTACT_SELECTION_WIZARD_SIMPLE, wizReg.m_hshWizardPageHash, &g_ClientCache);
	if (wiz.exec())
	{
		wiz.GetWizPageRecordSetList(RecordSetList);
		lstContacts=RecordSetList.at(0);
		if (lstContacts.getRowCount()==0) //issue: 1450: if empty then all
			lstContacts=lstContacts_All;
		return true;
	}

	return false;
}



//load email FUI, with given contact/email
void CommunicationActions::sendEmail_Project(int nProjectID,QString strDefaultEmail,QPoint *pos,QStringList *lstPackSendPaths,int nEmailReplayID, bool IsForwardMode,bool bScheduleTask)
{

	Email_MenuWidget *menuDoc= new Email_MenuWidget( NULL,true); 
	menuDoc->DisableHide();

	//set defaults:
	if (lstPackSendPaths)
		menuDoc->SetPackSendMode(lstPackSendPaths);

	if (bScheduleTask)
		menuDoc->SetScheduleFlag();
	menuDoc->SetDefaultMail(strDefaultEmail,nEmailReplayID,IsForwardMode);
	menuDoc->SetCloseOnSend();
	menuDoc->SetSerialMode();


	DbRecordSet lstContacts;
	lstContacts.addColumn(QVariant::Int,"BCNT_ID");
	DbRecordSet lstProjects;
	lstProjects.addColumn(QVariant::Int,"BUSP_ID");

	//set project:
	if (nProjectID>0)
	{
		lstProjects.addRow();
		lstProjects.setData(0,0,nProjectID);
		CommunicationActions::getProjectContacts(nProjectID,lstContacts);
	}

	menuDoc->SetDefaults(&lstContacts,&lstProjects);
	menuDoc->SetSideBarLayout(); //issue 1387

	if(pos)
	{
		//avoid to go off screen: issue 1387
		QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(menuDoc));
		if ((pos->x()+menuDoc->geometry().width())>(desktopRec.x()+desktopRec.width()))
			pos->setX(pos->x()-menuDoc->geometry().width());
		menuDoc->move(*pos);
	}

	//set title, go go
	menuDoc->setWindowTitle(QObject::tr("Send Email"));
	menuDoc->show();

}


//load FUI voicecall+contact+default phone
void CommunicationActions::callPhone_Project(int nProjectID,QString strDefaultPhone)
{
	//only if loaded contact, open voice calls
	if (nProjectID>0)
	{
		DbRecordSet lstContacts;
		CommunicationActions::getProjectContacts(nProjectID,lstContacts);
		int nSize=lstContacts.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			callPhone(lstContacts.getDataRef(i,"BCNT_ID").toInt(),"",nProjectID);
		}
		return;
	}
	else
	{
		g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true);
	}


}


void CommunicationActions::sendDocument_Project(int nProjectID,QString strDefaultDoc,QPoint *pos)
{
	Doc_MenuWidget *menuDoc= new Doc_MenuWidget( NULL,true);
	menuDoc->DisableHide();
	menuDoc->SetCloseOnSend();

	DbRecordSet lstContacts;
	lstContacts.addColumn(QVariant::Int,"BCNT_ID");
	DbRecordSet lstProjects;
	lstProjects.addColumn(QVariant::Int,"BUSP_ID");

	//set project:
	if (nProjectID>0)
	{
		lstProjects.addRow();
		lstProjects.setData(0,0,nProjectID);
		CommunicationActions::getProjectContacts(nProjectID,lstContacts);
	}

	menuDoc->SetDefaults(&lstContacts,&lstProjects);

	if(pos)
		menuDoc->move(*pos);

	//set title, go go
	menuDoc->setWindowTitle(QObject::tr("Create New Document"));
	menuDoc->show();

}

void CommunicationActions::openWeb_Project(int nProjectID,QString strDefaultInternet)
{
	//load contact data, send internet if default is empty
	if (nProjectID>0 && strDefaultInternet.isEmpty())
	{

		DbRecordSet lstContacts;
		CommunicationActions::getProjectContacts(nProjectID,lstContacts);
		int nSize=lstContacts.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			openWeb(lstContacts.getDataRef(i,"BCNT_ID").toInt());
		}

		return;
	}

	if (strDefaultInternet.isEmpty())
	{
		QMessageBox::information(NULL,QObject::tr("Warning"),QObject::tr("Contact does not have any web address!"));
		return;
	}


	//issue nr: 911
	//if (strDefaultInternet.indexOf("http://")==0)
	QDesktopServices::openUrl(strDefaultInternet.trimmed());
	//else
	//	QDesktopServices::openUrl("http://"+strDefaultInternet);

}

void CommunicationActions::openChat_Project(int nProjectID)
{
	if (nProjectID>0)
	{
		DbRecordSet lstContacts;
		CommunicationActions::getProjectContacts(nProjectID,lstContacts);
		int nSize=lstContacts.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			openChat(lstContacts.getDataRef(i,"BCNT_ID").toInt());
		}
	}
}

void CommunicationActions::openSMS_Project(int nProjectID)
{
	if (nProjectID>0)
	{
		DbRecordSet lstContacts;
		CommunicationActions::getProjectContacts(nProjectID,lstContacts);
		int nSize=lstContacts.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			openSMS(lstContacts.getDataRef(i,"BCNT_ID").toInt());
		}
	}

}


//if 1 recordset: open dialog
//if >1 fill rowTasks with default
void CommunicationActions::createEmailQuickTask(DbRecordSet &lstEmails, QPoint *pos, bool bForceSilentCreate)
{
	if (lstEmails.getRowCount()==0)
		return;

	if (lstEmails.getRowCount()==1 && !bForceSilentCreate)
	{
		int nEmailID=lstEmails.getDataRef(0,"BEM_ID").toInt();
		//CommunicationActions::sendEmail(0,"",-1,pos,NULL,nEmailID);
		Email_MenuWidget *menuDoc= new Email_MenuWidget( NULL,true); 
		menuDoc->DisableHide();
		//set defaults:
		menuDoc->SetDefaultMail("",nEmailID,false);
		menuDoc->SetCloseOnSend();
		menuDoc->SetCreateQuickTaskFlag();

		DbRecordSet lstContacts;
		lstContacts.addColumn(QVariant::Int,"BCNT_ID");
		DbRecordSet lstProjects;
		lstProjects.addColumn(QVariant::Int,"BUSP_ID");
		menuDoc->SetDefaults(&lstContacts,&lstProjects);
		menuDoc->SetSideBarLayout(); //issue 1387

		if(pos)
		{
			//avoid to go off screen: issue 1387
			QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(menuDoc));
			if ((pos->x()+menuDoc->geometry().width())>(desktopRec.x()+desktopRec.width()))
				pos->setX(pos->x()-menuDoc->geometry().width());
			menuDoc->move(*pos);
		}

		//set title, go go
		menuDoc->setWindowTitle(QObject::tr("Create Email Task"));
		menuDoc->show();
	}
	else //>1 email
	{
		int nNewFUI=g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_EMPTY,-1,true);
		QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
		EMailDialog* pFUI=dynamic_cast<EMailDialog*>(pFUIWidget);
		if (pFUI)
		{
			DbRecordSet lstEmailSaved;
			int nSize=lstEmails.getRowCount();
			for(int i=0;i<nSize;i++)
			{
				int nEmailReplyID=lstEmails.getDataRef(i,"BEM_ID").toInt();
				int nProjectID=lstEmails.getDataRef(i,"BUSP_ID").toInt();
				if (nProjectID==0)
					nProjectID=-1;
				DbRecordSet lstEmpty;
				pFUI->SetDefaults(lstEmpty,"",nProjectID,-1,true,false,nEmailReplyID);
				Status err;
				pFUI->CreateQuickTask(err,lstEmpty);
				_CHK_ERR(err);
				if (err.IsOK())
				{
					DbRecordSet *dataSaved=pFUI->GetRecordSet();
					if (lstEmailSaved.getRowCount()==0)
						lstEmailSaved.copyDefinition(*dataSaved);
					lstEmailSaved.merge(*dataSaved);
				}
			}
			if (lstEmailSaved.getRowCount()>0)
				pFUI->notifyCache_AfterWrite(lstEmailSaved);
			pFUI->close();
		}
	}
}


//fur Mihro
void CommunicationActions::createPhoneQuickTask(DbRecordSet &lstPhones, QPoint *pos, bool bForceSilentCreate)
{
	if (lstPhones.getRowCount()==0)
		return;

	if (lstPhones.getRowCount()==1 && !bForceSilentCreate)
	{
		//lstPhones.Dump();

		//show task dialog
		TaskDialog Dlg;
		Dlg.SetDefaultTaskType(GlobalConstants::TASK_TYPE_SCHEDULED_VOICE_CALL);
		Dlg.m_pRecMain=&lstPhones;
		Dlg.SetGUIFromData();
		Dlg.SetEnabled(true);

		if(Dlg.exec())
		{
			Dlg.GetDataFromGUI();
			DbRecordSet rowTask=Dlg.m_recTask;
			rowTask.setData(0, "BTKS_IS_TASK_ACTIVE", 1);
			rowTask.setData(0, "BTKS_OWNER", g_pClientManager->GetPersonID());
			rowTask.setData(0, "BTKS_ORIGINATOR", g_pClientManager->GetPersonID());

			//define list:
			DbRecordSet set, m_recData;
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
			m_recData.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_VOICECALLS));
			m_recData.copyDefinition(set, false);
			m_recData.merge(lstPhones.getRow(0));

			//re-write voice call record
			Status err;
			DbRecordSet lstUAR,lstGAR;
			_SERVER_CALL(VoiceCallCenter->WriteCall(err, m_recData,"",rowTask, lstUAR,lstGAR))
			if(!err.IsOK()){
				QString strErr = err.getErrorText();
				QMessageBox::information(NULL, "", strErr);
			}
			else{
				//refresh cache
				g_ClientCache.ModifyCache(ENTITY_BUS_VOICECALLS, m_recData, DataHelper::OP_EDIT, NULL); 
			}
		}
	}
	else //>1 phone call
	{
		//just write default tasks (one per call)
		int nSize=lstPhones.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			DbRecordSet rowTask;
			rowTask.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
			rowTask.addRow();
			rowTask.setData(0, "BTKS_IS_TASK_ACTIVE", 1);
			rowTask.setData(0, "BTKS_SYSTEM_STATUS", 1);		//set "scheduled"
			rowTask.setData(0, "BTKS_OWNER", g_pClientManager->GetPersonID());
			rowTask.setData(0, "BTKS_ORIGINATOR", g_pClientManager->GetPersonID());
			rowTask.setData(0, "BTKS_START", QDateTime::currentDateTime());	 //his active
			rowTask.setData(0, "BTKS_ORIGINATOR", g_pClientManager->GetPersonID());
			rowTask.setData(0, "BTKS_SYSTEM_TYPE", GlobalConstants::TASK_TYPE_SCHEDULED_VOICE_CALL);

			//define list:
			DbRecordSet set, m_recData;
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
			m_recData.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_VOICECALLS));
			m_recData.copyDefinition(set, false);

			m_recData.merge(lstPhones.getRow(i));


			Status err;
			//re-write voice call record
			DbRecordSet lstUAR,lstGAR;
			_SERVER_CALL(VoiceCallCenter->WriteCall(err, m_recData,"",rowTask, lstUAR,lstGAR))
			if(!err.IsOK()){
				QString strErr = err.getErrorText();
				QMessageBox::information(NULL, "", strErr);
			}
			else{
				//refresh cache
				g_ClientCache.ModifyCache(ENTITY_BUS_VOICECALLS, m_recData, DataHelper::OP_EDIT, NULL); 
			}
		}
	}
}

void CommunicationActions::sendEmail_Serial(DbRecordSet &lstContactDetail,QPoint *pos)
{
	Dlg_SerialEmailsMode dlg;
	if(!dlg.exec())
		return;
	bool bAllEmails = dlg.GetUseAllEmails();

	Email_MenuWidget *menuDoc= new Email_MenuWidget( NULL,true); 
	menuDoc->DisableHide();
	menuDoc->SetCloseOnSend();
	menuDoc->SetSerialMode(bAllEmails);

	DbRecordSet lstContacts;
	lstContacts.addColumn(QVariant::Int,"BCNT_ID");
	DbRecordSet lstProjects;
	lstProjects.addColumn(QVariant::Int,"BUSP_ID");

	lstContacts.merge(lstContactDetail);
	menuDoc->SetDefaults(&lstContacts,&lstProjects);
	menuDoc->SetSideBarLayout(); //issue 1387

	if(pos)
	{
		//avoid to go off screen: issue 1387
		QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(menuDoc));
		if ((pos->x()+menuDoc->geometry().width())>(desktopRec.x()+desktopRec.width()))
			pos->setX(pos->x()-menuDoc->geometry().width());
		menuDoc->move(*pos);
	}

	//set title, go go
	menuDoc->setWindowTitle(QObject::tr("Send Serial Email"));
	menuDoc->show();

}