#ifndef OPTIONS_CALENDAR_H
#define OPTIONS_CALENDAR_H

#include <QWidget>
#include "ui_options_calendar.h"
#include "settingsbase.h"

class options_calendar : public SettingsBase
{
	Q_OBJECT

public:
	options_calendar(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
	~options_calendar();

	DbRecordSet GetChangedValuesRecordSet();

private:
	Ui::options_calendar ui;
};

#endif // OPTIONS_CALENDAR_H
