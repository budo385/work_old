#ifndef SETTINGS_COMM_H
#define SETTINGS_COMM_H

#include <QWidget>
#include "generatedfiles/ui_settings_comm.h"
#include "settingsbase.h"

class Settings_Comm : public SettingsBase
{
    Q_OBJECT

public:
    Settings_Comm(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
    ~Settings_Comm();

	DbRecordSet GetChangedValuesRecordSet();
	
	int	 GetGroupSetting();
	void SetGroupSetting(int nGroupID);

private slots:
	void on_chkDialPfx_clicked();

private:
	bool m_bIsOptionsPage;
    Ui::Settings_CommClass ui;
};

#endif // SETTINGS_COMM_H
