#include "qc_accessrightsframe.h"
#include "gui_core/gui_core/thememanager.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"

QC_AccessRightsFrame::QC_AccessRightsFrame(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ui.arSelector->HideLabel();
	ui.arSelector->Invalidate();
	m_nEntityRecordID=-1;
}

QC_AccessRightsFrame::~QC_AccessRightsFrame()
{

}

void QC_AccessRightsFrame::Initialize(DbRecordSet *recFullContactData)
{
	m_recFullContactData = recFullContactData;
	SetupGUIwidgets();
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

void QC_AccessRightsFrame::SetupGUIwidgets()
{
	if (m_recFullContactData->getRowCount()>0)
	{
		m_nEntityRecordID=m_recFullContactData->getDataRef(0,"BCNT_ID").toInt();
		ui.arSelector->Load(m_nEntityRecordID,BUS_CM_CONTACT);
		QString strName=MainEntityCalculatedName::GetCalculatedName(BUS_CM_CONTACT,*m_recFullContactData,0);
		ui.arSelector->SetRecordDisplayName(strName);
	}
	else
	{
		m_nEntityRecordID=-1;
		ui.arSelector->Invalidate();
	}
	
}
void QC_AccessRightsFrame::GetUarListsForSave(DbRecordSet &lstUAR,DbRecordSet &lstGAR)
{
	ui.arSelector->Save(m_nEntityRecordID,BUS_CM_CONTACT,&lstUAR,&lstGAR); //get ar rows for save
}




/*
void QC_AccessRightsFrame::SetDataToRecordSet(int nValue)
{
	//m_recFullContactData->setData(0, "BCNT_ACCESS", nValue);
}

void QC_AccessRightsFrame::on_private_radioButton_toggled(bool checked)
{
	SetDataToRecordSet(0);
}	

void QC_AccessRightsFrame::on_otherscanview_radioButton_toggled(bool checked)
{
	SetDataToRecordSet(1);
}

void QC_AccessRightsFrame::on_otherscanwrite_radioButton_toggled(bool checked)
{
	SetDataToRecordSet(2);
}
*/