#include "calendarverticalheaderitem.h"

#include <QString>
#include <QToolTip>

#include "calendartableview.h"

CalendarVerticalHeaderItem::CalendarVerticalHeaderItem(int nRow, int nHeaderHeight, int nWidth, int nHeight, int nTimeScale, CalendarGraphicsView *pCalendarGraphicsView, QGraphicsProxyWidget *pCalendarTableView, QGraphicsItem *parent /*= 0*/, Qt::WindowFlags wFlags /*= 0*/)
	: QGraphicsProxyWidget(parent, wFlags)
{
	m_pCalendarGraphicsView = pCalendarGraphicsView;
	m_pCalendarTableView	= pCalendarTableView;
	m_nRow					= nRow;
	m_nWidth				= nWidth;
	m_nHeight				= nHeight;
	m_nTimeScale			= nTimeScale;
	m_nHeaderHeight			= nHeaderHeight;

	m_pLabel = new Label(Label::VERTICAL_ITEM);
	m_pLabel->setAlignment(Qt::AlignCenter);
	m_pLabel->setMinimumWidth(m_nWidth);
	m_pLabel->setMaximumWidth(m_nWidth);
	m_pLabel->setMinimumHeight(m_nHeight);
	m_pLabel->setMaximumHeight(m_nHeight);

	setAcceptHoverEvents(true);
	setToolTip("fgsdfgsdgsd");

	setWidget(m_pLabel);
	setZValue(6);

	//connect(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView), SIGNAL(UpdateCalendar()), this, SLOT(on_UpdateCalendar()));
	connect(m_pCalendarGraphicsView, SIGNAL(UpdateCalendar()), this, SLOT(on_UpdateCalendar()));
}

CalendarVerticalHeaderItem::~CalendarVerticalHeaderItem()
{
	m_pCalendarGraphicsView = NULL;
	m_pCalendarTableView	= NULL;
	m_pLabel				= NULL;
}

void CalendarVerticalHeaderItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget /*= 0*/)
{
	QGraphicsProxyWidget::paint(painter, option, widget);

	painter->setPen(Qt::white);
	if (m_nRow%m_nTimeScale)
	{
		int nLeading=0;
		if ((m_nRow%m_nTimeScale)==1)
		{
			if ((m_nRow/m_nTimeScale)>=10)
			{
				nLeading=53;
			}
			else
			{
				nLeading=45;
			}
		}
		painter->drawLine(option->rect.topLeft().x()+nLeading, option->rect.topLeft().y(), option->rect.topRight().x()-7, option->rect.topRight().y());
	}
	else
	{
		painter->drawLine(option->rect.topLeft(), option->rect.topRight());
		QRect rectBounding;
		int nHourVerticalOffSet = 2;
		int nHourLeading = 35;
		int nMinutesLeading = 2;
		//Draw hour.
		QFont big_font;
		big_font.setPointSize(11);
		big_font.setStyleStrategy(QFont::PreferAntialias);
		painter->setFont(big_font);
		painter->drawText(option->rect.x()+nHourLeading, option->rect.y()+nHourVerticalOffSet, option->rect.width(), option->rect.height(), Qt::AlignLeft | Qt::AlignTop, QVariant(m_nRow/m_nTimeScale).toString(), &rectBounding);
		//Draw oo.
		QFont little_font;
		little_font.setPointSize(6);
		painter->setFont(little_font);
		painter->drawText(option->rect.x()+nHourLeading+nMinutesLeading+rectBounding.width(), option->rect.y()+nHourVerticalOffSet, option->rect.width(), option->rect.height(), Qt::AlignLeft | Qt::AlignTop, "00");
	}
}

void CalendarVerticalHeaderItem::on_UpdateCalendar()
{
	QRect rect = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->getItemRectangleForVertHeader(m_nRow);
	//resize(m_nWidth, m_nHeight);
	setPos(0, rect.topLeft().y()+m_nHeaderHeight);
}
