#include "languagewarningdlg.h"

LanguageWarningDlg::LanguageWarningDlg(QString strUL, QString strDL, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	ui.labelMsg->setText(QString("Your preferred language %1 is currently not the default language of the software.").arg(strUL));
	ui.radContinue->setText(QString("Continue with the actual language %1").arg(strDL));
	ui.radSwitch->setText(QString("Switch your language %1. You will have to login again.").arg(strUL));
}

LanguageWarningDlg::~LanguageWarningDlg()
{

}
