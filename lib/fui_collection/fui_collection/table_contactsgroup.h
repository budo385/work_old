#ifndef TABLE_CONTACTSGROUP_H
#define TABLE_CONTACTSGROUP_H

#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>
#include <QSplitter>
#include "bus_client/bus_client/selection_gridviews.h"
#include "gui_core/gui_core/universaltablewidgetex.h"
#include "bus_client/bus_client/selection_grouptree.h"
#include "common/common/observer_ptrn.h"
#include "table_subcontactdata.h"

//content of contact group
//it can change main cache, or it can get changes from it
class Table_ContactsGroup : public UniversalTableWidgetEx, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Table_ContactsGroup(QWidget *parent);
	~Table_ContactsGroup();

	void Initialize(QWidget *pContactFUI,Selection_GroupTree* pSelectionGrouTree,QComboBox *pComboViews,QPushButton *btnEditView,Table_SubContactData *pTableSub,QSplitter *splitterSub,QPushButton *btnRefresh,QLabel *labelGroupName,QCheckBox *pbtnShowActive);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void GetSelectedContacts(DbRecordSet &lstContacts);
signals:
	void ShowContactDetails(int);
public slots:
	void OnRefreshContent();
	void RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);
private slots:
	void OnDataDroped(int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);
	void OpenViewEditor();
	void on_ViewChanged(int nIndex);
	void OnRefreshActive();
	void OnSelectionChanged();
	void OnShowDetails();
	void OnShowDetailsNewWindow();
	void OnDelete();
	void OnRemoveContactsAll();
	void OnRemoveContacts();
	void OnAddContactsToActual();
	void OnEnterPeriod();

protected:
	void Data2CustomWidget(int nRow, int nCol);

private:
	void ReadGroupContent(DbRecordSet lstGroupsToRead);

	QComboBox *m_pComboViews;
	QPushButton *m_btnEditView;
	Table_SubContactData *m_pTableSub;
	QSplitter *m_splitterSub;
	QWidget *m_pContactFUI;
	QPushButton *m_btnRefresh;
	QLabel *m_pLabelGroupName;
	QCheckBox *m_pbtnShowActive;
	DbRecordSet m_lstData;
	int m_nGridID;
	Selection_GridViews m_ComboHandler;			//super handler: connected to cache
	DbRecordSet m_lstSubListData;
	Selection_GroupTree* m_pSelectionGrouTree;
	int m_nCurrentGroupID;
	int m_bIsCurrentGroupCumulated;
	DbRecordSet m_lstCurrentGroups;
	QString m_strHtmlTag;
	QString m_strEndHtmlTag;
};

#endif // TABLE_CONTACTSGROUP_H
