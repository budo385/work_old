#include "options_calendar.h"
#include "bus_core/bus_core/contacttypemanager.h"

options_calendar::options_calendar(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent /*= 0*/)
	: SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

	QString strStartDailyWorkTime=GetSettingValue(CALENDAR_START_DAILY_WORK_TIME).toString();
	ui.startDailyWorkTime_timeEdit->setTime(QTime::fromString(strStartDailyWorkTime));

	QString strEndDailyWorkTime=GetSettingValue(CALENDAR_END_DAILY_WORK_TIME).toString();
	ui.endDailyWorkTime_timeEdit->setTime(QTime::fromString(strEndDailyWorkTime));
	
	int preliminaryID=GetSettingValue(PRESENCE_TYPE_PRELIMINARY).toInt();
	int finalID=GetSettingValue(PRESENCE_TYPE_FINAL).toInt();
	int availableID=GetSettingValue(PRESENCE_TYPE_AVAILABLE).toInt();

	//----------------------------------------
	//Preliminary presence.
	//----------------------------------------
	ui.preliminary_framePresenice->Initialize(ENTITY_BUS_CM_TYPES); 
	ui.preliminary_framePresenice->GetSelectionController()->GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_CALENDAR_PRESENCE);
	ui.preliminary_framePresenice->GetSelectionController()->ReloadData(); //activate cache
	ui.preliminary_framePresenice->SetEditMode(true);

	//set to predefined value: get list search by BCMT_TYPE_NAME then get ID from fld: BCMT_ID
	//DbRecordSet *lstPresenceTypes = ui.preliminary_framePresenice->GetSelectionController()->GetDataSource();
	//set value:
	if(preliminaryID)
		ui.preliminary_framePresenice->SetCurrentEntityRecord(preliminaryID);//"BCEP_PRESENCE_TYPE_ID"
	else
		ui.preliminary_framePresenice->Clear();
	ui.preliminary_framePresenice->RefreshDisplay();


	//----------------------------------------
	//Final presence.
	//----------------------------------------
	ui.final_framePresenice->Initialize(ENTITY_BUS_CM_TYPES); 
	ui.final_framePresenice->GetSelectionController()->GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_CALENDAR_PRESENCE);
	ui.final_framePresenice->GetSelectionController()->ReloadData(); //activate cache
	ui.final_framePresenice->SetEditMode(true);

	//set to predefined value: get list search by BCMT_TYPE_NAME then get ID from fld: BCMT_ID
	//DbRecordSet *lstPresenceTypes = ui.final_framePresenice->GetSelectionController()->GetDataSource();
	//set value:
	if(finalID)
		ui.final_framePresenice->SetCurrentEntityRecord(finalID);//"BCEP_PRESENCE_TYPE_ID"
	else
		ui.final_framePresenice->Clear();
	ui.final_framePresenice->RefreshDisplay();

	//----------------------------------------
	//Available presence.
	//----------------------------------------
	ui.available_framePresenice->Initialize(ENTITY_BUS_CM_TYPES); 
	ui.available_framePresenice->GetSelectionController()->GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_CALENDAR_PRESENCE);
	ui.available_framePresenice->GetSelectionController()->ReloadData(); //activate cache
	ui.available_framePresenice->SetEditMode(true);

	//set to predefined value: get list search by BCMT_TYPE_NAME then get ID from fld: BCMT_ID
	//DbRecordSet *lstPresenceTypes = ui.available_framePresenice->GetSelectionController()->GetDataSource();
	//set value:
	if(availableID)
		ui.available_framePresenice->SetCurrentEntityRecord(availableID);//"BCEP_PRESENCE_TYPE_ID"
	else
		ui.available_framePresenice->Clear();
	ui.available_framePresenice->RefreshDisplay();


	//----------------------------------------
	//Email
	//----------------------------------------
	QString strEmail=GetSettingValue(CALENDAR_EMAIL_CONFIRMATION_ADDRESS).toString();
	ui.txtEmailConf->setText(strEmail);
}

options_calendar::~options_calendar()
{

}

DbRecordSet options_calendar::GetChangedValuesRecordSet()
{
	QString startTime=ui.startDailyWorkTime_timeEdit->time().toString();
	QString endTime=ui.endDailyWorkTime_timeEdit->time().toString();
	SetSettingValue(CALENDAR_START_DAILY_WORK_TIME, startTime); //could be empty....
	SetSettingValue(CALENDAR_END_DAILY_WORK_TIME, endTime); //could be empty....

	QString strEmail=ui.txtEmailConf->text();
	SetSettingValue(CALENDAR_EMAIL_CONFIRMATION_ADDRESS,strEmail);


	int preliminaryID;
	int finalID;
	int availableID;

	//----------------------------------------
	//Preliminary presence.
	//----------------------------------------
	int nID1;
	DbRecordSet row;
	if(ui.preliminary_framePresenice->GetCurrentEntityRecord(nID1,row)) //return false if not set (NULL)
		preliminaryID = nID1;
	else
		preliminaryID= -1;

	SetSettingValue(PRESENCE_TYPE_PRELIMINARY, preliminaryID); //could be empty....

	//----------------------------------------
	//Final presence.
	//----------------------------------------
	int nID2;
	DbRecordSet row1;
	if(ui.final_framePresenice->GetCurrentEntityRecord(nID2,row1)) //return false if not set (NULL)
		finalID = nID2;
	else
		finalID = -1;

	SetSettingValue(PRESENCE_TYPE_FINAL, finalID); //could be empty....

	//----------------------------------------
	//Available presence.
	//----------------------------------------
	int nID3;
	DbRecordSet row3;
	if(ui.available_framePresenice->GetCurrentEntityRecord(nID3,row3)) //return false if not set (NULL)
		availableID = nID3;
	else
		availableID = -1;

	SetSettingValue(PRESENCE_TYPE_AVAILABLE, availableID); //could be empty....

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}
