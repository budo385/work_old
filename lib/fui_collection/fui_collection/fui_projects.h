#ifndef FUI_PROJECTS_H
#define FUI_PROJECTS_H

#include <QWidget>
#include "generatedfiles/ui_fui_projects.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "gui_core/gui_core/styledpushbutton.h"



class FUI_Projects : public FuiBase
{
    Q_OBJECT

public:
    FUI_Projects(QWidget *parent = 0, bool bOpenedAsAvatar = false);
    ~FUI_Projects();

	enum DetailTabs //tabContactGroups
	{
		TAB_DETAIL_BUDGET=0,
		TAB_DETAIL_DATE,
		TAB_DETAIL_LOCATION,
		TAB_DETAIL_DESC,
		TAB_DETAIL_RIGHT,
		//TAB_DETAIL_CUSTOM_FLDS,
		TAB_DETAIL_RESERVATIONS
	};
	enum MainTabs //tabWidgetContact
	{
		TAB_MAIN_COMM=0,
		TAB_MAIN_DETAILS
	};
	enum CommunicationTabs //tabWidgetComm
	{
		TAB_COMM_OVERVIEW=0,
		TAB_COMM_USERS_CONTACTS,
	};

	bool on_cmdEdit();
	void on_cmdCopy();
	void on_cmdInsert();
	bool on_cmdOK();
	bool on_cmdDelete();

	//fur hierarchy:
	void on_NewFromTemplate(bool bCopyCE = true);
	void on_NewStructFromTemplate();

	void SetFiltersForCurrentActualOrganization(int nActualOrganizationID);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	
	QString GetFUIName();
	void on_selectionChange(int nEntityRecordID);
	void SetCurrentTab(int nTabPane, int nDetailTab=0);
	void GetCurrentTab(int &nTabPane, int &nDetailTab, int &nCommTab);
	CommunicationMenu_Base* GetComMenu(){return m_p_CeMenu;};

	void ReloadFromAvatar();

	bool m_bHideOnMinimize;

public slots:
	void OnCEMenu_NeedNewData(int);
	void OnCEMenu_NewDataDropped(int,DbRecordSet);

protected:
	virtual void changeEvent (QEvent * event);
	virtual void closeEvent(QCloseEvent *event);
	void DataRead(Status &err,int nRecordID);
	void DataWrite(Status &err);
	void DataPrepareForInsert(Status &err);
	void DataCheckBeforeWrite(Status &err);
	void DataPrepareForCopy(Status &err);
	void DataClear();
	void DataDefine();
	void DataUnlock(Status &err);


private:
	void RefreshDisplay();
	void SetMode(int nMode);
	void prepareCacheRecord(DbRecordSet *recNewData);
	void SaveCommGridChBoxFilterState(int nState, int nSettingID);

	Selection_TreeBased* m_p_Selector;
	CommunicationMenu_Base* m_p_CeMenu;
	GuiFieldManager *m_GuiFieldManagerMain;
	DbRecordSet m_lstCustomFields;

private slots:
	void OnNMRXContactsChanged();
	void On_SelectionControllerSideBarDestroyed();
	void OnSetFindFocusDelayed();
	void OnBtnCalendar_Click();
	void OnTabWidgetData_currentChanged(int);

private:
    Ui::FUI_ProjectsClass ui;
	QString m_strOldCode;								//<store old code before EDIT
	bool m_bOpenedAsAvatar;
	StyledPushButton *m_pCalendarButton;
};

#endif // FUI_PROJECTS_H
