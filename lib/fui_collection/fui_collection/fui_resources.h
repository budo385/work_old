#ifndef FUI_RESOURCES_H
#define FUI_RESOURCES_H

#include <QWidget>
#include "ui_fui_resources.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "gui_core/gui_core/styledpushbutton.h"

class fui_resources : public FuiBase
{
	Q_OBJECT

public:
	fui_resources(QWidget *parent = 0);
	~fui_resources();

	QString GetFUIName();

protected:
	void DataWrite(Status &err);
	//void DataPrepareForInsert(Status &err);
	void DataPrepareForCopy(Status &err);
	void DataClear();
	//void DataDefine();
	void DataRead(Status &err,int nRecordID);
	void DataUnlock(Status &err);
	void DataCheckBeforeWrite(Status &err);

private slots:
	void on_ckbShowAll_stateChanged(int);
	void OnBtnCalendar_Click();

protected:
	GuiFieldManager *m_GuiFieldManagerMain;

private:
	void prepareCacheRecord(DbRecordSet *recNewData);
	void SetFiltersForCurrentActualOrganization(int nActualOrganizationID);
	void RefreshDisplay();
	void SetMode(int nMode);
	Ui::fui_resourcesClass ui;
	StyledPushButton *m_pCalendarButton;
};

#endif // FUI_RESOURCES_H
