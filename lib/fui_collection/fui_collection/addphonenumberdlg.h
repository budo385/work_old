#ifndef ADDPHONENUMBERDLG_H
#define ADDPHONENUMBERDLG_H

#include <QtWidgets/QDialog>
#include "ui_addphonenumberdlg.h"

class AddPhoneNumberDlg : public QDialog
{
	Q_OBJECT

public:
	AddPhoneNumberDlg(QString strNumber, QStringList lstPhoneTypes, int nSelectTypeIdx, QWidget *parent = 0);
	~AddPhoneNumberDlg();

	QString GetNumber();
	int		GetTypeIdx();

private slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();
	void on_btnReformat_clicked();

private:
	Ui::AddPhoneNumberDlg ui;
};

#endif // ADDPHONENUMBERDLG_H
