#include "commgridview.h"
#include <QDateTime>
#include <QHeaderView>
#include <QPainter>
#include <QAbstractItemDelegate>
#include <QDrag>
#include <QMimeData>
#include "os_specific/os_specific/mime_types.h"
#include "common/common/datahelper.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/globalconstants.h"
#include "communicationmanager.h"
#include <QHeaderView>
#include <QBoxLayout>
#include <QLabel>
#include <QFont>
#include <QColor>
#include <QToolTip>
#include "fui_importemail.h"
#include "fui_importcontact.h"
#include "fui_voicecallcenter.h"
#include "fui_importappointment.h"
#include "bus_client/bus_client/documenthelper.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"
#include "commworkbenchwidget.h"
#include <QTime>
#include "gui_core/gui_core/macros.h"

//GLOBALS:
 
#include "bus_client/bus_client/clientcontactmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "gui_core/gui_core/thememanager.h"
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher

void FirstColumnDelegateSideBarView::drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	//painter->setBrush(index.data(Qt::BackgroundRole).value<QBrush>());
	//painter->setPen(QColor(255,255,255));
	//painter->drawLine(QPoint(0,0),QPoint(0,50));
}

void FirstColumnDelegateSideBarView::drawDecoration ( QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QPixmap &pixmap) const
{
	painter->drawPixmap(rect.topLeft().x() + 1, rect.topLeft().y() + 2, pixmap);
	//issue 1685:
	painter->setPen(QColor(255,255,255));
	painter->drawLine(rect.bottomLeft().x()-3,rect.bottomLeft().y()+6,rect.bottomRight().x()+3,rect.bottomRight().y()+6);
}

void FirstColumnDelegateSideBarView::drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const
{
	QPen pen = painter->pen();
	QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
		? QPalette::Normal : QPalette::Disabled;
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(rect, option.palette.brush(cg, QPalette::Highlight));
		painter->setPen(option.palette.color(cg, QPalette::HighlightedText));
	} else {
		painter->setPen(option.palette.color(cg, QPalette::Text));
	}

	QFont font = option.font;
	font.setFamily("Arial Narrow");
	font.setPointSize(8);
	//Issue #2009.
	//if(QSysInfo::windowsVersion()==QSysInfo::WV_VISTA)
		font.setStyleStrategy(QFont::PreferAntialias);
	font.setBold(true);
	font.setItalic(true);
	painter->setFont(font);

	QStringList lstDisplay;
	lstDisplay = text.split("/|/");
	QString strOriginator = lstDisplay.value(1);
	QString strTaskPriority = lstDisplay.value(2);
	int nTaskPriority = QVariant(strTaskPriority).toInt();
	QString strTaskColor	= lstDisplay.value(3);
	QString strOwner = lstDisplay.value(4);

	QString strTitle = DataHelper::ReturnCommGridTextFromHTML(lstDisplay.value(0));

	QStringList lstSplitedTitle = strTitle.split("\n");
	QStringList lstTmp;
	foreach(QString strTmp, lstSplitedTitle)
	{
		if (!strTmp.isEmpty())
			lstTmp << strTmp;
	}
	strTitle = lstTmp.join(" ");

	if (nTaskPriority < 0)
		painter->drawText(rect.topLeft().x() + 2, rect.topLeft().y() + 14, strTitle);
	else
		painter->drawText(rect.topLeft().x() + 10, rect.topLeft().y() + 14, strTitle);
	font.setBold(false);
	font.setItalic(false);
	painter->setFont(font);
	if (nTaskPriority < 0)
		painter->drawText(rect.topLeft().x() + 2, rect.topLeft().y() + 32, strOriginator);
	else
		painter->drawText(rect.topLeft().x() + 10, rect.topLeft().y() + 32, strOriginator);

	//Draw task priority and color - only if task priority set.
	if (nTaskPriority >= 0)
	{
		if (!strTaskColor.isEmpty())
		{
			QColor taskColor(strTaskColor);

			painter->setPen(taskColor);
			painter->setBrush(taskColor);
			painter->drawRect(rect.topLeft().x(), rect.topLeft().y(), 7, rect.height()/3);
			painter->drawRect(rect.topLeft().x(), rect.topLeft().y() + rect.height()/3*2+1, 7, rect.height()/3-1);
		}

		painter->setPen(Qt::yellow);
		painter->drawText(rect.topLeft().x()+1, rect.topLeft().y() + rect.height()/3*2-2, strTaskPriority);
	}
}

void SecondColumnDelegateSideBarView::drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	//painter->setBrush(index.data(Qt::BackgroundRole).value<QBrush>());
}

void SecondColumnDelegateSideBarView::drawDecoration ( QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QPixmap &pixmap) const
{
	//painter->drawPixmap(rect.topLeft().x() + 1, rect.topLeft().y() + 2, pixmap);
}

void FirstColumnDelegate::drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const
{
	//QFont font = painter->font();
	QPen pen = painter->pen();
	QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
		? QPalette::Normal : QPalette::Disabled;
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(rect, option.palette.brush(cg, QPalette::Highlight));
		painter->setPen(option.palette.color(cg, QPalette::HighlightedText));
	} else {
		painter->setPen(option.palette.color(cg, QPalette::Text));
	}

	QFont font = option.font;
	painter->setFont(font);

	QStringList lstDisplay;
	lstDisplay = text.split("/|/");

	painter->drawText(rect.topLeft().x() + 4, rect.topLeft().y() + 11, lstDisplay.value(0));
	painter->drawText(rect.topLeft().x() + 4, rect.topLeft().y() + 24, lstDisplay.value(1));
	painter->drawText(rect.topLeft().x() + 4, rect.topLeft().y() + 36, lstDisplay.value(2));
	//issue 1685:
	//painter->setPen(QColor(255,255,255));
	//painter->drawLine(rect.bottomLeft(),rect.bottomRight());
}

void FirstColumnDelegate::drawBackground(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	painter->setBrush(index.data(Qt::BackgroundRole).value<QBrush>());
}
void FirstColumnDelegate::drawDecoration(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QPixmap &pixmap) const
{
	painter->drawPixmap(rect.topLeft().x() + 2, rect.topLeft().y() + 2, pixmap);
	//issue 1685:
	//painter->setPen(QColor(255,255,255));
	//painter->drawLine(rect.bottomLeft().x()-3,rect.bottomLeft().y()+6,rect.bottomRight().x()+3,rect.bottomRight().y()+6);
}

void SecondColumnDelegate::drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const
{
	QStringList fullList = text.split("/;/");
	QStringList lstDisplay;
	lstDisplay = fullList.value(0).split("/|/");
	QString strTaskPriority = fullList.value(1);
	int nTaskPriority = QVariant(strTaskPriority).toInt();
	QString strTaskColor	= fullList.value(2);
	
	QStringList lstOut;

	//Loop.
	foreach(QString strData, lstDisplay)
	{
		lstOut << DataHelper::ReturnCommGridTextFromHTML(strData);

/*		QString strTmp = strData;
		QRegExp rx2("<[^>]*>");
		if(strTmp.startsWith("<html") || strTmp.startsWith("<!DOCTYPE") || strTmp.contains(rx2))
		{
			//QTextEdit edit;
			//edit.setHtml(strTmp);
			//lstOut << edit.toPlainText();

			lstOut << DataHelper::ExtractTextFromHTML(strTmp);
		}
		else
			lstOut << strTmp;*/
	}

	//Make display string (subject separated with new line from description - in plain text).
	QString strDisplay;
	if (!lstOut.value(0).isEmpty())
		strDisplay = lstOut.value(0) + "\n" + lstOut.value(1);
	else
		strDisplay = lstOut.value(1);

	//QFont font = painter->font();
	QPen pen = painter->pen();
	QPalette::ColorGroup cg = option.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(rect, option.palette.brush(cg, QPalette::Highlight));
		painter->setPen(option.palette.color(cg, QPalette::HighlightedText));
	} else {
		painter->setPen(option.palette.color(cg, QPalette::Text));
	}

	QFont font = option.font;
	painter->setFont(font);

	//Make rectangle 4px or 20px smaller than the item original (make a little horizontal spacing in cell).
	QPoint topLeftPoint;
	if (nTaskPriority < 0)
		topLeftPoint = QPoint(rect.topLeft().x() + 4, rect.topLeft().y());
	else
		topLeftPoint = QPoint(rect.topLeft().x() + 20, rect.topLeft().y());
	QRect newRect(topLeftPoint, rect.bottomRight());
	painter->drawText(newRect, Qt::TextWrapAnywhere, strDisplay);

	//Draw task priority and color - only if task priority set.
	if (nTaskPriority >= 0)
	{
		if (!strTaskColor.isEmpty())
		{
			QColor taskColor(strTaskColor);
			
			painter->setPen(taskColor);
			painter->setBrush(taskColor);
			painter->drawRect(rect.topLeft().x(), rect.topLeft().y(), 9, rect.height()/3-2);
			painter->drawRect(rect.topLeft().x(), rect.topLeft().y() + rect.height()/3*2+2, 9, rect.height()/3-2);
		}

		painter->setPen(Qt::yellow);
		painter->drawText(rect.topLeft().x()+2, rect.topLeft().y() + rect.height()/3*2-2, strTaskPriority);
	}
}

void SecondColumnDelegate::drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	painter->setBrush(index.data(Qt::BackgroundRole).value<QBrush>());
}

void ThirdColumnDelegate::drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const
{
	//QFont font = painter->font();
	QPen pen = painter->pen();
	QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
		? QPalette::Normal : QPalette::Disabled;
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(rect, option.palette.brush(cg, QPalette::Highlight));
		painter->setPen(option.palette.color(cg, QPalette::HighlightedText));
	} else {
		painter->setPen(option.palette.color(cg, QPalette::Text));
	}

	QFont font = option.font;
	font.setBold(true);

	painter->setFont(font);

	painter->drawText(rect.topLeft().x() + 4, rect.topLeft().y() + 11, m_strOwnerString);
	painter->drawText(rect.topLeft().x() + 4, rect.topLeft().y() + 24, m_strContactString);
	painter->drawText(rect.topLeft().x() + 4, rect.topLeft().y() + 36, m_strProjectString);

	font.setBold(false);
	painter->setFont(font);

	QStringList lstDisplay;
	lstDisplay = text.split("/|/");

	painter->drawText(rect.topLeft().x() + 65, rect.topLeft().y() + 11, lstDisplay.value(0));
	painter->drawText(rect.topLeft().x() + 65, rect.topLeft().y() + 24, lstDisplay.value(1));
	painter->drawText(rect.topLeft().x() + 65, rect.topLeft().y() + 36, lstDisplay.value(2));
	//issue 1685:
	//painter->setPen(QColor(255,255,255));
	//painter->drawLine(rect.bottomLeft(),rect.bottomRight());
}

void ThirdColumnDelegate::drawBackground(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	painter->setBrush(index.data(Qt::BackgroundRole).value<QBrush>());
}


CommGridView::CommGridView(QWidget *parent)
	: QTableView(parent)
{
	setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	m_nRowCount = 0;
	m_bFirstTimeIn = true;

	setDragEnabled(true);
	setAcceptDrops(true);
	setContextMenuPolicy(Qt::DefaultContextMenu);
	CreateContextMenuActions(m_lstActions);
	
	//Set side bar mode.
	m_bSideBarMode = false;
	if (parent)
	{
		if (dynamic_cast<CommWorkBenchWidget*>(parent)->IsSidebarMode())
			m_bSideBarMode = true;
		else if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
			m_bSideBarMode = true;
	}
	else //If called from report wizard then the parent is NULL.
	{
		if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
			m_bSideBarMode = true;
	}

	//Make model.
	m_pGridModel = new CommGridModel(m_bSideBarMode);

	//Check is it in side bar mode.
	if (m_bSideBarMode)
	{
		setIconSize(QSize(4*16+12, 32));
		//Delegates.
		QAbstractItemDelegate *delegate0 = new FirstColumnDelegateSideBarView();
		//QAbstractItemDelegate *delegate1 = new SecondColumnDelegateSideBarView();
		setItemDelegateForColumn(0, delegate0);
		//setItemDelegateForColumn(1, delegate1);
		//Set row height.
		verticalHeader()->setDefaultSectionSize(43); 
		setShowGrid(false); //issue 1685 (BT)
	}
	else
	{
		setIconSize(QSize(4*32, 32));
		//Delegates.
		QAbstractItemDelegate *delegate0 = new FirstColumnDelegate();
		QAbstractItemDelegate *delegate1 = new SecondColumnDelegate();
		QAbstractItemDelegate *delegate2 = new ThirdColumnDelegate();
		setItemDelegateForColumn(0, delegate0);
		setItemDelegateForColumn(1, delegate1);
		setItemDelegateForColumn(2, delegate2);
		//Set row height.
		verticalHeader()->setDefaultSectionSize(43); 
	}

	

	if (m_bSideBarMode)
	{
		//Some layout.
		verticalHeader()->setSelectionBehavior(QAbstractItemView::SelectRows);
		verticalHeader()->setSelectionMode(QAbstractItemView::ContiguousSelection);
		setShowGrid(true);
	}
	else
	{
		//Some layout.
		verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
		verticalHeader()->setSelectionBehavior(QAbstractItemView::SelectRows);
		verticalHeader()->setSelectionMode(QAbstractItemView::ContiguousSelection);
		setShowGrid(true);
	}

	//Hide header for sidebar mode.
	if (m_bSideBarMode)
	{
		verticalHeader()->hide();
		horizontalHeader()->hide();
	}

	setStyleSheet(ThemeManager::GetTableViewBkg());

	//Connect signals.
	connect(this, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(on_CellDoubleClicked(const QModelIndex&)));

	horizontalHeader()->setStretchLastSection(true);
}

CommGridView::~CommGridView()
{
	m_recEmail		= NULL;
	m_recVoiceCall	= NULL;
	m_recDocument	= NULL;
	
	delete(m_pGridModel);
	m_pGridModel = NULL;
}

void CommGridView::Initialize(DbRecordSet *recEmails, DbRecordSet *recDocuments, DbRecordSet *recVoiceCalls, DbRecordSet *lstSortRecordSet, int nGridType /*= false*/, bool bClearSelection /*= true called when data edit with false*/, bool bRowsStripped /*= false*/)
{
	if(bRowsStripped)
	{
		QMessageBox::information(NULL, "", QObject::tr("Too many items found. List restricted to 4000 items. Please redefine your search!"));
	}
	_START_TIMER(view_initialize);
	//Are we in contact fui.
	m_nGridType=nGridType;

	setDragEnabled(true);
	setAcceptDrops(true);

	//Clear hashes.
	m_tableHelper.ClearHashes();

	//recEmails->Dump();

	//Locals;
	m_recEmail		= recEmails;
	m_recVoiceCall	= recVoiceCalls;
	m_recDocument	= recDocuments;

	//m_recEmail->Dump();
	//m_recDocument->Dump();
	//m_recVoiceCall->Dump();

	//Get row counts.
	int nRowCountEmails = recEmails->getRowCount();
	int nRowCountVoiceCalls = recVoiceCalls->getRowCount();
	int nRowCountDocuments = recDocuments->getRowCount();
	m_nRowCount = nRowCountEmails + nRowCountVoiceCalls + nRowCountDocuments;
	
	int nRowsInGrid = 0;
	for (int i = 0; i < nRowCountEmails; ++i)
	{
		//Fill model recordset.
		FirstColumnData(CommGridViewHelper::EMAIL, nRowsInGrid, *m_recEmail,i);
		SecondColumnData(CommGridViewHelper::EMAIL, *m_recEmail,i, nRowsInGrid);
		ThirdColumnData(CommGridViewHelper::EMAIL, nRowsInGrid, *m_recEmail,i);
		
		//Fill data and sorting hashes.
		m_tableHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::EMAIL, *m_recEmail,i);
		
		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountVoiceCalls; ++i)
	{
		//Fill model recordset.
		FirstColumnData(CommGridViewHelper::VOICECALL, nRowsInGrid, *m_recVoiceCall,i);
		SecondColumnData(CommGridViewHelper::VOICECALL, *m_recVoiceCall,i, nRowsInGrid);
		ThirdColumnData(CommGridViewHelper::VOICECALL, nRowsInGrid,*m_recVoiceCall,i);

		//Fill data and sorting hashes.
		m_tableHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::VOICECALL, *m_recVoiceCall,i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountDocuments; ++i)
	{
		FirstColumnData(CommGridViewHelper::DOCUMENT, nRowsInGrid, *m_recDocument,i);
		SecondColumnData(CommGridViewHelper::DOCUMENT, *m_recDocument,i, nRowsInGrid);
		ThirdColumnData(CommGridViewHelper::DOCUMENT, nRowsInGrid, *m_recDocument,i);

		//Fill data and sorting hashes.
		m_tableHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::DOCUMENT, *m_recDocument,i);

		//m_recDocument->Dump();

		//Increase row number.
		nRowsInGrid++;
	}
	_STOP_TIMER(view_initialize);
	
	//Sort table, initialize and set model.
	SortTable(lstSortRecordSet, bClearSelection);
}

void CommGridView::RefreshContactItem(int nCEType, DbRecordSet &recEntityRecord)
{
	//Get selected row.
	DbRecordSet tmp = recEntityRecord.getSelectedRecordSet();
	QList<int> lstSelectedRows;

	int nRowCount = tmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		//Fill hashes with new values.
		int nCENT_ID = tmp.getDataRef(i, "CENT_ID").toInt();
		int nBPER_ID = tmp.getDataRef(i, "A.BPER_ID").toInt();
		int nBCNT_ID = tmp.getDataRef(i, "BCNT_ID").toInt();
		int nBUSP_ID = tmp.getDataRef(i, "BUSP_ID").toInt();

		//Find corresponding row and update contact.
		int nRowInGrid = m_tableHelper.m_hshRow2CENT_ID.key(nCENT_ID);
		m_tableHelper.m_hshRow2BPER_ID.insert(nRowInGrid, nBPER_ID);
		m_tableHelper.m_hshRow2BCNT_ID.insert(nRowInGrid, nBCNT_ID);
		m_tableHelper.m_hshRow2BUSP_ID.insert(nRowInGrid, nBUSP_ID);

		//Refresh item display.
		ThirdColumnData(nCEType, nRowInGrid, tmp, i);

		//Fill changed rows list.
		lstSelectedRows << nRowInGrid;
	}

	//If in side bar mode there is no third column so don't refresh.
	if (m_bSideBarMode)
		return;

	m_pGridModel->RefreshItems(lstSelectedRows, m_tableHelper.m_hshRow2Owner, m_tableHelper.m_hshRow2Contact, m_tableHelper.m_hshRow2Project);
}

//Sort table - need to reorder the hashes.
void CommGridView::SortTable(DbRecordSet *recSortRecordSet /*= NULL*/, bool bClearSelection /*= false*/ , bool bForceSort /*= true*/)
{
	m_tableHelper.SortTableData(m_nRowCount, m_nGridType, recSortRecordSet, bForceSort);

	//Create model.
	DbRecordSet recApplications;
	QHash<int, QPixmap> hshDocumentIcons;
	DocumentHelper::GetDocApplications(recApplications, true);
	hshDocumentIcons = DocumentHelper::GetDocumentTypeIconHash();

	m_pGridModel->Initialize(m_recEmail, m_recVoiceCall, m_recDocument, 
							 m_tableHelper.m_hshRow2Color,
							 m_tableHelper.m_hshRow2Pix1,
							 m_tableHelper.m_hshRow2Pix2,
							 m_tableHelper.m_hshRow2Pix3,
							 m_tableHelper.m_hshRow2Pix4,
							 m_tableHelper.m_hshRow2Date1,
							 m_tableHelper.m_hshRow2Date2,
							 m_tableHelper.m_hshRow2Autor,
							 m_tableHelper.m_hshRow2Subject,
							 m_tableHelper.m_hshRow2Descr,
							 m_tableHelper.m_hshRow2SecondColDisplay,
							 m_tableHelper.m_hshRow2Owner,
							 m_tableHelper.m_hshRow2Contact,
							 m_tableHelper.m_hshRow2Project,
							 recApplications, 
							 hshDocumentIcons,
							 m_tableHelper.m_hshRow2TaskColor,
							 m_tableHelper.m_hshRow2TaskIcon,
							 m_tableHelper.m_hshRow2TaskPriority);
	
	if (bClearSelection)
		clearSelection();
	
	setModel(0);
	setModel(m_pGridModel);
	
	SetLayout();
}

void CommGridView::dragMoveEvent ( QDragMoveEvent * event )
{
	event->accept();
}

void CommGridView::dragEnterEvent ( QDragEnterEvent * event )
{
	event->accept();
	//startDrag()
}

//if TAB= template then template, else doc, if not exe...brb....
void CommGridView::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();
	//QStringList lstX=mime->formats();
		
	if (mime->hasUrls())
	{

		QList<QUrl> lst =mime->urls();
		CommWorkBenchWidget* pParent=dynamic_cast<CommWorkBenchWidget*>(this->parent());
		Q_ASSERT(pParent);
		pParent->SubMenuRequestedNewData();
		this->activateWindow(); //bring on top_:
		g_CommManager.ProcessDocumentDrop(lst,false,false);
		event->accept();
		return;
	}

	if (mime->hasFormat("application/x-qt-windows-mime;value=\"RenPrivateMessages\""))
	{
		//import emails:
		QByteArray arData = mime->data("application/x-qt-windows-mime;value=\"RenPrivateMessages\"");
		if(arData.size() > 0)
		{
			//get current project id if project fui:
			CommWorkBenchWidget* pParent=dynamic_cast<CommWorkBenchWidget*>(this->parent());
			Q_ASSERT(pParent);
			pParent->SubMenuRequestedNewData();
			int nGridType,nRecID,nProjectID=-1;
			pParent->GetGridData(nGridType,nRecID);
			if (nGridType==CommGridViewHelper::PROJECT_GRID && nRecID>0)
				nProjectID=nRecID;

			if(FUI_ImportEmail::IsDropContact(arData))
			{
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportContact* pFuiW=dynamic_cast<FUI_ImportContact*>(pFUI);
				if (pFuiW)
				{
					pFuiW->BuildImportListFromDrop(arData);
				}
				if(pFUI)pFUI->show();  //show FUI
			}
			else if(FUI_ImportEmail::IsDropAppointment(arData))
			{
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_APPOINTMENT, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportAppointment* pFuiW=dynamic_cast<FUI_ImportAppointment*>(pFUI);
				if (pFuiW)
				{
					pFuiW->BuildImportListFromDrop(arData);
				}
				if(pFUI)pFUI->show();  //show FUI
			}
			else //if(FUI_ImportEmail::IsDropEmail(arData))
			{
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_EMAIL, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportEmail* pFuiDM=dynamic_cast<FUI_ImportEmail*>(pFUI);
				if (pFuiDM)
				{
					pFuiDM->BuildImportListFromDrop(arData,false,nProjectID);
				}
				if(pFUI){
					pFUI->show();  //show FUI
					pFUI->raise();
					pFUI->activateWindow();
				}
			}

		}

		event->accept();
		return;
	}



	if (mime->hasFormat(OUTLOOK_MIME_MAIL_ID))
	{
		//import emails:
		QByteArray arData = mime->data(OUTLOOK_MIME_MAIL_ID);
		if(arData.size() > 0)
		{
			//get current project id if project fui:
			CommWorkBenchWidget* pParent=dynamic_cast<CommWorkBenchWidget*>(this->parent());
			Q_ASSERT(pParent);
			pParent->SubMenuRequestedNewData();
			int nGridType,nRecID,nProjectID=-1;
			pParent->GetGridData(nGridType,nRecID);
			if (nGridType==CommGridViewHelper::PROJECT_GRID && nRecID>0)
				nProjectID=nRecID;

			if(FUI_ImportEmail::IsDropContact(arData))
			{
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportContact* pFuiW=dynamic_cast<FUI_ImportContact*>(pFUI);
				if (pFuiW)
				{
					pFuiW->BuildImportListFromDrop(arData);
				}
				if(pFUI)pFUI->show();  //show FUI
			}
			else if(FUI_ImportEmail::IsDropAppointment(arData))
			{
				//B.T.2013-08-09, crash when appointment is dropped
				QMessageBox::information(this,tr("Warning"),tr("Appointment import is not supported!"));
				/*
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_APPOINTMENT, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportAppointment* pFuiW=dynamic_cast<FUI_ImportAppointment*>(pFUI);
				if (pFuiW)
				{
					pFuiW->BuildImportListFromDrop(arData);
				}
				if(pFUI)pFUI->show();  //show FUI
				*/
			}
			else //if(FUI_ImportEmail::IsDropEmail(arData))
			{
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_EMAIL, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportEmail* pFuiDM=dynamic_cast<FUI_ImportEmail*>(pFUI);
				if (pFuiDM)
				{
					pFuiDM->BuildImportListFromDrop(arData,false,nProjectID);
				}
				if(pFUI){
					pFUI->show();  //show FUI
					pFUI->raise();
					pFUI->activateWindow();
				}
			}

		}

		event->accept();
		return;
	}



	//------------------------------------------------
	// LIST of record->emit special signal
	//------------------------------------------------

	//internal/external
	if (mime->hasFormat(SOKRATES_MIME_LIST))
	{
		int nEntityID;
		DbRecordSet lstDropped;

		//import emails:
		QByteArray arData = mime->data(SOKRATES_MIME_LIST);
		if(arData.size() > 0)
		{
			lstDropped=XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
		}
		QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
		if(arDataType.size() > 0)
		{
			bool bOK;
			nEntityID=arDataType.toInt(&bOK);
			if (!bOK) nEntityID=-1;
		}

		if (nEntityID==ENTITY_COMM_GRID_DATA && lstDropped.getRowCount()>0)
		{
			QWidget *widget_parent=parentWidget();
			CommWorkBenchWidget *bench=dynamic_cast<CommWorkBenchWidget*>(widget_parent);
			int nGridType,nRecID;
			bench->GetGridData(nGridType,nRecID);
			if (nGridType==CommGridViewHelper::PROJECT_GRID && nRecID>0)
			{
				if(g_CommManager.DropCommDataToProject(lstDropped,nRecID))
					bench->ReloadData();
			}
		}

		if (nEntityID==ENTITY_EMAIL_ATTACHMENT && lstDropped.getRowCount()>0)
		{
			event->accept();

			DbRecordSet lstNewDocumentsCreated;
			lstNewDocumentsCreated.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));

			int nSize=lstDropped.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				int nOperation=0;
				QFileInfo info(lstDropped.getDataRef(i,"BEA_NAME").toString());
				if (info.suffix().toLower()=="zip")
				{
					int nResult = QMessageBox::question(this, "Store unzipped content of attachment", tr("Unzip attachment and store document(s)?"), tr("Yes"),tr("No"));
					if (nResult==0)
					{
						nOperation=1;
					}
				}
				int nAttachID=lstDropped.getDataRef(i,"BEA_ID").toInt();
				Status err;
				DbRecordSet lstDocId;

				//get current project or contact grid id:
				QWidget *widget_parent=parentWidget();
				CommWorkBenchWidget *bench=dynamic_cast<CommWorkBenchWidget*>(widget_parent);
				int nGridType,nRecID;
				bench->GetGridData(nGridType,nRecID);
				int nProjectID=-1;
				int nContactID=-1;
				if (nGridType==CommGridViewHelper::CONTACT_GRID)
					nContactID=nRecID;
				else if (nGridType==CommGridViewHelper::PROJECT_GRID)
					nProjectID=nRecID;

				_SERVER_CALL(BusDocuments->CheckInFromEmailAttachment(err,nAttachID,nOperation,nProjectID,nContactID,lstDocId));
				_CHK_ERR(err);

				lstNewDocumentsCreated.merge(lstDocId);
			}

			if (lstNewDocumentsCreated.getRowCount()>0) //fire global signal to refresh all grids...
			{
				QVariant varData;
				qVariantSetValue(varData,lstNewDocumentsCreated);
				g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ENTITY_BUS_DM_DOCUMENTS,varData); 
			}

			QMessageBox::information(this,tr("Information"),QString(tr("Successfully created %1 document(s)!")).arg(lstNewDocumentsCreated.getRowCount()));
			return;
		}
		event->accept();
		return;
	}
}

void CommGridView::SelectGridFromSourceRecordSet()
{
	blockSignals(true);
	//Locals.
	QList<int> lstSelectedCENT_IDs;

	//Voice calls.
	int nRowCount = m_recVoiceCall->getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		if (m_recVoiceCall->isRowSelected(i))
			lstSelectedCENT_IDs << m_recVoiceCall->getDataRef(i, "CENT_ID").toInt();

	//Emails.
	nRowCount = m_recEmail->getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		if (m_recEmail->isRowSelected(i))
			lstSelectedCENT_IDs << m_recEmail->getDataRef(i, "CENT_ID").toInt();

	//Documents.
	nRowCount = m_recDocument->getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		if (m_recDocument->isRowSelected(i))
			lstSelectedCENT_IDs << m_recDocument->getDataRef(i, "CENT_ID").toInt();

	blockSignals(false);
	
	SelectBySourceRecordSetRows(lstSelectedCENT_IDs);

	//B.T. select first item in list, this do not work!
	/*
	if (lstSelectedCENT_IDs.size()>0)
	{
	int nRowFirst=lstSelectedCENT_IDs.at(0);
	this->setCurrentCell(nRowFirst,0);
	on_CellClicked(nRowFirst,0);
	}
	*/
	//on_itemSelectionChanged(); B.T. ???
}

//B.T. improved: from row grid, select/deselect inside datasource
void CommGridView::SelectSourceRecordSetFromGrid()
{
	m_recEmail->clearSelection();
	m_recVoiceCall->clearSelection();
	m_recDocument->clearSelection();

	QList<int> lstSelectedRows;
	QModelIndexList lstSelectedIndexes = selectedIndexes();
	int nSelRowCount = lstSelectedIndexes.count();
	for (int i = 0; i < nSelRowCount; ++i)
	{
		QModelIndex index = lstSelectedIndexes.value(i);
		int nRow = index.row();
		if (lstSelectedRows.contains(nRow))
			continue;
		lstSelectedRows << nRow;
		int nEntitySysType	= m_tableHelper.m_hshRow2CENT_SYS.value(nRow);
		int nEntityID		= m_tableHelper.m_hshRow2CENT_ID.value(nRow);
		SelectDataInSourceRecordSet(nEntitySysType, nEntityID,true);
	}
}

void CommGridView::SetLayout()
{
	//if (!m_bFirstTimeIn)
	//	return;

	if (m_bSideBarMode)
	{
		//Layout only once when model is set.
//		horizontalHeader()->resizeSection(0,72);
//		horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
		horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
//		horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
//		horizontalHeader()->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
//		horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
		horizontalHeader()->setSectionsClickable(false);
		setSelectionBehavior(QAbstractItemView::SelectRows);
	}
	else
	{
		//Layout only once when model is set.
		horizontalHeader()->resizeSection(0,230);
		horizontalHeader()->resizeSection(2,360);
		horizontalHeader()->setSectionResizeMode(0, QHeaderView::Interactive);
		horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
		horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);
		//horizontalHeader()->hide();

		//For first time entering contact adjust grid geometry.
		//if (m_bInContact && m_bFirstTimeIn)
		//{
		//	m_bFirstTimeIn = false;
		//	horizontalHeader()->resizeSection(1,300);
		//}

		if(m_nGridType==CommGridViewHelper::CONTACT_GRID || m_nGridType==CommGridViewHelper::PROJECT_GRID)
		{
			horizontalHeader()->setSectionResizeMode(1, QHeaderView::Interactive);
			horizontalHeader()->resizeSection(1,300);	
		}

		horizontalHeader()->setSectionsClickable(false);
		setSelectionBehavior(QAbstractItemView::SelectRows);
	}

	m_bFirstTimeIn=false;
}

//Get minimum due date time and return it and it's belonging start date time.
void CommGridView::GetMinTaskDueAndStartTime(int nTaskID, QList<int> &lstTaskID, QDateTime &dueDateTime, QDateTime &startDateTime)
{
	//Get selected task start and due date.
	int nBTKS_ID = nTaskID;
	int nRow = m_tableHelper.m_hshRow2BTKS_ID.key(nBTKS_ID);
	int nCENT_ID = m_tableHelper.m_hshRow2CENT_ID.value(nRow);
	QDateTime datMinDueDateTime;
	QDateTime datStartDateTime;
	int nRowInRecordset = m_recEmail->find("CENT_ID", nCENT_ID, true);
	if (nRowInRecordset >= 0)
	{
		datMinDueDateTime = m_recEmail->getDataRef(nRowInRecordset, "BTKS_DUE").toDateTime();
		datStartDateTime = m_recEmail->getDataRef(nRowInRecordset, "BTKS_START").toDateTime();
	}
	else
	{
		nRowInRecordset = m_recDocument->find("CENT_ID", nCENT_ID, true);
		if (nRowInRecordset >= 0)
		{
			datMinDueDateTime = m_recDocument->getDataRef(nRowInRecordset, "BTKS_DUE").toDateTime();
			datStartDateTime = m_recDocument->getDataRef(nRowInRecordset, "BTKS_START").toDateTime();
		}
		else
		{
			nRowInRecordset = m_recVoiceCall->find("CENT_ID", nCENT_ID, true);
			if (nRowInRecordset >= 0)
			{
				datMinDueDateTime = m_recVoiceCall->getDataRef(nRowInRecordset, "BTKS_DUE").toDateTime();
				datStartDateTime = m_recVoiceCall->getDataRef(nRowInRecordset, "BTKS_START").toDateTime();
			}
		}
	}
	Q_ASSERT(nRowInRecordset>=0);
	
	dueDateTime = datMinDueDateTime;
	startDateTime = datStartDateTime;
	
	//If only one task selected return.
	if (lstTaskID.size()==1)
		return;

	//Then loop through selected task and check is there any with due date smaller than selected one.
	for (int i = 0; i < lstTaskID.size(); ++i) 
	{
		nBTKS_ID = lstTaskID.at(i);
		nRow = m_tableHelper.m_hshRow2BTKS_ID.key(nBTKS_ID);
		nCENT_ID = m_tableHelper.m_hshRow2CENT_ID.value(nRow);
		nRowInRecordset = m_recEmail->find("CENT_ID", nCENT_ID, true);
		if (nRowInRecordset >= 0)
		{
			QDateTime datDue = m_recEmail->getDataRef(nRowInRecordset, "BTKS_DUE").toDateTime();
			if (!datDue.isNull() && (datDue < datMinDueDateTime || datMinDueDateTime.isNull()))
			{
				datMinDueDateTime = datDue;
				datStartDateTime = m_recEmail->getDataRef(nRowInRecordset, "BTKS_START").toDateTime();
			}
		}
		else
		{
			nRowInRecordset = m_recDocument->find("CENT_ID", nCENT_ID, true);
			if (nRowInRecordset >= 0)
			{
				QDateTime datDue = m_recDocument->getDataRef(nRowInRecordset, "BTKS_DUE").toDateTime();
				if (!datDue.isNull() && (datDue < datMinDueDateTime || datMinDueDateTime.isNull()))
				{
					datMinDueDateTime = datDue;
					datStartDateTime = m_recDocument->getDataRef(nRowInRecordset, "BTKS_START").toDateTime();
				}
			}
			else
			{
				nRowInRecordset = m_recVoiceCall->find("CENT_ID", nCENT_ID, true);
				if (nRowInRecordset >= 0)
				{
					QDateTime datDue = m_recVoiceCall->getDataRef(nRowInRecordset, "BTKS_DUE").toDateTime();
					if (!datDue.isNull() && (datDue < datMinDueDateTime || datMinDueDateTime.isNull()))
					{
						datMinDueDateTime = datDue;
						datStartDateTime = m_recVoiceCall->getDataRef(nRowInRecordset, "BTKS_START").toDateTime();
					}
				}
			}
		}
		Q_ASSERT(nRowInRecordset>=0);
	}

	dueDateTime = datMinDueDateTime;
	startDateTime = datStartDateTime;
}

int CommGridView::CheckTask(DbRecordSet &recEntityRecord,int nRow)
{
	if (recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull())
		return CommGridViewHelper::NOT_SCHEDULED;

	bool bTaskActive = recEntityRecord.getDataRef(nRow, "BTKS_IS_TASK_ACTIVE").toBool();
	if (!bTaskActive)
		return CommGridViewHelper::NOT_SCHEDULED;

	int nTaskStatus = recEntityRecord.getDataRef(nRow, "BTKS_SYSTEM_STATUS").toInt();
	if (nTaskStatus == GlobalConstants::TASK_STATUS_COMPLETED || nTaskStatus == GlobalConstants::TASK_STATUS_CANCELLED)
		return CommGridViewHelper::NOT_SCHEDULED;

	int nStatus;
	bool bStartUnvalid			= recEntityRecord.getDataRef(nRow, "BTKS_START").isNull();
	bool bDueUnvalid			= recEntityRecord.getDataRef(nRow, "BTKS_DUE").isNull();
	//Check is start date reached.
	bool bStartPassed;
	if(!bStartUnvalid)
	{
		if(recEntityRecord.getDataRef(nRow, "BTKS_START").toDateTime() > QDateTime::currentDateTime())
			bStartPassed = false;
		else
			bStartPassed = true;
	}
	//Check is due date reached.
	bool bDuePassed;
	if (!bDueUnvalid)
	{
		if (recEntityRecord.getDataRef(nRow, "BTKS_DUE").toDateTime() > QDateTime::currentDateTime())
			bDuePassed = false;
		else
			bDuePassed = true;
	}

	//If start date invalid then it must at least be due now
	if	(bStartUnvalid)
	{
		if(bDueUnvalid)
			nStatus = CommGridViewHelper::SCHEDULED_DUENOW;
		else
		{
			if (bDuePassed)
				nStatus = CommGridViewHelper::SCHEDULED_OVERDUE;
			else
				nStatus = CommGridViewHelper::SCHEDULED_DUENOW;
		}

	}
	//If start valid - then it needs to be checked.
	else
	{
		//If start passed - due.
		if (bStartPassed)
		{
			if(bDueUnvalid)
				nStatus = CommGridViewHelper::SCHEDULED_DUENOW;
			else
			{
				if (bDuePassed)
					nStatus = CommGridViewHelper::SCHEDULED_OVERDUE;
				else
					nStatus = CommGridViewHelper::SCHEDULED_DUENOW;
			}
		}
		//If not yet passed then it can be only scheduled.
		else
			nStatus = CommGridViewHelper::SCHEDULED;
	}

	return nStatus;
}

void CommGridView::FirstColumnData(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow)
{
	m_tableHelper.FirstColumnData(nCEType, nRowsInGrid, recEntityRecord, nRow, m_bSideBarMode);
}

void CommGridView::SecondColumnData(int nCEType, DbRecordSet &recEntityRecord, int nRow, int nRowInGrid)
{
	m_tableHelper.SecondColumnData(nCEType, recEntityRecord, nRow, nRowInGrid, m_bSideBarMode);
}

void CommGridView::ThirdColumnData(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow)
{
	m_tableHelper.ThirdColumnData(nCEType, nRowsInGrid, recEntityRecord, nRow, m_bSideBarMode);
}

//B.T. improved: unselect & select
void CommGridView::SelectDataInSourceRecordSet(int nEntitySysType, int nEntityID,bool bSelect)
{
	if (bSelect)
	{
		switch(nEntitySysType)
		{
		case GlobalConstants::CE_TYPE_VOICE_CALL:
			{
				m_recVoiceCall->find("CENT_ID", nEntityID, false, false, false);
			}
			break;

		case GlobalConstants::CE_TYPE_DOCUMENT:
			{
				m_recDocument->find("CENT_ID", nEntityID, false, false, false);
			}
			break;
		case GlobalConstants::CE_TYPE_EMAIL:
			{
				m_recEmail->find("CENT_ID", nEntityID, false, false, false);
			}
			break;
		}
	}
	else
	{
		switch(nEntitySysType)
		{
		case GlobalConstants::CE_TYPE_VOICE_CALL:
			{
				int nRow=m_recVoiceCall->find("CENT_ID", nEntityID, true);
				if (nRow!=-1)
				{
					m_recVoiceCall->selectRow(nRow,false);
				}

			}
			break;

		case GlobalConstants::CE_TYPE_DOCUMENT:
			{
				int nRow=m_recDocument->find("CENT_ID", nEntityID, true);
				if (nRow!=-1)
				{
					m_recDocument->selectRow(nRow,false);
				}
			}
			break;
		case GlobalConstants::CE_TYPE_EMAIL:
			{
				int nRow=m_recEmail->find("CENT_ID", nEntityID, true);
				if (nRow!=-1)
				{
					m_recEmail->selectRow(nRow,false);
				}
			}
			break;
		}
	}
}

void CommGridView::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
	m_recEmail->clearSelection();
	m_recVoiceCall->clearSelection();
	m_recDocument->clearSelection();

	//Locals.
	int nOwnerID = 0;
	int nContactID = 0;
	int nProjectID = 0;
	int nEntitySysType = 0;
	int nTaskID = 0;
	int nTaskStatus = 0;
	int nEntityID = 0;
	int nDocID = 0;
	int nBDMD_IS_CHECK_OUT = 0;
	int nBDMD_CHECK_OUT_USER_ID = 0;
	QList<int> lstSelTaskList;
	QList<int> lstUnreadEmailsBEM_IDs;
	QList<int> lstStartTaskEmailsCENT_IDs, lstStartTaskPhoneCallsCENT_IDs;

	QList<int> lstSelectedRows;
	QModelIndexList lstSelectedIndexes = selectedIndexes();
	int nSelRowCount = selectedIndexes().count();
	if (nSelRowCount==0)
	{
		emit SectionClicked(-1, -1, -1, -1, -1, lstSelTaskList, -1, -1, -1, -1, -1, lstUnreadEmailsBEM_IDs, lstStartTaskEmailsCENT_IDs, lstStartTaskPhoneCallsCENT_IDs);
		return;
	}
	for (int i = 0; i < nSelRowCount; ++i)
	{
		QModelIndex index = lstSelectedIndexes.value(i);
		int nRow = index.row();
		if (lstSelectedRows.contains(nRow))
			continue;
		lstSelectedRows << nRow;
		nEntitySysType		= m_tableHelper.m_hshRow2CENT_SYS.value(nRow);
		nEntityID			= m_tableHelper.m_hshRow2CENT_ID.value(nRow);
		nOwnerID			= m_tableHelper.m_hshRow2BPER_ID.value(nRow);
		nContactID			= m_tableHelper.m_hshRow2BCNT_ID.value(nRow);
		nProjectID			= m_tableHelper.m_hshRow2BUSP_ID.value(nRow);
		nTaskID				= m_tableHelper.m_hshRow2BTKS_ID.value(nRow);
		nTaskStatus			= m_tableHelper.m_hshRow2BTKS_SYSTEM_STATUS.value(nRow);
		nDocID				= m_tableHelper.m_hshRow2ENTITY_ID.value(nRow);
		nBDMD_IS_CHECK_OUT	= m_tableHelper.m_hshRow2BDMD_IS_CHECK_OUT.value(nRow);
		nBDMD_CHECK_OUT_USER_ID = m_tableHelper.m_hshRow2BDMD_CHECK_OUT_USER_ID.value(nRow);

		//Select data in recordset.
		SelectDataInSourceRecordSet(nEntitySysType, nEntityID,true);

		//Task list.
		if (nTaskID > 0 && !lstSelTaskList.contains(nTaskID))
			lstSelTaskList << nTaskID;
		//Else schedule it for start task #1456.
		else
		{
			if (nEntitySysType == GlobalConstants::CE_TYPE_EMAIL)
			{
				if (CommGridViewHelper::CheckItemDirection(CommGridViewHelper::EMAIL, *m_recEmail, m_recEmail->find("CENT_ID", nEntityID, true)) == CommGridViewHelper::INCOMING)
					lstStartTaskEmailsCENT_IDs << nEntityID;
			}
			else if(nEntitySysType == GlobalConstants::CE_TYPE_VOICE_CALL)
			{
				if (CommGridViewHelper::CheckItemDirection(CommGridViewHelper::VOICECALL, *m_recVoiceCall, m_recVoiceCall->find("CENT_ID", nEntityID, true)) == CommGridViewHelper::INCOMING)
					lstStartTaskPhoneCallsCENT_IDs << nEntityID;
			}
		}

		//Check is it unread email.
		if (nEntitySysType == GlobalConstants::CE_TYPE_EMAIL && m_tableHelper.m_hshRow2BEM_UNREAD_FLAG.value(nRow) == 1)
			lstUnreadEmailsBEM_IDs << nDocID;
	}
	
	QAbstractItemView::selectionChanged(selected, deselected);
	
	//Emit section clicked signal.
	emit SectionClicked(nOwnerID, nContactID, nProjectID, nEntitySysType, nTaskID, lstSelTaskList, nEntityID, nDocID, nBDMD_IS_CHECK_OUT, nBDMD_CHECK_OUT_USER_ID, nTaskStatus, lstUnreadEmailsBEM_IDs, lstStartTaskEmailsCENT_IDs, lstStartTaskPhoneCallsCENT_IDs);
}

void CommGridView::on_CellDoubleClicked(const QModelIndex &index)

{
	//Emit signal for detail popup.
	emit SectionDoubleClicked();
}

void CommGridView::SelectBySourceRecordSetRows(QList<int> lstSelectedCENT_IDs)
{
	if (!lstSelectedCENT_IDs.count())
	{
		//Must not be empty.
		Q_ASSERT(false);
		return;
	}
	//Find selected rows.
	QList<int> lstSelectedRows;
	//Loop and fill item selection.
	for (int i = 0; i < lstSelectedCENT_IDs.size(); ++i)
	{
		int nCENT_ID = lstSelectedCENT_IDs.value(i);
		if (m_tableHelper.m_hshRow2CENT_ID.values().contains(nCENT_ID))
		{
			int nRow = m_tableHelper.m_hshRow2CENT_ID.key(nCENT_ID);
			if (!lstSelectedRows.contains(nRow))
				lstSelectedRows << nRow;
		}
	}

	//Sort selected rows and get starting and ending row.
	qSort(lstSelectedRows);
	int nStartRow = lstSelectedRows.first();
	int nEndingRow= lstSelectedRows.last();
	//Find start and ending indexes.
	QModelIndex startIndex = model()->index(nStartRow, 0);
	QModelIndex endIndex = model()->index(nEndingRow, 0);
	QItemSelection itemSelection(startIndex, endIndex);
	//Get selection model, clear and select.
	QItemSelectionModel *selModel = selectionModel();
	selModel->clearSelection();
	//Create item selection.
	selModel->select(itemSelection, QItemSelectionModel::Select | QItemSelectionModel::Rows);
	scrollTo(startIndex);
}



void CommGridView::startDrag(Qt::DropActions supportedActions)
{
	//what item is selected, only one is ok?
	if (m_recDocument->getSelectedCount()==1)
	{	
		//get document: (
		int nRow=m_recDocument->getSelectedRow();
		//get filename, create file drag
		Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();
		bool bCTRL=false;
		bool bALT=false;
		if(Qt::ControlModifier == (Qt::ControlModifier & keys))
			bCTRL = true;
		if(Qt::AltModifier == (Qt::AltModifier & keys))
			bALT = true;
		if (!bCTRL)
		{
			QTableView::startDrag(supportedActions);
			return;
		}
		bool bReadOnly=bCTRL;
		if (bALT)
			bReadOnly=false;

		DbRecordSet rowRevision,recChkInfo;
		int nDocID=m_recDocument->getDataRef(nRow,"BDMD_ID").toInt();
		QString strNewPath=DocumentHelper::CheckOutDocument(m_recDocument->getDataRef(nRow,"BDMD_ID").toInt(),m_recDocument->getDataRef(nRow,"BDMD_DOC_PATH").toString(),bReadOnly,rowRevision,recChkInfo,false,false,true);
		if (!strNewPath.isEmpty())
		{
			QMimeData *dragData = new QMimeData;
			dragData->setUrls(QList<QUrl>() <<QUrl::fromLocalFile(strNewPath));
			QDrag *drag = new QDrag(this);
			drag->setPixmap(QPixmap (":Document32.png"));
			drag->setMimeData(dragData);
			int nActionExecuted=drag->start(Qt::MoveAction | Qt::CopyAction);
			DocumentHelper::ClearCheckOutDocumentAfterDrag(nDocID,strNewPath);
		}

	}
	else
	{
		QTableView::startDrag(supportedActions);
		return;
	}

}

void CommGridView::CreateContextMenuActions(QList<QAction*> &lstActions)
{

	QAction* pAction = new QAction(tr("Open"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtOpen()));
	lstActions.append(pAction);

	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("View"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtView()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Modify"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtModify()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtDelete()));
	lstActions.append(pAction);

	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("Save A Copy in ..."), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtSaveCopy()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Check Out in ..."), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtCheckOut()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Load Document(s)..."), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtLoadDocuments()));
	lstActions.append(pAction);

}

void CommGridView::contextMenuEvent ( QContextMenuEvent * event )
{

	if(m_lstActions.size()==0) 
	{
		event->ignore();	
		return;
	}

	QMenu CnxtMenu(this);

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));

	CnxtMenu.exec(event->globalPos());

}

bool CommGridView::viewportEvent(QEvent *event)
{
	if (event->type() == QEvent::ToolTip) 
	{
		QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
		QPoint itemPos = helpEvent->pos();
		if (itemPos.x() > iconSize().width())
			return false;
		QModelIndex ind = indexAt(itemPos);
		if (!ind.isValid())
			return false;
		//If in area for general tooltip, display general tooltip.
		QString strText = model()->data(ind, Qt::ToolTipRole).toString();
		QPoint pos = helpEvent->globalPos();
		QToolTip::showText(pos, strText);
		return true;
	} 
	else
		return QTableView::viewportEvent(event);
}

void CommGridView::On_CnxtOpen()
{
	emit CnxtMenuActionTriggered(CNXT_OPEN);
}
void CommGridView::On_CnxtView()
{
	emit CnxtMenuActionTriggered(CNXT_VIEW);
}
void CommGridView::On_CnxtModify()
{
	emit CnxtMenuActionTriggered(CNXT_MODIFY);
}
void CommGridView::On_CnxtDelete()
{
	emit CnxtMenuActionTriggered(CNXT_DELETE);
}
void CommGridView::On_CnxtSaveCopy()
{
	emit CnxtMenuActionTriggered(CNXT_SAVECOPY);
}
void CommGridView::On_CnxtCheckOut()
{
	emit CnxtMenuActionTriggered(CNXT_CHECKOUT);
}
void CommGridView::On_CnxtLoadDocuments()
{
	emit CnxtMenuActionTriggered(CNXT_LAODDOCUMENTS);
}