#ifndef OPTIONS_ORGANIZATION_H
#define OPTIONS_ORGANIZATION_H

#include <QWidget>
#include "generatedfiles/ui_options_organization.h"
#include "settingsbase.h"

class Options_Organization : public SettingsBase
{
	Q_OBJECT

public:
	Options_Organization(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
	~Options_Organization();

	DbRecordSet GetChangedValuesRecordSet();

private:
	Ui::Options_OrganizationClass ui;
	DbRecordSet m_lstData;
};

#endif // OPTIONS_ORGANIZATION_H
