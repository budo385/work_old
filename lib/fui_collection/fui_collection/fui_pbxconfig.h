#ifndef FUI_PBXCONFIG_H
#define FUI_PBXCONFIG_H

#include <QWidget>
#include "generatedfiles/ui_fui_pbxconfig.h"
#include "fuibase.h"

class FUI_PBXConfig : public FuiBase
{
    Q_OBJECT

public:
    FUI_PBXConfig(QWidget *parent = 0);
    ~FUI_PBXConfig();

	QString GetFUIName(){return tr("Call Forwarding");};

private:
    Ui::FUI_PBXConfigClass ui;

	DbRecordSet m_lstFwdContact1;
	DbRecordSet m_lstFwdContact2;
	DbRecordSet m_lstFwdContact3;
	DbRecordSet m_lstFwdContact4;
	DbRecordSet m_lstFwdContact5;
	DbRecordSet m_lstFwdContact6;
	DbRecordSet m_lstFwdContact7;
	DbRecordSet m_lstFwdContact8;
	DbRecordSet m_lstFwdContact9;

	DbRecordSet m_lstGrid1;
	DbRecordSet m_lstGrid2;

private slots:
};

#endif // FUI_PBXCONFIG_H
