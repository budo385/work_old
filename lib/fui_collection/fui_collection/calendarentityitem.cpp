#include "calendarentityitem.h"

#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLinearLayout>
#include <QGraphicsDropShadowEffect>
#include <QApplication>
#include <QToolTip>
#include <QMimeData>
#include "calendartableview.h"

#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;				//global access right tester

CalendarEntityItem::CalendarEntityItem(int nItemID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, QDateTime startDateTime, 
					QDateTime endDateTime, QString strTitle, QString strLocation,
					QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, 
					QString strDescription, QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, int nBcep_Status, 
					int nBCEV_IS_INFORMATION, bool bIsProposed, CalendarGraphicsView *pCalendarGraphicsView)
					: QGraphicsProxyWidget()
{
	m_nCentID				= nItemID;
	m_nBcepID				= nBcepID;
	m_nBcevID				= nBcevID;
	m_nBCOL_ID				= nBCOL_ID;
	m_nBcep_Status			= nBcep_Status;
	m_nBCEV_IS_INFORMATION	= nBCEV_IS_INFORMATION;
	m_bIsProposed			= bIsProposed;
	m_nWidthFactor			= 1;
	m_nColumnCount			= 1;
	m_nHorizOrder			= 0;
	m_nHorizPos				= 0;
	m_isHResizing			= false;
	m_isVResizing			= false;
	m_isDResizing			= false;
	m_bIsMoving				= false;
	m_startDateTime			= startDateTime;
	m_endDateTime			= endDateTime;
	m_nEntityID				= nEntityID;
	m_nEntityType			= nEntityType;
	m_nEntityPersonID		= nEntityPersonID;
	m_pCalendarGraphicsView	= pCalendarGraphicsView;
	setToolTip(strTitle);

	//QString s = startDateTime.toString();
	//QString ss = endDateTime.toString();

	m_DisplayWidget = new CalendarEntityDisplayWidget(strTitle, strLocation, strColor, pixIcon, strOwner, strResource, strContact, 
							strProjects, strDescription, hshBrakeStart, hshBrakeEnd, m_pCalendarGraphicsView, m_nBcep_Status, 
							m_nBCEV_IS_INFORMATION, m_bIsProposed);
	setWidget(m_DisplayWidget);
	setMinimumWidth(0);
	setMinimumHeight(0);
	setZValue(2);
	setFlag(QGraphicsItem::ItemIsSelectable);
	setAcceptDrops(true);
	setAcceptHoverEvents(true);
	CreateContextMenuActions(m_lstActions);

	QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect();
	effect->setOffset(3,3);
	effect->setBlurRadius(3);
	setGraphicsEffect(effect);

//	if (nBCEV_IS_INFORMATION)
//		setOpacity(0.6);
}

CalendarEntityItem::~CalendarEntityItem()
{
	m_DisplayWidget = NULL;
}

void CalendarEntityItem::UpdateEntityItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, 
					  QDateTime startDateTime, QDateTime endDateTime, QString strTitle, QString strColor, QString strLocation,
					  QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
					  QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed)
{
	m_nCentID				= nCentID;
	m_nBcepID				= nBcepID;
	m_nBcevID				= nBcevID;
	m_nBCOL_ID				= nBCOL_ID;
	m_nBcep_Status			= nBcep_Status;
	m_nBCEV_IS_INFORMATION	= nBCEV_IS_INFORMATION;
	m_bIsProposed			= bIsProposed;
	m_nEntityID				= nEntityID;
	m_nEntityType			= nEntityType;
	m_nEntityPersonID		= nEntityPersonID;
	m_startDateTime			= startDateTime;
	m_endDateTime			= endDateTime;
	//setToolTip(strTitle);
	CalendarEntityDisplayWidget *old_DisplayWidget = m_DisplayWidget;
	m_DisplayWidget = new CalendarEntityDisplayWidget(strTitle, strLocation, strColor, pixIcon, strOwner, strResource, strContact, 
		strProjects, strDescription, hshBrakeStart, hshBrakeEnd, m_pCalendarGraphicsView, m_nBcep_Status, m_nBCEV_IS_INFORMATION, m_bIsProposed);
	setWidget(m_DisplayWidget);
	delete(old_DisplayWidget);
	setMinimumWidth(0);
	setMinimumHeight(0);
	setZValue(2);
	setAcceptHoverEvents(true);
	//if (nBCEV_IS_INFORMATION)
	//	setOpacity(0.6);
	//else
	//	setOpacity(1);
}

void CalendarEntityItem::setHorFactor(qreal nWidthFactor)
{
	m_nWidthFactor	= nWidthFactor;
}

void CalendarEntityItem::setHorOrder(qreal nHorizOrder)
{
	m_nHorizOrder	= nHorizOrder;
}
void CalendarEntityItem::setHorPos(qreal nHorizPos)
{
	m_nHorizPos = nHorizPos;
}

/*
void CalendarEntityItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	QGraphicsProxyWidget::paint(painter, option, widget);
}
*/
void CalendarEntityItem::getStartEndDateTime(QDateTime	&startDateTime, QDateTime &endDateTime)
{
	startDateTime	= m_startDateTime;
	endDateTime		= m_endDateTime;
}

void CalendarEntityItem::SetSelected(bool bSelected /*= true*/)
{
	setSelected(bSelected);
	m_DisplayWidget->SetSelected(bSelected);
	update();
}

void CalendarEntityItem::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	event->ignore();
	/*const QMimeData *mimeData = event->mimeData();
	QStringList lstData = mimeData->text().split(";");
	if (lstData.value(0) == "entity_item")
		event->accept();
	else
		event->ignore();*/
}

void CalendarEntityItem::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{

}

void CalendarEntityItem::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{

}

void CalendarEntityItem::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	const QMimeData *mimeData = event->mimeData();
	QStringList lstData = mimeData->text().split(";");
	int nEntityID = lstData.value(1).toInt();
	int nEntityType = lstData.value(2).toInt();
	int nEntityPersonID = lstData.value(3).toInt();

	emit DropEntityOnItem(this, nEntityID, nEntityType, nEntityPersonID);
}

void CalendarEntityItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
	if(g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
	{
		CalendarEntityItem::ResizeType type = isInResizeArea(event->pos());
		if (type == HORIZ_RESIZE)
			setCursor(Qt::SizeHorCursor);
		else if (type == VERT_RESIZE)
			setCursor(Qt::SizeVerCursor);
		else if (type == DIAG_RESIZE)
			setCursor(Qt::SizeFDiagCursor);
		else if (isInMoveArea(event->pos()))
			setCursor(Qt::OpenHandCursor);
		else
			setCursor(Qt::PointingHandCursor);
	}
	
	QGraphicsProxyWidget::hoverMoveEvent(event);
}

void CalendarEntityItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	if (m_isHResizing)
	{
		m_nWidth = event->pos().x();
		resize(m_nWidth, m_nHeight);
		emit ItemResizing(true);
		return;
	}
	else if(m_isVResizing)
	{
		m_nHeight = event->pos().y();
		resize(m_nWidth, m_nHeight);
		emit ItemResizing(true);
		return;
	}
	else if (m_isDResizing)
	{
		m_nWidth = event->pos().x();
		m_nHeight = event->pos().y();
		resize(m_nWidth, m_nHeight);
		emit ItemResizing(true);
		return;
	}
	else if (m_bIsMoving)
	{
		//QGraphicsProxyWidget::mouseMoveEvent(event);
		//QGraphicsItem::mouseMoveEvent(event);
		//setPos(event->pos());
		//dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->CheckForMultipleItemsInColumn(this);
		QPointF mousePos = event->pos();
		QPointF scenePos = mapToScene(mousePos);
		QPointF tablePos = m_pCalendarGraphicsView->getTableView()->mapFromScene(scenePos);
		
		QModelIndex topLeftIndex = dynamic_cast<CalendarTableView*>(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView())->Table()->indexAt(tablePos.toPoint());
		setPos(scenePos);

		QItemSelectionModel *selModel = dynamic_cast<CalendarTableView*>(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView())->Table()->selectionModel();
		QItemSelection selection(topLeftIndex, topLeftIndex);
		selModel->clearSelection();
		selModel->select(selection, QItemSelectionModel::Select);
		//update();
		//on_UpdateCalendar();
	}
}

void CalendarEntityItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
			return;

		CalendarEntityItem::ResizeType type = isInResizeArea(event->pos());
		if (type == HORIZ_RESIZE)
		{
			m_isHResizing = true;
		}
		else if (type == VERT_RESIZE)
		{
			m_isVResizing = true;
		}
		else if (type == DIAG_RESIZE)
		{
			m_isDResizing = true;
		}
		else if (isInMoveArea(event->pos()))
		{
			QItemSelectionModel *selModel = dynamic_cast<CalendarTableView*>(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView())->Table()->selectionModel();
			selModel->clearSelection();

			setCursor(Qt::ClosedHandCursor);
			m_bIsMoving = true;
			//QGraphicsProxyWidget::mousePressEvent(event);
		}
	}
	else
		QGraphicsProxyWidget::mousePressEvent(event);
}

void CalendarEntityItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && (m_isDResizing || m_isHResizing || m_isVResizing))
	{
		m_isVResizing = false;
		m_isHResizing = false;
		m_isDResizing = false;
		emit ItemResized(this);
		return;
	}
	else if (m_bIsMoving)
	{
		m_bIsMoving = false;
		//on_UpdateCalendar();
		emit ItemMoved(this);
	}
	else if ((event->button() == Qt::LeftButton) && isSelected())
	{
		if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
			return;

		emit ItemSelected(this);
		//setSelected(false);
		//m_DisplayWidget->SetSelected(false);
		//update();
		//on_UpdateCalendar();
	}
	else if((event->button() == Qt::LeftButton) && !isSelected())
	{
		if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
			return;

		emit ItemSelected(this);
		//setSelected(true);
		//m_DisplayWidget->SetSelected(true);
		//update();
		//on_UpdateCalendar();
	}
	else
	{
		QGraphicsProxyWidget::mouseReleaseEvent(event);
		on_UpdateCalendar();
	}
}

void CalendarEntityItem::keyPressEvent(QKeyEvent *event)
{
	if ((event->key() == Qt::Key_Escape) && m_bIsMoving)
	{
		m_bIsMoving = false;
		setCursor(Qt::ArrowCursor);
		on_UpdateCalendar();
	}
}

void CalendarEntityItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
		return;

	if(m_lstActions.size()==0) 
	{
		event->ignore();	
		return;
	}

	QMenu CnxtMenu;

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));

	CnxtMenu.exec(event->screenPos());
}

void CalendarEntityItem::on_UpdateCalendar()
{
	dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->CheckForMultipleItemsInColumn(this);
	m_nWidthFactor = 1/qreal(m_nColumnCount);
	QRect rect = dynamic_cast<CalendarTableView*>(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView())->getItemRectangle(m_nEntityID, m_nEntityType, m_startDateTime, m_endDateTime);
	m_nWidth = rect.width()*m_nWidthFactor-3;
	m_nHeight = rect.height();
/*	if(m_nHeight<5)
	{
		m_nHeight=5;
	}*/
	resize(m_nWidth, m_nHeight);
	setPos(rect.topLeft().x()+dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getVertHeaderWidth()+/*m_nHorizPos*/m_nHorizOrder*rect.width()*m_nWidthFactor+1, rect.topLeft().y()+dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTotalHeaderHeight());
	update();
}

void CalendarEntityItem::On_CnxtModify()
{
	emit ItemModify(this);
}
void CalendarEntityItem::On_CnxtDelete()
{
	emit ItemDelete(this);
}
void CalendarEntityItem::On_CnxtView()
{
	emit ItemView(this);
}

CalendarEntityItem::ResizeType CalendarEntityItem::isInResizeArea(const QPointF &pos)
{
	int nWidth = this->widget()->width();
	int nHeight = this->widget()->height();
	
	if (((nWidth - 5) < pos.x()) && (pos.x() < nWidth))
	{
		if (((nHeight- 10) < pos.y()) && (pos.y() < nHeight))
			return DIAG_RESIZE;
		else
			return HORIZ_RESIZE;
	}
	else if (((nHeight- 5) < pos.y()) && (pos.y() < nHeight))
	{
		if (((nWidth - 10) < pos.x()) && (pos.x() < nWidth))
			return DIAG_RESIZE;
		else
			return VERT_RESIZE;
	}
	else
		return NO_RESIZE;
}

bool CalendarEntityItem::isInMoveArea(const QPointF &pos)
{
	if(pos.y()>0 && pos.y()<20)
		return true;
	else
		return false;
}

void CalendarEntityItem::CreateContextMenuActions(QList<QAction*> &lstActions)
{
	QAction* pAction = new QAction(tr("View"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtView()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Modify"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtModify()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtDelete()));
	lstActions.append(pAction);


}

void CalendarEntityItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent * event)
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
		return;

	On_CnxtModify();
}

/*bool CalendarEntityItem::viewportEvent(QEvent *event)
{
	if (event->type() == QEvent::ToolTip) 
	{
		QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
		QPoint pos = helpEvent->globalPos();
		QPoint pos1 = m_pCalendarGraphicsView->mapToGlobal(pos);
//		QToolTip::showText(pos1, m_strDisplayText);

		return true;
	} 
	else
		return false;
}*/
