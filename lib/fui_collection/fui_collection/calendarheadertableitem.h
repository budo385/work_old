#ifndef CALENDARHEADERTABLEITEM_H
#define CALENDARHEADERTABLEITEM_H

#include <QGraphicsProxyWidget>
#include <QAbstractTableModel>
#include <QTableView>
#include <QItemDelegate>

#include "calendarwidget.h"

class HeaderTableItemDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	HeaderTableItemDelegate(QObject * parent = 0);
	void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
protected:
private:
};

class CalendarHeaderTableItem : public QGraphicsProxyWidget
{
	Q_OBJECT

public:
	CalendarHeaderTableItem(QGraphicsView* pCalendarMultiDayGraphicsView, QAbstractTableModel *pCalendarHeaderTableModel, QGraphicsItem * parent = 0, Qt::WindowFlags wFlags = 0);
	~CalendarHeaderTableItem();

	void Initialize();
	void UpdateModel();
	void setColumnWidth(int nColumn, int nWidth);
	void setRowHeight(int nRow, int nHeight);
	QTableView* Table(){return m_pTable;};
	QAbstractTableModel* Model(){return m_pModel;};
	QRect getItemRectangle(int nEntityID, int nEntityType, QDateTime &startDateTime, QDateTime &endDateTime);
	QRect getItemRectangle(QModelIndex &Index);
	QRect getItemRectangleForHeader(int nColumn);
	QRect getItemRectangleForVertPersonHeader(int nRow);
	void resizeColumn(int nColumn, int nWidth, bool bResizeAll);
	void setColumnsDefaultWidth(int nColumnWidth);

public slots:
	void on_UpdateCalendar();
	void on_scrollBar_valueChanged(int value);
	void on_HScrollShow();
	void on_HScrollHide();
	void on_VScrollShow();
	void on_VScrollHide();
	void on_doubleClicked(const QModelIndex &index);

protected:
	void testDragDrop(QGraphicsSceneDragDropEvent *event);
	void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
	void dragLeaveEvent(QGraphicsSceneDragDropEvent *event);
	void dragMoveEvent(QGraphicsSceneDragDropEvent *event);
	void dropEvent(QGraphicsSceneDragDropEvent *event);

private:
	QGraphicsView				*m_pCalendarMultiDayGraphicsView;
	TableView					*m_pTable;
	QAbstractTableModel			*m_pModel;
	int							m_nItemLeftOffSet;

signals:
	void horizontalScrollBarVisible(bool bVisible);
	void verticalScrollBarVisible(bool bVisible);
	void cellDoubleClicked(int row, int column);
};

#endif // CALENDARHEADERTABLEITEM_H
