#ifndef APPEARANCESETTINGS_H
#define APPEARANCESETTINGS_H

#include <QWidget>
#include "generatedfiles/ui_appearancesettings.h"

//Settings base class.
#include "settingsbase.h"

class AppearanceSettings : public SettingsBase
{
    Q_OBJECT

public:
    AppearanceSettings(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
    ~AppearanceSettings();

private:
    Ui::AppearanceSettingsClass ui;

	void InitializeWidgets();
	DbRecordSet GetChangedValuesRecordSet();

	int GetCheckedStartingModuleRadio();
	void SetCheckedStartingModuleRadio(int i);

private slots:
	void on_SkinChanged(int nNewSkin);
	void on_ThemeChanged(int nNewTheme);
	void on_ViewChanged(int nNewView);
	
};

#endif // APPEARANCESETTINGS_H
