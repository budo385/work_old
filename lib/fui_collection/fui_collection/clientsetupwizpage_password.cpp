#include "clientsetupwizpage_password.h"
#include "common/common/authenticator.h"
#include <QtWidgets/QMessageBox>


//globals:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager* g_pBoSet;

			

ClientSetupWizPage_Password::ClientSetupWizPage_Password(int nWizardPageID, QString strPageTitle, QWidget *parent)
: WizardPage(nWizardPageID, strPageTitle)
{

	setMinimumSize(QSize(600,450));
}

ClientSetupWizPage_Password::~ClientSetupWizPage_Password()
{

}

void ClientSetupWizPage_Password::Initialize()
{
	ui.setupUi(this);


	m_recResultRecordSet.addColumn(QVariant::Bool,"PASS_CHANGED");
	m_recResultRecordSet.addRow();
	m_recResultRecordSet.setData(0,0,false);

	m_bPassWordChanged=false;
	//ui.txtUserName->setEnabled(false);
	QString strUserName=g_pClientManager->GetLoggedUserName();
	ui.txtUserName->setText(strUserName);
	LoadPassword();
	ui.txtOldPassword->setFocus();

	//if password set, set chkbox:
	DbRecordSet rec;
	g_pClientManager->GetUserData(rec);
	QByteArray currentPass=rec.getDataRef(0,"CUSR_PASSWORD").toByteArray();
	if (currentPass!=Authenticator::GeneratePassword(strUserName,""))
	{
		ui.groupBox->setCheckable(false);
		ui.groupBox->setTitle(tr("Change password"));
	}


	//ui.checkBox->setChecked(g_pClientManager->GetIniFile()->m_nHideLogin);
	m_bComplete = true;
	emit completeStateChanged();

	ui.groupBox->setChecked(false);
	m_bInitialized = true;
}

//load dummy password:
void ClientSetupWizPage_Password::LoadPassword()
{
	ui.txtPassword1->setText("xxxyyyzzz");
	ui.txtPassword2->setText("xxxyyyzzz");
}


void ClientSetupWizPage_Password::ClearPassword()
{
	ui.txtPassword1->clear();
	ui.txtPassword2->clear();
}

void ClientSetupWizPage_Password::on_txtPassword2_editingFinished()
{
	//set dirty flag only if different from DB:
	if(ui.txtPassword2->text()!="xxxyyyzzz")
		m_bPassWordChanged=true;
}

void ClientSetupWizPage_Password::on_txtPassword1_editingFinished()
{
	//set dirty flag only if different from DB:
	if(ui.txtPassword1->text()!="xxxyyyzzz")
		m_bPassWordChanged=true;
}


//before writing check if password is ok:
bool ClientSetupWizPage_Password::CheckPassword(QByteArray &newPassword)
{
	if (ui.txtPassword2->text()!=ui.txtPassword1->text())
	{
		QMessageBox::warning(this,tr("Error"),tr("Password not match!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return false;
	}
	else
	{
		if(m_bPassWordChanged) 			//hash password if changed:
		{
			QString strUserName=ui.txtUserName->text();
			newPassword=Authenticator::GeneratePassword(strUserName,ui.txtPassword2->text());
		}
		return true;
	}
}



bool ClientSetupWizPage_Password::GetPageResult(DbRecordSet &RecordSet)
{

	RecordSet=m_recResultRecordSet;

	if (!ui.groupBox->isChecked() && ui.groupBox->isCheckable())
		return true;


	//save:
	QByteArray newPass;
	if(CheckPassword(newPass))
	{

		if (!m_bPassWordChanged) //if not changed, ass he presses cancel
			return true;

		//save:
		Authenticator Session;
		Session.SetSessionParameters(g_pClientManager->GetLoggedUserName(),ui.txtOldPassword->text(),"", "");	//cache login params
		QByteArray clientNonce,clientAuthToken;
		Session.GenerateAuthenticationToken(clientNonce,clientAuthToken);//create cnonce

		Status err;
		_SERVER_CALL(UserLogon->ChangePassword(err,g_pClientManager->GetLoggedUserName(),clientAuthToken,clientNonce,newPass,ui.txtUserName->text()))
		_CHK_ERR_NO_RET(err);
		if (!err.IsOK())
		{
			return false;
		}
		else
		{
			//never issue relogin->does not work!
			//RecordSet.setData(0,0,true); //if true-> after setup, re-login!

			//reset flag:
			//if (ui.txtPassword1->text()!="")
			g_pClientManager->GetIniFile()->m_nHideLogin=0;
			return true;
		}
	}
	else
		return false;


}


//reset all
void ClientSetupWizPage_Password::resetPage()
{
	ui.txtOldPassword->setText(ui.txtPassword1->text());
	LoadPassword();
	m_bPassWordChanged=false;

}