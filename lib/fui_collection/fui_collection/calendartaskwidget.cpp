#include "calendartaskwidget.h"
#include "gui_core/gui_core/thememanager.h"
#include <QLabel>
#include "commworkbenchwidget.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"

CalendarTaskWidget::CalendarTaskWidget(QWidget *parent)
	: QFrame(parent)
{
	//ui.setupUi(this);
	//ui.labelTitle->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	//ui.labelTitle->setAlignment(Qt::AlignLeft);

//	connect(ui.widgetCommGrid,SIGNAL(NeedNewData(int)),this,SIGNAL(NeedNewData(int)));

//	ui.widgetCommGrid->SetCurrentGridInSidebarMode(true);
//	ui.widgetCommGrid->update();
//	ui.widgetCommGrid->Initialize(CommWorkBenchWidget::DESKTOP_GRID, this);
//	ui.widgetCommGrid->ClearCommGrid();

	QGridLayout *gridLayout = new QGridLayout;
	QLabel *labelTitle = new QLabel(tr("Tasks"));
	labelTitle->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	labelTitle->setAlignment(Qt::AlignLeft);
	m_widgetCommGrid = new CommWorkBenchWidget(NULL, true);
	connect(m_widgetCommGrid,SIGNAL(NeedNewData(int)),this,SIGNAL(NeedNewData(int)));
	m_widgetCommGrid->SetCurrentGridInSidebarMode(true);
	m_widgetCommGrid->update();
	m_widgetCommGrid->Initialize(CommGridViewHelper::CALENDAR_GRID, this);
	//m_widgetCommGrid->ClearCommGrid();
	gridLayout->addWidget(labelTitle, 0, 0);
	gridLayout->addWidget(m_widgetCommGrid, 1, 0);
	gridLayout->setColumnStretch(0,0);
	gridLayout->setRowStretch(0, 0);
	gridLayout->setRowStretch(1, 10);
	gridLayout->setAlignment(Qt::AlignLeft);
	gridLayout->setContentsMargins(4,4,4,4);
	gridLayout->setSizeConstraint(QLayout::SetMinimumSize);
	//gridLayout.setSpacing(4);

	setLayout(gridLayout);

	setStyleSheet("QFrame#frame_2"+ThemeManager::GetSidebarChapter_Bkg());

	//if (m_nEntityRecordID > 0)
	//	ui.widgetCommGrid->SetData(m_nEntityRecordID);
}

CalendarTaskWidget::~CalendarTaskWidget()
{

}

void CalendarTaskWidget::Reload(DbRecordSet *recEntities)
{
	m_widgetCommGrid->SetEntitiesRecordset(recEntities);
}

void CalendarTaskWidget::SelectTask(int nCE_Type, int nID)
{
	m_widgetCommGrid->SelectTask(nCE_Type, nID, true);
}

void CalendarTaskWidget::OpenTask(int nCE_Type, int nID)
{
	m_widgetCommGrid->SelectTask(nCE_Type, nID, true);
	m_widgetCommGrid->ModifySelectedTask();
}
