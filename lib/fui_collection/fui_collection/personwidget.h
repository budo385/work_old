#ifndef PERSONWIDGET_H
#define PERSONWIDGET_H

#include <QWidget>
#include <QHash>
#include <QMenu>

#include "generatedfiles/ui_personwidget.h"

#include "artreewidget.h"
#include "multiassigndialog.h"
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "fuibase.h"

/*!
\class  PersonWidget.
\ingroup FUICore/AccessRights
\brief  Person manipulation. 

Person and his role adding, deleting and renaming widget.
*/
class PersonWidget : public FuiBase
{
    Q_OBJECT

public:
    PersonWidget(QWidget *parent = 0);
    ~PersonWidget();

private:
	Ui::PersonWidgetClass	ui;

	void InitContextMenu();

	void Initialize();
	void InitializeButtons();
	void InitializeRecordSets();
	void InitializePersonTree();
	void InitializeRoleList();
	void InsertRoleToPerson(int nPersonID, int nRoleID);
	void DeleteRoleFromPerson(QTreeWidgetItem *Item, QTreeWidgetItem *ParentItem);
	bool CheckIsRoleInPerson(int nPersonID, int nRoleID);
	void ChangeSaveState();
	bool LockResource(int PersonID);
	void UnLockResources();
	void ReloadData();
	

	Status							m_Status;
	DbRecordSet						m_recCORE_ROLE;					//< Roles recordset.
	DbRecordSet						m_recBUS_PERSON;				//< Person recordset.
	DbRecordSet						m_recBUS_PERSONROLE;			//< Person to role recordset.
	QHash<int, QTreeWidgetItem*>	m_hshPersonIDToItem;			//< Person ID to tree item hash.
	//Changes.
	DbRecordSet						m_recInsertedRoleToPerson;		//< Inserted roles to person recordset.
	QMultiHash<int, int>			m_hshDeletedRoleFromPerson;		//< Deleted roles from person hash (role id, person id).
	//Lock resource list.
	QHash<int,QString>				m_hshLockingResourceHash;		//< Person id to lock resource hash.
	//Context actions.
	QList<QAction*>					m_lstRoleActions;

private slots:
	void			on_AssignPushButton_clicked();
	void			on_SavePushButton_clicked();
	void			on_CancelPushButton_clicked();
	void			on_DeletePushButton_clicked();
	void			RoleSelectionChanged();
	void			PersonSelectionChanged();
	void			on_RoleDropped(QTreeWidgetItem *Parent, int index);
};

#endif // PERSONWIDGET_H
