#ifndef LABEL_H
#define LABEL_H

#include <QLabel>

class Label : public QLabel
{
	Q_OBJECT

public:
	enum Widgets
	{
		HEADER_ITEM = 0,
		VERTICAL_ITEM,
		TOP_LEFT_ITEM,
		BOTTOM_LEFT_ITEM,
		TOP_RIGHT_ITEM,
		BOTTOM_RIGHT_ITEM
	};

	Label(Widgets widgetType, QWidget * parent = 0, Qt::WindowFlags f = 0);
	~Label();

private:
	
};

#endif // LABEL_H
