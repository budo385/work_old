#ifndef CONTACTSELECTIONTABLEVIEW_H
#define CONTACTSELECTIONTABLEVIEW_H

#include <QTableView>

class ContactSelectionTableView : public QTableView
{
	Q_OBJECT

public:
	ContactSelectionTableView(QWidget *parent = 0);
	~ContactSelectionTableView();

private:
};

#endif // CONTACTSELECTIONTABLEVIEW_H
