#ifndef SIDEBAR_BASE_H
#define SIDEBAR_BASE_H

#include <QWidget>
#include "ui_sidebar_base.h"

class Sidebar_Base : public QWidget
{
	Q_OBJECT

public:
	Sidebar_Base(QWidget *parent = 0);
	~Sidebar_Base();

private:
	Ui::Sidebar_BaseClass ui;
};

#endif // SIDEBAR_BASE_H
