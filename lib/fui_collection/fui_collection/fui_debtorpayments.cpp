#include "fui_debtorpayments.h"



FUI_DebtorPayments::FUI_DebtorPayments(QWidget *parent)
	: FuiBase(parent)
{
	ui.setupUi(this);

	ui.frameSelection->Initialize(ENTITY_PAYMENT_CONDITIONS,false,true); //skip loading data, coz this is under filter =ACO
	InitFui(ENTITY_PAYMENT_CONDITIONS, BUS_CM_PAYMENT_CONDITIONS,dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);
	

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData, DbSqlTableView::TVIEW_BUS_CAL_RESOURCE);
	m_GuiFieldManagerMain->RegisterField("BCMPY_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BCMPY_PAYMENTPERIOD1",ui.txtDayPeriod1);
	m_GuiFieldManagerMain->RegisterField("BCMPY_TEXT",ui.frameDesc);

	SetMode(MODE_EMPTY);
}

FUI_DebtorPayments::~FUI_DebtorPayments()
{

}

QString FUI_DebtorPayments::GetFUIName()
{
	return tr("Payment Conditions");
}

void FUI_DebtorPayments::RefreshDisplay()
{
	m_GuiFieldManagerMain->RefreshDisplay();
}

//when changing state, call this:
void FUI_DebtorPayments::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		m_GuiFieldManagerMain->SetEditMode(false);
	}
	else
	{
		m_GuiFieldManagerMain->SetEditMode(true);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}

//prepare for insert: payment period=0
void FUI_DebtorPayments::DataPrepareForInsert(Status &err)
{
	FuiBase::DataPrepareForInsert(err);
	m_lstData.setData(0,"BCMPY_PAYMENTPERIOD1",0);
}

void FUI_DebtorPayments::prepareCacheRecord(DbRecordSet *recNewData)
{
	DbRecordSet newCacheRecord;
	newCacheRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PAYMENT_CONDITIONS_SELECT));
	newCacheRecord.merge(*recNewData);
	*recNewData=newCacheRecord;
}