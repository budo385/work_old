#ifndef COMMGRIDVIEW_H
#define COMMGRIDVIEW_H

#include <QtWidgets/QTableView>
#include <QItemDelegate>

#include "commgridmodel.h"
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/commgridviewtablehelper_client.h"

class FirstColumnDelegateSideBarView : public QItemDelegate
{
	Q_OBJECT

public:
	FirstColumnDelegateSideBarView(QObject *parent = 0){};
	~FirstColumnDelegateSideBarView(){};

protected:
	void drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	void drawDecoration ( QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QPixmap &pixmap) const;
	void drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const;
	void drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const {}; //BT: no focus rectangle
};

class SecondColumnDelegateSideBarView : public QItemDelegate
{
	Q_OBJECT

public:
	SecondColumnDelegateSideBarView(QObject *parent = 0){};
	~SecondColumnDelegateSideBarView(){};

protected:
	void drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	void drawDecoration ( QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QPixmap &pixmap) const;
	void drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const {}; //BT: no focus rectangle
}; 

class FirstColumnDelegate : public QItemDelegate
{
	Q_OBJECT

public:
	FirstColumnDelegate(QObject *parent = 0){};
	~FirstColumnDelegate(){};

protected:
	void drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const;
	void drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	void drawDecoration ( QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QPixmap &pixmap) const;
	void drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const {}; //BT: no focus rectangle
};

class SecondColumnDelegate : public QItemDelegate
{
	Q_OBJECT

public:
	SecondColumnDelegate(QObject *parent = 0){};
	~SecondColumnDelegate(){};

protected:
	void drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const;
	void drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	void drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const {}; //BT: no focus rectangle
};

class ThirdColumnDelegate : public QItemDelegate
{
	Q_OBJECT

public:
	ThirdColumnDelegate(QObject *parent = 0)
	{
		m_strOwnerString	= tr("Owner:");
		m_strContactString	= tr("Contact:");
		m_strProjectString	= tr("Project:");
	};
	~ThirdColumnDelegate(){};

protected:
	void drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const;
	void drawBackground (QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	void drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const {}; //BT: no focus rectangle

private:
	QString m_strOwnerString;
	QString m_strContactString;
	QString m_strProjectString;
};

class CommGridView : public QTableView
{
	Q_OBJECT

public:
	enum CnxtMenuActionTypes
	{
		CNXT_OPEN,
		CNXT_VIEW,
		CNXT_MODIFY,
		CNXT_DELETE,
		CNXT_SAVECOPY,
		CNXT_CHECKOUT,
		CNXT_LAODDOCUMENTS

	};

public:
	CommGridView(QWidget *parent = 0);
	~CommGridView();

	void						Initialize(DbRecordSet *recEmails, DbRecordSet *recDocuments, DbRecordSet *recVoiceCalls, DbRecordSet *lstSortRecordSet, int nGridType, 
											bool bClearSelection = true, bool bRowsStripped = false);
	void						RefreshContactItem(int nCEType, DbRecordSet &recEntityRecord);
	void						SortTable(DbRecordSet *recSortRecordSet = NULL, bool bClearSelection = false, bool bForceSort = true);
	void 						SelectBySourceRecordSetRows(QList<int> lstSelectedCENT_IDs);

	int							CheckTask(DbRecordSet &recEntityRecord,int nRow=0);

	void						GetMinTaskDueAndStartTime(int nTaskID, QList<int> &lstTaskID, QDateTime &dueDateTime, QDateTime &startDateTime);

	//simulate drop event
protected:
	void						dragMoveEvent ( QDragMoveEvent * event );
	void						dragEnterEvent ( QDragEnterEvent * event );
	void						dropEvent(QDropEvent *event);
	void						startDrag(Qt::DropActions supportedActions);
	void						contextMenuEvent ( QContextMenuEvent * event );
	bool						viewportEvent(QEvent *event);

public slots:
	void						SelectGridFromSourceRecordSet();
	void						SelectSourceRecordSetFromGrid();

private:
	void						SetLayout();
	void						CreateContextMenuActions(QList<QAction*> &lstActions);
	void						FirstColumnData(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow);
	void						SecondColumnData(int nCEType, DbRecordSet &recEntityRecord, int nRow, int nRowInGrid);
	void						ThirdColumnData(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow);

	void						SelectDataInSourceRecordSet(int nEntitySysType, int nEntityID,bool bSelect=true);

	CommGridModel				*m_pGridModel;
	CommGridViewTableHelper_Client m_tableHelper;

	int							m_nRowCount;
	bool						m_bFirstTimeIn;
	int							m_nGridType;
	DbRecordSet					*m_recEmail,
								*m_recVoiceCall,
								*m_recDocument;
	bool						m_bSideBarMode;		//Are we in sidebar mode.
	QList<QAction*>				m_lstActions;

private slots:
	void						selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
	void 						on_CellDoubleClicked(const QModelIndex &index);
	void						On_CnxtOpen();
	void						On_CnxtView();
	void						On_CnxtModify();
	void						On_CnxtDelete();
	void						On_CnxtSaveCopy();
	void						On_CnxtCheckOut();
	void						On_CnxtLoadDocuments();

signals:
	void 						SectionDoubleClicked();
	void 						SectionClicked(int nOwnerID, int nContactID, int nProjectID, int nEntitySysType, int nTaskID, QList<int> lstTaskList, int nEntityID, int nDocID, int nBDMD_IS_CHECK_OUT, int nBDMD_CHECK_OUT_USER_ID, int nBTKS_SYSTEM_STATUS, QList<int> lstUnreadEmailsCENTIDs, QList<int> lstStartTaskEmailsCENT_IDs, QList<int> lstStartTaskPhoneCallsCENT_IDs);
	void 						CnxtMenuActionTriggered(int nActionType);
};

#endif // COMMGRIDVIEW_H
