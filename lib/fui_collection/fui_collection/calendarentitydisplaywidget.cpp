#include "calendarentitydisplaywidget.h"

#include <QPainter>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QtWidgets/QTableWidget>
#include <QHeaderView>
#include <QToolTip>

#include "tabledisplaywidget.h"
#include "bus_core/bus_core/globalconstants.h"

TextEdit::TextEdit(QGraphicsView *pCalendarGraphicsView, const QString &text, QWidget *parent /*= 0*/)
: QTextEdit(text, parent)
{
	m_pCalendarGraphicsView = pCalendarGraphicsView;
}

/*bool TextEdit::viewportEvent(QEvent *event)
{
	if (event->type() == QEvent::ToolTip) 
	{
		QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
		QPoint pos = helpEvent->globalPos();
		//QPoint pos1 = m_pCalendarGraphicsView->mapToGlobal(pos);
		//QPoint pos2 = m_pCalendarGraphicsView->mapFromGlobal(pos);
		//QToolTip::showText(pos1, toHtml());
		QToolTip::showText(pos, toHtml());

		return true;
	} 
	else
		return QTextEdit::viewportEvent(event);
}*/

CalendarEntityDisplayWidget::CalendarEntityDisplayWidget(QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, 
														 QString strContact, QString strProjects, QString strDescription, QHash<int, qreal> hshBrakeStart, 
														 QHash<int, qreal> hshBrakeEnd, CalendarGraphicsView *pCalendarGraphicsView, int nBcep_Status, 
														 int nBCEV_IS_INFORMATION, bool bIsProposed, QWidget *parent)
	:	QFrame(parent)
{
	//QString strStyle = " QFrame {border: 1px solid black; border-radius: 20px; background: transparent;}";
	m_bSelected = false;
	m_nBcep_Status = nBcep_Status;
	m_nBCEV_IS_INFORMATION = nBCEV_IS_INFORMATION;
	m_bIsProposed = bIsProposed;

	QString strStyle = " QFrame {border: 0px; border-radius: 0px; background: transparent;}";
	setStyleSheet(strStyle);
	
	m_item1 = NULL;
	m_item2 = NULL;
	m_item3 = NULL;
	m_item4 = NULL;
	m_item5 = NULL;
	m_item6 = NULL;
	m_edit	= NULL;
	m_pCalendarGraphicsView = pCalendarGraphicsView;

	m_hshBrakeStart = hshBrakeStart;
	m_hshBrakeEnd	= hshBrakeEnd;

	if (m_nBcep_Status == GlobalConstants::TYPE_CAL_PRELIMINARY)
		m_bodyColor = QColor(255,255,215);
	else if (m_bIsProposed)
		m_bodyColor = QColor(224,224,224);
	else if (m_nBCEV_IS_INFORMATION)
		m_bodyColor = QColor(255, 255, 255, 100);
	else
		m_bodyColor = QColor(255, 255, 255, 255);
	
	if(!strColor.isEmpty())
	{
		m_typeColor = QColor(strColor);
		if (m_nBCEV_IS_INFORMATION)
			m_typeColor.setAlpha(100);
	}
	else
		m_typeColor = m_bodyColor;

	QIcon icon(pixIcon);

	//Title row will alway be.
	int nRows = 1;
	if(icon.isNull())
		m_item1 = new QTableWidgetItem(strTitle);
	else
		m_item1 = new QTableWidgetItem(icon, strTitle);

	m_item1->setTextAlignment(Qt::AlignVCenter);

	if (!strLocation.isEmpty())
	{
		m_item2 = new QTableWidgetItem(strLocation);
		m_item2->setTextAlignment(Qt::AlignBottom);
		nRows++;
	}
	
	if (!strOwner.isEmpty())
	{
		m_item3 = new QTableWidgetItem(QIcon(":User16.png"), strOwner);
		m_item3->setTextAlignment(Qt::AlignBottom);
		nRows++;
	}
	if (!strResource.isEmpty())
	{
		m_item4 = new QTableWidgetItem(QIcon(":Resource16.png"), strResource);
		m_item4->setTextAlignment(Qt::AlignBottom);
		nRows++;
	}
	if (!strContact.isEmpty())
	{
		m_item5 = new QTableWidgetItem(QIcon(":Contacts16.png"), strContact);
		m_item5->setTextAlignment(Qt::AlignBottom);
		nRows++;
	}
	if (!strProjects.isEmpty())
	{
		m_item6 = new QTableWidgetItem(QIcon(":Projects16.png"), strProjects);
		m_item6->setTextAlignment(Qt::AlignBottom);
		nRows++;
	}
	
	m_edit = new TextEdit(m_pCalendarGraphicsView, strDescription);
	m_edit->setToolTip(strDescription);
	m_edit->setReadOnly(true);
	m_edit->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_edit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	nRows++;

	//Create table widget.
	TableDisplayWidget *table = new TableDisplayWidget(m_pCalendarGraphicsView, nRows, 1, true);
	table->horizontalHeader()->hide();
	table->horizontalHeader()->setStretchLastSection(true);
	table->verticalHeader()->setStretchLastSection(true);
	table->verticalHeader()->setMinimumSectionSize(0);
	table->verticalHeader()->setDefaultSectionSize(18); 
	table->verticalHeader()->hide();
	table->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
	table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	table->setShowGrid(false);
	table->setSelectionMode(QAbstractItemView::NoSelection);
	table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	
	//Set items in table.
	int nRowCounter = 0;
	table->setItem(0,0,m_item1);
	nRowCounter++;
	if (m_item2)
	{
		table->setItem(nRowCounter,0,m_item2);
		nRowCounter++;
	}
	if (m_item3)
	{
		table->setItem(nRowCounter,0,m_item3);
		nRowCounter++;
	}
	if (m_item4)
	{
		table->setItem(nRowCounter,0,m_item4);
		nRowCounter++;
	}
	if (m_item5)
	{
		table->setItem(nRowCounter,0,m_item5);
		nRowCounter++;
	}
	if (m_item6)
	{
		table->setItem(nRowCounter,0,m_item6);
		nRowCounter++;
	}
	if (m_edit)
	{
		table->setCellWidget(nRowCounter,0, m_edit);
	}

	QVBoxLayout *layout = new QVBoxLayout();
	layout->setSizeConstraint(QLayout::SetMaximumSize);
	layout->setContentsMargins(2,2,3,4);
	table->verticalHeader()->resizeSection(0, 21);
	layout->setSpacing(3);
	layout->addWidget(table);
	layout->addStretch(10);
	setLayout(layout);
	setMinimumHeight(0);
}

CalendarEntityDisplayWidget::~CalendarEntityDisplayWidget()
{
	m_item1 = NULL;
	m_item2 = NULL;
	m_item3 = NULL;
	m_item4 = NULL;
	m_item5 = NULL;
	m_item6 = NULL;
	m_edit	= NULL;
	m_pCalendarGraphicsView = NULL;
}

void CalendarEntityDisplayWidget::paintEvent(QPaintEvent *event)
{
	QFrame::paintEvent(event);

	QPainter paint;
	paint.begin(this);
	paint.setRenderHint(QPainter::Antialiasing);
	QBrush brush(Qt::white);

	//Background.
	QLinearGradient g(width()/2, 0, width()/2, height());
	if (m_nBCEV_IS_INFORMATION)
	{
		g.setColorAt(0, m_bodyColor);
		g.setColorAt(0.9, QColor(240, 240, 240, 100));
		paint.setBrush(g);
	}
	else
	{
		if (m_nBcep_Status == GlobalConstants::TYPE_CAL_PRELIMINARY || m_bIsProposed)
		{
			brush.setColor(m_bodyColor);
			paint.setBrush(brush);
		}
		else
		{
			g.setColorAt(0, m_bodyColor);
			g.setColorAt(0.9, QColor(240, 240, 240, 255));
			paint.setBrush(g);
		}
	}

	QPen pen;
	pen.setColor(Qt::transparent);
	paint.setPen(pen);
	int n = height();
	paint.drawRoundedRect(1,1,width()-4,height()-2, 4, 4);

	//Title
	brush.setColor(m_typeColor);
	paint.setBrush(brush);
	pen.setColor(m_typeColor);
	pen.setStyle(Qt::SolidLine);
	paint.setPen(pen);
	QPainterPath path;
	if (!(m_nBCEV_IS_INFORMATION && (m_typeColor == m_bodyColor)))
	{
		if (height()<=21)
			path.addRoundedRect(2,2,width()-6,height()-3, 3, 3);
		else
			path.addRoundedRect(2,2,width()-6,20, 3, 3);
		paint.drawPath(path);
		if (height()>21 && !m_nBCEV_IS_INFORMATION)
		{
			path.addRect(2,10,width()-6, 13);
			paint.drawPath(path);
		}
	}

	//Brakes.
	brush.setColor(QColor(215,240,240));
	paint.setBrush(brush);
	int nBrakeCount = m_hshBrakeStart.count();
	for (int i = 0; i < nBrakeCount; i++)
	{
		paint.drawRect(2, height()*m_hshBrakeStart.value(i), width()-7, height()*m_hshBrakeEnd.value(i)-height()*m_hshBrakeStart.value(i));
		QPixmap pix(":CalendarEvent_Break16.png");
		QRect rect(width()-25, height()*m_hshBrakeStart.value(i), 16, 16);
		paint.drawPixmap(rect, pix);
	}

	//Dash style.
	QVector<qreal> dashes;
	qreal space = 4;
	dashes << 5 << space;

	//Widget outline.
	brush.setColor(Qt::transparent);
	if (m_nBcep_Status == GlobalConstants::TYPE_CAL_PRELIMINARY || m_nBcep_Status == GlobalConstants::TYPE_CAL_CANCELLED)
	{
		pen.setWidth(1.9);
		pen.setStyle(Qt::DashLine);
		pen.setDashPattern(dashes);
	}
	else if (m_bIsProposed)
	{
		pen.setWidth(2.5);
		pen.setStyle(Qt::DotLine);
	}
	else
	{
		pen.setStyle(Qt::SolidLine);
	}
	paint.setBrush(brush);
	if (m_bSelected)
	{
		pen.setColor(Qt::red);
		//pen.setStyle(Qt::SolidLine);
		paint.setPen(pen);
		if (height()>21)
			paint.drawLine(1,23,width()-4,23);
		pen.setWidth(2);
		//pen.setStyle(Qt::SolidLine);
		paint.setPen(pen);
		paint.drawRoundedRect(1,1,width()-4,height()-2, 4, 4);

		if (m_nBcep_Status == GlobalConstants::TYPE_CAL_CANCELLED)
		{
			pen.setWidth(1);
			paint.setPen(pen);
			paint.drawLine(4,4,width()-6,height()-4);
			paint.drawLine(4,height()-4,width()-6,4);
		}
	}
	else
	{
		if (m_nBcep_Status == GlobalConstants::TYPE_CAL_CANCELLED)
			pen.setColor(Qt::red);
		else if (m_nBcep_Status == GlobalConstants::TYPE_CAL_PRELIMINARY || m_bIsProposed)
		{
		//	pen.setWidth(2);
			pen.setColor(Qt::black);
		}
		else
			pen.setColor(Qt::gray);
		paint.setPen(pen);
		paint.drawRoundedRect(1,1,width()-4,height()-2, 4, 4);
		pen.setStyle(Qt::SolidLine);
		paint.setPen(pen);
		if (height()>21)
			paint.drawLine(1,23,width()-4,23);

		if (m_nBcep_Status == GlobalConstants::TYPE_CAL_CANCELLED)
		{
			pen.setWidth(1);
			paint.setPen(pen);
			paint.drawLine(4,4,width()-6,height()-4);
			paint.drawLine(4,height()-4,width()-6,4);
		}
	}
	
	paint.end();
}
