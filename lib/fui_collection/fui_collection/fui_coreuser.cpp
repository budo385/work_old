#include "fui_coreuser.h"
#include "common/common/entity_id_collection.h"
#include "common/common/authenticator.h"
#include "gui_core/gui_core/guifield_validator.h"

//GLOBALS:
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

FUI_CoreUser::FUI_CoreUser(QWidget *parent)
: FuiBase(parent)
{

	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------

	ui.setupUi(this);
	ui.frameSelection->Initialize(ENTITY_CORE_USER);
	InitFui(ENTITY_CORE_USER,CORE_USER,dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);
	
	//B.T.:these flags are hidden:
	ui.chkSystem->setVisible(false);
	ui.chkLocalAccess->setVisible(false);

	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_CORE_USER);
	m_GuiFieldManagerMain->RegisterField("CUSR_FIRST_NAME",ui.txtFirstName);
	m_GuiFieldManagerMain->RegisterField("CUSR_LAST_NAME",ui.txtLastName);
	m_GuiFieldManagerMain->RegisterField("CUSR_USERNAME",ui.txtUserName);
	m_GuiFieldManagerMain->RegisterField("CUSR_ACTIVE_FLAG",ui.chkActive);
	m_GuiFieldManagerMain->RegisterField("CUSR_IS_SYSTEM",ui.chkSystem);
	m_GuiFieldManagerMain->RegisterField("CUSR_LOCAL_ACCESS_ONLY",ui.chkLocalAccess);	

	m_CalcName.Initialize(ENTITY_BUS_CONTACT,m_lstData);
	m_nPasswordIdx=m_lstData.getColumnIdx("CUSR_PASSWORD");

	//set edit to false:
	SetMode(MODE_EMPTY);
	disconnect(ui.groupBox,SIGNAL(toggled (bool)),this,SLOT(on_groupBox_toggled(bool)));
}

FUI_CoreUser::~FUI_CoreUser()
{
	delete m_GuiFieldManagerMain;
}



//when programatically change data, call this:
void FUI_CoreUser::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();

	//IS PERSON:
	IsPerson();

}



//when changing state, call this:
void FUI_CoreUser::SetMode(int nMode)
{

	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameSelection->setEnabled(true);
		//ui.frameDetails->setEnabled(false);

		ui.txtFirstName->setEnabled(false);
		ui.txtLastName->setEnabled(false);
		ui.txtUserName->setEnabled(false);
		ui.groupBox->setEnabled(false);
		ui.chkSystem->setEnabled(false);
		ui.chkActive->setEnabled(false);
		ui.chkLocalAccess->setEnabled(false);
		ui.btnGeneratePassword->setEnabled(false);
		ui.txtPassword1->setEnabled(false);
		ui.txtPassword2->setEnabled(false);
	}
	else
	{
		ui.frameDetails->setEnabled(true);
		
		//to be sure: enable all of theM_
		ui.txtFirstName->setEnabled(true);
		ui.txtLastName->setEnabled(true);
		ui.txtUserName->setEnabled(true);
		ui.groupBox->setEnabled(true);
		ui.chkSystem->setEnabled(true);
		ui.chkActive->setEnabled(true);
		ui.chkLocalAccess->setEnabled(true);
		ui.btnGeneratePassword->setEnabled(true);
		ui.txtPassword1->setEnabled(true);
		ui.txtPassword2->setEnabled(true);

		ui.frameSelection->setEnabled(false);
		//Set focus to Pers. No:
		ui.txtFirstName->setFocus(Qt::OtherFocusReason);
	}

	m_bPassWordChanged=false;

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();

	//PASSWORDS:
	if(nMode==MODE_READ ||nMode==MODE_EDIT)
		LoadPassword();
	else
		ClearPassword();

	//if built in accounts forbid edit:
	if(nMode==MODE_EDIT && m_lstData.getRowCount()>0)
		if (m_lstData.getDataRef(0,"CUSR_USERNAME").toString()=="sysadmin" || m_lstData.getDataRef(0,"CUSR_USERNAME").toString()=="sysappserver")
		{
			QMessageBox::warning(this,tr("Built-in account"),tr("This is built-in account, you can not modify all settings!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);

			ui.txtFirstName->setEnabled(false);
			ui.txtLastName->setEnabled(false);
			ui.txtUserName->setEnabled(false);
			ui.groupBox->setEnabled(false);
			ui.chkSystem->setEnabled(false);
		}
}

/*!
Called after WRITE, refresh NAME column

\param recNewData	- one row record with entity data: NOTE: ID key column must at index=0
*/
void FUI_CoreUser::prepareCacheRecord(DbRecordSet* recNewData)
{
	//fill name (calculated field):)
	DbRecordSet newCacheRecord;
	newCacheRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USER_SELECTION_SHORT));
	newCacheRecord.merge(*recNewData);

	//calculate NAME:
	QString strName=m_CalcName.GetCalculatedName(*recNewData);
	newCacheRecord.setData(0,"CUSR_NAME",strName);

	*recNewData=newCacheRecord;
}

/*!
	Intercept OK message to test passwords
*/
bool FUI_CoreUser::on_cmdOK()
{
	if (!CheckPassword()) return false;

	//if password not changed & user is, set password:
	if (!m_bPassWordChanged && g_pClientManager->GetLoggedUserName()!=ui.txtUserName->text()) 
	{
		QMessageBox::warning(this,tr("Warning"),tr("You must set password when changing username!"));
		return false;
	}
	
	return FuiBase::on_cmdOK();
}

/*!
	Intercept DELETE to prevent deletion of built in account
*/
bool FUI_CoreUser::on_cmdDelete()
{
	//if built in accounts:
	if(m_nMode==MODE_READ && m_lstData.getRowCount()>0)
		if (m_lstData.getDataRef(0,"CUSR_USERNAME").toString()=="sysadmin" || m_lstData.getDataRef(0,"CUSR_USERNAME").toString()=="sysappserver")
		{
			QMessageBox::warning(this,tr("Built-in account"),tr("You can not delete built-in account!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return false;
		}
	
	//call base:
	return FuiBase::on_cmdDelete();
}

//load dummy password:
void FUI_CoreUser::LoadPassword()
{
	ui.txtPassword1->setText("xxxyyyzzz");
	ui.txtPassword2->setText("xxxyyyzzz");
}

//load dummy password:
void FUI_CoreUser::ClearPassword()
{
	ui.txtPassword1->clear();
	ui.txtPassword2->clear();
}

void FUI_CoreUser::on_txtPassword2_editingFinished()
{
	//set dirty flag only if different from DB:
	if(ui.txtPassword2->text()!="xxxyyyzzz")
		m_bPassWordChanged=true;
}

void FUI_CoreUser::on_txtPassword1_editingFinished()
{
	//set dirty flag only if different from DB:
	if(ui.txtPassword1->text()!="xxxyyyzzz")
		m_bPassWordChanged=true;
}

void FUI_CoreUser::on_btnGeneratePassword_clicked()
{
	QByteArray pass =Authenticator::GenerateRandomPassword(8);
	QString strPass(pass);
	ui.txtPassword1->setText(strPass);
	ui.txtPassword2->setText(strPass);
	QMessageBox::information(this,tr("New password set"),tr("Password: ")+strPass,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
	m_bPassWordChanged=true;
}

//before writing check if password is ok:
bool FUI_CoreUser::CheckPassword()
{
	if (ui.txtPassword2->text()!=ui.txtPassword1->text())
	{
		QMessageBox::warning(this,tr("Error"),tr("Password not match!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return false;
	}
	else
	{
		if(m_bPassWordChanged) 			//hash password if changed:
		{
			QString strUserName=m_lstData.getDataRef(0,"CUSR_USERNAME").toString();
			QByteArray pass=Authenticator::GeneratePassword(strUserName,ui.txtPassword2->text());
			m_lstData.setData(0,m_nPasswordIdx,pass);
		}

		return true;
	}
}

void FUI_CoreUser::on_pushButton_clicked()
{
		//if row count zero, exit
		if(m_lstData.getRowCount()==0)
			return;

		//get ID, if 0 exit
		int nUserID=m_lstData.getDataRef(0,"CUSR_PERSON_ID").toInt();
		if(nUserID==0)
			return;

		//open FUI with user data:
		int nNewFUI = g_objFuiManager.OpenFUI(MENU_PERSON, true, false, FuiBase::MODE_READ, nUserID);
}

void FUI_CoreUser::on_groupBox_toggled(bool)
{
	IsPerson();
}

void FUI_CoreUser::IsPerson()
{
	disconnect(ui.groupBox,SIGNAL(toggled (bool)),this,SLOT(on_groupBox_toggled(bool)));
	//IS PERSON:
	//if row count zero, exit
	ui.groupBox->setChecked(false);
	if(m_lstData.getRowCount()==0)
		return;
	//get ID, if 0 exit
	int nUserID=m_lstData.getDataRef(0,"CUSR_PERSON_ID").toInt();
	if(nUserID==0)
		return;
	ui.groupBox->setChecked(true);
	connect(ui.groupBox,SIGNAL(toggled (bool)),this,SLOT(on_groupBox_toggled(bool)));
}

//get FUI name
QString FUI_CoreUser::GetFUIName()
{
	if(m_nEntityRecordID>0)
		return tr("Login Account: ")+ui.frameSelection->GetCalculatedName(m_nEntityRecordID);
	else
		return tr("Login Account");
}


void FUI_CoreUser::on_cmdInsert()
{

	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_USER_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, CORE_USER, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		nCnt++; //<- hidden user counts
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed users!\nCan not insert new user!"));
			return;
		}
	}

	FuiBase::on_cmdInsert();

}

void FUI_CoreUser::on_cmdCopy()
{

	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_USER_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, CORE_USER, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		nCnt++; //<- hidden user counts
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed users!\nCan not insert new user!"));
			return;
		}
	}

	FuiBase::on_cmdCopy();
}


void FUI_CoreUser::DataPrepareForInsert(Status &err)
{
	DataClear();
	m_lstData.addRow();
	m_lstData.setData(0,"CUSR_ACTIVE_FLAG",1);	
	m_lstData.setData(0,"CUSR_IS_SYSTEM",0);			
	m_lstData.setData(0,"CUSR_LOCAL_ACCESS_ONLY",0); 
}
void FUI_CoreUser::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	//DataForWrite.debugPrintInfo();
	if(m_lstData.getDataRef(0,"CUSR_LAST_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Last Name is mandatory field!"));	return;	}

	//B.T. issue 2572 on 29.02.2012
	//if(m_lstData.getDataRef(0,"CUSR_FIRST_NAME").toString().isEmpty())
	//{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("First Name is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"CUSR_USERNAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Username is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"CUSR_PASSWORD").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Password must be set!"));	return;	}
}