#ifndef CALENDARTABLEVIEW_H
#define CALENDARTABLEVIEW_H

#include <QTableView>
#include <QGraphicsProxyWidget>
#include <QItemDelegate>
#include <QHeaderView>
#include <QDate>
#include <QScrollBar>
#include <QTableWidgetItem>

#include "calendarwidget.h"

class ScrollBar : public QScrollBar
{
	Q_OBJECT
public:
protected:
	void showEvent(QShowEvent *event);
	void hideEvent(QHideEvent *event);
private:
signals:
	void scrollBarVisible();
	void scrollBarHidden();
};

class ItemDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	ItemDelegate (QAbstractTableModel *pTableModel, int nLeftOffSet = 15, QObject * parent = 0);
	void AddMultiDayIDs(int nCentID, int nBcepID, int nBcevID);
	void GetMultiDayIDs(QHash<int, int> &hshMultiDayCentIDs, QHash<int, int> &hshMultiDayBcepIDs, QHash<int, int> &hshMultiDayBcevIDs);
	void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
protected:
private:
	QAbstractTableModel *m_pTableModel;

	int m_nLeftOffSet;
	int m_nWorkStartHour;
	int m_nWorkEndHour;
	QHash<int, int> m_hshMultiDayCentIDs;
	QHash<int, int> m_hshMultiDayBcepIDs;
	QHash<int, int> m_hshMultiDayBcevIDs;
};

class CalendarTableView : public QGraphicsProxyWidget
{
	Q_OBJECT

public:

	CalendarTableView(CalendarWidget* pCalendarWidget, QGraphicsView* pCalendarGraphicsView, QAbstractTableModel *pCalendarTableModel, QWidget *parent = 0);
	~CalendarTableView();

	void	Initialize(QDate startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites);

	QRect	getItemRectangle(int nEntityID, int nEntityType, QDateTime &startDateTime, QDateTime &endDateTime, bool bFromTaskItem = false);
	QRect	getItemRectangle(QModelIndex &Index);
	QRect	getItemRectangleForHeader(int nColumn);
	QRect	getItemRectangleForVertHeader(int nRow);
	void	getSelectionStartEndDateTime(QDateTime &startDateTime, QDateTime &endDateTime);
	void	setColumnWidth(int nColumn, int nWidth);
	int		getColumnWidth();
	void	ItemResizing(bool bIsResizing);
	void	setColumnsDefaultWidth(int nColumnWidth);

	QDateTime GetTopLeftVisibleItem();
	void	SetTopLeftVisibleItem(QDateTime dateTime);

	QAbstractTableModel* Model();
	TableView*			Table();

public slots:
	void on_UpdateCalendar();
	void on_headerScrollBar_valueChanged(int value);
	void on_HScrollShow();
	void on_HScrollHide();
	void on_VScrollShow();
	void on_VScrollHide();
protected:

private:
	CalendarWidget		*m_pCalendarWidget;
	QGraphicsView		*m_pCalendarGraphicsView;
	TableView			*m_pTable;
	QAbstractTableModel	*m_pModel;
	ScrollBar			*m_pHorizontalScrollBar;
	ScrollBar			*m_pVerticalScrollBar;
	
	QDate m_datStartDate;
	int m_nDayInterval;
	int m_nRowHeight;
	int m_nItemLeftOffSet;
	DbRecordSet *m_recEntites;		//Entities recordset.
	CalendarWidget::timeScale m_timeScale;

private slots:
	void on_doubleClicked(const QModelIndex &index);

signals:
	void horizontalScrollBarVisible(bool bVisible);
	void verticalScrollBarVisible(bool bVisible);
	void cellDoubleClicked(int row, int column);
	void openMultiDayEvent(QModelIndex index);
};

#endif // CALENDARTABLEVIEW_H
