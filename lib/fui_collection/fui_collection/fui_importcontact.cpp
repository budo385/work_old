#include "os_specific/os_specific/mapimanager.h"
//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "fui_importcontact.h"
#include "fui_importemail.h"
#include "os_specific/os_specific/skypemanager.h"
#include "common/common/csvimportfile.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "common/common/datahelper.h"
#include "gui_core/gui_core/gui_helper.h"
#include "fui_importemail.h"
#include <QtWidgets/QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "communicationmanager.h"
#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#include "bus_core/bus_core/countries.h"
#include "bus_client/bus_client/changemanager.h"
#include "os_specific/os_specific/mapimanager.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "os_specific/os_specific/twixtelmanager.h"

extern QString g_strLastDir_FileOpen;


#ifndef min
#define min(a,b) (((a)<(b))? (a):(b))
#endif

#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;				//global client cache
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services

#ifdef _WIN32
#include "os_specific/os_specific/skype/skypeparser.h"
extern SkypeParser	g_objParser;
#endif

#define MAPI_CONTACTS_FOLDER_EN "Contacts"
#define MAPI_CONTACTS_FOLDER_DE "Kontakte"

//build list modes
#define BLM_REPLACE_EXISTING	0
#define BLM_UPDATE_EXISTING		1
#define BLM_IGNORE_EXISTING		2
#define BLM_SKIP_EXISTING		3

#include "common/common/logger.h"
extern Logger g_Logger;

bool CsvProgress(unsigned long nUserData);



typedef bool (*FN_ADD_NEW_ROW)(unsigned long nCallerObject,int nRow);
bool Call_OnNewRowAdded(unsigned long nCallerObject,int nRow)
{
	FUI_ImportContact *pDlg = (FUI_ImportContact *)nCallerObject;
	Q_ASSERT(NULL != pDlg);
	if(pDlg)
	{
		return pDlg->OnNewRowAdded(nRow);
	}
	return false;
}



FUI_ImportContact::FUI_ImportContact(QWidget *parent)
    : FuiBase(parent)
{
	m_bSkipStartupFolderPicker = false;

	ui.setupUi(this);
	InitFui();

	m_pQCWFui = NULL;
	//ui.btnSourceFolder->hide(); //TOFIX does not work yet!

	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_FORMAT_CONTACTS_IMPORT__OUTLOOK, nValue))
		ui.cboImportSource->addItem(tr("Outlook"));
	if(g_FunctionPoint.IsFPAvailable(FP_FORMAT_CONTACTS_IMPORT__SKYPE, nValue))
		ui.cboImportSource->addItem(tr("Skype Contacts"));
	if(g_FunctionPoint.IsFPAvailable(FP_FORMAT_CONTACTS_IMPORT__TEXT_, nValue))
		ui.cboImportSource->addItem(tr("Text File"));
	if(g_FunctionPoint.IsFPAvailable(FP_FORMAT_CONTACTS_IMPORT__TWIXTEL, nValue))
		ui.cboImportSource->addItem(tr("TwixTel CD"));

	connect(ui.cboImportSource, SIGNAL(currentIndexChanged(int)), this, SLOT(OnImportTypeCboChanged(int)));

	ui.radSkipExisting->setChecked(true);

	ui.radNoFilter->setChecked(true);
	EnableFilterWidgets(false);

	connect(ui.radNoFilter, SIGNAL(toggled(bool)), this, SLOT(OnRadioNoFilterChanged(bool)));
	connect(ui.radUseFilter, SIGNAL(toggled(bool)), this, SLOT(OnRadioUseFilterChanged(bool)));
	
	//define import data record
	m_lstTmpImportData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));
	m_lstTmpImportData.addColumn(QVariant::String, "BCNT_LOCATION"); //MB 2006.11.17 copy city in location

	//define list attached to grid
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	//calculated fields needed for grid display
	m_lstData.addColumn(QVariant::String, "BCMA_STREET");
	m_lstData.addColumn(QVariant::String, "BCMA_CITY");
	m_lstData.addColumn(QVariant::String, "BCMA_COUNTRY_NAME");
	m_lstData.addColumn(QVariant::String, "PHONE_NUMBERS");	//all phones delimited with "/"



	ui.tableContacts->Initialize(&m_lstData,true);
	ui.tableContacts->SetEditMode(true);
	//delete some context menu actions
	QList<QAction*>&lstActions = ui.tableContacts->GetContextMenuActions();
	lstActions.removeAt(4);
	lstActions.removeAt(0);
	
	//additional action
	QAction *editContactsAction = new QAction(QIcon(), tr("Modify"), this);
	connect(editContactsAction, SIGNAL(triggered()), this, SLOT(on_editContacts()));
	lstActions.insert(0, editContactsAction);

	//
	QAction *separator = new QAction(QIcon(), "", this);
	separator->setSeparator(true);
	lstActions.insert(1, separator);

	//define column headers
	DbRecordSet columns;
	ui.tableContacts->AddColumnToSetup(columns,"BCNT_LASTNAME",tr("Last Name"),100,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BCNT_FIRSTNAME",tr("First Name"),100,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BCNT_ORGANIZATIONNAME",tr("Organization"),140,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BCMA_STREET",tr("Street"),140,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BCMA_CITY",tr("Town"),100,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"BCMA_COUNTRY_NAME",tr("Country"),100,false, "");
	ui.tableContacts->AddColumnToSetup(columns,"PHONE_NUMBERS",tr("Phone numbers"),160,false, "");
	ui.tableContacts->SetColumnSetup(columns);

	ui.frameOrganization->Initialize(ENTITY_BUS_ORGANIZATION);
	ui.frameOrganization->SetSelectionButtonLabel(tr("Organization:"));

	GUI_Helper::SetStyledButtonColorBkg(ui.btnBuildList, GUI_Helper::BTN_COLOR_YELLOW);
	GUI_Helper::SetStyledButtonColorBkg(ui.btnImportContacts, GUI_Helper::BTN_COLOR_YELLOW);
	ui.label1->setStyleSheet("* {font-weight:800; font-family:MS Shell Dlg 2; font-size:12pt}");

	//fill sex map
	m_mapSex.insert("Mr.", true);	// male titles
	m_mapSex.insert("Hr.", true);
	m_mapSex.insert("Herr", true);
	m_mapSex.insert("Mister", true);
	
	m_mapSex.insert("Ms.", false);	// female titles
	m_mapSex.insert("Mrs.", false);
	m_mapSex.insert("Miss", false);
	m_mapSex.insert("Misses", false);
	m_mapSex.insert("Fr.", false);
	m_mapSex.insert("Frau", false);
	m_mapSex.insert("Fr�ulein", false);
	m_mapSex.insert("Mme.", false);
	m_mapSex.insert("Madame", false);

	//load outlook folder selection setting
	/*
	QByteArray  arData  = g_pSettings->GetPersonSetting(IMPORT_CONTACT_OUTLOOK_FOLDERS).toByteArray();
	OutlookFolderPickerDlg::Binary2FolderList(arData, m_lstFolders);
	int nCnt = m_lstFolders.count();
	if(0 == nCnt)
	{
		//
		OutlookFolderPickerDlg::GetDefaultFolders(m_lstFolders, true);

		/*
		//fill default folders
		QStringList lstSub;
		lstSub.append("Contacts");
		m_lstFolders.append(lstSub);
		*/
	//}

	ui.btnSourceFolder->setVisible(false);

	//show folder picker on startup (delay to give a chance to skip this in case no Outlook is used)
	QTimer::singleShot(200,this,SLOT(onStartupCheckShowFolderPicker()));
}

FUI_ImportContact::~FUI_ImportContact()
{
}

void FUI_ImportContact::on_btnBuildList_clicked()
{
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		
	m_lstTmpImportData.clear();

	//prepare filters
	if(ui.radUseFilter->isChecked()){
		m_strFilterFirstName	= ui.txtFilterFirstName->text();
		m_strFilterLastName		= ui.txtFilterLastName->text();
		m_strFilterOrganization = ui.txtFilterOrganization->text();
		m_strFilterTown			= ui.txtFilterTown->text();
	}
	else{
		m_strFilterFirstName.clear();
		m_strFilterLastName.clear();
		m_strFilterOrganization.clear();
		m_strFilterTown.clear();
	}

	if(m_strFilterFirstName.right(1) != "*")	m_strFilterFirstName += "*";
	if(m_strFilterLastName.right(1) != "*")		m_strFilterLastName += "*";
	if(m_strFilterOrganization.right(1) != "*")	m_strFilterOrganization += "*";
	if(m_strFilterTown.right(1) != "*")			m_strFilterTown += "*";

	if(tr("Outlook") == ui.cboImportSource->currentText())
	{
#ifdef _WIN32
		//
		// build Outlook contacts list
		//
		int nContactsImported=0;
		MapiManager::ImportContacts(m_lstTmpImportData,m_lstFolders,nContactsImported,m_mapSex,Call_OnNewRowAdded,(unsigned long)this, MapiManager::InitGlobalMapi());
		OnImportRecordsListCreated();
		if(0 == nContactsImported)
			QMessageBox::information(this, tr("Info"), tr("Could not import contacts from Outlook(R). Please drag your contacts with your mouse from Outlook(R) and drop them to the (yet empty) contact list in this contact import window."));
#endif
	}
	else if(tr("Skype Contacts") == ui.cboImportSource->currentText())
	{
#ifdef _WIN32
		//
		// build skype contacts list
		//

		//register as parser result receiver
		g_objParser.SetSkypeBuddiesWnd(this); //m_pSkypeBuddiesFui = this;

		m_nTotalSkypeEntries = 0;
		m_nCurSkypeEntry = 0;

		SkypeManager::SendCommand("PING");	//to test connection
		SkypeManager::SendCommand("SEARCH FRIENDS");

		//check import results after some timeout
		QTimer::singleShot(500,this,SLOT(checkSkypeContactsImported()));
#endif
		return;
	}
	else if(tr("Text File") == ui.cboImportSource->currentText())
	{
		//
		// build contacts list from text file
		//
		QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                               g_strLastDir_FileOpen,
                                                tr("Text file (*.txt)"));
		if (!fileName.isEmpty())
		{
			QFileInfo fileInfo(fileName);
			g_strLastDir_FileOpen=fileInfo.absolutePath();

			QSize size(300,100);
			QProgressDialog progress(tr("Loading Contacts From File..."),tr("Cancel"),0,0,this);
			progress.setWindowTitle(tr("Operation In Progress"));
			progress.setMinimumSize(size);
			progress.setWindowModality(Qt::WindowModal);
			progress.setMinimumDuration(1200);//1.2s before pops up

			//QString strFormat1 = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Contacts - Subformat: Contacts Details";
			//QString strFormat2 = "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Contacts - Subformat: Contacts Details";
			DbRecordSet data;
			Status status;
			CsvImportFile import;

			QStringList lstFormatPlain;
			lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Contacts - Subformat: Contacts Details";
			lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 2.00 - Format: Contacts - Subformat: Contacts Details";
			QStringList lstFormatUtf8;
			lstFormatUtf8 << "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Contacts - Subformat: Contacts Details";
			lstFormatUtf8 << "~~~SOKRATES Data Exchange - Version: 2.00 - Charset: UTF8 - Format: Contacts - Subformat: Contacts Details";

			import.Load(status, fileName, data, lstFormatPlain, lstFormatUtf8, false, CsvProgress, (unsigned long)&progress);
			if(!status.IsOK()){
				QMessageBox::information(this, "Error", status.getErrorText());
				QApplication::restoreOverrideCursor();
				return;
			}
			int nFieldsCnt = data.getColumnCount();
			//V2.0 format has 66 fields (new one are ignored)
			if(56 != nFieldsCnt && 66 != nFieldsCnt){
				QMessageBox::information(this, "Error", tr("Document does not contain %1 fields for each record!").arg(56));
				QApplication::restoreOverrideCursor();
				return;
			}
			//data.Dump();

			//merge results with destination list
			int nRow = m_lstTmpImportData.getRowCount() - 1;
			int nCnt = data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				int nColumnsInThisRow = data.getRow(i).getColumnCount();
				Q_ASSERT(nColumnsInThisRow >= 56);

				m_lstTmpImportData.addRow();
				nRow ++;
				
				int nCol = 0;
				m_lstTmpImportData.setData(nRow, "CLC_IS_ADD_ON",            data.getDataRef(i, nCol++).toInt());
				m_lstTmpImportData.setData(nRow, "BCNT_FIRSTNAME",           data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_LASTNAME",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_MIDDLENAME",          data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_NICKNAME",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_BIRTHDAY",			DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
				m_lstTmpImportData.setData(nRow, "BCNT_SEX",                 data.getDataRef(i, nCol++).toInt());
				m_lstTmpImportData.setData(nRow, "BCNT_DEPARTMENTNAME",        data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_PROFESSION",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_FUNCTION",              data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_ORGANIZATIONNAME",      data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_FOUNDATIONDATE",        DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
				m_lstTmpImportData.setData(nRow, "BCNT_DESCRIPTION",           data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_LANGUAGECODE",          data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCNT_SUPPRESS_MAILING",      data.getDataRef(i, nCol++).toInt());
				m_lstTmpImportData.setData(nRow, "BCNT_OLD_CODE",              data.getDataRef(i, nCol++).toString());
				Q_ASSERT(m_lstTmpImportData.getDataRef(nRow, "BCNT_OLD_CODE").toString().length()<=80);
				m_lstTmpImportData.setData(nRow, "BCMA_FORMATEDADDRESS",       data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_FIRSTNAME",             data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_LASTNAME",              data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_MIDDLENAME",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_TITLE",                 data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_SALUTATION",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_ORGANIZATIONNAME",      data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_ORGANIZATIONNAME_2",    data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_STREET",                data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_ZIP",                   data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_CITY",                  data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_COUNTRY_CODE",          Countries::GetISOCodeFromDs(data.getDataRef(i, nCol++).toString()));
				m_lstTmpImportData.setData(nRow, "BCMA_COUNTRY_NAME",          data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_REGION",                data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_POBOX",                 data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_POBOXZIP",              data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMA_OFFICECODE",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_EMAIL_ADDRESS",          data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_EMAIL_ADDRESS_DESC",     data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_WEB_ADDRESS",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_WEB_ADDRESS_DESC",       data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_BUSINESS_CENTRAL", data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_BUSINESS_DIRECT",  data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_MOBILE",           data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_FAX",              data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_PRIVATE",          data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_PRIVATE_MOBILE",   data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_SKYPE",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_GENERIC",          data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "CLC_PHONE_GENERIC_NAME",     data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMJ_PERSON_CODE",           data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMJ_DATE",                  DataHelper::ParseDateString(data.getDataRef(i, nCol++).toString()));
				m_lstTmpImportData.setData(nRow, "BCMJ_TEXT",                  data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMJ_IS_PRIVATE",            data.getDataRef(i, nCol++).toInt());
				m_lstTmpImportData.setData(nRow, "CLC_GROUPS",                 data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMD_DEBTORCODE",            data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMD_DEBTORACCOUNT",         data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMD_CUSTOMERCODE",		   data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMD_DESCRIPTION",		   data.getDataRef(i, nCol++).toString());
				m_lstTmpImportData.setData(nRow, "BCMD_INVOICE_TEXT",          data.getDataRef(i, nCol++).toString());
				//NOTE: 10 additional columns defined in v2.0 are ignored on import

				//update progress
				progress.setValue(1);
				qApp->processEvents();
				if (progress.wasCanceled())
					break;
			}
			//m_lstTmpImportData.Dump();

			OnImportRecordsListCreated(&progress);
		}
	}
	else if(tr("TwixTel CD") == ui.cboImportSource->currentText())
	{
#ifdef _WIN32
		//
		// build contacts list from Twixtel CD
		//
		static QString strDrive;
		if(strDrive.isEmpty()) strDrive = DataHelper::GetFirstCdRomDrive();
	
		if( ui.txtFilterFirstName->text().isEmpty() &&
			ui.txtFilterLastName->text().isEmpty() &&
			ui.txtFilterOrganization->text().isEmpty() &&
			ui.txtFilterTown->text().isEmpty())
		{
			QMessageBox::information(this, "", "You must define search query for Twixtel (too many results)!");
			QApplication::restoreOverrideCursor();
			return;
		}

		QApplication::restoreOverrideCursor(); //before input box

		strDrive = QInputDialog::getText(NULL, "", "Please set the Twixtel directory/drive:",  QLineEdit::Normal, strDrive);
		if(!strDrive.isEmpty())
		{
			QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

			QString strLastNameFilterOriginal = ui.txtFilterLastName->text();
			QString strFilter_FirstName;
			if(!ui.txtFilterFirstName->text().isEmpty())
				strFilter_FirstName = ui.txtFilterFirstName->text() + "*";
			QString strFilter_Organization;
			if(!ui.txtFilterOrganization->text().isEmpty())
				strFilter_Organization = ui.txtFilterOrganization->text() + "*";
			QString strFilter_Town;
			if(!ui.txtFilterTown->text().isEmpty())
				strFilter_Town = ui.txtFilterTown->text() + "*";

			QHash<QString,QString> lstFilterData;
			lstFilterData["strLastNameFilterOriginal"]=strLastNameFilterOriginal;
			lstFilterData["strFilter_FirstName"]=strFilter_FirstName;
			lstFilterData["strFilter_Organization"]=strFilter_Organization;
			lstFilterData["strFilter_Town"]=strFilter_Town;
			if(TwixTelManager::ReadTwixTelContacts(m_lstTmpImportData,strDrive,lstFilterData))
				OnImportRecordsListCreated();
		}
		else
			QMessageBox::information(NULL, "", tr("User aborted!"));
#endif
	}

	QApplication::restoreOverrideCursor();
}

void FUI_ImportContact::SetSkypeBuddiesReply(QString strReply)
{
	//parse reply list to get user handles, then get additional info on the users
	if(0 == strReply.indexOf("USERS "))
	{
		strReply = strReply.mid(strlen("USERS "));
		QStringList list = strReply.split(",");

		//for each Skype handle
		int nCnt = list.size();
		
		m_nTotalSkypeEntries = nCnt;
		m_nCurSkypeEntry = 0;

		int nRow = m_lstTmpImportData.getRowCount()-1;
		for(int i=0; i<nCnt; i++)
		{
			QString strNumber = list.at(i).trimmed();

			m_lstTmpImportData.addRow();
			nRow ++;

			//store Skype handle
			m_lstTmpImportData.setData(nRow, "CLC_PHONE_SKYPE", strNumber);

			// get full info for Skype user
			SkypeManager::SendCommand(QString().sprintf("GET USER %s FULLNAME", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s BIRTHDAY", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s SEX", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s COUNTRY", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s PROVINCE", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s CITY", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s PHONE_HOME", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s PHONE_OFFICE", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s PHONE_MOBILE", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s HOMEPAGE", strNumber.toLatin1().constData()).toLatin1().constData());
			SkypeManager::SendCommand(QString().sprintf("GET USER %s ABOUT", strNumber.toLatin1().constData()).toLatin1().constData());
		}
	}
}

void FUI_ImportContact::OnUserResolved_FullName(QString strHandle, QString strFullName)
{
	//split full name to first and last
	QString strFirstName = strFullName;
	QString strLastName = strFullName;
	int nPos = strFirstName.indexOf(" ");
	if(nPos >= 0){
		strFirstName = strFirstName.left(nPos);
		strLastName  = strLastName.mid(nPos + 1);
	}
	else
		strLastName  = "";

	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE"); Q_ASSERT(nCol >= 0);
	int nRow = FindLastRow(strHandle);
	m_lstTmpImportData.setData(nRow, "BCNT_FIRSTNAME", strFirstName);
	m_lstTmpImportData.setData(nRow, "BCNT_LASTNAME", strLastName);
}

void FUI_ImportContact::OnUserResolved_Birthday(QString strNumber, QString strData)
{
	if(strData.length() == 8) //19780329
	{
		int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
		int nRow = FindLastRow(strNumber);

		//parse date string
		int nYear  = strData.mid(0, 4).toInt();
		int nMonth = strData.mid(4, 2).toInt();
		int nDay   = strData.mid(6, 2).toInt();
		QDate dateBDay(nYear, nMonth, nDay);
		m_lstTmpImportData.setData(nRow, "BCNT_BIRTHDAY", dateBDay);
	}
}

void FUI_ImportContact::OnUserResolved_Sex(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	if(strData == "MALE")
		m_lstTmpImportData.setData(nRow, "BCNT_SEX", ContactTypeManager::SEX_MALE);
	else if(strData == "FEMALE")
		m_lstTmpImportData.setData(nRow, "BCNT_SEX", ContactTypeManager::SEX_FEMALE);
}

void FUI_ImportContact::OnUserResolved_Country(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	m_lstTmpImportData.setData(nRow, "BCMA_COUNTRY_NAME", strData);
}

void FUI_ImportContact::OnUserResolved_Province(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	m_lstTmpImportData.setData(nRow, "BCNT_DEPARTMENTNAME", strData);
}

void FUI_ImportContact::OnUserResolved_City(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	m_lstTmpImportData.setData(nRow, "BCMA_CITY", strData);
}

void FUI_ImportContact::OnUserResolved_PhoneHome(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	m_lstTmpImportData.setData(nRow, "CLC_PHONE_PRIVATE", strData);
}

void FUI_ImportContact::OnUserResolved_PhoneOffice(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	m_lstTmpImportData.setData(nRow, "CLC_PHONE_BUSINESS_DIRECT", strData);
}

void FUI_ImportContact::OnUserResolved_PhoneMobile(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	m_lstTmpImportData.setData(nRow, "CLC_PHONE_MOBILE", strData);
}

void FUI_ImportContact::OnUserResolved_Homepage(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	m_lstTmpImportData.setData(nRow, "CLC_WEB_ADDRESS", strData);
}

void FUI_ImportContact::OnUserResolved_About(QString strNumber, QString strData)
{
	int nCol = m_lstTmpImportData.getColumnIdx("CLC_PHONE_SKYPE");
	int nRow = FindLastRow(strNumber);
	m_lstTmpImportData.setData(nRow, "BCNT_DESCRIPTION", strData);

	// handle item filtering when the last Skype field was received
	OnNewRowAdded(nRow);

	m_nCurSkypeEntry ++;
	if(m_nCurSkypeEntry == m_nTotalSkypeEntries){	//last Skype entry loaded 
		OnImportRecordsListCreated();
	}
}

void FUI_ImportContact::EnableFilterWidgets(bool bEnable)
{
	ui.txtFilterFirstName	->setEnabled(bEnable);
	ui.txtFilterLastName	->setEnabled(bEnable);
	ui.txtFilterOrganization->setEnabled(bEnable);
	ui.txtFilterTown		->setEnabled(bEnable);
}

void FUI_ImportContact::OnRadioNoFilterChanged(bool bChecked)
{
	EnableFilterWidgets(!bChecked);
}

void FUI_ImportContact::OnRadioUseFilterChanged(bool bChecked)
{
	EnableFilterWidgets(bChecked);
}

void FUI_ImportContact::UpdateImportContactsButton()
{
	int nCnt = ui.tableContacts->GetDataSource()->getRowCount(); //
	ui.btnImportContacts->setEnabled((nCnt > 0));
}

void FUI_ImportContact::OnImportTypeCboChanged(int nIdx)
{
	if(nIdx == 3)
	{
		ui.radUseFilter->setEnabled(false);
		ui.radNoFilter->setEnabled(false);
		ui.radUseFilter->hide();
		ui.radNoFilter->hide();
		EnableFilterWidgets(true);
	}
	else
	{
		ui.radUseFilter->setEnabled(true);
		ui.radNoFilter->setEnabled(true);
		ui.radUseFilter->show();
		ui.radNoFilter->show();
		EnableFilterWidgets(ui.radUseFilter->isChecked());
	}

	if(0 == nIdx){
		ui.btnSourceFolder->setEnabled(true);
	}
	else
		ui.btnSourceFolder->setEnabled(false);

	//delete current result list (mixed sources list is not allowed)
	m_lstTmpImportData.clear();
	m_lstData.clear();
	ui.tableContacts->RefreshDisplay();
	UpdateImportContactsButton();

	//trigger skype connection
	if(nIdx == 1)
		SkypeManager::SendCommand("PING");	//to test connection
}

bool FUI_ImportContact::OnNewRowAdded(int nRow)
{
	QString strFirstName = m_lstTmpImportData.getDataRef(nRow, "BCNT_FIRSTNAME").toString();
	QString strLastName  = m_lstTmpImportData.getDataRef(nRow, "BCNT_LASTNAME").toString();
	QString strCompany   = m_lstTmpImportData.getDataRef(nRow, "BCMA_ORGANIZATIONNAME").toString();
	
	//
	// Filter row data, delete row if neccessary
	//
	if(ui.radUseFilter->isChecked())
	{
		if(!m_strFilterFirstName.isEmpty()){
			QRegExp rxFirstName(m_strFilterFirstName, Qt::CaseInsensitive, QRegExp::Wildcard);
			if(!rxFirstName.exactMatch(strFirstName)){
				m_lstTmpImportData.deleteRow(nRow);
				return false;
			}
		}
		if(!m_strFilterLastName.isEmpty()){
			QRegExp rxLastName(m_strFilterLastName, Qt::CaseInsensitive, QRegExp::Wildcard);
			if(!rxLastName.exactMatch(strLastName)){
				m_lstTmpImportData.deleteRow(nRow);
				return false;
			}
		}
		if(!m_strFilterTown.isEmpty()){
			QRegExp rxTown(m_strFilterTown, Qt::CaseInsensitive, QRegExp::Wildcard);
			if(!rxTown.exactMatch(m_lstTmpImportData.getDataRef(nRow, "BCMA_CITY").toString())){
				m_lstTmpImportData.deleteRow(nRow);
				return false;
			}
		}
		if(!m_strFilterTown.isEmpty()){
			QRegExp rxCompany(m_strFilterTown, Qt::CaseInsensitive, QRegExp::Wildcard);
			if(!rxCompany.exactMatch(m_lstTmpImportData.getDataRef(nRow, "BCMA_ORGANIZATIONNAME").toString())){
				m_lstTmpImportData.deleteRow(nRow);
				return false;
			}
		}
	}

	return true;
}

void FUI_ImportContact::UpdateRowPhonesField(DbRecordSet &lstData, int nRow)
{
	DbRecordSet lstPhones = lstData.getDataRef(nRow, "LST_PHONE").value<DbRecordSet>();
	QString strPhones;

	int nCnt = lstPhones.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		QString strData = lstPhones.getDataRef(i, "BCMP_FULLNUMBER").toString();
		if(!strData.isEmpty()){
			if(!strPhones.isEmpty()) strPhones += " / ";
			strPhones += strData;
		}
	}

	if(!strPhones.isEmpty())
		lstData.setData(nRow, "PHONE_NUMBERS", strPhones);
}

void FUI_ImportContact::on_btnImportContacts_clicked()
{
	//update build list mode
	int nOperation = 0;
	if(ui.radReplaceExisting->isChecked())
		nOperation = BLM_REPLACE_EXISTING;
	else if(ui.radUpdateExisting->isChecked())
		nOperation = BLM_UPDATE_EXISTING;
	else if(ui.radIgnoreExisting->isChecked())
		nOperation = BLM_IGNORE_EXISTING;
	else
		nOperation = BLM_SKIP_EXISTING;

	//reset ID data
	int nIDIdx = m_lstData.getColumnIdx("BCNT_ID");
	m_lstData.setColValue(nIDIdx, QVariant(QVariant::Int));

	//extract selected rows
	DbRecordSet lstSelected = m_lstData;
	lstSelected.deleteUnSelectedRows();
	//lstSelected.Dump();
	if(lstSelected.getRowCount() < 1){
		QMessageBox::information(this, "", tr("No items have been selected for import!"));
		return;
	}

	//
	// import process
	//
	int nSize = lstSelected.getRowCount();

	QSize size(300,100);
	QProgressDialog progress(tr("Importing Contacts..."),tr("Cancel"),0,nSize+30,this);
	progress.setWindowTitle(tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	nSize = lstSelected.getRowCount();	//fix size (some rows were merged)
	progress.setMinimumSize(size);

	//update progress
	progress.setValue(10);
	qApp->processEvents();
	if (progress.wasCanceled())
		return;

	bool bCreateUsers = false;	//this parameter is not used for contacts
	Status status;

	const int nChunkSize = 50;

	int nFailuresCnt = 0;
	int nStart = -1;
	int nRowProgress = 0;

	int nCount = 0;
	while (1)
	{
		//prepare chunk
		DbRecordSet lstWork;
		lstWork.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL)); //BT: on server only TVIEW_BUS_CONTACT_FULL is allowed
		//lstWork.clear();

		int nMax = min(nCount+nChunkSize, nSize);
		for(int i=nCount; i<nMax; i++){
			lstWork.merge(lstSelected.getRow(i));
		}
		if(lstWork.getRowCount() < 1)
			break;

#ifdef _DEBUG
		int nDbgCol = lstWork.getColumnIdx("BCNT_LASTNAME");
		int nDbgPos = lstWork.find(nDbgCol, QString("Burlon"), true);
		if(nDbgPos >= 0){
			lstWork.getRow(nDbgPos).Dump();
			lstWork.getDataRef(nDbgPos, "LST_ADDRESS").value<DbRecordSet>().Dump();
		}
#endif
		
		DbRecordSet rowErr;
		rowErr.addColumn(QVariant::Int, "StatusCode");
		rowErr.addColumn(QVariant::String, "StatusText");
		rowErr.addColumn(QVariant::Int, "ErrRow");
		rowErr.addRow();

		//lstSelected.Dump();
		//lstWork.Dump();

		_SERVER_CALL(BusImport->ImportContacts(status, lstWork, rowErr, nOperation, bCreateUsers))
		if(!status.IsOK()){
			QMessageBox::information(this, "Error", status.getErrorText());
			return;
		}
		if(rowErr.getDataRef(0, "StatusCode").toInt() != 0){
			QString strErr = tr("Import stopped at contact #%1. Error: ").arg(rowErr.getDataRef(0, "ErrRow").toInt()+1);
			strErr += rowErr.getDataRef(0, "StatusText").toString();
			QMessageBox::information(this, "Error", strErr);
			return;
		}

		//lstWork.Dump();

		//merge status fields back to original list (only selected items were imported) and refresh grid display
		int nHandled = 0;
		while(-1 != (nStart = m_lstData.getSelectedRow(nStart+1))) 
		{
			//update main list
			//m_lstData.Dump();
			//lstWork.Dump();

			lstSelected.getDataRef(nRowProgress, "BCNT_ID") = lstWork.getDataRef(nHandled, "BCNT_ID");
			m_lstData.getDataRef(nStart, "BCNT_ID") = lstWork.getDataRef(nHandled, "BCNT_ID");
			m_lstData.getDataRef(nStart, "STATUS_CODE") = lstWork.getDataRef(nHandled, "STATUS_CODE");
			m_lstData.getDataRef(nStart, "STATUS_TEXT") = lstWork.getDataRef(nHandled, "STATUS_TEXT");
			if(lstWork.getDataRef(nHandled, "STATUS_CODE").toInt() > 0)
				nFailuresCnt++;

			nRowProgress ++;	//total progress
			nHandled ++;		//progress in this chunk
			if(nHandled >= nChunkSize)
				break;
		}

		int nWorkSize = lstWork.getRowCount();
		for(int i=0; i<nWorkSize; i++)
			if(lstWork.getDataRef(i, "STATUS_CODE").toInt() > 0)
				nFailuresCnt++;

		nCount = nMax;

		//update progress
		progress.setValue(nCount);
		qApp->processEvents();
		if (progress.wasCanceled())
			return;
	}

	//lstContactTxtFormat.Dump();
	ui.tableContacts->RefreshDisplay();

	progress.setValue(nCount+15);
	qApp->processEvents();

	//--------------------------------------------------------------------
	// BT: add all sucesfully inserted contacts into main contact cache (can produce duplicates, but that is predicted!)
	//extract only valid:
	//lstSelected.Dump();

	DbRecordSet lstContactActual;
	lstContactActual.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
	lstSelected.find(lstSelected.getColumnIdx("STATUS_CODE"),0,false,false,true,true);
	lstContactActual.merge(lstSelected,true);

	//if has lines:
	if (lstContactActual.getRowCount()>0)
	{
		//lstContactActual.Dump();
		//rearrange calculated names:
		MainEntityCalculatedName namer;
		namer.Initialize(ENTITY_BUS_CONTACT,lstContactActual);
		int nSize=lstContactActual.getRowCount();
		lstContactActual.clearSelection();

		for(int i=0;i<nSize;++i){
			lstContactActual.setData(i,"BCNT_NAME",namer.GetCalculatedName(lstContactActual,i));
			if (lstContactActual.getDataRef(i,0).isNull())
				lstContactActual.selectRow(i);
		}
		progress.setValue(nCount+25);
		qApp->processEvents();

		lstContactActual.deleteSelectedRows();
		//lstContactActual.Dump();
		if (lstContactActual.getRowCount()>0) //notify cache:
			g_ClientCache.ModifyCache(ENTITY_BUS_CONTACT,lstContactActual,DataHelper::OP_EDIT,this);

		//refresh organization: issue 1927:
		MainEntitySelectionController orgLoader;
		orgLoader.Initialize(ENTITY_CONTACT_ORGANIZATION);
		orgLoader.ReloadData(true);
	}

	//set to 100%:
	progress.setValue(progress.maximum());
	qApp->processEvents();

	//--------------------------------------------------------------------

	//report if there are any failures
	if(nFailuresCnt > 0){
		QMessageBox::information(this, "", QString(tr("%1 contacts imported, %2 contacts skipped."))
			.arg(nSize-nFailuresCnt).arg(nFailuresCnt));
	}
	else
	{
		QString strTmp = QString( tr("%1 contacts imported.") ).arg(nSize);
		QMessageBox::information(this, tr("Import Successful"), strTmp);
	}
}

//find last record with given Skype number
int FUI_ImportContact::FindLastRow(const QString &strHandle)
{
	int nCnt = m_lstTmpImportData.getRowCount();
	for(int i= nCnt-1; i>=0; i--)
		if(m_lstTmpImportData.getDataRef(i, "CLC_PHONE_SKYPE").toString() == strHandle)
			return i;

	return -1;
}

void FUI_ImportContact::OnImportRecordsListCreated(QProgressDialog *pDlgProgress)
{
	QTime time;
	time.start();

	//MB 2006.11.17 copy city in location
	int nTmpCnt = m_lstTmpImportData.getRowCount();
	for(int i=0; i<nTmpCnt; i++)
		m_lstTmpImportData.setData(i, "BCNT_LOCATION", m_lstTmpImportData.getDataRef(i, "BCMA_CITY"));

	qDebug() << "Time1" << time.restart();

	//rebuild GUI list from import list data
	DbRecordSet lstData;
	lstData.copyDefinition(m_lstTmpImportData);
	ClientContactManager manager;

	//get org data:
	QString strOrganizationNameForced; 
	DbRecordSet recOrg;
	int nOrgID;
	if(ui.frameOrganization->GetCurrentEntityRecord(nOrgID,recOrg))
	{
		strOrganizationNameForced=recOrg.getDataRef(0,"ORG_NAME").toString();
	}

	//m_lstTmpImportData.Dump();
	//lstData.Dump();

	manager.ConvertContactListFromImport(m_lstTmpImportData, lstData, strOrganizationNameForced, pDlgProgress); //BT: progress is not used anymore

	//lstData.Dump();
	qDebug() << "Time2" << time.restart();

	//BT: 28.01.2008 progress dialog parish
	if(pDlgProgress){
		pDlgProgress->setValue(1);
		qApp->processEvents();
	}


	//m_lstTmpImportData.Dump();
	//DbRecordSet lstJournal = lstData.getDataRef(0, "LST_JOURNAL").value<DbRecordSet>();
	//lstJournal.Dump();

	manager.PrepareContactListFromImport(lstData, pDlgProgress); //BT: progress is not used anymore

	//lstData.Dump();

	//update progress
	if(pDlgProgress){
		pDlgProgress->setValue(1);
		qApp->processEvents();
	}
	qDebug() << "Time3" << time.restart();

	//re-attach calculated fields needed for grid display
	lstData.addColumn(QVariant::String, "BCMA_STREET");
	lstData.addColumn(QVariant::String, "BCMA_CITY");
	lstData.addColumn(QVariant::String, "BCMA_COUNTRY_NAME");
	lstData.addColumn(QVariant::String, "PHONE_NUMBERS"); //all phones delimited with "/"

	//recalc calculated fields
	int nCnt = lstData.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		UpdateRowPhonesField(lstData, i); //list all phones delimited with "/" into field "PHONE_NUMBERS"

		DbRecordSet lstAddresses = lstData.getDataRef(i, "LST_ADDRESS").value<DbRecordSet>();
		if(lstAddresses.getRowCount() > 0){
			lstData.setData(i, "BCMA_STREET",       lstAddresses.getDataRef(0, "BCMA_STREET").toString());
			lstData.setData(i, "BCMA_CITY",         lstAddresses.getDataRef(0, "BCMA_CITY").toString());
			lstData.setData(i, "BCMA_COUNTRY_NAME", lstAddresses.getDataRef(0, "BCMA_COUNTRY_NAME").toString());
		}
	}

	//update progress
	if(pDlgProgress){
		pDlgProgress->setValue(1);
		qApp->processEvents();
	}
	qDebug() << "Time4" << time.restart();

	m_lstData.merge(lstData);
	m_lstData.selectAll();

	//update progress
	if(pDlgProgress){
		pDlgProgress->setValue(1);
		qApp->processEvents();
	}
	qDebug() << "Time5" << time.restart();

	ui.tableContacts->RefreshDisplay();
	UpdateImportContactsButton();
	ui.tableContacts->setFocus();

	qDebug() << "Time6" << time.restart();
}

//lstImport must be in TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT format
void FUI_ImportContact::SetImportListFromSource(DbRecordSet &lstImport)
{
	m_lstTmpImportData.clear();
	m_lstTmpImportData.merge(lstImport);
	OnImportRecordsListCreated();

	ui.cboImportSource->blockSignals(true);
	ui.cboImportSource->addItem(tr("vCard"));
	ui.cboImportSource->setCurrentIndex(ui.cboImportSource->count()-1);
	ui.cboImportSource->blockSignals(false);
}

void FUI_ImportContact::BuildImportListFromDrop(QByteArray &byteDrop)
{
#ifdef _WIN32
	MapiManager::BuildImportListFromDrop(m_lstTmpImportData,byteDrop,m_mapSex,Call_OnNewRowAdded,(unsigned long)this, MapiManager::InitGlobalMapi());
	OnImportRecordsListCreated();
#endif
}

void FUI_ImportContact::on_btnSourceFolder_clicked()
{
	OutlookFolderPickerDlg dlg(false, true); //contacts
	dlg.SetSelection(m_lstFolders);
	if(dlg.exec()){
		m_lstFolders = dlg.GetSelection();
		QByteArray arData;
		if(dlg.StoreSettings(arData))
			g_pSettings->SetPersonSetting(IMPORT_CONTACT_OUTLOOK_FOLDERS, arData);
	}
}

void FUI_ImportContact::SetDefaultSource(QString strSource)
{
	ui.cboImportSource->setCurrentIndex(ui.cboImportSource->findText(strSource));
}

void FUI_ImportContact::on_editContacts()
{
	//extract all rows
	DbRecordSet lstSelected = m_lstData;
	if(lstSelected.getRowCount() < 1){
		QMessageBox::information(this, "", tr("No items have been selected for edit!"));
		return;
	}

	DbRecordSet lstContacts;
	lstContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL)); //BT: on server only TVIEW_BUS_CONTACT_FULL is allowed

	//Clear selection in new recordset.
	lstContacts.clearSelection();
	//Get selected item id.
	int nFocusedItem = ui.tableContacts->GetCurrentSelectedRow();
	int nRowCount = lstSelected.getRowCount();
	//Loop through all records don't use merge and select the item with current focus #1464.
	for (int i = 0; i < nRowCount; ++i)
	{
		if (lstSelected.isRowSelected(i))
		{
			lstContacts.addRow();
			int nLastRow = lstContacts.getRowCount()-1;
			DbRecordSet row = lstSelected.getRow(i);
			lstContacts.assignRow(nLastRow, row, true);
			if (nFocusedItem != i)
				lstContacts.selectRow(nLastRow, false);
			else
				lstContacts.selectRow(nLastRow);
		}
	}

	m_pQCWFui = g_objFuiManager.OpenQCWWindow(this, -1, &lstContacts);
}

//When chkbox is activated and sync fields are changed, change all addresses, load them if not
void FUI_ImportContact::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);

	//QCW:
	if (pSubject==m_pQCWFui && nMsgCode==ChangeManager::GLOBAL_QCW_WINDOW_FINISH_PROCESS)
	{
		DbRecordSet lstAddedData=val.value<DbRecordSet>();

		m_lstData.deleteSelectedRows();
		m_lstData.merge(lstAddedData);
		ui.tableContacts->RefreshDisplay();
		UpdateImportContactsButton();
		ui.tableContacts->setFocus();
	}
}

void FUI_ImportContact::checkSkypeContactsImported()
{
	QApplication::restoreOverrideCursor();

	if(0 == m_nTotalSkypeEntries){
		QMessageBox::information(this, tr("Info"), tr("No contacts in Skype(R) found. Please make sure you use an actual version of Skype(R)!"));
	}
}

void FUI_ImportContact::StartSearch()
{
	on_btnBuildList_clicked();
}

void FUI_ImportContact::onStartupCheckShowFolderPicker()
{
	//MB changed his mind yet again
	/*
	if( tr("Outlook") == ui.cboImportSource->currentText() && 
		!m_bSkipStartupFolderPicker)
		on_btnSourceFolder_clicked();
	*/
}


void FUI_ImportContact::OnUserResolved(int nTypeData,QString strHandle,QVariant varData)
{
	switch(nTypeData)
	{
	case SKYPE_FULL_NAME:
		OnUserResolved_FullName(strHandle,varData.toString());
		break;
	case SKYPE_BIRTHDAY:
		OnUserResolved_Birthday(strHandle,varData.toString());
		break;
	case SKYPE_SEX:
		OnUserResolved_Sex(strHandle,varData.toString());
	    break;
	case SKYPE_COUNTRY:
		OnUserResolved_Country(strHandle,varData.toString());
	    break;
	case SKYPE_PROVINCE:
		OnUserResolved_Province(strHandle,varData.toString());
		break;
	case SKYPE_CITY:
		OnUserResolved_City(strHandle,varData.toString());
		break;
	case SKYPE_PHONE_HOME:
		OnUserResolved_PhoneHome(strHandle,varData.toString());
	    break;
	case SKYPE_PHONE_OFFICE:
		OnUserResolved_PhoneOffice(strHandle,varData.toString());
	    break;
	case SKYPE_PHONE_MOBILE:
		OnUserResolved_PhoneMobile(strHandle,varData.toString());
		break;
	case SKYPE_HOMEPAGE:
		OnUserResolved_Homepage(strHandle,varData.toString());
		break;
	case SKYPE_ABOUT:
		OnUserResolved_About(strHandle,varData.toString());
		break;
	default:
		Q_ASSERT(false);
	    break;
	}

}
