#ifndef SELECTION_ACTUALORGANIZATION_FUI_H
#define SELECTION_ACTUALORGANIZATION_FUI_H

#include "bus_client/bus_client/selection_actualorganization.h"

class Selection_ActualOrganization_FUI : public Selection_ActualOrganization
{
	Q_OBJECT

public:
	Selection_ActualOrganization_FUI(QWidget *parent);
	~Selection_ActualOrganization_FUI();
	QWidget* GetLastFUIOpen(){return m_pLastFUIOpen;};

protected slots:
	void on_btnModify_clicked();
	void on_btnInsert_clicked();
	void on_btnView_clicked();

private:
	QWidget *m_pLastFUIOpen;
	
};

#endif // SELECTION_ACTUALORGANIZATION_FUI_H
