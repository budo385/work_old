#include "table_contactjournal.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "dlg_contactjournal.h"

//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

Table_ContactJournal::Table_ContactJournal(QWidget * parent)
:UniversalTableWidgetEx(parent),m_toolbar(NULL)
{

}


//called after dark
void Table_ContactJournal::Initialize(QTextEdit *pExtJournalEntry,QPushButton *btnEditJournalEntry,toolbar_edit *pToolBar)
{
	//set data source for table, enable drop
	m_lstJournal.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_JOURNAL_SELECT));

	//store widgets
	m_pExtJournalEntry=pExtJournalEntry;
	m_btnEditJournalEntry=btnEditJournalEntry;
	m_btnEditJournalEntry->setVisible(false); //hide it baby

	connect(m_btnEditJournalEntry,SIGNAL(clicked()),this,SLOT(OpenRTFEditor()));

	DbRecordSet columns;
	AddColumnToSetup(columns,"BCMJ_DATE",tr("Date"),80,false, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMJ_IS_PRIVATE",tr("Is Private"),60,false, "",COL_TYPE_CHECKBOX);
	AddColumnToSetup(columns,"BPER_NAME",tr("Person"),160,false);
	AddColumnToSetup(columns,"BCMJ_TEXT",tr("Description"),260,true, "",COL_TYPE_CUSTOM_WIDGET);
	//SetColumnSetup(columns);
	UniversalTableWidgetEx::Initialize(&m_lstJournal,&columns,false,true,30);

	m_nJournalEntryIdx=m_plstData->getColumnIdx("BCMJ_TEXT");
	m_pExtJournalEntry->setHtml("");
	m_btnEditJournalEntry->setEnabled(false);
	m_pExtJournalEntry->setReadOnly(true);
	connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnEdit()));
	connect(this,SIGNAL(SignalSelectionChanged()),this,SLOT(OnSelectionChanged()));

	BuildToolBar(pToolBar);
	CreateContextMenu();
}


void Table_ContactJournal::CreateContextMenu()
{
	QList<QAction*> lstActions;
	QAction *SeparatorAct;

	//Insert new row:
	QAction* pAction = new QAction(tr("&Add New Entry"), this);
	pAction->setShortcut(tr("Ins"));
	pAction->setIcon(QIcon(":handwrite.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnInsert()));

	lstActions.append(pAction);
	this->addAction(pAction);

	pAction = new QAction(tr("Show Entry"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnShow()));
	lstActions.append(pAction);

	pAction = new QAction(tr("&Edit Entry"), this);
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnEdit()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete Selected Journal Entries From Database"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteSelected()));
	lstActions.append(pAction);

	lstActions.append(pAction);
	this->addAction(pAction);

	//Sorting:
	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	//Select All:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), this, SLOT(OpenSortDialog()));

	lstActions.append(pAction);
	SetContextMenuActions(lstActions);
}


//table changed state test it
void Table_ContactJournal::OnSelectionChanged()
{
	//refresh entry if one row is selected
	if(m_plstData->getSelectedCount()==1)
	{
		int nRowSelected=m_plstData->getSelectedRow();
		m_pExtJournalEntry->setHtml(m_plstData->getDataRef(nRowSelected,m_nJournalEntryIdx).toString());
		if(m_bEdit)m_btnEditJournalEntry->setEnabled(true);
	}
	else
	{
		m_pExtJournalEntry->setHtml("");
		m_btnEditJournalEntry->setEnabled(false);
	}
}


//newRecord is already readed data, if use batch read
void Table_ContactJournal::Reload(int nContactID,bool bSkipRead,DbRecordSet *newRecord)
{
	m_nContactID=nContactID;
	if (!bSkipRead)
	{
		Status err;
		_SERVER_CALL(BusContact->ReadContactJournals(err,nContactID,m_lstJournal))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
	}

	//assign it, if valid:
	if (newRecord)
	{
		m_lstJournal=*newRecord;
	}

	EnableToolbar(true);
	SetEditMode(true);
	RefreshDisplay();
	emit ContentChanged();
}



void Table_ContactJournal::Invalidate()
{
	m_lstJournal.clear();
	RefreshDisplay();
	SetEditMode(false);
	EnableToolbar(false);
	emit ContentChanged();
}



void Table_ContactJournal::OpenRTFEditor()
{
	RichEditorDlg dlg;
	dlg.SetHtml(m_pExtJournalEntry->toHtml());
	int nRowSelected=m_plstData->getSelectedRow();

	if(0 != dlg.exec() && nRowSelected>0)
	{
		
		m_pExtJournalEntry->setHtml(dlg.GetHtml());
		//store back to current row in data:
		//QByteArray byteValue=dlg.GetHtml();//.toLatin1().constData();
		QString f=dlg.GetHtml();
		m_plstData->setData(nRowSelected,m_nJournalEntryIdx,dlg.GetHtml());
		RefreshDisplay(nRowSelected);
	}
}


//edit journal:
void Table_ContactJournal::OnEdit()
{
	Dlg_ContactJournal Dlg;

	//get selected row:
	int nSelectedRow=m_plstData->getSelectedRow();
	if (nSelectedRow<0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select journal entry!"));
		return;
	}
	DbRecordSet row;
	row=m_plstData->getRow(nSelectedRow);

	Dlg.SetRow(row,Dlg_ContactJournal::MODE_EDIT);
	int nRes=Dlg.exec();
	if (nRes==1)			//changed
	{
		DbRecordSet newData;
		Dlg.GetRow(newData);
		m_plstData->assignRow(nSelectedRow,newData);
		RefreshDisplay();
	}
	else if (nRes==2)		//deleted
	{
		m_plstData->deleteSelectedRows();
		RefreshDisplay();
	}
	
}


//insert new journal:
void Table_ContactJournal::OnInsert()
{

	DbRecordSet Data;
	Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_JOURNAL_SELECT));
	Data.addRow();
	Data.setData(0,"BCMJ_ID",0);//reset ID to 0, if copy
	Data.setData(0,"BCMJ_DATE",QDate::currentDate()); //id = date
	Data.setData(0,"BCMJ_CONTACT_ID",m_nContactID); 

	//set default Person= logged
	int nPersonID=g_pClientManager->GetPersonID();
	if(nPersonID>0)
		Data.setData(0,"BCMJ_PERSON_ID",nPersonID); 

	//ISNERT with defaults:
	Dlg_ContactJournal Dlg;
	Dlg.SetRow(Data,Dlg_ContactJournal::MODE_INSERT);
	int nRes=Dlg.exec();
	if (nRes==1)			//changed
	{
		DbRecordSet newData;
		Dlg.GetRow(newData);
		m_plstData->merge(newData);
		RefreshDisplay();
	}

}

void Table_ContactJournal::BuildToolBar(toolbar_edit * pToolBar)
{
	m_toolbar=pToolBar;
	pToolBar->GetBtnAdd()->setText(tr("Add Journal Entry"));
	connect(pToolBar->GetBtnAdd(), SIGNAL(clicked()), this, SLOT(OnInsert()));
	connect(pToolBar->GetBtnSelect(), SIGNAL(clicked()), this, SLOT(OnShow()));
	connect(pToolBar->GetBtnModify(), SIGNAL(clicked()), this, SLOT(OnEdit()));
	connect(pToolBar->GetBtnClear(), SIGNAL(clicked()), this, SLOT(OnDeleteSelected()));
}


void Table_ContactJournal::OnShow()
{
	Dlg_ContactJournal Dlg;

	//get selected row:
	int nSelectedRow=m_plstData->getSelectedRow();
	if (nSelectedRow<0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select journal entry!"));
		return;
	}
	DbRecordSet row;
	row=m_plstData->getRow(nSelectedRow);

	Dlg.SetRow(row,Dlg_ContactJournal::MODE_READ);
	int nRes=Dlg.exec();
	if (nRes==1)			//changed
	{
		DbRecordSet newData;
		Dlg.GetRow(newData);
		m_plstData->assignRow(nSelectedRow,newData);
		RefreshDisplay();
	}
	else if (nRes==2)		//deleted
	{
		m_plstData->deleteSelectedRows();
		RefreshDisplay();
	}

}

void Table_ContactJournal::EnableToolbar(bool bEnable)
{

	if(m_toolbar!=NULL)
	{
		m_toolbar->setEnabled(bEnable);
	}
}




void Table_ContactJournal::OnDeleteSelected()
{
	DbRecordSet DataForDelete;
	DataForDelete.addColumn(QVariant::Int,"BCMJ_ID");
	DataForDelete.merge(*m_plstData,true);

	if (DataForDelete.getRowCount()>0)
	{
		Status err;

		int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete the selected journal entries {")+QVariant(DataForDelete.getRowCount()).toString()+tr("} from the database?"),tr("Yes"),tr("No"));
		if (nResult!=0) return; //only if YES

		_SERVER_CALL(BusContact->DeleteJournal(err,DataForDelete,true))
		//error:
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//refresh display:
		m_plstData->deleteSelectedRows();
		RefreshDisplay(); 
		emit ContentChanged();

	}

}

void Table_ContactJournal::Data2CustomWidget(int nRow, int nCol)
{
		if (nCol==3) //multiline
		{
			QTextEdit *pText = dynamic_cast<QTextEdit *>(cellWidget(nRow,nCol));
			if (!pText)
			{
				pText = new QTextEdit;
				pText->setReadOnly(true);
				setCellWidget(nRow,nCol,pText);
			}
			pText->setHtml(m_plstData->getDataRef(nRow,"BCMJ_TEXT").toString());
	}

}

void Table_ContactJournal::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	UniversalTableWidgetEx::RefreshDisplay(nRow,bApplyLastSortModel);
	OnSelectionChanged();
}