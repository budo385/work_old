#include "calendargraphicsview.h"

#include <QResizeEvent>
#include <QGraphicsProxyWidget>
#include <QLabel>
#include <QScrollBar>
#include <QApplication>

#include "calendarheaderitem.h"
#include "calendarpersonheaderitem.h" 
#include "calendarverticalheaderitem.h"
#include "calendarentityitem.h"
#include "calendarreservationitem.h"
#include "db_core/db_core/dbsqltableview.h"
#include "calendartaskitem.h"

CalendarDateHeaderItem::CalendarDateHeaderItem(bool bIsInMultiDayCalendar, int nColumn, QString	strString, int nHeight, QGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent /*= 0*/, Qt::WindowFlags wFlags /*= 0*/)
:CalendarHeaderItem(bIsInMultiDayCalendar, nColumn, strString, nHeight, pCalendarGraphicsView, parent, wFlags)
{
}

void CalendarDateHeaderItem::on_UpdateCalendar()
{
	QRect rect;
	rect = dynamic_cast<CalendarTableView*>(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView())->getItemRectangleForHeader(m_nColumn);
	setPos(rect.topLeft().x()+dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getVertHeaderWidth(), 0);
	resize(rect.width()-1, m_nHeight);
}

void CalendarDateHeaderItem::ConnectSignals()
{
	connect(dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView), SIGNAL(UpdateCalendar()), this, SLOT(on_UpdateCalendar()));
	connect(this, SIGNAL(columnResized(int, int, bool)), dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView), SLOT(on_columnResized(int, int, bool)));
}

CalendarTableModel::CalendarTableModel(QObject *parent)
: QAbstractTableModel(parent)
{
	m_recEntites=NULL;
}

CalendarTableModel::~CalendarTableModel()
{
	m_recEntites=NULL;
}

void CalendarTableModel::Initialize(QDate startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites)
{
	m_startDate		= startDate;
	//	QString s = m_startDate.toString();
	m_nDayInterval  = nDayInterval;
	m_timeScale		= scale;
	m_recEntites	= recEntites;
	m_lstSelectedIndexes.clear();
}

QVariant CalendarTableModel::data(const QModelIndex &index, int role /*= Qt::DisplayRole*/) const
{
	return QVariant();
}

int CalendarTableModel::rowCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
	return 24*m_timeScale;
}

int CalendarTableModel::columnCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
	if (m_recEntites!=NULL)
	{
		if (m_recEntites->getRowCount()>1)
			return m_nDayInterval*m_recEntites->getRowCount();
		else
			return m_nDayInterval;
	}
	else
		return m_nDayInterval;
}

QVariant CalendarTableModel::headerData(int section, Qt::Orientation orientation, int role /*= Qt::DisplayRole*/) const
{
	if (role == Qt::DisplayRole)
	{
		if(orientation == Qt::Horizontal)
		{
			//QString s = m_startDate.toString();
			return m_startDate.addDays(section);
		}
		else
			return section+1;
	}

	return QVariant();
}

QVariant CalendarTableModel::headerPersonData(int section) const
{
	if (m_recEntites==NULL)
		return QVariant();

	int nColumn = section%m_recEntites->getRowCount();
	return m_recEntites->getDataRef(nColumn, "ENTITY_NAME").toString();
}

int CalendarTableModel::headerEntityType(int section) const
{
	if (m_recEntites==NULL)
		return 0;

	int nColumn = section%m_recEntites->getRowCount();
	return m_recEntites->getDataRef(nColumn, "ENTITY_TYPE").toInt();
}

void CalendarTableModel::setDateInterval(QDate &startDate, int nDayInterval)
{
	m_startDate = startDate;
	m_nDayInterval = nDayInterval;
}

int	 CalendarTableModel::getDateInterval()
{
	return m_nDayInterval;
}

void CalendarTableModel::setTimeScale(CalendarWidget::timeScale scale)
{
	m_timeScale = scale;
}

QDateTime CalendarTableModel::getItemStartDateTime(QModelIndex &index)
{
	int nInterval;
	if (m_recEntites==NULL)
	{
		nInterval = index.column();
	}
	else
	{
		if (m_recEntites->getRowCount())
			nInterval = (index.column())/m_recEntites->getRowCount();
		else
			nInterval = index.column();
	}

	QDate date = headerData(nInterval, Qt::Horizontal).toDate();
	QTime time = getStartTimeForRow(index.row());
	return QDateTime(date, time);
}

QDateTime CalendarTableModel::getItemEndDateTime(QModelIndex &index)
{
	int nInterval = (index.column())/m_recEntites->getRowCount();

	QDate date = headerData(nInterval, Qt::Horizontal).toDate();
	QTime time = getEndTimeForRow(index.row());
	return QDateTime(date, time);
}

void CalendarTableModel::getEntityForCell(int nRow, int nColumn, int &nEntityID, int &nEntityType, int &nEntityPersonID)
{
	int nRecRow = nColumn%m_recEntites->getRowCount();
	nEntityID = m_recEntites->getDataRef(nRecRow, "ENTITY_ID").toInt();
	nEntityType = m_recEntites->getDataRef(nRecRow, "ENTITY_TYPE").toInt();
	nEntityPersonID = m_recEntites->getDataRef(nRecRow, "ENTITY_PERSON_ID").toInt();
}

void CalendarTableModel::getItem(int nEntityID, int nEntityType, QDateTime dateTime, QModelIndex &Index, qreal &remain)
{
	int nRow;
	QTime timeTmp = dateTime.time();
	getRowForTime(timeTmp, nRow, remain);
	QDate dtTmp = dateTime.date();
	int nColumn = getColumnForDate(nEntityID, nEntityType, dtTmp);

	Index = index(nRow, nColumn);
}

QModelIndex CalendarTableModel::getItem(QDateTime &dateTime)
{
	QModelIndex Index;
	int nRow;
	//int nColumn;
	qreal remain;
	//getColumnForDate(da)
	QTime timeTmp = dateTime.time();
	getRowForTime(timeTmp, nRow, remain);
	Index = index(nRow, 0);
	return Index;
}

int CalendarTableModel::getColumnForDate(int nEntityID, int nEntityType, QDate &date)
{
	if (m_recEntites==NULL)
		return 0;

	m_recEntites->clearSelection();
	m_recEntites->find("ENTITY_ID", nEntityID);
	m_recEntites->find("ENTITY_TYPE", nEntityType, false, true, true);
	//m_recEntites->Dump();
	int nPersonIdOrder=m_recEntites->getSelectedRow();
	int nColumn = m_startDate.daysTo(date)*m_recEntites->getRowCount()+ nPersonIdOrder;

	return nColumn;
}

void CalendarTableModel::getRowForTime(QTime &time, int &nRow, qreal &Remain)
{
	if (m_recEntites==NULL)
		return;

	qreal remain = (qreal)time.minute()/(60/m_timeScale);
	Remain = remain - (int)remain;
	nRow = time.hour()*m_timeScale + time.minute()/(60/m_timeScale);
}

QDate CalendarTableModel::getDateForColumn(int &nColumn)
{
	return headerData(nColumn, Qt::Horizontal).toDate();
}

QTime CalendarTableModel::getStartTimeForRow(int nRow)
{
	int hour = nRow/(int)m_timeScale;
	int min = (nRow%m_timeScale)*(60/m_timeScale);
	return	QTime(hour, min);
}

QTime CalendarTableModel::getEndTimeForRow(int nRow)
{
	QTime time = getStartTimeForRow(nRow+1);
	return	time;
	//	return	time.addSecs(-1);
}

CalendarGraphicsView::CalendarGraphicsView(CalendarWidget *pCalendarWidget, QWidget *parent)
:QGraphicsView(parent)
{
	m_pCalendarWidget		= pCalendarWidget;
	m_pCalendarTableModel	= new CalendarTableModel();
	m_pGraphicsScene		= new CalendarGraphicsScene();
	m_bColumnsManualResized	= false;
	m_bResizeColumnsToFit	= true;
	m_bResizeFromShowEvent  = false;
	m_bFirstTimeShow		= true;
	m_recEntites			= NULL;
	setScene(m_pGraphicsScene);
	
	//Scroll bars.
	m_pHScroll = new QScrollBar(Qt::Horizontal);
	m_pVScroll = new QScrollBar(Qt::Vertical);
	m_pHScrollBar = m_pGraphicsScene->addWidget(m_pHScroll);
	m_pVScrollBar = m_pGraphicsScene->addWidget(m_pVScroll);
	m_pHScrollBar->setZValue(4);
	m_pVScrollBar->setZValue(4);
	connect(m_pHScroll, SIGNAL(valueChanged(int)), this, SLOT(on_CustomHscrollBar_valueChanged(int)));
	connect(m_pVScroll, SIGNAL(valueChanged(int)), this, SLOT(on_CustomVscrollBar_valueChanged(int)));

	m_pCalendarTableView = new CalendarTableView(m_pCalendarWidget, this, m_pCalendarTableModel);
	m_pCalendarTableView->setZValue(1);
	m_pGraphicsScene->addItem(m_pCalendarTableView);
	
	//Create corner items.
	CreateCornerScrollBarItems();
	connect(m_pCalendarTableView, SIGNAL(horizontalScrollBarVisible(bool)), this, SLOT(on_horizontalScrollBarVisible(bool)));
	connect(m_pCalendarTableView, SIGNAL(verticalScrollBarVisible(bool)), this, SLOT(on_verticalScrollBarVisible(bool)));
	connect(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar(), SIGNAL(rangeChanged(int, int)), this, SLOT(on_HscrollBar_rangeChanged(int, int)));
	connect(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar(), SIGNAL(rangeChanged(int, int)), this, SLOT(on_VscrollBar_rangeChanged(int, int)));
	connect(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar(), SIGNAL(sliderMoved(int)), this, SLOT(on_horizontalScrollBar_sliderMoved(int)));
	connect(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar(), SIGNAL(sliderMoved(int)), this, SLOT(on_verticalScrollBar_sliderMoved(int)));
	connect(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table(), SIGNAL(SelectionChanged(QModelIndex, QModelIndex)), this, SLOT(on_SelectionChanged(QModelIndex, QModelIndex)));
	connect(dynamic_cast<CalendarTableView*>(m_pCalendarTableView), SIGNAL(openMultiDayEvent(QModelIndex)), this, SIGNAL(openMultiDayEvent(QModelIndex)));

	//Create background item.
	Label *label = new Label(Label::VERTICAL_ITEM);
	m_pBackgroundItem = new QGraphicsProxyWidget();
	m_pBackgroundItem->setWidget(label);
	m_pBackgroundItem->setZValue(0);
	m_pBackgroundItem->setPos(0,0);
	m_pBackgroundItem->setMinimumHeight(viewport()->rect().height());
	m_pBackgroundItem->setMaximumHeight(viewport()->rect().height());
	m_pBackgroundItem->setMinimumWidth(viewport()->rect().width());
	m_pBackgroundItem->setMaximumWidth(viewport()->rect().width());
	m_pGraphicsScene->addItem(m_pBackgroundItem);

	connect(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(on_VscrollBar_valueChanged(int)));
	connect(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(on_HscrollBar_valueChanged(int)));
	connect(this, SIGNAL(UpdateCalendar()), dynamic_cast<CalendarTableView*>(m_pCalendarTableView), SLOT(on_UpdateCalendar()));

	connect(m_pCalendarTableView, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(on_tableCellDoubleClicked(int, int)));

	setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
}

CalendarGraphicsView::~CalendarGraphicsView()
{
	m_recEntites				= NULL;
	m_pGraphicsScene			= NULL;
	m_pCalendarTableView		= NULL;
	m_pBackgroundItem			= NULL;
	m_pLeftDownScrollBarItem	= NULL;;
	m_pRightDownScrollBarItem	= NULL;
	m_pTopRightScrollBarItem	= NULL;
	m_pTopLeftScrollBarItem		= NULL;
}

void CalendarGraphicsView::Initialize(QDate startDate, int nDayInterval, CalendarWidget::timeScale scale, DbRecordSet *recEntites, bool bResizeColumnsToFit /*= false*/, QTime scrollToTime /*= QTime(6,0)*/)
{
	QDateTime dateTime = GetTopLeftVisibleItem();
	
	m_bResizeColumnsToFit = bResizeColumnsToFit;
	DestroyHeader();
	RemoveAllCalendarItems();

	//Initialize table view.
	m_recEntites = recEntites;
	dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Initialize(startDate, nDayInterval, scale, m_recEntites);

	//Create headers.
	CreateHeader();

	if (dateTime.isNull())
	{
		m_ScrollToDateTime = QDateTime(startDate, scrollToTime);
	}
	else
	{
		m_ScrollToDateTime = QDateTime(startDate, dateTime.time());
	}
	SetTopLeftVisibleItem(m_ScrollToDateTime);

	if (m_bResizeColumnsToFit)
		ResizeColumnsToFit();
	
	//Draw changes.
	emit UpdateCalendar();
}

void CalendarGraphicsView::AddCalendarItem(int nItemID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, QDateTime &startDateTime, 
					 QDateTime &endDateTime, QString strTitle, QString strLocation, QString strColor, 
					 QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
					 QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed,
					 QGraphicsItem* &Item)
{
	CalendarEntityItem *item = new CalendarEntityItem(nItemID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, strLocation, strColor, 
								pixIcon, strOwner, strResource, strContact, strProjects, strDescription, hshBrakeStart, hshBrakeEnd, nBcep_Status, nBCEV_IS_INFORMATION, bIsProposed, this);
	Item = item;
	m_lstCalendarEntityItems.insert(nItemID, item);
	//CheckForMultipleItemsInColumn(item);
	m_pGraphicsScene->addItem(item);
	connect(item, SIGNAL(ItemResized(QGraphicsProxyWidget *)), this, SLOT(on_ItemResized(QGraphicsProxyWidget *)));
	connect(item, SIGNAL(ItemModify(QGraphicsProxyWidget *)), this, SLOT(on_ItemModify(QGraphicsProxyWidget *)));
	connect(item, SIGNAL(ItemView(QGraphicsProxyWidget *)), this, SLOT(on_ItemView(QGraphicsProxyWidget *)));
	connect(item, SIGNAL(DropEntityOnItem(QGraphicsProxyWidget *, int, int, int)), this, SLOT(on_DropEntityOnItem(QGraphicsProxyWidget *, int, int, int)));
	connect(item, SIGNAL(ItemDelete(QGraphicsProxyWidget *)), this, SLOT(on_ItemDelete(QGraphicsProxyWidget *)));
	connect(item, SIGNAL(ItemMoved(QGraphicsProxyWidget *)), this, SLOT(on_ItemMoved(QGraphicsProxyWidget *)));
	connect(item, SIGNAL(ItemSelected(QGraphicsProxyWidget *)), this, SLOT(on_ItemSelected(QGraphicsProxyWidget *)));
	connect(this, SIGNAL(UpdateCalendar()), item, SLOT(on_UpdateCalendar()));
	connect(item, SIGNAL(ItemResizing(bool)), this, SLOT(on_ItemResizing(bool)));

	emit UpdateCalendar();
}

void CalendarGraphicsView::UpdateCalendarItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime &startDateTime, QDateTime &endDateTime, 
						QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, 
						QString strContact, QString strProjects, QString strDescription, QHash<int, qreal> hshBrakeStart, 
						QHash<int, qreal> hshBrakeEnd, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed, QGraphicsItem* &Item)
{
	QHashIterator<int, QGraphicsItem*> i(m_lstCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		if (i.key() == nCENT_ID)
		{
			CalendarEntityItem *item = dynamic_cast<CalendarEntityItem*>(i.value());
			Item = item;
			int nEntityID = item->getItemEntityID();
			int nEntityType = item->getItemEntityType();
			int nEntityPersonID = item->getItemEntityPersonID();
			int nItemBCOL_ID = item->getItemBCOL_ID();
			if (nItemBCOL_ID == nBCOL_ID)
			{
				item->UpdateEntityItem(nCENT_ID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, nEntityPersonID, 
					startDateTime, endDateTime, strTitle, strColor, strLocation, pixIcon, strOwner, strResource, strContact, strProjects, strDescription, 
					hshBrakeStart, hshBrakeEnd, nBcep_Status, nBCEV_IS_INFORMATION, bIsProposed);
			}
		}
	}

	emit UpdateCalendar();
}

void CalendarGraphicsView::RemoveCalendarItem(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID, QList<QGraphicsItem*> &Items)
{
	QHashIterator<int, QGraphicsItem*> i(m_lstCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		if (i.key() == nCENT_ID)
		{
			CalendarEntityItem *item = dynamic_cast<CalendarEntityItem*>(i.value());
			int nItemBCOL_ID = item->getItemBCOL_ID();
			if (nItemBCOL_ID == nBCOL_ID)
			{
				Items << item;
				m_lstCalendarEntityItems.remove(i.key(), i.value());
				m_pGraphicsScene->removeItem(item);
				m_lstSelectedItems.removeAll(item);
				delete(item);
			}
		}
	}

	emit UpdateCalendar();
}

void CalendarGraphicsView::AddCalendarReservation(int nEntityID, int nEntityType, int nBCRS_ID, int nBCRS_IS_POSSIBLE, QDateTime startDateTime, 
												  QDateTime endDateTime)
{
	CalendarReservationItem *item = new CalendarReservationItem(nEntityID, nEntityType, nBCRS_ID, nBCRS_IS_POSSIBLE, startDateTime, endDateTime, this);
	m_pGraphicsScene->addItem(item);
	m_lstCalendarReservationItems.insert(nBCRS_ID, item);
	connect(this, SIGNAL(UpdateCalendar()), dynamic_cast<CalendarReservationItem*>(item), SLOT(on_UpdateCalendar()));

	emit UpdateCalendar();
}

void CalendarGraphicsView::AddCalendarTaskItems(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, QPixmap Pixmap, int nEntityID, int nEntityType, int nTypeID)
{
	CalendarTaskItem *item = new CalendarTaskItem(nCENT_ID, nBTKS_ID, nCENT_SYSTEM_TYPE_ID, datTaskDate, bIsStart, Pixmap, nEntityID, nEntityType, nTypeID, this);
	m_pGraphicsScene->addItem(item);
	m_lstCalendarTaskItems.insert(nBTKS_ID, item);
	connect(this, SIGNAL(UpdateCalendar()), dynamic_cast<CalendarTaskItem*>(item), SLOT(on_UpdateCalendar()));
	connect(dynamic_cast<CalendarTaskItem*>(item), SIGNAL(ItemClicked(int, int, int, QDateTime, bool, int, int, int)), this, SIGNAL(TaskItemClicked(int, int, int, QDateTime, bool, int, int, int)));
	connect(dynamic_cast<CalendarTaskItem*>(item), SIGNAL(ItemDoubleClicked(int, int, int, QDateTime, bool, int, int, int)), this, SIGNAL(TaskItemDoubleClicked(int, int, int, QDateTime, bool, int, int, int)));

	emit UpdateCalendar();
}

void CalendarGraphicsView::RemoveAllCalendarItems()
{
	//Remove calendar items.
	QHashIterator<int, QGraphicsItem*> i(m_lstCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		m_pGraphicsScene->removeItem(i.value());
	}

	//Remove calendar reservations.
	QHashIterator<int, QGraphicsItem*> j(m_lstCalendarReservationItems);
	while (j.hasNext()) 
	{
		j.next();
		m_pGraphicsScene->removeItem(j.value());
	}

	//Remove task items.
	QHashIterator<int, QGraphicsItem*> k(m_lstCalendarTaskItems);
	while (k.hasNext()) 
	{
		k.next();
		m_pGraphicsScene->removeItem(k.value());
	}
	
	//Delete everything.
	qDeleteAll(m_lstCalendarEntityItems);
	m_lstCalendarEntityItems.clear();
	qDeleteAll(m_lstCalendarReservationItems);
	m_lstCalendarReservationItems.clear();
	qDeleteAll(m_lstCalendarTaskItems);
	m_lstCalendarTaskItems.clear();

	//Selected items list.
	m_lstSelectedItems.clear();
}

void CalendarGraphicsView::getSelectedItemDateTime(QDateTime &startDateTime, QDateTime &endDateTime)
{
	dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->getSelectionStartEndDateTime(startDateTime, endDateTime);
}

int CalendarGraphicsView::GetMultiDayColumnWidth()
{
	return m_nColumnWidth;
}

QDateTime CalendarGraphicsView::GetTopLeftVisibleItem()
{
	return dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->GetTopLeftVisibleItem();
}

void CalendarGraphicsView::SetTopLeftVisibleItem(QDateTime dateTime)
{
	dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->SetTopLeftVisibleItem(dateTime);
}

bool CalendarGraphicsView::ItemExists(int nCentID, int nEntityID, int nEntityType, int nBCOL_ID)
{
	QList<QGraphicsItem*> lstItems;
	lstItems = m_lstCalendarEntityItems.values(nCentID);
	QListIterator<QGraphicsItem*> iter(lstItems);
	while(iter.hasNext())
	{
		QGraphicsItem* item = iter.next();
		int nItemEntityID = dynamic_cast<CalendarEntityItem*>(item)->getItemEntityID();
		int nItemEntityType = dynamic_cast<CalendarEntityItem*>(item)->getItemEntityType();
		int nItemBCOL_ID = dynamic_cast<CalendarEntityItem*>(item)->getItemBCOL_ID();
		
		if (nItemEntityType == nEntityType && nItemEntityID == nEntityID && nItemBCOL_ID == nBCOL_ID)
			return true;
	}
	
	return false;
}

void CalendarGraphicsView::ResizeColumnsToFit(bool bForce /*= false*/)
{
	CalculateColumnWidth();
	on_columnResized(0, m_nColumnWidth, true);
}

void CalendarGraphicsView::on_ItemSelected(QGraphicsProxyWidget *thisItem)
{
	if (QApplication::keyboardModifiers().testFlag(Qt::ControlModifier) || QApplication::keyboardModifiers().testFlag(Qt::ShiftModifier))
	{
		if (m_lstSelectedItems.contains(thisItem))
			UnselectOneItem(thisItem);
		else
		{
			m_lstSelectedItems.append(thisItem);
			dynamic_cast<CalendarEntityItem*>(thisItem)->SetSelected(true);
		}
	}
	else
	{
		if (!m_lstSelectedItems.contains(thisItem))
		{
			UnselectAllItems();
			m_lstSelectedItems.append(thisItem);
			dynamic_cast<CalendarEntityItem*>(thisItem)->SetSelected(true);
		}
		else
			UnselectOneItem(thisItem);
	}
}

CalendarGraphicsScene* CalendarGraphicsView::getScene()
{
	return m_pGraphicsScene;
}

QGraphicsProxyWidget* CalendarGraphicsView::getTableView()
{
	return m_pCalendarTableView;
}

int CalendarGraphicsView::getTotalHeaderHeight()
{
	return m_pCalendarWidget->getDateHeaderHeight()+m_pCalendarWidget->getPersonHeaderHeight();
}

void CalendarGraphicsView::showLeftDownItem(bool bShow /*= true*/)
{
	if (bShow)
	{
	}
}

void CalendarGraphicsView::PositionCornerScrollBarItems()
{
	m_pTopRightScrollBarItem->setPos(viewport()->rect().width()-dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->rect().width(),0);
	m_pTopRightScrollBarItem->setMinimumHeight(getTotalHeaderHeight());
	m_pTopRightScrollBarItem->setMaximumHeight(getTotalHeaderHeight());
	m_pTopRightScrollBarItem->setMinimumWidth(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->rect().width());
	m_pTopRightScrollBarItem->setMaximumWidth(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->rect().width());

	m_pLeftDownScrollBarItem->setPos(0, viewport()->rect().height()-dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->rect().height());
	m_pLeftDownScrollBarItem->setMinimumHeight(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->rect().height());
	m_pLeftDownScrollBarItem->setMaximumHeight(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->rect().height());
	m_pLeftDownScrollBarItem->setMinimumWidth(getVertHeaderWidth());
	m_pLeftDownScrollBarItem->setMaximumWidth(getVertHeaderWidth());

	m_pTopLeftScrollBarItem->setPos(0, 0);
	m_pTopLeftScrollBarItem->setMinimumHeight(m_pCalendarWidget->getDateHeaderHeight()+m_pCalendarWidget->getPersonHeaderHeight());
	m_pTopLeftScrollBarItem->setMaximumHeight(m_pCalendarWidget->getDateHeaderHeight()+m_pCalendarWidget->getPersonHeaderHeight());
	m_pTopLeftScrollBarItem->setMinimumWidth(m_pCalendarWidget->getVertHeaderWidthForLowerCalendar());
	m_pTopLeftScrollBarItem->setMaximumWidth(m_pCalendarWidget->getVertHeaderWidthForLowerCalendar());

	m_pRightDownScrollBarItem->setPos(viewport()->rect().width()-dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->rect().width(),viewport()->rect().height()-dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->rect().height());
	m_pRightDownScrollBarItem->setMinimumHeight(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->rect().height());
	m_pRightDownScrollBarItem->setMaximumHeight(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->rect().height());
	m_pRightDownScrollBarItem->setMinimumWidth(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->rect().width());
	m_pRightDownScrollBarItem->setMaximumWidth(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->rect().width());
}

void CalendarGraphicsView::CheckForMultipleItemsInColumn(QGraphicsItem *newItem)
{
	QList<QGraphicsItem*> colidedItems;
	GetColidedItems(newItem, colidedItems, false);
	SortColidedItems(colidedItems);
	GetItemsPosAndWidth(colidedItems);
}

void CalendarGraphicsView::GetItemsPosAndWidth(QList<QGraphicsItem*> colidedItems)
{
	int nColumnCount;
	int nColidedItemsCount = colidedItems.count();
	for (int i = 0; i < nColidedItemsCount; i++)
	{
		int nItemHOrder = 0;
		QList<int> lstPossibleHOrder;
		QList<int> lstNotPossibleHOrder;
		CalendarEntityItem *Item = dynamic_cast<CalendarEntityItem*>(colidedItems.value(i));
		//If first item place it first.
		if (!i)
		{
			Item->setColumnCount(1);
			nColumnCount = 1;
			lstPossibleHOrder << nItemHOrder;
		}
		else
		{
			for (int j = 0; j < i; j++)
			{
				CalendarEntityItem *previousItem = dynamic_cast<CalendarEntityItem*>(colidedItems.value(j));
				int nPreviousItemHOrder = previousItem->getHorOrder();
				int nPreviousItemColumnCount = previousItem->getColumnCount();
				
				if (ItemsColide(Item, previousItem))
				{
					int nNewItemHOrder = nPreviousItemHOrder+1;
					int nNewItemColumncCount = nPreviousItemColumnCount+1;
					nItemHOrder = nNewItemHOrder;
					//Put previous item horizontal position in not possible list and add new one.
					lstNotPossibleHOrder << nPreviousItemHOrder;
					lstPossibleHOrder << nItemHOrder;
				}
				else
				{
					lstPossibleHOrder << nItemHOrder;
				}
			}

		}
		
		int nHOrder;
		bool bFirstIn = true;
		int nPossibleHOrder = lstPossibleHOrder.count();
		for (int i = 0; i < nPossibleHOrder; i++)
		{
			if (!lstNotPossibleHOrder.contains(lstPossibleHOrder.value(i)))
			{
				if (bFirstIn)
				{
					nHOrder = lstPossibleHOrder.value(i);
					bFirstIn = false;
				}
				else
				{
					if (lstPossibleHOrder.value(i) < nHOrder)
						nHOrder = lstPossibleHOrder.value(i);
				}
			}
		}
		Item->setHorOrder(nHOrder);
		if ((nHOrder+1) > nColumnCount)
			nColumnCount = nHOrder+1;
	}

	for (int i = 0; i < nColidedItemsCount; i++)
	{
		CalendarEntityItem *Item = dynamic_cast<CalendarEntityItem*>(colidedItems.value(i));
		Item->setColumnCount(nColumnCount);
	}
}

void CalendarGraphicsView::GetColidedItems(QGraphicsItem *Item, QList<QGraphicsItem*> &colidedItems, bool bThisItemOnly)
{
	int nEntityID = dynamic_cast<CalendarEntityItem*>(Item)->getItemEntityID();
	int nEntityType = dynamic_cast<CalendarEntityItem*>(Item)->getItemEntityType();
	QDateTime startDateTime, endDateTime;
	dynamic_cast<CalendarEntityItem*>(Item)->getStartEndDateTime(startDateTime, endDateTime);
	QRect newRect = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->getItemRectangle(nEntityID, nEntityType, startDateTime, endDateTime);
	QDate dtTmp1 = startDateTime.date(); 
	int nItemColumn = m_pCalendarTableModel->getColumnForDate(nEntityID, nEntityType, dtTmp1);

	//First get collided items.
	QHashIterator<int, QGraphicsItem *> i(m_lstCalendarEntityItems);
	while (i.hasNext()) 
	{
		i.next();
		CalendarEntityItem *item = dynamic_cast<CalendarEntityItem*>(i.value());
		int nThisEntityID = item->getItemEntityID();
		int nThisEntityType = item->getItemEntityType();
		QDateTime itemStartDateTime, itemEndDateTime;
		item->getStartEndDateTime(itemStartDateTime, itemEndDateTime);
		QRect itemRect = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->getItemRectangle(nThisEntityID, nThisEntityType, itemStartDateTime, itemEndDateTime);
		QDate dtTmp = itemStartDateTime.date();
		int nThisItemColumn = m_pCalendarTableModel->getColumnForDate(nThisEntityID, nThisEntityType, dtTmp);

		if (nThisItemColumn == nItemColumn)
		{
			//int ii = item->getItemEntityID();
			//int jj = item->getItemEntityType();
			//QString ss = itemStartDateTime.date().toString();
			//QString sss = startDateTime.date().toString();

			//Check.
			if (itemStartDateTime.date() == startDateTime.date() && nEntityID == nThisEntityID && nEntityType == nThisEntityType)
			{
				//if (newRect.intersects(itemRect))
				if (newRect.intersected(itemRect).height()>1)
				{
					if (!colidedItems.contains(item))
					{
						colidedItems << item;
						if (!bThisItemOnly)
							GetColidedItems(item, colidedItems, false);
					}
				}
			}
		}
	}
}

void CalendarGraphicsView::GetColidedItemsForRemove(QGraphicsItem *itemForRemove, QList<QGraphicsItem*> &colidedItems)
{
	GetColidedItems(itemForRemove, colidedItems, false);
	colidedItems.removeAll(itemForRemove);
}

bool CalendarGraphicsView::ItemsColide(QGraphicsItem *Item1, QGraphicsItem *Item2)
{
	int nEntityID1 = dynamic_cast<CalendarEntityItem*>(Item1)->getItemEntityID();
	int nEntityType1 = dynamic_cast<CalendarEntityItem*>(Item1)->getItemEntityType();
	QDateTime startDateTime1, endDateTime1;
	dynamic_cast<CalendarEntityItem*>(Item1)->getStartEndDateTime(startDateTime1, endDateTime1);
	QRect item1Rect = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->getItemRectangle(nEntityID1, nEntityType1, startDateTime1, endDateTime1);

	int nEntityID2 = dynamic_cast<CalendarEntityItem*>(Item2)->getItemEntityID();
	int nEntityType2 = dynamic_cast<CalendarEntityItem*>(Item2)->getItemEntityType();
	QDateTime itemStartDateTime2, itemEndDateTime2;
	dynamic_cast<CalendarEntityItem*>(Item2)->getStartEndDateTime(itemStartDateTime2, itemEndDateTime2);
	QRect item2Rect = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->getItemRectangle(nEntityID2, nEntityType2, itemStartDateTime2, itemEndDateTime2);


	//Check.
	if (itemStartDateTime2.date() == startDateTime1.date() && nEntityID1 == nEntityID2 && nEntityType1 == nEntityType2)
	{
		//if (newRect.intersects(itemRect))
		if (item1Rect.intersected(item2Rect).height()>1)
			return true;
	}

	return false;
}

void CalendarGraphicsView::CheckForMultipleItemsInColumnForRemove(QGraphicsItem *Item)
{
	QList<QGraphicsItem*> colidedItems;
	GetColidedItemsForRemove(Item, colidedItems);
	SortColidedItems(colidedItems);
	GetItemsPosAndWidth(colidedItems);
}

void CalendarGraphicsView::SortColidedItems(QList<QGraphicsItem*> &colidedItems)
{
	DbRecordSet recColidedItems;
	recColidedItems.addColumn(QVariant::Int, "ORDER");
	recColidedItems.addColumn(QVariant::DateTime, "START_DATETIME");
	recColidedItems.addColumn(QVariant::Int, "DURATION");
	int nCount = 0;	
	QListIterator<QGraphicsItem*> j(colidedItems);
	while (j.hasNext())
	{
		CalendarEntityItem *colItem = dynamic_cast<CalendarEntityItem*>(j.next());
		QDateTime itemStartDateTime, itemEndDateTime;
		colItem->getStartEndDateTime(itemStartDateTime, itemEndDateTime);
		recColidedItems.addRow();
		recColidedItems.setData(nCount, "ORDER", nCount);
		recColidedItems.setData(nCount, "START_DATETIME", itemStartDateTime);
		recColidedItems.setData(nCount, "DURATION", itemStartDateTime.time().secsTo(itemEndDateTime.time()));
		nCount++;
	}

	SortDataList defaultSortList;
	SortData sortDate1(1 /*START_DATETIME*/,  0 /* = ASCENDING*/);
	SortData sortTime1(2 /*DURATION*/, 1 /* = ASCENDING*/);
	defaultSortList << sortDate1 << sortTime1;
	recColidedItems.sortMulti(defaultSortList);
//	_DUMP(recColidedItems);

	QList<QGraphicsItem*> tmpColidedItems;
	int nRowCount = recColidedItems.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nOrder = recColidedItems.getDataRef(i, "ORDER").toInt();
		tmpColidedItems.append(colidedItems.value(nOrder));
	}
	colidedItems.clear();
	colidedItems = tmpColidedItems;
}

void CalendarGraphicsView::CreateCornerScrollBarItems()
{
	Label *label = new Label(Label::BOTTOM_LEFT_ITEM);
	Label *label1 = new Label(Label::TOP_RIGHT_ITEM);
	Label *label2 = new Label(Label::TOP_LEFT_ITEM);
	Label *label3 = new Label(Label::BOTTOM_RIGHT_ITEM);

	m_pLeftDownScrollBarItem	= new QGraphicsProxyWidget();
	m_pTopRightScrollBarItem	= new TopRightItem();
	connect(dynamic_cast<TopRightItem*>(m_pTopRightScrollBarItem), SIGNAL(ResizeColumnsToFit(bool)), this, SLOT(ResizeColumnsToFit(bool)));
	m_pTopLeftScrollBarItem		= new QGraphicsProxyWidget();
	m_pRightDownScrollBarItem	= new QGraphicsProxyWidget();

	m_pLeftDownScrollBarItem->setWidget(label);
	m_pTopRightScrollBarItem->setWidget(label1);
	m_pTopLeftScrollBarItem->setWidget(label2);
	m_pRightDownScrollBarItem->setWidget(label3);

	m_pLeftDownScrollBarItem->setZValue(10);
	m_pTopRightScrollBarItem->setZValue(7);
	m_pTopLeftScrollBarItem->setZValue(10);
	m_pRightDownScrollBarItem->setZValue(10);

	m_pGraphicsScene->addItem(m_pLeftDownScrollBarItem);
	m_pGraphicsScene->addItem(m_pTopRightScrollBarItem);
	m_pGraphicsScene->addItem(m_pTopLeftScrollBarItem);
	m_pGraphicsScene->addItem(m_pRightDownScrollBarItem);
}

void CalendarGraphicsView::PositionCustomScrollBars()
{
	//Horizontal scroll bar.
	m_pHScroll->resize(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->width(), dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->height());
	m_pHScroll->setRange(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->minimum(), dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->maximum());
	m_pHScroll->setPageStep(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->pageStep());
	m_pHScrollBar->setPos(getVertHeaderWidth(), frameRect().bottomLeft().y()-dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->frameGeometry().height());
	m_pVScroll->resize(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->width(), dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->height());
	m_pVScroll->setRange(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->minimum(), dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->maximum());
	m_pVScroll->setPageStep(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->pageStep());
	m_pVScrollBar->setPos(frameRect().topRight().x()-dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->frameGeometry().width(), getTotalHeaderHeight());
}

void CalendarGraphicsView::CalculateColumnWidth()
{
	//If columns are shorter than view width resize to fit.
	int nColumnWidth = (viewport()->rect().width()-getVertHeaderWidth()-m_pVScroll->width())/(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model()->columnCount());
	int nDiv = 1;

	if (m_recEntites!=NULL)
	{
		if (m_recEntites->getRowCount())
			nDiv = m_recEntites->getRowCount();
	}

	m_nColumnWidth = nColumnWidth*nDiv;
}

bool CalendarGraphicsView::AllColumnsAreInViewArea(int nViewportWidth)
{
	int nColumncount = (dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model()->columnCount());
	int nEntitesCount = 1;

	if (m_recEntites!=NULL)
	{
		if (m_recEntites->getRowCount()>0)
			nEntitesCount = m_recEntites->getRowCount();
	}

	int nAllColumnsWidth = m_nColumnWidth*nColumncount/nEntitesCount;
	
	int nViewPort = nViewportWidth-getVertHeaderWidth()-m_pVScroll->width();
	if (nViewPort >= nAllColumnsWidth)
		return true;
	else
		return false;
}

void CalendarGraphicsView::UnselectOneItem(QGraphicsItem* Item)
{
	dynamic_cast<CalendarEntityItem*>(Item)->SetSelected(false);
	m_lstSelectedItems.removeAll(Item);
}

void CalendarGraphicsView::UnselectAllItems()
{
	int nLstSize = m_lstSelectedItems.size();
	for (int i = 0; i < nLstSize; i++)
		dynamic_cast<CalendarEntityItem*>(m_lstSelectedItems.value(i))->SetSelected(false);

	m_lstSelectedItems.clear();
}

void CalendarGraphicsView::CreateHeader()
{
	CreateCalendarHeader();
	CreatePersonHeader();
	CreateVerticalHeader();
}

void CalendarGraphicsView::DestroyHeader()
{
	DestroyCalendarHeader();
	DestroyPersonHeader();
	DestroyVerticalHeader();
}

void CalendarGraphicsView::CreateCalendarHeader()
{
	//Create column header items.
	int nColumnCount = dynamic_cast<CalendarTableModel*>(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model())->getDateInterval();
	for (int i = 0; i < nColumnCount; i++)
	{
		int nHeight = m_pCalendarWidget->getDateHeaderHeight();
		QString strString = dynamic_cast<CalendarTableView*>(getTableView())->Model()->headerData(i, Qt::Horizontal).toDate().toString("ddd dd.MM.yyyy");
		CalendarDateHeaderItem *item = new CalendarDateHeaderItem(false, i, strString, nHeight, this);
		item->ConnectSignals();
		m_pGraphicsScene->addItem(item);
		m_lstHeaderItems << item;
	}
}

void CalendarGraphicsView::CreatePersonHeader()
{
	if (m_recEntites==NULL)
		return;

	if (m_recEntites->getRowCount()<=1)
		return;

	//Create column header items.
	int nColumnCount = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model()->columnCount();
	for (int i = 0; i < nColumnCount; i++)
	{
		int nEntityType = m_pCalendarTableModel->headerEntityType(i);
		QIcon ico;
		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
			ico = QIcon(":Contacts16.png");
		if (nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT)
			ico = QIcon(":Contacts16.png");
		if (nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
			ico = QIcon(":Projects16.png");
		if (nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
			ico = QIcon(":Resource16.png");

		CalendarPersonHeaderItem *item = new CalendarPersonHeaderItem(i, ico, m_pCalendarTableModel->headerPersonData(i).toString(), m_pCalendarWidget->getPersonHeaderHeight(), this, m_pCalendarTableView, m_pCalendarTableModel);
		m_pGraphicsScene->addItem(item);
		m_lstPersonHeaderItems << item;
	}
}

void CalendarGraphicsView::CreateVerticalHeader()
{
	int nRowCount = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model()->rowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		CalendarVerticalHeaderItem *item = new CalendarVerticalHeaderItem(i, getTotalHeaderHeight(), getVertHeaderWidth(), m_pCalendarWidget->getRowHeight(), m_pCalendarTableModel->getTimeScale(), this, m_pCalendarTableView);
		m_pGraphicsScene->addItem(item);
		m_lstVerticalHeaderItems << item;
	}
}

void CalendarGraphicsView::DestroyCalendarHeader()
{
	int nListCount = m_lstHeaderItems.count();
	for (int i = 0; i < nListCount; ++i)
	{
		m_pGraphicsScene->removeItem(m_lstHeaderItems.value(i));
		delete(m_lstHeaderItems.value(i));
	}
	m_lstHeaderItems.clear();
}

void CalendarGraphicsView::DestroyPersonHeader()
{
	int nListCount = m_lstPersonHeaderItems.count();
	for (int i = 0; i < nListCount; ++i)
	{
		m_pGraphicsScene->removeItem(m_lstPersonHeaderItems.value(i));
		delete(m_lstPersonHeaderItems.value(i));
	}
	m_lstPersonHeaderItems.clear();
}

void CalendarGraphicsView::DestroyVerticalHeader()
{
	int nListCount = m_lstVerticalHeaderItems.count();
	for (int i = 0; i < nListCount; ++i)
	{
		m_pGraphicsScene->removeItem(m_lstVerticalHeaderItems.value(i));
		delete(m_lstVerticalHeaderItems.value(i));
	}
	m_lstVerticalHeaderItems.clear();
}

void CalendarGraphicsView::UpdateCalendarSignal()
{
	DestroyHeader();
	CreateHeader();
	ResizeColumnsToFit();
	emit UpdateCalendar();
}

void CalendarGraphicsView::UpdateCalendarSignalFromOutside()
{
	emit UpdateCalendar();
}

void CalendarGraphicsView::on_VscrollBar_valueChanged(int value)
{
	//dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->setValue(value);
	m_pVScroll->setValue(value);
	int m = m_pVScroll->minimum();
	int mx = m_pVScroll->maximum();
	//update();
	emit UpdateCalendar();
}

void CalendarGraphicsView::on_HscrollBar_valueChanged(int value)
{
	m_pHScroll->setValue(value);
	emit UpdateCalendar();

	if (m_recEntites==NULL)
		return;
	if (m_recEntites->getRowCount()==1)
		emit scrollBar_valueChanged(value);
}

void CalendarGraphicsView::on_CustomVscrollBar_valueChanged(int value)
{
	dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->setValue(value);
}

void CalendarGraphicsView::on_CustomHscrollBar_valueChanged(int value)
{
	dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->setValue(value);
}

void CalendarGraphicsView::on_VscrollBar_rangeChanged(int min, int max)
{
	m_pVScroll->resize(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->width(), dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->height());
	m_pVScroll->setRange(min, max);
	m_pVScroll->setPageStep(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->verticalScrollBar()->pageStep());
}

void CalendarGraphicsView::on_HscrollBar_rangeChanged(int min, int max)
{
	m_pHScroll->resize(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->width(), dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->height());
	m_pHScroll->setRange(min, max);
	m_pHScroll->setPageStep(dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->horizontalScrollBar()->pageStep());
}

void CalendarGraphicsView::on_horizontalScrollBar_sliderMoved(int value)
{
	m_pHScroll->setValue(value);
}

void CalendarGraphicsView::on_verticalScrollBar_sliderMoved(int value)
{
	m_pVScroll->setValue(value);
}

void CalendarGraphicsView::on_columnResized(int nColumn, int nWidth, bool bResizeAll)
{
	if (bResizeAll)
	{
		int nColumnCount = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model()->columnCount();
		for (int i = 0; i < nColumnCount; ++i)
			dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->setColumnWidth(i, nWidth);
		m_bColumnsManualResized = false;
	}
	else
	{
		//If resized manually don't stretch columns any more.
		m_bColumnsManualResized = true;
		dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->setColumnWidth(nColumn, nWidth);
	}
	
	m_nColumnWidth = nWidth;

	emit columnResized(nColumn, nWidth, bResizeAll);
 	emit UpdateCalendar();
}

void CalendarGraphicsView::on_horizontalScrollBarVisible(bool bVisible)
{
	if(bVisible)
	{
		m_pLeftDownScrollBarItem->show();
		m_pHScrollBar->show();
		if (m_pVScrollBar->isVisible())
			m_pRightDownScrollBarItem->show();
		else
			m_pRightDownScrollBarItem->hide();
		PositionCustomScrollBars();
	}
	else
	{
		m_pLeftDownScrollBarItem->hide();
		m_pRightDownScrollBarItem->hide();
		m_pHScrollBar->hide();
	}
}

void CalendarGraphicsView::on_verticalScrollBarVisible(bool bVisible)
{
	if(bVisible)
	{
		m_pTopRightScrollBarItem->show();
		m_pVScrollBar->show();
		if (m_pHScrollBar->isVisible())
			m_pRightDownScrollBarItem->show();
		else
			m_pRightDownScrollBarItem->hide();
		PositionCustomScrollBars();
	}
	else
	{
		m_pTopRightScrollBarItem->hide();
		m_pRightDownScrollBarItem->hide();
		m_pVScrollBar->hide();
	}
}

void CalendarGraphicsView::on_ItemResized(QGraphicsProxyWidget *thisItem)
{
	QRectF rect0 = thisItem->boundingRect();
	if (rect0.height()<=10)
		rect0.setHeight(10);
	
	QRectF rectScene = thisItem->mapRectToScene(rect0);
	QRectF rectl = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->mapRectFromScene(rectScene);

	QModelIndex topLeftIndex = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->indexAt(rectl.topLeft().toPoint());
	QModelIndex bottomRightIndex = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->indexAt(QPoint(rectl.bottomRight().x()-1, rectl.bottomRight().y()-1));

	if (!bottomRightIndex.isValid())
	{
		int nColumn = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model()->columnCount()-1;
		int nRow = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->rowAt(rectl.bottomRight().y());
		bottomRightIndex = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model()->index(nRow, nColumn);
	}

	//If selection don't goes over half of selected row use row before.
	QRect rectBottomItemRect = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->visualRect(bottomRightIndex);
	QRectF rectSceneBottomitem = thisItem->mapRectToScene(rectBottomItemRect);
	QRectF rectBottomItem = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->mapRectFromScene(rectSceneBottomitem);
	int nHeight = rectBottomItemRect.height();
	int nItemHeight = rectBottomItemRect.bottomRight().y() - rectl.bottomRight().y();
	if (nItemHeight > (rectBottomItemRect.height()/2))
	{
		bottomRightIndex = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Model()->index(bottomRightIndex.row()-1, bottomRightIndex.column());
	}

	QItemSelectionModel *selModel = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->selectionModel();
	QItemSelection selection(topLeftIndex, bottomRightIndex);

	selModel->clearSelection();
	selModel->select(selection, QItemSelectionModel::Select);

	QDateTime datSelectionFrom = m_pCalendarTableModel->getItemStartDateTime(topLeftIndex);
	QDateTime datSelectionTo = m_pCalendarTableModel->getItemEndDateTime(bottomRightIndex);
	QDateTime datItemFrom, datItemTo;
	dynamic_cast<CalendarEntityItem*>(thisItem)->getStartEndDateTime(datItemFrom, datItemTo);
	int nEntityID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemEntityID();
	int nEntityType = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemEntityType();
	int nCentID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBCOL_ID();

	/*QString s = datSelectionFrom.toString();
	QString ss = datSelectionTo.toString();
	QString sss = datItemFrom.toString();
	QString ssss = datItemTo.toString();
	*/

	emit ItemResized(datSelectionFrom, datSelectionTo, nEntityID, nEntityType, nCentID, nBcepID, nBcevID, nBCOL_ID, datItemFrom, datItemTo);
	on_ItemResizing(false);
}

void CalendarGraphicsView::on_ItemResizing(bool bIsResizing)
{
	dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->ItemResizing(bIsResizing);
}

void CalendarGraphicsView::on_ItemModify(QGraphicsProxyWidget *thisItem)
{
	int nCentID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBCOL_ID();
	emit ItemModify(nCentID, nBcepID, nBcevID, nBCOL_ID);
}

void CalendarGraphicsView::on_ItemView(QGraphicsProxyWidget *thisItem)
{
	int nCentID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBCOL_ID();
	emit ItemView(nCentID, nBcepID, nBcevID, nBCOL_ID);
}



void CalendarGraphicsView::on_DropEntityOnItem(QGraphicsProxyWidget *thisItem, int nEntityID, int nEntityType, int nEntityPersonID)
{
	int nCentID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBCOL_ID();
	emit DropEntityOnItem(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, nEntityPersonID);
}

void CalendarGraphicsView::on_ItemDelete(QGraphicsProxyWidget *thisItem)
{
	//If multiple items selected send all for delete.
	if (m_lstSelectedItems.count()<=1)
	{
		int nCentID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemCentID();
		int nBcepID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcepID();
		int nBcevID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcevID();
		int nBCOL_ID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBCOL_ID();
		int nEntityID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemEntityID();
		int nEntityType = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemEntityType();
		emit ItemDelete(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType);
	}
	else
	{
		DbRecordSet recSelectedItemsData;
		recSelectedItemsData.addColumn(QVariant::Int, "nCentID");
		recSelectedItemsData.addColumn(QVariant::Int, "nBcepID");
		recSelectedItemsData.addColumn(QVariant::Int, "nBcevID");
		recSelectedItemsData.addColumn(QVariant::Int, "nBCOL_ID");
		recSelectedItemsData.addColumn(QVariant::Int, "nEntityID");
		recSelectedItemsData.addColumn(QVariant::Int, "nEntityType");

		int nSelItemsCount = m_lstSelectedItems.count();
		for (int i = 0; i < nSelItemsCount; i++)
		{
			CalendarEntityItem* Item = dynamic_cast<CalendarEntityItem*>(m_lstSelectedItems.value(i));
			int nCentID = Item->getItemCentID();
			int nBcepID = Item->getItemBcepID();
			int nBcevID = Item->getItemBcevID();
			int nBCOL_ID = Item->getItemBCOL_ID();
			int nEntityID = Item->getItemEntityID();
			int nEntityType = Item->getItemEntityType();
			
			int nRow = recSelectedItemsData.getRowCount();
			recSelectedItemsData.addRow();
			recSelectedItemsData.setData(nRow, "nCentID", nCentID);
			recSelectedItemsData.setData(nRow, "nBcepID", nBcepID);
			recSelectedItemsData.setData(nRow, "nBcevID", nBcevID);
			recSelectedItemsData.setData(nRow, "nBCOL_ID", nBCOL_ID);
			recSelectedItemsData.setData(nRow, "nEntityID", nEntityID);
			recSelectedItemsData.setData(nRow, "nEntityType", nBcepID);
		}

		emit ItemsDelete(recSelectedItemsData);
	}
}

void CalendarGraphicsView::on_ItemMoved(QGraphicsProxyWidget *thisItem)
{
	QItemSelectionModel *selModel = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->Table()->selectionModel();
	QModelIndexList lstSelIndexes = selModel->selectedIndexes();
	if (lstSelIndexes.isEmpty())
		return;
	
	QModelIndex selIndex = lstSelIndexes.first();
	QDateTime datSelectionFrom = m_pCalendarTableModel->getItemStartDateTime(selIndex);
	QDateTime datSelectionTo = m_pCalendarTableModel->getItemEndDateTime(selIndex);
	QDateTime datItemFrom, datItemTo;
	dynamic_cast<CalendarEntityItem*>(thisItem)->getStartEndDateTime(datItemFrom, datItemTo);
	int nEntityID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemEntityID();
	int nEntityType = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemEntityType();
	int nCentID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemCentID();
	int nBcepID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcepID();
	int nBcevID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBcevID();
	int nBCOL_ID = dynamic_cast<CalendarEntityItem*>(thisItem)->getItemBCOL_ID();

	emit ItemMoved(datSelectionFrom, datSelectionTo, nEntityID, nEntityType, nCentID, nBcepID, nBcevID, nBCOL_ID, datItemFrom, datItemTo);
}

void CalendarGraphicsView::on_tableCellDoubleClicked(int row, int column)
{
	QModelIndex selectedCell	= m_pCalendarTableModel->index(row, column);
	QDateTime datSelectionFrom	= m_pCalendarTableModel->getItemStartDateTime(selectedCell);
	QDateTime datSelectionTo	= m_pCalendarTableModel->getItemEndDateTime(selectedCell);
	int nEntityID, nEntityType, nEntityPersonID;
	m_pCalendarTableModel->getEntityForCell(row, column, nEntityID, nEntityType, nEntityPersonID);
	
	emit tableCellDoubleClicked(datSelectionFrom, datSelectionTo, nEntityID, nEntityType);
}

void CalendarGraphicsView::on_SelectionChanged(QModelIndex topLeftSelectedIndex, QModelIndex bottomRightSelectedIndex)
{
	if (topLeftSelectedIndex.row()>bottomRightSelectedIndex.row())
	{
		QModelIndex tmpIndex = topLeftSelectedIndex;
		topLeftSelectedIndex = bottomRightSelectedIndex;
		bottomRightSelectedIndex = tmpIndex;
	}
	
	QDateTime datSelectionFrom	= m_pCalendarTableModel->getItemStartDateTime(topLeftSelectedIndex);
	QDateTime datSelectionTo	= m_pCalendarTableModel->getItemEndDateTime(bottomRightSelectedIndex);
	
	//Set selection entities.
	QHash<int, int> hshAddedEntityID, hshAddedEntityType;
	DbRecordSet recSelectedEntites;
	recSelectedEntites.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	int nColumnCount = abs(bottomRightSelectedIndex.column()-topLeftSelectedIndex.column()+1);
	for(int i = 0; i < nColumnCount; i++)
	{
		int nEntityID, nEntityType, nEntityPersonID;
		m_pCalendarTableModel->getEntityForCell(topLeftSelectedIndex.row(), topLeftSelectedIndex.column()+i, nEntityID, nEntityType, nEntityPersonID);
		
		QList<int> lstEntityIDKeys = hshAddedEntityID.keys(nEntityID);
		if (lstEntityIDKeys.isEmpty())
		{
			AddEntityToSelectionRecordset(nEntityID, nEntityType, nEntityPersonID, hshAddedEntityID, hshAddedEntityType, recSelectedEntites);
		}
		else
		{
			bool bAddEntity = true;
			QListIterator<int> iter(lstEntityIDKeys);
			while (iter.hasNext())
			{
				int nkey = iter.next();
				if (hshAddedEntityType.keys(nEntityType).contains(nkey))
					bAddEntity = false;
			}
			if (bAddEntity)
				AddEntityToSelectionRecordset(nEntityID, nEntityType, nEntityPersonID, hshAddedEntityID, hshAddedEntityType, recSelectedEntites);
		}
	}

	//Set selection dates.
	if (datSelectionFrom > datSelectionTo)
	{
		QDateTime datTmp = datSelectionFrom;
		datSelectionFrom = datSelectionTo;
		datSelectionTo = datTmp;
	}

	emit selectionChanged(datSelectionFrom, datSelectionTo, recSelectedEntites);
}

void CalendarGraphicsView::AddEntityToSelectionRecordset(int nEntityID, int nEntityType, int nEntityPersonID, QHash<int, int> &hshAddedEntityID, QHash<int, int> &hshAddedEntityType, DbRecordSet &recSelectedEntites)
{
	recSelectedEntites.addRow();
	int nRow = recSelectedEntites.getRowCount()-1;
	recSelectedEntites.setData(nRow, "ENTITY_ID", nEntityID);
	recSelectedEntites.setData(nRow, "ENTITY_TYPE", nEntityType);
	recSelectedEntites.setData(nRow, "ENTITY_PERSON_ID", nEntityPersonID);
	
	hshAddedEntityID.insert(nRow, nEntityID);
	hshAddedEntityType.insert(nRow, nEntityType);
}

void CalendarGraphicsView::resizeEvent(QResizeEvent *event)
{
	m_pGraphicsScene->setSceneRect(viewport()->rect());
	m_pBackgroundItem->setMinimumHeight(viewport()->rect().height());
	m_pBackgroundItem->setMaximumHeight(viewport()->rect().height());
	m_pBackgroundItem->setMinimumWidth(viewport()->rect().width());
	m_pBackgroundItem->setMaximumWidth(viewport()->rect().width());
	
	//Calendar table item.
	dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->setPos(getVertHeaderWidth(), getTotalHeaderHeight());
	dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->resize(event->size().width()-getVertHeaderWidth(), event->size().height()-getTotalHeaderHeight());

	PositionCornerScrollBarItems();
	PositionCustomScrollBars();

	if (m_bResizeFromShowEvent && m_bFirstTimeShow)
	{
		m_bFirstTimeShow = false;
		m_bResizeFromShowEvent = false;	
		ResizeColumnsToFit();
		SetTopLeftVisibleItem(m_ScrollToDateTime);
	}
	else if (AllColumnsAreInViewArea(event->size().width()) && (event->oldSize().width()!=event->size().width()))
		ResizeColumnsToFit();
}

void CalendarGraphicsView::showEvent(QShowEvent *event)
{
	m_bResizeFromShowEvent = true;
}
