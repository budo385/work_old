#ifndef ROLETREEVIEW_H
#define ROLETREEVIEW_H

#include <QListView>
#include <QTreeView>
#include <QHeaderView>
#include <QDragMoveEvent>
#include <QDragEnterEvent>
#include <QDebug>
#include <QMenu>

#include "gui_core/gui_core/roledeftreemodel.h"
#include "gui_core/gui_core/dbrecordsetitem.h"

/*!
\class  RoleTreeView
\ingroup FUICore/AccessRights
\brief  Role tree view widget. 

Role tree view widget, sub classed from QTreeView for easier control of drag/drop.
*/


class RoleTreeView : public QTreeView
{
    Q_OBJECT

public:
    RoleTreeView(QWidget *parent = 0);
    ~RoleTreeView();

protected:
	void dragMoveEvent(QDragMoveEvent *event); 
	void dragEnterEvent(QDragEnterEvent *event);
	void dropEvent(QDropEvent *event);

	//Context menu.
	void contextMenuEvent(QContextMenuEvent *event);
	void CreateRolesContextMenuActions(QList<QAction*>& lstActions);
	void CreateAccRightsContextMenuActions(QList<QAction*>& lstActions);

private:
	QList<QAction*>			m_lstAccRightsActions;			//< Context menu list.
	QList<QAction*>			m_lstRoleActions;				//< Context menu list.
	QPersistentModelIndex	m_indContextMenuItemIndex;		//< Context menu index.

private slots:
	//Context menu slots.
	void RenameRole();
	void DeleteItem();

};

#endif // ROLETREEVIEW_H
