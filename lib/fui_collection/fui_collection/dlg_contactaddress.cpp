#include "dlg_contactaddress.h"
#include <QClipboard>
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/countries.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "gui_core/gui_core/macros.h"
#include "bus_client/bus_client/httplookup.h"

Dlg_ContactAddress::Dlg_ContactAddress(QWidget *parent)
: QDialog(parent),m_DataManager(&m_lstData)
{
	ui.setupUi(this);

	ui.tableTypes->Initialize();

	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
	//m_lstDataAddressTypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES));
	//m_lstDataAddressTypes.addColumn(QVariant::Int,NM_TEMP_ID); //add boogie column as it is not connected to bo entity
	
	m_GuiFieldManagerMain =new GuiFieldManager(&m_DataManager,DbSqlTableView::TVIEW_BUS_CM_ADDRESS);

	//connect 
	m_GuiFieldManagerMain->RegisterField("BCMA_FIRSTNAME",ui.txtFirstName);
	m_GuiFieldManagerMain->RegisterField("BCMA_LASTNAME",ui.txtLastName);
	m_GuiFieldManagerMain->RegisterField("BCMA_ORGANIZATIONNAME",ui.txtOrg);
	m_GuiFieldManagerMain->RegisterField("BCMA_ORGANIZATIONNAME_2",ui.txtOrg2);
	m_GuiFieldManagerMain->RegisterField("BCMA_STREET",ui.txtStreet);
	//m_GuiFieldManagerMain->RegisterField("BCMA_SALUTATION",ui.txtSalutation);
	//m_GuiFieldManagerMain->RegisterField("BCMA_SALUTATION",ui.txtSalutation);
	m_GuiFieldManagerMain->RegisterField("BCMA_ZIP",ui.txtZIP);
	m_GuiFieldManagerMain->RegisterField("BCMA_CITY",ui.txtCity);
	//m_GuiFieldManagerMain->RegisterField("BCMA_COUNTRY_CODE",ui.txtCountry);
	m_GuiFieldManagerMain->RegisterField("BCMA_REGION",ui.txtState);
	m_GuiFieldManagerMain->RegisterField("BCMA_POBOX",ui.txtPOBOX);
	m_GuiFieldManagerMain->RegisterField("BCMA_POBOXZIP",ui.txtPOBOXZIP);
	m_GuiFieldManagerMain->RegisterField("BCMA_OFFICECODE",ui.txtOffice);
	m_GuiFieldManagerMain->RegisterField("BCMA_MIDDLENAME",ui.txtMiddleName);
	m_GuiFieldManagerMain->RegisterField("BCMA_TITLE",ui.txtTitle);
	m_GuiFieldManagerMain->RegisterField("BCMA_LANGUGAGE_CODE",ui.txtLang);
	m_GuiFieldManagerMain->RegisterField("BCMA_FORMATEDADDRESS",ui.txtAddress); //must be plain text

	//enable to use plain text:
	GuiField_TextEdit* pText=dynamic_cast<GuiField_TextEdit*>(m_GuiFieldManagerMain->GetFieldHandler("BCMA_FORMATEDADDRESS"));
	if(pText!=NULL)pText->UsePlainText();


	//buttons & icons:
	//GUI_Helper::SetButtonTextIcon(ui.btnFromInternet,tr("From Internet"),":openbook.png");
	//GUI_Helper::SetButtonTextIcon(ui.btnTwixTel,tr("Find In TwixTel"),":openbook.png");
	GUI_Helper::SetButtonTextIcon(ui.btnFormat,tr("Format Address"),":cookies.png");
	GUI_Helper::SetButtonTextIcon(ui.btnFillFields,tr("Fill Fields From Address"),":undo.png");

	ui.btnCustomize->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnCustomize->setToolTip(tr("Modify Address Schema"));
	//ui.btnCustomize->setVisible(false);


	//combo's:
	m_SalutationHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_SalutationHandler.SetDataForCombo(ui.cmbSalutation,"BCMT_TYPE_NAME","BCMT_TYPE_NAME","BCMA_SALUTATION",&m_DataManager);
	m_SalutationHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_SALUTATION);
	m_SalutationHandler.ReloadData();

	m_ShortSalutationHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_ShortSalutationHandler.SetDataForCombo(ui.cmbShortSalutation,"BCMT_TYPE_NAME","BCMT_TYPE_NAME","BCMA_SHORT_SALUTATION",&m_DataManager);
	m_ShortSalutationHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_SHORT_SALUTATION);
	m_ShortSalutationHandler.ReloadData();


	m_TitleHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_TitleHandler.SetDataForCombo(ui.cmbTitle,"BCMT_TYPE_NAME","BCMT_TYPE_NAME","BCMA_TITLE",&m_DataManager,m_GuiFieldManagerMain->GetFieldHandler("BCMA_TITLE"));
	m_TitleHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_TITLE);
	m_TitleHandler.ReloadData();

	m_LangHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_LangHandler.SetDataForCombo(ui.cmbLang,"BCMT_TYPE_NAME","BCMT_TYPE_NAME","BCMA_LANGUGAGE_CODE",&m_DataManager,m_GuiFieldManagerMain->GetFieldHandler("BCMA_LANGUGAGE_CODE"));
	m_LangHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_LANGUAGE);
	m_LangHandler.ReloadData();


	m_CountryHandler.Initialize(ENTITY_COUNTRIES);
	m_CountryHandler.SetDataForCombo(ui.cmbCountry,"COUNTRY"); //,"DS-CODE","BCMA_COUNTRY_CODE",&m_DataManager,m_GuiFieldManagerMain->GetFieldHandler("BCMA_COUNTRY_CODE"));

	//COMBO:
	m_FormatAddressCombo.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
	m_FormatAddressCombo.SetDataForCombo(ui.cmbFormatSchemas,"BCMAS_SCHEMA_NAME");
	m_FormatAddressCombo.ReloadData(); 

	m_ParseAddressCombo.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
	m_ParseAddressCombo.SetDataForCombo(ui.cmbSchemaParser,"BCMAS_SCHEMA_NAME");
	m_ParseAddressCombo.ReloadData(); 

	//issue 1249:
	int nSettingsIdx=g_pSettings->GetPersonSetting(CONTACT_DEF_ADDR_SCHEMA_PERSON).toInt();
	if (nSettingsIdx<1)
		nSettingsIdx=g_pSettings->GetApplicationOption(APP_CONTACT_DEF_ADDR_SCHEMA_PERSON).toInt();
	if (nSettingsIdx>0)
	{
		m_FormatAddressCombo.SetCurrentIndexFromID(nSettingsIdx);			
		m_ParseAddressCombo.SetCurrentIndexFromID(nSettingsIdx);
	}

	//m_FormatAddressComboOrg.SetCurrentIndexFromID(GetSettingValue(CONTACT_DEF_ADDR_SCHEMA_ORG).toInt());



	ui.txtCountry->setMaxLength(3); //two-three code char
	connect(ui.txtCountry,SIGNAL(textEdited(const QString&)),this,SLOT(OnCountryEdit(const QString&)));
	
	//default is on
	ui.ckbSynchronize->setChecked(true);
	
	this->setWindowTitle(tr("Enter Address"));//set dialog title

	//CTRL-V action on address field:
	ui.txtAddress->acceptDrops();
	ui.txtAddress->setAcceptRichText(false);
	
	//temp:
	//ui.btnFromInternet->setEnabled(false);
	//ui.btnTwixTel->setEnabled(false);


	//issue 1343
	ui.btnFillFields->setVisible(false);
	ui.cmbSchemaParser->setVisible(false);

	ui.lookupZIP->setIcon(QIcon(":Search_In_Web24.png"));

	if(g_pSettings->GetApplicationOption(CONTACT_DISABLE_ZIP_LOOKUP).toBool())
		ui.lookupZIP->setEnabled(false);
}

Dlg_ContactAddress::~Dlg_ContactAddress()
{

}

void Dlg_ContactAddress::RefreshDisplay()
{
	//refresh display on window, from data to GUI
	m_GuiFieldManagerMain->RefreshDisplay();
	m_SalutationHandler.RefreshDisplay();
	m_ShortSalutationHandler.RefreshDisplay();
	m_TitleHandler.RefreshDisplay();
	m_LangHandler.RefreshDisplay();
	m_CountryHandler.RefreshDisplay();

	//set salutations:
	if (m_lstData.getRowCount()>0)
	{
		QString strCurrentSalutation = m_lstData.getDataRef(0, "BCMA_SALUTATION").toString();
		ui.cmbSalutation->blockSignals(true);
		ui.cmbSalutation->setEditText(strCurrentSalutation);
		ui.cmbSalutation->blockSignals(false);

		strCurrentSalutation = m_lstData.getDataRef(0, "BCMA_SHORT_SALUTATION").toString();
		ui.cmbShortSalutation->blockSignals(true);
		ui.cmbShortSalutation->setEditText(strCurrentSalutation);
		ui.cmbShortSalutation->blockSignals(false);
	}

	if (m_lstData.getRowCount()>0)
	{
		ui.txtCountry->setText(Countries::GetDsCodeFromISO(m_lstData.getDataRef(0, "BCMA_COUNTRY_CODE").toString()));
		int nRow=m_CountryHandler.GetDataSource()->find(0,m_lstData.getDataRef(0, "BCMA_COUNTRY_NAME").toString(),true);
		if (nRow<0)
			m_CountryHandler.GetDataSource()->find("ISO",m_lstData.getDataRef(0, "BCMA_COUNTRY_CODE").toString(),true);
		//refresh display:
		ui.cmbCountry->blockSignals(true);
		ui.cmbCountry->setCurrentIndex(nRow);
		ui.cmbCountry->blockSignals(false);
	}
	else
	{
		ui.txtCountry->setText("");
		ui.cmbCountry->blockSignals(true);
		ui.cmbCountry->setCurrentIndex(-1);
		ui.cmbCountry->blockSignals(false);

	}

}

void Dlg_ContactAddress::SetRow(DbRecordSet &rowAddr,bool bEditMode,bool bSyncFlag)
{

	ui.ckbSynchronize->setChecked(bSyncFlag);
	m_bEditMode=bEditMode;
	m_lstData=rowAddr;


	//all enabled
	m_GuiFieldManagerMain->SetEditMode(m_bEditMode);
	//ui.btnFromInternet->setEnabled(m_bEditMode);
	//ui.btnTwixTel->setEnabled(m_bEditMode);
	ui.btnFormat->setEnabled(m_bEditMode);
	ui.btnFillFields->setEnabled(m_bEditMode);
	ui.tableTypes->SetEditMode(m_bEditMode);
	//cmb's/buttons:
	ui.btnCustomize->setEnabled(m_bEditMode);
	ui.cmbSalutation->setEnabled(m_bEditMode);
	ui.cmbShortSalutation->setEnabled(m_bEditMode);
	
	ui.cmbTitle->setEnabled(m_bEditMode);
	ui.cmbLang->setEnabled(m_bEditMode);
	ui.btnFormat->setEnabled(m_bEditMode);
	ui.btnFillFields->setEnabled(m_bEditMode);
	ui.cmbCountry->setEnabled(m_bEditMode);
	ui.txtCountry->setEnabled(m_bEditMode);
	//ui.btnFromInternet->setEnabled(false);			//not an option
	//ui.btnTwixTel->setEnabled(false);			//not an option
	
	
	//init data change trackers
	m_DataManager.EnableDataChangeTracking("BCMA_ID");
	m_DataManager.ClearState();
	//m_DataManagerAddressTypes.EnableDataChangeTracking("BCMAT_ID");
	//m_DataManagerAddressTypes.ClearState();

	//set combo's to right schemas:
	int nRow=m_FormatAddressCombo.GetDataSource()->find(m_FormatAddressCombo.GetDataSource()->getColumnIdx("BCMAS_ID"),rowAddr.getDataRef(0,"BCMA_FORMATSCHEMA_ID").toInt(),true);
	if(nRow!=-1)ui.cmbFormatSchemas->setCurrentIndex(nRow);

	nRow=m_ParseAddressCombo.GetDataSource()->find(m_ParseAddressCombo.GetDataSource()->getColumnIdx("BCMAS_ID"),rowAddr.getDataRef(0,"BCMA_PARSESCHEMA_ID").toInt(),true);
	if(nRow!=-1)ui.cmbSchemaParser->setCurrentIndex(nRow);

	ui.tableTypes->SetAddressRow(&m_lstData);
	RefreshDisplay();
	ui.txtFirstName->setFocus(Qt::OtherFocusReason);

	//issue 2056:
	if (!ui.txtCountry->text().isEmpty())
	{
		OnCountryEdit(ui.txtCountry->text());
	}
}


bool Dlg_ContactAddress::GetRow(DbRecordSet &rowAddr,bool &bSyncNames)
{
	//extract format & parse schema id:
	DbRecordSet Data;
	int nIndex=m_FormatAddressCombo.GetSelection(Data);
	if(nIndex>=0)
		m_DataManager.ChangeData(0,"BCMA_FORMATSCHEMA_ID",Data.getDataRef(0,"BCMAS_ID").toInt());

	nIndex=m_ParseAddressCombo.GetSelection(Data);
	if(nIndex>=0)
		m_DataManager.ChangeData(0,"BCMA_PARSESCHEMA_ID",Data.getDataRef(0,"BCMAS_ID").toInt());

	//save country name:
	nIndex=m_CountryHandler.GetSelection(Data);
	if(nIndex>=0)
		m_DataManager.ChangeData(0,"BCMA_COUNTRY_NAME",Data.getDataRef(0,"COUNTRY").toString());

	//sync:
	bSyncNames=ui.ckbSynchronize->isChecked();
	//adress:
	rowAddr=m_lstData;

	//return if changed, probably always true...
	return (m_DataManager.IsChanged());
}




//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void Dlg_ContactAddress::on_btnOK_clicked()
{
	int nFormatAddressAlways=g_pSettings->GetApplicationOption(APP_FORMAT_CONTACT_ADDRESS_ALWAYS).toInt();
	if (nFormatAddressAlways)
	{
		on_btnFormat_clicked();
	}

	//save salutations:
	if (ui.cmbSalutation->lineEdit())
		m_lstData.setData(0,"BCMA_SALUTATION",ui.cmbSalutation->lineEdit()->text());
	if (ui.cmbShortSalutation->lineEdit())
		m_lstData.setData(0,"BCMA_SHORT_SALUTATION",ui.cmbShortSalutation->lineEdit()->text());

	//address must have one type if types are defined
	Status err;
	ui.tableTypes->CheckDataBeforeWrite(err);
	_CHK_ERR(err);

	done(1);
}


void Dlg_ContactAddress::on_btnCancel_clicked()
{
	done(0);
}




void Dlg_ContactAddress::on_btnCustomize_clicked()
{
	Dlg_ContactAddressSchemas dlg;
	bool bOK=dlg.Initialize(ui.cmbFormatSchemas->currentText());
	if(!bOK) return;
	int nRes=dlg.exec();
	if(nRes==0) return;

	//set cuurent schemas:
	QString strCurrentSchema=dlg.GetCurrentSchemaName();
	if(!strCurrentSchema.isEmpty())
	{
		int nRow=m_FormatAddressCombo.GetDataSource()->find(m_FormatAddressCombo.GetDataSource()->getColumnIdx("BCMAS_SCHEMA_NAME"),strCurrentSchema,true);
		if(nRow!=-1)ui.cmbFormatSchemas->setCurrentIndex(nRow);

		nRow=m_ParseAddressCombo.GetDataSource()->find(m_ParseAddressCombo.GetDataSource()->getColumnIdx("BCMAS_SCHEMA_NAME"),strCurrentSchema,true);
		if(nRow!=-1)ui.cmbSchemaParser->setCurrentIndex(nRow);
	}

}



//if edit mode, format address
void Dlg_ContactAddress::on_btnFormat_clicked()
{
	if(!m_bEditMode) return;
	if(m_lstData.getRowCount()!=1) return;

	//save salutations:
	if (ui.cmbSalutation->lineEdit())
		m_lstData.setData(0,"BCMA_SALUTATION",ui.cmbSalutation->lineEdit()->text());
	if (ui.cmbShortSalutation->lineEdit())
		m_lstData.setData(0,"BCMA_SHORT_SALUTATION",ui.cmbShortSalutation->lineEdit()->text());

	//extract format schema:
	DbRecordSet Data;
	QString  strSchema;
	int nIndex=m_FormatAddressCombo.GetSelection(Data);
	if(nIndex>=0)
		strSchema=Data.getDataRef(0,"BCMAS_SCHEMA").toString();
	else
		return;

	m_FormatAddress.AddressFormat(m_lstData,strSchema);
	m_DataManager.SetDirtyFlagOnAllRows();//data changed
	RefreshDisplay();
}

void Dlg_ContactAddress::on_btnFillFields_clicked()
{
	if(!m_bEditMode) return;
	if(m_lstData.getRowCount()!=1) return;

	//QMessageBox::warning(this,tr("Warning"),tr("Not yet implemented"));
	//return;

	//extract format schema:
	DbRecordSet Data;
	QString  strSchema;
	int nIndex=m_ParseAddressCombo.GetSelection(Data);
	if(nIndex>=0)
		strSchema=Data.getDataRef(0,"BCMAS_SCHEMA").toString();
	else
		return;

	//QString strAddress=ui.txtAddress->toPlainText();
	//QByteArray byteArray=strAddress.toLatin1().constData();
	//m_lstData.setData(0,"BCMA_FORMATEDADDRESS",byteArray);

	m_FormatAddress.AddressParse(m_lstData,strSchema);
	m_DataManager.SetDirtyFlagOnAllRows(); //data changed
	RefreshDisplay();

}



/*
void Dlg_ContactAddress::on_btnFromInternet_clicked()
{
	QMessageBox::warning(this,tr("Warning"),tr("Not yet implemented"));
	return;
}
*/

//when user manually enters string
void Dlg_ContactAddress::OnCountryEdit(const QString& text)
{
	if (text.isEmpty())
	{
		//refresh display:
		ui.cmbCountry->blockSignals(true);
		ui.cmbCountry->setCurrentIndex(-1);
		ui.cmbCountry->blockSignals(false);
		return;
	}

	//find ISO code:
	QString strISOCode=Countries::GetISOCodeFromDs(text);
	int nRow=m_CountryHandler.GetDataSource()->find("ISO",strISOCode,true);
	
	//refresh display:
	ui.cmbCountry->blockSignals(true);
	ui.cmbCountry->setCurrentIndex(nRow);
	ui.cmbCountry->blockSignals(false);

	QString strCountryName = ui.cmbCountry->itemText(nRow);
	m_lstData.setData(0,"BCMA_COUNTRY_CODE",strISOCode);
	m_lstData.setData(0,"BCMA_COUNTRY_NAME", strCountryName);

}


void Dlg_ContactAddress::on_cmbCountry_currentIndexChanged(const QString &text)
{
	if (text.isEmpty())
	{
		//refresh display:
		ui.txtCountry->blockSignals(true);
		ui.txtCountry->setText("");
		ui.txtCountry->blockSignals(false);
		m_lstData.setData(0,"BCMA_COUNTRY_CODE","");
		m_lstData.setData(0,"BCMA_COUNTRY_NAME","");
		return;
	}

	//find ISO code:
	int nRow=m_CountryHandler.GetDataSource()->find(0,text,true);

	QString strISOCode="";
	if (nRow>=0)
	{
		strISOCode=m_CountryHandler.GetDataSource()->getDataRef(nRow, "ISO").toString();
		ui.txtCountry->blockSignals(true);
		ui.txtCountry->setText(m_CountryHandler.GetDataSource()->getDataRef(nRow, "DS-CODE").toString());
		ui.txtCountry->blockSignals(false);
	}

	m_lstData.setData(0,"BCMA_COUNTRY_CODE",strISOCode);
	m_lstData.setData(0,"BCMA_COUNTRY_NAME", text);
}


void Dlg_ContactAddress::on_cmbSalutation_currentIndexChanged(const QString & textCmb)
{
	QString text=textCmb;
	//if count = 0 return false
	if (m_lstData.getRowCount()==0) return;
	if (text.indexOf("[")==-1) return;

	DbRecordSet rowAddTmp=m_lstData;

	//if count = 0 return false
	m_FormatAddress.AddressParse(rowAddTmp,text);
	text.replace("[First Name]",rowAddTmp.getDataRef(0,"BCMA_FIRSTNAME").toString());
	text.replace("[Last Name]",rowAddTmp.getDataRef(0,"BCMA_LASTNAME").toString());
	text.replace("[Middle Name]",rowAddTmp.getDataRef(0,"BCMA_MIDDLENAME").toString());
	text.replace("[Title]",rowAddTmp.getDataRef(0,"BCMA_TITLE").toString());
	text.replace("[First_Name]",rowAddTmp.getDataRef(0,"BCMA_FIRSTNAME").toString());
	text.replace("[Last_Name]",rowAddTmp.getDataRef(0,"BCMA_LASTNAME").toString());
	text.replace("[Middle_Name]",rowAddTmp.getDataRef(0,"BCMA_MIDDLENAME").toString());
	text.replace("[Title]",rowAddTmp.getDataRef(0,"BCMA_TITLE").toString());

	ui.cmbSalutation->blockSignals(true);
	ui.cmbSalutation->setEditText(text);
	ui.cmbSalutation->blockSignals(false);

	m_lstData.setData(0,"BCMA_SALUTATION",text);

}


void Dlg_ContactAddress::on_cmbShortSalutation_currentIndexChanged(const QString & textCmb)
{
	QString text=textCmb;
	//if count = 0 return false
	if (m_lstData.getRowCount()==0) return;
	if (text.indexOf("[")==-1) return;

	DbRecordSet rowAddTmp=m_lstData;

	//if count = 0 return false
	m_FormatAddress.AddressParse(rowAddTmp,text);
	text.replace("[First Name]",rowAddTmp.getDataRef(0,"BCMA_FIRSTNAME").toString());
	text.replace("[Last Name]",rowAddTmp.getDataRef(0,"BCMA_LASTNAME").toString());
	text.replace("[Middle Name]",rowAddTmp.getDataRef(0,"BCMA_MIDDLENAME").toString());
	text.replace("[Title]",rowAddTmp.getDataRef(0,"BCMA_TITLE").toString());
	text.replace("[First_Name]",rowAddTmp.getDataRef(0,"BCMA_FIRSTNAME").toString());
	text.replace("[Last_Name]",rowAddTmp.getDataRef(0,"BCMA_LASTNAME").toString());
	text.replace("[Middle_Name]",rowAddTmp.getDataRef(0,"BCMA_MIDDLENAME").toString());
	text.replace("[Title]",rowAddTmp.getDataRef(0,"BCMA_TITLE").toString());


	ui.cmbShortSalutation->blockSignals(true);
	ui.cmbShortSalutation->setEditText(text);
	ui.cmbShortSalutation->blockSignals(false);

	m_lstData.setData(0,"BCMA_SHORT_SALUTATION",text);

}

void Dlg_ContactAddress::on_lookupZIP_clicked()
{
	QString strZIP = ui.txtZIP->text();
	QString strTown = ui.txtCity->text();
	QString strCountry = ui.txtCountry->text();
	bool bZipWasEmpty = strZIP.isEmpty();
	bool bCountryWasEmpty = strCountry.isEmpty();

	if(HttpLookup::WebLookupZIPTown(strZIP, strTown, strCountry))
	{
		if(bZipWasEmpty)
			ui.txtZIP->setText(strZIP);
		else
			ui.txtCity->setText(strTown);
		if(bCountryWasEmpty){
			ui.txtCountry->setText(strCountry);
			OnCountryEdit(strCountry);
		}
	}
}


void Dlg_ContactAddress::HideButtonBar()
{
	ui.btnOK->setVisible(false);
	ui.btnCancel->setVisible(false);

	setModal(false);
	setWindowFlags(Qt::Widget);
	
}