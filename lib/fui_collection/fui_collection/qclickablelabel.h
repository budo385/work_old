#ifndef QCLICKABLELABEL_H
#define QCLICKABLELABEL_H

#include <QLabel>

class QClickableLabel : public QLabel
{
	Q_OBJECT

public:
	QClickableLabel(QWidget *parent);
	~QClickableLabel();

signals:
	void clicked();

protected:
	virtual void mousePressEvent ( QMouseEvent * event );

private:
	
};

#endif // QCLICKABLELABEL_H
