#include "dlg_contactaddressschemas.h"
#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbsqltableview.h"

//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services

Dlg_ContactAddressSchemas::Dlg_ContactAddressSchemas(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	this->setWindowTitle(tr("Define Address Schema"));//set dialog title

	ui.btnInfo->setIcon(QIcon(":omnis_1797.png"));
}



Dlg_ContactAddressSchemas::~Dlg_ContactAddressSchemas()
{

}


//init dialog, return false if failed:
bool Dlg_ContactAddressSchemas::Initialize(QString strDefaultSchemaName)
{

	//LIST:
	//enable for drag set list of addr fields:
	ui.lstAddressFields->setDragEnabled(true);
	ui.lstAddressFields->setDropIndicatorShown(true);
	ui.lstAddressFields->SetData(m_FormatAddress.m_lstAdrFields);

	//TEXT:
	ui.txtSchema->setAcceptDrops(true);
	ui.txtSchema->setAcceptRichText(false);


	//COMBO:
	m_SchemasHandler.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
	m_SchemasHandler.SetDataForCombo(ui.cmbSchemas,"BCMAS_SCHEMA_NAME");
	bool bOK=m_SchemasHandler.ReloadData(true); //always refresh from server
	if(!bOK) return false;						//server failed to response

	//connect signals:
	ui.cmbSchemas->setCurrentIndex(-1); //reset index
	connect(ui.cmbSchemas,SIGNAL(currentIndexChanged(int)),this,SLOT(onSchemaChanged(int)));

	//set mode:
	SetMode(MODE_READ);

	if (!strDefaultSchemaName.isEmpty())
		m_SchemasHandler.SetCurrentIndexFromName(strDefaultSchemaName);

	return true;

}





//when changing state, call this:
void Dlg_ContactAddressSchemas::SetMode(int nMode)
{

	m_nMode=nMode;

	if(m_nMode==MODE_READ || m_nMode==MODE_EMPTY)
	{
		ui.txtViewName->setVisible(false);
		ui.cmbSchemas->setVisible(true);
		ui.txtSchema->setReadOnly(true);
		ui.btnEdit->setEnabled(true);
		ui.btnNew->setEnabled(true);
		ui.btnDelete->setEnabled(true);
		ui.btnAddColumn->setEnabled(false);
		ui.txtSchema->clear();

	}
	else
	{
		if(m_nMode==MODE_INSERT)
		{
			ui.txtSchema->clear();
			ui.txtViewName->clear();
		}
		else
		{
			//set view name
			DbRecordSet Data;
			int nIndex=m_SchemasHandler.GetSelection(Data);
			if(nIndex>=0)
				ui.txtViewName->setText(Data.getDataRef(0,"BCMAS_SCHEMA_NAME").toString());
		}
		ui.txtViewName->setVisible(true);
		ui.cmbSchemas->setVisible(false);
		ui.txtSchema->setReadOnly(false);
		ui.btnEdit->setEnabled(false);
		ui.btnNew->setEnabled(false);
		ui.btnDelete->setEnabled(false);
		ui.btnAddColumn->setEnabled(true);
		ui.txtViewName->setFocus(Qt::OtherFocusReason);
	}

}






//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void Dlg_ContactAddressSchemas::on_btnOK_clicked()
{
	//try write:
	if(m_nMode==MODE_EDIT)
	{
		//if edit mode, take row from combo, fill, send, send locked, if ok, empty lock, refresh combo...
		int nIndex=ui.cmbSchemas->currentIndex();
		if(nIndex>=0) 
		{
			//prepare record:
			DbRecordSet* pData=m_SchemasHandler.GetDataSource();
			QString schemaDB=m_FormatAddress.MapDisplayToSchema(ui.txtSchema->toPlainText());
			QString schemaName=ui.txtViewName->text();
			if(schemaName.isEmpty())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Schema name is mandatory field"));
				return;
			}
			if(schemaDB.count("[")!=schemaDB.count("]"))
			{
				QMessageBox::warning(this,tr("Warning"),tr("Invalid Schema, count of open/closed brackets does not match!"));
				return;
			}
			pData->setData(nIndex,"BCMAS_SCHEMA_NAME",schemaName);
			//QByteArray byteValue = schemaDB.toLatin1().constData();	//saved as byteArray
			pData->setData(nIndex,"BCMAS_SCHEMA",schemaDB);
			//assign row for send:
			DbRecordSet Data;
			Data.copyDefinition(*pData);
			Data.addRow();
			DbRecordSet row = pData->getRow(nIndex);
			Data.assignRow(0,row);

			//send for write with lock
			Status err;
			_SERVER_CALL(BusAddressSchemas->Write(err,Data,m_strLockedRes))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			m_strLockedRes.clear();
			
			pData->assignRow(nIndex,Data); //store data back
			m_SchemasHandler.RefreshToGlobalCache(MainEntitySelectionController::CACHE_UPDATE_DATA_EDITED,false,&Data);
		}
	}

	//try insert:
	if(m_nMode==MODE_INSERT)
	{
		//if edit mode, take row from combo, fill, send, send locked, if ok, empty lock, refresh combo...
		//prepare record:
		DbRecordSet Data;
		Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SCHEMAS));
		QString schemaDB=m_FormatAddress.MapDisplayToSchema(ui.txtSchema->toPlainText());
		QString schemaName=ui.txtViewName->text();
		if(schemaName.isEmpty())
		{
			QMessageBox::warning(this,tr("Warning"),tr("Schema name is mandatory field"));
			return;
		}
		Data.addRow();
		Data.setData(0,"BCMAS_SCHEMA_NAME",schemaName);
		//QByteArray byteValue = schemaDB.toLatin1().constData();
		Data.setData(0,"BCMAS_SCHEMA",schemaDB);

		//Data.Dump();

		//send for write with lock
		Status err;
		_SERVER_CALL(BusAddressSchemas->Write(err,Data,""))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//refresh cache:
		m_SchemasHandler.GetDataSource()->merge(Data);
		m_SchemasHandler.RefreshToGlobalCache(MainEntitySelectionController::CACHE_UPDATE_DATA_INSERTED,false,&Data);
		//set to last index:
		ui.cmbSchemas->setCurrentIndex(m_SchemasHandler.GetDataSource()->getRowCount()-1);
	}
	
	
	done(1);
}


void Dlg_ContactAddressSchemas::on_btnCancel_clicked()
{
	if(m_nMode==MODE_EDIT && !m_strLockedRes.isEmpty())
	{
		Status err;
		_SERVER_CALL(BusAddressSchemas->Unlock(err,m_strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		m_strLockedRes.clear();
		SetMode(MODE_READ);	//lock released
		onSchemaChanged(ui.cmbSchemas->currentIndex()); //reload content
		return;
	}

	if(m_nMode==MODE_INSERT)
	{
		SetMode(MODE_READ);	//lock released
		onSchemaChanged(ui.cmbSchemas->currentIndex()); //reload content
		return;
	}


	done(0);
}

//when index changes, reload data
void Dlg_ContactAddressSchemas::onSchemaChanged(int nIndex)
{
	if(nIndex>=0)
	{
		ui.txtSchema->setPlainText(m_FormatAddress.MapSchemaToDisplay(m_SchemasHandler.GetDataSource()->getDataRef(nIndex,"BCMAS_SCHEMA").toString()));
	}
	else
	{
		ui.txtSchema->setPlainText("");		//if invalid index,clear text
	}
}



void Dlg_ContactAddressSchemas::on_btnNew_clicked()
{
	SetMode(MODE_INSERT);
}

void Dlg_ContactAddressSchemas::on_btnEdit_clicked()
{
	int nIndex=ui.cmbSchemas->currentIndex();
	if(nIndex>=0) //lock
	{
		Status err;
		_SERVER_CALL(BusAddressSchemas->Lock(err,m_SchemasHandler.GetDataSource()->getDataRef(nIndex,"BCMAS_ID").toInt(),m_strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		SetMode(MODE_EDIT);	//lock obtained
	}
}

void Dlg_ContactAddressSchemas::on_btnDelete_clicked()
{
	int nIndex=ui.cmbSchemas->currentIndex();
	if(nIndex>=0) 
	{
		int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete the selected schema from the database"),tr("Yes"),tr("No"));
		if (nResult!=0) return; //only if YES

		Status err;
		_SERVER_CALL(BusAddressSchemas->Delete(err,m_SchemasHandler.GetDataSource()->getDataRef(nIndex,"BCMAS_ID").toInt()))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//refresh from server:
		m_SchemasHandler.ReloadData(true);
		onSchemaChanged(0); //set to First
		ui.txtSchema->clear();
	}


}



//empty if none selected
QString Dlg_ContactAddressSchemas::GetCurrentSchemaName()
{
	//set view name
	DbRecordSet Data;
	int nIndex=m_SchemasHandler.GetSelection(Data);
	if(nIndex>=0)
		return Data.getDataRef(0,"BCMAS_SCHEMA_NAME").toString();
	else
		return "";

}

//on add column:
void Dlg_ContactAddressSchemas::on_btnAddColumn_clicked()
{

	QString strContent=ui.txtSchema->toPlainText();
	DbRecordSet lstCols;
	ui.lstAddressFields->GetDropValue(lstCols);

	QString strNewContent;
	for(int i=0; i<lstCols.getRowCount();++i)
	{
		strNewContent+=lstCols.getDataRef(i,0).toString();
	}

	int nDroPosition=ui.txtSchema->textCursor().position();
	strContent.insert(nDroPosition,strNewContent);
	ui.txtSchema->setPlainText(strContent);
}


//open INFO dialog:
void Dlg_ContactAddressSchemas::on_btnInfo_clicked()
{
	Dlg_AddrSchemaInfo dlg;
	dlg.exec();
}