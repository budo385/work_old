#include "tableselection_busperson.h"

TableSelection_BusPerson::TableSelection_BusPerson(QWidget *parent)
:UniversalTableWidget(parent)
{
}



void TableSelection_BusPerson::Initialize(DbRecordSet * pData)
{
	//set data source for table, set single selection:
	UniversalTableWidget::Initialize(pData,false,false,true);

	//redefine table look:
	DbView view=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON);

	DbRecordSet columns;
	AddColumnToSetup(columns,"BPER_CODE",tr("Code"),80,false); //, DbSqlTableView::GetColumnToolTip(view,"BPER_CODE"));
	AddColumnToSetup(columns,"BPER_NAME",tr("Name"),80,false); //calculated field

	SetColumnSetup(columns);

	//edit is false always:
	SetEditMode(false);

}


//create own context:
void TableSelection_BusPerson::CreateContextMenuActions(QList<QAction*>& lstActions)
{

	QAction* pAction;
	QAction* SeparatorAct;


	pAction = new QAction(tr("&Reload"), this);
	//  pAction->setShortcut(tr("Del"));
	//	pAction->setIcon(QIcon(":fp.png"));
	pAction->setData(QVariant(true));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(EmitReload()));

	lstActions.append(pAction);
	this->addAction(pAction);


	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);


	pAction = new QAction(tr("&Clear Selected Entries"), this);
	pAction->setShortcut(tr("Del"));
//	pAction->setIcon(QIcon(":fp.png"));
	pAction->setData(QVariant(true));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteSelection()));

	lstActions.append(pAction);
	this->addAction(pAction);

	pAction = new QAction(tr("Clear &All"), this);
	//pAction->setShortcut(tr("CTRL+S"));
//	pAction->setIcon(QIcon(":fp.png"));
	pAction->setData(QVariant(true));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteTable()));

	lstActions.append(pAction);
	//this->addAction(pAction);

	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);

	lstActions.append(SeparatorAct);


	//Select All:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
//	pAction->setIcon(QIcon(":fp.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), this, SLOT(SortDialog()));

	lstActions.append(pAction);
	this->addAction(pAction);
}
