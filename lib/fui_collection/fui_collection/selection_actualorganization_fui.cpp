#include "selection_actualorganization_fui.h"
#include "fui_busorganizations.h"
#include "fuimanager.h"
extern FuiManager g_objFuiManager;


Selection_ActualOrganization_FUI::Selection_ActualOrganization_FUI(QWidget *parent)
: Selection_ActualOrganization(parent),m_pLastFUIOpen(NULL)
{


}

Selection_ActualOrganization_FUI::~Selection_ActualOrganization_FUI()
{

}


//if click, open FUI, go insert, store Last FUI pointer, send signal 
void Selection_ActualOrganization_FUI::on_btnInsert_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;
	/*
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT) //issue 1274
	{
		m_pLastFUIOpen=g_objFuiManager.OpenQCWWindow(NULL);
	}
	else
	{
	*/
		//open FUI, get window pointer
		int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
		int nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_INSERT,-1,true);
		m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	//}

	if(m_nActualOrganizationID!=-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI
	m_bAutoAssigment=true;

	//notifyObservers(SelectorSignals::SELECTOR_ON_INSERT,0,m_nActualOrganizationID);
	notifyObservers(SelectorSignals::SELECTOR_ON_AFTER_OPEN_INSERT_HANDLER,0);

}


void Selection_ActualOrganization_FUI::on_btnModify_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized
	int nRecordID=-1;
	if (m_RowCurrentSelectedEntity.getRowCount()==1) //if something selected, get it
		nRecordID=m_RowCurrentSelectedEntity.getDataRef(0,m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx).toInt(); //get old ID if valid

	//if (m_nEntityTypeID==ENTITY_BUS_CONTACT) //issue 1274
	//{
		//m_pLastFUIOpen=g_objFuiManager.OpenQCWWindow(NULL,nRecordID);
	//}
	//else
	//{
		//open FUI, get window pointer
		int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
		int nNewFUI=-1;
		//if (nRecordID==-1)
		//	nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_INSERT,-1,true);
		//else
		nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_EDIT,nRecordID,true);
		m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	//}


	if(m_nActualOrganizationID!=-1 && nRecordID==-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI

	//notifyObservers(SelectorSignals::SELECTOR_ON_EDIT,nRecordID,m_nActualOrganizationID);
	notifyObservers(SelectorSignals::SELECTOR_ON_AFTER_OPEN_EDIT_HANDLER,0);
}


void Selection_ActualOrganization_FUI::on_btnView_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized

	int nRecordID=-1;
	if (m_RowCurrentSelectedEntity.getRowCount()==1) //if something selected, get it
		nRecordID=m_RowCurrentSelectedEntity.getDataRef(0,m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx).toInt(); //get old ID if valid

	//issue 1516:
	if (nRecordID==-1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("No Assignment Made!"));
		return;
	}

	//open FUI, get window pointer
	int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
	int nNewFUI=-1;
	//if (nRecordID==-1)
	//	nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_INSERT,-1,true);
	//else
	nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_READ,nRecordID,true);
	m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);

	//issue 1401: set tab to details
	/*
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT) 
	{
		FUI_Contacts *pCont=dynamic_cast<FUI_Contacts*>(m_pLastFUIOpen);
		if(pCont)
		{
			pCont->SetCurrentTab(1); 
		}
	}
	*/

	if(m_nActualOrganizationID!=-1 && nRecordID==-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI

	//notifyObservers(SelectorSignals::SELECTOR_ON_VIEW,nRecordID,m_nActualOrganizationID);

}
