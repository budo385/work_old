#include "qc_moreframe.h"
#include "db_core/db_core/dbsqltableview.h"
#include "qcw_largewidget.h"
#include "qcw_smallwidget.h"
#include "gui_core/gui_core/thememanager.h"

QC_MoreFrame::QC_MoreFrame(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	ui.addSelected_toolButton_4->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_5->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_6->setIcon(QIcon(":CopyText16.png"));

	ui.langcode_label->hide();
	ui.langcode_lineEdit->hide();
}

QC_MoreFrame::~QC_MoreFrame()
{

}

void QC_MoreFrame::Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType /*= 0 0-Large,1-small widget*/)
{
	m_nParentType = nParentType;
	m_pParentWidget = ParentWidget;
	m_recFullContactData = recFullContactData;

	ui.picture->Initialize(PictureWidget::MODE_PREVIEW_WITH_BUTTONS,m_recFullContactData,"BCNT_PICTURE","BPIC_PICTURE","BCNT_BIG_PICTURE_ID","BCNT_ID");
	ui.picture->SetEditMode(true);
	
	SetupGUIwidgets();
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

void QC_MoreFrame::SetupGUIwidgets()
{
	ui.nickname_lineEdit->setText(m_recFullContactData->getDataRef(0, "BCNT_NICKNAME").toString());
	ui.department_lineEdit->setText(m_recFullContactData->getDataRef(0, "BCNT_DEPARTMENTNAME").toString());
	ui.profession_lineEdit->setText(m_recFullContactData->getDataRef(0, "BCNT_PROFESSION").toString());
	ui.function_lineEdit->setText(m_recFullContactData->getDataRef(0, "BCNT_FUNCTION").toString());
	ui.langcode_lineEdit->setText(m_recFullContactData->getDataRef(0, "BCNT_LANGUAGECODE").toString());
	if(m_recFullContactData->getDataRef(0, "BCNT_SUPPRESS_MAILING").toBool())
		ui.noemail_checkBox->setCheckState(Qt::Checked);
	else
		ui.noemail_checkBox->setCheckState(Qt::Unchecked);
	ui.external_code_lineEdit->setText(m_recFullContactData->getDataRef(0, "BCNT_EXT_ID_STR").toString());
	ui.dateBirthDay->setDate(m_recFullContactData->getDataRef(0, "BCNT_BIRTHDAY").toDate());
//	ui.picture->Initialize(PictureWidget::MODE_PREVIEW_WITH_BUTTONS,m_recFullContactData,"BCNT_PICTURE","BPIC_PICTURE","BCNT_BIG_PICTURE_ID","BCNT_ID");
	ui.picture->RefreshDisplay();
}

void QC_MoreFrame::SetDataToRecordSet(QString strRecordSetFieldName, QString strValue)
{
	m_recFullContactData->setData(0, strRecordSetFieldName, strValue);
}

void QC_MoreFrame::on_nickname_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCNT_NICKNAME", text);
}

void QC_MoreFrame::on_department_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCNT_DEPARTMENTNAME", text);
}

void QC_MoreFrame::on_profession_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCNT_PROFESSION", text);
}

void QC_MoreFrame::on_function_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCNT_FUNCTION", text);
}

void QC_MoreFrame::on_langcode_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCNT_LANGUAGECODE", text);
}

void QC_MoreFrame::on_external_code_lineEdit_textChanged(const QString &text)
{
	SetDataToRecordSet("BCNT_EXT_ID_STR", text);
}

void QC_MoreFrame::on_noemail_checkBox_stateChanged(int state)
{
	if (state == Qt::Checked)
		m_recFullContactData->setData(0, "BCNT_SUPPRESS_MAILING", 1);
	else
		m_recFullContactData->setData(0, "BCNT_SUPPRESS_MAILING", 0);
}

void QC_MoreFrame::on_addSelected_toolButton_4_clicked()
{
	if (!m_nParentType)
		ui.department_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.department_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_MoreFrame::on_addSelected_toolButton_5_clicked()
{
	if (m_nParentType == 0)
		ui.profession_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.profession_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void QC_MoreFrame::on_addSelected_toolButton_6_clicked()
{
	if (m_nParentType == 0)
		ui.function_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.function_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}


void QC_MoreFrame::on_dateBirthDay_dateChanged(const QDate &date)
{
	m_recFullContactData->setData(0, "BCNT_BIRTHDAY", ui.dateBirthDay->date());
	//SetDataToRecordSet("BCNT_BIRTHDAY", ui.dateBirthDay->date());

}