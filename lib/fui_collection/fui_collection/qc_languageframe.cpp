#include "qc_languageframe.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_client/bus_client/selection_combobox.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/thememanager.h"
#include "qcw_largewidget.h"
#include "qcw_smallwidget.h"

QC_LanguageFrame::QC_LanguageFrame(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

QC_LanguageFrame::~QC_LanguageFrame()
{

}

void QC_LanguageFrame::Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType /*= 0 0-Large,1-small widget*/)
{
	m_nParentType = nParentType;
	m_pParentWidget = ParentWidget;
	m_recFullContactData = recFullContactData;
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

	SetupGUIwidgets(m_recLocalEntityRecordSet);
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

void QC_LanguageFrame::SetupGUIwidgets(DbRecordSet recData)
{
	Selection_ComboBox m_SalutationHandler;
	m_SalutationHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_SalutationHandler.SetDataForCombo(ui.language_comboBox,"BCMT_TYPE_NAME","BCMT_TYPE_NAME");
	m_SalutationHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_LANGUAGE);
	m_SalutationHandler.ReloadData();

	QString strCurrentLanguage = m_recLocalEntityRecordSet.getDataRef(0, "BCMA_LANGUGAGE_CODE").toString();
	if (strCurrentLanguage.isEmpty())
	{
		strCurrentLanguage = ClientContactManager::GetTypeName(ClientContactManager::GetDefaultType(ContactTypeManager::TYPE_LANGUAGE).toInt());
		ui.language_comboBox->setEditText(strCurrentLanguage);
		on_language_comboBox_editTextChanged(strCurrentLanguage);
	}
	else
	{
		ui.language_comboBox->blockSignals(true);
		ui.language_comboBox->setEditText(strCurrentLanguage);
		ui.language_comboBox->blockSignals(false);
	}
}

void QC_LanguageFrame::SetDataToRecordSet(QString strRecordSetFieldName, QString strValue)
{
	if (m_nParentType == 0)
		dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->LanguageChanged(strRecordSetFieldName, strValue);
	else if (m_nParentType == 1)
		dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->LanguageChanged(strRecordSetFieldName, strValue);

/*
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
	
	int nRowCount = m_recLocalEntityRecordSet.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		m_recLocalEntityRecordSet.setData(i, strRecordSetFieldName, strValue);

	m_recFullContactData->setData(0, "LST_ADDRESS", m_recLocalEntityRecordSet);
*/
}

void QC_LanguageFrame::on_language_comboBox_editTextChanged(const QString &text)
{
	SetDataToRecordSet("BCMA_LANGUGAGE_CODE", text);
}
