#include "selection_contactbutton_fui.h"
#include "fui_collection/fui_collection/communicationactions.h"
#include "common/common/entity_id_collection.h"

Selection_ContactButton_FUI::Selection_ContactButton_FUI(QWidget *parent)
:Selection_ContactButton(parent)
{

}
Selection_ContactButton_FUI::~Selection_ContactButton_FUI()
{

}


void Selection_ContactButton_FUI::OnSendMail()
{
QPoint piePos=m_pPieMenu->mapToGlobal(QPoint(100,0));
if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
	CommunicationActions::sendEmail(m_nEntityRecordID,"",-1,&piePos);
else
	CommunicationActions::sendEmail_Project(m_nEntityRecordID,"",&piePos);

}

void Selection_ContactButton_FUI::OnPhoneCall()
{
if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
	CommunicationActions::callPhone(m_nEntityRecordID);
else
	CommunicationActions::callPhone_Project(m_nEntityRecordID);

}

void Selection_ContactButton_FUI::OnDocument()
{
QPoint piePos=m_pPieMenu->mapToGlobal(QPoint(100,100));
if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
	CommunicationActions::sendDocument(m_nEntityRecordID,"",&piePos);
else
	CommunicationActions::sendDocument_Project(m_nEntityRecordID,"",&piePos);

}

void Selection_ContactButton_FUI::OnInternet()
{
if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
	CommunicationActions::openWeb(m_nEntityRecordID);
else
	CommunicationActions::openWeb_Project(m_nEntityRecordID);

}

void Selection_ContactButton_FUI::OnChat()
{
if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
	CommunicationActions::openChat(m_nEntityRecordID);
else
	CommunicationActions::openChat_Project(m_nEntityRecordID);
}

void Selection_ContactButton_FUI::OnSMS()
{
if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
	CommunicationActions::openSMS(m_nEntityRecordID);
else
	CommunicationActions::openSMS_Project(m_nEntityRecordID);
}
