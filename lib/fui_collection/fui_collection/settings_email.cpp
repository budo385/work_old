#include "settings_email.h"

#include "os_specific/os_specific/mailmanager.h"

#define EML_SECURITY_NONE	  1
#define EML_SECURITY_STARTTLS 2
#define EML_SECURITY_SSLTLS	  3

Settings_Email::Settings_Email(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
   	:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

	ui.txtSMTPHost->setText(GetSettingValue(EMAIL_SETTINGS_SMTP_NAME).toString());
	ui.txtSMTPPort->setText(GetSettingValue(EMAIL_SETTINGS_SMTP_PORT).toString());
	ui.txtUserName->setText(GetSettingValue(EMAIL_SETTINGS_USER_NAME).toString());
	ui.txtUserEmail->setText(GetSettingValue(EMAIL_SETTINGS_USER_EMAIL).toString());
	ui.chkUseAccount->setCheckState(GetSettingValue(EMAIL_SETTINGS_USE_AUTH).toBool() ? Qt::Checked : Qt::Unchecked);
	ui.txtAccountName->setText(GetSettingValue(EMAIL_SETTINGS_SMTP_ACC_NAME).toString());
	ui.txtAccountPass->setText(MailManager::DecryptSMTPPassword(GetSettingValue(EMAIL_SETTINGS_SMTP_ACC_PASS).toString()));
	ui.txtEmailCopyCC->setText(GetSettingValue(EMAIL_SETTINGS_SEND_COPY_CC).toString());
	ui.txtEmailCopyBCC->setText(GetSettingValue(EMAIL_SETTINGS_SEND_COPY_BCC).toString());

	ui.cboSecurity->addItem(tr("NONE"),	   EML_SECURITY_NONE);
	ui.cboSecurity->addItem(tr("OpenTLS"), EML_SECURITY_STARTTLS);
	ui.cboSecurity->addItem(tr("SSL/TLS"), EML_SECURITY_SSLTLS);
	ui.cboSecurity->setCurrentIndex(GetSettingValue(EMAIL_SETTINGS_USE_SSL).toInt()-1);

	on_chkUseAccount_toggled(GetSettingValue(EMAIL_SETTINGS_USE_AUTH).toBool());
}

Settings_Email::~Settings_Email()
{
}

DbRecordSet Settings_Email::GetChangedValuesRecordSet()
{
	if (ui.txtSMTPHost->text() != GetSettingValue(EMAIL_SETTINGS_SMTP_NAME).toString())	{
		SetSettingValue(EMAIL_SETTINGS_SMTP_NAME, ui.txtSMTPHost->text());
	}
	if (ui.txtSMTPPort->text() != GetSettingValue(EMAIL_SETTINGS_SMTP_PORT).toString())	{
		SetSettingValue(EMAIL_SETTINGS_SMTP_PORT, ui.txtSMTPPort->text().toInt());
	}
	if (ui.txtUserName->text() != GetSettingValue(EMAIL_SETTINGS_USER_NAME).toString())	{
		SetSettingValue(EMAIL_SETTINGS_USER_NAME, ui.txtUserName->text());
	}
	if (ui.txtUserEmail->text() != GetSettingValue(EMAIL_SETTINGS_USER_EMAIL).toString())	{
		SetSettingValue(EMAIL_SETTINGS_USER_EMAIL, ui.txtUserEmail->text());
	}
	if( (ui.chkUseAccount->checkState() == Qt::Checked && !GetSettingValue(EMAIL_SETTINGS_USE_AUTH).toBool()) ||
		(ui.chkUseAccount->checkState() == Qt::Unchecked && GetSettingValue(EMAIL_SETTINGS_USE_AUTH).toBool())){
		SetSettingValue(EMAIL_SETTINGS_USE_AUTH, (ui.chkUseAccount->checkState() == Qt::Checked)? 1 : 0);
	}
	if (ui.txtAccountName->text() != GetSettingValue(EMAIL_SETTINGS_SMTP_ACC_NAME).toString())	{
		SetSettingValue(EMAIL_SETTINGS_SMTP_ACC_NAME, ui.txtAccountName->text());
	}
	if (ui.txtAccountPass->text() != MailManager::DecryptSMTPPassword(GetSettingValue(EMAIL_SETTINGS_SMTP_ACC_PASS).toString())){
		SetSettingValue(EMAIL_SETTINGS_SMTP_ACC_PASS, MailManager::EncryptSMTPPassword(ui.txtAccountPass->text()));
	}
	if (ui.txtEmailCopyCC->text() != GetSettingValue(EMAIL_SETTINGS_SEND_COPY_CC).toString()){
		SetSettingValue(EMAIL_SETTINGS_SEND_COPY_CC, ui.txtEmailCopyCC->text());
	}
	if (ui.txtEmailCopyBCC->text() != GetSettingValue(EMAIL_SETTINGS_SEND_COPY_BCC).toString()){
		SetSettingValue(EMAIL_SETTINGS_SEND_COPY_BCC, ui.txtEmailCopyBCC->text());
	}
	if(ui.cboSecurity->currentIndex() != GetSettingValue(EMAIL_SETTINGS_USE_SSL).toInt()-1){
		SetSettingValue(EMAIL_SETTINGS_USE_SSL, ui.cboSecurity->currentIndex()+1);
	}

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

void Settings_Email::on_chkUseAccount_toggled(bool checked)
{
	ui.txtAccountName->setEnabled(checked);
	ui.txtAccountPass->setEnabled(checked);
}

/*
void Settings_Email::on_btnTestConnection_clicked()
{
	//test connection
	int nSecurity = ui.cboSecurity->currentIndex()+1;
	int nPort = ui.txtSMTPPort->text().toInt();
	QString strHost = ui.txtSMTPHost->text();
	QString strUser = ui.txtAccountName->text();
	QString strPass = ui.txtAccountPass->text();

	QString strError;
	if(!TestConnection(strHost, nPort, nSecurity, strUser, strPass, strError))
		QMessageBox::warning(NULL, "", tr("Connection failed: %1!").arg(strError));
	else
		QMessageBox::information(NULL, "", tr("Connection success!"));
}

bool Settings_Email_Out::TestConnection(const QString &strHost, int nPort, int nSecurity, const QString &strUser, const QString &strPass, QString &strError)
{
	bool bUseSTLS = (nSecurity == EML_SECURITY_STARTTLS);
	bool bUseSSL = bUseSTLS || (nSecurity == EML_SECURITY_SSLTLS);

	//if(!MailManager::InitSmtpSession(SmtpSession &session, const QString &strHost, int nPort, const QString &strAccName, const QString &strAccPass, bool bEMAIL_SETTINGS_USE_AUTH, bool bUseSSL, bool bUseSTLS))
	return true;
}
*/