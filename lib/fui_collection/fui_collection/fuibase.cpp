#include "fuibase.h"
#include "maincommandbar.h"
#include "common/common/status.h"
#include <QtWidgets/QMessageBox>
#include <QApplication>
#include <QDesktopWidget>
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "bus_client/bus_client/dlg_copyentity.h"
#include "bus_client/bus_client/generatecodedlg.h"
#include "gui_core/gui_core/gui_helper.h"
#include "bus_client/bus_client/selection_treebased.h"
#include "bus_client/bus_client/selection_grouptree.h"
#include "common/common/datahelper.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/macros.h"

//global access right
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				

//global cache:
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;

//global message dispatcher
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;				

//global FUI manager:
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;					

//bo set
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;



FuiBase::FuiBase(QWidget *parent)
: QWidget(parent)
{
	m_bEnableAccessRightCheck=false;
	m_bEnableWindowCloseOnCancelOrOK=false;
	m_pWndAvatar=NULL;
	m_bInitialized=false;
	m_bBlockCloseEvent=false;
	m_nMode=MODE_EMPTY;
	m_pfnClose = NULL;
	m_pMainWnd = NULL;
	m_pCmdBar = NULL;
	m_pSplitter=NULL;
	m_pACOHandler=NULL;
	m_strLockedRes="";
	m_bSideBarModeView=false;

	//entity based:
	m_pSelectionController=NULL;
	m_nEntityRecordID=0;
	m_nEntityID=-1;
	m_bUnconditionalClose = false;
	m_nTableID=-1;

	g_ChangeManager.registerObserver(this);
}

FuiBase::~FuiBase()
{
	if(m_pfnClose)
		m_pfnClose(this, m_pMainWnd);


	//unregister selection change event from selection controller
	if(m_pSelectionController!=NULL)
		m_pSelectionController->unregisterObserver(this);

	if (m_pSplitter) //if fui has main menusplitter->save it:
	{
		QList<int> lstSizes = m_pSplitter->sizes();
		GUI_Helper::SetMenuWidthForFUI(m_nFuiType,lstSizes[0]);
	}

	g_ChangeManager.unregisterObserver(this);
	//if (m_bSideBarModeView)
	g_objFuiManager.ClearActiveFUI(this);
}


void FuiBase::InitFui(int nEntityID,int nTableID,MainEntitySelectionController * pSelectionController,QWidget *pCmdBar)
{
	m_bInitialized=true;
	m_pCmdBar = pCmdBar;

	this->setAttribute(Qt::WA_DeleteOnClose, true);
	
	m_pSelectionController=pSelectionController;
	m_nEntityID=nEntityID;
	m_nTableID=nTableID;

	if(m_pSelectionController!=NULL)
		m_pSelectionController->registerObserver(this);

	//define data:
	if (nTableID!=-1)
		DataDefine();
	SetFUIStyle();
}



void FuiBase::RegisterCloseFn(FUI_OnClose pfn, QWidget *pMainWnd)
{
	m_pfnClose = pfn;
	m_pMainWnd = pMainWnd;
}

bool FuiBase::IsStandAlone()
{
	if (parent())
		return false;
	else
		return true;
}

//when inited: splitter is resized
//call in FUI windows constructor after all initialization
void FuiBase::InitializeFUIWidget(QSplitter *pSplitter)//, int nInitLeftMenuSize)
{
	m_pSplitter=pSplitter;
	//m_nInitLeftMenuSize=nInitLeftMenuSize;
	m_nInitLeftMenuSize=GUI_Helper::GetMenuWidthForFUI(m_nFuiType);

	//resize right away:
	if(m_pSplitter!=NULL)
	{
		QList<int> lstSizes = m_pSplitter->sizes();
		int nTotal = width();
		lstSizes[0] = m_nInitLeftMenuSize;
		lstSizes[1] = nTotal - lstSizes[0];
		m_pSplitter->setSizes(lstSizes);

		connect(m_pSplitter, SIGNAL(splitterMoved(int,int)), this, SLOT(OnSplitterMoved()));
	}

	//default keypress handling
	/*
	if(NULL != m_pCmdBar)
	{
		MainCommandBar *pCmd = ((MainCommandBar *)m_pCmdBar);
		QAction* pAction = new QAction(tr("&OK"), this);
		pAction->setShortcut(tr("Enter"));
		connect(pAction, SIGNAL(triggered()), pCmd->ui.btnOK, SLOT(clicked()));
		this->addAction(pAction);

		QAction* pAction1 = new QAction(tr("&Cancel"), this);
		pAction1->setShortcut(tr("Esc"));
		connect(pAction1, SIGNAL(triggered()), pCmd->ui.btnCancel, SLOT(clicked()));
		this->addAction(pAction1);
	}
	*/
}

//resize event of widget
void FuiBase::resizeEvent ( QResizeEvent * event )
{
	//if no splitter then exit
	if(m_pSplitter!=NULL)
	{
		//
		// on resize event make sure our html menu keeps the same width
		//
		QList<int> lstSizes = m_pSplitter->sizes();
		int nTotal = width();
		lstSizes[0] = m_nInitLeftMenuSize;
		lstSizes[1] = nTotal - lstSizes[0];
		m_pSplitter->setSizes(lstSizes);
	}


	//this must be after spliter resize, otherwise flicking!!!
	QWidget::resizeEvent(event);
}

//resize event of spliter
void FuiBase::OnSplitterMoved()
{
	QList<int> lstSizes = m_pSplitter->sizes();
	m_nInitLeftMenuSize = lstSizes[0];
}

//close event interceptor
void FuiBase::closeEvent(QCloseEvent *event)
{
	if (m_bBlockCloseEvent)
	{
		event->ignore();
		return;
	}


	if (m_bInitialized)on_cmdCancel(); //B.T. added<---- removes lock from record, if not init, then skip
	event->accept();
}

/*! Catches SELECTION change from selection controller
	
	\param pSignalSource	- source of msg
	\param nMsgCode			- msg code
	\param val				- value sent from observer
*/
void FuiBase::updateObserver(ObsrPtrn_Subject* pSignalSource, int nMsgCode,int nMsgDetail,const QVariant val)
{
	//determine if our selection controller & right message:
	if(m_pSelectionController==pSignalSource)
	{
		if (!g_objFuiManager.IsActiveWindowCurrentFUI(this) && m_bSideBarModeView) 
			return;
		if (m_nMode==FuiBase::MODE_EDIT || m_nMode==FuiBase::MODE_INSERT)
			return;

		if(nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
			on_selectionChange(nMsgDetail);
		else if(nMsgCode==SelectorSignals::SELECTOR_DATA_CHANGED)
			on_selectionDataChange();//load or reload data
		else if(nMsgCode==SelectorSignals::SELECTOR_ON_INSERT){
			on_cmdInsert();
		}
		else if(nMsgCode==SelectorSignals::SELECTOR_ON_DELETE){
			on_cmdDelete();
		}
		else if (nMsgCode==SelectorSignals::SELECTOR_ON_EDIT){
			on_cmdEdit();
		}
		else if (nMsgCode==SelectorSignals::SELECTOR_ON_INSERT_NEW_FROM_TEMPLATE)
		{
			on_NewFromTemplate();
		}
		else if (nMsgCode==SelectorSignals::SELECTOR_ON_INSERT_NEW_STRUCT_FROM_TEMPLATE)
		{
			on_NewStructFromTemplate();
		}
		else if (nMsgCode==SelectorSignals::SELECTOR_ON_SHOW_DETAILS_NEW_WINDOW)
		{
			//open same record in a new FUI
			g_objFuiManager.OpenFUI(m_nFuiType, true, false, FuiBase::MODE_READ, nMsgDetail);
		}
	}


	//Only if FUI has Actual Org pattern
	if(m_pACOHandler!=NULL)
	{
		//if ACO selection changed locally:
		if(pSignalSource==m_pACOHandler && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
		{
			SetFiltersForCurrentActualOrganization(nMsgDetail);	//virtual call: adjust filters, reload data
			if(GetMode()==MODE_READ || GetMode()==MODE_EMPTY) //ONLY IF READ MODE empty content on FUI
			{
				if(GetMode()==MODE_READ)
				{
					DataClear();
					SetMode(MODE_EMPTY);
				}
				g_ChangeManager.notifyObservers(ChangeManager::FUI_ACTUAL_ORGANIZATION_CHANGED,nMsgDetail,val,this); //propagate change to all
			}
		}

		//if ACO selection changed in other FUI
		if(pSignalSource==&g_ChangeManager && nMsgCode==ChangeManager::FUI_ACTUAL_ORGANIZATION_CHANGED)
		{
			if(GetMode()==MODE_READ || GetMode()==MODE_EMPTY) //ONLY IF READ MODE PROPAGATE GLOBAL, AND CLEAR CONTENT
			{
				SetActualOrganization(nMsgDetail,false); //if read mode and something is loaded, empty content, do not reemit signal
			}
		}
	}

	//if some1 is deleting entity that is sho
	if(pSignalSource==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED && nMsgDetail==m_nEntityID)
	{
		DbRecordSet lstDeleted=val.value<DbRecordSet>();
		on_DataDeleted(lstDeleted);
	}
}

//------------------------------------------------------------
//			BUTTON BAR EVENT HANDLER:
//------------------------------------------------------------

void FuiBase::on_selectionChange(int nEntityRecordID)
{
	Status err;
	if (m_bEnableAccessRightCheck)
	{
		if(!g_AccessRight->IsOperationAllowed(err,m_nTableID,UserAccessRight::OP_READ,nEntityRecordID))
			_CHK_ERR(err);
	}

	if(nEntityRecordID<=0)
	{
		m_nEntityRecordID=0;
		DataClear();
		SetMode(MODE_EMPTY);
		return;
	}

	if(nEntityRecordID==m_nEntityRecordID) 
		return; //if already loaded exit

	DataRead(err,nEntityRecordID);
	_CHK_ERR(err);
	m_nEntityRecordID=nEntityRecordID;
	SetMode(MODE_READ);

	if (m_pACOHandler!=NULL && m_lstData.getRowCount()>0)
	{
		//when read: always set ACO to match record value, if record valu empty, brb,,,assert
		int nOrgIDFromEntityRecord=m_lstData.getDataRef(0,m_strOrganizationId_Field).toInt();
		SetActualOrganization(nOrgIDFromEntityRecord,true);
	}
}

void FuiBase::on_cmdInsert()
{
	Status err;

	if (m_bEnableAccessRightCheck)
	{
		if(!g_AccessRight->IsOperationAllowed(err,m_nTableID,UserAccessRight::OP_INSERT))
			_CHK_ERR(err);
	}

	DataPrepareForInsert(err);
	if (err.IsOK())
	{
		//if hierarchy, open wizard:
		if (!m_strCodeField.isEmpty())
		{
			QString strCode,strName;
			if(NewCodeWizard(strCode,strName))
			{
				if (m_lstData.getRowCount()>0)
				{
					m_lstData.setData(0,m_strCodeField,strCode);
					m_lstData.setData(0,m_strNameField,strName);
				}
			}
			else
			{
				
				DataClear();
				SetMode(MODE_EMPTY);
				return;		
			}
		}
		SetMode(MODE_INSERT);
	}
}

bool FuiBase::on_cmdOK()
{
	qDebug() << "FuiBase::onOK";

	Status err;
	if (m_bEnableAccessRightCheck)
	{
		if (m_nMode==MODE_EDIT)
		{
			if(!g_AccessRight->IsOperationAllowed(err,m_nTableID,UserAccessRight::OP_EDIT,m_nEntityRecordID))
				_CHK_ERR_RET_BOOL_ON_FAIL(err);
		}
		if (m_nMode==MODE_INSERT)
		{
			if(!g_AccessRight->IsOperationAllowed(err,m_nTableID,UserAccessRight::OP_INSERT))
				_CHK_ERR_RET_BOOL_ON_FAIL(err);
		}
	}


	DataCheckBeforeWrite(err);
	_CHK_ERR_RET_BOOL_ON_FAIL(err);

	m_bBlockCloseEvent=true;

	//before write, if ACO pattern then write down ORG ID:
	if(m_pACOHandler!=NULL)
		m_pACOHandler->SaveActualOrganizationID();

	qDebug() << "FuiBase::onOK - write data";

	DataWrite(err);
	if (!err.IsOK())
	{
		qDebug() << "FuiBase::onOK - write error";
		m_bBlockCloseEvent=false;
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}
	if (m_lstData.getRowCount()>0)
	{	
		notifyCache_AfterWrite(m_lstData);
		m_nEntityRecordID=m_lstData.getDataRef(0,m_TableData.m_strPrimaryKey).toInt();
		if(m_pSelectionController)m_pSelectionController->SetCurrentEntityRecord(m_nEntityRecordID,!m_bSideBarModeView); //in sidebar mode notify observers: issue 1223
	}
	SetMode(MODE_READ);
	m_bBlockCloseEvent=false;

	if (m_bEnableWindowCloseOnCancelOrOK)
	{
		qDebug() << "FuiBase::onOK - close FUI";
		close();
	}
	else{
		qDebug() << "FuiBase::onOK - FUI not closed";
	}
	qDebug() << "FuiBase::onOK end";
	return true;
}

void FuiBase::on_cmdCancel()
{
	Status err;
	if(GetMode()==MODE_EDIT)
	{
		DataUnlock(err);
		_CHK_ERR(err);
		SetMode(MODE_READ);
	}
	else
	{
		DataClear();
		SetMode(MODE_EMPTY);
	}
	if (m_bEnableWindowCloseOnCancelOrOK)
	{
		close();
	}
}

/*!
	Invoked by main command bar, locks data, reload if needed
*/
bool FuiBase::on_cmdEdit()
{
	if (m_nEntityRecordID<=0) //no data to edit
		return false;
	Status err;

	if (m_bEnableAccessRightCheck)
	{
		if(!g_AccessRight->IsOperationAllowed(err,m_nTableID,UserAccessRight::OP_EDIT,m_nEntityRecordID))
			_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}

	DataLock(err);
	_CHK_ERR_RET_BOOL_ON_FAIL(err);
	SetMode(MODE_EDIT);
	if (!m_strCodeField.isEmpty())	//store old code if enabled
	{
		if (m_lstData.getRowCount()==1)
		{
			m_strOldCode=m_lstData.getDataRef(0,m_strCodeField).toString();
			m_strOldName=m_lstData.getDataRef(0,m_strNameField).toString();
		}
		else
		{
			m_strOldCode="";
			m_strOldName="";
		}
	}
	return true;
}

/*!
	Invoked by main command bar, delete content if entity loaded
*/
bool FuiBase::on_cmdDelete()
{
	Status err;
	if(m_nEntityRecordID==0)return true;

	if (m_bEnableAccessRightCheck)
	{
		if(!g_AccessRight->IsOperationAllowed(err,m_nTableID,UserAccessRight::OP_DELETE,m_lstData))
			_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}


	if (m_strCodeField.isEmpty())
	{
		QString strMsg=GetFUIName();
		if (!strMsg.isEmpty())
			strMsg=tr("Do you really want to delete '")+strMsg+tr("' permanently from the database?");
		else
			strMsg=tr("Do you really want to delete this record permanently from the database?");

		int nResult=QMessageBox::question(this,tr("Warning"),strMsg,tr("Yes"),tr("No"));
		if (nResult!=0)
			return false; //only if YES
	}
	else
	{
		if(!CheckHierarchyCodeBeforeDelete()) return false; 
	}

	m_bBlockCloseEvent=true;
	DataDelete(err);
	m_bBlockCloseEvent=false;
	_CHK_ERR_RET_BOOL_ON_FAIL(err);
	
	notifyCache_AfterDelete();
	DataClear();
	//m_nEntityRecordID=0;
	SetMode(MODE_EMPTY);

	if (m_bEnableWindowCloseOnCancelOrOK)
	{
		close();
	}
	return true;

}

/*!
	Invoked by main command bar, let user chose what groups to copy->goes into insert mode afterwards
*/
void FuiBase::on_cmdCopy()
{
	Status err;

	//record must be loaded:
	if (m_nEntityRecordID<=0)
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Load Data before copy!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	if (m_bEnableAccessRightCheck)
	{
		if(!g_AccessRight->IsOperationAllowed(err,m_nTableID,UserAccessRight::OP_INSERT))
			_CHK_ERR(err);
	}


	QString strCode,strName;
	if (!m_strCodeField.isEmpty())
	{
		if(!NewCodeWizard(strCode,strName))
			return;								//if user cancel hierarchy wizard, exit
	}
	
	DataPrepareForCopy(err);
	_CHK_ERR(err);

	if (!m_strCodeField.isEmpty() && m_lstData.getRowCount()>0)
	{
		if (!strCode.isEmpty())	m_lstData.setData(0,m_strCodeField,strCode);
		if (!strName.isEmpty()) m_lstData.setData(0,m_strNameField,strName);
	}
	SetMode(MODE_INSERT);
	return;
}


//------------------------------------------------------------
//			CACHE OBSERVER UPDATER
//------------------------------------------------------------

/*!
	Note: only sends record from parent entity record (main)
	Called after WRITE, use to notify global cache and synchronize all selection controllers (move selection to new)
	Override it to adapt it to the format held in cache if different. By default merge()

	\param recNewData	- one row record with entity data: NOTE: ID key column must at index=0
*/
void FuiBase::notifyCache_AfterWrite(DbRecordSet recNewData)
{
	//prepare record if needed
	prepareCacheRecord(&recNewData);

	//if whole reload, skip notification
	if(CheckHierarchyCodeForReload()) return; 		//HCT check

	//Now notify all selection controllers with filters:
	if(GetMode()==MODE_INSERT) //WHEN ADD data: send filter + data
		g_ClientCache.ModifyCache(m_nEntityID,recNewData,DataHelper::OP_ADD,this); 
	else
		g_ClientCache.ModifyCache(m_nEntityID,recNewData,DataHelper::OP_EDIT,this); 
}



/*!
	Called after DELETE, use to notify global cache and synchronize all selection controllers

	\param nDeletedID	- deleted ID, this ID will be look up in cache and purged (by PK name)
*/
void FuiBase::notifyCache_AfterDelete()
{
	if(CheckHierarchyCodeAfterDelete()) return;		//HCT check

	DbRecordSet *data=g_ClientCache.GetCache(m_nEntityID);
	if (data)
	{
		int nRow=data->find(0,m_nEntityRecordID,true);
		if(nRow!=-1)
		{

			DbRecordSet rowDeleted=data->getRow(nRow);
			g_ClientCache.ModifyCache(m_nEntityID,rowDeleted,DataHelper::OP_REMOVE,this);  //fire global signal
			return;
		}
	}

	//if not in cache, just fire signal:

	if (m_strTablePrefix.isEmpty())
		m_strTablePrefix=GetMainEntityTablePrefix();
	if (!m_strTablePrefix.isEmpty()) //only if prefix exists
	{
		DbRecordSet rowDeleted;
		rowDeleted.addColumn(QVariant::Int,m_strTablePrefix+"_ID");
		rowDeleted.addRow();
		rowDeleted.setData(0,0,m_nEntityRecordID);
		QVariant varData;
		qVariantSetValue(varData,rowDeleted);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED,m_nEntityID,varData,this); 
	}
}


void FuiBase::SetMode(int nMode)
{
	m_nMode=nMode;

	//refresh GUI state for the command bar
	//Q_ASSERT(NULL != m_pCmdBar);
	if(NULL != m_pCmdBar)
		((MainCommandBar *)m_pCmdBar)->SetMode(m_nMode);

	//reset current ID:
	if(m_nMode==MODE_EMPTY)
		m_nEntityRecordID=0;

	//emit signal to combo box that FUI changed state (and probably name):
	RefreshFuiName();
}

bool FuiBase::CanClose()
{
	return (m_bUnconditionalClose ||((m_nMode != MODE_EDIT) && (m_nMode != MODE_INSERT)));
}

//register SAPNE of ACO to FUI base
void FuiBase::SetActualOrganizationHandler(Selection_ActualOrganization *pACOHandler,QString strOrganizationId_Field)
{
	m_strOrganizationId_Field=strOrganizationId_Field;
	m_pACOHandler=pACOHandler;
	m_pACOHandler->registerObserver(this);
	SetFiltersForCurrentActualOrganization(m_pACOHandler->GetCurrentActualOrganizationID());
}

//when opening new FUI, set ACO selector to this org (default), can be different from default ACO
//if bSkipReadDataPurge=true then content in READ mode will not be deleted (by default)
void FuiBase::SetActualOrganization(int nActualOrganizationID,bool bSkipReadDataPurge)	
{

	//if same skip
	if (nActualOrganizationID==m_pACOHandler->GetCurrentActualOrganizationID())
		return;
	if (nActualOrganizationID==0)
		return;

	//set ACO on SAPNE, but forbid to emit signals
	m_pACOHandler->SetCurrentEntityRecord(nActualOrganizationID,true);

	SetFiltersForCurrentActualOrganization(nActualOrganizationID);	//reload selectors for new ACO

	if(GetMode()==MODE_READ && !bSkipReadDataPurge) //ONLY IF READ MODE empty content if allowed
	{
		DataClear();
		SetMode(MODE_EMPTY);
	}
}


void FuiBase::RefreshFuiName()
{
	notifyObservers(FUI_CHANGED_NAME,m_nMode,GetFUIName(),this); //refresh FUI name
}


//when return OK, whole data is reloaded into selection controllers (if code changed)
//if returned true, global cache is changed, skip notify cache after write
bool FuiBase::CheckHierarchyCodeForReload()
{
	if (m_strCodeField.isEmpty()) //if not hierarchy
		return false;

	if(m_lstData.getRowCount()!=1)
		return false;

	//HCT operation on a single item can cause big changes within the tree
	//BT: reload only if code changed in EDIT mode (op sucess):
	if (!m_strCodeField.isEmpty() && GetMode()==MODE_EDIT)
	{
		if(m_strOldCode!=m_lstData.getDataRef(0,m_strCodeField).toString())
		{
			//clear cache:
			g_ClientCache.PurgeCache(m_nEntityID);

			//fire reload signal to all selectors:
			QVariant empty;
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,m_nEntityID,empty,this);

			m_strOldCode=""; //reset code
			return true;
		}

		//if code & name not changed return true: ignore signals (issue: 884)
		if(m_strOldCode==m_lstData.getDataRef(0,m_strCodeField).toString() && m_strOldName==m_lstData.getDataRef(0,m_strNameField).toString())
		{
			return true;
		}

	}
	else if (GetMode()==MODE_INSERT) //new root node is inserted: reload all data on client
	{
		if (m_lstData.getDataRef(0,m_strTablePrefix+"_HASCHILDREN").toInt()>0)
			return true;
	}



	return false;
}


//ALWAYS RETURNS TRUE: DATA IS ALWAYS RELOADED!
//if returned true, global cache is changed, skip notify cache after delete
bool FuiBase::CheckHierarchyCodeAfterDelete()
{
	//if HCT then reload
	if (!m_strCodeField.isEmpty() && m_pSelectionController!=NULL)
	{

		if (m_pSelectionController)
		{
			int nParentID=-1;
			Selection_TreeBased *pSelect=dynamic_cast<Selection_TreeBased *>(m_pSelectionController);
			if (pSelect)
			{
				QTreeWidgetItem *item=pSelect->GetTreeWidget()->GetItemByRowID(m_nEntityRecordID);
				if (item)
				{
					QTreeWidgetItem *parent=item->parent();
					nParentID=pSelect->GetTreeWidget()->GetRowIDByItem(parent);
				}
			}
			Selection_GroupTree *pSelect1=dynamic_cast<Selection_GroupTree *>(m_pSelectionController);
			if (pSelect1)
			{
				QTreeWidgetItem *item=pSelect1->GetTreeWidget()->GetItemByRowID(m_nEntityRecordID);
				if (item)
				{
					QTreeWidgetItem *parent=item->parent();
					nParentID=pSelect1->GetTreeWidget()->GetRowIDByItem(parent);
				}
			}

			//reload data from server:
			m_pSelectionController->ReloadData(true);
			if (nParentID>0)
				m_pSelectionController->SetCurrentEntityRecord(nParentID,false);
		}


		//fire reload signal to all selectors:
		QVariant empty;
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,m_nEntityID,empty,m_pSelectionController); //sender <- is our controller: avoid double reload!!!
		return true;
	}

	return false;
}

//if FUI is HCT, before delete, fires this warning, returns true if user wants to delete items
bool FuiBase::CheckHierarchyCodeBeforeDelete()
{
	if (m_strCodeField.isEmpty()) //if not hierarchy
		return false;

	//find node data
	QString strCode = m_lstData.getDataRef(0, m_strCodeField).toString();
	QString strName = m_lstData.getDataRef(0, m_strNameField).toString(); 

	//how many children of this node is in the cache
	//get from cache
	int nDescendantCount = 0;
	DbRecordSet *pData=g_ClientCache.GetCache(m_nEntityID);
	if(pData)
		nDescendantCount=HierarchicalHelper::SelectChildren(strCode,*pData,m_strCodeField);

	//B.T. fixed for multilang (no latin1)
	QString strMsg=tr("Do you really want to delete element \"%1 %2\" from the database?").arg(strCode).arg(strName);

	if(nDescendantCount > 0) //if has children:
		strMsg += tr("\n%1 subelement(s) will be deleted as well!").arg(nDescendantCount);

	//ask
	int nRes = QMessageBox::question(this, tr("Confirmation"), strMsg, QMessageBox::Yes, QMessageBox::No);
	if(QMessageBox::Yes == nRes)
		return true;
	else
		return false;

}



//when data is deleted, if deleted entry is displayed, show empty:
void FuiBase::on_DataDeleted(DbRecordSet &lstDeletedData)
{
	//find current item inside list of deleted data
	int nRow=lstDeletedData.find(0,m_nEntityRecordID,true);
	if (nRow!=-1 && m_nMode==MODE_READ)
	{
		//Go insert mode
		m_nEntityRecordID=0;
		DataClear();
		SetMode(MODE_EMPTY);
	}
}

void FuiBase::EnableHierarchyCodeCheck(QString strCodeField)
{
	m_strTablePrefix=strCodeField.left(strCodeField.indexOf("_"));
	Q_ASSERT(!m_strTablePrefix.isEmpty()); //must be valid
	m_strCodeField=strCodeField;
	m_strNameField=m_strTablePrefix+"_NAME";
}

//Based on currently loaded entity, proposes new code for new entity or copy
//returns false if user cancels wizard
bool FuiBase::NewCodeWizard(QString &strNewCode, QString &strNewName)
{

	//calculate code of a selected item
	//issue 1187: use current selection from selector:
	QString strParentCode;
	QString strName;
	DbRecordSet recSelected;
	int nCurrentRecID;
	m_pSelectionController->GetSelection(nCurrentRecID,strParentCode,strName,recSelected);

	//generate new code for this item
	generatecodedlg dlg;
	dlg.m_strFldPrefix  = m_strTablePrefix;
	dlg.m_nEntityType   = m_nEntityID;
	dlg.SetParentCode(strParentCode);
	//if(NULL == cacheData || 0 == cacheData->getRowCount())
	if(strParentCode.isEmpty())
		dlg.SetProposedCode("P.");	//default
	else
		dlg.SetProposedCode(strParentCode);

	if(0 == dlg.exec())
	{
			if (m_bSideBarModeView) //issue 1461
			{
				close(); //close fui
			}
		return false;
	}
	strNewCode=dlg.m_strProposedCode;
	//TOFIX: strName?

	return true;
}



//---------------------------------------------------------------------------------
//						DATA HANDLERS
//---------------------------------------------------------------------------------

void FuiBase::DataDefine()
{
	DbSqlTableDefinition::GetKeyData(m_nTableID,m_TableData);
	m_lstData.defineFromView(DbSqlTableView::getView(m_TableData.m_nViewID));
}


void FuiBase::DataClear()
{
	m_nEntityRecordID=0;
	m_lstData.clear();
}

//prepare for insert:
void FuiBase::DataPrepareForInsert(Status &err)
{
	DataClear();
	m_lstData.addRow();
}


void FuiBase::DataPrepareForCopy(Status &err)
{
	err.setError(0);
	//clear ID field:
	m_lstData.setColValue(m_lstData.getColumnIdx(m_TableData.m_strPrimaryKey),0);
}



void FuiBase::DataLock(Status &err)
{
	DbRecordSet lst;
	lst.addColumn(QVariant::Int,m_TableData.m_strPrimaryKey);
	lst.addRow();
	lst.setData(0,0,m_nEntityRecordID);
	DbRecordSet lstStatusRows;
	_SERVER_CALL(ClientSimpleORM->Lock(err,m_nTableID,lst,m_strLockedRes, lstStatusRows))
		if (!err.IsOK())
			if (err.getErrorCode()==StatusCodeSet::ERR_SQL_LOCKED_BY_ANOTHER)
			{
				QString strMsg=err.getErrorTextRaw();
				int nPos=strMsg.indexOf(" - ");
				if (nPos>=0)
				{
					QString strPersCode=strMsg.left(nPos);
					//test if this is moa
					if(strPersCode==g_pClientManager->GetPersonCode())
					{

						int nResult=QMessageBox::question(this,tr("Warning"),tr("This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?"),tr("Yes"),tr("No"));
						if (nResult==0)
						{
							//unlock record on server:
							Status err_temp;
							bool bUnlocked=false;
							_SERVER_CALL(ClientSimpleORM->UnLockByRecordID(err_temp,m_nTableID,lst,bUnlocked))
								if (!err_temp.IsOK() || !bUnlocked)
								{
									return; //return with old lock error
								}
								else
								{
									err.setError(0); //clear error and return
									_SERVER_CALL(ClientSimpleORM->Lock(err,m_nTableID,lst,m_strLockedRes, lstStatusRows))
										return;
								}

						}

					}
				}
			}
}

void FuiBase::DataDelete(Status &err)
{
	DbRecordSet lst;
	lst.addColumn(QVariant::Int,m_TableData.m_strPrimaryKey);
	lst.addRow();
	lst.setData(0,0,m_nEntityRecordID);

	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;

	if (m_strCodeField.isEmpty())
		_SERVER_CALL(ClientSimpleORM->Delete(err,m_nTableID,lst,m_strLockedRes, lstStatusRows, pBoolTransaction))
	else
		_SERVER_CALL(ClientSimpleORM->DeleteHierarchicalData(err,m_nTableID,lst,m_strLockedRes)) //hier data


	//can not clear ID at this point-> after cache notify
	//if (err.IsOK())
	//	DataClear();
}


void FuiBase::DataWrite(Status &err)
{
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	DbRecordSet lstForDelete;

	if (m_strCodeField.isEmpty())
		_SERVER_CALL(ClientSimpleORM->Write(err,m_nTableID,m_lstData,m_strLockedRes, nQueryView, nSkipLastColumns, lstForDelete))
	else
		_SERVER_CALL(ClientSimpleORM->WriteHierarchicalData(err,m_nTableID,m_lstData,m_strLockedRes)) //hier data

	if (err.IsOK())
	{
		m_strLockedRes.clear();
		m_lstDataCache=m_lstData;
	}
}

void FuiBase::DataUnlock(Status &err)
{
	bool bOK;
	if(m_nMode==MODE_EDIT && !m_strLockedRes.isEmpty())
	{
		_SERVER_CALL(ClientSimpleORM->UnLock(err,m_nTableID,bOK,m_strLockedRes))
		if (err.IsOK())
		{
			m_strLockedRes.clear();
			m_lstData=m_lstDataCache;
		}
	}
	else
		DataClear();
}


//reads full view record based on given Primary key
void FuiBase::DataRead(Status &err,int nRecordID)
{
	QString strWhere=" WHERE " + m_TableData.m_strPrimaryKey+"="+QVariant(nRecordID).toString();
	_SERVER_CALL(ClientSimpleORM->Read(err,m_nTableID,m_lstData,strWhere))
	m_lstDataCache=m_lstData;
}




void FuiBase::SetFUIStyle()
{
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

bool FuiBase::event ( QEvent * event )
{

	//if focus->and if active set in fuimanager as last active FUI
	if (event->type()==QEvent::WindowActivate && !isMinimized())
	{
		g_objFuiManager.SetActiveFUI(this);
		if (m_bSideBarModeView)
		{
			if (m_nEntityID==ENTITY_BUS_CONTACT)
				g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_SIDEBAR_FUI_CONTACT_ACTIVE,m_nEntityRecordID,QVariant(),this); //propagate change to all
			else if (m_nEntityID==ENTITY_BUS_PROJECT)
				g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_SIDEBAR_FUI_PROJECT_ACTIVE,m_nEntityRecordID,QVariant(),this); //propagate change to all
		}
	}


	return QWidget::event(event); //pass event
}


QString FuiBase::GetMainEntityTablePrefix()
{
	return m_TableData.m_strFieldPrefix;
}

QPoint FuiBase::GetFuiPosition()
{
	//center window on screen (multi-monitor setup safe)
	int nScreen = QApplication::desktop()->primaryScreen();
	int nScreenHeight = QApplication::desktop()->screenGeometry(nScreen).height();
	int nScreenWidth  = QApplication::desktop()->screenGeometry(nScreen).width();
	int nHeight = rect().height();
	int nWidth  = rect().width();

	return QPoint((nScreenWidth-nWidth)/2, (nScreenHeight-nHeight)/2);
}
