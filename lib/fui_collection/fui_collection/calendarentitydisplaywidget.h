#ifndef CALENDARENTITYDISPLAYWIDGET_H
#define CALENDARENTITYDISPLAYWIDGET_H

#include <QtWidgets/QFrame>
#include <QHash>
#include <QtWidgets/QTableWidget>
#include <QTextEdit>

#include "calendargraphicsview.h"

class TextEdit : public QTextEdit
{
public:
	TextEdit(QGraphicsView *pCalendarGraphicsView, const QString &text, QWidget *parent = 0);

protected:
//	bool viewportEvent(QEvent *event);

private:
	QGraphicsView *m_pCalendarGraphicsView;
};

class CalendarEntityDisplayWidget : public QFrame
{
	Q_OBJECT

public:
	CalendarEntityDisplayWidget(QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, 
								QString strContact, QString strProjects, QString strDescription, QHash<int, qreal> hshBrakeStart, 
								QHash<int, qreal> hshBrakeEnd, CalendarGraphicsView *pCalendarGraphicsView, int nBcep_Status, 
								int nBCEV_IS_INFORMATION, bool bIsProposed, QWidget *parent=0);
	~CalendarEntityDisplayWidget();

	void SetSelected(bool bSelected){m_bSelected = bSelected;};

private:
	void paintEvent(QPaintEvent *event);

	QColor			  m_typeColor;
	QColor			  m_bodyColor;
	QHash<int, qreal> m_hshBrakeStart;
	QHash<int, qreal> m_hshBrakeEnd;
	bool			  m_bSelected;
	int				  m_nBcep_Status;
	int				  m_nBCEV_IS_INFORMATION;
	bool			  m_bIsProposed;
	CalendarGraphicsView *m_pCalendarGraphicsView;
	QTableWidgetItem *m_item1;
	QTableWidgetItem *m_item2;
	QTableWidgetItem *m_item3;
	QTableWidgetItem *m_item4;
	QTableWidgetItem *m_item5;
	QTableWidgetItem *m_item6;
	QTextEdit		 *m_edit;
};

#endif // CALENDARENTITYDISPLAYWIDGET_H
