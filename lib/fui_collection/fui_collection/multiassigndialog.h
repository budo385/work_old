#ifndef MULTIASSIGNDIALOG_H
#define MULTIASSIGNDIALOG_H

#include <QtWidgets/QDialog>
#include "ui_multiassigndialog.h"

#include "common/common/dbrecordset.h"

class MultiAssignDialog : public QDialog
{
    Q_OBJECT

public:
    MultiAssignDialog(DbRecordSet *ItemsRecordSet, QWidget *parent = 0);
    ~MultiAssignDialog();
	
	DbRecordSet& GetSelectedItems();
	void SetTitle(QString strTitle);

private:
    Ui::MultiAssignDialogClass ui;

	void InitializeSelectWidget();
	void InsertItem(QTreeWidgetItem *Item);
	void RemoveItem(QTreeWidgetItem *Item);
	void OkButtonStateChanged();

	QHash<int, QTreeWidgetItem*> m_hshItemsHash;				//< ID to tree item hash.
	QHash<int, int>				 m_hshItemIDToItemRowHash;		//< Stores Item ID to row in recordset (for faster operations - not to use find).
	QHash<int, QTreeWidgetItem*> m_hshSelectedItemHash;			//< Selected item ID to item hash.
	DbRecordSet					 *m_ItemsRecordSet;				//< Pointer to source recordset.
	DbRecordSet					 m_SelectedItemsRecordSet;		//< Selected recordset.

private slots:
	void on_CancelPushButton_clicked();
	void on_OkPushButton_clicked();
	void on_RemoveAll_pushButton_clicked();
	void on_RemoveSelected_pushButton_clicked();
	void on_AddAll_pushButton_clicked();
	void on_AddSelected_pushButton_clicked();
	void on_ItemDropped();
};

#endif // MULTIASSIGNDIALOG_H
