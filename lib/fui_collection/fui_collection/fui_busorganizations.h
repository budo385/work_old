#ifndef FUI_BUSORGANIZATIONS_H
#define FUI_BUSORGANIZATIONS_H

#include <QWidget>
#include "generatedfiles/ui_fui_busorganizations.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"

class FUI_BusOrganizations : public FuiBase
{
    Q_OBJECT

public:
    FUI_BusOrganizations(QWidget *parent = 0);
    ~FUI_BusOrganizations();

	void on_selectionChange(int nEntityRecordID);
	void on_cmdInsert();
	void on_cmdCopy();
	QString GetFUIName();

protected:
	void DataPrepareForInsert(Status &err);
	void DataCheckBeforeWrite(Status &err);

private:
	void RefreshDisplay();
	void SetMode(int nMode);

    Ui::FUI_BusOrganizationsClass ui;
	GuiFieldManager *m_GuiFieldManagerMain;
	void FilterAssigments(int nOrgID);

private slots:
	void on_btnEditDescription_clicked();
	void on_btnOpenPersons_clicked();
	void on_btnOpenCostCenters_clicked();
	void on_btnOpenDepartments_clicked();
};

#endif // FUI_BUSORGANIZATIONS_H
