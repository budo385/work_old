#ifndef ARTREEWIDGET_H
#define ARTREEWIDGET_H

#include <QTreeWidget>
#include <QDropEvent>
#include <QMenu>

class ARTreeWidget : public QTreeWidget
{
    Q_OBJECT

public:
    ARTreeWidget(QWidget *parent = 0);
    ~ARTreeWidget();

	QList<int> GetSelectedItems();
	void SetParentContextMenuActions(QList<QAction *> ParentActions);
	void SetChildContextMenuActions(QList<QAction *> ChildActions);
	void SetWidgetContextMenuActions(QList<QAction *> WidgetActions);
	bool dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action);

protected:
	virtual void contextMenuEvent(QContextMenuEvent *event);
	virtual void focusInEvent(QFocusEvent *event);
	virtual void focusOutEvent(QFocusEvent *event);

private:
	QList<QAction *>	m_ParentActions, 
						m_ChildActions,
						m_WidgetActions;

signals:
	void ARSDropped(QTreeWidgetItem *parent, int index);
	void RoleDropped();
	void FocusIn();
	void FocusOut();
};

#endif // ARTREEWIDGET_H
