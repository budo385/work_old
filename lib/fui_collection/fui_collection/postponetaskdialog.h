#ifndef POSTPONETASKDIALOG_H
#define POSTPONETASKDIALOG_H

#include <QtWidgets/QDialog>
#include "ui_postponetaskdialog.h"
#include <QDateTime>
#include <QTimer>

class PostponeTaskDialog : public QDialog
{
	Q_OBJECT

public:
	PostponeTaskDialog(QDateTime &dateStart, QDateTime &dateDue, QWidget *parent = 0);
	~PostponeTaskDialog();

	QDateTime GetStartDate();
	QDateTime GetDueDate();

	void SetStartDate(const QDateTime &date);
	void SetDueDate(const QDateTime &date);
	void RefdrawFromDate();
	void IncreaseDate(QDateTime &date, int nIndex, bool bForward = true);

private slots:
	void on_cboTimeAmount_selection(int index);
	void on_dateStart_changed(const QDateTime &datetime);
	void on_dateDue_changed(const QDateTime &datetime);
	void on_btnOK_clicked();
	void on_btnCancel_clicked();
	void on_fromDate_UpdateTimer();

private:
	Ui::PostponeTaskDialogClass ui;

	QTimer		m_fromUpdateTimer;
	QDateTime	m_dateFrom;
	QDateTime	m_dateStart;
	QDateTime	m_dateDue;
	QDateTime	m_dateStartOrig;
	QDateTime	m_dateDueOrig;
	int			m_nDueStartDiff;	//distance between due and start

signals:
	void returnDates(QDateTime datDueDateTime, QDateTime datStartDateTime);
};

#endif // POSTPONETASKDIALOG_H
