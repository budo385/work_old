#include "fui_resources.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "common/common/datahelper.h"
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;
#include "fui_collection/fui_collection/fui_calendar.h"

fui_resources::fui_resources(QWidget *parent)
	: FuiBase(parent)
{
	ui.setupUi(this);
	ui.frameSelection->Initialize(ENTITY_CALENDAR_RESOURCES,true,true); //skip loading data, coz this is under filter =ACO
	InitFui(ENTITY_CALENDAR_RESOURCES, BUS_CAL_RESOURCE,dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);
	ui.widgetReservations->Initialize(BUS_CAL_RESOURCE);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData, DbSqlTableView::TVIEW_BUS_CAL_RESOURCE);
	m_GuiFieldManagerMain->RegisterField("BRES_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BRES_LOCATION",ui.txtLocation);
	m_GuiFieldManagerMain->RegisterField("BRES_VALID_FROM",ui.dateValidFrom);
	m_GuiFieldManagerMain->RegisterField("BRES_VALID_TO",ui.dateValidTo);
	m_GuiFieldManagerMain->RegisterField("BRES_DESCRIPTION",ui.frameDesc);

	//---------------------------------------
	//init ACTUAL ORG SMR:
	//---------------------------------------
	ui.frameOrganization->Initialize(&m_lstData,"BRES_ORGANIZATION_ID");
	SetActualOrganizationHandler(ui.frameOrganization,"BRES_ORGANIZATION_ID");

	m_pCalendarButton =	new StyledPushButton(this,":CalendarEvent24.png",":CalendarEvent24.png","","",0,0);
	m_pCalendarButton->setMinimumSize(QSize(24,24));
	m_pCalendarButton->setMaximumSize(QSize(24,24));
	m_pCalendarButton->setToolTip(tr("Resource Calendar Entries"));
	ui.btnButtonBar->GetButtonLayout()->insertWidget(4,m_pCalendarButton);
	connect(m_pCalendarButton,SIGNAL(clicked()),this,SLOT(OnBtnCalendar_Click()));
	m_pCalendarButton->setEnabled(false);

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
		m_pCalendarButton->setVisible(false);

	if (!g_FunctionPoint.IsFPAvailable(FP_RESERVATIONS__RESOURCE))
	{
		ui.tabWidget->widget(1)->setVisible(false);
		ui.tabWidget->removeTab(1);
	}

	SetMode(MODE_EMPTY);
}

fui_resources::~fui_resources()
{
}

void fui_resources::RefreshDisplay()
{
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frameOrganization->RefreshDisplay();

}
//when changing state, call this:
void fui_resources::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.widgetReservations->SetEditMode(false);
		ui.txtName->setEnabled(false);
		ui.txtLocation->setEnabled(false);
		ui.dateValidFrom->setEnabled(false);
		ui.dateValidTo->setEnabled(false);
		ui.frameDesc->SetEditMode(false);
		ui.frameSelection->setEnabled(true);
	}
	else
	{
		ui.widgetReservations->SetEditMode(true);
		ui.txtName->setEnabled(true);
		ui.txtLocation->setEnabled(true);
		ui.dateValidFrom->setEnabled(true);
		ui.dateValidTo->setEnabled(true);
		ui.frameDesc->SetEditMode(true);
		ui.frameSelection->setEnabled(false);
	}

	if (m_pCalendarButton)
	{
		if (nMode==MODE_EDIT || nMode==MODE_READ)
		{
			bool bEnableCalendar=true;
			if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_RESOURCE_CALENDARS))
			{
				bEnableCalendar=false;
			}
			m_pCalendarButton->setEnabled(bEnableCalendar);
		}
		else
			m_pCalendarButton->setEnabled(false);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}

void fui_resources::DataClear()
{
	m_nEntityRecordID=0;
	m_lstData.clear();
	ui.widgetReservations->Clear();
}

void fui_resources::DataRead(Status &err,int nRecordID)
{
	QString strWhere = QString(" WHERE BRES_ID=%1 ").arg(nRecordID);
	//g_AccessRight->SQLFilterRecords(BUS_CAL_RESOURCE,strWhere);
	_SERVER_CALL(ClientSimpleORM->Read(err,BUS_CAL_RESOURCE,m_lstData,strWhere,DbSqlTableView::TVIEW_BUS_CAL_RESOURCE))

	if(err.IsOK())
	{
		ui.widgetReservations->Reload(err,nRecordID);
		if (!err.IsOK())
			return;

		m_lstDataCache=m_lstData;
	}
}

void fui_resources::DataWrite(Status &err)
{
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	DbRecordSet lstForDelete;
	_SERVER_CALL(ClientSimpleORM->Write(err,BUS_CAL_RESOURCE,m_lstData,m_strLockedRes, nQueryView, nSkipLastColumns, lstForDelete))
	if (err.IsOK())
	{
		int nRecordID=m_lstData.getDataRef(0,"BRES_ID").toInt();
		ui.widgetReservations->Write(err,nRecordID);
		m_strLockedRes="";
		m_lstDataCache=m_lstData;
	}
}

void fui_resources::DataPrepareForCopy(Status &err)
{
	m_lstData.setColValue(m_lstData.getColumnIdx("BRES_ID"),0);

	DbRecordSet data;
	ui.widgetReservations->GetData(data);
	data.setColValue("BCRS_ID",QVariant(QVariant::Int));
	ui.widgetReservations->Reload(err,-1,&data);
}

void fui_resources::DataUnlock(Status &err)
{
	FuiBase::DataUnlock(err);
	if (err.IsOK() && m_nMode==MODE_EDIT)
	{
		if (m_nEntityRecordID>0)
			ui.widgetReservations->Reload(err,m_nEntityRecordID); //instead of cache just reload widget...
		else
			ui.widgetReservations->Clear();
	}
}

//get FUI name
QString fui_resources::GetFUIName()
{
	return tr("Resources");
}


void fui_resources::on_ckbShowAll_stateChanged(int)
{
	if (ui.ckbShowAll->isChecked())
	{
		ui.frameSelection->ClearLocalFilter();
		ui.frameSelection->ReloadData();
	}
	else
	{
		ui.frameSelection->ClearLocalFilter();
		ui.frameSelection->GetLocalFilter()->SetFilter("BRES_ORGANIZATION_ID",ui.frameOrganization->GetCurrentActualOrganizationID());
		ui.frameSelection->ReloadData();
	}
}

//set filters and refresh display for current Actual Org
void fui_resources::SetFiltersForCurrentActualOrganization(int nActualOrganizationID)
{
	//notify left selection (specific for each FUI):
	if (!ui.ckbShowAll->isChecked())
	{
		ui.frameSelection->ClearLocalFilter();
		ui.frameSelection->GetLocalFilter()->SetFilter("BRES_ORGANIZATION_ID",nActualOrganizationID);
		ui.frameSelection->ReloadData();
	}

}

void fui_resources::prepareCacheRecord(DbRecordSet *recNewData)
{
	DbRecordSet newCacheRecord;
	newCacheRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_RESOURCE_SELECT));
	newCacheRecord.merge(*recNewData);
	*recNewData=newCacheRecord;
}

void fui_resources::DataCheckBeforeWrite(Status &err)
{
	ui.widgetReservations->DataCheckBeforeWrite(err);
}

void fui_resources::OnBtnCalendar_Click()
{
	if (m_nEntityRecordID<=0)
		return;

	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(QDate::currentDate(),from, to);

	QString strName=ui.frameSelection->GetCalculatedName(m_nEntityRecordID);

	DbRecordSet lstRes;
	lstRes.addColumn(QVariant::Int,"BRES_ID");
	lstRes.addColumn(QVariant::String,"BRES_NAME");
	lstRes.addRow();
	lstRes.setData(0,0,m_nEntityRecordID);
	lstRes.setData(0,1,strName);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		//lstContacts.Dump();
		pFui->Initialize(from,to,NULL,NULL,NULL,&lstRes);
	}


}