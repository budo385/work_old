#ifndef QC_MOREFRAME_H
#define QC_MOREFRAME_H

#include <QWidget>
#include "ui_qc_moreframe.h"
#include "common/common/dbrecordset.h"

class QC_MoreFrame : public QWidget
{
	Q_OBJECT

public:
	QC_MoreFrame(QWidget *parent = 0);
	~QC_MoreFrame();

	void Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType = 0 /*0-Large,1-small widget*/);

private:
	Ui::QC_MoreFrameClass ui;

	void SetupGUIwidgets();
	void SetDataToRecordSet(QString strRecordSetFieldName, QString strValue);
	
	DbRecordSet *m_recFullContactData;
	int			m_nParentType;
	QWidget		*m_pParentWidget;

private slots:
	void on_nickname_lineEdit_textChanged(const QString &text);
	void on_department_lineEdit_textChanged(const QString &text);
	void on_profession_lineEdit_textChanged(const QString &text);
	void on_function_lineEdit_textChanged(const QString &text);
	void on_langcode_lineEdit_textChanged(const QString &text);
	void on_external_code_lineEdit_textChanged(const QString &text);
	void on_noemail_checkBox_stateChanged(int state);

	void on_addSelected_toolButton_4_clicked();
	void on_addSelected_toolButton_5_clicked();
	void on_addSelected_toolButton_6_clicked();
	void on_dateBirthDay_dateChanged(const QDate &date);
};

#endif // QC_MOREFRAME_H
