#ifndef FUI_CEEVENTTYPE_H
#define FUI_CEEVENTTYPE_H

#include <QWidget>
#include "generatedfiles/ui_fui_ceeventtype.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"

//default system events names
class SE_INFO {
public:
	SE_INFO(int nSystem, int nCode, QString strCode, QString strName){
		m_nSystem = nSystem; 
		m_nCode	  = nCode; 
		m_strCode = strCode;
		m_strName = strName;	
	};
	void operator =(const SE_INFO &other){ 
		m_nSystem = other.m_nSystem; 
		m_nCode	  = other.m_nCode; 
		m_strCode = other.m_strCode;
		m_strName = other.m_strName;	
	};

public:
	int m_nSystem;
	int m_nCode;
	QString m_strCode;
	QString m_strName;
};

class FUI_CeEventType : public FuiBase
{
    Q_OBJECT

public:
    FUI_CeEventType(QWidget *parent = 0);
    ~FUI_CeEventType();

	virtual void on_cmdInsert();
	virtual bool on_cmdOK();

	QString GetFUIName();

protected:
	void DataCheckBeforeWrite(Status &err);

private:
	void RefreshDisplay();
	void SetMode(int nMode);

    Ui::FUI_CeEventTypeClass ui;
	int m_nCurRootTypeIdx;	// combo box index

	QList<SE_INFO> m_lstEvents;
	DbRecordSet m_lstEventsForType;
	GuiFieldManager *m_GuiFieldManagerMain;

private slots:
	void on_btnGenerateDefaultRec_clicked();
	void OnRootTypeComboChanged(int nIndex);
};

#endif // FUI_CEEVENTTYPE_H
