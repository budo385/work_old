#include "dlg_messenger.h"
/*#include <QWindowsStyle>*/
#include <QStyleFactory>
#include <QStyle>
#include <QDebug>
#include <QDateTime>
#include "common/common/status.h"
#include "common/common/logger.h"
extern Logger g_Logger;								//global logger


Dlg_Messenger::Dlg_Messenger(QWidget *parent)
    : QDialog(parent),m_btnOpenMessenger(NULL),m_pStatusBar(NULL)
{
	ui.setupUi(this);
	setWindowTitle(tr("Messenger"));
}

Dlg_Messenger::~Dlg_Messenger()
{
	//g_Logger.unregisterObserver(this);
}


void Dlg_Messenger::Initialize(QToolButton *btnOpenMessenger, QStatusBar *pStatusBar)
{
	m_btnOpenMessenger=btnOpenMessenger;
	m_pStatusBar=pStatusBar;
	//g_Logger.registerObserver(this);
	connect(btnOpenMessenger,SIGNAL(clicked()),this,SLOT(OnShowMessenger()));
	connect(&g_Logger,SIGNAL(MessageLogged(int,int,QString)),this,SLOT(OnMessageLogged(int,int,QString)));

	btnOpenMessenger->setIcon(QIcon(":bluedot.png"));
	btnOpenMessenger->setToolTip(tr("Show Logged Messages"));

}


void Dlg_Messenger::OnMessageLogged(int nMsgCode,int nMsgDetail,QString strVal)
{
	//if err get test:
	//if (strVal.isEmpty())
	//{
		Status err;
		err.setError(nMsgDetail,strVal);
		strVal=err.getErrorText();
	//}

	//add message to log:
	QIcon icon;
	/*QWindowsStyle style;*/
	QStyle *style = QStyleFactory::create(QLatin1String("windows"));
	QString strDate=QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss");

	switch(nMsgCode)
	{
	case StatusCodeSet::TYPE_ERROR:
		icon=style->standardIcon(QStyle::SP_MessageBoxCritical);
		break;
	case StatusCodeSet::TYPE_INFORMATION:
		icon=style->standardIcon(QStyle::SP_MessageBoxInformation);
		break;
	case StatusCodeSet::TYPE_WARNING:
		icon=style->standardIcon(QStyle::SP_MessageBoxWarning);
	    break;
	default:
		icon=style->standardIcon(QStyle::SP_MessageBoxInformation);
	    break;
	}

	QString strMessage=strDate+"     "+strVal;

	QListWidgetItem *item = new QListWidgetItem(icon,strMessage);
	ui.listWidget->addItem(item);

	//set message on status bar (15 sec delay):
	if (m_pStatusBar)
		m_pStatusBar->showMessage(strVal,15000);

	//ne message:
	m_btnOpenMessenger->setIcon(QIcon(":reddot.png"));
	m_btnOpenMessenger->setToolTip(tr("You have unread messages"));

}



//when button is clicked
void Dlg_Messenger::OnShowMessenger()
{
		
	//global button coordiantes:
	QPoint pos=m_btnOpenMessenger->mapToGlobal(QPoint(0,0));
	pos.setX(pos.x()-width());
	pos.setY(pos.y()-height()-50); //for title bar of window
	//QPoint logPos=mapFromGlobal(pos);
	move(pos);
	exec();

	m_btnOpenMessenger->setIcon(QIcon(":bluedot.png"));
	m_btnOpenMessenger->setToolTip(tr("Show Logged Messages"));

}