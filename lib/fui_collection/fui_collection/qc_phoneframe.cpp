#include "qc_phoneframe.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "phonenumbervalidator.h"
#include "qcw_largewidget.h"
#include "qcw_smallwidget.h"
#include "dlg_phoneformatter.h"
#include "gui_core/gui_core/thememanager.h"

#include "bus_core/bus_core/formatphone.h"

QC_PhoneFrame::QC_PhoneFrame(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	ui.addSelected_toolButton->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_2->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_3->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_5->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_6->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_7->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_8->setIcon(QIcon(":CopyText16.png"));

	ui.formatPhone_toolButton->setIcon(QIcon(":Reformat.png"));
	ui.formatPhone_toolButton_2->setIcon(QIcon(":Reformat.png"));
	ui.formatPhone_toolButton_3->setIcon(QIcon(":Reformat.png"));
	ui.formatPhone_toolButton_4->setIcon(QIcon(":Reformat.png"));
	ui.formatPhone_toolButton_5->setIcon(QIcon(":Reformat.png"));
	ui.formatPhone_toolButton_6->setIcon(QIcon(":Reformat.png"));

	m_pCheckButtonGroup = new QButtonGroup(this);
	m_pCheckButtonGroup->addButton(ui.isDefault_checkBox, 0);
	m_pCheckButtonGroup->addButton(ui.isDefault_checkBox_2, 1);
	m_pCheckButtonGroup->addButton(ui.isDefault_checkBox_3, 2);
	m_pCheckButtonGroup->addButton(ui.isDefault_checkBox_4, 3);
	m_pCheckButtonGroup->addButton(ui.isDefault_checkBox_5, 4);
	m_pCheckButtonGroup->addButton(ui.isDefault_checkBox_6, 5);
	m_pCheckButtonGroup->setExclusive(true);

	connect(m_pCheckButtonGroup, SIGNAL(buttonClicked(int)), this, SLOT(on_m_pCheckButtonGroup_buttonClicked(int)));
/*	
	PhoneNumberValidator *validator = new PhoneNumberValidator(this);
	ui.buscentral_phone_lineEdit->setValidator(validator);
	ui.busdirectl_phone_lineEdit->setValidator(validator);
	ui.busmobilel_phone_lineEdit->setValidator(validator);
	ui.fax_phone_lineEdit->setValidator(validator);
	ui.private_phone_lineEdit->setValidator(validator);
	ui.privatemobile_phone_lineEdit->setValidator(validator);
*/
}

QC_PhoneFrame::~QC_PhoneFrame()
{

}

void QC_PhoneFrame::Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType /*= 0 0-Large,1-small widget*/)
{
	m_nParentType = nParentType;
	m_pParentWidget = ParentWidget;
	m_recFullContactData = recFullContactData;
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_PHONE").value<DbRecordSet>();
	_DUMP(m_recLocalEntityRecordSet);
	SetupGUIwidgets(m_recLocalEntityRecordSet);
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

void QC_PhoneFrame::AddPhone(DbRecordSet recNewPhone)
{
	SetupGUIwidgets(recNewPhone, true);
}

void QC_PhoneFrame::SetupGUIwidgets(DbRecordSet recData, bool bFromAddPhone /*= false*/)
{
	//Business central phone.
	int nRow = recData.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), true);
	if (nRow>=0 && !ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS).isNull()
		&& !recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString().isEmpty())
	{
		m_pCheckButtonGroup->button(0)->blockSignals(true);
		ui.buscentral_phone_lineEdit->setText(recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString());
		if (recData.getDataRef(nRow, "BCMP_IS_DEFAULT").toInt())
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(0))->setCheckState(Qt::Checked);
		else
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(0))->setCheckState(Qt::Unchecked);
		m_pCheckButtonGroup->button(0)->blockSignals(false);
	}
	else if (!bFromAddPhone)
		ui.buscentral_phone_lineEdit->setText("");

	//Business direct phone.
	nRow = recData.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT), true);
	if (nRow>=0 && !ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT).isNull()
		&& !recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString().isEmpty())
	{
		m_pCheckButtonGroup->button(1)->blockSignals(true);
		ui.busdirectl_phone_lineEdit->setText(recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString());
		if (recData.getDataRef(nRow, "BCMP_IS_DEFAULT").toInt())
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(1))->setCheckState(Qt::Checked);
		else
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(1))->setCheckState(Qt::Unchecked);
		m_pCheckButtonGroup->button(1)->blockSignals(false);
	}
	else if (!bFromAddPhone)
		ui.busdirectl_phone_lineEdit->setText("");

	//Business mobile phone.
	nRow = recData.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), true);
	if (nRow>=0 && !ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE).isNull()
		&& !recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString().isEmpty())
	{
		m_pCheckButtonGroup->button(2)->blockSignals(true);
		ui.busmobilel_phone_lineEdit->setText(recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString());
		if (recData.getDataRef(nRow, "BCMP_IS_DEFAULT").toInt())
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(2))->setCheckState(Qt::Checked);
		else
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(2))->setCheckState(Qt::Unchecked);
		m_pCheckButtonGroup->button(2)->blockSignals(false);
	}
	else if (!bFromAddPhone)
		ui.busmobilel_phone_lineEdit->setText("");
	
	//Fax.
	nRow = recData.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), true);
	if (nRow>=0 && !ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX).isNull()
		&& !recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString().isEmpty())
	{
		m_pCheckButtonGroup->button(3)->blockSignals(true);
		ui.fax_phone_lineEdit->setText(recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString());
		if (recData.getDataRef(nRow, "BCMP_IS_DEFAULT").toInt())
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(3))->setCheckState(Qt::Checked);
		else
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(3))->setCheckState(Qt::Unchecked);
		m_pCheckButtonGroup->button(3)->blockSignals(false);
	}
	else if (!bFromAddPhone)
		ui.fax_phone_lineEdit->setText("");

	//Private phone.
	nRow = recData.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), true);
	if (nRow>=0 && !ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE).isNull()
		&& !recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString().isEmpty())
	{
		m_pCheckButtonGroup->button(4)->blockSignals(true);
		ui.private_phone_lineEdit->setText(recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString());
		if (recData.getDataRef(nRow, "BCMP_IS_DEFAULT").toInt())
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(4))->setCheckState(Qt::Checked);
		else
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(4))->setCheckState(Qt::Unchecked);
		m_pCheckButtonGroup->button(4)->blockSignals(false);
	}
	else if (!bFromAddPhone)
		ui.private_phone_lineEdit->setText("");
	
	//Private mobile phone.
	nRow = recData.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), true);
	if (nRow>=0 && !ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE).isNull()
		&& !recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString().isEmpty())
	{
		m_pCheckButtonGroup->button(5)->blockSignals(true);
		ui.privatemobile_phone_lineEdit->setText(recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString());
		if (recData.getDataRef(nRow, "BCMP_IS_DEFAULT").toInt())
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(5))->setCheckState(Qt::Checked);
		else
			dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(5))->setCheckState(Qt::Unchecked);
		m_pCheckButtonGroup->button(5)->blockSignals(false);
	}
	else if (!bFromAddPhone)
		ui.privatemobile_phone_lineEdit->setText("");

	//Skype.
	nRow = recData.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), true);
	if (nRow>=0 && !ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE).isNull()
		&& !recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString().isEmpty())
		ui.skypeusername_phone_lineEdit->setText(recData.getDataRef(nRow, "BCMP_FULLNUMBER").toString());
	else if (!bFromAddPhone)
		ui.skypeusername_phone_lineEdit->setText("");

	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), ui.buscentral_phone_lineEdit->text());
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt(), ui.busdirectl_phone_lineEdit->text());
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), ui.busmobilel_phone_lineEdit->text());
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), ui.fax_phone_lineEdit->text());
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), ui.private_phone_lineEdit->text());
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), ui.privatemobile_phone_lineEdit->text());
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), ui.skypeusername_phone_lineEdit->text());
}

void QC_PhoneFrame::SetDataToRecordSet(int nRow, QVariant nSystemType, QString strValue)
{
	//If there is no row add and assign.
	if (nRow<0)
	{
		m_recLocalEntityRecordSet.addRow();
		nRow = m_recLocalEntityRecordSet.getRowCount()-1;
	}
	m_recLocalEntityRecordSet.setData(nRow, "BCMP_TYPE_ID", nSystemType);
	m_recLocalEntityRecordSet.setData(nRow, "BCMP_FULLNUMBER", strValue);
	FormatPhone phoneFormat;
	//_DUMP(m_recLocalEntityRecordSet);
	phoneFormat.ReFormatPhoneNumbers(m_recLocalEntityRecordSet, true);
	//_DUMP(m_recLocalEntityRecordSet);
	m_recFullContactData->setData(0, "LST_PHONE", m_recLocalEntityRecordSet);
}

void QC_PhoneFrame::SetIntDataToRecordSet(int nRow, QVariant nSystemType, int nValue)
{
	//If there is no row add and assign.
	if (nRow<0)
	{
		m_recLocalEntityRecordSet.addRow();
		nRow = m_recLocalEntityRecordSet.getRowCount()-1;
	}
	m_recLocalEntityRecordSet.setData(nRow, "BCMP_IS_DEFAULT", nValue);
	FormatPhone phoneFormat;
	//_DUMP(m_recLocalEntityRecordSet);
	phoneFormat.ReFormatPhoneNumbers(m_recLocalEntityRecordSet, true);
	//_DUMP(m_recLocalEntityRecordSet);
	m_recFullContactData->setData(0, "LST_PHONE", m_recLocalEntityRecordSet);
}

void QC_PhoneFrame::on_buscentral_phone_lineEdit_editingFinished()
{
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), ui.buscentral_phone_lineEdit->text());
	if (ui.busdirectl_phone_lineEdit->text().isEmpty() && !ui.buscentral_phone_lineEdit->text().isEmpty())
	{
		dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(0))->setChecked(true);
		on_m_pCheckButtonGroup_buttonClicked(m_pCheckButtonGroup->checkedId());
	}
}

void QC_PhoneFrame::on_busdirectl_phone_lineEdit_editingFinished()
{
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt(), ui.busdirectl_phone_lineEdit->text());
	if (!ui.busdirectl_phone_lineEdit->text().isEmpty())
	{
		dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(1))->setChecked(true);
		on_m_pCheckButtonGroup_buttonClicked(m_pCheckButtonGroup->checkedId());
	}
}

void QC_PhoneFrame::on_busmobilel_phone_lineEdit_editingFinished()
{
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), ui.busmobilel_phone_lineEdit->text());
	if (ui.busdirectl_phone_lineEdit->text().isEmpty() && !ui.busmobilel_phone_lineEdit->text().isEmpty())
	{
		dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(2))->setChecked(true);
		on_m_pCheckButtonGroup_buttonClicked(m_pCheckButtonGroup->checkedId());
	}
}

void QC_PhoneFrame::on_fax_phone_lineEdit_editingFinished()
{
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), ui.fax_phone_lineEdit->text());
	if (ui.busdirectl_phone_lineEdit->text().isEmpty() && !ui.fax_phone_lineEdit->text().isEmpty())
	{
		dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(3))->setChecked(true);
		on_m_pCheckButtonGroup_buttonClicked(m_pCheckButtonGroup->checkedId());
	}
}

void QC_PhoneFrame::on_private_phone_lineEdit_editingFinished()
{
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), ui.private_phone_lineEdit->text());
	if (ui.busdirectl_phone_lineEdit->text().isEmpty() && !ui.private_phone_lineEdit->text().isEmpty())
	{
		dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(4))->setChecked(true);
		on_m_pCheckButtonGroup_buttonClicked(m_pCheckButtonGroup->checkedId());
	}
}

void QC_PhoneFrame::on_privatemobile_phone_lineEdit_editingFinished()
{
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), ui.privatemobile_phone_lineEdit->text());
	if (ui.busdirectl_phone_lineEdit->text().isEmpty() && !ui.privatemobile_phone_lineEdit->text().isEmpty())
	{
		dynamic_cast<QCheckBox*>(m_pCheckButtonGroup->button(5))->setChecked(true);
		on_m_pCheckButtonGroup_buttonClicked(m_pCheckButtonGroup->checkedId());
	}
}

void QC_PhoneFrame::on_skypeusername_phone_lineEdit_editingFinished()
{
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), ui.skypeusername_phone_lineEdit->text());
}

void QC_PhoneFrame::on_addSelected_toolButton_clicked()
{
	if (m_nParentType == 0)
		ui.buscentral_phone_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.buscentral_phone_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());

	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), ui.buscentral_phone_lineEdit->text());
}

void QC_PhoneFrame::on_addSelected_toolButton_2_clicked()
{
	if (m_nParentType == 0)
		ui.busdirectl_phone_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.busdirectl_phone_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());

	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt(), ui.busdirectl_phone_lineEdit->text());
}

void QC_PhoneFrame::on_addSelected_toolButton_5_clicked()
{
	if (m_nParentType == 0)
		ui.busmobilel_phone_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.busmobilel_phone_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());

	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), ui.busmobilel_phone_lineEdit->text());

}

void QC_PhoneFrame::on_addSelected_toolButton_3_clicked()
{
	if (m_nParentType == 0)
		ui.fax_phone_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.fax_phone_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), ui.fax_phone_lineEdit->text());

}

void QC_PhoneFrame::on_addSelected_toolButton_6_clicked()
{
	if (m_nParentType == 0)
		ui.private_phone_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.private_phone_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), ui.private_phone_lineEdit->text());

}

void QC_PhoneFrame::on_addSelected_toolButton_7_clicked()
{
	if (m_nParentType == 0)
		ui.privatemobile_phone_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.privatemobile_phone_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), ui.private_phone_lineEdit->text());

}

void QC_PhoneFrame::on_addSelected_toolButton_8_clicked()
{
	if (m_nParentType == 0)
		ui.skypeusername_phone_lineEdit->setText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.skypeusername_phone_lineEdit->setText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), ui.skypeusername_phone_lineEdit->text());

}

void QC_PhoneFrame::FormatPhoneDialogHandler(int nPhoneType, QLineEdit *p_phoneLineEdit)
{
	QString strPhone,strArea,strCountry,strSearch,strLocal;
	QString strISO = "";
	Dlg_PhoneFormatter dlg;

	int nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,(ContactTypeManager::PhoneSystemTypes)nPhoneType), true);
	if (nRow>=0)
		strISO = m_recLocalEntityRecordSet.getDataRef(nRow,"BCMP_COUNTRY_ISO").toString();
	dlg.SetRow(p_phoneLineEdit->text(), strISO);
	int nRes=dlg.exec();
	if (nRes)
	{
		dlg.GetRow(strPhone,strCountry,strArea,strLocal,strSearch,strISO);

		p_phoneLineEdit->blockSignals(true);
		p_phoneLineEdit->setText(strPhone);
		p_phoneLineEdit->blockSignals(false);
		nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,(ContactTypeManager::PhoneSystemTypes)nPhoneType), true);
		SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,(ContactTypeManager::PhoneSystemTypes)nPhoneType), p_phoneLineEdit->text());
		nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,(ContactTypeManager::PhoneSystemTypes)nPhoneType), true);
		m_recLocalEntityRecordSet.setData(nRow,"BCMP_AREA",strArea);
		m_recLocalEntityRecordSet.setData(nRow,"BCMP_COUNTRY",strCountry);
		m_recLocalEntityRecordSet.setData(nRow,"BCMP_LOCAL",strLocal);
		m_recLocalEntityRecordSet.setData(nRow,"BCMP_SEARCH",strSearch);
		m_recLocalEntityRecordSet.setData(nRow,"BCMP_COUNTRY_ISO",strISO);
		SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,(ContactTypeManager::PhoneSystemTypes)nPhoneType), p_phoneLineEdit->text());
	}
}

void QC_PhoneFrame::on_formatPhone_toolButton_clicked()
{
	FormatPhoneDialogHandler(ContactTypeManager::SYSTEM_PHONE_BUSINESS, ui.buscentral_phone_lineEdit);
}

void QC_PhoneFrame::on_formatPhone_toolButton_2_clicked()
{
	FormatPhoneDialogHandler(ContactTypeManager::SYSTEM_PHONE_DIRECT, ui.busdirectl_phone_lineEdit);
}

void QC_PhoneFrame::on_formatPhone_toolButton_3_clicked()
{
	FormatPhoneDialogHandler(ContactTypeManager::SYSTEM_PHONE_MOBILE, ui.busmobilel_phone_lineEdit);
}

void QC_PhoneFrame::on_formatPhone_toolButton_4_clicked()
{
	FormatPhoneDialogHandler(ContactTypeManager::SYSTEM_FAX, ui.fax_phone_lineEdit);
}

void QC_PhoneFrame::on_formatPhone_toolButton_5_clicked()
{
	FormatPhoneDialogHandler(ContactTypeManager::SYSTEM_PHONE_PRIVATE, ui.private_phone_lineEdit);
}

void QC_PhoneFrame::on_formatPhone_toolButton_6_clicked()
{
	FormatPhoneDialogHandler(ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE, ui.privatemobile_phone_lineEdit);
}

void QC_PhoneFrame::on_m_pCheckButtonGroup_buttonClicked(int id)
{
	int nRow;
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), true);
	SetIntDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), (id ==0 ? 1 : 0));
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT), true);
	SetIntDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt(), (id ==1 ? 1 : 0));
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), true);
	SetIntDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE), (id ==2 ? 1 : 0));
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), true);
	SetIntDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX), (id ==3 ? 1 : 0));
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), true);
	SetIntDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE), (id ==4 ? 1 : 0));
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), true);
	SetIntDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE), (id ==5 ? 1 : 0));
	nRow = m_recLocalEntityRecordSet.find("BCMP_TYPE_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), true);
	SetDataToRecordSet(nRow, ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE), ui.skypeusername_phone_lineEdit->text());
}
