#ifndef FUI_DEBTORPAYMENTS_H
#define FUI_DEBTORPAYMENTS_H

#include <QWidget>
#include "fuibase.h"
#include "ui_fui_debtorpayments.h"
#include "gui_core/gui_core/guifieldmanager.h"

class FUI_DebtorPayments : public FuiBase
{
	Q_OBJECT

public:
	FUI_DebtorPayments(QWidget *parent = 0);
	~FUI_DebtorPayments();

	QString GetFUIName();

protected:
	void DataPrepareForInsert(Status &err);


private:
	Ui::FUI_DebtorPayments ui;
	GuiFieldManager *m_GuiFieldManagerMain;

	void prepareCacheRecord(DbRecordSet *recNewData);
	void RefreshDisplay();
	void SetMode(int nMode);
};

#endif // FUI_DEBTORPAYMENTS_H
