#ifndef QCW_BASE_H
#define QCW_BASE_H

#include <QWidget>
#include "common/common/dbrecordset.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"

class QCW_Base : public FuiBase
{
	Q_OBJECT

public:
	enum QCW_Mode
	{
		MODE_SINGLE,
		MODE_MULTIPLE
	};

	QCW_Base(QWidget *parent);
	~QCW_Base();

	virtual bool Initialize(DbRecordSet recContactData);
	virtual bool Initialize(int nEntityRecordID);
	virtual bool Initialize(QString strDropText) = 0;
	virtual void SetDefaultValues(DbRecordSet &recContactData, QString strLeftSelectorText) = 0;
	void GetDefaultValues(DbRecordSet &recContactData, int &nMode);
	void SetFUIStyle();

	virtual QString GetSelectedText() = 0;

	void	UnlockContact(QString &strLockResourceID);
	void	UnlockContactNoRet(QString &strLockResourceID);
	void	InitializeSingleContact(DbRecordSet &recFullContactData, int &nEntityRecordID, QString &strLockResourceID);
	bool	OnOK(int &nOperation,DbRecordSet &lstUAR,DbRecordSet &lstGAR);
	void	OnCancel();
	QString GetFUIName();

protected:
	virtual void SetupFrames(bool bCheckNames = false) = 0;
	void ClearAll();
	bool CheckDuplicates();

	//Do not override this data with empty fields.
	void CheckAdresses(DbRecordSet &lstDroppedData);
	void CheckEmail(DbRecordSet &lstDroppedData);
	void CheckInternetAdresses(DbRecordSet &lstDroppedData);
	void CheckPhones(DbRecordSet &lstDroppedData);

	int						m_nEntityRecordID;
	int						m_nQCWMode;
	int						m_nSelectedRowInLeftSelector;
	DbRecordSet				m_recFullContactData;
	QHash<int, DbRecordSet> m_hshContactDataHash;
	QString					m_strLockResourceID;
	DbRecordSet				m_lstAddressOrganizationchangePropagate;
	DbRecordSet				m_lstDebtor;

public:
	void SalutationChanged(QString strRecordSetFieldName, QString strValue);
	void LanguageChanged(QString strRecordSetFieldName, QString strValue);
	void AddressChanged(int nCurrentRow, QString strRecordSetFieldName, QString strValue);
	void AddressTypeChanged(int nCurrentRow, DbRecordSet rowTypes);
	void AddressAdded(DbRecordSet recAddress);

private:
	void CleanUpBeforeSave(Status &status);
	virtual void UpdateSalutationOnAdressFrame(QString strRecordSetFieldName, QString strValue) = 0;
	
};

#endif // QCW_BASE_H
