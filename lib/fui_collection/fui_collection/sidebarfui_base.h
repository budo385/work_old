#ifndef SIDEBARFUI_BASE_H
#define SIDEBARFUI_BASE_H

#include "communicationmenu_base.h"

class SideBarFui_Base : public CommunicationMenu_Base
{
	Q_OBJECT

public:
	SideBarFui_Base(QWidget *parent);
	virtual ~SideBarFui_Base();

	virtual void SetCurrentFUI(bool bCurrent){m_bCurrentFUI=bCurrent;};
	virtual bool HideVisibleFramesIfNeeded(int nFrameOpened /* 0 -ACL, 1 -AC, 2-DP */)=0;
	bool IsCurrentFUI(){return m_bCurrentFUI;}
	void CloseChildWindows();
	int GetEntityRecordID(){return m_nEntityRecordID;};
	bool ProcessKeyPressEventFromMainWindow( QKeyEvent * event ); 

protected:
	virtual QFrame* GetFrameCommToolBar()=0;
	virtual QFrame* GetFrameCommParent()=0;
	virtual QLabel* GetFrameCommLabel()=0;
	virtual DropZone_MenuWidget* GetDropZone_MenuWidget()=0;
	virtual void GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strMail,int &nEmailReplyID,QString &strPhone,QString &strInternet)=0;

	void SetSharedMenuDoc(Doc_MenuWidget* pWidget){m_menuDoc=pWidget;};
	void SetSharedMenuEmail(Email_MenuWidget* pWidget){m_menuEmail=pWidget;};
	void SetSharedMenuApp(App_MenuWidget* pWidget){m_menuApp=pWidget;};
	void SetSharedMenuCalendar(Calendar_MenuWidget* pWidget){m_menuCalendar=pWidget;};

protected slots	:
	virtual void OnSelectionChange(int nEntityRecordID){};	//note: if 0 then it will invalidate FUI: go empty mode
	virtual void BuildMenu();	
	virtual void SubMenuRequestedNewData(int nSubMenuType=CommunicationMenu_Base::SUBMENU_DROPZONE);
	void OnMenu_EmailClick();
	void OnMenu_DocumentClick();
	void OnMenu_ApplicationClick();
	void OnMenu_CalendarClick();

	virtual void MenuDoc_Destroyed(QObject *);
	virtual void MenuEmail_Destroyed(QObject *);
	virtual void MenuApp_Destroyed(QObject *);
	virtual void MenuCalendar_Destroyed(QObject *);

	virtual void MenuDoc_Created();
	virtual void MenuEmail_Created();
	virtual void MenuApp_Created();
	virtual void MenuCalendar_Created();

	virtual void OnDropZone_NewDataDropped(int,DbRecordSet){};
	void OnDropZoneChapterCollapsed(bool bCollapsed);


protected:
	void OnThemeChanged();
	int m_nEntityRecordID;		//record id-> actual selection,-1 if none
	int m_nFuiTypeID;	//type of fui: project, contact, desktop
	bool m_bCurrentFUI;
	static QList<SideBarFui_Base*> m_lstpInstances;
};

#endif // SIDEBARFUI_BASE_H
