#include "sidebarfui_contact.h"
#include <QClipboard>
#include "gui_core/gui_core/thememanager.h"
#include "reportmanager.h"
#include <QDesktopWidget>
#include "bus_core/bus_core/nmrxmanager.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "fuimanager.h"
#include "fui_contacts.h"
#include "fui_collection/fui_collection/fui_calendar.h"
#include "gui_core/gui_core/macros.h"
#include "common/common/datahelper.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"
#include "gui_core/gui_core/dlg_enterperiod.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "report_core/report_core/rptprinter.h"
extern FuiManager g_objFuiManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;				
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester



bool Call_OnLoadFullPicture(int nPicID, DbRecordSet &rowPic);

SideBarFui_Contact::SideBarFui_Contact(QWidget *parent)
	: SideBarFui_Base(parent),m_pTxtActual(NULL),m_btnHideList(NULL),m_btnHideActual(NULL),m_bHideContactPaneOnDetailClickFirstTime(true),
	m_Spacer(NULL),m_bCommGridInit(false),m_pTxt1(NULL),m_pTxt2(NULL),m_bDetailFrameInit(false),m_pFrameContacts(NULL),m_pBtnCalendar(NULL)
{
	ui.setupUi(this);
	m_nFuiTypeID=MENU_CONTACTS;
	setProperty("sidebar_fui",true);
	m_pFrameContacts=ui.frameContactAndFavs->GetSelectionContacts();
	BuildMenu();

	//in rowContactPicture, load contact picture when contact sapne is loaded
	m_rowContactPicture.destroy();
	m_rowContactPicture.addColumn(QVariant::Int,"BCNT_ID" );
	m_rowContactPicture.addColumn(QVariant::ByteArray,"BCNT_PICTURE" );
	m_rowContactPicture.addColumn(QVariant::Int,"BCNT_BIG_PICTURE_ID" );
	m_rowContactPicture.addColumn(QVariant::ByteArray,"BPIC_PICTURE" );
	m_rowContactPicture.addRow();
	ui.picture->Initialize(PictureWidget::MODE_PREVIEW,&m_rowContactPicture,"BCNT_PICTURE","BPIC_PICTURE","BCNT_BIG_PICTURE_ID","BCNT_ID");
	ui.picture->SetLoadFullPictureCallBack(Call_OnLoadFullPicture);
	ui.picture->RefreshDisplay();

	m_bShowPicture=true;
	ui.txtInfo_Address->setReadOnly(true);
	ui.txtDesc->setReadOnly(true);
	ui.frameActualParent->setVisible(false);

	g_objFuiManager.RegisterSideBarCommMenuSelector(MENU_CONTACTS,this);
	//ui.splitter->handle(1)->setVisible(false);
	//ui.splitter->setCollapsible(0,false);
	ui.splitter->setCollapsible(1,false);

	g_ChangeManager.registerObserver(this);
	ui.frameContactAndFavs->registerObserver(this);

	connect (ui.frameContactAndFavs,SIGNAL(FavoriteChanged(int)),this,SLOT(OnFavoriteChanged(int)));

}

SideBarFui_Contact::~SideBarFui_Contact()
{
	g_objFuiManager.UnRegisterSideBarCommMenuSelector(MENU_CONTACTS,this);
	g_ChangeManager.unregisterObserver(this);
}

void SideBarFui_Contact::SetCurrentFUI(bool bCurrent)
{
	m_bCurrentFUI=bCurrent;
	ui.widgetCommGrid->SetCurrentGridInSidebarMode(bCurrent);
	if (m_bCurrentFUI)
		QTimer::singleShot(0,this,SLOT(OnSetFindFocusDelayed()));
}

void SideBarFui_Contact::BuildMenu()
{
	SideBarFui_Base::BuildMenu();

	

	//-----------------------------
	//LIST HEADER:
	//-----------------------------
	QSize buttonSize1(22, 22);
	//m_btnHideList= new StyledPushButton(":Icon_PulseMinus.mng","mng",true,this,"QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex(),"",0,0);
	m_btnHideList= new StyledPushButton(this,":Icon_Minus12.png",":Icon_Minus12.png","QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex(),"",0,0);
	m_btnHideList->hide();
	ui.labelComm->hide();

	//create buttons & labels:
	m_pTxt1		= new QLabel(this);
	m_pTxt1->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	m_pTxt1->setAlignment(Qt::AlignLeft);
	m_pTxt1->setText(tr("Contact List"));

	QSize buttonSize(17, 17);
	StyledPushButton *pBtnAdd1		=	new StyledPushButton(this,":DbIcon_Insert16.png",":DbIcon_Insert16.png","","",0,0);
	StyledPushButton *pBtnClear1	=	new StyledPushButton(this,":DbIcon_Clear16.png",":DbIcon_Clear16.png","","",0,0);
	StyledPushButton *pBtnPrintList	=	new StyledPushButton(this,":Icon_Printer16.png",":Icon_Printer16.png","","",0,0);

	//connect signals
	connect(m_btnHideList,SIGNAL(clicked()),this,SLOT(OnHideList()));
	connect(pBtnAdd1,SIGNAL(clicked()),this,SLOT(OnList_InsertClick()));
	connect(pBtnClear1,SIGNAL(clicked()),this,SLOT(OnList_ClearClick()));
	connect(pBtnPrintList,SIGNAL(clicked()),this,SLOT(OnList_PrintClick()));
	
	//size
	m_btnHideList->	setMaximumSize(buttonSize1);
	m_btnHideList->	setMinimumSize(buttonSize1);
	pBtnAdd1		->setMaximumSize(buttonSize);
	pBtnAdd1		->setMinimumSize(buttonSize);
	pBtnClear1		->setMaximumSize(buttonSize);
	pBtnClear1		->setMinimumSize(buttonSize);
	pBtnPrintList	->setMaximumSize(buttonSize);
	pBtnPrintList	->setMinimumSize(buttonSize);
	pBtnAdd1->setToolTip(tr("Add Contact"));
	pBtnClear1->setToolTip(tr("Clear List"));
	pBtnPrintList->setToolTip(tr("Print Selected Contacts"));
	
	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addWidget(m_btnHideList);
	buttonLayout->addSpacing(5);
	buttonLayout->addWidget(m_pTxt1);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnAdd1);
	buttonLayout->addWidget(pBtnClear1);
	buttonLayout->addWidget(pBtnPrintList);
	buttonLayout->setContentsMargins(0,0,0,0);
	buttonLayout->setSpacing(2);
	ui.frameList->setLayout(buttonLayout);

	//-----------------------------
	//CONTACT:
	//-----------------------------
	m_pFrameContacts->Initialize();
	m_pFrameContacts->registerObserver(this);
	//connect(m_pFrameContacts,SIGNAL(ShowContactDetails(int)),this,SLOT(OnSelector_ShowContactDetails(int)));
	
	

	//-----------------------------
	//ACTUAL HEADER:
	//-----------------------------
	//m_btnHideActual= new QPushButton(this);
	//m_btnHideActual= new StyledPushButton(":Icon_PulsePlus.mng","mng",true,this,"QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex(),"",0,0);
	//m_btnHideActual= new StyledPushButton(":Icon_PulsePlus.mng","mng",true,this,"","",0,0);
	
	
	//create buttons & labels:
	m_pTxt2		= new QLabel(this);
	m_pTxt2->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	m_pTxt2->setAlignment(Qt::AlignLeft);
	m_pTxt2->setText(tr("Active Subject"));

	//QSize buttonSize(17, 17);
	StyledPushButton *pBtnAdd2		=	new StyledPushButton(this,":DbIcon_Insert16.png",":DbIcon_Insert16.png","","",0,0);
	StyledPushButton *pBtnEdit		=	new StyledPushButton(this,":DbIcon_Edit16.png",":DbIcon_Edit16.png","","",0,0);
	StyledPushButton *pBtnDelete	=	new StyledPushButton(this,":DbIcon_Delete16.png",":DbIcon_Delete16.png","","",0,0);
	StyledPushButton *pBtnView		=	new StyledPushButton(this,":DbIcon_View16.png",":DbIcon_View16.png","","",0,0);
	StyledPushButton *pBtnPrintCont	=	new StyledPushButton(this,":Icon_Printer16.png",":Icon_Printer16.png","","",0,0);
	//StyledPushButton *pBtnPic		=	new StyledPushButton(this,":reddot.png",":reddot.png","","",0,0);

	//connect signals
	//connect(m_btnHideActual,SIGNAL(clicked()),this,SLOT(OnHideActual()));
	connect(pBtnAdd2,SIGNAL(clicked()),this,SLOT(OnList_InsertClick()));
	connect(pBtnEdit,SIGNAL(clicked()),this,SLOT(OnList_EditClick()));
	connect(pBtnDelete,SIGNAL(clicked()),this,SLOT(OnList_DeleteClick()));
	connect(pBtnView,SIGNAL(clicked()),this,SLOT(OnList_ViewClick()));
	connect(pBtnPrintCont,SIGNAL(clicked()),this,SLOT(OnDetail_PrintClick()));
	connect(ui.btnPict,SIGNAL(clicked()),this,SLOT(OnList_PicClick()));

	pBtnAdd2->setToolTip(tr("Add Contact"));
	pBtnEdit->setToolTip(tr("Edit Contact"));
	pBtnDelete->setToolTip(tr("Delete Contact"));
	pBtnView->setToolTip(tr("View Contact Detail"));
	pBtnPrintCont->setToolTip(tr("Print Contact Detail"));
	ui.btnPict->setToolTip(tr("Show/Hide Contact Picture"));
	ui.btnPict->setIcon(QIcon(":Icon_Picture.png"));


	//QImage *image = new QImage(":Icon_PulsePlus.mng");
	//size
	//m_btnHideActual	->setMaximumSize(buttonSize1);
	//m_btnHideActual	->setMinimumSize(buttonSize1);
	pBtnAdd2		->setMaximumSize(buttonSize);
	pBtnAdd2		->setMinimumSize(buttonSize);
	pBtnEdit		->setMaximumSize(buttonSize);
	pBtnEdit		->setMinimumSize(buttonSize);
	pBtnDelete		->setMaximumSize(buttonSize);
	pBtnDelete		->setMinimumSize(buttonSize);
	pBtnView		->setMaximumSize(buttonSize);
	pBtnView		->setMinimumSize(buttonSize);
	pBtnPrintCont	->setMaximumSize(buttonSize);
	pBtnPrintCont	->setMinimumSize(buttonSize);
	ui.btnPict		->setMaximumSize(buttonSize);
	ui.btnPict		->setMinimumSize(buttonSize);


	QHBoxLayout *buttonLayout1 = new QHBoxLayout;
	//buttonLayout1->addWidget(m_btnHideActual);
	//buttonLayout1->addSpacing(5);
	buttonLayout1->addWidget(m_pTxt2);
	buttonLayout1->addStretch(1);
	//buttonLayout1->addWidget(pBtnPic);
	buttonLayout1->addWidget(pBtnAdd2);
	buttonLayout1->addWidget(pBtnEdit);
	buttonLayout1->addWidget(pBtnDelete);
	buttonLayout1->addWidget(pBtnView);
	buttonLayout1->addWidget(pBtnPrintCont);
	buttonLayout1->setContentsMargins(0,0,0,0);
	buttonLayout1->setSpacing(2);
	

	ui.frameActual->setLayout(buttonLayout1);
	ui.frameDetailToolBar->setVisible(false);
	ui.stackedWidget->setVisible(false);
	ui.picture->setVisible(false);

	m_pTxtActual		= new QLabel(this);
	m_pTxtActual->setAlignment(Qt::AlignLeft);
	m_pTxtActual->setWordWrap(true);

	QHBoxLayout *txtlayout = new QHBoxLayout;
	txtlayout->addSpacing(3);
	txtlayout->addWidget(m_pTxtActual);
	txtlayout->addSpacing(3);
	txtlayout->setContentsMargins(0,0,0,0);
	txtlayout->setSpacing(0);
	ui.frameActualName->setLayout(txtlayout);

	//cntx menu:
	//ui.frameActual->setContextMenuPolicy(Qt::CustomContextMenu);
	ui.frameActualName->setContextMenuPolicy(Qt::CustomContextMenu);
	//connect(ui.frameActual,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnCustomContextMenu(const QPoint &)));
	connect(ui.frameActualName,SIGNAL(customContextMenuRequested ( const QPoint & )),this,SLOT(OnCustomContextMenu(const QPoint &)));
	m_pCntxReload = new QAction(tr("Reload Contact Data"), this);
	connect(m_pCntxReload, SIGNAL(triggered()), this, SLOT(ReloadContactData()));
	m_pCntxCopy = new QAction(tr("Copy Name to Clipboard"), this);
	connect(m_pCntxCopy, SIGNAL(triggered()), this, SLOT(CopyName2Clipboard()));



	//-----------------------------
	//DETAIL HEADER:
	//-----------------------------
	QSize buttonSize_detail(36, 36);
	StyledPushButton *pBtnQuickInfo		=	new StyledPushButton(this,":Icon_Person24_Mirror.png",":Icon_Person24_Mirror.png","","",0,0);
	StyledPushButton *pBtnCommGrid		=	new StyledPushButton(this,":Desktop32.png",":Desktop32.png","","",0,0);
	StyledPushButton *pBtnNMRXUsers		=	new StyledPushButton(this,":Icon_Contacts_24_Mirror.png",":Icon_Contacts_24_Mirror.png","","",0,0);
	StyledPushButton *pBtnNMRXProjects	=	new StyledPushButton(this,":Projects32.png",":Projects32.png","","",0,0);
	m_pBtnCalendar						=	new StyledPushButton(this,":Calendar36_Miror.png",":Calendar36_Miror.png","","",0,0);
	StyledPushButton *pBtnGroups		=	new StyledPushButton(this,":Groups36CM.png",":Groups36CM.png","","",0,0);

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
		m_pBtnCalendar->setVisible(false);

	//connect signals
	connect(pBtnQuickInfo,SIGNAL(clicked()),this,SLOT(OnDetail_QuickInfoClick()));
	connect(pBtnCommGrid,SIGNAL(clicked()),this,SLOT(OnDetail_CommGridClick()));
	connect(pBtnNMRXUsers,SIGNAL(clicked()),this,SLOT(OnDetail_NMRXUsersClick()));
	connect(pBtnNMRXProjects,SIGNAL(clicked()),this,SLOT(OnDetail_NMRXProjectsClick()));
	connect(m_pBtnCalendar,SIGNAL(clicked()),this,SLOT(OnDetail_CalendarClick()));
	connect(pBtnGroups,SIGNAL(clicked()),this,SLOT(OnDetail_GroupsClick()));

	pBtnQuickInfo->setToolTip(tr("Contact Quick Info"));
	pBtnCommGrid->setToolTip(tr("Contact Documents & Communication History"));
	pBtnNMRXUsers->setToolTip(tr("Assigned Users & Contacts"));
	pBtnNMRXProjects->setToolTip(tr("Assigned Projects"));
	m_pBtnCalendar->setToolTip(tr("Calendar"));
	pBtnGroups->setToolTip(tr("Groups"));

	//size
	pBtnQuickInfo		->setMaximumSize(buttonSize_detail);
	pBtnQuickInfo		->setMinimumSize(buttonSize_detail);
	pBtnCommGrid		->setMaximumSize(buttonSize_detail);
	pBtnCommGrid		->setMinimumSize(buttonSize_detail);
	pBtnNMRXUsers		->setMaximumSize(buttonSize_detail);
	pBtnNMRXUsers		->setMinimumSize(buttonSize_detail);
	pBtnNMRXProjects	->setMaximumSize(buttonSize_detail);
	pBtnNMRXProjects	->setMinimumSize(buttonSize_detail);
	m_pBtnCalendar		->setMaximumSize(buttonSize_detail);
	m_pBtnCalendar		->setMinimumSize(buttonSize_detail);
	pBtnGroups			->setMaximumSize(buttonSize_detail);
	pBtnGroups			->setMinimumSize(buttonSize_detail);


	//Tool m_ProgressBar.
	QHBoxLayout *buttonLayout_detail = new QHBoxLayout;
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnQuickInfo);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnCommGrid);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnNMRXUsers);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnNMRXProjects);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(m_pBtnCalendar);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->addWidget(pBtnGroups);
	buttonLayout_detail->addStretch(1);
	buttonLayout_detail->setContentsMargins(0,2,0,0);
	buttonLayout_detail->setSpacing(0);

	ui.frameDetailToolBar->setLayout(buttonLayout_detail);



	//-----------------------------
	//GROUP DATA :
	//-----------------------------
	DbRecordSet lstSetup;
	UniversalTableWidget::AddColumnToSetup(lstSetup,"_CALC_NAME_",tr("Group"),175,false,"",UniversalTableWidget::COL_TYPE_TEXT,"",1);
	UniversalTableWidget::AddColumnToSetup(lstSetup,"BGCN_CMCA_VALID_FROM",tr("From"),65,false);
	UniversalTableWidget::AddColumnToSetup(lstSetup,"BGCN_CMCA_VALID_TO",tr("To"),65,false);
	UniversalTableWidget::AddColumnToSetup(lstSetup,"BGTR_NAME",tr("Tree"),50,false);
	ui.frameGroupContacts->Initialize(ENTITY_BUS_GROUP_ITEMS,"BGCN_CONTACT_ID",DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT,lstSetup);
	ui.frameGroupContacts->HideTitle();
	dynamic_cast<QToolButton*>(ui.frameGroupContacts->GetButton(Selection_NM_SAPNE_FUI::BUTTON_SELECT))->setAutoRaise(false); 
	dynamic_cast<QToolButton*>(ui.frameGroupContacts->GetButton(Selection_NM_SAPNE_FUI::BUTTON_REMOVE))->setAutoRaise(false); 
	ui.frameGroupContacts->GetButton(Selection_NM_SAPNE_FUI::BUTTON_ADD)->setVisible(false); //add
	ui.frameGroupContacts->GetButton(Selection_NM_SAPNE_FUI::BUTTON_MODIFY)->setVisible(false); //modify 
	ui.frameGroupContacts->GetButton(Selection_NM_SAPNE_FUI::BUTTON_OPEN_NEW)->setVisible(false); //new win
	connect(ui.frameGroupContacts,SIGNAL(OnAdd(DbRecordSet &)),this,SLOT(OnGroupSAPNE_Add(DbRecordSet &)));
	connect(ui.frameGroupContacts,SIGNAL(OnRemove(DbRecordSet &)),this,SLOT(OnGroupSAPNE_Remove(DbRecordSet &)));
	connect(ui.btnChangeGroupDate,SIGNAL(clicked()),this,SLOT(OnGroupSAPNE_Edit()));

	//B.T. issue 2717:
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		ui.frameGroupContacts->SetEditMode(false);
	else
		ui.frameGroupContacts->SetEditMode(true);



	//-----------------------------
	//DETAIL :
	//-----------------------------
	ui.btnInternet->setIcon(QIcon(":Earth16.png"));
	ui.btnAddress->setIcon(QIcon(":Adress16.png"));
	ui.btnEmail->setIcon(QIcon(":Email16.png"));
	ui.btnPhone->setIcon(QIcon(":Phone16.png"));
	
	ui.btnInternet->setToolTip(tr("Copy Internet Address"));
	ui.btnAddress->setToolTip(tr("Copy Address"));
	ui.btnEmail->setToolTip(tr("Copy Email"));
	ui.btnPhone->setToolTip(tr("Copy Phone"));

	connect(ui.widgetCommGrid,SIGNAL(NeedNewData(int)),this,SLOT(SubMenuRequestedNewData(int)));
	
	//assigments:
	QList<int> lst;
	lst<<NMRXManager::PAIR_CONTACT_PERSON<<NMRXManager::PAIR_PERSON_CONTACT<<NMRXManager::PAIR_CONTACT_CONTACT;
	ui.widgetCommPerson->Initialize(tr("Assigned Users And Contacts"),BUS_CM_CONTACT,lst,BUS_CM_CONTACT,BUS_PERSON);


	//assigments:
	lst.clear();
	lst<<NMRXManager::PAIR_CONTACT_PROJECT<<NMRXManager::PAIR_PROJECT_CONTACT;
	ui.widgetCommProject->Initialize(tr("Assigned Projects"),BUS_CM_CONTACT,lst,BUS_PROJECT);


	//ALL BKG STYLESHEETS:
	setStyleSheet("QFrame[sidebar_fui=\"true\"] "+ThemeManager::GetSidebar_Bkg()+ " "+ThemeManager::GetGlobalWidgetStyle());
	ui.frameContactParent->setStyleSheet("QFrame#frameContactParent "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameActualParent->setStyleSheet("QFrame#frameActualParent "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameActualName->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg()+" "+"QLabel "+ThemeManager::GetSidebarActualName_Font());
	ui.frameQuick_Bkg->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg());


	ui.splitter->setStyleSheet("QSplitter::handle "+ThemeManager::GetSidebar_FooterHandle()); //{image: url(:Theme_Blue_Temple_Sidebar_Footer_Handle.png)}  ");
	//QString strStyle="QSplitter::handle "+ThemeManager::GetSidebar_CenterHandle();
	//ui.splitter->setStyleSheet(strStyle);
	ui.frameActualParent->setMaximumHeight(76);

	ui.frameContactAndFavs->ReloadData();

	ui.stackedWidget->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}


QFrame* SideBarFui_Contact::GetFrameCommToolBar()
{
	return ui.frameCommToolBar;
}

DropZone_MenuWidget* SideBarFui_Contact::GetDropZone_MenuWidget()
{
	return ui.widgetDropZone;
}

void SideBarFui_Contact::GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strEmail,int &nEmailReplyID,QString &strPhone,QString &strInternet)
{
	if (m_pFrameContacts==NULL) //anti-crash system (ACS)
		return;
	DbRecordSet lstData; //selected contacts: if one then skip first branch: issue 1920
	m_pFrameContacts->GetSelection(lstData);
	
	//issue 1819: use contact list selection for active contacts if serial mail:
	if (lstData.getRowCount()>1 && ui.frameContactAndFavs->IsSideBarVisible() && ui.frameContactAndFavs->IsContactFrameVisible() && nSubMenuType==CommunicationMenu_Base::SUBMENU_EMAIL)
	{
		DbRecordSet xlstContacts;
		xlstContacts.addColumn(QVariant::Int,"BCNT_ID");
		xlstContacts.merge(lstData);
		*lstContacts=xlstContacts;
		nEmailReplyID=-1;
		strEmail="";
		strPhone="";
		strInternet="";
		return;
	}

	if (m_nEntityRecordID>0 && m_RowContactData.getRowCount()>0)
	{
		DbRecordSet xlstContacts;
		xlstContacts.addColumn(QVariant::Int,"BCNT_ID");
		xlstContacts.addRow();
		xlstContacts.setData(0,0,m_nEntityRecordID);

		*lstContacts=xlstContacts;
		//find selected:
		nEmailReplyID=-1;
		strEmail=ui.cmbInfo_Email-> itemData(ui.cmbInfo_Email->currentIndex()).toString();
		strPhone=ui.cmbInfo_Phone-> itemData(ui.cmbInfo_Phone->currentIndex()).toString();
		strInternet=ui.cmbInfo_Internet-> itemData(ui.cmbInfo_Internet->currentIndex()).toString();
	}
}


void SideBarFui_Contact::OnHideList()
{
	bool bVisible=ui.frameContactAndFavs->IsSideBarVisible();
	if (bVisible)
	{
		ShowActualList(false);
	}
	else
	{
		ShowActualList(true);
	}


}
void SideBarFui_Contact::OnHideActual()
{
	bool bVisible=ui.stackedWidget->isVisible();

	if (bVisible)
	{
		ShowActualFrame(false);
	}
	else
	{
		ShowActualFrame(true);
	}


}
void SideBarFui_Contact::OnList_InsertClick()
{
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	g_objFuiManager.OpenQCWWindow(NULL);
	//g_objFuiManager.OpenSideBarModeFUI(MENU_CONTACTS,FuiBase::MODE_INSERT);
}
void SideBarFui_Contact::OnList_ClearClick()
{
	m_pFrameContacts->OnDeleteTable();
}

void SideBarFui_Contact::OnList_EditClick()
{
	if (m_nEntityRecordID>0)
	{
		Status err;
		if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_EDIT,m_nEntityRecordID))
			_CHK_ERR(err);

		Qt::KeyboardModifiers keys= QApplication::keyboardModifiers(); //issue 2466
		if(Qt::ControlModifier == (Qt::ControlModifier & keys))
		{
			g_objFuiManager.OpenQCWWindow(this,m_nEntityRecordID);
			return;
		}
		g_objFuiManager.OpenSideBarModeFUI(MENU_CONTACTS,FuiBase::MODE_EDIT,m_nEntityRecordID);
	}
}
void SideBarFui_Contact::OnList_DeleteClick()
{
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_DELETE))
		_CHK_ERR(err);

	m_pFrameContacts->OnDelete();
	//if (m_nEntityRecordID>0)
	//	g_objFuiManager.OpenSideBarModeFUI(MENU_CONTACTS,FuiBase::MODE_DELETE,m_nEntityRecordID);
}
void SideBarFui_Contact::OnList_ViewClick()
{
	if (m_nEntityRecordID>0)
	{
		Qt::KeyboardModifiers keys= QApplication::keyboardModifiers(); //issue 2466
		if(Qt::ControlModifier == (Qt::ControlModifier & keys))
		{
			g_objFuiManager.OpenQCWWindow(this,m_nEntityRecordID);
			return;
		}
		g_objFuiManager.OpenFUI(MENU_CONTACTS,true,false,FuiBase::MODE_READ,m_nEntityRecordID,false);
	}
		//g_objFuiManager.OpenSideBarModeFUI(MENU_CONTACTS,FuiBase::MODE_READ,m_nEntityRecordID);
}


// when Conatct or Project selection changes, set SAPNE to selected values
void SideBarFui_Contact::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{

	if (pSubject==&g_ChangeManager)
	{
		if(nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
		{
			OnThemeChanged();
			return;
		}

		if(nMsgCode==ChangeManager::GLOBAL_SIDEBAR_FUI_CONTACT_ACTIVE && nMsgDetail>0) //change selection to contact if valid id
		{
			if (m_nEntityRecordID==nMsgDetail) //issue 1487
				return;
			m_nEntityRecordID=nMsgDetail;
			m_pFrameContacts->SetCurrentEntityRecord(m_nEntityRecordID,true); //if from distant FUI
			ReloadContactData();
			return;
		}

		if(nMsgCode==ChangeManager::GLOBAL_REFRESH_GROUP_DATA_INSIDE_CONTACT_DETAILS)
		{
			//reload group data if contact is loaded:
			DbRecordSet lstGroups;
			Status err;

			_SERVER_CALL(BusContact->ReadContactGroup(err, m_nEntityRecordID, lstGroups));
			//lstGroups.Dump();

			ui.frameGroupContacts->Clear();
			ui.frameGroupContacts->SetList(lstGroups);
			return;
		}

		if(nMsgDetail!=ENTITY_BUS_CONTACT) return; //not our entity, exit

		//if EDIT mode, our FUI just inserted new id, assign back to FK link
		if(nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED || nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
		{
			DbRecordSet record=val.value<DbRecordSet>();
			if (record.getRowCount()>0 && record.getColumnIdx("BCNT_ID")>=0) //if valid format
			{
				if (m_nEntityRecordID==record.getDataRef(0,"BCNT_ID").toInt() && m_nEntityRecordID!=-1) //issue 1487: skip reloading comm data & other if same contact edited....
				{
					ReloadContactData(false);
					return;
				}
				m_nEntityRecordID=record.getDataRef(0,"BCNT_ID").toInt();
				m_pFrameContacts->SetCurrentEntityRecord(m_nEntityRecordID,true); //if from distant FUI
				ReloadContactData();
				return;
			}
		}

		if(nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED)
		{
			if(m_pFrameContacts->GetCalculatedName(m_nEntityRecordID).isEmpty())
			{
				m_nEntityRecordID=-1;
				ReloadContactData();
				//return;
			}
			ui.frameContactAndFavs->ReloadData(m_nEntityRecordID,true);
			return;
			
		}

		//reload selected:
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
				ReloadContactData();
				ui.frameContactAndFavs->ReloadData(m_nEntityRecordID,true);
		}

		


		return;

	}

	if(nMsgCode==SelectorSignals::SELECTOR_ON_DOUBLE_CLICK)
	{
		bool bListSoruce=bool(pSubject==dynamic_cast<ObsrPtrn_Subject*>(GetSelector()));
		if (!bListSoruce)
			OnFavoriteChanged(nMsgDetail);
		OnSelector_DoubleClick(nMsgDetail,bListSoruce);
		return;
	}
	if(nMsgCode==SelectorSignals::SELECTOR_ON_SHOW_DETAILS)
	{
		OnSelector_ShowContactDetails();
		return;
	}
	if(nMsgCode==SelectorSignals::SELECTOR_ON_SHOW_DETAILS_NEW_WINDOW)
	{
		OnSelector_ShowContactDetails(true);
		return;
	}

	/*//issue 1487
	if(nMsgCode==SelectorSignals::SELECTOR_DATA_CHANGED) //deleted
	{
		if(m_pFrameContacts->GetCalculatedName(m_nEntityRecordID).isEmpty())
		{
			m_nEntityRecordID=-1;
		}
		ReloadContactData();
		return;
	}
	*/
		
	if(nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED || nMsgCode==SelectorSignals::SELECTOR_SELECTION_CLICKED) //selection changed
	{
		if (m_nEntityRecordID==nMsgDetail) //issue 1487
			return;
		m_nEntityRecordID=nMsgDetail;
		ReloadContactData();
		return;
	}

	//issue 2263: on enter on find widget open actuala area!!!
	if(nMsgCode==SelectorSignals::SELECTOR_FIND_WIDGET_ENTER_PRESSED) //open actual area!!!!
	{
		ShowActualFrame(true);
		return;
	}
	
}

void SideBarFui_Contact::CopyName2Clipboard()
{
	QString strName=m_pTxtActual->text();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strName);
}

void SideBarFui_Contact::ReloadContactData(bool bReloadOtherData)
{
	QString strName=ui.frameContactAndFavs->GetCalculatedName(m_nEntityRecordID);
	m_RowContactData.clear(); //always clear

	if (m_nEntityRecordID>0)
	{
		if (!ui.frameActualParent->isVisible()) //if first time make visible, show actual:
		{
			ui.frameActualParent->setVisible(true);

		}


		DbRecordSet lstNmrx1,lstNmrx2,lstPicture;
		DbRecordSet lstNmrx1Filter=ui.widgetCommPerson->GetFilterForRead(m_nEntityRecordID);
		DbRecordSet lstNmrx2Filter=ui.widgetCommProject->GetFilterForRead(m_nEntityRecordID);

		Status err;
		_SERVER_CALL(BusContact->ReadContactDataSideBar(err,m_nEntityRecordID,m_RowContactData,lstNmrx1Filter,lstNmrx1,lstNmrx2Filter,lstNmrx2,m_bShowPicture,lstPicture))
		_CHK_ERR(err);

		if (bReloadOtherData)
		{
			if (m_bCommGridInit)
				ui.widgetCommGrid->SetData(m_nEntityRecordID);
		}
		ui.widgetCommPerson->Reload(strName,m_nEntityRecordID,true,&lstNmrx1);
		ui.widgetCommProject->Reload(strName,m_nEntityRecordID,true,&lstNmrx2);	


		//lstPicture.Dump();
		if (lstPicture.getRowCount()==0) //issue 1154, close picture if not there:
		{
			m_rowContactPicture.clear();
			ui.picture->RefreshDisplay();
			ui.picture->setVisible(false);
		}
		else
		{
			if(lstPicture.getDataRef(0,"BCNT_PICTURE").toByteArray().size()==0)
			{
				m_rowContactPicture.clear();
				ui.picture->RefreshDisplay();
				ui.picture->setVisible(false);
			}
			else
			{
				//load pic
				if(m_bShowPicture)
				{
					m_rowContactPicture.clear();
					m_rowContactPicture.merge(lstPicture);
					ui.picture->RefreshDisplay();
					ui.picture->setVisible(m_bShowPicture);
				}
			}

		}

		//MB on Skype: 
		/*
		a: Other User=0, (Contact=0) --> User & contact selection disabled
		b: Other User=1, Contact=0 --> User selection enabled, contact selection enabled for user contacts only (or disabled if too complicated)
		c: Other User=1, Contact=1 --> User & contact selection enabled
		*/
		bool bEnableCalendar=true;
		if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS))
		{
			bEnableCalendar=false;
		}
		if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS))
		{
			int nPersonID=ui.frameContactAndFavs->GetSelectionContacts()->GetPersonIDFromContactID(m_nEntityRecordID);
			if(nPersonID>0 && nPersonID != g_pClientManager->GetPersonID()) //if person_id then implement this rule if not exists 
				bEnableCalendar=false;
		}
		if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS) && g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS))
		{
			int nPersonID=ui.frameContactAndFavs->GetSelectionContacts()->GetPersonIDFromContactID(m_nEntityRecordID);
			if (nPersonID>0)
				bEnableCalendar=true;
		}
		if (m_pBtnCalendar)
			m_pBtnCalendar->setEnabled(bEnableCalendar);

		//reload group data if contact is loaded:
		DbRecordSet lstGroups;

		_SERVER_CALL(BusContact->ReadContactGroup(err, m_nEntityRecordID, lstGroups));
		_CHK_ERR(err);
		//lstGroups.Dump();

		ui.frameGroupContacts->Clear();
		ui.frameGroupContacts->SetList(lstGroups);
	}
	else
	{
		if (m_bCommGridInit)
			ui.widgetCommGrid->ClearCommGrid();  

		ui.widgetCommPerson->Invalidate();
		ui.widgetCommProject->Invalidate();
		if (m_pBtnCalendar)
			m_pBtnCalendar->setEnabled(false);

		ui.frameGroupContacts->Clear();
	}

	RefreshQuickInfo();
	RefreshActualName();


	//issue 1220:
	QTimer::singleShot(0,this,SLOT(OnScrollToCurrentItem()));
}


void SideBarFui_Contact::SetActualText(QString strText)
{
	if (!m_pTxtActual)return;
	m_pTxtActual->setText(strText);
}


void SideBarFui_Contact::OnDetail_QuickInfoClick()
{
	ui.stackedWidget->setCurrentIndex(0);
}
void SideBarFui_Contact::OnDetail_CommGridClick()
{
	if(!m_bCommGridInit)
	{
		ui.widgetCommGrid->Initialize(CommGridViewHelper::CONTACT_GRID, this);
		if (m_nEntityRecordID > 0)
			ui.widgetCommGrid->SetData(m_nEntityRecordID);
		m_bCommGridInit=true;
	}

	ui.stackedWidget->setCurrentIndex(1);

}
void SideBarFui_Contact::OnDetail_NMRXUsersClick()
{
	ui.stackedWidget->setCurrentIndex(2);
}
void SideBarFui_Contact::OnDetail_NMRXProjectsClick()
{
	ui.stackedWidget->setCurrentIndex(3);
}
void SideBarFui_Contact::OnDetail_GroupsClick()
{
	ui.stackedWidget->setCurrentIndex(4);
}



void SideBarFui_Contact::OnDetail_CalendarClick()
{
	//MB on Skype: 
	/*
	a: Other User=0, (Contact=0) --> User & contact selection disabled
	b: Other User=1, Contact=0 --> User selection enabled, contact selection enabled for user contacts only (or disabled if too complicated)
	c: Other User=1, Contact=1 --> User & contact selection enabled
	*/
	bool bEnableCalendar=true;
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS))
	{
		bEnableCalendar=false;
	}
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS))
	{
		int nPersonID=ui.frameContactAndFavs->GetSelectionContacts()->GetPersonIDFromContactID(m_nEntityRecordID);
		if(nPersonID>0 && nPersonID != g_pClientManager->GetPersonID()) //if person_id then implement this rule if not exists 
			bEnableCalendar=false;
	}
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS) && g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS))
	{
		int nPersonID=ui.frameContactAndFavs->GetSelectionContacts()->GetPersonIDFromContactID(m_nEntityRecordID);
		if (nPersonID>0)
			bEnableCalendar=true;
	}

	if (!bEnableCalendar)
		return;

	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(QDate::currentDate(),from, to);

	QString strName=ui.frameContactAndFavs->GetCalculatedName(m_nEntityRecordID);

	DbRecordSet lstContacts;
	lstContacts.addColumn(QVariant::Int,"BCNT_ID");
	lstContacts.addColumn(QVariant::String,"BCNT_NAME");
	lstContacts.addRow();
	lstContacts.setData(0,0,m_nEntityRecordID);
	lstContacts.setData(0,1,strName);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		//lstContacts.Dump();
		pFui->Initialize(from,to,NULL,NULL,&lstContacts);
	}
}


//from address, phones, emails, sets data
void SideBarFui_Contact::RefreshActualName()
{
	//NAME:
	QString strName;
	if (m_nEntityRecordID>=0)
		strName=ui.frameContactAndFavs->GetCalculatedName(m_nEntityRecordID);
	SetActualText(strName);
	m_pTxtActual->setToolTip(strName);
}

//from address, phones, emails, sets data
void SideBarFui_Contact::RefreshQuickInfo()
{

	ui.cmbInfo_Address->clear();
	ui.cmbInfo_Email->clear();
	ui.cmbInfo_Internet->clear();
	ui.cmbInfo_Phone->clear();
	ui.txtDesc->setHtml ("");
	ui.txtInfo_Address->setPlainText("");

	if (m_nEntityRecordID==-1)
		return;

	if (m_RowContactData.getRowCount()==0)
		return;

	DbRecordSet lstEmail,lstPhone,lstInternet, lstAddress;
	DbRecordSet lstAddressTypes;


	ui.txtDesc->setHtml (m_RowContactData.getDataRef(0,"BCNT_DESCRIPTION").toString());


	lstAddress=m_RowContactData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
	//lstAddressTypes=lstAddress.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
	lstEmail=m_RowContactData.getDataRef(0,"LST_EMAIL").value<DbRecordSet>();
	lstPhone=m_RowContactData.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
	lstInternet=m_RowContactData.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
	

	//refill sublists:
	ClientContactManager::PrepareListsWithTypeName(&lstEmail,&lstPhone,&lstInternet,&lstAddress,&lstAddressTypes);



	//-------------------------------------
	//		ADDRESS
	//-------------------------------------
	QString strDisplay;

		//"BCMT_TYPE_NAME<<' - '<<BCMA_STREET<<' / '<<BCMA_ZIP<<' '<<BCMA_CITY"
		//ui.cmbInfo_Address->clear();
		ui.cmbInfo_Address->blockSignals(true);

		int nSize=lstAddress.getRowCount();
		for(int i=0;i<nSize;++i)
		{

			if (!lstAddress.getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
				strDisplay=lstAddress.getDataRef(i,"BCMT_TYPE_NAME").toString()+" - ";
			else
				strDisplay="";

			if (!lstAddress.getDataRef(i,"BCMA_STREET").toString().isEmpty())
				strDisplay+=lstAddress.getDataRef(i,"BCMA_STREET").toString()+" / "+lstAddress.getDataRef(i,"BCMA_ZIP").toString()+" "+lstAddress.getDataRef(i,"BCMA_CITY").toString();
			else
				strDisplay+=lstAddress.getDataRef(i,"BCMA_ZIP").toString()+" "+lstAddress.getDataRef(i,"BCMA_CITY").toString();

			ui.cmbInfo_Address->addItem(strDisplay,lstAddress.getDataRef(i,"BCMA_FORMATEDADDRESS")); //add display + proper email without type
		}

		int nRow=lstAddress.find(lstAddress.getColumnIdx("BCMA_IS_DEFAULT"),1,true);
		if (nRow!=-1)
			ui.cmbInfo_Address->setCurrentIndex(nRow);

		on_cmbInfo_Address_currentIndexChanged(nRow);

		//ui.labelInfoAddress->setText(QVariant(nSize).toString()+" "+tr("Address(es):"));
		ui.cmbInfo_Address->blockSignals(false);





	//-------------------------------------
	//		EMAIL
	//-------------------------------------
		//ui.cmbInfo_Email->clear();

		nSize=lstEmail.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			if (!lstEmail.getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
				strDisplay=lstEmail.getDataRef(i,"BCMT_TYPE_NAME").toString()+" - "+lstEmail.getDataRef(i,"BCME_ADDRESS").toString();
			else
				strDisplay=lstEmail.getDataRef(i,"BCME_ADDRESS").toString();

			ui.cmbInfo_Email->addItem(strDisplay,lstEmail.getDataRef(i,"BCME_ADDRESS")); //add display + proper email without type
		}

		nRow=lstEmail.find(lstEmail.getColumnIdx("BCME_IS_DEFAULT"),1,true);
		if (nRow!=-1)
			ui.cmbInfo_Email->setCurrentIndex(nRow);

		//ui.labelInfoEmail->setText(QVariant(nSize).toString()+" "+tr("Email Address(es):"));


	//-------------------------------------
	//		INTERNET
	//-------------------------------------

		//ui.cmbInfo_Internet->clear();

		nSize=lstInternet.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			if (!lstInternet.getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
				strDisplay=lstInternet.getDataRef(i,"BCMT_TYPE_NAME").toString()+" - "+lstInternet.getDataRef(i,"BCMI_ADDRESS").toString();
			else
				strDisplay=lstInternet.getDataRef(i,"BCMI_ADDRESS").toString();

			ui.cmbInfo_Internet->addItem(strDisplay,lstInternet.getDataRef(i,"BCMI_ADDRESS")); //add display + proper email without type
		}

		nRow=lstInternet.find(lstInternet.getColumnIdx("BCMI_IS_DEFAULT"),1,true);
		if (nRow!=-1)
			ui.cmbInfo_Internet->setCurrentIndex(nRow);

		//ui.labelInfoInternet->setText(QVariant(nSize).toString()+" "+tr("Internet Address(es):"));


	//-------------------------------------
	//		PHONE
	//-------------------------------------
		//get default address:
		//ui.cmbInfo_Phone->clear();
		nSize=lstPhone.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			if (!lstPhone.getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
				strDisplay=lstPhone.getDataRef(i,"BCMT_TYPE_NAME").toString()+" - "+lstPhone.getDataRef(i,"BCMP_FULLNUMBER").toString();
			else
				strDisplay=lstPhone.getDataRef(i,"BCMP_FULLNUMBER").toString();

			ui.cmbInfo_Phone->addItem(strDisplay,lstPhone.getDataRef(i,"BCMP_SEARCH"));
		}

		nRow=lstPhone.find(lstPhone.getColumnIdx("BCMP_IS_DEFAULT"),1,true);
		if (nRow!=-1)
			ui.cmbInfo_Phone->setCurrentIndex(nRow);

		//ui.labelInfoPhone->setText(QVariant(nSize).toString()+" "+tr("Phone(s):"));



}


void SideBarFui_Contact::on_cmbInfo_Address_currentIndexChanged(int nIndex)
{
	if (nIndex>=0)
	{
		QString strAddress= ui.cmbInfo_Address->itemData(nIndex).toString();
		ui.txtInfo_Address->setPlainText(strAddress);
	}
	else
	{
		ui.txtInfo_Address->setPlainText("");
	}

}

void SideBarFui_Contact::OnList_PicClick()
{
	m_bShowPicture=!m_bShowPicture;
	/*
	if (m_bShowPicture)
	{
		ui.cmbInfo_Phone->setMaximumWidth(width()-150);
		ui.cmbInfo_Email->setMaximumWidth(150);
		ui.cmbInfo_Internet->setMaximumWidth(150);
		ui.cmbInfo_Address->setMaximumWidth(150);
	}
	else
	{
		ui.cmbInfo_Phone->setMaximumWidth(275);
		ui.cmbInfo_Email->setMaximumWidth(275);
		ui.cmbInfo_Internet->setMaximumWidth(275);
		ui.cmbInfo_Address->setMaximumWidth(275);
	}
	*/
	ui.picture->setVisible(m_bShowPicture);
}




void SideBarFui_Contact::OnCustomContextMenu(const QPoint &pos)
{
	QMenu CnxtMenu(this);
	CnxtMenu.addAction(m_pCntxReload);
	CnxtMenu.addAction(m_pCntxCopy);
	CnxtMenu.exec(ui.frameActualName->mapToGlobal(pos));
}

QFrame* SideBarFui_Contact::GetFrameCommParent()
{
	return ui.frameCommParent;
}

QLabel* SideBarFui_Contact::GetFrameCommLabel()
{
	return ui.labelComm;
}




void SideBarFui_Contact::OnSelector_DoubleClick(int nRecordID, bool bSourceList)
{
	bool bAlt = false;
	Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();
	if(Qt::ShiftModifier == (Qt::ShiftModifier & keys))
		bAlt = true;

	DbRecordSet lstRecords;
	if (!bSourceList)
	{
		lstRecords.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
		lstRecords.addRow();
		lstRecords.setData(0,"BCNT_ID",nRecordID);
	}
	else
	{
		Selection_Contacts *pGrid = (Selection_Contacts *)GetSelector();
		pGrid->GetSelection(lstRecords);
	}


	//lstRecords.Dump();

	int nSize = lstRecords.getRowCount();
	if(nSize > 5 && bAlt){
		QString strMsg = QString(tr("Do you really want to open %1 cards?")).arg(nSize);
		if(QMessageBox::Cancel == QMessageBox::question(NULL, "", strMsg, QMessageBox::Ok|QMessageBox::Cancel))
			return;
	}

	if (m_nEntityRecordID>0)
	{
		if(bAlt)
		{
			//calculate initial position for the new window
			QPoint pt;

			//open all selected lines as avatars
			for(int i=0; i<nSize; i++)
			{
				int nRecID = lstRecords.getDataRef(i, "BCNT_ID").toInt();
				int nGID = g_objFuiManager.OpenFUI(MENU_CONTACTS,true,false,FuiBase::MODE_READ, nRecID, true, true /*open as avatar*/);
				//minimize immediately to make it an avatar
				QWidget *pFui = g_objFuiManager.GetFUIWidget(nGID);
				if(pFui)
				{
					((FUI_Contacts *)pFui)->m_bHideOnMinimize = false; //we will hide ti below
					pFui->showMinimized();
					QTimer::singleShot(0,pFui,SLOT(hide()));//B.T. delay hide
					if(0 == i)
						pt = ((FUI_Contacts *)pFui)->m_pWndAvatar->geometry().topLeft();
					else{
						pt.setX(pt.x() + 5);
						pt.setY(pt.y() + 5);

						((FUI_Contacts *)pFui)->m_pWndAvatar->move(pt);
					}
				}
			}
		}
		else
		{
			if(Qt::ControlModifier == (Qt::ControlModifier & keys))//issue 2466
			{
				g_objFuiManager.OpenQCWWindow(this,m_nEntityRecordID);
				return;
			}

			int nGID = g_objFuiManager.OpenFUI(MENU_CONTACTS,true,false,FuiBase::MODE_READ,m_nEntityRecordID, (bAlt)? true : false);

			if(bAlt){
				//minimize immediately to make it an avatar
				QWidget *pFui = g_objFuiManager.GetFUIWidget(nGID);
				if(pFui){
					((FUI_Contacts *)pFui)->m_bHideOnMinimize = false; //we will hide ti below	
					pFui->showMinimized();
					QTimer::singleShot(0,pFui,SLOT(hide()));//B.T. delay hide
				}
			}



		}
	}
}



void SideBarFui_Contact::OnSelector_ShowContactDetails(bool bNewWindow)
{
	int nNewFUI;
	if (bNewWindow)
		nNewFUI=g_objFuiManager.OpenFUI(MENU_CONTACTS,true,false,FuiBase::MODE_READ,m_nEntityRecordID,true);
	else
		nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_CONTACTS,FuiBase::MODE_READ,m_nEntityRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_Contacts* pFuiW=dynamic_cast<FUI_Contacts*>(pFUI);
	if (pFuiW)
	{
		pFuiW->SetCurrentTab(1);
		pFuiW->show();
	}
}



void SideBarFui_Contact::on_btnInternet_clicked()
{
	QString strInternet=ui.cmbInfo_Internet-> itemData(ui.cmbInfo_Internet->currentIndex()).toString();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strInternet);
}

void SideBarFui_Contact::on_btnAddress_clicked()
{
	QString strAddress=ui.txtInfo_Address->toPlainText();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strAddress);
}

void SideBarFui_Contact::on_btnEmail_clicked()
{
	QString strEmail=ui.cmbInfo_Email-> itemData(ui.cmbInfo_Email->currentIndex()).toString();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strEmail);
}
void SideBarFui_Contact::on_btnPhone_clicked()
{
	QString strPhone=ui.cmbInfo_Phone-> itemData(ui.cmbInfo_Phone->currentIndex()).toString();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strPhone);
}



void SideBarFui_Contact::OnDropZone_NewDataDropped(int nDropType, DbRecordSet lstDroppedData)
{
	//if current fui is selected to curr id, then pass to him, else open new FUI
	FuiBase* pActive=g_objFuiManager.GetActiveFUI();
	int nNewFUI=g_objFuiManager.OpenSideBarModeFUI(MENU_CONTACTS,FuiBase::MODE_EMPTY,m_nEntityRecordID,true);
	if (g_objFuiManager.GetFUI(nNewFUI)==pActive) //do not send signal twice if it already active->fui_cont is linked to drop zone
		return;

	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_Contacts* pFuiW=dynamic_cast<FUI_Contacts*>(pFUI);
	if (pFuiW)
	{
		if (pFuiW->GetEntityRecordID()!=m_nEntityRecordID)
		{
			pFuiW->on_selectionChange(m_nEntityRecordID);
		}
		g_objFuiManager.SetActiveFUI(pFuiW);
		pFuiW->OnCEMenu_NewDataDropped(nDropType,lstDroppedData);
		pFuiW->show();
	}
}



void SideBarFui_Contact::OnScrollToCurrentItem()
{
	m_pFrameContacts->GetContactTableWidget()->scrollToItem(m_pFrameContacts->GetContactTableWidget()->currentItem());
}

void SideBarFui_Contact::ShowActualFrameDetails(int nTabIdx)
{
	ShowActualFrame(true);
	ui.stackedWidget->setCurrentIndex(nTabIdx);
}

void SideBarFui_Contact::OpenCurrentContactInNewWindow()
{
	OnList_ViewClick();
}

void SideBarFui_Contact::ShowActualFrame(bool bShow,bool bSkipAutoCollapseOthers,bool bSkipInsertSpacer)
{
	bool bVisible=ui.stackedWidget->isVisible();
	if (!bVisible && !bShow || bVisible && bShow)
		return;

	if (!bShow)
	{
		//m_btnHideActual->setIcon(":Icon_PulsePlus.mng");
		ui.frameDetailToolBar->setVisible(false);
		ui.stackedWidget->setVisible(false);
		if (!ui.frameContactAndFavs->IsSideBarVisible() && !bSkipInsertSpacer)
		{
			if (!m_Spacer)
			{
				dynamic_cast<QVBoxLayout*>(layout())->insertStretch (1,1); 
				m_Spacer=layout()->itemAt(1);
			}
			else
			{
				m_Spacer->spacerItem()->changeSize(this->width(),5,QSizePolicy::Preferred,QSizePolicy::Expanding);
			}
		}
		ui.frameActualParent->setMaximumHeight(76);
		//if (!bSkipAutoCollapseOthers)
		//emit ChapterCollapsed();
	}
	else
	{
		if (!m_bDetailFrameInit) //issue 1831: first init comm grid!!!
		{
			int nFuiToOpen=g_pSettings->GetPersonSetting(STARTING_MODULE).toInt();
			if (nFuiToOpen==3)
				OnDetail_CommGridClick(); //set to index=1 comm grid
			else
				ui.stackedWidget->setCurrentIndex(0);
			m_bDetailFrameInit=true;

			//ui.stackedWidget->setCurrentIndex(0);
		}

		ui.frameActualParent->setMaximumHeight(16777215);
		if (m_Spacer)
		{
			m_Spacer->spacerItem()->changeSize(this->width(),5,QSizePolicy::Preferred,QSizePolicy::Fixed);
		}

		//issue 1820
		//if (!IsEnoughSpaceForActualArea()) //issue 1820, only if there is NO enough space hide upper area (contact list)
		//	ShowActualList(false,true,true); //hide list if visible, but do not insert spacer!

		//m_btnHideActual->setIcon(":Icon_PulseMinus.mng");
		ui.frameDetailToolBar->setVisible(true);

		ui.stackedWidget->setVisible(true);
		//ui.frameActualParent->updateGeometry();

		
	}
	
	//this->updateGeometry();

	if (!bSkipAutoCollapseOthers)
	{
		bool b1=ui.stackedWidget->isVisible();
		bool b2=ui.frameContactAndFavs->IsSideBarVisible();

		emit ChapterCollapsed(m_nFuiTypeID,1, (!b1 && !b2)); //notify parent to resize
	}

}

void SideBarFui_Contact::ShowActualList(bool bShow,bool bSkipAutoCollapseOthers,bool bSkipInsertSpacer)
{
	bool bVisible=ui.frameContactAndFavs->IsSideBarVisible();
	if (!bVisible && !bShow || bVisible && bShow)
		return;

	if (!bShow)
	{
		m_btnHideList->setIcon(":Icon_Plus12.png");
		ui.frameContactAndFavs->SetSideBarVisible(false);
		if (!ui.stackedWidget->isVisible() && !bSkipInsertSpacer)
		{
			if (!m_Spacer)
			{
				dynamic_cast<QVBoxLayout*>(layout())->insertStretch (1,1); 
				m_Spacer=layout()->itemAt(1);
			}
			else
			{
				m_Spacer->spacerItem()->changeSize(this->width(),5,QSizePolicy::Preferred,QSizePolicy::Expanding);
			}
		}
		ui.frameContactParent->setMaximumHeight(66);
		//if (!bSkipAutoCollapseOthers)
		
	}
	else
	{
		ui.frameContactParent->setMaximumHeight(16777215);
		if (m_Spacer)
		{
			m_Spacer->spacerItem()->changeSize(this->width(),5,QSizePolicy::Preferred,QSizePolicy::Fixed);
		}
		m_btnHideList->setIcon(":Icon_Minus12.png");
		ui.frameContactAndFavs->SetSideBarVisible(true);

	}

	//this->updateGeometry();

	if (!bSkipAutoCollapseOthers)
	{
		bool b1=ui.stackedWidget->isVisible();
		bool b2=ui.frameContactAndFavs->IsSideBarVisible();
		emit ChapterCollapsed(m_nFuiTypeID,0, (!b1 && !b2) ); //notify parent to resize
	}

}


//issue 1273: 1st DZ, 2nd ACL, 3rd AC
bool SideBarFui_Contact::HideVisibleFramesIfNeeded(int nFrameOpened)
{
	//if coordinates are > window height then do some hiding:
	if (GUI_Helper::IsWidgetOutOfScreen(this->topLevelWidget()))
	{
		//DZ:
		if (nFrameOpened!=2)
			if (!ui.widgetDropZone->IsHidden())
			{
				ui.widgetDropZone->SetHidden(true,true);
				return true;
			}
		//ACL:
		if (nFrameOpened!=0)
			if (ui.frameContactAndFavs->IsSideBarVisible())
			{
				ShowActualList(false,true);
				return true;
			}
		//AC:
		if (nFrameOpened!=1)
			if (ui.stackedWidget->isVisible())
			{
				ShowActualFrame(false,true);
				return true;
			}

	}

	return false;
}


void SideBarFui_Contact::OnThemeChanged()
{
	SideBarFui_Base::OnThemeChanged();
	if(m_btnHideList)m_btnHideList->setStyleSheet("QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex());
	if(m_pTxt1)m_pTxt1->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	//if(m_btnHideActual)m_btnHideActual->setStyleSheet("QWidget "+ThemeManager::GetCEMenuButtonStyle_Ex());
	if(m_pTxt2)m_pTxt2->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	setStyleSheet("QFrame[sidebar_fui=\"true\"] "+ThemeManager::GetSidebar_Bkg());
	ui.frameContactParent->setStyleSheet("QFrame#frameContactParent "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameActualParent->setStyleSheet("QFrame#frameActualParent "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameActualName->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg()+" "+"QLabel "+ThemeManager::GetSidebarActualName_Font());
	ui.frameQuick_Bkg->setStyleSheet("QFrame "+ThemeManager::GetSidebar_Bkg());

}



void SideBarFui_Contact::OnList_PrintClick()
{
	DbRecordSet lstContacts;
	m_pFrameContacts->GetSelection(lstContacts);
	if (lstContacts.getRowCount()>0)
	{
		// 7
		QList<DbRecordSet> lstData;
		lstData.append(lstContacts);
		ReportManager Rpt;
		Rpt.PrintReport(ReportRegistration::REPORT_CONTACT_LIST,&lstData);

	}

}

void SideBarFui_Contact::OnDetail_PrintClick()
{
	if (m_nEntityRecordID>0)
	{
		QList<DbRecordSet> lstData;
		if (ui.stackedWidget->currentIndex()==1)
		{
			//1
			DbRecordSet recFilter;
			ui.widgetCommGrid->GetFilter(recFilter);
			lstData.append(m_RowContactData);
			lstData.append(recFilter);
			ReportManager Rpt;
			Rpt.PrintReport(ReportRegistration::REPORT_CONTACT_COMMUNICATION_DETAILS,&lstData);
		}
		else
		{//2
			lstData.append(m_RowContactData);
			ReportManager Rpt;
			Rpt.PrintReport(ReportRegistration::REPORT_CONTACT_DETAILS,&lstData);

		}

	}
}

void SideBarFui_Contact::OnSetFindFocusDelayed()
{
	m_pFrameContacts->SetFocusOnFind();
}

void SideBarFui_Contact::OnFavoriteChanged(int nMsgDetail)
{
	if (m_nEntityRecordID==nMsgDetail) //issue 1487
		return;
	m_nEntityRecordID=nMsgDetail;
	ReloadContactData();


}



void SideBarFui_Contact::FlipSelectorToFavoriteTabOrList()
{
	if (ui.frameContactAndFavs->GetCurrentTab()==1)
		ui.frameContactAndFavs->SetCurrentTab(0);
	else
		ui.frameContactAndFavs->SetCurrentTab(1);
}




void SideBarFui_Contact::OnGroupSAPNE_Add(DbRecordSet &lstData)
{
	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	if (lstData.getRowCount()==0)return;
	DbRecordSet lstGroupContent;
	lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
	int nGroupId=lstData.getDataRef(0,"BGIT_ID").toInt();
	int nTreeId=lstData.getDataRef(0,"BGIT_TREE_ID").toInt();
	lstGroupContent.addRow();
	lstGroupContent.setColValue(lstGroupContent.getColumnIdx("BGCN_CONTACT_ID"),m_nEntityRecordID);
	lstGroupContent.setColValue(lstGroupContent.getColumnIdx("BGCN_ITEM_ID"),nGroupId);



	//add contact to group, if not already
	//Status err;
	_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nGroupId,DataHelper::OP_ADD,lstGroupContent))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			lstData.clear();
			return;
		}

		lstGroupContent.find("BGCN_CONTACT_ID",m_nEntityRecordID);
		lstGroupContent.deleteUnSelectedRows();
		lstGroupContent.find("BGCN_ITEM_ID",nGroupId);
		lstGroupContent.deleteUnSelectedRows();

		if (lstGroupContent.getRowCount()==0)
		{
			QMessageBox::critical(NULL,tr("Error"),tr("Error while fetching saved data. Please refresh contact data!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			lstData.clear();
			return;


		}

		//copy new ID:
		lstData.setData(0,"BGCN_ID",lstGroupContent.getDataRef(0,"BGCN_ID"));
		lstData.setData(0,"BGCN_ITEM_ID",nGroupId);
		lstData.setData(0,"BGCN_CONTACT_ID",m_nEntityRecordID);

		//fill tree name:
		DbRecordSet *lstTrees=g_ClientCache.GetCache(ENTITY_BUS_CONTACT_TREES);
		if (lstTrees)
		{
			int nRow=lstTrees->find("BGTR_ID",nTreeId,true);
			if (nRow!=-1)
			{
				lstData.setData(0,"BGTR_NAME",lstTrees->getDataRef(nRow,"BGTR_NAME"));
			}
		}





}

void SideBarFui_Contact::OnGroupSAPNE_Remove(DbRecordSet &lstData)
{
	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	if (lstData.getRowCount()==0)return;
	DbRecordSet lstGroupContent;
	lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
	lstGroupContent.merge(lstData);


	DbRecordSet lstGroups;
	lstGroups.addColumn(QVariant::Int,"BGCN_ITEM_ID");
	lstGroups.merge(lstData);
	lstGroups.setColumn(0,QVariant::Int,"BGIT_ID");   //change name to proper one
	lstGroups.removeDuplicates(0);


	//add contact to group, if not already
	//Status err;
	_SERVER_CALL(BusGroupTree->RemoveGroupContentItem(err,ENTITY_BUS_CONTACT,lstGroupContent,lstGroups))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			lstData.clear();
			return;
		}

}

void SideBarFui_Contact::OnGroupSAPNE_Edit()
{
	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	//enter dialog:
	Dlg_EnterPeriod Dlg;

	DbRecordSet lstData;
	ui.frameGroupContacts->GetList(lstData);

	int nRow=lstData.getSelectedRow();
	if (nRow==-1) return;

	Dlg.setPeriod(lstData.getDataRef(nRow,"BGCN_CMCA_VALID_FROM").toDate(),lstData.getDataRef(nRow,"BGCN_CMCA_VALID_TO").toDate());
	if(Dlg.exec())
	{
		QDate from,to;
		Dlg.getPeriod(from,to);
		lstData.setData(nRow,"BGCN_CMCA_VALID_FROM",from);
		lstData.setData(nRow,"BGCN_CMCA_VALID_TO",to);

		DbRecordSet row=lstData.getRow(nRow);

		DbRecordSet lstGroupContent;
		lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
		lstGroupContent.merge(row);

		//Status err;
		_SERVER_CALL(BusGroupTree->WriteGroupContentData(err,ENTITY_BUS_CONTACT,lstGroupContent))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			ui.frameGroupContacts->SetList(lstData);
	}
}