#ifndef TABLE_CUSTOMFIELDS_H
#define TABLE_CUSTOMFIELDS_H

#include "gui_core/gui_core/universaltablewidgetex.h"
#include <QCheckBox>
#include <QRadioButton>
#include <QComboBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include "gui_core/gui_core/datetimeeditex.h"

class Table_CustomFields : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_CustomFields(QWidget *parent=0);
	~Table_CustomFields();

	void Initialize(DbRecordSet *pData);
	void ResetRecordIDs(); //for copy purpose:
	void SetEditMode(bool bEdit=true);

protected:
	void Data2CustomWidget(int nRow, int nCol);
	void CustomWidget2Data(int nRow, int nCol);

private slots:
	void On_CustomWidgetChanged(int);

private:
	
};



class Table_CustomFieldWidget : public QWidget
{
	Q_OBJECT

public:

	Table_CustomFieldWidget(Table_CustomFields *parent,int nRow,DbRecordSet &recCustomField);
	~Table_CustomFieldWidget();

	DbRecordSet GetDataFromWidget();
	void		SetData(DbRecordSet &recCustomField);  //call this first then setdata2widget after for edit mode...
	void		SetDataToWidget(); 
	void		SetButtonLongText(bool bEdit);

	int m_nDataType;
	int m_nSelType;

signals:
	void DataChanged(int nRow);

private slots:
	void OnStateChangedDate(){if (bEnableSignalDataChange)emit DataChanged(m_nRowInTable);};
	void OnStateChangedText(const QString &){if (bEnableSignalDataChange)emit DataChanged(m_nRowInTable);};
	void OnStateChanged(int){if (bEnableSignalDataChange)emit DataChanged(m_nRowInTable);};
	void OnStateChangedRadio(bool){if (bEnableSignalDataChange)emit DataChanged(m_nRowInTable);};
	void OnLongTextEdit();
	void OnFloatFldStateChangedText(const QString &);


private:
	int						m_nRowInTable;

	QTextEdit				*m_pFldLongText;
	QCheckBox				*m_pFldCheckBox;
	QComboBox				*m_pFldSelection;
	QList<QRadioButton*>	m_lstFldRadioButtons;
	QLineEdit				*m_pFldLineEdit;
	DateTimeEditEx			*m_pFldDateTime;
	QPushButton				*m_pbtnEditLongText;
	bool					bTableInEditMode;

	DbRecordSet m_recCustomField;
	DbRecordSet m_recData;
	DbRecordSet m_lstSelection;
	bool bEnableSignalDataChange;

	
	void CreateEmptyDataIfNeeded(); 

	void CreateBoolField();
	void CreateCharField();
	void CreateTextField();
	void CreateIntField();
	void CreateFloatField();
	void CreateDateField();
	void CreateDatetimeField();



};





#endif // TABLE_CUSTOMFIELDS_H
