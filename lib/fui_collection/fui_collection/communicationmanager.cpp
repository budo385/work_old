#include "communicationmanager.h"

#ifndef min
#define min(a,b) ((a)<(b)?(a):(b))
#endif

#include "bus_core/bus_core/emailhelpercore.h"
#include "bus_client/bus_client/documenthelper.h"
#include "fui_collection/fui_collection/fui_importemail.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/entity_id_collection.h"
#include "common/common/datahelper.h"
#include "fui_importcontact.h"
#include "communicationactions.h"
#include "os_specific/os_specific/mapimanager.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/logindlg.h"
#include "trans/trans/httpreadersync.h"
#include "fui_collection/fui_collection/fui_dm_documents.h"
#include <QFileDialog>

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "bus_client/bus_client/clientdownloadmanager.h"
extern ClientDownloadManager *g_DownloadManager;
extern QString g_strLastDir_FileOpen;

CommunicationManager::CommunicationManager()
:m_bTimerConnected(false)
{

	RedefineSentMailList();
	g_ChangeManager.registerObserver(this);
}

CommunicationManager::~CommunicationManager()
{
	g_ChangeManager.unregisterObserver(this);
}


void CommunicationManager::Initialize()
{
	//connect signals
	//connect(g_pClientManager, SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	connect(DocumentHelper::DWatcher(),SIGNAL(fileChanged(const QString &)),this,SLOT(OnDocumentChanged(const QString &)));
	connect(DocumentHelper::DWatcher(),SIGNAL(fileChanged(const QString &)),this,SLOT(OnDocumentChanged(const QString &)));
	connect(g_DownloadManager,SIGNAL(CheckInOutCompleted(Status,int,QString,DbRecordSet,DbRecordSet)),this,SLOT(OnCheckInOutCompleted(Status,int,QString,DbRecordSet,DbRecordSet)));

}

void CommunicationManager::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (nMsgCode==ChangeManager::GLOBAL_DOCUMENT_OPEN_FUI) //detail=rec id, val = fuisbase mode
	{
		g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,val.toInt(), nMsgDetail);
		return;
	}
	if (nMsgCode==ChangeManager::GLOBAL_DOCUMENT_OPEN_FUI_SIMPLE_MODE) 
	{
		int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,val.toInt(), nMsgDetail,true);
		FUI_DM_Documents* pFui=dynamic_cast<FUI_DM_Documents*>(g_objFuiManager.GetFUIWidget(nNewFUI));
		if (pFui)
		{
			pFui->SetSimpleMode();
			pFui->show();  
		}
		return;
	}
	if (nMsgCode==ChangeManager::GLOBAL_DOCUMENT_CHECK_OUT_AND_OPEN) 
	{
		DbRecordSet data=val.value<DbRecordSet>();
		if (data.getRowCount()!=1)
			return;
		m_lstDocsForCheckOutAndOpen[nMsgDetail]=data;
		g_DownloadManager->ScheduleCheckOutDocument(nMsgDetail,data.getDataRef(0,"strDocPath").toString(),data.getDataRef(0,"bReadOnly").toBool(),false,data.getDataRef(0,"bReadOnly").toBool(),false,data.getDataRef(0,"strDefaultDirPath").toString(),data.getDataRef(0,"nRevisionID").toInt());
		return;
	}
	if (nMsgCode==ChangeManager::GLOBAL_DOCUMENT_OPEN_FUI_SIMPLE_MODE_AFTER_UPLOAD) 
	{
		m_lstDocumentsForOpenAfterUpload[nMsgDetail]=val.toInt();
		return;
	}
	



}

void CommunicationManager::OnDocumentChanged(const QString & path)
{
	DocumentHelper::DWatcher_SetFileChanged(path);
	//ReLoadCheckedOutDocs();
}


//on communication fatal error, stop timer's & clear intermediate data
void CommunicationManager::ClearData()
{
	m_Timer.stop();
	m_lstSentEmails.clear();
	DocumentHelper::ClearData();
	m_lstDocsForCheckOutAndOpen.clear();
	m_lstDocumentsForOpenAfterUpload.clear();
}

//activate document & email gbc
void CommunicationManager::EndUserSession()
{
	ProcessEmailContentFromOutlook(true);
	DocumentHelper::DocumentGarbageCollector();
}


//set task is done, lstTaskIDs on first col contains tasks ID
void CommunicationManager::SetTaskDone(DbRecordSet &lstTaskIDs)
{
	Status err;
	_SERVER_CALL(BusCommunication->SetTaskIsDone(err,lstTaskIDs))
	_CHK_ERR(err);
}


void CommunicationManager::OpenMailInExternalClient(int nEmailID,QByteArray arData, QString strExternApp)
{
	//if fails from outlook, open from DB:
	if (!OpenMailInOutlook(arData,strExternApp,true))
		if(!OpenMailInOutlookFromDatabaseRecord(nEmailID))
			g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_READ, nEmailID);
}

void CommunicationManager::BatchReadAllCEMenuData()
{
	DbRecordSet lstDocsTemplates,lstDocsCheck,lstDocsApps,lstEmailTemplates;

	//SERVER:
	Status err;
	_SERVER_CALL(BusCommunication->ReadCEMenuData(err,g_pClientManager->GetPersonID(),lstDocsTemplates,lstDocsApps,lstDocsCheck,lstEmailTemplates))
	_CHK_ERR(err);

	//if all ok, set into cache:
	g_ClientCache.SetCache(ENTITY_BUS_DM_TEMPLATES,lstDocsTemplates);
	g_ClientCache.SetCache(ENTITY_BUS_DM_APPLICATIONS,lstDocsApps);
	g_ClientCache.SetCache(ENTITY_BUS_DM_CHECK_OUT_DOCS,lstDocsCheck);
	g_ClientCache.SetCache(ENTITY_BUS_EMAIL_TEMPLATES,lstEmailTemplates);

}


//process email by outlook id->try to find in the list of sent mails
//if found, get content->update
DbRecordSet CommunicationManager::ProcessEmailContentFromOutlook(bool bLogOut)
{
	m_Timer.stop(); //hold, to be sure, if this takes too long: later muv to thread

#ifdef _WIN32
	if ( m_lstSentEmails.getRowCount()==0) return DbRecordSet();

	QString strMessage=tr("Start Processing Sent Emails From Outlook");
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);


	//assemble mail id list:
	QList<QByteArray> lstEmailIDs;
	int nSize=m_lstSentEmails.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstEmailIDs<<m_lstSentEmails.getDataRef(i,1).toByteArray();
	}

	//import data from outlook:
	Status status; 
	DbRecordSet lstSelected;
	FUI_ImportEmail::ReadEmails(status, lstEmailIDs,lstSelected);
	if(!status.IsOK())
		return DbRecordSet();

	//remove all non filled
	lstSelected.clearSelection();
	nSize=lstSelected.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (!lstSelected.getDataRef(i,"BEM_SUBJECT").isNull() || !lstSelected.getDataRef(i,"BEM_FROM").isNull())
		{
			lstSelected.setData(i,"BEM_ID",m_lstSentEmails.getDataRef(i,0)); //set email id for update
			lstSelected.setData(i,"CENT_ID",m_lstSentEmails.getDataRef(i,2)); //set cent id for update
			lstSelected.selectRow(i); //no data from outlook, select it
		}
	}
	lstSelected.deleteUnSelectedRows();

	//send to server:
	if (lstSelected.getRowCount()>0)
	{
		int nSize = lstSelected.getRowCount();

		const int nChunkSize = 15;
		int nCount = 0;
		int nFailuresCnt = 0;
		int nStart = -1;
		int nRowProgress = 0;

		DbRecordSet lstChunk;
		lstChunk.copyDefinition(lstSelected);


		//lstSelected.Dump();


		while(1)
		{
			DbRecordSet lstAttachments;
			lstAttachments.addColumn(DbRecordSet::GetVariantType(), "ATT_LIST");

			//prepare chunk
			lstChunk.clear();

			int nMax = min(nCount+nChunkSize, nSize);
			for(int i=nCount; i<nMax; i++)
			{
				if(!lstChunk.addRow()) break;
				DbRecordSet row1 = lstSelected.getRow(i);
				lstChunk.assignRow(lstChunk.getRowCount()-1, row1);
				//append attachment
				lstAttachments.addRow();
				DbRecordSet row = lstSelected.getDataRef(i, "ATTACHMENTS").value<DbRecordSet>();
				lstAttachments.setData(lstAttachments.getRowCount()-1, 0, row);

			}
			if(lstChunk.getRowCount() < 1)
				break;

			nCount += lstChunk.getRowCount();


			//BT added: set owner to logged user:
			if (g_pClientManager->GetPersonID()!=0)
				lstChunk.setColValue(lstChunk.getColumnIdx("CENT_OWNER_ID"),g_pClientManager->GetPersonID());
			//BT added: set mail type:
			lstChunk.setColValue(lstChunk.getColumnIdx("CENT_SYSTEM_TYPE_ID"),GlobalConstants::CE_TYPE_EMAIL);

			DbRecordSet lsEmails;
			lsEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_UPDATE));
			lsEmails.merge(lstChunk);
			//lsEmails.Dump();

			//special for update: /email & cent id set, lock on server, update only mail data (body, to, from, attach) return...
			Status status;
			_SERVER_CALL(BusEmail->UpdateEmailFromClient(status, lsEmails, lstAttachments))
			_CHK_ERR_NO_RET(status);
			if (!status.IsOK())    //if failed, resume timer
			{
				if(!bLogOut)
				{
					m_Timer.start();
				}
				else
				{
					m_lstSentEmails.clear();
				}
				return DbRecordSet();
			}
		}

		
		//------------------------------------------------------
		//Send logger msg that some mails have been processed
		//------------------------------------------------------
		//log msg:
		QString strMessage=tr("Processed ")+QVariant(nSize).toString()+" Sent Emails From Outlook at "+QDateTime::currentDateTime().toString(Qt::LocaleDate);
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);
	}



	//match by outlookID, delete from local list, if empty, destroy timer:
	//m_lstSentEmails.Dump();
	nSize=lstSelected.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		int nRow=m_lstSentEmails.find(1,lstSelected.getDataRef(i,"BEM_EXT_ID"),true);
		if (nRow!=-1)
			m_lstSentEmails.deleteRow(nRow);
	}

	//notify cache:
	if (nSize>0)
	{
		g_ClientCache.ModifyCache(ENTITY_BUS_EMAILS,lstSelected,DataHelper::OP_EDIT);
	}

	//if logout, delete all sent mails (by CENT_ID field):
	Status err;
	if(bLogOut)
	{

		//issue 1580
		//if (m_lstSentEmails.getRowCount()>0)
		//{
		//	_SERVER_CALL(ClientSimpleORM->Delete(err,CE_COMM_ENTITY,m_lstSentEmails))
		//	_CHK_ERR_NO_RET(err);
		//}
		m_lstSentEmails.clear(); //whatever outcome, clear d list
		m_Timer.stop();			 //stop d'timer
	}
	else
	{
		//start timer if failed (start only if list has some left):
		if (m_lstSentEmails.getRowCount()>0)
		{
			m_Timer.setInterval(180000); //3mins
			m_Timer.start();
		}
	}

	strMessage=tr("End Processing Sent Emails From Outlook");
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);
#else
	DbRecordSet lstSelected;
#endif

	//return actually imported mails:
	return lstSelected;
}

//add to static list
void CommunicationManager::AddEmailToSentMailList(int nEmailID, QByteArray byteOutlookID, int nCENT_ID)
{
	//RedefineSentMailList();

	int nRow=m_lstSentEmails.find(0,nEmailID,true);
	if (nRow==-1)
	{
		m_lstSentEmails.addRow();
		nRow=m_lstSentEmails.getRowCount()-1;
		m_lstSentEmails.setData(nRow,0,nEmailID);
		m_lstSentEmails.setData(nRow,1,byteOutlookID);
		m_lstSentEmails.setData(nRow,2,nCENT_ID);
		
		if (!m_Timer.isActive())
		{
			if (!m_bTimerConnected)
			{
				connect(&m_Timer,SIGNAL(timeout()),this,SLOT(OnProcessOutlookEmails()));
				m_bTimerConnected=true;
			}
			
			m_Timer.setInterval(180000); //3mins
			m_Timer.start();
			
		}
	}
}



//redefine mail list
void CommunicationManager::RedefineSentMailList()
{
	if (m_lstSentEmails.getColumnCount()>0)return;
	m_lstSentEmails.destroy();
	m_lstSentEmails.addColumn(QVariant::Int,"ID");
	m_lstSentEmails.addColumn(QVariant::ByteArray,"OUTLOOK_ID");
	m_lstSentEmails.addColumn(QVariant::Int,"CENT_ID");
}



//check for emails:
void CommunicationManager::OnProcessOutlookEmails()
{
	ProcessEmailContentFromOutlook();
}



bool CommunicationManager::OpenMailInOutlook(QByteArray arData, QString strExternApp, bool bSupressMsg)
{
#ifdef _WIN32
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_OUTLOOK, nValue))
		return false;
	return MapiManager::OpenMailInOutlook(arData,strExternApp,bSupressMsg);
#else
	return false;
#endif
}




//reads email data, sends it to the outlook
bool CommunicationManager::OpenMailInOutlookFromDatabaseRecord(int nEmailID)
{
#ifdef _WIN32
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_OUTLOOK, nValue))
		return false;

	DbRecordSet recToRecordSet,recCCRecordSet,recBCCRecordSet;
	DbRecordSet m_recMail,lstContactLink,lstContactEmails,lstTask,lstAttachments, lstUAR,lstGAR;
	Status err;
	_SERVER_CALL(BusEmail->Read(err,nEmailID,m_recMail,lstContactLink,lstTask,lstContactEmails,lstAttachments,lstUAR,lstGAR))
	_CHK_ERR_NO_RET(err);
	if (!err.IsOK()) return false;

	//load data:
	EmailHelperCore::LoadRecipients(lstContactEmails,recToRecordSet,m_recMail.getDataRef(0,"BEM_TO").toString());
	EmailHelperCore::LoadRecipients(lstContactEmails,recCCRecordSet,m_recMail.getDataRef(0,"BEM_CC").toString());
	EmailHelperCore::LoadRecipients(lstContactEmails,recBCCRecordSet,m_recMail.getDataRef(0,"BEM_BCC").toString());

	//return OpenMailInOutlookFromRecord(m_recMail,lstAttachments,recToRecordSet,recCCRecordSet,recBCCRecordSet,false);
	bool bPlain=(m_recMail.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT);
	return MapiManager::OpenMailInOutlookFromRecord(m_recMail,lstAttachments,recToRecordSet,recCCRecordSet,recBCCRecordSet,bPlain,false);
#else
	return false;
#endif
}




//issue 1674: no document is zipped!
bool CommunicationManager::PackAndSendDocuments(DbRecordSet &lstDocuments,QPoint *pos,int nProjectID,int nContactID)
{

	QStringList lstZippedPaths;
	if (!DocumentHelper::PackDocuments(lstDocuments,lstZippedPaths,true))
		return false;

	CommunicationActions::sendEmail(nContactID,"",nProjectID,pos,&lstZippedPaths);
	return true;
}





//----------------------------------Document Drop processing------------------------------
//----------------------------------Document Drop processing------------------------------
//----------------------------------Document Drop processing------------------------------

//bIgnoreURLs all but vcf....
//bool= processed, else nothing
bool CommunicationManager::ProcessDocumentDrop(QList<QUrl> &lst, bool bCreateTemplates,bool bAcceptApplications,bool bIgnoreHttpURLs, QString strCategory)
{

	//list of paths (files), list of application paths (exe)
	DbRecordSet lstPaths;
	DbRecordSet lstApps;

	//SetDocumentDropDefaults(pLstContactsDefault,pLstProjectDefault);


	DataHelper::ParseFilesFromURLs(lst,lstPaths,lstApps,true,bAcceptApplications);

	if (lstPaths.getRowCount()==0 && lstApps.getRowCount()==0)
		return false;

	//lstPaths.Dump();

	//if HTTP drop then process
	int nSelCnt=lstPaths.find(2,QString("HTTP"));
	if (nSelCnt>0)
	{
		//A: find vcards: vcf & pdf
		int nSize=lstPaths.getRowCount();

		if (nSize==1)
			if (lstPaths.getDataRef(0,1).toString().right(4)==".vcf" || lstPaths.getDataRef(0,1).toString().right(4)==".vcard")
			{
				//->indicate dropzone widget to parse vcard file itself...
				//issue 2524
				return false;
			}


		for(int i=0;i<nSize;++i)
		{
			if (lstPaths.getDataRef(i,1).toString().right(4)==".vcf")
			{
				DbRecordSet lstImportContacts = ClientContactManager::CreateContactsFromvCardUrl(lst.at(0).toString());
				if (lstImportContacts.getRowCount()>0)
				{
					int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
					FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
					if (pFui)
					{
						pFui->SetImportListFromSource(lstImportContacts);
						pFui->show();  
					}

				}
				return true;
			}
			QString strExtension=lstPaths.getDataRef(i,1).toString().right(4);
			if (strExtension==".pdf" || strExtension==".zip" || strExtension==".jpg" || strExtension==".gif" || strExtension==".png" || strExtension==".doc" )
			{
				int nRes=QMessageBox::question(NULL,tr("Confirmation"),tr("File storage location:"),tr("Download and Store File"),tr("Store Link to File"),tr("Cancel"));
				if (nRes==1)
					break;
				if (nRes==0)
					return CreateFileFromURL(lst.at(i),lstPaths.getRow(i),bCreateTemplates,strCategory);
				else
					return false;
			}
		}


		//B: rest https:
		if(bIgnoreHttpURLs) //if http drop & ignore, ciao bao
			return false;



		//test if drop on template TAB:
		if (bCreateTemplates)
		{
			int nResult=QMessageBox::question(NULL,tr("Create Template"),tr("Create ")+QVariant(lstPaths.getRowCount()).toString()+tr(" template(s) from dropped items(s)"),tr("Yes"),tr("No"));
			if (nResult==0)
			{
				DocumentHelper::RegisterDocumentInternetAddresses(lstPaths,true,strCategory);
				return true;
			}
		}
		DocumentHelper::RegisterDocumentInternetAddresses(lstPaths);
		return true;

	}




	//register apps:
	if (lstApps.getRowCount()>0)
	{
		DocumentHelper::RegisterApplicationPaths(lstApps);
	}

	//sort out vcf files:
	DbRecordSet lstVCard;
	lstVCard.copyDefinition(lstPaths);
	lstPaths.find(2,QString("vcf"));
	lstPaths.find(2,QString("vcard"),false,false,false,true);
	lstVCard.merge(lstPaths,true);
	lstPaths.deleteSelectedRows(); //remove those from drop->go to vcf drop
	if (lstVCard.getRowCount()>0)
	{
		DbRecordSet lstImportContacts = ClientContactManager::CreateContactsFromvCard(lstVCard);
		//_DUMP(lstImportContacts);

		if (lstImportContacts.getRowCount()>0)
		{
			int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
			FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
			if (pFui)
			{
				pFui->SetImportListFromSource(lstImportContacts);
				pFui->show();  
			}
		}
		else
		{
			//->indicate dropzone widget to parse vcard file itself...
			//issue 2524
			return false;
		}

	}

	if (lstPaths.getRowCount()>0)
	{

		//test if drop on template TAB:
		if (bCreateTemplates)
		{
			int nResult=QMessageBox::question(NULL,tr("Create Template"),tr("Create ")+QVariant(lstPaths.getRowCount()).toString()+tr(" template(s) from dropped items(s)"),tr("Yes"),tr("No"));
			if (nResult==0)
				DocumentHelper::RegisterDocumentPaths(lstPaths,lst,1,strCategory);
			else
			{
				int nResult=QMessageBox::question(NULL,tr("Create Documents"),tr("Create ")+QVariant(lstPaths.getRowCount()).toString()+tr(" document(s) from dropped items(s)"),tr("Yes"),tr("No"));
				if (nResult==0)
					DocumentHelper::RegisterDocumentPaths(lstPaths,lst);
				else
					return false; //exit
			}
		}
		else
			DocumentHelper::RegisterDocumentPaths(lstPaths,lst);
	}

	return true;
}






void CommunicationManager::EmailReply(DbRecordSet &recEmail,QPoint *pos)
{

	if (recEmail.getRowCount()>0)
	{
		int nEmailID=recEmail.getDataRef(0,"BEM_ID").toInt();
		CommunicationActions::sendEmail(0,"",-1,pos,NULL,nEmailID);
	}

}
void CommunicationManager::EmailForward(DbRecordSet &recEmail,QPoint *pos)
{
	if (recEmail.getRowCount()>0)
	{
		int nEmailID=recEmail.getDataRef(0,"BEM_ID").toInt();
		CommunicationActions::sendEmail(0,"",-1,pos,NULL,nEmailID,true);
	}

}



bool CommunicationManager::DropCommDataToProject(DbRecordSet &Data,int nProjectID)
{

	//determine if projct id is alredy linked to dropped items (only for emails, phones), for doc, will be skipped or added:

	//get lists:
	DbRecordSet lstEmails,lstVC,lstDocs;
	int nRow=Data.find(0,QVariant(ENTITY_BUS_EMAILS).toInt(),true);
	if (nRow!=-1)
	{
		lstEmails=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}
	nRow=Data.find(0,QVariant(ENTITY_BUS_VOICECALLS).toInt(),true);
	if (nRow!=-1)
	{
		lstVC=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}
	nRow=Data.find(0,QVariant(ENTITY_BUS_DM_DOCUMENTS).toInt(),true);
	if (nRow!=-1)
	{
		lstDocs=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}

	//_DUMP(lstDocs);
	//lstEmails.Dump();

	int nSize=lstEmails.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstEmails.getDataRef(i,"BUSP_ID").toInt()==nProjectID || lstEmails.getDataRef(i,"BUSP_ID").isNull())
			continue;

		QString strMsg=tr("Email: ")+lstEmails.getDataRef(i,"BEM_SUBJECT").toString()+tr(" is already assigned to the other project. Continue with operation and reassign it to the current project?");

		if(QMessageBox::question(0,tr("Project Assignment"),strMsg,tr("Yes"),tr("No"),0,1)!=0)
			return false;
	}



	nSize=lstVC.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstVC.getDataRef(i,"BUSP_ID").toInt()==nProjectID || lstVC.getDataRef(i,"BUSP_ID").isNull())
			continue;

		QString strMsg=tr("Voice Call is already assigned to the other project. Continue with operation and reassign it to the current project?");

		if(QMessageBox::question(0,tr("Project Assignment"),strMsg,tr("Yes"),tr("No"),0,1)!=0)
			return false;
	}

	//write:
	Status err;
	_SERVER_CALL(BusCommunication->AssignCommDataToProject(err,Data,nProjectID))
	_CHK_ERR_RET_BOOL(err);


}




bool CommunicationManager::CreateFileFromURL(QUrl lstSourceURLPdf,DbRecordSet recPathURL, bool bCreateTemplates, QString strCategory)
{

	QList<QUrl> lstSourceURL;
	lstSourceURL.append(lstSourceURLPdf);

	if (recPathURL.getRowCount()!=1)
		return false;

	//download pdf?
	QString strURL=recPathURL.getDataRef(0,0).toString();
	QString strNamePdf=recPathURL.getDataRef(0,1).toString();
	QByteArray content;

	HttpReaderSync WebReader;
	connect(&WebReader,SIGNAL(ReEmitAuthenticationRequired ( const QString & , quint16 , QAuthenticator *  )),this, SLOT(OnEmitAuthenticationRequired ( const QString & , quint16 , QAuthenticator *  )));
	connect(&WebReader,SIGNAL(ReadInProgress ( int,int )),g_pClientManager, SLOT(OnCommunicationInProgress_Ext ( int,int  )));

	g_pClientManager->ShowTransferProgressDlg(tr("Downloading document: ")+strNamePdf);
	if(WebReader.ReadWebContent(strURL, &content))
	{
		g_pClientManager->HideTransferProgressDlg();

		//save filek:
		//save file in temp
		QString strTempDirectory=DocumentHelper::GetTempDirectoryForAutomaticStorage();
		if (strTempDirectory.isEmpty())
		{
			QMessageBox::critical(NULL,tr("Error"),tr("Failed to save:")+strNamePdf);
			return false;
		}
		QString strFilePath=strTempDirectory+"/"+strNamePdf;
		if (!DocumentHelper::SaveFileContent(content,strFilePath,false))
		{
			return false;
		}

		//now process as if document drop:
		recPathURL.clear();
		recPathURL.addRow();
		recPathURL.setData(0,0,strFilePath);
		recPathURL.setData(0,1,strNamePdf);
		recPathURL.setData(0,2,"pdf");

		//test if drop on template TAB:
		if (bCreateTemplates)
		{
			int nResult=QMessageBox::question(NULL,tr("Create Template"),tr("Create ")+QVariant(recPathURL.getRowCount()).toString()+tr(" template(s) from dropped items(s)"),tr("Yes"),tr("No"));
			if (nResult==0)
				DocumentHelper::RegisterDocumentPaths(recPathURL,lstSourceURL,1,strCategory,false,NULL,NULL,GlobalConstants::DOC_TYPE_INTERNET_FILE,1); //issue 1826 go edit
			else
			{
				int nResult=QMessageBox::question(NULL,tr("Create Documents"),tr("Create ")+QVariant(recPathURL.getRowCount()).toString()+tr(" document(s) from dropped items(s)"),tr("Yes"),tr("No"));
				if (nResult==0)
					DocumentHelper::RegisterDocumentPaths(recPathURL,lstSourceURL,0,"",false,NULL,NULL,GlobalConstants::DOC_TYPE_INTERNET_FILE,1); //issue 1826 go edit
				else
					return false; //exit
			}
		}
		else
			DocumentHelper::RegisterDocumentPaths(recPathURL,lstSourceURL,0,"",false,NULL,NULL,GlobalConstants::DOC_TYPE_INTERNET_FILE,1); //issue 1826 go edit

		return true;
	}
	
	else
	{
		g_pClientManager->HideTransferProgressDlg();
		QMessageBox::critical(NULL,tr("Error"),tr("File can not be downloaded!"));
	}

	return false;

}


void CommunicationManager::OnEmitAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth)
{
	LoginDlg Dlg;
	Dlg.Initialize("",LoginDialogAbstract::LOGIN_DLG_WEB_AUTHENTICATION);
	Dlg.setWindowTitle(tr("Connect to ")+hostname);
	int nRes=Dlg.exec();
	if (nRes)
	{
		auth->setPassword(Dlg.m_strPass);
		auth->setUser(Dlg.m_strUser);
	}

}

void CommunicationManager::OnCheckInOutCompleted(Status err,int nDocumentID,QString strFilePath,DbRecordSet recRev,DbRecordSet recCheckOutInfo)
{
	if (m_lstDocumentsForOpenAfterUpload.contains(nDocumentID))
	{
		if (!err.IsOK())
		{
			m_lstDocumentsForOpenAfterUpload.remove(nDocumentID);
			return;
		}
		int nFuiMode=m_lstDocumentsForOpenAfterUpload.value(nDocumentID);
		int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,nFuiMode, nDocumentID,true);
		FUI_DM_Documents* pFui=dynamic_cast<FUI_DM_Documents*>(g_objFuiManager.GetFUIWidget(nNewFUI));
		if (pFui)
		{
			pFui->SetSimpleMode();
			pFui->show();  
		}
		m_lstDocumentsForOpenAfterUpload.remove(nDocumentID);
		return;
	}
	else if(m_lstDocsForCheckOutAndOpen.contains(nDocumentID))
	{
		if (!err.IsOK())
		{
			m_lstDocsForCheckOutAndOpen.remove(nDocumentID);
			return;
		}

		DbRecordSet data=m_lstDocsForCheckOutAndOpen.value(nDocumentID);
		m_lstDocsForCheckOutAndOpen.remove(nDocumentID);
		if (data.getRowCount()!=1)
			return;

		//_DUMP(data);
		//open file
		DocumentHelper::OpenDocumentInExternalApp_Shell(data.getDataRef(0,"nAppID").toInt(),strFilePath,data.getDataRef(0,"strParams").toString(),data.getDataRef(0,"strAppParameters").toString());
	}


	
}

//issue: 2044: open file selector dialog, select url: process as doc drop!!
void CommunicationManager::LoadDocuments()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.*";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;

	QFileDialog Dlg;
	Dlg.setFileMode(QFileDialog::ExistingFiles);
	Dlg.setDirectory(strStartDir);

	QStringList lstFiles;
	if(Dlg.exec())
	{
		lstFiles=Dlg.selectedFiles();
	}
	else
		return;

	if (lstFiles.size()!=0)
	{
		QFileInfo fileInfo(lstFiles.at(0));
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	else
		return;

/*
	QStringList lstFiles = QFileDialog::getOpenFileNames (
		NULL,
		tr("Load Existing Document(s)/Directories"),
		strStartDir,
		strFilter);
	if (lstFiles.size()!=0)
	{
		QFileInfo fileInfo(lstFiles.at(0));
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	else
		return;
*/
	QList<QUrl> lstUrls;

	int nSize=lstFiles.size();
	for(int i=0;i<nSize;i++)
	{
		lstUrls.append(QUrl::fromLocalFile(lstFiles.at(i)));
	}

	ProcessDocumentDrop(lstUrls,false,false);
}