#include "FUI_TaskReminders.h"
#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt* g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;		

#include "menuitems.h"

FUI_TaskReminders::FUI_TaskReminders(QWidget *parent)
	: FuiBase(parent)
{
	ui.setupUi(this);

	//--------------------------
	//Set Grid.
	//--------------------------
	ui.widgetCommGrid->Initialize(CommGridViewHelper::DESKTOP_GRID, this);

	Status err;
	DbRecordSet lstData;
	_SERVER_CALL(BusCommunication->GetDefaultDesktopFilterViews(err,g_pClientManager->GetPersonID(),lstData))
	_CHK_ERR(err);
	if (lstData.getRowCount())
		ui.widgetCommGrid->DesktopStartupInitialization(g_pClientManager->GetPersonID(),lstData.getDataRef(0,"BUSCV_ID").toInt());

	g_ChangeManager.registerObserver(this);
}
	

FUI_TaskReminders::~FUI_TaskReminders()
{
	g_ChangeManager.unregisterObserver(this);

}

void FUI_TaskReminders::SetCurrentFUI(bool bCurrent)
{
	ui.widgetCommGrid->SetCurrentGridInSidebarMode(bCurrent);
}

QFrame* FUI_TaskReminders::GetFrameCommToolBar()
{
	return ui.frameCommToolBar;
}
QFrame* FUI_TaskReminders::GetFrameCommParent()
{
	return ui.frameCommParent;
}

DropZone_MenuWidget* FUI_TaskReminders::GetDropZone_MenuWidget()
{
	return ui.widgetDropZone;
}

QLabel* FUI_TaskReminders::GetFrameCommLabel()
{
	return ui.labelComm;
}

	

void FUI_TaskReminders::GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strMail,int &nEmailReplyID,QString &strPhone,QString &strInternet)
{
	//get selection from grid:brb...
}

void FUI_TaskReminders::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{

	if (pSubject==&g_ChangeManager)
	{
		if(nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
		{
			//OnThemeChanged();
			return;
		}
	}
}

