#include "calendarpersonheaderitem.h"
#include <QVBoxLayout>
#include <QHeaderView>

#include "calendartableview.h"
#include "tabledisplaywidget.h"

PersonHeaderDisplayWidget::PersonHeaderDisplayWidget(QIcon icon,  QString strText, int nHeight, CalendarGraphicsView *pCalendarGraphicsView, 
													 QWidget *parent /*= 0*/) : QFrame(parent)
{
	m_pCalendarGraphicsView = pCalendarGraphicsView;
	QTableWidgetItem *item = new QTableWidgetItem(icon, strText);
	TableDisplayWidget *table = new TableDisplayWidget(m_pCalendarGraphicsView, 1,1);
	table->horizontalHeader()->hide();
	table->verticalHeader()->hide();
	table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	table->setShowGrid(false);
	table->setSelectionMode(QAbstractItemView::NoSelection);
	table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	table->horizontalHeader()->setStretchLastSection(true);
	table->verticalHeader()->setStretchLastSection(true);
	table->setMaximumHeight(nHeight);
	QString strStyle = "QTableWidget {color:white; background: rgb(128,128,128); border-width:2px; border-style: outset; \
			  border-top-color: gray; border-left-color: gray; border-right-color: black; border-bottom-color: black; }";
	table->setStyleSheet(strStyle);
	table->setItem(0,0,item);
	
	QVBoxLayout *layout = new QVBoxLayout();
	layout->setSizeConstraint(QLayout::SetMaximumSize);
	layout->setContentsMargins(0,0,0,0);
	layout->addWidget(table);
	setLayout(layout);
}

PersonHeaderDisplayWidget::~PersonHeaderDisplayWidget()
{

}

CalendarPersonHeaderItem::CalendarPersonHeaderItem(int nColumn, QIcon icon,  QString strText, int nHeight, 
 CalendarGraphicsView *pCalendarGraphicsView, QGraphicsProxyWidget	 *pCalendarTableView, CalendarTableModel *pCalendarTableModel, QGraphicsItem * parent /*= 0*/, Qt::WindowFlags wFlags /*= 0*/)
	: QGraphicsProxyWidget(parent, wFlags)
{
	m_pCalendarGraphicsView = pCalendarGraphicsView;
	m_pCalendarTableView	= pCalendarTableView;
	m_pCalendarTableModel	= pCalendarTableModel;
	m_nColumn = nColumn;
	m_nHeight = nHeight;
	
	m_pLabel = new Label(Label::HEADER_ITEM);
	m_pLabel->setText(strText);
	m_pLabel->setAlignment(Qt::AlignCenter);
	m_pLabel->setMinimumHeight(m_nHeight);
	m_pLabel->setMaximumHeight(m_nHeight);

	PersonHeaderDisplayWidget *widg = new PersonHeaderDisplayWidget(icon, strText, m_nHeight, m_pCalendarGraphicsView);

	//setWidget(m_pLabel);
	setWidget(widg);
	setZValue(4);

	connect(m_pCalendarGraphicsView, SIGNAL(UpdateCalendar()), this, SLOT(on_UpdateCalendar()));

	setMinimumWidth(0);
}

CalendarPersonHeaderItem::~CalendarPersonHeaderItem()
{
	m_pCalendarGraphicsView = NULL;
	m_pLabel				= NULL;
	m_pCalendarTableView	= NULL;
	m_pCalendarTableModel	= NULL;
}

void CalendarPersonHeaderItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget /*= 0*/)
{
	QGraphicsProxyWidget::paint(painter, option, widget);
	
	//Draw vertical separator between column headers.
	QPen pen(Qt::lightGray);
	painter->setPen(pen);

	if (!m_nColumn)
		painter->drawLine(0, 0, 0, m_nHeight);

	//-1 for nice drawing of vertical line.
	painter->drawLine(this->widget()->width(), 0, this->widget()->width(), m_nHeight);

	//Bottom horizontal line.
	painter->drawLine(0, m_nHeight, this->widget()->width(), m_nHeight);
}

void CalendarPersonHeaderItem::on_UpdateCalendar()
{
	QModelIndex index = m_pCalendarTableModel->index(0, m_nColumn);
	QRect rect = dynamic_cast<CalendarTableView*>(m_pCalendarTableView)->getItemRectangle(index);
	resize(rect.width()-1, m_nHeight);
	setPos(rect.topLeft().x()+m_pCalendarGraphicsView->getVertHeaderWidth(), m_nHeight);
}
