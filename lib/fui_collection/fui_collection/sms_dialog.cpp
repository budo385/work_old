#include "sms_dialog.h"
#include "os_specific/os_specific/skypemanager.h"
#include "bus_client/bus_client/phoneselectordlg.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/thememanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "db_core/db_core/dbsqltableview.h"

//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

bool IsPhoneNumber(QString strNumber);
#ifdef _WIN32
#include "os_specific/os_specific/skype/skypeparser.h"
extern SkypeParser	g_objParser;
#endif

SMS_Dialog::SMS_Dialog(QWidget *parent)
	: QDialog(parent, Qt::WindowTitleHint|Qt::WindowSystemMenuHint)
{
	m_nContactID = -1;

	ui.setupUi(this);
	ui.btnPickPhone->setIcon(QIcon(":arrow_down16.png"));	//Phone16.png
	
	connect(ui.txtSMS, SIGNAL(textChanged()), this, SLOT(on_SMS_textChanged()));
	UpdateStats();

	//header style sheets
	ui.frame->setStyleSheet("QFrame#frame " + ThemeManager::GetSidebarChapter_Bkg());
	ui.label_3->setPixmap(QPixmap(":Comm_SMS48.png"));
	ui.label_4->setStyleSheet("QLabel {color:white; font-style:italic; font-weight:800; font-family:Arial; font-size:12pt}");
	ui.label->setStyleSheet("QLabel {color:white}");

	/*
	//add UAR
	uarWidget = new AccUserRecordSelector;
	QHBoxLayout *uarLayout = new QHBoxLayout;
	ui.hboxLayout4->addWidget(uarWidget);
	*/

	ui.btnSaveAndSend->hide();
}

SMS_Dialog::~SMS_Dialog()
{
}

void SMS_Dialog::SetNumber(QString &strNumber)
{
	ui.txtPhone->setText(strNumber);
}

void SMS_Dialog::on_btnCancel_clicked()
{
	done(0);
}

void SMS_Dialog::UpdateStats()
{
	int nLetters  = ui.txtSMS->toPlainText().length();
	int nMessages = (nLetters + 159) / 160;

	ui.labelStats->setText(QString(tr("%1 letters typed (%2 messages)")).arg(nLetters).arg(nMessages));
}

void SMS_Dialog::on_SMS_textChanged()
{
	UpdateStats();
}

bool SMS_Dialog::SendSMS()
{
	QString strPhone = ui.txtPhone->text();
	if(strPhone.isEmpty()){
		QMessageBox::information(NULL, "", tr("You must enter Phone Number!"));
		return false;
	}
	if(!IsPhoneNumber(strPhone)){
		QMessageBox::information(NULL, "", tr("Invalid Phone Number!"));
		return false;
	}
	QString strMessage = ui.txtSMS->toPlainText();
	if(strMessage.isEmpty()){
		QMessageBox::information(NULL, "", tr("Message is empty!"));
		return false;
	}

#ifdef _WIN32
	g_objParser.SetSkypeUserInfoWnd(this); //.m_pSkypeUserInfoFui = (FuiBase *)this;	//set result target
	QString strCmd = QString().sprintf("CREATE SMS OUTGOING %s", strPhone.toLatin1().constData());
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
#endif
	return true;
}

void SMS_Dialog::on_btnSend_clicked()
{
	SendSMS();
}

void SMS_Dialog::on_SMS_Created(QString &strID)
{
	m_strSMSID = strID;

#ifdef _WIN32
	//set body
	QString strMessage = ui.txtSMS->toPlainText();
	QString strCmd = QString().sprintf("SET SMS %s BODY %s", strID.toUtf8().constData(), strMessage.toUtf8().constData());
	SkypeManager::SendCommand(strCmd.toUtf8().constData());

	//try to send message
	g_objParser.SetSkypeUserInfoWnd(this); ///m_pSkypeUserInfoFui = (FuiBase *)this;	//set result target
	g_objParser.m_bSMSWaitResult = true;
	strCmd = QString().sprintf("ALTER SMS %s SEND", strID.toLatin1().constData());
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
#endif

	ui.btnCancel->setText(tr("Close"));
	//done(0);
	
	QMessageBox::information(NULL, tr("Acknowledgement"), tr("The SMS has been transferred to Skype(C) to be sent from there. You will check its delivery it in Skype's event protocol."));
}

void SMS_Dialog::on_SMS_Failed(QString &strID, QString &strMsg)
{
	QString strTxt = QString(tr("Error sending an SMS message (%1)!")).arg(strMsg);
	QMessageBox::information(NULL, "", strTxt);

	//delete unsent message
	QString strCmd = QString().sprintf("DELETE SMS %s", strID.toLatin1().constData());
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
}

void SMS_Dialog::on_SMS_Sent(QString &strID, QString &strMsg)
{
	QString strTxt = QString(tr("SMS message sent (%1)!")).arg(strMsg);
	QMessageBox::information(NULL, "", strTxt);

	ui.btnCancel->setText(tr("Close"));
	//done(0);
}

void SMS_Dialog::on_btnPickPhone_clicked()
{
	QStringList lstPhoneNums, lstPhoneNames, lstPhoneTypes;

	if(m_nContactID >= 0)
	{
		//get contact numbers
		Status err;
		DbRecordSet lstPhones;
		DbRecordSet lstPicture,lstDataNMRXContacts,lstDataNMRXProjects;
		_SERVER_CALL(BusContact->ReadContactDataForVoiceCall(err,m_nContactID,lstPhones,lstPicture,lstDataNMRXContacts,lstDataNMRXProjects))
		if (!err.IsOK()) return;

		//load phone types
		MainEntitySelectionController TypeHandler;
		TypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_PHONE);
		TypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
		TypeHandler.ReloadData();
		DbRecordSet recPhoneTypes = *TypeHandler.GetDataSource();

		int nSize=lstPhones.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			lstPhoneNums	<< lstPhones.getDataRef(i,"BCMP_SEARCH").toString();
			lstPhoneNames	<< lstPhones.getDataRef(i,"BCMP_NAME").toString();

			int nRow = recPhoneTypes.find("BCMT_ID", lstPhones.getDataRef(i,"BCMP_TYPE_ID").toString(), true);
			if(nRow >= 0)
				lstPhoneTypes	<< recPhoneTypes.getDataRef(nRow,"BCMT_TYPE_NAME").toString();
			else
				lstPhoneTypes	<< QString();
		}
	}

	PhoneSelectorDlg dlg(lstPhoneNums, lstPhoneNames, lstPhoneTypes);

	//set position to simulate combo box (MB 2008.01.23)
	QPoint pt = ui.txtPhone->geometry().bottomLeft();
	pt += geometry().topLeft();
	dlg.move(pt);

	if(dlg.exec() > 0)
	{
		QString strPhone = dlg.getSelectedPhone();
		if(!strPhone.isEmpty())
			ui.txtPhone->setText(strPhone);
	}
}

void SMS_Dialog::on_btnSaveAndSend_clicked()
{
	//send (with checks), then save
	if(SendSMS()){
		Status status;
		QString strLockRes;
		DbRecordSet lstData;
		DbRecordSet lstTask;
		
		DbRecordSet lstUAR, lstGAR;
		ui.arSelector->Save(-1,BUS_SMS,&lstUAR,&lstGAR); //get ar rows for save (fiktivni poziv)
	
		//define list:
		DbRecordSet set;
		set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
		lstData.copyDefinition(set, false);
		set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_SMS));
		lstData.copyDefinition(set, false);

		lstData.addRow();
		lstData.setData(0, "CENT_SYSTEM_TYPE_ID", GlobalConstants::CE_TYPE_SMS);
		lstData.setData(0, "CENT_OWNER_ID", g_pClientManager->GetPersonID());
		lstData.setData(0, "BSMS_TEXT", ui.txtSMS->toPlainText());
		lstData.setData(0, "BSMS_PHONE_NUMBER", ui.txtPhone->text());
		lstData.setData(0, "BSMS_DATE_SENT", QDateTime::currentDateTime());

		_SERVER_CALL(BusSms->WriteSms(status, lstData, strLockRes, lstTask, lstUAR, lstGAR));
		_CHK_ERR_NO_MSG(status);
		
		QMessageBox::information(NULL, "", tr("Done!"));
	}
}

class SMSInfo : public SkypeInterface
{
public:
	SMSInfo(){};
	~SMSInfo();

	void SendSMS(const QString &strPhone, const QString &strMessage);

protected:
	QString m_strPhone;
	QString m_strMessage;

private:
	void on_SMS_Created(QString &strID);
	void on_SMS_Failed(QString &strID, QString &strMsg);
	void on_SMS_Sent(QString &strID, QString &strMsg);
};

SMSInfo::~SMSInfo()
{
#ifdef _WIN32
	g_objParser.SetSkypeUserInfoWnd(NULL);
#endif
}

void SMSInfo::SendSMS(const QString &strPhone, const QString &strMessage)
{
	m_strPhone = strPhone;
	m_strMessage = strMessage;

#ifdef _WIN32
	g_objParser.SetSkypeUserInfoWnd(this); //.m_pSkypeUserInfoFui = (FuiBase *)this;	//set result target
	QString strCmd = QString().sprintf("CREATE SMS OUTGOING %s", strPhone.toLatin1().constData());
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
#endif
}

void SMSInfo::on_SMS_Created(QString &strID)
{
#ifdef _WIN32
	//set body
	QString strCmd = QString().sprintf("SET SMS %s BODY %s", strID.toUtf8().constData(), m_strMessage.toUtf8().constData());
	SkypeManager::SendCommand(strCmd.toUtf8().constData());

	//try to send message
	g_objParser.SetSkypeUserInfoWnd(this); ///m_pSkypeUserInfoFui = (FuiBase *)this;	//set result target
	g_objParser.m_bSMSWaitResult = true;
	strCmd = QString().sprintf("ALTER SMS %s SEND", strID.toLatin1().constData());
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
	
	QMessageBox::information(NULL, QObject::tr("Acknowledgement"), QObject::tr("The SMS has been transferred to Skype(C) to be sent from there. You will check its delivery it in Skype's event protocol."));
#endif
}

//notify of error
void SMSInfo::on_SMS_Failed(QString &strID, QString &strMsg)
{
	QString strTxt = QString(QObject::tr("Error sending an SMS message (%1)!")).arg(strMsg);
	QMessageBox::information(NULL, "", strTxt);

	//delete unsent message
	QString strCmd = QString().sprintf("DELETE SMS %s", strID.toLatin1().constData());
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
}

//notify of success
void SMSInfo::on_SMS_Sent(QString &strID, QString &strMsg)
{
	QString strTxt = QString(QObject::tr("SMS message sent (%1)!")).arg(strMsg);
	QMessageBox::information(NULL, "", strTxt);
}

SMSInfo g_info;

void SendSkypeSMS(const QString &strPhone, const QString &strMessage)
{
	g_info.SendSMS(strPhone, strMessage);
}
