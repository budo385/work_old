#include "wizardregistration.h"

//Wizard page inclusions - here you add new pages.
#include "fui_collection/fui_collection/reportselectionwizpage.h"
#include "fui_collection/fui_collection/deptselectwizpage.h"
#include "fui_collection/fui_collection/reportprintingdestination.h"
#include "fui_collection/fui_collection/clientsetupwizpage_persondata.h"
#include "fui_collection/fui_collection/clientsetupwizpage_password.h"
#include "fui_collection/fui_collection/clientsetupwizpage_comm.h"
#include "fui_collection/fui_collection/gridfilterselectionwizpage.h"
#include "fui_collection/fui_collection/clientsetupwizpage_firststep.h"
//common pages are relocated to the bus_client
#include "bus_client/bus_client/simpleselectionwizpage.h"
#include "bus_client/bus_client/simpleselectionwizfileselection.h"
#include "bus_client/bus_client/simpleselectionwizfileselectionexp.h"
#include "bus_client/bus_client/simpleselectioncheckboxwizpage.h"

WizardRegistration::WizardRegistration(int nWizardID)
{
	QList<WizardPage*> WizardPagesList;

	//--------------------------------------------------------------------------//
	//					HERE YOU ADD NEW WIZARD DEFINITIONS						//
	//--------------------------------------------------------------------------//

	switch(nWizardID)
	{
		case WizardRegistration::REPORT_WIZARD:
			{
				//Report wizard page definitions - ALLWAYS PASS PARENT TO PAGE.
				WizardPage *page = new ReportSelectionWizPage(WIZARD_REPORT_SELECTION_PAGE, QString(QObject::tr("Select Report")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				m_hshWizardPageHash.insert(WizardRegistration::REPORT_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::DEPARTMENT_SELECTION_WIZARD:
			{
				//Department selection report wizard.
				WizardPage *page = new DeptSelectWizPage(WIZARD_DEPARTMENT_SELECTION_PAGE, QString(QObject::tr("Department Selection")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				page = new ReportPrintingDestination(WIZARD_REPORT_DESTINATION_PAGE, QString(QObject::tr("Select report destination")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				m_hshWizardPageHash.insert(WizardRegistration::DEPARTMENT_SELECTION_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::CONTACT_SELECTION_WIZARD:
			{
				//Report wizard page definitions - ALLWAYS PASS PARENT TO PAGE.
				WizardPage *page = new SimpleSelectionWizPage(WIZARD_CONTACT_SELECTION_PAGE, QString(QObject::tr("Select Contacts")), ENTITY_BUS_CONTACT);
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				page = new ReportPrintingDestination(WIZARD_REPORT_DESTINATION_PAGE, QString(QObject::tr("Select report destination")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				m_hshWizardPageHash.insert(WizardRegistration::CONTACT_SELECTION_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::STARTUP_WIZARD:
			{
				//startup wizard:
				WizardPagesList << new ClientSetupWizPage_FirstStep(WIZARD_STARTUP_PAGE, QString(QObject::tr("First Steps")));
				WizardPagesList << new ClientSetupWizPage_PersonData(WIZARD_STARTUP_PAGE, QString(QObject::tr("Personal Data")));
				WizardPagesList << new ClientSetupWizPage_Password(WIZARD_STARTUP_PAGE, QString(QObject::tr("Password")));
				WizardPagesList << new ClientSetupWizPage_Comm(WIZARD_STARTUP_PAGE, QString(QObject::tr("Communication Data")));
				//WizardPagesList << new ClientSetupWizPage_Import(WIZARD_CONTACT_SELECTION_PAGE, QString(QObject::tr("Import Data")));
				m_hshWizardPageHash.insert(WizardRegistration::STARTUP_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::PROJECT_EXPORT_SELECTION_WIZARD:
			{
				//Project selection wizard
				WizardPagesList << new SimpleSelectionWizPage(WIZARD_PROJECT_SELECTION_PAGE, QString(QObject::tr("Select Projects for Export")), ENTITY_BUS_PROJECT);
				WizardPagesList << new SimpleSelectionWizFileSelection(WIZARD_FILE_SELECTION_PAGE, QString(QObject::tr("Select File for Project export (specify full file path, including custom filename)")));
				WizardPagesList << new SimpleSelectionCheckBoxWizPage(WIZARD_CHECKBOX_PAGE, QString(QObject::tr("Use Unicode for Export")));
				m_hshWizardPageHash.insert(WizardRegistration::PROJECT_EXPORT_SELECTION_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::USER_EXPORT_SELECTION_WIZARD:
			{
				//User selection wizard
				WizardPagesList << new SimpleSelectionWizPage(WIZARD_USER_SELECTION_PAGE, QString(QObject::tr("Select Users for Export")), ENTITY_BUS_PERSON);
				WizardPagesList << new SimpleSelectionWizFileSelection(WIZARD_FILE_SELECTION_PAGE, QString(QObject::tr("Select File for User export (specify full file path, including custom filename)")));
				WizardPagesList << new SimpleSelectionCheckBoxWizPage(WIZARD_CHECKBOX_PAGE, QString(QObject::tr("Use Unicode for Export")));
				m_hshWizardPageHash.insert(WizardRegistration::USER_EXPORT_SELECTION_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::CONTACT_EXPORT_SELECTION_WIZARD:
			{
				//Contact export selection wizard
				WizardPagesList << new SimpleSelectionWizPage(WIZARD_CONTACT_SELECTION_PAGE, QString(QObject::tr("Select Contacts for Export")), ENTITY_BUS_CONTACT);
				WizardPagesList << new SimpleSelectionWizFileSelectionExp(WIZARD_FILE_SELECTION_PAGE, QString(QObject::tr("Select File for Contact Export (specify full file path, including custom filename)")));
				WizardPagesList << new SimpleSelectionCheckBoxWizPage(WIZARD_CHECKBOX_PAGE, QString(QObject::tr("Use Unicode for Export")));
				m_hshWizardPageHash.insert(WizardRegistration::CONTACT_EXPORT_SELECTION_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::PROJECT_REPORT_SELECTION_WIZARD:
			{
				//Projects report selection wizard
				WizardPage *page = new SimpleSelectionWizPage(WIZARD_PROJECT_SELECTION_PAGE, QString(QObject::tr("Select Projects for Export")), ENTITY_BUS_PROJECT);
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				page = new ReportPrintingDestination(WIZARD_REPORT_DESTINATION_PAGE, QString(QObject::tr("Select report destination")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				m_hshWizardPageHash.insert(WizardRegistration::PROJECT_REPORT_SELECTION_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::CONTACT_COMMUNICATION_REPORT_SELECTION_WIZARD:
			{
				//Contact communication report selection wizard.
				WizardPage *page = new SimpleSelectionWizPage(WIZARD_CONTACT_SELECTION_PAGE, QString(QObject::tr("Select Contacts")), ENTITY_BUS_CONTACT);
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				page = new GridFilterSelectionWizPage(WIZARD_COMMUNICATION_FILTER_SELECTION_PAGE, QString(QObject::tr("Select Filter")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				page = new ReportPrintingDestination(WIZARD_REPORT_DESTINATION_PAGE, QString(QObject::tr("Select report destination")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				m_hshWizardPageHash.insert(WizardRegistration::CONTACT_COMMUNICATION_REPORT_SELECTION_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::CONTACT_SELECTION_WIZARD_SIMPLE:
			{
				//Contact selection wizard
				WizardPagesList << new SimpleSelectionWizPage(WIZARD_CONTACT_SELECTION_PAGE, QString(QObject::tr("Select Contacts")), ENTITY_BUS_CONTACT);
				m_hshWizardPageHash.insert(WizardRegistration::CONTACT_SELECTION_WIZARD_SIMPLE, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::REPORT_DESTINATION_WIZARD:
			{
				WizardPage *page = new ReportPrintingDestination(WIZARD_REPORT_DESTINATION_PAGE, QString(QObject::tr("Select report destination")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				m_hshWizardPageHash.insert(WizardRegistration::REPORT_DESTINATION_WIZARD, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
		case WizardRegistration::REPORT_CONTACT_RELATIONSHIPS:
			{
				//Report wizard page definitions - ALLWAYS PASS PARENT TO PAGE.
				WizardPage *page = new SimpleSelectionWizPage(WIZARD_CONTACT_SELECTION_PAGE, QString(QObject::tr("Select Contacts")), ENTITY_BUS_CONTACT);
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				page = new ReportPrintingDestination(WIZARD_REPORT_DESTINATION_PAGE, QString(QObject::tr("Select report destination")));
				page->SetFinishBtnTitle(QObject::tr("Print"));
				WizardPagesList << page;
				m_hshWizardPageHash.insert(WizardRegistration::REPORT_CONTACT_RELATIONSHIPS, new WizardPagesCollection(WizardPagesList));
				WizardPagesList.clear();
			}
			break;
			//Add new wizards here.
	}

}

WizardRegistration::~WizardRegistration()
{
	//This is deleted in gui_core/wizardbase.h
	//qDeleteAll(m_hshWizardPageHash);
	//m_hshWizardPageHash.clear();
}
