#include "dlg_addrschemainfo.h"
#include "gui_core/gui_core/thememanager.h"

Dlg_AddrSchemaInfo::Dlg_AddrSchemaInfo(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);
	
	this->setWindowTitle(tr("Info"));//set dialog title
	//issue 1702:
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

Dlg_AddrSchemaInfo::~Dlg_AddrSchemaInfo()
{

}


//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void Dlg_AddrSchemaInfo::on_btnOK_clicked()
{

	done(1);
}