#include "settings_projects.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;				//global access right tester

Settings_Projects::Settings_Projects(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
				  :SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	m_bIsOptionsPage = bOptionsSwitch;

	ui.setupUi(this);

	if(m_bIsOptionsPage)
	{
		ui.grpDefaultCommView->setVisible(false);
	}
	else
	{
		//Comm. grid view selector.
		LoadViewSelector();
		//Get default view.
		int nViewID = GetSettingValue(PROJECT_DEF_FILTER_VIEW).toInt();
		//Set current item.
		ui.viewSelect_comboBox->setCurrentIndex(ui.viewSelect_comboBox->findData(nViewID));
	}

	if(!g_AccessRight->TestAR(LOAD_PROJECT_LISTS))
		ui.grpStoredProjectList->setVisible(false);
	else
	{
		LoadStoredLists();
		int nListID = (m_bIsOptionsPage) ? GetSettingValue(PROJECT_OPTIONS_DEF_STORED_LIST).toInt() : GetSettingValue(PROJECT_SETTINGS_DEF_STORED_LIST).toInt();
		ui.cboDefaultStoredList->setCurrentIndex(ui.cboDefaultStoredList->findData(nListID));
	}
}

Settings_Projects::~Settings_Projects()
{

}

DbRecordSet Settings_Projects::GetChangedValuesRecordSet()
{
	if(m_bIsOptionsPage)
	{
		if(g_AccessRight->TestAR(LOAD_PROJECT_LISTS)){
			int nSelectedListID = ui.cboDefaultStoredList->itemData(ui.cboDefaultStoredList->currentIndex()).toInt();
			int nListID = GetSettingValue(PROJECT_OPTIONS_DEF_STORED_LIST).toInt();
			if (nSelectedListID != nListID)
				SetSettingValue(PROJECT_OPTIONS_DEF_STORED_LIST, nSelectedListID);
		}
	}
	else	//personal settings
	{
		//Comm. grid view selector.
		int nViewID = GetSettingValue(PROJECT_DEF_FILTER_VIEW).toInt();
		int nSelectedViewID = ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex()).toInt();
		if (nSelectedViewID != nViewID)
			SetSettingValue(PROJECT_DEF_FILTER_VIEW, nSelectedViewID);

		if(g_AccessRight->TestAR(LOAD_PROJECT_LISTS)){
			int nSelectedListID = ui.cboDefaultStoredList->itemData(ui.cboDefaultStoredList->currentIndex()).toInt();
			int nListID = GetSettingValue(PROJECT_SETTINGS_DEF_STORED_LIST).toInt();
			if (nSelectedListID != nListID)
				SetSettingValue(PROJECT_SETTINGS_DEF_STORED_LIST, nSelectedListID);
		}
	}

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

void Settings_Projects::LoadViewSelector()
{
	ui.viewSelect_comboBox->clear();

	Status status;
	DbRecordSet recTmp;
	_SERVER_CALL(BusCommunication->GetCommFilterViews(status, recTmp, g_pClientManager->GetPersonID(), 2 /*Project grid id*/))
	_CHK_ERR(status);

	//Fill combo.
	int nRowCount = recTmp.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		int ViewID = recTmp.getDataRef(i, "BUSCV_ID").toInt();
		QString strViewName = recTmp.getDataRef(i, "BUSCV_NAME").toString();
		ui.viewSelect_comboBox->addItem(strViewName, ViewID);
	}
}

void Settings_Projects::LoadStoredLists()
{
	ui.cboDefaultStoredList->clear();

	Status status;
	DbRecordSet recLists;
	_SERVER_CALL(StoredProjectLists->GetListNames(status, recLists /*,g_pClientManager->GetPersonID()*/)) //Project grid id
	_CHK_ERR(status);

	//Fill combo.
	int nRowCount = recLists.getRowCount();
	if(nRowCount > 0)
	{
		//empty list entry
		ui.cboDefaultStoredList->addItem(tr("(none)"), 0);

		for(int i = 0; i < nRowCount; i++)
		{
			int ViewID = recLists.getDataRef(i, "BSPL_ID").toInt();
			QString strViewName = recLists.getDataRef(i, "BSPL_NAME").toString();
			ui.cboDefaultStoredList->addItem(strViewName, ViewID);
		}
	}
}
