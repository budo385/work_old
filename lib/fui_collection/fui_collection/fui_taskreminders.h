#ifndef FUI_TaskReminders_H
#define FUI_TaskReminders_H

#include <QWidget>
#include "fuibase.h"
#include "generatedfiles/ui_FUI_TaskReminders.h"

class FUI_TaskReminders : public FuiBase
{
	Q_OBJECT

public:
	FUI_TaskReminders(QWidget *parent = 0);
	~FUI_TaskReminders();

	void SetCurrentFUI(bool bCurrent);
	bool HideVisibleFramesIfNeeded(int nFrameOpened /* 0 -ACL, 1 -AC, 2-DP */){return true;};
	
	//bool sidebar_fui(){return true;};
	//void Setsidebar_fui(bool bSet){};

protected:
	QFrame* GetFrameCommToolBar();
	QFrame* GetFrameCommParent();
	QLabel* GetFrameCommLabel();
	DropZone_MenuWidget* GetDropZone_MenuWidget();
	void GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strMail,int &nEmailReplyID,QString &strPhone,QString &strInternet);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

private:
	Ui::FUI_TaskRemindersClass ui;
};

#endif // FUI_TaskReminders_H
