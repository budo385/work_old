#ifndef STARTUPWIZARD_H
#define STARTUPWIZARD_H


class StartupWizard 
{

public:
	StartupWizard();
	~StartupWizard();


	static bool StartWizard();
	static void ResetOnStartUpFlag();
	static void StartImportWizard();


private:

	
};

#endif // STARTUPWIZARD_H
