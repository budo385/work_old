#include "commgridfilter.h"

#include <QHeaderView>
#include <QTableWidgetItem>
#include <QPainter>
#include <QPicture>
#include "report_core/report_core/reportlabel.h"
#include "bus_client/bus_client/documenthelper.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/commfiltersettingsids.h"
#include "bus_core/bus_core/commgridfiltersettings.h"
#include "commworkbenchwidget.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "bus_client/bus_client/selection_combobox.h"
#include "bus_core/bus_core/contacttypemanager.h"

#define _IS_CHECKED(X) (X ? Qt::Checked : Qt::Unchecked)

LastColumnSpinBox::LastColumnSpinBox(int nSettingID, QWidget *parent /*= 0*/)
{
	m_nSettingID = nSettingID;
	setMinimum(1);
	setMaximum(1000);
	connect(this, SIGNAL(editingFinished ()), this, SLOT(on_editingFinished()));
}

void LastColumnSpinBox::on_editingFinished()
{
	emit SpinBoxValueChanged(m_nSettingID, value());
}

CommGridFilterCheckBox::CommGridFilterCheckBox(int nSettingID, QWidget *parent /*= 0*/)
: QCheckBox(parent)
{
	m_nSettingID = nSettingID;
	connect(this, SIGNAL(stateChanged(int)), this, SLOT(on_stateChanged(int)));
}

void CommGridFilterCheckBox::on_stateChanged(int nState)
{
	emit ChBoxStateChanged(m_nSettingID, nState);
}

CommGridFilter::CommGridFilter(int nFilterType, QWidget *parent /*= 0*/)
	: FuiBase(parent)
{
	Initialize(nFilterType, false, parent);
}


CommGridFilter::CommGridFilter(int nFilterType, bool bLoadFromWizard, QWidget *parent)
	: FuiBase(parent)
{
	Initialize(nFilterType, bLoadFromWizard, parent);
}

void CommGridFilter::Initialize(int nFilterType, bool bLoadFromWizard, QWidget *parent)
{
	ui.setupUi(this);

	if (m_nLoggedPersonID<=0)
		m_nLoggedPersonID = g_pClientManager->GetPersonID();	

	ui.treeWidgetApp->header()->hide();
	ui.viewName_comboBox->setEditable(false);

	m_bLoadFromWizard = bLoadFromWizard;

	SetMode(MODE_READ);

	//If filter loaded from wizard hide buttons bar.
	if (bLoadFromWizard)
	{
		ui.button_bar->hide();
		ui.public_checkBox->setDisabled(true);
		ui.sortCode_lineEdit->setDisabled(true);
	}

	//Locals.
	m_nEntityID = nFilterType;
	m_recNewView.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW));
	m_recFilterViewData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
	//Get view grid type - get views for needed grid (desktop, contact or project).
	if (m_nEntityID == COMM_GRID_FILTER_WIDGET_DESKTOP)
		m_nGridType = 0;
	else if (m_nEntityID == COMM_GRID_FILTER_WIDGET_CONTACT)
	{
		m_nGridType = 1;
		ui.openOnStartup_checkBox->hide();
		ui.tabWidget->removeTab(0);
	}
	else if (m_nEntityID == COMM_GRID_FILTER_WIDGET_PROJECT)
	{
		m_nGridType = 2;
		ui.openOnStartup_checkBox->hide();
		ui.tabWidget->removeTab(0);
	}
	else if (m_nEntityID == COMM_GRID_FILTER_WIDGET_CALENDAR)
	{
		m_nGridType = 2;
		ui.top_frame->hide();
		ui.tabWidget->removeTab(2);
		ui.tabWidget->removeTab(1);
		ui.tabWidget->removeTab(0);
		ui.button_bar->SetButtonVisible(MainCommandBar::BUTTON_EDIT, false);
		ui.button_bar->SetButtonVisible(MainCommandBar::BUTTON_INSERT, false);
		ui.button_bar->SetButtonVisible(MainCommandBar::BUTTON_DELETE, false);
		ui.button_bar->SetButtonVisible(MainCommandBar::BUTTON_COPY, false);
	}

	InitFui(nFilterType, NULL, NULL, ui.button_bar);
	ui.button_bar->RegisterFUI(this);

	//Setup complex filter table widget.
	SetupTableLayout();

	//Setup CE type filter frame.
	SetupCETypeFilter();
	//Setup Taks type filter frame.
	SetupTaskTypeFilter();
	//Setup Sort Tab.
	SetupSortTab();

	//If not desktop filter then disable check box grid.
	//if (!(m_nEntityID == COMM_GRID_FILTER_WIDGET_DESKTOP))
	//	ui.tabWidget->removeTab(0);

	//Fill application tree data.
	FillAppTree();

	ConnectSignals();

	//Connect date range selector to it's slot.
	connect(ui.dateRange_frame,		SIGNAL(onDateChanged(QDateTime &, QDateTime &, int)), this,	SLOT(on_DateChanged(QDateTime &, QDateTime &, int)));
	connect(ui.TypeFilter_frame, SIGNAL(ItemCheckStateChanged(QList<QTreeWidgetItem*>)), this, SLOT(on_TypeFilter_frame_ItemCheckStateChanged(QList<QTreeWidgetItem*>)));
	
	SetFUIStyle();

	//Set OK to Apply.
	ui.button_bar->GetButton(MainCommandBar::BUTTON_OK)->setText(tr("Apply"));
}

CommGridFilter::~CommGridFilter()
{

}

void CommGridFilter::SetCurrentView(int nViewID, DbRecordSet recFilterViewData, int nPersonID, bool bLoadViewSelector /*= true*/)
{
	//Disconnect signals.
	DisconnectValueChangedSignal();
	
	//Set local.
	m_nLoggedPersonID	= nPersonID;
	m_nViewID			= nViewID;
	m_recFilterViewData = recFilterViewData;

	//View selector combo box.
	if (bLoadViewSelector)
		LoadViewSelector(nViewID);
	//Filter data.
	GetFilterDataFromServer();
	
	//If existing view passed - just set data to gui.
	if (ui.viewName_comboBox->findData(m_nViewID) < 0)
	{
		//If some filter data is given then show it.
		if (recFilterViewData.getRowCount())
			m_recFilterViewData = recFilterViewData;
		else //Fill default values.
			FillInsertDefaultValues(m_recFilterViewData);	//Just fill recordsets.
	}

	//Show filter data to gui.
	SetViewDataToGui();
	SetFilterDataToGui();
	
	//Set items of check boxes filter.
	SetTableItems();

	//Enable-disable delete button.
	EnableDisableDeleteButton();

	//Set sort items.
	ui.sort_frame->SetSortColumnSetup(m_lstSortRecordSet);

	//Setup CE type filter.
	int nCEComboIndex = GetFilterIntValue(m_recFilterViewData, SELECTED_COMBO_CE_ENTITY_TYPE_FILTER);
	ui.TypeFilter_comboBox->setCurrentIndex(nCEComboIndex);
	SetupCETypeFilterFromSavedData();

	//Connect changing signals.
	ConnectValueChangedSignal();
}

//get FUI name:
QString CommGridFilter::GetFUIName()
{
	QString strName = tr("Modify Filters");
	return strName;
}

void CommGridFilter::on_cmdInsert()
{
	SetMode(MODE_INSERT);
	int nNextSortCode = ui.viewName_comboBox->count() + 1;
	ui.viewName_comboBox->blockSignals(true);
	ui.viewName_comboBox->clear();
	ui.viewName_comboBox->setEditable(true);
	ui.viewName_comboBox->blockSignals(false);
	ui.openOnStartup_checkBox->setChecked(false);
	ui.public_checkBox->setChecked(false);
	ui.sortCode_lineEdit->setText(QVariant(nNextSortCode).toString());
	DbRecordSet tmp;
	SetCurrentView(-1, tmp, m_nLoggedPersonID, false);
	//Set Apply to OK.
	ui.button_bar->GetButton(MainCommandBar::BUTTON_OK)->setText("OK");

}

bool CommGridFilter::on_cmdEdit()
{
	ui.viewName_comboBox->setEditable(true);
	SetMode(MODE_EDIT);
	QString strItemText = ui.viewName_comboBox->itemText(ui.viewName_comboBox->currentIndex());
	m_nViewID = ui.viewName_comboBox->itemData(ui.viewName_comboBox->currentIndex()).toInt();

	ui.viewName_comboBox->blockSignals(true);
	ui.viewName_comboBox->clear();
	ui.viewName_comboBox->addItem(strItemText, m_nViewID);

	//Set Apply to OK.
	ui.button_bar->GetButton(MainCommandBar::BUTTON_OK)->setText("OK");

	if (!m_nViewID)
		m_recFilterViewData.setColValue(0, QVariant(QVariant::Int));
	ui.viewName_comboBox->blockSignals(false);
	return true;
}

bool CommGridFilter::on_cmdDelete()
{
	if (QMessageBox::warning(NULL, tr("Delete Filter"), tr("Are you sure you want to delete the selected filter?"), QMessageBox::Ok, QMessageBox::Cancel) == QMessageBox::Cancel)
		return false;

	DbRecordSet recDelete = m_recFilterViews.getRow(m_recFilterViews.find(0, m_nViewID, true));
	Status status;
	QString pLockResourceID;
	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	_SERVER_CALL(ClientSimpleORM->Delete(status, BUS_COMM_VIEW, recDelete, pLockResourceID, lstStatusRows, pBoolTransaction))
	_CHK_ERR_NO_RET(status);
	if (!status.IsOK()) return false;

	DbRecordSet tmp;
	SetCurrentView(0, tmp, m_nLoggedPersonID);
	//on_cmdCancel();
	return true;
}

void CommGridFilter::on_cmdCopy()
{
	SetMode(MODE_INSERT);
	m_recFilterViewData.setColValue(0, QVariant(QVariant::Int));	//Set ID values to null.
	//_DUMP(m_recFilterViewData);
	QString strNewViewName = ui.viewName_comboBox->currentText() + "_copy";
	SetCurrentView(-1, m_recFilterViewData, m_nLoggedPersonID);
	ui.viewName_comboBox->blockSignals(true);
	ui.viewName_comboBox->clear();
	ui.viewName_comboBox->setEditable(true);
	ui.viewName_comboBox->setEditText(strNewViewName);
	ui.viewName_comboBox->blockSignals(false);
	
	//Set Apply to OK.
	ui.button_bar->GetButton(MainCommandBar::BUTTON_OK)->setText("OK");
}

bool CommGridFilter::on_cmdOK()
{
	//Returned ID.
	int nViewID;
	GetFilterDataFromGui();
	if (GetMode() == MODE_INSERT || GetMode() == MODE_EDIT)
	{
		if (!GetViewDataFromGui())
		{
			QMessageBox::warning(NULL, "Filter Name", tr("Please insert filter name!"));
			ui.viewName_comboBox->setFocus(Qt::OtherFocusReason);
			return false;
		}

		Status status;
		bool bAppTypeActive	= m_recFilterViewData.getDataRef(m_recFilterViewData.find("BUSCS_SETTING_ID", FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE, true), "BUSCS_VALUE").toBool();
		_SERVER_CALL(BusCommunication->SaveCommFilterViews(status, nViewID, m_recNewView, m_recFilterViewData))
		_CHK_ERR_NO_RET(status);
		if (!status.IsOK()) return false;

		SetCurrentView(nViewID, m_recFilterViewData, m_nLoggedPersonID);
		SetMode(MODE_READ);

		//Set OK to Apply.
		ui.button_bar->GetButton(MainCommandBar::BUTTON_OK)->setText("Apply");
		ui.button_bar->SetButtonState(MainCommandBar::BUTTON_OK, true);
		ui.button_bar->SetButtonState(MainCommandBar::BUTTON_CANCEL, true);
		ui.viewName_comboBox->setEditable(false);
		return true;
	}
	else if (GetMode() == MODE_READ)
		nViewID = ui.viewName_comboBox->itemData(ui.viewName_comboBox->currentIndex()).toInt();
	
	//Emit set filter signal.
	emit SetFilter(nViewID, m_recNewView, m_recFilterViewData);
	//_DUMP(m_recFilterViewData);

	on_cmdCancel();
	return true;
}

void CommGridFilter::on_cmdCancel()
{
	if (GetMode() == MODE_INSERT || GetMode() == MODE_EDIT)
	{
		SetMode(MODE_READ);

		ui.viewName_comboBox->setEditable(false);
		DbRecordSet tmp;
		SetCurrentView(m_nViewID, tmp, m_nLoggedPersonID);

/*		ui.viewName_comboBox->blockSignals(true);
		//Set current item.
		ui.viewName_comboBox->setCurrentIndex(ui.viewName_comboBox->findData(m_nViewID));
		ui.viewName_comboBox->blockSignals(false);

		GetFilterDataFromServer();

		//Show filter data to gui.
		SetViewDataToGui();
		SetFilterDataToGui();
		
		//Set items of check boxes filter.
		SetTableItems();

		//Enable-disable delete button.
		EnableDisableDeleteButton();
*/		
		//Set OK to Apply.
		ui.button_bar->GetButton(MainCommandBar::BUTTON_OK)->setText("Apply");
		ui.button_bar->SetButtonState(MainCommandBar::BUTTON_OK, true);
		ui.button_bar->SetButtonState(MainCommandBar::BUTTON_CANCEL, true);
		return;
	}

	this->close();
}

void CommGridFilter::SetMode(int nMode)
{
	if (nMode == MODE_EDIT || nMode == MODE_INSERT)
		EnableViewChBoxes(true);
	else
		EnableViewChBoxes(false);

	FuiBase::SetMode(nMode);
}

void CommGridFilter::SetDefaultValues()
{
	//Set default today date-time - today date at 23:59.
	QDateTime datTodayDateTime = QDateTime::currentDateTime();
	QTime timeToday(23, 59, 59);
	datTodayDateTime.setTime(timeToday);
	//Set default one week ago date-time - at 00:00.
	QDate lastWeekDate = QDate::currentDate().addDays(-7);
	QTime timeLastWeek(00, 00);
	QDateTime datWeekAgoDateTime(lastWeekDate, timeLastWeek);
	
	bool bViewSelGrid	= GetFilterBoolValue(m_recFilterViewData, VIEW_SELECTION_GRID_ACTIVE);
	bool bDateFilterUsed= GetFilterBoolValue(m_recFilterViewData, FILTER_BY_DATE_ACTIVE);
	bool bDocTypeActive	= GetFilterBoolValue(m_recFilterViewData, FILTER_BY_ENTITY_TYPE_ACTIVE);
	bool bAppTypeActive	= GetFilterBoolValue(m_recFilterViewData, FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE);
	bool bSearchActive	= GetFilterBoolValue(m_recFilterViewData, FILTER_SEARCH_TEXT_ACTIVE);
	bool bDocFilter		= GetFilterBoolValue(m_recFilterViewData, SHOW_DOCUMENTS);
	bool bEmailsFilter	= GetFilterBoolValue(m_recFilterViewData, SHOW_EMAILS);
	bool bVoiceCFilter	= GetFilterBoolValue(m_recFilterViewData, SHOW_VOICE_CALLS);
	bool bApplicationType = GetFilterBoolValue(m_recFilterViewData, FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE);
	bool bSearchName	= GetFilterBoolValue(m_recFilterViewData, SEARCH_IN_NAME);
	bool bSearchDescr	= GetFilterBoolValue(m_recFilterViewData, SEARCH_IN_DESCRIPTION);
	QString strSearchTxt= GetFilterStringValue(m_recFilterViewData, SEARCH_TEXT);

	ui.ViewSelection_groupBox->setChecked(_IS_CHECKED(bViewSelGrid));
	ui.Date_groupBox->setChecked(_IS_CHECKED(bDateFilterUsed));
	ui.dateRange_frame->Initialize(datTodayDateTime, datWeekAgoDateTime, 0);
	ui.documentType_groupBox->setChecked(_IS_CHECKED(bDocTypeActive));
	ui.application_groupBox->setChecked(_IS_CHECKED(bAppTypeActive));
	ui.searchText_groupBox->setChecked(_IS_CHECKED(bSearchActive));

	//Filter by entity type check boxes.
	ui.documents_checkBox->setChecked(_IS_CHECKED(bDocFilter));
	ui.emails_checkBox->setChecked(_IS_CHECKED(bEmailsFilter));
	ui.voiceCalls_checkBox->setChecked(_IS_CHECKED(bVoiceCFilter));
	
	//Filter by application type. 
	ui.FilterByType_groupBox->setChecked(_IS_CHECKED(bApplicationType));

	ui.inName_checkBox->setChecked(_IS_CHECKED(bVoiceCFilter));
	ui.inDescription_checkBox->setChecked(_IS_CHECKED(bVoiceCFilter));
	ui.searchTextEdit->setText(strSearchTxt);
}

void CommGridFilter::LoadViewSelector(int nViewID /*= -1*/, int nPersonID /*= -1*/)
{
	ui.viewName_comboBox->blockSignals(true);
	ui.viewName_comboBox->clear();

	Status status;
	if (nPersonID != -1)
		m_nLoggedPersonID = nPersonID;

	_SERVER_CALL(BusCommunication->GetCommFilterViews(status, m_recFilterViews, m_nLoggedPersonID, m_nGridType))
	_CHK_ERR(status);

	//Fill combo.
	int nRowCount = m_recFilterViews.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		int ViewID = m_recFilterViews.getDataRef(i, "BUSCV_ID").toInt();
		QString strViewName = m_recFilterViews.getDataRef(i, "BUSCV_NAME").toString();
		ui.viewName_comboBox->addItem(strViewName, ViewID);
	}

	//Set current item.
	ui.viewName_comboBox->setCurrentIndex(ui.viewName_comboBox->findData(nViewID));

	ui.viewName_comboBox->blockSignals(false);
}

void CommGridFilter::GetViewAndfilterData(int &nViewID, DbRecordSet &recFilterData)
{
	nViewID = m_nViewID;
	GetFilterDataFromGui();
	recFilterData = m_recFilterViewData;
}

void CommGridFilter::SetupTableLayout()
{
	//Table header.
	WriteToComplexFilterHeader();

	ui.ComplexTableHeader_label->setMaximumWidth(607);
	ui.ComplexTableHeader_label->setMinimumWidth(607);
	ui.ComplexTableHeader_label->setMaximumHeight(80);
	ui.ComplexTableHeader_label->setMinimumHeight(80);

	//Table dimensions.
	ui.ComplexFilter_tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
	ui.ComplexFilter_tableWidget->setMaximumWidth(590);
	ui.ComplexFilter_tableWidget->setMinimumWidth(590);
	ui.ComplexFilter_tableWidget->setMaximumHeight(75);
	ui.ComplexFilter_tableWidget->setMinimumHeight(75);

	//Row and column count.
	ui.ComplexFilter_tableWidget->setColumnCount(10);
	ui.ComplexFilter_tableWidget->setRowCount(3);

	//Horizontal header look and feel.
	ui.ComplexFilter_tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(0,90);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(1,60);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(2,60);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(3,60);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(4,60);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(5,60);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(6,60);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(7,60);
	ui.ComplexFilter_tableWidget->horizontalHeader()->resizeSection(8,80);
	ui.ComplexFilter_tableWidget->horizontalHeader()->hide();

	//Vertical header look and feel.
	ui.ComplexFilter_tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Interactive);
	ui.ComplexFilter_tableWidget->verticalHeader()->resizeSection(0,25);
	ui.ComplexFilter_tableWidget->verticalHeader()->resizeSection(1,25);
	ui.ComplexFilter_tableWidget->verticalHeader()->resizeSection(2,25);
	ui.ComplexFilter_tableWidget->verticalHeader()->hide();
}

void CommGridFilter::SetTableItems()
{
	//Clear table.
	ui.ComplexFilter_tableWidget->clear();

	//Locals.
	int nUnassignedVoiceCalls	= 0;
	int nMissedVoiceCalls		= 0;
	int nScheduledVoiceCalls	= 0;
	int nDueVoiceCalls			= 0;
	int nOverdueVoiceCalls		= 0;
	int nUnreadEmails			= 0;
	int nUnassignedEmails		= 0;
	int nScheduledEmails		= 0;
	int nDueEmails				= 0;
	int nOverdueEmails			= 0;
	int nTemplatesEmails		= 0;
	int nCheckedOutDocuments	= 0;
	int nUnassignedDocuments	= 0;
	int nScheduledDocuments		= 0;
	int nDueDocuments			= 0;
	int nOverdueDocuments		= 0;
	int nTemplatesDocuments		= 0;

	//_DUMP(m_recFilterData);

	if (m_recFilterData.getRowCount())
	{
		//VOICE CALLS
		nUnassignedVoiceCalls = m_recFilterData.getDataRef(0, "UNASSIGNEDVOICECALLS").toInt();
		nMissedVoiceCalls	  = m_recFilterData.getDataRef(0, "MISSEDVOICECALLS").toInt();
		nScheduledVoiceCalls  = m_recFilterData.getDataRef(0, "SCHEDULEDVOICECALLS").toInt();
		nDueVoiceCalls		  = m_recFilterData.getDataRef(0, "DUEVOICECALLS").toInt();
		nOverdueVoiceCalls	  = m_recFilterData.getDataRef(0, "OVERDUEVOICECALLS").toInt();
		//EMAILS
		nUnreadEmails		  = m_recFilterData.getDataRef(0, "UNREADEMAILS").toInt();
		nUnassignedEmails	  = m_recFilterData.getDataRef(0, "UNASSIGNEDEMAILS").toInt();
		nScheduledEmails	  = m_recFilterData.getDataRef(0, "SCHEDULEDEMAILS").toInt();
		nDueEmails			  = m_recFilterData.getDataRef(0, "DUEEMAILS").toInt();
		nOverdueEmails		  = m_recFilterData.getDataRef(0, "OVERDUEEMAILS").toInt();
		nTemplatesEmails	  = m_recFilterData.getDataRef(0, "TEMPLATESEMAILS").toInt();
		//DOCUMENTS
		nCheckedOutDocuments  = m_recFilterData.getDataRef(0, "CHECKEDOUTDOCUMENTS").toInt();
		nUnassignedDocuments  = m_recFilterData.getDataRef(0, "UNASSIGNEDDOCUMENTS").toInt();
		nScheduledDocuments	  = m_recFilterData.getDataRef(0, "SCHEDULEDDOCUMENTS").toInt();
		nDueDocuments		  = m_recFilterData.getDataRef(0, "DUEDOCUMENTS").toInt();
		nOverdueDocuments	  = m_recFilterData.getDataRef(0, "OVERDUEDOCUMENTS").toInt();
		nTemplatesDocuments   = m_recFilterData.getDataRef(0, "TEMPLATESDOCUMENTS").toInt();
	}

	//Table items.
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			QTableWidgetItem *item = new QTableWidgetItem;
			ui.ComplexFilter_tableWidget->setItem(j, i, item);
		}
	}

	//First column push button.
	QPushButton *HideAll1_pushButton = new QPushButton(tr("Hide All"));
	QPushButton *HideAll2_pushButton = new QPushButton(tr("Hide All"));
	QPushButton *HideAll3_pushButton = new QPushButton(tr("Hide All"));
	connect(HideAll1_pushButton, SIGNAL(clicked(bool)), this, SLOT(HideAllFirstRow(bool)));
	connect(HideAll2_pushButton, SIGNAL(clicked(bool)), this, SLOT(HideAllSecondRow(bool)));
	connect(HideAll3_pushButton, SIGNAL(clicked(bool)), this, SLOT(HideAllThirdRow(bool)));
	ui.ComplexFilter_tableWidget->setCellWidget(0, 0, HideAll1_pushButton);
	ui.ComplexFilter_tableWidget->setCellWidget(1, 0, HideAll2_pushButton);
	ui.ComplexFilter_tableWidget->setCellWidget(2, 0, HideAll3_pushButton);

	//Unread, unassigned, missed.
	SetColumnColor(1, QColor(219,255,255));
	SetColumnColor(2, QColor(219,255,255));
	SetColumnColor(3, QColor(219,255,255));
	//Scheduled, due, overdue.
	SetColumnColor(4, QColor(254,255,237));
	SetColumnColor(5, QColor(254,255,237));
	SetColumnColor(6, QColor(254,255,237));
	//Templates.
	SetColumnColor(7, QColor(255,238,238,255));
	//Last.
	SetColumnColor(8, QColor(238,255,237,255));

	//Unread column.
	ui.ComplexFilter_tableWidget->setCellWidget(1, 1, CreateCheckBox(SHOW_EMAILS_UNREAD, GetFilterIntValue(m_recFilterViewData, SHOW_EMAILS_UNREAD), nUnreadEmails));
	//Unassigned.
	ui.ComplexFilter_tableWidget->setCellWidget(0, 2, CreateCheckBox(SHOW_VOICE_CALLS_UNASSIGNED, GetFilterIntValue(m_recFilterViewData, SHOW_VOICE_CALLS_UNASSIGNED), nUnassignedVoiceCalls));
	ui.ComplexFilter_tableWidget->setCellWidget(1, 2, CreateCheckBox(SHOW_EMAILS_UNASSIGNED, GetFilterIntValue(m_recFilterViewData, SHOW_EMAILS_UNASSIGNED), nUnassignedEmails));
	ui.ComplexFilter_tableWidget->setCellWidget(2, 2, CreateCheckBox(DOCUMENTS_UNASSIGNED, GetFilterIntValue(m_recFilterViewData, DOCUMENTS_UNASSIGNED), nUnassignedDocuments));
	//Missed.
	ui.ComplexFilter_tableWidget->setCellWidget(0, 3, CreateCheckBox(SHOW_VOICE_CALLS_MISSED, GetFilterIntValue(m_recFilterViewData, SHOW_VOICE_CALLS_MISSED), nMissedVoiceCalls));
	//Scheduled.
	ui.ComplexFilter_tableWidget->setCellWidget(0, 4, CreateCheckBox(SHOW_VOICE_CALLS_SCHEDULED, GetFilterIntValue(m_recFilterViewData, SHOW_VOICE_CALLS_SCHEDULED), nScheduledVoiceCalls));
	ui.ComplexFilter_tableWidget->setCellWidget(1, 4, CreateCheckBox(SHOW_EMAILS_SCHEDULED, GetFilterIntValue(m_recFilterViewData, SHOW_EMAILS_SCHEDULED), nScheduledEmails));
	ui.ComplexFilter_tableWidget->setCellWidget(2, 4, CreateCheckBox(DOCUMENTS_SCHEDULED, GetFilterIntValue(m_recFilterViewData, DOCUMENTS_SCHEDULED), nScheduledDocuments));
	//Due.
	ui.ComplexFilter_tableWidget->setCellWidget(0, 5, CreateCheckBox(SHOW_VOICE_CALLS_DUE, GetFilterIntValue(m_recFilterViewData, SHOW_VOICE_CALLS_DUE), nDueVoiceCalls));
	ui.ComplexFilter_tableWidget->setCellWidget(1, 5, CreateCheckBox(SHOW_EMAILS_DUE, GetFilterIntValue(m_recFilterViewData, SHOW_EMAILS_DUE), nDueEmails));
	ui.ComplexFilter_tableWidget->setCellWidget(2, 5, CreateCheckBox(DOCUMENTS_DUE, GetFilterIntValue(m_recFilterViewData, DOCUMENTS_DUE), nDueDocuments));
	//Overdue.
	ui.ComplexFilter_tableWidget->setCellWidget(0, 6, CreateCheckBox(SHOW_VOICE_CALLS_OVERDUE, GetFilterIntValue(m_recFilterViewData, SHOW_VOICE_CALLS_OVERDUE), nOverdueVoiceCalls));
	ui.ComplexFilter_tableWidget->setCellWidget(1, 6, CreateCheckBox(SHOW_EMAILS_OVERDUE, GetFilterIntValue(m_recFilterViewData, SHOW_EMAILS_OVERDUE), nOverdueEmails));
	ui.ComplexFilter_tableWidget->setCellWidget(2, 6, CreateCheckBox(DOCUMENTS_OVERDUE, GetFilterIntValue(m_recFilterViewData, DOCUMENTS_OVERDUE), nOverdueDocuments));
	//Templates.
	ui.ComplexFilter_tableWidget->setCellWidget(1, 7, CreateCheckBox(SHOW_EMAILS_TEMPLATES, GetFilterIntValue(m_recFilterViewData, SHOW_EMAILS_TEMPLATES), nTemplatesEmails));
	ui.ComplexFilter_tableWidget->setCellWidget(2, 7, CreateCheckBox(DOCUMENTS_TEMPLATES, GetFilterIntValue(m_recFilterViewData, DOCUMENTS_TEMPLATES), nTemplatesDocuments));
	//Last.
	CommGridFilterCheckBox *cBox0 = CreateLastCheckBox(SHOW_VOICE_CALLS_LAST, GetFilterIntValue(m_recFilterViewData, SHOW_VOICE_CALLS_LAST));
	CommGridFilterCheckBox *cBox1 = CreateLastCheckBox(SHOW_EMAILS_LAST, GetFilterIntValue(m_recFilterViewData, SHOW_EMAILS_LAST));
	CommGridFilterCheckBox *cBox2 = CreateLastCheckBox(DOCUMENTS_LAST, GetFilterIntValue(m_recFilterViewData, DOCUMENTS_LAST));

	LastColumnSpinBox  *sBox0 = CreateLastSpinBox(LAST_VOICE_CALLS_NUMBER, GetFilterIntValue(m_recFilterViewData, LAST_VOICE_CALLS_NUMBER));
	LastColumnSpinBox  *sBox1 = CreateLastSpinBox(LAST_EMAILS_NUMBER, GetFilterIntValue(m_recFilterViewData, LAST_EMAILS_NUMBER));
	LastColumnSpinBox  *sBox2 = CreateLastSpinBox(LAST_DOCUMENTS_NUMBER, GetFilterIntValue(m_recFilterViewData, LAST_DOCUMENTS_NUMBER));

	ui.ComplexFilter_tableWidget->setCellWidget(0, 8, CreateLastColumnWidget(cBox0, sBox0));
	ui.ComplexFilter_tableWidget->setCellWidget(1, 8, CreateLastColumnWidget(cBox1, sBox1));
	ui.ComplexFilter_tableWidget->setCellWidget(2, 8, CreateLastColumnWidget(cBox2, sBox2));
}

void CommGridFilter::SetColumnColor(int nColumn, QColor color)
{
	QTableWidgetItem *pItem = ui.ComplexFilter_tableWidget->item(0, nColumn);
	pItem->setBackground(QBrush(color));
	pItem = ui.ComplexFilter_tableWidget->item(1, nColumn);
	pItem->setBackground(QBrush(color));
	pItem = ui.ComplexFilter_tableWidget->item(2, nColumn);
	pItem->setBackground(QBrush(color));
}

QWidget* CommGridFilter::CreateCheckBox(int nSettingID, int nState, int nValue)
{
	CommGridFilterCheckBox *cBox = new CommGridFilterCheckBox(nSettingID);
	if (!nValue)
		cBox->setText("0");
	else
		cBox->setText(QVariant(nValue).toString());

	cBox->setChecked(QVariant(nState).toBool());
	//Connect signal to slot.
	connect(cBox, SIGNAL(ChBoxStateChanged(int, int)), this, SLOT(on_CheckBoxStateChanged(int, int)));
	//Put check box in hash.
	m_hshCheckBoxes.insert(nSettingID, cBox);

	QWidget *widget		= new QWidget();
	QHBoxLayout *layout = new QHBoxLayout(widget);
	layout->setMargin(0);
	layout->setSpacing(0);
	layout->addSpacing(5);
	layout->addWidget(cBox);
	layout->addSpacing(5);

	return widget;
}

CommGridFilterCheckBox* CommGridFilter::CreateLastCheckBox(int nSettingID, int nState)
{
	CommGridFilterCheckBox *cBox = new CommGridFilterCheckBox(nSettingID);
	cBox->setChecked(QVariant(nState).toBool());
	//Connect signal to slot.
	connect(cBox, SIGNAL(ChBoxStateChanged(int, int)), this, SLOT(on_CheckBoxStateChanged(int, int)));
	//Put check box in hash.
	m_hshCheckBoxes.insert(nSettingID, cBox);

	return cBox;
}

LastColumnSpinBox* CommGridFilter::CreateLastSpinBox(int nSettingID, int nValue)
{
	LastColumnSpinBox *sBox = new LastColumnSpinBox(nSettingID);
	sBox->setValue(nValue);
	connect(sBox, SIGNAL(SpinBoxValueChanged(int, int)), this, SLOT(on_spinBox_editingFinished(int, int)));
	m_hshSpinBoxes.insert(nSettingID, sBox);

	return sBox;
}

QWidget* CommGridFilter::CreateLastColumnWidget(CommGridFilterCheckBox *cBox, LastColumnSpinBox *sBox)
{
	QWidget *widget		= new QWidget();
	QHBoxLayout *layout = new QHBoxLayout(widget);
	layout->setMargin(0);
	layout->setSpacing(0);
	layout->addSpacing(5);
	layout->addWidget(cBox);
	layout->addWidget(sBox);
	layout->addSpacing(5);

	return widget;
}

void CommGridFilter::WriteToComplexFilterHeader()
{
	if(g_pClientManager->GetIniFile()->m_strLangCode == "de")
	{
		QPixmap pixDe(":CE_View_Header3_Ger.png");
		ui.ComplexTableHeader_label->setPixmap(pixDe);
	}
	else
	{
		QPixmap pixEn(":CE_View_Header.png");
		ui.ComplexTableHeader_label->setPixmap(pixEn);
	}
}

void CommGridFilter::GetFilterDataFromServer()
{
	//Get complex filter data.
	Status status;
	_SERVER_CALL(BusCommunication->GetCommFilterData(status,m_lstSortRecordSet,
															m_recFilterData		/*Ch. boxes values*/, 
															m_recFilterViewData	/*Other check boxes, etc.*/, 
															m_nViewID, m_nLoggedPersonID))
	_CHK_ERR(status);
}

void CommGridFilter::EnableDisableDeleteButton()
{
	//Set delete button enabled if there is something to delete.
	if (ui.viewName_comboBox->currentText().isEmpty() || GetMode() == MODE_INSERT)
		ui.button_bar->GetButton(MainCommandBar::BUTTON_DELETE)->setEnabled(false);
	else
		ui.button_bar->GetButton(MainCommandBar::BUTTON_DELETE)->setEnabled(true);
}

void CommGridFilter::FillInsertDefaultValues(DbRecordSet &recFilterSettings)
{
	//Fill default recordset data.
	recFilterSettings.clear();
	CommGridFilterSettings filterSettings;
	recFilterSettings = filterSettings.GetCommFilterSettings();

	//Default dates.
	recFilterSettings.setData(recFilterSettings.find("BUSCS_SETTING_ID", FROM_DATE, true), "BUSCS_VALUE_DATETIME", QDateTime::currentDateTime());
	recFilterSettings.setData(recFilterSettings.find("BUSCS_SETTING_ID", TO_DATE, true), "BUSCS_VALUE_DATETIME", QDateTime::currentDateTime());


	if (m_nEntityID == COMM_GRID_FILTER_WIDGET_CONTACT)
	{
		recFilterSettings.setData(recFilterSettings.find("BUSCS_SETTING_ID", FILTER_BY_ENTITY_TYPE_ACTIVE, true), "BUSCS_VALUE", 1);
	}
	else if (m_nEntityID == COMM_GRID_FILTER_WIDGET_PROJECT)
	{
		recFilterSettings.setData(recFilterSettings.find("BUSCS_SETTING_ID", FILTER_BY_ENTITY_TYPE_ACTIVE, true), "BUSCS_VALUE", 1);
	}
}

bool CommGridFilter::GetViewDataFromGui()
{
	m_recNewView.clear();
	m_recNewView.addRow();
	
	QString strFilterName = ui.viewName_comboBox->currentText();
	if(strFilterName.isEmpty())
		return false;

	//Get current view ID. If it is not 0, then it is an modified item.
	int nViewID = ui.viewName_comboBox->itemData(ui.viewName_comboBox->currentIndex()).toInt();
	if((nViewID > 0) && (GetMode() == MODE_EDIT))
		m_recNewView.setData(0, "BUSCV_ID", nViewID); //This will cause update on server side.

	bool bOpenOnStartup = ui.openOnStartup_checkBox->isChecked();
	bool bPublic		= ui.public_checkBox->isChecked();
	
	m_recNewView.setData(0, "BUSCV_NAME", strFilterName);
	m_recNewView.setData(0, "BUSCV_OPEN_ON_STARTUP", QVariant(bOpenOnStartup).toInt());
	m_recNewView.setData(0, "BUSCV_IS_PUBLIC", QVariant(bPublic).toInt());
	m_recNewView.setData(0, "BUSCV_OWNER_ID", m_nLoggedPersonID);
	//Set view type.
	if (m_nEntityID == COMM_GRID_FILTER_WIDGET_DESKTOP)
		m_recNewView.setData(0, "BUSCV_TYPE", 0);
	else if (m_nEntityID == COMM_GRID_FILTER_WIDGET_CONTACT)
		m_recNewView.setData(0, "BUSCV_TYPE", 1);
	else if (m_nEntityID == COMM_GRID_FILTER_WIDGET_PROJECT)
		m_recNewView.setData(0, "BUSCV_TYPE", 2);
	//Set sort code.
	m_recNewView.setData(0, "BUSCV_SORT_CODE", ui.sortCode_lineEdit->text());

	//Set sort data recordset from commworkbenchwidget.
	DbRecordSet row = ui.sort_frame->GetResultRecordSet();
	QByteArray bytGridSortData = XmlUtil::ConvertRecordSet2ByteArray_Fast(row);
	m_recNewView.setData(0, "BUSCV_COMMGRIDVIEW_SORT", bytGridSortData);

	return true;
}

void CommGridFilter::GetFilterDataFromGui()
{
	bool bViewSelGrid	= ui.ViewSelection_groupBox->isChecked();
	
	bool bDateFilterUsed= ui.Date_groupBox->isChecked();
	
	bool bDocTypeActive	= ui.documentType_groupBox->isChecked();
	bool bDocFilter		= ui.documents_checkBox->isChecked();
	bool bEmailsFilter	= ui.emails_checkBox->isChecked();
	bool bVoiceCFilter	= ui.voiceCalls_checkBox->isChecked();

	bool bAppTypeActive	= ui.application_groupBox->isChecked();

	bool bSearchActive	= ui.searchText_groupBox->isChecked();
	bool bSearchName	= ui.inName_checkBox->isChecked();
	bool bSearchDescr	= ui.inDescription_checkBox->isChecked();
	QString strSearchTxt= ui.searchTextEdit->toPlainText();
	
	//Date time filter.
	QDateTime dateFrom, dateTo;
	int nRangeIdx;
	ui.dateRange_frame->GetDates(dateFrom, dateTo, nRangeIdx);
	
	//CE type.
	bool bCETypeFilterActive = ui.FilterByType_groupBox->isChecked();

	//Write filter data to recordset.
	SetFilterIntValue(QVariant(bViewSelGrid).toInt(), VIEW_SELECTION_GRID_ACTIVE);
	SetFilterIntValue(QVariant(bDateFilterUsed).toInt(), FILTER_BY_DATE_ACTIVE);
	SetFilterIntValue(QVariant(bDocTypeActive).toInt(), FILTER_BY_ENTITY_TYPE_ACTIVE);
	SetFilterIntValue(QVariant(bDocFilter).toInt(), SHOW_DOCUMENTS);
	SetFilterIntValue(QVariant(bEmailsFilter).toInt(), SHOW_EMAILS);
	SetFilterIntValue(QVariant(bVoiceCFilter).toInt(), SHOW_VOICE_CALLS);
	SetFilterIntValue(QVariant(bAppTypeActive).toInt(), FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE);
	SetFilterIntValue(QVariant(bSearchActive).toInt(), FILTER_SEARCH_TEXT_ACTIVE);
	SetFilterIntValue(QVariant(bSearchName).toInt(), SEARCH_IN_NAME);
	SetFilterIntValue(QVariant(bSearchDescr).toInt(), SEARCH_IN_DESCRIPTION);
	SetFilterStringValue(strSearchTxt, SEARCH_TEXT);
	SetFilterDateTimeValue(dateFrom, FROM_DATE);
	SetFilterDateTimeValue(dateTo, TO_DATE);
	SetFilterIntValue(nRangeIdx, DATE_RANGE_SELECTOR);
	SetFilterIntValue(QVariant(bCETypeFilterActive).toInt(), FILTER_BY_CE_ENTITY_TYPE_ACTIVE);


	//Get application filter recordset.
	DbRecordSet recApplications;
	recApplications.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS));
	int nRow = 0;
	int nTopLevelItemsCount = ui.treeWidgetApp->topLevelItemCount();
	for(int i = 0; i < nTopLevelItemsCount; ++i)
	{
		QTreeWidgetItem *item = ui.treeWidgetApp->topLevelItem(i);
		if (item->checkState(0) == Qt::Checked)
		{
			recApplications.addRow();
			recApplications.setData(nRow, 0, item->data(0, Qt::UserRole));
			nRow++;
		}
	}
	SetFilterRecordSetValue(recApplications, APPLICATION_TYPES);

	//Get CE types filter recordset.
	DbRecordSet recCETypes;
	ui.TypeFilter_frame->GetTreeWidget()->GetCheckedItems(recCETypes);
	//_DUMP(recCETypes);
	
	if (ui.TypeFilter_comboBox->currentIndex() == 0)
		SetFilterRecordSetValue(recCETypes, CE_ENTITY_TYPE_FILTER_DOCUMENTS);
	else if (ui.TypeFilter_comboBox->currentIndex() == 1)
		SetFilterRecordSetValue(recCETypes, CE_ENTITY_TYPE_FILTER_VOICE_CALLS);
	else if (ui.TypeFilter_comboBox->currentIndex() == 2)
		SetFilterRecordSetValue(recCETypes, CE_ENTITY_TYPE_FILTER_EMAILS);

}


void CommGridFilter::SetViewDataToGui()
{
	//_DUMP(m_recFilterViews);
	
	//View data.
	int nRow = m_recFilterViews.find(0, m_nViewID, true);
	
	if (nRow < 0)
		return;
	
	bool bOpenOnStartup = m_recFilterViews.getDataRef(nRow, "BUSCV_OPEN_ON_STARTUP").toBool();
	bool bPublic		= m_recFilterViews.getDataRef(nRow, "BUSCV_IS_PUBLIC").toBool();
	QString strSortCode	= m_recFilterViews.getDataRef(nRow, "BUSCV_SORT_CODE").toString();

	ui.openOnStartup_checkBox->setChecked(bOpenOnStartup);
	ui.public_checkBox->setChecked(bPublic);
	ui.sortCode_lineEdit->setText(strSortCode);
}

void CommGridFilter::SetFilterDataToGui()
{
	//_DUMP(m_recFilterViewData);

	//Get filter data from recordset.
	bool bViewSelGrid	= GetFilterBoolValue(m_recFilterViewData, VIEW_SELECTION_GRID_ACTIVE);
	bool bDateFiltUsed	= GetFilterBoolValue(m_recFilterViewData, FILTER_BY_DATE_ACTIVE);
	QDateTime FromDat	= GetFilterDateTimeValue(m_recFilterViewData, FROM_DATE);
	QDateTime ToDat		= GetFilterDateTimeValue(m_recFilterViewData, TO_DATE);
	int  nRangeSelector = GetFilterIntValue(m_recFilterViewData, DATE_RANGE_SELECTOR);
	bool bDocTypeActive	= GetFilterBoolValue(m_recFilterViewData, FILTER_BY_ENTITY_TYPE_ACTIVE);
	bool bDocFilter		= GetFilterBoolValue(m_recFilterViewData, SHOW_DOCUMENTS);
	bool bEmailsFilter	= GetFilterBoolValue(m_recFilterViewData, SHOW_EMAILS);
	bool bVoiceCFilter	= GetFilterBoolValue(m_recFilterViewData, SHOW_VOICE_CALLS);
	bool bAppTypeActive	= GetFilterBoolValue(m_recFilterViewData, FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE);
	DbRecordSet recApplications = GetFilterRecordSetValue(m_recFilterViewData, APPLICATION_TYPES);
	bool bSearchActive	= GetFilterBoolValue(m_recFilterViewData, FILTER_SEARCH_TEXT_ACTIVE);
	bool bSearchName	= GetFilterBoolValue(m_recFilterViewData, SEARCH_IN_NAME);
	bool bSearchDescr	= GetFilterBoolValue(m_recFilterViewData, SEARCH_IN_DESCRIPTION);
	QString strSearchTxt= GetFilterStringValue(m_recFilterViewData, SEARCH_TEXT);
	bool bTaskTypeFilterActive = GetFilterBoolValue(m_recFilterViewData, FILTER_BY_TASK_TYPE_ACTIVE);
	int nTaskPriorityFrom = GetFilterIntValue(m_recFilterViewData, TASK_PRIORITY_FROM);
	int nTaskPriorityTo = GetFilterIntValue(m_recFilterViewData, TASK_PRIORITY_TO);
	DbRecordSet recTaskTypes = GetFilterRecordSetValue(m_recFilterViewData, TASK_TYPE_FILTER);

	ui.ViewSelection_groupBox->setChecked(bViewSelGrid);
	ui.Date_groupBox->setChecked(bDateFiltUsed);
	ui.dateRange_frame->Initialize(FromDat, ToDat, nRangeSelector);
	
	ui.documentType_groupBox->setChecked(bDocTypeActive);
	ui.documents_checkBox->setChecked(bDocFilter);
	ui.emails_checkBox->setChecked(bEmailsFilter);
	ui.voiceCalls_checkBox->setChecked(bVoiceCFilter);
	//TODO - application type. 
	ui.searchText_groupBox->setChecked(bSearchActive);
	ui.inName_checkBox->setChecked(bSearchName);
	ui.inDescription_checkBox->setChecked(bSearchDescr);
	ui.searchTextEdit->setText(strSearchTxt);

	//Applications type.
	ui.application_groupBox->blockSignals(true);
	ui.application_groupBox->setChecked(bAppTypeActive);
	ui.application_groupBox->blockSignals(false);
	
	FillAppTree();
	int nApplicationsCount = recApplications.getRowCount();
	for (int i = 0; i < nApplicationsCount; ++i)
	{
		//BDMA_ID from saved recordset.
		int nApplicationID = recApplications.getDataRef(i, "BDMA_ID").toInt();
		//Loop through tree items and find item.
		int nTopLevelItemsCount = ui.treeWidgetApp->topLevelItemCount();
		for(int j = 0; j < nTopLevelItemsCount; ++j)
		{
			QTreeWidgetItem *item = ui.treeWidgetApp->topLevelItem(j);
			if (item->data(0, Qt::UserRole).toInt() == nApplicationID)
				item->setCheckState(0, Qt::Checked);
		}
	}

	//CE type setup.
	bool bCETypeFilterActive = GetFilterBoolValue(m_recFilterViewData, FILTER_BY_CE_ENTITY_TYPE_ACTIVE);
	ui.FilterByType_groupBox->setChecked(bCETypeFilterActive);

	//Task type.
	ui.FilterByTaskType_groupBox->blockSignals(true);
	ui.FilterByTaskType_groupBox->setChecked(bTaskTypeFilterActive);
	ui.FilterByTaskType_groupBox->blockSignals(false);

	ui.cboPriority_From->blockSignals(true);
	ui.cboPriority_From->setCurrentIndex(ui.cboPriority_From->findText(QVariant(nTaskPriorityFrom).toString()));
	ui.cboPriority_From->blockSignals(false);

	ui.cboPriority_To->blockSignals(true);
	ui.cboPriority_To->setCurrentIndex(ui.cboPriority_To->findText(QVariant(nTaskPriorityTo).toString()));
	ui.cboPriority_To->blockSignals(false);

	SelectTaskTypeTableItems(recTaskTypes);

//	DbRecordSet recCETypes;
//	SetFilterRecordSetValue(recCETypes, CE_ENTITY_TYPE_FILTER);
//	ui.TypeFilter_frame->GetTreeWidget()->SetItemsChecked(recCETypes);
}

void CommGridFilter::FillAppTree()
{
	ui.treeWidgetApp->clear();

	DbRecordSet recApplications;
	DocumentHelper::GetDocApplications(recApplications);

	int nCodeIdx=recApplications.getColumnIdx("BDMA_CODE");
	recApplications.sort(nCodeIdx); //sort by code

	QList<QTreeWidgetItem *> items;
	int nSize=recApplications.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//QString strNodeName=m_lstApplications.getDataRef(i,nCodeIdx).toString()+" "+m_lstApplications.getDataRef(i,nNameIdx).toString();
		QString strNodeName=recApplications.getDataRef(i,"BDMA_NAME").toString();
		int nID=recApplications.getDataRef(i,"BDMA_ID").toInt();
		QTreeWidgetItem *pRoot=new QTreeWidgetItem((QTreeWidget *)0,QStringList(strNodeName));
		pRoot->setData(0,Qt::UserRole,nID);
		//set icon for application:
		QByteArray bytePicture=recApplications.getDataRef(i,"BDMA_ICON").toByteArray();
		if (bytePicture.size()>0)
		{
			QPixmap pixMap;
			pixMap.loadFromData(bytePicture);
			QIcon icon(pixMap);
			pRoot->setIcon(0,icon);
		}

		pRoot->setCheckState(0, Qt::Unchecked);
		pRoot->setData(0,Qt::UserRole,nID);
		items.append(pRoot);
	}
	ui.treeWidgetApp->insertTopLevelItems(0,items);
}

void CommGridFilter::ConnectSignals()
{
//! 	connect(ui.from_dateTimeEdit,			SIGNAL(dateTimeChanged(const QDateTime&)),	this,	SLOT(on_DateTimeChanged(const QDateTime&)));
//!	connect(ui.to_dateTimeEdit,				SIGNAL(dateTimeChanged(const QDateTime&)),	this,	SLOT(on_DateTimeChanged(const QDateTime&)));
	connect(ui.Date_groupBox,				SIGNAL(toggled(bool)),						this,	SLOT(on_groupBoxToggled(bool)));
	connect(ui.documentType_groupBox,		SIGNAL(toggled(bool)),						this,	SLOT(on_groupBoxToggled(bool)));
	connect(ui.application_groupBox,		SIGNAL(toggled(bool)),						this,	SLOT(on_groupBoxToggled(bool)));
	connect(ui.searchText_groupBox,			SIGNAL(toggled(bool)),						this,	SLOT(on_groupBoxToggled(bool)));
	connect(ui.openOnStartup_checkBox,		SIGNAL(stateChanged(int)),					this,	SLOT(on_normalCheckBox_stateChanged(int)));
	connect(ui.public_checkBox,				SIGNAL(stateChanged(int)),					this,	SLOT(on_normalCheckBox_stateChanged(int)));
	connect(ui.documents_checkBox,			SIGNAL(stateChanged(int)),					this,	SLOT(on_normalCheckBox_stateChanged(int)));
	connect(ui.emails_checkBox,				SIGNAL(stateChanged(int)),					this,	SLOT(on_normalCheckBox_stateChanged(int)));
	connect(ui.voiceCalls_checkBox,			SIGNAL(stateChanged(int)),					this,	SLOT(on_normalCheckBox_stateChanged(int)));
	connect(ui.inName_checkBox,				SIGNAL(stateChanged(int)),					this,	SLOT(on_normalCheckBox_stateChanged(int)));
	connect(ui.inDescription_checkBox,		SIGNAL(stateChanged(int)),					this,	SLOT(on_normalCheckBox_stateChanged(int)));
}

void CommGridFilter::ConnectValueChangedSignal()
{
	connect(this, SIGNAL(FilterValueChanged()), this, SLOT(on_FilterValueChanged()));
}

void CommGridFilter::DisconnectValueChangedSignal()
{
	disconnect(this, SIGNAL(FilterValueChanged()), this, SLOT(on_FilterValueChanged()));
}

void CommGridFilter::on_DateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange)
{
	emit FilterValueChanged();
}

void CommGridFilter::on_viewName_comboBox_currentIndexChanged(int index)
{
	int nViewID = ui.viewName_comboBox->itemData(index).toInt();
	DbRecordSet tmp;
	SetCurrentView(nViewID, tmp, m_nLoggedPersonID);
}

void CommGridFilter::on_spinBox_editingFinished(int nSetting, int nValue)
{
	//Write setting to recordset.
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nSetting, true);
	m_recFilterViewData.setData(nRow, "BUSCS_VALUE", nValue);

	emit FilterValueChanged();
}

void CommGridFilter::on_CheckBoxStateChanged(int nSettingID, int nValue)
{
	//Write setting to recordset.
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nSettingID, true);
	m_recFilterViewData.setData(nRow, "BUSCS_VALUE", nValue);

	emit FilterValueChanged();
}

void CommGridFilter::on_ViewSelection_groupBox_toggled(bool bChecked)
{
	if (!ui.documentType_groupBox->isChecked())
	{
		ui.documentType_groupBox->setChecked(true);
		ui.documents_checkBox->setChecked(true);
		ui.emails_checkBox->setChecked(true);
		ui.voiceCalls_checkBox->setChecked(true);
	}
	emit FilterValueChanged();
}

void CommGridFilter::on_documentType_groupBox_toggled(bool bChecked)
{
	if (!ui.ViewSelection_groupBox->isChecked() && !bChecked)
	{
		QMessageBox::warning(NULL, tr("Filter warning"), tr("View Selection and Show Filter groups can not be unchecked at the same time!"));
		ui.documentType_groupBox->blockSignals(true);
		ui.documentType_groupBox->setChecked(true);
		ui.documentType_groupBox->blockSignals(false);
	}
	emit FilterValueChanged();
}

void CommGridFilter::on_DateTimeChanged(const QDateTime &datetime)
{
	emit FilterValueChanged();
}

void CommGridFilter::on_spinBox_editingFinished()
{
	emit FilterValueChanged();
}

void CommGridFilter::on_groupBoxToggled(bool bOn)
{
	emit FilterValueChanged();
}

void CommGridFilter::on_normalCheckBox_stateChanged(int state)
{
	emit FilterValueChanged();
}

void CommGridFilter::on_FilterValueChanged()
{
	if (m_bLoadFromWizard)
	{
		ui.viewName_comboBox->setCurrentIndex(-1);
	}

	if (GetMode() == MODE_INSERT || GetMode() == MODE_EDIT)
		return;
	else if (GetMode() == MODE_READ)
	{
		ui.viewName_comboBox->blockSignals(true);
		ui.viewName_comboBox->setCurrentIndex(-1);
		ui.viewName_comboBox->blockSignals(false);
	}
}

void CommGridFilter::on_application_groupBox_toggled(bool bChecked)
{
	emit FilterValueChanged();
}

void CommGridFilter::on_treeWidgetApp_itemChanged(QTreeWidgetItem *item, int column)
{
	emit FilterValueChanged();
}

void CommGridFilter::on_searchTextEdit_textChanged()
{
	emit FilterValueChanged();
}

void CommGridFilter::HideAllFirstRow(bool bChecked)
{
	//Unassigned.
	m_hshCheckBoxes.value(SHOW_VOICE_CALLS_UNASSIGNED)->setCheckState(Qt::Unchecked);
	//Missed.
	m_hshCheckBoxes.value(SHOW_VOICE_CALLS_MISSED)->setCheckState(Qt::Unchecked);
	//Scheduled.
	m_hshCheckBoxes.value(SHOW_VOICE_CALLS_SCHEDULED)->setCheckState(Qt::Unchecked);
	//Due.
	m_hshCheckBoxes.value(SHOW_VOICE_CALLS_DUE)->setCheckState(Qt::Unchecked);
	//Overdue.
	m_hshCheckBoxes.value(SHOW_VOICE_CALLS_OVERDUE)->setCheckState(Qt::Unchecked);
	//Last.
	m_hshCheckBoxes.value(SHOW_VOICE_CALLS_LAST)->setCheckState(Qt::Unchecked);

	emit FilterValueChanged();
}

void CommGridFilter::HideAllSecondRow(bool bChecked)
{
	//Unread column.
	m_hshCheckBoxes.value(SHOW_EMAILS_UNREAD)->setCheckState(Qt::Unchecked);
	//Unassigned.
	m_hshCheckBoxes.value(SHOW_EMAILS_UNASSIGNED)->setCheckState(Qt::Unchecked);
	//Scheduled.
	m_hshCheckBoxes.value(SHOW_EMAILS_SCHEDULED)->setCheckState(Qt::Unchecked);
	//Due.
	m_hshCheckBoxes.value(SHOW_EMAILS_DUE)->setCheckState(Qt::Unchecked);
	//Overdue.
	m_hshCheckBoxes.value(SHOW_EMAILS_OVERDUE)->setCheckState(Qt::Unchecked);
	//Templates.
	m_hshCheckBoxes.value(SHOW_EMAILS_TEMPLATES)->setCheckState(Qt::Unchecked);
	//Last.
	m_hshCheckBoxes.value(SHOW_EMAILS_LAST)->setCheckState(Qt::Unchecked);

	emit FilterValueChanged();
}

void CommGridFilter::HideAllThirdRow(bool bChecked)
{
	//Unassigned.
	m_hshCheckBoxes.value(DOCUMENTS_UNASSIGNED)->setCheckState(Qt::Unchecked);
	//Scheduled.
	m_hshCheckBoxes.value(DOCUMENTS_SCHEDULED)->setCheckState(Qt::Unchecked);
	//Due.
	m_hshCheckBoxes.value(DOCUMENTS_DUE)->setCheckState(Qt::Unchecked);
	//Overdue.
	m_hshCheckBoxes.value(DOCUMENTS_OVERDUE)->setCheckState(Qt::Unchecked);
	//Templates.
	m_hshCheckBoxes.value(DOCUMENTS_TEMPLATES)->setCheckState(Qt::Unchecked);
	//Last.
	m_hshCheckBoxes.value(DOCUMENTS_LAST)->setCheckState(Qt::Unchecked);

	emit FilterValueChanged();
}

void CommGridFilter::on_FilterByType_groupBox_toggled(bool bChecked)
{
	emit FilterValueChanged();
}

void CommGridFilter::on_TypeFilter_comboBox_currentIndexChanged(int nIndex)
{
	//Re filter the tree.
	ui.TypeFilter_frame->blockSignals(true);
	ui.TypeFilter_frame->ClearLocalFilter();
	ui.TypeFilter_frame->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",CommEntityTypeFromComboIdx(nIndex));
	ui.TypeFilter_frame->ReloadData();
	ui.TypeFilter_frame->blockSignals(false);

	//Set combo filter value.
	SetFilterIntValue(nIndex, SELECTED_COMBO_CE_ENTITY_TYPE_FILTER);

	//Check and uncheck boxes.
	SetupCETypeFilterFromSavedData();
}

void CommGridFilter::on_TypeFilter_frame_ItemCheckStateChanged(QList<QTreeWidgetItem*> lstChangedCheckStateItems)
{
	//First get CE type (document, email, or voice call).
	int nComboID = ui.TypeFilter_comboBox->currentIndex();
	//Then get recordset you need.
	DbRecordSet recCETypes;
	if (nComboID == 0)
		recCETypes = GetFilterRecordSetValue(m_recFilterViewData, CE_ENTITY_TYPE_FILTER_DOCUMENTS);
	else if (nComboID == 1)
		recCETypes = GetFilterRecordSetValue(m_recFilterViewData, CE_ENTITY_TYPE_FILTER_EMAILS);
	else if (nComboID == 2)
		recCETypes = GetFilterRecordSetValue(m_recFilterViewData, CE_ENTITY_TYPE_FILTER_VOICE_CALLS);

	//Go through list and record the check states.
	int nItemsCount = lstChangedCheckStateItems.count();
	for (int i = 0; i < nItemsCount; ++i)
	{
		QTreeWidgetItem *item = lstChangedCheckStateItems.value(i);

		//Get item row ID.
		int nID = item->data(0, Qt::UserRole).toInt();

		if (recCETypes.getRowCount() == 0)
			recCETypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS));

		//Find ID in recordset, if not exists, add new row and set values.
		int nRow = recCETypes.find(0, nID, true);
		if (nRow < 0)
		{
			recCETypes.addRow();
			recCETypes.setData(recCETypes.getRowCount()-1, "CHECKED_ITEM_ID", nID);
			recCETypes.setData(recCETypes.getRowCount()-1, "CHECKED_ITEM_STATE", item->checkState(0));
		}
		else
			recCETypes.setData(nRow, 1, item->checkState(0));
	}

	if (nComboID == 0)
		SetFilterRecordSetValue(recCETypes, CE_ENTITY_TYPE_FILTER_DOCUMENTS);
	else if (nComboID == 1)
		SetFilterRecordSetValue(recCETypes, CE_ENTITY_TYPE_FILTER_EMAILS);
	else if (nComboID == 2)
		SetFilterRecordSetValue(recCETypes, CE_ENTITY_TYPE_FILTER_VOICE_CALLS);
	
	emit FilterValueChanged();
}

void CommGridFilter::on_cboPriority_From_currentIndexChanged(const QString& text)
{
	SetFilterIntValue(QVariant(text).toInt(), TASK_PRIORITY_FROM);
	emit FilterValueChanged();
}

void CommGridFilter::on_cboPriority_To_currentIndexChanged(const QString &text)
{
	SetFilterIntValue(QVariant(text).toInt(), TASK_PRIORITY_TO);
	emit FilterValueChanged();
}

void CommGridFilter::on_FilterByTaskType_groupBox_clicked(bool checked)
{
	int nChecked = 0;
	if (checked)
		nChecked = 1;
	SetFilterIntValue(nChecked, FILTER_BY_TASK_TYPE_ACTIVE);

	//Get and set combo values, just in case it was never set before.
	int nTaskPriorityFrom = GetFilterIntValue(m_recFilterViewData, TASK_PRIORITY_FROM);
	int nTaskPriorityTo = GetFilterIntValue(m_recFilterViewData, TASK_PRIORITY_TO);
	SetFilterIntValue(nTaskPriorityFrom, TASK_PRIORITY_FROM);
	SetFilterIntValue(nTaskPriorityTo, TASK_PRIORITY_TO);

	emit FilterValueChanged();
}

void CommGridFilter::on_TaskType_tableWidget_SelectionChanged()
{
	int nTaksTypeID;
	QString strName, strCode;
	DbRecordSet rowTaskType;
	ui.TaskType_tableWidget->GetSelection(nTaksTypeID, strCode, strName, rowTaskType);

	SetFilterRecordSetValue(rowTaskType, TASK_TYPE_FILTER);
	
	emit FilterValueChanged();
}

void CommGridFilter::EnableViewChBoxes(bool bEnable)
{
	ui.openOnStartup_checkBox->setEnabled(bEnable);
	ui.public_checkBox->setEnabled(bEnable);
	ui.sortCode_lineEdit->setEnabled(bEnable);
	ui.sortCode_label->setEnabled(bEnable);
}

void CommGridFilter::SetFilterIntValue(int nValue, int nFilterSetting)
{
	//_DUMP(m_recFilterViewData);
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		m_recFilterViewData.setData(nRow, "BUSCS_VALUE", nValue);
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		Q_ASSERT(nTmpRow >= 0);
		tmp.setData(nTmpRow, "BUSCS_VALUE", nValue);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		m_recFilterViewData.merge(tmp, true);
	}
}

void CommGridFilter::SetFilterStringValue(QString strValue, int nFilterSetting)
{
	//_DUMP(m_recFilterViewData);
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		m_recFilterViewData.setData(nRow, "BUSCS_VALUE_STRING", strValue);
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		Q_ASSERT(nTmpRow >= 0);
		tmp.setData(nTmpRow, "BUSCS_VALUE_STRING", strValue);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		m_recFilterViewData.merge(tmp, true);
	}
}

void CommGridFilter::SetFilterDateTimeValue(QDateTime datValue, int nFilterSetting)
{
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		m_recFilterViewData.setData(nRow, "BUSCS_VALUE_DATETIME", datValue);
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		Q_ASSERT(nTmpRow >= 0);
		tmp.setData(nTmpRow, "BUSCS_VALUE_DATETIME", datValue);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		m_recFilterViewData.merge(tmp, true);
	}
}

void CommGridFilter::SetFilterRecordSetValue(DbRecordSet recValue, int nFilterSetting)
{
	QByteArray bytValue(XmlUtil::ConvertRecordSet2ByteArray_Fast(recValue));
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		m_recFilterViewData.setData(nRow, "BUSCS_VALUE_BYTE", bytValue);
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		Q_ASSERT(nTmpRow >= 0);
		tmp.setData(nTmpRow, "BUSCS_VALUE_BYTE", bytValue);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		m_recFilterViewData.merge(tmp, true);
	}
}

bool CommGridFilter::GetFilterBoolValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	bool bValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		bValue = m_recFilterViewData.getDataRef(m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true), "BUSCS_VALUE").toBool();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		bValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE").toBool();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return bValue;
}

int CommGridFilter::GetFilterIntValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	int nValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		nValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE").toInt();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		nValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE").toInt();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return nValue;
}

QString CommGridFilter::GetFilterStringValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	QString strValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		strValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE_STRING").toString();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		strValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE_STRING").toString();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return strValue;
}

QDateTime CommGridFilter::GetFilterDateTimeValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	QDateTime datValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		datValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE_DATETIME").toDateTime();
		//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		datValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE_DATETIME").toDateTime();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return datValue;
}

DbRecordSet CommGridFilter::GetFilterRecordSetValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	DbRecordSet recValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0){
		QByteArray arr = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE_BYTE").toByteArray();
		recValue = XmlUtil::ConvertByteArray2RecordSet_Fast(arr);
	}
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		QByteArray arr = tmp.getDataRef(nTmpRow, "BUSCS_VALUE_BYTE").toByteArray();
		recValue = XmlUtil::ConvertByteArray2RecordSet_Fast(arr);
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return recValue;
}

void CommGridFilter::SetupCETypeFilter()
{
	ui.TypeFilter_frame->GetTreeWidget()->SetItemsUserCheckable(true);
	ui.TypeFilter_frame->Initialize(ENTITY_CE_TYPES);	//init selection controller
	
	ui.TypeFilter_comboBox->addItem(tr("Document types"));
	ui.TypeFilter_comboBox->addItem(tr("Email types"));
	ui.TypeFilter_comboBox->addItem(tr("Phone Call types"));

	//connect(ui.TypeFilter_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(OnRootTypeComboChanged(int)));

	EnableHierarchyCodeCheck("CET_CODE");

	//Set initial filter.
	ui.TypeFilter_comboBox->setCurrentIndex(0);
}

void CommGridFilter::SetupTaskTypeFilter()
{
	ui.TaskType_tableWidget->Initialize(ENTITY_BUS_CM_TYPES, true, true, true, true);
	ui.TaskType_tableWidget->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_TASK);
	ui.TaskType_tableWidget->ReloadData();
	
	ui.cboPriority_From->blockSignals(true);
	ui.cboPriority_From->addItem("0");
	ui.cboPriority_From->addItem("1");
	ui.cboPriority_From->addItem("2");
	ui.cboPriority_From->addItem("3");
	ui.cboPriority_From->addItem("4");
	ui.cboPriority_From->addItem("5");
	ui.cboPriority_From->addItem("6");
	ui.cboPriority_From->addItem("7");
	ui.cboPriority_From->addItem("8");
	ui.cboPriority_From->addItem("9");
	ui.cboPriority_From->blockSignals(false);

	ui.cboPriority_To->blockSignals(true);
	ui.cboPriority_To->addItem("0");
	ui.cboPriority_To->addItem("1");
	ui.cboPriority_To->addItem("2");
	ui.cboPriority_To->addItem("3");
	ui.cboPriority_To->addItem("4");
	ui.cboPriority_To->addItem("5");
	ui.cboPriority_To->addItem("6");
	ui.cboPriority_To->addItem("7");
	ui.cboPriority_To->addItem("8");
	ui.cboPriority_To->addItem("9");
	ui.cboPriority_To->blockSignals(false);
}

void CommGridFilter::SelectTaskTypeTableItems(DbRecordSet &recStoredTypes)
{
	ui.TaskType_tableWidget->blockSignals(true);
	//Task type table recordset.
	DbRecordSet *recTaskTypes = ui.TaskType_tableWidget->GetDataSource();
	recTaskTypes->clearSelection();
	
	//Loop through stored recordset and select existing task types.
	int nRowCount = recStoredTypes.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nBCMT_ID = recStoredTypes.getDataRef(i, "BCMT_ID").toInt();
		recTaskTypes->find("BCMT_ID", nBCMT_ID, false, false, false);
	}
	
	ui.TaskType_tableWidget->RefreshDisplay();
	ui.TaskType_tableWidget->blockSignals(false);
}

void CommGridFilter::SetupCETypeFilterFromSavedData()
{
	int nComboID = ui.TypeFilter_comboBox->currentIndex();

	//Get CE types filter recordset.
	DbRecordSet recCETypes;
	if (nComboID == 0)
		recCETypes = GetFilterRecordSetValue(m_recFilterViewData, CE_ENTITY_TYPE_FILTER_DOCUMENTS);
	else if (nComboID == 1)
		recCETypes = GetFilterRecordSetValue(m_recFilterViewData, CE_ENTITY_TYPE_FILTER_EMAILS);
	else if (nComboID == 2)
		recCETypes = GetFilterRecordSetValue(m_recFilterViewData, CE_ENTITY_TYPE_FILTER_VOICE_CALLS);

	//_DUMP(recCETypes);
	
	if (!recCETypes.getColumnCount())
		recCETypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS));
	
	//_DUMP(recCETypes);
	
	ui.TypeFilter_frame->GetTreeWidget()->SetItemsChecked(recCETypes);
}

void CommGridFilter::SetupSortTab()
{
	CommGridViewHelper::SetupSortRecordSet(m_lstSortColumnSetup);
	ui.sort_frame->SetColumnSetup(m_lstSortColumnSetup);
}

int CommGridFilter::CommEntityTypeFromComboIdx(int nCboIdx)
{
	//map combo index to type ID
	switch(nCboIdx)
	{
	case 0:	return GlobalConstants::CE_TYPE_DOCUMENT;	//0;	//document
	case 1:	return GlobalConstants::CE_TYPE_EMAIL;		//2;	//email
	case 2:	return GlobalConstants::CE_TYPE_VOICE_CALL;	//1;	//voice call
	}
	Q_ASSERT(false);	//unknown type
	return -1;
}
