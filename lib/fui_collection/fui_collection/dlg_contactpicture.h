#ifndef DLG_CONTACTPICTURE_H
#define DLG_CONTACTPICTURE_H

#include <QtWidgets/QDialog>
#include <QLabel>
#include <QPixmap>
#include "generatedfiles/ui_dlg_contactpicture.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "gui_core/gui_core/gui_helper.h"
#include "gui_core/gui_core/dlg_picture.h"



class Dlg_ContactPicture : public QDialog
{
    Q_OBJECT

public:
    Dlg_ContactPicture(QWidget *parent = 0);
    ~Dlg_ContactPicture();

	void SetRow(DbRecordSet &row,bool bEditMode);
	bool GetRow(DbRecordSet &row);


private slots:
		void on_btnCancel_clicked();
		void on_btnOK_clicked();

private:

	Ui::Dlg_ContactPictureClass ui;

	GuiFieldManager *m_GuiFieldManagerMain;
	GuiDataManipulator m_DataManager;
	DbRecordSet m_lstData;

	bool m_bEditMode;

};

#endif // DLG_CONTACTPICTURE_H
