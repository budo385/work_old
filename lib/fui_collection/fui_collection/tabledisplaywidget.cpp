#include "tabledisplaywidget.h"
#include <QToolTip>
#include <QTextEdit>

TableDisplayWidget::TableDisplayWidget(CalendarGraphicsView *pCalendarGraphicsView, int rows, int columns, bool bEntityItem /*= false*/, QWidget *parent /*= 0*/)
:QTableWidget(rows, columns, parent)
{
	m_bEntityItem = bEntityItem;
	m_pCalendarGraphicsView = pCalendarGraphicsView;
}

bool TableDisplayWidget::viewportEvent(QEvent *event)
{
	if (event->type() == QEvent::ToolTip) 
	{
		QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
		QPoint itemPos = helpEvent->pos();
		QTableWidgetItem *Item = itemAt(itemPos);
		if (Item == NULL)
			return false;
		//If in area for general tooltip, display general tooltip.
		QString strText;
		if (Item->row()==0 && itemPos.x() < 20 && m_bEntityItem)
		{
			strText= QString("<html><body style=\" font-family:Arial; text-decoration:none;\">");
			int nRowCount = rowCount();
			for (int i = 0; i < nRowCount; ++i)
			{
				if (i == (nRowCount-1)/*item(i,0)*/)
					strText+=dynamic_cast<QTextEdit*>(cellWidget(i, 0))->toHtml();
				else
					strText+=QString("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; \">%1</span></p>").arg(item(i, 0)->data(Qt::DisplayRole).toString());
			}
			strText+=QString("</body></html>");
		}
		//Else display only item tooltip.
		else
		{
			strText = Item->data(Qt::DisplayRole).toString();
		}

		QPoint pos = helpEvent->globalPos();
		QToolTip::showText(pos, strText);
		return true;
	} 
	else
		return QTableWidget::viewportEvent(event);
}
