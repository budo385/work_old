#ifndef OPTIONS_CONTACTS_H
#define OPTIONS_CONTACTS_H

#include <QWidget>
#include "generatedfiles/ui_options_contacts.h"
#include "settingsbase.h"
#include "bus_client/bus_client/selection_combobox.h"

class Options_Contacts : public SettingsBase
{
	Q_OBJECT

public:
	Options_Contacts(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
	~Options_Contacts();

	DbRecordSet GetChangedValuesRecordSet();

private slots:
		void OnCustomizeAddrPerson();
		void OnCustomizeAddrOrg();
		void OpenGroupViewEditor();
		void OpenACOViewEditor();

private:
	Ui::Options_ContactsClass ui;
	void LoadTree(DbRecordSet &lstData,bool bReloadFromServer=false);

	Selection_ComboBox m_ComboHandler;
	Selection_ComboBox m_ComboHandlerACO;
	Selection_ComboBox m_FormatAddressCombo;
	Selection_ComboBox m_FormatAddressComboOrg;
};

#endif // OPTIONS_CONTACTS_H
