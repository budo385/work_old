#include "nmrxrelationwidget.h"
#include "dlg_nmrxeditor.h"
#include "table_nmrxrelation.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "gui_core/gui_core/macros.h"
#include "bus_core/bus_core/globalconstants.h"

#include <QVBoxLayout>
#include <QTimer>

//globals:
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester



NMRXRelationWidget::NMRXRelationWidget(QWidget *parent)
	: QWidget(parent),m_bReadOnlyMode(false),m_bSetUsePersonAssignAsContact(false),m_pTable(NULL),m_toolbar(NULL),m_bSkipWriteMode(false),m_bDisableRoleSwitch(false),m_bEdit(true),m_pBtnInviteSend(NULL),m_bForbidDuplicateAssignments(false)
{

	//m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">";
	//m_strEndHtmlTag="</span></p></body></html>";
	
}

NMRXRelationWidget::~NMRXRelationWidget()
{

}



//init pattern: from master m_pTable: If from Contact FUI then master is Contact
//lstPairs are pair id's from NMRXManager for which this pattern will be initiated
//main list is in format: TVIEW_BUS_NMRX_RELATION_SELECT
void NMRXRelationWidget::Initialize(QString strTitle,int nMasterTableID,QList<int>lstPairs,int nAssigmentTableID_1,int nAssigmentTableID_2,int nMasterTableTypeID_1,int nTableTypeID_1,int nTableTypeID_2, int nX_TableID)
{

	if ((nMasterTableID==BUS_CM_CONTACT && nAssigmentTableID_1==BUS_CM_CONTACT))
	{
		if(!g_AccessRight->TestAR(ASSIGN_CONTACTS,true))
		{
			m_bReadOnlyMode=true;
		}
	}
	if ((nMasterTableID==BUS_CM_CONTACT && nAssigmentTableID_1==BUS_PROJECT) || (nMasterTableID==BUS_PROJECT && nAssigmentTableID_1==BUS_CM_CONTACT))
	{
		if(!g_AccessRight->TestAR(ASSIGN_PROJECTS,true))
		{
			m_bReadOnlyMode=true;
		}
	}


	NMRXManager::DefineRolePairList(m_lstPairs);

	//assemlbe pair list:
	int nSize=lstPairs.size();
	for(int i=0;i<nSize;++i)
		m_lstPairs.merge(NMRXManager::GetPairRow(lstPairs.at(i)));

	Q_ASSERT(m_lstPairs.getRowCount()!=0);

	Q_ASSERT(nAssigmentTableID_1!=-1); //1 must be valid


	m_nMasterTableID=nMasterTableID;
	m_nAssigmentTableID_1=nAssigmentTableID_1;
	m_nAssigmentTableID_2=nAssigmentTableID_2;
	m_nTableTypeID_1=nTableTypeID_1;
	m_nTableTypeID_2=nTableTypeID_2;
	m_nMasterTableTypeID_1=nMasterTableTypeID_1;
	m_nX_TableID=nX_TableID;



	//cacher:
	m_EntityAssigment1.Initialize(NMRXManager::GetEntityIDFromTableID(nAssigmentTableID_1,m_nTableTypeID_1));
	if (nAssigmentTableID_2!=-1)
	m_EntityAssigment2.Initialize(NMRXManager::GetEntityIDFromTableID(nAssigmentTableID_2,m_nTableTypeID_2));

	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));


	m_nTable1Idx=m_lstData.getColumnIdx("BNMR_TABLE_1");
	m_nTable2Idx=m_lstData.getColumnIdx("BNMR_TABLE_2");
	m_nKey1Idx=m_lstData.getColumnIdx("BNMR_TABLE_KEY_ID_1");
	m_nKey2Idx=m_lstData.getColumnIdx("BNMR_TABLE_KEY_ID_2");
	m_nNameIdx_1=m_lstData.getColumnIdx("NAME_1");
	m_nNameIdx_2=m_lstData.getColumnIdx("NAME_2");

	//build widget:
	m_toolbar= new QHBoxLayout;

	//------------------------------------------------------
	//				TOOLBAR
	//------------------------------------------------------
	//LABEL:
	//QLabel *Title=new QLabel;
	//Title->setText(m_strHtmlTag+strTitle+m_strEndHtmlTag);

	QSize buttonSize(22, 22);

	//CREATE BUTTONS/SIZE
	QPushButton *pBtnEdit			= new QPushButton;
	QPushButton *pBtnShow			= new QPushButton;
	QPushButton *pBtnDelete			= new QPushButton;
	QPushButton *pBtnShowDetails	= new QPushButton;
	QPushButton *pBtnShowCalendar	= new QPushButton;
	QPushButton *pBtnAssign1		= new QPushButton;
	QPushButton *pBtnAssign2		= new QPushButton;
	m_pBtnInviteSend		= new QPushButton(tr("Send Invitations Now!"),this);

	m_pieMenu_toolButton = new Selection_ContactButton_FUI(this);

	connect(pBtnEdit, SIGNAL(clicked()), this, SLOT(OnEdit()));
	connect(pBtnShow, SIGNAL(clicked()), this, SLOT(OnShow()));
	connect(pBtnDelete, SIGNAL(clicked()), this, SLOT(OnDelete()));
	connect(pBtnShowDetails, SIGNAL(clicked()), this, SLOT(OnShowDetails()));
	connect(pBtnShowCalendar, SIGNAL(clicked()), this, SLOT(OnShowCalendar()));
	connect(m_pBtnInviteSend, SIGNAL(clicked()), this, SLOT(OnInviteSend()));
	connect(pBtnAssign1, SIGNAL(clicked()), this, SLOT(OnAssign1()));
	connect(pBtnAssign2, SIGNAL(clicked()), this, SLOT(OnAssign2()));

	pBtnEdit->setIcon(QIcon(":SAP_Modify.png"));
	pBtnShow->setIcon(QIcon(":SAP_Select.png"));
	pBtnDelete->setIcon(QIcon(":SAP_Clear.png"));
	pBtnShowDetails->setIcon(QIcon(":SAP_Modify.png"));
	pBtnShowCalendar->setIcon(QIcon(":CalendarEvent16.png"));


	pBtnEdit->setToolTip(tr("Edit Assignment"));
	pBtnShow->setToolTip(tr("Show Assignment"));
	pBtnDelete->setToolTip(tr("Remove Assignment"));
	pBtnShowDetails->setToolTip(tr("Show Details"));
	pBtnShowCalendar->setToolTip(tr("Show Assignee Calendar"));
	m_pBtnInviteSend->setToolTip(tr("Send Invitations Now!"));

	QString strText,strIcon;
	NMRXManager::GetIconAndTextFromTableID(nAssigmentTableID_1,strIcon,strText,m_nTableTypeID_1);
	pBtnAssign1->setIcon(QIcon(strIcon));

	QString strBtnText;
	//if (nAssigmentTableID_1 == BUS_CM_CONTACT)
		strBtnText=strText + tr(" Assign");
	//else
	//strBtnText=tr("Assign ")+strText;

	if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
		pBtnAssign1->setText(strBtnText);

	pBtnAssign1->setToolTip(strBtnText);


	//assigment buttons
	if (nAssigmentTableID_2!=-1)
	{
		NMRXManager::GetIconAndTextFromTableID(nAssigmentTableID_2,strIcon,strText,m_nTableTypeID_2);
		pBtnAssign2->setIcon(QIcon(strIcon));

		QString strBtnText;
		if (nAssigmentTableID_2 == BUS_PERSON)
			strBtnText=strText + tr(" Assign");
		else
			strBtnText=tr("Assign ")+strText;

		if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
			pBtnAssign2->setText(strBtnText);

		pBtnAssign2->setToolTip(strBtnText);
	}
	else
		pBtnAssign2->setVisible(false);


	//size
	pBtnEdit		->setMaximumSize(buttonSize);
	pBtnEdit		->setMinimumSize(buttonSize);
	pBtnShow		->setMaximumSize(buttonSize);
	pBtnShow		->setMinimumSize(buttonSize);
	pBtnDelete		->setMaximumSize(buttonSize);
	pBtnDelete		->setMinimumSize(buttonSize);
	pBtnShowDetails->setMinimumSize(buttonSize);
	pBtnShowDetails->setMaximumSize(buttonSize);
	pBtnShowCalendar->setMinimumSize(buttonSize);
	pBtnShowCalendar->setMaximumSize(buttonSize);
	m_pBtnInviteSend->setMaximumHeight(22);

	m_pieMenu_toolButton->setMinimumSize(buttonSize);
	
	//pBtnAssign1		->setMaximumSize(buttonSize);
	pBtnAssign1		->setMinimumSize(buttonSize);
	//pBtnAssign2		->setMaximumSize(buttonSize);
	pBtnAssign2		->setMinimumSize(buttonSize);

	//Tool bar:
	//m_toolbar->addWidget(Title);
	//m_toolbar->addSpacing(10);
	//m_toolbar->addStretch(1);
	m_toolbar->addWidget(pBtnAssign1);
	m_toolbar->addWidget(pBtnAssign2);
	if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
		m_toolbar->addSpacing(10);
	m_toolbar->addWidget(m_pieMenu_toolButton);
	m_toolbar->addWidget(pBtnShowDetails);
	m_toolbar->addWidget(pBtnShowCalendar);
	m_toolbar->addStretch(1);
	m_toolbar->addWidget(m_pBtnInviteSend);
	m_toolbar->addStretch(1);
	m_toolbar->addWidget(pBtnShow);
	m_toolbar->addWidget(pBtnEdit);
	m_toolbar->addWidget(pBtnDelete);
	//m_toolbar->addSpacing(10);
	m_toolbar->setSpacing(1);
	m_toolbar->setMargin(0);

	//if contact involved, set pie
	if (m_nAssigmentTableID_1==BUS_CM_CONTACT || m_nAssigmentTableID_2==BUS_CM_CONTACT)
		m_pieMenu_toolButton->setVisible(true);
	else
		m_pieMenu_toolButton->setVisible(false);

	if (m_nMasterTableTypeID_1!=GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART)
		pBtnShowCalendar->setVisible(false);
	if (m_nMasterTableTypeID_1==GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART && m_nAssigmentTableID_1==BUS_CM_CONTACT)
		m_pBtnInviteSend->setVisible(true);
	else
		m_pBtnInviteSend->setVisible(false);

	


	//------------------------------------------------------
	//				TABLE
	//------------------------------------------------------

	m_pTable= new Table_NMRXRelation(this);
	m_pTable->Initialize(&m_lstData,nMasterTableID,lstPairs,nX_TableID);
	connect(m_pTable,SIGNAL(NewDataDropped(int,DbRecordSet)),this,SLOT(On_NewDataDropped(int,DbRecordSet)));


	//---------------------------------------- INIT CNTX MENU TABLE--------------------------------//
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;

	pAction = new QAction(pBtnAssign1->toolTip(), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnAssign1()));
	lstActions.append(pAction);

	if (nAssigmentTableID_2!=-1)
	{
		pAction = new QAction(pBtnAssign2->toolTip(), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnAssign2()));
		lstActions.append(pAction);
	}


	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);


	pAction = new QAction(tr("Show Assignment"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnShow()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Edit Assignment"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnEdit()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Remove Selected Assignment"), this);
	pAction->setShortcut(tr("Del"));
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDelete()));
	lstActions.append(pAction);
	m_pTable->addAction(pAction);

	pAction = new QAction(tr("Remove All Assignments"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteAll()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Show Details"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDetails()));
	lstActions.append(pAction);


	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);


	//Select All:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), m_pTable, SLOT(OpenSortDialog()));

	lstActions.append(pAction);
	m_pTable->addAction(pAction);


	m_pTable->SetContextMenuActions(lstActions);


	//---------------------------------------- INIT CNTX MENU TABLE--------------------------------//

	connect(m_pTable,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnShow()));
	connect(m_pTable,SIGNAL(SignalSelectionChanged()),this,SLOT(OnSelectionChanged()));

	m_pieMenu_toolButton->SetEntityRecordID(0);

	QVBoxLayout *layout=new QVBoxLayout;

	layout->addLayout(m_toolbar);
	layout->addWidget(m_pTable);
	layout->setSpacing(3);
	layout->setMargin(0);
	this->setLayout(layout);

	//invalidate at start:
	Invalidate();
}


void NMRXRelationWidget::Invalidate()
{
	//m_bSkipWriteMode=false;
	//enable toolbar:
	EnableToolbar(false);
	m_nMasterTableRecordID=-1;
	m_lstData.clear();
	if(m_pTable!=NULL)
	{
		m_pTable->RefreshDisplay();
		m_pTable->SetEditMode(false);
	}

	emit ContentChanged();
}
void NMRXRelationWidget::SetEditMode(bool bEdit)
{
	m_bEdit=bEdit;
	EnableToolbar(bEdit);
	m_pTable->SetEditMode(bEdit);
	m_pBtnInviteSend->setEnabled(!bEdit);
}

void NMRXRelationWidget::EnableToolbar(bool bEnable)
{
	if(m_toolbar!=NULL)
	{
		int nSize=m_toolbar->count();
		for(int i=0;i<nSize;++i)
		{

			QWidget *pWidget=m_toolbar->itemAt(i)->widget();
			if (pWidget)
				pWidget->setEnabled(bEnable);
		}

		m_pieMenu_toolButton->SetEntityRecordID(0);
	}
	
}

DbRecordSet NMRXRelationWidget::GetFilterForRead(int m_nMasterTableRecordID)
{
	return NMRXManager::GetPairListForRead(m_lstPairs,m_nMasterTableID,m_nMasterTableRecordID);
}



//reloads data for given ID
//bSkipRead is used to enable assigment pattern (after isnert) and to avoid read
void NMRXRelationWidget::Reload(QString strMasterEntityCalcualtedName,int nMasterTableRecordID,bool bSkipRead,DbRecordSet *newRecord)
{
	m_nMasterTableRecordID=nMasterTableRecordID;
	m_strMasterEntityCalcualtedName=strMasterEntityCalcualtedName;
/*
	if (nMasterTableRecordID==0) //init as preinsert= no save is done, only at end..
		m_bSkipWriteMode=true;
	else
		m_bSkipWriteMode=false;
*/
	//if (!m_bSkipWriteMode)
	//{

	//m_lstPairs.Dump();

		if (!bSkipRead)
		{
			DbRecordSet lstReadFilter=NMRXManager::GetPairListForRead(m_lstPairs,m_nMasterTableID,m_nMasterTableRecordID);
			Status err;

			//lstReadFilter.Dump();

			_SERVER_CALL(BusNMRX->ReadMultiPairs(err,lstReadFilter,m_lstData,m_nX_TableID))
				if(!err.IsOK())
				{
					QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
					return;
				}
		}
	//}

		//m_lstData.Dump();

	//assign it, if valid:
	if (newRecord)
	{
		//TestListForDuplicateAssignments(m_nAssigmentTableID_1,*newRecord,false);
		//TestListForDuplicateAssignments(m_nAssigmentTableID_2,*newRecord,false);
		//if (newRecord->getRowCount()>0)
		//{
			m_lstData=*newRecord;
		//}
	}

	//enable:
	//if (!m_bEdit)
	SetEditMode(m_bEdit);


	RefreshNames();
	m_lstData.clearSelection();		//remove selection caused by refresh names
	m_pTable->RefreshDisplay();

	emit ContentChanged();

}




//refresh all names of all assigned entities from cache/ auto reload from server if not found...
void NMRXRelationWidget::RefreshNames(int nRow)
{
	//loop through list
	QString strAssignName;
	int nAssigmentID,nKeyID;
	int nSize=m_lstData.getRowCount();
	int nRowEnd=nSize;

	//determine boundaries if 1 or whole lot
	if (nRow==-1)
	{
		nRow=0;
		//avoid one by one server call, reload all names into cache
		BuildCacheLoadTable();
	}
	else
		nRowEnd=nRow+1;

	//sort by key1



	for (int i=nRow;i<nRowEnd;++i)
	{
		//determine what is assignment table
		if (m_lstData.getDataRef(i,m_nTable1Idx)==m_nMasterTableID && m_lstData.getDataRef(i,m_nKey1Idx)==m_nMasterTableRecordID)
		{
			nAssigmentID=m_lstData.getDataRef(i,m_nTable2Idx).toInt();
			nKeyID=m_lstData.getDataRef(i,m_nKey2Idx).toInt();
			//get assignment name from cache:
			if (nAssigmentID==m_nAssigmentTableID_1)
				strAssignName=m_EntityAssigment1.GetCalculatedName(nKeyID);
			else
				strAssignName=m_EntityAssigment2.GetCalculatedName(nKeyID);

			m_lstData.setData(i,m_nNameIdx_1,m_strMasterEntityCalcualtedName);
			m_lstData.setData(i,m_nNameIdx_2,strAssignName);
		}
		else
		{
			nAssigmentID=m_lstData.getDataRef(i,m_nTable1Idx).toInt();
			nKeyID=m_lstData.getDataRef(i,m_nKey1Idx).toInt();
			//get assignment name from cache:
			if (nAssigmentID==m_nAssigmentTableID_1)
				strAssignName=m_EntityAssigment1.GetCalculatedName(nKeyID);
			else
				strAssignName=m_EntityAssigment2.GetCalculatedName(nKeyID);

			m_lstData.setData(i,m_nNameIdx_2,m_strMasterEntityCalcualtedName);
			m_lstData.setData(i,m_nNameIdx_1,strAssignName);
		}
	
	}

}

//create new assigment:
void NMRXRelationWidget::OnAssign1(int nDefaultID)
{
	if (m_bReadOnlyMode)
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}

	Q_ASSERT(m_nMasterTableRecordID!=-1);

	Dlg_NMRXEditor Dlg;
	DbRecordSet row;
	if (m_nMasterTableID==BUS_PROJECT)
	{
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
		row.addRow();
		row.setData(0,"BNMR_TABLE_2",m_nMasterTableID);
		row.setData(0,"BNMR_TABLE_1",m_nAssigmentTableID_1);
		row.setData(0,"BNMR_TABLE_KEY_ID_2",m_nMasterTableRecordID);
		if (nDefaultID!=-1)
			row.setData(0,"BNMR_TABLE_KEY_ID_1",nDefaultID);
	}
	else
	{
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
		row.addRow();
		row.setData(0,"BNMR_TABLE_1",m_nMasterTableID);
		row.setData(0,"BNMR_TABLE_2",m_nAssigmentTableID_1);
		row.setData(0,"BNMR_TABLE_KEY_ID_1",m_nMasterTableRecordID);
		if (nDefaultID!=-1)
			row.setData(0,"BNMR_TABLE_KEY_ID_2",nDefaultID);

	}

	//set default:
	DbRecordSet rowRecordX;
	NMRXManager::DefineDefaultRecordX(m_nX_TableID,rowRecordX);
	row.setData(0,"X_RECORD",rowRecordX);

	//bool bSwitchMaster = m_nMasterTableID==CE_COMM_ENTITY || (m_nMasterTableID==BUS_CM_CONTACT && m_nAssigmentTableID_1 == BUS_CM_CONTACT);
	bool bSwitchMaster = m_nMasterTableID==CE_COMM_ENTITY || m_nMasterTableID==BUS_CM_CONTACT || m_nMasterTableID==BUS_PERSON;
	//if (!m_bReverse) bSwitchMaster=false;

	//B.T. issue 2586: if contact -> project then contact is master, project is slave
	//B.T. issue 2586: if project -> project then contact is master, project is slave (false, coz is already reversed)
	if (m_nMasterTableID==BUS_CM_CONTACT && m_nAssigmentTableID_1 == BUS_PROJECT || m_nMasterTableID==BUS_PROJECT && m_nAssigmentTableID_1 == BUS_CM_CONTACT)
	{
		bSwitchMaster=false;
	}

	
	Dlg.SetRow(m_nMasterTableID,m_nMasterTableRecordID,m_strMasterEntityCalcualtedName,row,Dlg_NMRXEditor::MODE_INSERT,NMRXManager::GetEntityNameFromTableID(m_nMasterTableID,m_nMasterTableTypeID_1),NMRXManager::GetEntityNameFromTableID(m_nAssigmentTableID_1,m_nTableTypeID_1),m_nX_TableID,m_bSkipWriteMode,m_bDisableRoleSwitch,bSwitchMaster);
	int nRes=Dlg.exec();

	if (nRes==1) //inserted
	{
		

		DbRecordSet newData;
		Dlg.GetRow(newData);

		newData.Dump();

		int nTestAssignTable=m_nAssigmentTableID_1;

		if (m_bSetUsePersonAssignAsContact && m_nAssigmentTableID_1==BUS_PERSON && m_nAssigmentTableID_2==BUS_CM_CONTACT)
		{
			int nPersonID=newData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt();
			int nContactID=-1;
			if (m_EntityAssigment1.GetSelectionEntity()==ENTITY_BUS_PERSON)
			{
				DbRecordSet row;
				m_EntityAssigment1.GetEntityRecord(nPersonID,row);
				if (row.getRowCount()>0)
					nContactID=row.getDataRef(0,"BPER_CONTACT_ID").toInt();
			}
			else if (m_EntityAssigment2.GetSelectionEntity()==ENTITY_BUS_PERSON)
			{
				DbRecordSet row;
				m_EntityAssigment2.GetEntityRecord(nPersonID,row);
				if (row.getRowCount()>0)
					nContactID=row.getDataRef(0,"BPER_CONTACT_ID").toInt();
			}

			if (nContactID<=0)
			{
				QMessageBox::warning(this,tr("Warning"),tr("User is not a Contact! No assignment possible!"));
				return;
			}
			newData.setData(0,"BNMR_TABLE_KEY_ID_2",nContactID);
			newData.setData(0,"BNMR_TABLE_2",BUS_CM_CONTACT);
			nTestAssignTable=BUS_CM_CONTACT;
		}


		m_lstData.merge(newData);

		//if(!TestListForDuplicateAssignments(nTestAssignTable,m_lstData,true))
		//{
			//issue 2217: set invite by S_C if contact is user!
			if (newData.getDataRef(0,"BNMR_TABLE_2").toInt()==BUS_CM_CONTACT)
				if (m_EntityAssigment1.GetPersonIDFromContactID(newData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt())>0)
				{
					DbRecordSet rowRecordX=newData.getDataRef(0,"X_RECORD").value<DbRecordSet>();
					if (rowRecordX.getRowCount()==0)
					{
						NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
					}
					rowRecordX.setData(0,"BCIV_BY_NOTIFICATION",1);
					m_lstData.setData(m_lstData.getRowCount()-1,"X_RECORD",rowRecordX);
				}


				//issue 2539: if logged user or contact clear invite record:
				if (m_nMasterTableTypeID_1==GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART)
				{
					bool bResetInvite=false;
					if (newData.getDataRef(0,"BNMR_TABLE_2").toInt()==BUS_CM_CONTACT)
						if (newData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt()==g_pClientManager->GetPersonContactID())
							bResetInvite=true;
					if (newData.getDataRef(0,"BNMR_TABLE_2").toInt()==BUS_PERSON)
						if (newData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt()==g_pClientManager->GetPersonID())
							bResetInvite=true;

					if (bResetInvite)
					{
						DbRecordSet lstInvite=newData.getDataRef(0,"X_RECORD").value<DbRecordSet>();
						lstInvite.setColValue("BCIV_INVITE",0);
						//newData.setData(0,"X_RECORD",lstInvite);
						m_lstData.setData(m_lstData.getRowCount()-1,"X_RECORD",lstInvite);
					}
				}



			RefreshNames(m_lstData.getRowCount()-1);
			m_pTable->RefreshDisplay();
			emit ContentChanged();
		//}
	}
	
	//qDebug()<<time.elapsed();
}




//create new assigment:
void NMRXRelationWidget::OnAssign2(int nDefaultID)
{
	if (m_bReadOnlyMode)
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}


	Q_ASSERT(m_nMasterTableRecordID!=-1);
	Dlg_NMRXEditor Dlg;
	DbRecordSet row;
	
	if (m_nMasterTableID==BUS_PROJECT)
	{
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
		row.addRow();
		row.setData(0,"BNMR_TABLE_2",m_nMasterTableID);
		row.setData(0,"BNMR_TABLE_1",m_nAssigmentTableID_2);
		row.setData(0,"BNMR_TABLE_KEY_ID_2",m_nMasterTableRecordID);
		if (nDefaultID!=-1)
			row.setData(0,"BNMR_TABLE_KEY_ID_1",nDefaultID);
	}
	else
	{
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
		row.addRow();
		row.setData(0,"BNMR_TABLE_1",m_nMasterTableID);
		row.setData(0,"BNMR_TABLE_2",m_nAssigmentTableID_2);
		row.setData(0,"BNMR_TABLE_KEY_ID_1",m_nMasterTableRecordID);
		if (nDefaultID!=-1)
			row.setData(0,"BNMR_TABLE_KEY_ID_2",nDefaultID);
	}

	//set default:
	DbRecordSet rowRecordX;
	NMRXManager::DefineDefaultRecordX(m_nX_TableID,rowRecordX);
	row.setData(0,"X_RECORD",rowRecordX);

	//bool bSwitchMaster = (bool)(m_nMasterTableID==CE_COMM_ENTITY || (m_nMasterTableID==BUS_CM_CONTACT && m_nAssigmentTableID_2 == BUS_CM_CONTACT));
	bool bSwitchMaster = m_nMasterTableID==CE_COMM_ENTITY || m_nMasterTableID==BUS_CM_CONTACT || m_nMasterTableID==BUS_PERSON;
	//if (!m_bReverse) bSwitchMaster=false;

	//B.T. issue 2586: if contact -> project then contact is master, project is slave
	//B.T. issue 2586: if project -> project then contact is master, project is slave (false, coz is already reversed)
	if (m_nMasterTableID==BUS_CM_CONTACT && m_nAssigmentTableID_2 == BUS_PROJECT || m_nMasterTableID==BUS_PROJECT && m_nAssigmentTableID_2 == BUS_CM_CONTACT)
	{
		bSwitchMaster=false;
	}


	Dlg.SetRow(m_nMasterTableID,m_nMasterTableRecordID,m_strMasterEntityCalcualtedName,row,Dlg_NMRXEditor::MODE_INSERT,NMRXManager::GetEntityNameFromTableID(m_nMasterTableID,m_nMasterTableTypeID_1),NMRXManager::GetEntityNameFromTableID(m_nAssigmentTableID_2,m_nTableTypeID_2),m_nX_TableID,m_bSkipWriteMode,m_bDisableRoleSwitch,bSwitchMaster);
	int nRes=Dlg.exec();
	if (nRes==1) //inserted
	{
		DbRecordSet newData;
		Dlg.GetRow(newData);

		int nTestAssignTable=m_nAssigmentTableID_2;
		if (m_bSetUsePersonAssignAsContact && m_nAssigmentTableID_2==BUS_PERSON && m_nAssigmentTableID_1==BUS_CM_CONTACT)
		{
			int nPersonID=newData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt();
			int nContactID=-1;
			if (m_EntityAssigment1.GetSelectionEntity()==ENTITY_BUS_PERSON)
			{
				DbRecordSet row;
				m_EntityAssigment1.GetEntityRecord(nPersonID,row);
				if (row.getRowCount()>0)
					nContactID=row.getDataRef(0,"BPER_CONTACT_ID").toInt();
			}
			else if (m_EntityAssigment2.GetSelectionEntity()==ENTITY_BUS_PERSON)
			{
				DbRecordSet row;
				m_EntityAssigment2.GetEntityRecord(nPersonID,row);
				if (row.getRowCount()>0)
					nContactID=row.getDataRef(0,"BPER_CONTACT_ID").toInt();
			}

			if (nContactID<=0)
			{
				QMessageBox::warning(this,tr("Warning"),tr("User is not a Contact! No assignment possible!"));
				return;
			}
			newData.setData(0,"BNMR_TABLE_KEY_ID_2",nContactID);
			newData.setData(0,"BNMR_TABLE_2",BUS_CM_CONTACT);
			nTestAssignTable=BUS_CM_CONTACT;
		}

		m_lstData.merge(newData);

		//if(!TestListForDuplicateAssignments(nTestAssignTable,m_lstData,true))
		//{
			//issue 2217: set invite by S_C if contact is user!
			if (newData.getDataRef(0,"BNMR_TABLE_2").toInt()==BUS_CM_CONTACT)
				if (m_EntityAssigment1.GetPersonIDFromContactID(newData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt())>0)
				{
					DbRecordSet rowRecordX=newData.getDataRef(0,"X_RECORD").value<DbRecordSet>();
					if (rowRecordX.getRowCount()==0)
					{
						NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
					}
					rowRecordX.setData(0,"BCIV_BY_NOTIFICATION",1);
					m_lstData.setData(m_lstData.getRowCount()-1,"X_RECORD",rowRecordX);
				}

				//issue 2539: if logged user or contact clear invite record:
				if (m_nMasterTableTypeID_1==GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART)
				{
					bool bResetInvite=false;
					if (newData.getDataRef(0,"BNMR_TABLE_2").toInt()==BUS_CM_CONTACT)
						if (newData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt()==g_pClientManager->GetPersonContactID())
							bResetInvite=true;
					if (newData.getDataRef(0,"BNMR_TABLE_2").toInt()==BUS_PERSON)
						if (newData.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt()==g_pClientManager->GetPersonID())
							bResetInvite=true;

					if (bResetInvite)
					{
						DbRecordSet lstInvite=newData.getDataRef(0,"X_RECORD").value<DbRecordSet>();
						lstInvite.setColValue("BCIV_INVITE",0);
						//newData.setData(0,"X_RECORD",lstInvite);
						m_lstData.setData(m_lstData.getRowCount()-1,"X_RECORD",lstInvite);
					}
				}
		


			RefreshNames(m_lstData.getRowCount()-1);
			m_pTable->RefreshDisplay();
			emit ContentChanged();
		//}
	}

}


//edit
void NMRXRelationWidget::OnEdit()
{
	if (m_bReadOnlyMode)
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}


	Q_ASSERT(m_nMasterTableRecordID!=-1);

	Dlg_NMRXEditor Dlg;

	//get current row:
	int nSelectedRow=m_lstData.getSelectedRow();
	if (m_lstData.getSelectedCount()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select an assignment!"));
		return;
	}
	DbRecordSet row;
	row=m_lstData.getRow(nSelectedRow);

	//determine what is assignment table (can be 2 different)
	int nSlaveID=-1;
	if (row.getDataRef(0,m_nTable1Idx)==m_nMasterTableID && row.getDataRef(0,m_nKey1Idx)==m_nMasterTableRecordID)
	{
		nSlaveID=row.getDataRef(0,"BNMR_TABLE_2").toInt();
	}
	else
	{
		nSlaveID=row.getDataRef(0,"BNMR_TABLE_1").toInt();
	}

	int nSlaveTypeID=-1;
	if (nSlaveID==m_nAssigmentTableID_1)
		nSlaveTypeID=m_nTableTypeID_1;
	else
		nSlaveTypeID=m_nTableTypeID_2;

	//go edit: lock & load:
	//bool bSwitchMaster = (bool)(m_nMasterTableID==CE_COMM_ENTITY  || (m_nMasterTableID==BUS_CM_CONTACT && nSlaveTypeID == BUS_CM_CONTACT));
	bool bSwitchMaster = m_nMasterTableID==CE_COMM_ENTITY; //|| m_nMasterTableID==BUS_CM_CONTACT;
	//if (!m_bReverse) bSwitchMaster=false;

	Dlg.SetRow(m_nMasterTableID,m_nMasterTableRecordID,m_strMasterEntityCalcualtedName,row,Dlg_NMRXEditor::MODE_EDIT,NMRXManager::GetEntityNameFromTableID(m_nMasterTableID,m_nMasterTableTypeID_1),NMRXManager::GetEntityNameFromTableID(nSlaveID,nSlaveTypeID),m_nX_TableID,m_bSkipWriteMode,m_bDisableRoleSwitch,bSwitchMaster);
	int nRes=Dlg.exec();
	if (nRes==1)			//changed
	{
		DbRecordSet newData;
		Dlg.GetRow(newData);
		m_lstData.assignRow(nSelectedRow,newData);
		RefreshNames(nSelectedRow);
		m_pTable->RefreshDisplay();
	}
	else if (nRes==2)		//deleted
	{
		m_lstData.deleteSelectedRows();
		m_pTable->RefreshDisplay();
	}



}


//show, can be edited later on, check status
void NMRXRelationWidget::OnShow()
{
	if(!m_bEdit)return;

	Q_ASSERT(m_nMasterTableRecordID!=-1);

	Dlg_NMRXEditor Dlg;

	//get current row:
	int nSelectedRow=m_lstData.getSelectedRow();
	if (m_lstData.getSelectedCount()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select an assignment!"));
		return;
	}
	DbRecordSet row;
	row=m_lstData.getRow(nSelectedRow);

	//determine what is assignment table (can be 2 different)
	int nSlaveID=-1;
	if (row.getDataRef(0,m_nTable1Idx)==m_nMasterTableID && row.getDataRef(0,m_nKey1Idx)==m_nMasterTableRecordID)
	{
		nSlaveID=row.getDataRef(0,"BNMR_TABLE_2").toInt();
	}
	else
	{
		nSlaveID=row.getDataRef(0,"BNMR_TABLE_1").toInt();
	}


	int nSlaveTypeID=-1;
	if (nSlaveID==m_nAssigmentTableID_1)
		nSlaveTypeID=m_nTableTypeID_1;
	else
		nSlaveTypeID=m_nTableTypeID_2;

	int nMode=Dlg_NMRXEditor::MODE_READ;
	if (m_bEdit)
	{
		nMode=Dlg_NMRXEditor::MODE_EDIT;
	}

	if (m_bReadOnlyMode)
		nMode=Dlg_NMRXEditor::MODE_READ;

	//bool bSwitchMaster = (bool)(m_nMasterTableID==CE_COMM_ENTITY  || (m_nMasterTableID==BUS_CM_CONTACT && nSlaveTypeID == BUS_CM_CONTACT));
	//B.T. 07.03.2013: disable switching on contact:....
	bool bSwitchMaster = m_nMasterTableID==CE_COMM_ENTITY;// || m_nMasterTableID==BUS_CM_CONTACT;
	//if (!m_bReverse) bSwitchMaster=false;


	//go edit: lock & load:
	Dlg.SetRow(m_nMasterTableID,m_nMasterTableRecordID,m_strMasterEntityCalcualtedName,row,nMode,NMRXManager::GetEntityNameFromTableID(m_nMasterTableID,m_nMasterTableTypeID_1),NMRXManager::GetEntityNameFromTableID(nSlaveID,nSlaveTypeID),m_nX_TableID,m_bSkipWriteMode,m_bDisableRoleSwitch,bSwitchMaster,m_bReadOnlyMode);
	int nRes=Dlg.exec();
	if (nRes==1)			//changed
	{
		DbRecordSet newData;
		Dlg.GetRow(newData);
		m_lstData.assignRow(nSelectedRow,newData);
		RefreshNames(nSelectedRow);
		m_pTable->RefreshDisplay();
	}
	else if (nRes==2)		//deleted
	{
		m_lstData.deleteSelectedRows();
		m_pTable->RefreshDisplay();
	}



}


//lock and delete:
void NMRXRelationWidget::OnDelete()
{
	if (m_bReadOnlyMode)
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}


	QString strLockedRes;
	Status err;
	
	//get row:
	if (m_lstData.getSelectedCount()<0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select an assignment!"));
		return;
	}
	DbRecordSet lstData;
	lstData=m_lstData.getSelectedRecordSet();

	//lock if not already
	if(strLockedRes.isEmpty())
	{
		//int nID=m_lstData.getDataRef(nSelectedRow,"BNMR_ID").toInt();
		//Q_ASSERT(nID!=0);

		_SERVER_CALL(BusNMRX->LockMulti(err,lstData,strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
	}


	//delete it:

	_SERVER_CALL(BusNMRX->Delete(err,lstData))
	if(!err.IsOK())
	{
		if(!strLockedRes.isEmpty())
		{
			Status err_temp;
			_SERVER_CALL(BusNMRX->Unlock(err_temp,strLockedRes)) //unlock
		}
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//unlock if allowed:
	if(!strLockedRes.isEmpty())
	{
		_SERVER_CALL(BusNMRX->Unlock(err,strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
	}

	m_lstData.deleteSelectedRows();
	m_pTable->RefreshDisplay();

	emit ContentChanged();

}



void NMRXRelationWidget::OnDeleteAll()
{
	if (m_bReadOnlyMode)
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return; 
	}


	QString strLockedRes;
	Status err;

	//copy only ID's_:
	DbRecordSet lstForDelete;
	lstForDelete.addColumn(QVariant::Int,"BNMR_ID");
	lstForDelete.merge(m_lstData);
	if (lstForDelete.getRowCount()==0)
		return;


	//lock if not already
	if(strLockedRes.isEmpty())
	{

		_SERVER_CALL(BusNMRX->LockMulti(err,lstForDelete,strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
	}


	//delete it:

	_SERVER_CALL(BusNMRX->Delete(err,lstForDelete))
	if(!err.IsOK())
	{
		if(!strLockedRes.isEmpty())
		{
			Status err_temp;
			_SERVER_CALL(BusNMRX->Unlock(err_temp,strLockedRes)) //unlock
		}
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//unlock if allowed:
	if(!strLockedRes.isEmpty())
	{
		_SERVER_CALL(BusNMRX->Unlock(err,strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
	}
	
	m_lstData.clear();
	m_pTable->RefreshDisplay();
	emit ContentChanged();

}


void NMRXRelationWidget::OnShowDetails()
{

	//get row
	int nSelectedRow=m_lstData.getSelectedRow();
	if (m_lstData.getSelectedCount()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select an assignment!"));
		return;
	}


	//determine what is assignment table
	int nAssigmentID,nKeyID;
	if (m_lstData.getDataRef(nSelectedRow,m_nTable1Idx)==m_nMasterTableID && m_lstData.getDataRef(nSelectedRow,m_nKey1Idx)==m_nMasterTableRecordID)
	{
		nAssigmentID=m_lstData.getDataRef(nSelectedRow,m_nTable2Idx).toInt();
		nKeyID=m_lstData.getDataRef(nSelectedRow,m_nKey2Idx).toInt();
	}
	else
	{
		nAssigmentID=m_lstData.getDataRef(nSelectedRow,m_nTable1Idx).toInt();
		nKeyID=m_lstData.getDataRef(nSelectedRow,m_nKey1Idx).toInt();
	}

	int nSlaveTypeID=-1;
	if (nAssigmentID==m_nAssigmentTableID_1)
		nSlaveTypeID=m_nTableTypeID_1;
	else
		nSlaveTypeID=m_nTableTypeID_2;

	int nEntityTypeID=NMRXManager::GetEntityIDFromTableID(nAssigmentID,nSlaveTypeID);

	if (nEntityTypeID==-1) return;	//if invalid return:

	//determine other ID and FUI (hehe)
	//open FUI, get window pointer
	int nFUI_ID=g_objFuiManager.FromEntityToMenuID(nEntityTypeID);
	int nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_READ,nKeyID,true);
	QWidget *pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	pLastFUIOpen->show();  //show FUI

}



int NMRXRelationWidget::GetRowCountForAssignment(int nTableID)
{
	int nCount=0;
	int nSize=m_lstData.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if(m_lstData.getDataRef(i,m_nTable1Idx).toInt()==m_nMasterTableID)
		{
			if(m_lstData.getDataRef(i,m_nTable2Idx).toInt()==nTableID)
				nCount++;
		}
		else
		{
			if(m_lstData.getDataRef(i,m_nTable1Idx).toInt()==nTableID)
				nCount++;

		}
	}

	return nCount;
}

//to avoid readinig from server, one by one record, load whole bunch of needed names inot cache
//T1, T2 and T3, possible tables + 
void NMRXRelationWidget::BuildCacheLoadTable()
{

	//Asignment table 1: cache load
	DbRecordSet lstData1;
	lstData1.addColumn(QVariant::Int,"BNMR_TABLE_KEY_ID_1");

	m_lstData.find(m_nTable1Idx,m_nAssigmentTableID_1);
	lstData1.merge(m_lstData,true);
	lstData1.setColumn(0,QVariant::Int,"BNMR_TABLE_KEY_ID_2");
	m_lstData.find(m_nTable2Idx,m_nAssigmentTableID_1);
	lstData1.merge(m_lstData,true);
	lstData1.removeDuplicates(0);
	if (lstData1.getRowCount()>0)
	{
		DbRecordSet lstData;
		m_EntityAssigment1.GetEntityRecords(lstData1,0,lstData);
	}


	//Asignment table 2: cache load
	if (m_nAssigmentTableID_2==-1) return;

	lstData1.destroy();
	lstData1.addColumn(QVariant::Int,"BNMR_TABLE_KEY_ID_1");

	m_lstData.find(m_nTable1Idx,m_nAssigmentTableID_2);
	lstData1.merge(m_lstData,true);
	lstData1.setColumn(0,QVariant::Int,"BNMR_TABLE_KEY_ID_2");
	m_lstData.find(m_nTable2Idx,m_nAssigmentTableID_2);
	lstData1.merge(m_lstData,true);
	lstData1.removeDuplicates(0);
	if (lstData1.getRowCount()>0)
	{
		DbRecordSet lstData;
		m_EntityAssigment2.GetEntityRecords(lstData1,0,lstData);
	}





}




void NMRXRelationWidget::OnSelectionChanged()
{
	if (!m_pieMenu_toolButton->isVisible()) return; //no contact data

	int nSelectedCnt=m_lstData.getSelectedCount();
	if (nSelectedCnt==1)
	{
		int i=m_lstData.getSelectedRow();
		if (m_nAssigmentTableID_1==BUS_CM_CONTACT)
		{
			
			if (m_lstData.getDataRef(i,m_nTable1Idx)==m_nMasterTableID && m_lstData.getDataRef(i,m_nTable2Idx)==m_nAssigmentTableID_1 && m_lstData.getDataRef(i,m_nKey2Idx)!=m_nMasterTableRecordID)
			{
				m_pieMenu_toolButton->SetEntityRecordID(m_lstData.getDataRef(i,m_nKey2Idx).toInt());
			}
			else if (m_lstData.getDataRef(i,m_nTable2Idx)==m_nMasterTableID && m_lstData.getDataRef(i,m_nTable1Idx)==m_nAssigmentTableID_1 && m_lstData.getDataRef(i,m_nKey1Idx)!=m_nMasterTableRecordID)
			{
				m_pieMenu_toolButton->SetEntityRecordID(m_lstData.getDataRef(i,m_nKey1Idx).toInt());
			}
			else
				m_pieMenu_toolButton->SetEntityRecordID(0);
			
			return;
		}

		if (m_nAssigmentTableID_2==BUS_CM_CONTACT)
		{
			if (m_lstData.getDataRef(i,m_nTable1Idx)==m_nMasterTableID && m_lstData.getDataRef(i,m_nTable2Idx)==m_nAssigmentTableID_2  && m_lstData.getDataRef(i,m_nKey2Idx)!=m_nMasterTableRecordID)
			{
				m_pieMenu_toolButton->SetEntityRecordID(m_lstData.getDataRef(i,m_nKey2Idx).toInt());
			}
			else if (m_lstData.getDataRef(i,m_nTable2Idx)==m_nMasterTableID && m_lstData.getDataRef(i,m_nTable1Idx)==m_nAssigmentTableID_2   && m_lstData.getDataRef(i,m_nKey1Idx)!=m_nMasterTableRecordID)
			{
				m_pieMenu_toolButton->SetEntityRecordID(m_lstData.getDataRef(i,m_nKey1Idx).toInt());
			}
			else
				m_pieMenu_toolButton->SetEntityRecordID(0);

			return;
		}

	}
	else
		m_pieMenu_toolButton->SetEntityRecordID(0);
		
}

//create default list
void NMRXRelationWidget::CreateAssignmentData(DbRecordSet &lstNMRXData,int nMasterTableID,int nAssigmentTableID,QList<int> &lstIDForAssign, QString strDefaultRoleName,int nMasterTableRecordID,int nX_TableID)
{
	if (lstIDForAssign.size()==0)
		return;

	DbRecordSet data;
	data.addColumn(QVariant::Int,"ID");

	int nSize=lstIDForAssign.size();
	data.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		data.setData(i,0,lstIDForAssign.at(i));
	}

	DbRecordSet rowRole;
	if (!strDefaultRoleName.isEmpty())
	{
		Status err;
		//bo call: check role: if exists return row, if not inserts
		bool bInserted=false;
		DbRecordSet empty;
		_SERVER_CALL(BusNMRX->CreateAssignmentData(err,lstNMRXData,nMasterTableID,nAssigmentTableID,data,strDefaultRoleName,nMasterTableRecordID,nX_TableID,empty,bInserted));
		_CHK_ERR(err);
		if (bInserted)
			g_ClientCache.PurgeCache(ENTITY_NMRX_ROLES); //clear cache to reload new role if inserted..
	}

	/*
	MainEntitySelectionController mPersonCache;
	mPersonCache.Initialize(BUS_PERSON);
	mPersonCache.ReloadData();

	lstNMRXData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	int nSize=lstIDForAssign.size();
	for(int i=0;i<nSize;++i)
	{
		lstNMRXData.addRow();
		int nRow=lstNMRXData.getRowCount()-1;
		lstNMRXData.setData(nRow,"BNMR_TABLE_1",nMasterTableID);
		lstNMRXData.setData(nRow,"BNMR_TABLE_2",nAssigmentTableID);
		lstNMRXData.setData(nRow,"BNMR_TABLE_KEY_ID_1",nMasterTableRecordID);
		lstNMRXData.setData(nRow,"BNMR_TABLE_KEY_ID_2",lstIDForAssign.at(i));
	
		DbRecordSet rowRecordX;
		NMRXManager::DefineDefaultRecordX(nX_TableID,rowRecordX);

		//issue 2217: set invite by S_C if contact is user!
		if (nAssigmentTableID==BUS_CM_CONTACT)
			if (mPersonCache.GetPersonIDFromContactID(lstIDForAssign.at(i))>0)
			{
				rowRecordX.setData(0,"BCIV_BY_NOTIFICATION",1);
			}
		lstNMRXData.setData(nRow,"X_RECORD",rowRecordX);

		if (rowRole.getRowCount()>0)
		{
			lstNMRXData.setData(nRow,"BNMR_ROLE_ID",rowRole.getDataRef(0,"BNRO_ID"));
			lstNMRXData.setData(nRow,"BNRO_NAME",rowRole.getDataRef(0,"BNRO_NAME"));
			lstNMRXData.setData(nRow,"BNRO_ASSIGMENT_TEXT",rowRole.getDataRef(0,"BNRO_ASSIGMENT_TEXT"));
			lstNMRXData.setData(nRow,"BNRO_DESCRIPTION",rowRole.getDataRef(0,"BNRO_DESCRIPTION").toString());

			lstNMRXData.assignRow(nRow,rowRole,true); //set new role id if defined
		}
	}

	lstNMRXData.setColValue("BNMR_TYPE",QVariant(GlobalConstants::NMRX_TYPE_ASSIGN).toInt());
*/
}

//new: default role if defined, saves new role if not as old system
//if master record loaded, assignes to it with empty role
void NMRXRelationWidget::AssignData(int nAssigmentTableID,QList<int> &lstIDForAssign, QString strDefaultRoleName)
{
	//if invalidated:
	if (m_nMasterTableRecordID==-1)
		return;
	//if assignment not possible
	if (nAssigmentTableID!=m_nAssigmentTableID_1 && nAssigmentTableID!=m_nAssigmentTableID_2)
		return;

	//get id if role exists in DB, if not then insert, get name, desc,etc..
	DbRecordSet rowRole;
	if (!strDefaultRoleName.isEmpty())
	{
		Status err;
		//bo call: check role: if exists return row, if not inserts
		bool bInserted=false;
		_SERVER_CALL(BusNMRX->CheckRole(err,m_nMasterTableRecordID,nAssigmentTableID,strDefaultRoleName,rowRole,bInserted));
		_CHK_ERR(err);
		if (bInserted)
			g_ClientCache.PurgeCache(ENTITY_NMRX_ROLES); //clear cache to reload new role if inserted..
	}
	else
	{
		if (lstIDForAssign.size()==1 && nAssigmentTableID==m_nAssigmentTableID_1)
		{
			OnAssign1(lstIDForAssign.at(0));
			return;
		}

		if (lstIDForAssign.size()==1 && nAssigmentTableID==m_nAssigmentTableID_2)
		{
			OnAssign2(lstIDForAssign.at(0));
			return;
		}
	}

	DbRecordSet rowDateForWrite;
	rowDateForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	int nSize=lstIDForAssign.size();
	for(int i=0;i<nSize;++i)
	{
		rowDateForWrite.addRow();
		int nRow=rowDateForWrite.getRowCount()-1;
		if (m_nMasterTableID==BUS_PROJECT)
		{
			rowDateForWrite.setData(nRow,"BNMR_TABLE_2",m_nMasterTableID);
			rowDateForWrite.setData(nRow,"BNMR_TABLE_1",nAssigmentTableID);
			rowDateForWrite.setData(nRow,"BNMR_TABLE_KEY_ID_2",m_nMasterTableRecordID);
			rowDateForWrite.setData(nRow,"BNMR_TABLE_KEY_ID_1",lstIDForAssign.at(i));
		}
		else
		{
			rowDateForWrite.setData(nRow,"BNMR_TABLE_1",m_nMasterTableID);
			rowDateForWrite.setData(nRow,"BNMR_TABLE_2",nAssigmentTableID);
			rowDateForWrite.setData(nRow,"BNMR_TABLE_KEY_ID_1",m_nMasterTableRecordID);
			rowDateForWrite.setData(nRow,"BNMR_TABLE_KEY_ID_2",lstIDForAssign.at(i));
		}

		DbRecordSet rowRecordX;
		NMRXManager::DefineDefaultRecordX(m_nX_TableID,rowRecordX);

		//issue 2217: set invite by S_C if contact is user!
		if (nAssigmentTableID==BUS_CM_CONTACT)
			if (m_EntityAssigment1.GetPersonIDFromContactID(lstIDForAssign.at(i))>0)
			{
				rowRecordX.setData(0,"BCIV_BY_NOTIFICATION",1);
			}
		rowDateForWrite.setData(nRow,"X_RECORD",rowRecordX);

		if (rowRole.getRowCount()>0)
			rowDateForWrite.assignRow(nRow,rowRole,true); //set new role id if defined
	}

	rowDateForWrite.setColValue("BNMR_TYPE",QVariant(GlobalConstants::NMRX_TYPE_ASSIGN).toInt());

	TestListForDuplicateAssignments(nAssigmentTableID,rowDateForWrite,true);
	if (rowDateForWrite.getRowCount()>0)
	{
		if (m_bSkipWriteMode)
		{
			m_lstData.merge(rowDateForWrite);
			RefreshNames();
			m_pTable->RefreshDisplay();
			emit ContentChanged();
			return;
		}
		else
		{
			//save relation:
			Status err;
			_SERVER_CALL(BusNMRX->WriteSimple(err,rowDateForWrite,"",m_nX_TableID))
				_CHK_ERR(err);
			//reload to be sure:
			Reload(m_strMasterEntityCalcualtedName,m_nMasterTableRecordID,false);
		}
	}

}


//selected data is in format: <id,name> e.g. for contacts: <BCNT_ID,BCNT_NAME>
void NMRXRelationWidget::GetSelectedData(int nAssigmentTableID,DbRecordSet &lstData, bool bAllRecords /* not only selected */)
{
	QString strIDName=DbSqlTableDefinition::GetTablePrimaryKey(nAssigmentTableID);
	QString strNameCol=NMRXManager::GetNameColumnFromTableID(nAssigmentTableID);
	
	lstData.addColumn(QVariant::Int,strIDName);
	lstData.addColumn(QVariant::String,strNameCol);

	DbRecordSet lstTemp;
	lstTemp.copyDefinition(m_lstData);
	lstTemp.merge(m_lstData,!bAllRecords);
	lstTemp.clearSelection();
	lstTemp.find("BNMR_TABLE_1",nAssigmentTableID,false);
	lstTemp.deleteUnSelectedRows();
	lstTemp.renameColumn(lstTemp.getColumnIdx("BNMR_TABLE_KEY_ID_1"),strIDName);
	lstTemp.renameColumn(lstTemp.getColumnIdx("NAME_1"),strNameCol);
	lstData.merge(lstTemp);

	lstTemp.clear();
	lstTemp.merge(m_lstData,!bAllRecords);
	lstTemp.clearSelection();
	lstTemp.find("BNMR_TABLE_2",nAssigmentTableID,false);
	lstTemp.deleteUnSelectedRows();
	lstTemp.renameColumn(lstTemp.getColumnIdx("BNMR_TABLE_KEY_ID_2"),strIDName);
	lstTemp.renameColumn(lstTemp.getColumnIdx("NAME_2"),strNameCol);
	lstData.merge(lstTemp);


}


void NMRXRelationWidget::On_NewDataDropped(int nEntityTypeID,DbRecordSet lstData)
{
	if (nEntityTypeID==NMRXManager::GetEntityIDFromTableID(m_nAssigmentTableID_1,m_nTableTypeID_1))
	{
		QList<int> lstID;
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			lstID<<lstData.getDataRef(i,DbSqlTableDefinition::GetTablePrimaryKey(m_nAssigmentTableID_1)).toInt();
		}
		AssignData(m_nAssigmentTableID_1,lstID);
		return;
	}

	if (nEntityTypeID==NMRXManager::GetEntityIDFromTableID(m_nAssigmentTableID_2,m_nTableTypeID_2))
	{
		QList<int> lstID;
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			lstID<<lstData.getDataRef(i,DbSqlTableDefinition::GetTablePrimaryKey(m_nAssigmentTableID_2)).toInt();
		}
		AssignData(m_nAssigmentTableID_2,lstID);
		return;
	}
}

void NMRXRelationWidget::GetData(DbRecordSet &lstData)
{
	m_pTable->StoreCustomWidgetChanges();
	lstData=m_lstData;
}

//if was preinsertmode, reset
void NMRXRelationWidget::WriteChangedRecords(Status &err,int nMasterTableRecordID)
{
	Q_ASSERT(m_bSkipWriteMode==true); //must be in preinsert mode

	int nSize=m_lstData.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_lstData.getDataRef(i,"BNMR_TABLE_1").toInt()==m_nMasterTableID && m_lstData.getDataRef(i,"BNMR_TABLE_KEY_ID_1").toInt()==0)
		{
			m_lstData.setData(i,"BNMR_TABLE_KEY_ID_1",nMasterTableRecordID);
		}
		if (m_lstData.getDataRef(i,"BNMR_TABLE_2").toInt()==m_nMasterTableID && m_lstData.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt()==0)
		{
			m_lstData.setData(i,"BNMR_TABLE_KEY_ID_2",nMasterTableRecordID);
		}
	}

	m_pTable->StoreCustomWidgetChanges();

	_SERVER_CALL(BusNMRX->Write(err,m_lstData,"",m_nX_TableID))
	if(err.IsOK())
	{
		Reload(m_strMasterEntityCalcualtedName,nMasterTableRecordID);
	}
}

void NMRXRelationWidget::OnShowCalendar()
{
	DbRecordSet lstData1,lstData2;
	GetSelectedData(m_nAssigmentTableID_1,lstData1);
	if (m_nAssigmentTableID_2!=-1)
		GetSelectedData(m_nAssigmentTableID_2,lstData2);

	if (m_nAssigmentTableID_1==BUS_CM_CONTACT)
	{
		if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS))
		{
			int nSize=lstData1.getRowCount();
			for(int i=0;i<nSize;i++)
			{
				int nPersonID=m_EntityAssigment1.GetPersonIDFromContactID(lstData1.getDataRef(i,"BCNT_ID").toInt());
				if(nPersonID>0 && nPersonID != g_pClientManager->GetPersonID()) //if person_id then implement this rule if not exists 
					return;
			}
		}
		if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS))
		{
			return;
		}
	}

	if (m_nAssigmentTableID_1==BUS_PROJECT)
	{
		if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_PROJECT_CALENDARS))
		{
			return;
		}
	}
	if (m_nAssigmentTableID_1==BUS_CAL_RESOURCE)
	{
		if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_RESOURCE_CALENDARS))
		{
			return;
		}
	}


	//lstData1.Dump();
	//lstData2.Dump();
	if (lstData1.getRowCount()>0 || lstData2.getRowCount()>0)
		emit OpenCalendar(m_nAssigmentTableID_1,lstData1,m_nAssigmentTableID_2,lstData2);

}

//RetNmrxDataForInvite = BUS_CAL_INVITE + TABLE_ID<cont or person> + ID; 
//bool: ret true if ok, false if user aborted
bool NMRXRelationWidget::GetInviteRecordsForNewInvite(DbRecordSet &lstNMRXData,DbRecordSet &RetNmrxDataForInvite, bool bIgnoreSelection)
{
	static bool s_ReloadPersonsFromServer=true;
	//lstNMRXData.Dump();

	MainEntitySelectionController cachePerson;
	cachePerson.Initialize(ENTITY_BUS_PERSON);
	cachePerson.ReloadData(s_ReloadPersonsFromServer);
	s_ReloadPersonsFromServer=false;//only 1st time reload


	RetNmrxDataForInvite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_SELECT));

	if (lstNMRXData.getColumnIdx("X_RECORD")==-1)
		return true;

	int nSize=lstNMRXData.getRowCount();
	if (lstNMRXData.getSelectedCount()==nSize || lstNMRXData.getSelectedCount()==0)
	{
		bIgnoreSelection=true;
	}
	if (lstNMRXData.getSelectedCount()==1 && !bIgnoreSelection)
	{
		int nRowSelected=lstNMRXData.getSelectedRow();
		if (nRowSelected>=0)
			if (!lstNMRXData.getDataRef(nRowSelected,"X_RECORD").isNull())
			{
				DbRecordSet rowInvite=lstNMRXData.getDataRef(nRowSelected,"X_RECORD").value<DbRecordSet>();
				if (rowInvite.getRowCount()>0)
					if (rowInvite.getDataRef(0,"BCIV_INVITE").toInt()>0 && rowInvite.getDataRef(0,"BCIV_IS_SENT").toInt()==0)
					{

						QString strTitle=tr("Please confirm invitation send");
						QMessageBox box;
						QPushButton *btn1=box.addButton(tr("Send Invitation to the selected user/contact only"),QMessageBox::AcceptRole);
						QPushButton *btn2=box.addButton(tr("Send Invitation to all"),QMessageBox::AcceptRole);
						QPushButton *btn4=box.addButton(tr("Cancel"),QMessageBox::RejectRole);

						box.setIcon(QMessageBox::Question);
						box.setDefaultButton(btn1);
						box.setInformativeText(strTitle);
						box.setWindowTitle(strTitle);
						int nResult=box.exec();
						if (nResult==0)
						{
							bIgnoreSelection=false;
						}
						else if (nResult==1)
						{
							bIgnoreSelection=true;
						}
						else
						{
							return false;
						}

					}
			}
	}


	for(int i=0;i<nSize;i++)
	{
		if (lstNMRXData.getDataRef(i,"X_RECORD").isNull())
			continue;
		DbRecordSet rowInvite=lstNMRXData.getDataRef(i,"X_RECORD").value<DbRecordSet>();

		//rowInvite.Dump();

		if (rowInvite.getRowCount()>0)
		{
			if (rowInvite.getDataRef(0,"BCIV_INVITE").toInt()>0 && rowInvite.getDataRef(0,"BCIV_IS_SENT").toInt()==0)
			{
				if (!bIgnoreSelection && !lstNMRXData.isRowSelected(i)) 
					continue;

				RetNmrxDataForInvite.merge(rowInvite);
				
				
				if (lstNMRXData.getDataRef(i,"BNMR_TABLE_2")==BUS_PERSON)
				{
					RetNmrxDataForInvite.setData(RetNmrxDataForInvite.getRowCount()-1,"BPER_ID",lstNMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2"));
				}
				else
				{
					int nContactID = lstNMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt();
					RetNmrxDataForInvite.setData(RetNmrxDataForInvite.getRowCount()-1,"BCNT_ID",nContactID);
					DbRecordSet *pData=cachePerson.GetDataSource();
					int nRow=pData->find("BPER_CONTACT_ID",nContactID,true);
					if (nRow>=0)
					{
						RetNmrxDataForInvite.setData(RetNmrxDataForInvite.getRowCount()-1,"BPER_ID",pData->getDataRef(nRow,"BPER_ID"));
					}
				}
			}
		}
	}

	return true;
}

void NMRXRelationWidget::OnInviteSend()
{
	DbRecordSet lstInvite;
	if (NMRXRelationWidget::GetInviteRecordsForNewInvite(m_lstData,lstInvite,false))
	{
		emit SendInvite(lstInvite);
		Reload(m_strMasterEntityCalcualtedName,m_nMasterTableRecordID,false,NULL);
		emit InviteDataChanged();
	}
}

//ID-> 
//return bool = true if duplicate found!
bool NMRXRelationWidget::TestListForDuplicateAssignments(int nAssigmentTableID,DbRecordSet &lstNMRXNewData, bool bShowMsgWarn)
{
	if (lstNMRXNewData.getRowCount()<2)
		return false;
	
	lstNMRXNewData.clearSelection();
	int nSize=lstNMRXNewData.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nID=0;
		if (lstNMRXNewData.getDataRef(i,"BNMR_TABLE_2")==nAssigmentTableID)
			nID=lstNMRXNewData.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt(); //test only assignment 2: can be different roles, but it's ok to preevnt even with diff roles.
		else
			continue;
		//else
		//	nID=lstNMRXNewData.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt();

		for(int z=i+1;z<nSize;z++)
		{
			//if (lstNMRXNewData.getDataRef(z,"BNMR_TABLE_1")==nAssigmentTableID && lstNMRXNewData.getDataRef(z,"BNMR_TABLE_KEY_ID_1")==nID)
			//	lstNMRXNewData.selectRow(z);

			if (lstNMRXNewData.getDataRef(z,"BNMR_TABLE_2")==nAssigmentTableID && lstNMRXNewData.getDataRef(z,"BNMR_TABLE_KEY_ID_2")==nID)
				lstNMRXNewData.selectRow(z);

		}
	}
	bool bDuplicateFound=false;
	if (lstNMRXNewData.getSelectedCount()>0)
		bDuplicateFound=true;

	if (lstNMRXNewData.getSelectedCount()>0 && bShowMsgWarn)
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Assignment already exists!"));
	}
	lstNMRXNewData.deleteSelectedRows();

	return bDuplicateFound;
}
/*
bool NMRXRelationWidget::TestListForDuplicateAssignmentsPriv(int nAssigmentTableID,DbRecordSet &lstNMRXNewData, bool bShowMsgWarn)
{
	if (!m_bForbidDuplicateAssignments)
		return false;

	MainEntitySelectionController *pCache=NULL;
	if (m_EntityAssigment2.GetSelectionEntity()==ENTITY_BUS_PERSON)
		pCache=&m_EntityAssigment2;
	else if (m_EntityAssigment1.GetSelectionEntity()==ENTITY_BUS_PERSON) 
		pCache=&m_EntityAssigment2;
	else
		pCache=NULL;

	return TestListForDuplicateAssignments(nAssigmentTableID,lstNMRXNewData,pCache,bShowMsgWarn);
}*/



static void ConvertToAssignmentData(DbRecordSet &lstResult, DbRecordSet &lstNMRXData)
{

}