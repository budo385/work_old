#ifndef FUI_DM_DOCUMENTS_H
#define FUI_DM_DOCUMENTS_H


#include "generatedfiles/ui_fui_dm_documents.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"




class FUI_DM_Documents : public FuiBase
{
	Q_OBJECT

public:
	FUI_DM_Documents(QWidget *parent = 0);
	~FUI_DM_Documents();

	bool CanClose();
	void Execute (int nDocumentID);		//execute scheduled document
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void SetDefaults(int nDocType,DbRecordSet *rowDefault=NULL,DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL, bool bScheduleTask=false, bool bIsTemplate=false, DbRecordSet *recRevisionForInsert=NULL);
	void SetSimpleMode();
	void on_cmdCopy();

	DbRecordSet *GetTaskRecordSet();

	int GetTaskID();
	void RefreshAvatarData(bool bReload = false);

	QString GetFUIName();
	void SetMode(int nMode);

protected:
	void DataWrite(Status &err);
	void DataPrepareForInsert(Status &err);
	void DataPrepareForCopy(Status &err);
	void DataClear();
	void DataDefine();
	void DataRead(Status &err,int nRecordID);
	void DataLock(Status &err);
	void DataDelete(Status &err);
	void DataUnlock(Status &err);
	void DataCheckBeforeWrite(Status &err);

	virtual void changeEvent (QEvent * event);
	virtual void closeEvent(QCloseEvent *event);
	bool m_bHideOnMinimize;

private slots:
	void OnSelectPath();
	void OnRemovePath();
	void OnSaveFromTemplate();
	void OpenDocumentInApplication();
	void OnPrint();

	//revisions:
	void OnSetRevisionTag();
	void OnGetRevision();
	void OnOpenRevision();
	void OnDeleteRevision();
	void OnOnReplaceCurrentRevision();

	//clicks:
	void on_btnRefresh_clicked();
	void on_btnCheckIn_clicked();
	void on_btnCheckOut_clicked();
	void on_btnOpenDocument_clicked();

	void OnAssignedContactChanged();
	void OnAssignedProjectChanged();
	void OnNoteEditor_VerticalOpenClose_clicked(bool bSmaller);
	void OnEditor_VerticalOpenClose_clicked(bool bSmaller);

	//void on_groupBoxRevision_clicked(bool checked);
	//void on_groupBoxDesc_clicked(bool checked);
	void OnCheckInOutCompleted(Status err,int nDocumentID,QString strFilePath,DbRecordSet recRev,DbRecordSet recCheckOutInfo);
	void updateTaskTabCheckbox();

private:
	void RefreshDisplay();
	void RefreshRevisions(bool bReloadFromServer=false);
	void GetLinkListsFromSAPNE();
	void OnThemeChanged();
	void RefreshButtonState();
	void	keyPressEvent(QKeyEvent* event);

	//doc type: only available when insert mode:
	void PrepareLocalDocument(bool checked);
	void PrepareURLDocument(bool checked);
	void PreparePhysicalDocument(bool checked);
	void PrepareNoteDocument(bool checked);
	void PrepareInternetDocument(bool checked);
	void CreateNoteRevisionIfNeeded(DbRecordSet &rowRevision);
	

	DbRecordSet m_lstContacts;
	DbRecordSet m_lstProjects;
	DbRecordSet m_lstTaskCache;
	DbRecordSet m_lstRevisions;
	DbRecordSet m_recCheckOutInfo;
	DbRecordSet m_recInsertedDocumentRevision;
	bool m_bDocCheckedOut;


	QString m_strHtmlTag;
	QString m_strEndHtmlTag;


	GuiFieldManager *m_GuiFieldManagerMain;

	Ui::FUI_DM_DocumentsClass ui;

	bool m_bDisableRefresh;
	bool m_bNoteDocument;
	QString m_strOriginalNote;

	QAction *m_pRevSetTag;
	QAction *m_pRevSaveCopy;
	QAction *m_pRevOpenCopy;
	QAction *m_pRevReplaceCurrent;
};
#endif // FUI_DM_DOCUMENTS_H
