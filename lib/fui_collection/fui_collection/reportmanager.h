#ifndef REPORTMANAGER_H
#define REPORTMANAGER_H

#include "report_definitions/report_definitions/reportregistration.h"
#include "common/common/dbrecordset.h"
#include <QList>
#include <QPrinter>
#include <QObject>
#include "report_core/report_core/rptprinter.h"


class ReportManager : public QObject
{
	Q_OBJECT
public:

	ReportManager();
	~ReportManager();
	void PrintReportAll();
	void PrintReport(int nReportID, QList<DbRecordSet> *lstPageRecordSet=NULL, int nDestination=ReportRegistration::REPORT_DESTINATION_ASK);

private slots:
	void OnPrintScreen(QPrinter *pPrinter);

private:
	bool IsReportAllowed(int nReportID);
	bool OpenWizard(int nReportID,QList<DbRecordSet> &lstPageRecordSet, int &nDestination);
	void PrintReport_Printer(int nReportID, QList<DbRecordSet> &lstPageRecordSet);
	void PrintReport_Screen(int nReportID, QList<DbRecordSet> &lstPageRecordSet);
	void PrintReport_PDF(int nReportID, QList<DbRecordSet> &lstPageRecordSet);
	RptPrinter *m_RptScreen;
};
#endif // REPORTMANAGER_H
