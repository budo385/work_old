#ifndef TABLE_CONTACTPHONE_H
#define TABLE_CONTACTPHONE_H

#include "gui_core/gui_core/universaltablewidgetex.h"
#include "bus_core/bus_core/formatphone.h"
#include "toolbar_edit.h"
#include "bus_client/bus_client/selection_combobox.h"


class Table_ContactPhone : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
    Table_ContactPhone(QWidget *parent);
	void	Initialize(DbRecordSet *plstData,toolbar_edit *pToolBar, QWidget *pContactFUI);
	void	SetEditMode(bool bEdit=true);
	void	CheckDataBeforeWrite(Status &Ret_pStatus);
	QString GetDefaultEntry();
	void	ClearEmptyEntries();

private slots:
	void	OnSetDefault();
	void	OnFormatPhoneNumber();
	void	OnCellEdited(int nRow, int nCol);
	void	OnComboDataReload();
	void	OnComboIndexChanged(int nIndex);
	void	OnRowInserted(int nRow);
	void	OnDeleted();
	void	OnMakeAction();
	void	OnOpenTypeFUI();

protected:
	void	Data2CustomWidget(int nRow, int nCol);
private:
	void	BuildToolBar(toolbar_edit* pToolBar);
	void	CreateContextMenu();
	void	GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip);	
	void	RefreshDefaultFlag();

	QAction* m_pActMakeAct;
	QAction* m_pActOpenType;

	int m_nDefaultColIdx;
	int m_nPhoneColIdx;
	int m_nAreaColIdx;
	int m_nCuntryColIdx;
	int m_nLocalColIdx;
	int m_nTypeColIdx;

	FormatPhone m_FormatPhone;
	toolbar_edit *m_toolbar;
	Selection_ComboBoxEx m_TypeHandler;
	QWidget *m_pContactFUI;
    
};


#endif // TABLE_CONTACTPHONE_H

