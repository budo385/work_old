#ifndef FUI_DefineAvailabilityDlg_H
#define FUI_DefineAvailabilityDlg_H

#include <QtWidgets/QDialog>
#include "ui_DefineAvailabilityDlg.h"
#include "common/common/dbrecordset.h"

class DefineAvailabilityDlg : public QDialog
{
    Q_OBJECT

public:
    DefineAvailabilityDlg(QWidget *parent = 0);
    ~DefineAvailabilityDlg();

	void SetDateFrom(const QDate &date)	{	ui.dateFrom->setDate(date); };
	void SetDateTo(const QDate &date)	{	ui.dateTo->setDate(date); };

	QDate GetDateFrom()	{	return ui.dateFrom->date(); };
	QDate GetDateTo()	{	return ui.dateTo->date(); };
	bool  IsMoChecked()	{	return (ui.chkMo->checkState() == Qt::Checked); };
	bool  IsTuChecked()	{	return (ui.chkTu->checkState() == Qt::Checked); };
	bool  IsWeChecked()	{	return (ui.chkWe->checkState() == Qt::Checked); };
	bool  IsThChecked()	{	return (ui.chkTh->checkState() == Qt::Checked); };
	bool  IsFrChecked()	{	return (ui.chkFr->checkState() == Qt::Checked); };
	bool  IsSaChecked()	{	return (ui.chkSa->checkState() == Qt::Checked); };
	bool  IsSuChecked()	{	return (ui.chkSu->checkState() == Qt::Checked); };
	bool  IsWeekDayChecked(int nDay);

private:
	Ui::DefineAvailabilityDlgClass ui;

private slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();
};

#endif // DefineAvailabilityDlg_H
