#ifndef SETTINGS_Thunderbird_H
#define SETTINGS_Thunderbird_H

#include <QWidget>
#include "ui_settings_thunderbird.h"
#include "settingsbase.h"

class Settings_Thunderbird : public SettingsBase
{
    Q_OBJECT

public:
    Settings_Thunderbird(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
    ~Settings_Thunderbird();

	DbRecordSet GetChangedValuesRecordSet();
	void GetFolderSetting(QList<QStringList> &lstFolders);
	void RefreshInfoLabel();

private:
	bool m_bScanDateChanged;
    Ui::Settings_ThunderbirdClass ui;
	QList<QStringList> m_lstThunderbirdFoldersScan;
	int m_nIniLstRow;

private slots:
	void on_btnSelectFolders_clicked();
	void on_scannedDateTime_changed(const QDateTime &dateTime);
	void on_chkImport_clicked(bool bChecked);
};

#endif // SETTINGS_Thunderbird_H
