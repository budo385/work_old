#ifndef SINGLECONTACTSELECTIONWIZPAGE_H
#define SINGLECONTACTSELECTIONWIZPAGE_H

#include "bus_client/bus_client/simpleselectionwizpage.h"

class SingleContactSelectionWizPage : public SimpleSelectionWizPage
{
    Q_OBJECT

public:
    SingleContactSelectionWizPage(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
    ~SingleContactSelectionWizPage();

	void Initialize();

private slots:
	void CompleteChanged();
};

#endif // SINGLECONTACTSELECTIONWIZPAGE_H
