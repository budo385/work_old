#include "fui_contacts.h"
#include "dlg_gridviews.h"
#include <QDesktopServices>
#include "fui_voicecallcenter.h"
#include "fui_calendar.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "gui_core/gui_core/dlg_enterperiod.h"
#include <QFileDialog>
#include <QApplication>
#include <QDesktopWidget>
#include "trans/trans/ftpclient.h"
#include <QDebug>
#include <QClipboard>
#include "gui_core/gui_core/picturehelper.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"
#include "dlg_contactvcardimport.h"

#include "communicationactions.h"
#include "common/common/entity_id_collection.h"
#include "common/common/datahelper.h"
#include "bus_client/bus_client/changemanager.h"
#include "bus_core/bus_core/emailhelpercore.h"

//GLOBALS:
extern QString g_strLastDir_FileOpen;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "gui_core/gui_core/thememanager.h"
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;	

bool Call_OnLoadFullPicture(int nPicID, DbRecordSet &rowPic)
{
	Status err;
	_SERVER_CALL(BusContact->ReadBigPicture(err,nPicID,rowPic))
	_CHK_ERR_RET_BOOL(err)
}

FUI_Contacts::FUI_Contacts(QWidget *parent, bool bOpenedAsAvatar /*= false*/)
    : FuiBase(parent),m_GuiFieldManagerMain(NULL),m_GuiFieldManagerDebtor(NULL),m_pCalendarButton(NULL),m_bGridDialogRequestedEditMode(false)
{

	m_bHideOnMinimize = true;
	m_bOpenedAsAvatar = bOpenedAsAvatar;

	//special: init caches then init entity: saves n-times server calls
	//load all SAPNE patterns if not already loaded:
	MainEntitySelectionController cacheLoader;
	QList<int> lstIDs;
	lstIDs<<ENTITY_BUS_PERSON<<ENTITY_BUS_CM_ADDRESS_SCHEMAS<<ENTITY_CONTACT_ORGANIZATION<<ENTITY_CONTACT_DEPARTMENT<<ENTITY_CONTACT_FUNCTION<<ENTITY_CONTACT_PROFESSION<<ENTITY_BUS_CM_TYPES<<ENTITY_BUS_OPT_GRID_VIEWS;
	if(!cacheLoader.BatchReadEntityData(lstIDs))
		return;

	ui.setupUi(this);

	//issue 2082:
	//ui.label_22->setVisible(false);
	//ui.frameInvoiceText->setVisible(false);
	//ui.tabContactGroups->update();

	ui.btnSerialEmail->setIcon(QIcon(":Serial_Email_32.png"));
	ui.btnExportSerialLetter->setIcon(QIcon(":Serial_Letter_32.png"));
	ui.btnRefreshActual->setIcon(QIcon(":reload32.png"));

	connect (ui.frameContactAndFavs,SIGNAL(FavoriteChanged(int)),this,SLOT(OnFavoriteChanged(int)));

	//SIDEBAR engaged:
	m_p_CeMenu=ui.frameCommToolBar;
	m_pFrameContacts=ui.frameContactAndFavs->GetSelectionContacts();
	m_p_Selector=m_pFrameContacts;
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
	{
		m_p_CeMenu=g_objFuiManager.GetSideBarCommMenuSelector(MENU_CONTACTS);
		if (m_p_CeMenu)
			m_p_Selector=dynamic_cast<Selection_Contacts*>(m_p_CeMenu->GetSelector());

		if (!m_p_CeMenu)
			m_p_CeMenu=ui.frameCommToolBar;
		else
			ui.frameCommToolBar->setVisible(false);
		if (!m_p_Selector)
		{
			m_p_Selector=m_pFrameContacts;
			m_pFrameContacts->Initialize();
			ui.frameContactAndFavs->ReloadData();
		}
		else
		{
			connect(m_p_Selector,SIGNAL(destroyed()),this,SLOT(On_SelectionControllerSideBarDestroyed()));
			//m_pFrameContacts->setVisible(false);
			ui.frameContactAndFavs->setVisible(false);
		}
		m_bSideBarModeView=true;
		//MB: issue 2669: resize to 820:
		//resize(820,700);
		//resize(670,480);
	}
	else
	{
		m_pFrameContacts->Initialize();
		ui.frameContactAndFavs->ReloadData();
		resize(1200,700);
	}

	if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_PRO)
	{
		//hide actaul TAB:
		ui.tabWidgetMain->removeTab(2);
		ui.tabWidgetMain->removeTab(1);
		
		//hide Debtors:
		ui.tabContactGroups->removeTab(8);
		ui.tabContactGroups->removeTab(7);
		//ui.tabContactGroups->removeTab(6); //issue 1169

		//ui.tabContactGroups->removeTab(2);
		
		//quick is unchecked:
		ui.groupBoxInfo->blockSignals(true);
		ui.groupBoxInfo->setChecked(false);
		ui.groupBoxInfo->blockSignals(false);
		on_groupBoxInfo_clicked();
		//resize(670,480);
		//MB: issue 2669: resize to 820:
		resize(820,480);
	}


	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	InitFui(ENTITY_BUS_CONTACT,BUS_CM_CONTACT,dynamic_cast<MainEntitySelectionController*>(m_p_Selector),ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitterMain);
	ui.widgetReservations->Initialize(BUS_CM_CONTACT);
	
	m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; font-style:italic;\">";
	m_strEndHtmlTag="</span></p></body></html>";
	m_CalcName.Initialize(ENTITY_BUS_CONTACT,m_lstData);


	//------------------------------------
	//MAIN
	//------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_BUS_CM_CONTACT);
	
	//Type:
	QList<QRadioButton*> lstRadioButons;
	lstRadioButons << ui.btnPerson << ui.btnOrganization;
	QList<QVariant> lstValues;
	lstValues << ContactTypeManager::CM_TYPE_PERSON << ContactTypeManager::CM_TYPE_ORGANIZATION;
	m_GuiFieldManagerMain->RegisterField("BCNT_TYPE",new GuiField_RadioButton("BCNT_TYPE",lstRadioButons,lstValues,&m_lstData,&DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT)));

	//person:
	m_GuiFieldManagerMain->RegisterField("BCNT_FIRSTNAME",ui.txtFirstName);
	m_GuiFieldManagerMain->RegisterField("BCNT_LASTNAME",ui.txtLastName);
	m_GuiFieldManagerMain->RegisterField("BCNT_MIDDLENAME",ui.txtMiddleName);
	m_GuiFieldManagerMain->RegisterField("BCNT_NICKNAME",ui.txtNickName);
	m_GuiFieldManagerMain->RegisterField("BCNT_LOCATION",ui.txtLocation);
	//init custom picture control
	ui.picture->Initialize(PictureWidget::MODE_PREVIEW_WITH_BUTTONS,&m_lstData,"BCNT_PICTURE","BPIC_PICTURE","BCNT_BIG_PICTURE_ID","BCNT_ID");
	ui.picture->SetLoadFullPictureCallBack(Call_OnLoadFullPicture);
	ui.picture->setFocusPolicy(Qt::ClickFocus);
	ui.cmbInfo_Phone->setEditable(false);
	ui.cmbInfo_Email->setEditable(false);
	ui.txtInfo_Address->setReadOnly(true);
	//sex
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.btnMale << ui.btnFemale;
	lstValues << 0 << 1;
	m_GuiFieldManagerMain->RegisterField("BCNT_SEX",new GuiField_RadioButton("BCNT_SEX",lstRadioButons,lstValues,&m_lstData,&DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT)));
	m_GuiFieldManagerMain->RegisterField("BCNT_ORGANIZATIONNAME",ui.txtOrgName);
	m_GuiFieldManagerMain->RegisterField("BCNT_ORGANIZATIONNAME_2",ui.txtOrgShortName);
	m_GuiFieldManagerMain->RegisterField("BCNT_FOUNDATIONDATE",ui.dateFoundation);
	m_GuiFieldManagerMain->RegisterField("BCNT_DESCRIPTION",ui.frameEditor);
	m_GuiFieldManagerMain->RegisterField("BCNT_SUPPRESS_MAILING",ui.ckbSupressMail);
	m_GuiFieldManagerMain->RegisterField("BCNT_BIRTHDAY",ui.dateBirthDay);
	m_GuiFieldManagerMain->RegisterField("BCNT_DAT_CREATED",ui.dateCreated);
	m_GuiFieldManagerMain->RegisterField("BCNT_OLD_CODE",ui.txtOldCode);
	m_GuiFieldManagerMain->RegisterField("BCNT_SHORTNAME",ui.txtOldName);
	m_GuiFieldManagerMain->RegisterField("BCNT_DO_NOT_SYNC_MAIL",ui.ckbMail_DoNotSync);
	m_GuiFieldManagerMain->RegisterField("BCNT_OUT_MAIL_AS_PRIV",ui.ckbMail_OutgoingAsPrivate);
	m_GuiFieldManagerMain->RegisterField("BCNT_SYNC_MAIL_AS_PRIV",ui.ckbMail_SyncAsPrivate);
	//SAPNE's
	ui.framePersonOrganization->Initialize(ENTITY_CONTACT_ORGANIZATION,&m_lstData ,"BCNT_ORGANIZATIONNAME",false);
	ui.frameDepartment->Initialize(ENTITY_CONTACT_DEPARTMENT, &m_lstData, "BCNT_DEPARTMENTNAME",false);
	ui.frameFunction->Initialize(ENTITY_CONTACT_FUNCTION, &m_lstData, "BCNT_FUNCTION",false);
	ui.frameProfession->Initialize(ENTITY_CONTACT_PROFESSION, &m_lstData, "BCNT_PROFESSION",false);
	ui.frameContactOwner->Initialize(ENTITY_BUS_PERSON, &m_lstData,"BCNT_OWNER_ID");
	ui.framePaymentConditions->Initialize(ENTITY_PAYMENT_CONDITIONS, &m_lstDebtor,"BCMD_PAYMENT_ID");

	//--------------------------------------------------
	//		DEBTOR:
	//--------------------------------------------------
	m_GuiFieldManagerDebtor=new GuiFieldManager(&m_lstDebtor,DbSqlTableView::TVIEW_BUS_CM_DEBTOR);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_CUSTOMERCODE",ui.txtCustomerCode);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_DEBTORCODE",ui.txtDebtorCode);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_DEBTORACCOUNT",ui.txtDebtorAccount);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_DESCRIPTION",ui.frameDebtorDesc);
	m_GuiFieldManagerDebtor->RegisterField("BCMD_INTERNAL_DEBTOR",ui.ckbInternalDebtor);

	//m_GuiFieldManagerDebtor->RegisterField("BCMD_INVOICE_TEXT",ui.frameInvoiceText->GetTextWidget());
	if (!g_FunctionPoint.IsFPAvailable(FP_DEBTOR_DATA))
	{
		if(ui.tabContactGroups->count()>=8)
			ui.tabContactGroups->removeTab(8);
	}
	ui.frameDateSelector->setVisible(false);
	//--------------------------------------------------
	//		INIT GRIDS
	//--------------------------------------------------
	ui.tablePhone->Initialize(&m_lstPhones,ui.framePhoneToolBar,this);
	ui.tableEmail->Initialize(&m_lstEmails,ui.frameEmailToolBar,this);
	ui.tableInternet->Initialize(&m_lstInternet,ui.frameInternetToolBar,this);
	ui.tableCustomFields->Initialize(&m_lstCustomFields);
	ui.tablePicture->Initialize(&m_lstPictures,ui.framePicToolbar);
	ui.tableAddress->Initialize(&m_lstAddress,&m_lstData,ui.txtAddress,ui.tableTypes,ui.frameAddressToolbar,ui.ckbSynchronize,ui.ckbSynchronizeOrg);
	ui.ckbSynchronize->setChecked(false); //default is ON
	ui.ckbSynchronizeOrg->setChecked(false); //default is ON
	ui.tabContactGroups->setCurrentIndex(5); //set detail as defult
	ui.tabContactGroups->setTabIcon(TAB_DETAIL_PHONE,QIcon(":Phone16.png"));
	ui.tabContactGroups->setTabIcon(TAB_DETAIL_EMAIL,QIcon(":Email16.png"));
	ui.tabContactGroups->setTabIcon(TAB_DETAIL_INTERNET,QIcon(":Earth16.png"));
	GUI_Helper::AdjustFixedWidget(ui.tableActual,ui.tableActualSub,false);
	GUI_Helper::AdjustFixedWidget(ui.tableGroup,ui.tableGroupSub,false);
	ui.tableActual->Initialize(this,ui.cmbActualView,ui.btnActualView,ui.tableActualSub,ui.splitter_Actual,ui.btnRefreshActual);
	ui.tableGroup->Initialize(this,m_pFrameContacts->GetGroupSelector(),ui.cmbGroupView,ui.btnGroupView,ui.tableGroupSub,ui.splitter_Group,ui.btnRefreshGroup,ui.labelGroupName,ui.ckbShowActive);
	connect(ui.tableGroup,SIGNAL(ShowContactDetails(int)),this,SLOT(OnShowContactDetails(int)));
	connect(ui.tableActual,SIGNAL(ShowContactDetails(int)),this,SLOT(OnShowContactDetails(int)));
	connect(ui.tableAddress,SIGNAL(SignalDblClick()),this,SLOT(OnGridRequestEditModeAfterDblClick()));//MB: request: if dbl click on address and read then go edit mode
	connect(ui.tableAddress,SIGNAL(SignalDblClickOver()),this,SLOT(OnGridRequestCloseEditModeAfterDblClick()));//MB: request: if dbl click on address and read then go edit mode
	
	//--------------------------------------------------
	//		Build Up Communication TAB
	//--------------------------------------------------
	QList<int> lst;
	lst<<NMRXManager::PAIR_CONTACT_PERSON<<NMRXManager::PAIR_PERSON_CONTACT<<NMRXManager::PAIR_CONTACT_CONTACT;
	ui.widgetCommPerson->Initialize(tr("Assigned Users And Contacts"),BUS_CM_CONTACT,lst,BUS_CM_CONTACT,BUS_PERSON);
	lst.clear();
	lst<<NMRXManager::PAIR_CONTACT_PROJECT<<NMRXManager::PAIR_PROJECT_CONTACT;
	ui.widgetCommProject->Initialize(tr("Assigned Projects"),BUS_CM_CONTACT,lst,BUS_PROJECT);
	ui.tableJournal->Initialize(ui.textJournalEntry,ui.btnEditJournalEntry,ui.frameJournalToolBar); //apart from CONTACT
	GUI_Helper::AdjustFixedWidget(ui.tableJournal,ui.textJournalEntry,false);
	//connect CE menu:
	connect(m_p_CeMenu,SIGNAL(NeedNewData(int)),this,SLOT(OnCEMenu_NeedNewData(int)));
	connect(m_p_CeMenu->m_menuDropZone,SIGNAL(NewDataDropped(int,DbRecordSet)),this,SLOT(OnCEMenu_NewDataDropped(int,DbRecordSet)));
	connect(ui.contactGrid,SIGNAL(NeedNewData(int)),this,SLOT(OnCEMenu_NeedNewData(int)));

	//----------------------------------------------
	//  GROUP TAB
	//----------------------------------------------
	DbRecordSet lstSetup;
	UniversalTableWidget::AddColumnToSetup(lstSetup,"BGTR_NAME",tr("Tree"),240,false);
	UniversalTableWidget::AddColumnToSetup(lstSetup,"_CALC_NAME_",tr("Group"),240,false,"",UniversalTableWidget::COL_TYPE_TEXT,"",1);
	UniversalTableWidget::AddColumnToSetup(lstSetup,"BGCN_CMCA_VALID_FROM",tr("From"),80,false);
	UniversalTableWidget::AddColumnToSetup(lstSetup,"BGCN_CMCA_VALID_TO",tr("To"),80,false);
	ui.frameGroupContacts->Initialize(ENTITY_BUS_GROUP_ITEMS,"BGCN_CONTACT_ID",DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT,lstSetup,tr("Contact Groups"));
	ui.frameGroupContacts->GetButton(Selection_NM_SAPNE_FUI::BUTTON_ADD)->setVisible(false); //add
	ui.frameGroupContacts->GetButton(Selection_NM_SAPNE_FUI::BUTTON_MODIFY)->setVisible(false); //modify 
	ui.frameGroupContacts->GetButton(Selection_NM_SAPNE_FUI::BUTTON_OPEN_NEW)->setVisible(false); //new win
	connect(ui.frameGroupContacts,SIGNAL(OnAdd(DbRecordSet &)),this,SLOT(OnGroupSAPNE_Add(DbRecordSet &)));
	connect(ui.frameGroupContacts,SIGNAL(OnRemove(DbRecordSet &)),this,SLOT(OnGroupSAPNE_Remove(DbRecordSet &)));
	connect(ui.btnChangeGroupDate,SIGNAL(clicked()),this,SLOT(OnGroupSAPNE_Edit()));
	ui.frameCommToolBar->SetFUIParent(MENU_CONTACTS);

	
	SetMode(MODE_EMPTY);
	ui.tabWidgetMain->setCurrentIndex(0);
	ui.tabContactGroups->setCurrentIndex(0);
	ui.tabWidgetContact->setCurrentIndex(0);
	ui.tabWidgetComm->setCurrentIndex(0);

	ui.frameEditor->SetEmbeddedPixMode();

	//set TAB styles:
	QString strStyle="* {color: black;font-weight:bold; font-size:12px}";
	ui.tabWidgetMain->setTABStyle(strStyle);
	ui.tabWidgetMain->setTabIcon(0,QIcon(":Contacts32.png"));
	ui.tabWidgetContact->setTABStyle(strStyle);

	//Initalize comm. grid. if not opened from avatar.
	ui.contactGrid->Initialize(CommGridViewHelper::CONTACT_GRID, this);
	connect(ui.contactGrid,SIGNAL(ContentChanged()),this,SLOT(OnCEGridChanged()));
	connect(ui.widgetCommProject,SIGNAL(ContentChanged()),this,SLOT(OnNMRXProjectsChanged()));
	connect(ui.widgetCommPerson,SIGNAL(ContentChanged()),this,SLOT(OnNMRXContactsChanged()));
	connect(ui.tableJournal,SIGNAL(ContentChanged()),this,SLOT(OnJournalGridChanged()));

	//hide banner if screen size not big enough (MB request)
	int nHeight = QApplication::desktop()->rect().height();
	if(nHeight < 920)
	{
		if (ui.groupBoxInfo->isChecked())
		{
			ui.groupBoxInfo->blockSignals(true);
			ui.groupBoxInfo->setChecked(false);
			ui.groupBoxInfo->blockSignals(false);
			on_groupBoxInfo_clicked();
		}
	}
	//quick info frame:
	ui.btnInternet->setIcon(QIcon(":Earth16.png"));
	ui.btnAddress->setIcon(QIcon(":Adress16.png"));
	ui.btnEmail->setIcon(QIcon(":Email16.png"));
	ui.btnPhone->setIcon(QIcon(":Phone16.png"));
	ui.btnInternet->setToolTip(tr("Copy Internet Address"));
	ui.btnAddress->setToolTip(tr("Copy Address"));
	ui.btnEmail->setToolTip(tr("Copy Email"));
	ui.btnPhone->setToolTip(tr("Copy Phone"));
	m_pWndAvatar = NULL;

	connect(ui.btnPhone,SIGNAL(clicked()),this,SLOT(on_btnPhone_clicked()));
	connect(ui.btnAddress,SIGNAL(clicked()),this,SLOT(on_btnAddress_clicked()));
	connect(ui.btnEmail,SIGNAL(clicked()),this,SLOT(on_btnEmail_clicked()));
	connect(ui.btnInternet,SIGNAL(clicked()),this,SLOT(on_btnInternet_clicked()));

	//------------------------------
	//add QCW button:
	//------------------------------
	m_buttonQCW= new QCheckBox();
	QSize size(50,30);
	m_buttonQCW->setMaximumSize(size);
	m_buttonQCW->setMinimumSize(size);
	m_buttonQCW->setIconSize(QSize(24,24));
	m_buttonQCW->setIcon(QIcon(":EntryAssistent24.png"));
	ui.btnButtonBar->GetButtonLayout()->insertWidget(0,m_buttonQCW);
	m_buttonQCW->setToolTip(tr("Use Entry Assistant"));


	m_pCalendarButton =	new StyledPushButton(this,":CalendarEvent24.png",":CalendarEvent24.png","","",0,0);
	m_pCalendarButton->setMinimumSize(QSize(24,24));
	m_pCalendarButton->setMaximumSize(QSize(24,24));
	m_pCalendarButton->setToolTip(tr("Contact Calendar Entries"));
	ui.layoutContactName->insertWidget(1,m_pCalendarButton);
	connect(m_pCalendarButton,SIGNAL(clicked()),this,SLOT(OnBtnCalendar_Click()));
	m_pCalendarButton->setEnabled(false);

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
		m_pCalendarButton->setVisible(false);

	if (!m_bSideBarModeView)
	{
		QTimer::singleShot(0,this,SLOT(OnSetFindFocusDelayed()));
	}

	if (!g_FunctionPoint.IsFPAvailable(FP_RESERVATIONS__CONTACT) && ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
	{
		ui.tabContactGroups->widget(TAB_DETAIL_RESERVATIONS)->setVisible(false);
		ui.tabContactGroups->removeTab(TAB_DETAIL_RESERVATIONS);
	}

	connect(ui.tabWidgetContact,SIGNAL(currentChanged(int)),this,SLOT(OnTabWidgetContact_currentChanged(int)));
	OnTabWidgetContact_currentChanged(ui.tabWidgetContact->currentIndex());

	//ui.tabWidgetContact->setVisible(false);
	//ui.tabContactGroups->setVisible(false);
	//
	//ui.tabContactGroups->removeTab(TAB_DETAIL_DEBTOR);

//TAB_DEBTOR: main problem
	
}

FUI_Contacts::~FUI_Contacts()
{
	//save:
	if (g_pSettings->GetPersonSetting(CONTACT_DEF_SETTINGS_SAVE_ON_EXIT).toBool())
	{
		QString strCurrentView=ui.cmbGroupView->currentText();
		g_pSettings->SetPersonSetting(CONTACT_DEF_GROUP_LIST_VIEW,strCurrentView);
	}

	//save:
	if (g_pSettings->GetPersonSetting(CONTACT_DEF_SETTINGS_SAVE_ON_EXIT).toBool())
	{
		QString strCurrentView=ui.cmbActualView->currentText();
		g_pSettings->SetPersonSetting(CONTACT_DEF_ACTUAL_LIST_VIEW,strCurrentView);
	}

	if(m_GuiFieldManagerMain)delete m_GuiFieldManagerMain;
	if(m_GuiFieldManagerDebtor)delete m_GuiFieldManagerDebtor;

}



//when programatically change data, call this:
void FUI_Contacts::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();
	m_GuiFieldManagerDebtor->RefreshDisplay();

	ui.framePersonOrganization->RefreshDisplay();
	ui.frameDepartment->RefreshDisplay();
	ui.frameFunction->RefreshDisplay();
	ui.frameProfession->RefreshDisplay();
	ui.frameContactOwner->RefreshDisplay();
	ui.framePaymentConditions->RefreshDisplay();

	ui.tablePhone->RefreshDisplay();
	ui.tableEmail->RefreshDisplay();
	ui.tableInternet->RefreshDisplay();
	ui.tableCustomFields->RefreshDisplay();
	ui.tablePicture->RefreshDisplay();

	ui.tableAddress->GetDataSource()->find("BCMA_IS_DEFAULT",1); //select default
	ui.tableAddress->RefreshDisplay();

	RefreshQuickInfo();
	ui.picture->RefreshDisplay();
	
}



//when changing state, call this:
void FUI_Contacts::SetMode(int nMode)
{
	//FuiBase::SetMode(nMode);//set mode
	//return;

	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameDateSelector->setEnabled(true);
		//ui.stackedWidget->setEnabled(false);
		ui.frameContactAndFavs->setEnabled(true);


		m_GuiFieldManagerMain->SetEditMode(false);
		ui.frameEditor->SetEditMode(false);

		m_GuiFieldManagerDebtor->SetEditMode(false);
		//ui.frameInvoiceText->GetTextWidget()->setEnabled(true);
		ui.frameDebtorDesc->SetEditMode(false);


		//details:
		ui.framePersonOrganization->SetEditMode(false);
		ui.frameDepartment->SetEditMode(false);
		ui.frameFunction->SetEditMode(false);
		ui.frameProfession->SetEditMode(false);
		ui.arSelector->SetEditMode(false);
		ui.txtOldCode->setEnabled(false);
		ui.txtOldName->setEnabled(false);
		ui.ckbSupressMail->setEnabled(false);
		ui.frameContactOwner->SetEditMode(false);
		ui.framePaymentConditions->SetEditMode(false);
	
		ui.picture->SetEditMode(false);

		//RTF:
		ui.frameEditor->SetEditMode(false);
		//ui.frameInvoiceText->SetEditMode(false);
		ui.frameDebtorDesc->SetEditMode(false);

		ui.tablePhone->SetEditMode(false);
		ui.tableEmail->SetEditMode(false);
		ui.tableInternet->SetEditMode(false);
		ui.tableCustomFields->SetEditMode(false);
		ui.tablePicture->SetEditMode(false);
		ui.tableAddress->SetEditMode(false);

		ui.ckbSynchronize->setEnabled(false);
		ui.ckbSynchronizeOrg->setEnabled(false);
		ui.tabWidgetComm->setEnabled(true);

		ui.btnDebtorCode->setEnabled(false);
		ui.widgetReservations->SetEditMode(false);
	}
	else
	{
		ui.frameDateSelector->setEnabled(false);
		//ui.stackedWidget->setEnabled(true);
		ui.frameContactAndFavs->setEnabled(false);

		m_GuiFieldManagerMain->SetEditMode(true);
		m_GuiFieldManagerDebtor->SetEditMode(true);

		ui.picture->SetEditMode(true);

		//details:
		ui.framePersonOrganization->SetEditMode(true);
		ui.frameDepartment->SetEditMode(true);
		ui.frameFunction->SetEditMode(true);
		ui.frameProfession->SetEditMode(true);

		ui.arSelector->SetEditMode(true);
		ui.txtOldCode->setEnabled(true);
		ui.txtOldName->setEnabled(true);
		ui.ckbSupressMail->setEnabled(true);
		ui.frameContactOwner->SetEditMode(true);
		ui.framePaymentConditions->SetEditMode(true);


		ui.tablePhone->SetEditMode(true);
		ui.tableEmail->SetEditMode(true);
		ui.tableInternet->SetEditMode(true);
		ui.tableCustomFields->SetEditMode(true);
		ui.tablePicture->SetEditMode(true); //TOFIX!!!
		ui.tableAddress->SetEditMode(true);

		ui.tabWidgetComm->setEnabled(false);
		
		//RTF:
		ui.frameEditor->SetEditMode(true);
		//ui.frameInvoiceText->SetEditMode(true);
		ui.frameDebtorDesc->SetEditMode(true);


		ui.ckbSynchronize->setEnabled(true);
		ui.ckbSynchronizeOrg->setEnabled(true);

		ui.btnDebtorCode->setEnabled(true);
		ui.widgetReservations->SetEditMode(true);

		//Set focus to Pers. Name:
		ui.txtFirstName->setFocus(Qt::OtherFocusReason);
	}
	
	ui.dateCreated->setEnabled(false); //always false

	//insert:
	if(nMode==MODE_INSERT)
	{
		ui.frameMainHeader->setEnabled(true);
	}
	else
	{
		ui.btnChangeGroupDate->setEnabled(true);
		ui.frameMainHeader->setEnabled(false);

		Status err;
		if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
			ui.frameGroupContacts->SetEditMode(false);
		else
			ui.frameGroupContacts->SetEditMode(true);
	}

	//set person as def:
	if(nMode==MODE_EMPTY)
	{
		ui.btnPerson->setChecked(true);
	}
	
	if(nMode==MODE_INSERT || nMode==MODE_EMPTY)
	{
		ui.frameGroupContacts->SetEditMode(false);
		ui.btnChangeGroupDate->setEnabled(false);
		ui.widgetCommPerson->Invalidate();
		ui.widgetCommProject->Invalidate();
		ui.tableJournal->Invalidate();
		ui.contactGrid->ClearCommGrid();  //if insert, clear grid
	}

	if (m_pCalendarButton)
	{

		if (nMode==MODE_EDIT || nMode==MODE_READ)
		{
			//MB on Skype: 
			/*
			a: Other User=0, (Contact=0) --> User & contact selection disabled
			b: Other User=1, Contact=0 --> User selection enabled, contact selection enabled for user contacts only (or disabled if too complicated)
			c: Other User=1, Contact=1 --> User & contact selection enabled
			*/

			bool bEnableCalendar=true;
			if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS))
			{
				bEnableCalendar=false;
			}
			if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS))
			{
				int nPersonID=ui.frameContactOwner->GetSelectionController()->GetPersonIDFromContactID(m_nEntityRecordID);
				if(nPersonID>0 && nPersonID != g_pClientManager->GetPersonID()) //if person_id then implement this rule if not exists 
					bEnableCalendar=false;
			}
			if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS) && g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS))
			{
				int nPersonID=ui.frameContactOwner->GetSelectionController()->GetPersonIDFromContactID(m_nEntityRecordID);
				if (nPersonID>0)
					bEnableCalendar=true;
			}


			m_pCalendarButton->setEnabled(bEnableCalendar);
		}
		else
			m_pCalendarButton->setEnabled(false);
	}


	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}




void FUI_Contacts::on_btnPerson_toggled(bool bChecked)
{
	if(bChecked)
	{
		ui.stackedWidget->setCurrentIndex(0);
		ui.horizontalSpacerDetails->changeSize(1,188,QSizePolicy::Minimum,QSizePolicy::Minimum);
		ui.framePersonDetails->setVisible(true);
		ui.txtFirstName->setFocus(Qt::OtherFocusReason);
	}
	else
	{
		ui.stackedWidget->setCurrentIndex(1);
		ui.horizontalSpacerDetails->changeSize(1,188,QSizePolicy::Expanding,QSizePolicy::Minimum);
		ui.framePersonDetails->setVisible(false);
		ui.txtOrgName->setFocus(Qt::OtherFocusReason);
	}

}


void FUI_Contacts::on_ckbSynchronize_toggled(bool bChecked)
{
	m_bSyncNames=bChecked;
}



/*!
	Called after WRITE, refresh NAME column

	\param recNewData	- one row record with entity data
*/
void FUI_Contacts::prepareCacheRecord(DbRecordSet *recNewData)
{
	//TO FIX: copy all lists:

	//fill name (calculated field):)
	DbRecordSet newCacheRecord;
	newCacheRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
	newCacheRecord.merge(*recNewData);

	QString strName=m_CalcName.GetCalculatedName(*recNewData);

	newCacheRecord.setData(0,"BCNT_NAME",strName);

	*recNewData=newCacheRecord;

}



/*!
	Called after sucessfull WRITE, refresh NAME column

	\param recNewData	- one row record with entity data
*/
void FUI_Contacts::notifyCache_AfterWrite(DbRecordSet recNewData)
{
	FuiBase::notifyCache_AfterWrite(recNewData);

	//notify all other ACP patterns:
	DbRecordSet newRecord;
	QString strOrg;
	newRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_SUBSET_SELECTION));
	newRecord.addRow();
	
	//-----------------------------
	//Org
	//-----------------------------
	strOrg=recNewData.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString();
	newRecord.setData(0,1,strOrg);
	g_ClientCache.ModifyCache(ENTITY_CONTACT_ORGANIZATION,newRecord,DataHelper::OP_EDIT,this); //will add if not exists
	
	//-----------------------------
	//Dept:
	//-----------------------------
	strOrg=recNewData.getDataRef(0,"BCNT_DEPARTMENTNAME").toString();
	newRecord.setData(0,1,strOrg);
	g_ClientCache.ModifyCache(ENTITY_CONTACT_DEPARTMENT,newRecord,DataHelper::OP_EDIT,this); //will add if not exists


	//-----------------------------
	//Function:
	//-----------------------------
	strOrg=recNewData.getDataRef(0,"BCNT_FUNCTION").toString();
	DbRecordSet lstDataF;
	lstDataF.addColumn(QVariant::String,"NAME");
	lstDataF.addRow();
	lstDataF.setData(0,0,strOrg);
	g_ClientCache.ModifyCache(ENTITY_CONTACT_FUNCTION,lstDataF,DataHelper::OP_EDIT,this); //will add if not exists

	//-----------------------------
	//Profession:
	//-----------------------------
	strOrg=recNewData.getDataRef(0,"BCNT_PROFESSION").toString();
	newRecord.setData(0,1,strOrg);
	g_ClientCache.ModifyCache(ENTITY_CONTACT_PROFESSION,newRecord,DataHelper::OP_EDIT,this); //will add if not exists

}


void FUI_Contacts::on_cmdInsert()
{
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);


	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_CONTACTS_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_CM_CONTACT, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed contacts: ")+QVariant(nCnt).toString()+tr("!\nCan not insert new contact!"));
			return;
		}
	}

	//QCW: intercept:
	if (m_buttonQCW->isChecked())
	{
		m_pQCW_LastOpened=dynamic_cast<ObsrPtrn_Subject*>(g_objFuiManager.OpenQCWWindow(this));
		if (m_pQCW_LastOpened)
		{
			return;  //QCW successfully opened
		}
	}	

	//issue 2466: (if CTRL is pressed then go, and set chkbox)
	Qt::KeyboardModifiers keys= QApplication::keyboardModifiers(); //issue 2466
	if(Qt::ControlModifier == (Qt::ControlModifier & keys))
	{
		m_buttonQCW->blockSignals(true);
		m_buttonQCW->setChecked(true);
		m_buttonQCW->blockSignals(false);
		m_pQCW_LastOpened=dynamic_cast<ObsrPtrn_Subject*>(g_objFuiManager.OpenQCWWindow(this));
		if (m_pQCW_LastOpened)
		{
			return;  //QCW successfully opened
		}
	}



	ui.tabWidgetContact->setCurrentIndex(1);
	FuiBase::on_cmdInsert();

	//set SAPNE Contact owner to the logged user
	int nLoggedPersonID=g_pClientManager->GetPersonID();
	if(nLoggedPersonID>0)
		ui.frameContactOwner->SetCurrentEntityRecord(nLoggedPersonID);

	ui.frameGroupContacts->Clear();

}

//switch to detail tab:
bool FUI_Contacts::on_cmdEdit()
{
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_EDIT,m_nEntityRecordID))
		_CHK_ERR_RET_BOOL_ON_FAIL(err);

	ui.tabWidgetContact->setCurrentIndex(1);

	//issue 
	if (!m_bGridDialogRequestedEditMode)
	{
		//QCW: intercept:
		if (m_buttonQCW->isChecked())
		{
			m_pQCW_LastOpened=dynamic_cast<ObsrPtrn_Subject*>(g_objFuiManager.OpenQCWWindow(this,m_nEntityRecordID));
			if (m_pQCW_LastOpened)
			{
				return false;  //QCW successfully opened
			}
		}	

		//issue 2466: (if CTRL is pressed then go, and set chkbox)
		Qt::KeyboardModifiers keys= QApplication::keyboardModifiers(); //issue 2466
		if(Qt::ControlModifier == (Qt::ControlModifier & keys))
		{
			m_buttonQCW->blockSignals(true);
			m_buttonQCW->setChecked(true);
			m_buttonQCW->blockSignals(false);
			m_pQCW_LastOpened=dynamic_cast<ObsrPtrn_Subject*>(g_objFuiManager.OpenQCWWindow(this,m_nEntityRecordID));
			if (m_pQCW_LastOpened)
			{
				return false;  //QCW successfully opened
			}
		}
	}


	return FuiBase::on_cmdEdit();
}


bool FUI_Contacts::on_cmdDelete()
{
	Status err; 
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_DELETE,m_lstData))
	_CHK_ERR_RET_BOOL_ON_FAIL(err);

	return FuiBase::on_cmdDelete();
}
//switch to detail tab:
void FUI_Contacts::on_cmdCopy()
{
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_CONTACTS_NUMBER_LIMITATION, nValue) && nValue > 0)
	{
		//check current number of projects
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_CM_CONTACT, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed contacts: ")+QVariant(nCnt).toString()+tr("!\nCan not insert new contact!"));
			return;
		}
	}

	ui.tabWidgetContact->setCurrentIndex(1);
	//ui.frameGroupContacts->Clear();
	ui.frameContactOwner->SetCurrentEntityRecord(g_pClientManager->GetPersonID());
	FuiBase::on_cmdCopy();
}


//get FUI name
QString FUI_Contacts::GetFUIName()
{
	if(m_nEntityRecordID>0)
		return tr("Contact: ")+m_p_Selector->GetCalculatedName(m_nEntityRecordID);
	else
		return tr("Contact");
}



//When chkbox is activated and sync fields are changed, change all addresses, load them if not
void FUI_Contacts::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject == m_p_Selector && nMsgCode == SelectorSignals::SELECTOR_ON_CONTENT_CHANGE)
	{
		onActualListChange();
		return;
	}
	if (pSubject == m_p_Selector && nMsgCode == SelectorSignals::SELECTOR_ON_SHOW_DETAILS)
	{
		OnShowContactDetails(nMsgDetail);
		return;
	}
	if (pSubject == m_p_Selector && nMsgCode == SelectorSignals::SELECTOR_ON_SHOW_DETAILS_NEW_WINDOW)
	{
		int nNewFUI=g_objFuiManager.OpenFUI(MENU_CONTACTS,true,false,FuiBase::MODE_READ,nMsgDetail,true);
		QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
		FUI_Contacts* pFuiW=dynamic_cast<FUI_Contacts*>(pFUI);
		if (pFuiW)
		{
			pFuiW->SetCurrentTab(1);
			pFuiW->show();
		}
		return;
	}
	if (pSubject == &g_ChangeManager && nMsgCode == ChangeManager::GLOBAL_CONTACT_FAVORITE_ADD_PICTURE && m_nMode==MODE_READ && nMsgDetail == m_nEntityRecordID)
	{
		//reload picture (in main row):
		DataClear();
		m_nEntityRecordID=-1;
		on_selectionChange(nMsgDetail);
		return;
	}

	if(pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_REFRESH_GROUP_DATA_INSIDE_CONTACT_DETAILS)
	{
		//reload group data if contact is loaded:
		DbRecordSet lstGroups;
		Status err;

		_SERVER_CALL(BusContact->ReadContactGroup(err, m_nEntityRecordID, lstGroups));
		//lstGroups.Dump();

		ui.frameGroupContacts->Clear();
		ui.frameGroupContacts->SetList(lstGroups);
		return;
	}
	

	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);

	//QCW:
	if (pSubject==m_pQCW_LastOpened && nMsgCode==ChangeManager::GLOBAL_QCW_WINDOW_FINISH_PROCESS)
	{
		DbRecordSet lstAddedData=val.value<DbRecordSet>();

		//extract operation:
		if (lstAddedData.getRowCount()==1)
		{
			//clear prev
			m_nEntityRecordID=-1; //reset id to enable reload:
			DataClear();
			SetMode(MODE_EMPTY);

			//set new, but send notify signals before select..to upload all new orgs, dept to cache
			int nContID=lstAddedData.getDataRef(0,"BCNT_ID").toInt();
			//notifyCache_AfterWrite(lstAddedData);
			on_selectionChange(nContID);
			
			//set id on selector
			if(m_pSelectionController)m_pSelectionController->SetCurrentEntityRecord(m_nEntityRecordID,true); //fire one more event!

		}
	}
	
}


//when clicked if not loaded, load:
void FUI_Contacts::on_groupBoxInfo_clicked()
{
	//based on checkbook:
	if (ui.groupBoxInfo->isChecked())
	{
		ui.groupBoxInfo->setMinimumHeight(207);
		ui.frameX->setVisible(true);	
		//issue 1556:
		if (m_bSideBarModeView)
		{
			QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
			if (this->frameGeometry().height()>desktopRec.height())
			{
				this->showMaximized();
			}
			else
			{
				int nY=desktopRec.y()+10;//+desktopRec.height()/2-this->frameGeometry().height()/2;
				this->move(this->x(),nY);
			}
		}
	}
	else
	{
		ui.groupBoxInfo->setMinimumHeight(25);
		ui.frameX->setVisible(false);
	}
}

//TO FIX: change when default is changed
bool FUI_Contacts::on_cmdOK()
{
	//u never know, before save, intercept
	if (m_nMode==MODE_EDIT)
	{
		Status err;
		if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_EDIT,m_nEntityRecordID))
			_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}
	if (m_nMode==MODE_INSERT)
	{
		Status err;
		if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
			_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}

	int nModePrevious=m_nMode;
	bool bOK= FuiBase::on_cmdOK();
	if(!bOK)return false;


	//m_lstData.Dump();

	//only after insert:
	if (bOK && nModePrevious==MODE_INSERT)
	{
		//get name
		QString strName=m_p_Selector->GetCalculatedName(m_nEntityRecordID);
		ui.widgetCommPerson->Reload(strName,m_nEntityRecordID,true);
		ui.widgetCommProject->Reload(strName,m_nEntityRecordID,true);
		ui.tableJournal->Reload(m_nEntityRecordID,true);
		//Grid - complex filter or not.
		ui.contactGrid->SetData(m_nEntityRecordID);
	}
	
	if (bOK && nModePrevious==MODE_EDIT) //issue 1725: if change 1 change all:
	{
		if ( m_lstData.getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION && m_strPrevOrgName != m_lstData.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString() && !m_strPrevOrgName.trimmed().isEmpty())
		{
			int nResult=QMessageBox::question(NULL,tr("Confirmation"),tr("Shall the new organization name automatically be changed in all assigned persons, too?"),tr("Yes"),tr("No"));
			if (nResult==0) 
			{
				Status Ret_pStatus;
				g_pBoSet->app->BusContact->UpdateOrganization(Ret_pStatus,m_strPrevOrgName,ui.framePersonOrganization->GetCurrentDisplayName());
				_CHK_ERR_NO_RET(Ret_pStatus);
				if(Ret_pStatus.IsOK())
				{
					//ui.framePersonOrganization->GetSelectionController()->ReloadData(true);
					m_p_Selector->ReloadData(true);
				}
			}
		}
	}
	
	//m_lstData.Dump();
	return bOK;
}



//either from email grid default or from selected from quick info list
QString FUI_Contacts::GetDefaultMail()
{
	//contact empty:
	if (m_nMode==MODE_EMPTY)
		return "";
	//get selected entries from table if in focus:
	if (ui.tabWidgetContact->currentIndex()==TAB_MAIN_DETAILS && ui.tabContactGroups->currentIndex()==TAB_DETAIL_EMAIL)
	{
		if (m_lstEmails.getSelectedRow()>0)
		{
			return m_lstEmails.getDataRef(m_lstEmails.getSelectedRow(),"BCME_ADDRESS").toString();
		}
	}
	//get selected entries from quick info if shown:
	if (ui.groupBoxInfo->isChecked()) 
	{
		QString strEmail=ui.cmbInfo_Email-> itemData(ui.cmbInfo_Email->currentIndex()).toString();
		if (!strEmail.isEmpty())
			return strEmail;
	}
	//return default email if exists:
	return ui.tableEmail->GetDefaultEntry();
}

QString FUI_Contacts::GetDefaultInternet()
{
	//contact empty:
	if (m_nMode==MODE_EMPTY)
		return "";
	QString strDefaultInternet;

	//get selected entries from table if in focus:
	if (ui.tabWidgetContact->currentIndex()==TAB_MAIN_DETAILS && ui.tabContactGroups->currentIndex()==TAB_DETAIL_INTERNET)
	{
		if (m_lstInternet.getSelectedRow()>0)
		{
			strDefaultInternet=m_lstInternet.getDataRef(m_lstInternet.getSelectedRow(),"BCMI_ADDRESS").toString();
		}
	}
	//get selected entries from quick info if shown:
	if (ui.groupBoxInfo->isChecked() && strDefaultInternet.isEmpty()) 
	{
		strDefaultInternet=ui.cmbInfo_Internet-> itemData(ui.cmbInfo_Internet->currentIndex()).toString();
	}
	//return default email if exists:
	if (strDefaultInternet.isEmpty())
		strDefaultInternet=ui.tableInternet->GetDefaultEntry();

	//prepend http before internet address
	if (!strDefaultInternet.isEmpty())
		if (strDefaultInternet.indexOf("http://")==0)
			return strDefaultInternet;
		else
			return "http://"+strDefaultInternet;

	return "";
}

QString FUI_Contacts::GetDefaultPhoneCall()
{
	//contact empty:
	if (m_nMode==MODE_EMPTY)
		return "";
	//get selected entries from table if in focus:
	if (ui.tabWidgetContact->currentIndex()==TAB_MAIN_DETAILS && ui.tabContactGroups->currentIndex()==TAB_DETAIL_PHONE)
	{
		if (m_lstPhones.getSelectedRow()>0)
		{
			return m_lstPhones.getDataRef(m_lstPhones.getSelectedRow(),"BCMP_SEARCH").toString();
		}
	}
	//get selected entries from quick info if shown:
	if (ui.groupBoxInfo->isChecked()) 
	{
		QString strPhone=ui.cmbInfo_Phone-> itemData(ui.cmbInfo_Phone->currentIndex()).toString();
		if (!strPhone.isEmpty())
			return strPhone;
	}
	//return default email if exists:
	return ui.tablePhone->GetDefaultEntry();
}

//from address, phones, emails, sets data
void FUI_Contacts::RefreshQuickInfo()
{

	DbRecordSet* lstEmail=NULL,*lstPhone=NULL,*lstInternet=NULL, *lstAddress=NULL;
	DbRecordSet *lstAddressTypes=NULL;

	//-------------------------------------
	//		ADDRESS
	//-------------------------------------

	//address:
	lstAddress=&m_lstAddress;
	lstPhone=&m_lstPhones;
	lstEmail=&m_lstEmails;
	lstInternet=&m_lstInternet;

	//refill sublists:
	ClientContactManager::PrepareListsWithTypeName(lstEmail,lstPhone,lstInternet,NULL,NULL);

	//-------------------------------------
	//		ADDRESS
	//-------------------------------------
	QString strDisplay;
	if (lstAddress)
	{

		//"BCMT_TYPE_NAME<<' - '<<BCMA_STREET<<' / '<<BCMA_ZIP<<' '<<BCMA_CITY"
		ui.cmbInfo_Address->clear();
		ui.cmbInfo_Address->blockSignals(true);

		int nSize=lstAddress->getRowCount();
		for(int i=0;i<nSize;++i)
		{

			if (!lstAddress->getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
				strDisplay=lstAddress->getDataRef(i,"BCMT_TYPE_NAME").toString()+" - ";
			else
				strDisplay="";
			
			if (!lstAddress->getDataRef(i,"BCMA_STREET").toString().isEmpty())
				strDisplay+=lstAddress->getDataRef(i,"BCMA_STREET").toString()+" / "+lstAddress->getDataRef(i,"BCMA_ZIP").toString()+" "+lstAddress->getDataRef(i,"BCMA_CITY").toString();
			else
				strDisplay+=lstAddress->getDataRef(i,"BCMA_ZIP").toString()+" "+lstAddress->getDataRef(i,"BCMA_CITY").toString();

			ui.cmbInfo_Address->addItem(strDisplay,lstAddress->getDataRef(i,"BCMA_FORMATEDADDRESS")); //add display + proper email without type
		}

		int nRow=lstAddress->find(lstAddress->getColumnIdx("BCMA_IS_DEFAULT"),1,true);
		if (nRow!=-1)
			ui.cmbInfo_Address->setCurrentIndex(nRow);
		
		on_cmbInfo_Address_currentIndexChanged(nRow);

		ui.labelInfoAddress->setText(QVariant(nSize).toString()+" "+tr("Address(es):"));
		ui.cmbInfo_Address->blockSignals(false);
	}




	//-------------------------------------
	//		EMAIL
	//-------------------------------------
	if (lstEmail)
	{

		ui.cmbInfo_Email->clear();

		int nSize=lstEmail->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			if (!lstEmail->getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
				strDisplay=lstEmail->getDataRef(i,"BCMT_TYPE_NAME").toString()+" - "+lstEmail->getDataRef(i,"BCME_ADDRESS").toString();
			else
				strDisplay=lstEmail->getDataRef(i,"BCME_ADDRESS").toString();

			ui.cmbInfo_Email->addItem(strDisplay,lstEmail->getDataRef(i,"BCME_ADDRESS")); //add display + proper email without type
		}

		int nRow=lstEmail->find(lstEmail->getColumnIdx("BCME_IS_DEFAULT"),1,true);
		if (nRow!=-1)
			ui.cmbInfo_Email->setCurrentIndex(nRow);

		ui.labelInfoEmail->setText(QVariant(nSize).toString()+" "+tr("Email Address(es):"));
	}


	//-------------------------------------
	//		INTERNET
	//-------------------------------------
	if (lstInternet)
	{

		ui.cmbInfo_Internet->clear();

		int nSize=lstInternet->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			if (!lstInternet->getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
				strDisplay=lstInternet->getDataRef(i,"BCMT_TYPE_NAME").toString()+" - "+lstInternet->getDataRef(i,"BCMI_ADDRESS").toString();
			else
				strDisplay=lstInternet->getDataRef(i,"BCMI_ADDRESS").toString();

			ui.cmbInfo_Internet->addItem(strDisplay,lstInternet->getDataRef(i,"BCMI_ADDRESS")); //add display + proper email without type
		}

		int nRow=lstInternet->find(lstInternet->getColumnIdx("BCMI_IS_DEFAULT"),1,true);
		if (nRow!=-1)
			ui.cmbInfo_Internet->setCurrentIndex(nRow);

		ui.labelInfoInternet->setText(QVariant(nSize).toString()+" "+tr("Internet Address(es):"));
	}

	//-------------------------------------
	//		PHONE
	//-------------------------------------
	if (lstPhone)
	{
		//get default address:
		ui.cmbInfo_Phone->clear();
		int nSize=lstPhone->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			if (!lstPhone->getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
				strDisplay=lstPhone->getDataRef(i,"BCMT_TYPE_NAME").toString()+" - "+lstPhone->getDataRef(i,"BCMP_FULLNUMBER").toString();
			else
				strDisplay=lstPhone->getDataRef(i,"BCMP_FULLNUMBER").toString();

			ui.cmbInfo_Phone->addItem(strDisplay,lstPhone->getDataRef(i,"BCMP_SEARCH"));
		}

		int nRow=lstPhone->find(lstPhone->getColumnIdx("BCMP_IS_DEFAULT"),1,true);
		if (nRow!=-1)
			ui.cmbInfo_Phone->setCurrentIndex(nRow);

		ui.labelInfoPhone->setText(QVariant(nSize).toString()+" "+tr("Phone(s):"));

	}


	//NAME:
	QString strName;
	if (m_nEntityRecordID>0)
		strName=m_p_Selector->GetCalculatedName(m_nEntityRecordID);
	ui.labelContactName->setText(m_strHtmlTag+strName+m_strEndHtmlTag);
	ui.labelContactName->setToolTip(ui.labelContactName->text());


}


//reload 
void FUI_Contacts::on_selectionChange(int nEntityRecordID)
{
	if(nEntityRecordID==m_nEntityRecordID) return; //if already loaded exit
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_READ,nEntityRecordID))
		_CHK_ERR(err);

	//read data:
	FuiBase::on_selectionChange(nEntityRecordID);
	if(nEntityRecordID<=0)
		return;

	//Grid - complex filter or not.
	if (!m_bOpenedAsAvatar)
		ui.contactGrid->SetData(nEntityRecordID);
	else
		m_bOpenedAsAvatar = false;

	if (m_lstData.getRowCount()>0)
		m_strPrevOrgName=m_lstData.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString();
	else
		m_strPrevOrgName="";

	if (ui.tabWidgetContact->currentIndex()==TAB_MAIN_COMM)
		if (m_nMode==MODE_READ)
			ui.btnButtonBar->SetMode(MODE_EMPTY);
}

void FUI_Contacts::ReloadFromAvatar()
{
	ui.contactGrid->SetData(m_nEntityRecordID);
}

void FUI_Contacts::on_cmbInfo_Address_currentIndexChanged(int nIndex)
{
	if (nIndex>=0)
	{
		QString strAddress= ui.cmbInfo_Address->itemData(nIndex).toString();
		ui.txtInfo_Address->setPlainText(strAddress);
	}
	else
	{
		ui.txtInfo_Address->setPlainText("");
	}
}

//when actual, send flagger:
void FUI_Contacts::on_tabWidgetMain_currentChanged(int nIndex)
{
	if(nIndex==2)
		ui.tableActual->SetFlagVisibile(true);
	else
		ui.tableActual->SetFlagVisibile(false);


}

void FUI_Contacts::OnShowContactDetails(int nContactID)
{
	if (!g_objFuiManager.IsActiveWindowCurrentFUI(this))
		return;

	if (m_nMode==MODE_READ || m_nMode==MODE_EMPTY)
	{
		SetCurrentTab(1);
		on_selectionChange(nContactID);
	}
}


bool FUI_Contacts::CanClose()
{
	bool bOK=FuiBase::CanClose();
	if (!bOK) return bOK;   //if already in edit mode, return
	
	//else test: if group tree is in edit mode:
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
		return true;

	//in sidebar m_pFrameContacts is inactive...
	if (m_p_Selector == m_pFrameContacts)
	{
		Selection_GroupTree* pGroup=m_pFrameContacts->GetGroupSelector();
		if (pGroup->IsEditMode())
		{
			QMessageBox::information(this,tr("Warning"),tr("Can not exit Contact Window while editing group tree!"));
			return false;
		}
		else
			return true;
	}
	else
		return true;

}



void FUI_Contacts::OnCEMenu_NeedNewData(int nSubMenuType)
{
	if (!g_objFuiManager.IsActiveWindowCurrentFUI(this) && m_bSideBarModeView) 
		return;


	//which contact is actual:
	DbRecordSet lstContacts;
	lstContacts.addColumn(QVariant::Int,"BCNT_ID");

	//issue 2022: if favorites, use current record
	if (!m_bSideBarModeView && !ui.frameContactAndFavs->IsContactFrameVisible())
	{
		if (m_nEntityRecordID>0)
		{
			m_p_CeMenu->SetDefaultMail(GetDefaultMail());
			m_p_CeMenu->SetDefaultPhone(GetDefaultPhoneCall());
			m_p_CeMenu->SetDefaultInternet(GetDefaultInternet());

			lstContacts.addRow();
			lstContacts.setData(0,0,m_nEntityRecordID);
			m_p_CeMenu->SetDefaults(&lstContacts);
			return;
		}
	}

	//issue 1819: use contact list selection for active contacts if serial mail:
	if (nSubMenuType==CommunicationMenu_Base::SUBMENU_EMAIL)
	{
		DbRecordSet lstData;
		m_pFrameContacts->GetSelection(lstData);
		lstContacts.merge(lstData);
		m_p_CeMenu->SetDefaults(&lstContacts);
		return;
	}

	
	switch(ui.tabWidgetMain->currentIndex())
	{
	case TAB_HEADER_CONTACT:
		if (m_nEntityRecordID>0)
		{
			m_p_CeMenu->SetDefaultMail(GetDefaultMail());
			m_p_CeMenu->SetDefaultPhone(GetDefaultPhoneCall());
			m_p_CeMenu->SetDefaultInternet(GetDefaultInternet());

			lstContacts.addRow();
			lstContacts.setData(0,0,m_nEntityRecordID);
			m_p_CeMenu->SetDefaults(&lstContacts);
			return;
		}
		break;
	case TAB_HEADER_GROUP_LIST:
		{
			QString strWebOfFirstContact;
			m_p_CeMenu->SetDefaultMail("");
			m_p_CeMenu->SetDefaultPhone("");

			DbRecordSet lstContactsFull;
			ui.tableGroup->GetSelectedContacts(lstContactsFull);
			if (lstContactsFull.getRowCount()>0)
			{
				lstContacts.copyDefinition(lstContactsFull);
				lstContacts.merge(lstContactsFull);
				m_p_CeMenu->SetDefaults(&lstContacts);

				//get internet address of first contact:
				DbRecordSet lstInternet=lstContacts.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
				if (lstInternet.getRowCount()>0)
				{
					int nRow=lstInternet.find(lstInternet.getColumnIdx("BCMI_IS_DEFAULT"),1,true);
					if (nRow>=0)
					{
						strWebOfFirstContact=lstInternet.getDataRef(nRow,"BCMI_ADDRESS").toString();
					}
				}
			}
			m_p_CeMenu->SetDefaultInternet(strWebOfFirstContact);
		}
	    break;
	case TAB_HEADER_CONTACT_ACTUAL_DETAIL_LIST:
		{
			QString strWebOfFirstContact;
			m_p_CeMenu->SetDefaultMail("");
			m_p_CeMenu->SetDefaultPhone("");

			DbRecordSet lstContactsFull;
			ui.tableActual->GetSelectedContacts(lstContactsFull);
			if (lstContactsFull.getRowCount()>0)
			{
				lstContacts.copyDefinition(lstContactsFull);
				lstContacts.merge(lstContactsFull);
				m_p_CeMenu->SetDefaults(&lstContacts);
				//get internet address of first contact:
				DbRecordSet lstInternet=lstContacts.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
				if (lstInternet.getRowCount()>0)
				{
					int nRow=lstInternet.find(lstInternet.getColumnIdx("BCMI_IS_DEFAULT"),1,true);
					if (nRow>=0)
					{
						strWebOfFirstContact=lstInternet.getDataRef(nRow,"BCMI_ADDRESS").toString();
					}
				}

			}
			m_p_CeMenu->SetDefaultInternet(strWebOfFirstContact);
		}
	    break;
	}

	

}

void FUI_Contacts::SaveCommGridChBoxFilterState(int nState, int nSettingID)
{
	//Get state.
	bool bState = false;
	if (nState==Qt::Checked)
		bState = true;
	
	//Write.
	g_pSettings->SetPersonSetting(nSettingID, bState);
}





void FUI_Contacts::OnNMRXProjectsChanged()
{
	QString strTabTitle;
	int nProjects=ui.widgetCommProject->GetRowCountForAssignment(BUS_PROJECT);
	if (nProjects)
		strTabTitle+=tr("Projects (")+QVariant(nProjects).toString()+")";
	else
		strTabTitle+=tr("Projects");
	ui.tabWidgetComm->setTabText(2,strTabTitle);

}
void FUI_Contacts::OnNMRXContactsChanged()
{
	int nContacts=ui.widgetCommPerson->GetRowCountForAssignment(BUS_CM_CONTACT);
	int nPersons=ui.widgetCommPerson->GetRowCountForAssignment(BUS_PERSON);

	QString strTabTitle="";
	if (nContacts)
		strTabTitle+=tr("Contacts (")+QVariant(nContacts).toString()+")";
	else
		strTabTitle+=tr("Contacts");
	strTabTitle+=tr(" And ");
	if (nPersons)
		strTabTitle+=tr("Users (")+QVariant(nPersons).toString()+")";
	else
		strTabTitle+=tr("Users");

	ui.tabWidgetComm->setTabText(1,strTabTitle);

}
void FUI_Contacts::OnCEGridChanged()
{
	int nDocs=ui.contactGrid->GetDocumentsRecordSet()->getRowCount();
	int nEmails=ui.contactGrid->GetEmailRecordSet()->getRowCount();
	int nVoice=ui.contactGrid->GetVoiceCallRecordSet()->getRowCount();

	QString strTabTitle=tr("Overview");
	if (nDocs)
		strTabTitle+=tr(" Documents (")+QVariant(nDocs).toString()+")";
	if (nEmails)
		strTabTitle+=tr(" E-mails (")+QVariant(nEmails).toString()+")";
	if (nVoice)
		strTabTitle+=tr(" Voice Calls (")+QVariant(nVoice).toString()+")";

	ui.tabWidgetComm->setTabText(0,strTabTitle);

}
void FUI_Contacts::OnJournalGridChanged()
{
	QString strTabTitle="";
	int nJournals=ui.tableJournal->GetDataSource()->getRowCount();
	if (nJournals)
		strTabTitle+=tr("Journals (")+QVariant(nJournals).toString()+")";
	else
		strTabTitle+=tr("Journal");
	ui.tabWidgetComm->setTabText(3,strTabTitle);

}


//write group assigments after copy
void FUI_Contacts::WriteContactGroupAssignmentsAfterCopy()
{
	if (m_lstGroupContactAssignmentsForCopy.getRowCount()==0)
		return;
	
	DbRecordSet lstGroupContent;
	lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
	int nSize=m_lstGroupContactAssignmentsForCopy.getRowCount();
	lstGroupContent.addRow(nSize);
	lstGroupContent.setColValue(lstGroupContent.getColumnIdx("BGCN_CONTACT_ID"),m_nEntityRecordID);

	for (int i=0;i<nSize;i++)
	{
		int nNodeID=m_lstGroupContactAssignmentsForCopy.getDataRef(i,"BGCN_ITEM_ID").toInt();
		lstGroupContent.setData(i,"BGCN_ITEM_ID",nNodeID);
	}


	//add contact to group, if not already
	Status err;
	_SERVER_CALL(BusGroupTree->WriteGroupContentData(err,ENTITY_BUS_CONTACT,lstGroupContent))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	_SERVER_CALL(BusContact->ReadContactGroup(err,m_nEntityRecordID,lstGroupContent))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	ui.frameGroupContacts->Clear();
	ui.frameGroupContacts->SetList(lstGroupContent);
}




void FUI_Contacts::OnGroupSAPNE_Add(DbRecordSet &lstData)
{
	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	if (lstData.getRowCount()==0)return;
	DbRecordSet lstGroupContent;
	lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
	int nGroupId=lstData.getDataRef(0,"BGIT_ID").toInt();
	int nTreeId=lstData.getDataRef(0,"BGIT_TREE_ID").toInt();
	lstGroupContent.addRow();
	lstGroupContent.setColValue(lstGroupContent.getColumnIdx("BGCN_CONTACT_ID"),m_nEntityRecordID);
	lstGroupContent.setColValue(lstGroupContent.getColumnIdx("BGCN_ITEM_ID"),nGroupId);

	

	//add contact to group, if not already
	//Status err;
	_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nGroupId,DataHelper::OP_ADD,lstGroupContent))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		lstData.clear();
		return;
	}

	lstGroupContent.find("BGCN_CONTACT_ID",m_nEntityRecordID);
	lstGroupContent.deleteUnSelectedRows();
	lstGroupContent.find("BGCN_ITEM_ID",nGroupId);
	lstGroupContent.deleteUnSelectedRows();

	if (lstGroupContent.getRowCount()==0)
	{
		QMessageBox::critical(NULL,tr("Error"),tr("Error while fetching saved data. Please refresh contact data!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		lstData.clear();
		return;


	}

	//copy new ID:
	lstData.setData(0,"BGCN_ID",lstGroupContent.getDataRef(0,"BGCN_ID"));
	lstData.setData(0,"BGCN_ITEM_ID",nGroupId);
	lstData.setData(0,"BGCN_CONTACT_ID",m_nEntityRecordID);

	//fill tree name:
	DbRecordSet *lstTrees=g_ClientCache.GetCache(ENTITY_BUS_CONTACT_TREES);
	if (lstTrees)
	{
		int nRow=lstTrees->find("BGTR_ID",nTreeId,true);
		if (nRow!=-1)
		{
			lstData.setData(0,"BGTR_NAME",lstTrees->getDataRef(nRow,"BGTR_NAME"));
		}
	}
	

	

	
}

void FUI_Contacts::OnGroupSAPNE_Remove(DbRecordSet &lstData)
{
	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);
	
	if (lstData.getRowCount()==0)return;
	DbRecordSet lstGroupContent;
	lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
	lstGroupContent.merge(lstData);
	

	DbRecordSet lstGroups;
	lstGroups.addColumn(QVariant::Int,"BGCN_ITEM_ID");
	lstGroups.merge(lstData);
	lstGroups.setColumn(0,QVariant::Int,"BGIT_ID");   //change name to proper one
	lstGroups.removeDuplicates(0);


	//add contact to group, if not already
	//Status err;
	_SERVER_CALL(BusGroupTree->RemoveGroupContentItem(err,ENTITY_BUS_CONTACT,lstGroupContent,lstGroups))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		lstData.clear();
		return;
	}

}

void FUI_Contacts::OnGroupSAPNE_Edit()
{
	//B.T. issue 2717
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_INSERT))
		_CHK_ERR(err);

	//enter dialog:
	Dlg_EnterPeriod Dlg;
	
	DbRecordSet lstData;
	ui.frameGroupContacts->GetList(lstData);

	int nRow=lstData.getSelectedRow();
	if (nRow==-1) return;

	Dlg.setPeriod(lstData.getDataRef(nRow,"BGCN_CMCA_VALID_FROM").toDate(),lstData.getDataRef(nRow,"BGCN_CMCA_VALID_TO").toDate());
	if(Dlg.exec())
	{
		QDate from,to;
		Dlg.getPeriod(from,to);
		lstData.setData(nRow,"BGCN_CMCA_VALID_FROM",from);
		lstData.setData(nRow,"BGCN_CMCA_VALID_TO",to);

		DbRecordSet row=lstData.getRow(nRow);

		DbRecordSet lstGroupContent;
		lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
		lstGroupContent.merge(row);

		Status err;
		_SERVER_CALL(BusGroupTree->WriteGroupContentData(err,ENTITY_BUS_CONTACT,lstGroupContent))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		ui.frameGroupContacts->SetList(lstData);
	}
}



//0 - comm grid,
//1 - details
void FUI_Contacts::SetCurrentTab(int nTabPane, int nDetailTab)
{
	ui.tabWidgetContact->setCurrentIndex(nTabPane);
	OnTabWidgetContact_currentChanged(nTabPane);
	if (nTabPane==1)
	{
		ui.tabWidgetMain->setCurrentIndex(0);
		if (nDetailTab>=0)
			ui.tabContactGroups->setCurrentIndex(nDetailTab);
	}
	else
	{
		if (nDetailTab>=0)
		{
			if (nDetailTab==5)
				nDetailTab=0;
			ui.tabWidgetComm->setCurrentIndex(nDetailTab);
		}
	}
}

void FUI_Contacts::GetCurrentTab(int &nTabPane, int &nDetailTab, int &nCommTab)
{
	nTabPane	= ui.tabWidgetContact->currentIndex();
	nDetailTab	= ui.tabWidgetContact->currentIndex();
	nCommTab	= ui.tabWidgetComm->currentIndex();
}

//Drop zone detect drop: try to process it:
void FUI_Contacts::OnCEMenu_NewDataDropped(int nEntityID,DbRecordSet lstData)
{
	if (lstData.getRowCount()==0)
		return;

	if (!g_objFuiManager.IsActiveWindowCurrentFUI(this) && m_bSideBarModeView) 
		return;

	//accept call:
	if (nEntityID==ENTITY_CONTACT_VCARD_IMPORT)
	{
		if (m_nMode==MODE_READ)
		{
			//NAME:
			QString strName;
			if (m_nEntityRecordID>0)
				strName=m_p_Selector->GetCalculatedName(m_nEntityRecordID);

			Dlg_ContactVcardImport Dlg;
			Dlg.SetData(m_nEntityRecordID,strName,lstData);
			Dlg.exec();


			//reload contact_
			int nID=m_nEntityRecordID;
			m_nEntityRecordID=-1; //reset id to enable reload:
			DataClear();
			SetMode(MODE_EMPTY);
			on_selectionChange(nID);
		}
		else
		{
			Dlg_ContactVcardImport Dlg;
			Dlg.SetData(-1,"",lstData);
			Dlg.exec();
		}
		return;
	}



	//if non mode, insert or edit
	if (nEntityID==ENTITY_BUS_CM_EMAIL || nEntityID ==ENTITY_BUS_CM_PHONE || nEntityID ==ENTITY_BUS_CM_INTERNET || nEntityID ==ENTITY_BUS_CM_ADDRESS ||  nEntityID ==ENTITY_BUS_BIG_PICTURE )
	{
		if (m_nMode==MODE_EMPTY)
		{
			on_cmdInsert();
		}

		if (m_nMode==MODE_READ)
		{
			if(!on_cmdEdit()) return;
		}
	}
	

	if (m_nMode==MODE_INSERT || m_nMode==MODE_EDIT)
	{
		switch(nEntityID)
		{
		case ENTITY_BUS_CM_EMAIL:
			{
				//add first name/last name if not already set
				QString strEmail=lstData.getDataRef(0,"BCME_ADDRESS").toString();
				QString strFirstName,strLastName;
				if(EmailHelperCore::ExtractNamesFromEmail(strEmail,strFirstName,strLastName))
				{
					if (m_lstData.getDataRef(0,"BCNT_FIRSTNAME").isNull())
						m_lstData.setData(0,"BCNT_FIRSTNAME",strFirstName);
					if (m_lstData.getDataRef(0,"BCNT_LASTNAME").isNull())
						m_lstData.setData(0,"BCNT_LASTNAME",strLastName);
					m_GuiFieldManagerMain->RefreshDisplay();
				}

				lstData.setColValue(lstData.getColumnIdx("BCME_ID"),0);
				lstData.setColValue(lstData.getColumnIdx("BCME_IS_DEFAULT"),0);
				lstData.setColValue(lstData.getColumnIdx("BCME_CONTACT_ID"),m_nEntityRecordID);
				m_lstEmails.merge(lstData);
				//def. flag:
				if (m_lstEmails.find("BCME_IS_DEFAULT",1,true)==-1)
				{
					m_lstEmails.setData(0,"BCME_IS_DEFAULT",1);
				}
				ui.tableEmail->RefreshDisplay();
				ui.tabContactGroups->setCurrentIndex(TAB_DETAIL_EMAIL);
			}
			break;
		case ENTITY_BUS_CM_PHONE:
			{
				lstData.setColValue(lstData.getColumnIdx("BCMP_ID"),0);
				lstData.setColValue(lstData.getColumnIdx("BCMP_IS_DEFAULT"),0);
				lstData.setColValue(lstData.getColumnIdx("BCMP_CONTACT_ID"),m_nEntityRecordID);
				m_lstPhones.merge(lstData);
				//def. flag:
				if (m_lstPhones.find("BCMP_IS_DEFAULT",1,true)==-1)
				{
					m_lstPhones.setData(0,"BCMP_IS_DEFAULT",1);
				}
				ui.tablePhone->RefreshDisplay();
				ui.tabContactGroups->setCurrentIndex(TAB_DETAIL_PHONE);
			}
			break;
		case ENTITY_BUS_CM_INTERNET:
			{
				lstData.setColValue(lstData.getColumnIdx("BCMI_ID"),0);
				lstData.setColValue(lstData.getColumnIdx("BCMI_IS_DEFAULT"),0);
				lstData.setColValue(lstData.getColumnIdx("BCMI_CONTACT_ID"),m_nEntityRecordID);
				m_lstInternet.merge(lstData);
				//def. flag:
				if (m_lstInternet.find("BCMI_IS_DEFAULT",1,true)==-1)
				{
					m_lstInternet.setData(0,"BCMI_IS_DEFAULT",1);
				}
				ui.tableInternet->RefreshDisplay();
				ui.tabContactGroups->setCurrentIndex(TAB_DETAIL_INTERNET);
			}
			break;
		case ENTITY_BUS_CM_ADDRESS:
			{
				//only one pop-up, only format address give:
				if (lstData.getRowCount()>0)
					ui.tableAddress->AddRow(lstData.getDataRef(0,"BCMA_FORMATEDADDRESS").toString());

			}
		    break;

		case ENTITY_BUS_BIG_PICTURE: //if main pic empty set it
			{
				//if big pic =0, add to contact picture
				if (m_lstData.getDataRef(0,"BPIC_PICTURE").toByteArray().size()==0)
				{
					//preview:
					
					QPixmap pix;
					QByteArray picBig=lstData.getDataRef(0,"BPIC_PICTURE").toByteArray();
					ui.picture->SetPictureFromBinaryData(picBig);
					//ui.picture->RefreshDisplay();
					//


					
				}
				else //if big pic !0, add to picture collection:
				{

					m_lstPictures.addRow();
					int nRow=m_lstPictures.getRowCount()-1;
					//def. flag:
					if (m_lstPictures.find("BCMPC_IS_DEFAULT",1,true)==-1)
					{
						m_lstPictures.setData(0,"BCMPC_IS_DEFAULT",1);
					}
					QPixmap pix;
					QByteArray picBig=lstData.getDataRef(0,"BPIC_PICTURE").toByteArray();
					pix.loadFromData(picBig);
					PictureHelper::ResizePixMapToPreview(pix);
					QByteArray picPreview;
					PictureHelper::ConvertPixMapToByteArray(picPreview,pix,"PNG");
					m_lstPictures.setData(nRow,"BCMPC_PICTURE",picPreview);
					m_lstPictures.setData(nRow,"BPIC_PICTURE",picBig);
					ui.tablePicture->RefreshDisplay();
					
				}

				if (ui.tabContactGroups->count()>=7)
					ui.tabContactGroups->setCurrentIndex(7);
			}
			break;
			
		}
	}
	else  //NON EDIT MODE  or INSERT
	{

		switch(nEntityID)
		{

			case ENTITY_COMM_GRID_DATA: //if grid data dropped
				{
					if (m_nEntityRecordID>0)
					{
						//get Contact ID:
						Status err;
						_SERVER_CALL(BusCommunication->AssignCommDataToContact(err,lstData,m_nEntityRecordID))
						_CHK_ERR(err);

						//reload grid:
						ui.contactGrid->ReloadData();
					}
				}
				break;
			case ENTITY_BUS_CONTACT:	//if contact->nmrx
				{
					QList<int> lstID;
					int nSize=lstData.getRowCount();
					for(int i=0;i<nSize;++i)
					{
						lstID<<lstData.getDataRef(i,"BCNT_ID").toInt();
					}
					ui.widgetCommPerson->AssignData(BUS_CM_CONTACT,lstID);
					ui.tabWidgetContact->setCurrentIndex(0);
					ui.tabWidgetComm->setCurrentIndex(1);
				}
				break;
			case ENTITY_BUS_PROJECT:	//if contact->nmrx
				{
					QList<int> lstID;
					int nSize=lstData.getRowCount();
					for(int i=0;i<nSize;++i)
					{
						lstID<<lstData.getDataRef(i,"BUSP_ID").toInt();
					}
					ui.widgetCommProject->AssignData(BUS_PROJECT,lstID);
					ui.tabWidgetContact->setCurrentIndex(0);
					ui.tabWidgetComm->setCurrentIndex(2);

				}
				break;
			case ENTITY_BUS_PERSON:	//if contact->nmrx
				{
					QList<int> lstID;
					int nSize=lstData.getRowCount();
					for(int i=0;i<nSize;++i)
					{
						lstID<<lstData.getDataRef(i,"BPER_ID").toInt();
					}
					ui.widgetCommPerson->AssignData(BUS_PERSON,lstID);
					ui.tabWidgetContact->setCurrentIndex(0);
					ui.tabWidgetComm->setCurrentIndex(1);

				}
				break;
		}
	}
}

void FUI_Contacts::on_btnExportSerialLetter_clicked()
{
	DbRecordSet lstContacts;
	ui.tableActual->GetSelectedContacts(lstContacts);
	int nCount = lstContacts.getRowCount();
	if(nCount < 1){
		QMessageBox::information(NULL, tr("Error"), tr("There are no selected rows in the list!"));
		return;
	}


	QString strFile = QFileDialog::getSaveFileName(this, tr("Save as..."),
		g_strLastDir_FileOpen, tr("Text file (*.txt)"));
	if (strFile.isEmpty())
		return;
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	QFile file(strFile);
	if(!file.open(QIODevice::WriteOnly)){
		QMessageBox::information(NULL, tr("Error"), tr("Failed to open destination file!"));
		return;
	}

	//write header
	file.write(QString("\"First_Name\"\t\"Last_Name\"\t\"Organization\"\t\"Salutation\"\t\"Address1\"\t\"Address2\"\t\"Address3\"\t\"Address4\"\t\"Address5\"\t\"Address6\"\t\"Address7\"\t\"Address8\"\t\"Address9\"\r\n").toLatin1());

	//for each grid row
	for(int i=0; i<nCount; i++)
	{
		//write first name
		file.write(QString("\"").toLatin1());
		file.write(lstContacts.getDataRef(i, "BCNT_FIRSTNAME").toString().toLatin1());
		file.write(QString("\"").toLatin1());

		file.write(QString("\t").toLatin1());

		//write last name
		file.write(QString("\"").toLatin1());
		file.write(lstContacts.getDataRef(i, "BCNT_LASTNAME").toString().toLatin1());
		file.write(QString("\"").toLatin1());

		file.write(QString("\t").toLatin1());

		//write org name
		file.write(QString("\"").toLatin1());
		file.write(lstContacts.getDataRef(i, "BCNT_ORGANIZATIONNAME").toString().toLatin1());
		file.write(QString("\"").toLatin1());

		file.write(QString("\t").toLatin1());

		//write salutation and addr fields
		DbRecordSet lstAddrs = lstContacts.getDataRef(i, "LST_ADDRESS").value<DbRecordSet>();

		//find default address field
		int nAddrCnt = lstAddrs.getRowCount();
		int nDefAddrIdx = -1;
		for(int j=0; j<nAddrCnt; j++){
			if(lstAddrs.getDataRef(j, "BCMA_IS_DEFAULT").toInt() > 0){
				nDefAddrIdx = j; break;
			}
		}

		//write salutation
		file.write(QString("\"").toLatin1());
		file.write((nDefAddrIdx >= 0)? lstAddrs.getDataRef(nDefAddrIdx, "BCMA_SALUTATION").toString().toLatin1() : "");
		file.write(QString("\"").toLatin1());

		//break addr into lines
		QStringList lstAddrTxt;
		if(nDefAddrIdx >= 0){
			QString strFmtAdd = lstAddrs.getDataRef(nDefAddrIdx, "BCMA_FORMATEDADDRESS").toString();
			strFmtAdd.replace("\r\n", "\n");
			strFmtAdd.replace("\r", "\n");
			lstAddrTxt = strFmtAdd.split("\n");
		}

		//write addr 1-9
		int nLines = lstAddrTxt.count();
		for(int k=0; k<9; k++)
		{
			file.write(QString("\t").toLatin1());

			file.write(QString("\"").toLatin1());
			file.write((k < nLines)? lstAddrTxt.at(k).toLatin1() : "");
			file.write(QString("\"").toLatin1());
		}
		
		//write new line
		file.write(QString("\r\n").toLatin1());
	}
}
			





void FUI_Contacts::on_btnDebtorCode_clicked()
{

/*
	//-----------------------Connecting-----------------------------//
	FTPConnectionSettings conn;
	conn.m_strServerIP="ftp.sokrates.hr";
	conn.m_strFTPUserName="sokrates";
	conn.m_strFTPPassword="karota10";

	FTPClient client;
	Status err;
	client.Connect(err,conn);


	//-----------------------Dir change /create-----------------------------//
	client.Dir_Change(err,"web");
	client.Dir_Create(err,"z1");

	//-----------------------Listing files/dirs-----------------------------//
	QList<QUrlInfo> lst;
	client.Dir_List(err,lst);
	int nSize=lst.size();
	for(int i=0;i<nSize;++i)
	{
		qDebug()<<lst.at(i).name();
	}

	client.Dir_Remove(err,"z1");

	//-----------------------Listing files/dirs-----------------------------//
	client.Dir_List(err,lst);
	nSize=lst.size();
	for(int i=0;i<nSize;++i)
	{
		qDebug()<<lst.at(i).name();
	}
*/
/*

	//-----------------------File put/get/rename/delete-----------------------------//
	QByteArray fileContent;
	client.File_Get(err,fileContent,"dynamic.html");
	client.File_Put(err,fileContent,"xxxx.html");
	client.File_Rename(err,"xxxx.html","1234.html");
	client.File_Remove(err,"1234.html");

	//-----------------------File put & get with progress bar-----------------------------//
	
	QSize size(300,100);
	progressDialog->setMinimumSize(size);
	progressDialog->setWindowModality(Qt::WindowModal);
	connect(&client,SIGNAL(SignalFTPOperationInProgress(qint64,qint64)),this,SLOT(updateDataTransferProgress(qint64,qint64)));
	progressDialog->setLabelText(tr("Uploading %1...").arg("D:/ApplicationServerConfig.exe"));
	progressDialog->show();

	client.File_Get(err,"ApplicationServerConfig.exe","U:/ApplicationServerConfig.exe");

	progressDialog->hide();
	*/

	//-----------------------Disconnect-----------------------------//
	//client.Disconnect(err);



	Status err;
	QString strCode;
	_SERVER_CALL(BusContact->GetMaximumDebtorCode(err,strCode))
	_CHK_ERR(err);

	bool bOK;
	int nCode=strCode.toInt(&bOK);
	if (!bOK)
	{
		nCode=0;
		//QMessageBox::warning(this,tr("Warning"),tr("Invalid Debtor Code, can not resolve next Debtor Code!"));
		//return;
	}

	if (m_lstDebtor.getRowCount()==0)
	{
		m_lstDebtor.addRow();
	}

	nCode++;
	ui.txtDebtorCode->setText(QVariant(nCode).toString());
	
}





void FUI_Contacts::onActualListChange()
{
	if (!g_objFuiManager.IsActiveWindowCurrentFUI(this))
		return;

	ui.tableActual->OnActualListRebuilt();
}


void FUI_Contacts::on_btnInternet_clicked()
{
	QString strInternet=ui.cmbInfo_Internet-> itemData(ui.cmbInfo_Internet->currentIndex()).toString();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strInternet);
}

void FUI_Contacts::on_btnAddress_clicked()
{
	QString strAddress=ui.txtInfo_Address->toPlainText();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strAddress);
}

void FUI_Contacts::on_btnEmail_clicked()
{
	QString strEmail=ui.cmbInfo_Email-> itemData(ui.cmbInfo_Email->currentIndex()).toString();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strEmail);
}
void FUI_Contacts::on_btnPhone_clicked()
{
	QString strPhone=ui.cmbInfo_Phone-> itemData(ui.cmbInfo_Phone->currentIndex()).toString();
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(strPhone);
}

void FUI_Contacts::changeEvent(QEvent * event)
{
	QWidget::changeEvent(event);

	if(event->type() == QEvent::WindowStateChange)
	{
		//now check to see if the window is being minimised
        if( isMinimized() )
		{
			if(NULL == m_pWndAvatar)
			{
				//calculate initial position for the new window
				QPoint pt = geometry().topRight();
				pt.setX(pt.x() - 169);

				m_pWndAvatar = new fui_avatar(AVATAR_CONTACT, this);
				m_pWndAvatar->move(pt);
			}

			QString strTxt = m_p_Selector->GetCalculatedName(m_nEntityRecordID);
			m_pWndAvatar->SetLabel(strTxt);
			QPixmap pix = ui.picture->GetPixmap();
			m_pWndAvatar->SetPicture(pix);
			m_pWndAvatar->SetRecord(m_nEntityRecordID);

			m_pWndAvatar->show();

			if (m_bSideBarModeView)
				g_objFuiManager.ClearActiveFUI(this);

			if(m_bHideOnMinimize)
				QTimer::singleShot(0,this,SLOT(hide()));//B.T. delay hide
			m_bHideOnMinimize = true; //reset
		}
	}
}

void FUI_Contacts::closeEvent(QCloseEvent *event)
{
	FuiBase::closeEvent(event);

	if(m_pWndAvatar){
		m_pWndAvatar->close();
		m_pWndAvatar = NULL;
	}
}




//issue: 1153, selection controller is destroyed before FUI, disable it:
void FUI_Contacts::On_SelectionControllerSideBarDestroyed()
{
	m_p_Selector=NULL;
	m_pSelectionController=NULL;
}



void FUI_Contacts::resizeEvent ( QResizeEvent * event )
{

	//avoid height to be > screen height:
	QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
	if ((desktopRec.y()+this->geometry().top()+event->size().height())>(desktopRec.y()+desktopRec.height()))
	{
		QTimer::singleShot(0,this,SLOT(OnResizeDelayed()));
		event->accept();
		return;
	}
	QWidget::resizeEvent(event);
}


void FUI_Contacts::OnResizeDelayed()
{
	//issue 1259:
	QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
	int nY=desktopRec.height()-desktopRec.y()-this->geometry().height()-2;
	if (nY<desktopRec.y())
		nY=desktopRec.y();
	move(this->geometry().x(),nY); //move

	if (this->geometry().height()>desktopRec.height()) //if >resize
	{
		resize(this->geometry().width(), desktopRec.height()-2);
	}

}


void FUI_Contacts::on_btnSerialEmail_clicked()
{
	DbRecordSet lstContacts;
	ui.tableActual->GetSelectedContacts(lstContacts);
	if (lstContacts.getRowCount()==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select contact(s) for serial email"));
		return;
	}
	QPoint pos=ui.btnSerialEmail->mapToGlobal(QPoint(0,ui.btnSerialEmail->height()));
	CommunicationActions::sendEmail_Serial(lstContacts,&pos);
}

void FUI_Contacts::OnSetFindFocusDelayed()
{
	m_pFrameContacts->SetFocusOnFind();
}

//issue 1757:
void FUI_Contacts::ClearEmptyEntries()
{
	ui.tableEmail->ClearEmptyEntries();
	ui.tablePhone->ClearEmptyEntries();
	ui.tableInternet->ClearEmptyEntries();
}

void FUI_Contacts::OnFavoriteChanged(int nMsgDetail)
{
	on_selectionChange(nMsgDetail);
}


void FUI_Contacts::DataPrepareForInsert(Status &err)
{
	DataClear();
	ContactTypeManager::AddEmptyRowToFullContactList(m_lstData);
	m_lstData.setData(0,"BCNT_ID",0);												
	m_lstData.setData(0,"BCNT_TYPE",ContactTypeManager::CM_TYPE_PERSON);			
	m_lstData.setData(0,"BCNT_SEX",ContactTypeManager::SEX_MALE);					
	m_lstData.setData(0,"BCNT_SUPPRESS_MAILING",0);									
	m_lstDebtor.clear(); //1v1
	m_lstDebtor.addRow(); 

	//for custom fields, special procedure:
	_SERVER_CALL(BusCustomFields->ReadUserCustomDataForInsert(err,BUS_CM_CONTACT,m_lstCustomFields));
}

void FUI_Contacts::DataClear()
{
	m_lstData.clear();
	m_lstPhones.clear();
	m_lstEmails.clear();
	m_lstInternet.clear();
	m_lstAddress.clear();
	m_lstPictures.clear();
	m_lstDebtor.clear();
	ui.arSelector->Invalidate();
	ui.widgetReservations->Clear();
	m_lstCustomFields.clear();
}

void FUI_Contacts::DataDefine()
{
	DbSqlTableDefinition::GetKeyData(m_nTableID,m_TableData);
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	m_lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE_SELECT));
	m_lstEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT));
	m_lstInternet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET_SELECT));
	m_lstAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT));
	m_lstPictures.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PICTURE));
	m_lstDebtor.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_DEBTOR));
	m_lstCustomFields.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS_DATA));
}

void FUI_Contacts::DataRead(Status &err,int nRecordID)
{
	//read batch:
	DbRecordSet lstNmrx1,lstNmrx2,lstJournals,lstGroups;
	DbRecordSet lstNmrx1Filter=ui.widgetCommPerson->GetFilterForRead(nRecordID);
	DbRecordSet lstNmrx2Filter=ui.widgetCommProject->GetFilterForRead(nRecordID);
	QString strName=m_p_Selector->GetCalculatedName(nRecordID);
	DbRecordSet lstUAR,lstGAR;

	_SERVER_CALL(BusContact->BatchReadContactData(err,nRecordID,m_lstData,lstNmrx1Filter,lstNmrx1,lstNmrx2Filter,lstNmrx2,ContactTypeManager::USE_SUBDATA_ALL,lstUAR,lstGAR));
	if(err.IsOK())
	{
		m_lstDataCache=m_lstData;

		if (m_lstData.getRowCount()>0) //issue 2534:
			m_lstAddressOrganizationchangePropagate=m_lstData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();

		ui.widgetReservations->Reload(err,nRecordID);
		if (!err.IsOK())
			return;

		if (m_lstData.getRowCount()>0)
		{
			lstGroups=m_lstData.getDataRef(0,"LST_GROUPS").value<DbRecordSet>();
			lstJournals=m_lstData.getDataRef(0,"LST_JOURNAL").value<DbRecordSet>();
			ContactRow2SubData();
		}
		ui.widgetCommPerson->Reload(strName,nRecordID,true,&lstNmrx1);
		ui.widgetCommProject->Reload(strName,nRecordID,true,&lstNmrx2);
		ui.tableJournal->Reload(nRecordID,true,&lstJournals);
		ui.frameGroupContacts->Clear();
		ui.frameGroupContacts->SetList(lstGroups);
		ui.arSelector->Load(nRecordID,BUS_CM_CONTACT,&lstUAR,&lstGAR);
		ui.arSelector->SetRecordDisplayName(m_p_Selector->GetCalculatedName(nRecordID));

	}
}

void FUI_Contacts::DataWrite(Status &err)
{
	DbRecordSet lstUAR,lstGAR;
	ui.arSelector->Save(m_nEntityRecordID,BUS_CM_CONTACT,&lstUAR,&lstGAR); //get ar rows for save

	SubData2ContactRow();

	//flags: by default no creditor nor journal, if FP false then ignore debtor too
	int nWriteSubDataFlag=ContactTypeManager::USE_SUBDATA_PICTURE | ContactTypeManager::USE_SUBDATA_CUSTOM_FIELDS;
	if (g_FunctionPoint.IsFPAvailable(FP_DEBTOR_DATA))
	{
		nWriteSubDataFlag = nWriteSubDataFlag | ContactTypeManager::USE_SUBDATA_DEBTOR;
	}


	bool bGoForUpdateOfAddressAfterWrite=false;

	//issue 2534: (only if edit organization)
	if (m_lstData.getRowCount()>0)
	{
		if (m_lstData.getDataRef(0,"BCNT_ID").toInt()>0 &&   m_lstData.getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION)
		{
			//compare address fields:
			DbRecordSet lstAddress=m_lstData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();

			//just compare 1st address:
			if (lstAddress.getRowCount()==1 && m_lstAddressOrganizationchangePropagate.getRowCount()==1)
			{
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_ORGANIZATIONNAME").toString()!=lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_ORGANIZATIONNAME_2").toString()!=lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME_2").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_STREET").toString()!=lstAddress.getDataRef(0,"BCMA_STREET").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_POBOX").toString()!=lstAddress.getDataRef(0,"BCMA_POBOX").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_POBOXZIP").toString()!=lstAddress.getDataRef(0,"BCMA_POBOXZIP").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_ZIP").toString()!=lstAddress.getDataRef(0,"BCMA_ZIP").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_CITY").toString()!=lstAddress.getDataRef(0,"BCMA_CITY").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_REGION").toString()!=lstAddress.getDataRef(0,"BCMA_REGION").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_COUNTRY_CODE").toString()!=lstAddress.getDataRef(0,"BCMA_COUNTRY_CODE").toString())
					bGoForUpdateOfAddressAfterWrite=true;
				if (m_lstAddressOrganizationchangePropagate.getDataRef(0,"BCMA_COUNTRY_NAME").toString()!=lstAddress.getDataRef(0,"BCMA_COUNTRY_NAME").toString())
					bGoForUpdateOfAddressAfterWrite=true;
			}
		}
	}


	_SERVER_CALL(BusContact->WriteData(err,m_lstData,nWriteSubDataFlag,m_strLockedRes,lstUAR,lstGAR));
	if (err.IsOK())
	{
		m_nEntityRecordID=m_lstData.getDataRef(0,m_TableData.m_strPrimaryKey).toInt();
		ui.widgetReservations->Write(err,m_nEntityRecordID);

		m_strLockedRes="";
		m_lstDataCache=m_lstData;
		ContactRow2SubData();
		//re-init UAR selector (if after insert)
		
		ui.arSelector->Load(m_nEntityRecordID,BUS_CM_CONTACT,&lstUAR,&lstGAR);
		ui.arSelector->SetRecordDisplayName(m_p_Selector->GetCalculatedNameFromRecord(m_lstData));

		//issue 2574:
		if (m_nMode==MODE_INSERT)
		{
			WriteContactGroupAssignmentsAfterCopy();
		}
		

		//issue 2534:
		if (bGoForUpdateOfAddressAfterWrite)
		{
			//find how many persons have same organization and have unlocked address???
			DbRecordSet lstAddressIDs;
			_SERVER_CALL(BusContact->GetAllContactsOfOrganizationByAddress(err, m_nEntityRecordID,m_lstAddressOrganizationchangePropagate,lstAddressIDs));
			if (!err.IsOK())return;

			//ask user
			int nPersonsOfOrg=lstAddressIDs.getRowCount();
			if (nPersonsOfOrg>0)
			{
				QString strOrgName=m_lstData.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString();
				int nResult=QMessageBox::question(this,tr("Warning"),QString(tr("You changed the address of %1. Do you want this change to be made automatically to %2 addresses of people in this organization?")).arg(strOrgName).arg(nPersonsOfOrg),tr("Yes"),tr("No"));
				if (nResult==0) 
				{
					//if ok, then go
					DbRecordSet lstAddress=m_lstData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
					_SERVER_CALL(BusContact->UpdateContactsOfOrganizationByAddress(err, lstAddressIDs, lstAddress));
					
				}
			}
		}

		//update after write:
		m_lstAddressOrganizationchangePropagate=m_lstData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();


	}

	//m_lstData.Dump();

}

void FUI_Contacts::DataPrepareForCopy(Status &err)
{
	DbRecordSet lstGroups;
	if(!ClientContactManager::SelectGroupsForCopyContact(lstGroups))
	{
		err.setError(0,tr("User aborted!"));
		return;
	}



	//read big picture for main record, reset id's
	ClientContactManager::LoadBigPictureList(err,m_lstData,"BCNT_BIG_PICTURE_ID");
	if (!err.IsOK()) return;

	m_lstData.setColValue(m_lstData.getColumnIdx("BCNT_ID"),0);
	m_lstData.setColValue(m_lstData.getColumnIdx("BCNT_BIG_PICTURE_ID"),0);

	if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_PICTURE).toInt(),true)>=0)
	{
		//read big pictures for pic records, reset id's
		ClientContactManager::LoadBigPictureList(err,m_lstPictures,"BCMPC_BIG_PICTURE_ID");
		if (!err.IsOK()) return;
		m_lstPictures.setColValue(m_lstData.getColumnIdx("BCMPC_ID"),0);
		m_lstPictures.setColValue(m_lstData.getColumnIdx("BCMPC_BIG_PICTURE_ID"),0);
	}
	else
		m_lstPictures.clear();

	//issue 2574: copy contact-group assignments
	if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_CONTACT_GROUP_ASSIGN).toInt(),true)>=0)
	{
		ui.frameGroupContacts->GetList(m_lstGroupContactAssignmentsForCopy);
		m_lstGroupContactAssignmentsForCopy.setColValue("BGCN_CONTACT_ID",0);
	}
	ui.frameGroupContacts->Clear();

	m_lstPhones.setColValue(m_lstPhones.getColumnIdx("BCMP_ID"),0);
	m_lstEmails.setColValue(m_lstEmails.getColumnIdx("BCME_ID"),0);
	m_lstInternet.setColValue(m_lstInternet.getColumnIdx("BCMI_ID"),0);
	m_lstDebtor.setColValue(m_lstDebtor.getColumnIdx("BCMD_ID"),0);

	ui.tableCustomFields->ResetRecordIDs();

	//reset all types sublists:
	int nSize=m_lstAddress.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstType=m_lstAddress.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
		lstType.setColValue(lstType.getColumnIdx("BCMAT_ID"),0);
		m_lstAddress.setData(i,"LST_TYPES",lstType);
	}
	m_lstAddress.setColValue(m_lstAddress.getColumnIdx("BCMA_ID"),0);

	if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_ADDRESS).toInt(),true)<0)
		m_lstAddress.clear();
	if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_PHONE).toInt(),true)<0)
		m_lstPhones.clear();
	if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_EMAIL).toInt(),true)<0)
		m_lstEmails.clear();
	if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_INTERNET).toInt(),true)<0)
		m_lstInternet.clear();
	if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_CONTACT_CUSTOM_FIELDS).toInt(),true)<0)
		m_lstCustomFields.clear();

	//if (lstGroups.find(0,ContactTypeManager::GROUP_CREDITOR,true)<0)
	//	m_lstPictures.clear();
	if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_DEBTOR).toInt(),true)<0)
		m_lstDebtor.clear();

	ui.arSelector->Invalidate();

	DbRecordSet data;
	ui.widgetReservations->GetData(data);
	data.setColValue("BCRS_ID",QVariant(QVariant::Int));
	ui.widgetReservations->Reload(err,-1,&data);
}

void FUI_Contacts::DataUnlock(Status &err)
{
	FuiBase::DataUnlock(err);
	if (err.IsOK())
		ContactRow2SubData();
	
	ui.arSelector->CancelChanges();
	ui.arSelector->SetRecordDisplayName(m_p_Selector->GetCalculatedName(m_nEntityRecordID));
	if (m_nEntityRecordID>0)
		ui.widgetReservations->Reload(err,m_nEntityRecordID); //instead of cache just reload widget...
	else
		ui.widgetReservations->Clear();
}

void FUI_Contacts::SubData2ContactRow()
{
	m_lstData.setData(0,"LST_PHONE",m_lstPhones);
	m_lstData.setData(0,"LST_ADDRESS",m_lstAddress);
	m_lstData.setData(0,"LST_EMAIL",m_lstEmails);
	m_lstData.setData(0,"LST_INTERNET",m_lstInternet);
	m_lstData.setData(0,"LST_PICTURES",m_lstPictures);

	//ui.tableCustomFields->StoreCustomWidgetChanges(); //ensure that all changes are stored properly in the record
	m_lstData.setData(0,"LST_CUSTOM_FIELDS",m_lstCustomFields);

	//if customer code and debtor code is null and insert mode, then ignore data
	if (m_lstDebtor.getRowCount()>0)
	{
		if (m_lstDebtor.getDataRef(0,"BCMD_DEBTORCODE").toString().isEmpty() && m_lstDebtor.getDataRef(0,"BCMD_CUSTOMERCODE").toString().isEmpty() && (m_nMode==MODE_INSERT || m_nMode==MODE_EDIT))
		{
			m_lstDebtor.clear();
		}
	}

	m_lstData.setData(0,"LST_DEBTOR",m_lstDebtor);
}

void FUI_Contacts::ContactRow2SubData()
{
	m_lstPhones=m_lstData.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
	m_lstEmails=m_lstData.getDataRef(0,"LST_EMAIL").value<DbRecordSet>();
	m_lstInternet=m_lstData.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
	m_lstCustomFields=m_lstData.getDataRef(0,"LST_CUSTOM_FIELDS").value<DbRecordSet>();
	m_lstAddress=m_lstData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
	m_lstPictures=m_lstData.getDataRef(0,"LST_PICTURES").value<DbRecordSet>();
	m_lstDebtor=m_lstData.getDataRef(0,"LST_DEBTOR").value<DbRecordSet>();
	if (m_lstDebtor.getRowCount()==0) //B.T. 2013.03.11: this is special way to insert row, coz this list is mapped 1:1 on contact record, if empty when saving then discard record
		m_lstDebtor.addRow();
}



void FUI_Contacts::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);

	//set phonets:
	ContactTypeManager::SetPhonets(m_lstData);

	//check duplicates:
	if (m_nMode==MODE_INSERT || m_nMode==MODE_EDIT)
	{
		DbRecordSet lstData;
		lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_PHONETS));
		lstData.merge(m_lstData);
		DbRecordSet ret_data;
		_SERVER_CALL(BusContact->FindDuplicates(err,lstData,ret_data))
		if (!err.IsOK()) return;
		if (ret_data.getRowCount()>0)
		{
			QString strMsg=tr("Contact already exists in database:")+" "+ret_data.getDataRef(0,"BCNT_NAME").toString()+". Continue with save?";
			int nResult=QMessageBox::question(NULL,tr("Warning"),strMsg,tr("Yes"),tr("No"));
			if (nResult!=0) 
			{
				err.setError(1,tr("Operation Aborted"));
				return;
			}
		}
		ClearEmptyEntries(); //issue 1757
	}

	//check subdata:
	ui.tablePhone->CheckDataBeforeWrite(err);
	if (!err.IsOK()) return;
	ui.tableEmail->CheckDataBeforeWrite(err);
	if (!err.IsOK()) return;
	ui.tableInternet->CheckDataBeforeWrite(err);
	if (!err.IsOK()) return;
	ui.tablePicture->CheckDataBeforeWrite(err);
	if (!err.IsOK()) return;


	//if org, clear person data
	if (m_lstData.getRowCount()>0)
	{
		if (m_lstData.getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION)
		{
			QVariant empty(QVariant::String);
			m_lstData.setData(0,"BCNT_FIRSTNAME",empty);
			m_lstData.setData(0,"BCNT_LASTNAME",empty);
			m_lstData.setData(0,"BCNT_MIDDLENAME",empty);
		}
	}

	ui.widgetReservations->DataCheckBeforeWrite(err);
}


void FUI_Contacts::OnBtnCalendar_Click()
{
	if (m_nEntityRecordID<=0)
		return;

	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(QDate::currentDate(),from, to);

	QString strName=m_p_Selector->GetCalculatedName(m_nEntityRecordID);

	DbRecordSet lstContacts;
	lstContacts.addColumn(QVariant::Int,"BCNT_ID");
	lstContacts.addColumn(QVariant::String,"BCNT_NAME");
	lstContacts.addRow();
	lstContacts.setData(0,0,m_nEntityRecordID);
	lstContacts.setData(0,1,strName);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		//lstContacts.Dump();
		pFui->Initialize(from,to,NULL,NULL,&lstContacts);
	}


}

void FUI_Contacts::SwitchSelectorToFavoriteTab()
{
	ui.frameContactAndFavs->SetCurrentTab(1);
}

void FUI_Contacts::FlipSelectorToFavoriteTabOrList()
{
	if (ui.frameContactAndFavs->GetCurrentTab()==1)
		ui.frameContactAndFavs->SetCurrentTab(0);
	else
		ui.frameContactAndFavs->SetCurrentTab(1);
}

void FUI_Contacts::FlipShowQuickInfo()
{
	ui.groupBoxInfo->blockSignals(true);
	ui.groupBoxInfo->setChecked(!ui.groupBoxInfo->isChecked());
	ui.groupBoxInfo->blockSignals(false);
	on_groupBoxInfo_clicked();
}

void FUI_Contacts::OnTabWidgetContact_currentChanged(int nCurrentTab)
{
	if (nCurrentTab==TAB_MAIN_COMM)
	{
		if (m_nMode==MODE_READ)
			ui.btnButtonBar->SetMode(MODE_EMPTY);
	}
	else
	{
		if (m_nMode==MODE_READ)
			ui.btnButtonBar->SetMode(MODE_READ);
	}
		//ui.btnButtonBar->setEnabled(true);

}

//issue 2246: create shortcuts:
void FUI_Contacts::keyPressEvent ( QKeyEvent * event ) 
{
	if(!g_pClientManager->IsLogged())return;

	switch(event->key())
	{
	case Qt::Key_F2: 
		{
			event->accept();
			if (event->modifiers() & Qt::AltModifier) //ALT-F2: Switches "Overview" on an off
			{
				FlipShowQuickInfo();
			}
			return;
		}
		break;
	}

	FuiBase::keyPressEvent(event);
}


void FUI_Contacts::OnGridRequestEditModeAfterDblClick()
{
	if(m_nMode == MODE_READ)
	{
		m_bGridDialogRequestedEditMode=true;
		on_cmdEdit();
	}
}

void FUI_Contacts::OnGridRequestCloseEditModeAfterDblClick()
{

	if(m_nMode == MODE_EDIT && m_bGridDialogRequestedEditMode)
	{
		m_bGridDialogRequestedEditMode=false;
		on_cmdOK();
	}

}