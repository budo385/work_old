#include "table_contactemail.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "fui_collection/fui_collection/fui_contacttype.h"
#include "fui_collection/fui_collection/fui_contacts.h"
#include "menuitems.h"

#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "common/common/cliententitycache.h"
extern ClientEntityCache				g_ClientCache;				//global client cache


Table_ContactEmail::Table_ContactEmail(QWidget * parent)
:UniversalTableWidgetEx(parent),m_toolbar(NULL),m_pActMakeAct(NULL),m_pActOpenType(NULL)
{
}

void Table_ContactEmail::Initialize(DbRecordSet *plstData,toolbar_edit * pToolBar, QWidget *pContactFUI)
{
	BuildToolBar(pToolBar);
	m_pContactFUI=pContactFUI;

	m_TypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_EMAIL_ADDRESS);
	m_TypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	m_TypeHandler.ReloadData();

	//setup columns
	DbRecordSet columns;
	AddColumnToSetup(columns,"BCME_ADDRESS",tr("Email"),160,true, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCME_NAME",tr("Name"),100,true, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCME_DESCRIPTION",tr("Description"),260,true, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCME_SYSTEM_LIST",tr("Type"),100,true, "",COL_TYPE_CUSTOM_WIDGET);

	//set data source for table, enable drag
	UniversalTableWidgetEx::Initialize(plstData,&columns);
	EnableDrag(true,ENTITY_BUS_CM_EMAIL);
	CreateContextMenu();

	//store idx columns for faster access
	m_nTypeColIdx=m_plstData->getColumnIdx("BCME_TYPE_ID");
	m_nDefaultColIdx=m_plstData->getColumnIdx("BCME_IS_DEFAULT");

	connect(&m_TypeHandler,SIGNAL(SignalReloadCombo()),this,SLOT(OnComboDataReload()));
	connect(this,SIGNAL(SignalRowInserted(int)),this,SLOT(OnRowInserted(int)));
	connect(this,SIGNAL(SignalRowsDeleted()),this,SLOT(OnDeleted()));
}



//dummy: if inserted: blue icon, else red
void Table_ContactEmail::GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip)
{
	//if Default then show checked icon:
	if(m_plstData->getDataRef(nRow,m_nDefaultColIdx).toInt()!=0)
	{
		RowIcon.addFile(":checked.png");
		strStatusTip=tr("Default Type");
	}
	else
		RowIcon=QIcon(); //empty icon
}


//create custom cntx menu at end of existing one:
void Table_ContactEmail::CreateContextMenu()
{
	QList<QAction*> lstActions;
	CreateDefaultContextMenuActions(lstActions);

	//Add separator
	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.prepend(SeparatorAct);
	//----------
	m_pActMakeAct = new QAction(tr("Send Email"), this);
	m_pActMakeAct->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(m_pActMakeAct, SIGNAL(triggered()), this, SLOT(OnMakeAction()));
	lstActions.prepend(m_pActMakeAct);
	//----------
	m_pActOpenType = new QAction(tr("Create/Edit Email Type"), this);
	m_pActOpenType->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(m_pActOpenType, SIGNAL(triggered()), this, SLOT(OnOpenTypeFUI()));
	lstActions.prepend(m_pActOpenType);

	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.prepend(SeparatorAct);
	//Insert new row:
	QAction* pAction = new QAction(tr("&Set as Default"), this);
	pAction->setIcon(QIcon(":checked.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(SetDefault()));
	lstActions.prepend(pAction);
	
	SetContextMenuActions(lstActions);
}

//invoked by cnt menu, set default flag on selected row
void Table_ContactEmail::SetDefault()
{
	int nRow=m_plstData->getSelectedRow();
	if(nRow!=-1)
	{
		//set default flag:
		m_plstData->setColValue(m_nDefaultColIdx,0);
		m_plstData->setData(nRow,m_nDefaultColIdx,1);
		RefreshDisplay();
	}
}

void Table_ContactEmail::BuildToolBar(toolbar_edit * pToolBar)
{
	m_toolbar=pToolBar;
	pToolBar->GetBtnAdd()->setText(tr("Add New Email"));
	connect(pToolBar->GetBtnAdd(), SIGNAL(clicked()), this, SLOT(InsertRow()));
	pToolBar->GetBtnSelect()->setVisible(false);
	pToolBar->GetBtnModify()->setVisible(false);
	connect(pToolBar->GetBtnClear(), SIGNAL(clicked()), this, SLOT(DeleteSelection()));
}

void Table_ContactEmail::SetEditMode(bool bEdit)
{
	UniversalTableWidgetEx::SetEditMode(bEdit);
	if(m_toolbar)m_toolbar->setEnabled(bEdit);

	//always on:
	if (m_pActMakeAct)m_pActMakeAct->setEnabled(true);
	if (m_pActOpenType)m_pActOpenType->setEnabled(true);

}

void Table_ContactEmail::CheckDataBeforeWrite(Status &Ret_pStatus)
{
	int nTestCol=m_plstData->getColumnIdx("BCME_ADDRESS");
	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if(m_plstData->getDataRef(i,nTestCol).toString().isEmpty())
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Email address is mandatory field!"));	
			return;	
		}
	}
}

void Table_ContactEmail::RefreshDefaultFlag()
{
	int nCountDefs=m_plstData->countColValue(m_nDefaultColIdx,1);
	if(nCountDefs==0 && m_plstData->getRowCount()>0)				//set first to be default if default is deleted!
		m_plstData->setData(0,m_nDefaultColIdx,1);
	else if (nCountDefs>1) //if default copied
	{
		int nRowFirstDef=m_plstData->find(m_nDefaultColIdx,1,true);
		if (nRowFirstDef>=0)
		{
			m_plstData->setColValue(m_nDefaultColIdx,0);
			m_plstData->setData(nRowFirstDef,m_nDefaultColIdx,1);
		}
	}
}

void Table_ContactEmail::Data2CustomWidget(int nRow, int nCol)
{
	QComboBox *pComboType = dynamic_cast<QComboBox *>(cellWidget(nRow,nCol));
	if (!pComboType)
	{
		pComboType = new QComboBox;
		pComboType->setEditable(false);
		pComboType->setProperty("combo_row",nRow); //to identify sender
		connect(pComboType,SIGNAL(currentIndexChanged(int)),this,SLOT(OnComboIndexChanged(int)));  //connect on change signals:
		m_TypeHandler.ReloadCombo(pComboType);
		pComboType->setEnabled(m_bEdit);
		setCellWidget(nRow,nCol,pComboType);
	}
	m_TypeHandler.SetComboSelection(pComboType,m_plstData->getDataRef(nRow,m_nTypeColIdx));
}

void Table_ContactEmail::OnComboIndexChanged(int nIndex)
{
	QComboBox *pComboType = dynamic_cast<QComboBox *>(sender());
	if (pComboType)
	{
		int nRow=pComboType->property("combo_row").toInt();
		Q_ASSERT(nRow>=0 && nRow <m_plstData->getRowCount()); //row must be inside data
		m_plstData->setData(nRow,"BCME_TYPE_ID",pComboType->itemData(pComboType->currentIndex()));
		OnCustomWidgetCellClicked(nRow,0);
	}
}
//when data is changed from cache or new type added: reload all combo's
void Table_ContactEmail::OnComboDataReload()
{
	if (!m_plstData)
		return;

	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		QComboBox *pComboType = dynamic_cast<QComboBox *>(cellWidget(i,0)); //col type is first
		if (pComboType)
		{
			m_TypeHandler.ReloadCombo(pComboType);
			m_TypeHandler.SetComboSelection(pComboType,m_plstData->getDataRef(i,m_nTypeColIdx));
		}
	}
}

void Table_ContactEmail::OnRowInserted(int nRow)
{
	m_plstData->setData(nRow,"BCME_ID",0);//reset ID to 0, if copy
	if(m_plstData->getDataRef(nRow,"BCME_TYPE_ID").isNull())	//only if empty
		m_plstData->setData(nRow,m_nTypeColIdx,ContactTypeManager::GetDefaultTypeFromCache(ContactTypeManager::TYPE_EMAIL_ADDRESS,g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES)));	//reset to default

	RefreshDefaultFlag();
}

void Table_ContactEmail::OnDeleted()
{
	RefreshDefaultFlag();
}

QString Table_ContactEmail::GetDefaultEntry()
{
	int nRow=m_plstData->find(m_nDefaultColIdx,1,true);
	if (nRow!=-1)
		return m_plstData->getDataRef(nRow,"BCME_ADDRESS").toString();
	return "";
}

void Table_ContactEmail::ClearEmptyEntries()
{
	m_plstData->clearSelection();
	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if(m_plstData->getDataRef(i,"BCME_ADDRESS").toString().isEmpty())
			m_plstData->selectRow(i);
	}
	DeleteSelection();
}

void Table_ContactEmail::OnMakeAction()
{
	//determine row:
	int nRow=m_plstData->getSelectedRow();
	if(nRow==-1 || m_plstData->getSelectedCount()>1)
		return;

	//
	QString strEmail=m_plstData->getDataRef(nRow,"BCME_ADDRESS").toString();

	dynamic_cast<FUI_Contacts*>(m_pContactFUI)->m_p_CeMenu->SetDefaultMail(strEmail);
	dynamic_cast<FUI_Contacts*>(m_pContactFUI)->m_p_CeMenu->OnSendEmail();

}
void Table_ContactEmail::OnOpenTypeFUI()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CONTACT_TYPES,true,false,FuiBase::MODE_READ,-1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_ContactType* pFuiW=dynamic_cast<FUI_ContactType*>(pFUI);
	if (pFuiW)
	{
		pFuiW->on_selectionChange(ContactTypeManager::TYPE_EMAIL_ADDRESS);
		pFuiW->on_cmdEdit();
	}
	pFUI->show();
}
