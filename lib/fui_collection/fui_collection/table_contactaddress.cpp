#include "table_contactaddress.h"
#include "os_specific/os_specific/mime_types.h"
#include "dlg_contactaddress.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "trans/trans/xmlutil.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/countries.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

Table_ContactAddress::Table_ContactAddress(QWidget *parent)
	: UniversalTableWidgetEx(parent),m_toolbar(NULL)
{
}

//plstData defined as TVIEW_BUS_CM_ADDRESS + BCMT_TYPE_NAME + LST_TYPES(TVIEW_BUS_CM_ADDRESS_SELECT)
void Table_ContactAddress::Initialize(DbRecordSet *plstData,DbRecordSet *pContactRecord,QTextEdit *pTxtAddress,Table_ContactAddressTypes *tableTypes,toolbar_edit *pToolBar,QCheckBox *syncPers,QCheckBox *snycOrg)
{
	BuildToolBar(pToolBar);

	m_syncPers=syncPers;
	m_syncOrg=snycOrg;
	m_pTxtAddress=pTxtAddress;
	m_pTableTypes=tableTypes;
	m_plstContact=pContactRecord;

	//sync widgetS always in non edit mode:
	m_pTxtAddress->setReadOnly(true);
	m_pTableTypes->Initialize();
	m_pTableTypes->SetEditMode(false);

	//set data source for table, enable drop
	
	DbRecordSet columns;
	AddColumnToSetup(columns,"BCMA_ORGANIZATIONNAME",tr("Organization"),120,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_FIRSTNAME",tr("First Name"),100,false, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMA_MIDDLENAME",tr("Middle Name"),100,false, "",COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"BCMA_LASTNAME",tr("Last Name"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_TITLE",tr("Title"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_STREET",tr("Street"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_ZIP",tr("ZIP Code"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_CITY",tr("City"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_COUNTRY_NAME",tr("Country"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_REGION",tr("Region"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_POBOX",tr("PO BOX"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_POBOXZIP",tr("PO BOX ZIP"),100,false, "",COL_TYPE_TEXT);
	AddColumnToSetup(columns,"BCMA_OFFICECODE",tr("Office Code"),100,false, "",COL_TYPE_TEXT);
	UniversalTableWidgetEx::Initialize(plstData,&columns,true);
	EnableDrag(true,ENTITY_BUS_CM_ADDRESS);

	m_nDefaultColIdx=m_plstData->getColumnIdx("BCMA_IS_DEFAULT");
	m_nAdrIDColIdx=m_plstData->getColumnIdx("BCMA_ID");
	m_nAdrTxtAddressIdx=m_plstData->getColumnIdx("BCMA_FORMATEDADDRESS");

	connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
	connect(this,SIGNAL(SignalSelectionChanged()),this,SLOT(OnSelectionChanged()));
	connect(this,SIGNAL(SignalRowsDeleted()),this,SLOT(OnDeleted()));
	connect(this,SIGNAL(SignalRowInserted(int)),this,SLOT(OnRowInserted(int)));
	CreateContextMenu();
}


void Table_ContactAddress::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	UniversalTableWidgetEx::RefreshDisplay(nRow,bApplyLastSortModel);
	OnSelectionChanged();
}

//dummy: if inserted: blue icon, else red
void Table_ContactAddress::GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip)
{
	//if Default then show checked icon:
	if(m_plstData->getDataRef(nRow,m_nDefaultColIdx).toInt()!=0)
	{
		RowIcon.addFile(":checked.png");
		strStatusTip=tr("Default Type");
	}
	else
		RowIcon=QIcon(); //empty icon
}

//create custom cntx menu at end of existing one:
void Table_ContactAddress::CreateContextMenu()
{
	QList<QAction*> lstActions;
	CreateDefaultContextMenuActions(lstActions);
	//Add separator
	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.prepend(SeparatorAct);
	//set as def
	QAction *pAction = new QAction(tr("&Set as Default"), this);
	pAction->setIcon(QIcon(":checked.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(SetDefault()));
	lstActions.prepend(pAction);
	//format
	pAction = new QAction(tr("Open In New Window"), this);
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(OpenDetailsFromContextMenu()));
	lstActions.prepend(pAction);

	SetContextMenuActions(lstActions);
}

//invoked by cnt menu, set default flag on selected row
void Table_ContactAddress::SetDefault()
{
	int nRow=m_plstData->getSelectedRow();
	if(nRow!=-1)
	{
		//set default flag:
		m_plstData->setColValue(m_nDefaultColIdx,0);
		m_plstData->setData(nRow,m_nDefaultColIdx,1);
		RefreshDisplay();
	}
}

//opens dialog with details & big picture
void Table_ContactAddress::OpenDetails(int nRow)
{
	//open details:
	Dlg_ContactAddress dlg;
	bool bSync=false;
	if (m_plstContact->getRowCount()>0)
	{
		if (m_plstContact->getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_PERSON && m_syncPers->isChecked())
			bSync=true;

		if (m_plstContact->getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION && m_syncOrg->isChecked())
			bSync=true;

		//issue 945
		if (m_plstContact->getDataRef(0,"BCNT_FIRSTNAME").toString().isEmpty() && m_plstContact->getDataRef(0,"BCNT_LASTNAME").toString().isEmpty() && m_plstContact->getDataRef(0,"BCNT_ORGANIZATIONNAME").toString().isEmpty() && m_plstContact->getDataRef(0,"BCNT_LOCATION").toString().isEmpty())
		{
			bSync=true;
		}
	}
	DbRecordSet row = m_plstData->getRow(nRow);
	dlg.SetRow(row,m_bEdit,bSync);
	int nRes=dlg.exec();
	if(nRes!=0 && m_bEdit)
	{
		DbRecordSet rowAddr, lstTypes;
		bool bSyncNames=false;
		dlg.GetRow(rowAddr,bSyncNames);
		m_plstData->assignRow(nRow,rowAddr);						//set new data back

		if(bSyncNames)	//if sync names, change contact name, org...
		{
			m_plstContact->setData(0,"BCNT_FIRSTNAME",rowAddr.getDataRef(0,"BCMA_FIRSTNAME"));
			m_plstContact->setData(0,"BCNT_LASTNAME",rowAddr.getDataRef(0,"BCMA_LASTNAME"));
			m_plstContact->setData(0,"BCNT_MIDDLENAME",rowAddr.getDataRef(0,"BCMA_MIDDLENAME"));
			m_plstContact->setData(0,"BCNT_ORGANIZATIONNAME",rowAddr.getDataRef(0,"BCMA_ORGANIZATIONNAME"));
			m_plstContact->setData(0,"BCNT_ORGANIZATIONNAME_2",rowAddr.getDataRef(0,"BCMA_ORGANIZATIONNAME_2"));
			emit ContactDataChanged();
		}
		RefreshDisplay();
	}
	else
	{
		//if (m_bEdit && m_plstData->getRow(nRow).getDataRef(0,"BCMA_ID").toInt()==0)
		//{
		//	DeleteSelection();
		//}
	}

}

//public, with address, on insert row, insert into datasource, open details
void Table_ContactAddress::AddRow(QString strTextAddressFormat)
{
	UniversalTableWidgetEx::InsertRow();
	int nRowSize=m_plstData->getRowCount();
	m_plstData->setData(nRowSize-1,"BCMA_FORMATEDADDRESS",strTextAddressFormat);
	OpenDetails(nRowSize-1);
}
//cntx menu: on insert row, insert into datasource, open details
void Table_ContactAddress::InsertRow()
{
	UniversalTableWidgetEx::InsertRow();
	OpenDetails(m_plstData->getRowCount()-1);
}
//double click move focus, and opens detail dialog
void Table_ContactAddress::OnCellDoubleClicked(int nRow,int nCol)
{
	emit SignalDblClick();

	m_plstData->clearSelection();
	m_plstData->selectRow(nRow);
	RefreshDisplay();
	OpenDetails(nRow);
	
	emit SignalDblClickOver();
}
//open in new window
void Table_ContactAddress::OpenDetailsFromContextMenu()
{
	int nRowSelected=m_plstData->getSelectedRow();
	if (nRowSelected>=0)
	{
		OpenDetails(nRowSelected);
	}
}

void Table_ContactAddress::BuildToolBar(toolbar_edit * pToolBar)
{
	m_toolbar=pToolBar;
	pToolBar->GetBtnAdd()->setText(tr("Add New Address"));
	connect(pToolBar->GetBtnAdd(), SIGNAL(clicked()), this, SLOT(InsertRow()));
	connect(pToolBar->GetBtnSelect(), SIGNAL(clicked()), this, SLOT(OpenDetailsFromContextMenu()));
	connect(pToolBar->GetBtnModify(), SIGNAL(clicked()), this, SLOT(OpenDetailsFromContextMenu()));
	connect(pToolBar->GetBtnClear(), SIGNAL(clicked()), this, SLOT(DeleteSelection()));

}

void Table_ContactAddress::SetEditMode(bool bEdit)
{
	UniversalTableWidgetEx::SetEditMode(bEdit);
	if(m_toolbar)m_toolbar->setEnabled(bEdit);
}


//pack as last column: LST_TYPES:
//when drag: send selected items + type
QMimeData * Table_ContactAddress::mimeData( const QList<QTableWidgetItem *> items ) const
{
	QMimeData *mimeData=QTableWidget::mimeData(items);
	if (mimeData)
	{
		DbRecordSet lstSelectedItems=m_plstData->getSelectedRecordSet();
		if (lstSelectedItems.getRowCount()>0)
		{
			QByteArray byteListData=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstSelectedItems);
			mimeData->setData(SOKRATES_MIME_LIST,byteListData);
			mimeData->setData(SOKRATES_MIME_DROP_TYPE,QVariant(ENTITY_BUS_CM_ADDRESS).toString().toLatin1()); //pass unique type
		}
	}
	return mimeData;
}

void Table_ContactAddress::OnRowInserted(int nRow)
{
	m_plstData->setData(nRow,"BCMA_ID",0);//reset ID to 0, if copy
	m_plstData->setData(nRow,"BCMA_FIRSTNAME",m_plstContact->getDataRef(0,"BCNT_FIRSTNAME"));
	m_plstData->setData(nRow,"BCMA_LASTNAME",m_plstContact->getDataRef(0,"BCNT_LASTNAME"));
	m_plstData->setData(nRow,"BCMA_MIDDLENAME",m_plstContact->getDataRef(0,"BCNT_MIDDLENAME"));
	m_plstData->setData(nRow,"BCMA_ORGANIZATIONNAME",m_plstContact->getDataRef(0,"BCNT_ORGANIZATIONNAME"));
	m_plstData->setData(nRow,"BCMA_ORGANIZATIONNAME_2",m_plstContact->getDataRef(0,"BCNT_ORGANIZATIONNAME_2"));
	QString strLang=ClientContactManager::GetTypeName(ClientContactManager::GetDefaultType(ContactTypeManager::TYPE_LANGUAGE).toInt());
	m_plstData->setData(nRow,"BCMA_LANGUGAGE_CODE",strLang);
	QString strCountry=g_pSettings->GetPersonSetting(CURRENT_COUNTRY_ISO_CODE).toString();
	m_plstData->setData(nRow,"BCMA_COUNTRY_CODE",strCountry);	
	RefreshDefaultFlag();
}

void Table_ContactAddress::RefreshDefaultFlag()
{
	int nCountDefs=m_plstData->countColValue(m_nDefaultColIdx,1);
	if(nCountDefs==0 && m_plstData->getRowCount()>0)				//set first to be default if default is deleted!
		m_plstData->setData(0,m_nDefaultColIdx,1);
	else if (nCountDefs>1) //if default copied
	{
		int nRowFirstDef=m_plstData->find(m_nDefaultColIdx,1,true);
		if (nRowFirstDef>=0)
		{
			m_plstData->setColValue(m_nDefaultColIdx,0);
			m_plstData->setData(nRowFirstDef,m_nDefaultColIdx,1);
		}
	}
}

void Table_ContactAddress::OnDeleted()
{
	RefreshDefaultFlag();
}

//table changed state test it
void Table_ContactAddress::OnSelectionChanged()
{
	//refresh entry if one row is selected
	if(m_plstData->getSelectedCount()==1)
	{
		int nCurrentRow=m_plstData->getSelectedRow();
		m_pTxtAddress->setPlainText(m_plstData->getDataRef(nCurrentRow,m_nAdrTxtAddressIdx).toString());
		m_pTableTypes->SetAddressRow(m_plstData, nCurrentRow);
	}
	else
	{
		m_pTxtAddress->setPlainText("");
		m_pTableTypes->SetAddressRow(NULL);
	}

}



//when cotnact is changed, set fields from contact here:
void Table_ContactAddress::OnContactChanged()
{
	if (m_plstContact->getRowCount()==0)
		return;

	bool bSync=false;
	if (m_plstContact->getRowCount()>0)
	{
		if (m_plstContact->getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_PERSON && m_syncPers->isChecked())
			bSync=true;

		if (m_plstContact->getDataRef(0,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION && m_syncOrg->isChecked())
			bSync=true;

		//issue 945
		if (m_plstContact->getDataRef(0,"BCNT_FIRSTNAME").toString().isEmpty() && m_plstContact->getDataRef(0,"BCNT_LASTNAME").toString().isEmpty() && m_plstContact->getDataRef(0,"BCNT_ORGANIZATIONNAME").toString().isEmpty() && m_plstContact->getDataRef(0,"BCNT_LOCATION").toString().isEmpty())
		{
			bSync=true;
		}
	}

	if (!bSync)	return;

	//set data to all rows of contact:
	m_plstData->setColValue(m_plstData->getColumnIdx("BCMA_FIRSTNAME"),m_plstContact->getDataRef(0,"BCNT_FIRSTNAME").toString());
	m_plstData->setColValue(m_plstData->getColumnIdx("BCMA_LASTNAME"),m_plstContact->getDataRef(0,"BCNT_LASTNAME").toString());
	m_plstData->setColValue(m_plstData->getColumnIdx("BCMA_MIDDLENAME"),m_plstContact->getDataRef(0,"BCNT_MIDDLENAME").toString());
	m_plstData->setColValue(m_plstData->getColumnIdx("BCMA_ORGANIZATIONNAME"),m_plstContact->getDataRef(0,"BCNT_ORGANIZATIONNAME").toString());

	//Format address based on new data on all rows, based on stored schema ID or default:
	ClientContactManager::FormatAddressWithDefaultSchema(*m_plstData,m_plstContact->getDataRef(0,"BCNT_TYPE").toInt(),ClientContactManager::FORMAT_USING_STORED_FORMATSCHEMA,false);

	RefreshDisplay();
}



//used to check values before write
void Table_ContactAddress::CheckDataBeforeWrite(Status &Ret_pStatus)
{
	int nTestCol=m_plstData->getColumnIdx("BCMA_FORMATEDADDRESS");
	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if(m_plstData->getDataRef(i,nTestCol).toString().isEmpty())
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Address is mandatory field!"));	
			return;	
		}
	}
}
