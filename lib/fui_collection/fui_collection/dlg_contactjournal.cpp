#include "dlg_contactjournal.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/thememanager.h"

//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services



Dlg_ContactJournal::Dlg_ContactJournal(QWidget *parent)
: QDialog(parent)
{
	ui.setupUi(this);
	//define
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_JOURNAL_SELECT));

	setWindowTitle(tr("Journal Entry"));//set dialog title

	m_pData= new GuiDataManipulator(&m_lstData);

	//bring up field manager for description:
	m_GuiFieldManagerMain =new GuiFieldManager(m_pData);
	m_GuiFieldManagerMain->RegisterField("BCMJ_IS_PRIVATE",ui.ckbIsPrivate);
	m_GuiFieldManagerMain->RegisterField("BCMJ_DATE",ui.txtDate);
	m_GuiFieldManagerMain->RegisterField("BCMJ_TEXT",ui.frameEditor);

	ui.frameEditor->SetEmbeddedPixMode();

	//COMBO of persons:
	m_ComboHandler.Initialize(ENTITY_BUS_PERSON);
	m_ComboHandler.SetDataForCombo(ui.cmbPerson,"BPER_NAME","BPER_ID","BCMJ_PERSON_ID",m_pData);
	bool bOK=m_ComboHandler.ReloadData(); //reload

	m_pData->EnableDataChangeTracking("BCMJ_ID");

	connect(ui.frameEditor,SIGNAL(on_PrintIcon_clicked()),this,SLOT(OnPrint()));

	//issue 1702:
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

Dlg_ContactJournal::~Dlg_ContactJournal()
{
	delete m_GuiFieldManagerMain;
	delete m_pData;
}



/*!
	Called after WRITE, refresh NAME column

	\param rowData				- row must be filled with data when insert (table_1,2,record_1 or 2 )
	\param nMode				- READ,INSERT, or EDIT

*/
void Dlg_ContactJournal::SetRow(DbRecordSet &rowData,int nMode)
{

	m_lstData=rowData;
	m_nMode=nMode;
	Q_ASSERT(m_lstData.getRowCount()==1); //must contain 1 row

	//if edit lock
	if (nMode==MODE_EDIT)
	{
		int nID=m_lstData.getDataRef(0,"BCMJ_ID").toInt();
		Q_ASSERT(nID!=0);

		Status err;
		_SERVER_CALL(BusContact->LockJournal(err,nID,m_strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
	}

	//set Mode:
	SetMode(nMode);
}



void Dlg_ContactJournal::RefreshDisplay()
{
	m_ComboHandler.RefreshDisplay();
	m_GuiFieldManagerMain->RefreshDisplay();
}


void Dlg_ContactJournal::SetMode(int nMode)
{

	if(nMode==MODE_READ)
	{
		ui.cmbPerson->setEnabled(false);
		m_GuiFieldManagerMain->SetEditMode(false);
		ui.frameEditor->SetEditMode(false);
		ui.btnEdit->setEnabled(true);
		ui.btnDelete->setEnabled(true);
	}
	else
	{
		ui.cmbPerson->setEnabled(true);
		m_GuiFieldManagerMain->SetEditMode(true);
		ui.frameEditor->SetEditMode(true);
		ui.btnEdit->setEnabled(false);
		if (nMode!=MODE_EDIT)
			ui.btnDelete->setEnabled(false);  //delete enabled for edit: ensure unlock
	}
	RefreshDisplay();

}



void Dlg_ContactJournal::on_btnOK_clicked()
{
	if (m_nMode=MODE_EDIT || m_nMode==MODE_INSERT)
	{
		if (m_lstData.getDataRef(0,"BCMJ_PERSON_ID").toInt()==0)
		{
			QMessageBox::warning(this,tr("Warning"),tr("Person must be set!"));
			return;
		}

		Status err;
		DbRecordSet lstForDelete;
		//m_lstData.Dump();
		_SERVER_CALL(BusContact->WriteJournal(err,m_lstData,lstForDelete,m_strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		//assign person name back:
		m_lstData.setData(0,"BPER_NAME",ui.cmbPerson->currentText());

		done(1); //saved!
	}
	else
		done(0); //as it was cancel

}




void Dlg_ContactJournal::on_btnCancel_clicked()
{
	if(m_nMode==MODE_EDIT && !m_strLockedRes.isEmpty())
	{
		Status err;
		_SERVER_CALL(BusContact->UnlockJournal(err,m_strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		m_strLockedRes.clear();
	}

	done(0);
}

void Dlg_ContactJournal::on_btnEdit_clicked()
{
	Q_ASSERT(m_lstData.getRowCount()==1);//must be 1 row!
	int nID=m_lstData.getDataRef(0,"BCMJ_ID").toInt();
	Q_ASSERT(nID!=0);


	Status err;
	_SERVER_CALL(BusContact->LockJournal(err,nID,m_strLockedRes))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	SetMode(MODE_EDIT);

}


void Dlg_ContactJournal::on_btnDelete_clicked()
{
	Status err;
	Q_ASSERT(m_lstData.getRowCount()==1);//must be 1 row!

	//lock if not already
	bool bLock=false;
	if(m_strLockedRes.isEmpty())		//if not already locked, lock
		bLock=true;

	//delete it:
	_SERVER_CALL(BusContact->DeleteJournal(err,m_lstData,bLock));
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//unlock if allowed:
	if(!m_strLockedRes.isEmpty()) //if was edit mode, unlock deleted one
	{
		_SERVER_CALL(BusContact->UnlockJournal(err,m_strLockedRes))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		m_strLockedRes.clear();
	}

	done(2); //deleted
}


void Dlg_ContactJournal::OnPrint()
{
	if (m_lstData.getRowCount()==0)
		return;


	//set list:
	MainEntitySelectionController sel;
	sel.Initialize(ENTITY_BUS_CONTACT);

	QString strContact;
	DbRecordSet recContact;
	int iID=m_lstData.getDataRef(0,"BCMJ_CONTACT_ID").toInt();
	sel.GetEntityRecord(iID,recContact);
	if (recContact.getRowCount()>0)
		recContact.getData(0,"BCNT_NAME",strContact);


	QStringList list;
	list<<strContact;
	list<<ui.txtDate->text();
	list<<ui.cmbPerson->currentText();

	qDebug()<<list;

	//0 - contact name?
	//1 - date
	//2 - person

	ui.frameEditor->SetupPrintingHeader(list);
}