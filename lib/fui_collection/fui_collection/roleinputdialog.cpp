#include "roleinputdialog.h"

/*!
Role input, pass Role name, description, type name and insert (or rename) bool.

\param UserFirstName	- User first name.
\param UserLastName		- User last name.
\param bInsertUser		- User insert bool - true=insert, false=rename.
\param parent			- parent.
*/
RoleInputDialog::RoleInputDialog(QString *RoleName, QString *RoleDescription, int *nRoleType, bool bInsertRole, QWidget *parent /*= 0*/)
				: QDialog(parent)
{
	ui.setupUi(this);

	m_pstrRoleName			= RoleName;
	m_pstrRoleDescription	= RoleDescription;
	m_nRoleType				= nRoleType;
	m_bInsertRole			= bInsertRole;

	//Set maximum line edit length.
	ui.RoleNameLineEdit->setMaxLength(255);
	ui.RoleDescrLineEdit->setMaxLength(255);
	//Set labels.
	ui.name_label->setText(tr("Role Name"));
	ui.descr_label->setText(tr("Role Description")); 
	ui.RoleTypeCheckBox->setText(tr("System Role"));
	ui.BusVisibleCheckBox->setText(tr("Business visible"));
	ui.OkPushButton->setText(tr("&Ok"));
	ui.CancelPushButton->setText(tr("&Cancel"));

	//If new item show default.
	if (m_bInsertRole == true )
	{
		ui.RoleNameLineEdit->insert(tr("New Role"));
		ui.RoleNameLineEdit->setFocus(Qt::OtherFocusReason);
	}
	else
	{
		ui.RoleNameLineEdit->insert(*m_pstrRoleName);
		ui.RoleDescrLineEdit->insert(*m_pstrRoleDescription);
		if (*m_nRoleType < 2)
		{
			ui.RoleTypeCheckBox->setCheckState(Qt::Checked);
			
			if (*m_nRoleType)
				ui.BusVisibleCheckBox->setCheckState(Qt::Checked);
		}

		ui.RoleTypeCheckBox->setDisabled(true);
		ui.BusVisibleCheckBox->setDisabled(true);
		ui.RoleNameLineEdit->setFocus(Qt::OtherFocusReason);
	}
}

RoleInputDialog::~RoleInputDialog()
{
	m_pstrRoleName			= NULL;
	m_pstrRoleDescription	= NULL;
	m_nRoleType				= NULL;
}

/*!
Ok button clicked, check is there Role name (at least) and add  role.
*/
void RoleInputDialog::on_OkPushButton_clicked()
{
	if (ui.RoleNameLineEdit->text() == "")
	{
		QMessageBox::information(this, tr("Access Rights"),
			tr("Role Name field empty!\n"
			"Please insert role name field."));
	}
	else
	{
		*m_pstrRoleName = ui.RoleNameLineEdit->text();
		*m_pstrRoleDescription = ui.RoleDescrLineEdit->text();

		if (ui.BusVisibleCheckBox->checkState() == Qt::Checked)
			*m_nRoleType = 1;
		else if (ui.RoleTypeCheckBox->checkState() == Qt::Checked)
			*m_nRoleType = 0;
		else
			*m_nRoleType = 2;

		accept();
	}
}


/*!
Cancel all.
*/
void RoleInputDialog::on_CancelPushButton_clicked()
{
	reject();
}

void RoleInputDialog::on_BusVisibleCheckBox_stateChanged(int state)
{
	if (state == Qt::Checked)
		ui.RoleTypeCheckBox->setCheckState(Qt::Checked);
}

void RoleInputDialog::on_RoleTypeCheckBox_stateChanged(int state)
{
	if (state == Qt::Unchecked)
		ui.BusVisibleCheckBox->setCheckState(Qt::Unchecked);
}
