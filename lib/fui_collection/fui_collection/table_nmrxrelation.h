#ifndef TABLE_NMRXRELATION_H
#define TABLE_NMRXRELATION_H

#include <QCheckBox>
#include <QComboBox>


#include "gui_core/gui_core/universaltablewidgetex.h"

class Table_NMRXRelation : public UniversalTableWidgetEx
{
	friend class Table_NMRXRelation_WidgetInvite;
	Q_OBJECT

public:
	Table_NMRXRelation(QWidget * parent=0);

	void Initialize(DbRecordSet *pData,int nMasterTableID,QList<int>lstPairs, int nX_TableID=-1);
	void GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip);	
	
	//enable drop:
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void dragMoveEvent(QDragMoveEvent *event );

protected:
	void Data2CustomWidget(int nRow, int nCol);
	void CustomWidget2Data(int nRow, int nCol);

private slots:
	void On_X_WidgetChanged(int);

signals:
	void NewDataDropped(int nEntityTypeID, DbRecordSet recData);
private:
	int m_nMasterTableID,m_nTable1,m_nTable2,m_nX_TableID;
	int m_nCol_X_Widget;
	int m_nCol_Desc;
	int m_nAssignedTableID; //only for calendar assign
};


class Table_NMRXRelation_WidgetInvite : public QWidget
{
	Q_OBJECT
public:
	Table_NMRXRelation_WidgetInvite(Table_NMRXRelation *parent,int nRow);
	~Table_NMRXRelation_WidgetInvite();

	Table_NMRXRelation *m_Parent;
	int m_nRow;
	QCheckBox *m_pInvited;
	QCheckBox *m_pInviteSent;
	QCheckBox *m_pBySokrates;
	QCheckBox *m_pByEmail;
	QCheckBox *m_pBySMS;
	QComboBox *m_pStatus;

signals:
	void DataChanged(int nRow);

private slots:
	void OnStateChanged(int){emit DataChanged(m_nRow);};
};



#endif // TABLE_NMRXRELATION_H
