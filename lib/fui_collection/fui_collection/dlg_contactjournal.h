#ifndef DLG_CONTACTJOURNAL_H
#define DLG_CONTACTJOURNAL_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_contactjournal.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "bus_client/bus_client/selection_combobox.h"




/*!
	\class  Dlg_ContactJournal
	\brief  Dialog for editing, viewing and saving journal
	\ingroup Fui_collection
*/
class Dlg_ContactJournal : public QDialog
{
	Q_OBJECT

public:
	Dlg_ContactJournal(QWidget *parent = 0);
	~Dlg_ContactJournal();

	//Modes:
	enum Mode 
	{
		MODE_READ=0,	
		MODE_EDIT=1,			
		MODE_INSERT=2,			
		MODE_EMPTY=3	
	};

	void SetRow(DbRecordSet &rowData,int nMode=MODE_READ);
	void GetRow(DbRecordSet &rowData){rowData=m_lstData;}

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	//void on_btnNew_clicked(){};
	void on_btnEdit_clicked();
	void on_btnDelete_clicked();
	void OnPrint();



private:

	void RefreshDisplay();
	void SetMode(int nMode);

	QString m_strLockedRes;
	int m_nMode;
	DbRecordSet m_lstData;

	GuiFieldManager *m_GuiFieldManagerMain;
	GuiDataManipulator *m_pData;
	Selection_ComboBox m_ComboHandler;			//super handler: connected to cache of persons


	Ui::Dlg_ContactJournalClass ui;
};

#endif // DLG_CONTACTJOURNAL_H
