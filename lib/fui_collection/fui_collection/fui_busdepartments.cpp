#include "fui_busdepartments.h"
#include "fuimanager.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "common/common/entity_id_collection.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
extern BusinessServiceManager *g_pBoSet;


FUI_BusDepartments::FUI_BusDepartments(QWidget *parent)
: FuiBase(parent)
{

	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);
	InitFui(ENTITY_BUS_DEPARTMENT, BUS_DEPARTMENT, dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	EnableHierarchyCodeCheck("BDEPT_CODE");//reload left menu after write
	ui.btnButtonBar->RegisterFUI(this);
	ui.frameSelection->Initialize(ENTITY_BUS_DEPARTMENT,true,true);	//init selection controller
	ui.framePersons->Initialize(ENTITY_BUS_PERSON, true);
	InitializeFUIWidget(ui.splitter);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_BUS_DEPARTMENT);
	m_GuiFieldManagerMain->RegisterField("BDEPT_CODE",ui.txtCode);
	m_GuiFieldManagerMain->RegisterField("BDEPT_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("BDEPT_ACTIVE_FLAG",ui.chkActive);
	m_GuiFieldManagerMain->RegisterField("BDEPT_DESCRIPTION",ui.txtDescription);

	
	ui.frmCostCenter->Initialize(ENTITY_BUS_COST_CENTER, &m_lstData,"BDEPT_COST_CENTER_ID");

	//---------------------------------------
	//init ACTUAL ORG SMR:
	//---------------------------------------
	ui.frameOrganization->Initialize(&m_lstData,"BDEPT_ORGANIZATION_ID");
	SetActualOrganizationHandler(ui.frameOrganization,"BDEPT_ORGANIZATION_ID");
	//set edit to false:
	SetMode(MODE_EMPTY);

	ui.txtName->setFocus();
}

FUI_BusDepartments::~FUI_BusDepartments()
{
	delete m_GuiFieldManagerMain;
}
	
//when programatically change data, call this:
void FUI_BusDepartments::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frmCostCenter->RefreshDisplay();
	ui.frameOrganization->RefreshDisplay();
}

//when changing state, call this:
void FUI_BusDepartments::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameSelection->setEnabled(true);

		//details:
		m_GuiFieldManagerMain->SetEditMode(false);
		ui.frmCostCenter->SetEditMode(false);
		//ui.frameDetails->setEnabled(true);			//needed because of inverse state of one btn

		ui.txtCode->setEnabled(false);
		ui.txtName->setEnabled(false);
		ui.txtDescription->setEnabled(false);
		ui.chkActive->setEnabled(false);
		ui.btnEditDescription->setEnabled(false);

		//ui.btnButtonBar->SetMode(nMode); //this is necessary to reenable buttons!
	}
	else
	{
		//details:
		m_GuiFieldManagerMain->SetEditMode(true);
		ui.frmCostCenter->SetEditMode(true);
		//ui.frameDetails->setEnabled(true);
		ui.frameSelection->setEnabled(false);

		ui.txtCode->setEnabled(true);
		ui.txtName->setEnabled(true);
		ui.txtDescription->setEnabled(true);
		ui.chkActive->setEnabled(true);
		ui.btnEditDescription->setEnabled(true);

		//Set focus to Pers. No:
		ui.txtCode->setFocus(Qt::OtherFocusReason);
	}

	//this button is always enabled (MB changed his mind again)
	ui.btnOpenPersons->setEnabled(true);

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}

void FUI_BusDepartments::on_selectionChange(int nEntityRecordID)
{
	FuiBase::on_selectionChange(nEntityRecordID);	//call base class
	//reload persons based on new dept id
	FilterAssigments(nEntityRecordID);
}

void FUI_BusDepartments::on_cmdInsert()
{
	//filter other data to dummy record:
	FilterAssigments(-1);

	FuiBase::on_cmdInsert();
	ui.txtName->setFocus();
}


void FUI_BusDepartments::on_btnOpenPersons_clicked()
{
	//get selected person
	int nRecordID = -1;
	QString strCode, strName;
	DbRecordSet set;
	ui.framePersons->GetSelection(nRecordID, strCode, strName, set);

	//open FUI with PERSON data, but not show
	int nNewFUI = g_objFuiManager.OpenFUI(MENU_PERSON, true, false, FuiBase::MODE_READ, nRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	if (pFUI)
	{
		if(ui.frameOrganization->GetCurrentActualOrganizationID()>0 && nRecordID==-1)//if valid ORG and invalid record, set org, else, org is set automatically
		{
			dynamic_cast<FuiBase *>(pFUI)->SetActualOrganization(ui.frameOrganization->GetCurrentActualOrganizationID());  //set org as defult
		}
		pFUI->show();  //show FUI
	}
	
}

//set filters and refresh display for current Actual Org
void FUI_BusDepartments::SetFiltersForCurrentActualOrganization(int nActualOrganizationID)
{
	//notify left selection (specific for each FUI):
	ui.frameSelection->ClearLocalFilter();
	ui.frameSelection->GetLocalFilter()->SetFilter("BDEPT_ORGANIZATION_ID",nActualOrganizationID);
	ui.frameSelection->ReloadData();

	ui.frmCostCenter->GetSelectionController()->ClearLocalFilter();
	ui.frmCostCenter->GetSelectionController()->GetLocalFilter()->SetFilter("BCTC_ORGANIZATION_ID",nActualOrganizationID);
	ui.frmCostCenter->SetActualOrganization(nActualOrganizationID);
}

void FUI_BusDepartments::on_btnEditDescription_clicked()
{
	QString strHtml = ui.txtDescription->toHtml();

	RichEditorDlg dlg;
	dlg.SetHtml(strHtml);
	if(0 != dlg.exec()){
		ui.txtDescription->setHtml(dlg.GetHtml());
	}
}

void FUI_BusDepartments::FilterAssigments(int nDeptID)
{
	if (nDeptID<=0)//clear grid:
	{
		ui.framePersons->GetDataSource()->clear();
		ui.framePersons->RefreshDisplay();
		return;
	}

	//notify all other selection assignment controllers
	ui.framePersons->ClearLocalFilter();
	ui.framePersons->GetLocalFilter()->SetFilter("BPER_DEPARTMENT_ID",nDeptID);
	ui.framePersons->ReloadData();

	//ui.framePersons->GetDataSource()->Dump();
}

//get FUI name
QString FUI_BusDepartments::GetFUIName()
{
	if(m_nEntityRecordID>0)
		return tr("Department: ")+ui.frameSelection->GetCalculatedName(m_nEntityRecordID);
	else
		return tr("Department");
}


void FUI_BusDepartments::DataPrepareForInsert(Status &err)
{
	DataClear();
	m_lstData.addRow();
	m_lstData.setData(0,"BDEPT_ACTIVE_FLAG",1);
	m_lstData.setData(0,"BDEPT_ICON","");	
}
void FUI_BusDepartments::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	if(m_lstData.getDataRef(0,"BDEPT_CODE").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Code is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"BDEPT_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Name is mandatory field!"));	return;	}
}