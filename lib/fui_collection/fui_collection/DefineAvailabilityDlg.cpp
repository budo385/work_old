#include "DefineAvailabilityDlg.h"

#include "roleinputdialog.h"
#include "arstoroleassigndialog.h"
#include "gui_core/gui_core/gui_helper.h"
#include "bus_client/bus_client/selection_tablebased.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "common/common/datahelper.h"
#include "trans/trans/xmlutil.h"
#include <QInputDialog>
#include "bus_core/bus_core/globalconstants.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;					//global FUI manager:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/selectionpopup.h"

DefineAvailabilityDlg::DefineAvailabilityDlg(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	//set defaults
	ui.dateFrom->setDate(QDate::currentDate());
	ui.dateTo->setDate(QDate::currentDate().addDays(7));
	ui.chkMo->setCheckState(Qt::Checked);
	ui.chkTu->setCheckState(Qt::Checked);
	ui.chkWe->setCheckState(Qt::Checked);
	ui.chkTh->setCheckState(Qt::Checked);
	ui.chkFr->setCheckState(Qt::Checked);
}

DefineAvailabilityDlg::~DefineAvailabilityDlg()
{
}

void DefineAvailabilityDlg::on_btnOK_clicked()
{
	done(1);
}
	
void DefineAvailabilityDlg::on_btnCancel_clicked()
{
	done(0);
}

bool DefineAvailabilityDlg::IsWeekDayChecked(int nDay)
{
	switch (nDay){
		case 1:
			return IsMoChecked();
		case 2:
			return IsTuChecked();
		case 3:
			return IsWeChecked();
		case 4:
			return IsThChecked();
		case 5:
			return IsFrChecked();
		case 6:
			return IsSaChecked();
		case 7:
			return IsSuChecked();
		default:
			return false;
	}
}
