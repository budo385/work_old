#include "communicationmenu_base.h"
#include "communicationactions.h"
#include "fui_voicecallcenter.h"
#include <QDesktopServices>
#include "bus_client/bus_client/documenthelper.h"
#include "gui_core/gui_core/macros.h"

//global FUI manager:
#include "fuimanager.h"
extern FuiManager g_objFuiManager;	

//bo set
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	

#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager

//Doc_MenuWidget* CommunicationMenu_Base::m_menuDoc=NULL;
//Email_MenuWidget* CommunicationMenu_Base::m_menuEmail=NULL;


CommunicationMenu_Base::CommunicationMenu_Base(QWidget *parent)
 :QFrame(parent),m_menuDropZone(NULL),m_btnPhone(NULL),m_btnInternet(NULL),m_menuDoc(NULL),m_menuEmail(NULL),m_menuApp(NULL),m_menuCalendar(NULL)
{

	//connect(this,SIGNAL(NeedNewData()),this,SLOT(SubMenuRequestedNewData()));
}

CommunicationMenu_Base::~CommunicationMenu_Base()
{

}


void CommunicationMenu_Base::SubMenuRequestedNewData(int nSubMenuType)
{
	emit NeedNewData(nSubMenuType);
}

//format can be CE_LINK or oridnary contact
void CommunicationMenu_Base::SetDefaults(DbRecordSet *lstContacts,DbRecordSet *lstProjects,DbRecordSet *rowQCWContact)
{
	int nCntID=-1;
	if(lstContacts)	
	{
		m_lstContacts=*lstContacts;
		nCntID=lstContacts->getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nCntID==-1)
			nCntID=lstContacts->getColumnIdx("BCNT_ID");
		Q_ASSERT(nCntID!=-1);
		m_lstContacts.setColumn(nCntID,QVariant::Int,"BCNT_ID");
	}
	else
	{
		nCntID=0;
		m_lstContacts.destroy();
		m_lstContacts.addColumn(QVariant::Int,"BCNT_ID");
	}



	if(lstProjects) 
		m_lstProjects=*lstProjects;
	else
	{
		m_lstProjects.destroy();
		m_lstProjects.addColumn(QVariant::Int,"BUSP_ID");
	}

	//read NMRX projects from 1st contact:
	//DbRecordSet lstProjects;
	if (m_lstContacts.getRowCount()>0 && lstProjects==NULL)
	{
		int nContactID=m_lstContacts.getDataRef(0,nCntID).toInt();
		Status err;
		_SERVER_CALL(BusContact->ReadNMRXProjects(err,nContactID,m_lstProjects))
		_CHK_ERR(err);
		if (m_lstProjects.getRowCount()>1) //if more then 1 ignore all
			m_lstProjects.clear();
	}


	if (m_menuDoc)m_menuDoc->SetDefaults(&m_lstContacts,&m_lstProjects,rowQCWContact); //only resend roq QCW if exists
	if (m_menuEmail)m_menuEmail->SetDefaults(&m_lstContacts,&m_lstProjects);
	if (m_menuApp)m_menuApp->SetDefaults(&m_lstContacts,&m_lstProjects,rowQCWContact); //only resend roq QCW if exists
	if (m_menuCalendar)m_menuCalendar->SetDefaults(&m_lstContacts,&m_lstProjects); 


	//set to global manager: fro document drop (mostly):
	DocumentHelper::SetDocumentDropDefaults(&m_lstContacts,&m_lstProjects,rowQCWContact);

}

void CommunicationMenu_Base::SetDefaultMail(QString strMail,int nEmailReplyID)
{
	//in menu doc
	if (m_menuEmail)m_menuEmail->SetDefaultMail(strMail,nEmailReplyID);

	m_strMailDefault=strMail;
	m_nEmailReplyID=nEmailReplyID;

}
void CommunicationMenu_Base::SetDefaultPhone(QString strPhone)
{
	m_strPhone=strPhone;

}

void CommunicationMenu_Base::SetDefaultInternet(QString strInternet)
{
	m_strInternet=strInternet;
}


void CommunicationMenu_Base::OnPhoneCall()
{
	SubMenuRequestedNewData(); //if sidebar get from sidebar handler


	int nProjectID=-1;
	if (m_lstProjects.getRowCount()==1) //only if just 1
	{
		nProjectID=m_lstProjects.getDataRef(0,"BUSP_ID").toInt();
	}

	int nCntID=m_lstContacts.getColumnIdx("BCNT_ID");


	//only if loaded contact, open voice calls
	if (m_lstContacts.getRowCount()>0 && nCntID!=-1)
	{
		int nSize=m_lstContacts.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true);
			FUI_VoiceCallCenter *pVoiceCall=dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nNewFUI));
			if (pVoiceCall)
			{
				int nContactID=m_lstContacts.getDataRef(i,nCntID).toInt();
				pVoiceCall->SetDefaultContact(nContactID,m_strPhone,nProjectID);      
			}
		}
	}
	else if (nProjectID!=-1)
	{

		int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true);
		FUI_VoiceCallCenter *pVoiceCall=dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nNewFUI));
		if (pVoiceCall)
			pVoiceCall->SetDefaultContact(-1,"",nProjectID);      
	}
	else
	{
		g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true);
	}

}

void CommunicationMenu_Base::OnSendEmail()
{
	if (m_menuEmail)
		m_menuEmail->OnMenuButton();
}



void CommunicationMenu_Base::OnInternet()
{
	SubMenuRequestedNewData(); //if sidebar get from sidebar handler

	

	//if cybernetic address is empty and contacts are set, try to read out from server:
	if (m_strInternet.isEmpty() && m_lstContacts.getRowCount()>0)
	{
		int nCntID=m_lstContacts.getColumnIdx("BCNT_ID");
		if (nCntID>=0)
		{

			bool bInternetAddressFound=false;
			int nSize=m_lstContacts.getRowCount();
			for(int i=0;i<nSize;++i)
			{

				int nContactID=m_lstContacts.getDataRef(i,nCntID).toInt();
				Status err;
				DbRecordSet rowContactData;
				_SERVER_CALL(BusContact->ReadContactDefaults(err,nContactID,rowContactData))
				if(!err.IsOK())
				{
					QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
					return;
				}
				if (rowContactData.getRowCount()>0)
				{
					m_strInternet=rowContactData.getDataRef(0,"BCMI_ADDRESS").toString();
				}

				if (!m_strInternet.isEmpty())
				{
					bInternetAddressFound=true;
					QDesktopServices::openUrl(m_strInternet.trimmed());
				}

			}

			if (!bInternetAddressFound)
			{
				QMessageBox::information(this,tr("Warning"),tr("No internet address found for selected contact(s)!"));
			}
		}
	}
	else if (!m_strInternet.isEmpty())
	{
		QDesktopServices::openUrl(m_strInternet.trimmed());
	}
	else
	{

		QMessageBox::information(this,tr("Warning"),tr("No internet address found for selected contact(s)!"));
	}



}

void CommunicationMenu_Base::OnSMS()
{
	SubMenuRequestedNewData(); //if sidebar get from sidebar handler
	if (m_lstContacts.getRowCount()>0)
	{
		int nCntIDx=m_lstContacts.getColumnIdx("BCNT_ID");
		if (nCntIDx>=0)
		{
			int nContactID=m_lstContacts.getDataRef(0,nCntIDx).toInt();
			CommunicationActions::openSMS(nContactID);
		}
	}
	

}
void CommunicationMenu_Base::OnChat()
{
	SubMenuRequestedNewData(); //if sidebar get from sidebar handler
	if (m_lstContacts.getRowCount()>0)
	{
		int nCntIDx=m_lstContacts.getColumnIdx("BCNT_ID");
		if (nCntIDx>=0)
		{
			int nContactID=m_lstContacts.getDataRef(0,nCntIDx).toInt();
			CommunicationActions::openChat(nContactID);
		}
	}
}


void CommunicationMenu_Base::SetSerialEmailMode()
{
	if (m_menuEmail)m_menuEmail->SetSerialMode();
}



