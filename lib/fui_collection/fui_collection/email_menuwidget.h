#ifndef EMAIL_MENUWIDGET_H
#define EMAIL_MENUWIDGET_H

#include <QWidget>
#include "generatedfiles/ui_email_menuwidget.h"
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QAction>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QMenu>
#include "common/common/observer_ptrn.h"
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"


class Email_MenuWidget : public QWidget, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Email_MenuWidget(QWidget *parent = 0,bool bIgnoreViewMode=false);
	~Email_MenuWidget();

	//Qt::DropActions supportedDropActions() const;
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );

	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void SetDefaults(DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL);
	void SetDefaultMail(QString strEmail,int nEmailReplyID=-1,bool bIsForward=false){m_strEmailDefault=strEmail;m_nEmailReplyID=nEmailReplyID;m_bIsForward=bIsForward;}
	void DisableHide(){ui.btnHide->setVisible(false);};
	void SetPackSendMode(QStringList *lstPaths){m_lstPackSendPaths=*lstPaths;}
	void SetScheduleFlag(){m_bScheduleTask=true;}
	void SetCreateQuickTaskFlag(){m_bCreateQuickTask=true;}

	void SetCloseOnSend();
	void SetSerialMode(bool bUseAllContactEmails=false){m_bSerial_Mode=true; m_bSerial_UseAllContactEmails=bUseAllContactEmails; };
	bool IsHidden();
	void SetHidden(bool bHide=true);
	void SetSideBarLayout();

public slots:
	void OnMenuButton();

signals:
	void NeedNewData(int nSubMenuType);		//emited always when new document is about to be created...to assign doc to contacts/projects later

private slots:
	void OnHideTab();

	//template:
	void OnNewFromTemplate();
	void OnNewTask();
	void OnAddTemplate();
	void OnModifyTemplate();
	void OnRemoveTemplate();
	void OnCategoryChanged(const QString strCategory);

	//register:
	void OnRegister();

	void ReLoadTemplates(){LoadTemplates(true);}

protected:
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent * event);

private:
	Ui::Email_MenuWidgetClass ui;
	void OnThemeChanged();
	void LoadTemplates(bool bReload=false);
	void LoadCategory(bool bReload=false);
	void OpenQuickTaskEmail(int nOp, int nRecordID=-1);

	QTreeWidgetItem* SearchTree(QTreeWidget* pTree, QVariant nUserValue);

	DbRecordSet m_lstTemplates;
	DbRecordSet m_lstContacts; 
	int m_nProjectID;
	int m_nLoggedID;
	int m_nEmailReplyID;
	QString m_strEmailDefault;
	QStringList m_lstPackSendPaths;
	bool m_bIsForward;
	bool m_bCloseOnSend;
	bool m_bScheduleTask;
	MainEntitySelectionController m_Category;	
	QPoint m_dragPosition;
	bool m_bCreateQuickTask;
	bool m_bSerial_Mode;
	bool m_bSerial_UseAllContactEmails;
};






#endif // EMAIL_MENUWIDGET_H
