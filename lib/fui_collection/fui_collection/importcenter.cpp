#include "importcenter.h"
#include "fui_importcontact.h"
#include "fui_importproject.h"

#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester


#include "os_specific/os_specific/skypemanager.h"
#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#include "os_specific/os_specific/mapimanager.h"


ImportCenter::ImportCenter(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	ui.labelPic->setPixmap(QPixmap(":splash.png"));
	//TOFIX couldn't set this through the designer
	ui.labelPic->setAlignment(Qt::AlignLeft|Qt::AlignTop);
	//QTimer::singleShot(0,this,SLOT(Activate())); //bring on top after login process:

	//import:
	if (!g_FunctionPoint.IsFPAvailable(FP_IMPORT_CONTACTS_MANUALLY))
	{
		ui.btnImportSykpe_Contact->setEnabled(false);
		ui.btnImportOutlook_Contact->setEnabled(false);
		ui.btnImportTxt_Contact->setEnabled(false);
	}
	if (!g_FunctionPoint.IsFPAvailable(FP_IMPORT_PROJECTS_MANUALLY))
		ui.btnImportTxt_Project->setEnabled(false);

	if (!g_FunctionPoint.IsFPAvailable(FP_IMPORT_EMAILS_MANUALLY))
		ui.btnImportOutlook_Email->setEnabled(false);

#ifdef _WIN32
	bool bOutLook=MapiManager::IsOutlookInstalled();
	if (!bOutLook)
	{
		ui.btnImportOutlook_Contact->setEnabled(false);
		ui.btnImportOutlook_Email->setEnabled(false);
	}
#endif
	if (SkypeManager::IsSkypeInstalled())
	{
		ui.btnImportSykpe_Contact->setEnabled(false);
	}
}

ImportCenter::~ImportCenter()
{

}


void ImportCenter::on_btnImportSykpe_Contact_clicked()
{

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
	FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->m_bSkipStartupFolderPicker = true; // started from "Info Center"
		pFui->SetDefaultSource(tr("Skype Contacts"));
		pFui->show();  
		pFui->StartSearch();
	}
}

void ImportCenter::on_btnImportOutlook_Contact_clicked()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
	FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->m_bSkipStartupFolderPicker = true; // started from "Info Center"
		pFui->SetDefaultSource(tr("Outlook"));
		pFui->show();  
		pFui->StartSearch();
	}

}

void ImportCenter::on_btnImportTxt_Contact_clicked()
{
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
	FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->m_bSkipStartupFolderPicker = true; // started from "Info Center"
		pFui->SetDefaultSource(tr("Text File"));
		pFui->show();
		pFui->StartSearch();
	}
}
void ImportCenter::on_btnImportTxt_Project_clicked()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_PROJECT, true);
}


void ImportCenter::on_btnImportOutlook_Email_clicked()
{
	g_objFuiManager.OpenFUI(MENU_IMPORT_EMAIL,true);

}


void ImportCenter::on_btnFinish_clicked()
{
	this->close();
	this->deleteLater();
}

void ImportCenter::Activate()
{
	//this->activateWindow();
}