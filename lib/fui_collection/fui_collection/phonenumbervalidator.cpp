#include "phonenumbervalidator.h"

PhoneNumberValidator::PhoneNumberValidator(QObject *parent)
	: QValidator(parent)
{

}

PhoneNumberValidator::~PhoneNumberValidator()
{

}

QValidator::State PhoneNumberValidator::validate(QString &input, int &pos) const
{
	QString InputString = input.right(1);
	if (InputString == "+" || InputString == "-" || InputString == "(" || InputString == ")"
		|| InputString == "1" || InputString == "2" || InputString == "3" || InputString == "4"
		|| InputString == "5" || InputString == "6" || InputString == "7" || InputString == "8"
		|| InputString == "9" || InputString == "0" )
		return QValidator::Acceptable;
	else
		return QValidator::Invalid;
}
