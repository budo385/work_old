#ifndef EMAILDIALOG_H
#define EMAILDIALOG_H

#include <QWidget>
#include "generatedfiles/ui_emaildialog.h"

//WARNING WARNING
//WARNING WARNING
//
// clientmanager.h must be included before  mapiex.h
//
//WARNING WARNING
//WARNING WARNING

#include "common/common/dbrecordset.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"


class EMailDialog : public FuiBase
{
    Q_OBJECT

public:
    EMailDialog(QWidget *parent = 0);
    ~EMailDialog();
	
	void	Execute (int nEmailID);		//execute scheduled mail
	void	SetDefaults(DbRecordSet recContacts, QString strEmail = "", int nProjectID = -1, int nTemplateEmailID=-1, bool bScheduleTask=false,bool bSetAsTemplate=false, int nEmailReplyID=-1,QStringList *lstZippedFiles=NULL, bool bForwardMail=false);
	void	SetEmailData(QString strSubject, QString strBody,int nOwnerID=-1,QString strEmail="");
	void	SetEmailData(DbRecordSet &recData);
	void    SetSerialMailingMode(bool bUseAllContactEmails=false);
	QString GetFUIName();
	void	ShowMailSendRadios(bool bShow = true);

	int GetTaskID();
	void RefreshAvatarData(bool bReload = false);

	bool CanClose();

	//issue 1456
	DbRecordSet *GetTaskRecordSet();
	DbRecordSet *GetRecordSet(){return &m_lstData;};
	void CreateQuickTask(Status &err,DbRecordSet &rowTask);

	static QString GetTaskDate(int nCEType, DbRecordSet &recEntityRecord, DbRecordSet &recTaskRecord, int nRow=0);
	static QString GetTaskAssignedContact(int nContactID);
	static QString GetPersonInitials(int nPersonID);
	
	static void RemoveAvatar(int nType, int nRecordID);

private slots:
	void on_AddContacts_pushButton_clicked();
	void on_grupIsTemplate_clicked();
	void on_chkPlainText_clicked();
	void on_chkFixHTML_clicked();
	void OnScheduleTaskToggle();
	void OnBodyChanged();
	void OnSaveSend();
	void OnEditor_VerticalOpenClose_clicked(bool bSmaller);
	void OnEditViewSwitched();
	void FixHtmlViewLater();
	void updateTaskTabCheckbox();
	void updateTemplateTabCheckbox();

protected:
	virtual void changeEvent (QEvent * event);
	virtual void closeEvent(QCloseEvent *event);
	bool m_bHideOnMinimize;

	void DataWrite(Status &err);
	void DataPrepareForInsert(Status &err);
	void DataPrepareForCopy(Status &err);
	void DataClear();
	void DataDefine();
	void DataRead(Status &err,int nRecordID);
	void DataLock(Status &err);
	void DataDelete(Status &err);
	void DataUnlock(Status &err);

private:
    Ui::EMailDialogClass ui;
	void	PrepareReplyForwardMail(int nEmailID, bool bReply=true);
	void	PrepareMailToSendToContacts(DbRecordSet &recContacts, DbRecordSet &recContactsWithoutEmail, QString strDefaultEmailForFirstContact="");

	void	RefreshDisplay();
	void	SetMode(int nMode);
	void	ClearTemplateFields();

	void	Data2Emails(DbRecordSet &lstContactEmails,QString strTo,QString strCC,QString strBCC); //from DB to recipient table
	void	Emails2Data(QString &strTo,QString &strCC,QString &strBCC); //from recipient table 2 data for DB write
	void	updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void	keyPressEvent(QKeyEvent* event);
	void	SetPersonData();
	
	void	SetRecipientString(DbRecordSet &recRecipients, QString &strRecipients);
	void	WriteTo_CE_CONTACT_LINK(DbRecordSet &recRecipients, int nType /*1=From 2=To 3=CC 4=BCC*/);
	bool	SendMail(DbRecordSet &recError);
	bool	OutlookSendMailByDLL();
	void	OnThemeChanged();

	void	FillNamesInToCCBCCRecords();

	
	DbRecordSet m_lstTaskCache;				//task cachier
	DbRecordSet m_lstContactLink;			//CE_CONTACT_LINK recordset - for writing to DB.
	DbRecordSet m_lstAttachments;			
	GuiFieldManager *m_GuiFieldManagerMain;
	bool m_bSerialEmailMode;
	bool m_bSerialUseAllContactEmails;
	int  m_nProjectID;	//cache for serial mode
	DbRecordSet	m_recToRecordSet;	//TO recipients recordset.
	DbRecordSet	m_recCCRecordSet;	//CC recipients recordset.
	DbRecordSet	m_recBCCRecordSet;	//BCC recipients recordset.
	bool m_bBodyChanged;
	QString		m_strPersonContactID;	//Logged Person Contact ID.
	QPushButton *m_buttonSaveSend;
	bool		m_bSaveSendClicked;

	DbRecordSet	m_lstCIDInfo;	// stores info on local image path vs. CID name
	//bool m_bCIDPrepared;		// used for assert
	//bool m_bCIDFinalized;		// used for assert
	bool m_CID_ModifyEmailBodyInProgress;
	bool m_bOpenFromTemplate;	// Marko je trazio
	MainEntitySelectionController m_PersonCache;
	MainEntitySelectionController m_ContactCache;
	
};

#endif // EMAILDIALOG_H
