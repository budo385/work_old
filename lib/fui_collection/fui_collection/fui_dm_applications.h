#ifndef FUI_DM_APPLICATIONS_H
#define FUI_DM_APPLICATIONS_H

#include "generatedfiles/ui_fui_dm_applications.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"



class FUI_DM_Applications : public FuiBase
{
	Q_OBJECT

public:
	FUI_DM_Applications(QWidget *parent = 0);
	~FUI_DM_Applications();

	bool on_cmdOK();
	void on_cmdCancel();
	bool on_cmdDelete();
	bool CanClose();

	void DataPrepareForInsert(Status &err);

	QString GetFUIName();
	void DataWrite(Status &err);

private slots:
	void OnModifyIcon();


private:
	void RefreshDisplay();
	void SetMode(int nMode);
	void RefreshIcon();
	void	keyPressEvent(QKeyEvent* event);

	GuiFieldManager *m_GuiFieldManagerMain;

	Ui::FUI_DM_ApplicationsClass ui;
};

#endif // FUI_DM_APPLICATIONS_H

