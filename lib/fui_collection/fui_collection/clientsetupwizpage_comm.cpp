#include "clientsetupwizpage_comm.h"

//#include "settings_outlook.h"
void FolderList2String(const QList<QStringList> &lstData, QString &strResult);
void String2FolderList(QString strData, QList<QStringList> &lstResult);

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

#include "os_specific/os_specific/skypemanager.h"
#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#include "os_specific/os_specific/mapimanager.h"

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

ClientSetupWizPage_Comm::ClientSetupWizPage_Comm(int nWizardPageID, QString strPageTitle, QWidget *parent)
	: WizardPage(nWizardPageID, strPageTitle)
{
	setMinimumSize(QSize(600,450));

	//find/prepare Ini row (per-connection settings)
	m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstOutlookSync.find("Sync_ConnectionName", g_pClientManager->GetConnectionName(), true);
	if(-1 == m_nIniLstRow){
		g_pClientManager->GetIniFile()->m_lstOutlookSync.addRow();
		m_nIniLstRow = g_pClientManager->GetIniFile()->m_lstOutlookSync.getRowCount()-1;
		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_ConnectionName", g_pClientManager->GetConnectionName());
	}
}

void ClientSetupWizPage_Comm::Initialize()
{
	ui.setupUi(this);
	

	m_recResultRecordSet.addColumn(QVariant::String,"COMM");
	m_recResultRecordSet.addRow();
	m_recResultRecordSet.setData(0,0,"");

#ifdef _WIN32
	bool bOutLook=MapiManager::IsOutlookInstalled();

	ui.groupBoxOutlook->setChecked(bOutLook);
	ui.groupBoxSkype->setChecked(SkypeManager::IsSkypeInstalled());

	if (!bOutLook)
	{
		ui.groupBoxOutlook->setCheckable(false);
		ui.groupBoxOutlook->setTitle(tr("Outlook Integration disabled (OUTLOOK NOT INSTALLED)"));
		ui.groupBoxOutlook->setEnabled(false);
	}
	if (!SkypeManager::IsSkypeInstalled())
	{
		ui.groupBoxSkype->setCheckable(false);
		ui.groupBoxSkype->setTitle(tr("Skype Integration disabled (SKYPE NOT INSTALLED)"));
		ui.groupBoxSkype->setEnabled(false);
	}

	//init outlook:
	QStringList lstPersEmails;
	MapiManager::GetPersonalEmails(lstPersEmails, MapiManager::InitGlobalMapi());
	QString strOurEmail = (lstPersEmails.size() > 0)? lstPersEmails.at(0) : "";
#endif

	ui.txtScanPeriod->setText(QString("%1").arg(g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_TimerMin").toInt())); //g_pSettings->GetPersonSetting(OUTLOOK_SETTINGS_TIMER_MIN).toString()
	GetFolderSetting(m_lstOutlookFoldersScan);
	RefreshInfoLabel();


	ui.chkListenSkype->setChecked(g_pSettings->GetPersonSetting(COMM_SETTINGS_LISTEN_SKYPE).toBool());

	m_bComplete = true;
	emit completeStateChanged();

	m_bInitialized = true;
}

ClientSetupWizPage_Comm::~ClientSetupWizPage_Comm()
{

}


void ClientSetupWizPage_Comm::GetFolderSetting(QList<QStringList> &lstFolders)
{
	//load outlook folder selection setting
	//QByteArray  arData  = g_pSettings->GetPersonSetting(OUTLOOK_SETTINGS_SCAN_FOLDERS).toByteArray();
	//OutlookFolderPickerDlg::Binary2FolderList(arData, lstFolders);
	QString strData = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_ScanFolders").toString();
	String2FolderList(strData, lstFolders);
}

void ClientSetupWizPage_Comm::RefreshInfoLabel()
{
	//refresh info
	ui.labelFolders->setText(tr("%1 folders selected").arg(m_lstOutlookFoldersScan.count()));
}


void ClientSetupWizPage_Comm::on_btnSelectFolders_clicked()
{
	OutlookFolderPickerDlg dlg(false, false, false);
	dlg.SetSelection(m_lstOutlookFoldersScan);
	if(dlg.exec()){
		m_lstOutlookFoldersScan = dlg.GetSelection();
		RefreshInfoLabel();
	}
}

bool ClientSetupWizPage_Comm::GetPageResult(DbRecordSet &RecordSet)
{
	g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_Enabled", ui.groupBoxOutlook->isChecked() && ui.groupBoxOutlook->isCheckable()? true : false); //g_pSettings->SetPersonSetting(OUTLOOK_SETTINGS_SUBSCRIBED, ui.groupBoxOutlook->isChecked() && ui.groupBoxOutlook->isCheckable()? true : false);

#ifdef _WIN32
	//save:
	if (ui.groupBoxOutlook->isChecked() && ui.groupBoxOutlook->isCheckable())
	{
		//refresh list of allowed Outlook profiles
		QStringList lstPersEmails;
		MapiManager::GetPersonalEmails(lstPersEmails, MapiManager::InitGlobalMapi());
		QString strOurEmail = (lstPersEmails.size() > 0)? lstPersEmails.at(0) : "";

		if(!strOurEmail.isEmpty())
		{
			QString strSearchEmail  = ";" + strOurEmail;
			strSearchEmail += ";";

			QString strAllowedEmails = g_pClientManager->GetIniFile()->m_lstOutlookSync.getDataRef(m_nIniLstRow, "Sync_AllowedProfiles").toString(); //g_pSettings->GetPersonSetting(OUTLOOK_SETTINGS_WORKSTATIONS).toString();
			if(strAllowedEmails.indexOf(strSearchEmail) >= 0)
			{
				//remove email from allowed if required
				strAllowedEmails.replace(strSearchEmail, ";");
			}
			else
			{
				//add email to allowed if required
				if(true /*ui.chkImport->isChecked()*/)
				{
					if(strAllowedEmails.isEmpty())
						strAllowedEmails += ";";
					strAllowedEmails += strOurEmail;
					strAllowedEmails += ";";
				}
			}
			g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_AllowedProfiles", strAllowedEmails); //g_pSettings->SetPersonSetting(OUTLOOK_SETTINGS_WORKSTATIONS, strAllowedEmails); 
		}

		g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_TimerMin", ui.txtScanPeriod->text().toInt());// g_pSettings->SetPersonSetting(OUTLOOK_SETTINGS_TIMER_MIN, ui.txtScanPeriod->text().toInt());

		QList<QStringList> lstFolders;
		GetFolderSetting(lstFolders);
		if(m_lstOutlookFoldersScan != lstFolders){
			//QByteArray arData;
			//OutlookFolderPickerDlg::FolderList2Binary(m_lstOutlookFoldersScan, arData);
			//g_pSettings->SetPersonSetting(OUTLOOK_SETTINGS_SCAN_FOLDERS, arData);
			QString strData;
			FolderList2String(m_lstOutlookFoldersScan, strData);
			g_pClientManager->GetIniFile()->m_lstOutlookSync.setData(m_nIniLstRow, "Sync_ScanFolders", strData);//SetSettingValue(OUTLOOK_SETTINGS_SCAN_FOLDERS, strData); //arData
		}
	}
#endif

	if (ui.groupBoxSkype->isChecked() && ui.groupBoxSkype->isCheckable())
	{
		g_pSettings->SetPersonSetting(COMM_SETTINGS_LISTEN_SKYPE, ui.chkListenSkype->isChecked()? true : false);
	}
	else
	{
		g_pSettings->SetPersonSetting(COMM_SETTINGS_LISTEN_SKYPE, false);
	}
	

	RecordSet=m_recResultRecordSet;
	return true;
}