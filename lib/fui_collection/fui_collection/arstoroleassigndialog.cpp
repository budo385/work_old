#include "arstoroleassigndialog.h"

ARSToRoleAssignDialog::ARSToRoleAssignDialog(DbRecordSet *RoleRecordSet, QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	setWindowTitle(tr("Assign Selected Access Rights Sets To Multiple Roles"));

	m_RoleRecordSet = RoleRecordSet;
	m_SelectedRoleRecordSet.copyDefinition(*m_RoleRecordSet);

	ui.SelectedRoles_treeWidget->setAcceptDrops(true);
	ui.SelectedRoles_treeWidget->setSelectionMode(QAbstractItemView::ContiguousSelection);
	ui.Role_treeWidget->setDragEnabled(true);
	ui.Role_treeWidget->setSelectionMode(QAbstractItemView::ContiguousSelection);

	ui.OkPushButton->setDisabled(true);

	connect(ui.SelectedRoles_treeWidget, SIGNAL(RoleDropped()),  this, SLOT(on_RoleDropped()));

	InitializeTreeWidget();
}

ARSToRoleAssignDialog::~ARSToRoleAssignDialog()
{

}

DbRecordSet& ARSToRoleAssignDialog::GetSelectedRoles()
{
	return m_SelectedRoleRecordSet;
}

void ARSToRoleAssignDialog::InitializeTreeWidget()
{
	int nRoleCount = m_RoleRecordSet->getRowCount();
	for (int i = 0; i < nRoleCount; ++i)
	{
		QString RoleName = m_RoleRecordSet->getDataRef(i, "CROL_NAME").toString();
		QString RoleDesc = m_RoleRecordSet->getDataRef(i, "CROL_ROLE_DESC").toString();
		int RowID		 = m_RoleRecordSet->getDataRef(i, "CROL_ID").toInt();
		int RoleType	 = m_RoleRecordSet->getDataRef(i, "CROL_ROLE_TYPE").toInt();

		QTreeWidgetItem *Item = new QTreeWidgetItem();
		QString DisplayData = RoleName; //RoleName + " " + RoleDesc;
		//Set display data.
		Item->setData(0, Qt::UserRole,		RowID);
		Item->setData(1, Qt::UserRole,		RoleType);
		Item->setData(2, Qt::UserRole,		RoleDesc);
		Item->setData(0, Qt::DisplayRole,	DisplayData);

		//If system role.
		if (RoleType)
			Item->setIcon(0, QIcon(":MainDataCyBl.jpg"));
		else
			Item->setIcon(0, QIcon(":OptionsCyBl.jpg"));

		m_hshRoleHash.insert(RowID, Item);
		m_hshRoleIDToRoleRowHash.insert(RowID, i);
	}

	//Setup tree.
	ui.Role_treeWidget->addTopLevelItems(m_hshRoleHash.values());
}

void ARSToRoleAssignDialog::on_AddSelected_pushButton_clicked()
{
	QList<QTreeWidgetItem*> selectedItems = ui.Role_treeWidget->selectedItems();
	foreach(QTreeWidgetItem *Item, selectedItems)
		InsertRole(Item);
}

void ARSToRoleAssignDialog::on_AddAll_pushButton_clicked()
{
	ui.Role_treeWidget->selectAll();
	on_AddSelected_pushButton_clicked();
}

void ARSToRoleAssignDialog::on_RemoveSelected_pushButton_clicked()
{
	QList<QTreeWidgetItem*> selectedItems = ui.SelectedRoles_treeWidget->selectedItems();
	foreach(QTreeWidgetItem *Item, selectedItems)
		RemoveRole(Item);
}

void ARSToRoleAssignDialog::on_RemoveAll_pushButton_clicked()
{
	ui.SelectedRoles_treeWidget->selectAll();
	on_RemoveSelected_pushButton_clicked();	
}

void ARSToRoleAssignDialog::InsertRole(QTreeWidgetItem *Item)
{
	int RowID = Item->data(0, Qt::UserRole).toInt();
	if (!m_hshSelectedRoleHash.contains(RowID))
	{
		QTreeWidgetItem *NewItem = new QTreeWidgetItem(*Item);
		m_hshSelectedRoleHash.insert(RowID, NewItem);
		m_RoleRecordSet->clearSelection();
		m_RoleRecordSet->selectRow(m_hshRoleIDToRoleRowHash.value(RowID));
		m_SelectedRoleRecordSet.merge(*m_RoleRecordSet, true);
		ui.SelectedRoles_treeWidget->addTopLevelItem(NewItem);
	}

	OkButtonStateChanged();
}

void ARSToRoleAssignDialog::RemoveRole(QTreeWidgetItem *Item)
{
	int RowID = Item->data(0, Qt::UserRole).toInt();
	Q_ASSERT(m_hshSelectedRoleHash.contains(RowID));
	m_hshSelectedRoleHash.remove(RowID);

	m_SelectedRoleRecordSet.clearSelection();
	m_SelectedRoleRecordSet.find(0, RowID);
	m_SelectedRoleRecordSet.deleteSelectedRows();

	int indexItem = ui.SelectedRoles_treeWidget->indexOfTopLevelItem(Item);
	ui.SelectedRoles_treeWidget->takeTopLevelItem(indexItem);
	delete(Item);

	OkButtonStateChanged();
}

void ARSToRoleAssignDialog::OkButtonStateChanged()
{
	if (m_hshSelectedRoleHash.isEmpty())
		ui.OkPushButton->setDisabled(true);
	else
		ui.OkPushButton->setDisabled(false);
}

void ARSToRoleAssignDialog::on_RoleDropped()
{
	on_AddSelected_pushButton_clicked();
}

void ARSToRoleAssignDialog::on_OkPushButton_clicked()
{
	accept();
}

void ARSToRoleAssignDialog::on_CancelPushButton_clicked()
{
	reject();
}
