#ifndef DOC_MENUWIDGET_H
#define DOC_MENUWIDGET_H

#include <QWidget>
#include <QMenu>
#include "generatedfiles/ui_doc_menuwidget.h"
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QAction>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QUrl>

#include "common/common/observer_ptrn.h"
#include "common/common/dbrecordset.h"
#include "document_piemenu.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"


class Doc_MenuWidget : public QWidget, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Doc_MenuWidget(QWidget *parent = 0,bool bIgnoreViewMode=false);
	~Doc_MenuWidget();

	//Qt::DropActions supportedDropActions() const;
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void SetDefaults(DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL,DbRecordSet *rowQCWContact=NULL);
	void DisableHide(){ui.btnHide->setVisible(false);};

	//void ProcessDocumentDrop(QList<QUrl> &lst, bool bCreateTemplates=false,bool bAcceptApplications=false);
	void SetCloseOnSend();
	bool IsHidden();
	void SetHidden(bool bHide=true);
	void SetSideBarLayout();
	void SetCurrentTab();

public slots:
	void OnRegister();

signals:
	void NeedNewData(int nSubMenuType);		//emited always when new document is about to be created...to assign doc to contacts/projects later


private slots:
	void OnMenuButton();
	void OnDocumentTypeSelected(int nOperation,int nDocType);
	void OnHideTab();

	//template:
	void OnNewFromTemplate();
	void OnNewTask();
	void OnAddTemplate();
	void OnModifyTemplate();
	void OnRemoveTemplate();

	//app:
	//void OnNewFromAppPie();
	void OnNewFromApp();
	void OnAddApp();
	void OnModifyApp();
	void OnRemoveApp();
	void OnUserPaths();

	//chk docs:
	void OnDocCheckIn();
	void OnDocOpen();
	void OnDocOpenFUI();
	void OnDocCancelCheckOut();


	//on change docs:
	void OnCategoryChanged(const QString strCategory);

	//register:
	
	void OnImport();
	
	//reload
	void LoadTemplates(bool bReload=false);
	void LoadApplications(bool bReload=false);
	void LoadCheckedOutDocs(bool bReload=false);
	void LoadCategory(bool bReload=false);

	void ReLoadTemplates(){LoadTemplates(true);}
	void ReLoadApplications(){LoadApplications(true);}
	void ReLoadCheckedOutDocs(){LoadCheckedOutDocs(true);}

	void OnLocalFile();
	void OnInternetFile();
	void OnNote();
	void OnAddress();
	void OnPaperDoc();



protected:
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent * event);

private:
	Ui::Doc_MenuWidgetClass ui;
	
	//import
	//void RegisterDocumentPaths(DbRecordSet &lstPaths,int nIsTemplate=0);
	//void RegisterApplicationPaths(DbRecordSet &lstPaths);
	void OnThemeChanged();
	bool OpenDocumentInExternalApp(int nApplicationID,QString strDocPath,QString strParameters);
	bool WriteDocument(DbRecordSet &lstDocs,DbRecordSet &lstRevisions,bool bOpenDoc=false);
	QTreeWidgetItem* SearchTree(QTreeWidget* pTree, QVariant nUserValue);
	void OpenFUI(int nDocType);
	
	DbRecordSet m_lstTemplates;
	DbRecordSet m_lstApplications;
	DbRecordSet m_lstCheckedOutDocs;
	DbRecordSet m_lstContacts; 
	DbRecordSet m_lstProjects; 
	DbRecordSet m_rowQCWContactDef;					//default QCW list (must have at least 1 row

	int m_nLoggedID;
	//QMenu m_TemplateMenu; 
	bool m_bCloseOnSend;

	Document_PieMenu m_PieMenu;
	MainEntitySelectionController m_Category;
	QPoint m_dragPosition;
};

#endif // DOC_MENUWIDGET_H



