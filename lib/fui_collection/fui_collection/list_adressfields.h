#ifndef LIST_ADRESSFIELDS_H
#define LIST_ADRESSFIELDS_H

#include <QObject>
#include "gui_core/gui_core/listwidget.h"

class List_AdressFields : public ListWidget
{

public:
    List_AdressFields(QWidget *parent);
    ~List_AdressFields();

	void GetDropValue(DbRecordSet &values, bool bSelectItemChildren = false) const;

    
};

#endif // LIST_ADRESSFIELDS_H
