#include "fui_calendar.h"

#include "bus_core/bus_core/globalconstants.h"
#include "bus_core/bus_core/calendarhelpercore.h"
#include "gui_core/gui_core/thememanager.h"
#include <QtWidgets/QMessageBox>
#include "common/common/datahelper.h"
#include "fui_collection/fui_collection/calendarhelper.h"

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "gui_core/gui_core/gui_helper.h"
#include "fui_collection/fui_collection/fui_calendarevent.h"
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/commgridfiltersettings.h"
#include "bus_core/bus_core/commfiltersettingsids.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
#include "bus_client/bus_client/documenthelper.h"
#include "calviewselectdialog.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_core/bus_core/customavailability.h"
#include "bus_core/bus_core/contacttypemanager.h"

#include "bus_core/bus_core/nmrxmanager.h"
#include "calendarmultidaygraphicsview.h"
#include "DefineAvailabilityDlg.h"
#include "bus_core/bus_core/calendarhelpercore.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"

#include "bus_client/bus_client/clientcontactmanager.h"

Fui_Calendar::Fui_Calendar(QWidget *parent)
	: FuiBase(parent)
{
	ui.setupUi(this);

	InitFui();
	g_ChangeManager.registerObserver(this);

	SetFUIName(tr("SOKRATES")+QChar(174)+tr(" Calendar"));

	//Define entities recordset.
	m_recEntites.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	m_recFixedEntites.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	m_recEntitesFromPersonalSetting = m_recEntites;

	m_dlgUserPopUp.Initialize(ENTITY_BUS_PERSON, true);
	dynamic_cast<MainEntitySelectionController*>(m_dlgUserPopUp.GetSelectorWidget())->ShowOnlyUserContacts();
	m_dlgProjectPopUp.Initialize(ENTITY_BUS_PROJECT, true);
	m_dlgContactPopUp.Initialize(ENTITY_BUS_CONTACT, true);
	m_dlgResourcePopUp.Initialize(ENTITY_CALENDAR_RESOURCES, true);

	ui.frameUser->Initialize(ENTITY_BUS_PERSON);
	ui.frameUser->GetSelectionController()->ShowOnlyUserContacts();
	ui.frameProject->Initialize(ENTITY_BUS_PROJECT);
	ui.frameContact->Initialize(ENTITY_BUS_CONTACT);
	ui.frameResource->Initialize(ENTITY_CALENDAR_RESOURCES);
	ui.frameProject->registerObserver(this);
	ui.frameContact->registerObserver(this);
	ui.frameResource->registerObserver(this);
	ui.frameUser->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setVisible(false);
	ui.frameProject->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setVisible(false);
	ui.frameContact->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setVisible(false);
	ui.frameProject->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setVisible(false);
	
	ui.up_toolButton->setIconSize(QSize(29,26));
	ui.up_toolButton->setAutoRaise(true);
	ui.up_toolButton->setIcon(QIcon(":Up_Calendar.png"));
	ui.down_toolButton->setIconSize(QSize(29,26));
	ui.down_toolButton->setAutoRaise(true);
	ui.down_toolButton->setIcon(QIcon(":Down_Calendar.png"));

	ui.addUser_toolButton->setIconSize(QSize(36,16));
	ui.addUser_toolButton->setFlat(true);
	ui.addUser_toolButton->setIcon(QIcon(":Add_User_Calendar16.png"));

	ui.addContact_toolButton->setIconSize(QSize(36,16));
	ui.addContact_toolButton->setFlat(true);
	ui.addContact_toolButton->setIcon(QIcon(":Add_Contact_Calendar16.png"));

	ui.addProject_toolButton->setIconSize(QSize(36,16));
	ui.addProject_toolButton->setFlat(true);
	ui.addProject_toolButton->setIcon(QIcon(":Add_Project_Calendar16.png"));

	ui.addResource_toolButton->setIconSize(QSize(36,16));
	ui.addResource_toolButton->setFlat(true);
	ui.addResource_toolButton->setIcon(QIcon(":Add_Ressource_Calendar16.png"));

	ui.delete_toolButton->setIconSize(QSize(24,24));
	ui.delete_toolButton->setAutoRaise(true);
	ui.delete_toolButton->setIcon(QIcon(":Remove_Calendar24.png"));
	ui.delete_toolButton->setToolTip(tr("Hide Calendar"));

	ui.reload_toolButton->setIconSize(QSize(32,32));
	ui.reload_toolButton->setIcon(QIcon(":reload32.png"));
	ui.reload_toolButton->setToolTip(tr("Reload Calendar"));
	ui.reload_toolButton->setAutoRaise(true);
	
	ui.saveView_toolButton->setIconSize(QSize(32,32));
	ui.saveView_toolButton->setIcon(QIcon(":Remember_Layout.png"));
	ui.saveView_toolButton->setToolTip(tr("Save Calendar View"));
	ui.saveView_toolButton->setAutoRaise(true);

	ui.deleteView_toolButton->setIconSize(QSize(24,24));
	ui.deleteView_toolButton->setIcon(QIcon(":Remove_Calendar24.png"));
	ui.deleteView_toolButton->setToolTip(tr("Delete Selected Calendar View"));
	ui.deleteView_toolButton->setAutoRaise(true);

	ui.showAssignations_toolButton->setIconSize(QSize(24,24));
	ui.showAssignations_toolButton->setIcon(QIcon(":2dowarrow.png"));
	ui.showAssignations_toolButton->setToolTip(tr("Show Assignations"));
	ui.showAssignations_toolButton->setAutoRaise(true);
	
	ui.viewSelect_comboBox->setMaxVisibleItems(30);

	SetMode(FuiBase::MODE_READ);

	//Left - right splitter.
	QList<int> sizes;
	sizes << 200 << 400;
	ui.splitter->setSizes(sizes);
	ui.splitter->setStretchFactor(1, 10);

	LoadViewSettings();

	//Date frame.
	ui.dateFilter_frame->Initialize(m_datFrom, m_datTo, m_nDateRange, true);
	ui.dateFilter_frame->DisableDateRange(DataHelper::YEAR); //B.T. 04.09.2013 issue 2723
	//ui.dateFilter_frame->UseAsFocusOut(true);

	ui.frameUser->registerObserver(this);
	connect(ui.rangeFrame, SIGNAL(RangeChanged(int)), this, SLOT(on_rangeFrameRangeTimeIntervalChanged(int)));
	connect(ui.rangeFrame, SIGNAL(DateChanged(QDate)), this, SLOT(on_DateChanged(QDate)));
	connect(ui.dateFilter_frame, SIGNAL(onDateChanged(QDateTime &, QDateTime &, int, bool)), this, SLOT(on_onDateChanged(QDateTime &, QDateTime &, int, bool)));
	connect(ui.calendarWidget, SIGNAL(ItemResized(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)), this, SLOT(on_ItemResized(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)));
	connect(ui.calendarWidget, SIGNAL(MultiDayItemResized(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)), this, SLOT(on_MultiDayItemResized(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)));
	connect(ui.calendarWidget, SIGNAL(ItemMoved(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)), this, SLOT(on_ItemMoved(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)));
	connect(ui.calendarWidget, SIGNAL(MultiDayItemMoved(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)), this, SLOT(on_MultyDayItemMoved(QDateTime, QDateTime, int, int, int, int, int, int, QDateTime, QDateTime)));
	connect(ui.calendarWidget, SIGNAL(DeleteItem(int, int, int, int, int, int)), this, SLOT(on_DeleteItem(int, int, int, int, int, int)));
	connect(ui.calendarWidget, SIGNAL(ItemsDelete(DbRecordSet)), this, SLOT(on_ItemsDelete(DbRecordSet)));
	connect(ui.calendarWidget, SIGNAL(MultiDayDeleteItem(int, int, int, int)), this, SLOT(on_MultyDayDeleteItem(int, int, int, int)));
	connect(ui.calendarWidget, SIGNAL(ModifyItem(int, int, int, int)), this, SLOT(on_ModifyItem(int, int, int, int)));
	connect(ui.calendarWidget, SIGNAL(ViewItem(int, int, int, int)), this, SLOT(on_ViewItem(int, int, int, int)));

	connect(ui.calendarWidget, SIGNAL(DropEntityOnItem(int, int, int, int, int, int, int, bool)), this, SLOT(on_DropEntityOnItem(int, int, int, int, int, int, int, bool)));
	connect(ui.calendarWidget, SIGNAL(tableCellDoubleClicked(QDateTime, QDateTime, int, int)), this, SLOT(on_InsertNewItem(QDateTime, QDateTime, int, int)));
	connect(ui.calendarWidget, SIGNAL(selectionChanged(QDateTime, QDateTime, DbRecordSet)), this, SLOT(on_selectionChanged(QDateTime, QDateTime, DbRecordSet)));
	connect(ui.calendarWidget, SIGNAL(openMultiDayEvent(QHash<int, int>, QHash<int, int>, QHash<int, int>)), this, SLOT(on_openMultiDayEvent(QHash<int, int>, QHash<int, int>, QHash<int, int>)));
	connect(ui.calendarWidget, SIGNAL(TaskItemClicked(int, int, int, QDateTime, bool, int, int, int)), this, SLOT(on_TaskItemClicked(int, int, int, QDateTime, bool, int, int, int)));
	connect(ui.calendarWidget, SIGNAL(TaskItemDoubleClicked(int, int, int, QDateTime, bool, int, int, int)), this, SLOT(on_TaskItemDoubleClicked(int, int, int, QDateTime, bool, int, int, int)));
	connect(ui.calendarWidget, SIGNAL(defineAvailability(DbRecordSet)), this, SLOT(on_defineAvailability(DbRecordSet)));
	connect(ui.calendarWidget, SIGNAL(markAsFinal(DbRecordSet)), this, SLOT(on_markAsFinal(DbRecordSet)));
	connect(ui.calendarWidget, SIGNAL(markAsPreliminary(DbRecordSet)), this, SLOT(on_markAsPreliminary(DbRecordSet)));
	connect(ui.calendarWidget, SIGNAL(dropEntityOnTable(int, int, int, int, int, int, QDateTime, QDateTime)), this, SLOT(on_dropEntityOnTable(int, int, int, int, int, int, QDateTime, QDateTime)));

	//Show preliminary and information only events.
	connect(ui.rangeFrame, SIGNAL(ShowInformationEvents(bool)), this, SLOT(on_ShowInformationEvents(bool)));
	connect(ui.rangeFrame, SIGNAL(ShowPreliminaryEvents(bool)), this, SLOT(on_ShowPreliminaryEvents(bool)));
	connect(ui.rangeFrame, SIGNAL(FilterTypeChanged(QList<int>)), this, SLOT(on_FilterTypeChanged(QList<int>)));

	//Style sheets.
	ui.calendar_frame->setStyleSheet("QFrame#calendar_frame "+ThemeManager::GetSidebarChapter_Bkg());
	ui.addContact_frame->setStyleSheet("QFrame#addContact_frame "+ThemeManager::GetSidebarChapter_Bkg());
	
	ui.Calendar_label->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font("bold", "italic", "12"));
	ui.userCount_label->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font("bold", "normal", "12"));
	ui.resource_label->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font("bold", "normal", "12"));
	ui.dateFilter_frame->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font("normal", "normal", "10"));
	ui.dateSelector_label->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font("normal", "normal", "10"));
	
	ui.addUser_toolButton->setStyleSheet("QPushButton#addUser_toolButton " + ThemeManager::GetSidebarChapter_Font("bold", "normal", "10"));
	ui.addProject_toolButton->setStyleSheet("QPushButton#addProject_toolButton " + ThemeManager::GetSidebarChapter_Font("bold", "normal", "10"));
	ui.addContact_toolButton->setStyleSheet("QPushButton#addContact_toolButton " + ThemeManager::GetSidebarChapter_Font("bold", "normal", "10"));
	ui.addResource_toolButton->setStyleSheet("QPushButton#addResource_toolButton " + ThemeManager::GetSidebarChapter_Font("bold", "normal", "10"));

	//BT: enable/disable selector based on FP:
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_PROJECT_CALENDARS))
	{
		ui.addProject_toolButton->setEnabled(false);
	}
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_RESOURCE_CALENDARS))
	{
		ui.addResource_toolButton->setEnabled(false);
	}
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS))
	{
		ui.addContact_toolButton->setEnabled(false);
	}
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_OTHER_USER_S_CALENDARS))
	{
		ui.addUser_toolButton->setEnabled(false);
	}
	
	DbRecordSet recPerson;
	if (!m_recEntitesFromPersonalSetting.getRowCount())
	{
		//Get logged person recordset.
		int nLoggedPersonID = g_pClientManager->GetPersonID();
		ui.frameUser->blockSignals(true);
		ui.frameUser->SetCurrentEntityRecord(nLoggedPersonID);
		ui.frameUser->GetCurrentEntityRecord(nLoggedPersonID, recPerson);
		ui.frameUser->blockSignals(false);
	}

	m_bResizeColumnsToFit = true;
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__USE_PERSONAL_CALENDAR))
		Initialize(m_datFrom.date(), m_datTo.date());
	else
	{
		if (!m_recEntitesFromPersonalSetting.getRowCount())
			Initialize(m_datFrom.date(), m_datTo.date(), &recPerson);
		else
			Initialize(m_datFrom.date(), m_datTo.date(), NULL, NULL, NULL, NULL, &m_recEntitesFromPersonalSetting);
	}
}

Fui_Calendar::~Fui_Calendar()
{
	int nViewID = ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex()).toInt();
	g_pSettings->SetPersonSetting(CALENDAR_VIEW_SETTINGS, nViewID);
}

void Fui_Calendar::Initialize(QDate startDate, QDate endDate, DbRecordSet *recPersons, DbRecordSet *recProjects, DbRecordSet *recContacts, DbRecordSet *recResources, DbRecordSet *m_recEntitesFromPersonalSetting)
{
	QTime timer;
	timer.start();
	qDebug() << " /+++++++++++++ Initialize() " << timer.elapsed();

	m_recEntites.clear();
	m_recFixedEntites.clear();

	//If loaded from personal settings then go directly.
	if (m_recEntitesFromPersonalSetting)
	{
		m_recFixedEntites = *m_recEntitesFromPersonalSetting;
		//_DUMP(m_recFixedEntites);
		ReorderEntitiesRecordSet();
	}
	else
	{
		//Fill hashes.
		if (recPersons)
		{
			int nRowCount = recPersons->getRowCount();
			for (int i = 0; i < nRowCount; i++)
			{
				int nId = recPersons->getDataRef(i, "BPER_ID").toInt();
				int nContactId = recPersons->getDataRef(i, "BPER_CONTACT_ID").toInt();
				QString strName = recPersons->getDataRef(i, "BPER_NAME").toString();
				addEntity(nContactId, GlobalConstants::TYPE_CAL_USER_SELECT, strName, nId);
			}
		}
		if (g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_PROJECT_CALENDARS))
		{
			if (recProjects)
			{
				int nRowCount = recProjects->getRowCount();
				for (int i = 0; i < nRowCount; i++)
				{
					int nId = recProjects->getDataRef(i, "BUSP_ID").toInt();
					QString strName = recProjects->getDataRef(i, "BUSP_NAME").toString();
					addEntity(nId, GlobalConstants::TYPE_CAL_PROJECT_SELECT, strName);
				}
			}
		}
		if (g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_CONTACT_CALENDARS))
		{
			if (recContacts)
			{
				//recContacts->Dump();
				int nRowCount = recContacts->getRowCount();
				for (int i = 0; i < nRowCount; i++)
				{
					int nId = recContacts->getDataRef(i, "BCNT_ID").toInt();
					QString strName = recContacts->getDataRef(i, "BCNT_NAME").toString();
					addEntity(nId, GlobalConstants::TYPE_CAL_CONTACT_SELECT, strName);
				}
			}
		}
		if (g_FunctionPoint.IsFPAvailable(FP_CALENDAR__VIEW_RESOURCE_CALENDARS))
		{
			if (recResources)
			{
				int nRowCount = recResources->getRowCount();
				for (int i = 0; i < nRowCount; i++)
				{
					int nId = recResources->getDataRef(i, "BRES_ID").toInt();
					QString strName = recResources->getDataRef(i, "BRES_NAME").toString();
					addEntity(nId, GlobalConstants::TYPE_CAL_RESOURCE_SELECT, strName);
				}
			}
		}
	}


	//B.T.: fixed crash when m_nCurentRow > out of boundaries of m_recFixedEntites
	if (m_nCurentRow>=m_recFixedEntites.getRowCount())
		m_nCurentRow=m_recFixedEntites.getRowCount()-1;
	if (m_nCurentRow<0)
		m_nCurentRow=0;
	

	//_DUMP(m_recFixedEntites);
	//_DUMP(m_recEntites);

	ui.dateFilter_frame->Initialize(m_datFrom, m_datTo, m_nDateRange);
	ui.dateFilter_frame->DisableDateRange(DataHelper::YEAR); //B.T. 04.09.2013 issue 2723
	SetupDateIntervalLabel(m_nDateRange);
	
	qDebug() << " /+++++++++++++ Initialize - before InitializeCalendarWidget " << timer.elapsed();

	InitializeCalendarWidgetAndGetCalendarItems();

//		InitializeCalendarWidget();

	qDebug() << " /+++++++++++++ Initialize - after InitializeCalendarWidget " << timer.elapsed();

//		GetCalendarItems();

	//_DUMP(m_recFixedEntites);
	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
	
	enableDisableButtons();
	setLabelsAndSapne();

	if (m_bShowTasks)
		ui.frame_2->Reload(&m_recFixedEntites);

	qDebug() << " /+++++++++++++ Initialize (2) " << timer.elapsed();
}

void Fui_Calendar::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail/*=0*/,const QVariant val/*=QVariant()*/)
{
	if(nMsgDetail==ENTITY_BUS_PERSON)
	{
		//DbRecordSet rec = val.value<DbRecordSet>();
		//_DUMP(rec);
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED || nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			LoadViewSettings(m_nViewID);
			//m_bResizeColumnsToFit = true;
			Initialize(m_datFrom.date(), m_datTo.date(), NULL, NULL, NULL, NULL, &m_recEntitesFromPersonalSetting);
		}
	}
	if(nMsgDetail==ENTITY_BUS_PROJECT)
	{
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED || nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD) 
		{
			LoadViewSettings(m_nViewID);
			//m_bResizeColumnsToFit = true;
			Initialize(m_datFrom.date(), m_datTo.date(), NULL, NULL, NULL, NULL, &m_recEntitesFromPersonalSetting);
		}
	}
	if(nMsgDetail==ENTITY_BUS_CONTACT)
	{
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED || nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD) 
		{
			LoadViewSettings(m_nViewID);
			//m_bResizeColumnsToFit = true;
			Initialize(m_datFrom.date(), m_datTo.date(), NULL, NULL, NULL, NULL, &m_recEntitesFromPersonalSetting);
		}
	}
	if(nMsgDetail==ENTITY_CALENDAR_RESOURCES)
	{
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED || nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD) 
		{
			LoadViewSettings(m_nViewID);
			//m_bResizeColumnsToFit = true;
			Initialize(m_datFrom.date(), m_datTo.date(), NULL, NULL, NULL, NULL, &m_recEntitesFromPersonalSetting);
		}
	}

	if(nMsgDetail==ENTITY_CALENDAR_EVENT)
	{
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_CALENDAR_ENTITY_INSERTED_AFTER_DRAG_DROP)
		{
			int nFirstEntityID=m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt();
			int nFirstEntityType=m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt();

			DbRecordSet rec = val.value<DbRecordSet>();
			//_DUMP(rec);

			DbRecordSet lstAssignmnets = rec.getDataRef(0,"ASSIGNMENTS").value<DbRecordSet>();
			//_DUMP(lstAssignmnets);

			int nDroppedEntityID, nDroppedEntityType, nDroppedEntityPersonID;
			int nAssRowCount=lstAssignmnets.getRowCount();
			for (int i=0; i<nAssRowCount; i++)
			{
				int nASSEntityID=lstAssignmnets.getDataRef(i, "ENTITY_ID").toInt();
				int nASSEntityType=lstAssignmnets.getDataRef(i, "ENTITY_TYPE").toInt();
				int nASSEntityPersonID=lstAssignmnets.getDataRef(i, "ENTITY_PERSON_ID").toInt();

				if (nASSEntityID==nFirstEntityID && nASSEntityType==nFirstEntityType)
				{
					continue;
				}
				else
				{
					nDroppedEntityID=nASSEntityID;
					nDroppedEntityType=nASSEntityType;
					nDroppedEntityPersonID=nASSEntityPersonID;
					break;
				}
			}

			setDroppedEntityOnSecondRow(nDroppedEntityID, nDroppedEntityType, nDroppedEntityPersonID);

			InitializeCalendarWidgetAndGetCalendarItems(true,lstAssignmnets,true);
			//GetCalendarItems(true,lstAssignmnets);

			if (m_recFixedEntites.getRowCount()>1)
				ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());

			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_CALENDAR_VIEW_LIST_RELOAD)
		{
			int nLoggedPersonID = g_pClientManager->GetPersonID();
			Status err;
			_SERVER_CALL(BusCalendar->GetCalFilterViews(err, m_recCalendarViews, nLoggedPersonID, -1));
			_CHK_ERR(err);

			LoadViewComboBox(m_nViewID);

			return;
		}
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED)
		{
			DbRecordSet rec = val.value<DbRecordSet>();
			//_DUMP(rec);

			DbRecordSet lstAssignmnets = rec.getDataRef(0,"ASSIGNMENTS").value<DbRecordSet>();
			//_DUMP(lstAssignmnets);

			InitializeCalendarWidgetAndGetCalendarItems(true,lstAssignmnets,true);
			//GetCalendarItems(true,lstAssignmnets);

			if (m_recFixedEntites.getRowCount()>1)
				ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());

			return;
		}
		else if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED)
		{
			DbRecordSet rec = val.value<DbRecordSet>();
//			//_DUMP(rec);
			//InitializeCalendarWidget();
			InitializeCalendarWidgetAndGetCalendarItems(false,DbRecordSet(),true);
//			GetCalendarItems();

			if (m_recFixedEntites.getRowCount()>1)
				ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
			
			return;
		}
		else if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED)
		{
			DbRecordSet rec = val.value<DbRecordSet>();
			//_DUMP(rec);
			if (!rec.getRowCount())
				return;
			int nBcevID = rec.getDataRef(0, "BCEV_ID").toInt();
			//if(rec.getColumnIdx("PARTS")>=0)
			//{
			//	QByteArray bytParts = rec.getDataRef(0, "PARTS").toByteArray();
			//	DbRecordSet recParts = XmlUtil::ConvertByteArray2RecordSet_Fast(bytParts);
			//	_DUMP(recParts);
			//}
			//if (m_recEntites.getRowCount()>1)
			//{
			InitializeCalendarWidgetAndGetCalendarItems();
//				InitializeCalendarWidget();
//				GetCalendarItems();

				if (m_recFixedEntites.getRowCount()>1)
					ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
			//}
			//else
			//	ModifyOneEvent(nBcevID);
			return;
		}
		else if (nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			//LoadViewSettings(m_nViewID);
			InitializeCalendarWidgetAndGetCalendarItems();
//			InitializeCalendarWidget();
//			GetCalendarItems();

			if (m_recFixedEntites.getRowCount()>1)
				ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
			return;
		}
	}

	int nEntityId;
	QString strEntityName;

	if (pSubject==ui.frameUser && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		DbRecordSet rec;
		int nPersonID;
		ui.frameUser->GetCurrentEntityRecord(nPersonID, rec);
		//_DUMP(rec);
		if (nPersonID<=0)
		{
			return;
		}
		nEntityId = rec.getDataRef(0, "BPER_CONTACT_ID").toInt();
		strEntityName = rec.getDataRef(0, "BPER_NAME").toString();
		if (nEntityId <= 0)
			strEntityName = "";
		if (!EntityAlreadyExist(nEntityId, GlobalConstants::TYPE_CAL_USER_SELECT))
		{
			removeCurrentEntity();
			addEntity(nEntityId, GlobalConstants::TYPE_CAL_USER_SELECT, strEntityName, nPersonID);
		}
	}
	else if (pSubject==ui.frameProject && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		ui.frameProject->GetCurrentEntityRecord(nEntityId);
		if (nEntityId<=0)
		{
			return;
		}

		strEntityName = ui.frameProject->GetCurrentDisplayName();
		if (!EntityAlreadyExist(nEntityId, GlobalConstants::TYPE_CAL_PROJECT_SELECT))
		{
			removeCurrentEntity();
			addEntity(nEntityId, GlobalConstants::TYPE_CAL_PROJECT_SELECT, strEntityName);
		}
	}
	else if (pSubject==ui.frameContact && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		ui.frameContact->GetCurrentEntityRecord(nEntityId);
		if (nEntityId<=0)
		{
			return;
		}
		
		strEntityName = ui.frameContact->GetCurrentDisplayName();
		if (!EntityAlreadyExist(nEntityId, GlobalConstants::TYPE_CAL_CONTACT_SELECT))
		{
			removeCurrentEntity();
			addEntity(nEntityId, GlobalConstants::TYPE_CAL_CONTACT_SELECT, strEntityName);
		}
	}
	else if (pSubject==ui.frameResource && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		ui.frameResource->GetCurrentEntityRecord(nEntityId);
		if (nEntityId<=0)
		{
			return;
		}

		strEntityName = ui.frameResource->GetCurrentDisplayName();
		if (!EntityAlreadyExist(nEntityId, GlobalConstants::TYPE_CAL_RESOURCE_SELECT))
		{
			removeCurrentEntity();
			addEntity(nEntityId, GlobalConstants::TYPE_CAL_RESOURCE_SELECT, strEntityName);
		}
	}

	////InitializeCalendarWidget();
	////GetCalendarItems();
}

void Fui_Calendar::showEvent(QShowEvent *event)
{
	if (!m_ptFuiPosition.isNull())
		move(m_ptFuiPosition);
}

void Fui_Calendar::reorderEntitiesForAnesthesists()
{
	//_DUMP(m_recEntites);
	DbRecordSet recFilter;
	GetFilterRecordset(recFilter);
	Status err;
	_SERVER_CALL(BusCalendar->ReadEventPartsCountForEntities(err, m_recEntites, recFilter, m_datFrom, m_datTo));
	_CHK_ERR(err);
}

void Fui_Calendar::InitializeCalendarWidgetAndGetCalendarItems(bool bForInsert /*= false*/, DbRecordSet recEntites /*= DbRecordSet()*/, bool bNoInitialize /*= false*/)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);

	QTime timer;
	timer.start();
	qDebug() << " /+++++++++++++ prije IsAvailable_AS_CalendarViewWizard() " << timer.elapsed();
	if(CustomAvailability::IsAvailable_AS_CalendarViewWizard() && m_nCalendarViewSections==1 && !bForInsert)
	{
		//bNoInitialize=false;
		reorderEntitiesForAnesthesists();
	}
	qDebug() << " /+++++++++++++ poslije IsAvailable_AS_CalendarViewWizard() " << timer.elapsed();

	//************************** Initialize Calendar part. ********************************
	if (!bNoInitialize)
	{
		ui.calendarWidget->Initialize(m_datFrom.date(), m_datTo.date(), m_TimeScale, &m_recEntites, m_bResizeColumnsToFit, m_startScrollToTime, m_nCalendarViewSections);
		m_bResizeColumnsToFit = false;
	}


	//************************** Get Calendar items part. ********************************
	//_DUMP(m_recEntites);
	//_DUMP(recEntites);

	//Do this only if recEntites not empty - get items only for some entities.
	m_recEntites.clearSelection();
	int nRowCount = m_recEntites.getRowCount();
	int nColCount= recEntites.getColumnCount();
	if (nColCount)
	{
		for (int i = 0; i < nRowCount; i++)
		{
			recEntites.clearSelection();

			int nEntityId = m_recEntites.getDataRef(i, "ENTITY_ID").toInt();
			int nEntityType = m_recEntites.getDataRef(i, "ENTITY_TYPE").toInt();
			int nEntityPersonID = m_recEntites.getDataRef(i, "ENTITY_PERSON_ID").toInt();

			recEntites.find("ENTITY_ID", nEntityId);
			//If contact look also for users
			if (nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT || nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
			{
				//For contacts.
				nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;
				recEntites.find("ENTITY_TYPE", nEntityType, false, true, true);
				if (recEntites.getSelectedCount())
				{
					m_recEntites.selectRow(i);
				}

				//For users.
				recEntites.clearSelection();
				recEntites.find("ENTITY_ID", nEntityId);
				nEntityType = GlobalConstants::TYPE_CAL_USER_SELECT;
				recEntites.find("ENTITY_TYPE", nEntityType, false, true, true);
				if (recEntites.getSelectedCount())
				{
					m_recEntites.selectRow(i);
				}
			}
			else
			{
				recEntites.find("ENTITY_TYPE", nEntityType, false, true, true);
				if (recEntites.getSelectedCount())
				{
					m_recEntites.selectRow(i);
				}
			}
		}
	}

	QList<int> lstSelRows = m_recEntites.getSelectedRows();

	//Get Calendar events.
	DbRecordSet recParts, recContacts, recResources, recProjects, recBrakes;
	Status err;

	DbRecordSet recFilter;
	GetFilterRecordset(recFilter);

	for (int i = 0; i < nRowCount; i++)
	{
		if (nColCount)
		{
			if (!lstSelRows.contains(i))
			{
				continue;
			}
		}

		int nEntityId = m_recEntites.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = m_recEntites.getDataRef(i, "ENTITY_TYPE").toInt();
		int nEntityPersonID = m_recEntites.getDataRef(i, "ENTITY_PERSON_ID").toInt();
		int nTrueEntityType = nEntityType;
		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
			nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;
		
		qDebug() << " /+++++++++++++ prije ReadEventParts() " << timer.elapsed();
		_SERVER_CALL(BusCalendar->ReadEventParts(err, recFilter, nEntityType, nEntityId, m_datFrom, m_datTo, recParts, recContacts, recResources, recProjects, recBrakes));
		_CHK_ERR(err);
		qDebug() << " /+++++++++++++ poslije ReadEventParts() " << timer.elapsed();

		//qDebug()<< "parts";
		//_DUMP(recParts);
		//qDebug()<< "contacts";
		//_DUMP(recContacts);
		//qDebug()<< "projects";
		//_DUMP(recProjects);
		//qDebug()<< "brakes";
		//_DUMP(recBrakes);

		SortCalendarItems(nEntityId, nTrueEntityType, nEntityPersonID, recParts, recContacts, recResources, recProjects, recBrakes, bForInsert);
	}

	//Get calendar reservations.
	if (!bForInsert)
	{
		for (int i = 0; i < nRowCount; i++)
		{
			int nEntityId = m_recEntites.getDataRef(i, "ENTITY_ID").toInt();
			int nEntityType = m_recEntites.getDataRef(i, "ENTITY_TYPE").toInt();
			int nTrueEntityType = nEntityType;
			if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
				nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;

			DbRecordSet recReservations;
			DbRecordSet recReservationsRecalculated;
			Status err;
			_SERVER_CALL(BusCalendar->ReadReservation(err,nEntityType,nEntityId,m_datFrom,m_datTo,recReservations));
			_CHK_ERR(err);
			CalendarHelper::RecalcReservationList(m_datFrom,m_datTo,recReservations,recReservationsRecalculated);

			//recReservations.Dump();
			//recReservationsRecalculated.Dump();

			AddCalendarReservations(nEntityId, nTrueEntityType, recReservationsRecalculated);
		}
	}

	QApplication::restoreOverrideCursor();

	//QTime timer;
	//timer.start();
	//qDebug() << " /+++++++++++++ prije AddCalendarTaskItems() " << timer.elapsed();
	//AddCalendarTaskItems();
	//qDebug() << " /+++++++++++++ poslije AddCalendarTaskItems() " << timer.elapsed();
}

/*void Fui_Calendar::InitializeCalendarWidget()
{
	QApplication::setOverrideCursor(Qt::WaitCursor);

	ui.calendarWidget->Initialize(m_datFrom.date(), m_datTo.date(), m_TimeScale, &m_recEntites, m_bResizeColumnsToFit, m_startScrollToTime, m_nCalendarViewSections);
	m_bResizeColumnsToFit = false;

	QApplication::restoreOverrideCursor();
}*/

void Fui_Calendar::GetCalendarData(int nRow, DbRecordSet &recEventParts, QList<int> &lstMultiPartEventsIDs, QDateTime &startDateTime, 
								   QDateTime &endDateTime, int &nCentID, int &nBcepID, int &nBcevID, int &nBCOL_ID, int &nPersonID, 
								   int &nContactID, QString &strTitle, QString &strLocation, QString &strColor, QPixmap &pixIcon, int &nBcep_Status, 
								   int &nBCEV_IS_INFORMATION)
{
	//_DUMP(recEventParts);
		startDateTime = recEventParts.getDataRef(nRow, "BCOL_FROM").toDateTime();
		endDateTime = recEventParts.getDataRef(nRow, "BCOL_TO").toDateTime();
		nCentID = recEventParts.getDataRef(nRow, "CENT_ID").toInt();
		nBcepID = recEventParts.getDataRef(nRow, "BCEP_ID").toInt();
		nBcep_Status = recEventParts.getDataRef(nRow, "BCEP_STATUS").toInt();
		nBCEV_IS_INFORMATION = recEventParts.getDataRef(nRow, "BCEV_IS_INFORMATION").toInt();
		nBcevID = recEventParts.getDataRef(nRow, "BCEV_ID").toInt();
		nBCOL_ID = recEventParts.getDataRef(nRow, "BCOL_ID").toInt();
		nPersonID = recEventParts.getDataRef(nRow, "CENT_OWNER_ID").toInt();
		nContactID = recEventParts.getDataRef(nRow, "BPER_CONTACT_ID").toInt();
		//If multi part event put as a title event part title.
		if (lstMultiPartEventsIDs.contains(nBcevID))
			strTitle = recEventParts.getDataRef(nRow, "BCOL_SUBJECT").toString();
		else
			strTitle = recEventParts.getDataRef(nRow, "BCEV_TITLE").toString();
		strLocation = recEventParts.getDataRef(nRow, "BCOL_LOCATION").toString();
		strColor = recEventParts.getDataRef(nRow, "BCMT_COLOR").toString();
		pixIcon.loadFromData(recEventParts.getDataRef(nRow, "CET_ICON_BINARY").toByteArray());
}

/*void Fui_Calendar::GetCalendarItems(bool bForInsert = false, DbRecordSet recEntites = DbRecordSet())
{
//	if (true)
//		return;
	QApplication::setOverrideCursor(Qt::WaitCursor);

	//_DUMP(m_recEntites);
	//_DUMP(recEntites);

	m_recEntites.clearSelection();
	int nRowCount = m_recEntites.getRowCount();
	int nColCount= recEntites.getColumnCount();
	if (nColCount)
	{
		for (int i = 0; i < nRowCount; i++)
		{
			recEntites.clearSelection();

			int nEntityId = m_recEntites.getDataRef(i, "ENTITY_ID").toInt();
			int nEntityType = m_recEntites.getDataRef(i, "ENTITY_TYPE").toInt();
			int nEntityPersonID = m_recEntites.getDataRef(i, "ENTITY_PERSON_ID").toInt();

			recEntites.find("ENTITY_ID", nEntityId);
			//If contact look also for users
			if (nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT || nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
			{
				//For contacts.
				nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;
				recEntites.find("ENTITY_TYPE", nEntityType, false, true, true);
				if (recEntites.getSelectedCount())
				{
					m_recEntites.selectRow(i);
				}
				
				//For users.
				recEntites.clearSelection();
				recEntites.find("ENTITY_ID", nEntityId);
				nEntityType = GlobalConstants::TYPE_CAL_USER_SELECT;
				recEntites.find("ENTITY_TYPE", nEntityType, false, true, true);
				if (recEntites.getSelectedCount())
				{
					m_recEntites.selectRow(i);
				}
			}
			else
			{
				recEntites.find("ENTITY_TYPE", nEntityType, false, true, true);
				if (recEntites.getSelectedCount())
				{
					m_recEntites.selectRow(i);
				}
			}
		}
	}
	
	QList<int> lstSelRows = m_recEntites.getSelectedRows();

	//Get Calendar events.
	DbRecordSet recParts, recContacts, recResources, recProjects, recBrakes;
	Status err;
	
	DbRecordSet recFilter;
	GetFilterRecordset(recFilter);

	for (int i = 0; i < nRowCount; i++)
	{
		if (nColCount)
		{
			if (!lstSelRows.contains(i))
			{
				continue;
			}
		}
	
		int nEntityId = m_recEntites.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = m_recEntites.getDataRef(i, "ENTITY_TYPE").toInt();
		int nEntityPersonID = m_recEntites.getDataRef(i, "ENTITY_PERSON_ID").toInt();
		int nTrueEntityType = nEntityType;
		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
			nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;

		_SERVER_CALL(BusCalendar->ReadEventParts(err, recFilter, nEntityType, nEntityId, m_datFrom, m_datTo, recParts, recContacts, recResources, recProjects, recBrakes));
		_CHK_ERR(err);
		
		qDebug()<< "parts";
		//_DUMP(recParts);
		qDebug()<< "contacts";
		//_DUMP(recContacts);
		qDebug()<< "projects";
		//_DUMP(recProjects);
		qDebug()<< "brakes";
		//_DUMP(recBrakes);

		SortCalendarItems(nEntityId, nTrueEntityType, nEntityPersonID, recParts, recContacts, recResources, recProjects, recBrakes, bForInsert);
	}

	//Get calendar reservations.
	if (!bForInsert)
	{
		for (int i = 0; i < nRowCount; i++)
		{
			int nEntityId = m_recEntites.getDataRef(i, "ENTITY_ID").toInt();
			int nEntityType = m_recEntites.getDataRef(i, "ENTITY_TYPE").toInt();
			int nTrueEntityType = nEntityType;
			if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
				nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;

			DbRecordSet recReservations;
			DbRecordSet recReservationsRecalculated;
			Status err;
			_SERVER_CALL(BusCalendar->ReadReservation(err,nEntityType,nEntityId,m_datFrom,m_datTo,recReservations));
			_CHK_ERR(err);
			CalendarHelper::RecalcReservationList(m_datFrom,m_datTo,recReservations,recReservationsRecalculated);

			//recReservations.Dump();
			//recReservationsRecalculated.Dump();

			AddCalendarReservations(nEntityId, nTrueEntityType, recReservationsRecalculated);
		}
	}
	
	QApplication::restoreOverrideCursor();

	//QTime timer;
	//timer.start();
	//qDebug() << " /+++++++++++++ prije AddCalendarTaskItems() " << timer.elapsed();
	//AddCalendarTaskItems();
	//qDebug() << " /+++++++++++++ poslije AddCalendarTaskItems() " << timer.elapsed();
}*/

void Fui_Calendar::GetMultiPartEvents(DbRecordSet recEventParts, QList<int> &lstMultiPartEventsIDs)
{
	QList<int> tmpLst;
	int nPartCount = recEventParts.getRowCount();
	for (int i = 0; i < nPartCount; i++)
	{
		int nBcepID = recEventParts.getDataRef(i, "BCEV_ID").toInt();
		if (tmpLst.contains(nBcepID))
			lstMultiPartEventsIDs << nBcepID;
		else
			tmpLst << nBcepID;
	}
}

void Fui_Calendar::GetProposedEvents(DbRecordSet recEventParts, QList<int> &lstProposedEventPartIDs)
{
	QList<int> tmpLst;
	int nPartCount = recEventParts.getRowCount();
	for (int i = 0; i < nPartCount; i++)
	{
		int nBCOL_EVENT_PART_ID = recEventParts.getDataRef(i, "BCOL_EVENT_PART_ID").toInt();
		if (tmpLst.contains(nBCOL_EVENT_PART_ID))
			lstProposedEventPartIDs << nBCOL_EVENT_PART_ID;
		else
			tmpLst << nBCOL_EVENT_PART_ID;
	}
}

void Fui_Calendar::SortCalendarItems(int nEntityID, int nEntityType, int nEntityPersonID, DbRecordSet recEventParts, DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects, DbRecordSet recBreaks, bool bForInsert)
{
	DbRecordSet recSinglePartItems, recMultyPartItems;
	recSinglePartItems.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_EVENT_PARTS));
	recMultyPartItems.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_EVENT_PARTS));
	
	QList<int> lstMultiPartEventsIDs;
	GetMultiPartEvents(recEventParts, lstMultiPartEventsIDs);
	QList<int> lstProposedEventPartIDs;
	GetProposedEvents(recEventParts, lstProposedEventPartIDs);

	//_DUMP(recEventParts);
	int nPartCount = recEventParts.getRowCount();
	for (int i = 0; i < nPartCount; i++)
	{
		QDateTime startDateTime;
		QDateTime endDateTime;
		int nCentID;
		int nBcepID;
		int nBcep_Status;
		int nBCEV_IS_INFORMATION;
		bool bIsProposed = false;
		int nBcevID;
		int nBCOL_ID;
		int nPersonID;
		int nContactID;
		QString strTitle;
		QString strLocation;
		QString strColor;
		QPixmap pixIcon;
		GetCalendarData(i, recEventParts, lstMultiPartEventsIDs, startDateTime, endDateTime, nCentID, nBcepID, nBcevID, nBCOL_ID, 
			nPersonID, nContactID, strTitle, strLocation, strColor, pixIcon, nBcep_Status, nBCEV_IS_INFORMATION);
		
		if (lstProposedEventPartIDs.contains(nBcepID))
			bIsProposed = true;

		//Single part items.
		if (startDateTime.date() == endDateTime.date())
		{
			if (m_nCalendarViewSections == 1)
			{
				AddMultyDayEvent(true, i, nContactID, nBCOL_ID, nCentID, nBcepID, nBcevID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, 
					strLocation, strColor, pixIcon, recEventParts, recContacts, recResources, recProjects, recBreaks, bForInsert, nBcep_Status, 
					nBCEV_IS_INFORMATION, bIsProposed);
			}
			else if (m_nCalendarViewSections == 2)
			{
				AddSingleDayEvent(i, nContactID, nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, 
					strLocation, strColor, pixIcon, recEventParts, recContacts, recResources, recProjects, recBreaks, bForInsert, nBcep_Status, 
					nBCEV_IS_INFORMATION, bIsProposed);
			}
			else
			{
				AddMultyDayEvent(true, i, nContactID, nBCOL_ID, nCentID, nBcepID, nBcevID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, 
					strLocation, strColor, pixIcon, recEventParts, recContacts, recResources, recProjects, recBreaks, bForInsert, nBcep_Status, 
					nBCEV_IS_INFORMATION, bIsProposed);
				AddSingleDayEvent(i, nContactID, nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, 
					strLocation, strColor, pixIcon, recEventParts, recContacts, recResources, recProjects, recBreaks, bForInsert, nBcep_Status, 
					nBCEV_IS_INFORMATION, bIsProposed);
			}
		}
		//Multi part items.
		else
		{
			AddMultyDayEvent(false, i, nContactID, nBCOL_ID, nCentID, nBcepID, nBcevID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, 
				strLocation, strColor, pixIcon, recEventParts, recContacts, recResources, recProjects, recBreaks, bForInsert, nBcep_Status, 
				nBCEV_IS_INFORMATION, bIsProposed);
		}
	}
}

void Fui_Calendar::GetOwnerResProjParts(int nRow, DbRecordSet &recEventParts, int nCentID, int nContactID, int nBCOL_ID, QString &strOwner, QString &strResource, QString &strContact, QString &strProjects,
										QString &strDescription, QHash<int, qreal> &hshBrakeStart, QHash<int, qreal> &hshBrakeEnd, DbRecordSet &recContacts, 
										DbRecordSet &recResources, DbRecordSet &recProjects, DbRecordSet &recBreaks, DbRecordSet &recItemAssignedResources)
{
	DbRecordSet recSinglePartItem;
	recSinglePartItem.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_EVENT_PARTS));
	recEventParts.selectRow(nRow);
	recSinglePartItem.merge(recEventParts, true);
	
	recItemAssignedResources.addRow();
	recItemAssignedResources.setData(0, "BPER_CONTACT_ID", recEventParts.getDataRef(nRow, "BPER_CONTACT_ID").toInt());

	strOwner = recEventParts.getDataRef(nRow, "BPER_LAST_NAME").toString() + ", " + recEventParts.getDataRef(nRow, "BPER_FIRST_NAME").toString();
	strDescription = recEventParts.getDataRef(nRow, "BCOL_DESCRIPTION").toString();
	//_DUMP(recItemAssignedResources);
	strContact	= GetContactsForPart(nCentID, recContacts, nContactID, recItemAssignedResources);
	strResource	= GetResourcesForPart(nCentID, recResources, recItemAssignedResources);
	strProjects	= GetProjectsForPart(nCentID, recProjects, recItemAssignedResources);
}

void Fui_Calendar::AddEvent(int nBcevID, int nEntityID, int nEntityType, int nEntityPersonID)
{
	DbRecordSet recEventParts; 
	DbRecordSet recContacts;
	DbRecordSet recResources;
	DbRecordSet recProjects;
	DbRecordSet recBreaks;
	Status err;
	_SERVER_CALL(BusCalendar->ReadOneEvent(err, nBcevID, recEventParts, recContacts, recResources, recProjects, recBreaks));
	_CHK_ERR(err);

	//_DUMP(recEventParts);

	QList<int> lstMultiPartEventsIDs;
	GetMultiPartEvents(recEventParts, lstMultiPartEventsIDs);
	QList<int> lstProposedEventPartIDs;
	GetProposedEvents(recEventParts, lstProposedEventPartIDs);

	int nRowCount = recEventParts.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		QDateTime startDateTime;
		QDateTime endDateTime;
		int nCentID;
		int nBcepID;
		int nBcep_Status;
		int nBCEV_IS_INFORMATION;
		bool bIsProposed = false;
		//int nBcevID;
		int nBCOL_ID;
		int nPersonID;
		int nContactID;
		QString strTitle;
		QString strLocation;
		QString strColor;
		QPixmap pixIcon;

		GetCalendarData(i, recEventParts, lstMultiPartEventsIDs, startDateTime, endDateTime, nCentID, nBcepID, nBcevID, nBCOL_ID, 
			nPersonID, nContactID, strTitle, strLocation, strColor, pixIcon, nBcep_Status, nBCEV_IS_INFORMATION);

		if (lstProposedEventPartIDs.contains(nBcepID))
			bIsProposed = true;

		QString strOwner;
		QString strResource;
		QString strContact;
		QString strProjects;
		QString strDescription;

		QHash<int, qreal> hshBrakeStart, hshBrakeEnd;

		DbRecordSet recItemAssignedResources;
		recItemAssignedResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ITEM_ASSIGNED_RESOURCES_VIEW));
		GetOwnerResProjParts(i, recEventParts, nCentID, nContactID, nBCOL_ID, strOwner, strResource, strContact, strProjects, strDescription, hshBrakeStart, hshBrakeEnd, 
			recContacts, recResources, recProjects, recBreaks, recItemAssignedResources);

		CreateBrakesHashes(nBCOL_ID, startDateTime, endDateTime, recBreaks, hshBrakeStart, hshBrakeEnd);

		//Single part items.
		if (startDateTime.date() == endDateTime.date())
		{
			AddSingleDayEvent(i, nContactID, nBCOL_ID, nCentID, nBcepID, nBcevID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, 
				strLocation, strColor, pixIcon, recEventParts, recContacts, recResources, recProjects, recBreaks, false, nBcep_Status, 
				nBCEV_IS_INFORMATION, bIsProposed);
			AddMultyDayEvent(true, i, nContactID, nBCOL_ID, nCentID, nBcepID, nBcevID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, 
				strLocation, strColor, pixIcon, recEventParts, recContacts, recResources, recProjects, recBreaks, false, nBcep_Status, 
				nBCEV_IS_INFORMATION, bIsProposed);
		}
		//Multi part items.
		else
		{
			AddMultyDayEvent(false, i, nContactID, nBCOL_ID, nCentID, nBcepID, nBcevID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, 
				strLocation, strColor, pixIcon, recEventParts, recContacts, recResources, recProjects, recBreaks, false, nBcep_Status, 
				nBCEV_IS_INFORMATION, bIsProposed);
		}
	}
}

void Fui_Calendar::AddSingleDayEvent(int nRow, int nContactID, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, 
									 QDateTime startDateTime, QDateTime endDateTime, QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, 
									 DbRecordSet recEventParts, DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects, 
									 DbRecordSet recBreaks, bool bForInsert, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed)
{
	QString strOwner;
	QString strResource;
	QString strContact;
	QString strProjects;
	QString strDescription;

	QHash<int, qreal> hshBrakeStart, hshBrakeEnd;

	DbRecordSet recItemAssignedResources;
	recItemAssignedResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ITEM_ASSIGNED_RESOURCES_VIEW));
	
	GetOwnerResProjParts(nRow, recEventParts, nCentID, nContactID, nBCOL_ID, strOwner, strResource, strContact, strProjects, strDescription, hshBrakeStart, hshBrakeEnd, 
						recContacts, recResources, recProjects, recBreaks, recItemAssignedResources);

	//_DUMP(recItemAssignedResources);

	CreateBrakesHashes(nBCOL_ID, startDateTime, endDateTime, recBreaks, hshBrakeStart, hshBrakeEnd);
	ui.calendarWidget->AddCalendarItem(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, nEntityPersonID, startDateTime, endDateTime, strTitle, strLocation, 
		strColor, pixIcon, strOwner, strResource, strContact, strProjects, strDescription, hshBrakeStart, hshBrakeEnd, bForInsert, nBcep_Status, 
		nBCEV_IS_INFORMATION, bIsProposed, recItemAssignedResources);
}

void Fui_Calendar::RemoveSingleDayEvent(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	ui.calendarWidget->RemoveSingleDayCalendarItem(nCENT_ID, nBcepID, nBcevID, nBCOL_ID);
}

void Fui_Calendar::RemoveMultiDayEvent(int nCENT_ID, int nBcepID, int nBcevID, int nBCOL_ID)
{

	ui.calendarWidget->RemoveMultiDayEventItem(nCENT_ID, nBcepID, nBcevID, nBCOL_ID);
}

void Fui_Calendar::ModifyOneEvent(int nBcevID)
{
	DbRecordSet recEventParts; 
	DbRecordSet recContacts;
	DbRecordSet recResources;
	DbRecordSet recProjects;
	DbRecordSet recBreaks;
	Status err;
	_SERVER_CALL(BusCalendar->ReadOneEvent(err, nBcevID, recEventParts, recContacts, recResources, recProjects, recBreaks));
	_CHK_ERR(err);

	//_DUMP(recEventParts);
	QList<int> lstMultiPartEventsIDs;
	GetMultiPartEvents(recEventParts, lstMultiPartEventsIDs);
	QList<int> lstProposedEventPartIDs;
	GetProposedEvents(recEventParts, lstProposedEventPartIDs);

	//_DUMP(recEventParts);

	int nRowCount = recEventParts.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		ModifySingleDayEvent(i, recEventParts, recContacts, recResources, recProjects, recBreaks, lstMultiPartEventsIDs, lstProposedEventPartIDs);
	}
}

void Fui_Calendar::ModifySingleDayEvent(int nRow, DbRecordSet recEventParts, DbRecordSet recContacts, DbRecordSet recResources, 
										DbRecordSet recProjects, DbRecordSet recBreaks, QList<int> lstMultiPartEventsIDs,
										QList<int> lstProposedEventPartIDs)
{
	//_DUMP(recEventParts);
	
	QDateTime startDateTime;
	QDateTime endDateTime;
	int nCentID;
	int nBcepID;
	int nBcevID;
	int nBCOL_ID;
	int nBcep_Status;
	int nBCEV_IS_INFORMATION;
	bool bIsProposed = false;
	int nPersonID;
	int nContactID;
	QString strTitle;
	QString strLocation;
	QString strColor;
	QPixmap pixIcon;
	GetCalendarData(nRow, recEventParts, lstMultiPartEventsIDs, startDateTime, endDateTime, nCentID, nBcepID, nBcevID, nBCOL_ID, 
		nPersonID, nContactID, strTitle, strLocation, strColor, pixIcon, nBcep_Status, nBCEV_IS_INFORMATION);

	if (lstProposedEventPartIDs.contains(nBcepID))
		bIsProposed = true;
	
	QString strOwner;
	QString strResource;
	QString strContact;
	QString strProjects;
	QString strDescription;

	QHash<int, qreal> hshBrakeStart, hshBrakeEnd;

	DbRecordSet recItemAssignedResources;
	recItemAssignedResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ITEM_ASSIGNED_RESOURCES_VIEW));

	GetOwnerResProjParts(nRow, recEventParts, nCentID, nContactID, nBCOL_ID, strOwner, strResource, strContact, strProjects, strDescription, hshBrakeStart, hshBrakeEnd, 
		recContacts, recResources, recProjects, recBreaks, recItemAssignedResources);
	
	CreateBrakesHashes(nBCOL_ID, startDateTime, endDateTime, recBreaks, hshBrakeStart, hshBrakeEnd);
	
	ui.calendarWidget->UpdateCalendarItem(nCentID, nBcepID, nBcevID, nBCOL_ID, startDateTime, endDateTime, strTitle, strLocation, 
									strColor, pixIcon, strOwner, strResource, strContact, strProjects, strDescription, hshBrakeStart, 
									hshBrakeEnd, nBcep_Status, nBCEV_IS_INFORMATION, bIsProposed, recItemAssignedResources);
}

void Fui_Calendar::SplitMultiDayToSingleDays(int nRow, DbRecordSet recEventParts, DbRecordSet &recSplitedMultyDayEvent)
{
	QDateTime startDateTime = recEventParts.getDataRef(nRow, "BCOL_FROM").toDateTime();
	QDateTime endDateTime = recEventParts.getDataRef(nRow, "BCOL_TO").toDateTime();

	if (startDateTime < m_datFrom)
		startDateTime = m_datFrom;
	if (endDateTime > m_datTo)
		endDateTime = m_datTo;
	
	recSplitedMultyDayEvent.copyDefinition(recEventParts);
	int nRowCount = startDateTime.daysTo(endDateTime)+1;
	for (int i = 0; i < nRowCount; i++)
	{
		QDateTime rowStartDateTime;
		QDateTime rowEndDateTime;
		recSplitedMultyDayEvent.merge(recEventParts.getRow(nRow));
		if (i == 0)
			rowStartDateTime = startDateTime;
		else
			rowStartDateTime = QDateTime(startDateTime.date().addDays(i), QTime(0,0));

		if (i == (nRowCount-1))
			rowEndDateTime = endDateTime;
		else
			rowEndDateTime = QDateTime(startDateTime.date().addDays(i), QTime(23,59, 59));

		recSplitedMultyDayEvent.setData(i, "BCOL_FROM", rowStartDateTime);
		recSplitedMultyDayEvent.setData(i, "BCOL_TO", rowEndDateTime);
	}
}

void Fui_Calendar::AddMultyDayEvent(bool bFromSingleDayEvent, int nRow, int nContactID, int nBCOL_ID, int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType, int nEntityPersonID, 
									QDateTime startDateTime, QDateTime endDateTime, QString strTitle, QString strLocation, QString strColor, QPixmap pixIcon, 
									DbRecordSet recEventParts, DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects, 
									DbRecordSet recBreaks, bool bForInsert, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed)
{
	//First add multi day event in multi day calendar.
	DbRecordSet recSinglePartItem;
	recSinglePartItem.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_EVENT_PARTS));
	recEventParts.selectRow(nRow);
	recSinglePartItem.merge(recEventParts, true);

	DbRecordSet recItemAssignedResources;
	recItemAssignedResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ITEM_ASSIGNED_RESOURCES_VIEW));
	recItemAssignedResources.addRow();
	recItemAssignedResources.setData(0, "BPER_CONTACT_ID", recEventParts.getDataRef(nRow, "BPER_CONTACT_ID").toInt());
	QString strOwner = recEventParts.getDataRef(nRow, "BPER_LAST_NAME").toString() + ", " + recEventParts.getDataRef(nRow, "BPER_FIRST_NAME").toString();
	QString strResource;
	QString strContact;
	QString strProjects;
	QString strDescription = recEventParts.getDataRef(nRow, "BCOL_DESCRIPTION").toString();
	QString strSingleContacts	= GetContactsForPart(nCentID, recContacts, nContactID, recItemAssignedResources);
	QString recSingleResources	= GetResourcesForPart(nCentID, recResources, recItemAssignedResources);
	QString recSingleProjects	= GetProjectsForPart(nCentID, recProjects, recItemAssignedResources);

	//_DUMP(recItemAssignedResources);

	QString s = startDateTime.toString();
	QString ss = endDateTime.toString();
	
	ui.calendarWidget->AddMultiDayCalendarItem(nCentID, nBcepID, nBcevID, nBCOL_ID, nEntityID, nEntityType, startDateTime, endDateTime, 
		strTitle, strLocation, strColor, pixIcon, strOwner, recSingleResources, strSingleContacts, recSingleProjects, strDescription, bForInsert, 
		nBcep_Status, nBCEV_IS_INFORMATION, bIsProposed, recItemAssignedResources);

	AddMultyDayToSingleDayCalendar(bFromSingleDayEvent, nRow, recEventParts, nCentID, nBcepID, nBcevID, nEntityID, nEntityType, startDateTime, endDateTime);
}

void Fui_Calendar::AddMultyDayToSingleDayCalendar(bool bFromSingleDayEvent, int nRow, DbRecordSet recEventParts, int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType, 
									QDateTime startDateTime, QDateTime endDateTime)
{
	if (m_nCalendarViewSections == 1)
		return;

	//Then split this multi day to single day for single day event calendar.
	DbRecordSet recSplitedMultyDayEvent;
	SplitMultiDayToSingleDays(nRow, recEventParts, recSplitedMultyDayEvent);
	int nRowCount = recSplitedMultyDayEvent.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		QDateTime splittedStartDateTime = recSplitedMultyDayEvent.getDataRef(i, "BCOL_FROM").toDateTime();
		QDateTime splittedEndDateTime = recSplitedMultyDayEvent.getDataRef(i, "BCOL_TO").toDateTime();
		ui.calendarWidget->AddMultiDay(bFromSingleDayEvent, nCentID, nBcepID, nBcevID, nEntityID, nEntityType, splittedStartDateTime, splittedEndDateTime);
	}
}

void Fui_Calendar::RemoveMultyDayFromSingleDayCalendar(int nCentID, int nBcepID, int nBcevID, int nEntityID, int nEntityType)
{

}

void Fui_Calendar::AddCalendarReservations(int nEntityID, int nEntityType, DbRecordSet recReservationsRecalculated)
{
	int nRowCount = recReservationsRecalculated.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nBCRS_ID = recReservationsRecalculated.getDataRef(i, "BCRS_ID").toInt();
		int nBCRS_IS_POSSIBLE = recReservationsRecalculated.getDataRef(i, "BCRS_IS_POSSIBLE").toInt();
		int nBCRS_IS_FREE = recReservationsRecalculated.getDataRef(i, "BCRS_IS_FREE").toInt();
		QDateTime startDateTime;
		QDateTime endDateTime;
		if (nBCRS_IS_FREE)
		{
			DbRecordSet recSplitedReservationsRecalculated;
			SplitMultiDayReservationToSingleDay(i, recReservationsRecalculated, recSplitedReservationsRecalculated);

			int nSplittedRowCount = recSplitedReservationsRecalculated.getRowCount();
			for (int j = 0; j < nSplittedRowCount; j++)
			{
				startDateTime = recSplitedReservationsRecalculated.getDataRef(j, "DATE_FROM").toDateTime();
				endDateTime = recSplitedReservationsRecalculated.getDataRef(j, "DATE_TO").toDateTime();

				//QString s = startDateTime.toString();
				//QString ss = endDateTime.toString();

				ui.calendarWidget->AddCalendarReservation(nEntityID, nEntityType, nBCRS_ID, nBCRS_IS_POSSIBLE, startDateTime, endDateTime);
			}
		}
		else
		{
			startDateTime = recReservationsRecalculated.getDataRef(i, "DATE_FROM").toDateTime();
			endDateTime = recReservationsRecalculated.getDataRef(i, "DATE_TO").toDateTime();

			//QString s = startDateTime.toString();
			//QString ss = endDateTime.toString();
			
			ui.calendarWidget->AddCalendarReservation(nEntityID, nEntityType, nBCRS_ID, nBCRS_IS_POSSIBLE, startDateTime, endDateTime);
		}
	}
}

void Fui_Calendar::AddCalendarTaskItems()
{
	if (!m_bShowTasks)
		return;

	DbRecordSet recFilter;
	CommGridFilterSettings filterSettings;
	CommGridViewHelper_Client m_pCmGridHelper;

	recFilter = filterSettings.GetCalendarGridTasksFilterSettings();
	//_DUMP(recFilter);

	int nRow = recFilter.find("BUSCS_SETTING_ID", FROM_DATE, true);
	Q_ASSERT(nRow >= 0);
	recFilter.setData(nRow, "BUSCS_VALUE_DATETIME", m_datFrom);
	nRow = recFilter.find("BUSCS_SETTING_ID", TO_DATE, true);
	Q_ASSERT(nRow >= 0);
	recFilter.setData(nRow, "BUSCS_VALUE_DATETIME", m_datTo);

	DbRecordSet recEmail, recVoiceCall, recDocument;
	QApplication::setOverrideCursor(Qt::WaitCursor);

	QTime timer;
	timer.start();
	
	Status err;
	_SERVER_CALL(BusCommunication->GetCalendarGridCommData(err, recEmail, recDocument, recVoiceCall, m_recFixedEntites, recFilter))

	//_DUMP(recDocument);

	if (!err.IsOK())
		QApplication::restoreOverrideCursor();
	_CHK_ERR(err);

	qDebug() << "poslije GetCalendarGridCommData" << timer.elapsed();

	//CHECK FP FOR EMAIL.
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW))
		recEmail.clear();

	//_DUMP(m_recEmail);

	QApplication::restoreOverrideCursor();

	qDebug() << "prije stavljanja taskova " << timer.elapsed();
	
	int nRowCount = recEmail.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		//_DUMP(recEmail);
		QDateTime datStart	= recEmail.getDataRef(i, "BTKS_START").toDateTime();
		QDateTime datEnd	= recEmail.getDataRef(i, "BTKS_DUE").toDateTime();
		int nCENT_ID		= recEmail.getDataRef(i, "CENT_ID").toInt();
		int nBTKS_ID		= recEmail.getDataRef(i, "BTKS_ID").toInt();
		int nTypeID			= recEmail.getDataRef(i, "BEM_ID").toInt();
		int nCENT_SYSTEM_TYPE_ID = recEmail.getDataRef(i, "CENT_SYSTEM_TYPE_ID").toInt();
		int nEntityID		= recEmail.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType		= recEmail.getDataRef(i, "ENTITY_TYPE").toInt();
		int nPersonID		= recEmail.getDataRef(i, "ENTITY_PERSON_ID").toInt();
		QHash<int,int> hshRow2APPTYPE;
		QString strPixmap;
		bool bTaskIconExists = false;
		QPixmap pix;

		//_DUMP(recEmail);
		if (!recEmail.getDataRef(i, "BCMT_PICTURE").toByteArray().isEmpty())
		{
			bTaskIconExists = true;
			pix.loadFromData(recEmail.getDataRef(i, "BCMT_PICTURE").toByteArray());
		}

		int nInOutDocument = m_pCmGridHelper.CheckItemDirection(m_pCmGridHelper.EMAIL, recEmail,i);
		m_pCmGridHelper.GetCommGridFourthIcon(bTaskIconExists, m_pCmGridHelper.EMAIL, 0, i, strPixmap, recEmail, hshRow2APPTYPE, nInOutDocument);
		if (strPixmap != "-1")
			pix = QPixmap(strPixmap);

		if (strPixmap == ":Empty_icon.png")
			pix = QPixmap(":Run_Task16.png");

		//QString s = datStart.toString();
		//QString ss = m_datFrom.toString();
		//QString sss = datEnd.toString();
		//QString ssss = m_datTo.toString();

		if (datStart>m_datFrom && datStart<m_datTo && datStart.isValid())
		{
			QPixmap iconPixmap(34,16);
			iconPixmap.fill(QColor(255,255,255,0));
			QPainter painter(&iconPixmap);
			painter.drawPixmap(0, 0,pix.scaled(16,16));
			painter.drawPixmap(1*16+2, 0,QPixmap(":Task_Start16.png"));
			
			ui.calendarWidget->AddCalendarTaskItems(nCENT_ID, nBTKS_ID, nCENT_SYSTEM_TYPE_ID, datStart, true, iconPixmap, nEntityID, nEntityType, nTypeID);
		}
		if (datEnd<m_datTo && datEnd>m_datFrom && datEnd.isValid())
		{
			QPixmap iconPixmap(34,16);
			iconPixmap.fill(QColor(255,255,255,0));
			QPainter painter(&iconPixmap);
			painter.drawPixmap(0, 0,pix.scaled(16,16));
			painter.drawPixmap(1*16+2, 0,QPixmap(":Task_Due16.png"));

			ui.calendarWidget->AddCalendarTaskItems(nCENT_ID, nBTKS_ID, nCENT_SYSTEM_TYPE_ID, datEnd, false, iconPixmap, nEntityID, nEntityType, nTypeID);
		}
	}
	nRowCount = recDocument.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		QDateTime datStart	= recDocument.getDataRef(i, "BTKS_START").toDateTime();
		QDateTime datEnd	= recDocument.getDataRef(i, "BTKS_DUE").toDateTime();
		int nCENT_ID		= recDocument.getDataRef(i, "CENT_ID").toInt();
		int nBTKS_ID		= recDocument.getDataRef(i, "BTKS_ID").toInt();
		int nCENT_SYSTEM_TYPE_ID = recDocument.getDataRef(i, "CENT_SYSTEM_TYPE_ID").toInt();
		int nTypeID			= recDocument.getDataRef(i, "BDMD_ID").toInt();
		int nEntityID		= recDocument.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType		= recDocument.getDataRef(i, "ENTITY_TYPE").toInt();
		int nPersonID		= recDocument.getDataRef(i, "ENTITY_PERSON_ID").toInt();
		QHash<int,int> hshRow2APPTYPE;
		QString strPixmap;
		bool bTaskIconExists = false;
		QPixmap pix;

		if (!recDocument.getDataRef(i, "BCMT_PICTURE").toByteArray().isEmpty())
		{
			bTaskIconExists = true;
			pix.loadFromData(recDocument.getDataRef(i, "BCMT_PICTURE").toByteArray());
		}

		int nInOutDocument = m_pCmGridHelper.CheckItemDirection(m_pCmGridHelper.DOCUMENT, recDocument,i);
		m_pCmGridHelper.GetCommGridFourthIcon(bTaskIconExists, m_pCmGridHelper.DOCUMENT, 0, i, strPixmap, recDocument, hshRow2APPTYPE, nInOutDocument);
		if (strPixmap != "-1")
			pix = QPixmap(strPixmap);

		if (strPixmap == ":Empty_icon.png")
			pix = QPixmap(":Run_Task16.png");

		if (datStart>m_datFrom && datStart<m_datTo && datStart.isValid())
		{
			QPixmap iconPixmap(34,16);
			iconPixmap.fill(QColor(255,255,255,0));
			QPainter painter(&iconPixmap);
			painter.drawPixmap(0, 0,pix.scaled(16,16));
			painter.drawPixmap(1*16+2, 0,QPixmap(":Task_Start16.png"));

			ui.calendarWidget->AddCalendarTaskItems(nCENT_ID, nBTKS_ID, nCENT_SYSTEM_TYPE_ID, datStart, true, iconPixmap, nEntityID, nEntityType, nTypeID);
		}
		if (datEnd<m_datTo && datEnd>m_datFrom && datEnd.isValid())
		{
			QPixmap iconPixmap(34,16);
			iconPixmap.fill(QColor(255,255,255,0));
			QPainter painter(&iconPixmap);
			painter.drawPixmap(0, 0,pix.scaled(16,16));
			painter.drawPixmap(1*16+2, 0,QPixmap(":Task_Due16.png"));

			ui.calendarWidget->AddCalendarTaskItems(nCENT_ID, nBTKS_ID, nCENT_SYSTEM_TYPE_ID, datEnd, false, iconPixmap, nEntityID, nEntityType, nTypeID);
		}
	}
	nRowCount = recVoiceCall.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		QDateTime datStart	= recVoiceCall.getDataRef(i, "BTKS_START").toDateTime();
		QDateTime datEnd	= recVoiceCall.getDataRef(i, "BTKS_DUE").toDateTime();
		int nCENT_ID		= recVoiceCall.getDataRef(i, "CENT_ID").toInt();
		int nBTKS_ID		= recVoiceCall.getDataRef(i, "BTKS_ID").toInt();
		int nTypeID			= recVoiceCall.getDataRef(i, "BVC_ID").toInt();
		int nEntityID		= recVoiceCall.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType		= recVoiceCall.getDataRef(i, "ENTITY_TYPE").toInt();
		int nPersonID		= recVoiceCall.getDataRef(i, "ENTITY_PERSON_ID").toInt();
		int nCENT_SYSTEM_TYPE_ID = recVoiceCall.getDataRef(i, "CENT_SYSTEM_TYPE_ID").toInt();
		QHash<int,int> hshRow2APPTYPE;
		QString strPixmap;
		bool bTaskIconExists = false;
		QPixmap pix;

		if (!recVoiceCall.getDataRef(i, "BCMT_PICTURE").toByteArray().isEmpty())
		{
			bTaskIconExists = true;
			pix.loadFromData(recVoiceCall.getDataRef(i, "BCMT_PICTURE").toByteArray());
		}

		int nInOutDocument = m_pCmGridHelper.CheckItemDirection(m_pCmGridHelper.VOICECALL, recVoiceCall,i);
		m_pCmGridHelper.GetCommGridFourthIcon(bTaskIconExists, m_pCmGridHelper.VOICECALL, 0, i, strPixmap, recVoiceCall, hshRow2APPTYPE, nInOutDocument);
		if (strPixmap != "-1")
			pix = QPixmap(strPixmap);

		if (strPixmap == ":Empty_icon.png")
			pix = QPixmap(":Run_Task16.png");

		if (datStart>m_datFrom && datStart<m_datTo && datStart.isValid())
		{
			QPixmap iconPixmap(34,16);
			iconPixmap.fill(QColor(255,255,255,0));
			QPainter painter(&iconPixmap);
			painter.drawPixmap(0, 0,pix.scaled(16,16));
			painter.drawPixmap(1*16+2, 0,QPixmap(":Task_Start16.png"));

			ui.calendarWidget->AddCalendarTaskItems(nCENT_ID, nBTKS_ID, nCENT_SYSTEM_TYPE_ID, datStart, true, iconPixmap, nEntityID, nEntityType, nTypeID);
		}
		if (datEnd<m_datTo && datEnd>m_datFrom && datEnd.isValid())
		{
			QPixmap iconPixmap(34,16);
			iconPixmap.fill(QColor(255,255,255,0));
			QPainter painter(&iconPixmap);
			painter.drawPixmap(0, 0,pix.scaled(16,16));
			painter.drawPixmap(1*16+2, 0,QPixmap(":Task_Due16.png"));

			ui.calendarWidget->AddCalendarTaskItems(nCENT_ID, nBTKS_ID, nCENT_SYSTEM_TYPE_ID, datEnd, false, iconPixmap, nEntityID, nEntityType, nTypeID);
		}
	}

	qDebug() << "poslije stavljanja taskova " << timer.elapsed();
}

void Fui_Calendar::SplitMultiDayReservationToSingleDay(int nRow, DbRecordSet recReservationsRecalculated, DbRecordSet &recSplitedReservationsRecalculated)
{
	QDateTime startDateTime = recReservationsRecalculated.getDataRef(nRow, "DATE_FROM").toDateTime();
	QDateTime endDateTime = recReservationsRecalculated.getDataRef(nRow, "DATE_TO").toDateTime();

	if (startDateTime < m_datFrom)
		startDateTime = m_datFrom;
	if (endDateTime > m_datTo)
		endDateTime = m_datTo;

	recSplitedReservationsRecalculated.copyDefinition(recReservationsRecalculated);
	int nRowCount = startDateTime.daysTo(endDateTime)+1;
	for (int i = 0; i < nRowCount; i++)
	{
		QDateTime rowStartDateTime;
		QDateTime rowEndDateTime;
		recSplitedReservationsRecalculated.merge(recReservationsRecalculated.getRow(nRow));
//		if (i == 0)
//			rowStartDateTime = startDateTime;
//		else
			rowStartDateTime = QDateTime(startDateTime.date().addDays(i), QTime(0,0));

//		if (i == (nRowCount-1))
//			rowEndDateTime = endDateTime;
//		else
			rowEndDateTime = QDateTime(startDateTime.date().addDays(i), QTime(23,59, 59));

		recSplitedReservationsRecalculated.setData(i, "DATE_FROM", rowStartDateTime);
		recSplitedReservationsRecalculated.setData(i, "DATE_TO", rowEndDateTime);
	}
}

QString Fui_Calendar::GetContactsForPart(int nCentID, DbRecordSet &recContacts, int nOwnerContactID, DbRecordSet &recItemAssignedResources)
{
	QString strContacts;
	int nRowCount = recContacts.getRowCount();
	if (recItemAssignedResources.getRowCount() < nRowCount)
		recItemAssignedResources.addRow(nRowCount-recItemAssignedResources.getRowCount());
	
	//_DUMP(recContacts);
	for (int i = 0; i < nRowCount; ++i)
	{
		if ((nCentID == recContacts.getDataRef(i, "CENT_ID").toInt()) && !(nOwnerContactID == recContacts.getDataRef(i, "BCNT_ID").toInt()))
		{
			strContacts += recContacts.getDataRef(i, "BCNT_FIRSTNAME").toString() + " " + recContacts.getDataRef(i, "BCNT_LASTNAME").toString() + ", ";
			
			//if (recItemAssignedResources.getRowCount() <= i)
			//	recItemAssignedResources.addRow();
			//_DUMP(recItemAssignedResources);
			recItemAssignedResources.setData(i, "BCNT_ID", recContacts.getDataRef(i, "BCNT_ID").toInt());
			//_DUMP(recItemAssignedResources);
		}
	}
	if (nRowCount > 0)
		strContacts.chop(2); //Chop last space and coma.
	
	return strContacts;
}

QString Fui_Calendar::GetResourcesForPart(int nCentID, DbRecordSet &recResources, DbRecordSet &recItemAssignedResources)
{
	QString strResources;
	int nRowCount = recResources.getRowCount();
	if (recItemAssignedResources.getRowCount() < nRowCount)
		recItemAssignedResources.addRow(nRowCount-recItemAssignedResources.getRowCount());

	//_DUMP(recResources);
	for (int i = 0; i < nRowCount; ++i)
	{
		if (nCentID == recResources.getDataRef(i, "CENT_ID").toInt())
		{
			strResources += recResources.getDataRef(i, "BRES_NAME").toString() + ", ";
		
			//if (recItemAssignedResources.getRowCount() <= i)
			//	recItemAssignedResources.addRow();
			recItemAssignedResources.setData(i, "BRES_ID", recResources.getDataRef(i, "BRES_ID").toInt());
		}
	}
	if (nRowCount > 0)
		strResources.chop(2); //Chop last space and coma.

	return strResources;
}

QString Fui_Calendar::GetProjectsForPart(int nCentID, DbRecordSet &recProjects, DbRecordSet &recItemAssignedResources)
{
	QString strProjects;
	//_DUMP(recProjects);
	int nRowCount = recProjects.getRowCount();
	if (recItemAssignedResources.getRowCount() < nRowCount)
		recItemAssignedResources.addRow(nRowCount-recItemAssignedResources.getRowCount());
	
	for (int i = 0; i < nRowCount; ++i)
	{
		if (nCentID == recProjects.getDataRef(i, "CENT_ID").toInt())
		{
			strProjects += recProjects.getDataRef(i, "BUSP_NAME").toString() + ", ";

			//if (recItemAssignedResources.getRowCount() <= i)
			//	recItemAssignedResources.addRow();
			recItemAssignedResources.setData(i, "BUSP_ID", recProjects.getDataRef(i, "BUSP_ID").toInt());
		}
	}
	if (nRowCount > 0)
		strProjects.chop(2); //Chop last space and coma.

	return strProjects;
}

void Fui_Calendar::CreateBrakesHashes(int nBCOL_ID, QDateTime startDateTime, QDateTime endDateTime, DbRecordSet recBreaks, QHash<int, qreal> &hshBrakeStart, QHash<int, qreal> &hshBrakeEnd)
{
	int nRowCount = recBreaks.getRowCount();
	int nCounter = 0;
	for (int i = 0; i < nRowCount; i++)
	{
		if (nBCOL_ID == recBreaks.getDataRef(i, "BCBL_OPTIONS_ID").toInt())
		{
			//Check is brake in this day (in case this event is a split multi day event.
			if ((startDateTime.date() == recBreaks.getDataRef(i, "BCBL_FROM").toDateTime().date()))
			{
				int nItemDurationInSecs = startDateTime.secsTo(endDateTime);
				int nStartBrakeInSecs = startDateTime.secsTo(recBreaks.getDataRef(i, "BCBL_FROM").toDateTime());
				int nEndBrakeInSecs = startDateTime.secsTo(recBreaks.getDataRef(i, "BCBL_TO").toDateTime());
				hshBrakeStart.insert(nCounter, (qreal)nStartBrakeInSecs/nItemDurationInSecs);
				hshBrakeEnd.insert(nCounter, (qreal)nEndBrakeInSecs/nItemDurationInSecs);
				nCounter++;
			}
		}
	}
}

void Fui_Calendar::on_onDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange, bool bResizeColumns)
{
	m_bResizeColumnsToFit = bResizeColumns;
	
	m_nDateRange = nRange;
	m_datFrom		= dtFrom;
	m_datTo			= dtTo;

	SetupDateIntervalLabel(nRange);
	m_nDateRange = nRange;
	ui.rangeFrame->SetDateRange(m_datFrom.date(), m_datTo.date());
	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	//if (m_recFixedEntites.getRowCount()>1)
		//ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_DateChanged(QDate date)
{
	//m_nDateRange = 2;		//Same as week in date filter frame.
	QDate datFrom, datTo;

	datFrom = date;
	//QString s = datFrom.toString();
	
	if (!datTo.isValid())
		datTo = datFrom;
	
	DataHelper::CalendarRangeSelectorClicked(datFrom, datTo, m_nDateRange);

	//QString ss = datFrom.toString();
	//QString sss = datTo.toString();

	ui.rangeFrame->SetDateRange(datFrom, datTo);
	
	m_datFrom = QDateTime(datFrom, QTime(0, 0, 0));
	m_datTo = QDateTime(datTo, QTime(23, 59, 59));

	ui.dateFilter_frame->blockSignals(true);
	ui.dateFilter_frame->Initialize(m_datFrom, m_datTo, m_nDateRange);
	ui.dateFilter_frame->DisableDateRange(DataHelper::YEAR); //B.T. 04.09.2013 issue 2723
	ui.dateFilter_frame->blockSignals(false);

	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_rangeFrameRangeTimeIntervalChanged(int nTimeInterval)
{
	m_TimeScale = (CalendarWidget::timeScale)nTimeInterval;
	ui.calendarWidget->setTimeScale(m_TimeScale);
}

bool Fui_Calendar::EntityAlreadyExist(int nEntityID, int nEntityType)
{
	bool bEntityExist = false;
	int nRowCount = m_recFixedEntites.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		if ((m_recFixedEntites.getDataRef(i, "ENTITY_ID").toInt() == nEntityID) && (m_recFixedEntites.getDataRef(i, "ENTITY_TYPE").toInt() == nEntityType))
			bEntityExist = true;
	}

	return bEntityExist;
}

void Fui_Calendar::addEntity(int nEntityID, int nEntityType, QString strName, int nPersonID /*= -1*/)
{
	m_recEntites.addRow();
	m_recFixedEntites.addRow();
	int nRow = m_recFixedEntites.getRowCount()-1;

	m_recFixedEntites.setData(nRow, "ENTITY_ID", nEntityID);
	m_recFixedEntites.setData(nRow, "ENTITY_TYPE", nEntityType);
	m_recFixedEntites.setData(nRow, "ENTITY_NAME", strName);
	m_recFixedEntites.setData(nRow, "ENTITY_PERSON_ID", nPersonID);

	m_nCurentRow = nRow;
	ReorderEntitiesRecordSet();
}

void Fui_Calendar::removeCurrentEntity()
{
	m_recFixedEntites.deleteRow(m_nCurentRow);

	if (m_nCurentRow == m_recFixedEntites.getRowCount())
		m_nCurentRow--;
	
	ReorderEntitiesRecordSet();
}

void Fui_Calendar::enableDisableButtons()
{
	if (m_nCurentRow<=0)
		ui.up_toolButton->setDisabled(true);
	else
		ui.up_toolButton->setDisabled(false);

	int nEntityCount = m_recFixedEntites.getRowCount();
	if (nEntityCount > 1 && ((nEntityCount-1) > m_nCurentRow))
		ui.down_toolButton->setDisabled(false);
	else
		ui.down_toolButton->setDisabled(true);

	if (nEntityCount <= 1)
		ui.delete_toolButton->setDisabled(true);
	else
		ui.delete_toolButton->setDisabled(false);
}

void Fui_Calendar::on_delete_toolButton_clicked()
{
	int nCurrentEntityType = m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt();
	removeCurrentEntity();

	enableDisableButtons();
	setLabelsAndSapne();
	
	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());

	if (m_bShowTasks)
		ui.frame_2->Reload(&m_recFixedEntites);
}

void Fui_Calendar::on_up_toolButton_clicked()
{
	m_nCurentRow--;
	ReorderEntitiesRecordSet();
	enableDisableButtons();
	setLabelsAndSapne();

	ui.calendarWidget->UpdateCalendarFromFui();
	ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());

	////InitializeCalendarWidget();
	////GetCalendarItems();
}

void Fui_Calendar::on_down_toolButton_clicked()
{
	m_nCurentRow++;
	ReorderEntitiesRecordSet();
	enableDisableButtons();
	setLabelsAndSapne();

	ui.calendarWidget->UpdateCalendarFromFui();
	ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());

	////InitializeCalendarWidget();
	////GetCalendarItems();
}

void Fui_Calendar::setDroppedEntityOnSecondRow(int nDroppedEntityID, int nDroppedEntityType, int nDroppedEntityPersonID)
{
	SetEntityToSecondRow(nDroppedEntityID, nDroppedEntityType, nDroppedEntityPersonID);

	enableDisableButtons();
	setLabelsAndSapne();

	ui.calendarWidget->UpdateCalendarFromFui();
	ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_showAssignations_toolButton_clicked()
{
	DbRecordSet recContacts, recResources, recProjects;
	Status err;

	DbRecordSet recFilter;
	GetFilterRecordset(recFilter);

	int nCurrentEntityType = m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt();
	int nCurentEntitityID = m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt();
	int nTrueEntityType = nCurrentEntityType;
	if (nCurrentEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
		nCurrentEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;
	
	_SERVER_CALL(BusCalendar->ReadEventPartEntitesForOneEntity(err, recFilter, nCurrentEntityType, nCurentEntitityID, m_datFrom, m_datTo, recContacts, recResources, recProjects));
	_CHK_ERR(err);

	//_DUMP(recContacts);
	//_DUMP(recResources);
	//_DUMP(recProjects);

	CreateEntitiesRecordSets(recContacts, recResources, recProjects);
}

void Fui_Calendar::setLabelsAndSapne()
{
	if (!m_recFixedEntites.getRowCount())
	{
		ui.userCount_label->setText("");
		ui.resource_label->setText("");
		return;
	}
	
	QString strCountLabel = QVariant(m_nCurentRow+1).toString() + "/" + QVariant(m_recFixedEntites.getRowCount()).toString();
	ui.userCount_label->setText(strCountLabel);

	QString strResourceLabel;

	int nEntityType = m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt();
	int nCurentEntitityID = m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt();
	ui.stackedWidget->setCurrentIndex(nEntityType);
	if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
	{
		int nPersonID = m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_PERSON_ID").toInt();
		ui.frameUser->blockSignals(true);
		ui.frameUser->SetCurrentEntityRecord(nPersonID);
		ui.frameUser->blockSignals(false);
		strResourceLabel = tr("User");
	}
	else if (nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
	{
		ui.frameProject->blockSignals(true);
		ui.frameProject->SetCurrentEntityRecord(nCurentEntitityID);
		ui.frameProject->blockSignals(false);
		strResourceLabel = tr("Project");
	}
	else if (nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT)
	{
		ui.frameContact->blockSignals(true);
		ui.frameContact->SetCurrentEntityRecord(nCurentEntitityID);
		ui.frameContact->blockSignals(false);
		strResourceLabel = tr("Contact");
	}
	else if (nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
	{
		ui.frameResource->blockSignals(true);
		ui.frameResource->SetCurrentEntityRecord(nCurentEntitityID);
		ui.frameResource->blockSignals(false);
		strResourceLabel = tr("Resource");
	}

	ui.resource_label->setText(strResourceLabel);
}

void Fui_Calendar::ReorderEntitiesRecordSet()
{
	m_recEntites.clear();
	int nCounter = m_nCurentRow;
	int nRowCount = m_recFixedEntites.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		int nRow = m_recEntites.getRowCount();
		m_recEntites.addRow();

		if (nCounter > (nRowCount-1))
			nCounter = 0;
		//_DUMP(m_recFixedEntites);
		int nEntityID = m_recFixedEntites.getDataRef(nCounter, "ENTITY_ID").toInt();
		int nEntityType = m_recFixedEntites.getDataRef(nCounter, "ENTITY_TYPE").toInt();
		QString strName = m_recFixedEntites.getDataRef(nCounter, "ENTITY_NAME").toString();
		int nPersonID = m_recFixedEntites.getDataRef(nCounter, "ENTITY_PERSON_ID").toInt();
		m_recEntites.setData(nRow, "ENTITY_ID", nEntityID);
		m_recEntites.setData(nRow, "ENTITY_TYPE", nEntityType);
		m_recEntites.setData(nRow, "ENTITY_NAME", strName);
		m_recEntites.setData(nRow, "ENTITY_PERSON_ID", nPersonID);
		nCounter++;
	}
}

void Fui_Calendar::SetEntityToSecondRow(int nDroppedEntityID, int nDroppedEntityType, int nDroppedEntityPersonID)
{
	m_recEntites.clearSelection();
	m_recEntites.find("ENTITY_ID", nDroppedEntityID);
	m_recEntites.find("ENTITY_TYPE", nDroppedEntityType, false, true, true);
	int nRow = m_recEntites.getSelectedRow();
	DbRecordSet tmp;
	tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	tmp.addRow(2);
	
	DbRecordSet row2 = m_recEntites.getRow(0);
	tmp.assignRow(0,row2);
	
	DbRecordSet row1 = m_recEntites.getRow(nRow);
	tmp.assignRow(1,row1);

	int nRecCount=m_recEntites.getRowCount();
	for (int i = 1; i < nRecCount; i++)
	{
		if (i==nRow)
			continue;

		int newLastRow=tmp.getRowCount();
		tmp.addRow();
		DbRecordSet row = m_recEntites.getRow(i);
		tmp.assignRow(newLastRow, row);
	}

	m_recEntites = tmp;
}

void Fui_Calendar::on_addUser_toolButton_clicked()
{
	DbRecordSet empty;
	int nResult=m_dlgUserPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		m_dlgUserPopUp.GetSelectedData(nRecordID,record);
		int nRowCount = record.getRowCount();
		//_DUMP(record);
		for (int i = 0; i < nRowCount; i++)
		{
			int nBPER_ID = record.getDataRef(i, "BPER_ID").toInt();
			int nContactID = record.getDataRef(i, "BPER_CONTACT_ID").toInt();
			if (!EntityAlreadyExist(nContactID, GlobalConstants::TYPE_CAL_USER_SELECT))
				addEntity(nContactID, GlobalConstants::TYPE_CAL_USER_SELECT, record.getDataRef(i, "BPER_NAME").toString(), nBPER_ID);
		}
	}
	enableDisableButtons();
	setLabelsAndSapne();

	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());

	if (m_bShowTasks)
		ui.frame_2->Reload(&m_recFixedEntites);
}

void Fui_Calendar::on_addProject_toolButton_clicked()
{
	DbRecordSet empty;
	int nResult=m_dlgProjectPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		m_dlgProjectPopUp.GetSelectedData(nRecordID,record);
		int nRowCount = record.getRowCount();
		//_DUMP(record);
		for (int i = 0; i < nRowCount; i++)
		{
			int nBUSP_ID = record.getDataRef(i, "BUSP_ID").toInt();
			if (!EntityAlreadyExist(nBUSP_ID, GlobalConstants::TYPE_CAL_PROJECT_SELECT))
				addEntity(nBUSP_ID, GlobalConstants::TYPE_CAL_PROJECT_SELECT, record.getDataRef(i, "BUSP_NAME").toString());
		}
	}
	enableDisableButtons();
	setLabelsAndSapne();

	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_addContact_toolButton_clicked()
{
	DbRecordSet empty;
	int nResult=m_dlgContactPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		m_dlgContactPopUp.GetSelectedData(nRecordID,record);
		int nRowCount = record.getRowCount();
		//_DUMP(record);
		for (int i = 0; i < nRowCount; i++)
		{
			int nBCNT_ID = record.getDataRef(i, "BCNT_ID").toInt();
			if (!EntityAlreadyExist(nBCNT_ID, GlobalConstants::TYPE_CAL_CONTACT_SELECT))
				addEntity(nBCNT_ID, GlobalConstants::TYPE_CAL_CONTACT_SELECT, record.getDataRef(i, "BCNT_NAME").toString());
		}
	}
	enableDisableButtons();
	setLabelsAndSapne();

	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_addResource_toolButton_clicked()
{
	DbRecordSet empty;
	int nResult=m_dlgResourcePopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		m_dlgResourcePopUp.GetSelectedData(nRecordID,record);
		int nRowCount = record.getRowCount();
		//_DUMP(record);
		for (int i = 0; i < nRowCount; i++)
		{
			int nBRES_ID = record.getDataRef(i, "BRES_ID").toInt();
			if (!EntityAlreadyExist(nBRES_ID, GlobalConstants::TYPE_CAL_RESOURCE_SELECT))
				addEntity(nBRES_ID, GlobalConstants::TYPE_CAL_RESOURCE_SELECT, record.getDataRef(i, "BRES_NAME").toString());
		}
	}
	enableDisableButtons();
	setLabelsAndSapne();

	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_reload_toolButton_clicked()
{
	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();
	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_saveView_toolButton_clicked()
{
	int nViewID = ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex()).toInt();
	QString strViewName = ui.viewSelect_comboBox->currentText();
	CalViewSelectDialog dialog(nViewID, strViewName);
	connect(&dialog, SIGNAL(returnData(int, QString, bool)), this, SLOT(saveView(int, QString, bool)));
	dialog.exec();
	//SavePersonalSettings();
}

void Fui_Calendar::saveView(int nViewID, QString strViewName, bool bIsPublic)
{
	Status err;
	bool bReloadComboBox = false;
	int nLoggedPersonID = g_pClientManager->GetPersonID();
	DbRecordSet recView, recEntities, recCheckedItemsIDs;
	if(nViewID>=0)
	{
		_SERVER_CALL(BusCalendar->GetCalFilterView(err, recView, recEntities, recCheckedItemsIDs, nViewID));
	}
	else
	{
		bReloadComboBox = true;
	}
	
	//_DUMP(recCheckedItems);
	//_DUMP(recViewSettings);
	//_DUMP(m_recFixedEntites);
	
	bool bFilterByNoType;
	bool bFilterByType;
	bool bShowPreliminary;
	bool bShowInformation;
	int nRangeMinute;
	DbRecordSet recCheckedItems;
	ui.rangeFrame->GetData(nRangeMinute, recCheckedItems, bFilterByType, bShowPreliminary, bShowInformation, bFilterByNoType);

	//Checked type items ids
	recCheckedItemsIDs.destroy();
	recCheckedItemsIDs.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW_CE_TYPES));
	if (recCheckedItems.getRowCount())
	{
		int nCheckedIDsCount=recCheckedItems.getRowCount();
		for (int i=0; i<nCheckedIDsCount; i++)
		{
			recCheckedItemsIDs.addRow();
			recCheckedItemsIDs.setData(i, "BCCT_TYPE_ID",recCheckedItems.getDataRef(i, "CHECKED_ITEM_ID").toInt());
			recCheckedItemsIDs.setData(i, "BCCT_IS_SELECTED",recCheckedItems.getDataRef(i, "CHECKED_ITEM_STATE").toInt()); //Qt::CheckedState.
		}
	}
	//Entities.
	recEntities.destroy();
	recEntities.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW_ENTITIES));
	if (m_recFixedEntites.getRowCount())
	{
		int nEntitiesCount=m_recFixedEntites.getRowCount();
		for (int i=0; i<nEntitiesCount; i++)
		{
			recEntities.addRow();

			int nEntityType=m_recFixedEntites.getDataRef(i, "ENTITY_TYPE").toInt();
			int nEntitiyID=m_recFixedEntites.getDataRef(i, "ENTITY_ID").toInt();
			int nEntityPersonID=m_recFixedEntites.getDataRef(i, "ENTITY_PERSON_ID").toInt();
			int bIsSelected=(m_nCurentRow==i)? true : false;
			
			CalendarHelperCore::AddEntityRowToEntityRecordset(recEntities, i, nEntityType, nEntitiyID, nEntityPersonID, bIsSelected, i);
		}
	}

	//Calendar view data.
	QByteArray bytSplitterState = ui.calendarWidget->GetSpliterState();
	m_startScrollToTime = ui.calendarWidget->GetTopLeftVisibleItem().time();
	QByteArray bytWindowGeometry;
	if (IsStandAlone())
	{
		bytWindowGeometry = saveGeometry();
	}
	QPoint ptFuiPosition = pos();
	int nUpperCalendarColumnWidth=ui.calendarWidget->getVertHeaderWidthForUpperCalendar();
	int nOpenOnStartup=0;
	int nIsPublic=QVariant(bIsPublic).toInt();
	int nCalendarType=0;	//Default type - for now.
	int nFilterByNoType=QVariant(bFilterByNoType).toInt();
	QString strSortCode=QString();
	int nViewType=0;		//Just to put something.
	int nFilterByType=QVariant(bFilterByType).toInt();
	int nShowPreliminary=QVariant(bShowPreliminary).toInt();
	int nShowInformation=QVariant(bShowInformation).toInt();
	QDateTime datStartScrollToTime=QDateTime(QDate::currentDate(), m_startScrollToTime);
	int nShowTasks=QVariant(m_bShowTasks = ui.frame_2->isVisible()).toInt();

	CalendarHelperCore::CreateCalendarViewRecordset(recView,strViewName,nOpenOnStartup,nIsPublic,nCalendarType,nLoggedPersonID,
		m_nCalendarViewSections, m_nDateRange, strSortCode,nViewType,nFilterByNoType,nFilterByType,nShowPreliminary,nShowInformation,
		nRangeMinute,datStartScrollToTime, bytSplitterState,bytWindowGeometry, ptFuiPosition.x(), ptFuiPosition.y(), nShowTasks, 
		nUpperCalendarColumnWidth);

	_SERVER_CALL(BusCalendar->SaveCalFilterView(err, nViewID, recView, recEntities, recCheckedItemsIDs));
	_CHK_ERR(err);
	
	if (bReloadComboBox)
	{
		LoadViewSettings(nViewID);
		LoadViewComboBox(nViewID);
		ui.viewSelect_comboBox->setCurrentIndex(ui.viewSelect_comboBox->findData(nViewID));
	}
	else
		LoadViewSettings(nViewID);
}

void Fui_Calendar::on_viewSelect_comboBox_currentIndexChanged(int index)
{
	int nViewID = ui.viewSelect_comboBox->itemData(index).toInt();
	m_nViewID = nViewID;

	LoadViewSettings(nViewID);

	m_bResizeColumnsToFit = true;

	Initialize(m_datFrom.date(), m_datTo.date(), NULL, NULL, NULL, NULL, &m_recEntitesFromPersonalSetting);
}

void Fui_Calendar::on_deleteView_toolButton_clicked()
{
	int nIndex = ui.viewSelect_comboBox->currentIndex();
	
	if (nIndex<0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please select one View first!"));
		return;
	}
	else
	{
		QMessageBox message(QMessageBox::Warning, tr("Delete View"), tr("Do you really want to delete this view?"), QMessageBox::Ok | QMessageBox::Cancel);
		if(message.exec() == QMessageBox::Cancel)
			return;
	}

	int nViewID = ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex()).toInt();
	Status err;
	_SERVER_CALL(BusCalendar->DeleteCalFilterView(err, nViewID));
	_CHK_ERR(err);

	LoadViewSettings();
}

void Fui_Calendar::on_ItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo)
{
	Status err;
	_SERVER_CALL(BusCalendar->ModifyEventDateRange(err, nBcepID, nBCOL_ID, datItemFrom, datSelectionTo));
	_CHK_ERR(err);
	ModifyOneEvent(nBcevID);
////	InitializeCalendarWidget();
////	GetCalendarItems();
}

void Fui_Calendar::on_MultiDayItemResized(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo)
{
	Status err;
	_SERVER_CALL(BusCalendar->ModifyEventDateRange(err, nBcepID, nBCOL_ID, datSelectionFrom, datSelectionTo));
	_CHK_ERR(err);

	ModifyOneEvent(nBcevID);
	////	InitializeCalendarWidget();
	////	GetCalendarItems();
}

void Fui_Calendar::on_ItemMoved(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo)
{
	QDate datNewFrom = datSelectionFrom.date();
	int nMilisecondsFromTo = datItemFrom.time().msecsTo(datItemTo.time());
	QTime timeNewTo(datSelectionFrom.time().addMSecs(nMilisecondsFromTo));
	//QString sss = timeNewTo.toString();
	QDateTime datetimeNewTo(datNewFrom, timeNewTo);
	//QString sssss = datetimeNewTo.toString();

	Status err;
	_SERVER_CALL(BusCalendar->ModifyEventDateRange(err, nBcepID, nBCOL_ID, datSelectionFrom, datetimeNewTo));
	_CHK_ERR(err);

	ModifyOneEvent(nBcevID);
}

void Fui_Calendar::on_MultyDayItemMoved(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType, int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, QDateTime datItemFrom, QDateTime datItemTo)
{
	QDate datNewFrom = datSelectionFrom.date();
	//QString s = datItemFrom.toString();
	//QString ss = datItemTo.toString();
	int nSecondsFromTo = datItemFrom.secsTo(datItemTo);
	QDateTime datetimeNewTo = datSelectionFrom.addSecs(nSecondsFromTo);
	//QString sssss = datetimeNewTo.toString();

	Status err;
	_SERVER_CALL(BusCalendar->ModifyEventDateRange(err, nBcepID, nBCOL_ID, datSelectionFrom, datetimeNewTo));
	_CHK_ERR(err);

	ModifyOneEvent(nBcevID);
}

void Fui_Calendar::on_DeleteItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType)
{
	QMessageBox message(QMessageBox::Warning, tr("Delete Event"), tr("Do you really want to delete this item?"), QMessageBox::Ok | QMessageBox::Cancel);
	if(message.exec() == QMessageBox::Cancel)
		return;
	Status err;
	_SERVER_CALL(BusCalendar->DeleteEventPart(err,nBcepID, nBCOL_ID, nBcevID));
	_CHK_ERR(err);

	RemoveSingleDayEvent(nCentID, nBcepID, nBcevID, nBCOL_ID);
	DbRecordSet recTmp;
	recTmp.addColumn(QVariant::Int, "BCEV_ID");
	recTmp.addRow();
	recTmp.setData(0, "BCEV_ID", nBcevID);

	QVariant varData;
	qVariantSetValue(varData,recTmp);

	updateObserver(this, ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED, ENTITY_CALENDAR_EVENT, varData);
}

void Fui_Calendar::on_ItemsDelete(DbRecordSet recSelectedItemsData)
{
	int nRowCount = recSelectedItemsData.getRowCount();
	QString strMessage(tr("Do you really want to delete "));
	strMessage += QVariant(nRowCount).toString();
	strMessage += QString(tr(" items?"));
	QMessageBox message(QMessageBox::Warning, tr("Delete Event"), strMessage, QMessageBox::Ok | QMessageBox::Cancel);
	if(message.exec() == QMessageBox::Cancel)
		return;

	Status err;
	_SERVER_CALL(BusCalendar->DeleteEventParts(err, recSelectedItemsData));
	_CHK_ERR(err);
}

void Fui_Calendar::on_MultyDayDeleteItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	QMessageBox message(QMessageBox::Warning, tr("Delete Event"), tr("Do you really want to delete this item?"), QMessageBox::Ok | QMessageBox::Cancel);
	if(message.exec() == QMessageBox::Cancel)
		return;
	Status err;
	_SERVER_CALL(BusCalendar->DeleteEventPart(err,nBcepID, nBCOL_ID, nBcevID));
	_CHK_ERR(err);

	RemoveMultiDayEvent(nCentID, nBcepID, nBcevID, nBCOL_ID);
}

void Fui_Calendar::on_ModifyItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT,true,false,FuiBase::MODE_EDIT, nBcevID);
	FUI_CalendarEvent* pFui=dynamic_cast<FUI_CalendarEvent*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->ShowPart(nBcepID,nBCOL_ID);
		pFui->setFocus();
	}
	QApplication::restoreOverrideCursor();
}


void Fui_Calendar::on_ViewItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT,true,false,FuiBase::MODE_READ, nBcevID);
	FUI_CalendarEvent* pFui=dynamic_cast<FUI_CalendarEvent*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->ShowPart(nBcepID,nBCOL_ID);
		pFui->setFocus();
	}
	QApplication::restoreOverrideCursor();
}



void Fui_Calendar::on_DropEntityOnItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, bool bDroppedOnFirstRow)
{
	int nEntityRow = dynamic_cast<CalendarHeaderTableModel*>(ui.calendarWidget->HeaderTableModel())->getRowForEntity(nEntityID, nEntityType)-1;
	if (bDroppedOnFirstRow)
		ReorderEntityRecordSets(nEntityRow, nEntityID, nEntityType);



	bool bSkipAssignment=false;
	bool bAS_Customer=false;
	if(CustomAvailability::IsAvailable_AS_CalendarViewWizard())
	{
		int nID=ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex()).toInt();
		int nRow=m_recCalendarViews.find("BCALV_ID",nID,true);
		if (nRow>0)
		{
			bool bEngageEnemy=false;
			QString strType = ClientContactManager::GetTypeName(m_recCalendarViews.getDataRef(nRow,"BCALV_TYPE").toInt());
			if (strType=="Service Plan")
				bAS_Customer=true;
			else if (strType=="Plan Operating Room")
				bAS_Customer=true;
			else if (strType=="Overview Operating Rooms")
				bAS_Customer=true;
			else if (strType=="Plan Anesthetist")
				bAS_Customer=true;
			else if (strType=="Overview Anesthetists")
				bAS_Customer=true;
		}

		//if then filter if current user:
		if (m_recEntites.getDataRef(0,"ENTITY_TYPE").toInt()==nEntityType && m_recEntites.getDataRef(0,"ENTITY_ID").toInt()==nEntityID)
			bSkipAssignment=true;
	}


	DbRecordSet recEntity;
	//	if(nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
	//		recEntity.addColumn(QVariant::Int, "BPER_ID");
	if(nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
		recEntity.addColumn(QVariant::Int, "BUSP_ID");
	else if((nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT) || (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT))
		recEntity.addColumn(QVariant::Int, "BCNT_ID");
	else if(nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
		recEntity.addColumn(QVariant::Int, "BRES_ID");

	recEntity.addRow();
	recEntity.setData(0, 0, nEntityID);

	QApplication::setOverrideCursor(Qt::WaitCursor);
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT,true,false,FuiBase::MODE_EDIT, nBcevID);
	FUI_CalendarEvent* pFui=dynamic_cast<FUI_CalendarEvent*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		//if (!bSkipAssignment)
		//{
			if(nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
				pFui->AssignEntities(nBcepID,NULL, &recEntity);
			else if(nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT || nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
				pFui->AssignEntities(nBcepID,&recEntity);
			else if(nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
				pFui->AssignEntities(nBcepID,NULL, NULL, &recEntity);
		//}
			
		pFui->ShowPart(nBcepID);
		pFui->setFocus();
	}
	QApplication::restoreOverrideCursor();
}

void Fui_Calendar::on_InsertNewItem(QDateTime datSelectionFrom, QDateTime datSelectionTo, int nEntityID, int nEntityType)
{

	bool bSkipAssignment=false;
	bool bAS_Customer=false;
	if(CustomAvailability::IsAvailable_AS_CalendarViewWizard())
	{
		int nID=ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex()).toInt();
		int nRow=m_recCalendarViews.find("BCALV_ID",nID,true);
		if (nRow>0)
		{
			bool bEngageEnemy=false;
			QString strType = ClientContactManager::GetTypeName(m_recCalendarViews.getDataRef(nRow,"BCALV_TYPE").toInt());
			if (strType=="Service Plan")
				bAS_Customer=true;
			else if (strType=="Plan Operating Room")
				bAS_Customer=true;
			else if (strType=="Overview Operating Rooms")
				bAS_Customer=true;
			else if (strType=="Plan Anesthetist")
				bAS_Customer=true;
			else if (strType=="Overview Anesthetists")
				bAS_Customer=true;
		}

		//if then filter if current user:
		if (m_recEntites.getDataRef(0,"ENTITY_TYPE").toInt()==nEntityType && m_recEntites.getDataRef(0,"ENTITY_ID").toInt()==nEntityID)
			bSkipAssignment=true;
	}

	//B.T. 15.02.2012->Marin reported that users are not assigned to cal event
	if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
	{
		DbRecordSet lstPerson;
		Status err;
		_SERVER_CALL(ClientSimpleORM->Read(err,BUS_PERSON,lstPerson," WHERE BPER_ID="+QString::number(nEntityID),DbSqlTableView::TVIEW_BUS_PERSON_SELECTION));
		if (lstPerson.getRowCount()>0)
		{
			nEntityID=lstPerson.getDataRef(0,"BPER_CONTACT_ID").toInt();
		}
	}

	//m_recEntites.Dump();


	DbRecordSet recEntity;
//	if(nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
//		recEntity.addColumn(QVariant::Int, "BPER_ID");
	 if(nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
		recEntity.addColumn(QVariant::Int, "BUSP_ID");
	else if((nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT) || (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT))
		recEntity.addColumn(QVariant::Int, "BCNT_ID");
	else if(nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
		recEntity.addColumn(QVariant::Int, "BRES_ID");

	recEntity.addRow();
	recEntity.setData(0, 0, nEntityID);

	int	nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT,true,false,FuiBase::MODE_INSERT);
	FUI_CalendarEvent *pFui = dynamic_cast<FUI_CalendarEvent*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		DbRecordSet row;
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
		row.addRow();
		row.setData(0, "BCEV_FROM", datSelectionFrom);
		row.setData(0, "BCEV_TO", datSelectionTo);

		//B.T: removed user
		//if(nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
		//	pFui->SetDefaults(true,&row, NULL, &recEntity);
		if (!bSkipAssignment)
		{
			if(nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
				pFui->SetDefaults(true,&row, NULL, &recEntity,NULL,false,false,-1,bAS_Customer);
			else if(nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT || nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
				pFui->SetDefaults(true,&row, &recEntity,NULL,NULL,false,false,-1,bAS_Customer);
			else if(nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
				pFui->SetDefaults(true,&row, NULL, NULL, &recEntity,false,false,-1,bAS_Customer);
		}
		else
		{
				pFui->SetDefaults(true,&row,NULL,NULL,NULL,false,false,-1,bAS_Customer); //indicate that this is mode for ASsholes customers
		}
	}
}

void Fui_Calendar::on_selectionChanged(QDateTime datSelectionFrom, QDateTime datSelectionTo, DbRecordSet recSelectedEntities)
{
	//Check for person (owner) id. Issue #2327.
	//Logged person ID.
	int nOwnerID = -1;
	int nLoggedPersonID = g_pClientManager->GetPersonID();
	bool bLoggedPersonIsInEntities = false;
	int nEntitiesCount = m_recFixedEntites.getRowCount();
	for(int i = 0; i < nEntitiesCount; i++)
	{
		if (m_recFixedEntites.getDataRef(i, "ENTITY_PERSON_ID").toInt() == nLoggedPersonID)
			bLoggedPersonIsInEntities = true;
	}

	if (bLoggedPersonIsInEntities)
		nOwnerID = nLoggedPersonID;
	else
	{
		if (m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_PERSON_ID").toInt() >= 0)
			nOwnerID = m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_PERSON_ID").toInt();
		else
		{
			for(int i = 0; i < nEntitiesCount; i++)
			{
				if (m_recFixedEntites.getDataRef(i, "ENTITY_PERSON_ID").toInt()>=0)
				{
					nOwnerID = m_recFixedEntites.getDataRef(i, "ENTITY_PERSON_ID").toInt();
					break;
				}
			}
		}

		if (nOwnerID == -1)
			nOwnerID = nLoggedPersonID;
	}

	//Create recordsets.
	DbRecordSet recContacts, recProjects, recResources;
	recProjects.addColumn(QVariant::Int, "BUSP_ID");
	recContacts.addColumn(QVariant::Int, "BCNT_ID");
	recResources.addColumn(QVariant::Int, "BRES_ID");

	int nRowCount = recSelectedEntities.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nEntityID = recSelectedEntities.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = recSelectedEntities.getDataRef(i, "ENTITY_TYPE").toInt();
		int nEntityPersonID = recSelectedEntities.getDataRef(i, "ENTITY_PERSON_ID").toInt();
		
		if(nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
		{
			recProjects.addRow();
			int nRow = recProjects.getRowCount()-1;
			recProjects.setData(nRow, "BUSP_ID", nEntityID);
		}
		else if((nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT) || (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT))
		{
			recContacts.addRow();
			int nRow = recContacts.getRowCount()-1;
			recContacts.setData(nRow, "BCNT_ID", nEntityID);
		}
		else if(nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
		{
			recResources.addRow();
			int nRow = recResources.getRowCount()-1;
			recResources.setData(nRow, "BRES_ID", nEntityID);
		}
	}

	int	nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT,true,false,FuiBase::MODE_INSERT);
	FUI_CalendarEvent *pFui = dynamic_cast<FUI_CalendarEvent*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		DbRecordSet row;
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
		row.addRow();
		row.setData(0, "BCEV_FROM", datSelectionFrom);
		row.setData(0, "BCEV_TO", datSelectionTo);

		pFui->SetDefaults(true,&row, &recContacts, &recProjects, &recResources);
		pFui->SetOwner(nOwnerID);
	}

	//_DUMP(recSelectedEntities);
}

void Fui_Calendar::SetupDateIntervalLabel(int nRange)
{
	//Update date selector label.
	if (nRange == 1)
	{
		QString strText;
		int nDayOfWeek = m_datFrom.date().dayOfWeek();
		if (nDayOfWeek == Qt::Monday)
			strText = tr("Monday");
		else if (nDayOfWeek == Qt::Tuesday)
			strText = tr("Tuesday");
		else if (nDayOfWeek == Qt::Wednesday)
			strText = tr("Wednesday");
		else if (nDayOfWeek == Qt::Thursday)
			strText = tr("Thursday");
		else if (nDayOfWeek == Qt::Friday)
			strText = tr("Friday");
		else if (nDayOfWeek == Qt::Saturday)
			strText = tr("Saturday");
		else if (nDayOfWeek == Qt::Sunday)
			strText = tr("Sunday");

		ui.dateSelector_label->setText(strText);
	}
	else if (nRange == 2 || nRange == 6)
	{
		QString strText(tr("Week"));
		strText += " ";
		strText += QVariant(m_datFrom.date().weekNumber()).toString();
		ui.dateSelector_label->setText(strText);
	}
	else if (nRange == 3)
	{
		QString strText;
		int nMonthOfYear = m_datFrom.date().month();
		if (nMonthOfYear == 1)
			strText = tr("January");
		else if (nMonthOfYear == 2)
			strText = tr("February");
		else if (nMonthOfYear == 3)
			strText = tr("March");
		else if (nMonthOfYear == 4)
			strText = tr("April");
		else if (nMonthOfYear == 5)
			strText = tr("May");
		else if (nMonthOfYear == 6)
			strText = tr("June");
		else if (nMonthOfYear == 7)
			strText = tr("July");
		else if (nMonthOfYear == 8)
			strText = tr("August");
		else if (nMonthOfYear == 9)
			strText = tr("September");
		else if (nMonthOfYear == 10)
			strText = tr("October");
		else if (nMonthOfYear == 11)
			strText = tr("November");
		else if (nMonthOfYear == 12)
			strText = tr("December");

		ui.dateSelector_label->setText(strText);
	}
	else if (nRange == 4)
	{
		ui.dateSelector_label->setText(QVariant(m_datFrom.date().year()).toString());
	}
	else
	{
		ui.dateSelector_label->setText("");
	}
}

void Fui_Calendar::GetFilterRecordset(DbRecordSet &recFilter)
{
	recFilter.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_FILTER_VIEW));
	recFilter.addRow();
	
	bool bFilterByType, bShowPreliminary, bShowInformation, bFilterByNoType;
	int nRangeMinute;
	DbRecordSet recCheckedItems;
	
	ui.rangeFrame->GetData(nRangeMinute, recCheckedItems, bFilterByType, bShowPreliminary, bShowInformation, bFilterByNoType);
	
	int nShowPreliminary = 0;
	int nShowInformation = 0;
	if (bShowPreliminary)
		nShowPreliminary = 1;
	if (bShowInformation)
		nShowInformation = 1;
	
	recFilter.setData(0, "SHOW_PRELIMINARY_EVENTS", nShowPreliminary);
	recFilter.setData(0, "SHOW_INFORMATION_EVENTS", nShowInformation);
	recFilter.setData(0, "FILTER_BY_CALENDAR_TYPE", QVariant(bFilterByType).toInt());
	QByteArray byte = XmlUtil::ConvertRecordSet2ByteArray_Fast(recCheckedItems);
	recFilter.setData(0, "CALENDAR_TYPE_RECORDSET", byte);
}

void Fui_Calendar::LoadViewSettings(int nViewID /*= -1*/)
{
	int nLoggedPersonID = g_pClientManager->GetPersonID();
	Status err;
	_SERVER_CALL(BusCalendar->GetCalFilterViews(err, m_recCalendarViews, nLoggedPersonID, -1));
	_CHK_ERR(err);

	//_DUMP(m_recCalendarViews);

	int nSavedViewID = 0;
	if (nViewID > 0)
	{
		m_nViewID = nViewID;
		nSavedViewID = nViewID;
	}
	else
		nSavedViewID = g_pSettings->GetPersonSetting(CALENDAR_VIEW_SETTINGS).toInt();
	
	LoadViewComboBox(nSavedViewID);

	bool bFilterByNoType = false;
	bool bFilterByType = false;
	bool bShowPreliminary = true;
	bool bShowInformation = true;
	QByteArray bytSplitterState;
	m_startScrollToTime = QTime(6,0);

	QDate datFrom, datTo;
	DataHelper::GetCurrentWeek(QDate::currentDate(),datFrom, datTo);
	m_datFrom = QDateTime(datFrom, QTime(0, 0, 0));
	m_datTo = QDateTime(datTo, QTime(23, 59, 59));
	m_nDateRange = 2;		//Same as week in date filter frame (DateRangeSelector2).
	m_TimeScale = CalendarWidget::HALF_HOUR;//Half hour.
//	m_TimeScale = CalendarWidget::TEN_MINUTES;//Half hour.
	m_nCurentRow = 0;
	QByteArray bytWindowGeometry;
	m_bShowTasks = false;
	m_nCalendarViewSections=0;	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER.

	//_DUMP(recSettings);

	DbRecordSet recCurrentView, recCurrentViewEntities, recCurrentViewTypeIDs;
	recCurrentViewEntities.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	recCurrentViewTypeIDs.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW_CE_TYPES));

	if (m_recCalendarViews.find("BCALV_ID", nSavedViewID, false, false, true))
	{
		_SERVER_CALL(BusCalendar->GetCalFilterView(err, recCurrentView, recCurrentViewEntities, recCurrentViewTypeIDs, nSavedViewID));
		_CHK_ERR(err);
	}

//	_DUMP(recCurrentView);
//	_DUMP(recCurrentViewEntities);
//	_DUMP(recCurrentViewTypeIDs);

	if (recCurrentView.getRowCount())
	{
		m_TimeScale = (CalendarWidget::timeScale)recCurrentView.getDataRef(0, "BCALV_TIME_SCALE").toInt();
		bFilterByType = recCurrentView.getDataRef(0, "BCALV_FILTER_BY_TYPE").toBool();
		bShowPreliminary = recCurrentView.getDataRef(0, "BCALV_SHOW_PRELIMINARY").toBool();
		bShowInformation = recCurrentView.getDataRef(0, "BCALV_SHOW_INFORMATION").toBool();
		bFilterByNoType = recCurrentView.getDataRef(0, "BCALV_FILTER_BY_NOTYPE").toBool();
		m_nDateRange = recCurrentView.getDataRef(0, "BCALV_DATE_RANGE").toInt();
		m_startScrollToTime = recCurrentView.getDataRef(0, "BCALV_SCROLL_TO_TIME").toTime();
		bytSplitterState = recCurrentView.getDataRef(0, "BCALV_MULTIDAY_HEIGHT").toByteArray();
		DataHelper::CalendarRangeSelectorClicked(datFrom, datTo, m_nDateRange);
		m_datFrom = QDateTime(datFrom, QTime(0, 0, 0));
		m_datTo = QDateTime(datTo, QTime(23, 59, 59));
		bytWindowGeometry = recCurrentView.getDataRef(0, "BCALV_FUI_GEOMETRY").toByteArray();
		m_ptFuiPosition = QPoint(recCurrentView.getDataRef(0, "BCALV_FUI_POSITION_X").toInt(), recCurrentView.getDataRef(0, "BCALV_FUI_POSITION_Y").toInt());
		m_bShowTasks = recCurrentView.getDataRef(0, "BCALV_SHOW_TASKS").toBool();
		m_nCalendarViewSections = recCurrentView.getDataRef(0, "BCALV_SECTIONS").toInt();
		//recCurrentView.getDataRef(0, "BCALV_OVERVIEWLEFTCOLWIDTH").toInt(); Not used for who know why?
	}

	if (recCurrentViewTypeIDs.getRowCount())
	{
		DbRecordSet recTmpTypeIDs;
		recTmpTypeIDs.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS));

//		_DUMP(recTmpTypeIDs);

		int nCheckedIDsCount=recCurrentViewTypeIDs.getRowCount();
		for (int i=0; i<nCheckedIDsCount; i++)
		{
			recTmpTypeIDs.addRow();
			recTmpTypeIDs.setData(i, "CHECKED_ITEM_ID", recCurrentViewTypeIDs.getDataRef(i, "BCCT_TYPE_ID").toInt());
			recTmpTypeIDs.setData(i, "CHECKED_ITEM_STATE", recCurrentViewTypeIDs.getDataRef(i, "BCCT_IS_SELECTED").toInt()); //Qt::CheckedState.
		}

		recCurrentViewTypeIDs=recTmpTypeIDs;
	}

	if (recCurrentViewEntities.getRowCount())
	{
		//It should be sorted on server side.
		//recCurrentViewEntities.sort("BCVE_SORT_CODE");
		
		DbRecordSet recTmpEntities;
		recTmpEntities.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));

		int nEntitiesCount=recCurrentViewEntities.getRowCount();
		for (int i=0; i<nEntitiesCount; i++)
		{
			recTmpEntities.addRow();

			if (recCurrentViewEntities.getDataRef(i, "BCVE_IS_SELECTED").toInt())
			{
				m_nCurentRow=i;
			}

			if (recCurrentViewEntities.getDataRef(i,"BCVE_USER_ID").toInt()>0)
			{
				//User has stored both user and contact data.
				recTmpEntities.setData(i, "ENTITY_ID", recCurrentViewEntities.getDataRef(i, "BCVE_CONTACT_ID").toInt());
				recTmpEntities.setData(i, "ENTITY_PERSON_ID", recCurrentViewEntities.getDataRef(i, "BCVE_USER_ID").toInt());
				recTmpEntities.setData(i, "ENTITY_NAME", recCurrentViewEntities.getDataRef(i, "BPER_NAME").toString());
				recTmpEntities.setData(i, "ENTITY_TYPE", GlobalConstants::TYPE_CAL_USER_SELECT);
			}
			else if (recCurrentViewEntities.getDataRef(i,"BCVE_PROJECT_ID").toInt()>0)
			{
				recTmpEntities.setData(i, "ENTITY_ID", recCurrentViewEntities.getDataRef(i, "BCVE_PROJECT_ID").toInt());
				recTmpEntities.setData(i, "ENTITY_NAME", recCurrentViewEntities.getDataRef(i, "BUSP_NAME").toString());
				recTmpEntities.setData(i, "ENTITY_TYPE", GlobalConstants::TYPE_CAL_PROJECT_SELECT);
			}
			else if (recCurrentViewEntities.getDataRef(i,"BCVE_CONTACT_ID").toInt()>0)
			{
				recTmpEntities.setData(i, "ENTITY_ID", recCurrentViewEntities.getDataRef(i, "BCVE_CONTACT_ID").toInt());
				recTmpEntities.setData(i, "ENTITY_NAME", recCurrentViewEntities.getDataRef(i, "BCNT_NAME").toString());
				recTmpEntities.setData(i, "ENTITY_TYPE", GlobalConstants::TYPE_CAL_CONTACT_SELECT);
			}
			else if (recCurrentViewEntities.getDataRef(i,"BCVE_RESOURCE_ID").toInt()>0)
			{
				recTmpEntities.setData(i, "ENTITY_ID", recCurrentViewEntities.getDataRef(i, "BCVE_RESOURCE_ID").toInt());
				recTmpEntities.setData(i, "ENTITY_NAME", recCurrentViewEntities.getDataRef(i, "BRES_NAME").toString());
				recTmpEntities.setData(i, "ENTITY_TYPE", GlobalConstants::TYPE_CAL_RESOURCE_SELECT);
			}
		}

		recCurrentViewEntities=recTmpEntities;
		m_recEntitesFromPersonalSetting.clear();
		m_recEntitesFromPersonalSetting = recCurrentViewEntities;
	}

	//Splitter.
	if (!bytSplitterState.isEmpty())
		ui.calendarWidget->SetSpliterState(bytSplitterState);

	//Left frame.
	ui.rangeFrame->SetData(m_TimeScale, recCurrentViewTypeIDs, bFilterByType, bShowPreliminary, bShowInformation, bFilterByNoType, datFrom, datTo);

	//Fui geometry and position.
	if (!bytWindowGeometry.isEmpty())
		restoreGeometry(bytWindowGeometry);

	//Tasks show/hide.
	ui.showTasks_checkBox->setChecked(m_bShowTasks);
	ui.frame_2->setVisible(m_bShowTasks);
}

void Fui_Calendar::LoadViewComboBox(int nViewID)
{
	ui.viewSelect_comboBox->blockSignals(true);
	//Fill combo.
	ui.viewSelect_comboBox->clear();
	int nRowCount = m_recCalendarViews.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		int ViewID = m_recCalendarViews.getDataRef(i, "BCALV_ID").toInt();
		QString strViewName = m_recCalendarViews.getDataRef(i, "BCALV_NAME").toString();
		ui.viewSelect_comboBox->addItem(strViewName, ViewID);
	}
	//Set current item.
	ui.viewSelect_comboBox->setCurrentIndex(ui.viewSelect_comboBox->findData(nViewID));
	ui.viewSelect_comboBox->blockSignals(false);
}

void Fui_Calendar::CreateEntitiesRecordSets(DbRecordSet recContacts, DbRecordSet recResources, DbRecordSet recProjects)
{
	//Current entity.
	int nCurrentEntityID = m_recEntites.getDataRef(0, "ENTITY_ID").toInt();
	int nCurrentEntityType = m_recEntites.getDataRef(0, "ENTITY_TYPE").toInt();

	//Contacts.
	recContacts.clearSelection();
	DbRecordSet recTmpContacts;
	recTmpContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	int nContactsRowCount = recContacts.getRowCount();
	for (int i = 0; i < nContactsRowCount; i++)
	{
		int nEntityID = recContacts.getDataRef(i, "BCNT_ID").toInt();
		int nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;
		if ((nEntityID == nCurrentEntityID) && ((nEntityType == nCurrentEntityType) || (nCurrentEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)))
			continue;

		QString strName = recContacts.getDataRef(i, "BCNT_NAME").toString();
		recTmpContacts.clearSelection();
		recTmpContacts.find("ENTITY_ID", nEntityID);
		if (!recTmpContacts.find("ENTITY_TYPE", nEntityType, false, true, true))
		{
			recTmpContacts.addRow();
			int nRow = recTmpContacts.getRowCount()-1;
			recTmpContacts.setData(nRow, "ENTITY_ID", nEntityID);
			recTmpContacts.setData(nRow, "ENTITY_TYPE", nEntityType);
			recTmpContacts.setData(nRow, "ENTITY_NAME", strName);
		}
	}
	//_DUMP(recTmpContacts);

	//Resources.
	recResources.clearSelection();
	int nResourcesRowCount = recResources.getRowCount();
	DbRecordSet recTmpResources;
	recTmpResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	for (int i = 0; i < nResourcesRowCount; i++)
	{
		int nEntityID = recResources.getDataRef(i, "BRES_ID").toInt();
		int nEntityType = GlobalConstants::TYPE_CAL_RESOURCE_SELECT;
		if ((nEntityID == nCurrentEntityID) && (nEntityType == nCurrentEntityType))
			continue;
		QString strName = recResources.getDataRef(i, "BRES_NAME").toString();
		recTmpResources.clearSelection();
		recTmpResources.find("ENTITY_ID", nEntityID);
		if (!recTmpResources.find("ENTITY_TYPE", nEntityType, false, true, true))
		{
			recTmpResources.addRow();
			int nRow = recTmpResources.getRowCount()-1;
			recTmpResources.setData(nRow, "ENTITY_ID", nEntityID);
			recTmpResources.setData(nRow, "ENTITY_TYPE", nEntityType);
			recTmpResources.setData(nRow, "ENTITY_NAME", strName);
		}
	}
	//_DUMP(recTmpResources);

	//Projects.
	recProjects.clearSelection();
	int nProjectsRowCount = recProjects.getRowCount();
	DbRecordSet recTmpProjects;
	recTmpProjects.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	for (int i = 0; i < nProjectsRowCount; i++)
	{
		int nEntityID = recProjects.getDataRef(i, "BUSP_ID").toInt();
		int nEntityType = GlobalConstants::TYPE_CAL_PROJECT_SELECT;
		if ((nEntityID == nCurrentEntityID) && (nEntityType == nCurrentEntityType))
			continue;
		QString strName = recProjects.getDataRef(i, "BUSP_NAME").toString();
		recTmpProjects.clearSelection();
		recTmpProjects.find("ENTITY_ID", nEntityID);
		if (!recTmpProjects.find("ENTITY_TYPE", nEntityType, false, true, true))
		{
			recTmpProjects.addRow();
			int nRow = recTmpProjects.getRowCount()-1;
			recTmpProjects.setData(nRow, "ENTITY_ID", nEntityID);
			recTmpProjects.setData(nRow, "ENTITY_TYPE", nEntityType);
			recTmpProjects.setData(nRow, "ENTITY_NAME", strName);
		}
	}
	//_DUMP(recTmpProjects);

	//Create new fixed entity recordset.
	DbRecordSet recTmp;
	recTmp.copyDefinition(m_recFixedEntites);
	m_recEntites.clearSelection();
	m_recEntites.selectRow(0);
	recTmp.merge(m_recEntites, true);
	recTmp.merge(recTmpContacts);
	recTmp.merge(recTmpResources);
	recTmp.merge(recTmpProjects);
	int nRowCount = m_recEntites.getRowCount();
	for (int i = 1; i < nRowCount; i++)
	{
		m_recEntites.clearSelection();
		int nEntityID = m_recEntites.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = GlobalConstants::TYPE_CAL_PROJECT_SELECT;
		recTmp.find("ENTITY_ID", nEntityID);
		recTmp.find("ENTITY_TYPE", nEntityType, false, true, true);
		if (!recTmp.getSelectedCount())
			recTmp.merge(m_recEntites, true);
	}
	//m_recEntites.clear();
	//m_recFixedEntites.clear();
	//m_recEntites = recTmp;
	//m_recFixedEntites = recTmp;

	//_DUMP(m_recEntites);
	//_DUMP(m_recFixedEntites);
	m_nCurentRow = 0;

	////InitializeCalendarWidget();
	////GetCalendarItems();

	Initialize(m_datFrom.date(), m_datTo.date(), NULL, NULL, NULL, NULL, &recTmp);
}

void Fui_Calendar::ReorderEntityRecordSets(int nCurrentEntityRow, int nEntityID, int nEntityType)
{
	DbRecordSet recTmp = m_recEntites;
	recTmp.insertRow(1);
	DbRecordSet row = recTmp.getRow(nCurrentEntityRow);
	recTmp.assignRow(1, row);
	recTmp.deleteRow(nCurrentEntityRow);

	Initialize(m_datFrom.date(), m_datTo.date(), NULL, NULL, NULL, NULL, &recTmp);
}

void Fui_Calendar::on_openMultiDayEvent(QHash<int, int> hshMultiDayCentIDs, QHash<int, int> hshMultiDayBcepIDs, QHash<int, int> hshMultiDayBcevIDs)
{
	if (hshMultiDayCentIDs.count()==1)
	{
		int nCentID = hshMultiDayCentIDs.value(0);
		int nBcepID = hshMultiDayBcepIDs.value(0);
		int nBcevID = hshMultiDayBcevIDs.value(0);
		on_ModifyItem(nCentID, nBcepID, nBcevID, -1);
	}
	else
	{

		//lista: BCEP_ID/BCOL_SUBJECT
		DbRecordSet lstOfPArts;
		lstOfPArts.addColumn(QVariant::Int,"BCEP_ID");
		lstOfPArts.addColumn(QVariant::String,"BCOL_SUBJECT");

		lstOfPArts.addRow(2);
		lstOfPArts.setData(0,0,1);
		lstOfPArts.setData(0,1,"Part1");
		lstOfPArts.setData(1,0,2);
		lstOfPArts.setData(1,1,"Part2");

		SelectionPopup dlgPopUp;

		dlgPopUp.Initialize(ENTITY_BUS_CAL_PARTS_SELECTION,true); //can select more then 1
		MainEntitySelectionController* pdata=dynamic_cast<MainEntitySelectionController*>(dlgPopUp.GetSelectorWidget());
		*(pdata->GetDataSource()) = lstOfPArts;
		int nResult=dlgPopUp.OpenSelector();
		if(nResult==0)
			return;

		int nRecID;
		DbRecordSet lstSelectedParts;
		dlgPopUp.GetSelectedData(nRecID,lstSelectedParts);


		for (int i = 0; i < hshMultiDayCentIDs.count(); i++)
		{
			int nCentID = hshMultiDayCentIDs.value(0);
			int nBcepID = hshMultiDayBcepIDs.value(0);
			int nBcevID = hshMultiDayBcevIDs.value(0);
			//on_ModifyItem(nCentID, nBcepID, nBcevID);
		}
	}
}

void Fui_Calendar::on_ShowInformationEvents(bool bIsInformation)
{
	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_ShowPreliminaryEvents(bool bIsPreliminary)
{
	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_FilterTypeChanged(QList<int> lstTypeIDs)
{
	InitializeCalendarWidgetAndGetCalendarItems();
//	InitializeCalendarWidget();
//	GetCalendarItems();

	if (m_recFixedEntites.getRowCount()>1)
		ui.calendarWidget->SelectEntityItems(m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_ID").toInt(), m_recFixedEntites.getDataRef(m_nCurentRow, "ENTITY_TYPE").toInt());
}

void Fui_Calendar::on_TaskItemClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID)
{
	ui.frame_2->SelectTask(nCENT_SYSTEM_TYPE_ID, nTypeID);
}

void Fui_Calendar::on_TaskItemDoubleClicked(int nCENT_ID, int nBTKS_ID, int nCENT_SYSTEM_TYPE_ID, QDateTime datTaskDate, bool bIsStart, int nEntityID, int nEntityType, int nTypeID)
{
	ui.frame_2->OpenTask(nCENT_SYSTEM_TYPE_ID, nTypeID);
}

void Fui_Calendar::on_showTasks_checkBox_stateChanged(int state)
{
	if (state)
		m_bShowTasks = true;
	else
		m_bShowTasks = false;
	
	ui.frame_2->setVisible(m_bShowTasks);
	if (m_bShowTasks)
		ui.frame_2->Reload(&m_recFixedEntites);
}

void Fui_Calendar::on_defineAvailability(DbRecordSet recSelectedEntityItemsInLeftColumn)
{
	DefineAvailabilityDlg dlg;
	dlg.SetDateFrom(m_datFrom.date());
	dlg.SetDateTo(m_datTo.date());

	if(dlg.exec())
	{
		//recSelectedEntityItemsInLeftColumn.Dump();

		QDate dateFrom = dlg.GetDateFrom();
		QDate dateTo   = dlg.GetDateTo();
		int nDays = dateFrom.daysTo(dateTo);

		//to be used for observer notification
		DbRecordSet recTmp;
		recTmp.addColumn(QVariant::Int, "BCEV_ID");
		recTmp.addColumn(DbRecordSet::GetVariantType(),"ASSIGNMENTS");

		//options
		QString strTimeFrom = g_pSettings->GetApplicationOption(CALENDAR_START_DAILY_WORK_TIME).toString();
		QTime timeFrom = (strTimeFrom.isEmpty()) ? QTime(0, 0, 0) : QTime::fromString(strTimeFrom);

		QString strTimeTo   = g_pSettings->GetApplicationOption(CALENDAR_END_DAILY_WORK_TIME).toString();
		QTime timeTo = (strTimeTo.isEmpty()) ? QTime(23, 59, 59) : QTime::fromString(strTimeTo);

		int availableID=g_pSettings->GetApplicationOption(PRESENCE_TYPE_AVAILABLE).toInt();

		//for each day in the range
		for(int i=0; i<=nDays; i++)
		{
			QDate dateCur = dateFrom.addDays(i);
			int nDayOfWeek = dateCur.dayOfWeek();

			//if the day is one of the selected ones
			if(dlg.IsWeekDayChecked(nDayOfWeek))
			{
				//create a new calendar event
				DbRecordSet lstData;
				lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
				lstData.addRow();
				lstData.setData(0,"BCEV_DAT_CREATED",QDateTime::currentDateTime());
				lstData.setData(0,"BCEV_IS_PRIVATE",0);
				lstData.setData(0,"BCEV_IS_INFORMATION",1);
				lstData.setData(0,"BCEV_IS_PRELIMINARY",0);

				lstData.setData(0,"BCEV_FROM", QDateTime(dateCur, timeFrom));	//start of the work day
				lstData.setData(0,"BCEV_TO", QDateTime(dateCur, timeTo));		//end of the work day

				DbRecordSet lstDataCalEventPart;
				lstDataCalEventPart.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));

				CalendarHelperCore::AddEmptyPartRow(lstData, lstDataCalEventPart);
				//lstDataCalEventPart.addRow();

				//add presence type
				if(availableID > 0)
					lstDataCalEventPart.setData(0, "BCEP_PRESENCE_TYPE_ID", availableID);

				//set owner=logged:
				int nLoggedID=g_pClientManager->GetPersonID();
				lstDataCalEventPart.setData(0,"CENT_OWNER_ID",nLoggedID);
				int nLoggedContactID=g_pClientManager->GetPersonContactID();
				
				//create temp
				DbRecordSet lstTemp;
				lstTemp.copyDefinition(recSelectedEntityItemsInLeftColumn);

				//set assignment logged user = Owner:
				DbRecordSet lstNMRXPerson; //user or contact
				lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

				DbRecordSet lstNMRXContact; //user or contact
				lstNMRXContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

				DbRecordSet lstNMRXProject;
				lstNMRXProject.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

				DbRecordSet lstNMRXResource;
				lstNMRXResource.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

				//-------------------------------------OWNER
				/*
				DbRecordSet lstNMRXOwner; //user or contact
				lstNMRXOwner.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

				//Marko: only if not already in list, if in list set role='owner'
				int nRowOwnerInList=recSelectedEntityItemsInLeftColumn.find("ENTITY_PERSON_ID",nLoggedID,true);
				if (nRowOwnerInList<0)
				{
					QList<int> lstUsersAssign;
					lstUsersAssign.append(nLoggedContactID);
					NMRXRelationWidget::CreateAssignmentData(lstNMRXOwner,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("Owner"),0,BUS_CAL_INVITE);
					//issue 2188: do not invite owner:
					DbRecordSet lstInvite=lstNMRXOwner.getDataRef(0,"X_RECORD").value<DbRecordSet>();
					lstInvite.setData(0,"BCIV_INVITE",0);
					lstNMRXOwner.setData(0,"X_RECORD",lstInvite);
				}
				*/


				//-------------------------------------PERSON
				lstTemp.clear();
				recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE",(int)GlobalConstants::TYPE_CAL_USER_SELECT);
				lstTemp.merge(recSelectedEntityItemsInLeftColumn,true);

				//recSelectedEntityItemsInLeftColumn.Dump();

				for (int k=0;k<lstTemp.getRowCount();k++)
				{
					DbRecordSet row;
					row.copyDefinition(lstNMRXPerson);
					QList<int> lstUsersAssign;
					//qDebug()<<lstTemp.getDataRef(k,"ENTITY_PERSON_ID").toInt();
					int nContactID=ui.frameUser->GetSelectionController()->GetContactIDFromPersonID(lstTemp.getDataRef(k,"ENTITY_PERSON_ID").toInt());
					lstUsersAssign.append(nContactID);
					if (nContactID==-1)
						continue;

					if (nContactID==nLoggedContactID) //if owner then set as owner
						NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("Owner"),0,BUS_CAL_INVITE);
					else
						NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);

					lstNMRXPerson.merge(row);
					DbRecordSet rowRecordX;
					NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
					rowRecordX.setData(0,"BCIV_INVITE",0);
					lstNMRXPerson.setData(k,"X_RECORD",rowRecordX);
				}

				//-------------------------------------CONTACT
				lstTemp.clear();
				recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE",(int)GlobalConstants::TYPE_CAL_CONTACT_SELECT);
				lstTemp.merge(recSelectedEntityItemsInLeftColumn,true);

				for (int k=0;k<lstTemp.getRowCount();k++)
				{
					DbRecordSet row;
					row.copyDefinition(lstNMRXContact);

					QList<int> lstUsersAssign;
					lstUsersAssign.append(lstTemp.getDataRef(k,"ENTITY_ID").toInt());
					NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);
					lstNMRXContact.merge(row);
					DbRecordSet rowRecordX;
					NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
					rowRecordX.setData(0,"BCIV_INVITE",0);
					lstNMRXContact.setData(k,"X_RECORD",rowRecordX);
				}
		
				//-------------------------------------PROJECT
				lstTemp.clear();
				recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE",(int)GlobalConstants::TYPE_CAL_PROJECT_SELECT);
				lstTemp.merge(recSelectedEntityItemsInLeftColumn,true);

				//qDebug()<<lstTemp.getRowCount();

				for (int k=0;k<lstTemp.getRowCount();k++)
				{
					DbRecordSet row;
					row.copyDefinition(lstNMRXProject);
					QList<int> lstUsersAssign;
					lstUsersAssign.append(lstTemp.getDataRef(k,"ENTITY_ID").toInt());
					NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_PROJECT,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);
					lstNMRXProject.merge(row);
					//DbRecordSet rowRecordX;
					//NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
					//rowRecordX.setData(0,"BCIV_INVITE",0);
					//lstNMRXProject.setData(k,"X_RECORD",rowRecordX);
				}

				//qDebug()<<lstNMRXProject.getRowCount();

				//-------------------------------------RESOURCE
				lstTemp.clear();
				recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE",(int)GlobalConstants::TYPE_CAL_RESOURCE_SELECT);
				lstTemp.merge(recSelectedEntityItemsInLeftColumn,true);

				for (int k=0;k<lstTemp.getRowCount();k++)
				{
					DbRecordSet row;
					row.copyDefinition(lstNMRXResource);

					QList<int> lstUsersAssign;
					lstUsersAssign.append(lstTemp.getDataRef(k,"ENTITY_ID").toInt());
					NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_CAL_RESOURCE,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);
					lstNMRXResource.merge(row);

					//DbRecordSet rowRecordX;
					//NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
					//rowRecordX.setData(0,"BCIV_INVITE",0);
					//lstNMRXResource.setData(k,"X_RECORD",rowRecordX);
				}


				lstNMRXPerson.merge(lstNMRXContact); //merge into one list

				//per contact:
				DbRecordSet rowData=lstData.getRow(0);
				DbRecordSet rowDataPart=lstDataCalEventPart.getRow(0);

				//clear first row as values are stored into rows: now they will be filled
				lstDataCalEventPart.clear();
				lstData.clear();

				for (int k=0;k<lstNMRXContact.getRowCount();k++)
				{
					lstData.addRow();
					lstDataCalEventPart.addRow();
					int nRow=lstData.getRowCount()-1;

					DbRecordSet tmpPart=rowDataPart;
					DbRecordSet row=lstNMRXPerson.getRow(k);
					tmpPart.setData(0,"NMRX_PERSON_LIST",row);
					tmpPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
					tmpPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResource);
					
					lstDataCalEventPart.assignRow(nRow,tmpPart);
					lstData.assignRow(nRow,rowData);
				}


				//MB: 22.07.2011: define availability on projects / OR's, but not when contacts are present, as contact merges all into one record
				if (lstNMRXContact.getRowCount()==0)
				{
				
					for (int k=0;k<lstNMRXProject.getRowCount();k++)
					{
						lstData.addRow();
						lstDataCalEventPart.addRow();
						int nRow=lstData.getRowCount()-1;

						DbRecordSet tmpPart=rowDataPart;
						DbRecordSet row = lstNMRXProject.getRow(k);
						tmpPart.setData(0,"NMRX_PROJECT_LIST",row);

						lstDataCalEventPart.assignRow(nRow,tmpPart);
						lstData.assignRow(nRow,rowData);
					}
					for (int k=0;k<lstNMRXResource.getRowCount();k++)
					{
						lstData.addRow();
						lstDataCalEventPart.addRow();
						int nRow=lstData.getRowCount()-1;

						DbRecordSet tmpPart=rowDataPart;
						DbRecordSet row = lstNMRXResource.getRow(k);
						tmpPart.setData(0,"NMRX_RESOURCE_LIST",row);

						lstDataCalEventPart.assignRow(nRow,tmpPart);
						lstData.assignRow(nRow,rowData);
					}
				}
				



				//lstDataCalEventPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
				//lstDataCalEventPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
				//lstDataCalEventPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResource);


				//Issue No 26: twist by MB: for each assigned entity create new calendar event: so one can be deleted without deleting others...



				DbRecordSet lstDataCalEventPartTaskData;
				lstDataCalEventPartTaskData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));

			#ifdef _DEBUG
				lstDataCalEventPart.Dump();
				lstData.Dump();
			#endif

				QString strLockedRes;
				Status err;
				DbRecordSet lstUAR,lstGAR;
				_SERVER_CALL(BusCalendar->WriteEvent(err, lstData, lstDataCalEventPart, lstDataCalEventPartTaskData, strLockedRes, lstUAR,lstGAR));
				if (!err.IsOK()){
					QMessageBox::warning(NULL, tr("Error"), err.getErrorText());
					return;
				}

				int nCalEvID = lstData.getDataRef(0, "BCEV_ID").toInt();
				qDebug() << "Generated new cal. event with ID =" << nCalEvID;

				recTmp.addRow();
				int nRowIdx = recTmp.getRowCount()-1;
				recTmp.setData(nRowIdx, "BCEV_ID", nCalEvID);

				DbRecordSet lstCalData;
				NMRXManager::ConvertToAssignmentDataFromCalendarPartData(lstCalData,lstDataCalEventPart);
				recTmp.setData(nRowIdx,"ASSIGNMENTS",lstCalData);
			}
		}

		//update observers
		if(recTmp.getRowCount() > 0){
			QVariant varData;
			qVariantSetValue(varData,recTmp);
			updateObserver(this, ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED, ENTITY_CALENDAR_EVENT, varData);
		}
	}
}

void Fui_Calendar::on_markAsFinal(DbRecordSet recSelectedEntityItemsInLeftColumn)
{
	DbRecordSet recFilter;
	GetFilterRecordset(recFilter);

	//super moronizam: find type by name, ahahahaha
	DbRecordSet lstPresence;
	if(!ClientContactManager::GetTypeList(ContactTypeManager::TYPE_CALENDAR_PRESENCE,lstPresence))
	{
		QMessageBox::warning(this,tr("Warning"),"Can not find presence type 'final'. Please enter this type in the Organization/Types.");
		return;
	}
	int nRow=lstPresence.find("BCMT_TYPE_NAME",QString("endg�ltig"),true);
	if (nRow<0)
		nRow=lstPresence.find("BCMT_TYPE_NAME",QString("Endg�ltig"),true);
	if (nRow<0)
		nRow=lstPresence.find("BCMT_TYPE_NAME",QString("final"),true);
	if (nRow<0)
		nRow=lstPresence.find("BCMT_TYPE_NAME",QString("Final"),true);

	if (nRow<0)
	{
		QMessageBox::warning(this,tr("Warning"),"Can not find presence type 'final'. Please enter this type in the Organization/Types.");
		return;
	}

	int nPresenceTypeID=lstPresence.getDataRef(nRow,"BCMT_ID").toInt();

	Status err;
	_SERVER_CALL(BusCalendar->ModifyEventPresence(err, m_datFrom, m_datTo, recFilter, recSelectedEntityItemsInLeftColumn, nPresenceTypeID));
	_CHK_ERR(err);

	
	//update observers
	//QVariant varData;
	//qVariantSetValue(varData,recSelectedEntityItemsInLeftColumn);
	updateObserver(this, ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD, ENTITY_CALENDAR_EVENT);

	//m_datFrom, m_datTo
	//recSelectedEntityItemsInLeftColumn



}

void Fui_Calendar::on_markAsPreliminary(DbRecordSet recSelectedEntityItemsInLeftColumn)
{
	DbRecordSet recFilter;
	GetFilterRecordset(recFilter);

	//super moronizam: find type by name, ahahahaha
	DbRecordSet lstPresence;
	if(!ClientContactManager::GetTypeList(ContactTypeManager::TYPE_CALENDAR_PRESENCE,lstPresence))
	{
		QMessageBox::warning(this,tr("Warning"),"Can not find presence type 'preliminary'. Please enter this type in the Organization/Types.");
		return;
	}


	int nRow=lstPresence.find("BCMT_TYPE_NAME",QString("in Planung"),true);
	if (nRow<0)
		nRow=lstPresence.find("BCMT_TYPE_NAME",QString("In Planung"),true);
	if (nRow<0)
		nRow=lstPresence.find("BCMT_TYPE_NAME",QString("preliminary"),true);
	if (nRow<0)
		nRow=lstPresence.find("BCMT_TYPE_NAME",QString("Preliminary"),true);

	if (nRow<0)
	{
		QMessageBox::warning(this,tr("Warning"),"Can not find presence type 'preliminary'. Please enter this type in the Organization/Types.");
		return;
	}

	int nPresenceTypeID=lstPresence.getDataRef(nRow,"BCMT_ID").toInt();

	Status err;
	_SERVER_CALL(BusCalendar->ModifyEventPresence(err, m_datFrom, m_datTo, recFilter, recSelectedEntityItemsInLeftColumn, nPresenceTypeID));
	_CHK_ERR(err);

	//QVariant varData;
	//qVariantSetValue(varData,recSelectedEntityItemsInLeftColumn);
	updateObserver(this, ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD, ENTITY_CALENDAR_EVENT);


}

void Fui_Calendar::on_dropEntityOnTable(int nDraggedEntityID, int nDraggedEntityType, int nDraggedEntityPersonID, int nDroppedEntityID, int nDroppedEntityType, 
										int nDroppedEntityPersonID, QDateTime datSelectionFrom, QDateTime datSelectionTo)
{
	//options
	QString strTimeFrom = g_pSettings->GetApplicationOption(CALENDAR_START_DAILY_WORK_TIME).toString();
	QTime timeFrom = (strTimeFrom.isEmpty()) ? QTime(0, 0, 0) : QTime::fromString(strTimeFrom);
	datSelectionFrom.setTime(timeFrom);

	QString strTimeTo   = g_pSettings->GetApplicationOption(CALENDAR_END_DAILY_WORK_TIME).toString();
	QTime timeTo = (strTimeTo.isEmpty()) ? QTime(23, 59, 59) : QTime::fromString(strTimeTo);
	datSelectionTo.setTime(timeTo);

	int preliminaryID=g_pSettings->GetApplicationOption(PRESENCE_TYPE_PRELIMINARY).toInt();

	//store in row to keeps the code similar to upper fn code
	DbRecordSet recSelectedEntityItemsInLeftColumn;
	recSelectedEntityItemsInLeftColumn.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ENTITY_VIEW));
	recSelectedEntityItemsInLeftColumn.addRow();
	recSelectedEntityItemsInLeftColumn.setData(0, "ENTITY_TYPE", nDraggedEntityType);
	recSelectedEntityItemsInLeftColumn.setData(0, "ENTITY_ID", nDraggedEntityID);
	recSelectedEntityItemsInLeftColumn.setData(0, "ENTITY_PERSON_ID", nDraggedEntityPersonID);
	
	recSelectedEntityItemsInLeftColumn.addRow();
	recSelectedEntityItemsInLeftColumn.setData(1, "ENTITY_TYPE", nDroppedEntityType);
	recSelectedEntityItemsInLeftColumn.setData(1, "ENTITY_ID", nDroppedEntityID);
	recSelectedEntityItemsInLeftColumn.setData(1, "ENTITY_PERSON_ID", nDroppedEntityPersonID);

	//to be used for observer notification
	DbRecordSet recTmp;
	recTmp.addColumn(QVariant::Int, "BCEV_ID");
	recTmp.addColumn(DbRecordSet::GetVariantType(),"ASSIGNMENTS");

	//create a new calendar event
	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
	lstData.addRow();
	lstData.setData(0,"BCEV_DAT_CREATED",QDateTime::currentDateTime());
	lstData.setData(0,"BCEV_IS_PRIVATE",0);
	lstData.setData(0,"BCEV_IS_INFORMATION",0);
	lstData.setData(0,"BCEV_IS_PRELIMINARY",0);
	lstData.setData(0,"BCEV_FROM", datSelectionFrom);
	lstData.setData(0,"BCEV_TO", datSelectionTo);

	DbRecordSet lstDataCalEventPart;
	lstDataCalEventPart.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));

	CalendarHelperCore::AddEmptyPartRow(lstData, lstDataCalEventPart);
	//lstDataCalEventPart.addRow();

	//add presence type
	if(preliminaryID > 0)
		lstDataCalEventPart.setData(0, "BCEP_PRESENCE_TYPE_ID", preliminaryID);

	//set owner=logged:
	int nLoggedID=g_pClientManager->GetPersonID();
	lstDataCalEventPart.setData(0,"CENT_OWNER_ID",nLoggedID);
	int nLoggedContactID=g_pClientManager->GetPersonContactID();
	
	//create temp
	DbRecordSet lstTemp;
	lstTemp.copyDefinition(recSelectedEntityItemsInLeftColumn);

	//set assignment logged user = Owner:
	DbRecordSet lstNMRXPerson; //user or contact
	lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXContact; //user or contact
	lstNMRXContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXProject;
	lstNMRXProject.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXResource;
	lstNMRXResource.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	//-------------------------------------OWNER
	/*
	DbRecordSet lstNMRXOwner; //user or contact
	lstNMRXOwner.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	//Marko: only if not already in list, if in list set role='owner'
	int nRowOwnerInList=recSelectedEntityItemsInLeftColumn.find("ENTITY_PERSON_ID",nLoggedID,true);
	if (nRowOwnerInList<0)
	{
		QList<int> lstUsersAssign;
		lstUsersAssign.append(nLoggedContactID);
		NMRXRelationWidget::CreateAssignmentData(lstNMRXOwner,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("Owner"),0,BUS_CAL_INVITE);
		//issue 2188: do not invite owner:
		DbRecordSet lstInvite=lstNMRXOwner.getDataRef(0,"X_RECORD").value<DbRecordSet>();
		lstInvite.setData(0,"BCIV_INVITE",0);
		lstNMRXOwner.setData(0,"X_RECORD",lstInvite);
	}
	*/



	//-------------------------------------PERSON
	lstTemp.clear();
	recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE",(int)GlobalConstants::TYPE_CAL_USER_SELECT);
	lstTemp.merge(recSelectedEntityItemsInLeftColumn,true);

	for (int k=0;k<lstTemp.getRowCount();k++)
	{
		DbRecordSet row;
		row.copyDefinition(lstNMRXPerson);
		QList<int> lstUsersAssign;
		//qDebug()<<lstTemp.getDataRef(k,"ENTITY_PERSON_ID").toInt();
		int nContactID=ui.frameUser->GetSelectionController()->GetContactIDFromPersonID(lstTemp.getDataRef(k,"ENTITY_PERSON_ID").toInt());
		lstUsersAssign.append(nContactID);
		if (nContactID==-1)
			continue;

		if (nContactID==nLoggedContactID) //if owner then set as owner
			NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("Owner"),0,BUS_CAL_INVITE);
		else
			NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);

		
		lstNMRXPerson.merge(row);
		DbRecordSet rowRecordX;
		NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
		rowRecordX.setData(0,"BCIV_INVITE",0);
		lstNMRXPerson.setData(k,"X_RECORD",rowRecordX);
	}

	//-------------------------------------CONTACT
	lstTemp.clear();
	recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE",(int)GlobalConstants::TYPE_CAL_CONTACT_SELECT);
	lstTemp.merge(recSelectedEntityItemsInLeftColumn,true);

	for (int k=0;k<lstTemp.getRowCount();k++)
	{
		DbRecordSet row;
		row.copyDefinition(lstNMRXContact);

		QList<int> lstUsersAssign;
		lstUsersAssign.append(lstTemp.getDataRef(k,"ENTITY_ID").toInt());
		NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);
		lstNMRXContact.merge(row);
		DbRecordSet rowRecordX;
		NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
		rowRecordX.setData(0,"BCIV_INVITE",0);
		lstNMRXContact.setData(k,"X_RECORD",rowRecordX);
	}

	//-------------------------------------PROJECT
	lstTemp.clear();
	recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE",(int)GlobalConstants::TYPE_CAL_PROJECT_SELECT);
	lstTemp.merge(recSelectedEntityItemsInLeftColumn,true);

	for (int k=0;k<lstTemp.getRowCount();k++)
	{
		DbRecordSet row;
		row.copyDefinition(lstNMRXProject);
		QList<int> lstUsersAssign;
		lstUsersAssign.append(lstTemp.getDataRef(k,"ENTITY_ID").toInt());
		NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_PROJECT,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);
		lstNMRXProject.merge(row);
		//DbRecordSet rowRecordX;
		//NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
		//rowRecordX.setData(0,"BCIV_INVITE",0);
		//lstNMRXProject.setData(k,"X_RECORD",rowRecordX);
	}

	//-------------------------------------RESOURCE
	lstTemp.clear();
	recSelectedEntityItemsInLeftColumn.find("ENTITY_TYPE",(int)GlobalConstants::TYPE_CAL_RESOURCE_SELECT);
	lstTemp.merge(recSelectedEntityItemsInLeftColumn,true);

	for (int k=0;k<lstTemp.getRowCount();k++)
	{
		DbRecordSet row;
		row.copyDefinition(lstNMRXResource);

		QList<int> lstUsersAssign;
		lstUsersAssign.append(lstTemp.getDataRef(k,"ENTITY_ID").toInt());
		NMRXRelationWidget::CreateAssignmentData(row,CE_COMM_ENTITY,BUS_CAL_RESOURCE,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);
		lstNMRXResource.merge(row);

		//DbRecordSet rowRecordX;
		//NMRXManager::DefineDefaultRecordX(BUS_CAL_INVITE,rowRecordX);
		//rowRecordX.setData(0,"BCIV_INVITE",0);
		//lstNMRXResource.setData(k,"X_RECORD",rowRecordX);
	}

	lstNMRXPerson.merge(lstNMRXContact); //merge into one list
	//lstNMRXPerson.merge(lstNMRXOwner); //merge into one list

	//per contact:
	DbRecordSet rowData=lstData.getRow(0);
	DbRecordSet rowDataPart=lstDataCalEventPart.getRow(0);

	for (int k=0;k<lstNMRXContact.getRowCount();k++)
	{
		lstData.addRow();
		lstDataCalEventPart.addRow();
		int nRow=lstData.getRowCount()-1;

		DbRecordSet tmpPart=rowDataPart;
		//B.T. last twist: after two-three times MB finally decided what he wants
		DbRecordSet row=lstNMRXPerson.getRow(k);
		tmpPart.setData(0,"NMRX_PERSON_LIST",row);
		tmpPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
		tmpPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResource);

		lstDataCalEventPart.assignRow(nRow,tmpPart);
		lstData.assignRow(nRow,rowData);
	}

	/*
	for (int k=0;k<lstNMRXProject.getRowCount();k++)
	{
		lstData.addRow();
		lstDataCalEventPart.addRow();
		int nRow=lstData.getRowCount()-1;

		DbRecordSet tmpPart=rowDataPart;
		tmpPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject.getRow(k));

		lstDataCalEventPart.assignRow(nRow,tmpPart);
		lstData.assignRow(nRow,rowData);
	}
	for (int k=0;k<lstNMRXResource.getRowCount();k++)
	{
		lstData.addRow();
		lstDataCalEventPart.addRow();
		int nRow=lstData.getRowCount()-1;

		DbRecordSet tmpPart=rowDataPart;
		tmpPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResource.getRow(k));

		lstDataCalEventPart.assignRow(nRow,tmpPart);
		lstData.assignRow(nRow,rowData);
	}*/


	//lstDataCalEventPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
	//lstDataCalEventPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
	//lstDataCalEventPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResource);

	DbRecordSet lstDataCalEventPartTaskData;
	lstDataCalEventPartTaskData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));

	QString strLockedRes;
	Status err;
	DbRecordSet lstUAR,lstGAR;
	_SERVER_CALL(BusCalendar->WriteEvent(err, lstData, lstDataCalEventPart, lstDataCalEventPartTaskData, strLockedRes,lstUAR,lstGAR));
	if (!err.IsOK()){
		QMessageBox::warning(NULL, tr("Error"), err.getErrorText());
		return;
	}

	recTmp.addRow();
	int nRowIdx = recTmp.getRowCount()-1;
	recTmp.setData(nRowIdx, "BCEV_ID", lstData.getDataRef(0, "BCEV_ID").toInt());

	DbRecordSet lstCalData;
	NMRXManager::ConvertToAssignmentDataFromCalendarPartData(lstCalData,lstDataCalEventPart);
	recTmp.setData(nRowIdx, "ASSIGNMENTS",lstCalData);

	//update observers
	if(recTmp.getRowCount() > 0){
		QVariant varData;
		qVariantSetValue(varData,recTmp);
		//updateObserver(this, ChangeManager::GLOBAL_REFRESH_CALENDAR_ENTITY_INSERTED_AFTER_DRAG_DROP, ENTITY_CALENDAR_EVENT, varData);
		updateObserver(this, ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED, ENTITY_CALENDAR_EVENT, varData);
	}
}
