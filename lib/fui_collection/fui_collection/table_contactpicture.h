#ifndef TABLE_CONTACTPICTURE_H
#define TABLE_CONTACTPICTURE_H

#include "gui_core/gui_core/universaltablewidgetex.h"
#include "toolbar_edit.h"
#include "common/common/status.h"

class Table_ContactPicture : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_ContactPicture(QWidget *parent);

	void	Initialize(DbRecordSet *plstData,toolbar_edit * pToolBar);
	void	SetEditMode(bool bEdit=true);
	void	CheckDataBeforeWrite(Status &Ret_pStatus);

private slots:
	void	SetDefault();
	void	OnDeleted();
	void	OnCellDoubleClicked(int nRow,int nCol);
	void	OnRowInserted(int nRow);
	void	InsertRow();
	void	OpenDetailsFromContextMenu();

protected:
	void				Data2CustomWidget(int nRow, int nCol);
	virtual QMimeData * mimeData ( const QList<QTableWidgetItem *> items ) const;

private:
	void	OpenDetails(int nRow);
	void	BuildToolBar(toolbar_edit* pToolBar);
	void	CreateContextMenu();
	void	GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip);	
	void	RefreshDefaultFlag();

	int					m_nDefaultColIdx;
	toolbar_edit		*m_toolbar;
};

#endif // TABLE_CONTACTPICTURE_H
