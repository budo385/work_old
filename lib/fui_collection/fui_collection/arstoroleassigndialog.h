#ifndef ARSTOROLEASSIGNDIALOG_H
#define ARSTOROLEASSIGNDIALOG_H

#include <QtWidgets/QDialog>
#include "ui_arstoroleassigndialog.h"

#include "common/common/dbrecordset.h"

class ARSToRoleAssignDialog : public QDialog
{
    Q_OBJECT

public:
    ARSToRoleAssignDialog(DbRecordSet *RoleRecordSet, QWidget *parent = 0);
    ~ARSToRoleAssignDialog();

	DbRecordSet& GetSelectedRoles();

private:
    Ui::ARSToRoleAssignDialogClass ui;

	void InitializeTreeWidget();
	void InsertRole(QTreeWidgetItem *Item);
	void RemoveRole(QTreeWidgetItem *Item);
	void OkButtonStateChanged();

	QHash<int, QTreeWidgetItem*> m_hshRoleHash;				//Role ID to tree item hash.
	QHash<int, int> m_hshRoleIDToRoleRowHash;				//Stores role ID to row in recordset (for faster operations - not to use find).
	QHash<int, QTreeWidgetItem*> m_hshSelectedRoleHash;		//Selected Role ID to item hash.
	DbRecordSet *m_RoleRecordSet;							//Pointer to source recordset.
	DbRecordSet m_SelectedRoleRecordSet;					//Selected recordset.

private slots:
	void on_CancelPushButton_clicked();
	void on_OkPushButton_clicked();
	void on_RemoveAll_pushButton_clicked();
	void on_RemoveSelected_pushButton_clicked();
	void on_AddAll_pushButton_clicked();
	void on_AddSelected_pushButton_clicked();
	void on_RoleDropped();

};

#endif // ARSTOROLEASSIGNDIALOG_H
