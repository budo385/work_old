#include "selectedreportstree.h"

SelectedReportsTree::SelectedReportsTree(QWidget *parent)
	: UniversalTreeWidget(parent)
{

}

SelectedReportsTree::~SelectedReportsTree()
{

}

void SelectedReportsTree::DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
{
	//If not ignoring drop handler.
	if (!m_bIgnoreDropHandler && !topLevelItemCount())
	{
		//Check is there already an item with same RowID - it can not happen (at least for now).
		QList<int> ExistingRowList, DroppedRowIDList, DroppedRowList;
		DroppedValue.clearSelection();
		int count = DroppedValue.getRowCount();
		for (int i = 0; i < count; ++i)
		{
			int RowID = DroppedValue.getDataRef(i, 0).toInt();
			if (m_hshRowIDtoItem.contains(RowID))
				DroppedValue.selectRow(i);
		}
		DroppedValue.deleteSelectedRows();
		if (!DroppedValue.getRowCount())
			return;

		//copy data:
		if (!m_recData->getRowCount())
			m_recData->copyDefinition(DroppedValue);
		m_recData->clear();
		m_recData->merge(DroppedValue);
		SortList();
		RefreshDisplay();
		emit ItemNumberChanged();
	}
	else
		emit DataDroped(index, nDropType, DroppedValue, event);
}
