#ifndef PHONENUMBERVALIDATOR_H
#define PHONENUMBERVALIDATOR_H

#include <QValidator>

class PhoneNumberValidator : public QValidator
{
	Q_OBJECT

public:
	PhoneNumberValidator(QObject *parent);
	~PhoneNumberValidator();

	QValidator::State validate(QString &input, int &pos) const;

private:
	
};

#endif // PHONENUMBERVALIDATOR_H
