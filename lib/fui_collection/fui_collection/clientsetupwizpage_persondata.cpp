#include "clientsetupwizpage_persondata.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "fui_collection/fui_collection/dlg_phoneformatter.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "gui_core/gui_core/macros.h"
#include <QTimer>

//GLOBALS:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

bool Call_OnLoadFullPicture(int nPicID, DbRecordSet &rowPic);

ClientSetupWizPage_PersonData::ClientSetupWizPage_PersonData(int nWizardPageID, QString strPageTitle, QWidget *parent)
	: WizardPage(nWizardPageID, strPageTitle)
{
	setMinimumSize(QSize(600,450));
	m_GuiFieldManagerMain=NULL;

	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	m_lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE_SELECT));
	m_lstEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT));
	m_lstInternet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET_SELECT));
	m_lstAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT));
}

void ClientSetupWizPage_PersonData::Initialize()
{

	ui.setupUi(this);
	m_bInitialized = true;

	m_recResultRecordSet.addColumn(QVariant::Int,"BCNT_ID");
	m_recResultRecordSet.addRow();
	m_recResultRecordSet.setData(0,0,-1);


	//types:
	m_Types.Initialize(ENTITY_BUS_CM_TYPES);
	m_Types.ReloadData();


	//menu:
	SetMenuEmailTypes(&m_MenuEmail_1);
	SetMenuEmailTypes(&m_MenuEmail_2);
	SetMenuPhoneTypes(&m_MenuPhone_1);
	SetMenuPhoneTypes(&m_MenuPhone_2);
	SetMenuPhoneTypes(&m_MenuPhone_3);
	ui.btnEmail1->setMenu(&m_MenuEmail_1);
	ui.btnEmail2->setMenu(&m_MenuEmail_2);
	ui.btnPhone1->setMenu(&m_MenuPhone_1);
	ui.btnPhone2->setMenu(&m_MenuPhone_2);
	ui.btnPhone3->setMenu(&m_MenuPhone_3);


	connect(&m_MenuEmail_1,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerEmail1(QAction *)));
	connect(&m_MenuEmail_2,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerEmail2(QAction *)));
	connect(&m_MenuPhone_1,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerPhone1(QAction *)));
	connect(&m_MenuPhone_2,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerPhone2(QAction *)));
	connect(&m_MenuPhone_3,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerPhone3(QAction *)));

	ui.btnFormatPhone1->setIcon(QIcon(":cookies.png"));
	ui.btnFormatPhone2->setIcon(QIcon(":cookies.png"));
	ui.btnFormatPhone3->setIcon(QIcon(":cookies.png"));
	ui.btnFormatPhone1->setToolTip(tr("Format Phone"));
	ui.btnFormatPhone2->setToolTip(tr("Format Phone"));
	ui.btnFormatPhone3->setToolTip(tr("Format Phone"));

	//issue 1346: use Business, Private 
	ui.btnEmail1->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_EMAIL_ADDRESS,ContactTypeManager::SYSTEM_EMAIL_BUSINESS).toInt()));
	ui.btnEmail2->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_EMAIL_ADDRESS,ContactTypeManager::SYSTEM_EMAIL_PRIVATE).toInt()));

	//issue 1346: use Business Direct, Private and Fax
	ui.btnPhone1->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS).toInt()));
	ui.btnPhone2->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE).toInt()));
	ui.btnPhone3->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX).toInt()));

	//connect lineedit:
	connect(ui.txtLastName,SIGNAL(textChanged(const QString &)),this,SLOT(FieldChanged(const QString & )));
	//connect(ui.txtFirstName,SIGNAL(editingFinished()),this,SLOT(FieldChanged()));

	//------------------------------------
	//MAIN
	//------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_BUS_CM_CONTACT);

	//person:
	m_GuiFieldManagerMain->RegisterField("BCNT_FIRSTNAME",ui.txtFirstName);
	m_GuiFieldManagerMain->RegisterField("BCNT_LASTNAME",ui.txtLastName);
	m_GuiFieldManagerMain->RegisterField("BCNT_MIDDLENAME",ui.txtMiddleName);
	m_GuiFieldManagerMain->RegisterField("BCNT_NICKNAME",ui.txtNickName);
	m_GuiFieldManagerMain->RegisterField("BCNT_LOCATION",ui.txtLocation);
	m_GuiFieldManagerMain->RegisterField("BCNT_BIRTHDAY",ui.dateBirthDay);

	//init custom picture control
	ui.picture->Initialize(PictureWidget::MODE_PREVIEW_WITH_BUTTONS,&m_lstData,"BCNT_PICTURE","BPIC_PICTURE","BCNT_BIG_PICTURE_ID","BCNT_ID");
	ui.picture->SetLoadFullPictureCallBack(Call_OnLoadFullPicture);

	//sex
	QList<QRadioButton*> lstRadioButons;
	QList<QVariant> lstValues;
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.btnMale << ui.btnFemale;
	lstValues << 0 << 1;
	m_GuiFieldManagerMain->RegisterField("BCNT_SEX",new GuiField_RadioButton("BCNT_SEX",lstRadioButons,lstValues,&m_lstData,&DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT)));

	//ACP patterns:
	ui.framePersonOrganization->Initialize(ENTITY_CONTACT_ORGANIZATION,&m_lstData ,"BCNT_ORGANIZATIONNAME",false);

	//--------------------------------------------------
	//		PHONES/EMAILS/INTER/PICTURE
	//--------------------------------------------------
	ui.tableAddress->Initialize(&m_lstAddress,&m_lstData,ui.txtAddress,ui.tableTypes,ui.frameAddressToolbar,ui.ckbSynchronize,ui.ckbSynchronizeOrg);
	ui.tableAddress->SetEditMode(true);
	ui.framePersonOrganization->SetEditMode(true);
	ui.frameY->setVisible(false);
	m_GuiFieldManagerMain->SetEditMode(true);
	//insert 1 row:
	m_lstData.clear();
	ContactTypeManager::AddEmptyRowToFullContactList(m_lstData);
	m_lstData.setData(0,"BCNT_ID",0);												
	m_lstData.setData(0,"BCNT_TYPE",ContactTypeManager::CM_TYPE_PERSON);			
	m_lstData.setData(0,"BCNT_SEX",ContactTypeManager::SEX_MALE);					
	m_lstData.setData(0,"BCNT_SUPPRESS_MAILING",0);									
	
	//insert 2 mail, 3 phone
	m_lstPhones.addRow();
	m_lstPhones.addRow();
	m_lstPhones.addRow();
	m_lstEmails.addRow();
	m_lstEmails.addRow();
	m_lstInternet.addRow();

	//m_pBoMain->registerObserver(this);
	ui.picture->setFocusPolicy(Qt::ClickFocus);
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.picture->SetEditMode(true);
	ui.picture->RefreshDisplay();
	ui.txtFirstName->setFocus(Qt::OtherFocusReason);
	m_GuiFieldManagerMain->RefreshDisplay();

	QTimer::singleShot(0,this,SLOT(OnSetFocusDelayed()));
}

ClientSetupWizPage_PersonData::~ClientSetupWizPage_PersonData()
{
	delete m_GuiFieldManagerMain;

}


void ClientSetupWizPage_PersonData::on_btnAddress_clicked()
{
	if (m_lstAddress.getRowCount()==0)
		ui.tableAddress->AddRow();
	else
		ui.tableAddress->OpenDetailsFromContextMenu();
	
}



void ClientSetupWizPage_PersonData::SetMenuEmailTypes(QMenu *menu)
{
	DbRecordSet lstTypes=*m_Types.GetDataSource();

	//_DUMP(lstTypes)

	//select mails:	
	lstTypes.find(lstTypes.getColumnIdx("BCMT_ENTITY_TYPE"),QVariant(ContactTypeManager::TYPE_EMAIL_ADDRESS).toInt());
	lstTypes.deleteUnSelectedRows();

	int nSize=lstTypes.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		menu->addAction(lstTypes.getDataRef(i,"BCMT_TYPE_NAME").toString());
	}
}


void ClientSetupWizPage_PersonData::SetMenuPhoneTypes(QMenu *menu)
{
	DbRecordSet lstTypes=*m_Types.GetDataSource();

	//_DUMP(lstTypes)

	//select mails:	
	lstTypes.find(lstTypes.getColumnIdx("BCMT_ENTITY_TYPE"),QVariant(ContactTypeManager::TYPE_PHONE).toInt());
	lstTypes.deleteUnSelectedRows();

	int nSize=lstTypes.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		menu->addAction(lstTypes.getDataRef(i,"BCMT_TYPE_NAME").toString());
	}
}

QVariant ClientSetupWizPage_PersonData::GetTypeFromName(QString strName,int nEntity)
{
	DbRecordSet lstTypes=*m_Types.GetDataSource();

	//select mails:	
	lstTypes.find(lstTypes.getColumnIdx("BCMT_ENTITY_TYPE"),nEntity);
	lstTypes.deleteUnSelectedRows();

	int nRow=lstTypes.find("BCMT_TYPE_NAME",strName,true);
	if (nRow!=-1)
		return lstTypes.getDataRef(nRow,"BCMT_ID").toInt();
	else
		return ClientContactManager::GetDefaultType(nEntity);

}


void ClientSetupWizPage_PersonData::on_btnFormatPhone1_clicked()
{
	m_lstPhones.setData(0,"BCMP_FULLNUMBER",ui.txtPhone1->text());
	FormatPhoneNumber(0);
	ui.txtPhone1->setText(m_lstPhones.getDataRef(0,"BCMP_FULLNUMBER").toString());
}

void ClientSetupWizPage_PersonData::on_btnFormatPhone2_clicked()
{
	m_lstPhones.setData(1,"BCMP_FULLNUMBER",ui.txtPhone2->text());
	FormatPhoneNumber(1);
	ui.txtPhone2->setText(m_lstPhones.getDataRef(1,"BCMP_FULLNUMBER").toString());
}
void ClientSetupWizPage_PersonData::on_btnFormatPhone3_clicked()
{
	m_lstPhones.setData(2,"BCMP_FULLNUMBER",ui.txtPhone3->text());
	FormatPhoneNumber(2);
	ui.txtPhone3->setText(m_lstPhones.getDataRef(2,"BCMP_FULLNUMBER").toString());
}


void ClientSetupWizPage_PersonData::FormatPhoneNumber(int nRow)
{

	QString strPhone,strArea,strCountry,strSearch,strISO,strLocal;
	Dlg_PhoneFormatter dlg;
	int m_nPhoneColIdx=m_lstPhones.getColumnIdx("BCMP_FULLNUMBER");
	dlg.SetRow(m_lstPhones.getDataRef(nRow,m_nPhoneColIdx).toString(),"");
	int nRes=dlg.exec();
	if (nRes)
	{
		dlg.GetRow(strPhone,strCountry,strArea,strLocal,strSearch,strISO);
		m_lstPhones.setData(nRow,m_nPhoneColIdx,strPhone);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_AREA"),strArea);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_COUNTRY"),strCountry);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_LOCAL"),strLocal);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_SEARCH"),strSearch);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_COUNTRY_ISO"),strISO);
	}

}



void ClientSetupWizPage_PersonData::SaveFromGUI()
{

	//emails:
	if (ui.txtEmail1->text()!="")
	{
		m_lstEmails.setData(0,"BCME_ADDRESS",ui.txtEmail1->text());
		m_lstEmails.setData(0,"BCME_TYPE_ID",GetTypeFromName(ui.btnEmail1->text(),ContactTypeManager::TYPE_EMAIL_ADDRESS));
	}

	//emails:
	if (ui.txtEmail2->text()!="")
	{
		int nRow=1;
		m_lstEmails.setData(nRow,"BCME_ADDRESS",ui.txtEmail2->text());
		m_lstEmails.setData(nRow,"BCME_TYPE_ID",GetTypeFromName(ui.btnEmail2->text(),ContactTypeManager::TYPE_EMAIL_ADDRESS));
	}

	//remove empty:
	m_lstEmails.clearSelection();
	if (ui.txtEmail1->text()=="")
		m_lstEmails.selectRow(0);
	if (ui.txtEmail2->text()=="")
		m_lstEmails.selectRow(1);
	m_lstEmails.deleteSelectedRows();

	//set def=1
	if (m_lstEmails.getRowCount()>0)
	{
		m_lstEmails.setData(0,"BCME_IS_DEFAULT",1);
	}

	//phone:
	if (ui.txtPhone1->text()!="")
	{
		m_lstPhones.setData(0,"BCMP_TYPE_ID",GetTypeFromName(ui.btnPhone1->text(),ContactTypeManager::TYPE_PHONE));
		//do not format skype numbers:
		if (m_lstPhones.getDataRef(0,"BCMP_TYPE_ID")!=ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE))
			SetupPhone(ui.txtPhone1->text(),&m_lstPhones,0);
		else
			SetupPhone(ui.txtPhone1->text(),&m_lstPhones,0,true);
	}

	//phone:
	if (ui.txtPhone2->text()!="")
	{
		int nRow=1;
		m_lstPhones.setData(nRow,"BCMP_TYPE_ID",GetTypeFromName(ui.btnPhone2->text(),ContactTypeManager::TYPE_PHONE));
		//do not format skype numbers:
		if (m_lstPhones.getDataRef(nRow,"BCMP_TYPE_ID")!=ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE))
			SetupPhone(ui.txtPhone2->text(),&m_lstPhones,nRow);
		else
			SetupPhone(ui.txtPhone2->text(),&m_lstPhones,nRow,true);
	}

	//phone:
	if (ui.txtPhone3->text()!="")
	{
		int nRow=2;
	
		m_lstPhones.setData(nRow,"BCMP_TYPE_ID",GetTypeFromName(ui.btnPhone3->text(),ContactTypeManager::TYPE_PHONE));
		//do not format skype numbers:
		if (m_lstPhones.getDataRef(nRow,"BCMP_TYPE_ID")!=ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE))
			SetupPhone(ui.txtPhone3->text(),&m_lstPhones,nRow);
		else
			SetupPhone(ui.txtPhone3->text(),&m_lstPhones,nRow,true);
	}

	//remove empty:
	m_lstPhones.clearSelection();
	if (ui.txtPhone1->text()=="")
		m_lstPhones.selectRow(0);
	if (ui.txtPhone2->text()=="")
		m_lstPhones.selectRow(1);
	if (ui.txtPhone3->text()=="")
		m_lstPhones.selectRow(2);
	m_lstPhones.deleteSelectedRows();


	//set def=1
	if (m_lstPhones.getRowCount()>0)
	{
		m_lstPhones.setData(0,"BCMP_IS_DEFAULT",1);
	}

	//remove empty:
	if (ui.txtWeb->text()=="")
		m_lstInternet.clear();

	//set web:
	if (m_lstInternet.getRowCount()>0)
	{
		m_lstInternet.setData(0,"BCMI_IS_DEFAULT",1);
		m_lstInternet.setData(0,"BCMI_ADDRESS",ui.txtWeb->text());
	}

}


//set data:

void ClientSetupWizPage_PersonData::OnMenuTriggerEmail1(QAction *action)
{
	ui.btnEmail1->setText(action->text());
}
void ClientSetupWizPage_PersonData::OnMenuTriggerEmail2(QAction *action)
{
	ui.btnEmail2->setText(action->text());

}
void ClientSetupWizPage_PersonData::OnMenuTriggerPhone1(QAction *action)
{
	ui.btnPhone1->setText(action->text());

}
void ClientSetupWizPage_PersonData::OnMenuTriggerPhone2(QAction *action)
{
	ui.btnPhone2->setText(action->text());

}
void ClientSetupWizPage_PersonData::OnMenuTriggerPhone3(QAction *action)
{
	ui.btnPhone3->setText(action->text());
}


void ClientSetupWizPage_PersonData::SetupPhone(QString strPhone, DbRecordSet *data, int k, bool bIsSkype)
{
	//split & save:
	QString strCountry,strLocal,strISO,strArea,strSearch;
	m_PhoneFormatter.SplitPhoneNumber(strPhone,strCountry,strArea,strLocal,strSearch,strISO);
	if (!bIsSkype)
	{	
		data->setData(k,"BCMP_FULLNUMBER",m_PhoneFormatter.FormatPhoneNumber(strCountry,strArea,strLocal));
		data->setData(k,"BCMP_COUNTRY",strCountry);
		data->setData(k,"BCMP_AREA",strArea);
		data->setData(k,"BCMP_LOCAL",strLocal);
		data->setData(k,"BCMP_COUNTRY_ISO",strISO);
	}
	else
		data->setData(k,"BCMP_FULLNUMBER",strPhone);

	data->setData(k,"BCMP_SEARCH",strSearch); //only set search 
}




//get it:
bool ClientSetupWizPage_PersonData::GetPageResult(DbRecordSet &RecordSet)
{
	Status err;
	SaveFromGUI();

	m_lstData.setData(0,"LST_PHONE",m_lstPhones);
	m_lstData.setData(0,"LST_ADDRESS",m_lstAddress);
	m_lstData.setData(0,"LST_EMAIL",m_lstEmails);
	m_lstData.setData(0,"LST_INTERNET",m_lstInternet);

	int nSubDataFilter = 0;
	QString pLockResourceID;
	DbRecordSet lstUAR, lstGAR;
	_SERVER_CALL(BusContact->WriteData(err,m_lstData, nSubDataFilter, pLockResourceID, lstUAR, lstGAR)); //insert new contact

	_CHK_ERR_NO_RET(err);
	if (err.IsOK())
	{
		int nEntityRecordID=m_lstData.getDataRef(0,"BCNT_ID").toInt();
		m_recResultRecordSet.setData(0,0,nEntityRecordID);
		RecordSet=m_recResultRecordSet;
		return true;
	}
	else
	{
		RecordSet=m_recResultRecordSet;
		return false;
	}

}

void ClientSetupWizPage_PersonData::FieldChanged(const QString & text)
{
	if (!text.isEmpty())
	{
		m_bComplete = true;
		emit completeStateChanged();
	}
}


//after retry reset: add rows:
void ClientSetupWizPage_PersonData::resetPage()
{
	//insert 2 mail, 3 phone
	m_lstPhones.clear();
	m_lstPhones.addRow();
	m_lstPhones.addRow();
	m_lstPhones.addRow();
	m_lstEmails.clear();
	m_lstEmails.addRow();
	m_lstEmails.addRow();
	m_lstInternet.clear();
	m_lstInternet.addRow();

}

void ClientSetupWizPage_PersonData::OnSetFocusDelayed()
{
	ui.txtFirstName->setFocus();
}