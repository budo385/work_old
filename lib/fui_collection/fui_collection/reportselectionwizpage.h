#ifndef REPORTSELECTIONWIZPAGE_H
#define REPORTSELECTIONWIZPAGE_H

#include "generatedfiles/ui_reportselectionwizpage.h"
#include "gui_core/gui_core/wizardpage.h"

class ReportSelectionWizPage : public WizardPage
{
    Q_OBJECT

public:
    ReportSelectionWizPage(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
    ~ReportSelectionWizPage();
	void Initialize();

private:
    Ui::ReportSelectionWizPageClass ui;

	void BuildReportList();
	void AddSelectedReports();
	void RemoveSelectedReports();
	void SelectReportsFromCache();

	QList<WizardPage*> GetNextPageCollectionVector();
	
	DbRecordSet			m_ReportsRecordSet;				//< Available reports recordset.

private slots:
	//void on_RemoveAll_pushButton_clicked();
	//void on_RemoveSelected_pushButton_clicked();
	void CompleteChanged();
	void OnDblClick();
	//void on_AddAll_pushButton_clicked();
	//void on_AddSelected_pushButton_clicked();
};

#endif // REPORTSELECTIONWIZPAGE_H
