#ifndef SETTINGS_PROGRAMUPDATES_H
#define SETTINGS_PROGRAMUPDATES_H

#include "generatedfiles/ui_settings_programupdates.h"
#include "settingsbase.h"

class Settings_ProgramUpdates : public SettingsBase
{
	Q_OBJECT

public:
	Settings_ProgramUpdates(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
	~Settings_ProgramUpdates();

	DbRecordSet GetChangedValuesRecordSet();

private:
	Ui::Settings_ProgramUpdatesClass ui;
};

#endif // SETTINGS_PROGRAMUPDATES_H
