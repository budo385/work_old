#ifndef CALENDAREVENT_DETAILPANE_H
#define CALENDAREVENT_DETAILPANE_H

#include <QWidget>
#include "generatedfiles/ui_calendarevent_detailpane.h"
#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/guifieldmanager.h"

class CalendarEvent_DetailPane : public QWidget , public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	CalendarEvent_DetailPane(QWidget *parent = 0);
	~CalendarEvent_DetailPane();

	void	RefreshDisplay();
	void	SetEditMode(bool bEdit=true);
	void	SetData(DbRecordSet &lstDataCalEvent,DbRecordSet &lstDataCalEventPart,DbRecordSet &lstDataCalEventOption, DbRecordSet &lstNmrxPersonContact, DbRecordSet &lstNmrxProjects,DbRecordSet &lstNmrxResources);
	void	GetData(DbRecordSet &lstDataCalEvent,DbRecordSet &lstDataCalEventPart,DbRecordSet &lstDataCalEventOption, DbRecordSet &lstNmrxPersonContact, DbRecordSet &lstNmrxProjects,DbRecordSet &lstNmrxResources);
	int		GetPartCommEntityID();
	int		GetPartID();
	int		GetMainEventID();
	int		GetCurrentOptionRow();
	void	SetCurrentOptionDates(QDateTime from,QDateTime to,bool bOverRide=false);
	void	SetOptionRow(int nOptionID);
	void	AssignContacts(QList<int> &lst);
	void	AssignUsers(QList<int> &lst);
	void	AssignProject(QList<int> &lst);
	void	AssignResources(QList<int> &lst);
	void	updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void	RefreshTitle(QString strTitle);
	void	ShowCkbNewCRP(bool bIsTemplateActivated);

signals:
	void SignalTemplateChanged(int);
	void SignalInviteDataChanged();
	void DateTimeFromChanged(const QDateTime &);
	void DateTimeToChanged(const QDateTime &);

private slots:
	void OnOptionRowAdded(int nPrevRow,int nCurrRow);
	void OnOptionRowDeleted(int nRow);
	void OnOptionRowChanged(int nPrevRow,int nCurrRow);
	void OnOptionAccepted(int);
	void OnEditor_VerticalOpenClose_clicked(bool);
	void OnNMRXPersonChanged();
	void OnNMRXProjectsChanged();
	void OnNMRXResourcesChanged();
	void OnSendInvite(DbRecordSet);
	void OnBreakRowInserted(int nRow);
	void OnBreakDataChanged();
	void OnOpenCalendar_Person(int,DbRecordSet,int,DbRecordSet);
	void OnOpenCalendar_Project(int,DbRecordSet,int,DbRecordSet);
	void OnOpenCalendar_Resources(int,DbRecordSet,int,DbRecordSet);

	void OnDateTimeFromChanged();
	void OnDateTimeToChanged();
	

private:
	Ui::CalendarEvent_DetailPaneClass ui;

	void SaveBreaksForOptionRow(int nOptionRow);
	void LoadBreaksForOptionRow(int nOptionRow);

	DbRecordSet m_lstDataCalEvent;
	DbRecordSet m_lstDataCalEventPart;
	DbRecordSet m_lstDataCalEventOption;
	DbRecordSet m_lstDataCalEventBreaks;
	//DbRecordSet m_lstDataCalEventPartTaskData;

	GuiFieldManager *m_GuiFieldManagerOption;
	GuiFieldManager *m_GuiFieldManagerPart;
};

#endif // CALENDAREVENT_DETAILPANE_H
