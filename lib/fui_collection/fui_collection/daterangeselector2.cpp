#include "daterangeselector2.h"

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "gui_core/gui_core/thememanager.h"
#include "common/common/datahelper.h"
#include "gui_core/gui_core/gui_helper.h"

#include <QHBoxLayout>
#include  <QMessageBox>

DateRangeSelector2::DateRangeSelector2(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);

//	m_bUseAsFocusOut = true;
	m_bUseAsFocusOut = false;
	m_bFromCombo	 = false;
	m_bFromCalendar	 = false;
	m_pSpacer = new QSpacerItem(0, 0, QSizePolicy::Expanding);

	//Set geometry.
	m_bSideBarMode = false;
	if (ThemeManager::GetViewMode() == ThemeManager::VIEW_SIDEBAR)
		m_bSideBarMode = true;

	if (m_bSideBarMode)
	{
		
		int nHeight = 18;
		ui.label_4->setMaximumHeight(nHeight);
		ui.dateFrom->setMaximumHeight(nHeight);
		ui.label_5->setMaximumHeight(nHeight);
		ui.dateTo->setMaximumHeight(nHeight);
		ui.cboRangeSize->setMaximumHeight(nHeight);
		ui.btnCurDateRange->setMaximumHeight(22);
		ui.btnPrev->setMaximumHeight(22);
		ui.btnNext->setMaximumHeight(22);
		
		setMinimumWidth(270);
		setMaximumWidth(270);
		setMinimumHeight(50);
		QHBoxLayout *horLayout = new QHBoxLayout();
		QGridLayout *gridLayout = new QGridLayout();
		horLayout->setSpacing(0);
		horLayout->setContentsMargins( 0, 0, 0, 0);
		gridLayout->setHorizontalSpacing(4);
		gridLayout->setVerticalSpacing(2);
		gridLayout->setContentsMargins( 0, 0, 0, 0);
		gridLayout->addWidget(ui.label_4, 0, 0);
		gridLayout->setAlignment(ui.label_4, Qt::AlignRight);
		gridLayout->addWidget(ui.dateFrom, 0, 1);
		gridLayout->addWidget(ui.label_5, 1, 0);
		gridLayout->setAlignment(ui.label_5, Qt::AlignRight);
		gridLayout->addWidget(ui.dateTo, 1, 1);
		horLayout->addItem(gridLayout);
		horLayout->addSpacing(10);
		horLayout->addWidget(ui.btnCurDateRange);
		horLayout->setStretchFactor(ui.btnCurDateRange, 0);
		horLayout->addWidget(ui.btnPrev);
		horLayout->setStretchFactor(ui.btnPrev, 0);
		horLayout->addWidget(ui.cboRangeSize);
		horLayout->setStretchFactor(ui.cboRangeSize, 5);
		//horLayout->setAlignment(ui.cboRangeSize, Qt::AlignHCenter);
		horLayout->addWidget(ui.btnNext);
		horLayout->setStretchFactor(ui.btnNext, 0);
		m_pSpacer->changeSize(1000,10, QSizePolicy::Expanding);
		horLayout->addSpacerItem(m_pSpacer);
		//horLayout->addStretch(1000);
		setLayout(horLayout);
	}
	else
	{
		QHBoxLayout *horLayout = new QHBoxLayout();
		horLayout->setSpacing(4);
		horLayout->addWidget(ui.label_4);
		horLayout->addWidget(ui.dateFrom);
		horLayout->addWidget(ui.label_5);
		horLayout->addWidget(ui.dateTo);
		horLayout->addWidget(ui.btnCurDateRange);
		horLayout->addWidget(ui.btnPrev);
		horLayout->addWidget(ui.cboRangeSize);
		horLayout->addWidget(ui.btnNext);
		horLayout->setContentsMargins( 0, 0, 0, 0);
		m_pSpacer->changeSize(0,0, QSizePolicy::Expanding);
		horLayout->addSpacerItem(m_pSpacer);
		setLayout(horLayout);
	}

	//Set icons.
	ui.btnCurDateRange->setText("");
	ui.btnPrev->setText("");
	ui.btnNext->setText("");
	ui.btnCurDateRange->setIcon(QIcon(":2downarrow.png"));
	ui.btnPrev->setIcon(QIcon(":1leftarrow.png"));
	ui.btnNext->setIcon(QIcon(":1rightarrow.png"));
	ui.btnCurDateRange->setFlat(true);
	ui.btnPrev->setFlat(true);
	ui.btnNext->setFlat(true);

	ui.cboRangeSize->addItem("-", DataHelper::UNDEFINED);
	ui.cboRangeSize->addItem(tr("Day"), DataHelper::DAY);
	ui.cboRangeSize->addItem(tr("3 Days"), DataHelper::THREE_DAYS);
	ui.cboRangeSize->addItem(tr("Business Days"), DataHelper::BUSS_DAYS);
	ui.cboRangeSize->addItem(tr("Week"), DataHelper::WEEK);
	ui.cboRangeSize->addItem(tr("Two Weeks"), DataHelper::TWO_WEEK);
	ui.cboRangeSize->addItem(tr("Month"), DataHelper::MONTH);
	ui.cboRangeSize->addItem(tr("Year"), DataHelper::YEAR);
	ui.cboRangeSize->setCurrentIndex(0);

	ui.dateFrom->setDate(QDate::currentDate());
	ui.dateTo->setDate(QDate::currentDate());

	connect(ui.dateFrom,SIGNAL(dateChanged(const QDate)),this, SLOT(OnDateFromChanged(const QDate)));
	connect(ui.dateTo,SIGNAL(dateChanged(const QDate)),this, SLOT(OnDateToChanged(const QDate)));
	//connect(ui.dateFrom,SIGNAL(focusOutSignal()),this, SLOT(OnFromDateFocusOut()));
	//connect(ui.dateTo,SIGNAL(focusOutSignal()),this, SLOT(OnToDateFocusOut()));
	connect(ui.cboRangeSize, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboChanged(int)));
}

DateRangeSelector2::~DateRangeSelector2()
{
}

void DateRangeSelector2::on_btnPrev_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	DataHelper::DateRangeSelectorPreviousNextButtonClicked(dateFrom, dateTo, nRangeIdx, -1);

	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	if (m_bUseAsFocusOut)
		emit onDateChangedForCalendar(dtFrom, dtTo, nRangeIdx, false); 
	else
		emit onDateChanged(dtFrom, dtTo, nRangeIdx); 
}

void DateRangeSelector2::on_btnNext_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();
	
	DataHelper::DateRangeSelectorPreviousNextButtonClicked(dateFrom, dateTo, nRangeIdx, 1);

	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	if (m_bUseAsFocusOut)
		emit onDateChangedForCalendar(dtFrom, dtTo, nRangeIdx, false); 
	else
		emit onDateChanged(dtFrom, dtTo, nRangeIdx); 
}

void DateRangeSelector2::on_btnCurDateRange_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();
	
	DataHelper::DateRangeSelectorCurrentButtonClicked(dateFrom, dateTo, nRangeIdx);
	
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	if (m_bUseAsFocusOut)
	{
		emit onDateChangedForCalendar(dtFrom, dtTo, nRangeIdx, m_bFromCombo); 
		m_bFromCombo = false;
	}
	else
		emit onDateChanged(dtFrom, dtTo, nRangeIdx); 
}

void DateRangeSelector2::Initialize(QDateTime &dateFrom, QDateTime &dateTo, int nRange, bool bFromCalendar /*= false*/)
{
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.cboRangeSize->blockSignals(true);

	int nIndex = ui.cboRangeSize->findData(nRange);
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	ui.cboRangeSize->setCurrentIndex(nIndex);
	ui.dateFrom->setDate(dateFrom.date());
	ui.dateTo->setDate(dateTo.date());

	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);
	ui.cboRangeSize->blockSignals(false);

	//When in calendar be a little wider.
	m_bFromCalendar = bFromCalendar;
	if (m_bFromCalendar)
	{
		ui.cboRangeSize->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		m_pSpacer->changeSize(10,10, QSizePolicy::Expanding);
		setMinimumWidth(450);
		setMaximumWidth(450);
	}
}

void DateRangeSelector2::UseAsFocusOut(bool bUseAsFocusOut)
{
	m_bUseAsFocusOut = bUseAsFocusOut;
	ui.dateFrom->setReadOnly(true);
	ui.dateTo->setReadOnly(true);
}

void DateRangeSelector2::SendDateChangedSignal(QDate datFrom, QDate datTo)
{
	ui.cboRangeSize->blockSignals(true);
	ui.cboRangeSize->setCurrentIndex(0);
	int   nRange	= DataHelper::UNDEFINED;
	ui.cboRangeSize->blockSignals(false);

	QDateTime dtFrom = QDateTime(datFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(datTo, QTime(23,59,59));

	if (m_bUseAsFocusOut)
	{
		emit onDateChangedForCalendar(dtFrom, dtTo, nRange, m_bFromCombo); 
		m_bFromCombo = false;
	}
	emit onDateChanged(dtFrom, dtTo, nRange); 
}

void DateRangeSelector2::OnDateFromChanged(const QDate &)
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	if (dateFrom>dateTo)
	{
		dateTo = dateFrom;
		ui.dateTo->blockSignals(true);
		ui.dateTo->setDate(dateTo);
		ui.dateTo->blockSignals(false);
	}

	SendDateChangedSignal(dateFrom, dateTo);
}

void DateRangeSelector2::OnDateToChanged(const QDate &)
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	if (dateTo<dateFrom)
	{
		dateFrom=dateTo;
		ui.dateFrom->blockSignals(true);
		ui.dateFrom->setDate(dateFrom);
		ui.dateFrom->blockSignals(false);
	}

	SendDateChangedSignal(dateFrom, dateTo);
}

void DateRangeSelector2::OnComboChanged(int nIdx)
{
	m_bFromCombo = true;
	on_btnCurDateRange_clicked();
}
/*
void DateRangeSelector2::OnFromDateFocusOut()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	
	ui.cboRangeSize->blockSignals(true);
	ui.cboRangeSize->setCurrentIndex(0);
	int   nRange	= DataHelper::UNDEFINED;
	ui.cboRangeSize->blockSignals(false);

	emit onDateChangedForCalendar(QDateTime(dateFrom, QTime(0,0,0)), QDateTime(dateTo, QTime(23,59,59)), nRange, true); 
}
*/
/*
void DateRangeSelector2::OnToDateFocusOut()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();

	ui.cboRangeSize->blockSignals(true);
	ui.cboRangeSize->setCurrentIndex(0);
	int   nRange	= DataHelper::UNDEFINED;
	ui.cboRangeSize->blockSignals(false);

	emit onDateChangedForCalendar(QDateTime(dateFrom, QTime(0,0,0)), QDateTime(dateTo, QTime(23,59,59)), nRange, true); 
}
*/