#ifndef FUI_BUSDEPARTMENTS_H
#define FUI_BUSDEPARTMENTS_H

#include <QWidget>
#include "generatedfiles/ui_fui_busdepartments.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"

class FUI_BusDepartments : public FuiBase
{
    Q_OBJECT

public:
    FUI_BusDepartments(QWidget *parent = 0);
    ~FUI_BusDepartments();

	virtual void on_selectionChange(int nEntityRecordID);
	virtual void on_cmdInsert();
	void SetFiltersForCurrentActualOrganization(int nActualOrganizationID);
	QString GetFUIName();

private:
	void FilterAssigments(int nDeptID);
	void RefreshDisplay();
	void SetMode(int nMode);

    Ui::fui_busdepartmentsClass ui;
	GuiFieldManager *m_GuiFieldManagerMain;

protected:
	void DataPrepareForInsert(Status &err);
	void DataCheckBeforeWrite(Status &err);

private slots:
	void on_btnEditDescription_clicked();
	void on_btnOpenPersons_clicked();
};

#endif // FUI_BUSDEPARTMENTS_H
