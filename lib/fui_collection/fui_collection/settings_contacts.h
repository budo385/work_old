#ifndef SETTINGS_CONTACTS_H
#define SETTINGS_CONTACTS_H

#include <QWidget>
#include "generatedfiles/ui_settings_contacts.h"
#include "settingsbase.h"
#include "bus_client/bus_client/selection_combobox.h"


class Settings_Contacts : public SettingsBase
{
	Q_OBJECT

public:
	Settings_Contacts(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
	~Settings_Contacts();

	DbRecordSet GetChangedValuesRecordSet();
	void GetFolderSetting(QList<QStringList> &lstFolders);
	void RefreshInfoLabel();

private slots:
	void OnCustomizeAddrPerson();
	void OnCustomizeAddrOrg();
	void OpenGroupViewEditor();
	void OpenACOViewEditor();
	void on_btnSelectFolders_clicked();

private:
	Ui::Settings_ContactsClass ui;
	QList<QStringList> m_lstOutlookFoldersScan;

	void LoadTree(DbRecordSet &lstData,bool bReloadFromServer=false);
	void LoadViewSelector();

	Selection_ComboBox m_ComboHandler;
	Selection_ComboBox m_ComboHandlerACO;
	Selection_ComboBox m_FormatAddressCombo;
	Selection_ComboBox m_FormatAddressComboOrg;
};

#endif // SETTINGS_CONTACTS_H
