#ifndef DATERANGESELECTOR2_H
#define DATERANGESELECTOR2_H

#include <QWidget>
#include "ui_daterangeselector2.h"
#include <QSpacerItem>

class DateRangeSelector2 : public QWidget
{
    Q_OBJECT

public:
    DateRangeSelector2(QWidget *parent = 0);
    ~DateRangeSelector2();

	void Initialize(QDateTime &dateFrom, QDateTime &dateTo, int nRange, bool bFromCalendar = false);
	void UseAsFocusOut(bool bUseAsFocusOut);

private:
    Ui::DateRangeSelector2Class ui;

	void SendDateChangedSignal(QDate datFrom, QDate datTo);

	bool m_bUseAsFocusOut;
	bool m_bSideBarMode;
	bool m_bFromCombo;
	bool m_bFromCalendar;
	QSpacerItem *m_pSpacer;

signals:
	void onDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange);
	void onDateChangedForCalendar(QDateTime &dtFrom, QDateTime &dtTo, int nRange, bool bResizeColumns);
	void onFocusOut(QDateTime &dtFrom, QDateTime &dtTo, int nRange, bool bResizeColumns);

private slots:
	void on_btnPrev_clicked();
	void on_btnNext_clicked();
	void on_btnCurDateRange_clicked();
	void OnDateFromChanged(const QDate &);
	void OnDateToChanged(const QDate &);
	void OnComboChanged(int nIdx);
	//void OnFromDateFocusOut();
	//void OnToDateFocusOut();
};

#endif // DATERANGESELECTOR2_H
