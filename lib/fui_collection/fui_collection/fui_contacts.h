#ifndef FUI_CONTACTS_H
#define FUI_CONTACTS_H

#include <QHash>
#include <QTabWidget>
#include "generatedfiles/ui_fui_contacts.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "gui_core/gui_core/gui_helper.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"


class FUI_Contacts : public FuiBase
{
	friend class fui_avatar;
	friend class Table_ContactPhone;
	friend class Table_ContactInternet;
	friend class Table_ContactEmail;

    Q_OBJECT

public:
    FUI_Contacts(QWidget *parent = 0, bool bOpenedAsAvatar = false);
    ~FUI_Contacts();

	enum FUI_ShowType
	{
		TYPE_FULL_CONTACT,
		TYPE_SHORT_CONTACT
	};
	enum DetailTabs //tabContactGroups
	{
		TAB_DETAIL_DESC=0,
		TAB_DETAIL_PHONE,
		TAB_DETAIL_ADDRESS,
		TAB_DETAIL_EMAIL,
		TAB_DETAIL_INTERNET,
		TAB_DETAIL_DETAILS,
		TAB_DETAIL_GROUPS,
		TAB_DETAIL_PICTURES,
		TAB_DETAIL_RESERVATIONS,
		TAB_DETAIL_DEBTOR
	};
	enum MainTabs //tabWidgetContact
	{
		TAB_MAIN_COMM=0,
		TAB_MAIN_DETAILS
	};
	enum CommunicationTabs //tabWidgetComm
	{
		TAB_COMM_OVERVIEW=0,
		TAB_COMM_USERS_CONTACTS,
		TAB_COMM_PROJECTS,
		TAB_COMM_JOURNAL
	};
	enum HeaderTabs //tabWidget
	{
		TAB_HEADER_CONTACT=0,
		TAB_HEADER_GROUP_LIST,
		TAB_HEADER_CONTACT_ACTUAL_DETAIL_LIST
	};

	void on_selectionChange(int nEntityRecordID);
	bool on_cmdDelete();
	bool on_cmdEdit();
	void on_cmdInsert();
	bool on_cmdOK();
	void on_cmdCopy();

	void SetCurrentTab(int nTabPane, int nDetailTab=TAB_DETAIL_DETAILS);
	void GetCurrentTab(int &nTabPane, int &nDetailTab, int &nCommTab);
	void SwitchSelectorToFavoriteTab();
	void FlipSelectorToFavoriteTabOrList();
	void FlipShowQuickInfo();
	void notifyCache_AfterWrite(DbRecordSet recNewData);
	QString GetFUIName();
	QTabWidget* GetMainTabWidget(){return ui.tabWidgetMain;}
	CommunicationMenu_Base* GetComMenu(){return m_p_CeMenu;};
	bool CanClose();


	bool m_bHideOnMinimize;
	
public slots:
	void OnShowContactDetails(int nContactID);
	void OnCEMenu_NeedNewData(int);
	void OnCEMenu_NewDataDropped(int,DbRecordSet);
	void OnNMRXProjectsChanged();
	void OnNMRXContactsChanged();
	void OnCEGridChanged();
	void OnJournalGridChanged();
	void OnGroupSAPNE_Add(DbRecordSet &);
	void OnGroupSAPNE_Remove(DbRecordSet &);
	void OnGroupSAPNE_Edit();
	
private slots:
	void OnResizeDelayed();
	void on_btnPerson_toggled(bool);
	void on_ckbSynchronize_toggled(bool);
	void on_tabWidgetMain_currentChanged(int);
	void on_groupBoxInfo_clicked();
	void on_cmbInfo_Address_currentIndexChanged(int nIndex);
	void on_btnExportSerialLetter_clicked();
	void on_btnDebtorCode_clicked();
	void onActualListChange();
	void On_SelectionControllerSideBarDestroyed();
	void on_btnInternet_clicked();
	void on_btnAddress_clicked();
	void on_btnEmail_clicked();
	void on_btnPhone_clicked();
	void on_btnSerialEmail_clicked();
	void OnSetFindFocusDelayed();
	void OnFavoriteChanged(int nMsgDetail);
	void OnBtnCalendar_Click();
	void OnTabWidgetContact_currentChanged(int);
	void OnGridRequestEditModeAfterDblClick();
	void OnGridRequestCloseEditModeAfterDblClick();

protected:
	void DataWrite(Status &err);
	void DataPrepareForInsert(Status &err);
	void DataPrepareForCopy(Status &err);
	void DataClear();
	void DataDefine();
	void DataRead(Status &err,int nRecordID);
	void DataUnlock(Status &err);
	void DataCheckBeforeWrite(Status &err);
	
	virtual void changeEvent (QEvent * event);
	virtual void closeEvent(QCloseEvent *event);
	void resizeEvent ( QResizeEvent * event );
	virtual void keyPressEvent( QKeyEvent * event ); 

private:
    Ui::FUI_ContactsClass ui;

	QString GetDefaultMail();
	QString GetDefaultPhoneCall();
	QString	GetDefaultInternet();
	void	RefreshQuickInfo();
	void	ClearEmptyEntries();
	void RefreshDisplay();
	void SetMode(int nMode);
	void prepareCacheRecord(DbRecordSet *recNewData);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void SaveCommGridChBoxFilterState(int nState, int nSettingID);
	void ReloadFromAvatar();
	void SubData2ContactRow();
	void ContactRow2SubData();
	void WriteContactGroupAssignmentsAfterCopy();

	//Gui maanger:
	GuiFieldManager *m_GuiFieldManagerMain;
	GuiFieldManager *m_GuiFieldManagerDebtor;
	DbRecordSet m_lstPhones;
	DbRecordSet m_lstEmails;
	DbRecordSet m_lstInternet;
	DbRecordSet m_lstCustomFields;
	DbRecordSet m_lstPictures;
	DbRecordSet m_lstAddress;
	DbRecordSet m_lstDebtor;
	DbRecordSet	m_lstAddressOrganizationchangePropagate;
	DbRecordSet m_lstGroupContactAssignmentsForCopy;

	QString m_strHtmlTag;
	QString m_strEndHtmlTag;
	bool m_bSyncNames;
	QAction* m_pActSendMail;
	QAction* m_pActPhoneCall;
	Selection_Contacts* m_p_Selector;
	CommunicationMenu_Base* m_p_CeMenu;
	MainEntityCalculatedName m_CalcName;
	QCheckBox *m_buttonQCW;
	bool m_bOpenedAsAvatar;
	ObsrPtrn_Subject *m_pQCW_LastOpened;
	QString m_strPrevOrgName;
	Selection_Contacts *m_pFrameContacts;
	StyledPushButton *m_pCalendarButton;
	bool m_bGridDialogRequestedEditMode;
};

#endif // FUI_CONTACTS_H

