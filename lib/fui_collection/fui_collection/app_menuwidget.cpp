#include "app_menuwidget.h"

#include "gui_core/gui_core/gui_helper.h"
#include <QMimeData>
#include <QHeaderView>
#include <QFileDialog>
#include <QAction>
#include "common/common/datahelper.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/styledpushbutton.h"
#include <QStatusBar>
#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/documenthelper.h"
#include "communicationmenu_base.h"

#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight					*g_AccessRight;				//global access right tester
#include "fuimanager.h"
extern FuiManager g_objFuiManager;					
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
extern QString g_strLastDir_FileOpen;


App_MenuWidget::App_MenuWidget(QWidget *parent,bool bIgnoreViewMode)
: QWidget(parent)
{
	ui.setupUi(this);
	m_bCloseOnSend=false;

	this->setAttribute(Qt::WA_DeleteOnClose, true);
	setAcceptDrops(true);

	m_lstProjects.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_lstContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_rowQCWContactDef.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

	//MAIN BUTTON
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR && !bIgnoreViewMode)
	{
		ui.treeWidgetApp->setStyleSheet("* { font-size: 9px }"); 
	}

	connect(ui.treeWidgetApp,SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)),this,SLOT(OnNewFromApp())); 
	//APP TOOLBARS
	connect(ui.btnNewFromApp, SIGNAL(clicked()), this, SLOT(OnNewFromApp()));
	connect(ui.btnAddApp, SIGNAL(clicked()), this, SLOT(OnAddApp()));
	connect(ui.btnModifyApp, SIGNAL(clicked()), this, SLOT(OnModifyApp()));
	connect(ui.btnRemoveApp, SIGNAL(clicked()), this, SLOT(OnRemoveApp()));
	ui.btnNewFromApp->setIcon(QIcon(":Document16.png"));
	ui.btnNewFromApp->setToolTip(tr("New Document For Selected Application"));
	ui.btnAddApp->setIcon(QIcon(":handwrite.png"));
	ui.btnModifyApp->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnRemoveApp->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnModifyApp->setToolTip(tr("Modify/Create"));
	ui.btnRemoveApp->setToolTip(tr("Delete"));
	ui.btnAddApp->setToolTip(tr("Add"));
	connect(ui.btnPaths, SIGNAL(clicked()), this, SLOT(OnUserPaths()));
	ui.btnPaths->setIcon(QIcon(":handwithbook.png"));
	ui.btnPaths->setToolTip(tr("My Application Paths"));
	//APP TREE:
	ui.treeWidgetApp->header()->hide();
	ui.treeWidgetApp->setColumnCount(2);
	ui.treeWidgetApp->setSelectionBehavior( QAbstractItemView::SelectRows);
	ui.treeWidgetApp->setSelectionBehavior( QAbstractItemView::SelectItems);
	//LOGGED USER:
	m_nLoggedID=g_pClientManager->GetPersonID();

	LoadApplications();
	LoadTemplates();

	g_ChangeManager.registerObserver(this);

	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;

	//---------------------------------------- INIT CNTX MENU APP--------------------------------//
	pAction = new QAction(tr("New Document Using Application"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnNewFromApp()));
	lstActions.append(pAction);
	pAction = new QAction(tr("Reload"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(ReLoadApplications()));
	lstActions.append(pAction);
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	if (g_AccessRight->TestAR(REGISTER_APPLICATIONS))
	{
		pAction = new QAction(tr("Add Application"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnAddApp()));
		lstActions.append(pAction);

	}
	pAction = new QAction(tr("Modify Application"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnModifyApp()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete Application"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnRemoveApp()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("View my application paths"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnUserPaths()));
	lstActions.append(pAction);

	//set cnxt menu:
	ui.treeWidgetApp->SetContextMenuActions(lstActions);

	lstActions.clear();


	if (!g_AccessRight->TestAR(REGISTER_APPLICATIONS))
		ui.btnAddApp->setEnabled(false);

}

App_MenuWidget::~App_MenuWidget()
{
	g_ChangeManager.unregisterObserver(this);
}


/*
Qt::DropActions App_MenuWidget::supportedDropActions() const 
{
return  Qt::CopyAction | Qt::MoveAction | Qt::IgnoreAction | Qt::LinkAction | Qt::ActionMask | Qt::TargetMoveAction;
}
*/
void App_MenuWidget::dragEnterEvent ( QDragEnterEvent * event )
{
	if (event->mimeData()->hasUrls())
		event->accept();

}

//if TAB= template then template, else doc, if not exe...brb....
void App_MenuWidget::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	if (!mime->hasUrls()) return;

	{
		QList<QUrl> lst =mime->urls();
		bool bDropOnTemplates=false;
		bool bAcceptApplications=true;

		if (bAcceptApplications)
			if (!g_AccessRight->TestAR(REGISTER_APPLICATIONS))
				bAcceptApplications=false;

		if (bDropOnTemplates)
			if (!g_AccessRight->TestAR(REGISTER_TEMPLATES))
				bDropOnTemplates=false;

		if (!bDropOnTemplates && !bAcceptApplications)
		{
			event->ignore();
			return;
		}

		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT);
		this->activateWindow(); //bring on top_:
		g_CommManager.ProcessDocumentDrop(lst,bDropOnTemplates,bAcceptApplications,false);
	}

}



//load from server, rebuild tree
void App_MenuWidget::LoadApplications(bool bReload)
{
	if (m_nLoggedID==0) return;

	ui.treeWidgetApp->blockSignals(true);
	ui.treeWidgetApp->clear();

	DocumentHelper::GetDocApplications(m_lstApplications,bReload);

	int nIDIdx=m_lstApplications.getColumnIdx("BDMA_ID");
	int nCodeIdx=m_lstApplications.getColumnIdx("BDMA_CODE");
	int nNameIdx=m_lstApplications.getColumnIdx("BDMA_NAME");
	int nDesc=m_lstApplications.getColumnIdx("BDMA_DESCRIPTION");

	m_lstApplications.sort(nCodeIdx); //sort by code

	QList<QTreeWidgetItem *> items;
	int nSize=m_lstApplications.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//QString strNodeName=m_lstApplications.getDataRef(i,nCodeIdx).toString()+" "+m_lstApplications.getDataRef(i,nNameIdx).toString();
		QString strNodeName=m_lstApplications.getDataRef(i,nNameIdx).toString();
		int nID=m_lstApplications.getDataRef(i,nIDIdx).toInt();
		QStringList lst;
		lst<<""<<strNodeName;
		QTreeWidgetItem *pRoot=new QTreeWidgetItem((QTreeWidget *)0,lst);
		pRoot->setData(0,Qt::UserRole,nID);
		pRoot->setToolTip(0,m_lstApplications.getDataRef(i,nDesc).toString());
		pRoot->setToolTip(1,m_lstApplications.getDataRef(i,nDesc).toString());
		//set icon for application:
		QByteArray bytePicture=m_lstApplications.getDataRef(i,"BDMA_ICON").toByteArray();
		if (bytePicture.size()>0)
		{
			QPixmap pixMap;
			pixMap.loadFromData(bytePicture);
			QIcon icon(pixMap);
			pRoot->setIcon(0,icon);
		}

		// issue 1339
		if (m_lstApplications.getDataRef(i,"BDMA_NO_FILE_DOCUMENT").toInt()>0)
		{
			pRoot->setIcon(1,QIcon(":StartApp16g.png"));
		}
		else
		{
			pRoot->setIcon(1,QIcon(":Document_16b.png"));
		}


		pRoot->setData(0,Qt::UserRole,nID);
		items.append(pRoot);
	}
	ui.treeWidgetApp->insertTopLevelItems(0,items);
	ui.treeWidgetApp->header()->resizeSection(0,40);
	ui.treeWidgetApp->blockSignals(false);

}




/*! Catches global cache events

\param pSubject			- source of msg
\param nMsgCode			- msg code
\param val				- value sent from observer
*/
void App_MenuWidget::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{

	if(nMsgDetail==ENTITY_BUS_DM_DOCUMENTS)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			//CallBack to refresh displayed data:
			DbRecordSet lstNewData=val.value<DbRecordSet>();
			//set node to lstNewData
			if (lstNewData.getRowCount()>0)
			{
				if (lstNewData.getColumnIdx("BDMD_TEMPLATE_FLAG")>=0)
				{
					if (lstNewData.getDataRef(0,"BDMD_TEMPLATE_FLAG").toInt()>0) //only reload when new template mail is modified
					{
						//Reload data:
						LoadTemplates(true);
					}
				}
				else
					LoadTemplates(true);

			}
			return;
		}

	if(nMsgDetail==ENTITY_BUS_DM_APPLICATIONS)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			//CallBack to refresh displayed data:
			DbRecordSet lstNewData=val.value<DbRecordSet>();

			//Reload data:
			LoadApplications(true);

			ui.treeWidgetApp->blockSignals(true);
			//set node to lstNewData
			if (lstNewData.getRowCount()>0)
			{
				QVariant varNode=lstNewData.getDataRef(0,"BDMA_ID");
				QTreeWidgetItem *item=SearchTree(ui.treeWidgetApp,varNode);
				if (item)
				{
					item->setSelected(true);
					ui.treeWidgetApp->setCurrentItem(item);
				}
			}
			ui.treeWidgetApp->blockSignals(false);
			return;
		}

}




QTreeWidgetItem* App_MenuWidget::SearchTree(QTreeWidget* pTree, QVariant nUserValue)
{
	QTreeWidgetItem* item=NULL;
	int nSize=pTree->topLevelItemCount();
	for(int i=0;i<nSize;++i)
	{
		item=pTree->topLevelItem(i);
		//qDebug()<<item->data(0,Qt::UserRole).toInt();
		if (item->data(0,Qt::UserRole)==nUserValue)
			return item;
	}
	return NULL;
}


//-------------------------------------------------------------------------
//						APPLICATIONS
//-------------------------------------------------------------------------


//app:
void App_MenuWidget::OnNewFromApp()
{
	//get current app:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetApp->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an application!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstApplications.find(0,nRecordID,true);
	if (nRow==-1)return;


	int nAppNoFile=m_lstApplications.getDataRef(nRow,"BDMA_NO_FILE_DOCUMENT").toInt();
	int nAppType=m_lstApplications.getDataRef(nRow,"BDMA_APPLICATION_TYPE").toInt();
	int nDefaultDocType;

	if (nAppNoFile>0 && nAppType!=GlobalConstants::DOC_TYPE_URL)
	{
		nDefaultDocType = GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
	}
	else
	{
		if (nAppType==GlobalConstants::APP_TYPE_FILE)
		{
			nDefaultDocType=DocumentHelper::AskForDocumentType();
			if (nDefaultDocType==-1)return;

			//int nResult=QMessageBox::question(NULL,tr("Choose Document Storage Location"),tr("Choose Document Storage Location:"),tr("  Save Reference(s) to Local File(s)  "),tr("  Store File Centrally on Internet  "),tr(" Cancel "),0,2);
			//if (nResult==0)
			//	nDefaultDocType = GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
			//else if (nResult==1)
			//	nDefaultDocType = GlobalConstants::DOC_TYPE_INTERNET_FILE;
			//else
			//	return; //exit
		}
		else 
		{
			nDefaultDocType=GlobalConstants::DOC_TYPE_URL;
		}
	}


	DbRecordSet row=m_lstApplications.getRow(nRow);
	int nApplication=row.getDataRef(0,"BDMA_ID").toInt();
	if (nApplication<=0)
		return;

	DbRecordSet rowDocData,recRevision;
	rowDocData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
	rowDocData.addRow();

	rowDocData.setData(0,"BDMD_NAME","New Document");
	rowDocData.setData(0,"BDMD_DOC_PATH","");
	rowDocData.setData(0,"BDMD_APPLICATION_ID",nApplication);
	rowDocData.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
	rowDocData.setData(0,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
	rowDocData.setData(0,"CENT_OWNER_ID",m_nLoggedID);
	rowDocData.setData(0,"BDMD_DOC_TYPE",nDefaultDocType);
	//rowDocData.setData(0,"CENT_IS_PRIVATE",0);
	rowDocData.setData(0,"BDMD_TEMPLATE_FLAG",0);


	//if got template and local or internet
	int nTemplateID=row.getDataRef(0,"BDMA_TEMPLATE_ID").toInt();
	if (nTemplateID>0)
	{
		//load template row:
		int nRow=m_lstTemplates.find(0,nTemplateID,true);
		if (nRow==-1) ReLoadTemplates();
		nRow=m_lstTemplates.find(0,nTemplateID,true);
		if (nRow==-1)
		{
			QMessageBox::warning(this,tr("Warning"),tr("Could not load template from application record!"));
			return;
		}


		//-------------------------------------------------------
		//Create doc from template:
		//-------------------------------------------------------


		DbRecordSet row=m_lstTemplates.getRow(nRow);

		if (nDefaultDocType==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
		{
			if(!DocumentHelper::CreateLocalDocumentFromTemplate(row,rowDocData,nAppNoFile))
				return;
		}
		else if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		{
			if(!DocumentHelper::CreateInternetDocumentFromTemplate(row,rowDocData,recRevision,true))
				return;

			//clear revision:
			recRevision.clear();

			//automatically check out revision:
			if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
			{
				rowDocData.setData(0,"BDMD_IS_CHECK_OUT",1);
				rowDocData.setData(0,"BDMD_CHECK_OUT_USER_ID",g_pClientManager->GetPersonID());
				rowDocData.setData(0,"BDMD_CHECK_OUT_DATE",QDateTime::currentDateTime());
			}
		}
		else
		{
			if(!DocumentHelper::CreateDocumentFromTemplate(row,rowDocData,recRevision, true))
				return;
		}

	}

	//override application:
	rowDocData.setData(0,"BDMD_APPLICATION_ID",nApplication);

	//open NoFile docuemnt:
	if (nAppNoFile)
	{
		//get contacts:
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT); //give me new data, if some1 is listening....

		DocumentHelper::SetDocumentDropDefaults(&m_lstContacts,&m_lstProjects,&m_rowQCWContactDef);
		DocumentHelper::OpenDocumentInExternalApp(rowDocData,true);
	}
	else
		WriteDocument(rowDocData,recRevision,true);


	if (m_bCloseOnSend)
		close();

}


void App_MenuWidget::OnAddApp()
{
	g_objFuiManager.OpenFUI(MENU_DM_APPLICATIONS, true, false,FuiBase::MODE_INSERT);
}
void App_MenuWidget::OnModifyApp()
{
	//get selected cost center
	int nRecordID = -1;
	//get current item:
	QList<QTreeWidgetItem *> items=ui.treeWidgetApp->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an application!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_APPLICATIONS, true, false,FuiBase::MODE_EDIT, nRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(pFUI)pFUI->show();  //show FUI

}

void App_MenuWidget::OnRemoveApp()
{

	//get current item:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetApp->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an application!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	//warn
	int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete this record permanently from the database?"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES


	//delete
	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"BDMA_ID");
	lst.addRow();
	lst.setData(0,0,nRecordID);
	Status err;
	QString pLockResourceID;
	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	_SERVER_CALL(ClientSimpleORM->Delete(err,BUS_DM_APPLICATIONS,lst, pLockResourceID, lstStatusRows, pBoolTransaction))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}


		//remove item from tree:
		delete items.at(0);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_DM_APPLICATIONS,QVariant(),this); 

}



void App_MenuWidget::OnUserPaths()
{
	g_objFuiManager.OpenFUI(MENU_DM_USER_PATHS, true, false,FuiBase::MODE_EMPTY);
}



//lstContacts= must have bcnt id, project must have busp_id
void App_MenuWidget::SetDefaults(DbRecordSet *lstContacts,DbRecordSet *lstProjects,DbRecordSet *rowQCWContact)
{
	m_lstContacts.clear();
	m_lstProjects.clear();
	m_rowQCWContactDef.clear();

	if (lstContacts)
	{
		//lstContacts->Dump();

		int nCntIdx=lstContacts->getColumnIdx("BCNT_ID");
		int nCntIdx1=m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		int nSize=lstContacts->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			m_lstContacts.addRow();
			m_lstContacts.setData(m_lstContacts.getRowCount()-1,nCntIdx1,lstContacts->getDataRef(i,nCntIdx));
		}

	}

	if (lstProjects)
	{

		int nCntIdx=lstProjects->getColumnIdx("BUSP_ID");
		int nCntIdx1=m_lstProjects.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		int nSize=lstProjects->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			m_lstProjects.addRow();
			m_lstProjects.setData(m_lstProjects.getRowCount()-1,nCntIdx1,lstProjects->getDataRef(i,nCntIdx));
		}

	}

	if (rowQCWContact)
	{
		if (rowQCWContact->getRowCount()>0)
			m_rowQCWContactDef=*rowQCWContact;
	}
}


void App_MenuWidget::SetCloseOnSend()
{
	m_bCloseOnSend=true;
	//delete on close
	this->setAttribute(Qt::WA_DeleteOnClose, true);
	//m_PieMenu.SetCloseOnOpenDocument(this);
}



void App_MenuWidget::SetSideBarLayout()
{
	this->setWindowFlags(Qt::FramelessWindowHint);

	QVBoxLayout *layout=dynamic_cast<QVBoxLayout*>(this->layout());

	//HEADER:
	QFrame *pHeader= new QFrame(this);
	pHeader->setFrameShape(QFrame::NoFrame);
	pHeader->setMaximumHeight(30);
	pHeader->setMinimumHeight(30);
	pHeader->setObjectName("Header_bar");
	pHeader->setStyleSheet("QFrame#Header_bar "+ThemeManager::GetSidebarChapter_Bkg());

	QSize buttonSize_menu(13, 11);
	StyledPushButton *close		= new StyledPushButton(this,":Icon_CloseBlack.png",":Icon_CloseBlack.png","");
	close		->setMaximumSize(buttonSize_menu);
	close		->setMinimumSize(buttonSize_menu);
	close		->setToolTip(tr("Close"));

	QLabel *label = new QLabel(this);
	label->setStyleSheet("QLabel "+ThemeManager::GetSidebarChapter_Font());
	label->setText(tr("Application Manager"));

	QVBoxLayout *buttonLayout_menu_v = new QVBoxLayout;
	buttonLayout_menu_v->addWidget(close);
	buttonLayout_menu_v->addStretch(1);
	buttonLayout_menu_v->setSpacing(0);
	buttonLayout_menu_v->setContentsMargins(2,0,2,2);

	QHBoxLayout *buttonLayout_menu_h = new QHBoxLayout;

	buttonLayout_menu_h->addWidget(label);
	buttonLayout_menu_h->addStretch(1);
	buttonLayout_menu_h->addLayout(buttonLayout_menu_v);
	buttonLayout_menu_h->setSpacing(0);
	buttonLayout_menu_h->setContentsMargins(0,0,0,0);

	pHeader->setLayout(buttonLayout_menu_h);

	connect(close,SIGNAL(clicked()),this,SLOT(close()));


	//FOOTER:
	QStatusBar *statusBar = new QStatusBar(this);
	statusBar->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	statusBar->setObjectName("Status_bar");

	QFrame *pFooter= new QFrame(this);
	pFooter->setFrameShape(QFrame::NoFrame);
	pFooter->setMaximumHeight(20);
	pFooter->setMinimumHeight(20);

	QHBoxLayout *footer = new QHBoxLayout;
	footer->addWidget(statusBar);
	footer->setSpacing(0);
	footer->setContentsMargins(0,0,0,0);
	pFooter->setLayout(footer);

	pFooter->setStyleSheet("QFrame "+ThemeManager::GetSidebarChapter_Bkg());


	//GLOBAL BKG COLOR:
	this->setObjectName("MY_WIDGET");
	this->setStyleSheet("QWidget#MY_WIDGET "+ThemeManager::GetSidebar_Bkg()+ThemeManager::GetGlobalWidgetStyle());

	//ui.btnNewEntity->setVisible(false);

	//SET IT
	layout->insertWidget(0,pHeader);
	layout->addWidget(pFooter);
}



void App_MenuWidget::mouseMoveEvent(QMouseEvent *event)
{
	move(event->globalPos() - m_dragPosition);
	event->accept();
} 

void App_MenuWidget::mouseReleaseEvent(QMouseEvent * event)
{
	QWidget::mouseReleaseEvent(event);
}


void App_MenuWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && event->y()<30) 
	{
		m_dragPosition = event->globalPos() - frameGeometry().topLeft();
		event->accept();
	}
	else
		m_dragPosition=QPoint(0,0);

} 



bool App_MenuWidget::WriteDocument(DbRecordSet &lstDocs,DbRecordSet &lstRevisions,bool bOpenDoc)
{

	//if file size is greateer then allowed
	if(!DocumentHelper::TestDocumentRevisionsForMaximumSize(lstRevisions))
		return false;

	if (lstDocs.getRowCount()==0)
		return false;

	QString strFile=lstDocs.getDataRef(0,"BDMD_DOC_PATH").toString();
	int nApplication=lstDocs.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
	int nDocType=lstDocs.getDataRef(0,"BDMD_DOC_TYPE").toInt();


	//get contacts:
	emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT); //give me new data, if some1 is listening....

	//-------------------------------------------------------
	//Write
	//------------------------------------------------------
	DbRecordSet empty;
	Status err;
	DbRecordSet lstUAR,lstGAR;
	_SERVER_CALL(BusDocuments->Write(err,lstDocs,m_lstProjects,m_lstContacts,empty,lstRevisions,"",lstUAR,lstGAR))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return false;
		}



		//-------------------------------------------------------
		//Send signal for all to reload:
		//------------------------------------------------------
		QVariant varData;
		qVariantSetValue(varData,lstDocs);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ENTITY_BUS_DM_DOCUMENTS,varData,this); 


		//-------------------------------------------------------
		//Open document:
		//-------------------------------------------------------
		if (bOpenDoc)
		{

			//get back id:
			int nRecordID=lstDocs.getDataRef(0,0).toInt();

			if (nDocType==GlobalConstants::DOC_TYPE_URL || nDocType==GlobalConstants::DOC_TYPE_NOTE) //if url or note do not open app, just go edit
			{
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EDIT, nRecordID);
				return true;
			}


			//open FUI
			int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_READ, nRecordID);


			if (nApplication<=0) return true;

			QString strAppPath;
			int nRow=m_lstApplications.find(0,nApplication,true);
			if (nRow==-1) return true;

			//uset advanced methoD:
			return DocumentHelper::OpenDocumentInExternalApp(lstDocs);

		}
		else
			return true;


}


//load from server, rebuild tree
void App_MenuWidget::LoadTemplates(bool bReload)
{
	if (m_nLoggedID==0) return;
	DocumentHelper::GetDocTemplates(m_lstTemplates,bReload);
}
