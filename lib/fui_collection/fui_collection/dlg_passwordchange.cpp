#include "dlg_passwordchange.h"
#include "common/common/authenticator.h"
#include <QtWidgets/QMessageBox>

//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager			*g_pBoSet;					//global access to Bo services
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "gui_core/gui_core/thememanager.h"
		


Dlg_PasswordChange::Dlg_PasswordChange(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	
	setWindowTitle(tr("Change Password"));//set dialog title

	m_bPassWordChanged=false;
	//ui.txtUserName->setEnabled(false);
	QString strUserName=g_pClientManager->GetLoggedUserName();
	ui.txtUserName->setText(strUserName);
	LoadPassword();
	ui.txtOldPassword->setFocus();
	ui.checkBox->setChecked(g_pClientManager->GetIniFile()->m_nHideLogin);

	//issue 1702:
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

Dlg_PasswordChange::~Dlg_PasswordChange()
{

}

void Dlg_PasswordChange::SetUser(QString strUserName)
{
	ui.txtUserName->setText(strUserName);
}

//load dummy password:
void Dlg_PasswordChange::LoadPassword()
{
	ui.txtPassword1->setText("xxxyyyzzz");
	ui.txtPassword2->setText("xxxyyyzzz");
}

//load dummy password:
void Dlg_PasswordChange::ClearPassword()
{
	ui.txtPassword1->clear();
	ui.txtPassword2->clear();
}

void Dlg_PasswordChange::on_txtPassword2_editingFinished()
{
	//set dirty flag only if different from DB:
	if(ui.txtPassword2->text()!="xxxyyyzzz")
		m_bPassWordChanged=true;
}

void Dlg_PasswordChange::on_txtPassword1_editingFinished()
{
	//set dirty flag only if different from DB:
	if(ui.txtPassword1->text()!="xxxyyyzzz")
		m_bPassWordChanged=true;
}


//before writing check if password is ok:
bool Dlg_PasswordChange::CheckPassword(QByteArray &newPassword)
{
	if (ui.txtPassword2->text()!=ui.txtPassword1->text())
	{
		QMessageBox::warning(this,tr("Error"),tr("Password not match!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return false;
	}
	else
	{
		if(m_bPassWordChanged) 			//hash password if changed:
		{
			QString strUserName=ui.txtUserName->text();
			//B.T. 2013.02.05: skip hash, use base64, and server will decide what to do!!
			newPassword = ui.txtPassword2->text().toLatin1().toBase64();
			//newPassword=Authenticator::GeneratePassword(strUserName,ui.txtPassword2->text());
		}
		return true;
	}
}


void Dlg_PasswordChange::on_btnCancel_clicked()
{

	done(0);


}

void Dlg_PasswordChange::on_btnOK_clicked()
{
	//if password not changed & user is, set password:
	if (!m_bPassWordChanged && g_pClientManager->GetLoggedUserName()!=ui.txtUserName->text()) 
	{
		QMessageBox::warning(this,tr("Warning"),tr("You must set password when changing username!"));
		return;
	}


	//save:
	QByteArray newPass;
	if(CheckPassword(newPass))
	{
		//set or clear flag:
		if (ui.txtOldPassword->text()=="" && ui.checkBox->isChecked())
			g_pClientManager->GetIniFile()->m_nHideLogin=1;
		else
			g_pClientManager->GetIniFile()->m_nHideLogin=0;
	

		if (!m_bPassWordChanged) //if not changed, ass he presses cancel
		{
			done(0);
			return;
		}

		//save:
		Authenticator Session;
		Session.SetSessionParameters(g_pClientManager->GetLoggedUserName(),ui.txtOldPassword->text(),"", "");	//cache login params
		QByteArray clientNonce,clientAuthToken;
		Session.GenerateAuthenticationToken(clientNonce,clientAuthToken);//create cnonce

		Status err;
		_SERVER_CALL(UserLogon->ChangePassword(err,g_pClientManager->GetLoggedUserName(),clientAuthToken,clientNonce,newPass,ui.txtUserName->text()))
		_CHK_ERR_NO_RET(err);
		if (err.IsOK())
		{
			if (g_pClientManager->GetIniFile()->m_nHideLogin)
				QMessageBox::information(this,tr("Re-login"),tr("Password successfully changed, please restart application. Hide login dialog option will be effective on next application start."));
			else
				QMessageBox::information(this,tr("Re-login"),tr("Password successfully changed, please re-login"));
			done(1);
		}
	}

}