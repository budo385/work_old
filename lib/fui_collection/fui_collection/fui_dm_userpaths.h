#ifndef FUI_DM_USERPATHS_H
#define FUI_DM_USERPATHS_H


#include "generatedfiles/ui_fui_dm_userpaths.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"


class FUI_DM_UserPaths : public FuiBase
{
	Q_OBJECT

public:
	FUI_DM_UserPaths(QWidget *parent = 0);
	~FUI_DM_UserPaths();


	QString GetFUIName();
	void DataWrite(Status &err);
	void DataDelete(Status &err);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

private slots:
	void OnSelectPath();
	void OnRemovePath();
	void OnTableSelectionChanged();
	void OnOwnerChanged(); 
	void OnDelete();

private:
	void RefreshDisplay();
	void SetMode(int nMode);

	GuiFieldManager *m_GuiFieldManagerMain;
	DbRecordSet m_lstUserPaths;

	Ui::FUI_DM_UserPathsClass ui;
};

#endif // FUI_DM_USERPATHS_H
