#ifndef COMMGRIDFILTER_H
#define COMMGRIDFILTER_H

#include <QWidget>
#include <QCheckBox>
#include <QSpinBox>
#include "ui_commgridfilter.h"

#include "fuibase.h"
#include "common/common/observer_ptrn.h"
#include "common/common/dbrecordset.h"

class LastColumnSpinBox : public QSpinBox
{
	Q_OBJECT

public:
	LastColumnSpinBox(int nSettingID, QWidget *parent = 0);
	~LastColumnSpinBox(){};

private:
	int m_nSettingID;

signals:
	void SpinBoxValueChanged(int nSettingID, int nValue);

private slots:
	//void on_ValueChanged();
	void on_editingFinished();
};

class CommGridFilterCheckBox : public QCheckBox
{
	Q_OBJECT

public:
	CommGridFilterCheckBox(int nSettingID, QWidget *parent = 0);
	~CommGridFilterCheckBox(){};

private:
	int m_nSettingID;

signals:
	void ChBoxStateChanged(int nSettingID, int nState);

private slots:
	void on_stateChanged(int nState);
};

class CommGridFilter : public FuiBase
{
	Q_OBJECT

public:
	CommGridFilter(int nFilterType, QWidget *parent = 0);
	CommGridFilter(int nFilterType, bool bLoadFromWizard, QWidget *parent = 0);
	~CommGridFilter();

	void					SetCurrentView(int nViewID, DbRecordSet recFilterViewData, int nPersonID, bool bLoadViewSelector = true);
	QString					GetFUIName();

	//----------------------------------------
	// BUTTON BAR SUPPORT
	//----------------------------------------
	void 					on_cmdInsert();
	bool 					on_cmdEdit();
	bool 					on_cmdDelete();
	void 					on_cmdCopy();
	bool 					on_cmdOK();
	void 					on_cmdCancel();
	void					SetMode(int nMode);
	void 					SetDefaultValues();
	void 					LoadViewSelector(int nFilterType = -1, int nPersonID = -1);
	void					GetViewAndfilterData(int &nViewID, DbRecordSet &recFilterData);

private:
	Ui::CommGridFilterClass ui;

	void					Initialize(int nFilterType, bool bLoadFromWizard, QWidget *parent);
	void 					SetupTableLayout();
	void 					SetTableItems();
	void 					SetColumnColor(int nColumn, QColor color);
	QWidget*				CreateCheckBox(int nSettingID, int nState, int nValue);
	CommGridFilterCheckBox* CreateLastCheckBox(int nSettingID, int nState);
	LastColumnSpinBox*		CreateLastSpinBox(int nSettingID, int nValue);
	QWidget*				CreateLastColumnWidget(CommGridFilterCheckBox *cBox, LastColumnSpinBox *sBox);
	void 					WriteToComplexFilterHeader();
	void 					GetFilterDataFromServer();
	void 					EnableDisableDeleteButton();
	void 					FillInsertDefaultValues(DbRecordSet &recFilterSettings);
	void 					GetFilterDataFromGui();
	bool					GetViewDataFromGui();
	void					SetViewDataToGui();
	void					SetFilterDataToGui();
	void 					FillAppTree();
	void 					ConnectSignals();
	void					ConnectValueChangedSignal();
	void					DisconnectValueChangedSignal();
	void					EnableViewChBoxes(bool bEnable);
	void					SetFilterIntValue(int nValue, int nFilterSetting);
	void					SetFilterStringValue(QString strValue, int nFilterSetting);
	void					SetFilterDateTimeValue(QDateTime datValue, int nFilterSetting);
	void					SetFilterRecordSetValue(DbRecordSet recValue, int nFilterSetting);
	bool					GetFilterBoolValue(DbRecordSet recFilterViewData, int nFilterSetting);
	int						GetFilterIntValue(DbRecordSet recFilterViewData, int nFilterSetting);
	QString					GetFilterStringValue(DbRecordSet recFilterViewData, int nFilterSetting);
	QDateTime				GetFilterDateTimeValue(DbRecordSet recFilterViewData, int nFilterSetting);
	DbRecordSet				GetFilterRecordSetValue(DbRecordSet recFilterViewData, int nFilterSetting);
	void					SetupCETypeFilter();
	void					SetupTaskTypeFilter();
	void					SelectTaskTypeTableItems(DbRecordSet &recStoredTypes);
	void					SetupCETypeFilterFromSavedData();
	void					SetupSortTab();
	int						CommEntityTypeFromComboIdx(int);

	int									m_nParentFuiID;
	int									m_nLoggedPersonID;
	int									m_nViewID;
	int									m_nGridType;
	bool								m_bLoadFromWizard;
	QHash<int, CommGridFilterCheckBox*> m_hshCheckBoxes;
	QHash<int, LastColumnSpinBox*>		m_hshSpinBoxes;
	DbRecordSet							m_recFilterViews;		//Filter views.
	DbRecordSet							m_recFilterViewData;	//Filter views saved settings - checked, which one is used, etc.
	DbRecordSet							m_recNewView;			//This is new data recordset.
	DbRecordSet							m_recFilterData;		//Check boxes values.
	DbRecordSet							m_lstSortColumnSetup;
	DbRecordSet							m_lstSortRecordSet;

private slots:
	void 					on_DateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange);
	void 					on_viewName_comboBox_currentIndexChanged(int index);
	void 					on_spinBox_editingFinished(int nSetting, int nValue);
	void 					on_CheckBoxStateChanged(int nSettingID, int nValue);	//For connecting filter grid check boxes.
	void 					on_ViewSelection_groupBox_toggled(bool bChecked);
	void					on_documentType_groupBox_toggled(bool bChecked);
	void 					on_DateTimeChanged(const QDateTime &datetime);
	void 					on_spinBox_editingFinished();
	void 					on_groupBoxToggled(bool bOn);
	void 					on_normalCheckBox_stateChanged(int state);				//For connecting other check boxes except the ones in filter grid.
	void 					on_FilterValueChanged();
	void					on_application_groupBox_toggled(bool bChecked);
	void					on_treeWidgetApp_itemChanged(QTreeWidgetItem *item, int column);
	void					on_searchTextEdit_textChanged();
	void 					HideAllFirstRow(bool bChecked);
	void 					HideAllSecondRow(bool bChecked);
	void 					HideAllThirdRow(bool bChecked);
	void					on_FilterByType_groupBox_toggled(bool bChecked);
	void					on_TypeFilter_comboBox_currentIndexChanged(int nIndex);
	void					on_TypeFilter_frame_ItemCheckStateChanged(QList<QTreeWidgetItem*> lstChangedCheckStateItems);
	void					on_cboPriority_From_currentIndexChanged(const QString& text);
	void					on_cboPriority_To_currentIndexChanged(const QString &text);
	void					on_FilterByTaskType_groupBox_clicked(bool checked);
	void					on_TaskType_tableWidget_SelectionChanged();

signals:
	void FilterValueChanged();
	void SetFilter(int nViewID, DbRecordSet recNewView, DbRecordSet recFilterViewData);
};

#endif // COMMGRIDFILTER_H
