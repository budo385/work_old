#ifndef SETTINGS_Email_H
#define SETTINGS_Email_H

#include <QWidget>
#include "ui_settings_email.h"
#include "settingsbase.h"

class Settings_Email : public SettingsBase
{
    Q_OBJECT

public:
    Settings_Email(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
    ~Settings_Email();

	DbRecordSet GetChangedValuesRecordSet();

private:
	bool m_bScanDateChanged;
    Ui::Settings_EmailClass ui;
	QList<QStringList> m_lstEmailFoldersScan;

private slots:
	void on_chkUseAccount_toggled(bool checked);
	//void on_btnTestConnection_clicked();
};

#endif // SETTINGS_Email_H
