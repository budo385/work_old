#ifndef QC_ADDRESSFRAME_H
#define QC_ADDRESSFRAME_H

#include <QWidget>
#include "ui_qc_addressframe.h"
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/selection_combobox.h"

class QC_AddressFrame : public QWidget
{
	Q_OBJECT

public:
	QC_AddressFrame(QWidget *parent = 0);
	~QC_AddressFrame();

	void Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType = 0 /*0-Large,1-small widget*/);
	void AddAddress(DbRecordSet recAddress);
	void SetAddress(DbRecordSet recAddress);
	void SetLayout();
	void SetFirstName(QString strValue);
	void SetMiddleName(QString strValue);
	void SetLastName(QString strValue);
	void SetOrganization(QString strValue);
	void SetTown(QString strValue);
	void UpdateSalutation(QString strRecordSetFieldName, QString strValue);

signals:
	void TownChanged(QString strTown);
	

private:
	Ui::QC_AddressFrameClass ui;

	void SetupGUIwidgets(DbRecordSet recData);
	void SetDataToRecordSet(QString strRecordSetFieldName, QString strValue);
	void SetIntDataToRecordSet(QString strRecordSetFieldName, int nValue);
	void FormatAddressTextEdit(bool bForce = false);


	DbRecordSet *m_recFullContactData;
	DbRecordSet m_recLocalEntityRecordSet;
	int			m_nCurrentRow;
	int			m_nParentType;
	QWidget		*m_pParentWidget;
	bool		m_bLayoutSet;
	Selection_ComboBox m_CountryHandler;

public slots:
	void on_addressformat_toolButton_clicked();

private slots:
	void on_RowChanged(int nRow);

	void on_title_lineEdit_textChanged(const QString & text);
	void on_firstname_lineEdit_textChanged(const QString &text);
	void on_middlename_lineEdit_textChanged(const QString &text);
	void on_lastname_lineEdit_textChanged(const QString &text);
	void on_organizationname_lineEdit_textChanged(const QString &text);
	void on_organizationname_lineEdit_2_textChanged(const QString &text);
	void on_street_lineEdit_textChanged(const QString &text);
	void on_pobox_lineEdit_textChanged(const QString &text);
	void on_pozip_lineEdit_textChanged(const QString &text);
	void on_zip_lineEdit_textChanged(const QString &text);
	void on_town_lineEdit_textChanged(const QString &text);
	void on_town_lineEdit_editingFinished(){emit TownChanged(ui.town_lineEdit->text());}
	void on_state_lineEdit_textChanged(const QString &text);
	void on_countrycode_lineEdit_textChanged(const QString &text);
	void on_cmbCountry_editTextChanged(const QString &text);
	void on_formatedaddress_textEdit_textChanged();
	void on_simple_radioButton_toggled(bool checked);
	void on_lock_checkBox_stateChanged(int state);
	
	void on_lookupZIP_clicked();
	void OnTypeChanged(int,int);

	//Add selected part.
	void on_addSelected_toolButton_4_clicked();
	void on_addSelected_toolButton_clicked();
	void on_addSelected_toolButton_2_clicked();
	void on_addSelected_toolButton_3_clicked();
	void on_addSelected_toolButton_5_clicked();
	void on_addSelected_toolButton_6_clicked();
	void on_addSelected_toolButton_7_clicked();
	void on_addSelected_toolButton_9_clicked();
	void on_addSelected_toolButton_8_clicked();
	void on_addSelected_toolButton_10_clicked();
	void on_addSelected_toolButton_11_clicked();
	void on_addSelected_toolButton_12_clicked();
	void on_addSelected_toolButton_13_clicked();
	void on_addSelected_toolButton_14_clicked();
	void on_addSelected_toolButton_15_clicked();
	void on_isDefault_checkBox_stateChanged(int state);
};

#endif // QC_ADDRESSFRAME_H
