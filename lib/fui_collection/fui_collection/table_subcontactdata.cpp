#include "table_subcontactdata.h"
#include <QTextEdit>
#include <QListWidget>

Table_SubContactData::Table_SubContactData(QWidget *parent)
	: UniversalTableWidgetEx(parent)
{

}

Table_SubContactData::~Table_SubContactData()
{

}

//create multiline and pic:
void Table_SubContactData::Data2CustomWidget(int nRow, int nCol)
{
	int nColType=m_lstColumnSetup.getDataRef(nCol,"BOGW_COLUMN_TYPE").toInt();

	if (nColType==COL_TYPE_MULTILINE_TEXT)
	{
		QTextEdit *pText = dynamic_cast<QTextEdit *>(cellWidget(nRow,nCol));
		if (!pText)
		{
			pText = new QTextEdit;
			pText->setReadOnly(true);
			setCellWidget(nRow,nCol,pText);
		}
		pText->setHtml(m_plstData->getDataRef(nRow,m_lstColumnMapping[nCol]).toString());
	}
	else if (nColType==COL_TYPE_LIST)
	{
		QListWidget *pLst = dynamic_cast<QListWidget *>(cellWidget(nRow,nCol));
		if (!pLst)
		{
			pLst = new QListWidget;
			pLst->setProperty("row",nRow); //to identify sender
			pLst->setProperty("col",nCol); //to identify sender
			pLst->setSelectionMode(QAbstractItemView::ExtendedSelection);
			connect(pLst,SIGNAL(itemSelectionChanged()),this,SLOT(OnListSelectionChanged()));
			setCellWidget(nRow,nCol,pLst);
		}

		//SET DATA in list from sublist and datasource:
		DbRecordSet lstData=m_plstData->getDataRef(nRow,m_lstColumnMapping[nCol]).value<DbRecordSet>();
		QString strDataSourceFormat=m_lstColumnSetup.getDataRef(nCol,"BOGW_DATASOURCE").toString();
		pLst->blockSignals(true);
		pLst->clear();

		//get list :format: "COLUMN_1<<'Separator'<<COLUMN_2<<COLUMN_3 ... where Separator is custom defined text printed as is
		QStringList lstDataSource=strDataSourceFormat.split("<<");
		int nSize =lstData.getRowCount();
		QString strItem;

		//add item into combo:
		for(int nRow=0;nRow<nSize;++nRow)
		{
			strItem="";
			for(int i=0;i<lstDataSource.size();++i)
			{
				if (lstDataSource.at(i).indexOf("'")!=-1) //if found replace '
				{
					QString strSeparator=lstDataSource.at(i);
					if (!strItem.isEmpty()) //add separator only if not empty
						strItem+=strSeparator.replace("'","");
				}
				else
				{
					strItem+=lstData.getDataRef(nRow,lstDataSource.at(i)).toString();
				}
			}
			pLst->insertItem (nRow,strItem);
			pLst->setItemSelected(pLst->item(nRow),lstData.isRowSelected(nRow)); //set selection
		}
		pLst->blockSignals(false);
	}
}

//store selection from QList widget into sublist inside datasource
void Table_SubContactData::OnListSelectionChanged()
{
	QListWidget *pLst = dynamic_cast<QListWidget *>(sender());
	if (pLst)
	{
		int nRow=pLst->property("row").toInt();
		int nCol=pLst->property("col").toInt();
		Q_ASSERT(nRow>=0 && nRow <m_plstData->getRowCount()); //row must be inside data

		DbRecordSet lstData=m_plstData->getDataRef(nRow,m_lstColumnMapping[nCol]).value<DbRecordSet>();
		int nSize =lstData.getRowCount();
		for(int i=0;i<nSize;++i)
			lstData.selectRow(nRow,pLst->isItemSelected(pLst->item(i)));

		m_plstData->setData(nRow,m_lstColumnMapping[nCol],lstData);
	}
}