#include "dlg_phoneformatter.h"
#include "bus_core/bus_core/countries.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/thememanager.h"


Dlg_PhoneFormatter::Dlg_PhoneFormatter(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	m_CountryHandler.Initialize(ENTITY_COUNTRIES);
	m_CountryHandler.SetDataForCombo(ui.cmbCountry,"COUNTRY");
	m_CountryHandler.RefreshDisplay();
	ui.txtPhone->setReadOnly(true);
	connect(ui.cmbCountry,SIGNAL(currentIndexChanged(int)),this,SLOT(OnPhoneChange()));
	connect(ui.txtArea,SIGNAL(editingFinished()),this,SLOT(OnPhoneChange()));
	connect(ui.txtLocal,SIGNAL(editingFinished()),this,SLOT(OnPhoneChange()));

	//issue 1702:
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}


Dlg_PhoneFormatter::~Dlg_PhoneFormatter()
{

}


void Dlg_PhoneFormatter::SetRow(QString strPhone,QString strCountryISO)
{
	QString strArea,strCountry,strSearch,strISO,strLocal;
	m_FormatPhone.SplitPhoneNumber(strPhone,strCountry,strArea,strLocal,strSearch,strISO);
	strPhone=m_FormatPhone.FormatPhoneNumber(strCountry,strArea,strLocal);

	ui.txtPhone->setText(strPhone);
	ui.txtArea->setText(strArea);
	ui.txtLocal->setText(strLocal);

	//because predial can be same for more countries, use given ISO to set country
	if (!strCountryISO.isEmpty())
	{
		int nRow=m_CountryHandler.GetDataSource()->find(2,strCountryISO,true);
		if (nRow!=-1)
		{
			ui.cmbCountry->blockSignals(true);
			ui.cmbCountry->setCurrentIndex(nRow);
			ui.cmbCountry->blockSignals(false);
		}
	}
	else if (!strISO.isEmpty())
	{
	}
	{
		int nRow=m_CountryHandler.GetDataSource()->find(2,strISO,true);
		if (nRow!=-1)
		{
			ui.cmbCountry->blockSignals(true);
			ui.cmbCountry->setCurrentIndex(nRow);
			ui.cmbCountry->blockSignals(false);
		}
	}
	
}

void Dlg_PhoneFormatter::OnPhoneChange()
{
	QString strPhone=m_FormatPhone.FormatPhoneNumber(m_CountryHandler.GetDataSource()->getDataRef(ui.cmbCountry->currentIndex(),1).toString(),ui.txtArea->text(),ui.txtLocal->text());
	ui.txtPhone->setText(strPhone);
}



void Dlg_PhoneFormatter::GetRow(QString &strPhone, QString &strCountry,QString &strArea,QString &strLocal,QString &strSearch,QString &strCountryISO)
{
	strPhone=ui.txtPhone->text();
	strArea=ui.txtArea->text();
	strLocal=ui.txtLocal->text();
	strCountry=m_CountryHandler.GetDataSource()->getDataRef(ui.cmbCountry->currentIndex(),1).toString();
	strCountryISO=m_CountryHandler.GetDataSource()->getDataRef(ui.cmbCountry->currentIndex(),2).toString();
	strSearch=m_FormatPhone.GenerateSearchNumber(strCountry,strArea,strLocal);
}



//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void Dlg_PhoneFormatter::on_btnOK_clicked()
{

	done(1);
}


void Dlg_PhoneFormatter::on_btnCancel_clicked()
{
	done(0);
}
