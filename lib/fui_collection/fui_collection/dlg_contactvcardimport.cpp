#include "dlg_contactvcardimport.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "fui_collection/fui_collection/dlg_phoneformatter.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "common/common/entity_id_collection.h"
#include "fui_importcontact.h"
#include "macros.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;


#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;




Dlg_ContactVcardImport::Dlg_ContactVcardImport(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle("Read Data From VCard");

	ui.wxAddress->HideButtonBar();
	ui.wxAddress->show();

	m_lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE_SELECT));
	m_lstAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT));

	m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; font-style:italic;\">";
	m_strEndHtmlTag="</span></p></body></html>";

	//types:
	m_Types.Initialize(ENTITY_BUS_CM_TYPES);
	m_Types.ReloadData();

	SetMenuPhoneTypes(&m_MenuPhone_1);
	SetMenuPhoneTypes(&m_MenuPhone_2);
	SetMenuPhoneTypes(&m_MenuPhone_3);
	SetMenuPhoneTypes(&m_MenuPhone_4);

	ui.btnPhone1->setMenu(&m_MenuPhone_1);
	ui.btnPhone2->setMenu(&m_MenuPhone_2);
	ui.btnPhone3->setMenu(&m_MenuPhone_3);
	ui.btnPhone4->setMenu(&m_MenuPhone_4);

	connect(&m_MenuPhone_1,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerPhone1(QAction *)));
	connect(&m_MenuPhone_2,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerPhone2(QAction *)));
	connect(&m_MenuPhone_3,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerPhone3(QAction *)));
	connect(&m_MenuPhone_4,SIGNAL(triggered(QAction *)),this,SLOT(OnMenuTriggerPhone4(QAction *)));

	connect(ui.btnOK,SIGNAL(clicked(bool)),this,SLOT(on_btnOK_clicked(bool)));
	connect(ui.btnCancel,SIGNAL(clicked(bool)),this,SLOT(on_btnCancel_clicked(bool)));

	ui.btnFormatPhone1->setIcon(QIcon(":cookies.png"));
	ui.btnFormatPhone2->setIcon(QIcon(":cookies.png"));
	ui.btnFormatPhone3->setIcon(QIcon(":cookies.png"));
	ui.btnFormatPhone4->setIcon(QIcon(":cookies.png"));
	ui.btnFormatPhone1->setToolTip(tr("Format Phone"));
	ui.btnFormatPhone2->setToolTip(tr("Format Phone"));
	ui.btnFormatPhone3->setToolTip(tr("Format Phone"));
	ui.btnFormatPhone4->setToolTip(tr("Format Phone"));

	//issue 1346: use Business Direct, Private and Fax
	ui.btnPhone1->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS).toInt()));
	ui.btnPhone2->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE).toInt()));
	ui.btnPhone3->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX).toInt()));
	ui.btnPhone4->setText(ClientContactManager::GetTypeName(ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE).toInt()));

	//connect lineedit:
	ui.ckb1->setChecked(false);
	on_ckb1_stateChanged(Qt::Unchecked);
	ui.ckb2->setChecked(false);
	on_ckb2_stateChanged(Qt::Unchecked);
	ui.ckb3->setChecked(false);
	on_ckb3_stateChanged(Qt::Unchecked);
	ui.ckb4->setChecked(false);
	on_ckb4_stateChanged(Qt::Unchecked);

	ui.rb_New->setChecked(true);
	on_rb_New_clicked(true);

}

Dlg_ContactVcardImport::~Dlg_ContactVcardImport()
{

}


void Dlg_ContactVcardImport::SetData(int nActualContactID, QString strActualContactName, DbRecordSet lstVcardDataForImport)
{
	m_nActualContactID=nActualContactID;
	m_lstVcardDataForImport=lstVcardDataForImport;

	//set phones: fill list of phones with data:
	int nPhonePos=0;
	QString strPhone = lstVcardDataForImport.getDataRef(0,"CLC_PHONE_BUSINESS_CENTRAL").toString();
	if (!strPhone.isEmpty())
	{
		AddPhone(nPhonePos,strPhone,ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS).toInt());
		nPhonePos++;
	}
	strPhone = lstVcardDataForImport.getDataRef(0,"CLC_PHONE_BUSINESS_DIRECT").toString();
	if (!strPhone.isEmpty())
	{
		AddPhone(nPhonePos,strPhone,ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS).toInt());
		nPhonePos++;
	}
	strPhone = lstVcardDataForImport.getDataRef(0,"CLC_PHONE_MOBILE").toString();
	if (!strPhone.isEmpty())
	{
		AddPhone(nPhonePos,strPhone,ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE).toInt());
		nPhonePos++;
	}
	strPhone = lstVcardDataForImport.getDataRef(0,"CLC_PHONE_FAX").toString();
	if (!strPhone.isEmpty())
	{
		AddPhone(nPhonePos,strPhone,ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX).toInt());
		nPhonePos++;
	}
	strPhone = lstVcardDataForImport.getDataRef(0,"CLC_PHONE_PRIVATE").toString();
	if (!strPhone.isEmpty())
	{
		AddPhone(nPhonePos,strPhone,ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE).toInt());
		nPhonePos++;
	}
	strPhone = lstVcardDataForImport.getDataRef(0,"CLC_PHONE_PRIVATE_MOBILE").toString();
	if (!strPhone.isEmpty())
	{
		AddPhone(nPhonePos,strPhone,ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE).toInt());
		nPhonePos++;
	}
	strPhone = lstVcardDataForImport.getDataRef(0,"CLC_PHONE_SKYPE").toString();
	if (!strPhone.isEmpty())
	{
		AddPhone(nPhonePos,strPhone,ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE).toInt());
		nPhonePos++;
	}
	strPhone = lstVcardDataForImport.getDataRef(0,"CLC_PHONE_GENERIC").toString();
	if (!strPhone.isEmpty())
	{
		AddPhone(nPhonePos,strPhone,ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt());
		nPhonePos++;
	}

	//add extra phone rows at end: when ok: ignore all unchecked and empty...
	if (nPhonePos<4)
	{
		int nRowsToAdd=4-m_lstPhones.getRowCount();
		m_lstPhones.addRow(nRowsToAdd);
	}

	//set address
	m_lstAddress.merge(lstVcardDataForImport);

	//fill at least first name, last name, middle name and org:
	if (m_lstAddress.getDataRef(0,"BCMA_FIRSTNAME").toString().isEmpty())
		m_lstAddress.setData(0,"BCMA_FIRSTNAME",lstVcardDataForImport.getDataRef(0,"BCNT_FIRSTNAME"));

	if (m_lstAddress.getDataRef(0,"BCMA_LASTNAME").toString().isEmpty())
		m_lstAddress.setData(0,"BCMA_LASTNAME",lstVcardDataForImport.getDataRef(0,"BCNT_LASTNAME"));

	if (m_lstAddress.getDataRef(0,"BCMA_MIDDLENAME").toString().isEmpty())
		m_lstAddress.setData(0,"BCMA_MIDDLENAME",lstVcardDataForImport.getDataRef(0,"BCNT_MIDDLENAME"));

	if (m_lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME").toString().isEmpty())
		m_lstAddress.setData(0,"BCMA_ORGANIZATIONNAME",lstVcardDataForImport.getDataRef(0,"BCNT_ORGANIZATIONNAME"));

	//try to format address:
	if (m_lstAddress.getDataRef(0,"BCMA_FORMATEDADDRESS").toString().isEmpty())
	{
		m_FormatAddress.AddressFormat(m_lstAddress,m_FormatAddress.m_strDefaultSchema,true);
	}


	ui.wxAddress->SetRow(m_lstAddress,true,false);

	ui.rb_New->setChecked(true);
	if (m_nActualContactID<=0)
	{
		ui.rb_Add->setDisabled(true);
		strActualContactName = tr("To add data to existing contact, please select contact or exit edit/insert mode");
		ui.labelContactName->setText(m_strHtmlTag+strActualContactName+m_strEndHtmlTag);
		ui.labelContactName->setToolTip(ui.labelContactName->text());
	}


}


void Dlg_ContactVcardImport::SetMenuPhoneTypes(QMenu *menu)
{
	DbRecordSet lstTypes=*m_Types.GetDataSource();

	//_DUMP(lstTypes)

	//select mails:	
	lstTypes.find(lstTypes.getColumnIdx("BCMT_ENTITY_TYPE"),QVariant(ContactTypeManager::TYPE_PHONE).toInt());
	lstTypes.deleteUnSelectedRows();

	int nSize=lstTypes.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		menu->addAction(lstTypes.getDataRef(i,"BCMT_TYPE_NAME").toString());
	}
}

void Dlg_ContactVcardImport::FormatPhoneNumber(int nRow)
{
	QString strPhone,strArea,strCountry,strSearch,strISO,strLocal;
	Dlg_PhoneFormatter dlg;
	int m_nPhoneColIdx=m_lstPhones.getColumnIdx("BCMP_FULLNUMBER");
	dlg.SetRow(m_lstPhones.getDataRef(nRow,m_nPhoneColIdx).toString(),"");
	int nRes=dlg.exec();
	if (nRes)
	{
		dlg.GetRow(strPhone,strCountry,strArea,strLocal,strSearch,strISO);
		m_lstPhones.setData(nRow,m_nPhoneColIdx,strPhone);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_AREA"),strArea);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_COUNTRY"),strCountry);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_LOCAL"),strLocal);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_SEARCH"),strSearch);
		m_lstPhones.setData(nRow,m_lstPhones.getColumnIdx("BCMP_COUNTRY_ISO"),strISO);
	}

}

void Dlg_ContactVcardImport::on_btnFormatPhone1_clicked()
{
	m_lstPhones.setData(0,"BCMP_FULLNUMBER",ui.txtPhone1->text());
	FormatPhoneNumber(0);
	ui.txtPhone1->setText(m_lstPhones.getDataRef(0,"BCMP_FULLNUMBER").toString());
}

void Dlg_ContactVcardImport::on_btnFormatPhone2_clicked()
{
	m_lstPhones.setData(1,"BCMP_FULLNUMBER",ui.txtPhone2->text());
	FormatPhoneNumber(1);
	ui.txtPhone2->setText(m_lstPhones.getDataRef(1,"BCMP_FULLNUMBER").toString());
}
void Dlg_ContactVcardImport::on_btnFormatPhone3_clicked()
{
	m_lstPhones.setData(2,"BCMP_FULLNUMBER",ui.txtPhone3->text());
	FormatPhoneNumber(2);
	ui.txtPhone3->setText(m_lstPhones.getDataRef(2,"BCMP_FULLNUMBER").toString());
}


void Dlg_ContactVcardImport::on_btnFormatPhone4_clicked()
{
	m_lstPhones.setData(3,"BCMP_FULLNUMBER",ui.txtPhone4->text());
	FormatPhoneNumber(3);
	ui.txtPhone4->setText(m_lstPhones.getDataRef(3,"BCMP_FULLNUMBER").toString());
}


void Dlg_ContactVcardImport::AddPhone(int nPos,QString strPhone, int nTypeID)
{
	if (nPos>3) return;

	//m_lstPhones.Dump();
	m_lstPhones.addRow();
	int nRow=m_lstPhones.getRowCount()-1;

	QString strArea,strCountry,strSearch,strISO,strLocal;

	//B.T. - reformat phone number
	FormatPhone m_FormatPhone;
	m_FormatPhone.SplitPhoneNumber(strPhone,strCountry,strArea,strLocal,strSearch,strISO);
	m_lstPhones.setData(nRow,"BCMP_AREA",strArea);
	m_lstPhones.setData(nRow,"BCMP_COUNTRY",strCountry);
	m_lstPhones.setData(nRow,"BCMP_LOCAL",strLocal);
	m_lstPhones.setData(nRow,"BCMP_SEARCH",strSearch);
	m_lstPhones.setData(nRow,"BCMP_COUNTRY_ISO",strISO);
	strPhone=m_FormatPhone.FormatPhoneNumber(strCountry,strArea,strLocal);

	//add new record
	m_lstPhones.setData(nRow,"BCMP_FULLNUMBER",strPhone); //type=skype/TOFIX later for other interfaces:
	m_lstPhones.setData(nRow,"BCMP_TYPE_ID",nTypeID);

	switch(nPos)
	{
	case 0:
		{
			ui.txtPhone1->setText(m_lstPhones.getDataRef(0,"BCMP_FULLNUMBER").toString());
			ui.btnPhone1->setText(ClientContactManager::GetTypeName(nTypeID));
			ui.ckb1->setChecked(true);
			break;
		}
	case 1:
		{
			ui.txtPhone2->setText(m_lstPhones.getDataRef(1,"BCMP_FULLNUMBER").toString());
			ui.btnPhone2->setText(ClientContactManager::GetTypeName(nTypeID));
			ui.ckb2->setChecked(true);
			break;
		}
	case 2:
		{
			ui.txtPhone3->setText(m_lstPhones.getDataRef(2,"BCMP_FULLNUMBER").toString());
			ui.btnPhone3->setText(ClientContactManager::GetTypeName(nTypeID));
			ui.ckb3->setChecked(true);
			break;
		}
	case 3:
		{
			ui.txtPhone4->setText(m_lstPhones.getDataRef(3,"BCMP_FULLNUMBER").toString());
			ui.btnPhone4->setText(ClientContactManager::GetTypeName(nTypeID));
			ui.ckb4->setChecked(true);
			break;
		}
	}


}

void Dlg_ContactVcardImport::on_rb_New_clicked(bool bOn)
{
	ui.groupPhones->setEnabled(false);
	ui.groupAddr->setEnabled(false);
}

void Dlg_ContactVcardImport::on_rb_Add_clicked(bool bOn)
{
	ui.groupPhones->setEnabled(true);
	ui.groupAddr->setEnabled(true);
}

void Dlg_ContactVcardImport::on_ckb1_stateChanged(int nState)
{
	if (nState==Qt::Checked)
	{
		ui.txtPhone1->setEnabled(true);
		ui.btnPhone1->setEnabled(true);
		ui.btnFormatPhone1->setEnabled(true);
	}
	else
	{
		ui.txtPhone1->setEnabled(false);
		ui.btnPhone1->setEnabled(false);
		ui.btnFormatPhone1->setEnabled(false);
	}

}

void Dlg_ContactVcardImport::on_ckb2_stateChanged(int nState)
{
	if (nState==Qt::Checked)
	{
		ui.txtPhone2->setEnabled(true);
		ui.btnPhone2->setEnabled(true);
		ui.btnFormatPhone2->setEnabled(true);
	}
	else
	{
		ui.txtPhone2->setEnabled(false);
		ui.btnPhone2->setEnabled(false);
		ui.btnFormatPhone2->setEnabled(false);
	}

}
void Dlg_ContactVcardImport::on_ckb3_stateChanged(int nState)
{
	if (nState==Qt::Checked)
	{
		ui.txtPhone3->setEnabled(true);
		ui.btnPhone3->setEnabled(true);
		ui.btnFormatPhone3->setEnabled(true);
	}
	else
	{
		ui.txtPhone3->setEnabled(false);
		ui.btnPhone3->setEnabled(false);
		ui.btnFormatPhone3->setEnabled(false);
	}

}
void Dlg_ContactVcardImport::on_ckb4_stateChanged(int nState)
{
	if (nState==Qt::Checked)
	{
		ui.txtPhone4->setEnabled(true);
		ui.btnPhone4->setEnabled(true);
		ui.btnFormatPhone4->setEnabled(true);
	}
	else
	{
		ui.txtPhone4->setEnabled(false);
		ui.btnPhone4->setEnabled(false);
		ui.btnFormatPhone4->setEnabled(false);
	}

}
void Dlg_ContactVcardImport::on_groupAddr_clicked (bool bOn)
{
	ui.wxAddress->setEnabled(bOn);
}



void Dlg_ContactVcardImport::on_btnCancel_clicked(bool bP)
{
	done(0);
}

void Dlg_ContactVcardImport::on_btnOK_clicked(bool bP)
{
	if (ui.rb_New->isChecked())
	{
		int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
		FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
		if (pFui)
		{
			pFui->SetImportListFromSource(m_lstVcardDataForImport);
			pFui->show();  
		}
		close();
		return;
	}
	else
	{
		bool bSync;
		ui.wxAddress->GetRow(m_lstAddress,bSync);

		//add phones:
		//-----------------------------------------

		// Load list of phones from server:
		Status status;
		DbRecordSet lstPhones;
		_SERVER_CALL(BusContact->ReadContactPhones(status, m_nActualContactID, lstPhones));
		_CHK_ERR(status);

		//only those non empty and checked:
		m_lstPhones.clearSelection();
		if (ui.ckb1->isChecked() && !m_lstPhones.getDataRef(0,"BCMP_FULLNUMBER").toString().isEmpty())
			m_lstPhones.selectRow(0);
		if (ui.ckb2->isChecked() && !m_lstPhones.getDataRef(1,"BCMP_FULLNUMBER").toString().isEmpty())
			m_lstPhones.selectRow(1);
		if (ui.ckb3->isChecked() && !m_lstPhones.getDataRef(2,"BCMP_FULLNUMBER").toString().isEmpty())
			m_lstPhones.selectRow(2);
		if (ui.ckb4->isChecked() && !m_lstPhones.getDataRef(3,"BCMP_FULLNUMBER").toString().isEmpty())
			m_lstPhones.selectRow(3);
		m_lstPhones.deleteUnSelectedRows();


		if (m_lstPhones.getRowCount()>0)
		{
			lstPhones.merge(m_lstPhones);

			//save
			_SERVER_CALL(BusContact->WritePhones(status, m_nActualContactID, lstPhones));
			_CHK_ERR(status);
		}




		//add addresses:
		//-----------------------------------------
		if (ui.groupAddr->isChecked())
		{
			DbRecordSet lstIDs;
			lstIDs.addColumn(QVariant::Int,"BCNT_ID");
			lstIDs.addRow();
			lstIDs.setData(0,0,m_nActualContactID);
			DbRecordSet lstAddresses;
			_SERVER_CALL(BusContact->ReadContactAddress(status, lstIDs, lstAddresses));
			_CHK_ERR(status)

				bool bOK=false;
			ui.wxAddress->GetRow(m_lstAddress,bOK);

			lstAddresses.merge(m_lstAddress);

			if (lstAddresses.getRowCount()==1)
				lstAddresses.setData(0,"BCMA_IS_DEFAULT",1);

			//lstAddresses.Dump();

			_SERVER_CALL(BusContact->WriteAddress(status, m_nActualContactID, lstAddresses));
			_CHK_ERR(status)
		}
	}

	done(1);

}


void Dlg_ContactVcardImport::OnMenuTriggerPhone1(QAction *action)
{
	ui.btnPhone1->setText(action->text());

}
void Dlg_ContactVcardImport::OnMenuTriggerPhone2(QAction *action)
{
	ui.btnPhone2->setText(action->text());

}
void Dlg_ContactVcardImport::OnMenuTriggerPhone3(QAction *action)
{
	ui.btnPhone3->setText(action->text());
}
void Dlg_ContactVcardImport::OnMenuTriggerPhone4(QAction *action)
{
	ui.btnPhone4->setText(action->text());
}

