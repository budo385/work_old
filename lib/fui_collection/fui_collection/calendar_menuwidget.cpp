#include "calendar_menuwidget.h"

#include "fui_collection/fui_collection/fui_calendarevent.h"
#include "gui_core/gui_core/gui_helper.h"
#include "fui_collection/fui_collection/calendarhelper.h"

//#include <QMimeData>
//#include <QUrl>
#include <QToolButton>
#include <QHeaderView>
//#include <QFileDialog>
#include <QAction>
//#include "os_specific/os_specific/mime_types.h"
#include "gui_core/gui_core/styledpushbutton.h"
#include <QStatusBar>
//#include "taskdialog.h"
//#include "emaildialog.h"
//#include "fui_importemail.h"
#include "bus_core/bus_core/globalconstants.h"
//#include "os_specific/os_specific/mapimanager.h"
#include "gui_core/gui_core/thememanager.h"

//global comm manager
//#include "communicationmanager.h"
//extern CommunicationManager	g_CommManager;				
#include "fuimanager.h"
extern FuiManager g_objFuiManager;					
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester


Calendar_MenuWidget::Calendar_MenuWidget(QWidget *parent,bool bIgnoreViewMode)
: QWidget(parent)
{
	ui.setupUi(this);

	this->setAttribute(Qt::WA_DeleteOnClose, true);
	setAcceptDrops(true);

	m_bCloseOnSend=false;
	m_bScheduleTask=false;
	m_bCreateQuickTask=false;

	m_lstContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_lstProjects.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	m_lstResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));


	//MAIN BUTTON
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR && !bIgnoreViewMode)
	{
		GUI_Helper::CreateStyledButton(ui.btnNewEntity,":CalendarEvent24.png",QString("<html><b><i><font size=\"4\">%1</font></i></b></html>").arg(tr("Calendar ")),15,ThemeManager::GetCEMenuButtonStyleLighter(),10);
		ui.btnNewEntity->setMinimumHeight(26);
		ui.btnHide->setMinimumHeight(26);
		ui.treeWidgetTemplate->setStyleSheet("* { font-size: 9px }"); 
		ui.frame->setMaximumHeight(150);
		ui.btnHide->setStyleSheet(ThemeManager::GetCEMenuButtonStyleLighter());
	}
	else
	{
		GUI_Helper::CreateStyledButton(ui.btnNewEntity,":CalendarEvent32.png",QString("<html><b><i><font size=\"4\">%1</font></i></b></html>").arg(tr("Calendar ")),15,ThemeManager::GetCEMenuButtonStyle(),10);
		ui.btnHide->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	}

	ui.btnHide->setFocusPolicy(Qt::NoFocus);
	ui.btnNewEntity->setFocusPolicy(Qt::NoFocus);

	connect(ui.btnNewEntity,SIGNAL(clicked()),this,SLOT(OnMenuButton()));
	ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
	connect(ui.btnHide,SIGNAL(clicked()),this,SLOT(OnHideTab()));


	//TEMPLATE TOOLBARS
	//connect(ui.btnNewFromTemplate, SIGNAL(clicked()), this, SLOT(OnNewFromTemplate()));
	connect(ui.btnAddTemplate, SIGNAL(clicked()), this, SLOT(OnAddTemplate()));
	connect(ui.btnModifyTemplate, SIGNAL(clicked()), this, SLOT(OnModifyTemplate()));
	connect(ui.btnRemoveTemplate, SIGNAL(clicked()), this, SLOT(OnRemoveTemplate()));
	ui.btnAddTemplate->setIcon(QIcon(":handwrite.png"));
	ui.btnModifyTemplate->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnRemoveTemplate->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnModifyTemplate->setToolTip(tr("Modify/Create"));
	ui.btnRemoveTemplate->setToolTip(tr("Delete"));
	ui.btnAddTemplate->setToolTip(tr("Add"));


	ui.btnCreate->setIcon(QIcon(":CalendarEvent16.png"));
	ui.btnCreate->setToolTip(tr("Create Calendar Event From Template"));
	ui.btnSchedule->setIcon(QIcon(":ToDo32.png"));
	ui.btnSchedule->setToolTip(tr("New Task From Template"));
	connect(ui.btnCreate, SIGNAL(clicked()), this, SLOT(OnNewFromTemplate()));
	connect(ui.btnSchedule, SIGNAL(clicked()), this, SLOT(OnNewTask()));

	//TEMPLATE TREE:
	connect(ui.treeWidgetTemplate,SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)),this,SLOT(OnNewFromTemplate())); 
	ui.treeWidgetTemplate->header()->hide();
	ui.treeWidgetTemplate->setColumnCount(1);
	ui.treeWidgetTemplate->setSelectionBehavior( QAbstractItemView::SelectRows);
	ui.treeWidgetTemplate->setSelectionBehavior( QAbstractItemView::SelectItems);

	//LOGGED USER:
	m_nLoggedID=g_pClientManager->GetPersonID();

	//load all:
	m_Category.Initialize(ENTITY_CALENDAR_EVENT_TEMPLATE_CATEGORY);
	LoadCategory();
	LoadTemplates();

	g_ChangeManager.registerObserver(this);


	//---------------------------------------- INIT CNTX MENU TEMPLATE--------------------------------//
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;


	pAction = new QAction(tr("Create Calendar Event Using Template"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnNewFromTemplate()));
	lstActions.append(pAction);

	pAction = new QAction(tr("New Task From Template"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnNewTask()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Reload"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(ReLoadTemplates()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("Add Template"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnAddTemplate()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Modify Template"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnModifyTemplate()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete Template"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnRemoveTemplate()));
	lstActions.append(pAction);

	//set cnxt menu:
	ui.treeWidgetTemplate->SetContextMenuActions(lstActions);

	lstActions.clear();


	//ui.btnNewFromTemplate->setMenu(&m_TemplateMenu);

	//---------------------------------------- INIT CNTX MENU APP--------------------------------//

	connect(ui.cmbCategory, SIGNAL( currentIndexChanged(const QString &)), this, SLOT(OnCategoryChanged(const QString)));



	//FUI:
	int nValue;
	//if(!g_FunctionPoint.IsFPAvailable(FP_CAEMAIL_TEMPLATES, nValue))
	//{
		//ui.tabWidget->widget(0)->setEnabled(false);
		//ui.tabWidget->removeTab(0);
		/*
		ui.btnAddTemplate->setEnabled(false);
		ui.btnModifyTemplate->setEnabled(false);
		ui.btnRemoveTemplate->setEnabled(false);
		ui.btnCreate->setEnabled(false);
		ui.btnSchedule->setEnabled(false);
		*/
	//}

	if(!g_FunctionPoint.IsFPAvailable(FP_TASKS, nValue))
	{
		ui.btnSchedule->setEnabled(false);
	}

	/*
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW, nValue))
	{
		this->setEnabled(false);
	}
	*/


}

Calendar_MenuWidget::~Calendar_MenuWidget()
{
	g_ChangeManager.unregisterObserver(this);

}


void Calendar_MenuWidget::dragEnterEvent ( QDragEnterEvent * event )
{
	event->accept();
}

//if TAB= template then template, else doc, if not exe...brb....
void Calendar_MenuWidget::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

}

void Calendar_MenuWidget::OnHideTab()
{
	bool bVisible=ui.frame->isVisible();

	if (bVisible)
	{
		ui.btnHide->setIcon(QIcon(":Icon_Plus12.png"));
		ui.frame->setVisible(false);
	}
	else
	{
		ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
		ui.frame->setVisible(true);
	}

}


//load from server, rebuild tree
void Calendar_MenuWidget::LoadTemplates(bool bReload)
{
	if (m_nLoggedID==0) return;

	ui.treeWidgetTemplate->blockSignals(true);
	ui.treeWidgetTemplate->clear();

	CalendarHelper::GetCalendarTemplates(m_lstTemplates,bReload);

	int nIDIdx=m_lstTemplates.getColumnIdx("BCEV_ID");
	int nNameIdx=m_lstTemplates.getColumnIdx("BCEV_TITLE");
	int nDesc=m_lstTemplates.getColumnIdx("BCEV_DESCRIPTION");

	QString strCategory=ui.cmbCategory->currentText();

	QList<QTreeWidgetItem *> items;
	int nSize=m_lstTemplates.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//if category!='' and in template!='' then filter, else add...
		if (strCategory!=m_lstTemplates.getDataRef(i,"BCEV_CATEGORY").toString() && !m_lstTemplates.getDataRef(i,"BCEV_CATEGORY").toString().isEmpty() && !strCategory.isEmpty())
		{
			continue;
		}

		QString strNodeName=m_lstTemplates.getDataRef(i,nNameIdx).toString();
		int nID=m_lstTemplates.getDataRef(i,nIDIdx).toInt();
		QTreeWidgetItem *pRoot=new QTreeWidgetItem((QTreeWidget *)0,QStringList(strNodeName));
		pRoot->setData(0,Qt::UserRole,nID);
		pRoot->setToolTip(0,m_lstTemplates.getDataRef(i,nDesc).toString());
		items.append(pRoot);
	}
	ui.treeWidgetTemplate->insertTopLevelItems(0,items);
	ui.treeWidgetTemplate->blockSignals(false);
}


void Calendar_MenuWidget::LoadCategory(bool bReload)
{
	m_Category.ReloadData(bReload);

	int nIx=ui.cmbCategory->currentIndex();
	ui.cmbCategory->blockSignals(true);
	ui.cmbCategory->clear();

	DbRecordSet *lstData=m_Category.GetDataSource();
	int nSize=lstData->getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.cmbCategory->addItem(lstData->getDataRef(i,0).toString());
	}
	QString strLastCat=ui.cmbCategory->currentText();
	if (nIx!=-1)
		ui.cmbCategory->setCurrentIndex(nIx);
	else
		ui.cmbCategory->setCurrentIndex(0);


	ui.cmbCategory->blockSignals(false);
}

void Calendar_MenuWidget::OnCategoryChanged(QString strCategory)
{
	//filter templates by category:
	LoadTemplates();
}


void Calendar_MenuWidget::SetCloseOnSend()
{
	m_bCloseOnSend=true;
	this->setAttribute(Qt::WA_DeleteOnClose, true);
}
//-------------------------------------------------------------------------
//						TEMPLATES
//-------------------------------------------------------------------------




//go for new doc from template
void Calendar_MenuWidget::OnNewFromTemplate()
{

	//get current template:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetTemplate->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstTemplates.find(0,nRecordID,true);
	if (nRow==-1)return;

	//open Email FUI, go insert mode, give template:
	//issue 1445
	//if (m_bCreateQuickTask)
	//{
	//	OpenQuickTaskEmail(2,nRecordID);
	//	return;
	//}

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT, true, false,FuiBase::MODE_EMPTY,-1,true);
	QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_CalendarEvent* pFUI=dynamic_cast<FUI_CalendarEvent*>(pFUIWidget);
	if (pFUI)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_CALENDAR); //give me new data, if some1 is listening....

		DbRecordSet row;
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
		row.addRow();
		row.setData(0, "BCEV_FROM", m_datFrom);
		row.setData(0, "BCEV_TO", m_datTo);

		pFUI->SetDefaults(true,&row,&m_lstContacts,&m_lstProjects,&m_lstResources,m_bScheduleTask,false,nRecordID);
	}
	if(pFUI)pFUI->show();  //show FUI

	if (m_bCloseOnSend)
		close();
}

void Calendar_MenuWidget::OnAddTemplate()
{
	//open FUI
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT, true, false,FuiBase::MODE_EMPTY, -1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_CalendarEvent* pFuiDM=dynamic_cast<FUI_CalendarEvent*>(pFUI);
	if (pFuiDM)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_CALENDAR); //give me new data, if some1 is listening....
		pFuiDM->SetDefaults(true,NULL,&m_lstContacts,&m_lstProjects,&m_lstResources,false,true);
	}
	if(pFUI)pFUI->show();  //show FUI
}

void Calendar_MenuWidget::OnModifyTemplate()
{
	//get selected cost center
	int nRecordID = -1;
	//get current item:
	QList<QTreeWidgetItem *> items=ui.treeWidgetTemplate->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT, true, false,FuiBase::MODE_EDIT, nRecordID,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(pFUI)pFUI->show();  //show FUI
}



void Calendar_MenuWidget::OnRemoveTemplate()
{
	//get current item:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetTemplate->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	//warn
	int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete this record permanently from the database?"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES


	int nRow=m_lstTemplates.find(0,nRecordID,true);
	if (nRow>=0)
	{
		//int nCommEntityID=m_lstTemplates.getDataRef(nRow,"BEM_COMM_ENTITY_ID").toInt();

		//delete
		DbRecordSet lst;
		lst.addColumn(QVariant::Int,"BCEV_ID");
		lst.addRow();
		lst.setData(0,0,nRecordID);
		Status err;
		QString pLockResourceID;
		DbRecordSet lstStatusRows;
		bool pBoolTransaction = true;
		_SERVER_CALL(ClientSimpleORM->Delete(err,BUS_CAL_EVENT,lst, pLockResourceID, lstStatusRows, pBoolTransaction))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}


			//remove item from tree:
			delete items.at(0);
			LoadCategory(true);
	}
}




/*! Catches global cache events

\param pSubject			- source of msg
\param nMsgCode			- msg code
\param val				- value sent from observer
*/
void Calendar_MenuWidget::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}

	if(nMsgDetail==ENTITY_CALENDAR_EVENT)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			//always reload:
			LoadCategory(true);

			//CallBack to refresh displayed data:
			DbRecordSet lstNewData=val.value<DbRecordSet>();


			ui.treeWidgetTemplate->blockSignals(true);
			//set node to lstNewData
			if (lstNewData.getRowCount()>0)
			{
				if (lstNewData.getColumnIdx("BCEV_TEMPLATE_FLAG")>=0)
				{
					if (lstNewData.getDataRef(0,"BCEV_TEMPLATE_FLAG").toInt()>0) //only reload when new template mail is modified
					{
						//Reload data:
						CalendarHelper::GetCalendarTemplates(m_lstTemplates,true); //reload templates from server: specially for emails, coz entity is stored in different cache
						LoadTemplates();

						QVariant varNode=lstNewData.getDataRef(0,"BCEV_ID");
						QTreeWidgetItem *item=SearchTree(ui.treeWidgetTemplate,varNode);
						if (item)
						{
							item->setSelected(true);
							ui.treeWidgetTemplate->setCurrentItem(item);
						}
					}
				}
				else
					LoadTemplates(true);
			}
			ui.treeWidgetTemplate->blockSignals(false);
			return;
		}

}




QTreeWidgetItem* Calendar_MenuWidget::SearchTree(QTreeWidget* pTree, QVariant nUserValue)
{

	QTreeWidgetItem* item=NULL;

	int nSize=pTree->topLevelItemCount();
	for(int i=0;i<nSize;++i)
	{
		item=pTree->topLevelItem(i);
		if (item->data(0,Qt::UserRole)==nUserValue)
			return item;
	}

	return NULL;
}



//open empty doc
void Calendar_MenuWidget::OnMenuButton()
{
	//issue 1445
	//if (m_bCreateQuickTask)
	//{
	//	OpenQuickTaskEmail(0);
	//	return;
	//}

	//open FUI
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT, true, false,FuiBase::MODE_EMPTY, -1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_CalendarEvent* pFuiDM=dynamic_cast<FUI_CalendarEvent*>(pFUI);
	if (pFuiDM)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_CALENDAR); //give me new data, if some1 is listening....

		DbRecordSet row;
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
		row.addRow();
		row.setData(0, "BCEV_FROM", m_datFrom);
		row.setData(0, "BCEV_TO", m_datTo);

		//m_lstProjects.Dump();

		pFuiDM->SetDefaults(true,&row,&m_lstContacts,&m_lstProjects,&m_lstResources,m_bScheduleTask);
	}
	if(pFUI)pFUI->show();  //show FUI

	if (m_bCloseOnSend)
		close();

}


void Calendar_MenuWidget::SetDefaults(DbRecordSet *lstContacts,DbRecordSet *lstProjects, DbRecordSet *lstResources,QDateTime from, QDateTime to)
{
	if (lstContacts)
	{
		int nCntIdx=lstContacts->getColumnIdx("BCNT_ID");
		int nCntIdx1=m_lstContacts.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		m_lstContacts.clear();
		int nSize=lstContacts->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			m_lstContacts.addRow();
			m_lstContacts.setData(m_lstContacts.getRowCount()-1,nCntIdx1,lstContacts->getDataRef(i,nCntIdx));
		}
	}

	if (lstProjects)
	{

		int nCntIdx=lstProjects->getColumnIdx("BUSP_ID");
		int nCntIdx1=m_lstProjects.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		int nSize=lstProjects->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			m_lstProjects.addRow();
			m_lstProjects.setData(m_lstProjects.getRowCount()-1,nCntIdx1,lstProjects->getDataRef(i,nCntIdx));
		}

	}


	if (lstResources)
	{
		int nCntIdx=lstResources->getColumnIdx("BRES_ID");
		int nCntIdx1=m_lstResources.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		m_lstResources.clear();
		int nSize=lstResources->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			m_lstResources.addRow();
			m_lstResources.setData(m_lstResources.getRowCount()-1,nCntIdx1,lstResources->getDataRef(i,nCntIdx));
		}
	}

	m_datFrom=from;
	m_datTo=to;
}



void Calendar_MenuWidget::OnNewTask()
{
	//get current template:
	int nRecordID;
	QList<QTreeWidgetItem *> items=ui.treeWidgetTemplate->selectedItems();
	if (items.count()!=1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Please first select an template!"));
		return;
	}
	else
	{
		nRecordID=items.at(0)->data(0,Qt::UserRole).toInt();
	}
	int nRow=m_lstTemplates.find(0,nRecordID,true);
	if (nRow==-1)return;


	//issue 1445
	//if (m_bCreateQuickTask)
	//{
	//	OpenQuickTaskEmail(1,nRecordID);
	//	return;
	//}


	//open FUI
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR_EVENT, true, false,FuiBase::MODE_EMPTY, -1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_CalendarEvent* pFuiDM=dynamic_cast<FUI_CalendarEvent*>(pFUI);
	if (pFuiDM)
	{
		DbRecordSet row;
		row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
		row.addRow();
		row.setData(0, "BCEV_FROM", m_datFrom);
		row.setData(0, "BCEV_TO", m_datTo);

		pFuiDM->SetDefaults(true,&row,&m_lstContacts,&m_lstProjects,&m_lstResources,true,false,nRecordID);
	}
	if(pFUI)pFUI->show();  //show FUI
	if (m_bCloseOnSend)
		close();

}


bool Calendar_MenuWidget::IsHidden()
{
	if (height()<=50)
		return true;
	else
		return false;
}
void Calendar_MenuWidget::SetHidden(bool bHide)
{
	if (bHide)
	{
		ui.btnHide->setIcon(QIcon(":Icon_Plus12.png"));
		ui.frame->setVisible(false);
	}
	else
	{
		ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
		ui.frame->setVisible(true);
	}
}

void Calendar_MenuWidget::OnThemeChanged()
{
	ui.btnNewEntity->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	ui.btnHide->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
}




void Calendar_MenuWidget::SetSideBarLayout()
{
	this->setWindowFlags(Qt::FramelessWindowHint);

	QVBoxLayout *layout=dynamic_cast<QVBoxLayout*>(this->layout());

	//HEADER:
	QFrame *pHeader= new QFrame(this);
	pHeader->setFrameShape(QFrame::NoFrame);
	pHeader->setMaximumHeight(30);
	pHeader->setMinimumHeight(30);
	pHeader->setObjectName("Header_bar");
	pHeader->setStyleSheet("QFrame#Header_bar "+ThemeManager::GetSidebarChapter_Bkg());

	QSize buttonSize_menu(13, 11);
	StyledPushButton *close		= new StyledPushButton(this,":Icon_CloseBlack.png",":Icon_CloseBlack.png","");
	close		->setMaximumSize(buttonSize_menu);
	close		->setMinimumSize(buttonSize_menu);
	close		->setToolTip(tr("Close"));

	QLabel *label = new QLabel(this);
	label->setStyleSheet("QLabel "+ThemeManager::GetSidebarChapter_Font());
	label->setText(tr("Calendar Manager"));

	QVBoxLayout *buttonLayout_menu_v = new QVBoxLayout;
	buttonLayout_menu_v->addWidget(close);
	buttonLayout_menu_v->addStretch(1);
	buttonLayout_menu_v->setSpacing(0);
	buttonLayout_menu_v->setContentsMargins(2,0,2,2);

	QHBoxLayout *buttonLayout_menu_h = new QHBoxLayout;

	buttonLayout_menu_h->addWidget(label);
	buttonLayout_menu_h->addStretch(1);
	buttonLayout_menu_h->addLayout(buttonLayout_menu_v);
	buttonLayout_menu_h->setSpacing(0);
	buttonLayout_menu_h->setContentsMargins(0,0,0,0);

	pHeader->setLayout(buttonLayout_menu_h);

	connect(close,SIGNAL(clicked()),this,SLOT(close()));


	//FOOTER:
	QStatusBar *statusBar = new QStatusBar(this);
	statusBar->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	statusBar->setObjectName("Status_bar");

	QFrame *pFooter= new QFrame(this);
	pFooter->setFrameShape(QFrame::NoFrame);
	pFooter->setMaximumHeight(20);
	pFooter->setMinimumHeight(20);

	QHBoxLayout *footer = new QHBoxLayout;
	footer->addWidget(statusBar);
	footer->setSpacing(0);
	footer->setContentsMargins(0,0,0,0);
	pFooter->setLayout(footer);

	pFooter->setStyleSheet("QFrame "+ThemeManager::GetSidebarChapter_Bkg());


	//GLOBAL BKG COLOR:
	this->setObjectName("MY_WIDGET");
	this->setStyleSheet("QWidget#MY_WIDGET "+ThemeManager::GetSidebar_Bkg()+ThemeManager::GetGlobalWidgetStyle());


	//SET IT
	layout->insertWidget(0,pHeader);
	layout->addWidget(pFooter);
}



void Calendar_MenuWidget::mouseMoveEvent(QMouseEvent *event)
{
	move(event->globalPos() - m_dragPosition);
	event->accept();
} 

void Calendar_MenuWidget::mouseReleaseEvent(QMouseEvent * event)
{
	QWidget::mouseReleaseEvent(event);
}


void Calendar_MenuWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && event->y()<30) 
	{
		m_dragPosition = event->globalPos() - frameGeometry().topLeft();
		event->accept();
	}
	else
		m_dragPosition=QPoint(0,0);

} 
/*
void Calendar_MenuWidget::OpenQuickTaskEmail(int nOp, int nRecordID)
{
	//reset flag, coz we schedule all, regardless of this flag
	m_bScheduleTask=false;

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_EMAIL, true, false,FuiBase::MODE_EMPTY,-1,true);
	QWidget* pFUIWidget=g_objFuiManager.GetFUIWidget(nNewFUI);
	EMailDialog* pFUI=dynamic_cast<EMailDialog*>(pFUIWidget);
	if (pFUI)
	{
		//emit NeedNewData(CommunicationMenu_Base::SUBMENU_EMAIL); //give me new data, if some1 is listening....

		switch(nOp)
		{
		case 0:
			{
				pFUI->SetDefaults(m_lstContacts,m_strEmailDefault,m_nProjectID,-1,m_bScheduleTask,false,m_nEmailReplyID,&m_lstPackSendPaths,m_bIsForward);
			}
			break;
		case 1:
			{
				pFUI->SetDefaults(m_lstContacts,m_strEmailDefault,m_nProjectID,nRecordID,m_bScheduleTask,false,m_nEmailReplyID,&m_lstPackSendPaths,m_bIsForward);  //as task..........
			}
			break;
		case 2:
			{
				pFUI->SetDefaults(m_lstContacts,m_strEmailDefault,m_nProjectID,nRecordID,m_bScheduleTask,false,m_nEmailReplyID,&m_lstPackSendPaths,m_bIsForward); //never use reply mode if new from template
			}
			break;
		}

	}

	TaskDialog Dlg;
	Dlg.SetDefaultTaskType(GlobalConstants::TASK_TYPE_SCHEDULED_EMAIL);
	Dlg.m_pRecMain=pFUI->GetRecordSet();
	Dlg.SetGUIFromData();
	Dlg.SetEnabled(true);

	if(Dlg.exec())
	{
		Dlg.GetDataFromGUI();
		DbRecordSet rowTask=Dlg.m_recTask;
		Status err;
		pFUI->CreateQuickTask(err,rowTask);
		_CHK_ERR_NO_RET(err);
		if (err.IsOK())
			pFUI->notifyCache_AfterWrite(*pFUI->GetRecordSet());
	}

	pFUI->close();

	//close this widget
	close();
}
*/