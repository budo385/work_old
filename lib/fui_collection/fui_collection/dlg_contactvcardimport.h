#ifndef DLG_CONTACTVCARDIMPORT_H
#define DLG_CONTACTVCARDIMPORT_H

#include <QtWidgets/QDialog>
#include "ui_dlg_contactvcardimport.h"
#include "bus_core/bus_core/formatphone.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include <QMenu>
#include "bus_core/bus_core/formataddress.h"


class Dlg_ContactVcardImport : public QDialog
{
	Q_OBJECT

public:
	Dlg_ContactVcardImport(QWidget *parent = 0);
	~Dlg_ContactVcardImport();

	void SetData(int nActualContactID, QString strActualContactName,DbRecordSet lstVcardDataForImport);

private slots:
	void on_btnFormatPhone1_clicked();
	void on_btnFormatPhone2_clicked();
	void on_btnFormatPhone3_clicked();
	void on_btnFormatPhone4_clicked();

	void on_rb_New_clicked(bool);
	void on_rb_Add_clicked(bool);
	void on_ckb1_stateChanged(int);
	void on_ckb2_stateChanged(int);
	void on_ckb3_stateChanged(int);
	void on_ckb4_stateChanged(int);
	void on_groupAddr_clicked (bool);

	void OnMenuTriggerPhone1(QAction *action);
	void OnMenuTriggerPhone2(QAction *action);
	void OnMenuTriggerPhone3(QAction *action);
	void OnMenuTriggerPhone4(QAction *action);

	void on_btnCancel_clicked(bool);
	void on_btnOK_clicked(bool);


private:
	Ui::Dlg_ContactVcardImport ui;

	void SetMenuPhoneTypes(QMenu *menu);
	void FormatPhoneNumber(int nRow);
	void AddPhone(int nPos,QString strPhone, int nTypeID);

	MainEntitySelectionController m_Types;
	FormatPhone m_PhoneFormatter;
	FormatAddress m_FormatAddress;
	QMenu m_MenuPhone_1;
	QMenu m_MenuPhone_2;
	QMenu m_MenuPhone_3;
	QMenu m_MenuPhone_4;

	DbRecordSet m_lstPhones;
	DbRecordSet m_lstAddress;

	QString m_strHtmlTag;
	QString m_strEndHtmlTag;

	int m_nActualContactID;
	DbRecordSet m_lstVcardDataForImport;

};

#endif // DLG_CONTACTVCARDIMPORT_H
