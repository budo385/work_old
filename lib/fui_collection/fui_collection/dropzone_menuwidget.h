#ifndef DROPZONE_MENUWIDGET_H
#define DROPZONE_MENUWIDGET_H

#include <QWidget>
#include <QAuthenticator>
#include "generatedfiles/ui_dropzone_menuwidget.h"
#include "common/common/dbrecordset.h"
#include "common/common/observer_ptrn.h"

class DropZone_MenuWidget : public QWidget, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	DropZone_MenuWidget(QWidget *parent = 0);
	~DropZone_MenuWidget();

	//void SetDocumentDropHandler(QWidget *pWidget){m_DropDoc=pWidget;}

	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void dragMoveEvent(QDragMoveEvent *event );

	void SetFUIParent(int nFUI){m_nFUI=nFUI;}

	bool IsHidden();
	void SetHidden(bool bHide=true, bool bSkipSignals=false);

signals:
	void NeedNewData(int nSubMenuType);														//emited always when new document is about to be created...to assign doc to contacts/projects later
	void NewDataDropped(int nDropType, DbRecordSet lstDroppedData);			//emited always when soemthing is dropped on drop zone
	void ChapterCollapsed(bool);

private slots:
	void OnMenuButton(){};
	void OnHideTab();
	void OnPasteFromClipboard();
	void OnTextChanged();
	void ProcessTextDrop(QString strTextDrop);
	void OnEmitAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth);

	//btns:
	void on_btnInternet_clicked();
	void on_btnAddress_clicked();
	void on_btnEmail_clicked();
	void on_btnPhone_clicked();
	void on_btnQCW_clicked();


private:
	void OnThemeChanged();
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val);
	void ProcessAddress(QString strTextDrop);
	void ProcessEMail(QString strTextDrop);
	void ProcessPhone(QString strTextDrop);
	void ProcessInternet(QString strTextDrop);
	bool SaveAsContactPictureIfPictureLink(QList<QUrl> &lst);
	bool GetLinkContent(QString strFilePath, QByteArray &content);

	Ui::DropZone_MenuWidgetClass ui;
	//QWidget *m_DropDoc;
	bool m_bTextDropWasEmpty;
	int m_nFUI;

	

};

#endif // DROPZONE_MENUWIDGET_H
