#ifndef SETTINGS_DOCDEFAULTS_H
#define SETTINGS_DOCDEFAULTS_H

#include <QWidget>
#include "generatedfiles/ui_settings_docdefaults.h"
#include "settingsbase.h"


class Settings_DocDefaults : public SettingsBase
{
	Q_OBJECT

public:
	Settings_DocDefaults(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
	~Settings_DocDefaults();

	DbRecordSet GetChangedValuesRecordSet();

private slots:
	void OnSelectPath();
	void OnRemovePath();

private:
	Ui::Settings_DocDefaultsClass ui;
};

#endif // SETTINGS_DOCDEFAULTS_H
