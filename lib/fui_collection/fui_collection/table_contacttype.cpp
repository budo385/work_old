#include "table_contacttype.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include <QComboBox>
#include <QtWidgets/QMessageBox>
#include "gui_core/gui_core/dlg_picture.h"
//#include "gui_core/gui_core/colorpicker/qtcolorpicker.h"
#include "gui_core/gui_core/picturehelper.h"
#include "fui_collection/fui_collection/selection_sapne_fui.h"
#include "common/common/entity_id_collection.h"

Table_ContactType::Table_ContactType(QWidget * parent)
:UniversalTableWidgetEx(parent)
{
}


//called after dark
void Table_ContactType::Initialize(DbRecordSet *plstData,toolbar_edit *pToolBar)
{
	//set data source for table, enable drop
	BuildToolBar(pToolBar);
	DbRecordSet columns;
	AddColumnToSetup(columns,"BCMT_TYPE_NAME",tr("Type"),300,true,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"",tr("System Type"),300,true, "",COL_TYPE_CUSTOM_WIDGET,"SYSTEM_TYPE_NAME");
	AddColumnToSetup(columns,"BCMT_COLOR",tr("Color"),100,false, "",COL_TYPE_CUSTOM_WIDGET);
	AddColumnToSetup(columns,"BCMT_PICTURE",tr("Icon"),60,false, "",COL_TYPE_CUSTOM_WIDGET);
	AddColumnToSetup(columns,"BCMT_EMAIL_TEMPLATE_ID",tr("Invite Email Template"),200,false, "",COL_TYPE_CUSTOM_WIDGET);
	m_nCol_System=1;
	m_nCol_Color=2;
	m_nCol_Icon=3;
	m_nCol_EmailTemplate=4;

	UniversalTableWidgetEx::Initialize(plstData,&columns,false,true,38);
	m_nDefaultColIdx=plstData->getColumnIdx("BCMT_IS_DEFAULT");
	CreateContextMenu();

	connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
}

//dummy: if inserted: blue icon, else red
void Table_ContactType::GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip)
{
	//if Default then show checked icon:
	if(m_plstData->getDataRef(nRow,m_nDefaultColIdx).toInt()!=0)
	{
		RowIcon.addFile(":checked.png");
		strStatusTip=tr("Default Type");
	}
	else
		RowIcon=QIcon(); //empty icon
}


//create custom cntx menu at end of existing one:
void Table_ContactType::CreateContextMenu()
{
	QList<QAction*> lstActions;
	CreateDefaultContextMenuActions(lstActions);
	//Add separator
	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.prepend(SeparatorAct);
	//set as def
	QAction *pAction = new QAction(tr("&Set as Default"), this);
	pAction->setIcon(QIcon(":checked.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnSetDefault()));
	lstActions.prepend(pAction);

	SetContextMenuActions(lstActions);
}

//invoked by cnt menu, set default flag on selected row
void Table_ContactType::OnSetDefault()
{
	int nRow=m_plstData->getSelectedRow();
	if(nRow!=-1)
	{
		//set default flag:
		m_plstData->setColValue(m_nDefaultColIdx,0);
		m_plstData->setData(nRow,m_nDefaultColIdx,1);
		RefreshDisplay();
	}
}


void Table_ContactType::SetEntityType(int nEntityType,bool bAddFormatting, bool bAddSystemType,bool bAddEmailTemplate)
{
	m_nCol_System=-1;
	m_nCol_EmailTemplate=-1;
	ContactTypeManager::GetSystemTypeList(nEntityType,m_lstSystemTypes);
	DbRecordSet columns;
	if (!bAddFormatting)
	{
		AddColumnToSetup(columns,"BCMT_TYPE_NAME",tr("Type"),300,true, "",COL_TYPE_TEXT,"");
		if(bAddSystemType)
		{
			AddColumnToSetup(columns,"",tr("System Type"),300,true, "",COL_TYPE_CUSTOM_WIDGET,"SYSTEM_TYPE_NAME");
			m_nCol_System=1;
		}
		AddColumnToSetup(columns,"BCMT_COLOR",tr("Color"),100,false, "",COL_TYPE_CUSTOM_WIDGET);
		m_nCol_Color=columns.getRowCount()-1;
		AddColumnToSetup(columns,"BCMT_PICTURE",tr("Icon"),60,false, "",COL_TYPE_CUSTOM_WIDGET);
		m_nCol_Icon=columns.getRowCount()-1;
		if (bAddEmailTemplate)
		{
			AddColumnToSetup(columns,"BCMT_EMAIL_TEMPLATE_ID",tr("Invite Email Template"),200,false, "",COL_TYPE_CUSTOM_WIDGET);
			m_nCol_EmailTemplate=columns.getRowCount()-1;
		}
		SetColumnSetup(columns);
	}
	else
	{
		AddColumnToSetup(columns,"BCMT_TYPE_NAME",tr("Type"),300,true, "",COL_TYPE_TEXT,"");
		if(bAddSystemType)
		{
			AddColumnToSetup(columns,"",tr("System Type"),300,true, "",COL_TYPE_CUSTOM_WIDGET,"SYSTEM_TYPE_NAME");
			m_nCol_System=1;
		}
		AddColumnToSetup(columns,"BCMT_COLOR",tr("Color"),100,false, "",COL_TYPE_CUSTOM_WIDGET);
		m_nCol_Color=columns.getRowCount()-1;
		AddColumnToSetup(columns,"BCMT_PICTURE",tr("Icon"),60,false, "",COL_TYPE_CUSTOM_WIDGET);
		m_nCol_Icon=columns.getRowCount()-1;
		AddColumnToSetup(columns,"BCMT_IS_FORMATTING_ALLOWED",tr("Format Phone"),100,true, "",COL_TYPE_CHECKBOX);
		if (bAddEmailTemplate)
		{
			AddColumnToSetup(columns,"BCMT_EMAIL_TEMPLATE_ID",tr("Invite Email Template"),200,false, "",COL_TYPE_CUSTOM_WIDGET);
			m_nCol_EmailTemplate=columns.getRowCount()-1;
		}
		SetColumnSetup(columns);
	}

}

void Table_ContactType::BuildToolBar(toolbar_edit * pToolBar)
{
	m_toolbar=pToolBar;
	pToolBar->GetBtnAdd()->setText(tr("Add New Type"));
	connect(pToolBar->GetBtnAdd(), SIGNAL(clicked()), this, SLOT(InsertRow()));
	pToolBar->GetBtnSelect()->setVisible(false);
	pToolBar->GetBtnModify()->setVisible(false);
	connect(pToolBar->GetBtnClear(), SIGNAL(clicked()), this, SLOT(DeleteSelection()));

}


void Table_ContactType::SetEditMode(bool bEdit)
{
	UniversalTableWidgetEx::SetEditMode(bEdit);
	if(m_toolbar)m_toolbar->setEnabled(bEdit);
	
	if (m_nCol_EmailTemplate>=0)
	{
		int nSize=m_plstData->getRowCount()-1;
		for (int i=0;i<nSize;i++)
		{
			Selection_SAPNE_FUI *pSapne = dynamic_cast<Selection_SAPNE_FUI *>(cellWidget(i,m_nCol_EmailTemplate));
			if (pSapne)
			{
				pSapne->SetEditMode(bEdit);
				pSapne->GetButton(Selection_SAPNE::BUTTON_SELECT)->setEnabled(bEdit);
				pSapne->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setEnabled(bEdit);
			}
		}
	}
}


void Table_ContactType::Data2CustomWidget(int nRow, int nCol)
{
	if (nCol==m_nCol_System)
	{
		QComboBox *pCombo = dynamic_cast<QComboBox *>(cellWidget(nRow,nCol));
		if (!pCombo)
		{
			pCombo = new QComboBox;
			pCombo->setEditable(false);
			pCombo->setProperty("combo_row",nRow); //to identify sender
			connect(pCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(OnComboIndexChanged(int)));  //connect on change signals:

			pCombo->blockSignals(true);
			pCombo->clear();
			int nSize=m_lstSystemTypes.getRowCount();
			for(int i=0;i<nSize;++i)
			{
				pCombo->addItem(m_lstSystemTypes.getDataRef(i,"SYSTEM_TYPE_NAME").toString());
				pCombo->setItemData(i,m_lstSystemTypes.getDataRef(i,"SYSTEM_TYPE_ID").toInt());
			}
			pCombo->blockSignals(false);
			pCombo->setEnabled(m_bEdit);
			pCombo->setEditable(false);
			setCellWidget(nRow,nCol,pCombo);
		}

		int nSysID=m_plstData->getDataRef(nRow,"BCMT_SYSTEM_TYPE").toInt();
		pCombo->blockSignals(true);
		if (nSysID<0)
			pCombo->setCurrentIndex(-1);
		else
			pCombo->setCurrentIndex(pCombo->findData(nSysID));
		pCombo->blockSignals(false);
	}
	else if (nCol==m_nCol_Color)
	{
		//B.T. : this is not used and can not be compiled: use QtColorDialog
		/*
		QString strColor=m_plstData->getDataRef(nRow,"BCMT_COLOR").toString();
		QtColorPicker *pPic = dynamic_cast<QtColorPicker *>(cellWidget(nRow,nCol));
		if (!pPic)
		{
			pPic = new QtColorPicker;
			pPic->setProperty("row",nRow); //to identify sender
			pPic->setStandardColors();
			connect(pPic,SIGNAL(colorChanged(const QColor &)),this,SLOT(OnColorChanged(const QColor &)));  //connect on change signals:
			if (!strColor.isEmpty())
				pPic->setCurrentColor(QColor(strColor));
			setCellWidget(nRow,nCol,pPic); 
		}
		pPic->setEnabled(m_bEdit);
		if (!strColor.isEmpty())
			pPic->setCurrentColor(QColor(strColor));
		*/
	}
	else if (nCol==m_nCol_Icon)
	{
		QLabel *pPic = dynamic_cast<QLabel *>(cellWidget(nRow,nCol));
		if (!pPic)
		{
			pPic = new QLabel;
			pPic->setAlignment(Qt::AlignCenter);
			pPic->setScaledContents(false); //scales image..
			setCellWidget(nRow,nCol,pPic);
		}
		QPixmap pixMap;
		pixMap.loadFromData(m_plstData->getDataRef(nRow,"BCMT_PICTURE").toByteArray());
		pPic->setPixmap(pixMap);
	}
	else if (nCol==m_nCol_EmailTemplate)
	{
		Selection_SAPNE_FUI *frameTemplateID = dynamic_cast<Selection_SAPNE_FUI *>(cellWidget(nRow,nCol));
		if (!frameTemplateID)
		{
			frameTemplateID = new Selection_SAPNE_FUI;
			frameTemplateID->Initialize(ENTITY_BUS_EMAIL_TEMPLATES);
			frameTemplateID->GetButton(Selection_SAPNE::BUTTON_MODIFY)->setVisible(false);
			frameTemplateID->GetButton(Selection_SAPNE::BUTTON_ADD)->setVisible(false);
			frameTemplateID->GetButton(Selection_SAPNE::BUTTON_CONTACT_PIE)->setVisible(false);
			frameTemplateID->GetButton(Selection_SAPNE::BUTTON_VIEW)->setVisible(false);

			frameTemplateID->GetButton(Selection_SAPNE::BUTTON_SELECT)->setEnabled(m_bEdit);
			frameTemplateID->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setEnabled(m_bEdit);
			setCellWidget(nRow,nCol,frameTemplateID);
		}
		frameTemplateID->SetCurrentEntityRecord(m_plstData->getDataRef(nRow,"BCMT_EMAIL_TEMPLATE_ID").toInt());
	}

}
void Table_ContactType::CustomWidget2Data(int nRow, int nCol)
{
	if (nCol==m_nCol_EmailTemplate)
	{
		Selection_SAPNE_FUI *frameTemplateID = dynamic_cast<Selection_SAPNE_FUI *>(cellWidget(nRow,nCol));
		if (frameTemplateID)
		{
			int nTemplateID;
			frameTemplateID->GetCurrentEntityRecord(nTemplateID);
			if (nTemplateID<=0)
				m_plstData->setData(nRow,"BCMT_EMAIL_TEMPLATE_ID",QVariant(QVariant::Int));
			else
				m_plstData->setData(nRow,"BCMT_EMAIL_TEMPLATE_ID",nTemplateID);
		}
	}
}

void Table_ContactType::OnComboIndexChanged(int nIndex)
{
	QComboBox *pCombo = dynamic_cast<QComboBox *>(sender());
	if (pCombo)
	{
		int nRow=pCombo->property("combo_row").toInt();
		Q_ASSERT(nRow>=0 && nRow <m_plstData->getRowCount()); //row must be inside data

		if (nIndex>0) //first is dummy: empty one
		{
			int nVal=pCombo->itemData(pCombo->currentIndex()).toInt();
			int nCount=m_plstData->countColValue(m_plstData->getColumnIdx("BCMT_SYSTEM_TYPE"),nVal);
			if(nCount>=1)
			{
				QMessageBox::warning(NULL,QObject::tr("Error"),tr("Only one user defined type can have the selected system type!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				//restore cmbo back:
				int nSysID=m_plstData->getDataRef(nRow,"BCMT_SYSTEM_TYPE").toInt();
				pCombo->blockSignals(true);
				if (nSysID<0)
					pCombo->setCurrentIndex(-1);
				else
					pCombo->setCurrentIndex(pCombo->findData(nSysID));
				pCombo->blockSignals(false);
				return;
			}
		}

		m_plstData->setData(nRow,"BCMT_SYSTEM_TYPE",pCombo->itemData(pCombo->currentIndex()));
		OnCustomWidgetCellClicked(nRow,0);
	}
}


void Table_ContactType::CheckDataBeforeWrite(Status &Ret_pStatus)
{
	Table2Data();
	Ret_pStatus.setError(0);
	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (m_plstData->getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Type Name is mandatory field!"));	
			return;	
		}
	}
}

void Table_ContactType::OnDeleted()
{
	RefreshDefaultFlag();
}
void Table_ContactType::OnRowInserted(int nRow)
{
	RefreshDefaultFlag();
}


//when deleting row, make sure to have at least one flag
void Table_ContactType::RefreshDefaultFlag()
{
	int nCountDefs=m_plstData->countColValue(m_nDefaultColIdx,1);
	if(nCountDefs==0 && m_plstData->getRowCount()>0)				//set first to be default if default is deleted!
		m_plstData->setData(0,m_nDefaultColIdx,1);
	else if (nCountDefs>1) //if default copied
	{
		int nRowFirstDef=m_plstData->find(m_nDefaultColIdx,1,true);
		if (nRowFirstDef>=0)
		{
			m_plstData->setColValue(m_nDefaultColIdx,0);
			m_plstData->setData(nRowFirstDef,m_nDefaultColIdx,1);
		}
	}
}

void Table_ContactType::OnCellDoubleClicked(int nRow,int nCol)
{
	if (!IsEditMode())
		return;

	if (nCol==m_nCol_Color)
	{
		/*
		//Open clr picker
		QtColorPicker Dlg;
		Dlg.setColorDialogEnabled(true);

		QString strColor=m_plstData->getDataRef(nRow,"BCMT_COLOR").toString();
		if (!strColor.isEmpty())
		{
			QColor col(strColor);
			Dlg.setCurrentColor(col);
		}
		if (Dlg.exec())
		{
			QColor col=Dlg.currentColor();
			QString strColor=col.name();
			m_plstData->setData(nRow,"BCMT_COLOR",strColor);
		}
		*/
	}

	if (nCol==m_nCol_Icon)
	{
		//open picture picker
		Dlg_Picture Dlg;
		QByteArray ar = m_plstData->getDataRef(nRow,"BCMT_PICTURE").toByteArray();
		Dlg.SetPictureBinaryData(ar);
		Dlg.SetEditMode(true);
		if(Dlg.exec())
		{
			QPixmap pix=Dlg.GetPixMap();
			PictureHelper::ResizePixMap(pix,32,32);
			QByteArray data;
			PictureHelper::ConvertPixMapToByteArray(data,pix,Dlg.GetCurrentPictureFormat());
			m_plstData->setData(nRow,"BCMT_PICTURE",data);
			RefreshDisplay(nRow);
		}
	}
}

void Table_ContactType::OnColorChanged(const QColor &col)
{
	/*
	QtColorPicker *pPic = dynamic_cast<QtColorPicker *>(sender());
	if (pPic)
	{
		int nRow=pPic->property("row").toInt();
		Q_ASSERT(nRow>=0 && nRow <m_plstData->getRowCount()); //row must be inside data
		QString strColor=col.name();
		m_plstData->setData(nRow,"BCMT_COLOR",strColor);
	}
	*/
}