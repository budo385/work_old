#include "communicationmenu.h"
#include <QVBoxLayout>
#include <QScrollArea>
#include <QHeaderView>
#include <QSplitter>
#include <QDesktopServices>
#include "gui_core/gui_core/gui_helper.h"
#include "fui_voicecallcenter.h"
#include "gui_core/gui_core/thememanager.h"

#include "fuimanager.h"
extern FuiManager g_objFuiManager;	
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
//#include "bus_client/bus_client/clientmanagerext.h"
//extern ClientManagerExt *g_pClientManager;

CommunicationMenu::CommunicationMenu(QWidget* parent)
: CommunicationMenu_Base(parent)
{
	//MAIN BUTTON
	//QString	styleSectionButtonLvl0="QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(:Button_DarkGray.png) 4px 4px 4px 4px stretch stretch }\
	//							   QPushButton:hover {  border-width: 4px; color: white;border-image:url(:Button_LightGray.png) 4px 4px 4px 4px stretch stretch} ";

	
	m_nEmailReplyID=-1;

	//adjust size inside splitter:
	QSplitter *pSplitter=dynamic_cast<QSplitter *>(this->parentWidget());
	if(pSplitter)
	{
		QList<int> lstSizes=pSplitter->sizes();
		if (lstSizes.size()==2)
		{
			pSplitter->setCollapsible(0,false);
			pSplitter->setCollapsible(1,true);
			//GUI_Helper::AdjustFixedWidget(pSplitter->widget(0),pSplitter->widget(1),true);
			int n=pSplitter->indexOf(this);

			
			int nWidth=175;
			if (pSplitter->parentWidget()!=NULL)
			{
				if (pSplitter->parentWidget()->parentWidget()!=NULL)
				{
					QString strParent=pSplitter->parentWidget()->parentWidget()->objectName();
					if (strParent=="FUI_ContactsClass")
						nWidth=250;

					if (strParent=="FUI_ProjectsClass")
						nWidth=230;


				}
			}

			lstSizes.replace(n,nWidth);					
			pSplitter->setSizes(lstSizes);
			
			
		}
	}
	

	QVBoxLayout *vbox=new QVBoxLayout;
	vbox->setSpacing(0);
	vbox->setMargin(0);
	this->setLayout(vbox);
	

	QFrame *frame=new QFrame;
	QScrollArea* scroll=new QScrollArea;
	scroll->setBackgroundRole(QPalette::Dark);
	scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	scroll->setWidget(frame);
	scroll->setWidgetResizable(true);
	scroll->setVisible(false);

	m_vbox= new QVBoxLayout;
	m_vbox->setSpacing(0);
	m_vbox->setMargin(0);
	frame->setLayout(m_vbox);


	//---------------ADD SUB MENUS-START--------------------------------

	m_menuDropZone= new DropZone_MenuWidget;
	m_vbox->addWidget(m_menuDropZone);
	connect(m_menuDropZone,SIGNAL(NeedNewData(int)),this,SLOT(SubMenuRequestedNewData(int)));

	m_menuDoc= new Doc_MenuWidget;
	m_vbox->addWidget(m_menuDoc);
	connect(m_menuDoc,SIGNAL(NeedNewData(int)),this,SLOT(SubMenuRequestedNewData(int)));

	//m_menuDropZone->SetDocumentDropHandler(m_menuDoc); //set reference for drops target

	m_menuEmail= new Email_MenuWidget;
	m_vbox->addWidget(m_menuEmail);
	connect(m_menuEmail,SIGNAL(NeedNewData(int)),this,SLOT(SubMenuRequestedNewData(int)));

	if (g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
	{
		m_menuCalendar= new Calendar_MenuWidget;
		m_vbox->addWidget(m_menuCalendar);
		connect(m_menuCalendar,SIGNAL(NeedNewData(int)),this,SLOT(SubMenuRequestedNewData(int)));
	}

	m_btnPhone= new QPushButton(this);
	m_btnPhone->setMaximumHeight(36);
	m_btnPhone->setMinimumHeight(36);
	GUI_Helper::CreateStyledButton(m_btnPhone,":Phone32.png",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Phone Call")),15,ThemeManager::GetCEMenuButtonStyle(),10);
	connect(m_btnPhone,SIGNAL(clicked()),this,SLOT(OnPhoneCall()));
	m_vbox->addWidget(m_btnPhone);

	m_btnInternet= new QPushButton(this);
	m_btnInternet->setMaximumHeight(36);
	m_btnInternet->setMinimumHeight(36);
	GUI_Helper::CreateStyledButton(m_btnInternet,":Earth32.png",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Internet")),15,ThemeManager::GetCEMenuButtonStyle(),10);
	connect(m_btnInternet,SIGNAL(clicked()),this,SLOT(OnInternet()));
	m_vbox->addWidget(m_btnInternet);






	//---------------ADD SUB MENUS-END----------------------------------

	m_vbox->addStretch(1);
	vbox->addWidget(scroll);
	scroll->setVisible(true);

	//load settings:
	int nHiddenSections=g_pSettings->GetPersonSetting(APPEAR_CE_MENU_SECTIONS).toInt();
	if (nHiddenSections & 0x01)
		m_menuDropZone->SetHidden();

	if (nHiddenSections & 0x02)
		m_menuDoc->SetHidden();

	if (nHiddenSections & 0x04)
		m_menuEmail->SetHidden();

	if (nHiddenSections & 0x08 && m_menuCalendar)
		m_menuCalendar->SetHidden();

	g_ChangeManager.registerObserver(this);
}

CommunicationMenu::~CommunicationMenu()
{
	//save settings:
	int nHiddenSections=0;
	if (m_menuDropZone->IsHidden())
		nHiddenSections =nHiddenSections | 0x01;
	if (m_menuDoc->IsHidden())
		nHiddenSections =nHiddenSections | 0x02;
	if (m_menuEmail->IsHidden())
		nHiddenSections =nHiddenSections | 0x04;
	if (m_menuCalendar)
		if (m_menuCalendar->IsHidden())
			nHiddenSections =nHiddenSections | 0x08;

	g_pSettings->SetPersonSetting(APPEAR_CE_MENU_SECTIONS,nHiddenSections);
}



void CommunicationMenu::SetFUIParent(int nFUI)
{
	m_menuDropZone->SetFUIParent(nFUI);

}


void CommunicationMenu::OnThemeChanged()
{
	if (m_btnPhone)m_btnPhone->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	if (m_btnInternet)m_btnInternet->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
}


void CommunicationMenu::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
	}

}


//issue 2246: create shortcuts:
bool CommunicationMenu::ProcessKeyPressEventFromMainWindow ( QKeyEvent * event ) 
{
	//if(!g_pClientManager->IsLogged())return false;

	switch(event->key())
	{
	
	case Qt::Key_F5: //CTRL-F5: New Phone Call
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier)
			{
				OnPhoneCall();
			}
			return true;
		}
		break;
	case Qt::Key_F6://CTRL-F6: New Email (without template)
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) //ALT-F5: Open Tabs Details + Description
			{
				m_menuEmail->OnMenuButton();
			}
			return true;
		}
		break;
	case Qt::Key_F7: //CTRL-F7: Register a new document with file dialog (DocMan/Register/Register File)
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) //ALT-F5: Open Tabs Details + Description
			{
				m_menuDoc->OnRegister();
			}
			return true;
		}
		break;
	case Qt::Key_F8: //CTRL-F8: New Calendar Entry (opens wizard)
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) //ALT-F5: Open Tabs Details + Description
			{
				if (m_menuCalendar)
					m_menuCalendar->OnMenuButton();
			}
			return true;
		}
		break;
	case Qt::Key_F9: //CTRL-F9: New Chat
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) //ALT-F5: Open Tabs Details + Description
			{
				OnChat();
			}
			return true;
		}
		break;
	case Qt::Key_F10: //CTRL-F10: New SMS
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) //ALT-F5: Open Tabs Details + Description
			{
				OnSMS();
			}
			return true;
		}
		break;
	case Qt::Key_F11: //CTRL-F11: New Website
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) //ALT-F5: Open Tabs Details + Description
			{
				OnInternet();
			}
			return true;
		}
		break;
	case Qt::Key_F12: //CTRL-F12: Open Application tab in Document Manager (in menu)
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) //ALT-F5: Open Tabs Details + Description
			{
				m_menuDoc->SetCurrentTab();
			}
			return true;
		}
		break;

	}
	
	return false;
}

