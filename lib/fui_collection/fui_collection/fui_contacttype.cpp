#include "fui_contacttype.h"
#include "maincommandbar.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "gui_core/gui_core/macros.h"

//GLOBALS:
#include "common/common/cliententitycache.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern ClientEntityCache g_ClientCache;				//global client cache
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight					*g_AccessRight;				//global access right tester
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;


FUI_ContactType::FUI_ContactType(QWidget *parent)
: FuiBase(parent)
{
	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);
	ui.frameSelection->Initialize(ENTITY_BUS_CM_TYPES_FUI_SELECTOR);  //special
	InitFui(ENTITY_BUS_CM_TYPES,BUS_CM_TYPES,dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);

	//disable button
	dynamic_cast<MainCommandBar *>(m_pCmdBar)->GetButton(MainCommandBar::BUTTON_INSERT)->setVisible(false);
	dynamic_cast<MainCommandBar *>(m_pCmdBar)->GetButton(MainCommandBar::BUTTON_DELETE)->setVisible(false);
	dynamic_cast<MainCommandBar *>(m_pCmdBar)->GetButton(MainCommandBar::BUTTON_COPY)->setVisible(false);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	ui.tableWidget->Initialize(&m_lstData,ui.frameToolBar);
	SetMode(MODE_EMPTY);
}

FUI_ContactType::~FUI_ContactType()
{

}

void FUI_ContactType::RefreshDisplay()
{
	ui.tableWidget->RefreshDisplay();
}

void FUI_ContactType::SetMode(int nMode)
{

	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameSelection->setEnabled(true);
		ui.tableWidget->SetEditMode(false);
	}
	else
	{
		ui.frameSelection->setEnabled(false);
		ui.tableWidget->SetEditMode(true);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}

void FUI_ContactType::on_selectionChange(int nEntityRecordID)
{
	DbRecordSet lstSetup;
	ui.tableWidget->GetColumnSetup(lstSetup);
	ui.tableWidget->setUpdatesEnabled(false);
	
	//add format &/or system table:
	if(nEntityRecordID==ContactTypeManager::TYPE_PHONE)
		ui.tableWidget->SetEntityType(nEntityRecordID,true,true);
	else if(nEntityRecordID==ContactTypeManager::TYPE_LANGUAGE)
		ui.tableWidget->SetEntityType(nEntityRecordID,false,true,true);
	else 	if(	   nEntityRecordID==ContactTypeManager::TYPE_ADDRESS 
		|| nEntityRecordID==ContactTypeManager::TYPE_EMAIL_ADDRESS
		|| nEntityRecordID==ContactTypeManager::TYPE_PHONE)
	{
		ui.tableWidget->SetEntityType(nEntityRecordID,false,true);
	}
	else
	{
		ui.tableWidget->SetEntityType(nEntityRecordID,false,false);
	}
	
	ui.tableWidget->setUpdatesEnabled(true);

	FuiBase::on_selectionChange(nEntityRecordID);
}


QString FUI_ContactType::GetFUIName()
{
	if(m_nEntityRecordID>0)
		return tr("Types: ")+ui.frameSelection->GetCalculatedName(m_nEntityRecordID);
	else
		return tr("Types");

}

bool FUI_ContactType::on_cmdEdit()
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return false;
	}
	return FuiBase::on_cmdEdit();

}

bool FUI_ContactType::on_cmdDelete()
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return false;
	}

	return FuiBase::on_cmdDelete();

}

void FUI_ContactType::on_cmdInsert()
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_ALL_TYPES_AND_ADDRESS_SCHEMAS_IN_ORG_MENU))
	{
		QMessageBox::warning(this,tr("Warning"),tr("You do not have sufficient access rights to perform this operation!"));
		return;
	}

	FuiBase::on_cmdInsert();
}

void FUI_ContactType::DataRead(Status &err,int nRecordID)
{
	QString strWhere=" WHERE BCMT_ENTITY_TYPE ="+QVariant(nRecordID).toString();
	_SERVER_CALL(ClientSimpleORM->Read(err,m_nTableID,m_lstData,strWhere));
	if (err.IsOK())
		m_lstDataCache=m_lstData;
}

void FUI_ContactType::DataLock(Status &err)
{
	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"BCMT_ID");
	lst.merge(m_lstData);
	DbRecordSet lstStatusRows;
	_SERVER_CALL(ClientSimpleORM->Lock(err,BUS_CM_TYPES,lst,m_strLockedRes, lstStatusRows))
		if (!err.IsOK())
			if (err.getErrorCode()==StatusCodeSet::ERR_SQL_LOCKED_BY_ANOTHER)
			{
				QString strMsg=err.getErrorTextRaw();
				int nPos=strMsg.indexOf(" - ");
				if (nPos>=0)
				{
					QString strPersCode=strMsg.left(nPos);
					//test if this is moa
					if(strPersCode==g_pClientManager->GetPersonCode())
					{

						int nResult=QMessageBox::question(this,tr("Warning"),tr("This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?"),tr("Yes"),tr("No"));
						if (nResult==0)
						{
							//unlock record on server:
							Status err_temp;
							bool bUnlocked=false;
							_SERVER_CALL(ClientSimpleORM->UnLockByRecordID(err_temp,BUS_CM_TYPES,lst,bUnlocked))
								if (!err_temp.IsOK() || !bUnlocked)
								{
									return; //return with old lock error
								}
								else
								{
									err.setError(0); //clear error and return
									_SERVER_CALL(ClientSimpleORM->Lock(err,BUS_CM_TYPES,lst,m_strLockedRes, lstStatusRows))
										return;
								}

						}

					}
				}
			}
}

void FUI_ContactType::DataCheckBeforeWrite(Status &err)
{
	ui.tableWidget->CheckDataBeforeWrite(err);
}

//write & send msg
void FUI_ContactType::DataWrite(Status &err)
{
	_SERVER_CALL(ClientSimpleORM->WriteParentSubData(err,BUS_CM_TYPES,m_lstData,m_nEntityRecordID,"BCMT_ENTITY_TYPE",m_strLockedRes));
	if (err.IsOK())
		m_lstDataCache=m_lstData;
}

bool FUI_ContactType::on_cmdOK()
{
	Status err;
	DataCheckBeforeWrite(err);
	_CHK_ERR_RET_BOOL_ON_FAIL(err);

	m_bBlockCloseEvent=true;

	//before write, if ACO pattern then write down ORG ID:
	if(m_pACOHandler!=NULL)
		m_pACOHandler->SaveActualOrganizationID();

	DataWrite(err);
	if (!err.IsOK())
	{
		m_bBlockCloseEvent=false;
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}
	if (m_lstData.getRowCount()>0)
	{	
		notifyCache_AfterWrite(m_lstData);
		//m_nEntityRecordID=m_lstData.getDataRef(0,m_TableData.m_strPrimaryKey).toInt();
		//if(m_pSelectionController)m_pSelectionController->SetCurrentEntityRecord(m_nEntityRecordID,!m_bSideBarModeView); //in sidebar mode notify observers: issue 1223
	}

	SetMode(MODE_READ);
	m_bBlockCloseEvent=false;
	return true;
}