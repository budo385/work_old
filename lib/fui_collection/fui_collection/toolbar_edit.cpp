#include "toolbar_edit.h"

toolbar_edit::toolbar_edit(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);

	//ui.btnAdd->setText(tr("Add"));
	ui.btnAdd->setIcon(QIcon(":handwrite.png"));
	ui.btnSelect->setIcon(QIcon(":SAP_Select.png"));
	ui.btnModify->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));
	
	ui.btnModify->setToolTip(tr("Modify/Create"));
	ui.btnSelect->setToolTip(tr("Select"));
	ui.btnRemove->setToolTip(tr("Delete"));
	ui.btnAdd->setToolTip(tr("Add"));
}

toolbar_edit::~toolbar_edit()
{

}
