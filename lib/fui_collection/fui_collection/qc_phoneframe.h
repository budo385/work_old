#ifndef QC_PHONEFRAME_H
#define QC_PHONEFRAME_H

#include <QWidget>
#include <QLineEdit>
#include "ui_qc_phoneframe.h"
#include "common/common/dbrecordset.h"

class QC_PhoneFrame : public QWidget
{
	Q_OBJECT

public:
	QC_PhoneFrame(QWidget *parent = 0);
	~QC_PhoneFrame();

	void Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType = 0 /*0-Large,1-small widget*/);
	void AddPhone(DbRecordSet recNewPhone);

private:
	Ui::QC_PhoneFrameClass ui;

	void SetupGUIwidgets(DbRecordSet recData, bool bFromAddPhone = false);
	void SetDataToRecordSet(int nRow, QVariant nSystemType, QString strValue);
	void SetIntDataToRecordSet(int nRow, QVariant nSystemType, int nValue);
	void FormatPhoneDialogHandler(int nPhoneType, QLineEdit *p_phoneLineEdit);

	DbRecordSet *m_recFullContactData;
	DbRecordSet m_recLocalEntityRecordSet;
	int			m_nCurrentRow;
	int			m_nParentType;
	QWidget		*m_pParentWidget;
	QButtonGroup *m_pCheckButtonGroup;

private slots:
	void on_buscentral_phone_lineEdit_editingFinished();
	void on_busdirectl_phone_lineEdit_editingFinished();
	void on_busmobilel_phone_lineEdit_editingFinished();
	void on_fax_phone_lineEdit_editingFinished();
	void on_private_phone_lineEdit_editingFinished();
	void on_privatemobile_phone_lineEdit_editingFinished();
	void on_skypeusername_phone_lineEdit_editingFinished();

	void on_addSelected_toolButton_clicked();
	void on_addSelected_toolButton_2_clicked();
	void on_addSelected_toolButton_5_clicked();
	void on_addSelected_toolButton_3_clicked();
	void on_addSelected_toolButton_6_clicked();
	void on_addSelected_toolButton_7_clicked();
	void on_addSelected_toolButton_8_clicked();

	void on_formatPhone_toolButton_clicked();
	void on_formatPhone_toolButton_2_clicked();
	void on_formatPhone_toolButton_3_clicked();
	void on_formatPhone_toolButton_4_clicked();
	void on_formatPhone_toolButton_5_clicked();
	void on_formatPhone_toolButton_6_clicked();

	void on_m_pCheckButtonGroup_buttonClicked(int id);
};

#endif // QC_PHONEFRAME_H
