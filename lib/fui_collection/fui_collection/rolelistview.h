#ifndef ROLELISTVIEW_H
#define ROLELISTVIEW_H

#include <QListView>
#include <QHeaderView>
#include <QDragLeaveEvent>
#include <QDebug>

#include "gui_core/gui_core/rolelistmodel.h"
#include "gui_core/gui_core/dbrecordsetitem.h"

/*!
\class  RoleListView
\ingroup FUICore/AccessRights
\brief  Role list view widget. 

Role list view widget, sub classed from QListView for easier control of drag/drop.
*/
class RoleListView : public QListView
{
	Q_OBJECT

public:
    RoleListView(QWidget *parent = 0);
    ~RoleListView();

};

#endif // ROLELISTVIEW_H
