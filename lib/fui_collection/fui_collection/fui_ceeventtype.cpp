#include "fui_ceeventtype.h"
#include "bus_core/bus_core/systemevents.h"
#include "gui_core/gui_core/richeditordlg.h"
#include "common/common/entity_id_collection.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "gui_core/gui_core/guifield_combobox.h"

extern BusinessServiceManager *g_pBoSet;
static int CommEntityTypeFromComboIdx(int nCboIdx);

FUI_CeEventType::FUI_CeEventType(QWidget *parent)
    : FuiBase(parent)
{
	//--------------------------------------------------
	//		INIT FUI
	//--------------------------------------------------
	ui.setupUi(this);
	InitFui(ENTITY_CE_EVENT_TYPES, CE_EVENT_TYPE, dynamic_cast<MainEntitySelectionController*>(ui.frameSelection),ui.btnButtonBar); //FUI base
	ui.frameSelection->Initialize(ENTITY_CE_EVENT_TYPES);	//init selection controller
	ui.btnButtonBar->RegisterFUI(this);
	InitializeFUIWidget(ui.splitter);

	//--------------------------------------------------
	//		CONNECT FIELDS -> WIDGETS
	//--------------------------------------------------
	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData,DbSqlTableView::TVIEW_CE_EVENT_TYPE);
	m_GuiFieldManagerMain->RegisterField("CEVT_CODE",ui.txtCode);
	m_GuiFieldManagerMain->RegisterField("CEVT_NAME",ui.txtName);
	m_GuiFieldManagerMain->RegisterField("CEVT_DESCRIPTION",ui.txtDescription);

	//priority
	QList<QRadioButton*> lstRadioButons;
	QList<QVariant> lstValues;
	lstRadioButons << ui.radioHigh << ui.radioMedium << ui.radioLow;
	lstValues << 0 << 1 << 2;
	m_GuiFieldManagerMain->RegisterField("CEVT_PRIORITY",new GuiField_RadioButton("CEVT_PRIORITY",lstRadioButons,lstValues,&m_lstData,&DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_EVENT_TYPE)));

	//direction
	lstRadioButons.clear();
	lstValues.clear();
	lstRadioButons << ui.radioNone << ui.radioOutgoing << ui.radioIncoming;
	lstValues << 0 << 1 << 2;
	m_GuiFieldManagerMain->RegisterField("CEVT_DIRECTION",new GuiField_RadioButton("CEVT_DIRECTION",lstRadioButons,lstValues,&m_lstData,&DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_EVENT_TYPE)));

	ui.txtName->setFocus();

	ui.cboRootType->addItem(tr("Document types"));
	ui.cboRootType->addItem(tr("Email types"));
	ui.cboRootType->addItem(tr("Phone Call types"));

	connect(ui.cboRootType, SIGNAL(currentIndexChanged(int)), this, SLOT(OnRootTypeComboChanged(int)));

	//initialize combo box data
	m_lstEvents << SE_INFO(ST_DOCUMENTS, SE_DOCUMENTS_CREATED,	"DC", tr("Document Created"));
	m_lstEvents << SE_INFO(ST_DOCUMENTS, SE_DOCUMENTS_OPENED,	"DO", tr("Document Opened"));
	m_lstEvents << SE_INFO(ST_DOCUMENTS, SE_DOCUMENTS_SENT,		"DS", tr("Document Sent"));
	m_lstEvents << SE_INFO(ST_DOCUMENTS, SE_DOCUMENTS_RECEIVED,	"DR", tr("Document Received"));
	m_lstEvents << SE_INFO(ST_VOICECALLS, SE_VOICECALLS_INC_ANSWERED,			"ICA",	tr("Incoming Call Answered"));
	m_lstEvents << SE_INFO(ST_VOICECALLS, SE_VOICECALLS_OUT_INITIATED,			"ICI",	tr("Incoming Call Initiated"));
	m_lstEvents << SE_INFO(ST_VOICECALLS, SE_VOICECALLS_MISSED,					"CM",	tr("Call Missed"));
	m_lstEvents << SE_INFO(ST_VOICECALLS, SE_VOICECALLS_INC_ANSWERED_BY_OTHER,	"ICASE",tr("Incoming Call Answered By Someone Else"));
	m_lstEvents << SE_INFO(ST_VOICECALLS, SE_VOICECALLS_CALL_BACK_REQUIRED,		"CBR",  tr("Call Back Required"));
	m_lstEvents << SE_INFO(ST_VOICECALLS, SE_VOICECALLS_CALL_BACK_DONE,			"CBD",  tr("Call Back Done"));
	m_lstEvents << SE_INFO(ST_VOICECALLS, SE_VOICECALLS_OUT_NO_ANSWER,			"OCNA", tr("Outgoing Call No Answer"));
	m_lstEvents << SE_INFO(ST_VOICECALLS, SE_VOICECALLS_OUT_PARTNER_NOT_REACHED,"OCPNR",tr("Outgoing Call Partner Not Reached"));
	m_lstEvents << SE_INFO(ST_EMAILS, SE_EMAILS_RECEIVED,	"ER",	tr("Email Received"));
	m_lstEvents << SE_INFO(ST_EMAILS, SE_EMAILS_SENT,		"ES",	tr("Email Sent"));
	m_lstEvents << SE_INFO(ST_EMAILS, SE_EMAILS_FORWARDED,	"EF",	tr("Email Forwarded"));
	m_lstEvents << SE_INFO(ST_EMAILS, SE_EMAILS_ANSWERED,	"EA",	tr("Email Answered"));


	//register combo box:
	m_lstEventsForType.destroy();
	m_lstEventsForType.addColumn(QVariant::Int,"ID");
	m_lstEventsForType.addColumn(QVariant::String,"NAME");
	m_GuiFieldManagerMain->RegisterField("CEVT_STATUS_ID",new GuiField_ComboBox("CEVT_STATUS_ID",ui.cboSystemEvent,m_lstEventsForType,"ID","NAME",false,&m_lstData));
	

	m_nCurRootTypeIdx = 0;
	OnRootTypeComboChanged(0);	// set initial filter

	//set edit to false:
	SetMode(MODE_EMPTY);
}

FUI_CeEventType::~FUI_CeEventType()
{
	delete m_GuiFieldManagerMain;
}

//when programatically change data, call this:
void FUI_CeEventType::RefreshDisplay()
{
	//refresh edit controls:
	m_GuiFieldManagerMain->RefreshDisplay();
	//ui.frameSelection->RefreshDisplay();
}

//when changing state, call this:
void FUI_CeEventType::SetMode(int nMode)
{
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		ui.frameSelection->setEnabled(true);
		ui.cboRootType->setEnabled(true);

		ui.frameDetails->setEnabled(false);

		ui.btnButtonBar->SetMode(nMode); //this is necessary to reeanble buttons!
	}
	else
	{
		ui.frameSelection->setEnabled(false);
		ui.cboRootType->setEnabled(false);

		ui.frameDetails->setEnabled(true);

		//Set focus
		ui.txtCode->setFocus(Qt::OtherFocusReason);
	}

	FuiBase::SetMode(nMode);//set mode
	RefreshDisplay();
}

void FUI_CeEventType::on_cmdInsert()
{
	FuiBase::on_cmdInsert();
	ui.txtCode->setFocus();
}

void FUI_CeEventType::OnRootTypeComboChanged(int nIndex)
{
	m_nCurRootTypeIdx = nIndex;

	//refilter the tree
	ui.frameSelection->ClearLocalFilter();
	ui.frameSelection->GetLocalFilter()->SetFilter("CEVT_SYSTEM_TYPE_ID",CommEntityTypeFromComboIdx(m_nCurRootTypeIdx));
	ui.frameSelection->ReloadData();

	//clean the main record
	m_nEntityRecordID = -1;
	ui.txtCode->setText("");
	ui.txtName->setText("");
	ui.txtDescription->SetHtml("");

	//prepare list for combo, based on type:
	m_lstEventsForType.clear();
	int nSize = m_lstEvents.count();
	int nType = CommEntityTypeFromComboIdx(m_nCurRootTypeIdx);
	for(int i=0; i<nSize; i++)
	{
		if(nType == m_lstEvents[i].m_nSystem)
		{
			m_lstEventsForType.addRow();
			m_lstEventsForType.setData(m_lstEventsForType.getRowCount()-1,0,m_lstEvents[i].m_nCode);	//ID to store
			m_lstEventsForType.setData(m_lstEventsForType.getRowCount()-1,1,m_lstEvents[i].m_strName);//NAME to display
		}
	}
	//load combo with list:
	dynamic_cast<GuiField_ComboBox*>(m_GuiFieldManagerMain->GetFieldHandler("CEVT_STATUS_ID"))->LoadList(m_lstEventsForType);
}

int CommEntityTypeFromComboIdx(int nCboIdx)
{
	//map combo index to type ID
	switch(nCboIdx){
		case 0:	return 0;	//document
		case 1:	return 2;	//email
		case 2:	return 1;	//voice call
	}
	Q_ASSERT(false);	//unknown type
	return -1;
}

void FUI_CeEventType::on_btnGenerateDefaultRec_clicked()
{
	//TOFIX
	QMessageBox::information(this, "", "TODO!");
}

bool FUI_CeEventType::on_cmdOK()
{
	bool bInsertMode = (MODE_INSERT == m_nMode);
	m_lstData.setData(0,"CEVT_SYSTEM_TYPE_ID", CommEntityTypeFromComboIdx(m_nCurRootTypeIdx));
	return FuiBase::on_cmdOK();
}

//get FUI name
QString FUI_CeEventType::GetFUIName()
{
	if(!ui.txtName->text().isEmpty())
		return tr("Communication Event Types: ") + ui.txtName->text();
	else
		return tr("Communication Event Types");
}


void FUI_CeEventType::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	if(m_lstData.getDataRef(0,"CEVT_CODE").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Code is mandatory field!"));	return;	}

	if(m_lstData.getDataRef(0,"CEVT_NAME").toString().isEmpty())
	{err.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Name is mandatory field!"));	return;	}
}