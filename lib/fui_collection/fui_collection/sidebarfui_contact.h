#ifndef SIDEBARFUI_CONTACT_H
#define SIDEBARFUI_CONTACT_H

#include "sidebarfui_base.h"
#include "generatedfiles/ui_sidebarfui_contact.h"
#include "common/common/observer_ptrn.h"
#include "gui_core/gui_core/styledpushbutton.h"

class SideBarFui_Contact : public SideBarFui_Base, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	SideBarFui_Contact(QWidget *parent = 0);
	~SideBarFui_Contact();

	void SetCurrentFUI(bool bCurrent);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	QWidget* GetSelector(){return m_pFrameContacts;};
	bool HideVisibleFramesIfNeeded(int nFrameOpened /* 0 -ACL, 1 -AC, 2-DP */);
	void ShowActualFrameDetails(int nTabIdx);
	void OpenCurrentContactInNewWindow();
	void FlipSelectorToFavoriteTabOrList();

protected:
	QFrame* GetFrameCommToolBar();
	QFrame* GetFrameCommParent();
	QLabel* GetFrameCommLabel();
	DropZone_MenuWidget* GetDropZone_MenuWidget();
	void GetCurrentDefaults(int nSubMenuType,DbRecordSet *lstContacts,DbRecordSet *lstProjects,QString &strEmail,int &nEmailReplyID,QString &strPhone,QString &strInternet);
	void ShowActualFrame(bool bShow,bool bSkipAutoCollapseOthers=false,bool bSkipInsertSpacer=false);
	void ShowActualList(bool bShow,bool bSkipAutoCollapseOthers=false,bool bSkipInsertSpacer=false);

public slots:
	void OnDetail_CalendarClick();

protected slots:
	void BuildMenu();
	void OnDropZone_NewDataDropped(int nDropType, DbRecordSet lstDroppedData);
	void OnScrollToCurrentItem();
	
private slots:

	void ReloadContactData(bool bReloadOtherData=true);
	void CopyName2Clipboard();

	void OnHideList();
	void OnHideActual();

	void OnList_InsertClick();
	void OnList_ClearClick();
	void OnList_EditClick();
	void OnList_DeleteClick();
	void OnList_ViewClick();
	void OnList_PicClick();
	void OnList_PrintClick();

	void OnDetail_PrintClick();
	void OnDetail_QuickInfoClick();
	void OnDetail_CommGridClick();
	void OnDetail_NMRXUsersClick();
	void OnDetail_NMRXProjectsClick();
	void OnDetail_GroupsClick();

	//void OnDetail_CalendarClick();
	void OnCustomContextMenu(const QPoint &);
	void on_cmbInfo_Address_currentIndexChanged(int nIndex);
	void OnSetFindFocusDelayed();
	void OnFavoriteChanged(int);
	//btns:
	void on_btnInternet_clicked();
	void on_btnAddress_clicked();
	void on_btnEmail_clicked();
	void on_btnPhone_clicked();
	//groups:
	void OnGroupSAPNE_Add(DbRecordSet &);
	void OnGroupSAPNE_Remove(DbRecordSet &);
	void OnGroupSAPNE_Edit();

private:
	void SetActualText(QString strText);
	void RefreshQuickInfo();
	void RefreshActualName();
	void OnSelector_DoubleClick(int nRecordID, bool bSourceList=true);
	void OnSelector_ShowContactDetails(bool bNewWindow=false);
	void OnThemeChanged();
	QLabel *m_pTxt1;
	QLabel *m_pTxt2;

	DbRecordSet m_RowContactData;
	DbRecordSet m_rowContactPicture;
	bool m_bShowPicture;

	Ui::SideBarFui_ContactClass ui;
	StyledPushButton *m_btnHideList;
	StyledPushButton *m_btnHideActual;
	StyledPushButton *m_pBtnCalendar;
	QLabel *m_pTxtActual;
	bool m_bHideContactPaneOnDetailClickFirstTime;
	QAction *m_pCntxReload;
	QAction *m_pCntxCopy;
	QLayoutItem *m_Spacer;
	bool m_bCommGridInit;
	bool m_bDetailFrameInit;
	Selection_Contacts *m_pFrameContacts;
	
};

#endif // SIDEBARFUI_CONTACT_H
