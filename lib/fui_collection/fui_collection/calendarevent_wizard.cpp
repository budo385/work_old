#include "calendarevent_wizard.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "common/common/datahelper.h"

#include "fui_collection/fui_collection/fui_calendar.h"
#include "fui_collection/fui_collection/fuimanager.h"
extern FuiManager g_objFuiManager;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;	


#define  STYLE_HELP_WORDS  "QLabel {font-weight:400;font-style:italic;font-family:Arial; font-size:11pt; color:blue}"

CalendarEvent_Wizard::CalendarEvent_Wizard(QWidget *parent)
	: QStackedWidget(parent)
{
	ui.setupUi(this);

	m_lstDataCalEvent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
	m_lstDataCalEventPart.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));
	m_lstDataCalEventOption.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_OPTIONS_SELECT));
	m_lstDataCalEventBreaks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_BREAKS));

	m_GuiFieldManagerOption =new GuiFieldManager(&m_lstDataCalEventOption);
	//m_GuiFieldManagerPart=new GuiFieldManager(&m_lstDataCalEventPart);

	//------------------PAGE1:
	ui.labelHelp1->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp2->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp3->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp4->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp2_1->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp3_1->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp3_2->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp4_1->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp4_2->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp5_1->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp5_2->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp6_1->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp7_1->setStyleSheet(STYLE_HELP_WORDS);
	ui.labelHelp8_1->setStyleSheet(STYLE_HELP_WORDS);
	
	ui.labelHelpPic->setPixmap(QPixmap(":Select16.png"));

	ui.frameTemplate->Initialize(ENTITY_CALENDAR_EVENT_TEMPLATES,&m_lstDataCalEvent,"BCEV_TEMPLATE_ID");
	//ui.frameTemplate->GetButton(Selection_SAPNE::BUTTON_MODIFY)->setVisible(false);
	//ui.frameTemplate->GetButton(Selection_SAPNE::BUTTON_ADD)->setVisible(false);
	ui.frameTemplate->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setVisible(false);
	//ui.frameTemplate->GetButton(Selection_SAPNE::BUTTON_VIEW)->setVisible(false);
	ui.frameTemplate->SetEditMode(true);
	ui.frameTemplate->registerObserver(this);

	ui.frameType->Initialize( ENTITY_CE_TYPES, &m_lstDataCalEventPart,"CENT_CE_TYPE_ID");
	ui.frameType->GetSelectionController()->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);
	ui.frameType->SetEditMode(true);

	ui.frameShowTimeAs->Initialize(ENTITY_BUS_CM_TYPES); //,&m_lstDataCalEventPart,"BCEP_PRESENCE_TYPE_ID",false);
	ui.frameShowTimeAs->GetSelectionController()->GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_CALENDAR_PRESENCE);
	ui.frameShowTimeAs->GetSelectionController()->ReloadData(); //activate cache
	ui.frameShowTimeAs->SetEditMode(true);
	if(!g_AccessRight->TestAR(MODIFY_CALENDAR_SHOW_TIME_AS))
		ui.frameShowTimeAs->DisableEntryText();

	m_GuiFieldManagerOption->RegisterField("BCOL_LOCATION",ui.txtLocation);
	m_GuiFieldManagerOption->RegisterField("BCOL_DESCRIPTION",ui.frameEditor);

	//m_GuiFieldManagerPart->RegisterField("BCEP_DESCRIPTION",ui.frameEditor->GetTextWidget());

	//------------------NMRXs:
	QList<int> lst;
	lst<<NMRXManager::PAIR_CALENDAR_PERSON<<NMRXManager::PAIR_CALENDAR_CONTACT;
	//lst<<NMRXManager::PAIR_CALENDAR_CONTACT;
	ui.widgetNMRXPerson->Initialize(tr("Assigned Contacts"),CE_COMM_ENTITY,lst,BUS_CM_CONTACT,BUS_PERSON,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART,-1,-1,BUS_CAL_INVITE);
	ui.widgetNMRXPerson->DisableRoleSwitch();
	ui.widgetNMRXPerson->SetReverseMode(false);
	ui.widgetNMRXPerson->SetLazyWriteMode();
	ui.widgetNMRXPerson->SetForbidDuplicateAssignments();
	ui.widgetNMRXPerson->SetUsePersonAssignAsContact();
	connect(ui.widgetNMRXPerson,SIGNAL(OpenCalendar(int,DbRecordSet,int,DbRecordSet)),this,SLOT(OnOpenCalendar_Person(int,DbRecordSet,int,DbRecordSet)));

	lst.clear();
	lst<<NMRXManager::PAIR_CALENDAR_PROJECT;
	ui.widgetNMRXProject->Initialize(tr("Assigned Projects"),CE_COMM_ENTITY,lst,BUS_PROJECT,-1,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);
	ui.widgetNMRXProject->DisableRoleSwitch();
	ui.widgetNMRXProject->SetReverseMode(false);
	ui.widgetNMRXProject->SetLazyWriteMode();
	ui.widgetNMRXProject->SetForbidDuplicateAssignments();
	connect(ui.widgetNMRXProject,SIGNAL(OpenCalendar(int,DbRecordSet,int,DbRecordSet)),this,SLOT(OnOpenCalendar_Project(int,DbRecordSet,int,DbRecordSet)));

	lst.clear();
	lst<<NMRXManager::PAIR_CALENDAR_RESOURCE;
	ui.widgetNMRXResources->Initialize(tr("Reserved Resources"),CE_COMM_ENTITY,lst,BUS_CAL_RESOURCE,-1,GlobalConstants::CE_TYPE_CALENDAR_EVENT_PART);
	ui.widgetNMRXResources->DisableRoleSwitch();
	ui.widgetNMRXResources->SetReverseMode(false);
	ui.widgetNMRXResources->SetLazyWriteMode();
	ui.widgetNMRXResources->SetForbidDuplicateAssignments();
	connect(ui.widgetNMRXResources,SIGNAL(OpenCalendar(int,DbRecordSet,int,DbRecordSet)),this,SLOT(OnOpenCalendar_Resources(int,DbRecordSet,int,DbRecordSet)));

	//------------------breaks:
	ui.tableBreaks->Initialize(&m_lstDataCalEventBreaks,ui.frameBreakToolbar);
	ui.tableBreaks->SetEditMode(true);
	connect(ui.tableBreaks,SIGNAL(SignalRowInserted(int)),this,SLOT(OnBreakRowInserted(int)));

	//------------------tasks:
	ui.frameTask->SetDefaultTaskType(GlobalConstants::TASK_TYPE_SCHEDULED_CALENDAR);
	ui.frameTask->SetEnabled(true);

}

CalendarEvent_Wizard::~CalendarEvent_Wizard()
{

}


void CalendarEvent_Wizard::SetData(DbRecordSet &lstDataCalEvent,DbRecordSet &lstDataCalEventPart,DbRecordSet &lstDataCalEventOption, DbRecordSet &lstNmrxPersonContact, DbRecordSet &lstNmrxProjects,DbRecordSet &lstNmrxResources, DbRecordSet &lstTask)
{
	m_lstDataCalEvent=lstDataCalEvent;
	m_lstDataCalEventPart=lstDataCalEventPart;
	m_lstDataCalEventOption=lstDataCalEventOption;

	QString strName=tr("Calendar Event");
	if (lstDataCalEvent.getRowCount()>0)
		strName=lstDataCalEvent.getDataRef(0,"BCEV_TITLE").toString();

	int nCentID=GetPartCommEntityID();
	ui.widgetNMRXPerson->Reload(strName,nCentID,true,&lstNmrxPersonContact);
	ui.widgetNMRXProject->Reload(strName,nCentID,true,&lstNmrxProjects);
	ui.widgetNMRXResources->Reload(strName,nCentID,true,&lstNmrxResources);
	
	ui.frameTask->m_recTask=lstTask;
	if (lstTask.getRowCount()==0)
		ui.frameTask->m_recTask.addRow(); //BT: !??? stupid task interface, no1 knows how it is working
	
	//show time as:
	if (lstDataCalEventPart.getRowCount()>0)
		ui.frameShowTimeAs->SetCurrentEntityRecord(lstDataCalEventPart.getDataRef(0, "BCEP_PRESENCE_TYPE_ID").toInt());
	else
		ui.frameShowTimeAs->Clear();

	//breaks/options:
	m_lstDataCalEventBreaks=m_lstDataCalEventOption.getDataRef(0,"BREAK_LIST").value<DbRecordSet>();
	if (m_lstDataCalEventBreaks.getColumnCount()==0)
		m_lstDataCalEventBreaks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_BREAKS));

	//m_lstDataCalEvent.Dump();
	RefreshDisplay();
}
void CalendarEvent_Wizard::GetData(DbRecordSet &lstDataCalEvent,DbRecordSet &lstDataCalEventPart,DbRecordSet &lstDataCalEventOption, DbRecordSet &lstNmrxPersonContact, DbRecordSet &lstNmrxProjects,DbRecordSet &lstNmrxResources, DbRecordSet &lstTask)
{
	//show time as:
	QVariant intNull(QVariant::Int);
	int nIDShowTimeAs;
	DbRecordSet rowShowTmeAs;
	if(ui.frameShowTimeAs->GetCurrentEntityRecord(nIDShowTimeAs,rowShowTmeAs))
		m_lstDataCalEventPart.setData(0, "BCEP_PRESENCE_TYPE_ID", nIDShowTimeAs);
	else
		m_lstDataCalEventPart.setData(0, "BCEP_PRESENCE_TYPE_ID", intNull);

	//nmrx:
	ui.widgetNMRXPerson->GetData(lstNmrxPersonContact);
	ui.widgetNMRXProject->GetData(lstNmrxProjects);
	ui.widgetNMRXResources->GetData(lstNmrxResources);

	//break:
	ui.tableBreaks->StoreCustomWidgetChanges();
	m_lstDataCalEventOption.setData(0,"BREAK_LIST",m_lstDataCalEventBreaks);

	//task:
	ui.frameTask->GetDataFromGUI();
	lstTask=ui.frameTask->m_recTask;

	lstDataCalEvent=m_lstDataCalEvent;
	lstDataCalEventPart=m_lstDataCalEventPart;
	lstDataCalEventOption=m_lstDataCalEventOption;
}


void CalendarEvent_Wizard::RefreshDisplay()
{
	ui.frameTemplate->RefreshDisplay();
	ui.frameType->RefreshDisplay();
	ui.frameShowTimeAs->RefreshDisplay();
	m_GuiFieldManagerOption->RefreshDisplay();
	//m_GuiFieldManagerPart->RefreshDisplay();
	ui.frameTask->SetGUIFromData();
	ui.tableBreaks->RefreshDisplay();
}


int CalendarEvent_Wizard::GetPartCommEntityID()
{
	int nCentID=0;
	if (m_lstDataCalEventPart.getRowCount()>0)
		nCentID=m_lstDataCalEventPart.getDataRef(0,"CENT_ID").toInt();
	return nCentID;
}
int CalendarEvent_Wizard::GetPartID()
{
	int nPartID=0;
	if (m_lstDataCalEventPart.getRowCount()>0)
		nPartID=m_lstDataCalEventPart.getDataRef(0,"BCEP_ID").toInt();
	return nPartID;
}
int CalendarEvent_Wizard::GetMainEventID()
{
	int nCalID=0;
	if (m_lstDataCalEvent.getRowCount()>0)
		nCalID=m_lstDataCalEvent.getDataRef(0,"BCEV_ID").toInt();
	return nCalID;
}


void CalendarEvent_Wizard::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==ui.frameTemplate && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		emit SignalTemplateChanged(nMsgDetail);
	}
}


void CalendarEvent_Wizard::OnOpenCalendar_Person(int,DbRecordSet lstContacts,int,DbRecordSet lstPersons)
{
	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(m_lstDataCalEvent.getDataRef(0,"BCEV_FROM").toDateTime().date(),from, to);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true,false,FuiBase::MODE_INSERT);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		//lstContacts.Dump();
		//lstPersons.Dump();
		pFui->Initialize(from,to,NULL,NULL,&lstContacts);
	}
}
void CalendarEvent_Wizard::OnOpenCalendar_Project(int,DbRecordSet lstProject,int,DbRecordSet)
{
	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(m_lstDataCalEvent.getDataRef(0,"BCEV_FROM").toDateTime().date(),from, to);
	
	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true,false,FuiBase::MODE_INSERT);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->Initialize(from,to,NULL,&lstProject);
	}


}
void CalendarEvent_Wizard::OnOpenCalendar_Resources(int,DbRecordSet lstRes,int,DbRecordSet)
{
	QDate from; 
	QDate to;
	DataHelper::GetCurrentWeek(m_lstDataCalEvent.getDataRef(0,"BCEV_FROM").toDateTime().date(),from, to);

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_CALENDAR,true,false,FuiBase::MODE_INSERT);
	Fui_Calendar* pFui=dynamic_cast<Fui_Calendar*>(g_objFuiManager.GetFUIWidget(nNewFUI));
	if (pFui)
	{
		pFui->Initialize(from,to,NULL,NULL,NULL,&lstRes);
	}


}



void CalendarEvent_Wizard::RefreshTitle(QString strTitle)
{
	ui.widgetNMRXPerson->SetMasterEntityCalcualtedName(strTitle);
	ui.widgetNMRXProject->SetMasterEntityCalcualtedName(strTitle);
	ui.widgetNMRXResources->SetMasterEntityCalcualtedName(strTitle);
}


void CalendarEvent_Wizard::OnBreakRowInserted(int nRow) //by MB: every new break= from= option/break from
{
	if (m_lstDataCalEvent.getRowCount()<1)
		return;

	QDateTime pFrom=m_lstDataCalEventOption.getDataRef(0,"BCOL_FROM").toDateTime();
	QDateTime pTo=m_lstDataCalEventOption.getDataRef(0,"BCOL_TO").toDateTime();
	if (pFrom.isValid() && pTo.isValid())
	{
		int nSeconds=pFrom.secsTo(pTo)/2;
		if(nSeconds>7.5*60)
		{
			nSeconds=nSeconds-7.5*60;
			pFrom=pFrom.addSecs(nSeconds);
			pFrom.setTime(QTime(pFrom.time().hour(),pFrom.time().minute()));
		}
		m_lstDataCalEventBreaks.setData(nRow,"BCBL_FROM",pFrom);
		m_lstDataCalEventBreaks.setData(nRow,"BCBL_TO",pFrom.addSecs(60*15));
	}
	else
	{
		m_lstDataCalEventBreaks.setData(nRow,"BCBL_FROM",QDateTime::currentDateTime());
		m_lstDataCalEventBreaks.setData(nRow,"BCBL_TO",QDateTime::currentDateTime().addSecs(60*15));
	}

}

void CalendarEvent_Wizard::SetCurrentOptionDates(QDateTime from,QDateTime to,bool bOverRide)
{
	int nOptionRow=0;
	if(nOptionRow<m_lstDataCalEventOption.getRowCount())
	{
		QDateTime pFrom=m_lstDataCalEventOption.getDataRef(nOptionRow,"BCOL_FROM").toDateTime();
		QDateTime pTo=m_lstDataCalEventOption.getDataRef(nOptionRow,"BCOL_TO").toDateTime();

		if ((!pFrom.isValid() && !bOverRide) || bOverRide)
		{
			m_lstDataCalEventOption.setData(nOptionRow,"BCOL_FROM",from);
		}
		if ((!pTo.isValid() && !bOverRide) || bOverRide)
		{
			m_lstDataCalEventOption.setData(nOptionRow,"BCOL_TO",to);
		}
	}

}