#ifndef FUI_IMPORTPERSON_H
#define FUI_IMPORTPERSON_H

#include <QWidget>
#include "generatedfiles/ui_fui_importperson.h"
#include "fuibase.h"

class FUI_ImportPerson : public FuiBase
{
    Q_OBJECT

public:
    FUI_ImportPerson(QWidget *parent = 0);
    ~FUI_ImportPerson();

private:
    Ui::FUI_ImportPersonClass ui;
	QString GetFUIName(){return tr("Import Users");};
	void UpdateImportContactsButton();

	DbRecordSet m_lstData;

private slots:
	void on_btnImportContacts_clicked();
	void on_btnBuildList_clicked();
};

#endif // FUI_IMPORTPERSON_H
