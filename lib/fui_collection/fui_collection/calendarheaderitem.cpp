#include "calendarheaderitem.h"
#include <QGraphicsSceneHoverEvent>
#include "calendargraphicsview.h"
#include "calendarmultidaygraphicsview.h"

//#include <QtWidgets/QMessageBox>

CalendarHeaderItem::CalendarHeaderItem(bool bIsInMultiDayCalendar, int nColumn, QString	strString, int nHeight, QGraphicsView *pCalendarGraphicsView, QGraphicsItem * parent /*= 0*/, Qt::WindowFlags wFlags /*= 0*/)
: QGraphicsProxyWidget(parent, wFlags)
{
	m_bIsInMultiDayCalendar = bIsInMultiDayCalendar;
	m_isResizing = false;
	m_pCalendarGraphicsView = pCalendarGraphicsView;
	m_nColumn = nColumn;
	m_nMinWidth = 10;
	m_nHeight = nHeight;
	m_strString = strString;
	
	//QMessageBox::information(NULL, "1", QVariant(m_nWidth).toString());

	m_pLabel = new Label(Label::HEADER_ITEM);
	m_pLabel->setText(m_strString);
	m_pLabel->setAlignment(Qt::AlignCenter);
	m_pLabel->setMinimumHeight(m_nHeight);
	m_pLabel->setMaximumHeight(m_nHeight);

	setAcceptHoverEvents(true);
	setWidget(m_pLabel);
	setZValue(5);
}

CalendarHeaderItem::~CalendarHeaderItem()
{
	m_pCalendarGraphicsView = NULL;
	m_pLabel = NULL;
}

void CalendarHeaderItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget /*= 0*/)
{
	QGraphicsProxyWidget::paint(painter, option, widget);
}

void CalendarHeaderItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
	if (isInResizeArea(event->pos()))
		setCursor(Qt::SplitHCursor);
	else
		setCursor(Qt::ArrowCursor);

	QGraphicsProxyWidget::hoverMoveEvent(event);
}

void CalendarHeaderItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	//QGraphicsProxyWidget::mouseMoveEvent(event);

	if (m_isResizing)
	{
		QPointF mousePos = event->pos();
		QPointF scenePos = mapToScene(mousePos);
		QPointF tablePos;
		if (m_bIsInMultiDayCalendar)
			tablePos = dynamic_cast<CalendarMultiDayGraphicsView*>(m_pCalendarGraphicsView)->getTableView()->mapFromScene(scenePos);
		else
			tablePos = dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->getTableView()->mapFromScene(scenePos);

		m_nWidth = tablePos.x()/(m_nColumn+1);
		//m_nWidth = event->pos().x();
		if (m_nWidth < m_nMinWidth)
			m_nWidth = m_nMinWidth;
		//qDebug() << "width" << m_nWidth;
		resize(m_nWidth, m_nHeight);
		emit columnResized(m_nColumn, m_nWidth, false);
	}
}

void CalendarHeaderItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && isInResizeArea(event->pos()))
		m_isResizing = true;
	else
		QGraphicsProxyWidget::mousePressEvent(event);
}

void CalendarHeaderItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && m_isResizing)
	{
		m_isResizing = false;
		if(m_nWidth<0)
			return;
		emit columnResized(m_nColumn, m_nWidth, true);
	}
	else
		QGraphicsProxyWidget::mouseReleaseEvent(event);
}

void CalendarHeaderItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	return;
}

bool CalendarHeaderItem::isInResizeArea(const QPointF &pos)
{
	int nWidth = this->widget()->width();
	if (((nWidth - 5) < pos.x()) && (pos.x() < nWidth))
		return true;
	else
		return false;
}
