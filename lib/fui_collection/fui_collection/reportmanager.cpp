#include "reportmanager.h"
#include "wizardregistration.h"
#include <QFileDialog>
#include <QPrinter>
#include <QPrintPreviewDialog>
#include <QApplication>
#include <QPrintDialog>
#include <QDesktopWidget>

#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
extern QString g_strLastDir_FileOpen;

ReportManager::ReportManager()
:m_RptScreen(NULL)
{
}

ReportManager::~ReportManager()
{
}

// prints report with predefined data, if omitted then wizard is started //
void ReportManager::PrintReport(int nReportID, QList<DbRecordSet> *lstPageRecordSet, int nDestination)
{
	QList<DbRecordSet> lstPageData;
	//check FP:
	if (!IsReportAllowed(nReportID))
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Report not available, check your license!"));
		return;
	}

	//get data:
	if (lstPageRecordSet==NULL)
	{
		if(!OpenWizard(nReportID,lstPageData,nDestination))
			return;
	}
	else
	{
		lstPageData=*lstPageRecordSet;
		if (nDestination==ReportRegistration::REPORT_DESTINATION_ASK) //if data defined and ask, go here:
		{
			QList<DbRecordSet> lstDest;
			WizardRegistration wizReg(WizardRegistration::REPORT_DESTINATION_WIZARD);
			WizardBase wiz(WizardRegistration::REPORT_DESTINATION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);
			if (wiz.exec())
				wiz.GetWizPageRecordSetList(lstDest);
			else 
				return;
			nDestination = lstDest.at(0).getDataRef(0, 0).toInt();
		}
	}

	//destination & data defined, go print:
	switch(nDestination)
	{
	case ReportRegistration::REPORT_DESTINATION_PRINTER:
		return PrintReport_Printer(nReportID, lstPageData);
		break;
	case ReportRegistration::REPORT_DESTINATION_PREVIEW:
		return PrintReport_Screen(nReportID, lstPageData);
		break;
	case ReportRegistration::REPORT_DESTINATION_PDF:
		return PrintReport_PDF(nReportID, lstPageData);
	    break;
	default:
		Q_ASSERT(false); //wrong destination
	    break;
	}
	
}

void ReportManager::PrintReport_Printer(int nReportID, QList<DbRecordSet> &lstPageRecordSet)
{
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	QPrinter *printer = new QPrinter();
	QPrintDialog print_dialog(printer, NULL);
	if (print_dialog.exec() == QPrintDialog::Accepted)
		printer->setFullPage(true);
	else
	{
		QApplication::restoreOverrideCursor();
		return;
	}

	RptPrinter Rpt;
	ReportPage *RptPageDef=NULL;
	ReportRegistration::GetReportPage(nReportID,&RptPageDef);
	if (!RptPageDef)
	{
		Q_ASSERT(false);
		return;
	}
	if (!Rpt.InitalizeReport(ReportRegistration::GetReportPath(nReportID),nReportID,ReportRegistration::GetReportName(nReportID),RptPageDef,lstPageRecordSet))
		return;
	Rpt.PrintReportToPrinter(printer);
	QApplication::restoreOverrideCursor();
	if (RptPageDef) delete RptPageDef;
}

void ReportManager::PrintReport_Screen(int nReportID, QList<DbRecordSet> &lstPageRecordSet)
{
	m_RptScreen = new RptPrinter;
	ReportPage *RptPageDef=NULL;
	ReportRegistration::GetReportPage(nReportID,&RptPageDef);
	if (!RptPageDef)
	{
		Q_ASSERT(false);
		return;
	}
	if (!m_RptScreen->InitalizeReport(ReportRegistration::GetReportPath(nReportID),nReportID,ReportRegistration::GetReportName(nReportID),RptPageDef,lstPageRecordSet))
		return;

	QPrintPreviewDialog prevDlg(NULL, Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint);
	connect(&prevDlg,SIGNAL(paintRequested ( QPrinter * )),this,SLOT(OnPrintScreen(QPrinter *)));
	prevDlg.setWindowTitle(ReportRegistration::GetReportName(nReportID));
	//prevDlg.showMaximized();
	prevDlg.exec();

	delete m_RptScreen;
	if (RptPageDef) delete RptPageDef;
	m_RptScreen=NULL;
}
void ReportManager::PrintReport_PDF(int nReportID, QList<DbRecordSet> &lstPageRecordSet)
{

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	QString strPdfFileName = QFileDialog::getSaveFileName(NULL, "Enter PDF file name", g_strLastDir_FileOpen, "PDF files (*.pdf)");
	if (!strPdfFileName.isEmpty())
	{
		QFileInfo fileInfo(strPdfFileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	if (strPdfFileName.isNull())
	{
		QApplication::restoreOverrideCursor();
		return;
	}
	//Check file extension.
	int extensionIndex = strPdfFileName.lastIndexOf(".");
	QString extension = strPdfFileName;
	extension.remove(0, extensionIndex);
	if (extension != ".pdf")
		strPdfFileName.append(".pdf");

	//PRINT TO PDF.
	RptPrinter Rpt;
	ReportPage *RptPageDef=NULL;
	ReportRegistration::GetReportPage(nReportID,&RptPageDef);
	if (!RptPageDef)
	{
		Q_ASSERT(false);
		return;
	}
	if (!Rpt.InitalizeReport(ReportRegistration::GetReportPath(nReportID),nReportID,ReportRegistration::GetReportName(nReportID),RptPageDef,lstPageRecordSet))
		return;

	QPrinter *printer = new QPrinter();
	printer->setOutputFormat(QPrinter::PdfFormat);
	printer->setFullPage(true);
	printer->setOutputFileName(strPdfFileName);	

	Rpt.PrintReportToPrinter(printer);
	QApplication::restoreOverrideCursor();

	if (RptPageDef) delete RptPageDef;
}


bool ReportManager::OpenWizard(int nReportID,QList<DbRecordSet> &lstPageRecordSet, int &nDestination)
{
	switch(nReportID)
	{
	case ReportRegistration::REPORT_CONTACT_DETAILS:
		{
			WizardRegistration wizReg(WizardRegistration::CONTACT_SELECTION_WIZARD);
			if (nDestination!=ReportRegistration::REPORT_DESTINATION_ASK)
				wizReg.m_hshWizardPageHash[WizardRegistration::CONTACT_SELECTION_WIZARD]->RemoveLastPage();
			WizardBase wiz(WizardRegistration::CONTACT_SELECTION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);

			if (wiz.exec())
				wiz.GetWizPageRecordSetList(lstPageRecordSet);
			else 
				return false;
			if (nDestination==ReportRegistration::REPORT_DESTINATION_ASK)
			{
				nDestination = lstPageRecordSet.at(1).getDataRef(0, 0).toInt();
				lstPageRecordSet.removeAt(1);
			}
		}
		break;
	case ReportRegistration::REPORT_CONTACT_COMMUNICATION_DETAILS:
		{
			WizardRegistration wizReg(WizardRegistration::CONTACT_COMMUNICATION_REPORT_SELECTION_WIZARD);
			if (nDestination!=ReportRegistration::REPORT_DESTINATION_ASK)
				wizReg.m_hshWizardPageHash[WizardRegistration::CONTACT_COMMUNICATION_REPORT_SELECTION_WIZARD]->RemoveLastPage();
			WizardBase wiz(WizardRegistration::CONTACT_COMMUNICATION_REPORT_SELECTION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);

			if (wiz.exec())
				wiz.GetWizPageRecordSetList(lstPageRecordSet);
			else 
				return false;
			if (nDestination==ReportRegistration::REPORT_DESTINATION_ASK)
			{
				nDestination = lstPageRecordSet.at(2).getDataRef(0, 0).toInt();
				lstPageRecordSet.removeAt(2);
			}
		}
		break;
	case ReportRegistration::REPORT_CONTACT_LIST:
		{
			WizardRegistration wizReg(WizardRegistration::CONTACT_SELECTION_WIZARD);
			if (nDestination!=ReportRegistration::REPORT_DESTINATION_ASK)
				wizReg.m_hshWizardPageHash[WizardRegistration::CONTACT_SELECTION_WIZARD]->RemoveLastPage();

			WizardBase wiz(WizardRegistration::CONTACT_SELECTION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);

			if (wiz.exec())
				wiz.GetWizPageRecordSetList(lstPageRecordSet);
			else 
				return false;
			if (nDestination==ReportRegistration::REPORT_DESTINATION_ASK)
			{
				nDestination = lstPageRecordSet.at(1).getDataRef(0, 0).toInt();
				lstPageRecordSet.removeAt(1);
			}
		}
		break;
	case ReportRegistration::REPORT_PROJECT_LIST:
		{
			WizardRegistration wizReg(WizardRegistration::PROJECT_REPORT_SELECTION_WIZARD);
			if (nDestination!=ReportRegistration::REPORT_DESTINATION_ASK)
				wizReg.m_hshWizardPageHash[WizardRegistration::PROJECT_REPORT_SELECTION_WIZARD]->RemoveLastPage();
			WizardBase wiz(WizardRegistration::PROJECT_REPORT_SELECTION_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);

			if (wiz.exec())
				wiz.GetWizPageRecordSetList(lstPageRecordSet);
			else 
				return false;
			if (nDestination==ReportRegistration::REPORT_DESTINATION_ASK)
			{
				nDestination = lstPageRecordSet.at(1).getDataRef(0, 0).toInt();
				lstPageRecordSet.removeAt(1);
			}
		}
		break;
	case ReportRegistration::REPORT_CONTACT_RELATIONS:
		{
			WizardRegistration wizReg(WizardRegistration::REPORT_CONTACT_RELATIONSHIPS);
			if (nDestination!=ReportRegistration::REPORT_DESTINATION_ASK)
				wizReg.m_hshWizardPageHash[WizardRegistration::REPORT_CONTACT_RELATIONSHIPS]->RemoveLastPage();
			WizardBase wiz(WizardRegistration::REPORT_CONTACT_RELATIONSHIPS, wizReg.m_hshWizardPageHash, &g_ClientCache);
			if (wiz.exec())
				wiz.GetWizPageRecordSetList(lstPageRecordSet);
			else
				return false;
			if (nDestination==ReportRegistration::REPORT_DESTINATION_ASK)
			{
				nDestination = lstPageRecordSet.at(1).getDataRef(0, 0).toInt();
				lstPageRecordSet.removeAt(1);
			}
		}
		break;
	default:
		Q_ASSERT(false); //no wizard defined for this report!
		return false;
	}

	return true;


}


bool ReportManager::IsReportAllowed(int nReportID)
{
	if (nReportID ==ReportRegistration::REPORT_CONTACT_DETAILS)
	{	
		return g_FunctionPoint.IsFPAvailable(FP_REPORT__CONTACT_DETAILS);
	}
	if (nReportID ==ReportRegistration::REPORT_CONTACT_COMMUNICATION_DETAILS)
	{	
		return g_FunctionPoint.IsFPAvailable(FP_REPORT__CONTACT_COMMUNICATION_DETAILS);
	}
	if (nReportID ==ReportRegistration::REPORT_PROJECT_LIST)
	{	
		return g_FunctionPoint.IsFPAvailable(FP_REPORT__PROJECT_LIST);
	}
	if (nReportID ==ReportRegistration::REPORT_CONTACT_LIST)
	{	
		return g_FunctionPoint.IsFPAvailable(FP_REPORT__CONTACT_LIST);
	}

	return true;
}







void ReportManager::OnPrintScreen(QPrinter *pPrinter)
{
	if (m_RptScreen)
		m_RptScreen->PrintReportToPrinter(pPrinter);
}


void ReportManager::PrintReportAll()
{
	QList<DbRecordSet> lstPageRecordSet;
	WizardRegistration wizReg(WizardRegistration::REPORT_WIZARD);
	WizardBase wiz(WizardRegistration::REPORT_WIZARD, wizReg.m_hshWizardPageHash, &g_ClientCache);
	if (wiz.exec())
		wiz.GetWizPageRecordSetList(lstPageRecordSet);
	else 
		return;
	int	nDestination = lstPageRecordSet.last().getDataRef(0, 0).toInt();

	QString strReportFileName = lstPageRecordSet.first().getDataRef(0, 0).toString() + ".xml";
	int nRepID=ReportRegistration::GetReportIDFromFile(strReportFileName);
	lstPageRecordSet.removeFirst();
	lstPageRecordSet.removeLast();
	PrintReport(nRepID,&lstPageRecordSet,nDestination);
}
