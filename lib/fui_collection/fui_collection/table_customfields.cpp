#include "table_customfields.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QHeaderView>
#include <QGroupBox>
#include <QPushButton>
#include <QScrollArea>
#include "bus_core/bus_core/globalconstants.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/richeditordlg.h"

Table_CustomFields::Table_CustomFields(QWidget *parent)
	: UniversalTableWidgetEx(parent)
{

}

Table_CustomFields::~Table_CustomFields()
{

}

//pData: TVIEW_BUS_CUSTOM_FIELDS_DATA, for either contact or project list of all possible fields and their selections
//data record is empty (no row) if there is no data...(clear value button)...
void Table_CustomFields::Initialize(DbRecordSet *pData)
{
	int nCellHeight=30;
	DbRecordSet lstColumns;
	AddColumnToSetup(lstColumns,"CUSTOM_DATA",tr("Custom Field"),500,true,"",UniversalTableWidgetEx::COL_TYPE_CUSTOM_WIDGET);
	horizontalHeader()->setStretchLastSection(true);

	//set data source for table
	UniversalTableWidgetEx::Initialize(pData,&lstColumns,false,true,nCellHeight);
	//EnableDrop(true);

	disconnect(this,SIGNAL(cellClicked(int,int)),this,SLOT(OnCellClicked(int,int)));
}

void Table_CustomFields::ResetRecordIDs()
{
	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		//get data for user
		int nDataType = m_plstData->getDataRef(i,"BCF_DATA_TYPE").toInt();
		switch (nDataType)
		{
		case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
			{
				DbRecordSet rowData=m_plstData->getDataRef(i,"REC_BOOL_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCB_RECORD_ID",0);
				m_plstData->setData(i,"REC_BOOL_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_INT:
			{
				DbRecordSet rowData=m_plstData->getDataRef(i,"REC_INT_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCI_RECORD_ID",0);
				m_plstData->setData(i,"REC_INT_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
			{
				DbRecordSet rowData=m_plstData->getDataRef(i,"REC_FLOAT_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCFL_RECORD_ID",0);
				m_plstData->setData(i,"REC_FLOAT_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
			{
				DbRecordSet rowData=m_plstData->getDataRef(i,"REC_TEXT_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCT_RECORD_ID",0);
				m_plstData->setData(i,"REC_TEXT_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
			{
				DbRecordSet rowData=m_plstData->getDataRef(i,"REC_LONGTEXT_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCLT_RECORD_ID",0);
				m_plstData->setData(i,"REC_LONGTEXT_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
			{
				DbRecordSet rowData=m_plstData->getDataRef(i,"REC_DATE_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCD_RECORD_ID",0);
				m_plstData->setData(i,"REC_DATE_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
			{
				DbRecordSet rowData=m_plstData->getDataRef(i,"REC_DATETIME_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCDT_RECORD_ID",0);
				m_plstData->setData(i,"REC_DATETIME_DATA",rowData);
			}

			break;
		}



	}



}


void Table_CustomFields::Data2CustomWidget(int nRow, int nCol)
{
	//now we will translate field type to widgets: 
	//Name Label:
	//x clear button
	//x edit text button (for long text)
	
	//boolean: no selection: checkbox, 'optional: dropdownlist with edit', mandatory dropdownlist with values, mandatory with radio's: lab: one1 lab2: two2
	//when new then insert new record with default values....

	DbRecordSet rowField = m_plstData->getRow(nRow);

	Table_CustomFieldWidget *pWidget = dynamic_cast<Table_CustomFieldWidget *>(cellWidget(nRow,nCol));
	if (!pWidget)
	{
		pWidget				= new Table_CustomFieldWidget(this,nRow,rowField);
		pWidget->setEnabled(m_bEdit);
		pWidget->SetDataToWidget();
		pWidget->SetButtonLongText(m_bEdit);
		connect(pWidget,SIGNAL(DataChanged(int)),this,SLOT(On_CustomWidgetChanged(int)));
		setCellWidget(nRow,nCol,pWidget);
	}
	else
	{
		//test to see if data type is changed, if so then destroy old one and create new one:
		int nDataType		=rowField.getDataRef(0,"BCF_DATA_TYPE").toInt();
		int nSelType		=rowField.getDataRef(0,"BCF_SELECTION_TYPE").toInt();

		//if data structure is changed for some reason then abort all (only happen when grid is reloaded)
		if (nDataType!=pWidget->m_nDataType || nSelType!=pWidget->m_nSelType)
		{
			pWidget->deleteLater();
			pWidget				= new Table_CustomFieldWidget(this,nRow,rowField);
			pWidget->setEnabled(m_bEdit);
			pWidget->SetDataToWidget();
			pWidget->SetButtonLongText(m_bEdit);
			connect(pWidget,SIGNAL(DataChanged(int)),this,SLOT(On_CustomWidgetChanged(int)));
			setCellWidget(nRow,nCol,pWidget);
		}
		else
		{
			pWidget->SetData(rowField);
			pWidget->SetDataToWidget();
		}
	}
		

}

//now get all data:
void Table_CustomFields::CustomWidget2Data(int nRow, int nCol)
{
	Table_CustomFieldWidget *pX_Widget = dynamic_cast<Table_CustomFieldWidget *>(cellWidget(nRow,nCol));
	if (pX_Widget)
	{
		DbRecordSet rowData=pX_Widget->GetDataFromWidget();

		int nDataType = m_plstData->getDataRef(nRow,"BCF_DATA_TYPE").toInt();
		switch (nDataType)
		{
		case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
			{
				m_plstData->setData(nRow,"REC_BOOL_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_INT:
			{
				m_plstData->setData(nRow,"REC_INT_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
			{
				m_plstData->setData(nRow,"REC_FLOAT_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
			{
				m_plstData->setData(nRow,"REC_TEXT_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
			{
				m_plstData->setData(nRow,"REC_LONGTEXT_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
			{
				m_plstData->setData(nRow,"REC_DATE_DATA",rowData);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
			{
				m_plstData->setData(nRow,"REC_DATETIME_DATA",rowData);
			}
			break;
		}

		
	}
}


void Table_CustomFields::On_CustomWidgetChanged(int nRow)
{
	CustomWidget2Data(nRow,0);
}


void Table_CustomFields::SetEditMode(bool bEdit)
{
	UniversalTableWidgetEx::SetEditMode(bEdit);
	
	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		Table_CustomFieldWidget *pWidget = dynamic_cast<Table_CustomFieldWidget *>(cellWidget(i,0));
		if (pWidget)
			pWidget->SetButtonLongText(bEdit);
	}
	
}






/***************************************

WIDGET

****************************************/




Table_CustomFieldWidget::Table_CustomFieldWidget(Table_CustomFields *parent,int nRow,DbRecordSet &recCustomField):
m_pbtnEditLongText(NULL),m_pFldLongText(NULL),m_pFldCheckBox(NULL),m_pFldSelection(NULL),m_pFldLineEdit(NULL),m_pFldDateTime(NULL)
{
	bEnableSignalDataChange=false;
	bTableInEditMode=false;
	m_nRowInTable = nRow;
	setStyleSheet("QLabel {color:white;} QRadioButton {color:white;}");
	
	SetData(recCustomField);

	switch (m_nDataType)
	{
	case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
		{
			CreateBoolField();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_INT:
		{
			CreateIntField();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
		{
			CreateFloatField();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
		{
			CreateCharField();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
		{
			CreateTextField();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
		{
			CreateDateField();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
		{
			CreateDatetimeField();
		}
		break;
	}
	
	//SetDataToWidget();

	bEnableSignalDataChange=true;
}

Table_CustomFieldWidget::~Table_CustomFieldWidget()
{

}

void Table_CustomFieldWidget::CreateBoolField()
{
	QLabel *fldName = new QLabel;
	fldName->setText(m_recCustomField.getDataRef(0,"BCF_NAME").toString());
	fldName->setMinimumWidth(150);

	QVBoxLayout *vboxLeftPart = new QVBoxLayout;
	vboxLeftPart->addWidget(fldName);
	vboxLeftPart->setSpacing(1);
	vboxLeftPart->setContentsMargins(0,0,0,0);
	
	QVBoxLayout *vboxRightPart = new QVBoxLayout;
	vboxLeftPart->setContentsMargins(0,0,0,0);
	
	QHBoxLayout *hbox = new QHBoxLayout;
	
	switch (m_nSelType)
	{
	case GlobalConstants::TYPE_CUSTOM_SELECTION_NONE: //checkbox
		{
			m_pFldCheckBox = new QCheckBox;
			m_pFldCheckBox->setChecked(false);
			connect(m_pFldCheckBox,SIGNAL(stateChanged(int)),this,SLOT(OnStateChanged(int)));
						
			hbox->addWidget(m_pFldCheckBox);
			hbox->addStretch();
			hbox->setSpacing(1);
			hbox->setContentsMargins(0,0,0,0);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY: //dropdownlist
		{
			m_pFldSelection = new QComboBox;
			m_pFldSelection->setEditable(false);
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Maximum);
			m_pFldSelection->setSizePolicy(policy);

			//m_pFldSelection->setStyleSheet()
			connect(m_pFldSelection,SIGNAL(currentIndexChanged(int)),this,SLOT(OnStateChanged(int)));

			//m_lstSelection.sort("BCS_VALUE");
			int nSize=m_lstSelection.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				m_pFldSelection->addItem(m_lstSelection.getDataRef(i,"BCS_VALUE").toString());
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_pFldSelection->setCurrentIndex(nIdx);
				else
					m_pFldSelection->setCurrentIndex(0);
			}

			hbox->addWidget(m_pFldSelection);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON: //radio buttons
		{
			int nSize=m_lstSelection.getRowCount();
			//QGroupBox *group = new QGroupBox;
			for (int i=0;i<nSize;i++)
			{
				QRadioButton *pBtn= new QRadioButton;
				pBtn->setText(m_lstSelection.getDataRef(i,"BCS_RADIO_BUTTON_LABEL").toString());
				hbox->addWidget(pBtn);
				hbox->addSpacing(10);
				m_lstFldRadioButtons.append(pBtn);
				connect(pBtn,SIGNAL(clicked(bool)),this,SLOT(OnStateChangedRadio(bool)));
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_lstFldRadioButtons.at(nIdx)->setChecked(true);
				else
					m_lstFldRadioButtons.at(0)->setChecked(true);
			}

			hbox->addStretch(1);
			vboxRightPart->addStretch();
			vboxRightPart->addLayout(hbox);
			vboxRightPart->addStretch();
		}
		break;
	}

	
	QHBoxLayout *hboxTotal = new QHBoxLayout;
	hboxTotal->addLayout(vboxLeftPart);
	hboxTotal->addLayout(vboxRightPart);
	hboxTotal->setSpacing(1);
	hboxTotal->setContentsMargins(3,0,50,0);
	this->setLayout(hboxTotal);
}

void Table_CustomFieldWidget::CreateCharField()
{
	QLabel *fldName = new QLabel;
	fldName->setText(m_recCustomField.getDataRef(0,"BCF_NAME").toString());
	fldName->setMinimumWidth(150);
	QVBoxLayout *vboxLeftPart = new QVBoxLayout;
	vboxLeftPart->addWidget(fldName);
	vboxLeftPart->setSpacing(1);
	vboxLeftPart->setContentsMargins(0,0,0,0);

	QVBoxLayout *vboxRightPart = new QVBoxLayout;
	vboxLeftPart->setContentsMargins(0,0,0,0);
	
	QHBoxLayout *hbox = new QHBoxLayout;
	

	switch (m_nSelType)
	{
	case GlobalConstants::TYPE_CUSTOM_SELECTION_NONE: //checkbox
		{
			m_pFldLineEdit = new QLineEdit;
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Maximum);
			m_pFldLineEdit->setSizePolicy(policy);
			connect(m_pFldLineEdit,SIGNAL(textChanged(const QString &)),this,SLOT(OnStateChangedText(const QString &)));
						
			hbox->addWidget(m_pFldLineEdit);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL: //dropdownlist
		{
			m_pFldSelection = new QComboBox;
			m_pFldSelection->setEditable(true);
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Maximum);
			m_pFldSelection->setSizePolicy(policy);

			connect(m_pFldSelection,SIGNAL(currentTextChanged(const QString &)),this,SLOT(OnStateChangedText(const QString &)));

			//m_lstSelection.sort("BCS_VALUE");
			int nSize=m_lstSelection.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				m_pFldSelection->addItem(m_lstSelection.getDataRef(i,"BCS_VALUE").toString());
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_pFldSelection->setCurrentIndex(nIdx);
				else
					m_pFldSelection->setCurrentIndex(0);
			}

			hbox->addWidget(m_pFldSelection);
			vboxRightPart->addLayout(hbox);
		}
		break;

	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY: //dropdownlist
		{
			m_pFldSelection = new QComboBox;
			m_pFldSelection->setEditable(false);
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Maximum);
			m_pFldSelection->setSizePolicy(policy);
			connect(m_pFldSelection,SIGNAL(currentIndexChanged(int)),this,SLOT(OnStateChanged(int)));

			//m_lstSelection.sort("BCS_VALUE");
			int nSize=m_lstSelection.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				m_pFldSelection->addItem(m_lstSelection.getDataRef(i,"BCS_VALUE").toString());
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_pFldSelection->setCurrentIndex(nIdx);
				else
					m_pFldSelection->setCurrentIndex(0);
			}

			hbox->addWidget(m_pFldSelection);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON: //radio buttons
		{
			int nSize=m_lstSelection.getRowCount();
			//QGroupBox *group = new QGroupBox;
			for (int i=0;i<nSize;i++)
			{
				QRadioButton *pBtn= new QRadioButton;
				pBtn->setText(m_lstSelection.getDataRef(i,"BCS_RADIO_BUTTON_LABEL").toString());
				hbox->addWidget(pBtn);
				hbox->addSpacing(10);
				//hbox->addStretch(1);
				m_lstFldRadioButtons.append(pBtn);
				connect(pBtn,SIGNAL(clicked(bool)),this,SLOT(OnStateChangedRadio(bool)));
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_lstFldRadioButtons.at(nIdx)->setChecked(true);
				else
					m_lstFldRadioButtons.at(0)->setChecked(true);
			}

			hbox->addStretch(1);
			vboxRightPart->addStretch();
			vboxRightPart->addLayout(hbox);
			vboxRightPart->addStretch();
		}
		break;
	}

	
	QHBoxLayout *hboxTotal = new QHBoxLayout;
	hboxTotal->addLayout(vboxLeftPart);
	hboxTotal->addLayout(vboxRightPart);
	hboxTotal->setSpacing(1);
	hboxTotal->setContentsMargins(3,0,50,0);
	this->setLayout(hboxTotal);
	
}

void Table_CustomFieldWidget::OnLongTextEdit()
{
	QString strText;
	if (m_pFldLongText)
	{
		strText = m_pFldLongText->toHtml();
	}
	RichEditorDlg dlg;
	dlg.SetHtml(strText);
	dlg.SetEditMode(bTableInEditMode);
	if(0 != dlg.exec())
	{
		strText=dlg.GetHtml();
		m_pFldLongText->setHtml(strText);
		if (bEnableSignalDataChange)
			emit DataChanged(m_nRowInTable);
	}

}

void Table_CustomFieldWidget::CreateTextField()
{
	QLabel *fldName = new QLabel;
	fldName->setText(m_recCustomField.getDataRef(0,"BCF_NAME").toString());
	fldName->setMinimumWidth(69);
	
	m_pbtnEditLongText = new QPushButton;
	m_pbtnEditLongText->setIcon(QIcon(":SAP_Modify.png"));
	m_pbtnEditLongText->setText(tr("Edit"));
	QSize buttonSize(80, 22);
	m_pbtnEditLongText->setMaximumSize(buttonSize);
	m_pbtnEditLongText->setMinimumSize(buttonSize);
	connect(m_pbtnEditLongText, SIGNAL(clicked()), this, SLOT(OnLongTextEdit()));
	
	QHBoxLayout *hboxLeftPart = new QHBoxLayout;
	hboxLeftPart->addWidget(fldName);
	//hboxLeftPart->addStretch(1);
	hboxLeftPart->addWidget(m_pbtnEditLongText);
	hboxLeftPart->setContentsMargins(0,0,0,0);

	QVBoxLayout *vboxRightPart = new QVBoxLayout;

	switch (m_nSelType)
	{
	case GlobalConstants::TYPE_CUSTOM_SELECTION_NONE: //checkbox
		{
			m_pFldLongText = new QTextEdit;
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Expanding);
			m_pFldLongText->setSizePolicy(policy);
			m_pFldLongText->setReadOnly(true);
			vboxRightPart->addWidget(m_pFldLongText);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL: //dropdownlist
		{
	
		}
		break;

	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY: //dropdownlist
		{
	
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON: //radio buttons
		{
			}
		break;
	}
		
	QHBoxLayout *hboxTotal = new QHBoxLayout;
	hboxTotal->addLayout(hboxLeftPart);
	hboxTotal->addLayout(vboxRightPart);
	hboxTotal->setSpacing(1);
	hboxTotal->setContentsMargins(3,0,50,0);
	this->setLayout(hboxTotal);

}

void Table_CustomFieldWidget::CreateIntField()
{

	QLabel *fldName = new QLabel;
	fldName->setText(m_recCustomField.getDataRef(0,"BCF_NAME").toString());
	fldName->setMinimumWidth(150);

	QVBoxLayout *vboxLeftPart = new QVBoxLayout;
	vboxLeftPart->addWidget(fldName);
	vboxLeftPart->setSpacing(1);
	vboxLeftPart->setContentsMargins(0,0,0,0);
	
	QVBoxLayout *vboxRightPart = new QVBoxLayout;
	vboxLeftPart->setContentsMargins(0,0,0,0);

	QHBoxLayout *hbox = new QHBoxLayout;


	switch (m_nSelType)
	{
	case GlobalConstants::TYPE_CUSTOM_SELECTION_NONE: //checkbox
		{
			m_pFldLineEdit = new QLineEdit;
			m_pFldLineEdit->setMinimumWidth(100);
			m_pFldLineEdit->setMaximumWidth(100);
			m_pFldLineEdit->setValidator(new QIntValidator(-9999999, 9999999, m_pFldLineEdit));
			connect(m_pFldLineEdit,SIGNAL(textChanged(const QString &)),this,SLOT(OnStateChangedText(const QString &)));
			
			hbox->addWidget(m_pFldLineEdit);
			hbox->addStretch();
			hbox->setSpacing(1);
			hbox->setContentsMargins(0,0,0,0);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL: //dropdownlist
		{
			m_pFldSelection = new QComboBox;
			m_pFldSelection->setEditable(true);
			m_pFldSelection->lineEdit()->setValidator(new QIntValidator(-9999999, 9999999, m_pFldSelection->lineEdit()));
			
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Maximum);
			m_pFldSelection->setSizePolicy(policy);

			connect(m_pFldSelection,SIGNAL(currentTextChanged(const QString &)),this,SLOT(OnStateChangedText(const QString &)));

			//m_lstSelection.sort("BCS_VALUE");
			int nSize=m_lstSelection.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				m_pFldSelection->addItem(m_lstSelection.getDataRef(i,"BCS_VALUE").toString());
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_pFldSelection->setCurrentIndex(nIdx);
				else
					m_pFldSelection->setCurrentIndex(0);
			}

			hbox->addWidget(m_pFldSelection);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY: //dropdownlist
		{
			m_pFldSelection = new QComboBox;
			m_pFldSelection->setEditable(false);
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Maximum);
			m_pFldSelection->setSizePolicy(policy);

			connect(m_pFldSelection,SIGNAL(currentIndexChanged(int)),this,SLOT(OnStateChanged(int)));


			//m_lstSelection.sort("BCS_VALUE");
			int nSize=m_lstSelection.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				m_pFldSelection->addItem(m_lstSelection.getDataRef(i,"BCS_VALUE").toString());
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_pFldSelection->setCurrentIndex(nIdx);
				else
					m_pFldSelection->setCurrentIndex(0);
			}

			hbox->addWidget(m_pFldSelection);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON: //radio buttons
		{
			int nSize=m_lstSelection.getRowCount();
			//QGroupBox *group = new QGroupBox;
			for (int i=0;i<nSize;i++)
			{
				QRadioButton *pBtn= new QRadioButton;
				pBtn->setText(m_lstSelection.getDataRef(i,"BCS_RADIO_BUTTON_LABEL").toString());
				hbox->addWidget(pBtn);
				hbox->addSpacing(10);
				m_lstFldRadioButtons.append(pBtn);
				connect(pBtn,SIGNAL(clicked(bool)),this,SLOT(OnStateChangedRadio(bool)));
			}

			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_lstFldRadioButtons.at(nIdx)->setChecked(true);
				else
					m_lstFldRadioButtons.at(0)->setChecked(true);
			}
			hbox->addStretch(1);
			vboxRightPart->addStretch();
			vboxRightPart->addLayout(hbox);
			vboxRightPart->addStretch();

		}
		break;
	}


	QHBoxLayout *hboxTotal = new QHBoxLayout;
	hboxTotal->addLayout(vboxLeftPart);
	hboxTotal->addLayout(vboxRightPart);
	hboxTotal->setSpacing(1);
	hboxTotal->setContentsMargins(3,0,50,0);
	this->setLayout(hboxTotal);


}

void Table_CustomFieldWidget::CreateFloatField()
{
	QLabel *fldName = new QLabel;
	fldName->setText(m_recCustomField.getDataRef(0,"BCF_NAME").toString());
	fldName->setMinimumWidth(150);

	QVBoxLayout *vboxLeftPart = new QVBoxLayout;
	vboxLeftPart->addWidget(fldName);
	vboxLeftPart->setSpacing(1);
	vboxLeftPart->setContentsMargins(0,0,0,0);

	QVBoxLayout *vboxRightPart = new QVBoxLayout;
	vboxLeftPart->setContentsMargins(0,0,0,0);

	QHBoxLayout *hbox = new QHBoxLayout;


	switch (m_nSelType)
	{
	case GlobalConstants::TYPE_CUSTOM_SELECTION_NONE: //checkbox
		{
			m_pFldLineEdit = new QLineEdit;
			m_pFldLineEdit->setMinimumWidth(100);
			m_pFldLineEdit->setMaximumWidth(100);
			m_pFldLineEdit->setValidator(new QDoubleValidator(-999999.0, 999999.0, 4, m_pFldLineEdit));
			connect(m_pFldLineEdit,SIGNAL(textChanged(const QString &)),this,SLOT(OnFloatFldStateChangedText(const QString &)));
	
			hbox->addWidget(m_pFldLineEdit);
			hbox->addStretch();
			hbox->setSpacing(1);
			hbox->setContentsMargins(0,0,0,0);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL: //dropdownlist
		{
			m_pFldSelection = new QComboBox;
			m_pFldSelection->setEditable(true);
			m_pFldSelection->lineEdit()->setValidator(new QDoubleValidator(-999999.0, 999999.0, 4, m_pFldSelection->lineEdit()));
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Maximum);
			m_pFldSelection->setSizePolicy(policy);

			connect(m_pFldSelection,SIGNAL(currentTextChanged(const QString &)),this,SLOT(OnFloatFldStateChangedText(const QString &)));

			//m_lstSelection.sort("BCS_VALUE");
			int nSize=m_lstSelection.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				m_pFldSelection->addItem(m_lstSelection.getDataRef(i,"BCS_VALUE").toString());
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_pFldSelection->setCurrentIndex(nIdx);
				else
					m_pFldSelection->setCurrentIndex(0);
			}

			hbox->addWidget(m_pFldSelection);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY: //dropdownlist
		{
			m_pFldSelection = new QComboBox;
			m_pFldSelection->setEditable(false);
			QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Maximum);
			m_pFldSelection->setSizePolicy(policy);

			connect(m_pFldSelection,SIGNAL(currentIndexChanged(int)),this,SLOT(OnStateChanged(int)));


			//m_lstSelection.sort("BCS_VALUE");
			int nSize=m_lstSelection.getRowCount();
			for (int i=0;i<nSize;i++)
			{
				m_pFldSelection->addItem(m_lstSelection.getDataRef(i,"BCS_VALUE").toString());
			}
			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_pFldSelection->setCurrentIndex(nIdx);
				else
					m_pFldSelection->setCurrentIndex(0);
			}

			hbox->addWidget(m_pFldSelection);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON: //radio buttons
		{
			int nSize=m_lstSelection.getRowCount();
			//QGroupBox *group = new QGroupBox;
			for (int i=0;i<nSize;i++)
			{
				QRadioButton *pBtn= new QRadioButton;
				pBtn->setText(m_lstSelection.getDataRef(i,"BCS_RADIO_BUTTON_LABEL").toString());
				hbox->addWidget(pBtn);
				hbox->addSpacing(10);
				m_lstFldRadioButtons.append(pBtn);
				connect(pBtn,SIGNAL(clicked(bool)),this,SLOT(OnStateChangedRadio(bool)));
			}

			if (nSize>0)
			{
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_lstFldRadioButtons.at(nIdx)->setChecked(true);
				else
					m_lstFldRadioButtons.at(0)->setChecked(true);
			}

			hbox->addStretch(1);
			vboxRightPart->addStretch();
			vboxRightPart->addLayout(hbox);
			vboxRightPart->addStretch();
		}
		break;
	}


	QHBoxLayout *hboxTotal = new QHBoxLayout;
	hboxTotal->addLayout(vboxLeftPart);
	hboxTotal->addLayout(vboxRightPart);
	hboxTotal->setSpacing(1);
	hboxTotal->setContentsMargins(3,0,50,0);
	this->setLayout(hboxTotal);
}

void Table_CustomFieldWidget::CreateDateField()
{
	QLabel *fldName = new QLabel;
	fldName->setText(m_recCustomField.getDataRef(0,"BCF_NAME").toString());
	fldName->setMinimumWidth(150);

	QVBoxLayout *vboxLeftPart = new QVBoxLayout;
	vboxLeftPart->addWidget(fldName);
	vboxLeftPart->setSpacing(1);
	vboxLeftPart->setContentsMargins(0,0,0,0);

	QVBoxLayout *vboxRightPart = new QVBoxLayout;
	vboxLeftPart->setContentsMargins(0,0,0,0);

	QHBoxLayout *hbox = new QHBoxLayout;
	
	switch (m_nSelType)
	{
	case GlobalConstants::TYPE_CUSTOM_SELECTION_NONE: 
		{
			m_pFldDateTime = new DateTimeEditEx;
			m_pFldDateTime->setMinimumWidth(100);
			m_pFldDateTime->setMaximumWidth(100);
			connect(m_pFldLineEdit,SIGNAL(focusOutSignal()),this,SLOT(OnStateChangedDate()));

			hbox->addWidget(m_pFldDateTime);
			hbox->addStretch();
			hbox->setSpacing(1);
			hbox->setContentsMargins(0,0,0,0);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL: 
		{
		
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY: 
		{
	
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON: //radio buttons
		{
		
		}
		break;
	}

	QHBoxLayout *hboxTotal = new QHBoxLayout;
	hboxTotal->addLayout(vboxLeftPart);
	hboxTotal->addLayout(vboxRightPart);
	hboxTotal->setSpacing(1);
	hboxTotal->setContentsMargins(3,0,3,0);
	this->setLayout(hboxTotal);

}

void Table_CustomFieldWidget::CreateDatetimeField()
{
	QLabel *fldName = new QLabel;
	fldName->setText(m_recCustomField.getDataRef(0,"BCF_NAME").toString());
	fldName->setMinimumWidth(150);

	QVBoxLayout *vboxLeftPart = new QVBoxLayout;
	vboxLeftPart->addWidget(fldName);
	vboxLeftPart->setSpacing(1);
	vboxLeftPart->setContentsMargins(0,0,0,0);

	QVBoxLayout *vboxRightPart = new QVBoxLayout;
	vboxLeftPart->setContentsMargins(0,0,0,0);

	QHBoxLayout *hbox = new QHBoxLayout;

	switch (m_nSelType)
	{
	case GlobalConstants::TYPE_CUSTOM_SELECTION_NONE: 
		{
			m_pFldDateTime = new DateTimeEditEx;
			m_pFldDateTime->setMinimumWidth(150);
			m_pFldDateTime->setMaximumWidth(150);
			m_pFldDateTime->useAsDateTimeEdit();
			connect(m_pFldLineEdit,SIGNAL(focusOutSignal()),this,SLOT(OnStateChangedDate()));

			hbox->addWidget(m_pFldDateTime);
			hbox->addStretch();
			hbox->setSpacing(1);
			hbox->setContentsMargins(0,0,0,0);
			vboxRightPart->addLayout(hbox);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL: 
		{

		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY: 
		{

		}
		break;
	case GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON: //radio buttons
		{

		}
		break;
	}

	QHBoxLayout *hboxTotal = new QHBoxLayout;
	hboxTotal->addLayout(vboxLeftPart);
	hboxTotal->addLayout(vboxRightPart);
	hboxTotal->setSpacing(1);
	hboxTotal->setContentsMargins(3,0,3,0);
	this->setLayout(hboxTotal);
}


//get data from controls:
DbRecordSet Table_CustomFieldWidget::GetDataFromWidget()
{
	switch (m_nDataType)
	{
	case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
		{
			int nValue=0;
			if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
			{
				if (m_pFldCheckBox)
					nValue=m_pFldCheckBox->isChecked();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY)
			{
				if (m_pFldSelection)
					nValue=m_pFldSelection->currentText().toInt();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
			{
				for (int i=0;i<m_lstFldRadioButtons.size();i++)
				{
					if (m_lstFldRadioButtons.at(i)->isChecked())
					{
						if (i<m_lstSelection.getRowCount())
							nValue=m_lstSelection.getDataRef(i,"BCS_VALUE").toInt();
					}
				}
			}
			m_recData.setData(0,"BCB_VALUE",nValue);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_INT:
		{
			int nValue=0;
			if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
			{
				if (m_pFldLineEdit)
					nValue=	m_pFldLineEdit->text().toInt();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY)
			{
				if (m_pFldSelection)
					nValue=m_pFldSelection->currentText().toInt();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL)
			{
				if (m_pFldSelection)
					nValue=m_pFldSelection->currentText().toInt();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
			{
				for (int i=0;i<m_lstFldRadioButtons.size();i++)
				{
					if (m_lstFldRadioButtons.at(i)->isChecked())
					{
						if (i<m_lstSelection.getRowCount())
							nValue=m_lstSelection.getDataRef(i,"BCS_VALUE").toInt();
					}
				}
			}
			m_recData.setData(0,"BCI_VALUE",nValue);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
		{
			QString strfValue="0";
			if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
			{
				if (m_pFldLineEdit)
					strfValue=	m_pFldLineEdit->text();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY)
			{
				if (m_pFldSelection)
					strfValue=m_pFldSelection->currentText();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL)
			{
				if (m_pFldSelection)
					strfValue=m_pFldSelection->currentText();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
			{
				for (int i=0;i<m_lstFldRadioButtons.size();i++)
				{
					if (m_lstFldRadioButtons.at(i)->isChecked())
					{
						if (i<m_lstSelection.getRowCount())
							strfValue=m_lstSelection.getDataRef(i,"BCS_VALUE").toString();
					}
				}
			}
			m_recData.setData(0,"BCFL_VALUE",strfValue);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
		{
			QString  strValue;
			if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
			{
				if (m_pFldLineEdit)
					strValue=m_pFldLineEdit->text();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY)
			{
				if (m_pFldSelection)
					strValue=m_pFldSelection->currentText();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL)
			{
				if (m_pFldSelection)
					strValue=m_pFldSelection->currentText();
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
			{
				for (int i=0;i<m_lstFldRadioButtons.size();i++)
				{
					if (m_lstFldRadioButtons.at(i)->isChecked())
					{
						if (i<m_lstSelection.getRowCount())
							strValue=m_lstSelection.getDataRef(i,"BCS_VALUE").toString();
					}
				}
			}
			m_recData.setData(0,"BCT_VALUE",strValue);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
		{
			QString strText;
			if (m_pFldLongText)
			{
				strText = m_pFldLongText->toHtml();
			}
			//m_recData.Dump();
			m_recData.setData(0,"BCLT_VALUE",strText);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
		{
			QDate date;
			if (m_pFldDateTime)
					date =	m_pFldDateTime->date();
			m_recData.setData(0,"BCD_VALUE",date);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
		{
			QDateTime datetime;
			if (m_pFldDateTime)
				datetime =	m_pFldDateTime->dateTime();
			m_recData.setData(0,"BCDT_VALUE",datetime);
		}
		break;
	}


	return m_recData;

}



//set data if edit mode:
void Table_CustomFieldWidget::SetData(DbRecordSet &recCustomField)
{
	//custom field record must be non empty
	if (recCustomField.getRowCount()==0)
	{
		Q_ASSERT(false);
		return ;
	}

	//get data:
	m_recCustomField	=recCustomField;
	m_lstSelection		=recCustomField.getDataRef(0,"LST_SELECTIONS").value<DbRecordSet>();
	m_nDataType			=m_recCustomField.getDataRef(0,"BCF_DATA_TYPE").toInt();
	m_nSelType			=m_recCustomField.getDataRef(0,"BCF_SELECTION_TYPE").toInt();

	switch (m_nDataType)
	{
	case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
		{
			m_recData=m_recCustomField.getDataRef(0,"REC_BOOL_DATA").value<DbRecordSet>();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_INT:
		{
			m_recData=m_recCustomField.getDataRef(0,"REC_INT_DATA").value<DbRecordSet>();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
		{
			m_recData=m_recCustomField.getDataRef(0,"REC_FLOAT_DATA").value<DbRecordSet>();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
		{
			m_recData=m_recCustomField.getDataRef(0,"REC_TEXT_DATA").value<DbRecordSet>();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
		{
			m_recData=m_recCustomField.getDataRef(0,"REC_LONGTEXT_DATA").value<DbRecordSet>();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
		{
			m_recData=m_recCustomField.getDataRef(0,"REC_DATE_DATA").value<DbRecordSet>();
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
		{
			m_recData=m_recCustomField.getDataRef(0,"REC_DATETIME_DATA").value<DbRecordSet>();
		}

		break;
	}

	CreateEmptyDataIfNeeded();
}

void Table_CustomFieldWidget::SetDataToWidget()
{

	//custom field record must be non empty
	if (m_recData.getRowCount()==0 || m_recCustomField.getRowCount()==0)
	{
		Q_ASSERT(false);
		return;
	}

	bEnableSignalDataChange=false;
	switch (m_nDataType)
	{
	case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
		{
			int nValue=m_recData.getDataRef(0,"BCB_VALUE").toInt();
			int nRecID=m_recData.getDataRef(0,"BCB_ID").toInt(); //means if new record then set default values...
			if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
			{
				if (m_pFldCheckBox)
					m_pFldCheckBox->setChecked((bool)nValue);
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY)
			{
				if (m_pFldSelection)
				{
					int nIdx=m_pFldSelection->findText(m_recData.getDataRef(0,"BCB_VALUE").toString());
					m_pFldSelection->setCurrentIndex(nIdx);
				}
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
			{
				int nSize=m_lstSelection.getRowCount();
				for (int i=0;i<nSize;i++)
				{
					if (m_lstSelection.getDataRef(i,"BCS_VALUE").toInt()==nValue)
					{
						if (i<m_lstFldRadioButtons.size())
						{
							m_lstFldRadioButtons.at(i)->setChecked(true);
						}
					}
				}
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_INT:
		{
			int nValue=m_recData.getDataRef(0,"BCI_VALUE").toInt();
			if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
			{
				if (m_pFldLineEdit)
					m_pFldLineEdit->setText(QString::number(nValue));
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL)
			{
				if (m_pFldSelection)
				{
					m_pFldSelection->setCurrentText(m_recData.getDataRef(0,"BCI_VALUE").toString());
				}
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY)
			{
				if (m_pFldSelection)
				{
					int nIdx=m_pFldSelection->findText(m_recData.getDataRef(0,"BCI_VALUE").toString());
					m_pFldSelection->setCurrentIndex(nIdx);
				}
			}

			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
			{
				int nSize=m_lstSelection.getRowCount();
				for (int i=0;i<nSize;i++)
				{
					if (m_lstSelection.getDataRef(i,"BCS_VALUE").toInt()==nValue)
					{
						if (i<m_lstFldRadioButtons.size())
						{
							m_lstFldRadioButtons.at(i)->setChecked(true);
						}
					}
				}
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
		{
			QString strfValue=m_recData.getDataRef(0,"BCFL_VALUE").toString();
			if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
			{
				if (m_pFldLineEdit)
					m_pFldLineEdit->setText(strfValue);
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL)
			{
				if (m_pFldSelection)
				{
					m_pFldSelection->setCurrentText(strfValue);
				}
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY)
			{
				if (m_pFldSelection)
				{
					int nIdx=m_pFldSelection->findText(strfValue);
					m_pFldSelection->setCurrentIndex(nIdx);
				}
			}

			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
			{
				int nSize=m_lstSelection.getRowCount();
				for (int i=0;i<nSize;i++)
				{
					if (m_lstSelection.getDataRef(i,"BCS_VALUE").toString()==strfValue)
					{
						if (i<m_lstFldRadioButtons.size())
						{
							m_lstFldRadioButtons.at(i)->setChecked(true);
						}
					}
				}
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
		{
			QString  strValue=m_recData.getDataRef(0,"BCT_VALUE").toString();
			if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_NONE)
			{
				if (m_pFldLineEdit)
					m_pFldLineEdit->setText(strValue);
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_OPTIONAL)
			{
				if (m_pFldSelection)
				{
					m_pFldSelection->setCurrentText(strValue);
				}
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY)
			{
				if (m_pFldSelection)
				{
					m_pFldSelection->setCurrentIndex(m_pFldSelection->findText(strValue));
				}
			}
			else if (m_nSelType==GlobalConstants::TYPE_CUSTOM_SELECTION_MANDATORY_RADIO_BUTTON)
			{
				int nSize=m_lstSelection.getRowCount();
				for (int i=0;i<nSize;i++)
				{
					if (m_lstSelection.getDataRef(i,"BCS_VALUE").toString()==strValue)
					{
						if (i<m_lstFldRadioButtons.size())
						{
							m_lstFldRadioButtons.at(i)->setChecked(true);
						}
					}
				}
			}

		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
		{
			if (m_pFldLongText)
				m_pFldLongText->setHtml(m_recData.getDataRef(0,"BCLT_VALUE").toString());
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
		{
			if (m_pFldDateTime)
					m_pFldDateTime->setDate(m_recData.getDataRef(0,"BCD_VALUE").toDate());
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
		{
			if (m_pFldDateTime)
				m_pFldDateTime->setDateTime(m_recData.getDataRef(0,"BCDT_VALUE").toDateTime());
		}
		break;
	}


	bEnableSignalDataChange=true;

}



void Table_CustomFieldWidget::CreateEmptyDataIfNeeded()
{

	switch (m_nDataType)
	{
	case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
		{
			if (m_recData.getRowCount()==0)
			{
				m_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_BOOL));
				m_recData.addRow();
				m_recData.setData(0,"BCB_CUSTOM_FIELD_ID",m_recCustomField.getDataRef(0,"BCF_ID").toInt());
				//if selection is mandatory set default values:
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_recData.setData(0,"BCB_VALUE",m_lstSelection.getDataRef(nIdx,"BCS_VALUE").toInt());
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_INT:
		{
			if (m_recData.getRowCount()==0)
			{
				m_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_INT));
				m_recData.addRow();
				m_recData.setData(0,"BCI_CUSTOM_FIELD_ID",m_recCustomField.getDataRef(0,"BCF_ID").toInt());
				//if selection is mandatory set default values:
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_recData.setData(0,"BCI_VALUE",m_lstSelection.getDataRef(nIdx,"BCS_VALUE").toInt());
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
		{
			if (m_recData.getRowCount()==0)
			{
				m_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_FLOAT));
				m_recData.addRow();
				m_recData.setData(0,"BCFL_CUSTOM_FIELD_ID",m_recCustomField.getDataRef(0,"BCF_ID").toInt());
				//if selection is mandatory set default values:
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_recData.setData(0,"BCFL_VALUE",m_lstSelection.getDataRef(nIdx,"BCS_VALUE").toString());
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
		{
			if (m_recData.getRowCount()==0)
			{
				m_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_TEXT));
				m_recData.addRow();
				m_recData.setData(0,"BCT_CUSTOM_FIELD_ID",m_recCustomField.getDataRef(0,"BCF_ID").toInt());
				//if selection is mandatory set default values:
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_recData.setData(0,"BCT_VALUE",m_lstSelection.getDataRef(nIdx,"BCS_VALUE").toString());
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
		{
			if (m_recData.getRowCount()==0)
			{
				m_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_LONGTEXT));
				m_recData.addRow();
				m_recData.setData(0,"BCLT_CUSTOM_FIELD_ID",m_recCustomField.getDataRef(0,"BCF_ID").toInt());
				//if selection is mandatory set default values:
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_recData.setData(0,"BCLT_VALUE",m_lstSelection.getDataRef(nIdx,"BCS_VALUE").toString());
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
		{
			if (m_recData.getRowCount()==0)
			{
				m_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_DATE));
				m_recData.addRow();
				m_recData.setData(0,"BCD_CUSTOM_FIELD_ID",m_recCustomField.getDataRef(0,"BCF_ID").toInt());
				//if selection is mandatory set default values:
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_recData.setData(0,"BCD_VALUE",m_lstSelection.getDataRef(nIdx,"BCS_VALUE").toDate());
			}
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
		{
			if (m_recData.getRowCount()==0)
			{
				m_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_DATETIME));
				m_recData.addRow();
				m_recData.setData(0,"BCDT_CUSTOM_FIELD_ID",m_recCustomField.getDataRef(0,"BCF_ID").toInt());
				//if selection is mandatory set default values:
				int nIdx=m_lstSelection.find("BCS_IS_DEFAULT",1,true,false,true,true);
				if (nIdx>=0)
					m_recData.setData(0,"BCDT_VALUE",m_lstSelection.getDataRef(nIdx,"BCS_VALUE").toDateTime());
			}
		}
		break;
	}	

}


//if bEdit and if field is long text then set on button EDIT or VIEW
void Table_CustomFieldWidget::SetButtonLongText(bool bEdit)
{
	if (m_pbtnEditLongText)
	{
		this->setEnabled(true); //if this is text fld enable whole field...
		bTableInEditMode=bEdit;
		if (bEdit)
		{
			m_pbtnEditLongText->setIcon(QIcon(":SAP_Modify.png"));
			m_pbtnEditLongText->setText(tr("Edit"));
		}
		else
		{
			m_pbtnEditLongText->setIcon(QIcon(":SAP_Select.png"));
			m_pbtnEditLongText->setText(tr("View"));
		}
	}


}

void Table_CustomFieldWidget::OnFloatFldStateChangedText(const QString &text)
{
	if (bEnableSignalDataChange)emit DataChanged(m_nRowInTable);


}
