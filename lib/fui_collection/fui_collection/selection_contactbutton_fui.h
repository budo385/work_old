#ifndef SELECTION_CONTACTBUTTON_FUI_H
#define SELECTION_CONTACTBUTTON_FUI_H

#include "bus_client/bus_client/selection_contactbutton.h"

class Selection_ContactButton_FUI : public Selection_ContactButton
{
	Q_OBJECT

public:
		Selection_ContactButton_FUI(QWidget *parent=0);
		~Selection_ContactButton_FUI();

protected slots:
		virtual void OnSendMail();
		virtual void OnPhoneCall();
		virtual void OnDocument();
		virtual void OnInternet();
		virtual void OnChat();
		virtual void OnSMS();
	
};

#endif // SELECTION_CONTACTBUTTON_FUI_H
