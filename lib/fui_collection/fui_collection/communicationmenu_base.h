#ifndef COMMUNICATIONMENU_BASE_H
#define COMMUNICATIONMENU_BASE_H

#include <QObject>
#include <QVBoxLayout>
#include "doc_menuwidget.h"
#include "email_menuwidget.h"
#include "dropzone_menuwidget.h"
#include "app_menuwidget.h"
#include "calendar_menuwidget.h"
#include "bus_client/bus_client/selection_contacts.h"



class CommunicationMenu_Base : public QFrame
{
	Q_OBJECT

public:
	CommunicationMenu_Base(QWidget *parent);
	~CommunicationMenu_Base();

	enum SubMenuType
	{
		SUBMENU_EMAIL,
		SUBMENU_DOCUMENT,
		SUBMENU_DROPZONE,
		SUBMENU_CEGRID,
		SUBMENU_CALENDAR
	};

	void SetDefaults(DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL,DbRecordSet *rowQCWContact=NULL);
	void SetDefaultMail(QString strMail,int nEmailReplyID=-1);
	void SetDefaultPhone(QString strPhone);
	void SetDefaultInternet(QString strInternet);
	void SetSerialEmailMode();
	virtual void SetFUIParent(int nFUI){};

	virtual QWidget* GetSelector(){return NULL;};
	DropZone_MenuWidget *m_menuDropZone; //public coz drop event!

signals:
	void NeedNewData(int nSubMenuType);
	void DropZone_NewDataDropped(int,DbRecordSet);
	void ChapterCollapsed(int nFuiTypeID, int nChapterIdx /* 0 -ACL, 1 -AC, 2-DP */, bool bBothChaptersCollapsed);

public slots:
	void OnPhoneCall();
	void OnSendEmail();
	void OnInternet();
	void OnSMS();
	void OnChat();

protected:
	Doc_MenuWidget* m_menuDoc;
	Email_MenuWidget* m_menuEmail;
	App_MenuWidget* m_menuApp;
	Calendar_MenuWidget* m_menuCalendar;

	QString m_strPhone;
	QString m_strInternet;
	DbRecordSet m_lstContacts;
	DbRecordSet m_lstProjects;
	QString m_strMailDefault;
	int m_nEmailReplyID;

protected slots:
	virtual void SubMenuRequestedNewData(int nSubMenuType=CommunicationMenu_Base::SUBMENU_DROPZONE);


protected:
	QVBoxLayout *m_vbox;
	QPushButton *m_btnPhone;
	QPushButton *m_btnInternet;
	
};

#endif // COMMUNICATIONMENU_BASE_H
