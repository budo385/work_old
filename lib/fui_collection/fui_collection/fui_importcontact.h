#ifndef FUI_IMPORTCONTACT_H
#define FUI_IMPORTCONTACT_H

#include <QWidget>
#include "generatedfiles/ui_fui_importcontact.h"
#include "fuibase.h"
#include <QProgressDialog>
#include "os_specific/os_specific/skype/skypeinterface.h"

#define _MAPI_FROM_DLL

class FUI_ImportContact : public FuiBase, public SkypeInterface
{
    Q_OBJECT

public:
    FUI_ImportContact(QWidget *parent = 0);
    ~FUI_ImportContact();

	QString GetFUIName(){return tr("Import Contacts");};
	void BuildImportListFromDrop(QByteArray &byteDrop);

	//B.T.:
	void SetImportListFromSource(DbRecordSet &lstImport);
	void SetDefaultSource(QString strSource);

	//SKYPE INTERFACE:
	void SetSkypeBuddiesReply(QString strReply);
	void OnUserResolved(int nTypeData,QString strHandle,QVariant varData);
	
	void EnableFilterWidgets(bool bEnable = true);
	void UpdateImportContactsButton();
	int FindLastRow(const QString &strHandle);
	void OnImportRecordsListCreated(QProgressDialog *pDlgProgress = NULL);
	void StartSearch();
	bool OnNewRowAdded(int nRow);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

	DbRecordSet m_lstData;
	DbRecordSet m_lstTmpImportData;	// temporary data format
	QList<QStringList> m_lstFolders;	//Outlook contacts folder(s)

	QString m_strFilterFirstName;
	QString m_strFilterLastName;
	QString m_strFilterOrganization;
	QString m_strFilterTown;

	QMap<QString, bool> m_mapSex;	// maps "Mr." to sex 

	int m_nTotalSkypeEntries;
	int m_nCurSkypeEntry;
	bool m_bSkipStartupFolderPicker;

private:
    Ui::FUI_ImportContactClass ui;
	void UpdateRowPhonesField(DbRecordSet &lstData, int nRow);
	FuiBase *m_pQCWFui;
	void OnUserResolved_FullName(QString strHandle, QString strFullName);
	void OnUserResolved_Birthday(QString strNumber, QString strData);
	void OnUserResolved_Sex(QString strNumber, QString strData);
	void OnUserResolved_Country(QString strNumber, QString strData);
	void OnUserResolved_Province(QString strNumber, QString strData);
	void OnUserResolved_City(QString strNumber, QString strData);
	void OnUserResolved_PhoneHome(QString strNumber, QString strData);
	void OnUserResolved_PhoneOffice(QString strNumber, QString strData);
	void OnUserResolved_PhoneMobile(QString strNumber, QString strData);
	void OnUserResolved_Homepage(QString strNumber, QString strData);
	void OnUserResolved_About(QString strNumber, QString strData);

private slots:
	void on_btnImportContacts_clicked();
	void on_btnBuildList_clicked();
	void on_btnSourceFolder_clicked();
	void OnRadioNoFilterChanged(bool bChecked);
	void OnRadioUseFilterChanged(bool bChecked);
	void OnImportTypeCboChanged(int nIdx);
	void on_editContacts();
	void checkSkypeContactsImported();
	void onStartupCheckShowFolderPicker();
};

#endif // FUI_IMPORTCONTACT_H
