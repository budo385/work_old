#include "options_organization.h"
#include "common/common/datahelper.h"
#include "common/common/entity_id_collection.h"
#include <QtWidgets/QMessageBox>


//global:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	

//global cache:
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
Options_Organization::Options_Organization(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
	:SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

	ui.frameOrg->Initialize(ENTITY_BUS_ORGANIZATION);
	int nOrgID=GetSettingValue(APP_DEFAULT_ORGANIZATION_ID).toInt();
	if (nOrgID<=0)
	{
		//get first:
		ui.frameOrg->GetSelectionController()->ReloadData();
		DbRecordSet *plstOrg=ui.frameOrg->GetSelectionController()->GetDataSource();
		if (plstOrg->getRowCount()>0)
		{
			nOrgID=plstOrg->getDataRef(0,"BORG_ID").toInt();
		}
	}
	ui.frameOrg->SetCurrentEntityRecord(nOrgID);

}

Options_Organization::~Options_Organization()
{

}


DbRecordSet Options_Organization::GetChangedValuesRecordSet()
{
	int nOrgID;
	ui.frameOrg->GetCurrentEntityRecord(nOrgID);
	SetSettingValue(APP_DEFAULT_ORGANIZATION_ID, nOrgID); //could be empty....


	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

