#include "settings_contacts.h"
#include "dlg_gridviews.h"
#include "dlg_contactaddressschemas.h"

#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

Settings_Contacts::Settings_Contacts(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
:SettingsBase(nOptionsSetID, bOptionsSwitch, NULL, parent)
{
	ui.setupUi(this);

	//reload data:
	//--------------------------GROUP VIEW-----------------------------------
	m_ComboHandler.Initialize(ENTITY_BUS_OPT_GRID_VIEWS);
	m_ComboHandler.SetDataForCombo(ui.cmbViewGroup,"BOGW_VIEW_NAME");
	m_ComboHandler.SetDistinctDisplay();
	m_ComboHandler.GetLocalFilter()->SetFilter("BOGW_GRID_ID",Dlg_GridViews::GRID_GROUP_CONTACT_GRID);
	m_ComboHandler.ReloadData();							

	ui.btnGroupView->setIcon(QIcon(":SAP_Select.png"));
	ui.btnGroupView->setToolTip(tr("Modify View"));
	connect(ui.btnGroupView,SIGNAL(clicked()),this,SLOT(OpenGroupViewEditor()));

	//--------------------------ACO VIEW-----------------------------------
	m_ComboHandlerACO.Initialize(ENTITY_BUS_OPT_GRID_VIEWS);
	m_ComboHandlerACO.SetDataForCombo(ui.cmbViewActual,"BOGW_VIEW_NAME");
	m_ComboHandlerACO.SetDistinctDisplay();
	m_ComboHandlerACO.GetLocalFilter()->SetFilter("BOGW_GRID_ID",Dlg_GridViews::GRID_ACTUAL_CONTACT_GRID);
	m_ComboHandlerACO.ReloadData();		

	ui.btnACOView->setIcon(QIcon(":SAP_Select.png"));
	ui.btnACOView->setToolTip(tr("Modify View"));
	connect(ui.btnACOView,SIGNAL(clicked()),this,SLOT(OpenACOViewEditor()));


	//--------------------------GROUP TREE SELECTED-----------------------------------
	DbRecordSet lstTrees;
	LoadTree(lstTrees);

	int nSize=lstTrees.getRowCount();
	ui.cmbGroupTree->blockSignals(true);
	for(int i=0;i<nSize;++i)
		ui.cmbGroupTree->addItem(lstTrees.getDataRef(i,"BGTR_NAME").toString(),lstTrees.getDataRef(i,"BGTR_ID"));	
	ui.cmbGroupTree->blockSignals(false);


	//--------------------------FORMATTED ADDRESSES------------------------------------
	//COMBO:
	m_FormatAddressCombo.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
	m_FormatAddressCombo.SetDataForCombo(ui.cmbAddressPerson,"BCMAS_SCHEMA_NAME");
	m_FormatAddressCombo.ReloadData(); 

	ui.btnCustomizeAddrPerson->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnCustomizeAddrPerson->setToolTip(tr("Modify Address Schema"));
	connect(ui.btnCustomizeAddrPerson,SIGNAL(clicked()),this,SLOT(OnCustomizeAddrPerson()));

	m_FormatAddressComboOrg.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
	m_FormatAddressComboOrg.SetDataForCombo(ui.cmbAddressOrg,"BCMAS_SCHEMA_NAME");
	m_FormatAddressComboOrg.ReloadData(); 

	ui.btnCustomizeAddrOrg->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnCustomizeAddrOrg->setToolTip(tr("Modify Address Schema"));
	connect(ui.btnCustomizeAddrOrg,SIGNAL(clicked()),this,SLOT(OnCustomizeAddrOrg()));



	//--------------------------SET DEFAULTS FROM SETTINGS----------------------------------

	//combo's:
	m_ComboHandler.SetCurrentIndexFromName(GetSettingValue(CONTACT_DEF_GROUP_LIST_VIEW).toString());
	m_ComboHandlerACO.SetCurrentIndexFromName(GetSettingValue(CONTACT_DEF_ACTUAL_LIST_VIEW).toString());
	m_FormatAddressCombo.SetCurrentIndexFromID(GetSettingValue(CONTACT_DEF_ADDR_SCHEMA_PERSON).toInt());
	m_FormatAddressComboOrg.SetCurrentIndexFromID(GetSettingValue(CONTACT_DEF_ADDR_SCHEMA_ORG).toInt());

	ui.ckbSaveOnExit->setChecked(GetSettingValue(CONTACT_DEF_SETTINGS_SAVE_ON_EXIT).toBool());
	ui.ckbLoadActual->setChecked(GetSettingValue(CONTACT_DEF_ACTUAL_LIST_FOR_LOAD).toBool());

	//tree:
	int nTree=GetSettingValue(CONTACT_DEF_GROUP_TREE).toInt();
	if (nTree>0)
	{
		ui.cmbGroupTree->setCurrentIndex(ui.cmbGroupTree->findData(nTree));
	}
	else
		ui.cmbGroupTree->setCurrentIndex(-1);


	//Comm. grid view selector.
	LoadViewSelector();
	//Get default view.
	int nViewID = GetSettingValue(CONTACT_DEF_FILTER_VIEW).toInt();
	//Set current item.
	ui.viewSelect_comboBox->setCurrentIndex(ui.viewSelect_comboBox->findData(nViewID));
	
	
	//sapne: set group for load:
	/*
	int nGroupLoad=GetSettingValue(CONTACT_DEF_GROUP_FOR_LOAD).toInt();
	if (nGroupLoad>0)
		ui.frameGroup->SetCurrentEntityRecord(nGroupLoad);
	else
		ui.frameGroup->Clear();
	*/
	

	//set Group SAPNE:
	ui.frameGroup->Initialize(ENTITY_BUS_GROUP_ITEMS);
	ui.frameGroup->GetButton(Selection_SAPNE_FUI::BUTTON_ADD)->setVisible(false);
	ui.frameGroup->GetButton(Selection_SAPNE_FUI::BUTTON_MODIFY)->setVisible(false);
	//get exclude group:
	int nGroupLoad=GetSettingValue(CONTACT_GROUP_EXCLUDE_ID).toInt();
	if (nGroupLoad>0)
		ui.frameGroup->SetCurrentEntityRecord(nGroupLoad);
	else
		ui.frameGroup->Clear();

	QString strDefaultContactRole=GetSettingValue(CONTACT_DEFAULT_ROLE).toString();
	QString strDefaultPersonRole=GetSettingValue(PERSON_DEFAULT_ROLE).toString();
	ui.txtRoleContact->setText(strDefaultContactRole);
	ui.txtRolePerson->setText(strDefaultPersonRole);


	GetFolderSetting(m_lstOutlookFoldersScan);
	RefreshInfoLabel();
}

Settings_Contacts::~Settings_Contacts()
{

}


DbRecordSet Settings_Contacts::GetChangedValuesRecordSet()
{
	QString nGroupViewID=m_ComboHandler.GetSelectedName();
	if (nGroupViewID!=GetSettingValue(CONTACT_DEF_GROUP_LIST_VIEW).toString())
		SetSettingValue(CONTACT_DEF_GROUP_LIST_VIEW, nGroupViewID);


	QString nACOViewID=m_ComboHandlerACO.GetSelectedName();
	if (nACOViewID!=GetSettingValue(CONTACT_DEF_ACTUAL_LIST_VIEW).toString())
		SetSettingValue(CONTACT_DEF_ACTUAL_LIST_VIEW, nACOViewID);

	int nOrgID=m_FormatAddressCombo.GetSelectedID();
	if (nOrgID!=GetSettingValue(CONTACT_DEF_ADDR_SCHEMA_PERSON).toInt())
		SetSettingValue(CONTACT_DEF_ADDR_SCHEMA_PERSON, nOrgID);

	int nPersID=m_FormatAddressComboOrg.GetSelectedID();
	if (nPersID!=GetSettingValue(CONTACT_DEF_ADDR_SCHEMA_ORG).toInt())
		SetSettingValue(CONTACT_DEF_ADDR_SCHEMA_ORG, nPersID);


	if (ui.ckbSaveOnExit->isChecked()!=GetSettingValue(CONTACT_DEF_SETTINGS_SAVE_ON_EXIT).toBool())
		SetSettingValue(CONTACT_DEF_SETTINGS_SAVE_ON_EXIT, ui.ckbSaveOnExit->isChecked());

	if (ui.ckbLoadActual->isChecked()!=GetSettingValue(CONTACT_DEF_ACTUAL_LIST_FOR_LOAD).toBool())
		SetSettingValue(CONTACT_DEF_ACTUAL_LIST_FOR_LOAD, ui.ckbLoadActual->isChecked());


	int nTree=ui.cmbGroupTree->itemData(ui.cmbGroupTree->currentIndex()).toInt();
	if(nTree==0)nTree=-1;
	if (nTree!=GetSettingValue(CONTACT_DEF_GROUP_TREE).toInt())
		SetSettingValue(CONTACT_DEF_GROUP_TREE,nTree);


	//Comm. grid view selector.
	int nViewID = GetSettingValue(CONTACT_DEF_FILTER_VIEW).toInt();
	int nSelectedViewID = ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex()).toInt();
	if (nSelectedViewID != nViewID)
		SetSettingValue(CONTACT_DEF_FILTER_VIEW, nSelectedViewID);

	
	int nGroupLoad;
	ui.frameGroup->GetCurrentEntityRecord(nGroupLoad);
	if (nGroupLoad!=GetSettingValue(CONTACT_GROUP_EXCLUDE_ID).toInt())
		SetSettingValue(CONTACT_GROUP_EXCLUDE_ID,nGroupLoad);
		

	QList<QStringList> lstFolders;
	GetFolderSetting(lstFolders);
	if(m_lstOutlookFoldersScan != lstFolders){
		QByteArray arData;
		OutlookFolderPickerDlg::FolderList2Binary(m_lstOutlookFoldersScan, arData);
		SetSettingValue(IMPORT_CONTACT_OUTLOOK_FOLDERS, arData);
	}


	QString strDefaultContactRole=ui.txtRoleContact->text();
	QString strDefaultPersonRole=ui.txtRolePerson->text();
	SetSettingValue(CONTACT_DEFAULT_ROLE,strDefaultContactRole);
	SetSettingValue(PERSON_DEFAULT_ROLE,strDefaultPersonRole);

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}



void Settings_Contacts::LoadTree(DbRecordSet &lstData,bool bReloadFromServer)
{

	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_GROUP_TREE));


	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_BUS_CONTACT_TREES);
		if (plstCacheData)
		{
			lstData=*plstCacheData;
			return;
		}
	}


	//SERVER:
	Status err;
	_SERVER_CALL(BusGroupTree->ReadTree(err,ENTITY_BUS_CONTACT,lstData))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//Store in Cache:
	g_ClientCache.SetCache(ENTITY_BUS_CONTACT_TREES,lstData);

}


void Settings_Contacts::OpenGroupViewEditor()
{

	QString strViewName=ui.cmbViewGroup->currentText();
	Dlg_GridViews dlg;
	dlg.Initialize(strViewName,Dlg_GridViews::GRID_GROUP_CONTACT_GRID);

	if(0 != dlg.exec())
	{
		strViewName=dlg.GetCurrentViewName();
		//set to new view, block signals, coz we have manual reload
		ui.cmbViewGroup->blockSignals(true);
		m_ComboHandler.SetCurrentIndexFromName(strViewName);
		ui.cmbViewGroup->blockSignals(false);
	}
}

void Settings_Contacts::OpenACOViewEditor()
{

	QString strViewName=ui.cmbViewActual->currentText();
	Dlg_GridViews dlg;
	dlg.Initialize(strViewName,Dlg_GridViews::GRID_ACTUAL_CONTACT_GRID);

	if(0 != dlg.exec())
	{
		strViewName=dlg.GetCurrentViewName();
		//set to new view, block signals, coz we have manual reload
		ui.cmbViewActual->blockSignals(true);
		m_ComboHandlerACO.SetCurrentIndexFromName(strViewName);
		ui.cmbViewActual->blockSignals(false);
	}
}


void Settings_Contacts::OnCustomizeAddrOrg()
{
	Dlg_ContactAddressSchemas dlg;
	bool bOK=dlg.Initialize(ui.cmbAddressOrg->currentText());
	if(!bOK) return;
	int nRes=dlg.exec();
	if(nRes==0) return;

	//set cuurent schemas:
	QString strCurrentSchema=dlg.GetCurrentSchemaName();
	if(!strCurrentSchema.isEmpty())
	{
		int nRow=m_FormatAddressComboOrg.GetDataSource()->find(m_FormatAddressComboOrg.GetDataSource()->getColumnIdx("BCMAS_SCHEMA_NAME"),strCurrentSchema,true);
		if(nRow!=-1)ui.cmbAddressOrg->setCurrentIndex(nRow);

		nRow=m_FormatAddressComboOrg.GetDataSource()->find(m_FormatAddressComboOrg.GetDataSource()->getColumnIdx("BCMAS_SCHEMA_NAME"),strCurrentSchema,true);
		if(nRow!=-1)ui.cmbAddressOrg->setCurrentIndex(nRow);
	}

}

void Settings_Contacts::OnCustomizeAddrPerson()
{
	Dlg_ContactAddressSchemas dlg;
	bool bOK=dlg.Initialize(ui.cmbAddressPerson->currentText());
	if(!bOK) return;
	int nRes=dlg.exec();
	if(nRes==0) return;

	//set cuurent schemas:
	QString strCurrentSchema=dlg.GetCurrentSchemaName();
	if(!strCurrentSchema.isEmpty())
	{
		int nRow=m_FormatAddressCombo.GetDataSource()->find(m_FormatAddressCombo.GetDataSource()->getColumnIdx("BCMAS_SCHEMA_NAME"),strCurrentSchema,true);
		if(nRow!=-1)ui.cmbAddressPerson->setCurrentIndex(nRow);

		nRow=m_FormatAddressCombo.GetDataSource()->find(m_FormatAddressCombo.GetDataSource()->getColumnIdx("BCMAS_SCHEMA_NAME"),strCurrentSchema,true);
		if(nRow!=-1)ui.cmbAddressPerson->setCurrentIndex(nRow);
	}

}

void Settings_Contacts::LoadViewSelector()
{
	ui.viewSelect_comboBox->clear();

	Status status;
	DbRecordSet recTmp;
	_SERVER_CALL(BusCommunication->GetCommFilterViews(status, recTmp, g_pClientManager->GetPersonID(), 1 /*Contact grid id*/))
	_CHK_ERR(status);

	//Fill combo.
	int nRowCount = recTmp.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		int ViewID = recTmp.getDataRef(i, "BUSCV_ID").toInt();
		QString strViewName = recTmp.getDataRef(i, "BUSCV_NAME").toString();
		ui.viewSelect_comboBox->addItem(strViewName, ViewID);
	}
}

void Settings_Contacts::on_btnSelectFolders_clicked()
{
	OutlookFolderPickerDlg dlg(false, true);
	dlg.SetSelection(m_lstOutlookFoldersScan);
	if(dlg.exec()){
		m_lstOutlookFoldersScan = dlg.GetSelection();
		RefreshInfoLabel();
		QByteArray arData;
		if(dlg.StoreSettings(arData))
			SetSettingValue(IMPORT_CONTACT_OUTLOOK_FOLDERS, arData);
			//g_pSettings->SetPersonSetting(IMPORT_CONTACT_OUTLOOK_FOLDERS, arData);
	}
}

void Settings_Contacts::GetFolderSetting(QList<QStringList> &lstFolders)
{
	//load outlook folder selection setting
	QByteArray  arData  = GetSettingValue(IMPORT_CONTACT_OUTLOOK_FOLDERS).toByteArray();
	OutlookFolderPickerDlg::Binary2FolderList(arData, lstFolders);
}

void Settings_Contacts::RefreshInfoLabel()
{
	//refresh info
	ui.labelFolders->setText(tr("%1 folders selected").arg(m_lstOutlookFoldersScan.count()));
}
