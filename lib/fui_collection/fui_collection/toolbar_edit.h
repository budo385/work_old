#ifndef TOOLBAR_EDIT_H
#define TOOLBAR_EDIT_H

#include <QtWidgets/QFrame>
#include <QPushButton>
#include "ui_toolbar_edit.h"

class toolbar_edit : public QFrame
{
	Q_OBJECT

public:
	toolbar_edit(QWidget *parent = 0);
	~toolbar_edit();

	QPushButton* GetBtnAdd(){return ui.btnAdd;};
	QPushButton* GetBtnSelect(){return ui.btnSelect;};
	QPushButton* GetBtnModify(){return ui.btnModify;};
	QPushButton* GetBtnClear(){return ui.btnRemove;};

private:
	Ui::toolbar_editClass ui;
};

#endif // TOOLBAR_EDIT_H
