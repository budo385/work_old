#ifndef FUI_CE_TYPES_H
#define FUI_CE_TYPES_H

#include <QWidget>
#include "generatedfiles/ui_fui_ce_types.h"
#include "fuibase.h"
#include "gui_core/gui_core/guifieldmanager.h"

class FUI_CE_Types : public FuiBase
{
    Q_OBJECT

public:
    FUI_CE_Types(QWidget *parent = 0);
    ~FUI_CE_Types();

	void on_cmdInsert();
	bool on_cmdOK();
	void SetDefaultType(int nType);
	QString GetFUIName();

protected:
	void DataPrepareForInsert(Status &err);
	void DataCheckBeforeWrite(Status &err);

private slots:
	void OnModifyIcon();
private:
	void RefreshDisplay();
	void SetMode(int nMode);
	void RefreshIcon();

    Ui::FUI_CE_TypesClass ui;
	int CommEntityTypeFromComboIdx(int);
	int m_nCurRootTypeIdx;	// combo box index
	GuiFieldManager *m_GuiFieldManagerMain;
private slots:
	void OnRootTypeComboChanged(int nIndex);
};

#endif // FUI_CE_TYPES_H
