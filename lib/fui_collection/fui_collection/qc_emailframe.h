#ifndef QC_EMAILFRAME_H
#define QC_EMAILFRAME_H

#include <QWidget>
#include "ui_qc_emailframe.h"
#include "common/common/dbrecordset.h"

class QC_EmailFrame : public QWidget
{
	Q_OBJECT

public:
	QC_EmailFrame(QWidget *parent = 0);
	~QC_EmailFrame();

	void Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType = 0 /*0-Large,1-small widget*/);
	void AddEmail(DbRecordSet recEmail);

private:
	Ui::QC_EmailFrameClass ui;

	void SetupGUIwidgets(DbRecordSet recData);
	void SetDataToRecordSet(QString strRecordSetFieldName, QString strValue);
	void SetIntDataToRecordSet(QString strRecordSetFieldName, int nValue);

	DbRecordSet *m_recFullContactData;
	DbRecordSet m_recLocalEntityRecordSet;
	int			m_nCurrentRow;
	int			m_nParentType;
	QWidget		*m_pParentWidget;

private slots:
	void on_RowChanged(int nRow);
	void on_email_lineEdit_textChanged(const QString &text);
	void on_description_lineEdit_textChanged(const QString &text);
	void on_addSelected_toolButton_clicked();
	void on_addSelected_toolButton_2_clicked();
	void on_isDefault_checkBox_stateChanged(int state);
};

#endif // QC_EMAILFRAME_H
