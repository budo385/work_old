#ifndef SELECTION_SAPNE_FUI_H
#define SELECTION_SAPNE_FUI_H

#include "bus_client/bus_client/selection_sapne.h"

class Selection_SAPNE_FUI : public Selection_SAPNE
{
	Q_OBJECT

public:
	Selection_SAPNE_FUI(QWidget *parent = 0);
	~Selection_SAPNE_FUI();
	QWidget* GetLastFUIOpen(){return m_pLastFUIOpen;};

protected slots:
	void on_btnModify_clicked();
	void on_btnInsert_clicked();
	void on_btnView_clicked();

protected:
	void CreatePieButton();
	QWidget *m_pLastFUIOpen;

};

#endif // SELECTION_SAPNE_FUI_H
