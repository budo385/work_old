#ifndef CALENDARENTITYITEM_H
#define CALENDARENTITYITEM_H

#include <QGraphicsProxyWidget>
#include <QTextEdit>
#include <QMenu>
#include "calendargraphicsview.h"
#include "calendarentitydisplaywidget.h"
#include "common/common/dbrecordset.h"

class CalendarEntityItem : public QGraphicsProxyWidget
{
	Q_OBJECT;
public:
	enum ResizeType
	{
		NO_RESIZE = 0,
		HORIZ_RESIZE,
		HORIZ_LEFT_RESIZE,
		VERT_RESIZE,
		DIAG_RESIZE
	};

	CalendarEntityItem(int nItemID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, QDateTime startDateTime, QDateTime endDateTime, 
						QString strTitle, QString strColor, QString strLocation,
						QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
						QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, int nBcep_Status, 
						int nBCEV_IS_INFORMATION, bool bIsProposed, CalendarGraphicsView *pCalendarGraphicsView);
	~CalendarEntityItem();

	void UpdateEntityItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, int nEntityPersonID, QDateTime startDateTime, QDateTime endDateTime, 
						QString strTitle, QString strColor, QString strLocation,
						QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, QString strDescription, 
						QHash<int, qreal> hshBrakeStart, QHash<int, qreal> hshBrakeEnd, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed);

	void setHorFactor(qreal nWidthFactor);
	void setColumnCount(int nColumnCount){m_nColumnCount = nColumnCount;};
	void setHorOrder(qreal nHorizOrder);
	void setHorPos(qreal nHorizPos);
	qreal getHorFactor(){return m_nWidthFactor;};
	qreal getHorOrder(){return m_nHorizOrder;};
	qreal getHorPos(){return m_nHorizPos;};
	int	getColumnCount(){return m_nColumnCount;};
	void getStartEndDateTime(QDateTime	&startDateTime, QDateTime &endDateTime);
	int getItemEntityID(){return m_nEntityID;};
	int getItemEntityType(){return m_nEntityType;};
	int getItemEntityPersonID(){return m_nEntityPersonID;};
	int getItemCentID(){return m_nCentID;};
	int getItemBcepID(){return m_nBcepID;};
	int getItemBcevID(){return m_nBcevID;};
	int getItemBCOL_ID(){return m_nBCOL_ID;};
	void SetSelected(bool bSelected = true);	
	//virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
	void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
	void dragLeaveEvent(QGraphicsSceneDragDropEvent *event);
	void dragMoveEvent(QGraphicsSceneDragDropEvent *event);
	void dropEvent(QGraphicsSceneDragDropEvent *event);
	void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void keyPressEvent(QKeyEvent *event);
	void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
//	bool viewportEvent(QEvent *event);

public slots:
	void on_UpdateCalendar();

private slots:
	void On_CnxtView();
	void On_CnxtModify();
	void On_CnxtDelete();

private:
	CalendarEntityItem::ResizeType isInResizeArea(const QPointF &pos);
	bool isInMoveArea(const QPointF &pos);
	void CreateContextMenuActions(QList<QAction*> &lstActions);
	
	qreal			  m_nWidthFactor;
	int				  m_nColumnCount;
	qreal			  m_nHorizOrder;
	qreal			  m_nHorizPos;
	QDateTime		  m_startDateTime;
	QDateTime		  m_endDateTime;
	int				  m_nEntityID;
	int				  m_nEntityType;
	int				  m_nCentID;
	int				  m_nBcepID;
	int				  m_nBcevID;
	int				  m_nBCOL_ID;
	int				  m_nBcep_Status;
	int				  m_nBCEV_IS_INFORMATION;
	bool			  m_bIsProposed;
	QTextEdit		  *m_pTextEdit;
	QRect			  m_rectItemRectangle;
	QPoint			  m_ptItemPosition;
	CalendarGraphicsView *m_pCalendarGraphicsView;
	bool			  m_isHResizing;
	bool			  m_isVResizing;
	bool			  m_isDResizing;
	int				  m_nHeight;
	int				  m_nWidth;
	CalendarEntityDisplayWidget *m_DisplayWidget;
	QList<QAction*>	  m_lstActions;
	bool			  m_bIsMoving;
	int				  m_nEntityPersonID;

signals:
	void ItemResizing(bool bIsResizing);
	void ItemResized(QGraphicsProxyWidget *thisItem);
	void ItemModify(QGraphicsProxyWidget *thisItem);
	void ItemView(QGraphicsProxyWidget *thisItem);
	void DropEntityOnItem(QGraphicsProxyWidget *thisItem, int nEntityID, int nEntityType, int nEntityPersonID);
	void ItemDelete(QGraphicsProxyWidget *thisItem);
	void ItemMoved(QGraphicsProxyWidget *thisItem);
	void ItemSelected(QGraphicsProxyWidget *thisItem);
};

#endif // CALENDARENTITYITEM_H
