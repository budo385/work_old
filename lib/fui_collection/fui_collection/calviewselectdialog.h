#ifndef CALVIEWSELECTDIALOG_H
#define CALVIEWSELECTDIALOG_H

#include <QtWidgets/QDialog>
#include "ui_calviewselectdialog.h"

class CalViewSelectDialog : public QDialog
{
	Q_OBJECT

public:
	CalViewSelectDialog(int nViewID, QString strViewName, QWidget *parent = 0);
	~CalViewSelectDialog();

private:
	Ui::CalViewSelectDialogClass ui;

	int m_nViewID;
	QString m_strViewName;

private slots:
	void on_save_pushButton_clicked();
	void on_cancel_pushButton_clicked();

signals:
	void returnData(int nViewID, QString strViewName, bool bIsPublic);
};

#endif // CALVIEWSELECTDIALOG_H
