#ifndef FUI_VOICECALL_H
#define FUI_VOICECALL_H

#include "ui_fui_voicecall.h"
#include "fuibase.h"
#include "os_specific/os_specific/skype/skypeinterface.h"

class FUI_VoiceCall : public FuiBase, public SkypeInterface
{
    Q_OBJECT

public:
    FUI_VoiceCall(QWidget *parent = 0);
    ~FUI_VoiceCall();

	QString GetFUIName();
	void SetViewMode(bool bView);

	void SetData(DbRecordSet &recVoice, DbRecordSet &recTask);
	
	DbRecordSet &GetVoiceData(){	
		UpdateDataFromGui(); 
		return m_recVoiceLogData; 
	}
	DbRecordSet &GetTaskData(){	
		ui.frameTask->GetDataFromGUI(); 
		return ui.frameTask->m_recTask; 
	}

protected:
	void RefreshDisplay();
	void SetMode(int nMode);
	void UpdateDataFromGui();

	DbRecordSet m_recVoiceLogData;

private:
    Ui::FUI_VoiceCallClass ui;

	QtPieMenu *m_pPieMenu;

private slots:
	void on_btnEndTimeSetCurrent_clicked();
	void on_btnStartTimeSetCurrent_clicked();
	void on_btnSheduleCall_clicked();
	void on_btnVoiceCall_Cancel_clicked();
	void on_btnVoiceCall_OK_clicked();
	void on_btnVoiceCall_Edit_clicked();

	void OnScheduleCall();
	void OnScheduleEmail();
	void OnScheduleDoc();
};

#endif // FUI_VOICECALL_H
