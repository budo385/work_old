#ifndef DLG_CONTACTADDRESSSCHEMAS_H
#define DLG_CONTACTADDRESSSCHEMAS_H

#include <QtWidgets/QDialog>
#include <QtWidgets/QMessageBox>
#include "common/common/dbrecordset.h"
#include "generatedfiles/ui_dlg_contactaddressschemas.h"
#include "bus_core/bus_core/formataddress.h"
#include "bus_client/bus_client/selection_combobox.h"
#include "dlg_addrschemainfo.h"

class Dlg_ContactAddressSchemas : public QDialog
{
    Q_OBJECT

public:
    Dlg_ContactAddressSchemas(QWidget *parent = 0);
    ~Dlg_ContactAddressSchemas();

	//Modes:
	enum Mode 
	{
		MODE_READ=0,	
		MODE_EDIT=1,			
		MODE_INSERT=2,			
		MODE_EMPTY=3	
	};

	bool Initialize(QString strDefaultSchemaName);
	QString GetCurrentSchemaName();

private slots:
	void on_btnAddColumn_clicked();
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnNew_clicked();
	void on_btnEdit_clicked();
	void on_btnDelete_clicked();
	void on_btnInfo_clicked();
	void onSchemaChanged(int nIndex);


private:

	void RefreshDisplay();
	void SetMode(int nMode);

	QString m_strLockedRes;
	int m_nMode;

    Ui::Dlg_ContactAddressSchemasClass ui;
	FormatAddress m_FormatAddress;
	Selection_ComboBox m_SchemasHandler;			//super handler: connected to cache

};

#endif // DLG_CONTACTADDRESSSCHEMAS_H
