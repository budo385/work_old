#ifndef FUIBASE_H
#define FUIBASE_H

#include <QWidget>
#include <QSplitter> 
#include <QCloseEvent>

#include "common/common/observer_ptrn.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "bus_client/bus_client/selection_actualorganization.h"
#include "gui_core/gui_core/guidatamanipulator.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "fui_avatar.h"

typedef void (*FUI_OnClose)(QWidget *, QWidget *);

/*!
	\class  FuiBase
	\ingroup GUICore_FUI
	\brief  This is a base class for any FUI window

	Supports:
	- automatic resize of Splitter
	- automatic close event

	Any window that wants to be used as a FUI must do the following:
	- inherit FuiBase class
	- initialize FuiBase by calling "InitFui(this);" in its own constructor

*/
class FuiBase : public QWidget,public ObsrPtrn_Subject, public ObsrPtrn_Observer
{
	Q_OBJECT

	friend class FuiManager;

public:
	FuiBase(QWidget *parent);
	virtual ~FuiBase();

	void InitFui(int nEntityID=-1,int nTableID=-1,MainEntitySelectionController * pSelectionController=NULL,QWidget *pCmdBar = NULL);
	void EnableHierarchyCodeCheck(QString strCodeField);
	void EnableAccessRightCheck(){m_bEnableAccessRightCheck=true;}
	void EnableWindowCloseOnCancelOrOK(){m_bEnableWindowCloseOnCancelOrOK=true;}
	void InitializeFUIWidget(QSplitter *pSplitter);//, int nInitLeftMenuSize=100);
	void RegisterCloseFn(FUI_OnClose pfn, QWidget *pMainWnd);
	bool IsStandAlone();

	//----------------------------------------
	//  MODE OF OPERATION (WINDOW STATES)
	//----------------------------------------
	enum Mode 
	{
		MODE_READ=0,	
		MODE_EDIT=1,			
		MODE_INSERT=2,			
		MODE_EMPTY=3,
		MODE_DELETE=4 
	};
	enum FUISignals
	{
		FUI_CHANGED_NAME=2001 //B.T. to avoid problems
	};

	virtual void notifyCache_AfterWrite(DbRecordSet recNewData);
	MainEntitySelectionController* getSelectionController(){return m_pSelectionController;};
	virtual void SetMode(int nMode);
	int GetMode(){return m_nMode;}
	int GetEntityRecordID(){return m_nEntityRecordID;};
	int GetEntityID(){return m_nEntityID;};
	virtual bool CanClose();
	bool m_bUnconditionalClose;

	//----------------------------------------
	//	UPDATE OBSERVER 
	//----------------------------------------
	virtual void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	//----------------------------------------
	// BUTTON BAR SUPPORT
	//----------------------------------------
	virtual void on_selectionChange(int nEntityRecordID);	//note: if 0 then it will invalidate FUI: go empty mode
	virtual void on_selectionDataChange(){};
	virtual void on_cmdInsert();
	virtual bool on_cmdEdit();
	virtual bool on_cmdDelete();
	virtual void on_cmdCopy();
	virtual bool on_cmdOK();
	virtual void on_cmdCancel();
	virtual void on_NewFromTemplate(bool bCopyCE = true){};
	virtual void on_NewStructFromTemplate(){};
	//----------------------------------------
	// GLOBAL NAME PROPAGATE
	//----------------------------------------
	virtual QString GetFUIName(){return m_strFUIName;};
	void SetFUIName(QString strName){m_strFUIName=strName;}
	void RefreshFuiName();
	
	//----------------------------------------
	//	Actual Org support
	//----------------------------------------
	void SetActualOrganization(int nActualOrganizationID,bool bSkipReadDataPurge=true);	//when opening new FUI, set ACO selector to this org
	void SetActualOrganizationHandler(Selection_ActualOrganization *pACOHandler,QString strOrganizationId_Field);
	virtual void SetFiltersForCurrentActualOrganization(int nActualOrganizationID){};
	//avatars:
	fui_avatar *m_pWndAvatar;
	virtual void ReloadFromAvatar(){};

protected:
	virtual void RefreshDisplay(){};			//implement to refresh display on window (from data to window widget)
	virtual void prepareCacheRecord(DbRecordSet* recNewData){}; //called from notifyCache_AfterWrite to prepare data that will be merge onto existing data in the cache
	virtual void notifyCache_AfterDelete();
	virtual void on_DataDeleted(DbRecordSet &lstDeleteData);


	//----------------------------------------
	// DATA HANDLING SUPPORT
	//----------------------------------------
	virtual void DataPrepareForInsert(Status &err);
	virtual void DataPrepareForCopy(Status &err);
	virtual void DataClear();
	virtual void DataDefine();
	virtual void DataCheckBeforeWrite(Status &err){};
	virtual void DataRead(Status &err,int nRecordID);
	virtual void DataWrite(Status &err);
	virtual void DataDelete(Status &err);
	virtual void DataLock(Status &err);
	virtual void DataUnlock(Status &err);
	
	//----------------------------------------
	//	Hierarchy support:
	//----------------------------------------
	bool CheckHierarchyCodeForReload();
	bool CheckHierarchyCodeAfterDelete();
	bool CheckHierarchyCodeBeforeDelete();
	bool NewCodeWizard(QString &strNewCode, QString &strNewName);

	virtual void	SetFUIStyle();
	virtual QString GetMainEntityTablePrefix();
	virtual QPoint	GetFuiPosition();
	virtual void	resizeEvent(QResizeEvent *event);
	virtual void	closeEvent(QCloseEvent *event);
	virtual bool	event ( QEvent * event );

	Selection_ActualOrganization *m_pACOHandler;
	MainEntitySelectionController *m_pSelectionController;
	QString m_strOrganizationId_Field;
	bool m_bSideBarModeView;
	QWidget *m_pWidget;		// FUI window
	QWidget *m_pMainWnd;	// main window
	QWidget *m_pCmdBar;		// command bar
	FUI_OnClose m_pfnClose;
	int m_nMode;
	int m_nEntityRecordID;
	int m_nEntityID;
	int m_nFuiType;		//MENU_DEPARTMENTS, ...
	//SIMPLE FUI vars:
	QString m_strLockedRes;
	DbRecordSet m_lstData;
	DbRecordSet m_lstDataCache;
	DbTableKeyData m_TableData;
	int m_nTableID;
	//hierarchy checks (only valid for hierarchy)
	QString m_strCodeField;
	QString m_strNameField;
	QString m_strOldCode;
	QString m_strOldName;
	QString m_strTablePrefix;
	QString m_strFUIName;
	
	QSplitter *m_pSplitter;
	int m_nInitLeftMenuSize;
	bool m_bBlockCloseEvent;
	bool m_bInitialized;
	bool m_bEnableAccessRightCheck;
	bool m_bEnableWindowCloseOnCancelOrOK;

private slots:
	void OnSplitterMoved();


};

#endif // FUIBASE_H

