#ifndef CALENDARTASKWIDGET_H
#define CALENDARTASKWIDGET_H

#include <QtWidgets/QFrame>
#include "generatedfiles/ui_calendartaskwidget.h"

class CalendarTaskWidget : public QFrame
{
	Q_OBJECT

public:
	CalendarTaskWidget(QWidget *parent = 0);
	~CalendarTaskWidget();

	void Reload(DbRecordSet *recEntities);
	void SelectTask(int nCE_Type, int nID);
	void OpenTask(int nCE_Type, int nID);

signals:
	void NeedNewData(int);

private:
	Ui::CalendarTaskWidgetClass ui;

	CommWorkBenchWidget *m_widgetCommGrid;
};

#endif // CALENDARTASKWIDGET_H
