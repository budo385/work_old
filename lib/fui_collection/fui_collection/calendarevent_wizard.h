#ifndef CALENDAREVENT_WIZARD_H
#define CALENDAREVENT_WIZARD_H

#include <QStackedWidget>
#include "generatedfiles/ui_calendarevent_wizard.h"
#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/guifieldmanager.h"

class CalendarEvent_Wizard : public QStackedWidget , public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	enum WizardSteps //as layout in GUI (matches index of TAB index)
	{
		STEP_BASIC=0,
		STEP_REMINDER,
		STEP_CONTACTS,
		STEP_PROJECTS,
		STEP_RESOURCES,
		STEP_BREAKS,
		STEP_RECURRENCE,
		STEP_TASK,
		STEP_MULTIPART
	};

	enum IndexWizardSteps //logical layout by step: step INDEX_STEP_CONTACTS maps to STEP_USER_AND_CONTACTS)
	{
		INDEX_STEP_BASIC=0,
		INDEX_STEP_CONTACTS=1,
		INDEX_STEP_PROJECTS=3,
		INDEX_STEP_REMINDER=4,
		INDEX_STEP_RESOURCES=2,
		INDEX_STEP_BREAKS=5,
		INDEX_STEP_RECURRENCE=6,
		INDEX_STEP_TASK=7,
		INDEX_STEP_MULTIPART=8
	};


	CalendarEvent_Wizard(QWidget *parent = 0);
	~CalendarEvent_Wizard();

	void	RefreshDisplay();
	void	SetData(DbRecordSet &lstDataCalEvent,DbRecordSet &lstDataCalEventPart,DbRecordSet &lstDataCalEventOption, DbRecordSet &lstNmrxPersonContact, DbRecordSet &lstNmrxProjects,DbRecordSet &lstNmrxResources, DbRecordSet &lstTask);
	void	GetData(DbRecordSet &lstDataCalEvent,DbRecordSet &lstDataCalEventPart,DbRecordSet &lstDataCalEventOption, DbRecordSet &lstNmrxPersonContact, DbRecordSet &lstNmrxProjects,DbRecordSet &lstNmrxResources, DbRecordSet &lstTask);
	int		GetPartCommEntityID();
	int		GetPartID();
	int		GetMainEventID();
	void	updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void	RefreshTitle(QString strTitle);
	void	SetCurrentOptionDates(QDateTime from,QDateTime to,bool bOverRide=false);

signals:
	void SignalTemplateChanged(int);

private slots:
	void OnOpenCalendar_Person(int,DbRecordSet,int,DbRecordSet);
	void OnOpenCalendar_Project(int,DbRecordSet,int,DbRecordSet);
	void OnOpenCalendar_Resources(int,DbRecordSet,int,DbRecordSet);
	void OnBreakRowInserted(int nRow);


private:
	Ui::CalendarEvent_WizardClass ui;


	DbRecordSet m_lstDataCalEvent;
	DbRecordSet m_lstDataCalEventPart;
	DbRecordSet m_lstDataCalEventOption;
	DbRecordSet m_lstDataCalEventBreaks;
	//DbRecordSet m_lstDataCalEventPartTaskData;


	GuiFieldManager *m_GuiFieldManagerOption;
	GuiFieldManager *m_GuiFieldManagerPart;

	
};

#endif // CALENDAREVENT_WIZARD_H
