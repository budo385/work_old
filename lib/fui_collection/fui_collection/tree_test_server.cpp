#include "tree_test_server.h"
#include "bus_client/bus_client/bocliententity_testtree.h"
#include "bus_client/bus_client/businessservicemanager.h"

extern BusinessServiceManager *g_pBoSet;

tree_test_server::tree_test_server(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);
	setAttribute(Qt::WA_QuitOnClose, false);
}

tree_test_server::~tree_test_server()
{
}

void tree_test_server::LoadStoredProjectList(Status &pStatus, DbRecordSet &TreeViewRecordSet, int StoredListCode, SQLDBPointer* pDbConn /*= NULL*/)
{
	_SERVER_CALL(TreeView->LoadStoredProjectList(pStatus, TreeViewRecordSet, StoredListCode, pDbConn))
	if(!pStatus.IsOK())
		qDebug() << pStatus.getErrorText();
	else
		qDebug() << "Stored project list loaded.";
}

void tree_test_server::BuildProjectList(Status &pStatus, DbRecordSet &TreeViewRecordSet, int ProjectListLevel, SQLDBPointer* pDbConn /*= NULL*/)
{
	_SERVER_CALL(TreeView->BuildProjectList(pStatus, TreeViewRecordSet, ProjectListLevel))
	if(!pStatus.IsOK())
		qDebug() << pStatus.getErrorText();
	else
		qDebug() << "Project list build done.";
}

void tree_test_server::on_pushButton_clicked()
{
	ui.lineEdit->clear();
	ui.lineEdit_2->clear();
	ui.lineEdit_3->clear();
	ui.lineEdit_4->clear();
	ui.lineEdit_5->clear();

	DbRecordSet RecordSet;

	QTime start = QDateTime::currentDateTime().time();

	LoadStoredProjectList(m_pStatus, RecordSet, 44);

	Model = new DbRecordSetModel(this);
	Model->InitializeModel(&RecordSet);
	ui.treeView->setModel(Model);
	
	QTime end = QDateTime::currentDateTime().time();

	QString strTime = QVariant(start).toString() + " " + QVariant(end).toString();
	ui.lineEdit->insert(QVariant(RecordSet.getRowCount()).toString());
	ui.lineEdit_2->insert(strTime);

	QObject::connect(ui.treeView->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(on_SelectionChange(const QModelIndex&, const QModelIndex&)));
}

void tree_test_server::on_pushButton_2_clicked()
{
	ui.lineEdit->clear();
	ui.lineEdit_2->clear();
	ui.lineEdit_3->clear();
	ui.lineEdit_4->clear();
	ui.lineEdit_5->clear();

	DbRecordSet RecordSet;
	
	QTime start = QDateTime::currentDateTime().time();

	BuildProjectList(m_pStatus, RecordSet, 4);

	Model = new DbRecordSetModel(this);
	Model->InitializeModel(&RecordSet);
	QTime end = QDateTime::currentDateTime().time();

	QString strTime = QVariant(start).toString() + " " + QVariant(end).toString();

	qDebug() << "Column count:" << RecordSet.getColumnCount();

	ui.treeView->setModel(Model);
	ui.lineEdit->insert(QVariant(RecordSet.getRowCount()).toString());
	ui.lineEdit_2->insert(strTime);

	QObject::connect(ui.treeView->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(on_SelectionChange(const QModelIndex&, const QModelIndex&)));
}

void tree_test_server::on_SelectionChange(const QModelIndex &to, const QModelIndex &from)
{
	if(to.isValid())
	{
		ui.lineEdit_3->clear();
		ui.lineEdit_4->clear();
		ui.lineEdit_5->clear();

		DbRecordSetItem *item = static_cast<DbRecordSetItem*>(to.internalPointer());
		int RowId = item->GetRowId();

		BoClientEntity_TestTree BoEntityTmp(g_pBoSet->app->BoEntity_TestTree);
		BoEntityTmp.Read(m_pStatus,RowId);
		DbRecordSet* RecordSet = BoEntityTmp.GetEntityData();

		QString Code = RecordSet->getDataRef(0, 1).toString();
		QString Name = RecordSet->getDataRef(0, 8).toString();
		QString Level = RecordSet->getDataRef(0, 2).toString();

		ui.lineEdit_3->insert(Code);
		ui.lineEdit_4->insert(Name);
		ui.lineEdit_5->insert(Level);
	}
}
