#include "fui_voicecallcenter.h"
#include <QHBoxLayout>
#include "common/common/entity_id_collection.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "os_specific/os_specific/skypemanager.h"
#include "os_specific/os_specific/skype/skypeparser.h"
#include "gui_core/gui_core/gui_helper.h"
#include "bus_client/bus_client/changemanager.h"
#include "voicecallredirectdlg.h"
#include "bus_core/bus_core/countries.h"
#include "dlg_nmrxeditor.h"
#include "gui_core/gui_core/pie_menu/qtpiemenu.h"
#include "fui_dm_documents.h"
#include "emaildialog.h"
#include "bus_core/bus_core/globalconstants.h"
#include "fui_collection/fui_collection/communicationactions.h"
#include "bus_client/bus_client/phoneselectordlg.h"
#include "emaildialog.h"
#include "commgridview.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/formatphone.h"
#include <QPainter>
#include "assignedprojectsdlg.h"
#include "addphonenumberdlg.h"
//#include "bus_client/bus_client/emailhelper.h"

 //defines FP codes
#include <QDesktopWidget>
#include <QDate>
#include <QSound>
#include <QDebug>
#include "sms_dialog.h"
#include "qcw_base.h"
#include "common/common/datahelper.h"
#ifdef _WIN32
	#define NOMINMAX
	#include <windows.h>
#endif



#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "gui_core/gui_core/thememanager.h"

#ifdef _WIN32
extern SkypeParser	g_objParser;
#endif

#include "fuimanager.h"
extern FuiManager	g_objFuiManager;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache; //global cache:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger
#include "bus_core/bus_core/useraccessright.h"
#include "bus_client/bus_client/documenthelper.h"
#include "bus_core/bus_core/commgridviewhelper.h"
extern UserAccessRight *g_AccessRight;				//global access right tester

bool Call_OnLoadFullPicture(int nPicID, DbRecordSet &rowPic);
bool IsPhoneNumber(QString strNumber);

/*
class QCheckableTabWidget : public QTabWidget
{
public:
	QCheckableTabWidget();
}
*/

FUI_VoiceCallCenter::FUI_VoiceCallCenter(QWidget *parent)
: FuiBase(parent),m_bSkipBackUpdate(false)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window Start initialization"));

	m_bCallWasEstablished = false;
	m_bIncoming = true;
	m_nCallState = CommGridViewHelper::CALL_STATE_None;
	m_bWaitForOnHold = false;

	ui.setupUi(this);
	InitFui();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Init FUI done"));

	//ui.txtPhoneNumber->setEditable(true);

	QButtonGroup *pGrp = new QButtonGroup(this);
	pGrp->addButton(ui.radOutgoing);
	pGrp->addButton(ui.radIncoming);
	pGrp->setExclusive(true);

	//add handler for schedule button:
	connect(ui.pushButton,SIGNAL(OnScheduleEmail()),this,SLOT(OnScheduleEmail()));
	connect(ui.pushButton,SIGNAL(OnScheduleCall()),this,SLOT(OnScheduleCall()));
	connect(ui.pushButton,SIGNAL(OnScheduleDoc(int)),this,SLOT(OnScheduleDoc(int)));

	ui.frameTask->SetDefaultTaskType(GlobalConstants::TASK_TYPE_SCHEDULED_VOICE_CALL);
	ui.frameTask->SetAlwaysExpanded();

	//define list:
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	m_recData.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_VOICECALLS));
	m_recData.copyDefinition(set, false);
	
	m_recData.addRow();
	m_recData.setData(0, "BVC_DEVICE_CODE", 1);	//Skype call is default one

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Init contact frame start"));

	//Contacts:
	ui.frameContact->Initialize(ENTITY_BUS_CONTACT,&m_recData, "BVC_CONTACT_ID" );
	ui.frameContact->EnableInsertButton();
	ui.frameContact->registerObserver(this);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Init contact frame done"));

	ui.frmSelectedProject->Initialize( ENTITY_BUS_PROJECT, &m_recData,"BVC_PROJECT_ID" );
	ui.frmType->Initialize( ENTITY_CE_TYPES, &m_recData,"CENT_CE_TYPE_ID" );
	ui.frmType->GetSelectionController()->GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",GlobalConstants::CE_TYPE_VOICE_CALL);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Init project frame done"));

	ui.btnStartTimeSetCurrent->setIcon(QIcon(":clock.png"));
	ui.btnEndTimeSetCurrent->setIcon(QIcon(":clock.png"));
	ui.btnExtSearch->setIcon(QIcon(":EarthSearch.png"));
	ui.btnOpenChat->setIcon(QIcon(":Comm_Chat32.png"));
	ui.btnOpenSMS->setIcon(QIcon(":Comm_SMS32.png"));
	ui.pixPhone->setPixmap(QPixmap(":Phone48.png"));
	
	int nValue;
	if( g_FunctionPoint.IsFPAvailable(FP_TELEPHONY_SKYPE_CALL_REGISTRATION, nValue) &&
		g_FunctionPoint.IsFPAvailable(FP_TELEPHONY_SKYPE_CALL, nValue) &&
		SkypeManager::IsSkypeInstalled())
	{
		ui.cboInterface->addItem("Skype");
	}

#ifdef _WIN32
	if(g_FunctionPoint.IsFPAvailable(FP_TELEPHONY_OTHER_CALL_REGISTRATION, nValue)){
		//ui.cboInterface->addItem("Other");

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Voice call window: Start adding TAPI interfaces"));

		//add all TAPI devices that support voice calls
		m_lstTAPIDevIDs.clear();
		int nCnt = m_lineTAPI.GetDeviceCount();
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Voice call window: detected total of %1 TAPI devices").arg(nCnt));
		for(int i=0; i<nCnt; i++)
		{
			QString strDeviceName;
			bool bVoiceCalls = false;
			if(m_lineTAPI.GetDeviceInfo(i, strDeviceName, bVoiceCalls))
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Voice call window: got device info, name [%1], voice=%2").arg(strDeviceName).arg(bVoiceCalls));
				qDebug() << "TAPI device found: " << strDeviceName;
				//if(bVoiceCalls)
				{
					//qDebug() << "Device added to list (supports voice)";
					ui.cboInterface->addItem(strDeviceName);
					m_lstTAPIDevIDs.push_back(i);
				}
			}
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("End adding TAPI interfaces"));
	}
#endif

	ui.cboInterface->setCurrentIndex(0);

	if(g_pSettings->GetPersonSetting(COMM_SETTINGS_DEFAULT_SEL_TYPE).toInt() != 0) //if not Skype
	{
		m_recData.setData(0, "BVC_DEVICE_CODE", 0); //other

		//now try to selected our device
		QString strSelDevice = g_pSettings->GetPersonSetting(COMM_SETTINGS_DEFAULT_TAPI_DEV).toString();
		if(!strSelDevice.isEmpty()){
			int nIdx = ui.cboInterface->findText(strSelDevice);
			if(nIdx >= 0)
				ui.cboInterface->setCurrentIndex(nIdx);
		}
	}

	int nDefaultIncoming = g_pSettings->GetPersonSetting(COMM_SETTINGS_DEFAULT_CALL_INCOMING).toInt();
	if(nDefaultIncoming != 0)
	{
		ui.radOutgoing->setChecked(false);
		ui.radIncoming->setChecked(true);
		m_recData.setData(0, "BVC_DIRECTION", 1);	//incoming
	}
	else{
		ui.radOutgoing->setChecked(true);
		ui.radIncoming->setChecked(false);
		m_recData.setData(0, "BVC_DIRECTION", 0);	//outgoing
	}

	

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Set styled buttons"));

	//set font white for all call buttons
	GUI_Helper::SetButtonTextColor(ui.btnCallAction1, QColor(255,255,255));
	GUI_Helper::SetButtonTextColor(ui.btnCallAction2, QColor(255,255,255));
	GUI_Helper::SetButtonTextColor(ui.btnCallAction3, QColor(255,255,255));
	
	GUI_Helper::SetStyledButtonColorBkg(ui.btnScheduleTask, GUI_Helper::BTN_COLOR_YELLOW);
	
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Set styled buttons done"));

	//set current date
	ui.txtCallDate->setDate(QDate::currentDate());

	//set task
	ui.frameTask->setVisible(false);
	DbRecordSet rcDummy;
	SetShowTask(rcDummy, false);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Task widget initialized"));

	SetWindowState(WND_STATE_PreCall);

	m_nCallID = -1;
	m_nCallConferenceID = -1;
	m_bCallOnHold = false;
	m_pDurationTimer = NULL;
	m_pTapiTimer = NULL;
	m_bForwarded = false;

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: PING Skype API "));

	if(SkypeManager::IsSkypeInstalled())
		SkypeManager::SendCommand("PING");	//to test connection

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Init Contact picture widget"));

	//connect SAPNE contact change to reload phone combo box:
	ui.frameContact->registerObserver(this);

	//in rowContactPicture, load contact picture when contact sapne is loaded
	rowContactPicture.destroy();
	rowContactPicture.addColumn(QVariant::Int,"BCNT_ID" );
	rowContactPicture.addColumn(QVariant::ByteArray,"BCNT_PICTURE" );
	rowContactPicture.addColumn(QVariant::Int,"BCNT_BIG_PICTURE_ID" );
	rowContactPicture.addColumn(QVariant::ByteArray,"BPIC_PICTURE" );
	rowContactPicture.addRow();
	ui.picture->Initialize(PictureWidget::MODE_PREVIEW,&rowContactPicture,"BCNT_PICTURE","BPIC_PICTURE","BCNT_BIG_PICTURE_ID","BCNT_ID");
	ui.picture->SetLoadFullPictureCallBack(Call_OnLoadFullPicture);
	ui.picture->RefreshDisplay();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Init Contact picture widget done"));

	connect(ui.txtCallStartTime, SIGNAL(editingFinished()),this,SLOT(OnCallStartTimeEdited()));
	connect(ui.txtCallEndTime, SIGNAL(editingFinished()),this,SLOT(OnCallEndTimeEdited()));

	ui.frameTask->SetEnabled(true);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Load entity types"));

	MainEntitySelectionController TypeHandler;
	TypeHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_PHONE);
	TypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	TypeHandler.ReloadData();
	m_recPhoneTypes = *TypeHandler.GetDataSource();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Load entity types done"));

	ui.btnPickPhone->setIcon(QIcon(":arrow_down16.png"));	//Phone16.png
	ui.btnSearchContact->setIcon(QIcon(":SearchContact.png"));
	ui.btnInsertNewPhone->setIcon(QIcon(":Add_Calendar16.png"));

	ui.btnExtSearch->setVisible(false);
	//ui.btnPickPhone->setVisible(false);

	//FUI:
	if(!g_FunctionPoint.IsFPAvailable(FP_TASKS, nValue))
	{
		ui.btnScheduleTask->setEnabled(false);
	}

	if(SkypeManager::IsSkypeInstalled()){
		QTimer *timer = new QTimer();
		connect(timer, SIGNAL(timeout()), this, SLOT(UpdateSkypeStatus()));
		timer->start(30000);
		UpdateSkypeStatus();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Set header style"));

	connect(ui.frmEditor,SIGNAL(on_PrintIcon_clicked()),this,SLOT(OnPrint()));

	//header style sheets
	ui.frameHdr->setStyleSheet("QFrame#frameHdr "+ThemeManager::GetSidebarChapter_Bkg());
		//issue 1843
		//+ThemeManager::GetGlobalWidgetStyle());
	//BT: issue 1397:
	ui.labelPhoneCall->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	ui.labelPhoneCall->setAlignment(Qt::AlignLeft);
	ui.labelPhoneCall->setText(tr("Phone Call"));
	//ui.labelPhoneCall->setStyleSheet("QLabel {color:white; font-style:italic; font-weight:800; font-family:Arial; font-size:12pt}");
	ui.label_8->setStyleSheet("QLabel {color:white}");
	ui.label_10->setStyleSheet("QLabel {color:white}");
	ui.labelPhoneNumber->setStyleSheet("QLabel {color:white}");
	ui.frame->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
	SetFUIStyle();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window: Set header style done"));

	m_pWndAvatar = NULL;

	ui.arSelector->SetEditMode(true);
	ui.arSelector->HideLabel();

	ui.frmEditor ->SetEmbeddedPixMode();

    ui.btnAssignProjects->setIcon(QIcon(":/assign_project.png"));
	ui.btnAssignProjects->setIconSize(QSize(29, 16));

	//update tab widget check icon on task changes
	updateTaskTabCheckbox();
	connect(ui.frameTask, SIGNAL(ScheduleTaskToggle()),this,SLOT(updateTaskTabCheckbox()));

	//set active tab
	ui.tabWidget->setCurrentIndex(0);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Call Window initialized"));
}

FUI_VoiceCallCenter::~FUI_VoiceCallCenter()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Call window destructor"));
}

//when melodramatically change data, call this:
void FUI_VoiceCallCenter::RefreshDisplay()
{
	//TOFIX init other fields ?
	if(m_recData.getDataRef(0, "BVC_DEVICE_CODE").toInt() == 1){
		ui.cboInterface->setCurrentIndex(ui.cboInterface->findText("Skype"));	//"Skype"
	}
	else{
		//TOFIX what is actually a BVC_DEVICE_CODE
		//ui.cboInterface->findText("Other")
		//ui.cboInterface->setCurrentIndex();	//"Other"
	}

	ui.txtCallDate		->setDate(m_recData.getDataRef(0, "BVC_START").toDateTime().date());
	ui.txtCallStartTime	->setText(m_recData.getDataRef(0, "BVC_START").toDateTime().time().toString("hh:mm:ss"));
	ui.txtCallEndTime	->setText(m_recData.getDataRef(0, "BVC_END").toDateTime().time().toString("hh:mm:ss"));

	ui.txtPhoneNumber	->clear();
	ui.txtPhoneNumber	->setText(m_recData.getDataRef(0, "BVC_CALLER_ID").toString());

	ui.radIncoming		->setChecked(m_recData.getDataRef(0, "BVC_DIRECTION").toInt() == 1);
	ui.radOutgoing		->setChecked(m_recData.getDataRef(0, "BVC_DIRECTION").toInt() == 0);
	ui.frmEditor		->SetHtml(m_recData.getDataRef(0, "BVC_DESCRIPTION").toString());
	//ui.chkPrivateCall	->setChecked(m_recData.getDataRef(0, "CENT_IS_PRIVATE").toInt());

	//refresh edit controls:
	ui.frameContact->RefreshDisplay();
	ui.frmSelectedProject->RefreshDisplay();
	ui.frmType->RefreshDisplay();
}

void FUI_VoiceCallCenter::SetWindowState(int nState)
{
	//if(m_nWndState==nState)
	//	return;

	//only one state allows phone number input
	AllowPhoneInput(WND_STATE_PreCall == nState);

	if(WND_STATE_After == nState){
		GUI_Helper::SetStyledButtonColorBkg(ui.btnSave,	 GUI_Helper::BTN_COLOR_GREEN_1);
		GUI_Helper::SetStyledButtonColorBkg(ui.btnCancel, GUI_Helper::BTN_COLOR_YELLOW);
	}

	//change button visibility/icon/color based on a new state
	switch(nState){
		case WND_STATE_PreCall:
			qDebug("Set Skype window state: WND_STATE_PreCall");
			ui.btnCallAction1->show();
			ui.btnCallAction2->hide();
			ui.btnCallAction3->hide();
			
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_GREEN,"white");
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction2, GUI_Helper::BTN_COLOR_BROWN,"white");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Call Now!"), ":accept.png");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction2, tr("Hangup"), ":hangup.png");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction3, tr("Hangup"), ":hangup.png"); //FIX this fixes some strange things, do not delete
			break;

		case WND_STATE_Ringing_Inc:
			qDebug("Set Skype window state: WND_STATE_Ringing_Inc");
			ui.radIncoming->setChecked(true);
			ui.radOutgoing->setChecked(false);
			m_recData.setData(0, "BVC_DIRECTION", 1);	//incoming
			ui.btnPickPhone->hide();

			ui.btnCallAction1->show();
			ui.btnCallAction2->show();
			ui.btnCallAction3->show();
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_GREEN, "white");
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction2, GUI_Helper::BTN_COLOR_PINK);
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction3, GUI_Helper::BTN_COLOR_BROWN, "white");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Accept Call"), ":accept.png");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction2, tr("Redirect To..."), ":redirect.png");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction3, tr("Hangup"), ":hangup.png");
			m_callRingingTime = QTime::currentTime();
			break;

		case WND_STATE_Ringing_Out:
			qDebug("Set Skype window state: WND_STATE_Ringing_Out");
			ui.radOutgoing->setChecked(true);
			ui.radIncoming->setChecked(false);
			m_recData.setData(0, "BVC_DIRECTION", 0);	//outgoing

			ui.btnCallAction1->show();
			ui.btnCallAction2->hide();
			ui.btnCallAction3->hide();
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_BROWN, "white");
			//GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction2, GUI_Helper::BTN_COLOR_GREEN);
			//GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction3, GUI_Helper::BTN_COLOR_PINK);
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Hangup"), ":hangup.png");
			//GUI_Helper::SetButtonTextIcon(ui.btnCallAction2, tr("Accept Call"), ":accept.png");
			//GUI_Helper::SetButtonTextIcon(ui.btnCallAction3, tr("Redirect To..."), ":redirect.png");
			m_callRingingTime = QTime::currentTime();
			break;

		case WND_STATE_Talking:
			{
				qDebug("Set Skype window state: WND_STATE_Talking");
				GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_BROWN, "white");
				GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction2, GUI_Helper::BTN_COLOR_SAND);
				GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction3, GUI_Helper::BTN_COLOR_PINK);
				GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Hangup"), ":hangup.png");
				GUI_Helper::SetButtonTextIcon(ui.btnCallAction2, tr("Hold"), "");
			
			#ifdef _WIN32
				//is this call in conference
				int nConfID = g_objParser.Info_GetCall(m_nCallID).m_nConfID;
				bool bTransfered = g_objParser.Info_GetCall(m_nCallID).m_bTransfered;
				int nASide_CallID = g_objParser.Info_GetTransferedCallID();
				if(nConfID > 0 || bTransfered || (nASide_CallID > 0 && nASide_CallID!= m_nCallID))
					GUI_Helper::SetButtonTextIcon(ui.btnCallAction3, tr("Connect Now"), ":redirect.png");
				else
					GUI_Helper::SetButtonTextIcon(ui.btnCallAction3, tr("Connect To..."), ":redirect.png");
			#endif

				ui.btnCallAction1->show();
				ui.btnCallAction2->show();
				ui.btnCallAction3->show();

				//mark start date/time
				if( WND_STATE_Ringing_Inc == m_nWndState ||
					WND_STATE_Ringing_Out == m_nWndState)	// we came here from ringing state
				{
					ui.txtCallDate->setDate(QDate::currentDate());
					ui.txtCallStartTime->setText(QTime::currentTime().toString("hh:mm:ss")); 

					//start timer to calculate 
					if(NULL == m_pDurationTimer){
						m_pDurationTimer = new QTimer(this);
						connect(m_pDurationTimer, SIGNAL(timeout()), this, SLOT(OnCallDurationTimer()) );
						m_pDurationTimer->start(1000); //refresh once a second
					}
				}
			}
			break;

		case WND_STATE_OnHold:
			qDebug("Set Skype window state: WND_STATE_OnHold");
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_GREEN, "white");
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction2, GUI_Helper::BTN_COLOR_PINK);
			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction3, GUI_Helper::BTN_COLOR_BROWN, "white");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Resume"), "");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction2, tr("Connect To..."), ":redirect.png");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction3, tr("Hangup"), ":hangup.png");
			ui.btnCallAction1->show();
			ui.btnCallAction2->show();
			ui.btnCallAction3->show();
			break;

		case WND_STATE_After:
			qDebug("Set Skype window state: WND_STATE_After");
			if(m_bCallWasEstablished)
				ui.btnCallAction1->hide();
			else
			{
				//if call was not consumed, allow reuse of the same window to make another call (to the same caller)
				if(!m_bIncoming){
					GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_GREEN,"white");
					GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Call Now!"), ":accept.png");
				}
				else{
					GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_GREEN,"white");
					GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Call Back Now!"), ":accept.png");
				}
			}
			ui.btnCallAction2->hide();
			ui.btnCallAction3->hide();

			//mark end time, calculate duration
			{
				QString strFrom = ui.txtCallStartTime->text();
				QString strTo   = QDateTime::currentDateTime().toString("hh:mm:ss");

				if(!strFrom.isEmpty())
				{
					ui.txtCallEndTime->setText(strTo); 
					RecalcDuration();
				}
				else
				{
					//missed call takes "ringing started" time for "call start time"
					ui.txtCallStartTime->setText(m_callRingingTime.toString("hh:mm:ss"));
					ui.txtCallDate->setDate(QDate::currentDate());
				}
			}

		#ifdef _WIN32
			//reset current call id
			if(m_nCallID == g_objParser.m_nCurCallID)
				g_objParser.m_nCurCallID = -1;

			//reset other data
			//if(m_nCallID == g_objParser.m_nCurTransferedCallID)
			g_objParser.m_nCurTransferedCallID = -1;
		#endif
			break;

		default:
			Q_ASSERT(FALSE);
	}

	m_nWndState = nState;	// store state

	//emit signal to combo box that FUI changed state (and probably name):
	RefreshFuiName();
}

void FUI_VoiceCallCenter::AllowPhoneInput(bool bAllow)
{
	ui.labelPhoneNumber->setVisible(!bAllow);
	ui.labelInterface->setVisible(!bAllow);
	ui.txtConfInfoHelp->setVisible(!bAllow);

	ui.cboInterface->setVisible(bAllow);
	ui.txtPhoneNumber->setVisible(bAllow);
	ui.btnPickPhone->setEnabled(bAllow);
}

void FUI_VoiceCallCenter::SetCallID(int nID)
{ 
	m_nCallID = nID; 

	//if(m_nCallConferenceID >= 0) //TOFIX
	{
		//next check fixes problems when we switch-on the call listening in the menu
		//in the middle of existing call (such call would be kept as conference ID even after closed)
		if(CommGridViewHelper::CALL_STATE_InProgress == m_nCallState)
		{
	#ifdef _WIN32
			//is this call outgoing
			bool bOut = g_objParser.Info_GetCall(m_nCallID).m_bOutCall;
			int nConfID = g_objParser.Info_CalcJoinConferenceID(m_nCallID);
			if(bOut && nConfID > 0)
			{
				//we need to join existing conference		
				QString strCmd = QString().sprintf("SET CALL %d JOIN_CONFERENCE %d", m_nCallID, nConfID);
				SkypeManager::SendCommand(strCmd.toLatin1().constData());
				//set temoporary conf ID, so we don't call join twice
				g_objParser.Info_OnCallConferenceID(m_nCallID, 999999);
				g_objParser.Info_OnCallConferenceID(nConfID, 999999);

				//unblock the calls
				//strCmd = QString().sprintf("SET CALL %d STATUS INPROGRESS", g_objParser.m_nCurCallID);
				//SkypeManager::SendCommand(strCmd.toLatin1().constData());
			}
	#endif
		}
	}
}

void FUI_VoiceCallCenter::on_btnCallAction1_clicked()
{
	if( WND_STATE_PreCall == m_nWndState || 
		(!m_bCallWasEstablished && !m_bIncoming && WND_STATE_After == m_nWndState))
	{
		QString strNumber = ui.txtPhoneNumber->text();
		if(strNumber != m_strFmtNumber)
			StartCall(strNumber, strNumber);
		else
			StartCall(m_strUnfmtNumber, m_strFmtNumber);
	}
	else if (!m_bCallWasEstablished && m_bIncoming && WND_STATE_After == m_nWndState)
	{
		//call back the missed incoming call - open the new FUI
		int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false);
		FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
		if(NULL == pFUI) return;	//someone closed the FUI for this call

		pFUI->move(pFUI->x()+20, pFUI->y()+20);
		pFUI->SetCaller(m_strNumber);
		pFUI->StartCall(m_strNumber, m_strFmtNumber);	// start the call imediately (known callee)
	}
	else if(WND_STATE_Ringing_Inc == m_nWndState)
	{
		//accept call
		QString strCmd = QString().sprintf("SET CALL %d STATUS INPROGRESS", m_nCallID);
		SkypeManager::SendCommand(strCmd.toLatin1().constData());
		SetWindowState(WND_STATE_Talking);
	}
	else if(WND_STATE_Ringing_Out == m_nWndState)
	{
		//"hangup" button to stop ringing in progress
		EndCall();
		SetWindowState(WND_STATE_After);
	}
	else if(WND_STATE_Talking == m_nWndState)
	{
		//"hangup" button to stop call in progress
		EndCall();
		SetWindowState(WND_STATE_After);
	}
	else if(WND_STATE_OnHold == m_nWndState)
	{
		Q_ASSERT(m_bCallOnHold);

		//remove "on hold" state
		QString strCmd = QString().sprintf("SET CALL %d STATUS INPROGRESS", m_nCallID);
		SkypeManager::SendCommand(strCmd.toLatin1().constData());
		m_bCallOnHold = false;
		SetWindowState(WND_STATE_Talking);
	}
}

void FUI_VoiceCallCenter::on_btnCallAction2_clicked()
{
	if(WND_STATE_Ringing_Inc == m_nWndState)
	{
		//check FP access rights
		int nValue;
		if(!g_FunctionPoint.IsFPAvailable(FP_TELEPHONY_SKYPE_CALL_TRANSFER, nValue)){
			QMessageBox::information(NULL, "", tr("You don't have access rights to transfer the call!"));
			return;
		}

		//TOFIX redirect call - start dialog
		VoiceCallRedirectDlg dlg;
		if(dlg.exec() > 0)
		{
			m_bForwarded = true;
#if 1
			qDebug() << "m_bForwarded" << m_bForwarded << "m_nCallID=" << m_nCallID;

			//this is a new API available since Skype v3.0 beta
			QString strCmd = QString().sprintf("ALTER CALL %d TRANSFER %s", m_nCallID, dlg.m_strNumber.toLatin1().constData());
			SkypeManager::SendCommand(strCmd.toLatin1().constData());
#else
			//STEP 1: set call forwarding information
			QString strCmd = QString("SET PROFILE CALL_APPLY_CF True");
			SkypeManager::SendCommand(strCmd.toLatin1().constData());
			
			strCmd = QString("SET PROFILE CALL_SEND_TO_VM False");
			SkypeManager::SendCommand(strCmd.toLatin1().constData());
			
			//number of times a call rings on a forwarded number before timing out
			strCmd = QString("SET PROFILE CALL_NOANSWER_TIMEOUT 30");
			SkypeManager::SendCommand(strCmd.toLatin1().constData());
			
			//TOFIX not needed?, causes error "invalid property"
			//strCmd = QString("SET PROFILE CALL_NOANSWER_ACTION FORWARD");
			//SkypeManager::SendCommand(strCmd.toLatin1().constData());

			strCmd = QString().sprintf("SET PROFILE CALL_FORWARD_RULES 0,45,%s", dlg.m_strNumber.toLatin1().constData());
			SkypeManager::SendCommand(strCmd.toLatin1().constData());

			//STEP 2: forward the call
			//TOFIX??? for older protocol versions use deprecated "SET_CALL xxx STATUS"
			strCmd = QString().sprintf("ALTER CALL %d END FORWARD_CALL", m_nCallID);
			SkypeManager::SendCommand(strCmd.toLatin1().constData());

			//STEP 3: reset call forwarding information
			strCmd = QString("SET PROFILE CALL_APPLY_CF False");
			SkypeManager::SendCommand(strCmd.toLatin1().constData());

			//reset (temporary) rules
			strCmd = QString("SET PROFILE CALL_FORWARD_RULES");
			SkypeManager::SendCommand(strCmd.toLatin1().constData());
#endif
			SetWindowState(WND_STATE_After);
		}
	}
	else if(WND_STATE_Talking == m_nWndState)
	{
		//"Hold" button - set "on hold"
		QString strCmd = QString().sprintf("SET CALL %d STATUS ONHOLD", m_nCallID);
		SkypeManager::SendCommand(strCmd.toLatin1().constData());
		m_bCallOnHold = true;
		SetWindowState(WND_STATE_OnHold);
	}
	else if(WND_STATE_OnHold == m_nWndState)
	{
		//clicking "Connect To" button when inside "On Hold" state
		//check FP access rights
		int nValue;
		if(!g_FunctionPoint.IsFPAvailable(FP_TELEPHONY_SKYPE_CALL_TRANSFER, nValue)){
			QMessageBox::information(NULL, "", tr("You don't have access rights to transfer the call!"));
			return;
		}

#if 1
		//use new Skype 3.0 API
		VoiceCallRedirectDlg dlg;
		if(dlg.exec() > 0)
		{
			m_bForwarded = true;

			qDebug() << "m_bForwarded" << m_bForwarded << "m_nCallID=" << m_nCallID;
			//this is a new API available since Skype v3.0 beta
			QString strCmd = QString().sprintf("ALTER CALL %d TRANSFER %s", m_nCallID, dlg.m_strNumber.toLatin1().constData());
			SkypeManager::SendCommand(strCmd.toLatin1().constData());
		}
#else
		//set current call "on hold"
		QString strCmd = QString().sprintf("SET CALL %d STATUS ONHOLD", m_nCallID);
		SkypeManager::SendCommand(strCmd.toLatin1().constData());
		m_bCallOnHold = true;
		SetWindowState(WND_STATE_OnHold);

		//mark this call as being the one to be transfered
		g_objParser.m_nCurTransferedCallID = m_nCallID;

		// "Connect to" button opens new FUI
		g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false);
#endif
	}
}

void FUI_VoiceCallCenter::on_btnCallAction3_clicked()
{
	if(WND_STATE_Ringing_Inc == m_nWndState)
	{
		//"hangup" button to stop incoming call
		EndCall();
		SetWindowState(WND_STATE_After);
	}
	else if(WND_STATE_Talking == m_nWndState)
	{
		//"Connect To"  button clicked when in talking mode
		//check FP access rights
		int nValue;
		if(!g_FunctionPoint.IsFPAvailable(FP_TELEPHONY_SKYPE_CALL_TRANSFER, nValue)){
			QMessageBox::information(NULL, "", tr("You don't have access rights to transfer the call!"));
			return;
		}

#if 1
	#ifdef _WIN32
		//is this call in conference
		int nConfID = g_objParser.Info_GetCall(m_nCallID).m_nConfID;
		bool bTransfered = g_objParser.Info_GetCall(m_nCallID).m_bTransfered;
		int nASide_CallID = g_objParser.Info_GetTransferedCallID();
		if(nConfID > 0 || bTransfered || (nASide_CallID > 0 && nASide_CallID!= m_nCallID))
		{
			//this is C window -> kill C, then redirect A to C
			//Q_ASSERT(g_objParser.m_nCurTransferedCallID >= 0);
			//int nASide_CallID = g_objParser.m_nCurTransferedCallID;

			//kill C call
			if(!IsPhoneNumber(g_objParser.m_strTransferToHandle.toLatin1().constData()))
				EndCall();

			//kill "on hold" state for A
			QString strCmd = QString().sprintf("SET CALL %d STATUS INPROGRESS", nASide_CallID);
			SkypeManager::SendCommand(strCmd.toLatin1().constData());

		#ifdef _WIN32
			//without this call forwarding to PSTN does not work!!!
			::Sleep(3000);
		#endif

			m_bForwarded = true;
			qDebug() << "m_bForwarded" << m_bForwarded << "m_nCallID=" << m_nCallID << "nASide_CallID" << nASide_CallID;

			//redirect A to C
			//this is a new API available since Skype v3.0 beta
			strCmd = QString().sprintf("ALTER CALL %d TRANSFER %s", nASide_CallID, g_objParser.m_strTransferToHandle.toLatin1().constData());
			SkypeManager::SendCommand(strCmd.toLatin1().constData());

			g_objParser.m_strTransferToHandle  = ""; //reset data
			g_objParser.m_nCurTransferedCallID = -1; //reset conference ID
		}
		else
		{
			//use new Skype 3.0 API
			VoiceCallRedirectDlg dlg;
			if(dlg.exec() > 0)
			{
				g_objParser.m_strTransferToHandle = dlg.m_strNumber;

				//for old API, fake redirection through the conference
				//mark this call as being the one to be transfered
				//g_objParser.m_nCurTransferedCallID = m_nCallID;
				g_objParser.Info_GetCall(m_nCallID).m_bTransfered = true;

				int nNewFUI=g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false);
				FUI_VoiceCallCenter* pFui=dynamic_cast<FUI_VoiceCallCenter*>(g_objFuiManager.GetFUIWidget(nNewFUI));
				if (pFui)
				{
					pFui->StartCall(dlg.m_strNumber.toLatin1().constData(), dlg.m_strNumber.toLatin1().constData());
					pFui->show();  
					//m_bCallOnHold = true;
				}

				SetWindowState(WND_STATE_OnHold);
			}
		}
	#endif
#else
		//for old API, fake redirection through the conference
		//if(m_nCallConferenceID < 0) //TOFIX
		{
			//mark this call as being the one to be transfered
			g_objParser.Info_GetCall(m_nCallID).m_bTransfered = true;
			//g_objParser.m_nCurTransferedCallID = m_nCallID; //TOFIX remove

			//set current call "on hold"
			QString strCmd = QString().sprintf("SET CALL %d STATUS ONHOLD", m_nCallID);
			SkypeManager::SendCommand(strCmd.toLatin1().constData());
			m_bCallOnHold = true;

			g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false);

			SetCallState(WND_STATE_OnHold);
		}
		else
		{
			//we need to join existing conference		
			QString strCmd = QString().sprintf("SET CALL %d JOIN_CONFERENCE %d", m_nCallID, m_nCallConferenceID);
			SkypeManager::SendCommand(strCmd.toLatin1().constData());

			//FIX: adding new call into the conference automatically releases "hold" state of the first call
			//"un-mute" the first caller
			//strCmd = QString().sprintf("SET CALL %d STATUS INPROGRESS", m_nCallConferenceID);
			//SkypeManager::SendCommand(strCmd.toLatin1().constData());

			g_objParser.m_nCurTransferedCallID = -1; // reset conference ID

			//set myself to mute
			strCmd = QString().sprintf("SET MUTE ON");
			SkypeManager::SendCommand(strCmd.toLatin1().constData());

			//TOFIX mute my speaker
			strCmd = QString().sprintf("SET PCSPEAKER OFF");
			SkypeManager::SendCommand(strCmd.toLatin1().constData());
		}
#endif
	}
	else if(WND_STATE_OnHold == m_nWndState)
	{
		//"hangup" button to stop "on hold" call 
		EndCall();
		SetWindowState(WND_STATE_After);
	}
}

void FUI_VoiceCallCenter::SetCaller(QString strNumber)
{
	ui.txtPhoneNumber->setText(strNumber);
	ui.labelPhoneNumber->setText(strNumber);
	ui.labelInterface->setText(ui.cboInterface->currentText());
	OnRemoteNumberKnown(strNumber, (ui.cboInterface->currentIndex() == 0) ? 1 : 0);
}

void FUI_VoiceCallCenter::SetCallState(QString strStatus)
{
	//update call status flag
	SkypeInterface::SetCallState(strStatus);

	if("ROUTING" == strStatus || "RINGING" == strStatus) //avoid mess in title bar
	{
		m_strStatus="RINGING";
		//setWindowTitle(tr("Phone Call Center - Call status: ") + "RINGING");
		return;
	}

	if(strStatus == QString("REFUSED") && m_bForwarded)
		m_strStatus = "FORWARDED";
	else
		m_strStatus = strStatus;

	//setWindowTitle(tr("Phone Call Center - Call status: ") + strStatus);

	if(QString("INPROGRESS") == strStatus)
	{
		SetWindowState(WND_STATE_Talking);
	}
	else if(QString("FAILED") == strStatus ||
			QString("FINISHED") == strStatus ||
			QString("MISSED") == strStatus	 ||
			QString("BUSY") == strStatus	 ||
			QString("REFUSED") == strStatus)
	{
		SetWindowState(WND_STATE_After);
	}
	
	if(m_nCallState==CommGridViewHelper::CALL_STATE_InProgress)
	{
		m_bCallWasEstablished = true;	

#ifdef _WIN32
		int nConfID = g_objParser.Info_CalcJoinConferenceID(m_nCallID);
		bool bTransfered = g_objParser.Info_GetCall(m_nCallID).m_bTransfered;
		if(nConfID > 0 && !m_bForwarded && !bTransfered)
		{
			//aaa
			//we need to join existing conference		
			QString strCmd = QString().sprintf("SET CALL %d JOIN_CONFERENCE %d", m_nCallID, nConfID);
			SkypeManager::SendCommand(strCmd.toLatin1().constData());

			//set temoporary conf ID, so we don't call join twice
			g_objParser.Info_OnCallConferenceID(m_nCallID, 999999);
			g_objParser.Info_OnCallConferenceID(nConfID, 999999);

			//unblock the calls
			//strCmd = QString().sprintf("SET CALL %d STATUS INPROGRESS", g_objParser.m_nCurCallID);
			//SkypeManager::SendCommand(strCmd.toLatin1().constData());
		}
#endif
	}
	else if(m_nCallState == CommGridViewHelper::CALL_STATE_Forwarded){
		m_bCallWasEstablished = true;
		m_nCallState = CommGridViewHelper::CALL_STATE_Forwarded;
	}
	else if(m_nCallState == CommGridViewHelper::CALL_STATE_OnHold){
		m_bCallOnHold = true;
		m_nCallState = CommGridViewHelper::CALL_STATE_OnHold;
		SetWindowState(WND_STATE_OnHold);

		if(m_bWaitForOnHold)
		{
			m_bWaitForOnHold = false;

			//prepare command
			//QString strCmd = "CALL " + m_strRedirectTo;
			//SkypeManager::SendCommand(strCmd.toLatin1().constData());
			SkypeManager::Call(m_strRedirectTo);
			SetWindowState(WND_STATE_Ringing_Out);
			
		#ifdef _WIN32
			//register this window as outgoing call source
			g_objParser.m_nCurOutCallFUI = g_objFuiManager.GetFuiID(this);
		#endif
		}
	}


	//Eventum #1476 ("If a phone call is missed, deactivate the Schedule pushbutton")
	if(CommGridViewHelper::CALL_STATE_Missed == m_nCallState)
	{
		ui.btnScheduleTask->setEnabled(false);
	}
}

QString FUI_VoiceCallCenter::GetFUIName()
{
	DbRecordSet recUserData;
	g_pClientManager->GetUserData(recUserData);
	//recUserData.Dump();
	QString strFullName = recUserData.getDataRef(0, "CUSR_FIRST_NAME").toString();
	strFullName += " ";
	strFullName += recUserData.getDataRef(0, "CUSR_LAST_NAME").toString();

	QString strTitle;
	if(m_strStatus.isEmpty())
	{
		strTitle = tr("Phone Call Center");
		//if(!g_objParser.m_strMyFullName.isEmpty())
			strTitle += " - " + strFullName /*g_objParser.m_strMyFullName*/;
	}
	else
	{
		//if the contact has been set (selected), use full name from contact instead of Skype name
		QString strRemoteName = m_strFullName;
		QVariant objContactID = m_recData.getDataRef(0, "BVC_CONTACT_ID");
		if(!objContactID.isNull()){
			strRemoteName = ui.frameContact->GetCurrentDisplayName();
		}

		if(/*!g_objParser.m_strMyFullName.isEmpty() &&*/
		   !strRemoteName.isEmpty())
		{
			if(m_bIncoming)
				strTitle = QString(tr("Phone Call From %1 For %2: %3")).arg(strRemoteName).arg(strFullName /*g_objParser.m_strMyFullName*/).arg(m_strStatus);
			else
				strTitle = QString(tr("Phone Call From %1 For %2: %3")).arg(strFullName /*g_objParser.m_strMyFullName*/).arg(strRemoteName).arg(m_strStatus);
		}
		else
			strTitle = tr("Phone Call Center - Call status: ") + m_strStatus;
	}

	return strTitle;
}

void FUI_VoiceCallCenter::on_btnCancel_clicked()
{
	//end call before closing the FUI window
	EndCall();

	g_objFuiManager.CloseFUI(g_objFuiManager.GetFuiID(this));
}

void FUI_VoiceCallCenter::on_btnStartTimeSetCurrent_clicked()
{
	ui.txtCallStartTime->setText(QTime::currentTime().toString("hh:mm:ss")); 

	//set current date
	ui.txtCallDate->setDate(QDate::currentDate());
}

void FUI_VoiceCallCenter::on_btnEndTimeSetCurrent_clicked()
{
	if(ui.txtCallStartTime->text().isEmpty()){
		QMessageBox::information(NULL, "", tr("You must first define a start time!"));
		return;
	}

	ui.txtCallEndTime->setText(QTime::currentTime().toString("hh:mm:ss")); 
	RecalcDuration();
}

void FUI_VoiceCallCenter::OnCallDurationTimer()
{
	if( WND_STATE_Talking != m_nWndState &&
		WND_STATE_OnHold  != m_nWndState)
	{
		m_pDurationTimer->stop();
		return;
	}

	RecalcDuration(true);
}

void FUI_VoiceCallCenter::OnRemoteNumberKnown(QString strNumber, int nInterface)
{
	m_strNumber = strNumber;

	//TOFIX when you receive remote number, make a contact/projects search - set a filter?
	FilterContacts(strNumber, nInterface);
	//TOFIX FilterProjects(strNumber, nInterface);	//only projects related to listed contacts

	if(nInterface == 1)
	{
	#ifdef _WIN32
		//get remote name
		g_objParser.SetSkypeUserInfoWnd(this);	//set result target
	#endif

		SkypeManager::SendCommand(QString().sprintf("GET USER %s FULLNAME", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s BIRTHDAY", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s SEX", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s COUNTRY", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s PROVINCE", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s CITY", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s PHONE_HOME", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s PHONE_OFFICE", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s PHONE_MOBILE", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s HOMEPAGE", strNumber.toLatin1().constData()).toLatin1().constData());
		SkypeManager::SendCommand(QString().sprintf("GET USER %s ABOUT", strNumber.toLatin1().constData()).toLatin1().constData());
	}

	//if no contact was selected, see if we can find contact by remote number
	int nContactID;
	if(!ui.frameContact->GetCurrentEntityRecord(nContactID))
	{
		//find contact by number
		QString strWhere; 
		strWhere.sprintf("INNER JOIN BUS_CM_PHONE ON BUS_CM_PHONE.BCMP_CONTACT_ID=BUS_CM_CONTACT.BCNT_ID WHERE BUS_CM_PHONE.BCMP_SEARCH ='%s'", strNumber.toLatin1().constData());
		Status err;
		DbRecordSet lstData;
		_SERVER_CALL(ClientSimpleORM->Read(err,BUS_CM_CONTACT,lstData,strWhere))
		_CHK_ERR(err);
		if (lstData.getRowCount()<1)
			return;

		nContactID = lstData.getDataRef(0, "BCNT_ID").toInt();
		ui.frameContact->SetCurrentEntityRecord(nContactID);
		LoadContactData();
		RefreshDisplay();
	}
}

void FUI_VoiceCallCenter::FilterContacts(QString strNumber, int nInterface, bool bFromManualSearch)
{
	//---> reformat from input to search string:
	FormatPhone PhoneFormatter;
	QString strPhone,strCountry,strLocal,strISO,strArea,strSearch;
	PhoneFormatter.SplitPhoneNumber(strNumber,strCountry,strArea,strLocal,strSearch,strISO);


	//first check if number is assigned to existed contact, if so exit, else, proceeed:
	int nContactID;
	if(ui.frameContact->GetCurrentEntityRecord(nContactID))
	{
		QString strWhere; 
		strWhere.sprintf("INNER JOIN BUS_CM_PHONE ON BUS_CM_PHONE.BCMP_CONTACT_ID=BUS_CM_CONTACT.BCNT_ID WHERE BUS_CM_PHONE.BCMP_SEARCH ='%s'", strSearch.toLatin1().constData());
		strWhere+=" AND BCNT_ID="+QVariant(nContactID).toString();
		Status err;
		DbRecordSet lstData;
		_SERVER_CALL(ClientSimpleORM->Read(err,BUS_CM_CONTACT,lstData,strWhere))
		_CHK_ERR(err);
		if (lstData.getRowCount()>0)
			return;
	}
}

void FUI_VoiceCallCenter::on_btnSave_clicked()
{
	/*
#ifdef _DEBUG
	//test for embedded pix
	QString strTmp = ui.frmEditor->GetHtml();
	QStringList lstResOrigFiles;
	EmailHelper::EmbedHtmlPix(strTmp, lstResOrigFiles);
	qDebug() << lstResOrigFiles;
	QStringList lstResTempFiles;
	EmailHelper::UnembedHtmlPix(strTmp, lstResTempFiles);
	qDebug() << lstResTempFiles;
#endif
	*/

	//u never know, before save, intercept
	if (m_recData.getDataRef(0,"BVC_ID").toInt()==0) 
	{
		Status err;
		if(!g_AccessRight->IsOperationAllowed(err,BUS_VOICECALLS,UserAccessRight::OP_INSERT))
			_CHK_ERR(err);
	}
	else
	{
		Status err;
		if(!g_AccessRight->IsOperationAllowed(err,BUS_VOICECALLS,UserAccessRight::OP_EDIT,m_recData.getDataRef(0,"BVC_ID").toInt()))
			_CHK_ERR(err);
	}



	//m_recData.Dump();

	//save nmrx:
	CheckForProjectContactAssigment();

	//end call before closing the FUI window
	EndCall();

	RecalcDuration();

	//
	// save data before closing
	//
	UpdateDataFromGui();

	//check FP access rights
	int nValue;
	if(g_FunctionPoint.IsFPAvailable(FP_TELEPHONY_LIMITATION_OF_REGISTERED_PHONE_CALLS, nValue) && nValue > 0)
	{
		//check current number of calls
		Status status;
		int nCnt;
		_SERVER_CALL(ClientSimpleORM->GetRowCount(status, BUS_VOICECALLS, "", nCnt))
		if(!status.IsOK()){
			QString strErr = status.getErrorText();
			QMessageBox::information(NULL, "", strErr);
			return;
		}
		if(nCnt >= nValue){
			QMessageBox::information(NULL, "", tr("You have reached the limit to the number of allowed calls stored!\nCan not save this call!"));
			return;
		}
	}


	//save the attached task (if exists)
	Status status;
	ui.frameTask->GetDataFromGUI();
	ui.frameTask->CheckTaskRecordBeforeSave(status);
	_CHK_ERR(status);


	bool bTaskActive = ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0;
	DbRecordSet tempTaskRec;
	if(bTaskActive)
	{
		//if task is active (scheduled) then set as completed:
		//mark task as done if was active & duration>0
		if (!ui.txtCallDuration->text().isEmpty() && ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt()>0 )
		{
			ui.frameTask->m_recTask.setData(0, "BTKS_SYSTEM_STATUS", GlobalConstants::TASK_STATUS_COMPLETED);
			ui.frameTask->m_recTask.setData(0, "BTKS_COMPLETED", QDateTime::currentDateTime());
			if (ui.frameTask->m_recTask.getDataRef(0, "BTKS_OWNER").isNull())
				ui.frameTask->m_recTask.setData(0, "BTKS_OWNER", g_pClientManager->GetPersonID());
		}

		tempTaskRec=ui.frameTask->m_recTask;

		if(GlobalConstants::TASK_STATUS_COMPLETED == tempTaskRec.getDataRef(0, "BTKS_SYSTEM_STATUS").toInt())
		{
			//#2707: remove avatar for this task if it exists
			//09.08.2013: BT fixed!!!
			EMailDialog::RemoveAvatar(AVATAR_TASK_CALL, m_recData.getDataRef(0, "BVC_ID").toInt());
		}
	}

	//m_recData.Dump();
	bool bInsertMode=false;
	if (m_recData.getDataRef(0,"BVC_ID").toInt()==0)
		bInsertMode=true;

	SetMode(MODE_READ);

	DbRecordSet lstUAR,lstGAR;
	ui.arSelector->Save(m_recData.getDataRef(0,"BVC_ID").toInt(),BUS_VOICECALLS,&lstUAR,&lstGAR); //get ar rows for save

	//write voice call record
	_SERVER_CALL(VoiceCallCenter->WriteCall(status, m_recData,"",tempTaskRec,lstUAR,lstGAR))
	if(!status.IsOK())
	{
		QString strErr = status.getErrorText();
		QMessageBox::information(NULL, "", strErr);
	}
	else
	{
		if (bTaskActive) //return task rec:
			ui.frameTask->m_recTask=tempTaskRec;

		//refresh cache and close
		if (bInsertMode)
			g_ClientCache.ModifyCache(ENTITY_BUS_VOICECALLS, m_recData, DataHelper::OP_ADD, this); 
		else
			g_ClientCache.ModifyCache(ENTITY_BUS_VOICECALLS, m_recData, DataHelper::OP_EDIT, this); 

		g_objFuiManager.CloseFUI(g_objFuiManager.GetFuiID(this));

		ui.arSelector->Load(m_recData.getDataRef(0,"BVC_ID").toInt(),BUS_VOICECALLS,&lstUAR,&lstGAR);
		ui.arSelector->SetRecordDisplayName(m_recData.getDataRef(0,"BVC_CALLER_ID").toString());
	}
}

// when Conatct or Project selection changes, set SAPNE to selected values
void FUI_VoiceCallCenter::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if(nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}

	FuiBase::updateObserver(pSubject,nMsgCode,nMsgDetail,val);

	//IF Contact selected
	if(pSubject==ui.frameContact && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
	{
		LoadContactData();
	}
	else if (pSubject==ui.frameContact && nMsgCode==SelectorSignals::SELECTOR_ON_AFTER_OPEN_INSERT_HANDLER)
		OnContactSAPNEInsert();
}

void FUI_VoiceCallCenter::OnContactSAPNEInsert()
{
	//new Contact is about to be inserted, fetch new FUI:
	FuiBase* pFUI=dynamic_cast<FuiBase*>(ui.frameContact->GetLastFUIOpen());
	if(pFUI==NULL) return;

	//get data holder:
	QCW_Base *pQCW=dynamic_cast<QCW_Base*>(pFUI);
	if(pQCW==NULL) return;

	//BoClientEntitySet* pBoEntity=static_cast<BoClientEntitySet*>(pFUI->GetBoEntity());
	//Q_ASSERT(pBoEntity!=NULL);
	
	//set rows:
	DbRecordSet dataContact;
	dataContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	ContactTypeManager::AddEmptyRowToFullContactList(dataContact);
	
	//insert data:
	//pQCW->SetDefaultValues(recContactFull,"");

	//try to insert default values

	//---------------------------------------------
	//contact data:
	//---------------------------------------------
	//new contact data (name):
	dataContact.setData(0,"BCNT_FIRSTNAME",m_strNumber);
	QString strFirstName,strLastName;
	if(!m_strFullName.isEmpty())
	{
		strFirstName= m_strFullName.simplified().section(" ",0,0);
		strLastName= m_strFullName.simplified().section(" ",1,1);
		dataContact.setData(0,"BCNT_FIRSTNAME",strFirstName);
		dataContact.setData(0,"BCNT_LASTNAME",strLastName);
	}
	if(!m_strBirthday.isEmpty())
	{
		QDate dateOfBirth=QDate::fromString(m_strBirthday,"yyyyMMdd");
		dataContact.setData(0,"BCNT_BIRTHDAY",dateOfBirth);
	}
	if(!m_strSex.isEmpty())
	{
		int nSex=(m_strSex=="MALE") ? ContactTypeManager::SEX_MALE : ContactTypeManager::SEX_FEMALE;
		dataContact.setData(0,"BCNT_SEX",nSex);
	}
	if(!m_strAbout.isEmpty())
	{
		QByteArray byteValue=m_strAbout.toLatin1().constData();
		dataContact.setData(0,"BCNT_DESCRIPTION",byteValue);
	}
	//pBoEntity->InsertDefaultValues(dataContact,BoClientEntity_BusContact::GROUP_MAIN);


	//---------------------------------------------
	//Web Address
	//---------------------------------------------
	if(!m_strHomepage.isEmpty())
	{
		DbRecordSet data;
		data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET));
		data.addColumn(QVariant::String,"BCMT_TYPE_NAME");
		data.addRow();
		data.setData(0,"BCMI_ADDRESS",m_strHomepage);
		dataContact.setData(0,"LST_INTERNET",data); 
		//pBoEntity->InsertDefaultValues(data,BoClientEntity_BusContact::GROUP_INTERNET);
	}

	//------------------------------------------------------------
	//Address (type?)TOFIX: adding default or specified types!!!
	//------------------------------------------------------------
	if(!m_strCountry.isEmpty())
	{
		DbRecordSet data;
		data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
		data.addColumn(QVariant::String,"BCMT_TYPE_NAME");
		data.addRow();
		data.setData(0,"BCMA_COUNTRY_CODE",Countries::GetISOCodeFromCountry(m_strCountry));
		data.setData(0,"BCMA_CITY",m_strCity);
		data.setData(0,"BCMA_REGION",m_strProvince);
		data.setData(0,"BCMA_FIRSTNAME",strFirstName);
		data.setData(0,"BCMA_LASTNAME",strLastName);
		m_ContactMan.FormatAddressWithDefaultSchema(data,ContactTypeManager::CM_TYPE_PERSON,ClientContactManager::FORMAT_USING_USERDEFAULTS,false); //format address
	//	BoClientEntity_CM_AddressSet* m_pBoAddressSet=static_cast<BoClientEntity_CM_AddressSet*>(pBoEntity->GetBoEntity(BoClientEntity_BusContact::GROUP_ADDRESS));
		//Q_ASSERT(m_pBoAddressSet!=NULL);
	//	BoClientEntity_CM_Address* m_pBoAddress=static_cast<BoClientEntity_CM_Address*>(m_pBoAddressSet->GetBoEntity(BoClientEntity_CM_AddressSet::GROUP_MAIN));
		//Q_ASSERT(m_pBoAddress!=NULL);
		//m_pBoAddress->InsertDefaultValues(data,BoClientEntity_BusContact::GROUP_ADDRESS);
		dataContact.setData(0,"LST_ADDRESS",data); 
		
	}


	//---------------------------------------------
	//new phone data:
	//---------------------------------------------
	DbRecordSet dataNewPhone;
	dataNewPhone.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE));
	if(!m_strNumber.isEmpty())
	{
		dataNewPhone.addRow();
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_FULLNUMBER",m_strNumber); //type=skype/TOFIX later for other interfaces:
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_TYPE_ID",ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE));
	}
	else if(!ui.txtPhoneNumber->text().isEmpty())	//not yet called
	{
		dataNewPhone.addRow();
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_FULLNUMBER",ui.txtPhoneNumber->text()); //type=skype/TOFIX later for other interfaces:
		if (IsPhoneNumber(ui.txtPhoneNumber->text()))
			dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_TYPE_ID",ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT));
		else
			dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_TYPE_ID",ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE));
	}

	if(!m_strPhoneHome.isEmpty())
	{
		dataNewPhone.addRow();
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_FULLNUMBER",m_strPhoneHome);
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_TYPE_ID",ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE));
	}
	if(!m_strPhoneOffice.isEmpty())
	{
		dataNewPhone.addRow();
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_FULLNUMBER",m_strPhoneOffice);
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_TYPE_ID",ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS));
	}
	if(!m_strPhoneMobile.isEmpty())
	{
		dataNewPhone.addRow();
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_FULLNUMBER",m_strPhoneMobile);
		dataNewPhone.setData(dataNewPhone.getRowCount()-1,"BCMP_TYPE_ID",ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE));
	}
	//pBoEntity->InsertDefaultValues(dataNewPhone,BoClientEntity_BusContact::GROUP_PHONE);
	FormatPhone phone;
	phone.ReFormatPhoneNumbers(dataNewPhone);
	dataContact.setData(0,"LST_PHONE",dataNewPhone); 

	//refresh FUI, setactivegroupvisible to Phones:

	//pFUI->SetActiveGroupVisible(BoClientEntity_BusContact::GROUP_PHONE);
	//insert data:
	pQCW->SetDefaultValues(dataContact,"");
}

void FUI_VoiceCallCenter::OnUserResolved_FullName(QString strNumber, QString strFullName)
{
	m_strFullName = strFullName;	// store remote caller name
	RefreshFuiName();
}

void FUI_VoiceCallCenter::OnUserResolved_Birthday(QString strNumber, QString strData)
{
	m_strBirthday = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_Sex(QString strNumber, QString strData)
{
	m_strSex = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_Country(QString strNumber, QString strData)
{
	m_strCountry = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_Province(QString strNumber, QString strData)
{
	m_strProvince = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_City(QString strNumber, QString strData)
{
	m_strCity = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_PhoneHome(QString strNumber, QString strData)
{
	m_strPhoneHome = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_PhoneOffice(QString strNumber, QString strData)
{
	m_strPhoneOffice = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_PhoneMobile(QString strNumber, QString strData)
{
	m_strPhoneMobile = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_Homepage(QString strNumber, QString strData)
{
	m_strHomepage = strData;
}

void FUI_VoiceCallCenter::OnUserResolved_About(QString strNumber, QString strData)
{
	m_strAbout = strData;
}

void FUI_VoiceCallCenter::RecalcDuration(bool bCurTime)
{
	//refresh duration
	QString strFrom = ui.txtCallStartTime->text();
	QTime from = ParseTime(strFrom);
	QTime to;
	if(bCurTime)
		to = QTime::currentTime();
	else
	{
		QString strTo = ui.txtCallEndTime->text();
		if(strTo.isEmpty()){
			ui.txtCallDuration->setText(QString());
			return;
		}
		to = ParseTime(strTo);
	}

	int nDiffSecs = from.secsTo(to);
	int nSec = nDiffSecs % 60;
	int nMin = nDiffSecs / 60;
	int nHour = nMin / 60;
	nMin  = nMin % 60;
	ui.txtCallDuration->setText(QString().sprintf("%02d:%02d:%02d", nHour, nMin, nSec));
}

bool IsPhoneNumber(QString strNumber)
{
	int nLen = strNumber.length();
	for(int i=0; i<nLen; i++)
	{
		if( !strNumber.at(i).isDigit() &&
			!strNumber.at(i).isSpace() &&
			strNumber.at(i).toLatin1() != '+')
		{
			return false;	// probably Skype handle
		}
	}

	return true;	// should be phone number
}

void FUI_VoiceCallCenter::BuildPhoneCache(DbRecordSet &lstPhones)
{
	m_lstPhonesFmt.clear();
	m_lstPhones.clear();
	m_lstPhoneNames.clear();
	m_lstPhoneTypes.clear();
	m_lstPhoneTypeIDs.clear();

	QString strInitialTxt = ui.txtPhoneNumber->text();

	int nSize=lstPhones.getRowCount();
	//lstPhones.Dump();
	for(int i=0;i<nSize;++i)
	{
		//we need formatted number
		QString strPhone = lstPhones.getDataRef(i,"BCMP_FULLNUMBER").toString();
		m_lstPhonesFmt	<< strPhone;
		m_lstPhones		<< lstPhones.getDataRef(i,"BCMP_SEARCH").toString();
		m_lstPhoneNames	<< lstPhones.getDataRef(i,"BCMP_NAME").toString();

		int nRow = m_recPhoneTypes.find("BCMT_ID", lstPhones.getDataRef(i,"BCMP_TYPE_ID").toString(), true);
		if(nRow >= 0){
			m_lstPhoneTypes		<< m_recPhoneTypes.getDataRef(nRow,"BCMT_TYPE_NAME").toString();
			m_lstPhoneTypeIDs	<< m_recPhoneTypes.getDataRef(nRow,"BCMT_SYSTEM_TYPE").toInt();
		}
		else{
			m_lstPhoneTypes	<< QString();
			m_lstPhoneTypeIDs << -1;
		}
	}
}

//load combo, sets current to default
void FUI_VoiceCallCenter::LoadPhoneCombo(DbRecordSet lstPhones)
{
	ui.txtPhoneNumber->blockSignals(true);
	if (!m_bSkipBackUpdate)
		ui.txtPhoneNumber->clear();
	
	BuildPhoneCache(lstPhones);

	//set current phone to default
	int nDefRow=lstPhones.find(lstPhones.getColumnIdx("BCMP_IS_DEFAULT"),1,true);
	if (nDefRow!=-1 && !m_bSkipBackUpdate)
	{
		ui.txtPhoneNumber->setText(m_lstPhonesFmt[nDefRow]);
		m_strUnfmtNumber = m_lstPhones[nDefRow];
		m_strFmtNumber = m_lstPhonesFmt[nDefRow];
	}
	//else if(!strInitialTxt.isEmpty())
	//	ui.txtPhoneNumber->setEditText(strInitialTxt);

	ui.txtPhoneNumber->blockSignals(false);
}

//set default contact into sapne, load phones, set default phone
//strPhoneDefault is full number: display search phone
//lstOfOtherContactsForConference (only BCNT_ID)
void FUI_VoiceCallCenter::SetDefaultContact(int nContactID,QString strPhoneDefault,int nProjectID)
{
	if (nContactID!=-1)
	{
		ui.frameContact->SetCurrentEntityRecord(nContactID);
	}

	int nIndex=m_lstPhones.indexOf(QRegExp(strPhoneDefault));
	if (nIndex!=-1){
		ui.txtPhoneNumber->setText(m_lstPhonesFmt[nIndex]);
		m_strUnfmtNumber = m_lstPhones[nIndex];
		m_strFmtNumber = m_lstPhonesFmt[nIndex];
	}

}

QTime FUI_VoiceCallCenter::ParseTime(QString strTime) 
{
	if(!strTime.isEmpty())
	{
		//check for invalid chars in the input
		int nLen = strTime.length();
		for(int i=0; i<nLen; i++)
		{
			if(!strTime.at(i).isDigit() &&
				strTime.at(i) != ':')
			{
				return QTime();	//error, invalid time
			}
		}

		int nHour = 0;
		int nMin  = 0;
		int nSec  = 0;

		int nPos = strTime.indexOf(":");
		if(nPos > 0){
			nHour = strTime.mid(0, nPos).toInt();

			strTime = strTime.mid(nPos+1);
			nPos = strTime.indexOf(":");
			if(nPos > 0){
				nMin = strTime.mid(0, nPos).toInt();

				strTime = strTime.mid(nPos+1);
				nSec = strTime.toInt();
			}
			else
				nMin = strTime.toInt();
		}
		else
			nHour = strTime.toInt();

		//test if parsed time is valid
		if(nHour > 23 || nMin > 59 || nSec > 59)
			return QTime();	//error, invalid time

		return QTime(nHour, nMin, nSec);	
	}

	return QTime();	//error, invalid time
}

//contact is clicked in grid: select it in sapne & load nmrx projects, if only one, set it in project sapne
void FUI_VoiceCallCenter::LoadContactData(bool bClickedOnGrid)
{
	//reload phones:
	Status err;
	int nContactId;
	DbRecordSet lstPhones;
	DbRecordSet lstPicture,lstDataNMRXContacts;
	DbRecordSet rowCurrentContact;
	m_lstDataNMRXProjects.clear();
	if(ui.frameContact->GetCurrentEntityRecord(nContactId,rowCurrentContact))
	{
		_SERVER_CALL(BusContact->ReadContactDataForVoiceCall(err,nContactId,lstPhones,lstPicture,lstDataNMRXContacts, m_lstDataNMRXProjects))
		if (!err.IsOK()) return;

		//if projects==1, set to sapne, but only if project sapne is empty (when user clicks on project it will clear project sapne)
		int nProjectCurrentID;
		if (m_lstDataNMRXProjects.getRowCount()==1 && !ui.frmSelectedProject->GetCurrentEntityRecord(nProjectCurrentID))
		{
			int nProjectID=m_lstDataNMRXProjects.getDataRef(0,"BUSP_ID").toInt();
			ui.frmSelectedProject->SetCurrentEntityRecord(nProjectID,true);
		}

	}

	//load phones:
	LoadPhoneCombo(lstPhones);

	//load pic
	rowContactPicture.clear();
	rowContactPicture.merge(lstPicture);
	ui.picture->RefreshDisplay();

	UpdateSkypeStatus();
	updateContactInfo();
}

//check if project & contact are assigned, if not ask, if do, assign
void FUI_VoiceCallCenter::CheckForProjectContactAssigment()
{
	Status err;
	int nProjectId,nContactId;
	DbRecordSet rowCurrentContact;
	DbRecordSet rowCurrentProject;

	//if both sapne are setted
	bool bSelectedContact=ui.frameContact->GetCurrentEntityRecord(nContactId,rowCurrentContact);
	bool bSelectedProject=ui.frmSelectedProject->GetCurrentEntityRecord(nProjectId,rowCurrentProject);

	if (!bSelectedContact || !bSelectedProject)
		return;			//both must be assigned

	DbRecordSet lstData;
	_SERVER_CALL(BusContact->ReadNMRXContactsFromProject(err,nProjectId,lstData))
	if (!err.IsOK()) return;
	int nRow=lstData.find(0,nContactId,true);
	if (nRow!=-1)
		return;			//already assigned

	//ask ?
	int nResult=QMessageBox::question(this,tr("Assign Project To Contact"),tr("Assign Project To Contact?"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES

	//nmrx:
	Dlg_NMRXEditor Dlg;

	DbRecordSet row;
	row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	row.addRow();
	row.setData(0,"BNMR_TABLE_1",BUS_CM_CONTACT);
	row.setData(0,"BNMR_TABLE_2",BUS_PROJECT);
	row.setData(0,"BNMR_TABLE_KEY_ID_1",nContactId);
	row.setData(0,"BNMR_TABLE_KEY_ID_2",nProjectId);
	row.setData(0,"BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);
	
	Dlg.SetRow(BUS_CM_CONTACT,nContactId,rowCurrentContact.getDataRef(0,"BCNT_NAME").toString(),row,Dlg_NMRXEditor::MODE_INSERT,tr("Contact"),tr("Project"));
	Dlg.exec();
}

void FUI_VoiceCallCenter::OnScheduleCall()
{
	int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false);
	FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
	if(NULL == pFUI) return;	//someone closed the FUI for this call

	UpdateDataFromGui();
	pFUI->SetShowTask(m_recData);

	//get contact:
	int nContactID=-1;
	DbRecordSet rowCurrentContact,lstContacts;
	if(ui.frameContact->GetCurrentEntityRecord(nContactID,rowCurrentContact))
	{
		lstContacts.addColumn(QVariant::Int,"BCNT_ID");
		lstContacts.addRow();
		lstContacts.setData(0,0,nContactID);
	}

	//get project:
	int nProjectID=-1;
	DbRecordSet rowCurrentPr,lstProjects;
	if(ui.frmSelectedProject->GetCurrentEntityRecord(nProjectID,rowCurrentContact))
	{
		lstProjects.addColumn(QVariant::Int,"BUSP_ID");
		lstProjects.addRow();
		lstProjects.setData(0,0,nProjectID);
	}

	pFUI->SetDefaultContact(nContactID,"",nProjectID);
}

void FUI_VoiceCallCenter::OnScheduleEmail()
{
	//get contact:
	int nContactID=-1;
	ui.frameContact->GetCurrentEntityRecord(nContactID);

	//get project:
	int nProjectID=-1;
	ui.frmSelectedProject->GetCurrentEntityRecord(nProjectID);

	//open Email widget:
	QPoint piePos=ui.btnCallAction2->mapToGlobal(QPoint(10,10));
	CommunicationActions::sendEmail(nContactID,"",nProjectID,&piePos,NULL,-1,false,true);
}

void FUI_VoiceCallCenter::OnScheduleDoc(int nDocType)
{
	//-> as new task

	//get contact:
	int nContactID=-1;
	DbRecordSet rowCurrentContact,lstContacts;
	if(ui.frameContact->GetCurrentEntityRecord(nContactID,rowCurrentContact))
	{
		lstContacts.addColumn(QVariant::Int,"BCNT_ID");
		lstContacts.addRow();
		lstContacts.setData(0,0,nContactID);
	}

	//get project:
	int nProjectID=-1;
	DbRecordSet rowCurrentPr,lstProjects;
	if(ui.frmSelectedProject->GetCurrentEntityRecord(nProjectID,rowCurrentContact))
	{
		lstProjects.addColumn(QVariant::Int,"BUSP_ID");
		lstProjects.addRow();
		lstProjects.setData(0,0,nProjectID);
	}

	int nNewFUI=g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EMPTY, -1,true);
	QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_DM_Documents* pFuiDM=dynamic_cast<FUI_DM_Documents*>(pFUI);
	if (pFuiDM)
	{
		pFuiDM->SetDefaults(nDocType,NULL,&lstContacts,&lstProjects,true);  
	}
	if(pFUI)pFUI->show();  //show FUI
}

void FUI_VoiceCallCenter::SetShowTask(DbRecordSet &recData, bool bActive)
{
	if(recData.getRowCount() > 0)
	{
		//recData.Dump();

		int nTaskIdCol = recData.getColumnIdx("BTKS_ID");
		if(nTaskIdCol>=0 && recData.getDataRef(0, nTaskIdCol).isNull())
		{
			//initialize data for new task
			m_recData.setData(0, "BVC_CALLER_ID", recData.getDataRef(0, "BVC_CALLER_ID").toString());
			m_recData.setData(0, "BVC_DESCRIPTION", recData.getDataRef(0, "BVC_DESCRIPTION").toString());
			if(!recData.getDataRef(0, "BVC_CONTACT_ID").isNull())
				m_recData.setData(0, "BVC_CONTACT_ID", recData.getDataRef(0, "BVC_CONTACT_ID").toInt());
			if(!recData.getDataRef(0, "BVC_PROJECT_ID").isNull())
				m_recData.setData(0, "BVC_PROJECT_ID", recData.getDataRef(0, "BVC_PROJECT_ID").toInt());
			if(!recData.getDataRef(0, "CENT_CE_TYPE_ID").isNull())
				m_recData.setData(0, "CENT_CE_TYPE_ID", recData.getDataRef(0, "CENT_CE_TYPE_ID").toInt());
		}
		else
		{
			if (nTaskIdCol!=-1) //copy task only if task record is inside recData
			{
				//copy data for the existing task
				ui.frameTask->m_recTask.clear();
				ui.frameTask->m_recTask.merge(recData);
			}
		}
	}

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Task widget set gui start"));

	ui.frameTask->m_pRecMain = &m_recData;	//attach main record
	ui.frameTask->m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE", (bActive) ? 1 : 0);
	ui.frameTask->SetGUIFromData();
	ui.frameTask->setVisible(true);

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Task widget set html"));

	ui.txtPhoneNumber->setText(m_recData.getDataRef(0, "BVC_CALLER_ID").toString());
	ui.frmEditor->SetHtml(m_recData.getDataRef(0, "BVC_DESCRIPTION").toString());

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Task widget set refresh display start"));

	if(bActive)	//FIX: it takes 3seconds to execute RefreshDisplay for the non active task!!!
	{
		RefreshDisplay();
		
		//set active tab to task details
		ui.tabWidget->setCurrentIndex(1);
		updateTaskTabCheckbox();
	}
}

void FUI_VoiceCallCenter::UpdateDataFromGui()
{
	QDate dateCall = ui.txtCallDate->date();
	if(!dateCall.isValid())
		dateCall = QDate::currentDate();	//date must be set

	QTime timeFrom = ParseTime(ui.txtCallStartTime->text());
	m_recData.setData(0, "BVC_START", QDateTime(dateCall, timeFrom));

	if("Skype" == ui.cboInterface->currentText())	//"Skype"
		m_recData.getDataRef(0, "BVC_DEVICE_CODE") = QVariant(1);
	else
		m_recData.getDataRef(0, "BVC_DEVICE_CODE") = QVariant(QVariant::Int);

	if(!ui.txtCallEndTime->text().isEmpty())
	{
		QTime timeTo = ParseTime(ui.txtCallEndTime->text());
		m_recData.setData(0, "BVC_END", QDateTime(dateCall, timeTo));

		int nDiffSecs = timeFrom.secsTo(timeTo);
		m_recData.setData(0, "BVC_DURATION", nDiffSecs);
	}
	
	if(!m_strNumber.isEmpty())
		m_recData.setData(0, "BVC_CALLER_ID", m_strNumber);
	else
		m_recData.setData(0, "BVC_CALLER_ID", ui.txtPhoneNumber->text());	//call not yet made
	m_recData.setData(0, "BVC_CALL_ID", m_nCallID);
	m_recData.setData(0, "BVC_DIRECTION", m_bForwarded? 2: ui.radIncoming->isChecked()? 1:0);
	m_recData.setData(0, "BVC_DESCRIPTION", ui.frmEditor->GetHtml());
	m_recData.setData(0, "BVC_CALL_STATUS", m_nCallState);
	//m_recData.setData(0, "CENT_IS_PRIVATE", ui.chkPrivateCall->isChecked() ? 1 : 0);

	//fill owner ID
	DbRecordSet recUserData;
	g_pClientManager->GetUserData(recUserData);
	//recUserData.Dump();

	if(!recUserData.getDataRef(0, "CUSR_PERSON_ID").isNull())
		m_recData.setData(0, "CENT_OWNER_ID", recUserData.getDataRef(0, "CUSR_PERSON_ID").toInt());
	
	//m_recData.Dump();
}

void FUI_VoiceCallCenter::OnCallStartTimeEdited()
{
	//try to quickly fix time format
	QString strTime = ui.txtCallStartTime->text();
	if(!strTime.isEmpty())
	{
		int nPos = strTime.indexOf(":");
		if(nPos < 0){
			strTime += ":00:00";
			ui.txtCallStartTime->setText(strTime);
		}
		else{	
			nPos = strTime.indexOf(":", nPos+1);
			if(nPos < 0){
				strTime += ":00";
				ui.txtCallStartTime->setText(strTime);
			}
		}

		//check if the time format is valid
		if(QTime::fromString(strTime).isNull()){
			QMessageBox::information(this, tr("Error"), tr("Invalid time!"));
			ui.txtCallStartTime->setFocus();
			return;		
		}

		// recalculate duration in case of manual time entry
		RecalcDuration();
	}
}

void FUI_VoiceCallCenter::OnCallEndTimeEdited()
{
	//try to quickly fix time format
	QString strTime = ui.txtCallEndTime->text();
	if(!strTime.isEmpty())
	{
		int nPos = strTime.indexOf(":");
		if(nPos < 0){
			strTime += ":00:00";
			ui.txtCallEndTime->setText(strTime);
		}
		else{
			nPos = strTime.indexOf(":", nPos+1);
			if(nPos < 0){
				strTime += ":00";
				ui.txtCallEndTime->setText(strTime);
			}
		}

		//check if the time format is valid
		if(QTime::fromString(strTime).isNull()){
			QMessageBox::information(this, tr("Error"), tr("Invalid time!"));
			ui.txtCallEndTime->setFocus();
			return;		
		}

		// recalculate duration in case of manual time entry
		RecalcDuration();
	}
}

//BT: 04.03.2013: to support FUI manager load....connected to avatar open
void FUI_VoiceCallCenter::on_selectionChange(int nEntityRecordID)
{
	LoadCallData(nEntityRecordID);
}

void FUI_VoiceCallCenter::LoadCallData(int nDbRecordID)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Load call data"));

	/*
	DbRecordSet lstRes, set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	lstRes.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_VOICECALLS));
	lstRes.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	lstRes.copyDefinition(set, false);

	Status status;
	//B.T. adapted to work when task is empty
	QString strWhere= QString("\
		FROM CE_COMM_ENTITY \
		INNER JOIN BUS_VOICECALLS ON BVC_COMM_ENTITY_ID=CENT_ID\
		LEFT OUTER JOIN BUS_TASKS ON CENT_TASK_ID=BTKS_ID\
		WHERE BVC_ID=%1").arg(nDbRecordID);

	_SERVER_CALL(ClientSimpleORM->ReadAdv(status, lstRes, strWhere))
	_CHK_ERR(status);
	*/

	Status err;
	if(!g_AccessRight->IsOperationAllowed(err,BUS_VOICECALLS,UserAccessRight::OP_READ,nDbRecordID))
		_CHK_ERR(err);

	
	DbRecordSet lstRes;
	Status status;
	DbRecordSet lstUAR,lstGAR;
	_SERVER_CALL(VoiceCallCenter->ReadCall(status, nDbRecordID, lstRes, lstUAR, lstGAR))
	_CHK_ERR(status);

	//B.T. adapted to work when task is empty
	if (lstRes.getRowCount()>0)
	{
		ui.arSelector->Load(lstRes.getDataRef(0,"BVC_ID").toInt(),BUS_VOICECALLS,&lstUAR,&lstGAR);
		ui.arSelector->SetRecordDisplayName(lstRes.getDataRef(0,"BVC_CALLER_ID").toString());

		m_recData.clear();
		m_recData.merge(lstRes);
		//lstRes.Dump();

		//if task is assigned:
		if (lstRes.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt()>0)
		{
			SetShowTask(lstRes, lstRes.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt());
		}
	}

	RefreshDisplay();

	LoadContactData(); //fix the problem when this fui is opened through avatar (empty phone list)

	m_nEntityRecordID =	nDbRecordID;
}

void FUI_VoiceCallCenter::StartCall(QString strNumber, QString strFormattedNum)
{
	if("Skype" == ui.cboInterface->currentText())
	{
		strNumber.replace(" ", "");	//there can be no spaces in the number/handle
		if(!strNumber.isEmpty())
		{
			m_bIncoming = false;
			ui.radOutgoing->setChecked(true);
			ui.radIncoming->setChecked(false);
			m_recData.setData(0, "BVC_DIRECTION", 0);	//outgoing

			SetCaller(strFormattedNum);

		#ifdef _WIN32
			//NOTE: in new Skype v3.8, you can not start new call until you get incoming message
			//		that confirms that the old code was put into ONHOLD state (before it worked directly with no waiting)
			//if the Skype call already exists, the new call will be added into the conference
			bool bOtherCallsExist = g_objParser.Info_SetCallsOnHold();
			if(!bOtherCallsExist)
			{
				m_strRedirectTo = strNumber;
				continueRedirect();
			}
			else
			{
				//continue after 1sec allowing the previous calls to enter ONHOLD state
				//m_bWaitForOnHold = true;
				m_strRedirectTo = strNumber;
				QTimer::singleShot(1000,this,SLOT(continueRedirect()));
			}
		#endif
		}
		else
			QMessageBox::information(NULL, "", tr("You must enter Skype Number!"));
	}
	else
	{
#ifdef _WIN32
		int nLine = ui.cboInterface->currentIndex();
		DWORD dwDeviceID = m_lstTAPIDevIDs[nLine];
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Voice call window: Try to open TAPI device index=%1 (device ID=%2, name=%3)").arg(nLine-1).arg(dwDeviceID).arg(ui.cboInterface->currentText()));
		if(0 != m_lineTAPI.Open(dwDeviceID)){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: failed to open TAPI device!"));
			QMessageBox::information(NULL, "", tr("Could not open requested TAPI device!"));
			return;
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Voice call window: TAPI device %1 opened OK").arg(nLine-1));

		QString strNumToCall = strNumber;
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Voice call window: number to be called [%1]").arg(strNumber));
		if(g_pSettings->GetPersonSetting(COMM_SETTINGS_USE_DIAL_PREFIX).toBool()){
			strNumToCall = g_pSettings->GetPersonSetting(COMM_SETTINGS_DIAL_PREFIX_STR).toString() + strNumber;
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Voice call window: prepend prefix from settings [%1]").arg(g_pSettings->GetPersonSetting(COMM_SETTINGS_DIAL_PREFIX_STR).toString()));
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: Try to make a call to [%1]").arg(strNumToCall));

		int nRet = m_lineTAPI.MakeOutgoingCall(strNumToCall);
		if(0 == nRet)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: MakeOutgoingCall returned OK"));

			m_bIncoming = false;
			ui.radOutgoing->setChecked(true);
			ui.radIncoming->setChecked(false);
			m_recData.setData(0, "BVC_DIRECTION", 0);	//outgoing
			SetCaller(strNumber);
			SetWindowState(WND_STATE_Ringing_Out);

			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: Check to start the TAPI timer"));

			//start timer to calculate 
			if(NULL == m_pTapiTimer){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: Starting TAPI timer"));

				m_pTapiTimer = new QTimer(this);
				connect(m_pTapiTimer, SIGNAL(timeout()), this, SLOT(OnTAPICheckTimer()) );
				m_pTapiTimer->start(200); //100ms

				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: Done starting TAPI timer"));
			}
		}
		else{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: MakeOutgoingCall failed"));
		}
#endif
	}
}

void FUI_VoiceCallCenter::EndCall()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: EndCall start"));

	if("Skype" == ui.cboInterface->currentText())
	{
		if( m_nCallID >= 0 && 
			m_nWndState != WND_STATE_After /*&&
			!m_bForwarded*/)
		{
			//"hangup" button to stop inoming call
			if(m_nVoiceMailIncID > 0){
				QString strCmd = QString().sprintf("ALTER VOICEMAIL %d STOPPLAYBACK", m_nVoiceMailIncID);
				SkypeManager::SendCommand(strCmd.toLatin1().constData());
				m_nVoiceMailIncID = -1;
				QTimer::singleShot(1200,this,SLOT(StopVoicemail()));
			}
			else if(m_nVoiceMailOutID > 0)
			{
				StopVoicemail();
			}
			else{
				//"hangup" button to stop the call
				QString strCmd = QString().sprintf("SET CALL %d STATUS FINISHED", m_nCallID);
				SkypeManager::SendCommand(strCmd.toLatin1().constData());
			}
		}

	#ifdef _WIN32
		//reset current call id
		if(m_nCallID == g_objParser.m_nCurCallID)
			g_objParser.m_nCurCallID = -1;

		//reset other data
		//if(m_nCallID == g_objParser.m_nCurTransferedCallID)
			g_objParser.m_nCurTransferedCallID = -1;

		//if(m_nCallConferenceID > 0)	//TOFIX
		{
			g_objParser.m_nCurConferenceSpeakers --;
			m_nCallConferenceID = -1;

			//reset dead conference info
			if(g_objParser.m_nCurConferenceSpeakers < 1){
				g_objParser.m_nCurConferenceSpeakers = 0;
				g_objParser.m_nCurCallID = 0;
			}
		}
	#endif
	}
	else
	{
#ifdef _WIN32
		//if( m_lineTAPI.IsConnected() ||
		//	  m_lineTAPI.IsConnecting())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: close TAPI call"));
			m_lineTAPI.Close();
		}
#endif
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: EndCall done"));
}

void FUI_VoiceCallCenter::on_btnScheduleTask_clicked()
{
	bool bChecked = ui.frameTask->isChecked();
	ui.frameTask->SetChecked(!bChecked);

	ui.frameTask->m_recTask.setData(0, "BTKS_START", QDateTime::currentDateTime());
	ui.frameTask->SetGUIFromData();
	//ui.frameTask->ui.dateStart->setDate(QDate::currentDate());
	
	//set active tab to task details
	ui.tabWidget->setCurrentIndex(1);
	updateTaskTabCheckbox();
}

void FUI_VoiceCallCenter::on_btnPickPhone_clicked()
{
	PhoneSelectorDlg dlg(m_lstPhonesFmt, m_lstPhoneNames, m_lstPhoneTypes);

	//set position to simulate combo box (MB 2008.01.23)
	QPoint pt = ui.txtPhoneNumber->geometry().bottomLeft();
	pt += geometry().topLeft();
	dlg.move(pt);

	if(dlg.exec() > 0)
	{
		int nIndex = dlg.getSelectedPhoneIdx();

		//QString strPhone = dlg.getSelectedPhone();
		//if(!strPhone.isEmpty()){
		if(nIndex >= 0){
			m_strUnfmtNumber = m_lstPhones[nIndex];
			m_strFmtNumber = m_lstPhonesFmt[nIndex];
			ui.txtPhoneNumber->setText(m_strFmtNumber);
			UpdateSkypeStatus();
		}
	}
}

void FUI_VoiceCallCenter::on_btnSearchContact_clicked()
{
	int nInterface = 0;
	if(ui.cboInterface->findText("Skype") == ui.cboInterface->currentIndex())
		nInterface = 1;

	FilterContacts(ui.txtPhoneNumber->text(), nInterface,true);
}

void FUI_VoiceCallCenter::UpdateSkypeStatus()
{
	if(!SkypeManager::IsSkypeInstalled()){
		OnUserResolved_Status("", ""); // clean data
		return;
	}

	//if(WND_STATE_PreCall != m_nWndState)
	//	return; // prevents high CPU usage when running this timer

	//if contact selected
	DbRecordSet rowCurrentContact;
	int nContactId;
	if(ui.frameContact->GetCurrentEntityRecord(nContactId,rowCurrentContact))
	{
		QString strHandle = ui.txtPhoneNumber->text();
		if(!strHandle.isEmpty()){
			if(!IsPhoneNumber(strHandle))	// only for Skype numbers
			{
		#ifdef _WIN32
				g_objParser.SetSkypeUserInfoWnd(this);	//set result target
				QString strCmd = QString("GET USER %1 ONLINESTATUS").arg(strHandle);
				SkypeManager::SendCommand(strCmd.toLatin1().constData());
		#endif
			}
		}
		else
			OnUserResolved_Status("", ""); // clean data
	}
	else
		OnUserResolved_Status("", ""); // clean data
}

void FUI_VoiceCallCenter::OnUserResolved_Status(QString strNumber, QString strData)
{
	//TOFIX: UNKNOWN - unknown user, SKYPEOUT - user is in the SkypeOut contact list, SKYPEME (Protocol 2)
	if(strData == "OFFLINE")
	{
		ui.pixSkypeStatus->setPixmap(QPixmap(":StatusOffline_32x32.png"));
		ui.txtSkypeStatus->setText(tr("Status: Offline"));
	}
	else if(strData == "ONLINE")
	{
		ui.pixSkypeStatus->setPixmap(QPixmap(":StatusOnline_32x32.png"));
		ui.txtSkypeStatus->setText(tr("Status: Online"));
	}
	else if(strData == "AWAY")
	{
		ui.pixSkypeStatus->setPixmap(QPixmap(":StatusAway_32x32.png"));
		ui.txtSkypeStatus->setText(tr("Status: Away"));
	}
	else if(strData == "NA")	//NA - user is not available
	{
		ui.pixSkypeStatus->setPixmap(QPixmap(":StatusNotAvailable_32x32.png"));
		ui.txtSkypeStatus->setText(tr("Status: Not Available"));
	}
	else if(strData == "DND")	//DND - user is in "Do not disturb" mode. 
	{
		ui.pixSkypeStatus->setPixmap(QPixmap(":StatusDoNotDisturb_32x32.png"));
		ui.txtSkypeStatus->setText(tr("Status: Do Not Disturb!"));
	}
	else{
		ui.pixSkypeStatus->setPixmap(QPixmap());
		ui.txtSkypeStatus->setText("");
	}

	if( m_strLastSkypeStatUser == strNumber &&
		m_strLastSkypeStatStatus != strData)
	{
		//QApplication::beep ();

		//user just came online, play sound!
		if(strData == "ONLINE")
		{
			activateWindow();
			QSound::play(QCoreApplication::applicationDirPath() + QDir::separator() + "Ding.wav");
		}
	}

	m_strLastSkypeStatUser = strNumber;
	m_strLastSkypeStatStatus = strData;
}

void FUI_VoiceCallCenter::OnPrint()
{
	QString strIncomingOutgoing,
			strDate,
			strContact,
			strPhoneNumber,
			strOwner,
			strStartTime,
			strEndTime,
			strDurationTime,
			strProject;
	
	if (m_bIncoming)
		strIncomingOutgoing = tr("Incoming Phone Call");
	else
		strIncomingOutgoing = tr("Outgoing Phone Call");

	if (ui.txtCallDate->date().isValid())
		strDate = ui.txtCallDate->date().toString("dd.MM.yyyy");
	strStartTime = ui.txtCallStartTime->text();
	strEndTime = ui.txtCallEndTime->text();
	strDurationTime = ui.txtCallDuration->text();

	strContact = ui.frameContact->GetCurrentDisplayName();
	if (strPhoneNumber.isEmpty())
		strPhoneNumber = ui.txtPhoneNumber->text();
	else
		strPhoneNumber = m_strPhoneHome;
	strProject = ui.frmSelectedProject->GetCurrentDisplayName();

	int nOwnerID = m_recData.getDataRef(0, "CENT_OWNER_ID").toInt();
	MainEntitySelectionController sel;
	sel.Initialize(ENTITY_BUS_PERSON);
	strOwner = sel.GetCalculatedName(nOwnerID);
	if (strOwner.isEmpty())
	{
		nOwnerID = g_pClientManager->GetPersonID();
		strOwner = g_pClientManager->GetPersonName();
	}

	QStringList list;
	list << strIncomingOutgoing << strDate << strContact << strPhoneNumber << strOwner << strStartTime << strEndTime << strDurationTime << strProject;

	ui.frmEditor->SetupPrintingHeader(list);
}

void FUI_VoiceCallCenter::on_btnOpenChat_clicked()
{
	QString strNumber = ui.txtPhoneNumber->text();
	if(!strNumber.isEmpty())
	{
		//favor real Skype number instead of phone number
		if(IsPhoneNumber(strNumber)){
			//load all contact phones
			if(m_lstPhones.size() < 1)
				LoadContactData(false);

			bool bFound = false;
			int nCnt = m_lstPhoneTypeIDs.size();
			for(int i=0; i<nCnt; i++){
				if(m_lstPhoneTypeIDs[i] == ContactTypeManager::SYSTEM_SKYPE){
					strNumber = m_lstPhones[i];
					bFound = true;
					break;
				}
			}
			if(!bFound){
				QMessageBox::information(NULL, "", tr("The chat-window can only be used for contacts with a Skype-name!"));
				return;
			}
		}

	#ifdef _WIN32
		g_objParser.SetSkypeUserInfoWnd(this); //
		g_objParser.m_pSkypeUserInfoFui = NULL;	//set result target
		QString strCmd = QString().sprintf("CHAT CREATE %s", strNumber.toLatin1().constData());
		SkypeManager::SendCommand(strCmd.toLatin1().constData());
	#endif
	}
	else
		QMessageBox::information(NULL, "", tr("You must enter Skype Number!"));
}

void FUI_VoiceCallCenter::on_btnOpenSMS_clicked()
{
	QString strNumber = ui.txtPhoneNumber->text();

	SMS_Dialog dlg;

	int nContactID;
	if(ui.frameContact->GetCurrentEntityRecord(nContactID))
		dlg.SetContact(nContactID);

	dlg.SetNumber(strNumber);
	dlg.exec();
}

void FUI_VoiceCallCenter::OnChatResolved_Name(QString strName)
{
	QString strCmd = QString().sprintf("OPEN CHAT %s", strName.toLatin1().constData());
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
}

void FUI_VoiceCallCenter::continueRedirect()
{
#ifdef _WIN32
	//prepare command
	SkypeManager::Call(m_strRedirectTo);
	SetWindowState(WND_STATE_Ringing_Out);
	
	//register this window as outgoing call source
	g_objParser.m_nCurOutCallFUI = g_objFuiManager.GetFuiID(this);
#endif
}

void FUI_VoiceCallCenter::OnUserResolved(int nTypeData,QString strHandle,QVariant varData)
{
	switch(nTypeData)
	{
	case SKYPE_FULL_NAME:
		OnUserResolved_FullName(strHandle,varData.toString());
		break;
	case SKYPE_BIRTHDAY:
		OnUserResolved_Birthday(strHandle,varData.toString());
		break;
	case SKYPE_SEX:
		OnUserResolved_Sex(strHandle,varData.toString());
		break;
	case SKYPE_COUNTRY:
		OnUserResolved_Country(strHandle,varData.toString());
		break;
	case SKYPE_PROVINCE:
		OnUserResolved_Province(strHandle,varData.toString());
		break;
	case SKYPE_CITY:
		OnUserResolved_City(strHandle,varData.toString());
		break;
	case SKYPE_PHONE_HOME:
		OnUserResolved_PhoneHome(strHandle,varData.toString());
		break;
	case SKYPE_PHONE_OFFICE:
		OnUserResolved_PhoneOffice(strHandle,varData.toString());
		break;
	case SKYPE_PHONE_MOBILE:
		OnUserResolved_PhoneMobile(strHandle,varData.toString());
		break;
	case SKYPE_HOMEPAGE:
		OnUserResolved_Homepage(strHandle,varData.toString());
		break;
	case SKYPE_ABOUT:
		OnUserResolved_About(strHandle,varData.toString());
		break;
	case SKYPE_STATUS:
		OnUserResolved_Status(strHandle,varData.toString());
		break;
	default:
		Q_ASSERT(false);
		break;
	}
}

void FUI_VoiceCallCenter::OnThemeChanged()
{
	ui.frameHdr->setStyleSheet("QFrame#frameHdr "+ThemeManager::GetSidebarChapter_Bkg());
	ui.labelPhoneCall->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
}

void FUI_VoiceCallCenter::OnTAPICheckTimer()
{
#ifdef _WIN32
	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: OnTAPICheckTimer"));

	long nResult = m_lineTAPI.GetThreadResult();
	if(nResult != 0){
		char *szErrTxt = NULL;
		m_lineTAPI.GetErrorString(nResult, szErrTxt);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: TAPI error detected: %1 (%2)").arg(nResult).arg(szErrTxt));

		if(0 == strcmp("LINEERR_OPERATIONUNAVAIL", szErrTxt))
			QMessageBox::information(NULL, "", tr("Phone Line Occupied!"));
		else if(0 == strcmp("LINEERR_OPERATIONFAILED", szErrTxt))
			QMessageBox::information(NULL, "", tr("Phone Operation Failed!"));
		else
			QMessageBox::information(NULL, "", tr("TAPI Error: %1!").arg(szErrTxt));
		free(szErrTxt);

		//terminate the call on error
		EndCall();

		//m_lineTAPI.Close();
		SetWindowState(WND_STATE_After);

		delete m_pTapiTimer;
		m_pTapiTimer = NULL;
		return;
	}

	//terminated?
	if( !m_lineTAPI.IsConnected() &&
		!m_lineTAPI.IsConnecting())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: OnTAPICheckTimer: delete timer, call not connected"));

		delete m_pTapiTimer;
		m_pTapiTimer = NULL;

		RecalcDuration();
		SetWindowState(WND_STATE_After);

		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: OnTAPICheckTimer: done deleting timer"));
	}
	else if (m_lineTAPI.IsConnected()){
		if(!m_bCallWasEstablished)	//write log only once
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Voice call window: OnTAPICheckTimer: call connected"));
		m_bCallWasEstablished = true;
		SetWindowState(WND_STATE_Talking); //TOFIX
	}
#endif
}

void FUI_VoiceCallCenter::changeEvent ( QEvent * event)
{
	QWidget::changeEvent(event);

	if(event->type() == QEvent::WindowStateChange)
	{
		//now check to see if the window is being minimised
        if( isMinimized() )
		{
			LoadContactData(); //fix the problem when this fui is opened through avatar (empty phone list)

			//only for tasks
			Status status;
			ui.frameTask->GetDataFromGUI();
			bool bTaskActive = ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0;
			if(bTaskActive)
			{
				if(NULL == m_pWndAvatar)
				{
					//calculate position for the new window
					QPoint pt = geometry().topRight();
					pt.setX(pt.x() - 169);

					m_pWndAvatar = new fui_avatar(AVATAR_TASK_CALL, this);
					m_pWndAvatar->move(pt);
				}

				RefreshAvatarData();
				m_pWndAvatar->show();

				if (m_bSideBarModeView)
					g_objFuiManager.ClearActiveFUI(this);

				if(m_bHideOnMinimize)
					QTimer::singleShot(0,this,SLOT(hide()));//B.T. delay hide
				m_bHideOnMinimize = true; //reset
			}
		}
	}
}

void FUI_VoiceCallCenter::closeEvent(QCloseEvent *event)
{
	FuiBase::closeEvent(event);

	if(m_pWndAvatar){
		m_pWndAvatar->close();
		m_pWndAvatar = NULL;
	}
}

int FUI_VoiceCallCenter::GetTaskID()
{
	if(ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt() > 0)
		return ui.frameTask->m_recTask.getDataRef(0, "BTKS_ID").toInt();

	return -1;
}

void FUI_VoiceCallCenter::RefreshAvatarData(bool bReload)
{
	if(bReload){
		QString strWhere="WHERE BTKS_ID="+QVariant(GetTaskID()).toString();
		Status status;
		_SERVER_CALL(ClientSimpleORM->Read(status, BUS_TASKS, ui.frameTask->m_recTask, strWhere));

		ui.frameTask->SetGUIFromData();
	}

	QString strTxt = ui.frameTask->m_recTask.getDataRef(0, "BTKS_SUBJECT").toString();
	QString strContact;
	if(m_recData.getRowCount() > 0){
		strContact = EMailDialog::GetTaskAssignedContact(m_recData.getDataRef(0, "BVC_CONTACT_ID").toInt());
	}
	QString strLabel = strContact;
	strLabel += "\n";
	strLabel += strTxt;
	m_pWndAvatar->SetLabel(strLabel);

	QString strDesc = ui.frameTask->m_recTask.getDataRef(0, "BTKS_DESCRIPTION").toString();
	m_pWndAvatar->SetTaskDescTooltip(strDesc);

	QString strDate = EMailDialog::GetTaskDate(CommGridViewHelper::VOICECALL, m_recData, ui.frameTask->m_recTask);
	m_pWndAvatar->SetTaskDate(strDate);

	QString strOwner = EMailDialog::GetPersonInitials(ui.frameTask->m_recTask.getDataRef(0, "BTKS_OWNER").toInt());
	m_pWndAvatar->SetTaskOwnerInitials(strOwner);

	int nTask = CommGridViewHelper::CheckTask(ui.frameTask->m_recTask);
	m_pWndAvatar->SetTaskState(nTask);

	m_pWndAvatar->SetRecord(m_nEntityRecordID);

	m_pWndAvatar->SetTaskID(ui.frameTask->m_recTask.getDataRef(0, "BTKS_ID").toInt());
}

DbRecordSet *FUI_VoiceCallCenter::GetTaskRecordSet()
{
	return &(ui.frameTask->m_recTask);
}

void FUI_VoiceCallCenter::StopVoicemail()
{
	//"hangup" button to stop inoming call
	QString strCmd = QString().sprintf("ALTER VOICEMAIL %d STOPRECORDING", m_nVoiceMailOutID);
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
	m_nVoiceMailOutID = -1;
}

void FUI_VoiceCallCenter::updateTaskTabCheckbox()
{
	//customize 2nd tab title to simulate icon
	ui.tabWidget->setTabIcon(1, (ui.frameTask->isChecked())? QIcon(":/checkbox_checked.png") : QIcon(":/checkbox_unchecked.png"));
}

void FUI_VoiceCallCenter::updateContactInfo()
{
	QString strLine1;
	QString strLine2;

	int nEntityRecordID;
	DbRecordSet rowData;
	if(ui.frameContact->GetCurrentEntityRecord(nEntityRecordID, rowData))
	{
		QString strFirstName = rowData.getDataRef(0, "BCNT_FIRSTNAME").toString();
		QString strLastName	 = rowData.getDataRef(0, "BCNT_LASTNAME").toString();
		QString strOrgName	 = rowData.getDataRef(0, "BCNT_ORGANIZATIONNAME").toString();
		
		//"First Last" name line
		strLine1 = strFirstName;
		if (!strLastName.isEmpty()){
			if (!strLine1.isEmpty())
				strLine1 += " ";
			strLine1 += strLastName;
		}

		//"Organization" line
		strLine2 = strOrgName;

		//if org only, write it in the main line
		if (strLine1.isEmpty()){
			strLine1 = strLine2;
			strLine2 = "";
		}
	}

	ui.txtContactTitle->setText(strLine1);
	ui.txtContactTitle2->setText(strLine2);
}

void FUI_VoiceCallCenter::on_btnAssignProjects_clicked()
{
	//makes sense only if contact selected
	int nContactID;
	DbRecordSet rowCurrentContact;
	if(!ui.frameContact->GetCurrentEntityRecord(nContactID, rowCurrentContact)){
		QMessageBox::information(NULL, "", tr("No contact selected!"));
		return;
	}

	//see if any project is currently selected
	int nProjectCurrentID = -1;
	ui.frmSelectedProject->GetCurrentEntityRecord(nProjectCurrentID);

	//select a project out of all projects assigned to a contact
	AssignedProjectsDlg dlg(m_lstDataNMRXProjects, this, nProjectCurrentID);
	if(dlg.exec())
	{
		ui.frmSelectedProject->SetCurrentEntityRecord(dlg.GetSelectedProject(), true);
	}
}

void FUI_VoiceCallCenter::on_btnInsertNewPhone_clicked()
{
	//makes sense only if contact selected
	int nContactID;
	DbRecordSet rowCurrentContact;
	if(!ui.frameContact->GetCurrentEntityRecord(nContactID, rowCurrentContact)){
		QMessageBox::information(NULL, "", tr("No contact selected!"));
		return;
	}

	//prepare list of phone types
	QStringList lstPhoneTypes;
	int nMax = m_recPhoneTypes.getRowCount();
	for(int i=0; i<nMax; i++)
		lstPhoneTypes << m_recPhoneTypes.getDataRef(i,"BCMT_TYPE_NAME").toString();

	//find index of "Business direct" type (default selection)
	int nBusinessIdx = m_recPhoneTypes.find("BCMT_ID", ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS), true);

	//show dialog
	AddPhoneNumberDlg dlg(ui.txtPhoneNumber->text(), lstPhoneTypes, nBusinessIdx, this);
	if(dlg.exec())
	{
		QString strNumber = dlg.GetNumber();
		
		//m_recPhoneTypes.Dump();

		int nTypeID = m_recPhoneTypes.getDataRef(dlg.GetTypeIdx(),"BCMT_ID").toInt();

		if(!strNumber.isEmpty())
		{
			//
			// insert new phone for this contact
			//

			// Load list of phones from server:
			Status status;
			DbRecordSet lstPhones;
			_SERVER_CALL(BusContact->ReadContactPhones(status, nContactID, lstPhones));
			_CHK_ERR(status);

			//lstPhones.Dump();
			lstPhones.addRow();
			int nRow=lstPhones.getRowCount()-1;

			QString strPhone,strArea,strCountry,strSearch,strISO,strLocal;

			//add new record
			//B.T. - reformat phone number
			FormatPhone m_FormatPhone;
			m_FormatPhone.SplitPhoneNumber(strNumber,strCountry,strArea,strLocal,strSearch,strISO);
			lstPhones.setData(nRow,"BCMP_AREA",strArea);
			lstPhones.setData(nRow,"BCMP_COUNTRY",strCountry);
			lstPhones.setData(nRow,"BCMP_LOCAL",strLocal);
			lstPhones.setData(nRow,"BCMP_SEARCH",strSearch);
			lstPhones.setData(nRow,"BCMP_COUNTRY_ISO",strISO);
			strNumber=m_FormatPhone.FormatPhoneNumber(strCountry,strArea,strLocal);
			lstPhones.setData(nRow,"BCMP_FULLNUMBER",strNumber); //type=skype/TOFIX later for other interfaces:
			lstPhones.setData(nRow,"BCMP_TYPE_ID",nTypeID);

			//save
			_SERVER_CALL(BusContact->WritePhones(status, nContactID, lstPhones));
			_CHK_ERR(status);

			//rebuild contact phones cache
			BuildPhoneCache(lstPhones);
		}
	}
}
