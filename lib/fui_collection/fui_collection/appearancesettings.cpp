#include "appearancesettings.h"
#include "gui_core/gui_core/gui_helper.h"
#include "os_specific/os_specific/stylemanager.h"
#include <QtWidgets/QMessageBox>
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "gui_core/gui_core/thememanager.h"


AppearanceSettings::AppearanceSettings(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager /*= NULL*/, QWidget *parent)
    : SettingsBase(nOptionsSetID, bOptionsSwitch, pOptionsAndSettingsManager, parent)
{
	ui.setupUi(this);

	InitializeWidgets();

	connect(ui.Skin_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(on_SkinChanged(int)));
	connect(ui.Theme_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(on_ThemeChanged(int)));
	connect(ui.ViewMode_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(on_ViewChanged(int)));
}

AppearanceSettings::~AppearanceSettings()
{

}

void AppearanceSettings::InitializeWidgets()
{
	//COMBOS's:
	//First skin combo.
	ui.Skin_comboBox->addItem(tr("Default (no skin)"),StyleManager::DEFAULTSTYLE);
	ui.Skin_comboBox->addItem(tr("Windows"),StyleManager::QWINDOWSSTYLE);
#ifndef WINCE
  #ifdef _WIN32
	ui.Skin_comboBox->addItem(tr("Windows XP"),StyleManager::QWINDOWSXPSTYLE);
	ui.Skin_comboBox->addItem(tr("Windows Vista"),StyleManager::QWINDOWSVISTASTYLE);
  #endif
	ui.Skin_comboBox->addItem(tr("Clean Look"),StyleManager::QCLEANLOOKSSTYLE);
	ui.Skin_comboBox->addItem(tr("Plastique"),StyleManager::QPLASTIQUESTYLE);
	ui.Skin_comboBox->addItem(tr("Motif"),StyleManager::QMOTIFSTYLE);
#endif // WINCE

	//theme:
	ui.Theme_comboBox->addItem(tr("Blue Temple"),ThemeManager::THEME_BLUE_TEMPLE);
	ui.Theme_comboBox->addItem(tr("Grayhound"),ThemeManager::THEME_GRAYHOUND);
	ui.Theme_comboBox->addItem(tr("Cabernet"),ThemeManager::THEME_CABERNET);
	ui.Theme_comboBox->addItem(tr("Forest"),ThemeManager::THEME_FOREST);
	ui.Theme_comboBox->addItem(tr("Smaragd"),ThemeManager::THEME_SMARAGD);

	//view:
	ui.ViewMode_comboBox->addItem(tr("Sidebar Mode"),ThemeManager::VIEW_SIDEBAR);
	//ui.ViewMode_comboBox->addItem(tr("Easy Mode"),ThemeManager::VIEW_EASY);
	ui.ViewMode_comboBox->addItem(tr("Professional Mode"),ThemeManager::VIEW_PRO);

	int nCurrentSkin = GetSettingValue(SKIN).toInt();
	int nIndex=ui.Skin_comboBox->findData(nCurrentSkin);
	if (nIndex<0)
		nIndex=0;
	ui.Skin_comboBox->setCurrentIndex(nIndex);

	int nCurrentTheme = GetSettingValue(APPEAR_THEME).toInt();
	nIndex=ui.Theme_comboBox->findData(nCurrentTheme);
	if (nIndex<0)
		nIndex=0;
	ui.Theme_comboBox->setCurrentIndex(nIndex);

	int nCurrentView = GetSettingValue(APPEAR_VIEW_MODE).toInt();
	nIndex=ui.ViewMode_comboBox->findData(nCurrentView);
	if (nIndex<0)
		nIndex=0;
	ui.ViewMode_comboBox->setCurrentIndex(nIndex);

	//Starting module.
	int nStartModule = GetSettingValue(STARTING_MODULE).toInt();
	SetCheckedStartingModuleRadio(nStartModule);

	//cell:
	//if internet disabled return local:
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_CONTACTS_FUI, nValue))
	{
		ui.radioButton_4->setEnabled(false);
		ui.radioButton_3->setEnabled(false);
		
	}
	//else return internet:
	if(!g_FunctionPoint.IsFPAvailable(FP_PROJECT_FUI, nValue))
	{
		ui.radioButton_5->setEnabled(false);
	}
	//Show header.
	bool bShowHeader = GetSettingValue(SHOW_HEADER).toBool();
	ui.ShowHeader_checkBox->setChecked(bShowHeader);

	bool bShowStatus = GetSettingValue(SHOW_STATUS_BAR).toBool();
	ui.ShowStatus_checkBox->setChecked(bShowStatus);

	bool bShowFull = GetSettingValue(SHOW_FULL_SCREEN).toBool();
	ui.FullScreen_checkBox->setChecked(bShowFull);

	bool bTwoScreenReversed = GetSettingValue(TWO_SCREEN_REVERSED).toBool();
	ui.ckbTwoScreenReversed->setChecked(bTwoScreenReversed);


}

DbRecordSet AppearanceSettings::GetChangedValuesRecordSet()
{
	//Don't check SKIN it has it's own even handler.
	bool bLoggedUser=(GetPersonID()==g_pClientManager->GetPersonID());
	if (GetPersonID()<=0)
		bLoggedUser=true;

	//Starting module.
	if (GetCheckedStartingModuleRadio() != GetSettingValue(STARTING_MODULE).toInt())
		SetSettingValue(STARTING_MODULE, GetCheckedStartingModuleRadio());

	//Show header.
	if (ui.ShowHeader_checkBox->isChecked() != GetSettingValue(SHOW_HEADER).toBool())
	{
		SetSettingValue(SHOW_HEADER, ui.ShowHeader_checkBox->isChecked());
		if (bLoggedUser) g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_MENU_HEADER,ui.ShowHeader_checkBox->isChecked());
	}

	if (ui.ShowStatus_checkBox->isChecked() != GetSettingValue(SHOW_STATUS_BAR).toBool())
	{
		SetSettingValue(SHOW_STATUS_BAR, ui.ShowStatus_checkBox->isChecked());
		if (bLoggedUser)g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_MENU_STATUSBAR,ui.ShowStatus_checkBox->isChecked());
	}

	if (ui.FullScreen_checkBox->isChecked() != GetSettingValue(SHOW_FULL_SCREEN).toBool())
	{
		SetSettingValue(SHOW_FULL_SCREEN, ui.FullScreen_checkBox->isChecked());
		if (bLoggedUser)g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_MENU_FULLSCREEN,ui.FullScreen_checkBox->isChecked());
	}

	if (ui.ckbTwoScreenReversed->isChecked() != GetSettingValue(TWO_SCREEN_REVERSED).toBool())
	{
		SetSettingValue(TWO_SCREEN_REVERSED, ui.ckbTwoScreenReversed->isChecked());
	}


	if (bLoggedUser) 
	{
		g_pClientManager->GetIniFile()->m_nLastView=GetSettingValue(APPEAR_VIEW_MODE).toInt();
	}
	//g_pSettings->SetPersonSetting(APPEAR_VIEW_MODE,GetSettingValue(APPEAR_VIEW_MODE).toInt());
	//SetSettingValue(APPEAR_VIEW_MODE, ui.FullScreen_checkBox->isChecked());

	//if (ThemeManager::GetCurrentThemeID()!=GetSettingValue(APPEAR_THEME).toInt())
	//{
		//ThemeManager::SetCurrentThemeID(GetSettingValue(APPEAR_THEME).toInt());

	if (bLoggedUser) g_pClientManager->GetIniFile()->m_nLastTheme=GetSettingValue(APPEAR_THEME).toInt();
	//g_pSettings->SetPersonSetting(APPEAR_THEME,GetSettingValue(APPEAR_THEME).toInt());
	
	//g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_THEME_CHANGED);
	//}
	//ThemeManager::OnUserChangedTheme(GetSettingValue(APPEAR_THEME).toInt());
	//ThemeManager::OnUserChangedView(GetSettingValue(APPEAR_VIEW_MODE).toInt());

	//Call base class.
	return SettingsBase::GetChangedValuesRecordSet();
}

int AppearanceSettings::GetCheckedStartingModuleRadio()
{
	if (ui.radioButton_0->isChecked())
		return 0;
	//else if (ui.radioButton_1->isChecked())
	//	return 1;
	//else if (ui.radioButton_2->isChecked())
	//	return 2;
	else if (ui.radioButton_3->isChecked())
		return 3;
	else if (ui.radioButton_4->isChecked())
		return 4;
	else if (ui.radioButton_5->isChecked())
		return 5;
	
	return 0;  //def B.T. touched
}

void AppearanceSettings::SetCheckedStartingModuleRadio(int i)
{
	if (i == 0)
		ui.radioButton_0->setChecked(true);
	//else if (i == 1)
	//	ui.radioButton_1->setChecked(true);
	//else if (i == 2)
	//	ui.radioButton_2->setChecked(true);
	else if (i == 3)
		ui.radioButton_3->setChecked(true);
	else if (i == 4)
		ui.radioButton_4->setChecked(true);
	else if (i == 5)
		ui.radioButton_5->setChecked(true);
}

void AppearanceSettings::on_SkinChanged(int nNewSkin)
{
	int nNewSkinID=ui.Skin_comboBox->itemData(nNewSkin).toInt();
	if (nNewSkinID == GetSettingValue(SKIN).toInt())
		return;

	QApplication::setOverrideCursor(Qt::WaitCursor);
	StyleManager::SetStyle(nNewSkinID);
	SetSettingValue(SKIN, nNewSkinID);
	QApplication::restoreOverrideCursor();
}
void AppearanceSettings::on_ThemeChanged(int nNewTheme)
{
	int nNewID=ui.Theme_comboBox->itemData(nNewTheme).toInt();
	if (nNewID == GetSettingValue(APPEAR_THEME).toInt())
		return;


	SetSettingValue(APPEAR_THEME, nNewID);
	if (GetPersonID()==g_pClientManager->GetPersonID() || GetPersonID()<0)
		QMessageBox::information(this,tr("Information"),tr("Please log out and log in again to activate the new theme!"));

	//ThemeManager::OnUserChangedTheme(nNewID);

}
void AppearanceSettings::on_ViewChanged(int nNewView)
{
	int nNewID=ui.ViewMode_comboBox->itemData(nNewView).toInt();
	if (nNewID == GetSettingValue(APPEAR_VIEW_MODE).toInt())
		return;
	SetSettingValue(APPEAR_VIEW_MODE, nNewID);
	//ThemeManager::OnUserChangedView(nNewID);
}