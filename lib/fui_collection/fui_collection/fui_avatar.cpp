#include "fui_avatar.h"
#include <QBitmap>
#include <QMouseEvent>
#include <QPainter>

#include <QMenu>
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "fui_projects.h"
#include "fui_contacts.h"
#include "emaildialog.h"
#include "fui_dm_documents.h"
#include "fui_voicecallcenter.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_client/bus_client/commgridviewhelper_client.h"

#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager

fui_avatar::fui_avatar(int nType, QWidget *parent)
	: QWidget(NULL /*parent*/, Qt::FramelessWindowHint/*|Qt::Tool*/)
{
	m_nAvatarType = nType;
	m_pParent = parent;
	m_nRecordID = 0;
	m_nTaskID = 0;
	m_bFirstTimeReloadFromAvatar = true;
	m_bTaskDone = false;

	ui.setupUi(this);

	if( AVATAR_PROJECT == m_nAvatarType ||
		AVATAR_CONTACT == m_nAvatarType)
	{
		ui.btnPieMenu->SetEntityRecordID(0); //invalidate, else set to contact id
		ui.iconArrow->setPixmap(QPixmap(":DropZone32.png"));
		ui.iconArrow->setToolTip("");
		ui.btnPieMenu->setIcon(QIcon()); //overwrite default 16x16 icon
		ui.btnPieMenu->setStyleSheet("QToolButton { border-width: 4px; color: white;border-image:url(:PieMenu32.png) 4px 4px 4px 4px stretch stretch }\
									  QToolButton:hover {  border-width: 4px; color: white;border-image:url(:PieMenu32Shadow.png) 4px 4px 4px 4px stretch stretch} ");

		//hide Task-only fields
		ui.txtTaskInitials->hide();
		ui.txtTaskDate->hide();
		ui.imgTaskState->hide();
	}
	else{
		ui.btnPieMenu->SetEntityType(-1);
		ui.iconArrow->setPixmap(QPixmap(":Run_Task.png"));
		ui.imgTaskState->setPixmap(QPixmap(":Task_Done.png"));

		connect(ui.iconArrow, SIGNAL(clicked()), this, SLOT(onTaskRun_clicked()));
		connect(ui.imgTaskState, SIGNAL(clicked()), this, SLOT(onTaskDone_clicked()));
	}

	if(AVATAR_PROJECT == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Project64_Mirror.png"));
		ui.btnPieMenu->SetEntityType(ENTITY_BUS_PROJECT);
	}
	else if(AVATAR_CONTACT == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Contacts_64_Mirror.png")); //:Person64.png
		ui.btnPieMenu->SetEntityType(ENTITY_BUS_CONTACT);
	}
	else if(AVATAR_TASK_EMAIL == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Email48.png"));
		//ui.btnPieMenu->SetEntityType(ENTITY_BUS_EMAILS);
	}
	else if(AVATAR_TASK_CALL == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Phone48.png"));
		//ui.btnPieMenu->SetEntityType(ENTITY_BUS_VOICECALLS);
	}
	else if(AVATAR_TASK_ADDRESS == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Address_48.png"));
		//ui.btnPieMenu->SetEntityType(ENTITY_BUS_EMAIL);
	}
	else if(AVATAR_TASK_INTERNET_FILE == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Internet_File_48.png"));
		//ui.btnPieMenu->SetEntityType(ENTITY_BUS_EMAIL);
	}
	else if(AVATAR_TASK_LOCAL_FILE == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Local_File_48.png"));
		//ui.btnPieMenu->SetEntityType(ENTITY_BUS_EMAIL);
	}
	else if(AVATAR_TASK_NOTE == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Note_48.png"));
		//ui.btnPieMenu->SetEntityType(ENTITY_BUS_EMAIL);
	}
	else if(AVATAR_TASK_PAPER_DOCS == m_nAvatarType)
	{
		ui.iconImage->setPixmap(QPixmap(":Paper_Docs_48.png"));
		//ui.btnPieMenu->SetEntityType(ENTITY_BUS_EMAIL);
	}

	//cache window bkg image
	m_imageBkg = QImage(ThemeManager::GetAvatarBkg()).convertToFormat(QImage::Format_ARGB32_Premultiplied);

	//create window mask
	QImage image(ThemeManager::GetAvatarBkg());
	QImage mask = image.createAlphaMask();
	QBitmap bmpMask = QBitmap::fromImage(mask);
    setMask(bmpMask);

	// drag and drop
	if(AVATAR_PROJECT == m_nAvatarType)
	{
		m_pDropZone = dynamic_cast<FUI_Projects*>(m_pParent)->GetComMenu()->m_menuDropZone;
		//m_pDropZone->SetFUIParent(MENU_PROJECTS);
		//connect(m_pDropZone,SIGNAL(NewDataDropped(int,DbRecordSet)),m_pParent,SLOT(OnCEMenu_NewDataDropped(int,DbRecordSet)));
		//connect(m_pDropZone,SIGNAL(NeedNewData()),m_pParent,SLOT(OnCEMenu_NeedNewData()));
	}
	else if(AVATAR_CONTACT == m_nAvatarType)
	{
		m_pDropZone = dynamic_cast<FUI_Contacts*>(m_pParent)->GetComMenu()->m_menuDropZone;
		//m_pDropZone->SetFUIParent(MENU_CONTACTS);
		//connect(m_pDropZone,SIGNAL(NewDataDropped(int,DbRecordSet)),m_pParent,SLOT(OnCEMenu_NewDataDropped(int,DbRecordSet)));
		//connect(m_pDropZone,SIGNAL(NeedNewData()),m_pParent,SLOT(OnCEMenu_NeedNewData()));
	}
	setAcceptDrops(true);
	g_ChangeManager.registerObserver(this);

	raise();
	activateWindow();
}

fui_avatar::~fui_avatar()
{
}

void fui_avatar::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
	else if(event->button() == Qt::RightButton) 
	{
		QAction *pActClose = new QAction(tr("Close"),this);
		connect(pActClose, SIGNAL(triggered()), this, SLOT(OnMenu_Close()));
		QAction *pActStartup = new QAction(tr("Open on Startup"),this);
		pActStartup->setCheckable(true);
		connect(pActStartup, SIGNAL(triggered()), this, SLOT(OnMenu_Startup()));

		//get fixed part of the info
		QString strState = EncodeState();
		int nPos = strState.indexOf(":");
		if(nPos>=0)
			strState.remove(nPos+1, 10000);
		QString strAvatars = g_pSettings->GetPersonSetting(AVATAR_STARTUP_LIST).toString();
		if(strAvatars.indexOf(strState) >= 0)
			pActStartup->setChecked(true);

		QMenu *menu = new QMenu();
		menu->addAction(pActStartup);
		menu->addAction(pActClose);
		menu->popup(event->globalPos());

        event->accept();
    }
}

void fui_avatar::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPos() - m_dragPosition);
        event->accept();
    }
}

void fui_avatar::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
	painter.setBackgroundMode(Qt::TransparentMode);
	painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
	painter.drawImage(0, 0, m_imageBkg);
}

void fui_avatar::mouseDoubleClickEvent(QMouseEvent * event)
{
	//operation can be lenghty
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	m_pParent->showNormal();
	m_pParent->activateWindow();
	if(m_bFirstTimeReloadFromAvatar)
	{
		(dynamic_cast<FuiBase*>(m_pParent))->ReloadFromAvatar();
		m_bFirstTimeReloadFromAvatar = false;
	}
	close();

	QApplication::restoreOverrideCursor();
}

void fui_avatar::SetRecord(int nID)
{
	m_nRecordID = nID;
	
	if( AVATAR_PROJECT == m_nAvatarType ||
		AVATAR_CONTACT == m_nAvatarType)
	{
		ui.btnPieMenu->SetEntityRecordID(nID); //BT: issue 1428
	}
}

void fui_avatar::SetPicture(QPixmap &picture)
{
	if(!picture.isNull())
		ui.iconImage->setPixmap(picture.scaled(picture.width()/2, picture.height()/2));
	else 
		ui.iconImage->setPixmap(QPixmap(":Contacts_64_Mirror.png")); //:Person64.png
}

void fui_avatar::SetLabel(QString &strTxt)
{
	ui.txtLabel->setText(strTxt);

	if(AVATAR_PROJECT == m_nAvatarType)
		setWindowTitle(tr("Project Card: ") + strTxt);
	else if(AVATAR_CONTACT == m_nAvatarType)
		setWindowTitle(tr("Contact Card: ") + strTxt);
	else
		setWindowTitle(tr("Task Card: ") + strTxt);
}

void fui_avatar::SetTaskDate(QString &strTxt)
{
	ui.txtTaskDate->setText(strTxt);
}

void fui_avatar::SetTaskState(int nState)
{
	//ui.btnPieMenu->setEnabled(false);

	switch(nState){
		case CommGridViewHelper::SCHEDULED_OVERDUE:
			ui.btnPieMenu->setIcon(QIcon(":Task_Status_Overdue.png")); 
			ui.btnPieMenu->setToolTip(tr("Overdue"));
			break;
		case CommGridViewHelper::SCHEDULED_DUENOW:
			ui.btnPieMenu->setIcon(QIcon(":Task_Status_Due.png")); 
			ui.btnPieMenu->setToolTip(tr("Due"));
			break;
		case CommGridViewHelper::NOT_SCHEDULED:
			ui.btnPieMenu->setIcon(QIcon(":Task_Done.png")); 
			ui.btnPieMenu->setToolTip(tr("Done/Not Scheduled"));
			//disable the button
			//ui.imgTaskState->setEnabled(false);
			m_bTaskDone = true;
			break;
		default:
			ui.btnPieMenu->setIcon(QIcon(":Task_Status_Waiting.png")); 
			ui.btnPieMenu->setToolTip(tr("Pending"));
			break;
	}
}

void fui_avatar::SetTaskOwnerInitials(QString &strTxt)
{
	ui.txtTaskInitials->setText(strTxt);
}

void fui_avatar::SetTaskDescTooltip(const QString &strTxt)
{
	ui.txtLabel->setToolTip(strTxt);
}

void fui_avatar::dropEvent(QDropEvent *event)
{
	m_pDropZone->dropEvent(event);
}

void fui_avatar::dragEnterEvent ( QDragEnterEvent * event )
{
	event->accept();
}

void fui_avatar::OnMenu_Close()
{
	close();
	m_pParent->close();
}

void fui_avatar::OnMenu_Startup()
{
	//is it already registered ?
	QString strState = EncodeState();
	QString strStateFixed = strState;
	int nPos1 = strStateFixed.indexOf(":");
	if(nPos1 >= 0)
		strStateFixed.remove(nPos1+1, 10000);

	QString strAvatars = g_pSettings->GetPersonSetting(AVATAR_STARTUP_LIST).toString();
	int nPos = strAvatars.indexOf(strStateFixed);
	if(nPos >= 0)	// already registered
		strAvatars.remove(nPos, strState.size());	// unregister
	else
		strAvatars += strState;						// register

	//update settings
	g_pSettings->SetPersonSetting(AVATAR_STARTUP_LIST, strAvatars);
	//B.T. : issue 2700: save this at once, so if some other task comes we can reload safely just startup list...
	Status err;
	g_pSettings->SavePersonSettings(err);
}

QString fui_avatar::EncodeState()
{
	QString strState(";");
	strState += QString("%1").arg(m_nAvatarType);
	strState += ",";
	strState += QString("%1").arg(m_nRecordID);
	strState += ":";	//divides fixed from non-fixed data
	strState += QString("%1").arg(geometry().left());
	strState += ",";
	strState += QString("%1").arg(geometry().top());
	strState += ";";

	return strState;
}

void fui_avatar::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}
}

void fui_avatar::OnThemeChanged()
{
	m_imageBkg = QImage(ThemeManager::GetAvatarBkg()).convertToFormat(QImage::Format_ARGB32_Premultiplied);

	//create window mask
	QImage image(ThemeManager::GetAvatarBkg());
	QImage mask = image.createAlphaMask();
	QBitmap bmpMask = QBitmap::fromImage(mask);
	setMask(bmpMask);
	this->update();
}

void fui_avatar::onTaskRun_clicked()
{
	//Q_ASSERT(m_nRecordID > 0);

	//NOTE: adapted from CommWorkBenchWidget::on_startTask_toolButton_clicked()
	if(AVATAR_TASK_EMAIL == m_nAvatarType)
	{
		EMailDialog* pFUI=dynamic_cast<EMailDialog*>(m_pParent);
		pFUI->Execute(m_nRecordID);
	}
	else if(AVATAR_TASK_ADDRESS == m_nAvatarType ||
			AVATAR_TASK_INTERNET_FILE == m_nAvatarType ||
			AVATAR_TASK_LOCAL_FILE == m_nAvatarType ||
			AVATAR_TASK_NOTE == m_nAvatarType ||
			AVATAR_TASK_PAPER_DOCS == m_nAvatarType)
	{
		FUI_DM_Documents* pFUI=dynamic_cast<FUI_DM_Documents*>(m_pParent);
		pFUI->Execute(m_nRecordID);
		
		//#1896 same handler as in double click (only in edit mode)
		pFUI->SetMode(FuiBase::MODE_EDIT);
		mouseDoubleClickEvent(NULL); 
	}
	else if(AVATAR_TASK_CALL == m_nAvatarType)
	{
		FUI_VoiceCallCenter* pFUI=dynamic_cast<FUI_VoiceCallCenter*>(m_pParent);

		//#1896 same handler as in double click (only in edit mode)
		pFUI->SetMode(FuiBase::MODE_EDIT);
		mouseDoubleClickEvent(NULL);
	}
}

void fui_avatar::onTaskDone_clicked()
{
	//if already done at the time of click
	if(m_bTaskDone)
	{
		//just clsoe the avatar
		OnMenu_Close();
		return;
	}

	//NOTE: adapted from CommWorkBenchWidget::on_taskDone_toolButton_clicked()

	QString strDialogWarning = tr("Do you really want to mark task as done?");
	int nResult=QMessageBox::question(this, tr("Warning"), strDialogWarning, tr("Yes"), tr("No"));
	if (nResult!=0) return; //only if YES

	DbRecordSet lstTasks;
	lstTasks.addColumn(QVariant::Int,"BTKS_ID");
	lstTasks.addRow();
	lstTasks.setData(lstTasks.getRowCount()-1,0, m_nTaskID);

	//[#927] Note: Re: Time in "Task Done" Phone Call Tasks set to 00:00
	//When a phone call task is marked as "Done" AND the start time is 00:00, 
	//then set the date and start time to the actual date time. The end time remains empty.
	//--> it is done on server.
	g_CommManager.SetTaskDone(lstTasks);

	//[#2711]
	//remove "Open on startup" from "done" tasks
	//is it already registered ?
	QString strState = EncodeState();
	QString strStateFixed = strState;
	int nPos1 = strStateFixed.indexOf(":");
	if(nPos1 >= 0)
		strStateFixed.remove(nPos1+1, 10000);
	QString strAvatars = g_pSettings->GetPersonSetting(AVATAR_STARTUP_LIST).toString();
	int nPos = strAvatars.indexOf(strStateFixed);
	if(nPos >= 0)	// already registered
		strAvatars.remove(nPos, strState.size());	// unregister
	//update settings
	g_pSettings->SetPersonSetting(AVATAR_STARTUP_LIST, strAvatars);
	//B.T. : issue 2700: save this at once, so if some other task comes we can reload safely just startup list...
	Status err;
	g_pSettings->SavePersonSettings(err);


	//refrehs internal data
	if(AVATAR_TASK_EMAIL == m_nAvatarType)
	{
		EMailDialog* pFUI=dynamic_cast<EMailDialog*>(m_pParent);
		pFUI->GetTaskRecordSet()->setData(0, "BTKS_SYSTEM_STATUS", GlobalConstants::TASK_STATUS_COMPLETED);
	}
	else if(AVATAR_TASK_ADDRESS == m_nAvatarType ||
			AVATAR_TASK_INTERNET_FILE == m_nAvatarType ||
			AVATAR_TASK_LOCAL_FILE == m_nAvatarType ||
			AVATAR_TASK_NOTE == m_nAvatarType ||
			AVATAR_TASK_PAPER_DOCS == m_nAvatarType)
	{
		FUI_DM_Documents* pFUI=dynamic_cast<FUI_DM_Documents*>(m_pParent);
		pFUI->GetTaskRecordSet()->setData(0, "BTKS_SYSTEM_STATUS", GlobalConstants::TASK_STATUS_COMPLETED);
	}
	else if(AVATAR_TASK_CALL == m_nAvatarType)
	{
		FUI_VoiceCallCenter* pFUI=dynamic_cast<FUI_VoiceCallCenter*>(m_pParent);
		pFUI->GetTaskRecordSet()->setData(0, "BTKS_SYSTEM_STATUS", GlobalConstants::TASK_STATUS_COMPLETED);
	}

	//disable the button
	ui.imgTaskState->setEnabled(false);
	//set "done" icon
	SetTaskState(CommGridViewHelper::NOT_SCHEDULED); 
}
