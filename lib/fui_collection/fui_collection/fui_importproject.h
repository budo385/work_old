#ifndef FUI_IMPORTPROJECT_H
#define FUI_IMPORTPROJECT_H

#include <QWidget>
#include "generatedfiles/ui_fui_importproject.h"
#include "fuibase.h"

class FUI_ImportProject : public FuiBase
{
    Q_OBJECT

public:
    FUI_ImportProject(QWidget *parent = 0);
    ~FUI_ImportProject();

	QString GetFUIName(){return tr("Import Projects");};
	void UpdateImportProjectsButton();

	DbRecordSet m_lstData;
	DbRecordSet m_lstActOrg;		// used to attach to Organization SAPNE pattern

private:
    Ui::FUI_ImportProjectClass ui;
	
	MainEntitySelectionController m_Per;
	MainEntitySelectionController m_Org;
	MainEntitySelectionController m_Dep;

private slots:
	void on_btnImportProjects_clicked();
	void on_btnBuildList_clicked();
};

#endif // FUI_IMPORTPROJECT_H
