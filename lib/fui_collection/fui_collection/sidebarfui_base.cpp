#include "sidebarfui_base.h"
#include "fui_voicecallcenter.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/styledpushbutton.h"
#include "bus_core/bus_core/globalconstants.h"

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "fuimanager.h"
extern FuiManager g_objFuiManager;	
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager

#include "qcw_base.h"
#include "fui_dm_documents.h"

//
//#include "sidebarfui_contact.h"
//#include "sidebarfui_project.h"

QList<SideBarFui_Base*> SideBarFui_Base::m_lstpInstances; //=QList<SideBarFui_Base*>; //init on empty list
#include <QDesktopWidget>

SideBarFui_Base::SideBarFui_Base(QWidget *parent)
	: CommunicationMenu_Base(parent)
{

	m_nFuiTypeID=-1;
	m_nEntityRecordID=-1;

	m_lstpInstances.append(this);

}

SideBarFui_Base::~SideBarFui_Base()
{
	m_lstpInstances.removeAt(m_lstpInstances.indexOf(this));
	CloseChildWindows();
}

//set all properties, before build
void SideBarFui_Base::BuildMenu()
{
	
	connect(GetDropZone_MenuWidget(),SIGNAL(ChapterCollapsed(bool)),this,SLOT(OnDropZoneChapterCollapsed(bool)));

	//set COMM TOOLBAR:
	GetFrameCommParent()->setStyleSheet("QFrame#frameCommParent "+ThemeManager::GetSidebarChapter_Bkg());
	//GetFrameCommToolBar()->setStyleSheet("QFrame#frameCommToolBar "+ThemeManager::GetSidebar_Bkg());
	GetFrameCommToolBar()->setStyleSheet("QFrame#frameCommToolBar {background:transparent}");
	GetFrameCommLabel()->setStyleSheet("QLabel "+ThemeManager::GetSidebarChapter_Font());

	QSize buttonSize(36, 36);
	StyledPushButton *pBtnEmail		=	new StyledPushButton(this,":Email32.png",":Email32.png","","",0,0);
	StyledPushButton *pBtnPhone		=	new StyledPushButton(this,":Phone32.png",":Phone32.png","","",0,0);
	StyledPushButton *pBtnDoc		=	new StyledPushButton(this,":Document32.png",":Document32.png","","",0,0);
	StyledPushButton *pBtnCalendar	=	new StyledPushButton(this,":CalendarEvent32.png",":CalendarEvent32.png","","",0,0);
	StyledPushButton *pBtnInternet	=	new StyledPushButton(this,":Earth32.png",":Earth32.png","","",0,0);
	StyledPushButton *pBtnChat		=	new StyledPushButton(this,":Comm_Chat32.png",":Comm_Chat32.png","","",0,0);
	StyledPushButton *pBtnSMS		=	new StyledPushButton(this,":Comm_SMS32.png",":Comm_SMS32.png","","",0,0);
	StyledPushButton *pBtnAppManager=	new StyledPushButton(this,":Comm_AppManager.png",":Comm_AppManager.png","","",0,0);

	//connect signals
	connect(pBtnEmail,SIGNAL(clicked()),this,SLOT(OnMenu_EmailClick()));
	connect(pBtnPhone,SIGNAL(clicked()),this,SLOT(OnPhoneCall()));
	connect(pBtnDoc,SIGNAL(clicked()),this,SLOT(OnMenu_DocumentClick()));
	connect(pBtnInternet,SIGNAL(clicked()),this,SLOT(OnInternet()));
	connect(pBtnChat,SIGNAL(clicked()),this,SLOT(OnChat()));
	connect(pBtnSMS,SIGNAL(clicked()),this,SLOT(OnSMS()));
	connect(pBtnAppManager,SIGNAL(clicked()),this,SLOT(OnMenu_ApplicationClick()));
	connect(pBtnCalendar,SIGNAL(clicked()),this,SLOT(OnMenu_CalendarClick()));
	

	pBtnEmail->setToolTip(tr("Email Manager"));
	pBtnPhone->setToolTip(tr("Make Phone Call"));
	pBtnDoc->setToolTip(tr("Document Manager"));
	pBtnInternet->setToolTip(tr("Website"));
	pBtnChat->setToolTip(tr("Chat"));
	pBtnSMS->setToolTip(tr("Send SMS"));
	pBtnAppManager->setToolTip(tr("Application Manager"));
	pBtnCalendar->setToolTip(tr("Calendar Manager"));
	

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
		pBtnCalendar->setVisible(false);

	//size
	
	pBtnEmail		->setMaximumSize(buttonSize);
	pBtnPhone		->setMaximumSize(buttonSize);
	pBtnDoc			->setMaximumSize(buttonSize);
	pBtnInternet	->setMaximumSize(buttonSize);
	pBtnChat		->setMaximumSize(buttonSize);
	pBtnSMS			->setMaximumSize(buttonSize);
	pBtnAppManager	->setMaximumSize(buttonSize);
	pBtnCalendar	->setMaximumSize(buttonSize);
	
	//Tool m_ProgressBar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnPhone);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnEmail);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnDoc);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnCalendar);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnChat);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnSMS);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnInternet);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(pBtnAppManager);
	buttonLayout->addStretch(1);

	buttonLayout->setContentsMargins(0,2,0,0);
	buttonLayout->setSpacing(0);

	GetFrameCommToolBar()->setLayout(buttonLayout);

	m_menuDropZone=GetDropZone_MenuWidget();
	//connect drop zone widget:
	connect(m_menuDropZone,SIGNAL(NeedNewData(int)),this,SLOT(SubMenuRequestedNewData(int)));
	m_menuDropZone->SetHidden();
	m_menuDropZone->SetFUIParent(m_nFuiTypeID);

	connect(m_menuDropZone,SIGNAL(NewDataDropped(int,DbRecordSet)),this,SLOT(OnDropZone_NewDataDropped(int,DbRecordSet)));

	GetFrameCommLabel()->setText(tr("Communication"));
}	


void SideBarFui_Base::OnMenu_EmailClick()
{
	if (!m_menuEmail) //if not created, create
	{
		m_menuEmail= new Email_MenuWidget(NULL,true);

		QPoint currentPos=this->mapToGlobal(GetFrameCommParent()->pos());
		//currentPos.setY(currentPos.y()+this->geometry().height());
		if (currentPos.x()<this->geometry().width())
			currentPos.setX(currentPos.x()+this->geometry().width()-2);
		else
			currentPos.setX(currentPos.x()-this->geometry().width()-4);

		//issue: 1258:
		//QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
		//if (currentPos.y()+this->geometry().height()>desktopRec.y()+desktopRec.height())
		//	currentPos.setY(desktopRec.y()+desktopRec.height()-m_menuEmail->height()-5);

		//issue 1871: align bottom with parent:
		currentPos.setY(QApplication::activeWindow()->frameGeometry().y()+QApplication::activeWindow()->frameGeometry().height()-280);

		m_menuEmail->move(currentPos);
		m_menuEmail->resize(this->geometry().width(),250);
		m_menuEmail->SetSideBarLayout();

		//connect(m_menuEmail,SIGNAL(NeedNewData()),this,SLOT(SubMenuRequestedNewData()));
		MenuEmail_Created();
		connect(m_menuEmail,SIGNAL(destroyed(QObject *)),this,SLOT(MenuEmail_Destroyed(QObject *)));

		m_menuEmail->DisableHide();

		//set title, go go
		m_menuEmail->setWindowTitle(QObject::tr("Email"));
		m_menuEmail->show();
	}
	else
		m_menuEmail->activateWindow();
	//if (m_menuEmail->IsHidden())
	//	m_menuEmail->SetHidden(false);
}

void SideBarFui_Base::OnMenu_DocumentClick()
{

	int nNewFUI = g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false, FuiBase::MODE_EMPTY, -1, true);
	QWidget* pFUI = g_objFuiManager.GetFUIWidget(nNewFUI);
	FUI_DM_Documents* pFuiDM = dynamic_cast<FUI_DM_Documents*>(pFUI);
	if (pFuiDM)
	{
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DOCUMENT);
		pFuiDM->SetDefaults(GlobalConstants::DOC_TYPE_NOTE, NULL, &m_lstContacts, &m_lstProjects);
		pFuiDM->show();  //show FUI
	}

	//if (!m_menuDoc) //if not created, create
	//{

	//	m_menuDoc= new Doc_MenuWidget(NULL,true);
	//	
	//	//connect(m_menuDoc,SIGNAL(NeedNewData()),this,SLOT(SubMenuRequestedNewData()));
	//	MenuDoc_Created();
	//	connect(m_menuDoc,SIGNAL(destroyed(QObject *)),this,SLOT(MenuDoc_Destroyed(QObject *)));

	//	QPoint currentPos=this->mapToGlobal(GetFrameCommParent()->pos());
	//	if (currentPos.x()<this->geometry().width())
	//		currentPos.setX(currentPos.x()+this->geometry().width()-2);
	//	else
	//		currentPos.setX(currentPos.x()-this->geometry().width()-4);

	//	//issue: 1258:
	//	//QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
	//	//if (currentPos.y()+this->geometry().height()>desktopRec.y()+desktopRec.height())
	//	//	currentPos.setY(desktopRec.y()+desktopRec.height()-m_menuDoc->height()-5);
	//
	//	//issue 1871: align bottom with parent:
	//	currentPos.setY(QApplication::activeWindow()->frameGeometry().y()+QApplication::activeWindow()->frameGeometry().height()-280);
	//	m_menuDoc->move(currentPos);
	//	m_menuDoc->resize(this->geometry().width(),250);
	//	m_menuDoc->SetSideBarLayout();

	//	m_menuDoc->DisableHide();

	//	//set title, go go
	//	m_menuDoc->setWindowTitle(QObject::tr("Documents & Applications"));
	//	m_menuDoc->show();
	//}
	//else
	//	m_menuDoc->activateWindow();
}

void SideBarFui_Base::OnMenu_ApplicationClick()
{
	if (!m_menuApp) //if not created, create
	{

		m_menuApp= new App_MenuWidget(NULL,true);

		//connect(m_menuApp,SIGNAL(NeedNewData()),this,SLOT(SubMenuRequestedNewData()));
		MenuApp_Created();
		connect(m_menuApp,SIGNAL(destroyed(QObject *)),this,SLOT(MenuApp_Destroyed(QObject *)));

		QPoint currentPos=this->mapToGlobal(GetFrameCommParent()->pos());
		if (currentPos.x()<this->geometry().width())
			currentPos.setX(currentPos.x()+this->geometry().width()-2);
		else
			currentPos.setX(currentPos.x()-this->geometry().width()-4);

		//issue: 1258:
		//QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
		//if (currentPos.y()+this->geometry().height()>desktopRec.y()+desktopRec.height())
		//	currentPos.setY(desktopRec.y()+desktopRec.height()-m_menuApp->height()-5);

		//issue 1871: align bottom with parent:
		currentPos.setY(QApplication::activeWindow()->frameGeometry().y()+QApplication::activeWindow()->frameGeometry().height()-280);
		m_menuApp->move(currentPos);
		m_menuApp->resize(this->geometry().width(),250);
		m_menuApp->SetSideBarLayout();

		//m_menuApp->DisableHide();

		//set title, go go
		m_menuApp->setWindowTitle(QObject::tr("Application Manager"));
		m_menuApp->show();
	}
	else
		m_menuApp->activateWindow();
}

void SideBarFui_Base::OnMenu_CalendarClick()
{

	if (!m_menuCalendar) //if not created, create
	{
		m_menuCalendar= new Calendar_MenuWidget(NULL,true);

		//connect(m_menuCalendar,SIGNAL(NeedNewData()),this,SLOT(SubMenuRequestedNewData()));
		MenuCalendar_Created();
		connect(m_menuCalendar,SIGNAL(destroyed(QObject *)),this,SLOT(MenuCalendar_Destroyed(QObject *)));

		QPoint currentPos=this->mapToGlobal(GetFrameCommParent()->pos());
		if (currentPos.x()<this->geometry().width())
			currentPos.setX(currentPos.x()+this->geometry().width()-2);
		else
			currentPos.setX(currentPos.x()-this->geometry().width()-4);

		currentPos.setY(QApplication::activeWindow()->frameGeometry().y()+QApplication::activeWindow()->frameGeometry().height()-280);
		m_menuCalendar->move(currentPos);
		m_menuCalendar->resize(this->geometry().width(),250);
		m_menuCalendar->SetSideBarLayout();

		m_menuCalendar->DisableHide();

		//set title, go go
		m_menuCalendar->setWindowTitle(QObject::tr("Calendar Manager"));
		m_menuCalendar->show();
	}
	else
		m_menuCalendar->activateWindow();
}

//if local selection use it! elsewhere emit signal
void SideBarFui_Base::SubMenuRequestedNewData(int nSubMenuType)
{
	if(!IsCurrentFUI())
		return;

	//issue: 1209
	//if (g_objFuiManager.IsActiveWindowFUI(m_nFuiTypeID))
	//	emit NeedNewData(); //if fui is along t' side, use it
	//else
	//{

	//issue 1275: if QCW is active and open in insert mode, use QCW data:
	if (m_nFuiTypeID==MENU_CONTACTS)
	{
		FuiBase *pBase=g_objFuiManager.GetActiveFUI();
		QCW_Base *pQCW=dynamic_cast<QCW_Base*>(pBase);
		if (pQCW)
		{
			DbRecordSet recData;
			int nMode;
			pQCW->GetDefaultValues(recData,nMode);
			if (recData.getRowCount()>0 && nMode==QCW_Base::MODE_SINGLE)
				//if (recData.getDataRef(0,"BCNT_ID").toInt()==0) //only insert mode
				{
					//DbRecordSet lstPhones=recData.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
					//_DUMP(lstPhones)

					SetDefaults(NULL,NULL,&recData);
					return;
				}
		}
	}

	
	//if no FUI, use selection if exists;:
	DbRecordSet lstContacts,lstProjects;
	QString strPhone="",strMail="",strInternet="";
	int nEmailReply=-1;
	GetCurrentDefaults(nSubMenuType,&lstContacts,&lstProjects,strMail,nEmailReply,strPhone,strInternet);
	DbRecordSet *plst1=NULL,*plst2=NULL;
	if (lstContacts.getRowCount()!=0)
		plst1=&lstContacts;
	if (lstProjects.getRowCount()!=0)
		plst2=&lstProjects;
	SetDefaults(plst1,plst2);
	SetDefaultMail(strMail,nEmailReply);
	SetDefaultInternet(strInternet);
	SetDefaultPhone(strPhone);

	//}
}

void SideBarFui_Base::MenuDoc_Destroyed(QObject *)
{
	m_menuDoc=NULL;
	MenuDoc_Created();  //clear pointer
}
void SideBarFui_Base::MenuEmail_Destroyed(QObject *)
{
	m_menuEmail=NULL;
	MenuEmail_Created(); //clear pointer

}
void SideBarFui_Base::MenuApp_Destroyed(QObject *)
{
	m_menuApp=NULL;
	MenuApp_Created();  //clear pointer
}

void SideBarFui_Base::MenuCalendar_Destroyed(QObject *)
{
	m_menuCalendar=NULL;
	MenuCalendar_Created();  //clear pointer
}



void SideBarFui_Base::MenuDoc_Created()
{
	//connect signal on all instances (disconnect is automatic on object destroy
	int nSize=m_lstpInstances.size();
	for(int i=0;i<nSize;++i)
	{
		m_lstpInstances.at(i)->SetSharedMenuDoc(m_menuDoc);
		if (m_menuDoc)
			connect(m_menuDoc,SIGNAL(NeedNewData(int)),m_lstpInstances.at(i),SLOT(SubMenuRequestedNewData(int)));
	}
}
void SideBarFui_Base::MenuEmail_Created()
{
	//connect signal on all instances (disconnect is automatic on object destroy
	int nSize=m_lstpInstances.size();
	for(int i=0;i<nSize;++i)
	{
		m_lstpInstances.at(i)->SetSharedMenuEmail(m_menuEmail);
		if (m_menuEmail)
			connect(m_menuEmail,SIGNAL(NeedNewData(int)),m_lstpInstances.at(i),SLOT(SubMenuRequestedNewData(int)));
	}
}


void SideBarFui_Base::MenuApp_Created()
{
	//connect signal on all instances (disconnect is automatic on object destroy
	int nSize=m_lstpInstances.size();
	for(int i=0;i<nSize;++i)
	{
		m_lstpInstances.at(i)->SetSharedMenuApp(m_menuApp);
		if (m_menuApp)
			connect(m_menuApp,SIGNAL(NeedNewData(int)),m_lstpInstances.at(i),SLOT(SubMenuRequestedNewData(int)));
	}
}
void SideBarFui_Base::MenuCalendar_Created()
{
	//connect signal on all instances (disconnect is automatic on object destroy
	int nSize=m_lstpInstances.size();
	for(int i=0;i<nSize;++i)
	{
		m_lstpInstances.at(i)->SetSharedMenuCalendar(m_menuCalendar);
		if (m_menuCalendar)
			connect(m_menuCalendar,SIGNAL(NeedNewData(int)),m_lstpInstances.at(i),SLOT(SubMenuRequestedNewData(int)));
	}
}

void SideBarFui_Base::CloseChildWindows()
{
	if (m_menuDoc)
	{
		m_menuDoc->close();
		m_menuDoc=NULL;
		MenuDoc_Created();
	}
	if (m_menuEmail)
	{
		m_menuEmail->close();
		m_menuEmail=NULL;
		MenuEmail_Created();
	}
	if (m_menuApp)
	{
		m_menuApp->close();
		m_menuApp=NULL;
		MenuApp_Created();
	}

	if (m_menuCalendar)
	{
		m_menuCalendar->close();
		m_menuCalendar=NULL;
		MenuCalendar_Created();
	}
}


void SideBarFui_Base::OnDropZoneChapterCollapsed(bool bCollapsed)
{
	this->updateGeometry();
	emit ChapterCollapsed(m_nFuiTypeID,2,false); //notify parent to resize
}


void SideBarFui_Base::OnThemeChanged()
{
	GetFrameCommParent()->setStyleSheet("QFrame#frameCommParent "+ThemeManager::GetSidebarChapter_Bkg());
	GetFrameCommToolBar()->setStyleSheet("QFrame#frameCommToolBar "+ThemeManager::GetSidebar_Bkg());
	GetFrameCommLabel()->setStyleSheet("QLabel "+ThemeManager::GetSidebarChapter_Font());
}



//issue 2246: create shortcuts:
bool SideBarFui_Base::ProcessKeyPressEventFromMainWindow ( QKeyEvent * event ) 
{
	//if(!g_pClientManager->IsLogged())return false;

	switch(event->key())
	{

	case Qt::Key_F5: //CTRL-F5: Phone Call
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier)
			{
				OnPhoneCall();
			}
			return true;
		}
		break;
	case Qt::Key_F6://CTRL-F6: Email Manager
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier)
			{
				OnMenu_EmailClick();
			}
			return true;
		}
		break;
	case Qt::Key_F7: //CTRL-F7: Document Manager
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier)
			{
				OnMenu_DocumentClick();
			}
			return true;
		}
		break;
	case Qt::Key_F8: //CTRL-F8: Calendar Manager
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) 
			{
				if (g_FunctionPoint.IsFPAvailable(FP_CALENDAR__BASIC_FUNCTIONALITY))
					OnMenu_CalendarClick();
			}
			return true;
		}
		break;
	case Qt::Key_F9: //CTRL-F9: New Chat
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) 
			{
				OnChat();
			}
			return true;
		}
		break;
	case Qt::Key_F10: //CTRL-F10: New SMS
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) 
			{
				OnSMS();
			}
			return true;
		}
		break;
	case Qt::Key_F11: //CTRL-F11: New Website
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) 
			{
				OnInternet();
			}
			return true;
		}
		break;
	case Qt::Key_F12: //CTRL-F12: Application Manager
		{
			event->accept();
			if (event->modifiers() & Qt::ControlModifier) 
			{
				OnMenu_ApplicationClick();
			}
			return true;
		}
		break;

	}

	return false;
}
