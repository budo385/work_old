#ifndef QC_ACCESSRIGHTSFRAME_H
#define QC_ACCESSRIGHTSFRAME_H

#include <QWidget>
#include "ui_qc_accessrightsframe.h"
#include "common/common/dbrecordset.h"

class QC_AccessRightsFrame : public QWidget
{
	Q_OBJECT

public:
	QC_AccessRightsFrame(QWidget *parent = 0);
	~QC_AccessRightsFrame();

	void Initialize(DbRecordSet *recFullContactData);
	void GetUarListsForSave(DbRecordSet &lstUAR,DbRecordSet &lstGAR);

private:
	Ui::QC_AccessRightsFrameClass ui;

	DbRecordSet *m_recFullContactData;
	void SetupGUIwidgets();
	int m_nEntityRecordID;
};

#endif // QC_ACCESSRIGHTSFRAME_H
