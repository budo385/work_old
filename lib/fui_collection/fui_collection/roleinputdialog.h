#ifndef ROLEINPUTDIALOG_H
#define ROLEINPUTDIALOG_H

#include <QWidget>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QDialog>
#include "generatedfiles/ui_roleinputdialog.h"

/*!
\class  RoleInputDialog
\ingroup FUICore/AccessRights
\brief  Role Input/Rename dialog. 

Role Input/Rename dialog.
*/
class RoleInputDialog : public QDialog
{
    Q_OBJECT

public:
    RoleInputDialog(QString *RoleName, QString *RoleDescription, int *nRoleType, bool bInsertRole, QWidget *parent = 0);
    ~RoleInputDialog();

private:
    Ui::RoleInputDialogClass ui;

	QString *m_pstrRoleName;		//< Role first name.
	QString *m_pstrRoleDescription;	//< Role last name.
	int		*m_nRoleType;			//< Role type (system or business).
	bool	m_bInsertRole;			//< Insert (or rename) bool.

private slots:
	void on_CancelPushButton_clicked();
	void on_OkPushButton_clicked();
	void on_BusVisibleCheckBox_stateChanged(int state);
	void on_RoleTypeCheckBox_stateChanged(int state);
};

#endif // ROLEINPUTDIALOG_H
