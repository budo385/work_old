#include "multiassigndialog.h"

MultiAssignDialog::MultiAssignDialog(DbRecordSet *ItemsRecordSet, QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	m_ItemsRecordSet = ItemsRecordSet;
	m_SelectedItemsRecordSet.copyDefinition(*m_ItemsRecordSet);

	ui.Selected_treeWidget->setAcceptDrops(true);
	ui.Select_treeWidget->setDragEnabled(true);
	ui.Selected_treeWidget->setSelectionMode(QAbstractItemView::ContiguousSelection);
	ui.Select_treeWidget->setSelectionMode(QAbstractItemView::ContiguousSelection);

	ui.OkPushButton->setDisabled(true);

	connect(ui.Selected_treeWidget, SIGNAL(RoleDropped()),  this, SLOT(on_ItemDropped()));

	InitializeSelectWidget();
}

MultiAssignDialog::~MultiAssignDialog()
{

}

DbRecordSet& MultiAssignDialog::GetSelectedItems()
{
	return m_SelectedItemsRecordSet;
}

void MultiAssignDialog::InitializeSelectWidget()
{
	int nItemCount = m_ItemsRecordSet->getRowCount();
	for (int i = 0; i < nItemCount; ++i)
	{
		QString ItemName	 = m_ItemsRecordSet->getDataRef(i, "BPER_FIRST_NAME").toString();
		QString ItemLastName = m_ItemsRecordSet->getDataRef(i, "BPER_LAST_NAME").toString();
		int		RowID		 = m_ItemsRecordSet->getDataRef(i, "BPER_ID").toInt();

		QTreeWidgetItem *Item = new QTreeWidgetItem();
		//Set display data.
		QString DisplayData = ItemName + " " + ItemLastName;
		Item->setData(0, Qt::UserRole,		RowID);
		Item->setData(0, Qt::DisplayRole,	DisplayData);
		Item->setIcon(0, QIcon(":omnis_1797.png"));

		m_hshItemsHash.insert(RowID, Item);
		m_hshItemIDToItemRowHash.insert(RowID, i);
	}

	//Setup tree.
	ui.Select_treeWidget->addTopLevelItems(m_hshItemsHash.values());
}

void MultiAssignDialog::SetTitle(QString strTitle)
{
	 setWindowTitle(strTitle);
}

void MultiAssignDialog::InsertItem(QTreeWidgetItem *Item)
{
	int RowID = Item->data(0, Qt::UserRole).toInt();
	if (!m_hshSelectedItemHash.contains(RowID))
	{
		QTreeWidgetItem *NewItem = new QTreeWidgetItem(*Item);
		m_hshSelectedItemHash.insert(RowID, NewItem);
		m_ItemsRecordSet->clearSelection();
		m_ItemsRecordSet->selectRow(m_hshItemIDToItemRowHash.value(RowID));
		m_SelectedItemsRecordSet.merge(*m_ItemsRecordSet, true);
		ui.Selected_treeWidget->addTopLevelItem(NewItem);
	}

	OkButtonStateChanged();
}

void MultiAssignDialog::RemoveItem(QTreeWidgetItem *Item)
{
	int RowID = Item->data(0, Qt::UserRole).toInt();
	Q_ASSERT(m_hshSelectedItemHash.contains(RowID));
	m_hshSelectedItemHash.remove(RowID);

	m_SelectedItemsRecordSet.clearSelection();
	m_SelectedItemsRecordSet.find(0, RowID);
	m_SelectedItemsRecordSet.deleteSelectedRows();

	int indexItem = ui.Selected_treeWidget->indexOfTopLevelItem(Item);
	ui.Selected_treeWidget->takeTopLevelItem(indexItem);
	delete(Item);

	OkButtonStateChanged();
}

void MultiAssignDialog::OkButtonStateChanged()
{
	if (m_hshSelectedItemHash.isEmpty())
		ui.OkPushButton->setDisabled(true);
	else
		ui.OkPushButton->setDisabled(false);
}

void MultiAssignDialog::on_CancelPushButton_clicked()
{
	reject();
}

void MultiAssignDialog::on_OkPushButton_clicked()
{
	accept();
}

void MultiAssignDialog::on_RemoveAll_pushButton_clicked()
{
	ui.Selected_treeWidget->selectAll();
	on_RemoveSelected_pushButton_clicked();	
}

void MultiAssignDialog::on_RemoveSelected_pushButton_clicked()
{
	QList<QTreeWidgetItem*> selectedItems = ui.Selected_treeWidget->selectedItems();
	foreach(QTreeWidgetItem *Item, selectedItems)
		RemoveItem(Item);
}

void MultiAssignDialog::on_AddAll_pushButton_clicked()
{
	ui.Select_treeWidget->selectAll();
	on_AddSelected_pushButton_clicked();
}

void MultiAssignDialog::on_AddSelected_pushButton_clicked()
{
	QList<QTreeWidgetItem*> selectedItems = ui.Select_treeWidget->selectedItems();
	foreach(QTreeWidgetItem *Item, selectedItems)
		InsertItem(Item);
}

void MultiAssignDialog::on_ItemDropped()
{
	on_AddSelected_pushButton_clicked();
}
