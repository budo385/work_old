#include "qc_salutationframe.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_client/bus_client/selection_combobox.h"
#include "qcw_largewidget.h"
#include "qcw_smallwidget.h"
#include "gui_core/gui_core/thememanager.h"

qc_salutationframe::qc_salutationframe(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	ui.addSelected_toolButton->setIcon(QIcon(":CopyText16.png"));
	ui.addSelected_toolButton_2->setIcon(QIcon(":CopyText16.png"));
}

qc_salutationframe::~qc_salutationframe()
{

}

void qc_salutationframe::Initialize(DbRecordSet *recFullContactData, QWidget *ParentWidget, int nParentType /*= 0 0-Large,1-small widget*/)
{
	m_nParentType = nParentType;
	m_pParentWidget = ParentWidget;
	m_recFullContactData = recFullContactData;
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

	//_DUMP(m_recLocalEntityRecordSet);

	SetupGUIwidgets(m_recLocalEntityRecordSet);
	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

void qc_salutationframe::SetupGUIwidgets(DbRecordSet recData)
{
	Selection_ComboBox SalutationHandler;
	SalutationHandler.Initialize(ENTITY_BUS_CM_TYPES);
	SalutationHandler.SetDataForCombo(ui.salutation_comboBox,"BCMT_TYPE_NAME","BCMT_TYPE_NAME");
	SalutationHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_SALUTATION);
	SalutationHandler.ReloadData();

	Selection_ComboBox ShortSalutationHandler;
	ShortSalutationHandler.Initialize(ENTITY_BUS_CM_TYPES);
	ShortSalutationHandler.SetDataForCombo(ui.shortsalutation_comboBox,"BCMT_TYPE_NAME","BCMT_TYPE_NAME","BCMA_SHORT_SALUTATION");
	ShortSalutationHandler.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_SHORT_SALUTATION);
	ShortSalutationHandler.ReloadData();
	
	QString strCurrentSalutation = m_recLocalEntityRecordSet.getDataRef(0, "BCMA_SALUTATION").toString();
	ui.salutation_comboBox->blockSignals(true);
	ui.salutation_comboBox->setEditText(strCurrentSalutation);
	ui.salutation_comboBox->blockSignals(false);

	QString strCurrentShortSalutation = m_recLocalEntityRecordSet.getDataRef(0, "BCMA_SHORT_SALUTATION").toString();
	ui.shortsalutation_comboBox->blockSignals(true);
	ui.shortsalutation_comboBox->setEditText(strCurrentShortSalutation);
	ui.shortsalutation_comboBox->blockSignals(false);
}

void qc_salutationframe::SetDataToRecordSet(QString strRecordSetFieldName, QString strValue)
{
	if (m_nParentType == 0)
		dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->SalutationChanged(strRecordSetFieldName, strValue);
	else if (m_nParentType == 1)
		dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->SalutationChanged(strRecordSetFieldName, strValue);

/*	
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();

	int nRowCount = m_recLocalEntityRecordSet.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		m_recLocalEntityRecordSet.setData(i, strRecordSetFieldName, strValue);
	
	_DUMP(m_recLocalEntityRecordSet);
	m_recFullContactData->setData(0, "LST_ADDRESS", m_recLocalEntityRecordSet);
*/
}

void qc_salutationframe::on_salutation_comboBox_editTextChanged(const QString &text)
{
	//If there is nothing to replace return.
	if (text.indexOf("[")==-1)
	{
		SetDataToRecordSet("BCMA_SALUTATION", text);
		return;
	}

	//In case there is no row.
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
	if (m_recLocalEntityRecordSet.getRowCount() == 0)
	m_recLocalEntityRecordSet.addRow();

	QString tmpText = text;

	tmpText.replace("[First_Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_FIRSTNAME").toString());
	tmpText.replace("[Last_Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_LASTNAME").toString());
	tmpText.replace("[Middle_Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_MIDDLENAME").toString());
	tmpText.replace("[Title]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_TITLE").toString());

	tmpText.replace("[First Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_FIRSTNAME").toString());
	tmpText.replace("[Last Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_LASTNAME").toString());
	tmpText.replace("[Middle Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_MIDDLENAME").toString());
	
	ui.salutation_comboBox->blockSignals(true);
	ui.salutation_comboBox->setEditText(tmpText);
	ui.salutation_comboBox->blockSignals(false);

	SetDataToRecordSet("BCMA_SALUTATION", tmpText);
}

void qc_salutationframe::on_shortsalutation_comboBox_editTextChanged(const QString &text)
{
	//If there is nothing to replace return.
	if (text.indexOf("[")==-1)
	{
		SetDataToRecordSet("BCMA_SHORT_SALUTATION", text);
		return;
	}

	//In case there is no row.
	m_recLocalEntityRecordSet = m_recFullContactData->getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
	if (m_recLocalEntityRecordSet.getRowCount() == 0)
		m_recLocalEntityRecordSet.addRow();

	QString tmpText = text;

	tmpText.replace("[First_Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_FIRSTNAME").toString());
	tmpText.replace("[Last_Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_LASTNAME").toString());
	tmpText.replace("[Middle_Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_MIDDLENAME").toString());
	tmpText.replace("[Title]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_TITLE").toString());

	tmpText.replace("[First Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_FIRSTNAME").toString());
	tmpText.replace("[Last Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_LASTNAME").toString());
	tmpText.replace("[Middle Name]",m_recLocalEntityRecordSet.getDataRef(0,"BCMA_MIDDLENAME").toString());

	ui.shortsalutation_comboBox->blockSignals(true);
	ui.shortsalutation_comboBox->setEditText(tmpText);
	ui.shortsalutation_comboBox->blockSignals(false);

	SetDataToRecordSet("BCMA_SHORT_SALUTATION", tmpText);
}

void qc_salutationframe::on_addSelected_toolButton_clicked()
{
	if (m_nParentType == 0)
		ui.salutation_comboBox->setEditText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.salutation_comboBox->setEditText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}

void qc_salutationframe::on_addSelected_toolButton_2_clicked()
{
	if (m_nParentType == 0)
		ui.shortsalutation_comboBox->setEditText(dynamic_cast<QCW_LargeWidget*>(m_pParentWidget)->GetSelectedText());
	else if (m_nParentType == 1)
		ui.shortsalutation_comboBox->setEditText(dynamic_cast<QCW_SmallWidget*>(m_pParentWidget)->GetSelectedText());
}
