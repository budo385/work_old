#ifndef TABLE_CONTACTSGRID_H
#define TABLE_CONTACTSGRID_H

#include <QComboBox>
#include <QPushButton>
#include <QSplitter>
#include "bus_client/bus_client/selection_gridviews.h"
#include "gui_core/gui_core/universaltablewidgetex.h"
#include "table_subcontactdata.h"

class Table_ContactsGrid : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_ContactsGrid(QWidget *parent);
	~Table_ContactsGrid();

	void Initialize(QWidget *pContactFUI,QComboBox *pComboViews,QPushButton *btnEditView,Table_SubContactData *pTableSub,QSplitter *splitterSub,QPushButton *btnRefresh);
	void SetFlagVisibile(bool bVisible);
	void GetSelectedContacts(DbRecordSet &lstContacts);

signals:
	void ShowContactDetails(int);

public slots:
	void OnActualListRebuilt(); //check dates, go to cache, reload all
	void OnRefreshContent();
	void RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);

private slots:
	void OpenViewEditor();
	void on_ViewChanged(int nIndex);
	void OnSelectionChanged();
	void OnShowDetails();
	void OnShowDetailsNewWindow();
	void OnDelete();
protected:
	void Data2CustomWidget(int nRow, int nCol);

private:
	void ReloadIfNeed();

	QComboBox *m_pComboViews;
	QPushButton *m_btnEditView;
	Table_SubContactData *m_pTableSub;
	QSplitter *m_splitterSub;
	QWidget *m_pContactFUI;
	QPushButton *m_btnRefresh;
	DbRecordSet m_lstData;
	int m_nGridID;
	Selection_GridViews m_ComboHandler;			//super handler: connected to cache
	DbRecordSet m_lstSubListData;
	QDateTime m_datLastBuild;
	QDateTime m_datActualContactListBuild;
	bool m_bVisible;
 };

#endif // TABLE_CONTACTSGRID_H
