#include "dropzone_menuwidget.h"
#include "trans/trans/xmlutil.h"
#include "fui_importemail.h"
#include "fui_importcontact.h"
#include "fui_importappointment.h"
#include "gui_core/gui_core/gui_helper.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/picturehelper.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/formatphone.h"
#include "gui_core/gui_core/thememanager.h"
#include <QTextDocumentFragment>
#include "bus_client/bus_client/documenthelper.h"
#include "common/common/datahelper.h"
#include "trans/trans/httpreadersync.h"
#include "gui_core/gui_core/logindlg.h"

#include "fuimanager.h"
extern FuiManager g_objFuiManager;
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "communicationmanager.h"
extern CommunicationManager				g_CommManager;				//global comm manager
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#include "os_specific/os_specific/mime_types.h"

DropZone_MenuWidget::DropZone_MenuWidget(QWidget *parent)
	: QWidget(parent)//,m_DropDoc(NULL)
{
	ui.setupUi(this);
	m_nFUI=-1;
	

	this->setAttribute(Qt::WA_DeleteOnClose, true);
	setAcceptDrops(true);


	ui.labelPic->setToolTip(tr("Drop items here to assign them to a contact or project: Files, Other Contacts, Emails, Projects, vCards,..."));
	ui.textEdit->setToolTip(tr("Paste text here to assign it to the actual contact or project: Phone Numbers, Addresses, vCards, etc,"));

	//connect(&g_ThemeManager,SIGNAL(ThemeChanged()),this,SLOT(OnThemeChanged()));

	//ui.textEdit->set
	//CLIPBOARD:
	//NOTE: you can not connect CTRL+V coz this shortcut is tied to picture widget (they receive focus on click...)
	/*
	QAction *pActPastFromClipboard = new QAction(tr("Paste Picture From Clipboard"),this);
	pActPastFromClipboard->setShortcut(tr("CTRL+V"));
	//pActPastFromClipboard->setShortcutContext(Qt::WidgetShortcut);
	pActPastFromClipboard->setIcon(QIcon(":paste.png"));
	connect(pActPastFromClipboard, SIGNAL(triggered()), this, SLOT(OnPasteFromClipboard()));
	addAction(pActPastFromClipboard);
	setFocusPolicy(Qt::ClickFocus);		//add focus
	*/
	//ui.textEdit->EnableDropSignal();
	m_bTextDropWasEmpty=true;

	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
	{
		GUI_Helper::CreateStyledButton(ui.btnNewEntity,":DropZone24.png",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Drop Zone")),15,ThemeManager::GetCEMenuButtonStyle(),10);
		ui.btnNewEntity->setMinimumHeight(26);
		ui.btnHide->setMinimumHeight(26);
		ui.btnQCW->setMinimumHeight(26);
	}
	else
		GUI_Helper::CreateStyledButton(ui.btnNewEntity,":DropZone32.png",QString("<html><b><i><font size=\"4\">%1 </font></i></b></html>").arg(tr("Drop Zone")),15,ThemeManager::GetCEMenuButtonStyle(),10);

	ui.btnHide->setFocusPolicy(Qt::NoFocus);
	ui.btnQCW->setFocusPolicy(Qt::NoFocus);
	ui.btnNewEntity->setFocusPolicy(Qt::NoFocus);
	//connect(ui.btnNewEntity,SIGNAL(clicked()),this,SLOT(OnMenuButton()));


	ui.btnHide->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
	ui.btnHide->hide();
	connect(ui.btnHide, SIGNAL(clicked()), this, SLOT(OnHideTab()));

	ui.btnQCW->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	ui.btnQCW->setIcon(QIcon(":DbIcon_View16.png"));
	ui.btnQCW->hide();

	ui.labelPic->setPixmap(QPixmap(ThemeManager::GetCEDropZonePicture()));
	//ui.labelPic->setPixmap(QPixmap(":Desktop48.png"));

	//ui.frameBkg->setStyleSheet(" QFrame {background-color:white;color:black}");
	//ui.label->setStyleSheet(" QLabel {color:black}");

	ui.btnInternet->setIcon(QIcon(":Earth16.png"));
	ui.btnAddress->setIcon(QIcon(":Adress16.png"));
	ui.btnEmail->setIcon(QIcon(":Email16.png"));
	ui.btnPhone->setIcon(QIcon(":Phone16.png"));
	ui.btnInternet->setToolTip(tr("Add Selected Text As Internet Address to the Contact"));
	ui.btnAddress->setToolTip(tr("Add Selected Text As Address to the Contact"));
	ui.btnEmail->setToolTip(tr("Add Selected Text As Email Address to the Contact"));
	ui.btnPhone->setToolTip(tr("Add Selected Text As Phone Number to the Contact"));


	//QString	strFrameLayout="QFrame#frameBkg { border-width: 4px; color: white; border-image:url(:DropZone_BG.png) 4px 4px 4px 4px stretch stretch }";
	ui.frameBkg->setStyleSheet(ThemeManager::GetCEDropZoneBkgStyle());

	g_ChangeManager.registerObserver(this);
}

DropZone_MenuWidget::~DropZone_MenuWidget()
{

}


void DropZone_MenuWidget::dragEnterEvent ( QDragEnterEvent * event )
{
	//if (event->mimeData()->hasUrls())
	event->accept();

}

void DropZone_MenuWidget::dragMoveEvent(QDragMoveEvent *event )
{
	event->acceptProposedAction();
}


//if TAB= template then template, else doc, if not exe...brb....
void DropZone_MenuWidget::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	this->activateWindow(); //bring on top_:

	//------------------------------------------------
	//DOCS && Vcard ->import window or doc popup
	//------------------------------------------------
	if (mime->hasUrls())
	{
		QList<QUrl> lst =mime->urls();
		emit NeedNewData(CommunicationMenu_Base::SUBMENU_DROPZONE);		//<<connect docs to current fui selected data
		bool bIgnoreHTTP=false;
		if (m_nFUI==MENU_CONTACTS)
		{
			//B.T. issue 2: ignore picture drop on contact avatar
			/*bIgnoreHTTP=true;
			//issue 2215: if link with picture, then extract picture and open new contact or in edit mode set new picture!!!
			if (SaveAsContactPictureIfPictureLink(lst))
			{
				event->accept();
				return;
			}*/
		}
		if(!g_CommManager.ProcessDocumentDrop(lst,false,false,bIgnoreHTTP))
		{
			//if url drop, and not Vcaed, use as internet link to contact:
			if (lst.size()==1) 
			{
				//issue 2524
				DbRecordSet lstPaths;
				DbRecordSet lstApps;
				DataHelper::ParseFilesFromURLs(lst,lstPaths,lstApps,false,false);
				if (lstPaths.getDataRef(0,1).toString().right(4)==".vcf" || lstPaths.getDataRef(0,1).toString().right(4)==".vcard")
				{
					DbRecordSet lstImportContacts;
					if (lst.at(0).scheme().toUpper()==QString("http").toUpper())
						lstImportContacts = ClientContactManager::CreateContactsFromvCardUrl(lst.at(0).toString());
					else
						lstImportContacts = ClientContactManager::CreateContactsFromvCard(lstPaths);

					emit NewDataDropped(ENTITY_CONTACT_VCARD_IMPORT,lstImportContacts);
				}
				else if (lst.at(0).scheme().toUpper()==QString("http").toUpper())
				{
					ui.textEdit->blockSignals(true);
					ui.textEdit->setText(lst.at(0).toString());
					ui.textEdit->blockSignals(false);
					m_bTextDropWasEmpty=true;
					OnTextChanged();
				}
			}
		}
		event->accept();
		return;
	}

	//------------------------------------------------
	//OUTLOOK ->import window
	//------------------------------------------------

	if (mime->hasFormat(OUTLOOK_MIME_MAIL_ID))
	{
		if (!g_FunctionPoint.IsFPAvailable(FP_EMAILS_OUTLOOK))
		{
			event->accept();
			return;
		}

		//import emails:
		//QByteArray arData = mime->data("RenPrivateMessages");
		QByteArray arData = mime->data(OUTLOOK_MIME_MAIL_ID);
		if(arData.size() > 0)
		{
			if(FUI_ImportEmail::IsDropContact(arData))
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook contact dropped"));

				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportContact* pFuiW=dynamic_cast<FUI_ImportContact*>(pFUI);
				if (pFuiW)
				{
					pFuiW->BuildImportListFromDrop(arData);
				}
				if(pFUI)pFUI->show();  //show FUI
			}
			else if(FUI_ImportEmail::IsDropAppointment(arData))
			{

				//B.T.2013-08-09, crash when appointment is dropped
				QMessageBox::information(this,tr("Warning"),tr("Appointment import is not supported!"));
				/*
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook appointment dropped"));

				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_APPOINTMENT, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportAppointment* pFuiW=dynamic_cast<FUI_ImportAppointment*>(pFUI);
				if (pFuiW)
				{
					pFuiW->BuildImportListFromDrop(arData);
				}
				if(pFUI)pFUI->show();  //show FUI
				*/
			}
			else// if(FUI_ImportEmail::IsDropEmail(arData))
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook email dropped"));

				event->accept();

				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Open import email window"));

				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_EMAIL, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportEmail* pFuiDM=dynamic_cast<FUI_ImportEmail*>(pFUI);
				if (pFuiDM)
				{
					pFuiDM->BuildImportListFromDrop(arData);
				}
				if(pFUI){
					pFUI->show();  //show FUI
					pFUI->raise();
					pFUI->activateWindow();

					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import email window displayed"));
				}
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Exit dropzone event"));
				return;
			}
		}
		event->accept();
		return;
	}


	//------------------------------------------------
	// LIST of record->emit special signal
	//------------------------------------------------

	//internal/external
	if (mime->hasFormat(SOKRATES_MIME_LIST))
	{
		int nEntityID;
		DbRecordSet lstDropped;

		//import emails:
		QByteArray arData = mime->data(SOKRATES_MIME_LIST);
		if(arData.size() > 0)
		{
			lstDropped=XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
			//lstDropped.Dump();
		}
		QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
		if(arDataType.size() > 0)
		{
			bool bOK;
			nEntityID=arDataType.toInt(&bOK);
			if (!bOK) nEntityID=-1;
		}

		if (nEntityID>=0 && lstDropped.getRowCount()>0)
		{
			emit NewDataDropped(nEntityID,lstDropped);
		}
		event->accept();
		return;
	}



	//------------------------------------------------
	// Picture process
	//------------------------------------------------
	if (mime->hasImage())
	{
		QVariant var=mime->imageData();
		if (!var.isNull())
		{
			//convert to bytearray PNG
			QImage image = qvariant_cast<QImage>(var);
			QString strFormat="PNG";
			QByteArray bufPicture;
			QBuffer buffer(&bufPicture);
			buffer.open(QIODevice::WriteOnly);		
			image.save(&buffer, strFormat.toLatin1());			
			DbRecordSet lstData;
			lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
			lstData.addRow();
			lstData.setData(0,"BPIC_PICTURE",bufPicture);
			emit NewDataDropped(ENTITY_BUS_BIG_PICTURE,lstData);
			return;
		}
		event->accept();
		return;
	}


	//------------------------------------------------
	// Text drop (last)
	//------------------------------------------------
	if (mime->hasFormat("text/plain"))
	{
		ui.textEdit->blockSignals(true);
		ui.textEdit->setPlainText(mime->text());
		ui.textEdit->blockSignals(false);
		m_bTextDropWasEmpty=true;
		OnTextChanged();
		event->accept();
		return;
	}


	//default process:
	QWidget::dropEvent(event);
}


void DropZone_MenuWidget::OnHideTab()
{
	bool bVisible=ui.frameBkg->isVisible();

	if (bVisible)
	{
		ui.btnHide->setIcon(QIcon(":Icon_Plus12.png"));
		ui.frameBkg->setVisible(false);
		emit ChapterCollapsed(true);
	}
	else
	{
		ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
		ui.frameBkg->setVisible(true);
		emit ChapterCollapsed(false);
	}
}


bool DropZone_MenuWidget::IsHidden()
{
	return !ui.frameBkg->isVisible();
}
void DropZone_MenuWidget::SetHidden(bool bHide, bool bSkipSignals)
{
	if (bHide)
	{
		ui.btnHide->setIcon(QIcon(":Icon_Plus12.png"));
		ui.frameBkg->setVisible(false);
		if (!bSkipSignals)
			emit ChapterCollapsed(true);
	}
	else
	{
		ui.btnHide->setIcon(QIcon(":Icon_Minus12.png"));
		ui.frameBkg->setVisible(true);
		if (!bSkipSignals)
			emit ChapterCollapsed(false);
	}
}

//when text is changed
void DropZone_MenuWidget::OnTextChanged()
{
	QString strTextDrop=ui.textEdit->toPlainText();
	//if (strTextDrop.isEmpty()) 
	//{
	//	m_bTextDropWasEmpty=false;
	//	return;
	//}
	//else if (m_bTextDropWasEmpty)
	ProcessTextDrop(strTextDrop); //process only if it was empty before...
}

//parse text, emit signal
void DropZone_MenuWidget::ProcessTextDrop(QString strTextDrop) //QString &textDrop)
{
	strTextDrop=strTextDrop.trimmed();

	//VCARD:
	if (strTextDrop.indexOf("BEGIN:VCARD")!=-1)
	{
		DbRecordSet lstImportContacts = ClientContactManager::CreateContactsFromvCardData(strTextDrop);
		if (lstImportContacts.getRowCount()>1)
		{
			int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
			FUI_ImportContact* pFui=dynamic_cast<FUI_ImportContact*>(g_objFuiManager.GetFUIWidget(nNewFUI));
			if (pFui)
			{
				pFui->SetImportListFromSource(lstImportContacts);
				pFui->show();  
			}
		}
		else //issue 2524
		{
			emit NewDataDropped(ENTITY_CONTACT_VCARD_IMPORT,lstImportContacts);
		}
		return;
	}

	//if more then 1 line then it is multi part processing
	if (strTextDrop.count("\n")>1)
		return;

	//HTTP:
	if (strTextDrop.indexOf("http://")!=-1 || strTextDrop.indexOf("www.")!=-1 )
	{
		ProcessInternet(strTextDrop);
		return;
	}


	//MAIL:
	if (strTextDrop.indexOf("@")!=-1)
	{
		ProcessEMail(strTextDrop);
		return;
	}

	//TELEFON:
	bool bOK;
	int nDummy=strTextDrop.simplified().replace(" ","").toInt(&bOK);
	if (bOK || strTextDrop.indexOf("Mobile")!=-1 || strTextDrop.indexOf("Tel")!=-1 || strTextDrop.indexOf("Phone")!=-1 ||strTextDrop.indexOf("Fax")!=-1 || strTextDrop.indexOf("+")==0)
	{
		ProcessPhone(strTextDrop);
		return;
	}

	//REST: leave as is:

}


void DropZone_MenuWidget::ProcessAddress(QString strTextDrop)
{
	DbRecordSet lstData;
	//lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
	lstData.addColumn(QVariant::Int,"BCMA_ID");
	lstData.addColumn(QVariant::String,"BCMA_FORMATEDADDRESS");
	lstData.addColumn(QVariant::Int,"BCMA_IS_DEFAULT");
	lstData.addColumn(QVariant::Int,"BCMA_CONTACT_ID");
	lstData.addColumn(QVariant::Int,"NM_TEMP_ID");
	lstData.addColumn(DbRecordSet::GetVariantType(),"LST_TYPES");
	lstData.addRow();
	//lstData.setData(0,"BCME_NAME",strText);
	lstData.setData(0,"BCMA_FORMATEDADDRESS",strTextDrop);
	emit NewDataDropped(ENTITY_BUS_CM_ADDRESS,lstData);
	return;
}

void DropZone_MenuWidget::ProcessEMail(QString strTextDrop)
{
	QStringList lstItems=strTextDrop.trimmed().split(" ",QString::SkipEmptyParts,Qt::CaseInsensitive);
	int nSize=lstItems.size();
	for(int i=0;i<nSize;++i)
	{
		QString strText =lstItems.at(i);
		if (strText.indexOf("@")!=-1)
		{

			DbRecordSet lstData;
			lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL));
			lstData.addRow();
			//lstData.setData(0,"BCME_NAME",strText);
			lstData.setData(0,"BCME_ADDRESS",strText);
			lstData.setData(0,"BCME_TYPE_ID",ClientContactManager::GetDefaultType(ContactTypeManager::TYPE_EMAIL_ADDRESS));
			emit NewDataDropped(ENTITY_BUS_CM_EMAIL,lstData);
			return;
		}
	}

}
void DropZone_MenuWidget::ProcessPhone(QString strTextDrop)
{
	QStringList lstItems=strTextDrop.trimmed().split(" ",QString::SkipEmptyParts,Qt::CaseInsensitive);
	int nSize=lstItems.size();
	for(int i=0;i<nSize;++i)
	{
		QString strText =lstItems.at(i);
		bool bOk=false;
		float nDummy=strText.toFloat(&bOk);
		if (strText.indexOf("+")==0 || bOk)
		{
			QString strName;
			for(int k=0;k<i;++k)
			{
				strName+=lstItems.at(k)+" ";
			}
			strName=strName.trimmed();

			for(int k=i+1;k<nSize;++k)
			{
				strText+=" "+lstItems.at(k);
			}

			DbRecordSet lstData;
			lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE));
			lstData.addRow();
			lstData.setData(0,"BCMP_NAME",strName);
			lstData.setData(0,"BCMP_FULLNUMBER",strText);
			//calculate formated number:
			QString strPhone,strCountry,strLocal,strISO,strArea,strSearch;
			FormatPhone PhoneFormatter;
			PhoneFormatter.SplitPhoneNumber(strText,strCountry,strArea,strLocal,strSearch,strISO);
			lstData.setData(0,"BCMP_FULLNUMBER",PhoneFormatter.FormatPhoneNumber(strCountry,strArea,strLocal));
			lstData.setData(0,"BCMP_COUNTRY",strCountry);
			lstData.setData(0,"BCMP_AREA",strArea);
			lstData.setData(0,"BCMP_LOCAL",strLocal);
			lstData.setData(0,"BCMP_COUNTRY_ISO",strISO);
			lstData.setData(0,"BCMP_SEARCH",strSearch);
			lstData.setData(0,"BCMP_TYPE_ID",ClientContactManager::GetDefaultType(ContactTypeManager::TYPE_PHONE));

			//format back-forth....
			emit NewDataDropped(ENTITY_BUS_CM_PHONE,lstData);
			return;
		}
	}

}
void DropZone_MenuWidget::ProcessInternet(QString strTextDrop)
{

	QStringList lstItems=strTextDrop.trimmed().split(" ",QString::SkipEmptyParts,Qt::CaseInsensitive);
	int nSize=lstItems.size();
	for(int i=0;i<nSize;++i)
	{
		QString strText =lstItems.at(i);
		if (strText.indexOf("http://")!=-1 || strText.indexOf("www.")!=-1)
		{
			QString strName=lstItems.at(0);
			if (strName==strText)
				strName="";

			DbRecordSet lstData;
			lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET));
			lstData.addRow();
			lstData.setData(0,"BCMI_NAME",strName);
			lstData.setData(0,"BCMI_ADDRESS",strText);
			lstData.setData(0,"BCMI_TYPE_ID",ClientContactManager::GetDefaultType(ContactTypeManager::TYPE_INTERNET));
			emit NewDataDropped(ENTITY_BUS_CM_INTERNET,lstData);
			return;
		}
	}
}


//clipboard event occurd: resolve: (does not happen: can get CTRL+V, coz pic)
void DropZone_MenuWidget::OnPasteFromClipboard()
{
	/*
	QPixmap pix;
	if(!PictureHelper::PastePictureFromClipboard(pix))
	{
		QClipboard *clipboard = QApplication::clipboard();
		QString strText=clipboard->text();
		if (!strText.isEmpty())
		{
			ui.textEdit->setText(strText);
			ProcessTextDrop();
			return;
		}
	}
	else
	{
		QByteArray bufPicture;
		PictureHelper::ConvertPixMapToByteArray(bufPicture,pix);
		DbRecordSet lstData;
		lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
		lstData.addRow();
		lstData.setData(0,"BPIC_PICTURE",bufPicture);
		emit NewDataDropped(ENTITY_BUS_BIG_PICTURE,lstData);
		return;
	}
	*/
}


void DropZone_MenuWidget::on_btnInternet_clicked()
{
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	ProcessInternet(strSelected);

	
}

void DropZone_MenuWidget::on_btnAddress_clicked()
{
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	ProcessAddress(strSelected);
	
}

void DropZone_MenuWidget::on_btnEmail_clicked()
{
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	ProcessEMail(strSelected);
	
}
void DropZone_MenuWidget::on_btnPhone_clicked()
{
	QString strSelected=ui.textEdit->textCursor().selection().toPlainText();
	ProcessPhone(strSelected);


}


void DropZone_MenuWidget::OnThemeChanged()
{
	ui.btnNewEntity->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	ui.btnHide->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	ui.labelPic->setPixmap(QPixmap(ThemeManager::GetCEDropZonePicture()));
	ui.frameBkg->setStyleSheet(ThemeManager::GetCEDropZoneBkgStyle());
	ui.btnQCW->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
}


void DropZone_MenuWidget::on_btnQCW_clicked()
{
	//The Drop Zone contains a pushbutton icon for �Details� which opens the QTW. 
	//If a contact is already being edited, the window is loaded for this contact in edit mode, if not, 
	//it is just opened containing the text from the drop zone text area. 
	//A new contact will be generated when the user presses one of the icons.

	//??????? What is this?->open QCW in insert mode->copy text data....and execute parse...
	g_objFuiManager.OpenQCWWindow(this,-1,NULL,ui.textEdit->toPlainText());
}


void DropZone_MenuWidget::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}
}

//if one link is dropped and it is link and only in Contact FUI and it is picture (png, jpg, etc..) extract picture and edit contact
bool DropZone_MenuWidget::SaveAsContactPictureIfPictureLink(QList<QUrl> &lst)
{
	if (lst.size()!=1)
		return false;

	DbRecordSet lstPaths;
	DbRecordSet lstApps;

	DataHelper::ParseFilesFromURLs(lst,lstPaths,lstApps,false,false);

	if (lstPaths.getRowCount()!=1)
		return false;

	QString strExtension=lstPaths.getDataRef(0,2).toString().toLower();
	if (strExtension=="bmp" || strExtension=="jpeg" || strExtension=="jpg" || strExtension=="gif" || strExtension=="png" || strExtension=="tiff" )
	{
		//load picture from file
		QString strFilePath=lstPaths.getDataRef(0,0).toString();
		QFileInfo info(strFilePath);
		QByteArray bufPicture;
		QString strFileName=DocumentHelper::LoadFileContent(bufPicture,strFilePath);
		if (strFileName.isEmpty())
			return false;

		DbRecordSet lstData;
		lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
		lstData.addRow();
		lstData.setData(0,"BPIC_PICTURE",bufPicture);
		emit NewDataDropped(ENTITY_BUS_BIG_PICTURE,lstData);
		return true;
	}
	else if (strExtension=="http")
	{
		QString strExtensionURL=lstPaths.getDataRef(0,1).toString().right(4).toLower();
		if (strExtensionURL==".bmp" || strExtensionURL==".jpeg" || strExtensionURL==".jpg" || strExtensionURL==".gif" || strExtensionURL==".png" || strExtensionURL==".tiff" )
		{
			int nRes=QMessageBox::question(NULL,tr("Confirmation"),tr("Picture storage location:"),tr("Download Picture And Set As Contact Picture"),tr("Store Link/File As Document"),tr("Cancel"));
			if (nRes==1)
				return false; //proceed as usual
			if (nRes==0)
			{
				QString strFilePath=lstPaths.getDataRef(0,0).toString();
				QByteArray bufPicture;

				if(GetLinkContent(strFilePath,bufPicture))
				{
					DbRecordSet lstData;
					lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
					lstData.addRow();
					lstData.setData(0,"BPIC_PICTURE",bufPicture);
					emit NewDataDropped(ENTITY_BUS_BIG_PICTURE,lstData);
					return true;
				}
			}
			else
				return true; //cancel: abort
		}
	}

	return false;
}

bool DropZone_MenuWidget::GetLinkContent(QString strFilePath, QByteArray &content)
{
	HttpReaderSync WebReader(6000);  //6sec..
	connect(&WebReader,SIGNAL(ReEmitAuthenticationRequired ( const QString & , quint16 , QAuthenticator *  )),this, SLOT(OnEmitAuthenticationRequired ( const QString & , quint16 , QAuthenticator *  )));

	return WebReader.ReadWebContent(strFilePath, &content);
}

void DropZone_MenuWidget::OnEmitAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth)
{
	LoginDlg Dlg;
	Dlg.Initialize("",LoginDialogAbstract::LOGIN_DLG_WEB_AUTHENTICATION);
	Dlg.setWindowTitle(tr("Connect to ")+hostname);
	int nRes=Dlg.exec();
	if (nRes)
	{
		auth->setPassword(Dlg.m_strPass);
		auth->setUser(Dlg.m_strUser);
	}
}

