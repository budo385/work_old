#include "emailimporttable.h"
#include "os_specific/os_specific/mime_types.h"
#include "fui_importemail.h"
#include <QMimeData>

EmailImportTable::EmailImportTable(QWidget *parent)
{
	m_pParent = NULL;
	setAcceptDrops(true);
	setDropIndicatorShown(true);
}
    
EmailImportTable::~EmailImportTable()
{
}

void EmailImportTable::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	if (mime->hasFormat(OUTLOOK_MIME_MAIL_ID))
	{
		//import emails by DnD
		QByteArray arData = mime->data(OUTLOOK_MIME_MAIL_ID);
		if(arData.size() > 0)
			if(m_pParent)
				m_pParent->BuildImportListFromDrop(arData,false,-1);

		event->accept();
	}
}

void EmailImportTable::dragMoveEvent ( QDragMoveEvent * event )
{
	event->accept();
}

void EmailImportTable::dragEnterEvent ( QDragEnterEvent * event )
{
	if (event->mimeData()->hasFormat(OUTLOOK_MIME_MAIL_ID))
		event->acceptProposedAction();
}

