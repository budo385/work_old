#ifndef COMMUNICATIONACTIONS_H
#define COMMUNICATIONACTIONS_H

#include <QString>
#include <QPoint>
#include "common/common/dbrecordset.h"

/*!
	\class  CommunicationActions
	\brief  For handling communication events
	\ingroup GUICore


	Use:
	- use static functions, pass parameters
*/
class CommunicationActions //: public QObject
{
	//Q_OBJECT

public:
	//CommunicationActions(QObject *parent);
	//~CommunicationActions();
	//contact:
	static void sendEmail(int nContactID=0,QString strDefaultEmail="",int nProjectID=-1,QPoint *pos=NULL,QStringList *lstPackSendPaths=NULL,int nEmailReplayID=-1, bool IsForwardMode=false,bool bScheduleTask=false);
	static void callPhone(int nContactID=0,QString strDefaultPhone="",int nProjectID=-1);
	static void sendDocument(int nContactID=0,QString strDefaultDoc="",QPoint *pos=NULL);
	static void openWeb(int nContactID=0,QString strDefaultInternet="");
	static void openChat(int nContactID);
	static void openSMS(int nContactID);
	//project:
	static void sendEmail_Project(int nProjectID=0,QString strDefaultEmail="",QPoint *pos=NULL,QStringList *lstPackSendPaths=NULL,int nEmailReplayID=-1, bool IsForwardMode=false,bool bScheduleTask=false);
	static void callPhone_Project(int nProjectID=0,QString strDefaultPhone="");
	static void sendDocument_Project(int nProjectID=0,QString strDefaultDoc="",QPoint *pos=NULL);
	static void openWeb_Project(int nProjectID=0,QString strDefaultInternet="");
	static void openChat_Project(int nProjectID);
	static void openSMS_Project(int nProjectID);
	static bool getProjectContacts(int nProjectID, DbRecordSet &lstContacts);
	//issue 1456:
	static void createEmailQuickTask(DbRecordSet &lstEmails, QPoint *pos=NULL, bool bForceSilentCreate = false);
	static void createPhoneQuickTask(DbRecordSet &lstPhones, QPoint *pos=NULL, bool bForceSilentCreate = false);
	//issue 1450:
	static void sendEmail_Serial(DbRecordSet &lstContactDetail /*BCNT_ID must exists*/, QPoint *pos);


private:
	
};

#endif // COMMUNICATIONACTIONS_H
