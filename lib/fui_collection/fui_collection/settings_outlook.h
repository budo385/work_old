#ifndef SETTINGS_OUTLOOK_H
#define SETTINGS_OUTLOOK_H

#include <QWidget>
#include "ui_settings_outlook.h"
#include "settingsbase.h"

class Settings_Outlook : public SettingsBase
{
    Q_OBJECT

public:
    Settings_Outlook(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
    ~Settings_Outlook();

	DbRecordSet GetChangedValuesRecordSet();
	void GetFolderSetting(QList<QStringList> &lstFolders);
	void RefreshInfoLabel();

private:
	bool m_bScanDateChanged;
    Ui::Settings_OutlookClass ui;
	QList<QStringList> m_lstOutlookFoldersScan;

	int m_nIniLstRow;

private slots:
	void on_btnSelectFolders_clicked();
	void on_btnResetFolders_clicked();
	void on_scannedDateTime_changed(const QDateTime &dateTime);
	void on_chkImport_clicked(bool bChecked);
};

#endif // SETTINGS_OUTLOOK_H
