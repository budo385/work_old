#include "gridfilterselectionwizpage.h"
#include <QLayout>
#include "trans/trans/xmlutil.h"

//GLOBALS:
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "menuitems.h"


GridFilterSelectionWizPage::GridFilterSelectionWizPage(int nWizardPageID, QString strPageTitle, QWidget *parent)
	: WizardPage(nWizardPageID, strPageTitle)
{
}

GridFilterSelectionWizPage::~GridFilterSelectionWizPage()
{
	m_pCommGridFilter = NULL;
}

void GridFilterSelectionWizPage::Initialize()
{
	//If already initialized return.
	if (m_bInitialized)
		return;

	m_bInitialized = true;
	
	//Define locals.
	m_rFilterViews.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW));
	m_rFilterViewData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
	m_recResultRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_COMMGRIDFILTER_WIZARD_VIEW));
	
	m_pCommGridFilter = new CommGridFilter(COMM_GRID_FILTER_WIDGET_CONTACT, true, this);
	this->setFixedSize(m_pCommGridFilter->frameSize());
	ui.setupUi(this);

	//Read from cache, if there is something.
	ReadFromCache();
	if (m_nViewID <= 0)
		m_nViewID = -1;

	if (m_nViewID > 0)
		m_pCommGridFilter->SetCurrentView(m_nViewID, m_rFilterViewData, g_pClientManager->GetPersonID());
	else if (m_rFilterViewData.getRowCount())
		m_pCommGridFilter->SetCurrentView(m_nViewID, m_rFilterViewData, g_pClientManager->GetPersonID());
	else
	{
		m_rFilterViewData.addRow();
		m_pCommGridFilter->SetCurrentView(m_nViewID, m_rFilterViewData, g_pClientManager->GetPersonID());
	}
	
	m_pCommGridFilter->GetViewAndfilterData(m_nViewID, m_rFilterViewData);

	//Set widget result recordset.
	SetFilterDataToResultRecordSet(m_nViewID, m_rFilterViewData);

	//Connect filter changed signal and it's slot.
	connect(m_pCommGridFilter, SIGNAL(FilterValueChanged()), this, SLOT(on_FilterValueChanged()));

	//Set filter completed and emit complete signal by default.
	m_bComplete = true;

	emit completeStateChanged();
}

void GridFilterSelectionWizPage::WriteToCache()
{
	m_recResultRecordSet.clear();
	QByteArray byte = XmlUtil::ConvertRecordSet2ByteArray_Fast(m_rFilterViewData);
	m_recResultRecordSet.setData(0, "VIEW_ID", m_nViewID);
	m_recResultRecordSet.setData(0, "VIEW_DATA_RECORDSET", byte);

	WizardPage::WriteToCache();
}

void GridFilterSelectionWizPage::ReadFromCache()
{
	WizardPage::ReadFromCache();

	if (!m_recResultRecordSet.getRowCount())
	{
		m_recResultRecordSet.addRow();
		return;
	}

	m_nViewID = m_recResultRecordSet.getDataRef(0, "VIEW_ID").toInt();
	QByteArray ar = m_recResultRecordSet.getDataRef(0, "VIEW_DATA_RECORDSET").toByteArray();
	m_rFilterViewData = XmlUtil::ConvertByteArray2RecordSet_Fast(ar);
}

void GridFilterSelectionWizPage::SetFilterDataToResultRecordSet(int nViewID, DbRecordSet &recFilterViewData)
{
	m_recResultRecordSet.setData(0, "VIEW_ID", nViewID);
	QByteArray byte = XmlUtil::ConvertRecordSet2ByteArray_Fast(recFilterViewData);
	m_recResultRecordSet.setData(0, "VIEW_DATA_RECORDSET", byte);
}

void GridFilterSelectionWizPage::on_FilterValueChanged()
{
	m_pCommGridFilter->GetViewAndfilterData(m_nViewID, m_rFilterViewData);

	//Set widget result recordset.
	SetFilterDataToResultRecordSet(m_nViewID, m_rFilterViewData);
}
