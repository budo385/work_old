#include "calendarmultidayentityitem.h"

#include <QGraphicsSceneContextMenuEvent>
#include <QMenu>
#include <QToolTip>
#include <QGraphicsDropShadowEffect>
#include <QMimeData>

#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;				//global access right tester

CalendarMultiDayEntityItem::CalendarMultiDayEntityItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, 
		 QDateTime startDateTime, QDateTime endDateTime, QDateTime correctedStartDateTime, QDateTime correctedEndDateTime, QString strTitle, 
		 QString strLocation, QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, 
		 QString strDescription, CalendarMultiDayGraphicsView *pCalendarMultiDayGraphicsView, int nBcep_Status, int nBCEV_IS_INFORMATION, 
		 bool bIsProposed, QGraphicsItem * parent /*= 0*/, Qt::WindowFlags wFlags /*= 0*/)
	: QGraphicsProxyWidget()
{
	m_nCentID				= nCentID;
	m_nBcepID				= nBcepID;
	m_nBcevID				= nBcevID;
	m_nBCOL_ID				= nBCOL_ID;
	m_nBcep_Status			= nBcep_Status;
	m_nBCEV_IS_INFORMATION	= nBCEV_IS_INFORMATION;
	m_bIsProposed			= bIsProposed;
	m_nHeightFactor			= 1;
	m_nVertOrder			= 0;
	m_isLeftHResizing		= false;
	m_isRightHResizing		= false;
	m_bIsMoving				= false;
	m_startDateTime			= startDateTime;
	m_endDateTime			= endDateTime;
	m_correctedStartDateTime= correctedStartDateTime;
	m_correctedEndDateTime	= correctedEndDateTime;
	m_nEntityID				= nEntityID;
	m_nEnitiyType			= nEntityType;
	m_pCalendarMultiDayGraphicsView	= pCalendarMultiDayGraphicsView;
	m_nHeight = m_pCalendarMultiDayGraphicsView->getItemHeight();

	m_pDisplayWidget = new CalendarMultyDayDisplayWidget(nCentID, nBcepID, nBcevID, nEntityID, nEntityType, startDateTime, endDateTime, strTitle, 
		strLocation, strColor, pixIcon, strOwner, strResource, strContact, strProjects, strDescription, m_pCalendarMultiDayGraphicsView, 
		nBcep_Status, nBCEV_IS_INFORMATION, bIsProposed);
	setWidget(m_pDisplayWidget);

	setMinimumWidth(0);
	setMinimumHeight(m_nHeight);
	setMaximumHeight(m_nHeight);
	setZValue(2);
	setFlag(QGraphicsItem::ItemIsMovable, true);
	setFlag(QGraphicsItem::ItemIsSelectable, true);
	setFlag(QGraphicsItem::ItemIsFocusable, true);
	setAcceptHoverEvents(true);
	setToolTip(strTitle);

	QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect();
	effect->setBlurRadius(1.5);
	effect->setOffset(1.5,1.5);
	setGraphicsEffect(effect);

	if (nBCEV_IS_INFORMATION)
		setOpacity(0.6);

	CreateContextMenuActions(m_lstActions);
}

CalendarMultiDayEntityItem::~CalendarMultiDayEntityItem()
{
	m_pDisplayWidget = NULL;
}

void CalendarMultiDayEntityItem::UpdateMultyDayEntityItem(int nCentID, int nBcepID, int nBcevID, int nBCOL_ID, int nEntityID, int nEntityType, 
		QDateTime startDateTime, QDateTime endDateTime, QDateTime correctedStartDateTime, QDateTime correctedEndDateTime, QString strTitle, 
		QString strLocation, QString strColor, QPixmap pixIcon, QString strOwner, QString strResource, QString strContact, QString strProjects, 
		QString strDescription, int nBcep_Status, int nBCEV_IS_INFORMATION, bool bIsProposed)
{
	m_nCentID				= nCentID;
	m_nBcepID				= nBcepID;
	m_nBcevID				= nBcevID;
	m_nBCOL_ID				= nBCOL_ID;
	m_nBcep_Status			= nBcep_Status;
	m_nBCEV_IS_INFORMATION	= nBCEV_IS_INFORMATION;
	m_bIsProposed			= bIsProposed;
	m_startDateTime			= startDateTime;
	m_endDateTime			= endDateTime;
	m_correctedStartDateTime= correctedStartDateTime;
	m_correctedEndDateTime	= correctedEndDateTime;
	m_nEntityID				= nEntityID;
	m_nEnitiyType			= nEntityType;

	CalendarMultyDayDisplayWidget *old_DisplayWidget = m_pDisplayWidget;
	m_pDisplayWidget = new CalendarMultyDayDisplayWidget(nCentID, nBcepID, nBcevID, nEntityID, nEntityType, startDateTime, endDateTime, strTitle, 
		strLocation, strColor, pixIcon, strOwner, strResource, strContact, strProjects, strDescription, m_pCalendarMultiDayGraphicsView, nBcep_Status, 
		nBCEV_IS_INFORMATION, bIsProposed);
	setWidget(m_pDisplayWidget);
	delete(old_DisplayWidget);
	setMinimumWidth(0);
	setMinimumHeight(m_nHeight);
	setMaximumHeight(m_nHeight);
	setZValue(2);
	setFlag(QGraphicsItem::ItemIsSelectable, true);
	setAcceptHoverEvents(true);

	if (nBCEV_IS_INFORMATION)
		setOpacity(0.6);
	else
		setOpacity(1);
}

void CalendarMultiDayEntityItem::getStartEndDateTime(QDateTime	&startDateTime, QDateTime &endDateTime)
{
	startDateTime	= m_startDateTime;
	endDateTime		= m_endDateTime;
}

void CalendarMultiDayEntityItem::SetSelected(bool bSelected /*= true*/)
{
	m_pDisplayWidget->SetSelected(bSelected);
}

void CalendarMultiDayEntityItem::on_UpdateCalendar()
{
	m_pCalendarMultiDayGraphicsView->CheckForMultipleItemsInRow(this);
	QRect rect = m_pCalendarMultiDayGraphicsView->getTableView()->getItemRectangle(m_nEntityID, m_nEnitiyType, m_correctedStartDateTime, m_correctedEndDateTime);
	QPolygonF r = m_pCalendarMultiDayGraphicsView->mapToScene(rect);
	m_nWidth = rect.width();
	m_pDisplayWidget->resize(m_nWidth, m_nHeight);
	resize(m_nWidth, m_nHeight);
	setPos(rect.topLeft().x(), rect.topLeft().y()+m_pCalendarMultiDayGraphicsView->getHeaderHeight()+(m_nVertOrder)*m_nHeight);
	update();
}

void CalendarMultiDayEntityItem::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
		return;

	const QMimeData *mimeData = event->mimeData();
	QStringList lstData = mimeData->text().split(";");
	if (lstData.value(0) == "entity_item")
		event->accept();
	else
		event->ignore();
}

void CalendarMultiDayEntityItem::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{

}

void CalendarMultiDayEntityItem::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{

}

void CalendarMultiDayEntityItem::dropEvent(QGraphicsSceneDragDropEvent *event)
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
		return;

	const QMimeData *mimeData = event->mimeData();
	QStringList lstData = mimeData->text().split(";");
	int nEntityID = lstData.value(1).toInt();
	int nEntityType = lstData.value(2).toInt();
	int nEntityPersonID = lstData.value(3).toInt();

	emit DropEntityOnItem(this, nEntityID, nEntityType, nEntityPersonID);
}

void CalendarMultiDayEntityItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
	if(g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
	{
		CalendarEntityItem::ResizeType type = isInResizeArea(event->pos());
		if (type == CalendarEntityItem::HORIZ_RESIZE || type == CalendarEntityItem::HORIZ_LEFT_RESIZE)
			setCursor(Qt::SizeHorCursor);
		else
			setCursor(Qt::ArrowCursor);
	}
	QGraphicsProxyWidget::hoverMoveEvent(event);
}

void CalendarMultiDayEntityItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
		return;
	//return QGraphicsProxyWidget::mouseMoveEvent(event);

	if (m_isRightHResizing)
	{
		m_nWidth = event->pos().x();
		resize(m_nWidth, m_nHeight);
		emit ItemResizing(true);
	}
	else if (m_isLeftHResizing)
	{
		setPos(pos().x()+event->pos().x(), pos().y());
		m_nWidth = m_nWidth - event->pos().x();
		resize(m_nWidth, m_nHeight);
		emit ItemResizing(true);
	}
	else //if (m_bIsMoving)
	{
		//QGraphicsProxyWidget::mouseMoveEvent(event);
		//QGraphicsItem::mouseMoveEvent(event);
		//setPos(event->pos());
		//dynamic_cast<CalendarGraphicsView*>(m_pCalendarGraphicsView)->CheckForMultipleItemsInColumn(this);
		m_bIsMoving = true;
		QPointF mousePos = event->scenePos();
		//QPointF scenePos = mapToScene(mousePos);
		//QPointF tablePos = m_pCalendarMultiDayGraphicsView->getTableView()->mapFromScene(scenePos);
		setPos(mousePos);

		//QModelIndex topLeftIndex = dynamic_cast<CalendarHeaderTableItem*>(m_pCalendarMultiDayGraphicsView->getTableView())->Table()->indexAt(tablePos.toPoint());
		//QItemSelectionModel *selModel = dynamic_cast<CalendarHeaderTableItem*>(m_pCalendarMultiDayGraphicsView->getTableView())->Table()->selectionModel();
		//QItemSelection selection(topLeftIndex, topLeftIndex);
		//selModel->clearSelection();
		//selModel->select(selection, QItemSelectionModel::Select);
		//update();
		//on_UpdateCalendar();
	}
}

void CalendarMultiDayEntityItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
			return;

		CalendarEntityItem::ResizeType type = isInResizeArea(event->pos());
		if (type == CalendarEntityItem::HORIZ_RESIZE)
		{
			m_isRightHResizing = true;
		}
		else if (type == CalendarEntityItem::HORIZ_LEFT_RESIZE)
		{
			m_isLeftHResizing = true;
		}
		else
		{
			//m_bIsMoving = true;
		}
	}
	else
		QGraphicsProxyWidget::mousePressEvent(event);
}

void CalendarMultiDayEntityItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
/*	if ((event->button() == Qt::LeftButton) && isSelected())
	{
		setSelected(false);
		m_pDisplayWidget->SetSelected(false);
	}
	else if((event->button() == Qt::LeftButton) && !isSelected())
	{
		setSelected(true);
		m_pDisplayWidget->SetSelected(true);
	}
	
return QGraphicsProxyWidget::mouseReleaseEvent(event);
*/
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
		return;

	if (event->button() == Qt::LeftButton && (m_isLeftHResizing || m_isRightHResizing))
	{
		emit ItemResized(this, m_isLeftHResizing);
		m_isLeftHResizing  = false;
		m_isRightHResizing = false;
		return;
	}
	else if (event->button() == Qt::LeftButton && m_bIsMoving)
	{
		//emit ItemResized(this, m_isLeftHResizing);
		QPointF mousePos = event->pos();
		QPointF scenePos = mapToScene(mousePos);
		QPointF tablePos = m_pCalendarMultiDayGraphicsView->getTableView()->mapFromScene(scenePos);
		setPos(scenePos);

		m_bIsMoving = false;
		emit ItemMoved(this);
		return;
	}
	/*else if ((event->button() == Qt::LeftButton) && isSelected())
	{
		setSelected(false);
		m_pDisplayWidget->SetSelected(false);
		QGraphicsProxyWidget::mousePressEvent(event);
	}
	else if((event->button() == Qt::LeftButton) && !isSelected())
	{
		setSelected(true);
		m_pDisplayWidget->SetSelected(true);
		QGraphicsProxyWidget::mousePressEvent(event);
	}*/
	else
	{
		QGraphicsProxyWidget::mouseReleaseEvent(event);
		on_UpdateCalendar();
	}

}

void CalendarMultiDayEntityItem::On_CnxtModify()
{
	emit ItemModify(this);
}

void CalendarMultiDayEntityItem::On_CnxtDelete()
{
	emit ItemDelete(this);
}

void CalendarMultiDayEntityItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
		return;

	if(m_lstActions.size()==0) 
	{
		event->ignore();	
		return;
	}

	QMenu CnxtMenu;

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));

	CnxtMenu.exec(event->screenPos());
}

void CalendarMultiDayEntityItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent * event)
{
	if(!g_AccessRight->TestAR(ENTER_MODIFY_CALENDAR))
		return;

	On_CnxtModify();
}

bool CalendarMultiDayEntityItem::viewportEvent(QEvent *event)
{
	if (event->type() == QEvent::ToolTip) 
	{
		QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
		QPoint pos = helpEvent->globalPos();
		QPoint pos1 = m_pCalendarMultiDayGraphicsView->mapToGlobal(pos);
		QToolTip::showText(pos, m_strDisplayText);

		return true;
	} 
	else
		return false;
}

CalendarEntityItem::ResizeType CalendarMultiDayEntityItem::isInResizeArea(const QPointF &pos)
{
	int nWidth = this->widget()->width();
	int nHeight = this->widget()->height();

	if (((nWidth - 5) < pos.x()) && (pos.x() < nWidth))
	{
		return CalendarEntityItem::HORIZ_RESIZE;
	}
	else if ((pos.x() < 5) && (pos.x() > 0))
	{
		return CalendarEntityItem::HORIZ_LEFT_RESIZE;
	}
	else
		return CalendarEntityItem::NO_RESIZE;
}

void CalendarMultiDayEntityItem::CreateContextMenuActions(QList<QAction*> &lstActions)
{
	QAction* pAction = new QAction(tr("Modify"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtModify()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Delete"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(On_CnxtDelete()));
	lstActions.append(pAction);


}
