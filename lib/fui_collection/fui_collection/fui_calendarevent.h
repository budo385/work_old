#ifndef FUI_CALENDAREVENT_H
#define FUI_CALENDAREVENT_H

#include "fuibase.h"
#include "gui_core/gui_core/styledpushbutton.h"
#include "generatedfiles/ui_fui_calendarevent.h"
#include "calendarevent_wizard.h"
#include "calendarevent_detailpane.h"

class SuperTabBar;
class FUI_CalendarEvent : public FuiBase
{
	Q_OBJECT

public:
	FUI_CalendarEvent(QWidget *parent = 0);
	~FUI_CalendarEvent();

	void SetDefaults(bool bGoWizardMode,DbRecordSet *rowDefault=NULL,DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL, DbRecordSet *lstResources=NULL,bool bScheduleTask=false, bool bIsTemplate=false, int nTemplateID=-1, bool bAS_Customer=false);
	void AssignEntities(int nPartID,DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL, DbRecordSet *lstResources=NULL);
	void SetOwner(int nPersonID);
	void GoInviteAcceptMode(int nCalEventID,int nPartID,int nInviteID,int nContactId_ConfirmedBy, int nPersonID_ConfirmedBy, int nCalendarOwnerID, bool bNotifyOwner=false);
	void ShowPart(int nCalEvPartID, int nCalEvOptionID=-1);
	bool on_cmdOK();
	void on_cmdCancel();
	bool on_cmdEdit();
	bool on_cmdDelete();
	void on_cmdCopy();
	void SetMode(int nMode);

private slots:
	void OnSaveClose();
	void OnWizardChkBoxStateChanged(int);
	void OnAddPart();
	void AddPart(bool bCopyFromCurrent=true);
	void DeletePart(int nTabindex);
	void OnDeleteCurrentPart();
	void OnRecurrence();
	void OnTask();
	void OnTabCloseRequested(int);
	void OnTabCurrentChanged(int);
	void OnTabContextMenu( const QPoint &);
	void OnTabMoved(int,int);
	void OnTemplateChanged(int);
	void OnTabClicked();
	void OnInviteDataChanged();
	void OnDateTimeFromChanged(const QDateTime &);
	void OnDateTimeToChanged(const QDateTime &);
	void OnDateFromFocusOut();
	void OnDateToFocusOut();
	void OnTitleChanged();
	
	void On_WizardStep1();
	void On_WizardStep2();
	void On_WizardStep3();
	void On_WizardStep4();
	void On_WizardStep5();
	void On_WizardStep6();
	void On_WizardStep7();
	void On_WizardStep8();
	void On_WizardStep9();
	void on_ckbIsTemplate_stateChanged(int);

protected:
	virtual void	closeEvent(QCloseEvent *event);
	void DataPrepareForInsert(Status &err);
	void DataPrepareForCopy(Status &err);
	void DataClear();
	void DataDefine();
	void DataLock(Status &err);
	void DataDelete(Status &err);
	void DataUnlock(Status &err);
	void DataCheckBeforeWrite(Status &err);
	void DataWrite(Status &err);
	void DataRead(Status &err,int nRecordID);
	void prepareCacheRecord(DbRecordSet* recNewData); //called from notifyCache_AfterWrite to prepare data that will be merge onto existing data in the cache


private:
	void RefreshDisplay();

	void PrepareButtonBar(bool bWizardMode);
	void GoWizardMode(bool bWizardMode);
	void BuildYellowToolBar();
	void ResetWizardButtons();
	void InitWizard();
	void RebuildPartsTabBar();
	void RefreshParts(DbRecordSet &lstParts);
	bool SendInvitations();
	bool RejectInvite();
	bool AcceptInvite();
	void LoadFromTemplate(int nTemplate);

	void			GetAllDataFromGUI();
	void			GetAllPartData(DbRecordSet &lstParts, int index=-1);
	void			GetCurrentPartAndOption(DbRecordSet &rowPart,DbRecordSet &rowOption);
	//void			SetToCurrentPartAndOption(DbRecordSet &rowPart,DbRecordSet &rowOption);
	void			SetPartDataFromPartRow(DbRecordSet &rowPart,CalendarEvent_DetailPane *pane);
	DbRecordSet		CopyPartRow(DbRecordSet &rowPart);
	void			CleanPartListAfterCopy(DbRecordSet &lstParts);
	void			SetDataToWizard();
	void			GetDataFromWizard();
	void			SetAfterTemplate_DateTimes(QDateTime dateFrom,QDateTime  dateTo);
	void			SetAfterTemplate_NMRX(DbRecordSet &lstDataCalEventPartPrev);


	Ui::FUI_CalendarEventClass ui;

	//DbRecordSet m_lstDataCalEvent; ->m_lstData;
	DbRecordSet m_lstDataCalEventPart;
	DbRecordSet m_lstDataCalEventPartTaskData;
	DbRecordSet m_lstTaskCache;
	DbRecordSet m_lstDataCalEventPartCache;

	bool			m_bWizardMode;
	bool			m_bInviteAcceptMode;
	QCheckBox		*m_btnWizardMode;
	QPushButton		*m_btnSave;
	bool			m_bWizardInitialized;

	int m_nInviteMode_InviteID;
	int m_nInviteMode_nPartID;
	int m_nInviteMode_nContactId_ConfirmedBy;
	int m_nInviteMode_nPersonID_ConfirmedBy;
	int m_nInviteMode_nOwnerID;
	bool m_bInviteMode_bNotifyOwner;
	bool m_bSkipSetOwnerOnInsert;

	CalendarEvent_Wizard *m_pWizard;

	//yellow toolbar:
	StyledPushButton *m_p1;
	StyledPushButton *m_p2;
	StyledPushButton *m_p3;
	StyledPushButton *m_p4;
	StyledPushButton *m_p5;
	StyledPushButton *m_p6;
	StyledPushButton *m_p7;
	StyledPushButton *m_p8;
	StyledPushButton *m_p9;

	GuiFieldManager *m_GuiFieldManagerMain;

	QPushButton								*m_pRecurrence;
	QPushButton								*m_pTask;
	StyledPushButton						*m_pAddPart;
	SuperTabBar								*m_pTab;
	QHash <int,CalendarEvent_DetailPane*>	m_hshParts;
};


class SuperTabBar : public QTabBar
{
	Q_OBJECT

public:
	SuperTabBar ( QWidget * parent = 0 ):QTabBar(parent){};

signals:
	void TabClicked();
protected:
	virtual void mouseReleaseEvent ( QMouseEvent * event );
};

#endif // FUI_CALENDAREVENT_H
