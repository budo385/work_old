#ifndef FUI_CONTACTTYPE_H
#define FUI_CONTACTTYPE_H

#include <QWidget>
#include "generatedfiles/ui_fui_contacttype.h"
#include "fuibase.h"


class FUI_ContactType : public FuiBase
{
    Q_OBJECT

public:
    FUI_ContactType(QWidget *parent = 0);
    ~FUI_ContactType();
	
	QString GetFUIName();
	void on_cmdInsert();
	bool on_cmdEdit();
	bool on_cmdDelete();
	bool on_cmdOK();
	void on_selectionChange(int nEntityRecordID); //for external to go into edit mode at once...

protected:
	void DataWrite(Status &err);
	void DataRead(Status &err,int nRecordID);
	void DataLock(Status &err);
	void DataCheckBeforeWrite(Status &err);

private:
    Ui::FUI_ContactTypeClass ui;

	
	void RefreshDisplay();
	void SetMode(int nMode);
};

#endif // FUI_CONTACTTYPE_H
