#ifndef FUI_GLOBAL_OBJECTS_CREATE
#define FUI_GLOBAL_OBJECTS_CREATE

//-----------------------------------------------------------------
//				FUI/GUI GLOBAL OBJECT CREATOR (GUI APPLICATION FRAMEWORK)
//-----------------------------------------------------------------


#include "fuimanager.h"
#include "communicationmanager.h"

CommunicationManager			g_CommManager;				//global comm manager
FuiManager						g_objFuiManager;			//global FUI manager:


#endif 