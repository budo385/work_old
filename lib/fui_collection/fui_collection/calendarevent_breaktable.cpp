#include "calendarevent_breaktable.h"
#include "gui_core/gui_core/datetimeeditex.h"
#include "gui_core/gui_core/thememanager.h"
#define DATETIME_INSIDETABLE_STYLE "QDateTimeEdit {color: black;}"

CalendarEvent_BreakTable::CalendarEvent_BreakTable(QWidget *parent)
	: UniversalTableWidgetEx(parent),m_toolbar(NULL)
{

}


void CalendarEvent_BreakTable::Initialize(DbRecordSet *plstData,toolbar_edit *pToolBar)
{
	BuildToolBar(pToolBar);

	if (plstData->getColumnIdx("DURATION")==-1)
		plstData->addColumn(QVariant::Int,"DURATION");
	
	//redefine table look:
	DbRecordSet columns;
	AddColumnToSetup(columns,"BCBL_NAME",tr("Description"),300);
	AddColumnToSetup(columns,"BCBL_FROM",tr("From"),140,true,"",UniversalTableWidgetEx::COL_TYPE_CUSTOM_WIDGET);
	AddColumnToSetup(columns,"BCBL_TO",tr("To"),140,true,"",UniversalTableWidgetEx::COL_TYPE_CUSTOM_WIDGET);
	AddColumnToSetup(columns,"DURATION",tr("Duration (min)"),120,false);

	UniversalTableWidgetEx::Initialize(plstData,&columns);

	QList<QAction*> lstActions;
	CreateDefaultContextMenuActions(lstActions);
	SetContextMenuActions(lstActions);


	setObjectName("TABLE_BREAK");
	setStyleSheet(ThemeManager::GetTableViewBkg("TABLE_BREAK")+" QToolButton {background:transparent;}");


	//connect(this,SIGNAL(SignalRowsDeleted()),this,SLOT(OnDeleted()));
	//connect(this,SIGNAL(SignalRowInserted(int)),this,SLOT(OnRowInserted(int)));
}



void CalendarEvent_BreakTable::BuildToolBar(toolbar_edit * pToolBar)
{
	m_toolbar=pToolBar;
	pToolBar->GetBtnAdd()->setText(tr("Add New Break"));
	pToolBar->GetBtnAdd()->setIcon(QIcon(":CalendarEvent_Break16.png"));
	connect(pToolBar->GetBtnAdd(), SIGNAL(clicked()), this, SLOT(InsertRow()));
	pToolBar->GetBtnSelect()->setVisible(false);
	pToolBar->GetBtnModify()->setVisible(false);
	connect(pToolBar->GetBtnClear(), SIGNAL(clicked()), this, SLOT(DeleteSelection()));
}

void CalendarEvent_BreakTable::SetEditMode(bool bEdit)
{
	UniversalTableWidgetEx::SetEditMode(bEdit);
	if(m_toolbar)m_toolbar->setEnabled(bEdit);
}



void CalendarEvent_BreakTable::Data2CustomWidget(int nRow, int nCol)
{
	if (nCol==1)
	{
		DateTimeEditEx *pFrom = dynamic_cast<DateTimeEditEx *>(cellWidget(nRow,nCol));
		if (!pFrom)
		{
			pFrom= new DateTimeEditEx;
			pFrom->useAsDateTimeEdit(true);
			pFrom->setStyleSheet(DATETIME_INSIDETABLE_STYLE);
			connect(pFrom,SIGNAL(dateTimeChanged(const QDateTime&)),this,SLOT(RecalcDurations()));
			connect(pFrom,SIGNAL(focusOutSignal()),this,SLOT(OnFromFocusOutSignal()));  //connect on change signals:
			pFrom->setEnabled(m_bEdit);
			setCellWidget(nRow,nCol,pFrom);
			pFrom->setProperty("from_row",nRow); //to identify sender
			
		}
		pFrom->setDateTime(m_plstData->getDataRef(nRow,"BCBL_FROM").toDateTime());
	}
	else if (nCol==2) 
	{
		DateTimeEditEx *pTo = dynamic_cast<DateTimeEditEx *>(cellWidget(nRow,nCol));
		if (!pTo)
		{
			pTo= new DateTimeEditEx;
			pTo->useAsDateTimeEdit(true);
			pTo->setStyleSheet(DATETIME_INSIDETABLE_STYLE);
			pTo->setEnabled(m_bEdit);
			connect(pTo,SIGNAL(dateTimeChanged(const QDateTime&)),this,SLOT(RecalcDurations()));
			setCellWidget(nRow,nCol,pTo);
		}
		pTo->setDateTime(m_plstData->getDataRef(nRow,"BCBL_TO").toDateTime());
	}
}
void CalendarEvent_BreakTable::OnFromFocusOutSignal()
{
	DateTimeEditEx *pFrom = dynamic_cast<DateTimeEditEx *>(sender());
	if (pFrom)
	{
		int nRow=pFrom->property("from_row").toInt();
		if (nRow>=0 && nRow <m_plstData->getRowCount())
		{
			DateTimeEditEx *pTo = dynamic_cast<DateTimeEditEx *>(cellWidget(nRow,2));
			if (pTo)
			{
				if (!pTo->dateTime().isValid()) //if TO is invalid then TO=FROM+15
				{
					QDateTime pDateFrom=pFrom->dateTime().addSecs(60*15);
					pTo->setDateTime(pDateFrom);
				}
				else
				{
					QDateTime pDateFrom=pFrom->dateTime();
					QDateTime pDateTo=pTo->dateTime();
					if (pDateTo<=pDateFrom) //if TO < FROM then TO=FROM+15
					{
						QDateTime pDateFrom=pFrom->dateTime().addSecs(60*15);
						pTo->setDateTime(pDateFrom);
					}
				}
			}
		}
	}

}

void CalendarEvent_BreakTable::CustomWidget2Data(int nRow, int nCol)
{
	if (nCol==1)
	{
		DateTimeEditEx *pFrom = dynamic_cast<DateTimeEditEx *>(cellWidget(nRow,nCol));
		if (pFrom)
		{
			m_plstData->setData(nRow,"BCBL_FROM",pFrom->dateTime());
		}
	}
	else if (nCol==2) 
	{
		DateTimeEditEx *pTo = dynamic_cast<DateTimeEditEx *>(cellWidget(nRow,nCol));
		if (pTo)
		{
			m_plstData->setData(nRow,"BCBL_TO",pTo->dateTime());
		}
	}
}

void CalendarEvent_BreakTable::RecalcDurations()
{
	Table2Data();

	if (m_plstData->getColumnIdx("DURATION")==-1)
		m_plstData->addColumn(QVariant::Int,"DURATION");

	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_plstData->getDataRef(i,"BCBL_FROM").isNull() || m_plstData->getDataRef(i,"BCBL_TO").isNull())
		{
			m_plstData->setData(i,"DURATION",QVariant(QVariant::Int));
			continue;
		}


		QDateTime datFrom=m_plstData->getDataRef(i,"BCBL_FROM").toDateTime();
		QDateTime datTo=m_plstData->getDataRef(i,"BCBL_TO").toDateTime();
		if (datTo<datFrom)
		{
			m_plstData->setData(i,"DURATION",QVariant(QVariant::Int));
			continue;
		}

		int nDuration=datFrom.secsTo(datTo)/60;
		m_plstData->setData(i,"DURATION",nDuration);
		
	}
	UniversalTableWidgetEx::RefreshDisplay();
}

void CalendarEvent_BreakTable::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	RecalcDurations();
	UniversalTableWidgetEx::RefreshDisplay(nRow,bApplyLastSortModel);
}

void CalendarEvent_BreakTable::GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip)
{
	//set icon:
	RowIcon.addFile(":CalendarEvent_Break16.png");
}

