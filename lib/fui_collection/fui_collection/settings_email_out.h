#ifndef SETTINGS_Email_OUT_H
#define SETTINGS_Email_OUT_H

#include <QWidget>
#include "ui_settings_email_out.h"
#include "settingsbase.h"

#define EML_TYPE_POP3 1
#define EML_TYPE_IMAP 2

#define EML_SECURITY_NONE	  1
#define EML_SECURITY_STARTTLS 2
#define EML_SECURITY_SSLTLS	  3

//default ports
#define EML_DEFAULT_PORT_POP3	110
#define EML_DEFAULT_PORT_IMAP	143
#define EML_DEFAULT_PORT_POP3_SSL	995
#define EML_DEFAULT_PORT_IMAP_SSL	993

class EmailProvider {
public:
	EmailProvider(QString strName, QString strServer, int nType, int nPort, int nSecurity) : 
	  m_strName(strName),
	  m_strServer(strServer),
	  m_nType(nType),
	  m_nPort(nPort),
	  m_nSecurity(nSecurity)
	{};

	QString m_strName;
	QString m_strServer;
	int m_nType;
	int m_nPort;
	int m_nSecurity;
};

class Settings_Email_Out : public SettingsBase
{
    Q_OBJECT

public:
    Settings_Email_Out(int nOptionsSetID, bool bOptionsSwitch, OptionsAndSettingsManager *pOptionsAndSettingsManager = NULL, QWidget *parent = 0);
    ~Settings_Email_Out();

	DbRecordSet GetChangedValuesRecordSet();
	bool TestConnection(bool bPOP3, const QString &strHost, int nPort, int nSecurity, const QString &strUser, const QString &strPass, QString &strError);

	QList<EmailProvider> m_lstProviders;

private:
	void SetupCurrentPage();
	void UpdateDataFromPage();
	void UpdateWidgetStates();

private:
	int m_bChanged;
	int m_nCurrentPage;
	DbRecordSet m_recAccounts;
    Ui::Settings_EmailOutClass ui;

private slots:
	void on_previous_toolButton_clicked();
	void on_next_toolButton_clicked();
	void on_delete_toolButton_clicked();
	void on_add_toolButton_clicked();
	void on_cboProviders_currentIndexChanged(int index);
	void on_cboSecurity_currentIndexChanged(int index);
	void on_radioPOP3_toggled(bool);
	void on_txtPop3Port_textChanged( const QString & );
	void on_txtEmail_textChanged( const QString & );
	void on_btnTestConnection_clicked();
	void on_chkIsActive_clicked(bool bChecked);
};

#endif // SETTINGS_Email_OUT_H
