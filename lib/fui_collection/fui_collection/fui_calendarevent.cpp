#include "fui_calendarevent.h"
#include "gui_core/gui_core/macros.h"
#include "gui_core/gui_core/thememanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMenu>
#include "gui_core/gui_core/macros.h"
#include "fui_collection/fui_collection/calendarhelper.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_client/bus_client/emailhelper.h"
#include "bus_core/bus_core/calendarhelpercore.h"
#include "bus_core/bus_core/nmrxmanager.h"

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "gui_core/gui_core/gui_helper.h"
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

#define  STYLE_YELLOW_TOOLBAR_STEPS	  "QLabel {font-weight:900;font-style:italic;font-family:Arial; font-size:14pt; color:black}"
#define  STYLE_YELLOW_TOOLBAR_BUTTON  "<html><body style=\" font-family:Arial; text-decoration:none;\">\
										<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:%2pt; font-weight:800; font-style:italic; color:black;\">%1</span></p>\
										</body></html>"

#define PART_INDEX_OFFSET 2


FUI_CalendarEvent::FUI_CalendarEvent(QWidget *parent)
	: FuiBase(parent),m_btnWizardMode(NULL),m_bWizardMode(false),m_bWizardInitialized(false),m_pWizard(NULL),m_pTab(NULL),m_bInviteAcceptMode(false),m_bSkipSetOwnerOnInsert(false)
{
	ui.setupUi(this);
	InitFui(ENTITY_CALENDAR_EVENT,BUS_CAL_EVENT,NULL,ui.btnButtonBar); //FUI base
	EnableAccessRightCheck();
	EnableWindowCloseOnCancelOrOK();
	ui.btnButtonBar->RegisterFUI(this);
	SetFUIName(tr("Calendar Event"));

	ui.labelIcon->setPixmap(QPixmap(":CalendarEvent48.png"));
	ui.labelTitle->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	ui.labelTitle->setAlignment(Qt::AlignLeft);
	ui.labelName->setStyleSheet("QLabel {color:white}");
	ui.labelFrom->setStyleSheet("QLabel {color:white}");
	ui.labelTo->setStyleSheet("QLabel {color:white}");

	ui.frameBkg->setStyleSheet("QFrame#frameBkg "+ThemeManager::GetSidebarChapter_Bkg());

	ui.dateFrom->useAsDateTimeEdit(true);
	ui.dateTo->useAsDateTimeEdit(true);
	connect(ui.dateFrom,SIGNAL(focusOutSignal()),this,SLOT(OnDateFromFocusOut()));
	connect(ui.dateTo,SIGNAL(focusOutSignal()),this,SLOT(OnDateToFocusOut()));


	m_GuiFieldManagerMain =new GuiFieldManager(&m_lstData);
	m_GuiFieldManagerMain->RegisterField("BCEV_DAT_CREATED",ui.dateCreated);
	m_GuiFieldManagerMain->RegisterField("BCEV_DAT_LAST_MODIFIED",ui.dateModified);
	m_GuiFieldManagerMain->RegisterField("BCEV_TITLE",ui.txtTitle);
	m_GuiFieldManagerMain->RegisterField("BCEV_FROM",ui.dateFrom);
	m_GuiFieldManagerMain->RegisterField("BCEV_TO",ui.dateTo);
	m_GuiFieldManagerMain->RegisterField("BCEV_TEMPLATE_FLAG",ui.ckbIsTemplate);
	m_GuiFieldManagerMain->RegisterField("BCEV_IS_PRIVATE",ui.ckbPrivate);
	m_GuiFieldManagerMain->RegisterField("BCEV_IS_INFORMATION",ui.ckbInfoOnly);

	
	connect(ui.txtTitle,SIGNAL(editingFinished()),this,SLOT(OnTitleChanged()));


	//------------------tasks:
	ui.frameTask->SetDefaultTaskType(GlobalConstants::TASK_TYPE_SCHEDULED_CALENDAR);
	//ui.frameTask->m_pRecMain = &m_lstDataCalEventPartTaskData;
	//ui.frameTask->SetEnabled(true);

	setWindowTitle(QString("SOKRATES")+QChar(174)+QString(" Calendar"));

	PrepareButtonBar(false);
	RebuildPartsTabBar();
	AddPart(false);

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__NEW_ENTRIES))
	{
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_INSERT)->setEnabled(false);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_DELETE)->setEnabled(false);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_COPY)->setEnabled(false);
	}

	SetMode(FuiBase::MODE_EMPTY);
}

FUI_CalendarEvent::~FUI_CalendarEvent()
{


	
}
void FUI_CalendarEvent::RefreshDisplay()
{
	
	m_GuiFieldManagerMain->RefreshDisplay();
	ui.frameTask->SetGUIFromData();

	//rebuild tabs:
	RefreshParts(m_lstDataCalEventPart);

	//refresh all parts:
	QHashIterator <int,CalendarEvent_DetailPane*> i(m_hshParts);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->RefreshDisplay();
	}
	
}

void FUI_CalendarEvent::SetMode(int nMode)
{

	bool bEditMode=false;
	if(nMode==MODE_READ || nMode==MODE_EMPTY)
	{
		bEditMode=false;
	}
	else
	{
		bEditMode=true;
	}

	m_pAddPart->setEnabled(bEditMode);
	m_btnWizardMode->setEnabled(bEditMode);
	m_GuiFieldManagerMain->SetEditMode(bEditMode);
	ui.frameTask->SetEnabled(bEditMode);
	ui.dateModified->setEnabled(false);

	QHashIterator <int,CalendarEvent_DetailPane*> i(m_hshParts);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->SetEditMode(bEditMode);
	}
	FuiBase::SetMode(nMode);
	RefreshDisplay();

	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__NEW_ENTRIES))
	{
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_INSERT)->setEnabled(false);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_DELETE)->setEnabled(false);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_COPY)->setEnabled(false);
	}
}
bool FUI_CalendarEvent::on_cmdEdit()
{
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__NEW_ENTRIES))
	{
		return false;
	}
	return FuiBase::on_cmdEdit();
}
bool FUI_CalendarEvent::on_cmdDelete()
{
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__NEW_ENTRIES))
	{
		return false;
	}
	return FuiBase::on_cmdDelete();
}
void FUI_CalendarEvent::on_cmdCopy()
{
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__NEW_ENTRIES))
	{
		return;
	}
	FuiBase::on_cmdCopy();
}

bool FUI_CalendarEvent::on_cmdOK()
{
	_DUMP(m_lstDataCalEventPart);
	_DUMP(m_lstDataCalEventPartTaskData);
	_DUMP(m_lstTaskCache);
	_DUMP(m_lstDataCalEventPartCache);

	if (m_bInviteAcceptMode)
	{
		if(RejectInvite())
		{
			close();
		}
		return true;
	}

	if (m_bWizardMode)
	{
		FuiBase::on_cmdCancel();
		return true;
	}
	else
	{
		m_bEnableWindowCloseOnCancelOrOK=false;
		bool bOK=FuiBase::on_cmdOK();
		m_bEnableWindowCloseOnCancelOrOK=true;

		if (bOK)
			if (m_lstData.getDataRef(0,"BCEV_TEMPLATE_FLAG").toInt()==0) //do not send invite if template
			{
				if(!SendInvitations())
					on_cmdEdit();
				else
					close();
			}
			else
				close();

		return bOK;
	}
}

void FUI_CalendarEvent::on_cmdCancel()
{
	if (m_bInviteAcceptMode)
	{
		if(AcceptInvite())
		{
			close();
		}
		return;
	}

	if (!m_bWizardMode)
	{
		FuiBase::on_cmdCancel();
		return;
	}
	else
	{
		int nCurrentIndex=m_pWizard->currentIndex();

		if (nCurrentIndex==CalendarEvent_Wizard::INDEX_STEP_PROJECTS)
			nCurrentIndex=CalendarEvent_Wizard::INDEX_STEP_RESOURCES;
		else if (nCurrentIndex==CalendarEvent_Wizard::INDEX_STEP_RESOURCES)
			nCurrentIndex=CalendarEvent_Wizard::INDEX_STEP_BREAKS;
		else
		{
			nCurrentIndex++;
			//skip 4 and 7
			if (nCurrentIndex == CalendarEvent_Wizard::INDEX_STEP_REMINDER || nCurrentIndex == CalendarEvent_Wizard::INDEX_STEP_RECURRENCE)
				nCurrentIndex++;
		}

		switch(nCurrentIndex)
		{
		case CalendarEvent_Wizard::INDEX_STEP_REMINDER:
			On_WizardStep2();
			break;
		case CalendarEvent_Wizard::INDEX_STEP_CONTACTS:
			On_WizardStep3();
			break;
		case CalendarEvent_Wizard::INDEX_STEP_PROJECTS:
			On_WizardStep4();
		    break;
		case CalendarEvent_Wizard::INDEX_STEP_RESOURCES:
			On_WizardStep5();
		    break;
		case CalendarEvent_Wizard::INDEX_STEP_BREAKS:
			On_WizardStep6();
			break;
		case CalendarEvent_Wizard::INDEX_STEP_RECURRENCE:
			On_WizardStep7();
			break;
		case CalendarEvent_Wizard::INDEX_STEP_TASK:
			On_WizardStep8();
		    break;
		default:
			GoWizardMode(false);
		    break;
		}
	}

}


void FUI_CalendarEvent::GoWizardMode(bool bWizardMode)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	m_bWizardMode=bWizardMode;
	PrepareButtonBar(bWizardMode);
	if (bWizardMode)
	{
		SetDataToWizard();
		setWindowTitle(QString("SOKRATES")+QChar(174)+QString(" Calendar Wizard"));
	}
	else
	{
		GetDataFromWizard();
		setWindowTitle(QString("SOKRATES")+QChar(174)+QString(" Calendar"));
	}
	ui.stackedWidgetMain->setCurrentIndex((int)!bWizardMode);
	QApplication::restoreOverrideCursor();
}

void FUI_CalendarEvent::PrepareButtonBar(bool bWizardMode)
{

	if (m_btnWizardMode==NULL) //first time init:
	{
		m_btnWizardMode= new QCheckBox(tr("Show Wizard"),this);
		QSize size(140,30);
		m_btnWizardMode->setMaximumSize(size);
		//m_btnWizardMode->setMinimumSize(size);
		m_btnWizardMode->setIconSize(QSize(24,24));
		m_btnWizardMode->setIcon(QIcon(":EntryAssistent24.png"));
		ui.btnButtonBar->GetButtonLayout()->insertSpacing(0,20);
		ui.btnButtonBar->GetButtonLayout()->insertWidget(0,m_btnWizardMode);
		m_btnWizardMode->setToolTip(tr("Wizard Mode On/Off"));

		connect(m_btnWizardMode,SIGNAL(stateChanged(int)),SLOT(OnWizardChkBoxStateChanged(int)));

		//add second button:
		m_btnSave= new QPushButton(tr("Store && Close"),this);
		QFont font=m_btnSave->font();
		font.setPointSize(10);
		m_btnSave->setFont(font);
		//QSize size(130,27);
		m_btnSave->setFixedSize(150,27);
		QSizePolicy policy(QSizePolicy::Preferred,QSizePolicy::Fixed);
		m_btnSave->setSizePolicy(policy);
		ui.btnButtonBar->GetButtonLayout()->insertWidget(7,m_btnSave);
		connect(m_btnSave,SIGNAL(clicked()),this,SLOT(OnSaveClose()));

		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_INSERT)->setVisible(false);
	}

	if (bWizardMode)
	{
		InitWizard();

		m_btnWizardMode->setText(tr("Wizard Mode"));
		if (m_btnWizardMode->checkState()!=Qt::Checked)
		{
			m_btnWizardMode->blockSignals(true);
			m_btnWizardMode->setChecked(true);
			m_btnWizardMode->blockSignals(false);
		}

		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_EDIT)->setVisible(false);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_DELETE)->setVisible(false);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_COPY)->setVisible(false);
		m_btnSave->setVisible(true);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_OK)->setText(tr("Cancel"));
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_CANCEL)->setText(tr("Continue..."));

		GUI_Helper::SetStyledButtonColorBkg(ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_CANCEL), GUI_Helper::BTN_COLOR_GREEN_1);

		On_WizardStep1();
	}
	else
	{
		m_btnWizardMode->setText(tr("Show Wizard"));
		if (m_btnWizardMode->checkState()==Qt::Checked)
		{
			m_btnWizardMode->blockSignals(true);
			m_btnWizardMode->setChecked(false);
			m_btnWizardMode->blockSignals(false);
		}

		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_EDIT)->setVisible(true);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_DELETE)->setVisible(true);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_COPY)->setVisible(true);
		m_btnSave->setVisible(false);
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_OK)->setText(tr("OK"));
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_CANCEL)->setText(tr("Cancel"));
		ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_CANCEL)->setStyleSheet("");
	}

}

void FUI_CalendarEvent::InitWizard()
{
	if (m_bWizardInitialized)
		 return;

	BuildYellowToolBar();

	Q_ASSERT(m_pWizard==NULL);

	m_pWizard = new CalendarEvent_Wizard(this);
	connect(m_pWizard,SIGNAL(SignalTemplateChanged(int)),this,SLOT(OnTemplateChanged(int)));
	m_pWizard->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
	dynamic_cast<QVBoxLayout*>(ui.stackedWidgetMain->widget(0)->layout())-> addWidget(m_pWizard);

	m_bWizardInitialized=true;
}

//build up buttons dynamically and set it to frame:
void FUI_CalendarEvent::BuildYellowToolBar()
{
	ui.frameYellowToolBar->setStyleSheet("QFrame {background-color: rgb(255,255,165)}");
	
	QLabel *labSteps = new QLabel(tr("STEPS:"),this);
	labSteps->setStyleSheet(STYLE_YELLOW_TOOLBAR_STEPS);
	labSteps->setAlignment(Qt::AlignLeft);

	labSteps->setMaximumHeight(25);

	QSize buttonSize(170, 25);
	m_p1	=	new StyledPushButton(this,":CalendarEvent_1i.png",":CalendarEvent_1a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Basic")).arg("10"));
	//m_p2	=	new StyledPushButton(this,":CalendarEvent_4i.png",":CalendarEvent_4a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Reminder")).arg("10"));
	m_p3	=	new StyledPushButton(this,":CalendarEvent_2i.png",":CalendarEvent_2a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Contacts","nabijem")).arg("10"));
	m_p4	=	new StyledPushButton(this,":CalendarEvent_3i.png",":CalendarEvent_3a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Projects")).arg("10"));
	m_p5	=	new StyledPushButton(this,":CalendarEvent_4i.png",":CalendarEvent_4a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Resources")).arg("10"));
	m_p6	=	new StyledPushButton(this,":CalendarEvent_5i.png",":CalendarEvent_5a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Breaks")).arg("10"));
	//m_p7	=	new StyledPushButton(this,":CalendarEvent_7i.png",":CalendarEvent_7a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Recurrence")).arg("10"));
	m_p8	=	new StyledPushButton(this,":CalendarEvent_6i.png",":CalendarEvent_6a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Task")).arg("10"));
	m_p9	=	new StyledPushButton(this,":CalendarEvent_7i.png",":CalendarEvent_7a.png","",QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Multipart")).arg("10"));
	m_p1			->setMaximumSize(buttonSize);
	m_p1			->setMinimumSize(buttonSize);
	//m_p2			->setMaximumSize(buttonSize);
	//m_p2			->setMinimumSize(buttonSize);
	m_p3			->setMaximumSize(buttonSize);
	m_p3			->setMinimumSize(buttonSize);
	m_p4			->setMaximumSize(buttonSize);
	m_p4			->setMinimumSize(buttonSize);
	m_p5			->setMaximumSize(buttonSize);
	m_p5			->setMinimumSize(buttonSize);
	m_p6			->setMaximumSize(buttonSize);
	m_p6			->setMinimumSize(buttonSize);
	//m_p7			->setMaximumSize(buttonSize);
	//m_p7			->setMinimumSize(buttonSize);
	m_p8			->setMaximumSize(buttonSize);
	m_p8			->setMinimumSize(buttonSize);
	m_p9			->setMaximumSize(buttonSize);
	m_p9			->setMinimumSize(buttonSize);

	connect(m_p1,SIGNAL(clicked()),this,SLOT(On_WizardStep1()));
	//connect(m_p2,SIGNAL(clicked()),this,SLOT(On_WizardStep2()));
	connect(m_p3,SIGNAL(clicked()),this,SLOT(On_WizardStep3()));
	connect(m_p4,SIGNAL(clicked()),this,SLOT(On_WizardStep4()));
	connect(m_p5,SIGNAL(clicked()),this,SLOT(On_WizardStep5()));
	connect(m_p6,SIGNAL(clicked()),this,SLOT(On_WizardStep6()));
	//connect(m_p7,SIGNAL(clicked()),this,SLOT(On_WizardStep7()));
	connect(m_p8,SIGNAL(clicked()),this,SLOT(On_WizardStep8()));
	connect(m_p9,SIGNAL(clicked()),this,SLOT(On_WizardStep9()));


	QSize labArrowSize(21, 16);
	QLabel *labArrow2 = new QLabel;
	labArrow2->setPixmap(QPixmap(":CalendarEvent_ToolBarArrowRight.png"));
	labArrow2->setMaximumSize(labArrowSize);
	labArrow2->setMinimumSize(labArrowSize);
	QLabel *labArrow3 = new QLabel;
	labArrow3->setPixmap(QPixmap(":CalendarEvent_ToolBarArrowRight.png"));
	labArrow3->setMaximumSize(labArrowSize);
	labArrow3->setMinimumSize(labArrowSize);
	QLabel *labArrow4 = new QLabel;
	labArrow4->setPixmap(QPixmap(":CalendarEvent_ToolBarArrowRight.png"));
	labArrow4->setMaximumSize(labArrowSize);
	labArrow4->setMinimumSize(labArrowSize);
	QLabel *labArrow5 = new QLabel;
	labArrow5->setPixmap(QPixmap(":CalendarEvent_ToolBarArrowRight.png"));
	labArrow5->setMaximumSize(labArrowSize);
	labArrow5->setMinimumSize(labArrowSize);
	QLabel *labArrow6 = new QLabel;
	labArrow6->setPixmap(QPixmap(":CalendarEvent_ToolBarArrowRight.png"));
	labArrow6->setMaximumSize(labArrowSize);
	labArrow6->setMinimumSize(labArrowSize);
	QLabel *labArrow7 = new QLabel;
	labArrow7->setPixmap(QPixmap(":CalendarEvent_ToolBarArrowRight.png"));
	labArrow7->setMaximumSize(labArrowSize);
	labArrow7->setMinimumSize(labArrowSize);
	QLabel *labArrow8 = new QLabel;
	labArrow8->setPixmap(QPixmap(":CalendarEvent_ToolBarArrowRight.png"));
	labArrow8->setMaximumSize(labArrowSize);
	labArrow8->setMinimumSize(labArrowSize);
	QLabel *labArrow9 = new QLabel;
	labArrow9->setPixmap(QPixmap(":CalendarEvent_ToolBarArrowRight.png"));
	labArrow9->setMaximumSize(labArrowSize);
	labArrow9->setMinimumSize(labArrowSize);

	QGridLayout *grid1= new QGridLayout;
	grid1->addWidget(labSteps,0,0,1,3);
	grid1->addWidget(labArrow5,1,0);
	grid1->addWidget(m_p5,1,1);
	grid1->addWidget(labArrow6,1,2);
	grid1->setSpacing(1);
	grid1->setContentsMargins(0,0,0,0);

	QGridLayout *grid2= new QGridLayout;
	grid2->addWidget(m_p1,0,0);

	grid2->addWidget(labArrow3,0,1);
	grid2->addWidget(m_p3,0,2);

	grid2->addWidget(labArrow4,0,3);
	grid2->addWidget(m_p4,0,4);

	//grid2->addWidget(labArrow2,0,5);
	//grid2->addWidget(m_p2,0,6);

	grid2->addWidget(m_p6,1,0);
	grid2->addWidget(labArrow7,1,1);
	//grid2->addWidget(m_p7,1,2);
	
	grid2->addWidget(labArrow8,1,3);
	grid2->addWidget(m_p8,1,2);
	//grid2->addWidget(labArrow9,1,5);
	grid2->addWidget(m_p9,1,4);

	grid2->setSpacing(1);
	grid2->setContentsMargins(0,0,0,0);


	QHBoxLayout *box = new QHBoxLayout;
	//box->addStretch();
	box->addLayout(grid1);
	box->addLayout(grid2);
	box->addStretch();
	box->setSpacing(0);
	box->setContentsMargins(10,2,2,2);

	ui.frameYellowToolBar->setLayout(box);

}

void FUI_CalendarEvent::ResetWizardButtons()
{
	m_p1->setIcon(":CalendarEvent_1i.png");
	m_p1->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Basic")).arg("10"));
	//m_p2->setIcon(":CalendarEvent_4i.png");
	//m_p2->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Reminder")).arg("10"));
	m_p3->setIcon(":CalendarEvent_2i.png");
	m_p3->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Contacts")).arg("10"));
	m_p4->setIcon(":CalendarEvent_3i.png");
	m_p4->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Projects")).arg("10"));
	m_p5->setIcon(":CalendarEvent_4i.png");
	m_p5->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Resources")).arg("10"));
	m_p6->setIcon(":CalendarEvent_5i.png");
	m_p6->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Breaks")).arg("10"));
	//m_p7->setIcon(":CalendarEvent_7i.png");
	//m_p7->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Recurrence")).arg("10"));
	m_p8->setIcon(":CalendarEvent_6i.png");
	m_p8->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Task")).arg("10"));
	m_p9->setIcon(":CalendarEvent_7i.png");
	m_p9->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Multipart")).arg("10"));
}

void FUI_CalendarEvent::On_WizardStep1()
{
	ResetWizardButtons();
	m_p1->setIcon(":CalendarEvent_1a.png");
	m_p1->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Basic")).arg("12"));
	m_pWizard->setCurrentIndex(CalendarEvent_Wizard::STEP_BASIC);
}
void FUI_CalendarEvent::On_WizardStep2()
{
	return;

	ResetWizardButtons();
	m_p2->setIcon(":CalendarEvent_4a.png");
	m_p2->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Reminder")).arg("12"));
	m_pWizard->setCurrentIndex(CalendarEvent_Wizard::STEP_REMINDER);
}
void FUI_CalendarEvent::On_WizardStep3()
{
	ResetWizardButtons();
	m_p3->setIcon(":CalendarEvent_2a.png");
	m_p3->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Contacts")).arg("12"));
	m_pWizard->setCurrentIndex(CalendarEvent_Wizard::STEP_CONTACTS);
}
void FUI_CalendarEvent::On_WizardStep4()
{
	ResetWizardButtons();
	m_p4->setIcon(":CalendarEvent_3a.png");
	m_p4->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Projects")).arg("12"));
	m_pWizard->setCurrentIndex(CalendarEvent_Wizard::STEP_PROJECTS);
}
void FUI_CalendarEvent::On_WizardStep5()
{
	ResetWizardButtons();
	m_p5->setIcon(":CalendarEvent_4a.png");
	m_p5->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Resources")).arg("12"));
	m_pWizard->setCurrentIndex(CalendarEvent_Wizard::STEP_RESOURCES);
}
void FUI_CalendarEvent::On_WizardStep6()
{
	ResetWizardButtons();
	m_p6->setIcon(":CalendarEvent_5a.png");
	m_p6->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Breaks")).arg("12"));
	m_pWizard->setCurrentIndex(CalendarEvent_Wizard::STEP_BREAKS);
}
void FUI_CalendarEvent::On_WizardStep7()
{
	return;
	ResetWizardButtons();
	m_p7->setIcon(":CalendarEvent_7a.png");
	m_p7->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Recurrence")).arg("12"));
	m_pWizard->setCurrentIndex(CalendarEvent_Wizard::STEP_RECURRENCE);
}
void FUI_CalendarEvent::On_WizardStep8()
{
	ResetWizardButtons();
	m_p8->setIcon(":CalendarEvent_6a.png");
	m_p8->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Task")).arg("12"));
	m_pWizard->setCurrentIndex(CalendarEvent_Wizard::STEP_TASK);
}
void FUI_CalendarEvent::On_WizardStep9()
{
	ResetWizardButtons();
	m_p9->setIcon(":CalendarEvent_7a.png");
	m_p9->setText(QString(STYLE_YELLOW_TOOLBAR_BUTTON).arg(tr("Multipart")).arg("12"));
	GoWizardMode(false);
	//ui.stackedWidgetMain->setCurrentWidget(1); //go details
}


void FUI_CalendarEvent::OnWizardChkBoxStateChanged(int nState)
{
	if (nState==Qt::Checked)
	{
		GoWizardMode(true);
	}
	else
	{
		GoWizardMode(false);
	}

}

//save & close:
void FUI_CalendarEvent::OnSaveClose()
{
	GetDataFromWizard();
	m_bWizardMode=false;
	FUI_CalendarEvent::on_cmdOK();
	m_bWizardMode=true;
	//FuiBase::on_cmdOK();
}


void FUI_CalendarEvent::RebuildPartsTabBar()
{
	if (!m_pTab)
	{
		m_pTab= new SuperTabBar(this);
		m_pTab->setTabsClosable(true);
		m_pTab->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		m_pTab->setStyleSheet(ThemeManager::GetTabBarStyle());
		m_pTab->setContextMenuPolicy(Qt::CustomContextMenu);
		m_pTab->setExpanding(false);
		//m_pTab->setMovable(true);

		connect(m_pTab,SIGNAL(TabClicked()),this,SLOT(OnTabClicked()));
		connect(m_pTab,SIGNAL(tabMoved (int, int)),this,SLOT(OnTabMoved(int,int)));
		connect(m_pTab,SIGNAL(tabCloseRequested (int)),this,SLOT(OnTabCloseRequested(int)));
		connect(m_pTab,SIGNAL(currentChanged (int)),this,SLOT(OnTabCurrentChanged(int)));
		connect(m_pTab,SIGNAL(customContextMenuRequested ( const QPoint &)),this,SLOT(OnTabContextMenu( const QPoint &)));

		QSize buttonSize(24	, 18);
		m_pAddPart	=	new StyledPushButton(this,":Insert16_qcw.png",":Insert16_qcw.png","");
		m_pAddPart	->setMaximumSize(buttonSize);
		m_pAddPart	->setMinimumSize(buttonSize);
		connect(m_pAddPart,SIGNAL(clicked()),this,SLOT(OnAddPart()));
		m_pAddPart->setToolTip(tr("Add Calendar Part"));

		m_pRecurrence	=	new QPushButton(this);
		m_pRecurrence	->setMinimumSize(QSize(100,22));
		m_pRecurrence	->setMaximumSize(QSize(100,22));
		m_pRecurrence	->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed);

		QString strTextButton= "<html><body style=\" font-family:Arial; text-decoration:none;\">\
							   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:800; font-style:normal; color:%1;\">%2</span></p>\
							   </body></html>";
		GUI_Helper::CreateStyledButton(m_pRecurrence,"",strTextButton.arg("white").arg(tr("Recurrence")),0, ThemeManager::GetCEMenuButtonStyle(),0,false);
		connect(m_pRecurrence,SIGNAL(clicked()),this,SLOT(OnRecurrence()));

		m_pTask	=	new QPushButton(this);
		m_pTask	->setMinimumSize(QSize(100,22));
		m_pTask	->setMaximumSize(QSize(100,22));
		m_pTask	->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed);

		GUI_Helper::CreateStyledButton(m_pTask,"",strTextButton.arg("white").arg(tr("Task")),0, ThemeManager::GetCEMenuButtonStyle(),0,false);
		connect(m_pTask,SIGNAL(clicked()),this,SLOT(OnTask()));


		QHBoxLayout *box = new QHBoxLayout;
		box->addWidget(m_pAddPart);
		box->addWidget(m_pTab);
		//box->addStretch(1);
		box->addWidget(m_pRecurrence);
		box->addWidget(m_pTask);
		box->setSpacing(0);
		box->setContentsMargins(0,0,0,0);
		dynamic_cast<QVBoxLayout*>(ui.stackedWidgetMain->widget(1)->layout())-> insertLayout(0,box);
	}
}
void FUI_CalendarEvent::RefreshParts(DbRecordSet &lstParts)
{
	//based on parts list rebuild part tabs:
	int nSize=lstParts.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nPartIndex=i+PART_INDEX_OFFSET;
		if (nPartIndex>=ui.stackedWidgetParts->count())
		{
			AddPart(false);
			nPartIndex=ui.stackedWidgetParts->count()-1;
		}

		CalendarEvent_DetailPane *p=m_hshParts.value(nPartIndex,NULL);
		if (p)
		{
			DbRecordSet rowPart=lstParts.getRow(i);
			SetPartDataFromPartRow(rowPart,p);
			p->SetEditMode(m_nMode==MODE_EDIT || m_nMode==MODE_INSERT);
		}
	}

	if (nSize>0) //close all extra:
	{
		m_pTab->blockSignals(true);
		for(int i=m_pTab->count()-1;i>=nSize;i--)
		{
			m_pTab->removeTab(i);
		}
		m_pTab->blockSignals(false);
		
		if (nSize<(ui.stackedWidgetParts->count()-PART_INDEX_OFFSET))
		{
			int nStart=nSize+PART_INDEX_OFFSET;
			int nEnd=ui.stackedWidgetParts->count();

			for(int i=nStart;i<nEnd;i++)
			{
				DeletePart(nStart-1);
			}
		}
	}

	if (m_pTab->count()>0)
	{
		int nCurrentTabIndex=m_pTab->currentIndex()+PART_INDEX_OFFSET;
		if (nCurrentTabIndex<ui.stackedWidgetParts->count())
			ui.stackedWidgetParts->setCurrentIndex(nCurrentTabIndex);
	}
}



//reccurence is set on page 0:
void FUI_CalendarEvent::OnRecurrence()
{
	m_pTab->setCurrentIndex(-1);
	ui.stackedWidgetParts->setCurrentIndex(0);
}

void FUI_CalendarEvent::OnTask()
{
	m_pTab->setCurrentIndex(-1);
	ui.stackedWidgetParts->setCurrentIndex(1);
}

void FUI_CalendarEvent::OnAddPart()
{
	if (!m_pTab) return;
	if (m_nMode==MODE_EDIT || m_nMode==MODE_INSERT)
	{
		AddPart(true);
	}
}
void FUI_CalendarEvent::AddPart(bool bCopyFromCurrent)
{
	CalendarEvent_DetailPane *p = new CalendarEvent_DetailPane(this);
	connect(p,SIGNAL(SignalTemplateChanged(int)),this,SLOT(OnTemplateChanged(int)));
	connect(p,SIGNAL(SignalInviteDataChanged()),this,SLOT(OnInviteDataChanged()));

	connect(p,SIGNAL(DateTimeFromChanged(const QDateTime &)),this,SLOT(OnDateTimeFromChanged(const QDateTime &)));
	connect(p,SIGNAL(DateTimeToChanged(const QDateTime &)),this,SLOT(OnDateTimeToChanged(const QDateTime &)));

	 

	int nIdx=ui.stackedWidgetParts->addWidget(p);
	ui.stackedWidgetParts->setCurrentIndex(nIdx);
	m_hshParts[nIdx]=p;


	int nSourceIndex=m_pTab->currentIndex();
	if (m_pTab->count()==0)
		nSourceIndex=-1;

	int nTabIdx=m_pTab->addTab(tr("Part ")+QString::number(m_hshParts.size()));
	m_pTab->setCurrentIndex(nTabIdx); 
	//connect(m_pTab->tabButton(nTabIdx))

	
	//copy data from current pane
	if (nSourceIndex>=0 && bCopyFromCurrent)
	{
		DbRecordSet rowPartSource;
		GetAllPartData(rowPartSource,nSourceIndex);
		if (rowPartSource.getRowCount()>0)
		{
			DbRecordSet rowNewPart=CopyPartRow(rowPartSource);
			m_lstDataCalEventPart.merge(rowNewPart);
			//set data on pane:
			SetPartDataFromPartRow(rowNewPart,p);
		}
	}
}

void FUI_CalendarEvent::OnTabCloseRequested ( int index )
{
	if (!m_pTab) return;
	if (m_nMode==MODE_EDIT || m_nMode==MODE_INSERT)
	{
		QString strMsg =QString(tr("Do you really want to delete part %1?")).arg(index+1);
		int nResult=QMessageBox::question(this,tr("Warning"),strMsg,tr("Yes"),tr("No"));
		if (nResult!=0)
			return;
		DeletePart(index);
	}
}
void FUI_CalendarEvent::DeletePart(int nTabindex)
{
	if (nTabindex==0 && m_pTab->count()==1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Last Calendar Part can not be deleted"));
		return;
	}

	//remove from data:
	m_lstDataCalEventPart.deleteRow(nTabindex);

	int nPartIndex=nTabindex+PART_INDEX_OFFSET;
	CalendarEvent_DetailPane *p=m_hshParts.value(nPartIndex,NULL);

	if (p)
	{
		int nPreviousMaxIndex=ui.stackedWidgetParts->count();
		ui.stackedWidgetParts->removeWidget(p);
		m_pTab->removeTab(nTabindex);

		int nSize=m_pTab->count();
		for(int i=0;i<nSize;i++)
		{
			m_pTab->setTabText(i,tr("Part ")+QString::number(i+1));
		}

		delete p;
		ui.stackedWidgetParts->setCurrentIndex(PART_INDEX_OFFSET);
		m_pTab->setCurrentIndex(0);

		m_hshParts.remove(nPartIndex);

		//iterate through keys and move 'em:
		for(int i=nPartIndex+1;i<nPreviousMaxIndex;i++)
		{
			CalendarEvent_DetailPane *p=m_hshParts.value(i,NULL);
			if (p)
			{
				m_hshParts[i-1]=p; //move all with 1
				m_hshParts.remove(i);
			}
		}
	}
}

void FUI_CalendarEvent::OnTabCurrentChanged(int index)
{
	ui.stackedWidgetParts->setCurrentIndex(index+PART_INDEX_OFFSET);
}

 
void FUI_CalendarEvent::OnTabMoved(int from,int to)
{

}
/*
void FUI_CalendarEvent::OnTabClicked()
{
	if (ui.stackedWidgetParts->currentIndex()==0) //if on recurrence then switch back:
	{
		int nTabIndex=m_pTab->currentIndex();
		ui.stackedWidgetParts->setCurrentIndex(nTabIndex+1);
	}
}
*/
void FUI_CalendarEvent::OnDeleteCurrentPart()
{
	OnTabCloseRequested(m_pTab->currentIndex());
}

void FUI_CalendarEvent::OnTabContextMenu( const QPoint &pos)
{
	if(m_nMode==MODE_READ || m_nMode==MODE_EMPTY)
	{
		//event:
		QAction* pActionAdd = new QAction(tr("Add Calendar Part"), this);
		connect(pActionAdd, SIGNAL(triggered()), this, SLOT(OnAddPart()));
		QAction* pActionDel = new QAction(tr("Delete Calendar Part"), this);
		connect(pActionDel, SIGNAL(triggered()), this, SLOT(OnDeleteCurrentPart()));

		QMenu CnxtMenu(this);
		CnxtMenu.addAction(pActionAdd);
		CnxtMenu.addAction(pActionDel);
		CnxtMenu.exec( m_pTab->mapToGlobal(pos));
	}
}




void FUI_CalendarEvent::DataDefine()
{
	DbSqlTableDefinition::GetKeyData(m_nTableID,m_TableData);
	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));
	m_lstDataCalEventPart.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));
	m_lstDataCalEventPartTaskData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
}

void FUI_CalendarEvent::DataClear()
{
	m_lstData.clear();
	m_lstDataCalEventPart.clear();
	m_lstDataCalEventPartCache.clear();

	//tasks:
	ui.frameTask->ClearTaskData();
	m_lstTaskCache.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	m_lstTaskCache.addRow();
}


void FUI_CalendarEvent::DataPrepareForInsert(Status &err)
{
	FuiBase::DataPrepareForInsert(err);

	m_lstData.setData(0,"BCEV_DAT_CREATED",QDateTime::currentDateTime());
	m_lstData.setData(0,"BCEV_IS_PRIVATE",0);
	m_lstData.setData(0,"BCEV_IS_INFORMATION",0);
	m_lstData.setData(0,"BCEV_IS_PRELIMINARY",0);
	
	
	CalendarHelperCore::AddEmptyPartRow(m_lstData,m_lstDataCalEventPart);
	
	//set owner=logged:
	int nLoggedID=g_pClientManager->GetPersonID();
	if (nLoggedID!=0)
		m_lstDataCalEventPart.setData(0,"CENT_OWNER_ID",nLoggedID);

	m_lstDataCalEventPart.setData(0,"CENT_OWNER_ID",nLoggedID);

	if (!m_bSkipSetOwnerOnInsert)
	{
		int nLoggedContactID=g_pClientManager->GetPersonContactID();
		if (nLoggedContactID>0)
		{
			//set assignment logged user = Owner:
			DbRecordSet lstNMRXPerson=m_lstDataCalEventPart.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
			if (lstNMRXPerson.getColumnCount()==0)
				lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
			QList<int> lstUsersAssign;
			lstUsersAssign.append(nLoggedContactID);
			NMRXRelationWidget::CreateAssignmentData(lstNMRXPerson,CE_COMM_ENTITY,BUS_CM_CONTACT,lstUsersAssign,tr("Owner"),0,BUS_CAL_INVITE);
			//issue 2188: do not invite owner:
			DbRecordSet lstInvite=lstNMRXPerson.getDataRef(0,"X_RECORD").value<DbRecordSet>();
			lstInvite.setData(0,"BCIV_INVITE",0);
			lstNMRXPerson.setData(0,"X_RECORD",lstInvite);
			m_lstDataCalEventPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
		}

	}

}

void FUI_CalendarEvent::DataPrepareForCopy(Status &err)
{
	err.setError(0);
	QVariant intNull(QVariant::Int);

	//clear ID fields:
	m_lstData.setColValue(m_lstData.getColumnIdx(m_TableData.m_strPrimaryKey),intNull);
	CleanPartListAfterCopy(m_lstDataCalEventPart);
	ui.frameTask->m_recTask.setColValue(ui.frameTask->m_recTask.getColumnIdx("BTKS_ID"),intNull);
	m_lstTaskCache=ui.frameTask->m_recTask;
	//ui.arSelector->Invalidate();
}


void FUI_CalendarEvent::DataDelete(Status &err)
{
	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"BCEV_ID");
	lst.addRow();
	lst.setData(0,0,m_nEntityRecordID);
	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	_SERVER_CALL(ClientSimpleORM->Delete(err,BUS_CAL_EVENT,lst,m_strLockedRes, lstStatusRows, pBoolTransaction))
	if (err.IsOK())
		DataClear();

}
void FUI_CalendarEvent::DataLock(Status &err)
{
	DbRecordSet lst;
	lst.addColumn(QVariant::Int,"BCEV_ID");
	lst.addRow();
	lst.setData(0,0,m_nEntityRecordID);
	DbRecordSet lstStatusRows;
	_SERVER_CALL(ClientSimpleORM->Lock(err,BUS_CAL_EVENT,lst,m_strLockedRes, lstStatusRows))
		if (!err.IsOK())
			if (err.getErrorCode()==StatusCodeSet::ERR_SQL_LOCKED_BY_ANOTHER)
			{
				QString strMsg=err.getErrorTextRaw();
				int nPos=strMsg.indexOf(" - ");
				if (nPos>=0)
				{
					QString strPersCode=strMsg.left(nPos);
					//test if this is moa
					if(strPersCode==g_pClientManager->GetPersonCode())
					{
						QApplication::restoreOverrideCursor();
						int nResult=QMessageBox::question(this,tr("Warning"),tr("This record is already locked by you. Do you wish to discard the old locked version and to restart editing the record?"),tr("Yes"),tr("No"));
						if (nResult==0)
						{
							//unlock record on server:
							Status err_temp;
							bool bUnlocked=false;
							_SERVER_CALL(ClientSimpleORM->UnLockByRecordID(err_temp,BUS_CAL_EVENT,lst,bUnlocked))
								if (!err_temp.IsOK() || !bUnlocked)
								{
									return; //return with old lock error
								}
								else
								{
									err.setError(0); //clear error and return
									_SERVER_CALL(ClientSimpleORM->Lock(err,BUS_CAL_EVENT,lst,m_strLockedRes, lstStatusRows))
										return;
								}

						}

					}
				}
			}

}
void FUI_CalendarEvent::DataUnlock(Status &err)
{
	FuiBase::DataUnlock(err);
	m_lstDataCalEventPart=m_lstDataCalEventPartCache;

	if (!err.IsOK()) return;
	if (m_lstTaskCache.getRowCount()>0)
		ui.frameTask->m_recTask=m_lstTaskCache;			//restore from cache
	else
		ui.frameTask->ClearTaskData();
	//ui.arSelector->CancelChanges();
	//if (m_lstData.getRowCount()>0)
	//	ui.arSelector->SetRecordDisplayName(m_lstData.getDataRef(0,"BDMD_NAME").toString());
}
void FUI_CalendarEvent::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);
	GetAllDataFromGUI();

	ui.frameTask->CheckTaskRecordBeforeSave(err);
	if (!err.IsOK()) return;

	//if template, all data can be mixed
	if (m_lstData.getDataRef(0,"BCEV_TEMPLATE_FLAG").toInt())
	{
		return;
	}

	//check datetime ranges: if all parts fit into global from/to...
	QDateTime datEventFrom=m_lstData.getDataRef(0,"BCEV_FROM").toDateTime();
	QDateTime datEventTo=m_lstData.getDataRef(0,"BCEV_TO").toDateTime();
	if (datEventFrom.isNull() || datEventTo.isNull() || datEventFrom>datEventTo)
	{
		err.setError(1,tr("Event From and To Dates are not properly set!"));
		return;
	}
	//Check if the date to is al least to minutes bigger than date from.
	if ((datEventFrom.date() == datEventTo.date()) && (datEventFrom.time().hour()==datEventTo.time().hour()) && (datEventTo.time().minute()<(datEventFrom.time().minute()+5)))
	{
		err.setError(1,tr("Date To must be at least five minutes bigger than Date From!"));
		return;
	}


	bool bAlignMainDates=false;
	int nPreliminaryFlag=0;

	int nSize=m_lstDataCalEventPart.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstOptions=m_lstDataCalEventPart.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
		int nSize1=lstOptions.getRowCount();
		if (nSize1>1)
			nPreliminaryFlag=1; //mail from MB: 15.10.2009: if more >1 option then set this flag, else reset

		for(int k=0;k<nSize1;k++)
		{
			QDateTime datOptionFrom=lstOptions.getDataRef(k,"BCOL_FROM").toDateTime();
			QDateTime datOptionTo=lstOptions.getDataRef(k,"BCOL_TO").toDateTime();


			if (datOptionFrom.isNull() || datOptionTo.isNull() || datOptionFrom>datOptionTo)
			{
				err.setError(1,QString(tr("Part %1, Option %2, From and To Dates are not properly set!")).arg(i+1).arg(k+1));
				return;
			}
/*
			qDebug()<<datEventFrom;
			qDebug()<<datEventTo;
			qDebug()<<datOptionFrom;
			qDebug()<<datOptionTo;
*/
			if (datEventFrom>datOptionFrom || datEventTo<datOptionTo)
			{
				if (!bAlignMainDates)
				{
					int nResult = QMessageBox::question(NULL, tr("Confirmation"), tr("There are event parts lying outside the main event time period. Should the main time period be adjusted automatically?"), tr("Yes"),tr("No"));
					if (nResult==0)
					{
						bAlignMainDates=true;
					}
				}

				if (bAlignMainDates)
				{
					if (datEventFrom>datOptionFrom)
						m_lstData.setData(0,"BCEV_FROM",datOptionFrom);
					if (datEventTo<datOptionTo)
						m_lstData.setData(0,"BCEV_TO",datOptionTo);
				}
			}

			DbRecordSet lstBreak=lstOptions.getDataRef(k,"BREAK_LIST").value<DbRecordSet>();
			int nSize2=lstBreak.getRowCount();
			for(int z=0;z<nSize2;z++)
			{
				if (datOptionFrom>lstBreak.getDataRef(z,"BCBL_FROM").toDateTime() || datOptionTo<lstBreak.getDataRef(z,"BCBL_TO").toDateTime())
				{
					err.setError(1,QString(tr("Part %1,Option %2, Break row %3 From and To Dates are out of range of option!")).arg(i+1).arg(k+1).arg(z+1));
					return;
				}
			}
		}
	}

	//set preliminary flag:
	m_lstData.setData(0,"BCEV_IS_PRELIMINARY",nPreliminaryFlag);

}

void FUI_CalendarEvent::DataWrite(Status &err)
{
	//GetAllDataFromGUI();

	//m_lstDataCalEventPart.Dump();

	DbRecordSet lstUAR,lstGAR;
	//ui.arSelector->Save(m_nEntityRecordID,BUS_CM_CONTACT,&lstUAR,&lstGAR); //get ar rows for save
	_SERVER_CALL(BusCalendar->WriteEvent(err,m_lstData,m_lstDataCalEventPart,m_lstDataCalEventPartTaskData,m_strLockedRes, lstUAR, lstGAR));
	if (err.IsOK())
	{
		m_nEntityRecordID=m_lstData.getDataRef(0,m_TableData.m_strPrimaryKey).toInt();
		m_strLockedRes="";
		m_lstDataCache=m_lstData;
		m_lstDataCalEventPartCache=m_lstDataCalEventPart;
		if (m_lstDataCalEventPartTaskData.getRowCount()>0)
		{
			ui.frameTask->m_recTask=m_lstDataCalEventPartTaskData;	//return d'
			m_lstTaskCache=m_lstDataCalEventPartTaskData;
		}
		//re-init UAR selector (if after insert)
		//ui.arSelector->Load(m_nEntityRecordID,BUS_CM_CONTACT,&lstUAR,&lstGAR);
		//ui.arSelector->SetRecordDisplayName(m_p_Selector->GetCalculatedNameFromRecord(m_lstData));
	}
}
void FUI_CalendarEvent::DataRead(Status &err,int nRecordID)
{
	DbRecordSet lstUAR,lstGAR;
	_SERVER_CALL(BusCalendar->ReadEvent(err,nRecordID,m_lstData,m_lstDataCalEventPart,m_lstDataCalEventPartTaskData,lstUAR,lstGAR));
	if(err.IsOK())
	{
		m_lstDataCache=m_lstData;
		m_lstDataCalEventPartCache=m_lstDataCalEventPart;
		if (m_lstDataCalEventPartTaskData.getRowCount()>0)
		{
			ui.frameTask->m_recTask=m_lstDataCalEventPartTaskData;	//return d'
			m_lstTaskCache=m_lstDataCalEventPartTaskData;
		}
	}

}

DbRecordSet FUI_CalendarEvent::CopyPartRow(DbRecordSet &rowPart)
{
	DbRecordSet rowPartTarget=rowPart;
	CleanPartListAfterCopy(rowPartTarget);
	return rowPartTarget;
}



void FUI_CalendarEvent::CleanPartListAfterCopy(DbRecordSet &lstParts)
{
	QVariant intNull(QVariant::Int);

	lstParts.setColValue("BCEP_ID",intNull);
	lstParts.setColValue("CENT_ID",intNull);
	lstParts.setColValue("CENT_TASK_ID",intNull);
	lstParts.setColValue("BCEP_COMM_ENTITY_ID",intNull);

	int nSize=lstParts.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		//OPTIONS && BREAKS:
		DbRecordSet lstOptions=lstParts.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
		CalendarHelperCore::CleanOptionListAfterCopy(lstOptions);
		lstParts.setData(i,"OPTION_LIST",lstOptions);

		//NMR: clear all invite and key1 fields:
		DbRecordSet lstNMRXPerson=lstParts.getDataRef(i,"NMRX_PERSON_LIST").value<DbRecordSet>();
		if (lstNMRXPerson.getColumnCount()==0)
			lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
		else
		{
			lstNMRXPerson.setColValue("BNMR_ID",intNull);
			lstNMRXPerson.setColValue("BNMR_TABLE_KEY_ID_1",intNull);
			int nSize2=lstNMRXPerson.getRowCount();
			for(int k=0;k<nSize2;k++)
			{
				DbRecordSet x_Record=lstNMRXPerson.getDataRef(k,"X_RECORD").value<DbRecordSet>();
				x_Record.setColValue("BCIV_ID",intNull);
				lstNMRXPerson.setData(k,"X_RECORD",x_Record);
			}
		}
		lstParts.setData(i,"NMRX_PERSON_LIST",lstNMRXPerson);
		//lstNMRXPerson.Dump();

		DbRecordSet lstNMRXProject=lstParts.getDataRef(i,"NMRX_PROJECT_LIST").value<DbRecordSet>();
		if (lstNMRXProject.getColumnCount()==0)
			lstNMRXProject.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
		else
		{
			lstNMRXProject.setColValue("BNMR_ID",intNull);
			lstNMRXProject.setColValue("BNMR_TABLE_KEY_ID_1",intNull);
			int nSize2=lstNMRXProject.getRowCount();
			for(int k=0;k<nSize2;k++)
			{
				DbRecordSet x_Record=lstNMRXProject.getDataRef(k,"X_RECORD").value<DbRecordSet>();
				x_Record.setColValue("BCIV_ID",intNull);
				lstNMRXProject.setData(k,"X_RECORD",x_Record);
			}
		}
		lstParts.setData(i,"NMRX_PROJECT_LIST",lstNMRXProject);

		DbRecordSet lstNMRXResources=lstParts.getDataRef(i,"NMRX_RESOURCE_LIST").value<DbRecordSet>();
		if (lstNMRXResources.getColumnCount()==0)
			lstNMRXResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
		else
		{
			lstNMRXResources.setColValue("BNMR_ID",intNull);
			lstNMRXResources.setColValue("BNMR_TABLE_KEY_ID_1",intNull);
			int nSize2=lstNMRXResources.getRowCount();
			for(int k=0;k<nSize2;k++)
			{
				DbRecordSet x_Record=lstNMRXResources.getDataRef(k,"X_RECORD").value<DbRecordSet>();
				x_Record.setColValue("BCIV_ID",intNull);
				lstNMRXResources.setData(k,"X_RECORD",x_Record);
			}
		}
		lstParts.setData(i,"NMRX_RESOURCE_LIST",lstNMRXResources);
	}

}



void FUI_CalendarEvent::SetPartDataFromPartRow(DbRecordSet &rowPart,CalendarEvent_DetailPane *pane)
{
	DbRecordSet lstOptions=rowPart.getDataRef(0,"OPTION_LIST").value<DbRecordSet>();
	if (lstOptions.getRowCount()==0)
		lstOptions=CalendarHelperCore::CreateOptionForPart(m_lstData,rowPart);

	DbRecordSet lstNMRXPerson=rowPart.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
	if (lstNMRXPerson.getColumnCount()==0)
		lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	//lstNMRXPerson.Dump();

	DbRecordSet lstNMRXProject=rowPart.getDataRef(0,"NMRX_PROJECT_LIST").value<DbRecordSet>();
	if (lstNMRXProject.getColumnCount()==0)
		lstNMRXProject.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXResources=rowPart.getDataRef(0,"NMRX_RESOURCE_LIST").value<DbRecordSet>();
	if (lstNMRXResources.getColumnCount()==0)
		lstNMRXResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	pane->SetData(m_lstData,rowPart,lstOptions,lstNMRXPerson,lstNMRXProject,lstNMRXResources);
}

void FUI_CalendarEvent::GetAllPartData(DbRecordSet &lstParts, int index)
{
	lstParts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY));

	if (index==-1)
	{
		//refresh all parts:
		QHashIterator <int,CalendarEvent_DetailPane*> i(m_hshParts);
		while (i.hasNext()) 
		{
			i.next();
			DbRecordSet rowEvent;
			DbRecordSet rowPart;
			DbRecordSet lstOptions;
			DbRecordSet lstNMRXPerson;
			DbRecordSet lstNMRXProject;
			DbRecordSet lstNMRXResources;

			i.value()->GetData(rowEvent,rowPart,lstOptions,lstNMRXPerson,lstNMRXProject,lstNMRXResources);

			//rowEvent.Dump();

			rowPart.setData(0,"OPTION_LIST",lstOptions);
			rowPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
			rowPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
			rowPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResources);

			if (i.key()==1) //first part on first tab
			{
				//propagate desc frm opt->part->main only for desc at pos = 0
				rowPart.setData(0,"BCEP_DESCRIPTION",lstOptions.getDataRef(0,"BCOL_DESCRIPTION"));
				m_lstData.setData(0,"BCEV_DESCRIPTION",rowPart.getDataRef(0,"BCEP_DESCRIPTION"));
			}


			m_lstData.setData(0,"BCEV_TEMPLATE_ID",rowEvent.getDataRef(0,"BCEV_TEMPLATE_ID"));
			m_lstData.setData(0,"BCEV_CATEGORY",rowEvent.getDataRef(0,"BCEV_CATEGORY").toString());

			lstParts.merge(rowPart);
		}
	}
	else
	{
		int nPartIndex=index+PART_INDEX_OFFSET;
		CalendarEvent_DetailPane *p=m_hshParts.value(nPartIndex,NULL);
		if (p)
		{
			DbRecordSet rowEvent;
			DbRecordSet rowPart;
			DbRecordSet lstOptions;
			DbRecordSet lstNMRXPerson;
			DbRecordSet lstNMRXProject;
			DbRecordSet lstNMRXResources;

			p->GetData(rowEvent,rowPart,lstOptions,lstNMRXPerson,lstNMRXProject,lstNMRXResources);

			//rowEvent.Dump();

			m_lstData.setData(0,"BCEV_TEMPLATE_ID",rowEvent.getDataRef(0,"BCEV_TEMPLATE_ID"));
			m_lstData.setData(0,"BCEV_CATEGORY",rowEvent.getDataRef(0,"BCEV_CATEGORY").toString());


			//propagate desc frm opt->part->main
			rowPart.setData(0,"BCEP_DESCRIPTION",lstOptions.getDataRef(0,"BCOL_DESCRIPTION"));
			m_lstData.setData(0,"BCEV_DESCRIPTION",rowPart.getDataRef(0,"BCEP_DESCRIPTION"));

			rowPart.setData(0,"OPTION_LIST",lstOptions);
			rowPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
			rowPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
			rowPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResources);

			lstParts.merge(rowPart);
		}
	}

}
void FUI_CalendarEvent::GetCurrentPartAndOption(DbRecordSet &rowPart,DbRecordSet &rowOption)
{
	int nPartIndex=m_pTab->currentIndex()+PART_INDEX_OFFSET;
	CalendarEvent_DetailPane *p=m_hshParts.value(nPartIndex,NULL);
	if (p)
	{
		DbRecordSet rowEvent;
		DbRecordSet lstOptions;
		DbRecordSet lstNMRXPerson;
		DbRecordSet lstNMRXProject;
		DbRecordSet lstNMRXResources;

		p->GetData(rowEvent,rowPart,lstOptions,lstNMRXPerson,lstNMRXProject,lstNMRXResources);

		m_lstData.setData(0,"BCEV_TEMPLATE_ID",rowEvent.getDataRef(0,"BCEV_TEMPLATE_ID"));
		m_lstData.setData(0,"BCEV_CATEGORY",rowEvent.getDataRef(0,"BCEV_CATEGORY").toString());

		int nOptionRow=p->GetCurrentOptionRow();
		rowOption=lstOptions.getRow(nOptionRow);

		//propagate desc frm opt->part->main
		rowPart.setData(0,"BCEP_DESCRIPTION",rowOption.getDataRef(0,"BCOL_DESCRIPTION"));
		m_lstData.setData(0,"BCEV_DESCRIPTION",rowPart.getDataRef(0,"BCEP_DESCRIPTION"));

		rowPart.setData(0,"OPTION_LIST",rowOption);
		rowPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
		rowPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
		rowPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResources);
	}

}

void FUI_CalendarEvent::SetOwner(int nPersonID)
{
	m_lstDataCalEventPart.setColValue("CENT_OWNER_ID",nPersonID);
	RefreshDisplay();
}

//nViewType calendar: only if custom sulotion then fak 'em
void FUI_CalendarEvent::SetDefaults(bool bGoWizardMode,DbRecordSet *rowDefault,DbRecordSet *lstContacts,DbRecordSet *lstProjects, DbRecordSet *lstResources,bool bScheduleTask, bool bIsTemplate, int nTemplateID, bool bAS_Customer)
{
	if (!g_FunctionPoint.IsFPAvailable(FP_CALENDAR__NEW_ENTRIES))
		return;

	if (bAS_Customer)
		m_bSkipSetOwnerOnInsert=true;

	//MB over phone: skip insert owner if contacts are present:
	if (lstContacts)
		if (lstContacts->getRowCount()>0)
		{
			m_bSkipSetOwnerOnInsert=true;
		}

	//go insert mode
	on_cmdInsert();

	if (bAS_Customer)
	{
		
		//nViewType only for AS-Operation room assholes: if set then:
		QString strFrom = g_pSettings->GetApplicationOption(CALENDAR_START_DAILY_WORK_TIME).toString();
		QString strTo = g_pSettings->GetApplicationOption(CALENDAR_END_DAILY_WORK_TIME).toString();

		if(rowDefault)
		{
			if (!strFrom.isEmpty())
			{
				QDateTime datStart=rowDefault->getDataRef(0,"BCEV_FROM").toDateTime();
				datStart.setTime(QTime::fromString(strFrom,"hh:mm:ss"));
				rowDefault->setData(0,"BCEV_FROM",datStart);

			}
			if (!strTo.isEmpty())
			{
				QDateTime datStart=rowDefault->getDataRef(0,"BCEV_TO").toDateTime();
				datStart.setTime(QTime::fromString(strTo,"hh:mm:ss"));
				rowDefault->setData(0,"BCEV_TO",datStart);
			}
		}
	}



	if (rowDefault)
	{
		m_lstData.assignRow(0,*rowDefault);
		m_lstData.setData(0,"BCEV_DAT_CREATED",QDateTime::currentDateTime());
	}
	
	//if template:
	if (bIsTemplate)
		m_lstData.setData(0,"BCEV_TEMPLATE_FLAG",1);
	else
		m_lstData.setData(0,"BCEV_TEMPLATE_FLAG",0);

	//assignments:
	QList<int> lstContactsAssign;
	//QList<int> lstUsersAssign;
	QList<int> lstProjectAssign;
	QList<int> lstResourceAssign;



	if (lstContacts) //in format: <ID> or in NMRX assignemnet format: TVIEW_BUS_NMRX_RELATION_SELECT
	{
		int nIdx=lstContacts->getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nIdx<0)
			nIdx=lstContacts->getColumnIdx("BCNT_ID");
		if (nIdx<0 && lstContacts->getColumnCount()>0)
			nIdx=0;
		int nSize=lstContacts->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			lstContactsAssign.append(lstContacts->getDataRef(i,nIdx).toInt());
		}
	}
	//if (lstUsers) //in format: <ID> or in NMRX assignemnet format: TVIEW_BUS_NMRX_RELATION_SELECT
	//{
	//	Q_ASSERT(false);
		/*
		int nIdx=lstUsers->getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nIdx<0)
			nIdx=lstUsers->getColumnIdx("BPER_ID");;
		if (nIdx<0 && lstUsers->getColumnCount()>0)
			nIdx=0;
		int nSize=lstUsers->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			lstUsersAssign.append(lstUsers->getDataRef(i,nIdx).toInt());
		}
		*/
	//}
	if (lstProjects) //in format: <ID> or in NMRX assignemnet format: TVIEW_BUS_NMRX_RELATION_SELECT
	{
		int nIdx=lstProjects->getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nIdx<0)
			nIdx=lstProjects->getColumnIdx("BUSP_ID");;
		if (nIdx<0 && lstProjects->getColumnCount()>0)
			nIdx=0;
		int nSize=lstProjects->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			lstProjectAssign.append(lstProjects->getDataRef(i,nIdx).toInt());
		}
	}
	if (lstResources) //in format: <ID> or in NMRX assignemnet format: TVIEW_BUS_NMRX_RELATION_SELECT
	{
		int nIdx=lstResources->getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nIdx<0)
			nIdx=lstResources->getColumnIdx("BRES_ID");;
		if (nIdx<0 && lstResources->getColumnCount()>0)
			nIdx=0;
		int nSize=lstResources->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			lstResourceAssign.append(lstResources->getDataRef(i,nIdx).toInt());
		}
	}

	DbRecordSet lstNMRXPerson=m_lstDataCalEventPart.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
	if (lstNMRXPerson.getColumnCount()==0)
		lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXProject=m_lstDataCalEventPart.getDataRef(0,"NMRX_PROJECT_LIST").value<DbRecordSet>();
	if (lstNMRXProject.getColumnCount()==0)
		lstNMRXProject.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXResources=m_lstDataCalEventPart.getDataRef(0,"NMRX_RESOURCE_LIST").value<DbRecordSet>();
	if (lstNMRXResources.getColumnCount()==0)
		lstNMRXResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	
	DbRecordSet lstNMRX_1;
	NMRXRelationWidget::CreateAssignmentData(lstNMRX_1,CE_COMM_ENTITY,BUS_CM_CONTACT,lstContactsAssign,tr("assigned"),0,BUS_CAL_INVITE);
	//DbRecordSet lstNMRX_2;
	//NMRXRelationWidget::CreateAssignmentData(lstNMRX_2,CE_COMM_ENTITY,BUS_PERSON,lstUsersAssign,tr("assigned"),0,BUS_CAL_INVITE);
	DbRecordSet lstNMRX_3;
	NMRXRelationWidget::CreateAssignmentData(lstNMRX_3,CE_COMM_ENTITY,BUS_PROJECT,lstProjectAssign,tr("assigned"));
	DbRecordSet lstNMRX_4;
	NMRXRelationWidget::CreateAssignmentData(lstNMRX_4,CE_COMM_ENTITY,BUS_CAL_RESOURCE,lstResourceAssign,tr("assigned"));

	//MainEntitySelectionController cachePerson;
	//cachePerson.Initialize(ENTITY_BUS_PERSON);
	//cachePerson.ReloadData();

	lstNMRXPerson.merge(lstNMRX_1);
	//lstNMRXPerson.merge(lstNMRX_2);
	lstNMRXProject.merge(lstNMRX_3);
	lstNMRXResources.merge(lstNMRX_4);

	//By her majesty MB: filter duplicate assignments:
	NMRXRelationWidget::TestListForDuplicateAssignments(BUS_CM_CONTACT,lstNMRXPerson);
	NMRXRelationWidget::TestListForDuplicateAssignments(BUS_PROJECT,lstNMRXProject);
	NMRXRelationWidget::TestListForDuplicateAssignments(BUS_CAL_RESOURCE,lstNMRXResources);


	//if AS then reset invite flag for all persons
	if (bAS_Customer)
	{
		int nSize=lstNMRXPerson.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			DbRecordSet lstInvite=lstNMRXPerson.getDataRef(i,"X_RECORD").value<DbRecordSet>();
			lstInvite.setColValue("BCIV_INVITE",0);
			lstNMRXPerson.setData(i,"X_RECORD",lstInvite);
		}
	}
	else //MB if one: clear, if > one then clear for current user/contact
	{
		int nLoggedPersContactID=g_pClientManager->GetPersonContactID();
		int nSize=lstNMRXPerson.getRowCount();
		if (nSize==1) //remove if one
		{
			DbRecordSet lstInvite=lstNMRXPerson.getDataRef(0,"X_RECORD").value<DbRecordSet>();
			lstInvite.setColValue("BCIV_INVITE",0);
			lstNMRXPerson.setData(0,"X_RECORD",lstInvite);
		}
		else
		{
			for (int i=0;i<nSize;i++)
			{
				if (lstNMRXPerson.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt()==nLoggedPersContactID) //reset invite for logged contact
				{
					DbRecordSet lstInvite=lstNMRXPerson.getDataRef(i,"X_RECORD").value<DbRecordSet>();
					lstInvite.setColValue("BCIV_INVITE",0);
					lstNMRXPerson.setData(i,"X_RECORD",lstInvite);
				}
			}		
		}


	}

	m_lstDataCalEventPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
	m_lstDataCalEventPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
	m_lstDataCalEventPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResources);

	if (bAS_Customer)
	{
		int nType=g_pSettings->GetApplicationOption(PRESENCE_TYPE_PRELIMINARY).toInt();
		if (nType>0)
			m_lstDataCalEventPart.setData(0,"BCEP_PRESENCE_TYPE_ID",nType);
	}


	//if template > 0 then create from it:
	if (nTemplateID>0)
		LoadFromTemplate(nTemplateID);

	//if schedule task:
	if (bScheduleTask)
	{
		ui.frameTask->m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE", 1);	 //his active
	}

	RefreshDisplay();

	if (bGoWizardMode)
		GoWizardMode(true);

	//m_lstData.Dump();
}

void FUI_CalendarEvent::AssignEntities(int nPartID,DbRecordSet *lstContacts,DbRecordSet *lstProjects, DbRecordSet *lstResources)
{
	//go insert mode
	//on_cmdEdit();

	//if(lstContacts)lstContacts->Dump();
	//if(lstProjects)lstProjects->Dump();
	//if(lstResources)lstResources->Dump();

	//assignments:

	QList<int> lstContactsAssign;
	QList<int> lstProjectAssign;
	QList<int> lstResourceAssign;

	if (lstContacts) //in format: <ID> or in NMRX assignemnet format: TVIEW_BUS_NMRX_RELATION_SELECT
	{
		int nIdx=lstContacts->getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nIdx<0)
			nIdx=lstContacts->getColumnIdx("BCNT_ID");
		if (nIdx<0 && lstContacts->getColumnCount()>0)
			nIdx=0;
		int nSize=lstContacts->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			lstContactsAssign.append(lstContacts->getDataRef(i,nIdx).toInt());
		}
	}
	if (lstProjects) //in format: <ID> or in NMRX assignemnet format: TVIEW_BUS_NMRX_RELATION_SELECT
	{
		int nIdx=lstProjects->getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nIdx<0)
			nIdx=lstProjects->getColumnIdx("BUSP_ID");;
		if (nIdx<0 && lstProjects->getColumnCount()>0)
			nIdx=0;
		int nSize=lstProjects->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			lstProjectAssign.append(lstProjects->getDataRef(i,nIdx).toInt());
		}
	}
	if (lstResources) //in format: <ID> or in NMRX assignemnet format: TVIEW_BUS_NMRX_RELATION_SELECT
	{
		int nIdx=lstResources->getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nIdx<0)
			nIdx=lstResources->getColumnIdx("BRES_ID");;
		if (nIdx<0 && lstResources->getColumnCount()>0)
			nIdx=0;
		int nSize=lstResources->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			lstResourceAssign.append(lstResources->getDataRef(i,nIdx).toInt());
		}
	}

	DbRecordSet lstNMRXPerson;
	lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXProject;
	lstNMRXProject.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXResources;
	lstNMRXResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	
	DbRecordSet lstNMRX_1;
	NMRXRelationWidget::CreateAssignmentData(lstNMRX_1,CE_COMM_ENTITY,BUS_CM_CONTACT,lstContactsAssign,tr("assigned"),0,BUS_CAL_INVITE);
	DbRecordSet lstNMRX_3;
	NMRXRelationWidget::CreateAssignmentData(lstNMRX_3,CE_COMM_ENTITY,BUS_PROJECT,lstProjectAssign,tr("assigned"));
	DbRecordSet lstNMRX_4;
	NMRXRelationWidget::CreateAssignmentData(lstNMRX_4,CE_COMM_ENTITY,BUS_CAL_RESOURCE,lstResourceAssign,tr("assigned"));

	lstNMRXPerson.merge(lstNMRX_1);
	lstNMRXProject.merge(lstNMRX_3);
	lstNMRXResources.merge(lstNMRX_4);


	//find nPartID:
	int nRowPart=m_lstDataCalEventPart.find("BCEP_ID",nPartID,true);
	if(nPartID<0)
		return;
	int nCommEntityID=m_lstDataCalEventPart.getDataRef(nRowPart,"BCEP_COMM_ENTITY_ID").toInt();


	DbRecordSet lstNMRXPerson_Old=m_lstDataCalEventPart.getDataRef(nRowPart,"NMRX_PERSON_LIST").value<DbRecordSet>();
	DbRecordSet lstNMRXProject_Old=m_lstDataCalEventPart.getDataRef(nRowPart,"NMRX_PROJECT_LIST").value<DbRecordSet>();
	DbRecordSet lstNMRXResources_Old=m_lstDataCalEventPart.getDataRef(nRowPart,"NMRX_RESOURCE_LIST").value<DbRecordSet>();


	//MP: if already exists in list then skip it: find only on key2/table2
	lstNMRXPerson.clearSelection();
	int nSize=lstNMRXPerson.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nRow=lstNMRXPerson_Old.find("BNMR_TABLE_KEY_ID_2",lstNMRXPerson.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt(),true);
		if(nRow>=0)
		{
			lstNMRXPerson.selectRow(i);
		}
	}
	lstNMRXPerson.deleteSelectedRows();

	lstNMRXProject.clearSelection();
	nSize=lstNMRXProject.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nRow=lstNMRXProject_Old.find("BNMR_TABLE_KEY_ID_2",lstNMRXProject.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt(),true);
		if(nRow>=0)
		{
			lstNMRXProject.selectRow(i);
		}
	}
	lstNMRXProject.deleteSelectedRows();

	lstNMRXResources.clearSelection();
	nSize=lstNMRXResources.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nRow=lstNMRXResources_Old.find("BNMR_TABLE_KEY_ID_2",lstNMRXResources.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toInt(),true);
		if(nRow>=0)
		{
			lstNMRXResources.selectRow(i);
		}
	}
	lstNMRXResources.deleteSelectedRows();

	lstNMRXPerson.setColValue("BNMR_TABLE_KEY_ID_1",nCommEntityID);
	lstNMRXProject.setColValue("BNMR_TABLE_KEY_ID_1",nCommEntityID);
	lstNMRXResources.setColValue("BNMR_TABLE_KEY_ID_1",nCommEntityID);


	if (lstNMRXPerson_Old.getRowCount()>0)
		lstNMRXPerson_Old.merge(lstNMRXPerson);
	else
		lstNMRXPerson_Old=lstNMRXPerson;

	if (lstNMRXProject_Old.getRowCount()>0)
		lstNMRXProject_Old.merge(lstNMRXProject);
	else
		lstNMRXProject_Old=lstNMRXProject;

	if (lstNMRXResources_Old.getRowCount()>0)
		lstNMRXResources_Old.merge(lstNMRXResources);
	else
		lstNMRXResources_Old=lstNMRXResources;


	m_lstDataCalEventPart.setData(nRowPart,"NMRX_PERSON_LIST",lstNMRXPerson_Old);
	m_lstDataCalEventPart.setData(nRowPart,"NMRX_PROJECT_LIST",lstNMRXProject_Old);
	m_lstDataCalEventPart.setData(nRowPart,"NMRX_RESOURCE_LIST",lstNMRXResources_Old);


	RefreshDisplay();

	//m_lstData.Dump();


}

//write all dirty cached data into m_lsts
void FUI_CalendarEvent::GetAllDataFromGUI()
{
	GetAllPartData(m_lstDataCalEventPart);


	DbRecordSet rowTask;
	if (ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt()>0)
	{
		ui.frameTask->GetDataFromGUI(); 
		m_lstDataCalEventPartTaskData=ui.frameTask->m_recTask;
	}
	else
	{
		m_lstDataCalEventPartTaskData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	}
}

void FUI_CalendarEvent::SetDataToWizard()
{
	//get all data from gui:
	GetAllDataFromGUI();

	//by RQ: get current option && row:
	DbRecordSet rowPart,rowCurrentOption;
	GetCurrentPartAndOption(rowPart,rowCurrentOption);

	DbRecordSet lstNMRXPerson=rowPart.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
	if (lstNMRXPerson.getColumnCount()==0)
		lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXProject=rowPart.getDataRef(0,"NMRX_PROJECT_LIST").value<DbRecordSet>();
	if (lstNMRXProject.getColumnCount()==0)
		lstNMRXProject.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	DbRecordSet lstNMRXResources=rowPart.getDataRef(0,"NMRX_RESOURCE_LIST").value<DbRecordSet>();
	if (lstNMRXResources.getColumnCount()==0)
		lstNMRXResources.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	//m_lstData.Dump();
	m_pWizard->SetData(m_lstData,rowPart,rowCurrentOption,lstNMRXPerson,lstNMRXProject,lstNMRXResources,m_lstDataCalEventPartTaskData);
}
void FUI_CalendarEvent::GetDataFromWizard()
{
	DbRecordSet rowEvent;
	DbRecordSet rowPart;
	DbRecordSet rowOptions;
	DbRecordSet lstNMRXPerson;
	DbRecordSet lstNMRXProject;
	DbRecordSet lstNMRXResources;

	m_pWizard->GetData(rowEvent,rowPart,rowOptions,lstNMRXPerson,lstNMRXProject,lstNMRXResources,m_lstDataCalEventPartTaskData);

	//rowEvent.Dump();

	//only two fields extract from main:
	m_lstData.setData(0,"BCEV_TEMPLATE_ID",rowEvent.getDataRef(0,"BCEV_TEMPLATE_ID"));
	m_lstData.setData(0,"BCEV_CATEGORY",rowEvent.getDataRef(0,"BCEV_CATEGORY").toString());

	ui.frameTask->m_recTask=m_lstDataCalEventPartTaskData;

	//rowPart.setData(0,"OPTION_LIST",lstOptions);
	rowPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
	rowPart.setData(0,"NMRX_PROJECT_LIST",lstNMRXProject);
	rowPart.setData(0,"NMRX_RESOURCE_LIST",lstNMRXResources);

	//prepare option row if first time: subject= title, from, to = event from, to:
	if (rowOptions.getDataRef(0,"BCOL_FROM").isNull())
		rowOptions.setData(0,"BCOL_FROM",m_lstData.getDataRef(0,"BCEV_FROM"));
	if (rowOptions.getDataRef(0,"BCOL_TO").isNull())
		rowOptions.setData(0,"BCOL_TO",m_lstData.getDataRef(0,"BCEV_TO"));
	if (rowOptions.getDataRef(0,"BCOL_SUBJECT").isNull())
		rowOptions.setData(0,"BCOL_SUBJECT",m_lstData.getDataRef(0,"BCEV_TITLE"));

	int nOptionRow=-1;
	int nPartIndex=m_pTab->currentIndex()+PART_INDEX_OFFSET;
	CalendarEvent_DetailPane *p=m_hshParts.value(nPartIndex,NULL);
	if (p)
	{
		nOptionRow=p->GetCurrentOptionRow();
	}

	if (nOptionRow>=0)
	{
		DbRecordSet rowCurrentPart=m_lstDataCalEventPart.getRow(m_pTab->currentIndex());
		DbRecordSet lstOption=rowCurrentPart.getDataRef(0,"OPTION_LIST").value<DbRecordSet>();
		lstOption.assignRow(nOptionRow,rowOptions);
		rowPart.setData(0,"OPTION_LIST",lstOption);
	}

	m_lstDataCalEventPart.assignRow(m_pTab->currentIndex(),rowPart);

	//m_lstData.Dump();

	RefreshDisplay();
}


void FUI_CalendarEvent::OnTemplateChanged(int nTemplate)
{
	if (m_nMode==MODE_INSERT || m_nMode==MODE_EDIT)
	{
		QString strMsg =QString(tr("Do you really want to discard all data and create calendar event from template?"));
		int nResult=QMessageBox::question(this,tr("Warning"),strMsg,tr("Yes"),tr("No"));
		if (nResult!=0)
			return;

		//MB: save from/to main and all nmrxs from original:
		//A) special protocol how to determine datetime from OF/OT and template times
		//B) all new nmrx-s are merged to parts with checkbox=BCEP_ASSIGN_NEW_CRP=1

		GetDataFromWizard();
		LoadFromTemplate(nTemplate);
		RefreshDisplay();

		if (m_bWizardMode)
			GoWizardMode(true);
	}
}

void FUI_CalendarEvent::LoadFromTemplate(int nTemplate)
{
	QDateTime dateFrom=m_lstData.getDataRef(0,"BCEV_FROM").toDateTime();
	QDateTime dateTo=m_lstData.getDataRef(0,"BCEV_TO").toDateTime();
	DbRecordSet lstDataCalEventPartPrev=m_lstDataCalEventPart;
	QString strTitle=m_lstData.getDataRef(0,"BCEV_TITLE").toString();
	
	m_bEnableWindowCloseOnCancelOrOK=false;

	on_cmdCancel();
	on_selectionChange(nTemplate);
	on_cmdCopy();
	SetAfterTemplate_DateTimes(dateFrom,dateTo);

	//lstDataCalEventPartPrev.Dump();
	SetAfterTemplate_NMRX(lstDataCalEventPartPrev);

	m_bEnableWindowCloseOnCancelOrOK=true;

	m_lstData.setData(0,"BCEV_FROM",dateFrom);
	m_lstData.setData(0,"BCEV_TO",dateTo);
	m_lstData.setData(0,"BCEV_TEMPLATE_FLAG",0);
	m_lstData.setData(0,"BCEV_TEMPLATE_ID",nTemplate);
	if (!strTitle.isEmpty())
		m_lstData.setData(0,"BCEV_TITLE",strTitle);
	
}

void SuperTabBar::mouseReleaseEvent ( QMouseEvent * event )
{
	QTabBar::mouseReleaseEvent ( event );
	emit TabClicked();
}

void FUI_CalendarEvent::OnTabClicked()
{
	if (ui.stackedWidgetParts->currentIndex()==0 || ui.stackedWidgetParts->currentIndex()==1)
	{
		OnTabCurrentChanged(m_pTab->currentIndex());
	}
}



//call after write:
bool FUI_CalendarEvent::SendInvitations()
{
	DbRecordSet lstInviteBySC;
	lstInviteBySC.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_SELECT));

	DbRecordSet lstInviteByEmail;
	lstInviteByEmail.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS));
	DbRecordSet lstInviteBySMS;
	lstInviteBySMS.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS));

	MainEntitySelectionController cachePerson;
	cachePerson.Initialize(ENTITY_BUS_PERSON);
	cachePerson.ReloadData();
	DbRecordSet lstPersons=*cachePerson.GetDataSource();

	int nDefaultEmailTemplateID=EmailHelper::GetDefaultEmailInviteTemplateForDefaultLanguage();
	CalendarHelperCore::CreateInviteRecordsFromCalendarData(m_lstData,m_lstDataCalEventPart,lstPersons,lstInviteBySC,lstInviteByEmail,lstInviteBySMS);

	Status err;
	if (lstInviteByEmail.getRowCount()>0)
	{
		_SERVER_CALL(BusCalendar->PrepareInviteRecordsForSendByEmail(err, lstInviteByEmail, nDefaultEmailTemplateID));
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}
	if (lstInviteBySMS.getRowCount()>0)
	{
		_SERVER_CALL(BusCalendar->PrepareInviteRecordsForSendByEmail(err, lstInviteBySMS, -1));
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
	}

	if (lstInviteBySC.getRowCount()>0 || lstInviteByEmail.getRowCount()>0 || lstInviteBySMS.getRowCount()>0)
	{
		bool bSendInvite=false;

		QString strTitle=tr("Please Confirm Sending Invitations:");
		QMessageBox box;
		QPushButton *btn1=box.addButton(tr("Send all unsent invitations"),QMessageBox::AcceptRole);
		QPushButton *btn2=box.addButton(tr("Return to Edit Calendar Data"),QMessageBox::AcceptRole);
		QPushButton *btn4=box.addButton(tr("Close"),QMessageBox::RejectRole);

		box.setIcon(QMessageBox::Question);
		box.setDefaultButton(btn1);
		box.setInformativeText(strTitle);
		box.setWindowTitle(strTitle);
		box.setMaximumHeight(55);
		int nResult=box.exec();
		if (nResult==0)
		{
			bSendInvite=true;
		}
		else if (nResult==1)
		{
			return false;
		}
		else
		{
			return true;
		}

		if (bSendInvite)
		{
			lstInviteBySC.Dump();
			lstInviteByEmail.Dump();
			lstInviteBySMS.Dump();


			Status err;
			if (lstInviteBySC.getRowCount()>0)
			{
				//->server call rows, event_id
				_SERVER_CALL(BusCalendar->SendInviteBySokrates(err,m_nEntityRecordID,lstInviteBySC))
					_CHK_ERR_NO_RET(err)
			}
			if (lstInviteByEmail.getRowCount()>0)
			{
				CalendarHelper::SendInvitations(err,lstInviteByEmail);
				_CHK_ERR_NO_RET(err)
			}
			if (lstInviteBySMS.getRowCount()>0)
			{
				CalendarHelper::SendInvitationsBySMS(err,lstInviteBySMS);
				_CHK_ERR_NO_RET(err)
			}
		}

	}

	return true;
}


void FUI_CalendarEvent::OnInviteDataChanged()
{
	//get all part data:
	GetAllPartData(m_lstDataCalEventPart);
	//no need to refresh display as it already done in nmrx..just preserver nwe data

}

void FUI_CalendarEvent::GoInviteAcceptMode(int nCalEventID,int nPartID,int nInviteID,int nContactId_ConfirmedBy, int nPersonID_ConfirmedBy, int nCalendarOwnerID,bool bNotifyOwner)
{
	on_selectionChange(nCalEventID);

	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_EDIT)->setVisible(false);
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_DELETE)->setVisible(false);
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_COPY)->setVisible(false);
	m_btnWizardMode->setVisible(false);
	m_btnSave->setVisible(false);
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_OK)->setText(tr("Accept"));
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_CANCEL)->setText(tr("Reject"));

	m_nInviteMode_InviteID=nInviteID;
	m_nInviteMode_nPartID=nPartID;
	m_nInviteMode_nContactId_ConfirmedBy=nContactId_ConfirmedBy;
	m_nInviteMode_nPersonID_ConfirmedBy=nPersonID_ConfirmedBy;
	m_nInviteMode_nOwnerID=nCalendarOwnerID;
	m_bInviteMode_bNotifyOwner=bNotifyOwner;

	m_bInviteAcceptMode=true;

	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_OK)->setEnabled(true);
	ui.btnButtonBar->GetButton(MainCommandBar::BUTTON_CANCEL)->setEnabled(true);
}

bool FUI_CalendarEvent::RejectInvite()
{
	Status err;
	_SERVER_CALL(BusCalendar->SetInviteStatus(err,m_nEntityRecordID,m_nInviteMode_nPartID,m_nInviteMode_InviteID,m_nInviteMode_nContactId_ConfirmedBy,m_nInviteMode_nPersonID_ConfirmedBy,GlobalConstants::STATUS_CAL_INVITE_REJECTED,m_nInviteMode_nOwnerID,m_bInviteMode_bNotifyOwner))
	_CHK_ERR_RET_BOOL(err)
	return true;
}
bool FUI_CalendarEvent::AcceptInvite()
{
	Status err;
	_SERVER_CALL(BusCalendar->SetInviteStatus(err,m_nEntityRecordID,m_nInviteMode_nPartID,m_nInviteMode_InviteID,m_nInviteMode_nContactId_ConfirmedBy,m_nInviteMode_nPersonID_ConfirmedBy,GlobalConstants::STATUS_CAL_INVITE_CONFIRMED,m_nInviteMode_nOwnerID,m_bInviteMode_bNotifyOwner))
	_CHK_ERR_RET_BOOL(err)

	return true;
}


//close event interceptor
void FUI_CalendarEvent::closeEvent(QCloseEvent *event)
{
	if (m_bBlockCloseEvent)
	{
		event->ignore();
		return;
	}

	if (m_bInitialized && !m_bInviteAcceptMode)on_cmdCancel(); 
	event->accept();
}

void FUI_CalendarEvent::OnDateTimeFromChanged(const QDateTime &partDateTimeFrom)
{
	if (!partDateTimeFrom.isValid() || m_lstData.getRowCount()!=1)
		return;

	QDateTime datEventFrom=m_lstData.getDataRef(0,"BCEV_FROM").toDateTime();

	if (partDateTimeFrom<datEventFrom || !datEventFrom.isValid())
	{
		ui.dateFrom->setDateTime(partDateTimeFrom);
		m_lstData.setData(0,"BCEV_FROM",partDateTimeFrom);
	}


	//->option date field is filled later on + part list is still in pane so this list is not actual
	/*
	if (!partDateTimeFrom.isValid() || m_lstData.getRowCount()!=1)
		return;

	QDateTime datEventFrom=m_lstData.getDataRef(0,"BCEV_FROM").toDateTime();
	QDateTime dateMin;
	int nSize=m_lstDataCalEventPart.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstOptions=m_lstDataCalEventPart.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
		int nSize=lstOptions.getRowCount();
		for(int k=0;k<nSize;k++)
		{
			if (dateMin.isValid())
			{
				if (lstOptions.getDataRef(k,"BCOL_FROM").toDateTime()<dateMin)
					dateMin=lstOptions.getDataRef(k,"BCOL_FROM").toDateTime();
			}
			else
				dateMin=lstOptions.getDataRef(k,"BCOL_FROM").toDateTime();

		}
	}

	if (datEventFrom>dateMin || !datEventFrom.isValid())
	{
		ui.dateFrom->setDateTime(dateMin);

	}
	*/

	
}

void FUI_CalendarEvent::OnDateTimeToChanged(const QDateTime &partDateTimeTo)
{
	if (!partDateTimeTo.isValid() || m_lstData.getRowCount()!=1)
		return;

	QDateTime datEventTo=m_lstData.getDataRef(0,"BCEV_TO").toDateTime();


	if (datEventTo<partDateTimeTo || !datEventTo.isValid())
	{
			ui.dateTo->setDateTime(partDateTimeTo);
			m_lstData.setData(0,"BCEV_TO",partDateTimeTo);
	}


	/*
	if (!partDateTimeTo.isValid() || m_lstData.getRowCount()!=1)
		return;

	QDateTime datEventTo=m_lstData.getDataRef(0,"BCEV_TO").toDateTime();
	QDateTime dateMax;
	int nSize=m_lstDataCalEventPart.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstOptions=m_lstDataCalEventPart.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
		int nSize2=lstOptions.getRowCount();
		for(int k=0;k<nSize2;k++)
		{
			if (dateMax.isValid())
			{
				if (lstOptions.getDataRef(k,"BCOL_TO").toDateTime()>dateMax)
					dateMax=lstOptions.getDataRef(k,"BCOL_TO").toDateTime();
			}
			else
				dateMax=lstOptions.getDataRef(k,"BCOL_TO").toDateTime();

		}
	}

	if (datEventTo>dateMax  || !datEventTo.isValid())
	{
		ui.dateTo->setDateTime(dateMax);
	}

	*/
}


void FUI_CalendarEvent::OnDateFromFocusOut()
{
	//qDebug()<<m_lstData.getDataRef(0,"BCEV_TO").toDateTime();
	if (!m_lstData.getDataRef(0,"BCEV_TO").toDateTime().isValid())
	{
		QDateTime datEventFrom=m_lstData.getDataRef(0,"BCEV_FROM").toDateTime();
		//qDebug()<<datEventFrom;
		datEventFrom=datEventFrom.addSecs(60*60);
		ui.dateTo->setDateTime(datEventFrom);
	}

	if (m_bWizardMode)
	{
		m_pWizard->SetCurrentOptionDates(ui.dateFrom->dateTime(),ui.dateTo->dateTime(),true);
	}
}

//if non wizard mode and option from/to is empty: set to dates
void FUI_CalendarEvent::OnDateToFocusOut()
{
	if (!m_bWizardMode)
	{
		int nPartIndex=m_pTab->currentIndex()+PART_INDEX_OFFSET;
		CalendarEvent_DetailPane *p=m_hshParts.value(nPartIndex,NULL);
		if (p)
			p->SetCurrentOptionDates(ui.dateFrom->dateTime(),ui.dateTo->dateTime());
	}
	else
	{
		m_pWizard->SetCurrentOptionDates(ui.dateFrom->dateTime(),ui.dateTo->dateTime(),true);
	}
}

void FUI_CalendarEvent::ShowPart(int nCalEvPartID, int nCalEvOptionID)
{
	//switch tabs based on part ID:

	if (m_bWizardMode)
		GoWizardMode(false);

	int nTabIdx=0;
	CalendarEvent_DetailPane* part=NULL;
	//refresh all parts:
	QHashIterator <int,CalendarEvent_DetailPane*> i(m_hshParts);
	while (i.hasNext()) 
	{
		i.next();
		if (i.value()->GetPartID()==nCalEvPartID)
		{
			nTabIdx=i.key()-PART_INDEX_OFFSET;
			part=i.value();
		}
	}	

	//switch to nTabIdx:
	m_pTab->setCurrentIndex(nTabIdx);

	if (nCalEvOptionID!=-1)
	{
		//switch to the option selected:
		part->SetOptionRow(nCalEvOptionID);
	}

}

void FUI_CalendarEvent::OnTitleChanged()
{
	if (m_bWizardMode)
	{
		m_pWizard->RefreshTitle(ui.txtTitle->text());
	}
	else
	{
		QHashIterator <int,CalendarEvent_DetailPane*> i(m_hshParts);
		while (i.hasNext()) 
		{
			i.next();
			i.value()->RefreshTitle(ui.txtTitle->text());
		}
	}
}

//RS: Requirement Specification CalEv Templates 
void FUI_CalendarEvent::SetAfterTemplate_DateTimes(QDateTime dateFrom,QDateTime  dateTo)
{
	//both new from / to times must be valid
	if (!dateFrom.isValid() || !dateTo.isValid() || m_lstData.getRowCount()==0 || m_lstDataCalEventPart.getRowCount()==0)
		return;
	
	int nNewDiff=dateFrom.secsTo(dateTo);

	//1) Only 1 part = COPY
	if (m_lstDataCalEventPart.getRowCount()==1)
	{
		DbRecordSet lstOption=m_lstDataCalEventPart.getDataRef(0,"OPTION_LIST").value<DbRecordSet>();
		if (lstOption.getRowCount()>0)
		{
			lstOption.setColValue("BCOL_FROM",dateFrom);
			lstOption.setColValue("BCOL_TO",dateTo);
			m_lstDataCalEventPart.setData(0,"OPTION_LIST",lstOption);
			m_lstData.setData(0,"BCEV_FROM",dateFrom);
			m_lstData.setData(0,"BCEV_TO",dateTo);
		}
		return;
	}
	else
	{
		//calculate sums:
		int nCnt_Parts=m_lstDataCalEventPart.getRowCount();
		int nCnt_FullParts=0;
		int nCnt_EmptyParts=0;
		int nSumSecondsTotal_FullParts=0;

		for(int i=0;i<nCnt_Parts;i++)
		{
			DbRecordSet lstOption=m_lstDataCalEventPart.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
			if (lstOption.getDataRef(0,"BCOL_FROM").toDateTime().isValid() && lstOption.getDataRef(0,"BCOL_TO").toDateTime().isValid())
			{
				nCnt_FullParts++;
				QDateTime datTFrom = lstOption.getDataRef(0,"BCOL_FROM").toDateTime();
				QDateTime datTTo = lstOption.getDataRef(0,"BCOL_TO").toDateTime();
				if (datTFrom>datTTo) //security check
				{Q_ASSERT(false);	return;}
				nSumSecondsTotal_FullParts+=datTFrom.secsTo(datTTo);
			}
			else
			{
				nCnt_EmptyParts++;
			}
		}

		if (nCnt_EmptyParts==nCnt_Parts) //if TT and TF for each part are both empty (if only one filled then ignore) then symmetrically divide
		{
			int nSecondsToAdd=nNewDiff/nCnt_Parts;
			QDateTime datStart=dateFrom;
			for(int i=0;i<nCnt_Parts;i++)
			{
				DbRecordSet lstOption=m_lstDataCalEventPart.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
				lstOption.setData(0,"BCOL_FROM",datStart);
				datStart=datStart.addSecs(nSecondsToAdd);
				lstOption.setData(0,"BCOL_TO",datStart);
				if (i==(nCnt_Parts-1))
				{
					lstOption.setData(0,"BCOL_TO",dateTo);
				}
				m_lstDataCalEventPart.setData(i,"OPTION_LIST",lstOption);
				m_lstDataCalEventPart.setData(i,"PART_FROM",lstOption.getDataRef(0,"BCOL_FROM").toDateTime());
				m_lstDataCalEventPart.setData(i,"PART_TO",lstOption.getDataRef(0,"BCOL_TO").toDateTime());
			}
		}
		else if (nCnt_EmptyParts==0) //if TT and TF for each part are SET then divide symmetrically by coefficient
		{
			double dKoeficijent=(double)nNewDiff/(double)nSumSecondsTotal_FullParts;
			QDateTime datStart=dateFrom;
			for(int i=0;i<nCnt_Parts;i++)
			{
				DbRecordSet lstOption=m_lstDataCalEventPart.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
				QDateTime datTFrom = lstOption.getDataRef(0,"BCOL_FROM").toDateTime();
				QDateTime datTTo = lstOption.getDataRef(0,"BCOL_TO").toDateTime();
				int nDiff=datTFrom.secsTo(datTTo);
				lstOption.setData(0,"BCOL_FROM",datStart);

				int nSecondsToAdd=nDiff*dKoeficijent;
				datStart=datStart.addSecs(nSecondsToAdd);

				lstOption.setData(0,"BCOL_TO",datStart);
				if (i==(nCnt_Parts-1))
				{
					lstOption.setData(0,"BCOL_TO",dateTo);
				}
				m_lstDataCalEventPart.setData(i,"OPTION_LIST",lstOption);
				m_lstDataCalEventPart.setData(i,"PART_FROM",lstOption.getDataRef(0,"BCOL_FROM").toDateTime());
				m_lstDataCalEventPart.setData(i,"PART_TO",lstOption.getDataRef(0,"BCOL_TO").toDateTime());
			}
		}
		else  //we have mix zone: some parts are empty, and some are set: rule: first set those with set times and then try to insert/stretch empty ones
		{

			//MB:
			//a) sum all full parts
			//b) if sum <nNewDiff then all empty parts = (nNewDiff-sum)(no of empty parts)
			//c) then: add as is: from start if empty add empty size, if full, add difference
			//d) if sum >= nNewDiff then all empty parts = (Of-Ot)/(no of total parts)
			//e) all full parts must be shrink: koef=(nNewDiff-sumofemptyparts) full part time= (orig size/ sumof_origfullparts) * koef
			//f) e.g.: empty, empty, full=1h, full=2h, OF-OT=2h: empty_part=30min, koef=(120-60)=60, full_1=1/3 * 60=20, full_2=2/3 * 60=40.

			int nDefaultEmptyPartSize=0;
			if (nSumSecondsTotal_FullParts<nNewDiff)
			{
				nDefaultEmptyPartSize=(nNewDiff-nSumSecondsTotal_FullParts)/nCnt_EmptyParts;
				QDateTime datCurrentDate=dateFrom;

				for(int i=0;i<nCnt_Parts;i++)
				{
					DbRecordSet lstOption=m_lstDataCalEventPart.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
					QDateTime datTFrom = lstOption.getDataRef(0,"BCOL_FROM").toDateTime();
					QDateTime datTTo = lstOption.getDataRef(0,"BCOL_TO").toDateTime();

					int nSecondsDiff=nDefaultEmptyPartSize; //if empty part
					if (lstOption.getDataRef(0,"BCOL_FROM").toDateTime().isValid() && lstOption.getDataRef(0,"BCOL_TO").toDateTime().isValid())
						nSecondsDiff=datTFrom.secsTo(datTTo); //if full part

					datTFrom=datCurrentDate;
					datTTo=datTFrom.addSecs(nSecondsDiff);
					datCurrentDate=datTTo;
					lstOption.setData(0,"BCOL_FROM",datTFrom);
					lstOption.setData(0,"BCOL_TO",datTTo);
					qDebug()<<datTFrom;
					qDebug()<<datTTo;
					if (i==(nCnt_Parts-1))
					{
						lstOption.setData(0,"BCOL_TO",dateTo);
					}
					m_lstDataCalEventPart.setData(i,"OPTION_LIST",lstOption);
					m_lstDataCalEventPart.setData(i,"PART_FROM",lstOption.getDataRef(0,"BCOL_FROM").toDateTime());
					m_lstDataCalEventPart.setData(i,"PART_TO",lstOption.getDataRef(0,"BCOL_TO").toDateTime());
				}
			}
			else
			{
				nDefaultEmptyPartSize=nNewDiff/nCnt_Parts;
				int nSumOfSpaceAvailableForFullParts=nNewDiff-nCnt_EmptyParts*nDefaultEmptyPartSize;
				if (nSumOfSpaceAvailableForFullParts<=0) //security check
				{Q_ASSERT(false);	return;}

				QDateTime datCurrentDate=dateFrom;

				for(int i=0;i<nCnt_Parts;i++)
				{
					DbRecordSet lstOption=m_lstDataCalEventPart.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
					QDateTime datTFrom = lstOption.getDataRef(0,"BCOL_FROM").toDateTime();
					QDateTime datTTo = lstOption.getDataRef(0,"BCOL_TO").toDateTime();

					int nSecondsDiff=nDefaultEmptyPartSize; //if empty part
					if (lstOption.getDataRef(0,"BCOL_FROM").toDateTime().isValid() && lstOption.getDataRef(0,"BCOL_TO").toDateTime().isValid())
					{
						nSecondsDiff=datTFrom.secsTo(datTTo); //if full part
						nSecondsDiff=((double)nSecondsDiff/nSumSecondsTotal_FullParts)*((double)nSumOfSpaceAvailableForFullParts);
					}

					datTFrom=datCurrentDate;
					datTTo=datTFrom.addSecs(nSecondsDiff);
					datCurrentDate=datTTo;
					lstOption.setData(0,"BCOL_FROM",datTFrom);
					lstOption.setData(0,"BCOL_TO",datTTo);
					qDebug()<<datTFrom;
					qDebug()<<datTTo;

					if (i==(nCnt_Parts-1))
					{
						lstOption.setData(0,"BCOL_TO",dateTo);
					}
					m_lstDataCalEventPart.setData(i,"OPTION_LIST",lstOption);
					m_lstDataCalEventPart.setData(i,"PART_FROM",lstOption.getDataRef(0,"BCOL_FROM").toDateTime());
					m_lstDataCalEventPart.setData(i,"PART_TO",lstOption.getDataRef(0,"BCOL_TO").toDateTime());
				}

			}

		}

	}

}

//RS: Requirement Specification CalEv Templates
void FUI_CalendarEvent::SetAfterTemplate_NMRX(DbRecordSet &lstDataCalEventPartPrev)
{
	if (lstDataCalEventPartPrev.getRowCount()<1) //must be 1
		return;

	//MainEntitySelectionController cachePerson;
	//cachePerson.Initialize(ENTITY_BUS_PERSON);
	//cachePerson.ReloadData();

	int nSize=m_lstDataCalEventPart.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_lstDataCalEventPart.getDataRef(i,"BCEP_ASSIGN_NEW_CRP").toInt()>0)
		{
			//NMR: clear all invite and key1 fields:
			DbRecordSet lstNMRXPerson=m_lstDataCalEventPart.getDataRef(i,"NMRX_PERSON_LIST").value<DbRecordSet>();
			DbRecordSet lstNMRXPerson_New=lstDataCalEventPartPrev.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
			lstNMRXPerson.merge(lstNMRXPerson_New);
			//lstNMRXPerson.Dump();
			//m_lstDataCalEventPart.setData(i,"NMRX_PERSON_LIST",lstNMRXPerson);

			DbRecordSet lstNMRXProject=m_lstDataCalEventPart.getDataRef(i,"NMRX_PROJECT_LIST").value<DbRecordSet>();
			DbRecordSet lstNMRXProject_New=lstDataCalEventPartPrev.getDataRef(0,"NMRX_PROJECT_LIST").value<DbRecordSet>();
			lstNMRXProject.merge(lstNMRXProject_New);
			//m_lstDataCalEventPart.setData(i,"NMRX_PROJECT_LIST",lstNMRXProject);

			DbRecordSet lstNMRXResources=m_lstDataCalEventPart.getDataRef(i,"NMRX_RESOURCE_LIST").value<DbRecordSet>();
			DbRecordSet lstNMRXResources_New=lstDataCalEventPartPrev.getDataRef(0,"NMRX_RESOURCE_LIST").value<DbRecordSet>();
			lstNMRXResources.merge(lstNMRXResources_New);
			//m_lstDataCalEventPart.setData(i,"NMRX_RESOURCE_LIST",lstNMRXResources);

			//By her majesty MB: filter duplicate assignments:
			NMRXRelationWidget::TestListForDuplicateAssignments(BUS_CM_CONTACT,lstNMRXPerson);
			NMRXRelationWidget::TestListForDuplicateAssignments(BUS_PROJECT,lstNMRXProject);
			NMRXRelationWidget::TestListForDuplicateAssignments(BUS_CAL_RESOURCE,lstNMRXResources);

			//lstNMRXPerson.Dump();

			m_lstDataCalEventPart.setData(i,"NMRX_PERSON_LIST",lstNMRXPerson);
			m_lstDataCalEventPart.setData(i,"NMRX_PROJECT_LIST",lstNMRXProject);
			m_lstDataCalEventPart.setData(i,"NMRX_RESOURCE_LIST",lstNMRXResources);

		}
	}
}



void FUI_CalendarEvent::on_ckbIsTemplate_stateChanged(int value)
{
	QHashIterator <int,CalendarEvent_DetailPane*> i(m_hshParts);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->ShowCkbNewCRP(value==Qt::Checked);
	}

}

void FUI_CalendarEvent::prepareCacheRecord(DbRecordSet* recNewData)
{
	if (recNewData->getRowCount()>0)
	{
		recNewData->addColumn(DbRecordSet::GetVariantType(),"PARTS");
		recNewData->setData(0,recNewData->getColumnCount()-1,m_lstDataCalEventPart);
		//m_lstDataCalEventPart.Dump();

		DbRecordSet lstCalData;
		NMRXManager::ConvertToAssignmentDataFromCalendarPartData(lstCalData,m_lstDataCalEventPart);

		recNewData->addColumn(DbRecordSet::GetVariantType(),"ASSIGNMENTS");
		recNewData->setData(0,recNewData->getColumnCount()-1,lstCalData);
	
	}
}