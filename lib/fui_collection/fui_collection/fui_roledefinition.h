#ifndef FUI_ROLEDEFINITION_H
#define FUI_ROLEDEFINITION_H

#include <QWidget>
#include "ui_fui_roledefinition.h"

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "fuibase.h"
#include "gui_core/gui_core/property_browser/qttreepropertybrowser.h"
#include "bus_core/bus_core/accessrightsorganizer.h"

class FUI_RoleDefinition : public FuiBase
{
    Q_OBJECT

public:
    FUI_RoleDefinition(QWidget *parent = 0);
    ~FUI_RoleDefinition();

private:
	Ui::FUI_RoleDefinitionClass ui;

	void			InitContextMenu();
	void			InitializeButtons();
	void			InitializeTreeWidget();
	void			InitializeARSWidget();
	void			RenameRole(int RoleID, QString RoleName, QString RoleDescr, int RoleType);
	void			InsertRole(QString RoleName, QString RoleDescr, int RoleType);
	void			InsertARSToRole(int RoleID, int ARSCode);
	void			DeleteARS(QTreeWidgetItem *Item, QTreeWidgetItem *ParentItem);
	void			DeleteRole(QTreeWidgetItem *Item);
	void			CreateAccessRightsRecordSetForInsertedARS(int RoleID, int ARSID);
	bool			CheckIsARSinRole(int RoleID, int ARSID);
	void			SetRoleType(QString &RoleType);
	void			SaveChangedAccessRights();
	void			ReloadData();
	bool			LockResource(int RoleID);
	void			UnLockResources();

	QHash<int,QTreeWidgetItem*> m_RoleItemsHash;		//< Role row ID to item in tree hash.
	Status			m_Status;
	DbRecordSet		m_recCORE_ROLE;						//< Role tree roles recordset.
	DbRecordSet		m_recCORE_ACCRSETROLE;				//< CORE_ACCRSETROLE recordset (used to check is some AR set already inserted in some role).
	DbRecordSet		m_recCORE_ACCRSET;					//< Access rights (for access rights tree) recordset. m_AccessRightSetsRecordSet
	DbRecordSet		m_recCORE_ACCESSRIGHTS;				//< Access rights (for all roles) recordset (for property browser). m_ARRecordSet
	//Changes lists and hashes.
	//Roles.
	QList<int>		m_lstInsertedRoles;					//< Inserted roles row ID list.
	QList<int>		m_lstDeletedRoles;					//< Deleted roles row ID list.
	QList<int>		m_lstRenamedRoles;					//< Renamed roles row ID list.
	QHash<int, int> m_hshTmpToInsertedRoleID;			//< Map temporary role ID to real one (got after insertion).
	//Access Right Sets.
	DbRecordSet		m_recInsertedARSToRole;				//< New access rights set added to role.
	QMultiHash<int, int>	m_hshDeletedARSFromRole;			//< Deleted access rights set from role (first Role ID, then ARSID).
	//Access Rights.
	QList<int>		m_lstChangedAccessRight;			//< Changed access rights Row ID list.
	//Lock resource list.
	QHash<int,QString>	m_hshLockingResourceHash;		//< Role id to lock resource hash.
	//Actions for context menu.
	QList<QAction*> m_lstARSAction,
					m_lstRoleAction,
					m_lstTreeAction;
	bool			m_bEditMode;

	//B.T. for translation:
	void			TranslateAR(DbRecordSet &lstData);
	void			TranslateARSet(DbRecordSet &lstData);
	AccessRightsOrganizer	m_AROrganizer;

private slots:

	void			on_editPushButton_clicked();
	void			on_AssignToRolesPushButton_clicked();
	void			on_CancelPushButton_clicked();
	void			on_DeletePushButton_clicked();
	void			on_InsertRolePushButton_clicked();
	void			on_RenameRole();
	void			on_SavePushButton_clicked();
	void			RoleSelectionChanged();
	void			ARSSelectionChanged();
	void			ARSValueChanged(int RowID, QVariant PropertyValue, QString  PropertyStrValue);
	void			ChangeSaveState();
	void			on_ARSDropped(QTreeWidgetItem *parent, int index);
	void			on_TreeFocusIn();
	void			on_TreeFocusOut();

};

#endif // FUI_ROLEDEFINITION_H
