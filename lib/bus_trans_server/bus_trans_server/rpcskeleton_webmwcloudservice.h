#ifndef RPCSKELETON_WEBMWCLOUDSERVICE_H__
#define RPCSKELETON_WEBMWCLOUDSERVICE_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_webmwcloudservice.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_WebMWCloudService : public RpcSkeleton
{

typedef void (RpcSkeleton_WebMWCloudService::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );

public:
	RpcSkeleton_WebMWCloudService(BusinessServiceSet *pHandler, int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void CreateUser(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ChangeUserPassword(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void AddUserEmailAccountIn(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void AddUserEmailAccountOut(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void RemoveUserEmailAccount(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void DeleteEmail(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void DeleteMultipleEmails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void BlackListEmailAddress(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetEmailAddressBlackList(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void StoreEmail(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetUsersEmailCnt(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetUnreadEmailsCount(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void SendUserErrorPushMessage(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void RegisterPushNotificationID(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ActivatePushNotification(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void SetNewEmailsPushNotifications(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void SetEmailAccountPushNotifications(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void SendPushNotification(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void MoveEmail(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void EnableUserAccount(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void DeleteUser(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	Interface_WebMWCloudService *m_Handler; //< pointer to your BO

	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_WEBMWCLOUDSERVICE_H__
