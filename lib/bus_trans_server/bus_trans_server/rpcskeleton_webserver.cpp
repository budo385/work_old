#include "rpcskeleton_webserver.h"
RpcSkeleton_WebServer::RpcSkeleton_WebServer(BusinessServiceSet *pHandler,int RPCType):
RpcSkeleton(RPCType)
{
	m_Handler=pHandler->WebServer; //store handler to actual business object
	SetNameSpace("server"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("POST/server/login",&RpcSkeleton_WebServer::Login);
	mFunctList.insert("POST/server/logout",&RpcSkeleton_WebServer::Logout);
	mFunctList.insert("POST/server/changepassword",&RpcSkeleton_WebServer::ChangePassword);
	mFunctList.insert("GET/server/reloadwebpages",&RpcSkeleton_WebServer::ReloadWebPages);
	mFunctList.insert("GET/server/getservertime",&RpcSkeleton_WebServer::GetServerTime);
	mFunctList.insert("POST/server/setnexttesttime",&RpcSkeleton_WebServer::SetNextTestTime);
}

bool RpcSkeleton_WebServer::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;

	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) 
		return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);

		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);

			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

return true;
}

void RpcSkeleton_WebServer::Login(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=9)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  RetOut_strSessionID;
	int  Ret_nContactID;
	int  Ret_nPersonID;
	QString  strUserName;
	QString  strAuthToken;
	QString  strClientNonce;
	int  nProgCode;
	QString  strProgVer;
	QString  strClientID;
	QString  strPlatform;
	int  nClientTimeZoneOffsetMinutes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&RetOut_strSessionID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strUserName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strAuthToken,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&strClientNonce,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nProgCode,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&strProgVer,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&strClientID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&strPlatform,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&nClientTimeZoneOffsetMinutes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->Login(err,RetOut_strSessionID,Ret_nContactID,Ret_nPersonID,strUserName,strAuthToken,strClientNonce,nProgCode,strProgVer,strClientID,strPlatform,nClientTimeZoneOffsetMinutes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_strSessionID,"Session");
	rpc.msg_out->AddParameter(&Ret_nContactID,"Ret_nContactID");
	rpc.msg_out->AddParameter(&Ret_nPersonID,"Ret_nPersonID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebServer::Logout(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strSessionID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&strSessionID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->Logout(err,strSessionID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebServer::ChangePassword(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=6)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  RetOut_strSessionID;
	QString  strUsername;
	QString  strAuthToken;
	QString  strClientNonce;
	QByteArray  newPass;
	int  nClientTimeZoneOffsetMinutes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&RetOut_strSessionID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strUsername,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strAuthToken,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&strClientNonce,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&newPass,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&nClientTimeZoneOffsetMinutes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ChangePassword(err,RetOut_strSessionID,strUsername,strAuthToken,strClientNonce,newPass,nClientTimeZoneOffsetMinutes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_strSessionID,"Session");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebServer::ReloadWebPages(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->ReloadWebPages(err);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebServer::GetServerTime(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	QDateTime  Ret_datCurrentServerTime;
	QDateTime  Ret_datTimeNextTest;
	int  Ret_nActiveClientSocketCnt;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetServerTime(err,Ret_datCurrentServerTime,Ret_datTimeNextTest,Ret_nActiveClientSocketCnt);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_datCurrentServerTime,"Ret_datCurrentServerTime");
	rpc.msg_out->AddParameter(&Ret_datTimeNextTest,"Ret_datTimeNextTest");
	rpc.msg_out->AddParameter(&Ret_nActiveClientSocketCnt,"Ret_nActiveClientSocketCnt");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebServer::SetNextTestTime(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QDateTime  datSetTimeNextTest;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&datSetTimeNextTest,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->SetNextTestTime(err,datSetTimeNextTest);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

