#ifndef RPCSKELETON_WEBSERVER_H__
#define RPCSKELETON_WEBSERVER_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_webserver.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_WebServer : public RpcSkeleton
{

typedef void (RpcSkeleton_WebServer::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );

public:
	RpcSkeleton_WebServer(BusinessServiceSet *pHandler, int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Login(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void Logout(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ChangePassword(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReloadWebPages(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void GetServerTime(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void SetNextTestTime(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);

	Interface_WebServer *m_Handler; //< pointer to your BO

	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_WEBSERVER_H__
