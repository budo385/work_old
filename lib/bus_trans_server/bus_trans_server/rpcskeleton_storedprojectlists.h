#ifndef RPCSKELETON_STOREDPROJECTLISTS_H__
#define RPCSKELETON_STOREDPROJECTLISTS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_storedprojectlists.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_StoredProjectLists : public RpcSkeleton
{

typedef void (RpcSkeleton_StoredProjectLists::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_StoredProjectLists(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void GetListNames(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteList(Status &err,RpcSkeletonMessageHandler &rpc);
	void CreateList(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetListData(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetListData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetListDataLevelByLevel(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_StoredProjectLists *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_STOREDPROJECTLISTS_H__
