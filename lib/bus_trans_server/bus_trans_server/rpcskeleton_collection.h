#ifndef RPCSKELETONCOLLECTION_H
#define RPCSKELETONCOLLECTION_H


//-----------------------------------------------------------
//Holds header for all RPC skeletons, register your stub here:
//-----------------------------------------------------------

#include "rpcskeleton_userlogon.h"
#include "rpcskeleton_modulelicense.h"
#include "rpcskeleton_buseventlog.h"
#include "rpcskeleton_accessrights.h"
#include "rpcskeleton_mainentityselector.h"
#include "rpcskeleton_coreservices.h"
#include "rpcskeleton_buscontact.h"
#include "rpcskeleton_reports.h"
#include "rpcskeleton_voicecallcenter.h"
#include "rpcskeleton_busemail.h"
#include "rpcskeleton_busaddressschemas.h"
#include "rpcskeleton_clientsimpleorm.h"
#include "rpcskeleton_busimport.h"
#include "rpcskeleton_storedprojectlists.h"
#include "rpcskeleton_busgridviews.h"
#include "rpcskeleton_busnmrx.h"
#include "rpcskeleton_busgrouptree.h"
#include "rpcskeleton_busdocuments.h"
#include "rpcskeleton_buscommunication.h"
#include "rpcskeleton_servercontrol.h"
#include "rpcskeleton_busproject.h"
#include "rpcskeleton_busperson.h"
#include "rpcskeleton_bussms.h"
#include "rpcskeleton_buscalendar.h"
#include "rpcskeleton_buscustomfields.h"

#endif //RPCSKELETONCOLLECTION_H
