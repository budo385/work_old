#ifndef RPCSKELETON_BUSNMRX_H__
#define RPCSKELETON_BUSNMRX_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busnmrx.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusNMRX : public RpcSkeleton
{

typedef void (RpcSkeleton_BusNMRX::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusNMRX(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadOnePair(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadMultiPairs(Status &err,RpcSkeletonMessageHandler &rpc);
	void Write(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteSimple(Status &err,RpcSkeletonMessageHandler &rpc);
	void Delete(Status &err,RpcSkeletonMessageHandler &rpc);
	void Lock(Status &err,RpcSkeletonMessageHandler &rpc);
	void LockMulti(Status &err,RpcSkeletonMessageHandler &rpc);
	void Unlock(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteRelations(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteCEContactLink(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteCEProjectLink(Status &err,RpcSkeletonMessageHandler &rpc);
	void CreateAssignmentData(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void LockRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void UnlockRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckRole(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusNMRX *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSNMRX_H__
