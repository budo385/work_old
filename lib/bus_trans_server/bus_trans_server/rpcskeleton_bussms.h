#ifndef RPCSKELETON_BUSSMS_H__
#define RPCSKELETON_BUSSMS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_bussms.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusSms : public RpcSkeleton
{

typedef void (RpcSkeleton_BusSms::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusSms(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadSms(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteSms(Status &err,RpcSkeletonMessageHandler &rpc);
	void ListSmsMessages(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteSmsMessages(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusSms *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSSMS_H__
