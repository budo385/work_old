#include "webservicerpcdispatcher.h"

/*!
	Constructor: Makes instances of all skeleton objects, puts them in set.
	\param pHandler is handler to Business object set
	\param RPCType is constant defining what RPC protocol to use: 
	RPC_PROTOCOL_TYPE_XML_RPC, RPC_PROTOCOL_TYPE_SOAP, etc..defined in rpcprotocol.h
*/

WebServiceRpcDispatcher::WebServiceRpcDispatcher(BusinessServiceSet *pHandler)
:RpcSkeletonDispatcher(RPC_PROTOCOL_TYPE_REST)
{
	//---------------------------------------------------
	//	Make instances of each subskeleton object, init to point to its parent object and store in list
	//---------------------------------------------------

	WebServer=new RpcSkeleton_WebServer(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebServer);
	WebContacts=new RpcSkeleton_WebContacts(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebContacts);
	WebDocuments=new RpcSkeleton_WebDocuments(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebDocuments);
	WebEmails=new RpcSkeleton_WebEmails(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebEmails);
	WebProjects=new RpcSkeleton_WebProjects(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebProjects);
	WebCalendars=new RpcSkeleton_WebCalendars(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebCalendars);
	WebSessionData=new RpcSkeleton_WebSessionData(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebSessionData);
	WebCommunication=new RpcSkeleton_WebCommunication(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebCommunication);
	SpcSession=new RpcSkeleton_SpcSession(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(SpcSession);
	SpcBusinessFigures=new RpcSkeleton_SpcBusinessFigures(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(SpcBusinessFigures);
	WebMWCloudService=new RpcSkeleton_WebMWCloudService(pHandler,RPC_PROTOCOL_TYPE_REST);m_SkeletonList.append(WebMWCloudService);
			
}


void WebServiceRpcDispatcher::SetWebServiceURL(QString strURL)
{
	WebServer->SetWebServiceURL(strURL);
	WebContacts->SetWebServiceURL(strURL);
	WebDocuments->SetWebServiceURL(strURL);
	WebEmails->SetWebServiceURL(strURL);
	WebProjects->SetWebServiceURL(strURL);
	WebCalendars->SetWebServiceURL(strURL);
	WebSessionData->SetWebServiceURL(strURL);
	WebCommunication->SetWebServiceURL(strURL);
	SpcSession->SetWebServiceURL(strURL);
	SpcBusinessFigures->SetWebServiceURL(strURL);
	WebMWCloudService->SetWebServiceURL(strURL);

}


