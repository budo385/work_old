#ifndef RPCHTTPSERVER_H
#define RPCHTTPSERVER_H

#include <QtCore>
#include "common/common/status.h"
#include "trans/trans/httpcontext.h"
#include "trans/trans/httprequest.h"
#include "trans/trans/httpresponse.h"
#include "trans/trans/httphandler.h"
#include "trans/trans/rpcprotocol.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "trans/trans/rpchttpheader.h"
#include "bus_core/bus_core/usersessionmanager.h"


/*!
    \class RpcHTTPServer
    \brief Server handler for RPC messages over HTTP
    \ingroup Bus_Trans_Server

	Accepts incoming HTTP request, parses rpc message header, check session, dispatch to the skeleton set.
	Use:
	- make instance and register it to the httpserver
	
*/
class RpcHTTPServer : public HTTPHandler, protected RpcProtocol
{

public:
	RpcHTTPServer(RpcSkeleton *pServerSkeletonSet,UserSessionManager* pSessionManager, int pNumRPCProtocolType);
	
	void HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);
	//void ClientSocketInUse(HTTPContext &ctx);


protected:

	void ErrorResponseHandler(Status &pStatus,RpcSkeletonMessageHandler &rpc);
	void SetResponseHeader(HTTPResponse &response);

	RpcSkeleton *m_ServerSkeletonSet;
	UserSessionManager* m_SessionManager;
	RpcHttpHeader m_HttpHeader;
};


#endif //RPCHTTPSERVER_H


