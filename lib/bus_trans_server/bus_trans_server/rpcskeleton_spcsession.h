#ifndef RPCSKELETON_SPCSESSION_H__
#define RPCSKELETON_SPCSESSION_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_spcsession.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_SpcSession : public RpcSkeleton
{

typedef void (RpcSkeleton_SpcSession::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );

public:
	RpcSkeleton_SpcSession(BusinessServiceSet *pHandler, int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Login(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void Logout(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);

	Interface_SpcSession *m_Handler; //< pointer to your BO

	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_SPCSESSION_H__
