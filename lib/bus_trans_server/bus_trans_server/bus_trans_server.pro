# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

TEMPLATE = lib
TARGET = bus_trans_server
DESTDIR = ../../../exe/appserver/Debug
QT += core sql network
CONFIG += staticlib release
DEFINES += QT_SQL_LIB QT_NETWORK_LIB BUS_TRANS_SERVER_LIB
INCLUDEPATH += ./GeneratedFiles \
    ./GeneratedFiles/Debug \
    ./../../../lib \
    . \
    ./GeneratedFiles/$(Configuration)
LIBS += -L"."
DEPENDPATH += .
MOC_DIR += ./GeneratedFiles/debug
OBJECTS_DIR += debug
UI_DIR += ./GeneratedFiles
RCC_DIR += ./GeneratedFiles
include(bus_trans_server.pri)
