#include "rpcskeleton_webemails.h"
RpcSkeleton_WebEmails::RpcSkeleton_WebEmails(BusinessServiceSet *pHandler,int RPCType):
RpcSkeleton(RPCType)
{
	m_Handler=pHandler->WebEmails; //store handler to actual business object
	SetNameSpace("email"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("GET/email/[RES_ID]",&RpcSkeleton_WebEmails::ReadEmailDetails);
	mFunctList.insert("GET/email/[RES_ID]/pdf",&RpcSkeleton_WebEmails::ReadEmailAsPdf);
	mFunctList.insert("GET/email/[RES_ID]/html",&RpcSkeleton_WebEmails::ReadEmailAsHtml);
	mFunctList.insert("POST/email",&RpcSkeleton_WebEmails::ReadEmails);
	mFunctList.insert("POST/email/search",&RpcSkeleton_WebEmails::SearchEmails);
	mFunctList.insert("GET/email/attachment/[RES_ID]",&RpcSkeleton_WebEmails::GetAttachmentURL);
	mFunctList.insert("GET/email/attachment/[RES_ID]/unzip",&RpcSkeleton_WebEmails::GetAttachmentUnzippedFiles);
	mFunctList.insert("GET/email/attachment/[RES_ID]/binary",&RpcSkeleton_WebEmails::GetAttachmentBinary);
	mFunctList.insert("GET/email/[RES_ID]/pdf/binary",&RpcSkeleton_WebEmails::ReadEmailAsPdfBinary);
	mFunctList.insert("GET/email/[RES_ID]/html/binary",&RpcSkeleton_WebEmails::ReadEmailAsHtmlBinary);
}

bool RpcSkeleton_WebEmails::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;

	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) 
		return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);

		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);

			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

return true;
}

void RpcSkeleton_WebEmails::ReadEmails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	DbRecordSet  Ret_Emails;
	int  nFromN;
	int  nToN;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadEmails(err,Ret_nTotalCount,Ret_Emails,nFromN,nToN);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Emails,"Emails","email","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::SearchEmails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=11)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	DbRecordSet  Ret_Emails;
	int  nSortOrder;
	int  nFromN;
	int  nToN;
	int  nContactID;
	int  nProjectID;
	int  nOwnerUserID;
	QDate  datFrom;
	QDate  datTo;
	QString  strEmailAcc;
	QString  strEmailSender;
	int  nMailBox;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&nSortOrder,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nContactID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&nProjectID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&nOwnerUserID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&datFrom,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&datTo,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&strEmailAcc,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(9,&strEmailSender,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(10,&nMailBox,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->SearchEmails(err,Ret_nTotalCount,Ret_Emails,nSortOrder,nFromN,nToN,nContactID,nProjectID,nOwnerUserID,datFrom,datTo,strEmailAcc,strEmailSender,nMailBox);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Emails,"Emails","email","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::ReadEmailDetails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nEmailID;
	DbRecordSet  Ret_Email;
	DbRecordSet  Ret_Attachments;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nEmailID=nResource_id;

	//invoke handler
	m_Handler->ReadEmailDetails(err,nEmailID,Ret_Email,Ret_Attachments);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Email,"Emails","email","","");
	rpc.msg_out->AddParameter(&Ret_Attachments,"Attachment","document","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::GetAttachmentURL(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nAttachmentID;
	QString  Ret_strDocumentURL;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nAttachmentID=nResource_id;

	//invoke handler
	m_Handler->GetAttachmentURL(err,nAttachmentID,Ret_strDocumentURL);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_strDocumentURL,"strDocumentURL");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::GetAttachmentUnzippedFiles(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nAttachmentID;
	DbRecordSet  Ret_AttachmentFiles;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nAttachmentID=nResource_id;

	//invoke handler
	m_Handler->GetAttachmentUnzippedFiles(err,nAttachmentID,Ret_AttachmentFiles);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_AttachmentFiles,"Attachment","document","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::ReadEmailAsPdf(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nEmailID;
	QString  Ret_strDocumentURL;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nEmailID=nResource_id;

	//invoke handler
	m_Handler->ReadEmailAsPdf(err,nEmailID,Ret_strDocumentURL);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_strDocumentURL,"strDocumentURL");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::ReadEmailAsHtml(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nEmailID;
	QString  Ret_strDocumentURL;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nEmailID=nResource_id;

	//invoke handler
	m_Handler->ReadEmailAsHtml(err,nEmailID,Ret_strDocumentURL);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_strDocumentURL,"strDocumentURL");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::ReadEmailAsPdfBinary(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nEmailID;
	QByteArray  Ret_byteData;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nEmailID=nResource_id;

	//invoke handler
	m_Handler->ReadEmailAsPdfBinary(err,nEmailID,Ret_byteData);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_byteData,"Ret_byteData");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::ReadEmailAsHtmlBinary(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nEmailID;
	QByteArray  Ret_byteData;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nEmailID=nResource_id;

	//invoke handler
	m_Handler->ReadEmailAsHtmlBinary(err,nEmailID,Ret_byteData);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_byteData,"Ret_byteData");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebEmails::GetAttachmentBinary(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nAttachmentID;
	QByteArray  Ret_byteData;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nAttachmentID=nResource_id;

	//invoke handler
	m_Handler->GetAttachmentBinary(err,nAttachmentID,Ret_byteData);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_byteData,"Ret_byteData");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

