#ifndef RPCSKELETON_BUSCALENDAR_H__
#define RPCSKELETON_BUSCALENDAR_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_buscalendar.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusCalendar : public RpcSkeleton
{

typedef void (RpcSkeleton_BusCalendar::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusCalendar(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void SaveCalFilterView(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCalFilterViews(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCalFilterViewsWithEntitesAndTypes(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCalFilterView(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteCalFilterView(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteEvent(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadEvent(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadTemplates(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadOnePart(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadOneEvent(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadEventPartsCountForEntities(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadEventParts(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadEventPartsForInsert(Status &err,RpcSkeletonMessageHandler &rpc);
	void ModifyEventDateRange(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteEventPart(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteEventParts(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadReservation(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadEventPartEntitesForOneEntity(Status &err,RpcSkeletonMessageHandler &rpc);
	void ModifyEventPresence(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetInviteStatus(Status &err,RpcSkeletonMessageHandler &rpc);
	void SendInviteBySokrates(Status &err,RpcSkeletonMessageHandler &rpc);
	void PrepareInviteRecordsForSendByEmail(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteInviteRecordsAfterSend(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteInviteReplyStatus(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteFromOutlook(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadForOutlook(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetEventPartContacts(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetEventPartResources(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetEventPartProjects(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetEventPartBreaks(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusCalendar *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSCALENDAR_H__
