#ifndef RPCSKELETON_MAINENTITYSELECTOR_H__
#define RPCSKELETON_MAINENTITYSELECTOR_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_mainentityselector.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_MainEntitySelector : public RpcSkeleton
{

typedef void (RpcSkeleton_MainEntitySelector::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_MainEntitySelector(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadData(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadDataBatch(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadDataAll(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadDataOne(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_MainEntitySelector *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_MAINENTITYSELECTOR_H__
