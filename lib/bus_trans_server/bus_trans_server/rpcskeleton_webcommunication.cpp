#include "rpcskeleton_webcommunication.h"
RpcSkeleton_WebCommunication::RpcSkeleton_WebCommunication(BusinessServiceSet *pHandler,int RPCType):
RpcSkeleton(RPCType)
{
	m_Handler=pHandler->WebCommunication; //store handler to actual business object
	SetNameSpace("communication"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("GET/communication/contact/views",&RpcSkeleton_WebCommunication::ReadContactViews);
	mFunctList.insert("POST/communication/contact/views/data",&RpcSkeleton_WebCommunication::ReadContactViewData);
	mFunctList.insert("GET/communication/user/views",&RpcSkeleton_WebCommunication::ReadUserViews);
	mFunctList.insert("POST/communication/user/views/data",&RpcSkeleton_WebCommunication::ReadUserViewData);
	mFunctList.insert("GET/communication/project/views",&RpcSkeleton_WebCommunication::ReadProjectViews);
	mFunctList.insert("POST/communication/project/views/data",&RpcSkeleton_WebCommunication::ReadProjectViewData);
	mFunctList.insert("GET/communication/user/[RES_ID]/comm_entity/count",&RpcSkeleton_WebCommunication::ReadCommEntityCount);
	mFunctList.insert("POST/communication/user/[RES_ID]/documents",&RpcSkeleton_WebCommunication::ReadUserDocuments);
	mFunctList.insert("POST/communication/user/[RES_ID]/emails",&RpcSkeleton_WebCommunication::ReadUserEmails);
}

bool RpcSkeleton_WebCommunication::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;

	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) 
		return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);

		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);

			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

return true;
}

void RpcSkeleton_WebCommunication::ReadContactViews(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	DbRecordSet  Ret_Views;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->ReadContactViews(err,Ret_Views);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Views,"Views","view","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebCommunication::ReadContactViewData(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=9)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nContactID;
	DbRecordSet  Ret_Data;
	int  Ret_nTotalCount;
	int  nViewID;
	int  nUseOneColumn;
	int  nUseFromTo;
	QDateTime  datFrom;
	QDateTime  datTo;
	int  nPrevNext;
	int  nRangeSelectorSelector;
	int  nMaxRecords;
	QString  Ret_strFrom;
	QString  Ret_strTo;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&nContactID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nViewID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nUseOneColumn,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nUseFromTo,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&datFrom,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&datTo,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&nPrevNext,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&nRangeSelectorSelector,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&nMaxRecords,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadContactViewData(err,nContactID,Ret_Data,Ret_nTotalCount,nViewID,nUseOneColumn,nUseFromTo,datFrom,datTo,nPrevNext,nRangeSelectorSelector,nMaxRecords,Ret_strFrom,Ret_strTo);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Data,"Data","row_data","","");
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_strFrom,"Ret_strFrom");
	rpc.msg_out->AddParameter(&Ret_strTo,"Ret_strTo");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebCommunication::ReadUserViews(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	DbRecordSet  Ret_Views;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->ReadUserViews(err,Ret_Views);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Views,"Views","view","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebCommunication::ReadUserViewData(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=9)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nUserID;
	DbRecordSet  Ret_Data;
	int  Ret_nTotalCount;
	int  nViewID;
	int  nUseOneColumn;
	int  nUseFromTo;
	QDateTime  datFrom;
	QDateTime  datTo;
	int  nPrevNext;
	int  nRangeSelectorSelector;
	int  nMaxRecords;
	QString  Ret_strFrom;
	QString  Ret_strTo;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&nUserID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nViewID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nUseOneColumn,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nUseFromTo,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&datFrom,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&datTo,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&nPrevNext,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&nRangeSelectorSelector,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&nMaxRecords,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadUserViewData(err,nUserID,Ret_Data,Ret_nTotalCount,nViewID,nUseOneColumn,nUseFromTo,datFrom,datTo,nPrevNext,nRangeSelectorSelector,nMaxRecords,Ret_strFrom,Ret_strTo);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Data,"Data","row_data","","");
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_strFrom,"Ret_strFrom");
	rpc.msg_out->AddParameter(&Ret_strTo,"Ret_strTo");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebCommunication::ReadProjectViews(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	DbRecordSet  Ret_Views;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->ReadProjectViews(err,Ret_Views);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Views,"Views","view","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebCommunication::ReadProjectViewData(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=9)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nProjectID;
	DbRecordSet  Ret_Data;
	int  Ret_nTotalCount;
	int  nViewID;
	int  nUseOneColumn;
	int  nUseFromTo;
	QDateTime  datFrom;
	QDateTime  datTo;
	int  nPrevNext;
	int  nRangeSelectorSelector;
	int  nMaxRecords;
	QString  Ret_strFrom;
	QString  Ret_strTo;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&nProjectID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nViewID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nUseOneColumn,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&nUseFromTo,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&datFrom,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&datTo,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&nPrevNext,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&nRangeSelectorSelector,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&nMaxRecords,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadProjectViewData(err,nProjectID,Ret_Data,Ret_nTotalCount,nViewID,nUseOneColumn,nUseFromTo,datFrom,datTo,nPrevNext,nRangeSelectorSelector,nMaxRecords,Ret_strFrom,Ret_strTo);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Data,"Data","row_data","","");
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_strFrom,"Ret_strFrom");
	rpc.msg_out->AddParameter(&Ret_strTo,"Ret_strTo");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebCommunication::ReadCommEntityCount(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nUserID;
	int  Ret_nFileDocCount;
	int  Ret_nWebSiteCount;
	int  Ret_nNotesCount;
	int  Ret_nEmailCount;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nUserID=nResource_id;

	//invoke handler
	m_Handler->ReadCommEntityCount(err,nUserID,Ret_nFileDocCount,Ret_nWebSiteCount,Ret_nNotesCount,Ret_nEmailCount);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nFileDocCount,"nFileDocCount");
	rpc.msg_out->AddParameter(&Ret_nWebSiteCount,"nWebSiteCount");
	rpc.msg_out->AddParameter(&Ret_nNotesCount,"nNotesCount");
	rpc.msg_out->AddParameter(&Ret_nEmailCount,"nEmailCount");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebCommunication::ReadUserDocuments(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	DbRecordSet  Ret_Documents;
	int  nUserID;
	int  nDocType;
	int  nFromN;
	int  nToN;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nUserID=nResource_id;
	rpc.msg_in->GetParameter(0,&nDocType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadUserDocuments(err,Ret_nTotalCount,Ret_Documents,nUserID,nDocType,nFromN,nToN);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Documents,"Documents","document",GetWebServiceURL()+"/document","Document_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebCommunication::ReadUserEmails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	//B.T.: quick fix:
	//if(rpc.msg_in->GetParameterCount()!=5)
	
	if(rpc.msg_in->GetParameterCount()<4)
	{
			err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
			return;
	}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	DbRecordSet  Ret_Emails;
	int  nUserID;
	int  nFromN;
	int  nToN;
	int  nSortOrder;
	QString  strEmailAcc;
	QString  strEmailSender;
	int  nMailBox=0;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nUserID=nResource_id;
	rpc.msg_in->GetParameter(0,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nSortOrder,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&strEmailAcc,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	
	if(rpc.msg_in->GetParameterCount()>=5) //only if 5 extract, else just skip it:
	{
		rpc.msg_in->GetParameter(4,&strEmailSender,err);
		if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	}

	if(rpc.msg_in->GetParameterCount()==6) //only if 5 extract, else just skip it:
	{
		rpc.msg_in->GetParameter(5,&nMailBox,err);
		if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	}

	//invoke handler
	m_Handler->ReadUserEmails(err,Ret_nTotalCount,Ret_Emails,nUserID,nFromN,nToN,nSortOrder,strEmailAcc,strEmailSender,nMailBox);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Emails,"Emails","email",GetWebServiceURL()+"/email","Email_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

