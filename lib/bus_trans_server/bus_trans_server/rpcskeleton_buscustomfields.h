#ifndef RPCSKELETON_BUSCUSTOMFIELDS_H__
#define RPCSKELETON_BUSCUSTOMFIELDS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_buscustomfields.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusCustomFields : public RpcSkeleton
{

typedef void (RpcSkeleton_BusCustomFields::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusCustomFields(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Write(Status &err,RpcSkeletonMessageHandler &rpc);
	void Read(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadFields(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadUserCustomData(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteUserCustomData(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadUserCustomDataForInsert(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusCustomFields *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSCUSTOMFIELDS_H__
