#ifndef RPCSKELETON_BUSGRIDVIEWS_H__
#define RPCSKELETON_BUSGRIDVIEWS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busgridviews.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusGridViews : public RpcSkeleton
{

typedef void (RpcSkeleton_BusGridViews::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusGridViews(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Write(Status &err,RpcSkeletonMessageHandler &rpc);
	void Read(Status &err,RpcSkeletonMessageHandler &rpc);
	void Delete(Status &err,RpcSkeletonMessageHandler &rpc);
	void Lock(Status &err,RpcSkeletonMessageHandler &rpc);
	void Unlock(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusGridViews *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSGRIDVIEWS_H__
