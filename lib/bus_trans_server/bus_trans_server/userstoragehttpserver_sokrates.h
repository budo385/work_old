#ifndef USERSTORAGEHTTPSERVER_SOKRATES_H
#define USERSTORAGEHTTPSERVER_SOKRATES_H

#include "trans/trans/userstoragehttpserver.h"

class UserStorageHTTPServer_Sokrates : public UserStorageHTTPServer
{

public:
	void SetBackupDirectoryPath(QString strPath){m_strBackupDirectoryPath=strPath;}

protected:
	bool	AuthenticateUser(QString strCookieValue,QString &strSessionID);
	bool	IsEnoughDiskSpaceForUserStorage(qint64 nNewFileSize);
	QString InitializeUserStorageDirectory(Status &status,QString strUserSubDir);
	void	CheckPermission(Status &pStatus,QString strSession,int nMethodType,QString strAliasDir,QString strDirPath,QString strFileName,qint64 nFileSizeForUpload);
	void	GetDirectoryPath(Status &status,const QUrl &url,QString strSession,QString &strDirPath);
	
	QString		m_strBackupDirectoryPath;
};

#endif // USERSTORAGEHTTPSERVER_SOKRATES_H
