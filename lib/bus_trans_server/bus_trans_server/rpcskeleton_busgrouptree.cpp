#include "rpcskeleton_busgrouptree.h"

RpcSkeleton_BusGroupTree::RpcSkeleton_BusGroupTree(BusinessServiceSet *pHandler,int RPCType):
	RpcSkeleton(RPCType)
{
	m_Handler=pHandler->BusGroupTree; //store handler to actual business object
	SetNameSpace("BusGroupTree"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("ReadTree",&RpcSkeleton_BusGroupTree::ReadTree);
	mFunctList.insert("WriteTree",&RpcSkeleton_BusGroupTree::WriteTree);
	mFunctList.insert("DeleteTree",&RpcSkeleton_BusGroupTree::DeleteTree);
	mFunctList.insert("LockTree",&RpcSkeleton_BusGroupTree::LockTree);
	mFunctList.insert("UnLockTree",&RpcSkeleton_BusGroupTree::UnLockTree);
	mFunctList.insert("ReadGroupTree",&RpcSkeleton_BusGroupTree::ReadGroupTree);
	mFunctList.insert("WriteGroupItem",&RpcSkeleton_BusGroupTree::WriteGroupItem);
	mFunctList.insert("DeleteGroupItem",&RpcSkeleton_BusGroupTree::DeleteGroupItem);
	mFunctList.insert("ChangeGroupContent",&RpcSkeleton_BusGroupTree::ChangeGroupContent);
	mFunctList.insert("ReadGroupContent",&RpcSkeleton_BusGroupTree::ReadGroupContent);
	mFunctList.insert("WriteGroupItemData",&RpcSkeleton_BusGroupTree::WriteGroupItemData);
	mFunctList.insert("WriteGroupContentData",&RpcSkeleton_BusGroupTree::WriteGroupContentData);
	mFunctList.insert("RemoveGroupContentItem",&RpcSkeleton_BusGroupTree::RemoveGroupContentItem);
	mFunctList.insert("GetChildrenGroups",&RpcSkeleton_BusGroupTree::GetChildrenGroups);
}

bool RpcSkeleton_BusGroupTree::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	if(!TestNameSpace(strRPCMethod))return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);

	//get methodname, test for error
	QString strMethodName=rpc.GetSkeletonMethodName();
	if (strMethodName.isEmpty()){rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);return true;}

	//METHOD DISPATCHER
	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodName);
	if(i != mFunctList.end() && i.key() == strMethodName)
	{
		//call skeleton method
		(this->*i.value())(err,rpc);
		return true;
	}
	else
	{
		//if not found return error:
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}
	return true;
}

void RpcSkeleton_BusGroupTree::ReadTree(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nGroupEntityTypeID;
	DbRecordSet  Ret_Data;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nGroupEntityTypeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->ReadTree(err,nGroupEntityTypeID,Ret_Data);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Data,"Ret_Data");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::WriteTree(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  RetOut_Data;
	QString  strLockRes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RetOut_Data,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&strLockRes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteTree(err,RetOut_Data,strLockRes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_Data,"RetOut_Data");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::DeleteTree(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nTreeID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nTreeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->DeleteTree(err,nTreeID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::LockTree(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nTreeID;
	QString  Ret_strLockRes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nTreeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->LockTree(err,nTreeID,Ret_strLockRes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_strLockRes,"Ret_strLockRes");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::UnLockTree(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strLockRes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&strLockRes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->UnLockTree(err,strLockRes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::ReadGroupTree(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nTreeID;
	DbRecordSet  Ret_Data;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nTreeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->ReadGroupTree(err,nTreeID,Ret_Data);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Data,"Ret_Data");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::WriteGroupItem(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nTreeID;
	DbRecordSet  RetOut_Data;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nTreeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RetOut_Data,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteGroupItem(err,nTreeID,RetOut_Data);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_Data,"RetOut_Data");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::DeleteGroupItem(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nTreeID;
	DbRecordSet  DataForDelete;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nTreeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&DataForDelete,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->DeleteGroupItem(err,nTreeID,DataForDelete);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::ChangeGroupContent(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=5)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nGroupEntityTypeID;
	int  nGroupItemID;
	int  nOperation;
	DbRecordSet  RetOut_GroupContent;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nGroupEntityTypeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&nGroupItemID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&nOperation,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&RetOut_GroupContent,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->ChangeGroupContent(err,nGroupEntityTypeID,nGroupItemID,nOperation,RetOut_GroupContent);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_GroupContent,"RetOut_GroupContent");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::ReadGroupContent(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nGroupEntityTypeID;
	DbRecordSet  GroupItems;
	DbRecordSet  Ret_GroupContent;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nGroupEntityTypeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&GroupItems,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->ReadGroupContent(err,nGroupEntityTypeID,GroupItems,Ret_GroupContent);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_GroupContent,"Ret_GroupContent");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::WriteGroupItemData(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=4)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nGroupItemID;
	QString  strExtCategory;
	QString  strDescription;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nGroupItemID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&strExtCategory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&strDescription,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteGroupItemData(err,nGroupItemID,strExtCategory,strDescription);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::WriteGroupContentData(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nGroupEntityTypeID;
	DbRecordSet  GroupItems;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nGroupEntityTypeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&GroupItems,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteGroupContentData(err,nGroupEntityTypeID,GroupItems);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::RemoveGroupContentItem(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=4)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nGroupEntityTypeID;
	DbRecordSet  GroupItemsForRemove;
	DbRecordSet  Groups;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nGroupEntityTypeID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&GroupItemsForRemove,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&Groups,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->RemoveGroupContentItem(err,nGroupEntityTypeID,GroupItemsForRemove,Groups);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusGroupTree::GetChildrenGroups(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nGroupParentID;
	DbRecordSet  Ret_ChildrenGroups;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nGroupParentID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetChildrenGroups(err,nGroupParentID,Ret_ChildrenGroups);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_ChildrenGroups,"Ret_ChildrenGroups");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

