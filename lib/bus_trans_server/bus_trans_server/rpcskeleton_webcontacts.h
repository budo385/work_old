#ifndef RPCSKELETON_WEBCONTACTS_H__
#define RPCSKELETON_WEBCONTACTS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_webcontacts.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_WebContacts : public RpcSkeleton
{

typedef void (RpcSkeleton_WebContacts::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );

public:
	RpcSkeleton_WebContacts(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadContacts(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReadContactsWithFilter(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReadContactDetails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void SearchContacts(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReadFavorites(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReadContactAddresses(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReadContactDocuments(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReadContactEmails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);
	void ReadCommEntityCount(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);

	Interface_WebContacts *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_WEBCONTACTS_H__
