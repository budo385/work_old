#ifndef RPCSKELETON_VOICECALLCENTER_H__
#define RPCSKELETON_VOICECALLCENTER_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_voicecallcenter.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_VoiceCallCenter : public RpcSkeleton
{

typedef void (RpcSkeleton_VoiceCallCenter::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_VoiceCallCenter(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadCall(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteCall(Status &err,RpcSkeletonMessageHandler &rpc);
	void ListCalls(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteCalls(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_VoiceCallCenter *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_VOICECALLCENTER_H__
