#include "rpcskeleton_modulelicense.h"

RpcSkeleton_ModuleLicense::RpcSkeleton_ModuleLicense(BusinessServiceSet *pHandler,int RPCType):
	RpcSkeleton(RPCType)
{
	m_Handler=pHandler->ModuleLicense; //store handler to actual business object
	SetNameSpace("ModuleLicense"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("GetAllModuleLicenseData",&RpcSkeleton_ModuleLicense::GetAllModuleLicenseData);
	mFunctList.insert("GetFPList",&RpcSkeleton_ModuleLicense::GetFPList);
	mFunctList.insert("GetLicenseInfo",&RpcSkeleton_ModuleLicense::GetLicenseInfo);
}

bool RpcSkeleton_ModuleLicense::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	if(!TestNameSpace(strRPCMethod))return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);

	//get methodname, test for error
	QString strMethodName=rpc.GetSkeletonMethodName();
	if (strMethodName.isEmpty()){rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);return true;}

	//METHOD DISPATCHER
	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodName);
	if(i != mFunctList.end() && i.key() == strMethodName)
	{
		//call skeleton method
		(this->*i.value())(err,rpc);
		return true;
	}
	else
	{
		//if not found return error:
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}
	return true;
}

void RpcSkeleton_ModuleLicense::GetAllModuleLicenseData(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=8)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strModuleCode;
	QString  strMLIID;
	DbRecordSet  RetOut_pList;
	QString  RetOut_strUserName;
	int  RetOut_nLicenseID;
	QString  RetOut_strReportLine1;
	QString  RetOut_strReportLine2;
	QString  Ret_strCustomerID;
	int  Ret_nCustomSolutionID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&strModuleCode,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&strMLIID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&RetOut_pList,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&RetOut_strUserName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(5,&RetOut_nLicenseID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(6,&RetOut_strReportLine1,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(7,&RetOut_strReportLine2,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetAllModuleLicenseData(err,strModuleCode,strMLIID,RetOut_pList,RetOut_strUserName,RetOut_nLicenseID,RetOut_strReportLine1,RetOut_strReportLine2,Ret_strCustomerID,Ret_nCustomSolutionID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_pList,"RetOut_pList");
	rpc.msg_out->AddParameter(&RetOut_strUserName,"RetOut_strUserName");
	rpc.msg_out->AddParameter(&RetOut_nLicenseID,"RetOut_nLicenseID");
	rpc.msg_out->AddParameter(&RetOut_strReportLine1,"RetOut_strReportLine1");
	rpc.msg_out->AddParameter(&RetOut_strReportLine2,"RetOut_strReportLine2");
	rpc.msg_out->AddParameter(&Ret_strCustomerID,"Ret_strCustomerID");
	rpc.msg_out->AddParameter(&Ret_nCustomSolutionID,"Ret_nCustomSolutionID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_ModuleLicense::GetFPList(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=4)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strModuleCode;
	QString  strMLIID;
	DbRecordSet  RetOut_pList;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&strModuleCode,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&strMLIID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&RetOut_pList,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetFPList(err,strModuleCode,strMLIID,RetOut_pList);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_pList,"RetOut_pList");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_ModuleLicense::GetLicenseInfo(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=5)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  RetOut_strUserName;
	int  RetOut_nLicenseID;
	QString  RetOut_strReportLine1;
	QString  RetOut_strReportLine2;
	QString  Ret_strCustomerID;
	int  Ret_nCustomSolutionID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RetOut_strUserName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RetOut_nLicenseID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&RetOut_strReportLine1,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&RetOut_strReportLine2,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetLicenseInfo(err,RetOut_strUserName,RetOut_nLicenseID,RetOut_strReportLine1,RetOut_strReportLine2,Ret_strCustomerID,Ret_nCustomSolutionID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_strUserName,"RetOut_strUserName");
	rpc.msg_out->AddParameter(&RetOut_nLicenseID,"RetOut_nLicenseID");
	rpc.msg_out->AddParameter(&RetOut_strReportLine1,"RetOut_strReportLine1");
	rpc.msg_out->AddParameter(&RetOut_strReportLine2,"RetOut_strReportLine2");
	rpc.msg_out->AddParameter(&Ret_strCustomerID,"Ret_strCustomerID");
	rpc.msg_out->AddParameter(&Ret_nCustomSolutionID,"Ret_nCustomSolutionID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

