#ifndef RPCSKELETON_SPCBUSINESSFIGURES_H__
#define RPCSKELETON_SPCBUSINESSFIGURES_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_spcbusinessfigures.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_SpcBusinessFigures : public RpcSkeleton
{

typedef void (RpcSkeleton_SpcBusinessFigures::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );

public:
	RpcSkeleton_SpcBusinessFigures(BusinessServiceSet *pHandler, int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Routine_275(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);

	Interface_SpcBusinessFigures *m_Handler; //< pointer to your BO

	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_SPCBUSINESSFIGURES_H__
