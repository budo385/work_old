#include "rpcskeleton_busemail.h"

RpcSkeleton_BusEmail::RpcSkeleton_BusEmail(BusinessServiceSet *pHandler,int RPCType):
	RpcSkeleton(RPCType)
{
	m_Handler=pHandler->BusEmail; //store handler to actual business object
	SetNameSpace("BusEmail"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("Read",&RpcSkeleton_BusEmail::Read);
	mFunctList.insert("Write",&RpcSkeleton_BusEmail::Write);
	mFunctList.insert("WriteMultiple",&RpcSkeleton_BusEmail::WriteMultiple);
	mFunctList.insert("WriteEmailLog",&RpcSkeleton_BusEmail::WriteEmailLog);
	mFunctList.insert("List",&RpcSkeleton_BusEmail::List);
	mFunctList.insert("Delete",&RpcSkeleton_BusEmail::Delete);
	mFunctList.insert("UpdateEmailFromClient",&RpcSkeleton_BusEmail::UpdateEmailFromClient);
	mFunctList.insert("SetEmailRead",&RpcSkeleton_BusEmail::SetEmailRead);
	mFunctList.insert("WriteAttachments",&RpcSkeleton_BusEmail::WriteAttachments);
	mFunctList.insert("WriteEmailAttachmentsFromUserStorage",&RpcSkeleton_BusEmail::WriteEmailAttachmentsFromUserStorage);
}

bool RpcSkeleton_BusEmail::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	if(!TestNameSpace(strRPCMethod))return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);

	//get methodname, test for error
	QString strMethodName=rpc.GetSkeletonMethodName();
	if (strMethodName.isEmpty()){rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);return true;}

	//METHOD DISPATCHER
	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodName);
	if(i != mFunctList.end() && i.key() == strMethodName)
	{
		//call skeleton method
		(this->*i.value())(err,rpc);
		return true;
	}
	else
	{
		//if not found return error:
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}
	return true;
}

void RpcSkeleton_BusEmail::Read(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nEmailID;
	DbRecordSet  RetOut_Data;
	DbRecordSet  Ret_ContactLink;
	DbRecordSet  Ret_ScheduleTask;
	DbRecordSet  Ret_ContactEmails;
	DbRecordSet  Ret_Attachments;
	DbRecordSet  Ret_UAR;
	DbRecordSet  Ret_GAR;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nEmailID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RetOut_Data,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->Read(err,nEmailID,RetOut_Data,Ret_ContactLink,Ret_ScheduleTask,Ret_ContactEmails,Ret_Attachments,Ret_UAR,Ret_GAR);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_Data,"RetOut_Data");
	rpc.msg_out->AddParameter(&Ret_ContactLink,"Ret_ContactLink");
	rpc.msg_out->AddParameter(&Ret_ScheduleTask,"Ret_ScheduleTask");
	rpc.msg_out->AddParameter(&Ret_ContactEmails,"Ret_ContactEmails");
	rpc.msg_out->AddParameter(&Ret_Attachments,"Ret_Attachments");
	rpc.msg_out->AddParameter(&Ret_UAR,"Ret_UAR");
	rpc.msg_out->AddParameter(&Ret_GAR,"Ret_GAR");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::Write(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=8)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  RetOut_Data;
	DbRecordSet  RetOut_ContactLink;
	DbRecordSet  RetOut_ScheduleTask;
	DbRecordSet  RetOut_Attachments;
	QString  strLockRes;
	DbRecordSet  RetOut_UAR;
	DbRecordSet  RetOut_GAR;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RetOut_Data,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RetOut_ContactLink,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&RetOut_ScheduleTask,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&RetOut_Attachments,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(5,&strLockRes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(6,&RetOut_UAR,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(7,&RetOut_GAR,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->Write(err,RetOut_Data,RetOut_ContactLink,RetOut_ScheduleTask,RetOut_Attachments,strLockRes,RetOut_UAR,RetOut_GAR);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_Data,"RetOut_Data");
	rpc.msg_out->AddParameter(&RetOut_ContactLink,"RetOut_ContactLink");
	rpc.msg_out->AddParameter(&RetOut_ScheduleTask,"RetOut_ScheduleTask");
	rpc.msg_out->AddParameter(&RetOut_Attachments,"RetOut_Attachments");
	rpc.msg_out->AddParameter(&RetOut_UAR,"RetOut_UAR");
	rpc.msg_out->AddParameter(&RetOut_GAR,"RetOut_GAR");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::WriteMultiple(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=9)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  RetOut_Data;
	DbRecordSet  RetOut_ContactLink;
	DbRecordSet  RetOut_ScheduleTask;
	DbRecordSet  RetOut_Attachments;
	QString  strLockRes;
	bool  bSkipExisting;
	int  RetOut_nSkippedOrReplacedCnt;
	bool  bAddDupsAsNewRow;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RetOut_Data,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RetOut_ContactLink,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&RetOut_ScheduleTask,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&RetOut_Attachments,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(5,&strLockRes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(6,&bSkipExisting,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(7,&RetOut_nSkippedOrReplacedCnt,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(8,&bAddDupsAsNewRow,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteMultiple(err,RetOut_Data,RetOut_ContactLink,RetOut_ScheduleTask,RetOut_Attachments,strLockRes,bSkipExisting,RetOut_nSkippedOrReplacedCnt,bAddDupsAsNewRow);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_Data,"RetOut_Data");
	rpc.msg_out->AddParameter(&RetOut_ContactLink,"RetOut_ContactLink");
	rpc.msg_out->AddParameter(&RetOut_ScheduleTask,"RetOut_ScheduleTask");
	rpc.msg_out->AddParameter(&RetOut_Attachments,"RetOut_Attachments");
	rpc.msg_out->AddParameter(&RetOut_nSkippedOrReplacedCnt,"RetOut_nSkippedOrReplacedCnt");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::WriteEmailLog(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  RetOut_Data;
	QString  strLockRes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RetOut_Data,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&strLockRes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteEmailLog(err,RetOut_Data,strLockRes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_Data,"RetOut_Data");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::List(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=6)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  RetOut_lstData;
	QDate  datDateFrom;
	QDate  datDateTo;
	int  nOwnerID;
	int  nFilterID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RetOut_lstData,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&datDateFrom,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&datDateTo,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&nOwnerID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(5,&nFilterID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->List(err,RetOut_lstData,datDateFrom,datDateTo,nOwnerID,nFilterID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_lstData,"RetOut_lstData");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::Delete(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  RetOut_lstIDs;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RetOut_lstIDs,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->Delete(err,RetOut_lstIDs);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_lstIDs,"RetOut_lstIDs");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::UpdateEmailFromClient(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Emails;
	DbRecordSet  Attachments;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&Emails,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&Attachments,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->UpdateEmailFromClient(err,Emails,Attachments);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::SetEmailRead(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nCE_ID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nCE_ID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->SetEmailRead(err,nCE_ID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::WriteAttachments(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nEmailID;
	DbRecordSet  RetOut_Attachments;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nEmailID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RetOut_Attachments,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteAttachments(err,nEmailID,RetOut_Attachments);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_Attachments,"RetOut_Attachments");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusEmail::WriteEmailAttachmentsFromUserStorage(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nEmailID;
	DbRecordSet  Attachments;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nEmailID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&Attachments,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteEmailAttachmentsFromUserStorage(err,nEmailID,Attachments);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

