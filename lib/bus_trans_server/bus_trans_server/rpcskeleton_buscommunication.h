#ifndef RPCSKELETON_BUSCOMMUNICATION_H__
#define RPCSKELETON_BUSCOMMUNICATION_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_buscommunication.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusCommunication : public RpcSkeleton
{

typedef void (RpcSkeleton_BusCommunication::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusCommunication(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void SaveCommFilterViews(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetDefaultDesktopFilterViews(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCommFilterViews(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCommFilterData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCommFilterViewDataAndViews(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetGridViewForContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetGridViewForProject(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetPersonCommData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetContactCommData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetProjectCommData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCalendarCommData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCalendarGridCommData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetCommGridDataByIDs(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetComplexFilterData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetContactFromEmail(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetTaskIsDone(Status &err,RpcSkeletonMessageHandler &rpc);
	void PostPoneTasks(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetEmailsRead(Status &err,RpcSkeletonMessageHandler &rpc);
	void ChangeEntityAssigment(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadCEMenuData(Status &err,RpcSkeletonMessageHandler &rpc);
	void AssignCommDataToContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void AssignCommDataToProject(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckIfRecordExists(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusCommunication *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSCOMMUNICATION_H__
