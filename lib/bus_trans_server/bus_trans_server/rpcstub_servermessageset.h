#ifndef RPCSTUBSERVERMESSAGESET_H
#define RPCSTUBSERVERMESSAGESET_H


#include <QtCore>
#include <QDebug>
#include "common/common/status.h"
#include "trans/trans/rpcstub.h"
#include "trans/trans/httpserver.h"
#include "trans/trans/rpcstubmessagehandler.h"
#include "trans/trans/rpchttpheader.h"


/*!
    \class RpcStubServerMessageSet
    \brief Server side stub set of server messages (ASYNC)
    \ingroup Bus_Trans_Server

	Use:
	- when you want to send message to client, put your stub function here
	- when calling stub function from server thread you must know to what client you want to send msg or broadcast to all
	- client skeleton set on client side set must have 'mirrored' skeleton message handler to accept message
	- used as global object, mulitthread safe, must have pointer to httpserver

	Domain for server messages will be: 'ServerMessages'

*/

class RpcStubServerMessageSet : public RpcStub
{
public:
	RpcStubServerMessageSet(HTTPServer *pHttpServer,int RPCType);

	//Place your stubs here:
	//stubs: (2nd parameter is mandatory: socket id or thread id of client to which msg will be sent)
	void ServerMsg(Status pStatus,QList<int> lstSocketID, DbRecordSet rowServerMessage);


private:
	HTTPServer *m_HttpServer;		///< http client for sending reuests to server
	RpcHttpHeader m_HttpHeader;		///< htto header manager
};


#endif //RPCSTUBSERVERMESSAGESET_H
