#ifndef WEBSERVICERPCDISPATCHER_H
#define WEBSERVICERPCDISPATCHER_H

#include "trans/trans/rpcskeletondispatcher.h"
#include "bus_server/bus_server/businessserviceset.h"

//include all skeleton wb services here
#include "rpcskeleton_webcontacts.h"
#include "rpcskeleton_webserver.h"
#include "rpcskeleton_webdocuments.h"
#include "rpcskeleton_webemails.h"
#include "rpcskeleton_webprojects.h"
#include "rpcskeleton_webcalendars.h"
#include "rpcskeleton_websessiondata.h"
#include "rpcskeleton_webcommunication.h"
#include "rpcskeleton_webcommunication.h"
#include "rpcskeleton_spcsession.h"
#include "rpcskeleton_spcbusinessfigures.h"
#include "rpcskeleton_webmwcloudservice.h"

class WebServiceRpcDispatcher : public RpcSkeletonDispatcher
{
public:
	WebServiceRpcDispatcher(BusinessServiceSet *pHandler);
	virtual void	SetWebServiceURL(QString strURL);

private:
	RpcSkeleton_WebServer		*WebServer;
	RpcSkeleton_WebContacts		*WebContacts;
	RpcSkeleton_WebDocuments	*WebDocuments;
	RpcSkeleton_WebEmails		*WebEmails;
	RpcSkeleton_WebProjects		*WebProjects;
	RpcSkeleton_WebCalendars	*WebCalendars;	
	RpcSkeleton_WebSessionData  *WebSessionData;
	RpcSkeleton_WebCommunication *WebCommunication;
	RpcSkeleton_WebMWCloudService *WebMWCloudService;
	
	//same rest interface:
	RpcSkeleton_SpcSession			*SpcSession;
	RpcSkeleton_SpcBusinessFigures	*SpcBusinessFigures;
	
	
};

#endif // WEBSERVICERPCDISPATCHER_H
