#include "rpcskeleton_webcontacts.h"

RpcSkeleton_WebContacts::RpcSkeleton_WebContacts(BusinessServiceSet *pHandler,int RPCType):
RpcSkeleton(RPCType)
{
	m_Handler=pHandler->WebContacts; //store handler to actual business object
	SetNameSpace("contact"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("GET/contact",&RpcSkeleton_WebContacts::ReadContacts);
	mFunctList.insert("POST/contact/filter",&RpcSkeleton_WebContacts::ReadContactsWithFilter);
	mFunctList.insert("GET/contact/[RES_ID]",&RpcSkeleton_WebContacts::ReadContactDetails);
	mFunctList.insert("POST/contact/search",&RpcSkeleton_WebContacts::SearchContacts);
	mFunctList.insert("GET/contact/favorites",&RpcSkeleton_WebContacts::ReadFavorites);
	mFunctList.insert("GET/contact/[RES_ID]/addresses",&RpcSkeleton_WebContacts::ReadContactAddresses);
	mFunctList.insert("POST/contact/[RES_ID]/documents",&RpcSkeleton_WebContacts::ReadContactDocuments);
	mFunctList.insert("POST/contact/[RES_ID]/emails",&RpcSkeleton_WebContacts::ReadContactEmails);
	mFunctList.insert("GET/contact/[RES_ID]/comm_entity/count",&RpcSkeleton_WebContacts::ReadCommEntityCount);
}

bool RpcSkeleton_WebContacts::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	QString strMethodForCompare;
	QString strSchema;
	int nResID=-1;
	int nParentResID=-1;
	QString strHttpMethod;

	if(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))
		return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);
	rpc.msg_in->StartResponseParsing(err);
	if (!err.IsOK()) 
		return true;

	if (strHttpMethod=="OPTIONS")
	{
		strMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf("/")+1);
		QString strHttpMethodAllowed;
		QHashIterator<QString,PFN> i(mFunctList);

		while (i.hasNext()) 
		{
			i.next();
			QString strMethod=i.key();
			strMethod=strMethod.mid(strMethod.indexOf("/")+1);

			if (strMethodForCompare==strMethod)
			{
				strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
			}
		}

		if (!strHttpMethodAllowed.isEmpty())
		{
			strHttpMethodAllowed.chop(1);
			err.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);
			return true;
		}
		else
		{
			err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
			return true;
		}
	}

	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);
	if(i != mFunctList.end() && i.key() == strMethodForCompare)
	{
		(this->*i.value())(err,rpc,nResID,nParentResID);
		return true;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}

return true;
}

void RpcSkeleton_WebContacts::ReadContacts(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	DbRecordSet  Ret_Contacts;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->ReadContacts(err,Ret_Contacts);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Contacts,"Contacts","contact",GetWebServiceURL()+"/contact","Contact_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebContacts::ReadContactsWithFilter(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=5)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	DbRecordSet  Ret_Contacts;
	int  nFilter;
	int  nFromN;
	int  nToN;
	QString  strFromLetter;
	QString  strToLetter;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&nFilter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&strFromLetter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strToLetter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadContactsWithFilter(err,Ret_nTotalCount,Ret_Contacts,nFilter,nFromN,nToN,strFromLetter,strToLetter);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Contacts,"Contacts","contact",GetWebServiceURL()+"/contact","Contact_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebContacts::ReadContactDetails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nContactID;
	DbRecordSet  Ret_ContactData;
	DbRecordSet  Ret_Emails;
	DbRecordSet  Ret_WebSites;
	DbRecordSet  Ret_Phones;
	DbRecordSet  Ret_Sms;
	DbRecordSet  Ret_Sykpe;
	DbRecordSet  Ret_Addresses;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nContactID=nResource_id;

	//invoke handler
	m_Handler->ReadContactDetails(err,nContactID,Ret_ContactData,Ret_Emails,Ret_WebSites,Ret_Phones,Ret_Sms,Ret_Sykpe,Ret_Addresses);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_ContactData,"ContactData","contact",GetWebServiceURL()+"/contact","Contact_ID");
	rpc.msg_out->AddParameter(&Ret_Emails,"Emails","email","","");
	rpc.msg_out->AddParameter(&Ret_WebSites,"WebSites","website","","");
	rpc.msg_out->AddParameter(&Ret_Phones,"Phones","phone","","");
	rpc.msg_out->AddParameter(&Ret_Sms,"Sms","sms","","");
	rpc.msg_out->AddParameter(&Ret_Sykpe,"Ret_Sykpe");
	rpc.msg_out->AddParameter(&Ret_Addresses,"Addresses","address","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebContacts::SearchContacts(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=10)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	DbRecordSet  Ret_Contacts;
	int  nSearchType;
	QString  strField1_Name;
	QString  strField1_Value;
	QString  strField2_Name;
	QString  strField2_Value;
	int  nGroupID;
	int  nFromN;
	int  nToN;
	QString  strFromLetter;
	QString  strToLetter;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&nSearchType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&strField1_Name,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&strField1_Value,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(3,&strField2_Name,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(4,&strField2_Value,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(5,&nGroupID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(6,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(7,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(8,&strFromLetter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(9,&strToLetter,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->SearchContacts(err,Ret_nTotalCount,Ret_Contacts,nSearchType,strField1_Name,strField1_Value,strField2_Name,strField2_Value,nGroupID,nFromN,nToN,strFromLetter,strToLetter);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Contacts,"Contacts","contact",GetWebServiceURL()+"/contact","Contact_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebContacts::ReadFavorites(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	DbRecordSet  Ret_Favorites;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->ReadFavorites(err,Ret_Favorites);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Favorites,"Favorites","contact",GetWebServiceURL()+"/contact","Contact_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebContacts::ReadContactAddresses(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nContactID;
	DbRecordSet  Ret_Emails;
	DbRecordSet  Ret_WebSites;
	DbRecordSet  Ret_Phones;
	DbRecordSet  Ret_Sms;
	DbRecordSet  Ret_Sykpe;
	DbRecordSet  Ret_Addresses;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nContactID=nResource_id;

	//invoke handler
	m_Handler->ReadContactAddresses(err,nContactID,Ret_Emails,Ret_WebSites,Ret_Phones,Ret_Sms,Ret_Sykpe,Ret_Addresses);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Emails,"Emails","email","","");
	rpc.msg_out->AddParameter(&Ret_WebSites,"WebSites","website","","");
	rpc.msg_out->AddParameter(&Ret_Phones,"Phones","phone","","");
	rpc.msg_out->AddParameter(&Ret_Sms,"Sms","sms","","");
	rpc.msg_out->AddParameter(&Ret_Sykpe,"Ret_Sykpe");
	rpc.msg_out->AddParameter(&Ret_Addresses,"Addresses","address","","");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebContacts::ReadContactDocuments(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	DbRecordSet  Ret_Documents;
	int  nContactID;
	int  nDocType;
	int  nFromN;
	int  nToN;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nContactID=nResource_id;
	rpc.msg_in->GetParameter(0,&nDocType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(2,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadContactDocuments(err,Ret_nTotalCount,Ret_Documents,nContactID,nDocType,nFromN,nToN);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Documents,"Documents","document",GetWebServiceURL()+"/document","Document_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebContacts::ReadContactEmails(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nTotalCount;
	DbRecordSet  Ret_Emails;
	int  nContactID;
	int  nFromN;
	int  nToN;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nContactID=nResource_id;
	rpc.msg_in->GetParameter(0,&nFromN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
	rpc.msg_in->GetParameter(1,&nToN,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler
	m_Handler->ReadContactEmails(err,Ret_nTotalCount,Ret_Emails,nContactID,nFromN,nToN);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nTotalCount,"nTotalCount");
	rpc.msg_out->AddParameter(&Ret_Emails,"Emails","email",GetWebServiceURL()+"/email","Email_ID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

void RpcSkeleton_WebContacts::ReadCommEntityCount(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)
{

	//extract parameters to local vars
	int  nContactID;
	int  Ret_nFileDocCount;
	int  Ret_nWebSiteCount;
	int  Ret_nNotesCount;
	int  Ret_nEmailCount;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	nContactID=nResource_id;

	//invoke handler
	m_Handler->ReadCommEntityCount(err,nContactID,Ret_nFileDocCount,Ret_nWebSiteCount,Ret_nNotesCount,Ret_nEmailCount);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nFileDocCount,"nFileDocCount");
	rpc.msg_out->AddParameter(&Ret_nWebSiteCount,"nWebSiteCount");
	rpc.msg_out->AddParameter(&Ret_nNotesCount,"nNotesCount");
	rpc.msg_out->AddParameter(&Ret_nEmailCount,"nEmailCount");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
}

