#ifndef RPCSKELETON_WEBSESSIONDATA_H__
#define RPCSKELETON_WEBSESSIONDATA_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_websessiondata.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_WebSessionData : public RpcSkeleton
{

typedef void (RpcSkeleton_WebSessionData::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );

public:
	RpcSkeleton_WebSessionData(BusinessServiceSet *pHandler, int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void GetUserSessionData(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);

	Interface_WebSessionData *m_Handler; //< pointer to your BO

	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_WEBSESSIONDATA_H__
