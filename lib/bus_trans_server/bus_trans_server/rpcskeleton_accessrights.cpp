#include "rpcskeleton_accessrights.h"

RpcSkeleton_AccessRights::RpcSkeleton_AccessRights(BusinessServiceSet *pHandler,int RPCType):
	RpcSkeleton(RPCType)
{
	m_Handler=pHandler->AccessRights; //store handler to actual business object
	SetNameSpace("AccessRights"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("GetRoleList",&RpcSkeleton_AccessRights::GetRoleList);
	mFunctList.insert("GetPersons",&RpcSkeleton_AccessRights::GetPersons);
	mFunctList.insert("GetPersonRoleList",&RpcSkeleton_AccessRights::GetPersonRoleList);
	mFunctList.insert("InsertNewRoleToPerson",&RpcSkeleton_AccessRights::InsertNewRoleToPerson);
	mFunctList.insert("DeleteRoleFromPerson",&RpcSkeleton_AccessRights::DeleteRoleFromPerson);
	mFunctList.insert("GetRoleListForUsers",&RpcSkeleton_AccessRights::GetRoleListForUsers);
	mFunctList.insert("GetUsers",&RpcSkeleton_AccessRights::GetUsers);
	mFunctList.insert("GetUserRoleList",&RpcSkeleton_AccessRights::GetUserRoleList);
	mFunctList.insert("InsertNewRoleToUser",&RpcSkeleton_AccessRights::InsertNewRoleToUser);
	mFunctList.insert("DeleteRoleFromUser",&RpcSkeleton_AccessRights::DeleteRoleFromUser);
	mFunctList.insert("GetUserAccRightsRecordSet",&RpcSkeleton_AccessRights::GetUserAccRightsRecordSet);
	mFunctList.insert("GetRoles",&RpcSkeleton_AccessRights::GetRoles);
	mFunctList.insert("GetBERoles",&RpcSkeleton_AccessRights::GetBERoles);
	mFunctList.insert("GetTERoles",&RpcSkeleton_AccessRights::GetTERoles);
	mFunctList.insert("GetRoleAccessRightSets",&RpcSkeleton_AccessRights::GetRoleAccessRightSets);
	mFunctList.insert("GetAccessRightsSets",&RpcSkeleton_AccessRights::GetAccessRightsSets);
	mFunctList.insert("DeleteRole",&RpcSkeleton_AccessRights::DeleteRole);
	mFunctList.insert("DeleteAccRSetFromRole",&RpcSkeleton_AccessRights::DeleteAccRSetFromRole);
	mFunctList.insert("InsertNewAccRSetToRole",&RpcSkeleton_AccessRights::InsertNewAccRSetToRole);
	mFunctList.insert("InsertNewRole",&RpcSkeleton_AccessRights::InsertNewRole);
	mFunctList.insert("RenameRole",&RpcSkeleton_AccessRights::RenameRole);
	mFunctList.insert("GetAccessRights",&RpcSkeleton_AccessRights::GetAccessRights);
	mFunctList.insert("SetAccessRightValue",&RpcSkeleton_AccessRights::SetAccessRightValue);
	mFunctList.insert("GetDefaultRoles",&RpcSkeleton_AccessRights::GetDefaultRoles);
	mFunctList.insert("ChangeAccessRights",&RpcSkeleton_AccessRights::ChangeAccessRights);
	mFunctList.insert("ReadGUAR",&RpcSkeleton_AccessRights::ReadGUAR);
	mFunctList.insert("WriteGUAR",&RpcSkeleton_AccessRights::WriteGUAR);
	mFunctList.insert("PrepareGUAR",&RpcSkeleton_AccessRights::PrepareGUAR);
	mFunctList.insert("TestUAR",&RpcSkeleton_AccessRights::TestUAR);
	mFunctList.insert("UpdateProjectGUAR",&RpcSkeleton_AccessRights::UpdateProjectGUAR);
	mFunctList.insert("UpdateGroupContactsGUAR",&RpcSkeleton_AccessRights::UpdateGroupContactsGUAR);
	mFunctList.insert("UpdateProjectCommEntityGUAR",&RpcSkeleton_AccessRights::UpdateProjectCommEntityGUAR);
}

bool RpcSkeleton_AccessRights::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	if(!TestNameSpace(strRPCMethod))return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);

	//get methodname, test for error
	QString strMethodName=rpc.GetSkeletonMethodName();
	if (strMethodName.isEmpty()){rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);return true;}

	//METHOD DISPATCHER
	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodName);
	if(i != mFunctList.end() && i.key() == strMethodName)
	{
		//call skeleton method
		(this->*i.value())(err,rpc);
		return true;
	}
	else
	{
		//if not found return error:
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}
	return true;
}

void RpcSkeleton_AccessRights::GetRoleList(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pPersonRoleListRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetRoleList(err,Ret_pPersonRoleListRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pPersonRoleListRecordSet,"Ret_pPersonRoleListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetPersons(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pPersonsRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetPersons(err,Ret_pPersonsRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pPersonsRecordSet,"Ret_pPersonsRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetPersonRoleList(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pPersonRoleListRecordSet;
	int  PersonID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&PersonID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetPersonRoleList(err,Ret_pPersonRoleListRecordSet,PersonID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pPersonRoleListRecordSet,"Ret_pPersonRoleListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::InsertNewRoleToPerson(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  PersonID;
	int  RoleID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&PersonID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->InsertNewRoleToPerson(err,PersonID,RoleID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::DeleteRoleFromPerson(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  PersonID;
	int  RoleID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&PersonID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->DeleteRoleFromPerson(err,PersonID,RoleID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetRoleListForUsers(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pUserRoleListRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetRoleListForUsers(err,Ret_pUserRoleListRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pUserRoleListRecordSet,"Ret_pUserRoleListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetUsers(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pUsersRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetUsers(err,Ret_pUsersRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pUsersRecordSet,"Ret_pUsersRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetUserRoleList(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pUserRoleListRecordSet;
	int  UserID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&UserID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetUserRoleList(err,Ret_pUserRoleListRecordSet,UserID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pUserRoleListRecordSet,"Ret_pUserRoleListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::InsertNewRoleToUser(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  UserID;
	int  RoleID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&UserID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->InsertNewRoleToUser(err,UserID,RoleID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::DeleteRoleFromUser(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  UserID;
	int  RoleID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&UserID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->DeleteRoleFromUser(err,UserID,RoleID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetUserAccRightsRecordSet(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pUserAccRRecordSet;
	int  UserID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&UserID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetUserAccRightsRecordSet(err,Ret_pUserAccRRecordSet,UserID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pUserAccRRecordSet,"Ret_pUserAccRRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetRoles(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pUserRoleListRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetRoles(err,Ret_pUserRoleListRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pUserRoleListRecordSet,"Ret_pUserRoleListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetBERoles(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pRoleListRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetBERoles(err,Ret_pRoleListRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pRoleListRecordSet,"Ret_pRoleListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetTERoles(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pRoleListRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetTERoles(err,Ret_pRoleListRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pRoleListRecordSet,"Ret_pRoleListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetRoleAccessRightSets(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pAccessRightsListRecordSet;
	int  RoleID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetRoleAccessRightSets(err,Ret_pAccessRightsListRecordSet,RoleID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pAccessRightsListRecordSet,"Ret_pAccessRightsListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetAccessRightsSets(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pAccessRightsSetListRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->GetAccessRightsSets(err,Ret_pAccessRightsSetListRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pAccessRightsSetListRecordSet,"Ret_pAccessRightsSetListRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::DeleteRole(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  RoleID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->DeleteRole(err,RoleID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::DeleteAccRSetFromRole(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  RoleID;
	int  AccRSetID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&AccRSetID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->DeleteAccRSetFromRole(err,RoleID,AccRSetID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::InsertNewAccRSetToRole(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  RoleID;
	int  AccRSetID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&AccRSetID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->InsertNewAccRSetToRole(err,RoleID,AccRSetID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::InsertNewRole(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=4)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  Ret_nNewRoleRowID;
	QString  RoleName;
	QString  RoleDescription;
	int  RoleType;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RoleName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&RoleDescription,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&RoleType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->InsertNewRole(err,Ret_nNewRoleRowID,RoleName,RoleDescription,RoleType);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_nNewRoleRowID,"Ret_nNewRoleRowID");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::RenameRole(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=4)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  RoleID;
	QString  NewRoleName;
	QString  NewRoleDescription;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&NewRoleName,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&NewRoleDescription,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->RenameRole(err,RoleID,NewRoleName,NewRoleDescription);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetAccessRights(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_pAccessRightsRecordSet;
	int  RoleID;
	int  AccRSetID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RoleID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&AccRSetID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetAccessRights(err,Ret_pAccessRightsRecordSet,RoleID,AccRSetID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pAccessRightsRecordSet,"Ret_pAccessRightsRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::SetAccessRightValue(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  AccRightID;
	QString  Value;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&AccRightID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&Value,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->SetAccessRightValue(err,AccRightID,Value);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::GetDefaultRoles(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  RoleType;
	DbRecordSet  Ret_pDefaultRoleRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RoleType,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->GetDefaultRoles(err,RoleType,Ret_pDefaultRoleRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_pDefaultRoleRecordSet,"Ret_pDefaultRoleRecordSet");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::ChangeAccessRights(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  ChangedRightsRecordSet;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&ChangedRightsRecordSet,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->ChangeAccessRights(err,ChangedRightsRecordSet);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::ReadGUAR(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nRecord;
	int  nTableID;
	DbRecordSet  Ret_UAR;
	DbRecordSet  Ret_GAR;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nRecord,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&nTableID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->ReadGUAR(err,nRecord,nTableID,Ret_UAR,Ret_GAR);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_UAR,"Ret_UAR");
	rpc.msg_out->AddParameter(&Ret_GAR,"Ret_GAR");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::WriteGUAR(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=5)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nRecord;
	int  nTableID;
	DbRecordSet  RetOut_UAR;
	DbRecordSet  RetOut_GAR;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nRecord,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&nTableID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&RetOut_UAR,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&RetOut_GAR,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->WriteGUAR(err,nRecord,nTableID,RetOut_UAR,RetOut_GAR);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_UAR,"RetOut_UAR");
	rpc.msg_out->AddParameter(&RetOut_GAR,"RetOut_GAR");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::PrepareGUAR(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=5)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nRecord;
	int  nTableID;
	DbRecordSet  RetOut_UAR;
	DbRecordSet  RetOut_GAR;
	DbRecordSet  Ret_lstGroupUsers;
	int  Ret_ParentRecord;
	int  Ret_ParentTableID;
	QString  Ret_ParentName;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nRecord,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&nTableID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&RetOut_UAR,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&RetOut_GAR,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->PrepareGUAR(err,nRecord,nTableID,RetOut_UAR,RetOut_GAR,Ret_lstGroupUsers,Ret_ParentRecord,Ret_ParentTableID,Ret_ParentName);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_UAR,"RetOut_UAR");
	rpc.msg_out->AddParameter(&RetOut_GAR,"RetOut_GAR");
	rpc.msg_out->AddParameter(&Ret_lstGroupUsers,"Ret_lstGroupUsers");
	rpc.msg_out->AddParameter(&Ret_ParentRecord,"Ret_ParentRecord");
	rpc.msg_out->AddParameter(&Ret_ParentTableID,"Ret_ParentTableID");
	rpc.msg_out->AddParameter(&Ret_ParentName,"Ret_ParentName");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::TestUAR(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=6)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nTableID;
	int  nPersonID;
	int  nAccessLevel;
	DbRecordSet  lstTest_IDs;
	DbRecordSet  Ret_lstResult_IDs;
	bool  bReturnAllowed;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nTableID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&nPersonID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(3,&nAccessLevel,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(4,&lstTest_IDs,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(5,&bReturnAllowed,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->TestUAR(err,nTableID,nPersonID,nAccessLevel,lstTest_IDs,Ret_lstResult_IDs,bReturnAllowed);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_lstResult_IDs,"Ret_lstResult_IDs");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::UpdateProjectGUAR(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  lstProject_IDs;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&lstProject_IDs,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->UpdateProjectGUAR(err,lstProject_IDs);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::UpdateGroupContactsGUAR(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  lstData;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&lstData,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->UpdateGroupContactsGUAR(err,lstData);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_AccessRights::UpdateProjectCommEntityGUAR(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nTableID;
	DbRecordSet  lstData;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nTableID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&lstData,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->UpdateProjectCommEntityGUAR(err,nTableID,lstData);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

