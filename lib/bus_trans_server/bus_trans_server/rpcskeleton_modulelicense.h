#ifndef RPCSKELETON_MODULELICENSE_H__
#define RPCSKELETON_MODULELICENSE_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_modulelicense.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_ModuleLicense : public RpcSkeleton
{

typedef void (RpcSkeleton_ModuleLicense::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_ModuleLicense(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void GetAllModuleLicenseData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetFPList(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetLicenseInfo(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_ModuleLicense *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_MODULELICENSE_H__
