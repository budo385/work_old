#ifndef PINGHTTPSERVER_H
#define PINGHTTPSERVER_H

#include "trans/trans/httphandler.h"

class PingHTTPServer : public HTTPHandler
{
public:
	PingHTTPServer();
	~PingHTTPServer();

	void HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);

private:
	void AuthenticateUser(Status &Ret_Status,QString strUsername,QString strAuthToken,QString strClientNonce);	
	void SetResponseHeader(HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText);
};

#endif // PINGHTTPSERVER_H
