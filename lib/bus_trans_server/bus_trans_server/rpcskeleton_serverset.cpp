#include "rpcskeleton_serverset.h"


/*!
	Constructor: Makes instances of all skeleton objects, puts them in set.
	\param pHandler is handler to Business object set
	\param RPCType is constant defining what RPC protocol to use: 
	RPC_PROTOCOL_TYPE_XML_RPC, RPC_PROTOCOL_TYPE_SOAP, etc..defiend in rpcprotocol.h
*/

RpcSkeletonServerSet::RpcSkeletonServerSet(BusinessServiceSet *pHandler,int RPCType)
:RpcSkeletonDispatcher(RPCType)
{

	//---------------------------------------------------
	//	Make instances of each subskeleton object, init to point to its parent object and store in list
	//---------------------------------------------------

	UserLogon=new RpcSkeleton_UserLogon(pHandler,RPCType);m_SkeletonList.append(UserLogon);
	ModuleLicense=new RpcSkeleton_ModuleLicense(pHandler,RPCType);m_SkeletonList.append(ModuleLicense);
	BusEventLog=new RpcSkeleton_BusEventLog(pHandler,RPCType);m_SkeletonList.append(BusEventLog);
	AccessRights=new RpcSkeleton_AccessRights(pHandler,RPCType);m_SkeletonList.append(AccessRights);
	MainEntitySelector =new RpcSkeleton_MainEntitySelector(pHandler,RPCType);m_SkeletonList.append(MainEntitySelector);
	CoreServices = new RpcSkeleton_CoreServices(pHandler,RPCType);m_SkeletonList.append(CoreServices);
	BusContact=new RpcSkeleton_BusContact(pHandler,RPCType);m_SkeletonList.append(BusContact);
	VoiceCallCenter =new RpcSkeleton_VoiceCallCenter(pHandler,RPCType);m_SkeletonList.append(VoiceCallCenter);
	BusEmail =new RpcSkeleton_BusEmail(pHandler,RPCType);m_SkeletonList.append(BusEmail);
	Reports = new RpcSkeleton_Reports(pHandler,RPCType);m_SkeletonList.append(Reports);
	BusAddressSchemas = new RpcSkeleton_BusAddressSchemas(pHandler,RPCType);m_SkeletonList.append(BusAddressSchemas);
	ClientSimpleORM = new RpcSkeleton_ClientSimpleORM(pHandler,RPCType);m_SkeletonList.append(ClientSimpleORM);
	BusImport = new RpcSkeleton_BusImport(pHandler,RPCType);m_SkeletonList.append(BusImport);
	StoredProjectLists = new RpcSkeleton_StoredProjectLists(pHandler,RPCType);m_SkeletonList.append(StoredProjectLists);
	BusGridViews = new RpcSkeleton_BusGridViews(pHandler,RPCType);m_SkeletonList.append(BusGridViews);
	BusNMRX = new RpcSkeleton_BusNMRX(pHandler,RPCType);m_SkeletonList.append(BusNMRX);
	BusGroupTree = new RpcSkeleton_BusGroupTree(pHandler,RPCType);m_SkeletonList.append(BusGroupTree);
	BusDocuments = new RpcSkeleton_BusDocuments(pHandler,RPCType);m_SkeletonList.append(BusDocuments);
	BusCommunication = new RpcSkeleton_BusCommunication(pHandler,RPCType);m_SkeletonList.append(BusCommunication);
	ServerControl = new RpcSkeleton_ServerControl(pHandler,RPCType);m_SkeletonList.append(ServerControl);
	BusProject = new RpcSkeleton_BusProject(pHandler,RPCType);m_SkeletonList.append(BusProject);
	BusPerson = new RpcSkeleton_BusPerson(pHandler,RPCType);m_SkeletonList.append(BusPerson);
	BusSms = new RpcSkeleton_BusSms(pHandler,RPCType);m_SkeletonList.append(BusSms);
	BusCalendar = new RpcSkeleton_BusCalendar(pHandler,RPCType);m_SkeletonList.append(BusCalendar);
	BusCustomFields = new RpcSkeleton_BusCustomFields(pHandler,RPCType);m_SkeletonList.append(BusCustomFields);
}

