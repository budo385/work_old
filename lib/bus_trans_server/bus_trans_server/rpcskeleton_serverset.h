#ifndef RPCSKELETON_SERVERSET_H
#define RPCSKELETON_SERVERSET_H


#include <QtCore>
#include "common/common/status.h"
#include "trans/trans/rpcskeletondispatcher.h"
#include "rpcskeleton_collection.h"
#include "bus_server/bus_server/businessserviceset.h"


/*!
    \class RpcSkeletonServerSet
    \brief Rpc skeleton set of skeleton objects
    \ingroup Bus_Trans_Server

	Used by RpcServer for dispatching RPC messages to right skeleton object
*/

class RpcSkeletonServerSet: public RpcSkeletonDispatcher
{
public:
	RpcSkeletonServerSet(BusinessServiceSet *pHandler,int RPCType);
		
private:
	
	//Put your pointer to skeleton business object here:
	RpcSkeleton_UserLogon *UserLogon;
	RpcSkeleton_ModuleLicense *ModuleLicense;
	RpcSkeleton_BusEventLog *BusEventLog;
	RpcSkeleton_AccessRights *AccessRights;
	RpcSkeleton_MainEntitySelector *MainEntitySelector;
	RpcSkeleton_CoreServices *CoreServices;
	RpcSkeleton_Reports *Reports;
	RpcSkeleton_BusContact *BusContact;
	RpcSkeleton_BusAddressSchemas *BusAddressSchemas;
	RpcSkeleton_VoiceCallCenter *VoiceCallCenter;
	RpcSkeleton_BusEmail *BusEmail;
	RpcSkeleton_ClientSimpleORM *ClientSimpleORM;
	RpcSkeleton_BusImport *BusImport;
	RpcSkeleton_StoredProjectLists *StoredProjectLists;
	RpcSkeleton_BusGridViews *BusGridViews;
	RpcSkeleton_BusNMRX *BusNMRX;
	RpcSkeleton_BusGroupTree *BusGroupTree;
	RpcSkeleton_BusDocuments *BusDocuments;
	RpcSkeleton_BusCommunication *BusCommunication;
	RpcSkeleton_ServerControl	*ServerControl;
	RpcSkeleton_BusProject	*BusProject;
	RpcSkeleton_BusPerson *BusPerson;
	RpcSkeleton_BusSms	*BusSms;
	RpcSkeleton_BusCalendar	*BusCalendar;
	RpcSkeleton_BusCustomFields	*BusCustomFields;

};


#endif RPCSKELETON_SERVERSET_H
