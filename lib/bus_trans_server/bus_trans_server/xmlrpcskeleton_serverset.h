#ifndef XMLRPCSKELETON_SERVERSET_H
#define XMLRPCSKELETON_SERVERSET_H


#include "bus_trans_server/bus_trans_server/rpcskeleton_serverset.h"


/*!
    \class XmlRpcSkeletonServerSet
    \brief Xml Rpc skeleton set of skeleton objects
    \ingroup Bus_Trans_Server

	Used by XmlRpcHttpServer for dispatching RPC messages to right skeleton object
*/

class XmlRpcSkeletonServerSet : public RpcSkeletonServerSet
{
public:
	XmlRpcSkeletonServerSet(BusinessServiceSet *pHandler):RpcSkeletonServerSet(pHandler,RPC_PROTOCOL_TYPE_XML_RPC){};
};


#endif //XMLRPCSKELETON_SERVERSET_H
