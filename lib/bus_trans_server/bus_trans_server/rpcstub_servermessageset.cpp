#include "rpcstub_servermessageset.h"
#include "common/common/config_version_sc.h"


/*!
	Constructor: init with HTTP server
	\param pHttpServer - http server
	\param RPCType is constant defining what RPC protocol to use: 
	RPC_PROTOCOL_TYPE_XML_RPC, RPC_PROTOCOL_TYPE_SOAP, etc..defiend in rpcprotocol.h
*/
RpcStubServerMessageSet::RpcStubServerMessageSet(HTTPServer *pHttpServer,int RPCType)
:RpcStub(RPCType)
{
	m_HttpServer=pHttpServer;

	m_HttpHeader.InitRpcProtocol(this);
	m_HttpHeader.SetHTTPTags(QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION),QString(CLIENT_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));

}



//-------------------------------------------------------------------------
//					SERVER STUBS
//-------------------------------------------------------------------------

/*!
	Test stub.
	- As server can only send mesages to client if conection is open, there is no need for sessioning
	- no returned status: fire & go
	- thread is stopped until message is written to socket, no wait for any errors

	\param pStatus			- returns any error
	\param nSocketID		- socket ID of client to whom msg will be sent
	\param strServerMessage - our TEST message
*/
void RpcStubServerMessageSet::ServerMsg(Status pStatus, QList<int> lstSocketID, DbRecordSet rowServerMessage)
{

	RpcStubMessageHandler msg(this);
	
	//create param list:
	msg.msg_in->AddParameter(&rowServerMessage);

	//generate rpc call:
	msg.msg_in->GenerateRPC("ServerMessages.ServerMsg",pStatus);
		if (!pStatus.IsOK())return;
		

	//prepare req header:
	m_HttpHeader.SetHttpRequestHeader(msg.HttpRequest);

	//make call to server:
	if(lstSocketID.size()==0)
	{
		QByteArray pBuffer;
		msg.HttpRequest.setValue("content-encoding","deflate");
		msg.HttpRequest.GetData(pBuffer);	
		m_HttpServer->SendMessageBroadCast(&pBuffer);
	}
	else
		m_HttpServer->SendMessage(lstSocketID,&msg.HttpRequest);
		

}


