#include "userstoragehttpserver_sokrates.h"
#include "common/common/authenticator.h"
#include "common/common/datahelper.h"
#include <QUrlQuery>

#include "bus_server/bus_server/privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;



bool UserStorageHTTPServer_Sokrates::AuthenticateUser(QString strCookieValue,QString &strSessionID)
{
	return g_UserSessionManager->AuthenticateUser(strCookieValue,strSessionID);
}


QString UserStorageHTTPServer_Sokrates::InitializeUserStorageDirectory(Status &status,QString strUserSubDir)
{
	return g_UserSessionManager->InitializeUserStorageDirectory(status,strUserSubDir);
}

void UserStorageHTTPServer_Sokrates::CheckPermission(Status &pStatus,QString strSession,int nMethodType,QString strAliasDir,QString strDirPath,QString strFileName,qint64 nFileSizeForUpload)
{
	if (nMethodType==METHOD_TYPE_UPLOAD)
	{
		if(!g_UserSessionManager->IsEnoughDiskSpaceForUserStorage(nFileSizeForUpload)) 
		{
			pStatus.setError(1,"Disk Quota reached, file can not be written!");
		}
	}
}

void UserStorageHTTPServer_Sokrates::GetDirectoryPath(Status &status,const QUrl &url,QString strSession,QString &strDirPath)
{
	QUrlQuery urlQuery= QUrlQuery(url);
	QString method=urlQuery.queryItemValue("method").toLatin1(); 

	if (method=="downloadbackup" || method=="uploadbackup")
	{
		strDirPath=m_strBackupDirectoryPath;
	}
	else
	{
		UserStorageHTTPServer::GetDirectoryPath(status,url,strSession,strDirPath);
	}
}
