#include "pinghttpserver.h"
#include "trans/trans/config_trans.h"
#include "common/common/config_version_sc.h"
#include "common/common/authenticator.h"
#include <QUrlQuery>

#include "bus_server/bus_server/privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;

PingHTTPServer::PingHTTPServer()
{
	SetDomain(DOMAIN_HTTP_PING);
}

PingHTTPServer::~PingHTTPServer()
{

}


/*!
	Invoked by server, new  thread for user request.
	Request is authenticated, if authentication fails: kick, if session not found, return http ok, but error inside body.

	/param status	- returned status (defines how http server will handle response)
	/param ctx		- HTTP Context   - some HTTP/IP info on client
	/param request	- HTTP request	- client request
	/param response - HTTP response - server response
*/
void PingHTTPServer::HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{
	QUrlQuery url= QUrlQuery(QUrl(request.path()));
	QString strUsername=url.queryItemValue("user");
	//QByteArray token=QUrl::fromPercentEncoding(url.queryItemValue("token").toLatin1()).toLatin1();
	//QByteArray nonce=QUrl::fromPercentEncoding(url.queryItemValue("nonce").toLatin1()).toLatin1();
	QString parent_session=QUrl::fromPercentEncoding(url.queryItemValue("parent_session").toLatin1()); //sesion to be tested: http request must originate from same IP address!!

	QString strToken;
	QString strNonce;

	QString strAuth=request.value("auth");
	QStringList lstTokens=strAuth.split("\"",QString::SkipEmptyParts);
	if (lstTokens.size()>=4)
	{
		strNonce=lstTokens.at(1);
		strToken=lstTokens.at(3);
	}


	AuthenticateUser(status,strUsername,strToken,strNonce);
	if (!status.IsOK()) //failed to login: kick him
	{
		SetResponseHeader(response,401,"Unauthorized");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN_NCOUNT);
		//status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
		//response.GetBody()->append("Unauthorized"); //return error...
		return;
	}

	//test if session is alive:
	QString strIP=g_UserSessionManager->GetClientIP(parent_session);
	if (strIP==ctx.m_strPeerIP) //IP address found and match, session is active, all OK!
	{
		SetResponseHeader(response,200,"OK");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
		response.GetBody()->append("OK");
	}
	else if (strIP.isEmpty()) //IP address not found, session is dead!?
	{
		SetResponseHeader(response,200,"OK");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP); //respond to client, close connection
		response.GetBody()->append("Session Not Found");
		return;
	}
	else //IP address is different then original session
	{
		SetResponseHeader(response,401,"Unauthorized");
		status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN); //intruder alert: kick at once!
		response.GetBody()->append("Unauthorized");
		return;
	}

}

//--------------------------------------------------------
// AUTHENTICATE USER
//--------------------------------------------------------

void PingHTTPServer::AuthenticateUser(Status &Ret_Status,QString strUsername,QString strAuthToken,QString strClientNonce)
{
	Ret_Status.setError(0);

	//load from DB username, password, group, 
	DbRecordSet rowUserData;
	g_PrivateServiceSet->CoreUser->GetUserByUserName(Ret_Status,strUsername,rowUserData);
	if(!Ret_Status.IsOK())return;

	//is user exists:
	if(rowUserData.getRowCount()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	//is user active (first test core then bus):
	if(rowUserData.getDataRef(0,"CUSR_ACTIVE_FLAG").toInt()!=1)
		{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_ACTIVE);return;}
	else if(rowUserData.getDataRef(0,"BPER_ID").toInt()!=0 && rowUserData.getDataRef(0,"BPER_ACTIVE_FLAG").toInt()==0)
		{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_ACTIVE);return;}


	//test password:
	QByteArray bytePassword=rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray();
	if(!Authenticator::AuthenticateFromWebService(strClientNonce,strAuthToken, strUsername,bytePassword))
		{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

}

//--------------------------------------------------------
// RESPONSE HEADER
//--------------------------------------------------------

void PingHTTPServer::SetResponseHeader(HTTPResponse &response,int nHTTPStatusCode, QString strHTTPCodeText)
{
	//set response header, content length & compression is done in htttp server
	response.setStatusLine(nHTTPStatusCode,strHTTPCodeText);
	response.addValue("Server",QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
	response.addValue("Date",QDateTime::currentDateTime().toString(Qt::ISODate));
	response.setContentType("text/plain");
}

