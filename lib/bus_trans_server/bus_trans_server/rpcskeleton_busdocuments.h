#ifndef RPCSKELETON_BUSDOCUMENTS_H__
#define RPCSKELETON_BUSDOCUMENTS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busdocuments.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusDocuments : public RpcSkeleton
{

typedef void (RpcSkeleton_BusDocuments::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusDocuments(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadUserPaths(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadUserPathsForApplication(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadTemplates(Status &err,RpcSkeletonMessageHandler &rpc);
	void Write(Status &err,RpcSkeletonMessageHandler &rpc);
	void Read(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteDocument(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteApplication(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadNoteDocument(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReloadRevisions(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckIn(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckOut(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckOutToUserStorage(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckInFromUserStorage(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckInFromEmailAttachment(Status &err,RpcSkeletonMessageHandler &rpc);
	void CancelCheckOut(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckOutRevision(Status &err,RpcSkeletonMessageHandler &rpc);
	void UpdateRevision(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteRevision(Status &err,RpcSkeletonMessageHandler &rpc);
	void IsCheckedOut(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetDocumentFileNameAndSize(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetDocumentProjectAndContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetCheckOutPath(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusDocuments *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSDOCUMENTS_H__
