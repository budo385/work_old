#ifndef RPCSKELETON_WEBCALENDARS_H__
#define RPCSKELETON_WEBCALENDARS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_webcalendars.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_WebCalendars : public RpcSkeleton
{

typedef void (RpcSkeleton_WebCalendars::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );

public:
	RpcSkeleton_WebCalendars(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadCalendars(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);

	Interface_WebCalendars *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_WEBCALENDARS_H__
