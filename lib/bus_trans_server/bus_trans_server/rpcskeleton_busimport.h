#ifndef RPCSKELETON_BUSIMPORT_H__
#define RPCSKELETON_BUSIMPORT_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busimport.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusImport : public RpcSkeleton
{

typedef void (RpcSkeleton_BusImport::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusImport(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ImportProjects(Status &err,RpcSkeletonMessageHandler &rpc);
	void ImportPersons(Status &err,RpcSkeletonMessageHandler &rpc);
	void ImportContacts(Status &err,RpcSkeletonMessageHandler &rpc);
	void ImportUserContactRelationships(Status &err,RpcSkeletonMessageHandler &rpc);
	void ImportContactContactRelationships(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetContactContactRelationships(Status &err,RpcSkeletonMessageHandler &rpc);
	void ImportContactCustomData(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessProjects_Import(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessProjects_Export(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessUsers_Import(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessUsers_Export(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessDebtors_Export(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessContacts_Import(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessContacts_Export(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessContactContactRelations_Export(Status &err,RpcSkeletonMessageHandler &rpc);
	void ProcessStoredProjectList_Import(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusImport *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSIMPORT_H__
