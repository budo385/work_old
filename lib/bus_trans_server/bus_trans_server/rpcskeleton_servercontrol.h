#ifndef RPCSKELETON_SERVERCONTROL_H__
#define RPCSKELETON_SERVERCONTROL_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_servercontrol.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_ServerControl : public RpcSkeleton
{

typedef void (RpcSkeleton_ServerControl::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_ServerControl(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void GetBackupData(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetBackupData(Status &err,RpcSkeletonMessageHandler &rpc);
	void LoadBackupList(Status &err,RpcSkeletonMessageHandler &rpc);
	void Backup(Status &err,RpcSkeletonMessageHandler &rpc);
	void Restore(Status &err,RpcSkeletonMessageHandler &rpc);
	void LoadTemplateList(Status &err,RpcSkeletonMessageHandler &rpc);
	void RestoreFromTemplate(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetTemplateData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetTemplateData(Status &err,RpcSkeletonMessageHandler &rpc);
	void LoadBackupFile(Status &err,RpcSkeletonMessageHandler &rpc);
	void SaveBackupFile(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteBackupFile(Status &err,RpcSkeletonMessageHandler &rpc);
	void SaveImportExportSettings(Status &err,RpcSkeletonMessageHandler &rpc);
	void LoadImportExportSettings(Status &err,RpcSkeletonMessageHandler &rpc);
	void StartProcessProject(Status &err,RpcSkeletonMessageHandler &rpc);
	void StartProcessUser(Status &err,RpcSkeletonMessageHandler &rpc);
	void StartProcessDebtor(Status &err,RpcSkeletonMessageHandler &rpc);
	void StartProcessContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void StartProcessStoredProjectList(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetLog(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetLogData(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetLogData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetAllActiveRemindersForUser(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteReminders(Status &err,RpcSkeletonMessageHandler &rpc);
	void PostPoneReminders(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_ServerControl *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_SERVERCONTROL_H__
