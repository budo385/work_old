#ifndef RPCSKELETON_REPORTS_H__
#define RPCSKELETON_REPORTS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_reports.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_Reports : public RpcSkeleton
{

typedef void (RpcSkeleton_Reports::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_Reports(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void GetReportRecordSet(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_Reports *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_REPORTS_H__
