#ifndef RPCSKELETON_BUSGROUPTREE_H__
#define RPCSKELETON_BUSGROUPTREE_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busgrouptree.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusGroupTree : public RpcSkeleton
{

typedef void (RpcSkeleton_BusGroupTree::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusGroupTree(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadTree(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteTree(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteTree(Status &err,RpcSkeletonMessageHandler &rpc);
	void LockTree(Status &err,RpcSkeletonMessageHandler &rpc);
	void UnLockTree(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadGroupTree(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteGroupItem(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteGroupItem(Status &err,RpcSkeletonMessageHandler &rpc);
	void ChangeGroupContent(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadGroupContent(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteGroupItemData(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteGroupContentData(Status &err,RpcSkeletonMessageHandler &rpc);
	void RemoveGroupContentItem(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetChildrenGroups(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusGroupTree *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSGROUPTREE_H__
