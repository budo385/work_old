#ifndef RPCSKELETON_BUSCONTACT_H__
#define RPCSKELETON_BUSCONTACT_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_buscontact.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusContact : public RpcSkeleton
{

typedef void (RpcSkeleton_BusContact::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusContact(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadShortContactList(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactDataSideBar(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteContacts(Status &err,RpcSkeletonMessageHandler &rpc);
	void BatchReadContactData(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadNMRXContactsFromContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadNMRXContactsFromContact_Ext(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadNMRXUsersFromContact_Ext(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadNMRXContactsFromProject(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadNMRXProjects(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactDefaults(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactReminderData(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactDataForVoiceCall(Status &err,RpcSkeletonMessageHandler &rpc);
	void UpdateOrganization(Status &err,RpcSkeletonMessageHandler &rpc);
	void AddContactEmail(Status &err,RpcSkeletonMessageHandler &rpc);
	void CreatePersonFromContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadBigPicture(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteBigPicture(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteEntityPictures(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadEntityPictures(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadPreviewPicture(Status &err,RpcSkeletonMessageHandler &rpc);
	void AssignPictureContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void RemoveAssignedPictureContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactJournals(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteJournal(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteJournal(Status &err,RpcSkeletonMessageHandler &rpc);
	void LockJournal(Status &err,RpcSkeletonMessageHandler &rpc);
	void UnlockJournal(Status &err,RpcSkeletonMessageHandler &rpc);
	void CreateDefaultPerson(Status &err,RpcSkeletonMessageHandler &rpc);
	void CopyPerson2OrgContact(Status &err,RpcSkeletonMessageHandler &rpc);
	void FindDuplicates(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetMaximumDebtorCode(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadData(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteData(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteDataFromImport(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteGroupAssignments(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactGroup(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactsFromGroup(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadActiveContactsFromGroups(Status &err,RpcSkeletonMessageHandler &rpc);
	void WritePhones(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactPhones(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadContactAddress(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteAddress(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetAllContactsOfOrganizationByAddress(Status &err,RpcSkeletonMessageHandler &rpc);
	void UpdateContactsOfOrganizationByAddress(Status &err,RpcSkeletonMessageHandler &rpc);
	void AssignDebtors2Project(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusContact *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSCONTACT_H__
