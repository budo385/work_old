#ifndef RPCSKELETON_BUSPERSON_H__
#define RPCSKELETON_BUSPERSON_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busperson.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusPerson : public RpcSkeleton
{

typedef void (RpcSkeleton_BusPerson::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusPerson(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadData(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetPersonProjectListID(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusPerson *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSPERSON_H__
