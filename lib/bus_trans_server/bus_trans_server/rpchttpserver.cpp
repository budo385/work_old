#include "rpchttpserver.h"
#include "common/common/config_version_sc.h"
#include "common/common/threadid.h"

#include "common/common/logger.h"
extern Logger				g_Logger;


/*!
	Constructor: stores pointers to skeleton set & user session manager

	\param pServerSkeletonSet	- skeleton set
	\param pSessionManager   - session manager
	\param pNumRPCProtocolType	- defining what RPC protocol to use: 
	RPC_PROTOCOL_TYPE_XML_RPC, RPC_PROTOCOL_TYPE_SOAP, etc..defiend in rpcprotocol.h

*/
RpcHTTPServer::RpcHTTPServer(RpcSkeleton *pServerSkeletonSet,UserSessionManager* pSessionManager, int pNumRPCProtocolType)
:RpcProtocol(pNumRPCProtocolType)
{
	//pointers:
	m_ServerSkeletonSet=pServerSkeletonSet;
	m_SessionManager=pSessionManager;

	//set domain:
	SetDomain(GetRPCDomain());

	m_HttpHeader.InitRpcProtocol(this);
	m_HttpHeader.SetHTTPTags(QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION),QString(CLIENT_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
}


/*!
	Invoked by server, new  thread for user request.
	Request is dispatched to the registered m_ServerSkeletonSet.

	/param status	- returned status (defines how http server will handle response)
	/param ctx		- HTTP Context   - some HTTP/IP info on client
	/param request	- HTTP request	- client request
	/param response - HTTP response - server response
*/
void RpcHTTPServer::HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{

	bool bIsLogin;
	QString strSession;
	
	status.setError(0);

	//qDebug()<<"thread: "<<ThreadIdentificator::GetCurrentThreadID();
	//qDebug()<<request.toString();
	//qDebug()<<request.GetBody()->constData();

	//get rpc's
	RpcSkeletonMessageHandler rpc(this,request.GetBody(),response.GetBody(),0);

	if(request.value("Accept-Encoding")=="deflate") //if request msg is compressed, use it back
		response.setValue("Content-Encoding","deflate");

	//is it login?
	QString strMethodName=rpc.GetSkeletonMethodName(); 
	if (strMethodName.isEmpty()) //invalid RPC request -> KICKBAN (HACKER ALERT)
	{
		status.setError(StatusCodeSet::ERR_RPC_INVALID_REQUEST);
		g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SECURITY_RPC_INVALID_REQUEST,ctx.m_strPeerIP);
		ErrorResponseHandler(status,rpc);
		m_HttpHeader.SetHttpResponseHeader(response);
		return;
	}

	//If LOGIN then mark flag
	if(strMethodName=="Login")
		bIsLogin=true;
	else
		bIsLogin=false;

	//get session id as 1st param
	rpc.msg_in->GetParameter(0,&strSession,status);
	if (!status.IsOK())
	{
		ErrorResponseHandler(status,rpc);
		m_HttpHeader.SetHttpResponseHeader(response);
		return;
	}


	//validate session
	int nClientTimeZoneOffsetMinutes=0;
	m_SessionManager->ValidateUserRequest(status,strSession,nClientTimeZoneOffsetMinutes,ctx.m_strPeerIP,bIsLogin,ctx.m_nSocketID);
	if (!status.IsOK())
	{
		ErrorResponseHandler(status,rpc);
		m_HttpHeader.SetHttpResponseHeader(response);
		return;
	}

	m_SessionManager->LockThreadActive();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start("+ctx.m_strPeerIP+":"+QVariant(ctx.m_nSocketID).toString()+"): "+rpc.GetRequestMethodNameSpace()+"->"+rpc.GetSkeletonMethodName()+" xmlrpc unzip: "+QVariant(request.GetBody()->size()).toString());

	//if green light go to skeleton
	Status  err; //err is on BO layer or XML (parse failed or funct not found): not used here
	m_ServerSkeletonSet->HandleRPC(err,request.GetBody(),response.GetBody(),rpc.GetRequestMethodNameSpace(),nClientTimeZoneOffsetMinutes);

	m_SessionManager->UnLockThreadActive();
	
	//if response empty, notify http server to keep con, but dont send anythinf back
	if(response.GetBody()->isEmpty())
		status.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION_NO_RESP);
	else
	{
		m_HttpHeader.SetHttpResponseHeader(response);
	}

	//If LOGOUT then close connection: (do not close connection: wait for client to do that, or http server will close at garbage)
	//if(strMethodName=="Logout")
	//	status.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Processed("+ctx.m_strPeerIP+":"+QVariant(ctx.m_nSocketID).toString()+"): "+rpc.GetRequestMethodNameSpace()+"->"+rpc.GetSkeletonMethodName()+" xmlrpc unzip: "+QVariant(response.GetBody()->size()).toString());
	//qDebug()<<"Server response:";
	//qDebug()<<response.toString()<<"\r\n"<<response.GetBody()->constData();
	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,response.toString());
	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,response.GetBody()->constData());

}


/*!
	When error occurs, repsonse RPC message must be generated, also http server must be isntructed

	/param status	- contains last error, error is mapped to the HTTPServer errors
	/param rpc		- rpc messages
*/
void RpcHTTPServer::ErrorResponseHandler(Status &pStatus, RpcSkeletonMessageHandler &rpc)
{

	switch(pStatus.getErrorCode())
	{
		case 0:	// no error
			return;
		case StatusCodeSet::ERR_SYSTEM_KICKBAN:
			pStatus.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN,pStatus.getErrorTextRaw());
			return;
		case StatusCodeSet::ERR_SYSTEM_REAUTHENTICATE:
			pStatus.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_SYSTEM_REAUTHENTICATE,"");
			return;
		case StatusCodeSet::ERR_SYSTEM_REDIRECT:
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_SYSTEM_REDIRECT,pStatus.getErrorTextRaw());
			pStatus.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
			return;
		case StatusCodeSet::ERR_SYSTEM_SERVERFULL:
			pStatus.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_SYSTEM_SERVERFULL);
			return;
		case StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED:
			pStatus.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_SEND_RESP);
			rpc.msg_out->GenerateFault(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
			return;
		case StatusCodeSet::ERR_RPC_INVALID_REQUEST:									//flooding with damaged RPC messaging: can be transport error, give man a chance
			pStatus.setError(StatusCodeSet::COM_HTTP_CLOSE_CONNECTION_KICKBAN_NCOUNT);
			return;

		default:
			rpc.msg_out->GenerateFault(pStatus.getErrorCode(),pStatus.getErrorTextRaw());
			pStatus.setError(StatusCodeSet::COM_HTTP_KEEP_CONNECTION);
			return;
	}


}



