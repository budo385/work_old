/**
 *
 * \defgroup Bus_Trans_Server Business transport server side layer
 * 
 * \ingroup Bus_Services_All
 *
 * Contains collection of RPC based skeletons for server side
 *
 *
*/