#ifndef RPCSKELETON_CLIENTSIMPLEORM_H__
#define RPCSKELETON_CLIENTSIMPLEORM_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_clientsimpleorm.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_ClientSimpleORM : public RpcSkeleton
{

typedef void (RpcSkeleton_ClientSimpleORM::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_ClientSimpleORM(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Write(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteParentSubData(Status &err,RpcSkeletonMessageHandler &rpc);
	void Read(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadAdv(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadFromParentIDs(Status &err,RpcSkeletonMessageHandler &rpc);
	void Delete(Status &err,RpcSkeletonMessageHandler &rpc);
	void Lock(Status &err,RpcSkeletonMessageHandler &rpc);
	void UnLock(Status &err,RpcSkeletonMessageHandler &rpc);
	void UnLockByRecordID(Status &err,RpcSkeletonMessageHandler &rpc);
	void IsLocked(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetRowCount(Status &err,RpcSkeletonMessageHandler &rpc);
	void ExecuteSQL(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteHierarchicalData(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteHierarchicalData(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_ClientSimpleORM *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_CLIENTSIMPLEORM_H__
