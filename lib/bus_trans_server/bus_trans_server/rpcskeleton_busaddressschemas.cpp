#include "rpcskeleton_busaddressschemas.h"

RpcSkeleton_BusAddressSchemas::RpcSkeleton_BusAddressSchemas(BusinessServiceSet *pHandler,int RPCType):
	RpcSkeleton(RPCType)
{
	m_Handler=pHandler->BusAddressSchemas; //store handler to actual business object
	SetNameSpace("BusAddressSchemas"); //set namespace (business object name)

	//List all methods
	mFunctList.insert("Write",&RpcSkeleton_BusAddressSchemas::Write);
	mFunctList.insert("Read",&RpcSkeleton_BusAddressSchemas::Read);
	mFunctList.insert("Delete",&RpcSkeleton_BusAddressSchemas::Delete);
	mFunctList.insert("Lock",&RpcSkeleton_BusAddressSchemas::Lock);
	mFunctList.insert("Unlock",&RpcSkeleton_BusAddressSchemas::Unlock);
}

bool RpcSkeleton_BusAddressSchemas::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)
{
	//init locals:
	if(!TestNameSpace(strRPCMethod))return false;

	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);

	//get methodname, test for error
	QString strMethodName=rpc.GetSkeletonMethodName();
	if (strMethodName.isEmpty()){rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);return true;}

	//METHOD DISPATCHER
	QHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodName);
	if(i != mFunctList.end() && i.key() == strMethodName)
	{
		//call skeleton method
		(this->*i.value())(err,rpc);
		return true;
	}
	else
	{
		//if not found return error:
		err.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
		return true;
	}
	return true;
}

void RpcSkeleton_BusAddressSchemas::Write(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=3)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  RetOut_Data;
	QString  strLockRes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&RetOut_Data,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
	rpc.msg_in->GetParameter(2,&strLockRes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->Write(err,RetOut_Data,strLockRes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&RetOut_Data,"RetOut_Data");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusAddressSchemas::Read(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=1)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	DbRecordSet  Ret_Data;

	//getParam (Only incoming & RETURNED-OUTGOING) from client

	//invoke handler
	m_Handler->Read(err,Ret_Data);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_Data,"Ret_Data");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusAddressSchemas::Delete(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nRecordID;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nRecordID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->Delete(err,nRecordID);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusAddressSchemas::Lock(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	int  nRecordID;
	QString  Ret_strLockRes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&nRecordID,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->Lock(err,nRecordID,Ret_strLockRes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:
	rpc.msg_out->AddParameter(&Ret_strLockRes,"Ret_strLockRes");

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

void RpcSkeleton_BusAddressSchemas::Unlock(Status &err,RpcSkeletonMessageHandler &rpc)
{
	//test parameter count
	if(rpc.msg_in->GetParameterCount()!=2)
	{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars
	QString  strLockRes;

	//getParam (Only incoming & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(1,&strLockRes,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//invoke handler
	m_Handler->Unlock(err,strLockRes);

	//clear outgoing buffer
	rpc.msg_out->ClearData();
	//test if error: then return only error:
	if(!err.IsOK())
	{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}

	//return RETURNED variables to the client: (skip pStatus)
	//start from first RETURNED var:

	//generate response
	rpc.msg_out->GenerateResponse(err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}
}

