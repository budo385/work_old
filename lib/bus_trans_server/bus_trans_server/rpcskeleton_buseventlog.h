#ifndef RPCSKELETON_BUSEVENTLOG_H__
#define RPCSKELETON_BUSEVENTLOG_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_buseventlog.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusEventLog : public RpcSkeleton
{

typedef void (RpcSkeleton_BusEventLog::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusEventLog(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void AddEvent(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteEventByEventID(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteEventByDateTime(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetEventRecordsetByType(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetEventRecordsetByCategory(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetEventRecordsetByDateTime(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusEventLog *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSEVENTLOG_H__
