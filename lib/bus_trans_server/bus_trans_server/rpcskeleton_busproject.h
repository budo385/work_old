#ifndef RPCSKELETON_BUSPROJECT_H__
#define RPCSKELETON_BUSPROJECT_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busproject.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusProject : public RpcSkeleton
{

typedef void (RpcSkeleton_BusProject::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusProject(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadProjectDataSideBar(Status &err,RpcSkeletonMessageHandler &rpc);
	void NewProjectsFromTemplate(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadData(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteData(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusProject *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSPROJECT_H__
