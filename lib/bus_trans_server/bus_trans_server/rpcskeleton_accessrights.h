#ifndef RPCSKELETON_ACCESSRIGHTS_H__
#define RPCSKELETON_ACCESSRIGHTS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_accessrights.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_AccessRights : public RpcSkeleton
{

typedef void (RpcSkeleton_AccessRights::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_AccessRights(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void GetRoleList(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetPersons(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetPersonRoleList(Status &err,RpcSkeletonMessageHandler &rpc);
	void InsertNewRoleToPerson(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteRoleFromPerson(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetRoleListForUsers(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetUsers(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetUserRoleList(Status &err,RpcSkeletonMessageHandler &rpc);
	void InsertNewRoleToUser(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteRoleFromUser(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetUserAccRightsRecordSet(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetRoles(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetBERoles(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetTERoles(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetRoleAccessRightSets(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetAccessRightsSets(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void DeleteAccRSetFromRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void InsertNewAccRSetToRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void InsertNewRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void RenameRole(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetAccessRights(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetAccessRightValue(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetDefaultRoles(Status &err,RpcSkeletonMessageHandler &rpc);
	void ChangeAccessRights(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadGUAR(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteGUAR(Status &err,RpcSkeletonMessageHandler &rpc);
	void PrepareGUAR(Status &err,RpcSkeletonMessageHandler &rpc);
	void TestUAR(Status &err,RpcSkeletonMessageHandler &rpc);
	void UpdateProjectGUAR(Status &err,RpcSkeletonMessageHandler &rpc);
	void UpdateGroupContactsGUAR(Status &err,RpcSkeletonMessageHandler &rpc);
	void UpdateProjectCommEntityGUAR(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_AccessRights *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_ACCESSRIGHTS_H__
