#ifndef RPCSKELETON_CORESERVICES_H__
#define RPCSKELETON_CORESERVICES_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_coreservices.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_CoreServices : public RpcSkeleton
{

typedef void (RpcSkeleton_CoreServices::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_CoreServices(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void ReadLBOList(Status &err,RpcSkeletonMessageHandler &rpc);
	void CheckVersion(Status &err,RpcSkeletonMessageHandler &rpc);
	void UpdateVersion(Status &err,RpcSkeletonMessageHandler &rpc);
	void ReadUserSessionData(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetUserSessions(Status &err,RpcSkeletonMessageHandler &rpc);
	void SavePersonalSettings(Status &err,RpcSkeletonMessageHandler &rpc);
	void SavePersonalSettingsByPersonID(Status &err,RpcSkeletonMessageHandler &rpc);
	void GetPersonalSettingsByPersonID(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_CoreServices *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_CORESERVICES_H__
