#ifndef RPCSKELETON_BUSEMAIL_H__
#define RPCSKELETON_BUSEMAIL_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busemail.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusEmail : public RpcSkeleton
{

typedef void (RpcSkeleton_BusEmail::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusEmail(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Read(Status &err,RpcSkeletonMessageHandler &rpc);
	void Write(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteMultiple(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteEmailLog(Status &err,RpcSkeletonMessageHandler &rpc);
	void List(Status &err,RpcSkeletonMessageHandler &rpc);
	void Delete(Status &err,RpcSkeletonMessageHandler &rpc);
	void UpdateEmailFromClient(Status &err,RpcSkeletonMessageHandler &rpc);
	void SetEmailRead(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteAttachments(Status &err,RpcSkeletonMessageHandler &rpc);
	void WriteEmailAttachmentsFromUserStorage(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusEmail *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSEMAIL_H__
