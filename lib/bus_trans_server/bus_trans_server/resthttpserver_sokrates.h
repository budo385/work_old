#ifndef RESTHTTPSERVER_SOKRATES_H
#define RESTHTTPSERVER_SOKRATES_H

#include "trans/trans/resthttpserver.h"

class RestHTTPServer_Sokrates :public RestHTTPServer
{

public:
	RestHTTPServer_Sokrates(RpcSkeleton *pServerSkeletonSet);

protected:

	bool	AuthenticateUser(Status &err,HTTPContext &ctx,HTTPRequest &request,HTTPResponse &response, int &nClientTimeZoneOffsetMinutes);	
	void	LockThreadActive();
	void	UnLockThreadActive();

	QStringList m_lstAllowedIPAddresses; 

};

#endif // RESTHTTPSERVER_SOKRATES_H
