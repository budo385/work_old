#include "resthttpserver_sokrates.h"
#include <QHostAddress>
#include <QNetworkInterface>
#include <QUrlQuery>

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_server/bus_server/privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 

#define MODULE_CODE_FOR_WEBSERVICE "SC-BE"
#define MLIID_CODE_FOR_WEBSERVICE ""


RestHTTPServer_Sokrates::RestHTTPServer_Sokrates(RpcSkeleton *pServerSkeletonSet)
:RestHTTPServer(pServerSkeletonSet)
{

	QList<QHostAddress> lstAddress=	QNetworkInterface::allAddresses();
	int nSize=lstAddress.size();
	for(int i=0;i<nSize;++i)
	{
		m_lstAllowedIPAddresses.append(lstAddress.at(i).toString());
	}

}

bool RestHTTPServer_Sokrates::AuthenticateUser(Status &err,HTTPContext &ctx,HTTPRequest &request,HTTPResponse &response, int &nClientTimeZoneOffsetMinutes)
{
	err.setError(0);

	//qDebug()<<"-----------------HTTP rest request-----------------------";
	//qDebug()<<request.toString();
	//qDebug()<<"----------------------------------------------------------";

	//B.T. 21.03.2013: allow from local client to reload pages:
	if (request.path().right(6).toLower()=="/login")
	{
		return true;
	}

	//B.T. 13.02.2014: if mwcloud, allow session less:
	if (request.path().contains("/mwcloudservice/"))
	{
		if(!(request.path().contains("/mwcloudservice/createuser") || request.path().contains("/mwcloudservice/changeuserpassword"))) //these two are non session less
			return true;
	}

	
	//test allowed IP's: localhost+tarantula: reload command only possible from local client:
	if (m_lstAllowedIPAddresses.contains(ctx.m_strPeerIP) && request.path().right(QString("server/reloadwebpages").length()).toLower()=="server/reloadwebpages")
	{
		return true;
	}

	QString strCookie=request.value("Cookie");

	//B.T. only in special case: add support to not use cookies:
	if (strCookie.isEmpty())
	{
		QUrlQuery url= QUrlQuery(QUrl(request.path()));
		strCookie="web_session = "+url.queryItemValue("web_session").toLatin1()+";";
	}


	//test session cookie:
	QString strSession;
	if(g_UserSessionManager->AuthenticateUser(strCookie,strSession))
	{
		g_UserSessionManager->ValidateWebUserRequest(err,strSession,ctx,nClientTimeZoneOffsetMinutes);
		if (!err.IsOK())
		{
			SetResponseHeader(request,response,401,"Unauthorized");
			return false;
		}
		return true;
	}

	SetResponseHeader(request,response,401,"Unauthorized");
	return false;

}
void RestHTTPServer_Sokrates::LockThreadActive()
{
	g_UserSessionManager->LockThreadActive();

}
void RestHTTPServer_Sokrates::UnLockThreadActive()
{
	g_UserSessionManager->UnLockThreadActive();
}

