#ifndef XMLRPCHTTPSERVER_H
#define XMLRPCHTTPSERVER_H

#include "bus_trans_server/bus_trans_server/rpchttpserver.h"


/*!
    \class XmlRpcHTPPServer
    \brief XML-RPC server based on RpcHTTPServer
    \ingroup Bus_Trans_Server

	Receives XML-RPC calls dispatches to skeletons, sends back response
	\sa RpcHTTPServer

*/
class XmlRpcHTPPServer : public RpcHTTPServer
{
public:
	XmlRpcHTPPServer(RpcSkeleton *pServerSkeletonSet,UserSessionManager* pSessionManager)
		:RpcHTTPServer(pServerSkeletonSet,pSessionManager,RPC_PROTOCOL_TYPE_XML_RPC){};
};


#endif //XMLRPCHTTPSERVER_H
