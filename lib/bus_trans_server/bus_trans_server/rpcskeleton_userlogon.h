#ifndef RPCSKELETON_USERLOGON_H__
#define RPCSKELETON_USERLOGON_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_userlogon.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_UserLogon : public RpcSkeleton
{

typedef void (RpcSkeleton_UserLogon::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_UserLogon(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Logout(Status &err,RpcSkeletonMessageHandler &rpc);
	void IsAlive(Status &err,RpcSkeletonMessageHandler &rpc);
	void Login(Status &err,RpcSkeletonMessageHandler &rpc);
	void ChangePassword(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_UserLogon *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_USERLOGON_H__
