#ifndef RPCSKELETON_BUSADDRESSSCHEMAS_H__
#define RPCSKELETON_BUSADDRESSSCHEMAS_H__

#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"
#include "bus_interface/bus_interface/interface_busaddressschemas.h"
#include "bus_server/bus_server/businessserviceset.h"

class RpcSkeleton_BusAddressSchemas : public RpcSkeleton
{

typedef void (RpcSkeleton_BusAddressSchemas::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);

public:
	RpcSkeleton_BusAddressSchemas(BusinessServiceSet *pHandler,int RPCType);

	bool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);

private:

	//skeleton function for each method publicly accessible
	void Write(Status &err,RpcSkeletonMessageHandler &rpc);
	void Read(Status &err,RpcSkeletonMessageHandler &rpc);
	void Delete(Status &err,RpcSkeletonMessageHandler &rpc);
	void Lock(Status &err,RpcSkeletonMessageHandler &rpc);
	void Unlock(Status &err,RpcSkeletonMessageHandler &rpc);

	Interface_BusAddressSchemas *m_Handler; //< pointer to your BO
	QHash<QString,PFN> mFunctList;
};

#endif	// RPCSKELETON_BUSADDRESSSCHEMAS_H__
