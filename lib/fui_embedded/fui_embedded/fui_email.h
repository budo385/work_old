#ifndef FUI_EMAIL_H
#define FUI_EMAIL_H

#include "fuibase.h"
#include "ui_fui_email.h"
#include "common/common/dbrecordset.h"

class FUI_Email : public FuiBase
{
	Q_OBJECT
public:
	FUI_Email(QString strFuiName, QWidget *parent = 0);
	~FUI_Email();

	void			SetDefaults(DbRecordSet recContacts, QString strEmail = "", int nProjectID = -1, int nTemplateEmailID=-1, bool bScheduleTask=false,bool bSetAsTemplate=false, int nEmailReplyID=-1,QStringList *lstZippedFiles=NULL, bool bForwardMail=false);
	void			on_selectionChange(int nEntityRecordID);
	void			on_cmdInsert();
	QAction*		SetLeftSoftKeyAction();
	QList<QAction*>	SetRightSoftKeyAction();

private slots:
	void OnContinue();
	void OnAttachment();
	void OnRecepient();
	void OnBody();
	

private:
	void	SetPersonData();
	void	RefreshDisplay();
	void	SaveMail();
	bool	SendMail();
	void	ClearTemplateFields();
	void	SetRecipientString(DbRecordSet &recRecipients, QString &strRecipients);
	void	WriteTo_CE_CONTACT_LINK(DbRecordSet &recRecipients, int nType /*1=From 2=To 3=CC 4=BCC*/);
	void	Data2Emails(DbRecordSet &lstContactEmails,QString strTo,QString strCC,QString strBCC); //from DB to recipient table
	void	Emails2Data(QString &strTo,QString &strCC,QString &strBCC); //from recipient table 2 data for DB write
	void	PrepareMailToSendToContacts(DbRecordSet &recContacts,QString strDefaultEmailForFirstContact);
	void	UpdateLeftButton();


	Ui::FUI_EmailClass ui;
	
	DbRecordSet m_lstDataSimpleFUI;
	DbRecordSet m_lstTaskCache;				//task cachier
	DbRecordSet m_lstContactLink;			//CE_CONTACT_LINK recordset - for writing to DB.
	DbRecordSet m_lstAttachments;
	DbRecordSet	m_recToRecordSet;	//TO recipients recordset.
	DbRecordSet	m_recCCRecordSet;	//CC recipients recordset.
	DbRecordSet	m_recBCCRecordSet;	//BCC recipients recordset.
	DbRecordSet	m_lstCIDInfo;	// stores info on local image path vs. CID name
	QString		m_strPersonContactID;
	int			m_nProjectID;
	bool		m_bSerialEmailMode;
	bool		m_CID_ModifyEmailBodyInProgress;
	QAction		*m_pContinue;
	QAction		*m_pAttachment;
	QAction		*m_pRecepients;
	QAction		*m_pCancel;
	QAction		*m_pBody;



};

#endif // FUI_EMAIL_H
