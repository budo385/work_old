#include "commgridmodel.h"

#include <QColor>
#include <QSize>
#include <QPixmap>
#include <QPainter>
#include <QIcon>
#include <QMimeData>
#include "os_specific/os_specific/mime_types.h"
#include "common/common/entity_id_collection.h"
#include "trans/trans/xmlutil.h"

//#define  NO_DELEGATE

CommGridModel::CommGridModel(bool bSideBarViewMode, QObject *parent)
	: QAbstractTableModel(parent)
{
	m_bSideBarViewMode = bSideBarViewMode;
	m_nRows = 0;
	if (m_bSideBarViewMode)
		m_nColumns = 1;
	else	
		m_nColumns = 3;
}

CommGridModel::~CommGridModel()
{
	m_recEmail		= NULL;
	m_recVoiceCall	= NULL;
	m_recDocument	= NULL;
}
void CommGridModel::Initialize(DbRecordSet *recEmail, DbRecordSet *recVoiceCall, DbRecordSet *recDocument, 
								QHash<int, int> &hshRow2Color,
								QHash<int, QString> &hshRow2Pix1,
								QHash<int, QString> &hshRow2Pix2,
								QHash<int, QString> &hshRow2Pix3,
								QHash<int, QString> &hshRow2Pix4,
								QHash<int, QString> &hshRow2Date1,
								QHash<int, QString> &hshRow2Date2,
								QHash<int, QString> &hshRow2Autor,
								QHash<int, QString> &hshRow2Subject,
								QHash<int, QString> &hshRow2Descr,
								QHash<int, QString> &hshRow2SecondColDisplay,
								QHash<int, QString> &hshRow2Owner,
								QHash<int, QString> &hshRow2Contact,
								QHash<int, QString> &hshRow2Project,
								DbRecordSet &recApplicationIcons, 
								QHash<int, QPixmap> &hshDocumentIcons)
{
	m_recEmail		= recEmail;
	m_recVoiceCall	= recVoiceCall;
	m_recDocument	= recDocument;

	FillIconHashes(recApplicationIcons, hshDocumentIcons);

	//Clear hashes.
	ClearHashes();

	m_nRows = hshRow2Color.count();
	
	//Row color.
	m_hshColorID = hshRow2Color;
	
	int nColor = hshRow2Color.value(0);
	int nColor1 = m_hshColorID.value(0);

	//Fill first column.
	m_hshPix1	= hshRow2Pix1;
	m_hshPix2	= hshRow2Pix2;
	m_hshPix3	= hshRow2Pix3;
	m_hshPix4	= hshRow2Pix4;
	m_hshDate1	= hshRow2Date1;
	m_hshDate2	= hshRow2Date2;
	m_hshAutor	= hshRow2Autor;

	//Fill second column.
	m_hshDisplay	 = hshRow2SecondColDisplay;
	m_hshSubject	 = hshRow2Subject;
	m_hshDescription = hshRow2Descr;

	//Fill third column.
	m_hshOwner	 = hshRow2Owner;
	m_hshContact = hshRow2Contact;
	m_hshProject = hshRow2Project;
}

void CommGridModel::RefreshItems(QList<int> lstRows, QHash<int, QString> &hshRow2Owner, QHash<int, QString> &hshRow2Contact, QHash<int, QString> &hshRow2Project)
{
	//Fill hashes with data.
	int nRows = lstRows.count();
	for (int i = 0; i < nRows; i++)
	{
		int nRowInGrid = lstRows.value(i);
		//Fill third column.
		m_hshOwner.insert(nRowInGrid,   hshRow2Owner.value(nRowInGrid));
		m_hshContact.insert(nRowInGrid, hshRow2Contact.value(nRowInGrid));
		m_hshProject.insert(nRowInGrid, hshRow2Project.value(nRowInGrid));
	}
}

QVariant CommGridModel::data(const QModelIndex &index, int role /*= Qt::DisplayRole*/) const
{
	//If index not valid (root item), go out.
	if (!index.isValid())
		return QVariant();

	int nRow	= index.row();
	int nColumn = index.column();

	//Background color.
/*	if (role == Qt::BackgroundRole)
	{
		int nColorId = m_hshColorID.value(nRow);
		QColor color;

		//Direction.
		if (nColorId == 0)		//In house.
			color = QColor(254,255,237);
		else if (nColorId == 1)	//Outgoing.
			color = QColor(238,255,237);
		else					//Incoming.
			color = QColor(219,255,255);

		return QVariant(color);
	}*/
	//Icon setting.
	if (nColumn == 0 && role == Qt::DecorationRole)
	{
		//Calculate new icon length (16 pixels * number of icons.)
		QSize IconSize;
		if (m_bSideBarViewMode)
		{
			IconSize.setWidth(16*4+12);
			IconSize.setHeight(32);
		}
		else
		{
			IconSize.setWidth(32*4);
			IconSize.setHeight(32);
		}
		//Make pixmap of future icon.
		QPixmap IconPixmap(IconSize);
		//Fill with white (alpha 0-transparent).
		IconPixmap.fill(QColor(255,255,255,0));
		//Make painter.
		QPainter painter(&IconPixmap);
		//Make icons pixmaps.
		QPixmap Icon1(m_hshPix1.value(nRow));
		QPixmap Icon2(m_hshPix2.value(nRow));
		QString strPix3 = m_hshPix3.value(nRow);
		QString strPix4 = m_hshPix4.value(nRow);
		QPixmap Icon3;
		if (strPix3.startsWith(":"))
			Icon3 = QPixmap(strPix3);
		else
		{
			int nIconID = QVariant(strPix3).toInt();
			Icon3 = m_hshDocumentIcons.value(nIconID);
		}
		QPixmap Icon4;
		if (strPix4.startsWith(":"))
			Icon4 = QPixmap(strPix4);
		else
		{
			int nIconID = QVariant(strPix4).toInt();
			if (!m_hshApplicationIcons.contains(nIconID))
				Q_ASSERT(false);

			Icon4 = m_hshApplicationIcons.value(nIconID);
		}

		if (m_bSideBarViewMode)
		{
#ifndef NO_DELEGATE
			//Paint them.
			painter.drawPixmap(0,	 0,Icon1.scaled(QSize(16, 16)));
			painter.drawPixmap(1*16+2, 0,Icon2.scaled(QSize(16, 16)));
			painter.drawPixmap(2*16+4, 0,Icon3.scaled(QSize(16, 16)));
			painter.drawPixmap(3*16+6, 0,Icon4.scaled(QSize(16, 16)));
			QFont font("Arial Narrow", 8);
			font.setStyleStrategy(QFont::PreferAntialias);
			font.setItalic(true);
			painter.setFont(font);
			painter.setPen(QColor(Qt::white));
			painter.drawText(0, 28, m_hshDate1.value(nRow));
#endif
		}
		else
		{
			//Paint them.
			painter.drawPixmap(0,	 0,Icon1);
			painter.drawPixmap(1*32, 0,Icon2);
			painter.drawPixmap(2*32, 0,Icon3);
			painter.drawPixmap(3*32, 0,Icon4);
		}

		QPixmap pix;
		QIcon icon;
		icon.addPixmap(IconPixmap);

		return QVariant(icon);
	}
	if (role == Qt::ToolTipRole)
	{
		if ((nColumn == 0 && m_bSideBarViewMode) || (nColumn == 1 && !m_bSideBarViewMode))
		{
			if (m_hshSubject.value(nRow).startsWith("<html") || m_hshSubject.value(nRow).startsWith("<!DOCTYPE"))
				return QVariant(m_hshSubject.value(nRow));
			else
				return QVariant(m_hshDescription.value(nRow));
		}
	}

	if (role == Qt::DisplayRole)
	{
		if (m_bSideBarViewMode)
		{
			if (nColumn == 0)
			{
				QStringList lstDisplay;
				//lstDisplay << m_hshSubject.value(nRow) << m_hshDate1.value(nRow) << m_hshDate2.value(nRow) << m_hshContact.value(nRow);
				lstDisplay << m_hshSubject.value(nRow) << m_hshContact.value(nRow);
				return QVariant(lstDisplay.join("/|/"));
			}
		}
		else
		{
			if (nColumn == 0)
			{
				QStringList lstDisplay;
				lstDisplay << m_hshDate1.value(nRow) << m_hshDate2.value(nRow) << m_hshAutor.value(nRow);
				return QVariant(lstDisplay.join("/|/"));
			}
			else if (nColumn == 1)
			{
				return QVariant(m_hshDisplay.value(nRow));
			}
			else if (nColumn == 2)
			{
				QStringList lstDisplay;
				lstDisplay << m_hshOwner.value(nRow) << m_hshContact.value(nRow) << m_hshProject.value(nRow);
				return QVariant(lstDisplay.join("/|/"));
			}
		}
	}

	return QVariant();
}

int CommGridModel::rowCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
	return m_nRows;
}

int CommGridModel::columnCount(const QModelIndex &parent /*= QModelIndex()*/) const
{
	return m_nColumns;
}

QVariant CommGridModel::headerData(int section, Qt::Orientation orientation, int role /*= Qt::DisplayRole*/) const
{
	if (role == Qt::DisplayRole)
	{
		if(orientation == Qt::Horizontal)
			return QVariant();
		else
			return section+1;
	}
	
	return QVariant();
}

void CommGridModel::ClearHashes()
{
	m_hshColorID.clear();
	m_hshPix1.clear();
	m_hshPix2.clear();
	m_hshPix3.clear();
	m_hshPix4.clear();
	m_hshDate1.clear();		
	m_hshDate2.clear();		
	m_hshAutor.clear();
	m_hshDisplay.clear();
	m_hshSubject.clear();	
	m_hshDescription.clear();
	m_hshOwner.clear();		
	m_hshContact.clear();	
	m_hshProject.clear();	
}

void CommGridModel::FillIconHashes(DbRecordSet &recApplicationIcons, QHash<int, QPixmap> &hshDocumentIcons)
{
	int nRowCount = recApplicationIcons.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QByteArray bytePicture=recApplicationIcons.getDataRef(i, "BDMA_ICON").toByteArray();
		QPixmap pixMap;
		pixMap.loadFromData(bytePicture);
		m_hshApplicationIcons.insert(recApplicationIcons.getDataRef(i, "BDMA_ID").toInt(), pixMap);
	}

	m_hshDocumentIcons = hshDocumentIcons;
}

QMimeData * CommGridModel::mimeData(const QModelIndexList & indexes ) const
{
	QMimeData *mimeData=QAbstractTableModel::mimeData(indexes);
	if (mimeData)
	{
		DbRecordSet lstData;
		lstData.addColumn(QVariant::Int,"COMM_ENTITY_ID");
		lstData.addColumn(DbRecordSet::GetVariantType(),"DATA");

		DbRecordSet lstSelectedEmails=m_recEmail->getSelectedRecordSet();
		if (lstSelectedEmails.getRowCount()>0)
		{
			lstData.addRow();
			lstData.setData(lstData.getRowCount()-1,0,QVariant(ENTITY_BUS_EMAILS).toInt());
			lstData.setData(lstData.getRowCount()-1,1,lstSelectedEmails);
		}

		DbRecordSet lstSelectedVC=m_recVoiceCall->getSelectedRecordSet();
		if (lstSelectedVC.getRowCount()>0 )
		{
			lstData.addRow();
			lstData.setData(lstData.getRowCount()-1,0,QVariant(ENTITY_BUS_VOICECALLS).toInt());
			lstData.setData(lstData.getRowCount()-1,1,lstSelectedVC);
		}

		DbRecordSet lstSelectedDocs=m_recDocument->getSelectedRecordSet();
		if (lstSelectedDocs.getRowCount()>0)
		{
			lstData.addRow();
			lstData.setData(lstData.getRowCount()-1,0,QVariant(ENTITY_BUS_DM_DOCUMENTS).toInt());
			lstData.setData(lstData.getRowCount()-1,1,lstSelectedDocs);
		}

		QByteArray byteListData=XmlUtil::ConvertRecordSet2ByteArray(lstData);
		mimeData->setData(SOKRATES_MIME_LIST, byteListData);
		mimeData->setData(SOKRATES_MIME_DROP_TYPE, QVariant(ENTITY_COMM_GRID_DATA).toString().toAscii()); //pass unique type
	}

	return mimeData;
}

//add custom mime types: selected list items + drop type
QStringList CommGridModel::mimeTypes() const
{
	QStringList lstDefault=QAbstractTableModel::mimeTypes();
	lstDefault<<SOKRATES_MIME_LIST;
	lstDefault<<SOKRATES_MIME_DROP_TYPE;

	return lstDefault;
}

Qt::ItemFlags CommGridModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags defaultFlags = QAbstractTableModel::flags(index);

	if (index.isValid())
		return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
	else
		return Qt::ItemIsDropEnabled | defaultFlags;
}
/*
Qt::DropActions CommGridModel::supportedDragActions() const
{
	return Qt::CopyAction;
}

Qt::DropActions CommGridModel::supportedDropActions() const
{
	return Qt::CopyAction;
}
*/