#include "contactnameframe.h"
#include "gui_core/gui_core/thememanager.h"

ContactNameFrame::ContactNameFrame(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);
	this->setStyleSheet("QFrame "+ThemeManager::GetMobileFrameNameBkg());
	ui.labelName->setStyleSheet("QLabel {font-weight:900;font-style:normal;font-family:Arial; font-size:12pt; color:black}");
	ui.labelOrg->setStyleSheet("QLabel {font-weight:700;font-style:italic;font-family:Arial; font-size:10pt; color:black}");
}

ContactNameFrame::~ContactNameFrame()
{

}


void ContactNameFrame::RefreshActualName(DbRecordSet rowContact)
{

	if (rowContact.getRowCount()>0)
	{
		QString strName;
		QString strFirstName=rowContact.getDataRef(0,"BCNT_FIRSTNAME").toString();
		QString strLastName=rowContact.getDataRef(0,"BCNT_LASTNAME").toString();
		QString strOrgName=rowContact.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString();
		if (!strLastName.isEmpty())
			strName=strLastName;
		if (!strFirstName.isEmpty())
			strName+=" "+strFirstName.trimmed();
		strName.trimmed();
		ui.labelName->setText(strName);
		ui.labelOrg->setText(strOrgName);
	}
	else
	{
		ui.labelName->setText("");
		ui.labelOrg->setText("");
	}
}