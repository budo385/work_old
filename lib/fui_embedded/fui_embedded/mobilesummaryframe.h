#ifndef MOBILESUMMARYFRAME_H
#define MOBILESUMMARYFRAME_H

#include <QFrame>
#include "ui_mobilesummaryframe.h"

class MobileSummaryFrame : public QFrame
{
	Q_OBJECT

public:
	MobileSummaryFrame(QWidget *parent = 0);
	~MobileSummaryFrame();

private:
	Ui::MobileSummaryFrameClass ui;
};

#endif // MOBILESUMMARYFRAME_H
