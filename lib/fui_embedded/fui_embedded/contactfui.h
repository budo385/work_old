#ifndef CONTACTFUI_H
#define CONTACTFUI_H

#include <QWidget>
#include "ui_contactfui.h"
 
#include "fuibase.h"
#include "common/common/observer_ptrn.h"

class ContactFui : public FuiBase, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	ContactFui(QString strFuiName, QWidget *parent = 0);
	~ContactFui();

	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	virtual QAction* SetLeftSoftKeyAction();
	void OnOfflineModeChanged(bool bOffline=true);

private slots:
	void OnOpen();
	void OnCnxtMenu( const QPoint &);
protected:
	void contextMenuEvent(QContextMenuEvent * event);

private:
	Ui::ContactFuiClass ui;
	QAction *m_pOpen;
};

#endif // CONTACTFUI_H
