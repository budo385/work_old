#include "commgridview.h"
#include <QDateTime>
#include <QHeaderView>
#include <QPainter>
#include <QAbstractItemDelegate>
#include <QTextEdit>
#include <QDragMoveEvent>
#include "os_specific/os_specific/mime_types.h"
#include "common/common/datahelper.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/globalconstants.h"
#include <QHeaderView>
#include <QBoxLayout>
#include <QLabel>
#include <QFont>
#include <QColor>
#include "bus_client/bus_client/documenthelper.h"
//#include "fui_importemail.h"
//#include "fui_importcontact.h"
//#include "fui_voicecallcenter.h"

#include "commworkbenchwidget.h"
#include <QTime>
#include "macros.h"
#include "db_core/db_core/dbsqltableview.h"
#include "os_specific/os_specific/skype/skypeinterface.h"
#include "clientcontactmanager.h"
#include "thememanager.h"

//GLOBALS:
#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "fuimanager.h"
extern FuiManager						g_objFuiManager;			//global FUI manager:

//#define  NO_DELEGATE

void FirstColumnDelegateSideBarView::drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	painter->setBrush(index.data(Qt::BackgroundRole).value<QBrush>());
}

void FirstColumnDelegateSideBarView::drawDecoration ( QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QPixmap &pixmap) const
{
	painter->drawPixmap(rect.topLeft().x() + 1, rect.topLeft().y() + 1, pixmap);
}

void FirstColumnDelegateSideBarView::drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const
{
#ifndef NO_DELEGATE
	QPen pen = painter->pen();
	QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
		? QPalette::Normal : QPalette::Disabled;
	if (option.state & QStyle::State_Selected) {
		painter->fillRect(rect, option.palette.brush(cg, QPalette::Highlight));
		painter->setPen(option.palette.color(cg, QPalette::HighlightedText));
	} else {
		painter->setPen(option.palette.color(cg, QPalette::Text));
	}

	QFont font = option.font;
	font.setFamily("Arial");
	font.setPointSize(8);
	font.setStyleStrategy(QFont::PreferAntialias);
	font.setBold(true);
	font.setItalic(true);
	painter->setFont(font);

	QStringList lstDisplay;
	lstDisplay = text.split("/|/");
	QString strOriginator = lstDisplay.value(1);
	
/////////////
	//Replace newline with space.
	/*	QString strTitle;
	QString strTmp = lstDisplay.value(0);
	QStringList lstTitle;
	lstTitle = strTmp.split("\n");
	foreach(QString strTitlePart, lstTitle)
	{
	if(strTitlePart.startsWith("<html"))
	{
	QTextEdit edit;
	edit.setHtml(strTitlePart);
	if (strTitle.isEmpty())
	strTitle = edit.toPlainText();
	else
	strTitle += " " + edit.toPlainText();
	}
	else
	if (strTitle.isEmpty())
	strTitle = strTitlePart;
	else
	strTitle += " " + strTitlePart;
	}
	*/
	//Make rectangle 4px smaller than the item original (make a little horizontal spacing in cell).
///////////
	
	QString strTitle = lstDisplay.value(0);
	if(strTitle.startsWith("<html") || strTitle.startsWith("<!DOCTYPE"))
	{
		QTextEdit edit;
		edit.setHtml(strTitle);
		strTitle = edit.toPlainText();
	}


	//	strTitle.replace("\n", " ");

	QStringList lstSplitedTitle = strTitle.split("\n");
	QStringList lstTmp;
	foreach(QString strTmp, lstSplitedTitle)
	{
		if (!strTmp.isEmpty())
			lstTmp << strTmp;
	}
	strTitle = lstTmp.join(" ");

	//QPoint topLeftPoint(rect.topLeft().x() + 2, rect.topLeft().y());
	//QRect newRect(topLeftPoint, rect.bottomRight());
	//QRect newRect(topLeftPoint, QPoint(rect.topRight().x(), rect.topLeft().y() + 8));
	//painter->drawText(newRect, Qt::TextWrapAnywhere, strTitle);

	painter->drawText(rect.topLeft().x() + 2, rect.topLeft().y() + 14, strTitle);
	font.setBold(false);
	font.setItalic(false);
	painter->setFont(font);
	painter->drawText(rect.topLeft().x() + 2, rect.topLeft().y() + 32, strOriginator);
#endif
}

CommGridView::CommGridView(QWidget *parent)
: QTableView(parent)
{	
	_START_TIMER(document_view_constructor);
	setAutoFillBackground(true);
	//Set style sheet.
	QString str(ThemeManager::GetTableViewBkg());
	setStyleSheet(str);

	setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	m_nRowCount = 0;
	m_recSortRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_COMM_GRID_SORT_RECORDSET));
	m_bFirstTimeIn = true;

	setDragEnabled(false);
	setAcceptDrops(false);

	//Set side bar mode.
	m_bSideBarMode = true;

	//Make model.
	m_pGridModel = new CommGridModel(m_bSideBarMode);

	setIconSize(QSize(4*16+12, 32));
	//Delegates.
#ifndef NO_DELEGATE
	//QAbstractItemDelegate *delegate0 = new FirstColumnDelegateSideBarView();
	//setItemDelegateForColumn(0, delegate0);
#endif
	//Set row height.
	verticalHeader()->setDefaultSectionSize(40); 

	//Some layout.
	verticalHeader()->setResizeMode(QHeaderView::Fixed);
	verticalHeader()->setSelectionBehavior(QAbstractItemView::SelectRows);
	verticalHeader()->setSelectionMode(QAbstractItemView::ContiguousSelection);
	setShowGrid(true);

	//Hide header for sidebar mode.
	verticalHeader()->hide();
	horizontalHeader()->hide();

	//Connect signals.
	connect(this, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(on_CellDoubleClicked(const QModelIndex&)));

	_STOP_TIMER(document_view_constructor);
}

CommGridView::~CommGridView()
{
	m_recEmail		= NULL;
	m_recVoiceCall	= NULL;
	m_recDocument	= NULL;

	delete(m_pGridModel);
	m_pGridModel = NULL;
}

void CommGridView::Initialize(DbRecordSet *recEmails, DbRecordSet *recDocuments, DbRecordSet *recVoiceCalls, int nGridType, bool bClearSelection /*= true*/)
{
	_START_TIMER(document_view_initialize);
	//Are we in contact fui.
	m_nGridType=nGridType;

	setDragEnabled(true);
	setAcceptDrops(true);

	//Clear hashes.
	ClearHashes();

	//Locals;
	m_recEmail		= recEmails;
	m_recVoiceCall	= recVoiceCalls;
	m_recDocument	= recDocuments;

	//Get row counts.
	int nRowCountEmails = recEmails->getRowCount();
	int nRowCountVoiceCalls = recVoiceCalls->getRowCount();
	int nRowCountDocuments = recDocuments->getRowCount();
	m_nRowCount = nRowCountEmails + nRowCountVoiceCalls + nRowCountDocuments;

	int nRowsInGrid = 0;
	for (int i = 0; i < nRowCountEmails; ++i)
	{
		//Fill model recordset.
		FirstColumnData(EMAIL, nRowsInGrid, *m_recEmail,i);
		SecondColumnData(EMAIL, *m_recEmail,i, nRowsInGrid);
		ThirdColumnData(EMAIL, nRowsInGrid, *m_recEmail,i);

		//Fill data and sorting hashes.
		FillRowHashes(nRowsInGrid, EMAIL, *m_recEmail,i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountVoiceCalls; ++i)
	{
		//Fill model recordset.
		FirstColumnData(VOICECALL, nRowsInGrid, *m_recVoiceCall,i);
		SecondColumnData(VOICECALL, *m_recVoiceCall,i, nRowsInGrid);
		ThirdColumnData(VOICECALL, nRowsInGrid,*m_recVoiceCall,i);

		//Fill data and sorting hashes.
		FillRowHashes(nRowsInGrid, VOICECALL, *m_recVoiceCall,i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountDocuments; ++i)
	{
		FirstColumnData(DOCUMENT, nRowsInGrid, *m_recDocument,i);
		SecondColumnData(DOCUMENT, *m_recDocument,i, nRowsInGrid);
		ThirdColumnData(DOCUMENT, nRowsInGrid, *m_recDocument,i);

		//Fill data and sorting hashes.
		FillRowHashes(nRowsInGrid, DOCUMENT, *m_recDocument,i);


		//m_recDocument->Dump();

		//Increase row number.
		nRowsInGrid++;
	}

	//Sort table, initialize and set model.
	SortTable(NULL, bClearSelection);
	_STOP_TIMER(document_view_initialize);
}


void CommGridView::RefreshContactItem(int nCEType, DbRecordSet &recEntityRecord)
{
	//Get selected row.
	DbRecordSet tmp = recEntityRecord.getSelectedRecordSet();
	QList<int> lstSelectedRows;

	int nRowCount = tmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		//Fill hashes with new values.
		int nCENT_ID = tmp.getDataRef(i, "CENT_ID").toInt();
		int nBPER_ID = tmp.getDataRef(i, "A.BPER_ID").toInt();
		int nBCNT_ID = tmp.getDataRef(i, "BCNT_ID").toInt();
		int nBUSP_ID = tmp.getDataRef(i, "BUSP_ID").toInt();

		//Find corresponding row and update contact.
		int nRowInGrid = m_hshRow2CENT_ID.key(nCENT_ID);
		m_hshRow2BPER_ID.insert(nRowInGrid, nBPER_ID);
		m_hshRow2BCNT_ID.insert(nRowInGrid, nBCNT_ID);
		m_hshRow2BUSP_ID.insert(nRowInGrid, nBUSP_ID);

		//Refresh item display.
		ThirdColumnData(nCEType, nRowInGrid, tmp, i);

		//Fill changed rows list.
		lstSelectedRows << nRowInGrid;
	}

	//If in side bar mode there is no third column so don't refresh.
	if (m_bSideBarMode)
		return;

	m_pGridModel->RefreshItems(lstSelectedRows, m_hshRow2Owner, m_hshRow2Contact, m_hshRow2Project);
}

//Sort table - need to reorder the hashes.
void CommGridView::SortTable(DbRecordSet *recSortRecordSet /*= NULL*/, bool bClearSelection /*= false*/)
{
	//Clear sort recordset.
	m_recSortRecordSet.clear();
	m_recSortRecordSet.addRow(m_nRowCount);
	for(int i = 0; i < m_nRowCount; i++)
	{
		m_recSortRecordSet.setData(i, "ROW_IN_TABLE", i);
		m_recSortRecordSet.setData(i, "CONTACT", m_hshRow2CONTACT.value(i));
		m_recSortRecordSet.setData(i, "PROJECT", m_hshRow2PROJECT.value(i));
		m_recSortRecordSet.setData(i, "DATE_1", m_hshRow2DATE1.value(i));
		m_recSortRecordSet.setData(i, "DATE_2", m_hshRow2DATE2.value(i));
		m_recSortRecordSet.setData(i, "STATUS", m_hshRow2STATUS.value(i));
		m_recSortRecordSet.setData(i, "APP_TYPE", m_hshRow2APPTYPE.value(i));
		m_recSortRecordSet.setData(i, "SUBTYPE", m_hshRow2SUBTYPE.value(i));
		m_recSortRecordSet.setData(i, "MEDIA", m_hshRow2MEDIA.value(i));
		m_recSortRecordSet.setData(i, "ORIGINATOR", m_hshRow2ORIGINATOR.value(i));
		m_recSortRecordSet.setData(i, "OWNER", m_hshRow2OWNER.value(i));
		m_recSortRecordSet.setData(i, "TIME_1", m_hshRow2TIME1.value(i));
		m_recSortRecordSet.setData(i, "TIME_2", m_hshRow2TIME2.value(i));
	}

	//Create default sort.
	SortDataList defaultSortList;
	SortData sortDate1(3 /*DATE_1*/,  1 /* = DESCENDING*/);
	SortData sortTime1(11 /*TIME_1*/, 1 /* = DESCENDING*/);
	SortData sortTime2(12 /*TIME_2*/, 1 /* = DESCENDING*/);
	defaultSortList << sortDate1 << sortTime1;

	DbRecordSet recTmp;
	if (recSortRecordSet != NULL)
		recTmp = *recSortRecordSet;

	//Check is there something in sort recordset.
	if ( (!recTmp.getRowCount()) && (!m_recSortByRecordset.getRowCount()) )
		//Default sort - by date 1.
		m_recSortRecordSet.sortMulti(defaultSortList);
	//Sort by 
	else
	{
		SortDataList sortList;

		//If outside sort recordset is not null then sort on base of it.
		if (recTmp.getRowCount())
			m_recSortByRecordset = recTmp;

		int nSortRowCount = m_recSortByRecordset.getRowCount();
		for (int i = 0; i < nSortRowCount; i++)
		{
			//Get sort column name and order from recordset that came from outside.
			QString strSortColumn = m_recSortByRecordset.getDataRef(i, "BOGW_COLUMN_NAME").toString();
			int nSortOrder		  = m_recSortByRecordset.getDataRef(i, "BOGW_SORT_ORDER").toInt(); //0 - ASC, 1 - DESC;

			//Get sort column idx from sort recordset.
			int nColumnIDx		  = m_recSortRecordSet.getColumnIdx(strSortColumn);
			//Create sort data structure and place it in sort list.
			SortData sortData(nColumnIDx, nSortOrder);
			sortList << sortData;
		}

		//Check are dates used for sorting (if not add date1 for second sort).
		int nDate1Row = m_recSortByRecordset.find("BOGW_COLUMN_NAME", QString("DATE_1"), true);
		int nDate2Row = m_recSortByRecordset.find("BOGW_COLUMN_NAME", QString("DATE_2"), true);

		//If not used neither of dates to sort use date1 (and time1) to sort.
		if((nDate1Row<0 && nDate2Row<0))
		{
			sortList << sortDate1 <<  sortTime1;
		}
		//If date1 used and date2 not add time1
		else if (nDate1Row>=0 && nDate2Row<0)
			sortList << sortTime1;
		//If date2 used and date1 not add time2
		else if (nDate1Row<0 && nDate2Row>=0)
			sortList << sortTime2;
		//If both dates used check the order and add both times.
		else
		{
			if (nDate1Row > nDate2Row)
				sortList << sortTime2 << sortTime1;
			else
				sortList << sortTime1 << sortTime2;
		}

		//Sort recordset.
		m_recSortRecordSet.sortMulti(sortList);
	}

	//Create tmp hashes and reorder old ones.
	QHash<int, int> hshRow2CENT_ID,
		hshRow2BTKS_ID,
		hshRow2BTKS_SYSTEM_STATUS, 
		hshRow2BPER_ID,
		hshRow2BCNT_ID,
		hshRow2CENT_SYS,
		hshRow2BUSP_ID,
		hshRow2ENTITY_ID,
		hshRow2BDMD_IS_CHECK_OUT,
		hshRow2BEM_UNREAD_FLAG,
		hshRow2BDMD_CHECK_OUT_USER_ID,
		hshRow2STATUS,
		hshRow2APPTYPE,
		hshRow2SUBTYPE,
		hshRow2MEDIA;
	QHash<int, QDate> hshRow2DATE1,
		hshRow2DATE2;
	QHash<int, QTime> hshRow2TIME1,
		hshRow2TIME2;
	QHash<int, QString> hshRow2ORIGINATOR,
		hshRow2CONTACT,
		hshRow2PROJECT,
		hshRow2OWNER;

	//Model hashes.
	QHash<int, int> hshRow2Color;
	QHash<int, QString> hshRow2Pix1,
		hshRow2Pix2,
		hshRow2Pix3,
		hshRow2Pix4,
		hshRow2Date1,
		hshRow2Date2,
		hshRow2Autor,
		hshRow2Subject,
		hshRow2Descr,
		hshRow2SecondColDisplay,
		hshRow2Owner,
		hshRow2Contact,
		hshRow2Project;

	hshRow2CENT_ID	 = m_hshRow2CENT_ID;
	hshRow2BTKS_ID	 = m_hshRow2BTKS_ID;
	hshRow2BTKS_SYSTEM_STATUS = m_hshRow2BTKS_SYSTEM_STATUS;
	hshRow2BPER_ID	 = m_hshRow2BPER_ID;
	hshRow2BCNT_ID	 = m_hshRow2BCNT_ID;
	hshRow2CENT_SYS	 = m_hshRow2CENT_SYS;
	hshRow2BUSP_ID	 = m_hshRow2BUSP_ID;
	hshRow2ENTITY_ID = m_hshRow2ENTITY_ID;
	hshRow2BDMD_IS_CHECK_OUT = m_hshRow2BDMD_IS_CHECK_OUT;
	hshRow2BEM_UNREAD_FLAG = m_hshRow2BEM_UNREAD_FLAG;
	hshRow2BDMD_CHECK_OUT_USER_ID = m_hshRow2BDMD_CHECK_OUT_USER_ID;
	hshRow2STATUS	 = m_hshRow2STATUS;
	hshRow2APPTYPE	 = m_hshRow2APPTYPE;
	hshRow2CONTACT	 = m_hshRow2CONTACT;
	hshRow2PROJECT	 = m_hshRow2PROJECT;
	hshRow2DATE1	 = m_hshRow2DATE1;
	hshRow2DATE2	 = m_hshRow2DATE2;
	hshRow2SUBTYPE	 = m_hshRow2SUBTYPE;
	hshRow2MEDIA	 = m_hshRow2MEDIA;
	hshRow2ORIGINATOR= m_hshRow2ORIGINATOR;
	hshRow2OWNER	 = m_hshRow2OWNER;
	hshRow2TIME1	 = m_hshRow2TIME1;
	hshRow2TIME2	 = m_hshRow2TIME2;

	//Model hashes.
	hshRow2Color	= m_hshRow2Color;
	hshRow2Pix1		= m_hshRow2Pix1;
	hshRow2Pix2		= m_hshRow2Pix2;
	hshRow2Pix3		= m_hshRow2Pix3;
	hshRow2Pix4		= m_hshRow2Pix4;
	hshRow2Date1	= m_hshRow2Date1;
	hshRow2Date2	= m_hshRow2Date2;
	hshRow2Autor	= m_hshRow2Autor;
	hshRow2Subject	= m_hshRow2Subject;
	hshRow2Descr	= m_hshRow2Descr;
	hshRow2SecondColDisplay = m_hshRow2SecondColDisplay;
	hshRow2Owner	= m_hshRow2Owner;
	hshRow2Contact	= m_hshRow2Contact;
	hshRow2Project	= m_hshRow2Project;


	//Clear old hashes.
	m_hshRow2CENT_ID.clear();
	m_hshRow2BTKS_ID.clear();
	m_hshRow2BTKS_SYSTEM_STATUS.clear();
	m_hshRow2BPER_ID.clear();
	m_hshRow2BCNT_ID.clear();
	m_hshRow2CENT_SYS.clear();
	m_hshRow2BUSP_ID.clear();
	m_hshRow2ENTITY_ID.clear();
	m_hshRow2BDMD_IS_CHECK_OUT.clear();
	m_hshRow2BEM_UNREAD_FLAG.clear();
	m_hshRow2BDMD_CHECK_OUT_USER_ID.clear();
	m_hshRow2DATE1.clear();
	m_hshRow2DATE2.clear();
	m_hshRow2STATUS.clear();
	m_hshRow2APPTYPE.clear();
	m_hshRow2ORIGINATOR.clear();
	m_hshRow2CONTACT.clear();
	m_hshRow2PROJECT.clear();
	m_hshRow2SUBTYPE.clear();
	m_hshRow2MEDIA.clear();
	m_hshRow2OWNER.clear();
	m_hshRow2TIME1.clear();
	m_hshRow2TIME2.clear();

	//Model hashes.
	m_hshRow2Color.clear();
	m_hshRow2Pix1.clear();
	m_hshRow2Pix2.clear();
	m_hshRow2Pix3.clear();
	m_hshRow2Pix4.clear();
	m_hshRow2Date1.clear();
	m_hshRow2Date2.clear();
	m_hshRow2Autor.clear();
	m_hshRow2Subject.clear();
	m_hshRow2Descr.clear();
	m_hshRow2SecondColDisplay.clear();
	m_hshRow2Owner.clear();
	m_hshRow2Contact.clear();
	m_hshRow2Project.clear();

	//Start reordering.
	for (int i = 0; i < m_nRowCount; ++i)
	{
		//Get old row number.
		int nOldRow = m_recSortRecordSet.getDataRef(i, "ROW_IN_TABLE").toInt();

		//Set values in hashes.
		m_hshRow2CENT_ID.insert(i, hshRow2CENT_ID.value(nOldRow));
		m_hshRow2BTKS_ID.insert(i, hshRow2BTKS_ID.value(nOldRow));
		m_hshRow2BTKS_SYSTEM_STATUS.insert(i, hshRow2BTKS_SYSTEM_STATUS.value(nOldRow));
		m_hshRow2BPER_ID.insert(i, hshRow2BPER_ID.value(nOldRow));
		m_hshRow2BCNT_ID.insert(i, hshRow2BCNT_ID.value(nOldRow));
		m_hshRow2CENT_SYS.insert(i, hshRow2CENT_SYS.value(nOldRow));
		m_hshRow2BUSP_ID.insert(i, hshRow2BUSP_ID.value(nOldRow));
		m_hshRow2ENTITY_ID.insert(i, hshRow2ENTITY_ID.value(nOldRow));
		m_hshRow2BDMD_IS_CHECK_OUT.insert(i, hshRow2BDMD_IS_CHECK_OUT.value(nOldRow));
		m_hshRow2BEM_UNREAD_FLAG.insert(i, hshRow2BEM_UNREAD_FLAG.value(nOldRow));
		m_hshRow2BDMD_CHECK_OUT_USER_ID.insert(i, hshRow2BDMD_CHECK_OUT_USER_ID.value(nOldRow));
		m_hshRow2DATE1.insert(i, hshRow2DATE1.value(nOldRow));
		m_hshRow2DATE2.insert(i, hshRow2DATE2.value(nOldRow));
		m_hshRow2STATUS.insert(i, hshRow2STATUS.value(nOldRow));
		m_hshRow2APPTYPE.insert(i, hshRow2APPTYPE.value(nOldRow));
		m_hshRow2ORIGINATOR.insert(i, hshRow2ORIGINATOR.value(nOldRow));
		m_hshRow2CONTACT.insert(i, hshRow2CONTACT.value(nOldRow));
		m_hshRow2PROJECT.insert(i, hshRow2PROJECT.value(nOldRow));
		m_hshRow2SUBTYPE.insert(i, hshRow2SUBTYPE.value(nOldRow));
		m_hshRow2MEDIA.insert(i, hshRow2MEDIA.value(nOldRow));
		m_hshRow2OWNER.insert(i, hshRow2OWNER.value(nOldRow));
		m_hshRow2TIME1.insert(i, hshRow2TIME1.value(nOldRow));
		m_hshRow2TIME2.insert(i, hshRow2TIME2.value(nOldRow));

		//Set values in model hashes.
		m_hshRow2Color.insert(i, hshRow2Color.value(nOldRow));
		m_hshRow2Pix1.insert(i, hshRow2Pix1.value(nOldRow));
		m_hshRow2Pix2.insert(i, hshRow2Pix2.value(nOldRow));
		m_hshRow2Pix3.insert(i, hshRow2Pix3.value(nOldRow));
		m_hshRow2Pix4.insert(i, hshRow2Pix4.value(nOldRow));
		m_hshRow2Date1.insert(i, hshRow2Date1.value(nOldRow));
		m_hshRow2Date2.insert(i, hshRow2Date2.value(nOldRow));
		m_hshRow2Autor.insert(i, hshRow2Autor.value(nOldRow));
		m_hshRow2Subject.insert(i, hshRow2Subject.value(nOldRow));
		m_hshRow2Descr.insert(i, hshRow2Descr.value(nOldRow));
		m_hshRow2SecondColDisplay.insert(i, hshRow2SecondColDisplay.value(nOldRow));
		m_hshRow2Owner.insert(i, hshRow2Owner.value(nOldRow));
		m_hshRow2Contact.insert(i, hshRow2Contact.value(nOldRow));
		m_hshRow2Project.insert(i, hshRow2Project.value(nOldRow));

		//Set new row in sort recordset.
		m_recSortRecordSet.setData(i, "ROW_IN_TABLE", i);
	}

	//Create model.
	DbRecordSet recApplications;
	QHash<int, QPixmap> hshDocumentIcons;
	DocumentHelper::GetDocApplications(recApplications);
	hshDocumentIcons = DocumentHelper::GetDocumentTypeIconHash();

	int nn = hshDocumentIcons.size();

	m_pGridModel->Initialize(m_recEmail, m_recVoiceCall, m_recDocument, 
		m_hshRow2Color,
		m_hshRow2Pix1,
		m_hshRow2Pix2,
		m_hshRow2Pix3,
		m_hshRow2Pix4,
		m_hshRow2Date1,
		m_hshRow2Date2,
		m_hshRow2Autor,
		m_hshRow2Subject,
		m_hshRow2Descr,
		m_hshRow2SecondColDisplay,
		m_hshRow2Owner,
		m_hshRow2Contact,
		m_hshRow2Project,
		recApplications, hshDocumentIcons);

	if (bClearSelection)
		clearSelection();

	setModel(0);
	setModel(m_pGridModel);

	SetLayout();
}

void CommGridView::dragMoveEvent ( QDragMoveEvent * event )
{
	event->accept();
}

void CommGridView::dragEnterEvent ( QDragEnterEvent * event )
{
	event->accept();
	//startDrag()
}

//if TAB= template then template, else doc, if not exe...brb....
void CommGridView::dropEvent(QDropEvent *event)
{
	/*
	const QMimeData *mime = event->mimeData();

	if (mime->hasUrls())
	{

		QList<QUrl> lst =mime->urls();
		CommWorkBenchWidget* pParent=dynamic_cast<CommWorkBenchWidget*>(this->parent());
		Q_ASSERT(pParent);
		pParent->SubMenuRequestedNewData();
		this->activateWindow(); //bring on top_:
		g_CommManager.ProcessDocumentDrop(lst,false,false);
		event->accept();
		return;
	}


	if (mime->hasFormat(OUTLOOK_MIME_MAIL_ID))
	{
		//import emails:
		QByteArray arData = mime->data(OUTLOOK_MIME_MAIL_ID);
		if(arData.size() > 0)
		{
			//get current project id if project fui:
			CommWorkBenchWidget* pParent=dynamic_cast<CommWorkBenchWidget*>(this->parent());
			Q_ASSERT(pParent);
			pParent->SubMenuRequestedNewData();
			int nGridType,nRecID,nProjectID=-1;
			pParent->GetGridData(nGridType,nRecID);
			if (nGridType==CommWorkBenchWidget::PROJECT_GRID && nRecID>0)
				nProjectID=nRecID;


			if(FUI_ImportEmail::IsDropEmail(arData))
			{
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_EMAIL, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportEmail* pFuiDM=dynamic_cast<FUI_ImportEmail*>(pFUI);
				if (pFuiDM)
				{
					pFuiDM->BuildImportListFromDrop(arData,false,nProjectID);
				}
				if(pFUI)pFUI->show();  //show FUI
			}
			else if(FUI_ImportEmail::IsDropContact(arData))
			{
				//open FUI
				int nNewFUI=g_objFuiManager.OpenFUI(MENU_IMPORT_CONTACT, true, false,FuiBase::MODE_EMPTY, -1,true);
				QWidget* pFUI=g_objFuiManager.GetFUIWidget(nNewFUI);
				FUI_ImportContact* pFuiW=dynamic_cast<FUI_ImportContact*>(pFUI);
				if (pFuiW)
				{
					pFuiW->BuildImportListFromDrop(arData);
				}
				if(pFUI)pFUI->show();  //show FUI
			}
		}

		event->accept();
		return;
	}



	//------------------------------------------------
	// LIST of record->emit special signal
	//------------------------------------------------

	//internal/external
	if (mime->hasFormat(SOKRATES_MIME_LIST))
	{
		int nEntityID;
		DbRecordSet lstDropped;

		//import emails:
		QByteArray arData = mime->data(SOKRATES_MIME_LIST);
		if(arData.size() > 0)
		{
			lstDropped=XmlUtil::ConvertByteArray2RecordSet(arData);
		}
		QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
		if(arDataType.size() > 0)
		{
			bool bOK;
			nEntityID=arDataType.toInt(&bOK);
			if (!bOK) nEntityID=-1;
		}

		if (nEntityID==ENTITY_COMM_GRID_DATA && lstDropped.getRowCount()>0)
		{
			QWidget *widget_parent=parentWidget();
			CommWorkBenchWidget *bench=dynamic_cast<CommWorkBenchWidget*>(widget_parent);
			int nGridType,nRecID;
			bench->GetGridData(nGridType,nRecID);
			if (nGridType==CommWorkBenchWidget::PROJECT_GRID && nRecID>0)
			{
				if(g_CommManager.DropCommDataToProjct(lstDropped,nRecID))
					bench->ReloadData();
			}
		}
		event->accept();
		return;
	}
	*/
}

void CommGridView::SelectGridFromSourceRecordSet()
{
	blockSignals(true);
	//Locals.
	QList<int> lstSelectedCENT_IDs;

	//Voice calls.
	int nRowCount = m_recVoiceCall->getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		if (m_recVoiceCall->isRowSelected(i))
			lstSelectedCENT_IDs << m_recVoiceCall->getDataRef(i, "CENT_ID").toInt();

	//Emails.
	nRowCount = m_recEmail->getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		if (m_recEmail->isRowSelected(i))
			lstSelectedCENT_IDs << m_recEmail->getDataRef(i, "CENT_ID").toInt();

	//Documents.
	nRowCount = m_recDocument->getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		if (m_recDocument->isRowSelected(i))
			lstSelectedCENT_IDs << m_recDocument->getDataRef(i, "CENT_ID").toInt();

	blockSignals(false);

	SelectBySourceRecordSetRows(lstSelectedCENT_IDs);

	//B.T. select first item in list, this do not work!
	/*
	if (lstSelectedCENT_IDs.size()>0)
	{
	int nRowFirst=lstSelectedCENT_IDs.at(0);
	this->setCurrentCell(nRowFirst,0);
	on_CellClicked(nRowFirst,0);
	}
	*/
	//on_itemSelectionChanged(); B.T. ???
}

//B.T. improved: from row grid, select/deselect inside datasource
void CommGridView::SelectSourceRecordSetFromGrid()
{
	m_recEmail->clearSelection();
	m_recVoiceCall->clearSelection();
	m_recDocument->clearSelection();

	QList<int> lstSelectedRows;
	QModelIndexList lstSelectedIndexes = selectedIndexes();
	int nSelRowCount = lstSelectedIndexes.count();
	for (int i = 0; i < nSelRowCount; ++i)
	{
		QModelIndex index = lstSelectedIndexes.value(i);
		int nRow = index.row();
		if (lstSelectedRows.contains(nRow))
			continue;
		lstSelectedRows << nRow;
		int nEntitySysType	= m_hshRow2CENT_SYS.value(nRow);
		int nEntityID		= m_hshRow2CENT_ID.value(nRow);
		SelectDataInSourceRecordSet(nEntitySysType, nEntityID,true);
	}
}

void CommGridView::SetLayout()
{
	//if (!m_bFirstTimeIn)
	//	return;

	if (m_bSideBarMode)
	{
		//Layout only once when model is set.
		//		horizontalHeader()->resizeSection(0,72);
		//		horizontalHeader()->setResizeMode(0, QHeaderView::Fixed);
		horizontalHeader()->setResizeMode(0, QHeaderView::ResizeToContents);
		//		horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);
		horizontalHeader()->setClickable(false);
		setSelectionBehavior(QAbstractItemView::SelectRows);
	}
	else
	{
		//Layout only once when model is set.
		horizontalHeader()->resizeSection(0,230);
		horizontalHeader()->resizeSection(2,360);
		horizontalHeader()->setResizeMode(0, QHeaderView::Interactive);
		horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);
		horizontalHeader()->setResizeMode(2, QHeaderView::Fixed);
		//horizontalHeader()->hide();

		//For first time entering contact adjust grid geometry.
		//if (m_bInContact && m_bFirstTimeIn)
		//{
		//	m_bFirstTimeIn = false;
		//	horizontalHeader()->resizeSection(1,300);
		//}

		if(m_nGridType==1 /*CommWorkBenchWidget::CONTACT_GRID*/ || m_nGridType==2/*CommWorkBenchWidget::PROJECT_GRID*/)
		{
			horizontalHeader()->setResizeMode(1, QHeaderView::Interactive);
			horizontalHeader()->resizeSection(1,300);
		}

		horizontalHeader()->setClickable(false);
		setSelectionBehavior(QAbstractItemView::SelectRows);
	}

	m_bFirstTimeIn=false;
}

int CommGridView::CheckTask(DbRecordSet &recEntityRecord,int nRow/*=0*/)
{
	if (recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull())
		return NOT_SCHEDULED;

	bool bTaskActive = recEntityRecord.getDataRef(nRow, "BTKS_IS_TASK_ACTIVE").toBool();
	if (!bTaskActive)
		return NOT_SCHEDULED;

	int nTaskStatus = recEntityRecord.getDataRef(nRow, "BTKS_SYSTEM_STATUS").toInt();
	if (nTaskStatus == GlobalConstants::TASK_STATUS_COMPLETED || nTaskStatus == GlobalConstants::TASK_STATUS_CANCELLED)
		return NOT_SCHEDULED;

	int nStatus;
	bool bStartUnvalid			= recEntityRecord.getDataRef(nRow, "BTKS_START").isNull();
	bool bDueUnvalid			= recEntityRecord.getDataRef(nRow, "BTKS_DUE").isNull();
	//Check is start date reached.
	bool bStartPassed;
	if(!bStartUnvalid)
	{
		if(recEntityRecord.getDataRef(nRow, "BTKS_START").toDateTime() > QDateTime::currentDateTime())
			bStartPassed = false;
		else
			bStartPassed = true;
	}
	//Check is due date reached.
	bool bDuePassed;
	if (!bDueUnvalid)
	{
		if (recEntityRecord.getDataRef(nRow, "BTKS_DUE").toDateTime() > QDateTime::currentDateTime())
			bDuePassed = false;
		else
			bDuePassed = true;
	}

	//If start date invalid then it must at least be due now
	if	(bStartUnvalid)
	{
		if(bDueUnvalid)
			nStatus = SCHEDULED_DUENOW;
		else
		{
			if (bDuePassed)
				nStatus = SCHEDULED_OVERDUE;
			else
				nStatus = SCHEDULED_DUENOW;
		}

	}
	//If start valid - then it needs to be checked.
	else
	{
		//If start passed - due.
		if (bStartPassed)
		{
			if(bDueUnvalid)
				nStatus = SCHEDULED_DUENOW;
			else
			{
				if (bDuePassed)
					nStatus = SCHEDULED_OVERDUE;
				else
					nStatus = SCHEDULED_DUENOW;
			}
		}
		//If not yet passed then it can be only scheduled.
		else
			nStatus = SCHEDULED;
	}

	return nStatus;
}

int	CommGridView::CheckItemDirection(int nCEType, DbRecordSet &recEntityRecord,int nRow)
{
	int nInOutDocument;
	//Check is it outgoing, incoming or document.
	if (nCEType == VOICECALL)
	{
		int nDirection = recEntityRecord.getDataRef(nRow, "BVC_DIRECTION").toInt();
		if (nDirection == 0 || nDirection == 2)
			nInOutDocument = OUTGOING;
		else
			nInOutDocument = INCOMING;
	}
	else if (nCEType == EMAIL)
	{
		if (recEntityRecord.getDataRef(nRow, "BEM_OUTGOING").toBool())
			nInOutDocument = OUTGOING;
		else
			nInOutDocument = INCOMING;
	}
	else
		nInOutDocument = INHOUSE;

	return nInOutDocument;
}

QString	CommGridView::GetTaskPixmap(int nTask, int nRowsInGrid)
{
	//Set task icon.
	QString strTaskIcon;
	if		(nTask == SCHEDULED)
	{
		strTaskIcon = ":OnHold32.png";
		m_hshRow2STATUS.insert(nRowsInGrid, SCHEDULED_SORT);
	}
	else if (nTask == SCHEDULED_DUENOW)
	{
		strTaskIcon = ":ToDo32.png";
		m_hshRow2STATUS.insert(nRowsInGrid, SCHEDULED_DUENOW_SORT);
	}
	else if (nTask == SCHEDULED_OVERDUE)
	{
		strTaskIcon = ":Overdue32.png";
		m_hshRow2STATUS.insert(nRowsInGrid, SCHEDULED_OVERDUE_SORT);
	}
	else
	{
		strTaskIcon = ":Empty_icon.png";
		m_hshRow2STATUS.insert(nRowsInGrid, EMPTY_SORT);
	}

	return strTaskIcon;
}

QString	CommGridView::GetCETypePixmap(int nCEType, int nRowsInGrid)
{
	//Email, document or voice call icon.
	QString strCETypeIcon;
	if		(nCEType == EMAIL)
	{
		strCETypeIcon = ":Email32.png";
		m_hshRow2SUBTYPE.insert(nRowsInGrid, SECOND_ICON_EMAIL_SORT);
	}
	else if (nCEType == VOICECALL)
	{
		strCETypeIcon = ":Phone32.png";
		m_hshRow2SUBTYPE.insert(nRowsInGrid, SECOND_ICON_VOICECALL_SORT);
	}
	else
	{
		strCETypeIcon = ":Document32.png";
		m_hshRow2SUBTYPE.insert(nRowsInGrid, SECOND_ICON_DOCUMENT_SORT);
	}

	return strCETypeIcon;
}

QString CommGridView::GetInOutDocumentPixmap(int nInOutDocument, int nRowsInGrid)
{
	//Incoming, outgoing or document.
	QString strInOutIcon;
	if		(nInOutDocument == INCOMING)
	{
		strInOutIcon = ":Incoming32.png";
		m_hshRow2MEDIA.insert(nRowsInGrid, THIRD_ICON_INCOMING_SORT);
	}
	else if (nInOutDocument == OUTGOING)
	{
		strInOutIcon = ":Outgoing32.png";
		m_hshRow2MEDIA.insert(nRowsInGrid, THIRD_ICON_OUTGOING_SORT);
	}
	else
	{
		strInOutIcon = ":InHouse32.png";
		m_hshRow2MEDIA.insert(nRowsInGrid, THIRD_ICON_INHOUSE_SORT);
	}

	return strInOutIcon;
}

QString CommGridView::GetAppTypePixmap(int nAppType, int nRowsInGrid)
{
	//Email, document or voice call icon.
	QString strAppIcon;
	if		(nAppType == EMAIL)
	{
		strAppIcon = ":Outlook.png";
		m_hshRow2APPTYPE.insert(nRowsInGrid, EMAIL_SORT);
	}
	else if (nAppType == VOICECALL)
	{
		strAppIcon = ":SkypeBlue_32x32.png";
		m_hshRow2APPTYPE.insert(nRowsInGrid, VOICECALL_SORT);
	}
	else
	{
		strAppIcon = ":Word32.png";
		m_hshRow2APPTYPE.insert(nRowsInGrid, DOCUMENT_SORT);
	}

	return strAppIcon;
}

QStringList CommGridView::CreateThirdColumnStrings(int nCEType, DbRecordSet &recEntityRecord,int nRow)
{
	//Make owner string.
	QString strDataOwner;
	//If this is a task then use task owner else use entity owner (Marin - issue #673).
	if (recEntityRecord.getDataRef(nRow, "BTKS_ID").toInt() > 0)
	{
		if (!recEntityRecord.getDataRef(nRow, "A2.BPER_LAST_NAME").toString().isEmpty())
			strDataOwner = recEntityRecord.getDataRef(nRow, "A2.BPER_LAST_NAME").toString();

		if(!strDataOwner.isEmpty() && !recEntityRecord.getDataRef(nRow, "A2.BPER_FIRST_NAME").toString().isEmpty())
			strDataOwner += " " + recEntityRecord.getDataRef(nRow, "A2.BPER_FIRST_NAME").toString();
		else if (!recEntityRecord.getDataRef(nRow, "A2.BPER_FIRST_NAME").toString().isEmpty())
			strDataOwner += recEntityRecord.getDataRef(nRow, "A2.BPER_FIRST_NAME").toString();

		if(!strDataOwner.isEmpty() && !recEntityRecord.getDataRef(nRow, "B1.BORG_NAME").toString().isEmpty())
			strDataOwner += ", " + recEntityRecord.getDataRef(nRow, "B1.BORG_NAME").toString();
		else if (!recEntityRecord.getDataRef(nRow, "B1.BORG_NAME").toString().isEmpty())
			strDataOwner += recEntityRecord.getDataRef(nRow, "B1.BORG_NAME").toString();
	}
	else
	{
		if (!recEntityRecord.getDataRef(nRow, "A.BPER_LAST_NAME").toString().isEmpty())
			strDataOwner = recEntityRecord.getDataRef(nRow, "A.BPER_LAST_NAME").toString();

		if(!strDataOwner.isEmpty() && !recEntityRecord.getDataRef(nRow, "A.BPER_FIRST_NAME").toString().isEmpty())
			strDataOwner += " " + recEntityRecord.getDataRef(nRow, "A.BPER_FIRST_NAME").toString();
		else if (!recEntityRecord.getDataRef(nRow, "A.BPER_FIRST_NAME").toString().isEmpty())
			strDataOwner += recEntityRecord.getDataRef(nRow, "A.BPER_FIRST_NAME").toString();

		if(!strDataOwner.isEmpty() && !recEntityRecord.getDataRef(nRow, "B.BORG_NAME").toString().isEmpty())
			strDataOwner += ", " + recEntityRecord.getDataRef(nRow, "B.BORG_NAME").toString();
		else if (!recEntityRecord.getDataRef(nRow, "B.BORG_NAME").toString().isEmpty())
			strDataOwner += recEntityRecord.getDataRef(nRow, "B.BORG_NAME").toString();
	}

	//Make contact string.
	QString strContactData;
	if (!recEntityRecord.getDataRef(nRow, "BCNT_LASTNAME").toString().isEmpty())
		strContactData = recEntityRecord.getDataRef(nRow, "BCNT_LASTNAME").toString();

	if (!strContactData.isEmpty() && !recEntityRecord.getDataRef(nRow, "BCNT_FIRSTNAME").toString().isEmpty())
		strContactData += " " + recEntityRecord.getDataRef(nRow, "BCNT_FIRSTNAME").toString();
	else if (!recEntityRecord.getDataRef(nRow, "BCNT_FIRSTNAME").toString().isEmpty())
		strContactData += recEntityRecord.getDataRef(nRow, "BCNT_FIRSTNAME").toString();

	if (!strContactData.isEmpty() && !recEntityRecord.getDataRef(nRow, "BCNT_ORGANIZATIONNAME").toString().isEmpty())
		strContactData += ", " + recEntityRecord.getDataRef(nRow, "BCNT_ORGANIZATIONNAME").toString();
	else if (!recEntityRecord.getDataRef(nRow, "BCNT_ORGANIZATIONNAME").toString().isEmpty())
		strContactData += recEntityRecord.getDataRef(nRow, "BCNT_ORGANIZATIONNAME").toString();

	//Make project string.
	QString strProjectData	= recEntityRecord.getDataRef(nRow, "BUSP_CODE").toString()	 + \
		" "	 + recEntityRecord.getDataRef(nRow, "BUSP_NAME").toString();
	if (recEntityRecord.getDataRef(nRow, "BUSP_CODE").isNull())
		strProjectData = "";

	QStringList list;
	list << strDataOwner << strContactData << strProjectData;

	return list;
}

//Fill hashes per row and returns date last modified item used for sorting.
void CommGridView::FillRowHashes(int nRow, int nCEType, DbRecordSet &recEntityRecord, int nRowInList)
{
	//Get items values from recordset.
	int nCENT_ID = recEntityRecord.getDataRef(nRowInList, "CENT_ID").toInt();
	int nBTKS_ID = recEntityRecord.getDataRef(nRowInList, "BTKS_ID").toInt();
	int nBTKS_SYSTEM_STATUS = recEntityRecord.getDataRef(nRowInList, "BTKS_SYSTEM_STATUS").toInt();
	int nBPER_ID = recEntityRecord.getDataRef(nRowInList, "A.BPER_ID").toInt();
	int nBCNT_ID = recEntityRecord.getDataRef(nRowInList, "BCNT_ID").toInt();
	int nCENT_SYS = recEntityRecord.getDataRef(nRowInList, "CENT_SYSTEM_TYPE_ID").toInt();
	int nBUSP_ID = recEntityRecord.getDataRef(nRowInList, "BUSP_ID").toInt();
	int nBDMD_IS_CHECK_OUT = 0;
	int nBDMD_CHECK_OUT_USER_ID = 0;
	int nBEM_UNREAD_FLAG = 0;

	int nENTITY_ID;
	if (nCEType == EMAIL)
	{
		nENTITY_ID	= recEntityRecord.getDataRef(nRowInList, "BEM_ID").toInt();
		nBEM_UNREAD_FLAG = recEntityRecord.getDataRef(nRowInList, "BEM_UNREAD_FLAG").toInt();
	}
	else if (nCEType == VOICECALL)
		nENTITY_ID	= recEntityRecord.getDataRef(nRowInList, "BVC_ID").toInt();
	else
	{
		nENTITY_ID	= recEntityRecord.getDataRef(nRowInList, "BDMD_ID").toInt();
		nBDMD_IS_CHECK_OUT = recEntityRecord.getDataRef(nRowInList, "BDMD_IS_CHECK_OUT").toInt();
		nBDMD_CHECK_OUT_USER_ID= recEntityRecord.getDataRef(nRowInList, "BDMD_CHECK_OUT_USER_ID").toInt();
	}

	//Fill hashes.
	m_hshRow2CENT_ID.insert(nRow, nCENT_ID);
	m_hshRow2BTKS_ID.insert(nRow, nBTKS_ID);
	m_hshRow2BTKS_SYSTEM_STATUS.insert(nRow, nBTKS_SYSTEM_STATUS);

	//If there is task then task owner is person id to be shown i owner sapne.
	if (!recEntityRecord.getDataRef(nRowInList, "BTKS_ID").isNull())
		m_hshRow2BPER_ID.insert(nRow, recEntityRecord.getDataRef(nRowInList, "A2.BPER_ID").toInt());
	else
		m_hshRow2BPER_ID.insert(nRow, nBPER_ID);

	m_hshRow2BCNT_ID.insert(nRow, nBCNT_ID);
	m_hshRow2CENT_SYS.insert(nRow, nCENT_SYS);
	m_hshRow2BUSP_ID.insert(nRow, nBUSP_ID);
	m_hshRow2ENTITY_ID.insert(nRow, nENTITY_ID);
	m_hshRow2BDMD_IS_CHECK_OUT.insert(nRow, nBDMD_IS_CHECK_OUT);
	m_hshRow2BEM_UNREAD_FLAG.insert(nRow, nBEM_UNREAD_FLAG);
	m_hshRow2BDMD_CHECK_OUT_USER_ID.insert(nRow, nBDMD_CHECK_OUT_USER_ID);
}

void CommGridView::ClearHashes()
{
	//Data and Sorting hashes.
	m_hshRow2CENT_ID.clear();
	m_hshRow2BTKS_ID.clear();
	m_hshRow2BTKS_SYSTEM_STATUS.clear();
	m_hshRow2BPER_ID.clear();
	m_hshRow2BCNT_ID.clear();
	m_hshRow2CENT_SYS.clear();
	m_hshRow2BUSP_ID.clear();
	m_hshRow2ENTITY_ID.clear();
	m_hshRow2BDMD_IS_CHECK_OUT.clear();
	m_hshRow2BEM_UNREAD_FLAG.clear();
	m_hshRow2BDMD_CHECK_OUT_USER_ID.clear();
	m_hshRow2DATE1.clear();			
	m_hshRow2DATE2.clear();			
	m_hshRow2STATUS.clear();		
	m_hshRow2APPTYPE.clear();		
	m_hshRow2ORIGINATOR.clear();	
	m_hshRow2OWNER.clear();			
	m_hshRow2CONTACT.clear();		
	m_hshRow2PROJECT.clear();		
	m_hshRow2SUBTYPE.clear();		
	m_hshRow2MEDIA.clear();			
	m_hshRow2TIME1.clear();			
	m_hshRow2TIME2.clear();			

	//Hashes to pass to model;
	m_hshRow2Color.clear();
	m_hshRow2Pix1.clear();
	m_hshRow2Pix2.clear();
	m_hshRow2Pix3.clear();
	m_hshRow2Pix4.clear();
	m_hshRow2Date1.clear();
	m_hshRow2Date2.clear();
	m_hshRow2Autor.clear();
	m_hshRow2Subject.clear();
	m_hshRow2Descr.clear();
	m_hshRow2SecondColDisplay.clear();
	m_hshRow2Owner.clear();
	m_hshRow2Contact.clear();
	m_hshRow2Project.clear();
}

void CommGridView::FirstColumnData(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow)
{
	//Locals.
	int nTask			= -1;
	int nInOutDocument	= -1;
	int nAppType		= -1;
	QDateTime datFirstRow, datSecondRow;
	QString strFirstRow;
	QString strSecondRow; 
	QString strThirdRow;
	//Check scheduled time.
	nTask = CheckTask(recEntityRecord,nRow);

	//If not tasked then find appropriate strings. 
	if(nTask == NOT_SCHEDULED || (!recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull() && (recEntityRecord.getDataRef(nRow, "BTKS_IS_TASK_ACTIVE").toInt() == 0  || recEntityRecord.getDataRef(nRow, "BTKS_SYSTEM_STATUS").toInt() >= 3)))
	{
		if (nCEType == VOICECALL)
		{
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BVC_START").toDateTime();
			datSecondRow = recEntityRecord.getDataRef(nRow, "BVC_DURATION").toDateTime();
		}
		else if (nCEType == EMAIL)
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BEM_RECV_TIME").toDateTime();
		else
			datFirstRow	 = recEntityRecord.getDataRef(nRow, "BDMD_DAT_LAST_MODIFIED").toDateTime();
	}
	//Else write start time, due time and who tasked it.
	else
	{
		//recEntityRecord.Dump();
		datFirstRow	 = recEntityRecord.getDataRef(nRow, "BTKS_START").toDateTime();
		datSecondRow = recEntityRecord.getDataRef(nRow, "BTKS_DUE").toDateTime();
		if (!recEntityRecord.getDataRef(nRow, "A1.BPER_INITIALS").toString().isEmpty())
			strThirdRow	 = tr("by ") + recEntityRecord.getDataRef(nRow, "A1.BPER_INITIALS").toString();
	}

	strFirstRow	 = datFirstRow.toString("d.M.yyyy h:mm");
	strSecondRow = datSecondRow.toString("d.M.yyyy h:mm");

	//Fill DATE 1 and DATE 2 sort cache.
	m_hshRow2DATE1.insert(nRowsInGrid, datFirstRow.date());
	m_hshRow2TIME1.insert(nRowsInGrid, datFirstRow.time());
	m_hshRow2DATE2.insert(nRowsInGrid, datSecondRow.date());
	m_hshRow2TIME2.insert(nRowsInGrid, datSecondRow.time());

	//Check is it outgoing, incoming or document.
	nInOutDocument = CheckItemDirection(nCEType, recEntityRecord,nRow);

	//Set application type.
	nAppType = nCEType;

	//Set icons.
	QString strPix1;
	if (nTask == NOT_SCHEDULED)
	{
		if (nCEType == DOCUMENT)
		{
			int nDocType = recEntityRecord.getDataRef(nRow, "BDMD_DOC_TYPE").toInt();
			if (nDocType == GlobalConstants::DOC_TYPE_INTERNET_FILE)
			{
				if (recEntityRecord.getDataRef(nRow, "BDMD_IS_CHECK_OUT").toBool())
				{
					if (!recEntityRecord.getDataRef(nRow, "BDMD_CHECK_OUT_USER_ID").isNull())
					{
						int nCheckedUserID = recEntityRecord.getDataRef(nRow, "BDMD_CHECK_OUT_USER_ID").toInt();
						//If this user checked out this file then show checked out icon.
						if (nCheckedUserID == g_pClientManager->GetPersonID())
						{
							strPix1 = ":Check_Out_32.png";
							m_hshRow2STATUS.insert(nRowsInGrid, CHECK_OUT_SORT);
						}
						//Else if checked out by someone else then use locked icon.
						else
						{
							strPix1 = ":Locked_32.png";
							m_hshRow2STATUS.insert(nRowsInGrid, LOCKED_SORT);
						}
					}
					//Empty icon.
					else
						strPix1 = GetTaskPixmap(nTask, nRowsInGrid);
				}
				//If document is template then put template flag.
				else if (recEntityRecord.getDataRef(nRow, "BDMD_TEMPLATE_FLAG").toBool())
				{
					strPix1 = ":Template_32.png";
					m_hshRow2STATUS.insert(nRowsInGrid, TEMPLATE_SORT);
				}
				else
					strPix1 = GetTaskPixmap(nTask, nRowsInGrid);
			}
			//If document is template then put template flag.
			else if (recEntityRecord.getDataRef(nRow, "BDMD_TEMPLATE_FLAG").toBool())
			{
				strPix1 = ":Template_32.png";
				m_hshRow2STATUS.insert(nRowsInGrid, TEMPLATE_SORT);
			}
			else
				strPix1 = GetTaskPixmap(nTask, nRowsInGrid);
		}
		else if (nCEType == EMAIL)
		{
			//If document is template then put template flag.
			bool bEmailIsTemplate = recEntityRecord.getDataRef(nRow, "BEM_TEMPLATE_FLAG").toBool();
			if (bEmailIsTemplate)
			{
				strPix1 = ":Template_32.png";
				m_hshRow2STATUS.insert(nRowsInGrid, TEMPLATE_SORT);
			}
			//If email and unread show unread icon but not template (issue #700).
			else if (recEntityRecord.getDataRef(nRow, "BEM_UNREAD_FLAG").toBool() && !recEntityRecord.getDataRef(nRow, "BEM_OUTGOING").toBool())
			{
				strPix1 = ":Unread_Email_32.png";
				m_hshRow2STATUS.insert(nRowsInGrid, UNREAD_EMAIL_SORT);
			}
			//Empty icon.
			else
				strPix1 = GetTaskPixmap(nTask, nRowsInGrid);
		}
		else if(nCEType == VOICECALL)
		{
			//If voice call missed show missed icon.
			if (recEntityRecord.getDataRef(nRow, "BVC_CALL_STATUS").toInt() == SkypeInterface::CALL_STATE_Missed)
			{
				strPix1 = ":Missed_Call_32.png";
				m_hshRow2STATUS.insert(nRowsInGrid, MISSED_CALL_SORT);
			}
			//Empty icon.
			else
				strPix1 = GetTaskPixmap(nTask, nRowsInGrid);
		}
		else
			strPix1 = GetTaskPixmap(nTask, nRowsInGrid);
	}
	else
		strPix1 = GetTaskPixmap(nTask, nRowsInGrid);

	QString strPix2 = GetCETypePixmap(nCEType, nRowsInGrid);
	QString strPix3 = GetInOutDocumentPixmap(nInOutDocument, nRowsInGrid);
	QString strPix4;
	if (nInOutDocument==INHOUSE)
	{
		int AppID=recEntityRecord.getDataRef(nRow,"BDMD_APPLICATION_ID").toInt();
		//4-th icon sort hash.
		m_hshRow2APPTYPE.insert(nRowsInGrid, AppID);

		if (DocumentHelper::GetApplicationIcon(AppID).isNull())
			strPix4 = ":Empty_icon.png";
		else
			strPix4 = QVariant(AppID).toString();

		int DocTypeID=recEntityRecord.getDataRef(nRow,"BDMD_DOC_TYPE").toInt();
		strPix3 = QVariant(DocTypeID).toString();
	}
	else
		strPix4 = GetAppTypePixmap(nAppType, nRowsInGrid);

	//Fill originator sort cache.
	m_hshRow2ORIGINATOR.insert(nRowsInGrid, strThirdRow);

	//Fill model hashes.
	m_hshRow2Color.insert(nRowsInGrid, nInOutDocument);
	m_hshRow2Pix1.insert(nRowsInGrid, strPix1);
	m_hshRow2Pix2.insert(nRowsInGrid, strPix2);
	m_hshRow2Pix3.insert(nRowsInGrid, strPix3);
	m_hshRow2Pix4.insert(nRowsInGrid, strPix4);
	m_hshRow2Date1.insert(nRowsInGrid, strFirstRow);
	m_hshRow2Date2.insert(nRowsInGrid, strSecondRow);
	m_hshRow2Autor.insert(nRowsInGrid, strThirdRow);
}

void CommGridView::SecondColumnData(int nCEType, DbRecordSet &recEntityRecord, int nRow, int nRowInGrid)
{
	//Create widget and layout.
	QString strSubjectString = "";
	QString strDescrString = "";
	//Check does it have a task.
	if (recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull() || (!recEntityRecord.getDataRef(nRow, "BTKS_ID").isNull() && (recEntityRecord.getDataRef(nRow, "BTKS_IS_TASK_ACTIVE").toInt() == 0  || recEntityRecord.getDataRef(nRow, "BTKS_SYSTEM_STATUS").toInt() >= 3)))
	{
		if (nCEType == EMAIL)
			strSubjectString = recEntityRecord.getDataRef(nRow, "BEM_SUBJECT").toString();
		else if (nCEType == VOICECALL)
			strSubjectString = recEntityRecord.getDataRef(nRow, "BVC_DESCRIPTION").toString();
		//For documents.
		else if (nCEType == DOCUMENT)
		{
			double dblSizeb = recEntityRecord.getDataRef(nRow, "BDMD_SIZE").toDouble();
			//If there is document name then display it.
			strSubjectString = recEntityRecord.getDataRef(nRow, "BDMD_NAME").toString();  //B.T. doc name is always here
			if (m_bSideBarMode)
				strSubjectString += " " +  DataHelper::GetFormatedFileSize(dblSizeb);
			else
				strSubjectString += "\n" +  DataHelper::GetFormatedFileSize(dblSizeb);

			if (!recEntityRecord.getDataRef(nRow, "CET_ID").isNull())
				strSubjectString += ", " + recEntityRecord.getDataRef(nRow, "CET_CODE").toString() + " " + recEntityRecord.getDataRef(nRow, "CET_NAME").toString();

			strDescrString = recEntityRecord.getDataRef(nRow, "BDMD_DESCRIPTION").toString();
		}
	}
	else
	{
		//Marin wanted first subject of task then description. Since formats are different we will omit that
		//and hope he forget that.
		if (nCEType == EMAIL)
		{
			if(recEntityRecord.getDataRef(nRow, "BTKS_SUBJECT").toString().isEmpty())
				strSubjectString = recEntityRecord.getDataRef(nRow, "BEM_SUBJECT").toString();
			else
				strSubjectString = recEntityRecord.getDataRef(nRow, "BTKS_SUBJECT").toString();
		}
		else
			strSubjectString = recEntityRecord.getDataRef(nRow, "BTKS_SUBJECT").toString();

		if (!recEntityRecord.getDataRef(nRow, "BTKS_DESCRIPTION").isNull())
			strDescrString = recEntityRecord.getDataRef(nRow, "BTKS_DESCRIPTION").toString();
		else
		{
			//For email description is too big so skip it.
			if(nCEType == VOICECALL)
				strDescrString = recEntityRecord.getDataRef(nRow, "BTKS_DESCRIPTION").toString();
			else if(nCEType == DOCUMENT)
				strDescrString = recEntityRecord.getDataRef(nRow, "BDMD_NAME").toString();
		}
	}

	QString strSubjectTmp = strSubjectString;
	QString strDescrTmp	  = strDescrString;

	if (strSubjectTmp.startsWith("<html"))
	{
		QTextEdit edit;
		edit.setHtml(strSubjectTmp);
		strSubjectTmp = edit.toPlainText();
	}

	if (strDescrTmp.startsWith("<html"))
	{
		QTextEdit edit;
		edit.setHtml(strDescrTmp);
		strDescrTmp = edit.toPlainText();
	}

	QString strDisplay;
	if (strSubjectString.isEmpty())
		strDisplay = strDescrString;
	else
		strDisplay = strSubjectTmp + "\n" + strDescrTmp;

	//Fill model hashes.
	m_hshRow2SecondColDisplay.insert(nRowInGrid, strDisplay);
	m_hshRow2Subject.insert(nRowInGrid, strSubjectString);
	m_hshRow2Descr.insert(nRowInGrid, strDescrString);
}

void CommGridView::ThirdColumnData(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow)
{
	QStringList list = CreateThirdColumnStrings(nCEType, recEntityRecord,nRow);

	//Sort hashes.
	m_hshRow2OWNER.insert(nRowsInGrid, list.value(0));
	m_hshRow2CONTACT.insert(nRowsInGrid, list.value(1));
	m_hshRow2PROJECT.insert(nRowsInGrid, recEntityRecord.getDataRef(nRow, "BUSP_CODE").toString());

	//Fill model caches.
	m_hshRow2Owner.insert(nRowsInGrid, list.value(0));
	m_hshRow2Contact.insert(nRowsInGrid, list.value(1));
	m_hshRow2Project.insert(nRowsInGrid, list.value(2));
}

//B.T. improved: unselect & select
void CommGridView::SelectDataInSourceRecordSet(int nEntitySysType, int nEntityID,bool bSelect)
{
	if (bSelect)
	{
		switch(nEntitySysType)
		{
		case GlobalConstants::CE_TYPE_VOICE_CALL:
			{
				m_recVoiceCall->find("CENT_ID", nEntityID, false, false, false);
			}
			break;

		case GlobalConstants::CE_TYPE_DOCUMENT:
			{
				m_recDocument->find("CENT_ID", nEntityID, false, false, false);
			}
			break;
		case GlobalConstants::CE_TYPE_EMAIL:
			{
				m_recEmail->find("CENT_ID", nEntityID, false, false, false);
			}
			break;
		}
	}
	else
	{
		switch(nEntitySysType)
		{
		case GlobalConstants::CE_TYPE_VOICE_CALL:
			{
				int nRow=m_recVoiceCall->find("CENT_ID", nEntityID, true);
				if (nRow!=-1)
				{
					m_recVoiceCall->selectRow(nRow,false);
				}

			}
			break;

		case GlobalConstants::CE_TYPE_DOCUMENT:
			{
				int nRow=m_recDocument->find("CENT_ID", nEntityID, true);
				if (nRow!=-1)
				{
					m_recDocument->selectRow(nRow,false);
				}
			}
			break;
		case GlobalConstants::CE_TYPE_EMAIL:
			{
				int nRow=m_recEmail->find("CENT_ID", nEntityID, true);
				if (nRow!=-1)
				{
					m_recEmail->selectRow(nRow,false);
				}
			}
			break;
		}
	}
}

void CommGridView::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
	m_recEmail->clearSelection();
	m_recVoiceCall->clearSelection();
	m_recDocument->clearSelection();

	//Locals.
	int nOwnerID = 0;
	int nContactID = 0;
	int nProjectID = 0;
	int nEntitySysType = 0;
	int nTaskID = 0;
	int nTaskStatus = 0;
	int nEntityID = 0;
	int nDocID = 0;
	int nBDMD_IS_CHECK_OUT = 0;
	int nBDMD_CHECK_OUT_USER_ID = 0;
	QList<int> lstSelTaskList;
	QList<int> lstUnreadEmailsBEM_IDs;
	QList<int> lstStartTaskEmailsCENT_IDs, lstStartTaskPhoneCallsCENT_IDs;

	QList<int> lstSelectedRows;
	QModelIndexList lstSelectedIndexes = selectedIndexes();
	int nSelRowCount = selectedIndexes().count();
	if (nSelRowCount==0)
		return;
	for (int i = 0; i < nSelRowCount; ++i)
	{
		QModelIndex index = lstSelectedIndexes.value(i);
		int nRow = index.row();
		if (lstSelectedRows.contains(nRow))
			continue;
		lstSelectedRows << nRow;
		nEntitySysType		= m_hshRow2CENT_SYS.value(nRow);
		nEntityID			= m_hshRow2CENT_ID.value(nRow);
		nOwnerID			= m_hshRow2BPER_ID.value(nRow);
		nContactID			= m_hshRow2BCNT_ID.value(nRow);
		nProjectID			= m_hshRow2BUSP_ID.value(nRow);
		nTaskID				= m_hshRow2BTKS_ID.value(nRow);
		nTaskStatus			= m_hshRow2BTKS_SYSTEM_STATUS.value(nRow);
		nDocID				= m_hshRow2ENTITY_ID.value(nRow);
		nBDMD_IS_CHECK_OUT	= m_hshRow2BDMD_IS_CHECK_OUT.value(nRow);
		nBDMD_CHECK_OUT_USER_ID = m_hshRow2BDMD_CHECK_OUT_USER_ID.value(nRow);

		//Select data in recordset.
		SelectDataInSourceRecordSet(nEntitySysType, nEntityID,true);

		//Task list.
		if (nTaskID > 0 && !lstSelTaskList.contains(nTaskID))
			lstSelTaskList << nTaskID;
		//Else schedule it for start task #1456.
		else
		{
			if (nEntitySysType == GlobalConstants::CE_TYPE_EMAIL)
			{
				if (CheckItemDirection(EMAIL, *m_recEmail, m_recEmail->find("CENT_ID", nEntityID, true)) == INCOMING)
					lstStartTaskEmailsCENT_IDs << nEntityID;
			}
			else if(nEntitySysType == GlobalConstants::CE_TYPE_VOICE_CALL)
			{
				if (CheckItemDirection(VOICECALL, *m_recVoiceCall, m_recVoiceCall->find("CENT_ID", nEntityID, true)) == INCOMING)
					lstStartTaskPhoneCallsCENT_IDs << nEntityID;
			}
		}

		//Check is it unread email.
		if (nEntitySysType == GlobalConstants::CE_TYPE_EMAIL && m_hshRow2BEM_UNREAD_FLAG.value(nRow) == 1)
			lstUnreadEmailsBEM_IDs << nDocID;
	}

	QAbstractItemView::selectionChanged(selected, deselected);

	//Emit section clicked signal.
	emit SectionClicked(nOwnerID, nContactID, nProjectID, nEntitySysType, nTaskID, lstSelTaskList, nEntityID, nDocID, nBDMD_IS_CHECK_OUT, nBDMD_CHECK_OUT_USER_ID, nTaskStatus, lstUnreadEmailsBEM_IDs, lstStartTaskEmailsCENT_IDs, lstStartTaskPhoneCallsCENT_IDs);
}

void CommGridView::on_CellDoubleClicked(const QModelIndex &index)

{
	//Emit signal for detail popup.
	emit SectionDoubleClicked();
}

void CommGridView::SelectBySourceRecordSetRows(QList<int> lstSelectedCENT_IDs)
{
	//Find selected rows.
	QList<int> lstSelectedRows;
	//Loop and fill item selection.
	for (int i = 0; i < lstSelectedCENT_IDs.size(); ++i)
	{
		int nCENT_ID = lstSelectedCENT_IDs.value(i);
		if (m_hshRow2CENT_ID.values().contains(nCENT_ID))
		{
			int nRow = m_hshRow2CENT_ID.key(nCENT_ID);
			if (!lstSelectedRows.contains(nRow))
				lstSelectedRows << nRow;
		}
	}

	//Sort selected rows and get starting and ending row.
	qSort(lstSelectedRows);
	int nStartRow = lstSelectedRows.first();
	int nEndingRow= lstSelectedRows.last();
	//Find start and ending indexes.
	QModelIndex startIndex = model()->index(nStartRow, 0);
	QModelIndex endIndex = model()->index(nEndingRow, 0);
	QItemSelection itemSelection(startIndex, endIndex);
	//Get selection model, clear and select.
	QItemSelectionModel *selModel = selectionModel();
	selModel->clearSelection();
	//Create item selection.
	selModel->select(itemSelection, QItemSelectionModel::Select | QItemSelectionModel::Rows);
	scrollTo(startIndex);
}



void CommGridView::startDrag(Qt::DropActions supportedActions)
{
	/*
	//what item is selected, only one is ok?
	if (m_recDocument->getSelectedCount()==1)
	{	
		//get document: (
		int nRow=m_recDocument->getSelectedRow();
		//get filename, create file drag
		Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();
		bool bCTRL=false;
		if(Qt::ControlModifier == (Qt::ControlModifier & keys))
			bCTRL = true;
		DbRecordSet rowRevision,recChkInfo;
		int nDocID=m_recDocument->getDataRef(nRow,"BDMD_ID").toInt();
		QString strNewPath=g_CommManager.CheckOutDocument(m_recDocument->getDataRef(nRow,"BDMD_ID").toInt(),m_recDocument->getDataRef(nRow,"BDMD_DOC_PATH").toString(),bCTRL,rowRevision,recChkInfo,false,false,true);
		if (!strNewPath.isEmpty())
		{
			QMimeData *dragData = new QMimeData;
			dragData->setUrls(QList<QUrl>() <<QUrl::fromLocalFile(strNewPath));
			QDrag *drag = new QDrag(this);
			drag->setPixmap(QPixmap (":Document32.png"));
			drag->setMimeData(dragData);
			int nActionExecuted=drag->start(Qt::MoveAction | Qt::CopyAction);
			g_CommManager.ClearCheckOutDocumentAfterDrag(nDocID,strNewPath);
		}

	}
	*/
}