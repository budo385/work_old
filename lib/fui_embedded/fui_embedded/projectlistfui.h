#ifndef PROJECTLISTFUI_H
#define PROJECTLISTFUI_H

#include <QWidget>
#include "ui_projectlistfui.h"

#include "fuibase.h"
#include "common/common/observer_ptrn.h"

class ProjectListFui : public FuiBase, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	ProjectListFui(QString strFuiName, QWidget *parent = 0);
	~ProjectListFui();

	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

private:
	Ui::ProjectListFuiClass ui;
};

#endif // PROJECTLISTFUI_H
