#include "fui_selectedfavorite.h"

#include "bus_client/bus_client/clientcontactmanager.h"
#include "thememanager.h"
#include "mobilehelper.h"
#include "fui_contactdetail.h"
#include "fui_voicecall.h"
#include "email_menuwidget.h"
#include <QDesktopServices>
#include <QDebug>
#include "sms_dialog.h"
#include "fuimanager.h"
extern FuiManager *g_FuiManager;
#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

Fui_SelectedFavorite::Fui_SelectedFavorite(QString strFuiName, QWidget *parent)
	: FuiBase(strFuiName, parent)
{
	ui.setupUi(this);

	//Setup picture.
	MobileHelper::SelectedContactImageFrame_Create(ui.picFlow_frame, &m_pPic);

	//Setup upper tool buttons layout.
	m_pBtnPhone = new StyledPushButton(this,":Comm_Phone24.png",":Comm_Phone24.png","","",0,0);
	m_pBtnSendEmail = new StyledPushButton(this,":Comm_Email24.png",":Comm_Email24.png","","",0,0);
	m_pBtnSMS = new StyledPushButton(this,":Comm_SMS24.png",":Comm_SMS24.png","","",0,0);
	m_pBtnChat = new StyledPushButton(this,":Comm_Chat24.png",":Comm_Chat24.png","","",0,0);
	
	QHBoxLayout *hbox2=new QHBoxLayout;
	hbox2->setSpacing(0);
	hbox2->setMargin(0);
	hbox2->addSpacing(10);
	hbox2->addWidget(m_pBtnPhone);
	hbox2->addStretch(1);
	hbox2->addWidget(m_pBtnSendEmail);
	hbox2->addStretch(1);
	hbox2->addWidget(m_pBtnSMS);
	hbox2->addStretch(1);
	hbox2->addWidget(m_pBtnChat);
	hbox2->addSpacing(10);

	ui.frameToolBar->setLayout(hbox2);
	ui.frameToolBar->setStyleSheet(ThemeManager::GetToolbarBkgStyle());

	//Setup lower tool buttons layout.
	m_pBtnContactDetails = new StyledPushButton(this,":Info_Details.png",":Info_Details.png","","",0,0);
	m_pBtnContactDocuments = new StyledPushButton(this,":Info_Documents.png",":Info_Documents.png","","",0,0);
	m_pBtnContactInternetAddresses = new StyledPushButton(this,":Info_Internet.png",":Info_Internet.png","","",0,0);
	m_pBtnContactList = new StyledPushButton(this,":Info_Contacts.png",":Info_Contacts.png","","",0,0);
	m_pBtnContactProjects = new StyledPushButton(this,":Info_Projects.png",":Info_Projects.png","","",0,0);
	
	QHBoxLayout *hbox3=new QHBoxLayout;
	hbox3->setSpacing(0);
	hbox3->setMargin(0);
	hbox3->addSpacing(5);
	hbox3->addWidget(m_pBtnContactDetails);
	hbox3->addStretch(1);
	hbox3->addWidget(m_pBtnContactDocuments);
	hbox3->addStretch(1);
	hbox3->addWidget(m_pBtnContactInternetAddresses);
	hbox3->addStretch(1);
	hbox3->addWidget(m_pBtnContactList);
	hbox3->addStretch(1);
	hbox3->addWidget(m_pBtnContactProjects);
	hbox3->addSpacing(5);

	ui.lowerButtons_frame->setLayout(hbox3);
	ui.lowerButtons_frame->setStyleSheet(".QFrame "+ThemeManager::GetMobileSolidBkg_Dark());
	
	//connect signals
	connect(m_pBtnPhone,SIGNAL(clicked()),this,SLOT(On_PhoneButton_clicked()));
	connect(m_pBtnSendEmail,SIGNAL(clicked()),this,SLOT(On_SendEmail_clicked()));
	connect(m_pBtnSMS,SIGNAL(clicked()),this,SLOT(On_SMS_clicked()));
	connect(m_pBtnChat,SIGNAL(clicked()),this,SLOT(On_Chat_clicked()));
	connect(m_pBtnContactDetails,SIGNAL(clicked()),this,SLOT(On_Details_clicked()));
	connect(m_pBtnContactDocuments,SIGNAL(clicked()),this,SLOT(On_Documents_clicked()));
	connect(m_pBtnContactInternetAddresses,SIGNAL(clicked()),this,SLOT(On_InternetAddresses_clicked()));
	connect(m_pBtnContactList,SIGNAL(clicked()),this,SLOT(On_ContactList_clicked()));
	connect(m_pBtnContactProjects,SIGNAL(clicked()),this,SLOT(On_ProjectList_clicked()));
}

Fui_SelectedFavorite::~Fui_SelectedFavorite()
{

}

void Fui_SelectedFavorite::on_selectionChange(int nEntityRecordID)
{
	if (m_nEntityRecordID == nEntityRecordID)
		return;

	m_nEntityRecordID = nEntityRecordID;
	
	//Get contact data.
	m_recContactData.clear();
	Status status;
	ClientContactManager::GetContactData(status, m_nEntityRecordID, m_recContactData,!g_pClientManager->IsLogged());
	
	//Setup picture flow.
	MobileHelper::SelectedContactImageFrame_SetContactPicture(m_pPic, m_recContactData);

	//Actual contact frame.
	ui.actualContact_frame->RefreshActualName(m_recContactData);

}

void Fui_SelectedFavorite::On_PhoneButton_clicked()
{
	//issue request: if only 1 phone->skip phone select:
	if (m_recContactData.getRowCount()!=1)
		return;
	DbRecordSet lstPhone=m_recContactData.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
	if (lstPhone.getRowCount()==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Contact does not have Phone entry!"));
		return;
	}
	//open TAPI if 1 phone:
	if (lstPhone.getRowCount()==1)
	{
		//open TAPI direct:
		QString strPhone=lstPhone.getDataRef(0,"BCMP_SEARCH").toString();
		//open FUI
		QWidget* pFUIWidget=g_FuiManager->OpenFUI(VOICE_CALL,FuiBase::MODE_EMPTY,-1,false);
		FUI_VoiceCall *pFUI=dynamic_cast<FUI_VoiceCall*>(pFUIWidget);
		if (pFUI)
		{
			pFUI->Initialize(m_recContactData,strPhone);
		}
		if (pFUIWidget)pFUIWidget->show();
		return;
	}

	//open FUI if more phones:
	QWidget* pFUIWidget=g_FuiManager->OpenFUI(CONTACT_DETAILS,FuiBase::MODE_READ,m_nEntityRecordID,false);
	FUI_ContactDetail *pFUI=dynamic_cast<FUI_ContactDetail*>(pFUIWidget);
	if (pFUI)
	{
		pFUI->SetDetailMode(FUI_ContactDetail::CONTACT_DETAIL_PHONE);
	}
	if (pFUIWidget)pFUIWidget->show();
}

void Fui_SelectedFavorite::On_SendEmail_clicked()
{
	if(!g_FuiManager->GoOffline(false,true))
		return;

	//issue request: if only 1 phone->skip phone select:
	if (m_recContactData.getRowCount()!=1)
		return;
	DbRecordSet lstEmail=m_recContactData.getDataRef(0,"LST_EMAIL").value<DbRecordSet>();
	if (lstEmail.getRowCount()==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Contact does not have Email entry!"));
		return;
	}
	//open SendMail if 1:
	if (lstEmail.getRowCount()==1)
	{
		QString strEmail=lstEmail.getDataRef(0,"BCME_ADDRESS").toString();
		//open FUI
		QWidget* pFUIWidget=g_FuiManager->OpenFUI(EMAIL_TEMPLATES);
		Email_MenuWidget *pFUI=dynamic_cast<Email_MenuWidget*>(pFUIWidget);
		if (pFUI)
		{
			pFUI->SetDefaults(&m_recContactData);
			pFUI->SetDefaultMail(strEmail);
		}
		return;
	}


	//open FUI
	QWidget* pFUIWidget=g_FuiManager->OpenFUI(CONTACT_DETAILS,FuiBase::MODE_READ,m_nEntityRecordID,false);
	FUI_ContactDetail *pFUI=dynamic_cast<FUI_ContactDetail*>(pFUIWidget);
	if (pFUI)
	{
		pFUI->SetDetailMode(FUI_ContactDetail::CONTACT_DETAIL_EMAIL);
	}
	if (pFUIWidget)pFUIWidget->show();
}

void Fui_SelectedFavorite::On_SMS_clicked()
{
	//issue request: if only 1 phone->skip phone select:
	if (m_recContactData.getRowCount()!=1)
		return;
	DbRecordSet lstPhone=m_recContactData.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
	if (lstPhone.getRowCount()==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Contact does not have Phone entry!"));
		return;
	}
	//open TAPI if 1 phone:
	if (lstPhone.getRowCount()>0)
	{
		QString strPhone=lstPhone.getDataRef(0,"BCMP_SEARCH").toString();

		SMS_Dialog dlg;
		dlg.SetNumber(strPhone);
		//dlg.SetContact(0);
		dlg.exec();
	}
}

void Fui_SelectedFavorite::On_Chat_clicked()
{

}

void Fui_SelectedFavorite::On_Details_clicked()
{
	g_FuiManager->OpenFUI(CONTACT_DETAILS, FuiBase::MODE_READ, m_nEntityRecordID);
}

void Fui_SelectedFavorite::On_Documents_clicked()
{
	if(!g_FuiManager->GoOffline(false,true))
		return;

	g_FuiManager->OpenFUI(CONTACT_GRID, FuiBase::MODE_READ, m_nEntityRecordID);
}

void Fui_SelectedFavorite::On_InternetAddresses_clicked()
{
	if (m_recContactData.getRowCount()!=1)
		return;
	DbRecordSet lstInternet=m_recContactData.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
	if (lstInternet.getRowCount()==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Contact does not have Internet entry!"));
		return;
	}
	//if 1 open directly:
	if (lstInternet.getRowCount()==1)
	{
		QString strInternet=lstInternet.getDataRef(0,"BCMI_ADDRESS").toString();
		QDesktopServices::openUrl(strInternet.trimmed());
		return;
	}

	//open FUI
	QWidget* pFUIWidget=g_FuiManager->OpenFUI(CONTACT_DETAILS,FuiBase::MODE_READ,m_nEntityRecordID,false);
	FUI_ContactDetail *pFUI=dynamic_cast<FUI_ContactDetail*>(pFUIWidget);
	if (pFUI)
	{
		pFUI->SetDetailMode(FUI_ContactDetail::CONTACT_DETAIL_INTERNET);
	}
	if (pFUIWidget)pFUIWidget->show();
}

void Fui_SelectedFavorite::On_ContactList_clicked()
{
	g_FuiManager->OpenFUI(CONTACT_LIST, FuiBase::MODE_READ, m_nEntityRecordID);
}

void Fui_SelectedFavorite::On_ProjectList_clicked()
{
	if(!g_FuiManager->GoOffline(false,true))
		return;

	g_FuiManager->OpenFUI(PROJECT_LIST, FuiBase::MODE_READ, m_nEntityRecordID);
}


void Fui_SelectedFavorite::OnOfflineModeChanged(bool bOffline)
{

}