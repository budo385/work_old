#ifndef SMS_DIALOG_H
#define SMS_DIALOG_H

#include <QDialog>
#include "generatedfiles/ui_sms_dialog.h"

class SMS_Dialog : public QDialog
{
	Q_OBJECT

public:
	SMS_Dialog(QWidget *parent = 0);
	~SMS_Dialog();

	void SetNumber(QString &strNumber);
	void SetContact(int nID){ m_nContactID = nID; }
	void UpdateStats();

protected slots:
	void on_btnCancel_clicked();
	void on_btnSend_clicked();
	void on_SMS_textChanged();

private:
	Ui::SMS_DialogClass ui;
	int m_nContactID;
};

#endif // SMS_DIALOG_H
