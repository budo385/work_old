#ifndef COMMGRIDVIEW_H
#define COMMGRIDVIEW_H

#include <QTableView>
#include <QItemDelegate>
#include <QItemSelection>

#include "common/common/dbrecordset.h"
#include "commgridmodel.h"

class FirstColumnDelegateSideBarView : public QItemDelegate
{
	Q_OBJECT

public:
	FirstColumnDelegateSideBarView(QObject *parent = 0){};
	~FirstColumnDelegateSideBarView(){};

protected:
	void drawBackground ( QPainter *painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	void drawDisplay(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QString & text) const;
	void drawDecoration ( QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QPixmap &pixmap) const;
};

class CommGridView : public QTableView
{
	Q_OBJECT

		enum TaskType
	{
		NOT_SCHEDULED		= 1,
		SCHEDULED			= 2,
		SCHEDULED_DUENOW	= 3,
		SCHEDULED_OVERDUE	= 4
	};

	enum AppType
	{
		VOICECALL,
		EMAIL,
		DOCUMENT
	};

	enum FirstColumn2IconSortIdentificators
	{
		SECOND_ICON_VOICECALL_SORT	= 1,
		SECOND_ICON_EMAIL_SORT		= 2,
		SECOND_ICON_DOCUMENT_SORT	= 3
	};

	enum FirstColumn3IconSortIdentificators
	{
		THIRD_ICON_INCOMING_SORT = 1,
		THIRD_ICON_OUTGOING_SORT = 2,
		THIRD_ICON_INHOUSE_SORT	 = 3
	};

	enum FirstColumn4IconSortIdentificators
	{
		VOICECALL_SORT			= -1,
		EMAIL_SORT				= -2,
		DOCUMENT_SORT			= -3
	};

	enum IncomingOutgoing
	{
		INHOUSE					= 0,
		OUTGOING				= 1,
		INCOMING				= 2
	};

	enum FirstColumn1IconSortIdentificators
	{
		NOT_SCHEDULED_SORT		= 1,
		SCHEDULED_SORT			= 2,
		SCHEDULED_DUENOW_SORT	= 3,
		SCHEDULED_OVERDUE_SORT	= 4,
		EMPTY_SORT				= 5,
		INCOMING_SORT			= 6,
		OUTGOING_SORT			= 7,
		INHOUSE_SORT			= 8, 
		TEMPLATE_SORT			= 9,
		CHECK_OUT_SORT			= 10,
		LOCKED_SORT				= 11,
		UNREAD_EMAIL_SORT		= 12,
		MISSED_CALL_SORT		= 13
	};

public:
	CommGridView(QWidget *parent = 0);
	~CommGridView();

	void						Initialize(DbRecordSet *recEmails, DbRecordSet *recDocuments, DbRecordSet *recVoiceCalls, int nGridType, bool bClearSelection = true);
	void						RefreshContactItem(int nCEType, DbRecordSet &recEntityRecord);
	void						SortTable(DbRecordSet *recSortRecordSet = NULL, bool bClearSelection = false);
	void 						SelectBySourceRecordSetRows(QList<int> lstSelectedCENT_IDs);

	//simulate drop event
protected:
	void						dragMoveEvent ( QDragMoveEvent * event );
	void						dragEnterEvent ( QDragEnterEvent * event );
	void						dropEvent(QDropEvent *event);
	void						startDrag(Qt::DropActions supportedActions);

	public slots:
		void					SelectGridFromSourceRecordSet();
		void					SelectSourceRecordSetFromGrid();

private:
	void						SetLayout();
	int							CheckTask(DbRecordSet &recEntityRecord,int nRow=0);
	int							CheckItemDirection(int nCEType, DbRecordSet &recEntityRecord,int nRow);	//Check is it incoming, outgoing or inhouse.
	QString						GetTaskPixmap(int nTask, int nRowsInGrid);
	QString						GetCETypePixmap(int nCEType, int nRowsInGrid);
	QString						GetInOutDocumentPixmap(int nInOutDocument, int nRowsInGrid);
	QString						GetAppTypePixmap(int nAppType, int nRowsInGrid);
	QStringList					CreateThirdColumnStrings(int nCEType, DbRecordSet &recEntityRecord,int nRow);
	void 						FillRowHashes(int nRow, int nCEType, DbRecordSet &recEntityRecord, int nRowInList);
	void						ClearHashes();

	void						FirstColumnData(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow);
	void						SecondColumnData(int nCEType, DbRecordSet &recEntityRecord, int nRow, int nRowInGrid);
	void						ThirdColumnData(int nCEType, int nRowsInGrid, DbRecordSet &recEntityRecord, int nRow);

	void						SelectDataInSourceRecordSet(int nEntitySysType, int nEntityID,bool bSelect=true);

	CommGridModel				*m_pGridModel;
	int							m_nRowCount;
	bool						m_bFirstTimeIn;
	int							m_nGridType;
	DbRecordSet					*m_recEmail,
		*m_recVoiceCall,
		*m_recDocument;
	bool						m_bSideBarMode;		//Are we in sidebar mode.

	//Data and Sorting hashes.
	QHash<int,int>				m_hshRow2CENT_ID;
	QHash<int,int>				m_hshRow2BTKS_ID;
	QHash<int,int>				m_hshRow2BTKS_SYSTEM_STATUS;
	QHash<int,int>				m_hshRow2BPER_ID;
	QHash<int,int>				m_hshRow2BCNT_ID;
	QHash<int,int>				m_hshRow2CENT_SYS;
	QHash<int,int>				m_hshRow2BUSP_ID;
	QHash<int,int>				m_hshRow2ENTITY_ID;
	QHash<int,int>				m_hshRow2BDMD_IS_CHECK_OUT;
	QHash<int,int>				m_hshRow2BEM_UNREAD_FLAG;
	QHash<int,int>				m_hshRow2BDMD_CHECK_OUT_USER_ID;
	QHash<int,QDate>			m_hshRow2DATE1;			//For sorting - first date shown in first column.
	QHash<int,QDate>			m_hshRow2DATE2;			//For sorting - second date shown in first column.
	QHash<int,int>				m_hshRow2STATUS;		//For sorting - first icon shown in first column (scheduled, not scheduled, ...).
	QHash<int,int>				m_hshRow2APPTYPE;		//For sorting - fourth icon shown in first column (application type).
	QHash<int,QString>			m_hshRow2ORIGINATOR;	//For sorting - originator shown in first column under dates for task.
	QHash<int,QString>			m_hshRow2OWNER;			//For sorting - owner shown in third column.
	QHash<int,QString>			m_hshRow2CONTACT;		//For sorting - contact shown in third column.
	QHash<int,QString>			m_hshRow2PROJECT;		//For sorting - project shown in third column.
	QHash<int,int>				m_hshRow2SUBTYPE;		//For sorting - second icon shown in first column (voice call, email, document).
	QHash<int,int>				m_hshRow2MEDIA;			//For sorting - third icon shown in first column (incoming, outgoing, inhouse).
	QHash<int,QTime>			m_hshRow2TIME1;			//For sorting - first date's time shown in first column.
	QHash<int,QTime>			m_hshRow2TIME2;			//For sorting - second date's time shown in first column.

	//Sorting recordsets.
	DbRecordSet					m_recSortRecordSet;		//Sorting recordset.
	DbRecordSet					m_recSortByRecordset;	//Sorting recordset.

	//Hashes to pass to model;
	QHash<int,int>				m_hshRow2Color;
	QHash<int,QString>			m_hshRow2Pix1;
	QHash<int,QString>			m_hshRow2Pix2;
	QHash<int,QString>			m_hshRow2Pix3;
	QHash<int,QString>			m_hshRow2Pix4;
	QHash<int,QString>			m_hshRow2Date1;
	QHash<int,QString>			m_hshRow2Date2;
	QHash<int,QString>			m_hshRow2Autor;
	QHash<int,QString>			m_hshRow2Subject;
	QHash<int,QString>			m_hshRow2Descr;
	QHash<int,QString>			m_hshRow2SecondColDisplay;
	QHash<int,QString>			m_hshRow2Owner;
	QHash<int,QString>			m_hshRow2Contact;
	QHash<int,QString>			m_hshRow2Project;

private slots:
	void						selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
	void 						on_CellDoubleClicked(const QModelIndex &index);

signals:
		void 						SectionDoubleClicked();
		void 						SectionClicked(int nOwnerID, int nContactID, int nProjectID, int nEntitySysType, int nTaskID, QList<int> lstTaskList, int nEntityID, int nDocID, int nBDMD_IS_CHECK_OUT, int nBDMD_CHECK_OUT_USER_ID, int nBTKS_SYSTEM_STATUS, QList<int> lstUnreadEmailsCENTIDs, QList<int> lstStartTaskEmailsCENT_IDs, QList<int> lstStartTaskPhoneCallsCENT_IDs);
};

#endif // COMMGRIDVIEW_H
