#ifndef FUIBASE_H
#define FUIBASE_H

#include <QWidget>
#include <QAction>

#define	PIC_FLOW_MENU_ON 0

class FuiBase : public QWidget
{
	Q_OBJECT

public:
	//Modes:
	enum FUI_Mode 
	{
		MODE_READ	= 0,	
		MODE_EDIT	= 1,			
		MODE_INSERT	= 2,			
		MODE_EMPTY	= 3,
		MODE_DELETE	= 4 
	};

	FuiBase(QString strFuiName, QWidget *parent);
	~FuiBase();

	virtual void			on_selectionChange(int nEntityRecordID){};
	virtual QString			GetFuiName(){return m_strFuiName;};
	virtual void			SetFuiName(QString strFuiName){m_strFuiName=strFuiName;};
	virtual QAction*		SetLeftSoftKeyAction(){return NULL;}; 
	virtual QList<QAction*>	SetRightSoftKeyAction();
	virtual void			SetMode(int nMode){m_nMode=nMode;};
	virtual	void			OnOfflineModeChanged(bool bOffline=true);

protected:
	int m_nEntityRecordID;
	QString m_strFuiName;
	int m_nMode;

private:
	QAction		*m_pActCancel;
};

#endif // FUIBASE_H
