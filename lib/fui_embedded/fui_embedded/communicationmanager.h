#ifndef COMMUNICATIONMANAGER_H
#define COMMUNICATIONMANAGER_H

#include <QString>
#include <QTimer>
#include <QObject>
#include "common/common/observer_ptrn.h"
#include <QProgressDialog>
#include "common/common/dbrecordset.h"


class CommunicationManager : public QObject, public ObsrPtrn_Observer
{
	Q_OBJECT

public:

	CommunicationManager();
	~CommunicationManager();

	void Initialize();
	void ClearData();
	void EndUserSession(); //clear all caches
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());


	//DOCUMENTS
	//--------------------------------------------------------
	//bool PackAndSendDocuments(DbRecordSet &lstDocuments,QPoint *pos=NULL,int nProjectID=-1,int nContactID=-1);
	//void EmailReply(DbRecordSet &recEmail,QPoint *pos=NULL);
	//void EmailForward(DbRecordSet &recEmail,QPoint *pos=NULL);
	static void BatchReadAllCEMenuData();
	//bool ProcessDocumentDrop(QList<QUrl> &lst, bool bCreateTemplates,bool bAcceptApplications,bool bIgnoreHttpURLs=false, QString strCategory="");
	bool DropCommDataToProject(DbRecordSet &Data,int nProjectID);

	//EMAIL:
	//--------------------------------------------------------
	//void OpenMailInExternalClient(int nEmailID,QByteArray byteExternID, QString strExternApp);
	//DbRecordSet ProcessEmailContentFromOutlook(bool bLogOut=false);
	void AddEmailToSentMailList(int nEmailID, QByteArray byteOutlookID, int nCENT_ID);
	bool OpenMailInOutlookFromDatabaseRecord(int nEmailID);
		

	//TASK:
	//--------------------------------------------------------
	void SetTaskDone(DbRecordSet &lstTaskIDs);

public slots:
	void OnCommunicationInProgress(int nTicks,qint64 done, qint64 total);
	void OnDocumentChanged(const QString & path);

private slots:
	void OnProcessOutlookEmails();

private:

	void RedefineSentMailList();
	bool OpenMailInOutlook(QByteArray byteExternID, QString strExternApp, bool bSupressMsg=false);

	void ApplicationInstallFinder(int nDocTemplateID);
	void DetermineDocType(DbRecordSet &lstPaths, QList<QUrl> &lstSource);

	
	bool ClearDocumentProgress();


	QTimer m_Timer;
	bool m_bTimerConnected;

	//document cancel:
	bool m_bDocumentTransferProgressActive;
	bool m_bDocumentTransferProgressCanceled;
	QString m_strDocumentTransferName;
	QProgressDialog *m_prgDlgDocumentTransfer;

	DbRecordSet m_lstSentEmails;					//global list of all sent emails<id, id>
};


#endif // COMMUNICATIONMANAGER_H
