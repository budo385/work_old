#include "contactgrid.h"

ContactGrid::ContactGrid(QString strFuiName, QWidget *parent)
	: FuiBase(strFuiName, parent)
{
	ui.setupUi(this);

	//Initialize grid widget.
	ui.CommGridWidget_widget->Initialize(1, this);
}

ContactGrid::~ContactGrid()
{

}

void ContactGrid::on_selectionChange(int nEntityRecordID)
{
	if (m_nEntityRecordID==nEntityRecordID)
		return;
	m_nEntityRecordID = nEntityRecordID;

	//Set contact grid data.
	ui.CommGridWidget_widget->SetData(nEntityRecordID);
}
