#ifndef CONTACTNAMEFRAME_H
#define CONTACTNAMEFRAME_H

#include <QFrame>
#include "ui_contactnameframe.h"
#include "common/common/dbrecordset.h"

class ContactNameFrame : public QFrame
{
	Q_OBJECT

public:
	ContactNameFrame(QWidget *parent = 0);
	~ContactNameFrame();

	void RefreshActualName(DbRecordSet rowContact);

private:
	Ui::ContactNameFrameClass ui;
};

#endif // CONTACTNAMEFRAME_H
