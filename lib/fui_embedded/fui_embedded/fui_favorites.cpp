#include "fui_favorites.h"

#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/mobilehelper.h"
#include "bus_client/bus_client/selectionpopup.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "fuimanager.h"
extern FuiManager *g_FuiManager;
#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

Fui_Favorites::Fui_Favorites(QString strFuiName, QWidget *parent)
	: FuiBase(strFuiName, parent)
{
	ui.setupUi(this);
	
	//Connect picture flow.
	if (PIC_FLOW_MENU_ON)
	{
		MobileHelper::PictureFlowWidget_Create(ui.picFlow_frame, &m_pPictureFlow, QStringList(), QSize(60,80), 0, 1/*favorites pic. flow*/);
		//m_pPictureFlow->setBackground()
		connect(m_pPictureFlow, SIGNAL(centerIndexSelectedForEditing(int)), this, SLOT(OncenterIndexSelectedForEditing(int)));
		connect(m_pPictureFlow, SIGNAL(centerIndexChanged(int)), this, SLOT(on_centerIndexChanged(int)));
	}

	//Left soft key action.
	m_pSoftKeyAction = new QAction("Open", NULL);
	connect(m_pSoftKeyAction, SIGNAL(triggered()), this, SLOT(on_LeftSoftKey_triggered()));
	m_pAddFavorite = new QAction(tr("Favorites: Add"),this);
	connect(m_pAddFavorite, SIGNAL(triggered()), this, SLOT(OnAddFav()));
	m_pRemoveFavorite = new QAction(tr("Favorites: Remove"),this);
	connect(m_pRemoveFavorite, SIGNAL(triggered()), this, SLOT(OnRemoveFav()));
	m_pAddPicToFav = new QAction(tr("Add Picture"),this);
	connect(m_pAddPicToFav, SIGNAL(triggered()), this, SLOT(OnAddPicToFav()));


	ReloadData();
}

Fui_Favorites::~Fui_Favorites()
{
	
}

QAction* Fui_Favorites::SetLeftSoftKeyAction()
{
	return m_pSoftKeyAction;
}

void Fui_Favorites::on_centerIndexChanged(int index)
{
	m_nCurrentCenterIndex = index;
	ui.actualContact_frame->RefreshActualName(m_recContactData.getRow(index));
}

void Fui_Favorites::OncenterIndexSelectedForEditing(int index)
{
	int nContactID = m_recContactData.getDataRef(index, "BCNT_ID").toInt();
	g_FuiManager->OpenFUI(SELECTED_FAVORITE, FuiBase::MODE_READ, nContactID);
}

void Fui_Favorites::on_LeftSoftKey_triggered()
{
	if (m_nCurrentCenterIndex==-1)
		return;
	OncenterIndexSelectedForEditing(m_nCurrentCenterIndex);
}



QList<QAction*>	Fui_Favorites::SetRightSoftKeyAction()
{
	QList<QAction*> lst;
	lst.append(m_pAddFavorite);
	lst.append(m_pRemoveFavorite);
	lst.append(m_pAddPicToFav);
	lst+=FuiBase::SetRightSoftKeyAction(); //from cancel
	return lst;
}

void Fui_Favorites::OnAddFav()
{
	if(!g_FuiManager->GoOffline(false,true))
		return;

	//open pop up
	SelectionPopup dlgPopUp;
	dlgPopUp.Initialize(ENTITY_BUS_CONTACT);
	int nResult=dlgPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		dlgPopUp.GetSelectedData(nRecordID,record);
		if (nRecordID>0)
		{
			Status err;
			ClientContactManager::AddFavorite(err,nRecordID);
			_CHK_ERR(err);

			int nCurrentCenterIndex=m_recContactData.find("BCNT_ID",nRecordID,true);
			ReloadData(nCurrentCenterIndex);
		}
	}

}
void Fui_Favorites::OnRemoveFav()
{
	if(!g_FuiManager->GoOffline(false,true))
		return;

	int nContactID = m_recContactData.getDataRef(m_nCurrentCenterIndex, "BCNT_ID").toInt();

	Status err;
	ClientContactManager::RemoveFavorite(err,nContactID);
	_CHK_ERR(err);
}



void Fui_Favorites::OnOfflineModeChanged(bool bOffline)
{

}

void Fui_Favorites::ReloadData(int nDefaultFavIndex, bool bRealodFromServer)
{
	//Get favorites.
	Status status;
	ClientContactManager::GetFavoriteList(status, m_recContactData,!g_pClientManager->IsLogged(),bRealodFromServer);

	//Set current center slide - see the number of favorites, divide by 2 and take ceiling value - shortly take the middle one.
	if (nDefaultFavIndex==-1)
	{
		int nRowCount = m_recContactData.getRowCount();
		//QMessageBox::information(NULL,"","cnt favs:"+QVariant(nRowCount).toString());
		m_nCurrentCenterIndex = ceil(nRowCount/2);
	}
	else
		m_nCurrentCenterIndex=nDefaultFavIndex;

	//Picture flow.
	if (PIC_FLOW_MENU_ON)
		MobileHelper::PictureFlowWidget_SetContactsPictures(m_pPictureFlow, m_recContactData, m_nCurrentCenterIndex);

	//Actual contact frame.
	if(m_recContactData.getRowCount()>0)
		ui.actualContact_frame->RefreshActualName(m_recContactData.getRow(m_nCurrentCenterIndex));
	else
		m_nCurrentCenterIndex=-1;
}

void Fui_Favorites::OnAddPicToFav()
{
	if(!g_pClientManager->IsLogged())
		return;

	int nContactID = m_recContactData.getDataRef(m_nCurrentCenterIndex, "BCNT_ID").toInt();
	if (nContactID>0)
	{
		Status err;
		ClientContactManager::AddPictureFavorite(err,nContactID);
		_CHK_ERR(err);
		ReloadData(m_nCurrentCenterIndex,true); //get new pic
	}

}