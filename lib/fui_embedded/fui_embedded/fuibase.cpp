#include "fuibase.h"
#include "fuimanager.h"
extern FuiManager *g_FuiManager;
#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
FuiBase::FuiBase(QString strFuiName, QWidget *parent)
	: QWidget(parent),m_pActCancel(NULL)
{
	m_strFuiName = strFuiName;
	m_nEntityRecordID = -1;
	setAttribute(Qt::WA_DeleteOnClose, true);
	SetMode(MODE_EMPTY);
	m_pActCancel = new QAction(tr("Close Window"),this);
	connect(m_pActCancel, SIGNAL(triggered()), this, SLOT(close()));

}

FuiBase::~FuiBase()
{
	//auto unregister from FUIMANAGER:
	g_FuiManager->CloseFUI(g_FuiManager->GetFuiID(this),true);
}


void FuiBase::OnOfflineModeChanged(bool bOffline)
{
	if (bOffline)
		close();
}



QList<QAction*>	FuiBase::SetRightSoftKeyAction()
{
	QList<QAction*> lst; 
	lst.append(m_pActCancel);
	return lst;

};