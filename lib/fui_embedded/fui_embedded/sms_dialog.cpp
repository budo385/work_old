#include "sms_dialog.h"
#include "os_specific/os_specific/tapi/TapiLine.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/thememanager.h"


//globals:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
//bool IsPhoneNumber(QString strNumber);

SMS_Dialog::SMS_Dialog(QWidget *parent)
	: QDialog(parent, Qt::WindowTitleHint|Qt::WindowSystemMenuHint)
{
	m_nContactID = -1;

	ui.setupUi(this);
//	ui.btnPickPhone->setIcon(QIcon(":arrow_down16.png"));	//Phone16.png
	
	connect(ui.txtSMS, SIGNAL(textChanged()), this, SLOT(on_SMS_textChanged()));
	UpdateStats();

	//header style sheets
	ui.frame->setStyleSheet("QFrame#frame " + ThemeManager::GetSidebarChapter_Bkg());
	ui.label_3->setPixmap(QPixmap(":Comm_SMS48.png"));
	ui.label_4->setStyleSheet("QLabel {color:white; font-style:italic; font-weight:800; font-family:Arial; font-size:12pt}");
	ui.label->setStyleSheet("QLabel {color:white}");
}

SMS_Dialog::~SMS_Dialog()
{
}

void SMS_Dialog::SetNumber(QString &strNumber)
{
	ui.txtPhone->setText(strNumber);
}

void SMS_Dialog::on_btnCancel_clicked()
{
	done(0);
}

void SMS_Dialog::UpdateStats()
{
	int nLetters  = ui.txtSMS->toPlainText().length();
	int nMessages = (nLetters + 159) / 160;

	ui.labelStats->setText(QString(tr("%1 letters typed (%2 messages)")).arg(nLetters).arg(nMessages));
}

void SMS_Dialog::on_SMS_textChanged()
{
	UpdateStats();
}

void SMS_Dialog::on_btnSend_clicked()
{
	QString strPhone = ui.txtPhone->text();
	if(strPhone.isEmpty()){
		QMessageBox::information(NULL, "", tr("You must enter Phone Number!"));
		return;
	}
	//if(!IsPhoneNumber(strPhone)){
	//	QMessageBox::information(NULL, "", tr("Invalid Phone Number!"));
	//	return;
	//}
	QString strMessage = ui.txtSMS->toPlainText();
	if(strMessage.isEmpty()){
		QMessageBox::information(NULL, "", tr("Message is empty!"));
		return;
	}

	/*CTapiLine::*/SendSMS(TRUE, TRUE, NULL, strPhone.utf16(), strMessage.utf16());
}
