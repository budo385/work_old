#ifndef FUI_VOICECALL_H
#define FUI_VOICECALL_H

#include <QTimerEvent>
#include "fuibase.h"
#include "selectedcontactpicture.h"
#include "ui_fui_voicecall.h"
#include "os_specific/os_specific/tapi/tapiline.h"
#include "common/common/dbrecordset.h"

class FUI_VoiceCall : public FuiBase
{
	Q_OBJECT
public:
	FUI_VoiceCall(QString strFuiName, QWidget *parent = 0);
	~FUI_VoiceCall();

	void Initialize(DbRecordSet rowContact,QString strPhone);


private slots:
	void on_btnCallAction1_clicked();
	void tableWidget_itemClicked ( QTableWidgetItem * item);
	void OnTAPICheckTimer();
protected:
	void timerEvent ( QTimerEvent * event ); 

private:
	Ui::FUI_VoiceCallClass ui;
	QString GetTapiDeviceName(int nTapiDevID);
	void ClearCall();
	void OnError();

	CTapiLine m_lineTAPI;
	int m_nTapiDeviceID;
	SelectedContactPicture *m_pPic;
	DbRecordSet m_RowContactData;
	QString m_strPhone;
	bool m_bCallInProgress;
	int m_nTimerID;
};

#endif // FUI_VOICECALL_H
