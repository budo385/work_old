#include "fui_email.h"
#include "bus_client/bus_client/emailhelper.h"
#include "bus_core/bus_core/emailhelpercore.h"
#include "os_specific/os_specific/mailmanager.h"
#include "gui_core/gui_core/thememanager.h"
#include <QInputDialog>
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/globalconstants.h"
#include "db_core/db_core/dbtableiddefinition.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager* g_pSettings;
#include "fuimanager.h"
extern FuiManager *g_FuiManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt* g_pClientManager;

typedef bool (*FN_Call_CreateEmailFromTemplate)(QString bodyTemplate, QString &NewBody, int nContactID, int nProjectID);
bool Call_CreateEmailFromTemplate(QString bodyTemplate, QString &NewBody, int nContactID, int nProjectID)
{
	return EmailHelper::CreateEmailFromTemplate(bodyTemplate,NewBody,nContactID,nProjectID);
}


FUI_Email::FUI_Email(QString strFuiName, QWidget *parent)
: FuiBase(strFuiName, parent)
{
	qDebug() << "ulaz u FUI_Email(QString strFuiName, QWidget *parent)";
	ui.setupUi(this);

	qDebug() << "1 u FUI_Email(QString strFuiName, QWidget *parent)";

	m_CID_ModifyEmailBodyInProgress=false;
	m_nEntityRecordID=-1;
	m_pContinue=NULL;
	m_pAttachment=NULL;
	m_pRecepients=NULL;
	m_pBody=NULL;
	m_nProjectID=0;
	m_strPersonContactID="";
	m_bSerialEmailMode=false;

	qDebug() << "2 u FUI_Email(QString strFuiName, QWidget *parent)";
	ui.labelSubject->setStyleSheet("QLabel "+ThemeManager::GetMobileChapter_Font());
	ui.labelRecepient->setStyleSheet("QLabel "+ThemeManager::GetMobileChapter_Font());
	ui.labelAttach->setStyleSheet("QLabel "+ThemeManager::GetMobileChapter_Font());
	qDebug() << "3 u FUI_Email(QString strFuiName, QWidget *parent)";

	ui.frameRecepient->setStyleSheet("QFrame#frameRecepient "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameSubject->setStyleSheet("QFrame#frameSubject "+ThemeManager::GetSidebarChapter_Bkg());
	ui.frameAttach->setStyleSheet("QFrame#frameAttach "+ThemeManager::GetSidebarChapter_Bkg());
	qDebug() << "4 u FUI_Email(QString strFuiName, QWidget *parent)";


	ui.stackedWidget->setCurrentIndex(0);
	qDebug() << "5 u FUI_Email(QString strFuiName, QWidget *parent)";

	m_recToRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	m_recCCRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	m_recBCCRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	m_lstContactLink.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_CONTACT_LINK));
	m_lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));

	qDebug() << "6 u FUI_Email(QString strFuiName, QWidget *parent)";

	m_pAttachment = new QAction(tr("Email: Attachments"),this);
	connect(m_pAttachment, SIGNAL(triggered()), this, SLOT(OnAttachment()));
	m_pRecepients = new QAction(tr("Email: Recepients"),this);
	connect(m_pRecepients, SIGNAL(triggered()), this, SLOT(OnRecepient()));
	m_pBody = new QAction(tr("Email: Body"),this);
	connect(m_pBody, SIGNAL(triggered()), this, SLOT(OnBody()));

	qDebug() << "7 u FUI_Email(QString strFuiName, QWidget *parent)";

	on_cmdInsert();
	qDebug() << "8 u FUI_Email(QString strFuiName, QWidget *parent)";
	SetPersonData();

	qDebug() << "izlaz iz FUI_Email(QString strFuiName, QWidget *parent)";

//	QMessageBox::information(NULL,"","1");
}

FUI_Email::~FUI_Email()
{

}


/*
	Opus Dei:
	1) recContacts - 1st contact default mail is set to TO, others to BCC
	2) if strEmail is set, it will be picked up from TO contact
	3) project is is assign to SAPNE
	4) template id: mail is read, if recontact>0, addresses are reset, else if reply id is set, addresses are fetched from ce_link from reply mail: swithc to<->from
	5) if reply id is only set: load ce link, switch addresses
*/


/* Must be called right after OpenFui. If not call ClearData(), or the result will be unpredictable and you'll suffer the consequences. */
void FUI_Email::SetDefaults(DbRecordSet recContacts, QString strEmail, int nProjectID, int nTemplateEmailID, bool bScheduleTask,bool bSetAsTemplate, int nEmailReplyID,QStringList *lstZippedFiles,bool bForwardMail)
{
	//SetMode(MODE_INSERT);
	//only strEmail, nTemplateID;

	m_CID_ModifyEmailBodyInProgress=true;
	DbRecordSet lstContactEmails;
	DbRecordSet recReplyEmail;
	DbRecordSet recReplyTask,recReplyAttachments;
	bool bLoadEmailAddresses=true;


	//determine one contact:
	int nContactID=-1;
	if (recContacts.getRowCount()>0)
	{
		//get contact & project from data:
		QString strCntID;
		if(recContacts.getColumnIdx("CELC_CONTACT_ID")>=0)
			strCntID="CELC_CONTACT_ID";
		else if (recContacts.getColumnIdx("BCNT_ID")!=-1)
			strCntID="BCNT_ID";

		if (!strCntID.isEmpty())
			nContactID=recContacts.getDataRef(0,strCntID).toInt();
	}

	//QMessageBox::information(NULL,"","ContactID="+QVariant(nContactID).toString());

	//Template: load from DB, copy+ add recContact fields
	if (nTemplateEmailID!=-1)
	{
		on_selectionChange(nTemplateEmailID);
		ClearTemplateFields();

		if (nEmailReplyID==-1 && !m_bSerialEmailMode)
		{
			QString strNewBody;
			EmailHelper::CreateEmailFromTemplate(m_lstDataSimpleFUI.getDataRef(0,"BEM_BODY").toString(),strNewBody,nContactID,nProjectID);
			m_lstDataSimpleFUI.setData(0,"BEM_BODY",strNewBody);
		}
	}
	else
	{
		on_cmdInsert();
	}

	//prepare mail (reply or forward, or only)
	/*
	if (nEmailReplyID!=-1)
	{
		PrepareReplyForwardMail(nEmailReplyID,!bForwardMail);
	}
	else if (recContacts.getRowCount()>0)
	{
	*/
//	QMessageBox::information(NULL,"","prepare1, mail="+strEmail);
//	QMessageBox::information(NULL,"","rowCnt="+QVariant(recContacts.getRowCount()).toString());
	PrepareMailToSendToContacts(recContacts,strEmail);
//	QMessageBox::information(NULL,"","prepare2");
	//}
	

	//TOFIX:
	//clear duplicates:
	//m_recToRecordSet.removeDuplicates(m_recToRecordSet.getColumnIdx("CONTACT_ID"));
	//m_recBCCRecordSet.removeDuplicates(m_recToRecordSet.getColumnIdx("CONTACT_ID"));
	//m_recCCRecordSet.removeDuplicates(m_recToRecordSet.getColumnIdx("CONTACT_ID"));
	//m_recToRecordSet.Dump();
	ui.Address_tableWidget->ClearAll();
	/*
	if (recContacts.getRowCount()>0 && m_recToRecordSet.getRowCount()==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("No email address for this contact found!"));
	}
	*/
	ui.Address_tableWidget->AppendContacts(m_recToRecordSet, m_recCCRecordSet, m_recBCCRecordSet);

	//Project:
	//if (nProjectID > 0)
	//{
	//	ui.frameProject->SetCurrentEntityRecord(nProjectID);
	//}

	//if sender is empty then set it to logged user...
//	QMessageBox::information(NULL,"","2");
	if (m_lstDataSimpleFUI.getDataRef(0,"BEM_FROM").toString().isEmpty())
	{
		SetPersonData();
	}

	//set as template:
	if (bSetAsTemplate)
	{
		m_lstDataSimpleFUI.setData(0,"BEM_TEMPLATE_FLAG",1);
	}

	//if schedule task:
	//if (bScheduleTask)
	//{
	//	ui.frameTask->m_recTask.setData(0, "BTKS_IS_TASK_ACTIVE", 1);	 //his active
	//}

	//always outgoing:
	m_lstDataSimpleFUI.setData(0,"BEM_OUTGOING",1);



	EmailHelper::UnpackCIDToTemp(m_lstCIDInfo,m_lstDataSimpleFUI,m_lstAttachments);
	ui.listAttachemnts->SetData(m_lstAttachments);
	ui.listAttachemnts->AppendFiles(lstZippedFiles);
	RefreshDisplay();
	m_CID_ModifyEmailBodyInProgress=false;
	ui.Subject_lineEdit->setFocus();;

	
}

void FUI_Email::on_selectionChange(int nEntityRecordID)
{
	//QMessageBox::information(NULL,"","2");

	ui.stackedWidget->setCurrentIndex(0);
	UpdateLeftButton();

	m_nEntityRecordID=nEntityRecordID;
	DbRecordSet lstContactEmails, lst1, lst2;
	Status err;
	//QMessageBox::information(NULL,"","Load template:"+QVariant(nEntityRecordID).toString());
	_SERVER_CALL(BusEmail->Read(err,nEntityRecordID,m_lstDataSimpleFUI,m_lstContactLink,m_lstTaskCache,lstContactEmails,m_lstAttachments, lst1, lst2))
	_CHK_ERR(err);
	if (err.IsOK())
	{
		//if read flag is unset, set it, fire global signal:
		if (m_lstDataSimpleFUI.getRowCount()>0)
		{
			//unread flag
			if (m_lstDataSimpleFUI.getDataRef(0,"BEM_UNREAD_FLAG").toInt()>0)
			{
				_SERVER_CALL(BusEmail->SetEmailRead(err,m_lstDataSimpleFUI.getDataRef(0,"BEM_COMM_ENTITY_ID").toInt()))

			}
			/*
			//if flag=not snyc'd, try to sync
			if (m_lstDataSimpleFUI.getDataRef(0,"BEM_IS_SYNC_IN_PROGRESS").toInt()>0)
			{
				bool bFireWarning=true;
				DbRecordSet lstProcessedEmails=g_CommManager.ProcessEmailContentFromOutlook();
				if (lstProcessedEmails.getColumnIdx("BEM_ID")!=-1)
				{
					int nRow=lstProcessedEmails.find("BEM_ID",m_lstDataSimpleFUI.getDataRef(0,"BEM_ID").toInt(),true);
					if (nRow!=-1)
					{
						bFireWarning=false;
						//update content (re-read from server):
						_SERVER_CALL(BusEmail->Read(err,nRecordID,m_lstDataSimpleFUI,m_lstContactLink,m_lstTaskCache,lstContactEmails,m_lstAttachments))
					}
				}

				//if not synced, warn user:
				if (bFireWarning)
					QMessageBox::warning(this,tr("Warning"),tr("The contents of this email is not yet actual, because the email is still being written or cancelled"));

			}
			*/
		}
		/*
		if (m_lstTaskCache.getRowCount()>0)
		{
			ui.frameTask->m_recTask=m_lstTaskCache; //store into cache & tasker
		}
		*/
		if (m_lstDataSimpleFUI.getRowCount()>0)
		{
			Data2Emails(lstContactEmails,m_lstDataSimpleFUI.getDataRef(0,"BEM_TO").toString(),m_lstDataSimpleFUI.getDataRef(0,"BEM_CC").toString(),m_lstDataSimpleFUI.getDataRef(0,"BEM_BCC").toString());
			//QMessageBox::information(NULL,"",m_lstDataSimpleFUI.getDataRef(0,"BEM_TO").toString());
			//QMessageBox::information(NULL,"",QVariant(m_recToRecordSet.getRowCount()).toString());
			ui.Address_tableWidget->AppendContacts(m_recToRecordSet, m_recCCRecordSet, m_recBCCRecordSet);
		}

		//ShowAttachment();
		//m_bBodyChanged=false;
	}

	//m_lstContactLink.Dump();
	if(!m_CID_ModifyEmailBodyInProgress)
		EmailHelper::UnpackCIDToTemp(m_lstCIDInfo,m_lstDataSimpleFUI,m_lstAttachments);

	ui.listAttachemnts->SetData(m_lstAttachments);
	RefreshDisplay();

	//QMessageBox::information(NULL,"","3");
}

void FUI_Email::RefreshDisplay()
{
	//ui.Address_tableWidget->RefreshDisplay();
	if (m_lstDataSimpleFUI.getRowCount()>0)
	{
		ui.Subject_lineEdit->setText(m_lstDataSimpleFUI.getDataRef(0,"BEM_SUBJECT").toString());
		if(m_lstDataSimpleFUI.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
			ui.TextEdit_widget->SetPlainText(m_lstDataSimpleFUI.getDataRef(0,"BEM_BODY").toString());
		else
			ui.TextEdit_widget->SetHtml(m_lstDataSimpleFUI.getDataRef(0,"BEM_BODY").toString());
	}
	else
	{
		ui.TextEdit_widget->SetPlainText("");
		ui.Subject_lineEdit->setText("");
	}

	if (m_nMode==MODE_READ)
	{
		ui.Address_tableWidget->SetEditMode(false);
		ui.Subject_lineEdit->setDisabled(true);
		ui.TextEdit_widget->SetEditMode(false);
		ui.listAttachemnts->SetEditMode(false);
	}
	else
	{
		ui.Address_tableWidget->SetEditMode(true);
		ui.Subject_lineEdit->setDisabled(false);
		ui.TextEdit_widget->SetEditMode(true);
		ui.listAttachemnts->SetEditMode(true);
	}

}

void FUI_Email::SaveMail()
{
	
	//save body:
	//if (m_bBodyChanged)
	//{
		QString strBody;
		if(m_lstDataSimpleFUI.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
		{
			strBody = ui.TextEdit_widget->GetPlainText();
		}
		else
		{
			strBody = ui.TextEdit_widget->GetHtml();
			MailManager::FixRtfEmail(strBody);
		}
		m_lstDataSimpleFUI.setData(0,"BEM_BODY",strBody);
	//}

	if(!ui.listAttachemnts->GetData(m_lstAttachments,true))
		return;
	EmailHelper::UpdateEmailImageCID(m_lstCIDInfo,m_lstDataSimpleFUI,m_lstAttachments);

	//-----------------------------------------------
	//			CONVERT & CHECK DATA
	//-----------------------------------------------

	//Clear recordsets.
	m_recToRecordSet.clear();
	m_recCCRecordSet.clear();
	m_recBCCRecordSet.clear();
	//Get recipients from recipients table.
	ui.Address_tableWidget->GetRecipients(m_recToRecordSet, m_recCCRecordSet, m_recBCCRecordSet);

	//Convert data:
	QString strTo,strCC,strBCC;
	Emails2Data(strTo,strCC,strBCC);

	m_lstDataSimpleFUI.setData(0,"BEM_TO",strTo);
	m_lstDataSimpleFUI.setData(0,"BEM_CC",strCC);
	m_lstDataSimpleFUI.setData(0,"BEM_BCC",strBCC);


	//-----------------------------------------------
	//			SEND MESSAGE
	//-----------------------------------------------

	bool bMsgSucessSend=true;
	bool bMsgActuallySent=false;

	//if subject is empty, warn
	if (ui.Subject_lineEdit->toPlainText().isEmpty())
	{
		QString strSubject = QInputDialog::getText(this, tr("Message Subject!"), tr("Please enter message subject"), QLineEdit::Normal,	tr("(Subject)"));
		ui.Subject_lineEdit->setText(strSubject);
	}

	Status err;
	//if (m_bSaveSendClicked)
	//{
		//m_lstDataSimpleFUI.Dump();
		bMsgSucessSend=SendMail();
		bMsgActuallySent=true;
		if (!bMsgSucessSend)
		{
			int nResult=QMessageBox::question(this,tr("Sending message failed"),tr("Sending message failed. Do you wish to save email and exit without sending?"),tr("Yes"),tr("No"));
			if (nResult!=0) //if cliked NO, exit
			{
				err.setError(1,tr("Sending message failed!"));
				EmailHelper::UnpackCIDToTemp(m_lstCIDInfo,m_lstDataSimpleFUI,m_lstAttachments); //return body back..
				ui.listAttachemnts->SetData(m_lstAttachments);
				return;
			}
			bMsgActuallySent=false;
		}
		else
		{
			m_lstDataSimpleFUI.setData(0,"BEM_RECV_TIME",QDateTime::currentDateTime());
			QMessageBox::information(this,tr("Success"),tr("Message sent!"));
			close(); //close FUI
		}
/*
		//mark task as done if was active::
		if (bMsgActuallySent && ui.frameTask->m_recTask.getDataRef(0, "BTKS_IS_TASK_ACTIVE").toInt()>0 && rowTask.getRowCount()>0)
		{
			rowTask.setData(0, "BTKS_SYSTEM_STATUS", GlobalConstants::TASK_STATUS_COMPLETED);
			rowTask.setData(0, "BTKS_COMPLETED", QDateTime::currentDateTime());
			if (rowTask.getDataRef(0, "BTKS_OWNER").isNull())
				rowTask.setData(0, "BTKS_OWNER", g_pClientManager->GetPersonID());
		}
*/
		//set date & flag for sync
		//if (bMsgActuallySent)
		//{
			//m_lstDataSimpleFUI.Dump();
			//ui.dateTimeEdit->setDateTime(QDateTime::currentDateTime());
		//	m_lstDataSimpleFUI.setData(0,"BEM_IS_SYNC_IN_PROGRESS",1);
		//}
	//}

	//set datetime:
	

	//reverse back:
	EmailHelper::UnpackCIDToTemp(m_lstCIDInfo,m_lstDataSimpleFUI,m_lstAttachments);
	ui.listAttachemnts->SetData(m_lstAttachments);


}


QAction* FUI_Email::SetLeftSoftKeyAction()
{
	//QMessageBox::information(NULL,"","_1");

	if (!m_pContinue)
	{
		m_pContinue = new QAction(tr("Continue"),this);
		connect(m_pContinue, SIGNAL(triggered()), this, SLOT(OnContinue()));
	}

	UpdateLeftButton();
	return m_pContinue;
	
}

void FUI_Email::OnContinue()
{
	if (ui.stackedWidget->currentIndex()==0)
	{
		ui.stackedWidget->setCurrentIndex(1);
	}
	else if (ui.stackedWidget->currentIndex()==2)
	{
		ui.stackedWidget->setCurrentIndex(1);
	}
	else
	{
		if(m_nMode==MODE_READ)
			close();
		else
			SaveMail();
	}
	UpdateLeftButton();
}

void FUI_Email::OnAttachment()
{
	ui.stackedWidget->setCurrentIndex(2);
	UpdateLeftButton();
}
void FUI_Email::OnRecepient()
{
	ui.stackedWidget->setCurrentIndex(0);
	UpdateLeftButton();
}
void FUI_Email::OnBody()
{
	ui.stackedWidget->setCurrentIndex(1);
	UpdateLeftButton();
}

//from DB to recipient table, updates->to, bcc, cc records
void FUI_Email::Data2Emails(DbRecordSet &lstContactEmails,QString strTo,QString strCC,QString strBCC)
{
	//load data:
	EmailHelperCore::LoadRecipients(lstContactEmails,m_recToRecordSet,strTo);
	EmailHelperCore::LoadRecipients(lstContactEmails,m_recCCRecordSet,strCC);
	EmailHelperCore::LoadRecipients(lstContactEmails,m_recBCCRecordSet,strBCC);
}

//updates-> m_lstContactLink
void FUI_Email::Emails2Data(QString &strTo,QString &strCC,QString &strBCC)
{
	SetRecipientString(m_recToRecordSet, strTo);
	SetRecipientString(m_recCCRecordSet, strCC);
	SetRecipientString(m_recBCCRecordSet, strBCC);

	//Define TVIEW_CE_CONTACT_LINK.
	m_lstContactLink.clear();

	//test:
	/*
	if (!ui.From_lineEdit->text().isEmpty())
	{
		//First write FROM to CE_CONTACT_LINK.
		if (m_strPersonContactID.isEmpty()) //logged = FROM
			m_strPersonContactID = QVariant(g_pClientManager->GetPersonContactID()).toString();
	}
	*/

	DbRecordSet tmp;
	tmp.copyDefinition(m_recToRecordSet);
	tmp.addRow();
	tmp.setData(0, "CONTACT_ID", QVariant(m_strPersonContactID).toInt());


	WriteTo_CE_CONTACT_LINK(tmp, GlobalConstants::CONTACT_LINK_ROLE_FROM);
	//
	//all others:
	WriteTo_CE_CONTACT_LINK(m_recToRecordSet, GlobalConstants::CONTACT_LINK_ROLE_TO);
	WriteTo_CE_CONTACT_LINK(m_recCCRecordSet, GlobalConstants::CONTACT_LINK_ROLE_CC);
	WriteTo_CE_CONTACT_LINK(m_recBCCRecordSet, GlobalConstants::CONTACT_LINK_ROLE_BCC);


	//m_lstContactLink.Dump();
}



void FUI_Email::SetPersonData()
{
	//m_nPersonID = g_pClientManager->GetPersonID();

	qDebug() << "ulaz u SetPersonData()";

	Status status;

	if (g_pClientManager->GetPersonContactID() == 0)
		return;

	m_strPersonContactID = QVariant(g_pClientManager->GetPersonContactID()).toString();

	DbRecordSet tmp;
	g_pClientManager->GetUserContactData(tmp);
	if (tmp.getRowCount()==0 || tmp.getColumnCount()==0)
		return;

	QString strName  = tmp.getDataRef(0, "BCME_NAME").toString();
	QString strEmail = tmp.getDataRef(0, "BCME_ADDRESS").toString();


	//Add it to form.
	if (m_lstDataSimpleFUI.getRowCount()>0)
	{
		m_lstDataSimpleFUI.setData(0,"BEM_FROM",EmailHelperCore::GetFullEmailAddress(strName,strEmail,true));
	}
	
	qDebug() << "izlaz iz SetPersonData()";
}


void FUI_Email::SetRecipientString(DbRecordSet &recRecipients, QString &strRecipients)
{
	int nRowCount = recRecipients.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		QString strRecipientName  = recRecipients.getDataRef(i, "CONTACT_NAME").toString();
		QString strRecipientEmail = recRecipients.getDataRef(i, "CONTACT_EMAIL").toString();

		//Set return string.
		strRecipients += EmailHelperCore::GetFullEmailAddress(strRecipientName,strRecipientEmail,true);
	}
}



void FUI_Email::WriteTo_CE_CONTACT_LINK(DbRecordSet &recRecipients, int nType /*1=From 2=To 3=CC 4=BCC*/)
{
	DbRecordSet tmpCntLink;
	tmpCntLink.copyDefinition(m_lstContactLink);

	//Define recordset.
	int nRowCount = recRecipients.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nContactID = recRecipients.getDataRef(i, "CONTACT_ID").toInt();

		//If contact ID is null then skip.
		if (nContactID<=0)
			continue;
		tmpCntLink.addRow();
		int nRow = tmpCntLink.getRowCount() - 1;
		tmpCntLink.setData(nRow, "CELC_CONTACT_ID", nContactID);
		tmpCntLink.setData(nRow, "CELC_LINK_ROLE_ID", nType);
	}

	//remove duplicate  contacts from role:
	tmpCntLink.removeDuplicates(tmpCntLink.getColumnIdx("CELC_CONTACT_ID"));


	m_lstContactLink.merge(tmpCntLink);
}





void FUI_Email::ClearTemplateFields()
{
	if (m_lstDataSimpleFUI.getRowCount()>0)
	{
		m_lstDataSimpleFUI.setData(0,"BEM_ID",0);
		m_lstDataSimpleFUI.setData(0,"BEM_TEMPLATE_FLAG",0);
		m_lstDataSimpleFUI.setData(0,"BEM_TEMPLATE_NAME","");
		m_lstDataSimpleFUI.setData(0,"BEM_TEMPLATE_DESCRIPTION","");
		m_lstDataSimpleFUI.setData(0,"CENT_OWNER_ID",g_pClientManager->GetPersonID());
		QByteArray tmpEmpty;
		m_lstDataSimpleFUI.setData(0,"BEM_EXT_ID",tmpEmpty);
		m_lstDataSimpleFUI.setData(0,"BEM_EXT_APP_DB_INSTANCE","");
		m_lstDataSimpleFUI.setData(0,"BEM_EXT_APP","");
	}
}

void FUI_Email::on_cmdInsert()
{
	qDebug() << "ulaz u on_cmdInsert()";
	m_nEntityRecordID=-1;
	m_lstDataSimpleFUI.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_COMM_ENTITY));
	m_lstDataSimpleFUI.clear();
	m_lstDataSimpleFUI.addRow();
	m_lstDataSimpleFUI.setData(0,"BEM_EMAIL_TYPE",GlobalConstants::EMAIL_TYPE_HTML);

	qDebug() << "1 on_cmdInsert()";

	//set owner=logged----->>>
	int nLoggedID=g_pClientManager->GetPersonID();
	qDebug() << "1.1 on_cmdInsert()";
	if (nLoggedID!=0)
		m_lstDataSimpleFUI.setData(0,"CENT_OWNER_ID",nLoggedID);
	qDebug() << "1.2 on_cmdInsert()";
	m_lstDataSimpleFUI.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_EMAIL);
	qDebug() << "1.3 on_cmdInsert()";
	m_lstDataSimpleFUI.setData(0,"CENT_IS_PRIVATE",0);
	qDebug() << "1.4 on_cmdInsert()";
	//issue: 1462:
	qDebug() << "1.5 on_cmdInsert()";
	DbRecordSet recContactData;
	qDebug() << "1.6 on_cmdInsert()";
	g_pClientManager->GetUserContactData(recContactData);
	qDebug() << "1.7 on_cmdInsert()";
	if (recContactData.getRowCount()>0)
	{
		if (recContactData.getDataRef(0,"BCNT_OUT_MAIL_AS_PRIV").toInt()>0)
			m_lstDataSimpleFUI.setData(0,"CENT_IS_PRIVATE",1);
	}
	qDebug() << "1.8 on_cmdInsert()";
	m_lstDataSimpleFUI.setData(0,"BEM_OUTGOING",1);

	qDebug() << "2 on_cmdInsert()";
	
	m_strPersonContactID="";

	m_lstTaskCache.clear();
	m_lstContactLink.clear();
	m_lstAttachments.clear();
	m_recToRecordSet.clear();
	m_recCCRecordSet.clear();
	m_recBCCRecordSet.clear();
	ui.Address_tableWidget->ClearAll();
	ui.listAttachemnts->ClearAll();

	qDebug() << "3 on_cmdInsert()";
	
	RefreshDisplay();
	
	qDebug() << "izlaz iz on_cmdInsert()";
}


bool FUI_Email::SendMail()
{
	/*
	bool bOK=false;
	bool bExternalClient = ui.radExternalMail->isChecked();
	if(bExternalClient)
	{
		//try Outlook MAPI
		bOK=OutlookSendMailByDLL();
		if (!bOK)
		{
			return MailManager::SendMailDefaultClient(m_lstDataSimpleFUI,m_recToRecordSet,m_recCCRecordSet,m_recBCCRecordSet);
		}
	}
	else
	{
	*/
/*
		//save body:
		if (m_bBodyChanged)
		{
			QString strBody;
			if(m_lstDataSimpleFUI.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT ||
				ui.chkPlainText->checkState() == Qt::Checked)
			{
				strBody = ui.TextEdit_widget->GetPlainText();
				m_lstDataSimpleFUI.setData(0, "BEM_EMAIL_TYPE", GlobalConstants::EMAIL_TYPE_PLAIN_TEXT); //force plaintext
			}
			else
			{
				strBody = ui.TextEdit_widget->GetHtml();
				MailManager::FixRtfEmail(strBody);
			}
			m_lstDataSimpleFUI.setData(0,"BEM_BODY",strBody);
		}
		else
		{
			//check if text-only requested and we have HTML
			if(ui.chkPlainText->checkState() == Qt::Checked &&
				m_lstDataSimpleFUI.getDataRef(0,"BEM_EMAIL_TYPE").toInt() != GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
			{
				//conversion required
				QString strBody = ui.TextEdit_widget->GetPlainText();
				m_lstDataSimpleFUI.setData(0,"BEM_BODY",strBody);
				m_lstDataSimpleFUI.setData(0, "BEM_EMAIL_TYPE", GlobalConstants::EMAIL_TYPE_PLAIN_TEXT); //force plaintext
			}
		}
*/
		//UpdateEmailImageCID();

		SMTPConnectionSettings conn;
		conn.m_strHost				= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_NAME).toString();
		conn.m_nPort				= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_PORT).toInt();
		conn.m_strUserName			= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USER_NAME).toString();
		conn.m_strUserEmail			= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USER_EMAIL).toString();
		conn.m_strAccountName		= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_ACC_NAME).toString();
		conn.m_strAccountPassword	= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_ACC_PASS).toString();
		return MailManager::SendMailDirectly(m_lstDataSimpleFUI,conn,m_lstAttachments,m_recToRecordSet,m_recCCRecordSet,m_recBCCRecordSet,Call_CreateEmailFromTemplate,g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_CC).toString(),g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_BCC).toString(),g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USE_AUTH).toBool(),m_nProjectID,false);
	
	//}

	//return bOK;
}




//send email to contact id's (defaults) 
void FUI_Email::PrepareMailToSendToContacts(DbRecordSet &recContacts,QString strDefaultEmailForFirstContact)
{
	//clear TO:
	ui.Address_tableWidget->ClearAll();
	m_recToRecordSet.clear();
	m_recCCRecordSet.clear();
	m_recBCCRecordSet.clear();

//	QMessageBox::information(NULL,"","1="+QVariant(0).toString());

	if(recContacts.getRowCount()==0) 
		return;

	DbRecordSet lstContactEmails;

	//check list:
	QString strCntID="BCNT_ID";
	if(recContacts.getColumnIdx("CELC_CONTACT_ID")>=0)
		strCntID="CELC_CONTACT_ID";
	else if (recContacts.getColumnIdx("BCNT_ID")==-1)
		return;

//	QMessageBox::information(NULL,"","2="+QVariant(0).toString());

	//read all emails (only defaults) from server:
	Status err;
	QString strWhere = " AND BCME_IS_DEFAULT=1";
	_SERVER_CALL(ClientSimpleORM->ReadFromParentIDs(err, BUS_CM_EMAIL, lstContactEmails, "BCME_CONTACT_ID",recContacts,strCntID,"",strWhere))
		_CHK_ERR(err);
	if (lstContactEmails.getRowCount()==0)
		return;

//	QMessageBox::information(NULL,"","lstContactEmails="+QVariant(lstContactEmails.getRowCount()).toString());
//	QMessageBox::information(NULL,"","3="+QVariant(0).toString());

	//define temp list
	DbRecordSet tmp;
	tmp.copyDefinition(lstContactEmails);
	int nContactIdx=lstContactEmails.getColumnIdx("BCME_CONTACT_ID");
	int nContactIdx_1=recContacts.getColumnIdx("BCNT_ID");
	if (nContactIdx_1==-1)
		nContactIdx_1=recContacts.getColumnIdx("CELC_CONTACT_ID");
	if (nContactIdx_1!=-1 && nContactIdx!=-1)
	{


		//First row is TO recipient.
		int nRowCount = recContacts.getRowCount();

		//QMessageBox::information(NULL,"","recContacts="+QVariant(nRowCount).toString());

		for (int i = 0; i < nRowCount; ++i)
		{
			//find contact mails, copy 2 tmp
			int nContactID = recContacts.getDataRef(i, nContactIdx_1).toInt();
			lstContactEmails.find(nContactIdx,nContactID);
			tmp.clear();
			tmp.merge(lstContactEmails,true); //get only selected //preferably only one

			//lstContactEmails.Dump();
			//tmp.Dump();

			if (tmp.getRowCount()>0)
			{
				//If first row and strEmail is already sent then add all you need.
				if (!i || m_bSerialEmailMode)
				{
					m_recToRecordSet.addRow();
					int nRow=m_recToRecordSet.getRowCount()-1;
					m_recToRecordSet.setData(nRow, "CONTACT_ID",	nContactID);
					m_recToRecordSet.setData(nRow, "EMAIL_ID",		tmp.getDataRef(0, "BCME_ID").toInt());
					m_recToRecordSet.setData(nRow, "CONTACT_NAME",	tmp.getDataRef(0, "BCME_NAME").toString());
					if (strDefaultEmailForFirstContact.isEmpty())
						m_recToRecordSet.setData(nRow, "CONTACT_EMAIL", tmp.getDataRef(0, "BCME_ADDRESS").toString());
					else
						m_recToRecordSet.setData(nRow, "CONTACT_EMAIL", strDefaultEmailForFirstContact);
				}
				//Else add to BCC.
				else
				{
					m_recBCCRecordSet.addRow();
					int nRow=m_recBCCRecordSet.getRowCount()-1;
					m_recBCCRecordSet.setData(nRow, "CONTACT_ID",	nContactID);
					m_recBCCRecordSet.setData(nRow, "EMAIL_ID",		tmp.getDataRef(0, "BCME_ID").toInt());
					m_recBCCRecordSet.setData(nRow, "CONTACT_NAME",	tmp.getDataRef(0, "BCME_NAME").toString());
					m_recBCCRecordSet.setData(nRow, "CONTACT_EMAIL",tmp.getDataRef(0, "BCME_ADDRESS").toString());
				}

			}
		}
	}


	//if sender is empty then set it to logged user...
	if (m_lstDataSimpleFUI.getDataRef(0,"BEM_FROM").toString().isEmpty())
	{
		SetPersonData();
	}
}

void FUI_Email::UpdateLeftButton()
{
	if (!m_pContinue) return;

	if (ui.stackedWidget->currentIndex()==0)
		m_pContinue->setText(tr("Continue"));
	else if (ui.stackedWidget->currentIndex()==2)
		m_pContinue->setText(tr("Back"));
	else
	{
		if(m_nMode==MODE_READ)
			m_pContinue->setText(tr("Close"));
		else
			m_pContinue->setText(tr("Send"));
	}

}


QList<QAction*>	FUI_Email::SetRightSoftKeyAction()
{
	QList<QAction*> lst;
	lst.append(m_pRecepients);
	lst.append(m_pBody);
	lst.append(m_pAttachment);
	lst+=FuiBase::SetRightSoftKeyAction(); //from cancel
	return lst;
}