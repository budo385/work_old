#include "selection_nm_sapne_fui.h"

#include "fuimanager.h"
extern FuiManager *g_FuiManager;


Selection_NM_SAPNE_FUI::Selection_NM_SAPNE_FUI(QWidget *parent)
	: Selection_NM_SAPNE(parent),m_pLastFUIOpen(NULL)
{

}

Selection_NM_SAPNE_FUI::~Selection_NM_SAPNE_FUI()
{

}

void Selection_NM_SAPNE_FUI::CreatePieButton()
{
	/*
	//create selection button:
	m_btnContactPie = new Selection_ContactButton_FUI(this);
	QSizePolicy pol(QSizePolicy::Fixed,QSizePolicy::Fixed);
	m_btnContactPie->setMaximumWidth(24);
	m_btnContactPie->setMinimumWidth(24);
	m_btnContactPie->setSizePolicy(pol);
	QBoxLayout *pHorButtonBar=dynamic_cast<QBoxLayout*>(dynamic_cast<QBoxLayout*>(this->layout())->itemAt(1)->layout());
	pHorButtonBar->insertWidget(4,m_btnContactPie);
	*/
}


//if click, open FUI, go insert, store Last FUI pointer, send signal 
void Selection_NM_SAPNE_FUI::on_btnInsert_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;

	/*
	//open FUI, get window pointer
	int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
	int nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_INSERT,-1,true);
	m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(m_nActualOrganizationID!=-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI
	m_bAutoAssigment=true;
	notifyObservers(SelectorSignals::SELECTOR_ON_AFTER_OPEN_INSERT_HANDLER,0);
	*/
}

//open FUI in new window, set new ID if exists
void Selection_NM_SAPNE_FUI::on_btnModify_clicked()
{
	/*
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;
	int nRow=m_lstData.getSelectedRow();
	int nRecordID =-1;
	if (nRow!=-1)
	{
		nRecordID=m_lstData.getDataRef(nRow,m_nFK_IDColIdx).toInt();
	}

	//open FUI, get window pointer
	int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
	int nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_READ,nRecordID,true);
	m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(m_nActualOrganizationID!=-1 && nRecordID==-1)
	{
	if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI
	notifyObservers(SelectorSignals::SELECTOR_ON_AFTER_OPEN_EDIT_HANDLER,0);
	*/
}


