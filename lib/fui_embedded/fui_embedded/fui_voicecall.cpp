#include "fui_voicecall.h"
#include "mobilehelper.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager* g_pSettings;
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/gui_helper.h"
#include <QHeaderView>
#include <QMessageBox>

FUI_VoiceCall::FUI_VoiceCall(QString strFuiName, QWidget *parent)
: FuiBase(strFuiName, parent)

{
	m_bCallInProgress=false;
	m_nTimerID=-1;
	ui.setupUi(this);

	//set layout:

	//TAPI PAGE:
	//------------------
	ui.labelTAPITitle->setStyleSheet("QLabel "+ThemeManager::GetMobileChapter_Font());
	ui.labelTAPITitle->setText(tr("Phone Lines"));
	ui.frameTAPITitle->setStyleSheet("QFrame#frameTAPITitle "+ThemeManager::GetSidebarChapter_Bkg());
	ui.stackedWidget->widget(0)->setStyleSheet(".QWidget "+ThemeManager::GetMobileBkg());

	
	ui.tableWidget->setColumnCount(1);
	ui.tableWidget->horizontalHeader()->hide();
	ui.tableWidget->verticalHeader()->hide();
	ui.tableWidget->horizontalHeader()->stretchLastSection();
	ui.tableWidget->verticalHeader()->setDefaultSectionSize(20);
	ui.tableWidget->setStyleSheet(ThemeManager::GetTableViewBkg());
	ui.tableWidget->setColumnWidth(0,300);
	ui.tableWidget->setShowGrid(false);

	QFont strStyle2("Arial",8,QFont::DemiBold);
	strStyle2.setStyleStrategy(QFont::PreferAntialias);

	//get preffered device
	QString strSelDevice = g_pSettings->GetPersonSetting(COMM_SETTINGS_DEFAULT_TAPI_DEV).toString();
	int nSelIdx = -1;

	int nCnt = m_lineTAPI.GetDeviceCount(); 
	int k=0;
	ui.tableWidget->setRowCount(nCnt);
	for(int i=0; i<nCnt; i++)
	{
		QString strDeviceName;
		bool bVoiceCalls = false;
		if(m_lineTAPI.GetDeviceInfo(i, strDeviceName, bVoiceCalls))
		{
			if (bVoiceCalls && !strDeviceName.isEmpty())
			{
				QTableWidgetItem *newItem=ui.tableWidget->item(i,0);
				if(newItem==NULL)	//create new if not
				{
					newItem = new QTableWidgetItem;
					newItem->setFlags(  Qt::ItemIsSelectable | Qt::ItemIsEnabled);
					newItem->setFont(strStyle2);
					ui.tableWidget->setItem(k,0,newItem);
				}
				newItem->setText(strDeviceName);

				if(strSelDevice == strDeviceName){
					nSelIdx = i;
				}
				k++;
			}
		}
	}
	ui.tableWidget->setRowCount(k);

	ui.ckbTapi->setChecked(false);

	m_nTapiDeviceID=g_pSettings->GetPersonSetting(MOBILE_LAST_TAPI_DEVICE_ID).toInt();
	if(nSelIdx >= 0)
	{
		m_nTapiDeviceID = nSelIdx;
		g_pSettings->SetPersonSetting(MOBILE_LAST_TAPI_DEVICE_ID, m_nTapiDeviceID);
	}

	if (m_nTapiDeviceID<0 || m_nTapiDeviceID>=k)
	{
		ui.stackedWidget->setCurrentIndex(0);
		ui.tableWidget->setCurrentCell (0, 0, QItemSelectionModel::Select);
	}
	else
	{
		ui.stackedWidget->setCurrentIndex(1);
		ui.tableWidget->setCurrentCell (m_nTapiDeviceID, 0, QItemSelectionModel::Select);
	}

	connect(ui.tableWidget,SIGNAL(  itemClicked ( QTableWidgetItem *)),this,SLOT(tableWidget_itemClicked ( QTableWidgetItem *)));


	//CALL PAGE:
	//------------------
	MobileHelper::SelectedContactImageFrame_Create(ui.frameBkg, &m_pPic);
	
	GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_GREEN,"white");
	GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Call Now!"), ":accept.png");

	ui.labelPhone->setStyleSheet("QLabel "+ThemeManager::GetMobileChapter_Font());
	ui.frameButton->setStyleSheet("QFrame#frameButton "+ThemeManager::GetMobileSolidBkg_Dark());

	setStyleSheet(ThemeManager::GetGlobalWidgetStyle()+" * "+ThemeManager::GetMobileControls_Font());
}

FUI_VoiceCall::~FUI_VoiceCall()
{
	ClearCall();

	if (ui.ckbTapi->isChecked())
	{
		m_nTapiDeviceID=ui.tableWidget->currentRow();
		g_pSettings->SetPersonSetting(MOBILE_LAST_TAPI_DEVICE_ID,m_nTapiDeviceID);
	}
}


void FUI_VoiceCall::Initialize(DbRecordSet rowContact,QString strPhone)
{
	m_strPhone=strPhone;
	m_RowContactData=rowContact;
	ui.frameName->RefreshActualName(m_RowContactData);
	
	MobileHelper::SelectedContactImageFrame_SetContactPicture(m_pPic,rowContact);
	
	ui.labelPhone->setText(strPhone);
}

void FUI_VoiceCall::on_btnCallAction1_clicked()
{
	qDebug() << "FUI_VoiceCall::on_btnCallAction1_clicked";

	if (m_bCallInProgress)
	{
		qDebug() << "end call";

		ClearCall();
	}
	else
	{
		qDebug() << "start call";

		//QMessageBox::information(NULL,"","opening");
		if(0 != m_lineTAPI.Open(m_nTapiDeviceID))
		{
			//QMessageBox::information(NULL, "", tr("Could not open requested TAPI device: ")+GetTapiDeviceName(m_nTapiDeviceID));
			OnError();
			return;
		}
		//QMessageBox::information(NULL,"","opening ok, calling");
		QString strNumToCall = m_strPhone;
		if(g_pSettings->GetPersonSetting(COMM_SETTINGS_USE_DIAL_PREFIX).toBool()){
			strNumToCall = g_pSettings->GetPersonSetting(COMM_SETTINGS_DIAL_PREFIX_STR).toString() + m_strPhone;
		}
		int nRet = m_lineTAPI.MakeOutgoingCall(strNumToCall.toLocal8Bit().constData());
		if(0 == nRet)
		{
			//QMessageBox::information(NULL,"","calling ok, calling");

			GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_BROWN,"white");
			GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Hangup"), ":hangup.png");
			m_nTimerID=startTimer(1000);
			m_bCallInProgress=true;
		}
		else
		{
			OnError();
		}
	}

}
QString FUI_VoiceCall::GetTapiDeviceName(int nTapiDevID)
{
	QTableWidgetItem *item=ui.tableWidget->item(nTapiDevID,0);
	if (item)
	{
		return item->text();
	}
	else
		return "";

}


void FUI_VoiceCall::OnTAPICheckTimer()
{
	int nResult = m_lineTAPI.GetThreadResult();
	if(nResult != 0)
	{
		char *szErrTxt = NULL;
		m_lineTAPI.GetErrorString(nResult, szErrTxt);
		if(0 == strcmp("LINEERR_OPERATIONUNAVAIL", szErrTxt))
			QMessageBox::information(NULL, "", tr("Phone Line Occupied!"));
		else if(0 == strcmp("LINEERR_OPERATIONFAILED", szErrTxt))
			QMessageBox::information(NULL, "", tr("Phone Operation Failed!"));
		else
			QMessageBox::information(NULL, "", tr("TAPI Error: %1!").arg(szErrTxt));
		free(szErrTxt);
		ClearCall();
		OnError();
		return;
	}

	//terminated?
	if( m_bCallInProgress && 
		!m_lineTAPI.IsConnecting() &&
		!m_lineTAPI.IsConnected())
	{
		ClearCall();
	}
}



void FUI_VoiceCall::timerEvent ( QTimerEvent * event )
{
	//find message:
	int nTimerID=event->timerId();
	if (nTimerID==m_nTimerID)
	{
		OnTAPICheckTimer();
	}

}

void FUI_VoiceCall::ClearCall()
{
	if (m_nTimerID!=-1)
		killTimer(m_nTimerID);

	m_lineTAPI.Close();
	GUI_Helper::SetStyledButtonColorBkg(ui.btnCallAction1, GUI_Helper::BTN_COLOR_GREEN,"white");
	GUI_Helper::SetButtonTextIcon(ui.btnCallAction1, tr("Call Now!"), ":accept.png");	
	m_nTimerID=-1;
	m_bCallInProgress=false;
}


void FUI_VoiceCall::OnError()
{
	//asked: change tapi device or exit...
	int nResult=QMessageBox::question(NULL,tr("Error"),tr("Error occurred while trying to use TAPI device: ")+GetTapiDeviceName(m_nTapiDeviceID)+tr(" Do you want to select another TAPI device or to exit?"),tr("Select another TAPI device"),tr("Exit"));
	if (nResult==0)
	{
		ui.stackedWidget->setCurrentIndex(0);
	}
	else
	{
		close();  //exit fui
	} 
}

void FUI_VoiceCall::tableWidget_itemClicked ( QTableWidgetItem * item)
{
	m_nTapiDeviceID=ui.tableWidget->row(item);
	ui.stackedWidget->setCurrentIndex(1);
}
