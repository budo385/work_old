#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include <QWidget>
#include "ui_splashscreen.h"

#include "styledpushbutton.h"

class SplashScreen : public QWidget
{
	Q_OBJECT

public:
	SplashScreen(QWidget *parent = 0);
	~SplashScreen();

private:
	Ui::SplashScreenClass ui;

	StyledPushButton *m_pBtnComm ;
	StyledPushButton *m_pBtnContact;
	StyledPushButton *m_pBtnProject;
};

#endif // SPLASHSCREEN_H
