#ifndef FUI_FAVORITES_H
#define FUI_FAVORITES_H

#include "fuibase.h"
#include "ui_fui_favorites.h"

//#include "pictureflow.h"
#include "pictureflowwidget.h"

class Fui_Favorites : public FuiBase
{
	Q_OBJECT

public:
	Fui_Favorites(QString strFuiName, QWidget *parent = 0);
	~Fui_Favorites();

	QAction*		SetLeftSoftKeyAction(); 
	QList<QAction*>	SetRightSoftKeyAction();
	void			OnOfflineModeChanged(bool bOffline=true);

private:
	Ui::Fui_FavoritesClass ui;

	void		ReloadData(int nDefaultFavIndex=-1, bool bRealodFromServer=false);
	DbRecordSet m_recContactData;
	PictureFlowWidget *m_pPictureFlow;
	int			m_nCurrentCenterIndex;
	QAction		*m_pSoftKeyAction;
	QAction		*m_pAddFavorite;
	QAction		*m_pRemoveFavorite;
	QAction		*m_pAddPicToFav;


private slots:
	void on_centerIndexChanged(int index);
	void OncenterIndexSelectedForEditing(int index);
	void on_LeftSoftKey_triggered();
	void OnAddFav();
	void OnRemoveFav();
	void OnAddPicToFav();
};

#endif // FUI_FAVORITES_H
