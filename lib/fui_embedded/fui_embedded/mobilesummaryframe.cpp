#include "mobilesummaryframe.h"
#include "thememanager.h"
#include "styledpushbutton.h"

#include <QHBoxLayout>

MobileSummaryFrame::MobileSummaryFrame(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);

	//Set background style sheet.
	QString style = ThemeManager::GetMobileStatusBarBackground();
	ui.summary_frame->setStyleSheet(style);

	//Layout
	QHBoxLayout *hLayout = new QHBoxLayout;
	StyledPushButton *missedCalls= new StyledPushButton(this, ":Missed_Calls.png", ":Missed_Calls.png", "");
	StyledPushButton *voiceMails = new StyledPushButton(this, ":Voicemail.png", ":Voicemail.png", "");
	StyledPushButton *tasks		 = new StyledPushButton(this, ":Tasks.png", ":Tasks.png", "");
	StyledPushButton *emails	 = new StyledPushButton(this, ":Email.png", ":Email.png", "");
	StyledPushButton *sms		 = new StyledPushButton(this, ":SMS.png", ":SMS.png", "");
	StyledPushButton *chat		 = new StyledPushButton(this, ":Chat.png", ":Chat.png", "");
	
	missedCalls->setMaximumHeight(20);
	missedCalls->setMinimumHeight(20);
	voiceMails->setMaximumHeight(20);
	missedCalls->setMinimumHeight(20);
	tasks->setMaximumHeight(20);
	tasks->setMinimumHeight(20);
	emails->setMaximumHeight(20);
	emails->setMinimumHeight(20);
	sms->setMaximumHeight(20);
	sms->setMinimumHeight(20);
	chat->setMaximumHeight(20);
	chat->setMinimumHeight(20);
	
	hLayout->setContentsMargins(0,0,0,0);
	hLayout->setSpacing(0);
	hLayout->addSpacing(5);
	//hLayout->addStretch(1);
	hLayout->addWidget(missedCalls);
	hLayout->addStretch(1);
	//hLayout->addSpacing(15);
	hLayout->addWidget(voiceMails);
	hLayout->addStretch(1);
//	hLayout->addSpacing(15);
	hLayout->addWidget(tasks);
	hLayout->addStretch(2);
	//hLayout->addSpacing(25);
	hLayout->addWidget(emails);
	hLayout->addStretch(1);
	//hLayout->addSpacing(15);
	hLayout->addWidget(sms);
	hLayout->addStretch(1);
	//hLayout->addSpacing(15);
	hLayout->addWidget(chat);
	//hLayout->addSpacing(15);
	hLayout->addStretch(2);

	ui.summary_frame->setLayout(hLayout);
}

MobileSummaryFrame::~MobileSummaryFrame()
{

}
