#ifndef FUI_SELECTEDFAVORITE_H
#define FUI_SELECTEDFAVORITE_H

#include "fuibase.h"
#include "ui_fui_selectedfavorite.h"

#include "selectedcontactpicture.h"
#include "gui_core/gui_core/styledpushbutton.h"

class Fui_SelectedFavorite : public FuiBase
{
	Q_OBJECT

public:
	Fui_SelectedFavorite(QString strFuiName, QWidget *parent = 0);
	~Fui_SelectedFavorite();

	void on_selectionChange(int nEntityRecordID);
	void OnOfflineModeChanged(bool bOffline=true);

private:
	Ui::Fui_SelectedFavoriteClass ui;

	StyledPushButton *m_pBtnPhone;
	StyledPushButton *m_pBtnSendEmail;
	StyledPushButton *m_pBtnSMS;
	StyledPushButton *m_pBtnChat;
	StyledPushButton *m_pBtnContactDetails;
	StyledPushButton *m_pBtnContactDocuments;
	StyledPushButton *m_pBtnContactInternetAddresses;
	StyledPushButton *m_pBtnContactList;
	StyledPushButton *m_pBtnContactProjects;

	DbRecordSet m_recContactData;
	SelectedContactPicture	*m_pPic;

private slots:
	void On_PhoneButton_clicked();
	void On_SendEmail_clicked();
	void On_SMS_clicked();
	void On_Chat_clicked();
	void On_Details_clicked();
	void On_Documents_clicked();
	void On_InternetAddresses_clicked();
	void On_ContactList_clicked();
	void On_ProjectList_clicked();
};

#endif // FUI_SELECTEDFAVORITE_H
