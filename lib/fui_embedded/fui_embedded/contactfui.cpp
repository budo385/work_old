#include "contactfui.h"
#include "gui_core/gui_core/thememanager.h"
#include "fuimanager.h"
extern FuiManager *g_FuiManager;
#include <QMenu>
ContactFui::ContactFui(QString strFuiName, QWidget *parent)
	: FuiBase(strFuiName, parent)
{
	ui.setupUi(this);

	ui.frameContact->DisableGroupSelector();
	ui.frameContact->Initialize(false,true);
	ui.frameContact->registerObserver(this);
	m_pOpen=NULL;

	setStyleSheet(ThemeManager::GetGlobalWidgetStyle());

	//ui.frameContact->setContextMenuPolicy(Qt::CustomContextMenu);
	//connect(ui.frameContact,SIGNAL(customContextMenuRequested ( const QPoint &)),this,SLOT(OnCnxtMenu( const QPoint &)));
}

ContactFui::~ContactFui()
{

}

void ContactFui::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail/*=0*/,const QVariant val/*=QVariant()*/)
{
	if (pSubject==ui.frameContact && nMsgCode==SelectorSignals::SELECTOR_ON_DOUBLE_CLICK)
	{
		g_FuiManager->OpenFUI(SELECTED_FAVORITE, FuiBase::MODE_READ, nMsgDetail);
	}

}



QAction* ContactFui::SetLeftSoftKeyAction()
{
	if (!m_pOpen)
	{
		m_pOpen = new QAction(tr("Open"),this);
		connect(m_pOpen, SIGNAL(triggered()), this, SLOT(OnOpen()));
	}

	return m_pOpen;
}

void ContactFui::OnOpen()
{
	int nContactID;
	DbRecordSet lstContact;
	ui.frameContact->GetSelection(lstContact);
	if (lstContact.getRowCount()>0)
	{
		nContactID=lstContact.getDataRef(0,"BCNT_ID").toInt();
		g_FuiManager->OpenFUI(SELECTED_FAVORITE, FuiBase::MODE_READ, nContactID);
	}


}

void ContactFui::OnCnxtMenu( const QPoint &pos)
{
	QMessageBox::information(NULL,"","1");
	QList<QAction*> m_lstActions=ui.frameContact->GetContactTableWidget()->GetContextMenuActions();
	//event:
	if(m_lstActions.size()==0) 
	{
		return;
	}

	QMenu CnxtMenu(this);

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));
	QMessageBox::information(NULL,"","2");
	CnxtMenu.exec(ui.frameContact->mapToGlobal(pos));
}


void ContactFui::contextMenuEvent(QContextMenuEvent * event)
{
	/*
	QMenu *menu = new QMenu;
	QAction *act = new QAction("akcija", this);
	menu->addAction(act);
	menu->popup(event->globalPos());
	*/
}

void ContactFui::OnOfflineModeChanged(bool bOffline)
{
	ui.frameContact->OnOfflineModeChanged();
}