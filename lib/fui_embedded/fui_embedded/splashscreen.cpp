#include "splashscreen.h"

#include "thememanager.h"

SplashScreen::SplashScreen(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	//Tool bar buttons.
	QString	styleNew=ThemeManager::GetToolbarButtonStyle();
	m_pBtnComm	  = new StyledPushButton(this,":Desktop_G.png",":Desktop_C.png",styleNew);
	m_pBtnContact = new StyledPushButton(this,":Contacts_G.png",":Contacts_C.png",styleNew);
	m_pBtnProject = new StyledPushButton(this,":Projects_G.png",":Projects_C.png",styleNew);

	//MB always lite on on start:
	m_pBtnComm->setIcon(":Desktop_C.png");
	m_pBtnContact->setIcon(":Contacts_C.png");
	m_pBtnProject->setIcon(":Projects_C.png");

	//Tool bar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnComm);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnContact);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnProject);
	buttonLayout->addStretch(1);

	//Label.
	QLabel *start_splashLabel = new QLabel;
	QLabel *sokrates_splashLabel = new QLabel;
	QLabel *wait_splashLabel = new QLabel;
	start_splashLabel->setAlignment(Qt::AlignCenter);
	sokrates_splashLabel->setAlignment(Qt::AlignCenter);
	wait_splashLabel->setAlignment(Qt::AlignCenter);
	start_splashLabel ->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:14pt; color:black; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Starting</p></body></html>"));
	sokrates_splashLabel->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:14pt; color:black; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">SOKRATES<sup>�</sup> Connect...</p></body></html>"));
	wait_splashLabel->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:14pt; color:black; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Please wait!</p></body></html>"));

	//Vertical layout.
	QVBoxLayout *vertLayout = new QVBoxLayout;
	vertLayout->setAlignment(Qt::AlignCenter);
	vertLayout->addSpacing(80);
	vertLayout->addLayout(buttonLayout);
	vertLayout->addSpacing(80);
	vertLayout->addWidget(start_splashLabel);
	vertLayout->addWidget(sokrates_splashLabel);
	vertLayout->addWidget(wait_splashLabel);
	vertLayout->addStretch(1);
	vertLayout->addSpacing(27);

	ui.frameToolBar->setStyleSheet(ThemeManager::GetToolbarBkgStyle());
	ui.frameToolBar->setLayout(vertLayout);
}

SplashScreen::~SplashScreen()
{

}
