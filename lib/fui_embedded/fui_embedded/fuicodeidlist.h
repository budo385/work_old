#ifndef FUICODEIDLIST_H
#define FUICODEIDLIST_H

#define CONTACT_LIST				1
#define CONTACT_DETAILS				2
#define CONTACT_GRID				3
#define PROJECT_LIST				4
#define PIC_FLOW_MENU				5
#define EMAIL_SEND					6
#define FAVORITES_FUI				7
#define EMAIL_TEMPLATES				8
#define VOICE_CALL					9
#define SELECTED_FAVORITE			10

#endif // FUICODEIDLIST_H
