#include "commworkbenchwidget.h"

#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/commfiltersettingsids.h"
#include "bus_core/bus_core/commgridfiltersettings.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "bus_client/bus_client/documenthelper.h"

#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager* g_pSettings;

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

#include "thememanager.h"
#include "fuicodeidlist.h"
//#include "fui_email.h"
#include "fuimanager.h"
extern FuiManager *g_FuiManager;

#include "bus_client/bus_client/clientcontactmanager.h"

CommWorkBenchWidget::CommWorkBenchWidget(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	//Set grid type.
	m_nGridType = 1;

	//Set style sheets.
	QString buttons_frame_style("QFrame#buttons_frame ");
	QString filter_frame_style("QFrame#filter_frame ");
	buttons_frame_style += ThemeManager::GetSidebarChapter_Bkg();
	filter_frame_style += ThemeManager::GetSidebarChapter_Bkg();
	ui.buttons_frame->setStyleSheet(buttons_frame_style);
	ui.filter_frame->setStyleSheet(filter_frame_style);

	//Init values.
	m_pParentFUI = NULL;
	m_nCurrentViewID = -1;
	m_nLoggedPersonID = g_pClientManager->GetPersonID();


	//Buttons look and feel.
	//Auto raise buttons.
	ui.pieMenu_toolButton->setAutoRaise(true);
	ui.openDoc_toolButton->setAutoRaise(true);
	ui.startTask_toolButton->setAutoRaise(true);
	ui.taskDone_toolButton->setAutoRaise(true);
	ui.select_toolButton->setAutoRaise(true);
	ui.modify_toolButton->setAutoRaise(true);
	ui.clear_toolButton->setAutoRaise(true);
	ui.checkIn_toolButton->setAutoRaise(true);
	ui.Reply_toolButton->setAutoRaise(true);
	ui.Forward_toolButton->setAutoRaise(true);
	ui.PackAndSend_toolButton->setAutoRaise(true);
	ui.showFilter_toolButton->setAutoRaise(true);
	ui.hideFilter_toolButton->setAutoRaise(true);
	ui.refresh_toolButton->setAutoRaise(true);

	//Set buttons and icon sizes.
	QSize IconSize;
	IconSize = QSize(16,16);

	ui.pieMenu_toolButton->setMinimumSize(IconSize);
	ui.openDoc_toolButton->setMinimumSize(IconSize);
	ui.startTask_toolButton->setMinimumSize(IconSize);
	ui.taskDone_toolButton->setMinimumSize(IconSize);
	ui.select_toolButton->setMinimumSize(IconSize);
	ui.modify_toolButton->setMinimumSize(IconSize);
	ui.clear_toolButton->setMinimumSize(IconSize);
	ui.checkIn_toolButton->setMinimumSize(IconSize);
	ui.Reply_toolButton->setMinimumSize(IconSize);
	ui.Forward_toolButton->setMinimumSize(IconSize);
	ui.PackAndSend_toolButton->setMinimumSize(IconSize);
	ui.showFilter_toolButton->setMinimumSize(IconSize);

	ui.pieMenu_toolButton->setIconSize(IconSize);
	ui.select_toolButton->setIconSize(IconSize);
	ui.modify_toolButton->setIconSize(IconSize);
	ui.clear_toolButton->setIconSize(IconSize);
	ui.checkIn_toolButton->setIconSize(IconSize);
	ui.Reply_toolButton->setIconSize(IconSize);
	ui.Forward_toolButton->setIconSize(IconSize);
	ui.PackAndSend_toolButton->setIconSize(IconSize);
	ui.showFilter_toolButton->setIconSize(IconSize);

	ui.refresh_toolButton->setMinimumSize(QSize(24,24));
	ui.refresh_toolButton->setMaximumSize(QSize(24,24));
	ui.refresh_toolButton->setIconSize(QSize(24,24));
	ui.refresh_toolButton->setText("");
	ui.refresh_toolButton->setIcon(QIcon(":reload24.png"));
	ui.refresh_toolButton->setToolTip(tr("Refresh"));

	//Set button icons.
	ui.pieMenu_toolButton->setIcon(QIcon(":PieMenu16.png"));
	//Open doc. tool button.
	ui.openDoc_toolButton->setIconSize(IconSize);
	ui.openDoc_toolButton->setIcon(QIcon(":OpenDoc16.png"));
	ui.openDoc_toolButton->setToolTip(tr("Open Document"));
	//Start task.
	ui.startTask_toolButton->setIcon(QIcon(":StartTask16.png"));
	ui.startTask_toolButton->setIconSize(IconSize);
	ui.startTask_toolButton->setToolTip(tr("Start Task"));
	//Done task.
	ui.taskDone_toolButton->setIcon(QIcon(":TaskDone16.png"));
	ui.taskDone_toolButton->setIconSize(IconSize);
	ui.taskDone_toolButton->setToolTip(tr("Task Done / Email Read"));

	ui.select_toolButton->setIcon(QIcon(":View16.png"));
	ui.select_toolButton->setToolTip(tr("View"));
	ui.modify_toolButton->setIcon(QIcon(":Edit16.png"));
	ui.modify_toolButton->setToolTip(tr("Edit"));
	ui.clear_toolButton->setIcon(QIcon(":Delete16.png"));
	ui.clear_toolButton->setToolTip(tr("Delete"));

	ui.checkIn_toolButton->setIcon(QIcon(":Check_In_16.png"));
	ui.checkIn_toolButton->setToolTip(tr("Check In"));
	ui.Reply_toolButton->setIcon(QIcon(":Email_Reply_16.png"));
	ui.Reply_toolButton->setToolTip(tr("Reply"));
	ui.Forward_toolButton->setIcon(QIcon(":Email_Forward_16.png"));
	ui.Forward_toolButton->setToolTip(tr("Forward"));
	ui.PackAndSend_toolButton->setIcon(QIcon(":Email_Attach_16.png"));
	ui.PackAndSend_toolButton->setToolTip(tr("Pack and Send"));

	ui.showFilter_toolButton->setIconSize(QSize(24,24));
	ui.showFilter_toolButton->setMinimumSize(QSize(24,24));
	ui.showFilter_toolButton->setIcon(QIcon(":Eye.png"));
	ui.showFilter_toolButton->setToolTip(tr("Show Filter Frame"));

	ui.hideFilter_toolButton->setIconSize(QSize(24,24));
	ui.hideFilter_toolButton->setMinimumSize(QSize(24,24));
	ui.hideFilter_toolButton->setIcon(QIcon(":Eye_OK.png"));
	ui.hideFilter_toolButton->setToolTip(tr("Hide Filter Frame"));

	//Filter selector.
	ui.viewSelect_toolButton->setAutoRaise(true);
	ui.viewSelect_toolButton->setIconSize(QSize(32,32));
	ui.viewSelect_toolButton->setIcon(QIcon(":View24.png"));
	ui.viewSelect_toolButton->setToolTip(tr("View"));

	//Hide filter frame.
	ui.filter_frame->setVisible(false);

	//Filter views recordset.
	m_recFilterViews.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW));
	m_recFilterViewData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));

	//Setup sorting recordset.
	SetupSortRecordSet();

	//Connect stuff.
	connect(ui.CommGrid_widget, SIGNAL(SectionClicked(int, int, int, int, int, QList<int>, int, int, int, int, int, QList<int>, QList<int>, QList<int>)), this, SLOT(on_SelectionChanged(int, int, int, int, int, QList<int>, int, int, int, int, int, QList<int>, QList<int>, QList<int>)));
	connect(ui.CommGrid_widget, SIGNAL(SectionDoubleClicked()), this, SLOT(on_openDoc_toolButton_clicked()));
	//Date range selector.
	connect(ui.dateFilter_frame,SIGNAL(onDateChanged(QDateTime&, QDateTime&, int)),this,SLOT(OnDateChanged(QDateTime&, QDateTime&, int)));

	setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

CommWorkBenchWidget::~CommWorkBenchWidget()
{

}

void CommWorkBenchWidget::Initialize(int nGridType, QWidget *pParentFUI)
{
	m_pParentFUI = 	pParentFUI;

	//First load views to see if there is saved view.
	if (!ReloadViewSelector(-1))
		return;
	int nSavedDefaultView = g_pSettings->GetPersonSetting(CONTACT_DEF_FILTER_VIEW).toInt();
	//If there is not that view than set default view to -1.
	if (ui.viewSelect_comboBox->findData(nSavedDefaultView) < 0)
		g_pSettings->SetPersonSetting(CONTACT_DEF_FILTER_VIEW, -1);
	else	
		m_nCurrentViewID = nSavedDefaultView;
	//If no view loaded then fill default values to filter (show all).
	if(m_nCurrentViewID < 0)
		FillInsertDefaultValues(m_recFilterViewData);

	if (!ReloadViewSelector(m_nCurrentViewID))
		return;
}

void CommWorkBenchWidget::SetData(int nEntityRecordID)
{
	m_RowContactData.clear(); //always clear

	/*
	//Just for server call to work
	DbRecordSet lstNmrx1,lstNmrx2,lstPicture;
	DbRecordSet lstNmrx1Filter; //=ui.widgetCommPerson->GetFilterForRead(m_nEntityRecordID);
	DbRecordSet lstNmrx2Filter; //=ui.widgetCommProject->GetFilterForRead(m_nEntityRecordID);
	
	Status err;
	_SERVER_CALL(BusContact->ReadContactDataSideBar(err,nEntityRecordID,m_RowContactData,lstNmrx1Filter,lstNmrx1,lstNmrx2Filter,lstNmrx2,false,lstPicture))
		_CHK_ERR(err);
	*/
	
	if (nEntityRecordID>0)
	{
		//read from proxy-> either cache or server
		Status err;
		ClientContactManager::GetContactData(err,nEntityRecordID,m_RowContactData,!g_pClientManager->IsLogged());
		if (!err.IsOK())return;
	}

	//Set contact grid.
	ContactChanged(nEntityRecordID);
	
	ui.actualContact_frame->RefreshActualName(m_RowContactData);
}

void CommWorkBenchWidget::ContactChanged(int nEntityRecordID)
{
	m_nCurrentEntityID = nEntityRecordID;

	//_DUMP(m_recFilterViewData);

	//Get data.
	Status status;
	m_recEmail.clear();
	m_recVoiceCall.clear();
	m_recDocument.clear();

	_SERVER_CALL(BusCommunication->GetContactCommData(status, m_recEmail, m_recDocument, m_recVoiceCall, nEntityRecordID, m_recFilterViewData))
		_CHK_ERR(status);

	//_DUMP(m_recEmail);
	//_DUMP(m_recVoiceCall);
	//_DUMP(m_recDocument);

	//Initialize grid.
	ui.CommGrid_widget->Initialize(&m_recEmail, &m_recDocument, &m_recVoiceCall, 1 /*CONTACT_GRID*/);

	emit ContentChanged();
}

void CommWorkBenchWidget::on_SelectionChanged(int nOwnerID, int nContactID, int nProjectID, int nEntitySysType, int nTaskID, QList<int> lstTaskList, int nEntityID, int nPolymorficID, int nBDMD_IS_CHECK_OUT, int nBDMD_CHECK_OUT_USER_ID, int nBTKS_SYSTEM_STATUS, QList<int> lstUnreadEmailsBEM_IDs, QList<int> lstStartTaskEmailsCENT_IDs, QList<int> lstStartTaskPhoneCallsCENT_IDs)
{
	//store all selection data:
	m_nTaskID = nTaskID;
	m_nOwnerID=nOwnerID;
	m_nContactID=nContactID; 
	m_nProjectID=nProjectID;
	m_nEntityID = nEntityID;
	m_nEntitySysType=nEntitySysType;
	m_nPolymorficID=nPolymorficID;
	m_lstTaskList.clear();
	m_lstUnreadEmailsList.clear();
	m_lstTaskList=lstTaskList;
	m_lstUnreadEmailsList = lstUnreadEmailsBEM_IDs;
	m_lstStartTaskEmailsCENT_IDs = lstStartTaskEmailsCENT_IDs;
	m_lstStartTaskPhoneCallsCENT_IDs = lstStartTaskPhoneCallsCENT_IDs;

	bool bStartTask = false;
	if ((lstStartTaskEmailsCENT_IDs.count() + lstStartTaskPhoneCallsCENT_IDs.count()) > 0)
		bStartTask = true;
}

void CommWorkBenchWidget::on_openDoc_toolButton_clicked()
{
	//open doc in app:
	DbRecordSet *pLstData = GetDocumentsRecordSet();

	int nRow=pLstData->getSelectedRow();
	if (nRow!=-1)
	{
		int nCount=pLstData->getSelectedCount();
		if (nCount>1)
		{
			int nResult=QMessageBox::question(this,tr("Warning"),QVariant(nCount).toString()+tr(" items selected. Do you want to open them all?"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
		}

		DbRecordSet lstData=pLstData->getSelectedRecordSet();
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			DbRecordSet rowDoc=lstData.getRow(i);
			DocumentHelper::OpenDocumentInExternalApp(rowDoc);
		}
	}

/*
	DbRecordSet *pLstData=GetVoiceCallRecordSet();
	int nRow=pLstData->getSelectedRow();
	if (nRow!=-1)
	{
		int nCount=pLstData->getSelectedCount();
		if (nCount>1)
		{
			int nResult=QMessageBox::question(this,tr("Warning"),QVariant(nCount).toString()+tr(" items selected. Do you want to open them all?"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
		}

		DbRecordSet lstData=pLstData->getSelectedRecordSet();
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			//open one by one:
			int nFUI = g_objFuiManager.OpenFUI(MENU_COMM_VOICE_CENTER, true, false,FuiBase::MODE_EMPTY,-1,true);
			FUI_VoiceCallCenter* pFUI = dynamic_cast<FUI_VoiceCallCenter *>(g_objFuiManager.GetFUIWidget(nFUI));
			if(NULL == pFUI) return;	//someone closed the FUI for this call
			pFUI->LoadCallData(lstData.getDataRef(i,"BVC_ID").toInt());
			if(pFUI)pFUI->show();
		}
	}


	//open doc in app:
	pLstData=GetDocumentsRecordSet();

	nRow=pLstData->getSelectedRow();
	if (nRow!=-1)
	{
		int nCount=pLstData->getSelectedCount();
		if (nCount>1)
		{
			int nResult=QMessageBox::question(this,tr("Warning"),QVariant(nCount).toString()+tr(" items selected. Do you want to open them all?"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
		}

		DbRecordSet lstData=pLstData->getSelectedRecordSet();
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			DbRecordSet rowDoc=lstData.getRow(i);
			g_CommManager.OpenDocumentInExternalApp(rowDoc);
		}
	}

*/
	//open email in app:
	pLstData=GetEmailRecordSet();

	nRow=pLstData->getSelectedRow();
	if (nRow!=-1)
	{
		int nCount=pLstData->getSelectedCount();
		if (nCount>1)
		{
			int nResult=QMessageBox::question(this,tr("Warning"),QVariant(nCount).toString()+tr(" items selected. Do you want to open them all?"),tr("Yes"),tr("No"));
			if (nResult!=0) return; //only if YES
		}



		bool bReloadGrid=false;
		DbRecordSet lstData=pLstData->getSelectedRecordSet();
		int nSize=lstData.getRowCount();
		QList<int> lstSelectedCENT_IDs;
		//_DUMP(lstData);
		for(int i=0;i<nSize;++i)
		{
			//QByteArray byteExternID=lstData.getDataRef(i,"BEM_EXT_ID").toByteArray();
			//QString strExtApp=lstData.getDataRef(i,"BEM_EXT_APP").toString();
			int nEmailID=lstData.getDataRef(i,"BEM_ID").toInt();
			g_FuiManager->OpenFUI(EMAIL_SEND,FuiBase::MODE_READ,nEmailID);

			//g_CommManager.OpenMailInExternalClient(nEmailID,byteExternID,strExtApp);
			//int nCENT_ID = lstData.getDataRef(0,"CENT_ID").toInt();

			/*
			lstSelectedCENT_IDs << nCENT_ID;

			//mark as READ (issue: 540):
			//unread flag is set, then set:
			if (lstData.getDataRef(i,"BEM_UNREAD_FLAG").toInt()>0)
			{
				Status err;
				_SERVER_CALL(BusEmail->SetEmailRead(err, nCENT_ID))
					if (err.IsOK())
					{
						int nRow = pLstData->find("CENT_ID", nCENT_ID, true);
						pLstData->setData(nRow, "BEM_UNREAD_FLAG", 0);

						//UpdateRecordsetsWithChangedData(ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED, ENTITY_BUS_EMAILS, pLstData);

						ui.CommGrid_widget->Initialize(GetEmailRecordSet(), GetDocumentsRecordSet(), GetVoiceCallRecordSet(),m_nGridType);
					}
			}
			*/
		}

		//Select previous selected rows.
		ui.CommGrid_widget->SelectBySourceRecordSetRows(lstSelectedCENT_IDs);
	}

}

void CommWorkBenchWidget::on_dateFilter_checkBox_toggled(bool bChecked)
{
	//First check that some filter is already selected.
	if (!m_recFilterViewData.getRowCount())
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Please select a filter first!"));
		ui.dateFilter_checkBox->blockSignals(true);
		if (!bChecked)
		{
			ui.dateFilter_checkBox->setCheckState(Qt::Checked);
			ui.dateFilter_frame->setDisabled(false);
		}
		else
		{
			ui.dateFilter_checkBox->setCheckState(Qt::Unchecked);
			ui.dateFilter_frame->setDisabled(true);
		}
		ui.dateFilter_checkBox->blockSignals(false);
		return;
	}

	if (!bChecked)
	{
		ui.dateFilter_frame->setDisabled(true);
		SetFilterIntValue(0, FILTER_BY_DATE_ACTIVE);
	}
	else
	{
		ui.dateFilter_frame->setDisabled(false);
		SetFilterIntValue(1, FILTER_BY_DATE_ACTIVE);
	}
	ReloadData();
}

void CommWorkBenchWidget::on_viewSelect_toolButton_clicked()
{
	/*
	int nFuiID;
	if (m_nGridType == DESKTOP_GRID)
		nFuiID = COMM_GRID_FILTER_WIDGET_DESKTOP;
	else if (m_nGridType == CONTACT_GRID)
		nFuiID = COMM_GRID_FILTER_WIDGET_CONTACT;
	else if (m_nGridType == PROJECT_GRID)
		nFuiID = COMM_GRID_FILTER_WIDGET_PROJECT;

	//So far only for desktop
	int nGID = g_objFuiManager.OpenFUI(nFuiID, true, false);

	int nCurrentViewID = QVariant(ui.viewSelect_comboBox->itemData(ui.viewSelect_comboBox->currentIndex())).toInt();
	dynamic_cast<CommGridFilter*>(g_objFuiManager.GetFUIWidget(nGID))->SetCurrentView(nCurrentViewID, m_recFilterViewData, m_nLoggedPersonID);
	connect(dynamic_cast<CommGridFilter*>(g_objFuiManager.GetFUIWidget(nGID)), SIGNAL(SetFilter(int, DbRecordSet)), this, SLOT(SetFilter(int, DbRecordSet)));
	*/
}

void CommWorkBenchWidget::on_viewSelect_comboBox_currentIndexChanged(int index)
{
	m_nCurrentViewID = ui.viewSelect_comboBox->itemData(index).toInt();
	if (!ReloadViewSelector(m_nCurrentViewID))
		return;	
	SetupDateRangeSelector();
	ReloadData();

	//emit FilterChanged(m_strCurrentPersonInitials + ": " + ui.viewSelect_comboBox->currentText());
}

void CommWorkBenchWidget::OnDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange)
{
	if(!m_recFilterViewData.getRowCount())
	{
		QMessageBox::information(NULL, tr("No Filter Selected"), tr("Please select a filter first!"));
		return;
	}

	SetFilterDateTimeValue(dtFrom, FROM_DATE);
	SetFilterDateTimeValue(dtTo, TO_DATE);
	SetFilterIntValue(nRange, DATE_RANGE_SELECTOR);

	//_DUMP(m_recFilterViewData);

	ReloadData();
}

void CommWorkBenchWidget::on_refresh_toolButton_clicked()
{
	ReloadData();
}

DbRecordSet* CommWorkBenchWidget::GetEmailRecordSet()
{
	return &m_recEmail;
}

DbRecordSet* CommWorkBenchWidget::GetVoiceCallRecordSet()
{
	return &m_recVoiceCall;
}

DbRecordSet* CommWorkBenchWidget::GetDocumentsRecordSet()
{
	return &m_recDocument;
}

void CommWorkBenchWidget::ClearCommGrid()
{
	m_nCurrentEntityID = -1;

	//Clear grid data.
	m_recEmail.clear();
	m_recVoiceCall.clear();
	m_recDocument.clear();

	//Initialize grid with empty recordsets.
	ui.CommGrid_widget->Initialize(&m_recEmail, &m_recDocument, &m_recVoiceCall,m_nGridType);

	emit ContentChanged();
}

void CommWorkBenchWidget::ReloadData()
{
	ContactChanged(m_nCurrentEntityID);
	
	emit ContentChanged();
}

void CommWorkBenchWidget::SetupDateRangeSelector()
{
	//_DUMP(m_recFilterViewData);
	bool bDateFilterUsed = GetFilterBoolValue(m_recFilterViewData, FILTER_BY_DATE_ACTIVE);
	QDateTime datFrom	 = GetFilterDateTimeValue(m_recFilterViewData, FROM_DATE);
	QDateTime datTo		 = GetFilterDateTimeValue(m_recFilterViewData, TO_DATE);
	int nRange			 = GetFilterIntValue(m_recFilterViewData, DATE_RANGE_SELECTOR);

	//If some range selected use the range not the stored date.
	if (nRange > 0)
	{
		QDate dateFrom, dateTo;
		switch(nRange){
			case 1: // "Day"
				dateFrom = QDate::currentDate();
				dateTo = dateFrom; 
				break;
			case 2: // "Week" (current week)
				{
					dateFrom = QDate::currentDate();
					int nWeekday = dateFrom.dayOfWeek();
					dateFrom = dateFrom.addDays(-nWeekday+1); 
					dateTo = dateFrom; 
					dateTo = dateTo.addDays(6);
				}
				break;
			case 3: // "Month"
				{
					dateFrom = QDate::currentDate();
					if(dateFrom.day() > 1)
						dateFrom = dateFrom.addDays(-(dateFrom.day()-1));
					dateTo = dateFrom;
					int nDaysInMonth = dateFrom.daysInMonth();
					dateTo = dateTo.addDays(nDaysInMonth-1);
				}
				break;
			case 4: // "Year" - move to the start of the this year
				dateFrom = QDate::currentDate();
				dateFrom = QDate(dateFrom.year(), 1, 1);
				dateTo = QDate(dateFrom.year(), 12, 31); 
				break;
		}

		datFrom = QDateTime(dateFrom, QTime(0,0,0));
		datTo	= QDateTime(dateTo, QTime(23,59,59));

		SetFilterDateTimeValue(datFrom, FROM_DATE);
		SetFilterDateTimeValue(datTo, TO_DATE);
	}

	ui.dateFilter_checkBox->blockSignals(true);
	ui.dateFilter_checkBox->setChecked(bDateFilterUsed);
	ui.dateFilter_checkBox->blockSignals(false);
	ui.dateFilter_frame->blockSignals(true);
	ui.dateFilter_frame->setEnabled(bDateFilterUsed);
	ui.dateFilter_frame->Initialize(datFrom, datTo, nRange);
	ui.dateFilter_frame->blockSignals(false);
}

void CommWorkBenchWidget::SetupSortRecordSet()
{
	m_lstSortColumnSetup.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_GRID_COLUMN_SETUP));
	m_lstSortColumnSetup.addRow(10);
	m_lstSortColumnSetup.setData(0, "BOGW_HEADER_TEXT", tr("Contact"));
	m_lstSortColumnSetup.setData(0, "BOGW_COLUMN_NAME", "CONTACT");
	m_lstSortColumnSetup.setData(0, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(0, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(1, "BOGW_HEADER_TEXT", tr("Project"));
	m_lstSortColumnSetup.setData(1, "BOGW_COLUMN_NAME", "PROJECT");
	m_lstSortColumnSetup.setData(1, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(1, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(2, "BOGW_HEADER_TEXT", tr("Date 1"));
	m_lstSortColumnSetup.setData(2, "BOGW_COLUMN_NAME", "DATE_1");
	m_lstSortColumnSetup.setData(2, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(2, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(3, "BOGW_HEADER_TEXT", tr("Date 2"));
	m_lstSortColumnSetup.setData(3, "BOGW_COLUMN_NAME", "DATE_2");
	m_lstSortColumnSetup.setData(3, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(3, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(4, "BOGW_HEADER_TEXT", tr("Status"));
	m_lstSortColumnSetup.setData(4, "BOGW_COLUMN_NAME", "STATUS");
	m_lstSortColumnSetup.setData(4, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(4, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(5, "BOGW_HEADER_TEXT", tr("Application type"));
	m_lstSortColumnSetup.setData(5, "BOGW_COLUMN_NAME", "APP_TYPE");
	m_lstSortColumnSetup.setData(5, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(5, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(6, "BOGW_HEADER_TEXT", tr("Subtype"));
	m_lstSortColumnSetup.setData(6, "BOGW_COLUMN_NAME", "SUBTYPE");
	m_lstSortColumnSetup.setData(6, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(6, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(7, "BOGW_HEADER_TEXT", tr("Media"));
	m_lstSortColumnSetup.setData(7, "BOGW_COLUMN_NAME", "MEDIA");
	m_lstSortColumnSetup.setData(7, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(7, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(8, "BOGW_HEADER_TEXT", tr("Originator"));
	m_lstSortColumnSetup.setData(8, "BOGW_COLUMN_NAME", "ORIGINATOR");
	m_lstSortColumnSetup.setData(8, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(8, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.

	m_lstSortColumnSetup.setData(9, "BOGW_HEADER_TEXT", tr("Owner"));
	m_lstSortColumnSetup.setData(9, "BOGW_COLUMN_NAME", "OWNER");
	m_lstSortColumnSetup.setData(9, "BOGW_COLUMN_TYPE", 0);				//Edit text - 0.
	m_lstSortColumnSetup.setData(9, "BOGW_SORT_ORDER", 0);				//0 - Ascending, 1 - Descending.
}

bool CommWorkBenchWidget::ReloadViewSelector(int nViewID /*= -1*/)
{
	m_recFilterViews.clear();
	Status status;
	if (nViewID <= 0)
	{
		_SERVER_CALL(BusCommunication->GetCommFilterViews(status, m_recFilterViews, m_nLoggedPersonID, m_nGridType))
	}
	else
	{
		_SERVER_CALL(BusCommunication->GetCommFilterViewDataAndViews(status, m_recFilterViewData, m_recFilterViews, nViewID, m_nGridType))
	}
	_CHK_ERR_NO_RET(status);

	//_DUMP(m_recFilterViews);
	//_DUMP(m_recFilterViewData);

	if (status.IsOK())
	{
		//Load views in combo.
		LoadViewComboBox(nViewID);
		return true;
	}
	else
		return false;
}

void CommWorkBenchWidget::LoadViewComboBox(int nViewID)
{
	ui.viewSelect_comboBox->blockSignals(true);
	//Fill combo.
	ui.viewSelect_comboBox->clear();
	int nRowCount = m_recFilterViews.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		int ViewID = m_recFilterViews.getDataRef(i, "BUSCV_ID").toInt();
		QString strViewName = m_recFilterViews.getDataRef(i, "BUSCV_NAME").toString();
		ui.viewSelect_comboBox->addItem(strViewName, ViewID);
	}
	//Set current item.
	ui.viewSelect_comboBox->setCurrentIndex(ui.viewSelect_comboBox->findData(nViewID));
	ui.viewSelect_comboBox->blockSignals(false);
}

void CommWorkBenchWidget::FillInsertDefaultValues(DbRecordSet &recFilterSettings)
{
	//Fill default recordset data.
	recFilterSettings.clear();
	CommGridFilterSettings filterSettings;
	recFilterSettings = filterSettings.GetCommFilterSettings();

	//_DUMP(recFilterSettings);

	recFilterSettings.setData(recFilterSettings.find("BUSCS_SETTING_ID", FILTER_BY_ENTITY_TYPE_ACTIVE, true), "BUSCS_VALUE", 1);
	//_DUMP(recFilterSettings);
}

void CommWorkBenchWidget::SetFilterIntValue(int nValue, int nFilterSetting)
{
	//_DUMP(m_recFilterViewData);
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		m_recFilterViewData.setData(nRow, "BUSCS_VALUE", nValue);
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		Q_ASSERT(nTmpRow >= 0);
		tmp.setData(nTmpRow, "BUSCS_VALUE", nValue);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		m_recFilterViewData.merge(tmp, true);
	}
}

void CommWorkBenchWidget::SetFilterDateTimeValue(QDateTime datValue, int nFilterSetting)
{
	int nRow = m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		m_recFilterViewData.setData(nRow, "BUSCS_VALUE_DATETIME", datValue);
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		Q_ASSERT(nTmpRow >= 0);
		tmp.setData(nTmpRow, "BUSCS_VALUE_DATETIME", datValue);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		m_recFilterViewData.merge(tmp, true);
	}
}

bool CommWorkBenchWidget::GetFilterBoolValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	bool bValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		bValue = m_recFilterViewData.getDataRef(m_recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true), "BUSCS_VALUE").toBool();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		bValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE").toBool();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return bValue;
}

int CommWorkBenchWidget::GetFilterIntValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	int nValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		nValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE").toInt();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		nValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE").toInt();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return nValue;
}

QString CommWorkBenchWidget::GetFilterStringValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	QString strValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		strValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE_STRING").toString();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		strValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE_STRING").toString();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return strValue;
}

QDateTime CommWorkBenchWidget::GetFilterDateTimeValue(DbRecordSet recFilterViewData, int nFilterSetting)
{
	QDateTime datValue;
	int nRow = recFilterViewData.find("BUSCS_SETTING_ID", nFilterSetting, true);
	if (nRow >= 0)
		datValue = m_recFilterViewData.getDataRef(nRow, "BUSCS_VALUE_DATETIME").toDateTime();
	//If filter setting not found then load default.
	else
	{
		DbRecordSet tmp;
		tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS ));
		FillInsertDefaultValues(tmp);
		int nTmpRow = tmp.find("BUSCS_SETTING_ID", nFilterSetting, true);
		datValue = tmp.getDataRef(nTmpRow, "BUSCS_VALUE_DATETIME").toDateTime();
		Q_ASSERT(nTmpRow >= 0);
		//Merge new value with old recordset.
		tmp.clearSelection();
		tmp.selectRow(nTmpRow);
		recFilterViewData.merge(tmp, true);
	}
	return datValue;
}

void CommWorkBenchWidget::on_showFilter_toolButton_clicked()
{
	ui.filter_frame->setVisible(true);
	ui.buttons_frame->setVisible(false);
}

void CommWorkBenchWidget::on_hideFilter_toolButton_clicked()
{
	ui.filter_frame->setVisible(false);
	ui.buttons_frame->setVisible(true);
}
