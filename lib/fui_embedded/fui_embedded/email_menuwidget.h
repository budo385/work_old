#ifndef EMAIL_MENUWIDGET_H
#define EMAIL_MENUWIDGET_H

#include <QWidget>
#include "generatedfiles/ui_email_menuwidget.h"
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include "common/common/observer_ptrn.h"
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "fuibase.h"

//
class Email_MenuWidget : public FuiBase, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Email_MenuWidget(QString strFuiName, QWidget *parent=0);
	~Email_MenuWidget();


	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void SetDefaults(DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL);
	void SetDefaultMail(QString strEmail,int nEmailReplyID=-1,bool bIsForward=false){m_strEmailDefault=strEmail;m_nEmailReplyID=nEmailReplyID;m_bIsForward=bIsForward;}
	void SetPackSendMode(QStringList *lstPaths){m_lstPackSendPaths=*lstPaths;}
	void SetScheduleFlag(){m_bScheduleTask=true;}

signals:
	void NeedNewData();		//emited always when new document is about to be created...to assign doc to contacts/projects later

private slots:
	void OnItemClicked(QTreeWidgetItem *,int);
	void OnMenuButton();
	void OnCategoryChanged(const QString strCategory);
	void ReLoadTemplates(){LoadTemplates(true);}

private:
	Ui::Email_MenuWidgetClass ui;

	void LoadTemplates(bool bReload=false);
	void LoadCategory(bool bReload=false);
	QTreeWidgetItem* SearchTree(QTreeWidget* pTree, QVariant nUserValue);

	DbRecordSet m_lstTemplates;
	DbRecordSet m_lstContacts; 
	int m_nProjectID;
	int m_nLoggedID;
	int m_nEmailReplyID;
	QString m_strEmailDefault;
	QStringList m_lstPackSendPaths;
	bool m_bIsForward;
	bool m_bCloseOnSend;
	bool m_bScheduleTask;
	MainEntitySelectionController m_Category;	
	QPoint m_dragPosition;
	bool m_bCreateQuickTask;
	bool m_bSerial_Mode;
};






#endif // EMAIL_MENUWIDGET_H
