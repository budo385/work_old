#ifndef FUIMANAGER_H
#define FUIMANAGER_H

#include <QMainWindow>
#include <QHash>
#include <QLabel>
#include "ui_fuimanager.h"
#include "fuicodeidlist.h"
#include "fuibase.h"
#include "pictureflowwidget.h"
#include "splashscreen.h"

#include "styledpushbutton.h"

class FuiManager : public QMainWindow
{
	Q_OBJECT

public:
	FuiManager(QWidget *parent = 0, Qt::WFlags flags = 0);
	~FuiManager();

	void Initialize();
	QWidget* OpenFUI(int nFuiCode, int nMode = FuiBase::MODE_EMPTY, int nRecordID = -1,bool bShowVisible=true);
	void CloseFUI(int nFuiID, bool bSkipWidgetDeletion=false);
	void CloseAllFUIWindows();
	int GetFuiID(FuiBase *pWidget);
	void SetFUILeftSoftKey(int nFuiID);
	void SetFUIRightSoftKey(int nFuiID);
	int GetCurrentFuiID();

	void Login();

	/*	
	QWidget *GetFUIWidget(int nGID);
	FuiBase *GetFUI(int nGID);
	bool ActivateFUI(int nGID);
	*/
public slots:
	bool GoOffline(bool bOffline=true, bool bWarn=false);

private:
	Ui::FuiManagerClass ui;

	int	 AddStackedWidget(QWidget *pFuiWnd);
	void SetCurrentWidget(QWidget *pFuiWnd);
	void RemoveWidget(QWidget *pFuiWnd);
	void SetWindowsActionVisible(int nFuiCodeID);
	void SetWindowsActionInVisible(int nFuiCodeID);
	void OnUserChangedTheme(int nThemeID);
	void ClearFUIRightActions();
	QWidget* OpenPicFlowMenu();
	void CreateMenus();
	void CreateFuiMenus();
	void SetupMenus();
	void UpdateLoginMenuState();
	void OnWinMenuActionTriggered(int nFuiCodeID);
	bool DestroyLayout();
	void SetClockText(QString strText);
	void UpdateClock();
	void SetFuiCodeToBackHash(int nFuiCode);	//Call from OpenFui() to set fui to back button hash.
	void RemoveFuiCodeFromBackHash(int nFuiCode);	//Call from CloseFui() to remove fui to back button hash.
	int  GetPreviousFuiCode();					//For Back left Soft Key.
	void CloseAllModalWindows();
	

	//Menus.
	QAction*	m_pMenuFile;
	QAction*	m_pMenuWindows;		//For displaying currently open windows (FUIs).
	//Menu actions.
	QAction*	m_pActLogin;
	QAction*	m_pActLogout;
	QAction*	m_pActFUI_PersonalSettings;
	QAction*	m_pActQuit;
	QAction*	m_pActPurgeCache;
	QHash<int, QAction*> m_hshFuiCodeIDToWinMenuAction; //Maps FUI ID to Windows menu action.
	PictureFlowWidget *m_MainPicFlowMenu;
	int			m_nCurrentPicFlowMenuItem;

	QWidget* CreateFuiWidget(int nFuiCode);

	int						 m_nFuiIDCounter;			// unique IDs generator
	int						 m_nCurrentFuiCode;			// FuiID to be opened after login, it must not be -1;

	QList<int>				 m_lstFuiIDsList;			//List for Windows menu in exact order in which they are displayed.
	QHash<int, int>			 m_hshFuiIDToFuiCode;		//Maps FUI ID to FUI code.
	QHash<int, int>			 m_hshFuiIDToStackIndex;	//Maps FUI ID to main stacked widget index.
	QHash<int, QWidget*>	 m_hshFuiIDToFuiWidget;		//Maps FUI ID to FUI Widget.
	QLabel					 *m_pClock;					//Clock label.
	int						 m_nTimer;
	StyledPushButton		 *m_pLoginButton;
	StyledPushButton		 *m_pMainMenuButton;
	QAction					 *m_pBackAction;
	SplashScreen			 *m_pSplashScreen;
	QHash<int, int>			 m_hshBackButtonHash;		//Maps FUI order of opening to Fui Code.
	QList<int>				 m_lstBackButtonList;		//Maps FUI order of opening to Fui Code.
	int						 m_nFuiOrderCounter;
	QList<QAction *>		 m_lstRightFUIActions;	
	int						 m_nRightFUIActionsFuiCode;			// FuiID that holds right menu: when closeed: destroy
	bool					 m_bFromOffline2OnlineInProgress;	//true when user activates 
	bool					 m_bProcessingFatalError;

protected:
	void timerEvent ( QTimerEvent * event ) ;

private slots:
		//Picture flow menu slot.
		void OncenterIndexSelectedForEditing(int index);
		//Menu slots.
		void OnMenu_OnLogin();
		void OnMenu_OnLogout();
		void OnMenu_PersonalSettings();
		void OnMenu_PurgeCache();
		//Login slots.
		void OnClientLogged();
		void OnClientLoggOff();
		void OnClientClosedLoginWnd();
		void OnProcessBeforeLogout();
		void OnCommunicationFatalError(int nErrorCode);
		void OnLoginDialogCreated(QDialog *pDlg);

		//FUI action triggers:
		void GoBack();
		void OnContactList_triggered();
		void OnContactDetails_triggered();
		void OnContactGrid_triggered();
		void OnMainMenuButton_clicked();
		void OnProjectList_triggered();
		void OnEmailsend_triggered();
		void OnFavoritesList_triggered();
		void OnEmailTemplates_triggered();
		void OnVoiceCall_triggered();
		void OnSelectedFavourite_triggered();
		void OnLoginStatusClick();
};

#endif // FUIMANAGER_H
