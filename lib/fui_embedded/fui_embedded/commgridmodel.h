#ifndef COMMGRIDMODEL_H
#define COMMGRIDMODEL_H

#include <QAbstractTableModel>

#include "common/common/dbrecordset.h"

class CommGridModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	CommGridModel(bool bSideBarViewMode, QObject *parent = 0);
	~CommGridModel();

	void Initialize(DbRecordSet *recEmail, DbRecordSet *recVoiceCall, DbRecordSet *recDocument, 
					QHash<int, int>		&hshRow2Color,
					QHash<int, QString> &hshRow2Pix1,
					QHash<int, QString> &hshRow2Pix2,
					QHash<int, QString> &hshRow2Pix3,
					QHash<int, QString> &hshRow2Pix4,
					QHash<int, QString> &hshRow2Date1,
					QHash<int, QString> &hshRow2Date2,
					QHash<int, QString> &hshRow2Autor,
					QHash<int, QString> &hshRow2Subject,
					QHash<int, QString> &hshRow2Descr,
					QHash<int, QString> &hshRow2SecondColDisplay,
					QHash<int, QString> &hshRow2Owner,
					QHash<int, QString> &hshRow2Contact,
					QHash<int, QString> &hshRow2Project,
					DbRecordSet &recApplicationIcons, 
					QHash<int, QPixmap> &hshDocumentIcons);
	void RefreshItems(QList<int> lstRows, 
					  QHash<int, QString> &hshRow2Owner, 
					  QHash<int, QString> &hshRow2Contact, 
					  QHash<int, QString> &hshRow2Project);

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	QMimeData *mimeData(const QModelIndexList & indexes) const;
	QStringList mimeTypes() const;

	Qt::ItemFlags flags(const QModelIndex &index) const;

private:
	void ClearHashes();
	void FillIconHashes(DbRecordSet &recApplicationIcons, QHash<int, QPixmap> &hshDocumentIcons);

	int					m_nRows;
	int					m_nColumns;
	bool				m_bSideBarViewMode;
	QHash<int, int>		m_hshColorID;
	QHash<int, QString> m_hshPix1;
	QHash<int, QString> m_hshPix2;
	QHash<int, QString> m_hshPix3;
	QHash<int, QString> m_hshPix4;
	QHash<int, QString> m_hshDate1;			//First column first line.
	QHash<int, QString> m_hshDate2;			//First column second line.
	QHash<int, QString> m_hshAutor;			//First column third line.
	QHash<int, QString> m_hshDisplay;		//Second column display.
	QHash<int, QString> m_hshSubject;		//Second column first line.
	QHash<int, QString> m_hshDescription;	//Second column second line.
	QHash<int, QString> m_hshOwner;			//Third column first line.
	QHash<int, QString> m_hshContact;		//Third column second line.
	QHash<int, QString> m_hshProject;		//Third column third line.

	QHash<int,QPixmap>	m_hshApplicationIcons; //Application icons to it's id.
	QHash<int,QPixmap>	m_hshDocumentIcons; //Document icons to it's id.

	//Pointers to communication grid widget recordset just for drag enabling.
	DbRecordSet			*m_recEmail,
						*m_recVoiceCall,
						*m_recDocument;
};

#endif // COMMGRIDMODEL_H
