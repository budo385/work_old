#ifndef CONTACTGRID_H
#define CONTACTGRID_H

#include <QWidget>
#include "ui_contactgrid.h"

#include "fuibase.h"

class ContactGrid : public FuiBase
{
	Q_OBJECT

public:
	ContactGrid(QString strFuiName, QWidget *parent = 0);
	~ContactGrid();

	void on_selectionChange(int nEntityRecordID);

private:
	Ui::ContactGridClass ui;
};

#endif // CONTACTGRID_H
