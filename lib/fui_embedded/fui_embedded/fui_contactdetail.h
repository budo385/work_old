#ifndef FUI_CONTACTDETAIL_H
#define FUI_CONTACTDETAIL_H

#include "fuibase.h"
#include "ui_fui_contactdetail.h"
#include "gui_core/gui_core/styledpushbutton.h"
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/selection_acp.h"
#include "selection_sapne_fui.h"
#include "gui_core/gui_core/datetimeeditex.h"
#include "selectedcontactpicture.h"
#include <QRadioButton>
#include <QTextEdit>
#include <QComboBox>
#include <QLabel>
#include <QTableWidget>


class FUI_ContactDetail : public FuiBase
{
	Q_OBJECT

public:
	enum ContactDetailMode
	{
		CONTACT_DETAIL_ALL=-1,
		CONTACT_DETAIL_DESC=0,
		CONTACT_DETAIL_PHONE=1,
		CONTACT_DETAIL_EMAIL=2,
		CONTACT_DETAIL_ADDRESS=3,
		CONTACT_DETAIL_INTERNET=4,
		CONTACT_DETAIL_ACCESS=5,
		CONTACT_DETAIL_PIC=6,
	};

	FUI_ContactDetail(QString strFuiName, QWidget *parent = 0);
	~FUI_ContactDetail();

	void on_selectionChange(int nEntityRecordID);
	void SetDetailMode(int nMode=CONTACT_DETAIL_ALL);
	void OnOfflineModeChanged(bool bOffline=true);

private slots:
	void OnDesc();
	void OnPhone();
	void OnEmail();
	void OnAddress();
	void OnInternet();
	void OnAccess();
	void OnPic();
	void OnAddress_currentIndexChanged(int nIndex);
	void tablePhones_itemClicked ( int,int );
	void tableEmails_itemClicked ( int,int);
	void tableInternet_itemClicked ( int,int );

private:
	Ui::FUI_ContactDetailClass ui;

	void TabDesc_Create();
	void TabDesc_Refresh();
	void TabPhone_Create();
	void TabPhone_Refresh();
	void TabEmail_Create();
	void TabEmail_Refresh();
	void TabInternet_Create();
	void TabInternet_Refresh();
	void TabAddress_Create();
	void TabAddress_Refresh();
	void TabAccess_Create();
	void TabAccess_Refresh();
	void TabPic_Create();
	void TabPic_Refresh();



	StyledPushButton *m_pBtnDesc;
	StyledPushButton *m_pBtnPhone;
	StyledPushButton *m_pBtnEmail;
	StyledPushButton *m_pBtnAddress;
	StyledPushButton *m_pBtnInternet;
	StyledPushButton *m_pBtnAccess;
	StyledPushButton *m_pBtnPic;

	bool		m_bShowPicture;
	DbRecordSet m_RowContactData;

	bool m_bTabAccessCreated;
	bool m_bTabAddressCreated;
	bool m_bTabDescCreated;
	bool m_bTabPhoneCreated;
	bool m_bTabEmailCreated;
	bool m_bTabInternetCreated;
	bool m_bTabPicCreated;

	
	//access:
	QRadioButton *m_pbtnPrivate;
	QRadioButton *m_pbtnPublicRead;
	QRadioButton *m_pbtnPublicWrite;
	Selection_ACP *m_pDepartment;
	Selection_ACP *m_pFunct;
	Selection_ACP *m_pProfession;
	Selection_SAPNE_FUI *m_pOwner;
	DateTimeEditEx	*m_pBirthday;
	SelectedContactPicture *m_pPic;
	QTextEdit		*m_pTextDesc;
	QTableWidget	*m_tablePhones;
	QTableWidget	*m_tableEmails;
	QTableWidget	*m_tableInternet;
	QComboBox		*m_cmbInfo_Address;
	QTextEdit		*m_txtInfo_Address;
	QLabel			*m_lbl_Cnt_Address;


};

#endif // FUI_CONTACTDETAIL_H
