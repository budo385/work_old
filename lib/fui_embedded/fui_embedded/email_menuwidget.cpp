#include "email_menuwidget.h"
#include <QHeaderView>
#include "bus_client/bus_client/emailhelper.h"

#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbsqltableview.h"
#include "fui_embedded/fui_embedded/fui_email.h"
//#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/gui_helper.h"

//global comm manager
#include "fuimanager.h"
extern FuiManager *g_FuiManager;				
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher





Email_MenuWidget::Email_MenuWidget(QString strFuiName, QWidget *parent)
: FuiBase(strFuiName,parent)
{

	ui.setupUi(this);

	m_bSerial_Mode=false;
		
	m_nProjectID=-1;
	m_nEmailReplyID=-1;
	m_bIsForward=false;
	m_bCloseOnSend=false;
	m_bScheduleTask=false;
	m_bCreateQuickTask=false;

	m_lstContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_CONTACT_LINK));

	QSize buttonSize_menu(13, 11);
	StyledPushButton *close		= new StyledPushButton(this,":Icon_CloseBlack.png",":Icon_CloseBlack.png","");
	close		->setMaximumSize(buttonSize_menu);
	close		->setMinimumSize(buttonSize_menu);
	close		->setToolTip(tr("Close"));
	QLabel *label = new QLabel(this);
	label->setStyleSheet("QLabel "+ThemeManager::GetMobileChapter_Font());
	label->setText(tr("Email Templates"));
	label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

	QVBoxLayout *buttonLayout_menu_v = new QVBoxLayout;
	buttonLayout_menu_v->addWidget(close);
	buttonLayout_menu_v->addStretch(1);
	buttonLayout_menu_v->setSpacing(0);
	buttonLayout_menu_v->setContentsMargins(2,0,2,2);

	QHBoxLayout *buttonLayout_menu_h = new QHBoxLayout;
	buttonLayout_menu_h->addWidget(label);
	buttonLayout_menu_h->addStretch(1);
	buttonLayout_menu_h->addLayout(buttonLayout_menu_v);
	buttonLayout_menu_h->setSpacing(0);
	buttonLayout_menu_h->setContentsMargins(2,0,0,0);
	connect(close,SIGNAL(clicked()),this,SLOT(close()));

	ui.frame->setLayout(buttonLayout_menu_h);
	ui.frame->setStyleSheet("QFrame#frame "+ThemeManager::GetSidebarChapter_Bkg());

	GUI_Helper::CreateStyledButton(ui.btnSendMail,":Email16.png","",0,ThemeManager::GetCEMenuButtonStyleLighter(),0);
	ui.btnSendMail->setToolTip(tr("Empty Email"));

	connect(ui.btnSendMail,SIGNAL(clicked()),this,SLOT(OnMenuButton()));

	//TEMPLATE TREE:
	ui.treeWidgetTemplate->header()->hide();
	ui.treeWidgetTemplate->setColumnCount(1);
	ui.treeWidgetTemplate->setSelectionBehavior( QAbstractItemView::SelectRows);
	ui.treeWidgetTemplate->setSelectionBehavior( QAbstractItemView::SelectItems);
	connect(ui.treeWidgetTemplate,SIGNAL(itemClicked (QTreeWidgetItem *,int)),this,SLOT(OnItemClicked(QTreeWidgetItem *,int)));

	//LOGGED USER:
	m_nLoggedID=g_pClientManager->GetPersonID();

	//load all:
	m_Category.Initialize(ENTITY_EMAIL_TEMPLATE_CATEGORY);
	LoadCategory();
	LoadTemplates();
	g_ChangeManager.registerObserver(this);



	//---------------------------------------- INIT CNTX MENU APP--------------------------------//
	
	connect(ui.cmbCategory, SIGNAL( currentIndexChanged(const QString &)), this, SLOT(OnCategoryChanged(const QString)));


}

Email_MenuWidget::~Email_MenuWidget()
{
	g_ChangeManager.unregisterObserver(this);

}





//load from server, rebuild tree
void Email_MenuWidget::LoadTemplates(bool bReload)
{
	if (m_nLoggedID==0) return;

	ui.treeWidgetTemplate->blockSignals(true);
	ui.treeWidgetTemplate->clear();

	EmailHelper::GetEmailTemplates(m_lstTemplates,bReload);

	int nIDIdx=m_lstTemplates.getColumnIdx("BEM_ID");
	int nNameIdx=m_lstTemplates.getColumnIdx("BEM_TEMPLATE_NAME");
	int nDesc=m_lstTemplates.getColumnIdx("BEM_TEMPLATE_DESCRIPTION");

	//m_lstTemplates.Dump();

	QString strCategory=ui.cmbCategory->currentText();

	QList<QTreeWidgetItem *> items;
	int nSize=m_lstTemplates.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//if category!='' and in template!='' then filter, else add...
		if (strCategory!=m_lstTemplates.getDataRef(i,"BEM_CATEGORY").toString() && !m_lstTemplates.getDataRef(i,"BEM_CATEGORY").toString().isEmpty() && !strCategory.isEmpty())
		{
			continue;
		}

		QString strNodeName=m_lstTemplates.getDataRef(i,nNameIdx).toString();
		int nID=m_lstTemplates.getDataRef(i,nIDIdx).toInt();
		QTreeWidgetItem *pRoot=new QTreeWidgetItem((QTreeWidget *)0,QStringList(strNodeName));
		pRoot->setData(0,Qt::UserRole,nID);
		pRoot->setToolTip(0,m_lstTemplates.getDataRef(i,nDesc).toString());
		items.append(pRoot);
	}
	ui.treeWidgetTemplate->insertTopLevelItems(0,items);
	ui.treeWidgetTemplate->blockSignals(false);

}


void Email_MenuWidget::LoadCategory(bool bReload)
{
	m_Category.ReloadData(bReload);

	int nIx=ui.cmbCategory->currentIndex();
	ui.cmbCategory->blockSignals(true);
	ui.cmbCategory->clear();

	DbRecordSet *lstData=m_Category.GetDataSource();
	int nSize=lstData->getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.cmbCategory->addItem(lstData->getDataRef(i,0).toString());
	}
	QString strLastCat=ui.cmbCategory->currentText();
	if (nIx!=-1)
		ui.cmbCategory->setCurrentIndex(nIx);
	else
		ui.cmbCategory->setCurrentIndex(0);


	ui.cmbCategory->blockSignals(false);
}

void Email_MenuWidget::OnCategoryChanged(QString strCategory)
{
	//filter templates by category:
	LoadTemplates();
}





/*! Catches global cache events

\param pSubject			- source of msg
\param nMsgCode			- msg code
\param val				- value sent from observer
*/
void Email_MenuWidget::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	
	if(nMsgDetail==ENTITY_BUS_EMAILS)
		if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
		{
			//always reload:
			LoadCategory(true);

			//CallBack to refresh displayed data:
			DbRecordSet lstNewData=val.value<DbRecordSet>();

	
			ui.treeWidgetTemplate->blockSignals(true);
			//set node to lstNewData
			if (lstNewData.getRowCount()>0)
			{
				if (lstNewData.getColumnIdx("BEM_TEMPLATE_FLAG")>=0)
				{
					if (lstNewData.getDataRef(0,"BEM_TEMPLATE_FLAG").toInt()>0) //only reload when new template mail is modified
					{
						//Reload data:
						EmailHelper::GetEmailTemplates(m_lstTemplates,true); //reload templates from server: specially for emails, coz entity is stored in different cache
						LoadTemplates();

						QVariant varNode=lstNewData.getDataRef(0,"BEM_ID");
						QTreeWidgetItem *item=SearchTree(ui.treeWidgetTemplate,varNode);
						if (item)
						{
							item->setSelected(true);
							ui.treeWidgetTemplate->setCurrentItem(item);
						}
					}
				}
				else
					LoadTemplates(true);
			}
			ui.treeWidgetTemplate->blockSignals(false);
			return;
		}
	
}




QTreeWidgetItem* Email_MenuWidget::SearchTree(QTreeWidget* pTree, QVariant nUserValue)
{

	QTreeWidgetItem* item=NULL;

	int nSize=pTree->topLevelItemCount();
	for(int i=0;i<nSize;++i)
	{
		item=pTree->topLevelItem(i);
		if (item->data(0,Qt::UserRole)==nUserValue)
			return item;
	}

	return NULL;
}

void Email_MenuWidget::OnItemClicked(QTreeWidgetItem *itemX,int ncol)
{

	//get current template:
	int nRecordID=itemX->data(0,Qt::UserRole).toInt();
	int nRow=m_lstTemplates.find(0,nRecordID,true);
	if (nRow==-1)return;

	//open FUI
	QWidget* pFUIWidget=g_FuiManager->OpenFUI(EMAIL_SEND,FuiBase::MODE_INSERT,-1,false);
	FUI_Email *pFUI=dynamic_cast<FUI_Email*>(pFUIWidget);
	if (pFUI)
	{
		emit NeedNewData(); //give me new data, if some1 is listening....
		pFUI->SetDefaults(m_lstContacts,m_strEmailDefault,m_nProjectID,nRecordID,m_bScheduleTask,false,m_nEmailReplyID,&m_lstPackSendPaths,m_bIsForward);
	}
	if(pFUIWidget)pFUIWidget->show();  //show FUI

	close();
}


//open empty doc
void Email_MenuWidget::OnMenuButton()
{
	
	//open FUI
	QWidget* pFUIWidget=g_FuiManager->OpenFUI(EMAIL_SEND,FuiBase::MODE_INSERT);
	FUI_Email *pFUI=dynamic_cast<FUI_Email*>(pFUIWidget);
	if (pFUI)
	{
		emit NeedNewData(); //give me new data, if some1 is listening....
		pFUI->SetDefaults(m_lstContacts,m_strEmailDefault,m_nProjectID,-1,m_bScheduleTask,false,m_nEmailReplyID,&m_lstPackSendPaths,m_bIsForward);
	}
	//if(pFUI)pFUI->show();  //show FUI

	close();
}




void Email_MenuWidget::SetDefaults(DbRecordSet *lstContacts,DbRecordSet *lstProjects)
{

	if (lstContacts)
	{
		int nCntIdx=lstContacts->getColumnIdx("BCNT_ID");
		int nCntIdx1=m_lstContacts.getColumnIdx("CELC_CONTACT_ID");
		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		m_lstContacts.clear();
		int nSize=lstContacts->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			m_lstContacts.addRow();
			m_lstContacts.setData(m_lstContacts.getRowCount()-1,nCntIdx1,lstContacts->getDataRef(i,nCntIdx));
		}
	}

	if (lstProjects)
	{
		int nCntIdx=lstProjects->getColumnIdx("BUSP_ID");
		Q_ASSERT(nCntIdx!=-1);
		m_nProjectID=-1;
		if (lstProjects->getRowCount()>0)
		{
			m_nProjectID=lstProjects->getDataRef(0,nCntIdx).toInt();
		}
	}

	//m_lstPackSendPaths.clear();
}


