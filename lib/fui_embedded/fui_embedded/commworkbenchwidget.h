#ifndef COMMWORKBENCHWIDGET_H
#define COMMWORKBENCHWIDGET_H

#include <QWidget>
#include <QLabel>
#include "ui_commworkbenchwidget.h"

#include "common/common/observer_ptrn.h"

class CommWorkBenchWidget : public QWidget
{
	Q_OBJECT
/*
public:
	enum GridType			//Numbered because it is going to be written in DB (for filter view type).
	{
		DESKTOP_GRID = 0,
		CONTACT_GRID = 1,
		PROJECT_GRID = 2
	};*/
 
public:
	CommWorkBenchWidget(QWidget *parent = 0);
	~CommWorkBenchWidget();

	void 			Initialize(int nGridType, QWidget *pParentFUI);
	void			SetData(int nEntityRecordID);
	
	DbRecordSet*	GetEmailRecordSet();
	DbRecordSet*	GetVoiceCallRecordSet();
	DbRecordSet*	GetDocumentsRecordSet();
	void			ClearCommGrid();

public slots:
	void 			ReloadData();

private slots:
	void on_SelectionChanged(int nOwnerID, int nContactID, int nProjectID, int nEntitySysType, int nTaskID, QList<int> lstTaskList, int nEntityID, int nPolymorficID, int nBDMD_IS_CHECK_OUT, int nBDMD_CHECK_OUT_USER_ID, int nBTKS_SYSTEM_STATUS, QList<int> lstUnreadEmailsCENTIDs, QList<int> lstStartTaskEmailsCENT_IDs, QList<int> lstStartTaskPhoneCallsCENT_IDs);
	void on_openDoc_toolButton_clicked();
	void on_dateFilter_checkBox_toggled(bool bChecked);
	void on_viewSelect_toolButton_clicked();
	void on_viewSelect_comboBox_currentIndexChanged(int index);
	void on_refresh_toolButton_clicked();
	void OnDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange);

private slots:
	//btns:
	void on_showFilter_toolButton_clicked();
	void on_hideFilter_toolButton_clicked();

private:
	Ui::CommWorkBenchWidgetClass ui;

	void			ContactChanged(int nEntityRecordID);
	void			SetupDateRangeSelector();
	void			SetupSortRecordSet();
	bool			ReloadViewSelector(int nViewID = -1);
	void 			LoadViewComboBox(int nViewID);
	void			FillInsertDefaultValues(DbRecordSet &recFilterSettings);
	void			SetFilterIntValue(int nValue, int nFilterSetting);
	void			SetFilterDateTimeValue(QDateTime datValue, int nFilterSetting);
	bool			GetFilterBoolValue(DbRecordSet recFilterViewData, int nFilterSetting);
	int				GetFilterIntValue(DbRecordSet recFilterViewData, int nFilterSetting);
	QString			GetFilterStringValue(DbRecordSet recFilterViewData, int nFilterSetting);
	QDateTime		GetFilterDateTimeValue(DbRecordSet recFilterViewData, int nFilterSetting);

	int				m_nCurrentEntityID;
	int				m_nCurrentViewID;
	int				m_nGridType;					//Person, contact or project grid. See GridType enum. For view selection and view fui opening.
	int				m_nLoggedPersonID;
	int 			m_nEntityID;
	int 			m_nTaskID;
	int 			m_nOwnerID;
	int 			m_nContactID; 
	int 			m_nProjectID;
	int 			m_nEntitySysType;
	QList<int>		m_lstTaskList;
	QList<int>		m_lstUnreadEmailsList;
	QList<int>		m_lstStartTaskEmailsCENT_IDs;
	QList<int>		m_lstStartTaskPhoneCallsCENT_IDs;
	int				m_nPolymorficID;
	
	DbRecordSet		m_recEmail,
					m_recVoiceCall,
					m_recDocument;
	DbRecordSet		m_recFilterViews;
	DbRecordSet		m_lstSortColumnSetup;
	DbRecordSet		m_recFilterViewData;

	QHash<int, QString> m_hshViewHash;

	QWidget			*m_pParentFUI;

	DbRecordSet		m_RowContactData;

signals:
	void 			ContentChanged();
	void 			NeedNewData();
	void 			FilterChanged(QString strFilterName);
};

#endif // COMMWORKBENCHWIDGET_H
