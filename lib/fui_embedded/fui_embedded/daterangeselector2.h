#ifndef DATERANGESELECTOR2_H
#define DATERANGESELECTOR2_H

#include <QWidget>
#include "ui_daterangeselector2.h"

class DateRangeSelector2 : public QWidget
{
    Q_OBJECT

public:
    DateRangeSelector2(QWidget *parent = 0);
    ~DateRangeSelector2();

	void Initialize(QDateTime &dateFrom, QDateTime &dateTo, int nRange);

private:
    Ui::DateRangeSelector2Class ui;

	bool m_bSideBarMode;

signals:
	void onDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange);

private slots:
	void on_btnPrev_clicked();
	void on_btnNext_clicked();
	void on_btnCurDateRange_clicked();
	void OnDateChanged(const QDate &);
	void OnComboChanged(int nIdx);
};

#endif // DATERANGESELECTOR2_H
