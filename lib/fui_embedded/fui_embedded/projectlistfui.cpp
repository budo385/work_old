#include "projectlistfui.h"

#include "fuicodeidlist.h"
#include "common/common/entity_id_collection.h"

ProjectListFui::ProjectListFui(QString strFuiName, QWidget *parent)
	: FuiBase(strFuiName, parent)
{
	ui.setupUi(this);

	ui.frameProjects->Initialize(ENTITY_BUS_PROJECT, false, true, true, false);
}

ProjectListFui::~ProjectListFui()
{

}

void ProjectListFui::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail/*=0*/,const QVariant val/*=QVariant()*/)
{
	
}
