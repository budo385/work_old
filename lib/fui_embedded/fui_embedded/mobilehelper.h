#ifndef MOBILEHELPER_H
#define MOBILEHELPER_H

#include <QWidget>
#include "pictureflowwidget.h"
#include "selectedcontactpicture.h"
#include "common/common/dbrecordset.h"

class MobileHelper //: public QObject
{
	//Q_OBJECT

public:
	//MobileHelper(QObject *parent);
	//~MobileHelper();

	//New single picture widget.
	static QImage CreateImageWithMirror(const QImage &image);
	static void SelectedContactImageFrame_Create(QWidget *pWidget, SelectedContactPicture **pPic, QSize slideSize = QSize(60,80));
	static void SelectedContactImageFrame_SetContactPicture(SelectedContactPicture *pPicFlow, DbRecordSet &recContact);

	//New picture flow widget.
	static void PictureFlowWidget_Create(QWidget *pWidget, PictureFlowWidget **pPic, QStringList &lstPictures, QSize slideSize = QSize(60,80), int nCenterItem = 0, int nPicFlowType = 0);
	static void PictureFlowWidget_SetContactsPictures(PictureFlowWidget *pPicFlow, DbRecordSet &recContact, int nCenterIndex);
	
	//Picture flow menu helpers.
/*
	static void PictureFlow_Create(QWidget *pWidget,PictureFlow **pPic, QSize slideSize = QSize(60,80));
	static void PictureFlow_SetContactPicture(PictureFlow *pPicFlow, DbRecordSet &recContact);
	static void PictureFlow_SetContactsPictures(PictureFlow *pPicFlow, DbRecordSet &recContact, int nCenterIndex);
*/
	//Destop and screen geometry helpers.
	static bool IsScreenInPortraitMode();

private:
	
};

#endif // MOBILEHELPER_H
