#include "mobilehelper.h"
#include "gui_core/gui_core/thememanager.h"
#include <QHBoxLayout>
#include <QFrame>
#include <QApplication>
#include <QDesktopWidget>
#include <QPainter>

/*
MobileHelper::MobileHelper(QObject *parent)
	: QObject(parent)
{

}

MobileHelper::~MobileHelper()
{

}
*/

QImage MobileHelper::CreateImageWithMirror(const QImage &image)
{
	//Mirror scale factor.
	qreal scaleFactor = 0.5;

	//Mirrored image.
	QImage mirrorImage = image;

	mirrorImage = mirrorImage.mirrored();
	mirrorImage = mirrorImage.scaled(mirrorImage.width(), mirrorImage.height()*scaleFactor);

	QPoint p1, p2;
	p2.setY(mirrorImage.height());

	QLinearGradient gradient(p1, p2);
	gradient.setColorAt(0, Qt::white);
	gradient.setColorAt(0.4, QColor(0, 0, 0, 20));
	gradient.setColorAt(1, Qt::transparent);

	QPainter p(&mirrorImage);
	p.setBackgroundMode(Qt::TransparentMode);
	p.setCompositionMode(QPainter::CompositionMode_DestinationIn);
	p.fillRect(0, 0, mirrorImage.width(), mirrorImage.height(), gradient);
	p.end();

	//Now create new image with it's mirror.
	QImage newImage(image.width(), image.height() + mirrorImage.height(), QImage::Format_ARGB32_Premultiplied);
	QPainter painter(&newImage);
	painter.setBackgroundMode(Qt::TransparentMode);
	painter.drawImage(0,0, image);
	painter.drawImage(0, image.height(), mirrorImage);
	painter.end();

	return newImage;
}

void MobileHelper::SelectedContactImageFrame_Create(QWidget *pWidget, SelectedContactPicture **pPic, QSize slideSize /*= QSize(60,80)*/)
{
	*pPic = new SelectedContactPicture();
	//Get image.
	QImage img;
	img.load(":Icon_Person128.png");
	QImage image(img.alphaChannel());
	img.setAlphaChannel(image);
	img = CreateImageWithMirror(img);

	//Create layout.
	QVBoxLayout *vbox1=new QVBoxLayout;
	vbox1->setSpacing(0);
	vbox1->setMargin(0);
	vbox1->addStretch(1);
	vbox1->addWidget((*pPic));
	vbox1->addStretch(1);

	(*pPic)->SetPixmap(QPixmap::fromImage(img));

	pWidget->setLayout(vbox1);
	if (dynamic_cast<QFrame*>(pWidget))
		pWidget->setStyleSheet(".QFrame "+ThemeManager::GetMobileSolidBkg_Dark());
	else
		pWidget->setStyleSheet(".QWidget "+ThemeManager::GetMobileSolidBkg_Dark());
}

void  MobileHelper::SelectedContactImageFrame_SetContactPicture(SelectedContactPicture *pPic, DbRecordSet &recContact)
{
	QImage img;
	if (recContact.getRowCount()>0)
	{
		if(recContact.getDataRef(0,"BCNT_PICTURE").toByteArray().size()==0)
		{
			img.load(":Icon_Person128.png");
		}
		else
		{
			img.loadFromData(recContact.getDataRef(0,"BCNT_PICTURE").toByteArray());
		}
	}
	else
	{
		img.load(":Icon_Person128.png");
	}

	QImage image(img.alphaChannel());
	img.setAlphaChannel(image);
	img	= CreateImageWithMirror(img);
	
	pPic->SetPixmap(QPixmap::fromImage(img));
}

void MobileHelper::PictureFlowWidget_Create(QWidget *pWidget, PictureFlowWidget **pPic, QStringList &lstPictures, QSize slideSize /*= QSize(60,80)*/, int nCenterItem /*= 0*/, int nPicFlowType /*= 0*/)
{
	*pPic = new PictureFlowWidget(lstPictures, slideSize, nCenterItem, nPicFlowType);

	int r,g,b;
	ThemeManager::ExtractRGBFromColorString(ThemeManager::GetMobileSolidBkg_Dark(),r,g,b);
	(*pPic)->setBackground(QBrush(QColor(r,g,b)));

	if (pWidget)
	{
		QHBoxLayout *hbox1=new QHBoxLayout;
		hbox1->setSpacing(0);
		hbox1->setMargin(0);
		hbox1->addWidget((*pPic));

		pWidget->setLayout(hbox1);

		if (dynamic_cast<QFrame*>(pWidget))
			pWidget->setStyleSheet(".QFrame "+ThemeManager::GetMobileSolidBkg_Dark());
		else
			pWidget->setStyleSheet(".QWidget "+ThemeManager::GetMobileSolidBkg_Dark());
	}
	else
		(*pPic)->setStyleSheet(".QWidget "+ThemeManager::GetMobileSolidBkg_Dark());
}

void MobileHelper::PictureFlowWidget_SetContactsPictures(PictureFlowWidget *pPicFlow, DbRecordSet &recContact, int nCenterIndex)
{
	pPicFlow->SetItems(recContact, nCenterIndex);
}
/*
void MobileHelper::PictureFlow_Create(QWidget *pWidget,PictureFlow **pPic, QSize slideSize)
{
	*pPic = new PictureFlow;
	(*pPic)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
	QImage img;
	img.load(":Icon_Person128.png");
	QImage image(img.alphaChannel());
	img.setAlphaChannel(image);
	(*pPic)->setSlideSize(slideSize);
	(*pPic)->addSlide(img);
	(*pPic)->setCenterIndex(0);

	int r,g,b;
	ThemeManager::ExtractRGBFromColorString(ThemeManager::GetMobileSolidBkg_Dark(),r,g,b);
	(*pPic)->setBackgroundColor(QColor(r,g,b));

	QHBoxLayout *hbox1=new QHBoxLayout;
	hbox1->setSpacing(0);
	hbox1->setMargin(0);
	hbox1->addWidget((*pPic));

	pWidget->setLayout(hbox1);
	if (dynamic_cast<QFrame*>(pWidget))
		pWidget->setStyleSheet(".QFrame "+ThemeManager::GetMobileSolidBkg_Dark());
	else
		pWidget->setStyleSheet(".QWidget "+ThemeManager::GetMobileSolidBkg_Dark());
	(*pPic)->show();
}

void MobileHelper::PictureFlow_SetContactPicture(PictureFlow *pPicFlow, DbRecordSet &recContact)
{
	QImage img;
	if (recContact.getRowCount()>0)
	{
		if(recContact.getDataRef(0,"BCNT_PICTURE").toByteArray().size()==0)
		{
			img.load(":Icon_Person128.png");
		}
		else
		{
			img.loadFromData(recContact.getDataRef(0,"BCNT_PICTURE").toByteArray());
		}
	}
	else
	{
		img.load(":Icon_Person128.png");
	}

	QImage image(img.alphaChannel());
	img.setAlphaChannel(image);
	pPicFlow->clear();
	pPicFlow->addSlide(img);
	pPicFlow->setCenterIndex(0);
	pPicFlow->show();

}

void MobileHelper::PictureFlow_SetContactsPictures(PictureFlow *pPicFlow, DbRecordSet &recContact, int nCenterIndex)
{
	pPicFlow->clear();
	int nRowCount = recContact.getRowCount();
	for(int i = 0; i < nRowCount; ++i)
	{
		QImage img;
		if(recContact.getDataRef(i,"BCNT_PICTURE").toByteArray().size()==0)
		{
			img.load(":Icon_Person128.png");
		}
		else
		{
			img.loadFromData(recContact.getDataRef(i,"BCNT_PICTURE").toByteArray());
		}
		pPicFlow->addSlide(img);
	}

	pPicFlow->setCenterIndex(nCenterIndex);
	pPicFlow->show();
}
*/
bool MobileHelper::IsScreenInPortraitMode()
{
	int nScreen = QApplication::desktop()->primaryScreen();
	int nHeight = QApplication::desktop()->screenGeometry(nScreen).height();
	int nWidth  = QApplication::desktop()->screenGeometry(nScreen).width();

	if (nHeight>nWidth)
		return true;
	else
		return false;
}
