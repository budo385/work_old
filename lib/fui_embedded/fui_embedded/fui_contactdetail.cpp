#include "fui_contactdetail.h"
#include "gui_core/gui_core/thememanager.h"
#include "db_core/db_core/dbsqltableview.h"
#include <QHeaderView>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/picturehelper.h"
#include <QDesktopServices>
#include <QMessageBox>
#include <QGroupBox>
#include <QScrollArea>
#include "email_menuwidget.h"
#include "fui_voicecall.h"
#include "mobilehelper.h"

#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "fuimanager.h"
extern FuiManager *g_FuiManager;


#define LABEL_DATA   "<html><body style=\" font-family:Arial; text-decoration:none;\">\
					 <span style=\" font-size:8pt; font-style: italic; font-weight:800; color:white;\">%1</span><br>\
					 <span style=\" font-size:8pt; font-style: normal; font-weight:600; color:white;\">%2</span>\
					 </body></html>"
#define ROW_HEIGHT  40


FUI_ContactDetail::FUI_ContactDetail(QString strFuiName, QWidget *parent)
	: FuiBase(strFuiName, parent)
{
	ui.setupUi(this);
	m_nEntityRecordID=-1;

	m_bTabAccessCreated=false;
	m_bTabAddressCreated=false;
	m_bTabDescCreated=false;
	m_bTabPhoneCreated=false;
	m_bTabEmailCreated=false;
	m_bTabInternetCreated=false;
	m_bTabPicCreated=false;

	m_RowContactData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

	
	QSize buttonSize(30, 30);
	QSize buttonSizeMax(30, 30);
	m_pBtnDesc		=	new StyledPushButton(this,":Mobile_Det_Description.png",":Mobile_Det_Description.png","","",0,0);
	m_pBtnPhone		=	new StyledPushButton(this,":Mobile_Det_Phone.png",":Mobile_Det_Phone.png","","",0,0);
	m_pBtnEmail		=	new StyledPushButton(this,":Mobile_Det_Email.png",":Mobile_Det_Email.png","","",0,0);
	m_pBtnAddress	=	new StyledPushButton(this,":Mobile_Det_Address.png",":Mobile_Det_Address.png","","",0,0);
	m_pBtnInternet	=	new StyledPushButton(this,":Mobile_Det_Internet.png",":Mobile_Det_Internet.png","","",0,0);
	m_pBtnAccess	=	new StyledPushButton(this,":Mobile_Det_Access.png",":Mobile_Det_Access.png","","",0,0);
	m_pBtnPic		=	new StyledPushButton(this,":Icon_Person24.png",":Icon_Person24.png","","",0,0);

	//connect signals
	connect(m_pBtnDesc,SIGNAL(clicked()),this,SLOT(OnDesc()));
	connect(m_pBtnPhone,SIGNAL(clicked()),this,SLOT(OnPhone()));
	connect(m_pBtnEmail,SIGNAL(clicked()),this,SLOT(OnEmail()));
	connect(m_pBtnAddress,SIGNAL(clicked()),this,SLOT(OnAddress()));
	connect(m_pBtnInternet,SIGNAL(clicked()),this,SLOT(OnInternet()));
	connect(m_pBtnAccess,SIGNAL(clicked()),this,SLOT(OnAccess()));
	connect(m_pBtnPic,SIGNAL(clicked()),this,SLOT(OnPic()));

	m_pBtnDesc->setToolTip(tr("Description"));
	m_pBtnPhone->setToolTip(tr("Phones"));
	m_pBtnEmail->setToolTip(tr("eMail"));
	m_pBtnAddress->setToolTip(tr("Addresses"));
	m_pBtnInternet->setToolTip(tr("Internet"));
	m_pBtnAccess->setToolTip(tr("Details"));
	m_pBtnPic->setToolTip(tr("Picture"));

	//size
	m_pBtnDesc		->setMaximumSize(buttonSizeMax);
	m_pBtnDesc		->setMinimumSize(buttonSize);
	m_pBtnPhone		->setMaximumSize(buttonSizeMax);
	m_pBtnPhone		->setMinimumSize(buttonSize);
	m_pBtnEmail		->setMaximumSize(buttonSizeMax);
	m_pBtnEmail		->setMinimumSize(buttonSize);
	m_pBtnAddress	->setMaximumSize(buttonSizeMax);
	m_pBtnAddress	->setMinimumSize(buttonSize);
	m_pBtnInternet	->setMaximumSize(buttonSizeMax);
	m_pBtnInternet	->setMinimumSize(buttonSize);
	m_pBtnAccess	->setMaximumSize(buttonSizeMax);
	m_pBtnAccess	->setMinimumSize(buttonSize);
	m_pBtnPic		->setMaximumSize(buttonSizeMax);
	m_pBtnPic		->setMinimumSize(buttonSize);


	//Tool m_ProgressBar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnDesc);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnPhone);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnEmail);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnAddress);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnInternet);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnAccess);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnPic);
	buttonLayout->addStretch(1);
	buttonLayout->setContentsMargins(0,0,0,0);
	buttonLayout->setSpacing(0);

	ui.frameDetailToolBar->setLayout(buttonLayout);
	ui.frameDetailToolBar->setStyleSheet("QFrame#frameDetailToolBar "+ThemeManager::GetSidebarChapter_Bkg());

	setStyleSheet(ThemeManager::GetGlobalWidgetStyle());

	OnDesc();

}

FUI_ContactDetail::~FUI_ContactDetail()
{

}

void FUI_ContactDetail::on_selectionChange(int nEntityRecordID)
{
	//QTime time;
	//time.start();
	//qDebug()<<"start load"<<time.elapsed();

	if (m_nEntityRecordID==nEntityRecordID)
		return;
	m_nEntityRecordID=nEntityRecordID;
	
	if (nEntityRecordID>0)
	{
		//read from proxy-> either cache or server
		Status err;
		ClientContactManager::GetContactData(err,nEntityRecordID,m_RowContactData,!g_pClientManager->IsLogged());
		if (!err.IsOK())return;
	}

	//qDebug()<<"load"<<time.elapsed();
	//time.start();
	ui.frameName->RefreshActualName(m_RowContactData);
	//qDebug()<<"name"<<time.elapsed();
	
	//time.start();
	
	TabDesc_Refresh();
	TabEmail_Refresh();
	TabPhone_Refresh();
	TabInternet_Refresh();
	TabAddress_Refresh();
	TabAccess_Refresh();
	TabPic_Refresh();
	//qDebug()<<"refresh"<<time.elapsed();
	SetDetailMode(CONTACT_DETAIL_ALL); //if previous was hidden...

}


void FUI_ContactDetail::SetDetailMode(int nMode)
{

	if (nMode!=CONTACT_DETAIL_ALL)
		ui.frameDetailToolBar->setVisible(false);
	else
	{
		ui.frameDetailToolBar->setVisible(true);
		return;
	}

	switch(nMode)
	{
	case CONTACT_DETAIL_DESC:
		OnDesc();
		break;
	case CONTACT_DETAIL_PHONE:
		OnPhone();
		break;
	case CONTACT_DETAIL_EMAIL:
		OnEmail();
	    break;
	case CONTACT_DETAIL_INTERNET:
		OnInternet();
	    break;
	case CONTACT_DETAIL_ACCESS:
		OnAccess();
		break;
	case CONTACT_DETAIL_PIC:
		OnPic();
		break;
	default:
		OnDesc();
	    break;
	}


}


void FUI_ContactDetail::OnDesc()
{
	TabDesc_Create();
	TabDesc_Refresh();

	m_pBtnDesc->setIcon(":Mobile_Det_Description2_Foc.png");
	ui.stackedWidget->setCurrentIndex(CONTACT_DETAIL_DESC);
	m_pBtnPhone->setIcon(":Mobile_Det_Phone.png");
	m_pBtnEmail->setIcon(":Mobile_Det_Email.png");
	m_pBtnAddress->setIcon(":Mobile_Det_Address.png");
	m_pBtnInternet->setIcon(":Mobile_Det_Internet.png");
	m_pBtnAccess->setIcon(":Mobile_Det_Access.png");
	m_pBtnPic->setChecked(false);
}
void FUI_ContactDetail::OnPhone()
{
	TabPhone_Create();
	TabPhone_Refresh();

	m_pBtnDesc->setIcon(":Mobile_Det_Description.png");
	ui.stackedWidget->setCurrentIndex(CONTACT_DETAIL_PHONE);
	m_pBtnPhone->setIcon(":Mobile_Det_Phone2_Foc.png");
	m_pBtnEmail->setIcon(":Mobile_Det_Email.png");
	m_pBtnAddress->setIcon(":Mobile_Det_Address.png");
	m_pBtnInternet->setIcon(":Mobile_Det_Internet.png");
	m_pBtnAccess->setIcon(":Mobile_Det_Access.png");
	m_pBtnPic->setChecked(false);


}
void FUI_ContactDetail::OnEmail()
{
	TabEmail_Create();
	TabEmail_Refresh();

	m_pBtnDesc->setIcon(":Mobile_Det_Description.png");
	ui.stackedWidget->setCurrentIndex(CONTACT_DETAIL_EMAIL);
	m_pBtnPhone->setIcon(":Mobile_Det_Phone.png");
	m_pBtnEmail->setIcon(":Mobile_Det_Email2_Foc.png");
	m_pBtnAddress->setIcon(":Mobile_Det_Address.png");
	m_pBtnInternet->setIcon(":Mobile_Det_Internet.png");
	m_pBtnAccess->setIcon(":Mobile_Det_Access.png");
	m_pBtnPic->setChecked(false);
}
void FUI_ContactDetail::OnAddress()
{
	TabAddress_Create();
	TabAddress_Refresh();

	m_pBtnDesc->setIcon(":Mobile_Det_Description.png");
	ui.stackedWidget->setCurrentIndex(CONTACT_DETAIL_ADDRESS);
	m_pBtnPhone->setIcon(":Mobile_Det_Phone.png");
	m_pBtnEmail->setIcon(":Mobile_Det_Email.png");
	m_pBtnAddress->setIcon(":Mobile_Det_Address2_Foc.png");
	m_pBtnInternet->setIcon(":Mobile_Det_Internet.png");
	m_pBtnAccess->setIcon(":Mobile_Det_Access.png");
	m_pBtnPic->setChecked(false);
}


void FUI_ContactDetail::OnInternet()
{
	TabInternet_Create();
	TabInternet_Refresh();

	m_pBtnDesc->setIcon(":Mobile_Det_Description.png");
	ui.stackedWidget->setCurrentIndex(CONTACT_DETAIL_INTERNET);
	m_pBtnPhone->setIcon(":Mobile_Det_Phone.png");
	m_pBtnEmail->setIcon(":Mobile_Det_Email.png");
	m_pBtnAddress->setIcon(":Mobile_Det_Address.png");
	m_pBtnInternet->setIcon(":Mobile_Det_Internet2_Foc.png");
	m_pBtnAccess->setIcon(":Mobile_Det_Access.png");
	m_pBtnPic->setChecked(false);
}
void FUI_ContactDetail::OnAccess()
{
	qDebug() << "ulaz u OnAccess()";
	TabAccess_Create();
	TabAccess_Refresh();
	
	m_pBtnDesc->setIcon(":Mobile_Det_Description.png");
	ui.stackedWidget->setCurrentIndex(CONTACT_DETAIL_ACCESS);
	m_pBtnPhone->setIcon(":Mobile_Det_Phone.png");
	m_pBtnEmail->setIcon(":Mobile_Det_Email.png");
	m_pBtnAddress->setIcon(":Mobile_Det_Address.png");
	m_pBtnInternet->setIcon(":Mobile_Det_Internet.png");
	m_pBtnAccess->setIcon(":Mobile_Det_Access2_Foc.png");
	m_pBtnPic->setChecked(false);
	qDebug() << "izlaz iz OnAccess()";

}
void FUI_ContactDetail::OnPic()
{
	TabPic_Create();
	TabPic_Refresh();

	m_pBtnDesc->setIcon(":Mobile_Det_Description.png");
	ui.stackedWidget->setCurrentIndex(CONTACT_DETAIL_PIC);
	m_pBtnPhone->setIcon(":Mobile_Det_Phone.png");
	m_pBtnEmail->setIcon(":Mobile_Det_Email.png");
	m_pBtnAddress->setIcon(":Mobile_Det_Address.png");
	m_pBtnInternet->setIcon(":Mobile_Det_Internet.png");
	m_pBtnAccess->setIcon(":Mobile_Det_Access.png");
	m_pBtnPic->setChecked(true);

}







void FUI_ContactDetail::OnAddress_currentIndexChanged(int nIndex)
{
	if (nIndex>=0)
	{
		QString strAddress= m_cmbInfo_Address->itemData(nIndex).toString();
		m_txtInfo_Address->setPlainText(strAddress);
	}
	else
	{
		m_txtInfo_Address->setPlainText("");
	}

}

void FUI_ContactDetail::tablePhones_itemClicked ( int nRow, int column )
{
	QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

	DbRecordSet lstPhone=m_RowContactData.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
	if (nRow>=0 && nRow<lstPhone.getRowCount())
	{
		QString strPhone=lstPhone.getDataRef(nRow,"BCMP_SEARCH").toString();
		//open FUI
		QWidget* pFUIWidget=g_FuiManager->OpenFUI(VOICE_CALL,FuiBase::MODE_EMPTY,-1,false);
		FUI_VoiceCall *pFUI=dynamic_cast<FUI_VoiceCall*>(pFUIWidget);
		if (pFUI)
		{
			pFUI->Initialize(m_RowContactData,strPhone);
		}
		if (pFUIWidget)pFUIWidget->show();
		if (!ui.frameDetailToolBar->isVisible()) //mode not all, close fui
			close();

	}

}
void FUI_ContactDetail::tableEmails_itemClicked (int nRow, int column)
{
	QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

	DbRecordSet lstEmail=m_RowContactData.getDataRef(0,"LST_EMAIL").value<DbRecordSet>();
	if (nRow>=0 && nRow<lstEmail.getRowCount())
	{
		QString strEmail=lstEmail.getDataRef(nRow,"BCME_ADDRESS").toString();
		//open mail manager
		//QString strMailTo = "mailto:"+strEmail;
		//QDesktopServices::openUrl(QUrl(strMailTo));

		//open FUI
		QWidget* pFUIWidget=g_FuiManager->OpenFUI(EMAIL_TEMPLATES,FuiBase::MODE_INSERT,-1,false);
		Email_MenuWidget *pFUI=dynamic_cast<Email_MenuWidget*>(pFUIWidget);
		if (pFUI)
		{
			pFUI->SetDefaults(&m_RowContactData);
			pFUI->SetDefaultMail(strEmail);
		}
		if (pFUIWidget)pFUIWidget->show();
		if (!ui.frameDetailToolBar->isVisible()) //mode not all, close fui
			close();
	}

}
void FUI_ContactDetail::tableInternet_itemClicked (int nRow, int column )
{
	QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

	DbRecordSet lstInternet=m_RowContactData.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
	if (nRow>=0 && nRow<lstInternet.getRowCount())
	{
		QString strInternet=lstInternet.getDataRef(nRow,"BCMI_ADDRESS").toString();
		QDesktopServices::openUrl(strInternet.trimmed());
		if (!ui.frameDetailToolBar->isVisible()) //mode not all, close fui
			close();
	}
}




void FUI_ContactDetail::TabAccess_Create()
{
	qDebug() << "ulaz u TabAccess_Create()";
	if (m_bTabAccessCreated) return;
	m_bTabAccessCreated=true;

	QHBoxLayout *hbox=new QHBoxLayout;
	hbox->setSpacing(1);
	hbox->setMargin(1);
	m_pbtnPrivate = new QRadioButton(tr("Private Contact"));
	m_pbtnPublicRead = new QRadioButton(tr("Others can view"));
	m_pbtnPublicWrite = new QRadioButton(tr("Others can write"));
	m_pbtnPrivate->setStyleSheet("QRadioButton {color:white}");
	m_pbtnPublicRead->setStyleSheet("QRadioButton {color:white}");
	m_pbtnPublicWrite->setStyleSheet("QRadioButton {color:white}");
	m_pbtnPrivate->setEnabled(false);
	m_pbtnPublicRead->setEnabled(false);
	m_pbtnPublicWrite->setEnabled(false);

	hbox->addWidget(m_pbtnPrivate);
	hbox->addWidget(m_pbtnPublicRead);
	hbox->addWidget(m_pbtnPublicWrite);

	QLabel *p1= new QLabel(tr("Department:"));
	QLabel *p2= new QLabel(tr("Function:"));
	QLabel *p3= new QLabel(tr("Profession:"));
	QLabel *p4= new QLabel(tr("Owner:"));
	QLabel *p5= new QLabel(tr("Birthday:"));
	m_pDepartment= new Selection_ACP;
	m_pFunct= new Selection_ACP;
	m_pProfession= new Selection_ACP;
	m_pOwner= new Selection_SAPNE_FUI;
	m_pBirthday = new DateTimeEditEx;

	m_pDepartment->Initialize(ENTITY_CONTACT_DEPARTMENT, &m_RowContactData, "BCNT_DEPARTMENTNAME",false);
	m_pFunct->Initialize(ENTITY_CONTACT_FUNCTION, &m_RowContactData, "BCNT_FUNCTION",false);
	m_pProfession->Initialize(ENTITY_CONTACT_PROFESSION, &m_RowContactData, "BCNT_PROFESSION",false);
	m_pOwner->Initialize(ENTITY_BUS_PERSON, &m_RowContactData,"BCNT_OWNER_ID");
	m_pDepartment->SetEditMode(false);
	m_pFunct->SetEditMode(false);
	m_pProfession->SetEditMode(false);
	m_pOwner->SetEditMode(false);
	m_pBirthday->setEnabled(false);
	m_pOwner->GetButton(Selection_SAPNE::BUTTON_ADD)->setVisible(false);
	m_pOwner->GetButton(Selection_SAPNE::BUTTON_MODIFY)->setVisible(false);
	m_pOwner->GetButton(Selection_SAPNE::BUTTON_VIEW)->setVisible(false);


	//birthday
	QHBoxLayout *hbox1=new QHBoxLayout;
	hbox1->setSpacing(5);
	hbox1->setMargin(0);
	hbox1->addWidget(p5);
	hbox1->addWidget(m_pBirthday);
	hbox1->addStretch(1);

	QVBoxLayout *vbox=new QVBoxLayout;
	vbox->setSpacing(0);
	vbox->setMargin(0);
	ui.stackedWidget->widget(CONTACT_DETAIL_ACCESS)->setLayout(vbox);
	
	QFrame *frame=new QFrame;
	frame->setObjectName("x1");
	QScrollArea* scroll=new QScrollArea;
	scroll->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	scroll->setBackgroundRole(QPalette::Dark);
	scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	scroll->setWidget(frame);
	scroll->setWidgetResizable(true);
	scroll->setVisible(false);

	QVBoxLayout *vboxAll= new QVBoxLayout;
	vboxAll->setSpacing(2);
	vboxAll->setContentsMargins(2,2,2,2);
	frame->setLayout(vboxAll);
	vboxAll->addLayout(hbox);
	vboxAll->addLayout(hbox1);
	vboxAll->addWidget(p1);
	vboxAll->addWidget(m_pDepartment);
	vboxAll->addWidget(p2);
	vboxAll->addWidget(m_pFunct);
	vboxAll->addWidget(p3);
	vboxAll->addWidget(m_pProfession);
	vboxAll->addWidget(p4);
	vboxAll->addWidget(m_pOwner);
	vboxAll->addStretch(1);
	vbox->addWidget(scroll);
	scroll->setVisible(true);

	ui.stackedWidget->widget(CONTACT_DETAIL_ACCESS)->setStyleSheet("QFrame#x1 "+ThemeManager::GetMobileBkg());

	qDebug() << "izlaz iz TabAccess_Create()";
}

void FUI_ContactDetail::TabAccess_Refresh()
{
	qDebug() << "ulaz u TabAccess_Refresh()";
	
	if (!m_bTabAccessCreated) return;

	qDebug() << "1 u TabAccess_Refresh()";
	//-------------------------------------
	//		DETAILS:
	//-------------------------------------
	
	m_pDepartment->RefreshDisplay();
	qDebug() << "3 u TabAccess_Refresh()";
	m_pFunct->RefreshDisplay();
	qDebug() << "4 u TabAccess_Refresh()";
	m_pProfession->RefreshDisplay();
	qDebug() << "5 u TabAccess_Refresh()";
	m_pOwner->RefreshDisplay();

	qDebug() << "6 u TabAccess_Refresh()";

	if (m_RowContactData.getRowCount()>0)
		m_pBirthday->setDate(m_RowContactData.getDataRef(0,"BCNT_BIRTHDAY").toDate());
	else
		m_pBirthday->setDate(QDate());

	qDebug() << "7 u TabAccess_Refresh()";
	
	if (m_RowContactData.getRowCount()>0)
	{
		qDebug() << "8 u TabAccess_Refresh()";
	switch(m_RowContactData.getDataRef(0,"BCNT_ACCESS").toInt())
	{
		qDebug() << "8.0 u TabAccess_Refresh()";
	case 0:
		qDebug() << "8.1 u TabAccess_Refresh()";
		m_pbtnPrivate->setChecked(true);
	break;
	case 1:
		qDebug() << "8.2 u TabAccess_Refresh()";
		m_pbtnPublicRead->setChecked(true);
	break;
	case 2:
		qDebug() << "8.3 u TabAccess_Refresh()";
		m_pbtnPublicWrite->setChecked(true);;
	break;
	}
	qDebug() << "8.4 u TabAccess_Refresh()";
	}
	else
	{
		qDebug() << "9 u TabAccess_Refresh()";

		m_pbtnPrivate->setChecked(false);
		m_pbtnPublicRead->setChecked(false);
		m_pbtnPublicWrite->setChecked(false);;
		
		qDebug() << "10 u TabAccess_Refresh()";
	}
	qDebug() << "izlaz iz TabAccess_Refresh()";
}
void FUI_ContactDetail::TabPic_Create()
{
	if (m_bTabPicCreated) return;
	m_bTabPicCreated=true;
	MobileHelper::SelectedContactImageFrame_Create(ui.stackedWidget->widget(CONTACT_DETAIL_PIC),&m_pPic);
}
void FUI_ContactDetail::TabPic_Refresh()
{
	//refresh toolbar icoN:
	if (m_RowContactData.getRowCount()>0)
	{
		if(m_RowContactData.getDataRef(0,"BCNT_PICTURE").toByteArray().size()==0)
		{
			m_pBtnPic->setIcon(":Icon_Person24.png");
		}
		else
		{
			QPixmap pix;
			pix.loadFromData(m_RowContactData.getDataRef(0,"BCNT_PICTURE").toByteArray());
			m_pBtnPic->setIcon(pix.scaled(24,24));
		}
	}
	
	if (!m_bTabPicCreated) 
		return;

	MobileHelper::SelectedContactImageFrame_SetContactPicture(m_pPic,m_RowContactData);
}


void FUI_ContactDetail::TabDesc_Create()
{
	if (m_bTabDescCreated) return;
	m_bTabDescCreated=true;

	m_pTextDesc = new QTextEdit;
	m_pTextDesc->setReadOnly(true);
	QHBoxLayout *hbox1=new QHBoxLayout;
	hbox1->setSpacing(0);
	hbox1->setMargin(0);
	hbox1->addWidget(m_pTextDesc);
	ui.stackedWidget->widget(CONTACT_DETAIL_DESC)->setLayout(hbox1);

}
void FUI_ContactDetail::TabDesc_Refresh()
{
	if (!m_bTabDescCreated) return;
	m_pTextDesc->setHtml ("");

	if (m_RowContactData.getRowCount()==0)
		return;
	m_pTextDesc->setHtml (m_RowContactData.getDataRef(0,"BCNT_DESCRIPTION").toString());


}


void FUI_ContactDetail::TabPhone_Create()
{
	if (m_bTabPhoneCreated) return;
	m_bTabPhoneCreated=true;

	m_tablePhones = new QTableWidget;
	QHBoxLayout *hbox1=new QHBoxLayout;
	hbox1->setSpacing(0);
	hbox1->setMargin(0);
	hbox1->addWidget(m_tablePhones);
	ui.stackedWidget->widget(CONTACT_DETAIL_PHONE)->setLayout(hbox1);

	m_tablePhones->setColumnCount(1);
	m_tablePhones->horizontalHeader()->hide();
	m_tablePhones->verticalHeader()->hide();
	m_tablePhones->horizontalHeader()->stretchLastSection();
	m_tablePhones->verticalHeader()->setDefaultSectionSize(ROW_HEIGHT);
	m_tablePhones->setStyleSheet(ThemeManager::GetTableViewBkg());
	m_tablePhones->setColumnWidth(0,300);
	m_tablePhones->setShowGrid(false);

	connect(m_tablePhones,SIGNAL( cellClicked(int,int)),this,SLOT(tablePhones_itemClicked ( int,int)));


}
void FUI_ContactDetail::TabPhone_Refresh()
{
	if (!m_bTabPhoneCreated) return;

	m_tablePhones->clear();
	if (m_nEntityRecordID==-1)
		return;
	if (m_RowContactData.getRowCount()==0)
		return;

	DbRecordSet lstPhone;
	lstPhone=m_RowContactData.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
	ClientContactManager::PrepareListsWithTypeName(NULL,&lstPhone,NULL,NULL,NULL);


	//-------------------------------------
	//		PHONES
	//-------------------------------------

	int nSize=lstPhone.getRowCount();
	m_tablePhones->setRowCount(nSize);
	for(int i=0;i<nSize;++i)
	{
		QString strDisplay=QString(LABEL_DATA).arg(lstPhone.getDataRef(i,"BCMT_TYPE_NAME").toString()).arg(lstPhone.getDataRef(i,"BCMP_FULLNUMBER").toString());

		QWidget *pWidget=m_tablePhones->cellWidget(i,0);
		StyledPushButton *cell=NULL;
		if (!pWidget)
		{
			cell = new StyledPushButton(NULL,":Mobile_Sel_Phone.png",":Mobile_Sel_Phone.png","",strDisplay);
			m_tablePhones->setCellWidget(i,0,cell);
		}
		else
		{
			cell=dynamic_cast<StyledPushButton*>(pWidget);
			if (cell)
				cell->setText(strDisplay);
		}
	}
	int nRow=lstPhone.find(lstPhone.getColumnIdx("BCMP_IS_DEFAULT"),1,true);
	if (nRow!=-1)
	{
		m_tablePhones->setCurrentCell (nRow, 0, QItemSelectionModel::Select);
	}

}
void FUI_ContactDetail::TabEmail_Create()
{
	if (m_bTabEmailCreated) return;
	m_bTabEmailCreated=true;

	m_tableEmails = new QTableWidget;
	QHBoxLayout *hbox1=new QHBoxLayout;
	hbox1->setSpacing(0);
	hbox1->setMargin(0);
	hbox1->addWidget(m_tableEmails);
	ui.stackedWidget->widget(CONTACT_DETAIL_EMAIL)->setLayout(hbox1);

	m_tableEmails->setColumnCount(1);
	m_tableEmails->horizontalHeader()->hide();
	m_tableEmails->verticalHeader()->hide();
	m_tableEmails->horizontalHeader()->stretchLastSection();
	m_tableEmails->verticalHeader()->setDefaultSectionSize(ROW_HEIGHT);
	m_tableEmails->setStyleSheet(ThemeManager::GetTableViewBkg());
	m_tableEmails->setColumnWidth(0,300);
	m_tableEmails->setShowGrid(false);

	connect(m_tableEmails,SIGNAL( cellClicked(int,int)),this,SLOT(tableEmails_itemClicked ( int,int)));

}
void FUI_ContactDetail::TabEmail_Refresh()
{
	if (!m_bTabEmailCreated) return;

	m_tableEmails->clear();
	if (m_nEntityRecordID==-1)
		return;
	if (m_RowContactData.getRowCount()==0)
		return;

	DbRecordSet lstEmail;
	lstEmail=m_RowContactData.getDataRef(0,"LST_EMAIL").value<DbRecordSet>();
	ClientContactManager::PrepareListsWithTypeName(&lstEmail,NULL,NULL,NULL,NULL);

	//-------------------------------------
	//		EMAIL
	//-------------------------------------

	int nSize=lstEmail.getRowCount();
	m_tableEmails->setRowCount(nSize);
	for(int i=0;i<nSize;++i)
	{
		QString strDisplay=QString(LABEL_DATA).arg(lstEmail.getDataRef(i,"BCMT_TYPE_NAME").toString()).arg(lstEmail.getDataRef(i,"BCME_ADDRESS").toString());

		QWidget *pWidget=m_tableEmails->cellWidget(i,0);
		StyledPushButton *cell=NULL;
		if (!pWidget)
		{
			cell = new StyledPushButton(NULL,":Mobile_Sel_Email.png",":Mobile_Sel_Email.png","",strDisplay);
			m_tableEmails->setCellWidget(i,0,cell);
		}
		else
		{
			cell=dynamic_cast<StyledPushButton*>(pWidget);
			if (cell)
				cell->setText(strDisplay);
		}
	}
	int nRow=lstEmail.find(lstEmail.getColumnIdx("BCME_IS_DEFAULT"),1,true);
	if (nRow!=-1)
	{
		m_tableEmails->setCurrentCell (nRow, 0, QItemSelectionModel::Select);
	}

}

void FUI_ContactDetail::TabInternet_Create()
{
	if (m_bTabInternetCreated) return;
	m_bTabInternetCreated=true;

	m_tableInternet = new QTableWidget;
	QHBoxLayout *hbox1=new QHBoxLayout;
	hbox1->setSpacing(0);
	hbox1->setMargin(0);
	hbox1->addWidget(m_tableInternet);
	ui.stackedWidget->widget(CONTACT_DETAIL_INTERNET)->setLayout(hbox1);

	m_tableInternet->setColumnCount(1);
	m_tableInternet->horizontalHeader()->hide();
	m_tableInternet->verticalHeader()->hide();
	m_tableInternet->horizontalHeader()->stretchLastSection();
	m_tableInternet->verticalHeader()->setDefaultSectionSize(ROW_HEIGHT);
	m_tableInternet->setStyleSheet(ThemeManager::GetTableViewBkg());
	m_tableInternet->setColumnWidth(0,300);
	m_tableInternet->setShowGrid(false);

	connect(m_tableInternet,SIGNAL( cellClicked(int,int)),this,SLOT(tableInternet_itemClicked( int,int)));


}
void FUI_ContactDetail::TabInternet_Refresh()
{
	if (!m_bTabInternetCreated) return;

	m_tableInternet->clear();
	if (m_nEntityRecordID==-1)
		return;
	if (m_RowContactData.getRowCount()==0)
		return;

	DbRecordSet lstInternet;
	lstInternet=m_RowContactData.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
	ClientContactManager::PrepareListsWithTypeName(NULL,NULL,&lstInternet,NULL,NULL);


	//-------------------------------------
	//		INTERNET
	//-------------------------------------

	int nSize=lstInternet.getRowCount();
	m_tableInternet->setRowCount(nSize);
	for(int i=0;i<nSize;++i)
	{
		QString strDisplay=QString(LABEL_DATA).arg(lstInternet.getDataRef(i,"BCMT_TYPE_NAME").toString()).arg(lstInternet.getDataRef(i,"BCMI_ADDRESS").toString());

		QWidget *pWidget=m_tableInternet->cellWidget(i,0);
		StyledPushButton *cell=NULL;
		if (!pWidget)
		{
			cell = new StyledPushButton(NULL,":Mobile_Sel_Internet.png",":Mobile_Sel_Internet.png","",strDisplay);
			m_tableInternet->setCellWidget(i,0,cell);
		}
		else
		{
			cell=dynamic_cast<StyledPushButton*>(pWidget);
			if (cell)
				cell->setText(strDisplay);
		}
	}
	int nRow=lstInternet.find(lstInternet.getColumnIdx("BCMI_IS_DEFAULT"),1,true);
	if (nRow!=-1)
	{
		m_tableInternet->setCurrentCell (nRow, 0, QItemSelectionModel::Select);
	}

}
void FUI_ContactDetail::TabAddress_Create()
{
	if (m_bTabAddressCreated) return;
	m_bTabAddressCreated=true;

	m_lbl_Cnt_Address = new QLabel("(2)");
	m_lbl_Cnt_Address->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	m_lbl_Cnt_Address->setMinimumWidth(20);
	m_lbl_Cnt_Address->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred));
	m_cmbInfo_Address = new QComboBox;
	m_cmbInfo_Address->setMinimumHeight(20);
	m_cmbInfo_Address->setMaximumWidth(200);
	m_cmbInfo_Address->setSizePolicy(QSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed));
	//m_cmbInfo_Address->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed));
	m_txtInfo_Address = new QTextEdit;
	m_txtInfo_Address->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	m_txtInfo_Address->setReadOnly(true);

	QHBoxLayout *hbox1=new QHBoxLayout;
	hbox1->setSpacing(1);
	hbox1->setMargin(0);
	hbox1->addWidget(m_lbl_Cnt_Address);
	hbox1->addWidget(m_cmbInfo_Address);
	//hbox1->addStretch(1);

	QVBoxLayout *vbox1=new QVBoxLayout;
	vbox1->setSpacing(0);
	vbox1->setMargin(0);
	vbox1->addLayout(hbox1);
	vbox1->addWidget(m_txtInfo_Address);

	ui.stackedWidget->widget(CONTACT_DETAIL_ADDRESS)->setLayout(vbox1);
	connect(m_cmbInfo_Address,SIGNAL(currentIndexChanged(int)),this,SLOT(OnAddress_currentIndexChanged(int)));

}
void FUI_ContactDetail::TabAddress_Refresh()
{
	if (!m_bTabAddressCreated) return;

	m_cmbInfo_Address->clear();
	m_txtInfo_Address->setPlainText("");
	m_lbl_Cnt_Address->setText("");

	if (m_nEntityRecordID==-1)
		return;
	if (m_RowContactData.getRowCount()==0)
		return;

	DbRecordSet lstAddress;
	DbRecordSet lstAddressTypes;
	lstAddress=m_RowContactData.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
	ClientContactManager::PrepareListsWithTypeName(NULL,NULL,NULL,&lstAddress,&lstAddressTypes);


	//-------------------------------------
	//		ADDRESS
	//-------------------------------------
	QString strDisplay;
	m_cmbInfo_Address->blockSignals(true);

	int nSize=lstAddress.getRowCount();
	for(int i=0;i<nSize;++i)
	{

		if (!lstAddress.getDataRef(i,"BCMT_TYPE_NAME").toString().isEmpty())
			strDisplay=lstAddress.getDataRef(i,"BCMT_TYPE_NAME").toString()+" - ";
		else
			strDisplay="";

		if (!lstAddress.getDataRef(i,"BCMA_STREET").toString().isEmpty())
			strDisplay+=lstAddress.getDataRef(i,"BCMA_STREET").toString()+" / "+lstAddress.getDataRef(i,"BCMA_ZIP").toString()+" "+lstAddress.getDataRef(i,"BCMA_CITY").toString();
		else
			strDisplay+=lstAddress.getDataRef(i,"BCMA_ZIP").toString()+" "+lstAddress.getDataRef(i,"BCMA_CITY").toString();

		m_cmbInfo_Address->addItem(strDisplay,lstAddress.getDataRef(i,"BCMA_FORMATEDADDRESS")); //add display + proper email without type
	}

	int nRow=lstAddress.find(lstAddress.getColumnIdx("BCMA_IS_DEFAULT"),1,true);
	if (nRow!=-1)
		m_cmbInfo_Address->setCurrentIndex(nRow);

	OnAddress_currentIndexChanged(nRow);
	m_lbl_Cnt_Address->setText("("+QVariant(nSize).toString()+")");
	m_cmbInfo_Address->blockSignals(false);
}



void FUI_ContactDetail::OnOfflineModeChanged(bool bOffline)
{

}