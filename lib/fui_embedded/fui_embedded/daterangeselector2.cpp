#include "daterangeselector2.h"

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "gui_core/gui_core/thememanager.h"

#include <QHBoxLayout>
#include  <QMessageBox>

DateRangeSelector2::DateRangeSelector2(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);


	//Set geometry.
	//m_bSideBarMode = false;
	//if (ThemeManager::GetViewMode() == ThemeManager::VIEW_SIDEBAR)
		m_bSideBarMode = true;

	if (m_bSideBarMode)
	{
		
		int nHeight = 18;
		ui.label_4->hide();
		ui.label_5->hide();
		ui.dateFrom->setMaximumHeight(nHeight);
		ui.dateTo->setMaximumHeight(nHeight);
		ui.cboRangeSize->setMaximumHeight(nHeight);
		ui.btnCurDateRange->setMaximumHeight(22);
		ui.btnPrev->setMaximumHeight(22);
		ui.btnNext->setMaximumHeight(22);
		

		ui.btnCurDateRange->setStyleSheet("QPushButton { background: transparent; }");
		ui.btnPrev->setStyleSheet("QPushButton { background: transparent; }");
		ui.btnNext->setStyleSheet("QPushButton { background: transparent; }");
		
		QLabel *label = new QLabel("-");
		//setMinimumHeight(50);
		QHBoxLayout *horLayout = new QHBoxLayout();
		QHBoxLayout *horLayout1 = new QHBoxLayout();
		horLayout->setContentsMargins( 0, 0, 0, 0);
		horLayout1->setContentsMargins( 0, 0, 0, 0);
		
		horLayout->addWidget(ui.dateFrom);
		horLayout->addWidget(label);
		horLayout->addWidget(ui.dateTo);
		
		horLayout1->addWidget(ui.btnCurDateRange);
		horLayout1->addWidget(ui.btnPrev);
		horLayout1->addWidget(ui.cboRangeSize);
		horLayout1->addWidget(ui.btnNext);
		horLayout1->setSpacing(1);
		
		QVBoxLayout *verLayout = new QVBoxLayout;
		verLayout->setContentsMargins( 0, 0, 0, 0);
		verLayout->setSpacing(1);
		verLayout->addLayout(horLayout);
		verLayout->addLayout(horLayout1);

		setLayout(verLayout);
	}
	else
	{
		QHBoxLayout *horLayout = new QHBoxLayout();
		horLayout->setSpacing(4);
		horLayout->addWidget(ui.label_4);
		horLayout->addWidget(ui.dateFrom);
		horLayout->addWidget(ui.label_5);
		horLayout->addWidget(ui.dateTo);
		horLayout->addWidget(ui.btnCurDateRange);
		horLayout->addWidget(ui.btnPrev);
		horLayout->addWidget(ui.cboRangeSize);
		horLayout->addWidget(ui.btnNext);
		horLayout->setContentsMargins( 0, 0, 0, 0);
		setLayout(horLayout);
	}

	//Set icons.
	ui.btnCurDateRange->setText("");
	ui.btnPrev->setText("");
	ui.btnNext->setText("");
	ui.btnCurDateRange->setIcon(QIcon(":2downarrow.png"));
	ui.btnPrev->setIcon(QIcon(":1leftarrow.png"));
	ui.btnNext->setIcon(QIcon(":1rightarrow.png"));
	ui.btnCurDateRange->setFlat(true);
	ui.btnPrev->setFlat(true);
	ui.btnNext->setFlat(true);

	ui.cboRangeSize->addItem("-");
	ui.cboRangeSize->addItem(tr("Day"));
	ui.cboRangeSize->addItem(tr("Week"));
	ui.cboRangeSize->addItem(tr("Month"));
	ui.cboRangeSize->addItem(tr("Year"));
	ui.cboRangeSize->setCurrentIndex(0);

	ui.dateFrom->setDate(QDate::currentDate());
	ui.dateTo->setDate(QDate::currentDate());

	connect(ui.dateFrom,SIGNAL(dateChanged(const QDate)),this, SLOT(OnDateChanged(const QDate)));
	connect(ui.dateTo,SIGNAL(dateChanged(const QDate)),this, SLOT(OnDateChanged(const QDate)));
	connect(ui.cboRangeSize, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboChanged(int)));
}

DateRangeSelector2::~DateRangeSelector2()
{
}

void DateRangeSelector2::on_btnPrev_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nDiff		= dateFrom.daysTo(dateTo);

	int nRangeIdx = ui.cboRangeSize->currentIndex();
	switch(nRangeIdx){
		case 0:	// "-"
			dateFrom = dateFrom.addDays(-nDiff-1);
			dateTo = dateTo.addDays(-nDiff-1);
			break;
		case 1: // "Day"
			dateFrom = dateFrom.addDays(-1);
			dateTo = dateTo.addDays(-1);
			//dateTo = dateFrom; 
			//dateTo = dateTo.addDays(1);
			break;
		case 2: // "Week"
			{
			int nWeekday = dateFrom.dayOfWeek();
			dateFrom = dateFrom.addDays(-(7+nWeekday-1)); 
			dateTo = dateFrom; 
			dateTo = dateTo.addDays(6);
			}
			break;
		case 3: // "Month"
			{
			dateFrom = dateFrom.addMonths(-1);
			if(dateFrom.day() > 1)
				dateFrom = dateFrom.addDays(-(dateFrom.day()-1));
			dateTo = dateFrom;
			int nDaysInMonth = dateFrom.daysInMonth();
			dateTo = dateTo.addDays(nDaysInMonth-1);
			}
			break;
		case 4: // "Year" - move to the start of the prev year
			{
			int nYear = dateFrom.year() - 1;
			dateFrom = QDate(nYear, 1, 1);
			dateTo = QDate(nYear, 12, 31); 
			}
			break;
	}
	
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	emit onDateChanged(QDateTime(dateFrom, QTime(0,0,0)), QDateTime(dateTo, QTime(23,59,59)), nRangeIdx); 
}

void DateRangeSelector2::on_btnNext_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nDiff		= dateFrom.daysTo(dateTo);

	int nRangeIdx = ui.cboRangeSize->currentIndex();
	switch(nRangeIdx){
		case 0:	// "-"
			dateFrom = dateFrom.addDays(nDiff+1);
			dateTo = dateTo.addDays(nDiff+1);
			break;
		case 1: // "Day"
			dateFrom = dateFrom.addDays(1);
			//dateTo = dateFrom; 
			dateTo = dateTo.addDays(1);
			break;
		case 2: // "Week"
			{
			int nWeekday = dateFrom.dayOfWeek();
			dateFrom = dateFrom.addDays(7-nWeekday+1); 
			dateTo = dateFrom; 
			dateTo = dateTo.addDays(6);
			}
			break;
		case 3: // "Month"
			{
			dateFrom = dateFrom.addMonths(1);
			if(dateFrom.day() > 1)
				dateFrom = dateFrom.addDays(-(dateFrom.day()-1));
			dateTo = dateFrom;
			int nDaysInMonth = dateFrom.daysInMonth();
			dateTo = dateTo.addDays(nDaysInMonth-1);
			}
			break;
		case 4: // "Year" - move to the start of the prev year
			{
			int nYear = dateFrom.year() + 1;
			dateFrom = QDate(nYear, 1, 1);
			dateTo = QDate(nYear, 12, 31); 
			}
			break;
	}
	
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	emit onDateChanged(QDateTime(dateFrom, QTime(0,0,0)), QDateTime(dateTo, QTime(23,59,59)), nRangeIdx); 
}

void DateRangeSelector2::on_btnCurDateRange_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nDiff		= dateFrom.daysTo(dateTo);

	int nRangeIdx = ui.cboRangeSize->currentIndex();
	switch(nRangeIdx){
		case 0:	// "-" (preserve range ending on today)
			dateTo = QDate::currentDate();
			dateFrom = dateTo;
			dateFrom = dateFrom.addDays(-nDiff);
			break;
		case 1: // "Day"
			dateFrom = QDate::currentDate();
			dateTo = dateFrom; 
			break;
		case 2: // "Week" (current week)
			{
			dateFrom = QDate::currentDate();
			int nWeekday = dateFrom.dayOfWeek();
			dateFrom = dateFrom.addDays(-nWeekday+1); 
			dateTo = dateFrom; 
			dateTo = dateTo.addDays(6);
			}
			break;
		case 3: // "Month"
			{
			dateFrom = QDate::currentDate();
			if(dateFrom.day() > 1)
				dateFrom = dateFrom.addDays(-(dateFrom.day()-1));
			dateTo = dateFrom;
			int nDaysInMonth = dateFrom.daysInMonth();
			dateTo = dateTo.addDays(nDaysInMonth-1);
			}
			break;
		case 4: // "Year" - move to the start of the this year
			dateFrom = QDate::currentDate();
			dateFrom = QDate(dateFrom.year(), 1, 1);
			dateTo = QDate(dateFrom.year(), 12, 31); 
			break;
	}
	
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	emit onDateChanged(QDateTime(dateFrom, QTime(0,0,0)), QDateTime(dateTo, QTime(23,59,59)), nRangeIdx); 
}

void DateRangeSelector2::Initialize(QDateTime &dateFrom, QDateTime &dateTo, int nRange)
{
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.cboRangeSize->blockSignals(true);

	ui.cboRangeSize->setCurrentIndex(nRange);
	ui.dateFrom->setDate(dateFrom.date());
	ui.dateTo->setDate(dateTo.date());

	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);
	ui.cboRangeSize->blockSignals(false);
}

void DateRangeSelector2::OnDateChanged(const QDate &)
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int   nRange	= ui.cboRangeSize->currentIndex();
	
	emit onDateChanged(QDateTime(dateFrom, QTime(0,0,0)), QDateTime(dateTo, QTime(23,59,59)), nRange); 
}

void DateRangeSelector2::OnComboChanged(int nIdx)
{
	on_btnCurDateRange_clicked();
}
