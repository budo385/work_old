#include "fuimanager.h"

#include "fuicodeidlist.h"
#include "thememanager.h"

#include "contactfui.h"
//#include "contactdetails.h"
#include "fui_contactdetail.h"
#include "contactgrid.h"
#include "projectlistfui.h"
#include "fui_email.h"
#include "fui_favorites.h"
#include "email_menuwidget.h"
#include "fui_voicecall.h"
#include "styledpushbutton.h"
#include "fui_selectedfavorite.h"
#include <QCoreApplication>
#include "bus_client/bus_client/clientcontactmanager.h"
#include "os_specific/os_specific/tapi/TapiLine.h"
#include "mobilehelper.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager* g_pSettings;
#include "communicationmanager.h"
CommunicationManager g_CommManager;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "common/common/logger.h"
extern Logger					g_Logger;


#include <QMenuBar>

#define FIRST_FUI_TO_SHOW CONTACT_LIST

FuiManager::FuiManager(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags),m_bProcessingFatalError(false)
{
	ui.setupUi(this);
	
	setWindowTitle("SOKRATES");

	m_nRightFUIActionsFuiCode=-1;
	m_bFromOffline2OnlineInProgress=false;
	g_CommManager.Initialize();

	//Splash screen.
	m_pSplashScreen = new SplashScreen();
	m_pSplashScreen->setAttribute(Qt::WA_DeleteOnClose, true);
	m_pSplashScreen->hide();

	//showMaximized();
	showFullScreen();
 
	//Set left frame - name and date-time.
	QString styleLeftFrame = ThemeManager::GetMobileLeftHeaderBackground();
	QString styleMiddleFrame = ThemeManager::GetMobileCenterHeaderBackground();
	QString styleRightFrame = ThemeManager::GetMobileRightHeaderBackground();
	ui.left_frame->setStyleSheet(styleLeftFrame);
	ui.center_frame->setStyleSheet(styleMiddleFrame);
	ui.right_frame->setStyleSheet(styleRightFrame);
	
	//Login semaphore.
	QSize IconSize = QSize(25,25);
	m_pLoginButton = new StyledPushButton(this,":Offline.png",":Offline.png","");
	m_pLoginButton->setMinimumSize(IconSize);
	m_pLoginButton->setToolTip(tr("Online/Offline"));
	connect(m_pLoginButton,SIGNAL(clicked()),this,SLOT(OnLoginStatusClick()));

	//Sokrates label.
	QLabel *pSokratesLabel = new QLabel;
	pSokratesLabel->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:8pt; color:white; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">SOKRATES<sup>�</sup></p></body></html>"));
	m_pClock = new QLabel;

	//Sokrates and clock layout.
	QVBoxLayout *labelsLayout = new QVBoxLayout;
	labelsLayout->setContentsMargins(0,0,0,2);
	labelsLayout->setSpacing(0);
	labelsLayout->setAlignment(Qt::AlignLeft);
	labelsLayout->addSpacing(2);
	labelsLayout->addWidget(pSokratesLabel);
	labelsLayout->addWidget(m_pClock);
	labelsLayout->addSpacing(2);
	
	//Left layout
	QHBoxLayout *leftHeaderLayout = new QHBoxLayout;
	leftHeaderLayout->setContentsMargins(0,0,0,0);
	leftHeaderLayout->addWidget(m_pLoginButton);
	leftHeaderLayout->addSpacing(2);
	leftHeaderLayout->addLayout(labelsLayout);
	leftHeaderLayout->addStretch(1);
	ui.left_frame->setLayout(leftHeaderLayout);

	//Set right frame - main menu button.
	m_pMainMenuButton = new StyledPushButton(this, ":MainMenuIcon.png", ":MainMenuIcon.png", "");
	m_pMainMenuButton->setMinimumHeight(28);
	m_pMainMenuButton->setMaximumHeight(28);
	QHBoxLayout *rightHeaderLayout = new QHBoxLayout;
	rightHeaderLayout->setContentsMargins(0,0,0,0);
	rightHeaderLayout->addStretch(1);
	rightHeaderLayout->addWidget(m_pMainMenuButton);
	connect(m_pMainMenuButton, SIGNAL(clicked()), this, SLOT(OnMainMenuButton_clicked()));
	ui.right_frame->setLayout(rightHeaderLayout);

	CreateMenus();
	CreateFuiMenus();
	
	m_MainPicFlowMenu = NULL;
	m_nFuiIDCounter = 0;
	m_nCurrentFuiCode = -1;
	m_nFuiOrderCounter = 0;

	//Back action.
	m_pBackAction = new QAction(tr("Back"), NULL);
	connect(m_pBackAction, SIGNAL(triggered()), this, SLOT(GoBack()));

	//---------	-----------------INIT LOGIN CONTROLLER---------------------------------------
	connect(g_pClientManager, SIGNAL(ClientLogged()), this, SLOT(OnClientLogged()));
	connect(g_pClientManager, SIGNAL(ClientLogout()), this, SLOT(OnClientLoggOff()));
	connect(g_pClientManager, SIGNAL(ClientClosedLoginWnd()), this, SLOT(OnClientClosedLoginWnd()));
	connect(g_pClientManager, SIGNAL(ClientBeforeLogout()),this,SLOT(OnProcessBeforeLogout()));
	connect(g_pClientManager, SIGNAL(CommunicationFatalError(int)),this,SLOT(OnCommunicationFatalError(int)));
	connect(g_pClientManager, SIGNAL(LoginDialogCreated(QDialog *)),this,SLOT(OnLoginDialogCreated(QDialog *)));

	//Timer for date time in application header.
	m_nTimer=startTimer(1000); //start now then every min
/*
	g_pClientManager->Login();
	m_nCurrentFuiCode = FIRST_FUI_TO_SHOW;
*/
	//Offline:
	if (g_pClientManager->GetIniFile()->m_nMobileGoOfflineOnStartup)
	{
		GoOffline();
		return;
	}

	if (PIC_FLOW_MENU_ON)
	{
		OpenPicFlowMenu();
	}
	else
	{
		g_pClientManager->Login();
		m_nCurrentFuiCode = FIRST_FUI_TO_SHOW;
	}
}

FuiManager::~FuiManager()
{
	DestroyLayout(); //BT: destroy must be first!!!
	if (m_MainPicFlowMenu)
		delete(m_MainPicFlowMenu);
	if (g_pClientManager->IsLogged())
		g_pClientManager->Logout();
	if (m_pSplashScreen)
		m_pSplashScreen->close();
}

void FuiManager::Initialize()
{
}

QWidget* FuiManager::OpenFUI(int nFuiCode, int nMode /*= FuiBase::MODE_EMPTY*/, int nRecordID /*= -1*/,bool bShowVisible)
{
	_START_TIMER(open_fui);

	qDebug() << "ulaz u OpenFUI()";
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	qDebug() << "1 u OpenFUI()";

	m_pLoginButton->setIcon(":Busy_LED.png");
	//if (bShowVisible)
	//	QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents); //repaint

	FuiBase* pBase = NULL;
	QWidget *pFuiWnd = NULL;
	int nFuiID = m_hshFuiIDToFuiCode.key(nFuiCode, 0);

	//m_MainPicFlowMenu->hide();
	//this->showFullScreen();

	//If no widget created create it and set visible in main stacked widget.
	qDebug() << "2 u OpenFUI()";
	if (!nFuiID)
	{
		QTabWidget *pTabWnd = NULL;
		QString strTitle;
		int nTabCount = -1;

		//Create FUI widget.
		qDebug() << "3 u OpenFUI()";
		pFuiWnd = CreateFuiWidget(nFuiCode);
		qDebug() << "4 u OpenFUI()";
		pFuiWnd->setVisible(false);
	
		if(NULL == pFuiWnd)
		{
			QApplication::restoreOverrideCursor();
			return NULL;	// error - failed to create new window with the FUI
		}

		qDebug() << "5 u OpenFUI()";
		pBase = dynamic_cast<FuiBase*>(pFuiWnd);
		Q_ASSERT(pBase!=NULL);
		if (pBase==NULL)		//error - bot FUI Base!
		{
			QApplication::restoreOverrideCursor();
			return NULL;
		}

		//Add widget to main stack widget.
		qDebug() << "6 u OpenFUI()";
		int nStackIndex = AddStackedWidget(pFuiWnd);
		m_nFuiIDCounter++;

		qDebug() << "7 u OpenFUI()";
		nFuiID = m_nFuiIDCounter;

		//Fill all lists and hashes.
		m_lstFuiIDsList.append(nFuiID);
		m_hshFuiIDToFuiCode.insert(nFuiID, nFuiCode);
		m_hshFuiIDToStackIndex.insert(nFuiID, nStackIndex);
		m_hshFuiIDToFuiWidget.insert(nFuiID, pFuiWnd);
		
		//Set FUI menu action visible.
		SetWindowsActionVisible(nFuiCode);

		//Add fui to back button hash.
		SetFuiCodeToBackHash(nFuiCode);
	}
	else
	{
		pBase = dynamic_cast<FuiBase*>(m_hshFuiIDToFuiWidget.value(nFuiID));
	}

	//Remember current fui ID.
	m_nCurrentFuiCode = nFuiCode;

	SetCurrentWidget(m_hshFuiIDToFuiWidget.value(nFuiID));
	pBase->SetMode(nMode);

	//now set the required mode for the FUI window
	switch (nMode)
	{
	case FuiBase::MODE_READ:
		if(nRecordID >= 0)
			pBase->on_selectionChange(nRecordID);
		else
			break;	// do nothing
		break;
	case FuiBase::MODE_EDIT:
		break;	// do nothing
	case FuiBase::MODE_INSERT:
		break;	// do nothing
	case FuiBase::MODE_DELETE:
		break;	// do nothing
	case FuiBase::MODE_EMPTY:	
		break;	// do nothing
	default:
		Q_ASSERT(FALSE);	//mode not supported
	}

	//Set fui left soft key.
	SetFUILeftSoftKey(nFuiID);
	SetFUIRightSoftKey(nFuiID);

	//Set current widget visible.
	pFuiWnd=m_hshFuiIDToFuiWidget.value(nFuiID);
	pFuiWnd->setVisible(bShowVisible);
	//Change header state button.
	if (g_pClientManager->IsLogged())
		m_pLoginButton->setIcon(":Online.png");
	else
		m_pLoginButton->setIcon(":Offline.png");
	QApplication::restoreOverrideCursor();

	qDebug() << "izlaz iz OpenFUI()";
	_STOP_TIMER(open_fui);

	return pFuiWnd; // return unique ID
}

void FuiManager::CloseFUI(int nFuiID, bool bSkipWidgetDeletion)
{
	static bool bDeleteInProgress=false;
	if (bDeleteInProgress)
	{
		return;
	}
	bDeleteInProgress=true;

	QWidget *pWidget=m_hshFuiIDToFuiWidget.value(nFuiID,NULL);
	if (!pWidget) //widget alread removed = NULL
		return;

	if (m_hshFuiIDToFuiCode.value(nFuiID,-1)==m_nRightFUIActionsFuiCode && m_nRightFUIActionsFuiCode!=-1)
	{
		ClearFUIRightActions();
		m_nRightFUIActionsFuiCode=-1;
	}

	//Remove the widget from stacked widget.
	RemoveWidget(m_hshFuiIDToFuiWidget.value(nFuiID));

	//Remove menu action.
	SetWindowsActionInVisible(m_hshFuiIDToFuiCode.value(nFuiID));

	if (!bSkipWidgetDeletion)
	{
		//Delete the widget from memory.
		delete(m_hshFuiIDToFuiWidget.value(nFuiID));
	}

	//Remove it from back button hash.
	int nFuiCode = m_hshFuiIDToFuiCode.value(nFuiID);
	RemoveFuiCodeFromBackHash(nFuiCode);
	
	//Clean hashes.
	m_hshFuiIDToFuiCode.remove(nFuiID);
	m_hshFuiIDToStackIndex.remove(nFuiID);
	m_hshFuiIDToFuiWidget.remove(nFuiID);
	m_lstFuiIDsList.removeOne(nFuiID);

	bDeleteInProgress=false;
}

void FuiManager::CloseAllFUIWindows()
{
	foreach(int nFuiID, m_lstFuiIDsList)
	{
		CloseFUI(nFuiID);
	}
}

int FuiManager::GetFuiID(FuiBase *pWidget)
{
	return m_hshFuiIDToFuiWidget.key(pWidget);
}

int FuiManager::AddStackedWidget(QWidget *pFuiWnd)
{
	return ui.main_stackedWidget->addWidget(pFuiWnd);
}

void FuiManager::SetCurrentWidget(QWidget *pFuiWnd)
{
	ui.main_stackedWidget->setCurrentWidget(pFuiWnd);
}

void FuiManager::RemoveWidget(QWidget *pFuiWnd)
{
	ui.main_stackedWidget->removeWidget(pFuiWnd);
}

void FuiManager::SetWindowsActionVisible(int nFuiCodeID)
{
	//Set right soft menu fui action.
	m_hshFuiCodeIDToWinMenuAction.value(nFuiCodeID)->setVisible(true);
}

void FuiManager::SetWindowsActionInVisible(int nFuiCodeID)
{
	m_hshFuiCodeIDToWinMenuAction.value(nFuiCodeID)->setVisible(false);
}

void FuiManager::SetFUILeftSoftKey(int nFuiID)
{
	//Check does fui have his own left soft action.
	FuiBase *fui = dynamic_cast<FuiBase*>(m_hshFuiIDToFuiWidget.value(nFuiID));
	QAction *pLeftSoftKeyAction;
	if (fui)
		pLeftSoftKeyAction = fui->SetLeftSoftKeyAction();
	else
		pLeftSoftKeyAction = NULL;

	//Set back action if no action is defined from fui.
	if (!pLeftSoftKeyAction && GetPreviousFuiCode() > 0)
	{
		pLeftSoftKeyAction = m_pBackAction;
	}

	menuBar()->setDefaultAction(pLeftSoftKeyAction);
}

void FuiManager::SetFUIRightSoftKey(int nFuiID)
{
	//Check does fui have his own left soft action.
	FuiBase *fui = dynamic_cast<FuiBase*>(m_hshFuiIDToFuiWidget.value(nFuiID));
	QList<QAction *> lstRightFUIActions;
	if (fui)
		lstRightFUIActions = fui->SetRightSoftKeyAction();

	ClearFUIRightActions();

	if (lstRightFUIActions.size()>0)
	{
		m_nRightFUIActionsFuiCode=m_hshFuiIDToFuiCode.value(nFuiID,-1);
		m_lstRightFUIActions=lstRightFUIActions;
		QAction *pSep=menuBar()->addSeparator();
		int nSize=m_lstRightFUIActions.size();
		for (int i=0;i<nSize;i++)
		{
			m_lstRightFUIActions.at(i)->setVisible(true);
			menuBar()->addAction(m_lstRightFUIActions.at(i));
		}
		m_lstRightFUIActions.append(pSep);
	}
}

void FuiManager::ClearFUIRightActions()
{
	//clear previous
	int nSize=m_lstRightFUIActions.size();
	for (int i=0;i<nSize;i++)
		menuBar()->removeAction(m_lstRightFUIActions.at(i));
	m_lstRightFUIActions.clear();
}

int FuiManager::GetCurrentFuiID()
{
	QWidget *pCurrentWidget = ui.main_stackedWidget->currentWidget();
	int nFuiID = m_hshFuiIDToFuiWidget.key(pCurrentWidget);
	return nFuiID;
}

QWidget* FuiManager::OpenPicFlowMenu()
{
	if (m_MainPicFlowMenu)
	{
		m_MainPicFlowMenu->showFullScreen();
	}
	else
	{
		//m_MainPicFlowMenu = new PictureFlow();
		QStringList files;
		files << ":1_Desktop.png" << ":2_Contacts.png" << ":3_Favourites.png" << ":4_Projects.png" << ":5_Config.png";

		MobileHelper::PictureFlowWidget_Create(NULL, &m_MainPicFlowMenu, files, QSize(128,192), 2);
		/*
		QImage img;
		for(int i = 0; i < (int)files.count(); i++)
			if(img.load(files.value(i)))
			{
				QImage image(img.alphaChannel());
				img.setAlphaChannel(image);
				m_MainPicFlowMenu->addSlide(img);
			}

			m_MainPicFlowMenu->setCenterIndex(m_nCurrentPicFlowMenuItem);
			m_MainPicFlowMenu->setBackgroundColor(Qt::black);
			m_MainPicFlowMenu->showFullScreen();
			m_MainPicFlowMenu->setCenterIndex(2);
		
		*/
		
		m_MainPicFlowMenu->setBackground(QBrush(Qt::black));
		connect(m_MainPicFlowMenu, SIGNAL(centerIndexSelectedForEditing(int)), this, SLOT(OncenterIndexSelectedForEditing(int)));
		m_MainPicFlowMenu->showFullScreen();

		return m_MainPicFlowMenu;
	}
	return m_MainPicFlowMenu;
}

void FuiManager::CreateMenus()
{
	//Create menu actions.
	m_pActLogin= new QAction(tr("Login"),this);
	connect(m_pActLogin, SIGNAL(triggered()), this, SLOT(OnMenu_OnLogin()));

	m_pActLogout= new QAction(tr("Logout"),this);
	connect(m_pActLogout, SIGNAL(triggered()), this, SLOT(OnMenu_OnLogout()));

	m_pActQuit= new QAction(tr("Quit"),this);
	QKeySequence key(tr("Ctrl+Q"));
	m_pActQuit->setShortcut(key);
	connect(m_pActQuit, SIGNAL(triggered()), this, SLOT(close()));

	m_pActFUI_PersonalSettings= new QAction(tr("Personal Settings"),this);;
	connect(m_pActFUI_PersonalSettings, SIGNAL(triggered()), this, SLOT(OnMenu_PersonalSettings()));

	m_pActPurgeCache= new QAction(tr("Purge Cache Data"),this);
	connect(m_pActPurgeCache, SIGNAL(triggered()), this, SLOT(OnMenu_PurgeCache()));

	//Menu File.
	QMenu *MenuFile = new QMenu(tr("File"));
	MenuFile->addAction(m_pActLogin);
	MenuFile->addAction(m_pActLogout);
	MenuFile->addSeparator();
	MenuFile->addAction(m_pActFUI_PersonalSettings);
	MenuFile->addAction(m_pActPurgeCache);
	MenuFile->addSeparator();
	MenuFile->addAction(m_pActQuit);
	m_pMenuFile = menuBar()->addMenu(MenuFile);

}

void FuiManager::CreateFuiMenus()
{
	//Menu Windows.
	QMenu *pMenuWindows = new QMenu(tr("Windows"));
	m_pMenuWindows = menuBar()->addMenu(pMenuWindows);

	QAction *pAction = new QAction(tr("Contact List"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnContactList_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(CONTACT_LIST, pAction);
	
	pAction = new QAction(tr("Contact Details"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnContactDetails_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(CONTACT_DETAILS, pAction);
	
	pAction = new QAction(tr("Contact Data"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnContactGrid_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(CONTACT_GRID, pAction);
	
	pAction = new QAction(tr("Project List"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnProjectList_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(PROJECT_LIST, pAction);

	pAction = new QAction(tr("Send Email"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnEmailsend_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(EMAIL_SEND, pAction);

	pAction = new QAction(tr("Favorites List"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnFavoritesList_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(FAVORITES_FUI, pAction);

	pAction = new QAction(tr("Email Templates"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnEmailTemplates_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(EMAIL_TEMPLATES, pAction);

	pAction = new QAction(tr("Voice Call"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnVoiceCall_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(VOICE_CALL, pAction);

	pAction = new QAction(tr("Selected Contact"),m_pMenuWindows);
	pMenuWindows->addAction(pAction);
	pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnSelectedFavourite_triggered()));
	m_hshFuiCodeIDToWinMenuAction.insert(SELECTED_FAVORITE, pAction);
}

void FuiManager::SetupMenus()
{
	//IF NOT LOGGED: Allow File/Login/Quit, Windows.
	if (!g_pClientManager->IsLogged())
	{
		//FILE:
		m_pActFUI_PersonalSettings->setEnabled(false);
		m_pActPurgeCache->setEnabled(false);
		m_pMenuFile->setVisible(true);
		m_pMenuWindows->setVisible(true);
	}
	else
	{
		m_pActFUI_PersonalSettings->setEnabled(true);
		m_pActPurgeCache->setEnabled(true);
		m_pMenuFile->setVisible(true);
		m_pMenuWindows->setVisible(true);
	}
}

void FuiManager::UpdateLoginMenuState()
{
	if(g_pBoSet!=NULL)
	{
		m_pActLogin->setEnabled(!g_pClientManager->IsLogged());
		m_pActLogout->setEnabled(g_pClientManager->IsLogged());
	}
	else
	{
		m_pActLogin->setEnabled(true);
		m_pActLogout->setEnabled(false);
	}
}

bool FuiManager::DestroyLayout()
{
	CloseAllFUIWindows();
	return true;
}


/*
QWidget* FuiManager::GetFUIWidget(int nGID)
{

}

bool FuiManager::ActivateFUI(int nGID)
{

}
*/
QWidget* FuiManager::CreateFuiWidget(int nFuiCode)
{
	_START_TIMER(create_fui_widget);
	qDebug() << "ulaz u CreateFuiWidget()" << nFuiCode;
	switch (nFuiCode)
	{
	case CONTACT_LIST: //Contact FUI
		return new ContactFui(tr("Contact List"));
	case CONTACT_DETAILS: //Contact details FUI
		return new FUI_ContactDetail(tr("Contact Details"));
	case CONTACT_GRID: //Contact grid FUI
		return new ContactGrid(tr("Contact Data"));
	case PROJECT_LIST: //Project list FUI
		return new ProjectListFui(tr("Project List"));
	case EMAIL_SEND: //Project list FUI
		return new FUI_Email(tr("Send Email"));
	case FAVORITES_FUI: //Favorites list FUI
		return new Fui_Favorites(tr("Favorites List"));
	case EMAIL_TEMPLATES: //Favorites list FUI
		return new Email_MenuWidget(tr("Email Templates"));
	case VOICE_CALL: //Favorites list FUI
		return new FUI_VoiceCall(tr("Voice Call"));
	case SELECTED_FAVORITE: //Selected Favorites FUI
		return new Fui_SelectedFavorite(tr("Selected Contact"));
	}

	_STOP_TIMER(create_fui_widget);
	return NULL;
}

void FuiManager::UpdateClock()
{
	//every sec:
	QString strClock=QDateTime::currentDateTime().toString("HH:mm dd.MM.yy");
	SetClockText(strClock);
}

void FuiManager::SetFuiCodeToBackHash(int nFuiCode)
{
	if (m_lstBackButtonList.contains(nFuiCode))
		return;

	m_lstBackButtonList << nFuiCode;
	return;



	if (m_hshBackButtonHash.contains(m_hshBackButtonHash.key(nFuiCode)))
		return;

	m_nFuiOrderCounter++;
	m_hshBackButtonHash.insert(m_nFuiOrderCounter, nFuiCode);
}

void FuiManager::RemoveFuiCodeFromBackHash(int nFuiCode)
{
	m_lstBackButtonList.removeAll(nFuiCode);
	return;


	//Now remove nFuiCode and reorder back button hash.
	m_hshBackButtonHash.remove(m_hshBackButtonHash.key(nFuiCode));
	m_nFuiOrderCounter = 0;
	QHash<int, int> newOrderHash;
	QHashIterator<int, int> iter(m_hshBackButtonHash);
	while (iter.hasNext())
	{
		iter.next();
		m_nFuiOrderCounter++;
		newOrderHash.insert(m_nFuiOrderCounter, iter.value());
	}

	m_hshBackButtonHash.clear();
	m_hshBackButtonHash = newOrderHash;
}

int  FuiManager::GetPreviousFuiCode()
{
	int nFuiCode = -1;

	int nCurrentFuiCode = m_hshFuiIDToFuiCode.value(GetCurrentFuiID());

	QListIterator<int> iterator(m_lstBackButtonList);
	while (iterator.hasNext())
	{
		int nCode = iterator.next();
		if (nCode == nCurrentFuiCode)
		{
			if (iterator.hasNext())
			{
				nFuiCode = iterator.next();
				break;
			}
			else if (iterator.hasPrevious())
			{
				iterator.previous();
				nFuiCode = iterator.peekPrevious();
				break;
			}
			else
				break;
		}
	}

	return nFuiCode;


	//Find the fui code in hash. If there is a next value (means we have already pressed back button) take it.
	//Else look for previous value - if exit take it.
	//If none above return -1.
	QHashIterator<int, int> iter(m_hshBackButtonHash);
	while (iter.hasNext())
	{
		iter.next();
		int i = iter.value();
		if (iter.value() == m_nCurrentFuiCode)
		{
			if (iter.hasNext())
			{
				iter.next();
				nFuiCode = iter.value();
				return nFuiCode;
			}
			else if (iter.hasPrevious())
			{
				iter.previous();
				nFuiCode = iter.peekPrevious().value();
				return nFuiCode;
			}
			else
				return nFuiCode;
		}
	}

	return nFuiCode;
}

void FuiManager::SetClockText(QString strText)
{
	if (!m_pClock)return;

	m_pClock->setText(	QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
								<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:bold; color:white;\">%1</span></p>\
								</body></html>").arg(strText));

}

void FuiManager::timerEvent(QTimerEvent * event)
{
	if (event->timerId()==m_nTimer)
	{
		killTimer(m_nTimer);
		m_nTimer=startTimer(10000); //start now then every min
		UpdateClock();
	}
}

void FuiManager::OncenterIndexSelectedForEditing(int index)
{
	m_MainPicFlowMenu->hide();
	m_nCurrentPicFlowMenuItem = index;
	if (index == 1)
	{
		if (!g_pClientManager->IsLogged())
		{
			g_pClientManager->Login();
			m_nCurrentFuiCode = CONTACT_LIST;
		}
		else
			OpenFUI(CONTACT_LIST);
	}
	else if (index == 2)
	{
		if (!g_pClientManager->IsLogged())
		{
			g_pClientManager->Login();
			m_nCurrentFuiCode = FAVORITES_FUI;
		}
		else
			OpenFUI(FAVORITES_FUI);
	}
	else if (index == 3)
	{
		if (!g_pClientManager->IsLogged())
		{
			g_pClientManager->Login();
			m_nCurrentFuiCode = PROJECT_LIST;
		}
		else
			OpenFUI(PROJECT_LIST);
	}
}

void FuiManager::OnMenu_OnLogin()
{
	if(!DestroyLayout()) //will be recreated on login success
		return;

	if (g_pClientManager->IsLogged()) g_pClientManager->Logout(); //try logout
	g_pClientManager->Login();
	UpdateLoginMenuState();
}

void FuiManager::OnMenu_OnLogout()
{
	if(!DestroyLayout()) //will be recreated on login success
		return;

	QApplication::processEvents(QEventLoop::DeferredDeletion); //kill all windows while BO objects are alive

	if (g_pClientManager->IsLogged()) g_pClientManager->Logout(); //try logout
	UpdateLoginMenuState();

	QApplication::processEvents(QEventLoop::DeferredDeletion); //kill all windows while BO objects are alive

	if (PIC_FLOW_MENU_ON)
	{
		if (!g_pClientManager->IsLogged()) 
			g_pClientManager->Login(); //Try login.
			//OpenPicFlowMenu(); //Open picture flow menu.
	}
	else
	{
		if (!g_pClientManager->IsLogged()) 
			g_pClientManager->Login(); //Try login.
		m_nCurrentFuiCode = FAVORITES_FUI;
	}
}

void FuiManager::OnMenu_PersonalSettings()
{

}

void FuiManager::OnClientLogged()
{
	m_pLoginButton->setIcon(":Online.png");
	OnUserChangedTheme(g_pSettings->GetPersonSetting(APPEAR_THEME).toInt());

	if (!m_bFromOffline2OnlineInProgress)
		OpenFUI(m_nCurrentFuiCode);
	
	//Load hidden empty fui's.
	if (PIC_FLOW_MENU_ON)
	{
/*
		this->setVisible(false);
		m_pSplashScreen->showFullScreen();
		QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents); //repaint
		OpenFUI(CONTACT_DETAILS, FuiBase::MODE_EMPTY, -1, false);
		OpenFUI(CONTACT_GRID, FuiBase::MODE_EMPTY, -1, false);
		OpenFUI(CONTACT_LIST, FuiBase::MODE_EMPTY, -1, false);
		m_pSplashScreen->hide();
		this->setVisible(true);
*/
		OpenPicFlowMenu();
	}	
	else
		OpenFUI(FIRST_FUI_TO_SHOW);


	//Setup gui.
	SetupMenus();
	UpdateLoginMenuState();

	if (m_bFromOffline2OnlineInProgress)
	{
		m_bFromOffline2OnlineInProgress=false;
		int nSize=m_lstFuiIDsList.size(); //close all that are not offline enabled
		for (int i=nSize-1;i>=0;i--)
		{
			FuiBase *pFUI= dynamic_cast<FuiBase*>(m_hshFuiIDToFuiWidget.value(m_lstFuiIDsList.at(i),NULL));
			if (pFUI)
				pFUI->OnOfflineModeChanged(false);
		}
		g_pClientManager->GetIniFile()->m_nMobileGoOfflineOnStartup=0; //set as online
	}
}

void FuiManager::OnClientLoggOff()
{
	m_pLoginButton->setIcon(":Offline.png");
	//Set current fui id in case it's only temporary login.
	SetupMenus();
	UpdateLoginMenuState();
}

void FuiManager::OnClientClosedLoginWnd()
{
	SetupMenus();		//to be on safe side, disable all menu actions
	UpdateLoginMenuState();
	m_bFromOffline2OnlineInProgress=false;
}

void FuiManager::OnProcessBeforeLogout()
{
	g_CommManager.EndUserSession();		//save comm data
	Status err;
	ClientContactManager::Offline_SaveData(err);
	_CHK_ERR(err);

	//	QByteArray byteData;
	//	GUI_Helper::SaveWindowProperties(byteData); //save window data (assume that are all closed now)
	//	g_pSettings->SetPersonSetting(APPEAR_WINDOW_PROPERTIES,byteData);
	//	g_pSettings->SetPersonSetting(APPEAR_THEME,ThemeManager::GetCurrentThemeID());
	//	g_pSettings->SetPersonSetting(APPEAR_VIEW_MODE,ThemeManager::GetViewMode());
	//	g_pSettings->SetPersonSetting(SHOW_HEADER,m_pActBanner->isChecked());
}

void FuiManager::OnCommunicationFatalError(int nErrorCode)
{
	if (m_bProcessingFatalError) return;   //prevent to loop itself!!!-> just precaution, QTimer::singleshot fires two times sometimes...
	m_bProcessingFatalError=true;

	//m_EmailSubscribe.StopAll();
	//SkypeManager::ReleaseInstance();
	//MapiManager::UnIntiliazeMapiDll();

	g_CommManager.ClearData(); //stop email timers: prevent outlook from crash

	//filter out any events from queue:
	QCoreApplication::instance()->processEvents();

	CloseAllModalWindows();					//close all modals
	DestroyLayout();						//close all FUI's
	//ui.frameToolBar->setVisible(false);

	//if some1 uses hourglass when accessing BO, restore to normal:
	QApplication::changeOverrideCursor(Qt::ArrowCursor);

	//pop up massage
	QString strErrorTxt;
	StatusCodeSet::getErrorDetails(nErrorCode,strErrorTxt);
	if (strErrorTxt.isEmpty() || strErrorTxt==tr("Unknown Error!"))
	{
		strErrorTxt=tr("Connection to server is lost!");
	}

	strErrorTxt=tr("Session is terminated. Connection to server has been lost. Reason: ")+strErrorTxt;
	QMessageBox::critical(NULL, tr("Error Message"), strErrorTxt);
	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,strErrorTxt);

	Status err;
	//kill BO SET-> if any1 left in event queue this will crash application, in all tests, this wasn't happen yet!
	g_pClientManager->StopAfterFatalError(err,true); //clear all session data cached -> preserver main cache!
	if(!err.IsOK())	
	{
		//show error message
		QMessageBox::warning(NULL, tr("Error Message"), err.getErrorText());
	}

	//adjust GUI to start screen:
	SetupMenus();
	UpdateLoginMenuState();

	//MB requested: jump to offline mode:
	QTimer::singleShot(0,this,SLOT(GoOffline()));

}

void FuiManager::OnWinMenuActionTriggered(int nFuiCodeID)
{
	OpenFUI(nFuiCodeID);
}

void FuiManager::GoBack()
{
	OpenFUI(GetPreviousFuiCode());
}

void FuiManager::OnContactList_triggered()
{
	OnWinMenuActionTriggered(CONTACT_LIST);
}

void FuiManager::OnContactDetails_triggered()
{
	OnWinMenuActionTriggered(CONTACT_DETAILS);
}

void FuiManager::OnContactGrid_triggered()
{
	OnWinMenuActionTriggered(CONTACT_GRID);
}

void FuiManager::OnMainMenuButton_clicked()
{
	if (PIC_FLOW_MENU_ON)
		m_MainPicFlowMenu->showFullScreen();
}

void FuiManager::OnProjectList_triggered()
{
	OnWinMenuActionTriggered(PROJECT_LIST);
}

void FuiManager::OnEmailsend_triggered()
{
	OnWinMenuActionTriggered(EMAIL_SEND);
}

void FuiManager::OnFavoritesList_triggered()
{
	OnWinMenuActionTriggered(FAVORITES_FUI);
}

void FuiManager::OnEmailTemplates_triggered()
{
	OnWinMenuActionTriggered(EMAIL_TEMPLATES);
}

void FuiManager::OnVoiceCall_triggered()
{
	OnWinMenuActionTriggered(VOICE_CALL);
}

void FuiManager::OnSelectedFavourite_triggered()
{
	OnWinMenuActionTriggered(SELECTED_FAVORITE);
}



void FuiManager::OnUserChangedTheme(int nThemeID)
{
	if (ThemeManager::GetCurrentThemeID()==nThemeID)
		return;
	ThemeManager::SetCurrentThemeID(nThemeID);

	//Set left frame - name and date-time.
	QString styleLeftFrame = ThemeManager::GetMobileLeftHeaderBackground();
	QString styleMiddleFrame = ThemeManager::GetMobileCenterHeaderBackground();
	QString styleRightFrame = ThemeManager::GetMobileRightHeaderBackground();
	ui.left_frame->setStyleSheet(styleLeftFrame);
	ui.center_frame->setStyleSheet(styleMiddleFrame);
	ui.right_frame->setStyleSheet(styleRightFrame);
	
	g_pClientManager->GetIniFile()->m_nLastTheme=nThemeID;
	//g_pSettings->SetPersonSetting(APPEAR_THEME,nThemeID);
}


//called for offline:
// 1. on startup
// 2. anywhere in application -> press on green dot or server connection lost
// closes all fui's open, open favorites!
//notify all fui's change mode
//called for online:
// press on red dot: opens login dialog, notify all fui's change mode

bool FuiManager::GoOffline(bool bOffline, bool bWarn)
{

	if (bOffline) //from online to offline
	{
		if (bWarn)
		{
			int nResult=QMessageBox::question(this,tr("Confirmation"),tr("Do you wish to switch to the Offline mode?"),tr("Yes"),tr("No"));
			if (nResult!=0)
				return false; //only if YES
		}
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		if (g_pClientManager->IsLogged())
		{
			int nSize=m_lstFuiIDsList.size(); //close all that are not offline enabled
			for (int i=nSize-1;i>=0;i--)
			{
				FuiBase *pFUI= dynamic_cast<FuiBase*>(m_hshFuiIDToFuiWidget.value(m_lstFuiIDsList.at(i),NULL));
				if (pFUI)
					pFUI->OnOfflineModeChanged(true);
			}
			QApplication::processEvents(QEventLoop::DeferredDeletion); //kill all windows while BO objects are alive
			if (g_pClientManager->IsLogged()) g_pClientManager->Logout(); //try logout
			UpdateLoginMenuState();
			QApplication::processEvents(QEventLoop::DeferredDeletion); //kill all windows while BO objects are alive
		}

		//else //this means startup: load all data from cache
		//{
		//always reload as logout clears cache!
		Status err;
		ClientContactManager::Offline_LoadData(err);
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
		//}
		g_pClientManager->GetIniFile()->m_nMobileGoOfflineOnStartup=1; //set as offline

		//open favs if any, if not then open list:
		QString strFavs = g_pSettings->GetPersonSetting(CONTACT_FAVORITES).toString(); 
		if (strFavs.isEmpty())
			OpenFUI(CONTACT_LIST);
		else
			OpenFUI(FAVORITES_FUI);

		m_pActPurgeCache->setDisabled(true);
		QApplication::restoreOverrideCursor();
		return true;
	}

	if (!bOffline && !g_pClientManager->IsLogged()) //from offline to online
	{
		if (bWarn)
		{
			int nResult=QMessageBox::question(this,tr("Confirmation"),tr("Do you wish to switch to the Online mode?"),tr("Yes"),tr("No"));
			if (nResult!=0)
				return false; //only if YES
		}

		m_bFromOffline2OnlineInProgress=true;
		g_pClientManager->Login();
		return false;
	}
	return true;
}

void FuiManager::OnLoginStatusClick()
{
	GoOffline(g_pClientManager->IsLogged(),true);
}

void FuiManager::OnMenu_PurgeCache()
{
	g_ClientCache.PurgeCache();
	QFile::remove(QCoreApplication::applicationDirPath()+"/cache.dat"); 
}



//close all top windows
void FuiManager::CloseAllModalWindows()
{
	QWidget *pWidget=QApplication::activeModalWidget();
	while (pWidget)
	{
		pWidget->close();
		QCoreApplication::instance()->processEvents();
		pWidget=QApplication::activeModalWidget();
	}

}

void FuiManager::OnLoginDialogCreated(QDialog *pDlg)
{
	//MIRO: if not already online, get online....
	ConnectToNetwork(1000);	
}

void FuiManager::Login()
{

}
