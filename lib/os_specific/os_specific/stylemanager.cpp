#include "stylemanager.h"

#include <QApplication>
#include "arthurstyle.h"

#include <QStyle>
#include <QStyleFactory>


#ifndef WINCE
  #ifdef _WIN32
	//#include <QWindowsXPStyle>  //warning: only on XP
  #endif
	//#include <QMotifStyle>
	//#include <QPlastiqueStyle>
	//#include <QMacStyle>		//warning: only on MAC		(not bundled in Qt dll->rebuild all dll's)
  #ifdef _WIN32
	//#include <QWindowsVistaStyle>
  #endif
	//#include <QCDEStyle>
	//#include <QCleanlooksStyle>

	#include "paperstyle.h"
	#include "norwegianwoodstyle.h"
#endif // WINCE

static int g_DefaultStyle=StyleManager::QWINDOWSSTYLE;

void StyleManager::GetMainMenuStyles(int nStyle,QString &strMenuLegend,QString &styleSectionHeader,QString &styleSectionButtonLvl0,QString &styleSectionButtonLvl1)
{
	strMenuLegend="QLabel {color:white;} QPushButton { border-width: 4px; color: white;border-image:url(:PB_Menu_DarkGray_V1-0 Kopie.png) 4px 4px 4px 4px stretch stretch }";

	styleSectionHeader="QPushButton { border-width: 4px; color: white;border-image:url(:PB_Menu_DarkGray_V1-0 Kopie.png) 4px 4px 4px 4px stretch stretch }\
						QPushButton:hover {  border-width: 4px; color: white;border-image:url(:PB_Menu_MediumGray_V1-0 Kopie.png) 4px 4px 4px 4px stretch stretch} ";
	styleSectionButtonLvl0="QLabel {color:white;font-size:11px;} QPushButton { border-width: 4px; color: white;border-image:url(:PB_Menu_MediumGray_V1-0 Kopie.png) 4px 4px 4px 4px stretch stretch }\
						QPushButton:hover {  border-width: 4px; color: white;border-image:url(:PB_Menu_DarkGray_V1-0 Kopie.png) 4px 4px 4px 4px stretch stretch} ";
	styleSectionButtonLvl1="QLabel {color:black;font-size:11px;} QPushButton { border-width: 4px; color: white;border-image:url(:PB_Menu_LightGray_V1-0 Kopie.png) 4px 4px 4px 4px stretch stretch }";
}


void StyleManager::DetermineDefaultStyle()
{
	QStyle *style=QApplication::style();

	QStringList lstkeys = QStyleFactory::keys();

#ifndef WINCE
	#ifdef _WIN32
	QStyle *pStyle2 = QStyleFactory::create(QLatin1String("windowsxp"));
	//QWindowsXPStyle *pStyle2=dynamic_cast<QWindowsXPStyle*>(style);
	if (pStyle2)
	{
		g_DefaultStyle=QWINDOWSXPSTYLE;
		return;
	}
    #endif

	QStyle *pStyle3 = QStyleFactory::create(QLatin1String("cleanlooks"));
	//QCleanlooksStyle *pStyle3=dynamic_cast<QCleanlooksStyle*>(style);
	if (pStyle3)
	{
		g_DefaultStyle=QCLEANLOOKSSTYLE;
		return;
	}
#endif // WINCE

	/*
	QMacStyle *pStyle4=dynamic_cast<QMacStyle*>(style);
	if (pStyle4)
	{
	g_DefaultStyle=QMACSTYLE;
	return;
	}
	*/

#ifndef WINCE
	QStyle *pStyle5 = QStyleFactory::create(QLatin1String("plastique"));
	//QPlastiqueStyle *pStyle5=dynamic_cast<QPlastiqueStyle*>(style);
	if (pStyle5)
	{
		g_DefaultStyle=QPLASTIQUESTYLE;
		return;
	}


	ArthurStyle *pStyle9=dynamic_cast<ArthurStyle*>(style);
	if (pStyle9)
	{
		g_DefaultStyle=ARTHURSTYLE;
		return;
	}
#endif // WINCE



#ifndef WINCE
	//QCDEStyle *pStyle7=dynamic_cast<QCDEStyle*>(style);
	QStyle *pStyle7 = QStyleFactory::create(QLatin1String("CDE"));
	if (pStyle7)
	{
		g_DefaultStyle=QCDESTYLE;
		return;
	}

	NorwegianWoodStyle *pStyle8=dynamic_cast<NorwegianWoodStyle*>(style);
	if (pStyle8)
	{
		g_DefaultStyle=NORWEGIANWOODSTYLE;
		return;
	}

	PaperStyle *pStyle10=dynamic_cast<PaperStyle*>(style);
	if (pStyle10)
	{
		g_DefaultStyle=PAPERSTYLE;
		return;
	}

	//those two base, must be last:
	//QMotifStyle *pStyle6=dynamic_cast<QMotifStyle*>(style);
	QStyle *pStyle6 = QStyleFactory::create(QLatin1String("Motif"));
	if (pStyle6)
	{
		g_DefaultStyle=QMOTIFSTYLE;
		return;
	}
#endif // WINCE

	//QWindowsStyle *pStyle1=dynamic_cast<QWindowsStyle*>(style);
	QStyle *pStyle1 = QStyleFactory::create(QLatin1String("Windows"));
	if (pStyle1)
	{
		g_DefaultStyle=QWINDOWSSTYLE;
		return;
	}

#ifndef WINCE
 #ifdef _WIN32
	//QWindowsVistaStyle *pStyle11=dynamic_cast<QWindowsVistaStyle*>(style);
	QStyle *pStyle11 = QStyleFactory::create(QLatin1String("WindowsVista"));
	if (pStyle11)
	{
		g_DefaultStyle=QWINDOWSVISTASTYLE;
		return;
	}
 #endif
#endif // WINCE


}

//nStyle=from enum
void StyleManager::SetStyle(int nStyle)
{
	if (nStyle==g_DefaultStyle) //style set
		return;

	if (nStyle==DEFAULTSTYLE)  //do not change style if default-> use current
		return; 
	//{
	//	nStyle=g_DefaultStyle;
	//}

	QStyle *style=NULL;

	switch(nStyle)
	{
	case QWINDOWSSTYLE:
		//style = new QWindowsStyle();
		style = QStyleFactory::create(QLatin1String("windows"));
		break;
	case ARTHURSTYLE:
		style = new ArthurStyle();
		break;
#ifndef WINCE
  #ifdef _WIN32
	case QWINDOWSXPSTYLE:
		//style = new QWindowsXPStyle();
		style = QStyleFactory::create(QLatin1String("windowsxp"));
		break;
  #endif
	case QCLEANLOOKSSTYLE:
		//style = new QCleanlooksStyle();
		style = QStyleFactory::create(QLatin1String("Cleanlooks"));
		break;
	case QPLASTIQUESTYLE:
		//style = new QPlastiqueStyle();
		style = QStyleFactory::create(QLatin1String("Plastique"));
		break;
	case QMOTIFSTYLE:
		//style = new QMotifStyle();
		style = QStyleFactory::create(QLatin1String("Motif"));
		break;
	case QCDESTYLE:
		//style = new QCDEStyle();
		style = QStyleFactory::create(QLatin1String("CDE"));
		break;
	case NORWEGIANWOODSTYLE:
		style = new NorwegianWoodStyle();
		break;
	case PAPERSTYLE:
		style = new PaperStyle();
		break;
#ifdef _WIN32
	case QWINDOWSVISTASTYLE:
		//style = new QWindowsVistaStyle();
		style = QStyleFactory::create(QLatin1String("WindowsVista"));
		break;
#endif
	case QMACSTYLE:
		//style = new QWindowsStyle();	//temp
		style = QStyleFactory::create(QLatin1String("Windows"));
		break;
#endif // WINCE

	default:
		//style = new QWindowsStyle();
		style = QStyleFactory::create(QLatin1String("Windows"));
		break;
	}


	QApplication::setStyle(style);

}
