#include "os_specific/os_specific/outlookfolderpickerdlg.h"
#ifndef WINCE
#include "trans/trans/xmlutil.h"
#include "thunderbird/mboxparser.h"
#include <qDebug>
#include <QHeaderView>
#include <QtWidgets/QMessageBox>
#include <QDir>
#include "os_specific/os_specific/mapimanager.h"
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#ifdef _WIN32
 #define USES_IID_IMailUser
 #include <initguid.h>
 #include <MapiGuid.h>
 #include "mapi/mapiex.h"
#endif

//#define EXTRA_DEBUG

#define MAPI_CONTACTS_FOLDER_EN L"Contacts"
#define MAPI_CONTACTS_FOLDER_DE L"Kontakte"

#ifdef _WIN32
static QMap<qlonglong, int> g_mapFolderReleaseCnt;

#define BOOKS_ADD_REF(x) if(g_mapFolderReleaseCnt.contains((qlonglong)x)){ \
		g_mapFolderReleaseCnt[(qlonglong)x] ++; \
    } else { \
		g_mapFolderReleaseCnt[(qlonglong)x] = 1; \
	}

#define BOOKS_RELEASING(x) if(!g_mapFolderReleaseCnt.contains((qlonglong)x)){ \
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR: Releasing unknown MAPI pointer: %1 (file: %2, line: %3)").arg((qlonglong)x).arg(__FILE__).arg(__LINE__)); \
		Q_ASSERT(false); \
	} else { \
		g_mapFolderReleaseCnt[(qlonglong)x] --; \
		if(g_mapFolderReleaseCnt[(qlonglong)x] < 0){ \
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR2: Releasing unknown MAPI pointer: %1 (file: %2, line: %3)").arg((qlonglong)x).arg(__FILE__).arg(__LINE__)); \
			Q_ASSERT(false); \
		} \
    }

#define BOOKS_VERIFY_CLEAN()  QMap<qlonglong, int>::const_iterator It = g_mapFolderReleaseCnt.constBegin(); \
		while(It != g_mapFolderReleaseCnt.constEnd()){ \
			if(It.value() < 0){ \
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR: Map contains unreleased MAPI pointer: %1").arg(It.value())); \
				Q_ASSERT(false); \
			} \
			It ++; \
		}
#endif

OutlookFolderPickerDlg::OutlookFolderPickerDlg(bool bThunderbird, bool bContacts, bool bRememberSettingBtn, QWidget *parent)
    : QDialog(parent),m_bStoreSetings(false)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook/Firebird folder picker dialog constructor"));

	m_bThunderbird = bThunderbird;
	m_bTreeBuilt = false;
	m_bRecursing = false;
	m_bContacts = bContacts;

	ui.setupUi(this);
	ui.treeFolders->header()->hide();

	if(!bRememberSettingBtn)
		ui.btnRememberSetting->hide();

	if(m_bThunderbird)
	{
		setWindowTitle(tr("Select Thunderbird folder"));

		//
		// build Thunderbird email folders list
		//
		
		//start enumerating from root
		QStringList lstRoot;
		ListBranchRecursive_Thunderbird(NULL, lstRoot);
		m_bTreeBuilt = true;
	}
	else
	{
#ifdef _WIN32
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List Outlook"));

		//
		// build Outlook email folders list
		//
		CMAPIEx* pMapi = MapiManager::InitGlobalMapi();
		if(!pMapi)
		{
			qDebug() << "Failed to initialize MAPI\n";
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Failed to Initialize MAPI"));
			return;
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MAPI interface created"));

		if(m_bContacts)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List Outlook contacts"));

			m_strContactsName = "Contacts";
			if(pMapi->OpenRootFolder())
			{
				/*
				if(pMapi->OpenSubFolder(MAPI_CONTACTS_FOLDER_DE))
					m_strContactsName = "Kontakte";
				else if(pMapi->OpenSubFolder(MAPI_CONTACTS_FOLDER_EN))
					m_strContactsName = "Contacts";
				*/
			}

			//start enumerating from root
			QStringList lstRoot;
			ListBranchRecursive(NULL, lstRoot, pMapi);
			BOOKS_VERIFY_CLEAN();
			m_bTreeBuilt = true;
		}
		else
		{
			//start enumerating from root
			//QStringList lstRoot;
			//ListBranchRecursive(NULL, lstRoot, pMapi);
			//BOOKS_VERIFY_CLEAN();
		
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List Outlook folders"));

			//top level items - message stores
			int nCnt = pMapi->GetMessageStoreCount();
			for(int i=0; i<nCnt; i++){
				QString strStoreName;
				if(pMapi->OpenMessageStore(i, strStoreName)){
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List message store: %1").arg(strStoreName));

					QTreeWidgetItem *pFolder = new QTreeWidgetItem;
					ui.treeFolders->addTopLevelItem(pFolder);
					pFolder->setText(0,	strStoreName);
					pFolder->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
					pFolder->setCheckState(0, Qt::Unchecked);

					//start enumerating from root
					QStringList lstRoot;
					ListBranchRecursive(pFolder, lstRoot, pMapi);
					BOOKS_VERIFY_CLEAN();
				}
			}

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Done listing folders for %1 message stores").arg(nCnt));
			m_bTreeBuilt = true;
		}

		//QMessageBox::information(this, tr("test"), QString("%1 Outlook folders found!").arg(nRes));
#endif //#ifdef _WIN32
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook/Firebird folder picker dialog constructor done"));
}

OutlookFolderPickerDlg::~OutlookFolderPickerDlg()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: destructor done");
}

void OutlookFolderPickerDlg::on_btnOK_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: OK click");

	RefreshSelection();
	accept();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: OK click done");
}

void OutlookFolderPickerDlg::on_btnCancel_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: Cancel click");

	reject();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: Cancel click done");
}

#ifdef _WIN32
void OutlookFolderPickerDlg::ListBranchRecursive(QTreeWidgetItem *pParent, QStringList lstParent, CMAPIEx *pMapi)
{
	LPMAPIFOLDER lpMapiFolder = (m_bContacts) ? pMapi->OpenContacts(false) : pMapi->OpenRootFolder(false);
	if(!lpMapiFolder)
		return;
	BOOKS_ADD_REF(lpMapiFolder);

	QString strBranch;
	int nCnt = lstParent.length();
	for(int i=0; i<nCnt; i++)
		strBranch += lstParent[i] + ";";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ListBranchRecursive: %1").arg(strBranch));

	//Contacts root
	if(m_bContacts && !pParent){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List contacts branch: %1").arg(m_strContactsName));

		QTreeWidgetItem *pFolder = new QTreeWidgetItem;
		ui.treeFolders->addTopLevelItem(pFolder);
		pFolder->setText(0,	m_strContactsName);
		pFolder->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
		pFolder->setCheckState(0, Qt::Unchecked);
		pParent = pFolder;
	}

	//find parent folder
	//TOFIX reuse existing fn?: LPMAPIFOLDER lpParentFolder = MapiManager::OpenOutlookFolder(*pMapi, lstParent, m_bContacts) -> MapiManager::OpenOutlookFolderFromCurrentStore -> shared code
	LPMAPIFOLDER lpParentFolder = lpMapiFolder;
	int nLevel = 0;
	int nDepth = lstParent.count();
#ifdef EXTRA_DEBUG
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Find MAPI pointer for folder at depth: %1, parent folder=%2").arg(nDepth).arg((qlonglong)lpParentFolder));
#endif
	while(nLevel < nDepth){
		//LPMAPITABLE lpHierarachy = pMapi->GetHierarchy(lpParentFolder); //TOFIX handler NULL result!!!
		pMapi->GetHierarchy(lpParentFolder);
	#ifdef EXTRA_DEBUG
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Find MAPI pointer for parent at level: %1, folder name: %2").arg(nLevel).arg(lstParent[nLevel]));
	#endif
		QString strFolder;
		LPMAPIFOLDER lpFolder = pMapi->GetNextSubFolder(strFolder, lpParentFolder);
	#ifdef EXTRA_DEBUG
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Match folder name [%1] to target folder [%2], ptr=%3").arg(strFolder).arg(lstParent[nLevel]).arg((qlonglong)lpFolder));
	#endif
		BOOKS_ADD_REF(lpFolder);

		while(NULL != lpFolder && strFolder != lstParent[nLevel]){
		#ifdef EXTRA_DEBUG
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Match folder name [%1] to target folder [%2] failed").arg(strFolder).arg(lstParent[nLevel]));
		#endif
			LPMAPIFOLDER lpNewFolder = pMapi->GetNextSubFolder(strFolder, lpParentFolder);
		#ifdef EXTRA_DEBUG
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Got new folder [%1], ptr=%2, will release previous one: %3").arg(strFolder).arg((qlonglong)lpNewFolder).arg((qlonglong)lpFolder));
		#endif
			BOOKS_ADD_REF(lpNewFolder);

			BOOKS_RELEASING(lpFolder);
			RELEASE(lpFolder);
		#ifdef EXTRA_DEBUG
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Folder released"));
		#endif
			lpFolder = lpNewFolder;
		#ifdef EXTRA_DEBUG
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Match next folder name [%1] to target folder [%2], ptr=%3").arg(strFolder).arg(lstParent[nLevel]).arg((qlonglong)lpFolder));
		#endif
		}
		LPMAPIFOLDER lpNewParent = lpFolder;
		//LPMAPIFOLDER lpNewParent = pMapi->OpenSubFolder((LPCTSTR)lstParent[nLevel].utf16(), lpParentFolder);
		if(!lpNewParent){
		#ifdef EXTRA_DEBUG
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Failed to find MAPI pointer for folder, release parent=%1").arg((qlonglong)lpParentFolder));
		#endif
			BOOKS_RELEASING(lpParentFolder);
			RELEASE(lpParentFolder);
			return;	//error, not found
		}

	#ifdef EXTRA_DEBUG
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Found new parent ptr=%1, release old parent=%2").arg((qlonglong)lpNewParent).arg((qlonglong)lpParentFolder));
	#endif
		BOOKS_RELEASING(lpParentFolder);
		RELEASE(lpParentFolder);
		lpParentFolder = lpNewParent;
		nLevel ++;
	}

#ifdef EXTRA_DEBUG
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Found MAPI pointer for folder: %1").arg((qlonglong)lpParentFolder));
#endif
	pMapi->GetHierarchy(lpParentFolder);

#ifdef EXTRA_DEBUG
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List folder contents"));
#endif

	//now list the folder in the tree view widget
	QStringList listed; 
	QList<QTreeWidgetItem *> folders;
	QString strFolder;
	LPMAPIFOLDER lpSubFolder = NULL;
	while(NULL != (lpSubFolder = pMapi->GetNextSubFolder(strFolder, lpParentFolder)))
	{
	#ifdef EXTRA_DEBUG
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List subfolder: %1").arg(strFolder));
	#endif
		BOOKS_ADD_REF(lpSubFolder);

		QTreeWidgetItem *pFolder = (pParent) ? new QTreeWidgetItem(pParent) : new QTreeWidgetItem;
		if(!pParent)
			ui.treeFolders->addTopLevelItem(pFolder);
		pFolder->setText(0,	strFolder);
		pFolder->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
		pFolder->setCheckState(0, Qt::Unchecked);

		//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Added subfolder item, release ptr=%1").arg((qlonglong)lpSubFolder));

		BOOKS_RELEASING(lpSubFolder);
		RELEASE(lpSubFolder);
		listed.append(strFolder);
		folders.append(pFolder);
	}

#ifdef EXTRA_DEBUG
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List folder contents - done, release parent ptr=%1").arg((qlonglong)lpParentFolder));
#endif

	BOOKS_RELEASING(lpParentFolder);
	RELEASE(lpParentFolder);

#ifdef EXTRA_DEBUG
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List subfolder contents recursively"));
#endif

	//now recurse
	int nCount = listed.count();
	for(int i=0; i<nCount; i++){
		QStringList list(lstParent);
		list.append(listed[i]);
		ListBranchRecursive(folders[i], list, pMapi);
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("List subfolder contents recursively - done"));
}
#endif //#ifdef _WIN32

void OutlookFolderPickerDlg::RefreshSelection()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook folders: GetSelection"));

	m_lstSelection.clear();
	
	QStringList lstParent;
	RefreshSelectionRecursive(NULL, lstParent);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook folders: GetSelection done"));
}

void OutlookFolderPickerDlg::RefreshSelectionRecursive(QTreeWidgetItem *pParent, QStringList lstParent)
{
	int nCnt = (pParent)? pParent->childCount() : ui.treeFolders->topLevelItemCount();
	for(int i=0; i<nCnt; i++)
	{
		QTreeWidgetItem *item = (pParent)? pParent->child(i) : ui.treeFolders->topLevelItem(i);

		QStringList lstItem;
		lstItem = lstParent;
		lstItem << item->text(0);

		if(Qt::Checked == item->checkState(0))
			m_lstSelection << lstItem;

		RefreshSelectionRecursive(item, lstItem);
	}
}

void OutlookFolderPickerDlg::SetSelection(QList<QStringList> &lstSelection)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook folders: SetSelection"));

	m_bRecursing = true;

	int nCnt = lstSelection.count();
	for(int i=0; i<nCnt; i++)
	{
		//select single item (search by name on each level)	
		QTreeWidgetItem *pParent = NULL;
		int nDepth = lstSelection[i].count();
		int nLevel = 0;
		while(nLevel < nDepth)
		{
			//search item by name
			QTreeWidgetItem *item = NULL;
			int nChildCnt = (pParent)? pParent->childCount() : ui.treeFolders->topLevelItemCount();
			for(int j=0; j<nChildCnt; j++){
				QTreeWidgetItem *tmp = (pParent)? pParent->child(j) : ui.treeFolders->topLevelItem(j);
				if(tmp && tmp->text(0) == lstSelection[i][nLevel]){
					item = tmp;
					break;
				}
			}

			//go to the next level
			nLevel ++;
			if(item)
				pParent = item;
			else{
				pParent = NULL;
				break;
			}
		}

		if(pParent)
			pParent->setCheckState(0, Qt::Checked);
	}

	m_bRecursing = false;

	if(m_bContacts && 0 == nCnt)
	{
		SetDefaultSelection();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Outlook folders: SetSelection done"));
}

void OutlookFolderPickerDlg::on_treeFolders_itemChanged(QTreeWidgetItem * item,int column)
{
	if(!m_bTreeBuilt || m_bRecursing)
		return;

	//checking/unchecking collapsed nodes checks/unchecks all subnodes
	if(item && !item->isExpanded())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: on item checked");

		bool bChecked = (Qt::Checked == item->checkState(0));
		m_bRecursing = true;
		CheckBranchRecursively(item, bChecked);
		m_bRecursing = false;

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: on item checked done");
	}
}

void OutlookFolderPickerDlg::CheckBranchRecursively(QTreeWidgetItem * item, bool bCheck)
{
	if(item) {
		int nChildCnt = item->childCount();
		for(int j=0; j<nChildCnt; j++){
			QTreeWidgetItem *tmp = item->child(j);
			if(tmp){
				tmp->setCheckState(0, bCheck ? Qt::Checked : Qt::Unchecked);
				CheckBranchRecursively(tmp, bCheck);
			}
		}
	}
}

void OutlookFolderPickerDlg::on_btnRememberSetting_clicked()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: Remember setting click");

	m_bStoreSetings=true;
	RefreshSelection();

	//convert from list to recordset
	//QByteArray arData;
	//FolderList2Binary(m_lstSelection, arData);

	/*
	//store setting
	if(m_bThunderbird)
		g_pSettings->SetPersonSetting(IMPORT_EMAIL_THUNDERBIRD_FOLDERS, arData);
	else{
		if(m_bContacts)
			g_pSettings->SetPersonSetting(IMPORT_CONTACT_OUTLOOK_FOLDERS, arData);
		else
			g_pSettings->SetPersonSetting(IMPORT_EMAIL_OUTLOOK_FOLDERS, arData);
	}
	*/

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: Remember setting click done");
}

bool OutlookFolderPickerDlg::StoreSettings(QByteArray &ardata)
{
	FolderList2Binary(m_lstSelection, ardata);
	return m_bStoreSetings;
}

void OutlookFolderPickerDlg::FolderList2Binary(QList<QStringList> &lstData, QByteArray &arResult)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: folder list to binary");

	//convert from list to recordset
	DbRecordSet data;
	data.addColumn(DbRecordSet::GetVariantType(),"A");
	int nCnt = lstData.count();
	for(int i=0; i<nCnt; i++)
	{
		data.addRow();

		QStringList lstSub = lstData.at(i);

		DbRecordSet subdata;
		subdata.addColumn(QVariant::String,"B");
		int nSubCnt = lstSub.count();
		for(int j=0; j<nSubCnt; j++)
		{
			subdata.addRow();
			subdata.setData(j, 0, lstSub.at(j));
		}

		data.setData(i, 0, subdata);
	}

	arResult = XmlUtil::ConvertRecordSet2ByteArray_Fast(data);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: folder list to binary done");
}

void OutlookFolderPickerDlg::Binary2FolderList(QByteArray &arData, QList<QStringList> &lstResult)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: binary to folder list");

	lstResult.clear();

	DbRecordSet lstData = XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
	int nCnt = lstData.getRowCount();
	for(int i=0; i<nCnt; i++){
		DbRecordSet recRow = lstData.getDataRef(i, 0).value<DbRecordSet>();
		QStringList lstSub;
		int nCntSub = recRow.getRowCount();
		for(int j=0; j<nCntSub; j++){
			lstSub.append(recRow.getDataRef(j, 0).toString());
		}

		lstResult.append(lstSub);
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: binary to folder list done");
}

void OutlookFolderPickerDlg::ListBranchRecursive_Thunderbird(QTreeWidgetItem *pParent, QStringList lstParent)
{
	QString strProfiles = MboxParser::GetThunderbirdProfileDir();
	if(NULL == pParent)
	{
		QTreeWidgetItem *pFolder = new QTreeWidgetItem;
		ui.treeFolders->addTopLevelItem(pFolder);
		pFolder->setText(0,	tr("Profiles"));
		pFolder->setFlags(/*Qt::ItemIsUserCheckable|*/Qt::ItemIsEnabled|Qt::ItemIsSelectable);
		//pFolder->setCheckState(0, Qt::Unchecked);
		pParent = pFolder;

		QDir dir(strProfiles);
		QStringList subfolders = dir.entryList(QDir::Dirs|QDir::NoDotAndDotDot, QDir::Name);

		//now list the folder
		QList<QTreeWidgetItem *> folders;
		int nMax = subfolders.size();
		for(int i=0; i<nMax; i++)
		{
			QTreeWidgetItem *pFolder = (pParent) ? new QTreeWidgetItem(pParent) : new QTreeWidgetItem;
			if(!pParent)
				ui.treeFolders->addTopLevelItem(pFolder);
			pFolder->setText(0,	subfolders[i]);
			pFolder->setFlags(/*Qt::ItemIsUserCheckable|*/Qt::ItemIsEnabled|Qt::ItemIsSelectable);
			//pFolder->setCheckState(0, Qt::Unchecked);
			folders.append(pFolder);
		}

		//now recurse
		for(int i=0; i<nMax; i++){
			QStringList list(lstParent);
			list.append(subfolders[i]);
			ListBranchRecursive_Thunderbird(folders[i], list);
		}
	}
	else{
		QStringList subfolders;
		int nDepth = lstParent.size();
		if(nDepth == 1)	// profile names level
		{
			strProfiles += "/";
			strProfiles += lstParent[0];
			strProfiles += "/Mail";

			QDir dir(strProfiles);
			subfolders = dir.entryList(QDir::Dirs|QDir::NoDotAndDotDot, QDir::Name);
		}
		else // inside the actual mail folder
		{
			strProfiles += "/";
			strProfiles += lstParent[0];
			strProfiles += "/Mail";

			for(int i=1; i<nDepth; i++)
			{
				strProfiles += "/";
				strProfiles += lstParent[i];
				if(i > 1) // subfolders
					strProfiles += ".sbd";
			}
		
			//list files now
			QDir dir(strProfiles);
			QStringList strLstFilter;
			strLstFilter << "*.msf";
			subfolders = dir.entryList(strLstFilter, QDir::Files, QDir::Name);
			int nMax = subfolders.size();
			for(int i=0; i<nMax; i++){
				subfolders[i].chop(4);
			}
		}

		//now list the folder
		QList<QTreeWidgetItem *> folders;
		int nMax = subfolders.size();
		for(int i=0; i<nMax; i++)
		{
			QTreeWidgetItem *pFolder = (pParent) ? new QTreeWidgetItem(pParent) : new QTreeWidgetItem;
			if(!pParent)
				ui.treeFolders->addTopLevelItem(pFolder);
			pFolder->setText(0,	subfolders[i]);
			if(nDepth > 1){
				pFolder->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
				pFolder->setCheckState(0, Qt::Unchecked);
			}
			else
				pFolder->setFlags(/*Qt::ItemIsUserCheckable|*/Qt::ItemIsEnabled|Qt::ItemIsSelectable);
			
			folders.append(pFolder);
		}

		//now recurse
		for(int i=0; i<nMax; i++){
			QStringList list(lstParent);
			list.append(subfolders[i]);
			ListBranchRecursive_Thunderbird(folders[i], list);
		}
	}
}

QString OutlookFolderPickerDlg::ThunderbirdFolder2String(QStringList &lstData)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: thunderbird folders to string");

	QString strFolderFile;

	int nDepth = lstData.size();
	if(nDepth > 1) // inside the actual mail folder
	{
		strFolderFile  = MboxParser::GetThunderbirdProfileDir();
		strFolderFile += "/";
		strFolderFile += lstData[1];
		strFolderFile += "/Mail";

		for(int i=2; i<nDepth; i++)
		{
			strFolderFile += "/";
			strFolderFile += lstData[i];
			if(i > 2 && i<(nDepth-1)) // subfolders
				strFolderFile += ".sbd";
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: thunderbird folders to string done");

	return QDir::toNativeSeparators(strFolderFile);
}

void OutlookFolderPickerDlg::GetDefaultFolders(QList<QStringList> &lstData, bool bContacts)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: Get default folders");

	OutlookFolderPickerDlg dlg(false, bContacts);
	dlg.SetDefaultSelection();
	dlg.RefreshSelection();
	lstData = dlg.GetSelection();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: Get default folders Done");
}

void OutlookFolderPickerDlg::SetDefaultSelection()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: Set default selection");

	//selects all the top level items
	QTreeWidgetItem *item = NULL;
	int nChildCnt = ui.treeFolders->topLevelItemCount();
	for(int j=0; j<nChildCnt; j++){
		QTreeWidgetItem *tmp = ui.treeFolders->topLevelItem(j);
		tmp->setCheckState(0, Qt::Checked);
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook folders: Set default selection done");
}

#endif // WINCE