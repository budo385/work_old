#ifndef _TWIXTEL_CLASS_H_
#define _TWIXTEL_CLASS_H_

//#include <QObject>
#include <QString>

//
// Twixtel object
//
typedef	int (__stdcall *PFN_TwixInitializeEngine)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchName)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchFirstName)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchStreetNo)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchZipCity)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchProfession)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchMaidenName)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchPhone)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchSpecial)(char*);
typedef	int (__stdcall *PFN_TwixSetSearchCantons)(char*);
typedef	int (__stdcall *PFN_TwixClearSearchEntries)();
typedef	int (__stdcall *PFN_TwixGetSearchResult)(int);
typedef	int (__stdcall *PFN_TwixGetMainEntry)(int, char*, int);
typedef	int (__stdcall *PFN_TwixSearch)();
typedef	void (__stdcall *PFN_TwixSetSearchLanguage)(int);
typedef	void (__stdcall *PFN_TwixSetShowMessages)(int);
typedef	int (__stdcall *PFN_TwixGetFollowCount)();
typedef	int (__stdcall *PFN_TwixGetFollowEntry)(int, int, char *, int);

class TwixTel
{
public:    TwixTel();
    ~TwixTel();

	void SetQuery_LastName(const QString &strName);
	void SetQuery_FirstName(const QString &strName);
	void SetQuery_Organization(const QString &strName);
	void SetQuery_Town(const QString &strName);

	void ClearSearch();
	int RunQuery();
	void SetCurrentRow(int nIdx);
	void GetCurrentField(int nIdx, QString &strRes);
	int GetNumSubRows(); 
	void GetSubRowField(int nIdx, int nField, QString &strRes);

protected:
	static bool m_bLoaded;
	static PFN_TwixInitializeEngine		TwixInitializeEngine;
	static PFN_TwixSetSearchName		TwixSetSearchName;
	static PFN_TwixSetSearchFirstName	TwixSetSearchFirstName;
	static PFN_TwixSetSearchStreetNo	TwixSetSearchStreetNo;
	static PFN_TwixSetSearchZipCity		TwixSetSearchZipCity;
	static PFN_TwixSetSearchProfession	TwixSetSearchProfession;
	static PFN_TwixSetSearchMaidenName	TwixSetSearchMaidenName;
	static PFN_TwixSetSearchPhone		TwixSetSearchPhone;
	static PFN_TwixSetSearchSpecial		TwixSetSearchSpecial;
	static PFN_TwixSetSearchCantons		TwixSetSearchCantons;
	static PFN_TwixClearSearchEntries	TwixClearSearchEntries;
	static PFN_TwixGetSearchResult		TwixGetSearchResult;
	static PFN_TwixGetMainEntry			TwixGetMainEntry;
	static PFN_TwixSearch				TwixSearch;
	static PFN_TwixSetSearchLanguage	TwixSetSearchLanguage;
	static PFN_TwixSetShowMessages		TwixSetShowMessages;
	static PFN_TwixGetFollowCount		TwixGetFollowCount;
	static PFN_TwixGetFollowEntry		TwixGetFollowEntry;
	
public:
	bool 	Initialize(QString strCDDrive);
	bool 	Shutdown();
};


#endif   //_TWIXTEL_CLASS_H_

