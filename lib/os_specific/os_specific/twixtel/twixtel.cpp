// twixtel wrapper code
#ifndef WINCE

#include <qt_windows.h>
#include "twixtel.h"
#include <QtWidgets/QMessageBox>
#include <QDebug>
//#define NOMINMAX
//#include <windows.h>

#include "twixapi.h"

//Twixtel interface implementation is for Windows ONLY.
#ifndef _WIN32
 #error "Windows is required for Twixtel."
#endif

bool TwixTel::m_bLoaded	= false;
PFN_TwixInitializeEngine		TwixTel::TwixInitializeEngine	= NULL;
PFN_TwixSetSearchName			TwixTel::TwixSetSearchName		= NULL;
PFN_TwixSetSearchFirstName		TwixTel::TwixSetSearchFirstName	= NULL;
PFN_TwixSetSearchStreetNo		TwixTel::TwixSetSearchStreetNo	= NULL;
PFN_TwixSetSearchZipCity		TwixTel::TwixSetSearchZipCity	= NULL;
PFN_TwixSetSearchProfession		TwixTel::TwixSetSearchProfession = NULL;
PFN_TwixSetSearchMaidenName		TwixTel::TwixSetSearchMaidenName = NULL;
PFN_TwixSetSearchPhone			TwixTel::TwixSetSearchPhone		= NULL;
PFN_TwixSetSearchSpecial		TwixTel::TwixSetSearchSpecial	= NULL;
PFN_TwixSetSearchCantons		TwixTel::TwixSetSearchCantons	= NULL;
PFN_TwixClearSearchEntries		TwixTel::TwixClearSearchEntries	= NULL;
PFN_TwixGetSearchResult			TwixTel::TwixGetSearchResult	= NULL;
PFN_TwixGetMainEntry			TwixTel::TwixGetMainEntry		= NULL;
PFN_TwixSearch					TwixTel::TwixSearch				= NULL;
PFN_TwixSetSearchLanguage		TwixTel::TwixSetSearchLanguage	= NULL;
PFN_TwixSetShowMessages			TwixTel::TwixSetShowMessages	= NULL;
PFN_TwixGetFollowCount			TwixTel::TwixGetFollowCount		= NULL;
PFN_TwixGetFollowEntry			TwixTel::TwixGetFollowEntry		= NULL;

TwixTel::TwixTel() 
{
}
 
TwixTel::~TwixTel() 
{
	//TOFIX TwixUnInitializeEngine(), freelibrary, ...
}

bool TwixTel::Initialize(QString strCDDrive)
{
	if(!m_bLoaded)
	{
		HINSTANCE hDLL = LoadLibrary(L"twxapi32.dll");
		if (hDLL == NULL){
			QMessageBox::information(NULL, "", QObject::tr("Failed to load twxapi32.dll!"));
			return false;
		}
		
		TwixInitializeEngine	= (PFN_TwixInitializeEngine)	GetProcAddress(hDLL, "TwixInitializeEngine");
		TwixSetSearchName		= (PFN_TwixSetSearchName)		GetProcAddress(hDLL, "TwixSetSearchName");
		TwixSetSearchFirstName	= (PFN_TwixSetSearchFirstName)	GetProcAddress(hDLL, "TwixSetSearchFirstName");
		TwixSetSearchStreetNo	= (PFN_TwixSetSearchStreetNo)	GetProcAddress(hDLL, "TwixSetSearchStreetNo");
		TwixSetSearchZipCity	= (PFN_TwixSetSearchZipCity)	GetProcAddress(hDLL, "TwixSetSearchZipCity");
		TwixSetSearchProfession = (PFN_TwixSetSearchProfession)	GetProcAddress(hDLL, "TwixSetSearchProfession");
		TwixSetSearchMaidenName = (PFN_TwixSetSearchMaidenName)	GetProcAddress(hDLL, "TwixSetSearchMaidenName");
		TwixSetSearchPhone		= (PFN_TwixSetSearchPhone)		GetProcAddress(hDLL, "TwixSetSearchPhone");
		TwixSetSearchSpecial	= (PFN_TwixSetSearchSpecial)	GetProcAddress(hDLL, "TwixSetSearchSpecial");
		TwixSetSearchCantons	= (PFN_TwixSetSearchCantons)	GetProcAddress(hDLL, "TwixSetSearchCantons");
		TwixClearSearchEntries	= (PFN_TwixClearSearchEntries)	GetProcAddress(hDLL, "TwixClearSearchEntries");
		TwixGetSearchResult		= (PFN_TwixGetSearchResult)		GetProcAddress(hDLL, "TwixGetSearchResult");
		TwixGetMainEntry		= (PFN_TwixGetMainEntry)		GetProcAddress(hDLL, "TwixGetMainEntry");
		TwixSearch				= (PFN_TwixSearch)				GetProcAddress(hDLL, "TwixSearch");
		TwixSetSearchLanguage	= (PFN_TwixSetSearchLanguage)	GetProcAddress(hDLL, "TwixSetSearchLanguage");
		TwixSetShowMessages		= (PFN_TwixSetShowMessages)		GetProcAddress(hDLL, "TwixSetShowMessages");
		TwixGetFollowCount		= (PFN_TwixGetFollowCount)		GetProcAddress(hDLL, "TwixGetFollowCount");
		TwixGetFollowEntry		= (PFN_TwixGetFollowEntry)		GetProcAddress(hDLL, "TwixGetFollowEntry");

		if( NULL == TwixInitializeEngine		||
			NULL == TwixSetSearchName			||
			NULL == TwixSetSearchFirstName		||
			NULL == TwixSetSearchStreetNo		||
			NULL == TwixSetSearchZipCity		||
			NULL == TwixSetSearchProfession 	||
			NULL == TwixSetSearchMaidenName 	||
			NULL == TwixSetSearchPhone			||
			NULL == TwixSetSearchSpecial		||
			NULL == TwixSetSearchCantons		||
			NULL == TwixClearSearchEntries		||
			NULL == TwixGetSearchResult			||
			NULL == TwixGetMainEntry			||
			NULL == TwixSearch					||
			NULL == TwixSetSearchLanguage		||
			NULL == TwixSetShowMessages			||
			NULL == TwixGetFollowCount			||
			NULL == TwixGetFollowEntry)
		{
			QMessageBox::information(NULL, "", QObject::tr("Your version of twxapi32.dll does not export required methods!"));
			return 	false;
		}

		m_bLoaded = true;
	}

	/////////////////////////////////////////////////////////////////////////
	// must be called once at the beginning to initialize engine amd allocate 
	// ca. 3 x 500 KB for bitstream-operations !!!
	/////////////////////////////////////////////////////////////////////////
	if (TwixInitializeEngine((char *)strCDDrive.toLatin1().constData()) < 0)
    {
		QMessageBox::information(NULL, "", QObject::tr("Failed initializing Twixtel engine!"));
		return false;
    }

	// to process all error messages unmark line "TwixSetShowMessages(FALSE)" below:
	// If unmarked, no messageboxes are displayed on errors,
	// only an error code (< 0) is returned
	// displaying error messages is recommended at least in the developing phase
#ifdef _DEBUG
	TwixSetShowMessages(FALSE);
#else
	TwixSetShowMessages(FALSE);
#endif
	
	TwixSetSearchLanguage(LNG_GERMAN);
	return true;
}

bool TwixTel::Shutdown()
{
	return true;
}

int TwixTel::RunQuery()
{
	return TwixSearch();
}

void TwixTel::SetCurrentRow(int nIdx)
{
	TwixGetSearchResult(nIdx+1);
}

void TwixTel::GetCurrentField(int nIdx, QString &strRes)
{
	char value[1000] = "";
	if(TwixGetMainEntry(nIdx, value, sizeof(value)))
		strRes = QString::fromLocal8Bit(value);
	else
		strRes = "";
}

void TwixTel::SetQuery_LastName(const QString &strName)
{
	TwixSetSearchName((char *)strName.toLatin1().constData());
}

void TwixTel::SetQuery_FirstName(const QString &strName)
{
	TwixSetSearchFirstName((char *)strName.toLatin1().constData());
}

void TwixTel::SetQuery_Organization(const QString &strName)
{
	SetQuery_LastName(strName);
}
	
void TwixTel::SetQuery_Town(const QString &strName)
{
	TwixSetSearchZipCity((char *)strName.toLatin1().constData());
}

void TwixTel::ClearSearch()
{ 
	TwixClearSearchEntries();
}

int TwixTel::GetNumSubRows()
{
	// get number of "follow"-entries  (Natel, Fax etc... )
	return TwixGetFollowCount();
}

void TwixTel::GetSubRowField(int nIdx, int nField, QString &strRes)
{
	char value[1000] = "";
	if(TwixGetFollowEntry(nIdx, nField, value, sizeof(value)))
		strRes = QString::fromLocal8Bit(value);
	else
		strRes = "";
}

#endif // WINCE
