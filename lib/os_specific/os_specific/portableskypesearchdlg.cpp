#include "portableskypesearchdlg.h"

PortableSkypeSearchDlg::PortableSkypeSearchDlg(QWidget *parent)
	: QDialog(parent)
{
	m_bSearchSkype = true;
	ui.setupUi(this);
}

PortableSkypeSearchDlg::~PortableSkypeSearchDlg()
{
}

void PortableSkypeSearchDlg::on_btnOK_clicked()
{
	m_bSearchSkype = ui.radSkypeSearch->isChecked();
	accept();
}

void PortableSkypeSearchDlg::on_btnCancel_clicked()
{
	reject();
}
