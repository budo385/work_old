#ifndef PORTABLESKYPESEARCHDLG_H
#define PORTABLESKYPESEARCHDLG_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_portableskypesearchdlg.h"

class PortableSkypeSearchDlg : public QDialog
{
	Q_OBJECT

public:
	PortableSkypeSearchDlg(QWidget *parent = 0);
	~PortableSkypeSearchDlg();

	bool m_bSearchSkype;

private slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();

private:
	Ui::PortableSkypeSearchDlgClass ui;
};

#endif // PORTABLESKYPESEARCHDLG_H
