#define NOMINMAX
#include "mapi/mapiex.h"
#include "mapimanager.h"
#include <QtWidgets/QMessageBox>
#include <QObject>
#include <QLibrary>
#include <QApplication>
#include <QFile>
#include "mailmanager.h"
#include "common/common/threadid.h"
#include "bus_client/bus_client/emailhelper.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

//to avoid bus_core
#define CONTACT_SEX_MALE	0		
#define CONTACT_SEX_FEMALE	1		

#include "db_core/db_core/dbsqltableview.h"

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#ifndef WINCE
#ifdef _WIN32

//#include <windows.h>
QLibrary g_MapiDLL;
bool g_mapi_initialized=false;
QList<QByteArray> g_lstMailsToDelete;

//global 
bool g_mapi_global_initialized=false;
CMAPIEx g_mapi;
int g_nMapiThreadID=0;

bool MapiManager::LoadMapiDll()
{
	typedef bool (*tInitTest)();

	//load only if needed:
	if(g_MapiDLL.isLoaded())
		return true;
	g_MapiDLL.setFileName("MAPIEx");
	if(!g_MapiDLL.load())
	{
		QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Failed to load MAPIex.dll (error:%1)").arg(g_MapiDLL.errorString()));
		return false;
	}
	//initialize:
	tInitTest pfInitialize = (tInitTest) g_MapiDLL.resolve("Initialize");
	if (!pfInitialize)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"ERROR: Failed to initialize MAPIEx library!");
		g_MapiDLL.unload();
		return false;
	}
	//check if MAPI installes:
	if(!pfInitialize()) 
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"ERROR: Failed to initialize MAPIEx library (1)!");
		g_MapiDLL.unload();
		return false;
	}
	return true;
}

bool MapiManager::UnIntiliazeMapiDll()
{
	//load only if needed:
	if(!g_MapiDLL.isLoaded()){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Trying to uninitialize MAPI (not loaded)!");
		return true;
	}

	//delete temporary mails from the Outlook
	typedef bool (*tDeleteMailByID)(int, const char *);
	tDeleteMailByID DeleteMailByID = (tDeleteMailByID) g_MapiDLL.resolve("DeleteMailByID");
	if(DeleteMailByID) 
	{
		int nSize = g_lstMailsToDelete.length();

		QSize size(300,100);
		QProgressDialog progress(QObject::tr("Deleting Temporary Outlook Mails..."),QObject::tr("Cancel"),0, nSize, NULL);
		progress.setWindowTitle(QObject::tr("Cleanup"));
		progress.setMinimumSize(size);
		progress.setWindowModality(Qt::NonModal);
		progress.setMinimumDuration(1200);//2.4s before pops up

		for(int i=0; i<nSize; i++)
		{
			progress.setValue(i);
			qApp->processEvents();
			if (progress.wasCanceled()){
				progress.close();
				break;
			}
			DeleteMailByID(g_lstMailsToDelete[i].length(), g_lstMailsToDelete[i].data());
		}

		progress.close();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Going to Uninitialize MAPI (Logoff)!");

	typedef void (*tUninitTest)();
	tUninitTest Uninitialize = (tUninitTest)g_MapiDLL.resolve("Uninitialize");
	if(!Uninitialize)
	{
		QMessageBox::information(NULL, "Info", "Error getting method!");
		return false;
	}
	Uninitialize();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Uninitialize MAPI done!");

	g_MapiDLL.unload();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"MAPIEx dll unloaded!");

	return true;
}


bool MapiManager::OpenMailInOutlook(QByteArray arData, QString strExternApp, bool bSupressMsg, bool bModal)
{

	if(arData.length() < 1)
	{
		if(!bSupressMsg)QMessageBox::information(NULL, QObject::tr("Error"), QObject::tr("Message does not have Entry ID!"));
		return false;
	}

	if (!LoadMapiDll()) return false;

	typedef bool (*tOpenMailByID)(int, const char *, bool);
	tOpenMailByID OpenMailByID = (tOpenMailByID) g_MapiDLL.resolve("OpenMailByID");
	if(!OpenMailByID) 
	{
		if(!bSupressMsg)QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Error getting method from MAPI dll!"));
		return false;
	}

	bool bRes = OpenMailByID(arData.length(), arData.data(), bModal);
	if(!bRes)
	{
		if(!bSupressMsg)QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Error opening email!"));
		return false;
	}

	return true;
}


bool MapiManager::OpenMailInOutlookFromRecord(DbRecordSet &m_recMail,DbRecordSet &lstAttachments,DbRecordSet &recToRecordSet,DbRecordSet &recCCRecordSet,DbRecordSet &recBCCRecordSet,bool bBodyAsPlainText,bool bSentMode)
{
	//initiate send mail:
	typedef bool (*tSendTest)(const char *, const char *, int, const char **, int, const char **, int, const char **, const char *,const char *,const char *,int, const char **, const char **, int *, const char **, int, int*, char *, int, bool, bool);

	//load only if needed:
	if(!LoadMapiDll()) return false;

	tSendTest SendMail = (tSendTest) g_MapiDLL.resolve("SendMail");
	if(!SendMail) 
	{
		QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Error getting method from MAPI dll!"));
		return false;
	}

	const char **pToArray=NULL;
	const char **pCCArray=NULL;
	const char **pBCCArray=NULL;
	QList<QByteArray> lstTo;
	QList<QByteArray> lstCC;
	QList<QByteArray> lstBCC;

	//-----------------------------MANAGE TO, CC, BCC---------------------------------------
	int nEmailIdx=recToRecordSet.getColumnIdx("CONTACT_EMAIL");
	int nNameIdx=recToRecordSet.getColumnIdx("CONTACT_NAME");

	int nSizeTo=recToRecordSet.getRowCount();
	if (nSizeTo>0)
	{
		pToArray = new const char*[nSizeTo];
		for(int i=0;i<nSizeTo;++i)
		{
			QString strEmail = recToRecordSet.getDataRef(i,nEmailIdx).toString();
			EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters

			lstTo << strEmail.toLocal8Bit();
			pToArray[i]=lstTo.at(lstTo.size()-1).constData();
		}
	}

	int nSizeCC=recCCRecordSet.getRowCount();
	if (nSizeCC)
	{
		pCCArray = new const char*[nSizeCC];
		for(int i=0;i<nSizeCC;++i)
		{
			QString strEmail = recCCRecordSet.getDataRef(i,nEmailIdx).toString();
			EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters

			lstCC << strEmail.toLocal8Bit();
			pCCArray[i]=lstCC.at(lstCC.size()-1).constData();
		}
	}

	int nSizeBCC=recBCCRecordSet.getRowCount();
	if (nSizeBCC>0)
	{
		pBCCArray = new const char*[nSizeBCC];
		for(int i=0;i<nSizeBCC;++i)
		{
			QString strEmail = recBCCRecordSet.getDataRef(i,nEmailIdx).toString();
			EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters

			lstBCC << strEmail.toLocal8Bit();
			pBCCArray[i]=lstBCC.at(lstBCC.size()-1).constData();
		}
	}

	//-----------------------------MANAGE ATTACHEMTNS---------------------------------------
	const char **pAttachNames=NULL;
	const char **pAttachCIDNames=NULL;
	const char **pAttachContent=NULL;
	int *pAttachContentSize=NULL;

	QList<QByteArray> lstNames,lstCIDNames;
	QList<QByteArray> lstContent;
	QList<int> lstSizes;

	int nSizeAttach=lstAttachments.getRowCount();
	if (nSizeAttach>0)
	{
		pAttachNames=new const char*[nSizeAttach];
		pAttachCIDNames=new const char*[nSizeAttach];
		pAttachContent=new const char*[nSizeAttach];
		pAttachContentSize=new int[nSizeAttach];

		for(int i=0;i<nSizeAttach;++i)
		{

			lstNames<<lstAttachments.getDataRef(i,"BEA_NAME").toString().toLocal8Bit();
			pAttachNames[i]=lstNames.at(lstNames.size()-1).constData();

			lstCIDNames<<lstAttachments.getDataRef(i,"BEA_CID_LINK").toString().toLocal8Bit();
			pAttachCIDNames[i]=lstCIDNames.at(lstNames.size()-1).constData();

			QByteArray binContent=qUncompress(lstAttachments.getDataRef(i,"BEA_CONTENT").toByteArray());
			lstSizes<<binContent.size();
			pAttachContentSize[i]=lstSizes[lstSizes.size()-1];

			lstContent<<binContent;
			pAttachContent[i]=lstContent.at(lstContent.size()-1).constData();
		}
	}



	//Get from name and email.
	QString strSender		= GetEmailFromName(m_recMail.getDataRef(0,"BEM_FROM").toString());
	QString strSenderName	= GetContactFromName(m_recMail.getDataRef(0,"BEM_FROM").toString());
	QString strHtmlBody="";	
	QString strPlainBody="";


	//BT if changed, get from HTML, else from DB directly
	//if(m_recMail.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
	//BEM_BODY is prepared: if changed in our window then it will contain html, else plain txt
	if (bBodyAsPlainText)
		strPlainBody	= m_recMail.getDataRef(0,"BEM_BODY").toString();
	else
		strHtmlBody		= m_recMail.getDataRef(0,"BEM_BODY").toString();

	//QByteArray dd=m_recMail.getDataRef(0,"BEM_BODY").toByteArray();
	//_DUMP_FILE(strHtmlBody.toLatin1());

	//QByteArray body=strHtmlBody.toLocal8Bit().constData();
	//CommunicationManager::SaveFileContent(body,"D:/mail_body_before_send.html");

	QString strSubject		= m_recMail.getDataRef(0,"BEM_SUBJECT").toString();
	/*
	if (strSubject.isEmpty())
	{
	strSubject = QInputDialog::getText(this, QObject::tr("Message Subject!"), QObject::tr("Please enter message subject"), QLineEdit::Normal,	QObject::tr("(Subject)"));
	ui.Subject_lineEdit->setText(strSubject);
	}
	*/

	char szReturnedOutlookID[256]; //returned ID
	int nActualReturnedOutlookIDSize;

	//-----------------------------SEND EMAIL---------------------------------------

	bool bOK=SendMail(
		strSenderName.toLatin1().constData(), 
		strSender.toLatin1().constData(),
		nSizeTo,
		pToArray,
		nSizeCC,
		pCCArray,
		nSizeBCC,
		pBCCArray,
		strSubject.toLatin1().constData(),
		//NULL,
		strPlainBody.toLatin1().constData(),
		//dd.constData(),
		strHtmlBody.toLatin1().constData(),
		nSizeAttach,
		pAttachNames,
		pAttachCIDNames,
		pAttachContentSize,
		pAttachContent,
		256,
		&nActualReturnedOutlookIDSize,
		szReturnedOutlookID,
		IMPORTANCE_NORMAL,
		true,
		!bSentMode);



	if (bOK)
	{
		if (nActualReturnedOutlookIDSize>0)
		{
			QByteArray byteID(szReturnedOutlookID,nActualReturnedOutlookIDSize);
			m_recMail.setData(0,"BEM_EXT_ID",byteID);
			m_recMail.setData(0, "BEM_EXT_APP", "Outlook");

			g_lstMailsToDelete.append(byteID); //to be deleted
		}
	}

	//delete all dynamic objects:
	delete [] pToArray;
	delete [] pCCArray;
	delete [] pBCCArray;
	delete [] pAttachContentSize;
	delete [] pAttachNames;
	delete [] pAttachContent;
	return bOK;
}

QString MapiManager::GetEmailFromName(QString strName)
{
	//If string not contain "<" we will assume that is only email string (no name).
	if (!strName.contains("<"))
		return strName;

	strName.truncate(strName.indexOf(">"));
	strName.remove(0, strName.indexOf("<")+1);
	return strName;
}

QString MapiManager::GetContactFromName(QString strName)
{
	QString str;
	if (!strName.contains("<"))
		return str;

	strName.truncate(strName.indexOf("<"));
	return strName;
}

bool MapiManager::ImportContacts(DbRecordSet &lstTmpImportData, QList<QStringList> lstFolders, int &nContactsImported,QMap<QString, bool> mapSex,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject, CMAPIEx *pMapi)
{
	qDebug() << "MapiManager::ImportContacts";

	//
	// build Outlook contacts list
	//
#if 0 //#ifdef _MAPI_FROM_DLL
	if(!LoadMapiDll())
	{
		qDebug() << "Failed to initialize MAPI\n";
		return false;
	}
	typedef bool (*tOpenContacts)();
	tOpenContacts OpenContacts = (tOpenContacts)g_MapiDLL.resolve("OpenContacts");
	if(!OpenContacts) 
	{
		QMessageBox::information(this, tr("Info"), tr("Error getting method from MAPI dll!"));
		return false;
	}
	bool bRes = OpenContacts();
	if(!bRes){
		QMessageBox::information(this, tr("Info"), tr("Error opening email!"));
		return false;
	}

	typedef bool (*tGetNextContact)(char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, short *);
	tGetNextContact GetNextContact = (tGetNextContact)g_MapiDLL.resolve("GetNextContact");
	if(!GetNextContact) 
	{
		QMessageBox::information(this, tr("Info"), tr("Error getting method from MAPI dll!"));
		return false;
	}

	int nIdx = lstTmpImportData.getRowCount()-1;

	char szFirstName[256];
	char szLastName[256];
	char szEmail[256];
	char szAddrCity[256];
	char szAddrStreet[256];
	char szAddrCountry[256];
	char szAddrRegion[256];
	char szAddrZIP[256];
	char szPhoneBussiness1[256];
	char szPhoneBussiness2[256];
	char szPhoneHome[256];
	char szPhoneMobile[256];
	char szPhoneFax[256];
	char szOrganization[256];
	char szCountry[256];
	char szCity[256];
	char szDescription[4048];
	char szNamePrefix[256];
	char szWebsite[256];
	short  nGender = -1;

	while(GetNextContact(	szFirstName,
		szLastName,
		szEmail,
		szAddrCity,
		szAddrStreet,
		szAddrCountry,
		szAddrRegion,
		szAddrZIP,
		szPhoneBussiness1,
		szPhoneBussiness2,
		szPhoneHome,
		szPhoneMobile,
		szPhoneFax,
		szWebsite,
		szOrganization,
		szCountry,
		szCity,
		szDescription,
		szNamePrefix,
		&nGender)) 
	{
		lstTmpImportData.addRow();
		nIdx ++;

		QString strLastName(szLastName);
		qDebug() << "Last Name: " << strLastName;
		lstTmpImportData.setData(nIdx, "BCNT_LASTNAME", strLastName);

		QString strFirstName(szFirstName);
		qDebug() << "First Name: " << strFirstName;
		lstTmpImportData.setData(nIdx, "BCNT_FIRSTNAME", strFirstName);

		QString strEmail(szEmail);
		qDebug() << "Email: " << strEmail;
		lstTmpImportData.setData(nIdx, "CLC_EMAIL_ADDRESS", strEmail);

		qDebug() << "Address: " << szAddrStreet << szAddrCity << szAddrRegion << szAddrCountry << szAddrZIP;
		lstTmpImportData.setData(nIdx, "BCMA_STREET", QString(szAddrStreet).replace("\r", "").replace("\n", ""));
		lstTmpImportData.setData(nIdx, "BCMA_CITY", QString(szAddrCity).replace("\r", "").replace("\n", ""));
		lstTmpImportData.setData(nIdx, "BCMA_COUNTRY_NAME", QString(szAddrCountry).replace("\r", "").replace("\n", ""));
		lstTmpImportData.setData(nIdx, "BCMA_REGION", QString(szAddrRegion).replace("\r", "").replace("\n", ""));
		lstTmpImportData.setData(nIdx, "BCMA_ZIP", QString(szAddrZIP).replace("\r", "").replace("\n", ""));

		QString strPhone(szPhoneBussiness1);
		qDebug() << "Phone: " << strPhone;
		lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_DIRECT", strPhone);

		strPhone = szPhoneBussiness2;
		qDebug() << "Phone1: " << strPhone;
		lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_CENTRAL", strPhone);

		strPhone = szPhoneHome;
		qDebug() << "Phone2: " << strPhone;
		lstTmpImportData.setData(nIdx, "CLC_PHONE_PRIVATE", strPhone);

		strPhone = szPhoneMobile;
		qDebug() << "Phone3: " << strPhone;
		lstTmpImportData.setData(nIdx, "CLC_PHONE_MOBILE", strPhone);

		strPhone = szPhoneFax;
		qDebug() << "Fax: " << strPhone;
		lstTmpImportData.setData(nIdx, "CLC_PHONE_FAX", strPhone);

		strPhone = szWebsite;
		qDebug() << "Website: " << strPhone;
		lstTmpImportData.setData(nIdx, "CLC_WEB_ADDRESS", strPhone);

		QString strCompany(szOrganization);
		qDebug() << "Company: " << strCompany;
		lstTmpImportData.setData(nIdx, "BCMA_ORGANIZATIONNAME", strCompany);

		QString strCountry(szCountry);
		qDebug() << "Country: " << strCountry;
		lstTmpImportData.setData(nIdx, "BCMA_COUNTRY_NAME", strCountry);

		QString strText(szDescription);
		qDebug() << "Notes: " << strText;
		lstTmpImportData.setData(nIdx, "BCNT_DESCRIPTION", strText);

		QString strCity(szCity);
		qDebug() << "City: " << strCity;
		lstTmpImportData.setData(nIdx, "BCMA_CITY", strCity);

		if(nGender > 0)
		{
			if(2 == nGender)
				lstTmpImportData.setData(nIdx, "BCNT_SEX", CONTACT_SEX_MALE);
			else if(1 == nGender)
				lstTmpImportData.setData(nIdx, "BCNT_SEX", CONTACT_SEX_FEMALE);
		}
		else
		{
			//try to guess the sex by person title
			strText = szNamePrefix;
			qDebug() << "Title: " << strText;
			if(!strText.isEmpty())
			{
				QMap<QString, bool>::const_iterator it = m_mapSex.find(strText);
				if(it != m_mapSex.end())
				{
					if(true == it.value())
						lstTmpImportData.setData(nIdx, "BCNT_SEX", CONTACT_SEX_MALE);
					else
						lstTmpImportData.setData(nIdx, "BCNT_SEX", CONTACT_SEX_FEMALE);
				}
			}
		}

		//if(!OnNewRowAdded(nIdx)) nIdx--;	// handle item filtering
		if (!pfCallBack_OnNewRowAdded(nCallerObject,nIdx))nIdx--;
	}

#endif//#ifdef _MAPI_FROM_DLL

	/*
	int nFoldersCnt = lstFolders.count();
	if(nFoldersCnt < 1){
		QMessageBox::information(NULL, "Error", QObject::tr("There are no folders selected!"));
		return false;
	}
	*/
	if(!pMapi) 
	{
		Q_ASSERT(false);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "ImportContacts: Invalid MAPI handle");
		qDebug() << "Failed to initialize MAPI\n";
		return false;
	}

	//for(int i=0; i<nFoldersCnt; i++)
	//{
		//B.T. changed on 2016-01-10: go right into head: open contacts and import it...
		LPMAPIFOLDER lpFolder = pMapi->OpenContacts(false);
		//LPMAPIFOLDER lpFolder = OpenOutlookFolder(*pMapi, lstFolders[i]);
		if(lpFolder)
		{
			pMapi->GetContents(lpFolder);
			pMapi->SortContents(TABLE_SORT_ASCEND,PR_SUBJECT);

			CMAPIContact contact;
			while(pMapi->GetNextContact(contact)) 
			{
				//QString strContactName;
				//contact.GetName(strContactName);
				//if(!strContactName.isEmpty())
					if(AddContact(lstTmpImportData,contact,mapSex,pfCallBack_OnNewRowAdded,nCallerObject))
						nContactsImported++;
			}

			RELEASE(lpFolder);
		}
	//}
	return true;
}

bool MapiManager::AddContact(DbRecordSet &lstTmpImportData, CMAPIContact &contact,QMap<QString, bool> mapSex,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject)
{
	qDebug() << "MapiManager::AddContact";

	QString strText;

	lstTmpImportData.addRow();
	int nIdx = lstTmpImportData.getRowCount()-1;

	contact.GetName(strText);

	QString strLastName;
	contact.GetPropertyString(strLastName, PR_SURNAME);
	qDebug() << "Last Name: " << strLastName;
	lstTmpImportData.setData(nIdx, "BCNT_LASTNAME", strLastName);

	QString strFirstName;
	contact.GetPropertyString(strFirstName, PR_GIVEN_NAME);
	qDebug() << "First Name: " << strFirstName;
	lstTmpImportData.setData(nIdx, "BCNT_FIRSTNAME", strFirstName);

	QString strEmail;
	contact.GetEmail(strEmail);
	qDebug() << "Email: " << strEmail;
	lstTmpImportData.setData(nIdx, "CLC_EMAIL_ADDRESS", strEmail);

	CContactAddress address;
	contact.GetAddress(address,CContactAddress::BUSINESS);
	qDebug() << "Business Address: " << address.m_strStreet << address.m_strCity << address.m_strStateOrProvince << address.m_strCountry << address.m_strPostalCode;
	lstTmpImportData.setData(nIdx, "BCMA_STREET", address.m_strStreet);
	lstTmpImportData.setData(nIdx, "BCMA_CITY", address.m_strCity);
	lstTmpImportData.setData(nIdx, "BCMA_COUNTRY_NAME", address.m_strCountry);
	lstTmpImportData.setData(nIdx, "BCMA_REGION", address.m_strStateOrProvince);
	lstTmpImportData.setData(nIdx, "BCMA_ZIP", address.m_strPostalCode);
	lstTmpImportData.setData(nIdx, "BCMA_TITLE", address.m_strTitle);

	QString strPhone;
	contact.GetPhoneNumber(strPhone,PR_BUSINESS_TELEPHONE_NUMBER);
	qDebug() << "Phone: " << strPhone;
	lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_DIRECT", strPhone);

	contact.GetPhoneNumber(strPhone,PR_BUSINESS2_TELEPHONE_NUMBER);
	qDebug() << "Phone1: " << strPhone;
	lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_CENTRAL", strPhone);

	contact.GetPhoneNumber(strPhone,PR_HOME_TELEPHONE_NUMBER);
	qDebug() << "Phone2: " << strPhone;
	lstTmpImportData.setData(nIdx, "CLC_PHONE_PRIVATE", strPhone);

	contact.GetPhoneNumber(strPhone,PR_MOBILE_TELEPHONE_NUMBER);
	qDebug() << "Phone3: " << strPhone;
	lstTmpImportData.setData(nIdx, "CLC_PHONE_MOBILE", strPhone);

	contact.GetPhoneNumber(strPhone,PR_PRIMARY_FAX_NUMBER);
	qDebug() << "Fax: " << strPhone;
	lstTmpImportData.setData(nIdx, "CLC_PHONE_FAX", strPhone);

	QString strCompany;
	contact.GetCompany(strCompany);
	qDebug() << "Company: " << strCompany;
	lstTmpImportData.setData(nIdx, "BCMA_ORGANIZATIONNAME", strCompany);

	QString strCountry;
	contact.GetCountry(strCountry);
	qDebug() << "Country: " << strCountry;
	lstTmpImportData.setData(nIdx, "BCMA_COUNTRY_NAME", strCountry);

	contact.GetNotes(strText,FALSE);
	qDebug() << "Notes: " << strText;
	lstTmpImportData.setData(nIdx, "BCNT_DESCRIPTION", strText);

	QString strCity;
	contact.GetCity(strCity);
	qDebug() << "City: " << strCity;
	lstTmpImportData.setData(nIdx, "BCMA_CITY", strCity);

	QString strFunction;
	contact.GetPropertyString(strFunction, PR_TITLE);
	qDebug() << "Function: " << strFunction;
	lstTmpImportData.setData(nIdx, "BCNT_FUNCTION", strFunction);
	

	short int nGender = -1;
	if(contact.GetPropertyShort(nGender, PR_GENDER))
	{
		if(2 == nGender)
			lstTmpImportData.setData(nIdx, "BCNT_SEX", CONTACT_SEX_MALE);
		else if(1 == nGender)
			lstTmpImportData.setData(nIdx, "BCNT_SEX", CONTACT_SEX_FEMALE);
	}
	else
	{
		//try to guess the sex by person title
		contact.GetPropertyString(strText, PR_DISPLAY_NAME_PREFIX);
		qDebug() << "Name Prefix: " << strText;
		if(!strText.isEmpty())
		{
			QMap<QString, bool>::const_iterator it = mapSex.find(strText);
			if(it != mapSex.end())
			{
				if(true == it.value())
					lstTmpImportData.setData(nIdx, "BCNT_SEX", CONTACT_SEX_MALE);
				else
					lstTmpImportData.setData(nIdx, "BCNT_SEX", CONTACT_SEX_FEMALE);
			}
		}
	}

	if (!pfCallBack_OnNewRowAdded(nCallerObject,nIdx))
	//if(!OnNewRowAdded(nIdx))
	{
		nIdx--;	// handle item filtering
		return false;
	}

	//now check the other non-empty addresses
	CContactAddress address1;
	contact.GetAddress(address1, CContactAddress::HOME);
	//contact.GetPropertyString(address.m_strStreet, PR_BUSINESS_ADDRESS_STREET);
	//contact.GetPropertyString(strTest, PR_HOME_ADDRESS_STREET);

	if( !address1.m_strStreet.isEmpty() || 
		!address1.m_strCity.isEmpty() ||
		!address1.m_strStateOrProvince.isEmpty() || 
		!address1.m_strCountry.isEmpty() || 
		!address1.m_strPostalCode.isEmpty()	)
	{
		qDebug() << "Address home: " << address1.m_strStreet << address1.m_strCity << address1.m_strStateOrProvince << address1.m_strCountry << address1.m_strPostalCode;

		lstTmpImportData.addRow();
		int nNewIdx = lstTmpImportData.getRowCount()-1;
		lstTmpImportData.assignRow(nNewIdx, lstTmpImportData.getRow(nIdx));
		nIdx = nNewIdx;

		lstTmpImportData.setData(nIdx, "CLC_IS_ADD_ON", 1); //add on row
		lstTmpImportData.setData(nIdx, "BCMA_STREET", address1.m_strStreet);
		lstTmpImportData.setData(nIdx, "BCMA_CITY", address1.m_strCity);
		lstTmpImportData.setData(nIdx, "BCMA_COUNTRY_NAME", address1.m_strCountry);
		lstTmpImportData.setData(nIdx, "BCMA_REGION", address1.m_strStateOrProvince);
		lstTmpImportData.setData(nIdx, "BCMA_ZIP", address1.m_strPostalCode);
		lstTmpImportData.setData(nIdx, "BCMA_TITLE", address1.m_strTitle);
		
		//clear phone fields so we don't create duplicate phones
		lstTmpImportData.setData(nIdx, "CLC_EMAIL_ADDRESS", ""); 
		lstTmpImportData.setData(nIdx, "CLC_EMAIL_ADDRESS_DESC", "");
		lstTmpImportData.setData(nIdx, "CLC_WEB_ADDRESS", "");
		lstTmpImportData.setData(nIdx, "CLC_WEB_ADDRESS_DESC", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_CENTRAL", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_DIRECT", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_MOBILE", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_FAX", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_PRIVATE", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_PRIVATE_MOBILE", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_SKYPE", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_GENERIC", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_GENERIC_NAME", "");
		lstTmpImportData.setData(nIdx, "CLC_GROUPS", "");
	}

	CContactAddress address2;
	contact.GetAddress(address2,CContactAddress::OTHER);
	if( !address2.m_strStreet.isEmpty() || 
		!address2.m_strCity.isEmpty() ||
		!address2.m_strStateOrProvince.isEmpty() || 
		!address2.m_strCountry.isEmpty() || 
		!address2.m_strPostalCode.isEmpty()	)
	{
		qDebug() << "Address other: " << address2.m_strStreet << address2.m_strCity << address2.m_strStateOrProvince << address2.m_strCountry << address2.m_strPostalCode;

		lstTmpImportData.addRow();
		int nNewIdx = lstTmpImportData.getRowCount()-1;
		lstTmpImportData.assignRow(nNewIdx, lstTmpImportData.getRow(nIdx));

		nIdx = nNewIdx;

		lstTmpImportData.setData(nIdx, "CLC_IS_ADD_ON", 1); //add on row
		lstTmpImportData.setData(nIdx, "BCMA_STREET", address2.m_strStreet);
		lstTmpImportData.setData(nIdx, "BCMA_CITY", address2.m_strCity);
		lstTmpImportData.setData(nIdx, "BCMA_COUNTRY_NAME", address2.m_strCountry);
		lstTmpImportData.setData(nIdx, "BCMA_REGION", address2.m_strStateOrProvince);
		lstTmpImportData.setData(nIdx, "BCMA_ZIP", address2.m_strPostalCode);
		lstTmpImportData.setData(nIdx, "BCMA_TITLE", address2.m_strTitle);

		//clear phone fields so we don't create duplicate phones
		lstTmpImportData.setData(nIdx, "CLC_EMAIL_ADDRESS", ""); 
		lstTmpImportData.setData(nIdx, "CLC_EMAIL_ADDRESS_DESC", "");
		lstTmpImportData.setData(nIdx, "CLC_WEB_ADDRESS", "");
		lstTmpImportData.setData(nIdx, "CLC_WEB_ADDRESS_DESC", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_CENTRAL", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_DIRECT", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_MOBILE", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_FAX", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_PRIVATE", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_PRIVATE_MOBILE", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_SKYPE", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_GENERIC", "");
		lstTmpImportData.setData(nIdx, "CLC_PHONE_GENERIC_NAME", "");
		lstTmpImportData.setData(nIdx, "CLC_GROUPS", "");
	}

	//_DUMP(lstTmpImportData);
	return true; //added
}

LPMAPIFOLDER MapiManager::OpenOutlookFolder(CMAPIEx &mapi, QStringList &lstFolderPath)
{
	qDebug() << "MapiManager::OpenOutlookFolder";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder start");

	int nDepth = lstFolderPath.count();
	if(nDepth < 1)
		return NULL;

	//open the correct message store first
	//(top level name is a message store name)
	bool bOK = false;
	int nCnt = mapi.GetMessageStoreCount();
	for(int i=0; i<nCnt; i++){
		QString strStoreName;
		if(mapi.OpenMessageStore(i, strStoreName)){
			if(lstFolderPath[0] == strStoreName){
				bOK = true;
				break;
			}
		}
	}
	if(!bOK){
		//try to find from root:
		if(mapi.OpenRootFolder())
		{
			//LPMAPIFOLDER lpMapiFolder = mapi.OpenSubFolder((LPCTSTR)lstFolderPath.at(0).toLocal8Bit().constData());
			//LPCSTR lstr = lstFolderPath.at(0).toStdString().c_str();
			//LPMAPIFOLDER lpMapiFolder = mapi.OpenSubFolder(L"Contacts");
			LPMAPIFOLDER lpMapiFolder = mapi.OpenSubFolder((LPCTSTR)lstFolderPath.at(0).utf16());
			if(lpMapiFolder){
				return lpMapiFolder;
			}
		}
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder end (failed to find message store)");
		return NULL;
	}

	//open root folder in the selected message store
	LPMAPIFOLDER lpMapiFolder = mapi.OpenRootFolder(false);
	if(!lpMapiFolder){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder end (failed to open root folder)");
		return NULL;
	}

	//find folder, traveling level by level into the deep
	LPMAPIFOLDER lpParentFolder = lpMapiFolder;
	int nLevel = 1;
	while(nLevel < nDepth){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("OpenOutlookFolder: find hierarchy at level %1").arg(nLevel));
		mapi.GetHierarchy(lpParentFolder);

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder: match folder names at current level");

		//get folder pointer at this level (match by name)
		QString strFolder;
		LPMAPIFOLDER lpFolder = mapi.GetNextSubFolder(strFolder, lpParentFolder);
		while(NULL != lpFolder && strFolder != lstFolderPath[nLevel]){
			RELEASE(lpFolder);
			lpFolder = mapi.GetNextSubFolder(strFolder, lpParentFolder);
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder: check if result found");

		LPMAPIFOLDER lpNewParent = lpFolder;
		if(!lpNewParent){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder (failed to find folder at current level)");
			RELEASE(lpParentFolder);
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder end");
			return NULL;	//error, not found
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder: folder segment found for this level");

		RELEASE(lpParentFolder);
		lpParentFolder = lpNewParent;
		nLevel ++;
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, "OpenOutlookFolder: level scan done");
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"OpenOutlookFolder end");
	return lpParentFolder;
}

bool MapiManager::GetOutlookFolders(int *nPIDs,char **szNames)
{
	if(!MapiManager::LoadMapiDll())
	{
		qDebug() << "Failed to initialize MAPI\n";
		return false;
	}
	typedef int (*tEmail_GetFoldersCount)();
	tEmail_GetFoldersCount Email_GetFoldersCount = (tEmail_GetFoldersCount)g_MapiDLL.resolve("Email_GetFoldersCount");
	if(!Email_GetFoldersCount) 
	{
		QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Error getting method from MAPI dll!"));
		return false;
	}
	int nCount = Email_GetFoldersCount();

	typedef bool (*tEmail_FillFolderInfo)(int *, char **);
	tEmail_FillFolderInfo Email_FillFolderInfo = (tEmail_FillFolderInfo)g_MapiDLL.resolve("Email_FillFolderInfo");
	if(!Email_FillFolderInfo) 
	{
		QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Error getting method from MAPI dll!"));
		return false;
	}

	return Email_FillFolderInfo(nPIDs, szNames);
}

bool MapiManager::ImportEmails(DbRecordSet &lstTmpImportData, QList<QStringList> lstFolders, int &nEmailsImported,QSet<QString> &setEmails,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject, CMAPIEx *pMapi)
{
	qDebug() << "MapiManager::ImportEmails";

	nEmailsImported=0;
	int nFoldersCnt = lstFolders.count();
	if(nFoldersCnt < 1){
		QMessageBox::information(NULL, QObject::tr("Error"), QObject::tr("There are no folders selected!"));
		return false;
	}

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	if(!pMapi) {
		Q_ASSERT(false);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "ImportEmails: Invalid MAPI handle");
		QApplication::restoreOverrideCursor();
		QMessageBox::information(NULL, QObject::tr("Error"), QObject::tr("Failed to initialize MAPI!"));
		return false;
	}

	QSize size(300,100);
	QProgressDialog progress(QObject::tr("Listing Emails..."),QObject::tr("Cancel"),0,0,QApplication::activeWindow());
	progress.setWindowTitle(QObject::tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	QString strOurEmail;
	#if 0
		QStringList lstPersEmails;
		mapi.GetPersonalEmails(lstPersEmails);
		strOurEmail = (lstPersEmails.size() > 0)? lstPersEmails.at(0) : "";
	#endif
	
	lstTmpImportData.clear();

	int nCounter = 0;
	bool bSkip = false;

	for(int i=0; i<nFoldersCnt; i++)
	{
		if(bSkip)
			break;

		LPMAPIFOLDER lpFolder = OpenOutlookFolder(*pMapi, lstFolders[i]);
		if(lpFolder)
		{
			pMapi->GetContents(lpFolder);
			pMapi->SortContents(TABLE_SORT_ASCEND,PR_SUBJECT);

			QString strText;
			CMAPIMessage message;
			while(pMapi->GetNextMessage(message)) 
			{
				AddEmail(lstTmpImportData,message, nCounter, progress, bSkip, setEmails, false,pfCallBack_FilterMatchRow,nCallerObject);
				nEmailsImported++;
				if(bSkip){
					break;
				}
			}
			RELEASE(lpFolder);
		}
	} // for

	QApplication::restoreOverrideCursor();
	return true;
}

void MapiManager::AddEmail(DbRecordSet &lstData, CMAPIMessage &message, int &nCounter, QProgressDialog &progress, bool &bSkip, QSet<QString> &setEmails, bool bAreTemplates,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject)
{
	qDebug() << "MapiManager::AddEmail";

	int nIdx = lstData.getRowCount()-1;

	if(nCounter%4 == 0)
	{
		//update progress
		progress.setValue(1);
		//qApp->processEvents(); //FIX: causes crash
		if (progress.wasCanceled())
		{
			QMessageBox::information(NULL, QObject::tr("Information"), QObject::tr("Operation stopped by user request!"));
			bSkip = true;
			return;
		}
	}

	nCounter ++;

	lstData.addRow();
	nIdx ++;

	QSet<QString> setTmp;	// temp email set (we don't know yet if this msg will pass the filter)
	setTmp.insert(message.GetSenderEmail());

	QString strData = message.GetSenderName();
	if(!message.GetSenderEmail().isEmpty()){
		strData += " <";
		strData += message.GetSenderEmail();
		strData += ">";
	}

	if(bAreTemplates)
	{
		lstData.setData(nIdx, "BEM_TEMPLATE_FLAG", 1);
		lstData.setData(nIdx, "BEM_TEMPLATE_NAME", message.GetSubject());
	}
	else
		lstData.setData(nIdx, "BEM_FROM", strData);

	lstData.setData(nIdx, "BEM_SUBJECT", message.GetSubject());

	//fix date to local date
	QDateTime dtRev = message.GetReceivedTime();
	dtRev.setTimeSpec(Qt::UTC);
	QDateTime tmpDat=dtRev.toLocalTime();
	lstData.setData(nIdx, "BEM_RECV_TIME", tmpDat);

	if(message.GetRecipients())
	{
		if(!bAreTemplates)
		{
			QString strName, strEmail;
			int nType;
			while(message.GetNextRecipient(strName, strEmail, nType))
			{
				//append new recipient
				QString strValue;
				if(1 == nType)
					strValue = lstData.getDataRef(nIdx, "BEM_TO").toString();
				else if(2 == nType)
					strValue = lstData.getDataRef(nIdx, "BEM_CC").toString();
				else if(3 == nType)
					strValue = lstData.getDataRef(nIdx, "BEM_BCC").toString();

				if(!strValue.isEmpty())
					strValue += ";";

				QString strMail(strName);
				if(!strEmail.isEmpty()){
					strMail += " <";
					strMail += strEmail;
					strMail += ">";
				}

				strValue += strMail;

				//qDebug() << "Email" << strEmail;
				setTmp.insert(strEmail);

				//store back the value
				if(1 == nType)
					lstData.setData(nIdx, "BEM_TO", strValue);
				else if(2 == nType)
					lstData.setData(nIdx, "BEM_CC", strValue);
				else if(3 == nType)
					lstData.setData(nIdx, "BEM_BCC", strValue);
			}
		}
	}

	//row that does not match filter will be removed
	if(!pfCallBack_FilterMatchRow(nCallerObject,nIdx))
	{
		lstData.deleteRow(nIdx);
		return;
	}

	//append emails from this mail
	setEmails += setTmp;

	lstData.setData(nIdx, "BEM_UNREAD_FLAG", message.IsUnread() ? 1 : 0);

	//FIX: GetBody returns empty string (or string with one space inside) too frequently
	//QString strRTF1 = message.GetBody();
	QString strRTF = message.GetRTF();

	if(!strRTF.isEmpty()){
		//can still be text-only, check mail type
		bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0);
		lstData.setData(nIdx, "BEM_BODY", strRTF);
		lstData.setData(nIdx, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);
	}
	else{
		lstData.setData(nIdx, "BEM_BODY", message.GetBody());
		lstData.setData(nIdx, "BEM_EMAIL_TYPE", 0);

		if(message.GetBody().isEmpty()){
			//test if this is an appointment reply
			CMAPIAppointment appointment;
			if(appointment.Open(message.GetMapiEx(), *message.GetEntryID()))
			{
				int j=0;
			}
		}
	}

	//store attachments
	DbRecordSet lstAttachments;
	lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
	int nAttCount = message.GetAttachmentCount();
	for(int i=0; i<nAttCount; i++){
		lstAttachments.addRow();
		lstAttachments.setData(i, "BEA_NAME", message.GetAttachmentName(i));
		QByteArray att = message.GetAttachmentData(i);
		//B.T.: compress attachment, damn it:
		att=qCompress(att,1); //min compression
		lstAttachments.setData(i, "BEA_CONTENT", att);
		lstAttachments.setData(i, "BEA_CID_LINK", message.GetAttachmentCID(i));
		//qDebug() << "Attachment CID:" << message.GetAttachmentCID(i);
	}
	lstData.setData(nIdx, "ATTACHMENTS", lstAttachments);
	lstData.setData(nIdx, "ATTACHMENTS_SIZE", lstAttachments.getStoredByteSize());

	//TOFIX: precise algorithm to detect if this is incoming mail (flags + mail checking)
	QString strFrom = lstData.getDataRef(nIdx, "BEM_FROM").toString();
	message.GetStringProperty(strData, PR_TRANSPORT_MESSAGE_HEADERS);

	bool bIncoming = true;
	//email header lines must be empty for outgoing
	if( strData.isEmpty() /*&&
						  !strOurEmail.isEmpty() && strFrom.indexOf(strOurEmail, 0, Qt::CaseInsensitive) >= 0*/)
						  bIncoming = false;	//our mail

	if(bIncoming) // message.IsIncoming()
		lstData.setData(nIdx, "BEM_OUTGOING", 0);
	else
		lstData.setData(nIdx, "BEM_OUTGOING", 1);

	//qDebug() << "From:" << lstData.getDataRef(nIdx, "BEM_FROM").toString();
	//qDebug() << "To:"   << lstData.getDataRef(nIdx, "BEM_TO").toString();
	//qDebug() << "CC:"   << lstData.getDataRef(nIdx, "BEM_CC").toString();
	//qDebug() << "BCC:"  << lstData.getDataRef(nIdx, "BEM_BCC").toString();
	//qDebug() << "Headers:"	   << strData;
	//qDebug() << "Subject:"	   << lstData.getDataRef(nIdx, "BEM_SUBJECT").toString();
	//qDebug() << "IsOutgoing:"  << lstData.getDataRef(nIdx, "BEM_OUTGOING").toInt();

	//lstData.setData(nIdx, "CENT_IS_PRIVATE", 0);

	//store entry ID
	QByteArray arEntry(((const char *)message.GetEntryID()->lpb), message.GetEntryID()->cb);
	lstData.setData(nIdx, "BEM_EXT_ID", arEntry);

#ifdef _DEBUG
#if 0
	QFile file("D:\\aaa.txt");
	if(file.open(QIODevice::WriteOnly))
		file.write(arEntry);
#endif
#endif

	//store app name
	lstData.setData(nIdx, "BEM_EXT_APP", "Outlook");
}

bool MapiManager::ListAppointments(DbRecordSet &lstTmpImportData, QList<QStringList> lstFolders, int &nEmailsImported,QSet<QString> &setEmails,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject, CMAPIEx *pMapi)
{
	qDebug() << "MapiManager::ListAppointments";

	nEmailsImported=0;
	int nFoldersCnt = lstFolders.count();
	if(nFoldersCnt < 1){
		QMessageBox::information(NULL, QObject::tr("Error"), QObject::tr("There are no folders selected!"));
		return false;
	}

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	if(!pMapi || !pMapi->OpenMessageStore()) {
		Q_ASSERT(false);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "ListAppointments: Invalid MAPI handle");
		QApplication::restoreOverrideCursor();
		QMessageBox::information(NULL, QObject::tr("Error"), QObject::tr("Failed to initialize MAPI!"));
		return false;
	}

	QSize size(300,100);
	QProgressDialog progress(QObject::tr("Listing Appointments..."),QObject::tr("Cancel"),0,0,QApplication::activeWindow());
	progress.setWindowTitle(QObject::tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	QString strOurEmail;
	#if 0
		QStringList lstPersEmails;
		mapi.GetPersonalEmails(lstPersEmails);
		strOurEmail = (lstPersEmails.size() > 0)? lstPersEmails.at(0) : "";
	#endif
	
	lstTmpImportData.clear();

	int nCounter = 0;
	bool bSkip = false;

	for(int i=0; i<nFoldersCnt; i++)
	{
		if(bSkip)
			break;

		LPMAPIFOLDER lpFolder = OpenOutlookFolder(*pMapi, lstFolders[i]); //mapi.OpenCalendar()
		if(lpFolder)
		{
			pMapi->GetContents(lpFolder);
			pMapi->SortContents(TABLE_SORT_ASCEND,PR_SUBJECT);

			CMAPIAppointment appointment;
			while (pMapi->GetNextAppointment(appointment))
			{
				AddAppointment(lstTmpImportData, appointment, nCounter, progress, bSkip, setEmails, false, pfCallBack_FilterMatchRow,nCallerObject);
				nEmailsImported++;
				if(bSkip){
					break;
				}
				nCounter ++;
			}
			RELEASE(lpFolder);
		}
		else{
			Q_ASSERT(false);
			qDebug() << "Failed to open MAPI folder" << lstFolders[i];
		}
	} // for

	QApplication::restoreOverrideCursor();
	return true;
}

void MapiManager::AddAppointment(DbRecordSet &lstData, CMAPIAppointment &appointment, int &nCounter, QProgressDialog &progress, bool &bSkip, QSet<QString> &setEmails, bool bAreTemplates,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject)
{
	qDebug() << "MapiManager::AddAppointment";

	int nIdx = lstData.getRowCount()-1;

	if(nCounter%4 == 0)
	{
		//update progress
		progress.setValue(1);
		//qApp->processEvents(); //FIX: causes crash
		if (progress.wasCanceled())
		{
			QMessageBox::information(NULL, QObject::tr("Information"), QObject::tr("Operation stopped by user request!"));
			bSkip = true;
			return;
		}
	}

	nCounter ++;

	lstData.addRow();
	nIdx ++;

	QSet<QString> setTmp;	// temp email set (we don't know yet if this msg will pass the filter)
	//setTmp.insert(appointment.GetSenderEmail());

	//QString strData = message.GetSenderName();
	/*if(!message.GetSenderEmail().isEmpty()){
		strData += " <";
		strData += message.GetSenderEmail();
		strData += ">";
	}*/

	QString strSubject;
	appointment.GetSubject(strSubject);
	lstData.setData(nIdx, "BEM_SUBJECT", strSubject);

	//fix date to local date
	QDateTime dtRev = appointment.GetStartDate(); //appointment.GetReceivedTime();
	dtRev.setTimeSpec(Qt::UTC);
	QDateTime tmpDat=dtRev.toLocalTime();
	lstData.setData(nIdx, "BEM_RECV_TIME", tmpDat);
	qDebug() << "Appointment date:" << tmpDat;


	if(appointment.GetRecipients())
	{
		if(!bAreTemplates)
		{
			QString strName, strEmail;
			int nType, nResponseStatus;
			while(appointment.GetNextRecipient(strName, strEmail, nType, nResponseStatus))
			{
				//append new recipient
				QString strValue;
				if(1 == nType)
					strValue = lstData.getDataRef(nIdx, "BEM_TO").toString();
				else if(2 == nType)
					strValue = lstData.getDataRef(nIdx, "BEM_CC").toString();
				else if(3 == nType)
					strValue = lstData.getDataRef(nIdx, "BEM_BCC").toString();

				if(!strValue.isEmpty())
					strValue += ";";

				QString strMail(strName);
				if(!strEmail.isEmpty()){
					strMail += " <";
					strMail += strEmail;
					strMail += ">";
				}

				strValue += strMail;

				//qDebug() << "Email" << strEmail;
				setTmp.insert(strEmail);

				//store back the value
				if(1 == nType)
					lstData.setData(nIdx, "BEM_TO", strValue);
				else if(2 == nType)
					lstData.setData(nIdx, "BEM_CC", strValue);
				else if(3 == nType)
					lstData.setData(nIdx, "BEM_BCC", strValue);
			}
		}
	}

	//row that does not match filter will be removed
	if( pfCallBack_FilterMatchRow && 
		!pfCallBack_FilterMatchRow(nCallerObject,nIdx))
	{
		lstData.deleteRow(nIdx);
		nIdx --;
		return;
	}

	//append emails from this mail
	setEmails += setTmp;

	QString strRTF;
	appointment.GetBodyHtml(strRTF);
	if(!strRTF.isEmpty()){
		//can still be text-only, check mail type
		bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0);
		lstData.setData(nIdx, "BEM_BODY", strRTF);
		lstData.setData(nIdx, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);
	}
	else{
		appointment.GetBody(strRTF);
		lstData.setData(nIdx, "BEM_BODY", strRTF);
		lstData.setData(nIdx, "BEM_EMAIL_TYPE", 0);
	}

	//TOFIX: precise algorithm to detect if this is incoming mail (flags + mail checking)
	QString strFrom = lstData.getDataRef(nIdx, "BEM_FROM").toString();
//	QString strData;
//	appointment.GetStringProperty(strData, PR_TRANSPORT_MESSAGE_HEADERS);

//	bool bIncoming = true;
	//email header lines must be empty for outgoing
//	if( strData.isEmpty() /*&&
//						  !strOurEmail.isEmpty() && strFrom.indexOf(strOurEmail, 0, Qt::CaseInsensitive) >= 0*/)
//						  bIncoming = false;	//our mail

//	if(bIncoming) // message.IsIncoming()
//		lstData.setData(nIdx, "BEM_OUTGOING", 0);
//	else
//		lstData.setData(nIdx, "BEM_OUTGOING", 1);

	//qDebug() << "From:" << lstData.getDataRef(nIdx, "BEM_FROM").toString();
	//qDebug() << "To:"   << lstData.getDataRef(nIdx, "BEM_TO").toString();
	//qDebug() << "CC:"   << lstData.getDataRef(nIdx, "BEM_CC").toString();
	//qDebug() << "BCC:"  << lstData.getDataRef(nIdx, "BEM_BCC").toString();
	//qDebug() << "Headers:"	   << strData;
	//qDebug() << "Subject:"	   << lstData.getDataRef(nIdx, "BEM_SUBJECT").toString();
	//qDebug() << "IsOutgoing:"  << lstData.getDataRef(nIdx, "BEM_OUTGOING").toInt();

	//lstData.setData(nIdx, "CENT_IS_PRIVATE", 0);

	//store entry ID
	QByteArray arEntry(((const char *)appointment.GetEntryID()->lpb), appointment.GetEntryID()->cb);
	lstData.setData(nIdx, "BEM_EXT_ID", arEntry);

#ifdef _DEBUG
#if 0
	QFile file("D:\\aaa.txt");
	if(file.open(QIODevice::WriteOnly))
		file.write(arEntry);
#endif
#endif

	//store app name
	lstData.setData(nIdx, "BEM_EXT_APP", "Outlook");
}

void MapiManager::GetEmailEntries(QList<QStringList> lstFolders,QList<QByteArray> &lstEntryIDs,QDateTime &dtLastEmail,QDateTime dtLastOutlookScan, CMAPIEx *pMapi)
{
	qDebug() << "MapiManager::GetEmailEntries";

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook GetEmailEntries");

	if(!pMapi){
		Q_ASSERT(false);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "GetEmailEntries: Invalid MAPI handle");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Open Outlook Message Store");
	pMapi->OpenMessageStore();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Process Each Requested Outlook Folder");

	int nFoldersCnt = lstFolders.count();
	for(int i=0; i<nFoldersCnt; i++)
	{
		//debug: print the path of the folder
		QStringList list = lstFolders[i];
		QString strFolderName;
		int nFolderSegments = list.count();
		for(int z=0; z<nFolderSegments; z++){
			if(!strFolderName.isEmpty())
				strFolderName += " / ";
			strFolderName += list[z];
		}
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Open Outlook Folder (%1)").arg(strFolderName));

		LPMAPIFOLDER lpFolder = OpenOutlookFolder(*pMapi, lstFolders[i]);
		if(lpFolder)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook Folder GetContents");

			pMapi->GetContents(lpFolder);

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook Folder SortContents");

			pMapi->SortContents(TABLE_SORT_DESCEND,PR_MESSAGE_DELIVERY_TIME);

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Fetch messages");

			QString strText;
			CMAPIMessage message;
			while(pMapi->GetNextMessage(message)) 
			{
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Message fetched");

				//fix date to local date
				QDateTime dtRev = message.GetReceivedTime();
				dtRev.setTimeSpec(Qt::UTC);
				QDateTime tmpDat=dtRev.toLocalTime();

				if(tmpDat <= dtLastOutlookScan)
					break;	// no more "new" messages

				//first message in a sorted folder has latest date
				if(lstEntryIDs.size() < 1){
					if(dtLastEmail.isValid()){
						if(tmpDat > dtLastEmail)
							dtLastEmail = tmpDat;
					}
					else
						dtLastEmail = tmpDat;
				}

				QString strMessage=QString("New message found:") + message.GetSubject();
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,strMessage);

				//qDebug() << 
				lstEntryIDs << QByteArray(((const char *)message.GetEntryID()->lpb), message.GetEntryID()->cb);

				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Check next message");
			}

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook Folder Processed");
			RELEASE(lpFolder);
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook Folder Released");
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Outlook GetEmailEntries end");
}

bool MapiManager::GenerateEmailsFromDrop(DbRecordSet &lstTmpImportData,QList<QByteArray> lstEntryIDs,QSet<QString> &setEmails,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject, CMAPIEx *pMapi)
{
	qDebug() << "MapiManager::GenerateEmailsFromDrop";

	if(!pMapi){	//|| !pMapi->OpenMessageStore() || !pMapi->OpenRootFolder()
		Q_ASSERT(false);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "GenerateEmailsFromDrop: Invalid MAPI handle");
		return false;
	}

	QSize size(300,100);
	QProgressDialog progress(QObject::tr("Listing Emails..."),QObject::tr("Cancel"),0,0,QApplication::activeWindow());
	progress.setWindowTitle(QObject::tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	int nCounter = 0;
	bool bCancel = false;

	int nMax = lstEntryIDs.count();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("MapiManager::GenerateEmailsFromDrop found %1 entries from drop").arg(nMax));
	for(int i=0; i<nMax; i++)
	{
		//extract entry ID
		SBinary entryID;
		entryID.lpb = (LPBYTE)lstEntryIDs[i].constData();
		entryID.cb	= lstEntryIDs[i].size();

		//open message
		CMAPIMessage message;
		if(message.Open(pMapi, entryID))
		{
			AddEmail(lstTmpImportData,message, nCounter, progress, bCancel, setEmails, false,pfCallBack_FilterMatchRow,nCallerObject);
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("MapiManager::GenerateEmailsFromDrop added email to the list"));
			if(bCancel) break;
		}
		else
		{
			//check if the entry ID is possibly stored in another store?
			int nStoreCnt = pMapi->GetMessageStoreCount();
			for(int i=0; i<nStoreCnt; i++){
				QString strStoreName;
				pMapi->OpenMessageStore(i, strStoreName);
				if(message.Open(pMapi, entryID))
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("MapiManager::GenerateEmailsFromDrop added email to the list from %1 store").arg(strStoreName));
					AddEmail(lstTmpImportData,message, nCounter, progress, bCancel, setEmails, false,pfCallBack_FilterMatchRow,nCallerObject);
					break;
				}
			}
			if(bCancel) break;
		}
		//else
		//	QMessageBox::information(NULL, "", "Failed to open message!");
	}

	pMapi->OpenMessageStore();	//reset store to the default one
	return true;
}

bool MapiManager::IsOutlookInstalled()
{
	qDebug() << "MapiManager::IsOutlookInstalled";

	bool bOutLook = (NULL != MapiManager::InitGlobalMapi());
	return bOutLook;
}

bool MapiManager::GetPersonalEmails(QStringList &lstPersEmails, CMAPIEx *pMapi)
{
	qDebug() << "MapiManager::GetPersonalEmails";

	if(!pMapi){
		Q_ASSERT(false);
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "GetPersonalEmails: Invalid MAPI handle");
		qDebug() << "MapiManager::GetPersonalEmails NO mapi";
		return false;
	}
	
	//mapi.OpenMessageStore();
	pMapi->GetPersonalEmails(lstPersEmails);
	
	qDebug() << "MapiManager::GetPersonalEmails end";
	return true;
}

void MapiManager::GetEntryIDsFromDrop(QByteArray &byteDrop, QList<QByteArray> &lstEntryIDs)
{
	qDebug() << "MapiManager::GetEntryIDsFromDrop";

	//fill the list with email
	int nByteSize = byteDrop.size();
	if(nByteSize > 0)
	{
#ifdef _DEBUG
#if 1
		QFile file("D:\\aaa.txt");
		if(file.open(QIODevice::WriteOnly))
			file.write(byteDrop);
			file.close();
#endif 
#endif
		//marker of the start of the entry ID data
		static const char szTmp[4] = {0x18,0x00,0x00,0x00};
		static const QByteArray arMarker = QByteArray::fromRawData(szTmp, 4);

		int nPos = 0;
		//B.T. 2016-01-10: changed because it does not work for Outlook2013 and above: bytedrop is structured as its explained at: https://github.com/yasoonOfficial/outlook-dndprotocol
		//just before message class (IPM.Note or similar) there is length of message class, then after message class, length of subject then after subject length of the entryID that we are looking for!!
		while((nPos = byteDrop.indexOf(QByteArray("IPM"), nPos)) >= 0)
		{
			//int x = byteDrop.at(nPos-1);
			nPos+=byteDrop.at(nPos-1); //length of message class
			//int y = byteDrop.at(nPos);
			nPos+=2*byteDrop.at(nPos); //length of subject as unicode (*2)
			nPos++; //move to right location as we didn't move before to start of the subject
			int nEntryIDLen=byteDrop.at(nPos); //we will assume that entryID will not be longer then 256 bytes...
			nPos+=4; //set nPos to start od entryID
			if(nPos + nEntryIDLen >= nByteSize){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "MapiManager::GetEntryIDsFromDrop: can not parse byte Outlook entryID from drop!!");
				break;	//no enough data bytes after the marker
			}
			qDebug() << "MapiManager::GetEntryIDsFromDrop found new message entry ID";
			//extract entry ID
			SBinary entryID;
			entryID.lpb = (LPBYTE)byteDrop.constData() + nPos;
			entryID.cb	= nEntryIDLen;

			QByteArray arEntryID = QByteArray::fromRawData((const char*)entryID.lpb, entryID.cb);

#ifdef _DEBUG
#if 1
			QFile file("D:\\entryid.txt");
			if(file.open(QIODevice::WriteOnly))
				file.write(arEntryID);
				file.close();
#endif 
#endif

			lstEntryIDs << arEntryID;
		}
	}
}

bool MapiManager::BuildImportListFromDrop(DbRecordSet &lstTmpImportData,QByteArray &byteDrop,QMap<QString, bool> mapSex,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject, CMAPIEx *pMapi)
{
	qDebug() << "MapiManager::BuildImportListFromDrop (contacts)";

	//fill the grid with email
	if(byteDrop.size() > 0)
	{
#ifdef _DEBUG
#if 1
		QFile file("D:\\aaa.txt");
		if(file.open(QIODevice::WriteOnly))
			file.write(byteDrop);
#endif 
#endif
		if(!pMapi){// || !mapi.OpenMessageStore() || !mapi.OpenRootFolder()){
			Q_ASSERT(false);
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "BuildImportListFromDrop: Invalid MAPI handle");
			return false;
		}

		QSize size(300,100);
		QProgressDialog progress(QObject::tr("Listing Contacts..."),QObject::tr("Cancel"),0,0,QApplication::activeWindow());
		progress.setWindowTitle(QObject::tr("Operation In Progress"));
		progress.setMinimumSize(size);
		progress.setWindowModality(Qt::WindowModal);
		progress.setMinimumDuration(1200);//1.2s before pops up

		int nCounter = 0;
		bool bCancel = false;
		QSet<QString> setEmails;

		QList<QByteArray> lstEntryIDs;
		MapiManager::GetEntryIDsFromDrop(byteDrop,lstEntryIDs);

		int nMax = lstEntryIDs.count();
		for(int i=0; i<nMax; i++)
		{
			//extract entry ID
			SBinary entryID;
			entryID.lpb = (LPBYTE)lstEntryIDs[i].constData();
			entryID.cb	= lstEntryIDs[i].size();

			//open contact
			CMAPIContact contact;
			if(contact.Open(pMapi, entryID)) 
			{
				AddContact(lstTmpImportData,contact,mapSex,pfCallBack_OnNewRowAdded,nCallerObject);
			}
			else{
				//check if the entry ID is possibly stored in another store?
				int nStoreCnt = pMapi->GetMessageStoreCount();
				for(int i=0; i<nStoreCnt; i++){
					QString strStoreName;
					pMapi->OpenMessageStore(i, strStoreName);
					if(contact.Open(pMapi, entryID))
					{
						AddContact(lstTmpImportData,contact,mapSex,pfCallBack_OnNewRowAdded,nCallerObject);
						break;
					}
				}
			}
		}
		return true;
	}
	else
		return false;
}

bool MapiManager::ReadEmailsFromEntryIDs(QList<QByteArray> &lstEntryIDs, QSet<QString> &setEmails,DbRecordSet &lstData, bool bAreTemplates, QProgressDialog *progress, CMAPIEx *pMapi, bool bOnlyOutgoingEmails)
{
	qDebug() << "MapiManager::ReadEmailsFromEntryIDs";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmailsFromEntryIDs - start"));

	if(!pMapi){
		//Q_ASSERT(false);
		//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, "ReadEmailsFromEntryIDs: Invalid MAPI handle");
		//return false;

		//go blindly and believe that someone called this from the main thread
		pMapi = MapiManager::InitGlobalMapi();
	}

	pMapi->OpenMessageStore();

	//define list:
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	lstData.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
	lstData.copyDefinition(set, false);
	lstData.addColumn(QVariant::String, "PROJECT_NAME");
	lstData.addColumn(QVariant::String, "PROJECT_CODE");
	lstData.addColumn(QVariant::String, "EMAIL_UNASSIGNED");
	lstData.addColumn(DbRecordSet::GetVariantType(), "ATTACHMENTS");
	lstData.addColumn(DbRecordSet::GetVariantType(), "CONTACTS");
	lstData.addColumn(DbRecordSet::GetVariantType(), "UAR_TABLE"); //BT: 29.01.2009: added for GUAR
	lstData.addColumn(DbRecordSet::GetVariantType(), "GAR_TABLE");
	lstData.addColumn(QVariant::Int, "ATTACHMENTS_SIZE");


	int nCnt = lstEntryIDs.count();
	int i;
	for(i=0; i<nCnt; i++)
	{
		if(progress)
		{
			if(i%4 == 0)
			{
				//update progress
				progress->setValue(1);
				qApp->processEvents();
				if (progress->wasCanceled()){
					QMessageBox::information(NULL, QObject::tr("Information"), QObject::tr("Operation stopped by user request!"));
					return false;
				}
			}
		}

		//extract entry ID
		SBinary entryID;
		entryID.lpb = (LPBYTE)lstEntryIDs[i].constData();
		entryID.cb	= lstEntryIDs[i].size();

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmailsFromEntryIDs - read message"));

		//open message
		bool bOpened = true;
		CMAPIMessage message;
		if(!message.Open(pMapi, entryID, 0)){
			bOpened = false;
			//check if the entry ID is possibly stored in another store?
			int nStoreCnt = pMapi->GetMessageStoreCount();
			for(int i=0; i<nStoreCnt; i++){
				QString strStoreName;
				pMapi->OpenMessageStore(i, strStoreName);
				if(message.Open(pMapi, entryID))
				{
					bOpened = true;
					break;
				}
			}
		}
		if(bOpened)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmailsFromEntryIDs - parse message"));

			//TOFIX: code similar to InsertEmailToGrid - without filtering
			lstData.addRow();
			int nIdx = lstData.getRowCount()-1;

			//lstData.setData(nIdx, "CENT_IS_PRIVATE", 0);

			QSet<QString> setTmp;	// temp email set (we don't know yet if this msg will pass the filter)
			setTmp.insert(message.GetSenderEmail());

			QString strData = message.GetSenderName();
			if(!message.GetSenderEmail().isEmpty()){
				strData += " <";
				strData += message.GetSenderEmail();
				strData += ">";
			}

			if(bAreTemplates)
			{
				lstData.setData(nIdx, "BEM_TEMPLATE_FLAG", 1);
				lstData.setData(nIdx, "BEM_TEMPLATE_NAME", message.GetSubject());
			}
			else
				lstData.setData(nIdx, "BEM_FROM", strData);

			QString strSubject = message.GetSubject();
			lstData.setData(nIdx, "BEM_SUBJECT", strSubject);

			//fix date to local date
			QDateTime dtRev = message.GetReceivedTime();
			//dtRev.setTimeSpec(Qt::UTC);
			//QDateTime tmpDat=dtRev.toLocalTime();
			//lstData.setData(nIdx, "BEM_RECV_TIME", tmpDat);
			lstData.setData(nIdx, "BEM_RECV_TIME", dtRev);

			if(message.GetRecipients())
			{
				if(!bAreTemplates)
				{
					QString strName, strEmail;
					int nType;
					while(message.GetNextRecipient(strName, strEmail, nType))
					{
						//append new recipient
						QString strValue;
						if(1 == nType)
							strValue = lstData.getDataRef(nIdx, "BEM_TO").toString();
						else if(2 == nType)
							strValue = lstData.getDataRef(nIdx, "BEM_CC").toString();
						else if(3 == nType)
							strValue = lstData.getDataRef(nIdx, "BEM_BCC").toString();

						if(!strValue.isEmpty())
							strValue += ";";

						QString strMail(strName);
						if(!strEmail.isEmpty()){
							strMail += " <";
							strMail += strEmail;
							strMail += ">";
						}

						strValue += strMail;

						setTmp.insert(strEmail);

						//store back the value
						if(1 == nType)
							lstData.setData(nIdx, "BEM_TO", strValue);
						else if(2 == nType)
							lstData.setData(nIdx, "BEM_CC", strValue);
						else if(3 == nType)
							lstData.setData(nIdx, "BEM_BCC", strValue);
					}
				}
			}

			//TOFIX: precise algorithm to detect if this is incoming mail (flags + mail checking)
			QString strFrom = lstData.getDataRef(nIdx, "BEM_FROM").toString();
			message.GetStringProperty(strData, PR_TRANSPORT_MESSAGE_HEADERS);
			QString strRcvdBy;
			message.GetStringProperty(strRcvdBy, PR_RECEIVED_BY_EMAIL_ADDRESS);

			//https://community.zarafa.com/pg/blog/read/15374/webapp-recipient-properties
			//http://blogs.msdn.com/b/stephen_griffin/archive/2013/02/19/exchange-2013-recipient-properties-on-sent-items.aspx
			//http://msdn.microsoft.com/en-us/library/cc839866.aspx
			bool bIncoming = true;
			//email header lines must be empty for outgoing
			if( strData.isEmpty() && 
				strRcvdBy.isEmpty())
			{
				bIncoming = false;	//our mail
			}

			if(bIncoming) // message.IsIncoming()
				lstData.setData(nIdx, "BEM_OUTGOING", 0);
			else
				lstData.setData(nIdx, "BEM_OUTGOING", 1);

			//skip incoming mail if requested
			if(bOnlyOutgoingEmails && bIncoming){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Skip importing email: [%1] (only outgoing mails allowed)").arg(strSubject));
				lstData.deleteRow(nIdx);
				continue;
			}

			//append emails from this mail
			setEmails += setTmp;

			lstData.setData(nIdx, "BEM_UNREAD_FLAG", message.IsUnread() ? 1 : 0);

			//FIX: GetBody returns empty string (or string with one space inside) too frequently
			//QString strRTF1 = message.GetBody();
			QString strRTF = message.GetRTF();
			if(!strRTF.isEmpty()){
				//can still be text-only, check mail type
				bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0);
				lstData.setData(nIdx, "BEM_BODY", strRTF);
				lstData.setData(nIdx, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);
			}
			else{
				lstData.setData(nIdx, "BEM_BODY", message.GetBody());
				lstData.setData(nIdx, "BEM_EMAIL_TYPE", 0);
			}

			//store attachments
			DbRecordSet lstAttachments;
			lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
			int nAttCount = message.GetAttachmentCount();
			if(nAttCount > 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmailsFromEntryIDs - process attachments"));
			}
			for(int i=0; i<nAttCount; i++){

				lstAttachments.addRow();
				lstAttachments.setData(i, "BEA_NAME", message.GetAttachmentName(i));
				QByteArray att = message.GetAttachmentData(i);
				//B.T.: compress attachment, damn it:
				att=qCompress(att,1); //min compression
				lstAttachments.setData(i, "BEA_CONTENT", att);
				lstAttachments.setData(i, "BEA_CID_LINK", message.GetAttachmentCID(i));
				//qDebug() << "Attachment CID:" << message.GetAttachmentCID(i);
			}
			lstData.setData(nIdx, "ATTACHMENTS", lstAttachments);
			lstData.setData(nIdx, "ATTACHMENTS_SIZE", lstAttachments.getStoredByteSize());

			//qDebug() << "From:" << m_lstData.getDataRef(nIdx, "BEM_FROM").toString();
			//qDebug() << "To:"   << m_lstData.getDataRef(nIdx, "BEM_TO").toString();
			//qDebug() << "CC:"   << m_lstData.getDataRef(nIdx, "BEM_CC").toString();
			//qDebug() << "BCC:"  << m_lstData.getDataRef(nIdx, "BEM_BCC").toString();
			//qDebug() << "Headers:"	   << strData;
			//qDebug() << "Subject:"	   << m_lstData.getDataRef(nIdx, "BEM_SUBJECT").toString();
			//qDebug() << "IsOutgoing:"  << m_lstData.getDataRef(nIdx, "BEM_OUTGOING").toInt();

			//lstData.setData(nIdx, "CENT_IS_PRIVATE", 0);

			//store entry ID
			QByteArray arEntry(((const char *)message.GetEntryID()->lpb), message.GetEntryID()->cb);
			lstData.setData(nIdx, "BEM_EXT_ID", arEntry);

#ifdef _DEBUG
#if 0
			QFile file("D:\\aaa.txt");
			if(file.open(QIODevice::WriteOnly))
				file.write(arEntry);
#endif
#endif

			//store app name
			lstData.setData(nIdx, "BEM_EXT_APP", "Outlook");
			message.Close();
		}
		else
			lstData.addRow();	// empty row, failed to read message data
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ReadEmailsFromEntryIDs - done"));
	return true;
}

bool MapiManager::SendAppointment(QString strBody, QString strSubject, QString strEmailFrom, QString strEmailTo, bool bSupressMsg)
{
	qDebug() << "MapiManager::SendAppointment";

	/*
	if (!LoadMapiDll()) return false;

	typedef bool (*tSendAppointment)(const char *, const char *, const char *, const char *);
	tSendAppointment SendAppointment = (tSendAppointment) g_MapiDLL.resolve("SendAppointment");
	if(!SendAppointment) 
	{
		if(!bSupressMsg)QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Error getting method from MAPI dll!"));
		return false;
	}

	bool bRes = SendAppointment(strEmailTo.toLatin1().constData(), "Appointment", "Hello", strBody.toLatin1().constData());
	if(!bRes)
	{
		if(!bSupressMsg)QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Error sending appointment!"));
		return false;
	}
	*/

	SMTPConnectionSettings conn;
	conn.m_strHost				= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_NAME).toString();
	conn.m_nPort				= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_PORT).toInt();
	conn.m_strUserName			= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USER_NAME).toString();
	conn.m_strUserEmail			= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USER_EMAIL).toString();
	conn.m_strAccountName		= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_ACC_NAME).toString();
	conn.m_strAccountPassword	= g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SMTP_ACC_PASS).toString();
	bool bEMAIL_SETTINGS_USE_AUTH = g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USE_AUTH).toBool();
	//#define EML_SECURITY_NONE	  1
	//#define EML_SECURITY_STARTTLS 2
	//#define EML_SECURITY_SSLTLS	  3
	int nSSLType = g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USE_SSL).toInt();
	if(nSSLType > 1){
		conn.m_bUseSSL = true;
		if(2 == nSSLType)
			conn.m_bUseSTLS = true;
	}

	if(conn.m_strHost.isEmpty() ||
	   conn.m_strUserEmail.isEmpty())
	{
		QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Please setup your SMTP settings!"));
		return false;
	}

	bool bOK = MailManager::SendAppointmentDirectly(strBody, strSubject, strEmailTo, conn, bEMAIL_SETTINGS_USE_AUTH);
	if(!bOK)
			QMessageBox::information(NULL, QObject::tr("Info"), QObject::tr("Error sending appointment!"));

	return bOK;
}

bool MapiManager::ReadAppointment(QByteArray &entryID, QString &strMessageClass, DbRecordSet &set)
{
	CMAPIEx* pMapi = MapiManager::InitGlobalMapi();
	if(!pMapi || !pMapi->OpenRootFolder())
		return false;

	SBinary entID;
	entID.lpb = (LPBYTE)entryID.constData();
	entID.cb  = entryID.size();

	CMAPIAppointment appointment;
	if(!appointment.Open(pMapi, entID))
		return false;

	QString strData;
	appointment.GetSubject(strData);
	set.setData(0, "BCEV_TITLE", strData);
	set.setData(0, "BCOL_SUBJECT", strData);

	appointment.GetLocation(strData);
	set.setData(0, "BCOL_LOCATION", strData);
	appointment.GetVCalendarUID(strData);
	set.setData(0, "BCIV_OUID", strData);
				
	appointment.GetStartDate(strData);
	QDateTime dateTime=QDateTime::fromString(strData,"MM/dd/yyyy hh:mm:ss AP");
	set.setData(0, "BCOL_FROM", dateTime);
	//qDebug()<<dateTime.toString("dd.MM.yyyy hh:mm:ss");
				
	appointment.GetEndDate(strData);
	dateTime=QDateTime::fromString(strData,"MM/dd/yyyy hh:mm:ss AP");
	//qDebug()<<dateTime.toString("dd.MM.yyyy hh:mm:ss");
	set.setData(0, "BCOL_TO", dateTime);

	return true;
}

//to be used within the main thread only
CMAPIEx* MapiManager::InitGlobalMapi()
{
	//make sure we are are using the shared MAPI object always from the same thread!!!!
	if(g_nMapiThreadID <= 0)
		g_nMapiThreadID = ThreadIdentificator::GetCurrentThreadID();
	else
		Q_ASSERT(ThreadIdentificator::GetCurrentThreadID() == g_nMapiThreadID);

	if(g_mapi_global_initialized)
		return &g_mapi;

	if(CMAPIEx::Init(false)){
		if(g_mapi.Login()){
			g_mapi.OpenMessageStore();
			g_mapi.OpenRootFolder();
			g_mapi_global_initialized = true;
		
		}
		else
			g_mapi.Logout();
	}

	if(g_mapi_global_initialized)
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION, 0,"Main Thread: logged into MAPI system!");
	else{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR, 0,"Main Thread: Error logging into MAPI system!");
		CMAPIEx::Term();
	}

	return (g_mapi_global_initialized) ? &g_mapi : NULL;
}

void MapiManager::UninitGlobalMapi()
{
	//make sure we are are using the shared MAPI object always from the same thread!!!!
	if(g_nMapiThreadID > 0)
		Q_ASSERT(ThreadIdentificator::GetCurrentThreadID() == g_nMapiThreadID);

	if(g_mapi_global_initialized){
		g_mapi.Logout();
		CMAPIEx::Term();
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Main Thread: logged off from MAPI system!");
	}
	else
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Main Thread: no need to log off from MAPI.");
	g_mapi_global_initialized = false;
}

#endif // _WIN32
#endif // WINCE
