#ifndef TWIXTELMANAGER_H
#define TWIXTELMANAGER_H

#include "common/common/dbrecordset.h"
#include <QtWidgets/QProgressDialog>

#ifdef _WIN32 
#if !defined WINCE

	#include "twixtel/twixtel.h"
	class TwixTelManager 
	{
	public:
		static bool TwixTelManager::ReadTwixTelContacts(DbRecordSet &lstTmpImportData, QString strDrive,QHash<QString,QString> lstFilterData);
	private:
		static void TwixTelManager::TwixTelFetchResults(DbRecordSet &lstTmpImportData, TwixTel &reader, int nRows, int &nResultListSize, QProgressDialog *progress=NULL);
		static bool TwixTelManager::TwixTelCalcNextSuffix(QString &strSuffix, bool bSkipDepthForCurCode=false);
	};

#else // WINCE

class TwixTelManager 
	{
	public:
		static bool TwixTelManager::ReadTwixTelContacts(DbRecordSet &lstTmpImportData, QString strDrive,QHash<QString,QString> lstFilterData){return false;}
	};

#endif // WINCE
#endif //#ifdef _WIN32 
#endif // TWIXTELMANAGER_H
