#ifndef PRINTMANAGER_H
#define PRINTMANAGER_H

#include <QPixmap>
#include <QtWidgets/QTextEdit>

#ifndef WINCE
	class PrintManager 
	{
	public:

		static bool PrintPixMap(const QPixmap *pixmap);
		static bool PrintText(const QTextEdit *pText);

	private:
		
	};
#else
	class PrintManager 
	{
	public:

		static bool PrintPixMap(const QPixmap *pixmap){return false;};
		static bool PrintText(const QTextEdit *pText){return false;};

	private:

	};
#endif // WINCE
#endif // PRINTMANAGER_H
