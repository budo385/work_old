#include "skypemanager.h"

#ifndef WINCE
#include "skype/skype.h"
#include "skype/skypeparser.h"


bool SkypeManager::IsSkypeInstalled()
{
	return SkypeLine::IsSkypeInstalled();
}
bool SkypeManager::SendCommand(const char *szParam, bool bCache)
{
	return SkypeLine::getInstance()->SendCommand(szParam,bCache);
}
bool SkypeManager::Call(QString strPhone)
{
	QString strCmd = "CALL " + strPhone;
	SkypeLine::m_pParser->m_strLastCallCommand=strCmd;
	return SendCommand(strCmd.toLatin1().constData());
}
void SkypeManager::ReleaseInstance()
{
	SkypeLine::releaseInstance();
}

void SkypeManager::SetListenIncomingCalls(bool bSet)
{
	SkypeLine::m_bListenIncomingCalls=bSet;

}
bool SkypeManager::GetListenIncomingCalls()
{
	return SkypeLine::m_bListenIncomingCalls;
}

bool SkypeManager::IsSkypeLogged()
{
	return SkypeLine::IsSkypeLogged();
}

void SkypeManager::EnsureInitialised()
{
	return SkypeLine::EnsureInitialised();
}

void SkypeManager::SetParser(void *pParser)
{
	if (pParser)
		SkypeLine::m_pParser=static_cast<SkypeParser*>(pParser);
	else
		SkypeLine::m_pParser=NULL;
}
#endif