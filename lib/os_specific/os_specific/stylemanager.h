#ifndef STYLESHEETMANAGER_H
#define STYLESHEETMANAGER_H


#include <QString>

/*!
	\class  StyleSheetManager
	\brief  Repository of all style sheets of all styles
	\ingroup GUICore


*/

class StyleManager 
{
public:

	enum MENU_STYLES
	{
		STYLE_DEEP_GREY,
		STYLE_SUNGLARE_BLUE,
	};

	enum STYLES
	{
		DEFAULTSTYLE=0,
		QWINDOWSSTYLE,
		QWINDOWSXPSTYLE,
		QCLEANLOOKSSTYLE,
		QPLASTIQUESTYLE,
		QMOTIFSTYLE,
		QCDESTYLE,
		NORWEGIANWOODSTYLE,
		ARTHURSTYLE,
		PAPERSTYLE,
		QWINDOWSVISTASTYLE,
		QMACSTYLE
	};

	static void GetMainMenuStyles(int nStyle,QString &strMenuLegend,QString &styleSectionHeader,QString &styleSectionButtonLvl0,QString &styleSectionButtonLvl1);
	static void DetermineDefaultStyle();
	static void SetStyle(int nStyle);
private:



};

#endif // STYLESHEETMANAGER_H
