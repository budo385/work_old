#ifndef SKYPEMANAGER_H
#define SKYPEMANAGER_H

#include <QString>

#ifndef WINCE
	class SkypeManager
	{
	public:
		static bool	IsSkypeInstalled();
		static bool	SendCommand(const char *szParam, bool bCache = true);
		static bool	Call(QString strPhone);
		static void ReleaseInstance();
		static void SetListenIncomingCalls(bool bSet=true);
		static bool GetListenIncomingCalls();
		static bool IsSkypeLogged();
		static void EnsureInitialised();
		static void SetParser(void *pParser);

	private:

	};

#else // WINCE
	class SkypeManager
	{
	public:
		static bool	IsSkypeInstalled(){return false;};
		static bool	SendCommand(const char *szParam, bool bCache = true){return false;};
		static bool	Call(QString strPhone){return false;};
		static void ReleaseInstance(){};
		static void SetListenIncomingCalls(bool bSet=true){};
		static bool GetListenIncomingCalls(){return false;};
		static bool IsSkypeLogged(){return false;};
		static void EnsureInitialised(){};
		static void SetParser(void *pParser){};

	private:

	};
#endif // WINCE
#endif // SKYPEMANAGER_H
