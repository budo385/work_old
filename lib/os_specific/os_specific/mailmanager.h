#ifndef MAILMANAGER_H
#define MAILMANAGER_H

#include "common/common/dbrecordset.h"
#include "trans/trans/smtpconnectionsettings.h"
typedef bool (*FN_FILTER_MATCH_ROW)(unsigned long nCallerObject,int nRow);
typedef bool (*FN_Call_CreateEmailFromTemplate)(QString bodyTemplate, QString &NewBody, int nContactID, int nProjectID);
#include <QtWidgets/QProgressDialog>

//#ifndef WINCE
#include "thunderbird/MBoxParser.h"

#ifndef WINCE
	#include "mailpp/src/SmtpSession.h"
	#include "mimepp/src/mimepp/mimepp.h"
#else
	#include "mimepp/wince/src/mimepp/mimepp.h"
#endif


class MailManager 
{
public:
	static bool SendMailDirectly(DbRecordSet &recResults, DbRecordSet &m_recMail,SMTPConnectionSettings connSettings,DbRecordSet &lstAttachments,DbRecordSet &recToRecordSet,DbRecordSet &recCCRecordSet,DbRecordSet &recBCCRecordSet,FN_Call_CreateEmailFromTemplate pfCallBack_CreateTemplate=NULL,QString strEMAIL_SETTINGS_SEND_COPY_CC="",QString strEMAIL_SETTINGS_SEND_COPY_BCC="", bool bEMAIL_SETTINGS_USE_AUTH=false, int nProjectID=-1,bool bSerialEmailMode=false);
	static bool InitSmtpSession(SmtpSession &session, const QString &strHost, int nPort, const QString &strAccName, const QString &strAccPass, bool bEMAIL_SETTINGS_USE_AUTH, bool bUseSSL, bool bUseSTLS);
	static bool SendAppointmentDirectly(QString strBody, QString strSubject, QString strToEmail, SMTPConnectionSettings connSettings, bool bEMAIL_SETTINGS_USE_AUTH=false);
	static bool SendMailDefaultClient(DbRecordSet &m_recMail,DbRecordSet &recToRecordSet,DbRecordSet &recCCRecordSet,DbRecordSet &recBCCRecordSet);
	static QString DecryptSMTPPassword(QString strTxt);
	static QString EncryptSMTPPassword(QString strTxt);
#ifndef WINCE
	static void GetThunderbirdMails(DbRecordSet &m_lstData,QList<QStringList> lstFoldersThunder,QSet<QString> &setEmails,bool bAreTemplates,FN_FILTER_MATCH_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject);
	static bool IsThunderbirdInstalled();
	static void GetThunderBirdEmailEntries(QString strMboxPath,DbRecordSet &lstDataEmails,QDateTime &dtLastEmail,QDateTime dtLastThunderbirdScan, int nStartOffset = 0, bool bOnlyOutgoingEmails = false);
	static void FillMailContents(QString strMboxPath, std::vector<MailInfo> &lstMsgs, int i);
#else
	static void GetThunderbirdMails(DbRecordSet &m_lstData,QList<QStringList> lstFoldersThunder,QSet<QString> &setEmails,bool bAreTemplates,FN_FILTER_MATCH_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject){};
	static bool IsThunderbirdInstalled(){return false;}
	static void GetThunderBirdEmailEntries(QString strMboxPath,DbRecordSet &lstDataEmails,QDateTime &dtLastEmail,QDateTime dtLastThunderbirdScan, int nStartOffset = 0, bool bOnlyOutgoingEmails = false){};
#endif
	static void GetMimeFromFile(QString strFile, QString &strType, QString &strSubtype);

private:
	static void AddEmail_Thunderbird(DbRecordSet &m_lstData,MailInfo &message, int &nCounter, QProgressDialog &progress, bool &bSkip, QSet<QString> &setEmails, bool bAreTemplates,FN_FILTER_MATCH_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject);
	static void ParseMail_Thunderbird(MailInfo &message, DbRecordSet &lstRowResult, QSet<QString> &setEmails, bool bAreTemplates=false);
	static void ProcessAddressList(mimepp::AddressList &list, DbRecordSet &lstRowResult, int nType, bool bAreTemplates, QSet<QString> &setTmp);
	static void SetMailToRecipients(DbRecordSet &recRecipients, QString &strRecipients);
#ifdef _WIN32
	static bool Thunderbird_GetMailAttBodyRecursive(mimepp::Entity &part, DbRecordSet &lstRowResult, DbRecordSet &lstAttachments);
#endif
};

#endif // MAILMANAGER_H
