#ifndef MAPIMANAGER_H
#define MAPIMANAGER_H

#define NOMINMAX
#include "mapi/mapiex.h"
#include <QtWidgets/QProgressDialog>
#include "common/common/dbrecordset.h"
typedef bool (*FN_ADD_NEW_ROW)(unsigned long nCallerObject,int nRow);

#ifdef _WIN32

#ifndef WINCE
	class CMAPIEx;
	class CMAPIAppointment;
	class CMAPIMessage;
	class CMAPIContact;
	 
	class MapiManager  
	{
	public:
		static bool LoadMapiDll();
		static bool UnIntiliazeMapiDll();
		static bool OpenMailInOutlook(QByteArray byteExternID, QString strExternApp="", bool bSupressMsg=false, bool bModal = false);
		static bool OpenMailInOutlookFromRecord(DbRecordSet &m_recMail,DbRecordSet &lstAttachments,DbRecordSet &recToRecordSet,DbRecordSet &recCCRecordSet,DbRecordSet &recBCCRecordSet,bool bBodyAsPlainText,bool bSentMode=true);
		static bool ImportContacts(DbRecordSet &lstTmpImportData, QList<QStringList> lstFolders, int &nContactsImported,QMap<QString, bool> mapSex,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject, CMAPIEx *pMapi);
		static bool GetOutlookFolders(int *nPIDs,char **szNames);
		static bool ImportEmails(DbRecordSet &lstTmpImportData, QList<QStringList> lstFolders, int &nEmailsImported,QSet<QString> &setEmails,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject, CMAPIEx *pMapi);
		static void GetEmailEntries(QList<QStringList> lstFolders,QList<QByteArray> &lstEntryIDs,QDateTime &dtLastEmail,QDateTime dtLastOutlookScan, CMAPIEx *pMapi);
		static bool GenerateEmailsFromDrop(DbRecordSet &lstTmpImportData,QList<QByteArray> lstEntryIDs,QSet<QString> &setEmails,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject, CMAPIEx *pMapi);
		static bool IsOutlookInstalled();
		static bool GetPersonalEmails(QStringList &lstPersEmails, CMAPIEx *pMapi);
		static void GetEntryIDsFromDrop(QByteArray &byteDrop, QList<QByteArray> &lstEntryIDs);
		static bool BuildImportListFromDrop(DbRecordSet &lstTmpImportData,QByteArray &byteDrop,QMap<QString, bool> mapSex,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject, CMAPIEx *pMapi);
		static bool ReadEmailsFromEntryIDs(QList<QByteArray> &lstEntryIDs, QSet<QString> &setEmails,DbRecordSet &lstData, bool bAreTemplates,QProgressDialog *progress, CMAPIEx *pMapi, bool bOnlyOutgoingEmails = false);
		
		static bool ListAppointments(DbRecordSet &lstTmpImportData, QList<QStringList> lstFolders, int &nEmailsImported,QSet<QString> &setEmails,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject, CMAPIEx *pMapi);
		static void AddAppointment(DbRecordSet &lstData, CMAPIAppointment &appointment, int &nCounter, QProgressDialog &progress, bool &bSkip, QSet<QString> &setEmails, bool bAreTemplates,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject);
		static bool SendAppointment(QString strBody, QString strSubject, QString strEmailFrom, QString strEmailTo, bool bSupressMsg=false);
		static bool ReadAppointment(QByteArray &entryID, QString &strMessageClass, DbRecordSet &set);
		
		//to be used within the main thread only
		static CMAPIEx* InitGlobalMapi();
		static void UninitGlobalMapi();

	private:
		static LPMAPIFOLDER OpenOutlookFolder(CMAPIEx &mapi, QStringList &lstFolderPath);
		static void AddEmail(DbRecordSet &lstData, CMAPIMessage &message, int &nCounter, QProgressDialog &progress, bool &bSkip, QSet<QString> &setEmails, bool bAreTemplates,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject);
		static bool AddContact(DbRecordSet &lstTmpImportData, CMAPIContact &contact,QMap<QString, bool> mapSex,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject);
		static QString GetEmailFromName(QString strName);
		static QString GetContactFromName(QString strName);
	};

#else // WINCE
	class MapiManager  
	{
	public:
		static bool LoadMapiDll(){return false;};
		static bool UnIntiliazeMapiDll(){return false;};
		static bool OpenMailInOutlook(QByteArray byteExternID, QString strExternApp="", bool bSupressMsg=false){return false;};
		static bool OpenMailInOutlookFromRecord(DbRecordSet &m_recMail,DbRecordSet &lstAttachments,DbRecordSet &recToRecordSet,DbRecordSet &recCCRecordSet,DbRecordSet &recBCCRecordSet,bool bBodyAsPlainText,bool bSentMode=true){return false;};
		static bool ImportContacts(DbRecordSet &lstTmpImportData, QList<QStringList> lstFolders, int &nContactsImported,QMap<QString, bool> mapSex,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject){return false;};
		static bool GetOutlookFolders(int *nPIDs,char **szNames){return false;};
		static bool ImportEmails(DbRecordSet &lstTmpImportData, QList<QStringList> lstFolders, int &nEmailsImported,QSet<QString> &setEmails,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject){return false;};
		static void GetEmailEntries(QList<QStringList> lstFolders,QList<QByteArray> &lstEntryIDs,QDateTime &dtLastEmail,QDateTime dtLastOutlookScan){};
		static bool GenerateEmailsFromDrop(DbRecordSet &lstTmpImportData,QList<QByteArray> lstEntryIDs,QSet<QString> &setEmails,FN_ADD_NEW_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject){return false;};
		static bool IsOutlookInstalled(){return false;};
		static bool GetPersonalEmails(QStringList &lstPersEmails){return false;};
		static void GetEntryIDsFromDrop(QByteArray &byteDrop, QList<QByteArray> &lstEntryIDs){return;};
		static bool BuildImportListFromDrop(DbRecordSet &lstTmpImportData,QByteArray &byteDrop,QMap<QString, bool> mapSex,FN_ADD_NEW_ROW pfCallBack_OnNewRowAdded, unsigned long nCallerObject, CMAPIEx *pMapi){return false;};
		static bool ReadEmailsFromEntryIDs(QList<QByteArray> &lstEntryIDs, QSet<QString> &setEmails, DbRecordSet &lstData, bool bAreTemplates,QProgressDialog *progress, bool bOnlyOutgoingEmails = false){return false;};


	private:
	};
#endif // WINCE

#endif //#ifdef _WIN32

#endif // MAPIMANAGER_H


