//=============================================================================
// LsubResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef LSUB_RESPONSE_H
#define LSUB_RESPONSE_H

class ListItem;


/// Class that represents an IMAP4 LSUB response

class MAILPP_API LsubResponse : public Response
{
public:

    /// Default constructor.
    LsubResponse();

    /// Copy constructor.
    LsubResponse(const LsubResponse& other);

    /// Destructor.
    virtual ~LsubResponse();

    /// Assignment operator.
    LsubResponse& operator = (const LsubResponse& other);

    /// Indicates if a \\Noinferiors flag is present in the response.
    bool Noinferiors() const;

    /// Indicates if a \\Noselect flag is present in the response.
    bool Noselect() const;

    /// Indicates if a \\Marked flag is present in the response.
    bool Marked() const;

    /// Indicates if an \\Unmarked flag is present in the response.
    bool Unmarked() const;

    /// Indicates if the specified flag is present in the response.
    bool Attribute(const char* flag) const;

    /// Gets the delimiter character for the naming hierarchy.
    char Delimiter() const;

    /// Gets the mailbox name.
    const char* MailboxName() const;

    /// Gets the number of attributes.
    unsigned NumAttributes() const;

    /// Gets the attribute at the specified position.
    const char* AttributeAt(unsigned index);

    /// Called by the parser to set the data for this object.
    void ImportData(ListItem* root);

private:

    char* mMailboxName;
    char mDelimiter;
    StringSequence mAttributes;
};

#endif
