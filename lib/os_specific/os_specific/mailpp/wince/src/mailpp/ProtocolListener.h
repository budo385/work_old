//=============================================================================
// ProtocolListener.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef PROTOCOL_LISTENER_H
#define PROTOCOL_LISTENER_H


/// Interface for processing the lines of a multiple line server response

/**
 * \class ProtocolListener mailpp.h mailpp/mailpp.h
 *
 * The <b>ProtocolListener</b> is a class that collaborates with a
 * <b>Pop3Client</b> or <b>NntpClient</b> class to process multiple line
 * responses from the server.  <b>ProtocolListener</b> is an abstract class
 * that defines an interface.  To process lines of a multiple line
 * response, you must create a subclass to implement the interface.
 */

class MAILPP_API ProtocolListener {
public:

    virtual ~ProtocolListener();

    /// Clears any stored lines
    /**
     * Clears any stored lines.  This member function is called at the
     * beginning of a new server response.
     */
    virtual void ClearLines() = 0;

    /// Passes a single line for processing
    /**
     * Passes a single line to the listener object for processing.  This
     * member function is called for each line of a multiple line server
     * response.
     *
     * The first line -- that is, the "status" line -- is not passed to the
     * listener object.  The final line, consisting of a single dot ("."),
     * also is not passed.
     *
     * If a line contains a dot (".") as the first character, that dot is
     * removed before it is passed to the listener for processing.  In
     * other words, any dot-stuffing is removed before the line is passed.
     * The end of line characters, CR LF, are not removed from the line
     * before it is passed.
     *
     * <p>
     * @param line the line as a NUL-terminated C string
     */
    virtual void LineReceived(const char* line) = 0;
};

#endif
