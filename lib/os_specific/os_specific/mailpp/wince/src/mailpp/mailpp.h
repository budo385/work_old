//=============================================================================
// mailpp.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef DW_MAILPP_H
#define DW_MAILPP_H

// If we are compiling on Windows, then define the MAILPP_API macro to
// correctly define the DLL export/import directive.  Otherwise, define
// MAILPP_API to a null value.

#if (defined(WIN32) || defined(_WIN32) || defined(_WIN32_WCE)) && ! defined(MAILPP_STATIC)
#  ifdef MAILPP_EXPORTS
#    define MAILPP_API __declspec(dllexport)
#  else
#    define MAILPP_API __declspec(dllimport)
#  endif
#else
#  define MAILPP_API
#endif

#include <stdlib.h>

// Compiler specific sections

// Microsoft Visual C++ on Microsoft Windows
#if defined(_MSC_VER) && defined(_WIN32) && ! defined(_WIN32_WCE)
#  if !defined(MAILPP_INT_TYPES_DEFINED)
#  define MAILPP_INT_TYPES_DEFINED
   typedef signed char int8_t;
   typedef unsigned char uint8_t;
   typedef short int16_t;
   typedef unsigned short uint16_t;
   typedef int int32_t;
   typedef unsigned int uint32_t;
#  endif // !defined(MAILPP_INT_TYPES_DEFINED)
#endif

// Microsoft Visual C++ on Microsoft Windows CE
#if defined(_MSC_VER) && defined(_WIN32) && defined(_WIN32_WCE)
#  if !defined(MAILPP_INT_TYPES_DEFINED)
#  define MAILPP_INT_TYPES_DEFINED
   typedef signed char int8_t;
   typedef unsigned char uint8_t;
   typedef short int16_t;
   typedef unsigned short uint16_t;
   typedef int int32_t;
   typedef unsigned int uint32_t;
#  endif // !defined(MAILPP_INT_TYPES_DEFINED)
#endif // Microsoft Visual C++ on Microsoft Windows CE

// GCC or Forte C++ on Sun Solaris
#if defined(__sun)
#  include <inttypes.h>  // for int32_t, etc
#endif // GCC or Forte C++ on Sun Solaris

// GCC on Linux
#if defined(__linux)
#  include <inttypes.h>  // for int32_t, etc
#endif // GCC on Linux

// DEC cxx on Tru64 Unix
#if defined(__DECCXX)
#  if !defined(MAILPP_INT_TYPES_DEFINED)
#  define MAILPP_INT_TYPES_DEFINED
   typedef signed char int8_t;
   typedef unsigned char uint8_t;
   typedef short int16_t;
   typedef unsigned short uint16_t;
   typedef int int32_t;
   typedef unsigned int uint32_t;
#  endif // !defined(MAILPP_INT_TYPES_DEFINED)
#endif // defined(__DECCXX)

namespace mailpp {

#include "Error.h"
#include "TraceOutput.h"
#if ! defined(_WIN32_WCE)
# include "StdTraceOutput.h"
#endif
#include "NameValue.h"
#include "NameValueSequence.h"
#include "StringSequence.h"
#include "Uint32Sequence.h"
#include "ListItem.h"
#include "MUTF7.h"
#include "UTF8.h"
#include "Imap4Date.h"
#include "AddressData.h"
#include "AddressDataSequence.h"
#include "BodySectionData.h"
#include "BodyStructureDataSequence.h"
#include "BodyStructureData.h"
#include "EnvelopeData.h"
#include "FlagsData.h"
#include "NamespaceAttributes.h"
#include "NamespaceAttributesSequence.h"
#include "Response.h"
#include "BadResponse.h"
#include "ByeResponse.h"
#include "CapabilityResponse.h"
#include "ContinuationResponse.h"
#include "ExistsResponse.h"
#include "ExpungeResponse.h"
#include "FetchResponse.h"
#include "FlagsResponse.h"
#include "ListResponse.h"
#include "LsubResponse.h"
#include "NamespaceResponse.h"
#include "NoResponse.h"
#include "OkResponse.h"
#include "PreauthResponse.h"
#include "QuotaResponse.h"
#include "QuotaRootResponse.h"
#include "RecentResponse.h"
#include "SearchResponse.h"
#include "StatusResponse.h"
#include "ProtocolListener.h"
#include "StreamBuffer.h"
#include "Imap4Client.h"
#include "SmtpClient.h"
#include "Pop3Client.h"
#include "NntpClient.h"
#include "TlsConfig.h"

MAILPP_API const char* VersionInfo();
MAILPP_API const char* BuildInfo();

} // namespace mailpp

#endif
