//=============================================================================
// FetchResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef FETCH_RESPONSE_H
#define FETCH_RESPONSE_H

class ListItem;
class FlagsData;
class EnvelopeData;
class BodyStructureData;
class BodySectionData;


/// Class that represents an IMAP4 FETCH response

class MAILPP_API FetchResponse : public Response
{
public:

    /// Default constructor
    FetchResponse();

    /// Constructor that takes a sequence number parameter
    FetchResponse(uint32_t sequenceNumber);

    /// Copy constructor
    FetchResponse(const FetchResponse& other);

    /// Destructor
    virtual ~FetchResponse();

    /// Assignment operator
    FetchResponse& operator = (const FetchResponse& other);

    /// Returns message sequence number
    uint32_t SequenceNumber() const;

    /// Returns INTERNALDATE data, if present
    const Imap4Date& InternalDate() const;

    /// Returns RFC822 data, if present
    const char* Rfc822() const;

    /// Returns RFC822.HEADER data, if present
    const char* Rfc822Header() const;

    /// Returns RFC822.TEXT data, if present
    const char* Rfc822Text() const;

    /// Returns RFC822.SIZE data, if present
    uint32_t Rfc822Size() const;

    /// Returns UID data, if present
    uint32_t Uid() const;

    /// Returns the FLAGS data, if present
    const FlagsData* Flags() const;

    /// Returns ENVELOPE data, if present
    const EnvelopeData* Envelope() const;

    /// Returns BODYSTRUCTURE or BODY data, if present
    const BodyStructureData* BodyStructure() const;

    /// Returns the body section data, if present
    const BodySectionData* BodySection() const;

    /// Called by the parser to set the data for this object
    void ImportData(ListItem* root);

private:

    uint32_t mSequenceNumber;

    Imap4Date mInternalDate;

    char* mRfc822;
    char* mRfc822Header;
    char* mRfc822Text;
    uint32_t mRfc822Size;

    FlagsData* mFlags;
    EnvelopeData* mEnvelope;
    BodyStructureData* mBodyStructureData;
    BodySectionData* mBodySectionData;

    uint32_t mUid;

    void Clear();
};

#endif
