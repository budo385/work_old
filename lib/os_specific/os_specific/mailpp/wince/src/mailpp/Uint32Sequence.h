//=============================================================================
// Uint32Sequence.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef UINT32_SEQUENCE_H
#define UINT32_SEQUENCE_H


class MAILPP_API Uint32Sequence
{
public:

    Uint32Sequence();
    Uint32Sequence(int initialCapacity);
    Uint32Sequence(const Uint32Sequence& other);
    ~Uint32Sequence();

    Uint32Sequence& operator = (const Uint32Sequence& other);

    unsigned NumElements() const;
    void InsertElement(unsigned pos, uint32_t element);
    void AppendElement(uint32_t element);
    uint32_t RemoveElement(unsigned index);
    uint32_t ElementAt(unsigned index) const;

    void DeleteAll();

private:

    unsigned mNumElements;
    unsigned mMaxElements;
    uint32_t* mElements;

    void Copy(const Uint32Sequence&);
};

#endif
