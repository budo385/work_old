//=============================================================================
// FlagsResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef FLAGS_RESPONSE_H
#define FLAGS_RESPONSE_H


/// Class that represents an IMAP4 FLAGS response

class MAILPP_API FlagsResponse : public Response
{
public:

    /// Default constructor
    FlagsResponse();

    /// Copy constructor
    FlagsResponse(const FlagsResponse& other);

    /// Destructor
    virtual ~FlagsResponse();

    /// Assignment operator
    FlagsResponse& operator = (const FlagsResponse& other);

    /// Indicates if \\Answered flag was present in response
    bool Answered() const;

    /// Indicates if \\Flagged flag was present in response
    bool Flagged() const;

    /// Indicates if \\Deleted flag was present in response
    bool Deleted() const;

    /// Indicates if \\Seen flag was present in response
    bool Seen() const;

    /// Indicates if \\Draft flag was present in response
    bool Draft() const;

    /// Indicates if specified flag was present in response
    bool Flag(const char* name) const;

    /// Gets the number of flags.
    unsigned NumFlags() const;

    /// Gets the flag at the specified position.
    const char* FlagAt(unsigned index);

    /// Sets parameters for the response
    void SetParams(const char* str);

private:

    StringSequence mFlags;

};

#endif
