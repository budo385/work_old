#include <mailpp/mailpp.h>
#include <string.h>
#include <iostream>
using namespace std;
using namespace mailpp;
#include "NntpListener.h"


NntpListener::NntpListener(ostream& out_)
  : out(out_)
{
}

NntpListener::~NntpListener()
{
}

void NntpListener::ClearLines()
{
}

void NntpListener::LineReceived(const char* line)
{
    // End of line characters are always CR LF.  This may be okay if you
    // want to write directly to a binary mode stream.  However, if you are
    // writing to a text stream, then you probably want to strip the CR and
    // conform to the standard C/C++ newline convention.
    int lineLen = (int) strlen(line) - 1;
    if (lineLen >= 0 && line[lineLen] == '\n') {
        --lineLen;
    }
    if (lineLen >= 0 && line[lineLen] == '\r') {
        --lineLen;
    }
    ++lineLen;
    out.write(line, lineLen);
    out << endl;
}
