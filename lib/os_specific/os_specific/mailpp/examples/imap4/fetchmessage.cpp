// fetchmessage.cpp
//
// This example program demonstrates how to retrieve an entire message
// from the INBOX folder.  It's useful to use from the command line to
// view a message when you are testing.
//
// Note: This program does not do the kind of error checking that would
// be required in a real application.
//

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mailpp/mailpp.h>
using namespace std;
using namespace mailpp;

#if defined(WIN32)
#  define strcasecmp(x,y) _stricmp(x,y)
#endif


void processFetchRfc822(const Imap4Client& client);


int main(int argc, char** argv)
{
    // Get commandline arguments

    if (argc < 5) {
        printf("usage:\n");
        printf("    %s host user password seq_number\n", argv[0]);
        exit(1);
    }
    const char* host = argv[1];
    const char* user = argv[2];
    const char* password = argv[3];
    int seqNumber = atoi(argv[4]);

    // Create an IMAP4 client

    Imap4Client client;

    // Set debugging on for now, so that we can see if there are any
    // problems with the connection, LOGIN, or SELECT.

    StdTraceOutput traceOut;
    client.SetTraceOutput(&traceOut, false);

    // Open connection to the server

    client.Connect(host);

    // Send LOGIN command

    client.Login(user, password);

    // Send SELECT to select the folder

    client.Select("INBOX");

    // Set debugging off before we send the FETCH command, since we would
    // see a lot of debugging output

    //client.SetTraceOutput(0, false);

    // Send FETCH (RFC822) to get the complete message

    client.Fetch(seqNumber, "(RFC822)");

    // Find the FETCH response, then print the message

    processFetchRfc822(client);

    // Send LOGOUT command to end this connection

    client.Logout();

    // Call Disconnect to close the socket

    client.Disconnect();

    return 0;
}


void processFetchRfc822(const Imap4Client& client)
{
    // Iterate over all the responses to find the FETCH response

    int numResponses = client.NumResponses();
    for (int i=0; i < numResponses; ++i) {
        const Response& r = client.ResponseAt(i);
        if (strcasecmp(r.Name(), "fetch") == 0) {

            // When we find the response, print the message

            const FetchResponse& response = (const FetchResponse&) r;
            cout << "---------------------------------------"
                "---------------------------------------" << endl;
            cout << response.Rfc822();
            cout << "---------------------------------------"
                "---------------------------------------" << endl;
        }
    }
}
