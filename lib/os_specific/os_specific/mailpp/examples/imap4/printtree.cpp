// printtree.cpp
//
// This example sends a FETCH command to get the body structure of a
// message in the INBOX folder, then prints the body parts in a tree (or
// outline) form.
//
// Note: This program does not do the kind of error checking that would
// be required in a real application.
//

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mailpp/mailpp.h>
using namespace std;
using namespace mailpp;

#if defined(WIN32)
#  define snprintf _snprintf
#  define strcasecmp _stricmp
#endif

using std::string;
using std::cout;
using std::endl;


void processFetch(const Imap4Client& client);
void processBodyStructure(const BodyStructureData& bodyStruct, int level,
    const string& partRef);


int main(int argc, char** argv)
{
    // Get commandline arguments

    if (argc < 5) {
        printf("usage:\n");
        printf("    %s host user password seq_number\n", argv[0]);
        exit(1);
    }
    const char* host = argv[1];
    const char* user = argv[2];
    const char* password = argv[3];
    int seqNumber = atoi(argv[4]);

    // Create an IMAP4 client

    Imap4Client client;

    // For this example, set debugging on, so that we can see the protocol
    // messages exchanged.

    StdTraceOutput traceOut;
    client.SetTraceOutput(&traceOut, false);

    // Open connection to the server

    client.Connect(host);

    // Send LOGIN command

    client.Login(user, password);

    // Send SELECT to select the folder

    client.Select("INBOX");

    // Send a FETCH command to get the body structure

    client.Fetch(seqNumber, "(BODYSTRUCTURE)");

    // Process all the responses to find the FETCH response and to extract
    // the information in it

    processFetch(client);

    // Send LOGOUT command to end this connection

    client.Logout();

    // Call Disconnect to close the socket

    client.Disconnect();

    return 0;
}


void processFetch(const Imap4Client& client)
{
    // Iterate over the responses to find the FETCH response

    int numResponses = client.NumResponses();
    for (int i=0; i < numResponses; ++i) {
        const Response& r = client.ResponseAt(i);
        if (strcasecmp(r.Name(), "fetch") == 0) {

            // Process this FETCH response

            const FetchResponse& response = (const FetchResponse&) r;

            // If there is a BODYSTRUCTURE structure, then process it to
            // print the outline

            const BodyStructureData* bodyStruct = response.BodyStructure();
            if (bodyStruct != 0) {
                processBodyStructure(*bodyStruct, 0, string());
            }
        }
    }
}


// Process a BODYSTRUCTURE structure to print the body structure as an
// outline.  This function is called recursively.  'level' indicates the
// recursion depth.

void processBodyStructure(const BodyStructureData& bodyStruct, int level,
    const string& partRef)
{
    int numBodyParts = bodyStruct.NumBodyParts();
    if (numBodyParts == 0) {
        for (int i=0; i < level; ++i) {
            cout << " ";
        }
        cout << partRef << " ";
        cout << bodyStruct.MediaType() << "/" << bodyStruct.MediaSubtype();
        cout << " " <<  bodyStruct.Size();
        cout << " (" << bodyStruct.ContentDescription() << ")" << endl;
    }
    else /* if (numBodyParts > 0) */ {
        int i;
        for (i=0; i < level; ++i) {
            cout << " ";
        }
        cout << partRef << " ";
        cout << "multipart/" << bodyStruct.MediaSubtype() << endl;
        for (i=0; i < numBodyParts; ++i) {
            const BodyStructureData* nestedBodyStruct =
                bodyStruct.BodyPart(i);
            char s[64];
            if (partRef.size() > 0) {
                snprintf(s, sizeof(s), "%s.%d", partRef.c_str(), i+1);
            }
            else {
                snprintf(s, sizeof(s), "%d", i+1);
            }
            string childPartRef = s;
            processBodyStructure(*nestedBodyStruct, level+1, childPartRef);
        }
    }
}
