// listfolders.cpp
//
// This example program demonstrates how to send the LIST command to list
// the folders available to the client.  (Note: IMAP4 uses the term
// "mailbox" rather than "folder".)  The LIST command is tricky, because
// mailbox naming is not very standard among IMAP4 server implementations.
// The program will allow you to experiment with the LIST command by
// providing different commandline arguments.
//
// Note: This program does not do the kind of error checking that would
// be required in a real application.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mailpp/mailpp.h>
using namespace mailpp;


int main(int argc, char** argv)
{
    // Get commandline arguments

    if (argc < 6) {
        printf("usage:\n");
        printf("    %s host user password reference pattern\n", argv[0]);
        exit(1);
    }
    const char* host = argv[1];
    const char* user = argv[2];
    const char* password = argv[3];
    const char* ref = argv[4];
    const char* pattern = argv[5];

    // Create an IMAP4 client

    Imap4Client client;

    // For this example, set debugging on, so that we can see the protocol
    // messages exchanged.

    StdTraceOutput traceOut;
    client.SetTraceOutput(&traceOut, false);

    // Open connection to the server

    client.Connect(host);

    // Send LOGIN command

    client.Login(user, password);

    // Send LIST command

    client.List(ref, pattern);

    // Send LOGOUT command to end this connection

    client.Logout();

    // Call Disconnect to close the socket

    client.Disconnect();

    return 0;
}
