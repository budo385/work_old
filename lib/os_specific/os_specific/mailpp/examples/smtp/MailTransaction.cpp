// MailTransaction.cpp

// To do:
// * Provide an indication of whether an error was a communication error
//   or a protocol error.
// * Maybe close the connection on any comm error.  It might be a good idea
//   to have an SmptClient::IsConnected() member function.
// * Set the timeout values according to RFC 2821

#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <mailpp/mailpp.h>
#include "MailTransaction.h"
using namespace std;
using namespace mailpp;

#if defined(WIN32) || defined(_WIN32)
#  define strdup   _strdup
#  define snprintf _snprintf
#endif


MailTransaction::MailTransaction()
{
    mFrom = 0;
    mMaxTo = 0;
    mNumTo = 0;
    mTo = 0;
    mFilename = 0;
}

MailTransaction::MailTransaction(const char* from, const char* to,
    const char* filename)
{
    mFrom = strdup(from);
    mMaxTo = 0;
    mNumTo = 0;
    mTo = 0;
    mFilename = strdup(filename);

    int maxTo = 10;
    char** to_ = (char**) malloc(maxTo*sizeof(char*));
    if (to_ != 0) {
        mMaxTo = maxTo;
        mTo = to_;
        mTo[mNumTo++] = strdup(to);
    }
}

MailTransaction::~MailTransaction()
{
    if (mFrom) free(mFrom);
    mFrom = 0;
    while (mNumTo > 0) {
        --mNumTo;
        free(mTo[mNumTo]);
    }
    if (mTo) free(mTo);
    mTo = 0;
    mMaxTo = 0;
    if (mFilename) free(mFilename);
    mFilename = 0;
}

void MailTransaction::SetFrom(const char* from)
{
    if (mFrom) free(mFrom);
    mFrom = strdup(from);
}

void MailTransaction::AddTo(const char* to)
{
    if (mNumTo == mMaxTo) {
        int maxTo = mMaxTo + 10;
        char** to_ = (char**) realloc(mTo, maxTo*sizeof(char*));
        if (to_) {
            mMaxTo = maxTo;
            mTo = to_;
        }
    }
    if (mNumTo < mMaxTo) {
        mTo[mNumTo++] = strdup(to);
    }
}

void MailTransaction::SetFilename(const char* filename)
{
    if (mFilename) free(mFilename);
    mFilename = strdup(filename);
}

int MailTransaction::Send(SmtpClient& client)
{
    if (mFrom && mNumTo > 0 && mFilename) {
        ifstream in(mFilename);
        if (in) {
            return Send_MAIL(client, in);
        }
        else /* if (!in) */ {
            cout << "Can't open file " << mFilename << endl;
        }
    }
    else {
        cout << "Can't process incomplete mail transaction" << endl;
    }
    return -1;
}

int MailTransaction::Send_MAIL(SmtpClient& client, ifstream& in)
{
    //
    // If the server supports it, send the MAIL command with the SIZE
    // parameter.
    //
    // This optional SMTP capability allows a server to reject a message
    // before it is sent if the server considers the message too big.  The
    // alternative is for the client to send the complete message, which is
    // then rejected by the server after it is sent.  Obviously, the SIZE
    // extension is a very useful extension.  Note that on Unix systems,
    // the data sent is actually larger than the file size because of the
    // LF -> CR LF conversion.  In most cases, a size estimate, and not the
    // exact size, is sufficient.
    //
	int commErr = 0;
    if (client.HasCapability("SIZE")) {
        in.seekg(0, ios::end);
        int fileSize = (int) in.tellg();
        in.seekg(0, ios::beg);
        char params[40];
        snprintf(params, sizeof(params), "SIZE=%d", fileSize);
        commErr = client.MailFrom(mFrom, params);
    }
    else {
        commErr = client.MailFrom(mFrom);
    }
    if (! commErr) {
        int replyCode = client.ReplyCode();
        if (replyCode/100%10 == 2) {
            return Send_RCPT(client, in);
        }
        else /* if (replyCode/100%10 != 2) */ {
            ReportProtocolError("SMTP MAIL command failed", client);
        }
    }
    else /* if (commErr) */ {
        ReportNetworkError("Network error", client);
    }
    if (! commErr) {
        client.Rset();
    }
    return -1;
}

int MailTransaction::Send_RCPT(SmtpClient& client, ifstream& in)
{
    int commErr = false;
    int okTo = 0;
    for (int i=0; i < mNumTo; ++i) {
        commErr = client.RcptTo(mTo[i]);
        if (! commErr) {
            int replyCode = client.ReplyCode();
            if (replyCode/100%10 == 2) {
                ++okTo;
            }
            else /* if (replyCode/100%10 != 2) */ {
                char buf[200];
                snprintf(buf, sizeof(buf), "SMTP RCPT command failed "
                    "for recipient %s", mTo[i]);
                ReportProtocolError(buf, client);
            }
        }
        else /* if (commErr) */ {
            ReportNetworkError("Network error", client);
            break;
        }
    }
    if (! commErr) {
        if (okTo > 0) {
            return Send_DATA(client, in);
        }
        else {
            client.Rset();
        }
    }
    return -1;
}

int MailTransaction::Send_DATA(SmtpClient& client, ifstream& in)
{
    int commErr = 0;
    commErr = client.Data();
    if (! commErr) {
        int replyCode = client.ReplyCode();
        if (replyCode/100%10 == 3) {
            const int bufSize = 1024;
            char buf[bufSize];
            int bufPos = 0;
            int flags = SmtpClient::FIRST;
            bool isBufSent = false;
            char c;
            while (in.get(c)) {
                // If the buffer is full, or nearly so, then sent it
                if (bufPos + 2 > bufSize) {
                    commErr = client.SendLines(buf, bufPos, flags);
                    isBufSent = true;
                    bufPos = 0;
                    flags = 0;
                    if (commErr) {
                        ReportNetworkError("Network error", client);
                        break;
                    }
                }
                // Convert line ending to CR LF unconditionally
                if (c == '\n') {
                    buf[bufPos++] = '\r';
                    buf[bufPos++] = '\n';
                }
                else if (c == '\r') {
                }
                else {
                    buf[bufPos++] = c;
                }
            }
            if (! commErr) {
                // Send the final partial buffer
                flags = SmtpClient::LAST;
                if (! isBufSent) {
                    flags |= SmtpClient::FIRST;
                }
                commErr = client.SendLines(buf, bufPos, flags);
                if (! commErr) {
                    replyCode = client.ReplyCode();
                    if (replyCode/100%10 == 2) {
                        //
                        // Return here on success
                        //
                        return 0;
                    }
                    else /* if (replyCode/100%10 != 2) */ {
                        ReportProtocolError("Sending mail data failed",client);
                    }
                }
                else /* if (commErr) */ {
                    ReportNetworkError("Network error", client);
                }
            }
        }
        else /* if (replyCode/100%10 != 3) */ {
            ReportProtocolError("SMTP DATA command failed", client);
        }
    }
    else /* if (commErr) */ {
        ReportNetworkError("Network error", client);
    }
    if (! commErr) {
        client.Rset();
    }
    //
    // Return here on failure
    //
    return -1;
}

void MailTransaction::ReportNetworkError(const char* message,
    SmtpClient& client)
{
    cout << message << endl;
    cout << "Error code: " << client.ErrorCode() << endl;
    cout << "Error message: " << client.ErrorMessage() << endl;
    cout << "OS error code: " << client.OsErrorCode() << endl;
}

void MailTransaction::ReportProtocolError(const char* message,
    SmtpClient& client)
{
    cout << message << endl;
    cout << "Server's reply was: "
         << client.ReplyCode() << " "
         << client.ReplyText() << endl;
}
