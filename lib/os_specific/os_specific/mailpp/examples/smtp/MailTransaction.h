// MailTransaction.h

#ifndef MAIL_TRANSACTION_H
#define MAIL_TRANSACTION_H

#include <fstream>
#include <mailpp/mailpp.h>

using mailpp::SmtpClient;
using std::ifstream;


class MailTransaction
{
public:

    MailTransaction();
    MailTransaction(const char* from, const char* to, const char* filename);
    virtual ~MailTransaction();

    void SetFrom(const char* from);
    void AddTo(const char* to);
    void SetFilename(const char* filename);

    virtual int Send(SmtpClient& client);

protected:

    void ReportNetworkError(const char* message, SmtpClient& client);
    void ReportProtocolError(const char* message, SmtpClient& client);

private:

    char* mFrom;
    int mMaxTo;
    int mNumTo;
    char** mTo;
    char* mFilename;

    int Send_MAIL(SmtpClient& client, ifstream& in);
    int Send_RCPT(SmtpClient& client, ifstream& in);
    int Send_DATA(SmtpClient& client, ifstream& in);
};

#endif
