//=============================================================================
// Imap4Date.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef MAILPP_IMAP4_DATE_H
#define MAILPP_IMAP4_DATE_H

/// Class that represents an IMAP4 date-time string.

class MAILPP_API Imap4Date
{
public:

    /// Default constructor.
    Imap4Date();

    /// Copy constructor.
    Imap4Date(const Imap4Date& other);

    /// Constructor that sets the value to null.
    Imap4Date(int);

    /// Constructor that takes an initial string value.
    Imap4Date(const char* str);

    /// Constructor that takes an initial date value.
    Imap4Date(int year, int month, int day, int hour, int minute,
        int second, int zone);

    /// Assignment operator.
    Imap4Date& operator=(const Imap4Date& other);

    /// Returns true if the last parse succeeded.
    bool IsValid() const { return mIsValid; }
    /// Returns true if the value is null.
    bool IsNull() const { return mIsNull; }
    /// Gets the IMAP4 date as a string.
    const char* String()const { return mString; }
    /// Gets the year.
    int Year() const { return mYear; }
    /// Gets the month.
    int Month() const { return mMonth; }
    /// Gets the day.
    int Day() const { return mDay; }
    /// Gets the hour.
    int Hour() const { return mHour; }
    /// Gets the minute.
    int Minute() const { return mMinute; }
    /// Gets the second.
    int Second() const { return mSecond; }
    /// Gets the zone.
    int Zone() const { return mZone; }
    /// Returns a scalar value.
    unsigned AsScalar() const;

private:
    bool mIsNull;
    bool mIsValid;
    char mString[28];
    int mYear;
    int mMonth;
    int mDay;
    int mHour;
    int mMinute;
    int mSecond;
    int mZone;
};

#endif
