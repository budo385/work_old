//=============================================================================
// SearchResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef SEARCH_RESPONSE_H
#define SEARCH_RESPONSE_H


/// Class that represents an IMAP4 SEARCH response

class MAILPP_API SearchResponse : public Response
{
public:

    /// Default constructor
    SearchResponse();

    /// Copy constructor
    SearchResponse(const SearchResponse& other);

    /// Destructor
    virtual ~SearchResponse();

    /// Assignment operator
    SearchResponse& operator = (const SearchResponse& other);

    /// Returns number of matches
    unsigned NumMatches() const;

    /// Returns match at the specified position
    uint32_t Match(unsigned index) const;

    /// Sets parameters for the response
    void SetParams(const char* s);

private:

    Uint32Sequence mMatches;
};

#endif
