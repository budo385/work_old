//=============================================================================
// StdTraceOutput.h
//
// Copyright (c) 2000-2005 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef STD_TRACE_OUTPUT_H
#define STD_TRACE_OUTPUT_H


/// Class that writes trace output to the standard output

class MAILPP_API StdTraceOutput : public TraceOutput
{
public:

    /// Default constructor
    StdTraceOutput();

    /// Destructor
    virtual ~StdTraceOutput();

    /// Logs the client-to-server stream
    virtual void Send(const char* buffer, unsigned length);

    /// Logs the server-to-client stream
    virtual void Receive(const char* buffer, unsigned length);

private:
    enum {
        INITIAL,
        SEND,
        RECEIVE
    } mState;
};

#endif
