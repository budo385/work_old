//=============================================================================
// Imap4Client.h
//
// Copyright (c) 2000-2005 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef IMAP4_CLIENT_H
#define IMAP4_CLIENT_H


/// Class that represents an IMAP4 client connection

class MAILPP_API Imap4Client
{
    friend class Imap4ClientErrorHandler;
    friend class Imap4Parser;

public:

    enum CommandStatusCode {
        ERR,
        OK,
        NO,
        BAD
    };

    /// Default constructor
    Imap4Client();

    /// Destructor
    virtual ~Imap4Client();

    /// Sets a Trace Output instance
    void SetTraceOutput(TraceOutput* out, bool takeOwnership);

    /// Sets the timeout value for network send and receive operations
    void SetTimeout(int secs);

    /// Sets the timeout value for network receive operations
    void SetReceiveTimeout(int secs);

    /// Sets the timeout value for network send operations
    void SetSendTimeout(int secs);

    /// Cancels a currently executing command
    void Cancel();

    /// Opens a connection to an IMAP4 server
    int Connect(const char* address, int port=143, bool wait=true);

    /// Establishes a TLS connection over an existing TCP connection
    int ConnectTls(const char* serverHostName, bool wait=false);

    /// Establishes a TLS connection over an existing TCP connection
    int ConnectTlsOpenSSL(const char* serverHostName, void* ssl,
        bool wait=false);

    /// Closes the connection to the IMAP4 server
    void Disconnect();

    /// Indicates if the last command succeeded.
    CommandStatusCode CommandStatus() const;

    /// Sends the CAPABILITY command
    int Capability();

    /// Sends the NOOP command
    int Noop();

    /// Sends the LOGOUT command
    int Logout();

    /// Sends the STARTTLS command
    int Starttls();

    /// Sends the AUTHENTICATE PLAIN command
    int AuthenticatePlain(const char* user, const char* password,
        const char* authorizeUser=0);

    /// Sends the AUTHENTICATE CRAM-MD5 command
    int AuthenticateCramMd5(const char* user, const char* password);

    /// Sends the AUTHENTICATE NTLM command
    int AuthenticateNtlm(const char* user, const char* password);

    /// Sends the LOGIN command
    int Login(const char* name, const char* password);

    /// Sends the SELECT command
    int Select(const char* mailboxName);

    /// Sends the EXAMINE command
    int Examine(const char* mailboxName);

    /// Sends the CREATE command
    int Create(const char* mailboxName);

    /// Sends the DELETE command
    int Delete(const char* mailboxName);

    /// Sends the RENAME command
    int Rename(const char* oldName, const char* newName);

    /// Sends the SUBSCRIBE command
    int Subscribe(const char* mailboxName);

    /// Sends the UNSUBSCRIBE command
    int Unsubscribe(const char* mailboxName);

    /// Sends the LIST command
    int List(const char* reference, const char* mailboxName);

    /// Sends the LSUB command
    int Lsub(const char* reference, const char* mailboxName);

    /// Sends the STATUS command
    int Status(const char* mailboxName, const char* items);

    /// Sends the APPEND command
    int Append(const char* mailboxName, const char* flags, const char* date,
        const char* message);

    /// Sends the APPEND command
    int Append(const char* mailboxName, const char* flags, const char* date,
        StreamBuffer& sbuf);

    /// Sends the CHECK command
    int Check();

    /// Sends the CLOSE command
    int Close();

    /// Sends the EXPUNGE command
    int Expunge();

    /// Sends the SEARCH command
    int Search(const char* charset, const char* criteria);

    /// Sends the FETCH command with a single message sequence number
    int Fetch(int msgNum, const char* items);

    /// Sends the FETCH command with a range of message sequence numbers
    int Fetch(int firstMsgNum, int lastMsgNum, const char* items);

    /// Sends the FETCH command with a message sequence number set
    int Fetch(const char* set, const char* items);

    /// Sends the STORE command with a single message sequence number
    int Store(int msgNum, const char* item, const char* value);

    /// Sends the STORE command with a range of message sequence numbers
    int Store(int firstMsgNum, int lastMsgNum, const char* item,
        const char* value);

    /// Sends the STORE command with a message sequence number set
    int Store(const char* set, const char* item, const char* value);

    /// Sends the COPY command with a single message sequence number
    int Copy(int msgNum, const char* mailboxName);

    /// Sends the COPY command with a range of message sequence numbers
    int Copy(int firstMsgNum, int lastMsgNum, const char* mailboxName);

    /// Sends the COPY command with a message sequence number set
    int Copy(const char* set, const char* mailboxName);

    /// Sends the UID FETCH command with a single message UID
    int UidFetch(int uid, const char* items);

    /// Sends the UID FETCH command with a range of message UIDs
    int UidFetch(int firstUid, int lastUid, const char* items);

    /// Sends the UID FETCH command with a message UID set
    int UidFetch(const char* set, const char* items);

    /// Sends the UID STORE command with a single message UID
    int UidStore(int uid, const char* item, const char* value);

    /// Sends the UID STORE command with a range of message UIDs
    int UidStore(int firstUid, int lastUid, const char* item,
        const char* value);

    /// Sends the UID STORE command with a message UID set
    int UidStore(const char* set, const char* item, const char* value);

    /// Sends the UID COPY command with a single message UID
    int UidCopy(int uid, const char* mailboxName);

    /// Sends the UID COPY command with a range of message UIDs
    int UidCopy(int firstUid, int lastUid, const char* mailboxName);

    /// Sends the UID COPY command with a message UID set
    int UidCopy(const char* set, const char* mailboxName);

    /// Sends the UID SEARCH command
    int UidSearch(const char* charset, const char* criteria);

    /// Sends the SETQUOTA command
    int SetQuota(const char* quotaRoot, const char* resourceList);

    /// Sends the GETQUOTA command
    int GetQuota(const char* quotaRoot);

    /// Sends the GETQUOTAROOT command
    int GetQuotaRoot(const char* mailboxName);

    /// Sends the NAMESPACE command.
    int Namespace();

    /// Sends the PROXYAUTH command
    int ProxyAuth(const char* userName);

    /// Gets the number of responses received from the server
    int NumResponses() const;

    /// Gets the response at the specified position
    const Response& ResponseAt(int index) const;

    /// Gets the platform-independent error code of last error
    Error ErrorCode() const;

    /// Gets the platform-specific error code of last error
    int OsErrorCode() const;

    /// Gets the error message associated with last error
    const char* ErrorMessage() const;

private:

    class ErrorHandler* mErrorHandler;
    class CriticalSectionImpl* mCriticalSectionImpl;
    class Socket* mSocket;

    bool mDeleteTraceOut;
    TraceOutput* mTraceOut;
    bool mIsConnected;
    int mReceiveTimeout;
    int mSendTimeout;
    int mBufferSize;
    char* mBuffer;
    Error mErrorCode;
    int mOsErrorCode;
    char* mErrorMessage;

    class Imap4Parser* mParser;

    int mSerialNum;

    int mNumResponses;
    int mMaxResponses;
    Response** mResponses;

    int Authenticate(const char* mechanism, const char* params = 0);
    int AuthenticateNtlm_2(const char* user, const char* password);
    int Append_2(const char* mailboxName, const char* flags, const char* date,
        StreamBuffer& sbuf);
    int PartialCommand(const char* command);
    int SendCommand(const char* command, const char* param1 = 0,
        const char* param2 = 0);
    void AppendResponse(Response*);
    void DeleteResponse(int index);
    void ClearResponses();
    void pSetError(Error errorCode, int osErrorCode, const char* errorMessage);
    int pSendStreamBuffer(StreamBuffer& sbuf);
    void pPrintCommand(const char* str);
};

#endif
