//=============================================================================
// UTF8.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef UTF8_H
#define UTF8_H

/// Class for converting to or from UTF-8 encoding

class MAILPP_API UTF8 {
public:
    /// Gets the length of the UTF-8 string that would result from
    /// encoding the UTF-16 string
    static size_t EncodeLen(const uint16_t* utf16, size_t utf16Len);
    /// Converts a UTF-16 string to UTF-8
    static size_t Encode(const uint16_t* utf16, size_t utf16Len,
        char* utf8, size_t utf8MaxLen);
    /// Gets the length of the UTF-16 string that would result from
    /// decoding the UTF-8 string
    static size_t DecodeLen(const char* utf8, size_t utf8Len);
    /// Converts a UTF-8 string to UTF-16
    static size_t Decode(const char* utf8, size_t utf8Len,
        uint16_t* utf16, size_t utf16MaxLen);
};

#endif
