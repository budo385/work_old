//=============================================================================
// StatusResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef STATUS_RESPONSE_H
#define STATUS_RESPONSE_H


/// Class that represents an IMAP4 STATUS response

class MAILPP_API StatusResponse : public Response
{
public:

    /// Default constructor
    StatusResponse();

    /// Copy constructor
    StatusResponse(const StatusResponse& other);

    /// Destructor
    virtual ~StatusResponse();

    /// Assignment operator
    StatusResponse& operator = (const StatusResponse& other);

    /// Returns name of the mailbox
    const char* MailboxName() const;

    /// Returns number of messages in the mailbox
    uint32_t Messages() const;

    /// Returns number of recent messages
    uint32_t Recent() const;

    /// Returns next UID value for the mailbox
    uint32_t UidNext() const;

    /// Returns UID validity value for the mailbox
    uint32_t UidValidity() const;

    /// Returns number of "unseen" messages
    uint32_t Unseen() const;

    /// Sets parameters for the response
    void SetParams(const char* s);

private:

    char* mMailboxName;

    uint32_t mMessages;
    uint32_t mRecent;
    uint32_t mUidNext;
    uint32_t mUidValidity;
    uint32_t mUnseen;
};

#endif
