//=============================================================================
// NamespaceAttributesSequence.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef NAMESPACE_ATTRIBUTES_SEQUENCE_H
#define NAMESPACE_ATTRIBUTES_SEQUENCE_H


// Container class for %NamespaceAttributes objects

class MAILPP_API NamespaceAttributesSequence
{
public:

    // Default constructor.
    NamespaceAttributesSequence();

    // Constructor that takes an initial capacity.
    NamespaceAttributesSequence(int initialCapacity);

    // Copy constructor.
    NamespaceAttributesSequence(const NamespaceAttributesSequence& other);

    // Destructor.
    ~NamespaceAttributesSequence();

    // Assignment operator.
    NamespaceAttributesSequence& operator=(
        const NamespaceAttributesSequence& other);

    // Gets the number of %NamespaceAttributes objects in the container.
    unsigned NumElements() const;

    // Inserts an %NamespaceAttributes object at the specified position.
    void InsertElement(unsigned pos, NamespaceAttributes* element);

    // Appends an %NamespaceAttributes object.
    void AppendElement(NamespaceAttributes* element);

    // Removes an %NamespaceAttributes object from the collection.
    NamespaceAttributes* RemoveElement(unsigned index);

    // Gets the %NamespaceAttributes object at the specified position.
    NamespaceAttributes* ElementAt(unsigned index) const;

    // Removes and deletes all objects from the collection.
    void DeleteAll();

    // Called by the parser to set the data for this object.
    void ImportData(ListItem* root);

private:

    unsigned mNumElements;
    unsigned mMaxElements;
    NamespaceAttributes** mElements;

    void Copy(const NamespaceAttributesSequence&);
};

#endif
