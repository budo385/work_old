//=============================================================================
// BodySectionData.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef BODY_SECTION_DATA_H
#define BODY_SECTION_DATA_H

class ListItem;


/// Class that contains the characters of a body section

class MAILPP_API BodySectionData
{
public:

    /// Constructor.
    BodySectionData(const char* section, int offset);

    /// Copy constructor.
    BodySectionData(const BodySectionData& other);

    /// Destructor.
    ~BodySectionData();

    /// Assignment operator.
    BodySectionData& operator = (const BodySectionData& other);

    /// Gets the identifier for the section that the content is from.
    const char* Section() const;

    /// Gets the offset of the content within the section.
    uint32_t Offset() const;

    /// Gets the length of the content.
    uint32_t Length() const;

    /// Gets the characters of the content.
    const char* Content() const;

    /// Called by the parser to set the data for this object.
    void ImportData(ListItem* root);

private:

    char* mContent;
    char* mSection;
    uint32_t mOffset;
    uint32_t mLength;

    BodySectionData();
    void SetSection(const char* section);
    void SetOffset(int offset);

};

#endif
