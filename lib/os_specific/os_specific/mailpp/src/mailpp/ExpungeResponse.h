//=============================================================================
// ExpungeResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef EXPUNGE_RESPONSE_H
#define EXPUNGE_RESPONSE_H


/// Class that represents an IMAP4 EXPUNGE response

class MAILPP_API ExpungeResponse : public Response
{
public:

    /// Default constructor
    ExpungeResponse();

    /// Copy constructor
    ExpungeResponse(const ExpungeResponse& other);

    /// Destructor
    virtual ~ExpungeResponse();

    /// Assignment operator
    ExpungeResponse& operator = (const ExpungeResponse& other);

    /// Returns the sequence number of an expunged message
    uint32_t Number() const;

    /// Sets the sequence number
    void Number(uint32_t n);

private:

    int mNumber;
};

#endif
