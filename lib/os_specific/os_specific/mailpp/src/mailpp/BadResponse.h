//=============================================================================
// BadResponse.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef BAD_RESPONSE_H
#define BAD_RESPONSE_H


/// Class that represents an IMAP4 BAD response

class MAILPP_API BadResponse : public Response
{
public:

    /// Default constructor
    BadResponse();

    /// Constructor that takes the tag as a parameter
    BadResponse(const char* tag);

    /// Copy constructor
    BadResponse(const BadResponse& other);

    /// Destructor
    virtual ~BadResponse();

    /// Assignment operator
    BadResponse& operator = (const BadResponse& other);

    /// Returns the tag of a tagged response
    const char* Tag() const;

    /// Returns the response code
    const char* ResponseCode() const;

    /// Returns the response text
    const char* ResponseText() const;

    /// Sets parameters for the response
    void SetParams(const char* s);

private:

    char* mTag;
    char* mResponseCode;
    char* mResponseText;

    void Clear();
};

#endif
