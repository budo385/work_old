//=============================================================================
// AddressData.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef ADDRESS_DATA_H
#define ADDRESS_DATA_H

class ListItem;


/// Class that contains the data of an IMAP4 ADDRESS structure

class MAILPP_API AddressData
{
public:

    /// Default constructor
    AddressData();

    /// Copy constructor
    AddressData(const AddressData& other);

    /// Destructor
    virtual ~AddressData();

    /// Assignment operator
    AddressData& operator = (const AddressData&);

    /// Returns the personal name
    const char* PersonalName() const;

    /// Returns the source route
    const char* SourceRoute() const;

    /// Returns the mailbox name
    const char* MailboxName() const;

    /// Returns the host name
    const char* HostName() const;

    /// Called by the parser to set the data for this object
    void ImportData(ListItem* root);

private:

    char* mPersonalName;
    char* mSourceRoute;
    char* mMailboxName;
    char* mHostName;

    void Clear();
};

#endif
