//=============================================================================
// FlagsData.h
//
// Copyright (c) 2000-2004 Hunny Software, Inc
// All rights reserved.
//
// IN NO EVENT SHALL HUNNY SOFTWARE, INC BE LIABLE TO ANY PARTY FOR DIRECT,
// INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF HUNNY SOFTWARE,
// INC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// HUNNY SOFTWARE, INC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
// BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON
// AN "AS IS" BASIS, AND HUNNY SOFTWARE, INC HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
//
//=============================================================================

#ifndef FLAGS_DATA_H
#define FLAGS_DATA_H

class ListItem;


/// Class that contains the data of an IMAP4 FLAGS list

class MAILPP_API FlagsData
{

public:

    /// Default constructor.
    FlagsData();

    /// Copy constructor.
    FlagsData(const FlagsData& other);

    /// Destructor.
    virtual ~FlagsData();

    /// Assignment operator.
    FlagsData& operator = (const FlagsData& other);

    /// Gets the number of flags.
    unsigned NumFlags() const;

    /// Gets the flag at the specified position.
    const char* Flag(unsigned index) const;

    /// Returns true if the specified flag is present.
    bool Flag(const char* which) const;

    /// Called by the parser to set the data for this object.
    void ImportData(const ListItem* root);

private:

    StringSequence mFlags;
};

#endif
