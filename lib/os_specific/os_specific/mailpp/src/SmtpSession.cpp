#include "SmtpSession.h"
#include "MailTransaction.h"
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

using namespace std;
using namespace mailpp;


#if defined(WIN32) || defined(_WIN32)
#  define strdup   _strdup
#  define snprintf _snprintf
#endif

static int verify_callback2(X509_STORE_CTX *,void *)
{
	return 1;	//do not terminate
}

SmtpSession::SmtpSession(const char* hostname)
{
    mHostname = strdup(hostname);
	//BT: does not work on WINCE...
    //mTraceOutput = new StdTraceOutput; 
	mTraceOutput = 0;
}

void SmtpSession::SetTracer(mailpp::TraceOutput* pTracer)
{
	//delete mTraceOutput;
    mTraceOutput = 0;
    mTraceOutput = pTracer;
}

SmtpSession::~SmtpSession()
{
    if (mHostname) free(mHostname);
    mHostname = 0;
    delete mTraceOutput;
    mTraceOutput = 0;
}

int SmtpSession::Open(const char* address, int port, bool bUseSSL, bool bUseSTLS)
{
    mClient.SetTraceOutput(mTraceOutput, false);

    int connectErr = mClient.Connect(address, port);
    if (! connectErr) {
        int replyCode = mClient.ReplyCode();
        if (replyCode/100%10 == 2) {
            int err = SayHello();
            if (! err) {
                //
                // Return here on success
                //

				//initiate SSL if needed
				if(bUseSSL)
				{
					if(bUseSTLS)
					{
						if(mClient.Starttls() < 0){
							return -1;
						}
						int replyCode = mClient.ReplyCode();
						if (replyCode/100%10 != 2) {
							return -1;	//bad reply
						}
					}
		
				#if 1
					//ignore error about hostname mismatch within the certificate
					SSL_CTX	*ctx = SSL_CTX_new(SSLv23_client_method());	//TLSv1_client_method
					SSL *ssl = NULL;
					if(ctx){
						SSL_CTX_set_options(ctx, SSL_OP_ALL);
						SSL_CTX_set_cert_verify_callback(ctx, verify_callback2, NULL);
						ssl = SSL_new(ctx);
					}
					else{
						char szBuf[1000];
						//ERR_error_string(ERR_get_error(), szBuf);
						//qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";
						mClient.Disconnect();
						return -1;
					}
					if( mClient.ConnectTlsOpenSSL(address, ssl, true) < 0 &&
						mailpp::BAD_CERTIFICATE_NAME_ERROR != mClient.ErrorCode())

				#else
					if( client.ConnectTls(strHost.toLocal8Bit().constData(), true) < 0 &&
						mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
				#endif
					{
						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Failed to establish TLS connection to SMTP host (code=%1, msg %2).").arg(client.ErrorCode()).arg(client.ErrorMessage()));
					#ifdef _DEBUG
						//qDebug() << QString("Failed to establish TLS connection to SMTP host (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage());
					#endif

						char szBuf[1000];
						//ERR_error_string(ERR_get_error(), szBuf);
						//qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";

						mClient.Disconnect();
						return -1;
					}
					if(mailpp::Pop3Client::OK != mClient.ReplyCode()){
						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("POP3 error: %1").arg(client.ErrorMessage()));
					#ifdef _DEBUG
						//qDebug() << "POP3 error:" << client.ErrorMessage();
					#endif
						mClient.Quit();
						mClient.Disconnect();
						return -1;
					}
				}

                return 0;
            }
        }
        else /* if (replyCode/100%10 != 2) */ {
            ReportProtocolError("SMTP server is not accepting commands");
        }
        mClient.Disconnect();
    }
    else /* if (connectErr) */ {
        char s[200];
        snprintf(s, sizeof(s), "Can't connect to server at %s", address);
        ReportNetworkError(s);
    }
    //
    // Return here on failure
    //
    return -1;
}

void SmtpSession::Close()
{
    mClient.Quit();
    mClient.Disconnect();
}

int SmtpSession::DoTransaction(MailTransaction& transaction)
{
    return transaction.Send(mClient);
}

int SmtpSession::SayHello()
{
    int retVal = -1;
    //
    // Send the EHLO command
    //
    int commErr = mClient.Ehlo(mHostname);
    if (! commErr) {
        int replyCode = mClient.ReplyCode();
        if (replyCode/100%10 == 5) {
            //
            // If EHLO was not accepted, send the HELO command
            //
            commErr = mClient.Helo(mHostname);
            if (! commErr) {
                replyCode = mClient.ReplyCode();
            }
            else /* if (commErr) */ {
                ReportNetworkError("Network error");
            }
        }
        //
        // Check for a 2xx reply code, which indicates success
        //
        if (replyCode/100%10 == 2) {
            retVal = 0;
        }
        else /* if (replyCode/100%10 != 2) */ {
            ReportProtocolError("SMTP EHLO (and HELO) failed");
        }
    }
    else /* if (commErr) */ {
        ReportNetworkError("Network error");
    }
    return retVal;
}

void SmtpSession::ReportNetworkError(const char* message)
{
    cout << message << endl;
    cout << "Error code: " << mClient.ErrorCode() << endl;
    cout << "Error message: " << mClient.ErrorMessage() << endl;
    cout << "OS error code: " << mClient.OsErrorCode() << endl;
}

void SmtpSession::ReportProtocolError(const char* message)
{
    cout << message << endl;
    cout << "Server's reply was: " << mClient.ReplyCode() << " "
        << mClient.ReplyText() << endl;
}

