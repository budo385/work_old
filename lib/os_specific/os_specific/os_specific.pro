# -----------------------------------------------------------
# This file is generated by the Qt Visual Studio Integration.
# -----------------------------------------------------------

TEMPLATE = lib
TARGET = os_specific
DESTDIR = ../../debug
QT += network sql xml gui
CONFIG += staticlib
DEFINES += QT_XML_LIB QT_SQL_LIB QT_NETWORK_LIB OS_SPECIFIC_LIB
INCLUDEPATH += ./generatedfiles \
    ./generatedfiles/debug \
    ./../../../lib \
    . \
    ./../../../lib/zlib
LIBS += -L"./../../../lib/zlib/win32_build" \
    -L"./../../../lib/debug" \
    -L"./../../../lib/os_specific/os_specific/mailpp/lib" \
    -L"./../../../lib/os_specific/os_specific/mimepp/lib" \
    -lquazipd

DEPENDPATH += .
MOC_DIR += ./generatedfiles/debug
OBJECTS_DIR += debug
UI_DIR += ./generatedfiles
RCC_DIR += ./generatedfiles

#Include file(s)
include(os_specific.pri)

TRANSLATIONS = os_specific_de.ts