// get_x_foo.cpp -- shows how to get the value of an X-Foo header field

#include <mimepp/mimepp.h>
#include <iostream>
using namespace std;

mimepp::String getXFooHeader(mimepp::Entity& entity,
    const mimepp::String& defaultValue)
{
    // Check if there is an X-Foo header field
    if (entity.headers().hasField("X-Foo")) {
        return entity.headers().fieldBody("X-Foo").text();
    }
    else {
        return defaultValue;
    }
}

int main()
{
    mimepp::Initialize();

    mimepp::Message msg;
    msg.setString("X-Foo: All cows eat grass\r\n\r\nHi, Mom!\r\n");
    msg.parse();
    cout << getXFooHeader(msg, "not found").c_str() << endl;

    msg.setString("X-Bar: All cows eat grass\r\n\r\nHi, Mom!\r\n");
    msg.parse();
    cout << getXFooHeader(msg, "not found").c_str() << endl;

    mimepp::Finalize();

    return 0;
}
