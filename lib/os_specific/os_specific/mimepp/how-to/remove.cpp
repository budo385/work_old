// remove.cpp -- shows how to remove header fields (bcc field in particular)

#include <mimepp/mimepp.h>
#include <iostream>
using namespace std;

void removeBccFields(mimepp::Message& msg)
{
    mimepp::Headers& hdrs = msg.headers();
    int numFields = hdrs.numFields();
    // Iterate backwards, because it's simpler to do deletions
    for (int i=numFields-1; i >= 0; --i) {
        mimepp::Field& field = hdrs.fieldAt(i);
        if (mimepp::Strcasecmp(field.fieldName(), "bcc") == 0) {
            mimepp::Field* ptr = hdrs.removeFieldAt(i);
            delete ptr;
        }
    }
}

int main()
{
    mimepp::Initialize();

    mimepp::Message msg;
    msg.setString(
        "Bcc: fran@jkl.mil\r\n"
        "From: sally@abc.pri\r\n"
        "To: pete@def.net\r\n"
        "Bcc: ralph@ghi.org\r\n"
        "\r\n"
        "Every good boy does fine\r\n");
    msg.parse();
    removeBccFields(msg);
    msg.assemble();

    cout << msg.getString().c_str() << endl;

    mimepp::Finalize();

    return 0;
}
