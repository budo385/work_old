// unicode.cpp -- shows how to set Unicode text into a message body

#include <mimepp/mimepp.h>
#include <iostream>
#include <string.h>
using namespace std;

#ifdef _WIN32
void utf16()
{
    // This code assumes that wchar_t is a 16-bit char type.
    // This assumption is probably true only for Microsoft Windows.  On Linux
    // and most Unix systems, wchar_t is a 32-bit char, and this code must be
    // modified.
    mimepp::Message msg;
    // Set the header fields: To, From, Subject, etc
    // [code not shown]
    // Set the message body string.  Note that String handles any kind of
    // content, even binary content.
    wchar_t* s = L"Hi, Mom! \x263A";
    size_t sLen = wcslen(s);
    mimepp::String bodyStr((const char*) s, sLen*sizeof(wchar_t));
    // Encode using base64
    mimepp::Base64Encoder encoder;
    bodyStr = encoder.encode(bodyStr);
    // Set the content-type for the message
    // For Intel processors, use UTF-16LE (little endian)
    // For Sparc processors, use UTF-16BE (big endian)
    mimepp::String contentType = "text/plain; charset=UTF-16LE";
    msg.headers().contentType().setString(contentType);
    // Set the content-transfer-encoding
    msg.headers().contentTransferEncoding().setString("base64");
    // Set this text into the message body
    msg.body().setString(bodyStr);
    // Finally, see how it looks
    msg.assemble();
    cout << msg.getString().c_str() << endl;
}
#endif

void utf8()
{
    mimepp::Message msg;
    // Set the header fields: To, From, Subject, etc
    // [code not shown]
    // Set the message body string
    const char* s = "Hi, Mom! \xe2\x98\xba";
    mimepp::String bodyStr = s;
    // Encode using quoted-printable (alternatively, base64 could be used)
    mimepp::QuotedPrintableEncoder encoder;
    bodyStr = encoder.encode(bodyStr);
    // Set the content-type for the message
    mimepp::String contentType = "text/plain; charset=UTF-8";
    msg.headers().contentType().setString(contentType);
    // Set the content-transfer-encoding
    msg.headers().contentTransferEncoding().setString("quoted-printable");
    // Set this text into the message body
    msg.body().setString(bodyStr);
    // Finally, see how it looks
    msg.assemble();
    cout << msg.getString().c_str() << endl;
}

int main()
{
    mimepp::Initialize();

#ifdef _WIN32
    utf16();
#endif
    utf8();

    mimepp::Finalize();

    return 0;
}
