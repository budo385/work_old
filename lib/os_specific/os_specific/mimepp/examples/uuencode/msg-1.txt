From: "Doug Sauder" <doug@example.com>
To: "Joe Blow" <joe@example.com>
Subject: Test from Microsoft Outlook 5
Date: Sat, 13 May 2000 13:06:03 -0400
Message-ID: <NDBBIAKOPKHFGPLCODIGOEGMCHAA.doug@broadsoft.com>
X-Priority: 3 (Normal)
X-MSMail-Priority: Normal
X-Mailer: Microsoft Outlook IMO, Build 9.0.2416 (9.0.2910.0)
Importance: Normal
X-MimeOLE: Produced By Microsoft MimeOLE V5.00.2314.1300

Chief executive Steve Jobs is expected to offer new details about Apple's
forthcoming operating system, the OS X, when he kicks off Apple's annual
developer forum Monday.

But industry watchers aren't expecting Apple to unveil any shiny new models
at the San Jose, Calif., conference.

"The real concern will be on Mac OS X," said analyst David Bailey of Gerard
Klauer Mattison. "It's a very important announcement for Apple, (but) it may
not have the cachet of hardware."

Apple has promised more details on its new operating system, which may go
from the developer preview to beta stage. The company will offer its
programming faithful updates on all of its favorite code words: Darwin,
Aqua, Carbon and Cocoa, among others

Darwin is the open-source core of OS X, which Apple has made publicly
available. That core sits underneath the proprietary Aqua, Apple's fluid new
animated interface. Developers will also learn more about Apple's two new
development environments. Carbon allows programmers to revamp existing
programs to take advantage of OS X's new features, while Cocoa promises an
advanced object-oriented environment to develop new programs.

"This is the year Mac OS X becomes a reality for both developers and
customers," Apple said on its Web site for the conference.

Bailey said it is critical for Apple to keep improving its user interface
and connection to the Internet.

Although Apple was rumored to be introducing its multi-processor PowerMacs
at this year's event, speculation on that has waned. At last year's
conference, Apple unveiled two PowerBook notebook computers. The year
before, it first announced OS X.

"We don't expect any significant hardware announcements," Bailey said.

Business appears to be on track for Apple heading into the conference.

Morgan Stanley Dean Witter analyst Gillian Munson said this week that
second-quarter sales should be flat, despite what is traditionally a slower
buying season. In a research note, Munson said high-end PowerMac sales
should be up slightly, while consumer-oriented models are slowing somewhat.

"This is built into our model but should be watched," she said.

The developer conference runs through Friday.


begin 666 16.jpg
M_]C_X `02D9)1@`!`0```0`!``#_VP!#``0#`P0#`P0$! 0%!00%!PL'!P8&
M!PX*"@@+$ X1$1 .$ \2%!H6$A,8$P\0%A\7&!L;'1T=$18@(A\<(AH<'1S_
MVP!#`04%!0<&!PT'!PT<$A 2'!P<'!P<'!P<'!P<'!P<'!P<'!P<'!P<'!P<
M'!P<'!P<'!P<'!P<'!P<'!P<'!P<'!S_P `1" #A`(0#`2(``A$!`Q$!_\0`
M'P```04!`0$!`0$```````````$"`P0%!@<("0H+_\0`M1 ``@$#`P($`P4%
M! 0```%]`0(#``01!1(A,4$&$U%A!R)Q%#*!D:$((T*QP152T? D,V)R@@D*
M%A<8&1HE)B<H*2HT-38W.#DZ0T1%1D=(24I35%565UA96F-D969G:&EJ<W1U
M=G=X>7J#A(6&AXB)BI*3E)66EYB9FJ*CI*6FIZBIJK*SM+6VM[BYNL+#Q,7&
MQ\C)RM+3U-76U]C9VN'BX^3EYN?HZ>KQ\O/T]?;W^/GZ_\0`'P$``P$!`0$!
M`0$!`0````````$"`P0%!@<("0H+_\0`M1$``@$"! 0#! <%! 0``0)W``$"
M`Q$$!2$Q!A)!40=A<1,B,H$(%$*1H;'!"2,S4O 58G+1"A8D-.$E\1<8&1HF
M)R@I*C4V-S@Y.D-$149'2$E*4U155E=865IC9&5F9VAI:G-T=79W>'EZ@H.$
MA8:'B(F*DI.4E9:7F)F:HJ.DI::GJ*FJLK.TM;:WN+FZPL/$Q<;'R,G*TM/4
MU=;7V-G:XN/DY>;GZ.GJ\O/T]?;W^/GZ_]H`# ,!``(1`Q$`/P#[^HHHH **
M** "BBB@`KS+XK_''PY\*+=(+I9]3\07"EK71K ;YY!_>;M&F?XVP/3)XIGQ
M\^*#_"KP!/J%DJR:_J4RZ;I,3$;3=2 [78'JJ ,Y'?;COFOC&RM)(!)-=W<]
M_J5P0]U?W3EY[F3'+.QY/L.PP!P*B<^4\S,LQ6#BDE>3.[UG]H'XPZ[=+=6.
MIZ#X;MP^Y;"&P^VG;V5Y789/KM5:X+Q]X^^)_P`4;[PLWB/^R[.V\.WIG,FA
M7,L$UTI,9+JKY59%"$*20!O?@Y!JY2$!@01D'J#6/M9'SJSO%W=VG\CZ-T[]
MK?0EE":YX4\0Z="6`^U0)%>1*O'+"-_,_*,]*]D\&>/_``S\0]-;4?#&M6FJ
M6L;;)# _S1-_==#AD/'1@#7YV6L5YH>N+;>9)/H]_GR0[;FM90"Q3/785!(S
M]TC'0BM-]4U;P-J2>,O"CK;>(-.'F.H&$OX1R\$H_B5@.,\J<$8-6JNMF>GA
ML[;FH5DK/JOU1^DE%8_A/Q)9^,?"^C>(=/+&QU:SBO(=WW@DB!@#[C."/6MB
MMCZ(****`"BBB@`HHHH`***JZAJ=EI-L]UJ%Y;VELG+2W$JQHOU)(% %JBO&
MO$/[4GPTT2006FL3:]=L[((=#MVNAD+GF08C'4#EQU] 2/&O%?[4?CV\G>?2
M;/0/#.BQJ=TFHEKRX(_O$ADC0@=OG&>YJ7)+<YJV,H4?XDDOS^XZW]L^[AL=
M$^'D]Q(L<*^(<%V.`,VEP!D_4BOFW_A+M';F&Z>X7IOMH))E_P"^D4BJ_P`2
M?B#IGQ?AL8/&OCRUU.UL6,D%NEQ;V\:.1@OB-02<<<DCKBLBTTKPK9Q0?V7X
MBELH(U_=1V^KL8QGN$9F4]>XQ6,VI.Y\YF52CB:BG:6BMMI_7R-]/%^B-)Y;
MZA';R9P%NE: D^P<"MH$$ @Y![USEIK4$+A+KQ!I%W!V8NL<@^N&()^@7^E:
M/_"1:/\`]!:P_P# A/\`&LVCQJE*S]U/^ODC(\53*^O>$+$MM::^><$=<1P2
M$_S _&I?$WB6?1;BV@@TV:[60%I7"L$4$$*NX C)(]^!TR5!RM=U/2[WQAX,
M>"^MIIHY[G_5S*V%-N^<X/J%KN>&`/!!YIO2QK.U-4^>-]'IMU9]4?LB232?
MLZ^"!<&3S(XKF+$C;F54NIE5<^P4#\*]MKYN_8SO[D^"O%NBSJGE:1XAN! 5
MX_=S(D^/P,IKZ1KJ3NC[RE-3A&:ZJX4444S0****`"L'QEXPTGP'X=O->UJX
M,-C:@9"+N>5R<+&BCEG8D `=S6]7F/QO^%=_\6?#VE:;IWB!-%N-/U!;X2RV
M8NHY,1R)M*;E/_+3((8=*"9\RB^7<^6?B=^TMX\\1M;6]K=-X/TJ]D$5O8:2
MGVO6+USP(P^"$8\<1J2"<;CD57\(?!#Q_P#$B[M+O4O#%];:<@\R._\`&.H2
M2388_,4@;S'5^.C!.W(KZ=^$W[/7A_X8W#:O=7$GB#Q7(I1M7OHU#1+G[D*#
MB)?7')[D]*]?J.6_Q'$L)*JKXF3;[)M+\-7\SY"\0?";PKX#NK'2_$7BC6=7
M\0WX9['1/#EG'#+-& 02_F>9L0$C]X7B&5[]*RKC]G#P=K]U9:KXJM;F*.W,
M<*Z6NJ33IO9@%\Y\@,Y)48C6-1D_?X:N\(_LWXK?%.VUP3IJ-X]I>V=S"6,L
MNG&%8XTBVC=\DT<X( ^\_?=5F33O%,?Q*BNM/&EV_A&6$/?[=HN+B<(Z\@1[
MBP_<88N %4C!)&/&QN*G&;I0]U+[V>_EV586E"-6,$V_+8Q=1^'7PXN-.O\`
MX96^CV&G2:EIC3>7:VP63R@^T2B3'+*[`C))SZC-?FKXY\$ZA\,/'%[X?UNV
M\R6PG!YX6YBSE7!YP&7\LXZBOO[Q\OBR\^,EC>^!].L/,T;19;&\UC6976R@
M::1&5,CF1U"9P/\`GH,FN'^//[,]WK_A;6_&>I^+]2UCQ9IMDT^UX(X[9HX\
MNT<<:@L@QNQ\QYZ]34X*O[&2YY:2^^YU8NC[5/DCK'[K'QYXGU;PWJ/A7PY#
MI-C)::O:SWRW@< [H6D5[?YP!O(#2*3@=.@&*^F?A#^RE!XW^%NC:Y=:\;"]
MU.*25(VTJWG"*78(277><@`_>[\'%9/A/]AS5];M=%U2\\4V,>FWL<-S+'#"
MYE$;@,5&>-V#C/2OM*_O="^%_@AYVC^R:#H5HJI%$-Q6- %5%'=CP!ZDUKC<
M<FE"@];F>#P33<JRTL?%OB32M.^"M^W@/Q+X9T7Q5%%#'/IUVMNEO=W4<C;<
M%2&W,)&9``V[`!QC)%#Q3\-]5T#Q!!I]GJ-YH^I>4L[>'/#(N-4GMHSR#,TD
MB1+R> " 0>_4^B7'C6'1=6\&:AXXUC3D35-<FU2YFCA\QXTA21HHVVYWNCB&
M+*@8*@#.,GGO#'B#QG?_`!?\5^-K#1-6MO"NKWT,;Q7</EB6($)YKEP"JI&'
MDR.A(7OFI56<4YK>W?1N]O3O_P``GZO1K/DFKJ^UKV7]?TSOO@GXXU/X+:)K
M^KVNO'Q'X;MKU+GQ%I6I:0;#5[,NJJ9TRYWJJ[#@Y4JK!2".?O*SO+?4;.WO
M+29)K6XC66*5#E71AD,#W!!!K\\?'O@3Q-XV:W\;:+(N-<CF\.6&G$;&NK2Y
M0PI*3U;+NTV#P(XPWK7Z`^&M&3PYX<T?1HY&D33;2&T5VZL(T"Y/UQ7H8.K*
MI"\G_P`#R.7%TH4I\D%9?IW-2BBBNLY0HHHH`**X_P"(OQ.\._"[28-0U^YE
M#74H@M+.UA::YO)3_!%$O+']!W(KB'^,/CLZ4VKQ?!7Q(VFX,BHU_:+>&//7
M[-OW!\<[.O:@#V>BN'^&OQ:\,_%:QO)M!N)UO-/=8K_3KV!H+NRD.<++&W(/
M!P>0<'!X-=Q0!YO\5?A4_C_^S=4TC7+CP_XKTC>++4H4$B%'*EX9HR0)(FV+
M\I/!`(KP;Q;\5?%'P=U&RTCQ?\/S=W^J&1X+GPM<)<"^9,;Y/(;;*G&W)8''
M3)K[!K\_)/B392>)O%_B_P`5SQ6NKR:[<:3+'=2[7T^VA?9'"@8<@91V`QDL
M[=L5P8ZE3<.:4;LZ\)6J0ERQE9%'5=9T+XG>+]1U+5?ACXLUZ6(K:36^I:M#
M#:Z=F-2$CC20`D[E9LY92<]<"N6T_P`1?$GX9^#T\(7_`(7GNO"4U^\UW=6=
MQ]IDCT^3!DM0>2O5@7(Z$X_O"]\"?B1I<FF:EHNJ7T::Q!>W5Q-/)("DX,@)
M??DC^,#/`P..A->[5YE2HZ4O9RCHO7\-3LBW-<Z>OR_$JZ+^TYX`_LV!M1&H
MZ#&L.^..>PDEB,:\9CD@#HRC@9!P.E<M\8O'-I\3'T;P9H]G+/I5])'?3W-_
M;W%O:WR)\ZP)((SU.UF)P,* ,EN.<T+0K&Y\?^+=(9C$MA?6NM68B.&B::/$
MZ\@C:Y1@P[B0]#@UZ;86%KI=JEK96\5O;(25BB4*JY))P!TY)/XUC)4Z<E**
M=_4U]K4J1Y96L<C\._AWH7B_QGXKT[Q/I%B\&FZ39V5IIL)(A@AE+M(T6 K 
MEXP-X (VXK%U_P`$Z+I_B^XTG2-9UGQ-I6FW"RKX5CF$>G:>X((CN;EBQ=,D
MGR0&8#@C`JMXTU72_ OQ&/B;Q!JVM:9I-]H4MA'/I#!)&G20.(BVTX+AOE.1
M@J>1R:ZW]G3X<_!GQ/>W>AM83:_XE6#^V+VY%S<2Z?!YDG%NDF_;*4#*&)#!
MR&Y."!T1C)KVC;M9=-//^EJ8SK1IQ44M=?Z_X<O?!/X]:3;_`!$-I\0K.[M_
M$&HW#6.FZRH#:-""Q"06[#IN&W,C98L=K;0H%?:M<CX@\">'?$_A*?PIJ6D6
MLGA^6$0?8DC")&H^[L"XV%<`J1C!`Q7G?PV\5:O\/O%R_"SQKJ,M\94:7PQK
MMUM#:E;(!N@E;/-Q%G&<#>H#=>OHX/$PFO9I6L>;4;DW)NY[E1117>9A1110
M!\^?`>)OBMXBUSXP:T7N6:\N]*\,Q/M,-GIR2;3+&!G]Y*RG<_7 P#M//T'7
MS;^Q;J#:?\-M7\"7LK_VSX*UB[TZXAE.'"&1G1]O93E\8)'RGFF:CXD^,VJ:
MQX.O8+O[#X;\4ZC/:W-OI&F13W.@Q*&$+RSR>8C,2O[S,853N4$'! !H_&K3
M_P#A67Q#\)_&#3)1;0-<PZ#XEB ^6YLIW"1RG_:BD*\]2"!G`P?H2OE_]J+5
MI-6_9%U.778F_MF\2R@:*'"F2\%Q&"%4$Y&Y&.T$\#OBOIFR4K9VZL"&$:@@
M]N* )Z^'/BWX=TSXB?%?Q+KVD6MO;V^F+)IDQM)/FU>\1=DAF##8OEX\I25+
M!E+9X6OLCQAXHL_!7A;5_$.H;C::9;O<.J?>? X1?=C@#W(KXY\$6&K*WB'7
M->M8+/6/$>J3:I/:6[[TMQ)C;'NZ$J `2!R<UPYA5]G2LGJSIPT.:>I\[G]G
M*[/VZ"VL\^8T/V:252JC:ESN#'.X*TB1$]3MD7N./3;'2OBAX)T69M.N-.U;
M3[25Y(=,OMQN5M@`5B28-AB!E?F'5>X(%>S45X\\7.?Q69W1H1CL>6^"/$FG
M>*?B%>ZO8.GE:AH%G(%R-P99IPZG_:0E5/IQ[5ZE7D?C'X&0:IXE'BKPOK$_
MAWQ#G=))`N8ICW+*""">_.#W&237?>#].US2M"@M?$.K1ZMJ49;==QP"+<N?
ME! ZD#OQG]3%;D:4H/Y=1PYEI)&;XIU;4=1U>#P7H'AZ75_$>JVKSV@F$8M(
MPK!3)(SMT0E6( )QBOH_X/?"N;X=:;<W.KZL^L>)]36/[;>%`D2!`<10H -L
M:EF/J2Q)]O%_A/HT_BG]H=]6@G1=/\&Z089P%R7N+O.$SVPD8;OU'KQ[W\0O
MB#%X).CK&UI-<7.I65M<6KOB46]Q.(!*BCKB1TZ\8SW(K11M%0BM6<=>;<FK
MZ([BN"^+WPW7XF>$9+"WNCI^O6,JWVD:FGW[*\CYCD!ZXSP?4$UWM%1&3BU)
M;F)R/PD\=2>/O!UO=W\/V7Q#8.UAK%D1@VU[%@2+C^Z3AE/=66NZKP_Q$LWP
MR^,V@>)+1F'A_P`=7":+K$&"5CO1&?LEPH'0ML,+$\8*=Z]PKZ.A556"FC)J
MS"BBBM1'@GCWP/K7P\^)LOQ>\%Z;<:O]NMDLO$GAZU \Z]A7`2X@'1ID`'RG
M[RY ().>&N/VU/!.@R:E)8V&M7%E-:R7LL!LF672+W<RO;W*YP@=P&RI;YF?
M/!&/?/C'XRNOA_\`#'Q-XCL5C-[86N8#+RBR,P168=PI8$COC%?GC^UEX''@
M_6],\67/B2?6-8\27*G5+#;';K<LB;5DC2)0$7;\F3ELG.XDDUE.K&$XP>[V
M-(TI2BYK9&Q\'_B^/C1XZ^&'A;Q=J\&A^%O"7V:Y@M&0LVL:DK!8RS 84;V.
M,\ <=6&/TNK\^/V>_"$/C3X1^(YHY[2'QL^HHX?RU"Z?+;-'+:0X'_+%616V
M\@[F[YKZI\#_`!VAUWQ)I_A'Q)X;U;P[XHNXY/+,\0>PNY(P2ZV]P#^\.U2X
M!53M!R*BGB83G*GLTRZF'E"$9[IG:_$CP7;_`!%\":_X7N93"FJ6K0K,O6&3
MJCCW5PK?A7RAX'U2^U7PW:MJJ"/6+1Y+*_CZ;;F%VBEX[?,A./>OMBOGWQ7\
M`-;M=<U[5_ VKZ9"FLW37\VF:M#(8DN'QYCQRQG*AR&<J5;YB<$`\98[#NO!
M<NZ'AZJIRUV.+HKB_%_C75/AC>Q:9XW\*:K9W\TT=O!<Z?$9["[D?E5BN&V*
M6ZY5MI&#U'-,MO$_CC5]7ET+2OA;X@.N+;+=B&^E@MXEB9R@=G+X`W*W`R3M
M.!7B/"U8NS5CO]O3WN=O7.7/BA[W6;;P]X:L)]=\0W1VK!:#,-MD9#W$O2),
M<Y/)QP#76Z!^S3XK\1ZY:7/Q.\1Z9=:#:9?^P]!2:&&[?C;Y[L=S*/[@X.![
M@_1^B>'])\,Z?%IVBZ99Z;81#"6UG L4:_15`%'LXQW=S&>)Z11QWP;^&C?#
M'PF]C>WJ:AKVH7+WVJ7Z(5$\[X&%!Y"*H5%'HN<#.*X']J/P+>ZKX5N_%UMJ
MMS&GAVV6ZDLHU;+1Q2K,YC*LOS,$&0^Y3Y:'Y2NZOH"F2Q1SQ/%*BR12*59&
M&0P/4$=Q51J-3YSD>IXK??'V"6PM]1631?"NB7B V^I>+;Y(9925W QV:-N=
M<8SODB/H""#6)=?'#P7XGMS%9^*_%VOOPS1>%=)N8XR>ORRI'D >AE/'K5#4
MOV?_`(?^%/BGIUSI_A/28[&\T>8+;R0+*BSQ31YD4/G#;90/H/K7ID420QK'
M$BI&HPJJ, #V%*K6IP=HK^OQ.[#8+VT>=RL?*=]H#?$3XN>$_#?AWP7XVMKJ
MTNK76FU;Q=X@GE*6$%POF,MM)(W\14 $[AO^Z,Y'Z!UYOX5@TP>-(9W51K+:
M=.D+;1EH!+"9!G&<!C'QG')KTBO<P,_:45(X\52]E5<$%%%%=9SE/5M)L=>T
MR[TS4[2&\T^\C:&>WG0,DB$8*D'J*_-?]JGX.:%X:\9:UK'@."ZAM_#-E;C4
M8'B>]BAN9!N2,>86VKY)\QBP*J2G]_C]-*^3/C!/X@^&_P"T#IEUX;MK;4K;
MXA6;_:],O;@PJUU9Q*H>.7:P5FAV+M(P=G/:L:\I0@Y0W7],UHQC*:C+9GR=
M^SGI4_B_4?%CW?B2]TOQ]=0VUSHJ_:)+8W?ELSL"``KQD*B[><*6(7BOLSQ+
M8ZEX]\!27/\`8D^C^*M-D%[IT%W(C-!>P'=&RR1L049AC(/*L<@=*\1^/OC.
M\U:R\&Z5XT\$77A2PN=4BD?6)+Z.YDM!'AG$(M6=R2A=<D <@X[CTW1OCK\.
M-$T:WCTQ]:7PY:KAM1.D7IMK?+[1YDLD>>7=1DYY8<UXE=U:K5:$7?RUM;S7
MY'L452IITIR5O/2]_)_F>^?"3XM>'_B_X2LM;T:ZB^T-&/ME@7'GV4W1HY%Z
MC!!P>A'(KO:^>1H?AOQFEMXFT"^C@U!ES:Z_HDRK-@8XWKD2+P`4<,IQ@K6=
MXK^.?Q(^&_AV^74_`Y\3W:Q%;+5]"XC:0@A3<6Q)>/G&2A93[=*]+#YA3JOE
ME[K\SSZ^!J4O>CJO(YWXRZ4GQ'^.UPB:IJ>FZ#X,T&2'5[V*16A,T^)1"(I%
M>-L18=R5.1L4]!3OA9\`X?'W@.+Q3XIG>UU[5XXWTI[2TBM4T^Q1V:!3:J/)
M8N&:1P5/^M(&,9H\3>'YM(^&6E^#HV=]>\:7L&FW]VN#+/-<D&\N&)^\WEB9
MN>@`'0`5]40PQ6T,<,,:1PQJ$2-%"JJ@8 `'0 5Y]7%2J-SCHF]/1?YCKT8T
ME&'7=_,X/P-X)\4>$I8HK[QM_:^DQ1^6EB^D6]L(P.A1HMN/Q!'\Z[^BBN:4
MG)W9SA1114@>?>,[C2_$7A2S\4:=,MTFCW O(IX>JH,QW (//$;294\AE'&0
M*@KC-0\/3_#7XBWNIZQ=7=_X"\6H--N9))%*V<\C;8Q=!AF1&W>6DV=RY5'+
M+M*ZG@R\GN="C@N\?;M.FFTZYQQND@D:(MCL&V;A[,*6(A9*2V/5RVI\5-^I
M@:MXWM/"/QX^&-O<75I&FM6VI:5(LDH#KYGD21,%]#);A,\<M7TE7QO^T/X/
M\/R^'+O5Y;&RE\5ZM=:?I>FWMZF_[-*;A=FQL9C7YG9MN,C/7@5]BQ;_`"D\
MPJ7VC<5Z$]\5[>623H)+I_PYQ9C%JNV^O_##Z***] X0KF?'/P_\.?$C0Y-'
M\2Z9%>VC<HQ^62!O[\;CYD8>H(KIJ* /(/!/[-?@GP/XJMO$\#:UJFM6D;1V
ML^L:E)=_9]P()0,<`X)&?<XKTKQ'X>LO%6C7.DZBC/9W&W>JXR=K!AU![J*U
M:*226B&W?<_/WXG>%/$_[)6OP^)/",TNI>$+IK.VO]):!EAD8Q,ID3 VK*?L
MSEB,D&1200<5]'>#/&VB>/\`0XM8T&]2YM'.UQT>&3 )C=>JN,\@U[)K>AZ=
MXCTRYTS5K*&\L+J-HI89EW*RLI5A[95B,CUKY^^+_P`!=2L/$%I\1OA/:PV_
MC&WD']I:8T_DP:W <[@^>/-S@AF(SR2<@5Y^-P$:ZYHZ2_,[L)C71]V6L?R)
MY)?[7^/7@31_+=H]*L+_`%J1C'N0-M6WCY_A/[Z0Y_#OQ[]7R=X>^)%YIW[2
M7A&+Q#X0UGPV?$.ASZ7"VK&)5,PD6;"&-G#Y*!.2IRPXZ _6->5.G*FHQDK:
M?JR<3452K*47=!11169@%%%%`%/5])LM>TN\TO4K9+FPO8F@GAD'RR(PP0?P
M-?)G[,WB34]:MO'UAJTHN+O2=?DM#<AF/FB.*.+<0Q)R?)WDYY9R>*]%_:._
M::\/?!+0;NSMKJ&]\;7$)%EIR?/Y+$<2S8^Z@R#@\MT'&2/EW]@W6);S4_B#
M%.#)/=?9;IYB>K;IL\>Y;-=#HR^K3FUII;[SJP,K5XKU.O\`VL=<-QI%[:AI
M[P:%-97RVUO LEK [3(NZ\W??+!MJ0J>0S,P((*_=.A22S:)ILD\@EG>WC9Y
M%@,`9BH)(C))3_=))'2O@#]IDS:WXPT702DCW%QJ5NL'VF[B1Q",/(;=3^[A
M4;26FF^8\J,H&K[Z\-ZS8^(-"L=1TV^@O[.>/Y+F"82I(0=K8< !OF!&0!TK
MT\NC:@B,>[UF:M%%%=YQ!1110 4444 %%%% 'R?^W1#]I\->"T6"X%Q#?7EW
M;W=NQ1X)H;">2/:P.X-O5&&.OEU\Q_"_]N[XF^'Y+/2M9L[7Q=$^((5GS#=N
MY.$'FH"&Y./F4D^M?7_[:VDS7_P?M;VVN'MKK2M8M;J.XC7,D;$M&"I[<R#/
MJ 1WKXML/&-SHZK>:;X5\(V_B%=W_$V2Q*ON.<N$!PK<^N/;M656%.>E1'+6
MQU'#RY:LK,^J=)_;V^'8N;JP\3:?K&@ZG9L8YH]B7<7F#AE62)CG!SS@=/PK
M7'[=GP9)P-8U,G_L&R_X5^5.H_:/[0N_M3%[KS6\UF.2SY.3^=;.E>$-3U*U
MAO;4P>6QRI9\$$'TQ[5RRP%'?4WJ8J%*//.229^AOCS_`(*%>"-%M9HO"NC:
MIK.IXPANH_LL"G_:)RYQZ;1TZ]Z^6_'?[;?Q9\;6ES90ZG:Z#93\%-(A,4FW
MGCS6+..V2I'3MR*\UU?P)?ZEKD]PLT*VTS[BY)RO'/'UK1L?AQIT!5KF>:Y8
M=5^XI_ <_K54Z%"FKVNSSI9UAHP4I2U:V1RJ^&]1\06=GJ$(:>>X+B>663))
M#<,2QR>/3TKZ=_8QT4:!\5-=M5F,K-H(DD/;<9T&!^5<;X.\'ZSXWU^+PSX6
ML[?SHHA+<3S';;V4.<!FQR23T4<G\Z^U?A-\)=,^%6D3P6\[WVK7S*]]J,JA
M6G89V@*.%1<G"CU).2:Y<?BHJFZ;W9W9!'&8JLL3-<M)7MW?;[CG/AY\,K+Q
M]\>_%=SXQMX-8L_",,$ME:FS$5DEU=,TAD"#/F2+%''DR%OF?( `3'UPJA5"
MJ % P .U?)>L?&>W_9K\3^()]?T'4M3T/Q?>"^LKO3Y(VDCN$MXHG@='*A5Q
M$&5@23DC'%<5K/[;/C;Q9;W/_"%>%M*T5(=N)-9F>XFDRH8;40*J@@X!);/\
MO2P%.56C!4U?3_ASJQTU2JR=1VU_X8^ZJ*XKX5_$O2OBKX+L?$>FMY9DS#=6
MKGY[6X7'F1-[J>_<$'O16IF=K1110 4444 %%%% 'F7[0^B-K_P2\<6D:EI8
MM.>\11G): B88QU.8Q@=^E?FE7ZY7]E#J5C=65RF^WN8FAD7)&Y6!!&1R.#7
MY >)/#=Y'<S:?-.;>^LG>"24!U99%;!XR,<CG(_"L:JV/G,^IQE*FY.RUU^X
M\R\>Z5)9:U)=!,6]UAE(Z!L`,#[YY_&NE\$Z;>:]IB6%GJ#V,]E:F[C")O-S
M-)<I;PQ$=@7E4DG/'8\5MVRP>)='$5_"'96*31\C;(IP<$=/\#76_#BVM+.[
M\8,NV./2K32]0\M<;O)M[Z.23&2.`!R>W&:<97]UEX'$QQ')A*T;N%_3167Y
MG,:K>W_@_P`0W'AOQ;9_V;K-L54D'=%*#T=6'\)'?^7(I+KQ+I5FVV2\1GP6
MVQ R' [_`"@XKZ(_:#T*/QZ^K>(]+T>WNK/P$4MM;NIGPT\4H1VCC3!W>6'6
M0L2N.0,Y->%P6-II-AJ%J(HY+;3(QJ%I(H!8Q-O.S)Z_=9?=6%=.%P=/$N7+
M/X=&NVE_R,L9E5"%1-)I/HCH_@!\3-1^'VM7]S:Z);W&D^(=7M+>ZNKB3%S#
M&7\O:J>S2@C)Z=N<C]!*_,:R\8:-I&N6XDN%:S66VF8+\Q'DRQNC#'<Q$?\`
M?OUK[#_:%U_4-6@\&^"_#,>I7^I>*+Y97@TB;RY9K&(;Y ).`BME`7) `R>0
M"#Y&<X&"Q%.%'>2U]5_P#[/)L3*.&DJFT;6]/^'./_;0N3J&D^"]&LC -0DO
MI=12=@"8HX8\'GLI:1>.Y ]*^=?"6M"_UR.QT33=2U65K811VNF6KW+G`$BH
MH7^Z'=>>FP9Q7W!X<_8\TSQ!KEMXF^)]RFJW4$"6]GX>T]FBTW3X%^[#GAYM
MOJ=H)SE3QCZ2T/P[I'ABQ6QT32[+3;)>1!9P+"F?7:H`S7IY;.6"HJ$=]V<.
M/C'%U7.6Q^=WA+]G_P#:,6WU"\T&SL_#>GZC=M=K8ZA>HLQ+(@WE4W;20HR&
MP00<CIDK](Z*<I.3<GU%&*BDET"BBBD,****`"BBB@`K\QOCGIBZ/\7_`!E:
MJ !_:#S<9Q^]`E[_`._^=?IS7YX_M96HM?C?JQ\S<;JSM;G&W&SY/+QGO_JL
M_C6=5>Z>+GM/FPW-V?\`P#Y]LR+;7+^V'"SHER!CC/*M_P"@K^==1X+UJS\+
M^/+.]U4[= UBTET+4WW;?+AGP Y/8!\9/85RU["8]>TRZ!P'62W;CKD;A_Z"
M:T+J""^BFM)E#HZ8=3Z'/^%8IV:9\]1Q#P]:G76NFOY/\CZATSPCH7Q-T/6=
M/U#5-4L?%=E#_9NO1Z3J,EN;X(I2.2:)25D26/YE+ _*Y&:^,K/6;SQ99:9H
M)FE@M;2V_P!.V#8[LK%1&?; !_.NV\"^,M3^%7B>RUU].N/$,-I%]EC\JY>&
MX6':56-PN5E5=WR[E)7 P0``.:\*::]M'>WUQ T-Q?SM*8W^\BY)`/YFNE5G
M3C+D=N;<^@Q^/HRPWM:4KOIW3-.QT6PTV%HK:UC1'!#<9+ ]B3R:]<_9/T^>
M3]I#PLL4EU-!INDWC;9[AI%MX=NQ50,?E4-)]U?[V?6O,J]D_8__`.3BX_\`
ML7KK_P!'15ST]9'DY-6J2Q:3D];WUWTZGZ+4445TGV(4444`%%%%`!1110`4
M444`%?#O[;-GY/Q!\-WFYS]JTIH@NWY1Y<S'.?4^=T]J^XJ^0?VZXHHX/A]>
MO(5?[1>6BK_"=Z1OGZ_N0!]3435XLX,TI\^$FO*_W:GQAKBXMH)@2#;W$4F0
M,\;@&_\`'2:N7-R+=H V-LLGEY/8D''Z@#\:9J4#76GW4*_?DC95QU!QP?SJ
MCK,WFZ']K0,-GEW &<8`8,?T%<ZU/BZ:4U&+[V^^UOU-BB@'(R**DYPKV7]C
M6WFNOVC+R6/#067AR;S.0-I:>,#Z]J\:KW']AU@?CWXO`()'AX ^W[^*M*7Q
M'LY$KXJ_9,_0FBBBND^R"BBB@ HHHH **** "BBB@ KY:_;T\.1:U\(=*O3<
M:?!+I.LQ77^E7*P231B&8/'"S$;I,8<)G)$9QD@`]K\:_P!H.V^'FHCPKH-M
M'J/C.>W^T[)LBVL8B<"28CDD\[47DXY*C!/QQ\2]/O?B;?/KFO7?]J>((X3%
M#)=*HB"<D1! ,(F2>1\P)SD]^3$8VE0?++<Z:6"J5XMK;^MCR*VO[U(-ZQKJ
M,!YCFMG4,P_V@<#/T/X"L_2=2CNK34]+NLV\R/(@28A3L?) R..,XXKT#1_A
MS;36)DNH&T^]?#I-9/Y38(R!)&,Q[EZ'`(/6L7Q/X0N/"PU#60_]IZ9(B?:H
M9% D0+QN`^ZP]1Q7-#&49RY+ZGS^)X:Q%&$IP2DM]-'H^VVU]B+3I/-T^T?=
MNW1(=V<YX'-6:Y:RETU(8HH]2GL)C$"87?: 2,\*X('X<5JB.1@"-9<@\@A8
MN?\`QVNEH^6K8=PFT]/5-?H:G2OI[]@30OM6J_$CQ;&=UG/+:Z;"X'RNT:%I
M,'VW1]#W^E?(L.ER>([N/2M.74/$>J7))@TVQ'F-)@9.5C'W>Y)XK].?V9OA
MA>_"3X/Z+H&JQP1ZU(\M[?K <JLTKEMO''RIL3CCY>*UI1MJ>[D>$<)2K.^U
MMK+_`#_ ]=HHHK8^C"BBB@`HHHH`****`"BBB@#Y&_:(_9&UKXA^.KOQYX+U
MZ&TUR^ABBNK2]FEB1BBA`T<L?*_*JY4J02,Y&:\,O_V>OV@]&F,4&AS7P+$>
M=9ZK9R(0#@']\ 1GKT'O7Z645A5PU.J[S5S:GB*E-6@['YER_ _]H4H0GA;5
MR3Q@WNE+^H:L#QU^S]\3O!GP_E\=>.[B*VTFPGMQ/I,VH&>X97G"'Y8@(NX/
M+-P?48K]4ZY;XC> -'^*'@W5/"NNK*=-U% K- P62-E8,CJ2" RLH(R"..0:
MB&#HP^%%RQ56>[/S'\365WXT>&VB\/VUWI&$DAOKB_:.-P1G<$C.2,' S6;<
M?!_3EMR8=)T]K@'*E;J>,?3YC(#SZBNR^(OPH^(W[/.H7CM9_P!K>#59GCU&
M.-FM]F>-X7)MWQUR"A)XK@)?BMJVM20V.CV=I'>S-M1+>1KZ:4]E2)57+&O)
M=+$TFH0T7J_\_P`CTU4P]1.<]7\OZ^\]O_8:M=:\/?'77M)O8XK6UNO#C3&W
MB,1WF*>%49M@'($CC.!G/>OT1KY&_8R^`NO^!9M9\>^,;5[37=<MTM[6UG??
M/%;DAW:7^Z[L$^7C;MP0.@^N:]RDI*"4MSQJC3DW'8****T("BBB@ HHHH *
M*** "BBB@ HHHH **** ,CQ7_P`BMK?_`%Y3_P#HLUXU^S7_`,BW<_\`81D_
5]%1T44 >^4444 %%%% !1110!__9
`
end


