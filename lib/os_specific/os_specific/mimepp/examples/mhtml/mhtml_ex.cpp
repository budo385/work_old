// mhtml_ex.cpp

#pragma warning(disable: 4786)

#include "MhtmlGenerator.h"


int main()
{

    // Initialize the library

    mimepp::Initialize();

    MhtmlGenerator gen;
    gen.setHtmlFile("test.htm");
    gen.setMhtmlFile("out.mht");
    gen.generate();

    // Finalize the library

    mimepp::Finalize();

    return 0;
}
