// mmap_ex.cpp

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <iostream>
#include <mimepp/mimepp.h>
#include "MmapStringRep.h"
#include "SystemError.h"
using namespace std;
using mimepp::String;
using mimepp::Message;


// Recursively print information about nested entities (body parts).

void PrintEntity(const mimepp::Entity& aEntity, const String& aIndent,
    const String& aIndentAdd)
{
    mimepp::MediaType::MType type = mimepp::MediaType::TEXT;
    String contentTypeStr = "text/plain (implicit)";
    if (aEntity.headers().hasField("Content-Type")) {
        type = aEntity.headers().contentType().typeAsEnum();
        contentTypeStr = aEntity.headers().contentType().type();
        contentTypeStr += "/";
        contentTypeStr += aEntity.headers().contentType().subtype();
    }
    if (type == mimepp::MediaType::MULTIPART) {
        cout << aIndent.c_str() << "+-" << contentTypeStr.c_str();
        int numBodyParts = aEntity.body().numBodyParts();
        if (numBodyParts == 1) {
            cout << " (1 body part)" << endl;
        }
        else {
            cout << " (" << numBodyParts << " body parts)" << endl;
        }
        String indent = aIndent + aIndentAdd;
        String indentAdd1 = "| ";
        String indentAdd2 = "  ";
        int i = 0;
        for (i=0; i < numBodyParts-1; ++i) {
            cout << indent.c_str() << "| " << endl;
            const mimepp::BodyPart& part = aEntity.body().bodyPartAt(i);
            PrintEntity(part, indent, indentAdd1);
        }
        if (i < numBodyParts) {
            cout << indent.c_str() << "| " << endl;
            const mimepp::BodyPart& part = aEntity.body().bodyPartAt(i);
            PrintEntity(part, indent, indentAdd2);
        }
    }
    else if (type == mimepp::MediaType::MESSAGE) {
        cout << aIndent.c_str() << "+-" << contentTypeStr.c_str() << endl;
        const Message* msg = aEntity.body().message();
        if (msg != 0) {
            String indent = aIndent + aIndentAdd;
            String indentAdd = "  ";
            cout << indent.c_str() << "| " << endl;
            PrintEntity(*msg, indent, indentAdd);
        }
    }
    else {
        String str;
        str = contentTypeStr;
        if (aEntity.headers().hasField("Content-Description")) {
            str += " (";
            str += aEntity.headers().fieldBody("Content-Description").text();
            str += ")";
        }
        cout << aIndent.c_str() << "+-" << str.c_str() << endl;
    }
}


int main(int argc, char** argv)
{
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " filename" << endl;
        return 1;
    }

    // Initialize the library

    mimepp::Initialize();

    const char* filename = argv[1];
    try {
        MmapStringRep* rep = new MmapStringRep(filename);
        size_t offset = 0;
        size_t length = rep->size();
        String str(rep, offset, length);
        Message msg(str);
        msg.parse();
        String indent = "";
        String indentAdd = "  ";
        PrintEntity(msg, indent, indentAdd);
    }
    catch (const SystemError& err) {
        cout << "System error " << err.errno_() << endl;
    }

    // Finalize the library

    mimepp::Finalize();

    return 0;
}
