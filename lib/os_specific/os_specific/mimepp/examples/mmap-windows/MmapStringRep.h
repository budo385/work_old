// MmapStringRep.h

#ifndef MMAP_STRING_REP_H
#define MMAP_STRING_REP_H

#include <mimepp/mimepp.h>


class MmapStringRep : public mimepp::StringRep {

public:

    MmapStringRep(const char* pathname);
    MmapStringRep(const char* pathname, size_t offset);
    MmapStringRep(const char* pathname, size_t offset, size_t length);

    virtual ~MmapStringRep();

    virtual char* buffer();
    virtual size_t size();
    virtual bool isShared();
    virtual int incRef();
    virtual int decRef();

private:

    HANDLE mFileHandle;
    HANDLE mMappingHandle;
    char* mBuffer;
    size_t mSize;
    long mRefCount;

    int init(const char* pathname, size_t offset, size_t length);
};

#endif
