// Text.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include <mimepp/mimepp.h>
#include "email/email.h"
using namespace mimepp::email;
using std::string;
using mimepp::TextDecoder;
using mimepp::TextEncoder;

/**
 * \class mimepp::email::Text
 *
 * <b>%Text</b> represents a text string with preferred encoding and language
 * identifiers.
 */

/**
 * Default constructor.
 */
mimepp::email::Text::Text()
  : mIsNull(true)
{
}

/**
 * Copy constructor.
 */
mimepp::email::Text::Text(const Text& other)
  : mIsNull(other.mIsNull),
    mText(other.mText),
    mEncoding(other.mEncoding),
    mLanguage(other.mLanguage),
    mIsFlowed(other.mIsFlowed)
{
}

/**
 * Constructor that takes an initial value.
 *
 * The character encoding identifier should be one that is registered with
 * IANA.  Examples: "UTF-8", "ISO-8859-1", "Shift_JIS".
 *
 * The language identifier should be one that is specified in ISO&nbsp;639
 * (see also RFC&nbsp;3066).  Examples: "en", "jp", "de".
 *
 * @param text the text value of the string (UTF-8 encoding)
 * @param encoding the external character encoding identifier
 * @param language the two character language identifier
 * @param isFlowed true if the plain text is flowed
 */
mimepp::email::Text::Text(const string& text, const string& encoding,
    const string& language, bool isFlowed)
  : mIsNull(false),
    mText(text),
    mEncoding(encoding),
    mLanguage(language),
    mIsFlowed(isFlowed)
{
}

/**
 * Destructor.
 */
mimepp::email::Text::~Text()
{
}

/**
 * Assignment operator.
 */
Text& mimepp::email::Text::operator = (const Text& other)
{
    if (this != &other) {
        mIsNull   = other.mIsNull;
        mText     = other.mText;
        mEncoding = other.mEncoding;
        mLanguage = other.mLanguage;
        mIsFlowed = other.mIsFlowed;
    }
    return *this;
}

void mimepp::email::Text::clear()
{
    mIsNull = true;
    mText = "";
    mEncoding = "";
    mLanguage = "";
    mIsFlowed = false;
}

/**
 * Sets the text, preferred external encoding identifier, and language
 * identifier.
 *
 * The character encoding identifier should be one that is registered with
 * IANA.  Examples: "UTF-8", "ISO-8859-1", "Shift_JIS".
 *
 * The language identifier should be one that is specified in ISO&nbsp;639
 * (see also RFC&nbsp;3066).  Examples: "en", "jp", "de".
 *
 * @param text the text value of the string (UTF-8 encoding)
 * @param encoding the external character encoding identifier
 * @param language the two character language identifier
 * @param isFlowed true if the plain text is flowed
 */
void mimepp::email::Text::set(const string& text, const string& encoding,
    const string& language, bool isFlowed)
{
    mIsNull = false;
    mText = text;
    mEncoding = encoding;
    mLanguage = language;
    mIsFlowed = isFlowed;
}

void mimepp::email::Text::fromExternal(const string& encodedText,
    const string& encoding, const string& language, bool isFlowed)
{
    TextDecoder* decoder = TextDecoder::create(encoding.c_str());
    if (decoder) {
        int utf8MaxLen = 3 * (int)(encodedText.length() + 1);
        char* utf8 = (char*) malloc(utf8MaxLen);
        if (utf8) {
            size_t utf8Len = decoder->getChars(encodedText.data(),
                (int) encodedText.length(), utf8, utf8MaxLen);
            mIsNull = false;
            mEncoding = encoding;
            mLanguage = language;
            mIsFlowed = isFlowed;
            mText.assign(utf8, utf8Len);
            free(utf8);
            utf8 = 0;
        }
        delete decoder;
        decoder = 0;
    }
}

string mimepp::email::Text::toExternal()
{
    string ret;
    TextEncoder* encoder = TextEncoder::create(mEncoding.c_str());
    if (encoder) {
        int encMaxLen =
            encoder->getBytesLen(mText.data(), (int) mText.length()) + 1;
        char* enc = (char*) malloc(encMaxLen);
        if (enc) {
            int encLen = encoder->getBytes(mText.data(), (int)mText.length(),
                enc, encMaxLen);
            ret.assign(enc, encLen);
            free(enc);
            enc = 0;
        }
        delete encoder;
        encoder = 0;
    }
    return ret;
}

bool mimepp::email::Text::isNull() const
{
    return mIsNull;
}

/**
 * Gets the text (UTF-8 encoding).
 */
const string& mimepp::email::Text::text() const
{
    return mText;
}

/**
 * Gets the character encoding.
 */
const string& mimepp::email::Text::encoding() const
{
    return mEncoding;
}

/**
 * Gets the language.
 */
const string& mimepp::email::Text::language() const
{
    return mLanguage;
}

bool mimepp::email::Text::isFlowed() const
{
    return mIsFlowed;
}
