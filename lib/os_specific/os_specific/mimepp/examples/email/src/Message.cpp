// Message.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include <algorithm>
#include <iostream>
#include <functional>
#include "email/email.h"
using namespace mimepp;
using namespace mimepp::email;
using namespace std;

#if defined(_WIN32)
#  define strcasecmp stricmp
#  define strncasecmp strnicmp
#endif

static void getAttachments(Entity& entity, AttachmentList& attachments,
    bool strict, int level);
static void getAttachment(Entity& entity, Attachment& att);
static bool partContainsText(Entity& entity, int level);
static char selectEncoding(const mimepp::email::Text& text);

struct eq_nocase : public std::binary_function<char, char, bool> {
    bool operator()(char x, char y) const {
        return toupper(static_cast<unsigned char>(x)) ==
            toupper(static_cast<unsigned char>(y));
    }
};

// Compare strings, ignoring uppercase/lowercase differences
inline bool isEqualNoCase(const string& s, const string& t)
{
    return s.length() == t.length()
        && equal(s.begin(), s.end(), t.begin(), eq_nocase());
}

/**
 * \class mimepp::email::Message
 *
 * <b>%Message</b> represents an Internet email message.
 *
 * <b>%Message</b>, the main class of the mimepp-email library, is based on
 * a popular model for email messages: the message is assumed to contain
 * basic header fields, memo text, and zero or more file attachments.  The
 * basic header fields include "From", "To", "Cc", "Bcc", "Date", and
 * "Subject".  The memo text is the basic text of the message, which is
 * displayed in the viewer window of an email client application.
 *
 * The main value of this class is to interpret an arbitrary email message
 * according to this model.  That task involves looking at the parts of a
 * multipart message and deciding which part contains the memo text and
 * which parts contain file attachments.
 *
 * While this class is simpler to use than the more basic Hunny MIME++
 * classes, you do give up some flexibility by using it.  In particular,
 * the mimepp::email::Message class is not good for making modifications to
 * an existing message, because some of the information in the original
 * message will be lost when a mimepp::email::Message object is serialized
 * into a buffer.  However, if your goal is to create a simple message with
 * file attachments, then mimepp::email::Message is an ideal solution.  Or,
 * if your goal is to extract basic information from a message -- if you
 * were creating a simple mail viewer, for example -- then you would also
 * find mimepp::email::Message very useful.
 *
 * Using <b>%Message</b> is simple.  If you need to parse an email message,
 * then you create a <b>%Message</b> object, call the parseFrom() member
 * function to parse the message, and call other member functions to get
 * the properties from the email message.  If you need to create a new
 * email message, then you create a <b>%Message</b> object, set the values
 * of various properties, and call the serializeTo() member function to
 * write the message to a buffer.
 *
 * <b>%Message</b> provides member functions to get or set various
 * properties of an email message.
 *
 * <b>%Message</b> supports messages that contain the memo text as plain
 * text, HTML text, or both.  The presence of both plain text and HTML text
 * is possible with the multipart/alternative MIME type.  You don't need to
 * concern yourself with the MIME type, though, because <b>%Message</b>
 * takes care of this for you.  If you parse a message, then the
 * <b>%Message</b> object contains a memo in plain text, in HTML text, or
 * both, according to what was in the message that you parsed.  Similarly,
 * if you serialize a message, then the serialized message contains a memo
 * in plain text, HTML text, or both, depending on what the <b>%Message</b>
 * object contained.
 */

/**
 * Default constructor.
 */
mimepp::email::Message::Message()
{
}

/**
 * Destructor.
 */
mimepp::email::Message::~Message()
{
}

/**
 * Sets the plain version of the memo text.
 */
void mimepp::email::Message::setMemoTextFromPlainText(
    const mimepp::email::Text& text)
{
    mPlainText = text;
}

/**
 * Sets the HTML version of the memo text.
 */
void mimepp::email::Message::setMemoTextFromHtmlText(
    const mimepp::email::HtmlText& text)
{
    mHtmlText = text;
}

/**
 * Parses a message from a string.
 *
 * @param messageString the string containing the message
 */
void mimepp::email::Message::parseFrom(const String& messageString)
{
    mimepp::Message message(messageString);
    message.parse();
    _parse(message);
}

/**
 * Serializes a message to a string.
 *
 * When the member function returns, the serialized message is in
 * messageString.
 *
 * @param messageString the string to serialize the message to
 */
void mimepp::email::Message::serializeTo(mimepp::String& messageString)
{
    mimepp::Message message;
    Headers& headers = message.headers();
    Body& body = message.body();

    // MIME-Version

    headers.fieldBody("MIME-Version").setText(String("1.0"));

    // From

    Mailbox* mailbox = new Mailbox();
    headers.from().addMailbox(mailbox);
    mailbox->setString(mimepp::String(mOriginator.mailboxName().c_str()));
    mailbox->parse();
    String displayName = mOriginator.displayName().c_str();
    String charset = mOriginator.charset().c_str();
    if (displayName.length() > 0) {
        if (charset.length() > 0) {
            mailbox->setDisplayNameUtf8(displayName, charset);
        }
        else { // if charset is ""
            mailbox->setDisplayNameUtf8(displayName);
        }
    }
    mailbox = 0;

    // To

    unsigned i;
    for (i=0; i < mToRecipients.count(); ++i) {
        const mimepp::email::Address& addr = mToRecipients.get(i);
        mailbox = new Mailbox();
        headers.to().addAddress(mailbox);
        mailbox->setString(String(addr.mailboxName().c_str()));
        mailbox->parse();
        if (addr.displayName().length() > 0) {
            if (addr.charset().length() > 0) {
                mailbox->setDisplayNameUtf8(displayName,
                    addr.charset().c_str());
            }
            else {
                mailbox->setDisplayNameUtf8(addr.displayName().c_str());
            }
        }
    }

    // Cc

    for (i=0; i < mCcRecipients.count(); ++i) {
        const mimepp::email::Address& addr = mCcRecipients.get(i);
        mailbox = new Mailbox();
        headers.cc().addAddress(mailbox);
        mailbox->setString(String(addr.mailboxName().c_str()));
        mailbox->parse();
        if (addr.displayName().length() > 0) {
            if (addr.charset().length() > 0) {
                mailbox->setDisplayNameUtf8(displayName,
                    addr.charset().c_str());
            }
            else {
                mailbox->setDisplayNameUtf8(addr.displayName().c_str());
            }
        }
    }

    // Bcc

    for (i=0; i < mBccRecipients.count(); ++i) {
        const mimepp::email::Address& addr = mBccRecipients.get(i);
        mailbox = new Mailbox();
        headers.bcc().addAddress(mailbox);
        mailbox->setString(String(addr.mailboxName().c_str()));
        mailbox->parse();
        if (addr.displayName().length() > 0) {
            if (addr.charset().length() > 0) {
                mailbox->setDisplayNameUtf8(displayName,
                    addr.charset().c_str());
            }
            else {
                mailbox->setDisplayNameUtf8(addr.displayName().c_str());
            }
        }
    }

    // Subject

    headers.subject().setUtf8Text(mSubject.text().c_str(),
        mSubject.encoding().c_str());

    // Date

    if (! mDate.isNull()) {
        headers.date().setValuesLiteral(mDate.year(), mDate.month(),
            mDate.day(), mDate.hour(), mDate.minute(),
            mDate.second(), mDate.zone());
    }

    // Message-ID

    // Note: Message-ID is not necessary.  The first SMTP server will
    // add it.  For most applications, it's probably a good idea to let
    // the SMTP server add it, especially, if the server is set up as a
    // message submission agent.

    headers.messageId().createDefault();

    // Create a memo part, which combines the plain and HTML parts, if
    // necessary

    BodyPart memoPart;
    _createMemoPart(memoPart);

    // If there are no attachments

    if (mAttachments.count() == 0) {

        // Copy the header fields from the memo part to the message

        Headers phdrs = memoPart.headers();
        int numFields = phdrs.numFields();
        for (int j = 0; j < numFields; ++j) {
            Field* field = phdrs.removeFieldAt(0);
            headers.addField(field);
        }

        // Copy the body from the memo part to the message

        body.setString(memoPart.body().getString());
    }

    // If this is a multipart message...

    else {

        // Create a top-level multipart/mixed part

        headers.contentType().setType("multipart");
        headers.contentType().setSubtype("mixed");
        headers.contentType().createBoundary(0);

        // Add the memo part as the first part

        body.addBodyPart((BodyPart*)memoPart.clone());

        // Add the attachments as additional body parts

        unsigned n = mAttachments.count();
        for (i=0; i < n; ++i) {
            const Attachment& attach = mAttachments.get(i);
            BodyPart* part = new BodyPart;
            Headers& partHeaders = part->headers();
            partHeaders.contentTransferEncoding()
                .fromEnum(TransferEncodingType::BASE64);
            partHeaders.contentType().setType(attach.type().c_str());
            partHeaders.contentType().setSubtype(attach.subtype().c_str());
            partHeaders.contentType().setName(attach.fileName().c_str());
            partHeaders.contentDisposition()
                .fromEnum(DispositionType::ATTACHMENT);
            partHeaders.contentDisposition()
                .setFilename(attach.fileName().c_str());
            String content = Base64Encoder().encode(attach.content());
            part->body().setString(content);
            body.addBodyPart(part);
        }
    }

    message.assemble();
    messageString = message.getString();
}

void mimepp::email::Message::_parse(mimepp::Message& msg)
{
    Headers& headers = msg.headers();

    parseOriginator(msg, mOriginator);
    parseRecipients(msg, "to", mToRecipients);
    parseRecipients(msg, "cc", mCcRecipients);
    if (headers.hasField("Date")) {
        const DateTime& date = headers.date();
        int year = date.year();
        int month = date.month();
        int day = date.day();
        int hour = date.hour();
        int minute = date.minute();
        int second = date.second();
        int zone = date.zone();
        mDate.setValues(year, month, day, hour, minute, second, zone);
    }
    parseSubject(msg, mSubject);
    parseMemoText(msg, 0);
    parseAttachments(msg, mAttachments);
}

void mimepp::email::Message::_createMemoPart(BodyPart& memoPart)
{
    // If we have plain text, then create a text/plain part

    BodyPart plainPart;
    bool hasPlainPart = false;
    if (! mPlainText.isNull()) {
        hasPlainPart = true;
        Headers& phdrs = plainPart.headers();

        String text = mPlainText.text().c_str();
        String encoding = mPlainText.encoding().c_str();
        String language = mPlainText.language().c_str();

        // Content-type

        phdrs.contentType().setType("text");
        phdrs.contentType().setSubtype("plain");
        phdrs.contentType().setCharset(encoding);

        // Content-language

        if (language.length() > 0) {
            phdrs.fieldBody("Content-Language").setText(language);
        }

        // Memo text

        char xfer_encoding = selectEncoding(mPlainText);
        if (xfer_encoding == 'B') {
            phdrs.contentTransferEncoding().setType("base64");
            text = Base64Encoder().encode(text);
        }
        else if (xfer_encoding == 'Q') {
            phdrs.contentTransferEncoding().setType("quoted-printable");
            text = QuotedPrintableEncoder().encode(text);
        }
        plainPart.body().setString(text);
    }

    // If we have HTML text, then create a text/html part

    BodyPart htmlPart;
    bool hasHtmlPart = false;
    if (! mHtmlText.isNull()) {
        hasHtmlPart = true;
        Headers& phdrs = htmlPart.headers();

        String text = mHtmlText.text().c_str();
        String encoding = mHtmlText.encoding().c_str();
        String language = mHtmlText.language().c_str();

        // Content-type

        phdrs.contentType().setType("text");
        phdrs.contentType().setSubtype("html");
        phdrs.contentType().setCharset(encoding);

        // Content-language

        if (language.length() > 0) {
            phdrs.fieldBody("Content-Language").setText(language);
        }

        // Memo text

        char xfer_encoding = selectEncoding(mHtmlText);
        if (xfer_encoding == 'B') {
            phdrs.contentTransferEncoding().setType("base64");
            text = Base64Encoder().encode(text);
        }
        else if (xfer_encoding == 'Q') {
            phdrs.contentTransferEncoding().setType("quoted-printable");
            text = QuotedPrintableEncoder().encode(text);
        }
        htmlPart.body().setString(text);
    }

    // If we have both plain and HTML text, then combine them into a
    // multipart/alternative part.  Otherwise, the memo part is either
    // the plain part or the HTML part.

    if (hasPlainPart && ! hasHtmlPart) {
        // Plain text only
        memoPart = plainPart;
    }
    else if (! hasPlainPart && hasHtmlPart) {
        // HTML text only
        memoPart = htmlPart;
    }
    else if (hasPlainPart && hasHtmlPart) {
        // Plain and HTML text
        Headers& phdrs = memoPart.headers();
        Body& pbody = memoPart.body();
        phdrs.contentType().setType("multipart");
        phdrs.contentType().setSubtype("alternative");
        phdrs.contentType().createBoundary(0);
        pbody.addBodyPart((BodyPart*)plainPart.clone());
        pbody.addBodyPart((BodyPart*)htmlPart.clone());
    }
    else /* if (plainPart == null && htmlPart == null) */ {
        // Neither plain nor HTML text.  Just add a plain text part
        // with no content.
        Headers phdrs = memoPart.headers();
        Body pbody = memoPart.body();
        phdrs.contentType().setType("text");
        phdrs.contentType().setSubtype("plain");
    }

    // If we have embedded objects, then create a multipart/related
    // part
    if (! mHtmlText.isNull() && mHtmlText.count() > 0) {
        BodyPart* primaryPart = (BodyPart*) memoPart.clone();
        Headers& phdrs = memoPart.headers();
        Body& pbody = memoPart.body();
        // Remove pointers to the body parts, since they are now part of
        // primaryPart
        for (unsigned i=pbody.numBodyParts(); i > 0 ; --i) {
            pbody.removeBodyPartAt(i - 1);
        }
        pbody.addBodyPart(primaryPart);
        phdrs.contentType().setType("multipart");
        phdrs.contentType().setSubtype("related");
        phdrs.contentType().createBoundary(0);
        unsigned n = mHtmlText.count();
        for (unsigned i=0; i < n; ++i) {
            const EmbeddedObject& obj = mHtmlText.get(i);
            BodyPart* part = new BodyPart;
            pbody.addBodyPart(part);
            part->headers().contentType().setString(
                String(obj.mediaType().c_str()));
            part->headers().contentId().setString(
                String(obj.contentID().c_str()));
            part->headers().contentTransferEncoding().fromEnum(
                TransferEncodingType::BASE64);
            String content = Base64Encoder().encode(obj.content());
            part->body().setString(content);
        }
    }
    memoPart.assemble();
}


//--------------------------------------------------------------------------
// Virtual functions
//--------------------------------------------------------------------------


void mimepp::email::Message::parseOriginator(mimepp::Message& msg,
     mimepp::email::Address& addr)
{
    // Here we assume that there is only one author.  RFC 2822 allows
    // more than one individual to be specified as the author, but only
    // if there is also a "Sender" header field.
    //
    // We return the originator's mail address in the standard form.

    string mailboxName;
    string displayName;
    string charset = "US-ASCII";
    addr.set(mailboxName, displayName, charset);
    Headers& headers = msg.headers();
    if (headers.hasField("From")) {
        const MailboxList& fromList = headers.from();
        if (fromList.numMailboxes() > 0) {
            const Mailbox& from = fromList.mailboxAt(0);
            String xMailboxName = from.localPart();
            xMailboxName += "@";
            xMailboxName += from.domain();
            String xDisplayName = from.displayNameUtf8();
            String xCharset = from.charset();
            mailboxName = xMailboxName.c_str();
            displayName = xDisplayName.c_str();
            charset = xCharset.c_str();
            addr.set(mailboxName, displayName, charset);
        }
    }
}


void mimepp::email::Message::parseRecipients(mimepp::Message& aMessage,
    const char* which, mimepp::email::AddressList& aAddresses)
{
    Headers& headers = aMessage.headers();

    // Iterate through all header fields, and get the addresses from each
    // "To" field.  Note: It has been recommended in a recent Internet
    // draft that multiple "To" fields not be used.  However, the official
    // standard (RFC 2822) allows the use of multiple "To" fields, so we will
    // accommodate it.

    int numFields = headers.numFields();
    for (int i=0; i < numFields; ++i) {
        const Field& field = headers.fieldAt(i);
        if (Strcasecmp(which, field.fieldName()) == 0) {
            const mimepp::AddressList& rcptList = (const mimepp::AddressList &)
                field.fieldBody();
            int numAddresses = rcptList.numAddresses();
            for (int j=0; j < numAddresses; ++j) {
                const mimepp::Address& addr = rcptList.addressAt(j);
                if (addr.class_().isA(MAILBOX_CLASS)) {
                    const Mailbox& rcpt = (const Mailbox&) addr;
                    String inetName = rcpt.localPart();
                    inetName += "@";
                    inetName += rcpt.domain();
                    String displayName, charset;
                    displayName = rcpt.displayNameUtf8();
                    charset = rcpt.charset();
                    mimepp::email::Address eaddr(inetName.c_str(),
                        displayName.c_str(), charset.c_str());
                    aAddresses.add(eaddr);
                }
                else if (addr.class_().isA(GROUP_CLASS)) {
                    const Group& rcpt = (const Group&) addr;
                    mimepp::email::Address
                        eaddr("", rcpt.groupName().c_str(), "");
                    aAddresses.add(eaddr);
                }
            }
        }
    }
}


void mimepp::email::Message::parseSubject(mimepp::Message& aMessage,
    mimepp::email::Text& aSubject)
{
    Headers& headers = aMessage.headers();
    std::string text = headers.subject().utf8Text().c_str();
    std::string charset = headers.subject().charset().c_str();
    aSubject.set(text, charset, std::string(), false);
}


void mimepp::email::Message::parseMemoText(Entity& entity, int level)
{
    if (level == 0) {
        mPlainText.clear();
        mHtmlText.clear();
    }
    else if (level > 2) {
        // Fail -- we can't allow deeply nested content, because it may
        // allow denial-of-service attacks.
        string emptyStr;
        mPlainText.set(emptyStr, "US-ASCII", emptyStr, false);
        return;
    }
    ++level;

    // Get content type

    int type = MediaType::TEXT;
    String subtype;
    Headers& headers = entity.headers();
    if (headers.hasField("Content-Type")) {
        const MediaType& mtype = headers.contentType();
        type = mtype.typeAsEnum();
        subtype = mtype.subtype();
    }

    if (type == MediaType::TEXT) {
        // text/*
        parseMemoText_Text(entity);
    }
    else  if (type == MediaType::MULTIPART) {
        if (0 == Strcasecmp("alternative", subtype)) {
            // multipart/alternative
            parseMemoText_MultipartAlternative(entity, level);
        }
        else if (0 == Strcasecmp("related", subtype)) {
            // multipart/related
            parseMemoText_MultipartRelated(entity, level);
        }
        else {
            // Probably multipart/mixed.  But an unknown subtype should
            // be treated as "mixed".
            if (entity.body().numBodyParts() > 0) {
                BodyPart& part = entity.body().bodyPartAt(0);
                parseMemoText(part, level);
            }
        }
    }
    else {
        // No memo text -- just an attachment
        string emptyStr;
        mPlainText.set(emptyStr, "US-ASCII", emptyStr, false);
    }
}

void mimepp::email::Message::parseMemoText_Text(Entity& entity)
{
    Headers& headers = entity.headers();
    Body& body = entity.body();
    String text = body.getString();

    // Check content-transfer-encoding, and decode if necessary

    int cte = TransferEncodingType::_7BIT;
    if (headers.hasField("Content-Transfer-Encoding")) {
        cte = headers.contentTransferEncoding().asEnum();
    }
    if (cte == TransferEncodingType::QUOTED_PRINTABLE) {
        QuotedPrintableDecoder dec;
        text = dec.decode(text);
    }
    else if (cte == TransferEncodingType::BASE64) {
        Base64Decoder dec;
        text = dec.decode(text);
    }

    // Get the MIME subtype.
    // Get the character encoding and format, if they are there.

    String subtype = "plain";
    String charset = "US-ASCII";
    String format = "";
    if (headers.hasField("Content-Type")) {
        const MediaType& mtype = headers.contentType();
        subtype = mtype.subtype();
        int numParams = mtype.numParameters();
        for (int i=0; i < numParams; ++i) {
            const Parameter& param = headers.contentType().parameterAt(i);
            const String& name = param.name();
            if (0 == Strcasecmp("charset", name)) {
                charset = param.value();
            }
            else if (0 == Strcasecmp("format", name)) {
                format = param.value();
            }
        }
    }

    // Get the language, if it's there.  There's no default for the
    // language.

    String language = "";
    if (headers.hasField("Content-Lanaugage")) {
        const String& s = headers.fieldBody("Content-Language").text();
        int n = (int) s.find(',');
        if (n >= 0) {
            language = s.substr(0, n);
        }
    }

    // Check for HTML text

    if (0 == Strcasecmp("html", subtype)) {
        string s(text.data(), text.length());
        mHtmlText.set(s, charset.c_str(), language.c_str(), false);
    }

    // If it's not HTML, assume it's like plain text

    else {
        bool isFlowed = false;
        if (0 == Strcasecmp("flowed", format)) {
            isFlowed =true;
        }
        string s(text.data(), text.length());
        mPlainText.set(s, charset.c_str(), language.c_str(), isFlowed);
    }
}


void mimepp::email::Message::parseMemoText_MultipartAlternative(Entity& entity,
    int level)
{
    Body& body = entity.body();
    int numParts = body.numBodyParts();
    for (int i = 0; i < numParts; ++i) {
        BodyPart& part = body.bodyPartAt(i);
        Headers& phdrs = part.headers();
        const MediaType& mtype = phdrs.contentType();
        int type = mtype.typeAsEnum();
        if (type == MediaType::TEXT) {
            parseMemoText_Text(part);
        }
        else if (type == MediaType::MULTIPART) {
            parseMemoText(part, level);
        }
    }
}


void mimepp::email::Message::parseMemoText_MultipartRelated(Entity& entity,
    int level)
{
    Body& body = entity.body();
    int numParts = body.numBodyParts();
    // First part is the text
    int i = 0;
    if (i < numParts)  {
        BodyPart& part = body.bodyPartAt(i);
        Headers& phdrs = part.headers();
        MediaType& mtype = phdrs.contentType();
        int type = mtype.typeAsEnum();
        if (type == MediaType::TEXT) {
            parseMemoText_Text(part);
        }
        else if (type == MediaType::MULTIPART) {
            String subtype = mtype.subtype();
            if (0 == Strcasecmp("alternative", subtype)) {
                parseMemoText(part, level);
            }
        }
        else {
            // Error!
            return;
        }
        ++i;
    }
    // Following parts are embedded objects (usually images)
    if (! mHtmlText.isNull()) {
        for ( ; i < numParts; ++i) {
            BodyPart& part = body.bodyPartAt(i);
            Headers& phdrs = part.headers();
            Body& pbody = part.body();
            MediaType& mtype = phdrs.contentType();
            int type = mtype.typeAsEnum();
            if (type == MediaType::APPLICATION
                || type == MediaType::AUDIO
                || type == MediaType::IMAGE
                || type == MediaType::MODEL
                || type == MediaType::TEXT
                || type == MediaType::VIDEO) {
                // Remove any transfer encoding
                String content = pbody.getString();
                if (phdrs.hasField("Content-Transfer-Encoding")) {
                    int cte = phdrs.contentTransferEncoding().asEnum();
                    if (cte == TransferEncodingType::BASE64) {
                        content = Base64Decoder().decode(content);
                    }
                    else if (cte == TransferEncodingType::QUOTED_PRINTABLE){
                        content = QuotedPrintableDecoder().decode(content);
                    }
                }
                // Get the Content ID
                String id;
                if (phdrs.hasField("Content-Id")) {
                    id = phdrs.contentId().getString();
                    if (id.find('<') == 0) {
                        id = id.substr(1);
                    }
                    if (id.rfind('>') == id.length() - 1) {
                        id = id.substr(0, id.length()-1);
                    }
                }
                // Get the media type
                String mediaType;
                if (phdrs.hasField("Content-Type")) {
                    mediaType = mtype.type()+"/" + mtype.subtype();
                }
                if (id.length() > 0) {
                    EmbeddedObject obj;
                    obj.set(id.c_str(), mediaType.c_str(), content);
                    mHtmlText.add(obj);
                }
            }
        }
    }
    else {
        // Error!
    }
}

void mimepp::email::Message::parseAttachments(mimepp::Message& msg,
    AttachmentList& attachments)
{
    int type = MediaType::TEXT;
    String subtype = "plain";
    Headers& headers = msg.headers();
    if (headers.hasField("Content-Type")) {
        type = headers.contentType().typeAsEnum();
        subtype = headers.contentType().subtype();
    }
    if (type == MediaType::TEXT) {
        // If the message type is text, then there are no attachments
        //attachments.clear();
    }
    else if (type == MediaType::MESSAGE) {
        parseAttachments_Message(msg, attachments);
    }
    else if (type == MediaType::MULTIPART) {
        if (0 == Strcasecmp("alternative", subtype)
            || 0 == Strcasecmp("related", subtype)) {
        }
        else {
            parseAttachments_Multipart(msg, attachments);
        }
    }
    else {
        parseAttachments_Simple(msg, attachments);
    }
}


void mimepp::email::Message::parseAttachments_Simple(mimepp::Message& msg,
    AttachmentList& attachments)
{
    Attachment att;
    getAttachment(msg, att);
    attachments.add(att);
}


void mimepp::email::Message::parseAttachments_Message(mimepp::Message& msg,
    AttachmentList& attachments)
{
    Headers& headers = msg.headers();
    Body& body = msg.body();
    mimepp::Message* message = body.message();
    Attachment att;
    if (message != 0) {
        String type = msg.headers().contentType().type();
        String subtype = msg.headers().contentType().subtype();
        att.mediaType(type.c_str(), subtype.c_str());
        att.content(message->getString());
    }
    else {
        String type = headers.contentType().type();
        String subtype = headers.contentType().subtype();
        att.mediaType(type.c_str(), subtype.c_str());
        att.content(body.getString());
    }
    attachments.add(att);
}


void mimepp::email::Message::parseAttachments_Multipart(mimepp::Message& msg,
    AttachmentList& attachments)
{
    Body& body = msg.body();
    int numParts = body.numBodyParts();
    // The first body part is treated specially, since it normally
    // contains the memo text.
    if (numParts > 0) {
        BodyPart& part = body.bodyPartAt(0);
        // If the first body part contains text, then get only the
        // body parts that have disposition "attachment."
        bool strict = true;
        if (! partContainsText(part, 0)) {
            strict = false;
        }
        getAttachments(part, attachments, strict, 0);
    }
    // All body parts after the first body part are treated as
    // attachments
    for (int i=1; i < numParts; ++i) {
        BodyPart& part = body.bodyPartAt(i);
        getAttachments(part, attachments, false, 0);
    }
}


static void getAttachments(Entity& entity, AttachmentList& attachments,
    bool strict, int level)
{
    if (level >= 10) {
        return;
    }
    ++level;
    int type = MediaType::TEXT;
    Headers& headers = entity.headers();
    if (headers.hasField("Content-Type")) {
        type = headers.contentType().typeAsEnum();
    }
    if (type == MediaType::MULTIPART) {
        getAttachments(entity, attachments, strict, level);
    }
    else {
        // If strict is true, then save this part as an attachment only
        // if it has an explicit disposition of "attachment."
        bool save = true;
        if (strict) {
            save = false;
            if (headers.hasField("Content-Disposition")) {
                int dtype = headers.contentDisposition().asEnum();
                if (dtype == DispositionType::ATTACHMENT) {
                    save = true;
                }
            }
        }
        if (save) {
            Attachment att;
            getAttachment(entity, att);
            attachments.add(att);
        }
    }
}

static void getAttachment(Entity& entity, Attachment& att)
{
    Headers& headers = entity.headers();
    Body& body = entity.body();
    // Get the media type
    String type = headers.contentType().type();
    String subtype = headers.contentType().subtype();
    // Get the filename.  Try the disposition-type field first, then
    // try the content-type field.
    String filename = "";
    if (headers.hasField("Content-Disposition")) {
        DispositionType& dtype = headers.contentDisposition();
        int numParameters = dtype.numParameters();
        for (int i=0; i < numParameters; ++i) {
            Parameter& param = dtype.parameterAt(i);
            if (0 == Strcasecmp("filename", param.name())) {
                filename = param.value();
                break;
            }
        }
    }
    if (filename.length() == 0) {
        MediaType& mtype = headers.contentType();
        int numParameters = mtype.numParameters();
        for (int i=0; i < numParameters; ++i) {
            Parameter& param = mtype.parameterAt(i);
            if (0 == Strcasecmp("name", param.name())) {
                filename = param.value();
                break;
            }
        }
    }
    // FIXME get description
    String description = "";
    // Get the content.  Decode it if necessary.
    String content = body.getString();
    if (headers.hasField("Content-Transfer-Encoding")) {
        int cte = headers.contentTransferEncoding().asEnum();
        if (cte == TransferEncodingType::BASE64) {
            content = Base64Decoder().decode(content);
        }
        else if (cte == TransferEncodingType::QUOTED_PRINTABLE) {
            content = QuotedPrintableDecoder().decode(content);
        }
    }
    att.mediaType(type.c_str(), subtype.c_str());
    att.fileName(filename.c_str());
    att.description(description.c_str());
    att.content(content);
}

static bool partContainsText(Entity& entity, int level)
{
    bool containsText = false;
    if (level > 4) {
        return containsText;
    }
    ++level;

    String type = "text";
    String subtype = "plain";
    Headers& headers = entity.headers();
    Body& body = entity.body();
    if (headers.hasField("Content-Type")) {
        type = headers.contentType().type();
        subtype = headers.contentType().subtype();
    }

    int i, numParts;
    if (0 == Strcasecmp("text", type)) {
        containsText = true;
    }
    else if (0 == Strcasecmp("multipart", type)) {
        numParts = body.numBodyParts();
        for (i=0; i < numParts; ++i) {
            BodyPart& part = body.bodyPartAt(i);
            if (partContainsText(part, level)) {
                containsText = true;
                break;
            }
        }
    }
    else {
    }
    return containsText;
}

static char selectEncoding(const mimepp::email::Text& text)
{
    // To do: add the Windows code pages charsets, such as 1252

    char xencoding = 0;
    string cencoding = text.encoding();
    if (isEqualNoCase(cencoding, "US-ASCII")
        || isEqualNoCase(cencoding, "ISO-8859-1")
        || isEqualNoCase(cencoding, "ISO-8859-2")
        || isEqualNoCase(cencoding, "ISO-8859-3")
        || isEqualNoCase(cencoding, "ISO-8859-4")
        || isEqualNoCase(cencoding, "ISO-8859-9")
        || isEqualNoCase(cencoding, "UTF-8")) {
        xencoding = 'Q';
    }
    else {
        xencoding = 'B';
    }
    return xencoding;
}
