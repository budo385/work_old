// Date.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include <time.h>
#include "email/email.h"
using namespace mimepp::email;
using std::string;

/**
 * \class mimepp::email::Date
 *
 * <b>%Date</b> represents a combined date and time value.
 *
 * This class is really just a structured data type -- that is, it doesn't
 * do anything except act as a container for data.  The fields that are
 * present are the usual items of a date/time, including year, month, day,
 * hour, minute, second, and zone offset.  The constraints on the fields
 * are listed here:
 *
 * 1900 <= year<br>
 * 1 <= month <= 12<br>
 * 1 <= day <= 31<br>
 * 0 <= hour <= 23<br>
 * 0 <= minute <= 59<br>
 * 0 <= second <= 59<br>
 * -720 <= zone <= 720
 *
 * Zone is the offset from GMT in minutes.  For example, the offset for
 * U.S. Eastern Standard Time (New York) is -300.
 */

/**
 * Default constructor.
 */
mimepp::email::Date::Date()
{
    mIsNull = true;
    mYear = 1970;
    mMonth = 1;
    mDay = 1;
    mHour = 0;
    mMinute = 0;
    mSecond = 0;
    mZone = 0;
}

/**
 * Copy constructor.
 */
mimepp::email::Date::Date(const Date& other)
{
    mIsNull = other.mIsNull;
    mYear = other.mYear;
    mMonth = other.mMonth;
    mDay = other.mDay;
    mHour = other.mHour;
    mMinute = other.mMinute;
    mSecond = other.mSecond;
    mZone = other.mZone;
}

/**
 * Destructor.
 */
mimepp::email::Date::~Date()
{
}

/**
 * Assignment operator.
 */
Date& mimepp::email::Date::operator = (const Date& other)
{
    if (this != &other) {
        mIsNull = other.mIsNull;
        mYear = other.mYear;
        mMonth = other.mMonth;
        mDay = other.mDay;
        mHour = other.mHour;
        mMinute = other.mMinute;
        mSecond = other.mSecond;
        mZone = other.mZone;
    }
    return *this;
}

/**
 * Sets the date and time value.
 *
 * This function sets the date and time value as a local time.  It
 * determines the time zone from the operating system.  To set the date
 * and time value to a non-local time zone, call the member function
 * setValues(int,int,int,int,int,int,int).
 *
 * @param year the year (1900 &lt;= year &lt;= 2100)
 * @param month the month (1 &lt;= <i>month</i> &lt;= 12)
 * @param day the day of the month (1 &lt;= <i>day</i> &lt;= 31)
 * @param hour the hour (0 &lt;= <i>hour</i> &lt;= 23)
 * @param minute the minute (0 &lt;= minute &lt;= 59)
 * @param second the second (0 &lt;= second &lt;= 59)
 * @see setValues(int,int,int,int,int,int,int).
 */
void mimepp::email::Date::setValues(int year, int month, int day, int hour,
    int minute, int second)
{
    // The DateTime class has code to figure out the time zone.
    // We can reuse that code.

    mimepp::DateTime date;
    date.setValuesLocal(year, month, day, hour, minute, second);

    mIsNull = false;
    mYear = date.year();
    mMonth = date.month();
    mDay = date.day();
    mHour = date.hour();
    mMinute = date.minute();
    mSecond = date.second();
    mZone = date.zone();
}

/**
 * Sets the date and time value.
 *
 * This function sets the date and time value with an explicit time zone.
 * To set the date and time value as a local time, call the member function
 * setValues(int,int,int,int,int,int).
 *
 * The <i>zone</i> parameter specifies the difference in minutes between
 * local time and GMT time.  For example, for standard time in New York
 * City, the value of <i>zone</i> should be -300.
 *
 * @param year the year (1900 &lt;= year &lt;= 2100)
 * @param month the month (1 &lt;= <i>month</i> &lt;= 12)
 * @param day the day of the month (1 &lt;= <i>day</i> &lt;= 31)
 * @param hour the hour (0 &lt;= <i>hour</i> &lt;= 23)
 * @param minute the minute (0 &lt;= minute &lt;= 59)
 * @param second the second (0 &lt;= second &lt;= 59)
 * @param zone the time zone offset in minutes (-720 &lt;= <i>zone</i> &lt;=
 * 720)
 * @see setValues(int,int,int,int,int,int).
 */
void mimepp::email::Date::setValues(int year, int month, int day, int hour,
    int minute, int second, int zone)
{
    mIsNull = false;
    mYear = year;
    mMonth = month;
    mDay = day;
    mHour = hour;
    mMinute = minute;
    mSecond = second;
    mZone = zone;
}

/**
 * Sets the value to the current date and time.
 */
void mimepp::email::Date::setCurrent()
{
    // The DateTime class has code to figure out the time zone.
    // We can reuse that code.

    mimepp::DateTime date;
    date.fromCalendarTime(time(0));

    mIsNull = false;
    mYear = date.year();
    mMonth = date.month();
    mDay = date.day();
    mHour = date.hour();
    mMinute = date.minute();
    mSecond = date.second();
    mZone = date.zone();
}

/**
 * Gets a string representation of the date and time.
 *
 * @return a string suitable for displaying the date and time in a user
 * interface
 */
const string& mimepp::email::Date::displayString() const
{
    char scratch[100];
    struct tm stime;

    stime.tm_year = mYear - 1900;
    stime.tm_mon = mMonth - 1;
    stime.tm_mday = mDay;
    stime.tm_hour = mHour;
    stime.tm_min = mMinute;
    stime.tm_sec = mSecond;
    mktime(&stime);
    //strftime(scratch, sizeof(scratch), "%c", &stime);
    strftime(scratch, sizeof(scratch), "%a, %d %b %Y %H:%M:%S", &stime);
    mString = scratch;
    return mString;
}
