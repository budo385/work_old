// Attachment.cpp

#if defined(_WIN32)
#  define EMAIL_API __declspec(dllexport)
#endif

#pragma warning(disable: 4251)

#include "email/email.h"
using namespace mimepp::email;
using std::string;


/**
 * \class mimepp::email::Attachment
 *
 * <b>%Attachment</b> represents a file attachment to an Internet email
 * message.
 *
 * An Internet email message may contain zero or more file attachments.
 * The <i>content</i> of an attachment is a sequence of bytes.  Most
 * attachments also have a <i>MIME type</i>, which indicates the format of
 * the content.  Typical MIME types include "image/jpeg",
 * "application/msword", "application/pdf", "application/x-zip", and so
 * forth.  Other attributes of an attachment include:
 *
 *   - <i>size</i> -- the size of the attachment (number of bytes)
 *   - <i>charset</i> -- the character encoding of a text attachment
 *   - <i>file name</i> -- the file name of the attachment
 *   - <i>description</i> -- a description of the content (very often omitted)
 *
 * <b>%Attachment</b> has member functions to get and set the attachment
 * content and attributes.
 *
 * All attributes of the attachment are optional.
 */

/**
 * Default constructor.
 */
mimepp::email::Attachment::Attachment()
{
}

/**
 * Copy constructor.
 */
mimepp::email::Attachment::Attachment(const Attachment& other)
  : mType(other.mType),
    mSubtype(other.mSubtype),
    mFileName(other.mFileName),
    mDescription(other.mDescription),
    mContent(other.mContent)
{
}

/**
 * Destructor.
 */
mimepp::email::Attachment::~Attachment()
{
}

/**
 * Assignment operator.
 */
Attachment& mimepp::email::Attachment::operator=(const Attachment& other)
{
    if (this != &other) {
        mFileName    = other.mFileName;
        mType        = other.mType;
        mSubtype     = other.mSubtype;
        mDescription = other.mDescription;
        mContent     = other.mContent;
    }
    return *this;
}

/**
 * Gets the primary type of the attachment's MIME type.
 *
 * Every MIME type consists of a primary type and a secondary type.
 * For example, the MIME type "application/x-zip" has the primary type
 * "application" and the secondary type "x-zip".  This member function
 * returns the primary type.  Use subtype() to get the secondary type.
 *
 * @return primary MIME type
 * @see subtype()
 */
const string& mimepp::email::Attachment::type() const
{
    return mType;
}

/**
 * Gets the secondary type of the attachment's MIME type.
 *
 * Every MIME type consists of a primary type and a secondary type.
 * For example, the MIME type "application/x-zip" has the primary type
 * "application" and the secondary type "x-zip".  This member function
 * returns the secondary type.  Use type() to get the primary type.
 *
 * @return secondary MIME type
 * @see type()
 */ 
const string& mimepp::email::Attachment::subtype() const
{
    return mSubtype;
}

/**
 * Sets the attachment's MIME type.
 *
 * The MIME type indicates the type of the content.  Typical MIME types
 * include "image/jpeg", "application/msword", "application/pdf",
 * "application/x-zip", and so forth.
 *
 * A MIME type consists of a primary type and a secondary type.  The part
 * before the solidus character ("/") is the primary type.  The part after
 * the solidus character is the secondary type.  Therefore, if the MIME
 * type is "image/jpeg", the primary type is "image" and the secondary
 * type is "jpeg".
 *
 * @param type primary MIME type
 * @param subtype secondary MIME type
 */
void mimepp::email::Attachment::mediaType(const string& type,
    const string& subtype)
{
    mType = type;
    mSubtype = subtype;
}

/**
 * Gets the charset of the attachment, if available.
 *
 * If the attachment contains text, then the attachment's MIME headers may
 * contain an identifier for the character encoding of the text, which is
 * often referred to as a <em>charset</em>.  This member function returns
 * the charset if it exists in the MIME headers.  If the charset does not
 * exist, then the function returns an empty string.
 *
 * If the attachment does not contain text, then the charset is normally
 * absent.
 *
 * Note: "charset" is a synonym for "character encoding identifier."
 *
 * @return charset of the attachment, or empty string
 */
const string& mimepp::email::Attachment::charset() const
{
    return mCharset;
}

/**
 * Sets the charset of the attachment.
 *
 * If the attachment contains text you should include an indication of the
 * character encoding of the text, especially if the text is not ASCII.
 * Call this member function to set the character encoding identifier.
 *
 * If the attachment does not contain text, you should omit the charset.
 *
 * Note: "charset" is a synonym for "character encoding identifier."
 *
 * @param charset character encoding identifer for the attachment
 */
void mimepp::email::Attachment::charset(const string& charset)
{
    mCharset = charset;
}

/**
 * Gets the size of the attachment in bytes.
 */
unsigned mimepp::email::Attachment::size() const
{
    return (unsigned) mContent.size();
}

/**
 * Gets the file name of the attachment, if available.
 *
 * In many cases, the MIME header fields of the attachment contain a
 * suggested file name.  If file name is present, this member function
 * returns it.  If the file name is absent, this function returns an empty
 * string.
 *
 * You must never trust the suggested file name.  Here are some steps that
 * you may want to take to make the file name safer:
 *
 *   - strip any leading path
 *   - replace any sequence of white space characters with a single space
 *     character.  (Note this unsafe file name:
 *     "file.txt<SP><SP><SP><SP><SP><SP><SP>.bat")
 *   - remove all characters except the characters you trust
 *
 * This library does not change the file name in any way.  Therefore, it is
 * your responsibility to handle the file name safely.
 *
 * @return the suggested name of the file
 */
const string& mimepp::email::Attachment::fileName() const
{
    return mFileName;
}

/**
 * Sets the file name of the attachment.
 *
 * @param name name of the file
 */
void mimepp::email::Attachment::fileName(const string& name)
{
    mFileName = name;
}

/**
 * Gets the description of the attachment.
 *
 * In some cases, the MIME header fields of the attachment contain a
 * description of the attachment.  If description is present, this member
 * function returns it.  If the description is absent, this function
 * returns an empty string.
 *
 * @return description of the attachment
 */
const string& mimepp::email::Attachment::description() const
{
    return mDescription;
}

/**
 * Sets the description of the attachment.
 *
 * Use this member function to set a short description of the attachment.
 * Example: "Jack Spratt's homework assignment"
 *
 * @param descr a short description of the attachment
 */
void mimepp::email::Attachment::description(const string& descr)
{
    mDescription = descr;
}

/**
 * Gets the content of the attachment.
 *
 * Use this member function to get the content of the attachment.  Note that
 * the content can be any sequence of bytes.
 *
 * @return the content of the attachment
 */
const mimepp::String& mimepp::email::Attachment::content() const
{
    return mContent;
}

/**
 * Sets the content of the attachment.
 *
 * Use this member function to set the content of the attachment.  Note that
 * the content can be any sequence of bytes.
 *
 * @param content the content of the attachment
 */
void mimepp::email::Attachment::content(const mimepp::String& content)
{
    mContent = content;
}
