// Address.h

#ifndef MIMEPP_EMAIL_ADDRESS_H
#define MIMEPP_EMAIL_ADDRESS_H

#include <string>


namespace mimepp {
namespace email {

    /// Internet email address
    class MIMEPP_EMAIL_API Address {

    public:

        /// Default constructor
        Address();

        /// Copy constructor
        Address(const Address& other);

        /// Constructor with initial values
        Address(const std::string& mailboxName, const std::string& displayName,
            const std::string& charset);

        /// Destructor
        virtual ~Address();

        /// Assignment operator
        Address& operator = (const Address& other);

        /// Sets the Address object's value
        void set(const std::string& mailboxName,
            const std::string& displayName, const std::string& charset);

        /// Gets the global mailbox name
        const std::string& mailboxName() const { return mMailboxName; }

        /// Gets the display name
        const std::string& displayName() const { return mDisplayName; }

        /// Gets the charset identifier for the display name
        const std::string& charset() const { return mCharset; }

    private:

        std::string mMailboxName;
        std::string mDisplayName;
        std::string mCharset;

    };

} // namespace email
} // namespace mimepp

#endif
