// EmbeddedObject.h

#ifndef MIMEPP_EMAIL_EMBEDDED_OBJECT_H
#define MIMEPP_EMAIL_EMBEDDED_OBJECT_H

#include <string>


namespace mimepp {
namespace email {

    /// Represents and embedded image or other object.

    class MIMEPP_EMAIL_API EmbeddedObject {

    public:

        /// Default constructor
        EmbeddedObject();

        /// Copy constructor
        EmbeddedObject(const EmbeddedObject& other);

        /// Constructor with initial values
        EmbeddedObject(const std::string& mContentID,
            const std::string& mediaType, const mimepp::String& content);

        /// Destructor
        virtual ~EmbeddedObject();

        /// Assignment operator
        EmbeddedObject& operator = (const EmbeddedObject& other);

        /// Sets the EmbeddedObject's value
        void set(const std::string& mContentID,
            const std::string& mediaType, const mimepp::String& content);

        /// Gets the content ID
        const std::string& contentID() const { return mContentID; }

        /// Gets the media type
        const std::string& mediaType() const { return mMediaType; }

        /// Gets the content
        const mimepp::String& content() const { return mContent; }

        std::string makeFileName() const;

    private:

        std::string mContentID;
        std::string mMediaType;
        mimepp::String mContent;
    };

} // namespace email
} // namespace mimepp

#endif
