// Message.h

#ifndef MIMEPP_EMAIL_MESSAGE_H
#define MIMEPP_EMAIL_MESSAGE_H


namespace mimepp {
namespace email {

    /// Internet email message
    class MIMEPP_EMAIL_API Message {

    public:

        /// Default constructor
        Message();

        /// Destructor
        virtual ~Message();

        /// Parses a message from a string
        void parseFrom(const mimepp::String& messageString);

        /// Serializes a message to a string
        void serializeTo(mimepp::String& messageString);

        /// Gets the email address of the originator of the message
        const Address& originator() const { return mOriginator; }

        /// Sets the email address of the originator of the message
        void originator(const Address& addr) { mOriginator = addr; }

        /// Gets the list of "To" recipients
        mimepp::email::AddressList& toRecipients() { return mToRecipients; }

        /// Gets the list of "Cc" recipients
        mimepp::email::AddressList& ccRecipients() { return mCcRecipients; }

        /// Gets the list of "Bcc" recipients
        mimepp::email::AddressList& bccRecipients() { return mBccRecipients; }

        /// Gets the subject of the message
        const mimepp::email::Text& subject() const { return mSubject; }

        /// Sets the subject of the message
        void subject(const mimepp::email::Text& str) { mSubject = str; }

        /// Gets the date the message was written
        const mimepp::email::Date& date() const { return mDate; }

        /// Sets the date the message was written
        void date(const mimepp::email::Date& dt) { mDate = dt; }

        /// Gets the plain version of the memo text.
        const mimepp::email::Text& memoTextAsPlainText() const { return mPlainText; }

        /// Gets the HTML version of the memo text.
        const mimepp::email::HtmlText& memoTextAsHtmlText() const { return mHtmlText; }

        /// Sets the plain version of the memo text.
        void setMemoTextFromPlainText(const mimepp::email::Text& text);

        /// Sets the HTML version of the memo text.
        void setMemoTextFromHtmlText(const mimepp::email::HtmlText& text);

        /// Gets the list of file attachments
        mimepp::email::AttachmentList& attachments() { return mAttachments; }

    protected:

        mimepp::email::Date mDate;
        mimepp::email::Address mOriginator;
        mimepp::email::AddressList mToRecipients;
        mimepp::email::AddressList mCcRecipients;
        mimepp::email::AddressList mBccRecipients;
        mimepp::email::Text mSubject;
        mimepp::email::Text mPlainText;
        mimepp::email::HtmlText mHtmlText;
        mimepp::email::AttachmentList mAttachments;

        void _parse(mimepp::Message& message);
        void _createMemoPart(BodyPart& memoPart);
        virtual void parseOriginator(mimepp::Message& message,
            mimepp::email::Address& address);
        virtual void parseRecipients(mimepp::Message& message,
            const char* which, mimepp::email::AddressList& addresses);
        virtual void parseSubject(mimepp::Message& aMessage,
            mimepp::email::Text& aSubject);
        virtual void parseMemoText(mimepp::Entity& entity, int level);
        virtual void parseMemoText_Text(mimepp::Entity& entity);
        virtual void parseMemoText_MultipartAlternative(
            mimepp::Entity& entity, int level);
        virtual void parseMemoText_MultipartRelated(
            mimepp::Entity& entity, int level);

        virtual void parseAttachments(mimepp::Message& msg,
            AttachmentList& attachments);
        virtual void parseAttachments_Simple(mimepp::Message& msg,
            mimepp::email::AttachmentList& attachments);
        virtual void parseAttachments_Message(mimepp::Message& msg,
            mimepp::email::AttachmentList& attachments);
        virtual void parseAttachments_Multipart(mimepp::Message& msg,
            mimepp::email::AttachmentList& attachments);

    };

} // namespace email
} // namespace mimepp

#endif
