#ifndef WINCE

#include "contraststyle.h"
#include <QtGui>

ContrastStyle::ContrastStyle()
{
}

ContrastStyle::~ContrastStyle()
{
}

void ContrastStyle::polish(QPalette &palette)
{
	//BASE_STYLE::polish(palette);

    QBrush brush = palette.background();
    brush.setColor(brush.color().black());
    palette.setBrush(QPalette::Disabled, QPalette::Text, brush);
	/*
	palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
	palette.setBrush(QPalette::Disabled, QPalette::Foreground, brush);
	palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
	palette.setBrush(QPalette::Disabled, QPalette::Mid, brush);
	*/
}

int ContrastStyle::styleHint(StyleHint hint, const QStyleOption *option,
                                  const QWidget *widget,
                                  QStyleHintReturn *returnData) const
{
    switch (hint) {
    case SH_DitherDisabledText:
        return int(false);
    case SH_EtchDisabledText:
        return int(true);
    default:
        return QProxyStyle::styleHint(hint, option, widget, returnData);
    }
}

#endif // WINCE
