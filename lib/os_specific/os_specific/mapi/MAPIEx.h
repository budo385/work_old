#ifndef __MAPIEX_H__
#define __MAPIEX_H__

#define NOMINMAX
#ifdef _WIN32

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File: MAPIEx.h
// Description: Windows Extended MAPI class 
//				Works in Windows CE and Windows XP with Outlook 2000 and 2003, maybe others but untested
//
// Copyright (C) 2005-2006, Noel Dillabough
//
// This source code is free to use and modify provided this notice remains intact and that any enhancements
// or bug fixes are posted to the CodeProject page hosting this class for all to benefit.
//
// Usage: see the Codeproject article at http://www.codeproject.com/internet/CMapiEx.asp
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _WIN32_WCE
#include <cemapi.h>
#include <objidl.h>
#else
#include <mapix.h>
#include <objbase.h>
#endif

#define RELEASE(s) if(s!=NULL) { s->Release();s=NULL; }

#include "MAPIMessage.h"
#include "MAPIContact.h"
#include "MAPIAppointment.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CMAPIEx

class CMAPIEx 
{
	friend class CMAPIMessage;

public:
	CMAPIEx();
	~CMAPIEx();

	enum { PROP_MESSAGE_FLAGS, PROP_ENTRYID, MESSAGE_COLS };

// Attributes
public:
	static int cm_nMAPICode;

protected:
	IMAPISession* m_pSession;
	LPMDB m_pMsgStore;
	LPMAPIFOLDER m_pFolder;
	LPMAPITABLE m_pHierarchy;
	LPMAPITABLE m_pContents;
	ULONG m_sink;

// Operations
public:
	static BOOL Init(BOOL bMultiThreadedNotifcations=TRUE);
	static void Term();

	IMAPISession* GetSession() { return m_pSession; }
	LPMDB GetMessageStore() { return m_pMsgStore; }
	LPMAPIFOLDER GetFolder() { return m_pFolder; }

	BOOL Login(LPCTSTR szProfileName=NULL);
	void Logout();

	QString GetProfileName();

	BOOL OpenMessageStore(LPCTSTR szStore=NULL);
	ULONG GetMessageStoreSupport();
	int  GetMessageStoreCount();
	BOOL OpenMessageStore(int nStoreIndex, QString &strStoreName);

	// use bInternal to specify that MAPIEx keeps track of and subsequently RELEASEs the folder 
	// remember to eventually RELEASE returned folders if calling with bInternal=FALSE
	LPMAPIFOLDER OpenFolder(unsigned long ulFolderID,BOOL bInternal);
	LPMAPIFOLDER OpenSpecialFolder(unsigned long ulFolderID,BOOL bInternal);
	LPMAPIFOLDER OpenRootFolder(BOOL bInternal=TRUE);
	LPMAPIFOLDER OpenContacts(BOOL bInternal=TRUE);
	LPMAPIFOLDER OpenInbox(BOOL bInternal=TRUE);
	LPMAPIFOLDER OpenOutbox(BOOL bInternal=TRUE);
	LPMAPIFOLDER OpenSentItems(BOOL bInternal=TRUE);
	LPMAPIFOLDER OpenDeletedItems(BOOL bInternal=TRUE);
	LPMAPIFOLDER OpenCalendar(BOOL bInternal=TRUE);

	LPMAPITABLE GetHierarchy(LPMAPIFOLDER pFolder=NULL);
	LPMAPIFOLDER GetNextSubFolder(QString& strFolder,LPMAPIFOLDER pFolder=NULL);
	LPMAPIFOLDER OpenSubFolder(LPCTSTR szSubFolder,LPMAPIFOLDER pFolder=NULL);
	LPMAPIFOLDER CreateSubFolder(LPCTSTR szSubFolder,LPMAPIFOLDER pFolder=NULL);
	BOOL DeleteSubFolder(LPCTSTR szSubFolder,LPMAPIFOLDER pFolder=NULL);
	BOOL DeleteSubFolder(LPMAPIFOLDER pSubFolder,LPMAPIFOLDER pFolder=NULL);

	BOOL GetContents(LPMAPIFOLDER pFolder=NULL);
	BOOL SortContents(ULONG ulSortParam=TABLE_SORT_ASCEND,ULONG ulSortField=PR_MESSAGE_DELIVERY_TIME);
	BOOL GetNextMessage(CMAPIMessage& message,BOOL bUnreadOnly=FALSE);
	BOOL GetNextContact(CMAPIContact& contact);

	BOOL GetNextAppointment(CMAPIAppointment& appointment);

	BOOL DeleteMessage(CMAPIMessage& message,LPMAPIFOLDER pFolder=NULL);
	BOOL CopyMessage(CMAPIMessage& message,LPMAPIFOLDER pFolderDest,LPMAPIFOLDER pFolderSrc=NULL);
	BOOL MoveMessage(CMAPIMessage& message,LPMAPIFOLDER pFolderDest,LPMAPIFOLDER pFolderSrc=NULL);

	BOOL SendMessage(CMAPIMessage& message);

	BOOL Notify(LPNOTIFCALLBACK lpfnCallback,LPVOID lpvContext,ULONG ulEventMask=fnevNewMail);

	BOOL GetEmail(ADRENTRY& adrEntry,QString& strEmail);
	BOOL GetExEmail(SBinary entryID,QString& strEmail);
	
	void GetPersonalEmails(QStringList &lstEmails);

	static void GetNarrowString(SPropValue& prop,QString& strNarrow);
	static QString GetValidString(SPropValue& prop);
};

#ifndef PR_BODY_HTML
#define PR_BODY_HTML PROP_TAG( PT_TSTRING,	0x1013)
#endif

#ifndef STORE_HTML_OK
#define	STORE_HTML_OK ((ULONG) 0x00010000)
#endif

#ifndef PR_SMTP_ADDRESS
#ifdef UNICODE
#define PR_SMTP_ADDRESS 0x39FE001F
#else
#define PR_SMTP_ADDRESS 0x39FE001E
#endif
#endif

#endif //#ifdef _WIN32

#endif
