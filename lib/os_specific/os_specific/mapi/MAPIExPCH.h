#ifndef __MAPIEXPCH_H__
#define __MAPIEXPCH_H__

#ifdef _WIN32

//#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
//#include <afxwin.h>         // MFC core and standard components
#define WIN32_LEAN_AND_MEAN
#include <qt_windows.h>
//#define NOMINMAX
//#include <windows.h>

#ifdef MAPIEX_EXPORTS
#undef AFX_EXT_CLASS
#define AFX_EXT_CLASS __declspec(dllexport)
#else
#undef AFX_EXT_CLASS
#define AFX_EXT_CLASS
#endif

#define USES_IID_IMAPIViewAdviseSink

#include <MapiUtil.h>

#endif

#endif
