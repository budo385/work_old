#ifndef __MAPIMESSAGE_H__
#define __MAPIMESSAGE_H__

#ifdef _WIN32

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File: MAPIMessage.h
// Description: MAPI Message class wrapper
//
// Copyright (C) 2005-2006, Noel Dillabough
//
// This source code is free to use and modify provided this notice remains intact and that any enhancements
// or bug fixes are posted to the CodeProject page hosting this class for the community to benefit.
//
// Usage: see the Codeproject article at http://www.codeproject.com/internet/CMapiEx.asp
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CMAPIEx;

#include <QString>
#include <QTime>

/////////////////////////////////////////////////////////////
// CMAPIMessage

class CMAPIMessage
{
public:
	CMAPIMessage();
	CMAPIMessage(CMAPIMessage& message);
	~CMAPIMessage();

	enum { PROP_RECIPIENT_TYPE, PROP_RECIPIENT_NAME, PROP_RECIPIENT_EMAIL, PROP_ADDR_TYPE, PROP_ENTRYID, RECIPIENT_COLS };

// Attributes
protected:
	QString m_strSenderName;
	QString m_strSenderEmail;
	QString m_strSubject;
	QString m_strBody;
	QString m_strRTF;
	int m_nAttachments;
	QDateTime m_tmReceived;

	CMAPIEx* m_pMAPI;
	LPMESSAGE m_pMessage;
	DWORD m_dwMessageFlags;
	SBinary m_entry;
	LPMAPITABLE m_pRecipients;

// Operations
public:
	void SetEntryID(SBinary* pEntry=NULL);
	SBinary* GetEntryID() { return &m_entry; }

	CMAPIEx* GetMapiEx() { return m_pMAPI; }

	BOOL IsUnread() { return (0 == (m_dwMessageFlags & MSGFLAG_READ)); }
	void MarkAsRead(BOOL bRead=TRUE);
	BOOL IsIncoming();
	BOOL Open(CMAPIEx* pMAPI,SBinary entry,DWORD dwMessageFlags = -1);
	void Close();
	BOOL Create(CMAPIEx* pMAPI,int nPriority=IMPORTANCE_NORMAL); // IMPORTANCE_HIGH or IMPORTANCE_LOW also valid
	int ShowForm(CMAPIEx* pMAPI);			// returns IDOK|IDCANCEL if ok

	QString& GetSenderName() { return m_strSenderName; }
	QString& GetSenderEmail() { return m_strSenderEmail; }
	QString& GetSubject() { return m_strSubject; }
	QString& GetBody();
	QString& GetRTF();
	QDateTime& GetReceivedTime() { return m_tmReceived; }
	QString GetReceivedTime(LPCTSTR szFormat); // set to NULL for default of "%m/%d/%Y %I:%M %p"
	bool GetStringProperty(QString &strData, int nProperty);

	bool GetTo(QString& strTo);
	bool GetCC(QString& strCC);
	bool GetBCC(QString& strBCC);
	bool GetRecipients();
	bool GetNextRecipient(QString& strName,QString& strEmail,int& nType);
	bool GetMessageClass(QString& strMessageClass);

	int GetAttachmentCount() { return m_nAttachments; }
	QString GetAttachmentName(int nIndex);
	QString GetAttachmentCID(int nIndex);
	QByteArray GetAttachmentData(int nIndex);
	BOOL SaveAttachments(LPCTSTR szFolder,int nIndex=-1);
	BOOL DeleteAttachments(int nIndex=-1);
	
	void SetMessageStatus(int nMessageStatus); // used only by WinCE, pass in MSGSTATUS_RECTYPE_SMS to send an SMS
	BOOL AddRecipient(LPCTSTR szEmail,int nType=MAPI_TO);  // MAPI_CC and MAPI_BCC also valid
	void SetSubject(LPCTSTR szSubject);
	void SetSenderName(LPCTSTR szSenderName);
	void SetSenderEmail(LPCTSTR szSenderEmail);
	void SetBody(QString strBody);
	//void SetBody(LPCTSTR szBody);
	void SetRTF(QString strRTF);
	//void SetRTF(LPCTSTR szRTF);
	BOOL AddAttachment(LPCTSTR szPath,LPCTSTR szName=NULL,LPCTSTR szCID=NULL,LPCTSTR szMime=NULL);
	void SetReadReceipt(BOOL bSet=TRUE,LPCTSTR szReceiverEmail=NULL);
	void MarkAsPrivate();
	BOOL Send();

	// does not copy/reference m_pMessage, shallow copy only
	CMAPIMessage& operator=(CMAPIMessage& message);

	// compares entryID only
	BOOL operator==(CMAPIMessage& message);

protected:
	HRESULT GetProperty(ULONG ulProperty, LPSPropValue &prop);

	void FillSenderName();
	void FillSenderEmail();
	void FillSubject();
	void FillBody();
	void FillRTF();
	void FillAttachmentCount();
	void FillReceivedTime();

	BOOL SaveAttachment(LPATTACH pAttachment,QString strPath);
	BOOL StoreAttachment(LPATTACH pAttachment,QByteArray &strData);
};

#endif //#ifdef _WIN32

#endif
