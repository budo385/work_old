////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File: MAPIEx.cpp
// Description: Windows Extended MAPI class 
//
// Copyright (C) 2005-2006, Noel Dillabough
//
// This source code is free to use and modify provided this notice remains intact and that any enhancements
// or bug fixes are posted to the CodeProject page hosting this class for the community to benefit.
//
// Usage: see the Codeproject article at http://www.codeproject.com/internet/CMapiEx.asp
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
#ifndef WINCE

#include "MAPIExPCH.h"
#include "MAPIEx.h"
#include "MAPISink.h"

#include <QStringList>

#define USES_IID_IMailUser
#include <initguid.h>
#include <MapiGuid.h>

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#define PR_IPM_CONTACT_ENTRYID (PROP_TAG(PT_BINARY, 0x36D1))
#define PR_IPM_APPOINTMENT_ENTRYID (PROP_TAG(PT_BINARY, 0x36D0))

#ifdef _WIN32_WCE
#pragma comment(lib,"cemapi.lib")
#pragma comment(lib,"UUID.lib")
#else
#pragma comment (lib,"mapi32")
#endif
#pragma comment(lib,"Ole32.lib")

/////////////////////////////////////////////////////////////
// CMAPIEx

#ifdef UNICODE
int CMAPIEx::cm_nMAPICode=MAPI_UNICODE;
#else
int CMAPIEx::cm_nMAPICode=0;
#endif

CMAPIEx::CMAPIEx()
{
	m_pSession=NULL;
	m_pMsgStore=NULL;
	m_pFolder=NULL;	
	m_pContents=NULL;
	m_pHierarchy=NULL;
	m_sink=0;
}

CMAPIEx::~CMAPIEx()
{
	Logout();
}

BOOL CMAPIEx::Init(BOOL bMultiThreadedNotifcations)
{
#ifdef _WIN32_WCE
	if(CoInitializeEx(NULL, COINIT_MULTITHREADED)!=S_OK) return FALSE;
#endif
	if(bMultiThreadedNotifcations) {
		MAPIINIT_0 MAPIInit={ 0, MAPI_MULTITHREAD_NOTIFICATIONS };
		if(MAPIInitialize(&MAPIInit)!=S_OK) return FALSE;
	} else {
		if(MAPIInitialize(NULL)!=S_OK) return FALSE;
	}
	return TRUE;
}

void CMAPIEx::Term()
{
	MAPIUninitialize();
#ifdef _WIN32_WCE
	CoUninitialize();
#endif
}

BOOL CMAPIEx::Login(LPCTSTR szProfileName)
{
	return (MAPILogonEx(NULL,(LPTSTR)szProfileName,NULL,MAPI_EXTENDED | MAPI_USE_DEFAULT | MAPI_NEW_SESSION,&m_pSession)==S_OK);
}

void CMAPIEx::Logout()
{
	if(m_sink) {
		if(m_pMsgStore) m_pMsgStore->Unadvise(m_sink);
		m_sink=0;
	}

	RELEASE(m_pHierarchy);
	RELEASE(m_pContents);
	RELEASE(m_pFolder);
	RELEASE(m_pMsgStore);
	RELEASE(m_pSession);
}

// if I try to use MAPI_UNICODE when UNICODE is defined I get the MAPI_E_BAD_CHARWIDTH 
// error so I force narrow strings here
QString CMAPIEx::GetProfileName()
{
	if(!m_pSession) return NULL;

	static QString strProfileName;
	LPSRowSet pRows=NULL;
	const int nProperties=2;
	SizedSPropTagArray(nProperties,Columns)={nProperties,{PR_DISPLAY_NAME_A, PR_RESOURCE_TYPE}};

	IMAPITable*	pStatusTable;
	if(m_pSession->GetStatusTable(0,&pStatusTable)==S_OK) {
		if(pStatusTable->SetColumns((LPSPropTagArray)&Columns, 0)==S_OK) {
			while(TRUE) {
				if(pStatusTable->QueryRows(1,0,&pRows)!=S_OK) MAPIFreeBuffer(pRows);
				else if(pRows->cRows!=1) FreeProws(pRows);
				else if(pRows->aRow[0].lpProps[1].Value.ul==MAPI_SUBSYSTEM) {
					strProfileName=GetValidString(pRows->aRow[0].lpProps[0]);
					FreeProws(pRows);
				} else {
					FreeProws(pRows);
					continue;
				}
				break;
			}
		}
		RELEASE(pStatusTable);
	}
	return strProfileName;
}

int CMAPIEx::GetMessageStoreCount()
{
	if(!m_pSession) return -1;

	LPSRowSet pRows=NULL;
	const int nProperties=3;
	SizedSPropTagArray(nProperties,Columns)={nProperties,{PR_DISPLAY_NAME, PR_ENTRYID, PR_DEFAULT_STORE}};

	int nCount = 0;
	BOOL bResult=FALSE;
	IMAPITable*	pMsgStoresTable;
	if(m_pSession->GetMsgStoresTable(0, &pMsgStoresTable)==S_OK) {
		if(pMsgStoresTable->SetColumns((LPSPropTagArray)&Columns, 0)==S_OK) {
			if(pMsgStoresTable->QueryRows(1000,0,&pRows)==S_OK){
				nCount = pRows->cRows;
				FreeProws(pRows);
			}
			else
				MAPIFreeBuffer(pRows);
		}
		RELEASE(pMsgStoresTable);
	}
	return nCount;
}

BOOL CMAPIEx::OpenMessageStore(int nStoreIndex, QString &strStoreName)
{
	if(!m_pSession) return FALSE;

	strStoreName = "";

	LPSRowSet pRows=NULL;
	const int nProperties=3;
	SizedSPropTagArray(nProperties,Columns)={nProperties,{PR_DISPLAY_NAME, PR_ENTRYID, PR_DEFAULT_STORE}};

	BOOL bResult=FALSE;
	IMAPITable*	pMsgStoresTable;
	if(m_pSession->GetMsgStoresTable(0, &pMsgStoresTable)==S_OK) 
	{
		if(pMsgStoresTable->SetColumns((LPSPropTagArray)&Columns, 0)==S_OK) 
		{
			if(pMsgStoresTable->QueryRows(1000,0,&pRows)!=S_OK) 
				MAPIFreeBuffer(pRows);
			else if(pRows->cRows <= nStoreIndex) 
				FreeProws(pRows);
			else {
				bResult=TRUE;
				strStoreName=GetValidString(pRows->aRow[nStoreIndex].lpProps[0]);
			}
			if(bResult) {
				RELEASE(m_pMsgStore);
				bResult=(m_pSession->OpenMsgStore(NULL,pRows->aRow[nStoreIndex].lpProps[1].Value.bin.cb,(ENTRYID*)pRows->aRow[nStoreIndex].lpProps[1].Value.bin.lpb,NULL,MDB_NO_DIALOG | MAPI_BEST_ACCESS,&m_pMsgStore)==S_OK);
				FreeProws(pRows);
			}
		}
		RELEASE(pMsgStoresTable);
	}
	return bResult;
}

BOOL CMAPIEx::OpenMessageStore(LPCTSTR szStore)
{
	if(!m_pSession) return FALSE;

	LPSRowSet pRows=NULL;
	const int nProperties=3;
	SizedSPropTagArray(nProperties,Columns)={nProperties,{PR_DISPLAY_NAME, PR_ENTRYID, PR_DEFAULT_STORE}};

	BOOL bResult=FALSE;
	IMAPITable*	pMsgStoresTable;
	if(m_pSession->GetMsgStoresTable(0, &pMsgStoresTable)==S_OK) {
		if(pMsgStoresTable->SetColumns((LPSPropTagArray)&Columns, 0)==S_OK) {
			while(TRUE) {
				if(pMsgStoresTable->QueryRows(1,0,&pRows)!=S_OK) 
					MAPIFreeBuffer(pRows);
				else if(pRows->cRows!=1) 
					FreeProws(pRows);
				else {
					if(!szStore) { 
						if(pRows->aRow[0].lpProps[2].Value.b) bResult=TRUE;
					} else {
						QString strStore=GetValidString(pRows->aRow[0].lpProps[0]);
						if(strStore.indexOf((const char *)szStore)!=-1) bResult=TRUE;
					}
					if(!bResult) {
						FreeProws(pRows);
						continue;
					}
				}
				break;
			}
			if(bResult) {
				RELEASE(m_pMsgStore);
				bResult=(m_pSession->OpenMsgStore(NULL,pRows->aRow[0].lpProps[1].Value.bin.cb,(ENTRYID*)pRows->aRow[0].lpProps[1].Value.bin.lpb,NULL,MDB_NO_DIALOG | MAPI_BEST_ACCESS,&m_pMsgStore)==S_OK);
				FreeProws(pRows);
			}
		}
		RELEASE(pMsgStoresTable);
	}
	return bResult;
}

ULONG CMAPIEx::GetMessageStoreSupport()
{
	if(!m_pMsgStore) return FALSE;

	LPSPropValue props=NULL;
	ULONG cValues=0;
	ULONG rgTags[]={ 1, PR_STORE_SUPPORT_MASK };
	ULONG ulSupport=0;

	if(m_pMsgStore->GetProps((LPSPropTagArray) rgTags, CMAPIEx::cm_nMAPICode, &cValues, &props)==S_OK) {
		ulSupport=props->Value.ul;
		MAPIFreeBuffer(props);
	}
	return ulSupport;
}

LPMAPIFOLDER CMAPIEx::OpenSpecialFolder(unsigned long ulFolderID,BOOL bInternal)
{
	LPMAPIFOLDER pInbox=OpenInbox(FALSE);
	if(!pInbox || !m_pMsgStore) return FALSE;

	LPSPropValue props=NULL;
	ULONG cValues=0;
	DWORD dwObjType;
	ULONG rgTags[]={ 1, ulFolderID };
	LPMAPIFOLDER pFolder;

	if(pInbox->GetProps((LPSPropTagArray) rgTags, 0, &cValues, &props)!=S_OK) 
		return NULL;
	HRESULT hr=m_pMsgStore->OpenEntry(props[0].Value.bin.cb, (LPENTRYID)props[0].Value.bin.lpb, NULL, MAPI_MODIFY, &dwObjType, (LPUNKNOWN*)&pFolder);
	MAPIFreeBuffer(props);
	RELEASE(pInbox);
	if(hr!=S_OK) 
		return NULL;

	if(pFolder && bInternal) {
		RELEASE(m_pFolder);
		m_pFolder=pFolder;
	}
	
	return pFolder;
}

LPMAPIFOLDER CMAPIEx::OpenFolder(unsigned long ulFolderID,BOOL bInternal)
{
	if(!m_pMsgStore) return NULL;

	LPSPropValue props=NULL;
	ULONG cValues=0;
	DWORD dwObjType;
	ULONG rgTags[]={ 1, ulFolderID };
	LPMAPIFOLDER pFolder;

	if(m_pMsgStore->GetProps((LPSPropTagArray) rgTags, cm_nMAPICode, &cValues, &props)!=S_OK) 
		return NULL;
	m_pMsgStore->OpenEntry(props[0].Value.bin.cb,(LPENTRYID)props[0].Value.bin.lpb, NULL, MAPI_MODIFY, &dwObjType,(LPUNKNOWN*)&pFolder);
	MAPIFreeBuffer(props);

	if(pFolder && bInternal) {
		RELEASE(m_pFolder);
		m_pFolder=pFolder;
	}
	return pFolder;
}

LPMAPIFOLDER CMAPIEx::OpenRootFolder(BOOL bInternal)
{
	return OpenFolder(PR_IPM_SUBTREE_ENTRYID,bInternal);
}

LPMAPIFOLDER CMAPIEx::OpenContacts(BOOL bInternal)
{
	return OpenSpecialFolder(PR_IPM_CONTACT_ENTRYID, bInternal);
}

LPMAPIFOLDER CMAPIEx::OpenCalendar(BOOL bInternal)
{
	return OpenSpecialFolder(PR_IPM_APPOINTMENT_ENTRYID, bInternal);
}

LPMAPIFOLDER CMAPIEx::OpenInbox(BOOL bInternal)
{
	if(!m_pMsgStore) return NULL;

#ifdef _WIN32_WCE
	return OpenFolder(PR_CE_IPM_INBOX_ENTRYID);
#else
	ULONG cbEntryID;
	LPENTRYID pEntryID;
	DWORD dwObjType;
	LPMAPIFOLDER pFolder;

	if(m_pMsgStore->GetReceiveFolder(NULL,0,&cbEntryID,&pEntryID,NULL)!=S_OK) return NULL;
	m_pMsgStore->OpenEntry(cbEntryID,pEntryID, NULL, MAPI_MODIFY,&dwObjType,(LPUNKNOWN*)&pFolder);
	MAPIFreeBuffer(pEntryID);
#endif

	if(pFolder && bInternal) {
		RELEASE(m_pFolder);
		m_pFolder=pFolder;
	}
	return pFolder;
}

LPMAPIFOLDER CMAPIEx::OpenOutbox(BOOL bInternal)
{
	return OpenFolder(PR_IPM_OUTBOX_ENTRYID,bInternal);
}

LPMAPIFOLDER CMAPIEx::OpenSentItems(BOOL bInternal)
{
	return OpenFolder(PR_IPM_SENTMAIL_ENTRYID,bInternal);
}

LPMAPIFOLDER CMAPIEx::OpenDeletedItems(BOOL bInternal)
{
	return OpenFolder(PR_IPM_WASTEBASKET_ENTRYID,bInternal);
}

LPMAPITABLE CMAPIEx::GetHierarchy(LPMAPIFOLDER pFolder)
{
	if(!pFolder) {
		pFolder=m_pFolder;
		if(!pFolder) return NULL;
	}
	RELEASE(m_pHierarchy);
	if(pFolder->GetHierarchyTable(0,&m_pHierarchy)!=S_OK) return NULL;

	const int nProperties=2;
	SizedSPropTagArray(nProperties,Columns)={nProperties,{PR_DISPLAY_NAME, PR_ENTRYID}};
	if(m_pHierarchy->SetColumns((LPSPropTagArray)&Columns, 0)==S_OK) return m_pHierarchy;
	return NULL;
}

LPMAPIFOLDER CMAPIEx::GetNextSubFolder(QString& strFolder, LPMAPIFOLDER pFolder)
{
	if(!m_pHierarchy) return NULL;
	if(!pFolder) {
		pFolder=m_pFolder;
		if(!pFolder) return FALSE;
	}

	DWORD dwObjType;
	LPSRowSet pRows=NULL;
	strFolder = "";

	LPMAPIFOLDER pSubFolder=NULL;
	if(m_pHierarchy->QueryRows(1,0,&pRows)==S_OK) {
		if(pRows->cRows > 0) {
			if(pFolder->OpenEntry(pRows->aRow[0].lpProps[PROP_ENTRYID].Value.bin.cb,(LPENTRYID)pRows->aRow[0].lpProps[PROP_ENTRYID].Value.bin.lpb, NULL, MAPI_MODIFY, &dwObjType,(LPUNKNOWN*)&pSubFolder)==S_OK) {
				if(pRows->aRow[0].cValues > 0){
					strFolder = GetValidString(pRows->aRow[0].lpProps[0]);
				}
			}
		}
		FreeProws(pRows);
	}
	MAPIFreeBuffer(pRows);
	return pSubFolder;
}

// High Level function to open a sub folder by iterating recursively (DFS) over all folders 
// (use instead of manually calling GetHierarchy and GetNextSubFolder)
LPMAPIFOLDER CMAPIEx::OpenSubFolder(LPCTSTR szSubFolder,LPMAPIFOLDER pFolder)
{
	LPMAPIFOLDER pSubFolder=NULL;
	LPMAPITABLE pHierarchy;

	RELEASE(m_pHierarchy);
	pHierarchy=GetHierarchy(pFolder);
	if(pHierarchy) {
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MAPI OpenSubFolder: start loop"));

		QString strFolder;
		LPMAPIFOLDER pRecurse=NULL;
		do {
			RELEASE(pSubFolder);
			m_pHierarchy=pHierarchy;
			pSubFolder=GetNextSubFolder(strFolder,pFolder);
			if(pSubFolder) {
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MAPI OpenSubFolder: found subfolder [%1]").arg(strFolder));
				if(strFolder == QString::fromUtf16((const ushort *)szSubFolder)) break;
				m_pHierarchy=NULL; // so we don't release it in subsequent drilldown
				pRecurse=OpenSubFolder(szSubFolder,pSubFolder);
				if(pRecurse) {
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MAPI OpenSubFolder: result found, release previous subfolder"));
					RELEASE(pSubFolder);
					pSubFolder=pRecurse;
					break;
				}
			}
		} while(pSubFolder);

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MAPI OpenSubFolder: end loop"));
		RELEASE(pHierarchy);
		m_pHierarchy=NULL;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MAPI OpenSubFolder: out of the loop"));

	// this may occur many times depending on how deep the recursion is; make sure we haven't already assigned m_pFolder
	if(pSubFolder && m_pFolder!=pSubFolder) {
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MAPI OpenSubFolder: result found, release old folder"));
		RELEASE(m_pFolder);
		m_pFolder=pSubFolder;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("MAPI OpenSubFolder: done"));
	return pSubFolder;
} 

BOOL CMAPIEx::GetContents(LPMAPIFOLDER pFolder)
{
	if(!pFolder) {
		pFolder=m_pFolder;
		if(!pFolder) return FALSE;
	}
	RELEASE(m_pContents);
	if(pFolder->GetContentsTable(0,&m_pContents)!=S_OK) return FALSE;

	const int nProperties=MESSAGE_COLS;
	SizedSPropTagArray(nProperties,Columns)={nProperties,{PR_MESSAGE_FLAGS, PR_ENTRYID }};
	return (m_pContents->SetColumns((LPSPropTagArray)&Columns,0)==S_OK);
}

BOOL CMAPIEx::SortContents(ULONG ulSortParam,ULONG ulSortField)
{
	if(!m_pContents) return FALSE;

	SizedSSortOrderSet(1, SortColums) = {1, 0, 0, {{ulSortField,ulSortParam}}};
	return (m_pContents->SortTable((LPSSortOrderSet)&SortColums,0)==S_OK);
}

BOOL CMAPIEx::GetNextMessage(CMAPIMessage& message,BOOL bUnreadOnly)
{
	if(!m_pContents) return FALSE;
	
	DWORD dwMessageFlags;
	LPSRowSet pRows=NULL;
	BOOL bResult=FALSE;
	while(m_pContents->QueryRows(1,0,&pRows)==S_OK) {
		if(pRows->cRows) {
			dwMessageFlags=pRows->aRow[0].lpProps[PROP_MESSAGE_FLAGS].Value.ul;
			if(bUnreadOnly &&  dwMessageFlags&MSGFLAG_READ) {
				FreeProws(pRows);
				continue;
			}
			bResult=message.Open(this,pRows->aRow[0].lpProps[PROP_ENTRYID].Value.bin,dwMessageFlags);
		}
		FreeProws(pRows);
		break;
	}
	MAPIFreeBuffer(pRows);
	return bResult;
}

BOOL CMAPIEx::GetNextContact(CMAPIContact& contact)
{
	if(!m_pContents) return FALSE;

	LPSRowSet pRows=NULL;
	BOOL bResult=FALSE;
	while(m_pContents->QueryRows(1,0,&pRows)==S_OK) {
		if(pRows->cRows) bResult=contact.Open(this,pRows->aRow[0].lpProps[PROP_ENTRYID].Value.bin);
		FreeProws(pRows);
		break;
	}
	MAPIFreeBuffer(pRows);
	return bResult;
}

BOOL CMAPIEx::GetNextAppointment(CMAPIAppointment& appointment)
{
	if(!m_pContents) return FALSE;

	LPSRowSet pRows=NULL;
	BOOL bResult=FALSE;
	while(m_pContents->QueryRows(1,0,&pRows)==S_OK) {
		if(pRows->cRows) bResult=appointment.Open(this,pRows->aRow[0].lpProps[PROP_ENTRYID].Value.bin);
		FreeProws(pRows);
		break;
	}
	MAPIFreeBuffer(pRows);
	return bResult;
}

// Creates a subfolder under pFolder, opens the folder if it already exists
LPMAPIFOLDER CMAPIEx::CreateSubFolder(LPCTSTR szSubFolder,LPMAPIFOLDER pFolder)
{
	if(!pFolder) {
		pFolder=m_pFolder;
		if(!pFolder) return NULL;
	}
	
	LPMAPIFOLDER pSubFolder=NULL;
	ULONG ulFolderType=FOLDER_GENERIC;
	ULONG ulFlags=OPEN_IF_EXISTS | cm_nMAPICode;

	pFolder->CreateFolder(ulFolderType,(LPTSTR)szSubFolder,NULL,NULL,ulFlags,&pSubFolder);
	return pSubFolder;
}

// Deletes a sub folder and ALL sub folders/messages
BOOL CMAPIEx::DeleteSubFolder(LPCTSTR szSubFolder,LPMAPIFOLDER pFolder)
{
	if(!pFolder) {
		pFolder=m_pFolder;
		if(!pFolder) return FALSE;
	}

	LPMAPIFOLDER pSubFolder=NULL;
	if(GetHierarchy(pFolder)) {
		QString strFolder;
		do {
			RELEASE(pSubFolder);
			pSubFolder=GetNextSubFolder(strFolder,pFolder);
			if(pSubFolder && strFolder == QString((const char *)szSubFolder)) break;
		} while(pSubFolder);
	}
	return DeleteSubFolder(pSubFolder,pFolder);
}

// Deletes a sub folder and ALL sub folders/messages
BOOL CMAPIEx::DeleteSubFolder(LPMAPIFOLDER pSubFolder,LPMAPIFOLDER pFolder)
{
	if(!pSubFolder) return FALSE;

	if(!pFolder) {
		pFolder=m_pFolder;
		if(!pFolder) return FALSE;
	}

	LPSPropValue props=NULL;
	ULONG cValues=0;
	ULONG rgTags[]={ 1, PR_ENTRYID };

	if(pSubFolder->GetProps((LPSPropTagArray) rgTags, CMAPIEx::cm_nMAPICode, &cValues, &props)==S_OK) {
		HRESULT hr=pFolder->DeleteFolder(props[0].Value.bin.cb,(LPENTRYID)props[0].Value.bin.lpb,NULL,NULL,DEL_FOLDERS|DEL_MESSAGES);
		MAPIFreeBuffer(props);
		return (hr==S_OK);
	}
	return FALSE;
}

BOOL CMAPIEx::DeleteMessage(CMAPIMessage& message,LPMAPIFOLDER pFolder)
{
	if(!pFolder) {
		pFolder=m_pFolder;
		if(!pFolder) return FALSE;
	}

	ENTRYLIST entries={ 1, message.GetEntryID() };
	HRESULT hr=pFolder->DeleteMessages(&entries,NULL,NULL,0);
	return (hr==S_OK);
}

BOOL CMAPIEx::CopyMessage(CMAPIMessage& message,LPMAPIFOLDER pFolderDest,LPMAPIFOLDER pFolderSrc)
{
	if(!pFolderDest) return FALSE;

	if(!pFolderSrc) {
		pFolderSrc=m_pFolder;
		if(!pFolderSrc) return FALSE;
	}

	ENTRYLIST entries={ 1, message.GetEntryID() };
	HRESULT hr=pFolderSrc->CopyMessages(&entries,NULL,pFolderDest,NULL,NULL,0);
	return (hr==S_OK);
}

BOOL CMAPIEx::MoveMessage(CMAPIMessage& message,LPMAPIFOLDER pFolderDest,LPMAPIFOLDER pFolderSrc)
{
	if(!pFolderDest) return FALSE;

	if(!pFolderSrc) {
		pFolderSrc=m_pFolder;
		if(!pFolderSrc) return FALSE;
	}

	ENTRYLIST entries={ 1, message.GetEntryID() };
	HRESULT hr=pFolderSrc->CopyMessages(&entries,NULL,pFolderDest,NULL,NULL,MESSAGE_MOVE);
	return (hr==S_OK);
}

// call with ulEventMask set to ALL notifications ORed together, only one Advise Sink is used.
BOOL CMAPIEx::Notify(LPNOTIFCALLBACK lpfnCallback,LPVOID lpvContext,ULONG ulEventMask)
{
	if(GetMessageStoreSupport()&STORE_NOTIFY_OK) {
		if(m_sink) m_pMsgStore->Unadvise(m_sink);
		CMAPISink* pAdviseSink=new CMAPISink(lpfnCallback,lpvContext);
		if(m_pMsgStore->Advise(0,NULL,ulEventMask,pAdviseSink,&m_sink)==S_OK) return TRUE;
		delete pAdviseSink;
		m_sink=0;
	}
	return FALSE;
}

// sometimes the string in prop is invalid, causing unexpected crashes
QString CMAPIEx::GetValidString(SPropValue& prop)
{
	LPCTSTR s=prop.Value.LPSZ;
	if(s && !::IsBadStringPtr(s,(UINT_PTR)-1)) return QString::fromUtf16((const ushort *)s);
	return QString("");
}

// ADDRENTRY objects from Address don't come in unicode so I check for _A and force narrow strings
BOOL CMAPIEx::GetEmail(ADRENTRY& adrEntry, QString& strEmail)
{
	LPSPropValue pProp=PpropFindProp(adrEntry.rgPropVals,adrEntry.cValues,PR_ADDRTYPE);
	if(!pProp) pProp=PpropFindProp(adrEntry.rgPropVals,adrEntry.cValues,PR_ADDRTYPE_A);
	if(pProp) {
		QString strAddrType;
		GetNarrowString(*pProp,strAddrType);
		if(strAddrType=="EX") {
			pProp=PpropFindProp(adrEntry.rgPropVals,adrEntry.cValues,PR_ENTRYID);

			SBinary entryID;
			entryID.cb=pProp->Value.bin.cb;
			entryID.lpb=pProp->Value.bin.lpb;

			return GetExEmail(entryID,strEmail);
		}
	}
	pProp=PpropFindProp(adrEntry.rgPropVals,adrEntry.cValues,PR_EMAIL_ADDRESS);
	if(!pProp) pProp=PpropFindProp(adrEntry.rgPropVals,adrEntry.cValues,PR_EMAIL_ADDRESS_A);
	if(pProp) {
		GetNarrowString(*pProp,strEmail);
		return TRUE;
	}
	return FALSE;
}

BOOL CMAPIEx::GetExEmail(SBinary entryID, QString& strEmail)
{
	BOOL bResult=FALSE;
#ifndef _WIN32_WCE
	if(!m_pSession) return FALSE;

	LPADRBOOK pAddressBook;
	if(m_pSession->OpenAddressBook(0, NULL, AB_NO_DIALOG, &pAddressBook)==S_OK) {
		ULONG ulObjType;
		IMAPIProp* pItem=NULL;
		if(pAddressBook->OpenEntry(entryID.cb,(ENTRYID*)entryID.lpb,NULL,MAPI_BEST_ACCESS,&ulObjType,(LPUNKNOWN*)&pItem)==S_OK) {
			if(ulObjType==MAPI_MAILUSER) {
				LPSPropValue pProp;
				ULONG ulPropCount;
				ULONG p[2]={ 1,PR_SMTP_ADDRESS };

				if(pItem->GetProps((LPSPropTagArray)p, CMAPIEx::cm_nMAPICode, &ulPropCount, &pProp)==S_OK) {
					strEmail=CMAPIEx::GetValidString(*pProp);
					MAPIFreeBuffer(pProp);
					bResult=TRUE;
				}
			}
			RELEASE(pItem);
		}
		RELEASE(pAddressBook);
	}
#endif
	return bResult;
}


// special case of GetValidString to take the narrow string in UNICODE
void CMAPIEx::GetNarrowString(SPropValue& prop,QString& strNarrow)
{
	const char *s=GetValidString(prop).toLatin1().constData();
	if(!s) strNarrow = "";
	else {
		strNarrow=s;
	}
}

void CMAPIEx::GetPersonalEmails(QStringList &lstEmails)
{
	if(!m_pSession) 
		return;

	//
	// Get the EntryID of the current user
	//
	HRESULT  hr;
	DWORD  cEntryID = 0;
	LPENTRYID  pEntryID = 0;
	hr = m_pSession->QueryIdentity( &cEntryID, &pEntryID );

	//
	// Open the messaging user object
	//
	DWORD  dwObjType = 0;
	LPMAILUSER  pUser  = NULL;
	LPSPropTagArray pPTA   = NULL;

	hr = m_pSession->OpenEntry( cEntryID, pEntryID, &IID_IMailUser, NULL,
		&dwObjType, (LPUNKNOWN*)&pUser );

	ULONG p[2]={ 1, PR_EMAIL_ADDRESS};

	DWORD  cProps;
	LPSPropValue  pPV;
	if(pUser->GetProps((LPSPropTagArray)p, NULL, &cProps, &pPV )==S_OK) {
		QString strEmail=CMAPIEx::GetValidString(*pPV);
		MAPIFreeBuffer(pPV);

		lstEmails.append(strEmail);
	}
	
	MAPIFreeBuffer(pEntryID);
	RELEASE(pUser);
}
#endif // WINCE
#endif //#ifdef _WIN32