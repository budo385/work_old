////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File: MAPIContact.cpp
// Description: MAPI Contact class wrapper
//
// Copyright (C) 2005-2006, Noel Dillabough
//
// This source code is free to use and modify provided this notice remains intact and that any enhancements
// or bug fixes are posted to the CodeProject page hosting this class for the community to benefit.
//
// Usage: see the Codeproject article at http://www.codeproject.com/internet/CMapiEx.asp
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _WIN32
#include "MAPIExPCH.h"
#include "MAPIEx.h"

/////////////////////////////////////////////////////////////
// CContactAddress

CContactAddress::CContactAddress()
{

}

void CContactAddress::FillAddress(CMAPIContact& contact,AddressType nType)
{
	if(nType<BUSINESS || nType>OTHER) return;

#ifndef WINCE
	int nAddressTag[][5]={
		{ PR_BUSINESS_ADDRESS_CITY, PR_BUSINESS_ADDRESS_COUNTRY, PR_BUSINESS_ADDRESS_STATE_OR_PROVINCE,
			PR_BUSINESS_ADDRESS_STREET, PR_BUSINESS_ADDRESS_POSTAL_CODE },
		{ PR_HOME_ADDRESS_CITY, PR_HOME_ADDRESS_COUNTRY, PR_HOME_ADDRESS_STATE_OR_PROVINCE,
			PR_HOME_ADDRESS_STREET, PR_HOME_ADDRESS_POSTAL_CODE },
		{ PR_OTHER_ADDRESS_CITY, PR_OTHER_ADDRESS_COUNTRY, PR_OTHER_ADDRESS_STATE_OR_PROVINCE, 
			PR_OTHER_ADDRESS_STREET, PR_OTHER_ADDRESS_POSTAL_CODE },
	};

	contact.GetPropertyString(m_strCity,nAddressTag[nType][0]);
	contact.GetPropertyString(m_strCountry,nAddressTag[nType][1]);
	contact.GetPropertyString(m_strStateOrProvince,nAddressTag[nType][2]);
	contact.GetPropertyString(m_strStreet,nAddressTag[nType][3]);
	contact.GetPropertyString(m_strPostalCode,nAddressTag[nType][4]);
	contact.GetPropertyString(m_strTitle,PR_DISPLAY_NAME_PREFIX);
	/*
	int nProp = nAddressTag[nType][0];
	LPSPropValue pProp;
	if(contact.GetProperty(nProp,pProp)==S_OK) {
		m_strCity=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	} else {
		m_strCity="";
	}
	if(contact.GetProperty(nAddressTag[nType][1],pProp)==S_OK) {
		m_strCountry=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	} else {
		m_strCountry="";
	}
	if(contact.GetProperty(nAddressTag[nType][2],pProp)==S_OK) {
		m_strStateOrProvince=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	} else {
		m_strStateOrProvince="";
	}
	if(contact.GetProperty(nAddressTag[nType][3],pProp)==S_OK) {
		m_strStreet=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	} else {
		m_strStreet="";
	}
	if(contact.GetProperty(nAddressTag[nType][4],pProp)==S_OK) {
		m_strPostalCode=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	} else {
		m_strPostalCode="";
	}
*/
#endif //WINCE
}

/////////////////////////////////////////////////////////////
// CMAPIContact

CMAPIContact::CMAPIContact()
{
	m_pUser=NULL;
	m_pMAPI=NULL;
	m_entry.cb=0;
	SetEntryID(NULL);
}

CMAPIContact::~CMAPIContact()
{
	Close();
}

void CMAPIContact::SetEntryID(SBinary* pEntry)
{
	if(m_entry.cb) delete [] m_entry.lpb;
	m_entry.lpb=NULL;

	if(pEntry) {
		m_entry.cb=pEntry->cb;
		if(m_entry.cb) {
			m_entry.lpb=new BYTE[m_entry.cb];
			memcpy(m_entry.lpb,pEntry->lpb,m_entry.cb);
		}
	} else {
		m_entry.cb=0;
	}
}

BOOL CMAPIContact::Open(CMAPIEx* pMAPI,SBinary entry)
{
	Close();
	m_pMAPI=pMAPI;
	ULONG ulObjType;
	if(m_pMAPI->GetSession()->OpenEntry(entry.cb,(LPENTRYID)entry.lpb,NULL,MAPI_BEST_ACCESS,&ulObjType,(LPUNKNOWN*)&m_pUser)!=S_OK) return FALSE;

	SetEntryID(&entry);
	return TRUE;
}

void CMAPIContact::Close()
{
	SetEntryID(NULL);
	RELEASE(m_pUser);
	m_pMAPI=NULL;
}

HRESULT CMAPIContact::GetProperty(ULONG ulProperty,LPSPropValue &prop)
{
#ifndef WINCE
	ULONG ulPropCount;
	ULONG p[2]={ 1,ulProperty };
	return m_pUser->GetProps((LPSPropTagArray)p, CMAPIEx::cm_nMAPICode, &ulPropCount, &prop);
#else
	return 0;
#endif //WINCE
}

void CMAPIContact::GetPropertyString(QString& strProperty,ULONG ulProperty)
{
#ifndef WINCE
	LPSPropValue pProp;
	if(GetProperty(ulProperty,pProp)==S_OK) {
		strProperty=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	}
	else strProperty="";
#endif //WINCE
}

bool CMAPIContact::GetPropertyShort(short int &nValue, ULONG ulProperty)
{
#ifndef WINCE
	LPSPropValue pProp;
	if(GetProperty(ulProperty,pProp)==S_OK) {
		nValue = pProp->Value.i;
		MAPIFreeBuffer(pProp);
		return true;
	}
#endif //WINCE
	return false;
}

void CMAPIContact::GetName(QString& strName,int nNameID)
{
	int i=0,nNameIDs[]={
		PR_DISPLAY_NAME, PR_GIVEN_NAME, PR_MIDDLE_NAME, PR_SURNAME, PR_INITIALS
		-1
	};

	while(nNameIDs[i]!=nNameID && nNameIDs[i]>0) i++;
	if(nNameIDs[i]<0) strName="";
	else GetPropertyString(strName,nNameID);
}

void CMAPIContact::GetEmail(QString& strEmail)
{
#ifndef WINCE
	const GUID guidOutlookEmail1={0x00062004, 0x0000, 0x0000, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46 };
	
	MAPINAMEID nameID;
	nameID.lpguid=(GUID*)&guidOutlookEmail1;
	nameID.ulKind=MNID_ID;
	nameID.Kind.lID=0x8083;

	LPMAPINAMEID lpNameID[1]={ &nameID };
	LPSPropTagArray lppPropTags; 

	if(m_pUser->GetIDsFromNames(1,lpNameID,0,&lppPropTags)==S_OK) {
		LPSPropValue pProp;
		ULONG ulPropCount;
		if(m_pUser->GetProps(lppPropTags, CMAPIEx::cm_nMAPICode, &ulPropCount, &pProp)==S_OK) {
			strEmail=CMAPIEx::GetValidString(*pProp);
			MAPIFreeBuffer(pProp);
		} else {
			strEmail="";
		}
		MAPIFreeBuffer(lppPropTags);
	}
	else strEmail="";
#endif //WINCE
}

void CMAPIContact::GetPhoneNumber(QString& strPhoneNumber,int nPhoneNumberID)
{
	int i=0,nPhoneNumberIDs[]={
		PR_PRIMARY_TELEPHONE_NUMBER, PR_BUSINESS_TELEPHONE_NUMBER, PR_HOME_TELEPHONE_NUMBER, 
		PR_CALLBACK_TELEPHONE_NUMBER, PR_BUSINESS2_TELEPHONE_NUMBER, PR_MOBILE_TELEPHONE_NUMBER,
		PR_RADIO_TELEPHONE_NUMBER, PR_CAR_TELEPHONE_NUMBER, PR_OTHER_TELEPHONE_NUMBER,
		PR_PAGER_TELEPHONE_NUMBER, PR_PRIMARY_FAX_NUMBER, PR_BUSINESS_FAX_NUMBER,
		PR_HOME_FAX_NUMBER, PR_TELEX_NUMBER, PR_ISDN_NUMBER, PR_ASSISTANT_TELEPHONE_NUMBER,
		PR_HOME2_TELEPHONE_NUMBER, PR_TTYTDD_PHONE_NUMBER, PR_COMPANY_MAIN_PHONE_NUMBER,
		-1
	};

	while(nPhoneNumberIDs[i]!=nPhoneNumberID && nPhoneNumberIDs[i]>0) i++;
	if(nPhoneNumberIDs[i]<0) strPhoneNumber="";
	else GetPropertyString(strPhoneNumber,nPhoneNumberID);
}

void CMAPIContact::GetAddress(CContactAddress& address,CContactAddress::AddressType nType)
{
	address.FillAddress(*this,nType);
}

void CMAPIContact::GetAddress(QString& strAddress)
{
	GetPropertyString(strAddress,PR_POSTAL_ADDRESS);
}

void CMAPIContact::GetCompany(QString& strCompany)
{
	GetPropertyString(strCompany,PR_COMPANY_NAME);
}

void CMAPIContact::GetCountry(QString& strCountry)
{
	GetPropertyString(strCountry,PR_COUNTRY);
}

void CMAPIContact::GetCity(QString& strCity)
{
	GetPropertyString(strCity,PR_BUSINESS_ADDRESS_CITY);
}

void CMAPIContact::GetNotes(QString& strNotes,BOOL bRTF)
{
#ifndef WINCE
	strNotes="";
	IStream* pStream;

#ifdef _WIN32_WCE
	const int BUF_SIZE=1024;
#else
	const int BUF_SIZE=16384;
#endif
	char szBuf[BUF_SIZE+1];
	ULONG ulNumChars;

	if(bRTF) {
		if(m_pUser->OpenProperty(PR_RTF_COMPRESSED,&IID_IStream,STGM_READ,0,(LPUNKNOWN*)&pStream)!=S_OK) return;

		IStream *pUncompressed;
		if(WrapCompressedRTFStream(pStream,0,&pUncompressed)==S_OK) {
			do {
				pUncompressed->Read(szBuf,BUF_SIZE,&ulNumChars);
				szBuf[qMin((int)BUF_SIZE,(int)ulNumChars)]=0;
				strNotes+= szBuf;
			} while(ulNumChars>=BUF_SIZE);
			RELEASE(pUncompressed);
		}
	} else {
		if(m_pUser->OpenProperty(PR_BODY,&IID_IStream,STGM_READ,NULL,(LPUNKNOWN*)&pStream)!=S_OK) return;

		do {
			pStream->Read(szBuf,BUF_SIZE*sizeof(TCHAR),&ulNumChars);
			ulNumChars/=sizeof(TCHAR);
			szBuf[qMin((int)BUF_SIZE,(int)ulNumChars)]=0;
			strNotes+= QString::fromUtf16((ushort*)szBuf, ulNumChars);
		} while(ulNumChars>=BUF_SIZE);
	}
	RELEASE(pStream);
#endif //WINCE
}
#endif