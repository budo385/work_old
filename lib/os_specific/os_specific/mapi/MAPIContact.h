#ifndef __MAPICONTACT_H__
#define __MAPICONTACT_H__

#ifdef _WIN32

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File: MAPIContact.h
// Description: MAPI Contact class wrapper
//
// Copyright (C) 2005-2006, Noel Dillabough
//
// This source code is free to use and modify provided this notice remains intact and that any enhancements
// or bug fixes are posted to the CodeProject page hosting this class for the community to benefit.
//
// Usage: see the Codeproject article at http://www.codeproject.com/internet/CMapiEx.asp
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CMAPIEx;
class CMAPIContact;

/////////////////////////////////////////////////////////////
// CContactAddress

class CContactAddress
{
public:
	CContactAddress();

	enum AddressType { BUSINESS, HOME, OTHER, MAX_ADDRESS_TYPES };

// Attributes
public:
	QString m_strStreet;
	QString m_strCity;
	QString m_strStateOrProvince;
	QString m_strPostalCode;
	QString m_strCountry;
	QString m_strTitle;

// Operations
public:
	void FillAddress(CMAPIContact& contact,AddressType nType);
};

/////////////////////////////////////////////////////////////
// CMAPIContact

class CMAPIContact
{
public:
	CMAPIContact();
	~CMAPIContact();

// Attributes
protected:
	QString m_strSubject;

	CMAPIEx* m_pMAPI;
	LPMAILUSER m_pUser;
	SBinary m_entry;

// Operations
public:
	void SetEntryID(SBinary* pEntry=NULL);
	SBinary* GetEntryID() { return &m_entry; }

	BOOL Open(CMAPIEx* pMAPI,SBinary entry);
	void Close();

	void GetPropertyString(QString& strProperty,ULONG ulProperty);
	bool GetPropertyShort(short int &nValue, ULONG ulProperty);
	void GetName(QString& strName,int nNameID=PR_DISPLAY_NAME);
	void GetEmail(QString& strEmail);
	void GetPhoneNumber(QString& strPhoneNumber,int nPhoneNumberID);
	void GetAddress(CContactAddress& address,CContactAddress::AddressType nType);
	void GetAddress(QString& strAddress);
	void GetNotes(QString& strNotes,BOOL bRTF=FALSE);
	void GetCompany(QString& strCompany);
	void GetCountry(QString& strCountry);
	void GetCity(QString& strCity);

	QString& GetSubject() { return m_strSubject; }

protected:
	HRESULT GetProperty(ULONG ulProperty, LPSPropValue &prop);

	friend CContactAddress;
};

#endif //#ifdef _WIN32

#endif
