////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File: MAPIAppointment.cpp
// Description: MAPI Appointment class wrapper
//
// Copyright (C) 2005-2007, Noel Dillabough
//
// This source code is free to use and modify provided this notice remains intact and that any enhancements
// or bug fixes are posted to the CodeProject page hosting this class for the community to benefit.
//
// Usage: see the CodeProject article at http://www.codeproject.com/internet/CMapiEx.asp
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _WIN32
#include "MAPIExPCH.h"
#include "MAPIEx.h"

#define PR_APPOINTMENT_RESPONSE_STATUS PROP_TAG(PT_LONG, 0x8218)
#define respNone		0x00000000
#define respOrganized	0x00000001
#define respTentative	0x00000002
#define respAccepted	0x00000003
#define respDeclined	0x00000004
#define respNotResponded 0x00000005
#define PR_MESSAGE_UID	0x81000003 //PROP_TAG(PT_TSTRING, 0x001A)

//const GUID PS_INTERNET_HEADERS  = {0x00020386, 0x0000, 0x0000, {0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46}};
//const GUID PS_PUBLIC_STRINGS    = {0x00020329, 0x0000, 0x0000, {0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46}};
//const GUID PSETID_Appointment	= {0x00062002, 0x0000, 0x0000, {0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46}};
//const GUID PSETID_Address       = {0x00062004, 0x0000, 0x0000, {0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46}};
//const GUID PSETID_Common        = {0x00062008, 0x0000, 0x0000, {0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46}};
//const GUID PSETID_Log           = {0x0006200A, 0x0000, 0x0000, {0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46}};
const GUID PSETID_Meeting 	= {0x6ED8DA90, 0x450B, 0x101B, {0x98, 0xDA, 0x00, 0xAA, 0x00, 0x3F, 0x13, 0x05}};
//const GUID PSETID_Task          = {0x00062003, 0x0000, 0x0000, {0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46}};

#define LID_GLOBAL_OBJID 0x23

/////////////////////////////////////////////////////////////
// CMAPIAppointment

CMAPIAppointment::CMAPIAppointment()
{
#ifdef _WIN32_WCE
	m_pAppointment=NULL;
#endif
	m_pRecipients = NULL;
	m_pMAPI=NULL;
	m_entry.cb=0;
	m_pItem=NULL;
	SetEntryID(NULL);
}

CMAPIAppointment::~CMAPIAppointment()
{
	Close();
}

void CMAPIAppointment::SetEntryID(SBinary* pEntry)
{
	if(m_entry.cb) delete [] m_entry.lpb;
	m_entry.lpb=NULL;

	if(pEntry) {
		m_entry.cb=pEntry->cb;
		if(m_entry.cb) {
			m_entry.lpb=new BYTE[m_entry.cb];
			memcpy(m_entry.lpb,pEntry->lpb,m_entry.cb);
		}
	} else {
		m_entry.cb=0;
	}
}


#ifdef _WIN32_WCE
BOOL CMAPIAppointment::Open(CMAPIEx* pMAPI, IAppointment* pAppointment)
{
	Close();
	m_pMAPI=pMAPI;
	m_pAppointment=pAppointment;
	return TRUE;
}

void CMAPIAppointment::Close()
{
	RELEASE(m_pAppointment);
	RELEASE(m_pRecipients);
	m_pMAPI=NULL;
}

BOOL CMAPIAppointment::GetPropertyString(ULONG ulProperty, QString& strProperty)
{
	return m_pMAPI->GetPOOM()->GetProperty(m_pAppointment,ulProperty, strProperty);
}

BOOL CMAPIAppointment::SetPropertyString(ULONG ulProperty, LPCTSTR szProperty)
{
	return m_pMAPI->GetPOOM()->SetProperty(m_pAppointment,ulProperty, szProperty);
}

HRESULT CMAPIAppointment::GetProperty(ULONG ulProperty, LPSPropValue& pProp)
{
	return FALSE; //TOFIX
}

// gets a custom outlook property string (ie EmailAddress1 of a contact)
BOOL CMAPIAppointment::GetOutlookPropertyString(ULONG ulData, ULONG ulProperty, QString& strProperty)
{
	return FALSE;
}

BOOL CMAPIAppointment::GetOutlookProperty(ULONG ulData, ULONG ulProperty, LPSPropValue& pProp)
{
	return FALSE;
}

BOOL CMAPIAppointment::GetOutlookPropTagArray(ULONG ulData, ULONG ulProperty, LPSPropTagArray& lppPropTags, int& nFieldType, BOOL bCreate)
{
	return FALSE;
}

BOOL CMAPIAppointment::Save(BOOL bClose)
{
	return FALSE;
}

BOOL CMAPIAppointment::SetOutlookProperty(ULONG ulData, ULONG ulProperty, LPCTSTR szField)
{
	return FALSE;
}

BOOL CMAPIAppointment::SetOutlookProperty(ULONG ulData, ULONG ulProperty, int nField)
{
	return FALSE;
}

BOOL CMAPIAppointment::SetOutlookProperty(ULONG ulData, ULONG ulProperty, FILETIME ftField)
{
	return FALSE;
}

#else

BOOL CMAPIAppointment::Open(CMAPIEx* pMAPI,SBinary entryID)
{
	Close();
	m_pMAPI=pMAPI;
	ULONG ulObjType;
	if(m_pMAPI->GetSession()->OpenEntry(entryID.cb, (LPENTRYID)entryID.lpb, NULL, MAPI_BEST_ACCESS, &ulObjType, (LPUNKNOWN*)&m_pItem)!=S_OK) return FALSE;
	SetEntryID(&entryID);
	return TRUE;
}

void CMAPIAppointment::Close()
{
	SetEntryID(NULL);
	m_pMAPI=NULL;
	RELEASE(m_pItem);
}

HRESULT CMAPIAppointment::GetProperty(ULONG ulProperty, LPSPropValue& pProp)
{
	if(!m_pItem) return E_INVALIDARG;
	ULONG ulPropCount;
	ULONG p[2]={ 1,ulProperty };
	return m_pItem->GetProps((LPSPropTagArray)p, CMAPIEx::cm_nMAPICode, &ulPropCount, &pProp);
}

BOOL CMAPIAppointment::GetPropertyString(ULONG ulProperty, QString& strProperty)
{
	LPSPropValue pProp;
	if(GetProperty(ulProperty, pProp)==S_OK) 
	{
		strProperty=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
		return TRUE;
	} 
	else 
	{
		strProperty="";
		return FALSE;
	}
}

BOOL CMAPIAppointment::SetPropertyString(ULONG ulProperty, LPCTSTR szProperty)
{
	if(szProperty) 
	{
		SPropValue prop;
		prop.ulPropTag=ulProperty;
		prop.Value.LPSZ=(LPTSTR)szProperty;
		return (m_pItem->SetProps(1, &prop, NULL)==S_OK);
	}
	return FALSE;
}

// gets a custom outlook property string (ie EmailAddress1 of a contact)
BOOL CMAPIAppointment::GetOutlookPropertyString(ULONG ulData, ULONG ulProperty, QString& strProperty)
{
	LPSPropValue pProp;
	if(GetOutlookProperty(ulData, ulProperty, pProp)) 
	{
		strProperty=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
		return TRUE;
	} 
	return FALSE;
}

// gets a custom outlook property (ie EmailAddress1 of a contact)
BOOL CMAPIAppointment::GetOutlookProperty(ULONG ulData, ULONG ulProperty, LPSPropValue& pProp)
{
	LPSPropTagArray lppPropTags;
	int nFieldType;
	if(!GetOutlookPropTagArray(ulData,ulProperty, lppPropTags, nFieldType, FALSE)) return FALSE;

	ULONG ulPropCount;
	HRESULT hr=m_pItem->GetProps(lppPropTags, CMAPIEx::cm_nMAPICode, &ulPropCount, &pProp);
	MAPIFreeBuffer(lppPropTags);
	return (hr==S_OK);
}

BOOL CMAPIAppointment::GetOutlookPropTagArray(ULONG ulData, ULONG ulProperty, LPSPropTagArray& lppPropTags, int& nFieldType, BOOL bCreate)
{
	if(!m_pItem) return FALSE;

	const GUID guidOutlookEmail1={ulData, 0x0000, 0x0000, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46 };

	MAPINAMEID nameID;
	nameID.lpguid=(GUID*)&guidOutlookEmail1;
	nameID.ulKind=MNID_ID;
	nameID.Kind.lID=ulProperty;

#ifdef UNICODE
	nFieldType=PT_UNICODE;
#else
	nFieldType=PT_STRING8;
#endif

	LPMAPINAMEID lpNameID[1]={ &nameID };

	HRESULT hr=m_pItem->GetIDsFromNames(1, lpNameID, bCreate ? MAPI_CREATE : 0, &lppPropTags);
	return (hr==S_OK);
}

BOOL CMAPIAppointment::Save(BOOL bClose)
{
	ULONG ulFlags=bClose ? 0 : KEEP_OPEN_READWRITE;
	if(m_pItem && m_pItem->SaveChanges(ulFlags)==S_OK) 
	{
		if(bClose) Close();
		return TRUE;
	}
	return FALSE;
}

BOOL CMAPIAppointment::SetOutlookProperty(ULONG ulData, ULONG ulProperty, LPCTSTR szField)
{
	LPSPropTagArray lppPropTags;
	int nFieldType;
	if(!GetOutlookPropTagArray(ulData,ulProperty, lppPropTags, nFieldType, FALSE)) return FALSE;

	SPropValue prop;
	prop.ulPropTag=(lppPropTags->aulPropTag[0]|nFieldType);
	prop.Value.LPSZ=(LPTSTR)szField;
	HRESULT hr=m_pItem->SetProps(1, &prop, NULL);
	MAPIFreeBuffer(lppPropTags);
	return (hr==S_OK);
}

BOOL CMAPIAppointment::SetOutlookProperty(ULONG ulData, ULONG ulProperty, int nField)
{
	LPSPropTagArray lppPropTags;
	int nFieldType;
	if(!GetOutlookPropTagArray(ulData,ulProperty, lppPropTags, nFieldType, FALSE)) return FALSE;
	nFieldType=PT_LONG;

	SPropValue prop;
	prop.ulPropTag=(lppPropTags->aulPropTag[0]|nFieldType);
	prop.Value.l=nField;
	HRESULT hr=m_pItem->SetProps(1, &prop, NULL);
	MAPIFreeBuffer(lppPropTags);
	return (hr==S_OK);
}

BOOL CMAPIAppointment::SetOutlookProperty(ULONG ulData, ULONG ulProperty, FILETIME ftField)
{
	LPSPropTagArray lppPropTags;
	int nFieldType;
	if(!GetOutlookPropTagArray(ulData,ulProperty, lppPropTags, nFieldType, FALSE)) return FALSE;
	nFieldType=PT_SYSTIME;

	SPropValue prop;
	prop.ulPropTag=(lppPropTags->aulPropTag[0]|nFieldType);
	prop.Value.ft=ftField;
	HRESULT hr=m_pItem->SetProps(1, &prop, NULL);
	MAPIFreeBuffer(lppPropTags);
	return (hr==S_OK);
}

#endif

BOOL CMAPIAppointment::GetSubject(QString& strSubject)
{
#ifndef _WIN32_WCE
	if(GetPropertyString(PR_SUBJECT, strSubject)) return TRUE;
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetLocation(QString& strLocation)
{
#ifndef _WIN32_WCE
	if(GetOutlookPropertyString(OUTLOOK_DATA2, OUTLOOK_APPOINTMENT_LOCATION, strLocation)) return TRUE;
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetStartDate(SYSTEMTIME& tmStart)
{
#ifndef _WIN32_WCE
	LPSPropValue pProp;
	if(GetOutlookProperty(OUTLOOK_DATA2, OUTLOOK_APPOINTMENT_START, pProp)) 
	{
		FILETIME tmLocal;
		FileTimeToLocalFileTime(&pProp->Value.ft, &tmLocal);
		FileTimeToSystemTime(&tmLocal, &tmStart);
		MAPIFreeBuffer(pProp);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetStartDate(QString& strStartDate, LPCTSTR szFormat)
{
#ifndef _WIN32_WCE
	SYSTEMTIME tm;
	if(GetStartDate(tm)) 
	{
		TCHAR szTime[256];
		if(!szFormat) szFormat=L"MM/dd/yyyy hh:mm:ss tt";
		GetDateFormat(LOCALE_SYSTEM_DEFAULT, 0, &tm, szFormat, (LPTSTR)szTime, 256);
		GetTimeFormat(LOCALE_SYSTEM_DEFAULT, 0, &tm, (LPCTSTR)szTime, (LPTSTR)szTime, 256);
		strStartDate = strStartDate.fromUtf16((const ushort *)szTime);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetEndDate(SYSTEMTIME& tmEnd)
{
#ifndef _WIN32_WCE
	LPSPropValue pProp;
	if(GetOutlookProperty(OUTLOOK_DATA2, OUTLOOK_APPOINTMENT_END, pProp)) 
	{
		FILETIME tmLocal;
		FileTimeToLocalFileTime(&pProp->Value.ft, &tmLocal);
		FileTimeToSystemTime(&tmLocal, &tmEnd);
		MAPIFreeBuffer(pProp);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetEndDate(QString& strEndDate, LPCTSTR szFormat)
{
#ifndef _WIN32_WCE
	SYSTEMTIME tm;
	if(GetEndDate(tm)) 
	{
		TCHAR szTime[256];
		if(!szFormat) szFormat=L"MM/dd/yyyy hh:mm:ss tt";
		GetDateFormat(LOCALE_SYSTEM_DEFAULT, 0, &tm, szFormat, (LPTSTR)szTime, 256);
		GetTimeFormat(LOCALE_SYSTEM_DEFAULT, 0, &tm, (LPCTSTR)szTime, (LPTSTR)szTime, 256);
		strEndDate = strEndDate.fromUtf16((const ushort *)szTime);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetBody(QString& strBody)
{
#ifndef _WIN32_WCE
	if(GetPropertyString(PR_BODY, strBody)) return TRUE;
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetResponseStatus(int &nResponseStatus)
{
#ifndef _WIN32_WCE
	LPSPropValue val;
	if(GetProperty(PR_APPOINTMENT_RESPONSE_STATUS, val)){ 
		nResponseStatus = val->Value.i;
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetBodyHtml(QString& strBody)
{
#ifndef _WIN32_WCE
	if(GetPropertyString(PR_BODY_HTML, strBody)) return TRUE;
#endif
	return FALSE;
}

BOOL CMAPIAppointment::SetSubject(const QString& strSubject)
{
#ifndef _WIN32_WCE
	if(SetPropertyString(PR_SUBJECT, (LPCTSTR)strSubject.utf16())){
		Save(FALSE);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::SetLocation(const QString& strLocation)
{
#ifndef _WIN32_WCE
	if(SetOutlookProperty(OUTLOOK_DATA2, OUTLOOK_APPOINTMENT_LOCATION, (LPCTSTR)strLocation.utf16())){
		Save(FALSE);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::SetBody(const QString& strBody)
{
#ifndef _WIN32_WCE
	if(SetPropertyString(PR_BODY, (LPCTSTR)strBody.utf16())){
		Save(FALSE);
		return TRUE;
	}
#endif
	return FALSE;
}
	
BOOL CMAPIAppointment::SetBodyHtml(const QString& strBody)
{
#ifndef _WIN32_WCE
	if(SetPropertyString(PR_BODY_HTML, (LPCTSTR)strBody.utf16())){
		Save(FALSE);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::SetStartDate(const SYSTEMTIME& tmStart)
{
#ifndef _WIN32_WCE
	FILETIME ftStartTime;
	SystemTimeToFileTime(&tmStart, &ftStartTime);
	if(SetOutlookProperty(OUTLOOK_DATA2, OUTLOOK_APPOINTMENT_START, ftStartTime)) 
	{
		Save(FALSE);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::SetEndDate(const SYSTEMTIME& tmEnd)
{
#ifndef _WIN32_WCE
	FILETIME ftEndTime;
	SystemTimeToFileTime(&tmEnd, &ftEndTime);
	if(SetOutlookProperty(OUTLOOK_DATA2, OUTLOOK_APPOINTMENT_END, ftEndTime)) 
	{
		Save(FALSE);
		return TRUE;
	}
#endif
	return FALSE;
}

BOOL CMAPIAppointment::GetRecipients()
{
	if(!m_pItem) return FALSE;
	RELEASE(m_pRecipients);

	if(((LPMESSAGE)m_pItem)->GetRecipientTable(CMAPIEx::cm_nMAPICode, &m_pRecipients)!=S_OK) return FALSE;

	const int nProperties=RECIPIENT_COLS;
	SizedSPropTagArray(nProperties, Columns)={nProperties,{PR_RECIPIENT_TYPE, PR_DISPLAY_NAME, PR_EMAIL_ADDRESS, PR_ADDRTYPE, PR_ENTRYID }};
	return (m_pRecipients->SetColumns((LPSPropTagArray)&Columns, 0)==S_OK);
}

BOOL CMAPIAppointment::GetNextRecipient(QString& strName, QString& strEmail, int& nType, int &nAppResponseStatus)
{
	if(!m_pRecipients) return FALSE;

	LPSRowSet pRows=NULL;
	BOOL bResult=FALSE;
	if(m_pRecipients->QueryRows(1, 0, &pRows)==S_OK) 
	{
		if(pRows->cRows) 
		{
			nType=pRows->aRow[0].lpProps[PROP_RECIPIENT_TYPE].Value.ul;
			strName=CMAPIEx::GetValidString(pRows->aRow[0].lpProps[PROP_RECIPIENT_NAME]);

			// for Microsoft Exchange server internal mails we want to try to resolve the SMTP email address
			QString strAddrType=CMAPIEx::GetValidString(pRows->aRow[0].lpProps[PROP_ADDR_TYPE]);
			if(strAddrType=="EX") 
			{
				if(m_pMAPI) m_pMAPI->GetExEmail(pRows->aRow[0].lpProps[PROP_ENTRYID].Value.bin, strEmail);
			} 
			else 
			{
				strEmail=CMAPIEx::GetValidString(pRows->aRow[0].lpProps[PROP_RECIPIENT_EMAIL]);
			}

			//nAppResponseStatus = pRows->aRow[0].lpProps[5].Value.i; 
			bResult=TRUE;
		}
		FreeProws(pRows);
		MAPIFreeBuffer(pRows);
	}
	return bResult;
}

QDateTime CMAPIAppointment::GetStartDate()
{
	QDateTime tmReceived;

	SYSTEMTIME st;
	if(GetStartDate(st)){
		QTime rcTime(st.wHour, st.wMinute, st.wSecond);
		QDate rcDate(st.wYear, st.wMonth, st.wDay);
		tmReceived = QDateTime(rcDate, rcTime);
	}
	return tmReceived;
}


QDateTime CMAPIAppointment::GetReceivedTime()
{
	QDateTime tmReceived;

	LPSPropValue pProp;
	if(GetProperty(PR_MESSAGE_DELIVERY_TIME,pProp)==S_OK) {
		SYSTEMTIME st;
		::FileTimeToSystemTime(&pProp->Value.ft, &st);
		QTime rcTime(st.wHour, st.wMinute, st.wSecond);
		QDate rcDate(st.wYear, st.wMonth, st.wDay);
		
		tmReceived = QDateTime(rcDate, rcTime);
		MAPIFreeBuffer(pProp);
	}
	return tmReceived;
}

/*
BOOL CMAPIAppointment::Create(CMAPIEx* pMAPI,int nPriority)
{
	if(!pMAPI) return FALSE;
	LPMAPIFOLDER pOutbox=pMAPI->GetFolder();
	if(!pOutbox) pOutbox=pMAPI->OpenOutbox();
	if(!pOutbox || pOutbox->CreateMessage(NULL,0,&m_pItem)!=S_OK) return FALSE;
	m_pMAPI=pMAPI;

    SPropValue prop;
	prop.ulPropTag=PR_MESSAGE_FLAGS;
	prop.Value.l=MSGFLAG_UNSENT | MSGFLAG_FROMME;
	m_pItem->SetProps(1,&prop,NULL);

#ifdef _WIN32_WCE
	// the following uses non outlook attachments (no winmail.dat) so other clients can read mail and attachments
	static const GUID ID_USE_TNEF = {0x00062008,0x000,0x0000,{0xc0,0x00,0x0,0x00,0x00,0x00,0x00,0x46 } };
	MAPINAMEID nameid={(GUID*)&ID_USE_TNEF, MNID_ID, 0x8582};
	MAPINAMEID *rgpnameid[1]={&nameid}; 
	_SPropTagArray* lpSPropTags; 
	HRESULT hr=m_pItem->GetIDsFromNames(1, rgpnameid, MAPI_CREATE, &lpSPropTags);
	unsigned long ulPropTag=lpSPropTags->aulPropTag[0];
	ulPropTag=PROP_TAG(PT_BOOLEAN, PROP_ID(ulPropTag)); 

	prop.ulPropTag=ulPropTag;
	prop.Value.b=FALSE;
	m_pItem->SetProps(1,&prop,NULL);

	prop.ulPropTag=PR_MESSAGE_CLASS;
	prop.Value.lpszA=(TCHAR*)"IPM.Note";
	m_pItem->SetProps(1,&prop,NULL);
#endif

	LPSPropValue props=NULL;
	ULONG cValues=0;
	ULONG rgTags[]={ 1, PR_IPM_SENTMAIL_ENTRYID };

	if(m_pMAPI->GetMessageStore()->GetProps((LPSPropTagArray) rgTags, CMAPIEx::cm_nMAPICode, &cValues, &props)==S_OK) {
		prop.ulPropTag=PR_SENTMAIL_ENTRYID;
		prop.Value.bin.cb=props[0].Value.bin.cb;
		prop.Value.bin.lpb=props[0].Value.bin.lpb;
		m_pItem->SetProps(1,&prop,NULL);
	}

	if(nPriority!=IMPORTANCE_NORMAL) {
		prop.ulPropTag=PR_IMPORTANCE;
		prop.Value.l=nPriority;
		m_pItem->SetProps(1,&prop,NULL);
	}

#ifdef _WIN32_WCE
	prop.ulPropTag=PR_MSG_STATUS;
	prop.Value.ul=MSGSTATUS_RECTYPE_SMTP;
	m_pItem->SetProps(1,&prop,NULL);
#endif

	MAPIFreeBuffer(props);
	return TRUE;
}
/*
BOOL CMAPIAppointment::Send()
{
	if(m_pItem && m_pItem->SubmitMessage(0)==S_OK) {
		Close();
		return TRUE;
	}
	return FALSE;
}

BOOL CMAPIAppointment::AddRecipient(LPCTSTR szEmail,int nType)
{
	if(!m_pItem || !m_pMAPI) return FALSE;

#ifndef _WIN32_WCE
	LPADRBOOK pAddressBook;
	if(m_pMAPI->GetSession()->OpenAddressBook(0, NULL, AB_NO_DIALOG, &pAddressBook)!=S_OK) return FALSE;
#endif

	int nBufSize=CbNewADRLIST(1);
	LPADRLIST pAddressList=NULL;
	MAPIAllocateBuffer(nBufSize,(LPVOID FAR*)&pAddressList);
	memset(pAddressList,0,nBufSize);

	const int nProperties=2;
	pAddressList->cEntries=1;

	pAddressList->aEntries[0].ulReserved1=0;
	pAddressList->aEntries[0].cValues=nProperties;

	MAPIAllocateBuffer(sizeof(SPropValue)*nProperties,(LPVOID FAR*)&pAddressList->aEntries[0].rgPropVals);
	memset(pAddressList->aEntries[0].rgPropVals, 0, sizeof(SPropValue)*nProperties);

	pAddressList->aEntries[0].rgPropVals[0].ulPropTag=PR_RECIPIENT_TYPE;
	pAddressList->aEntries[0].rgPropVals[0].Value.ul=nType;

#ifdef _WIN32_WCE
	pAddressList->aEntries[0].rgPropVals[1].ulPropTag=PR_EMAIL_ADDRESS;
	pAddressList->aEntries[0].rgPropVals[1].Value.LPSZ=(TCHAR*)szEmail;
#else
	pAddressList->aEntries[0].rgPropVals[1].ulPropTag=PR_DISPLAY_NAME;
	pAddressList->aEntries[0].rgPropVals[1].Value.LPSZ=(TCHAR*)szEmail;
#endif

#ifdef _WIN32_WCE
	HRESULT hr=m_pItem->ModifyRecipients(MODRECIP_ADD, pAddressList);
#else
	HRESULT hr=E_INVALIDARG;
	if(pAddressBook->ResolveName(0, CMAPIEx::cm_nMAPICode, NULL, pAddressList)==S_OK) hr=m_pItem->ModifyRecipients(MODRECIP_ADD, pAddressList);
#endif
	MAPIFreeBuffer(pAddressList->aEntries[0].rgPropVals);
	MAPIFreeBuffer(pAddressList);
#ifndef _WIN32_WCE
	RELEASE(pAddressBook);
#endif
	return (hr==S_OK);
}
*/


bool CMAPIAppointment::GetMessageClass(QString& strMessageClass)
{
	LPSPropValue pProp;
	if(GetProperty(PR_MESSAGE_CLASS,pProp)==S_OK) {
		strMessageClass=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
		return true;
	}
	return false;
}

bool CMAPIAppointment::GetVCalendarUID(QString& strUID)
{
	strUID="";

	// Set up the request to GetIDsFromNames.
	MAPINAMEID NamedID = {0};
	NamedID.lpguid = (LPGUID) &PSETID_Meeting;
	NamedID.ulKind = MNID_ID;
	NamedID.Kind.lID = LID_GLOBAL_OBJID;
	LPMAPINAMEID lpNamedID = &NamedID;

	// Find the prop tag
	LPSPropTagArray lpNamedPropTags = NULL;
	HRESULT hr = m_pItem->GetIDsFromNames(1, &lpNamedID, NULL, &lpNamedPropTags);
	if(SUCCEEDED(hr) && lpNamedPropTags)
	{
		// Set our type to binary 
		lpNamedPropTags->aulPropTag[0] = 
		   CHANGE_PROP_TYPE(lpNamedPropTags->aulPropTag[0],PT_BINARY); 

		// Get the value of the property. 
		LPSPropValue lpPropVal = NULL;
		ULONG ulVal = 0;
		hr = m_pItem->GetProps(lpNamedPropTags, 0, &ulVal, &lpPropVal);
		if(SUCCEEDED(hr)){
			// conver binary ID to hex string
			const char hex2char[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};   
			char szBuffer1[10];
			int nNumBytes = (*lpPropVal).Value.bin.cb;
			for(int i=0; i<nNumBytes; i++){
				unsigned char b = (*lpPropVal).Value.bin.lpb[i];
				int nNibbleHigh = (b & 0xF0) >> 4;
				int nNibbleLow  = (b & 0x0F);
				sprintf(szBuffer1, "%c%c", hex2char[nNibbleHigh], hex2char[nNibbleLow]);
				strUID += szBuffer1;
			}
		}
		MAPIFreeBuffer(lpPropVal); 
	}
	MAPIFreeBuffer(lpNamedPropTags);
	
	return SUCCEEDED(hr);
}
#endif