////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File: MAPIMessage.cpp
// Description: MAPI Message class wrapper
//
// Copyright (C) 2005-2006, Noel Dillabough
//
// This source code is free to use and modify provided this notice remains intact and that any enhancements
// or bug fixes are posted to the CodeProject page hosting this class for the community to benefit.
//
// Usage: see the Codeproject article at http://www.codeproject.com/internet/CMapiEx.asp
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
#ifndef WINCE

#include "MAPIExPCH.h"
#include "MAPIEx.h"
#include <TCHAR.H>
#include <QFile>
#include <QDebug>
#include <ExchForm.h>
#include <QChar>
//#include <MapiTags.h>

#ifndef PR_ATTACH_CONTENT_ID
 #define PR_ATTACH_CONTENT_ID PROP_TAG(PT_TSTRING, 0x3712)
#endif

/////////////////////////////////////////////////////////////
// CMAPIMessage

CMAPIMessage::CMAPIMessage()
{
	m_pMessage=NULL;
	m_nAttachments=0;
	m_pMAPI=NULL;
	m_entry.cb=0;
	SetEntryID(NULL);
	m_pRecipients = NULL;
}

CMAPIMessage::CMAPIMessage(CMAPIMessage& message)
{
	m_pMessage=NULL;
	m_nAttachments=0;
	m_pMAPI=NULL;
	m_pRecipients=NULL;
	m_entry.cb=0;
	*this=message;
}

CMAPIMessage::~CMAPIMessage()
{
	Close();
}

BOOL CMAPIMessage::IsIncoming() 
{ 
	//TOFIX more checks? MSGFLAG_SUBMIT, MSGFLAG_UNSENT, MSGFLAG_RESEND, MSGFLAG_FROMME
	//return ((m_dwMessageFlags & (MSGFLAG_ORIGIN_INTERNET|MSGFLAG_ORIGIN_MISC_EXT|MSGFLAG_ORIGIN_X400)) != 0); 
	return ((m_dwMessageFlags & (MSGFLAG_SUBMIT|MSGFLAG_UNSENT|MSGFLAG_RESEND)) == 0);
}

void CMAPIMessage::SetEntryID(SBinary* pEntry)
{
	if(m_entry.cb) delete [] m_entry.lpb;
	m_entry.lpb=NULL;

	if(pEntry) {
		m_entry.cb=pEntry->cb;
		if(m_entry.cb) {
			m_entry.lpb=new BYTE[m_entry.cb];
			memcpy(m_entry.lpb,pEntry->lpb,m_entry.cb);
		}
	} else {
		m_entry.cb=0;
	}
}

BOOL CMAPIMessage::Open(CMAPIEx* pMAPI,SBinary entry,DWORD dwMessageFlags)
{
	Close();
	m_pMAPI=pMAPI;
	m_dwMessageFlags=dwMessageFlags;
	ULONG ulObjType;
	if( !m_pMAPI ||
		!m_pMAPI->GetSession() ||
		 m_pMAPI->GetSession()->OpenEntry(entry.cb,(LPENTRYID)entry.lpb,NULL,MAPI_BEST_ACCESS,&ulObjType,(LPUNKNOWN*)&m_pMessage)!=S_OK) 
		 return FALSE;

	SetEntryID(&entry);
	FillSenderName();
	FillSenderEmail();
	FillSubject();
	FillAttachmentCount();
	FillReceivedTime();
	
	//fill message if not filled already
	if(-1 == dwMessageFlags)
	{
		LPSPropValue pProp;
		if(GetProperty(PR_MESSAGE_FLAGS,pProp)==S_OK) {
			m_dwMessageFlags=pProp->Value.ul;
			MAPIFreeBuffer(pProp);
		}
	}

	return TRUE;
}

void CMAPIMessage::FillAttachmentCount()
{
	BOOL bHasAttachments=FALSE;
	LPSPropValue pProp;
	if(GetProperty(PR_HASATTACH,pProp)==S_OK) {
		bHasAttachments=pProp->Value.b;
		MAPIFreeBuffer(pProp);
	}

	if(bHasAttachments) {
		LPMAPITABLE pAttachTable=NULL;
		if(m_pMessage->GetAttachmentTable(0,&pAttachTable)!=S_OK) return;

		ULONG ulCount;
		if(pAttachTable->GetRowCount(0,&ulCount)==S_OK) {
			m_nAttachments=ulCount;
		}
		RELEASE(pAttachTable);
	}
}

void CMAPIMessage::Close()
{
	SetEntryID(NULL);
	RELEASE(m_pMessage);
	m_nAttachments=0;
	m_pMAPI=NULL;
	m_strBody="";
	m_strRTF="";
}

HRESULT CMAPIMessage::GetProperty(ULONG ulProperty,LPSPropValue &prop)
{
	ULONG ulPropCount;
	ULONG p[2]={ 1,ulProperty };
	return m_pMessage->GetProps((LPSPropTagArray)p, CMAPIEx::cm_nMAPICode, &ulPropCount, &prop);
}

QString& CMAPIMessage::GetBody()
{
	if(m_strBody.isEmpty()) FillBody();
	return m_strBody;
}

QString& CMAPIMessage::GetRTF()
{
	if(m_strRTF.isEmpty()) 
	{
		FillRTF();
		//qDebug() << "RTF source: " << m_strRTF;

		// does this RTF contain encoded HTML? If so decode it
		// code taken from Lucian Wischik's example at http://www.wischik.com/lu/programmer/mapi_utils.html
		if(m_strRTF.indexOf("\\fromhtml") != -1)
		{
			QString strTmp = m_strRTF;

			// scan to <html tag
			// Ignore { and }. These are part of RTF markup.
			// Ignore \htmlrtf...\htmlrtf0. This is how RTF keeps its equivalent markup separate from the html.
			// Ignore \r and \n. The real carriage returns are stored in \par tags.
			// Ignore \pntext{..} and \liN and \fi-N. These are RTF junk.
			// Convert \par and \tab into \r\n and \t
			// Convert \'XX into the ascii character indicated by the hex number XX
			// Convert \{ and \} into { and }. This is how RTF escapes its curly braces.
			// When we get \*\mhtmltagN, keep the tag, but ignore the subsequent \*\htmltagN
			// When we get \*\htmltagN, keep the tag as long as it isn't subsequent to a \*\mhtmltagN
			// All other text should be kept as it is.

			int nIdx = 0;
			int nPos = strTmp.indexOf("<html");
			if(nPos >= 0)
				nIdx = nPos;

			int nTotal = strTmp.length();

			QString strHTML;
			int nTag=-1,nIgnoreTag=-1;
			while(nTotal > nIdx) 
			{
				if(strTmp.at(nIdx)==(QChar)'{') nIdx++;
				else if(strTmp.at(nIdx)==(TCHAR)'}') nIdx++;
				else if(strTmp.at(nIdx)==(TCHAR)'\r' || strTmp.at(nIdx)==(TCHAR)'\n') nIdx++;
				else if(strTmp.mid(nIdx, 10) == QString("\\*\\htmltag")) {
					nIdx += 10;
					nTag=0;
					while(strTmp.at(nIdx)>=(QChar)'0' && strTmp.at(nIdx)<=(QChar)'9') {
						nTag=nTag*10 + strTmp.at(nIdx).toLatin1()- '0';
						nIdx++;
					}
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
					if(nTag==nIgnoreTag) {
						while(nTotal > nIdx) {
							if(strTmp.at(nIdx)==(TCHAR)'}') break;
							nIdx++;
						}
						nIgnoreTag=-1;
					}
				} else if(strTmp.mid(nIdx, 11) == QString("\\*\\mhtmltag")) { 
					nIdx += 11;
					while(strTmp.at(nIdx)>=(TCHAR)'0' && strTmp.at(nIdx)<=(TCHAR)'9') {
						nTag=nTag*10+strTmp.at(nIdx).toLatin1()-'0';
						nIdx++;
					}
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
					nIgnoreTag=nTag;
				} else if(strTmp.mid(nIdx, 4) == QString("\\par")) {
					strHTML+="\r\n";
					nIdx += 4;
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
				} else if(strTmp.mid(nIdx, 4) == QString("\\tab")) {
					strHTML+="\t";
					nIdx += 4;
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
				} else if(strTmp.mid(nIdx, 3) == QString("\\li")) { 
					nIdx += 3;
					while(strTmp.at(nIdx)>=(TCHAR)'0' && strTmp.at(nIdx)<=(TCHAR)'9') nIdx++; 
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
				} else if(strTmp.mid(nIdx, 4) == QString("\\fi-")) { 
					nIdx += 4;
					while(strTmp.at(nIdx)>=(TCHAR)'0' && strTmp.at(nIdx)<=(TCHAR)'9') nIdx++;
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
				} else if(strTmp.mid(nIdx, 2) == QString("\\'")) { 
					char hi=strTmp.at(nIdx+2).toLatin1(),lo=strTmp.at(nIdx+3).toLatin1();
					if(hi>='0' && hi<='9') hi-='0'; else if(hi>='A' && hi<='Z') hi=hi-'A'+10; else if(hi>='a' && hi<='z') hi=hi-'a'+10;
					if(lo>='0' && lo<='9') lo-='0'; else if(lo>='A' && lo<='Z') lo=lo-'A'+10; else if(lo>='a' && lo<='z') lo=lo-'a'+10;
					strHTML+=(char)(hi*16+lo);
					nIdx += 4;
				} else if(strTmp.mid(nIdx, 7) == QString("\\pntext")) {
					nIdx += 7;
					while(nTotal > nIdx) {
						if(strTmp.at(nIdx)==(QChar)'}') break;
						nIdx++;
					}
				} else if(strTmp.mid(nIdx, 8) == QString("\\htmlrtf")) {
					nIdx += 8;
					while(nTotal > nIdx) {
						if(strTmp.mid(nIdx, 9) == QString("\\htmlrtf0")) {
							nIdx += 9;
							if(strTmp.at(nIdx)==(QChar)' ') nIdx++;
							break;
						}
						nIdx++;
					}
				} else if(strTmp.mid(nIdx, 2) == QString("\\{")) { 
					strHTML+='{';
					nIdx += 2;
				} else if(strTmp.mid(nIdx, 2) == QString("\\}")) { 
					strHTML+='}';
					nIdx += 2;
				} else {
					strHTML+=strTmp.at(nIdx);
					nIdx ++;
				}
			}

			//strip rtf tags before the start
			nPos = strHTML.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive);
			if(nPos < 0)
				nPos = strHTML.indexOf("<html", 0, Qt::CaseInsensitive);
			if(nPos > 0)
				strHTML = strHTML.right(strHTML.length()-nPos);

			//back slash characters are escaped in RTF? (unescape)
			strHTML.replace("\\\\", "\\");

	#ifdef _DEBUG
	#if 0
			QFile file("D:\\rtf_src.txt");
			if(file.open(QIODevice::WriteOnly))
				file.write(QByteArray().append(m_strRTF));

			QFile file2("D:\\htm_dst.txt");
			if(file2.open(QIODevice::WriteOnly))
				file2.write(QByteArray().append(strHTML));

	#endif
	#endif
			m_strRTF=strHTML;
		}
		else if(m_strRTF.indexOf("\\fromtext") != -1)
		{
			// this is not HTML code, it is a plain text wrapped in RTF
			QString strTmp = m_strRTF;
			
			int nIdx = 0;
			int nTotal = strTmp.length();

			QString strHTML;
			int nTag=-1,nIgnoreTag=-1;
			while(nTotal > nIdx) 
			{
				if(strTmp.at(nIdx)==(QChar)'{') nIdx++;
				else if(strTmp.at(nIdx)==(TCHAR)'}') nIdx++;
				else if(strTmp.at(nIdx)==(TCHAR)'\r' || strTmp.at(nIdx)==(TCHAR)'\n') nIdx++;
				else if(strTmp.mid(nIdx, 4) == QString("\\par")) {
					strHTML+="\r\n";
					nIdx += 4;
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
				} else if(strTmp.mid(nIdx, 4) == QString("\\tab")) {
					strHTML+="\t";
					nIdx += 4;
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
				} else if(strTmp.mid(nIdx, 3) == QString("\\li")) { 
					nIdx += 3;
					while(strTmp.at(nIdx)>=(TCHAR)'0' && strTmp.at(nIdx)<=(TCHAR)'9') nIdx++; 
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
				} else if(strTmp.mid(nIdx, 4) == QString("\\fi-")) { 
					nIdx += 4;
					while(strTmp.at(nIdx)>=(TCHAR)'0' && strTmp.at(nIdx)<=(TCHAR)'9') nIdx++;
					if(strTmp.at(nIdx)==(TCHAR)' ') nIdx++;
				} else if(strTmp.mid(nIdx, 2) == QString("\\'")) { 
					char hi=strTmp.at(nIdx+2).toLatin1(),lo=strTmp.at(nIdx+3).toLatin1();
					if(hi>='0' && hi<='9') hi-='0'; else if(hi>='A' && hi<='Z') hi=hi-'A'+10; else if(hi>='a' && hi<='z') hi=hi-'a'+10;
					if(lo>='0' && lo<='9') lo-='0'; else if(lo>='A' && lo<='Z') lo=lo-'A'+10; else if(lo>='a' && lo<='z') lo=lo-'a'+10;
					strHTML+=(char)(hi*16+lo);
					nIdx += 4;
				} else if(strTmp.mid(nIdx, 7) == QString("\\pntext")) {
					nIdx += 7;
					while(nTotal > nIdx) {
						if(strTmp.at(nIdx)==(QChar)'}') break;
						nIdx++;
					}
				} else if(strTmp.mid(nIdx, 2) == QString("\\{")) { 
					strHTML+='{';
					nIdx += 2;
				} else if(strTmp.mid(nIdx, 2) == QString("\\}")) { 
					strHTML+='}';
					nIdx += 2;
				} else {
					strHTML+=strTmp.at(nIdx);
					nIdx ++;
				}
			}

			m_strRTF = strHTML;
		}

		//FIX: quick patch to fix text based email (strip RTF header)
		if(m_strRTF.indexOf("\\rtf1") == 0){
			int nPos = m_strRTF.indexOf("\\fs20 ");
			if(nPos > 0)
				m_strRTF = m_strRTF.right(m_strRTF.length()-nPos-6);
			else{
				nPos = m_strRTF.indexOf("\\fs24 ");
				if(nPos > 0)
					m_strRTF = m_strRTF.right(m_strRTF.length()-nPos-6);
			}
		}
	}

	return m_strRTF;
}

bool CMAPIMessage::GetStringProperty(QString &strData, int nProperty)
{
	LPSPropValue pProp;
	if(GetProperty(nProperty,pProp)==S_OK) {
		strData = CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
		return true;
	}
	else strData="";

	return false;
}

void CMAPIMessage::FillSenderName()
{
	LPSPropValue pProp;
	if(GetProperty(PR_SENDER_NAME,pProp)==S_OK) {
		m_strSenderName = CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	}
	else m_strSenderName="";
}

void CMAPIMessage::FillSenderEmail()
{
	QString strAddrType;
	LPSPropValue pProp;
	if(GetProperty(PR_SENDER_ADDRTYPE,pProp)==S_OK) {
		strAddrType=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	}

	if(GetProperty(PR_SENDER_EMAIL_ADDRESS,pProp)==S_OK) {
		m_strSenderEmail=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	}
	else m_strSenderEmail=_T("");

#ifndef _WIN32_WCE
	// for Microsoft Exchange server internal mails we want to try to resolve the SMTP email address
	if(strAddrType=="EX") {
		if(GetProperty(PR_SENDER_ENTRYID,pProp)==S_OK) {
			LPADRBOOK pAddressBook;
			if(m_pMAPI->GetSession()->OpenAddressBook(0, NULL, AB_NO_DIALOG, &pAddressBook)==S_OK) {
				ULONG ulObjType;
				IMailUser* pMailUser;
				if(pAddressBook->OpenEntry(pProp->Value.bin.cb,(ENTRYID*)pProp->Value.bin.lpb,NULL,MAPI_BEST_ACCESS,&ulObjType,(LPUNKNOWN*)&pMailUser)==S_OK) {
					if(ulObjType==MAPI_MAILUSER) {
						MAPIFreeBuffer(pProp);
						ULONG ulPropCount;
						ULONG p[2]={ 1,PR_SMTP_ADDRESS };
						if(pMailUser->GetProps((LPSPropTagArray)p, CMAPIEx::cm_nMAPICode, &ulPropCount, &pProp)==S_OK) {
							m_strSenderEmail=CMAPIEx::GetValidString(*pProp);
						}
					}
					RELEASE(pMailUser);
				}
				RELEASE(pAddressBook);
			}
			MAPIFreeBuffer(pProp);
		}
	}
#endif
}

void CMAPIMessage::FillSubject()
{
	LPSPropValue pProp;
	if(GetProperty(PR_SUBJECT,pProp)==S_OK) {
		m_strSubject=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
	}
	else m_strSubject=_T("");
}

void CMAPIMessage::FillRTF()
{
	m_strRTF=_T("");
	IStream* pStream;
	if(m_pMessage->OpenProperty(PR_RTF_COMPRESSED,&IID_IStream,STGM_READ,0,(LPUNKNOWN*)&pStream)!=S_OK) return;

#ifdef _WIN32_WCE
	const int BUF_SIZE=1024;
#else
	const int BUF_SIZE=16384;
#endif
	char szBuf[BUF_SIZE+1];
	ULONG ulNumChars;

	IStream *pUncompressed;
	if(WrapCompressedRTFStream(pStream,0,&pUncompressed)==S_OK) {
		do {
			pUncompressed->Read(szBuf,BUF_SIZE,&ulNumChars);
			szBuf[qMin((int)BUF_SIZE,(int)ulNumChars)]=0;
			m_strRTF+=szBuf;
		} while(ulNumChars>=BUF_SIZE);
		RELEASE(pUncompressed);
	}

	RELEASE(pStream);
}

void CMAPIMessage::FillBody()
{
	m_strBody=_T("");
	IStream* pStream;
	if(m_pMessage->OpenProperty(PR_BODY,&IID_IStream,STGM_READ,NULL,(LPUNKNOWN*)&pStream)!=S_OK) return;

#ifdef _WIN32_WCE
	const int BUF_SIZE=1024;
#else
	const int BUF_SIZE=16384;
#endif
	TCHAR szBuf[BUF_SIZE+1];
	ULONG ulNumChars;
	
	do {
		pStream->Read(szBuf,BUF_SIZE*sizeof(TCHAR),&ulNumChars);
		ulNumChars/=sizeof(TCHAR);
		szBuf[qMin((int)BUF_SIZE,(int)ulNumChars)]=0;
		m_strBody+= QByteArray((const char *)szBuf, qMin((int)BUF_SIZE,(int)ulNumChars));
	} while(ulNumChars>=BUF_SIZE);

	RELEASE(pStream);
}

void  CMAPIMessage::FillReceivedTime()
{
	LPSPropValue pProp;
	if(GetProperty(PR_MESSAGE_DELIVERY_TIME,pProp)==S_OK) {
		SYSTEMTIME st;
		::FileTimeToSystemTime(&pProp->Value.ft, &st);
		QTime rcTime(st.wHour, st.wMinute, st.wSecond);
		QDate rcDate(st.wYear, st.wMonth, st.wDay);
		
		m_tmReceived = QDateTime(rcDate, rcTime);
		MAPIFreeBuffer(pProp);
	}
}

QString CMAPIMessage::GetReceivedTime(LPCTSTR szFormat)
{
	static QString strTime;
	QString strFormat=szFormat ? (const char*)szFormat : "%m/%d/%Y %I:%M %p";
	strTime=m_tmReceived.toString(strFormat);
	return strTime;
}

QString CMAPIMessage::GetAttachmentName(int nIndex)
{
	static QString strAttachmentName;
	strAttachmentName="";

	LPMAPITABLE pAttachTable=NULL;
	if(m_pMessage->GetAttachmentTable(0,&pAttachTable)==S_OK)
	{
		enum { PROP_ATTACH_LONG_FILENAME, PROP_ATTACH_FILENAME, ATTACH_COLS };
		static SizedSPropTagArray(ATTACH_COLS,Columns)={ATTACH_COLS, PR_ATTACH_LONG_FILENAME, PR_ATTACH_FILENAME };
		ULONG ulCount;
		if(pAttachTable->SetColumns((LPSPropTagArray)&Columns,0)==S_OK &&
			pAttachTable->GetRowCount(0,&ulCount)==S_OK && nIndex < (int)ulCount)
		{
			LPSRowSet pRows=NULL;
			if(pAttachTable->QueryRows(ulCount,0,&pRows)!=S_OK) MAPIFreeBuffer(pRows);
			else {
				if(nIndex < (int)pRows->cRows) {
					if(!CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_LONG_FILENAME]).isEmpty()) 
						strAttachmentName = CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_LONG_FILENAME]); //pRows->aRow[nIndex].lpProps[PROP_ATTACH_LONG_FILENAME].Value.lpszW;
					else if(!CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_FILENAME]).isEmpty())
						strAttachmentName = CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_FILENAME]); //pRows->aRow[nIndex].lpProps[PROP_ATTACH_FILENAME].Value.lpszW;
					else 
						strAttachmentName="";
				}
				FreeProws(pRows);
			}
		}
		RELEASE(pAttachTable);
	}
	return strAttachmentName;
}

QString CMAPIMessage::GetAttachmentCID(int nIndex)
{
	/*
	static QString strAttachmentCID;
	strAttachmentCID="";

	LPMAPITABLE pAttachTable=NULL;
	if(m_pMessage->GetAttachmentTable(0,&pAttachTable)==S_OK)
	{
		enum { PROP_ATTACH_CONTENT_ID, ATTACH_COLS };
		static SizedSPropTagArray(ATTACH_COLS,Columns)={ATTACH_COLS, PR_ATTACH_CONTENT_ID };
		ULONG ulCount;
		if(pAttachTable->SetColumns((LPSPropTagArray)&Columns,0)==S_OK &&
			pAttachTable->GetRowCount(0,&ulCount)==S_OK && nIndex < (int)ulCount)
		{
			LPSRowSet pRows=NULL;
			if(pAttachTable->QueryRows(ulCount,0,&pRows)!=S_OK) 
				MAPIFreeBuffer(pRows);
			else {
				if(nIndex < (int)pRows->cRows) {
					if(!CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_CONTENT_ID]).isEmpty()) 
						strAttachmentCID = CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_CONTENT_ID]);
					else 
						strAttachmentCID="";
				}
				FreeProws(pRows);
			}
		}
		RELEASE(pAttachTable);
	}
	return strAttachmentCID;
	*/
	
	QString strAttachmentCID;
	strAttachmentCID="";

	LPMAPITABLE pAttachTable=NULL;
	if(m_pMessage->GetAttachmentTable(0, &pAttachTable)==S_OK)
	{
		ULONG ulCount=GetAttachmentCount();
		if(nIndex<(int)ulCount) 
		{
			enum { PROP_ATTACH_CONTENT_ID, ATTACH_COLS };
			static SizedSPropTagArray(ATTACH_COLS, Columns)={ATTACH_COLS, PR_ATTACH_CONTENT_ID };
			if(pAttachTable->SetColumns((LPSPropTagArray)&Columns, 0)==S_OK)
			{
				LPSRowSet pRows=NULL;
				if(pAttachTable->QueryRows(ulCount, 0, &pRows)!=S_OK) MAPIFreeBuffer(pRows);
				else {
					if(nIndex < (int)pRows->cRows) 
					{
						if(!CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_CONTENT_ID]).isEmpty()) 
							strAttachmentCID=CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_CONTENT_ID]);
					}
					FreeProws(pRows);
				}
			}
		}
		RELEASE(pAttachTable);
	}
	return strAttachmentCID;
}

BOOL CMAPIMessage::StoreAttachment(LPATTACH pAttachment,QByteArray &strData)
{
	IStream* pStream;
	if(pAttachment->OpenProperty(PR_ATTACH_DATA_BIN,&IID_IStream,STGM_READ,NULL,(LPUNKNOWN*)&pStream)!=S_OK) {
		return FALSE;
	}

	const int BUF_SIZE=4096;
	BYTE b[BUF_SIZE];
	ULONG ulRead;

	do {
		pStream->Read(&b,BUF_SIZE,&ulRead);
		strData.append(QByteArray((const char *)b,ulRead));
	} while(ulRead>=BUF_SIZE);

	RELEASE(pStream);
	return TRUE;
}

BOOL CMAPIMessage::SaveAttachment(LPATTACH pAttachment,QString strPath)
{
	QFile file(strPath);
	if(!file.open(QIODevice::WriteOnly)) return FALSE;

	IStream* pStream;
	if(pAttachment->OpenProperty(PR_ATTACH_DATA_BIN,&IID_IStream,STGM_READ,NULL,(LPUNKNOWN*)&pStream)!=S_OK) {
		file.close();
		return FALSE;
	}

	const int BUF_SIZE=4096;
	BYTE b[BUF_SIZE];
	ULONG ulRead;

	do {
		pStream->Read(&b,BUF_SIZE,&ulRead);
		if(ulRead) file.write((const char *)b,ulRead);
	} while(ulRead>=BUF_SIZE);

	file.close();
	RELEASE(pStream);
	return TRUE;
}

QByteArray CMAPIMessage::GetAttachmentData(int nIndex)
{
	QByteArray result;

	LPMAPITABLE pAttachTable=NULL;
	if(m_pMessage->GetAttachmentTable(0,&pAttachTable)!=S_OK) return result;

	QString strPath;
	BOOL bResult=FALSE;
	enum { PROP_ATTACH_NUM, PROP_ATTACH_LONG_FILENAME, PROP_ATTACH_FILENAME, ATTACH_COLS };
	static SizedSPropTagArray(ATTACH_COLS,Columns)={ATTACH_COLS, PR_ATTACH_NUM, PR_ATTACH_LONG_FILENAME, PR_ATTACH_FILENAME };
	if(pAttachTable->SetColumns((LPSPropTagArray)&Columns,0)==S_OK) {
		int i=0;
		LPSRowSet pRows=NULL;
		while(TRUE) {
			if(pAttachTable->QueryRows(1,0,&pRows)!=S_OK) MAPIFreeBuffer(pRows);
			else if(!pRows->cRows) FreeProws(pRows);
			else if(i<nIndex) {
				i++;
				continue;
			} else {
				LPATTACH pAttachment;
				if(m_pMessage->OpenAttach(pRows->aRow[0].lpProps[PROP_ATTACH_NUM].Value.bin.cb,NULL,0,&pAttachment)==S_OK) 
				{
					if(!StoreAttachment(pAttachment, result)) {
						pAttachment->Release();
						FreeProws(pRows);
						RELEASE(pAttachTable);
						return FALSE;
					}
					bResult=TRUE;
					pAttachment->Release();
				}

				FreeProws(pRows);
				//if(nIndex==-1) continue;
			}
			break;
		}
	}
	RELEASE(pAttachTable);
	return result;
}

// use nIndex of -1 to save all attachments to szFolder
BOOL CMAPIMessage::SaveAttachments(LPCTSTR szFolder,int nIndex)
{
	LPMAPITABLE pAttachTable=NULL;
	if(m_pMessage->GetAttachmentTable(0,&pAttachTable)!=S_OK) return FALSE;

	QString strPath;
	BOOL bResult=FALSE;
	enum { PROP_ATTACH_NUM, PROP_ATTACH_LONG_FILENAME, PROP_ATTACH_FILENAME, ATTACH_COLS };
	static SizedSPropTagArray(ATTACH_COLS,Columns)={ATTACH_COLS, PR_ATTACH_NUM, PR_ATTACH_LONG_FILENAME, PR_ATTACH_FILENAME };
	if(pAttachTable->SetColumns((LPSPropTagArray)&Columns,0)==S_OK) {
		int i=0;
		LPSRowSet pRows=NULL;
		while(TRUE) {
			if(pAttachTable->QueryRows(1,0,&pRows)!=S_OK) MAPIFreeBuffer(pRows);
			else if(!pRows->cRows) FreeProws(pRows);
			else if(i<nIndex) {
				i++;
				continue;
			} else {
				LPATTACH pAttachment;
				if(m_pMessage->OpenAttach(pRows->aRow[0].lpProps[PROP_ATTACH_NUM].Value.bin.cb,NULL,0,&pAttachment)==S_OK) {
					if(!CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_LONG_FILENAME]).isEmpty()) strPath.sprintf("%s\\%s",szFolder,pRows->aRow[0].lpProps[PROP_ATTACH_LONG_FILENAME].Value.LPSZ);
					else if(!CMAPIEx::GetValidString(pRows->aRow[nIndex].lpProps[PROP_ATTACH_FILENAME]).isEmpty()) strPath.sprintf("%s\\%s",szFolder,pRows->aRow[0].lpProps[PROP_ATTACH_FILENAME].Value.LPSZ);
					else strPath.sprintf("%s\\Attachment.dat", szFolder);
						
					if(!SaveAttachment(pAttachment, strPath)) {
						pAttachment->Release();
						FreeProws(pRows);
						RELEASE(pAttachTable);
						return FALSE;
					}
					bResult=TRUE;
					pAttachment->Release();
				}

				FreeProws(pRows);
				if(nIndex==-1) continue;
			}
			break;
		}
	}
	RELEASE(pAttachTable);
	return bResult;
}

// use nIndex of -1 to delete all attachments
BOOL CMAPIMessage::DeleteAttachments(int nIndex)
{
	LPMAPITABLE pAttachTable=NULL;
	if(m_pMessage->GetAttachmentTable(0,&pAttachTable)!=S_OK) return FALSE;

	BOOL bResult=FALSE;
	enum { PROP_ATTACH_NUM, ATTACH_COLS };
	static SizedSPropTagArray(ATTACH_COLS,Columns)={ATTACH_COLS, PR_ATTACH_NUM };
	if(pAttachTable->SetColumns((LPSPropTagArray)&Columns,0)==S_OK) {
		int i=0;
		LPSRowSet pRows=NULL;
		while(TRUE) {
			if(pAttachTable->QueryRows(1,0,&pRows)!=S_OK) MAPIFreeBuffer(pRows);
			else if(!pRows->cRows) FreeProws(pRows);
			else if(i<nIndex) {
				i++;
				continue;
			} else {
				if(m_pMessage->DeleteAttach(pRows->aRow[0].lpProps[PROP_ATTACH_NUM].Value.bin.cb,0,NULL,0)!=S_OK) {
					FreeProws(pRows);
					RELEASE(pAttachTable);
					return FALSE;
				}
				bResult=TRUE;
				FreeProws(pRows);
				if(nIndex==-1) continue;
			}
			break;
		}
	}
	RELEASE(pAttachTable);
	if(bResult) m_pMessage->SaveChanges(FORCE_SAVE | KEEP_OPEN_READWRITE);
	return bResult;
}

BOOL CMAPIMessage::Create(CMAPIEx* pMAPI,int nPriority)
{
	if(!pMAPI) return FALSE;
	LPMAPIFOLDER pOutbox=pMAPI->GetFolder();
	if(!pOutbox) pOutbox=pMAPI->OpenOutbox();
	if(!pOutbox || pOutbox->CreateMessage(NULL,0,&m_pMessage)!=S_OK) return FALSE;
	m_pMAPI=pMAPI;

    SPropValue prop;
	prop.ulPropTag=PR_MESSAGE_FLAGS;
	prop.Value.l=MSGFLAG_UNSENT | MSGFLAG_FROMME;
	m_pMessage->SetProps(1,&prop,NULL);

#ifdef _WIN32_WCE
	// the following uses non outlook attachments (no winmail.dat) so other clients can read mail and attachments
	static const GUID ID_USE_TNEF = {0x00062008,0x000,0x0000,{0xc0,0x00,0x0,0x00,0x00,0x00,0x00,0x46 } };
	MAPINAMEID nameid={(GUID*)&ID_USE_TNEF, MNID_ID, 0x8582};
	MAPINAMEID *rgpnameid[1]={&nameid}; 
	_SPropTagArray* lpSPropTags; 
	HRESULT hr=m_pMessage->GetIDsFromNames(1, rgpnameid, MAPI_CREATE, &lpSPropTags);
	unsigned long ulPropTag=lpSPropTags->aulPropTag[0];
	ulPropTag=PROP_TAG(PT_BOOLEAN, PROP_ID(ulPropTag)); 

	prop.ulPropTag=ulPropTag;
	prop.Value.b=FALSE;
	m_pMessage->SetProps(1,&prop,NULL);

	prop.ulPropTag=PR_MESSAGE_CLASS;
	prop.Value.lpszA=(TCHAR*)"IPM.Note";
	m_pMessage->SetProps(1,&prop,NULL);
#endif

	LPSPropValue props=NULL;
	ULONG cValues=0;
	ULONG rgTags[]={ 1, PR_IPM_SENTMAIL_ENTRYID };

	if(m_pMAPI->GetMessageStore()->GetProps((LPSPropTagArray) rgTags, CMAPIEx::cm_nMAPICode, &cValues, &props)==S_OK) {
		prop.ulPropTag=PR_SENTMAIL_ENTRYID;
		prop.Value.bin.cb=props[0].Value.bin.cb;
		prop.Value.bin.lpb=props[0].Value.bin.lpb;
		m_pMessage->SetProps(1,&prop,NULL);
	}

	if(nPriority!=IMPORTANCE_NORMAL) {
		prop.ulPropTag=PR_IMPORTANCE;
		prop.Value.l=nPriority;
		m_pMessage->SetProps(1,&prop,NULL);
	}

#ifdef _WIN32_WCE
	prop.ulPropTag=PR_MSG_STATUS;
	prop.Value.ul=MSGSTATUS_RECTYPE_SMTP;
	m_pMessage->SetProps(1,&prop,NULL);
#endif

	MAPIFreeBuffer(props);
	return TRUE;
}

// used only by WinCE, pass in MSGSTATUS_RECTYPE_SMS to send an SMS 
// (default is set to MSGSTATUS_RECTYPE_SMTP in Create(...) above)
void CMAPIMessage::SetMessageStatus(int nMessageStatus)
{
	SPropValue prop;
	prop.ulPropTag=PR_MSG_STATUS;
	prop.Value.ul=nMessageStatus;
	m_pMessage->SetProps(1,&prop,NULL);
}

void CMAPIMessage::MarkAsRead(BOOL bRead)
{
	ULONG ulFlags=bRead ? 0 : CLEAR_READ_FLAG;
	if(m_pMessage) m_pMessage->SetReadFlag(ulFlags);
}

BOOL CMAPIMessage::AddRecipient(LPCTSTR szEmail,int nType)
{
	if(!m_pMessage || !m_pMAPI) return FALSE;

#ifndef _WIN32_WCE
	LPADRBOOK pAddressBook;
	if(m_pMAPI->GetSession()->OpenAddressBook(0, NULL, AB_NO_DIALOG, &pAddressBook)!=S_OK) return FALSE;
#endif

	int nBufSize=CbNewADRLIST(1);
	LPADRLIST pAddressList=NULL;
	MAPIAllocateBuffer(nBufSize,(LPVOID FAR*)&pAddressList);
	memset(pAddressList,0,nBufSize);

	const int nProperties=2;
	pAddressList->cEntries=1;

	pAddressList->aEntries[0].ulReserved1=0;
	pAddressList->aEntries[0].cValues=nProperties;

	MAPIAllocateBuffer(sizeof(SPropValue)*nProperties,(LPVOID FAR*)&pAddressList->aEntries[0].rgPropVals);
	memset(pAddressList->aEntries[0].rgPropVals, 0, sizeof(SPropValue)*nProperties);

	pAddressList->aEntries[0].rgPropVals[0].ulPropTag=PR_RECIPIENT_TYPE;
	pAddressList->aEntries[0].rgPropVals[0].Value.ul=nType;

#ifdef _WIN32_WCE
	pAddressList->aEntries[0].rgPropVals[1].ulPropTag=PR_EMAIL_ADDRESS;
	pAddressList->aEntries[0].rgPropVals[1].Value.LPSZ=(TCHAR*)szEmail;
#else
	pAddressList->aEntries[0].rgPropVals[1].ulPropTag=PR_DISPLAY_NAME;
	pAddressList->aEntries[0].rgPropVals[1].Value.LPSZ=(TCHAR*)szEmail;
#endif

#ifdef _WIN32_WCE
	HRESULT hr=m_pMessage->ModifyRecipients(MODRECIP_ADD, pAddressList);
#else
	HRESULT hr=E_INVALIDARG;
	if(pAddressBook->ResolveName(0, CMAPIEx::cm_nMAPICode, NULL, pAddressList)==S_OK) hr=m_pMessage->ModifyRecipients(MODRECIP_ADD, pAddressList);
#endif
	MAPIFreeBuffer(pAddressList->aEntries[0].rgPropVals);
	MAPIFreeBuffer(pAddressList);
#ifndef _WIN32_WCE
	RELEASE(pAddressBook);
#endif
	return (hr==S_OK);
}

void CMAPIMessage::SetSubject(LPCTSTR szSubject)
{
	m_strSubject=(const char *)szSubject;
	if(m_strSubject.length() && m_pMessage) {
		SPropValue prop;
		prop.ulPropTag=PR_SUBJECT;
		prop.Value.LPSZ=(TCHAR*)szSubject;
		m_pMessage->SetProps(1,&prop,NULL);
	}
}

void CMAPIMessage::SetSenderName(LPCTSTR szSenderName)
{
	m_strSenderName = (const char *)szSenderName;
	if(m_strSenderName.length() && m_pMessage) {
		SPropValue prop;
		prop.ulPropTag=PR_SENDER_NAME;
		prop.Value.LPSZ=(TCHAR*)szSenderName;
		m_pMessage->SetProps(1,&prop,NULL);
	}
}

void CMAPIMessage::SetSenderEmail(LPCTSTR szSenderEmail)
{
	m_strSenderEmail=(const char *)szSenderEmail;
	if(m_strSenderEmail.length() && m_pMessage) {
		SPropValue prop;
		prop.ulPropTag=PR_SENDER_EMAIL_ADDRESS;
		prop.Value.LPSZ=(TCHAR*)szSenderEmail;
		m_pMessage->SetProps(1,&prop,NULL);
	}
}

void CMAPIMessage::SetBody(QString strBody)
{
	m_strBody = strBody;
	if(m_strBody.length() && m_pMessage) 
	{
		LPSTREAM pStream=NULL;

		if(m_pMessage->OpenProperty(PR_BODY, &IID_IStream, 0, MAPI_MODIFY | MAPI_CREATE, (LPUNKNOWN*)&pStream)==S_OK) 
		{
			pStream->Write(strBody.utf16(), (ULONG)((strBody.length() + 1)*sizeof(TCHAR)), NULL);
		}
		
		RELEASE(pStream);
	}
}
#include <QDebug>
void CMAPIMessage::SetRTF(QString strRTF)
{
	if(!m_pMessage) return;
	m_strRTF=strRTF;

	ULONG ulSupport=m_pMAPI ? m_pMAPI->GetMessageStoreSupport() : 0;
	if(strRTF.startsWith("<html")) 
	{
		// is this HTML?  Does this store support HTML directly?
		if(ulSupport&STORE_HTML_OK) 
		{
			LPSTREAM pStream=NULL;

			char szbuf[] = ("<html><body><font size=2 color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;color:red'>Body</font></body></html>");			

			 wchar_t *wc = NULL;
			 //int len = strRTF.toWCharArray(wc);

			//if(m_pMessage->OpenProperty(PR_BODY_HTML, &IID_IStream, 0, MAPI_MODIFY | MAPI_CREATE, (LPUNKNOWN*)&pStream)==S_OK) 
			if(m_pMessage->OpenProperty(PR_BODY_HTML, &IID_IStream, 0, MAPI_CREATE |MAPI_MODIFY, (LPUNKNOWN*)&pStream)==S_OK) 
			{
				ULONG wr;
				pStream->Write(strRTF.utf16(), (ULONG)((strRTF.length() + 1)*sizeof(TCHAR)), &wr);
				qDebug() << wr ;
				
				//pStream->Write(&szbuf, (ULONG)(strlen(szbuf)+1), &wr);
				//pStream->Write(strRTF.toUtf8(), (ULONG)(131), &wr);
				//pStream->Write(strRTF.utf16(), (ULONG)(262), &wr);
				//pStream->Write(szbuf, (ULONG)((strlen(szbuf))), &wr);
				//pStream->Write(wc, (ULONG)(len*sizeof(TCHAR)), &wr);
				//pStream->Write(szbuf, (ULONG)((strlen(szbuf))), NULL);
				//pStream->Write(&szbuf, (ULONG)(strlen(szbuf)), NULL);
				//pStream->Write(&szbuf, (ULONG)((strlen(szbuf))), NULL);
				//pStream->Write((const ushort *)(strRTF.utf16()), (ULONG)(strRTF.length()+ 1)*sizeof(TCHAR), NULL);
				//pStream->Write((const char *)(strRTF.utf16()), (ULONG)((strRTF.length() + 1)*sizeof(TCHAR)), NULL);
				//pStream->Write(strRTF.toLatin1().constData(), (ULONG)((strlen(strRTF.toLatin1().constData()) +1)*sizeof(TCHAR)), NULL);
				
				//pStream->Write((LPCTSTR)(_T("<html><body><font size=2 color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;color:red'>Body</font></body></html>")), (ULONG)(131)*sizeof(TCHAR)/*(ULONG)((strRTF.length() + 5)*sizeof(TCHAR)), NULL);
				//if (pStream->Write((strRTF.utf16()), (ULONG)((strRTF.length() + 1)*sizeof(TCHAR)), NULL) == S_OK)
				//{
				//	qDebug() << "gafas";
				//}
			}

			RELEASE(pStream);
			return;
		} else {
			// otherwise lets encode it into RTF 
			char szCodePage[6]= "1252"; // default codepage is ANSI - Latin I
			GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_IDEFAULTANSICODEPAGE, (LPWSTR)szCodePage,sizeof(szCodePage));

			m_strRTF.sprintf("{\\rtf1\\ansi\\ansicpg%s\\fromhtml1 {\\*\\htmltag1 ",szCodePage);
			m_strRTF += strRTF;
			m_strRTF += " }}";
		}
	}

	if(m_strRTF.length()) 
	{
		m_strRTF = ("<html><body><font size=2 color=red face=Arial><span style='font-size:10.0pt;font-family:Arial;color:red'>Body</font></body></html>");
		LPSTREAM pStream=NULL;
		//if(m_pMessage->OpenProperty(PR_RTF_COMPRESSED, &IID_IStream, STGM_CREATE | STGM_WRITE, MAPI_MODIFY | MAPI_CREATE, (LPUNKNOWN*)&pStream)==S_OK) 
		if(m_pMessage->OpenProperty(PR_RTF_COMPRESSED, &IID_IStream, STGM_CREATE | STGM_WRITE, MAPI_MODIFY | MAPI_CREATE, (LPUNKNOWN*)&pStream)==S_OK) 
		{
			IStream *pUncompressed;
			if(WrapCompressedRTFStream(pStream,MAPI_MODIFY,&pUncompressed)==S_OK) {
				ULONG wr;
				pUncompressed->Write(m_strRTF.utf16(),(ULONG)m_strRTF.length()*sizeof(TCHAR),&wr);
				qDebug() << wr;
				if(pUncompressed->Commit(STGC_DEFAULT)==S_OK) pStream->Commit(STGC_DEFAULT);
				RELEASE(pUncompressed);
			}
			RELEASE(pStream);
		}
	}
}

/*
void CMAPIMessage::SetBody(LPCTSTR szBody)
{
	m_strBody=(const char *)szBody;
	if(m_strBody.length() && m_pMessage) {
		LPSTREAM pStream=NULL;
		if(m_pMessage->OpenProperty(PR_BODY, &IID_IStream, 0, MAPI_MODIFY | MAPI_CREATE, (LPUNKNOWN*)&pStream)==S_OK) {
			pStream->Write((const char *)szBody,(ULONG)(_tcslen((const char *)szBody)+2)*sizeof(TCHAR),NULL);
		}
		RELEASE(pStream);
	}
}
*/
// use this function to set the body of the message to HTML or RTF
/*
void CMAPIMessage::SetRTF(LPCTSTR szRTF)
{
	if(!m_pMessage) return;
	m_strBody=(const char *)szRTF;

	ULONG ulSupport=m_pMAPI ? m_pMAPI->GetMessageStoreSupport() : 0;
	if(strncmp((const char *)szRTF,"<html",5)==0) {
		// is this HTML?  Does this store support HTML directly?
		if(ulSupport&STORE_HTML_OK) {
			LPSTREAM pStream=NULL;

			if(m_pMessage->OpenProperty(PR_BODY_HTML, &IID_IStream, 0, MAPI_MODIFY | MAPI_CREATE, (LPUNKNOWN*)&pStream)==S_OK) 
			{
				pStream->Write(szRTF,(ULONG)(_tcslen((const char *)szRTF))*sizeof(TCHAR),NULL);
			}
			RELEASE(pStream);
			return;
		} else {
			// otherwise lets encode it into RTF 
			char szCodePage[6]= "1252"; // default codepage is ANSI - Latin I
			GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_IDEFAULTANSICODEPAGE, (LPWSTR)szCodePage,sizeof(szCodePage));

			m_strRTF.sprintf("{\\rtf1\\ansi\\ansicpg%s\\fromhtml1 {\\*\\htmltag1 ",szCodePage);
			m_strRTF+=(const char *)szRTF;
			m_strRTF+=" }}";
		}
	}

	if(m_strRTF.length()) {
		LPSTREAM pStream=NULL;
		if(m_pMessage->OpenProperty(PR_RTF_COMPRESSED, &IID_IStream, STGM_CREATE | STGM_WRITE, MAPI_MODIFY | MAPI_CREATE, (LPUNKNOWN*)&pStream)==S_OK) {
			IStream *pUncompressed;
			if(WrapCompressedRTFStream(pStream,MAPI_MODIFY,&pUncompressed)==S_OK) {
				pUncompressed->Write(m_strRTF.toLatin1().constData(),(ULONG)m_strRTF.length()*sizeof(TCHAR),NULL);
				if(pUncompressed->Commit(STGC_DEFAULT)==S_OK) pStream->Commit(STGC_DEFAULT);
				RELEASE(pUncompressed);
			}
			RELEASE(pStream);
		}
	}
}
*/
BOOL CMAPIMessage::AddAttachment(LPCTSTR szPath,LPCTSTR szName,LPCTSTR szCID, LPCTSTR szMime)
{
	if(!m_pMessage) return FALSE;

	IAttach* pAttachment=NULL;
	ULONG ulAttachmentNum=0;

	QFile file((const char*)szPath);
	if(!file.open(QIODevice::ReadOnly)) return FALSE;

	LPTSTR szFileName=(LPTSTR)szName;
	if(!szFileName) {
		szFileName=(LPTSTR)szPath;
		for(int i=(int)_tcsclen((const char *)szPath)-1;i>=0;i--) if(szPath[i]=='\\' || szPath[i]=='/') {
			szFileName=(LPTSTR)&szPath[i+1];
			break;
		}
	}

	if(m_pMessage->CreateAttach(NULL,0,&ulAttachmentNum,&pAttachment)!=S_OK) {
		file.close();
		return FALSE;
	}

	const int nProperties=5;
	SPropValue prop[nProperties];
	memset(prop,0,sizeof(SPropValue)*nProperties);
	prop[0].ulPropTag=PR_ATTACH_METHOD;
	prop[0].Value.ul=ATTACH_BY_VALUE;
	prop[1].ulPropTag=PR_ATTACH_LONG_FILENAME;
	prop[1].Value.LPSZ=(TCHAR*)szFileName;
	prop[2].ulPropTag=PR_ATTACH_FILENAME;
	prop[2].Value.LPSZ=(TCHAR*)szFileName;
	prop[3].ulPropTag=PR_RENDERING_POSITION;
	prop[3].Value.l=-1;
	prop[4].ulPropTag=PR_ATTACH_CONTENT_ID;
	prop[4].Value.LPSZ=(TCHAR*)szCID;

	if(pAttachment->SetProps(nProperties,prop,NULL)==S_OK) {
		LPSTREAM pStream=NULL;
		if(pAttachment->OpenProperty(PR_ATTACH_DATA_BIN, &IID_IStream, 0, MAPI_MODIFY | MAPI_CREATE, (LPUNKNOWN*)&pStream)==S_OK) {
			const int BUF_SIZE=4096;
			BYTE pData[BUF_SIZE];
			ULONG ulSize=0,ulRead,ulWritten;

			ulRead=file.read((char *)pData,BUF_SIZE);
			while(ulRead) {
				pStream->Write((const char *)pData,ulRead,&ulWritten);
				ulSize+=ulRead;
				ulRead=file.read((char *)pData, BUF_SIZE);
			}

			pStream->Commit(STGC_DEFAULT);
			RELEASE(pStream);
			file.close();

			prop[0].ulPropTag=PR_ATTACH_SIZE;
			prop[0].Value.ul=ulSize;
			pAttachment->SetProps(1, prop, NULL);

			//set mime if required
			if(szMime){
				prop[0].ulPropTag=PR_ATTACH_MIME_TAG;
				prop[0].Value.LPSZ=(TCHAR*)szMime;
				pAttachment->SetProps(1, prop, NULL);
			}

			pAttachment->SaveChanges(KEEP_OPEN_READONLY);
			RELEASE(pAttachment);
			m_nAttachments++;
			return TRUE;
		}
	}

	file.close();
	RELEASE(pAttachment);
	return FALSE;
}

// request a Read Receipt sent to szReceiverEmail 
void CMAPIMessage::SetReadReceipt(BOOL bSet,LPCTSTR szReceiverEmail)
{
	if(!m_pMessage) return;

	SPropValue prop;
	prop.ulPropTag=PR_READ_RECEIPT_REQUESTED;
	prop.Value.b=(unsigned short)bSet;
	m_pMessage->SetProps(1,&prop,NULL);

	if(bSet && szReceiverEmail && _tcslen((const char *)szReceiverEmail)>0) {
		prop.ulPropTag=PR_READ_RECEIPT_SEARCH_KEY;
		prop.Value.LPSZ=(TCHAR*)szReceiverEmail;
		m_pMessage->SetProps(1,&prop,NULL);
	}
}

BOOL CMAPIMessage::Send()
{
	if(m_pMessage && m_pMessage->SubmitMessage(0)==S_OK) {
		Close();
		return TRUE;
	}
	return FALSE;
}

// shallow copy only, no message pointer
CMAPIMessage& CMAPIMessage::operator=(CMAPIMessage& message)
{
	m_strSenderName=message.m_strSenderName;
	m_strSenderEmail=message.m_strSenderEmail;
	m_strSubject=message.m_strSubject;
	m_strBody=message.m_strBody;
	m_strRTF=message.m_strRTF;

	m_dwMessageFlags=message.m_dwMessageFlags;
	m_tmReceived=message.m_tmReceived;
	SetEntryID(message.GetEntryID());
	return *this;
}

// limited compare, compares entry IDs and subject to determine if two emails are equal
BOOL CMAPIMessage::operator==(CMAPIMessage& message)
{
	if(!m_entry.cb || !message.m_entry.cb || m_entry.cb!=message.m_entry.cb) return FALSE;
	if(memcmp(&m_entry.lpb,&message.m_entry.lpb,m_entry.cb)) return FALSE;
	return (m_strSubject != message.m_strSubject);
}

// Shows the default MAPI form for IMessage, returns FALSE on failure, IDOK on success or close existing messages 
// and IDCANCEL on close new messages
int CMAPIMessage::ShowForm(CMAPIEx* pMAPI)
{
	IMAPISession* pSession=pMAPI->GetSession();
	ULONG ulMessageToken;

	if(pSession && pSession->PrepareForm(NULL,m_pMessage,&ulMessageToken)==S_OK) {
		ULONG ulMessageStatus=0,ulMessageFlags=0,ulAccess=0;

		LPSPropValue pProp;
		if(GetProperty(PR_MSG_STATUS,pProp)==S_OK) {
			ulMessageStatus=pProp->Value.ul;
			MAPIFreeBuffer(pProp);
		}
		if(GetProperty(PR_MESSAGE_FLAGS,pProp)==S_OK) {
			ulMessageFlags=pProp->Value.ul;
			MAPIFreeBuffer(pProp);
		}
		if(GetProperty(PR_ACCESS,pProp)==S_OK) {
			ulAccess=pProp->Value.ul;
			MAPIFreeBuffer(pProp);
		}
		if(GetProperty(PR_MESSAGE_CLASS,pProp)==S_OK) {
#ifdef UNICODE
			char szMessageClass[256];
			WideCharToMultiByte(CP_ACP,0,pProp->Value.LPSZ,-1,szMessageClass,255,NULL,NULL);
#else
			char* szMessageClass=pProp->Value.LPSZ;
#endif
			HRESULT hr=pSession->ShowForm(NULL,pMAPI->GetMessageStore(),pMAPI->GetFolder(),NULL,ulMessageToken,NULL,0,ulMessageStatus,ulMessageFlags,ulAccess,szMessageClass);
			MAPIFreeBuffer(pProp);
			if(hr==S_OK) return IDOK;
			if(hr==MAPI_E_USER_CANCEL) return IDCANCEL;
		}
	}
	return FALSE;
}

// Novell GroupWise customization by jcadmin
#ifndef GROUPWISE
void CMAPIMessage::MarkAsPrivate() {}
#else
#include GWMAPI.h
//(from Novell Developer Kit)
#define SEND_OPTIONS_MARK_PRIVATE 0x00080000L

void CMAPIMessage::MarkAsPrivate() 
{
	SPropValue prop;
	prop.ulPropTag=PR_NGW_SEND_OPTIONS;
	prop.Value.l=NGW_SEND_OPTIONS_MARK_PRIVATE;
	m_pMessage->SetProps(1,&prop,NULL);
}
#endif

bool CMAPIMessage::GetTo(QString& strTo)
{
	LPSPropValue pProp;
	if(GetProperty(PR_DISPLAY_TO,pProp)==S_OK) {
		strTo=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
		return TRUE;
	}
	return FALSE;
}

bool CMAPIMessage::GetCC(QString& strCC)
{
	LPSPropValue pProp;
	if(GetProperty(PR_DISPLAY_CC,pProp)==S_OK) {
		strCC=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
		return true;
	}
	return false;
}

bool CMAPIMessage::GetBCC(QString& strBCC)
{
	LPSPropValue pProp;
	if(GetProperty(PR_DISPLAY_BCC,pProp)==S_OK) {
		strBCC=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
		return true;
	}
	return false;
}

bool CMAPIMessage::GetRecipients()
{
	if(!m_pMessage) return FALSE;
	RELEASE(m_pRecipients);

	if(m_pMessage->GetRecipientTable(CMAPIEx::cm_nMAPICode,&m_pRecipients)!=S_OK) return FALSE;

	const int nProperties=RECIPIENT_COLS;
	SizedSPropTagArray(nProperties,Columns)={nProperties,{PR_RECIPIENT_TYPE, PR_DISPLAY_NAME, PR_EMAIL_ADDRESS, PR_ADDRTYPE, PR_ENTRYID }};
	return (m_pRecipients->SetColumns((LPSPropTagArray)&Columns,0)==S_OK);
}

bool CMAPIMessage::GetNextRecipient(QString& strName, QString& strEmail, int& nType)
{
	if(!m_pRecipients) return false;

	LPSRowSet pRows=NULL;
	bool bResult=false;
	if(m_pRecipients->QueryRows(1,0,&pRows)==S_OK) {
		if(pRows->cRows) {
			nType=pRows->aRow[0].lpProps[PROP_RECIPIENT_TYPE].Value.ul;
			strName=CMAPIEx::GetValidString(pRows->aRow[0].lpProps[PROP_RECIPIENT_NAME]);

			// for Microsoft Exchange server internal mails we want to try to resolve the SMTP email address
			QString strAddrType=CMAPIEx::GetValidString(pRows->aRow[0].lpProps[PROP_ADDR_TYPE]);
			if(strAddrType==_T("EX")) {
				if(m_pMAPI) m_pMAPI->GetExEmail(pRows->aRow[0].lpProps[PROP_ENTRYID].Value.bin,strEmail);
			} else {
				strEmail=CMAPIEx::GetValidString(pRows->aRow[0].lpProps[PROP_RECIPIENT_EMAIL]);
			}
			bResult=true;
		}
		FreeProws(pRows);
	}
	//MAPIFreeBuffer(pRows); //B.T changed 08.02.2016: crashes Outloook 2016
	return bResult;
}

bool CMAPIMessage::GetMessageClass(QString& strMessageClass)
{
	LPSPropValue pProp;
	if(GetProperty(PR_MESSAGE_CLASS,pProp)==S_OK) {
		strMessageClass=CMAPIEx::GetValidString(*pProp);
		MAPIFreeBuffer(pProp);
		return true;
	}
	return false;
}

#endif // WINCE
#endif