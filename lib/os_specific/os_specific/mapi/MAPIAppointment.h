#ifndef __MAPIAPPOINTMENT_H__
#define __MAPIAPPOINTMENT_H__

#ifdef _WIN32

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File: MAPIAppointment.h
// Description: MAPI Appointment class wrapper
//
// Copyright (C) 2005-2007, Noel Dillabough
//
// This source code is free to use and modify provided this notice remains intact and that any enhancements
// or bug fixes are posted to the CodeProject page hosting this class for the community to benefit.
//
// Usage: see the CodeProject article at http://www.codeproject.com/internet/CMapiEx.asp
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CMAPIEx;
class CMAPIAppointment;

#ifdef _WIN32_WCE
#include "POOM.h"
#endif 

/////////////////////////////////////////////////////////////
// CMAPIAppointment

class CMAPIAppointment
{
public:
	CMAPIAppointment();
	~CMAPIAppointment();

	enum { OUTLOOK_DATA2=0x00062002, OUTLOOK_APPOINTMENT_START=0x820D, OUTLOOK_APPOINTMENT_END=0x820E,
			OUTLOOK_APPOINTMENT_LOCATION=0x8208
	};
	enum { PROP_RECIPIENT_TYPE, PROP_RECIPIENT_NAME, PROP_RECIPIENT_EMAIL, PROP_ADDR_TYPE, PROP_ENTRYID, /*PR_APPOINTMENT_RESPONSE_STATUS=0x8218,*/ RECIPIENT_COLS/*=6*/ };

// Attributes
protected:
#ifdef _WIN32_WCE
	IAppointment* m_pAppointment;
#endif
	LPMAPITABLE m_pRecipients;
	CMAPIEx* m_pMAPI;
	SBinary m_entry;
	IMAPIProp* m_pItem;

// Operations
public:
	void SetEntryID(SBinary* pEntry=NULL);
	SBinary* GetEntryID() { return &m_entry; }

//	BOOL Create(CMAPIEx* pMAPI,int nPriority);
	void Close();
	//BOOL Send();
	//BOOL AddRecipient(LPCTSTR szEmail,int nType=MAPI_TO);  // MAPI_CC and MAPI_BCC also valid

	BOOL GetSubject(QString& strSubject);
	BOOL GetLocation(QString& strLocation);
	QDateTime GetStartDate();
	BOOL GetStartDate(SYSTEMTIME& tmStart);
	BOOL GetStartDate(QString& strStartDate, LPCTSTR szFormat=NULL); // NULL defaults to "MM/dd/yyyy hh:mm:ss tt"
	BOOL GetEndDate(SYSTEMTIME& tmEnd);
	BOOL GetEndDate(QString& strEndDate, LPCTSTR szFormat=NULL); // NULL defaults to "MM/dd/yyyy hh:mm:ss tt"
	BOOL GetBody(QString& strBody);
	BOOL GetBodyHtml(QString& strBody);
	BOOL GetResponseStatus(int &nResponseStatus);
	bool GetMessageClass(QString& strMessageClass);
	bool GetVCalendarUID(QString& strUID);

	BOOL SetSubject(const QString& strSubject);
	BOOL SetLocation(const QString& strLocation);
	BOOL SetBody(const QString& strBody);
	BOOL SetBodyHtml(const QString& strBody);
	BOOL SetStartDate(const SYSTEMTIME& tmStart);
	BOOL SetEndDate(const SYSTEMTIME& tmEnd);
	
	QDateTime GetReceivedTime();

	BOOL GetRecipients();
	BOOL GetNextRecipient(QString& strName, QString& strEmail, int& nType, int &nAppResponseStatus);

#ifdef _WIN32_WCE
	BOOL Open(CMAPIEx* pMAPI, IAppointment* pAppointment);
	virtual void Close();
#else
	BOOL Open(CMAPIEx* pMAPI,SBinary entryID);
#endif

	virtual BOOL GetPropertyString(ULONG ulProperty, QString& strProperty);
	virtual BOOL SetPropertyString(ULONG ulProperty, LPCTSTR szProperty);
	HRESULT GetProperty(ULONG ulProperty, LPSPropValue& pProp);
	BOOL GetOutlookPropertyString(ULONG ulData, ULONG ulProperty, QString& strProperty);
	BOOL GetOutlookProperty(ULONG ulData, ULONG ulProperty, LPSPropValue& pProp);
	BOOL GetOutlookPropTagArray(ULONG ulData, ULONG ulProperty, LPSPropTagArray& lppPropTags, int& nFieldType, BOOL bCreate);
	BOOL SetOutlookProperty(ULONG ulData, ULONG ulProperty, LPCTSTR szField);
	BOOL SetOutlookProperty(ULONG ulData, ULONG ulProperty, int nField);
	BOOL SetOutlookProperty(ULONG ulData, ULONG ulProperty, FILETIME ftField);
	BOOL Save(BOOL bClose);
};

#define PR_APPOINTMENT_START PROP_TAG( PT_SYSTIME, CMAPIAppointment::OUTLOOK_APPOINTMENT_START)
#define PR_APPOINTMENT_END PROP_TAG( PT_SYSTIME, CMAPIAppointment::OUTLOOK_APPOINTMENT_END)

#endif

#endif

