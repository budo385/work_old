#include "printmanager.h"

#ifndef WINCE
#include <QPrintDialog>
#include <QPrinter>
#include <QPainter>
bool PrintManager::PrintPixMap(const QPixmap *pixmap)
{
	QPrinter printer;
	QPrintDialog dialog(&printer);
	if (dialog.exec()) 
	{
		QPainter painter(&printer);
		QRect rect = painter.viewport();
		QSize size =pixmap->size();
		size.scale(rect.size(), Qt::KeepAspectRatio);
		painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
		painter.setWindow(pixmap->rect());
		painter.drawPixmap(0, 0, *pixmap);
		return true;
	}
	return false;
}


bool PrintManager::PrintText(const QTextEdit *pText)
{
	QPrinter printer;
	QPrintDialog print_dialog(&printer);
	if (print_dialog.exec() == QPrintDialog::Accepted)
	{
		pText->print(&printer);
		return true;
	}
	else
		return false;
}
#endif // WINCE