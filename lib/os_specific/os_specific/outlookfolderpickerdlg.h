#ifndef OUTLOOKFOLDERPICKERDLG_H
#define OUTLOOKFOLDERPICKERDLG_H

#include <QtWidgets/QDialog>
#include <QTreeWidgetItem>
#include "generatedfiles/ui_outlookfolderpickerdlg.h"

#ifndef WINCE
//#include "mapi/mapiex.h"
class CMAPIEx;

class OutlookFolderPickerDlg : public QDialog
{
    Q_OBJECT

public:
    OutlookFolderPickerDlg(bool bThunderbird = false, bool bContacts = false, bool bRememberSettingBtn = true, QWidget *parent = 0);
    ~OutlookFolderPickerDlg();

	void RefreshSelection();
	QList<QStringList>& GetSelection(){	return m_lstSelection; }
	bool StoreSettings(QByteArray &ardata);
	void SetSelection(QList<QStringList> &lstSelection);

	static void FolderList2Binary(QList<QStringList> &lstData, QByteArray &arResult);
	static void Binary2FolderList(QByteArray &arData, QList<QStringList> &lstResult);
	static QString ThunderbirdFolder2String(QStringList &lstData);
	static void GetDefaultFolders(QList<QStringList> &lstData, bool bContacts = false);

private:
#ifdef _WIN32
	void ListBranchRecursive(QTreeWidgetItem *pParent, QStringList lstParent, CMAPIEx *pMapi);
#endif
	void RefreshSelectionRecursive(QTreeWidgetItem *pParent, QStringList lstParent);
	void CheckBranchRecursively(QTreeWidgetItem * item, bool bCheck = true);
	void ListBranchRecursive_Thunderbird(QTreeWidgetItem *pParent, QStringList lstParent);
	void SetDefaultSelection();

private:
	bool m_bThunderbird;
    Ui::OutlookFolderPickerDlgClass ui;
	QList<QStringList> m_lstSelection;
	bool m_bTreeBuilt;
	bool m_bRecursing;
	bool m_bContacts;
	QString m_strContactsName;
	bool m_bStoreSetings;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnRememberSetting_clicked();
	void on_treeFolders_itemChanged(QTreeWidgetItem * item,int column);
};

#else // WINCE


class OutlookFolderPickerDlg : public QDialog
{
	Q_OBJECT

public:
	OutlookFolderPickerDlg(bool bThunderbird = false, bool bContacts = false, bool bRememberSettingBtn = true, QWidget *parent = 0){};
	~OutlookFolderPickerDlg(){};

	void RefreshSelection(){};
	QList<QStringList>& GetSelection(){	return m_lstSelection;}
	void SetSelection(QList<QStringList> &lstSelection){};

	static void FolderList2Binary(QList<QStringList> &lstData, QByteArray &arResult){};
	static void Binary2FolderList(QByteArray &arData, QList<QStringList> &lstResult){};
	static QString ThunderbirdFolder2String(QStringList &lstData){return "";};
	static void GetDefaultFolders(QList<QStringList> &lstData, bool bContacts = false){};

public slots:
	int exec(){return 1;}; //return cancel
	private slots:
	void on_btnCancel_clicked(){};
	void on_btnOK_clicked(){};
	void on_btnRememberSetting_clicked(){};
	void on_treeFolders_itemChanged(QTreeWidgetItem * item,int column){};

private:
	QList<QStringList> m_lstSelection;
};


#endif // WINCE
#endif // OUTLOOKFOLDERPICKERDLG_H
