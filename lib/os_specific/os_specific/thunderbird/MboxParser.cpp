#ifndef WINCE

#ifdef _WIN32 
//#define WIN32_LEAN_AND_MEAN
//#define NOMINMAX
//#include <windows.h>
#include <qt_windows.h>
//#include <shlobj.h>
//#include <lm.h>
#endif //_WIN32 


#include "MboxParser.h"
#include <QFile>
#include <QRegExp>
#include <QLocale>
#include "common/common/datahelper.h"

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;


#ifndef MAX_LINE
 #define MAX_LINE 4096
#endif

#ifdef _WIN32
  #define strcasecmp stricmp
  #define strncasecmp strnicmp
#endif

// Regular expression to find the line that seperates messages in a mail
// folder:
#define MSG_SEPERATOR_START "From "
#define MSG_SEPERATOR_START_LEN (sizeof(MSG_SEPERATOR_START) - 1)
#define MSG_SEPERATOR_REGEX "^From .*[0-9][0-9]:[0-9][0-9]"

MboxParser::MboxParser()
{
	m_pStream = NULL;
}

MboxParser::~MboxParser()
{
}

bool MboxParser::open(QString &strFile)
{
	// load messages file
	m_pStream = fopen(QFile::encodeName(strFile).constData(), "r");
	return (NULL != m_pStream);
}
	
void MboxParser::close()
{
	if(m_pStream){
		fclose(m_pStream);
		m_pStream = NULL;
	}
}

void MboxParser::buildMailIndex(int nStartOffset)
{
  char line[MAX_LINE];
  char line_stripped[MAX_LINE];
  char status[8], xstatus[8];
  QString fullMsgStr, subjStr, dateStr, fromStr, toStr, xmarkStr, *lastStr=0;
  QString replyToIdStr, replyToAuxIdStr, referencesStr, msgIdStr, XMozillaStatusStr, XMozillaStatusStr2;
  QString sizeServerStr, uidStr;
  QString contentTypeStr, charset;
  int nFileOffset;
  bool atEof = false;
  bool inHeader = true;
  bool bIncoming = false;
  QString msgStr;
  QRegExp regexp(MSG_SEPERATOR_REGEX);
  int i, num, numStatus;
  short needStatus;

  //assert(m_pStream != 0);
  if(nStartOffset > 0)
		fseek(m_pStream, nStartOffset, SEEK_SET);
  else
		rewind(m_pStream);

  m_lstMsgs.clear();

  num     = -1;
  numStatus= 11;
  off_t offs = 0;
  size_t size = 0;
  dateStr = "";
  fromStr = "";
  toStr = "";
  fullMsgStr = "";
  subjStr = "";
  bIncoming = false;
  *status = '\0';
  *xstatus = '\0';
  xmarkStr = "";
  replyToIdStr = "";
  replyToAuxIdStr = "";
  referencesStr = "";
  msgIdStr = "";
  XMozillaStatusStr = "";
  XMozillaStatusStr2 = "";
  needStatus = 3;
  size_t sizeServer = 0;
  ulong uid = 0;
  nFileOffset = 0;

  while (!atEof)
  {
    off_t pos = ftell(m_pStream);
    if (!fgets(line, MAX_LINE, m_pStream)) 
		atEof = true;
	else{
		memcpy(line_stripped, line, MAX_LINE);
		//strip "\n"
		int nLen = strlen(line_stripped);
		if(nLen > 0 && '\n' == line_stripped[nLen-1]){
			line_stripped[nLen-1] = '\0'; nLen--;
		}
		if(nLen > 0 && '\r' == line_stripped[nLen-1])
			line_stripped[nLen-1] = '\0';
	}

    if (atEof ||
        (memcmp(line, MSG_SEPERATOR_START, MSG_SEPERATOR_START_LEN)==0)) /*&& regexp.exactMatch(line)*/ //TOFIX
    {
		//found new email in mbox file or end of file
      size = pos - offs;
      pos = ftell(m_pStream);

      if (num >= 0)
      {
        if (size > 0)
        {
          msgIdStr = msgIdStr.trimmed();
          if( !msgIdStr.isEmpty() ) {
            int rightAngle;
            rightAngle = msgIdStr.indexOf( '>' );
            if( rightAngle != -1 )
              msgIdStr.truncate( rightAngle + 1 );
          }

          replyToIdStr = replyToIdStr.trimmed();
          if( !replyToIdStr.isEmpty() ) {
            int rightAngle;
            rightAngle = replyToIdStr.indexOf( '>' );
            if( rightAngle != -1 )
              replyToIdStr.truncate( rightAngle + 1 );
          }

          referencesStr = referencesStr.trimmed();
          if( !referencesStr.isEmpty() ) {
            int leftAngle, rightAngle;
            leftAngle = referencesStr.lastIndexOf( '<' );
            if( ( leftAngle != -1 )
                && ( replyToIdStr.isEmpty() || ( replyToIdStr[0] != '<' ) ) ) {
              // use the last reference, instead of missing In-Reply-To
              replyToIdStr = referencesStr.mid( leftAngle );
            }

            // find second last reference
            leftAngle = referencesStr.lastIndexOf( '<', leftAngle - 1 );
            if( leftAngle != -1 )
              referencesStr = referencesStr.mid( leftAngle );
            rightAngle = referencesStr.lastIndexOf( '>' );
            if( rightAngle != -1 )
              referencesStr.truncate( rightAngle + 1 );

            // Store the second to last reference in the replyToAuxIdStr
            // It is a good candidate for threading the message below if the
            // message In-Reply-To points to is not kept in this folder,
            // but e.g. in an Outbox
            replyToAuxIdStr = referencesStr;
            rightAngle = referencesStr.indexOf( '>' );
            if( rightAngle != -1 )
              replyToAuxIdStr.truncate( rightAngle + 1 );
          }

          contentTypeStr = contentTypeStr.trimmed();
          charset = "";
          if ( !contentTypeStr.isEmpty() )
          {
            int cidx = contentTypeStr.indexOf( "charset=" );
            if ( cidx != -1 ) {
              charset = contentTypeStr.mid( cidx + 8 );
              if ( charset[0] == '"' ) {
                charset = charset.mid( 1 );
              }
              cidx = 0;
              while ( (unsigned int) cidx < charset.length() ) {
                if ( charset[cidx] == '"' || ( !isalnum(charset[cidx].toLatin1()) &&
                    charset[cidx] != '-' && charset[cidx] != '_' ) )
                  break;
                ++cidx;
              }
              charset.truncate( cidx );
              // kdDebug() << "KMFolderMaildir::readFileHeaderIntern() charset found: " <<
              //              charset << " from " << contentTypeStr << endl;
            }
          }

		  //insert new mail info
		  MailInfo info;
		  info.m_bIncoming = bIncoming;
		  info.m_strFrom = fromStr;
		  info.m_strSubject = subjStr;
		  info.m_nStartOffset = nFileOffset;
		  //info.m_strContent = fullMsgStr;
		  info.m_dtSent = ParseMsgDate(dateStr);
		  info.m_nXMozillaStatus = XMozillaStatusStr.toUInt(NULL, 16);
		  info.m_nXMozillaStatus2 = XMozillaStatusStr2.toUInt(NULL, 16);
		  m_lstMsgs.push_back(info);

		  nFileOffset = ftell(m_pStream);
		  
		  //reset temp data
		  fullMsgStr = "";
          *status = '\0';
          *xstatus = '\0';
          needStatus = 3;
          xmarkStr = "";
          replyToIdStr = "";
          replyToAuxIdStr = "";
          referencesStr = "";
          msgIdStr = "";
		  XMozillaStatusStr = "";
		  XMozillaStatusStr2 = "";
          dateStr = "";
          fromStr = "";
          subjStr = "";
		  bIncoming = false;
          sizeServer = 0;
          uid = 0;
        }
		else
		{ 
			num--,numStatus++;
		}
      }

      offs = ftell(m_pStream);
      num++;
      numStatus--;
      inHeader = true;
      continue;
    }
	else{
		if(!fullMsgStr.isEmpty())
			fullMsgStr += "\r\n";
		fullMsgStr += line_stripped;
	}

    // Is this a long header line?
    if (inHeader && (line[0]=='\t' || line[0]==' '))
    {
      i = 0;
      while (line [i]=='\t' || line [i]==' ') i++;
      if (line [i] < ' ' && line [i]>0) inHeader = false;
      else if (lastStr) *lastStr += line + i;
    }
    else lastStr = 0;

    if (inHeader && (line [0]=='\n' || line [0]=='\r'))
      inHeader = false;
    if (!inHeader) 
		continue;

    /* -sanders Make all messages read when auto-recreating index */
    /* Reverted, as it breaks reading the sent mail status, for example.
       -till */
    if ((needStatus & 1) && strncasecmp(line, "Status:", 7) == 0)
    {
      for(i=0; i<4 && line[i+8] > ' '; i++)
        status[i] = line[i+8];
      status[i] = '\0';
      needStatus &= ~1;
    }
    else if ((needStatus & 2) && strncasecmp(line, "X-Status:", 9)==0)
    {
      for(i=0; i<4 && line[i+10] > ' '; i++)
        xstatus[i] = line[i+10];
      xstatus[i] = '\0';
      needStatus &= ~2;
    }
    else if (strncasecmp(line,"X-KMail-Mark:",13)==0)
        xmarkStr = QString(line+13);
    else if (strncasecmp(line,"In-Reply-To:",12)==0) {
      replyToIdStr = QString(line+12);
      lastStr = &replyToIdStr;
    }
    else if (strncasecmp(line,"References:",11)==0) {
      referencesStr = QString(line+11);
      lastStr = &referencesStr;
    }
    else if (strncasecmp(line,"Message-Id:",11)==0) {
      msgIdStr = QString(line+11);
      lastStr = &msgIdStr;
    }
	else if (strncasecmp(line,"X-Mozilla-Status: ",18)==0) {
      XMozillaStatusStr = QString(line+18);
      lastStr = &XMozillaStatusStr;
    }
	else if (strncasecmp(line,"X-Mozilla-Status2: ",19)==0) {
      XMozillaStatusStr2 = QString(line+19);
      lastStr = &XMozillaStatusStr2;
    }
    else if (strncasecmp(line,"Date:",5)==0)
    {
      dateStr = QString(line_stripped+5);
	  Q_ASSERT(!dateStr.isEmpty());
      lastStr = &dateStr;
    }
    else if (strncasecmp(line,"From:", 5)==0)
    {
      fromStr = QString(line+5);
      lastStr = &fromStr;
    }
    else if (strncasecmp(line,"To:", 3)==0)
    {
      toStr = QString(line+3);
      lastStr = &toStr;
    }
    else if (strncasecmp(line,"Subject:",8)==0)
    {
      subjStr = QString(line_stripped+8);
      lastStr = &subjStr;
    }
	else if (strncasecmp(line,"Received:",9)==0)
	{
		bIncoming = true;
		lastStr = NULL;
	}
    else if (strncasecmp(line,"X-Length:",9)==0)
    {
      sizeServerStr = QString(line+9);
      sizeServer = sizeServerStr.toULong();
      lastStr = &sizeServerStr;
    }
    else if (strncasecmp(line,"X-UID:",6)==0)
    {
      uidStr = QString(line+6);
      uid = uidStr.toULong();
      lastStr = &uidStr;
    }
    else if (strncasecmp(line, "Content-Type:", 13) == 0)
    {
      contentTypeStr = QString(line+13);
      lastStr = &contentTypeStr;
    }
  }
}

bool MboxParser::IsThunderbirdInstalled()
{
	QString strValue;
	if(!DataHelper::Registry_GetKeyValue("HKEY_LOCAL_MACHINE", "SOFTWARE\\Mozilla\\Mozilla Thunderbird",strValue))
		return false;
	return true;
}

QString MboxParser::GetThunderbirdProfileDir()
{
	if(DataHelper::IsPortableVersion())
	{
		if(!g_pClientManager->GetIniFile()->m_strPortableThunderbirdProfile.isEmpty())
		{
			QString strDir = g_pClientManager->GetIniFile()->m_strPortableThunderbirdProfile;
			DataHelper::ReplaceCurrentDriveInPath(strDir);
			return strDir;
		}
	}
	return DataHelper::GetHomeDir() + "/Thunderbird/Profiles";
}

QDateTime MboxParser::ParseMsgDate(QString strDate)
{
	if(strDate.isEmpty()){
		//Q_ASSERT(FALSE);
		return QDateTime();
	}

	//SAMPLE input formats:
	//"Tue, 29 Apr 2008 10:08:31 +0200"
	//"Mon, 05 May 2008 09:59:37 +0200"
	//"Mon, 22 May 06 10:24:24 Central European Standard Time"
	//"Tue, 16 Jan 2007 18:12 -0500 (EST)"
	strDate = strDate.trimmed();
	int nTmpPos = strDate.indexOf(',');
	if(nTmpPos >= 0){
		//strip day name
		strDate = strDate.mid(nTmpPos+1);
		strDate = strDate.trimmed();
	}
	//strDate = strDate.mid(0, 20);
	strDate = strDate.trimmed();

	//parse date/time in format "29 Apr 2008 10:08:31"
	QString strDateOnly;
	QString strTimeOnly;
	//find 3rd space
	nTmpPos = strDate.indexOf(' ');
	if(nTmpPos >=0){
		nTmpPos = strDate.indexOf(' ', nTmpPos+1);
		if(nTmpPos >=0){
			nTmpPos = strDate.indexOf(' ', nTmpPos+1);
			if(nTmpPos >=0){
				strDateOnly = strDate.mid(0, nTmpPos);
				
				strTimeOnly = strDate.mid(nTmpPos+1);
				//cut time after 1st space
				nTmpPos = strTimeOnly.indexOf(' ');
				if(nTmpPos >=0)
					strTimeOnly = strTimeOnly.mid(0, nTmpPos);
				if(strTimeOnly.length() < 8)
					strTimeOnly += ":00";	//add missing seconds
			}
		}
	}
	QTime timePart = QTime::fromString(strTimeOnly, "hh:mm:ss");
	Q_ASSERT(timePart.isValid());
	//qDebug() << "hour" << timePart.hour();

	//date part is more complex, month name parsing does not work correctly (depends on locale)
	QDate datePart; // = QDate::fromString(strDateOnly, "dd MMM yyyy");
	int nYear = -1;
	int nMonth = -1;
	int nDay = -1;
	nTmpPos = strDateOnly.lastIndexOf(' ');
	if(nTmpPos >=0){
		nYear = strDateOnly.mid(nTmpPos+1).toInt();
		if(nYear < 100)
			nYear += 2000;

		strDateOnly = strDateOnly.mid(0, nTmpPos);
		nTmpPos = strDateOnly.indexOf(' ');
		if(nTmpPos >=0){
			nDay = strDateOnly.mid(0, nTmpPos).toInt();
			strDateOnly = strDateOnly.mid(nTmpPos+1);

			//what's left is a month name in English always
			if("Jan" == strDateOnly)
				nMonth = 1;
			else if("Feb" == strDateOnly)
				nMonth = 2;
			else if("Mar" == strDateOnly)
				nMonth = 3;
			else if("Apr" == strDateOnly)
				nMonth = 4;
			else if("May" == strDateOnly)
				nMonth = 5;
			else if("Jun" == strDateOnly)
				nMonth = 6;
			else if("Jul" == strDateOnly)
				nMonth = 7;
			else if("Aug" == strDateOnly)
				nMonth = 8;
			else if("Sep" == strDateOnly)
				nMonth = 9;
			else if("Oct" == strDateOnly)
				nMonth = 10;
			else if("Nov" == strDateOnly)
				nMonth = 11;
			else if("Dec" == strDateOnly)
				nMonth = 12;
		}
	}

	datePart = QDate(nYear, nMonth, nDay);
	Q_ASSERT(datePart.isValid());

	return QDateTime(datePart, timePart).toUTC();
}

#endif // WINCE
