#ifndef MBOXPARSER
#define MBOXPARSER




#include <stdio.h>
#include <QString>
#include <QDateTime>
#include <vector>

//X-Mozilla-Status flags
#define MSG_FLAG_READ  			0x0001  //Message has been read.
#define MSG_FLAG_REPLIED 		0x0002 	//A reply has been successfully sent.
#define MSG_FLAG_MARKED			0x0004 	//The user has flagged this message.
#define MSG_FLAG_EXPUNGED		0x0008 	//Already gone (when folder not compacted). Since actually removing a message from a folder is a semi-expensive operation, we tend to delay it; messages with this bit set will be removed the next time folder compaction is done. Once this bit is set, it never gets un-set.
#define MSG_FLAG_HAS_RE			0x0010 	//Whether subject has �Re:� on the front. The folder summary uniquifies all of the strings in it, and to help this, any string which begins with �Re:� has that stripped first. This bit is then set, so that when presenting the message, we know to put it back (since the �Re:� is not itself stored in the file).
#define MSG_FLAG_ELIDED			0x0020 	//Whether the children of this sub-thread are folded in the display.
#define MSG_FLAG_OFFLINE		0x0080 	//DB has offline news or imap article.
#define MSG_FLAG_WATCHED		0x0100 	//If set, this thread is watched.
#define MSG_FLAG_SENDER_AUTHED 	0x0200 	//If set, then this message's sender has been authenticated when sending this msg. This means the POP3 server gave a positive answer to the XSENDER command. Since this command is no standard and only known by few servers, this flag is unmeaning in most cases.
#define MSG_FLAG_PARTIAL 		0x0400 	//If set, then this message's body contains not the whole message, and a link is available in the message to download the rest of it from the POP server. This can be only a few lines of the message (in case of size restriction for the download of messages) or nothing at all (in case of �Fetch headers only�)
#define MSG_FLAG_QUEUED 		0x0800 	//If set, this message is queued for delivery. This only ever gets set on messages in the queue folder, but is used to protect against the case of other messages having made their way in there somehow � if some other program put a message in the queue, it won't be delivered later!
#define MSG_FLAG_FORWARDED		0x1000 	//This message has been forwarded.
#define MSG_FLAG_PRIORITIES 	0xE000 	//These are used to remember the message priority in interal status flags.

//X-Mozilla-Status2
#define MSG_FLAG_NEW  			0x00010000		//This message is new since the last time the folder was closed.
#define MSG_FLAG_IGNORED 		0x00040000 		//If set, this thread is ignored.
#define MSG_FLAG_IMAP_DELETED 	0x00200000 		//If set, this message is marked as deleted on the server. This only applies to messages on IMAP servers.
#define MSG_FLAG_MDN_REPORT_NEEDED 	0x00400000 	//This message required to send a MDN to the sender of the message. For information about MDN see http://www.mozilla.org/quality/mailnews/tests/sea-mn-basfunc-return-receipt.html.
#define MSG_FLAG_MDN_REPORT_SENT 	0x00800000 	//An MDN report message has been sent for this message. No more MDN report should be sent to the sender.
#define MSG_FLAG_TEMPLATE 		0x01000000 		//If set, this message is a template.
#define MSG_FLAG_LABELS 		0x0E000000 		//These are used to store the message label.
#define MSG_FLAG_ATTACHMENT 	0x10000000 		//If set, this message has files attached to it.

class MailInfo
{
public:
	bool m_bIncoming;
	QDateTime m_dtSent;
	QString m_strSubject;
	QString m_strFrom;
	QString m_strContent;
	int m_nStartOffset;
	unsigned int m_nXMozillaStatus;
	unsigned int m_nXMozillaStatus2;
};

#ifndef WINCE

class MboxParser
{
public:
    MboxParser();
    virtual ~MboxParser();

	bool open(QString &strFile);
	void close();
	void buildMailIndex(int nStartOffset = 0);

	static bool    IsThunderbirdInstalled();
	static QString GetThunderbirdProfileDir();
	static QDateTime ParseMsgDate(QString strDate);

public:
	std::vector<MailInfo> m_lstMsgs;

private:
	FILE *m_pStream; 
};

#endif // WINCE


#endif // MBOXPARSER
