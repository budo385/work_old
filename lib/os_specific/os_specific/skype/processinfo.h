#ifndef _PROESSINFO_H_
#define _PROESSINFO_H_

//Check if there exist the process started
//from a given exe file
//
//Example: is_process_started(L"Skype.exe");
#ifndef WINCE
	bool is_process_started(LPCTSTR szLibrary);
#else
	bool is_process_started(LPCTSTR szLibrary){};	
#endif

#endif   //_PROESSINFO_H_

