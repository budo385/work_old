// Skype core code
#include "skypeparser.h"
#include "skypemanager.h"
#include <QtWidgets/QMessageBox>
#include <QDebug>
#include "common/common/statuscodeset.h"
#include "common/common/loggerabstract.h"

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

CallInfo::CallInfo()
{
	m_nCallID = -1;
	m_nConfID = -1;
	m_nStatus = CS_INITIAL;
	m_bOutCall = false;
	m_bTransfered = false;
}

//this counter generates unique ID for each command executed
//int	SkypeParser::m_nCmdIDCounter = 0;

SkypeParser::SkypeParser(SkypeCallHanlder *pCallHandler) 
{
	m_SkypeCallHandler=pCallHandler;
	m_pSkypeBuddiesFui = NULL;
	m_pSkypeUserInfoFui = NULL;
	m_nCurOutCallFUI = -1;
	m_nCurTransferedCallID = -1;
	m_nCurCallID = -1;
	m_nCurConferenceSpeakers = 0;
	m_bSMSWaitResult = false;
	m_bVoiceMailEvent = false;
	AttachToLine();
}

SkypeParser::~SkypeParser() 
{
	DetachFromLine();
}

void SkypeParser::StartCall(QString strNumber)
{
	//TOFIX generate new command ID to track this command answer
	QString strCmd = "CALL " + strNumber;
	m_strLastCallCommand=strCmd;
	SkypeManager::SendCommand(strCmd.toLatin1().constData());
}

void SkypeParser::ProcessIncomingMsg(QString strMsg)
{
	qDebug() << "Skype MSG (incoming): " << strMsg;
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Skype MSG (incoming): " + strMsg);

	if(0 == strMsg.indexOf("CALL"))
	{
		int nPos = strMsg.indexOf("DURATION");
		if(nPos > 0)
			return; //skip message box output
		nPos = strMsg.indexOf(" SEEN "); // "CALL 136 SEEN TRUE" when missed call clicked in Skype above history list
		if(nPos > 0)
			return; //skip message box output

		//extract call ID (next token)
		nPos = strMsg.indexOf(" ", 5);
		if(nPos > 0)
		{
			QString strCallID = strMsg.mid(5, nPos-5); 
			int nCallID = strCallID.toInt();

			//is this a new call
			if(m_mapCall2Fui.find(nCallID) == m_mapCall2Fui.end())
			{
				bool bSokratesIsOriginOfCall=(m_nCurOutCallFUI >= 0);
				bool bOutCall = (m_nCurOutCallFUI >= 0);

				//detect also outgoing calls that were initiated on Skype client
				//(not in VoiceCallCenter FUI window)
				//TOFIX proper way to get call type is to send a command "GET CALL xxx TYPE"
				bool bStatusRouting=false;
				if(strMsg.indexOf("STATUS ROUTING") >= 0)
				{
					bOutCall = true;
					bStatusRouting=true;
				}

				//obey m_bListenIncomingCalls setting
				if(!bOutCall && !SkypeManager::GetListenIncomingCalls())
					return;

				int nFUI = m_nCurOutCallFUI;
				if(nFUI < 0)
				{
					if(m_SkypeCallHandler){
						g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Open Skype for incoming call"));

						nFUI = m_SkypeCallHandler->OpenSkypeIncomingCallHandler();
					}

					//MB: fixes bug 942: Incoming Phone Call: Window Not in Front 
					SkypeInterface* pFUI = NULL;
					if(m_SkypeCallHandler)pFUI = m_SkypeCallHandler->GetSkypeIncomingCallHandler(nFUI);
					if(pFUI){
						//Windows flags code: fixes bug 1407: Window in Vista not in front
						//Qt::WindowFlags flagsOrig = pFUI->windowFlags();
						//pFUI->setWindowFlags(flagsOrig|Qt::WindowStaysOnTopHint);
						pFUI->Wnd_ActivateWindow();
						//pFUI->setWindowFlags(flagsOrig);
					}
				}
				
				m_mapCall2Fui[nCallID] = nFUI;
				m_nCurOutCallFUI = -1;	//reset flag
				Info_OnCallCreated(nCallID, bOutCall,bStatusRouting,bSokratesIsOriginOfCall);

				SkypeInterface* pFUI = NULL;
				if(m_SkypeCallHandler)pFUI =m_SkypeCallHandler->GetSkypeIncomingCallHandler(nFUI);
				if(pFUI) pFUI->SetWindowState( (bOutCall)? SkypeInterface::WND_STATE_Ringing_Out : SkypeInterface::WND_STATE_Ringing_Inc);

				if(m_nCurTransferedCallID >= 0)
				{
					if(pFUI) pFUI->SetCallID(nCallID);

					//mark this call as being required to join the conference
					if(pFUI) pFUI->SetConferenceID(m_nCurTransferedCallID);
					m_nCurConferenceSpeakers ++;
					//m_nCurTransferedCallID = -1;
				}
				else if (m_nCurCallID >= 0)
				{
					//mark new call as being required to join the conference
					if(pFUI) pFUI->SetConferenceID(m_nCurCallID);
					m_nCurConferenceSpeakers ++;
					if(pFUI) pFUI->SetCallID(nCallID);
				}
				else{
					if(pFUI) pFUI->SetCallID(nCallID);
					m_nCurCallID = nCallID;
				}

				//ask for caller info
				if(!(bSokratesIsOriginOfCall && bOutCall))
				{
					QString strCmd = QString().sprintf("GET CALL %d PARTNER_HANDLE", nCallID);
					SkypeManager::SendCommand(strCmd.toLatin1().constData());
				}

				return; //skip message box output
			}
			else	// already existing call
			{
				int nFUI = m_mapCall2Fui[nCallID];
				SkypeInterface* pFUI = NULL;
				if(m_SkypeCallHandler)pFUI =m_SkypeCallHandler->GetSkypeIncomingCallHandler(nFUI);

				nPos = strMsg.indexOf("PARTNER_HANDLE");
				if(nPos > 0)
				{
					int nPosX = Info_GetCallIdx(nCallID);
					if(nPosX >= 0)
					{
						if (m_lstCurCalls[nPosX].m_bOutCall && m_lstCurCalls[nPosX].m_bSokratesIsOriginOfCall) //if outgoing call skip retrieving caller data..we already have that..
							return;
					}

					QString strHandle = strMsg.mid(nPos + strlen("PARTNER_HANDLE") + 1).trimmed();
					if(pFUI) pFUI->SetCaller(strHandle);
					return; //skip message box output
				}
				else
				{
					nPos = strMsg.indexOf(" STATUS");
					if(nPos > 0)
					{
						QString strStatus = strMsg.mid(nPos + strlen(" STATUS") + 1).trimmed();
						if(strStatus=="MISSED") //missed is valid state, but can be due to skype/windows bug
						{
							//check MISSED status
							int nPos = Info_GetCallIdx(nCallID);
							if(nPos >= 0) //get previous state
							{
								if(m_lstCurCalls[nPos].m_nStatus==CS_ROUTING && m_lstCurCalls[nPos].m_bOutCall) //if last status = missed then engage retry
								{
									//remove all from last call
									//m_lstCurCalls.erase(m_lstCurCalls.begin()+nPos);
									ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Retrying call: "+m_strLastCallCommand);
									m_nCurOutCallFUI=m_mapCall2Fui[nCallID]; //remember us...
									//m_mapCall2Fui.remove(nCallID);
									SkypeManager::SendCommand(m_strLastCallCommand.toLatin1().constData());
								}
								return; 
							}
						}

						if(pFUI) pFUI->SetCallState(strStatus);
						Info_OnCallStatusChanged(nCallID, strStatus);	// internal bookkeeping
						return; //skip message box output
					}
					else{
						nPos = strMsg.indexOf(" TRANSFER_STATUS");
						if(nPos > 0)
						{
							QString strStatus = strMsg.mid(nPos + strlen(" TRANSFER_STATUS") + 1).trimmed();
							if(pFUI) pFUI->SetCallState("FORWARDED");
							Info_OnCallStatusChanged(nCallID, "FORWARDED");	// internal bookkeeping
							return; //skip message box output
						}
						else{
							nPos = strMsg.indexOf(" CONF_ID");
							if(nPos > 0)
							{
								int nConfID = strMsg.mid(nPos + strlen(" CONF_ID") + 1).trimmed().toInt();
								Info_OnCallConferenceID(nCallID, nConfID);
							}
						}
					}
				}
			}
		}
	}
	else if(0 == strMsg.indexOf("USERS "))
	{
		//parse "buddy list" result ("SEARCH FRIENDS")
		if(NULL != m_pSkypeBuddiesFui)
		{
			//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->SetSkypeBuddiesReply(strMsg);
			m_pSkypeBuddiesFui->SetSkypeBuddiesReply(strMsg);
		}
	}
	else if(0 == strMsg.indexOf("CURRENTUSERHANDLE "))
	{
		m_strMyHandle = strMsg.mid(strlen("CURRENTUSERHANDLE ")); 	

		//get my full name
		SkypeManager::SendCommand("GET PROFILE FULLNAME");	
	}
	else if(0 == strMsg.indexOf("PROFILE FULLNAME "))
	{
		m_strMyFullName = strMsg.mid(strlen("PROFILE FULLNAME ")); 	
	}
	else if(0 == strMsg.indexOf("USER "))
	{
		int nPos = strMsg.indexOf(" FULLNAME ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strFullName = strMsg.mid(nPos+strlen(" FULLNAME "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_FULL_NAME,strHandle,strFullName);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_FullName(strHandle, strFullName);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_FULL_NAME,strHandle,strFullName);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_FullName(strHandle, strFullName);
			}
			return;
		}
		
		//parse birthday
		nPos = strMsg.indexOf(" BIRTHDAY ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" BIRTHDAY "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_BIRTHDAY,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_Birthday(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_BIRTHDAY,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_Birthday(strHandle, strData);
			}
			return;
		}

		//parse sex
		nPos = strMsg.indexOf(" SEX ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" SEX "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_SEX,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_Sex(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_SEX,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_Sex(strHandle, strData);
			}
			return;
		}

		//parse country
		nPos = strMsg.indexOf(" COUNTRY ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" COUNTRY "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_COUNTRY,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_Country(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_COUNTRY,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_Country(strHandle, strData);
			}
			return;
		}

		//parse province
		nPos = strMsg.indexOf(" PROVINCE ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" PROVINCE "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_PROVINCE,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_Province(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_PROVINCE,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_Province(strHandle, strData);
			}
			return;
		}

		//parse city
		nPos = strMsg.indexOf(" CITY ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" CITY "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_CITY,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_City(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_CITY,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_City(strHandle, strData);
			}
			return;
		}

		//parse PHONE_HOME
		nPos = strMsg.indexOf(" PHONE_HOME ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" PHONE_HOME "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_PHONE_HOME,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_PhoneHome(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_PHONE_HOME,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_PhoneHome(strHandle, strData);
			}
			return;
		}

		//parse PHONE_OFFICE
		nPos = strMsg.indexOf(" PHONE_OFFICE ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" PHONE_OFFICE "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_PHONE_OFFICE,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_PhoneOffice(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_PHONE_OFFICE,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_PhoneOffice(strHandle, strData);
			}
			return;
		}

		//parse PHONE_MOBILE
		nPos = strMsg.indexOf(" PHONE_MOBILE ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" PHONE_MOBILE "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_PHONE_MOBILE,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_PhoneMobile(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_PHONE_MOBILE,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_PhoneMobile(strHandle, strData);
			}
			return;
		}

		//parse HOMEPAGE
		nPos = strMsg.indexOf(" HOMEPAGE ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" HOMEPAGE "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_HOMEPAGE,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_Homepage(strHandle, strData);
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_HOMEPAGE,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_Homepage(strHandle, strData);
			}
			return;
		}

		//parse ABOUT
		nPos = strMsg.indexOf(" ABOUT ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" ABOUT "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_ABOUT,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_About(strHandle, strData);
				m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
			}
			else if(NULL != m_pSkypeBuddiesFui)	{
				//multiple resolving
				m_pSkypeBuddiesFui->OnUserResolved(SkypeInterface::SKYPE_ABOUT,strHandle,strData);
				//dynamic_cast<FUI_ImportContact *>(m_pSkypeBuddiesFui)->OnUserResolved_About(strHandle, strData);
			}
			return;
		}

		//parse ONLINESTATUS 
		nPos = strMsg.indexOf(" ONLINESTATUS ");
		if(nPos > 0)
		{
			QString strHandle	= strMsg.mid(strlen("USER "), nPos-strlen("USER "));
			QString strData = strMsg.mid(nPos+strlen(" ONLINESTATUS "));

			//decide who to send this resolved data
			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnUserResolved(SkypeInterface::SKYPE_STATUS,strHandle,strData);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnUserResolved_Status(strHandle, strData);
				m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
			}
			return;
		}
	}
	else if(0 == strMsg.indexOf("CHAT "))
	{
		//parse NAME 
		int nPos = strMsg.indexOf(" NAME ");
		if(nPos > 0)
		{
			QString strName = strMsg.mid(nPos+strlen(" NAME "));

			if(NULL != m_pSkypeUserInfoFui){
				m_pSkypeUserInfoFui->OnChatResolved(strName);
				//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnChatResolved_Name(strName);
				m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
			}
			else{
				//case when starting from SAPNE
				QString strCmd = QString().sprintf("OPEN CHAT %s", strName.toLatin1().constData());
				SkypeManager::SendCommand(strCmd.toLatin1().constData());
			}
		}
		else{
			//sometimes only this message is sent !?
			nPos = strMsg.indexOf(" STATUS ");

			if(nPos > 0)
			{
				QString strName = strMsg.mid(strlen("CHAT "), nPos-strlen("CHAT "));

				if(NULL != m_pSkypeUserInfoFui){
					m_pSkypeUserInfoFui->OnChatResolved(strName);
					//dynamic_cast<FUI_VoiceCallCenter *>(m_pSkypeUserInfoFui)->OnChatResolved_Name(strName);
					m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
				}
				else{
					//case when starting from SAPNE
					QString strCmd = QString().sprintf("OPEN CHAT %s", strName.toLatin1().constData());
					SkypeManager::SendCommand(strCmd.toLatin1().constData());
				}
			}
		}
	}
	else if(0 == strMsg.indexOf("SMS "))
	{
		//extract SMS ID number (first argument after "SMS ")
		QString strID;
		int nPos = strMsg.indexOf(" ", 4);
		if(nPos > 0)
			strID = strMsg.mid(strlen("SMS "), nPos-strlen("SMS "));
/*
		if(m_bSMSWaitResult)
		{
			int nPos = strMsg.indexOf(" FAILUREREASON ");
			if(nPos > 0)
			{
				QString strTxt = strMsg.mid(nPos+strlen(" FAILUREREASON "));
				if(NULL != m_pSkypeUserInfoFui){
					((SMS_Dialog *)m_pSkypeUserInfoFui)->on_SMS_Failed(strID, strTxt);
					m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
				}
				m_bSMSWaitResult = false;
			}
			else if( strMsg.indexOf(" STATUS DELIVERED ") > 0 //|| strMsg.indexOf(" STATUS SENT_TO_SERVER ") > 0
			)
			{
				//success, clean state
				if(strMsg.indexOf(" STATUS DELIVERED ") > 0)
				{
					QString strTxt("DELIVERED");
					if(NULL != m_pSkypeUserInfoFui){
						((SMS_Dialog *)m_pSkypeUserInfoFui)->on_SMS_Sent(strID, strTxt);
						m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
					}
				}
				m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
				m_bSMSWaitResult = false;
			}
			else if( strMsg.indexOf(" TYPE OUTGOING") > 0)
			{
				m_bSMSWaitResult = false;
				if(NULL != m_pSkypeUserInfoFui){
					((SMS_Dialog *)m_pSkypeUserInfoFui)->on_SMS_Created(strID);
				//m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
				}
			}
		}
		else
*/
		{
			if(NULL != m_pSkypeUserInfoFui)
			{
				//((SMS_Dialog *)m_pSkypeUserInfoFui)->on_SMS_Created(strID);
				m_pSkypeUserInfoFui->OnSMSCreated(strID);
				//m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
				m_pSkypeUserInfoFui = NULL;	// reset pointer after the batch has been processed
			}
		}
	}
	else if(0 == strMsg.indexOf("VOICEMAIL "))
	{
		m_bVoiceMailEvent = true;

		//extract ID number (first argument after "VOICEMAIL ")
		QString strID;
		int nPos = strMsg.indexOf(" ", strlen("VOICEMAIL "));
		if(nPos > 0){
			strID = strMsg.mid(strlen("VOICEMAIL "), nPos-strlen("VOICEMAIL "));
			int nID = strID.toInt();

			qDebug() << "Voicemail message ID=" << nID << " Msg:" << strMsg;

			int nPos = strMsg.indexOf(" STATUS PLAYING");
			if(nPos > 0){
				m_nVoiceMailIncID = nID;
				qDebug() << "Voicemail incoming message ID=" << nID;
			}
			else{
				nPos = strMsg.indexOf(" TYPE OUTGOING");
				if(nPos > 0){
					m_nVoiceMailOutID = nID;
					qDebug() << "Voicemail outgoing message ID=" << nID;
				}
			}

			//update FUI
			int nFUI = m_mapCall2Fui[m_nVoiceCallID];
			SkypeInterface* pFUI = NULL;
			if(m_SkypeCallHandler)pFUI =m_SkypeCallHandler->GetSkypeIncomingCallHandler(nFUI);
			if(pFUI){
				if(m_nVoiceMailIncID >= 0)
					pFUI->SetVoiceMailIncID(m_nVoiceMailIncID);
				if(m_nVoiceMailOutID >= 0)
					pFUI->SetVoiceMailOutID(m_nVoiceMailOutID);
			}
		}
	}

//#ifdef _DEBUG
	if(strMsg.indexOf("ERROR") >= 0)
	{
		if(strMsg.indexOf("ERROR 8 Invalid user handle") >= 0)
		{
			//asked status for invalid handle
			m_pSkypeUserInfoFui = NULL;
		}
		else if(strMsg.indexOf("VOICEMAIL") >= 0)
		{
			//filter out this error message
		}
		else
			QMessageBox::information(NULL, "Skype", strMsg);
	}
//#endif
}

void SkypeParser::AttachToLine()
{
	SkypeManager::SetParser(this);
}

void SkypeParser::DetachFromLine()
{
	SkypeManager::SetParser(NULL);
}

int SkypeParser::Info_GetCallIdx(int nCallID)
{
	int nMax = Info_GetCallsCount();
	for(int i=0; i<nMax; i++)
		if(nCallID == m_lstCurCalls[i].m_nCallID)
			return i;
	return -1;
}

bool SkypeParser::Info_OnCallCreated(int nCallID, bool bOutCall, bool bStatusRouting,bool bSokratesIsOriginOfCall)
{
	Q_ASSERT(Info_GetCallIdx(nCallID) < 0); // do not add same call twice into the list

	CallInfo info;
	info.m_nCallID = nCallID;
	info.m_bOutCall = bOutCall;
	info.m_bSokratesIsOriginOfCall = bSokratesIsOriginOfCall;
	if (bStatusRouting)
		info.m_nStatus=CS_ROUTING;
	else
		info.m_nStatus=CS_INITIAL;
	//TOFIX status
	m_lstCurCalls.push_back(info);

	return true;
}

bool SkypeParser::Info_OnCallEnded(int nCallID)
{
	int nPos = Info_GetCallIdx(nCallID);
	if(nPos >= 0){
		m_lstCurCalls.erase(m_lstCurCalls.begin()+nPos);
		qDebug() << "Skype: Call #" << nCallID << " erased (num calls: " << Info_GetCallsCount() << ")";
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Skype handler: Skype: Call #%1 erased (num calls: %2").arg(nCallID).arg(Info_GetCallsCount()));
		return true;
	}
	return false;
}

void SkypeParser::Info_OnCallStatusChanged(int nCallID, QString strStatus)
{
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Skype handler: set call %1 status %2").arg(nCallID).arg(strStatus));

	if(QString("FINISHED") == strStatus){
		m_bVoiceMailEvent = false;

		//check if the call will be replaced with a voicecall - do not clear immediately
		if(NULL == m_pVoiceCheckTimer){
			m_nVoiceCallID = nCallID;
			m_pVoiceCheckTimer = new QTimer();
			m_pVoiceCheckTimer->setSingleShot(true);
			connect(m_pVoiceCheckTimer, SIGNAL(timeout()), this, SLOT(UpdateCallVoiceStatus()));
			m_pVoiceCheckTimer->start(1000);	//ms
		}
	}
	else if( QString("FAILED") == strStatus ||
		QString("MISSED") == strStatus	 ||
		QString("BUSY") == strStatus	 ||
		QString("REFUSED") == strStatus  ||
		QString("FORWARDED") == strStatus)
	{
		qDebug() << "Skype: Call #" << nCallID << " ended with status: " << strStatus;
		Info_OnCallEnded(nCallID);
	}
	else if(QString("INPROGRESS") == strStatus)
	{
		int nPos = Info_GetCallIdx(nCallID);
		if(nPos >= 0)
			m_lstCurCalls[nPos].m_nStatus = CS_INPROGRESS;
	}
	else if(QString("ONHOLD") == strStatus)
	{
		int nPos = Info_GetCallIdx(nCallID);
		if(nPos >= 0)
			m_lstCurCalls[nPos].m_nStatus = CS_ONHOLD;
	}
	else if(QString("ROUTING") == strStatus) //important to disctinct change of state from routing to missed (win7/8 and skype>ver6 error) and from routing to ringing (valid)
	{
		int nPos = Info_GetCallIdx(nCallID);
		if(nPos >= 0)
			m_lstCurCalls[nPos].m_nStatus = CS_ROUTING;
	}
	else if(QString("RINGING") == strStatus)
	{
		int nPos = Info_GetCallIdx(nCallID);
		if(nPos >= 0)
			m_lstCurCalls[nPos].m_nStatus = CS_RINGING;
	}
	else{
		//etc states
		int nPos = Info_GetCallIdx(nCallID);
		if(nPos >= 0)
			m_lstCurCalls[nPos].m_nStatus = CS_INITIAL;
	}
}

// should this call join the conference	
int  SkypeParser::Info_CalcJoinConferenceID(int nCallID)
{
	//TOFIX do not join if transfer used
	int nPos = Info_GetCallIdx(nCallID);
	if(nPos >= 0){
		//already in the conference?
		if(m_lstCurCalls[nPos].m_nConfID > 0)
			return -1;

		//if there is another outgoing call, join new call to this one
		int nMax = Info_GetCallsCount();
		for(int i=0; i<nMax; i++)
			if( nCallID != m_lstCurCalls[i].m_nCallID &&
				m_lstCurCalls[i].m_bOutCall)
				return m_lstCurCalls[i].m_nCallID;	// join to already exiting call
	}

	return -1;
}

bool SkypeParser::Info_OnCallConferenceID(int nCallID, int nConfID)
{
	int nPos = Info_GetCallIdx(nCallID);
	if(nPos >= 0){
		qDebug() << "Skype: Call #" << nCallID << " is part of conference: " << nConfID;
		m_lstCurCalls[nPos].m_nConfID = nConfID;
		return true;
	}
	return false;
}

bool SkypeParser::Info_SetCallsOnHold()
{
	int nCnt = 0;
	int nMax = Info_GetCallsCount();
	for(int i=0; i<nMax; i++)
	{
		//TOFIX match system type to skype
		//only calls in progress can be put on hold
		if(m_lstCurCalls[i].m_nStatus != CS_INPROGRESS)
			continue;

		//put current call on hold until we create this new one
		QString strCmd = QString().sprintf("SET CALL %d STATUS ONHOLD", m_lstCurCalls[i].m_nCallID);
		SkypeManager::SendCommand(strCmd.toLatin1().constData());

		nCnt ++;
	}

	return (nCnt > 0);
}

CallInfo& SkypeParser::Info_GetCall(int nCallID)
{
	int nPos = Info_GetCallIdx(nCallID);
	if(nPos >= 0)
		return m_lstCurCalls[nPos];
	return m_lstDummy;
}

int SkypeParser::Info_GetTransferedCallID()
{
	int nMax = Info_GetCallsCount();
	for(int i=0; i<nMax; i++)
		if(m_lstCurCalls[i].m_bTransfered)
			return m_lstCurCalls[i].m_nCallID;

	return -1;
}

void SkypeParser::UpdateCallVoiceStatus()
{
	if(!m_bVoiceMailEvent){
		qDebug() << "Skype: Call #" << m_nVoiceCallID << " ended with status: FINISHED (after waiting for voicemail)";
		Info_OnCallEnded(m_nVoiceCallID);
	}
	else{
		//voice mail activated (but the main call finished)
		//simulate that the call is still alive because of the button states
		//Info_OnCallStatusChanged(m_nVoiceCallID, "INPROGRESS");
		int nFUI = m_mapCall2Fui[m_nVoiceCallID];
		SkypeInterface* pFUI = NULL;
		if(m_SkypeCallHandler)pFUI =m_SkypeCallHandler->GetSkypeIncomingCallHandler(nFUI);
		if(pFUI){
			if(m_nVoiceMailIncID >= 0)
				pFUI->SetVoiceMailIncID(m_nVoiceMailIncID);
			if(m_nVoiceMailOutID >= 0)
				pFUI->SetVoiceMailOutID(m_nVoiceMailOutID);
			pFUI->SetCallState("INPROGRESS");
		}
	}

	m_pVoiceCheckTimer->stop();
	delete m_pVoiceCheckTimer;
	m_pVoiceCheckTimer = NULL;
}
