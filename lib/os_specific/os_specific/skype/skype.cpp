// Skype core code
//#ifndef WINCE

	#include <qt_windows.h>
	//#define NOMINMAX
	//#include <windows.h> 
	#include <tchar.h> 
	#include <process.h> 
	#include <rpcdce.h>

#include "skype.h"

	#include "processinfo.h"

#include <QtGlobal>		//qDebug
#include <QtWidgets/QMessageBox>
#include <QDebug>
#include <QList>
#include <QDir>
#include "common/common/statuscodeset.h"
#include "common/common/loggerabstract.h"
#include "common/common/datahelper.h"
#include "portableskypesearchdlg.h"
#include "gui_core/gui_core/timemessagebox.h"

 #include <io.h>


#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;


static LRESULT APIENTRY SkypeWindowProc(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam);


//this Skype interface implementation is for Windows ONLY.
//#ifndef _WIN32
// #error "Windows is required for Skype."
//

#pragma comment(lib, "rpcrt4")	//automatical linking

#define TRACE qDebug	//static void TRACE (char *fmt , ...) {}


#define WND_CLASS_PREFIX	_T("SokratesXP_Skype-")


enum {
	SKYPECONTROLAPI_ATTACH_SUCCESS=0,				// Client is successfully attached
	SKYPECONTROLAPI_ATTACH_PENDING_AUTHORIZATION=1,	// Skype has acknowledged connection request and is waiting for confirmation from the user.
													// The client is not yet attached and should wait for SKYPECONTROLAPI_ATTACH_SUCCESS message
	SKYPECONTROLAPI_ATTACH_REFUSED=2,				// User has explicitly denied access to client
	SKYPECONTROLAPI_ATTACH_NOT_AVAILABLE=3,			// API is not available at the moment. For example, this happens when no user is currently logged in.
													// Client should wait for SKYPECONTROLAPI_ATTACH_API_AVAILABLE broadcast before making any further
													// connection attempts.
	SKYPECONTROLAPI_ATTACH_API_AVAILABLE=0x8001
};


// SKYPE global variables
static HINSTANCE g_hCurrentProcess = NULL;		// handle for current process
static TCHAR g_szWndClassName[128];
static HWND g_hWndSkype = NULL;					// Skype window
static HWND g_hWndTalker = NULL;				// invisible window to exchange messages with Skype

static unsigned int g_nMsgID_SkypeAPIAttach = 0;		// windows message ID
static unsigned int g_nMsgID_SkypeAPIDiscover = 0;		// windows message ID
static bool g_bSkypeLogged = false;
static QList<QString> g_lstCmdCache;

#if defined(_DEBUG)
 bool volatile g_bDumpWindowsMessages = true;
#else
 bool volatile g_bDumpWindowsMessages = false;
#endif

// static vars
SkypeParser *SkypeLine::m_pParser = NULL;
bool SkypeLine::m_bInited = false;
bool SkypeLine::m_bWndCreated = false;
bool SkypeLine::m_bListenIncomingCalls = false;
SkypeLine* SkypeLine::m_pInstance = NULL;	// singleton object

SkypeLine* SkypeLine::getInstance()
{
	if (NULL == m_pInstance)
		m_pInstance = new SkypeLine;
	return m_pInstance;
}

void SkypeLine::releaseInstance() 
{ 
	if (NULL != m_pInstance) { 
		delete m_pInstance; 
		m_pInstance = NULL; 
	} 
}

//
// callback - send data back to $callback(pLong,pString) of OMNIS object
//

void lineCallbackFunc(PVOID pszMsg, DWORD dwStrSizeBytes) 
{
	QString strData;
	if(NULL != pszMsg)
		strData = QString::fromUtf8((const char *)pszMsg);

	//parse input
	if(NULL != SkypeLine::m_pParser)
		SkypeLine::m_pParser->ProcessIncomingMsg(strData);
}


bool SkypeLine::Init_CreateWindowClass()
{

	unsigned char *paucUUIDString = NULL;
	RPC_STATUS lUUIDResult;
	bool bReturnStatus;
	UUID oUUID;
	
	bReturnStatus=false;
	lUUIDResult=UuidCreate(&oUUID);
	g_hCurrentProcess = (HINSTANCE)OpenProcess(PROCESS_DUP_HANDLE, FALSE, GetCurrentProcessId());
	if( g_hCurrentProcess != NULL && 
		(lUUIDResult==RPC_S_OK || lUUIDResult==RPC_S_UUID_LOCAL_ONLY) )
	{
		if(RPC_S_OK == UuidToString(&oUUID, (RPC_WSTR *)&paucUUIDString))
		{
			//prepare unique window class name
			_tcscpy((char *)g_szWndClassName, WND_CLASS_PREFIX);
			_tcscat((char *)g_szWndClassName, (char *)paucUUIDString);

			WNDCLASS oWC;
			oWC.style			= CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS;
			oWC.lpfnWndProc		= (WNDPROC)&SkypeWindowProc;
			oWC.cbClsExtra		= 0;
			oWC.cbWndExtra		= 0;
			oWC.hInstance		= g_hCurrentProcess;
			oWC.hIcon			= NULL;
			oWC.hCursor			= NULL;
			oWC.hbrBackground	= NULL;
			oWC.lpszMenuName	= NULL;
			oWC.lpszClassName	= g_szWndClassName;

			if(RegisterClass(&oWC) != 0)
				bReturnStatus = true;
			
			RpcStringFree((RPC_WSTR *)&paucUUIDString);
		}
	}
	if(!bReturnStatus){
		CloseHandle(g_hCurrentProcess);
		g_hCurrentProcess = NULL;
	}
	return(bReturnStatus);

	return false;

}

bool SkypeLine::Init_CreateMainWindow()
{

	//creates invisible window required to exchange
	//windows messages with Skype
	g_hWndTalker = CreateWindowEx(  WS_EX_APPWINDOW|WS_EX_WINDOWEDGE,
									g_szWndClassName, L"",
									WS_BORDER|WS_SYSMENU|WS_MINIMIZEBOX,
									CW_USEDEFAULT, CW_USEDEFAULT,
									128, 128, NULL, 0, g_hCurrentProcess, 0);
	return (g_hWndTalker != NULL);

	return false;

}

void SkypeLine::Cleanup_DestroyWindowClass()
{

	UnregisterClass(g_szWndClassName, g_hCurrentProcess);
	CloseHandle(g_hCurrentProcess);
	g_hCurrentProcess = NULL;

}

void SkypeLine::Cleanup_DestroyMainWindow()
{

	if(g_hWndTalker != NULL){
		DestroyWindow(g_hWndTalker);
		g_hWndTalker = NULL;
	}

}

//
// SkypeLine
//
bool SkypeLine::Initialize() 
{
	

	bool bResult = true;
	if(!m_bInited) 
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Skype initialize");

		//.. try to init
		if(0 == g_nMsgID_SkypeAPIAttach){
			g_nMsgID_SkypeAPIAttach		= RegisterWindowMessage(L"SkypeControlAPIAttach");
			g_nMsgID_SkypeAPIDiscover	= RegisterWindowMessage(L"SkypeControlAPIDiscover");
		}
		
		bool bOK=false;
		long nInitStage=0;
		if( g_nMsgID_SkypeAPIAttach != 0 &&
			g_nMsgID_SkypeAPIDiscover != 0)
		{
			nInitStage = 1;
			if(m_bWndCreated)
			{
				nInitStage = 3;
				bOK = true;
			}
			else if(Init_CreateWindowClass())
			{
				nInitStage = 2;
				if(Init_CreateMainWindow())
				{
					m_bWndCreated = true;
					nInitStage = 3;
					bOK = true;
				}
			}
		}
		
		if(bOK){
			// initiate SkypeAPI discovery process
			::SendMessageTimeout(HWND_BROADCAST, g_nMsgID_SkypeAPIDiscover, (WPARAM)g_hWndTalker, 0, SMTO_ABORTIFHUNG, 1000, NULL);
			m_bInited = true;
			bResult = true;
		} else { //failed to init - so clean up
			m_bInited = false;
			switch(nInitStage) {
			case 3:
				Cleanup_DestroyMainWindow();
			case 2:
				Cleanup_DestroyWindowClass();
			case 1:
				; // do nothing (there is no need to unregister window message)
			}
			bResult = false; //error in init 
		}
	}
	return bResult;

	return false;

}

bool SkypeLine::Shutdown()
{
	if(m_bInited) 
	{

		Q_ASSERT(NULL != g_hWndTalker);

		// close the window
		PostMessage(g_hWndTalker, WM_CLOSE, 0, 0);
		// cleanup
		Cleanup_DestroyMainWindow();
		Cleanup_DestroyWindowClass();

		g_hWndSkype = NULL;
		m_bWndCreated = false;

	
		//unregister the messages
		g_nMsgID_SkypeAPIAttach = 0;
		g_nMsgID_SkypeAPIDiscover = 0;
		
		m_bInited = false;
	}
	
	return true;
}

void SkypeLine::EnsureInitialised() 
{
	//Skype already started ?
	bool bRes = SkypeLine::getInstance()->IsSkypeStarted();
	if(bRes){
		Initialize();
		return;
	}

	//Skype not already started
	bool bLocalInstalled = SkypeLine::getInstance()->IsSkypeInstalled();
	bool bPortableUsed = false;
	
	if(DataHelper::IsPortableVersion())
	{
		qDebug() << "Portable Version detected";

		QString strParams = " /removable /datapath:\"Userdata\"";

		//current root dir
		QString strDir = QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
		QDir dir(strDir);
		while(dir.cdUp()) NULL;
		strDir = dir.path();

		if(g_pClientManager->GetIniFile()->m_strPortableSkypePath.isEmpty())
		{
			qDebug() << "Portable Skype Path not defined";

			//ask user for a portable path
			PortableSkypeSearchDlg dlg;
			if(dlg.exec())
			{
				if(dlg.m_bSearchSkype){
					QString strPath;

					//first search for explicit path "Skype/Skype.exe"
					QString strWorkDir = QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
					strWorkDir += "/Skype/";
					strPath = strWorkDir + "Skype.exe";
					if(!QFile::exists(strPath))
					{
						//alternatively search this root drive for "Skype.exe"
						DbRecordSet lstPaths, lstApps;
						DataHelper::GetFilesFromFolder(strDir, lstPaths, lstApps, true);
						int nResults = lstApps.getRowCount();
						for(int i=0; i<nResults; i++)
						{
							QString strTmpPath = lstApps.getDataRef(i, 1).toString();
							if(lstApps.getDataRef(i, 1).toString() == "Skype")
							{
								strPath = lstApps.getDataRef(i, 0).toString();
								break;
							}
						}
					}

					if(!strPath.isEmpty()){
						g_pClientManager->GetIniFile()->m_strPortableSkypePath = strPath;
						//try starting the custom Skype by ourselves
						SkypeLine::getInstance()->StartSkypeAppCustom(g_pClientManager->GetIniFile()->m_strPortableSkypePath, strParams, strWorkDir);
						bPortableUsed = true;
					}
					else{
						g_pClientManager->GetIniFile()->m_strPortableSkypePath = "NONE";
						QMessageBox::information(NULL, "", QObject::tr("Portable Skype was not found!"));
					}
				}
				else{
					//save to .ini
					g_pClientManager->GetIniFile()->m_strPortableSkypePath = "NONE";
					bPortableUsed = true;
				}
			}
		}
		else if (g_pClientManager->GetIniFile()->m_strPortableSkypePath != "NONE")
		{
			qDebug() << "Portable Skype Path is defined";

			//try starting the custom Skype by ourselves (replace drive name with current)
			QString strPath = g_pClientManager->GetIniFile()->m_strPortableSkypePath;
			strPath = strPath.mid(strDir.length());
			strPath = strDir + strPath;

			QFileInfo info(strPath);
			QString strWorkDir = info.absolutePath();
			SkypeLine::getInstance()->StartSkypeAppCustom(strPath, strParams, strWorkDir);
			bPortableUsed = true;
		}
		else{
			qDebug() << "Portable Skype Path is NONE";
		}
	}
	
	//startup Skype if required
	if(bLocalInstalled && !bPortableUsed){
		//try starting the Skype by ourselves
		SkypeLine::getInstance()->StartSkypeApp();
	}

	//QMessageBox::information(NULL, "", QObject::tr("Skype is not started!"));
	TimeMessageBox box(3, false, QMessageBox::Information, QObject::tr("Sokrates"),QObject::tr("Initializing Phone Interface..."));
	box.setStandardButtons(QMessageBox::NoButton);
	box.exec();	
}

// send command to skype (NOTE: expecting input in utf-8)
bool SkypeLine::SendCommand(const char *szParam, bool bCache) 
{
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Skype sending command ")+QString(szParam));

	EnsureInitialised();

	bool bSuccess = true;
	if(m_bInited && g_hWndSkype && IsSkypeLogged()) 
	{
		//empty cache first:
		QList<QString> lstFailed;
		int nCmds = g_lstCmdCache.count();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Skype purging %1 messages from cache").arg(nCmds));
		//TRACE("Empty Skype cache of %d items.", nCmds);
		for(int i=0; i<nCmds; i++)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Skype send cached command = %1").arg(g_lstCmdCache[i]));
			bSuccess = SendCommandExt(g_lstCmdCache[i].toLatin1().constData()); //should check for status
			if(!bSuccess)
				lstFailed.append(g_lstCmdCache[i]);
		}
		g_lstCmdCache.clear();
		g_lstCmdCache=lstFailed;

		//try command, if failed store in cache
		bSuccess = SendCommandExt(szParam);
		if(!bSuccess && bCache)
		{
			g_lstCmdCache.append(szParam);
		}
	
	}
	else if (bCache)
	{
		//cache command to be replayed when we connect
		g_lstCmdCache.append(QString(szParam));

		//if really no skype
		if(g_lstCmdCache.size()>1000)
			g_lstCmdCache.clear();
	}

	return bSuccess;

	return false;

}


bool SkypeLine::SendCommandExt(const char *szParam) 
{
		COPYDATASTRUCT oCD;
		oCD.dwData = 0;
		oCD.lpData = (void *)szParam;
		oCD.cbData = strlen(szParam)+1;

		if( oCD.cbData > 1 )
		{
			if(::SendMessage(g_hWndSkype, WM_COPYDATA, (WPARAM)g_hWndTalker, (LPARAM)&oCD)==FALSE)
			{
				//Q_ASSERT(false);
				TRACE("!!! Disconnected\n");
				m_bInited = false;
				g_hWndSkype = NULL;
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Skype disconnected!!");
				return false;
			}
			else
			{
				qDebug() << "Skype MSG (outgoing): " << QString(szParam);
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Skype MSG (outgoing): " + QString(szParam));
				return true;
			}
		}
		else
			return false;

}


bool SkypeLine::IsSkypeInstalled()
{

	QString strSkypeExe = GetSkypeAppPath();
	return (!strSkypeExe.isEmpty() && 0 == _access(strSkypeExe.toLatin1().constData(), 00));

	return false;

}

bool SkypeLine::IsSkypeLogged()
{
	return g_bSkypeLogged;
}

QString SkypeLine::GetSkypeAppPath()
{
	static QString strPath;		//cache result to speed-up multiple calls

	if(!strPath.isEmpty())
		return strPath;

	char szPath[1024] = "";
	DWORD dwValueSize = sizeof(szPath);

	//first check under HKEY_CURRENT_USER
	HKEY regKey = NULL;
	LONG lRes=RegOpenKeyEx( HKEY_CURRENT_USER, 
							L"SOFTWARE\\Skype\\Phone", 
							(DWORD)0, KEY_READ, &regKey);
	if(ERROR_SUCCESS==lRes) 
	{
		lRes=RegQueryValueEx(regKey, L"SkypePath",
							 NULL, NULL, (LPBYTE)szPath, &dwValueSize);
		if(ERROR_SUCCESS==lRes)
			strPath.setUnicode((QChar *)szPath, dwValueSize);

		RegCloseKey(regKey);
	}
	
	//on failure, check under HKEY_LOCAL_MACHINE
	if(ERROR_SUCCESS!=lRes)
	{
		lRes=RegOpenKeyEx( HKEY_LOCAL_MACHINE, 
								L"SOFTWARE\\Skype\\Phone", 
								(DWORD)0, KEY_READ, &regKey);
		if(ERROR_SUCCESS==lRes) 
		{
			lRes=RegQueryValueEx(regKey, L"SkypePath",
								 NULL, NULL, (LPBYTE)szPath, &dwValueSize);
			if(ERROR_SUCCESS==lRes)
				strPath.setUnicode((QChar *)szPath, dwValueSize);

			RegCloseKey(regKey);
		}
	}

	return strPath;
}

bool SkypeLine::IsSkypeStarted()
{

	return is_process_started(L"Skype.exe");

	return false;

}

bool SkypeLine::StartSkypeApp()
{
	qDebug() << "Start Skype:" << GetSkypeAppPath();


	HINSTANCE nRes = ::ShellExecute(NULL, L"open", (LPCTSTR)GetSkypeAppPath().constData(), NULL, NULL, SW_SHOW);
	return ((int)nRes > 32);

	return false;

}

bool SkypeLine::StartSkypeAppCustom(const QString &strPath, const QString strParams, const QString strWorkDir)
{
	qDebug() << "Start Skype Custom:" << strPath << strParams << strWorkDir;


	HINSTANCE nRes = ::ShellExecute(NULL, L"open", (LPCTSTR)strPath.constData(), (LPCTSTR)strParams.constData(), (LPCTSTR)strWorkDir.constData(), SW_SHOW);
	return ((int)nRes > 32);

	return false;

}


// process SkypeControlAPIAttach message
//we must return in 1 second...
void HandleSkypeAttach(LPARAM lParam, WPARAM wParam)
{
	qDebug()<<"HandleSkypeAttach "<<lParam;
	switch(lParam){
		case SKYPECONTROLAPI_ATTACH_SUCCESS:
			TRACE("Skype API attached succesfuly!");
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Skype API is attached!");
			g_hWndSkype = (HWND)wParam;
			g_bSkypeLogged = true;
			//g_lstCmdCache.append("SET SILENT_MODE ON");

			
			//QMessageBox::information(NULL, "", "Skype API success!");
			{
				//replay all stacked comands and clear the cache
				int nCmds = g_lstCmdCache.count();
				TRACE("Empty Skype cache of %d items.", nCmds);
				for(int i=0; i<nCmds; i++)
					SkypeLine::getInstance()->SendCommand(g_lstCmdCache[i].toLatin1().constData(), false);
				g_lstCmdCache.clear();
			}
			//SkypeLine::getInstance()->SendCommand("SET SILENT_MODE ON");
			
			SkypeLine::getInstance()->SendCommandExt("SET SILENT_MODE ON");
			break;
		case SKYPECONTROLAPI_ATTACH_PENDING_AUTHORIZATION:
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Skype is asked for access!");
			break;
		case SKYPECONTROLAPI_ATTACH_REFUSED:
			{
				g_bSkypeLogged = false;
				SkypeLine::m_bListenIncomingCalls = false;
				//QMessageBox::information(QApplication::activeWindow(), QObject::tr("Warning"), QObject::tr("Skype access refused!"));
				TimeMessageBox box(3, false, QMessageBox::Information, QObject::tr("Warning"),QObject::tr("Skype access refused!"));
				box.setStandardButtons(QMessageBox::NoButton);
				box.exec();	
				break;
			}
		case SKYPECONTROLAPI_ATTACH_NOT_AVAILABLE:
			{
			g_bSkypeLogged = false;
			SkypeLine::m_bListenIncomingCalls = false;
			//QMessageBox::information(QApplication::activeWindow(), QObject::tr("Warning"), QObject::tr("Skype access not available!\nPlease make sure you are logged into Skype."));
			TimeMessageBox box(3, false, QMessageBox::Information, QObject::tr("Warning"),QObject::tr("Skype access not available!\nPlease make sure you are logged into Skype."));
			box.setStandardButtons(QMessageBox::NoButton);
			box.exec();	
			break;
			}
		case SKYPECONTROLAPI_ATTACH_API_AVAILABLE:
			//g_bSkypeLogged = false;
			//QMessageBox::information(NULL, "", "Skype API available!");
			break;
		default:
			TRACE("Unknown Skype MSG %08lx: %08lx\n", lParam, wParam);
			break;
	}
}

LRESULT APIENTRY SkypeWindowProc(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lReturnCode = 0;
	bool bIssueDefProc	= false;

	switch(uMsg)
	{
	case WM_DESTROY:
		g_hWndTalker = NULL;
		break;
	case WM_COPYDATA:
		if( g_hWndSkype == (HWND)wParam )
		{
			PCOPYDATASTRUCT poCopyData=(PCOPYDATASTRUCT)lParam;
			if(poCopyData->cbData > 1)
				lineCallbackFunc(poCopyData->lpData, poCopyData->cbData);
			lReturnCode=1;
		}
		break;
	default:
		if( uMsg == g_nMsgID_SkypeAPIAttach){
			HandleSkypeAttach(lParam, wParam);
			lReturnCode=1;
			break;
		}
		bIssueDefProc=true;
		break;
    }
	if( bIssueDefProc )
		lReturnCode=DefWindowProc(hWindow, uMsg, wParam, lParam);

	if( g_bDumpWindowsMessages ){
		//TRACE( "WindowProc: hWindow=0x%08X, MainWindow=0x%08X, Message=%5u, WParam=0x%08X, LParam=0x%08X; Return=%ld%s\n",
		//	hWindow, g_hWndTalker, uMsg, wParam, lParam, lReturnCode, bIssueDefProc? " (default)":"");
	}
	
	return lReturnCode;
}
 //

 // WINCE
