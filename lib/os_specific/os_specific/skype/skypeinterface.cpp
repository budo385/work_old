#include "skypeinterface.h"
#include "bus_core/bus_core/commgridviewhelper.h"

SkypeInterface::SkypeInterface()
:m_nCallID(-1),m_nCallConferenceID(-1),m_nWndState(WND_STATE_PreCall),m_nCallState(CommGridViewHelper::CALL_STATE_None), m_nVoiceMailIncID(-1), m_nVoiceMailOutID(-1)
{

}

SkypeInterface::~SkypeInterface()
{

}


void SkypeInterface::SetCallState(QString strStatus)
{
	m_strStatus=strStatus;

	//update call status flag
	if(QString("FAILED") == strStatus)
		m_nCallState = CommGridViewHelper::CALL_STATE_Failed;
	else if(QString("FINISHED") == strStatus)
		m_nCallState = CommGridViewHelper::CALL_STATE_Finished;
	else if(QString("MISSED") == strStatus)
		m_nCallState = CommGridViewHelper::CALL_STATE_Missed;
	else if(QString("REFUSED") == strStatus)
		m_nCallState = CommGridViewHelper::CALL_STATE_Refused;
	else if(QString("BUSY") == strStatus)
		m_nCallState = CommGridViewHelper::CALL_STATE_Failed;
	else if(QString("INPROGRESS") == strStatus)
		m_nCallState = CommGridViewHelper::CALL_STATE_InProgress;
	else if(QString("FORWARDED") == strStatus)
		m_nCallState = CommGridViewHelper::CALL_STATE_Forwarded;
	else if(QString("ONHOLD") == strStatus)
		m_nCallState = CommGridViewHelper::CALL_STATE_OnHold;
	else
		m_nCallState = CommGridViewHelper::CALL_STATE_None;
}