#ifndef SKYPEINTERFACE_H
#define SKYPEINTERFACE_H

#include <QString>
#include <QVariant>

class SkypeInterface 
{
public:

	SkypeInterface();
	~SkypeInterface();

	enum WndState
	{
		WND_STATE_PreCall		=0,	//Initial window state (before making an outgoing call)
		WND_STATE_Ringing_Inc	=1,	//Somebody is calling us from any line/device
		WND_STATE_Ringing_Out	=2,	//We initiated an outgoing call
		WND_STATE_Talking		=3,	//The call has been accepted and the call is being made
		WND_STATE_OnHold		=4,	//The user has pressed the HOLD-key to pause a current call or to connect it to another contact/person
		WND_STATE_After			=5	//The call is terminated but the data can be completed
	};
	//
	// define call states (only final states should go to the database)
	//

	enum SkypeTypeData
	{
		SKYPE_FULL_NAME,
		SKYPE_BIRTHDAY,
		SKYPE_SEX,
		SKYPE_COUNTRY,
		SKYPE_PROVINCE,
		SKYPE_CITY,
		SKYPE_PHONE_HOME,
		SKYPE_PHONE_OFFICE,
		SKYPE_PHONE_MOBILE,
		SKYPE_HOMEPAGE,
		SKYPE_ABOUT,
		SKYPE_STATUS,
		SKYPE_NAME
	};

	virtual void OnUserResolved(int nTypeData,QString strHandle,QVariant varData){};
	virtual void OnChatResolved(QString strName){};
	virtual void SetSkypeBuddiesReply(QString strMsg){};
	virtual void OnSMSCreated(QString &strID){};

	virtual void SetCallID(int nID){m_nCallID=nID;}
	virtual void SetVoiceMailIncID(int nID){m_nVoiceMailIncID=nID;}
	virtual void SetVoiceMailOutID(int nID){m_nVoiceMailOutID=nID;}
	virtual void SetCaller(QString strNumber){m_strNumber=strNumber;}
	virtual void SetWindowState(int nState){m_nWndState=nState;}
	virtual void SetCallState(QString strStatus);
	virtual void SetConferenceID(int nID){m_nCallConferenceID=nID;}
	//widget:
	virtual void Wnd_ActivateWindow(){};
	virtual void Wnd_SetWindowFlags(Qt::WindowFlags type){};
	
protected:
	int m_nCallID;
	int m_nVoiceMailIncID;	//indicates main call is terminated and the voice mail box activated
	int m_nVoiceMailOutID;
	QString m_strNumber;
	int m_nWndState;
	int m_nCallState;
	QString m_strStatus; //same as m_nCallState
	int m_nCallConferenceID;

};

#endif // SKYPEINTERFACE_H
