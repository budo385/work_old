#ifndef SKYPEPARSER_H_
#define SKYPEPARSER_H_

#include <QString>
#include <QMap>
#include <QTimer>
#include <vector>
#include "skypeinterface.h"

#define CS_ROUTING		-3
#define CS_RINGING		-2
#define CS_INITIAL		-1
#define CS_INPROGRESS	0
#define CS_ONHOLD		1

class CallInfo 
{
public:
	CallInfo();

	bool operator == (const CallInfo &other){
		return (m_nSystem == other.m_nSystem && m_nCallID == other.m_nCallID);
	}

public:
	int m_nSystem;
	int m_nCallID;
	int m_nConfID;
	int m_nStatus;
	bool m_bOutCall;
	bool m_bSokratesIsOriginOfCall;
	bool m_bTransfered;
	
	//TOFIX remote num, conf num, duration, istransfered, ...
};

//base class for opening window for handling incoming calls
class SkypeCallHanlder
{
public:
	virtual int OpenSkypeIncomingCallHandler()=0;
	virtual SkypeInterface * GetSkypeIncomingCallHandler(int nSkypeIncomingCallHandlerID)=0;
};

class SkypeParser : public QObject
{
	Q_OBJECT

public:
    SkypeParser(SkypeCallHanlder *pCallHandler=NULL);
    ~SkypeParser();

	void SetSkypeCallHandler(SkypeCallHanlder *pCallHandler){m_SkypeCallHandler=pCallHandler;};
	void ProcessIncomingMsg(QString strMsg);
	void AttachToLine();
	void DetachFromLine();
	void SetSkypeBuddiesWnd(SkypeInterface *pWnd){m_pSkypeBuddiesFui=pWnd;}
	void SetSkypeUserInfoWnd(SkypeInterface *pWnd){m_pSkypeUserInfoFui=pWnd;};

	// COMMANDS
	void StartCall(QString strNumber);

	QString m_strLastCallCommand;


	int m_nCurOutCallFUI;
	int m_nCurTransferedCallID;
	int m_nCurCallID;	// id of the current active call
	int m_nCurConferenceSpeakers;	// count of speakers without counting myself
	QString m_strTransferToHandle;
	bool m_bSMSWaitResult;

	//Skype settings
	QString m_strMyHandle;
	QString m_strMyFullName;

	SkypeInterface *m_pSkypeBuddiesFui;	// link to window that requested results
	SkypeInterface *m_pSkypeUserInfoFui;	// link to window that requested results

	//central book-keeping
	CallInfo& Info_GetCall(int nCallID);
	std::vector<CallInfo>& Info_GetCallsList(){ return m_lstCurCalls; }
	int Info_GetCallsCount(){ return m_lstCurCalls.size(); };
	int Info_GetCallIdx(int nCallID);
	int Info_GetTransferedCallID();
	bool Info_OnCallCreated(int nCallID, bool bOutCall, bool bStatusRouting=false,bool bSokratesIsOriginOfCall=true);
	bool Info_OnCallEnded(int nCallID);
	void Info_OnCallStatusChanged(int nCallID, QString strStatus);
	int  Info_CalcJoinConferenceID(int nCallID);	// should this call join the conference	
	bool Info_OnCallConferenceID(int nCallID, int nConfID);
	bool Info_SetCallsOnHold();	// param nSystemID, move above to commands

protected slots:
	void UpdateCallVoiceStatus();

protected:
	//static int	m_nCmdIDCounter;
	int	m_nProtocolVersion;

	QMap<int, int>			m_mapCall2Fui;
	std::vector<CallInfo>	m_lstCurCalls;
	CallInfo				m_lstDummy;
	SkypeCallHanlder		*m_SkypeCallHandler;

	bool m_bVoiceMailEvent;
	QTimer *m_pVoiceCheckTimer;
	int m_nVoiceMailIncID;
	int m_nVoiceMailOutID;
	int m_nVoiceCallID;	//related call
};

#endif   //SKYPEPARSER_H_
