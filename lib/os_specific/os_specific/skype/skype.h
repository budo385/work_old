#ifndef _SSKYPE_H_
#define _SSKYPE_H_

#ifndef WINCE

#include <QString>
#include "skypeparser.h"

class SkypeParser;

//
// Skype line object (implemented as Singleton)
//

class SkypeLine
{
public:
	static SkypeLine* getInstance();
	static void releaseInstance();

private:
    SkypeLine() {};
    ~SkypeLine() {};

	//make copy constructor and assignment operator private too
	SkypeLine(const SkypeLine&){}
	SkypeLine& operator=(const SkypeLine&){ return *this; }

	static SkypeLine* m_pInstance;

public:
	static 	bool 	Initialize();
	static 	bool 	Shutdown();
	static 	void	EnsureInitialised();
	static 	bool	IsSkypeInstalled();
	static 	bool	IsSkypeStarted();
	static 	bool	IsSkypeLogged();
	static 	QString GetSkypeAppPath();

    bool	SendCommand(const char *szParam, bool bCache = true);
	bool SendCommandExt(const char *szParam);
	bool	StartSkypeApp();
	bool	StartSkypeAppCustom(const QString &strPath, const QString strParams=QString(""), const QString strWorkDir=QString(""));

public:
	static SkypeParser *m_pParser;		 
	static bool    m_bInited;		/**< is Skype API initialized. */
	static bool    m_bWndCreated;	/**< is message Window created */
	//static long    m_nRefCount;	/**< how many times has TAPI been initialized. */
	static bool    m_bListenIncomingCalls;		/**< is Skype API initialized. */

protected:
	static bool Init_CreateWindowClass();
	static bool Init_CreateMainWindow();
	static void Cleanup_DestroyWindowClass();
	static void Cleanup_DestroyMainWindow();
	
};

#endif   //WINCE
#endif   //_SSKYPE_H_

