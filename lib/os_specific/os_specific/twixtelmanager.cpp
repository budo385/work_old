#include "twixtelmanager.h"
#ifdef _WIN32 
	#include <qt_windows.h>
#endif
#include <QApplication>
#include <QtWidgets/QMessageBox>
#ifdef _WIN32 
#ifndef WINCE
//#define NOMINMAX
//#include <windows.h>
//#include <qt_windows.h>
#include "twixtel/twixapi.h"

bool TwixTelManager::ReadTwixTelContacts(DbRecordSet &lstTmpImportData, QString strDrive,QHash<QString,QString> lstFilterData)
{
	TwixTel reader;
	if(!reader.Initialize(strDrive))
		return false;

	QSize size(300,100);
	QProgressDialog progress(QObject::tr("TwixTel query..."),QObject::tr("Cancel"),0,0,QApplication::activeWindow());
	progress.setWindowTitle(QObject::tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	QString strLastNameFilterOriginal =lstFilterData["strLastNameFilterOriginal"];
	QString strFilter_FirstName =lstFilterData["strFilter_FirstName"];
	QString strFilter_Organization =lstFilterData["strFilter_Organization"];
	QString strFilter_Town=lstFilterData["strFilter_Town"];


	//define search query
	reader.ClearSearch();
	if(!strFilter_FirstName.isEmpty())
		reader.SetQuery_FirstName(strFilter_FirstName);
	if(!strLastNameFilterOriginal.isEmpty())
		reader.SetQuery_LastName(strLastNameFilterOriginal + "*");
	if(!strFilter_Organization.isEmpty())
		reader.SetQuery_Organization(strFilter_Organization);
	if(!strFilter_Town.isEmpty())
		reader.SetQuery_Town(strFilter_Town);

	bool bTwixCheat = true;
	bool bRunSilentLoop = false; //"TwixTel cheat"
	QString strCurExtension = "";

	int nResultListSize = lstTmpImportData.getRowCount();

	//QTime time;
	//time.start();

	while(1)
	{
		//update progress
		progress.setValue(1);
		qApp->processEvents();
		if (progress.wasCanceled()){
			QApplication::restoreOverrideCursor();
			return true; //some record could be already imported
		}

		int nRows = reader.RunQuery();
		if(-16 == nRows)
		{
			if(!bTwixCheat)
			{
				QMessageBox::information(NULL, "", "Too many results (only first record will be displayed)! Please narrow your search query.");
				nRows = 1;
			}
			else
			{
				bRunSilentLoop = true;

#ifdef _DEBUG
				qDebug() << "Twixtel: Found  >999 result rows for query:\t" << strLastNameFilterOriginal + strCurExtension + "*";
#endif	

				//
				// rerun current query with larger depth
				//
				//define search query
				reader.ClearSearch();
				if(!strFilter_FirstName.isEmpty())
					reader.SetQuery_FirstName(strFilter_FirstName);
				if(!strFilter_Organization.isEmpty())
					reader.SetQuery_Organization(strFilter_Organization);
				if(!strFilter_Town.isEmpty())
					reader.SetQuery_Town(strFilter_Town);

				if(!TwixTelCalcNextSuffix(strCurExtension))
					break;	//no more codes
				reader.SetQuery_LastName(strLastNameFilterOriginal + strCurExtension + "*");
				continue;	//rerun this query
			}
		}
		else if (nRows < 0)
		{
			QMessageBox::information(NULL, "", QString("Twixtel error %1").arg(nRows));
			QApplication::restoreOverrideCursor();
			return false;
		}
		else if (0 == nRows)
		{
			if(bRunSilentLoop){
#ifdef _DEBUG
				qDebug() << "Twixtel: Found " << nRows << "result rows for query:\t" << strLastNameFilterOriginal + strCurExtension + "*";
#endif

				//define search query
				reader.ClearSearch();
				if(!strFilter_FirstName.isEmpty())
					reader.SetQuery_FirstName(strFilter_FirstName);
				if(!strFilter_Organization.isEmpty())
					reader.SetQuery_Organization(strFilter_Organization);
				if(!strFilter_Town.isEmpty())
					reader.SetQuery_Town(strFilter_Town);

				if(!TwixTelCalcNextSuffix(strCurExtension, true))
					break;	//no more codes
				reader.SetQuery_LastName(strLastNameFilterOriginal + strCurExtension + "*");
				continue;
			}
			QMessageBox::information(NULL, "", QObject::tr("No records found!"));
			QApplication::restoreOverrideCursor();
			return false;
		}

#ifdef _DEBUG
		qDebug() << "Twixtel: Found " << nRows << "result rows for query:\t" << strLastNameFilterOriginal + strCurExtension + "*";
#endif

		TwixTelFetchResults(lstTmpImportData,reader, nRows, nResultListSize, &progress);	//append query results

		if(bRunSilentLoop)
		{
			//define search query
			reader.ClearSearch();
			if(!strFilter_FirstName.isEmpty())
				reader.SetQuery_FirstName(strFilter_FirstName);
			if(!strFilter_Organization.isEmpty())
				reader.SetQuery_Organization(strFilter_Organization);
			if(!strFilter_Town.isEmpty())
				reader.SetQuery_Town(strFilter_Town);

			if(!TwixTelCalcNextSuffix(strCurExtension, true))
				break;	//no more codes
			reader.SetQuery_LastName(strLastNameFilterOriginal + strCurExtension + "*");
			continue;
		}
		else
			break;
	}

#ifdef _DEBUG
	qDebug() << "Twixtel: Query finished! Result list has " << nResultListSize << "result rows";
#endif

	return true;
}


void TwixTelManager::TwixTelFetchResults(DbRecordSet &lstTmpImportData, TwixTel &reader, int nRows, int &nResultListSize, QProgressDialog *progress)
{
	int nIdx = nResultListSize-1;

	for(int i=0; i<nRows; i++)
	{
		if(progress && (i%9 == 0)){
			progress->setValue(1);
			qApp->processEvents();
			if (progress->wasCanceled())
				return;
		}

		reader.SetCurrentRow(i);

		bool bAdded = lstTmpImportData.addRow();
		Q_ASSERT(bAdded);
		nResultListSize ++;
		nIdx ++;

		QString strData;
		reader.GetCurrentField(MAIN_FIRSTNAME, strData);
		lstTmpImportData.setData(nIdx, "BCNT_FIRSTNAME", strData);
		bool bIsOrg = strData.isEmpty();

		reader.GetCurrentField(MAIN_NAME, strData);
		if(bIsOrg)
			lstTmpImportData.setData(nIdx, "BCMA_ORGANIZATIONNAME", strData);
		else
			lstTmpImportData.setData(nIdx, "BCNT_LASTNAME", strData);

		QString strStreet;
		reader.GetCurrentField(MAIN_STREET, strData);
		strStreet += strData;
		reader.GetCurrentField(MAIN_HOUSENO, strData);
		strStreet += " "; strStreet += strData;
		lstTmpImportData.setData(nIdx, "BCMA_STREET", strStreet);

		reader.GetCurrentField(MAIN_CITY, strData);
		lstTmpImportData.setData(nIdx, "BCMA_CITY", strData);

		reader.GetCurrentField(MAIN_ZIP, strData);
		lstTmpImportData.setData(nIdx, "BCMA_ZIP", strData);

		//country name is hardcoded - Twixtel covers only Switzerland
		lstTmpImportData.setData(nIdx, "BCMA_COUNTRY_CODE", "CH");
		lstTmpImportData.setData(nIdx, "BCMA_COUNTRY_NAME", "Switzerland");

		reader.GetCurrentField(MAIN_PROFESSION, strData);
		lstTmpImportData.setData(nIdx, "BCNT_PROFESSION", strData);

		QString strPhone;
		reader.GetCurrentField(MAIN_AREACODE, strData);
		strPhone += strData;
		reader.GetCurrentField(MAIN_PHONE, strData);
		strPhone += " "; strPhone += strData;
		lstTmpImportData.setData(nIdx, "CLC_PHONE_PRIVATE", strPhone);

		reader.GetCurrentField(MAIN_TEXTBEFOREPHONE, strData);
		//qDebug() << "Text before phone: " << strData;

		QString strDesc = "<html><body>";
		strDesc += strData;
		strDesc += "\t";
		strDesc += strPhone;

		// get number of "follow"-entries  (Natel, Fax etc... )
		int nSubCnt = reader.GetNumSubRows();
		if(nSubCnt > 0)
		{
			for(int j=0; j<nSubCnt; j++)
			{
				strDesc += "<br>\n";

				QString strType;
				reader.GetSubRowField(j, FOLLOW_TEXTBEFOREPHONE, strType);
				//qDebug() << "Follow #" << j << " text before phone: " << strType;
				strDesc += strType;

				reader.GetSubRowField(j, FOLLOW_AREACODE, strData);
				//qDebug() << "Follow #" << j << " area: " << strData;
				strPhone = strData;

				reader.GetSubRowField(j, FOLLOW_PHONE, strData);
				//qDebug() << "Follow #" << j << " phone: " << strData;
				strPhone += " ";
				strPhone += strData;

				strDesc += "\t";
				strDesc += strPhone;

				if(strType == QString("Fax"))
				{
					lstTmpImportData.setData(nIdx, "CLC_PHONE_FAX", strPhone);
				}
				else if(strType == QString("Mobile"))
				{
					lstTmpImportData.setData(nIdx, "CLC_PHONE_MOBILE", strPhone);
				}
				else if(strType == QString("B�ro"))
				{
					lstTmpImportData.setData(nIdx, "CLC_PHONE_BUSINESS_DIRECT", strPhone);
				}
			}
		}

		strDesc += "</body></html>\n";
		lstTmpImportData.setData(nIdx, "BCNT_DESCRIPTION", strDesc);
	}
}


bool TwixTelManager::TwixTelCalcNextSuffix(QString &strSuffix, bool bSkipDepthForCurCode)
{
	//NOTE: Twixtel ignores accented chars so they are not used for search
	const QString strValidChars("abcdefghijklmnopqrstuvwxyz");
	const QChar cFirstValid('a');
	const QChar cLastValid('z');
	const int nMaxSilentDepth = 7;

	//calc next silent query suffix
	int nLen = strSuffix.length();
	if(0 == nLen){
		strSuffix = cFirstValid;
	}
	else{
		QChar cLast = strSuffix.at(nLen-1);
		if(cLast == cLastValid)	//we reached last allowed char on the last place
		{
			if(!bSkipDepthForCurCode){
				strSuffix += cFirstValid;
				nLen ++;
			}

			if(nLen > nMaxSilentDepth) //passed maximal depth
			{
				strSuffix.remove(nLen-1, 1);
				nLen --;

				//find previous letter that can be incremented
				int nIncrementPos = -1;
				for(int k=nLen-1; k>=0; k--){
					if(strSuffix.at(k) != cLastValid){
						nIncrementPos = k;
						break;
					}
				}
				if(nIncrementPos < 0)
					return false;	//reached last available code

				// increment the letter
				cLast = strSuffix.at(nIncrementPos);
				int nPos = strValidChars.indexOf(cLast);
				Q_ASSERT(nPos >= 0);
				strSuffix[nIncrementPos] = strValidChars.at(nPos+1);

				//cut all letters after it
				strSuffix = strSuffix.left(nIncrementPos+1);
			}
			else if (bSkipDepthForCurCode)
			{
				//find previous letter that can be incremented
				int nIncrementPos = -1;
				for(int k=nLen-1; k>=0; k--){
					if(strSuffix.at(k) != cLastValid){
						nIncrementPos = k;
						break;
					}
				}
				if(nIncrementPos < 0)
					return false;	//reached last available code

				// increment the letter
				cLast = strSuffix.at(nIncrementPos);
				int nPos = strValidChars.indexOf(cLast);
				Q_ASSERT(nPos >= 0);
				strSuffix[nIncrementPos] = strValidChars.at(nPos+1);

				//cut all letters after it
				strSuffix = strSuffix.left(nIncrementPos+1);
			}
		}
		else	// last char has not maximum val
		{
			if(!bSkipDepthForCurCode && nLen < nMaxSilentDepth){
				strSuffix += cFirstValid;
			}
			else //passed maximal depth
			{
				// increment char on a last position
				int nPos = strValidChars.indexOf(cLast);
				Q_ASSERT(nPos >= 0);
				strSuffix[nLen-1] = strValidChars.at(nPos+1);
			}
		}
	}
	return true;
}

#endif //WINCE
#endif //#ifdef _WIN32 