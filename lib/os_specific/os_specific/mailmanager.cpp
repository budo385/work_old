//#ifndef WINCE

#include "mailmanager.h"
#include <QDesktopServices>
#include <QApplication>
#include <QtWidgets/QMessageBox>
#include <QRegExp>
#include <QProgressDialog>
#include <QUrl>
#include <QFileInfo>
#include "common/common/loggerabstract.h"

#include "trans/trans/smtpconnectionsettings.h"
#include "common/common/statuscodeset.h"
#include "db_core/db_core/dbsqltableview.h"
#include "outlookfolderpickerdlg.h"

#include "common/common/rijndael.h"
#include "common/common/sha2.h"
#ifdef _WIN32
#include "mailpp/src/SmtpSession.h"
#include "mailpp/src/MailTransaction.h"
#endif
#include "common/common/datahelper.h"
#include "bus_client/bus_client/emailhelper.h"

#ifndef WINCE
	#include "mimepp/src/mimepp/MediaType.h"
#else
	#include "mimepp/wince/src/mimepp/MediaType.h"
#endif

#include "bus_core/bus_core/globalconstants.h"

#define KEY_AES "ptice"

QString MailManager::EncryptSMTPPassword(QString strPass)
{
	//prepare key
	//use KEY_AES AS KEY:
	unsigned char szKey[32];
	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)QString(KEY_AES).toLatin1().constData(),QString(KEY_AES).size(), &ctx);
	sha256_end((unsigned char *)szKey, &ctx);

	Rijndael crypt;
	crypt.init(Rijndael::CBC, Rijndael::Encrypt, szKey, Rijndael::Key32Bytes);

	//NOTE: buffer must be multiple of 16 bytes (rijndael uses 16bytes=128bit blocks)
	const int BUFF_SIZE = 10*1024;
	char szInBuf[BUFF_SIZE];
	char szOutBuf[BUFF_SIZE+16] = "";

	int nInLen = strPass.toLocal8Bit().length();
	memcpy(szInBuf, strPass.toLocal8Bit().constData(), nInLen);
	szInBuf[nInLen] = '\0';

	int len = crypt.padEncrypt((const UINT8*)szInBuf, nInLen, (UINT8*)szOutBuf);
	if(len < 0)
		return QString();

	QByteArray datEnc(szOutBuf, len);
	QString strRes = QString(datEnc.toBase64());	// BASE64 encoding to make a decent string
	return strRes;
}


QString MailManager::DecryptSMTPPassword(QString strTxt)
{
	//prepare key
	//use KEY_AES AS KEY:
	unsigned char szKey[32];
	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)QString(KEY_AES).toLatin1().constData(),QString(KEY_AES).size(), &ctx);
	sha256_end((unsigned char *)szKey, &ctx);

	QByteArray datIn(strTxt.toLocal8Bit().constData());
	QByteArray datEnc = QByteArray::fromBase64(datIn);

	Rijndael crypt;
	crypt.init(Rijndael::CBC, Rijndael::Decrypt, szKey, Rijndael::Key32Bytes);

	//NOTE: buffer must be multiple of 16 bytes (rijndael uses 16bytes=128bit blocks)
	const int BUFF_SIZE = 10*1024;
	char szInBuf[BUFF_SIZE];
	char szOutBuf[BUFF_SIZE+16] = "";

	int nInLen = datEnc.length();
	memcpy(szInBuf, datEnc.constData(), nInLen);
	szInBuf[nInLen] = '\0';

	int len = crypt.padDecrypt((const UINT8*)szInBuf, nInLen, (UINT8*)szOutBuf);
	if(len < 0)
		return QString();

	return QString(szOutBuf);
}


#ifdef _WIN32
//--------------------------------TEST----------------------------
class myTraceOutput : public mailpp::TraceOutput
{
public:
	virtual void Send (const char *buffer, unsigned length)
	{
#ifdef _DEBUG
		qDebug() << "SMTP >" << buffer;
#endif
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "SMTP > " + QString(buffer));
	};

	virtual void Receive (const char *buffer, unsigned length)
	{
#ifdef _DEBUG
		qDebug() << "SMTP <" << buffer;
#endif
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "SMTP < " + QString(buffer));
	};
};
#endif


//--------------------------------TEST----------------------------

bool MailManager::InitSmtpSession(SmtpSession &session, const QString &strHost, int nPort, const QString &strAccName, const QString &strAccPass, bool bEMAIL_SETTINGS_USE_AUTH, bool bUseSSL, bool bUseSTLS)
{
	if(0 == session.Open(strHost.toLocal8Bit().constData(), nPort, bUseSSL, bUseSTLS))
	{
		qDebug() << "SMTP session started";

		//authentication required ?
		//g_pSettings->GetPersonSetting(EMAIL_SETTINGS_USE_AUTH).toBool()
		if( bEMAIL_SETTINGS_USE_AUTH && strAccName.length() > 0)
		{
			int nRet = session.GetClient().AuthLogin(strAccName.toLocal8Bit().constData(), strAccPass.toLocal8Bit().constData());
			if (0 != nRet) {
				//TCP error
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("TCP error when authenticating to SMPT server"));
				session.Close();
				qDebug() << "SMTP Auth failed";
				return false;
			}
			int replyCode = session.GetClient().ReplyCode();
			if(replyCode/100%10 != 2){
				//auth error
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("SMTP session error (reply code %1)").arg(replyCode));
				session.Close();
				qDebug() << "SMTP Auth failed1";
				return false;
			}
		}
		return true;
	}
	return false;
}

bool MailManager::SendMailDirectly(DbRecordSet &recError, DbRecordSet &m_recMail,SMTPConnectionSettings connSettings,DbRecordSet &lstAttachments,DbRecordSet &recToRecordSet,DbRecordSet &recCCRecordSet,DbRecordSet &recBCCRecordSet,FN_Call_CreateEmailFromTemplate pfCallBack_CreateTemplate,QString strEMAIL_SETTINGS_SEND_COPY_CC,QString strEMAIL_SETTINGS_SEND_COPY_BCC, bool bEMAIL_SETTINGS_USE_AUTH, int nProjectID,bool bSerialEmailMode)
{
	if (m_recMail.getRowCount()!=1)
		return false;

	bool bOK = true;

#ifdef _WIN32
	recError.destroy();
	recError.addColumn(QVariant::String, "CONTACT_EMAIL");
	recError.addColumn(QVariant::String, "CONTACT_NAME");

	//use internal Mailpp library
	QString strBody  = m_recMail.getDataRef(0,"BEM_BODY").toString();
	bool bPlainText  = m_recMail.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT;
	QString strHost  = connSettings.m_strHost;
	int nPort        = connSettings.m_nPort;
	QString strName  = connSettings.m_strUserName;
	QString strEmail = connSettings.m_strUserEmail;
	QString strAccName = connSettings.m_strAccountName;
	QString strAccPass = DecryptSMTPPassword(connSettings.m_strAccountPassword);
	bool bUseSSL = connSettings.m_bUseSSL;
	bool bUseSTLS = connSettings.m_bUseSTLS;

	//		DbRecordSet recToRecordSet,recCCRecordSet,recBCCRecordSet, lstContactEmails;
	//		CommunicationManager::LoadRecipients(lstContactEmails,recToRecordSet,m_recMail.getDataRef(0,"BEM_TO").toString());
	//		CommunicationManager::LoadRecipients(lstContactEmails,recCCRecordSet,m_recMail.getDataRef(0,"BEM_CC").toString());
	//		CommunicationManager::LoadRecipients(lstContactEmails,recBCCRecordSet,m_recMail.getDataRef(0,"BEM_BCC").toString());
	int nEmailIdx=recToRecordSet.getColumnIdx("CONTACT_EMAIL");
	int nNameIdx=recToRecordSet.getColumnIdx("CONTACT_NAME");
	int nCIDIdx=recToRecordSet.getColumnIdx("CONTACT_ID");

	int nSizeTo=recToRecordSet.getRowCount();
	int nMessageCount = (bSerialEmailMode) ? nSizeTo : 1;

	QSize size(300,100);
	QProgressDialog progress(QObject::tr("Sending Emails..."),QObject::tr("Cancel"),0,nMessageCount,NULL);
	progress.setWindowTitle(QObject::tr("Send In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//2.4s before pops up

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Mail send operation start (%1 mails to send)").arg(nMessageCount));

	//NOTE: use single session for all the emails sent (or else SMTP server can kick us if sending too many mails - wasting too many connections)
	//
	// Create an SmtpSession object and connect to the SMTP server
	//
	SmtpSession session(strHost.toLocal8Bit().constData());
	session.SetTracer(new myTraceOutput());

	bool bAborted = false;
	if(InitSmtpSession(session, strHost, nPort, strAccName, strAccPass, bEMAIL_SETTINGS_USE_AUTH, bUseSSL, bUseSTLS))
	{
		//send one or more messages
		for(int nMsgIdx=0; nMsgIdx<nMessageCount; nMsgIdx++)
		{
			//any embedded image
			bool bAnyCID = false;
			unsigned nAttCount = lstAttachments.getRowCount();
			int i;
			for (i=0; i<nAttCount; ++i){
				if(!lstAttachments.getDataRef(i,"BEA_CID_LINK").toString().isEmpty()){
					bAnyCID = true;
					break;
				}
			}

			mimepp::Message msg;

			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Mail %1/%2").arg(nMsgIdx).arg(nMessageCount));

			// Set the content-type header field to multipart/related.  Add a
			// boundary parameter and a type parameter.
			mimepp::MediaType& mtype = msg.headers().contentType();
			mtype.setType("multipart");
			if(bAnyCID)
				mtype.setSubtype("related");
			else
				mtype.setSubtype("mixed");
			mtype.createBoundary();
			mimepp::Parameter* param = new mimepp::Parameter;
			param->setName("type");
			if(bPlainText)
				param->setValue("text/plain");
			else
				param->setValue("text/html");
			mtype.addParameter(param);
			param = 0;

			//add attachments
			//BodyPart* part = new BodyPart;
			//msg.body().addBodyPart(part);

			QString strNewBody;
			if(bSerialEmailMode)
			{
				//one body per "To" recepient
				//recToRecordSet.Dump();
				int nContactID = recToRecordSet.getDataRef(nMsgIdx,nCIDIdx).toInt();
				//int nProjectID = m_nProjectID;
				//g_CommManager.CreateEmailFromTemplate(strBody, strNewBody, nContactID, nProjectID);
				if (pfCallBack_CreateTemplate)
					pfCallBack_CreateTemplate(strBody, strNewBody, nContactID, nProjectID);
			}
			else
				strNewBody = strBody;

			//add body
			mimepp::QuotedPrintableEncoder qencoder;
			mimepp::String encodedText = qencoder.encode(strNewBody.toUtf8().constData());

			mimepp::BodyPart* part = new mimepp::BodyPart;
			// Set the content-type header field to text/plain; charset=iso-8859-1
			part->headers().contentType().setType("text");
			if(bPlainText)
				part->headers().contentType().setSubtype("plain");
			else
				part->headers().contentType().setSubtype("html");
			param = new mimepp::Parameter;
			param->setName("charset");
			param->setValue("utf-8"); //param->setValue("iso-8859-1");
			part->headers().contentType().addParameter(param);
			// Set the content-transfer-encoding header field to quoted-printable
			part->headers().contentTransferEncoding().fromEnum(mimepp::TransferEncodingType::QUOTED_PRINTABLE);
			// Set the encoded HTML text as the body of the body part
			part->body().setString(encodedText);
			// Add the body part to the message
			msg.body().addBodyPart(part);

			/*
			if(!bPlainText)
			{
				//add additional plain text version of the same mail
				QString strTxtBody = DataHelper::ExtractTextFromHTML(strNewBody);
				if(!strTxtBody.isEmpty())
				{
					//encodedText = qencoder.encode(strTxtBody.toLocal8Bit().constData());
					encodedText = qencoder.encode(strTxtBody.toUtf8().constData());

					part = new mimepp::BodyPart;
					// Set the content-type header field to text/plain; charset=iso-8859-1
					part->headers().contentType().setType("text");
					part->headers().contentType().setSubtype("plain");
					param = new mimepp::Parameter;
					param->setName("charset");
					param->setValue("utf-8"); //param->setValue("iso-8859-1");//
					part->headers().contentType().addParameter(param);
					// Set the content-transfer-encoding header field to quoted-printable
					part->headers().contentTransferEncoding().fromEnum(mimepp::TransferEncodingType::QUOTED_PRINTABLE);
					// Set the encoded HTML text as the body of the body part
					part->body().setString(encodedText);
					// Add the body part to the message
					msg.body().addBodyPart(part);
				}
			}
			*/

			QString strTmpEmail = strName;
			EmailHelper::FixEmailInputLine(strTmpEmail);
			strTmpEmail += " <";
			strTmpEmail += strEmail;
			strTmpEmail += ">";

			mimepp::Mailbox *address1 = new mimepp::Mailbox(strTmpEmail.toLocal8Bit().constData());
			address1->parse();
			msg.headers().from().addMailbox(address1);

			mimepp::Address *address;
			if(bSerialEmailMode)
			{
				//one separate message per "TO" recipient
				QString strTmpEmail = recToRecordSet.getDataRef(nMsgIdx,nNameIdx).toString();
				strTmpEmail = strTmpEmail.replace(",", " ");
				QString strEmail = recToRecordSet.getDataRef(nMsgIdx,nEmailIdx).toString();
				EmailHelper::FixEmailInputLine(strEmail);

				strTmpEmail += " <";
				strTmpEmail += strEmail;
				strTmpEmail += ">";

				address = new mimepp::Mailbox(strTmpEmail.toLocal8Bit().constData());
				address->parse();
				msg.headers().to().addAddress(address);

				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Send to: %1").arg(strEmail));
			}
			else
			{
				for(i=0; i<nSizeTo; i++){
					QString strTmpEmail = recToRecordSet.getDataRef(i,nNameIdx).toString();
					strTmpEmail = strTmpEmail.replace(",", " ");
					QString strEmail = recToRecordSet.getDataRef(i,nEmailIdx).toString();
					EmailHelper::FixEmailInputLine(strEmail);

					strTmpEmail += " <";
					strTmpEmail += strEmail;
					strTmpEmail += ">";

					address = new mimepp::Mailbox(strTmpEmail.toLocal8Bit().constData());
					address->parse();
					msg.headers().to().addAddress(address);

					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Send to: %1").arg(strEmail));
				}
			}
			int nSizeCC=recCCRecordSet.getRowCount();
			for(i=0; i<nSizeCC; i++){
				QString strTmpEmail = recCCRecordSet.getDataRef(i,nNameIdx).toString();
				strTmpEmail = strTmpEmail.replace(",", " ");
				QString strEmail = recCCRecordSet.getDataRef(i,nEmailIdx).toString();
				EmailHelper::FixEmailInputLine(strEmail);

				strTmpEmail += " <";
				strTmpEmail += strEmail;
				strTmpEmail += ">";

				address = new mimepp::Mailbox(strTmpEmail.toLocal8Bit().constData());
				address->parse();
				msg.headers().cc().addAddress(address);

				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("CC to: %1").arg(strEmail));
			}
			int nSizeBCC=recBCCRecordSet.getRowCount();
			for(i=0; i<nSizeBCC; i++){
				QString strTmpEmail = recBCCRecordSet.getDataRef(i,nNameIdx).toString();
				strTmpEmail = strTmpEmail.replace(",", " ");

				QString strEmail = recBCCRecordSet.getDataRef(i,nEmailIdx).toString();
				EmailHelper::FixEmailInputLine(strEmail);

				strTmpEmail += " <";
				strTmpEmail += strEmail;
				strTmpEmail += ">";

				address = new mimepp::Mailbox(strTmpEmail.toLocal8Bit().constData());
				address->parse();
				msg.headers().bcc().addAddress(address);

				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("BCC to: %1").arg(strEmail));
			}

			//get additional targets for serial mailing mode
			if(bSerialEmailMode)
			{
				QString strCopyCC = strEMAIL_SETTINGS_SEND_COPY_CC; //g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_CC).toString();
				QStringList lstCopyCC = strCopyCC.split(';', QString::SkipEmptyParts);
				int nMax = lstCopyCC.size();
				for(i=0; i<nMax; i++)
				{
					QString strEmail = lstCopyCC[i];
					EmailHelper::FixEmailInputLine(strEmail);

					address = new mimepp::Mailbox(strEmail.toLocal8Bit().constData());
					address->parse();
					msg.headers().cc().addAddress(address);

					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Additional CC to: %1").arg(strEmail));
				}

				QString strCopyBCC = strEMAIL_SETTINGS_SEND_COPY_BCC; //g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_BCC).toString();
				QStringList lstCopyBCC = strCopyBCC.split(';', QString::SkipEmptyParts);
				nMax = lstCopyBCC.size();
				for(i=0; i<nMax; i++)
				{
					QString strEmail = lstCopyBCC[i];
					EmailHelper::FixEmailInputLine(strEmail);

					address = new mimepp::Mailbox(strEmail.toLocal8Bit().constData());
					address->parse();
					msg.headers().bcc().addAddress(address);

					ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("Additional BCC to: %1").arg(strEmail));
				}
			}

			//set subject
			msg.headers().subject().setUtf8Text(mimepp::String(m_recMail.getDataRef(0,"BEM_SUBJECT").toString().toUtf8().constData()));

			//attachments
			nAttCount = lstAttachments.getRowCount();
			for (i=0; i<nAttCount; ++i) 
			{
				QString strBaseName;
				lstAttachments.getData(i,"BEA_NAME",strBaseName);
				QByteArray binary;
				lstAttachments.getData(i,"BEA_CONTENT",binary);
				binary=qUncompress(binary); //decompress attachment
				QString strCID;
				lstAttachments.getData(i,"BEA_CID_LINK",strCID);

				mimepp::BodyPart* part = new mimepp::BodyPart;
				mimepp::Headers& partHeaders = part->headers();

				//set CID
				bool bAttachmentDisposition = true;
				if (!strCID.isEmpty()){
					QString strCidEncoded = "<";
					strCidEncoded += strCID;
					strCidEncoded += ">";

					partHeaders.fieldBody("Content-ID").setText(strCidEncoded.toLocal8Bit().constData());

					//set correct headers for pictures
					if( strBaseName.endsWith(".jpg") ||
						strBaseName.endsWith(".jpeg") ||
						strBaseName.endsWith(".png") ||
						strBaseName.endsWith(".gif"))
					{
						bAttachmentDisposition = false;

						//embedded images are attached as inline
						partHeaders.contentDisposition().fromEnum(mimepp::DispositionType::INLINE);
						partHeaders.contentDisposition().setFilename(strBaseName.toLocal8Bit().constData());
					}
				}

				//define content type header
				QString strType, strSubType;
				GetMimeFromFile(strBaseName.toLocal8Bit().constData(), strType, strSubType);
				partHeaders.contentType().setType(strType.toLocal8Bit().constData());
				partHeaders.contentType().setSubtype(strSubType.toLocal8Bit().constData());
				
				partHeaders.contentType().setName(strBaseName.toLocal8Bit().constData());
				partHeaders.contentTransferEncoding().fromEnum(mimepp::TransferEncodingType::BASE64);

				if(bAttachmentDisposition){
					partHeaders.contentDisposition().fromEnum(mimepp::DispositionType::ATTACHMENT);
					partHeaders.contentDisposition().setFilename(strBaseName.toLocal8Bit().constData());
				}

				mimepp::String content = mimepp::Base64Encoder().encode(mimepp::String(binary.constData(), binary.length()));
				part->body().setString(content);
				msg.body().addBodyPart(part);
			}

			msg.assemble();
			// Serialize the message
			mimepp::String msgStr;
			msgStr = msg.getString();

			//qDebug() << "Full mail:" << msgStr.c_str();
			qDebug() << "Mail body prepared, initiate SMTP transaction";

			//
			// Create a MailTransaction instance.  The MailTransaction requires a
			// sender, at least one recipient, and the name of a file that contains
			// the message to send.
			//
			MailTransaction trans;
			trans.SetFrom(strEmail.toLocal8Bit().constData());

			qDebug() << "SMTP set address";

			if(bSerialEmailMode)
			{
				QString strEmail = recToRecordSet.getDataRef(nMsgIdx, nEmailIdx).toString();
				EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters
				trans.AddTo(strEmail.toLocal8Bit().constData());
			}
			else{
				for(i=0; i<nSizeTo; i++){
					QString strEmail = recToRecordSet.getDataRef(i, nEmailIdx).toString();
					EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters
					trans.AddTo(strEmail.toLocal8Bit().constData());
				}
			}
			for(i=0; i<nSizeCC; i++){
				QString strEmail = recCCRecordSet.getDataRef(i,nEmailIdx).toString();
				EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters
				trans.AddTo(strEmail.toLocal8Bit().constData());
			}
			for(i=0; i<nSizeBCC; i++){
				QString strEmail = recBCCRecordSet.getDataRef(i,nEmailIdx).toString();
				EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters
				trans.AddTo(strEmail.toLocal8Bit().constData());
			}

			//get additional targets for serial mailing mode
			if(bSerialEmailMode)
			{
				QString strCopyCC = strEMAIL_SETTINGS_SEND_COPY_CC; //g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_CC).toString();
				QStringList lstCopyCC = strCopyCC.split(';', QString::SkipEmptyParts);
				int nMax = lstCopyCC.size();
				for(i=0; i<nMax; i++){
					QString strEmail = lstCopyCC[i];
					EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters
					trans.AddTo(strEmail.toLocal8Bit().constData());
				}
				QString strCopyBCC = strEMAIL_SETTINGS_SEND_COPY_BCC; //g_pSettings->GetPersonSetting(EMAIL_SETTINGS_SEND_COPY_BCC).toString();
				QStringList lstCopyBCC = strCopyBCC.split(';', QString::SkipEmptyParts);
				nMax = lstCopyBCC.size();
				for(i=0; i<nMax; i++){
					QString strEmail = lstCopyBCC[i];
					EmailHelper::FixEmailInputLine(strEmail);	//ensure no invalid characters
					trans.AddTo(strEmail.toLocal8Bit().constData());
				}
			}

			qDebug() << "SMTP set body";

			trans.SetBody(msgStr.c_str());

			//
			// Provide the MailTransaction to the SmtpSession to process
			//

			qDebug() << "SMTP do transaction";

			session.DoTransaction(trans);

			qDebug() << "SMTP transaction done";

			int nErr = trans.m_nLastErr;
			if(nErr != 0){
				recError.addRow();
				recError.setData(recError.getRowCount()-1, "CONTACT_EMAIL", recToRecordSet.getDataRef(nMsgIdx,nEmailIdx).toString());
				recError.setData(recError.getRowCount()-1, "CONTACT_NAME", recToRecordSet.getDataRef(nMsgIdx,nNameIdx).toString());
				//bOK = false;
				//break;	//keep trying to send other queued mails
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("SMTP: error %2 sending mail to %1 (%3)").arg(recToRecordSet.getDataRef(nMsgIdx,nEmailIdx).toString()).arg(nErr).arg(trans.m_strLastErr.c_str()));

				//in case of hard errors, restart the session (if needed)
				if( mailpp::INTERNAL_ERROR				== nErr ||
					mailpp::SYSTEM_ERROR				== nErr ||
					mailpp::HOST_UNREACHABLE_ERROR		== nErr ||
					mailpp::CONNECTION_REFUSED_ERROR	== nErr ||
					mailpp::TIMED_OUT_ERROR				== nErr ||
					mailpp::TLS_DATA_ERROR				== nErr)
				{
					session.GetClient().Disconnect();	//hard disconnect, does not send "QUIT"
					bAborted = true;

					//if network error, restart the connection to ensure other messages get sent
					if(nMsgIdx+1<nMessageCount) //are there more messages to be sent
					{
						if(!InitSmtpSession(session, strHost, nPort, strAccName, strAccPass, bEMAIL_SETTINGS_USE_AUTH, bUseSSL, bUseSTLS)){
							ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("SMTP: failed to restart SMTP session"));
							break;
						}
						bAborted = false;
					}
				}
			}
			else
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("SMTP: mail sent"));

			progress.setValue(nMsgIdx+1);
			qApp->processEvents();
		}	//end for loop

		//close SMTP session unless already closed
		if(!bAborted)
			session.Close();
	}
	else
	{
		qDebug() << "SMTP failed to connect";

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, QString("SMTP: failed connecting to host: %1").arg(strHost));
		bOK = false;
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "Mail send operation done");
#endif

	qDebug() << "SMTP done";

	return bOK;

}

bool MailManager::SendMailDefaultClient(DbRecordSet &m_recMail,DbRecordSet &recToRecordSet,DbRecordSet &recCCRecordSet,DbRecordSet &recBCCRecordSet)
{

	if (m_recMail.getRowCount()!=1)
		return false;
	//Go.
	QString strMailTo = "mailto:";
	//Set mail TO.
	SetMailToRecipients(recToRecordSet, strMailTo);
	strMailTo += "?";
	//Set subject.
	strMailTo += "subject=";
	strMailTo += m_recMail.getDataRef(0,"BEM_SUBJECT").toString();
	//Set CC.
	if (recCCRecordSet.getRowCount())
	{
		strMailTo += "&cc=";
		SetMailToRecipients(recCCRecordSet, strMailTo);
	}
	//Set BCC.
	if (recCCRecordSet.getRowCount())
	{
		strMailTo += "&bcc=";
		SetMailToRecipients(recBCCRecordSet, strMailTo);
	}
	//Set Body.
	strMailTo += "&body=";
	strMailTo +=m_recMail.getDataRef(0,"BEM_BODY").toString();

	return QDesktopServices::openUrl(QUrl(strMailTo));
}

void MailManager::SetMailToRecipients(DbRecordSet &recRecipients, QString &strRecipients)
{
	int nRowCount = recRecipients.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		QString strRecipientEmail = recRecipients.getDataRef(i, "CONTACT_EMAIL").toString();
		EmailHelper::FixEmailInputLine(strRecipientEmail);	//ensure no invalid characters

		//Set return string.
		if (i == (nRowCount - 1))
			strRecipients += strRecipientEmail;
		else
			strRecipients += strRecipientEmail + ",";
	}
}

#ifdef _WIN32
void MailManager::ProcessAddressList(mimepp::AddressList &list, DbRecordSet &lstRowResult, int nType, bool bAreTemplates, QSet<QString> &setTmp)
{
	int nAddrCnt = list.numAddresses();
	for(int i=0; i<nAddrCnt; i++)
	{
		if(!bAreTemplates)
		{
			QString strName, strEmail;
			mimepp::Address &addr = list.addressAt(i);
			mimepp::Mailbox addr1(addr.getString());
			addr1.parse();

			strName  = QString::fromUtf8(addr1.displayNameUtf8().c_str());
			strEmail  = addr1.localPart().c_str();
			strEmail += "@";
			strEmail += addr1.domain().c_str();

			//append new recipient
			QString strValue;
			if(1 == nType)
				strValue = lstRowResult.getDataRef(0, "BEM_TO").toString();
			else if(2 == nType)
				strValue = lstRowResult.getDataRef(0, "BEM_CC").toString();
			else if(3 == nType)
				strValue = lstRowResult.getDataRef(0, "BEM_BCC").toString();

			if(!strValue.isEmpty())
				strValue += ";";

			QString strMail(strName);
			if(!strEmail.isEmpty()){
				strMail += " <";
				strMail += strEmail;
				strMail += ">";
			}

			strValue += strMail;

			//qDebug() << "Email" << strEmail;
			setTmp.insert(strEmail);

			//store back the value
			if(1 == nType)
				lstRowResult.setData(0, "BEM_TO", strValue);
			else if(2 == nType)
				lstRowResult.setData(0, "BEM_CC", strValue);
			else if(3 == nType)
				lstRowResult.setData(0, "BEM_BCC", strValue);
		}
	}
}
#endif

void MailManager::ParseMail_Thunderbird(MailInfo &message, DbRecordSet &lstRowResult, QSet<QString> &setEmails, bool bAreTemplates)
{
	lstRowResult.clear();
	lstRowResult.addRow();

#ifdef _WIN32
	mimepp::Mailbox addr1(message.m_strFrom.toLatin1().constData());
	addr1.parse();

	QString strData;
	strData  = QString::fromUtf8(addr1.displayNameUtf8().c_str());
	strData  += " <";
	strData  += addr1.localPart().c_str();
	strData  += "@";
	strData  += addr1.domain().c_str();
	strData  += ">";

	QSet<QString> setTmp;	// temp email set (we don't know yet if this msg will pass the filter)
	setTmp.insert(strData);

	if(bAreTemplates)
	{
		lstRowResult.setData(0, "BEM_TEMPLATE_FLAG", 1);
		lstRowResult.setData(0, "BEM_TEMPLATE_NAME", message.m_strSubject);
	}
	else
		lstRowResult.setData(0, "BEM_FROM", strData);

	//fix date to local date
	QDateTime dtRev = message.m_dtSent;
	dtRev.setTimeSpec(Qt::UTC);
	QDateTime tmpDat=dtRev.toLocalTime();
	lstRowResult.setData(0, "BEM_RECV_TIME", tmpDat);

	//qDebug() << "MSG START:" << message.m_strContent.toLatin1().constData() << ":MSG END";

#ifdef _DEBUG
#if 0
	QFile file("D:\\mail.txt");
	if(file.open(QIODevice::WriteOnly))
		file.write(message.m_strContent.toLatin1());
#endif
#endif

	//parse a single message
	mimepp::Message msg(message.m_strContent.toLatin1().constData());
	msg.parse();

	// Check content-transfer-encoding, and decode if necessary
	QString strSubject;
    int cte = mimepp::TransferEncodingType::_7BIT;
    if (msg.headers().hasField("Content-Transfer-Encoding")) {
        cte = msg.headers().contentTransferEncoding().asEnum();
    }
    if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
        mimepp::QuotedPrintableDecoder dec;
        strSubject = dec.decode(msg.headers().subject().text().c_str()).c_str();
    }
    else if (cte == mimepp::TransferEncodingType::BASE64) {
		mimepp::Base64Decoder dec;
        strSubject = dec.decode(msg.headers().subject().text().c_str()).c_str();
    }
	else
		strSubject = msg.headers().subject().text().c_str();
	lstRowResult.setData(0, "BEM_SUBJECT", strSubject);

	ProcessAddressList(msg.headers().to(),  lstRowResult,	1,	bAreTemplates, setTmp);
	ProcessAddressList(msg.headers().cc(),  lstRowResult,	2,	bAreTemplates, setTmp);
	ProcessAddressList(msg.headers().bcc(), lstRowResult,	3,	bAreTemplates, setTmp);

	//append emails from this mail
	setEmails += setTmp;

	bool bRead = (MSG_FLAG_READ == (message.m_nXMozillaStatus & MSG_FLAG_READ));
	lstRowResult.setData(0, "BEM_UNREAD_FLAG", bRead ? 0 : 1);

	bool bBodyPartSet = false;
	bool bBodyPartHTML = false;

	//store attachments
	DbRecordSet lstAttachments;
	lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
	if(Thunderbird_GetMailAttBodyRecursive(msg, lstRowResult, lstAttachments)){
		bBodyPartSet = true;
	}
	lstRowResult.setData(0, "ATTACHMENTS", lstAttachments);
	lstRowResult.setData(0, "ATTACHMENTS_SIZE", lstAttachments.getStoredByteSize());

	//only if no body part found (not multipart)
	if(!bBodyPartSet){
		QString strRTF = msg.body().getString().c_str();
		if(!strRTF.isEmpty()){
			//can still be text-only, check mail type
			bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0);
			lstRowResult.setData(0, "BEM_BODY", strRTF);
			lstRowResult.setData(0, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);
		}
	}

	if(message.m_bIncoming)
		lstRowResult.setData(0, "BEM_OUTGOING", 0);
	else
		lstRowResult.setData(0, "BEM_OUTGOING", 1);

#ifdef _DEBUG
	qDebug() << "From:" << lstRowResult.getDataRef(0, "BEM_FROM").toString();
	qDebug() << "To:"   << lstRowResult.getDataRef(0, "BEM_TO").toString();
	qDebug() << "CC:"   << lstRowResult.getDataRef(0, "BEM_CC").toString();
	qDebug() << "BCC:"  << lstRowResult.getDataRef(0, "BEM_BCC").toString();
	qDebug() << "Subject:"	   << lstRowResult.getDataRef(0, "BEM_SUBJECT").toString();
	qDebug() << "IsOutgoing:"  << lstRowResult.getDataRef(0, "BEM_OUTGOING").toInt();
#endif

	//lstRowResult.setData(0, "CENT_IS_PRIVATE", 0);

	//store entry ID
	QByteArray arEntry(msg.headers().messageId().getString().c_str());
	lstRowResult.setData(0, "BEM_EXT_ID", arEntry);

#ifdef _DEBUG
#if 0
	QFile file("D:\\aaa.txt");
	if(file.open(QIODevice::WriteOnly))
		file.write(arEntry);
#endif
#endif

	//store app name
	lstRowResult.setData(0, "BEM_EXT_APP", "Thunderbird");
#endif //#ifdef _WIN32
}


#ifndef WINCE

void MailManager::GetThunderbirdMails(DbRecordSet &m_lstData,QList<QStringList> lstFoldersThunder,QSet<QString> &setEmails,bool bAreTemplates,FN_FILTER_MATCH_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject)
{
	QSize size(300,100);
	QProgressDialog progress(QObject::tr("Listing Emails..."),QObject::tr("Cancel"),0,0,QApplication::activeWindow());
	progress.setWindowTitle(QObject::tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::WindowModal);
	progress.setMinimumDuration(1200);//1.2s before pops up

	int nCounter = 0;
	bool bSkip = false;
	
	int nFoldersCnt = lstFoldersThunder.count();

	for(int i=0; i<nFoldersCnt; i++)
	{
		QString strFile = OutlookFolderPickerDlg::ThunderbirdFolder2String(lstFoldersThunder[i]);
		MboxParser parser;
		if(parser.open(strFile)){
			parser.buildMailIndex();
			parser.close();

			int nMsgCnt = parser.m_lstMsgs.size();
			for(int j=0; j<nMsgCnt; j++)
			{
				FillMailContents(strFile, parser.m_lstMsgs, j);
				AddEmail_Thunderbird(m_lstData,parser.m_lstMsgs[j], nCounter, progress, bSkip, setEmails, false,pfCallBack_FilterMatchRow,nCallerObject);
				parser.m_lstMsgs[j].m_strContent = ""; //clear memory
				if(bSkip) break;
			}
		}
	}

}
#endif

void MailManager::AddEmail_Thunderbird(DbRecordSet &m_lstData,MailInfo &message, int &nCounter, QProgressDialog &progress, bool &bSkip, QSet<QString> &setEmails, bool bAreTemplates,FN_FILTER_MATCH_ROW pfCallBack_FilterMatchRow, unsigned long nCallerObject)
{
	//if the message is deleted, do not add it
	if(MSG_FLAG_EXPUNGED == (message.m_nXMozillaStatus & MSG_FLAG_EXPUNGED))
		return;

	int nIdx = m_lstData.getRowCount()-1;

	if(nCounter%4 == 0)
	{
		//update progress
		progress.setValue(1);
		qApp->processEvents();
		if (progress.wasCanceled()){
			QMessageBox::information(NULL, QObject::tr("Information"), QObject::tr("Operation stopped by user request!"));
			bSkip = true;
			return;
		}
	}
	nCounter ++;

	m_lstData.addRow();
	nIdx ++;

	//parse a single email row
	QSet<QString> setTmp;
	DbRecordSet lstRowResult;
	lstRowResult.copyDefinition(m_lstData);
	lstRowResult.addRow();

	ParseMail_Thunderbird(message, lstRowResult, setTmp, bAreTemplates);
	m_lstData.assignRow(nIdx, lstRowResult);

	//row that does not match filter will be removed
	if(!pfCallBack_FilterMatchRow(nCallerObject,nIdx))
	//if(!FilterMatchRow(nIdx))
	{
		m_lstData.deleteRow(nIdx);
		nIdx --;
		return;
	}

	//append emails from this mail
	setEmails += setTmp;
}

#ifndef WINCE

bool MailManager::IsThunderbirdInstalled()
{
	return MboxParser::IsThunderbirdInstalled();
}

void MailManager::GetThunderBirdEmailEntries(QString strMboxPath,DbRecordSet &lstDataEmails,QDateTime &dtLastEmail,QDateTime dtLastThunderbirdScan, int nStartOffset, bool bOnlyOutgoingEmails)
{
	qDebug() << "GetThunderBirdEmailEntries for mbox = " << strMboxPath << "(starting from offset: " << nStartOffset << ")";

	//Note: mbox file does not store mails sorted by date
	MboxParser parser;
	if(parser.open(strMboxPath))
	{
		parser.buildMailIndex(nStartOffset);
		parser.close();

		int nMsgCnt = parser.m_lstMsgs.size();
		for(int j=0; j<nMsgCnt; j++)
		{
			//if the message is deleted, do not add it
			if(MSG_FLAG_EXPUNGED == (parser.m_lstMsgs[j].m_nXMozillaStatus & MSG_FLAG_EXPUNGED))
				continue;

			//fix date to local date
			QDateTime dtRev = parser.m_lstMsgs[j].m_dtSent;
			//Q_ASSERT(dtRev.isValid());
			//if(!dtRev.isValid())
			//	continue;
			dtRev.setTimeSpec(Qt::UTC);
			QDateTime tmpDat=dtRev.toLocalTime();

			//skip if one of the older mails
			if(tmpDat <= dtLastThunderbirdScan)
				continue;

			//remember latest message date
			if(dtLastEmail.isValid()){
				if(tmpDat > dtLastEmail)
					dtLastEmail = tmpDat;
			}
			else
				dtLastEmail = tmpDat;

			//parse a single email
			QSet<QString> setTmp;
			DbRecordSet lstRowResult;
			lstRowResult.copyDefinition(lstDataEmails);
			lstRowResult.addRow();
			FillMailContents(strMboxPath, parser.m_lstMsgs, j);
			ParseMail_Thunderbird(parser.m_lstMsgs[j], lstRowResult, setTmp, false);
			parser.m_lstMsgs[j].m_strContent = ""; //clear memory

			//skip incoming messages if needed
			if( bOnlyOutgoingEmails && 
				parser.m_lstMsgs[j].m_bIncoming)
				continue;

			lstDataEmails.addRow();
			lstDataEmails.assignRow(lstDataEmails.getRowCount()-1, lstRowResult);

			QString strMessage=QObject::tr("New message found:") + lstRowResult.getDataRef(0,"BEM_SUBJECT").toString();
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);
			//qDebug() << strMessage;
		}
	}
}

void MailManager::FillMailContents(QString strMboxPath, std::vector<MailInfo> &lstMsgs, int i)
{
	FILE *pMbox = fopen(QFile::encodeName(strMboxPath).constData(), "r");
	if(pMbox){
		int nMsgCnt = lstMsgs.size();
		int nUntilByte = -1;
		if(i < nMsgCnt-1)
			nUntilByte = lstMsgs[i+1].m_nStartOffset;

		//jump to the start of the message
		fseek(pMbox, lstMsgs[i].m_nStartOffset, SEEK_SET);

		//read the contents
		char szLine[4000];
		while(1){
			off_t pos = ftell(pMbox);
			if(nUntilByte >= 0 && pos >= nUntilByte)
				break;
			if (!fgets(szLine, sizeof(szLine), pMbox)) 
				break;
			lstMsgs[i].m_strContent += szLine;
		}

		fclose(pMbox);
	}
}

#endif

#ifdef _WIN32
bool MailManager::Thunderbird_GetMailAttBodyRecursive(mimepp::Entity &parent, DbRecordSet &lstRowResult, DbRecordSet &lstAttachments)
{
	//recursively drill down to find the correct body and attachments (may have multipart inside the multipart, ...)
	bool bBodyPartSet = false;
	bool bBodyPartHTML = false;

	int nParts = parent.body().numBodyParts();
	for(int i=0; i<nParts; i++)
	{
		mimepp::BodyPart &part = parent.body().bodyPartAt(i);
		mimepp::MediaType::MType type = part.headers().contentType().typeAsEnum();

		//extract attachment name
		QString strContType = part.headers().fieldBody("Content-Type").text().c_str();
		QString strName;
		int nPos = strContType.indexOf("name=\"");
		if(nPos >= 0){
			strName = strContType.mid(nPos+6);
			nPos = strName.indexOf("\"");
			if(nPos >= 0)
				strName = strName.left(nPos);
		}
		if(strName.isEmpty())
		{
			// not an attachment, must be the body
			if(!bBodyPartSet || !bBodyPartHTML)
			{
				if (type == mimepp::MediaType::MULTIPART)
				{
					if(Thunderbird_GetMailAttBodyRecursive(part, lstRowResult, lstAttachments)){
						 bBodyPartSet = true;
						 bBodyPartHTML = lstRowResult.getDataRef(0, "BEM_EMAIL_TYPE").toInt();
					}
				}
				else if (type == mimepp::MediaType::TEXT)
				{
					//refresh body
					QString strRTF = part.body().getString().c_str();
					if(!strRTF.isEmpty()){
						//can still be text-only, check mail type
						bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0);
						lstRowResult.setData(0, "BEM_BODY", strRTF);
						lstRowResult.setData(0, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);
						bBodyPartHTML = bHtml;
					}
					bBodyPartSet = true;
				}
				else{
					//int i=0; //TOFIX ?
				}
			}
		}
		else
		{
			//an attachment
			lstAttachments.addRow();
			int nRow = lstAttachments.getRowCount()-1;
			lstAttachments.setData(nRow, "BEA_NAME", strName);

			//Attachment contents
			// Check content-transfer-encoding, and decode if necessary
			mimepp::String text = part.body().getString();
			int cte = mimepp::TransferEncodingType::_7BIT;
			if (part.headers().hasField("Content-Transfer-Encoding")) {
				cte = part.headers().contentTransferEncoding().asEnum();
			}
			if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
				mimepp::QuotedPrintableDecoder dec;
				text = dec.decode(text);
			}
			else if (cte == mimepp::TransferEncodingType::BASE64) {
				mimepp::Base64Decoder dec;
				text = dec.decode(text);
			}

			int nLen = text.length();
			QByteArray att(text.c_str(), nLen);
			//B.T.: compress attachment, damn it:
			att=qCompress(att,1); //min compression
			lstAttachments.setData(nRow, "BEA_CONTENT", att);

			//extract CID
			if (part.headers().hasField("Content-ID")){
				QString strCID = part.headers().fieldBody("Content-ID").text().c_str();
				//strip <> quotes
				strCID.chop(1); 
				strCID = strCID.right(strCID.length()-1);
				lstAttachments.setData(nRow, "BEA_CID_LINK", strCID);
				qDebug() << "Attachment CID:" << strCID;
			}
		}
	}
	return bBodyPartSet;
}
#endif

bool MailManager::SendAppointmentDirectly(QString strBody, QString strSubject, QString strToEmail, SMTPConnectionSettings connSettings, bool bEMAIL_SETTINGS_USE_AUTH)
{
	bool bOK=true;
#ifdef _WIN32
	QString strHost  = connSettings.m_strHost;
	int nPort        = connSettings.m_nPort;
	QString strName  = connSettings.m_strUserName;
	QString strEmail = connSettings.m_strUserEmail;
	QString strAccName = connSettings.m_strAccountName;
	QString strAccPass = DecryptSMTPPassword(connSettings.m_strAccountPassword);
	bool bUseSSL = connSettings.m_bUseSSL;
	bool bUseSTLS = connSettings.m_bUseSTLS;

	//prepare message
	mimepp::Message msg;

	// Set the content-type header field to "text/calendar"
	mimepp::MediaType& mtype = msg.headers().contentType();
	mtype.setType("text");
	mtype.setSubtype("calendar");
	mimepp::Parameter* param = new mimepp::Parameter;
	param->setName("charset");
	param->setValue("utf-8");
	mtype.addParameter(param);
	param = 0;

	param = new mimepp::Parameter;
	param->setName("method");
	param->setValue("REQUEST");
	mtype.addParameter(param);
	param = 0;

	//add calendar body (not as a part, but as a main text)
	mimepp::String strData(strBody.toUtf8().constData());
	msg.body().setString(strData.c_str());

	//"From" email
	QString strTmpEmail = strName;
	strTmpEmail += " <";
	strTmpEmail += strEmail;
	strTmpEmail += ">";

	mimepp::Mailbox *address1 = new mimepp::Mailbox(strTmpEmail.toLocal8Bit().constData());
	address1->parse();
	msg.headers().from().addMailbox(address1);

	//"To" email
	mimepp::Address *address;
	strTmpEmail  = " <";
	strTmpEmail += strToEmail;
	strTmpEmail += ">";

	address = new mimepp::Mailbox(strTmpEmail.toLocal8Bit().constData());
	address->parse();
	msg.headers().to().addAddress(address);

	//set subject
	msg.headers().subject().setUtf8Text(mimepp::String(strSubject.toUtf8().constData()));

	msg.assemble();
	// Serialize the message
	mimepp::String msgStr;
	msgStr = msg.getString();

	qDebug() << "SMTP full message:" << msgStr.c_str();

	//
	// Create an SmtpSession object and connect to the SMTP server
	//
	SmtpSession session(strHost.toLocal8Bit().constData());
	session.SetTracer(new myTraceOutput());

	if(InitSmtpSession(session, strHost, nPort, strAccName, strAccPass, bEMAIL_SETTINGS_USE_AUTH, bUseSSL, bUseSTLS))
	{
		//
		// Create a MailTransaction instance.  The MailTransaction requires a
		// sender, at least one recipient, and the name of a file that contains
		// the message to send.
		//
		MailTransaction trans;
		trans.SetFrom(strEmail.toLocal8Bit().constData());
		trans.AddTo(strToEmail.toLocal8Bit().constData());
		trans.SetBody(msgStr.c_str());

		//
		// Provide the MailTransaction to the SmtpSession to process
		//
		session.DoTransaction(trans);

		//
		// We could process more transactions now.  We won't.  But you can
		// if you want to. :-)  We will just close the session and quit
		// this example program.
		//
		session.Close();

		int nErr = trans.m_nLastErr;
		if(nErr != 0){
			bOK = false;
		}
	}

	//
	// Finalize the library
	//
	mimepp::Finalize();
#endif
	return bOK;
}

//#endif // WINCE

#define ADD_ENTRY(ext, type, sub)	mapExt2Type[ext] = type;  mapExt2Subtype[ext] = sub;

void MailManager::GetMimeFromFile(QString strFile, QString &strType, QString &strSubtype)
{
	static QMap<QString, QString> mapExt2Type;
	static QMap<QString, QString> mapExt2Subtype;

	//one-time initialization
	if(mapExt2Type.empty())
	{
		ADD_ENTRY("html",	"text", "html");
		ADD_ENTRY("htm",	"text", "html");
		ADD_ENTRY("txt",	"text", "plain");
		ADD_ENTRY("js",		"text", "javascript");
		ADD_ENTRY("css",	"text", "css");
		ADD_ENTRY("xml",	"text", "xml");
		ADD_ENTRY("doc",	"application", "msword");
		ADD_ENTRY("dot",	"application", "msword");
		ADD_ENTRY("docx",	"application", "vnd.openxmlformats-officedocument.wordprocessingml.document");
		ADD_ENTRY("zip",	"application", "zip");
		ADD_ENTRY("pdf",	"application", "pdf");
		ADD_ENTRY("gif",	"image", "gif");
		ADD_ENTRY("jpeg",	"image", "jpeg");
		ADD_ENTRY("jpg",	"image", "jpeg");
		ADD_ENTRY("png",	"image", "png");
		ADD_ENTRY("tiff",	"image", "tiff");
		ADD_ENTRY("tif",	"image", "tiff");
		ADD_ENTRY("mpeg",	"video", "mpeg");
		ADD_ENTRY("mpg",	"video", "mpeg");
		ADD_ENTRY("mp4",	"video", "mp4");
		ADD_ENTRY("wmv",	"video", "x-ms-wmv");
		ADD_ENTRY("wav",	"audio", "x-wav");
		ADD_ENTRY("xls",	"application", "vnd.ms-excel");
		ADD_ENTRY("xlsx",	"application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		ADD_ENTRY("ppt",	"application", "vnd.ms-powerpoint");
		ADD_ENTRY("pptx",	"application", "vnd.openxmlformats-officedocument.presentationml.presentation");
		ADD_ENTRY("ppsx",	"application", "vnd.openxmlformats-officedocument.presentationml.slideshow");
		ADD_ENTRY("dll",	"application", "x-msdownload");
		ADD_ENTRY("odt",	"application", "vnd.oasis.opendocument.text");
		ADD_ENTRY("odg",	"application", "vnd.oasis.opendocument.graphics");
		ADD_ENTRY("odp",	"application", "vnd.oasis.opendocument.graphics");
		ADD_ENTRY("ods",	"application", "vnd.oasis.opendocument.graphics");
		ADD_ENTRY("manifest",	"text", "cache-manifest");
	}

	QFileInfo info(strFile);
	QString strExt=info.suffix();

	if(mapExt2Type.contains(strExt))
	{
		strType	= mapExt2Type[strExt];
		strSubtype = mapExt2Subtype[strExt];
	}
	else
	{
		//default (binary data): "application/octet-stream";
		strType	= "application";
		strSubtype = "octet-stream";
	}
}

