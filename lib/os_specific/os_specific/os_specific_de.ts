<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>MapiManager</name>
    <message>
        <location filename="mapimanager.cpp" line="384"/>
        <location filename="mapimanager.cpp" line="389"/>
        <location filename="mapimanager.cpp" line="397"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="384"/>
        <location filename="mapimanager.cpp" line="397"/>
        <source>Error getting method from MAPI dll!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="389"/>
        <source>Error opening email!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutlookFolderPickerDlg</name>
    <message>
        <location filename="outlookfolderpickerdlg.cpp" line="73"/>
        <source>Select Thunderbird folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="outlookfolderpickerdlg.cpp" line="538"/>
        <source>Profiles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutlookFolderPickerDlgClass</name>
    <message>
        <location filename="outlookfolderpickerdlg.ui" line="13"/>
        <source>Select Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="outlookfolderpickerdlg.ui" line="54"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="outlookfolderpickerdlg.ui" line="74"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="outlookfolderpickerdlg.ui" line="84"/>
        <source>Remember Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="outlookfolderpickerdlg.ui" line="92"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PortableSkypeSearchDlgClass</name>
    <message>
        <location filename="portableskypesearchdlg.ui" line="13"/>
        <source>Search Portable Skype Installation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="portableskypesearchdlg.ui" line="21"/>
        <source>Search Skype(R) Installation on Memory Device Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="portableskypesearchdlg.ui" line="31"/>
        <source>No Skype(R) Installation on Memory Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="portableskypesearchdlg.ui" line="66"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="portableskypesearchdlg.ui" line="73"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="mailmanager.cpp" line="196"/>
        <source>Sending Emails...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mailmanager.cpp" line="196"/>
        <location filename="mailmanager.cpp" line="872"/>
        <location filename="mapimanager.cpp" line="85"/>
        <location filename="mapimanager.cpp" line="915"/>
        <location filename="mapimanager.cpp" line="1170"/>
        <location filename="mapimanager.cpp" line="1475"/>
        <location filename="mapimanager.cpp" line="1630"/>
        <location filename="twixtelmanager.cpp" line="21"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mailmanager.cpp" line="197"/>
        <source>Send In Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mailmanager.cpp" line="872"/>
        <location filename="mapimanager.cpp" line="915"/>
        <location filename="mapimanager.cpp" line="1475"/>
        <source>Listing Emails...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mailmanager.cpp" line="873"/>
        <location filename="mapimanager.cpp" line="916"/>
        <location filename="mapimanager.cpp" line="1171"/>
        <location filename="mapimanager.cpp" line="1476"/>
        <location filename="mapimanager.cpp" line="1631"/>
        <location filename="twixtelmanager.cpp" line="22"/>
        <source>Operation In Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mailmanager.cpp" line="919"/>
        <location filename="mapimanager.cpp" line="975"/>
        <location filename="mapimanager.cpp" line="1234"/>
        <location filename="mapimanager.cpp" line="1721"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mailmanager.cpp" line="919"/>
        <location filename="mapimanager.cpp" line="975"/>
        <location filename="mapimanager.cpp" line="1234"/>
        <location filename="mapimanager.cpp" line="1721"/>
        <source>Operation stopped by user request!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mailmanager.cpp" line="1013"/>
        <source>New message found:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="48"/>
        <location filename="mapimanager.cpp" line="140"/>
        <location filename="mapimanager.cpp" line="147"/>
        <location filename="mapimanager.cpp" line="166"/>
        <location filename="mapimanager.cpp" line="878"/>
        <location filename="mapimanager.cpp" line="887"/>
        <location filename="mapimanager.cpp" line="1978"/>
        <location filename="mapimanager.cpp" line="1984"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="48"/>
        <source>Failed to load MAPIex.dll (error:%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="85"/>
        <source>Deleting Temporary Outlook Mails...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="86"/>
        <source>Cleanup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="130"/>
        <location filename="mapimanager.cpp" line="901"/>
        <location filename="mapimanager.cpp" line="910"/>
        <location filename="mapimanager.cpp" line="1156"/>
        <location filename="mapimanager.cpp" line="1165"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="130"/>
        <source>Message does not have Entry ID!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="140"/>
        <location filename="mapimanager.cpp" line="166"/>
        <location filename="mapimanager.cpp" line="878"/>
        <location filename="mapimanager.cpp" line="887"/>
        <source>Error getting method from MAPI dll!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="147"/>
        <source>Error opening email!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="901"/>
        <location filename="mapimanager.cpp" line="1156"/>
        <source>There are no folders selected!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="910"/>
        <location filename="mapimanager.cpp" line="1165"/>
        <source>Failed to initialize MAPI!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="1170"/>
        <source>Listing Appointments...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="1630"/>
        <source>Listing Contacts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="1978"/>
        <source>Please setup your SMTP settings!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mapimanager.cpp" line="1984"/>
        <source>Error sending appointment!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="skype/skype.cpp" line="356"/>
        <source>Portable Skype was not found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="skype/skype.cpp" line="392"/>
        <source>Sokrates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="skype/skype.cpp" line="392"/>
        <source>Initializing Phone Interface...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="skype/skype.cpp" line="607"/>
        <location filename="skype/skype.cpp" line="617"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="skype/skype.cpp" line="607"/>
        <source>Skype access refused!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="skype/skype.cpp" line="617"/>
        <source>Skype access not available!
Please make sure you are logged into Skype.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="twixtelmanager.cpp" line="21"/>
        <source>TwixTel query...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="twixtelmanager.cpp" line="124"/>
        <source>No records found!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
