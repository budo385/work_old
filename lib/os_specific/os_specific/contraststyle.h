#ifndef CONTRASTSTYLE_H
#define CONTRASTSTYLE_H

#ifndef WINCE
/*
#ifdef _WIN32
 #include <QWindowsXPStyle>
#else
 #include <QPlastiqueStyle>
#endif
#include <QPalette>

#ifdef _WIN32
#define BASE_STYLE QWindowsXPStyle
#else
#define BASE_STYLE QPlastiqueStyle
#endif
 */
#include <QProxyStyle>
//class ContrastStyle : public BASE_STYLE
class ContrastStyle : public QProxyStyle
{
public:
    ContrastStyle();
    ~ContrastStyle();

	void polish(QPalette &palette);
    int styleHint(StyleHint hint, const QStyleOption *option,
                  const QWidget *widget, QStyleHintReturn *returnData) const;
};

#endif // WINCE

#endif // CONTRASTSTYLE_H
