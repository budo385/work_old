#include "useraccessright_client.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

UserAccessRight_Client::UserAccessRight_Client()
{
	
}


UserAccessRight_Client::~UserAccessRight_Client()
{

}

int UserAccessRight_Client::GetUserID()
{
	return g_pClientManager->GetUserID();
}

int	UserAccessRight_Client::GetPersonID()
{
	return g_pClientManager->GetPersonID();
}
bool UserAccessRight_Client::IsSystemUser()
{
	return g_pClientManager->IsSystemUser();
}

void UserAccessRight_Client::TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed)
{
	_SERVER_CALL(AccessRights->TestUAR(Ret_pStatus,nTableID,nPersonID,nAccessLevel,lstTest_IDs,Ret_lstResult_IDs,bReturnAllowed))
}
