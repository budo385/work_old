#ifndef TREE_SELECTIONGROUP_H
#define TREE_SELECTIONGROUP_H


#include "gui_core/gui_core/universaltreewidget.h"


class Tree_SelectionGroup : public UniversalTreeWidget
{
	Q_OBJECT

public:
    Tree_SelectionGroup(QWidget *parent);
    ~Tree_SelectionGroup();

	void DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);

	QTreeWidgetItem *m_lastitem;
	DbRecordSet m_lastDroppedValue;

signals:
	void DropOccured();

private:
    
};

#endif // TREE_SELECTIONGROUP_H
