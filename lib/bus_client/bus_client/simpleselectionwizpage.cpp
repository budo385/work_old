#include "simpleselectionwizpage.h"
//#include "wizardregistration.h"

#include "simpleselectionwiztree.h"
#include "gui_core/gui_core/universaltreewidget.h"

#include "db_core/db_core/dbsqltableview.h"
#include "bus_client/bus_client/selection_contacts.h"
#include "bus_client/bus_client/selection_tablebased.h"
#include "bus_client/bus_client/selection_treebased.h"
#include "bus_client/bus_client/selection_users.h"
#include <QInputDialog>
#include <QMimeData>
#include "common/common/entity_id_collection.h"

SimpleSelectionWizPage::SimpleSelectionWizPage(int nWizardPageID, QString strPageTitle, int nPageEntityID /*= -1*/, QWidget *parent /*=0*/, bool bShowNewBtn /*= false*/)
    : WizardPage(nWizardPageID, strPageTitle)
{
	ui.setupUi(this);

	m_nPageEntityID = nPageEntityID;
	m_pSelectorWidget = NULL;

	ui.AddSelected_pushButton->setIconSize(QSize(50, 55));
	ui.AddAll_pushButton->setIconSize(QSize(50, 55));
	ui.RemoveSelected_pushButton->setIconSize(QSize(50, 55));
	ui.AddSelected_pushButton->setIcon(QIcon(":Selection_Arrow_Right.png"));
	ui.AddSelected_pushButton->setToolTip(tr("Add Elements to Selection"));
	ui.AddAll_pushButton->setIcon(QIcon(":Selection_Arrow_Right_Plus.png"));
	ui.AddAll_pushButton->setToolTip(tr("Add Elements and Subelements to Selection"));
	ui.RemoveSelected_pushButton->setIcon(QIcon(":Selection_Arrow_Remove.png"));
	ui.RemoveSelected_pushButton->setToolTip(tr("Remove From Selection"));
	ui.AddSelected_pushButton->setFlat(true);
	ui.AddAll_pushButton->setFlat(true);
	ui.RemoveSelected_pushButton->setFlat(true);
	
	ui.RemoveAll_pushButton->hide();
	ui.RemoveSelected_pushButton->setEnabled(false);

	if(!bShowNewBtn)
		ui.AddNew_pushButton->hide();

	m_mainDataCacheSelector.Initialize(nPageEntityID);
}

SimpleSelectionWizPage::~SimpleSelectionWizPage()
{

}

void SimpleSelectionWizPage::Initialize()
{
	//-------------------------------
	//Standard Initialize stuff.
	//-------------------------------
	if (m_bInitialized)
		return;
	m_bInitialized = true;
	//-------------------------------

	ReadFromCache();
	
	EntityType();
}

void SimpleSelectionWizPage::EntityType()
{
	//determine selector type by trick: instante MainEntitySelectionController and get datatype
	m_nSelectorType = m_mainDataCacheSelector.GetDataType(m_nPageEntityID);

	//create selector widget:
	Q_ASSERT(m_pSelectorWidget == NULL); //must be NULL or we are doing something wrong!

	switch(m_nSelectorType)
	{
	case MainEntitySelectionController::TYPE_TABLE:
		m_pSelectorWidget = new Selection_TableBased;
		m_pSelectedWidget = new SimpleSelectionWizTree;
		dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->Initialize(m_nPageEntityID, false, false, true, true, true); //enable double click!
		InsertTableItems(m_recResultRecordSet, NULL, 0, true);
		connect(dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget), SIGNAL(DataDroped(QTreeWidgetItem*, int, const QMimeData*, Qt::DropAction)), this, SLOT(onSelectedTreeDroped(QTreeWidgetItem*, int, const QMimeData*, Qt::DropAction)));
		connect(dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget), SIGNAL(itemSelectionChanged()), this, SLOT(onItemSelectionChanged()));
		//Issue #2027.
		ui.AddAll_pushButton->hide();
		break;
	case MainEntitySelectionController::TYPE_TREE:
		m_pSelectorWidget = new Selection_TreeBased;
		m_pSelectedWidget = new UniversalTreeWidget;
		dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->Initialize(m_nPageEntityID, false, false, true, true, true); //enable double click!
		connect(dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget), SIGNAL(DataDropedAndProcessed()), this, SLOT(onSelectedTreeChanged()));
		connect(dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget), SIGNAL(itemSelectionChanged()), this, SLOT(onItemSelectionChanged()));
		//initialize TREE (BT)
		m_recResultRecordSet.copyDefinition(*(dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->GetDataSource()));
		dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->Initialize(&m_recResultRecordSet, true);
		dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->AddAcceptableDropTypes(m_nPageEntityID);
		dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->SetHeader();
		break;
	case MainEntitySelectionController::TYPE_CONTACTS:
		m_pSelectorWidget = new Selection_Contacts;
		m_pSelectedWidget = new SimpleSelectionWizTree;
		dynamic_cast<Selection_Contacts*>(m_pSelectorWidget)->Initialize(false, false, true, true, true); //enable double click!
		InsertContactItems(m_recResultRecordSet, NULL, 0, true);
		connect(dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget), SIGNAL(DataDroped(QTreeWidgetItem*, int, const QMimeData*, Qt::DropAction)), this, SLOT(onSelectedTreeDroped(QTreeWidgetItem*, int, const QMimeData*, Qt::DropAction)));
		connect(dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget), SIGNAL(itemSelectionChanged()), this, SLOT(onItemSelectionChanged()));
		//Set multiple selection on selected widget so that multiple items can be removed in one step.
		dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->setSelectionMode(QAbstractItemView::ExtendedSelection);
		//Issue #2027.
		ui.AddAll_pushButton->hide();
		break;
	case MainEntitySelectionController::TYPE_USERS:
		m_pSelectorWidget = new Selection_Users;
		m_pSelectedWidget = new SimpleSelectionWizTree;
		dynamic_cast<Selection_Users*>(m_pSelectorWidget)->Initialize(ENTITY_BUS_PERSON, false, false, true, true, true); //enable double click!
		InsertContactItems(m_recResultRecordSet, NULL, 0, true);
		connect(dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget), SIGNAL(DataDroped(QTreeWidgetItem*, int, const QMimeData*, Qt::DropAction)), this, SLOT(onSelectedTreeDroped(QTreeWidgetItem*, int, const QMimeData*, Qt::DropAction)));
		connect(dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget), SIGNAL(itemSelectionChanged()), this, SLOT(onItemSelectionChanged()));
		//Set multiple selection on selected widget so that multiple items can be removed in one step.
		dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->setSelectionMode(QAbstractItemView::ExtendedSelection);
		//Issue #2027.
		ui.AddAll_pushButton->hide();
		break;
	default:
		Q_ASSERT(false);
		break;
	}

	//plop widget on dialog
	ui.Select_stackedWidget->addWidget(m_pSelectorWidget);
	ui.Select_stackedWidget->setCurrentWidget(m_pSelectorWidget);

	ui.Selected_stackedWidget->addWidget(m_pSelectedWidget);
	ui.Selected_stackedWidget->setCurrentWidget(m_pSelectedWidget);
}

void SimpleSelectionWizPage::InsertTableItems(DbRecordSet &DropedRecordSet, QTreeWidgetItem *parent, int index, bool bOnInitialize /*= false*/)
{
	//B.T. Added 2007-01-11: optimize get from cache (load in sweep all names of all items):
	DbRecordSet lstCachedRecords;
	m_mainDataCacheSelector.GetEntityRecords(DropedRecordSet,0,lstCachedRecords);

	int nDropedCount = DropedRecordSet.getRowCount();
	for (int i = 0; i < nDropedCount; ++i)
	{
		//Check is it already inserted in tree.
		int nEntityID = DropedRecordSet.getDataRef(i, 0).toInt();
		if (m_recResultRecordSet.find(0, nEntityID, true) >= 0 && !bOnInitialize)
			continue;

		//Add this row to result recordset if not on initialize.
		if (!bOnInitialize){
			DbRecordSet row = DropedRecordSet.getRow(i);
			AddRowToResultRecordSet(row);
		}


		//Get item name
		//---------------------------------------------
		QString strEntityName;
		if (m_mainDataCacheSelector.m_nNameColumnIdx!=-1 && i<lstCachedRecords.getRowCount())
			if(nEntityID==lstCachedRecords.getDataRef(i,m_mainDataCacheSelector.m_nPrimaryKeyColumnIdx).toInt())
				strEntityName=lstCachedRecords.getDataRef(i,m_mainDataCacheSelector.m_nNameColumnIdx).toString();
		//---------------------------------------------

		//Now insert after selected item.
		int nTopLevelIndex = dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->indexOfTopLevelItem(parent);
		//If index -1 (invalid), insert after all.
		if (nTopLevelIndex == -1)
			nTopLevelIndex = dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->topLevelItemCount();

		QTreeWidgetItem *item = new QTreeWidgetItem;
		item->setData(0, Qt::UserRole, nEntityID);
		item->setData(0, Qt::DisplayRole, strEntityName);

		dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->insertTopLevelItem(index, item);
	}

	//Signal complete changed.
	CompleteChanged();
}

void SimpleSelectionWizPage::InsertTreeItems(DbRecordSet &DropedRecordSet, QTreeWidgetItem *parent, int index, bool bOnInitialize /*= false*/)
{

	//Q_ASSERT(false);
	//_DUMP(DropedRecordSet);
	//B.T.: avoid
/*
	dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->GetTreeWidget()->SetDropType(m_nWizardPageID);
	dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->AddAcceptableDropTypes(m_nWizardPageID);

	dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->SetHeader();
	dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->GetTreeWidget()->SetDropType(m_nWizardPageID);
	dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->AddAcceptableDropTypes(m_nWizardPageID);

	//Signal complete changed.
	CompleteChanged();
*/
}

void SimpleSelectionWizPage::InsertContactItems(DbRecordSet &DropedRecordSet, QTreeWidgetItem *parent, int index, bool bOnInitialize /*= false*/)
{
	//B.T. Added 2007-01-11: optimize get from cache (load in sweep all names of all items):
	DbRecordSet lstCachedRecords;
	m_mainDataCacheSelector.GetEntityRecords(DropedRecordSet,0,lstCachedRecords);

	int nDropedCount = DropedRecordSet.getRowCount();
	for (int i = 0; i < nDropedCount; ++i)
	{
		//Check is it already inserted in tree.
		int nEntityID = DropedRecordSet.getDataRef(i, 0).toInt();
		if (m_recResultRecordSet.find(0, nEntityID, true) >= 0 && !bOnInitialize)
			continue;

		//Add this row to result recordset if not on initialize.
		if (!bOnInitialize){
			DbRecordSet row = DropedRecordSet.getRow(i);
			AddRowToResultRecordSet(row);
		}

		//Get item name
		//---------------------------------------------
		QString strEntityName;
		if (m_mainDataCacheSelector.m_nNameColumnIdx!=-1 && i<lstCachedRecords.getRowCount())
			if(nEntityID==lstCachedRecords.getDataRef(i,m_mainDataCacheSelector.m_nPrimaryKeyColumnIdx).toInt())
				strEntityName=lstCachedRecords.getDataRef(i,m_mainDataCacheSelector.m_nNameColumnIdx).toString();
		//---------------------------------------------

		//Now insert after selected item.
		int nTopLevelIndex = dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->indexOfTopLevelItem(parent);
		//If index -1 (invalid), insert after all.
		if (nTopLevelIndex == -1)
			nTopLevelIndex = dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->topLevelItemCount();
		
		QTreeWidgetItem *item = new QTreeWidgetItem;
		item->setData(0, Qt::UserRole, nEntityID);
		item->setData(0, Qt::DisplayRole, strEntityName);

		dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->insertTopLevelItem(index, item);
	}

	//Signal complete changed.
	CompleteChanged();
}

void SimpleSelectionWizPage::AddRowToResultRecordSet(DbRecordSet &RecordSet)
{
	if (m_recResultRecordSet.getRowCount() == 0)
		m_recResultRecordSet.copyDefinition(RecordSet);

	m_recResultRecordSet.merge(RecordSet);
}

void SimpleSelectionWizPage::RemoveTreeItemsForTreeEntity()
{
	QList<int> selectedRows = m_recResultRecordSet.getSelectedRows();

	if (!selectedRows.count())
		return;

	//Find row ID's.
	QList<int> selectedRowID;
	QListIterator<int> iter(selectedRows);
	while (iter.hasNext())
	{
		int row   = iter.next();
		int RowID = m_recResultRecordSet.getDataRef(row, 0).toInt();
		selectedRowID << RowID;
	}

	//Delete by row ID's.
	QListIterator<int> rowIter(selectedRowID);
	while (rowIter.hasNext())
	{
		int RowID_toDelete = rowIter.next();
		int Row_toDelete = m_recResultRecordSet.find(0, RowID_toDelete, true);
		if (Row_toDelete != -1)
			m_recResultRecordSet.deleteRow(Row_toDelete);
	}

	dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->RefreshDisplay();

}

void SimpleSelectionWizPage::CompleteChanged()
{
	if (dynamic_cast<QTreeWidget*>(m_pSelectedWidget)->topLevelItemCount() != 0)
		m_bComplete = true;
	else
		m_bComplete = false;

	//m_recResultRecordSet.Dump();

	emit completeStateChanged();
}

void SimpleSelectionWizPage::onSelectedTreeChanged()
{
	CompleteChanged();
}


void SimpleSelectionWizPage::onSelectedTreeDroped(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action)
{
	//Get selected recordset.
	DbRecordSet tmpRecordSet = *(dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->GetDataSource());

	DbRecordSet SelectedRecodSet;
	SelectedRecodSet.copyDefinition(tmpRecordSet);
	SelectedRecodSet.merge(tmpRecordSet, true);

	switch(m_nSelectorType)
	{
	case MainEntitySelectionController::TYPE_TABLE:
		InsertTableItems(SelectedRecodSet, parent, index);
		break;
	case MainEntitySelectionController::TYPE_TREE:
		InsertTreeItems(SelectedRecodSet, parent, index);
		break;
	case MainEntitySelectionController::TYPE_CONTACTS:
		InsertContactItems(SelectedRecodSet, parent, index);
		break;
	case MainEntitySelectionController::TYPE_USERS:
		InsertContactItems(SelectedRecodSet, parent, index);
		break;
	default:
		Q_ASSERT(false);
		break;
	}
}

void SimpleSelectionWizPage::on_AddSelected_pushButton_clicked(bool bExpanItems /*= false*/)
{
	DbRecordSet RecordSet;
	switch(m_nSelectorType)
	{
	case MainEntitySelectionController::TYPE_TABLE:
	case MainEntitySelectionController::TYPE_CONTACTS:
	case MainEntitySelectionController::TYPE_USERS:	
		onSelectedTreeDroped(NULL, 0, new QMimeData(), Qt::MoveAction);
		break;
	case MainEntitySelectionController::TYPE_TREE:
		{
			dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->GetTreeWidget()->GetDropValue(RecordSet, true);

			dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->DropHandler(QModelIndex(), 0, RecordSet, NULL);
			
			if (bExpanItems)
				dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->ExpandItemByRowIDFromRecordSet(RecordSet);
			
			break;
		}
	default:
		Q_ASSERT(false);
		break;
	}

	//Signal complete changed.
	CompleteChanged();
}

void SimpleSelectionWizPage::on_AddNew_pushButton_clicked()
{
	//input item name
	QInputDialog dlg;
	dlg.exec();
}

void SimpleSelectionWizPage::on_AddAll_pushButton_clicked()
{
	/* 
	//Commented in issue #1963.
	//Select all in source recordset.
	switch(m_nSelectorType)
	{
	case MainEntitySelectionController::TYPE_TABLE:
	case MainEntitySelectionController::TYPE_CONTACTS:
		dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->GetDataSource()->selectAll();
		on_AddSelected_pushButton_clicked();
		dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->GetDataSource()->clearSelection();
		dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->RefreshDisplay();
		break;
	case MainEntitySelectionController::TYPE_TREE:
		dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->GetTreeWidget()->selectAll();
		on_AddSelected_pushButton_clicked();
		dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->GetTreeWidget()->clearSelection();
		dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->RefreshDisplay();
		break;
	}
	*/
	
	//Commented in issue #1963.
	on_AddSelected_pushButton_clicked(true);
}

void SimpleSelectionWizPage::on_RemoveSelected_pushButton_clicked()
{
	switch(m_nSelectorType)
	{
	case MainEntitySelectionController::TYPE_TABLE:
	case MainEntitySelectionController::TYPE_CONTACTS:
	case MainEntitySelectionController::TYPE_USERS:
		{
			QList<QTreeWidgetItem*> items = dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->selectedItems();
			foreach(QTreeWidgetItem *item, items)
			{
				int nEntityID = item->data(0, Qt::UserRole).toInt();
				//Delete from recordset.
				m_recResultRecordSet.clearSelection();
				m_recResultRecordSet.find(0, nEntityID);
				m_recResultRecordSet.deleteSelectedRows();

				//Delete from tree and from heap.
				dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->takeTopLevelItem(dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->indexOfTopLevelItem(item));
				delete(item);
			}
			break;
		}
	case MainEntitySelectionController::TYPE_TREE:
		RemoveTreeItemsForTreeEntity();
		break;
	}

	//Signal complete changed.
	CompleteChanged();
}

void SimpleSelectionWizPage::on_RemoveAll_pushButton_clicked()
{

	m_recResultRecordSet.clear();

	switch(m_nSelectorType)
	{
	case MainEntitySelectionController::TYPE_TABLE:
	case MainEntitySelectionController::TYPE_CONTACTS:
		dynamic_cast<SimpleSelectionWizTree*>(m_pSelectedWidget)->clear();
		break;
	case MainEntitySelectionController::TYPE_TREE:
		dynamic_cast<UniversalTreeWidget*>(m_pSelectedWidget)->RefreshDisplay();
		break;
	}

	
}

void SimpleSelectionWizPage::onItemSelectionChanged()
{
	if (dynamic_cast<QTreeWidget*>(m_pSelectedWidget)->selectedItems().isEmpty())
		ui.RemoveSelected_pushButton->setEnabled(false);
	else
		ui.RemoveSelected_pushButton->setEnabled(true);
}
