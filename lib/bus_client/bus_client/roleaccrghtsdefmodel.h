#ifndef ROLEACCRGHTSDEFMODEL_H
#define ROLEACCRGHTSDEFMODEL_H

#include "common/common/status.h"
#include "gui_core/gui_core/dbrecordsetlistmodel.h"

/*!
\class  RoleAccRghtsDefModel
\ingroup GUICore/ModelClasses
\brief  Role Access rights definition list model. 

Model class for Access rights manipulation on role definition widget.
*/
class RoleAccRghtsDefModel : public DbRecordSetListModel
{
public:
    RoleAccRghtsDefModel(QObject *parent = NULL);
    ~RoleAccRghtsDefModel();

	void LoadModel(Status &pStatus, int Source, int SourceID);

	bool setHeaderData (int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole);
	Qt::ItemFlags flags(const QModelIndex &index) const;

	bool setData ( const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
	Qt::DropActions supportedDropActions() const;

protected:
	void SetupModelData(DbRecordSetItem *Parent = NULL);
};

#endif // ROLEACCRGHTSDEFMODEL_H

