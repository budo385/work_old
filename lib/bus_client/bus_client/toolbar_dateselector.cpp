#include "toolbar_dateselector.h"



ToolBar_DateSelector::ToolBar_DateSelector(QWidget *parent)
    : QFrame(parent)
{
	ui.setupUi(this);

	//define fields:
	m_Values.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_DATE_SELECTOR));

	m_Values.addRow();

	m_pData = new GuiDataManipulator(&m_Values);
	//B.T.: TOFIX
	m_GuiFieldManagerMain =new GuiFieldManager(m_pData);//,DbSqlTableView::MVIEW_DATE_SELECTOR,this);
	m_GuiFieldManagerMain->RegisterField("DATE",ui.dateDate);

	QList<QRadioButton*> lstRadioButons;
	lstRadioButons << ui.btnCurrentDate << ui.btnPerDate;
	QList<QVariant> lstValues;
	lstValues << 0 << 1;
	m_GuiFieldManagerMain->RegisterField("TYPE",new GuiField_RadioButton("TYPE",lstRadioButons,lstValues,&m_Values,&DbSqlTableView::getView(DbSqlTableView::MVIEW_DATE_SELECTOR)));

	Initialize();
}



ToolBar_DateSelector::~ToolBar_DateSelector()
{
	delete m_GuiFieldManagerMain;
	delete m_pData;

}

//reset 
void ToolBar_DateSelector::Initialize()
{
	m_Values.setData(0,0,0);
	m_Values.setData(0,1,QDate::currentDate());
	m_GuiFieldManagerMain->RefreshDisplay();
	//ui.btnCurrentDate->setEnabled(true);
	//ui.btnPerDate->setEnabled(false);
	ui.dateDate->setEnabled(false);
}

bool ToolBar_DateSelector::ValidateUserInput(QWidget *pWidget,int nRow, int nCol, QVariant& value)
{
	//if different, signal
	if(pWidget==ui.dateDate && value.toDate()!= m_Values.getDataRef(0,1).toDate())
	{
		emit DateChanged(value.toDate());
	}
	
	//toggle date disable:
	ui.dateDate->setEnabled(ui.btnPerDate->isChecked());

	return true;
}