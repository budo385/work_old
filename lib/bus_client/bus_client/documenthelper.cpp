#include "documenthelper.h"
#include <QFile>
#include <QtWidgets/QFileDialog>
#include <QFileInfo>
#include <QInputDialog>
#include <QLineEdit>
#include "gui_core/gui_core/richeditwidget.h"
#include "common/common/zipcompression.h"
#include <QtWidgets/QMessageBox>
#include <QDesktopServices>
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/datahelper.h"
#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "bus_core/bus_core/globalconstants.h"
#include "trans/trans/config_trans.h"
#include "gui_core/gui_core/picturehelper.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "bus_client/bus_client/emailhelper.h"
#include "dlg_storagelocation.h"
#include "gui_core/gui_core/biginputdialog.h"
#include "bus_core/bus_core/commgridviewhelper.h"

#ifdef WINCE
	//For calling shellexecuteex.
	#include "windows.h"
	#include "shellapi.h"
#endif


static QFileSystemWatcher s_DocumentWatcher;		//global file system watcher (used for document manager)
static DbRecordSet s_lstChangedDocument;
static DbRecordSet s_lstContactsDef; 
static DbRecordSet s_lstProjectsDef; 
static DbRecordSet s_rowQCWContactDef;				//default QCW list (must have at least 1 row


#define SOKRATES_TEMP_FOLDER_FOR_CHECK_OUT_DOCS "Temp_Docs"
#define SOKRATES_AUTOINCREMENT_TEMP_FOLDER_SUFFIX "Temp"

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "bus_client/bus_client/clientdownloadmanager.h"
extern ClientDownloadManager *g_DownloadManager;

extern QString g_strLastDir_FileOpen;


//false if failed
bool DocumentHelper::SaveFileContent(QByteArray byteFileContent,QString strFilePath,bool bContentCompressed, bool bSetReadOnly)
{
	QFileInfo infoTarget(strFilePath);
	QString strBaseName=infoTarget.fileName();


	QFile file(strFilePath);
	if(!file.open(QIODevice::WriteOnly))
	{
		QString strTitle=QObject::tr("The File ")+strFilePath+QObject::tr(" can not be written!");
		return false;
	}

	if (bContentCompressed)
	{
		byteFileContent=qUncompress(byteFileContent); //max compression
	}

	int nErr=file.write(byteFileContent);
	if (nErr==-1)
	{
		QMessageBox::warning(NULL,QObject::tr("Warning"),QObject::tr("File can not be written!"));
		file.close();
		return false;
	}

	if (bSetReadOnly)
	{
		file.setPermissions(QFile::ReadOwner | QFile::ExeOwner | QFile::ReadUser | QFile::ExeUser | QFile::ReadGroup | QFile::ExeGroup | QFile::ReadOther | QFile::ExeOther);
	}

	file.close();
	return true;
}

//empty string if failed or path (same as strFilePath or new if user is asked for location)
QString DocumentHelper::LoadFileContent(QByteArray &byteFileContent,QString strFilePath,bool bContentCompressed,bool bAskForLocation, int *nSizeUnCompressed)
{
	if(strFilePath.isEmpty() || bAskForLocation)
	{
		QString strStartDir=QCoreApplication::applicationDirPath();
		//if ask for location & path valid, use path:
		if (bAskForLocation)
		{
			QFileInfo infoTarget(strFilePath);
			if (infoTarget.exists())
			{
				strStartDir=strFilePath;
			}
		}

		QString strFilter="*.*";

		//open file dialog:
		if (g_strLastDir_FileOpen.isEmpty())
			g_strLastDir_FileOpen=strStartDir;
		else
			strStartDir=g_strLastDir_FileOpen;

		strFilePath = QFileDialog::getOpenFileName(
			NULL,
			QObject::tr("Load Document"),
			g_strLastDir_FileOpen,
			strFilter);

		if (strFilePath.isEmpty())
			return "";
		QFileInfo fileInfo(strFilePath);
		g_strLastDir_FileOpen=fileInfo.absolutePath();


	}
	//QFileInfo infoTarget(strFilePath);
	//QString strBaseName=infoTarget.fileName();

	QFile file(strFilePath);
	if(!file.open(QIODevice::ReadOnly))
	{
		QMessageBox::warning(NULL,QObject::tr("Warning"),QObject::tr("File: ")+ strFilePath+ QObject::tr(" was not found!"));
		return "";
	}

	byteFileContent=file.readAll();
	file.close();

	if (nSizeUnCompressed!=NULL)
		*nSizeUnCompressed=byteFileContent.size();

	if (bContentCompressed)
	{
		byteFileContent=qCompress(byteFileContent,2); //min compression
	}

	return strFilePath;
}




QString DocumentHelper::GetDefaultDocumentDirectory()
{

	QString strDir;
	//issue 1619: if portable return app dir
	if (DataHelper::IsPortableVersion())
	{
		//check if exists, if so return, else create
		QString strMyDir=QCoreApplication::applicationDirPath();
		QDir dir(strMyDir);
		if (!dir.exists(SOKRATES_TEMP_FOLDER_FOR_CHECK_OUT_DOCS))
		{
			if(dir.mkdir(SOKRATES_TEMP_FOLDER_FOR_CHECK_OUT_DOCS))
				strDir= strMyDir+"/"+SOKRATES_TEMP_FOLDER_FOR_CHECK_OUT_DOCS;
			else
				strDir= QCoreApplication::applicationDirPath();
		}
		else
			strDir= strMyDir+"/"+SOKRATES_TEMP_FOLDER_FOR_CHECK_OUT_DOCS; 
		return strDir;
	}


	strDir=g_pSettings->GetPersonSetting(PERSON_DEFAULT_DOCUMENT_DIR).toString();
	if (strDir.isEmpty())
	{
		strDir=g_pSettings->GetApplicationOption(APP_DEFAULT_DOCUMENT_DIR).toString();
		if (!strDir.isEmpty())
			strDir+="_"+g_pClientManager->GetConnectionName();
	}

	if (!strDir.isEmpty()) //return if found && exists, else try default:
	{
		QDir dir(strDir);
		if (dir.exists())
			return strDir;
	}


	//ISSUE BY M: 1760: add sufix conn name!!!

	//if empty then try to create hardcoded:
	//check if exists, if so return, else create
	QString strMyDir=DataHelper::GetMyDocumentsDir();
	strDir=QString(SOKRATES_TEMP_FOLDER_FOR_CHECK_OUT_DOCS)+"_"+g_pClientManager->GetConnectionName();

	QDir dir(strMyDir);
	if (!dir.exists(strDir))
	{
		if(dir.mkdir(strDir))
			strDir= strMyDir+"/"+strDir;
		else
			strDir= QDir::tempPath(); //propose this one:
	}
	else
		strDir= strMyDir+"/"+strDir; 


	g_pSettings->SetPersonSetting(PERSON_DEFAULT_DOCUMENT_DIR,strDir);

	return strDir;
}


QString DocumentHelper::GetCheckOutDocumentSavePath(QString strFileName,bool bUseTempFolder)
{

	if (bUseTempFolder)
	{
		return GetDefaultDocumentDirectory()+"/"+strFileName;
	}
	else
	{
		QString strStartDir;
		QString strFilter="*.*";

		//open file dialog:
		if (g_strLastDir_FileOpen.isEmpty())
			g_strLastDir_FileOpen=strStartDir;
		else
			strStartDir=g_strLastDir_FileOpen;
		
		strStartDir+=+"/"+strFileName;
		QString strPath=QFileDialog::getSaveFileName(
			NULL,
			tr("Save As"),
			strStartDir,
			strFilter);
		if (!strPath.isEmpty())
		{
			QFileInfo fileInfo(strPath);
			g_strLastDir_FileOpen=fileInfo.absolutePath();
		}
		return strPath;
	}

}
QString DocumentHelper::GetCheckOutDocumentDirectoryPath(bool bUseTempFolder)
{
	if (bUseTempFolder)
	{
		return GetDefaultDocumentDirectory();
	}
	else
	{
		QString strStartDir=GetDefaultDocumentDirectory();
		QString strFilter="*.*";

		//open file dialog:
		if (g_strLastDir_FileOpen.isEmpty())
			g_strLastDir_FileOpen=strStartDir;
		else
			strStartDir=g_strLastDir_FileOpen;
		QString strPath=QFileDialog::getExistingDirectory(
			NULL,
			tr("Save As"),
			strStartDir);
		if (!strPath.isEmpty())
		{
			g_strLastDir_FileOpen=strPath;
		}
		return strPath;
	}
}


//engine san: Temp0001 etc...
//empty string if failed
QString DocumentHelper::GetTempDirectoryForAutomaticStorage()
{
	QString strDirPath=GetDefaultDocumentDirectory();
	int nDirNameSize=QString(SOKRATES_AUTOINCREMENT_TEMP_FOLDER_SUFFIX).size()+4;

	int nMaxID=0;
	QDir dir(strDirPath);
	if (!dir.exists())
		return "";

	QString strNewDirectory=QString(SOKRATES_AUTOINCREMENT_TEMP_FOLDER_SUFFIX)+"0001";
	int nCount=1;

	//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
	QFileInfo info(strDirPath+"/"+strNewDirectory);
	QFileInfoList	lstFiles=dir.entryInfoList();
	while(lstFiles.contains(info))
	{
		nCount++;
		QString strDir=GetNewTempDirName(nCount);
		if (strDir.isEmpty()) return "";
		strNewDirectory=SOKRATES_AUTOINCREMENT_TEMP_FOLDER_SUFFIX+strDir;
		info.setFile(strDirPath+"/"+strNewDirectory);
	}


	if(dir.mkdir(strNewDirectory))
		return strDirPath+"/"+strNewDirectory;
	else
		return "";
}






QVariant DocumentHelper::GetApplicationFromExtension(QString strFileName,bool bUseAsExtension)
{
	QVariant valAppID(QVariant::Int);

	QString strExtension=strFileName.toUpper();
	if (!bUseAsExtension)
	{
		QFileInfo fileInfo(strFileName);
		strExtension=fileInfo.suffix().toUpper();	
		if (strExtension.isEmpty())
			return valAppID;
	}


	DbRecordSet lstApps;
	GetDocApplications(lstApps);



	int nExtIdx=lstApps.getColumnIdx("BDMA_EXTENSIONS");
	int nSizeApp=lstApps.getRowCount();
	for (int k=0;k<nSizeApp;++k)
	{
		if (lstApps.getDataRef(k,nExtIdx).toString().toUpper().indexOf(strExtension)>=0)
		{
			return lstApps.getDataRef(k,"BDMA_ID");
		}
	}

	return valAppID; 
}



//load from cache and server, store in cache
void DocumentHelper::GetDocApplications(DbRecordSet &lstData,bool bReloadFromServer)
{
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS));

	if(g_pClientManager->GetPersonID()==0) return;

	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_BUS_DM_APPLICATIONS);
		if (plstCacheData)
		{
			lstData=*plstCacheData;
			return;
		}
	}


	Status err;
	_SERVER_CALL(ClientSimpleORM->Read(err,BUS_DM_APPLICATIONS,lstData))
	if (!err.IsOK())
	{
		QMessageBox::critical(NULL,(QString)tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//Store in Cache:
	g_ClientCache.SetCache(ENTITY_BUS_DM_APPLICATIONS,lstData);
}

//load from cache and server, store in cache
void DocumentHelper::GetDocUserPaths(DbRecordSet &lstData,bool bReloadFromServer)
{
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_USER_PATHS));

	if(g_pClientManager->GetPersonID()==0) return;


	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_BUS_DM_USER_PATHS);
		if (plstCacheData)
		{
			lstData=*plstCacheData;
			return;
		}
	}

	Status err;
	_SERVER_CALL(BusDocuments->ReadUserPaths(err,g_pClientManager->GetPersonID(),lstData))
		if (!err.IsOK())
		{
			QMessageBox::critical(NULL,(QString)tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//Store in Cache:
		g_ClientCache.SetCache(ENTITY_BUS_DM_USER_PATHS,lstData);

}

//load from cache and server, store in cache
void DocumentHelper::GetDocTemplates(DbRecordSet &lstData,bool bReloadFromServer)
{
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));

	if(g_pClientManager->GetPersonID()==0) return;

	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_BUS_DM_TEMPLATES);
		if (plstCacheData)
		{
			lstData=*plstCacheData;
			return;
		}
	}


	//SERVER:
	Status err;
	QString strWhere =" WHERE BDMD_TEMPLATE_FLAG=1 "; 
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strWhere);
	_SERVER_CALL(ClientSimpleORM->Read(err,BUS_DM_DOCUMENTS,lstData,strWhere,DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY))
		if (!err.IsOK())
		{
			QMessageBox::critical(NULL,(QString)tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//Store in Cache:
		g_ClientCache.SetCache(ENTITY_BUS_DM_TEMPLATES,lstData);
}




//loads all user paths
//test if application exists
//if not open find dialog-> save new path by that application....
//proceeded with open if success return true, else false, and if user canceled false, if bo server fails, false
bool DocumentHelper::OpenDocumentInExternalApp_Shell(int nApplicationID,QString strDocPath,QString strParameters,QString strAppParameters)
{

#ifdef WINCE
	if (!strDocPath.isEmpty())
	{
		strDocPath=strDocPath.trimmed();
		//put in quotes for windows
		strDocPath=	QDir::toNativeSeparators(strDocPath); 
		strDocPath.prepend("\"");
		strDocPath.append("\"");
	}
	else
		return false;
	return StartWINCEProcess(strDocPath);
#endif

	//MB 736: open doc as application in system...
	if (nApplicationID==0)
	{
		QString strApp="cmd /C";
#ifdef _WIN32 //put in quotes for windows
		//strApp.prepend("\"");
		//strApp.append("\"");
#endif

		QUrl urlOpen=QUrl::fromLocalFile(strDocPath);
		//convert to natural born separators (damn that Bill)
		if (!strDocPath.isEmpty())
		{
			strDocPath=strDocPath.trimmed();
			strDocPath=	QDir::toNativeSeparators(strDocPath); 
			strDocPath.prepend("\"");
			strDocPath.append("\"");
			strApp=strApp+" "+strDocPath;
		}

		//QDesktopServices::openUrl(strDocPath);

		//bool bOK=QProcess::startDetached(strApp);	//open another application as detached process
		bool bOKOpen=QDesktopServices::openUrl(urlOpen);
		if (!bOKOpen)
		{
			bool bOK=QProcess::startDetached(strApp);	//open another application as detached process
			if (!bOK)
			{
				QMessageBox::warning(NULL,tr("Warning"),tr("Application is not set for this document. Operation aborted!"));
				return false;
			}
			else
				return true;
		}
		else
			return true;
	}

	//resolve application path:
	QString strAppName=GetApplicationName(nApplicationID);
	QString strApp=GetApplicationPath(nApplicationID,strAppName);
	if (strApp.isEmpty())
		return false;


	//convert to natural born separators (damn that Bill)
	if (!strDocPath.isEmpty() && strDocPath.indexOf("http://")!=0 && strDocPath.indexOf("ftp://")!=0)
	{
		strDocPath=strDocPath.trimmed();
		strDocPath=	QDir::toNativeSeparators(strDocPath); 
	}

	//if empty load:
	if (strAppParameters.isEmpty())
	{
		DbRecordSet lstApps;
		GetDocApplications(lstApps);
		int nRow=lstApps.find(0,nApplicationID,true);
		if (nRow!=-1)
		{
			strAppParameters=lstApps.getDataRef(nRow,"BDMA_OPEN_PARAMETER").toString();
			strAppParameters=strAppParameters.trimmed();
		}
	}



	//set args:
	QStringList lstArgs;


	//APP parameters:
	if (!strAppParameters.isEmpty())
	{
		lstArgs<<strAppParameters;
	}

	//DOCUMENT it self:
	if (!strDocPath.isEmpty())
	{
		lstArgs<<strDocPath;	
	}


	//DOC parameters:
	if (!strParameters.isEmpty())
	{
		strParameters=strParameters.trimmed();
		lstArgs<<strParameters;
	}

	QFileInfo info(strApp);
	QProcess proces;
	proces.setWorkingDirectory(info.absolutePath());


#ifdef _WIN32 //put in quotes for windows
	strApp.prepend("\"");
	strApp.append("\"");
#endif

	return proces.startDetached(strApp,lstArgs);	//open another application as detached process

}

bool DocumentHelper::OpenDirectoryInExternalApp_Shell(QString strDocPath)
{
	QFileInfo infoFile(strDocPath);
	strDocPath=infoFile.absolutePath();

	QString strApp="cmd /C";

	QUrl urlOpen=QUrl::fromLocalFile(strDocPath);
	//convert to natural born separators (damn that Bill)
	if (!strDocPath.isEmpty())
	{
		strDocPath=strDocPath.trimmed();
		strDocPath=	QDir::toNativeSeparators(strDocPath); 
		strDocPath.prepend("\"");
		strDocPath.append("\"");
		strApp=strApp+" "+strDocPath;
	}

	bool bOKOpen=QDesktopServices::openUrl(urlOpen);
	if (!bOKOpen)
	{
		bool bOK=QProcess::startDetached(strApp);	//open another application as detached process
		if (!bOK)
		{
			return false;
		}
		else
			return true;
	}
	else
		return true;

}

QString DocumentHelper::GetApplicationName(int nApplicationID)
{
	QString strAppName;
	DbRecordSet lstApps;
	GetDocApplications(lstApps);
	int nRow=lstApps.find(0,nApplicationID,true);
	if (nRow!=-1)
	{
		strAppName=lstApps.getDataRef(nRow,"BDMA_NAME").toString();
	}

	return strAppName;
}



QString DocumentHelper::GetApplicationPath(int nApplicationID, QString strApplicationName)
{


	//------------------------------TRY USER PATHS---------------------------------------
	//read user paths:
	DbRecordSet lstPathsUser;
	GetDocUserPaths(lstPathsUser);

	//lstPaths.Dump();
	QString strPath;

	//find app. id:
	lstPathsUser.find(lstPathsUser.getColumnIdx("BDMU_APPLICATION_ID"),nApplicationID);
	lstPathsUser.deleteUnSelectedRows();
	lstPathsUser.sort(lstPathsUser.getColumnIdx("BDMU_PRIORITY"),1); //desceding

	int nSize2=lstPathsUser.getRowCount();

	//issue 1632: if portable use application exe drive 1st and test:
	if (DataHelper::IsPortableVersion()) 
	{
		for (int i=0;i<nSize2;++i)
		{
			QString strAppPath=DataHelper::ReplaceCurrentDriveInPath(lstPathsUser.getDataRef(i,"BDMU_APP_PATH").toString());
			if(QFile::exists(strAppPath))
			{
				strPath=strAppPath;
				break;
			}
		}
		if (!strPath.isEmpty())
		{
			return strPath;
		}
	}

	//if normal: try paths as they are:
	for (int i=0;i<nSize2;++i)
	{
		QString strAppPath=lstPathsUser.getDataRef(i,"BDMU_APP_PATH").toString();
		if (QFile::exists(strAppPath))
		{
			strPath=strAppPath;
			break;
		}
	}
	if (!strPath.isEmpty())
	{
		return strPath;
	}



	//------------------------------TRY OTHER USER PATHS---------------------------------------
	DbRecordSet lstData;
	DbRecordSet lstPaths;
	Status err;
	_SERVER_CALL(BusDocuments->ReadUserPathsForApplication(err,nApplicationID,lstPaths))
		if (!err.IsOK())
		{
			QMessageBox::critical(NULL,(QString)tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return "";
		}

		nSize2=lstPaths.getRowCount();

		//issue 1632: if portable use application exe drive 1st and test:
		if (DataHelper::IsPortableVersion()) 
		{
			for (int i=0;i<nSize2;++i)
			{
				QString strAppPath=DataHelper::ReplaceCurrentDriveInPath(lstPaths.getDataRef(i,"BDMU_APP_PATH").toString());
				if(QFile::exists(strAppPath))
				{
					strPath=strAppPath;
					break;
				}
			}
			if (!strPath.isEmpty())
			{
				SaveUserApplicationPath(nApplicationID,strPath); //save as this is new
				return strPath;
			}
		}

		//normal mode:
		for (int i=0;i<nSize2;++i)
		{
			QString strAppPath=lstPaths.getDataRef(i,"BDMU_APP_PATH").toString();
			if (QFile::exists(strAppPath))
			{
				strPath=strAppPath;
				break;
			}
		}
		if (!strPath.isEmpty())
		{
			SaveUserApplicationPath(nApplicationID,strPath); //save as this is new
			return strPath;
		}




		//------------------------------TRY REGISTRY---------------------------------------
		strPath=DataHelper::Registry_GetApplicationPathFromRegistry(strApplicationName);
		if (!strPath.isEmpty())
		{
			SaveUserApplicationPath(nApplicationID,strPath); //save as this is new
			return strPath;
		}

		//------------------------------TRY DIFFERENT DRIVES---------------------------------------
		//normal mode: try all...
		lstPaths.merge(lstPathsUser);
		strPath.clear();
		//issue 1619: find application path in way to iterate through drive letters
		QString chSeparator=QDir::separator(); //native
		QFileInfoList lstDrives=QDir::drives();
		int nSizeDrives=lstDrives.count();
		bool bFound=false;
		for (int i=0;i<nSize2;++i)
		{
			QString strAppPath=QDir::toNativeSeparators(lstPaths.getDataRef(i,"BDMU_APP_PATH").toString()).trimmed();
			QString strDrive;
			int nPos=strAppPath.indexOf(chSeparator);
			if (nPos>0)
			{
				for(int k=0;k<nSizeDrives;k++)
				{
					strAppPath=QDir::toNativeSeparators(lstDrives.at(k).absoluteFilePath()+strAppPath.mid(nPos+1));
					if(QFile::exists(strAppPath))
					{
						strPath=strAppPath;
						bFound=true;
						break;
					}
				}
			}
			if (bFound)
				break;
		}

		if (bFound && !strPath.isEmpty())
		{
			SaveUserApplicationPath(nApplicationID,strPath); //save as this is new
			return strPath;
		}


		//------------------------------TRY DEFAULT HARDCODED: issue 1490---------------------------------------
		if (strApplicationName.toLower()=="internet explorer" || strApplicationName.toLower().left(6)=="google")
		{
			strPath="C:/Program Files/Internet Explorer/iexplore.exe";
			if(QFile::exists(strPath))
			{
				SaveUserApplicationPath(nApplicationID,strPath); //save as this is new
				return strPath;
			}
			strPath="C:/Programme/Internet Explorer/iexplore.exe";
			if(QFile::exists(strPath))
			{
				SaveUserApplicationPath(nApplicationID,strPath); //save as this is new
				return strPath;
			}
		}


		//------------------------------ASK USER---------------------------------------
		//							    issue:938
		//------------------------------ASK USER---------------------------------------
		DbRecordSet lstApps;
		GetDocApplications(lstApps);
		int nRow=lstApps.find(0,nApplicationID,true);
		int nDocAppId=0,nDocViewerID=0;
		if (nRow!=-1)
		{
			nDocAppId=lstApps.getDataRef(nRow,"BDMA_INSTALL_APP_TEMPL_ID").toInt();
			nDocViewerID=lstApps.getDataRef(nRow,"BDMA_INSTALL_VIEWER_TEMPL_ID").toInt();
		}
		else
			return "";

		QString strTitle=tr("Please Search Your Installation of ") + strApplicationName;

		//construct messagebox:

		/*
		if (nDocViewerID!=0 || nDocAppId !=0)
		{
			QMessageBox box;
			QPushButton *btn1=box.addButton(tr("Find Application Path Manually"),QMessageBox::AcceptRole);
			QPushButton *btn2=box.addButton(tr("Download/Install Application"),QMessageBox::AcceptRole);
			QPushButton *btn3=box.addButton(tr("Download/Install Viewer"),QMessageBox::AcceptRole);
			QPushButton *btn4=box.addButton(tr("Cancel"),QMessageBox::RejectRole);
			if (nDocAppId<=0)
				btn2->setEnabled(false);
			if (nDocViewerID<=0)
				btn3->setEnabled(false);

			box.setDefaultButton(btn1);
			box.setInformativeText(strTitle);
			box.setWindowTitle(tr("Search Application Installation"));
			int nResult=box.exec();
			if (nResult==1)
			{
				ApplicationInstallFinder(nDocAppId);
				return "";
			}
			else if (nResult==2)
			{
				ApplicationInstallFinder(nDocAppId);
				return "";
			}
			else if (nResult!=0)
			{
				return ""; //abort
			}
		}
		*/


		//open file open dialog
		QString strStartDir=QDir::currentPath(); 
		QString strFilter="*.*";

		//open file dialog:
		if (g_strLastDir_FileOpen.isEmpty())
			g_strLastDir_FileOpen=strStartDir;
		else
			strStartDir=g_strLastDir_FileOpen;
		strPath = QFileDialog::getOpenFileName(
			NULL,
			strTitle,
			strStartDir,
			strFilter);

		if (!strPath.isEmpty())
		{
			QFileInfo fileInfo(strPath);
			g_strLastDir_FileOpen=fileInfo.absolutePath();

			SaveUserApplicationPath(nApplicationID,strPath); //save as this is new
			return strPath;
		}

		return strPath;

}

bool DocumentHelper::OpenDocumentFromAttachment(QByteArray &contentAttachCompressed, QString strFileName)
{
	//save file in temp
	QString strTempDirectory=GetTempDirectoryForAutomaticStorage();
	if (strTempDirectory.isEmpty())
	{
		QMessageBox::critical(NULL,tr("Error"),tr("Failed to open attachment:")+strFileName);
		return false;
	}
	QString strFilePath=strTempDirectory+"/"+strFileName;
	if (!SaveFileContent(contentAttachCompressed,strFilePath,true))
		return false;
		
	//if zip?
	QString strPathUnzipped=strFilePath;
	QFileInfo info(strFileName); //if already zip..skip
	if (info.completeSuffix().toLower()=="zip")
	{
		//unpack if zip
		QStringList lstOut;
		if(!ZipCompression::Unzip(strFilePath,strTempDirectory,lstOut,true))
		{
			QMessageBox::critical(NULL,tr("Error"),tr("Failed to unzip attachment:")+strFileName);
			QFile::remove(strFilePath);
			return false;
		}

		if (lstOut.size()==0)
		{
			QMessageBox::critical(NULL,tr("Error"),tr("Failed to unzip attachment:")+strFileName);
			QFile::remove(strFilePath);
			return false;
		}
		QFile::remove(strFilePath);
		strPathUnzipped=lstOut.at(0);
	}


	QVariant var=GetApplicationFromExtension(strPathUnzipped);
	return OpenDocumentInExternalApp_Shell(var.toInt(),strPathUnzipped,"","");
}


void DocumentHelper::SaveUserApplicationPath(int nApplicationID, QString strApplicationPath)
{

	//register it in user paths:
	DbRecordSet lstWriteUserPath;
	lstWriteUserPath.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_USER_PATHS));
	lstWriteUserPath.addRow();
	lstWriteUserPath.setData(0,"BDMU_OWNER_ID",g_pClientManager->GetPersonID());
	lstWriteUserPath.setData(0,"BDMU_APPLICATION_ID",nApplicationID);
	lstWriteUserPath.setData(0,"BDMU_APP_PATH",strApplicationPath.replace("\\","/"));
	Status err;
	QString pLockResourceID;
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	DbRecordSet lstForDelete;
	_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_USER_PATHS,lstWriteUserPath, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//update cache
		DbRecordSet lstPaths;
		GetDocUserPaths(lstPaths);
		lstPaths.merge(lstWriteUserPath);
		g_ClientCache.SetCache(ENTITY_BUS_DM_USER_PATHS,lstPaths);
}

//returns icon or empty if not fond
QPixmap DocumentHelper::GetApplicationIcon(int nApplicationID)
{
	DbRecordSet lstApps;
	Status err;

	GetDocApplications(lstApps);
	int nRow=lstApps.find(0,nApplicationID,true);

	if (nRow!=-1)
	{
		QByteArray bytePicture=lstApps.getDataRef(nRow,"BDMA_ICON").toByteArray();
		if (bytePicture.size()>0)
		{
			QPixmap pixMap;
			pixMap.loadFromData(bytePicture);
			return pixMap;
		}
		else
			return QPixmap();
	}

	return QPixmap();
}

//if empty: error
QString DocumentHelper::GetNewTempDirName(int nCount)
{
	//comprise d'name, num part must be 4 digit:
	QString strNumPart=QVariant(nCount).toString();
	if (strNumPart.size()==0)
		return "";
	if (strNumPart.size()==1)
		strNumPart="000"+strNumPart;
	if (strNumPart.size()==2)
		strNumPart="00"+strNumPart;
	if (strNumPart.size()==3)
		strNumPart="0"+strNumPart;
	if (strNumPart.size()>4)
		return "";

	return strNumPart;
}


QHash<int, QPixmap> DocumentHelper::GetDocumentTypeIconHash()
{
	QHash<int, QPixmap> hshDocIcons;

	QHash<int, QString> hshDocIconNames = CommGridViewHelper::GetDocumentTypeIconNames();
	
	QHashIterator<int, QString> i(hshDocIconNames);
	while (i.hasNext()) 
	{
		i.next();
		hshDocIcons.insert(i.key(), QPixmap(i.value()));
	}
	
	return hshDocIcons;
}

QPixmap DocumentHelper::GetDocumentTypeIcon(int nDocumentTypeID)
{
	QPixmap docTypeIcon;

	QHash<int, QPixmap> hshDocIcons = GetDocumentTypeIconHash();
	if (hshDocIcons.contains(nDocumentTypeID))
		docTypeIcon = hshDocIcons.value(nDocumentTypeID);
	else
		Q_ASSERT(false);

	return docTypeIcon;
}

//strPath if empty can contain actual path...
bool DocumentHelper::CreateDocumentRevisionFromPath(DbRecordSet &rowRevision,QString &strPath,bool bAskForPathConfirmation,int *nSizeUnCompressed, bool bSkipLoadContent)
{
	//issue 1619: find application path in way to iterate through drive letters
	QFileInfoList lstDrives=QDir::drives();
	QChar chSeparator=QDir::separator();
	int nSizeDrives=lstDrives.count();
	bool bFound=false;
	QString strFileName=strPath;

	if (!bAskForPathConfirmation && !strPath.isEmpty() )
		if (!QFile::exists(strPath))
		{
			QString strAppPath=QDir::toNativeSeparators(strPath).trimmed();
			QString strDrive;
			int nPos=strAppPath.indexOf(chSeparator);
			if (nPos>0)
			{
				for(int k=0;k<nSizeDrives;k++)
				{
					strAppPath=QDir::toNativeSeparators(lstDrives.at(k).absoluteFilePath()+strAppPath.mid(nPos+1));
					if(QFile::exists(strAppPath))
					{
						strFileName=strAppPath;
						bFound=true;
						break;
					}
				}
			}
			if (!bFound)
				return false;
		}

		//ask for doc:
		bool bCompressed=false;
		QByteArray content;
		if (!bSkipLoadContent)
		{
			//bCompressed=true; why bother to compress!?
			strFileName=LoadFileContent(content,strFileName,bCompressed,bAskForPathConfirmation,nSizeUnCompressed);
		}
		else
		{
			if (bAskForPathConfirmation || strFileName.isEmpty())
			{
				QString strFilePath=strFileName;
				QString strStartDir=QCoreApplication::applicationDirPath();
				//if ask for location & path valid, use path:
				if (bAskForPathConfirmation)
				{
					QFileInfo infoTarget(strFilePath);
					if (infoTarget.exists())
					{
						strStartDir=strFilePath;
					}
				}

				QString strFilter="*.*";

				//open file dialog:
				if (g_strLastDir_FileOpen.isEmpty())
					g_strLastDir_FileOpen=strStartDir;
				else
					strStartDir=g_strLastDir_FileOpen;

				strFilePath = QFileDialog::getOpenFileName(
					NULL,
					QObject::tr("Load Document"),
					g_strLastDir_FileOpen,
					strFilter);

				if (strFilePath.isEmpty())
					return false;
				QFileInfo fileInfo(strFilePath);
				g_strLastDir_FileOpen=fileInfo.absolutePath();
				strFileName=strFilePath;
			}
		}

		if (strFileName.isEmpty()) return false;
		//if not check out, errr->only if edit

		QFileInfo fileInfo(strFileName);
		*nSizeUnCompressed=fileInfo.size();

		rowRevision.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_REVISIONS));
		rowRevision.addRow();
		rowRevision.setData(0,"BDMR_CHECK_IN_USER_ID",g_pClientManager->GetPersonID());
		if (!bSkipLoadContent)
			rowRevision.setData(0,"BDMR_CONTENT",content); //skip
		rowRevision.setData(0,"BDMR_NAME",fileInfo.fileName());
		rowRevision.setData(0,"BDMR_DAT_CHECK_IN",QDateTime::currentDateTime());
		rowRevision.setData(0,"BDMR_SIZE",*nSizeUnCompressed);
		int tmp_int=(int)(bCompressed);
		rowRevision.setData(0,"BDMR_IS_COMPRESSED",tmp_int);

		//_DUMP(rowRevision)

		strPath=strFileName; //return real path (can be new)
		return true;
}



bool DocumentHelper::PackDocuments(DbRecordSet &lstDocuments,QStringList &lstZippedPaths, bool bSkipZip)
{
	//test example for 1 internet file:

	QString strTempDirectory=DocumentHelper::GetTempDirectoryForAutomaticStorage();
	if (strTempDirectory.isEmpty())
	{
		QMessageBox::critical(NULL,tr("Error"),tr("Failed to Pack & Send Documents"));
		return false;
	}
	QStringList strFilePaths;

	int nSize=lstDocuments.getRowCount();
	for(int i=0;i<nSize;++i)
	{

		//Pavle: for croatia chars:
		//QByteArray fileName=QFile::encodeName(lstDocuments.getDataRef(i,"BDMD_DOC_PATH").toString());
		//QString strDocPath=QFile::decodeName(fileName);

		//QByteArray fileName=lstDocuments.getDataRef(i,"BDMD_DOC_PATH").toString();
		QString strDocPath=lstDocuments.getDataRef(i,"BDMD_DOC_PATH").toString();


		//QString strDocPath=lstDocuments.getDataRef(i,"BDMD_DOC_PATH").toString().toLocal8Bit(); //BT: issue by Pavle, our chars...
		QString strDocName=lstDocuments.getDataRef(i,"BDMD_NAME").toString();
		int nDocID=lstDocuments.getDataRef(i,"BDMD_ID").toInt();
		int nDocType=lstDocuments.getDataRef(i,"BDMD_DOC_TYPE").toInt();


		switch(nDocType)
		{
		case GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE:
			{
				QFileInfo info(strDocPath);
				if (!info.exists())
				{
					QString strTitle=tr("The Local File ")+strDocPath+tr(" is not visible on this computer and can not be opened!");
					QMessageBox::warning(NULL,tr("Warning"),strTitle);
					return false;
				}
				strFilePaths<<strDocPath;
			}
			break;
		case GlobalConstants::DOC_TYPE_INTERNET_FILE:
			{
				DbRecordSet rowRev,rowCheckOut;
				
				if(!TestDocumentTemplateRevisionsForMaximumSize(nDocID)) //test if revision can be fetched ho-ruk
					return false;

				QString strFile=CheckOutDocument(nDocID,strDocPath,true,rowRev,rowCheckOut);
				if (strFile.isEmpty())
				{
					QString strTitle=tr("The Document ")+strDocName+tr(" can not be check out!");
					QMessageBox::warning(NULL,tr("Warning"),strTitle);
					return false;
				}
				strFilePaths<<strFile;
			}
			break;
		case GlobalConstants::DOC_TYPE_URL:
			{
				//save BDMD_DOC_PATH
				QString strContent;
				QString strUrl=lstDocuments.getDataRef(i,"BDMD_DOC_PATH").toString();
				if (strUrl.indexOf("http")==0)
				{
					strContent="<head><meta http-equiv=\"refresh\" content=\"0; URL=";
					strContent+=strUrl+"\"";
					strContent+="></head><html><body><a href=";
					strContent+="\""+strUrl+"\">";
					strContent+=lstDocuments.getDataRef(i,"BDMD_DOC_PATH").toString()+"</a></body></html>";
				}
				else
				{
					strContent="<html>"+strUrl;
					strContent+="<html>";
				}
				QString strFile=strTempDirectory+"/"+DataHelper::EncodeFileName(strDocName)+".html";
				if(!DocumentHelper::SaveFileContent(strContent.toLocal8Bit(),strFile,false))
				{
					return false;
				}
				strFilePaths<<strFile;
			}
			break;
		case GlobalConstants::DOC_TYPE_PHYSICAL:
			{
				//save BDMD_LOCATION
				QString strContent="<html>";
				strContent+=tr("Name: ")+strDocName;
				strContent+="<br>";
				strContent+=tr("Location: ")+lstDocuments.getDataRef(i,"BDMD_LOCATION").toString();
				strContent+="<html>";
				QString strFile=strTempDirectory+"/"+DataHelper::EncodeFileName(strDocName)+".html";
				if(!DocumentHelper::SaveFileContent(strContent.toLocal8Bit(),strFile,false))
				{
					return false;
				}
				strFilePaths<<strFile;
			}
			break;
		case GlobalConstants::DOC_TYPE_NOTE:
			{
				//save BDMD_DESCRIPTION
				QString strContent;
				Status err;
				int nID=lstDocuments.getDataRef(i,"BDMD_ID").toInt();
				_SERVER_CALL(BusDocuments->ReadNoteDocument(err,nID,strContent));
				if (!err.IsOK())
					return false;


				QString strFile=strTempDirectory+"/"+DataHelper::EncodeFileName(strDocName)+".pdf";
				
				//issue 2491 12.12.2011 save to pdf and send
				//--------------------------------------------------------
				RichEditWidget rich;
				rich.SetEmbeddedPixMode();
				rich.SetHtml(strContent);
				rich.SaveToPdf(strFile);

				strFilePaths<<strFile;
			}
			break;
		}

	}

	if (bSkipZip) //issue 1674: no document is zipped until dragged on email attachemnt...brb...
	{
		lstZippedPaths=strFilePaths;
		return true;
	}

	ZipCompression Zipper;

	//all docs -> each pack and zip -> name+.zip -> all zips inside temp dierctory
	nSize=strFilePaths.size();
	for(int i=0;i<nSize;++i)
	{

		QFileInfo info(strFilePaths.at(i));
		if (!info.exists()) continue;
		QString strZipFile=strTempDirectory+"/"+info.baseName()+".zip";

		QStringList strLstSource;
		strLstSource<<strFilePaths.at(i);
		//strLstNames<<info.fileName();
		if(!ZipCompression::ZipFiles(strZipFile,strLstSource))
		{
			QString strTitle=tr("Failed to zip document ")+strLstSource.at(i);
			QMessageBox::warning(NULL,tr("Warning"),strTitle);
			return false;
		}

		lstZippedPaths<<strZipFile;

	}

	return true;

}



//source= template, target doc type= same or ask
bool DocumentHelper::CreateDocumentFromTemplate(DbRecordSet &rowTemplate,DbRecordSet &rowDocData,DbRecordSet &rowRevision,bool bAskForNewName,bool bNoFileApp)
{

	if (rowTemplate.getRowCount()==0)
		return false;

	int nDocTemplateID=rowTemplate.getDataRef(0,"BDMD_ID").toInt();
	int nDocTemplateTypeOriginal=rowTemplate.getDataRef(0,"BDMD_DOC_TYPE").toInt();
	//int nDocType=nDocTemplateTypeOriginal;

	//	Check if template or internet then ask what type:
	if (nDocTemplateTypeOriginal==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE || nDocTemplateTypeOriginal==GlobalConstants::DOC_TYPE_INTERNET_FILE)
	{
		if (bNoFileApp)
		{
			return CreateLocalDocumentFromTemplate(rowTemplate,rowDocData,true);
		}

		int nDefaultDocType=AskForDocumentType();
		//int nResult=QMessageBox::question(NULL,tr("Choose Document Storage Location"),tr("Choose Document Storage Location:"),tr("  Save Reference(s) to Local File(s)  "),tr("  Store File Centrally on Internet  "),tr(" Cancel "),0,2);
		if (nDefaultDocType==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
			return CreateLocalDocumentFromTemplate(rowTemplate,rowDocData);
		else if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
			return CreateInternetDocumentFromTemplate(rowTemplate,rowDocData,rowRevision,bAskForNewName);
		else
			return false; //exit
	}
	else //ordinary file/url based:
	{
		rowDocData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
		rowDocData.addRow();

		rowDocData.setData(0,"BDMD_NAME",rowTemplate.getDataRef(0,"BDMD_NAME"));
		rowDocData.setData(0,"BDMD_DOC_PATH",rowTemplate.getDataRef(0,"BDMD_DOC_PATH"));
		rowDocData.setData(0,"BDMD_DESCRIPTION",rowTemplate.getDataRef(0,"BDMD_DESCRIPTION"));
		rowDocData.setData(0,"BDMD_APPLICATION_ID",rowTemplate.getDataRef(0,"BDMD_APPLICATION_ID"));
		rowDocData.setData(0,"BDMD_TEMPLATE_ID",rowTemplate.getDataRef(0,"BDMD_ID"));
		rowDocData.setData(0,"CENT_CE_TYPE_ID",rowTemplate.getDataRef(0,"CENT_CE_TYPE_ID"));
		rowDocData.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
		rowDocData.setData(0,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
		rowDocData.setData(0,"CENT_OWNER_ID",g_pClientManager->GetPersonID());
		rowDocData.setData(0,"BDMD_DOC_TYPE",rowTemplate.getDataRef(0,"BDMD_DOC_TYPE"));
		rowDocData.setData(0,"BDMD_OPEN_PARAMETER",rowTemplate.getDataRef(0,"BDMD_OPEN_PARAMETER"));
		//rowDocData.setData(0,"CENT_IS_PRIVATE",0);
		rowDocData.setData(0,"BDMD_READ_ONLY",rowTemplate.getDataRef(0,"BDMD_READ_ONLY"));

		return true;
	}

}


bool DocumentHelper::CreateLocalDocumentFromTemplate(DbRecordSet &rowTemplate,DbRecordSet &rowDocData,bool bNoFileApp)
{

	QString strFileNameTarget;

	if (rowTemplate.getRowCount()==0)
		return false;

	int nDocTemplateID=rowTemplate.getDataRef(0,"BDMD_ID").toInt();
	int nDocTemplateTypeOriginal=rowTemplate.getDataRef(0,"BDMD_DOC_TYPE").toInt();
	int nDocTemplateType=nDocTemplateTypeOriginal;


	//	Check if: template is not local or internet and new type is, abort:
	if (!(nDocTemplateTypeOriginal==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE || nDocTemplateTypeOriginal==GlobalConstants::DOC_TYPE_INTERNET_FILE))
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Failed to create document from template! To create Local Or Internet Document, Template must be either Local or Internet document type!"));
		return false;
	}



	//if local file, create ATS dir, copy file to it
	if (nDocTemplateTypeOriginal==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
	{
		//get new & old name:
		QString strFileNameSource=rowTemplate.getDataRef(0,"BDMD_DOC_PATH").toString();

		//copy from local file (always ask):
		strFileNameTarget=CopyFromPath(strFileNameSource,"",bNoFileApp);
		if (strFileNameTarget.isEmpty()) return false;

	}
	else
	{
		DbRecordSet rowRevision;
		QString strFileNameSource=CopyFromRevision(nDocTemplateID,rowRevision,false);
		if (strFileNameSource.isEmpty()) 
			return false;

		QString strNewFileName=rowTemplate.getDataRef(0,"BDMD_DOC_PATH").toString();

		//copy from local file (always ask):
		strFileNameTarget=CopyFromPath(strFileNameSource,"",bNoFileApp,strNewFileName);
		if (strFileNameTarget.isEmpty()) return false;
	}



	//-------------------------------------------------------
	//Copy from template, assign all fields that are possible
	//-------------------------------------------------------

	//only if file is used, then use it, else, copy all from template:
	QString strBaseName;
	QFileInfo infoTarget(strFileNameTarget);
	strBaseName=infoTarget.fileName();


	rowDocData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
	rowDocData.addRow();

	rowDocData.setData(0,"BDMD_NAME",strBaseName);
	rowDocData.setData(0,"BDMD_DOC_PATH",strFileNameTarget);
	rowDocData.setData(0,"BDMD_DESCRIPTION",rowTemplate.getDataRef(0,"BDMD_DESCRIPTION"));
	rowDocData.setData(0,"BDMD_APPLICATION_ID",rowTemplate.getDataRef(0,"BDMD_APPLICATION_ID"));
	rowDocData.setData(0,"BDMD_TEMPLATE_ID",rowTemplate.getDataRef(0,"BDMD_ID"));
	rowDocData.setData(0,"CENT_CE_TYPE_ID",rowTemplate.getDataRef(0,"CENT_CE_TYPE_ID"));
	rowDocData.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
	rowDocData.setData(0,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
	rowDocData.setData(0,"CENT_OWNER_ID",g_pClientManager->GetPersonID());
	rowDocData.setData(0,"BDMD_DOC_TYPE",GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE);
	//rowDocData.setData(0,"CENT_IS_PRIVATE",0);
	rowDocData.setData(0,"BDMD_SIZE",(int)infoTarget.size());
	rowDocData.setData(0,"BDMD_OPEN_PARAMETER",rowTemplate.getDataRef(0,"BDMD_OPEN_PARAMETER"));
	rowDocData.setData(0,"BDMD_READ_ONLY",rowTemplate.getDataRef(0,"BDMD_READ_ONLY"));

	return true;


}




//source: local or internet, target=internet
bool DocumentHelper::CreateInternetDocumentFromTemplate(DbRecordSet &rowTemplate,DbRecordSet &rowDocData,DbRecordSet &rowRevision, bool bAskForNewName)
{
	QString strFileNameTarget;

	if (rowTemplate.getRowCount()==0)
		return false;

	int nDocTemplateID=rowTemplate.getDataRef(0,"BDMD_ID").toInt();
	int nDocTemplateTypeOriginal=rowTemplate.getDataRef(0,"BDMD_DOC_TYPE").toInt();
	int nDocTemplateType=nDocTemplateTypeOriginal;


	//	Check if: template is not local or internet and new type is, abort:
	if (!(nDocTemplateTypeOriginal==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE || nDocTemplateTypeOriginal==GlobalConstants::DOC_TYPE_INTERNET_FILE))
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("Failed to create document from template! To create Local Or Internet Document, Template must be either Local or Internet document type!"));
		return false;
	}


	//if local file, create ATS dir, copy file to it
	if (nDocTemplateTypeOriginal==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
	{
		//get temp dir:
		QString strATSDir=DocumentHelper::GetTempDirectoryForAutomaticStorage();
		if (strATSDir.isEmpty()) return false;

		//get new & old name:
		QString strFileNameSource=rowTemplate.getDataRef(0,"BDMD_DOC_PATH").toString();
		QFileInfo info(strFileNameSource);
		strFileNameTarget=info.fileName();

		//ask for name
		if (bAskForNewName)
		{
			biginputdialog Dlg(tr("Enter Document Name With Extension"),tr("Enter Document Name With Extension"),NULL,GetNewDocumentProposedName(strFileNameTarget));
			Dlg.resize(500,40);
			if(Dlg.exec())
				strFileNameTarget=Dlg.m_strInput;
			else
				return false;

			//QInputDialog Dlg(NULL); //(tr("Enter Document Name")); //issue 1872
			//Dlg.resize(500,40);
			//strFileNameTarget=QInputDialog::getText(NULL,tr("Enter Document Name"),tr("Enter Document Name:"),QLineEdit::Normal,GetNewDocumentProposedName(strFileNameTarget));
			//if (strFileNameTarget.isEmpty())
			//	return false;
		}

		//new file path:
		strFileNameTarget=strATSDir+"/"+strFileNameTarget;

		//copy from local file:
		strFileNameTarget=CopyFromPath(strFileNameSource,strFileNameTarget);
		if (strFileNameTarget.isEmpty()) return false;


		if(!DocumentHelper::CreateDocumentRevisionFromPath(rowRevision,strFileNameTarget))
		{
			QMessageBox::warning(NULL,tr("Warning"),tr("Failed to create document from template!"));
			return false;
		}
	}
	else
	{
		QString strNewFileName=rowTemplate.getDataRef(0,"BDMD_DOC_PATH").toString();
		strFileNameTarget=CopyFromRevision(nDocTemplateID,rowRevision,bAskForNewName,strNewFileName);
		if (strFileNameTarget.isEmpty()) 
			return false;
	}


	//-------------------------------------------------------
	//Copy from template, assign all fields that are possible
	//-------------------------------------------------------

	//only if file is used, then use it, else, copy all from template:
	QString strBaseName;
	QFileInfo infoTarget(strFileNameTarget);
	strBaseName=infoTarget.fileName();


	rowDocData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
	rowDocData.addRow();

	rowDocData.setData(0,"BDMD_NAME",strBaseName);
	rowDocData.setData(0,"BDMD_DOC_PATH",strFileNameTarget);
	rowDocData.setData(0,"BDMD_DESCRIPTION",rowTemplate.getDataRef(0,"BDMD_DESCRIPTION"));
	rowDocData.setData(0,"BDMD_APPLICATION_ID",rowTemplate.getDataRef(0,"BDMD_APPLICATION_ID"));
	rowDocData.setData(0,"BDMD_TEMPLATE_ID",rowTemplate.getDataRef(0,"BDMD_ID"));
	rowDocData.setData(0,"CENT_CE_TYPE_ID",rowTemplate.getDataRef(0,"CENT_CE_TYPE_ID"));
	rowDocData.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
	rowDocData.setData(0,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
	rowDocData.setData(0,"CENT_OWNER_ID",g_pClientManager->GetPersonID());
	rowDocData.setData(0,"BDMD_DOC_TYPE",GlobalConstants::DOC_TYPE_INTERNET_FILE);
	//rowDocData.setData(0,"CENT_IS_PRIVATE",0);
	rowDocData.setData(0,"BDMD_SIZE",(int)infoTarget.size());
	rowDocData.setData(0,"BDMD_OPEN_PARAMETER",rowTemplate.getDataRef(0,"BDMD_OPEN_PARAMETER"));
	rowDocData.setData(0,"BDMD_READ_ONLY",rowTemplate.getDataRef(0,"BDMD_READ_ONLY"));


	return true;
}



//strFilePath last check-in/out path, just to extract filename
QString DocumentHelper::CheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly,DbRecordSet &rowRevision, DbRecordSet &recCheckOutInfo,bool bAskForLocation,bool bSkipLock,bool bDoNotTrackCheckOutDoc,QString strDefaultDirPath)
{
	//Q_ASSERT(false);

	Status err;
	int nSize=0;
	QString strPath=ResolvePathBeforeCheckOutDocument(err,nDocumentID,strFilePath,nSize,bAskForLocation,strDefaultDirPath);
	if (!err.IsOK())
	{
		_CHK_ERR_NO_RET(err);
		return "";
	}

	if (strPath.isEmpty())
		return "";


	//save path when check out: 

	//check out //prepare for cancel:
	QFileInfo info2(strPath);
	g_pClientManager->ShowTransferProgressDlg(tr("Transferring document: ")+info2.fileName());
	_SERVER_CALL(BusDocuments->CheckOut(err,nDocumentID,g_pClientManager->GetPersonID(),rowRevision,bReadOnly,recCheckOutInfo,strPath,bSkipLock,-1))
	g_pClientManager->HideTransferProgressDlg();

	if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED)
		return "";

	//if (ClearDocumentProgress())
	//	return "";

	if (!err.IsOK())
	{
		if (err.getErrorCode()==StatusCodeSet::ERR_BUS_FAILED_TO_CHECK_OUT_DOCUMENT)
		{
			//recCheckOutInfo.Dump();
			QString strError="Document can not be checked out because it is checked out by other user!"; //+recCheckOutInfo.getDataRef(0,"BPER_LAST_NAME").toString()+", "+recCheckOutInfo.getDataRef(0,"BPER_FIRST_NAME").toString();
			QMessageBox::critical(NULL,tr("Error"),strError,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return "";
		}
		else
		{
			_CHK_ERR_NO_RET(err);
			return "";
		}
	}

	//test if got at least 1 revision:
	if (rowRevision.getRowCount()==0)
	{
		QMessageBox::critical(NULL,tr("Error"),tr("Document does not have any revisions uploaded!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		if(!bReadOnly)
		{
			_SERVER_CALL(BusDocuments->CancelCheckOut(err,nDocumentID,g_pClientManager->GetPersonID(),bSkipLock))
				_CHK_ERR_NO_RET(err);
		}
		return "";
	}

	//issue 1436:
	//load apps, find right one, if flag is set then set file as read only
	bool bSetReadOnlyFile=false;
	if(bReadOnly) //only if read only, set flag only if document have set read only flag on check out copy
	{
		DbRecordSet lstApps;
		DocumentHelper::GetDocApplications(lstApps);
		int nAppID=recCheckOutInfo.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
		int nAppRow=lstApps.find("BDMA_ID",nAppID,true);
		if (nAppRow>=0)
		{
			if (lstApps.getDataRef(nAppRow,"BDMA_SET_READ_ONLY_FLAG").toInt()>0)
				bSetReadOnlyFile=true;
		}
	}




	//save content to desired location, if fails, revert
	if(!DocumentHelper::SaveFileContent(rowRevision.getDataRef(0,"BDMR_CONTENT").toByteArray(),strPath,rowRevision.getDataRef(0,"BDMR_IS_COMPRESSED").toInt(),bSetReadOnlyFile))
	{
		if(!bReadOnly)
		{
			_SERVER_CALL(BusDocuments->CancelCheckOut(err,nDocumentID,g_pClientManager->GetPersonID(),bSkipLock))
				_CHK_ERR_NO_RET(err);
		}
		return "";
	}


	//reload check-out docs:
	DbRecordSet lstDummy;
	GetCheckedOutDocs(lstDummy,true);


	//notify all other that document is checked out:
	if(!bReadOnly)
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CHECK_OUT_DOC,nDocumentID); 


	//add path:
	if (!bDoNotTrackCheckOutDoc)
		DWatcher_AddFilePath(nDocumentID,strPath);

	return strPath;
}



//strFilePath location for check in:
//issue 1821: if strTag is set nAskForTag is ignored
//issue 1821: if strTag is empty nAskForTag=-1 (go to server/cache and check BDMD_SET_TAG_FLAG), 0 - do not ask, 1- ask
bool DocumentHelper::CheckInDocument(int nDocumentID,QString strFilePath,DbRecordSet &recInsertedDocumentRevision,QString strTag,bool bOverWrite,bool bSkipLock,int nAskForTag)
{
	
	//Q_ASSERT(false);
	int nSize;
	if(!ResolveRevisionBeforeCheckInDocument(nDocumentID,strFilePath,nSize,recInsertedDocumentRevision,strTag,bSkipLock,nAskForTag))
		return false;

	if(!TestDocumentRevisionsForMaximumSize(strFilePath))
		return false;
	
	//save data, as server destroys at checkin:
	//temporary record:
	DbRecordSet rowRevision=recInsertedDocumentRevision;
	DbRecordSet recCheckOutInfo;
	Status err;

	//check out //prepare for cancel:
	QFileInfo info2(strFilePath);
	g_pClientManager->ShowTransferProgressDlg(tr("Transferring document: ")+info2.fileName());
	_SERVER_CALL(BusDocuments->CheckIn(err,nDocumentID,rowRevision,bOverWrite,recCheckOutInfo,bSkipLock,nSize))
	g_pClientManager->HideTransferProgressDlg();

	if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED)
		return false;

	if (!err.IsOK())
	{
		_CHK_ERR_NO_RET(err);
		return false;
	}

	//reload check-out docs:
	DbRecordSet lstDummy;
	GetCheckedOutDocs(lstDummy,true);
	//notify all other that document is checked in:
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CHECK_IN_DOC,nDocumentID); 
	DWatcher_RemoveFilePath(nDocumentID);
	recInsertedDocumentRevision.setData(0,0,rowRevision.getDataRef(0,0)); //get ID from server
	return true;
}

bool DocumentHelper::ResolveRevisionBeforeCheckInDocument(int nDocumentID,QString strFilePath,int &nFileSize,DbRecordSet &recInsertedDocumentRevision,QString strTag,bool bSkipLock,int nAskForTag, bool bSkipLoadContent)
{
	if(!CreateDocumentRevisionFromPath(recInsertedDocumentRevision,strFilePath,false,&nFileSize,bSkipLoadContent))
	{
		if(!CheckFileBeforeCheckInDocument(nDocumentID,strFilePath,bSkipLock)) //check path, ask user to find manually
			return false;

		if(!CreateDocumentRevisionFromPath(recInsertedDocumentRevision,strFilePath,false,&nFileSize,bSkipLoadContent)) //try again with new path, if failed, exit!!
			return false;
	}

	//issue 1821:
	if (strTag.isEmpty())
		strTag=AskForRevisionTag(nDocumentID,strFilePath,nAskForTag);

	recInsertedDocumentRevision.setData(0,"BDMR_DOCUMENT_ID",nDocumentID);
	recInsertedDocumentRevision.setData(0,"BDMR_REVISION_TAG",strTag);

	return true;
}



//Check if file exists, if so then ok, if not, ask user to manually find, if failed or wish to discard or cancel: return false!!!
bool DocumentHelper::CheckFileBeforeCheckInDocument(int nDocumentID,QString &strFilePath,bool bSkipLock)
{
	//B.T. 05.12.2007: new feature: when not found document, then ask for file location:
	QFileInfo filecheckIn(strFilePath);
	if (filecheckIn.exists()) //if exists then OK!
		 return true;

	QString strFilter="*.*";
	QString strStartDir=DocumentHelper::GetDefaultDocumentDirectory()+"/"+strFilePath;

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	strFilePath = QFileDialog::getOpenFileName(
		NULL,
		tr("Find Checked-out Document"),
		strStartDir,
		strFilter);

	if (!strFilePath.isEmpty())
	{
		QFileInfo fileInfo(strFilePath);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	bool bAssert=true;
	if (!strFilePath.isEmpty())//if file picked and create success then ok
	{
		return true;
	}

	if (bAssert) //default handler
	{
		QString strError="Failed to check in document! Document Not Found";
		int nResult=QMessageBox::question(NULL,tr("Failed to check in document"),tr("Failed to check in document! Document Not Found!"),tr("  Discard Checked-Out Revision of Document  "),tr(" Cancel "),"",0,1);
		if (nResult==0)
		{
			DocumentHelper::CancelCheckOutDocument(nDocumentID,bSkipLock);
		}
		return false; //exit as is
	}
	return false;
}

bool DocumentHelper::CancelCheckOutDocument(int nDocumentID, bool bSkipLock)
{
	Status err;
	_SERVER_CALL(BusDocuments->CancelCheckOut(err,nDocumentID,g_pClientManager->GetPersonID(),bSkipLock))
		if (!err.IsOK())
		{
			_CHK_ERR_NO_RET(err);
			return false;
		}

		//clear
		DWatcher_RemoveFilePath(nDocumentID);


		//reload check-out docs:
		DbRecordSet lstDummy;
		GetCheckedOutDocs(lstDummy,true);


		//notify all other that document is checked in:
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CHECK_IN_DOC,nDocumentID); 

		return true;
}

void DocumentHelper::CheckInIfPossible(DbRecordSet &lstDocuments)
{
	int nCnt=0;
	DbRecordSet lstCheckedOutDocs;
	GetCheckedOutDocs(lstCheckedOutDocs,true);

	//lstCheckedOutDocs.Dump();
	//lstDocuments.Dump();

	int nSize=lstDocuments.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		int nRow=lstCheckedOutDocs.find("BDMD_ID",lstDocuments.getDataRef(i,"BDMD_ID"),true);
		if (nRow!=-1)
		{
			int nDocID=lstCheckedOutDocs.getDataRef(nRow,"BDMD_ID").toInt();
			QString strFilePath=lstCheckedOutDocs.getDataRef(nRow,"BDMD_DOC_PATH").toString();
			DbRecordSet rowRev;
			g_DownloadManager->ScheduleCheckInDocument(nDocID,strFilePath,"",false);
			//if(CheckInDocument(nDocID,strFilePath,rowRev,"",false))
				nCnt++;
		}
	}

	QString strMsg;
	if (nCnt==0)
	{
		strMsg=tr("No document for check in!"); //skip msg
	}
	/*
	else
	{
		strMsg=QVariant(nCnt).toString()+tr(" documents scheduled for checked in!");
		QMessageBox::information(NULL,tr("Information"),strMsg);
	}
	*/

}


void DocumentHelper::ClearCheckOutDocumentAfterDrag(int nDocumentID,QString strFilePath)
{
	//delete file:
	QFile::remove(strFilePath);

	QFileInfo info(strFilePath);

	Status err;
	_SERVER_CALL(BusDocuments->SetCheckOutPath(err,nDocumentID,info.fileName()))
		_CHK_ERR(err);

	//reload check-out docs:
	DbRecordSet lstDummy;
	GetCheckedOutDocs(lstDummy,true);
}

//ENTITY_BUS_DM_CHECK_OUT_DOCS
void DocumentHelper::GetCheckedOutDocs(DbRecordSet &lstData,bool bReloadFromServer)
{
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS));

	if(g_pClientManager->GetPersonID()==0) return;


	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_BUS_DM_CHECK_OUT_DOCS);
		if (plstCacheData)
		{
			lstData=*plstCacheData;
			return;
		}
	}

	//SERVER:
	Status err;
	QString strWhere =" WHERE BDMD_IS_CHECK_OUT=1 AND BDMD_CHECK_OUT_USER_ID="+QVariant(g_pClientManager->GetPersonID()).toString();
	_SERVER_CALL(ClientSimpleORM->Read(err,BUS_DM_DOCUMENTS,lstData,strWhere,DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS))
		if (!err.IsOK())
		{
			QMessageBox::critical(NULL,(QString)tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//Store in Cache:
		g_ClientCache.SetCache(ENTITY_BUS_DM_CHECK_OUT_DOCS,lstData);

}

//--------------------------------------------------------------
//FILE WATCHER:
//--------------------------------------------------------------

void DocumentHelper::DWatcher_AddFilePath(int nDocument,QString strFilePath)
{

	RedefineFileWatcherList();

	int nRow=s_lstChangedDocument.find(0,nDocument,true);
	if (nRow!=-1) return;

	//if file does not exists, skip it:
	QFileInfo info(strFilePath);
	if (!info.exists()) 
		return;

	s_lstChangedDocument.addRow();
	nRow=s_lstChangedDocument.getRowCount()-1;
	s_lstChangedDocument.setData(nRow,0,nDocument);
	s_lstChangedDocument.setData(nRow,1,strFilePath);
	s_lstChangedDocument.setData(nRow,2,false);

	s_DocumentWatcher.addPath(strFilePath);
}

void DocumentHelper::DWatcher_RemoveFilePath(int nDocument)
{
	RedefineFileWatcherList();

	int nRow=s_lstChangedDocument.find(0,nDocument,true);
	if (nRow==-1) return;
	QString strFilePath=s_lstChangedDocument.getDataRef(nRow,1).toString();
	s_lstChangedDocument.deleteRow(nRow);

	s_DocumentWatcher.removePath(strFilePath);
}


bool DocumentHelper::DWatcher_IsFileChanged(int &nDocument,QString strFilePath)
{
	RedefineFileWatcherList();

	int nRow=s_lstChangedDocument.find(0,nDocument,true);
	if (nRow==-1) return false;
	else
		return s_lstChangedDocument.getDataRef(nRow,2).toBool();
}

void DocumentHelper::DWatcher_SetFileChanged(QString strFilePath)
{
	RedefineFileWatcherList();

	int nRow=s_lstChangedDocument.find(1,strFilePath,true);
	if (nRow==-1) return;

	s_lstChangedDocument.setData(nRow,2,true);
}

void DocumentHelper::DWatcher_ClearState()
{
	RedefineFileWatcherList();

	int nSize=s_lstChangedDocument.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strFilePath=s_lstChangedDocument.getDataRef(i,1).toString();
		s_DocumentWatcher.removePath(strFilePath);
	}
	s_lstChangedDocument.clear();

}

//Test before logout: if return true, abort QUIT, send event to check in documents..
bool DocumentHelper::DWatcher_CheckForUnsavedDocuments()
{
	RedefineFileWatcherList();
	int nSize=s_lstChangedDocument.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (s_lstChangedDocument.getDataRef(i,2).toBool()) //on first stop
		{
			//ask to choose: ready or full
			int nResult=QMessageBox::question(NULL,tr("Save Changes"),tr("There are some checked out and modified documents. Do you want to check in all modified documents?"),tr("Yes"),tr("No"));
			if (nResult==0) 
				return true;
		}
	}
	return false;
}

//try to save locally modified files: check in, but leave file on disc
bool DocumentHelper::DWatcher_SaveChanges(bool bSkipAsk)
{
	bool bSomeDocumentAreInSavingProcess=false;
	RedefineFileWatcherList();

	int nSize=s_lstChangedDocument.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (s_lstChangedDocument.getDataRef(i,2).toBool()) //on first stop
		{
			//ask to choose: ready or full
			int nResult=-1;
			if (!bSkipAsk)
				nResult=QMessageBox::question(NULL,tr("Save Changes"),tr("There are some checked out and modified documents. Do you want to check in all modified documents?"),tr("Yes"),tr("No"));
			else
				nResult=0; //just check in..

			if (nResult==0) 
			{
				DbRecordSet lstPaths=s_lstChangedDocument; //copy to local coz, check in will delete them
				nSize=lstPaths.getRowCount();
				for(int k=0;k<nSize;++k)
				{
					if (!lstPaths.getDataRef(k,2).toBool()) continue;

					g_DownloadManager->ScheduleCheckInDocument(lstPaths.getDataRef(k,0).toInt(),lstPaths.getDataRef(k,1).toString());
					bSomeDocumentAreInSavingProcess=true;
					/*
					//save all:
					DbRecordSet recRev;
					if(!DocumentHelper::CheckInDocument(lstPaths.getDataRef(k,0).toInt(),lstPaths.getDataRef(k,1).toString(),recRev))
						return false;
					*/
				}
			}
			break;
		}
	}

	return !bSomeDocumentAreInSavingProcess;
}


void DocumentHelper::RedefineFileWatcherList()
{
	if (s_lstChangedDocument.getColumnCount()>0)return;
	s_lstChangedDocument.destroy();
	s_lstChangedDocument.addColumn(QVariant::Int,"ID");
	s_lstChangedDocument.addColumn(QVariant::String,"PATH");
	s_lstChangedDocument.addColumn(QVariant::Bool,"CHANGED");

}

QFileSystemWatcher* DocumentHelper::DWatcher()
{
	return &s_DocumentWatcher;
}

void DocumentHelper::RedefineDefaultsList()
{
	if (s_lstContactsDef.getColumnCount()>0)return;
	s_lstProjectsDef.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	s_lstContactsDef.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
	s_rowQCWContactDef.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

}



//when closing application:
//load all checked out docs
//if changed, ask to save->check in, else leave as is
//get all from TempXXXX directories:
//if checked out leave, other: compare last modified if >24hr, delete
void DocumentHelper::DocumentGarbageCollector()
{

	//try to save locally modified files
	if(!DWatcher_SaveChanges()) 
	{ DWatcher_ClearState();return;}


	//--------------GET ALL FILES FROM TEMP DIRECTORY------------------------------
	QString strDirPath=DocumentHelper::GetDefaultDocumentDirectory();
	int nDirNameSize=QString(SOKRATES_AUTOINCREMENT_TEMP_FOLDER_SUFFIX).size()+4;

	QDir dir(strDirPath);
	if (!dir.exists())
	{ DWatcher_ClearState();return;}

	QFileInfoList   lstDocumentFiles;

	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isDir())
		{
			QString strDirectoryName=lstFiles.at(i).fileName();
			//skip current and parent directory:
			if (strDirectoryName==".") continue;
			if (strDirectoryName=="..") continue;

			//test if match out criteria
			if (strDirectoryName.indexOf(SOKRATES_AUTOINCREMENT_TEMP_FOLDER_SUFFIX)>=0 && strDirectoryName.size()==nDirNameSize)
			{ 
				QDir dirTemp(lstFiles.at(i).filePath());
				if (!dirTemp.exists()) continue;

				QFileInfoList	lstDocs=dirTemp.entryInfoList(QDir::Files);
				if (lstDocs.size()>0)
				{
					lstDocumentFiles<<lstDocs.at(0);
				}
				else
				{
					dir.rmdir(strDirectoryName);
				}
			}
		}
	}

	if (lstDocumentFiles.size()==0)
	{
		DWatcher_ClearState();
		return;
	}


	//-----------------SKIP ALL CHECKED OUT------------------------------
	//even if they are checked in, skip delete, leave 2 days on disk

	nSize=lstDocumentFiles.size();
	for(int i=nSize-1;i>=0;--i)
	{
		//skip delete
		QString strPath=lstDocumentFiles.at(i).filePath();
		if (s_lstChangedDocument.find(1,strPath,true)!=-1)
			lstDocumentFiles.removeAt(i);
		else
		{
			//ready for delete if older then 2 days:
			if (lstDocumentFiles.at(i).lastModified().addDays(2)<QDateTime::currentDateTime())
			{
				QFile file(lstDocumentFiles.at(i).absoluteFilePath());
				file.setPermissions(QFile::WriteOwner | QFile::ExeOwner | QFile::WriteUser | QFile::ExeUser | QFile::WriteGroup | QFile::ExeGroup | QFile::WriteOther | QFile::ExeOther);

				QDir tmpDir(lstDocumentFiles.at(i).absolutePath());
				QString strDirectoryName=tmpDir.dirName();
				if (strDirectoryName==".") continue;
				if (strDirectoryName=="..") continue;
				if (strDirectoryName.indexOf(SOKRATES_AUTOINCREMENT_TEMP_FOLDER_SUFFIX)>=0 && strDirectoryName.size()==nDirNameSize)
				{
					bool bOK=tmpDir.remove(lstDocumentFiles.at(i).fileName());
					Q_ASSERT(bOK);
					dir.rmdir(strDirectoryName);
				}
			}
		}
	}


	//clear all lists:
	DWatcher_ClearState();
}


//maximum size for uploaded documents is 16Mb: 16777216
bool DocumentHelper::TestDocumentRevisionsForMaximumSize(DbRecordSet &lstRevisons)
{
	//Q_ASSERT(false);

	int nSize=lstRevisons.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		int nSizeFile=lstRevisons.getDataRef(i,"BDMR_CONTENT").toByteArray().size();
		if (nSizeFile>XML_MAXIMUM_BINARY_SIZE)
		{
			QString strName=tr("File: ")+lstRevisons.getDataRef(i,"BDMR_NAME").toString()+tr(" can not be uploaded on server!");
			QMessageBox::warning(NULL,tr("Warning"),strName+tr("File size can not be larger than 80Mb!"));
			return false;
		}
	}

	return true;
}

bool DocumentHelper::TestDocumentRevisionsForMaximumSize(QString strPath)
{
	QFileInfo file(strPath);
	if (file.size()>XML_MAXIMUM_BINARY_SIZE)
	{
		QString strName=tr("File: ")+strPath+tr(" can not be uploaded on server!");
		QMessageBox::warning(NULL,tr("Warning"),strName+tr("File size can not be larger than 80Mb!"));
		return false;
	}
	return true;
}

//adds all checked out paths (of logged person) to file system watcher if not already added
//returns flag is file is modified locally while SOKRATES session was up
void DocumentHelper::TestCheckedOutDocsForModificiation(DbRecordSet &lstData,QList<bool> &lstChangedByUser)
{
	int nCheckOutPersonID=lstData.getColumnIdx("BDMD_CHECK_OUT_USER_ID");
	int nIDIdx=lstData.getColumnIdx("BDMD_ID");
	int nPathIdx=lstData.getColumnIdx("BDMD_DOC_PATH");
	int nLMIdx=lstData.getColumnIdx("BDMD_DAT_LAST_MODIFIED");
	int nSize=lstData.getRowCount();

	for(int i=0;i<nSize;++i)
	{
		//only if our:
		if (g_pClientManager->GetPersonID()==lstData.getDataRef(i,nCheckOutPersonID).toInt())
		{
			int nID=lstData.getDataRef(i,nIDIdx).toInt();
			QString strPath=lstData.getDataRef(i,nPathIdx).toString();
			DWatcher_AddFilePath(nID,strPath);
			lstChangedByUser<<DWatcher_IsFileChanged(nID,strPath);
		}
		else
		{
			lstChangedByUser<<false;
		}
	}
}




//lstContacts= must have bcnt id, project must have busp_id
void DocumentHelper::SetDocumentDropDefaults(DbRecordSet *lstContacts,DbRecordSet *lstProjects,DbRecordSet *rowQCWContact)
{
	RedefineDefaultsList();

	s_lstContactsDef.clear();
	s_lstProjectsDef.clear();
	s_rowQCWContactDef.clear();

	if (lstContacts)
	{
		//lstContacts->Dump();
		int nCntIdx=lstContacts->getColumnIdx("BCNT_ID");
		int nCntIdx1=s_lstContactsDef.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nCntIdx==-1)
			nCntIdx=lstContacts->getColumnIdx("BNMR_TABLE_KEY_ID_2");

		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		int nSize=lstContacts->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			s_lstContactsDef.addRow();
			s_lstContactsDef.setData(s_lstContactsDef.getRowCount()-1,nCntIdx1,lstContacts->getDataRef(i,nCntIdx));
		}

	}

	if (lstProjects)
	{
		int nCntIdx=lstProjects->getColumnIdx("BUSP_ID");
		int nCntIdx1=s_lstProjectsDef.getColumnIdx("BNMR_TABLE_KEY_ID_2");
		if (nCntIdx==-1)
			nCntIdx=lstProjects->getColumnIdx("BNMR_TABLE_KEY_ID_2");

		Q_ASSERT(nCntIdx!=-1);
		Q_ASSERT(nCntIdx1!=-1);

		int nSize=lstProjects->getRowCount();
		for(int i=0;i<nSize;++i)
		{
			s_lstProjectsDef.addRow();
			s_lstProjectsDef.setData(s_lstProjectsDef.getRowCount()-1,nCntIdx1,lstProjects->getDataRef(i,nCntIdx));
		}

	}

	if (rowQCWContact)
	{
		if (rowQCWContact->getRowCount()>0)
			s_rowQCWContactDef=*rowQCWContact;
	}
}


void DocumentHelper::GetDocumentDropDefaults(DbRecordSet *lstContacts,DbRecordSet *lstProjects,DbRecordSet *rowQCWContact)
{
	RedefineDefaultsList();

	if (lstContacts)
	{
		*lstContacts=s_lstContactsDef; //copy data
	}

	if (lstProjects)
	{
		*lstProjects=s_lstProjectsDef;
	}

	if (rowQCWContact)
	{
		*rowQCWContact=s_rowQCWContactDef;
	}


}




int DocumentHelper::AskForDocumentType()
{

	//if both disabled, return:
	int nValue;

	if(!g_FunctionPoint.IsFPAvailable(FP_DM_INTERNET, nValue) && !g_FunctionPoint.IsFPAvailable(FP_DM_LOCAL, nValue))
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("You do not have permission to save document either locally or on Internet!"));
		return -1;
	}

	//if internet disabled return local:
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_INTERNET, nValue))
	{
		return GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
	}

	//else return internet:
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_LOCAL, nValue))
	{
		return GlobalConstants::DOC_TYPE_INTERNET_FILE;
	}

	int nPersonSetting=g_pSettings->GetPersonSetting(PERSON_DEFAULT_DOCUMENT_TYPE).toInt();
	if (nPersonSetting==0)
	{
		nPersonSetting=g_pSettings->GetApplicationOption(APP_DEFAULT_DOCUMENT_TYPE).toInt();
		if(nPersonSetting==0)
		{
			//int nResult; //=QMessageBox::question(NULL,tr("Choose Document Storage Location"),tr("Choose Document Storage Location:"),tr("  Save Reference(s) to Local File(s)  "),tr("  Store File Centrally on Internet  "),tr(" Cancel "),0,2);
			Dlg_StorageLocation Dlg;
			int nResult=Dlg.exec();
			if (nResult==1)
				return GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
			else if (nResult==2)
				return GlobalConstants::DOC_TYPE_INTERNET_FILE;
			else
				return -1; //exit
		}
		else if (nPersonSetting==1)
		{
			return GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
		}
		else
		{
			return GlobalConstants::DOC_TYPE_INTERNET_FILE;
		}
	}
	else if (nPersonSetting==1)//ASK
	{
		//int nResult=QMessageBox::question(NULL,tr("Choose Document Storage Location"),tr("Choose Document Storage Location:"),tr("  Save Reference(s) to Local File(s)  "),tr("  Store File Centrally on Internet  "),tr(" Cancel "),0,2);
		Dlg_StorageLocation Dlg;
		int nResult=Dlg.exec();
		if (nResult==1)
			return GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
		else if (nResult==2)
			return GlobalConstants::DOC_TYPE_INTERNET_FILE;
		else
			return -1; //exit
	}
	else if (nPersonSetting==2)//LOCAL
	{
		return GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE;
	}
	else
	{
		return GlobalConstants::DOC_TYPE_INTERNET_FILE; //INTERNET
	}
}


void DocumentHelper::ClearData()
{
	s_lstContactsDef.clear();
	s_lstProjectsDef.clear();
	s_rowQCWContactDef.clear();
	DWatcher_ClearState();
}

//Basically: used for pack & send documents and create from template: limits to 20mb (should be lower)...
bool DocumentHelper::TestDocumentTemplateRevisionsForMaximumSize(int nDocumentID)
{
	//check for size:
	Status err;
	int nFileSize;
	QString strServerFileName;
	_SERVER_CALL(BusDocuments->GetDocumentFileNameAndSize(err,nDocumentID,strServerFileName,nFileSize))
	if (!err.IsOK())
	{
		_CHK_ERR_NO_RET(err);
		return false;
	}
	if (nFileSize>MAX_TEMPLATE_SIZE) //restrict template size
	{
		QMessageBox::warning(NULL,tr("Warning"),QString(tr("Operation failed! Create from template and pack and send functions can not use documents with size greater then %1!")).arg(MAX_TEMPLATE_SIZE));
		return false;
	}

	return true;

}

//copies file from server, if failed, empty string return: only for templates!!!
QString DocumentHelper::CopyFromRevision(int nDocumentID,DbRecordSet &rowRevision,bool bAskForNewName,QString strNewProposedName)
{
	if(!TestDocumentTemplateRevisionsForMaximumSize(nDocumentID)) //test if revision can be fetched ho-ruk
		return "";

	//check for size:
	Status err;
	int nFileSize;
	QString strServerFileName;
	_SERVER_CALL(BusDocuments->GetDocumentFileNameAndSize(err,nDocumentID,strServerFileName,nFileSize))
	if (!err.IsOK())
	{
		_CHK_ERR_NO_RET(err);
		return "";
	}
	if (nFileSize>MAX_TEMPLATE_SIZE) //restrict template size
	{
		QMessageBox::warning(NULL,tr("Warning"),QString(tr("Operation failed because document revision size is greater then %1!")).arg(MAX_TEMPLATE_SIZE));
		return "";
	}


	QString strFile;
	//load latest revision, read only for template, if not exists: false
	
	DbRecordSet  recCheckOutInfo;
	_SERVER_CALL(BusDocuments->CheckOut(err,nDocumentID,g_pClientManager->GetPersonID(),rowRevision,true,recCheckOutInfo,"",true,-1))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return "";
		}

		if (rowRevision.getRowCount()==0)
		{
			QMessageBox::critical(NULL,tr("Error"),tr("Unable to retrieve latest revision from server"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return "";
		}

		rowRevision.setData(0,"BDMR_DOCUMENT_ID",0);
		rowRevision.setData(0,"BDMR_ID",0);


		QString strATSDir=DocumentHelper::GetTempDirectoryForAutomaticStorage();
		if (strATSDir.isEmpty())
			return "";


		QString strFileNameTarget=rowRevision.getDataRef(0,"BDMR_NAME").toString();

		//ask for name
		if (bAskForNewName)
		{
			if (!strNewProposedName.isEmpty())
			{
				QFileInfo info(strNewProposedName);
				strFileNameTarget=info.fileName(); //MB screw up!
			}

			biginputdialog Dlg(tr("Enter Document Name With Extension"),tr("Enter Document Name With Extension:"),NULL,GetNewDocumentProposedName(strFileNameTarget));
			Dlg.resize(500,40);
			if(Dlg.exec())
				strFileNameTarget=Dlg.m_strInput;
			else
				return "";
			//QInputDialog Dlg(tr("Enter Document Name")); //issue 1872
			//Dlg.resize(500,40);
			//strFileNameTarget=QInputDialog::getText(NULL,tr("Enter Document Name"),tr("Enter Document Name:"),QLineEdit::Normal,GetNewDocumentProposedName(strFileNameTarget));
			//if (strFileNameTarget.isEmpty())
			//	return false;
		}

		strFile=strATSDir+"/"+strFileNameTarget;


		//save content to desired location, if fails, revert
		if(!DocumentHelper::SaveFileContent(rowRevision.getDataRef(0,"BDMR_CONTENT").toByteArray(),strFile,rowRevision.getDataRef(0,"BDMR_IS_COMPRESSED").toInt()))
			return "";

		return strFile;
}



//copies file from given location, if failed, empty string return
//if strCopyToPath is set, it will copy to it, else, it will ask user
QString DocumentHelper::CopyFromPath(QString strPath, QString strCopyToPath,bool bSilent,QString strNewProposedName)
{
	QString strFile;

	//-------------------------------------------------------
	//Check if template exists:
	//-------------------------------------------------------
	QFileInfo info(strPath);
	QFile newDoc(strPath);
	if (!newDoc.exists())
	{
		QMessageBox::critical(NULL,tr("Error"),tr("Template file  ")+strPath+ tr(" does not exists! Operation aborted!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return "";
	}


	if (strCopyToPath.isEmpty())
	{
		QString strProposedFileName=info.fileName();
		if (!strNewProposedName.isEmpty())
		{
			QFileInfo info2(strNewProposedName);
			strProposedFileName=info2.fileName();
		}
		//ask for save path:
		strFile=DocumentHelper::GetCheckOutDocumentSavePath(strProposedFileName,bSilent);
		if (strFile.isEmpty())
			return "";
	}
	else
		strFile=strCopyToPath;


	//-------------------------------------------------------
	//Copy from template file to target (on disk)
	//-------------------------------------------------------
	if(!newDoc.copy(strFile)) //copy to new location
	{
		QFileInfo info(strFile);	//only fire error if not exists
		if (!info.exists())
		{
			QMessageBox::critical(NULL,tr("Copy Failed!"),tr("Copy failed to location:  ")+strFile+ tr(". Operation aborted!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return "";
		}
	}

	return strFile;
}


QString DocumentHelper::GetNewDocumentProposedName(QString strTemplateName)
{
	QString strName;

	int npos=strTemplateName.indexOf(".");
	QString strTemplate,strAfter;
	if (npos>0)
	{
		strTemplate=strTemplateName.left(npos);
		strAfter=strTemplateName.right(strTemplateName.length()-npos);
	}
	else
		strTemplate=strTemplateName;

	//name:
	int nNameFormat=g_pSettings->GetApplicationOption(APP_DEFAULT_DOCUMENT_NAME).toInt();
	if (nNameFormat & 0x01)
		strName=strTemplate+" ";
	if (nNameFormat & 0x02)
		strName+=QDate::currentDate().toString("dd-MM-yyyy")+" "; //Qt::LocaleDate);
	if (nNameFormat & 0x04)
		strName+=g_pClientManager->GetPersonCode();

	strName=strName.trimmed();

	if (strName.isEmpty())
		return strTemplateName;
	else
		return strName+strAfter;
}


//always insert mode!!!
bool DocumentHelper::WriteDocument(DbRecordSet &lstDocs,DbRecordSet &lstRevisions,int  nOpenDocumentFUIMode)
{

	RedefineDefaultsList();

	//if file size is greateer then allowed
	//if(!DocumentHelper::TestDocumentRevisionsForMaximumSize(lstRevisions))
	//	return false;

	if (lstDocs.getRowCount()==0)
		return false;

	QString strFile=lstDocs.getDataRef(0,"BDMD_DOC_PATH").toString();
	int nApplication=lstDocs.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
	int nDocType=lstDocs.getDataRef(0,"BDMD_DOC_TYPE").toInt();


	//-------------------------------------------------------
	//Write
	//------------------------------------------------------
	DbRecordSet empty;
	Status err;

	//actual check in /empty revisions:

	lstRevisions.clear();
	/*
	int nSize=lstDocs.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		QString strFilePath=lstDocs.getDataRef(i,"BDMD_DOC_PATH").toString();
		int nDocType=lstDocs.getDataRef(i,"BDMD_DOC_TYPE").toInt();
		if (nDocType!=GlobalConstants::DOC_TYPE_INTERNET_FILE) continue;

		int nDocID=lstDocs.getDataRef(i,"BDMD_ID").toInt();
		g_DownloadManager->ScheduleCheckInDocument(nDocID,strFilePath);
	}
*/

	//lstDocs.Dump();
	//s_lstProjectsDef.Dump();

	//check out //prepare for cancel:
	QFileInfo info2(strFile);
	g_pClientManager->ShowTransferProgressDlg(tr("Transferring document: ")+info2.fileName());
	DbRecordSet lstUAR,lstGAR;
	_SERVER_CALL(BusDocuments->Write(err,lstDocs,s_lstProjectsDef,s_lstContactsDef,empty,lstRevisions,"", lstUAR,lstGAR))
	g_pClientManager->HideTransferProgressDlg();

	if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED)
		return false;
	//if (ClearDocumentProgress())
	//	return false;

	//actual check in:
	int nSize=lstDocs.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		QString strFilePath=lstDocs.getDataRef(i,"BDMD_DOC_PATH").toString();
		int nDocType=lstDocs.getDataRef(i,"BDMD_DOC_TYPE").toInt();
		if (nDocType!=GlobalConstants::DOC_TYPE_INTERNET_FILE) continue;

		int nDocID=lstDocs.getDataRef(i,"BDMD_ID").toInt();
		g_DownloadManager->ScheduleCheckInDocument(nDocID,strFilePath,"",false,false,-1,true); //delete inserted document if check in fails!!!
	}


	//return false;

	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return false;
	}

	//issue: 986; check if app has got template
	if (lstDocs.getDataRef(0,"BDMD_TEMPLATE_FLAG").toInt()>0)
	{
		DbRecordSet lstApplications;
		DocumentHelper::GetDocApplications(lstApplications);
		int nRow=lstApplications.find(0,nApplication,true);
		if (nRow!=-1)
		{
			if (lstApplications.getDataRef(nRow,"BDMA_TEMPLATE_ID").toInt()<=0)
			{
				//if not then assign this one->save->reload cache or empty
				DbRecordSet rowApp=lstApplications.getRow(nRow);
				rowApp.setData(0,"BDMA_TEMPLATE_ID",lstDocs.getDataRef(0,"BDMD_ID"));
				Status err;
				QString pLockResourceID;
				int nQueryView = -1;
				int nSkipLastColumns = 0; 
				DbRecordSet lstForDelete;
				_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_APPLICATIONS,rowApp, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
				_CHK_ERR_RET_BOOL_ON_FAIL(err);
				g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,ENTITY_BUS_DM_APPLICATIONS); 
			}
		}


	}




	//-------------------------------------------------------
	//Send signal for all to reload:
	//------------------------------------------------------
	QVariant varData;
	qVariantSetValue(varData,lstDocs);
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ENTITY_BUS_DM_DOCUMENTS,varData); 


	//-------------------------------------------------------
	//Open document:
	//-------------------------------------------------------
	if (nOpenDocumentFUIMode>=0)
	{
		//get back id:
		int nRecordID=lstDocs.getDataRef(0,0).toInt();
		if (nDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_DOCUMENT_OPEN_FUI_SIMPLE_MODE_AFTER_UPLOAD,nRecordID,nOpenDocumentFUIMode); //issue 1826
		else
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_DOCUMENT_OPEN_FUI_SIMPLE_MODE,nRecordID,nOpenDocumentFUIMode); //issue 2351


		//issue 1826: do not open doc
		/*
		if (nApplication<=0) return true;
		DbRecordSet lstApplications;
		DocumentHelper::GetDocApplications(lstApplications);

		QString strAppPath;
		int nRow=lstApplications.find(0,nApplication,true);
		if (nRow==-1) return true;

		//issue 1826: do not open doc
		//return DocumentHelper::OpenDocumentInExternalApp_Shell(nApplication,strFile,""); 
		*/
		return true;
	}
	else
		return true;


}



//imports docs: sets path, 
//lookup all applications with same extensions, assignes them
//set default type, sets name = name of file
void DocumentHelper::RegisterDocumentInternetAddresses(DbRecordSet &lstPaths, bool bRegisterAsTemplate, QString strCategory)
{

	DbRecordSet lstRevisions;
	lstRevisions.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_REVISIONS));

	DbRecordSet lstWrite;
	lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));

	int nSize=lstPaths.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		lstWrite.addRow();
		int nRow=lstWrite.getRowCount()-1;

		QString strFilePath=lstPaths.getDataRef(i,0).toString();
		//issue 2399: safari: try to find EOF and cut off rest:
		if (strFilePath.indexOf("\n"))
			strFilePath=strFilePath.left(strFilePath.indexOf("\n"));

		lstWrite.setData(nRow,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
		lstWrite.setData(nRow,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
		lstWrite.setData(nRow,"CENT_OWNER_ID",g_pClientManager->GetPersonID());
		//lstWrite.setData(nRow,"BDMD_NAME",lstPaths.getDataRef(i,1));
		lstWrite.setData(nRow,"BDMD_NAME",lstPaths.getDataRef(i,1).toString().simplified()); //issue 2409
		lstWrite.setData(nRow,"BDMD_DOC_PATH",strFilePath);
		lstWrite.setData(nRow,"BDMD_DOC_TYPE",GlobalConstants::DOC_TYPE_URL);
		lstWrite.setData(nRow,"BDMD_TEMPLATE_FLAG",(int)bRegisterAsTemplate);
		//lstWrite.setData(nRow,"CENT_IS_PRIVATE",0);
		lstWrite.setData(nRow,"BDMD_CATEGORY",strCategory);

		//try resolve application by extension
		//issue 1643
		QVariant varAppID;
		if (strFilePath.indexOf("http")==0)
		{
			varAppID=DocumentHelper::GetApplicationFromExtension("htm",true);
			if (varAppID.isNull())
				varAppID=DocumentHelper::GetApplicationFromExtension("html",true);
		}
		else
		{
			varAppID=DocumentHelper::GetApplicationFromExtension(strFilePath);
		}


		//qDebug()<<varAppID.toInt();
		lstWrite.setData(nRow,"BDMD_APPLICATION_ID",varAppID); 

	}

	if (lstWrite.getRowCount()==0)
		return;


	//write & open FUI (issue 1826: open internet files in edit mode)
	WriteDocument(lstWrite,lstRevisions,1); //edit mode

}






//write app: code=name, usr paths...
void DocumentHelper::RegisterApplicationPaths(DbRecordSet &lstPaths)
{
	DbRecordSet lstWrite;
	lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS));

	DbRecordSet lstWriteUserPath;
	lstWriteUserPath.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_USER_PATHS));

	int nSize=lstPaths.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		lstWrite.addRow();
		int nRow=lstWrite.getRowCount()-1;
		lstWriteUserPath.addRow();

		lstWrite.setData(nRow,"BDMA_CODE",lstPaths.getDataRef(i,1));
		lstWrite.setData(nRow,"BDMA_NAME",lstPaths.getDataRef(i,1));
		lstWriteUserPath.setData(nRow,"BDMU_APP_PATH",lstPaths.getDataRef(i,0).toString().replace("\\","/")); //always save /

		//extract icon:
		QPixmap icon;
		if(PictureHelper::GetApplicationIcon(QDir::toNativeSeparators(lstPaths.getDataRef(i,0).toString()),icon))
		{
			QByteArray picture;
			PictureHelper::ConvertPixMapToByteArray(picture,icon);
			lstWrite.setData(nRow,"BDMA_ICON",picture);
		}
	}

	//write:
	Status err;
	QString pLockResourceID;
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	DbRecordSet lstForDelete;
	_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_APPLICATIONS,lstWrite, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		int nSize2=lstWrite.getRowCount();
		for (int i=0;i<nSize2;++i)
		{
			lstWriteUserPath.setData(i,"BDMU_OWNER_ID",g_pClientManager->GetPersonID());
			lstWriteUserPath.setData(i,"BDMU_APPLICATION_ID",lstWrite.getDataRef(0,0));
		}

		_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_USER_PATHS,lstWriteUserPath, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			else //register user patH:
			{
				DocumentHelper::GetDocUserPaths(lstWriteUserPath,true);
			}

			//LoadApplications(true);
			//-------------------------------------------------------
			//Send signal for all to reload:
			//------------------------------------------------------
			QVariant varData;
			qVariantSetValue(varData,lstWrite);
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ENTITY_BUS_DM_APPLICATIONS,varData); 


}




//imports docs: sets path, 
//lookup all applications with same extensions, assignes them
//set default type, sets name = name of file
void DocumentHelper::RegisterDocumentPaths(DbRecordSet &lstPaths,QList<QUrl> &lstSource,int nIsTemplate, QString strCategory,bool bSkipWrite, DbRecordSet *plstWrite, DbRecordSet *plstRevisions, int nDefaultDocType, int nOpenDocumentFUI)
{
	RedefineDefaultsList();

	DbRecordSet lstRevisions;
	lstRevisions.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_REVISIONS));

	if (nDefaultDocType==-1)
		nDefaultDocType=DocumentHelper::AskForDocumentType();
	if (nDefaultDocType==-1) return;

	DbRecordSet lstWrite;
	lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));

	//issue 2063: test for templates:
	if (nIsTemplate)
	{	
		int nSize=lstPaths.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			QString strPath=lstPaths.getDataRef(i,0).toString();
			if(!TestForOfficeTemplateFiles(strPath))
				return; 
		}
	}

	//get person settings:
	bool bParseTypes=g_pSettings->GetApplicationOption(APP_PERSON_DOCUMENT_TYPE_PARSING_ON).toBool();
	if (bParseTypes)
	{
		DetermineDocType(lstPaths,lstSource);		//adds column -> CENT_CE_TYPE_ID=NULL by default
	}

	//B.T. compare by actual filename, not document name
	DbRecordSet lstCheckOutDocs;
	GetCheckedOutDocs(lstCheckOutDocs);
	lstCheckOutDocs.addColumn(QVariant::String,"REAL_FILE_NAME");
	int nSize2=lstCheckOutDocs.getRowCount();
	for(int i=0;i<nSize2;++i)
	{
		QFileInfo info(lstCheckOutDocs.getDataRef(i,"BDMD_DOC_PATH").toString());
		lstCheckOutDocs.setData(i,"REAL_FILE_NAME",info.fileName());
	}

	//_DUMP(lstPaths);

	//DbRecordSet lstApplications;
	//GetDocApplications(lstApplications);

	//int nExtIdx=lstApplications.getColumnIdx("BDMA_EXTENSIONS");
	//int nSizeApp=lstApplications.getRowCount();
	int nSizeM=lstPaths.getRowCount();
	for (int i=0;i<nSizeM;++i)
	{
		QString strFilePath=lstPaths.getDataRef(i,0).toString();
		QFileInfo info(strFilePath);

		//B.T.:05.12.2007 when check in new documents, check if they exists in already check-out (by filename), if so, ask for check in?
		int nRowC=lstCheckOutDocs.find("REAL_FILE_NAME",info.fileName(),true);
		if (nRowC>=0)
		{
			//ask if wanna check in:
			if(QMessageBox::question(NULL,tr("Check In"),tr("Checked-out document with same name: ")+info.fileName()+tr(" already exists. Do you wish to check-in?"),tr("Yes"),tr("No"),0,1)==0)
			{
				int nDocID=lstCheckOutDocs.getDataRef(nRowC,"BDMD_ID").toInt();
				//QString strFilePath=lstCheckOutDocs.getDataRef(nRowC,"BDMD_DOC_PATH").toString();
				//DbRecordSet rowRev;
				//DocumentHelper::CheckInDocument(nDocID,strFilePath,rowRev,"",false,false,lstCheckOutDocs.getDataRef(nRowC,"BDMD_SET_TAG_FLAG").toInt());
				g_DownloadManager->ScheduleCheckInDocument(nDocID,strFilePath,"",false,false,lstCheckOutDocs.getDataRef(nRowC,"BDMD_SET_TAG_FLAG").toInt());
				continue;
			}
		}

		lstWrite.addRow();
		int nRow=lstWrite.getRowCount()-1;


		lstWrite.setData(nRow,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
		lstWrite.setData(nRow,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
		lstWrite.setData(nRow,"CENT_OWNER_ID",g_pClientManager->GetPersonID());
		lstWrite.setData(nRow,"BDMD_NAME",info.fileName()); //lstPaths.getDataRef(i,1)); issue==803
		lstWrite.setData(nRow,"BDMD_DOC_PATH",strFilePath);
		lstWrite.setData(nRow,"BDMD_DOC_TYPE",nDefaultDocType);
		lstWrite.setData(nRow,"BDMD_TEMPLATE_FLAG",nIsTemplate);
		//lstWrite.setData(nRow,"CENT_IS_PRIVATE",0);
		lstWrite.setData(nRow,"BDMD_CATEGORY",strCategory);
		if(bParseTypes)lstWrite.setData(nRow,"CENT_CE_TYPE_ID",lstPaths.getDataRef(i,"CENT_CE_TYPE_ID"));

		lstWrite.setData(nRow,"BDMD_APPLICATION_ID",DocumentHelper::GetApplicationFromExtension(strFilePath)); //set to app


		if (nDefaultDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		{
			//if(!TestDocumentRevisionsForMaximumSize(strFilePath)) //precheck
			//	return;

			int nSize;
			DbRecordSet recRevision;
			if(!DocumentHelper::CreateDocumentRevisionFromPath(recRevision,strFilePath,false,&nSize,true))
			{
				QMessageBox::critical(NULL,tr("Error"),tr("Revision could not be loaded for file:")+strFilePath,QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			lstWrite.setData(nRow,"BDMD_SIZE",nSize);
			lstRevisions.merge(recRevision);
		}
		else if (nDefaultDocType==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
		{
			lstWrite.setData(nRow,"BDMD_SIZE",(int)info.size());
		}

	}

	if (lstWrite.getRowCount()==0)
		return;

	//issue: 1265
	if (nIsTemplate)
	{
		s_lstProjectsDef.clear();
		s_lstContactsDef.clear();
		s_rowQCWContactDef.clear();
	}

	//write & open FUI
	if (!bSkipWrite)
	{
		WriteDocument(lstWrite,lstRevisions,nOpenDocumentFUI);
	}
	else
	{
		*plstWrite=lstWrite;
		*plstRevisions=lstRevisions;
		return;
	}

	//reload from server:
	//if (nIsTemplate)
	//	LoadTemplates(true);
	//place info:
	//QString strMsg=
	//QMessageBox::information(this,tr("Information"),tr("Successfully registered {")+QVariant(nSize).toString()+tr("} document(s)!"));

}



//from doc template, execute: create new doc from template: doc:menu widget
void DocumentHelper::ApplicationInstallFinder(int nDocTemplateID)
{
	DbRecordSet lstTemplates,lstApps;
	DocumentHelper::GetDocTemplates(lstTemplates);
	DocumentHelper::GetDocApplications(lstApps);

	int nRow=lstTemplates.find("BDMD_ID",nDocTemplateID,true);
	if (nRow==-1)return;
	DbRecordSet row=lstTemplates.getRow(nRow);
	DbRecordSet rowDocData,recRevision;

	int nApplicationID=row.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
	int nRow1=lstApps.find("BDMA_ID",nApplicationID,true);
	int nAppNoFile=0;
	if (nRow1!=-1)
		nAppNoFile=lstApps.getDataRef(nRow1,"BDMA_NO_FILE_DOCUMENT").toInt();

	//-------------------------------------------------------
	//Create doc from template:
	//-------------------------------------------------------
	if(!DocumentHelper::CreateDocumentFromTemplate(row,rowDocData,recRevision,true,nAppNoFile))
		return;

	OpenDocumentInExternalApp(rowDocData,true);
}


//CENT_CE_TYPE_ID-
void DocumentHelper::DetermineDocType(DbRecordSet &lstPaths, QList<QUrl> &lst)
{
	//load ce_types
	MainEntitySelectionController ce_types;
	ce_types.Initialize(ENTITY_CE_TYPES);
	ce_types.GetLocalFilter()->SetFilter("CET_COMM_ENTITY_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
	ce_types.ReloadData();
	DbRecordSet *lstTypes=ce_types.GetDataSource();

	//if url is directory->look below 1 step-> that's type, but what step?
	//if url is file->look up one step->that's type


	if (lstPaths.getColumnIdx("CENT_CE_TYPE_ID")==-1)
		lstPaths.addColumn(QVariant::Int,"CENT_CE_TYPE_ID");
	lstPaths.setColValue(lstPaths.getColumnIdx("CENT_CE_TYPE_ID"),QVariant(QVariant::Int));


	//determine types:
	int nSize=lstPaths.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//file:
		QString strPath=lstPaths.getDataRef(i,0).toString();
		QFileInfo file(strPath);
		QDir dir = file.absoluteDir();

		bool bWasDropAsFile=false;
		for (int n=0;n<lst.size();++n)
			if (lst.at(n).toLocalFile()==strPath)
				bWasDropAsFile=true;

		if (bWasDropAsFile)
		{
			QString strTpye=dir.dirName();
			if (!strTpye.isEmpty())
			{
				int nRow=lstTypes->find("CET_MAPPED_NAME",strTpye,true);
				if (nRow!=-1)
					lstPaths.setData(i,"CENT_CE_TYPE_ID",lstTypes->getDataRef(nRow,"CET_ID"));				
			}
		}
		else //was drop as directory, file is probably inside directory
		{
			QString strDirPath=dir.absolutePath();		//dir path of dropped file
			for (int k=0;k<lst.size();++k)
			{
				QFileInfo infoSource(lst.at(k).toLocalFile());
				qDebug()<<lst.at(k).toLocalFile();
				qDebug()<<infoSource.absoluteFilePath();

				if (infoSource.isDir() && strDirPath.contains(infoSource.absoluteFilePath()))
				{
					//qDebug()<<infoSource.absolutePath();
					QDir dirSource(lst.at(k).toLocalFile());
					//qDebug()<<dirSource.dirName();

					QFileInfoList	lstFiles=dirSource.entryInfoList();
					int nSize=lstFiles.size();
					for(int z=0;z<nSize;++z)
					{
						if (lstFiles.at(z).isDir())
						{
							//skip current and parent directory:
							if (lstFiles.at(z).fileName()==".") continue;
							if (lstFiles.at(z).fileName()=="..") continue;

							if (strDirPath.contains(lstFiles.at(z).absoluteFilePath()))
							{
								QDir dirTarget(lstFiles.at(z).absoluteFilePath());
								QString strTpye=dirTarget.dirName();
								if (!strTpye.isEmpty())
								{
									int nRow=lstTypes->find("CET_MAPPED_NAME",strTpye,true);
									if (nRow!=-1)
										lstPaths.setData(i,"CENT_CE_TYPE_ID",lstTypes->getDataRef(nRow,"CET_ID"));				
								}
								break;
							}
						}
					}
					break;
				}
			}
		}
	}

}




//based on row: opens doc or loads revision, opens...
//nCheckOutCopy =-1 ask for check out type
bool DocumentHelper::OpenDocumentInExternalApp(DbRecordSet &rowDocument,bool bNoFileDocument,QString strDefaultDirPath, int nCheckOutCopy)
{
	RedefineDefaultsList();

	//PackAndSendDocuments(rowDocument);
	//return true;


	QString strDocName=rowDocument.getDataRef(0,"BDMD_NAME").toString();
	QString strDocPath=rowDocument.getDataRef(0,"BDMD_DOC_PATH").toString();
	QString strParams=rowDocument.getDataRef(0,"BDMD_OPEN_PARAMETER").toString();
	int nAppID=rowDocument.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
	int nDocID=rowDocument.getDataRef(0,"BDMD_ID").toInt();
	int nDocType=rowDocument.getDataRef(0,"BDMD_DOC_TYPE").toInt();
	int nReadOnlyDoc=rowDocument.getDataRef(0,"BDMD_READ_ONLY").toInt();
	int nReadOnlyApp=0;
	int nEncodeParams=0;




	//------------------------------------isuee 859------------------------------------------
	//								Square bracket notation
	//------------------------------------isuee 859------------------------------------------

	bool bLoaded=false;
	int nContactID=0, nProjectID=0;

	//-----------------------------------------------
	//			APP PARAMETERS
	//-----------------------------------------------

	//prepare app paths:
	DbRecordSet lstApps;
	QString strAppParameters;

	DocumentHelper::GetDocApplications(lstApps);

	int nRow=lstApps.find(0,nAppID,true);
	if (nRow!=-1)
	{
		strAppParameters=lstApps.getDataRef(nRow,"BDMA_OPEN_PARAMETER").toString().trimmed();
		nReadOnlyApp=lstApps.getDataRef(nRow,"BDMA_READ_ONLY").toInt();
		nEncodeParams=lstApps.getDataRef(nRow,"BDMA_ENCODE_PARAMETERS").toInt();
		if (!strAppParameters.isEmpty())
		{
			nContactID=0;
			nProjectID=0;
			if (!bNoFileDocument)
			{
				if (!bLoaded)
				{
					Status err;
					_SERVER_CALL(BusDocuments->GetDocumentProjectAndContact(err,nDocID,nProjectID,nContactID))
						_CHK_ERR_RET_BOOL_ON_FAIL(err);
				}
			}
			else
			{
				if (s_lstContactsDef.getRowCount()>0)
				{
					nContactID=s_lstContactsDef.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt();
				}
				if (s_lstProjectsDef.getRowCount()>0)
				{
					nProjectID=s_lstProjectsDef.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt();
				}
			}

			//parse:
			QString strNewAppParameters;
			EmailHelper::CreateEmailFromTemplate(strAppParameters,strNewAppParameters,nContactID,nProjectID,nEncodeParams,&s_rowQCWContactDef);
			strAppParameters=strNewAppParameters;
		}
	}

	//-----------------------------------------------
	//			DOC PARAMETERS
	//-----------------------------------------------



	//prepare doc paths:
	if (!strParams.isEmpty())
	{
		Status err;
		if (!bNoFileDocument)
		{
			_SERVER_CALL(BusDocuments->GetDocumentProjectAndContact(err,nDocID,nProjectID,nContactID))
				_CHK_ERR_RET_BOOL_ON_FAIL(err);
		}
		else
		{
			if (s_lstContactsDef.getRowCount()>0)
			{
				nContactID=s_lstContactsDef.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt();
			}
			if (s_lstProjectsDef.getRowCount()>0)
			{
				nProjectID=s_lstProjectsDef.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toInt();
			}
		}

		bLoaded=true;

		//parse:
		QString strNewParam;
		EmailHelper::CreateEmailFromTemplate(strParams,strNewParam,nContactID,nProjectID,false,&s_rowQCWContactDef);
		strParams=strNewParam;

	}




	//------------------------------------isuee 859------------------------------------------
	//								Square bracket notation
	//------------------------------------isuee 859------------------------------------------



	if (nDocType==GlobalConstants::DOC_TYPE_URL )
	{
		return DocumentHelper::OpenDocumentInExternalApp_Shell(nAppID,strDocPath,strParams,strAppParameters);
	}
	else if (nDocType==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
	{
		if (!QFile::exists(strDocPath))
		{
			if (DataHelper::IsPortableVersion()) //try with local drive/removable:
			{
				if (QFile::exists(DataHelper::ReplaceCurrentDriveInPath(strDocPath)))
					return DocumentHelper::OpenDocumentInExternalApp_Shell(nAppID,DataHelper::ReplaceCurrentDriveInPath(strDocPath),strParams,strAppParameters);
			}
			QString strTitle=tr("The Local File ")+strDocPath+tr(" is not visible on this computer and can not be opened!");
			QMessageBox::warning(NULL,tr("Warning"),strTitle);
			return false;
		}
		else
			return DocumentHelper::OpenDocumentInExternalApp_Shell(nAppID,strDocPath,strParams,strAppParameters);
	}
	else if (nDocType==GlobalConstants::DOC_TYPE_INTERNET_FILE)
	{

		//check if doc is checked
		Status err;
		int nPersonID;
		_SERVER_CALL(BusDocuments->IsCheckedOut(err,nDocID,nPersonID,strDocPath))
			_CHK_ERR_NO_RET(err);
		if (!err.IsOK()) return false;

		if (nPersonID==-1 && strDocPath.isEmpty()) //if doc is not checked, path is reseted: return back
		{
			strDocPath=rowDocument.getDataRef(0,"BDMD_DOC_PATH").toString();
		}

		if (nPersonID==g_pClientManager->GetPersonID()) //if I did do check out: just open it
		{
			if (DataHelper::IsPortableVersion() && !QFile::exists(strDocPath)) //try with local drive/removable:
			{
				if (QFile::exists(DataHelper::ReplaceCurrentDriveInPath(strDocPath)))
					strDocPath=DataHelper::ReplaceCurrentDriveInPath(strDocPath);
			}
			return DocumentHelper::OpenDocumentInExternalApp_Shell(nAppID,strDocPath,strParams,strAppParameters); 
		}

		bool bReadOnly=false;
		if (nCheckOutCopy==-1)
		{
			//if so: tell
			
			if (nPersonID==-1 && nReadOnlyApp==0 && nReadOnlyDoc==0)
			{
				//ask to choose: ready or full
				Dlg_StorageLocation Dlg(false,strDocName,strDocPath);
				//int nResult=QMessageBox::question(NULL,tr("Open Document"),tr("Open Document: ")+strDocName,tr("Load Original And Modify"),tr("Load Copy to View Only"),tr(" Cancel "),0,2);
				int nResult=Dlg.exec();
				if (nResult==1) 
					bReadOnly=false;
				else if(nResult==2)
					bReadOnly=true;
				else
					return false;
			}
			else
			{
				//issue 2666: if locked notify user and propose him to open document in read only mode:
				if (nPersonID!=-1 && nReadOnlyApp==0 && nReadOnlyDoc==0)
				{
					//load persons & org:
					MainEntitySelectionController pers;
					pers.Initialize(ENTITY_BUS_PERSON);
					pers.ReloadData(true);

					DbRecordSet *plstPersons = pers.GetDataSource();
					int nRow=plstPersons->find("BPER_ID",nPersonID,true);
					if (nRow>=0)
					{
						QString strUserName=plstPersons->getDataRef(nRow,"BPER_FIRST_NAME").toString()+" "+plstPersons->getDataRef(nRow,"BPER_LAST_NAME").toString()+" ("+plstPersons->getDataRef(nRow,"BPER_INITIALS").toString()+")";
						QString strMessage=QString(tr("Document is already checked out by user %1. Do you want to open document in read-only mode?")).arg(strUserName);
						int nResult=QMessageBox::question(NULL,tr("Open Document"),strMessage,tr("Ok"),tr("Cancel"));
						if (nResult!=0)
							return false;
					}
				}

				bReadOnly=true;
			}
		}
		else
		{
			bReadOnly=(bool)nCheckOutCopy;
		}

		/*
		DbRecordSet rowRev,rowCheckOut;
		QString strFile=DocumentHelper::CheckOutDocument(nDocID,strDocPath,bReadOnly,rowRev,rowCheckOut,false,false,false,strDefaultDirPath);
		if (strFile.isEmpty()) return false;
		//open file
		return DocumentHelper::OpenDocumentInExternalApp_Shell(nAppID,strFile,strParams,strAppParameters);
		*/
		DbRecordSet recData;
		recData.addColumn(QVariant::Bool,"bReadOnly");
		recData.addColumn(QVariant::String,"strDocPath");
		recData.addColumn(QVariant::String,"strDefaultDirPath");
		recData.addColumn(QVariant::Int,"nAppID");
		recData.addColumn(QVariant::String,"strParams");
		recData.addColumn(QVariant::String,"strAppParameters");
		recData.addColumn(QVariant::Int,"nRevisionID");

		recData.addRow();
		recData.setData(0,"bReadOnly",bReadOnly);
		recData.setData(0,"strDocPath",strDocPath);
		recData.setData(0,"strDefaultDirPath",strDefaultDirPath);
		recData.setData(0,"nAppID",nAppID);
		recData.setData(0,"strParams",strParams);
		recData.setData(0,"strAppParameters",strAppParameters);
		recData.setData(0,"nRevisionID",-1);

		//_DUMP(recData);
		QVariant varData;
		qVariantSetValue(varData,recData);

		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_DOCUMENT_CHECK_OUT_AND_OPEN,nDocID,varData);
		return true;
	}
	else
	{
		//open FUI edit: nDocID
		//g_objFuiManager.OpenFUI(MENU_DM_DOCUMENTS, true, false,FuiBase::MODE_EDIT, nDocID);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_DOCUMENT_OPEN_FUI,nDocID,1);
		return false;
	}

}




bool DocumentHelper::StartWINCEProcess(QString strFile)
{
#ifdef WINCE
	QString strWarnText(tr(" Document path is "));
	strWarnText += strFile + tr(" and will be deleted after SOKRATES")+QChar(174)+ tr(" closes.");
	

	SHELLEXECUTEINFO ShExecInfo;
	memset(&ShExecInfo, 0, sizeof(ShExecInfo));
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = SEE_MASK_FLAG_NO_UI;
	//ShExecInfo.hwnd = NULL;
	ShExecInfo.lpVerb = QString("Open").utf16();
	ShExecInfo.lpFile = (LPCTSTR)strFile.utf16();
	ShExecInfo.lpParameters = NULL;
	ShExecInfo.lpDirectory = 0;
	ShExecInfo.nShow = SW_MAXIMIZE;
	//ShExecInfo.hInstApp = NULL;
	bool bOK = ShellExecuteEx(&ShExecInfo);

	switch((int)ShExecInfo.hInstApp)
	{
	case SE_ERR_FNF:
		strWarnText.prepend(QObject::tr("File not found!"));
		break;
	case SE_ERR_PNF:
		strWarnText.prepend(QObject::tr("Path not found!"));
		break;
	case SE_ERR_ACCESSDENIED:
		strWarnText.prepend(QObject::tr("Access denied!"));
		break;
	case SE_ERR_OOM:
		strWarnText.prepend(QObject::tr("Out of memory!"));
		break;
	case SE_ERR_DLLNOTFOUND:
		strWarnText.prepend(QObject::tr("Dynamic-link library not found!"));
		break;
	case SE_ERR_SHARE:
		strWarnText.prepend(QObject::tr("Cannot share an open file!"));
		break;
	case SE_ERR_ASSOCINCOMPLETE:
		strWarnText.prepend(QObject::tr("File association information not complete!"));
		break;
	case SE_ERR_DDETIMEOUT:
		strWarnText.prepend(QObject::tr("DDE operation timed out!"));
		break;
	case SE_ERR_DDEFAIL:
		strWarnText.prepend(QObject::tr("DDE operation failed!"));
		break;
	case SE_ERR_DDEBUSY:
		strWarnText.prepend(QObject::tr("DDE operation is busy!"));
		break;
	case SE_ERR_NOASSOC:
		strWarnText.prepend(QObject::tr("File association not available!"));
		break;
	}

	if (!bOK)
	{
		QMessageBox::warning(NULL,QObject::tr("Warning"),strWarnText);
		return false;
	}
	else
		return true;

#else
	return false;
#endif
	
}


void DocumentHelper::RegisterApplicationsAndTemplatesFromRFFile(DbRecordSet lstApps,DbRecordSet lstTemplates, DbRecordSet lstInterfaces, QString strRFFileDirectory)
{
	if (lstApps.getRowCount()>0)
	{
		int nDone,nSkipped;
		RegisterApplicationsFromRFFile(lstApps,strRFFileDirectory,nDone,nSkipped);
		QMessageBox::information(NULL,tr("Information"),QString(tr("Processed successfully: %1 applications, skipped/failed: %2")).arg(nDone).arg(nSkipped));
	}


	if (lstTemplates.getRowCount()>0)
	{
		int nDone,nSkipped;
		RegisterTemplatesFromRFFile(lstTemplates,strRFFileDirectory,nDone,nSkipped);
		QMessageBox::information(NULL,tr("Information"),QString(tr("Processed successfully: %1 templates, skipped/failed: %2")).arg(nDone).arg(nSkipped));
	}

	if (lstInterfaces.getRowCount()>0)
	{
		int nDone,nSkipped;
		RegisterInterfacesFromRFFile(lstInterfaces,strRFFileDirectory,nDone,nSkipped);
		QMessageBox::information(NULL,tr("Information"),QString(tr("Processed successfully: %1 interfaces, skipped/failed: %2")).arg(nDone).arg(nSkipped));
	}
}


void DocumentHelper::RegisterApplicationsFromRFFile(DbRecordSet lstApps, QString strRFFileDirectory, int &nDone, int &nSkipped)
{
	nDone=0;
	nSkipped=0;
	if (lstApps.getRowCount()==0)
		return;

	strRFFileDirectory=QDir::toNativeSeparators(strRFFileDirectory);
	if (strRFFileDirectory.right(1)!=QDir::separator())
		strRFFileDirectory +=QDir::separator();

	DbRecordSet lstWrite;
	lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS));
	lstWrite.addColumn(QVariant::String,"APP_PATH");
	DbRecordSet lstWriteUserPath;
	lstWriteUserPath.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_USER_PATHS));

	//templates: only check if exists with same name:
	DbRecordSet lstStoredApps;
	GetDocApplications(lstStoredApps,true);

	lstApps.clearSelection();

	int nSize=lstApps.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		//RESOLVE NAME
		QString strName=lstApps.getDataRef(i,"BDMA_NAME").toString();
		if (lstStoredApps.find("BDMA_NAME",strName,true)>=0)
		{
			bool bFound=false;
			int nTries=2;
			while (nTries<1000)
			{
				QString strNameTry=strName+" "+QVariant(nTries).toString();
				if (lstStoredApps.find("BDMA_NAME",strNameTry,true)>=0)
				{
					nTries++;
					continue;
				}

				lstApps.setData(i,"BDMA_NAME",strNameTry);
				strName=strNameTry;
				bFound=true;
				break;
			}

			if (!bFound)
			{
				lstApps.selectRow(i); //if name can not be resolved, then do not import..
				continue;
			}
		}

		//RESOLVE PATH
		//if full path:
		QString strPath=lstApps.getDataRef(i,"APP_PATH").toString();
		strPath=QDir::toNativeSeparators(strPath);
		if (strPath.left(1)==".") 
		{
			strPath=strRFFileDirectory+strPath.mid(2);
		}		
		else if (strPath.left(1)=="\\" && strPath.left(2)!="\\\\")
		{
			strPath=DataHelper::ReplaceCurrentDriveInPath(strPath);
		}

		//check if document exists:
		if(!QFile::exists(strPath))
		{
			QMessageBox::critical(NULL,tr("Error"),QString(tr("Application %1 does not exists on given path: %2. Application Registration skipped!")).arg(strName).arg(strPath));
			lstApps.selectRow(i); //if name can not be resolved, then do not import..
			continue;
		}
		strPath=strPath.replace("\\","/");
		lstApps.setData(i,"APP_PATH",strPath);

		lstWriteUserPath.addRow();
		lstWrite.merge(lstApps.getRow(i));
		int nRow=lstWrite.getRowCount()-1;
		lstWriteUserPath.setData(nRow,"BDMU_APP_PATH",lstApps.getDataRef(i,"APP_PATH").toString().replace("\\","/")); //always save /

		//extract icon:
		QPixmap icon;
		if(PictureHelper::GetApplicationIcon(QDir::toNativeSeparators(lstApps.getDataRef(i,"APP_PATH").toString()),icon))
		{
			QByteArray picture;
			PictureHelper::ConvertPixMapToByteArray(picture,icon);
			lstWrite.setData(nRow,"BDMA_ICON",picture);
		}
	}

	nSkipped=lstApps.getSelectedCount();
	lstApps.deleteSelectedRows();

	//lstWrite has all !...
	
	//2nd check: if application path already exists..skip
	//3nd check: if application with extension already exists (1-1)- SKIP, if partial, then OK!!

	DbRecordSet lstUserPaths;
	GetDocUserPaths(lstUserPaths,true);
	nSize=lstUserPaths.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		QString path=lstUserPaths.getDataRef(i,"BDMU_APP_PATH").toString().toUpper();
		path=QDir::toNativeSeparators(path);
		lstUserPaths.setData(i,"BDMU_APP_PATH",path);
	}


	lstWrite.clearSelection();
	nSize=lstWrite.getRowCount();
	for(int z=0;z<nSize;z++)
	{

		//1st check: if has extension: if so OK
		if (lstWrite.getDataRef(z,"BDMA_EXTENSIONS").toString().isEmpty())
			continue;

		//2nd check: if application path already exists..skip
		QString strAppPath=lstWrite.getDataRef(z,"APP_PATH").toString().toUpper();
		strAppPath=QDir::toNativeSeparators(strAppPath);
		QString strAppParam=lstWrite.getDataRef(z,"BDMA_EXTENSIONS").toString();
		QStringList lstParams=strAppParam.split(",",QString::SkipEmptyParts);

		//find app path:
		int nRow=lstUserPaths.find("BDMU_APP_PATH",strAppPath,true);
		if(nRow>=0)
		{
			lstWrite.selectRow(z);
			lstWriteUserPath.selectRow(z);
			continue;
		}
		int nSize2=lstUserPaths.getRowCount();
		for (int i=0;i<nSize2;++i)
		{
			QString strAppPath_User=DataHelper::ReplaceCurrentDriveInPath(lstUserPaths.getDataRef(i,"BDMU_APP_PATH").toString());
			if (strAppPath==strAppPath_User && QFile::exists(strAppPath_User)) //match!
			{
				lstWrite.selectRow(z);
				lstWriteUserPath.selectRow(z);
				continue;
			}
		}

		//3nd check: if application with extension already exists (1-1)- SKIP, if partial, then OK!!
		int nSize=lstStoredApps.getRowCount();
		for (int i=0;i<nSize;++i)
		{
			QString strOpenDoc=lstStoredApps.getDataRef(i,"BDMA_EXTENSIONS").toString();
			if (strOpenDoc.isEmpty())
				continue;
			QStringList lstStoredParam=strOpenDoc.split(",",QString::SkipEmptyParts);

			if (lstStoredParam.size()!=lstParams.size())
				continue;

			int nSize4=lstStoredParam.size();
			bool bMatch=true;
			for(int k=0;k<nSize4;k++)
			{
				if (lstStoredParam.at(k).trimmed().toUpper()!=lstParams.at(k).trimmed().toUpper())
				{
					bMatch=false;
					break;
				}
			}

			if (bMatch)
			{
				lstWrite.selectRow(z);
				lstWriteUserPath.selectRow(z);
				break;
			}
		}
	
	}

	nSkipped+=lstWrite.getSelectedCount();

	lstWrite.deleteSelectedRows();
	lstWriteUserPath.deleteSelectedRows();

	if (lstWrite.getRowCount()==0)
	{
		return;
	}


	DbRecordSet lstTemplates;
	lstTemplates.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
	lstTemplates.addColumn(QVariant::String,"FILE_PATH");
	lstTemplates.addColumn(QVariant::String,"APP_PATH");

	


	//filter templates
	nSize=lstWrite.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nRow=lstApps.find("APP_PATH",lstWrite.getDataRef(i,"APP_PATH").toString(),true);
		if (nRow>=0)
		{
			if (lstApps.getDataRef(nRow,"TEMPLATE_NAME").toString().isEmpty()) //if no template
				continue;

			lstTemplates.addRow();
			int nLastRow=lstTemplates.getRowCount()-1;

			//if (lstApps.getDataRef(nRow,"TEMPLATE_TYPE").toInt()==0)
			//	lstTemplates.setData(nLastRow, "BDMD_DOC_TYPE",GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE);
			//else
			//	lstTemplates.setData(nLastRow, "BDMD_DOC_TYPE",GlobalConstants::DOC_TYPE_INTERNET_FILE);
			lstTemplates.setData(nLastRow, "BDMD_DOC_TYPE",lstApps.getDataRef(nRow,"TEMPLATE_TYPE").toInt());
			lstTemplates.setData(nLastRow, "BDMD_NAME",lstApps.getDataRef(nRow,"TEMPLATE_NAME").toString()); 
			lstTemplates.setData(nLastRow, "FILE_PATH",lstApps.getDataRef(nRow,"TEMPLATE_PATH").toString()); 
			lstTemplates.setData(nLastRow, "APP_PATH",lstApps.getDataRef(nRow,"APP_PATH").toString()); 
			
		}
		else
			Q_ASSERT(false);
	}

	//write templates/clear app id:;
	lstTemplates.setColValue(lstTemplates.getColumnIdx("BDMD_APPLICATION_ID"),QVariant(QVariant::Int));

	int nDoneT,nSkippedT;
	RegisterTemplatesFromRFFile(lstTemplates,strRFFileDirectory,nDone,nSkipped,true);
	nSize=lstWrite.getRowCount(); //get id back
	for(int i=0;i<nSize;i++)
	{
		int nRow=lstTemplates.find("APP_PATH",lstWrite.getDataRef(i,"APP_PATH").toString(),true);
		if (nRow>=0)
		{
			if (lstTemplates.getDataRef(nRow,"BDMD_ID").toInt()>0)
				lstWrite.setData(i,"BDMA_TEMPLATE_ID",lstTemplates.getDataRef(nRow,"BDMD_ID").toInt());
		}
	}




	//write:
	Status err;
	QString strDummy;
	DbRecordSet lstForDelete;
	_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_APPLICATIONS,lstWrite,strDummy,DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS,1, lstForDelete)) //skip last 1
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	DbRecordSet lstDataUpdate;
	lstDataUpdate.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_UPDATE_APP));

	int nSize2=lstWrite.getRowCount();
	for (int i=0;i<nSize2;++i)
	{

		//update templates : app_id=
		//lstTemplates
		int nRow=lstTemplates.find("APP_PATH",lstWrite.getDataRef(i,"APP_PATH").toString(),true);
		if (nRow>=0)
		{
			lstDataUpdate.merge(lstTemplates.getRow(nRow));
			int nLast=lstDataUpdate.getRowCount()-1;
			lstDataUpdate.setData(nLast,"BDMD_APPLICATION_ID",lstWrite.getDataRef(i,"BDMA_ID"));
		}

		lstWriteUserPath.setData(i,"BDMU_OWNER_ID",g_pClientManager->GetPersonID());
		lstWriteUserPath.setData(i,"BDMU_APPLICATION_ID",lstWrite.getDataRef(0,0));
	}

	if (lstDataUpdate.getRowCount()>0)
	{
		QString strDummy;
		int nSkipLastColumns = 0; 
		DbRecordSet lstForDelete;
		_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_DOCUMENTS,lstDataUpdate,strDummy,DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_UPDATE_APP, nSkipLastColumns, lstForDelete));
		_CHK_ERR_NO_RET(err);
	}

	QString pLockResourceID;
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	_SERVER_CALL(ClientSimpleORM->Write(err,BUS_DM_USER_PATHS,lstWriteUserPath, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}
	else //register user patH:
	{
		DocumentHelper::GetDocUserPaths(lstWriteUserPath,true);
	}

	nDone=nSize2;

	//LoadApplications(true);
	//-------------------------------------------------------
	//Send signal for all to reload:
	//------------------------------------------------------
	QVariant varData;
	qVariantSetValue(varData,lstWrite);
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED,ENTITY_BUS_DM_APPLICATIONS,varData); 


}

void DocumentHelper::RegisterTemplatesFromRFFile(DbRecordSet &lstTemplates, QString strRFFileDirectory, int &nDone, int &nSkipped,bool bOverrideApplicationID)
{
	nDone=0;
	nSkipped=0;
	if (lstTemplates.getRowCount()==0)
		return;

	strRFFileDirectory=QDir::toNativeSeparators(strRFFileDirectory);
	if (strRFFileDirectory.right(1)!=QDir::separator())
		strRFFileDirectory +=QDir::separator();


	//templates: only check if exists with same name:
	DbRecordSet lstStoredTemplates;
	GetDocTemplates(lstStoredTemplates,true);

	QList<QUrl> lstTemplatesURL_Local,lstTemplatesURL_Internet;
	DbRecordSet lstWriteOtherTemplates;
	lstWriteOtherTemplates.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));

	lstTemplates.clearSelection();
	int nSize=lstTemplates.getRowCount();
	
	for(int i=0;i<nSize;i++)
	{
		//RESOLVE NAME
		QString strName=lstTemplates.getDataRef(i,"BDMD_NAME").toString();
		if (lstStoredTemplates.find("BDMD_NAME",strName,true)>=0)
		{
			int nTries=2;
			bool bFound=false;
			while (nTries<1000)
			{
				QString strNameTry=strName+" "+QVariant(nTries).toString();
				if (lstStoredTemplates.find("BDMD_NAME",strNameTry,true)>=0)
				{
					nTries++;
					continue;
				}
				lstTemplates.setData(i,"BDMD_NAME",strNameTry);
				strName=strNameTry;
				bFound=true;
				break;
			}

			if (!bFound)
			{
				lstTemplates.selectRow(i); //if name can not be resolved, then do not import..
				continue;
			}
		}


		//RESOLVE PATH
		if (lstTemplates.getDataRef(i,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE || lstTemplates.getDataRef(i,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_INTERNET_FILE)
		{
			//if full path:
			QString strPath=lstTemplates.getDataRef(i,"FILE_PATH").toString();
			strPath=QDir::toNativeSeparators(strPath);
			if (strPath.left(1)==".") 
			{
				strPath=strRFFileDirectory+strPath.mid(2);
			}
			else if (strPath.left(1)=="\\" && strPath.left(2)!="\\\\")
			{
				strPath=DataHelper::ReplaceCurrentDriveInPath(strPath);
			}

			//check if document exists:
			if(!QFile::exists(strPath))
			{
				QMessageBox::critical(NULL,tr("Error"),QString(tr("Template %1 does not exists on given path: %2. Template Registration skipped!")).arg(strName).arg(strPath));
				lstTemplates.selectRow(i); //if name can not be resolved, then do not import..
				continue;
			}
			strPath=strPath.replace("\\","/");
			lstTemplates.setData(i,"FILE_PATH",strPath);


			if (lstTemplates.getDataRef(i,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE)
				lstTemplatesURL_Local.append(QUrl(strPath));
			else
				lstTemplatesURL_Internet.append(QUrl(strPath));
		}
		else
		{
			//ALL OTHERS:
			lstWriteOtherTemplates.merge(lstTemplates.getRow(i));
			int nRow=lstWriteOtherTemplates.getRowCount()-1;
			lstWriteOtherTemplates.setData(nRow,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
			lstWriteOtherTemplates.setData(nRow,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
			lstWriteOtherTemplates.setData(nRow,"CENT_OWNER_ID",g_pClientManager->GetPersonID());
			if (bOverrideApplicationID)
				//if(lstTemplates.getDataRef(i,"BDMD_APPLICATION_ID").toInt()>0)
					lstWriteOtherTemplates.setData(nRow,"BDMD_APPLICATION_ID",lstTemplates.getDataRef(i,"BDMD_APPLICATION_ID"));
		}

	}

	nSkipped=lstTemplates.getSelectedCount();
	lstTemplates.deleteSelectedRows();

	//ALL OTHERS:
	if (lstWriteOtherTemplates.getRowCount()>0)
	{
		DbRecordSet empty;
		Status err;
		DbRecordSet lstRevisions;
		DbRecordSet lstUAR,lstGAR;
		_SERVER_CALL(BusDocuments->Write(err,lstWriteOtherTemplates,s_lstProjectsDef,s_lstContactsDef,empty,lstRevisions,"",lstUAR,lstGAR))
		_CHK_ERR_NO_RET(err);
	}

	//LOCAL:
	if (lstTemplatesURL_Local.size()>0)
	{
		//prepare for write/create revision:
		DbRecordSet lstPaths;
		DbRecordSet lstApps;
		DbRecordSet lstWrite;
		DbRecordSet lstRev;
		DataHelper::ParseFilesFromURLs(lstTemplatesURL_Local,lstPaths,lstApps,true,false);
		RegisterDocumentPaths(lstPaths,lstTemplatesURL_Local,1,"",true,&lstWrite,&lstRev,GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE);

		int nSize=lstWrite.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nRow=lstTemplates.find("FILE_PATH",lstWrite.getDataRef(i,"BDMD_DOC_PATH").toString(),true);
			if (nRow>=0)
			{
				lstWrite.setData(i,"BDMD_NAME",lstTemplates.getDataRef(nRow,"BDMD_NAME").toString());
				//lstWrite.setData(i,"CENT_IS_PRIVATE",lstTemplates.getDataRef(nRow,"CENT_IS_PRIVATE").toInt());
				lstWrite.setData(i,"BDMD_DESCRIPTION",lstTemplates.getDataRef(nRow,"BDMD_DESCRIPTION").toString());
				if (bOverrideApplicationID)
					//if(lstTemplates.getDataRef(nRow,"BDMD_APPLICATION_ID").toInt()>0)
						lstWrite.setData(i,"BDMD_APPLICATION_ID",lstTemplates.getDataRef(nRow,"BDMD_APPLICATION_ID"));

			}
		}

		if(WriteDocument(lstWrite,lstRev))
		{
			nDone+=lstWrite.getRowCount();
		}
		//return ID back:
		nSize=lstWrite.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nRow=lstTemplates.find("FILE_PATH",lstWrite.getDataRef(i,"BDMD_DOC_PATH").toString(),true);
			if (nRow>=0)
			{
				lstTemplates.setData(nRow,"BDMD_ID",lstWrite.getDataRef(i,"BDMD_ID").toInt());
			}
		}
		
	}

	//INTERNET:
	if (lstTemplatesURL_Internet.size()>0)
	{
		//prepare for write/create revision:
		DbRecordSet lstPaths;
		DbRecordSet lstApps;
		DbRecordSet lstWrite;
		DbRecordSet lstRev;
		DataHelper::ParseFilesFromURLs(lstTemplatesURL_Internet,lstPaths,lstApps,true,false);
		RegisterDocumentPaths(lstPaths,lstTemplatesURL_Internet,1,"",true,&lstWrite,&lstRev,GlobalConstants::DOC_TYPE_INTERNET_FILE);
		int nSize=lstWrite.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nRow=lstTemplates.find("FILE_PATH",lstWrite.getDataRef(i,"BDMD_DOC_PATH").toString(),true);
			if (nRow>=0)
			{
				lstWrite.setData(i,"BDMD_NAME",lstTemplates.getDataRef(nRow,"BDMD_NAME").toString());
				//lstWrite.setData(i,"CENT_IS_PRIVATE",lstTemplates.getDataRef(nRow,"CENT_IS_PRIVATE").toInt());
				lstWrite.setData(i,"BDMD_DESCRIPTION",lstTemplates.getDataRef(nRow,"BDMD_DESCRIPTION").toString());
				if (bOverrideApplicationID)
					//if(lstTemplates.getDataRef(nRow,"BDMD_APPLICATION_ID").toInt()>0)
						lstWrite.setData(i,"BDMD_APPLICATION_ID",lstTemplates.getDataRef(nRow,"BDMD_APPLICATION_ID"));
			}
		}
		if(WriteDocument(lstWrite,lstRev))
		{
			nDone+=lstWrite.getRowCount();
		}

		//return ID back:
		nSize=lstWrite.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nRow=lstTemplates.find("FILE_PATH",lstWrite.getDataRef(i,"BDMD_DOC_PATH").toString(),true);
			if (nRow>=0)
			{
				lstTemplates.setData(nRow,"BDMD_ID",lstWrite.getDataRef(i,"BDMD_ID").toInt());
			}
		}

	}



	//lstTemplates in CENT format...ready


}

void DocumentHelper::RegisterInterfacesFromRFFile(DbRecordSet &lstInterfaces, QString strRFFileDirectory, int &nDone, int &nSkipped)
{
	nDone=0;
	nSkipped=0;

	int nCnt = lstInterfaces.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		//store Skype and Thunderbird settings in .ini file
		QString strApp = lstInterfaces.getDataRef(i, "INTERFACE_NAME").toString();
		if(strApp == "Skype")
		{
			QString strAppPath = lstInterfaces.getDataRef(i, "APP_PATH").toString();
			g_pClientManager->GetIniFile()->m_strPortableSkypePath = strAppPath;
		}
		else if(strApp == "Thunderbird")
		{
			QString strDataPath = lstInterfaces.getDataRef(i, "DATA_PATH").toString();
			g_pClientManager->GetIniFile()->m_strPortableThunderbirdProfile = strDataPath;
		}
	}
}


QString DocumentHelper::AskForRevisionTag(int nDocumentID,QString strPath, int nAskForTag)
{
	QString strTag;
	if (nAskForTag==-1)
	{
		DbRecordSet lstData;
		GetCheckedOutDocs(lstData);
		int nRow=lstData.find("BDMD_ID",nDocumentID,true);
		if (nRow>=0)
		{
			nAskForTag=lstData.getDataRef(nRow,"BDMD_SET_TAG_FLAG").toInt();
		}
		else
			nAskForTag=0;
	}

	if (nAskForTag>0)
	{
		QFileInfo info(strPath);
		strTag=QInputDialog::getText(NULL,tr("Revision TAG"),tr("Set Revision TAG for file: ")+info.fileName());
	}

	return strTag;
}


QString DocumentHelper::ResolvePathBeforeCheckOutDocument(Status &err,int nDocumentID,QString strFilePath,int &nFileSize,bool bAskForLocation,QString strDefaultDirPath, int nRevisionID)
{
	err.setError(0);

	//strFilePath last check in path:
	QFileInfo info(strFilePath);
	QString strFile=info.fileName();

	QString strServerFileName;
	_SERVER_CALL(BusDocuments->GetDocumentFileNameAndSize(err,nDocumentID,strServerFileName,nFileSize,nRevisionID))
	if (!err.IsOK())
	{
		return "";
	}

	//if empty, use serverfilename or abort if both empty...
	if (strFile.isEmpty())
	{
		if (strServerFileName.isEmpty())
		{
			QMessageBox::warning(NULL,tr("Warning"),tr("Check out not possible: Please check-in a document using 'Edit' before you can open it!"));
			return "";
		}
		else
			strFile=strServerFileName;
	}


	QString strPath;
	if (strDefaultDirPath.isEmpty()) //issue external caller can enforce 1 path: make check out there
	{
		if (bAskForLocation)
		{
			//ask user to manually enter path, if not cancel check out
			strPath=DocumentHelper::GetCheckOutDocumentSavePath(strFile);
		}
		else
		{
			//find check out directory+path:
			strPath=DocumentHelper::GetTempDirectoryForAutomaticStorage(); //ATS mechanism
			if (strPath.isEmpty())
			{
				//ask user to manually enter path, if not cancel check out
				int nResult=QMessageBox::question(NULL,tr("Warning"),tr("Automated Temporary Storage mechanism failed to determine Document check out location. Choose path for check-out location?"),tr("Yes"),tr("No"));
				if (nResult==0) 
					strPath=DocumentHelper::GetCheckOutDocumentSavePath(strFile);
				else
					return "";
			}
			else
			{
				strPath=strPath+"/"+strFile;
			}

		}
		strPath=QDir::cleanPath(strPath);
	}
	else
	{
		strPath=QDir::cleanPath(strDefaultDirPath+"/"+strFile);
		//issue 1883: if already exists: ask user:
		if (QFile::exists(strPath))
		{
			int nResult=QMessageBox::question(NULL,tr("Warning"),QString(tr("File %1 exists. Overwrite?")).arg(QDir::toNativeSeparators(strPath)),tr("Yes"),tr("No"));
			if (nResult==1) 
				return strPath;
			else if (nResult!=0)
				return "";
		}
	}

	return strPath;
}

//deletes document and fires cache signals...no questions ask
void DocumentHelper::DeleteDocument(Status &err,int nDocumentID,QString strLockRes)
{
	_SERVER_CALL(BusDocuments->DeleteDocument(err,nDocumentID,strLockRes));
	if (!err.IsOK())
		 return;

	DbRecordSet *data=g_ClientCache.GetCache(ENTITY_BUS_DM_DOCUMENTS);
	if (data)
	{
		int nRow=data->find(0,nDocumentID,true);
		if(nRow!=-1)
		{

			DbRecordSet rowDeleted=data->getRow(nRow);
			g_ClientCache.ModifyCache(nDocumentID,rowDeleted,DataHelper::OP_REMOVE,NULL);  //fire global signal
			return;
		}
	}

	//if not in cache, just fire signal:
	DbRecordSet rowDeleted;
	rowDeleted.addColumn(QVariant::Int,"BDMD_ID");
	rowDeleted.addRow();
	rowDeleted.setData(0,0,nDocumentID);
	QVariant varData;
	qVariantSetValue(varData,rowDeleted);
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED,ENTITY_BUS_DM_DOCUMENTS,varData,NULL); 

}

bool DocumentHelper::TestForOfficeTemplateFiles(QString strPath)
{
	QFileInfo info(strPath);
	QString strExt=info.completeSuffix();
	if (strExt=="dot" || strExt=="dotx" || strExt=="dotm" || strExt=="xlt" || strExt=="xltx" || strExt=="xltm" || strExt=="pot" || strExt=="potx" || strExt=="potm")
	{
		QMessageBox::warning(NULL,tr("Warning"),tr("You try to register a Microsoft(R) Office(R) template document. Office(R) templates are not valid to be registered as SOKRATES(R) templates: Please open the template in the Office(R) application and store it as a \"Document\" (with extension doc,docx,xls,xlsx,ppt,pptx...), not as a \"Template\". You can register this new document in SOKRATES(R) Communicator as a template afterwords!"));
		return false;
	}

	return true;
}