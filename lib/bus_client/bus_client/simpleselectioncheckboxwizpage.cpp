#include "simpleselectioncheckboxwizpage.h"
#include "gui_core/gui_core/wizardbase.h"
#include <QHash>


SimpleSelectionCheckBoxWizPage::SimpleSelectionCheckBoxWizPage(int nWizardPageID, QString strPageTitle, QWidget *parent)
    : WizardPage(nWizardPageID, strPageTitle)
{
	m_strLabel = strPageTitle;

	m_recResultRecordSet.destroy();
	m_recResultRecordSet.addColumn(QVariant::Bool, "CheckState");
	m_recResultRecordSet.addRow();

}

SimpleSelectionCheckBoxWizPage::~SimpleSelectionCheckBoxWizPage()
{
}

void SimpleSelectionCheckBoxWizPage::Initialize()
{
	if (m_bInitialized)
		return;
	ui.setupUi(this);
	ui.checkBox->setText(m_strLabel);
	m_bInitialized = true;
	CompleteChanged();
}

void SimpleSelectionCheckBoxWizPage::CompleteChanged()
{
	m_bComplete = true;
	emit completeStateChanged();
}

bool SimpleSelectionCheckBoxWizPage::GetPageResult(DbRecordSet &RecordSet)
{
	if (!m_bInitialized)
		return false;
	bool bState = (ui.checkBox->checkState() == Qt::Checked);
	m_recResultRecordSet.setData(0,0,bState);
	RecordSet = m_recResultRecordSet;
	return true;
}
