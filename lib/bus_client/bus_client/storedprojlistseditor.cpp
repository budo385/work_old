#include "storedprojlistseditor.h"
#include <QMessageBox>
#include <QProgressDialog>
#include <QApplication>
#include <QDesktopWidget>
#include "gui_core/gui_core/macros.h"
#include "bus_client/bus_client/selection_treebased.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "bus_client/bus_client/simpleselectionwizpage.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbtableiddefinition.h"

#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;

StoredProjListsEditor::StoredProjListsEditor(int nListID, QWidget *parent) 
	: QDialog(parent), 
	m_bGetDescOnly(true),
	m_bIsSelection(false),
	m_bIsFilter(false)
{
	ui.setupUi(this);
	m_nListID = nListID;

	ui.radTypeCollection->hide(); // TOFIX hide collections until implemented	
	ui.cboStoredProjLists->setDisabled(true); 

	// Initialize pages
	m_pPageGroup = new SimpleSelectionWizPage(WIZARD_PROJECT_STORED_LIST_SELECTION_PAGE, QString(QObject::tr("Select Projects for Stored List")), ENTITY_BUS_PROJECT);
	m_pPageGroup->SetGlobalCache(&g_ClientCache);
	m_pPageGroup->Initialize();
	m_pPageSelections = new CustomSelectionsWnd();
	m_pPageCollections = new CustomCollectionsWnd();
	ui.stackedWidget->addWidget(m_pPageGroup);
	ui.stackedWidget->addWidget(m_pPageSelections);
	ui.stackedWidget->addWidget(m_pPageCollections);

	//--------------------------------
	// load data 
	//--------------------------------
	if (m_nListID>0)
	{
		Status status;
		_SERVER_CALL(StoredProjectLists->GetListData(status, m_nListID, m_lstGroupData, m_bIsSelection, m_strSelectionXML, m_bIsFilter, m_strFilterXML, m_bGetDescOnly));
		_CHK_ERR(status);
		ui.uarWidget->Load(m_nListID, BUS_STORED_PROJECT_LIST);
	}
	
	m_pUarWidget=ui.uarWidget;


	//----------------------------------------------------------------
	// Fill Project Lists (combo) and set current
	//----------------------------------------------------------------
	DbRecordSet recLists;
	Status status;
	_SERVER_CALL(StoredProjectLists->GetListNames(status, recLists)) //Project grid id (,g_pClientManager->GetPersonID())
	_CHK_ERR(status);

	int nSelectedListIdx = -1;

	//MR: issue #2532 (select one of the lists based on access rights)
	int nDefListID = g_pClientManager->GetPersonProjectListID();
	bool bAccPublicModify = g_AccessRight->TestAR(ENTER_MODIFY_PUBLIC_PROJECT_LISTS);
	bool bAccPrivateModify = g_AccessRight->TestAR(ENTER_MODIFY_PRIVATE_PROJECT_LISTS);

	//PM
	if(m_nListID != -1 ){
		nDefListID = m_nListID;
	}

	// Fill Project Lists (combo)
	int nRowCount = recLists.getRowCount();
	if(nRowCount > 0)
	{
		int nSelectIdx = 0;
		for(int i = 0; i < nRowCount; i++)
		{
			int ViewID = recLists.getDataRef(i, "BSPL_ID").toInt();
			QString strViewName = recLists.getDataRef(i, "BSPL_NAME").toString();
			ui.cboStoredProjLists->addItem(strViewName, ViewID);

			//check if we need to select this view on dialog startup
			if(nSelectedListIdx<0 && bAccPublicModify && bAccPrivateModify){    // !bAccPublicModify 
				if(ViewID == nDefListID){
					nSelectedListIdx = i;
				}
			}
		}
		//check if we need to select first initial list
		if(nSelectedListIdx<0 && bAccPublicModify && bAccPrivateModify){	//!bAccPublicModif
			if(nRowCount > 0){
				nSelectedListIdx = 0;
			}
		}

		ui.cboStoredProjLists->setCurrentIndex(nSelectedListIdx);
	}

	//----------------------------------------------------------------
	// Make connections and load data 2 GUI if needed
	//----------------------------------------------------------------
	
	// connections
	connect(ui.btnCancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(ui.btnOK, SIGNAL(clicked()), this, SLOT(accept()));
	//BT: disabled always... //connect(ui.cboStoredProjLists, SIGNAL(currentIndexChanged(int)), this, SLOT(listNamesCombo_selectionChanged(int)));

	ClearPages();
	DisplayPageByListType(); //this loads data 2 GUI

	//BT: disable list type switch between group & selection when EDIT
	if (nListID>0)
	{
		ui.groupBoxType->setDisabled(true);
	}
}

StoredProjListsEditor::~StoredProjListsEditor()
{
}

void StoredProjListsEditor::DisableNameEdit()
{
	ui.txtProjectListName->setEnabled(false);
}

void StoredProjListsEditor::setFirstPage()
{
	// TOFIX OLD CODE!!
	//connect(m_pPage, SIGNAL(completeStateChanged()), this, SLOT(completeStateChanged()));
	//if (!m_pPage->IsInitialized())m_pPage->Initialize(); //B.T. impruvd
	//((Selection_TreeBased *)((SimpleSelectionWizPage *)m_pPage)->GetSelectorWidget())->HideProjectToolBar();
	// setWindowTitle(m_pPage->GetPageTitle());

	// TOFIX WTF IS THIS?
	//First disable and then enable buttons if they read something from cache.
	/*
	// 	DisableButtons();
	// 	EnableButtons();
	*/
}

void StoredProjListsEditor::completeStateChanged()
{
	DisableButtons();
	EnableButtons();
}

void StoredProjListsEditor::DisableButtons()
{
}

void StoredProjListsEditor::EnableButtons()
{
	// TOFIX OLD CODE!!
	//	finishButton->setEnabled(m_hshWizardPageOrder.value(m_hshWizardPageOrder.count()-1)->isComplete());
}

void StoredProjListsEditor::ClearPageHashFromThisPageUp(int nThisPageNumber)
{
}

void StoredProjListsEditor::SetMainLayoutSize(QSize szSize)
{
	QLayoutItem *item = mainLayout->itemAt(0);
	QRect rect(0, 0, szSize.width(), szSize.height());
	item->setGeometry(rect);
}

void StoredProjListsEditor::GetWizPageRecordSetList(QList<DbRecordSet> &RecordSetList)
{
	RecordSetList = m_lstPageRecordSet;
}

void StoredProjListsEditor::accept()
{
	if(GetListName().isEmpty()){
		QMessageBox::information(NULL, "", tr("List name must not be empty!"));
		return;
	}
	DbRecordSet lstData;

	// handle SPL type
	m_bIsSelection	= false;
	m_bIsFilter		= false;
	m_bGetDescOnly	= false;

	// calculate active page where OK was clicked
	int nPageIdx = ui.stackedWidget->currentIndex();
	if(nPageIdx == PAGE_GROUP_IDX)
	{
		DbRecordSet rec;
		bool bOK=m_pPageGroup->GetPageResult(rec);
		if (!bOK)
		{
			done(QDialog::Rejected);
			return;
		}
		//rec.Dump();
		m_lstPageRecordSet << rec;

		if(m_nListID != -1)
		{
			GetTreeData(lstData); //get list name/contents
		}
		//lstData.Dump();
		m_nProjlistType = PAGE_GROUP_IDX;
		// MP comment: lstData.setColValue("BSPL_OWNER",g_pClientManager->GetPersonID()); //set to id of logged person;
	}
	else if(nPageIdx == PAGE_SELECTIONS_IDX)
	{
		m_bIsSelection = true;
		m_strSelectionXML = m_pPageSelections->GetSelectionXML(); // for new lists fetching XML is done from parent
		m_strProjlistXML = m_strSelectionXML; // for parent to get XML
		m_nProjlistType = PAGE_SELECTIONS_IDX;
	}
	else if(nPageIdx == PAGE_COLLECTIONS_IDX)
	{
		m_bIsFilter = true;
		m_nProjlistType = PAGE_COLLECTIONS_IDX;
		// TOFIX: m_strFilterXML = m_pPageCollections->GetCollectionXML();
	}
	
	// when creating a new list, ListID is -1 and handled from parent class
	if(m_nListID != -1)
	{
		// server call
		QApplication::setOverrideCursor(Qt::WaitCursor);
		Status status;
		_SERVER_CALL( StoredProjectLists->SetListData(status, m_nListID, lstData, m_bIsSelection, m_strSelectionXML, m_bIsFilter, m_strFilterXML));
		QApplication::restoreOverrideCursor();
		_CHK_ERR_NO_RET(status);

		// save uarwidget
		ui.uarWidget->Save(m_nListID, BUS_STORED_PROJECT_LIST); 
	}
	


	
	// trenutna aktivna stranica mora javit dali smis ili nesmis zatvorit taj dialog 
	// (npr: ako je lista prazna ili nesto slicno, ogranicenja treba vidit)

/*  TOFIX
// 	bool bAccept = true;
// 	DbRecordSet rec;
// 		bool bOK=m_pPage->GetPageResult(rec);
// 		if (!bOK)
// 			bAccept=false;
// 		m_lstPageRecordSet << rec;
*/

	//if some page did not succeed in retrieving data, return error
	//NEW code:
/*
// 	if (bAccept)
// 		done(QDialog::Accepted);
// 	else
// 	{
// 		done(QDialog::Rejected);
// 	}
*/
	done(QDialog::Accepted);
}

QString	StoredProjListsEditor::GetListName()
{
	return ui.txtProjectListName->text();
}

void StoredProjListsEditor::SetListName(QString strName)
{
	ui.txtProjectListName->setText(strName);
}

void StoredProjListsEditor::GetTreeData(DbRecordSet &lstData)
{
	QList<DbRecordSet> RecordSetList;
	GetWizPageRecordSetList(RecordSetList);	// Get wizard recordset list.
	
	if (RecordSetList.size()==0)
		return;

	//filter the list to remove invisible
	DbRecordSet &lstTmp = RecordSetList[0]; //TOFIX CRASH!!
	HierarchicalHelper::RemoveInvisibleTreeNodes(lstTmp, "BUSP_CODE", "BUSP_EXPANDED");

	//delete everything except ID and expanded column!
	lstData.addColumn(QVariant::Int, "BUSP_ID");
	lstData.addColumn(QVariant::Int, "BUSP_EXPANDED");
	lstData.merge(lstTmp);

#ifdef _DEBUG
	//lstData.Dump();
#endif
}

/*
void StoredProjListsEditor::listNamesCombo_selectionChanged(int nIdx)
{
	//load selected project stored list at the left side tree
	if(nIdx >= 0)
	{
		int nListID = ui.cboStoredProjLists->itemData(nIdx).toInt();
		m_nListID = nListID;

		if(m_bIsSelection) 
		{
			m_pPageSelections->LoadSelectionXML(m_strSelectionXML);
			ui.stackedWidget->setCurrentIndex(PAGE_SELECTIONS_IDX);
			ui.radTypeSelection->setChecked(true);
		}
	}
}
*/

void StoredProjListsEditor::on_radTypeGroup_toggled(bool bOn)
{
	if(bOn){
		qDebug() << "Switch to group SPL type";
		ui.stackedWidget->setCurrentIndex(PAGE_GROUP_IDX);
	}
}	

void StoredProjListsEditor::on_radTypeSelection_toggled(bool bOn)
{
	if(bOn){
		qDebug() << "Switch to selection SPL type";
		ui.stackedWidget->setCurrentIndex(PAGE_SELECTIONS_IDX);
	}
}

void StoredProjListsEditor::on_radTypeCollection_toggled(bool bOn)
{
	if(bOn){
		qDebug() << "Switch to collection SPL type";
		ui.stackedWidget->setCurrentIndex(PAGE_COLLECTIONS_IDX);
	}
}

//BT: load data, show it and switch to right pane!!!
void StoredProjListsEditor::DisplayPageByListType()
{
	if(m_bIsSelection) 
	{ // selection type 

		m_pPageSelections->LoadSelectionXML(m_strSelectionXML);
		ui.stackedWidget->setCurrentIndex(PAGE_SELECTIONS_IDX);
		ui.radTypeSelection->setChecked(true);
	}
	else if(m_bIsFilter) 
	{ // collection type
	
		//Filters are not implemented!!!
		Q_ASSERT(false);

		/* TOFIX: currently never go to collection until implemented! */
		//ui.stackedWidget->setCurrentIndex(PAGE_COLLECTIONS_IDX);
		//ui.radTypeCollection->setChecked(true);
		//m_pPageSelections->LoadSelectionXML(m_strSelectionXML);
		ui.stackedWidget->setCurrentIndex(PAGE_SELECTIONS_IDX);
		ui.radTypeSelection->setChecked(true);
	}
	else 
	{ // group type
	
		if (m_lstGroupData.getRowCount()>0) //only if data exists
		{
			// fill right tree
			DbRecordSet lstDataFmt;
			lstDataFmt.copyDefinition(*(((UniversalTreeWidget *)m_pPageGroup->GetSelectedWidget())->GetDataSource()));
			if(lstDataFmt.getColumnIdx("BUSP_EXPANDED") < 0){
				lstDataFmt.addColumn(QVariant::Int, "BUSP_EXPANDED");
			}
			lstDataFmt.merge(m_lstGroupData);

			// TVIEW_BUS_PROJECT_SELECT
			// set the loaded data into the selected tree
			*(((UniversalTreeWidget *)m_pPageGroup->GetSelectedWidget())->GetDataSource()) = lstDataFmt;
			//*(((UniversalTreeWidget *)m_pPageGroup->GetSelectedWidget())->GetDataSource()) = m_lstGroupData;
			((UniversalTreeWidget *)m_pPageGroup->GetSelectedWidget())->RefreshDisplay();
		}

		
		ui.stackedWidget->setCurrentIndex(PAGE_GROUP_IDX);
		ui.radTypeGroup->setChecked(true);
	}
}

void StoredProjListsEditor::ClearPages()
{
	//TOFIX 
	m_pPageSelections->ClearPage();
	// ...
}

