#include "dlg_copyentity.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"

Dlg_CopyEntity::Dlg_CopyEntity(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	//setWindowTitle(tr("Select Groups For Copy"));
	ui.lstGroups->setDragEnabled(true);
	ui.lstGroups->setDropIndicatorShown(true);
	ui.lstGroups->setSelectionMode(QAbstractItemView::ExtendedSelection);

	//set acceptable types:
	m_lstGroupsSelected.addColumn(QVariant::Int,"GROUP_ID");
	m_lstGroupsSelected.addColumn(QVariant::String,"GROUP_NAME");

	//set acceptable types:
	ui.tableWidget->AddAcceptableDropTypes(ENTITY_DROP_TYPE_LIST_ITEM);
	ui.tableWidget->Initialize(&m_lstGroupsSelected,true);
	DbRecordSet columns;
	ui.tableWidget->AddColumnToSetup(columns,"GROUP_NAME",tr("Group Name"),120,false, "",UniversalTableWidget::COL_TYPE_TEXT);
	ui.tableWidget->SetColumnSetup(columns);
	ui.tableWidget->SetEditMode(true); //drag enable

}


Dlg_CopyEntity::~Dlg_CopyEntity()
{

}


void Dlg_CopyEntity::Initialize(DbRecordSet lstGroups)
{
	ui.lstGroups->SetData(lstGroups,1); //display col 1

	
	//B.T. issue 2574: remove group contact from default data:
	int nRow=lstGroups.find(0,(int)ContactTypeManager::GROUP_CONTACT_GROUP_ASSIGN,true);
	if (nRow>=0)
	{
		lstGroups.deleteRow(nRow);
	}
	

	m_lstGroupsSelected.merge(lstGroups);
	ui.tableWidget->RefreshDisplay();


}


void Dlg_CopyEntity::GetSelectedGroups(DbRecordSet &lstGroups)
{
	lstGroups=m_lstGroupsSelected;
}


void Dlg_CopyEntity::on_btnCancel_clicked()
{

	done(0);
}


void Dlg_CopyEntity::on_btnOK_clicked()
{
	done(1); 
}


void Dlg_CopyEntity::on_btnAdd_clicked()
{
	DbRecordSet lstData;
	ui.lstGroups->GetDropValue(lstData);
	m_lstGroupsSelected.merge(lstData);
	ui.tableWidget->RefreshDisplay();
}

void Dlg_CopyEntity::on_btnRemove_clicked()
{
	ui.tableWidget->DeleteSelection();
}
