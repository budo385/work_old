#include "bus_client/bus_client/selection_combobox.h"



Selection_ComboBox::Selection_ComboBox()
:m_pCombo(NULL),m_bShowDistinct(false)
{

}

Selection_ComboBox::~Selection_ComboBox()
{

}


//WARNING: first call setupcombo or setupcomboingrid then set filter then initialize

//if skip loading, then data will not loaded
void Selection_ComboBox::Initialize(int nEntityID)
{
	MainEntitySelectionController::Initialize(nEntityID);	//init data source
	ReloadData();											//reload data
}

//must be called after Initialize
void Selection_ComboBox::SetDataForCombo(QComboBox* pCombo,QString strCmbDisplay,QString strCmbMapFld,QString strDataMapFldTo,GuiDataManipulator *plstData, GuiField *pField)
{
	m_pCombo=pCombo;
	m_plstData=plstData;
	m_pField=pField;
	m_nCmbMapFldIdx=m_lstData.getColumnIdx(strCmbMapFld);
	m_nCmbDisplayIdx=m_lstData.getColumnIdx(strCmbDisplay);
	if(m_plstData!=NULL)
		m_nDataMapFldToIdx=plstData->GetDataSource()->getColumnIdx(strDataMapFldTo);

	//Q_ASSERT(m_nCmbMapFldIdx);
	Q_ASSERT(m_nCmbDisplayIdx!=-1);
	//Q_ASSERT(m_nDataMapFldToIdx);

	//connect signals:
	connect(m_pCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(on_cmbName_currentIndexChanged(int)));
}




//refresh content in widget if new data arrived
void Selection_ComboBox::RefreshDisplay(int nAction, int nRowInsideDataSource)
{
	//m_lstData.Dump();

	//refresh display on single combo
	if(m_pCombo!=NULL)
	{
		QString strDisplayVal=m_pCombo->currentText();

		//disconnect signals:
		m_pCombo->blockSignals(true);

		m_pCombo->clear();

		//add data into combo:
		if (m_bShowDistinct) //if distinct, sort it, show only different.
		{
			//m_lstData.Dump();
			m_lstData.sort(m_nCmbDisplayIdx);
			//m_lstData.Dump();
			QString strPrev;
			int nSize=m_lstData.getRowCount();
			for(int i=0;i<nSize;++i)
			{
				QString strVal=m_lstData.getDataRef(i,m_nCmbDisplayIdx).toString();
				if (strPrev!=strVal)
				{
					m_pCombo->addItem(strVal);
					strPrev=strVal;
				}
			}
		}
		else
		{
			int nSize=m_lstData.getRowCount();
			for(int i=0;i<nSize;++i)
			{
				m_pCombo->addItem(m_lstData.getDataRef(i,m_nCmbDisplayIdx).toString());
			}
		}

		//if connected
		if(m_plstData!=NULL)
		{
			//restore back current index, only if datasource contains one row
			m_lstData.clearSelection();
			if(m_plstData->GetDataSource()->getRowCount()==1)
			{
				QVariant valueToMap=m_plstData->GetDataSource()->getDataRef(0,m_nDataMapFldToIdx);  //from row 0, when combo
				int nCurrentIndex=m_lstData.find(m_nCmbMapFldIdx,valueToMap,true);
				m_pCombo->setCurrentIndex(nCurrentIndex);
			}
		}
		else //if not just same old index:
		{
			int nIndex=m_pCombo->findText(strDisplayVal);
			if(nIndex==-1 && m_lstData.getRowCount()!=0) //set on first
				m_pCombo->setCurrentIndex(0);
			else
				m_pCombo->setCurrentIndex(nIndex);

		}

		//connect signals:
		m_pCombo->blockSignals(false);
	}


}



//if combo registered, change it:
void Selection_ComboBox::on_cmbName_currentIndexChanged(int index)
{

	//only if combo is connected
	if(m_plstData!=NULL)
	{
		//get value from mapped col, assign to datasource:
		QVariant valueToMap=m_lstData.getDataRef(index,m_nCmbMapFldIdx);
		m_plstData->ChangeData(0,m_nDataMapFldToIdx,valueToMap);
		if(m_pField!=NULL)m_pField->RefreshDisplay();
	}
}




//only works for registered combo's;
// -1 for invalid selection or none
int Selection_ComboBox::GetSelection(DbRecordSet &selectedRow)
{
	//find by name:
	if (m_bShowDistinct)
	{
		int nRow=m_lstData.find(m_nCmbDisplayIdx,m_pCombo->currentText(),true);
		if (nRow==-1)
			return -1;
		else
		{
			selectedRow=m_lstData.getRow(nRow);
			return m_pCombo->currentIndex();
		}
	}
	else
	{
		if(m_pCombo==NULL) return -1;
		int nIndex=m_pCombo->currentIndex();
		if(nIndex>=0)
			selectedRow=m_lstData.getRow(nIndex);
		return nIndex;
	}

}

int Selection_ComboBox::GetSelectedID()
{
	DbRecordSet row;
	int nSel=GetSelection(row);
	if (nSel==-1) return -1;
	if (m_nPrimaryKeyColumnIdx==-1) return -1;
	
	return row.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
}

//sets current index based on name, if not found sets on first in list
void Selection_ComboBox::SetCurrentIndexFromName(QString strDisplayName)
{
	int nIndex=m_pCombo->findText(strDisplayName);
	if(nIndex==-1 && m_lstData.getRowCount()!=0) //set on first
		m_pCombo->setCurrentIndex(-1);
	else
		m_pCombo->setCurrentIndex(nIndex);

}

void Selection_ComboBox::SetCurrentIndexFromID(int nID)
{
	if (m_nPrimaryKeyColumnIdx==-1) return;
	int nRow=m_lstData.find(m_nPrimaryKeyColumnIdx,nID,true); //select it
	if (nRow!=-1)
		m_pCombo->setCurrentIndex(nRow);
	else
		m_pCombo->setCurrentIndex(-1); 
}

QString Selection_ComboBox::GetSelectedName()
{
	return m_pCombo->currentText();
}





//intialize: set id and name col to display in combo from mainentityselector
void Selection_ComboBoxEx::Initialize(int nEntityID,QString strDisplayCol,QString strIDColumn)
{
	MainEntitySelectionController::Initialize(nEntityID);//init data source

	if (!strDisplayCol.isEmpty())
		m_nNameColumnIdx=m_lstData.getColumnIdx(strDisplayCol);
	if (!strIDColumn.isEmpty())
		m_nPrimaryKeyColumnIdx=m_lstData.getColumnIdx(strIDColumn);

	Q_ASSERT(m_nNameColumnIdx!=-1);
	Q_ASSERT(m_nPrimaryKeyColumnIdx!=-1);
}



//combo reload: display name and user_value=id
void Selection_ComboBoxEx::ReloadCombo(QComboBox* pCombo,bool m_bShowDistinct)
{
	pCombo->blockSignals(true);
	pCombo->clear();

	//add data into combo:
	if (m_bShowDistinct) //if distinct, sort it, show only different.
	{
		DbRecordSet lstData=m_lstData;
		lstData.sort(m_nNameColumnIdx);
		lstData.removeDuplicates(m_nNameColumnIdx);
		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			pCombo->addItem(lstData.getDataRef(i,m_nNameColumnIdx).toString());
			pCombo->setItemData(i,lstData.getDataRef(i,m_nPrimaryKeyColumnIdx).toInt());
		}
	}
	else
	{
		int nSize=m_lstData.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			pCombo->addItem(m_lstData.getDataRef(i,m_nNameColumnIdx).toString());
			pCombo->setItemData(i,m_lstData.getDataRef(i,m_nPrimaryKeyColumnIdx).toInt());
		}
	}

	pCombo->blockSignals(false);
}

void Selection_ComboBoxEx::SetComboSelection(QComboBox* pCombo,QVariant varID)
{
	pCombo->blockSignals(true);
	if (varID.isNull())
		pCombo->setCurrentIndex(-1);
	else
		pCombo->setCurrentIndex(pCombo->findData(varID));
	pCombo->blockSignals(false);
}

