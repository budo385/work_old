#include "bus_client/bus_client/selection_contacts.h"
#include <QHeaderView>
#include <QtWidgets/QMessageBox>
#ifndef WINCE
	#include "dlg_contactbuildall.h"
#else
	#include "dlg_contactbuildall_embedded.h"
#endif  //WINCE
#include "gui_core/gui_core/universaltablewidget.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "gui_core/gui_core/gui_helper.h"
#include "dlg_copyentity.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include "common/common/datahelper.h"

#include "gui_core/gui_core/thememanager.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include "common/common/logger.h"
extern Logger g_Logger;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;				
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher
#include "gui_core/gui_core/universaltablefindwidget.h"
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

Selection_Contacts::Selection_Contacts(QWidget *parent)
    : QFrame(parent)
{ 
	ui.setupUi(this);

	QList<int> list;
	list<<130<<400;
	ui.splitter->setSizes(list);

	GUI_Helper::CreateStyledButton(ui.btnBuild,":Contacts_24_Mirror.png","",0,ThemeManager::GetCEMenuButtonStyleLighter(),0);
	ui.btnBuild->setToolTip(tr("Build Contacts"));

	GUI_Helper::CreateStyledButton(ui.btnOfflineSave,":filesave.png","",0,ThemeManager::GetCEMenuButtonStyleLighter(),0);
	ui.btnOfflineSave->setToolTip(tr("Save Contacts To Cache"));

#ifndef WINCE
	ui.btnOfflineSave->setVisible(false);
#endif

	m_pAct_CreateOrganizationFromPerson=NULL;
	m_pAct_Delete=NULL;
	m_pAct_FindDuplicates=NULL;
	m_pAct_CreateUserFromContact=NULL;

	connect(ui.tableWidget,SIGNAL(ContentChanged()),this,SLOT(OnTableContentChanged()));
}

Selection_Contacts::~Selection_Contacts()
{

}






void Selection_Contacts::Initialize(bool bSkipLoadingData,bool bIsFUISelectorWidget,bool bSingleClickIsSelection,bool bMultiSelection,bool bEnableDrag)
{

	m_bIsFUISelectorWidget=bIsFUISelectorWidget;			//if FUI selector then this flag can be used to enable some features (Insert node, edit node, etc..)
	m_bSingleClickIsSelection=bSingleClickIsSelection;		//set click mode

	MainEntitySelectionController::Initialize(ENTITY_BUS_CONTACT);	//init data source

	ui.frameGroups->SetActualListDataSource(&m_lstData);
	//BT: groups are not used in mobile??

#ifndef WINCE
	ui.frameGroups->Initialize(ENTITY_BUS_CONTACT,false,bIsFUISelectorWidget,true,true,true);
#else
	DisableGroupSelector();
#endif

	ui.frameGroups->registerObserver(this);
	
	//issue 1653: selection changed update on table cell before actual signal
	connect(ui.tableWidget,SIGNAL(SignalSelectionChanged()),this,SLOT(SlotSelectionChanged()),Qt::QueuedConnection);  //MB requested key navigation, will trigger event twice, but...
	connect(ui.tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(SlotSelectionChanged_DoubleClicked()),Qt::QueuedConnection);
	connect(ui.tableWidget,SIGNAL(cellClicked(int,int)),this,SLOT(SlotCellClicked(int,int)));


	QHeaderView* header = ui.tableWidget->horizontalHeader();
	header->setSectionResizeMode(QHeaderView::ResizeToContents);
//	header->setSectionResizeMode(1, QHeaderView::Stretch);

	DbRecordSet columns;
	GetColumnSetup(columns);
	ui.tableWidget->Initialize(&m_lstData,&columns,false,true,20);
	CreateContextMenu();

	//---------------------------------------- LOAD TABLE DATA --------------------------------//


	if (m_bIsFUISelectorWidget && g_pClientManager->IsLogged())
	{
		int nGroupIDForLoad=g_pSettings->GetPersonSetting(CONTACT_DEF_GROUP_FOR_LOAD).toInt();
		if (nGroupIDForLoad>=0)
		{
			DbRecordSet lstAllNodes;
			Status err;
			_SERVER_CALL(BusGroupTree->GetChildrenGroups(err,nGroupIDForLoad,lstAllNodes))
			_CHK_ERR_NO_RET(err);
			lstAllNodes.addRow();
			lstAllNodes.setData(lstAllNodes.getRowCount()-1,0,nGroupIDForLoad); //add parent on end
			ui.tableWidget->LoadDataFromGroup(nGroupIDForLoad,lstAllNodes);
		}
	}

	if (g_pClientManager->IsLogged())
	{
		//if load:
		if (m_bIsFUISelectorWidget && g_pSettings->GetPersonSetting(CONTACT_DEF_ACTUAL_LIST_FOR_LOAD).toBool())
		{
			bool bExclude;
			DbRecordSet lstFilterEmpty;
			BuildList(lstFilterEmpty,DataHelper::OP_ADD,false);  //reload all from server, include exclude groups
		}
		else
		{
			ReloadFromCache(); 	//only load from cache now, ignore parameters:
			//ui.tableWidget->SortList();
		}
	}
	else
		ReloadFromCache();

	//Find thing.
	ui.Find_widget->Initialize(ui.tableWidget,ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR,QString("QFrame "+ThemeManager::GetSidebar_Bkg()+" "+"QLabel "+ThemeManager::GetSidebarActualName_Font()));

	connect(ui.Find_widget->m_pFindCombo,SIGNAL(EnterPressed()),this,SLOT(OnFindWidgetEnterPressed()));

	OnOfflineModeChanged();
}







void Selection_Contacts::GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord)
{
	ui.tableWidget->GetSelection(lstEntityRecord);
	if(lstEntityRecord.getRowCount()>0)
	{
		nEntityRecordID=lstEntityRecord.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
		if(m_nCodeColumnIdx!=-1)strCode=lstEntityRecord.getDataRef(0,m_nCodeColumnIdx).toString();
		if(m_nNameColumnIdx!=-1)strName=lstEntityRecord.getDataRef(0,m_nNameColumnIdx).toString();
	}

}

void Selection_Contacts::GetSelection(DbRecordSet& lstRecords)
{
	ui.tableWidget->GetSelection(lstRecords);
}


//-----------------------------------------------------------------
//			SLOTS
//-----------------------------------------------------------------



//user selected cell with dbl click:
void Selection_Contacts::SlotSelectionChanged()
{
	DbRecordSet lstSelection;
	ui.tableWidget->GetSelection(lstSelection);
	if(lstSelection.getRowCount()>0)
	{
		int nRecordID=lstSelection.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID);
	}

}

void Selection_Contacts::SlotCellClicked(int nRow, int nCol)
{
	DbRecordSet lstSelection;
	ui.tableWidget->GetSelection(lstSelection);
	if(lstSelection.getRowCount()>0)
	{
		int nRecordID=lstSelection.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CLICKED,nRecordID);
	}
}

//refresh content in table widget:
void Selection_Contacts::RefreshDisplay(int nAction, int nPrimaryKeyValue)
{
	ui.tableWidget->SortList(); //because of special sorting, whole list is refreshed
}



//on Btn Build:
void Selection_Contacts::on_btnBuild_clicked()
{
#ifndef WINCE
	Dlg_ContactBuildAll dlg;
#else
	Dlg_ContactBuildAll_Embedded dlg;
	dlg.showFullScreen();
#endif  //WINCE

	if(dlg.exec())
	{
		//get filters
		DbRecordSet lstFilterData;
		int nOperation;
		bool bExclude;
		dlg.GetResult(lstFilterData,nOperation,bExclude);
		BuildList(lstFilterData,nOperation,bExclude);
	}
}


//do d'action & fire cache event
void Selection_Contacts::OnDeleteSelection()
{
	DbRecordSet data;
	ui.tableWidget->DeleteSelection();
	notifyObservers(SelectorSignals::SELECTOR_ON_CONTENT_CHANGE);
	FilterUserContacts();
}


//do d'action & fire cache event
void Selection_Contacts::OnDeleteTable()
{
	DbRecordSet data;
	ui.tableWidget->DeleteTable();
	notifyObservers(SelectorSignals::SELECTOR_ON_CONTENT_CHANGE);
	FilterUserContacts();
}


//remove duplos:
void Selection_Contacts::OnRemoveDuplicates()
{
	DbRecordSet data;
	//QTime time;
	//time.start();
	m_lstData.removeDuplicates(m_nPrimaryKeyColumnIdx);
	//qDebug()<<"bubui"<<time.elapsed();
	//time.restart();
	ui.tableWidget->RefreshDisplay();
	//qDebug()<<"bubui"<<time.elapsed();
}



void Selection_Contacts::OnDelete()
{
	DbRecordSet DataForDelete;
	DataForDelete.addColumn(QVariant::Int,"BCNT_ID");
	DataForDelete.merge(m_lstData,true);

	if (DataForDelete.getRowCount()>0)
	{
		Status err;

		int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete the selected contacts (%1) from the database?").arg(QVariant(DataForDelete.getRowCount()).toString()),tr("Yes"),tr("No"));
		if (nResult!=0) return; //only if YES

		//TEST AR:
		if(!g_AccessRight->IsOperationAllowed(err,BUS_CM_CONTACT,UserAccessRight::OP_DELETE,DataForDelete))
		{
			_CHK_ERR(err);
		}

		_SERVER_CALL(BusContact->DeleteContacts(err,DataForDelete,true))
		//error:
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			if (err.getErrorCode()==StatusCodeSet::ERR_BUS_ACESS_REFUSED_ON_DELETE) //reload:
			{
				//OnRefreshContent();
			}
			else
				return;
		}

		//refresh display:
		m_lstData.deleteSelectedRows();
		RefreshDisplay(); 
		
		//emit global:
		RefreshToGlobalCache(CACHE_UPDATE_DATA_DELETED,false,&DataForDelete);
		
		//emit special change signal for detailed list
		notifyObservers(SelectorSignals::SELECTOR_ON_CONTENT_CHANGE);
		FilterUserContacts();
	}


}


void Selection_Contacts::OnShowDetails()
{
	int nRow=m_lstData.getSelectedRow();
	if (nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BCNT_ID").toInt();
		//emit ShowContactDetails(nContactID);
		notifyObservers(SelectorSignals::SELECTOR_ON_SHOW_DETAILS,nContactID); 
	}

}


void Selection_Contacts::OnShowDetailsNewWindow()
{
	int nRow=m_lstData.getSelectedRow();
	if (nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BCNT_ID").toInt();
		//g_objFuiManager.OpenFUI(MENU_CONTACTS,true,false,FuiBase::MODE_READ,nContactID);
		//emit ShowContactDetails(nContactID);
		notifyObservers(SelectorSignals::SELECTOR_ON_SHOW_DETAILS_NEW_WINDOW,nContactID,m_nEntityID); 
	}

}



void Selection_Contacts::DisableGroupSelector()
{
	ui.frameGroups->setVisible(false);
}


void Selection_Contacts::BuildList(DbRecordSet lstFilterData, int nOperation,bool bExcludeGroup)
{

	Status err;
	//lstFilterData.Dump();

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	//load from server, send filter data to server...
	DbRecordSet lstNewData;
	_SERVER_CALL(MainEntitySelector->ReadData(err,ENTITY_BUS_CONTACT,lstFilterData,lstNewData))
	//error:
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		QApplication::restoreOverrideCursor();
		return;
	}
	else
	{
		//set into cache only if was empty:
		//DbRecordSet *dataCache=g_ClientCache.GetCache(ENTITY_BUS_CONTACT);
		//if(dataCache==NULL)
		//	g_ClientCache.SetCache(m_nEntityID,lstNewData);
		g_ClientCache.ModifyCache(ENTITY_BUS_CONTACT,lstNewData,DataHelper::OP_EDIT,this,true);

		//set new data:
		DataHelper::DataOperation(nOperation,&lstNewData,&m_lstData);

		//MB issue: 351
		m_lstData.removeDuplicates(0);

		//issue 1410:
		//load all contact from options and person groups, remove duplicates
		//remove them from list:
		//issue 1410---------------------------------------------------------------------------------------------
		if (!bExcludeGroup)
		{
			MainEntityFilter Filter;
			int nOptionExGroupID=g_pSettings->GetApplicationOption(APP_CONTACT_GROUP_EXCLUDE_ID).toInt();
			int nSettingExGroupID=g_pSettings->GetPersonSetting(CONTACT_GROUP_EXCLUDE_ID).toInt();
			if (nOptionExGroupID>0)
			{
				Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID="+QVariant(nOptionExGroupID).toString());
			}
			if (nSettingExGroupID>0)
			{
				Filter.ClearFilter();
				if (nOptionExGroupID>0)
					Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID IN ("+QVariant(nOptionExGroupID).toString()+","+QVariant(nSettingExGroupID).toString()+")");
				else
					Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID="+QVariant(nSettingExGroupID).toString());
			}
			DbRecordSet lstExFilter=Filter.Serialize();
			if (lstExFilter.getRowCount()>0)
			{
				DbRecordSet lstExcludeData;
				_SERVER_CALL(MainEntitySelector->ReadData(err,ENTITY_BUS_CONTACT,Filter.Serialize(),lstExcludeData))
					//error:
					if(!err.IsOK())
					{
						QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
						QApplication::restoreOverrideCursor();
						return;
					}
					else
					{
						lstExcludeData.removeDuplicates(0);
						DataHelper::DataOperation(DataHelper::OP_REMOVE,&lstExcludeData,&m_lstData);
					}
			}
		}


		//issue 1410---------------------------------------------------------------------------------------------
	

		//MB issue: 457: always resort whole list after:
		//QTime time;
		//time.start();
		ui.tableWidget->SortList(0);
		//qDebug()<<"contact build "<<time.elapsed();


		notifyObservers(SelectorSignals::SELECTOR_ON_CONTENT_CHANGE);
		FilterUserContacts();


		QApplication::restoreOverrideCursor();
	}
}

//select all persons selected
void Selection_Contacts::OnCopyPerson2Org()
{
	DbRecordSet lstSelected;
	ui.tableWidget->GetSelection(lstSelected);
	lstSelected.find("BCNT_TYPE",QVariant(ContactTypeManager::CM_TYPE_PERSON).toInt());
	lstSelected.deleteUnSelectedRows();
	if (lstSelected.getRowCount()==0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("No person contact selected!"));
		return;
	}
	DbRecordSet lstGroups;
	if(!ClientContactManager::SelectGroupsForCopyContact(lstGroups))
		return;
		
	Status err;
	DbRecordSet lstNewContacts;
	_SERVER_CALL(BusContact->CopyPerson2OrgContact(err, lstSelected,lstGroups,lstNewContacts));
	_CHK_ERR(err);

	m_lstData.merge(lstNewContacts);
	RefreshDisplay();
	QString strMsg=QVariant(lstNewContacts.getRowCount()).toString()+tr(" contact successfully copied!");
	QMessageBox::information(this,tr("Information"),strMsg);

}



void Selection_Contacts::OnFindDuplicates()
{

	DbRecordSet lstSelected;
	ui.tableWidget->GetSelection(lstSelected);

	DbRecordSet lstData;
	Status err;
	_SERVER_CALL(BusContact->FindDuplicates(err,lstSelected,lstData))
	_CHK_ERR(err);


	if (lstData.getRowCount()==0)
	{
		QMessageBox::information(this,tr("Information"),tr("No duplicate contact found!"));
		return;
	}

	//filter out duplicate entries: if doublets are selected
	//lstData.merge(m_lstData,true); //merge selected
	lstData.removeDuplicates(0);

	SortDataList lstSort;

	//asc
	lstSort<<SortData(lstData.getColumnIdx("BCNT_LASTNAME"),0);
	lstSort<<SortData(lstData.getColumnIdx("BCNT_FIRSTNAME"),0);
	lstSort<<SortData(lstData.getColumnIdx("BCNT_ORGANIZATIONNAME"),0);
	lstData.sortMulti(lstSort);

	//remove all contacts that exists inside:
	//issue: 811
	//qDebug()<<m_lstData.getSelectedCount();
	//m_lstData.deleteSelectedRows();
	//DataHelper::DataOperation(DataHelper::OP_REMOVE,&lstData,&m_lstData);issue 1271->clear all, only doublets left
	//lstData.selectAll();
	//qDebug()<<m_lstData.getSelectedCount();

//	m_lstData.clearSelection();
	//DbRecordSet lstTemp=m_lstData;
	m_lstData=lstData;
	//m_lstData.merge(lstTemp); issue 1271->clear all, only doublets left

	//qDebug()<<m_lstData.getSelectedCount();
	

	ui.tableWidget->RefreshDisplay();
	ui.tableWidget->blockSignals(true);
	ui.tableWidget->ScrollToFirstItem(false);
	ui.tableWidget->blockSignals(false);
	ui.tableWidget->RefreshSelection();


	notifyObservers(SelectorSignals::SELECTOR_ON_CONTENT_CHANGE);
	FilterUserContacts();
}

//select all children
void Selection_Contacts::SelectGroupContacts(int nGroupID)
{
	if (!g_pClientManager->IsLogged())
		return;
	
	//get children:
	Status err;
	DbRecordSet lstAllNodes;
	/*
	_SERVER_CALL(BusGroupTree->GetChildrenGroups(err,nGroupID,lstAllNodes))
	_CHK_ERR(err);

	QModelIndex empty;
	QDropEvent empty;
	ui.tableWidget->DropHandler(empty,ENTITY_BUS_GROUP_ITEMS,lstAllNodes,&empty);
	*/

	//read group tree:
	QString strSQL="WHERE BGIT_ID="+QVariant(nGroupID).toString();
	DbRecordSet lstRead;
	_SERVER_CALL(ClientSimpleORM->Read(err,BUS_GROUP_ITEMS,lstRead,strSQL))
	_CHK_ERR(err);
	int nTreeID=0;
	if (lstRead.getRowCount()>0)
		nTreeID=lstRead.getDataRef(0,"BGIT_TREE_ID").toInt();
	if (nTreeID<=0)
		return;


	//set tree, set group, clear & load into actual list:
	if(ui.frameGroups->SetSelectionOnGroup(nGroupID,nTreeID))
	{
		OnDeleteTable();
		DbRecordSet lstSelectedItems;
		ui.frameGroups->GetSelection(lstSelectedItems);
		ui.tableWidget->LoadDataFromGroup(nGroupID,lstSelectedItems);
	}

}

void Selection_Contacts::OnThemeChanged()
{
	ui.btnBuild->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	ui.tableWidget->setStyleSheet(ThemeManager::GetContactGridBkg());

}


void Selection_Contacts::SlotSelectionChanged_DoubleClicked()
{
	SlotSelectionChanged();
	int nRecordID=-1;
	DbRecordSet lstSelection;
	ui.tableWidget->GetSelection(lstSelection);
	if(lstSelection.getRowCount()>0)
	{
		nRecordID=lstSelection.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
	}
	notifyObservers(SelectorSignals::SELECTOR_ON_DOUBLE_CLICK,nRecordID);
}

void Selection_Contacts::SlotGroupSelection_DoubleClicked()
{
	//MB: when double click on grop replace contact list with group selection contacts
	MainEntityFilter Filter;
	int nGroupID;
	int nSelectedRow;
	nGroupID=ui.frameGroups->GetCurrentGroupID(nSelectedRow);
	if (nGroupID>0)
	{
		Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID="+QVariant(nGroupID).toString());
		DbRecordSet lstFilterData=Filter.Serialize();
		BuildList(lstFilterData,ClientContactManager::OP_REPLACE);
	}

}



//issue: 1251
void Selection_Contacts::SetSideBarVisible(bool bVisible)
{
	ui.frameGroups->blockSignals(true);
	ui.tableWidget->blockSignals(true);
	ui.frameGroups->setVisible(bVisible);
	//ui.btnBuild->setVisible(bVisible); //issue 1634
	ui.tableWidget->setVisible(bVisible);
	ui.frameGroups->blockSignals(false);
	ui.tableWidget->blockSignals(false);
	if (bVisible) //issue: 1579
		RefreshDisplay();

}

bool Selection_Contacts::IsSideBarVisible()
{
	return ui.tableWidget->isVisible();

}

void Selection_Contacts::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==ui.frameGroups && nMsgCode==SelectorSignals::SELECTOR_ON_DOUBLE_CLICK)
	{
		SlotGroupSelection_DoubleClicked();
		return;
	}
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		OnThemeChanged();
		return;
	}

	//issue 1739: in sidebar mode alter group content:
	//------------------------------------------
	DbRecordSet lstData=val.value<DbRecordSet>();
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR && pSubject==ui.frameGroups && lstData.getRowCount()>0)
	{
		if (nMsgCode<Selection_GroupTree::GROUP_ADD_SELECTED)
			return;

		DbRecordSet lstGroupContent;
		lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
		int nGroupIdx=lstGroupContent.getColumnIdx("BGCN_ITEM_ID");
		int nContactIdx=lstData.getColumnIdx("BCNT_ID");
		lstData.setColumn(nContactIdx,QVariant::Int,"BGCN_CONTACT_ID");
		Status err;

		switch(nMsgCode)
		{
		case Selection_GroupTree::GROUP_ADD_SELECTED:
		case Selection_GroupTree::GROUP_ADD_WHOLE_LIST:
			{
				lstGroupContent.merge(lstData);
				lstGroupContent.setColValue(nGroupIdx,nMsgDetail);
				//lstGroupContent.Dump();
				_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nMsgDetail,DataHelper::OP_ADD,lstGroupContent))
					if(!err.IsOK())
					{
						QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
						return;
					}

			}
			break;
		case Selection_GroupTree::GROUP_REMOVE_SELECTED:
			{
				lstGroupContent.merge(lstData);
				lstGroupContent.setColValue(nGroupIdx,nMsgDetail);
				_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nMsgDetail,DataHelper::OP_REMOVE,lstGroupContent))
					if(!err.IsOK())
					{
						QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
						return;
					}
			}
			break;
		case Selection_GroupTree::GROUP_REPLACE_SELECTED:
		case Selection_GroupTree::GROUP_REPLACE_WHOLE_LIST:
			{

				lstGroupContent.merge(lstData);
				lstGroupContent.setColValue(nGroupIdx,nMsgDetail);
				_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nMsgDetail,DataHelper::OP_REPLACE,lstGroupContent))
					if(!err.IsOK())
					{
						QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
						return;
					}
			}
			break;
		case Selection_GroupTree::GROUP_INTERSECT_SELECTED:
			{
				lstGroupContent.merge(lstData);
				lstGroupContent.setColValue(nGroupIdx,nMsgDetail);
				_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nMsgDetail,DataHelper::OP_INTERSECT,lstGroupContent))
					if(!err.IsOK())
					{
						QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
						return;
					}
			}
			break;
		default:
			//Q_ASSERT(false);
			break;
		}

		return;
	}





	MainEntitySelectionController::updateObserver(pSubject,nMsgCode,nMsgDetail,val);
}



/*
void Selection_Contacts::on_btnEmbededShowDetails_clicked()
{
	OnShowDetails();
}

void Selection_Contacts::on_btnEmbededShowGrid_clicked()
{
	int nRow=ui.tableWidget->GetCurrentSelectedRow();
	if (nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BCNT_ID").toInt();
		//emit ShowContactDetails(nContactID);
		notifyObservers(SelectorSignals::SELECTOR_ON_SHOW_COMM_GRID,nContactID); 
	}
}
*/


void Selection_Contacts::SetFocusOnFind()
{
	ui.Find_widget->m_pFindCombo->setFocus();
}


//disable server calls if user is offline and disable build method
void Selection_Contacts::OnOfflineModeChanged()
{
	if (g_pClientManager->IsLogged())
	{
		ui.btnBuild->setEnabled(true);
		ui.btnOfflineSave->setEnabled(true);
		if (m_pAct_FindDuplicates)m_pAct_FindDuplicates->setEnabled(true);
		if (m_pAct_CreateOrganizationFromPerson)m_pAct_CreateOrganizationFromPerson->setEnabled(true);
		if (m_pAct_CreateUserFromContact)m_pAct_CreateUserFromContact->setEnabled(true);
		if (m_pAct_Delete)m_pAct_Delete->setEnabled(true);
	}
	else
	{
		ui.btnBuild->setEnabled(false);
		ui.btnOfflineSave->setEnabled(false);
		if (m_pAct_FindDuplicates)m_pAct_FindDuplicates->setEnabled(false);
		if (m_pAct_CreateOrganizationFromPerson)m_pAct_CreateOrganizationFromPerson->setEnabled(false);
		if (m_pAct_CreateUserFromContact)m_pAct_CreateUserFromContact->setEnabled(false);
		if (m_pAct_Delete)m_pAct_Delete->setEnabled(false);
	}
}


void Selection_Contacts::OnAddFavorite()
{
	DbRecordSet data;
	data.addColumn(QVariant::Int,"BCNT_ID");
	data.merge(m_lstData,true);

	int nSize=data.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		Status err;
		ClientContactManager::AddFavorite(err,data.getDataRef(i,"BCNT_ID").toInt());
		_CHK_ERR(err);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CONTACT_FAVORITE_ADD,data.getDataRef(i,"BCNT_ID").toInt(),QVariant(),this);
	}
}


void Selection_Contacts::OnRemoveFavorite()
{

	DbRecordSet data;
	data.addColumn(QVariant::Int,"BCNT_ID");
	data.merge(m_lstData,true);

	int nSize=data.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		Status err;
		ClientContactManager::RemoveFavorite(err,data.getDataRef(i,"BCNT_ID").toInt());
		_CHK_ERR(err);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CONTACT_FAVORITE_REMOVE,data.getDataRef(i,"BCNT_ID").toInt(),QVariant(),this);
	}

}

void Selection_Contacts::on_btnOfflineSave_clicked()
{
	int nResult=QMessageBox::question(this,tr("Confirmation"),tr("Do you want to save current contact list into the offline cache?"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES

	Status err;
	ClientContactManager::Offline_SaveContactList(err,m_lstData);
	_CHK_ERR(err);
	

}

void Selection_Contacts::OnCreateUser()
{
	DbRecordSet lstData;
	GetSelection(lstData);
	if (lstData.getRowCount()==0)
		return;

	int nOrgID=-1;
	/*
	SelectionPopup popUp;
	popUp.Initialize(ENTITY_BUS_ORGANIZATION);
	if(popUp.OpenSelector()>0)
	{
		DbRecordSet data;
		popUp.GetSelectedData(nOrgID,data);
	}
	*/

	//lstData.sort("BCNT_NAME");
	//lstData.Dump();

	int nCreated;
	Status err;
	_SERVER_CALL(BusContact->CreatePersonFromContact(err,lstData,nOrgID,nCreated));
	_CHK_ERR(err);

	QString strMessage=QString(tr("%1 users generated from contact data!")).arg(nCreated);
	QMessageBox::information(this,tr("Information"),strMessage);

	//purge person cache:
	g_ClientCache.PurgeCache(ENTITY_BUS_PERSON);
}



void Selection_Contacts::CreateContextMenu()
{

	//---------------------------------------- INIT CNTX MENU TABLE--------------------------------//
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;


	//ui.tableWidget->SetDragIcon(":Contacts32.png");

	if(m_bIsFUISelectorWidget)
	{
		pAction = new QAction(tr("Show Contact Details"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDetails()));
		lstActions.append(pAction);

	}

	pAction = new QAction(tr("Show Contact Details In New Window"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDetailsNewWindow()));
	lstActions.append(pAction);
	if(!m_bIsFUISelectorWidget)
		pAction->setVisible(false);

	m_pAct_Delete = new QAction(tr("Delete Selected Contacts From Database"), this);
	connect(m_pAct_Delete, SIGNAL(triggered()), this, SLOT(OnDelete()));
	lstActions.append(m_pAct_Delete);

	pAction = new QAction(tr("Add Selected Contacts to Favorites"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnAddFavorite()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Remove Selected Contacts From Favorites"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnRemoveFavorite()));
	lstActions.append(pAction);

	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	pAction = new QAction(tr("Clear List"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteTable()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Remove Selected Contacts From List"), this);
	pAction->setShortcut(tr("Del"));
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteSelection()));
	lstActions.append(pAction);
	ui.tableWidget->addAction(pAction);

	pAction = new QAction(tr("Remove Duplicates From List"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnRemoveDuplicates()));
	lstActions.append(pAction);

	m_pAct_FindDuplicates = new QAction(tr("Find Duplicates in Database"), this);
	connect(m_pAct_FindDuplicates, SIGNAL(triggered()), this, SLOT(OnFindDuplicates()));
	lstActions.append(m_pAct_FindDuplicates);

	if(m_bIsFUISelectorWidget)
	{
		SeparatorAct = new QAction(this);
		SeparatorAct->setSeparator(true);
		lstActions.append(SeparatorAct);

		m_pAct_CreateOrganizationFromPerson = new QAction(tr("Create Organization(s) From Person(s)"), this);
		connect(m_pAct_CreateOrganizationFromPerson, SIGNAL(triggered()), this, SLOT(OnCopyPerson2Org()));
		lstActions.append(m_pAct_CreateOrganizationFromPerson);

		m_pAct_CreateUserFromContact = new QAction(tr("Create User"), this);
		connect(m_pAct_CreateUserFromContact, SIGNAL(triggered()), this, SLOT(OnCreateUser()));
		lstActions.append(m_pAct_CreateUserFromContact);
	}

	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);

	//Select All:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), ui.tableWidget, SLOT(OpenSortDialog()));

	lstActions.append(pAction);
	ui.tableWidget->addAction(pAction);


	ui.tableWidget->SetContextMenuActions(lstActions);
}


//group data dropped on grid: re-emit signal to observers:
void Selection_Contacts::OnTableContentChanged()
{
	notifyObservers(SelectorSignals::SELECTOR_ON_CONTENT_CHANGE);
	FilterUserContacts();
}


//get only contacts that are users!!
void Selection_Contacts::FilterUserContacts()
{
	MainEntitySelectionController::FilterUserContacts();
	RefreshDisplay();
}

void Selection_Contacts::OnFindWidgetEnterPressed()
{
	DbRecordSet lstSelection;
	ui.tableWidget->GetSelection(lstSelection);
	if(lstSelection.getRowCount()>0)
	{
		int nRecordID=lstSelection.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
		notifyObservers(SelectorSignals::SELECTOR_FIND_WIDGET_ENTER_PRESSED,nRecordID);
	}

}