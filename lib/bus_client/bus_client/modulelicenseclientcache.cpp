#include "modulelicenseclientcache.h"
#include "bus_core/bus_core/customavailability.h"

//GLOBAL BO SET:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	


ModuleLicenseClientCache::ModuleLicenseClientCache()
{
	Invalidate();
	m_bInitialised=false;
}

/*!
	Inits client with module code / mliidd, called after login

	\param Ret_pStatus			error
	\param strModuleCode		module code
	\param strMLIID				mliid id
*/
void ModuleLicenseClientCache::Initialize(Status& Ret_pStatus,QString strModuleCode,QString strMLIID)
{
	Q_ASSERT(!m_bInitialised); //must be non initialized

	//_SERVER_CALL(ModuleLicense->GetFPList(Ret_pStatus, strModuleCode, strMLIID, m_List))
	//if(!Ret_pStatus.IsOK()){
	//	return;
	//}
	QString strLicUser;
	int nLicID;
	QString strReportLine1;
	QString strReportLine2;
	QString strCustomerID;
	int nCustomSolutionID;


	_SERVER_CALL(ModuleLicense->GetAllModuleLicenseData(Ret_pStatus, strModuleCode, strMLIID, m_List,strLicUser, nLicID, strReportLine1, strReportLine2, strCustomerID,nCustomSolutionID))
	if(!Ret_pStatus.IsOK())return;

	m_strMLIID=strMLIID;
	m_strModuleCode=strModuleCode;
	m_strLicUser = strLicUser;
	m_nLicID = nLicID;

	m_strReportLine1 = strReportLine1;
	m_strReportLine2 = strReportLine2;

	m_strCustomerID		= strCustomerID;
	m_nCustomSolutionID = nCustomSolutionID;

	CustomAvailability::SetCustomSolutionID(nCustomSolutionID);

	m_bInitialised = true;


}



bool ModuleLicenseClientCache::IsFPAvailable(int FPcode, int &Ret_nValue)
{
	//if not UP->ret false, it's OK (as Fani said)
	//Q_ASSERT(m_bInitialised); //must be inited before accessing


	//now search access rights within the local cached list
	bool bAvailable = false;
	int nPos = m_List.find(0, FPcode, true);
	if(nPos > 0){	//record found
		//m_List.Dump();
		int nAvailable = 0;
		m_List.getData(nPos, 1, nAvailable);
		m_List.getData(nPos, 2, Ret_nValue);
		bAvailable = (nAvailable > 0);
	}

	return bAvailable;
}

bool ModuleLicenseClientCache::IsFPAvailable( int FPcode)
{
	int nValue;
	return IsFPAvailable(FPcode, nValue);
}

void ModuleLicenseClientCache::GetFPList(DbRecordSet& RetOut_pList)
{
	Q_ASSERT(m_bInitialised); //must be inited before accessing

	//copy the cached list 
	RetOut_pList = m_List;
}

void ModuleLicenseClientCache::GetLicenseInfo(QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID)
{
	//Q_ASSERT(m_bInitialised); //must be inited before accessing

	//copy the cached data 
	RetOut_strUserName = m_strLicUser;
	RetOut_nLicenseID = m_nLicID;
	RetOut_strReportLine1	= m_strReportLine1;
	RetOut_strReportLine2	= m_strReportLine2;
	Ret_strCustomerID		= m_strCustomerID;
	Ret_nCustomSolutionID	= m_nCustomSolutionID;
}
