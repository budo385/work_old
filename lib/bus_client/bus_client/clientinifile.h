#ifndef SPCINIFILE_H
#define SPCINIFILE_H

#include "common/common/inifile.h"
#include "common/common/dbrecordset.h"
#include <QFileInfo>



/*!
	\class ClientIniFile
	\brief INI file reader for Sokrates SPC
	\ingroup Bus_Client

	Client INI file

*/
class ClientIniFile
{
public:
	ClientIniFile();
	~ClientIniFile();

	ClientIniFile(const ClientIniFile &other){ operator = (other); }
	
	void operator = (const ClientIniFile &other)
	{
		if(this != &other){
			m_strConnectionName			= other.m_strConnectionName;	
			m_nIsThinClient				= other.m_nIsThinClient;
			m_strLastUserName			= other.m_strLastUserName;
			m_strMLIID					= other.m_strMLIID;

			m_nLogLevel					= other.m_nLogLevel;
			m_nLogTargetFile			= other.m_nLogTargetFile;
			m_nLogTargetConsole			= other.m_nLogTargetConsole;
			m_nLogMaxSize				= other.m_nLogTargetConsole;
			m_nLogBufferSize			= other.m_nLogBufferSize;

			m_nBackupFreq				= other.m_nBackupFreq;
			m_strBackupPath				= other.m_strBackupPath;
			m_datBackupLastDate			= other.m_datBackupLastDate;
			m_strRestoreOnNextStart		= other.m_strRestoreOnNextStart;
			m_strBackupTime				= other.m_strBackupTime;
			m_nBackupDay				= other.m_nBackupDay;
			m_strModuleCode				= other.m_strModuleCode;
			m_strKeyFilePath			= other.m_strKeyFilePath;
			m_nHideLogin				= other.m_nHideLogin;
			m_strOrderURL				= other.m_strOrderURL;
			m_nOrderAsk					= other.m_nOrderAsk;
			m_strLangCode				= other.m_strLangCode;
			m_nLastTheme				= other.m_nLastTheme;
			m_nLastView					= other.m_nLastView;
			m_nLastX					= other.m_nLastX;
			m_nLastY					= other.m_nLastY;
			m_nStartImportWizard		= other.m_nStartImportWizard;
			m_strFile					= other.m_strFile;
			m_nPortableVersion			= other.m_nPortableVersion;	
			m_strPortableSkypePath		= other.m_strPortableSkypePath;
			m_strPortableThunderbirdProfile		= other.m_strPortableThunderbirdProfile;
			m_bAskPortableSkypeInstallation		= other.m_bAskPortableSkypeInstallation;
			m_lstOutlookSync					= other.m_lstOutlookSync;
			m_lstThunderbirdSync				= other.m_lstThunderbirdSync;
			m_nMobileGoOfflineOnStartup			= other.m_nMobileGoOfflineOnStartup;
			m_bAskForPortableAppsInstallation	= other.m_bAskForPortableAppsInstallation;
			m_strUpdateSetupPath				= other.m_strUpdateSetupPath;
			m_nDeleteOldBackupsDay				= other.m_nDeleteOldBackupsDay;
		}
	}
	
	void Clear();
	bool Load(QString strFile="", bool bCreateIfNotExists=true);
	bool Save(QString strFile="");
	void SetINIPath(QString strFile){m_strFile=strFile;};

public:

	//data stored in the INI file
	QString m_strConnectionName;		// name of current database/network connection from cfg file
	QString m_strLastUserName;			// last logged user name
	QString m_strMLIID;					// MLIID
	int m_nHideLogin;					// hide login dlg
	QString m_strOrderURL;				// if non empty, redirect to this, otherwise..
	int m_nOrderAsk;					// for first time use in thin client, ask it
	QString m_strLangCode;
	int m_nStartImportWizard;
	int m_nPortableVersion;				// 1 portable, 0 normal
	QString m_strPortableSkypePath;		// "NONE" means no path as specified by MB :-)
	int m_bAskPortableSkypeInstallation;
	int m_nMobileGoOfflineOnStartup;	// for mobile ver
	int m_bAskForPortableAppsInstallation;
	QString m_strPortableThunderbirdProfile;
	QString m_strUpdateSetupPath;

	//LOGGER
	int m_nLogLevel;					
	int m_nLogMaxSize;					
	int m_nLogBufferSize;				
	int m_nLogTargetFile;				
	int m_nLogTargetConsole;				

	//BACKUP:
	int m_nBackupFreq;		
	QString m_strBackupPath;		
	QString m_datBackupLastDate;		
	QString m_strRestoreOnNextStart;		
	QString m_strBackupTime;
	int m_nBackupDay;
	int m_nDeleteOldBackupsDay;

	//Key file..
	int m_nIsThinClient;
	QString m_strModuleCode;
	QString m_strKeyFilePath;

	//THEME
	int m_nLastTheme;
	int m_nLastView;
	int m_nLastX;
	int m_nLastY;

	//Outlook_Synchronization
	DbRecordSet m_lstOutlookSync;

	//Thunderbird_Synchronization
	DbRecordSet m_lstThunderbirdSync;

protected:
	bool SaveDefaults(QString strFile);
	IniFile m_objIni;
	QString m_strFile;
};

#endif //SERVERINIFILE_H