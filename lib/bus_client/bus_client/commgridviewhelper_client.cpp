#include "commgridviewhelper_client.h"

#include "bus_client/bus_client/documenthelper.h"

void CommGridViewHelper_Client::GetDocApplicationsFromServer(DbRecordSet &lstData, Status &err)
{
	DocumentHelper::GetDocApplications(lstData); //this smart object written by B.T: already uses client side cache
}

