#include "emailselectordialog.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/macros.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

EmailSelectorDialog::EmailSelectorDialog(int nContactID, QStringList lstPreselectedMails,QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	ui.treeWidget->setColumnCount(5);
	ui.treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
	ui.treeWidget->setColumnHidden(4, true);
	QStringList lstLabels;
	lstLabels << tr("Email") << tr("Name") << tr("Type") << tr("Description");
	ui.treeWidget->setHeaderLabels(lstLabels);

	ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	DbRecordSet recEmails;
	if(!GetSingleContacMails(nContactID,recEmails))
		return;

	int nRowCount = recEmails.getRowCount();
	for(int i=0; i<nRowCount; i++)
	{
		QStringList lstRow;
		lstRow << recEmails.getDataRef(i, "BCME_ADDRESS").toString() << recEmails.getDataRef(i, "BCME_NAME").toString() 
			   << recEmails.getDataRef(i, "BCMT_TYPE_NAME").toString() << recEmails.getDataRef(i, "BCME_DESCRIPTION").toString() << recEmails.getDataRef(i, "BCME_ID").toString();
		QTreeWidgetItem *item = new QTreeWidgetItem(lstRow);
		if (lstPreselectedMails.indexOf(recEmails.getDataRef(i, "BCME_ADDRESS").toString())>=0)
		{
			item->setSelected(true);
		}
		ui.treeWidget->addTopLevelItem(item);
	}
	ui.treeWidget->setCurrentItem(ui.treeWidget->topLevelItem(0));
	connect(ui.treeWidget, SIGNAL(itemDoubleClicked ( QTreeWidgetItem * , int )), this, SLOT(OnDblClick(QTreeWidgetItem *, int)));

#ifdef WINCE
	resize(200,150);
#endif
}

EmailSelectorDialog::~EmailSelectorDialog()
{

}

QList<QString> EmailSelectorDialog::getSelectedEmail()
{
	QList<QString> lstData;
	
	QTreeWidgetItem *item = ui.treeWidget->currentItem();
	if(item)
		lstData << item->text(4) << item->text(0); //Insert email ID and email

	return lstData;
}

void EmailSelectorDialog::on_Ok_pushButton_clicked()
{
	done(1);
}

void EmailSelectorDialog::on_Cancel_pushButton_clicked()
{
	done(0);
}


void EmailSelectorDialog::OnDblClick(QTreeWidgetItem *, int)
{
	done(1);
}

bool EmailSelectorDialog::GetSingleContacMails(int nContactID, DbRecordSet &recMailRecordSet)
{
	//Get contact emails.
	Status status;
	QString strWhere = " FROM BUS_CM_EMAIL LEFT OUTER JOIN BUS_CM_TYPES ON BCME_TYPE_ID = BCMT_ID";
	strWhere += " WHERE BCME_CONTACT_ID = " + QVariant(nContactID).toString();
	recMailRecordSet.destroy();
	recMailRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT));
	_SERVER_CALL(ClientSimpleORM->ReadAdv(status, recMailRecordSet, strWhere))
	_CHK_ERR_RET_BOOL(status);
}