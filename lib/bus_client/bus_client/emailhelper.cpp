#include "emailhelper.h"
#include "bus_core/bus_core/emailhelpercore.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "clientcontactmanager.h"
#include "documenthelper.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "trans/trans/qhttp.h"
#include <QDir>
#include <QBuffer>
#include <QEventLoop>
#include <QCoreApplication>

#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;



//load from cache and server, store in cache
void EmailHelper::GetEmailTemplates(DbRecordSet &lstData,bool bReloadFromServer)
{
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_TEMPLATES));

	if(g_pClientManager->GetPersonID()==0) return;

	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_BUS_EMAIL_TEMPLATES);
		if (plstCacheData)
		{
			lstData=*plstCacheData;
			return;
		}
	}

	Status err;
	QString strWhere =" WHERE BEM_TEMPLATE_FLAG=1 ";
	g_AccessRight->SQLFilterRecords(BUS_EMAIL,strWhere);
	_SERVER_CALL(ClientSimpleORM->Read(err,BUS_EMAIL,lstData,strWhere,DbSqlTableView::TVIEW_BUS_EMAIL_TEMPLATES))
		if (!err.IsOK())
		{
			QMessageBox::critical(NULL,(QString)QObject::tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		//Store in Cache:
		g_ClientCache.SetCache(ENTITY_BUS_EMAIL_TEMPLATES,lstData);
}

void EmailHelper::ReplaceTag(QString &strBody, const QString strTag, const QString strValue, bool bHtml)
{
	if(bHtml)
	{
		int nStartPos = 0;
		while(1)
		{
			//search for palceholder in the input
			int nStart = strBody.indexOf("[", nStartPos, Qt::CaseInsensitive);
			if(nStart < 0)
				break;
			int nEnd = strBody.indexOf("]", nStart+1);
			if(nEnd < 0)
				break;

			//extract possible placeholder [some_text]
			QString strData = strBody.mid(nStart, nEnd-nStart+1);
			
			//strip HTML from placeholder
			//void Service_BusImport::StripHTML(QString &strData)
			int nHtmlStart = 0;
			while((nHtmlStart = strData.indexOf('<', nHtmlStart)) >= 0)
			{
				int nHtmlEnd = strData.indexOf('>', nHtmlStart+1);
				if(nHtmlEnd < 0)
					break;
				//cut the tag 
				strData.remove(nHtmlStart, nHtmlEnd-nHtmlStart+1);
			}
			strData.replace("\r", "");
			strData.replace("\n", "");

			if(strData == strTag)
			{
				//match found, replace
				strBody.remove(nStart, nEnd-nStart+1);
				strBody.insert(nStart, strValue);

				nStartPos = nStart+strValue.length()+1;
			}
			else
				nStartPos = nEnd+1;
		}
	}
	else
	{
		strBody.replace(strTag,	strValue);
	}
}

bool EmailHelper::CreateEmailFromTemplate(QString bodyTemplate, QString &NewBody, int nContactID, int nProjectID, bool btoPercentEncoding, DbRecordSet *rowQCWContactData)
{
	NewBody = bodyTemplate;


	bool bHtml = false;
	if( strstr(NewBody.toLatin1().constData(), "<html") != NULL ||
		strstr(NewBody.toLatin1().constData(), "<HTML") != NULL) 
		bHtml = true;

	//support for info on logged user
	DbRecordSet recUserData;
	g_pClientManager->GetUserData(recUserData);
	//recUserData.Dump();
	ReplaceTag(NewBody, "[User_First_Name]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(recUserData.getDataRef(0,"BPER_FIRST_NAME").toString()) : recUserData.getDataRef(0,"BPER_FIRST_NAME").toString(), bHtml);
	ReplaceTag(NewBody, "[User_Last_Name]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(recUserData.getDataRef(0,"BPER_LAST_NAME").toString()) : recUserData.getDataRef(0,"BPER_LAST_NAME").toString(), bHtml);
	ReplaceTag(NewBody, "[User_Initials]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(recUserData.getDataRef(0,"BPER_INITIALS").toString()) : recUserData.getDataRef(0,"BPER_INITIALS").toString(), bHtml);

	bool bUserDone = false;
	if(!recUserData.getDataRef(0,"BPER_CONTACT_ID").isNull())
	{
		int nCurUserContactID = recUserData.getDataRef(0,"BPER_CONTACT_ID").toInt();

		DbRecordSet lstRes;
		lstRes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

		bool bReadFromQCW=false;
		if (rowQCWContactData && nContactID<=0)  //BT: issue 1275
		{
			if (rowQCWContactData->getRowCount()>0)
			{
				lstRes=*rowQCWContactData;
				bReadFromQCW=true;
			}
		}

		if (!bReadFromQCW) //BT: issue 1275
		{
			Status status;
			QString strWhere= QString(" WHERE BCNT_ID=%1").arg(nCurUserContactID);
			MainEntityFilter filter;
			filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
			_SERVER_CALL(BusContact->ReadData(status, filter.Serialize(), lstRes))
				_CHK_ERR_RET_BOOL_ON_FAIL(status);
		}


		if(lstRes.getRowCount() > 0)
		{
			bUserDone = true;
			ReplaceTag(NewBody, "[User_Department]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_DEPARTMENTNAME").toString()) : lstRes.getDataRef(0,"BCNT_DEPARTMENTNAME").toString(), bHtml);
			ReplaceTag(NewBody, "[User_Organization]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString()) : lstRes.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString(), bHtml);
			ReplaceTag(NewBody, "[User_Function]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_FUNCTION").toString()) : lstRes.getDataRef(0,"BCNT_FUNCTION").toString(), bHtml);
			ReplaceTag(NewBody, "[User_Location]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_LOCATION").toString()) : lstRes.getDataRef(0,"BCNT_LOCATION").toString(), bHtml);

			//extract default address
			DbRecordSet lstAddresses = lstRes.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
			lstAddresses.find("BCMA_IS_DEFAULT", 1);
			lstAddresses.deleteUnSelectedRows();

			if(lstAddresses.getRowCount() > 0){
				QString strAddr = lstAddresses.getDataRef(0,"BCMA_FORMATEDADDRESS").toString();
				if(bHtml)
					strAddr.replace("\n","<br>\n");	// HTML requires line break to propely display address
				ReplaceTag(NewBody, "[User_Address_Formatted]", (btoPercentEncoding) ? QUrl::toPercentEncoding(strAddr) : strAddr, bHtml);
			}
			else
				ReplaceTag(NewBody, "[User_Address_Formatted]", "", bHtml);

			//extract default email
			DbRecordSet lstEmails = lstRes.getDataRef(0, "LST_EMAIL").value<DbRecordSet>();
			lstEmails.find("BCME_IS_DEFAULT", 1);
			lstEmails.deleteUnSelectedRows();

			if(lstEmails.getRowCount() > 0)
				ReplaceTag(NewBody, "[User_Email_Address]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstEmails.getDataRef(0,"BCME_ADDRESS").toString()) : lstEmails.getDataRef(0,"BCME_ADDRESS").toString(), bHtml);
			else
				ReplaceTag(NewBody, "[User_Email_Address]", "", bHtml);

			//extract default phone
			DbRecordSet lstPhones = lstRes.getDataRef(0, "LST_PHONE").value<DbRecordSet>();
			lstPhones.find("BCMP_IS_DEFAULT", 1);
			lstPhones.deleteUnSelectedRows();

			if(lstPhones.getRowCount() > 0)
				ReplaceTag(NewBody, "[User_Phone]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstPhones.getDataRef(0,"BCMP_FULLNUMBER").toString()) : lstPhones.getDataRef(0,"BCMP_FULLNUMBER").toString(), bHtml);
			else
				ReplaceTag(NewBody, "[User_Phone]", "", bHtml);
		}
	}

	if(!bUserDone)
	{
		ReplaceTag(NewBody, "[User_Department]", "", bHtml);
		ReplaceTag(NewBody, "[User_Organization]", "", bHtml);
		ReplaceTag(NewBody, "[User_Function]", "", bHtml);
		ReplaceTag(NewBody, "[User_Location]", "", bHtml);
		ReplaceTag(NewBody, "[User_Address_Formatted]", "", bHtml);
		ReplaceTag(NewBody, "[User_Email_Address]", "", bHtml);
		ReplaceTag(NewBody, "[User_Phone]", "", bHtml);
	}

	if(nContactID > 0 || rowQCWContactData)
	{
		DbRecordSet lstRes;
		lstRes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
		bool bReadFromQCW=false;
		if (rowQCWContactData && nContactID<=0)  //BT: issue 1275
		{
			if (rowQCWContactData->getRowCount()>0)
			{
				lstRes=*rowQCWContactData;
				bReadFromQCW=true;
			}
		}

		if (!bReadFromQCW) //BT: issue 1275
		{
			Status status;
			QString strWhere= QString(" WHERE BCNT_ID=%1").arg(nContactID);
			MainEntityFilter filter;
			filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
			_SERVER_CALL(BusContact->ReadData(status, filter.Serialize(), lstRes))
				_CHK_ERR_RET_BOOL_ON_FAIL(status);
		}

		if(lstRes.getRowCount() > 0)
		{
			ReplaceTag(NewBody, "[First_Name]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_FIRSTNAME").toString()) : lstRes.getDataRef(0,"BCNT_FIRSTNAME").toString(), bHtml);
			ReplaceTag(NewBody, "[Last_Name]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_LASTNAME").toString()) : lstRes.getDataRef(0,"BCNT_LASTNAME").toString(), bHtml);
			ReplaceTag(NewBody, "[Middle_Name]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_MIDDLENAME").toString()) : lstRes.getDataRef(0,"BCNT_MIDDLENAME").toString(), bHtml);
			ReplaceTag(NewBody, "[Department]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_DEPARTMENTNAME").toString()) : lstRes.getDataRef(0,"BCNT_DEPARTMENTNAME").toString(), bHtml);
			ReplaceTag(NewBody, "[Organization]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString()) : lstRes.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString(), bHtml);
			ReplaceTag(NewBody, "[Profession]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_PROFESSION").toString()) : lstRes.getDataRef(0,"BCNT_PROFESSION").toString(), bHtml);
			ReplaceTag(NewBody, "[Function]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_FUNCTION").toString()) : lstRes.getDataRef(0,"BCNT_FUNCTION").toString(), bHtml);
			ReplaceTag(NewBody, "[Location]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BCNT_LOCATION").toString()) : lstRes.getDataRef(0,"BCNT_LOCATION").toString(), bHtml);

			//extract default address
			DbRecordSet lstAddresses = lstRes.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
			lstAddresses.find("BCMA_IS_DEFAULT", 1);
			lstAddresses.deleteUnSelectedRows();

			if(lstAddresses.getRowCount() > 0)
			{
				QString strAddr = lstAddresses.getDataRef(0,"BCMA_FORMATEDADDRESS").toString();
				if(bHtml)
					strAddr.replace("\n","<br>\n");	// HTML requires line break to propely display address

				ReplaceTag(NewBody, "[Address_Title]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_TITLE").toString()) : lstAddresses.getDataRef(0,"BCMA_TITLE").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_Formatted]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(strAddr) : strAddr, bHtml);
				ReplaceTag(NewBody, "[Address_Salutation]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_SALUTATION").toString()) : lstAddresses.getDataRef(0,"BCMA_SALUTATION").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_Short_Salutation]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_SHORT_SALUTATION").toString()) : lstAddresses.getDataRef(0,"BCMA_SHORT_SALUTATION").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_Country Code]",(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_COUNTRY_CODE").toString()) : lstAddresses.getDataRef(0,"BCMA_COUNTRY_CODE").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_Country]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_COUNTRY_NAME").toString()) : lstAddresses.getDataRef(0,"BCMA_COUNTRY_NAME").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_Street]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_STREET").toString()) : lstAddresses.getDataRef(0,"BCMA_STREET").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_City]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_CITY").toString()) : lstAddresses.getDataRef(0,"BCMA_CITY").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_Zip]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_ZIP").toString()) : lstAddresses.getDataRef(0,"BCMA_ZIP").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_Region]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_REGION").toString()) : lstAddresses.getDataRef(0,"BCMA_REGION").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_POBOX]",		(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_POBOX").toString()) : lstAddresses.getDataRef(0,"BCMA_POBOX").toString(), bHtml);
				ReplaceTag(NewBody, "[Address_POBOX_Zip]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstAddresses.getDataRef(0,"BCMA_POBOXZIP").toString()) : lstAddresses.getDataRef(0,"BCMA_POBOXZIP").toString(), bHtml);
			}
			else{
				ReplaceTag(NewBody, "[Address_Title]",		"", bHtml);
				ReplaceTag(NewBody, "[Address_Formatted]",	"", bHtml);
				ReplaceTag(NewBody, "[Address_Salutation]",	"", bHtml);
				ReplaceTag(NewBody, "[Address_Short_Salutation]",	"", bHtml);
				ReplaceTag(NewBody, "[Address_Country Code]","", bHtml);
				ReplaceTag(NewBody, "[Address_Country]",	"", bHtml);
				ReplaceTag(NewBody, "[Address_Street]",		"", bHtml);
				ReplaceTag(NewBody, "[Address_City]",		"", bHtml);
				ReplaceTag(NewBody, "[Address_Zip]",		"", bHtml);
				ReplaceTag(NewBody, "[Address_Region]",		"", bHtml);
				ReplaceTag(NewBody, "[Address_POBOX]",		"", bHtml);
				ReplaceTag(NewBody, "[Address_POBOX_Zip]",	"", bHtml);
			}

			//extract default email
			DbRecordSet lstEmails = lstRes.getDataRef(0, "LST_EMAIL").value<DbRecordSet>();
			lstEmails.find("BCME_IS_DEFAULT", 1);
			lstEmails.deleteUnSelectedRows();

			if(lstEmails.getRowCount() > 0)
			{
				ReplaceTag(NewBody, "[Email_Address]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstEmails.getDataRef(0,"BCME_ADDRESS").toString()) : lstEmails.getDataRef(0,"BCME_ADDRESS").toString(), bHtml);
			}
			else
				ReplaceTag(NewBody, "[Email_Address]",	"", bHtml);

			//extract default internet address
			DbRecordSet lstInternet = lstRes.getDataRef(0, "LST_INTERNET").value<DbRecordSet>();
			lstInternet.find("BCMI_IS_DEFAULT", 1);
			lstInternet.deleteUnSelectedRows();

			if(lstInternet.getRowCount() > 0)
			{
				ReplaceTag(NewBody, "[Internet_Address]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstInternet.getDataRef(0,"BCMI_ADDRESS").toString()) : lstInternet.getDataRef(0,"BCMI_ADDRESS").toString(), bHtml);
			}
			else
				ReplaceTag(NewBody, "[Internet_Address]",	"", bHtml);


			//extract default phone
			DbRecordSet lstPhones = lstRes.getDataRef(0, "LST_PHONE").value<DbRecordSet>();
			lstPhones.find("BCMP_IS_DEFAULT", 1);
			lstPhones.deleteUnSelectedRows();

			//lstPhones.Dump();

			if(lstPhones.getRowCount() > 0)
			{
				ReplaceTag(NewBody, "[Phone]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstPhones.getDataRef(0,"BCMP_FULLNUMBER").toString()) : lstPhones.getDataRef(0,"BCMP_FULLNUMBER").toString(), bHtml);
				ReplaceTag(NewBody, "[nPhone]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstPhones.getDataRef(0,"BCMP_SEARCH").toString()) : lstPhones.getDataRef(0,"BCMP_SEARCH").toString(), bHtml);
			}
			else{
				ReplaceTag(NewBody, "[Phone]",	"", bHtml);
				ReplaceTag(NewBody, "[nPhone]",	"", bHtml);
			}

			//extract Skype phone
			int nPhoneSkypeID = ClientContactManager::GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE).toInt();
			DbRecordSet lstSkype = lstRes.getDataRef(0, "LST_PHONE").value<DbRecordSet>();
			lstSkype.find("BCMP_TYPE_ID", nPhoneSkypeID);
			lstSkype.deleteUnSelectedRows();

			if(lstSkype.getRowCount() > 0)
				ReplaceTag(NewBody, "[Skype]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstSkype.getDataRef(0,"BCMP_FULLNUMBER").toString()) : lstSkype.getDataRef(0,"BCMP_FULLNUMBER").toString(), bHtml);
			else
				ReplaceTag(NewBody, "[Skype]",	"", bHtml);
		}
	}

	if(nProjectID > 0)
	{
		DbRecordSet lstRes, set;
		set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));
		lstRes.copyDefinition(set, false);

		Status status;
		QString strWhere= QString(" FROM BUS_PROJECTS WHERE BUSP_ID=%1").arg(nProjectID);
		_SERVER_CALL(ClientSimpleORM->ReadAdv(status, lstRes, strWhere))
			_CHK_ERR_RET_BOOL_ON_FAIL(status);

		if(lstRes.getRowCount() > 0)
		{
			ReplaceTag(NewBody, "[Project_Code]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BUSP_CODE").toString()) : lstRes.getDataRef(0,"BUSP_CODE").toString(), bHtml);
			ReplaceTag(NewBody, "[Project_Name]",	(btoPercentEncoding) ? QUrl::toPercentEncoding(lstRes.getDataRef(0,"BUSP_NAME").toString()) : lstRes.getDataRef(0,"BUSP_NAME").toString(), bHtml);
		}
		else{
			ReplaceTag(NewBody, "[Project_Code]",	"", bHtml);
			ReplaceTag(NewBody, "[Project_Name]",	"", bHtml);
		}
	}
	else{
		ReplaceTag(NewBody, "[Project_Code]",	"", bHtml);
		ReplaceTag(NewBody, "[Project_Name]",	"", bHtml);
	}
	return true;
}


void EmailHelper::UnpackCIDToTemp(DbRecordSet &lstCIDInfo,DbRecordSet &rowEmail,DbRecordSet &lstAttachments)
{
	//PROTECTION: must not call this more than once
	//Q_ASSERT(!m_bCIDPrepared);
	//if (m_bCIDPrepared)
	//	return;
	//m_bCIDPrepared = true;

	//THIS is called at the dialog startup:
	//inspect html body for "<img scr="cid:... " and 
	//rewrite these links as an ordinary link to temporary unpacked image attachments
	lstCIDInfo.destroy();
	lstCIDInfo.addColumn(QVariant::String,  "CID");
	lstCIDInfo.addColumn(QVariant::String,  "Path");
	lstCIDInfo.addColumn(QVariant::Int,	  "IsTemporary");

	bool bChanged = false;
	int nStartPos = 0;
	QString strBody = rowEmail.getDataRef(0,"BEM_BODY").toString();
	strBody = strBody.replace("=3D", "=");

	while(1)
	{
		//search sample: <img src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
		//search sample: <img id="aaaa" src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
		int nPos = strBody.indexOf("<img", nStartPos, Qt::CaseInsensitive);
		if(nPos >= 0)
		{
			int nPosLinkEnd = strBody.indexOf(">", nPos+1);
			bool bStrange = false;
			int nPos2 = strBody.indexOf("src=\"cid:", nPos+1);
			if(nPos2 < 0){
				nPos2 = strBody.indexOf("src=3D\"cid:", nPos+1);
				bStrange = true;
			}

			if(nPos2 < 0 || nPos2 > nPosLinkEnd)
			{
				nStartPos = nPos+1;
				continue;
			}

			bChanged = true;

			//extract CID
			QString strCIDLink;
			QString strCID;
			if(nPosLinkEnd >= 0){
				strCIDLink = strBody.mid(nPos, nPosLinkEnd-nPos+1);

				nPos2 += bStrange? strlen("src=3D\"cid:") : strlen("src=\"cid:");
				strCID = strBody.mid(nPos2, nPosLinkEnd-nPos2+1);
				int nEnd = strCID.indexOf("\"");
				if(nEnd > 0)
					strCID = strCID.mid(0, nEnd);
			}

			if(strCID.isEmpty()){
				Q_ASSERT(false);		//invalid CID in HTML
				nStartPos = nPos+1;
				continue;
			}

			//find attachment having matching CID
			int nAttCnt = lstAttachments.getRowCount();
			int nAttIdx = -1;
			for(int i=0; i<nAttCnt; i++){
				QString strAttCID = lstAttachments.getDataRef(i,"BEA_CID_LINK").toString();
				if (strAttCID == strCID){
					nAttIdx = i;
					break;
				}
			}
			if(nAttIdx < 0){
				//Q_ASSERT(false);		//could not find matching attachment
				nStartPos = nPos + qMin(1, strCIDLink.length());
				continue;
			}

			// unpack the attachment to temp folder
			QString strName = lstAttachments.getDataRef(nAttIdx,"BEA_NAME").toString();
			QFileInfo attInfo(strName);
			QString strTmpPath;
			//generate unique temp name
			int nTry = 0; bool bOK = false;
			while(nTry < 30 && !bOK){
				strTmpPath = QDir::tempPath();
				strTmpPath += "/";
				strTmpPath += attInfo.completeBaseName();
				strTmpPath += QVariant(nTry).toString();
				strTmpPath += ".";
				strTmpPath += attInfo.completeSuffix();
				nTry ++;
				bOK = !QFile::exists(strTmpPath);
			}
			if(!bOK){
				Q_ASSERT(false);	//could not create unused temp path
				nStartPos = nPos + qMin(1, strCIDLink.length());
				continue;
			}

			//save attachment
			DocumentHelper::SaveFileContent(lstAttachments.getDataRef(nAttIdx,"BEA_CONTENT").toByteArray(), strTmpPath, true);

			//remember att info for revert operation
			lstCIDInfo.addRow();
			int nRow = lstCIDInfo.getRowCount()-1;
			lstCIDInfo.setData(nRow, "CID", strCID);
			lstCIDInfo.setData(nRow, "Path", strTmpPath);
			lstCIDInfo.setData(nRow, "IsTemporary", 1);

			//rewrite tag in the HTML body
			strBody = strBody.mid(0, nPos) + strBody.mid(nPos + strCIDLink.length());

			QString strNewTag = "<img src=\"";
			strNewTag += strTmpPath;
			strNewTag += "\">";
			strBody.insert(nPos, strNewTag);

			//keep searching forward
			nStartPos = nPos + strNewTag.length();
		}
		else
			break;
	}

	qDebug() << "Attachments:";
	_DUMP(lstAttachments);
	qDebug() << "CID List:";
	_DUMP(lstCIDInfo);

	//now try deleting all attachments with CID, not present anymore in the body (lstCIDInfo list)
	int nAttCnt = lstAttachments.getRowCount();
	int nCidCnt = lstCIDInfo.getRowCount();
	for(int i=nAttCnt-1; i>=0; i--)
	{
		QString strCID = lstAttachments.getDataRef(i,"BEA_CID_LINK").toString();
		if(!strCID.isEmpty())	//only for embedded attachments
		{
			bool bCidFound = false;
			for(int j=0; j<nCidCnt; j++){
				QString strCID2 = lstCIDInfo.getDataRef(j,"CID").toString();
				if(strCID == strCID2){
					bCidFound = true;	
					break;
				}
			}
			if(!bCidFound){
				lstAttachments.deleteRow(i);
			}
		}
	}

	//replace body string with "patched" one
	if(bChanged)
		rowEmail.setData(0,"BEM_BODY", strBody);
}

//TOFIX detect attachments that may be deleted (user deleted embedded image)
void EmailHelper::UpdateEmailImageCID(DbRecordSet &lstCIDInfo,DbRecordSet &rowEmail,DbRecordSet &lstAttachments)
{
	//PROTECTION: must not call this more than once
	//BT commented: break when clean mail is sent from our app: //Q_ASSERT(!m_bCIDFinalized);
	//m_bCIDFinalized = true;
	//BT commented: break when clean mail is sent from our app: //Q_ASSERT(m_bCIDPrepared); //init must be called at the start

	//THIS is called when the dialog closes (on email save/send):
	//inspect html body for image links and 
	//rewrite these links back to CID links "<img src="cid:... " 
	//(any new image will have to be added as an attachment with its CID generated on-the-fly)
	//Then clear all temporary unpacked images

	bool bChanged = false;
	int nStartPos = 0;
	QString strBody = rowEmail.getDataRef(0,"BEM_BODY").toString();
	while(1)
	{
		//search for image tag, examples: 
		//<img src="
		//<img style="width: 250px; height: 80px;" alt="" src="
		int nPos = strBody.indexOf("<img ", nStartPos);
		if(nPos >= 0)
		{
			bChanged = true;

			//extract image path
			QString strImageLink;
			QString strImagePath;
			int nPosLinkEnd = strBody.indexOf(">", nPos+1);
			if(nPosLinkEnd >= 0){
				strImageLink = strBody.mid(nPos, nPosLinkEnd-nPos+1);
				
				//extract image "src" content
				strImagePath = strImageLink.mid(strlen("<img "));
				int nStart = strImagePath.indexOf("src=\"");
				if(nStart >= 0)
					strImagePath = strImagePath.mid(nStart+strlen("src=\""));
				int nEnd = strImagePath.indexOf("\"");
				if(nEnd >= 0)
					strImagePath = strImagePath.mid(0, nEnd);
			}
			if(strImagePath.isEmpty()){
				Q_ASSERT(false);		//invalid <img> tag in HTML (no path)
				nStartPos = nPos+1;
				continue;
			}
			if( strImagePath.startsWith("http://") ||
				strImagePath.startsWith("https://")){
				nStartPos = nPos+1;	//external linked image
				continue;
			}

			//find CID by path
			QString strCID;
			int nInfoIdx = -1;
			int nInfoCnt = lstCIDInfo.getRowCount();
			for(int j=0; j<nInfoCnt; j++){
				if (lstCIDInfo.getDataRef(j,"Path").toString() == strImagePath){
					nInfoIdx = j;
					break;
				}
			}
			if(nInfoIdx < 0){
				//image not found in the list (newly inserted)
				//generate new CID number (12=3 * 4 bytes long)
				//sample CID: <img src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
				QByteArray rndData;
//#ifndef WINCE
				qsrand(QTime::currentTime().msec());
//#endif
				while(1){
					int nNum = qrand();
					rndData += QByteArray::fromRawData((const char *)&nNum, sizeof(int));
					nNum = qrand();
					rndData += QByteArray::fromRawData((const char *)&nNum, sizeof(int));
					nNum = qrand();
					rndData += QByteArray::fromRawData((const char *)&nNum, sizeof(int));
					strCID = rndData.toBase64();

					//if this CID already exists, generate new one
					bool bCidExists = false;
					for(int j=0; j<nInfoCnt; j++){
						if (lstCIDInfo.getDataRef(j,"CID").toString() == strCID){
							bCidExists = true;
							break;
						}
					}
					if(bCidExists)
						rndData = "";	//generate new one
					else
						break;
				}

				//add new attachment in the list
				QFileInfo attInfo(strImagePath);
				if(attInfo.exists())
				{
					lstAttachments.addRow();
					int nRowLast = lstAttachments.getRowCount()-1;

					lstAttachments.setData(nRowLast,"BEA_NAME", attInfo.fileName());

					QByteArray binary;
					DocumentHelper::LoadFileContent(binary, strImagePath, true);
					lstAttachments.setData(nRowLast,"BEA_CONTENT",binary);

					lstAttachments.setData(nRowLast,"BEA_CID_LINK",strCID);

					//store new CID entry
					lstCIDInfo.addRow();
					int nCidRowLast = lstCIDInfo.getRowCount()-1;
					lstCIDInfo.setData(nCidRowLast,"CID", strCID);
					lstCIDInfo.setData(nCidRowLast,"Path", strImagePath);
					lstCIDInfo.setData(nCidRowLast,"IsTemporary", 1);
				}
				else
				{
					//this must be an embedded picture with data directly in the tag
					//must convert it to attachment, similar to UnembedHtmlPix code
					if(strImagePath.indexOf("data:image") == 0)
					{
						int nSrcStart1 = strImagePath.indexOf(";base64,");
						if(nSrcStart1 > 0){
							nSrcStart1 += 8; //strlen(";base64,")

							//extract data
							QString strBase64 = strImagePath.mid(nSrcStart1);
							//convert from base64
							QByteArray arData = QByteArray::fromBase64(strBase64.toLatin1());
							//compress the data as required by storage
							arData=qCompress(arData,2); //min compression

							//extract image type
							const static int nPfxLen = strlen("data:image/");
							QString strExt = strImagePath.mid(nPfxLen, nSrcStart1-nPfxLen-8);	//8 -> strlen(";base64,")
							
							QString strName = "SokratesImage." + strExt;

							// unpack the attachment to temp folder
							QFileInfo attInfo(strName);
							QString strTmpPath;
							//generate unique temp name
							int nTry = 0; bool bOK = false;
							while(nTry < 30 && !bOK){
								strTmpPath = QDir::tempPath();
								strTmpPath += "/";
								strTmpPath += attInfo.completeBaseName();
								strTmpPath += QVariant(nTry).toString();
								strTmpPath += ".";
								strTmpPath += attInfo.completeSuffix();
								nTry ++;
								bOK = !QFile::exists(strTmpPath);
							}
							if(!bOK){
								Q_ASSERT(false);	//could not create unused temp path
								nStartPos = nPos + qMin(1, strCID.length());
								continue;
							}
							else{
								QFileInfo attInfo1(strTmpPath);
								strName = attInfo1.baseName();
							}

							//save data to temporary path as well
							DocumentHelper::SaveFileContent(arData, strTmpPath, true);

							//add as an attachment
							lstAttachments.addRow();
							int nRowLast = lstAttachments.getRowCount()-1;

							lstAttachments.setData(nRowLast,"BEA_NAME", strName);
							lstAttachments.setData(nRowLast,"BEA_CONTENT",arData);
							lstAttachments.setData(nRowLast,"BEA_CID_LINK",strCID);
							
							//store new CID entry
							lstCIDInfo.addRow();
							int nCidRowLast = lstCIDInfo.getRowCount()-1;
							lstCIDInfo.setData(nCidRowLast,"CID", strCID);
							lstCIDInfo.setData(nCidRowLast,"Path", strTmpPath);
							lstCIDInfo.setData(nCidRowLast,"IsTemporary", 1);
						}
					}
					else{
						Q_ASSERT(false);	//file missing, not embedded
					}
				}
			}
			else
				strCID = lstCIDInfo.getDataRef(nInfoIdx,"CID").toString();

			//rewrite tag in the HTML body
			strBody = strBody.mid(0, nPos) + strBody.mid(nPos + strImageLink.length());

			QString strNewTag = "<img src=\"cid:";
			strNewTag += strCID;
			strNewTag += "\" alt=\"\">";
			strBody.insert(nPos, strNewTag);

			//keep searching forward
			nStartPos = nPos + strNewTag.length();
		}
		else
			break;
	}

	//replace body string with "patched" one
	if(bChanged)
		rowEmail.setData(0,"BEM_BODY", strBody);

	//delete all temporary images
	int nInfoCnt = lstCIDInfo.getRowCount();
	for(int j=0; j<nInfoCnt; j++){
		if (lstCIDInfo.getDataRef(j,"IsTemporary").toInt()){
			QFile file(lstCIDInfo.getDataRef(j,"Path").toString());
			file.remove();
		}
	}
	lstCIDInfo.clear(); // no need for this anymore
}

void EmailHelper::EmbedHtmlPix(QString &strHtmlData, QStringList &lstResOrigFiles)
{
	lstResOrigFiles.clear();

	//find and embed each picture in the HTML
	int nPos = strHtmlData.indexOf("<img", 0, Qt::CaseInsensitive);
	while(nPos >= 0)
	{
		//extract file path
		int nSrcStart = strHtmlData.indexOf("src=\"", nPos, Qt::CaseInsensitive);
		if(nSrcStart > 0)
		{
			nSrcStart += 5; //strlen("src=\"")
			int nSrcEnd = strHtmlData.indexOf("\"", nSrcStart);
			if(nSrcEnd > 0)
			{
				QString strFile = strHtmlData.mid(nSrcStart, nSrcEnd-nSrcStart);

				//FIX: skip loading remote files, issue #2050
				if(strFile.startsWith("http://")){
					nPos = nSrcEnd + 1;
					//search next
					nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
					continue;
				}

				//load file into the byte array
				QFile file(strFile);
				if (file.open(QIODevice::ReadOnly))
				{
					//convert to base64
					QByteArray arDataBase64 = file.readAll().toBase64();
					
					//write back into the string (replace old "src" value with embedded data)
					strHtmlData.remove(nSrcStart, nSrcEnd-nSrcStart);
					strHtmlData.insert(nSrcStart, QString(arDataBase64));

					//use extension for mime type
					QString strExt = strFile;
					int nPosExt = strExt.lastIndexOf('.');
					if(nPosExt > 0)
						strExt = strExt.mid(nPosExt+1);
					strHtmlData.insert(nSrcStart, QString("data:image/%1;base64,").arg(strExt));

					lstResOrigFiles.append(strFile);
				}
			}
		}
		
		//search next
		nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
	}
}

void EmailHelper::UnembedHtmlPix(QString &strHtmlData, QStringList &lstResTempFiles)
{
	lstResTempFiles.clear();

	//find and embed each picture in the HTML
	int nPos = strHtmlData.indexOf("<img", 0, Qt::CaseInsensitive);
	while(nPos >= 0)
	{
		//extract file path
		int nSrcStart = strHtmlData.indexOf("src=\"", nPos, Qt::CaseInsensitive);
		if(nSrcStart > 0)
		{
			int nSrcStart1 = strHtmlData.indexOf(";base64,", nSrcStart);
			if(nSrcStart1 < 0){
				nPos = nSrcStart + 1;

				//search next
				nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
				continue;
			}
			nSrcStart1 += 8; //strlen(";base64,")
			int nSrcEnd = strHtmlData.indexOf("\"", nSrcStart1);
			if(nSrcEnd > 0)
			{
				QString strBase64 = strHtmlData.mid(nSrcStart1, nSrcEnd-nSrcStart1);
				//convert from base64
				QByteArray arData = QByteArray::fromBase64(strBase64.toLatin1());

				//extract extension from mime type
				const static int nHdrLen = strlen("src=\"data:image/");
				QString strExt = strHtmlData.mid(nSrcStart+nHdrLen, nSrcStart1-nSrcStart-nHdrLen-8);	//8 -> strlen(";base64,")

				// save the image contents into the temp file
				QString strName = "SokratesImage." + strExt;
				QFileInfo attInfo(strName);
				QString strTmpPath;
				//generate unique temp name
				int nTry = 0; bool bOK = false;
				while(nTry < 1000 && !bOK){
					strTmpPath = QDir::tempPath();
					strTmpPath += "/";
					strTmpPath += attInfo.completeBaseName();
					strTmpPath += QVariant(nTry).toString();
					strTmpPath += ".";
					strTmpPath += attInfo.completeSuffix();
					nTry ++;
					bOK = !QFile::exists(strTmpPath);
				}
				if(!bOK){
					Q_ASSERT(false);	//could not create unused temp path
					//search next
					nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
					continue;
				}

				//save to file
				QFile file(strTmpPath);
				if (file.open(QIODevice::WriteOnly|QIODevice::Truncate))
				{
					file.write(arData);
					file.close();

					//write back into the string (replace old "src" value with file url)
					strHtmlData.remove(nSrcStart + strlen("src=\""), nSrcEnd-nSrcStart-strlen("src=\""));
					strHtmlData.insert(nSrcStart + strlen("src=\""), strTmpPath);

					lstResTempFiles.append(strTmpPath);
				}
			}
		}
		
		//search next
		nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
	}
}

void EmailHelper::EmbedRemoteHtmlPix(QString &strHtmlData, QStringList &lstResOrigURLS, QStringList &lstResTempFiles)
{
	lstResOrigURLS.clear();

	//find and embed each picture in the HTML
	int nPos = strHtmlData.indexOf("<img", 0, Qt::CaseInsensitive);
	while(nPos >= 0)
	{
		//extract file path
		int nSrcStart = strHtmlData.indexOf("src=\"", nPos, Qt::CaseInsensitive);
		if(nSrcStart > 0)
		{
			nSrcStart += 5; //strlen("src=\"")
			int nSrcEnd = strHtmlData.indexOf("\"", nSrcStart);
			if(nSrcEnd > 0)
			{
				QString strFile = strHtmlData.mid(nSrcStart, nSrcEnd-nSrcStart);

				//only load remote files
				if(!strFile.startsWith("http://")){
					nPos = nSrcEnd + 1;
					//search next
					nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
					continue;
				}

				//fetch remote image file using HTTP request
				QString strHost = strFile;
				QString strPage = strFile;
				strHost.remove(0, strlen("http://"));
				int nPosEnd = strHost.indexOf("/");
				if(nPosEnd >= 0){
					strPage = strHost.mid(nPosEnd);
					strHost = strHost.left(nPosEnd);
				}

				QHttp *http = new QHttp(strHost, QHttp::ConnectionModeHttp);

				//progress dialog
				QSize size(300,100);
				QProgressDialog progress(QObject::tr("Contacting Web Site..."),QObject::tr("Cancel"),0,0,NULL);
				progress.setWindowTitle(QObject::tr("Operation In Progress"));
				progress.setMinimumSize(size);
				progress.setWindowModality(Qt::NonModal);
				progress.setMinimumDuration(300);//.3s before pops up

				QBuffer httpResponse;
				int nReqID = http->get(strPage /*QUrl::toPercentEncoding(strPage)*/, &httpResponse);

				//wait up to 6 seconds for the result
				QTime timer;
				timer.start();
				bool bResult = false;
				while(1){
					QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents|QEventLoop::WaitForMoreEvents, 10);
					progress.setValue(1);

					if(0 == http->currentId())	//request finished?
						break;
					if(timer.elapsed() > 6000)	//timeout elapsed?
						break;
				}
				if(http->error() != 0){
					qDebug() << "HTTP iamge fetching error:" << http->errorString();	
				}
				else
					bResult = true;
				delete http;

				progress.close();

				if(!bResult){
					//QMessageBox::information(NULL, QObject::tr("Invalid Research"), QObject::tr("The ZIP/Town server could not be reached. Please try again later!"));
					//return;
					nPos = nSrcEnd + 1;
					//search next
					nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
					continue;
				}

				qDebug() << "HTTP image (" << timer.elapsed() << "ms elapsed) result:" << httpResponse.data();

				//convert from base64
				//QByteArray arDataBase64 = file.readAll().toBase64();
				QString strExt = "jpg";
				int nExtStart = strPage.lastIndexOf(".");
				if(nExtStart >= 0)
					strExt = strPage.mid(nExtStart+1);
				
				// save the image contents into the temp file
				QString strName = "SokratesImage." + strExt;
				QFileInfo attInfo(strName);
				QString strTmpPath;
				//generate unique temp name
				int nTry = 0; bool bOK = false;
				while(nTry < 1000 && !bOK){
					strTmpPath = QDir::tempPath();
					strTmpPath += "/";
					strTmpPath += attInfo.completeBaseName();
					strTmpPath += QVariant(nTry).toString();
					strTmpPath += ".";
					strTmpPath += attInfo.completeSuffix();
					nTry ++;
					bOK = !QFile::exists(strTmpPath);
				}
				if(!bOK){
					Q_ASSERT(false);	//could not create unused temp path
					//search next
					nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
					continue;
				}

				//save to file
				QFile file(strTmpPath);
				if (file.open(QIODevice::WriteOnly|QIODevice::Truncate))
				{
					file.write(httpResponse.data());
					file.close();

					//write back into the string (replace old "src" value with file url)
					strHtmlData.remove(nSrcStart, nSrcEnd-nSrcStart);
					strHtmlData.insert(nSrcStart, strTmpPath);

					lstResOrigURLS.append(strFile);
					lstResTempFiles.append(strTmpPath);
				}
			}
		}
		
		//search next
		nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
	}
}

int EmailHelper::GetDefaultEmailInviteTemplateForDefaultLanguage()
{
	//find default template ID:
	int nDefaultEmailTemplateID=0;
	MainEntitySelectionController cacheTypes;
	cacheTypes.Initialize(ENTITY_BUS_CM_TYPES);
	cacheTypes.GetLocalFilter()->SetFilter("BCMT_ENTITY_TYPE",ContactTypeManager::TYPE_LANGUAGE);
	cacheTypes.ReloadData();
	DbRecordSet *pLstTypes=cacheTypes.GetDataSource();
	//find def lang then extract email template id:	
	for (int i=0;i<pLstTypes->getRowCount();i++)
	{
		if (pLstTypes->getDataRef(i,"BCMT_IS_DEFAULT").toInt()>0)
			nDefaultEmailTemplateID=pLstTypes->getDataRef(i,"BCMT_EMAIL_TEMPLATE_ID").toInt();
	}

	return nDefaultEmailTemplateID;
}

void EmailHelper::FixEmailInputLine(QString &strEmail)
{
	//MR: fix email input line to remove new line and other invalid chars (#2702)
	int nPos = strEmail.indexOf("\n");
	if(nPos >= 0)
		strEmail = strEmail.mid(0, nPos);
	nPos = strEmail.indexOf("\r");
	if(nPos >= 0)
		strEmail = strEmail.mid(0, nPos);
	strEmail = strEmail.replace("(at)","@");
	strEmail = strEmail.replace("\t"," ");
	strEmail = strEmail.trimmed();
}
