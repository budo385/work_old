#include "dlg_trialpopup.h"
#include <QProcess>
#include <QDesktopServices>
#include <QFileDialog>
#include "common/common/datahelper.h"
#include <QtWidgets/QMessageBox>
#include <QUrl>

#define HTML_START_F1	"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:11pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:bold; \">"
#define HTML_END_F1		"</span></p></body></html>"
extern QString g_strLastDir_FileOpen;

Dlg_TrialPopUp::Dlg_TrialPopUp(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle(tr("Order Now"));

	m_nState=1; //by default-> exit app!

	//not available in thin mode:
	if (m_bIsThinClientMode)
	{
		ui.btnRegister->setVisible(false);
	}
	ui.btnTrial->setVisible(false);
}

Dlg_TrialPopUp::~Dlg_TrialPopUp()
{

}

void Dlg_TrialPopUp::on_btnTrial_clicked()
{
	//open web
	QDesktopServices::openUrl(m_strOrderURL);
	//m_nState=2;
	done(1);

}
void Dlg_TrialPopUp::on_btnFull_clicked()
{
	//open web
	QDesktopServices::openUrl(m_strOrderURL);
	//m_nState=2;
	done(1);
}

void Dlg_TrialPopUp::on_btnRegister_clicked()
{
	//file dlg: open keyfile or sokrates_setup.exe
	
	QString strStartDir=QDir::currentPath(); 
	QString strFilter;
	if (m_bIsThinClientMode)
		strFilter="Sokrates_Register.exe";
	else
		strFilter="*.key Sokrates_Register.exe";


	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Select keyfile or registration utility"),
		strStartDir,
		strFilter);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	

	if(strFile.isEmpty())
	{
		//m_nState=0;
		done(0);
		return;
	}

	QFileInfo file(strFile);

	if (file.suffix()=="key")
	{
		//copy key to settings dir:
		QFile keyfile(strFile);
		QString strLicensePath= DataHelper::GetApplicationHomeDir()+"/settings/sokrates.key";
		QFile keyfileTarget(strLicensePath);
		keyfileTarget.remove();
		keyfile.copy(strLicensePath);
		m_strLicensePath= strLicensePath;

		QMessageBox::information(this,tr("Warning"),tr("Please restart application, to start using registered version!"));
		
		//m_nState=2;		//restart
		done(1);
		return;
	}
	if (file.suffix()=="exe")
	{
		strFile="\""+QDir::toNativeSeparators(strFile)+"\"";
		QProcess::startDetached(strFile);
		//m_nState=2;		//shutdown
		done(1);
		return;
	}


	//m_nState=0;
	done(0);

}

void Dlg_TrialPopUp::on_btnCancel_clicked()
{
	// only if..
	m_nState=0;
	done(0);
}



void Dlg_TrialPopUp::Initialize(int nPeriodLeft, QString strOrderURL,bool bIsThinClientMode)
{
	m_bIsThinClientMode=bIsThinClientMode;
	m_strOrderURL=strOrderURL;
	m_nPeriod=nPeriodLeft;
	if (m_nPeriod==0)
	{
		ui.btnCancel->setEnabled(false);
		ui.labelStart->setText(QString(HTML_START_F1)+tr("Your trial version of SOKRATES")+QChar(174)+tr(" Communicator has expired! You can purchase a license keyfile for the unlimited version.")+QString(HTML_END_F1));
	}
	else
	{
		ui.labelStart->setText(QString(HTML_START_F1)+tr("This is a trial version of SOKRATES")+QChar(174)+tr(" Communicator. You can use it for a ")+QVariant(m_nPeriod).toString()+tr(" day trial period or you can purchase a licence keyfile for the unlimited version.")+QString(HTML_END_F1));
	}

}