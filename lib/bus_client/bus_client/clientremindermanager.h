#ifndef CLIENTREMINDERMANAGER_H
#define CLIENTREMINDERMANAGER_H

#include <QObject>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "dlg_reminders.h"

class ClientReminderManager : public QObject
{
	Q_OBJECT

public:
	ClientReminderManager(QObject *parent=NULL);
	~ClientReminderManager();

	void DismissReminders(Status &err, DbRecordSet &lstReminders);
	void ReloadUserReminders(Status &err,DbRecordSet &lstReminders);
	void PostPoneReminders(Status &err,DbRecordSet &lstReminders, int nMinutes);

	static void RecalcDueTimes(DbRecordSet &lstReminders);

public slots:
	void OnNewReminderReceived(int nMsgCode,QString strMsg);
	void OnShowDlg();
	void OnHideDlg();

private:
	void ActivateAndPositionDlg();
	void CheckDlg();

	DbRecordSet			m_lstReminders;
	Dlg_Reminders		*m_Dlg;
	//QSystemTrayIcon		*m_sysTray;
};

#endif // CLIENTREMINDERMANAGER_H
