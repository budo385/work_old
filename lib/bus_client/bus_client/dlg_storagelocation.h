#ifndef DLG_STORAGELOCATION_H
#define DLG_STORAGELOCATION_H

#include <QtWidgets/QDialog>
#ifndef WINCE
	#include "generatedfiles/ui_dlg_storagelocation.h"
#else
	#include "embedded_core/embedded_core/generatedfiles/ui_dlg_storagelocation.h"
#endif

class Dlg_StorageLocation : public QDialog
{
	Q_OBJECT

public:
	Dlg_StorageLocation(bool bStore=true,QString strDocName="", QString strDocPath="");
	~Dlg_StorageLocation();

private slots:
	void on_btnLocal_clicked();
	void on_btnInternet_clicked();
	void on_btnCancel_clicked();

private:
	Ui::Dlg_StorageLocationClass ui;
};

#endif // DLG_STORAGELOCATION_H
