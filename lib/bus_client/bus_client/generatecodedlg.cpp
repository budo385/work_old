#include "generatecodedlg.h"
#include "common/common/cliententitycache.h"
#include <QtWidgets/QMessageBox>
#include "bus_core/bus_core/hierarchicalhelper.h"

extern ClientEntityCache g_ClientCache; //global cache:

generatecodedlg::generatecodedlg(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	QPixmap pix(":SameLevel.png");
	ui.pixSameLevel->setPixmap(pix);

	QPixmap pix1(":SubLevel.png");
	ui.pixSubLevel->setPixmap(pix1);

	m_nEntityType = -1;

	ui.txtProposedName->setVisible(false);
	ui.label_Name->setVisible(false);

	m_pLstData=NULL;

	ui.chkUseTemplate->setVisible(false);
	on_chkUseTemplate_stateChanged(0);

	QTimer::singleShot(0, this, SLOT(on_DialogInitialized()));
}

generatecodedlg::~generatecodedlg()
{
}

void generatecodedlg::SetParentCode(QString strCode)
{
	m_strParentCode = strCode;
	ui.txtParentCode->setText(strCode);
}

void generatecodedlg::SetProposedCode(QString strCode)
{
	m_strProposedCode = strCode;
	ui.txtProposedCode->setText(strCode);
}

void generatecodedlg::on_btnSameLevel_clicked()
{
	Q_ASSERT(m_nEntityType >= 0);
	RefreshData();

	DbRecordSet *cacheData=GetCacheData();
	if(cacheData)
	{
		//cacheData->Dump();

		//calculate parent index
		int nIdCol = cacheData->getColumnIdx(m_strFldPrefix + "_ID");
		Q_ASSERT(nIdCol >= 0);
		int nPidCol = cacheData->getColumnIdx(m_strFldPrefix + "_PARENT");
		Q_ASSERT(nPidCol >= 0);
		int nCodeCol = cacheData->getColumnIdx(m_strFldPrefix + "_CODE");
		Q_ASSERT(nCodeCol >= 0);
		
		int nParentCode = 0;
		if(!m_strParentCode.isEmpty()){
			int nParentIdx = cacheData->find(nCodeCol, m_strParentCode, true);
			if(nParentIdx < 0)
				QMessageBox::information(this, "Warning", tr("Could not find parent node!"));
			else{
				cacheData->getData(nParentIdx, nPidCol, nParentCode);
			}
		}

		Q_ASSERT(nParentCode >= 0);
		cacheData->sort(nCodeCol, 0);	//sort by code to ease finding the last sibling
		cacheData->find(nPidCol, nParentCode, 0,0,1,1);	// select all siblings

		//search last sibling (last selected row since the cache was sorted by the code)
		int nIdx = -1;
		int nCount = cacheData->getSelectedCount();
		if(nCount > 0){
			QList<int> lstRows = cacheData->getSelectedRows();
			nIdx = lstRows.back();
		}

		//cacheData->Dump();
		//DbRecordSet selected = *cacheData;
		//selected.deleteUnSelectedRows();
		//selected.Dump();

		if(nIdx >= 0)
		{
			//
			// New code is equal to the last sibling with its last code segment incremented
			//
			QString strCode;
			cacheData->getData(nIdx, nCodeCol, strCode);
			HierarchicalHelper::IncrementLastSegment(strCode);
			SetProposedCode(strCode);
		}
	}
}

void generatecodedlg::on_btnSubLevel_clicked()
{
	Q_ASSERT(m_nEntityType >= 0);
	RefreshData();

	DbRecordSet *cacheData=GetCacheData();
	if(cacheData)
	{
		//calculate parent index
		int nIdCol = cacheData->getColumnIdx(m_strFldPrefix + "_ID");
		Q_ASSERT(nIdCol >= 0);
		int nPidCol = cacheData->getColumnIdx(m_strFldPrefix + "_PARENT");
		Q_ASSERT(nPidCol >= 0);
		int nCodeCol = cacheData->getColumnIdx(m_strFldPrefix + "_CODE");
		Q_ASSERT(nCodeCol >= 0);
		
		int nParentCode = 0;
		if(!m_strParentCode.isEmpty()){
			int nParentIdx = cacheData->find(nCodeCol, m_strParentCode, true);
			if(nParentIdx < 0)
				QMessageBox::information(this, "Warning", "Could not find parent node!");
			else{
				cacheData->getData(nParentIdx, nIdCol, nParentCode);
			}
		}

		Q_ASSERT(nParentCode >= 0);

		// select all siblings (next search will be within this range)
		cacheData->sort(nCodeCol, 0);	//sort by code to ease finding the last sibling
		cacheData->find(nPidCol, nParentCode, 0,0,1,1);	

		//search last sibling (last selected row since the cache was sorted by the code)
		int nIdx = -1;
		int nCount = cacheData->getSelectedCount();
		if(nCount > 0){
			QList<int> lstRows = cacheData->getSelectedRows();
			nIdx = lstRows.back();
		}

		//cacheData->Dump();

		QString strCode;
		if(nIdx >= 0)
		{
			cacheData->getData(nIdx, nCodeCol, strCode);

			//before incrementing strip the code to the proper length (number of segments)
			// For example we have a parent code "P.2" with a single child "P.2.4.6.7."
			// correct sub-level proposal should be "P.2.5." (NOT "P.2.4.6.8.")
			int nNumSegments = HierarchicalHelper::CalcCodeSegmentsCount(m_strParentCode);
			strCode = HierarchicalHelper::CutToTheNumberOfSegments(strCode, nNumSegments+1);

			HierarchicalHelper::IncrementLastSegment(strCode);
			SetProposedCode(strCode);
		}
		else
		{
			strCode = m_strParentCode;
			HierarchicalHelper::GenerateChildCode(strCode);
			SetProposedCode(strCode);
		}
	}
}

void generatecodedlg::on_btnOK_clicked()
{
	RefreshData();

	//ensure the code is terminated with "."
	if(m_strProposedCode.right(1) != ".")
		m_strProposedCode += ".";

	// validate proposed code (must not exist in the cache)
	DbRecordSet *cacheData=GetCacheData();
	if(cacheData)	
	{
//		cacheData->Dump();
		int nCodeCol = cacheData->getColumnIdx(m_strFldPrefix + "_CODE");
		Q_ASSERT(nCodeCol >= 0);

		int nExistingIdx = cacheData->find(nCodeCol, m_strProposedCode, true);
		if(nExistingIdx >= 0)
		{
			QMessageBox::information(this, "Warning", tr("This code is already taken!\nPlease try another one."));
			return;
		}
	}

	done(1);
}

void generatecodedlg::on_btnCancel_clicked()
{
	done(0);
}

void generatecodedlg::RefreshData()
{
	m_strParentCode = ui.txtParentCode->text();
	m_strProposedCode = ui.txtProposedCode->text();
	m_strProposedName = ui.txtProposedName->text();
}

void generatecodedlg::EnableNameInput()
{
	ui.txtProposedName->setVisible(true);
	ui.label_Name->setVisible(true);
}

void generatecodedlg::EnableTemplateInput()
{
	ui.chkUseTemplate->setVisible(true);
	ui.chkUseTemplate->setChecked(true);
	
	MainEntityFilter filter;
	filter.SetFilter(m_strFldPrefix + "_TEMPLATE_FLAG", 1);
	ui.frmTemplateSelector->Initialize(m_nEntityType);
	ui.frmTemplateSelector->GetSelectionController()->SetLocalFilter(filter);
	ui.frmTemplateSelector->GetSelectionController()->ReloadData();						//refresh content
}

DbRecordSet* generatecodedlg::GetCacheData()
{
	if (m_pLstData)	//first lookup local data:
		return m_pLstData;
	else
		return g_ClientCache.GetCache(m_nEntityType);
}

void generatecodedlg::on_chkUseTemplate_stateChanged(int nChecked)
{
	ui.frmTemplateSelector->setVisible(nChecked);
}


//B.T. new sapne: get code & name from selected record
int generatecodedlg::GetTemplateData(QString &strCode,QString &strName)
{
	if(ui.chkUseTemplate->isChecked())
	{
		int nID;
		DbRecordSet record;
		if (ui.frmTemplateSelector->GetCurrentEntityRecord(nID,record))
		{
			if (ui.frmTemplateSelector->GetSelectionController()->m_nCodeColumnIdx!=-1)
				strCode=record.getDataRef(0,ui.frmTemplateSelector->GetSelectionController()->m_nCodeColumnIdx).toString();
			
			if (ui.frmTemplateSelector->GetSelectionController()->m_nNameColumnIdx!=-1)
				strName=record.getDataRef(0,ui.frmTemplateSelector->GetSelectionController()->m_nNameColumnIdx).toString();

			return nID;
		}
	}

	return -1;
}

void generatecodedlg::on_DialogInitialized()
{
	//if in template mode, trigger popup for selecting a template project
	if( ui.chkUseTemplate->isVisible() &&
		ui.chkUseTemplate->isChecked())
	{
		((QToolButton *)ui.frmTemplateSelector->GetButton(Selection_SAPNE::BUTTON_SELECT))->click();
	}
}
