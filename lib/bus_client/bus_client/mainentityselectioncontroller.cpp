#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/universaltablewidget.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/countries.h"
#include "bus_core/bus_core/nmrxmanager.h"
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "common/common/datahelper.h"
#include <QApplication>

//GLOBALS:
#include "bus_client/bus_client/changemanager.h"			
#include "common/common/cliententitycache.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "bus_client/bus_client/clientmanagerext.h"

extern ClientEntityCache g_ClientCache;				//global cache
extern ChangeManager g_ChangeManager;				//global message dispatcher
extern BusinessServiceManager *g_pBoSet;						//Bo set
extern ClientManagerExt *g_pClientManager;

 //defines FP codes
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester


MainEntitySelectionController::MainEntitySelectionController()
:m_nEntityID(-1),m_nDataTypeID(TYPE_TABLE),m_bLocalData(false),m_bShowOnlyUserContacts(false),m_PersonCache(NULL)
{
	m_bForceFullProjectDataReload=false;
	m_nPrimaryKeyColumnIdx=-1;
	m_nCodeColumnIdx=-1;
	m_nNameColumnIdx=-1;
	m_nLevelColumnIdx	= -1;
	m_nParentColumnIdx	= -1;
	m_strTablePrefix="";
	m_bProjectHideInvisible = true;
}



/*
	Holds all of info needed for init one selection controller for one entity type

	\param nEntityID	- ID of entity data
*/
void MainEntitySelectionController::Initialize(int nEntityID)
{
	//register data:
	m_nEntityID=nEntityID;
	g_ChangeManager.registerObserver(this);

	

	//init specific data:
	switch(m_nEntityID)
	{

		//----------------------------------------
		//				TABLE
		//----------------------------------------
		case ENTITY_BUS_PERSON:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON_SELECTION));

				//init m_TableColumns
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BPER_CODE",QObject::tr("Code"),60,false);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"IS_LOGIN_ACC",QObject::tr("Is Login"),60,false,"",UniversalTableWidget::COL_TYPE_CHECKBOX);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BPER_NAME",QObject::tr("Name"),100,false);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BPER_DEPT_NAME",QObject::tr("Dept. Name"),100,false);

				//init table widget with our custom view
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_PERSON));
				
				break;
			}
		case ENTITY_CORE_USER:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USER_SELECTION_SHORT));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"CUSR_USERNAME",QObject::tr("Username"),80,false);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"CUSR_NAME",QObject::tr("Name"),80,false);

				//init table widget with our custom view
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(CORE_USER));
				
				break;
			}
		case ENTITY_BUS_CM_TYPES_FUI_SELECTOR:
			{
				//load list:
				ContactTypeManager::GetEntityTypeList(m_lstData);

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"ENTITY_TYPE_NAME",QObject::tr("Type"),110,false);

				m_nPrimaryKeyColumnIdx=m_lstData.getColumnIdx("ENTITY_TYPE_ID");
				m_nNameColumnIdx=m_lstData.getColumnIdx("ENTITY_TYPE_NAME");
				m_nCodeColumnIdx=-1;

				m_bLocalData=true; //no server nor cache use
				
				break;
			}

		case ENTITY_NMRX_SELECTOR:
			{
				//load list:
				NMRXManager::GetRolePairList(m_lstData);

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"PAIR_NAME",QObject::tr("Role NM Pairs"),110,false);

				m_nPrimaryKeyColumnIdx=m_lstData.getColumnIdx("PAIR_ID");
				m_nNameColumnIdx=m_lstData.getColumnIdx("PAIR_NAME");
				m_nCodeColumnIdx=-1;

				m_bLocalData=true; //no server nor cache use
				
				break;
			}

		case ENTITY_COUNTRIES:
			{
				//load list:
				Countries::GetCountries(m_lstData);

				//register this cont to countries for lang change...
				m_nPrimaryKeyColumnIdx=-1;
				m_nCodeColumnIdx=-1;
				m_nNameColumnIdx=-1;

				m_bLocalData=true; //no server nor cache use
				
				break;
			}

		case ENTITY_BUS_CAL_PARTS_SELECTION:
			{
				//load list:
				m_lstData.addColumn(QVariant::Int,"BCEP_ID");
				m_lstData.addColumn(QVariant::String,"BCOL_SUBJECT");

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BCOL_SUBJECT",QObject::tr("Part"),110,false);

				//register this cont to countries for lang change...
				m_nPrimaryKeyColumnIdx=m_lstData.getColumnIdx("BCEP_ID");
				m_nNameColumnIdx=m_lstData.getColumnIdx("BCOL_SUBJECT");
				m_nCodeColumnIdx=-1;

				m_bLocalData=true; //no server nor cache use

				break;
			}

		case ENTITY_BUS_CM_TYPES:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_TYPES));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BCMT_TYPE_NAME",QObject::tr("Name"),140,false);

				//init table widget with our custom view
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CM_TYPES));
				m_nNameColumnIdx=m_lstData.getColumnIdx("BCMT_TYPE_NAME");
				
				break;
			}
		case ENTITY_CE_EVENT_TYPES:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_EVENT_TYPE));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"CEVT_CODE",QObject::tr("Code"),80,false);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"CEVT_NAME",QObject::tr("Name"),80,false);

				//init table widget with our custom view
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(CE_EVENT_TYPE));
				break;
			}


		case ENTITY_BUS_CONTACT:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"","",35,false); //issue 1634: dummy datasource: all handled manually add numbers inside grid
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BCNT_NAME",QObject::tr("Contact"),600,false); //issue 1634: no last section stretch->enable horz scroll bar

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CM_CONTACT));
				break;
			}


		case ENTITY_CONTACT_ORGANIZATION:
		case ENTITY_CONTACT_DEPARTMENT:
		case ENTITY_CONTACT_PROFESSION:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_SUBSET_SELECTION));
				
				//m_lstData.Dump();

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"NAME",QObject::tr("Name"),120,false);

				//set column indexes, its ok for Code & Name to be -1
				m_nPrimaryKeyColumnIdx=0;
				m_nNameColumnIdx=1;
				break;
			}
		case ENTITY_CONTACT_FUNCTION:
		case ENTITY_DOC_TEMPLATE_CATEGORY:
		case ENTITY_EMAIL_TEMPLATE_CATEGORY:
		case ENTITY_CALENDAR_EVENT_TEMPLATE_CATEGORY:
			{
				//define list:
				m_lstData.destroy();
				m_lstData.addColumn(QVariant::String,"NAME");

				//m_lstData.Dump();

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"NAME",QObject::tr("Name"),120,false);

				//set column indexes, its ok for Code & Name to be -1
				m_nParentColumnIdx=-1;
				m_nNameColumnIdx=0;
				break;
			}
		case ENTITY_BUS_CM_ADDRESS_SCHEMAS:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SCHEMAS));

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CM_ADDRESS_SCHEMAS));
				break;
			}

		case ENTITY_BUS_OPT_GRID_VIEWS:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_GRID_VIEWS));

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_OPT_GRID_VIEWS));
				break;
			}

		case ENTITY_CONTACT_GROUPS:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_GRID_VIEWS));

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_OPT_GRID_VIEWS));
				break;
			}

		case ENTITY_NMRX_ROLES:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_ROLE));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BNRO_NAME",QObject::tr("Role"),80,false);
				//UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BNRO_ASSIGMENT_TEXT",QObject::tr("Contact"),120,false);

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_NMRX_ROLE));
				break;
			}

		case ENTITY_BUS_DM_APPLICATIONS:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BDMA_NAME",QObject::tr("Application Name"),240,false);
				
				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_DM_APPLICATIONS));
				m_nCodeColumnIdx=-1; //ignore code field
				break;
			}

		case ENTITY_BUS_DM_TEMPLATES:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BDMD_NAME",QObject::tr("Template Name"),240,false);

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_DM_DOCUMENTS));
				break;
			}
			

		case ENTITY_BUS_CM_GROUP: //group-contact NM pattern
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BGTR_NAME",QObject::tr("Tree"),100,false);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BGIT_ITEM_NAME",QObject::tr("Group"),100,false);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BGCN_CMCA_VALID_FROM",QObject::tr("Valid From"),80,false);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BGCN_CMCA_VALID_TO",QObject::tr("Valid To"),80,false);

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CM_GROUP));
				break;
			}

		case ENITITY_ALL_EMAIL_ADDRESSES:
			{
				//define list:
				m_lstData.destroy();
				m_lstData.addColumn(QVariant::String,"EMAIL");

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"EMAIL",QObject::tr("EMail"),100,false);

				//set column indexes, its ok for Code & Name to be -1
				//SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CM_GROUP));
				break;
			}
		case ENTITY_PERSON_EMAIL_ADDRESS_SELECTOR:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BCME_ADDRESS",QObject::tr("Email"),100,false);

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CM_EMAIL));
				m_nNameColumnIdx=m_lstData.getColumnIdx("BCME_ADDRESS");
				break;
			}
		

		case ENTITY_CALENDAR_EVENT_TEMPLATES:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BCEV_TITLE",QObject::tr("Template Name"),240,false);
				
				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CAL_EVENT));
				m_nNameColumnIdx=m_lstData.getColumnIdx("BCEV_TITLE");

				break;
			}
		case ENTITY_BUS_EMAIL_TEMPLATES:
			{
				//define list:
				m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_TEMPLATES));

				//redefine table look:
				m_TableColumns.destroy();
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BEM_CATEGORY",QObject::tr("Category Name"),140,false);
				UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BEM_TEMPLATE_NAME",QObject::tr("Template Name"),240,false);

				//set column indexes, its ok for Code & Name to be -1
				SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_EMAIL));
				m_nNameColumnIdx=m_lstData.getColumnIdx("BEM_TEMPLATE_NAME");

				break;
			}
			
		//----------------------------------------
		//				TREE
		//----------------------------------------
		case ENTITY_BUS_DEPARTMENT:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DEPARTMENT_SELECTION));
			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_DEPARTMENT)); //get column indexes
			break;
		case ENTITY_BUS_ORGANIZATION:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_ORGANIZATION_SELECTION));
			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_ORGANIZATION)); //get column indexes
			break;
		case ENTITY_BUS_COST_CENTER:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_COST_CENTER_SELECTION));
			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_COST_CENTER)); //get column indexes
			break;
		case ENTITY_CE_TYPES:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_TYPE_SELECT));
			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(CE_TYPE)); //get column indexes
			break;
			
		case ENTITY_BUS_PROJECT:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_PROJECT)); //get column indexes
			break;

		case ENTITY_CALENDAR_VIEW:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW));
			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CAL_VIEW)); //get column indexes

			//init m_TableColumns
			m_TableColumns.destroy();
			UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BCALV_NAME",QObject::tr("Name"),150,false);
			break;
		
		//----------------------------------------
		//				GROUP ITEMS
		//----------------------------------------
		case ENTITY_BUS_GROUP_ITEMS:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_GROUP_ITEMS));
			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_GROUP_ITEMS)); //get column indexes
			break;

		case ENTITY_CALENDAR_RESOURCES:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_RESOURCE_SELECT));

			//init m_TableColumns
			m_TableColumns.destroy();
			UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BRES_NAME",QObject::tr("Name"),150,false);

			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CAL_RESOURCE)); //get column indexes
			break;


		case ENTITY_PAYMENT_CONDITIONS:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PAYMENT_CONDITIONS_SELECT));

			//init m_TableColumns
			m_TableColumns.destroy();
			UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BCMPY_NAME",QObject::tr("Name"),150,false);

			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CM_PAYMENT_CONDITIONS)); //get column indexes
			break;

		case ENTITY_CUSTOM_FIELDS:
			m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS_SELECT));

			//init m_TableColumns
			m_TableColumns.destroy();
			UniversalTableWidget::AddColumnToSetup(m_TableColumns,"BCF_NAME",QObject::tr("Field Name"),150,false);
			UniversalTableWidget::AddColumnToSetup(m_TableColumns,"TABLE_NAME",QObject::tr("Table"),50,false);

			SetColumnIndexes(DbSqlTableDefinition::GetTablePrefix(BUS_CUSTOM_FIELDS)); //get column indexes
			break;

		default:
			Q_ASSERT_X(false, "MainEntitySelectionController::Initialize", "Unsupported entity!");
			break;

	}

	m_nDataTypeID=GetDataType(m_nEntityID);
	m_CalcName.Initialize(nEntityID,m_lstData,m_nCodeColumnIdx,m_nNameColumnIdx);
}

MainEntitySelectionController::~MainEntitySelectionController()
{
	g_ChangeManager.unregisterObserver(this);
	if(m_PersonCache) delete m_PersonCache;
}



/*! Catches global cache events

	\param pSubject			- source of msg
	\param nMsgCode			- msg code
	\param val				- value sent from observer
*/
void MainEntitySelectionController::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	Q_ASSERT(m_nEntityID >= 0); //check if initialized
	if(nMsgDetail!=m_nEntityID) return; //not our entity, exit
	if (pSubject!=&g_ChangeManager) return;

	//IF FUI sends signal that entity data is changed:
	if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
	{
		//CallBack to refresh displayed data:
		DbRecordSet lstNewData=val.value<DbRecordSet>();
		//lstNewData.Dump();
		//m_lstData.Dump();
		RefreshFromGlobalCache(nMsgCode,&lstNewData);
	}

}


//-----------------------------------------------------------------
//			DATA
//-----------------------------------------------------------------

//reloads data from server or cache
bool MainEntitySelectionController::ReloadData(bool bReloadFromServer)
{
	if(m_bLocalData) return true;

	//if from server, reload:
	if(bReloadFromServer)
		return ReloadFromServer();

	//if initial reload, then check on server if all entity data is here, proceed from cache, if not...load
	if (m_lstData.getRowCount()==0)
	{
		return LoadAllFromServer();
	}

	//try cache, if fails try server:
	if(!ReloadFromCache())
		return ReloadFromServer();
	else
		return true;
}



/*
	Reloads data from server
*/
bool MainEntitySelectionController::ReloadFromServer()
{
	Q_ASSERT(m_nEntityID >= 0);		//check if initialised
	if(m_bLocalData) return true;	//if hardcoded data return

	if (m_nEntityID==ENTITY_BUS_PROJECT)
		return LoadProjectsFromServer();

	Status err;

	//load from server, send filter data to server...
	_SERVER_CALL(MainEntitySelector->ReadData(err,m_nEntityID,m_FilterData.Serialize(),m_lstData))
	_CHK_ERR_RET_BOOL_ON_FAIL(err);
	
	if(m_FilterData.IsEmpty()) //if filter is empty store new data from server inside global cache
	{
		g_ClientCache.SetCache(m_nEntityID,m_lstData); 
		g_ClientCache.SetCacheReloadCurrentTime(m_nEntityID);
	}
	
	ApplyLocalFilter();						//filter locally if needed
	RefreshDisplay();						//call virtual funct to refresh display on selector widget
	notifyObservers(SelectorSignals::SELECTOR_DATA_CHANGED);	//emit signal that data is changed in this controller
	g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,m_nEntityID,0,this); //issue 1927: notify all other controllers
	return true;


}

bool MainEntitySelectionController::LoadAllFromServer()
{
	Q_ASSERT(m_nEntityID >= 0);				//check if initialised
	if(m_bLocalData) return true;			//if hardcoded data return
	if (m_nEntityID==ENTITY_BUS_CONTACT)	//if contact, skip this->have build actual list button
		return true;

	//if entity does not need reload test, use cache
	if(!g_ClientCache.IsCacheNeedReload(m_nEntityID))
	{
		return ReloadFromCache();
	}

	if (m_nEntityID==ENTITY_BUS_PROJECT)
		return LoadProjectsFromServer();

	Status err;
	int nCount=0;
	QDateTime datEmpty; //not used
	DbRecordSet *pCacheData=g_ClientCache.GetCache(m_nEntityID);
	if (pCacheData)
	{
		nCount=pCacheData->getRowCount();
	}

	DbRecordSet newData;

	//load all from server, filters are ignored
	_SERVER_CALL(MainEntitySelector->ReadDataAll(err,m_nEntityID,nCount,datEmpty,newData))
	//error:
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,QObject::tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return false;
	}

	//set reload time to avoid unnecessary repetitive server calls if bunch of selectors are created
	g_ClientCache.SetCacheReloadCurrentTime(m_nEntityID);

	if (newData.getRowCount()==0)
	{
		return ReloadFromCache();
	}
	else
	{
		//replace data:
		m_lstData=newData;
	#ifdef _DEBUG
		//m_lstData.Dump();
	#endif

		g_ClientCache.SetCache(m_nEntityID,m_lstData); 

		ApplyLocalFilter();						//filter locally if needed
		RefreshDisplay();						//call virtual fun ct to refresh display on selector widget
		notifyObservers(SelectorSignals::SELECTOR_DATA_CHANGED);	//emit signal that data is changed in this controller
		if (m_nEntityID != ENTITY_CALENDAR_RESOURCES)	//If calendar resource skip this because of calendar crash.
		{
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD, m_nEntityID, 0, this); //issue 1927: notify all other controllers
		}

		return true;
	}

}





/*
	Reloads data from cache, if entity ID is matched, 

	\param nEntityID	- ID of entity data
	\return				- false if there is no data in cache or expired
*/
bool MainEntitySelectionController::ReloadFromCache()
{
	Q_ASSERT(m_nEntityID >= 0);					//check if initialised
	if(m_bLocalData) return true;				//if hardcoded data return
	if(!m_FilterData.IsEmpty()) return false;	//if filter is set do not touch cache

	DbRecordSet *data=g_ClientCache.GetCache(m_nEntityID);
	if(data) //only reload if not expired && if exists
	{
		m_lstData=*data;						//copy...<-------
		ApplyLocalFilter();						//filter locally if needed
		RefreshDisplay();						//call virtual funct to refresh display on selector widget
		notifyObservers(SelectorSignals::SELECTOR_DATA_CHANGED);	//emit signal that data is changed in this controller
		return true;
	}
	else
	{
		return false;
	}
}






//Accepts ID and look up in the cache for name/lastname other data to assemble calculated name
//Reloads data from server if not found in cache
//if calculation is not needed/supported, returns empty string
QString MainEntitySelectionController::GetCalculatedName(int nEntityRecordID)
{
	//if zero, return:
	if(nEntityRecordID<1)
		return "";

	DbRecordSet tmpData;
	if(GetEntityRecord(nEntityRecordID,tmpData))
	{
		return m_CalcName.GetCalculatedName(tmpData,0);		//return calc name
	}
	else
	{
		//Q_ASSERT_X(false,"MAIN SELECTOR",QString(QString("RECORD WITH ID=")+QVariant(nEntityRecordID).toString()+QString(" DOES NOT EXIST IN DB. DB CORRUPTED-ORPHAN RECORD DETECTED")).toLatin1());
		return ""; //no record exits in database
	}
}

QString MainEntitySelectionController::GetCalculatedNameFromRecord(DbRecordSet &recData)
{
	if (recData.getRowCount()==0)
		return "";
	return m_CalcName.GetCalculatedName(recData,0);		//return calc name
}





//sets column indexes:
void MainEntitySelectionController::SetColumnIndexes(QString strTablePrefix)
{
	m_nPrimaryKeyColumnIdx	= m_lstData.getColumnIdx(strTablePrefix+"_ID");
	m_nCodeColumnIdx		= m_lstData.getColumnIdx(strTablePrefix+"_CODE");
	m_nNameColumnIdx		= m_lstData.getColumnIdx(strTablePrefix+"_NAME");
	m_nLevelColumnIdx		= m_lstData.getColumnIdx(strTablePrefix + "_LEVEL");
	m_nParentColumnIdx		= m_lstData.getColumnIdx(strTablePrefix + "_PARENT");
	m_nHasChildrenColumnIdx	= m_lstData.getColumnIdx(strTablePrefix + "_HASCHILDREN");

	m_strTablePrefix=strTablePrefix;
}



//when local filter is set, after reload from server or cache, data is store in filtered format
//LIMITATION OF LOCAL FILTER IS THAT COLUMN = VALUE(INT)  AND OP between multiple filters...
void MainEntitySelectionController::ApplyLocalFilter()
{
	FilterUserContacts();

	//if local filter is empty, return
	if(m_LocalFilterData.IsEmpty()) return;
	
 	m_lstData.selectAll();

	//m_lstData.Dump();
	DbRecordSet m_lstFilterRecords=m_LocalFilterData.Serialize();
	int nSize=m_lstFilterRecords.getRowCount();
	
	//WARNING: if null value -> will be zero: all NULLS will be selected if find 0
	m_lstData.Dump();
	m_lstFilterRecords.Dump();

	for (int i=0;i<nSize;++i)
	{
		if(m_lstFilterRecords.getDataRef(i,0).toInt()==MainEntityFilter::FILTER_SQL_WHERE)
		{
			int nValue=m_lstFilterRecords.getDataRef(i,3).toInt();
			m_lstData.find(m_lstData.getColumnIdx(m_lstFilterRecords.getDataRef(i,2).toString()), nValue,false,true,true,true);
		}
	}

	
	m_lstData.deleteUnSelectedRows();
	m_lstData.clearSelection();

	m_lstData.Dump();
}





//accepts list of entity id's
//check in main cache if they exists
//if not exists they will be loaded and put in the cache (no emit observer signal)
bool MainEntitySelectionController::BatchReadEntityData(QList<int> lstEntityIDs)
{
	
	DbRecordSet lstEntityID,lstFilters,lstResults;
	DbRecordSet *data;

	lstEntityID.addColumn(QVariant::Int,"ID");

	//find not loaded 
	int nSize=lstEntityIDs.size();
	for(int i=0;i<nSize;++i)
	{
		data=g_ClientCache.GetCache(lstEntityIDs.at(i));
		if(data==NULL)
		{
			lstEntityID.addRow();
			lstEntityID.setData(lstEntityID.getRowCount()-1,0,lstEntityIDs.at(i));
		}
	}

	//load into cache
	Status err;
	if (lstEntityID.getRowCount()>0)
	{
		//read, accept only not errored, if error, do not notify
		_SERVER_CALL(MainEntitySelector->ReadDataBatch(err,lstEntityID,lstFilters,lstResults));
		_CHK_ERR_RET_BOOL_ON_FAIL(err);
		int nSize=lstResults.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			if (lstResults.getDataRef(i,1).toInt()==0)
			{
				int nEntityID=lstEntityID.getDataRef(i,0).toInt();
				DbRecordSet row = lstResults.getDataRef(i,0).value<DbRecordSet>();
				g_ClientCache.SetCache(nEntityID,row);
				g_ClientCache.SetCacheReloadCurrentTime(nEntityID);
			}
		}
	}


	return true;

}



void MainEntitySelectionController::SetCurrentEntityRecord(int nEntityRecordID,bool bSkipNotifyObservers)
{
	m_lstData.clearSelection();
	int nRow=m_lstData.find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
	if(nRow!=-1)
	{
		m_lstData.selectRow(nRow);
		if(!bSkipNotifyObservers)notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nEntityRecordID);
		RefreshDisplay();
	}
	else
	{
		if (nEntityRecordID>0) //if id is valid, and not found: try to reload all entity from server (can be after project/hierarhical insert when no cache is updated)
		{
			ReloadFromServer();
			int nRow=m_lstData.find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
			if(nRow!=-1)
			{
				m_lstData.selectRow(nRow);
				if(!bSkipNotifyObservers)notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nEntityRecordID);
				RefreshDisplay();
			}
		}
	}

}



/*
	Change global cache, notify others
	Data in m_lstData is already changed or about to change, change cache, notfiy others.
	Note: use only when data is changed inside DB

	\param nOperation				- ENTITY_DATA_DELETED, ENTITY_DATA_INSERTED, ENTITY_DATA_EDITED
	\param bOperateOnSelectedRows	- if true then new data/deleted is selected inside m_lstData
	\param newData					- if bOperateOnSelectedRows=false,this must be set, contains new data

*/
void MainEntitySelectionController::RefreshToGlobalCache(int nOperation, bool bOperateOnSelectedRows,DbRecordSet *newData)
{
	//if selected:
	DbRecordSet lstNewData;
	if (bOperateOnSelectedRows)
	{
		lstNewData.copyDefinition(m_lstData);
		lstNewData.merge(m_lstData,true);
		newData=&lstNewData;
	}

	Q_ASSERT(newData);	//must be valid

	if (newData->getRowCount()==0) return;		//if empty exit

	
	//MODIFY CACHE
	int nSignal;
	if (nOperation==CACHE_UPDATE_DATA_EDITED)nSignal=DataHelper::OP_EDIT;
	if (nOperation==CACHE_UPDATE_DATA_DELETED)nSignal=DataHelper::OP_REMOVE;
	if (nOperation==CACHE_UPDATE_DATA_INSERTED)nSignal=DataHelper::OP_ADD;

	g_ClientCache.ModifyCache(m_nEntityID,*newData,nSignal,this);

}


/*
	Internally used: when global signal received, refresh data.
	Three:
	1. if no filter set, always refresh
	2. if local filter set, applylocalfilter afterwards
	3. if db filter set, only apply edit/delete, ignore add

	\param nOperation				- ENTITY_DATA_DELETED, ENTITY_DATA_INSERTED, ENTITY_DATA_EDITED
	\param bOperateOnSelectedRows	- if true then new data/deleted is selected inside m_lstData
	\param newData					- if bOperateOnSelectedRows=false,this must be set, contains new data

*/
void MainEntitySelectionController::RefreshFromGlobalCache(int nOperation, DbRecordSet *newData)
{

	int nPreviousSelection=0;
	if (m_lstData.getSelectedCount()>0 && m_nPrimaryKeyColumnIdx!=-1)
	{
		nPreviousSelection=m_lstData.getSelectedRecordSet().getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
	}
	
	//int nPkID=newData->getColumnIdx(m_strTablePrefix+"_ID");	//try to gert PK column for precise refresh


	switch (nOperation)
	{
		case ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED: //entity record is added: add no matter if filter is set or not,....
			{
				if (!m_FilterData.IsEmpty()) return;	//abort if db filter

				//issue 2218: avoid duplicates in same cases in sidebar mode: if IF already exists in list, skip refresh
				int nEntityRecordID=newData->getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
				int nRow=m_lstData.find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
				if(nRow!=-1)
					return;

				DataHelper::DataOperation(DataHelper::OP_ADD,newData,&m_lstData);
				ApplyLocalFilter();

/*
				if (nPkID!=-1 && newData->getRowCount()==1 && m_nPrimaryKeyColumnIdx!=-1) //special case if 1 item (save on refresh)
				{
					int nID=newData->getDataRef(nPkID,0).toInt();
					if(m_lstData.find(m_nPrimaryKeyColumnIdx,nID,true)!=-1) //it is possible that it not exists, check it
						RefreshDisplay(REFRESH_ITEM_ADD,nID); 
					else
						RefreshDisplay(); //all
				}
				else
					RefreshDisplay(); //all
*/
				RefreshDisplay(REFRESH_ITEM_ADD,nPreviousSelection);
			}
			break;

		case ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED: //entity record is edited, try to edit: 
			{
				//Miro request: new is added:
				//if (m_nEntityID==ENTITY_BUS_CONTACT)
				//	DataHelper::DataOperation(DataHelper::OP_EDIT_ONLY,newData,&m_lstData);	//if use below, then strange contacts will appear in list
				//else
					DataHelper::DataOperation(DataHelper::OP_EDIT,newData,&m_lstData); 
				//m_lstData.Dump();
				ApplyLocalFilter();
				//m_lstData.Dump();
				

				//special case for HCT: if node code edited, it will be reloaded: not to worry about that here: GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD is fired then
/*
				if (nPkID!=-1 && newData->getRowCount()==1 && m_nPrimaryKeyColumnIdx!=-1) //special case if 1 item (save on refresh)
				{
					int nID=newData->getDataRef(0,nPkID).toInt();
					if(m_lstData.find(m_nPrimaryKeyColumnIdx,nID,true)!=-1) //it is possible that it not exists, check it
						RefreshDisplay(REFRESH_ITEM_EDIT,nID); 
					else
						RefreshDisplay(); //all
				}
				else
					RefreshDisplay(); //all
		*/
					
				RefreshDisplay(REFRESH_ITEM_EDIT,nPreviousSelection);
				
			}
			break;
		
		case ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED: //entity record is edited, try to delete: 
			{
				DataHelper::DataOperation(DataHelper::OP_REMOVE,newData,&m_lstData);

//				m_lstData.Dump();

				//special case for HCT: delete childrens also (probably already removed from cache):
				if (m_nDataTypeID==TYPE_TREE ||m_nDataTypeID==TYPE_GROUP_ITEMS)
				{
					//reload whole data from server
					Q_ASSERT(false); //--->>> NO WAY THAT I CAN RECONSTRUCT HIERARCHY ON CLIENT, please reload from server & notify all controllers then
/*
					QString strCodeCol=m_strTablePrefix+"_CODE";
					int nCodeCol=newData->getColumnIdx(strCodeCol);	//try to get code colum
					if (nCodeCol!=-1)
					{
						int nSize1=newData->getRowCount();
						for (int i=0;i<nSize1;++i)
						{
							if(HierarchicalHelper::SelectChildren(newData->getDataRef(i,nCodeCol).toString(),m_lstData,strCodeCol)>0)
								m_lstData.deleteSelectedRows();
						}
					}
*/
				}
				
//				m_lstData.Dump();
				
/*
				if (nPkID!=-1 && newData->getRowCount()==1 && m_nPrimaryKeyColumnIdx!=-1) //special case if 1 item
				{
					int nID=newData->getDataRef(0,nPkID).toInt();
					//if(m_lstData.find(m_nPrimaryKeyColumnIdx,nID,true)!=-1)//it is possible that it not exists, check it
					RefreshDisplay(REFRESH_ITEM_DELETE,nID); 
					//else
					//	RefreshDisplay(); //all
				}
				else
					RefreshDisplay(); //all
					*/

				RefreshDisplay(REFRESH_ITEM_DELETE,nPreviousSelection);
			}
			break;
		
		case ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD:
			{
				
				//reload data:// only used in HCT entites when code is edited
				ReloadData();
				return;
			}
			break;
	}


	//some1 will use this signal to refresh data instead of RefreshDisplay()
	notifyObservers(SelectorSignals::SELECTOR_DATA_CHANGED);	//emit signal that data is changed in this controller


}


//sometimes used for calculating names:
//do not refresh cache
void MainEntitySelectionController::LoadOneFromServer(int nRecordID,DbRecordSet &data)
{
	Status err;
	MainEntityFilter filter;
	QString strPkID=m_strTablePrefix+"_ID";	//try to get PK column for precise refresh
	filter.SetFilter(strPkID,nRecordID);
	//DbRecordSet data;


	//load from server, send filter data to server...NOTE: if server decide it can return all entity data records
	_SERVER_CALL(MainEntitySelector->ReadDataOne(err,m_nEntityID,filter.Serialize(),data))
	//error:
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,QObject::tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
	}
	else
	{
		int nSize=data.getRowCount();
		if (nSize==1)		//if only one-> add to cache
			g_ClientCache.ModifyCache(m_nEntityID,data,DataHelper::OP_EDIT,this,true);  //from ADD-> to edit
		else if (nSize>1)  //if more, filter our, set all into cache
		{
			g_ClientCache.SetCache(m_nEntityID,data);
			g_ClientCache.SetCacheReloadCurrentTime(m_nEntityID); //should be always after setcache...
		}

	}
}


//get record as defined in selection controller (SELECT view for project, contacts,...)
//gets from cache first, if not from then from  server, else return false
bool MainEntitySelectionController::GetEntityRecord(int &nEntityRecordID,DbRecordSet &record)
{

	int nRow=-1;
	DbRecordSet *data;
	DbRecordSet rowTmpData;

	if (m_bLocalData) 
	{
		nRow=m_lstData.find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true); //if local get from list
		data=&m_lstData;
		if (nRow==-1)
		{
			return false;
		}
		else
		{
			record.copyDefinition(*data);
			record.addRow();
			DbRecordSet row = data->getRow(nRow);
			record.assignRow(0,row);
			return true;
		}
	}

	//if not local, skip if 0
	if (nEntityRecordID<=0)
		return false;
		

	//first try cache, as cache can contain whole entity data (no filters)
	data=g_ClientCache.GetCache(m_nEntityID);
	
	if(data)
	{
		//data->Dump();
		nRow=data->find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
	}

	//if not found, try local instance data, if filter is set, or for contacts, local copy can contain more data then cache
	if (nRow==-1)
	{
		nRow=m_lstData.find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true); //if local get from list
		data=&m_lstData;
	}

	//if not found load 1 record from server: //sometimes better to load all....depends on data....
	if (nRow==-1)
	{
		
		LoadOneFromServer(nEntityRecordID,rowTmpData); //load data from server, update cache, but do not change state of this controller
		data=&rowTmpData;
		nRow=data->find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
	}

	//if not found, DB is corrupted!!!
	if(nRow==-1)
	{
		return false;
	}


	record.copyDefinition(*data);
	record.addRow();
	DbRecordSet row = data->getRow(nRow);
	record.assignRow(0,row);
	return true;
}


//reloads multi records from server, based on id, if succeed, return result in entity format
bool MainEntitySelectionController::GetEntityRecords(DbRecordSet &lstData,int nIDColumnIdx_Inside_lstData,DbRecordSet &lstResults)
{
	int nRow;
	DbRecordSet *data;
	lstResults.copyDefinition(m_lstData);
	int nSize=lstData.getRowCount();
	data=g_ClientCache.GetCache(m_nEntityID); //get pointer to cache

	//-----------------------------------
	//	Get from cache
	//-----------------------------------
	QString strWhere="("; 	//if not found in cache it will be empty row added
	for(int i=0;i<nSize;++i)
	{
		int nEntityRecordID=lstData.getDataRef(i,nIDColumnIdx_Inside_lstData).toInt();
		nRow=-1;
		if(data)
			nRow=data->find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
		lstResults.addRow();
		int nLastAdded=lstResults.getRowCount()-1;
		if (nRow!=-1){
			DbRecordSet row = data->getRow(nRow);
			lstResults.assignRow(nLastAdded,row,true);
		}
		else
			strWhere+=QVariant(nEntityRecordID).toString()+",";
	}

	//-----------------------------------
	//get all unreeded, and go to server:
	//-----------------------------------
	if (strWhere!="(")
	{
		DbRecordSet lstDataFromServer;
		Status err;
		QString strPkID=m_strTablePrefix+"_ID";	//try to get PK column for precise refresh
		strWhere.chop(1);
		strWhere+=")";
		strWhere=strPkID+" IN "+strWhere;
		MainEntityFilter filter;
		filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
		//load from server, send filter data to server...
		_SERVER_CALL(MainEntitySelector->ReadData(err,m_nEntityID,filter.Serialize(),lstDataFromServer))
		_CHK_ERR_RET_BOOL_ON_FAIL(err);

		if (lstDataFromServer.getRowCount()>0) 		//save data to cache:
			g_ClientCache.ModifyCache(m_nEntityID,lstDataFromServer,DataHelper::OP_EDIT,this,true);

		//add to result list:
		int nSize=lstDataFromServer.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			int nID=lstDataFromServer.getDataRef(i,m_nPrimaryKeyColumnIdx).toInt();
			int nRow=lstData.find(nIDColumnIdx_Inside_lstData,nID,true);
			Q_ASSERT(nRow!=-1); //must be inside list
			DbRecordSet row = lstDataFromServer.getRow(i);
			lstResults.assignRow(nRow,row);
		}

	}



	return true;

}


//hardcoded: when inserting new entity, fill in here:
int MainEntitySelectionController::GetDataType(int nEntityID)
{

	switch(nEntityID)
	{
		case ENTITY_BUS_CONTACT:
			{
				return TYPE_CONTACTS;
				break;
			}
		case ENTITY_CONTACT_ORGANIZATION:
		case ENTITY_CONTACT_DEPARTMENT:
		case ENTITY_CONTACT_FUNCTION:
		case ENTITY_CONTACT_PROFESSION:
			{
				return TYPE_TABLE;
				break;
			}
		case ENTITY_BUS_DEPARTMENT:
		case ENTITY_BUS_ORGANIZATION:
		case ENTITY_BUS_COST_CENTER:
		case ENTITY_CE_TYPES:
		case ENTITY_BUS_PROJECT:
			{
				return TYPE_TREE;
				break;
			}
		case ENTITY_BUS_GROUP_ITEMS:
			{
				return TYPE_GROUP_ITEMS;
				break;
			}
		case ENTITY_BUS_PERSON: //MB issue 2417
			{
				return TYPE_USERS;
				break;
			}
		default:
				return TYPE_TABLE;

	}



}



//checks FP: if not available disable SAPNE's
bool MainEntitySelectionController::IsEntityEnabled()
{
	switch(m_nEntityID)
	{
	case ENTITY_BUS_PROJECT:
		if (!g_FunctionPoint.IsFPAvailable(FP_PROJECT_FUI))
		{
			return false;
		}
		break;
	case ENTITY_BUS_CONTACT:
		if (!g_FunctionPoint.IsFPAvailable(FP_CONTACTS_FUI))
		{
			return false;
		}
	    break;
	case ENTITY_BUS_PERSON:
		if (!g_FunctionPoint.IsFPAvailable(FP_USER_FUI))
		{
			return false;
		}
		break;
	}

	return true;

}

bool MainEntitySelectionController::LoadProjectsFromServer()
{
	//try to load user list:
	int nProjectListID=g_pClientManager->GetPersonProjectListID();
	//QApplication::setOverrideCursor(Qt::WaitCursor);
	Status err;
	if (nProjectListID<=0 || m_bForceFullProjectDataReload)
	{
		_SERVER_CALL(MainEntitySelector->ReadData(err,m_nEntityID,m_FilterData.Serialize(),m_lstData));
		if(m_bProjectHideInvisible){
			//delete invisible
			//_DUMP(m_lstData);
			m_lstData.find("BUSP_ACTIVE_FLAG", 0);
			m_lstData.deleteSelectedRows();
		}	
	}
	else
	{
		bool bIsSelection = false;
		QString strSelectionXML;
		bool bIsFilter = false;
		QString strFilterXML;
		bool bGetDescOnly = false;
		_SERVER_CALL(StoredProjectLists->GetListData(err, nProjectListID, m_lstData, bIsSelection, strSelectionXML, bIsFilter, strFilterXML, bGetDescOnly));
	}
	//QApplication::restoreOverrideCursor();
	_CHK_ERR_RET_BOOL_ON_FAIL(err);

	if(m_FilterData.IsEmpty()) //if filter is empty store new data from server inside global cache
	{
		if (m_lstData.getRowCount()>0)
		{
			g_ClientCache.SetCache(m_nEntityID,m_lstData); 
			g_ClientCache.SetCacheReloadCurrentTime(m_nEntityID);
		}
	}

	ApplyLocalFilter();						
	RefreshDisplay();						
	notifyObservers(SelectorSignals::SELECTOR_DATA_CHANGED);	//emit signal that data is changed in this controller
	return true;
}



void MainEntitySelectionController::FilterUserContacts()
{
	if (!m_bShowOnlyUserContacts)
		return;

	if (m_nEntityID==ENTITY_BUS_PERSON)
	{
		m_lstData.find("BPER_CONTACT_ID",0);
		m_lstData.deleteSelectedRows();
	}
	else if (m_nEntityID==ENTITY_BUS_CONTACT)
	{
		if (!m_PersonCache) return; //should never occur!
		DbRecordSet *lstPersons=m_PersonCache->GetDataSource();
		int nSize=m_lstData.getRowCount();
		m_lstData.clearSelection();
		for(int i=0;i<nSize;i++)
		{
			if(lstPersons->find("BPER_CONTACT_ID",m_lstData.getDataRef(i,"BCNT_ID").toInt(),true)<0)
				m_lstData.selectRow(i);
		}
		m_lstData.deleteSelectedRows();
	}
}
void MainEntitySelectionController::ShowOnlyUserContacts()
{
	if (m_nEntityID==ENTITY_BUS_CONTACT)
	{
		m_PersonCache = new MainEntitySelectionController();
		m_PersonCache->Initialize(ENTITY_BUS_PERSON);
		m_PersonCache->ReloadData();
	}
	m_bShowOnlyUserContacts=true;

	FilterUserContacts();
	RefreshDisplay();
}

//-1 if not, else person id
int MainEntitySelectionController::GetPersonIDFromContactID(int nContactID)
{
	if (!m_PersonCache)
	{
		m_PersonCache = new MainEntitySelectionController();
		m_PersonCache->Initialize(ENTITY_BUS_PERSON);
		m_PersonCache->ReloadData();
	}

	DbRecordSet *lstPersons=m_PersonCache->GetDataSource();
	int nRow=lstPersons->find("BPER_CONTACT_ID",nContactID,true);
	if (nRow>=0)
		return lstPersons->getDataRef(nRow,"BPER_ID").toInt();
	else
		return -1;
}

//-1 if not, else cont id
int MainEntitySelectionController::GetContactIDFromPersonID(int nPersonID)
{
	if (!m_PersonCache)
	{
		m_PersonCache = new MainEntitySelectionController();
		m_PersonCache->Initialize(ENTITY_BUS_PERSON);
		m_PersonCache->ReloadData();
	}

	DbRecordSet *lstPersons=m_PersonCache->GetDataSource();
	int nRow=lstPersons->find("BPER_ID",nPersonID,true);
	if (nRow>=0)
		return lstPersons->getDataRef(nRow,"BPER_CONTACT_ID").toInt();
	else
		return -1;
}

