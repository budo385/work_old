#include "dlg_contactbuildall.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "common/common/datahelper.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_client/bus_client/selectorsignals.h"
#include "gui_core/gui_core/thememanager.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_client/bus_client/clientcontactmanager.h"

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;

Dlg_ContactBuildAll::Dlg_ContactBuildAll(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	setWindowTitle(tr("Build Actual Contact List"));//set dialog title

	//Set last tab.
	int nTabID = g_pSettings->GetPersonSetting(CONTACT_SEARCH_DIALOG_TAB).toInt();
	ui.tabWidget->setCurrentIndex(nTabID);

	//set SAPNE's: (filter & selection invisibile)
	ui.rb4->setVisible(false);
	ui.rb5->setVisible(false);
	ui.frameSelection->setVisible(false);
	ui.frameFilter->setVisible(false);

	ui.rb1->setChecked(true);
	ui.rbOp1->setChecked(true);

	//set Group SAPNE:
	ui.frameGroup->Initialize(ENTITY_BUS_GROUP_ITEMS);
	ui.frameGroup->GetButton(Selection_SAPNE::BUTTON_REMOVE)->setVisible(false);
	ui.frameGroup->GetButton(Selection_SAPNE::BUTTON_ADD)->setVisible(false);
	ui.frameGroup->GetButton(Selection_SAPNE::BUTTON_MODIFY)->setVisible(false);

	connect(ui.frameSearch2,SIGNAL(contentChanged()),this,SLOT(OnGroup2Selected()));
	
	ui.frameGroup->registerObserver(this);

	ui.ckbDoNotUseExGroup->setChecked(false);

	//fixable size:
	//this->setFixedHeight(440);
	//this->setFixedWidth(this->width());

	setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

Dlg_ContactBuildAll::~Dlg_ContactBuildAll()
{
	int nTabID = ui.tabWidget->currentIndex();
	g_pSettings->SetPersonSetting(CONTACT_SEARCH_DIALOG_TAB, nTabID);
}

void Dlg_ContactBuildAll::GetResult(DbRecordSet &lstFilterData, int &nOperation,bool &bExcludeGroup)
{
	DbView viewContact=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT);
	MainEntityFilter Filter;
	QString strSQLWhereJoin;
	QString strSQLWhere;

	if (!ui.tabWidget->currentIndex())
	{
		QString strColName1;
		QString strCol1Search1;
		QString strCol1Search2;
		QString strColName2;
		QString strCol2Search1;
		QString strCol2Search2;
		int nDataTypeCustomFld1;
		int nDataTypeCustomFld2;

		ui.frameSearch1->GetSearchData(strColName1, nDataTypeCustomFld1,strCol1Search1, strCol1Search2, strColName2, nDataTypeCustomFld2,strCol2Search1, strCol2Search2);

		ContactTypeManager::AssembleSQLQueryForSearch(strSQLWhereJoin, strSQLWhere, strColName1, nDataTypeCustomFld1, strCol1Search1, strCol1Search2, strColName2, nDataTypeCustomFld2, strCol2Search1, strCol2Search2, true);

		if (!strSQLWhere.isEmpty())
			Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strSQLWhereJoin+" WHERE "+strSQLWhere);

		lstFilterData=Filter.Serialize();

		if (ui.rbOp1_2->isChecked())
		{
			nOperation=	ClientContactManager::OP_REPLACE;
		}
		else
		{
			nOperation=	ClientContactManager::OP_INTERSECT;
		}
		
		bExcludeGroup=false;
	}
	else
	{

		QString strColName;

		//----------------------------------------------------
		//FILTER:
		//----------------------------------------------------
		if (ui.rb2->isChecked())
		{
			QString strColName1;
			QString strCol1Search1;
			QString strCol1Search2;
			QString strColName2;
			QString strCol2Search1;
			QString strCol2Search2;
			int nDataTypeCustomFld1;
			int nDataTypeCustomFld2;

			ui.frameSearch2->GetSearchData(strColName1, nDataTypeCustomFld1, strCol1Search1, strCol1Search2, strColName2, nDataTypeCustomFld2, strCol2Search1, strCol2Search2);

			ContactTypeManager::AssembleSQLQueryForSearch(strSQLWhereJoin, strSQLWhere, strColName1, nDataTypeCustomFld1, strCol1Search1, strCol1Search2, strColName2, nDataTypeCustomFld2, strCol2Search1, strCol2Search2, true);

			if (!strSQLWhere.isEmpty())
				Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strSQLWhereJoin+" WHERE "+strSQLWhere);
		}
		//If group selector
		else if (ui.rb3->isChecked())
		{
			int nGroupID;
			if (ui.frameGroup->GetCurrentEntityRecord(nGroupID))
				Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID="+QVariant(nGroupID).toString());
		}

		lstFilterData=Filter.Serialize();

		//----------------------------------------------------
		//OPERATION:
		//----------------------------------------------------

		if (ui.rbOp1->isChecked())
		{
			nOperation=	ClientContactManager::OP_ADD;
		}
		if (ui.rbOp2->isChecked())
		{
			nOperation=	ClientContactManager::OP_REMOVE;
		}
		if (ui.rbOp3->isChecked())
		{
			nOperation=	ClientContactManager::OP_REPLACE;
		}
		if (ui.rbOp4->isChecked())
		{
			nOperation=	ClientContactManager::OP_INTERSECT;
		}

		bExcludeGroup=ui.ckbDoNotUseExGroup->isChecked();
	}
}






//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void Dlg_ContactBuildAll::on_btnOK_clicked()
{
	//build filter:




	done(1);
}


void Dlg_ContactBuildAll::on_btnCancel_clicked()
{
	done(0);
}


void Dlg_ContactBuildAll::OnGroup2Selected()
{
	if (!ui.rb2->isChecked())
	{
		ui.rb2->setChecked(true);
	}
}

void Dlg_ContactBuildAll::OnGroup3Selected()
{
	ui.rb3->setChecked(true);
}






void Dlg_ContactBuildAll::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==ui.frameGroup && nMsgCode==SelectorSignals::SELECTOR_SELECTION_CHANGED)
		OnGroup3Selected();
}