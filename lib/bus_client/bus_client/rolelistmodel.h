#ifndef ROLELISTMODEL_H
#define ROLELISTMODEL_H

#include <QAbstractListModel>
#include <Qt>
#include <QtGlobal>
#include <QIcon>
#include <QColor>
#include <QFont>
#include <QPainter>
#include <QImageReader>
#include <QPersistentModelIndex>

#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/dbrecordsetlistmodel.h"
#include "gui_core/gui_core/dbrecordsetitem.h"



/*!
	\class  RoleListModel
	\ingroup GUICore_ModelClasses
	\brief  Role definition access right sets list model. 

	Model class for access right sets definition, subclasses DbRecordSetModel.
*/
class RoleListModel : public DbRecordSetListModel
{
public:
    RoleListModel(QObject *Parent = NULL);
    ~RoleListModel();

	Qt::ItemFlags flags(const QModelIndex &index) const;
	Qt::DropActions supportedDropActions() const;
	QStringList mimeTypes() const;

};

#endif // ROLELISTMODEL_H

