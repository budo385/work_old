#ifndef TABLE_SELECTIONBASED_H
#define TABLE_SELECTIONBASED_H

#include <QObject>
#include "gui_core/gui_core/universaltablewidget.h"


/*!
	\class  Table_SelectionTableBased
	\brief  TableClass for BusPerson entity selection controller
	\ingroup GUICore

	Use:
	- initialize it with Initialize(), provide datasource
	- emits ReloadData() signal to reload entity data
	- if programatically reload, then RefreshDisplay()
*/
class Table_SelectionTableBased : public UniversalTableWidget
{
	Q_OBJECT

public:
    Table_SelectionTableBased(QWidget * parent);

	void Initialize(int nEntityID,DbRecordSet * pData,DbRecordSet *lstColumnSetup,bool bEnableDrag,bool bEnableMultiSelection);
	int GetDropType() const {return m_nEntityID;};
	void CreateContextMenuActions(QList<QAction*>& lstActions);

private slots:
		void SlotReload(){emit EmitReloadData();};//re-emits signals:
signals:
		void EmitReloadData();


private:
	int m_nEntityID;
};

#endif // TABLE_SELECTIONBASED_H
