#ifndef VERSIONPARSER_H
#define VERSIONPARSER_H

#include <QString>
#include <QDate>

class VersionParser 
{
public:
	VersionParser(QString strVersionURL);

	void 		SetAbsoluteUpdateFileURL(QString strNewUpdateFileURL){m_strAbsoluteUpdateFileURL=strNewUpdateFileURL;};
	QString		GetAbsoluteUpdateFileURL(){return m_strAbsoluteUpdateFileURL;};
	QString		GetSetupExePath();
	QString		GetVersionID_String();
	double		GetVersionID_Double();
	void		GetAllVersionData(QString &versionid, QString &path,double &version);
	QString		GetSetupExePathBasedOnDate(QDate datValidPerion);
	QString		GetSetupExePathBasedOnVersion(QString strVersion);
	double		Version2Double(QString strVer);

private:
	bool LoadContent();
	QByteArray	m_byteVersionData;
	bool		m_bContentLoaded;
	QString		m_strVersionURL;
	QString		m_strAbsoluteUpdateFileURL;
	
};

#endif // VERSIONPARSER_H
