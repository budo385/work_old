#include "bus_client/bus_client/selection_gridviews.h"
#include "bus_client/bus_client/clientcontactmanager.h"

void Selection_GridViews::RefreshDisplay(int nAction, int nRowInsideDataSource)
{
	ClientContactManager::TranslateViewList(m_lstData);
	Selection_ComboBox::RefreshDisplay(nAction,nRowInsideDataSource);
}