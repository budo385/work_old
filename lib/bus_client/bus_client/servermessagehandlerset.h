#ifndef SERVERMESSAGEHANDLERSET_H
#define SERVERMESSAGEHANDLERSET_H


#include <QtCore>
#include "common/common/status.h"




//-----------------------------------------------
// TEST objects: make your own objects, it can be instanced here
//-----------------------------------------------


/*!
    \class ServerMessages
    \brief Cotnains client side handlers for server messages
    \ingroup Bus_Client

	Proxy_BusinessServiceSet is responsible to connect skeleton and this object.
	Communication is done by SIGNAL/SLOT.
*/

class ServerMessageHandlerSet: public QObject
{
	Q_OBJECT 

public slots:
	void Test_ServerMsg(QString strMessage)
	{qDebug()<<"Serving Server message:\r\n"<<strMessage;};


};



#endif //SERVERMESSAGEHANDLERSET_H
