#include "email_attachmenttable.h"
#include "emailhelper.h"
#include "common/common/zipcompression.h"
#include <QFileInfo>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QtWidgets/QMessageBox>
#include <QDropEvent>
#include <QUrl>
#include <QDrag>
#include <QMimeData>
#include "common/common/datahelper.h"
#include <QCoreApplication>
#include "db_core/db_core/dbsqltableview.h"
#include "documenthelper.h"
#include "gui_core/gui_core/thememanager.h"
#include "os_specific/os_specific/mime_types.h"
#include "trans/trans/xmlutil.h"
#include "common/common/entity_id_collection.h"

extern QString g_strLastDir_FileOpen;

Email_AttachmentTable::Email_AttachmentTable(QWidget *parent)
	: QTableWidget(parent)
{
	m_bEdit=true;
	horizontalHeader()->hide();
	verticalHeader()->hide();
	verticalHeader()->setDefaultSectionSize(38);
	horizontalHeader()->setStretchLastSection(true);
	setColumnCount(1);
	setColumnWidth(0,150);
	m_lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
	m_lstAttachments.addColumn(QVariant::Int,"IS_ZIP");
	m_lstAttachmentsCID.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));



	setStyleSheet(ThemeManager::GetTableViewBkg()+" QToolButton {background:transparent;}" + " QCheckBox {color:white;}");
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setSelectionMode(QAbstractItemView::SingleSelection);
	setEditTriggers(QAbstractItemView::NoEditTriggers); //no edit by double click possible

/*
	QAction* pAction;
	QAction* SeparatorAct;
	QList<QAction*> lstActions;

	pAction = new QAction(tr("Attach File"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnAddAttachment()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Remove Selected Attachments"), this);
	connect(pAction, SIGNAL(triggered()), ui.listAttachemnts, SLOT(DeleteSelection()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Save Attachment As"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnSaveAttachemnt()));
	lstActions.append(pAction);

	ui.listAttachemnts->SetContextMenuActions(lstActions);
	*/

	this->setAcceptDrops(true);
	setDragEnabled(true);
	//setDragDropMode(QAbstractItemView::DragDrop);

	m_nDragMimeType=ENTITY_EMAIL_ATTACHMENT;
	m_strDragIcon=":Email_Attach_32.png";
}

Email_AttachmentTable::~Email_AttachmentTable()
{

}

void Email_AttachmentTable::SetEditMode(bool bEdit)
{
	int nSize=rowCount();
	for(int i=0;i<nSize;i++)
	{
		WidgetRow *pWidget=dynamic_cast<WidgetRow *>(cellWidget(i,0));
		if (pWidget)
		{
			pWidget->btnSelect->setEnabled(bEdit);
			pWidget->btnDelete->setEnabled(bEdit);
			pWidget->m_btnZIP->setEnabled(bEdit);
			pWidget->m_txtName->setEnabled(bEdit);
		}
	}
}
void Email_AttachmentTable::SetData (DbRecordSet lstAttachments)
{
	//remove all CID attach: store 'em in special recordset
	int nSize=lstAttachments.getRowCount();
	lstAttachments.clearSelection();
	for(int i=0;i<nSize;++i)
	{
		if (!lstAttachments.getDataRef(i,"BEA_CID_LINK").toString().isEmpty())
			lstAttachments.selectRow(i);
	}
	m_lstAttachmentsCID.clear();
	m_lstAttachmentsCID.merge(lstAttachments,true);
	//remove all CID attach: store 'em in special recordset

	lstAttachments.deleteSelectedRows();
	m_lstAttachments=lstAttachments;

	if (m_lstAttachments.getColumnIdx("IS_ZIP")<0)
		m_lstAttachments.addColumn(QVariant::Int,"IS_ZIP");

	m_lstAttachments.setColValue("IS_ZIP",0);
	RefreshDisplay();
}
bool Email_AttachmentTable::GetData (DbRecordSet &lstAttachments, bool bZipAttachments)
{
	if (bZipAttachments)
	{
		if (!ZipAttachmentsBeforeSend())
			return false;
	}
	lstAttachments=m_lstAttachments;
	lstAttachments.merge(m_lstAttachmentsCID);
	
	return true;
}


void Email_AttachmentTable::ClearAll()
{
	m_lstAttachments.clear();
	m_lstAttachmentsCID.clear();
	RefreshDisplay();
}

void Email_AttachmentTable::RefreshDisplay()
{
	clear();
	int nSize=m_lstAttachments.getRowCount();

	//m_lstAttachments.Dump();

	setRowCount(nSize+1); //last is empty
	for(int i=0;i<=nSize;i++)
	{

		WidgetRow *pWidget=dynamic_cast<WidgetRow *>(cellWidget(i,0));
		if (!pWidget)
		{
			pWidget = new WidgetRow(this,i);
			setCellWidget(i,0,pWidget);
			connect(pWidget,SIGNAL(ZIPChanged(bool,int)),this,SLOT(OnZIPChanged(bool,int)));
		}
		if (i!=nSize)
		{
			pWidget->SetData(m_lstAttachments.getDataRef(i,"BEA_NAME").toString(),m_lstAttachments.getDataRef(i,"IS_ZIP").toInt());	
			pWidget->setToolTip(m_lstAttachments.getDataRef(i,"BEA_NAME").toString());
		}
		else
		{
			pWidget->SetData("",0);
			pWidget->setToolTip("");
		}
	}

	clearSelection();
}

void Email_AttachmentTable::OnAddAttachment(int nRow)
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.*";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Choose file"),
		strStartDir,
		strFilter);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}

	if (nRow==m_lstAttachments.getRowCount()) //init append new row
		nRow=-1;
	AddAttachmentFromFile(strFile,nRow);
	RefreshDisplay();

	
	
}
void Email_AttachmentTable::OnSaveAttachemnt(int nRow)
{
	if (nRow==m_lstAttachments.getRowCount())
	{
		//m_lstAttachments.addRow();
		return;
	}

	QString strPath=m_lstAttachments.getDataRef(nRow,"BEA_NAME").toString();
	QString strStartDir=QDir::currentPath()+"/"+strPath;
	QString strFilter="*.*";
	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	strPath = QFileDialog::getSaveFileName(
		NULL,
		tr("Save As"),
		strStartDir,
		strFilter);
	if (!strPath.isEmpty())
	{
		QFileInfo fileInfo(strPath);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		DocumentHelper::SaveFileContent(m_lstAttachments.getDataRef(nRow,"BEA_CONTENT").toByteArray(),strPath,true);
	}
}
void Email_AttachmentTable::OnDeleteAttachment(int nRow)
{
	if (nRow==m_lstAttachments.getRowCount())
	{
		return;
	}

	m_lstAttachments.deleteRow(nRow);
	RefreshDisplay();

}
void Email_AttachmentTable::OnOpenAttachment(int nRow)
{
	if (nRow==m_lstAttachments.getRowCount())
	{
		return;
	}
	QByteArray ar = m_lstAttachments.getDataRef(nRow,"BEA_CONTENT").toByteArray();
	DocumentHelper::OpenDocumentFromAttachment(ar, m_lstAttachments.getDataRef(nRow,"BEA_NAME").toString());
}



void Email_AttachmentTable::OnDropSokratesDocument(DbRecordSet lstData)
{
	//zip docs:
	QStringList lstZippedFiles;
	if(!DocumentHelper::PackDocuments(lstData,lstZippedFiles,true))
		return;
	
	//add packed files:
	int nSize=lstZippedFiles.size();
	for(int i=0;i<nSize;++i)
	{
		AddAttachmentFromFile(lstZippedFiles.at(i));
	}
	RefreshDisplay();
}


bool Email_AttachmentTable::AddAttachmentFromFile(QString strFile, int nRow)
{
	if(!strFile.isEmpty())
	{
		QFileInfo infoTarget(strFile);
		QString strBaseName=infoTarget.fileName();

		int k=m_lstAttachments.find(m_lstAttachments.getColumnIdx("BEA_NAME"),strBaseName,true);
		if (k!=-1) //alredy exists: abort abort
		{
			QMessageBox::warning(this,tr("Warning"),tr("Attachment with this name already exists!"));
			return false;
		}

		QFile file(strFile);
		if(!file.open(QIODevice::ReadOnly))
		{
			QMessageBox::warning(this,tr("Warning"),tr("Attachment can not be loaded!"));
			return false;
		}
		QByteArray content=file.readAll();
		file.close();

		if (nRow==-1)
		{
			m_lstAttachments.addRow(); //after set on empty, add empty
			nRow=m_lstAttachments.getRowCount()-1;
		}
		m_lstAttachments.setData(nRow,"BEA_NAME",strBaseName);
		QByteArray binary=qCompress(content,1); //min compression
		m_lstAttachments.setData(nRow,"BEA_CONTENT",binary);
		int nZIP=1;
		//B.T. 29.04.2013: exclude pdf files from zipping attachments: issue 2703
		if (infoTarget.completeSuffix().toLower()=="zip" || infoTarget.completeSuffix().toLower()=="pdf")
			nZIP=0;
		m_lstAttachments.setData(nRow,"IS_ZIP",nZIP);

		return true;

	}

	return false;
}




bool Email_AttachmentTable::ZipAttachmentsBeforeSend()
{
	//skip if already have *zip extension, store back inside m_ls
	if (m_lstAttachments.getRowCount()==0)
		return true;
	//QString strTempDirectory=QCoreApplication::applicationDirPath();
	QString strTempDirectory=QDir::tempPath();
	if (strTempDirectory.isEmpty())
	{
		QMessageBox::critical(NULL,tr("Error"),tr("Failed to compress attachments!"));
		return false;
	}
	ZipCompression Zipper;
	int nSize=m_lstAttachments.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (!IsZip(i))
			continue;

		QString strFilePath=strTempDirectory+"/"+m_lstAttachments.getDataRef(i,"BEA_NAME").toString();
		QFileInfo info(strFilePath);
		DocumentHelper::SaveFileContent(m_lstAttachments.getDataRef(i,"BEA_CONTENT").toByteArray(),strFilePath,true);
		//zip it:
		QString strZipFile=QDir::toNativeSeparators(info.absolutePath()+"/"+info.baseName()+".zip");
		QStringList strLstSource;
		strLstSource<<strFilePath;
		if(!ZipCompression::ZipFiles(strZipFile,strLstSource))
		{
			QString strTitle=tr("Failed to zip document ")+strFilePath;
			QMessageBox::warning(NULL,tr("Warning"),strTitle);
			return false;
		}

		QFile file(strZipFile);
		if(!file.open(QIODevice::ReadOnly))
		{
			QMessageBox::warning(this,tr("Warning"),tr("Attachment can not be saved!"));
			return false;
		}
		QByteArray content=file.readAll();
		QByteArray binary=qCompress(content,1); //min compression
		m_lstAttachments.setData(i,"BEA_NAME",info.baseName()+".zip");
		m_lstAttachments.setData(i,"BEA_CONTENT",binary);
		file.remove();
		file.close();
	}
	return true;
}

bool Email_AttachmentTable::IsZip(int nRow)
{
	bool bZip=false;
	WidgetRow *pWidget=dynamic_cast<WidgetRow *>(cellWidget(nRow,0));
	if (pWidget)
	{
		QString strDoc;
		pWidget->GetData(bZip,strDoc);
		return bZip;
	}
	return bZip;
}

void Email_AttachmentTable::AppendFiles(QStringList *lstFiles)
{
	if (lstFiles)
	{
		//add packed files:
		int nSize=lstFiles->size();
		for(int i=0;i<nSize;++i)
		{
			AddAttachmentFromFile(lstFiles->at(i));
		}
	}
	RefreshDisplay();
}

bool Email_AttachmentTable::IsCIDAttachmentExists()
{
	return (m_lstAttachmentsCID.getRowCount()>0);
}

void Email_AttachmentTable::AddNewAttachment()
{
	m_lstAttachments.addRow();
	int nRow=m_lstAttachments.getRowCount()-1; //set on empty;
	RefreshDisplay();
	OnAddAttachment(nRow);
	; //add empty
}


//if not edit mode: disable drop & drag
void Email_AttachmentTable::dropEvent(QDropEvent *event)
{
	//QListWidget::dropEvent(event);

		const QMimeData *mime = event->mimeData();

		//------------------------------------------------
		// LIST of files:
		//------------------------------------------------
		if (mime->hasUrls()) 
		{
			QList<QUrl> lst =mime->urls();

			//list of paths (files), list of application paths (exe)
			QStringList lstFiles;
			for (int i=0;i<lst.size();++i)
			{
				if (lst.at(i).scheme().toUpper()==QString("file").toUpper())
				{	
					QString strExt,strName;
					QString strFilePath=DataHelper::ResolveFilePath(lst.at(i).toLocalFile(),strName,strExt);
					lstFiles.append(strFilePath);
				}
			}
			if (lstFiles.size()>0)
			{
				AppendFiles(&lstFiles);
			}

			event->accept();
			return;
		}

		//------------------------------------------------
		// LIST of documents entities:
		//------------------------------------------------

		//internal/external
		if (mime->hasFormat(SOKRATES_MIME_LIST))
		{
			int nEntityID;
			DbRecordSet lstDropped;

			//import emails:
			QByteArray arData = mime->data(SOKRATES_MIME_LIST);
			if(arData.size() > 0)
			{
				lstDropped=XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
				//lstDropped.Dump();
			}
			QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
			if(arDataType.size() > 0)
			{
				bool bOK;
				nEntityID=arDataType.toInt(&bOK);
				if (!bOK) nEntityID=-1;
			}

			if (nEntityID==ENTITY_COMM_GRID_DATA && lstDropped.getRowCount()>0)
			{
				DbRecordSet lstDocs;
				int nRow=lstDropped.find(0,QVariant(ENTITY_BUS_DM_DOCUMENTS).toInt(),true);
				if (nRow!=-1)
				{
					lstDocs=lstDropped.getDataRef(nRow,1).value<DbRecordSet>();
					if (lstDocs.getRowCount()>0)
					{
						OnDropSokratesDocument(lstDocs);
					}
				}

				event->accept();
				return;
			}
		}

	event->ignore();
	return;
}



Qt::DropActions Email_AttachmentTable::supportedDropActions() const 
{
	return  Qt::CopyAction | Qt::MoveAction | Qt::IgnoreAction | Qt::LinkAction | Qt::ActionMask | Qt::TargetMoveAction;
}


void Email_AttachmentTable::dragMoveEvent ( QDragMoveEvent * event )
{
	event->accept();
	return;
}

//if not edit mode: disable drop & drag
void Email_AttachmentTable::dragEnterEvent ( QDragEnterEvent * event )
{
	event->accept();
	return;
}


/*!
Starts a drag by calling drag->start() using the given \a supportedActions.
*/
void Email_AttachmentTable::startDrag(Qt::DropActions supportedActions)
{
	//QTableWidget::startDrag(supportedActions);
	//return;

	//Q_D(QAbstractItemView);
	QModelIndexList indexes = selectedIndexes();
	if (indexes.count() > 0) 
	{
		QMimeData *data = model()->mimeData(indexes);
		if (!data)
			return;
		QRect rect;
		QPixmap pixmap(m_strDragIcon); //= d->renderToPixmap(indexes, &rect);
		QDrag *drag = new QDrag(this);
		drag->setPixmap(pixmap);
		drag->setMimeData(data);
		drag->setHotSpot(QPoint(-10,0)); //drag->pixmap().width()/2,0drag->pixmap().height()));

		if (drag->start(supportedActions) == Qt::MoveAction)
		{
			//------------------B.T. copied form void QAbstractItemViewPrivate::clearOrRemove()
			// we can't remove the rows so reset the items (i.e. the view is like a table)
			QModelIndexList list = selectedIndexes();
			for (int i=0; i < list.size(); ++i) 
			{
				QModelIndex index = list.at(i);
				QMap<int, QVariant> roles = model()->itemData(index);
				for (QMap<int, QVariant>::Iterator it = roles.begin(); it != roles.end(); ++it)
					it.value() = QVariant();
				model()->setItemData(index, roles);
			}
			//------------------B.T. copied form void QAbstractItemViewPrivate::clearOrRemove()
		}

	}
}


//when drag: send selected items + type
QMimeData * Email_AttachmentTable::mimeData( const QList<QTableWidgetItem *> items ) const
{
	QMimeData *mimeData=QTableWidget::mimeData(items);
	if (mimeData)
	{
		//m_bSelectionChangeEmitted=true;
		QList<QTableWidgetSelectionRange> lstSelections = selectedRanges();
		int nSize=lstSelections.size();
		if(nSize>0)
		{
			DbRecordSet lstSelectedItems;
			lstSelectedItems.copyDefinition(m_lstAttachments);

			for(int i=0;i<nSize;++i)
			{
				QTableWidgetSelectionRange range=lstSelections.at(i);
				for(int j=range.topRow();j<=range.bottomRow();++j)
				{	
					if (j<m_lstAttachments.getRowCount())
						lstSelectedItems.merge(m_lstAttachments.getRow(j)); //get selected rows..
				}
			}

			if (lstSelectedItems.getRowCount()>0 && m_nDragMimeType!=-1)
			{
				QByteArray byteListData=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstSelectedItems);
				mimeData->setData(SOKRATES_MIME_LIST,byteListData);
				mimeData->setData(SOKRATES_MIME_DROP_TYPE,QVariant(m_nDragMimeType).toString().toLatin1()); //pass unique type
			}
		}
	}

	return mimeData;
}

//add custom mime types: selected list items + drop type
QStringList Email_AttachmentTable::mimeTypes() const
{
	QStringList lstDefault=QTableWidget::mimeTypes();
	lstDefault<<SOKRATES_MIME_LIST;
	lstDefault<<SOKRATES_MIME_DROP_TYPE;

	return lstDefault;
}

void Email_AttachmentTable::OnZIPChanged(bool bIschecked,int nRow)
{
	if (nRow<m_lstAttachments.getRowCount() && nRow>=0)
	{
		m_lstAttachments.setData(nRow,"IS_ZIP",(int)bIschecked);
	}
}



WidgetRow::WidgetRow(Email_AttachmentTable *parent,int nRow)
{
	m_Parent=parent;
	m_nRow=nRow;

	//: select, open, delete
	//ckb zip
	QSize buttonSize(18,18);
	btnSelect = new QToolButton;
	btnDelete = new QToolButton;
	btnOpen = new QToolButton;
	btnSave = new QToolButton;
	btnSelect->setIcon(QIcon(":Select16.png"));
	btnDelete->setIcon(QIcon(":SAP_Clear.png"));
	btnOpen->setIcon(QIcon(":SAP_Select.png"));
	btnSave->setIcon(QIcon(":filesave.png"));


	btnSelect->setToolTip(tr("Add Attachment"));
	btnDelete->setToolTip(tr("Remove Attachment"));
	btnOpen->setToolTip(tr("Open Attachment"));
	btnSave->setToolTip(tr("Save Attachment"));
	btnSelect->setAutoRaise(true);
	btnOpen->setAutoRaise(true);
	btnDelete->setAutoRaise(true);
	btnSave->setAutoRaise(true);

	btnSelect->setMaximumSize(buttonSize);
	btnSelect->setMinimumSize(buttonSize);
	btnDelete->setMaximumSize(buttonSize);
	btnDelete->setMinimumSize(buttonSize);
	btnOpen->setMaximumSize(buttonSize);
	btnOpen->setMinimumSize(buttonSize);
	btnSave->setMaximumSize(buttonSize);
	btnSave->setMinimumSize(buttonSize);

	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->addWidget(btnSelect);
	hbox->addWidget(btnDelete);
	hbox->addWidget(btnOpen);
	hbox->addWidget(btnSave);
	hbox->setSpacing(1);
	hbox->setMargin(0);

	m_btnZIP = new QCheckBox(tr("ZIP"));
	m_btnZIP->setMaximumHeight(18);
	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addLayout(hbox);
	vbox->addWidget(m_btnZIP);
	vbox->setSpacing(0);
	vbox->setContentsMargins(2,0,0,0);

	connect(m_btnZIP,SIGNAL(stateChanged(int)),this, SLOT(OnZipStateChanged(int)));

	m_txtName = new QTextEdit;
	m_txtName->setMaximumHeight(36);
	m_txtName->setReadOnly(true);
	m_txtName->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
	//m_txtName->setLineWrapMode()

	QHBoxLayout *hboxTotal = new QHBoxLayout;
	hboxTotal->addLayout(vbox);
	hboxTotal->addWidget(m_txtName);
	hboxTotal->setSpacing(0);
	hboxTotal->setMargin(0);
	this->setLayout(hboxTotal);

	connect(btnSelect,SIGNAL(clicked()),this,SLOT(OnSelect()));
	connect(btnDelete,SIGNAL(clicked()),this,SLOT(OnRemove()));
	connect(btnOpen,SIGNAL(clicked()),this,SLOT(OnOpen()));
	connect(btnSave,SIGNAL(clicked()),this,SLOT(OnSave()));

	//setDragEnabled(true);
}

void WidgetRow::OnSelect()
{
	//add attachment
	m_Parent->OnAddAttachment(m_nRow);
}
void WidgetRow::OnRemove()
{
	//remove
	m_Parent->OnDeleteAttachment(m_nRow);
}
void WidgetRow::OnOpen()
{
	//open
	m_Parent->OnOpenAttachment(m_nRow);
}

void WidgetRow::OnSave()
{
	//open
	m_Parent->OnSaveAttachemnt(m_nRow);
}

WidgetRow::~WidgetRow()
{


}


void WidgetRow::SetData(QString strDocument,bool bZip)
{
	m_txtName->setPlainText(strDocument);
	m_btnZIP->setChecked(bZip);
	

}
void WidgetRow::GetData(bool &bZip,QString &strDocument)
{
	bZip=(m_btnZIP->checkState()==Qt::Checked);
	strDocument=m_txtName->toPlainText();
}

void WidgetRow::OnZipStateChanged(int nState)
{
	emit ZIPChanged(nState==Qt::Checked,m_nRow);
}



