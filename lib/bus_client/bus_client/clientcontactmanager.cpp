#include "bus_client/bus_client/clientcontactmanager.h"
#include "gui_core/gui_core/universaltablewidget.h"
#include "common/common/datahelper.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include <QApplication>
#include "trans/trans/httpreadersync.h"
#include "bus_core/bus_core/countries.h"
#include <QDesktopWidget>
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/formataddress.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/picturehelper.h"
#include "trans/trans/xmlutil.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "dlg_copyentity.h"
#include "bus_client/bus_client/phoneselectordlg.h"
#include "bus_core/bus_core/formatphone.h"

//global cache:
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

ClientContactManager::ClientContactManager()
{

}

ClientContactManager::~ClientContactManager()
{

}




//gets default type from cache, if not in cache or no def type, return NULL variant
QVariant ClientContactManager::GetDefaultType(int nEntityTypeID)
{
	QVariant varType(QVariant::Int); //NULL TYPE
	DbRecordSet *data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
	if(data==NULL)
	{
		MainEntitySelectionController type;
		type.Initialize(ENTITY_BUS_CM_TYPES);
		type.ReloadData();
		data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
		if(data==NULL)
			return varType;
	}
		

	//select all
	data->find(data->getColumnIdx("BCMT_ENTITY_TYPE"),nEntityTypeID);

	//select defaults:
	int nSelected=data->find(data->getColumnIdx("BCMT_IS_DEFAULT"),1,false,true,true);

	if(nSelected==1)
		return data->getDataRef(data->getSelectedRow(),"BCMT_ID");
	else
		return varType;
}

//based on system type, fetch user defined type:
QVariant ClientContactManager::GetUserTypeBasedOnSystemType(int nEntityTypeID, int nSystemTypeID)
{
	QVariant varType(QVariant::Int); //NULL TYPE
	DbRecordSet *data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
	if(data==NULL)
	{
		MainEntitySelectionController type;
		type.Initialize(ENTITY_BUS_CM_TYPES);
		type.ReloadData();
		data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
		if(data==NULL)
			return varType;
	}

	//select all
	data->find(data->getColumnIdx("BCMT_ENTITY_TYPE"),nEntityTypeID);

	
	//select defaults:
	int nSelected=data->find(data->getColumnIdx("BCMT_SYSTEM_TYPE"),nSystemTypeID,false,true,true);
	if(nSelected==1)
		return data->getDataRef(data->getSelectedRow(),"BCMT_ID");
	else
		return varType;

}

QString ClientContactManager::GetTypeName(int TypeID)
{

	DbRecordSet *data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
	if(data==NULL)
	{
		MainEntitySelectionController type;
		type.Initialize(ENTITY_BUS_CM_TYPES);
		type.ReloadData();
		data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
		if(data==NULL)
			return "";
	}

	//select all
	int nRow=data->find(data->getColumnIdx("BCMT_ID"),TypeID,true);
	if (nRow!=-1)
	{
		return data->getDataRef(nRow,"BCMT_TYPE_NAME").toString();
	}
	else
		return "";

}

//TypeEntityID from clientcontactmanager.h
bool ClientContactManager::GetTypeList(int TypeEntityID,DbRecordSet &lstData)
{

	DbRecordSet *data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
	if(data==NULL)
	{
		MainEntitySelectionController type;
		type.Initialize(ENTITY_BUS_CM_TYPES);
		type.ReloadData();
		data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
		if(data==NULL)
			return false;
	}

	//select all
	data->find(data->getColumnIdx("BCMT_ENTITY_TYPE"),TypeEntityID);

	DbRecordSet lstTemp;
	lstData.copyDefinition(*data);
	lstData.merge(*data,true);
	return true;
}





/*!
	Format address inside address list with A) default address schema from user settings, B) form current def. schema C) from schema found inside address row in CMAD_FORMATSCHEMA_ID
	Returns results in list of address:
	NOTE: CMAD_FORMATSCHEMA_ID is set to def. schema if formated from def. user settings
	NOTE: current schema is preserved after this operation

	\param lstAddresses				- address list
	\param nContactType				- ContactTypeManager::CM_TYPE_PERSON or ContactTypeManager::CM_TYPE_ORGANIZATION
	\param nOperation				- 1-use def schema from user settings (if empty use current def. schema), 2 - use CMAD_FORMATSCHEMA_ID for format, 3 - use current def. schema, 
										Note:  if 2. fails, then it will use defaultschema
	\param bSkipFormattingIfExists	- 1 - if CMAD_FORMATEDADDRESS is filled then skip

*/
bool ClientContactManager::FormatAddressWithDefaultSchema(DbRecordSet &lstAddresses, int nContactType,int nOperation, bool bSkipFormattingIfExists)
{
	
	FormatAddress AdrFormatter;

	switch(nOperation)
	{
	
	case FORMAT_USING_USERDEFAULTS:
		{

			
			//TOFIX load user settings:
			int nSchema=-1;
			if (nContactType==ContactTypeManager::CM_TYPE_PERSON)
			{
				nSchema=g_pSettings->GetPersonSetting(CONTACT_DEF_ADDR_SCHEMA_PERSON).toInt();
				if (nSchema<=0)
					nSchema=g_pSettings->GetApplicationOption(APP_CONTACT_DEF_ADDR_SCHEMA_PERSON).toInt();
			}
			else
			{
				nSchema=g_pSettings->GetPersonSetting(CONTACT_DEF_ADDR_SCHEMA_ORG).toInt();
				if (nSchema<=0)
					nSchema=g_pSettings->GetApplicationOption(APP_CONTACT_DEF_ADDR_SCHEMA_ORG).toInt();

			}


			if (nSchema==-1)
			{
				AdrFormatter.AddressFormat(lstAddresses,AdrFormatter.m_strDefaultSchema,bSkipFormattingIfExists);
			}
			else
			{
				MainEntitySelectionController SchemaHandler;
				SchemaHandler.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
				SchemaHandler.ReloadData();

				QString strSchema;
				DbRecordSet *lstSchemas=SchemaHandler.GetDataSource();
				int nRow=lstSchemas->find(lstSchemas->getColumnIdx("BCMAS_ID"),nSchema,true);
				if(nRow!=-1)
					strSchema=lstSchemas->getDataRef(nRow,"BCMAS_SCHEMA").toString();
				else
					strSchema=AdrFormatter.m_strDefaultSchema;

				AdrFormatter.AddressFormat(lstAddresses,strSchema,bSkipFormattingIfExists);
			}

		}
		break;


	case FORMAT_USING_STORED_FORMATSCHEMA:
		{

			QStringList lstSchemas;
			//if fails, use userdefaults, or current:
			int nSize=lstAddresses.getRowCount();
			int nFormatSchemaIDIdx=lstAddresses.getColumnIdx("BCMA_FORMATSCHEMA_ID");
			for(int i=0;i<nSize;++i)
			{
				QString strSchema;
		
				int nFormatSchemaID=lstAddresses.getDataRef(i,nFormatSchemaIDIdx).toInt();
				if(nFormatSchemaIDIdx!=0)
				{
					MainEntitySelectionController SchemaHandler;
					SchemaHandler.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
					SchemaHandler.ReloadData();

					DbRecordSet *lstSchemas=SchemaHandler.GetDataSource();
					int nRow=lstSchemas->find(lstSchemas->getColumnIdx("BCMAS_ID"),nFormatSchemaID,true);
					if(nRow!=-1)
						strSchema=lstSchemas->getDataRef(nRow,"BCMAS_SCHEMA").toString();
					else
						strSchema=AdrFormatter.m_strDefaultSchema;
				}
				lstSchemas<<strSchema;
			}
		AdrFormatter.AddressFormat(lstAddresses,lstSchemas,bSkipFormattingIfExists);
		}
	    break;

	case FORMAT_USING_HARDCODE_DEFAULTSCHEMA:
		AdrFormatter.AddressFormat(lstAddresses,AdrFormatter.m_strDefaultSchema,bSkipFormattingIfExists);

		break;
	default:

	    break;
	}





	return false;
}




QString ClientContactManager::GetFirstLettersFromString(QString strString)
{
	QString strReturnString;
	QStringList lst = strString.split(" ", QString::SkipEmptyParts);
	foreach(QString str, lst)
		strReturnString += str.left(1);

	return strReturnString;
}

void ClientContactManager::GetFullContactGridSetup(DbRecordSet &lstData)
{
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_OLD_CODE",QObject::tr("External Code"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_DESCRIPTION",QObject::tr("Description"),100,false,"",UniversalTableWidget::COL_TYPE_MULTILINE_TEXT);
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_SHORTNAME",QObject::tr("Ancient Last Name"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_LASTNAME", QObject::tr("Last Name", "FUCK"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_FIRSTNAME",QObject::tr("First Name", "FUCK"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_MIDDLENAME",QObject::tr("Middle Name"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_DEPARTMENTNAME",QObject::tr("Department", "FUCK"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_BIRTHDAY",QObject::tr("Day of birth"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_ORGANIZATIONNAME",QObject::tr("Organization"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_ORGANIZATIONNAME_2",QObject::tr("Organization Short Name"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_FOUNDATIONDATE",QObject::tr("Organization Foundation Date"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_PROFESSION",QObject::tr("Profession"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_FUNCTION",QObject::tr("Function"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_SEX",QObject::tr("Male/Female"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_LOCATION",QObject::tr("Location"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BGCN_CMCA_VALID_FROM",QObject::tr("Valid From"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BGCN_CMCA_VALID_TO",QObject::tr("Valid To"));
	
	UniversalTableWidget::AddColumnToSetup(lstData,"LST_PHONE",QObject::tr("Phone"),100,false,QObject::tr("Phones"),UniversalTableWidget::COL_TYPE_LIST,"BCMT_TYPE_NAME<<' - '<<BCMP_FULLNUMBER");
	UniversalTableWidget::AddColumnToSetup(lstData,"LST_EMAIL",QObject::tr("Emails"),100,false,QObject::tr("Emails"),UniversalTableWidget::COL_TYPE_LIST,"BCMT_TYPE_NAME<<' - '<<BCME_ADDRESS");
	UniversalTableWidget::AddColumnToSetup(lstData,"LST_ADDRESS",QObject::tr("Address"),100,false,QObject::tr("Address"),UniversalTableWidget::COL_TYPE_LIST,"BCMT_TYPE_NAME<<' - '<<BCMA_STREET<<' / '<<BCMA_ZIP<<' '<<BCMA_CITY");
	UniversalTableWidget::AddColumnToSetup(lstData,"LST_INTERNET",QObject::tr("Web Site"),100,false,QObject::tr("Internet"),UniversalTableWidget::COL_TYPE_LIST,"BCMT_TYPE_NAME<<' - '<<BCMI_ADDRESS");

	//qDebug() << "Bug test: " << QObject::tr("Last Name");
}

void ClientContactManager::GetBuildAllContactGridSetup(DbRecordSet &lstData)
{
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_OLD_CODE",QObject::tr("External Code"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_DESCRIPTION",QObject::tr("Description"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_SHORTNAME",QObject::tr("Ancient Last Name"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_LASTNAME",QObject::tr("Last Name","FUCK1"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_FIRSTNAME",QObject::tr("First Name","FUCK1"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_MIDDLENAME",QObject::tr("Middle Name"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_DEPARTMENTNAME",QObject::tr("Department","FUCK1"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_BIRTHDAY",QObject::tr("Day of birth"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_ORGANIZATIONNAME",QObject::tr("Organization"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_ORGANIZATIONNAME_2",QObject::tr("Organization Short Name"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_FOUNDATIONDATE",QObject::tr("Organization Foundation Date"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_PROFESSION",QObject::tr("Profession"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_FUNCTION",QObject::tr("Function"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_SEX",QObject::tr("Male/Female"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCMP_FULLNUMBER",QObject::tr("Any Phone Number"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCMI_ADDRESS",QObject::tr("Any Net Address"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCME_ADDRESS",QObject::tr("Email"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCMA_FORMATEDADDRESS",QObject::tr("Address"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_LOCATION",QObject::tr("Location"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BGCN_CMCA_VALID_FROM",QObject::tr("Valid From"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BGCN_CMCA_VALID_TO",QObject::tr("Valid To"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_DAT_CREATED",QObject::tr("Creation Date"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCMA_ZIP",QObject::tr("ZIP Code"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCMD_DEBTORCODE",QObject::tr("Debtor Code"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCMD_DEBTORACCOUNT",QObject::tr("Debtor Account"));
	UniversalTableWidget::AddColumnToSetup(lstData,"BCMD_CUSTOMERCODE",QObject::tr("Customer Code"));

	
}

void ClientContactManager::GetDefaultContactGridSetup(DbRecordSet &lstData)
{
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_LASTNAME",QObject::tr("Last Name"),100);
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_FIRSTNAME",QObject::tr("First Name"),100);
	UniversalTableWidget::AddColumnToSetup(lstData,"BCNT_ORGANIZATIONNAME",QObject::tr("Organization"),100);
}

//prepare HEADER TEXT to native language
void ClientContactManager::TranslateViewList(DbRecordSet &lstData)
{
	
	DbRecordSet lstTranslated;
	GetFullContactGridSetup(lstTranslated);
	int nIdx=lstTranslated.getColumnIdx("BOGW_COLUMN_NAME");

	//lstTranslated.Dump();
	//lstData.Dump();

	int nSize=lstData.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strCol=lstData.getDataRef(i,"BOGW_COLUMN_NAME").toString();
		//qDebug() << "Original: " << strCol;

		int nRow=lstTranslated.find(nIdx,strCol,true);
		if (nRow!=-1)
		{
			//qDebug() << "Translated: " << lstTranslated.getDataRef(nRow,"BOGW_HEADER_TEXT"); 
			lstData.setData(i,"BOGW_HEADER_TEXT",lstTranslated.getDataRef(nRow,"BOGW_HEADER_TEXT"));
		}
	}
}







//add's BCMT_TYPE_NAME at end of list, if not already exists and set's LST_TYPES inside address
void ClientContactManager::PrepareListsWithTypeName(DbRecordSet *pLstEmail,DbRecordSet *pLstPhone,DbRecordSet *pLstInternet,DbRecordSet *pLstAddress,DbRecordSet *pLstAddressTypes)
{

	//get each sublist:, get types first:
	DbRecordSet *lstAllTypes=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);

	//phones:
	if (pLstPhone!=NULL)
	{
		if (pLstPhone->getColumnIdx("BCMT_TYPE_NAME")==-1)
			pLstPhone->addColumn(QVariant::String,"BCMT_TYPE_NAME");
		if (lstAllTypes)
			ContactTypeManager::AssignTypeName(*lstAllTypes,*pLstPhone,"BCMP_TYPE_ID");
	}

	//address:
	if (pLstAddress!=NULL)
	{
		if (pLstAddress->getColumnIdx("BCMT_TYPE_NAME")==-1)
			pLstAddress->addColumn(QVariant::String,"BCMT_TYPE_NAME");
		if (pLstAddress->getColumnIdx("LST_TYPES")==-1)
			pLstAddress->addColumn(DbRecordSet::GetVariantType(),"LST_TYPES");
			//Filter only types that are inside address
		AssignChildToParent(*pLstAddress,"LST_TYPES",*pLstAddressTypes,"BCMAT_ADDRESS_ID","BCMA_ID");

		if (lstAllTypes)
			ContactTypeManager::AssignTypeName(*lstAllTypes,*pLstAddress,"",true);
	}


	//email:
	if (pLstEmail!=NULL)
	{
		if (pLstEmail->getColumnIdx("BCMT_TYPE_NAME")==-1)
			pLstEmail->addColumn(QVariant::String,"BCMT_TYPE_NAME");
		if (lstAllTypes)
			ContactTypeManager::AssignTypeName(*lstAllTypes,*pLstEmail,"BCME_TYPE_ID");
	}

	//internet:
	if (pLstInternet)
	{
		if (pLstInternet->getColumnIdx("BCMT_TYPE_NAME")==-1)
			pLstInternet->addColumn(QVariant::String,"BCMT_TYPE_NAME");
		if (lstAllTypes)
			ContactTypeManager::AssignTypeName(*lstAllTypes,*pLstInternet,"BCMI_TYPE_ID");
	}


}


//based on FK=ParentID assign all records from child list to the parent sublists:
void ClientContactManager::AssignChildToParent(DbRecordSet &lstParent,QString strChildSubListCol,DbRecordSet &lstChilds,QString strFKCol,QString strParentIDCol)
{

	//determine parent ID in parent list
	int nParentID=0;
	if (!strParentIDCol.isEmpty())
		nParentID=lstParent.getColumnIdx(strParentIDCol);

	//get sublist col:
	int nSubListIdx=lstParent.getColumnIdx(strChildSubListCol);

	//get FK Idx:
	int nFKID=lstChilds.getColumnIdx(strFKCol);

	//setup temp list
	DbRecordSet lstTemp;
	lstTemp.copyDefinition(lstChilds);

	int nSize=lstParent.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		lstChilds.find(nFKID,lstParent.getDataRef(i,nParentID));
		lstTemp.clear();
		lstTemp.merge(lstChilds,true);
		lstParent.setData(i,nSubListIdx,lstTemp);
	}

	//if == 0, assign empty list to all cols
	if (nSize==0)
	{
		lstParent.setColValue(nSubListIdx,lstTemp);
	}

}



/*!
	Compare list1 and list2, compare is done by ID, 
	Returns intersection in list2 result and new entries 

	\param lstContact1				- contact list
	\param lstContact2				- contact list
	\param lstContactMatch			- matched contacts (list2 format)
	\param lstContactNew			- new contacts (list1 format)
	\param nPKIdx1					- primary key list1
	\param nPKIdx2					- primary key list2
	

*/
void ClientContactManager::CompareLists(DbRecordSet &lstContact1,DbRecordSet &lstContact2,DbRecordSet *lstContactMatch,DbRecordSet *lstContactNew, int nPKIdx1,int nPKIdx2)
{

	if(lstContactMatch)lstContactMatch->copyDefinition(lstContact2);
	if(lstContactNew)lstContactNew->copyDefinition(lstContact1);

	//loop through all
	lstContact1.clearSelection();
	lstContact2.clearSelection();
	int nRow;
	int nSize=lstContact1.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		nRow=lstContact2.find(nPKIdx2,lstContact1.getDataRef(i,nPKIdx1).toInt(),true);
		if (nRow!=-1)
			lstContact2.selectRow(nRow);
		else
			lstContact1.selectRow(i);
	}

	if(lstContactMatch)lstContactMatch->merge(lstContact2,true);
	if(lstContactNew)lstContactNew->merge(lstContact1,true);
}





void ClientContactManager::Read_vCard(const QString &strContents, DbRecordSet &lstResult)
{
	//define result list
	lstResult.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));

	//start parsing contents line by line
	QString strTmp = strContents;
	strTmp.replace("\r\n", "\n");
	QStringList lstLines = strTmp.split("\n", QString::SkipEmptyParts);

	int nRowLine = -1;

	int nCnt = lstLines.count();
	for(int i=0; i<nCnt; i++)
	{
		QString &strLine = lstLines[i];
		strLine = strLine.trimmed();

		//parse a single line
		if(0 == strLine.indexOf("BEGIN:VCARD", 0, Qt::CaseInsensitive))
		{
			//we found a new vCard inside the file
			lstResult.addRow();
			nRowLine = lstResult.getRowCount()-1;
		}
		else if(0 == strLine.indexOf("END:VCARD", 0, Qt::CaseInsensitive))
		{
			//vCard ended
			nRowLine = -1;
		}
		else if(nRowLine >= 0)
		{
			if( 0==strLine.indexOf("N:", 0, Qt::CaseInsensitive) ||
				0==strLine.indexOf("N;", 0, Qt::CaseInsensitive))
			{
				//parse "name" field
				int nPos = strLine.indexOf(":");
				if(nPos > 0)
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker

				QStringList lstParts = strLine.split(";", QString::KeepEmptyParts);
				int nPartsCnt = lstParts.count();
				if(nPartsCnt>0) lstResult.setData(nRowLine, "BCNT_LASTNAME", lstParts[0]);
				if(nPartsCnt>1) lstResult.setData(nRowLine, "BCNT_FIRSTNAME", lstParts[1]);
				if(nPartsCnt>2) lstResult.setData(nRowLine, "BCNT_MIDDLENAME", lstParts[2]);
			}
			else if(0==strLine.indexOf("ADR:", 0, Qt::CaseInsensitive) ||
					0==strLine.indexOf("ADR;", 0, Qt::CaseInsensitive))
			{
				//parse "address" field
				int nPos = strLine.indexOf(":");
				if(nPos > 0)
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker

				QStringList lstParts = strLine.split(";", QString::KeepEmptyParts);
				int nPartsCnt = lstParts.count();
				if(nPartsCnt>0) lstResult.setData(nRowLine, "BCMA_POBOX", lstParts[0]);
				//if(nPartsCnt>1) lstResult.setData(nRowLine, "BCNT_FIRSTNAME", lstParts[1]);
				if(nPartsCnt>2) lstResult.setData(nRowLine, "BCMA_STREET", lstParts[2]);
				if(nPartsCnt>3) lstResult.setData(nRowLine, "BCMA_CITY", lstParts[3]);
				if(nPartsCnt>4) lstResult.setData(nRowLine, "BCMA_REGION", lstParts[4]);
				if(nPartsCnt>5) lstResult.setData(nRowLine, "BCMA_ZIP", lstParts[5]);
				if(nPartsCnt>6){ 
					lstResult.setData(nRowLine, "BCMA_COUNTRY_NAME", lstParts[6]);
					lstResult.setData(nRowLine, "BCMA_COUNTRY_CODE", Countries::GetISOCodeFromCountry(lstParts[6]));
				}
			}
			else if(0==strLine.indexOf("TEL:", 0, Qt::CaseInsensitive) ||
					0==strLine.indexOf("TEL;", 0, Qt::CaseInsensitive))
			{
				//parse "telephone" field
				QString strLeft;
				int nPos = strLine.indexOf(":");
				if(nPos > 0){
					strLeft = strLine.left(nPos);
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker
				}

				int nFieldsWritten = 0;
				if(strLeft.indexOf("FAX", 0, Qt::CaseInsensitive) >= 0){
					lstResult.setData(nRowLine, "CLC_PHONE_FAX", strLine);
					nFieldsWritten ++;
				}
				if(strLeft.indexOf("CELL", 0, Qt::CaseInsensitive) >= 0){
					lstResult.setData(nRowLine, "CLC_PHONE_MOBILE", strLine);
					nFieldsWritten ++;
				}
				if(strLeft.indexOf("WORK", 0, Qt::CaseInsensitive) >= 0){
					bool bPref = (strLeft.indexOf(",pref;") >= 0);
					//MB request: when having two work phones, keep the prefered one and move the other to the "fax" field
					QString strOldData = lstResult.getDataRef(nRowLine, "CLC_PHONE_BUSINESS_DIRECT").toString();
					if(strOldData.isEmpty())
						lstResult.setData(nRowLine, "CLC_PHONE_BUSINESS_DIRECT", strLine);
					else{
						if(bPref){
							lstResult.setData(nRowLine, "CLC_PHONE_BUSINESS_DIRECT", strLine);
							lstResult.setData(nRowLine, "CLC_PHONE_FAX", strOldData);
						}
						else
							lstResult.setData(nRowLine, "CLC_PHONE_FAX", strLine);
					}
					nFieldsWritten ++;
				}
				if(strLeft.indexOf("HOME", 0, Qt::CaseInsensitive) >= 0){
					lstResult.setData(nRowLine, "CLC_PHONE_PRIVATE", strLine);
					nFieldsWritten ++;
				}
				if(0 == nFieldsWritten) //default
					lstResult.setData(nRowLine, "CLC_PHONE_PRIVATE", strLine);
			}
			else if(0==strLine.indexOf("EMAIL:", 0, Qt::CaseInsensitive) ||
					0==strLine.indexOf("EMAIL;", 0, Qt::CaseInsensitive))
			{
				//parse "Email" field
				int nPos = strLine.indexOf(":");
				if(nPos > 0){
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker
				}
				lstResult.setData(nRowLine, "CLC_EMAIL_ADDRESS", strLine);
			}
			else if(0==strLine.indexOf("URL:", 0, Qt::CaseInsensitive) ||
					0==strLine.indexOf("URL;", 0, Qt::CaseInsensitive))
			{
				//parse "web address" field
				int nPos = strLine.indexOf(":");
				if(nPos > 0){
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker
				}
				lstResult.setData(nRowLine, "CLC_WEB_ADDRESS", strLine);
			}
			else if(0==strLine.indexOf("ORG:", 0, Qt::CaseInsensitive) ||
					0==strLine.indexOf("ORG;", 0, Qt::CaseInsensitive))
			{
				//parse "Organization" field
				int nPos = strLine.indexOf(":");
				if(nPos > 0){
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker
				}
				lstResult.setData(nRowLine, "BCNT_ORGANIZATIONNAME_2", strLine);
				lstResult.setData(nRowLine, "BCMA_ORGANIZATIONNAME", strLine);
			}
			else if(0==strLine.indexOf("BDAY:", 0, Qt::CaseInsensitive) ||
					0==strLine.indexOf("BDAY;", 0, Qt::CaseInsensitive))
			{
				//parse "Birthday" field
				int nPos = strLine.indexOf(":");
				if(nPos > 0){
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker
				}

				QDate datBirth = QDate::fromString(strLine, "yyyyMMdd");
				if(!datBirth.isValid())
					datBirth = QDate::fromString(strLine, "yyyy-MM-dd");
				lstResult.setData(nRowLine, "BCNT_BIRTHDAY", datBirth);
			}
			else if(0==strLine.indexOf("NOTE:", 0, Qt::CaseInsensitive) ||
					0==strLine.indexOf("NOTE;", 0, Qt::CaseInsensitive))
			{
				//parse "Note" field
				int nPos = strLine.indexOf(":");
				if(nPos > 0){
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker
				}
				lstResult.setData(nRowLine, "BCNT_DESCRIPTION", strLine);
			}
			else if(0==strLine.indexOf("TITLE:", 0, Qt::CaseInsensitive) ||
					0==strLine.indexOf("TITLE;", 0, Qt::CaseInsensitive))
			{
				//parse "Title" field
				int nPos = strLine.indexOf(":");
				if(nPos > 0){
					strLine = strLine.right(strLine.size()-nPos-1);	//cut field marker
				}
				lstResult.setData(nRowLine, "BCNT_FUNCTION", strLine);
			}
		}
	}
}

void ClientContactManager::Write_vCard(const DbRecordSet &lstContents, QString &strResult)
{
	strResult = "";

	int nCnt = lstContents.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		strResult += "BEGIN:VCARD\r\n";
		strResult += "VERSION:2.1\r\n";

		//write formatted name
		strResult += "FN:";
		strResult += lstContents.getDataRef(i, "BCNT_FIRSTNAME").toString();
		strResult += " ";
		strResult += lstContents.getDataRef(i, "BCNT_LASTNAME").toString();
		strResult += "\r\n";
		
		//write name
		strResult += "N:";
		strResult += lstContents.getDataRef(i, "BCNT_LASTNAME").toString();
		strResult += ";";
		strResult += lstContents.getDataRef(i, "BCNT_FIRSTNAME").toString();
		strResult += ";";
		strResult += lstContents.getDataRef(i, "BCNT_MIDDLENAME").toString();
		strResult += ";;\r\n";

		//write address
		strResult += "ADR:";
		strResult += lstContents.getDataRef(i, "BCMA_POBOX").toString();
		strResult += ";;"; // one field empty
		strResult += lstContents.getDataRef(i, "BCMA_STREET").toString();
		strResult += ";";
		strResult += lstContents.getDataRef(i, "BCMA_CITY").toString();
		strResult += ";";
		strResult += lstContents.getDataRef(i, "BCMA_REGION").toString();
		strResult += ";";
		strResult += lstContents.getDataRef(i, "BCMA_ZIP").toString();
		strResult += ";";
		strResult += lstContents.getDataRef(i, "BCMA_COUNTRY_NAME").toString();
		strResult += "\r\n";

		//write phones
		if(!lstContents.getDataRef(i, "CLC_PHONE_FAX").toString().isEmpty()){
			strResult += "TEL;TYPE=FAX:";
			strResult += lstContents.getDataRef(i, "CLC_PHONE_FAX").toString();
			strResult += "\r\n";
		}
		if(!lstContents.getDataRef(i, "CLC_PHONE_MOBILE").toString().isEmpty()){
			strResult += "TEL;TYPE=CELL:";
			strResult += lstContents.getDataRef(i, "CLC_PHONE_MOBILE").toString();
			strResult += "\r\n";
		}
		if(!lstContents.getDataRef(i, "CLC_PHONE_BUSINESS_DIRECT").toString().isEmpty()){
			strResult += "TEL;TYPE=WORK:";
			strResult += lstContents.getDataRef(i, "CLC_PHONE_BUSINESS_DIRECT").toString();
			strResult += "\r\n";
		}
		if(!lstContents.getDataRef(i, "CLC_PHONE_PRIVATE").toString().isEmpty()){
			strResult += "TEL;TYPE=HOME:";
			strResult += lstContents.getDataRef(i, "CLC_PHONE_PRIVATE").toString();
			strResult += "\r\n";
		}

		//write web address
		if(!lstContents.getDataRef(i, "CLC_WEB_ADDRESS").toString().isEmpty()){
			strResult += "URL:";
			strResult += lstContents.getDataRef(i, "CLC_WEB_ADDRESS").toString();
			strResult += "\r\n";
		}

		//write organization
		if(!lstContents.getDataRef(i, "BCNT_ORGANIZATIONNAME_2").toString().isEmpty()){
			strResult += "ORG:";
			strResult += lstContents.getDataRef(i, "BCNT_ORGANIZATIONNAME_2").toString();
			strResult += "\r\n";
		}

		//write birthday
		if(!lstContents.getDataRef(i, "BCNT_BIRTHDAY").toDate().isValid()){
			strResult += "BDAY:";
			strResult += lstContents.getDataRef(i, "BCNT_BIRTHDAY").toDate().toString("yyyyMMdd");
			strResult += "\r\n";
		}

		//write email
		if(!lstContents.getDataRef(i, "CLC_EMAIL_ADDRESS").toString().isEmpty()){
			strResult += "EMAIL:";
			strResult += lstContents.getDataRef(i, "CLC_EMAIL_ADDRESS").toString();
			strResult += "\r\n";
		}

		//write function
		if(!lstContents.getDataRef(i, "BCNT_FUNCTION").toString().isEmpty()){
			strResult += "TITLE:";
			strResult += lstContents.getDataRef(i, "BCNT_FUNCTION").toString();
			strResult += "\r\n";
		}
		
		//write description
		if(!lstContents.getDataRef(i, "BCNT_DESCRIPTION").toString().isEmpty()){
			strResult += "NOTE:";
			strResult += lstContents.getDataRef(i, "BCNT_DESCRIPTION").toString();
			strResult += "\r\n";
		}
		
		strResult += "END:VCARD\r\n";
	}
}

bool ClientContactManager::LoadFileFromURL(const QString &strURL, QByteArray &strResult)
{
	HttpReaderSync WebReader(6000);  //6sec..
	WebReader.ReadWebContent(strURL, &strResult);;
	//qDebug()<<strResult.constData();
	return true;
}


//lstVcards is file list: <filepath,name, extension>
DbRecordSet ClientContactManager::CreateContactsFromvCard(DbRecordSet &lstVcards)
{
	DbRecordSet lstImportContacts;
	lstImportContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));
	//read file content
	int nSize=lstVcards.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strFilePath=lstVcards.getDataRef(i,0).toString();
		QFile file(strFilePath);
		if(!file.open(QIODevice::ReadOnly)) continue;

		QByteArray byteContent=file.readAll();
		file.close();

		if (byteContent.size()>0)
		{
			DbRecordSet lstData;
			Read_vCard(byteContent,lstData);
			lstImportContacts.merge(lstData);
		}
	}

	//lstImportContacts.Dump();
	return lstImportContacts;

}
DbRecordSet ClientContactManager::CreateContactsFromvCardUrl(QString strURL)
{	
	DbRecordSet lstImportContacts;
	lstImportContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));

	QByteArray strvCard;
	LoadFileFromURL(strURL,strvCard);
	//qDebug()<<strvCard;
	Read_vCard(QString(strvCard),lstImportContacts);
	//lstImportContacts.Dump();
	return lstImportContacts;
}

DbRecordSet ClientContactManager::CreateContactsFromvCardData(QString strvCard)
{
	DbRecordSet lstImportContacts;
	lstImportContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));
	Read_vCard(strvCard,lstImportContacts);
	return lstImportContacts;
}

//BT: for QCW parse string as url or vcf content
void ClientContactManager::CreateFullContactFromvCardData(QString strvCard, bool bIsUrl, DbRecordSet &lstContact)
{
	DbRecordSet lstImportContacts;
	lstImportContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));
	if (bIsUrl)
	{
		QByteArray strvCardContent;
		LoadFileFromURL(strvCard,strvCardContent);
		Read_vCard(QString(strvCardContent),lstImportContacts);
	}
	else
	{
		Read_vCard(strvCard,lstImportContacts);
	}

	ConvertContactListFromImport(lstImportContacts, lstContact,""); 
	PrepareContactListFromImport(lstContact); 
}
//BT: for QCW parse list of file url to vcf files
void ClientContactManager::CreateFullContactFromvCardFile(DbRecordSet &lstVcards, DbRecordSet &lstContact)
{
	DbRecordSet lstImportContacts;
	lstImportContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));
	//read file content
	int nSize=lstVcards.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strFilePath=lstVcards.getDataRef(i,0).toString();
		QFile file(strFilePath);
		if(!file.open(QIODevice::ReadOnly)) continue;

		QByteArray byteContent=file.readAll();
		file.close();

		if (byteContent.size()>0)
		{
			DbRecordSet lstData;
			Read_vCard(byteContent,lstData);
			lstImportContacts.merge(lstData);
		}
	}

	ConvertContactListFromImport(lstImportContacts, lstContact,""); 
	PrepareContactListFromImport(lstContact); 
}

bool ClientContactManager::CreateSubTypeAddressList(DbRecordSet &lstAddress,DbRecordSet &lstTypes,QString strSubTypeColName)
{

	//if does not have tmp cols, exit
	int nAdrTmpIdx=lstAddress.getColumnIdx("NM_TEMP_ID");
	int nTypeTmpIdx=lstTypes.getColumnIdx("NM_TEMP_ID");
	if (nAdrTmpIdx==-1 || nTypeTmpIdx==-1)
		return false;

	if (lstAddress.getColumnIdx(strSubTypeColName)==-1)
		lstAddress.addColumn(DbRecordSet::GetVariantType(),strSubTypeColName);
	
	int nSize=lstTypes.getRowCount();
	int nSize2=lstAddress.getRowCount();
	for(int j=0;j<nSize2;++j)
	{
		int nAdrressID=lstAddress.getDataRef(j,"BCMA_ID").toInt();
		int nAdrressTempID=lstAddress.getDataRef(j,nTypeTmpIdx).toInt();
		lstTypes.clearSelection();

		for (int i=0;i<nSize;++i)
		{
			if(nAdrressID!=0)
			{
				if(lstTypes.getDataRef(i,"BCMAT_ADDRESS_ID").toInt()==nAdrressID) 
				{
					lstTypes.selectRow(i);
					continue;
				}
			}
			else
			{
				if(lstTypes.getDataRef(i,nTypeTmpIdx).toInt()==nAdrressTempID) 
				{
					lstTypes.selectRow(i);
					continue;
				}
			}
		}
		DbRecordSet row = lstTypes.getSelectedRecordSet();
		lstAddress.setData(j,strSubTypeColName,row);
	}

	return true;
}




void ClientContactManager::ConvertContactListFromImport(DbRecordSet &lstContactTxtFormat,DbRecordSet &lstContact, QString strOrganizationNameForced, QProgressDialog *pDlg)
{
	MainEntitySelectionController Person;
	Person.Initialize(ENTITY_BUS_PERSON);
	Person.ReloadData();
	ContactTypeManager::ConvertContactListFromImport(lstContactTxtFormat,lstContact,strOrganizationNameForced,Person.GetDataSource());

}
void ClientContactManager::PrepareContactListFromImport(DbRecordSet &lstContact, QProgressDialog *pDlg)
{
	DbRecordSet *data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
	DbRecordSet recSettings,recOptions;
	g_pSettings->GetDataSource(recOptions,recSettings);
	MainEntitySelectionController SchemaHandler;
	SchemaHandler.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
	SchemaHandler.ReloadData();
	DbRecordSet *lstSchemas=SchemaHandler.GetDataSource();
	ContactTypeManager::PrepareContactListFromImport(lstContact,data,&recSettings,&recOptions,g_pClientManager->GetPersonID(),lstSchemas);
}






//  Extracts an email address and it's description from a on-line-string if there is any.
//  Valid characters are checked according to RFC 2822, but with a stronger restriction:
//  Only .-+%_ are accepted as special characters in the email address.
void ClientContactManager::Parse_Mail(QString strLine, QString &strEmail, QString &strDesc)
{

	strEmail="";
	strDesc="";

	int nPos=strLine.indexOf("@");
	if(nPos==-1)
		return;

	QString cValidCharsLP("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+%_");
	QString cValidCharsDP("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_");


	if (strLine.mid(nPos-1,1)!=" " && strLine.mid(nPos+1,1)!=" ") //;; @ has characters around it
		if (nPos>0 && nPos<(strLine.length()-1)) //;; @ is not on the start or end of the line
		{
			//	Find local part:
			int nIndex=nPos;
			do 
			{
				nIndex--;
				if (nIndex<0)
				{
					nIndex=0;
					break;
				}
			} while(cValidCharsLP.indexOf(strLine.at(nIndex))>=0);

	
			//;  Find domain part:
			int nIndex2=nPos;
			do 
			{
				nIndex2++;
				if (nIndex2>(strLine.length()-1))
				{
					nIndex2=strLine.length()-1;
					break;
				}
			} while(cValidCharsDP.indexOf(strLine.at(nIndex2))>=0);

			if (nIndex2>nPos && nIndex<nPos-1)
			{
				int nOffset1=1,nOffset2=1;
				if (nIndex==0)
					nOffset1=0;
				if (nIndex2==(strLine.length()-1))
					nOffset2=0;


				strEmail=strLine.mid(nIndex+nOffset1,nIndex2-nIndex-nOffset2+1).trimmed();

				//detect desc:
				QString strLeft;
				if (nIndex>0) //if 0 then left is empty
					strLeft=strLine.left(nIndex+1).trimmed();

				QString strRight;
				if (nIndex2<(strLine.length()-1))
					strRight=strLine.mid(nIndex2,strLine.length()).trimmed();

			

				if (strLeft.indexOf("MAILTO:",0,Qt::CaseInsensitive)>=0)
				{
					strLeft = strLeft.replace("MAILTO:","",Qt::CaseInsensitive).trimmed();
				}

				if (strLeft.length()>1 && strLeft.indexOf(":")>0)// ;; a ':' found on the left side: The description must be there!
				{
					strDesc=strLeft.mid(0,strLeft.indexOf(":"));
				}
				else if (strLeft.length()>strRight.length())
				{
					strDesc=strLeft;
				}
				else
				{
					strRight.replace("(","");
					strRight.replace(")","");
					strDesc=strRight;
				}
			}

		}

}


//  Extracts a phone number, its description and its type from a on-line-string (if there is any).
//  This one is not using regular expressions because O$ does not support this. Regex for int. phone numbers: http://regexlib.com/Search.aspx?k=phone
 void ClientContactManager::Parse_Phone(QString strLine, QString &strPhone, QString &strDesc,int &nType)
{


	//  Initialize return vars:
	strPhone="";
	strDesc="";
	nType=ContactTypeManager::SYSTEM_TYPE_EMPTY;
	QString cValidCharsSk("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_");
	QString cValidCharsPH("0123456789-+() ");


	int i=strLine.indexOf("SKYPE");
	if (i>0)
	{
		QString strLeft=strLine.mid(0,i);
		QString strRight=strLine.mid(i+5,strLine.length());
		if (strRight.size()>strLeft.size())
		{
			strLeft=strRight;
		}

		//  Find and remove "Name" if it occurs after "Skype" (e.g. in "Skype-name:"):
		strLeft=strLeft.trimmed();
		QString strSpecial("-: ");
		while (strSpecial.indexOf(strLeft.left(1))>=0 && strLeft.size()>0)
		{
			strLeft=strLeft.mid(1,strLeft.length());
		}
		if (strLeft.toUpper().indexOf("NAME")==0)
		{
			strLeft=strLeft.mid(4,strLeft.length());
		}

		//Remove leading invalid characters:
		while (cValidCharsSk.indexOf(strLeft.left(1))<0 && strLeft.size()>0)
		{
			strLeft=strLeft.mid(1,strLeft.length());
		}

		//;  Find all valid characters:
		if (strLeft.size()>0)
		{
			strPhone=strLeft;
			strDesc="Skype";
			nType=ContactTypeManager::SYSTEM_SKYPE; //internal
			return;
		}
	}



	// Not a Skype number but an "ordinary" phone number with digits:
	// Are there at least two digits?
	int nDigits=CountDigits(strLine);
	if (nDigits<2)
		return; //failed

	// Is there any expression showing this could be an internal number? These are the only phone numbers which may be shorter then 6 digits!
	if (nDigits<7)
	{
		QString strLineUPP=strLine.toUpper();
		if (strLineUPP.indexOf("(INTERN)")>=0 || 
			strLineUPP.indexOf("(INT)")>=0 ||
			strLineUPP.indexOf("INTERN:")>=0 ||
			strLineUPP.indexOf("INT:")>=0 ||
			strLineUPP.indexOf("INTERNAL:")>=0 ||
			strLineUPP.indexOf("INTERNE:")>=0 ||
			strLineUPP.indexOf("INTERNO:")>=0 ||
			strLineUPP.indexOf("INT")==0 ||
			strLineUPP.indexOf("INTERN ")==0)
		{
			nType=ContactTypeManager::SYSTEM_PHONE_INTERNAL; //internal
		}
		else
			return;
	}

	if (nDigits!=7) //  Find a block of at least 6 valid characters and expand it to get the full phone number:
	{
		i=0;
		while (i<strLine.size() && ( strLine.mid(i,1)==")" || !(
			cValidCharsPH.indexOf(strLine.mid(i,1))>=0  &&
			cValidCharsPH.indexOf(strLine.mid(i+1,1))>=0   &&
			cValidCharsPH.indexOf(strLine.mid(i+2,1))>=0   &&
			cValidCharsPH.indexOf(strLine.mid(i+3,1))>=0   &&
			cValidCharsPH.indexOf(strLine.mid(i+4,1))>=0   &&
			cValidCharsPH.indexOf(strLine.mid(i+5,1))>=0)))
		{
			i++;
		}
		if (i>=strLine.size())//No valid block found --> leave
		{
			return;
		}


	}
	else //;  Find a block of at least 2 valid characters and expand it to get the full phone number:
	{
		
		i=0;
		while (i<strLine.size() && ( strLine.mid(i,1)==")" || 
			!(cValidCharsPH.indexOf(strLine.mid(i,1))>=0   &&
			cValidCharsPH.indexOf(strLine.mid(i+1,1))>=0   )))
		{
			i++;
		}
		if (i>=strLine.size())//No valid block found --> leave
		{
			return;
		}
	}


	// Copy the phone number into lString and remove everything not belonging to it:
	QString strString=strLine.mid(i,strLine.length());

	i=0;
	while (i<strString.size() && cValidCharsPH.indexOf(strString.mid(i,1))>=0)
	{
		i++;
	}
	if (strString.mid(i-1,1)=="(")
	{
		i=i-1;
	}
	strString=strString.mid(0,i).trimmed();
	if (strString.length()>0)
	{
		strPhone=strString;
	}

	//Phone number extracted, no find description and type:
	
	//Search for description:
	//------------------------------------
	i=strLine.indexOf(strString);
	QString strLeft=strLine.mid(0,i).trimmed();
	QString strRight=strLine.mid(i+strString.length(),strLine.length()).trimmed();

	if (strLeft.length()>1 && strLeft.indexOf(":")>0) //;; a ':' found on the left side: The description must be there!
	{
		strDesc=strLeft.mid(0,strLeft.indexOf(":"));
	}
	else if (strLeft.length()>strRight.length())//  ;; Select the larger string to be the description and ignore the other
	{
		strDesc=strLeft;
	}
	else
	{
		//  Remove brackets:
		strRight=strRight.replace("(","");
		strRight=strRight.replace(")","");
		strDesc=strRight;
	}


	// Determine Type:
	//------------------------------------
	if (nType==0)
	{
		QString strDescUpp=strDesc.toUpper();

		if (strDescUpp.indexOf("FAX")>=0 ||
			strDescUpp.indexOf("(F)")>=0 ||
			strDescUpp=="F" ||
			strDescUpp=="F:" ||
			strDescUpp.mid(strDescUpp.length()-2,2)=="F:" )
		{
			nType=ContactTypeManager::SYSTEM_FAX;
		}
		else if (strDescUpp.indexOf("MOB")>=0 ||
			strDescUpp.indexOf("MOBILE")>=0 ||
			strDescUpp.indexOf("NAT")>=0 ||
			strDescUpp.indexOf("(M)")>=0 ||
			strDescUpp.indexOf("(N)")>=0 ||
			strDescUpp=="N" ||
			strDescUpp=="N:" ||
			strDescUpp=="M" ||
			strDescUpp=="M:" ||
			strDescUpp.mid(strDescUpp.length()-2,2)==" M" ||
			strDescUpp.mid(strDescUpp.length()-2,2)==" N" 
			)
		{
			nType=ContactTypeManager::SYSTEM_PHONE_MOBILE;
		}
		else if (strDescUpp.indexOf("PRIVATE")>=0 ||
			strDescUpp.indexOf("PRIV")>=0 ||
			strDescUpp.indexOf("(P)")>=0 ||
			strDescUpp=="P" ||
			strDescUpp=="P:" ||
			strDescUpp.mid(strDescUpp.length()-2,2)==" P" 
			)
		{
			nType=ContactTypeManager::SYSTEM_PHONE_PRIVATE;
		}
		else if (strDescUpp.indexOf("DIRECT")>=0 ||
			strDescUpp.indexOf("DIREKT")>=0 ||
			strDescUpp.indexOf("(D)")>=0 ||
			strDescUpp=="D" ||
			strDescUpp=="D:" ||
			strDescUpp=="DIR:" ||
			strDescUpp=="DIR" ||
			strDescUpp.mid(strDescUpp.length()-2,2)==" D" 
			)
		{
			nType=ContactTypeManager::SYSTEM_PHONE_DIRECT;
		}
		else
			nType=ContactTypeManager::SYSTEM_PHONE_BUSINESS; //default

	}


}

int ClientContactManager::CountDigits(QString strLine)
{
	int nCnt=0;
	int nSize=strLine.size();
	for(int i=0;i<nSize;++i)
	{
		if (strLine.at(i).isNumber())
			nCnt++;
	}

	return nCnt;
}





void ClientContactManager::Parse_Internet(QString strLine, QString &strUrl, QString &strDesc)
{

	strUrl="";
	strDesc="";

	QString cValidCharsURL("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+%_&@#/:;=");


	QString strKey="http://";
	int i=strLine.indexOf(strKey);
	if (i<0)
	{
		strKey="https://";
		i=strLine.indexOf(strKey);
	}
	if (i<0)
	{
		strKey="ftp://";
		i=strLine.indexOf(strKey);
	}
	if (i<0)
	{
		strKey="www.";
		i=strLine.indexOf(strKey);
	}


	if (i>=0)
	{
		//  Extract URL
		int j=i+strKey.length(); //    ;; Start detecting characters belonging to the local part (left side) of the email address

		while (j<strLine.length())
		{
			if (cValidCharsURL.indexOf(strLine.at(j))<0)
				break;
			j++;
		}

		strUrl=strLine.mid(i,j-i+1); //     ;; Email address found --> store in result var
		//  Search for description:
		QString strLeft=strLine.left(i).trimmed();
		QString strRight=strLine.mid(j+1,strLine.length()).trimmed();

		if (strLeft.length()>1 && strLeft.indexOf(":")>=0)//;; a ':' found on the left side: The description must be there!
		{
			strDesc=strLeft.left(strLeft.indexOf(":"));
		}
		else if (strLeft.length()>strRight.length())
		{
			strDesc=strLeft;
		}
		else
		{
			strRight.replace("(","");
			strRight.replace(")","");
			strDesc=strRight;
		}

	}

}

void ClientContactManager::ExtractTownNames(QString strLine,QString &strTown,QString &strState,int &nZIP)
{
	//;  Gets a line of text, interprets it as an address line containing state/country, ZIP code and town, and returns the single items separated by //
	nZIP=FindLargestInteger(strLine); //;; Find ZIP code
	//;  Extract the strings on the left and right of the ZIP code:

	strTown="";
	strState="";

	if (nZIP==-1)
		return;

	int i= strLine.indexOf(QVariant(nZIP).toString());
	QString strLeft=strLine.left(i).trimmed();
	QString strRight=strLine.mid(i+QVariant(nZIP).toString().length(),strLine.length()).trimmed();
	//	;  Is there a string on the right side of the ZIP code?
	if (strRight.length()>1)
	{
		strTown=strRight;//	;  It must be the town:
		strState=strLeft;//	;  The Country or State code is on the left (if any):
		strState.replace("-",""); //;; Remove a "-" if it is found between country and zip
	}
	else
	{
		//;; The town is on the start of the line!

		if (strLeft.indexOf(",")>=0)
		{
			strTown=strLeft.left(strLeft.indexOf(","));
			strState=strLeft.mid(strLeft.indexOf(",")+1,strLeft.length());
		}
		else
		{
			strTown=strLeft;
		}
	}

	strTown=strTown.trimmed();
	strState=strState.trimmed();



}

int ClientContactManager::FindLargestInteger(QString strLine)
{
	//;  Finds the largest integer number in a string

	QString strDigits="0123456789";
	int nNum=-1;
	int nLen=strLine.length();
	int i=0,j=0;

	while (i<nLen)
	{
		if (strDigits.indexOf(strLine.mid(i,1))>=0)//;; Digit found
		{
			j=i+1;
			while (j<strLine.length()) // ;; Find more digits
			{
				if (strDigits.indexOf(strLine.at(j))<0)
					break;
				j++;
			}

			if (strLine.mid(i,j-i).toInt()>nNum)
			{
				nNum=strLine.mid(i,j-i).toInt(); // ;; Larger number found
			}

			i=j; //increase i
		}
		else
			i++;
	}


	return nNum;

}

void ClientContactManager::ExtractNames(QString strLine1,QString strLine2,	QString &strFirstName,QString &strLastName,QString &strMiddleName,QString &strCompanyName)
{

	strCompanyName="";
	strLastName="";
	strFirstName="";
	strMiddleName="";


	//;  Gets one or two lines of text, interprets it as the first two address lines containing company and name information, and returns the single items separated by //
	//;  Try to find typical suffixes of companies in one of the two lines: This is the company, the other line could be a person's name
	QString strCompanyKeys="AG*PLC*Limited*Ltd*Ltd.*LLC*ltd*ltd.*ag*S.p.A*S.p.A.*SA*Srl*S.r.l.*S.A*N.V.*NV*B.V.*BV*AO*OOO*d.d.*d.o.o.*doo*dd*Co.*Cie.*Bros.*cie.*co.*sa*GmbH*gmbh*";
	QStringList lstCompanyKeys=strCompanyKeys.split("*",QString::SkipEmptyParts);

	strLine1=strLine1.trimmed();
	strLine2=strLine2.trimmed();

	int nCompLine=-1,nNameLine=-1;
	bool bFound=false;

	if (!strLine1.isEmpty())
	{
		//;  Find a company suffix in the first line:
		int i=0;
		while (i<lstCompanyKeys.size() && !bFound)
		{
			QString strFind1=QString(" ")+lstCompanyKeys.at(i)+QString(" ");
			if (strLine1.indexOf(strFind1)>=0) //Company key with spaces around it can be found
			{
				bFound=true;
				strCompanyName=strLine1;
			}
			QString strFind2=QString(" ")+lstCompanyKeys.at(i);
			if (strLine1.right(strFind2.length())==strFind2) // ;; Company key with a space in front of it is on the end of the line
			{
				bFound=true;
				strCompanyName=strLine1;
			}

			i++;
		}

		if (bFound)
		{
			nCompLine=0;
		}
		else
		{

			if (!strLine2.isEmpty())
			{
				//;  Find a company suffix in the second line:
				int i=0;
				while (i<lstCompanyKeys.size() && !bFound)
				{
					QString strFind1=QString(" ")+lstCompanyKeys.at(i)+QString(" ");
					if (strLine2.indexOf(strFind1)>=0) //Company key with spaces around it can be found
					{
						bFound=true;
						strCompanyName=strLine2;
					}
					QString strFind2=QString(" ")+lstCompanyKeys.at(i);
					if (strLine2.right(strFind2.length())==strFind2) // ;; Company key with a space in front of it is on the end of the line
					{
						bFound=true;
						strCompanyName=strLine2;
					}

					i++;
				}

				if (bFound)
				{
					nCompLine=1;
				}
			}
		}
	}


	if (!bFound)
	{
		//  No company has been identified yet...
		//  Find special characters or numbers --> This is a company name

		if (!strLine1.isEmpty())
		{
			bFound=false;
			QString strNormalChars="%'&?$#\"@+_*/�()[]=�<>:";

			int nSize=strLine1.size();
			for(int i=0;i<nSize;++i)
			{
				if (strNormalChars.indexOf(strLine1.toUpper().mid(i,1),Qt::CaseInsensitive)>=0)
				{
					bFound=true;
					break;
				}
			}
			if (bFound)
			{
				strCompanyName=strLine1;
				nCompLine=0;
			}
		}

		if (!strLine2.isEmpty() && !bFound)
		{
			bFound=false;
			QString strNormalChars="%'&?$#\"@+_*/�()[]=�<>:";

			int nSize=strLine2.size();
			for(int i=0;i<nSize;++i)
			{
				if (strNormalChars.indexOf(strLine2.toUpper().mid(i,1),Qt::CaseInsensitive)>=0)
				{
					bFound=true;
					break;
				}
			}
			if (bFound)
			{
				strCompanyName=strLine2;
				nCompLine=1;
			}
		}
	}


	if (!bFound)
	{
		//;  No company has been identified yet...
		//;  Find a line with characters only and two or three parts --> This is a person's name

		if (!strLine1.isEmpty())
		{
			int nSpaces=strLine1.count(" ");
			if (nSpaces==1)
			{
				strFirstName=strLine1.left(strLine1.indexOf(" ")).trimmed();
				strLastName=strLine1.mid(strLine1.indexOf(" ")+1,strLine1.length()).trimmed();
				nNameLine=0;
				bFound=true;
			}
			else if (nSpaces==2)
			{
				int nFirstSpace=strLine1.indexOf(" ");
				int nSecondSpace=strLine1.indexOf(" ",nFirstSpace+1);

				strFirstName=strLine1.left(nFirstSpace).trimmed();
				strMiddleName=strLine1.mid(nFirstSpace+1,nSecondSpace-nFirstSpace).trimmed();
				strLastName=strLine1.mid(nSecondSpace+1,strLine1.length()).trimmed();

				bFound=true;
				nNameLine=0;
			}
			if (bFound && !strLine2.isEmpty())
			{
				strCompanyName=strLine2;
				nCompLine=1;

			}
		}
	}

	if (!bFound)
	{
		//;  No check the second line for the name...
		//;  Find a line with characters only and two or three parts --> This is a person's name
		if (!strLine2.isEmpty())
		{
			int nSpaces=strLine2.count(" ");
			if (nSpaces==1)
			{
				strFirstName=strLine2.left(strLine2.indexOf(" ")).trimmed();
				strLastName=strLine2.mid(strLine2.indexOf(" ")+1,strLine2.length()).trimmed();
				nNameLine=1;
				bFound=true;
			}
			else if (nSpaces==2)
			{
				int nFirstSpace=strLine2.indexOf(" ");
				int nSecondSpace=strLine2.indexOf(" ",nFirstSpace+1);

				strFirstName=strLine2.left(nFirstSpace).trimmed();
				strMiddleName=strLine2.mid(nFirstSpace+1,nSecondSpace-nFirstSpace).trimmed();
				strLastName=strLine2.mid(nSecondSpace+1,strLine2.length()).trimmed();

				bFound=true;
				nNameLine=1;
			}
			if (bFound && !strLine1.isEmpty())
			{
				strCompanyName=strLine1;
				nCompLine=0;

			}


		}
	}


	QString strNames;

	if (nCompLine >=0 && nNameLine==-1)//    ;; Company found but name not yet extracted
	{
		if (nCompLine==1)
			strNames=strLine1;
		else
			strNames=strLine2;

		if (!strNames.isEmpty())
		{
			//;  Are there only characters? This is mandatory for a name:
			bFound=false;

			QString strNormalChars="%'&?$#\"@+_*/�()[]=�<>:";
			int nSize=strNames.size();
			for(int i=0;i<nSize;++i)
			{
				if (strNormalChars.indexOf(strNames.toUpper().mid(i,1),Qt::CaseInsensitive)>=0)
				{
					bFound=true;
					break;
				}
			}
			if (!bFound) //;; No special characters found --> This is a name!
			{
				int nSpaces=strNames.count(" ");
				if (nSpaces==1)
				{
					strFirstName=strNames.left(strNames.indexOf(" "));
					strLastName=strNames.mid(strNames.indexOf(" ")+1,strNames.length());
					bFound=true;
				}
				else if (nSpaces==2)
				{
					int nFirstSpace=strNames.indexOf(" ");
					int nSecondSpace=strNames.indexOf(" ",nFirstSpace);

					strFirstName=strNames.left(nFirstSpace);
					strMiddleName=strNames.mid(nFirstSpace+1,nSecondSpace-nFirstSpace);
					strLastName=strNames.mid(nSecondSpace+1,strNames.length());

					bFound=true;
				}
			}
		}
	}


}




void ClientContactManager::Parse_Text(QString strText, DbRecordSet &lstContact)
{
	DbRecordSet lstContactLines;
	lstContactLines.addColumn(QVariant::String,"Line");
	lstContactLines.addColumn(QVariant::Int,"Type");			//0=Not Assigned 1=Separator 2=Phone Number 3=Email 4=URL 5=Adress Line 6=Unknown
	lstContactLines.addColumn(QVariant::String,"Item_Text");	//mail or phone, or address or url
	lstContactLines.addColumn(QVariant::String,"Item_Desc");
	lstContactLines.addColumn(QVariant::Int,"Item_SubType");    //system phone type or local use
	lstContactLines.addColumn(QVariant::Int,"Item_MaxNumber");

	DbRecordSet lstAddress;
	DbRecordSet lstEmails;
	DbRecordSet lstPhones;
	DbRecordSet lstInternet;
	lstContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE));
	lstPhones.addColumn(QVariant::String,"BCMT_TYPE_NAME");
	lstEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL));
	lstEmails.addColumn(QVariant::String,"BCMT_TYPE_NAME");
	lstInternet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET));
	lstInternet.addColumn(QVariant::String,"BCMT_TYPE_NAME");


	QStringList lstLines = strText.split("\n", QString::KeepEmptyParts);
	//determine separators & max numberS:
	int nSize=lstLines.size();
	for(int i=0;i<nSize;++i)
	{
		
		QString strTest=lstLines.at(i).left(4).trimmed();
		int nLen=strTest.length();

		int nType=0;int nMax;
		if (strTest==QString("----").left(nLen))
			nType=1;
		if (strTest==QString("****").left(nLen))
			nType=1;
		if (strTest==QString("____").left(nLen))
			nType=1;
		if (strTest==QString("=====").left(nLen))
			nType=1;
		if (strTest==QString("    ").left(nLen))
			nType=1;
		if (lstLines.at(i).trimmed().isEmpty())
			nType=1;

		if (nType==0)
		{
			nMax=FindLargestInteger(lstLines.at(i));
		}

		lstContactLines.addRow();
		lstContactLines.setData(i,0,lstLines.at(i));
		lstContactLines.setData(i,1,nType);
		lstContactLines.setData(i,"Item_MaxNumber",nMax);
	}


	//;  Find Emails:
	nSize=lstContactLines.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//skip assigned and separator lines
		if (lstContactLines.getDataRef(i,"Type").toInt()!=0)
			continue;

		QString strEmail,strDesc;
		Parse_Mail(lstContactLines.getDataRef(i,0).toString(),strEmail,strDesc);
		if (!strEmail.isEmpty())
		{
			lstContactLines.setData(i,"Item_Text",strEmail);
			lstContactLines.setData(i,"Item_Desc",strDesc);
			lstContactLines.setData(i,"Type",3);

			lstEmails.addRow();
			int n=lstEmails.getRowCount()-1;
			lstEmails.setData(n,"BCME_ADDRESS",strEmail);
			lstEmails.setData(n,"BCME_DESCRIPTION",strDesc);
		}
	}

	//;  Find Phone Numbers:
	nSize=lstContactLines.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//skip assigned and separator lines
		if (lstContactLines.getDataRef(i,"Type").toInt()!=0)
			continue;

		QString strEmail,strDesc;
		int nType;
		Parse_Phone(lstContactLines.getDataRef(i,0).toString(),strEmail,strDesc,nType);
		if (!strEmail.isEmpty())
		{
			lstContactLines.setData(i,"Item_Text",strEmail);
			lstContactLines.setData(i,"Item_Desc",strDesc);
			lstContactLines.setData(i,"Item_SubType",nType);
			lstContactLines.setData(i,"Type",2);

			lstPhones.addRow();
			int n=lstPhones.getRowCount()-1;
			lstPhones.setData(n,"BCMP_FULLNUMBER",strEmail);
			lstPhones.setData(n,"BCMP_NAME",strDesc);
			lstPhones.setData(n,"BCMP_TYPE_ID",GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,nType));
		}
	}


	//;  Find URLs:
	nSize=lstContactLines.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//skip assigned and separator lines
		if (lstContactLines.getDataRef(i,"Type").toInt()!=0)
			continue;

		QString strEmail,strDesc;
		Parse_Internet(lstContactLines.getDataRef(i,0).toString(),strEmail,strDesc);
		if (!strEmail.isEmpty())
		{
			lstContactLines.setData(i,"Item_Text",strEmail);
			lstContactLines.setData(i,"Item_Desc",strDesc);
			lstContactLines.setData(i,"Type",4);

			lstInternet.addRow();
			int n=lstInternet.getRowCount()-1;
			lstInternet.setData(n,"BCMI_ADDRESS",strEmail);
			lstInternet.setData(n,"BCMI_DESCRIPTION",strDesc);
		}
	}


	Parse_Addresses(lstContactLines,lstAddress);


	//filter and prepare for out:
	ContactTypeManager::AddEmptyRowToFullContactList(lstContact);
	//lstContact.addRow();
	int nContType=ContactTypeManager::CM_TYPE_PERSON;
	if (lstAddress.getRowCount()>0)
	{
		//if (lstContact.getDataRef(0,"BCNT_LASTNAME").toString().isEmpty())
		lstContact.setData(0,"BCNT_LASTNAME",lstAddress.getDataRef(0,"BCMA_LASTNAME"));
		//if (lstContact.getDataRef(0,"BCNT_FIRSTNAME").toString().isEmpty())
		lstContact.setData(0,"BCNT_FIRSTNAME",lstAddress.getDataRef(0,"BCMA_FIRSTNAME"));
		//if (lstContact.getDataRef(0,"BCNT_MIDDLENAME").toString().isEmpty())
		lstContact.setData(0,"BCNT_MIDDLENAME",lstAddress.getDataRef(0,"BCMA_MIDDLENAME"));
		//if (lstContact.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString().isEmpty())
		lstContact.setData(0,"BCNT_ORGANIZATIONNAME",lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME"));
		//if (lstContact.getDataRef(0,"BCNT_ORGANIZATIONNAME_2").toString().isEmpty())
		lstContact.setData(0,"BCNT_ORGANIZATIONNAME_2",lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME_2"));
		
		if (!lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME").toString().isEmpty() && lstAddress.getDataRef(0,"BCMA_LASTNAME").toString().isEmpty())
			nContType=ContactTypeManager::CM_TYPE_ORGANIZATION;

		//issue 1306:
		lstContact.setData(0,"BCNT_LOCATION",lstAddress.getDataRef(0,"BCMA_CITY"));
	}

	lstContact.setData(0,"LST_EMAIL",lstEmails);
	lstContact.setData(0,"LST_PHONE",lstPhones);
	lstContact.setData(0,"LST_INTERNET",lstInternet);
	lstContact.setData(0,"LST_ADDRESS",lstAddress);
	lstContact.setData(0,"BCNT_TYPE",nContType);

	//refill data:
	DbRecordSet *data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
	DbRecordSet recSettings,recOptions;
	g_pSettings->GetDataSource(recOptions,recSettings);
	MainEntitySelectionController SchemaHandler;
	SchemaHandler.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
	SchemaHandler.ReloadData();
	DbRecordSet *lstSchemas=SchemaHandler.GetDataSource();
	ContactTypeManager::PrepareContactListFromImport(lstContact,data,&recSettings,&recOptions,g_pClientManager->GetPersonID(),lstSchemas,true);

}


void ClientContactManager::Parse_Addresses(DbRecordSet &lstContactLines,DbRecordSet &recAddress)
{

	recAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
	DbRecordSet lstAddressTypes;
	lstAddressTypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT));
	recAddress.addColumn(QVariant::String,"BCMT_TYPE_NAME");
	recAddress.addColumn(DbRecordSet::GetVariantType(),"LST_TYPES");

	//none left to parse, skip
	int nCntUnAssignedLines=lstContactLines.find("Type",0);
	if (nCntUnAssignedLines==0)
		return;
	
	int nPosOpt_A=0;
	int nNumLns_A=0;
	int nLineCount=0;
	int nPos=-1;
	int nPosTown=0;
	int nPosTown_A=0;
	bool bTouched=false;


	//;  RUN  1: not Ignore single separators
	int nSize=lstContactLines.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstContactLines.getDataRef(i,1).toInt()==0) //type=0->unassigned
		{
			if (nPos==-1) //set start
				nPos=i;
			nLineCount++;
			bTouched=true;
		}
		else
		{
			bTouched=false;
			if (nLineCount<2) //if less then two lines, ignore
				continue;

			if ((i-1)>=0 && lstContactLines.getDataRef(i-1,"Item_MaxNumber").toInt()>=1000) //Last line contains a possible ZIP code --> Series valid
			{
				if (nLineCount>nNumLns_A)
				{
					nNumLns_A=nLineCount;
					nPosOpt_A=nPos;
					nPosTown_A=i-1;
				}
			}
			else if ((i-2)>=0 && lstContactLines.getDataRef(i-2,"Item_MaxNumber").toInt()>=1000)
			{
				if (nLineCount>nNumLns_A)
				{
					nNumLns_A=nLineCount;
					nPosOpt_A=nPos;
					nPosTown_A=i-2;
				}
			}
 
			nPos=-1;
			nLineCount=0;

		}
	}

	//some valid lines are detect at end, test em:
	if (bTouched)
	{
		int i=nSize;
		if (i>=0)
		{
			if ((i-1)>=0 && lstContactLines.getDataRef(i-1,"Item_MaxNumber").toInt()>=1000) //Last line contains a possible ZIP code --> Series valid
			{
				if (nLineCount>nNumLns_A)
				{
					nNumLns_A=nLineCount;
					nPosOpt_A=nPos;
					nPosTown_A=i-1;
				}
			}
			else if ((i-2)>=0 && lstContactLines.getDataRef(i-2,"Item_MaxNumber").toInt()>=1000)
			{
				if (nLineCount>nNumLns_A)
				{
					nNumLns_A=nLineCount;
					nPosOpt_A=nPos;
					nPosTown_A=i-2;
				}
			}
		}
	}



	//-----------------------------------------------------------------------------

	int nPosOpt_B=0;
	int nNumLns_B=0;
	bool bOnSeparator=false;
	int nLastLine=0;
	int nPosTown_B=0;
	

	nLineCount=0;
	nPos=-1;
	bTouched=false;
		

	//;  RUN 1: ignore single separators
	for(int i=0;i<nSize;++i)
	{
		if (lstContactLines.getDataRef(i,1).toInt()==0) //unassigned
		{
			if (nPos==-1) //set start
				nPos=i;
			bOnSeparator=false;
			nLineCount++;
			nLastLine=i;
			bTouched=true;
		}
		else
		{
			bTouched=false;
			if (nLineCount<2) //if less then two lines, ignore
				continue;

			if (lstContactLines.getDataRef(i,1).toInt()==1 && !bOnSeparator) //ignore once -> separator
			{
				bOnSeparator=true;
			}
			else
			{
				bOnSeparator=false;

				if ((nLastLine)>=0 && lstContactLines.getDataRef(nLastLine,"Item_MaxNumber").toInt()>=1000) //Last line contains a possible ZIP code --> Series valid
				{
					if (nLineCount>nNumLns_B)
					{
						nNumLns_B=nLineCount;
						nPosOpt_B=nPos;
						nPosTown_B=nLastLine;
					}
				}
				else if ((nLastLine-1)>=0 && lstContactLines.getDataRef(nLastLine-1,"Item_MaxNumber").toInt()>=1000) //Last line contains a possible ZIP code --> Series valid
				{
					if (nLineCount>nNumLns_B)
					{
						nNumLns_B=nLineCount;
						nPosOpt_B=nPos;
						nPosTown_B=nLastLine-1;
					}

				}

				nPos=-1;
				nLineCount=0;
				nLastLine=0;
			}
		}
	}


	//some valid lines are detect at end, test em:
	if (bTouched)
	{
		if (nLastLine>=0 && nLastLine<nSize)
		{
			if ((nLastLine)>=0 && lstContactLines.getDataRef(nLastLine,"Item_MaxNumber").toInt()>=1000) //Last line contains a possible ZIP code --> Series valid
			{
				if (nLineCount>nNumLns_B)
				{
					nNumLns_B=nLineCount;
					nPosOpt_B=nPos;
					nPosTown_B=nLastLine;
				}
			}
			else if ((nLastLine-1)>=0 && lstContactLines.getDataRef(nLastLine-1,"Item_MaxNumber").toInt()>=1000) //Last line contains a possible ZIP code --> Series valid
			{
				if (nLineCount>nNumLns_B)
				{
					nNumLns_B=nLineCount;
					nPosOpt_B=nPos;
					nPosTown_B=nLastLine-1;
				}

			}
		}
	}

	//-----------------------------------------------------------------------------


	//compare two passes:
	//;  Use the better result of the two runs:

	if (nNumLns_A<=2 && (nNumLns_B>nNumLns_A))
	{
		nPos=nPosOpt_B;
		nLineCount=nNumLns_B;
		nPosTown=nPosTown_B;
	}
	else
	{
		nPos=nPosOpt_A;
		nLineCount=nNumLns_A;
		nPosTown=nPosTown_A;
	}


	//	;  Mark the address line and put the subtype for the town and street lines:
	nLastLine=0;			 //;; The street line is marked as the one before the town line, and the last line is used to remember the one before the current.
	int nSecondLine=0;		 // ;; The second line is remembered for the extraction of the organization name
	int nStreetLine=0;
	int k=nPos;
	int nPOBOXLine=0;

	QString strTown,strState,strAddressAll,strStreet;
	QString strFirstName,strLastName,strMiddleName,strOrg;
	int nZIP;


	//check for PO BOX:
	//Emch+Berger AG Bern
	//Gartenstrasse 1
	//Postfach 6025
	//CH-3001 Bern
	if (nPosTown>(nPos+1) && nPosTown<(nPos+nLineCount)) //Postfach
	{
		QString strLine=lstContactLines.getDataRef(nPosTown-1,0).toString().toUpper(); 
		if (strLine.indexOf("POSTFACH")>=0 || 
			strLine.indexOf("P.O. BOX")>=0 ||
			strLine.indexOf("PO BOX")>=0)
		{
			nPOBOXLine=nPosTown-1;
		}
	}
	
	while (k<nSize && nLineCount>0)
	{
		if (lstContactLines.getDataRef(k,1).toInt()==0) //unassigned
		{
			if (nSecondLine==0 && k>nPos && nLineCount>2) //;; Second Line of Address found in an Address with more than 2 lines
			{
				nSecondLine=k;
			}

			QString strLine=lstContactLines.getDataRef(k,0).toString(); 
			if (!strAddressAll.isEmpty())
				strAddressAll.append("\n"+strLine);
			else
				strAddressAll=strLine;


			if (k==nPOBOXLine && k>0)
			{
				nStreetLine=nPOBOXLine-1;
			}
			else if (k==nPosTown)
			{
				if (nLastLine!=nPos && nStreetLine==0)
					nStreetLine=nLastLine;
				ExtractTownNames(strLine,strTown,strState,nZIP);
			}
			else
				nLastLine=k;

			nLineCount--;
		}
	k++;
	}


	//;  Extract and store the names:
	QString strLine2;
	if (nSecondLine==nStreetLine) //if they match then ignore 2nd line->postfach
		nSecondLine=0;

	if (nSecondLine>0)
		strLine2=lstContactLines.getDataRef(nSecondLine,0).toString();  //get 2nd line
	QString strLine=lstContactLines.getDataRef(nPos,0).toString();  //get first line
	ExtractNames(strLine,strLine2,strFirstName,strLastName,strMiddleName,strOrg);


	//	;  Extract and store the street:
	if (nStreetLine>0)
	{
		strStreet=lstContactLines.getDataRef(nStreetLine,0).toString();  
	}


	QString strPOBOX;
	if (nPOBOXLine>0)
	{
		strPOBOX=lstContactLines.getDataRef(nPOBOXLine,"Item_MaxNumber").toString();
	}

	//if exists, at least one
	if (!strAddressAll.isEmpty() ||!strFirstName.isEmpty() || !strLastName.isEmpty() || !strMiddleName.isEmpty() || !strOrg.isEmpty())
	{

		recAddress.addRow();
		recAddress.setData(0,"BCMA_FORMATEDADDRESS",strAddressAll);
		recAddress.setData(0,"BCMA_FIRSTNAME",strFirstName);
		recAddress.setData(0,"BCMA_LASTNAME",strLastName);
		recAddress.setData(0,"BCMA_MIDDLENAME",strMiddleName);
		recAddress.setData(0,"BCMA_ORGANIZATIONNAME",strOrg);
		recAddress.setData(0,"BCMA_COUNTRY_CODE",strState);
		recAddress.setData(0,"BCMA_ZIP",QVariant(nZIP).toString());
		recAddress.setData(0,"BCMA_STREET",strStreet);
		recAddress.setData(0,"BCMA_CITY",strTown);
		recAddress.setData(0,"BCMA_POBOX",strPOBOX);

		recAddress.setData(0,"LST_TYPES",lstAddressTypes);


	}

}





//nMode==2 insert
void ClientContactManager::NotifyCacheAfterContactWrite(bool bIsInsert,DbRecordSet recNewData,ObsrPtrn_Observer *pSender)
{
	//FROM FUI BASE:
	//--------------------------------------------
	//fill name (calculated field):)
	DbRecordSet newCacheRecord;
	newCacheRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
	newCacheRecord.merge(recNewData);
	MainEntityCalculatedName m_CalcName;
	m_CalcName.Initialize(ENTITY_BUS_CONTACT,recNewData);
	QString strName=m_CalcName.GetCalculatedName(recNewData);
	newCacheRecord.setData(0,"BCNT_NAME",strName);
	int nContactId=recNewData.getDataRef(0,"BCNT_ID").toInt();
	//recNewData=newCacheRecord;

	//Now notify all selection controllers with filters:
	if(bIsInsert) //WHEN ADD data: send filter + data
		g_ClientCache.ModifyCache(ENTITY_BUS_CONTACT,newCacheRecord,DataHelper::OP_ADD,pSender); 
	else
		g_ClientCache.ModifyCache(ENTITY_BUS_CONTACT,newCacheRecord,DataHelper::OP_EDIT,pSender); 
	//--------------------------------------------


	//notify all other ACP patterns:
	DbRecordSet newRecord;
	QString strOrg;
	newRecord.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_SUBSET_SELECTION));
	newRecord.addRow();

	//-----------------------------
	//Org
	//-----------------------------
	strOrg=recNewData.getDataRef(0,"BCNT_ORGANIZATIONNAME").toString();
	newRecord.setData(0,0,nContactId);
	newRecord.setData(0,1,strOrg);
	
	g_ClientCache.ModifyCache(ENTITY_CONTACT_ORGANIZATION,newRecord,DataHelper::OP_EDIT,pSender); //will add if not exists

	//-----------------------------
	//Dept:
	//-----------------------------
	strOrg=recNewData.getDataRef(0,"BCNT_DEPARTMENTNAME").toString();
	newRecord.setData(0,0,nContactId);
	newRecord.setData(0,1,strOrg);
	g_ClientCache.ModifyCache(ENTITY_CONTACT_DEPARTMENT,newRecord,DataHelper::OP_EDIT,pSender); //will add if not exists


	//-----------------------------
	//Function:
	//-----------------------------
	strOrg=recNewData.getDataRef(0,"BCNT_FUNCTION").toString();
	DbRecordSet lstDataF;
	lstDataF.addColumn(QVariant::String,"NAME");
	lstDataF.addRow();
	lstDataF.setData(0,0,strOrg);
	g_ClientCache.ModifyCache(ENTITY_CONTACT_FUNCTION,lstDataF,DataHelper::OP_EDIT,pSender); //will add if not exists

	//-----------------------------
	//Profession:
	//-----------------------------
	strOrg=recNewData.getDataRef(0,"BCNT_PROFESSION").toString();
	newRecord.setData(0,0,nContactId);
	newRecord.setData(0,1,strOrg);
	g_ClientCache.ModifyCache(ENTITY_CONTACT_PROFESSION,newRecord,DataHelper::OP_EDIT,pSender); //will add if not exists


}

void ClientContactManager::Parse_TextForm(QString strText, DbRecordSet &lstContact)
{
	DbRecordSet lstContactLines;
	lstContactLines.addColumn(QVariant::String,"Line");
	lstContactLines.addColumn(QVariant::Int,"Type");			//0=Not Assigned 1=Separator 2=Phone Number 3=Email 4=URL 5=Adress Line 6=Unknown
	lstContactLines.addColumn(QVariant::String,"Item_Text");	//mail or phone, or address or url
	lstContactLines.addColumn(QVariant::String,"Item_Desc");
	lstContactLines.addColumn(QVariant::Int,"Item_SubType");    //system phone type or local use
	lstContactLines.addColumn(QVariant::Int,"Item_MaxNumber");

	DbRecordSet lstAddress;
	DbRecordSet lstEmails;
	DbRecordSet lstPhones;
	DbRecordSet lstInternet;
	lstContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	lstPhones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE));
	lstPhones.addColumn(QVariant::String,"BCMT_TYPE_NAME");
	lstEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL));
	lstEmails.addColumn(QVariant::String,"BCMT_TYPE_NAME");
	lstInternet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET));
	lstInternet.addColumn(QVariant::String,"BCMT_TYPE_NAME");


	QStringList lstLines = strText.split("\n", QString::KeepEmptyParts);
	//determine separators & max numberS:
	int nSize=lstLines.size();
	for(int i=0;i<nSize;++i)
	{
		int nPos=lstLines.at(i).indexOf(":");
		if (nPos<=0)
			continue;
		
		QString strType=lstLines.at(i).left(nPos).trimmed().toLower();
		QString strValue=lstLines.at(i).mid(nPos+1).trimmed();
		lstContactLines.addRow();
		int n=lstContactLines.getRowCount()-1;
		lstContactLines.setData(n,"Line",lstLines.at(i));
		lstContactLines.setData(n,"Type",0);								//0=Not Assigned 1=Separator 2=Phone Number 3=Email 4=URL 5=Adress Line 6=Unknown
		lstContactLines.setData(n,"Item_Text",strType);
		lstContactLines.setData(n,"Item_Desc",strValue);
		int	nMax=FindLargestInteger(lstLines.at(i));
		lstContactLines.setData(n,"Item_MaxNumber",nMax);
	}


	//Find Emails:
	//---------------------------------------------------
	//Email: mail, email, e-mail, find 'em isolate them
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("mail"));
	lstContactLines.find("Item_Text",QString("email"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("e-mail"),false,false,false,true);
	DbRecordSet lstTemp;
	lstTemp.copyDefinition(lstContactLines);
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();

	nSize=lstTemp.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		/*
		lstEmails.addRow();
		int n=lstEmails.getRowCount()-1;
		lstEmails.setData(n,"BCME_ADDRESS",lstTemp.getDataRef(i,"Item_Desc"));
		*/

		QString strEmail,strDesc;
		Parse_Mail(lstTemp.getDataRef(i,0).toString(),strEmail,strDesc);
		if (!strEmail.isEmpty())
		{

			lstEmails.addRow();
			int n=lstEmails.getRowCount()-1;
			lstEmails.setData(n,"BCME_ADDRESS",strEmail);
			lstEmails.setData(n,"BCME_DESCRIPTION",strDesc);
		}

	}


	//Find Phone Numbers:
	//---------------------------------------------------
	nSize=lstContactLines.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstContactLines.clearSelection();
		QString strPhone,strDesc;
		int nType;
		Parse_Phone(lstContactLines.getDataRef(i,0).toString(),strPhone,strDesc,nType);
		if (!strPhone.isEmpty())
		{
			lstContactLines.selectRow(i);

			lstPhones.addRow();
			int n=lstPhones.getRowCount()-1;
			lstPhones.setData(n,"BCMP_FULLNUMBER",strPhone);
			lstPhones.setData(n,"BCMP_NAME",strDesc);
			lstPhones.setData(n,"BCMP_TYPE_ID",GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,nType));
		}
	}
	lstContactLines.deleteSelectedRows();


	// Find URLs:
	//---------------------------------------------------
	//skip assigned and separator lines
	//Internet: homepage, internet, web, Web-Adresse, web-address, www, website, Webseite
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("homepage"));
	lstContactLines.find("Item_Text",QString("internet"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("web"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("web-adresse"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("web-address"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("www"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("website"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("webseite"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();

	nSize=lstTemp.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		lstInternet.addRow();
		int n=lstInternet.getRowCount()-1;
		lstInternet.setData(n,"BCMI_ADDRESS",lstTemp.getDataRef(i,"Item_Desc"));
	}



	Parse_AddressForm(lstContactLines,lstAddress);


	//filter and prepare for out:
	ContactTypeManager::AddEmptyRowToFullContactList(lstContact);
	//lstContact.addRow();
	int nContType=ContactTypeManager::CM_TYPE_PERSON;
	if (lstAddress.getRowCount()>0)
	{
		FormatAddressWithDefaultSchema(lstAddress,ContactTypeManager::CM_TYPE_PERSON,ClientContactManager::FORMAT_USING_USERDEFAULTS,false); //format address
		lstContact.setData(0,"BCNT_LASTNAME",lstAddress.getDataRef(0,"BCMA_LASTNAME"));
		lstContact.setData(0,"BCNT_FIRSTNAME",lstAddress.getDataRef(0,"BCMA_FIRSTNAME"));
		lstContact.setData(0,"BCNT_MIDDLENAME",lstAddress.getDataRef(0,"BCMA_MIDDLENAME"));
		lstContact.setData(0,"BCNT_ORGANIZATIONNAME",lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME"));
		lstContact.setData(0,"BCNT_ORGANIZATIONNAME_2",lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME_2"));
		if (!lstAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME").toString().isEmpty() && lstAddress.getDataRef(0,"BCMA_LASTNAME").toString().isEmpty())
			nContType=ContactTypeManager::CM_TYPE_ORGANIZATION;
		//issue 1306:
		lstContact.setData(0,"BCNT_LOCATION",lstAddress.getDataRef(0,"BCMA_CITY"));
	}

	lstContact.setData(0,"LST_EMAIL",lstEmails);
	lstContact.setData(0,"LST_PHONE",lstPhones);
	lstContact.setData(0,"LST_INTERNET",lstInternet);
	lstContact.setData(0,"LST_ADDRESS",lstAddress);
	lstContact.setData(0,"BCNT_TYPE",nContType);

	//refill data:
	DbRecordSet *data=g_ClientCache.GetCache(ENTITY_BUS_CM_TYPES);
	DbRecordSet recSettings,recOptions;
	g_pSettings->GetDataSource(recOptions,recSettings);
	MainEntitySelectionController SchemaHandler;
	SchemaHandler.Initialize(ENTITY_BUS_CM_ADDRESS_SCHEMAS);
	SchemaHandler.ReloadData();
	DbRecordSet *lstSchemas=SchemaHandler.GetDataSource();
	ContactTypeManager::PrepareContactListFromImport(lstContact,data,&recSettings,&recOptions,g_pClientManager->GetPersonID(),lstSchemas,true);

}


void ClientContactManager::Parse_AddressForm(DbRecordSet &lstContactLines,DbRecordSet &recAddress)
{

	/*
	First Name: Vorname, First Name
	Middle Name: Zweiter Vorname, Middle Name
	Last Name: Nachname, Last Name, Name
	Organization (Name): Organization, Company, Betrieb, Unternehmen, Firma, Organisation
	Language: Sprache, Language 
	Organization Name 2: Zusatz, Addresszusatz, Organisation 2, Firma 2, Organization 2, Company 2
	Country Code: Land, Country, Staat
	Country Name: Land, Country (check the code first, and if not found, the country name and look up the other field)

	Street: Strasse, Street
	ZIP: PLZ, Postleitzahl, ZIP, ZIP-Code
	Town: Stadt, Ort, Town, City
	Title: Titel, Anrede, Title
	PO Box: PO Box, Postfach
	State: Bundestland, Kanton, state
	*/

	recAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
	DbRecordSet lstAddressTypes;
	lstAddressTypes.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT));
	recAddress.addColumn(QVariant::String,"BCMT_TYPE_NAME");
	recAddress.addColumn(DbRecordSet::GetVariantType(),"LST_TYPES");
	recAddress.addRow();

	//first name:
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("vorname"));
	lstContactLines.find("Item_Text",QString("first name"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("ime"),false,false,false,true);
	DbRecordSet lstTemp;
	lstTemp.copyDefinition(lstContactLines);
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_FIRSTNAME",lstTemp.getDataRef(0,"Item_Desc"));
	}
	//Middle name:
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("zweiter vorname"));
	lstContactLines.find("Item_Text",QString("middle name"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_MIDDLENAME",lstTemp.getDataRef(0,"Item_Desc"));
	}
	//last name:
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("nachname"));
	lstContactLines.find("Item_Text",QString("last name"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("name"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("prezime"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_LASTNAME",lstTemp.getDataRef(0,"Item_Desc"));
	}
	//Organization:
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("organization"));
	lstContactLines.find("Item_Text",QString("company"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("betrieb"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("unternehmen"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("firma"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("organisation"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_ORGANIZATIONNAME",lstTemp.getDataRef(0,"Item_Desc"));
	}
	//Organization Name 2
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("zusatz"));
	lstContactLines.find("Item_Text",QString("addresszusatz"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("organisation 2"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("firma 2"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("organization 2"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("company 2"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_ORGANIZATIONNAME_2",lstTemp.getDataRef(0,"Item_Desc"));
	}

	//Country Code:
	//------------------------------------
	bool bCountryFound=false;
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("land"));
	lstContactLines.find("Item_Text",QString("country"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("staat"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		bCountryFound=true;
		QString strCode=lstTemp.getDataRef(0,"Item_Desc").toString();
		QString strCountry=Countries::GetCountryFromISOCode(strCode);
		if (strCountry.isEmpty())
		{
			strCountry=lstTemp.getDataRef(0,"Item_Desc").toString();
			strCode=Countries::GetISOCodeFromCountry(strCountry);
			if (!strCode.isEmpty())
			{
				recAddress.setData(0,"BCMA_COUNTRY_NAME",strCountry);
				recAddress.setData(0,"BCMA_COUNTRY_CODE",strCode);
			}
		}
		else
		{
			recAddress.setData(0,"BCMA_COUNTRY_NAME",strCountry);
			recAddress.setData(0,"BCMA_COUNTRY_CODE",strCode);
		}
	
	}

	//Language: Sprache, Language:
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("sprache"));
	lstContactLines.find("Item_Text",QString("language"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_LANGUGAGE_CODE",lstTemp.getDataRef(0,"Item_Desc"));
	}

	//Street: Strasse, Street
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("strasse"));
	lstContactLines.find("Item_Text",QString("adresse"));
	lstContactLines.find("Item_Text",QString("street"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_STREET",lstTemp.getDataRef(0,"Item_Desc"));
	}

	//ZIP: PLZ, Postleitzahl, ZIP, ZIP-Code
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("plz"));
	lstContactLines.find("Item_Text",QString("postleitzahl"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("zip"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("zip-code"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_ZIP",lstTemp.getDataRef(0,"Item_Desc"));
	}

	//Town: Stadt, Ort, Town, City
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("stadt"));
	lstContactLines.find("Item_Text",QString("ort"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("town"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("city"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_CITY",lstTemp.getDataRef(0,"Item_Desc"));
	}


	//Title: Titel, Anrede, Title
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("titel"));
	lstContactLines.find("Item_Text",QString("anrede"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("title"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_TITLE",lstTemp.getDataRef(0,"Item_Desc"));
	}

	//PO Box: PO Box, Postfach
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("po box"));
	lstContactLines.find("Item_Text",QString("postfach"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_POBOX",lstTemp.getDataRef(0,"Item_Desc"));
	}

	//State: Bundestland, Kanton, state
	//------------------------------------
	lstContactLines.clearSelection();
	lstContactLines.find("Item_Text",QString("bundestland"));
	lstContactLines.find("Item_Text",QString("kanton"),false,false,false,true);
	lstContactLines.find("Item_Text",QString("state"),false,false,false,true);
	lstTemp.clear();
	lstTemp.merge(lstContactLines,true);
	lstContactLines.deleteSelectedRows();
	if (lstTemp.getRowCount()>0)
	{
		recAddress.setData(0,"BCMA_REGION",lstTemp.getDataRef(0,"Item_Desc"));
	}


	recAddress.setData(0,"LST_TYPES",lstAddressTypes);
}

//server settings call: add favorite in settings
void ClientContactManager::AddFavorite(Status &pStatus,int nContactID)
{
	pStatus.setError(0);
	QString strFavs = g_pSettings->GetPersonSetting(CONTACT_FAVORITES).toString(); //, comma delimited
	QStringList lstFavs = strFavs.split(",",QString::SkipEmptyParts);
	if (lstFavs.indexOf(QVariant(nContactID).toString())>=0)
	{
		pStatus.setError(1,QObject::tr("Contact is already favorite!"));
		return;
	}
	if (lstFavs.size()+1>=MAX_CONTACT_FAVORITES)
	{
		pStatus.setError(1,QObject::tr("You can not have more then")+ MAX_CONTACT_FAVORITES+QObject::tr("favorites!"));
		return;
	}

	if (!strFavs.isEmpty())
		strFavs.append(",");
	strFavs.append(QVariant(nContactID).toString());
	g_pSettings->SetPersonSetting(CONTACT_FAVORITES,strFavs);

	//MB issue 1809, save at once:
	g_pSettings->SavePersonSettings(pStatus);
}
//server  settings call: removes favorite from settings
void ClientContactManager::RemoveFavorite(Status &pStatus,int nContactID)
{
	pStatus.setError(0);

	QString strFavs = g_pSettings->GetPersonSetting(CONTACT_FAVORITES).toString(); //, comma delimited
	QStringList lstFavs = strFavs.split(",",QString::SkipEmptyParts);
	int n=lstFavs.indexOf(QVariant(nContactID).toString());
	if (n<0)
	{
		pStatus.setError(1,QObject::tr("Contacts is not favorite!"));
		return;
	}
	else
	{
		lstFavs.removeAt(n);
	}
	strFavs=lstFavs.join(",");
	g_pSettings->SetPersonSetting(CONTACT_FAVORITES,strFavs);
	//MB issue 1809, save at once:
	g_pSettings->SavePersonSettings(pStatus);
}


//same as above..almost
void ClientContactManager::GetContactData(Status &pStatus,int nContactID, DbRecordSet &rowData, bool bOfflineMode, bool bReloadFromServer)
{
	pStatus.setError(0);
	rowData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

	DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENITITY_CONTACT_DETAILS_DATA);
	if (!bReloadFromServer)
	{
		if (plstCacheData)
		{
			int nRow=plstCacheData->find("BCNT_ID",nContactID,true);
			if (nRow>=0)
			{
				rowData=plstCacheData->getRow(nRow);
				return;
			}
		}
	}

	if (bOfflineMode)
		return;

	//server call:
	QString strWhere =" BCNT_ID = "+QVariant(nContactID).toString();
	MainEntityFilter filter;
	filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
	_SERVER_CALL(BusContact->ReadData(pStatus,filter.Serialize(),rowData));

	_CHK_ERR(pStatus);

	if (plstCacheData)
	{
		plstCacheData->addRow();
		plstCacheData->assignRow(plstCacheData->getRowCount()-1,rowData);
	}
	else
		g_ClientCache.SetCache(ENITITY_CONTACT_DETAILS_DATA,rowData);

}
//server call: assigns pic to the contact
void ClientContactManager::AddPictureFavorite(Status &pStatus,int nContactID)
{

	QPixmap big_picture;
	QString strFormat="jpg";
	bool bOK=PictureHelper::LoadPicture(big_picture,strFormat); //store picture format as no Qt control stores this
	if(!bOK)
	{ 
		pStatus.setError(1,QObject::tr("Failed to load picture"));
		return;
	}

	pStatus.setError(0);
	//if changed, convert small preview, save big & preview, refresh
	QByteArray bytePicture,bytePreview;
	if(big_picture.isNull())
	{
		bytePicture=QByteArray();
		bytePreview=QByteArray();
	}
	else
	{
		PictureHelper::ConvertPixMapToByteArray(bytePicture,big_picture,strFormat);
		PictureHelper::ResizePixMapToPreview(big_picture);
		PictureHelper::ConvertPixMapToByteArray(bytePreview,big_picture,strFormat);
	}

	int nBigPicID;
	_SERVER_CALL(BusContact->AssignPictureContact(pStatus,nContactID,bytePreview,bytePicture,nBigPicID));
	_CHK_ERR(pStatus);

	//save changes in the cache:
	DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENITITY_CONTACT_DETAILS_DATA);
	if (plstCacheData)
	{
		int nRow=plstCacheData->find("BCNT_ID",nContactID,true);
		if (nRow>=0)
		{
			plstCacheData->setData(nRow,"BCNT_PICTURE",bytePreview);
			plstCacheData->setData(nRow,"BCNT_BIG_PICTURE_ID",nBigPicID);
		}
	}

}

//lstFavorites=TVIEW_BUS_CONTACT_FULL
//gets data from cache: cache must be filled in offline mode..
void ClientContactManager::GetFavoriteList(Status &pStatus,DbRecordSet &lstFavorites,bool bOfflineMode,bool bReloadFromServer)
{
	pStatus.setError(0);
	lstFavorites.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

	QString strFavs = g_pSettings->GetPersonSetting(CONTACT_FAVORITES).toString(); //, comma delimited
	QStringList lstFavsToRead = strFavs.split(",",QString::SkipEmptyParts);

	DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENITITY_CONTACT_DETAILS_DATA);
	if (!bReloadFromServer)
	{
		if (plstCacheData)
		{
			
			//find all in list: not found load 
			int nSize=lstFavsToRead.size();
			int nFoundInCache=0;
			for(int i=nSize-1;i>=0;--i)
			{
				int nRow=plstCacheData->find("BCNT_ID",lstFavsToRead.at(i).toInt(),true);
				if (nRow>=0)
				{
					lstFavorites.merge(plstCacheData->getRow(nRow));
					lstFavsToRead.removeAt(i);
					nFoundInCache++;
				}
			}
			
			if(nFoundInCache==nSize) //if all in cache its ok...
			{
				return;
			}
		}
	}

	if (bOfflineMode)
	{
		return;
	}
	if (lstFavsToRead.size()==0)
		return;

	DbRecordSet lstMissingFavs;
	QString strWhere =" BCNT_ID IN ("+lstFavsToRead.join(",")+")";
	MainEntityFilter filter;
	filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
	_SERVER_CALL(BusContact->ReadData(pStatus,filter.Serialize(),lstMissingFavs));
	_CHK_ERR(pStatus);
	
	lstFavorites.merge(lstMissingFavs);
		
	//set back in the cache: merge new, or set as is
	if (plstCacheData)
		plstCacheData->merge(lstMissingFavs);
	else
		g_ClientCache.SetCache(ENITITY_CONTACT_DETAILS_DATA,lstMissingFavs);


	//check: if contact is deleted: update settings:
	DeleteOrphanFavorite(lstFavorites);

}
//change data in cache
void ClientContactManager::ChangeFavorite(Status &pStatus,DbRecordSet &rowData)
{
	pStatus.setError(0);

	DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENITITY_CONTACT_DETAILS_DATA);
	if (plstCacheData && rowData.getRowCount()==1)
	{
		int nRow=plstCacheData->find("BCNT_ID",rowData.getDataRef(0,"BCNT_ID"),true);
		if (nRow>=0)
		{
			plstCacheData->assignRow(nRow,rowData);
			return;
		}
	}
}

void ClientContactManager::Offline_SaveContactList(Status &pStatus,DbRecordSet &lstContacts)
{
	if (lstContacts.getRowCount()==0)
		return;

	//CONTACT LIST:
	g_ClientCache.SetCache(ENTITY_BUS_CONTACT,lstContacts);
	

	//CONTACT LIST DETAILS DATA:
	DbRecordSet lstContactDetails;
	ReadContactDetails(pStatus,lstContacts,lstContactDetails);
	if (!pStatus.IsOK()) return;

	DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENITITY_CONTACT_DETAILS_DATA);
	if (plstCacheData)
	{
		plstCacheData->merge(lstContactDetails);
		plstCacheData->removeDuplicates(plstCacheData->getColumnIdx("BCNT_ID"));
	}
	else
		g_ClientCache.SetCache(ENITITY_CONTACT_DETAILS_DATA,lstContactDetails);


}

//offline: saves all data from cache into file: favs.dat
//WARNING: SERVER CALL: call before logout....
//STRUCTURE: ROW: FAVORITES (TVIEW_BUS_CONTACT_FULL) // CONTACT LIST (TVIEW_BUS_CONTACT_SELECTION)  // CONTACT LIST DETAILS (TVIEW_BUS_CONTACT_FULL)
void ClientContactManager::Offline_SaveData(Status &pStatus)
{
	pStatus.setError(0);

	//FAVORITES only add on existing if not already in cache:
	DbRecordSet lstFavs;
	GetFavoriteList(pStatus,lstFavs,false,false);
	if (!pStatus.IsOK()) return;

	if (lstFavs.getRowCount()>0)
	{
		//CONTACT LIST DETAILS DATA:
		DbRecordSet lstContactDetailsFavs;
		ReadContactDetails(pStatus,lstFavs,lstContactDetailsFavs);
		if (!pStatus.IsOK()) return;

		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENITITY_CONTACT_DETAILS_DATA);
		if (plstCacheData)
		{
			plstCacheData->merge(lstContactDetailsFavs);
			plstCacheData->removeDuplicates(plstCacheData->getColumnIdx("BCNT_ID"));
		}
		else
			g_ClientCache.SetCache(ENITITY_CONTACT_DETAILS_DATA,lstContactDetailsFavs);
	}

	//get from cache:
	DbRecordSet lstContactDetails,lstContacts;
	DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENITITY_CONTACT_DETAILS_DATA);
	if (plstCacheData)
		lstContactDetails=*plstCacheData;

	lstContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
	DbRecordSet *plstCacheDataLst=g_ClientCache.GetCache(ENTITY_BUS_CONTACT);
	if (plstCacheDataLst)
		lstContacts=*plstCacheDataLst;


	//TYPES (phones, etc..)
	MainEntitySelectionController TypeHandler;
	TypeHandler.Initialize(ENTITY_BUS_CM_TYPES);
	TypeHandler.ReloadData();
	DbRecordSet lstTypes = *TypeHandler.GetDataSource();

	//USERS
	MainEntitySelectionController Users;
	Users.Initialize(ENTITY_BUS_PERSON);
	Users.ReloadData();
	DbRecordSet lstUsers = *Users.GetDataSource();

	//datOrg
	MainEntitySelectionController datOrg;
	datOrg.Initialize(ENTITY_CONTACT_ORGANIZATION);
	datOrg.ReloadData();
	DbRecordSet lstOrg = *datOrg.GetDataSource();

	//datDept
	MainEntitySelectionController datDept;
	datDept.Initialize(ENTITY_CONTACT_DEPARTMENT);
	datDept.ReloadData();
	DbRecordSet lstDept = *datDept.GetDataSource();

	//datFunct
	MainEntitySelectionController datFunct;
	datFunct.Initialize(ENTITY_CONTACT_FUNCTION);
	datFunct.ReloadData();
	DbRecordSet lstFunct = *datFunct.GetDataSource();

	//datProf
	MainEntitySelectionController datProf;
	datProf.Initialize(ENTITY_CONTACT_PROFESSION);
	datProf.ReloadData();
	DbRecordSet lstProf = *datProf.GetDataSource();

	//settings + logged user data:
	DbRecordSet recOpt,recSettings,recLoggedUserData,recLoggedContactData;
	g_pSettings->GetDataSource(recOpt,recSettings);
	g_pClientManager->GetUserData(recLoggedUserData);
	g_pClientManager->GetUserContactData(recLoggedContactData);


	//COMPILE DATA FILE:
	DbRecordSet lstDataOffline;
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_CONTACT_LIST");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_CONTACT_LIST_DETAILS");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_TYPES");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_USERS");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_ORG");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_DEPT");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_FUNCT");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_PROF");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_SETTINGS");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_OPTIONS");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_REC_LOGGED_USERDATA");
	lstDataOffline.addColumn(DbRecordSet::GetVariantType(),"LST_REC_LOGGED_CONTACTDATA");

	lstDataOffline.addRow();
	lstDataOffline.setData(0,"LST_CONTACT_LIST",lstContacts);
	lstDataOffline.setData(0,"LST_CONTACT_LIST_DETAILS",lstContactDetails);
	lstDataOffline.setData(0,"LST_TYPES",lstTypes);
	lstDataOffline.setData(0,"LST_USERS",lstUsers);
	lstDataOffline.setData(0,"LST_ORG",lstOrg);
	lstDataOffline.setData(0,"LST_DEPT",lstDept);
	lstDataOffline.setData(0,"LST_FUNCT",lstFunct);
	lstDataOffline.setData(0,"LST_PROF",lstProf);
	lstDataOffline.setData(0,"LST_SETTINGS",recSettings);
	lstDataOffline.setData(0,"LST_OPTIONS",recOpt);
	lstDataOffline.setData(0,"LST_REC_LOGGED_USERDATA",recLoggedUserData);
	lstDataOffline.setData(0,"LST_REC_LOGGED_CONTACTDATA",recLoggedContactData);

	QString strConnName=g_pClientManager->GetConnectionName();
	if (strConnName.isEmpty())
		strConnName=g_pClientManager->GetIniFile()->m_strConnectionName;
	QString strCacheFile="cache_"+strConnName+".dat";
	if (strConnName.isEmpty())
		strCacheFile="cache.dat";

	QFile fileFavs(QCoreApplication::applicationDirPath()+"/"+strCacheFile);
	if (fileFavs.exists())
	{
		if(!fileFavs.remove())
		{
			pStatus.setError(1,QString(QObject::tr("Failed to remove %1 file")).arg(QCoreApplication::applicationDirPath()+"/"+strCacheFile));
			return;
		}
	}
	if(!fileFavs.open(QIODevice::WriteOnly))
	{
		pStatus.setError(1,QString(QObject::tr("File %1  can not be written!")).arg(QCoreApplication::applicationDirPath()+"/"+strCacheFile));
		return;
	}


	QByteArray byteData=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstDataOffline);
	byteData=qCompress(byteData,2);
	int nErr=fileFavs.write(byteData);
	fileFavs.close();
	if (nErr==-1)
	{
		pStatus.setError(1,QString(QObject::tr("File %1  can not be written!")).arg(QCoreApplication::applicationDirPath()+"/"+strCacheFile));
		return;
	}

}
//offline: loads all data from file into cache
void ClientContactManager::Offline_LoadData(Status &pStatus)
{
	pStatus.setError(0);
	
	QString strConnName=g_pClientManager->GetConnectionName();
	if (strConnName.isEmpty())
		strConnName=g_pClientManager->GetIniFile()->m_strConnectionName;
	QString strCacheFile="cache_"+strConnName+".dat";
	if (strConnName.isEmpty())
		strCacheFile="cache.dat";


	QFile fileFavs(QCoreApplication::applicationDirPath()+"/"+strCacheFile); //file can be non existant
	if (!fileFavs.exists())
	{
		return;
	}
	if(!fileFavs.open(QIODevice::ReadOnly))
	{
		pStatus.setError(1,QString(QObject::tr("File: %1 can not be loaded!")).arg(QCoreApplication::applicationDirPath()+"/"+strCacheFile));
		return;
	}

	QByteArray byteData=fileFavs.readAll();
	fileFavs.close();
	byteData=qUncompress(byteData);
	DbRecordSet lstDataOffline =XmlUtil::ConvertByteArray2RecordSet_Fast(byteData);

	if (lstDataOffline.getRowCount()==1)
	{
		DbRecordSet lstContacts=lstDataOffline.getDataRef(0,"LST_CONTACT_LIST").value<DbRecordSet>();
		DbRecordSet lstContactDetails=lstDataOffline.getDataRef(0,"LST_CONTACT_LIST_DETAILS").value<DbRecordSet>();
		DbRecordSet lstTypes=lstDataOffline.getDataRef(0,"LST_TYPES").value<DbRecordSet>();
		DbRecordSet lstUsers=lstDataOffline.getDataRef(0,"LST_USERS").value<DbRecordSet>();
		DbRecordSet lstOrg=lstDataOffline.getDataRef(0,"LST_ORG").value<DbRecordSet>();
		DbRecordSet lstDept=lstDataOffline.getDataRef(0,"LST_DEPT").value<DbRecordSet>();
		DbRecordSet lstFunct=lstDataOffline.getDataRef(0,"LST_FUNCT").value<DbRecordSet>();
		DbRecordSet lstProf=lstDataOffline.getDataRef(0,"LST_PROF").value<DbRecordSet>();
		DbRecordSet lstSettings=lstDataOffline.getDataRef(0,"LST_SETTINGS").value<DbRecordSet>();
		DbRecordSet lstOptions=lstDataOffline.getDataRef(0,"LST_OPTIONS").value<DbRecordSet>();
		DbRecordSet lstUserDataLogged=lstDataOffline.getDataRef(0,"LST_REC_LOGGED_USERDATA").value<DbRecordSet>();
		DbRecordSet lstContactDataLogged=lstDataOffline.getDataRef(0,"LST_REC_LOGGED_CONTACTDATA").value<DbRecordSet>();

	
		if (lstContacts.getRowCount()>0)
			g_ClientCache.SetCache(ENTITY_BUS_CONTACT,lstContacts);
		if (lstContactDetails.getRowCount()>0)
			g_ClientCache.SetCache(ENITITY_CONTACT_DETAILS_DATA,lstContactDetails);
		if (lstTypes.getRowCount()>0)
			g_ClientCache.SetCache(ENTITY_BUS_CM_TYPES,lstTypes);
		if (lstUsers.getRowCount()>0)
			g_ClientCache.SetCache(ENTITY_BUS_PERSON,lstUsers);
		if (lstOrg.getRowCount()>0)
			g_ClientCache.SetCache(ENTITY_CONTACT_ORGANIZATION,lstOrg);
		if (lstDept.getRowCount()>0)
			g_ClientCache.SetCache(ENTITY_CONTACT_DEPARTMENT,lstDept);
		if (lstFunct.getRowCount()>0)
			g_ClientCache.SetCache(ENTITY_CONTACT_FUNCTION,lstFunct);
		if (lstProf.getRowCount()>0)
			g_ClientCache.SetCache(ENTITY_CONTACT_PROFESSION,lstProf);
		if (lstOptions.getRowCount()>0)
			g_pSettings->SetOptionsDataSource(&lstOptions);
		if (lstUserDataLogged.getRowCount()>0 && lstSettings.getRowCount()>0)
			g_pSettings->SetSettingsDataSource(&lstSettings,lstUserDataLogged.getDataRef(0,"CUSR_PERSON_ID").toInt());

		g_pClientManager->SetUserContactData(lstContactDataLogged);
		g_pClientManager->SetUserData(lstUserDataLogged);
	}
}






void ClientContactManager::ReadContactDetails(Status &pStatus,DbRecordSet &lstSourceID,DbRecordSet &lstContactDetails)
{

		lstContactDetails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
		int nChunkSize=150;
		int nSize=lstSourceID.getRowCount();
		if (nSize==0)
		{
			return;
		}

		//make filter, chunked read...
		MainEntityFilter filter;
		DbRecordSet lstChunkRead;
		Status err;
		int nCount=0;
		QString strWhereIn,strWhere;
		QSize size(300,100);
		g_pClientManager->HideCommunicationInProgress(true);
		QProgressDialog progress(QObject::tr("Reading Detailed Contact List..."),QObject::tr("Cancel"),0,nSize+10,NULL);
		progress.setWindowTitle(QObject::tr("Read In Progress"));
		progress.setMinimumSize(size);
		progress.setWindowModality(Qt::WindowModal);
		progress.setMinimumDuration(1200);//1.2s before pops up

		qApp->processEvents();
		//READ Contact data at 100records chunks:
		while (nCount>=0)
		{
			//compile FK in (n1,n2,...) statement
			nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstSourceID,"BCNT_ID",strWhereIn,nCount,nChunkSize);
			if (nCount<0) break;
			strWhere="BCNT_ID IN "+strWhereIn;
			filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
			_SERVER_CALL(BusContact->ReadData(err,filter.Serialize(),lstChunkRead))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,QObject::tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				g_pClientManager->HideCommunicationInProgress(false);
				return;
			}
			filter.ClearFilter();
			lstContactDetails.merge(lstChunkRead);
			if (nCount>=nChunkSize)//hide progress for short operations
			{
				progress.setValue(nCount);
				qApp->processEvents();
				if (progress.wasCanceled())
				{
					g_pClientManager->HideCommunicationInProgress(false);
					pStatus.setError(1,QObject::tr("User aborted operation!"));
					return;
				}
			}
		}

		//hide progress for short operations
		if (nCount>=nChunkSize)
		{
			progress.setValue(nSize+10);
			qApp->processEvents();
		}
		g_pClientManager->HideCommunicationInProgress(false);
}





//wehn cont is deleted, settings can contains orphans: check it here:
void ClientContactManager::DeleteOrphanFavorite(DbRecordSet lstAllFavs)
{
	QString strFavs = g_pSettings->GetPersonSetting(CONTACT_FAVORITES).toString(); //, comma delimited
	QStringList lstFavs = strFavs.split(",",QString::SkipEmptyParts);
	int nSize=lstFavs.size();
	for(int i=nSize-1;i>=0;--i)
	{
		int nRow=lstAllFavs.find("BCNT_ID",lstFavs.at(i).toInt(),true);
		if (nRow<0)
		{
			lstFavs.removeAt(i);
		}
	}

	strFavs=lstFavs.join(",");
	g_pSettings->SetPersonSetting(CONTACT_FAVORITES,strFavs);
}


void ClientContactManager::LoadBigPictureList(Status &Ret_pStatus,DbRecordSet &lstData,QString strBigPicIDColName)
{
	Ret_pStatus.setError(0);
	QString strWhere;
	int nSize=lstData.getRowCount();
	if (nSize==0)
		return;
	int nActualPicsToRead=0;
	for(int i=0;i<nSize;++i)
	{
		if (lstData.getDataRef(i,strBigPicIDColName).toInt()>0)
		{
			strWhere+=lstData.getDataRef(i,strBigPicIDColName).toString()+",";
			nActualPicsToRead++;
		}
	}
	if (nActualPicsToRead==0)
		return;
	strWhere.chop(1);
	strWhere=" WHERE BPIC_ID IN ("+strWhere+") ";

	Status err;
	DbRecordSet lstPics;
	_SERVER_CALL(ClientSimpleORM->Read(Ret_pStatus,BUS_BIG_PICTURE,lstPics,strWhere))
		if (Ret_pStatus.IsOK())
		{
			for(int i=0;i<nSize;++i)
			{
				int nRow=lstPics.find("BPIC_ID",lstData.getDataRef(i,strBigPicIDColName).toInt(),true);
				if (nRow!=-1)
				{
					lstData.setData(i,"BPIC_PICTURE",lstPics.getDataRef(nRow,"BPIC_PICTURE"));
				}
			}
		}
}

//returns selected groups
bool ClientContactManager::SelectGroupsForCopyContact(DbRecordSet &lstGroupsForCopy)
{
	DbRecordSet lstGroups=ContactTypeManager::GetContactGroupsForCopy();
	Dlg_CopyEntity Dlg;
	Dlg.Initialize(lstGroups);
	if(!Dlg.exec())
		return false;

	Dlg.GetSelectedGroups(lstGroupsForCopy);
	return true;
}


//false: selection aborted: lstPhones in format: TVIEW_BUS_CM_PHONE_SELECT_EXT
//lstAllowedSystemPhoneTypes if >0 then only show those numbers of that type
bool ClientContactManager::SelectPhoneNumberOfContact(DbRecordSet &lstPhones,QList<int> lstAllowedSystemPhoneTypes,QStringList &lstSelectedPhones)
{
	QStringList lstPhonesFmt;
	QStringList lstPhoneNames;
	QStringList lstPhoneTypes;

	int nSize=lstPhones.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//we need formatted number
		QString strPhone = lstPhones.getDataRef(i,"BCMP_FULLNUMBER").toString();
		FormatPhone PhoneFormatter;
		QString strCountry,strLocal,strISO,strArea,strSearch;
		PhoneFormatter.SplitPhoneNumber(strPhone,strCountry,strArea,strLocal,strSearch,strISO);
		strPhone = PhoneFormatter.FormatPhoneNumber(strCountry,strArea,strLocal);

		if (lstAllowedSystemPhoneTypes.size()>0)
			if (lstAllowedSystemPhoneTypes.indexOf(lstPhones.getDataRef(i,"BCMT_SYSTEM_TYPE").toInt())>=0)
			{
				lstPhonesFmt	<< strPhone; 
				lstPhoneNames	<< lstPhones.getDataRef(i,"BCMP_NAME").toString();
				lstPhoneTypes	<< lstPhones.getDataRef(i,"BCMT_TYPE_NAME").toString();
			}
	}


	if (lstPhonesFmt.size()==0)
	{
		return false;
	}

	/*
	//get default
	QString strDefaultPhone;
	int nDefRow=lstPhones.find(lstPhones.getColumnIdx("BCMP_IS_DEFAULT"),1,true);
	if (nDefRow!=-1)
	{
		strDefaultPhone=lstPhones.getDataRef(nDefRow,"").toString();
	}
	*/

	//open selector dialog: try to pass list
	PhoneSelectorDlg dlg(lstPhonesFmt, lstPhoneNames, lstPhoneTypes);
	/*
	//set position to simulate combo box (MB 2008.01.23)
	QPoint pt = ui.txtPhoneNumber->geometry().bottomLeft();
	pt += geometry().topLeft();
	dlg.move(pt);
	*/
	if(dlg.exec() > 0)
	{
		QString strPhone = dlg.getSelectedPhone();
		lstSelectedPhones.append(strPhone);
		return true;
	}
	else
		return false;
}