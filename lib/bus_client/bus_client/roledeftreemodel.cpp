#include "roledeftreemodel.h"
#include "common/common/status.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
#include "bus_client/bus_client/servermessagehandlerset.h"
#include "trans/trans/httpclientconnectionsettings.h"
extern BusinessServiceManager *g_pBoSet;

RoleDefTreeModel::RoleDefTreeModel(QObject *Parent /*= NULL*/): DbRecordSetModel(Parent)
{
//	m_pDbRecordSet = new DbRecordSet();
}

RoleDefTreeModel::~RoleDefTreeModel()
{
//	delete(m_pDbRecordSet);
}

/*!
Initialize model.

\param &pStatus			- status.
*/
void RoleDefTreeModel::InitializeModel(Status &pStatus)
{
	//Initialize.
	m_pDropedItem	  = NULL;
	m_sizeMaxIconSize = QSize(16, 16);	//Default icon size.
	//Set root item (override if needed).
	SetRootItem();
	
	//Clear recordset.
	m_pDbRecordSet->clear();

	//Call BO.
	_SERVER_CALL(AccessRights->GetRoles(pStatus, *m_pDbRecordSet))
	if(!pStatus.IsOK())
	{
		qDebug() << pStatus.getErrorText();
		return;
	}
	else
		qDebug() << "Roles retrieved.";

	//Setup model data.
	SetupModelData();

	//Add access rights sets.
	QHashIterator<int, DbRecordSetItem*> i(m_hshModelItems);
	while (i.hasNext()) 
	{
		i.next();
		//Clear recordset.
		m_pDbRecordSet->clear();

		//Call BO.
		_SERVER_CALL(AccessRights->GetRoleAccessRightSets(pStatus, *m_pDbRecordSet, i.key()))
		if(!pStatus.IsOK())
		{
			qDebug() << pStatus.getErrorText();
			return;
		}
		else
			qDebug() << "Access rights retrieved.";

		AddDataToModel(i.key());
	}
	m_pDbRecordSet->clear();
}

/*!
Setup model data from recordset.

\param Parent	 - Parent item.
*/
void RoleDefTreeModel::SetupModelData(DbRecordSetItem *Parent /*= NULL*/)
{
	//Parents per column hash.
	QMultiHash<int, DbRecordSetItem*> Parents;		

	//Check are we building from scratch or just some node.
	Parents.insert(0, m_pRootItem);

	int column_count = m_pDbRecordSet->getColumnCount();
	int row_count = m_pDbRecordSet->getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Set some data to display and some more.
		QList<QString> data;
		QString RoleName		= m_pDbRecordSet->getDataRef(row, 3).toString();
		QString RoleDescription = m_pDbRecordSet->getDataRef(row, 4).toString();
		QString RoleType		= m_pDbRecordSet->getDataRef(row, 5).toString();
		data << RoleName << RoleDescription << RoleType;

		//Row ID.
		int RowId = m_pDbRecordSet->getDataRef(row, 0).toInt();

		//First separate icons.
		QStringList IconList;
		IconList << QString("2");

		//Create new item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, 1, m_pRootItem);
		//Insert it in parents hash (key value is record id).
		Parents.insert(RowId, item);
		//Append item to it's parent.
		m_pRootItem->appendChild(item);
		//Finally append to model item list.
		m_hshModelItems.insert(RowId, item);
	}
	//Clear recordset.
	m_pDbRecordSet->clear();
}

/*!
Add data to model.

\param RoleID - role ID.
*/
void RoleDefTreeModel::AddDataToModel(int RoleID)
{
	//Third column is parent id and is always the same for whole recordset.
	DbRecordSetItem *ParentItem = m_hshModelItems.value(RoleID);

	int row_count = m_pDbRecordSet->getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Set some data to display.
		QList<QString> data;
		QString Name = m_pDbRecordSet->getDataRef(row, 3).toString();
		QString Descr = m_pDbRecordSet->getDataRef(row, 4).toString();
		data << Name << Descr;
		int RowId	 = m_pDbRecordSet->getDataRef(row, 0).toInt();

		//AccR type (business or system).
		int RoleType = m_pDbRecordSet->getDataRef(row, 5).toInt();
		//Add some Icon.
		QStringList IconList;
		if (RoleType == 1)
			IconList << "1";
		else
			IconList << "3";

		//Create new item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, 0, ParentItem);
		//Append item to it's parent.
		ParentItem->appendChild(item);
	}
}

QVariant RoleDefTreeModel::data(const QModelIndex &index, int role /*= Qt::DisplayRole*/) const 
{
	//If index not valid (root item), go out.
	if (!index.isValid())
		return QVariant();

	//Get item pointer for later.
	DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());

	//Icon setting.
	if (role == Qt::DecorationRole)
	{
		QString Icon_resource = item->GetIcons().value(0);
		Icon_resource.prepend(":");
		QIcon icon;
		icon.addFile(Icon_resource);
		return QVariant(icon);
	}
	//Font decoration.
	if (role == Qt::FontRole)
	{
		QFont font;
		font.setStyle(QFont::StyleItalic);
		font.setFamily("Helvetica");
		font.setBold(true);
		font.setPointSize(10);
		return QVariant(font);
	}
	//Activated or not (font color).
	/*if (role == Qt::TextColorRole && (item->data(2).toString() == "0" || item->parent()->data(2).toString() == "0"))
	{
		QColor color(Qt::gray);
		return QVariant(color);
	}*/
	//Checked or not.
	if (role == Qt::CheckStateRole)
	{
		return QVariant();
		//		return QVariant(Qt::PartiallyChecked);
		//		return QVariant(Qt::PartiallyChecked);
	}
	//Display role.
	if (role != Qt::DisplayRole)
		return QVariant();

	//Return data.
	return item->data(0);
}

Qt::ItemFlags RoleDefTreeModel::flags(const QModelIndex &index) const 
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;

	Qt::ItemFlags DefaultFlags = QAbstractItemModel::flags(index);

	if (index.parent().isValid())
		return Qt::ItemIsDragEnabled | DefaultFlags;
	else
		return Qt::ItemIsDropEnabled | DefaultFlags; //| Qt::ItemIsEditable - BEFORE IT WAS (cont.menu).
}

bool RoleDefTreeModel::setData (const QModelIndex &index, const QVariant &value, int role /*= Qt::EditRole*/)
{
	//Rename.
	if (index.isValid() && role == Qt::EditRole) 
	{
		//Get item.
		DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());
		int ItemRowID = item->GetRowId();

		//Call BO.
		Status status;
		_SERVER_CALL(AccessRights->RenameRole(status, ItemRowID, m_strRoleName, m_strRoleDescription))
		if(!status.IsOK())
		{
			qDebug() << status.getErrorText();
			return false;
		}
		else
			qDebug() << "Role renamed.";

		//Change item data.
		item->SetData(0, m_strRoleName);
		item->SetData(1, m_strRoleDescription);
		emit dataChanged(index, index);
		return true;
	}

	return false;
}

Qt::DropActions RoleDefTreeModel::supportedDropActions() const
{
	return Qt::CopyAction;
}

bool RoleDefTreeModel::insertRows(int position, int rows, const QModelIndex &parent /*= QModelIndex()*/)
{
	//Role adding
	if (!parent.isValid())
	{
		int RowID;
		Status status;

		_SERVER_CALL(AccessRights->InsertNewRole(status, RowID, m_strRoleName,  m_strRoleDescription, m_nRoleType))
		if(!status.IsOK())
		{
			qDebug() << status.getErrorText();
			return false;
		}
		else
			qDebug() << "Role inserted.";

		beginInsertRows(QModelIndex(), position, position+rows-1);
		QList<QString> data;
		data << m_strRoleName << m_strRoleDescription << QVariant(m_nRoleType).toString();
		QStringList IconList;
		IconList << QString("2");

		DbRecordSetItem *item = new DbRecordSetItem(RowID, data, IconList,0, m_pRootItem);
		m_pRootItem->appendChild(item);

		endInsertRows();
	}
	//Access rights adding
	else
	{
		Q_ASSERT(m_pDropedItem);

		DbRecordSetItem *ParentItem = static_cast<DbRecordSetItem*>(parent.internalPointer());
		DbRecordSetItem *item = new DbRecordSetItem(*m_pDropedItem);

		int RoleID			 = ParentItem->GetRowId();
		int AccRightSetRowID = item->GetRowId();

		Status status;
		_SERVER_CALL(AccessRights->InsertNewAccRSetToRole(status, RoleID, AccRightSetRowID))
		if(!status.IsOK())
		{
			qDebug() << status.getErrorText();
			return false;
		}
		else
			qDebug() << "Access right inserted.";

		beginInsertRows(parent, position, position+rows-1);

		item->SetParent(ParentItem);
		ParentItem->appendChild(item);

		endInsertRows();
	}

	return true;
}

bool RoleDefTreeModel::removeRows(int position, int rows, const QModelIndex &index /*= QModelIndex()*/)
{
	DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());
	int ItemRowID = item->GetRowId();

	//Delete Role.
	if (!index.parent().isValid())
	{
		Status status;
		_SERVER_CALL(AccessRights->DeleteRole(status, ItemRowID))
		if(!status.IsOK())
		{
			qDebug() << status.getErrorText();
			return false;
		}
		else
			qDebug() << "Role deleted.";

		beginRemoveRows(QModelIndex(), position, position+rows-1);
		m_pRootItem->DeleteChild(item);
		endRemoveRows();
	}
	//Delete access right set.
	else
	{
		DbRecordSetItem *ItemParent = static_cast<DbRecordSetItem*>(index.parent().internalPointer());
		int ItemParentRowID = ItemParent->GetRowId();

		Status status;
		_SERVER_CALL(AccessRights->DeleteAccRSetFromRole(status, ItemParentRowID, ItemRowID))
		if(!status.IsOK())
		{
			qDebug() << status.getErrorText();
			return false;
		}
		else
			qDebug() << "Access right set deleted.";

		beginRemoveRows(index.parent(), position, position+rows-1);
		ItemParent->DeleteChild(item);
		endRemoveRows();
	}

	return true;
}

bool RoleDefTreeModel::dropMimeData(const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent)
{
	insertRows(row,1, parent);
	return true;
}

QStringList RoleDefTreeModel::mimeTypes() const 
{
	QStringList types;
	types << "helix/accrightsitem";
	return types;
}


void RoleDefTreeModel::AddNewRole(QString RoleName, QString RoleDescription, int RoleType)
{
	m_strRoleName		 = RoleName;
	m_strRoleDescription = RoleDescription;
	m_nRoleType			 = RoleType;

	insertRows(1, 1);
}

void RoleDefTreeModel::SetRoleName(const QString &RoleName)
{
	m_strRoleName = RoleName;
}

void RoleDefTreeModel::SetRoleDescription(const QString &RoleDescription)
{
	m_strRoleDescription = RoleDescription;
}

void RoleDefTreeModel::SetRoleType(const int nRoleType)
{
	m_nRoleType = nRoleType;
}



