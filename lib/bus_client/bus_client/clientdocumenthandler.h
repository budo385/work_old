#ifndef CLIENTDOCUMENTHANDLER_H
#define CLIENTDOCUMENTHANDLER_H

#include <QObject>
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "bus_trans_client/bus_trans_client/userstoragehttpclient.h"


//2 modes: http user storage or as is it was before..
class ClientDocumentHandler : public QObject
{
	Q_OBJECT

public:

	enum Phases
	{
		PHASE_NO_OP,
		PHASE_CHECK_OUT_BLOB2FILE_WRITE,
		PHASE_CHECK_OUT_DOWNLOAD,
		PHASE_CHECK_IN_UPLOAD,
		PHASE_CHECK_IN_FILE2BLOB_READ
	};

	ClientDocumentHandler(QObject *parent=NULL);
	~ClientDocumentHandler();

	void CheckOutDocument(Status& pStatus, int nDocumentID,QString strFilePath,bool bReadOnly=false,bool bSkipLock=false, int nRevisionID=-1); 
	void CheckInDocument(Status& pStatus, int nDocumentID,QString strFilePath,bool bOverWrite=false,bool bSkipLock=false,QString strTag=""); 
	void GetLastDocumentInfo(DbRecordSet &recLastCheckOutInfo){recLastCheckOutInfo=RecLastCheckOutInfo;};
	void GetDocumentRevision(DbRecordSet &rec){rec=RecDocumentRevision;};
	void SetDocumentRevision(DbRecordSet &rec){RecDocumentRevision=rec;};
	void EnableHTTPUserStorageMode(bool bEnable){m_bEnableHTTPUserStorageMode=bEnable;};
	bool IsHTTPUserStorageModeEnabled(){return m_bEnableHTTPUserStorageMode;};

public slots:
	void OnExecCheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly=false,bool bSkipLock=false, int nRevisionID=-1);
	void OnExecCheckInDocument(int nDocumentID,QString strFilePath,bool bOverWrite=false,bool bSkipLock=false,QString strTag="");
	void OnCancelOperation();

protected slots:
	virtual void OnFatalErrorDetected(int){};

signals:
	void CommunicationInProgress(int,qint64,qint64);
	void CommunicationServerMessageRecieved(DbRecordSet);
	void OperationEnded(int nErrCode, QString strErrorRawText);

private:
	DbRecordSet RecLastCheckOutInfo;
	DbRecordSet RecDocumentRevision;
	int m_nPhase;
	UserStorageHTTPClient *m_Httpclient;
	bool	m_bEnableHTTPUserStorageMode;				//use doc rev. instead.
	
};


//Execute download/upload in separate client thread:
//set settings and start thread by start thread
//use functions Exec...Abort, catch signals: SocketOperationInProgress and SignalOperationEnded

class ClientDocumentHandlerThread : public QThread
{
	Q_OBJECT

public:
	ClientDocumentHandlerThread();
	~ClientDocumentHandlerThread();

	void StartThread();

	void ExecCheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly=false,bool bSkipLock=false, int nRevisionID=-1); 
	void ExecCheckInDocument(int nDocumentID,QString strFilePath,bool bOverWrite=false,bool bSkipLock=false,QString strTag=""); 
	void GetLastDocumentInfo(DbRecordSet &recLastCheckOutInfo);
	void GetDocumentRevision(DbRecordSet &rec);
	void SetDocumentRevision(DbRecordSet &rec);
	void EnableHTTPUserStorageMode(bool bEnable=true);
	bool IsHTTPUserStorageModeEnabled();
	void AbortOperation();

signals:
	//intern
	void SignalExecCheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly,bool bSkipLock, int nRevisionID); 
	void SignalExecCheckInDocument(int nDocumentID,QString strFilePath,bool bOverWrite,bool bSkipLock,QString strTag); 
	void SignalDestroy();
	void SignalAbortOperation();

	//use this:
	void SocketOperationInProgress(int,qint64,qint64);
	void SignalOperationEnded(int, QString);

protected:
	void run();

	ThreadSynchronizer m_ThreadSync;///< for syncing between mastr & slave thread
	ClientDocumentHandler* m_SlaveThread;
};

#endif // CLIENTDOCUMENTHANDLER_H
