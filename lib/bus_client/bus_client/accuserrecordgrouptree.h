#ifndef ACCUSERRECORDGROUPTREE_H
#define ACCUSERRECORDGROUPTREE_H

#include <QTreeWidget>
#include <QHash>
#include <QDropEvent>
#include <QDragMoveEvent>
#include <QDragEnterEvent>
#include "common/common/dbrecordset.h"
/*!
	\class  AccUserRecordGroupTree
	\brief  GUI widget for handling AR for contact groups
	\ingroup Bus_Client

	-header (Group/Read/Modify/Full)
	-every node has 3chx (5 columnsin total): group code/name,read, modify, full, inherit from parent
	-more then 1 root node
	-every root node is treename
	-recreate tree based on UAR data from CORE_ACC_GROUP_REC,BUS_GROUP_ITEMS and BUS_GROUP_TREE
	-add new on drop/add: group node->new GAR records (no id)
	-add childs too, default is all full, read, modify, full disabled, only inherit is set.
	-when parent is changed, all childs with flag parent are changed
	-if inherit is unchecked: all enabled, change on own.
	-result are GAR records (with or without id, with or without parent<->link)

	<record_set:
	- treename, treeid, group<all from BUS_GROUP_ITEMS>, gar<all from CORE_ACC_GROUP_REC> for given record_id/table_id
*/
class AccUserRecordGroupTree : public QTreeWidget
{
	Q_OBJECT

public:
	AccUserRecordGroupTree(QWidget *parent);
	~AccUserRecordGroupTree();
	
	void Initialize(DbRecordSet *plstData);
	void RefreshDisplay();
	void DeleteSelected();
	void EnableDrop(bool bEnable,int nDropMimeType);
	void ExpandItemByRowID(int nNodeID);

signals:
	void DataChanged();
	void UpdateUARRights();
	void SignalDataDroped(int nDropType, DbRecordSet &lstDropValue, QDropEvent *event);

private slots:
	void SelectionChanged();
	void OnItemChanged(QTreeWidgetItem *item, int column);

protected:
	virtual Qt::DropActions	supportedDropActions() const;
	virtual void			dropEvent(QDropEvent *event);
	virtual void			dragEnterEvent ( QDragEnterEvent * event );
	virtual void			dragMoveEvent ( QDragMoveEvent * event );


private:
	void CreateItemsFromRecordSet(QTreeWidgetItem *ParentItem, int nStartRow, int nEndRow);
	void SelectItemByRowID(int nNodeID);
	void SelectInRecordSetByNodeID(int nNodeID);
	void RefreshHeader();
	void ExpandTree();

	DbRecordSet						*m_pData;					//< Pointer to data source recordset.
	QHash<int, QTreeWidgetItem*>	m_hshRowIDtoItem;			//< Row ID to widget item pointer hash. Used for fast item access.
	int					m_nDragMimeType;
	int					m_nDropMimeType;
	QString				m_strDragIcon;
	
};

#endif // ACCUSERRECORDGROUPTREE_H
