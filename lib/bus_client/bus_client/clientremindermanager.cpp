#include "clientremindermanager.h"
#include <QApplication>
#include <QRect>
#include <QDesktopWidget>

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

ClientReminderManager::ClientReminderManager(QObject *parent)
	: QObject(parent)
{

	m_Dlg = NULL;
}

ClientReminderManager::~ClientReminderManager()
{
	if (m_Dlg)
	{
		m_Dlg->hide();
		m_Dlg->deleteLater();
		//m_sysTray->hide();
		//m_sysTray->deleteLater();
		m_Dlg=NULL;
	}

}


void ClientReminderManager::OnNewReminderReceived(int nMsgCode,QString strMsg)
{
	int nReminderID=strMsg.toInt();
	Status err;
	ReloadUserReminders(err,m_lstReminders);
	_CHK_ERR(err);

	OnShowDlg();

	//check err:
	//find reminder:
	//open window, reload data
	//set current reminder and ring ring...
}


void ClientReminderManager::ReloadUserReminders(Status &err,DbRecordSet &lstReminders)
{
	int nPersonID=g_pClientManager->GetPersonID();
	_SERVER_CALL(ServerControl->GetAllActiveRemindersForUser(err,nPersonID,lstReminders));
}


void ClientReminderManager::DismissReminders(Status &err, DbRecordSet &lstReminders)
{
	_SERVER_CALL(ServerControl->DeleteReminders(err,lstReminders));
	//_CHK_ERR(err);
}

void ClientReminderManager::PostPoneReminders(Status &err,DbRecordSet &lstReminders, int nMinutes)
{
	_SERVER_CALL(ServerControl->PostPoneReminders(err,lstReminders,nMinutes));
}


void ClientReminderManager::RecalcDueTimes(DbRecordSet &lstReminders)
{

	if (lstReminders.getColumnIdx("COL_DUE_TIMES")<0)
	{
		lstReminders.addColumn(QVariant::String,"COL_DUE_TIMES");
	}

	QDateTime currentDate=QDateTime::currentDateTime();
	int nSize=lstReminders.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QDateTime datDue=lstReminders.getDataRef(i,"BRE_DAT_DUE").toDateTime();
		bool bOverDue=false;
		int nMinTo=datDue.secsTo(currentDate)/60;
		if (nMinTo<0)
			{bOverDue=true;nMinTo=abs(nMinTo);}
		int nDays=nMinTo/(60*24);
		int nHrs=(nMinTo-nDays*60*24)/60;
		nMinTo=nMinTo-nDays*60*24-nHrs*60;

		QString strDisplay;
		if (nDays>0)
			strDisplay += QString::number(nDays)+tr(" days ");
		if (nHrs>0)
			strDisplay += QString::number(nHrs)+tr(" hours ");
		if (nMinTo>0)
			strDisplay += QString::number(nMinTo)+tr(" hours ");

		if (bOverDue)
			strDisplay=tr("Overdue for");
		else
			strDisplay=tr("Due in");

		lstReminders.setData(i,"COL_DUE_TIMES",strDisplay);
	}

}


void ClientReminderManager::CheckDlg()
{
	if (m_Dlg==NULL)
	{
		m_Dlg = new Dlg_Reminders;
		m_Dlg->hide();
		m_Dlg->setWindowFlags(Qt::Window); //| Qt::WindowStaysOnTopHint);
		//connect(m_Dlg,SIGNAL(ClearAll()),this,SLOT(ClearAll()));
		/*
		m_sysTray = new QSystemTrayIcon;
		m_sysTray->hide();
		QMenu *CnxtMenu = new QMenu;
		QAction* pAction = new QAction(tr("Show Current Download/Uploads Manager"), m_sysTray);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDlg()));
		CnxtMenu->addAction(pAction);
		pAction = new QAction(tr("Hide Current Download/Uploads"), m_sysTray);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnHideDlg()));
		CnxtMenu->addAction(pAction);
		m_sysTray->setContextMenu(CnxtMenu);
		m_sysTray->setIcon(QIcon(":Earth16.png"));
		connect(m_sysTray,SIGNAL(messageClicked()),this,SLOT(OnSysMessageClicked()));
		connect(m_sysTray,SIGNAL(activated ( QSystemTrayIcon::ActivationReason)),this,SLOT(OnSysMessageActivate(QSystemTrayIcon::ActivationReason)));
		*/
	}
}

void ClientReminderManager::ActivateAndPositionDlg()
{
	QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(m_Dlg));

	int nY=desktopRec.y()+desktopRec.height()-m_Dlg->height()-40;
	int nX=desktopRec.x()+desktopRec.width()-m_Dlg->width()-20;

	m_Dlg->move(nX,nY);
	m_Dlg->show();
	m_Dlg->activateWindow();

}
void ClientReminderManager::OnShowDlg()
{
	if(m_Dlg)
		ActivateAndPositionDlg();

}
void ClientReminderManager::OnHideDlg()
{
	if(m_Dlg)
		m_Dlg->hide();
}