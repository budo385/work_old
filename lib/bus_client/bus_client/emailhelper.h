#ifndef EMAILHELPER_H
#define EMAILHELPER_H

#include "common/common/dbrecordset.h"

class EmailHelper 
{
public:
	static void		GetEmailTemplates(DbRecordSet &lstData,bool bReloadFromServer=false);
	static bool		CreateEmailFromTemplate(QString bodyTemplate, QString &NewBody, int nContactID=-1, int nProjectID=-1, bool btoPercentEncoding=false, DbRecordSet *rowQCWContactData=NULL);
	static void		UnpackCIDToTemp(DbRecordSet &lstCIDInfo,DbRecordSet &rowEmail,DbRecordSet &lstAttachments);
	static void		UpdateEmailImageCID(DbRecordSet &lstCIDInfo,DbRecordSet &rowEmail,DbRecordSet &lstAttachments);
	static int		GetDefaultEmailInviteTemplateForDefaultLanguage();

	static void		EmbedHtmlPix(QString &strHtmlData, QStringList &lstResOrigFiles);
	static void		UnembedHtmlPix(QString &strHtmlData, QStringList &lstResTempFiles);
	static void		EmbedRemoteHtmlPix(QString &strHtmlData, QStringList &lstResOrigURLS, QStringList &lstResTempFiles);
	static void		ReplaceTag(QString &strBody, const QString strTag, const QString strValue, bool bHtml);

	static void		FixEmailInputLine(QString &strEmail);

private:
	
};

#endif // EMAILHELPER_H
