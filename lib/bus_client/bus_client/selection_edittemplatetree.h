#ifndef SELECTION_EDITTEMPLATETREE_H
#define SELECTION_EDITTEMPLATETREE_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_selection_edittemplatetree.h"

class Selection_EditTemplateTree : public QDialog
{
    Q_OBJECT

public:
    Selection_EditTemplateTree(QWidget *parent = 0);
    ~Selection_EditTemplateTree();

	void SetData(DbRecordSet &data, QString strCodeField, QString strNameField);

public:
	DbRecordSet m_lstData;

private:
    Ui::Selection_EditTemplateTreeClass ui;

private slots:
	void on_btnRemove_clicked();
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
};

#endif // SELECTION_EDITTEMPLATETREE_H
