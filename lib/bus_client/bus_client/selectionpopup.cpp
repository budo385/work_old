#include "bus_client/bus_client/selectionpopup.h"
//Add selectors (table & grid):
#include "bus_client/bus_client/selection_tablebased.h"
#include "bus_client/bus_client/selection_treebased.h"
#include "bus_client/bus_client/selection_contacts.h"
#include "bus_client/bus_client/selection_grouptree.h"
#include "bus_client/bus_client/selection_users.h"

//QHash<int,DbRecordSet> SelectionPopup::s_hshCachedData;

SelectionPopup::SelectionPopup(QWidget *parent)
    : QDialog(parent),m_bSelectorLoaded(false)
{
	ui.setupUi(this);
	m_pSelectorWidget=NULL;
	m_nEntityID=-1;
	this->setWindowIcon(QIcon(":SAP_Select")); //set window icon

#ifdef WINCE
	resize(150,200);
#endif

}


SelectionPopup::~SelectionPopup()
{
	if(m_pSelectorWidget!=NULL)delete m_pSelectorWidget;
}



void SelectionPopup::Initialize(int nEntityID,bool bEnableMultiSelection)
{
	//init selected data:
	m_nSelectedRecordID=-1;
	m_lstSelected=DbRecordSet();
	m_strCode="";
	m_strName="";
	m_nEntityID=nEntityID;



	//determine selector type by trick: instante MainEntitySelectionController and get datatype
	m_nSelectorType=MainEntitySelectionController::GetDataType(nEntityID);

	//create selector widget:
	Q_ASSERT(m_pSelectorWidget==NULL); //must be NULL or we are doing something wrong!

	switch(m_nSelectorType)
	{
	case MainEntitySelectionController::TYPE_TABLE:
		m_pSelectorWidget = new Selection_TableBased;
		dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->Initialize(nEntityID,true,false,false,bEnableMultiSelection); //enable double click!
		dynamic_cast<Selection_TableBased*>(m_pSelectorWidget)->registerObserver(this);
		break;
	case MainEntitySelectionController::TYPE_TREE:
		m_pSelectorWidget = new Selection_TreeBased;
		dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->Initialize(nEntityID,true,false,false,bEnableMultiSelection); //enable double click!
		dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->registerObserver(this);
		break;
	case MainEntitySelectionController::TYPE_CONTACTS:
		m_pSelectorWidget = new Selection_Contacts;
		dynamic_cast<Selection_Contacts*>(m_pSelectorWidget)->Initialize(true,false,false,bEnableMultiSelection); //enable double click!
		dynamic_cast<Selection_Contacts*>(m_pSelectorWidget)->registerObserver(this);
	    break;
	case MainEntitySelectionController::TYPE_USERS:
		m_pSelectorWidget = new Selection_Users;
		dynamic_cast<Selection_Users*>(m_pSelectorWidget)->Initialize(nEntityID,true,false,false,bEnableMultiSelection); //enable double click!
		dynamic_cast<Selection_Users*>(m_pSelectorWidget)->registerObserver(this);
		break;
	case MainEntitySelectionController::TYPE_GROUP_ITEMS:
		m_pSelectorWidget = new Selection_GroupTree;
		dynamic_cast<Selection_GroupTree*>(m_pSelectorWidget)->Initialize(ENTITY_BUS_CONTACT,true,false,false,bEnableMultiSelection); //enable double click!
		dynamic_cast<Selection_GroupTree*>(m_pSelectorWidget)->registerObserver(this);
		break;

	default:
		Q_ASSERT(false);
	    break;
	}



	//plop widget on dialog
	ui.stackedWidget->addWidget(m_pSelectorWidget);
	ui.stackedWidget->setCurrentWidget(m_pSelectorWidget);

}

// open previously cached selector with Init (could be faster)
int SelectionPopup::OpenSelector()
{


	//MainEntityFilter FilterData

	//init selected data:
	m_nSelectedRecordID=-1;
	m_lstSelected=DbRecordSet();
	m_strCode="";
	m_strName="";


	//create selector widget:
	Q_ASSERT(m_pSelectorWidget!=NULL); //must valid! call Initialize before

	MainEntitySelectionController *selector=dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget);
	Q_ASSERT(selector!=NULL); //must valid! 

	/*
	if (s_hshCachedData.contains(m_nEntityID))
	{
		selector->m_lstData=s_hshCachedData.value(m_nEntityID);
		selector->RefreshDisplay();
		m_bSelectorLoaded=true;

	}
	else
	{
		if (!m_bSelectorLoaded || m_nEntityID==ENTITY_BUS_CONTACT)
		{
			selector->ReloadData();		//for contacts, always cache lookup
			m_bSelectorLoaded=true;
		}
	}
	*/

	selector->ReloadData();		//for contacts, always cache lookup


	//issue 2304: always focus on Find
	if (m_nEntityID==ENTITY_BUS_CONTACT)
	{
		dynamic_cast<Selection_Contacts*>(m_pSelectorWidget)->SetFocusOnFind();
	}
	if (m_nSelectorType==MainEntitySelectionController::TYPE_TREE)
	{
		dynamic_cast<Selection_TreeBased*>(m_pSelectorWidget)->SetFocusOnFind();
	}

	//issue 2398
	if (m_nEntityID==ENTITY_BUS_DM_APPLICATIONS)
	{
		selector->m_lstData.sort("BDMA_NAME");
		selector->RefreshDisplay();
	}


	//_STOP_TIMER(reload);


	//open it (blocking modal):
	int nRes=this->exec();

	//return result:
	return nRes;

}



/*!
	Get selected data (only one row is allowed)

	\param nSelectedRecordID	- record is selected
	\param rowSelected			- entire row of data (cotnains code, name or other for display)
*/
void SelectionPopup::GetSelectedData(int &nSelectedRecordID, DbRecordSet &lstSelected)
{
	nSelectedRecordID=m_nSelectedRecordID;
	lstSelected=m_lstSelected;
}




//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void SelectionPopup::on_btnOK_clicked()
{
	Q_ASSERT(m_pSelectorWidget!=NULL); //must valid

	//get selection
	//qDebug()<<dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->GetDataSource()->getSelectedCount();
	dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->GetSelection(m_nSelectedRecordID, m_strCode,m_strName,m_lstSelected);
	//MainEntityFilter* x=dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->GetLocalFilter();
	/*
	if (x->IsEmpty()) //issue 2279: do not store when filter
	{
		s_hshCachedData[m_nEntityID]=dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->m_lstData;
	}
	*/

	if(m_lstSelected.getRowCount() > 0)
		done(1);
	else
		done(0);
	//QMessageBox::information(this, tr("Warning"), tr("Please select an item!"));
}

void SelectionPopup::on_btnCancel_clicked()
{
	done(0);
}



void SelectionPopup::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (nMsgCode==SelectorSignals::SELECTOR_ON_DOUBLE_CLICK)
	{
		dynamic_cast<MainEntitySelectionController*>(m_pSelectorWidget)->GetSelection(m_nSelectedRecordID, m_strCode,m_strName,m_lstSelected);
		if(m_lstSelected.getRowCount() > 0)
			done(1);
		else
			done(0);
	}
}

void SelectionPopup::ClearCache()
{
	//s_hshCachedData.clear();

}