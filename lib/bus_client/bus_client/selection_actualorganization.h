#ifndef SELECTION_ACTUALORGANIZATION_H
#define SELECTION_ACTUALORGANIZATION_H

#include "bus_client/bus_client/selection_sapne.h"
#include "common/common/dbrecordset.h"


/*!
	\class  Selection_ActualOrganization
	\brief  Assigment for Actual org
	\ingroup GUICore

	General Info:
	- use in FUI, call Initialize()
	- override update observer FUIBase, register FUI to this, catch selection change
	- set filter to all selection controllers in FUI (left menu & assigments) then reload from server, when ACO changed
	- in Validate method of BoEntity client just call getCurrentACO from global user default object
	
*/
class Selection_ActualOrganization : public Selection_SAPNE
{
		
public:
    Selection_ActualOrganization(QWidget *parent);
    ~Selection_ActualOrganization();

	void Initialize(DbRecordSet *pData=NULL,QString strIDCol="");
	void SelectionChanged(int nRecordID,DbRecordSet &record); //inherit and implement if needed
	int GetCurrentActualOrganizationID();
	QString GetCurrentActualOrganizationName();
	void SaveActualOrganizationID();


private:
   	DbRecordSet *m_plstData;				//pointer to FUI data where Org ID will be stored (must call  SaveActualOrganizationID)
	QString m_strActualOrgID_ColName;					//name of column inside m_pData where Org ID is stored
};

#endif // SELECTION_ACTUALORGANIZATION_H
