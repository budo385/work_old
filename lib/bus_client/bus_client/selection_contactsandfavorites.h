#ifndef SELECTION_CONTACTSANDFAVORITES_H
#define SELECTION_CONTACTSANDFAVORITES_H

#include <QtWidgets/QFrame>
#include <QStackedWidget>
#include "bus_client/bus_client/selection_contacts.h"
#include "gui_core/gui_core/pictureflowwidget.h"
#include "generatedfiles/ui_selection_contactsandfavorites.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"

class Selection_ContactsAndFavorites : public QFrame , public ObsrPtrn_Observer , public ObsrPtrn_Subject
{
	Q_OBJECT

public:
	Selection_ContactsAndFavorites(QWidget *parent = 0);
	~Selection_ContactsAndFavorites();

	Selection_Contacts *	GetSelectionContacts(){return m_pFrameContacts;};
	void					ReloadData(int nDefaultFavID=-1, bool bReloadFromServer=false);
	void					SetSideBarVisible(bool bVisible);
	bool					IsSideBarVisible();
	int						GetCurrentFavoriteID();
	void					updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	QString					GetCalculatedName(int nEntityRecordID); 
	bool					IsContactFrameVisible();
	void					SetCurrentTab(int index){m_pTab->setCurrentIndex(index);};
	int						GetCurrentTab(){return m_pTab->currentIndex();};


signals:
	void FavoriteChanged(int nContactID);

private slots:
	void OnCurrentChanged(int);
	void OncenterIndexChanged(int index);
	void OncenterIndexDoubleClicked(int index);
	void OncenterIndexSelectedForEditing(int index);
	void OnAddFav();
	void OnRemoveFav();
	void OnReload();
	void OnAddPicToFav();
	void OnAddItemButton_clicked();



private:
	Ui::Selection_ContactsAndFavoritesClass ui;
	QStackedWidget			*m_pStackedWidget;
	Selection_Contacts		*m_pFrameContacts;
	QFrame					*m_pFramePicFlow;
	PictureFlowWidget		*m_pPictureFlow;
	DbRecordSet				m_recContactData;
	int						m_nCurrentCenterIndex;
	QTabBar					*m_pTab;
	MainEntityCalculatedName m_CalcName;
	QAction		*m_pAddFavorite;
	QAction		*m_pRemoveFavorite;
	QAction		*m_pReload;
	QAction		*m_pAddPicToFav;


};

#endif // SELECTION_CONTACTSANDFAVORITES_H
