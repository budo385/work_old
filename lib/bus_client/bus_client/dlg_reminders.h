#ifndef DLG_REMINDERS_H
#define DLG_REMINDERS_H

#include <QtWidgets/QWidget>
#include "generatedfiles/ui_dlg_reminders.h"
#include "common/common/dbrecordset.h"

class Dlg_Reminders : public QWidget
{
	Q_OBJECT

public:
	Dlg_Reminders(QWidget *parent = 0);
	~Dlg_Reminders();

	void NewReminderReceived(int nReminderID);

private:
	Ui::Dlg_RemindersClass ui;
	DbRecordSet m_lstReminders;
};

#endif // DLG_REMINDERS_H
