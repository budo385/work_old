#ifndef EMAILSELECTORDIALOG_H
#define EMAILSELECTORDIALOG_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_emailselectordialog.h"

#include "common/common/dbrecordset.h"

class EmailSelectorDialog : public QDialog
{
	Q_OBJECT

public:
	EmailSelectorDialog(int nContactID, QStringList lstPreselectedMails=QStringList(), QWidget *parent = 0);
	~EmailSelectorDialog();

	QList<QString> getSelectedEmail();

private:
	Ui::EmailSelectorDialogClass ui;

private slots:
	void on_Ok_pushButton_clicked();
	void on_Cancel_pushButton_clicked();
	void OnDblClick(QTreeWidgetItem *, int);
	bool GetSingleContacMails(int nContactID, DbRecordSet &recMailRecordSet);
};

#endif // EMAILSELECTORDIALOG_H
