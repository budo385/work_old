#ifndef CUSTOMSELECTIONSWND_H
#define CUSTOMSELECTIONSWND_H

#include <QWidget>
#include "ui_customselectionswnd.h"
#include "common/common/dbrecordset.h"
#include "db_core/db_core/dbtableiddefinition.h"

#define LOGIC_AND_IDX			0
#define LOGIC_OR_IDX			1
#define LOGIC_LEFTBRACKET_IDX	2
#define LOGIC_RIGHTBRACKET_IDX	3
#define LOGIC_NOTAND_LB_IDX		4
#define LOGIC_OR_LB_IDX			5
#define LOGIC_NOT_LB_IDX		6


class CustomSelectionsWnd : public QWidget
{
	Q_OBJECT

public:
	CustomSelectionsWnd(QWidget *parent = 0);
	~CustomSelectionsWnd();

	void Initialize(int nTableID=BUS_PROJECT);
	void LoadSelectionXML(QString strXML);
	QString GetSelectionXML();
	void ClearPage(){ on_btnCustSelectionClear_clicked(); };
	bool IsValid();


private:
	Ui::CustomSelectionsWnd ui;
	DbRecordSet m_lstSelectionData;
	int m_nTableID;
	QString EncodeXmlValue(QString strValue);
	QString DecodeXmlValue(QString strValue);
	QString getLogicDisplay(int nLogic);
	int getLogicIndex(QString strLogic);
	static QString getOperationFromIdx(int nOp);
	void AddLine(int nLogic, int nField, int nOperation, QString strValue);
	void DisableFieldsOnBracketOperators(bool bDisable);

private slots:
	void on_btnCustSelectionInsertLine_clicked();
	void on_btnCustSelectionClear_clicked();	
	void on_btnCustSelectionMoveUp_clicked();
	void on_btnCustSelectionMoveDown_clicked();
	void on_btnCustSelectionRemoveLine_clicked();
	void rowSelection(int,int);
	void on_cboCustSelectionField_currentIndexChanged(int);

	void on_radioLeftBracket_toggled(bool checked);
	void on_radioRightBracket_toggled(bool checked);
	void on_radioNOTAND_lb_toggled(bool checked);
	void on_radioOR_lb_toggled(bool checked);
	void on_radioNOT_lb_toggled(bool checked);
	void on_radioAND_toggled(bool checked);
	void on_radioOR_toggled(bool checked);

	void OnPersonChanged(int);
};

#endif // CUSTOMSELECTIONSWND_H
