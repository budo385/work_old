#include "versionparser.h"
#include "trans/trans/httpreadersync.h"
#include <QDomDocument>

VersionParser::VersionParser(QString strVersionURL)
:m_bContentLoaded(false)
{
	m_strVersionURL=strVersionURL;
}


bool VersionParser::LoadContent()
{
	if (m_bContentLoaded)return true;
	HttpReaderSync WebReader(6000);  //6sec..
	if(!WebReader.ReadWebContent(m_strVersionURL+"version.xml", &m_byteVersionData))
		return false;
	m_bContentLoaded=true;
	return true;
}


//read all version.xml...search installation by date
QString VersionParser::GetSetupExePathBasedOnDate(QDate datValidPeriod)
{
	if(!LoadContent())
		return "";

	int nStartPos=0;
	QDate dateTest;

	//parse XML:
	QDomDocument domDocument;
	if (!domDocument.setContent(m_byteVersionData)) 
		return "";
	QDomNodeList ListOfElements	= domDocument.elementsByTagName("SOKRATES_Downloads");
	QDomNode Node = ListOfElements.at(0).firstChild();
	while(!Node.isNull()) 
	{
		QDomElement Element = Node.toElement();
		QString name = Element.tagName();
		if (name=="CommunicatorClient")
		{
			dateTest=QDate::fromString(Element.attribute("Date"),"dd.MM.yyyy");
			if (dateTest<datValidPeriod)
			{
				return Element.attribute("DownloadURL");
			}
		}
		Node = Node.nextSibling();
	}

	return "";
}



//read all version.xml...search installation by date
QString VersionParser::GetSetupExePathBasedOnVersion(QString strVersion)
{
	if(!LoadContent())
		return "";

	int nStartPos=0;
	QDate dateTest;
	QString strVer="";

	//parse XML:
	QDomDocument domDocument;
	if (!domDocument.setContent(m_byteVersionData)) 
		return "";
	QDomNodeList ListOfElements	= domDocument.elementsByTagName("SOKRATES_Downloads");
	QDomNode Node = ListOfElements.at(0).firstChild();
	while(!Node.isNull()) 
	{
		QDomElement Element = Node.toElement();
		QString name = Element.tagName();
		if (name=="CommunicatorClient")
		{
			if (Element.attribute("VersionID")==strVersion)
			{
				return Element.attribute("DownloadURL");
			}
		}
		Node = Node.nextSibling();
	}

	return "";
}

QString VersionParser::GetSetupExePath()
{
	if(!LoadContent())
		return "";

	QString strAppServerVersion(m_byteVersionData);

	int nPos1=m_byteVersionData.indexOf("DownloadURL");
	nPos1=m_byteVersionData.indexOf("\"",nPos1);
	int nPos2=m_byteVersionData.indexOf("\"",nPos1+1);

	if (nPos1<=0 || nPos2<=0)
		return "";

	QString strUrl=m_byteVersionData.mid(nPos1+1,nPos2-nPos1-1);
	return strUrl;
}

QString VersionParser::GetVersionID_String()
{

	if(!LoadContent())
		return "";

	QString strAppServerVersion(m_byteVersionData);

	int nPos1=m_byteVersionData.indexOf("VersionID");
	nPos1=m_byteVersionData.indexOf("\"",nPos1);
	int nPos2=m_byteVersionData.indexOf("\"",nPos1+1);

	if (nPos1<=0 || nPos2<=0)
		return "";

	QString strUrl=m_byteVersionData.mid(nPos1+1,nPos2-nPos1-1);
	return strUrl;
}


double VersionParser::GetVersionID_Double()
{

	if(!LoadContent())
		return -1;

	QString strAppServerVersion(m_byteVersionData);

	int nPos1=m_byteVersionData.indexOf("VersionID");
	nPos1=m_byteVersionData.indexOf("\"",nPos1);
	int nPos2=m_byteVersionData.indexOf("\"",nPos1+1);

	if (nPos1<=0 || nPos2<=0)
		return -1;

	QString strUrl=m_byteVersionData.mid(nPos1+1,nPos2-nPos1-1);

	return Version2Double(strUrl);
}


void VersionParser::GetAllVersionData(QString &versionid, QString &path,double &version)
{
	version=-1;
	path="";
	versionid="";

	if(!LoadContent())
		return;

	QString strAppServerVersion(m_byteVersionData);


	//path
	{
		int nPos1=m_byteVersionData.indexOf("DownloadURL");
		nPos1=m_byteVersionData.indexOf("\"",nPos1);
		int nPos2=m_byteVersionData.indexOf("\"",nPos1+1);

		if (nPos1<=0 || nPos2<=0)
		{
			path="";
		}
		else
		{
			path=m_byteVersionData.mid(nPos1+1,nPos2-nPos1-1);
		}

	}


	//versionid
	{

		int nPos1=m_byteVersionData.indexOf("VersionID");
		nPos1=m_byteVersionData.indexOf("\"",nPos1);
		int nPos2=m_byteVersionData.indexOf("\"",nPos1+1);

		if (nPos1<=0 || nPos2<=0)
		{
			versionid="";
		}
		else
		{
			versionid=m_byteVersionData.mid(nPos1+1,nPos2-nPos1-1);
			version=Version2Double(versionid);
		}
	}

}



double VersionParser::Version2Double(QString strVer)
{
	bool bOK=true;
	double fVer=0.0;
	if (strVer.indexOf(".")>=0) //new format: V2008-189.000.000
	{
		strVer=strVer.replace("-","");
		strVer=strVer.mid(1);
		strVer=strVer.replace(".","").trimmed();
		fVer=strVer.toDouble(&bOK);
	}
	else //old format: V2008-111
	{
		strVer=strVer.replace("-","");
		strVer=strVer.mid(1);
		fVer=strVer.toDouble(&bOK);
	}
	Q_ASSERT(bOK);
	return fVer;
}