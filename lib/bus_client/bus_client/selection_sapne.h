#ifndef SELECTION_SAPNE_H
#define SELECTION_SAPNE_H

#include <QtWidgets/QFrame>
#ifndef WINCE
	#include "generatedfiles/ui_selection_sapne.h"
#else
	#include "embedded_core/embedded_core/generatedfiles/ui_selection_sapne.h"
#endif
#include "bus_client/bus_client/selectionpopup.h"
#include "common/common/observer_ptrn.h"
#include "gui_core/gui_core/guidatamanipulator.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "bus_client/bus_client/selection_contactbutton.h"


/*!
	\class  Selection_SAPNE
	\brief  1N assignment pattern
	\ingroup GUICore

*/
class Selection_SAPNE : public QFrame,public ObsrPtrn_Subject, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Selection_SAPNE(QWidget *parent = 0);
	virtual ~Selection_SAPNE();

	enum Button
	{
		BUTTON_MODIFY,			
		BUTTON_ADD,	
		BUTTON_REMOVE,	
		BUTTON_SELECT,
		BUTTON_CONTACT_PIE,
		BUTTON_VIEW
	};

	//init
	void Initialize(int nEntityTypeID);														  //simple
	void Initialize(int nEntityTypeID,GuiDataManipulator *pDataManipulator,QString strIDCol); //autofill from external data
	void Initialize(int nEntityTypeID,DbRecordSet *pDataSource,QString strIDCol);			  //autofill from external data
	//main functions:
	void SetCurrentEntityRecord(int nEntityRecordID,bool bSkipNotifyObservers=false);			
	bool GetCurrentEntityRecord(int &nEntityRecordID,DbRecordSet &record);			//record as defined in selection controller (SELECT view for project, contacts,...)
	bool GetCurrentEntityRecord(int &nEntityRecordID);								//overloaded for convenience   
	QString GetCurrentDisplayName();
	void Clear(bool bSkipNotifyObservers=false);
	//GUI functions:
	void SetEditMode(bool bEditMode);
	void SetSelectionButtonLabel(QString strSelButtonLabel);
	QWidget* GetButton(int nButton);
	void EnableInsertButton();
	//only if external data provided
	void RefreshDisplay();	//data->gui
	void SetCurrentRow(unsigned int nRow){m_nCurrentRow=nRow;}; //set current row inside external data
	void SetActualOrganization(int nActualOrganizationID){m_nActualOrganizationID=nActualOrganizationID;}
	MainEntitySelectionController* GetSelectionController(){return m_dlgCachedPopUpSelector;}
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void setDisabled ( bool );	//override
	void setEnabled ( bool );	//override
	bool blockSignals(bool b){blockObserverSignals(b);return QFrame::blockSignals(b);}
	int OpenSelector();

signals:
	void CurrentEntityRecordChanged(int);

private:
	Ui::Selection_SAPNEClass ui;
protected:
	//current selection data is stored inside:
	QString m_strCurrentDisplay;
	DbRecordSet m_RowCurrentSelectedEntity;		//< row in selection format from selection controller
	void SetEntityData(int nSelectedRecordID,DbRecordSet &rowSelected,bool bSkipNotifyObservers=false);
	virtual void CreatePieButton(){};

	//external data  (optional)
	GuiDataManipulator *m_pExternDataManipulator;		//< data source manager
	DbRecordSet *m_pExternData;
	int m_nIDColIdx;									//< id col in extern data
	int	m_nCurrentRow;									//< row inside extern data source in which to change data
	int m_nEntityTypeID;
	bool m_bAutoAssigment;
	bool m_bRefreshFromExternalDataInProgress;
	SelectionPopup m_dlgPopUp;
	MainEntitySelectionController *m_dlgCachedPopUpSelector;		//cached selector controller, 2nd time when open it will not go on server for data...
	QString m_strHtmlTag;
	QString m_strEndHtmlTag;
	int m_nActualOrganizationID;
	Selection_ContactButton * m_btnContactPie;

protected slots:
	virtual void on_btnModify_clicked();
	virtual void on_btnRemove_clicked();
	virtual void on_btnSelect_clicked();
	virtual void on_btnInsert_clicked();
	virtual void on_btnView_clicked();

};

#endif // SELECTION_SAPNE_H
