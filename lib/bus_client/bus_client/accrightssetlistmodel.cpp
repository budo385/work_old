#include "accrightssetlistmodel.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
#include "bus_client/bus_client/servermessagehandlerset.h"
#include "trans/trans/httpclientconnectionsettings.h"
extern BusinessServiceManager *g_pBoSet;

AccRightsSetListModel::AccRightsSetListModel(QObject *parent)
	: DbRecordSetListModel(parent)
{
	m_pDbRecordSet = new DbRecordSet();
}

AccRightsSetListModel::~AccRightsSetListModel()
{
	delete(m_pDbRecordSet);
}

/*!
Initialize model.

\param &pStatus			- status.
*/
void AccRightsSetListModel::InitializeModel(Status &pStatus)
{
	//Initialize.
	m_sizeMaxIconSize = QSize(16, 16);	//Default icon size.
	//Set root item (override if needed).
	//SetRootItem();

	//Clear recordset.
	m_pDbRecordSet->clear();

	//Call BO.
	_SERVER_CALL(AccessRights->GetAccessRightsSets(pStatus, *m_pDbRecordSet))
	if(!pStatus.IsOK())
	{
		qDebug() << pStatus.getErrorText();
		return;
	}
	else
		qDebug() << "AccessRightsSets retrieved.";

	//Setup model data.
	SetupModelData();

	m_pDbRecordSet->clear();
}

/*!
Setup model data from recordset.

\param Parent	 - Parent item.
*/
void AccRightsSetListModel::SetupModelData(DbRecordSetItem *Parent /*= NULL*/)
{
	int row_count = m_pDbRecordSet->getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Set some data to display.
		QList<QString> data;
		QString Name = m_pDbRecordSet->getDataRef(row, 3).toString();
		QString Descr = m_pDbRecordSet->getDataRef(row, 4).toString();
		data << Name << Descr;
		int RowId	 = m_pDbRecordSet->getDataRef(row, 0).toInt();

		//AccR type (business or system).
		int RoleType = m_pDbRecordSet->getDataRef(row, 5).toInt();
		//Add some Icon.
		QStringList IconList;
		if (RoleType == 1)
			IconList << "1";
		else
			IconList << "3";

		//Create new item. In list item don't have children and Parent is always root item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, 0);
		m_hshModelItems.insert(row, item);
	}
}

Qt::ItemFlags AccRightsSetListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;

	Qt::ItemFlags DefaultFlags = QAbstractItemModel::flags(index);
	return Qt::ItemIsDragEnabled | DefaultFlags;
}

Qt::DropActions AccRightsSetListModel::supportedDropActions() const
{
	return Qt::MoveAction;
}

QStringList AccRightsSetListModel::mimeTypes() const
{
	QStringList types;
	types << "helix/accrightsitem";
	return types;
}

