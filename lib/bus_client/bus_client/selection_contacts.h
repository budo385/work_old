#ifndef SELECTION_CONTACTS_H
#define SELECTION_CONTACTS_H

#include <QtWidgets/QFrame>
#include "common/common/status.h"
#ifdef WINCE
#include "embedded_core/embedded_core/generatedfiles/ui_selection_contacts.h"
#else
#include "generatedfiles/ui_selection_contacts.h"
#endif
#include "bus_client/bus_client/mainentityselectioncontroller.h"


/*!
	\class  Selection_Contacts
	\brief  Selection frame for selecting Main entities , list/table based
	\ingroup GUICore

	Special implementation of selection controller for contacts

*/
class Selection_Contacts : public QFrame, public MainEntitySelectionController
{
	Q_OBJECT


public:
	Selection_Contacts(QWidget *parent = 0);
	~Selection_Contacts();

	void Initialize(bool bSkipLoadingData=false,bool bIsFUISelectorWidget=true,bool bSingleClickIsSelection=true,bool bMultiSelection=false,bool bEnableDrag=true); 
	void GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord);
	void GetSelection(DbRecordSet& lstRecords);
	void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nPrimaryKeyValue=-1);

	Selection_GroupTree* GetGroupSelector(){return ui.frameGroups;};
	void SelectGroupContacts(int nGroupID);
	void DisableGroupSelector();
	UniversalTableWidgetEx *GetContactTableWidget(){return ui.tableWidget;};
	bool blockSignals(bool b){blockObserverSignals(b); return QFrame::blockSignals(b);}
	bool IsSideBarVisible();
	void SetSideBarVisible(bool bVisible=true);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void SetFocusOnFind();

public slots: //cntx menu actions are now availabel from public
	void OnShowDetails();
	void OnShowDetailsNewWindow();
	void SlotSelectionChanged();
	void SlotSelectionChanged_DoubleClicked();
	void SlotGroupSelection_DoubleClicked();
	void SlotCellClicked(int,int);
	void OnDeleteSelection();
	void OnDeleteTable();
	void OnRemoveDuplicates();
	void OnDelete(); 
	void OnAddFavorite();
	void OnRemoveFavorite();
	void OnCopyPerson2Org();
	void OnFindDuplicates();
	void OnOfflineModeChanged();
	
private slots:
	void on_btnBuild_clicked();
	void on_btnOfflineSave_clicked();
	void OnCreateUser();
	void OnTableContentChanged();
	void OnFindWidgetEnterPressed();
	//void on_btnEmbededShowDetails_clicked();
	//void on_btnEmbededShowGrid_clicked();

private:
	void CreateContextMenu();
	void OnThemeChanged();
	void BuildList(DbRecordSet lstFilterData,int nOperation,bool bExcludeGroup=true);
	void FilterUserContacts();

	Ui::Selection_ContactsClass ui;
	bool m_bIsFUISelectorWidget;
	bool m_bSingleClickIsSelection;
	QAction *m_pAct_FindDuplicates;
	QAction *m_pAct_CreateOrganizationFromPerson;
	QAction *m_pAct_Delete;
	QAction *m_pAct_CreateUserFromContact;
	MainEntitySelectionController	mPersonCache;

};
#endif // SELECTION_CONTACTS_H
