#include "qvideosplash.h"
#include <Phonon/MediaObject>
#include <Phonon/MediaSource>
#include <QDesktopWidget>

QVideoSplash::QVideoSplash(QString &strVideoPath, QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	//center
	QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
	int w = desktopRec.width();                   
	int h = desktopRec.height();     
	int mw = width();                   
	int mh = height();     
	int cw = (w/2) - (mw/2);	
	int ch = (h/2) - (mh/2);
	move(cw,ch);

	Phonon::MediaObject *mediaObject = new Phonon::MediaObject(this);
    Phonon::createPath(mediaObject, ui.wndVideo);

	Phonon::MediaSource ms(strVideoPath);
	mediaObject->enqueue(ms);
	mediaObject->play();
}

QVideoSplash::~QVideoSplash()
{

}
