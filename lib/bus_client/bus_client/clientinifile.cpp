#include "clientinifile.h"
#include "common/common/logger.h"

ClientIniFile::ClientIniFile()
{
	m_lstOutlookSync.addColumn(QVariant::String,	"Sync_ConnectionName");
	m_lstOutlookSync.addColumn(QVariant::Bool,		"Sync_Enabled");
	m_lstOutlookSync.addColumn(QVariant::Int,		"Sync_TimerMin");
	m_lstOutlookSync.addColumn(QVariant::String,	"Sync_LastScan");
	m_lstOutlookSync.addColumn(QVariant::Bool,		"Sync_OnlyKnownEmails");
	m_lstOutlookSync.addColumn(QVariant::Bool,		"Sync_OnlyOutgoingEmails");
	m_lstOutlookSync.addColumn(QVariant::String,	"Sync_ScanFolders");
	m_lstOutlookSync.addColumn(QVariant::String,	"Sync_AllowedProfiles");
	m_lstOutlookSync.addColumn(QVariant::String,	"Sync_LastScanFileOffsets");	//used only for Thunderbird, one offset per folder
	m_lstThunderbirdSync.copyDefinition(m_lstOutlookSync);
}

ClientIniFile::~ClientIniFile()
{
}

void ClientIniFile::Clear()
{
	//m_lstConnections.clear();
}

bool ClientIniFile::Load(QString strFile, bool bCreateIfNotExists)
{
	Clear();
	if (strFile.isEmpty())
		strFile=m_strFile;
	else
		m_strFile=strFile;
	if(!QFileInfo(strFile).exists()&& bCreateIfNotExists)
	{return SaveDefaults(strFile);}

	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	// read INI values
	m_objIni.GetValue("Info", "ConnectionName",		m_strConnectionName);
	m_objIni.GetValue("Info", "IsThinClient",		m_nIsThinClient, 1);
	m_objIni.GetValue("Info", "LastUserName",		m_strLastUserName);
	m_objIni.GetValue("Info", "MLIID",				m_strMLIID);
	m_objIni.GetValue("Info", "Module",				m_strModuleCode);
	m_objIni.GetValue("Info", "KeyFile Path",		m_strKeyFilePath);
	m_objIni.GetValue("Info", "Hide Login Dialog",	m_nHideLogin);
	m_objIni.GetValue("Info", "Order Form URL",		m_strOrderURL);
	m_objIni.GetValue("Info", "Order Ask Form",		m_nOrderAsk,0);
	m_objIni.GetValue("Info", "LanguageCode",		m_strLangCode);
	m_objIni.GetValue("Info", "Start Import Wizard",m_nStartImportWizard);
	m_objIni.GetValue("Info", "Portable Version",	m_nPortableVersion);
	m_objIni.GetValue("Info", "Portable_Skype_Path", m_strPortableSkypePath);
	m_objIni.GetValue("Info", "Portable_Thunderbird_Profile", m_strPortableThunderbirdProfile);
	m_objIni.GetValue("Info", "AskForSkypeInstallation", m_bAskPortableSkypeInstallation, 1);
	m_objIni.GetValue("Info", "Mobile Offline StartUp", m_nMobileGoOfflineOnStartup, 0);
	m_objIni.GetValue("Info", "AskForPortableAppplicationsInstallation", m_bAskForPortableAppsInstallation, 1);
	m_objIni.GetValue("Info", "Update Setup Path", m_strUpdateSetupPath);
		
	m_objIni.GetValue("Appearance", "LastTheme",			m_nLastTheme,0);
	m_objIni.GetValue("Appearance", "LastView",				m_nLastView,0);
	m_objIni.GetValue("Appearance", "LastX",				m_nLastX,0);
	m_objIni.GetValue("Appearance", "LastY",				m_nLastY,0);
	m_objIni.GetValue("Logging", "Log Level",												m_nLogLevel,Logger::LOG_LEVEL_NONE);
	m_objIni.GetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize,10);
	m_objIni.GetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize,64);
	m_objIni.GetValue("Logging", "Use File Log",		m_nLogTargetFile,1);
	m_objIni.GetValue("Logging", "Use Console Log",		m_nLogTargetConsole,1);
	m_objIni.GetValue("Backup", "Backup Frequency",		m_nBackupFreq,2);
	m_objIni.GetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.GetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.GetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.GetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.GetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
	m_objIni.GetValue("Backup", "Delete old Backup Files Older Than",m_nDeleteOldBackupsDay,0);
	
	

	int nCount = 0;
	m_objIni.GetValue("Outlook_Synchronization", "Item_Count", nCount);
	m_lstOutlookSync.clear();
	for(int i=0; i<nCount; i++)
	{
		m_lstOutlookSync.addRow();
	
		QString strTitle;
		bool	bOutlookAutoSyncON;
		int		nOutlookSyncTimerMin;
		QString strOutlookSyncLastScan;
		bool	bOutlookSyncOnlyKnownEmails;
		bool	bOutlookSyncOnlyOutgoingEmails;
		QString strOutlookSyncScanFolders;
		QString strOutlookSyncAllowedProfiles;
		
		m_objIni.GetValue("Outlook_Synchronization", QString("Auto_Sync_Title_%1").arg(i+1), strTitle);
		m_objIni.GetValue("Outlook_Synchronization", QString("Auto_Sync_ON_%1").arg(i+1), bOutlookAutoSyncON);
		m_objIni.GetValue("Outlook_Synchronization", QString("Auto_Sync_Frequence_Minutes_%1").arg(i+1), nOutlookSyncTimerMin);
		m_objIni.GetValue("Outlook_Synchronization", QString("Auto_Sync_Last_Scanned_%1").arg(i+1), strOutlookSyncLastScan);
		m_objIni.GetValue("Outlook_Synchronization", QString("Auto_Sync_Only_Known_Emails_%1").arg(i+1), bOutlookSyncOnlyKnownEmails);
		m_objIni.GetValue("Outlook_Synchronization", QString("Auto_Sync_Only_Outgoing_Emails_%1").arg(i+1), bOutlookSyncOnlyOutgoingEmails);
		m_objIni.GetValue("Outlook_Synchronization", QString("Auto_Sync_Scan_Folders_%1").arg(i+1), strOutlookSyncScanFolders);
		m_objIni.GetValue("Outlook_Synchronization", QString("Auto_Sync_Allowed_Profiles_%1").arg(i+1), strOutlookSyncAllowedProfiles);

		m_lstOutlookSync.setData(i, "Sync_ConnectionName", strTitle);
		m_lstOutlookSync.setData(i, "Sync_Enabled", bOutlookAutoSyncON);
		m_lstOutlookSync.setData(i, "Sync_TimerMin", nOutlookSyncTimerMin);
		m_lstOutlookSync.setData(i, "Sync_LastScan", strOutlookSyncLastScan);
		m_lstOutlookSync.setData(i, "Sync_OnlyKnownEmails", bOutlookSyncOnlyKnownEmails);
		m_lstOutlookSync.setData(i, "Sync_OnlyOutgoingEmails", bOutlookSyncOnlyOutgoingEmails);
		m_lstOutlookSync.setData(i, "Sync_ScanFolders", strOutlookSyncScanFolders);
		m_lstOutlookSync.setData(i, "Sync_AllowedProfiles", strOutlookSyncAllowedProfiles);
	}
	//remove all lines with empty title
	m_lstOutlookSync.find("Sync_ConnectionName", QString()); 
	m_lstOutlookSync.deleteSelectedRows();

	nCount = 0;
	m_objIni.GetValue("Thunderbird_Synchronization", "Item_Count", nCount);
	m_lstThunderbirdSync.clear();
	for(int i=0; i<nCount; i++)
	{
		QString strTitle;
		bool	bThunderbirdAutoSyncON;
		int		nThunderbirdSyncTimerMin;
		QString strThunderbirdSyncLastScan;
		bool	bThunderbirdSyncOnlyKnownEmails;
		bool	bThunderbirdSyncOnlyOutgoingEmails;
		QString strThunderbirdSyncScanFolders;
		QString strThunderbirdSyncLastScanOffsets;

		m_objIni.GetValue("Thunderbird_Synchronization", QString("Auto_Sync_Title_%1").arg(i+1), strTitle);
		m_objIni.GetValue("Thunderbird_Synchronization", QString("Auto_Sync_ON_%1").arg(i+1), bThunderbirdAutoSyncON);
		m_objIni.GetValue("Thunderbird_Synchronization", QString("Auto_Sync_Frequence_Minutes_%1").arg(i+1), nThunderbirdSyncTimerMin);
		m_objIni.GetValue("Thunderbird_Synchronization", QString("Auto_Sync_Last_Scanned_%1").arg(i+1), strThunderbirdSyncLastScan);
		m_objIni.GetValue("Thunderbird_Synchronization", QString("Auto_Sync_Only_Known_Emails_%1").arg(i+1), bThunderbirdSyncOnlyKnownEmails);
		m_objIni.GetValue("Thunderbird_Synchronization", QString("Auto_Sync_Only_Outgoing_Emails_%1").arg(i+1), bThunderbirdSyncOnlyOutgoingEmails);
		m_objIni.GetValue("Thunderbird_Synchronization", QString("Auto_Sync_Scan_Folders_%1").arg(i+1), strThunderbirdSyncScanFolders);
		m_objIni.GetValue("Thunderbird_Synchronization", QString("Auto_Sync_Scan_Offsets_%1").arg(i+1), strThunderbirdSyncLastScanOffsets);

		m_lstThunderbirdSync.addRow();
		m_lstThunderbirdSync.setData(i, "Sync_ConnectionName", strTitle);
		m_lstThunderbirdSync.setData(i, "Sync_Enabled", bThunderbirdAutoSyncON);
		m_lstThunderbirdSync.setData(i, "Sync_TimerMin", nThunderbirdSyncTimerMin);
		m_lstThunderbirdSync.setData(i, "Sync_LastScan", strThunderbirdSyncLastScan);
		m_lstThunderbirdSync.setData(i, "Sync_OnlyKnownEmails", bThunderbirdSyncOnlyKnownEmails);
		m_lstThunderbirdSync.setData(i, "Sync_OnlyOutgoingEmails", bThunderbirdSyncOnlyOutgoingEmails);
		m_lstThunderbirdSync.setData(i, "Sync_ScanFolders", strThunderbirdSyncScanFolders);
		m_lstThunderbirdSync.setData(i, "Sync_LastScanFileOffsets", strThunderbirdSyncLastScanOffsets);
	}
	//remove all lines with empty title
	m_lstThunderbirdSync.find("Sync_ConnectionName", QString()); 
	m_lstThunderbirdSync.deleteSelectedRows();

	return true;
}

bool ClientIniFile::Save(QString strFile)
{
	if (strFile.isEmpty())
		strFile=m_strFile;
	else
		m_strFile=strFile;
	m_objIni.SetPath(strFile);
	

	//fill the data inside
	m_objIni.RemoveSection("Info");	//cleanup existing data

	// write INI values
	m_objIni.SetValue("Info", "ConnectionName",	m_strConnectionName);
	m_objIni.SetValue("Info", "IsThinClient",		m_nIsThinClient);
	m_objIni.SetValue("Info", "LastUserName",		m_strLastUserName);
	m_objIni.SetValue("Info", "MLIID",				m_strMLIID);
	m_objIni.SetValue("Info", "Module",				m_strModuleCode);
	m_objIni.SetValue("Info", "KeyFile Path",		m_strKeyFilePath);
	m_objIni.SetValue("Info", "Hide Login Dialog",	m_nHideLogin);
	m_objIni.SetValue("Info", "Order Form URL",				m_strOrderURL);
	m_objIni.SetValue("Info", "Order Ask Form",				m_nOrderAsk);
	m_objIni.SetValue("Info", "LanguageCode",				m_strLangCode);
	m_objIni.SetValue("Info", "Start Import Wizard",		m_nStartImportWizard);
	m_objIni.SetValue("Info", "Portable Version",			m_nPortableVersion);
	m_objIni.SetValue("Info", "Portable_Skype_Path", m_strPortableSkypePath);
	m_objIni.SetValue("Info", "Portable_Thunderbird_Profile", m_strPortableThunderbirdProfile);
	m_objIni.SetValue("Info", "AskForSkypeInstallation", m_bAskPortableSkypeInstallation);
	m_objIni.SetValue("Info", "Mobile Offline StartUp", m_nMobileGoOfflineOnStartup);
	m_objIni.SetValue("Info", "AskForPortableAppplicationsInstallation", m_bAskForPortableAppsInstallation);
	m_objIni.SetValue("Info", "Update Setup Path", m_strUpdateSetupPath);

	m_objIni.SetValue("Appearance", "LastTheme",			m_nLastTheme);
	m_objIni.SetValue("Appearance", "LastView",				m_nLastView);
	m_objIni.SetValue("Appearance", "LastX",				m_nLastX);
	m_objIni.SetValue("Appearance", "LastY",				m_nLastY);
	m_objIni.SetValue("Logging", "Log Level",												m_nLogLevel);
	m_objIni.SetValue("Logging", "Log Max Size (Mb) (0 for unlimited)",						m_nLogMaxSize);
	m_objIni.SetValue("Logging", "Log Memory Cache Size (Kb) (0 for instant file write)",	m_nLogBufferSize);
	m_objIni.SetValue("Logging", "Use File Log",		m_nLogTargetFile);
	m_objIni.SetValue("Logging", "Use Console Log",		m_nLogTargetConsole);
	m_objIni.SetValue("Backup", "Backup Frequency",		m_nBackupFreq);
	m_objIni.SetValue("Backup", "Backup Directory",		m_strBackupPath);
	m_objIni.SetValue("Backup", "Backup Last Date",		m_datBackupLastDate);
	m_objIni.SetValue("Backup", "Backup Day",			m_nBackupDay);
	m_objIni.SetValue("Backup", "Backup Time",			m_strBackupTime);
	m_objIni.SetValue("Backup", "Restore On Next Start",m_strRestoreOnNextStart);
	m_objIni.SetValue("Backup", "Delete old Backup Files Older Than",m_nDeleteOldBackupsDay);

	m_objIni.RemoveSection("Outlook_Synchronization");

	int nCount = m_lstOutlookSync.getRowCount();
	m_objIni.SetValue("Outlook_Synchronization", "Item_Count", nCount);
	for(int i=0; i<nCount; i++)
	{
		m_objIni.SetValue("Outlook_Synchronization", QString("Auto_Sync_Title_%1").arg(i+1),				m_lstOutlookSync.getDataRef(i, "Sync_ConnectionName").toString());
		m_objIni.SetValue("Outlook_Synchronization", QString("Auto_Sync_ON_%1").arg(i+1),					m_lstOutlookSync.getDataRef(i, "Sync_Enabled").toInt());
		m_objIni.SetValue("Outlook_Synchronization", QString("Auto_Sync_Frequence_Minutes_%1").arg(i+1),	m_lstOutlookSync.getDataRef(i, "Sync_TimerMin").toInt());
		m_objIni.SetValue("Outlook_Synchronization", QString("Auto_Sync_Last_Scanned_%1").arg(i+1),			m_lstOutlookSync.getDataRef(i, "Sync_LastScan").toString());
		m_objIni.SetValue("Outlook_Synchronization", QString("Auto_Sync_Only_Known_Emails_%1").arg(i+1),	m_lstOutlookSync.getDataRef(i, "Sync_OnlyKnownEmails").toInt());
		m_objIni.SetValue("Outlook_Synchronization", QString("Auto_Sync_Only_Outgoing_Emails_%1").arg(i+1),	m_lstOutlookSync.getDataRef(i, "Sync_OnlyOutgoingEmails").toInt());
		m_objIni.SetValue("Outlook_Synchronization", QString("Auto_Sync_Scan_Folders_%1").arg(i+1),			m_lstOutlookSync.getDataRef(i, "Sync_ScanFolders").toString());
		m_objIni.SetValue("Outlook_Synchronization", QString("Auto_Sync_Allowed_Profiles_%1").arg(i+1),		m_lstOutlookSync.getDataRef(i, "Sync_AllowedProfiles").toString());
	}

	m_objIni.RemoveSection("Thunderbird_Synchronization");

	nCount = m_lstThunderbirdSync.getRowCount();
	m_objIni.SetValue("Thunderbird_Synchronization", "Item_Count", nCount);
	for(int i=0; i<nCount; i++)
	{
		m_objIni.SetValue("Thunderbird_Synchronization", QString("Auto_Sync_Title_%1").arg(i+1),				m_lstThunderbirdSync.getDataRef(i, "Sync_ConnectionName").toString());
		m_objIni.SetValue("Thunderbird_Synchronization", QString("Auto_Sync_ON_%1").arg(i+1),					m_lstThunderbirdSync.getDataRef(i, "Sync_Enabled").toInt());
		m_objIni.SetValue("Thunderbird_Synchronization", QString("Auto_Sync_Frequence_Minutes_%1").arg(i+1),	m_lstThunderbirdSync.getDataRef(i, "Sync_TimerMin").toInt());
		m_objIni.SetValue("Thunderbird_Synchronization", QString("Auto_Sync_Last_Scanned_%1").arg(i+1),			m_lstThunderbirdSync.getDataRef(i, "Sync_LastScan").toString());
		m_objIni.SetValue("Thunderbird_Synchronization", QString("Auto_Sync_Only_Known_Emails_%1").arg(i+1),	m_lstThunderbirdSync.getDataRef(i, "Sync_OnlyKnownEmails").toInt());
		m_objIni.SetValue("Thunderbird_Synchronization", QString("Auto_Sync_Only_Outgoing_Emails_%1").arg(i+1),	m_lstThunderbirdSync.getDataRef(i, "Sync_OnlyOutgoingEmails").toInt());
		m_objIni.SetValue("Thunderbird_Synchronization", QString("Auto_Sync_Scan_Folders_%1").arg(i+1),			m_lstThunderbirdSync.getDataRef(i, "Sync_ScanFolders").toString());
		m_objIni.SetValue("Thunderbird_Synchronization", QString("Auto_Sync_Scan_Offsets_%1").arg(i+1),			m_lstThunderbirdSync.getDataRef(i, "Sync_LastScanFileOffsets").toString());
	}
	return m_objIni.Save();
}

bool ClientIniFile::SaveDefaults(QString strFile)
{
	m_strConnectionName="";
	m_strLastUserName="";
	m_nLogLevel=Logger::LOG_LEVEL_NONE;
	m_nLogMaxSize=10;				
	m_nLogBufferSize=64;	
	m_nLogTargetFile=1;
	m_nLogTargetConsole=1;
	m_nBackupFreq=2;
	m_nBackupDay=0;
	m_nIsThinClient=1;
	m_nHideLogin=0;
	m_nOrderAsk=0;
    m_strLangCode ="";
	m_nLastTheme=0;
	m_nLastView=0;
	m_nLastX=0;
	m_nLastY=0;
	m_nStartImportWizard=0;
	m_nPortableVersion=0;
	m_bAskPortableSkypeInstallation=false;
	m_strPortableSkypePath="";
	m_strPortableThunderbirdProfile="";
	m_nMobileGoOfflineOnStartup=0;
	m_bAskForPortableAppsInstallation=1;
	m_nDeleteOldBackupsDay=0;
	m_strUpdateSetupPath=""; //if empty then as usual, else use for update seek location

	return Save(strFile);
}
