#ifndef CONTACTBUILDALL_SEARCHFIELDS_H
#define CONTACTBUILDALL_SEARCHFIELDS_H

#include <QtWidgets/QFrame>
#include <QDateEdit>
#include <QTextEdit>
#include "common/common/dbrecordset.h"

#include "ui_contactbuildall_searchfields.h"

class contactbuildall_searchfields : public QFrame
{
	Q_OBJECT

public:
	contactbuildall_searchfields(QWidget *parent = 0);
	~contactbuildall_searchfields();

	void GetSearchData(QString &strColName1, int &nDataTypeCustomFld1, QString &strCol1Search1, QString &strCol1Search2, QString &strColName2, int &nDataTypeCustomFld2, QString &strCol2Search1, QString &strCol2Search2);

	DbRecordSet m_lstCustomFlds; //needed for later..

private slots:
	void currentIndexChanged_column1(int index);
	void currentIndexChanged_column2(int index);
	void dateEdit1Col1_dateChanged(const QDate	&date);
	void dateEdit1Col2_dateChanged(const QDate	&date);
	void txtSearch1Col1_textChanged(const QString& text);
	void txtSearch1Col2_textChanged(const QString& text);
	void txtEdited();

signals:
	void  contentChanged();

private:
	Ui::contactbuildall_searchfields ui;

	void SetFilterFldBasedOnData(QString strColumnName,QStackedWidget *pStacked);

};

#endif // CONTACTBUILDALL_SEARCHFIELDS_H
