#ifndef CLIENTMANAGEREXT_H
#define CLIENTMANAGEREXT_H

#include <QObject>
#include <QString>
#include <QtWidgets/QDialog>
#include <QtWidgets/QProgressDialog>
#include "clientinifile.h"
#include "common/common/status.h"
#include "versionparser.h"
#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/logindialogabstract.h"
#include "gui_core/gui_core/macros.h"
#include <QtWidgets/QSplashScreen>
#include <QProcess>
//#include "qvideosplash.h"

typedef void (*FN_CREATE_THICK_CLIENT)(Status &err);

/*!
	\class ClientManagerExt
	\brief Global client side object/cache for user default data & connection
	\ingroup Bus_Client

	Use:
	1. InitializeService();
	2. Login/Logout for login/logout dlg-> must have global objects
	3.


*/
class ClientManagerExt : public QObject
{
	Q_OBJECT

public:
	ClientManagerExt(QObject *parent);
	~ClientManagerExt();

	void			Initialize(Status &pStatus, bool bEnableBcpManager=false,bool bEnableImportExportManager=false,FN_CREATE_THICK_CLIENT pfCallBack_CreateThick=NULL);
	bool			IsLogged(){return m_bIsLogged;};
	bool			IsStartUpWizard(){return (bool)m_nStartUpWizardEnabled;};
	bool			IsThinClient(){return m_bIsThinClient;};
	void			HideCommunicationInProgress(bool bHide=true);
	void			SetLastFatalError(Status pStatus){m_LastFatalError=pStatus;};
	Status			GetLastFatalError(){return m_LastFatalError;};
	void			ClearLastFatalError(){m_LastFatalError.setError(0);};
	void*			GetBackupManager();
	void*			GetImportExportManager();
	bool			CheckForOrderForm();
	ClientIniFile*	GetIniFile(){return &m_INIFile;};
	bool			SaveIniFile(){return m_INIFile.Save();};
	void 			StopAfterFatalError(Status &pStatus, bool bPreserveCacheForOfflineMode=false);
	int				CheckForUpdatesThickClient();
	bool			IsClientIPMatchHost();
	void			ShowTransferProgressDlg(QString strLabel);
	void			HideTransferProgressDlg();
	void			SetApplicationDisplayName(QString strName){m_strApplicationDisplayName=strName;};
	QString			GetApplicationDisplayName(){return m_strApplicationDisplayName;};

	int			GetUserID();
	int			GetPersonID();
	int			GetPersonContactID();
	int			GetPersonProjectListID(); //a) or b) or c)  issue #1940, returns 0 if no list defined
	QString		GetPersonName();
	QString		GetPersonCode();
	QString		GetPersonInitials();
	bool		IsSystemUser();
	QString		GetModuleCode(){return m_strModuleCode;};
	void		GetUserData(DbRecordSet &recData){recData=m_recUserData;};								//TVIEW_CORE_USER_SELECTION, if person is null, person data is null,only login acc data live
	void		GetUserContactData(DbRecordSet &recContactData){recContactData=m_recContactData;};		//TVIEW_BUS_CM_DEAFULTS, test if contactid!=0, if 0 then row is empty
	void		SetUserData(DbRecordSet recData){m_recUserData=recData;};								//TVIEW_CORE_USER_SELECTION, if person is null, person data is null,only login acc data live
	void		SetUserContactData(DbRecordSet recContactData){m_recContactData=recContactData;};		//TVIEW_BUS_CM_DEAFULTS, test if contactid!=0, if 0 then row is empty
	void		SetConnectionName(QString strName);
	QString		GetConnectionName();
	void		SetLoggedUserName(QString strName);
	QString		GetLoggedUserName(){return m_strLoggedUserName;}
	void		SetConnectionIPAddress(QString strIP);
	QString		GetConnectionIPAddress();
	void		ReloadAccessRights();
	void		StartThreadConnection(Status& pStatus, QObject *pSignalReceiver=NULL);
	void		CloseThreadConnection(Status& pStatus);
	void		StopProgressDialog();
	void		StartProgressDialog(QString strMsg);
	void		CheckServerMessagesAfterLogin();
	QString		CheckSettingsDirectory(Status &pStatus);

public slots:
	void		Login();
	void		Logout();

signals:
	void		LoginDialogCreated(QDialog *pDlg);
	void		LoginDialogBeforeDestroy(QDialog *pDlg);
	void		ClientLogged();
	void		ClientBeforeLogout();
	void		ClientLogout();
	void		ClientClosedLoginWnd();
	void		CommunicationInProgress(int,qint64,qint64);
	void		CommunicationFatalError(int);
	void		CommunicationServerMessageRecieved(DbRecordSet);
	void		ServerMessageReceived(int nCode, QString strText);

private slots:
	void OnServerMessageRecieved(DbRecordSet rowMsg);
	void OnCommunicationFatalError(int val);
	void OnCommunicationFatalErrorDelayNotification(); 
	void OnCommunicationInProgress(int,qint64,qint64);
	void OnCommunicationInProgress_Ext(int done, int total);
	void CloseAllModalWindows();
	void OnLoginClick();
	void OnLoginCloseClick();
	void OnLoginDialogDestroyed();
	void ShowLoginDialog();

private:
	void		BusinessLogin(Status &pStatus,QString pStrUserName,QString pStrPassword);
	void		BusinessLogout(Status &pStatus);
	void		CloseLoginDalog(QDialog *pDlg);
	void		AskForUpdate(QString strServerActualVersion);
	int			CheckForDemoPeriod(Status &pStatus,QByteArray byteDemoKey);
	bool		CheckSubscriptionPeriod();
	
	void		InitializeGlobalObjects(Status &pStatus); 
	void		ClearGlobalObjects(Status &pStatus, bool bFatalErrorDetected=false, bool bPreserveCacheForOfflineMode=false);
	void		ProcessAfterStart_BusinessService();
	void		ProcessAfterStop_BusinessService(bool bFatalErrorDetected=false);
	

	bool					m_bIsThinClient;
	QString					m_strModuleCode;
	QString					m_strMLIID;
	bool					m_bIsLogged;
	ClientIniFile			m_INIFile;
	int						m_nGUICloseRetries;
	QString					m_strUpdateCheckURL;
	Status					m_LastFatalError;
	int						m_nStartUpWizardEnabled;
	VersionParser			m_VersionParser;
	LoginDialogAbstract*	m_pLoginDlg;
	int						m_nCurrentLoginTry;
	int						m_nModeLogin;
	QSplashScreen*			m_pSplashLogin;
	//QVideoSplash*			m_pSplashLogin;
	QProcess				m_procSplashApp;
	bool					m_bLoginDlgShown;
	QProgressDialog			*m_prgProgressDialogTransfer;
	bool					m_bShowProgressDlg;
	QString					m_strApplicationDisplayName;
	int						m_nClientTimeZoneOffsetMinutes;

	//logged user data:
	DbRecordSet m_recUserData;				//TVIEW_CORE_USER_SELECTION
	DbRecordSet m_recContactData;			//TVIEW_BUS_CM_DEAFULTS
	QString		m_strConnectionName;
	QString		m_strLoggedUserName;
	QString		m_strConnIPAddress;
	DbRecordSet m_recSrvMsg;
	
};

#endif // CLIENTMANAGEREXT_H
