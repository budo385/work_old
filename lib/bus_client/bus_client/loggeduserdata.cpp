#include "loggeduserdata.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/optionsandsettingsorganizer.h"


#include "bus_client/bus_client/clientmanager.h"
extern ClientManager *g_BoSet;

#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache; //global cache.

#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager g_Settings;

LoggedUserData::LoggedUserData()
{

}

LoggedUserData::~LoggedUserData()
{
}


//load all data needed for user session
void LoggedUserData::ReadUserData(Status &pStatus)
{
	//Options and settings.
	DbRecordSet recSettings, recOptions;
	//Get user session data.
	DbRecordSet lstMessages;
	_SERVER_CALL(CoreServices->ReadUserSessionData(pStatus,m_nIsAliveInterval,m_recUserData,m_recARSet,m_recContactData,recSettings,recOptions,m_recSrvMsg))

	g_Settings.SetSettingsDataSource(&recSettings,GetPersonID());
	g_Settings.SetOptionsDataSource(&recOptions);

}




int LoggedUserData::GetUserID()
{
	if (m_recUserData.getRowCount()>0)
		return m_recUserData.getDataRef(0,"CUSR_ID").toInt();
	else
		return -1;
}

int LoggedUserData::GetPersonID()
{
	return m_recUserData.getDataRef(0,"BPER_ID").toInt();

}

QString LoggedUserData::GetPersonName()
{
	//if (m_recUserData.getRowCount()>0)
		return m_recUserData.getDataRef(0,"BPER_LAST_NAME").toString()+", "+m_recUserData.getDataRef(0,"BPER_FIRST_NAME").toString();
	//else
	//	return "";

}

QString LoggedUserData::GetPersonCode()
{
	return m_recUserData.getDataRef(0,"BPER_CODE").toString();
}

QString LoggedUserData::GetPersonInitials()
{
	QString strInitials = m_recUserData.getDataRef(0,"BPER_INITIALS").toString();

	if (strInitials.isEmpty())
		strInitials = m_recUserData.getDataRef(0,"BPER_LAST_NAME").toString() + m_recUserData.getDataRef(0,"BPER_FIRST_NAME").toString();

	return strInitials;
}

void LoggedUserData::SetConnectionName(QString strName)
{
	m_strConnectionName=strName;
}


QString LoggedUserData::GetConnectionName()
{
	return m_strConnectionName;
}

void LoggedUserData::SetConnectionIPAddress(QString strIP)
{
	m_strConnIPAddress=strIP;
}

QString LoggedUserData::GetConnectionIPAddress()
{
	return m_strConnIPAddress;
}


//returns zero if invalid
int LoggedUserData::GetPersonContactID()
{
	//if (m_recContactData.getRowCount()==0 || m_recContactData.getColumnCount()==0)
	//	return 0;
	//m_recUserData.Dump();
	return m_recUserData.getDataRef(0,"BPER_CONTACT_ID").toInt();
}

/*
//Communication filter views getting method.
QHash<int, QString>	LoggedUserData::GetCommFilterViews()
{
	QHash<int, QString> hshView;
	
	//If there is no cache return empty hash.
	if (!g_ClientCache.GetCache(ENTITY_COMM_GRID_VIEWS))
		return hshView; //empty

	//Get comm. views cache and or if not exits create it.
	DbRecordSet *tmp = g_ClientCache.GetCache(ENTITY_COMM_GRID_VIEWS);
	int nRowCount = tmp->getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nViewID = tmp->getDataRef(i, "BUSCV_ID").toInt();
		QString strViewName = tmp->getDataRef(i, "BUSCV_NAME").toString();
		hshView.insert(nViewID, strViewName);
	}

	return hshView;
}
*/
