#include "selectionpopupfloat.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "bus_client/bus_client/selection_tablebased.h"
#include "bus_client/bus_client/selection_treebased.h"
#include "bus_client/bus_client/selection_contacts.h"
#include "bus_client/bus_client/selection_grouptree.h"
#include "common/common/entity_id_collection.h"

#include <QWidget>


//open as always top window, multi select, drag enabled!
void SelectionPopupFloat::openPopUpSelector(int nEntityID)
{
	bool bEnableMultiSelection=true;
	QWidget *pSelectorWidget;

	//determine selector type by trick: instante MainEntitySelectionController and get datatype
	int nSelectorType=MainEntitySelectionController::GetDataType(nEntityID);

	//create selector widget:
	//Q_ASSERT(pSelectorWidget==NULL); //must be NULL or we are doing something wrong!

	switch(nSelectorType)
	{
	case MainEntitySelectionController::TYPE_TABLE:
		pSelectorWidget = new Selection_TableBased;
		dynamic_cast<Selection_TableBased*>(pSelectorWidget)->Initialize(nEntityID,false,false,false,bEnableMultiSelection); //enable double click!
		break;
	case MainEntitySelectionController::TYPE_TREE:
		pSelectorWidget = new Selection_TreeBased;
		dynamic_cast<Selection_TreeBased*>(pSelectorWidget)->Initialize(nEntityID,false,false,false,bEnableMultiSelection); //enable double click!
		break;
	case MainEntitySelectionController::TYPE_CONTACTS:
		pSelectorWidget = new Selection_Contacts;
		dynamic_cast<Selection_Contacts*>(pSelectorWidget)->Initialize(false,false,false,bEnableMultiSelection); //enable double click!
		break;
	case MainEntitySelectionController::TYPE_GROUP_ITEMS:
		pSelectorWidget = new Selection_GroupTree;
		dynamic_cast<Selection_GroupTree*>(pSelectorWidget)->Initialize(ENTITY_BUS_CONTACT,false,false,false,bEnableMultiSelection); //enable double click!
		break;

	default:
		Q_ASSERT(false);
		break;
	}

	//determine name: tofix: some global place for this?
	QString strEntityName=QObject::tr("Selector");
	switch(nEntityID)
	{
	case ENTITY_BUS_PERSON:
		strEntityName=QObject::tr("User Selector");
		break;
	case ENTITY_BUS_CONTACT:
		strEntityName=QObject::tr("Contact Selector");
		break;
	case ENTITY_BUS_PROJECT:
		strEntityName=QObject::tr("Project Selector");
	    break;
	}

	//set title, go go
	Q_ASSERT(pSelectorWidget);
	if(pSelectorWidget){
		pSelectorWidget->setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint);
		pSelectorWidget->setWindowTitle(strEntityName);
		pSelectorWidget->show();
	}
}