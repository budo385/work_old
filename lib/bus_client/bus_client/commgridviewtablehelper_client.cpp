#include "commgridviewtablehelper_client.h"

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

#include "commgridviewhelper_client.h"

CommGridViewTableHelper_Client::CommGridViewTableHelper_Client(QObject *parent/*=0*/)
	: CommGridViewTableHelper(parent)
{
	InitializeHelper();
}

CommGridViewTableHelper_Client::~CommGridViewTableHelper_Client()
{

	delete m_pCmGrdHelper;
}

int CommGridViewTableHelper_Client::GetLoggedPersonID()
{
	return g_pClientManager->GetPersonID();
}

QString CommGridViewTableHelper_Client::GetLoggedPersonInitials()
{
	return g_pClientManager->GetPersonInitials();
}

void CommGridViewTableHelper_Client::InitializeHelper()
{
	m_pCmGrdHelper=new CommGridViewHelper_Client;

}