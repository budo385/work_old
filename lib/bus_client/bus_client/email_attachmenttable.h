#ifndef EMAIL_ATTACHMENTTABLE_H
#define EMAIL_ATTACHMENTTABLE_H

#include "common/common/dbrecordset.h"
#include <QtWidgets/QTableWidget>
#include <QToolButton>
#include <QTextEdit>
#include <QCheckBox>


class Email_AttachmentTable : public QTableWidget
{
	friend class WidgetRow;

	Q_OBJECT

public:
	Email_AttachmentTable(QWidget *parent=0);
	~Email_AttachmentTable();

	void	SetEditMode(bool bEdit=true);
	void	SetData (DbRecordSet lstAttachments);
	bool	GetData (DbRecordSet &lstAttachments, bool bZipAttachments=false);
	void	ClearAll();
	void	AppendFiles(QStringList *lstFiles);
	bool	IsCIDAttachmentExists();
public slots:
	void	AddNewAttachment();

private slots:
	void	OnZIPChanged(bool,int);

private:
	void	OnAddAttachment(int nRow);
	void	OnSaveAttachemnt(int nRow);
	void	OnDeleteAttachment(int nRow);
	void	OnOpenAttachment(int nRow);
	void	RefreshDisplay();
	void	OnDropSokratesDocument(DbRecordSet);
	bool	AddAttachmentFromFile(QString strFilePath, int nRow=-1);	
	bool	ZipAttachmentsBeforeSend();
	bool	IsZip(int nRow);

	Qt::DropActions		supportedDropActions() const;
	void				dropEvent(QDropEvent *event);
	void				dragEnterEvent ( QDragEnterEvent * event );
	void				dragMoveEvent ( QDragMoveEvent * event );
	
	bool m_bEdit;
	DbRecordSet m_lstAttachments;
	DbRecordSet m_lstAttachmentsCID;

	int					m_nDragMimeType;
	int					m_nDropMimeType;
	QString				m_strDragIcon;

protected:
	virtual QMimeData *		mimeData ( const QList<QTableWidgetItem *> items ) const;
	virtual QStringList		mimeTypes () const;
	virtual void			startDrag(Qt::DropActions supportedActions);
};


class WidgetRow : public QWidget
{
	Q_OBJECT
public:
	WidgetRow(Email_AttachmentTable *parent,int nRow);
	~WidgetRow();

	void SetData(QString strDocument,bool bZip);
	void GetData(bool &bZip,QString &strDocument);

	QToolButton *btnSelect;
	QToolButton *btnDelete;
	QToolButton *btnOpen;
	QToolButton *btnSave;
	QCheckBox *m_btnZIP;
	QTextEdit *m_txtName;

signals:
	void ZIPChanged(bool bIsChecked,int nRow);

private slots:
	void OnSelect();
	void OnRemove();
	void OnOpen();
	void OnSave();

	void OnZipStateChanged(int nState);

private:
	Email_AttachmentTable *m_Parent;
	int m_nRow;
};
#endif // EMAIL_ATTACHMENTTABLE_H
