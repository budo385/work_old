#include "rolelistmodel.h"

RoleListModel::RoleListModel(QObject *Parent /*= NULL*/)
	: DbRecordSetListModel(Parent)
{

}

RoleListModel::~RoleListModel()
{

}

Qt::ItemFlags RoleListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;

	Qt::ItemFlags DefaultFlags = QAbstractItemModel::flags(index);
	return Qt::ItemIsDragEnabled | DefaultFlags;
}

Qt::DropActions RoleListModel::supportedDropActions() const
{
	return Qt::MoveAction;
}

QStringList RoleListModel::mimeTypes() const
{
	QStringList types;
	types << "helix/roleitem";
	return types;
}



