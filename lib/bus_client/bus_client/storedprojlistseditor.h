#ifndef StoredProjListsEditor_H
#define StoredProjListsEditor_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QComboBox>
#include <QLineEdit>
#include <QLabel>
#include <QHash>
#include "common/common/cliententitycache.h"
#include "gui_core/gui_core/wizardpage.h"
#include "bus_client/bus_client/accuserrecordselector.h"
#include "ui_storedprojlistseditor.h"
#include "simpleselectionwizpage.h"
#include "customselectionswnd.h"
#include "customcollectionswnd.h"

#define PAGE_GROUP_IDX			0
#define PAGE_SELECTIONS_IDX		1
#define PAGE_COLLECTIONS_IDX	2

class StoredProjListsEditor : public QDialog
{
	Q_OBJECT

public:
	StoredProjListsEditor(int nListID, QWidget *parent = 0);
    ~StoredProjListsEditor();

	void				DisplayPageByListType();
	void				ClearPages();
	void				SetListName(QString strName);
	QString				GetListName();

	void				GetWizPageRecordSetList(QList<DbRecordSet> &RecordSetList);	 // Get wizard recordset list.
	void				ClearPageHashFromThisPageUp(int nThisPageNumber);
	void				SetMainLayoutSize(QSize szSize);
	void				DisableNameEdit();
	void				GetTreeData(DbRecordSet &lstData);

	QString				GetProjListXML(){return m_strProjlistXML;}
	int					GetProjListType(){return m_nProjlistType;}


	AccUserRecordSelector *GetAccUserRecordSelector(){return m_pUarWidget;};

protected:
	void				setFirstPage();
	void				DisableButtons();
	void				EnableButtons();
	int					m_nListID;

public:
	

private:
	Ui::StoredProjListsEditorClass ui;
	DbRecordSet m_lstGroupData;						// SPL GROUP DATA

	QList<DbRecordSet>	m_lstPageRecordSet;			// Pages results recordset list
	SimpleSelectionWizPage *m_pPageGroup;
	CustomSelectionsWnd	*m_pPageSelections;
	CustomCollectionsWnd *m_pPageCollections;
	QVBoxLayout	*mainLayout;

	QString m_strProjlistXML;
	int m_nProjlistType;

	// List data parameters (::GetListData)
	bool m_bIsSelection;
	QString m_strSelectionXML;
	bool m_bIsFilter;
	QString m_strFilterXML;
	bool m_bGetDescOnly;		// set to TRUE before actual build.
	AccUserRecordSelector *m_pUarWidget;

private slots:
	//void listNamesCombo_selectionChanged(int nIdx);
	void completeStateChanged();
	virtual void accept();

	void on_radTypeGroup_toggled(bool bOn);
	void on_radTypeSelection_toggled(bool bOn);
	void on_radTypeCollection_toggled(bool bOn);

	
};

#endif // StoredProjListsEditor_H