#include "tree_selectiongroup.h"
#include "common/common/entity_id_collection.h"

Tree_SelectionGroup::Tree_SelectionGroup(QWidget *parent)
	: UniversalTreeWidget(parent)
{

	m_lastitem=NULL;
	AddAcceptableDropTypes(ENTITY_BUS_CONTACT);
}

Tree_SelectionGroup::~Tree_SelectionGroup()
{

}



void Tree_SelectionGroup::DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
{
	m_lastitem=itemFromIndex(index);
	if (m_lastitem==NULL) return;

	m_lastDroppedValue=DroppedValue;
	emit DropOccured();

}
