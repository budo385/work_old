#ifndef MAINENTITYSELECTIONCONTROLLER_H
#define MAINENTITYSELECTIONCONTROLLER_H

#include <QtWidgets/QMessageBox>
#include "common/common/dbrecordset.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "common/common/observer_ptrn.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "bus_client/bus_client/selectorsignals.h"



/*!
	\class  MainEntitySelectionController
	\brief  Class providing interface for Main Entity Selection controllers 
	\ingroup GUICore_SelectionController


	Features:
	- provides interface for fetching main entity data for selection controllers from server.
	- uses global cache to save server calls
	- accepts signals from FUI's when main entity data is changed to update data in selection controller
	- can hold filters for filtering entity data (e.g. persons from only one organization)
	- used as backend of GUI selection controller widgets, but can be used as standalone
	- data displayed in controller can be hardcoded (not in database)

	Adding new entity handler:
	- to add new entity selection handler, implement server method inside BO service:
		_SERVER_CALL(MainEntitySelector->ReadData()
	- in Initialize() and all other functions, add switch/case for own entity type (see others)
	
	Use:
	- can be use as standalone object, as datasource for entity data. This is better then calling server 
	  directly, because: a) uses cache, b) any change in entity data is reflected here
    - most common use is as datasource for GUI widget for displaying selected data (tree, table, combo, etc..)
	  Inherit your widget from this class and implement all methods needed.
    - Initialize() with your unique entityID (unique entity ID/data ID from bus_core/entity_id_collection.h )
	- use Set/GetFilter() to filter data
	- use GetDataSource() to access data

	Signals/obeserver/cache/FUI:
	- when Initialized(), data will be loaded from cache if exists or from server (use bSkipLoadingData to skip this load)
	- if filter is set then loading will be from server only (no cache)
	- if data is reloaded from server (ReloadFromServer) and no filter is set, new data from server will stored in the cache 
	  and global ENTITY_DATA_RELOADED_IN_CACHE signal will emited, other controllers will accept this signal and reload data from cache (ReloadFromCache)
	- if data is changed inside FUI or other GUI (add, del, edit) then this controller will accept signal (RefreshData()) and change data, 
      but only if filter data is same, new entity will be added, delete & edit is propagated to all instances...
    - GUI widgets that are using this controller must preserve state of tree/table/widget if whole data is beeing reloaded, or when only one item is changed
	- when data is changed inside, SELECTOR_DATA_CHANGED is emitted
	- when selection is changed, emit SELECTOR_SELECTION_CHANGED in your widget to notify others


*/
class MainEntitySelectionController : public ObsrPtrn_Subject, public ObsrPtrn_Observer
{
	//Q_OBJECT
	friend class SelectionPopup;

public:
    MainEntitySelectionController();
    virtual ~MainEntitySelectionController();

	enum UpdateCache					//use as nOperation in RefreshToGlobalCache() when data is changed here in database -> notify others 
	{
		CACHE_UPDATE_DATA_DELETED,			
		CACHE_UPDATE_DATA_INSERTED,
		CACHE_UPDATE_DATA_EDITED
	};
	enum DataTypes						//supported data types...
	{
		TYPE_TREE,
		TYPE_TABLE,
		TYPE_CONTACTS,
		TYPE_GROUP_ITEMS,
		TYPE_USERS
	};
	enum RefreshAction					//for RefreshDisplay overloaded function
	{
		REFRESH_RELOAD_ALL,
		REFRESH_ITEM_ADD,
		REFRESH_ITEM_EDIT,
		REFRESH_ITEM_DELETE
	};

	virtual void Initialize(int nEntityID);
	int GetSelectionEntity(){return m_nEntityID;};
	int GetPrimaryKeyIdx(){return m_nPrimaryKeyColumnIdx;};
	virtual void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	bool ReloadData(bool bReloadFromServer=false);		//After Init() or SetFilter() call this function to actually reload data from server or from cache or from hardcoded data structure
	DbRecordSet* GetDataSource(){return &m_lstData;};	//return pointer to data storage
	static int GetDataType(int nEntityID);
	virtual QString GetCalculatedName(int nEntityRecordID); //from common columns assembles calculated name column (e.g. Contact: Org,LastName FirstName, Pers: LastName, First
	virtual QString GetCalculatedNameFromRecord(DbRecordSet &recData);

	bool IsEntityEnabled();
	void SetForceFullProjectDataReload(bool bSet){m_bForceFullProjectDataReload=bSet;};

	//Filter Ops:
	//----------------------------------------------------------------
	MainEntityFilter* GetFilter(){return &m_FilterData;};
	void SetFilter(MainEntityFilter filterData){m_FilterData=filterData;};
	void ClearFilter(){m_FilterData.ClearFilter();};
	MainEntityFilter* GetLocalFilter(){return &m_LocalFilterData;};
	void SetLocalFilter(MainEntityFilter filterData){m_LocalFilterData=filterData;};
	void ClearLocalFilter(){m_LocalFilterData.ClearFilter();};
	void ShowOnlyUserContacts();
	int GetPersonIDFromContactID(int nContactID);
	int GetContactIDFromPersonID(int nPersonID);

	//----------------------------------------------------------------

	//GUI support (gets selection will return more then 1 selection in lstEntityRecord if more then 1 selected):
	virtual	void GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord){};  //called from outside to get current selection from Selector Widget
	virtual void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nPrimaryKeyValue=-1){};		//called after data reload or change, implement to refresh display on widget (preserver prev. state) 
	void GetColumnSetup(DbRecordSet &columns){columns=m_TableColumns;};								//called by widget to get its data
	virtual void SetCurrentEntityRecord(int nEntityRecordID,bool bSkipNotifyObservers=false);			
	virtual bool GetEntityRecord(int &nEntityRecordID,DbRecordSet &record); 
	virtual bool GetEntityRecords(DbRecordSet &lstData,int nIDColumnIdx_Inside_lstData,DbRecordSet &lstResults); //reloads multi records from server, based on id, if suceed, return result in entity format
	bool BatchReadEntityData(QList<int> lstEntityIDs);
	//Brief Info:
	//RefreshToGlobalCache: data is changed here (records are deleted, modifed at db), use to notify others
	//								emits global signals 
	void RefreshToGlobalCache(int nOperation, bool bOperateOnSelectedRows=true,DbRecordSet *newData=NULL);
	//specific data (public domain):
	int m_nPrimaryKeyColumnIdx;
	int m_nParentColumnIdx;
	int m_nLevelColumnIdx;
	int m_nCodeColumnIdx;
	int m_nNameColumnIdx;
	int m_nHasChildrenColumnIdx;
	QString m_strTablePrefix;
	bool m_bProjectHideInvisible;
	bool m_bShowOnlyUserContacts;

protected:
	//Re implement this functions in GUI widgets to refresh displayed data in widget
	virtual bool ReloadFromServer();		
	virtual bool ReloadFromCache();			
	virtual void RefreshFromGlobalCache(int nOperation, DbRecordSet *newData);
	virtual void ApplyLocalFilter();
	virtual void LoadOneFromServer(int nRecordID,DbRecordSet &data);		
	virtual bool LoadAllFromServer();									//check if cache has current data,refresh cache & display whole entity data 
	virtual bool LoadProjectsFromServer();	
	virtual void FilterUserContacts();

	void SetColumnIndexes(QString strTablePrefix);		//calc column names based on table prefix
	int m_nDataTypeID;			//< Data Type: TABLE,TREE, CONTACT, -- define in enumerator, set in Initialize
	DbRecordSet m_lstData;		//< actual data of selection controller
	int m_nEntityID;			//< Entity ID which uniquely identifies type of data ( defined in bus_core/entity_id_collection.h)
	DbRecordSet m_TableColumns;	//< collection of column data for table type of selection controllers
	bool m_bLocalData;			//< if data is hardcoded then set this to true to skip calls to server & cache
	MainEntityFilter m_FilterData;
	MainEntityFilter m_LocalFilterData;
	MainEntityCalculatedName m_CalcName;
	MainEntitySelectionController *m_PersonCache;
	bool m_bForceFullProjectDataReload;
};

#endif // MAINENTITYSELECTIONCONTROLLER_H



