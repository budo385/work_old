#include "bus_client/bus_client/selection_nm_sapne.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include <QHeaderView>
#include "db_core/db_core/dbsqltableview.h"
//bo set
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	

//global message dispatcher
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;

#define HTML_START_F1	"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">"
#define HTML_END_F1		"</span></p></body></html>"

Selection_NM_SAPNE::Selection_NM_SAPNE(QWidget *parent)
: QFrame(parent),m_btnContactPie(NULL) //must be crreated by implementation class
{
	ui.setupUi(this);


	m_pLastFUIOpen=NULL;

	//set icons:
	ui.btnModify->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnSelect->setIcon(QIcon(":Select16.png"));
	ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnInsert->setIcon(QIcon(":handwrite.png"));
	ui.btnOpenNewWindow->setIcon(QIcon(":handwithbook.png"));

	ui.btnSelect->setAutoRaise(false);


	//ui.btnModify->setToolTip(strComplexToolTip);
	ui.btnModify->setToolTip(tr("Modify/Create"));
	ui.btnSelect->setToolTip(tr("Select"));
	ui.btnRemove->setToolTip(tr("Remove Assignment"));
	ui.btnInsert->setToolTip(tr("Add"));
	ui.btnOpenNewWindow->setToolTip(tr("Open In New Window"));


	m_dlgCachedPopUpSelector=NULL;
	m_nActualOrganizationID=-1;
	m_bAutoAssigment=false;
	m_strTitle="";
	m_strFKColumnName="";
	m_nTableDisplayViewID=-1;

	connect(ui.tableView,SIGNAL(SignalSelectionChanged()),this,SLOT(OnItemClicked()));

	g_ChangeManager.registerObserver(this);

}

Selection_NM_SAPNE::~Selection_NM_SAPNE()
{
	//unregister observer:
	g_ChangeManager.unregisterObserver(this);
}






/*!
	Inits entity ID

	//if bBlockNeePopUp, then no new window button: use when already open as dialog
*/


void Selection_NM_SAPNE::Initialize(int nEntityTypeID,QString strTitle,bool bBlockNewPopUp)
{
	if (!strTitle.isEmpty())
	{
		ui.labelTitle->setText(QString(HTML_START_F1)+strTitle+HTML_END_F1);
		m_strTitle=strTitle;
	}

	if (bBlockNewPopUp)
		ui.btnOpenNewWindow->setVisible(false);


	//set entity ID:
	m_nEntityTypeID=nEntityTypeID;

	//init selector: data is not yet loaded from server, nor it will be until selection pop's up
	m_dlgPopUp.Initialize(nEntityTypeID,true);
	m_dlgCachedPopUpSelector=dynamic_cast<MainEntitySelectionController*>(m_dlgPopUp.GetSelectorWidget());
	Q_ASSERT(m_dlgCachedPopUpSelector!=NULL); //must be valid


	m_lstData.copyDefinition(*m_dlgCachedPopUpSelector->GetDataSource());
	m_NameFiller.Initialize(m_nEntityTypeID,*m_dlgCachedPopUpSelector->GetDataSource(),m_dlgCachedPopUpSelector->m_nCodeColumnIdx,m_dlgCachedPopUpSelector->m_nNameColumnIdx);
	ui.tableView->horizontalHeader()->hide();
	ui.tableView->horizontalHeader()->setStretchLastSection(true);
	

	m_lstData.addColumn(QVariant::String,"_CALC_NAME_"); //Addcolumn for display

	ui.tableView->Initialize(&m_lstData,false,false,false,true,25,true);
	DbRecordSet lstSetup;
	ui.tableView->AddColumnToSetup(lstSetup,"_CALC_NAME_",tr("Data"),100,false);
	ui.tableView->SetColumnSetup(lstSetup);
	//ui.tableView->horizontalHeader()->hide();

	m_nFK_IDColIdx=m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx;
	

	//if contact, enable pie
	if (nEntityTypeID==ENTITY_BUS_CONTACT || nEntityTypeID==ENTITY_BUS_PERSON || nEntityTypeID==ENTITY_BUS_PROJECT)
	{
		CreatePieButton();
		if(m_btnContactPie)m_btnContactPie->SetEntityType(m_nEntityTypeID);
		if(m_btnContactPie)m_btnContactPie->setVisible(true);
	}
	else
		if(m_btnContactPie)m_btnContactPie->setVisible(false);

	QWidget::setEnabled(m_dlgCachedPopUpSelector->IsEntityEnabled()); //FP
}


void Selection_NM_SAPNE::setDisabled ( bool bEnable)
{
	if (m_dlgCachedPopUpSelector)
		if (!m_dlgCachedPopUpSelector->IsEntityEnabled())
			return;
	QWidget::setDisabled(bEnable);
}

void Selection_NM_SAPNE::setEnabled ( bool bEnable)
{
	if (m_dlgCachedPopUpSelector)
		if (!m_dlgCachedPopUpSelector->IsEntityEnabled())
			return;
	QWidget::setEnabled(bEnable);
}

void Selection_NM_SAPNE::Initialize(int nEntityTypeID,QString strFKColumnName,int nTableViewID,DbRecordSet &lstColumnSetup,QString strTitle,bool bBlockNewPopUp)
{
	if (!strTitle.isEmpty())
	{
		ui.labelTitle->setText(QString(HTML_START_F1)+strTitle+HTML_END_F1);
		m_strTitle=strTitle;
	}

	if (bBlockNewPopUp)
		ui.btnOpenNewWindow->setVisible(false);


	m_strFKColumnName=strFKColumnName;

	//set entity ID:
	m_nEntityTypeID=nEntityTypeID;

	//init selector: data is not yet loaded from server, nor it will be until selection pop's up
	m_dlgPopUp.Initialize(nEntityTypeID,true);
	m_dlgCachedPopUpSelector=dynamic_cast<MainEntitySelectionController*>(m_dlgPopUp.GetSelectorWidget());
	Q_ASSERT(m_dlgCachedPopUpSelector!=NULL); //must be valid


	//init grid:
	m_nTableDisplayViewID=nTableViewID;
	m_lstData.defineFromView(DbSqlTableView::getView(nTableViewID));
	m_NameFiller.Initialize(m_nEntityTypeID,m_lstData,m_lstData.getColumnIdx(m_dlgCachedPopUpSelector->m_strTablePrefix+"_CODE"),m_lstData.getColumnIdx(m_dlgCachedPopUpSelector->m_strTablePrefix+"_NAME"));


	m_lstData.addColumn(QVariant::String,"_CALC_NAME_"); //Addcolumn for display
	ui.tableView->Initialize(&m_lstData,false,false,false,true,25,true);
	ui.tableView->SetColumnSetup(lstColumnSetup);

	m_nFK_IDColIdx=m_lstData.getColumnIdx(strFKColumnName);
	Q_ASSERT(m_nFK_IDColIdx!=-1);

	//if contact, enable pie
	if(m_btnContactPie){
		if (nEntityTypeID==ENTITY_BUS_CONTACT || nEntityTypeID==ENTITY_BUS_PERSON || nEntityTypeID==ENTITY_BUS_PROJECT)
			m_btnContactPie->setVisible(true);
		else
			m_btnContactPie->setVisible(false);
	}
}



//set list (if empty it will be cleared,nPK_IDColIdx only used if list in other format
void Selection_NM_SAPNE::SetList(DbRecordSet &lstNMData, int nPK_IDColIdx)		
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;


	DbRecordSet lstData;
	
	//only if view is internal, do calc name job:
	if (m_nTableDisplayViewID==-1)
	{
		Q_ASSERT(nPK_IDColIdx!=-1); //must be defined
		if (m_dlgCachedPopUpSelector->GetEntityRecords(lstNMData,nPK_IDColIdx,lstData))
		{
			m_lstData.clear();
			m_lstData.merge(lstData);
			int nNameColIdx=m_lstData.getColumnCount()-1;
			m_NameFiller.FillCalculatedNames(m_lstData,nNameColIdx); //last col fill with names
			ui.tableView->RefreshDisplay();//listWidget->SetData(lstData,nNameColIdx);
		}
		else
			ui.tableView->RefreshDisplay();
	}
	else
	{
		m_lstData.clear();
		m_lstData.merge(lstNMData);
		int nNameColIdx=m_lstData.getColumnCount()-1;
		m_NameFiller.FillCalculatedNames(m_lstData,nNameColIdx); //last col fill with names
		ui.tableView->RefreshDisplay();
	}



	if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(0);
	notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,0);
}

//data in entity format:
void Selection_NM_SAPNE::GetList(DbRecordSet &lstNMData)				
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;
	lstNMData=m_lstData;
	if (m_lstData.getColumnCount()>0)
	{
		lstNMData.removeColumn(lstNMData.getColumnCount()-1);
	}
	
}






//--------------------------------------------------------------
//					EVENT HANDLERS
//--------------------------------------------------------------

//open selector, get selection, display & store inside data source
void Selection_NM_SAPNE::on_btnSelect_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;
	DbRecordSet empty;
	int nResult=m_dlgPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		m_dlgPopUp.GetSelectedData(nRecordID,record);
		SetEntityData(nRecordID,record);
	}

}


//remove all entries, store NULL inside datasource
void Selection_NM_SAPNE::on_btnRemove_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;
	//ui.listWidget->DeleteSelection();

	DbRecordSet lstData=m_lstData.getSelectedRecordSet();
	if (lstData.getRowCount()>0)
	{
		emit OnRemove(lstData);
		if (lstData.getRowCount()>0) 
		{
			m_lstData.deleteSelectedRows();
			ui.tableView->RefreshDisplay();
		}
	}
}



//open FUI in new window, set new ID if exists
//TOFIX: connect back with observer to get new changes!! brb....
void Selection_NM_SAPNE::on_btnModify_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;

	int nRow=m_lstData.getSelectedRow();
	int nRecordID =-1;
	if (nRow!=-1)
	{
		nRecordID=m_lstData.getDataRef(nRow,m_nFK_IDColIdx).toInt();
	}
	
	notifyObservers(SelectorSignals::SELECTOR_ON_EDIT,nRecordID,m_nActualOrganizationID);

	/*
	//open FUI, get window pointer
	int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
	int nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_READ,nRecordID,true);
	m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(m_nActualOrganizationID!=-1 && nRecordID==-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI
	*/

}





//reload data signal from selector: reload combo if needed, if selector changed data
void Selection_NM_SAPNE::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant data)
{
	//if EDIT mode, our FUI just inserted new id, assign back to FK link
	if(m_bAutoAssigment && pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED && nMsgDetail==m_dlgCachedPopUpSelector->GetSelectionEntity())
	{
		int nPK=m_dlgCachedPopUpSelector->GetPrimaryKeyIdx();
		//extract new row:
		DbRecordSet record=data.value<DbRecordSet>();
		//record.Dump();
		if (record.getRowCount()!=1 || nPK==-1) //if row count doesnt match or ID col is not given, abort
			return;

		SetEntityData(record.getDataRef(0,nPK).toInt(),record);
	}
}




//set this data on display and inside data source
void Selection_NM_SAPNE::SetEntityData(int nRecordID, DbRecordSet &record,bool bSkipNotifyObservers)
{
	//only add on existing one:
	DbRecordSet lstData;
	//ui.listWidget->GetData(lstData);
	
	//-------------calculate name for display
	if (m_nTableDisplayViewID!=-1)
		lstData.defineFromView(DbSqlTableView::getView(m_nTableDisplayViewID));
	else
		lstData.copyDefinition(*m_dlgCachedPopUpSelector->GetDataSource());
	
	//-------------calculate name for display
	lstData.addColumn(QVariant::String,"_CALC_NAME_"); //Addcolumn for display
	lstData.merge(record);			//add new data

	emit OnAdd(lstData);		   //emit & change data

	//cacl data:
	int nNameColIdx=lstData.getColumnCount()-1;
	m_NameFiller.FillCalculatedNames(lstData,nNameColIdx); //last col fill with names


	//lstData.Dump();

	m_lstData.merge(lstData);
	m_lstData.removeDuplicates(0); //remove duplicates (on idx=0) //TOFIX...
	ui.tableView->RefreshDisplay();

	//ui.listWidget->SetData(lstData,nNameColIdx);

	//notify all observers
	if(!bSkipNotifyObservers)
	{
		QVariant varData;
		qVariantSetValue(varData,record);
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID,varData);
	}
}






void Selection_NM_SAPNE::SetEditMode(bool bEditMode)
{
	ui.btnSelect->setEnabled(bEditMode);
	ui.btnRemove->setEnabled(bEditMode);
	ui.btnInsert->setEnabled(bEditMode);
	ui.tableView->SetEditMode(bEditMode);
	//ui.btnOpenNewWindow->setEnabled(bEditMode);

	m_bAutoAssigment=false;					//block input for insert if changing edit mode
}



//if click, open FUI, go insert, store Last FUI pointer, send signal 
void Selection_NM_SAPNE::on_btnInsert_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;
	notifyObservers(SelectorSignals::SELECTOR_ON_INSERT,0,m_nActualOrganizationID);

	/*
	//open FUI, get window pointer
	int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
	int nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_INSERT,-1,true);
	m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	if(m_nActualOrganizationID!=-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI

	*/
	m_bAutoAssigment=true;
}



void Selection_NM_SAPNE::Clear(bool bSkipNotifyObservers)
{
	if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(0);
	m_lstData.clear();
	ui.tableView->RefreshDisplay();
	//ui.listWidget->ClearData();

	//notify all observers
	if(!bSkipNotifyObservers)
	{
		DbRecordSet empty;
		QVariant varData;
		qVariantSetValue(varData,empty);
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,-1,varData); //invalid record:
	}
}




void Selection_NM_SAPNE::OnItemClicked()
{
	int nRow=m_lstData.getSelectedRow();

	if (m_nEntityTypeID==ENTITY_BUS_CONTACT && nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BCNT_ID").toInt();
		if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(nContactID);
	}

	if (m_nEntityTypeID==ENTITY_BUS_PERSON && nRow!=-1)
	{
		int nContactID=m_lstData.getDataRef(nRow,"BPER_CONTACT_ID").toInt();
		if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(nContactID);
	}

	if (m_nEntityTypeID==ENTITY_BUS_PROJECT && nRow!=-1)
	{
		int nProjectID=m_lstData.getDataRef(nRow,"BUSP_ID").toInt();
		if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(nProjectID);
	}
}


void Selection_NM_SAPNE::on_btnOpenNewWindow_clicked()
{
	//open Frame as dialog in new window:
	Selection_NM_SAPNE *winNew= new Selection_NM_SAPNE( NULL);
	DbRecordSet data;
	GetList(data);

	DbRecordSet lstSetup;
	ui.tableView->GetColumnSetup(lstSetup);
	if (m_nTableDisplayViewID==-1)
	{
		winNew->Initialize(m_nEntityTypeID,m_strTitle,true);
		winNew->SetList(data,m_nFK_IDColIdx); //this is pk from cache record
	}
	else
	{
		winNew->Initialize(m_nEntityTypeID,m_strFKColumnName,m_nTableDisplayViewID,lstSetup,m_strTitle,true);
		winNew->SetList(data);
	}

	
	winNew->SetEditMode(ui.btnInsert->isEnabled());

	QPoint myPos=ui.btnOpenNewWindow->mapToGlobal(QPoint(0,0));
	winNew->move(myPos);
	winNew->setWindowTitle(m_strTitle);
	winNew->setWindowModality (Qt::ApplicationModal);
	winNew->show();
}



QWidget* Selection_NM_SAPNE::GetButton(int nButton)
{
	switch(nButton)
	{
	case BUTTON_CONTACT_PIE:
		return m_btnContactPie;
		break;
	case BUTTON_SELECT:
		return ui.btnSelect;
		break;
	case BUTTON_MODIFY:
		return ui.btnModify;
	    break;
	case BUTTON_REMOVE:
		return ui.btnRemove;
	    break;
	case BUTTON_ADD:
		return ui.btnInsert;
		break;
	case BUTTON_OPEN_NEW:
		return ui.btnOpenNewWindow;
		break;
	default:
		return ui.btnSelect;
	    break;
	}

}