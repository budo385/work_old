#ifndef CLIENTBACKUPMANAGER_H
#define CLIENTBACKUPMANAGER_H

#include "common/common/status.h"
#include "bus_core/bus_core/backupmanager.h"


/*!
	\class ClientBackupManager
	\brief Global client object for restoring, organize, backup DB
	\ingroup Bus_Client

	THICK mode:
	- on start up THICK service: check for backup, restore, reorg.
	- in options-> save all settings in INI file, restore..backup

	THIN mode:
	- Backup mode->on server<-ret list
	- Restore mode->on server<-disconect, server->restore()----->>>>clean all cached, locked ressources

	SETUP mode (template) THIN:
	- CDI_DB_INFO: flag=IsDatabaseSetup, String=CurrentTemplateName
	-> if not, pick, restore from....

	SETUP mode (template) THICK:
	- CDI_DB_INFO: flag=IsDatabaseSetup, String=CurrentTemplateName
	-> if not, pick, restore from....


*/
class ClientBackupManager : public BackupManager
{
	Q_OBJECT

public:
	ClientBackupManager();
	~ClientBackupManager();

	//after set, use save..
	void SetClientMode(bool bIsThinClient){m_bIsThinClient=bIsThinClient;};
	bool AskForTemplateDatabase();
	void Backup(Status &pStatus);
	void Restore(Status &pStatus,QString strFileNameForRestore);
	//start up API for THICK client:
	void ThickModeRestartTimer();
	void StopThickModeTimer();

signals:
	void Thick_ClearDatabaseAfterRestore();
	void Thick_StartDatabase();
	void Thick_CloseDatabase();
	
protected:
	void timerEvent(QTimerEvent *event);
	QString ExecBackup(Status &pStatus);
	
	void ClearDatabaseAfterRestore(Status &pStatus){emit Thick_ClearDatabaseAfterRestore();}
	void StartDatabase(){emit Thick_StartDatabase();};
	void CloseDatabase(){emit Thick_CloseDatabase();};


private:

	int m_nTimerID;						///< timer ID for backuping
	bool m_bIsThinClient;
	
};

#endif // CLIENTBACKUPMANAGER_H
