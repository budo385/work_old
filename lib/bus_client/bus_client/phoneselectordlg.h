#ifndef PHONESELECTORDLG_H
#define PHONESELECTORDLG_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_phoneselectordlg.h"

class PhoneSelectorDlg : public QDialog
{
    Q_OBJECT

public:
    PhoneSelectorDlg(QStringList &lstPhones, QStringList &lstNames, QStringList &lstTypes, QStringList lstPreselectedPhones=QStringList(),QWidget *parent = 0);
    ~PhoneSelectorDlg();

	QString getSelectedPhone();
	int getSelectedPhoneIdx();

private:
    Ui::PhoneSelectorDlgClass ui;

private slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();

};

#endif // PHONESELECTORDLG_H
