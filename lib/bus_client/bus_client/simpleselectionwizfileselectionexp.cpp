#include "SimpleSelectionWizFileSelectionexp.h"
#include <QFileDialog>
#include "common/common/datahelper.h"
#include <QDir>
extern QString g_strLastDir_FileOpen;
SimpleSelectionWizFileSelectionExp::SimpleSelectionWizFileSelectionExp(int nWizardPageID, QString strPageTitle, QWidget *parent, int nPageEntityID /*= -1*/)
: WizardPage(nWizardPageID, strPageTitle)
{
	setMinimumSize(QSize(600,50));
	m_recResultRecordSet.addColumn(QVariant::String,"PATH");
	m_recResultRecordSet.addColumn(QVariant::Bool,"SOKRATES_HEADER");
	m_recResultRecordSet.addColumn(QVariant::Bool,"COLUMN_TITLES_HEADER");
	m_recResultRecordSet.addColumn(QVariant::Bool,"OUTLOOK_EXPORT");
	m_recResultRecordSet.addRow();
	m_recResultRecordSet.setData(0,0,"");
	m_bHideOutlookExpChk = false;
}

SimpleSelectionWizFileSelectionExp::~SimpleSelectionWizFileSelectionExp()
{

}

void SimpleSelectionWizFileSelectionExp::Initialize()
{
	ui.setupUi(this);

	ui.txtPath->setFocus();
	ui.btnSelectPath->setToolTip(tr("Select directory"));
	ui.btnSelectPath->setIcon(QIcon(":SAP_Select.png"));
	m_bInitialized = true;

	if(m_bHideOutlookExpChk)
		ui.chkOutlookExport->hide();
}

void SimpleSelectionWizFileSelectionExp::on_btnSelectPath_clicked()
{
	QString strStartDir=ui.txtPath->text();

	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetApplicationHomeDir();
	}

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getSaveFileName(
		NULL,
		tr("Select File"),
		strStartDir,
		tr("Text file (*.txt)"));

	if(!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath->setText(QDir::toNativeSeparators(strFile));
		m_bComplete = true;
		emit completeStateChanged();
	}
}

void SimpleSelectionWizFileSelectionExp::on_txtPath_textChanged( const QString &text )
{
	m_bComplete = true;
	emit completeStateChanged();
}

bool SimpleSelectionWizFileSelectionExp::GetPageResult(DbRecordSet &RecordSet)
{
	if (!m_bInitialized)
		return false;

	m_recResultRecordSet.setData(0, 0, ui.txtPath->text());
	m_recResultRecordSet.setData(0, 1, ui.chkSokratesHdr->isChecked()); //"SOKRATES_HEADER"
	m_recResultRecordSet.setData(0, 2, ui.chkColumnHdr->isChecked()); //"COLUMN_TITLES_HEADER"
	m_recResultRecordSet.setData(0, 3, ui.chkOutlookExport->isChecked()); //"OUTLOOK_EXPORT"

	RecordSet=m_recResultRecordSet;
	return true;
}

//after retry reset: add rows:
void SimpleSelectionWizFileSelectionExp::resetPage()
{
	ui.txtPath->setText("");
}

void SimpleSelectionWizFileSelectionExp::on_chkSokratesHdr_stateChanged(int nChecked)
{
}

void SimpleSelectionWizFileSelectionExp::on_chkColumnHdr_stateChanged(int nChecked)
{
}

void SimpleSelectionWizFileSelectionExp::on_chkOutlookExport_stateChanged(int nChecked)
{
	if(nChecked){
		ui.chkSokratesHdr->setChecked(false);
		ui.chkColumnHdr->setChecked(true);
		ui.chkSokratesHdr->setEnabled(false);
		ui.chkColumnHdr->setEnabled(false);
	}
	else{
		ui.chkSokratesHdr->setEnabled(true);
		ui.chkColumnHdr->setEnabled(true);
	}
}
