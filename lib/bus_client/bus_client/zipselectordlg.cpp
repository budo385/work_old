#include "zipselectordlg.h"
#include <QXmlSimpleReader>
#include <QXmlInputSource>
#include <QtWidgets/QMessageBox>
#include "gui_core/gui_core/thememanager.h"

#define XML_EXPECT_NONE 0
#define XML_EXPECT_ZIP	1
#define XML_EXPECT_TOWN	2
#define XML_EXPECT_COUNTRY	3

class MyXmlHandler : public QXmlDefaultHandler
{
public:
	MyXmlHandler(ZipSelectorDlg &dlg) : m_dlg(dlg) { m_nXmlExpect = XML_EXPECT_NONE; };

	bool startElement ( const QString &, const QString &, const QString &, const QXmlAttributes & );
	bool endElement ( const QString &, const QString &, const QString &);
	bool characters ( const QString & ch );

public:
	ZipSelectorDlg &m_dlg;
	int m_nXmlExpect;

	QString m_strZIP;
	QString m_strTown;
};

bool MyXmlHandler::startElement ( const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts)
{
	if(localName == "postalcode")
		m_nXmlExpect = XML_EXPECT_ZIP;
	else if(localName == "name")
		m_nXmlExpect = XML_EXPECT_TOWN;
	else if(localName == "countryCode")
		m_nXmlExpect = XML_EXPECT_COUNTRY;
	else
		m_nXmlExpect = XML_EXPECT_NONE;
	return true;
}

bool MyXmlHandler::endElement ( const QString & namespaceURI, const QString & localName, const QString & qName )
{
	m_nXmlExpect = XML_EXPECT_NONE;
	return true;
}

bool MyXmlHandler::characters ( const QString & ch )
{
	if(XML_EXPECT_ZIP == m_nXmlExpect)
		m_strZIP = ch;
	else if(XML_EXPECT_TOWN == m_nXmlExpect)
		m_strTown = ch;
	else if(XML_EXPECT_COUNTRY == m_nXmlExpect)
	{
		m_dlg.AddLine(m_strZIP, m_strTown, ch);
		m_strZIP = "";
		m_strTown = "";
	}
	return true;
}

ZipSelectorDlg::ZipSelectorDlg(QString strXMLData, QString strCountryCode, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	ui.frame->setStyleSheet("QFrame#frame "+ThemeManager::GetSidebarChapter_Bkg());

	connect(ui.listWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)), this, SLOT(on_itemDoubleClicked(QTreeWidgetItem *, int)));

	m_strCountryCodeFilter = strCountryCode;

	//example XML to parse
	/*
	<?xml version="1.0" encoding="UTF-8" standalone="no"?>
	<geonames>
	<totalResultsCount>2</totalResultsCount>
	<code>
	<postalcode>21000</postalcode>
	<name>Split</name>
	<countryCode>HR</countryCode>
	<lat>43.5138889</lat>
	<lng>16.4558333</lng>
	<adminCode1/>
	<adminName1>Splitsko-Dalmatinska</adminName1>
	<adminCode2/>
	<adminName2/>
	<adminCode3/>
	<adminName3>Split</adminName3>
	</code>
	<code>
	<postalcode>21000</postalcode>
	<name>Kamen</name>
	<countryCode>HR</countryCode>
	<lat>43.5141667</lat>
	<lng>16.5147222</lng>
	<adminCode1/>
	<adminName1>Splitsko-Dalmatinska</adminName1>
	<adminCode2/>
	<adminName2/>
	<adminCode3/>
	<adminName3>Split</adminName3>
	</code>
	</geonames>
	*/

	//QStringList &lstPhones, QStringList &lstNames, QStringList &lstTypes, 
	ui.listWidget->setColumnCount(3);
	ui.listWidget->setColumnWidth(0, 50);
	ui.listWidget->setColumnWidth(1, 205);
	//ui.listWidget->header()->setHidden(false);
	QStringList lstLabels;
	lstLabels << tr("ZIP") << tr("Town") << tr("Country");
	ui.listWidget->setHeaderLabels(lstLabels);

	QXmlSimpleReader xmlReader;
	MyXmlHandler xmlHandler(*this); 
	xmlReader.setContentHandler(&xmlHandler);
    QXmlInputSource source;
	source.setData(strXMLData);
	bool ok = xmlReader.parse(&source);
	if (!ok){
		QMessageBox::information(NULL, "", tr("XML parsing failed."));
		return;
	}
}

ZipSelectorDlg::~ZipSelectorDlg()
{
}

void ZipSelectorDlg::on_btnOK_clicked()
{
	QTreeWidgetItem *item = ui.listWidget->currentItem();	
	if(!item){
		QMessageBox::information(NULL, "", tr("You need to select a row first."));
		return;
	}
	done(1);
}
	
void ZipSelectorDlg::on_btnCancel_clicked()
{
	done(0);
}

QString ZipSelectorDlg::getSelectedZIP()
{
	QTreeWidgetItem *item = ui.listWidget->currentItem();	
	if(item)
		return item->text(0);

	return QString();
}

QString ZipSelectorDlg::getSelectedTown()
{
	QTreeWidgetItem *item = ui.listWidget->currentItem();	
	if(item)
		return item->text(1);

	return QString();
}

QString ZipSelectorDlg::getSelectedCountryCode()
{
	QTreeWidgetItem *item = ui.listWidget->currentItem();	
	if(item)
		return item->text(2);

	return QString();
}

void ZipSelectorDlg::AddLine(QString strZip, QString strTown, QString strCountry)
{
	//now do the mapping from country code as used by the web service to the code as used by us
	if("US" == strCountry)
		strCountry = "USA";
	else if("DE" == strCountry)
		strCountry = "D";

	if(m_strCountryCodeFilter.isEmpty() || m_strCountryCodeFilter == strCountry)
	{
		QStringList lstRow;
		lstRow << strZip << strTown << strCountry;
		QTreeWidgetItem *item = new QTreeWidgetItem(lstRow);
		ui.listWidget->addTopLevelItem (item);
	}
}

void ZipSelectorDlg::on_itemDoubleClicked ( QTreeWidgetItem * item, int column )
{
	done(1);
}
