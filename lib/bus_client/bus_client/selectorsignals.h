#ifndef SELECTORSIGNALS_H
#define SELECTORSIGNALS_H


class SelectorSignals
{
	public:
	enum ObserverSignals				//selector signals
	{
		SELECTOR_SELECTION_CHANGED,		//selection changed inside Selector Widget that uses this controller as datasource
		SELECTOR_DATA_CHANGED,			//emitted when data is reloaded from cache/server/received global refresh signal
		SELECTOR_ON_INSERT,				//special use for selector->FUI comm
		SELECTOR_ON_DELETE,
		SELECTOR_ON_EDIT,
		SELECTOR_ON_VIEW,
		SELECTOR_ON_SHOW_DETAILS,			
		SELECTOR_ON_SHOW_DETAILS_NEW_WINDOW,			
		SELECTOR_ON_INSERT_NEW_FROM_TEMPLATE,
		SELECTOR_ON_INSERT_NEW_STRUCT_FROM_TEMPLATE,
		SELECTOR_ON_DOUBLE_CLICK,
		SELECTOR_ON_CONTENT_CHANGE,
		SELECTOR_ON_AFTER_OPEN_INSERT_HANDLER,
		SELECTOR_ON_AFTER_OPEN_EDIT_HANDLER,
		SELECTOR_ON_SHOW_COMM_GRID,
		SELECTOR_SELECTION_CLICKED,
		SELECTOR_ON_PROJECT_LIST_LOAD,
		SELECTOR_ON_PROJECT_LIST_CREATE,
		SELECTOR_ON_PROJECT_LIST_MODIFY,
		SELECTOR_ON_PROJECT_LIST_DELETE,
		SELECTOR_FIND_WIDGET_ENTER_PRESSED
	};
};


#endif // SELECTORSIGNALS_H