#include "selection_users.h"
#include "gui_core/gui_core/thememanager.h"
#include "bus_client/bus_client/selection_tablebased.h"
#include <QtWidgets/QMessageBox>
#include <QHeaderView>
#include "common/common/status.h"
#include "common/common/entity_id_collection.h"

Selection_Users::Selection_Users(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);
}

Selection_Users::~Selection_Users()
{

}


void Selection_Users::OnFindWidgetEnterPressed()
{
	DbRecordSet lstSelection;
	lstSelection.copyDefinition(m_lstData);
	lstSelection.merge(m_lstData,true);

	if(lstSelection.getRowCount()>0)
	{
		int nRecordID=lstSelection.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
		notifyObservers(SelectorSignals::SELECTOR_FIND_WIDGET_ENTER_PRESSED,nRecordID);
	}

}








//if skip loading, then data will not loaded
void Selection_Users::Initialize(int nEntityID,bool bSkipLoadingData,bool bIsFUISelectorWidget,bool bSingleClickIsSelection,bool bMultiSelection,bool bEnableDrag)
{

	ui.tableWidget->SetDropType(nEntityID);

	m_bIsFUISelectorWidget=bIsFUISelectorWidget;			//if FUI selector then this flag can be used to enable some features (Insert node, edit node, etc..)
	m_bSingleClickIsSelection=bSingleClickIsSelection;		//set click mode
	
	MainEntitySelectionController::Initialize(nEntityID);	//init data source

	//set tree drag icon:
	switch(nEntityID)
	{
	case ENTITY_BUS_PERSON:
		ui.tableWidget->SetDragIcon(":User32.png");
		break;
	}

	//----------------------INIT TABLE----------------------------------

	//connect signals from table widget:
	/*
	if(bSingleClickIsSelection)
	{
		connect(ui.tableWidget,SIGNAL(SignalSelectionChanged()),this,SLOT(SlotSelectionChanged()));  //MB requested key navigation, will trigger event twice, but...
	}
	else
		connect(ui.tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(SlotSelectionChanged()));
	*/
	
	connect(ui.tableWidget,SIGNAL(SignalSelectionChanged()),this,SLOT(SlotSelectionChanged()));  //MB requested key navigation, will trigger event twice, but...
	connect(ui.tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(SlotSelectionChanged_DoubleClicked()));

	
	//connect Reload from Context menu to our Reload slot
	connect(ui.tableWidget,SIGNAL(EmitReloadData()),this,SLOT(SlotReloadFromServer()));

	//setup Table widget:
	DbRecordSet columns;
	GetColumnSetup(columns);
	ui.tableWidget->Initialize(m_nEntityID,&m_lstData,&columns,bEnableDrag,bMultiSelection); 

	//----------------------INIT TABLE----------------------------------

	//set last column streched:
	ui.tableWidget->horizontalHeader()->setStretchLastSection(true);

	ui.Find_widget->Initialize(ui.tableWidget,ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR,QString("QFrame "+ThemeManager::GetSidebar_Bkg()+" "+"QLabel "+ThemeManager::GetSidebarActualName_Font()),2);
	connect(ui.Find_widget->m_pFindCombo,SIGNAL(EnterPressed()),this,SLOT(OnFindWidgetEnterPressed()));

	//issue 2709: block all users with ACTIVE_FLAG=0
	if (nEntityID==ENTITY_BUS_PERSON && !bIsFUISelectorWidget)
		SetFilter("BPER_ACTIVE_FLAG",1);

	//if allowed, load data right away:
	if( !bSkipLoadingData)
		ReloadData();
}



//returns id, code, name from row=0, if more then records-> stored in lstEntityRecord
void Selection_Users::GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord)
{
	ui.tableWidget->GetDropValue(lstEntityRecord);
	if(lstEntityRecord.getRowCount()>0)
	{
		if(m_nPrimaryKeyColumnIdx!=-1)nEntityRecordID=lstEntityRecord.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
		if(m_nCodeColumnIdx!=-1)strCode=lstEntityRecord.getDataRef(0,m_nCodeColumnIdx).toString();
		if(m_nNameColumnIdx!=-1)strName=lstEntityRecord.getDataRef(0,m_nNameColumnIdx).toString();
	}

}


//refresh content in table widget:
void Selection_Users::RefreshDisplay(int nAction, int nPrimaryKeyValue)
{
	if (nAction==REFRESH_ITEM_ADD)
	{
		ui.tableWidget->scrollToLastItem(true);
		return;
	}
	//else if(nAction==REFRESH_ITEM_DD)
	//	m_lstData.clearSelection();

	ui.tableWidget->RefreshDisplay(); 
}

void Selection_Users::SetFilter(QString strColumnName, int nColumnVal,int nPosition/*=-1*/)
{
	MainEntitySelectionController::GetLocalFilter()->SetFilter(strColumnName, nColumnVal, nPosition);
}

void Selection_Users::ReloadData()
{
	MainEntitySelectionController::ReloadData();
}



//-----------------------------------------------------------------
//			SLOTS
//-----------------------------------------------------------------

//invoked by cnxt menu: load data from server, store it inot cache, notify all observers
void Selection_Users::SlotReloadFromServer()
{
	ReloadFromServer();
}



//user selected cell with dbl click:
void Selection_Users::SlotSelectionChanged()
{
	DbRecordSet lstSelection;
	ui.tableWidget->GetDropValue(lstSelection);
	if(lstSelection.getRowCount()>0)
	{
		if(m_nPrimaryKeyColumnIdx!=-1) //for ACP'e pk can be=-1
		{
			int nRecordID=lstSelection.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
			notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID);
		}
	}

	emit SelectionChanged();
}



void Selection_Users::SlotSelectionChanged_DoubleClicked()
{
	SlotSelectionChanged();
	notifyObservers(SelectorSignals::SELECTOR_ON_DOUBLE_CLICK);
}


