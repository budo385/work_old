#include "bus_client/bus_client/selection_treebased.h"
#include "common/common/entity_id_collection.h"
#include "common/common/status.h"
#include "gui_core/gui_core/universaltreefindwidget.h"
#include "gui_core/gui_core/thememanager.h"
#include "storedprojlistsdlg.h"
#include "simpleselectionwizpage.h"
#include "storedprojlistseditor.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "bus_core/bus_core/accuarcore.h"
#include <time.h>

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;	//global message dispatcher
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;
//#include "common/common/cliententitycache.h"
//extern ClientEntityCache g_ClientCache;	
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

static int g_nCurrentStoredListID = -1;

Selection_TreeBased::Selection_TreeBased(QWidget *parent)
    : QFrame(parent)
{
	ui.setupUi(this);
	connect(ui.treeView, SIGNAL(ItemCheckStateChanged(QList<QTreeWidgetItem*>)), this, SLOT(on_treeView_ItemCheckStateChanged(QList<QTreeWidgetItem*>)));
}

Selection_TreeBased::~Selection_TreeBased()
{
}

void Selection_TreeBased::Initialize(int nEntityID,bool bSkipLoadingData,bool bIsFUISelectorWidget,bool bSingleClickIsSelection,bool bMultiSelection,bool bEnableDrag)
{
	ui.treeView->SetDropType(nEntityID);

	m_bIsFUISelectorWidget=bIsFUISelectorWidget;			//if FUI selector then this flag can be used to enable some features (Insert node, edit node, etc..)
	m_bSingleClickIsSelection=bSingleClickIsSelection;		//set click mode

	MainEntitySelectionController::Initialize(nEntityID);	//init data source

	//set tree drag icon:
	switch(nEntityID)
	{
	case ENTITY_BUS_PROJECT:
		ui.treeView->SetDragIcon(":Projects32.png");
		break;
	}

	RebuildProjectToolBar();


	//----------------------INIT TREE----------------------------------

	//create Cnxt Menu actions:
	QList<QAction*> lstActions; //all actions will be reparented
	QAction* pAction;
	QAction* SeparatorAct;

	pAction = new QAction(tr("&Reload"), this);
	pAction->setData(QVariant(true));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(SlotReloadFromServer()));
	lstActions.append(pAction);

	pAction = new QAction(tr("&Find"), this);
	//TOFIX waiting for Marko to implement this
	pAction->setEnabled(false);
	//connect(pAction, SIGNAL(triggered()), this, SLOT(find()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Show Details In New Window"), this);
	if(!bIsFUISelectorWidget)
		pAction->setVisible(false);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnDetailsSelectedEntry()));
	lstActions.append(pAction);

	//separator
	pAction = new QAction(this);
	pAction->setSeparator(true);
	pAction->setEnabled(false);
	lstActions.append(pAction);

	//Add & Edit only if in FUI
	if (m_bIsFUISelectorWidget)
	{
		//Add separator
		SeparatorAct = new QAction(this);
		SeparatorAct->setSeparator(true);
		lstActions.append(pAction);

		//Insert new row:
		pAction = new QAction(tr("&New"), this);
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnInsertNewEntry()));
		lstActions.append(pAction);

		pAction = new QAction(tr("New From Template"), this);
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnNewFromTemplate()));
		lstActions.append(pAction);

		pAction = new QAction(tr("New Substructure From Template"), this);
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnNewStructFromTemplate()));
		lstActions.append(pAction);

		if(ENTITY_BUS_PROJECT == nEntityID)
		{
			//separator
			pAction = new QAction(this);
			pAction->setSeparator(true);
			pAction->setEnabled(false);
			lstActions.append(pAction);

			pAction = new QAction(tr("Load Project List"), this);
			//pAction->setData(QVariant(false));	//means that is enabled only in edit mode
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnProjectListLoad()));
			lstActions.append(pAction);

			pAction = new QAction(tr("Create Project List"), this);
			//pAction->setData(QVariant(false));	//means that is enabled only in edit mode
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnProjectListCreate()));
			lstActions.append(pAction);

			pAction = new QAction(tr("Modify Project List"), this);
			//pAction->setData(QVariant(false));	//means that is enabled only in edit mode
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnProjectListModify()));
			lstActions.append(pAction);

			pAction = new QAction(tr("Delete Project List"), this);
			//pAction->setData(QVariant(false));	//means that is enabled only in edit mode
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnProjectListDelete()));
			lstActions.append(pAction);

			pAction = new QAction(tr("Hide Inactive Projects"), this);
			pAction->setCheckable(true);
			pAction->setChecked(true);
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnProjectHideShowInactive()));
			lstActions.append(pAction);
			m_pActionProjectHideInvisible = pAction;
		}

		//separator
		pAction = new QAction(this);
		pAction->setSeparator(true);
		pAction->setEnabled(false);
		lstActions.append(pAction);

		pAction = new QAction(tr("&Copy"), this);
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		pAction->setEnabled(false);	//TOFIX
		//connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteSelectedEntry()));
		lstActions.append(pAction);

		pAction = new QAction(tr("&Move"), this);
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		pAction->setEnabled(false);	//TOFIX
		//connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteSelectedEntry()));
		lstActions.append(pAction);

		pAction = new QAction(tr("&Edit"), this);
		pAction->setData(QVariant(false));	//means that is enabled only in non-edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnEditSelectedEntry()));
		lstActions.append(pAction);

		pAction = new QAction(tr("&Delete"), this);
		pAction->setData(QVariant(false));	//means that is enabled only in edit mode
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnDeleteSelectedEntry()));
		lstActions.append(pAction);
	}
	else
		ui.btnFromTemplate->setVisible(false);

	//ui.treeView->setColumnCount(1);
	ui.treeView->SetDropType(m_nEntityID);
	ui.treeView->SetHeader();
	//m_lstData.Dump();

	ui.treeView->Initialize(&m_lstData, false, bEnableDrag, !bMultiSelection); //init with data tree list
	ui.treeView->SetContextMenuActions(lstActions);      //set cntx menu
	//ui.treeView->setStyleSheet(ThemeManager::GetTreeBkg());

	//connect dbl click
	/*
	if(bSingleClickIsSelection) //connect selection changed
	{
		connect(ui.treeView,SIGNAL(SignalSelectionChanged()),this,SLOT(SlotSelectionChanged()));  //MB requested key navigation, will trigger event twice, but...
		//connect(ui.treeView,SIGNAL(itemClicked (QTreeWidgetItem*,int)),this,SLOT(SlotSelectionChanged())); 
	}
	else
		connect(ui.treeView,SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)),this,SLOT(SlotSelectionChanged())); 
	*/

	connect(ui.treeView,SIGNAL(SignalSelectionChanged()),this,SLOT(SlotSelectionChanged()));  //MB requested key navigation, will trigger event twice, but...
	connect(ui.treeView,SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)),this,SLOT(SlotSelectionChanged_DoubleClicked())); 

	//----------------------INIT TREE----------------------------------

	//if allowed, load data right away:

	//issue 2226
	if (nEntityID==ENTITY_BUS_PROJECT)
	{
		ui.treeView->EnableTrackExpandedState("BUSP_EXPANDED");
	}
	if ( nEntityID==ENTITY_CE_TYPES)
	{
		ui.treeView->EnableTrackExpandedState("CET_EXPANDED");
	}


	if( !bSkipLoadingData)
		ReloadData();

	//Find thing.
	ui.Find_widget->Initialize(ui.treeView,ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR,QString("QFrame "+ThemeManager::GetSidebar_Bkg()+" "+"QLabel "+ThemeManager::GetSidebarActualName_Font()));

	connect(ui.Find_widget->m_pFindCombo,SIGNAL(EnterPressed()),this,SLOT(OnFindWidgetEnterPressed()));
}

void Selection_TreeBased::GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord)
{
	ui.treeView->GetDropValue(lstEntityRecord);
	if(lstEntityRecord.getRowCount()>0)
	{
		if(m_nPrimaryKeyColumnIdx!=-1)nEntityRecordID=lstEntityRecord.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
		if(m_nCodeColumnIdx!=-1)strCode=lstEntityRecord.getDataRef(0,m_nCodeColumnIdx).toString();
		if(m_nNameColumnIdx!=-1)strName=lstEntityRecord.getDataRef(0,m_nNameColumnIdx).toString();
	}
}

void Selection_TreeBased::GetSelection(DbRecordSet& lstRecords)
{
	ui.treeView->GetDropValue(lstRecords);
}

void Selection_TreeBased::RefreshDisplay(int nAction, int nPrimaryKeyValue)
{
	//set tree drag icon:
	if (m_nEntityID== ENTITY_BUS_PROJECT)
	{
		m_lstData.sort(m_nCodeColumnIdx);
	}

	//always try to refresh to old nPrimaryKeyValue, if empty then all
	if(nPrimaryKeyValue>0)
	{
		if(ui.treeView->GetDataSource()->find(m_nPrimaryKeyColumnIdx,nPrimaryKeyValue,true)!=-1) //it is possible that it not exists, check it
		{
			ui.treeView->RefreshDisplay();
			ui.treeView->SelectItemByRowID(nPrimaryKeyValue);
			ui.treeView->ExpandItemByRowID(nPrimaryKeyValue);
			return;
		}
	}
	ui.treeView->RefreshDisplay();
}


//-----------------------------------------------------------------
//			SLOTS
//-----------------------------------------------------------------

//invoked by cnxt menu: load data from server, store it inot cache, notify all observers
void Selection_TreeBased::SlotReloadFromServer()
{
	ReloadFromServer();
}

void Selection_TreeBased::SlotSelectionChanged()
{
	DbRecordSet lstSelection;
	ui.treeView->GetDropValue(lstSelection);
	if(lstSelection.getRowCount()>0)
	{
		if(m_nPrimaryKeyColumnIdx!=-1) //for ACP'e pk can be=-1
		{
			int nRecordID=lstSelection.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
			notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID);
		}
	}
	//if dbl clik emit signal:
	//if(!m_bSingleClickIsSelection)
}

void Selection_TreeBased::on_treeView_ItemCheckStateChanged(QList<QTreeWidgetItem*> lstChangedCheckStateItems)
{
	emit ItemCheckStateChanged(lstChangedCheckStateItems);
}

void Selection_TreeBased::OnInsertNewEntry()
{
	int nRecordID;
	DbRecordSet lstSelection;
	QString strCode,strName;
	GetSelection(nRecordID,strCode,strName,lstSelection);
	notifyObservers(SelectorSignals::SELECTOR_ON_INSERT, nRecordID);
}

void Selection_TreeBased::OnNewFromTemplate()
{
	notifyObservers(SelectorSignals::SELECTOR_ON_INSERT_NEW_FROM_TEMPLATE, -1);
}

void Selection_TreeBased::OnNewStructFromTemplate()
{
	notifyObservers(SelectorSignals::SELECTOR_ON_INSERT_NEW_STRUCT_FROM_TEMPLATE, -1);
}

void Selection_TreeBased::OnDeleteSelectedEntry()
{
	int nRecordID;
	DbRecordSet lstSelection;
	QString strCode,strName;
	GetSelection(nRecordID,strCode,strName,lstSelection);
	notifyObservers(SelectorSignals::SELECTOR_ON_DELETE, nRecordID);
}

void Selection_TreeBased::OnEditSelectedEntry()
{
	int nRecordID;
	DbRecordSet lstSelection;
	QString strCode,strName;
	GetSelection(nRecordID,strCode,strName,lstSelection);
	notifyObservers(SelectorSignals::SELECTOR_ON_EDIT, nRecordID);
}

void Selection_TreeBased::OnDetailsSelectedEntry()
{
	int nRecordID;
	DbRecordSet lstSelection;
	QString strCode,strName;
	GetSelection(nRecordID,strCode,strName,lstSelection);
	notifyObservers(SelectorSignals::SELECTOR_ON_SHOW_DETAILS_NEW_WINDOW, nRecordID);
}

//call this to expand and set selection to specified RecordID, must be inside list:
void Selection_TreeBased::SetCurrentEntityRecord(int nEntityRecordID,bool bSkipNotifyObservers)
{
	m_lstData.find(m_nPrimaryKeyColumnIdx,nEntityRecordID); //select it

	if (m_lstData.getSelectedCount()==0) //whole tree must be reloaded: special case for HIER entities: see notifyCacheAfterWrite inside fuibase
	{
		if (nEntityRecordID>0) //if id is valid, and not found: try to reload all entity from server (can be after project/hierarhical insert when no cache is updated)
		{
			ReloadFromServer();
			int nRow=m_lstData.find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
			if(nRow!=-1)
			{
				m_lstData.selectRow(nRow);
				RefreshDisplay();
				ui.treeView->ExpandItemByRowID(nEntityRecordID);
				if (!bSkipNotifyObservers)
					notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nEntityRecordID);
			}
		}
		return;
	}

	ui.treeView->RefreshDisplay(nEntityRecordID);			//if selected
	ui.treeView->ExpandItemByRowID(nEntityRecordID);
	if (!bSkipNotifyObservers)
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nEntityRecordID);
}

void Selection_TreeBased::SlotSelectionChanged_DoubleClicked()
{
	SlotSelectionChanged();
	notifyObservers(SelectorSignals::SELECTOR_ON_DOUBLE_CLICK,0);
}

//issue: 1251
void Selection_TreeBased::SetSideBarVisible(bool bVisible)
{
	ui.treeView->blockSignals(true);
	ui.treeView->setVisible(bVisible);
	ui.treeView->blockSignals(false);
}

bool Selection_TreeBased::IsSideBarVisible()
{
	return ui.treeView->isVisible();

}

void Selection_TreeBased::SetFocusOnFind()
{
	ui.Find_widget->m_pFindCombo->setFocus();
}

void Selection_TreeBased::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		ui.treeView->setStyleSheet(ThemeManager::GetTreeBkg());
		return;
	}
	MainEntitySelectionController::updateObserver(pSubject,nMsgCode,nMsgDetail,val);
}

void Selection_TreeBased::LoadStoredList(int nListID)
{
	Status status;
	QApplication::setOverrideCursor(Qt::WaitCursor);

	time_t tStart = time(NULL);

	bool bIsSelection = false;
	QString strSelectionXML;
	bool bIsFilter = false;
	QString strFilterXML;
	bool bGetDescOnly = false;
	_SERVER_CALL(StoredProjectLists->GetListData(status, nListID, m_lstData, bIsSelection, strSelectionXML, bIsFilter, strFilterXML, bGetDescOnly));

	//_DUMP(m_lstData);

	time_t tEnd = time(NULL);
	qDebug() << "Project list data loaded in (sec): " << (tEnd-tStart) << ", item count:" << m_lstData.getRowCount();
	//QMessageBox::information(NULL, "Info", QString("Project list data loaded in (sec): %1").arg(tEnd-tStart));

	//m_lstData.Dump();
	QApplication::restoreOverrideCursor();
	_CHK_ERR_NO_RET(status);
		
	if (status.IsOK())
	{
		RefreshDisplay();
		notifyObservers(SelectorSignals::SELECTOR_DATA_CHANGED);	//emit signal that data is changed in this controller

		g_nCurrentStoredListID = nListID; //store info
	}

}

void Selection_TreeBased::OnProjectListLoad()
{
	if(!g_AccessRight->TestAR(LOAD_PROJECT_LISTS)){
		QMessageBox::information(NULL, "Warning", tr("You don't have the rights to perform this operation!"));
		return;
	}

	//fix storage for the current stored project list
	//to include the ID of the list loaded on startup
	if(g_nCurrentStoredListID < 0){
		int nDefList = g_pClientManager->GetPersonProjectListID();
		if(nDefList > 0)
			g_nCurrentStoredListID = nDefList;
	}

	StoredProjListsDlg dlg(g_nCurrentStoredListID);
	if(dlg.exec())
	{
		int nListID = dlg.GetSelectedListID();
		LoadStoredList(nListID);
	}
}

void Selection_TreeBased::OnProjectListCreate()
{
	//test MP's AR
	bool bLoadListAR = g_AccessRight->TestAR(LOAD_PROJECT_LISTS);
	bool bAccPrivateModify = g_AccessRight->TestAR(ENTER_MODIFY_PRIVATE_PROJECT_LISTS);
	bool bAccPublicModify = g_AccessRight->TestAR(ENTER_MODIFY_PUBLIC_PROJECT_LISTS);
	if(!bLoadListAR || (!bAccPrivateModify && !bAccPublicModify)){
		QMessageBox::information(NULL, "Warning", tr("You don't have the rights to perform this operation!"));
		return;
	}
	if (bAccPublicModify)
		bAccPrivateModify=false; //if public is enabled then override private, set to false->avoid, else look

	//TEST AR:
	Status err;
	if(!g_AccessRight->IsOperationAllowed(err, BUS_STORED_PROJECT_LIST, UserAccessRight::OP_INSERT))
	{
		_CHK_ERR(err);
	}

	StoredProjListsEditor wiz(-1);

	if(bAccPrivateModify){
		//can not modify preser UAR - #1939
		if (wiz.GetAccUserRecordSelector())
		{
			wiz.GetAccUserRecordSelector()->SetAccessDisplayLevel(AccUARCore::ACC_PRIVATE);
			wiz.GetAccUserRecordSelector()->SetEditMode(false);
		}
	}

	//If wizard accepted.
	int nListID = -1; // new list
	if (wiz.exec())
	{
		//get List name
		QString strName = wiz.GetListName();
		DbRecordSet lstData;

		// handle SPL type
		bool bIsSelection = false;
		bool bIsFilter = false;
		QString strSelectionXML;
		QString strFilterXML;
		int nProjlistType = wiz.GetProjListType();

		if(nProjlistType == PAGE_GROUP_IDX)
		{
			wiz.GetTreeData(lstData); // for old SPL types (group)
			if (lstData.getRowCount()==0)
				return;

			//MP comment: lstData.setColValue("BSPL_OWNER",g_pClientManager->GetPersonID()); //set to id of logged person;
		}
		else if(nProjlistType == PAGE_SELECTIONS_IDX)
		{
			bIsSelection = true;
			strSelectionXML = wiz.GetProjListXML();
		}
		else if(nProjlistType == PAGE_COLLECTIONS_IDX)
		{
			bIsFilter = true;
			strFilterXML = wiz.GetProjListXML();
		}
		
		QApplication::setOverrideCursor(Qt::WaitCursor);
		Status status;
		
		_SERVER_CALL(StoredProjectLists->CreateList(status, strName, lstData, nListID, bIsSelection, strSelectionXML, bIsFilter, strFilterXML));
		_CHK_ERR_NO_RET(status);
		QApplication::restoreOverrideCursor();
		
		// save uarwidget
		if (status.IsOK())
		{
			if (wiz.GetAccUserRecordSelector())
				wiz.GetAccUserRecordSelector()->Save(nListID, BUS_STORED_PROJECT_LIST);
		}
			
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_STORED_PROJECT_LISTS_UPDATED, 0,QVariant(),this); //propagate change to all	

		//#2731 load the SPL immediately
		LoadStoredList(nListID);
	}
	LoadStoredProjectLists();
	SetSelectionOnView(nListID);
}

void Selection_TreeBased::OnProjectListModify()
{
	//test MP's AR
	bool bLoadListAR = g_AccessRight->TestAR(LOAD_PROJECT_LISTS);
	bool bAccPrivateModify = g_AccessRight->TestAR(ENTER_MODIFY_PRIVATE_PROJECT_LISTS);
	bool bAccPublicModify = g_AccessRight->TestAR(ENTER_MODIFY_PUBLIC_PROJECT_LISTS);
	if(!bLoadListAR || (!bAccPrivateModify && !bAccPublicModify)){
		QMessageBox::information(NULL, "Warning", tr("You don't have the rights to perform this operation!"));
		return;
	}

	//fix storage for the current stored project list
	//to include the ID of the list loaded on startup
	if(g_nCurrentStoredListID < 0){
		int nDefList = g_pClientManager->GetPersonProjectListID();
		if(nDefList > 0)
			g_nCurrentStoredListID = nDefList;
	}

	StoredProjListsDlg dlg(g_nCurrentStoredListID);
	if(dlg.exec())
	{
		int nListID = dlg.GetSelectedListID();
		QString strName = dlg.GetSelectedName();

		//TEST AR:
		Status err;
		if(!g_AccessRight->IsOperationAllowed(err, BUS_STORED_PROJECT_LIST,UserAccessRight::OP_EDIT, nListID)){
			_CHK_ERR(err);
		}
		
		StoredProjListsEditor wiz(nListID);
		wiz.SetListName(strName);
		wiz.DisableNameEdit();

		//If wizard accepted.
		if (wiz.exec()){
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_STORED_PROJECT_LISTS_UPDATED, 0,QVariant(),this); //propagate change to all

			//#2731 load the SPL immediately
			LoadStoredList(nListID);
		}
	}
	LoadStoredProjectLists();
}

void Selection_TreeBased::OnProjectListDelete()
{
	bool bAccPrivateModify = g_AccessRight->TestAR(ENTER_MODIFY_PRIVATE_PROJECT_LISTS);
	bool bAccPublicModify = g_AccessRight->TestAR(ENTER_MODIFY_PUBLIC_PROJECT_LISTS);
	if(!bAccPrivateModify && !bAccPublicModify){
		QMessageBox::information(NULL, "Warning", tr("You don't have the rights to perform this operation!"));
		return;
	}

	//fix storage for the current stored project list
	//to include the ID of the list loaded on startup
	if(g_nCurrentStoredListID < 0){
		int nDefList = g_pClientManager->GetPersonProjectListID();
		if(nDefList > 0)
			g_nCurrentStoredListID = nDefList;
	}

	StoredProjListsDlg dlg(g_nCurrentStoredListID);
	if(dlg.exec())
	{
		int nListID = dlg.GetSelectedListID();

		//TEST AR:
		Status err;
		if(!g_AccessRight->IsOperationAllowed(err, BUS_STORED_PROJECT_LIST,UserAccessRight::OP_DELETE, nListID))
		{
			_CHK_ERR(err);
		}

		Status status;
		_SERVER_CALL(StoredProjectLists->DeleteList(status, nListID));
		_CHK_ERR(status);

		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_STORED_PROJECT_LISTS_UPDATED, 0,QVariant(),this); //propagate change to all	
	}
	LoadStoredProjectLists();
}

void Selection_TreeBased::OnProjectHideShowInactive()
{
	m_bProjectHideInvisible = !m_bProjectHideInvisible;
	if(m_pActionProjectHideInvisible)
		m_pActionProjectHideInvisible->setChecked(m_bProjectHideInvisible);

	//reload project tree with new flag setting
	QApplication::setOverrideCursor(Qt::WaitCursor);
	ReloadData(true);
	QApplication::restoreOverrideCursor();
}

void Selection_TreeBased::HideProjectToolBar()
{
	ui.paneProjectToolBar->setVisible(false);
}

void Selection_TreeBased::RebuildProjectToolBar()
{
	if (m_nEntityID!=ENTITY_BUS_PROJECT)
	{
		ui.paneProjectToolBar->setVisible(false);
	}
	else
	{
		//set icons:
		ui.btnModify->setIcon(QIcon(":SAP_Modify.png"));
		//ui.btnSelect->setIcon(QIcon(":Select16.png"));
		ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));
		ui.btnInsert->setIcon(QIcon(":handwrite.png"));
		ui.btnModify->setToolTip(tr("Modify Project List"));
		//ui.btnSelect->setToolTip(tr("Load Project List"));
		ui.btnRemove->setToolTip(tr("Delete Project List"));
		ui.btnInsert->setToolTip(tr("Create Project List"));

		ui.btnFromTemplate->setIcon(QIcon(":Icon_ProjectFromTempl16.png"));
		ui.btnFromTemplate->setToolTip(tr("New From Template"));

		//nafiluj je s stored project listama:
		LoadStoredProjectLists();
		ui.projectList->setStyleSheet(ThemeManager::GetLisViewBkg());
		connect(ui.projectList,SIGNAL(itemSelectionChanged()),this,SLOT(OnProjectListChanged()));
		ui.projectList->setSelectionMode(QAbstractItemView::SingleSelection);

		QList<int> list;
		list<<130<<400;
		ui.splitter->setSizes(list);

	}
}

void Selection_TreeBased::on_btnModify_clicked()
{
	OnProjectListModify();
}

void Selection_TreeBased::on_btnRemove_clicked()
{
	OnProjectListDelete();
}

void Selection_TreeBased::on_btnSelect_clicked()
{
	OnProjectListLoad();
}

void Selection_TreeBased::on_btnInsert_clicked()
{
	OnProjectListCreate();
}

void Selection_TreeBased::on_btnFromTemplate_clicked()
{
	OnNewFromTemplate();
}

void Selection_TreeBased::OnFindWidgetEnterPressed()
{
	DbRecordSet lstSelection;
	ui.treeView->GetDropValue(lstSelection);
	if(lstSelection.getRowCount()>0)
	{
		if(m_nPrimaryKeyColumnIdx!=-1) //for ACP'e pk can be=-1
		{
			int nRecordID=lstSelection.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
			notifyObservers(SelectorSignals::SELECTOR_FIND_WIDGET_ENTER_PRESSED,nRecordID);
		}
	}

}

//load stored list and prevent signal firing: none is selected at start..
void Selection_TreeBased::LoadStoredProjectLists()
{
	ui.projectList->blockSignals(true);
	ui.projectList->clear();
	
	Status status;
	DbRecordSet recLists;
	_SERVER_CALL(StoredProjectLists->GetListNames(status, recLists /*,g_pClientManager->GetPersonID()*/)) //Project grid id
	_CHK_ERR(status);
	recLists.sort("BSPL_NAME");

	//Add ALL at start:
	QListWidgetItem *pItem = new QListWidgetItem(tr("All"), ui.projectList);
	pItem->setData(Qt::UserRole,0);

	//Fill list:
	int nRowCount = recLists.getRowCount();
	for(int i = 0; i < nRowCount; i++)
	{
		int ViewID = recLists.getDataRef(i, "BSPL_ID").toInt();
		QString strViewName = recLists.getDataRef(i, "BSPL_NAME").toString();

		QListWidgetItem *pItem = new QListWidgetItem(strViewName, ui.projectList);
		pItem->setData(Qt::UserRole,ViewID);
	}
	ui.projectList->blockSignals(false);

}

void Selection_TreeBased::OnProjectListChanged()
{
	//get selected item, then based on id, load project list:
	QList<QListWidgetItem *> lstSelected = ui.projectList->selectedItems();
	if (lstSelected.count()>0)
	{
		int nViewID=lstSelected.at(0)->data(Qt::UserRole).toInt();
		if(nViewID!=0)
			LoadStoredList(nViewID);
		else
			ReloadFromServer(); //nothing selected, reload whole project tree..
	}
	else
	{
		ReloadFromServer(); //nothing selected, reload whole project tree..
	}
	
}

//set selection on view but disable propagation of item changed signal..
void Selection_TreeBased::SetSelectionOnView(int nViewID)
{
	ui.projectList->blockSignals(true);

	int nRowCount = ui.projectList->count();
	for(int i = 0; i < nRowCount; i++)
	{
		if (ui.projectList->item(i)->data(Qt::UserRole).toInt()==nViewID)
		{
			ui.projectList->setCurrentRow(i);
			break;
		}
	}
	
	ui.projectList->blockSignals(false);
	
}
