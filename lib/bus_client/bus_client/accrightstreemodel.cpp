#include "accrightstreemodel.h"
#include "common/common/status.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
#include "bus_client/bus_client/servermessagehandlerset.h"
#include "trans/trans/httpclientconnectionsettings.h"
extern BusinessServiceManager *g_pBoSet;

AccRightsTreeModel::AccRightsTreeModel(QObject *Parent /*= NULL*/)
	: DbRecordSetModel(Parent)
{

}

AccRightsTreeModel::~AccRightsTreeModel()
{

}

/*!
Setup model data from recordset.

\param Parent	 - Parent item.
*/
void AccRightsTreeModel::SetupModelData(DbRecordSetItem *Parent /*= NULL*/)
{
	//Parents per column hash.
	QMultiHash<int, DbRecordSetItem*> Parents;		

	//Check are we building from scratch or just some node.
	Parents.insert(0, m_pRootItem);

	int row_count = m_pDbRecordSet->getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Set some data to display.
		QList<QString> data;
		QString FirstName = m_pDbRecordSet->getDataRef(row, 1).toString();
		QString LastName  = m_pDbRecordSet->getDataRef(row, 2).toString();
		QString ActiveFlag= m_pDbRecordSet->getDataRef(row, 3).toString();
		data << FirstName << LastName << ActiveFlag;

		int RowId	 = m_pDbRecordSet->getDataRef(row, 0).toInt();

		//First separate icons.
		QStringList IconList;
		IconList << QString("2");

		//Create new item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, 1, m_pRootItem);
		//Insert it in parents hash (key value is record id).
		Parents.insert(RowId, item);
		//Append item to it's parent.
		m_pRootItem->appendChild(item);
		//Finally append to model item list.
		m_hshModelItems.insert(RowId, item);
	}
	//Clear recordset.
	m_pDbRecordSet->clear();
}

/*!
Add new data (items) to model.

\param PersonRolesRecordSet	 - recordset with role items.
*/
void AccRightsTreeModel::AddDataToModel(DbRecordSet &PersonRolesRecordSet)
{
	//Third column is parent id and is always the same for whole recordset.
	int ParentRowId = PersonRolesRecordSet.getDataRef(0, 3).toInt();
	DbRecordSetItem *ParentItem = m_hshModelItems.value(ParentRowId);

	int row_count = PersonRolesRecordSet.getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Set some data to display.
		QList<QString> data;
		QString Name = PersonRolesRecordSet.getDataRef(row, 1).toString();
		QString Descr = PersonRolesRecordSet.getDataRef(row, 2).toString();
		data << Name << Descr;
		int RowId	 = PersonRolesRecordSet.getDataRef(row, 0).toInt();
		
		//Role type (business or system).
		int RoleType = m_pDbRecordSet->getDataRef(row, 4).toInt();
		//Add some Icon.
		QStringList IconList;
		if (RoleType == 1)
			IconList << QString("1");
		else
			IconList << QString("3");

		//Create new item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, 0, ParentItem);
		//Append item to it's parent.
		ParentItem->appendChild(item);
	}
}

QVariant AccRightsTreeModel::data(const QModelIndex &index, int role) const
{
	//If index not valid (root item), go out.
	if (!index.isValid())
		return QVariant();

	//Get item pointer for later.
	DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());

	//Icon setting.
	if (role == Qt::DecorationRole)
	{
		QStringList IconList = item->GetIcons();
		int IconCount = IconList.count();

		if (IconCount <= 1)
		{
			QString Icon_resource = IconList.value(0);
			Icon_resource.prepend(":");
			QIcon icon;
			icon.addFile(Icon_resource);
			return QVariant(icon);
		}
		else //Add multiple icons.
		{
			//Calculate new icon length (16 pixels * number of icons.)
			int IconWidth = IconCount*16;
			QSize IconSize(IconWidth, 16); 
			//Make pixmap of future icon.
			QPixmap IconPixmap(IconSize);
			//Fill with white (alpha 0-transparent).
			IconPixmap.fill(QColor(255,255,255,0));
			//Make painter.
			QPainter painter(&IconPixmap);

			for (int i = 0; i < IconCount; i++)
			{
				//Get icon from icon list.
				QString Icon_resource = IconList.value(i);
				//Make it visible to resources.
				Icon_resource.prepend(":");

				QPixmap CurentIcon(Icon_resource);

				painter.drawPixmap(i*16, 0,CurentIcon);
			}

			QIcon icon(IconPixmap);
			return QVariant(icon);
		}
	}
	//Font decoration.
	if (role == Qt::FontRole)
	{
		QFont font;
		font.setStyle(QFont::StyleItalic);
		font.setFamily("Helvetica");
		font.setBold(true);
		font.setPointSize(10);
		return QVariant(font);
	}
	//Activated or not (font color).
	if (role == Qt::TextColorRole && (item->data(2).toString() == "0" || item->parent()->data(2).toString() == "0"))
	{
		QColor color(Qt::gray);
		return QVariant(color);
	}
	//Checked or not.
	if (role == Qt::CheckStateRole)
	{
		return QVariant();
		//		return QVariant(Qt::PartiallyChecked);
		//		return QVariant(Qt::PartiallyChecked);
	}
	//Display role.
	if (role != Qt::DisplayRole)
	{
		return QVariant();
	}

	//Return data (First name and last name).
	QString FirstName = item->data(0).toString();
	QString LastName = item->data(1).toString();
	QString DisplayData = FirstName + " " + LastName;

	return QVariant(DisplayData);
}

Qt::ItemFlags AccRightsTreeModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;

	Qt::ItemFlags DefaultFlags = QAbstractItemModel::flags(index);

	if (index.parent().isValid())
		return Qt::ItemIsDragEnabled | DefaultFlags;
	else
		return Qt::ItemIsDropEnabled | DefaultFlags; //| Qt::ItemIsEditable - BEFORE IT WAS (cont.menu).
}

bool AccRightsTreeModel::setData (const QModelIndex &index, const QVariant &value /*For this model this is just a dummy*/, 
								  int role /*= Qt::EditRole*/)
{
	//Rename and stuf.
	if (index.isValid() && role == Qt::EditRole) 
	{
		/*
		//Get item.
		DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());
		int ItemRowID = item->GetRowId();

		//Call BO.
		Status status;

		//If user.
		if (m_nPersonUserID == -100)
		{
			_SERVER_CALL(AccessRights->RenameUser(status, ItemRowID, m_strFirstName, m_strLastName))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "User renamed.";
		}
		//If person.
		/*
		else
		{
			_SERVER_CALL(AccessRights->RenamePerson(status, ItemRowID, m_strFirstName, m_strLastName))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "Person renamed.";
		}
		*/
		/*
		//Change item data.
		item->SetData(0, m_strFirstName);
		item->SetData(1, m_strLastName);
		emit dataChanged(index, index);*/
		return true;
	}
	//Activate/Deactivate.
	else if (index.isValid() && role == Qt::UserRole) 
	{
		//Get item.
		DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());
		int ItemRowID = item->GetRowId();

		//Call BO.
		Status status;

		//If user.
		if (m_nPersonUserID == -100)
		{
			//If Activate (first check is it already active).
			//User activate
			if (value.toString() == "1" && item->data(2).toString() == "0")
			{
				/*	For monthly cleanup.
				_SERVER_CALL(AccessRights->ActivateUser(status, ItemRowID))
				if(!status.IsOK())
				{
					qDebug() << status.getErrorText();
					return false;
				}
				else
					qDebug() << "User activated.";
					*/
			}
			//User deactivate
			else if (value.toString() == "0" && item->data(2).toString() == "1")
			{
				/*	For monthly cleanup.
				_SERVER_CALL(AccessRights->DeActivateUser(status, ItemRowID))
				if(!status.IsOK())
				{
					qDebug() << status.getErrorText();
					return false;
				}
				else
					qDebug() << "User deactivated.";
					*/
			}
			else
				return false;
		}
		//If person.
		else
		{
			//If Activate (first check is it already active).
			//Person activate
			if (value.toString() == "1" && item->data(2).toString() == "0")
			{
				/*
				_SERVER_CALL(AccessRights->ActivatePerson(status, ItemRowID))
				*/
				if(!status.IsOK())
				{
					qDebug() << status.getErrorText();
					return false;
				}
				else
					qDebug() << "Person activated.";
			}
			//Person deactivate
			else if (value.toString() == "0" && item->data(2).toString() == "1")
			{
				/*
				_SERVER_CALL(AccessRights->DeActivatePerson(status, ItemRowID))
				*/
				if(!status.IsOK())
				{
					qDebug() << status.getErrorText();
					return false;
				}
				else
					qDebug() << "Person deactivated.";
			}
			else
				return false;
		}

		item->SetData(2, value);
		emit dataChanged(index, index.sibling(index.row()+1, 0));
		return true;
	}


	return false;
}

Qt::DropActions AccRightsTreeModel::supportedDropActions() const
{
	return Qt::CopyAction;
}

bool AccRightsTreeModel::insertRows(int position, int rows, const QModelIndex &parent)
{
	//Person or user adding
	if (!parent.isValid())
	{
		int RowID = 0;
		Status status;

		//Add new User (m_nPersonUserID = -100).
		if (m_nPersonUserID == -100)
		{
			/*
			_SERVER_CALL(AccessRights->InsertNewUser(status, RowID, m_strFirstName, m_strLastName))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "User inserted.";
				*/
		}
		//Add new Person.
		/*
		else
		{
			_SERVER_CALL(AccessRights->InsertNewPerson(status, RowID, m_strFirstName, m_strLastName, m_nPersonUserID))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "Person inserted.";
		}
		*/

		beginInsertRows(QModelIndex(), position, position+rows-1);
		QList<QString> data;
		data << m_strFirstName << m_strLastName << "1";
		QStringList IconList;
		IconList << QString("2");

		DbRecordSetItem *item = new DbRecordSetItem(RowID, data, IconList,/*Item state*/ 0, m_pRootItem);
		m_pRootItem->appendChild(item);

		endInsertRows();
	}
	//Role adding
	else
	{
		Q_ASSERT(m_pDropedItem);

		DbRecordSetItem *ParentItem = static_cast<DbRecordSetItem*>(parent.internalPointer());
		DbRecordSetItem *item = new DbRecordSetItem(*m_pDropedItem);

		int PersonRowID = ParentItem->GetRowId();
		int RoleID		= item->GetRowId();

		Status status;
		//User role.
		if (m_nPersonUserID == -100)
		{
			_SERVER_CALL(AccessRights->InsertNewRoleToUser(status, PersonRowID, RoleID))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "Role inserted.";
		}
		//Person role
		else
		{
			_SERVER_CALL(AccessRights->InsertNewRoleToPerson(status, PersonRowID, RoleID))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "Role inserted.";
		}

		beginInsertRows(parent, position, position+rows-1);
		
		item->SetParent(ParentItem);
		ParentItem->appendChild(item);

		endInsertRows();
	}
		
	return true;
}

bool AccRightsTreeModel::removeRows(int position, int rows, const QModelIndex &index)
{
	DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());
	int ItemRowID = item->GetRowId();

	//Delete Persons or Users.
	if (!index.parent().isValid())
	{
		/*
		Status status;
		//If user.
		if (m_nPersonUserID == -100)
		{
			//For monthly cleanup - we don't delete users from here any more.
			_SERVER_CALL(AccessRights->DeleteUser(status, ItemRowID))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "User deleted.";

		}
		//If person.
		else
		{
			_SERVER_CALL(AccessRights->DeletePerson(status, ItemRowID))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "Person deleted.";
		}

		beginRemoveRows(QModelIndex(), position, position+rows-1);
		m_pRootItem->DeleteChild(item);
		endRemoveRows();
		*/
		return true;
	}
	//Delete roles.
	else
	{
		DbRecordSetItem *ItemParent = static_cast<DbRecordSetItem*>(index.parent().internalPointer());
		int ItemParentRowID = ItemParent->GetRowId();

		Status status;
		//If User.
		if (m_nPersonUserID == -100)
		{
			_SERVER_CALL(AccessRights->DeleteRoleFromUser(status, ItemParentRowID, ItemRowID))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "Role deleted.";
		}
		//else If person.
		else
		{
			_SERVER_CALL(AccessRights->DeleteRoleFromPerson(status, ItemParentRowID, ItemRowID))
			if(!status.IsOK())
			{
				qDebug() << status.getErrorText();
				return false;
			}
			else
				qDebug() << "Role deleted.";
		}

		beginRemoveRows(index.parent(), position, position+rows-1);
		ItemParent->DeleteChild(item);
		endRemoveRows();
	}
	
	return true;
}

bool AccRightsTreeModel::dropMimeData(const QMimeData * data, Qt::DropAction action, int row, 
				  int column, const QModelIndex & parent)
{
	insertRows(row,1, parent);
	return true;
}

QStringList AccRightsTreeModel::mimeTypes() const
{
	QStringList types;
	types << "helix/roleitem";
	return types;
}

/*!
Add new Person.

\param PersonName		- Person name.
\param PersonLastName	- Person last name.
\param PersonUserID		- Person user id.
*/
void AccRightsTreeModel::AddNewPerson(QString PersonName, QString PersonLastName, int PersonUserID)
{
	m_strFirstName		= PersonName;
	m_strLastName		= PersonLastName;
	m_nPersonUserID		= PersonUserID;

	insertRows(1, 1);
}

/*!
Add new User.

\param UserName		- Person name.
\param UserLastName	- Person last name.
*/
void AccRightsTreeModel::AddNewUser(QString UserName, QString UserLastName)
{
	m_strFirstName		= UserName;
	m_strLastName		= UserLastName;
	m_nPersonUserID		= -100;	//This means we are adding User not a Person.
	
	insertRows(1, 1);
}

void AccRightsTreeModel::SetPersonFirstName(const QString &FirstName)
{
	m_strFirstName = FirstName;
}
void AccRightsTreeModel::SetPersonLastName(const QString &LastName)
{
	m_strLastName = LastName;
}

void AccRightsTreeModel::SetPersonUserID(const int nUserID)
{
	m_nPersonUserID = nUserID;
}
