#ifndef CHANGEMANAGER_H
#define CHANGEMANAGER_H


#include "common/common/observer_ptrn.h"


/*!
	\class  ChangeManager
	\brief  Global observer object, receives  messages and dispatch them to registered observers
	\ingroup Bus_Client

	To enable that receiver and sender does not know for each other this global object is used.
	E.G. When Person data is changed in one FUI, this change can propagate through all selection controllers.


	Use:
	- if sending message, register message here in enumerator, notify all observers
	- if receivieng, register and filter only messageses of interest:

	Note:
	- mainselection controller automatically uses this

*/

class ChangeManager : public ObsrPtrn_Subject
{
public:
    ChangeManager();
    ~ChangeManager();

	enum GlobalMessages
	{
		// signals to all to refresh display:
		GLOBAL_REFRESH_ENTITY_INSERTED,
		GLOBAL_REFRESH_ENTITY_DELETED,
		GLOBAL_REFRESH_ENTITY_EDITED,
		GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD,		//special use for HCT entities: reload whole tree
									
		FUI_CHANGED_NAME,					//FUI to global combo box with FUI name:
		FUI_ACTUAL_ORGANIZATION_CHANGED,	//Propagate Actual Org. change in all FUI's
		
		GLOBAL_DISPLAY_STATUS_TEXT,			//write text on status bar
		GLOBAL_CHECK_OUT_DOC,				//DM manager
		GLOBAL_CHECK_IN_DOC,				//DM manager

		GLOBAL_MENU_HEADER,				//Appearance
		GLOBAL_MENU_STATUSBAR,			//Appearance
		GLOBAL_MENU_FULLSCREEN,			//Appearance
		GLOBAL_SIDEBAR_FUI_CONTACT_ACTIVE,
		GLOBAL_SIDEBAR_FUI_PROJECT_ACTIVE,
		GLOBAL_QCW_WINDOW_FINISH_PROCESS,
		GLOBAL_SUBSCRIPTION_TIMER_OUTLOOK_CHANGED,
		GLOBAL_SUBSCRIPTION_TIMER_THUNDERBIRD_CHANGED,
		GLOBAL_THEME_CHANGED,
		//GLOBAL_VIEW_CHANGED,
		//GLOBAL_VIEW_SHOW_MAIN_WINDOW,
		//GLOBAL_VIEW_SHOW_SIDEBAR_WINDOW,
		//GLOBAL_VIEW_CLOSE_MAIN_WINDOW,
		//GLOBAL_VIEW_CLOSE_SIDEBAR_WINDOW,
		GLOBAL_DOCUMENT_OPEN_FUI,
		GLOBAL_DOCUMENT_OPEN_FUI_SIMPLE_MODE,
		GLOBAL_CONTACT_FAVORITE_ADD,
		GLOBAL_CONTACT_FAVORITE_REMOVE,
		GLOBAL_CONTACT_FAVORITE_ADD_PICTURE,
		GLOBAL_DOCUMENT_CHECK_OUT_AND_OPEN,
		GLOBAL_DOCUMENT_OPEN_FUI_SIMPLE_MODE_AFTER_UPLOAD,

		GLOBAL_STORED_PROJECT_LISTS_UPDATED,
		GLOBAL_CE_MENU_MAKE_PHONE_CALL,
		GLOBAL_CE_MENU_WRITE_EMAIL,
		GLOBAL_CE_MENU_OPEN_WEBSITE,

		GLOBAL_CALENDAR_VIEW_LIST_RELOAD,
		GLOBAL_REFRESH_CALENDAR_ENTITY_INSERTED_AFTER_DRAG_DROP,
		GLOBAL_REFRESH_GROUP_DATA_INSIDE_CONTACT_DETAILS
	};

private:
    
};

#endif // CHANGEMANAGER_H
