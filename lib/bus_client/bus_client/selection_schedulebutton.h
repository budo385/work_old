#ifndef SELECTION_SCHEDULEBUTTON_H
#define SELECTION_SCHEDULEBUTTON_H

#include <QPushButton>
#include "gui_core/gui_core/pie_menu/qtpiemenu.h"

/*!
	\class  Selection_ScheduleButton
	\brief  Implements universal PIE menu for all day long use
	\ingroup Bus_Client
*/

class Selection_ScheduleButton : public QPushButton
{
	Q_OBJECT

public:
	Selection_ScheduleButton(QWidget *parent);
	~Selection_ScheduleButton();

private slots:
	void OnClick();
	
	//main:
	void OnSendMail();
	void OnPhoneCall();
	void OnDocument(){};

	//DOC:
	void OnLocalFile();
	void OnInternetFile();
	void OnNote();
	void OnAddress();
	void OnPaperDoc();

signals:
	void OnScheduleCall();
	void OnScheduleEmail();
	void OnScheduleDoc(int nDocType);

private:
	void contextMenuEvent(QContextMenuEvent *event);
	QtPieMenu *m_pPieMenu;
	QtPieMenu *m_pPieMenuDoc;

};

#endif // SELECTION_SCHEDULEBUTTON_H
