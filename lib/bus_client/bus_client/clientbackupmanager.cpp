#include "clientbackupmanager.h"
#include <QtWidgets/QMessageBox>
#include "common/common/config.h"
#include <QApplication>
#include "gui_core/gui_core/timemessagebox.h"
#include "dlg_templatedb.h"
#define MUTEX_LOCK_TIMEOUT 20000
#include "gui_core/gui_core/macros.h"

//GLOBALS:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager	*g_pBoSet;				



ClientBackupManager::ClientBackupManager()
{
	m_bIsThinClient=true;
	m_nTimerID=-1;
}

ClientBackupManager::~ClientBackupManager()
{

}





void ClientBackupManager::Backup(Status &pStatus)
{
	pStatus.setError(0);
	if(QMessageBox::question(NULL,tr("Confirmation"),tr("Create new backup. Proceed?"),tr("Yes"),tr("No"))) 
	{
		pStatus.setError(1,"User Abort!");
		return;
	}

	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	if (!m_bIsThinClient) ////THICK
	{
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		QString strBcp=BackupManager::ExecBackup(pStatus);
		QApplication::restoreOverrideCursor();
		//ThickModeRestartTimer();
		if (pStatus.IsOK())
		{
			Status err;
			err.setError(StatusCodeSet::MSG_SYSTEM_BACKUP_SUCCESS,strBcp);
			QMessageBox::information(NULL,tr("Information"),err.getErrorText());
		}

	}
	else
	{
		_SERVER_CALL(ServerControl->Backup(pStatus))
	}

	m_RWMutex.unlock();

}

void ClientBackupManager::Restore(Status &pStatus,QString strFileNameForRestore)
{
	pStatus.setError(0);
	if(QMessageBox::question(NULL,tr("Confirmation"),tr("Restore Database from: ")+strFileNameForRestore+tr(". This will destroy current database data. Proceed?"),tr("Yes"),tr("No"))) return;

	if(!m_RWMutex.tryLockForWrite(MUTEX_LOCK_TIMEOUT))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_BUSY);
		return;
	}

	if (!m_bIsThinClient) //thick
	{
		m_strRestoreOnNextStart=strFileNameForRestore;
		QMessageBox::information(NULL,tr("Information"),tr("Please, restart application to start restore operation!"));
	}
	else
	{
		_SERVER_CALL(ServerControl->Restore(pStatus,strFileNameForRestore))
		QMessageBox::information(NULL,tr("Information"),tr("Please, save all work and close application, server will restart and restore from backup in few minutes."));
	}

	m_RWMutex.unlock();
}


//restart timer
void ClientBackupManager::ThickModeRestartTimer()
{
	if (m_bIsThinClient) //not used in thin
		return;

	if (m_nTimerID!=-1)
		killTimer(m_nTimerID);

	m_nTimerID=-1;

	Status err;
	int nNextInterval=GetNextBackupStart(err);
	if (!err.IsOK())
	{
		nNextInterval=10*60000; //try to start after 10min;
	}
	if (nNextInterval>0)
		m_nTimerID=startTimer(nNextInterval);
}

void ClientBackupManager::StopThickModeTimer()
{
	if (m_nTimerID!=-1)
		killTimer(m_nTimerID);

	m_nTimerID=-1;
}


//time to execute backup:
void ClientBackupManager::timerEvent(QTimerEvent *event)
{

	if(event->timerId()==m_nTimerID)
	{
		if (m_nTimerID!=-1)
			killTimer(m_nTimerID);

		m_nTimerID=-1;
		if (m_bIsThinClient)
		{
			Q_ASSERT(false); //this could never happen in thin!
			return;
		}

		Status err;
		ExecBackup(err);
		_CHK_ERR(err);
		ThickModeRestartTimer();
		
	}

}


bool ClientBackupManager::AskForTemplateDatabase()
{
	Dlg_TemplateDb Dlg;
	if(Dlg.Initialize(this))
		if(Dlg.exec())
		{
			if (m_bIsThinClient)
				QMessageBox::information(NULL,tr("Information"),tr("Application will close now. Server will restart. Restart application in few minutes to start using new database."));

			return true;
		}

	return false;
}



QString ClientBackupManager::ExecBackup(Status &pStatus)
{
	QString strBcp;
	pStatus.setError(0);
	QString strMsg=tr("A scheduled backup will start now. It will take few minutes. Proceed? (You can change backup schedule settings in Menu/Backup & Restore.)\n\n");
	TimeMessageBox box(60,true,QMessageBox::Question,tr("Backup Scheduler"),strMsg,QMessageBox::Ok | QMessageBox::Cancel);
	box.exec();	
	if (box.result()==QMessageBox::Ok)
	{
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		strBcp=BackupManager::ExecBackup(pStatus);
		QApplication::restoreOverrideCursor();
		if (pStatus.IsOK())
		{
			Status err;
			err.setError(StatusCodeSet::MSG_SYSTEM_BACKUP_SUCCESS,strBcp);
			QMessageBox::information(NULL,tr("Information"),err.getErrorText());
		}
	}
	return strBcp;
}