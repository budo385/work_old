#ifndef SIMPLESELECTIONWIZFILESELECTION_H
#define SIMPLESELECTIONWIZFILESELECTION_H


#include "generatedfiles/ui_simpleselectionwizfileselection.h"
#include "gui_core/gui_core/wizardpage.h"

class SimpleSelectionWizFileSelection : public WizardPage
{
	Q_OBJECT

public:
	SimpleSelectionWizFileSelection(int nWizardPageID, QString strPageTitle, QWidget *parent = 0, int nPageEntityID = -1);
	~SimpleSelectionWizFileSelection();

	void Initialize();
	void resetPage();
	bool GetPageResult(DbRecordSet &RecordSet);

private:
	Ui::SimpleSelectionWizFileSelectionClass ui;

private slots:
	void on_btnSelectPath_clicked();
	void on_txtPath_textChanged( const QString & ); 
};

#endif // SIMPLESELECTIONWIZFILESELECTION_H
