#ifndef SIMPLESELECTIONCHECKBOXWIZPAGECHECKBOXWIZPAGE_H
#define SIMPLESELECTIONCHECKBOXWIZPAGECHECKBOXWIZPAGE_H

#include "generatedfiles/ui_simpleselectioncheckboxwizpage.h"
#include "gui_core/gui_core/wizardpage.h"

class SimpleSelectionCheckBoxWizPage : public WizardPage
{
    Q_OBJECT

public:
    SimpleSelectionCheckBoxWizPage(int nWizardPageID, QString strPageTitle, QWidget *parent = 0);
    ~SimpleSelectionCheckBoxWizPage();
	
	void Initialize();
	bool GetPageResult(DbRecordSet &RecordSet);

private:
    Ui::SimpleSelectionCheckBoxWizPageClass ui;
	QString m_strLabel;

private slots:
	void CompleteChanged();
};

#endif // CheckBoxWIZPAGE_H
