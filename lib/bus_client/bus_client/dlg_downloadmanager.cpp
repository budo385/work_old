#include "dlg_downloadmanager.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFile>
#include <QDir>
#include <QScrollArea>
#include "common/common/datahelper.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/gui_helper.h"
#include "common/common/threadid.h"
#include "gui_core/gui_core/picturehelper.h"


Dlg_DownloadManager::Dlg_DownloadManager(QWidget *parent)
	: QWidget(parent)
{
	setObjectName("DL_MANAGER");
	m_nFileIDCounter=0;
	ui.setupUi(this);

	m_pParentWidget = new QWidget;
	QVBoxLayout *vbox=new QVBoxLayout;
	vbox->addStretch();
	vbox->setSpacing(1);
	vbox->setContentsMargins(0,0,0,0);
	m_pParentWidget->setLayout(vbox);

	ui.frameBkg->setStyleSheet("QFrame {background:transparent;}");
	//ui.frameBkg->setAutoFillBackground(false);
	//m_pParentWidget->setAutoFillBackground(false);
	//ui.frameBkg->setBackgroundRole(QPalette::Dark);
	ui.frameBkg->setWidget(m_pParentWidget);
	ui.frameBkg->setAutoFillBackground(false);
	m_pParentWidget->setAutoFillBackground(false);
	

	setWindowTitle(tr("Download/Upload Manager"));

	QString strTextButton= "<html><body style=\" font-family:Arial; text-decoration:none;\">\
						   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:800; font-style:normal; color:%1;\">%2</span></p>\
						   </body></html>";
	GUI_Helper::CreateStyledButton(ui.btnClearAll,"",strTextButton.arg("white").arg(tr("Clear All")),0, ThemeManager::GetCEMenuButtonStyle(),0);
	setStyleSheet("QWidget#DL_MANAGER " + ThemeManager::GetMobileBkg());
	//ui.frameBkg->setStyleSheet("QScrollArea#frameBkg {background:transparent;}");
}
void Dlg_DownloadManager::OnThemeChanged()
{
	ui.btnClearAll->setStyleSheet(ThemeManager::GetCEMenuButtonStyle());
	setStyleSheet("QWidget#DL_MANAGER " + ThemeManager::GetMobileBkg());
}

Dlg_DownloadManager::~Dlg_DownloadManager()
{

}
void Dlg_DownloadManager::on_btnClearAll_clicked()
{
	emit ClearAll();
}

DownloadWidget* Dlg_DownloadManager::AddFileForDownLoad(QString strFilePath,QString strIcon)
{
	//create widget, assigns unique id, returns back
	m_nFileIDCounter++;
	DownloadWidget *pWidget = new DownloadWidget(this,m_nFileIDCounter,strFilePath,true,strIcon);

	pWidget->setMinimumHeight(54);
	pWidget->setMaximumHeight(54);
	pWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
	dynamic_cast<QVBoxLayout*>(m_pParentWidget->layout())->insertWidget(0,pWidget);
	update();
	return pWidget;
}
DownloadWidget* Dlg_DownloadManager::AddFileForUpload(QString strFilePath,QString strIcon)
{
	//create widget, assigns unique id, returns back
	m_nFileIDCounter++;
	DownloadWidget *pWidget = new DownloadWidget(this,m_nFileIDCounter,strFilePath,false,strIcon);

	pWidget->setMinimumHeight(54);
	pWidget->setMaximumHeight(54);
	pWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
	dynamic_cast<QVBoxLayout*>(m_pParentWidget->layout())->insertWidget(0,pWidget);
	update();
	return pWidget;
}

void Dlg_DownloadManager::RemoveFile(DownloadWidget* pWidget)
{
	m_pParentWidget->layout()->removeWidget(pWidget);
	pWidget->deleteLater();
	//delete pWidget;
	update();
}



//-------------------------------------------------------------
//			each download: own widget
//-------------------------------------------------------------



DownloadWidget::DownloadWidget(QWidget *parent,int nFileID,QString strFilePath, bool bIsDownLoad,QString strIcon)
:QFrame(parent),m_bIsBackup(false),m_bOverWrite(false),m_bIsCheckInOut(false),m_bDoNotTrackCheckOutDoc(false),m_bDeleteDocumentWhenCancel(false)
{
	setObjectName("DL_WIDGET");

	m_bIsDownLoadOperation=bIsDownLoad;
	m_strFilePath=strFilePath;
	m_nFileID=nFileID;
	m_pIcon=new QLabel;
	m_pIcon->setAlignment(Qt::AlignVCenter|Qt::AlignHCenter);
	m_pIcon->setMaximumSize(QSize(48,48));
	//m_pIcon->setMinimumSize(QSize(48,48));
	m_pIcon->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);

	QVBoxLayout *vboxIcon = new QVBoxLayout;
	vboxIcon->addStretch();
	vboxIcon->addWidget(m_pIcon);
	vboxIcon->addStretch();
	vboxIcon->setSpacing(0);
	vboxIcon->setContentsMargins(0,0,0,0);


	if (strIcon.isEmpty())
	{
		if (QFile::exists(strFilePath))
		{
			//try to extract else, default icon
			QPixmap icon;
			if(PictureHelper::GetApplicationIcon(QDir::toNativeSeparators(strFilePath),icon))
			{
				m_pIcon->setPixmap(icon);
			}
			else
				m_pIcon->setPixmap(QPixmap(":Internet_File_32.png"));
		}
		else
			m_pIcon->setPixmap(QPixmap(":Internet_File_32.png"));
	}
	else
		m_pIcon->setPixmap(QPixmap(strIcon));

	QFileInfo info(strFilePath);
	m_fileName = new QLabel(info.fileName()); 
	m_ProgressBar = new QProgressBar;
	m_Status = new QLabel; 

	m_ProgressBar->setMinimumHeight(12);
	m_ProgressBar->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
	m_Status->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
	m_fileName->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(m_fileName);
	vbox->addWidget(m_ProgressBar);
	vbox->addWidget(m_Status);
	vbox->setSpacing(1);
	vbox->setContentsMargins(0,0,0,0);


	m_pBtnOpen		=	new StyledPushButton(this,"","","",tr("Open"),0,0);
	m_pBtnRemove	=	new StyledPushButton(this,"","","",tr("Remove"),0,0);
	m_pBtnCancel	=	new StyledPushButton(this,"","","",tr("Cancel"),0,0);
	m_pBtnOpenDir	=	new StyledPushButton(this,"","","",tr("Open Directory"),0,0);

	QSize buttonSize1(90,18);
	m_pBtnOpen->setMaximumSize(buttonSize1);
	m_pBtnOpen->setMinimumSize(buttonSize1);
	m_pBtnOpenDir->setMaximumSize(buttonSize1);
	m_pBtnOpenDir->setMinimumSize(buttonSize1);
	m_pBtnRemove->setMaximumSize(buttonSize1);
	m_pBtnRemove->setMinimumSize(buttonSize1);
	m_pBtnCancel->setMaximumSize(buttonSize1);
	m_pBtnCancel->setMinimumSize(buttonSize1);


	connect(m_pBtnOpen,SIGNAL(clicked()),this,SLOT(OnOpenFile()));
	connect(m_pBtnRemove,SIGNAL(clicked()),this,SLOT(OnRemoveFile()));
	connect(m_pBtnCancel,SIGNAL(clicked()),this,SLOT(OnCancel()));
	connect(m_pBtnOpenDir,SIGNAL(clicked()),this,SLOT(OnOpenDirectory()));

	QVBoxLayout *vbox1 = new QVBoxLayout;
	vbox1->addWidget(m_pBtnOpen);
	vbox1->addWidget(m_pBtnRemove);
	vbox1->addWidget(m_pBtnCancel);
	vbox1->addWidget(m_pBtnOpenDir);
	vbox1->setSpacing(1);
	vbox1->setContentsMargins(0,0,0,0);

	m_ProgressBar->setVisible(false);
	m_ProgressBar->setVisible(false);

	QHBoxLayout *hbox = new QHBoxLayout;

	hbox->addLayout(vboxIcon);
	hbox->addLayout(vbox);
	hbox->addLayout(vbox1);
	hbox->setSpacing(3);
	hbox->setContentsMargins(0,0,0,0);
	this->setLayout(hbox);

	/*
	QString strBkg=" {background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #E1E1E1, stop: 0.4 #DDDDDD, stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);";
//strBkg+="border: 2px solid #C4C4C3;	border-bottom-color: #C2C7CB;" 
	strBkg+="border-top-left-radius: 4px;border-top-right-radius: 4px;}";// min-width: 8ex; padding: 2px;
	*/
	
	QString strBkg=QString(" {border-width: 4px; border-image:url(%1) 4px 4px 4px 4px stretch stretch }").arg(ThemeManager::m_strThemePrefix+"_ToolbarBkg.png");
	this->setStyleSheet("QFrame#DL_WIDGET "+strBkg+" QLabel {color:black}");

	SetStatus(DownloadWidget::STATUS_WAITING);
}
DownloadWidget::~DownloadWidget()
{

}


void DownloadWidget::SetStatus(int nStatus)
{
	m_nStatus=nStatus;
	switch(nStatus)
	{
	case STATUS_WAITING:
		{
			m_ProgressBar->setVisible(false);
			m_pBtnCancel->setVisible(false);
			m_pBtnRemove->setVisible(true);
			m_pBtnOpen->setVisible(true);
			m_pBtnOpenDir->setVisible(true);
			m_Status->setText(tr("Queued"));
		}
		break;
	case STATUS_DOWNLOAD:
		{
			m_ProgressBar->reset();
			m_ProgressBar->setVisible(true);
			m_pBtnCancel->setVisible(true);
			m_pBtnRemove->setVisible(false);
			m_pBtnOpen->setVisible(false);
			m_pBtnOpenDir->setVisible(false);
			m_Status->setText(tr("DownLoading"));
		}
		break;
	case STATUS_UPLOAD:
		{
			m_ProgressBar->reset();
			m_ProgressBar->setVisible(true);
			m_pBtnCancel->setVisible(true);
			m_pBtnRemove->setVisible(false);
			m_pBtnOpen->setVisible(false);
			m_pBtnOpenDir->setVisible(false);
			m_Status->setText(tr("Uploading"));
		}
	    break;
	case STATUS_FINISHED:
		{
			m_ProgressBar->setVisible(false);
			m_pBtnCancel->setVisible(false);
			m_pBtnRemove->setVisible(true);
			m_pBtnOpen->setVisible(true);
			m_pBtnOpenDir->setVisible(true);
			m_Status->setText(tr("Done"));
		}
	    break;
	case STATUS_CANCEL:
		{
			m_ProgressBar->setVisible(false);
			m_pBtnCancel->setVisible(false);
			m_pBtnRemove->setVisible(true);
			m_pBtnOpen->setVisible(false);
			m_pBtnOpenDir->setVisible(false);
			m_Status->setText(tr("Canceled"));
		}
		break;
	case STATUS_ERROR:
		{
			m_ProgressBar->setVisible(false);
			m_pBtnCancel->setVisible(false);
			m_pBtnRemove->setVisible(true);
			m_pBtnOpen->setVisible(false);
			m_pBtnOpenDir->setVisible(false);
			QString strMsg=tr("Error: %1");
			strMsg=strMsg.arg(m_LastError.getErrorText());
			m_Status->setToolTip(strMsg);
			if (strMsg.size()>200)
				strMsg=strMsg.left(200)+"...";
			m_Status->setText(strMsg);
		}
		break;
	default:
		Q_ASSERT(false);
	    break;
	}
}

void DownloadWidget::OnCommunicationInProgress(int nTicks,qint64 done, qint64 total)
{
	if (nTicks>0)
	{
		m_ProgressBar->setVisible(true);
		QString strMsg=tr("Uploading");
		if (m_nStatus==STATUS_DOWNLOAD)
			strMsg=tr("Downloading");

		if (total>0 && done < total)
		{
			if (m_ProgressBar->maximum()==0)
			{
				m_ProgressBar->setRange(0,100);
			}
			m_ProgressBar->setValue(((double)done/total)*100);
		}
		else
		{
			if (m_ProgressBar->maximum()!=0)
			{
				m_ProgressBar->setRange(0,0); //floating progress
			}
			m_ProgressBar->setValue(1);
		}

		//int nOffset=nTicks/10;
		//int nTicksProgress=nTicks-nOffset*10;
		//nTicks=nTicks/4;
		if (total>0 && done < total)
		{
			double speed=(done/double(nTicks))*4;
			QString strSpeed=DataHelper::GetFormatedFileSize((double)speed,2);
			strMsg +=(QString(": %1/%2 speed: %3/s").arg(DataHelper::GetFormatedFileSize((double)done,2,false)).arg(DataHelper::GetFormatedFileSize((double)total,2)).arg(strSpeed));
		}
		else
			strMsg +=(QString(": waiting for server, elapsed: %1s").arg(nTicks/4));

		m_Status->setText(strMsg);
	}
	else
	{
		if (!m_ProgressBar->isVisible() )return;
		m_ProgressBar->reset();
		//m_ProgressBar->setVisible(false);
	}
}


void DownloadWidget::OnOperationEnded(int nCode,QString strError)
{
	//qDebug()<<"OnOperationEnd signal received"<<ThreadIdentificator::GetCurrentThreadID();
	m_LastError.setError(nCode,strError);
	emit SignalOperationEnd(m_nFileID,nCode,strError);
}