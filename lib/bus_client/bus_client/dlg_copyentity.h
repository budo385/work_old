#ifndef DLG_COPYENTITY_H
#define DLG_COPYENTITY_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_copyentity.h"

class Dlg_CopyEntity : public QDialog
{
	Q_OBJECT

public:
	Dlg_CopyEntity(QWidget *parent = 0);
	~Dlg_CopyEntity();

	void Initialize(DbRecordSet lstGroups);
	void GetSelectedGroups(DbRecordSet &lstGroups);

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnAdd_clicked();
	void on_btnRemove_clicked();

private:
	Ui::Dlg_CopyEntityClass ui;

	DbRecordSet m_lstGroups;
	DbRecordSet m_lstGroupsSelected;
};

#endif // DLG_COPYENTITY_H
