#ifndef SP_NO_THICK_CLIENT

#include "clientbackupmanager.h"
extern ClientBackupManager				g_BackupManager;			//database manager
#include "common/common/logger.h"
extern Logger g_Logger;	
#include "bus_client/bus_client/loggeduserdata.h"
#include "bus_client/bus_client/useraccessright_client.h"
extern TestAccRights *g_AccessRight;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager g_Settings;
#include "bus_client/bus_client/loggeduserdata.h"
extern LoggedUserData g_LoggedUserData;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;

#include "thick_client.h"
#include "common/common/config.h"
#include "common/common/config_version_sc.h"
#include "trans/trans/httpreadersync.h"
#include <QMessageBox>
#include <QDesktopServices>

#include "db_core/db_core/db_drivers.h"
#include "common/common/datahelper.h"
#include "usersessionmanager_client.h"
#include "messagedispatcher_client.h"
#include <QObject>
#include "bus_core/bus_core/dbconnectionsetup.h"
#include "bus_client/bus_client/clientimportexportmanager.h"
extern ClientImportExportManager				g_ImportExportManager;			//database manager


//license file:

//FOR THICK global objects (in thin they are useless)
#include "bus_core/bus_core/licenseaccessrights.h"
#include "bus_server/bus_server/systemserviceset.h"
#include "bus_server/bus_server/businessserviceset.h"
#include "bus_server/bus_server/privateserviceset.h"
#include "bus_core/bus_core/usersessionmanager.h"
#include "bus_server/bus_server/loadbalancer.h"
#include "bus_core/bus_core/messagedispatcher.h"
#include "bus_core/bus_core/servercontrolabstract.h"

BusinessServiceSet*		g_BusinessServiceSet;
PrivateServiceSet*		g_PrivateServiceSet;
SystemServiceSet*		g_SystemServiceSet; 
LoadBalancer*			g_LoadBalancer;
UserSessionManager*		g_UserSessionManager;
MessageDispatcher*		g_MessageDispatcher;
LicenseAccessRights		g_ModuleLicense;
ServerControlAbstract*	g_AppServer=NULL;	//fake app server interface on client->to correctly link service_servercontrol

/*!
	Constructor
*/
Thick_Client::Thick_Client()
{
	ClearPointers();
	
}

/*!
	Destructor: kill all objects
*/
Thick_Client::~Thick_Client()
{
	//try to stop app srv session:
	Status err;
	StopService(err);
	Q_ASSERT(err.IsOK()); //stop failed
}



void Thick_Client::ClearPointers()
{
	g_PrivateServiceSet=NULL;
	g_BusinessServiceSet=NULL;
	g_UserSessionManager=NULL;
	g_SystemServiceSet=NULL;
	m_ServerSessionManager=NULL;
	m_SrvSessionBkgTask=NULL;
	g_MessageDispatcher=NULL;
	g_LoadBalancer=NULL;  //not used in thick mode!!
	app=NULL;

}



//stop database session
void Thick_Client::StopService(Status &pStatus)
{
	if(!m_bServiceStarted)return;
	
	
	//clear globals:
	Status err;
	ProcessAfterLogout(err);
	//	if(!pStatus.IsOK()) return;
	
	m_bIsLogged=false;


	//ask for bcp:
	g_BackupManager.CheckDatabaseForBackup(pStatus);
	g_BackupManager.StopThickModeTimer();
	g_ImportExportManager.StopThickModeTimer();

	//do not stop on error, but inform on last error:
	if (!err.IsOK() && pStatus.IsOK())
		pStatus=err;

	//stop bckg tasks:
	m_SrvSessionBkgTask->Stop();
	if(m_SrvSessionBkgTask!=NULL) {delete m_SrvSessionBkgTask;}

	//stop session:
	if(m_ServerSessionManager!=NULL)
	{
		m_ServerSessionManager->StopSession(err);
		delete m_ServerSessionManager;
	}

	m_DbManager.ShutDown();


	//destroy objects:
	if(g_PrivateServiceSet!=NULL)	delete g_PrivateServiceSet;
	if(g_BusinessServiceSet!=NULL)	delete g_BusinessServiceSet;
	if(g_SystemServiceSet!=NULL)	delete g_SystemServiceSet;
	if(g_UserSessionManager!=NULL)	delete g_UserSessionManager;
	if(g_MessageDispatcher!=NULL)	delete g_MessageDispatcher;
	

	//get params from backup manager:
	g_BackupManager.GetBackupData(m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime);


	//SAVE INI file back with any changes:
	m_INIFile.Save();


	m_bServiceStarted=false;

	ClearPointers();
}




void Thick_Client::StartService(Status &pStatus,DbConnectionSettings& DbConnSettings)
{

	//if started stop it
	if(m_bServiceStarted) 
	{
		StopService(pStatus);
		if(!pStatus.IsOK()) return;
	}

	//----------------------------------------------
	//Start logger (based on settings: file-logger, db logger, no logging):
	//log level	0 - basic(system)+common, 1 -network, 2-sql+business, 3 - profiler, 4- full debug (time profile+all errors)
	//----------------------------------------------
	g_Logger.UseAsGlobalLogger();
	if (m_INIFile.m_nLogLevel && m_INIFile.m_nLogTargetFile)
	{
		if (m_INIFile.m_nLogLevel==Logger::LOG_LEVEL_ALL_FLUSH)
			g_Logger.EnableFileLogging(DataHelper::GetApplicationHomeDir()+"/settings/client.log",0);
		else
			g_Logger.EnableFileLogging(DataHelper::GetApplicationHomeDir()+"/settings/client.log");
	}
	if (m_INIFile.m_nLogLevel && m_INIFile.m_nLogTargetConsole)
		g_Logger.EnableConsoleLogging();
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);


	//----------------------------------------------
	//init Database
	//----------------------------------------------
	m_DbManager.Initialize(pStatus,DbConnSettings,5);  //only 5 connection for embeded
	if(!pStatus.IsOK()) return;
	//qDebug() << "Database is ready!";

	//----------------------------------------------
	//Core services
	//----------------------------------------------

	//get data for testing if app. server was alive:
	int nUniqueLicenseID=g_ModuleLicense.GetUniqueLicenseID();

	//set conn:
	g_LoggedUserData.SetConnectionName(DbConnSettings.m_strCustomName);

	//----------------------------------------------
	//Sys services
	//----------------------------------------------
	g_SystemServiceSet= new SystemServiceSet(&m_DbManager);


	//----------------------------------------------
	//start App. server session (automatic garbage cleaning) in separate thred:
	//----------------------------------------------
	m_ServerSessionManager=new ServerSessionManager(g_SystemServiceSet);

	
	g_MessageDispatcher=new MessageDispatcher_Client();
	//----------------------------------------------
	//Backup manager initialize:
	//----------------------------------------------
	g_BackupManager.SetClientMode(false);
	g_BackupManager.SetBackupData(m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime);
	g_BackupManager.Initialize(pStatus,m_DbManager.GetDbSettings(),true,false);
	if (!pStatus.IsOK()) return;

	//check for restore save right now if done:
	if(g_BackupManager.CheckDatabaseForRestore(pStatus))
		m_INIFile.Save();
	if (!pStatus.IsOK()) return;

	//check for backup: (in firebird, client must not connect to database->ensure with shutdown)
	//if restore, backup will not execute,save right now if done
	if(g_BackupManager.CheckDatabaseForBackup(pStatus))
		m_INIFile.Save();
	if (!pStatus.IsOK()) return;

	//check for reorganization
	g_BackupManager.CheckDatabaseForReorganization(pStatus);
	if (!pStatus.IsOK()) return;



	
	QString IPAdrr="0.0.0.0";
	int nIsLBO=0;
	int nIsMaster=0;
	int nAppSrvSessionID=m_ServerSessionManager->StartSession(pStatus,IPAdrr,nUniqueLicenseID,SRV_ROLE_THICK,1,nIsLBO,nIsMaster,"","",m_INIFile.m_strSerial);
	if(!pStatus.IsOK())return;
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Thick client is running with session = "+QVariant(nAppSrvSessionID).toString());

	//start reporter:
	m_SrvSessionBkgTask=new BackgroundTaskExecutor(m_ServerSessionManager,NULL,APP_SESSION_REPORT_INTERVAL*1000);
	m_SrvSessionBkgTask->Start();


	//----------------------------------------------
	//UserSession manager (heart of system):
	//----------------------------------------------
	g_UserSessionManager =new UserSessionManager_Client(g_SystemServiceSet,&g_ModuleLicense,nAppSrvSessionID);

	//----------------------------------------------
	//Launch Business & Private sets/init pointers:
	//----------------------------------------------
	g_PrivateServiceSet = new PrivateServiceSet(&m_DbManager,g_UserSessionManager);
	g_BusinessServiceSet = new BusinessServiceSet(&m_DbManager,g_UserSessionManager);


	//main app:
	app=g_BusinessServiceSet;

	//-----------------------------
	//			import /export
	//-----------------------------

	if(g_ImportExportManager.ReloadSettings())
		g_ImportExportManager.ThickModeRestartTimer();


	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Main thread is:"+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());

	m_bServiceStarted=true;


}





/*!
	After success login, init global objects

	\param pStatus			- error
*/
void Thick_Client::ProcessAfterLogin(Status &pStatus)
{

	//load after login data:
	g_LoggedUserData.ReadUserData(pStatus);
	if(!pStatus.IsOK()) return;

	
	//	//init AR:
	int nUserID=g_LoggedUserData.GetUserID();
	DbRecordSet rowARSet;
	g_LoggedUserData.GetAccessRightData(rowARSet);
	if(g_AccessRight)
	{
		dynamic_cast<UserAccessRight_Client*>(g_AccessRight)->SetUserID(nUserID);
		g_AccessRight->SetUserARRecordSet(nUserID,rowARSet);
	}
	

	//init FP:
	g_FunctionPoint.Initialize(pStatus,m_strModuleCode,m_strMLIID);
	if(!pStatus.IsOK())return;

	//check for THIN:
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_THICK_CLIENT_ACCESS,nValue))
	{
		pStatus.setError(1,tr("Access not allowed from thick client!"));
		return;
	}

	//store username back
	m_INIFile.m_strLastUserName=g_LoggedUserData.GetLoggedUserName();
	m_INIFile.m_strConnectionName=g_LoggedUserData.GetConnectionName();


	QString strMessage=tr("Client successfully logged to: ")+m_INIFile.m_strConnectionName;
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);

}




/*!
	Before Logout, 

	\param pStatus			- error
*/
void Thick_Client::ProcessBeforeLogout(Status &pStatus)
{
	emit SignalProcessBeforeLogout();
	g_Settings.SavePersonSettings(pStatus);
}


/*!
	After Logout, reset flags, and all cached data

	\param pStatus			- error
*/
void Thick_Client::ProcessAfterLogout(Status &pStatus)
{
	//reset AR:
	if(g_AccessRight)
	{
		g_AccessRight->DeleteUserARRecordSet(g_LoggedUserData.GetUserID());
		dynamic_cast<UserAccessRight_Client*>(g_AccessRight)->SetUserID(0);
	}

	//reset FP:
	g_FunctionPoint.Invalidate();

	//clear cache:
	g_ClientCache.PurgeCache();


	ClearUserData();

}





//0-no updates available, 1- user abort, 2- new update exists
int Thick_Client::CheckForUpdates()
{

	double fInternet;
	QString strUrl;
	QString strVersionID;
	GetAllVersionData(strVersionID,strUrl,fInternet);

	fInternet=GetVersion();
	if (fInternet==-1)
		return 0;

	strUrl=GetSetupExePath();
	if (strUrl.isEmpty())
		return 0;

	strVersionID=GetVersionID();


	double fCurrent=QString(APPLICATION_VERSION).toDouble();
	if(fCurrent<fInternet)
	{
		//TEMPORARY: ask version:
		QString strMsg=tr("Your version %1 is not actual any more. Do you want to download and install an update to version %2 now?");
		strMsg=strMsg.arg(QString(APPLICATION_VERSION_ID));
		strMsg=strMsg.arg(strVersionID);
		int nResult=QMessageBox::question(NULL,tr("Warning"),strMsg,tr("Yes"),tr("No"));
		if (nResult==0)
		{
			//->link
			QDesktopServices::openUrl(strUrl);
			return 2;
		}
		return 1;
	}

	return 0;
}

void Thick_Client::InitializeService(Status &pStatus,ClientIniFile &ini)
{
	ClientManager::InitializeService(pStatus,ini);
	if (!pStatus.IsOK()) return;

	//MODULE LICENSE file:
	//------------------------------------------
	QString strLicensePath=m_INIFile.m_strKeyFilePath;
	if (strLicensePath.isEmpty())
		strLicensePath= DataHelper::GetApplicationHomeDir()+"/settings/sokrates.key";

	QFileInfo info(strLicensePath);
	if (!info.exists())
	{
		//ask for path:
		strLicensePath=GetKeyFileDlg();
		if (strLicensePath.isEmpty())
		{
			pStatus.setError(1,tr("License file is missing or corrupted!"));
			return;
		}
	}

	bool bOK=g_ModuleLicense.LoadLicense(strLicensePath);
	if(!bOK)
	{
		pStatus.setError(1,tr("License file is missing or corrupted!"));
		return;
	}
	else
	{
		if (m_INIFile.m_strKeyFilePath!=strLicensePath) //save if ok:
		{
			m_INIFile.m_strKeyFilePath=strLicensePath;
			m_INIFile.Save();
		}
	}

	//DATABASE CHECK
	//------------------------------------------
	QString strConnectionName;
	if(DbConnectionSetup::SetupThickDatabase(pStatus,strConnectionName))
	{
		m_INIFile.m_strConnectionName=strConnectionName;
		m_INIFile.Save();
	}

}




#endif //SP_NO_THICK_CLIENT


