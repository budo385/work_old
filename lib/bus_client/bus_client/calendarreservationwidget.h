#ifndef CALENDARRESERVATIONWIDGET_H
#define CALENDARRESERVATIONWIDGET_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QTableWidget>
#include <QRadioButton>
#include <QPushButton>
#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/datetimeeditex.h"
#include "common/common/status.h"
//#include "mainentityselectioncontroller.h"

class CalendarReservationTable;

class CalendarReservationWidget : public QWidget
{
	Q_OBJECT

public:
	CalendarReservationWidget(QWidget *parent);
	~CalendarReservationWidget();

	void Initialize(int nTableID);
	void Reload(Status &pStatus, int nRecordId,DbRecordSet *newRecord=NULL);
	void DataCheckBeforeWrite(Status &err);
	//void Lock(Status &pStatus);
	void Write(Status &pStatus, int nRecordId);
	void GetData(DbRecordSet &data);
	void Clear();
	void SetEditMode(bool bEdit=true);

private:
	int m_nTableID;
	int m_nRecordId;
	CalendarReservationTable *m_pTable;
	QPushButton *m_pbtnAdd;
};

class CalendarReservationTable : public QTableWidget
{
	friend class CalendarReservationTable_WidgetRow_Cell_Before_0;
	Q_OBJECT

public:
	CalendarReservationTable(QWidget * parent=0);
	~CalendarReservationTable();

	void	SetData(DbRecordSet *data);
	void	GetData(DbRecordSet *data);
	void	ClearAll();
	void	SetEditMode(bool bEdit=true);
	
public slots:
	void	AddNewRow();
	void	OnCellClicked ( int row, int column );

private:
	void	OnDelete(int nRow);
	void	RefreshDisplay();
	void	Widgets2Data();

	DbRecordSet m_lstData; 	
	bool							m_bEdit;
};



class CalendarReservationTable_WidgetRow_Cell_Before_0 : public QWidget
{
	Q_OBJECT
public:
	CalendarReservationTable_WidgetRow_Cell_Before_0(CalendarReservationTable *parent,int nRow);
	~CalendarReservationTable_WidgetRow_Cell_Before_0();

private slots:
	void OnSelect();
	void OnRemove();

private:
	CalendarReservationTable *m_Parent;
	int m_nRow;
};



class CalendarReservationTable_WidgetRow_Cell_0 : public QWidget
{
	Q_OBJECT
public:
	CalendarReservationTable_WidgetRow_Cell_0();
	~CalendarReservationTable_WidgetRow_Cell_0();

public:
	QRadioButton *m_pBtnImpossible;
	QRadioButton *m_pBtnUndesired;

};

class CalendarReservationTable_WidgetRow_Cell_2 : public QWidget
{
	Q_OBJECT
public:
	CalendarReservationTable_WidgetRow_Cell_2();
	~CalendarReservationTable_WidgetRow_Cell_2();

public:
	DateTimeEditEx *m_pFromTime;
	DateTimeEditEx *m_pToTime;
};

class CalendarReservationTable_WidgetRow_Cell_1 : public QWidget
{
	Q_OBJECT
public:
	CalendarReservationTable_WidgetRow_Cell_1(CalendarReservationTable_WidgetRow_Cell_2 *pCell2);
	~CalendarReservationTable_WidgetRow_Cell_1();

public:
	QRadioButton *m_pBtnDay_0;
	QRadioButton *m_pBtnDay_1;
	QRadioButton *m_pBtnDay_2;
	QRadioButton *m_pBtnDay_3;
	QRadioButton *m_pBtnDay_4;
	QRadioButton *m_pBtnDay_5;
	QRadioButton *m_pBtnDay_6;

	QRadioButton *m_pBtnFree;

	DateTimeEditEx *m_pFromDateTime;
	DateTimeEditEx *m_pToDateTime;

	CalendarReservationTable_WidgetRow_Cell_2 *m_pCell2;

private slots:
	void OnRadioButtonClicked();
	void OnRadioButtonToggled(bool);


};




#endif // CALENDARRESERVATIONWIDGET_H
