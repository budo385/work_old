#include "accrightsetdelegate.h"

AccRightSetDelegate::AccRightSetDelegate(RoleAccRghtsDefModel *AccRModel, QObject *parent)
	:  QItemDelegate(parent)
{
	m_pAccRModel = AccRModel;
}

AccRightSetDelegate::~AccRightSetDelegate()
{

}

QWidget *AccRightSetDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	//Get item row.
	int row = index.row();
	//Get item.
	DbRecordSetItem *item = m_pAccRModel->GetItemsHash()->value(row);

	//Bool value.
	if (item->data(1).toInt() == 1)
	{
		QCheckBox *box = new QCheckBox(parent);
		return box;
	}
	//Integer or string value.
	else //(item->data(1).toInt() == 1)
	{
		QLineEdit *line = new QLineEdit(parent);
		//Set input mask for integer values.
		if (item->data(1).toInt() == 0)
			line->setInputMask("99999999999999999999999999999999999999999");

		return line;
	}
}

void AccRightSetDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	//Get item row.
	int row = index.row();
	//Get item.
	DbRecordSetItem *item = m_pAccRModel->GetItemsHash()->value(row);

	//If bool.
	if (item->data(1).toInt() == 1)
	{
		QCheckBox *box = static_cast<QCheckBox*>(editor);
		QString boxDesc = item->data(0).toString();
		box->setAutoFillBackground(true);
		box->setText(boxDesc);

		if (item->data(2).toInt())
			box->setChecked(true);
		else
			box->setChecked(false);
	}
	//If integer.
	else if (item->data(1).toInt() == 0)
	{
		QLineEdit *line = static_cast<QLineEdit*>(editor);
		line->insert(item->data(2).toString());
	}
	//If string.
	else
	{
		QLineEdit *line = static_cast<QLineEdit*>(editor);
		line->insert(item->data(3).toString());
	}

}

void AccRightSetDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	//Delegate value.
	QVariant Value;
	//Get item row.
	int row = index.row();
	//Get item.
	DbRecordSetItem *item = m_pAccRModel->GetItemsHash()->value(row);

	//Get value.
	if (item->data(1).toInt() == 1)
	{
		QCheckBox *box = static_cast<QCheckBox*>(editor);
		if (box->isChecked())
			Value = 1;
		else
			Value = 0;
	}
	else
	{
		QLineEdit *line = static_cast<QLineEdit*>(editor);
		Value = line->text();
	}

	model->setData(index, Value);
}

void AccRightSetDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	editor->setGeometry(option.rect);
}
