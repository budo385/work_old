#include "calendarreservationwidget.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QHeaderView>
#include <QToolButton>
#include <QCalendarWidget>

#include "db_core/db_core/dbtableiddefinition.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/macros.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;						//global access to Bo services


CalendarReservationWidget::CalendarReservationWidget(QWidget *parent)
	: QWidget(parent)
{
	m_nRecordId=-1;
	m_nTableID=-1;

}

CalendarReservationWidget::~CalendarReservationWidget()
{

}



void CalendarReservationWidget::Initialize(int nTableID)
{
	m_nTableID=nTableID;

	m_pbtnAdd =new QPushButton(tr("Add New Calendar Reservation"));
	m_pbtnAdd->setMaximumWidth(200);
	
	QLabel *label = new QLabel;
	label->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:10pt; color:black; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">%1</p></body></html>").arg("List of times where calendar assigments are impossible or undesired"));
	m_pTable= new CalendarReservationTable;
	m_pTable->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

	connect(m_pbtnAdd,SIGNAL(clicked()),m_pTable,SLOT(AddNewRow()));

	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->addWidget(m_pbtnAdd);
	hbox->addWidget(label);
	hbox->addStretch();
	hbox->setSpacing(3);
	hbox->setContentsMargins(1,1,1,1);

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addLayout(hbox);
	vbox->addWidget(m_pTable);
	vbox->setSpacing(1);
	vbox->setContentsMargins(0,0,0,0);
	this->setLayout(vbox);
	Clear();
}

void CalendarReservationWidget::DataCheckBeforeWrite(Status &err)
{
	err.setError(0);

	//if is_free==0 then weekday from, to must be set!!
	DbRecordSet data;
	m_pTable->GetData(&data);
	int nSize=data.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (data.getDataRef(i,"BCRS_IS_FREE").toInt()==0)
		{
			if (!data.getDataRef(i,"BCRS_WEEKDAY_TIME_FROM").toDateTime().isValid() || !data.getDataRef(i,"BCRS_WEEKDAY_TIME_TO").toDateTime().isValid())
			{
				err.setError(1,QString(tr("Reservation times is not set for row: %1 !")).arg(i));
				return;
			}

			if (data.getDataRef(i,"BCRS_WEEKDAY_TIME_FROM").toDateTime()>data.getDataRef(i,"BCRS_WEEKDAY_TIME_TO").toDateTime())
			{
				err.setError(1,QString(tr("Reservation times are invalid for row: %1 !")).arg(i));
				return;
			}

		}
	}

}


void CalendarReservationWidget::Reload(Status &pStatus, int nRecordId,DbRecordSet *newRecord)
{
	Q_ASSERT(m_nTableID!=-1);
	m_nRecordId=nRecordId;
	if (newRecord)
	{
		m_pTable->SetData(newRecord);
	}
	else
	{
		Q_ASSERT(nRecordId!=-1);
		DbRecordSet data;
		QString strWhere;
		switch(m_nTableID)
		{
		case BUS_PROJECT:
			strWhere=" WHERE BCRS_PROJECT_ID="+QString::number(nRecordId);
			break;
		case BUS_CM_CONTACT:
			strWhere=" WHERE BCRS_CONTACT_ID="+QString::number(nRecordId);
			break;
		case BUS_PERSON:
			strWhere=" WHERE BCRS_PERSON_ID="+QString::number(nRecordId);
		    break;
		case BUS_CAL_RESOURCE:
			strWhere=" WHERE BCRS_RESOURCE_ID="+QString::number(nRecordId);
		    break;
		default:
			Q_ASSERT(false);
		    break;
		}
		_SERVER_CALL(ClientSimpleORM->Read(pStatus,BUS_CAL_RESERVATION,data,strWhere));
		if (!pStatus.IsOK())
			return;
		m_pTable->SetData(&data);
	}
}

void CalendarReservationWidget::Clear()
{
	m_nRecordId=-1;
	m_pTable->ClearAll();

}
void CalendarReservationWidget::SetEditMode(bool bEdit)
{
	m_pbtnAdd->setEnabled(bEdit);
	m_pTable->SetEditMode(bEdit);
}

/*
void CalendarReservationWidget::Lock(Status &pStatus)
{
	Q_ASSERT(m_nTableID!=-1);
	//do not need as it is locked by parent table: contact/person/project
}
*/
void CalendarReservationWidget::Write(Status &pStatus, int nRecordId)
{
	Q_ASSERT(m_nTableID!=-1);

	m_nRecordId=nRecordId;
	DbRecordSet data;
	m_pTable->GetData(&data);

	QString strParentColIdName="";
	switch(m_nTableID)
	{
	case BUS_PROJECT:
		data.setColValue("BCRS_PROJECT_ID",m_nRecordId);
		data.setColValue("BCRS_CONTACT_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_PERSON_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_RESOURCE_ID",QVariant(QVariant::Int));
		strParentColIdName="BCRS_PROJECT_ID";
		break;
	case BUS_CM_CONTACT:
		data.setColValue("BCRS_PROJECT_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_CONTACT_ID",m_nRecordId);
		data.setColValue("BCRS_PERSON_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_RESOURCE_ID",QVariant(QVariant::Int));
		strParentColIdName="BCRS_CONTACT_ID";
		break;
	case BUS_PERSON:
		data.setColValue("BCRS_PROJECT_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_CONTACT_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_PERSON_ID",m_nRecordId);
		data.setColValue("BCRS_RESOURCE_ID",QVariant(QVariant::Int));
		strParentColIdName="BCRS_PERSON_ID";
		break;
	case BUS_CAL_RESOURCE:
		data.setColValue("BCRS_PROJECT_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_CONTACT_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_PERSON_ID",QVariant(QVariant::Int));
		data.setColValue("BCRS_RESOURCE_ID",m_nRecordId);
		strParentColIdName="BCRS_RESOURCE_ID";
		break;
	default:
		Q_ASSERT(false);
		break;
	}

	_SERVER_CALL(ClientSimpleORM->WriteParentSubData(pStatus,BUS_CAL_RESERVATION,data,m_nRecordId,strParentColIdName));
}
void CalendarReservationWidget::GetData(DbRecordSet &data)
{
	Q_ASSERT(m_nTableID!=-1);
	m_pTable->GetData(&data);
}


CalendarReservationTable::CalendarReservationTable(QWidget *parent)
:QTableWidget(parent)
{
	m_bEdit=true;
	horizontalHeader()->hide();
	verticalHeader()->hide();
	verticalHeader()->setDefaultSectionSize(60);
	horizontalHeader()->setStretchLastSection(true);
	setColumnCount(4);
	setColumnWidth(0,30);
	setColumnWidth(1,100);
	setColumnWidth(2,300);
	setColumnWidth(3,150);

	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_RESERVATION));

	//connect(this,SIGNAL(cellClicked(int, int)),this,SLOT(OnCellClicked(int,int)));

	setSelectionBehavior( QAbstractItemView::SelectRows);
	setSelectionMode(QAbstractItemView::NoSelection);
	setEditTriggers(QAbstractItemView::NoEditTriggers); //no edit by double click possible

	setObjectName("TABLE_X");
	setStyleSheet(ThemeManager::GetTableViewBkg("TABLE_X")+" QToolButton {background:transparent;}");
}

CalendarReservationTable::~CalendarReservationTable()
{

}

void CalendarReservationTable::SetData(DbRecordSet *data)
{
	m_lstData=*data;
	RefreshDisplay();
}
void CalendarReservationTable::GetData(DbRecordSet *data)
{
	Widgets2Data();
	*data=m_lstData;
}

void CalendarReservationTable::ClearAll()
{
	m_lstData.clear();
	RefreshDisplay();
}

void CalendarReservationTable::AddNewRow()
{
	OnCellClicked(m_lstData.getRowCount(),1);
}

void CalendarReservationTable::SetEditMode(bool bEdit)
{
	if (bEdit)
	{
		int nSize=rowCount();
		for(int i=0;i<nSize;i++)
		{
			QWidget *pWidget=cellWidget(i,0);
			if (pWidget)
				pWidget->setDisabled(false);
			pWidget=cellWidget(i,1);
			if (pWidget)
				pWidget->setDisabled(false);
			pWidget=cellWidget(i,2);
			if (pWidget)
				pWidget->setDisabled(false);
			pWidget=cellWidget(i,3);
			if (pWidget)
				pWidget->setDisabled(false);
		}
	}
	else
	{	
		int nSize=rowCount();
		for(int i=0;i<nSize;i++)
		{
			QWidget *pWidget=cellWidget(i,0);
			if (pWidget)
				pWidget->setDisabled(true);
			pWidget=cellWidget(i,1);
			if (pWidget)
				pWidget->setDisabled(true);
			pWidget=cellWidget(i,2);
			if (pWidget)
				pWidget->setDisabled(true);
			pWidget=cellWidget(i,3);
			if (pWidget)
				pWidget->setDisabled(true);
		}
	}

	m_bEdit=bEdit;

}


void CalendarReservationTable::OnDelete(int nRow)
{
	m_lstData.deleteRow(nRow);
	RefreshDisplay();
}

void CalendarReservationTable::OnCellClicked ( int row, int column )
{
	if (!m_bEdit) return;

	if (row==m_lstData.getRowCount())
	{
		Widgets2Data();
		m_lstData.addRow();
		m_lstData.setData(m_lstData.getRowCount()-1,"BCRS_WEEKDAY",0);
		m_lstData.setData(m_lstData.getRowCount()-1,"BCRS_IS_FREE",0);
		m_lstData.setData(m_lstData.getRowCount()-1,"BCRS_FREE_FROM",QDateTime::currentDateTime());
		m_lstData.setData(m_lstData.getRowCount()-1,"BCRS_FREE_TO",QDateTime::currentDateTime());
		RefreshDisplay();
		if (column==2)
		{
			QTableWidgetItem *itemX=item(row,column);
			editItem(itemX);
			cellWidget(row,column)->setFocus();
		}
	}
}



void CalendarReservationTable::Widgets2Data()
{
	int nSize=m_lstData.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		CalendarReservationTable_WidgetRow_Cell_0 *pWidget=dynamic_cast<CalendarReservationTable_WidgetRow_Cell_0 *>(cellWidget(i,1));
		if (pWidget)
		{
			m_lstData.setData(i,"BCRS_IS_POSSIBLE",(int)(pWidget->m_pBtnUndesired->isChecked()));
		}
		CalendarReservationTable_WidgetRow_Cell_1 *pWidget1=dynamic_cast<CalendarReservationTable_WidgetRow_Cell_1 *>(cellWidget(i,2));
		if (pWidget1)
		{
			if (pWidget1->m_pBtnFree->isChecked())
			{
				m_lstData.setData(i,"BCRS_IS_FREE",1);
				m_lstData.setData(i,"BCRS_FREE_FROM",pWidget1->m_pFromDateTime->dateTime());
				m_lstData.setData(i,"BCRS_FREE_TO",pWidget1->m_pToDateTime->dateTime());
				m_lstData.setData(i,"BCRS_WEEKDAY",0);
			}
			else
			{
				m_lstData.setData(i,"BCRS_IS_FREE",0);
				m_lstData.setData(i,"BCRS_WEEKDAY_TIME_FROM",QDateTime(QDate::currentDate(),pWidget1->m_pCell2->m_pFromTime->time())); //store as datetime...
				m_lstData.setData(i,"BCRS_WEEKDAY_TIME_TO",QDateTime(QDate::currentDate(),pWidget1->m_pCell2->m_pToTime->time()));

				int nDay=0;
				if (pWidget1->m_pBtnDay_0->isChecked()) nDay=0;
				if (pWidget1->m_pBtnDay_1->isChecked()) nDay=1;
				if (pWidget1->m_pBtnDay_2->isChecked()) nDay=2;
				if (pWidget1->m_pBtnDay_3->isChecked()) nDay=3;
				if (pWidget1->m_pBtnDay_4->isChecked()) nDay=4;
				if (pWidget1->m_pBtnDay_5->isChecked()) nDay=5;
				if (pWidget1->m_pBtnDay_6->isChecked()) nDay=6;

				m_lstData.setData(i,"BCRS_WEEKDAY",nDay);
			}
		}
			
	}
}

void CalendarReservationTable::RefreshDisplay()
{
	clear();

	int nSize=m_lstData.getRowCount();
	setRowCount(nSize+4); //last 4 is empty
	int nSizeTotal=nSize+4;
	for(int i=0;i<nSizeTotal;i++)
	{
		if (i<nSize)
		{

			CalendarReservationTable_WidgetRow_Cell_Before_0 *pWidget=dynamic_cast<CalendarReservationTable_WidgetRow_Cell_Before_0 *>(cellWidget(i,0));
			if (!pWidget)
			{
				pWidget = new CalendarReservationTable_WidgetRow_Cell_Before_0(this,i);
				setCellWidget(i,0,pWidget);
			}
			pWidget->setVisible(true);

			CalendarReservationTable_WidgetRow_Cell_0 *pWidget0=dynamic_cast<CalendarReservationTable_WidgetRow_Cell_0 *>(cellWidget(i,1));
			if (!pWidget0)
			{
				pWidget0 = new CalendarReservationTable_WidgetRow_Cell_0();
				setCellWidget(i,1,pWidget0);
			}
			pWidget0->setVisible(true);

			
			CalendarReservationTable_WidgetRow_Cell_2 *pWidget2=dynamic_cast<CalendarReservationTable_WidgetRow_Cell_2 *>(cellWidget(i,3));
			if (!pWidget2)
			{
				pWidget2 = new CalendarReservationTable_WidgetRow_Cell_2();
				setCellWidget(i,3,pWidget2);
			}
			pWidget2->setVisible(true);


			CalendarReservationTable_WidgetRow_Cell_1 *pWidget1=dynamic_cast<CalendarReservationTable_WidgetRow_Cell_1 *>(cellWidget(i,2));
			if (!pWidget1)
			{
				pWidget1 = new CalendarReservationTable_WidgetRow_Cell_1(pWidget2);
				setCellWidget(i,2,pWidget1);
			}
			pWidget1->setVisible(true);

			pWidget0->m_pBtnUndesired->setChecked((bool)(m_lstData.getDataRef(i,"BCRS_IS_POSSIBLE").toInt()==1));
			pWidget0->m_pBtnImpossible->setChecked((bool)(m_lstData.getDataRef(i,"BCRS_IS_POSSIBLE").toInt()==0));

			if (m_lstData.getDataRef(i,"BCRS_IS_FREE").toInt()>0)
			{
				pWidget1->m_pBtnFree->setChecked(true);
				pWidget1->m_pFromDateTime->setEnabled(true);
				pWidget1->m_pToDateTime->setEnabled(true);
				pWidget1->m_pFromDateTime->setDateTime(m_lstData.getDataRef(i,"BCRS_FREE_FROM").toDateTime());
				pWidget1->m_pToDateTime->setDateTime(m_lstData.getDataRef(i,"BCRS_FREE_TO").toDateTime());
				pWidget2->m_pFromTime->setEnabled(false);
				pWidget2->m_pToTime->setEnabled(false);
			}
			else
			{
				int nDay=m_lstData.getDataRef(i,"BCRS_WEEKDAY").toInt();
				pWidget1->m_pBtnFree->setChecked(false);
				switch(nDay)
				{
				case 0:
					pWidget1->m_pBtnDay_0->setChecked(true);
					break;
				case 1:
					pWidget1->m_pBtnDay_1->setChecked(true);
					break;
				case 2:
					pWidget1->m_pBtnDay_2->setChecked(true);
				    break;
				case 3:
					pWidget1->m_pBtnDay_3->setChecked(true);
				    break;
				case 4:
					pWidget1->m_pBtnDay_4->setChecked(true);
					break;
				case 5:
					pWidget1->m_pBtnDay_5->setChecked(true);
					break;
				case 6:
					pWidget1->m_pBtnDay_6->setChecked(true);
				    break;
				default:
					pWidget1->m_pBtnDay_0->setChecked(true);
				    break;
				}
				pWidget1->m_pFromDateTime->setEnabled(false);
				pWidget1->m_pToDateTime->setEnabled(false);
				pWidget2->m_pFromTime->setTime(m_lstData.getDataRef(i,"BCRS_WEEKDAY_TIME_FROM").toTime());
				pWidget2->m_pToTime->setTime(m_lstData.getDataRef(i,"BCRS_WEEKDAY_TIME_TO").toTime());
				pWidget2->m_pFromTime->setEnabled(true);
				pWidget2->m_pToTime->setEnabled(true);
			}

		}

		else
		{

			QWidget *pWidget=cellWidget(i,0);
			if (pWidget)
				pWidget->setVisible(false);
			pWidget=cellWidget(i,1);
			if (pWidget)
				pWidget->setVisible(false);
			pWidget=cellWidget(i,2);
			if (pWidget)
				pWidget->setVisible(false);
			pWidget=cellWidget(i,3);
			if (pWidget)
				pWidget->setVisible(false);

		}
	}

	clearSelection();
	//if (currentRow()<0)
	//	setCurrentCell (0, 2, QItemSelectionModel::Select);
}







CalendarReservationTable_WidgetRow_Cell_Before_0::CalendarReservationTable_WidgetRow_Cell_Before_0(CalendarReservationTable *parent,int nRow)
{
	m_Parent=parent;
	m_nRow=nRow;
	//: select, open, delete
	//ckb zip
	QSize buttonSize(18,18);
	QToolButton *btnDelete = new QToolButton;
	btnDelete->setIcon(QIcon(":SAP_Clear.png"));
	btnDelete->setToolTip(tr("Remove Reservation"));
	btnDelete->setAutoRaise(true);
	btnDelete->setMaximumSize(buttonSize);
	btnDelete->setMinimumSize(buttonSize);

	QHBoxLayout *hbox = new QHBoxLayout;
	//hbox->addWidget(btnSelect);
	hbox->addWidget(btnDelete);
	//hbox->addWidget(btnOpen);
	hbox->setSpacing(1);
	hbox->setContentsMargins(3,0,3,0);

	this->setLayout(hbox);

	connect(btnDelete,SIGNAL(clicked()),this,SLOT(OnRemove()));
}

void CalendarReservationTable_WidgetRow_Cell_Before_0::OnSelect()
{
	//add attachment
	//m_Parent->OnSelectContact(m_nRow);
}
void CalendarReservationTable_WidgetRow_Cell_Before_0::OnRemove()
{
	//remove
	m_Parent->OnDelete(m_nRow);
}


CalendarReservationTable_WidgetRow_Cell_Before_0::~CalendarReservationTable_WidgetRow_Cell_Before_0()
{


}




CalendarReservationTable_WidgetRow_Cell_0::CalendarReservationTable_WidgetRow_Cell_0()
{
	m_pBtnImpossible = new QRadioButton(tr("Impossible"));
	m_pBtnUndesired = new QRadioButton(tr("Undesired"));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(m_pBtnImpossible);
	vbox->addWidget(m_pBtnUndesired);
	vbox->setSpacing(1);
	vbox->setContentsMargins(3,1,3,1);
	this->setLayout(vbox);
}

CalendarReservationTable_WidgetRow_Cell_0::~CalendarReservationTable_WidgetRow_Cell_0()
{

}

CalendarReservationTable_WidgetRow_Cell_1::CalendarReservationTable_WidgetRow_Cell_1(CalendarReservationTable_WidgetRow_Cell_2 *pCell2)
{
	QString strStyleD="QDateTimeEdit {color: black;}";
	QString strStyleLabel="QLabel:disabled {color: black;} QLabel {color: white;}";


	m_pCell2=pCell2;
	m_pBtnDay_0= new QRadioButton(tr("Mo"));
	m_pBtnDay_1= new QRadioButton(tr("Tu"));
	m_pBtnDay_2= new QRadioButton(tr("We"));
	m_pBtnDay_3= new QRadioButton(tr("Th"));
	m_pBtnDay_4= new QRadioButton(tr("Fr"));
	m_pBtnDay_5= new QRadioButton(tr("Sa"));
	m_pBtnDay_6= new QRadioButton(tr("Su"));

	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->addWidget(m_pBtnDay_0);
	hbox->addWidget(m_pBtnDay_1);
	hbox->addWidget(m_pBtnDay_2);
	hbox->addWidget(m_pBtnDay_3);
	hbox->addWidget(m_pBtnDay_4);
	hbox->addWidget(m_pBtnDay_5);
	hbox->addWidget(m_pBtnDay_6);
	hbox->setSpacing(1);
	hbox->setContentsMargins(1,1,1,1);

	m_pBtnFree= new QRadioButton(tr("Period: "));;
	QLabel *pLabel1 = new QLabel(tr("From "));
	QLabel *pLabel2 = new QLabel(tr("To "));
	pLabel1->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	pLabel2->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	pLabel1->setStyleSheet(strStyleLabel);
	pLabel2->setStyleSheet(strStyleLabel);

	m_pFromDateTime = new DateTimeEditEx;
	m_pToDateTime= new DateTimeEditEx;

	m_pFromDateTime->setStyleSheet(strStyleD);
	//m_pFromDateTime->calendarWidget()->setStyleSheet("");
	m_pToDateTime->setStyleSheet(strStyleD);
	//m_pToDateTime->calendarWidget()->setStyleSheet("");

	QHBoxLayout *hbox1 = new QHBoxLayout;
	hbox1->addWidget(m_pBtnFree);
	hbox1->addWidget(pLabel1);
	hbox1->addWidget(m_pFromDateTime);
	hbox1->addWidget(pLabel2);
	hbox1->addWidget(m_pToDateTime);
	hbox1->setSpacing(3);
	hbox1->setContentsMargins(1,1,1,1);

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addLayout(hbox);
	vbox->addLayout(hbox1);
	vbox->setSpacing(0);
	vbox->setContentsMargins(3,0,3,0);

	this->setLayout(vbox);

	connect(m_pBtnFree,SIGNAL( toggled(bool)),this,SLOT(OnRadioButtonToggled(bool)));
	
}
void CalendarReservationTable_WidgetRow_Cell_1::OnRadioButtonClicked()
{
	if(m_pBtnFree->isChecked())
	{
		m_pCell2->m_pFromTime->setEnabled(true);
		m_pCell2->m_pToTime->setEnabled(true);
	}
	else
	{
		m_pCell2->m_pFromTime->setEnabled(false);
		m_pCell2->m_pToTime->setEnabled(false);
	}
}

void CalendarReservationTable_WidgetRow_Cell_1::OnRadioButtonToggled(bool bChecked)
{
	if(bChecked)
	{
		m_pFromDateTime->setEnabled(true);
		m_pToDateTime->setEnabled(true);
		m_pCell2->m_pFromTime->setEnabled(false);
		m_pCell2->m_pToTime->setEnabled(false);
	}
	else
	{
		m_pFromDateTime->setEnabled(false);
		m_pToDateTime->setEnabled(false);
		m_pCell2->m_pFromTime->setEnabled(true);
		m_pCell2->m_pToTime->setEnabled(true);
	}
}

CalendarReservationTable_WidgetRow_Cell_1::~CalendarReservationTable_WidgetRow_Cell_1()
{

}

CalendarReservationTable_WidgetRow_Cell_2::CalendarReservationTable_WidgetRow_Cell_2()
{
	QString strStyleLabel="QLabel:disabled {color: black;} QLabel {color: white;}";
	QString strStyleD="QDateTimeEdit {color: black;}";

	m_pFromTime = new DateTimeEditEx;
	m_pFromTime->setStyleSheet(strStyleD);
	m_pFromTime->useAsTimeEdit();
	QLabel *pLabel1 = new QLabel(tr("From "));
	pLabel1->setStyleSheet(strStyleLabel);
	pLabel1->setMinimumWidth(50);
	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->addWidget(pLabel1);
	hbox->addWidget(m_pFromTime);
	hbox->setSpacing(3);
	hbox->setContentsMargins(1,1,1,1);

	QLabel *pLabel2 = new QLabel(tr("To "));
	pLabel2->setMinimumWidth(50);
	pLabel2->setStyleSheet(strStyleLabel);
	m_pToTime= new DateTimeEditEx;
	m_pToTime->setStyleSheet(strStyleD);
	m_pToTime->useAsTimeEdit();

	m_pFromTime->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
	m_pToTime->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

	QHBoxLayout *hbox1 = new QHBoxLayout;
	hbox1->addWidget(pLabel2);
	hbox1->addWidget(m_pToTime);
	hbox1->setSpacing(3);
	hbox1->setContentsMargins(1,1,1,1);

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addStretch();
	vbox->addLayout(hbox);
	vbox->addLayout(hbox1);
	vbox->addStretch();
	vbox->setSpacing(3);
	vbox->setContentsMargins(3,0,3,0);
	this->setLayout(vbox);
}

CalendarReservationTable_WidgetRow_Cell_2::~CalendarReservationTable_WidgetRow_Cell_2()
{

}