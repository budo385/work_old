#ifndef SELECTION_CONTACTBUTTON_H
#define SELECTION_CONTACTBUTTON_H


#include <QtWidgets/QToolButton>
#include "gui_core/gui_core/pie_menu/qtpiemenu.h"



/*!
	\class  Selection_ContactButton
	\brief  Implements universal PIE menu for all day long use
	\ingroup Bus_Client
*/
class Selection_ContactButton : public QToolButton
{
	Q_OBJECT

public:
    Selection_ContactButton(QWidget *parent=0);
    ~Selection_ContactButton();


	void SetEntityType(int nEntityType);
	void SetEntityRecordID(int nEntityRecordID);
	

protected slots:
	virtual void OnClick();
	virtual void OnSendMail(){};
	virtual void OnPhoneCall(){};
	virtual void OnDocument(){};
	virtual void OnInternet(){};
	virtual void OnChat(){};
	virtual void OnSMS(){};

protected:
	void contextMenuEvent(QContextMenuEvent *event);
	QtPieMenu *m_pPieMenu;
	int m_nEntityRecordID;
	int m_nEntityTypeID;
   
};

#endif // SELECTION_CONTACTBUTTON_H
