#ifndef SELECTION_RECORDSELECTOR_H
#define SELECTION_RECORDSELECTOR_H

#include <QtWidgets/QFrame>
#include "gui_core/gui_core/styledpushbutton.h"
#include "generatedfiles/ui_selection_recordselector.h"
#include "common/common/dbrecordset.h"


class Selection_RecordSelector : public QFrame
{
	Q_OBJECT

public:
	Selection_RecordSelector(QWidget *parent = 0);
	~Selection_RecordSelector();

	void Initialize(DbRecordSet *pLstSource, QString strSelectorTitle="",bool bPreventDelOfFirstRow=true);
	int m_nCurrentRow;
	void RefreshLabel();
	void Reset();
	void SetEditMode(bool bEditMode);

signals:
	void RowAdded(int nPrevRow,int nCurrRow);
	void RowDeleted(int);
	void RowChanged(int nPrevRow,int nCurrRow);
	void Accepted(int);

private slots:
	void OnAdd();
	void OnDelete();
	void OnLeft();
	void OnRight();
	void OnAccept();

private:
	Ui::Selection_RecordSelectorClass ui;
	

	//yellow toolbar:
	StyledPushButton	*m_btnAdd;
	StyledPushButton	*m_btnDel;
	StyledPushButton	*m_btnLeft;
	StyledPushButton	*m_btnRight;
	StyledPushButton	*m_btnAccept;
	
	QLabel				*m_pTitle;
	QLabel				*m_pRowSelector;

	bool				m_bPreventDelOfFirstRow;
	DbRecordSet			*m_pLstSource;
};

#endif // SELECTION_RECORDSELECTOR_H
