#ifndef USERACCESSRIGHT_CLIENT_H
#define USERACCESSRIGHT_CLIENT_H

#include "bus_core/bus_core/useraccessright.h"

/*!
	\class UserAccessRight_Client
	\brief Global client side object/cache for testing user access rights
	\ingroup Bus_Server
*/
class UserAccessRight_Client : public UserAccessRight
{
public:
    UserAccessRight_Client();
    ~UserAccessRight_Client();

protected:
	int		GetUserID(); 
	int		GetPersonID();
	bool	IsSystemUser();
	void	TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed=true);

};

#endif // USERACCESSRIGHT_CLIENT_H
