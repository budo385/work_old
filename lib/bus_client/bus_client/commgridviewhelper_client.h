#ifndef COMMGRIDVIEWHELPER_CLIENT_H
#define COMMGRIDVIEWHELPER_CLIENT_H

#include <QObject>
#include "bus_core/bus_core/commgridviewhelper.h"

class CommGridViewHelper_Client : public CommGridViewHelper
{
	Q_OBJECT

public:
	void GetDocApplicationsFromServer(DbRecordSet &lstData, Status &err);
	
private:
	
};

#endif // COMMGRIDVIEWHELPER_CLIENT_H
