#ifndef Selection_TreeBased_H
#define Selection_TreeBased_H

#include <QtWidgets/QFrame>
#ifdef WINCE
#include "embedded_core/embedded_core/generatedfiles/ui_selection_treebased.h"
#else
#include "generatedfiles/ui_selection_treebased.h"
#endif
#include "bus_client/bus_client/mainentityselectioncontroller.h"

/*!
	\class  Selection_TreeBased
	\brief  Selection frame for selecting Main entities, tree based
	\ingroup GUICore

	Use:
	- Add own entity data in initialize method
	- Call Initialize() to init to specific entity
	- register as observer to this object (dynamic cast to subject or mainselectioncontroller)
	- use GetSelection() to fetch selected data 
	- see interface MainEntitySelectionController for details
*/
class Selection_TreeBased : public QFrame, public MainEntitySelectionController
{
    Q_OBJECT

public:
    Selection_TreeBased(QWidget *parent = 0);
    ~Selection_TreeBased();

	void Initialize(int nEntityID,bool bSkipLoadingData=false,bool bIsFUISelectorWidget=true,bool bSingleClickIsSelection=true,bool bMultiSelection=false,bool bEnableDrag=true); 
	void GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord);
	void GetSelection(DbRecordSet& lstRecords);
	void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nPrimaryKeyValue=-1);
	void SetCurrentEntityRecord(int nEntityRecordID,bool bSkipNotifyObservers=false);			
	UniversalTreeWidget* GetTreeWidget(){return ui.treeView;};
	bool blockSignals(bool b){blockObserverSignals(b);return QFrame::blockSignals(b);}
	bool IsSideBarVisible();
	void SetSideBarVisible(bool bVisible=true);
	void SetFocusOnFind();
	virtual void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void HideProjectToolBar();
	void LoadStoredList(int nListID);

public slots:
	void SlotReloadFromServer();				//by context menu
	void OnInsertNewEntry();
	void OnDeleteSelectedEntry();
	void OnEditSelectedEntry();
	void OnDetailsSelectedEntry();
	void OnNewFromTemplate();
	void OnNewStructFromTemplate();


private slots:
	void SlotSelectionChanged();
	void SlotSelectionChanged_DoubleClicked();
	void on_treeView_ItemCheckStateChanged(QList<QTreeWidgetItem*> lstChangedCheckStateItems);
	void OnProjectListLoad();
	void OnProjectListCreate();
	void OnProjectListModify();
	void OnProjectListDelete();
	void on_btnModify_clicked();
	void on_btnRemove_clicked();
	void on_btnSelect_clicked();
	void on_btnInsert_clicked();
	void on_btnFromTemplate_clicked();
	void OnProjectHideShowInactive();
	void OnFindWidgetEnterPressed();
	void OnProjectListChanged();

signals:
	void ItemCheckStateChanged(QList<QTreeWidgetItem*> lstChangedCheckStateItems);

private:
	void RebuildProjectToolBar();
	void LoadStoredProjectLists();
	void SetSelectionOnView(int nViewID);
	Ui::Selection_TreeBasedClass ui;
	bool m_bIsFUISelectorWidget;
	bool m_bSingleClickIsSelection;

	QAction *m_pActionProjectHideInvisible;
};

#endif // Selection_TreeBased_H
