#ifndef HTTPLOOKUP_H
#define HTTPLOOKUP_H

#include <QString>

class HttpLookup
{
public:
	HttpLookup();
	~HttpLookup();

	static bool WebLookupZIPTown(QString &strZIP, QString &strTown, QString &strCountryCode);
};

#endif // HTTPLOOKUP_H
