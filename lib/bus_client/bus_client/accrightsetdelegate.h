#ifndef ACCRIGHTSETDELEGATE_H
#define ACCRIGHTSETDELEGATE_H

#include <QItemDelegate>
#include <QCheckBox>
#include <QLineEdit>
#include <QComboBox>

#include "roleaccrghtsdefmodel.h"

class AccRightSetDelegate : public QItemDelegate
{
	Q_OBJECT

public:
    AccRightSetDelegate(RoleAccRghtsDefModel *AccRModel, QObject *parent = NULL);
    ~AccRightSetDelegate();

	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	void setEditorData(QWidget *editor, const QModelIndex &index) const;
	void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
	void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
	RoleAccRghtsDefModel *m_pAccRModel;
};

#endif // ACCRIGHTSETDELEGATE_H
