#include "bus_client/bus_client/selection_grouptree.h"
#include <QtWidgets/QMessageBox>
#include <QInputDialog>
#include "common/common/entity_id_collection.h"
#include "common/common/status.h"
#include "generatecodedlg.h"
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "common/common/datahelper.h"
#include "gui_core/gui_core/thememanager.h"
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/csvimportfile.h"
#include <QFileDialog>
#include "gui_core/gui_core/macros.h"

#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight					*g_AccessRight;				//global access right tester
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "bus_client/bus_client/changemanager.h"			
extern ChangeManager g_ChangeManager;				//global message dispatcher

extern QString g_strLastDir_FileOpen;



Selection_GroupTree::Selection_GroupTree(QWidget *parent)
    : QFrame(parent),m_pLstGroupContent(NULL),m_TreeMenu(this),m_pActExtCategory(NULL),m_pActImportSub(NULL)
{
	m_nCurrTreeID=-1;
	m_pActGroupLoad=NULL;
	ui.setupUi(this);
	m_nLastGroupID=0;

	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
	{
		ui.txtGroupNote->setVisible(false);
	}
	ui.treeWidget->SetDragMode(UniversalTreeWidget::DRAG_MODE_ALL_CHILDREN_IF_COLLPASED);
}

Selection_GroupTree::~Selection_GroupTree()
{
	//save last tree if allowed:
	//save:
	if (m_bIsFUISelectorWidget && g_pSettings->GetPersonSetting(CONTACT_DEF_SETTINGS_SAVE_ON_EXIT).toBool())
	{
		int nTreeID=ui.cmbGroupTree->itemData(ui.cmbGroupTree->currentIndex()).toInt();
		g_pSettings->SetPersonSetting(CONTACT_DEF_GROUP_TREE,nTreeID);
	}
}


//note: group tree is ENTITY_BUS_GROUP_ITEMS, but nEntityID points to entity of group content
void Selection_GroupTree::Initialize(int nEntityID,bool bSkipLoadingData,bool bIsFUISelectorWidget,bool bSingleClickIsSelection,bool bMultiSelection,bool bEnableDrag)
{

	m_nEntityID=nEntityID;
	m_bIsFUISelectorWidget=bIsFUISelectorWidget;			//if FUI selector then this flag can be used to enable some features (Insert node, edit node, etc..)
	m_bSingleClickIsSelection=bSingleClickIsSelection;

	MainEntitySelectionController::Initialize(ENTITY_BUS_GROUP_ITEMS);	//init data source


	m_nExtCatIdx=m_lstData.getColumnIdx("BGIT_EXTERNAL_CATEGORY");
	m_nDescIdx=m_lstData.getColumnIdx("BGIT_DESCRIPTION");

	//----------------------COMBO----------------------------------

		//create Cnxt Menu actions:
	QAction* pAction;
	QList<QAction*> lstActions; 


	pAction = new QAction(tr("Add New Tree"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnTreeAdd()));
	m_lstTreeActions.append(pAction);

	pAction = new QAction(tr("Rename Tree"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnTreeRename()));
	m_lstTreeActions.append(pAction);

	pAction = new QAction(tr("Delete Tree"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnTreeDelete()));
	m_lstTreeActions.append(pAction);

	//Add separator
	pAction = new QAction(this);
	pAction->setSeparator(true);
	m_lstTreeActions.append(pAction);

	pAction = new QAction(tr("Reload Trees"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnTreeReloadFromServer()));
	m_lstTreeActions.append(pAction);

	//create menu, set it on button:
	int nSize=m_lstTreeActions.size();
	for(int i=0;i<nSize;++i)
		m_TreeMenu.addAction(m_lstTreeActions.at(i));
	ui.btnTreeOps->setMenu(&m_TreeMenu);

	//----------------------COMBO----------------------------------


	//----------------------INIT TREE----------------------------------

	//create Cnxt Menu actions:
	QString strName,strNamePlural;
	GetEntityName(nEntityID,strName,strNamePlural);

	if (m_bIsFUISelectorWidget)
	{
		pAction = new QAction(tr("Add Group"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupItemAdd()));
		lstActions.append(pAction);

		pAction = new QAction(tr("Rename Group"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupItemRename()));
		lstActions.append(pAction);

		pAction = new QAction(tr("Delete Group"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupItemDelete()));
		lstActions.append(pAction);


		//separator
		pAction = new QAction(this);
		pAction->setSeparator(true);
		lstActions.append(pAction);

		if (m_pLstGroupContent) //only if external data group source is set, enable actions
		{
		
			pAction = new QAction(tr("Add Selected ", "Z")+strNamePlural+tr(" to Group", "Z"), this);
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupAddSelected()));
			lstActions.append(pAction);

			pAction = new QAction(tr("Remove Selected ", "Z")+strNamePlural+tr(" From Group", "Z"), this);
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupRemoveSelected()));
			lstActions.append(pAction);

			pAction = new QAction(tr("Replace Group ", "Z")+strNamePlural+tr(" With Selected Records", "Z"), this);
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupReplaceSelected()));
			lstActions.append(pAction);

			pAction = new QAction(tr("Intersect Selected ", "Z")+strNamePlural+tr(" With Group Records", "Z"), this);
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupIntersectSelected()));
			lstActions.append(pAction);
		
			//separator
			pAction = new QAction(this);
			pAction->setSeparator(true);
			lstActions.append(pAction);


			pAction = new QAction(tr("Add Whole Actual ", "Z")+strName+tr(" List to Group", "Z"), this);
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupAddWholeList()));
			lstActions.append(pAction);

			pAction = new QAction(tr("Replace Group ", "Z")+strName+tr(" With Whole Actual ", "Z")+strName+tr(" List", "Z"), this);
			connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupReplaceWholeList()));
			lstActions.append(pAction);

		}

		/*
		pAction = new QAction(tr("New Record"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnGroupNewEntity()));
		lstActions.append(pAction);
		*/


		//separator
		pAction = new QAction(this);
		pAction->setSeparator(true);
		lstActions.append(pAction);


		m_pActExtCategory = new QAction(tr("External Category", "Z"), this);
		connect(m_pActExtCategory, SIGNAL(triggered()), this, SLOT(OnExtCategoryEdit()));
		lstActions.append(m_pActExtCategory);

		m_pActImportSub = new QAction(tr("Import Sub-groups", "Z"), this);
		connect(m_pActImportSub, SIGNAL(triggered()), this, SLOT(OnGroupImport()));
		lstActions.append(m_pActImportSub);

		//separator
		pAction = new QAction(this);
		pAction->setSeparator(true);
		lstActions.append(pAction);

		m_pActGroupLoad = new QAction(tr("Startup: Load in Actual Contact List", "Z"), this);
		m_pActGroupLoad->setCheckable(true);
		connect(m_pActGroupLoad, SIGNAL(triggered()), this, SLOT(OnSetGroupLoad()));
		lstActions.append(m_pActGroupLoad);

		/*
		pAction = new QAction(tr("Clear Group For Load In Actual List"), this);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnClearGroupLoad()));
		lstActions.append(pAction);
		*/

		//separator
		pAction = new QAction(this);
		pAction->setSeparator(true);
		lstActions.append(pAction);

	}

	bool bDenied=!g_AccessRight->TestAR(ENTER_MODIFY_CONTACT_GROUP_TREES);

	if (bDenied)
		lstActions.clear();

	pAction = new QAction(tr("Refresh Data", "Z"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnTreeRefreshDataFromServer()));
	lstActions.append(pAction);

	pAction = new QAction(tr("Expand All Nodes", "Z"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnTreeExpandAll()));
	lstActions.append(pAction);


	ui.treeWidget->setColumnCount(1);
	ui.treeWidget->SetDropType(m_nEntityID);
	ui.treeWidget->SetHeader();
	ui.treeWidget->Initialize(&m_lstData, !bDenied, bEnableDrag,!bMultiSelection); //init with data tree list
	ui.treeWidget->SetContextMenuActions(lstActions);      //set cntx menu
	ui.treeWidget->SetToolTipColumnDataSource(m_nDescIdx);

	//connect dbl click
	/*
	if(bSingleClickIsSelection) //connect selection changed
	{
		connect(ui.treeWidget,SIGNAL(SignalSelectionChanged()),this,SLOT(SlotSelectionChanged()));  //MB requested key navigation, will trigger event twice, but...
		//connect(ui.treeWidget,SIGNAL(itemClicked (QTreeWidgetItem*,int)),this,SLOT(SlotSelectionChanged())); 
	}
	else
		connect(ui.treeWidget,SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)),this,SLOT(SlotSelectionChanged())); 
	*/

	connect(ui.treeWidget,SIGNAL(SignalSelectionChanged()),this,SLOT(SlotSelectionChanged()));  //MB requested key navigation, will trigger event twice, but...
	connect(ui.treeWidget,SIGNAL(itemDoubleClicked (QTreeWidgetItem*,int)),this,SLOT(SlotSelectionChanged_DoubleClicked())); 

	
	connect(ui.treeWidget,SIGNAL(itemExpanded ( QTreeWidgetItem *)),this,SLOT(OnItemExpanded(QTreeWidgetItem *))); 
	connect(ui.treeWidget,SIGNAL(itemCollapsed ( QTreeWidgetItem *)),this,SLOT(OnItemCollapsed(QTreeWidgetItem *))); 
	connect(ui.treeWidget,SIGNAL(DropOccured()),this,SLOT(OnTreeDropEvent())); 

	ui.treeWidget->SetDropType(ENTITY_BUS_GROUP_ITEMS); //group item is drop type: target must recognize
	ui.treeWidget->AddAcceptableDropTypes(m_nEntityID);	//brb.........
	ui.treeWidget->SetEditMode(true);


	//----------------------INIT TREE----------------------------------


	//----------------------BUTTONS----------------------------------

	connect(ui.btnTreeEdit,SIGNAL(clicked()),this,SLOT(ToggleEditMode())); 

	//----------------------BUTTONS----------------------------------



	//always go non EDIT mode+
	m_bEditMode=true;
	if (bIsFUISelectorWidget)
	{
		ToggleEditMode(); 
	}
	else
	{
		ui.btnTreeEdit->setVisible(false);
		ui.btnTreeOps->setVisible(false);
	}
	
	//load trees:
	int nCurrentIndex=-1; //current index= invalid
	
	LoadTree(); //load tree (either from cache or server)

	//get from personal settings:
	int nTreeID=g_pSettings->GetPersonSetting(CONTACT_DEF_GROUP_TREE).toInt();
	if (nTreeID<=0)
		nTreeID=g_pSettings->GetApplicationOption(APP_CONTACT_DEF_GROUP_TREE).toInt();
	if (nTreeID>0)
	{
		nCurrentIndex=m_lstTrees.find("BGTR_ID",nTreeID,true);
	}


	OnTreeReload(nCurrentIndex); //load tree and content & use settings


	//if not FUI disalbe
	if (!m_bIsFUISelectorWidget)
	{
		ui.btnTreeEdit->setVisible(false);
		ui.btnTreeOps->setVisible(false);
	}
	else
	{
		int nGroupLoadOnStart=g_pSettings->GetPersonSetting(CONTACT_DEF_GROUP_FOR_LOAD).toInt();
		if (nGroupLoadOnStart>0)
		{
			QString strGroupName=GetGroupName(nGroupLoadOnStart);
			if (!strGroupName.isEmpty())
			{
				m_pActGroupLoad->blockSignals(true);
				m_pActGroupLoad->setText(tr("Set Group For Load In Actual List (")+strGroupName+")");
				m_pActGroupLoad->setChecked(true);
				m_pActGroupLoad->blockSignals(false);
			}
		}
	}

	

	if (bDenied)
	{
		ui.btnTreeEdit->setEnabled(false);
		ui.btnTreeOps->setEnabled(false);
	}
	

	this->setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
	ui.treeWidget->setStyleSheet(ThemeManager::GetTreeBkg());

	//issue 1663:
	ui.txtGroupNote->setVisible(false);
}


void Selection_GroupTree::SetActualListDataSource(DbRecordSet *pLstData)
{
	m_pLstGroupContent=pLstData;
}




void Selection_GroupTree::GetEntityName(int nEntityID,QString &strName, QString &strNamePlural)
{
	switch(nEntityID)
	{
	case ENTITY_BUS_CONTACT:
		strName= tr("Contact");
		strNamePlural=tr ("Contacts");
		break;
	default:
		strName= tr("Record");
		strNamePlural=tr("Records");
	    break;
	}


}

void Selection_GroupTree::ToggleEditMode()
{

	Status err;
	bool bEdit=!m_bEditMode;

	if (bEdit)
	{
		//check index:
		if (ui.cmbGroupTree->currentIndex()<0)
		{
			QMessageBox::warning(this,tr("Warning"),tr("Please Select Tree For Edit!"));
			return;
		}


		//try lock tree
		_SERVER_CALL(BusGroupTree->LockTree(err,ui.cmbGroupTree->itemData(ui.cmbGroupTree->currentIndex()).toInt(),m_strLockedResourceTree))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}

		ui.btnTreeOps->setEnabled(false);
		
		//set button state:
		ui.btnTreeEdit->setToolTip(tr("Go View Mode"));
		ui.btnTreeEdit->setIcon(QIcon(":btn_view.png"));
		ui.btnTreeEdit->setFlat(true);
		//ui.treeWidget->setStyleSheet("QTreeWidget#treeWidget { background-color: rgb(254,255,237) }");

		//disable combo:
		ui.cmbGroupTree->setEnabled(false);

		if (m_pActImportSub)m_pActImportSub->setVisible(true);

		//Disable actions on combo tree menu:
		QList<QAction*> &lstTreeActions=ui.treeWidget->GetContextMenuActions();
		lstTreeActions.at(0)->setVisible(true);
		lstTreeActions.at(1)->setVisible(true);
		lstTreeActions.at(2)->setVisible(true);
		lstTreeActions.at(3)->setVisible(true);

		//enable drag on tree: on itself....

		if (m_nLastGroupID>0)
			ui.txtGroupNote->setReadOnly(false);

		//issue 1663: show if edit->onl< pro mode
		if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
			ui.txtGroupNote->setVisible(true);
	}
	else
	{
		//try lock tree
		if (!m_strLockedResourceTree.isEmpty())
		{
			_SERVER_CALL(BusGroupTree->UnLockTree(err,m_strLockedResourceTree))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
		}
		
		ui.btnTreeOps->setEnabled(true);
		m_strLockedResourceTree.clear();
		if (m_pActImportSub)m_pActImportSub->setVisible(false);

		//set button state:
		ui.btnTreeEdit->setToolTip(tr("Modify Tree"));
		ui.btnTreeEdit->setIcon(QIcon(":btn_edit.png"));
		ui.btnTreeEdit->setFlat(false);
		//ui.treeWidget->setStyleSheet("");

		//disable combo:
		ui.cmbGroupTree->setEnabled(true);

		//disable group item actions:

		if (g_AccessRight->TestAR(ENTER_MODIFY_CONTACT_GROUP_TREES))
		{
			QList<QAction*> &lstTreeActions=ui.treeWidget->GetContextMenuActions();
			lstTreeActions.at(0)->setVisible(false);
			lstTreeActions.at(1)->setVisible(false);
			lstTreeActions.at(2)->setVisible(false);
			lstTreeActions.at(3)->setVisible(false);
		}

		//after edit: destroy cache:
		g_ClientCache.PurgeCache(ENTITY_BUS_CONTACT_GROUPS);

		ui.txtGroupNote->setReadOnly(true);
		//issue 1663:
		ui.txtGroupNote->setVisible(false);
	}

	//success:
	m_bEditMode=bEdit;

}


//------------------------------------------------------------------------------------
//						TREE OPERATIONS
//------------------------------------------------------------------------------------


//reload groups:
void Selection_GroupTree::on_cmbGroupTree_currentIndexChanged(int nIndex)
{

	//--------------------------------------------------------
	//			Save expanded status into hash
	//--------------------------------------------------------
	SaveExpandedTreeState();

	Status err;

	//check index:
	if (ui.cmbGroupTree->currentIndex()<0)
		return;

	int nTreeID=ui.cmbGroupTree->itemData(ui.cmbGroupTree->currentIndex()).toInt();


	//------------------------------------------
	//			INVALIDATE
	//------------------------------------------
	m_nLastGroupID=0;
	InvalidateGroupData();


	//------------------------------------------
	//			LOAD TREE
	//------------------------------------------

	LoadGroupTree(nTreeID);
	
	RefreshDisplay();

	//--------------------------------------------------------
	//			Load expanded status into hash
	//--------------------------------------------------------
	LoadExpandedTreeState();


}




//Open Dialog:
void Selection_GroupTree::OnTreeAdd()
{
	bool ok;
	QString text = QInputDialog::getText(this, tr("Enter Tree Name"),
		tr("Tree Name:"), QLineEdit::Normal,"", &ok);

	if (!ok)
		return;

	//read all tree's based on entity:

	DbRecordSet rowTree;
	rowTree.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_GROUP_TREE));
	rowTree.addRow();
	rowTree.setData(0,"BGTR_NAME",text);
	rowTree.setData(0,"BGTR_ENTITY_TYPE_ID",m_nEntityID);

	Status err;
	_SERVER_CALL(BusGroupTree->WriteTree(err,rowTree,""))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//add on combo:
	m_lstTrees.merge(rowTree);
	int i=m_lstTrees.getRowCount()-1;
	ui.cmbGroupTree->blockSignals(true);
	ui.cmbGroupTree->addItem(m_lstTrees.getDataRef(i,"BGTR_NAME").toString(),m_lstTrees.getDataRef(i,"BGTR_ID"));	
	ui.cmbGroupTree->blockSignals(false);
	//set index on it:
	ui.cmbGroupTree->setCurrentIndex(i);

}

void Selection_GroupTree::OnTreeRename()
{
	bool ok;
	DbRecordSet rowTree;
	Status err;

	//check index:
	if (ui.cmbGroupTree->currentIndex()<0)
		return;

	int i=ui.cmbGroupTree->currentIndex();
	int nTreeID=m_lstTrees.getDataRef(i,"BGTR_ID").toInt();

	rowTree=m_lstTrees.getRow(i);

	QString text = QInputDialog::getText(this, tr("Enter Tree Name"),
		tr("Tree Name:"), QLineEdit::Normal,rowTree.getDataRef(0,"BGTR_NAME").toString(), &ok);

	if (!ok)
		return;

	//read all tree's based on entity:
	rowTree.setData(0,"BGTR_NAME",text);

	//try lock tree if not already
	if (!m_bEditMode)
	{
		_SERVER_CALL(BusGroupTree->LockTree(err,nTreeID,m_strLockedResourceTree))
		if(!err.IsOK())
		{
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
	}


	bool bError=false;
	_SERVER_CALL(BusGroupTree->WriteTree(err,rowTree,""))
	if(!err.IsOK())
	{
		bError=true;
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		//return;
	}

	//unlock if not
	if (!m_strLockedResourceTree.isEmpty() && !m_bEditMode)
	{
		_SERVER_CALL(BusGroupTree->UnLockTree(err,m_strLockedResourceTree))
		if(!err.IsOK())
		{
			bError=true;
			QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return;
		}
		m_strLockedResourceTree.clear();
	}

	if (bError)	return;

	//update on combo:
	m_lstTrees.assignRow(i,rowTree);
	ui.cmbGroupTree->setItemText(i,text);	

}

void Selection_GroupTree::OnTreeDelete()
{

	DbRecordSet rowTree;
	Status err;

	//check index:
	if (ui.cmbGroupTree->currentIndex()<0)
		return;

	int i=ui.cmbGroupTree->currentIndex();
	int nTreeID=m_lstTrees.getDataRef(i,"BGTR_ID").toInt();

	int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete this tree permanently from the database? Warning: all groups inside will be deleted!"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES


	//read all tree's based on entity:
	_SERVER_CALL(BusGroupTree->DeleteTree(err,nTreeID))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//update on combo:
	//m_lstTrees.deleteRow(i);

	//after edit: destroy cache:
	g_ClientCache.PurgeCache(ENTITY_BUS_CONTACT_GROUPS);
	LoadTree(true);

	ui.cmbGroupTree->blockSignals(true);
	ui.cmbGroupTree->removeItem(i);
	ui.cmbGroupTree->blockSignals(false);
	ui.cmbGroupTree->setCurrentIndex(0);



}



void Selection_GroupTree::OnTreeReload(int nCurrentIndex)
{
	ui.cmbGroupTree->blockSignals(true);
	ui.cmbGroupTree->clear();
	ui.cmbGroupTree->blockSignals(false);


	//LoadTree();

	//fill combo:
	int nSize=m_lstTrees.getRowCount();
	ui.cmbGroupTree->blockSignals(true);
	for(int i=0;i<nSize;++i)
		ui.cmbGroupTree->addItem(m_lstTrees.getDataRef(i,"BGTR_NAME").toString(),m_lstTrees.getDataRef(i,"BGTR_ID"));	

	if (nCurrentIndex>=0) //if valid set as default:
		ui.cmbGroupTree->setCurrentIndex(nCurrentIndex);

	ui.cmbGroupTree->blockSignals(false);

	//relaod tree data
	OnTreeRefreshData();

}





//------------------------------------------------------------------------------------
//						GROUP  ITEM OPERATIONS
//------------------------------------------------------------------------------------


void Selection_GroupTree::OnGroupItemAdd()
{

	//Get selected node:
	int nRowSelected=m_lstData.getSelectedRow();
	QString strParentCode;
	if (nRowSelected!=-1)
		strParentCode=m_lstData.getDataRef(nRowSelected,"BGIT_CODE").toString();
	else
		strParentCode="P.";

	//generate new code for this item
	generatecodedlg dlg;
	dlg.m_strFldPrefix  = "BGIT";
	dlg.m_nEntityType   = ENTITY_BUS_GROUP_ITEMS;
	dlg.SetHierarhicalCodeData(&m_lstData);
	dlg.EnableNameInput();
	dlg.SetParentCode(strParentCode);
	dlg.SetProposedCode(strParentCode);
	if(0 == dlg.exec())
		return;
	//dlg.RefreshData();



	//save to DB
	DbRecordSet rowGroup;
	int nTreeID=m_lstTrees.getDataRef(ui.cmbGroupTree->currentIndex(),"BGTR_ID").toInt();
	rowGroup.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_GROUP_ITEMS));
	rowGroup.addRow();
	rowGroup.setData(0,"BGIT_CODE",dlg.m_strProposedCode);
	rowGroup.setData(0,"BGIT_NAME",dlg.m_strProposedName);
	rowGroup.setData(0,"BGIT_TREE_ID",nTreeID);


	if (dlg.m_strProposedCode.isEmpty() || dlg.m_strProposedName.isEmpty())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Group Code And Name Must Be Specified!"));
		return;
	}
	


	Status err;
	DbRecordSet rowWrite=rowGroup;
	_SERVER_CALL(BusGroupTree->WriteGroupItem(err,nTreeID,rowWrite))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//add node...brb...
	rowGroup.assignRow(0,rowWrite,true);
	m_lstData.merge(rowGroup);

	
	
	//reload tree, restore expanded node
	RefreshDisplay(REFRESH_ITEM_ADD,rowGroup.getDataRef(0,"BGIT_ID").toInt());

	//cache:
	SaveGroupTreeIntoCache();


	//if single ndoe
	if (m_bIsFUISelectorWidget)					//allow edit only if in FUI
		if (ui.txtGroupNote->isReadOnly() && m_lstData.getRowCount()==1)   //re enable text
		{
			m_nLastGroupID=m_lstData.getDataRef(0,"BGIT_ID").toInt();
			if (m_bEditMode)
				ui.txtGroupNote->setReadOnly(false);
		}
}


//code & name can be renamed:
void Selection_GroupTree::OnGroupItemRename()
{

	bool ok;
	//Get selected node:
	int nRowSelected=m_lstData.getSelectedRow();
	if (nRowSelected==-1)
		 return;

	//get current data:
	int nTreeID=m_lstTrees.getDataRef(ui.cmbGroupTree->currentIndex(),"BGTR_ID").toInt();
	DbRecordSet	rowGroup=m_lstData.getRow(nRowSelected);
	QString strOldCode=rowGroup.getDataRef(0,"BGIT_CODE").toString();
	QString strCodeName=rowGroup.getDataRef(0,"BGIT_CODE").toString()+" "+rowGroup.getDataRef(0,"BGIT_NAME").toString();
	QString text = QInputDialog::getText(this, tr("Enter New Tree Name"),
		tr("Tree Name:"), QLineEdit::Normal,strCodeName, &ok);
	if (!ok)
		return;

	//find code:
	int nIndex=text.indexOf(" ",0);
	if(nIndex<=0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Group Code And Name Must Be Specified!"));
		return;
	}
	QString strCode=text.left(nIndex);
	QString strName=text.remove(0,nIndex+1);
	rowGroup.setData(0,"BGIT_CODE",strCode);
	rowGroup.setData(0,"BGIT_NAME",strName);

	//save to DB:
	Status err;
	DbRecordSet rowWrite=rowGroup;
	_SERVER_CALL(BusGroupTree->WriteGroupItem(err,nTreeID,rowWrite))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//add node...brb...
	rowGroup.assignRow(0,rowWrite,true);
	m_lstData.assignRow(nRowSelected,rowGroup);


	//reload whole tree if code changed:
	int nNodeID=rowGroup.getDataRef(0,"BGIT_ID").toInt();
	if (strOldCode!=strCode)
	{
		OnTreeRefreshDataFromServer();
		ui.treeWidget->SelectItemByRowID(nNodeID);
		ui.treeWidget->ExpandItemByRowID(nNodeID);
	}
	else
	{
		RefreshDisplay(REFRESH_ITEM_EDIT,nNodeID);
		SaveGroupTreeIntoCache();
	}


}


void Selection_GroupTree::OnGroupItemDelete()
{

	//Get selected node:
	int nRowSelected=m_lstData.getSelectedRow();
	if (nRowSelected==-1)
		return;

	//get current data:
	int nTreeID=m_lstTrees.getDataRef(ui.cmbGroupTree->currentIndex(),"BGTR_ID").toInt();
	//int nNodeID=m_lstData.getDataRef(nRowSelected,"BGIT_ID").toInt();

	int nResult=QMessageBox::question(this,tr("Warning"),tr("Do you really want to delete selected groups from the database?"),tr("Yes"),tr("No"));
	if (nResult!=0) return; //only if YES

	//copy selected:
	DbRecordSet lstForDelete;
	lstForDelete.addColumn(QVariant::Int,"BGIT_ID");
	lstForDelete.merge(m_lstData,true);

	//lstForDelete.Dump();

	//save to DB:
	Status err;
	_SERVER_CALL(BusGroupTree->DeleteGroupItem(err,nTreeID,lstForDelete))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	m_lstData.deleteSelectedRows();//nRowSelected);


	//reset desc:
	m_nLastGroupID=0;
	InvalidateGroupData();

	//reload whole tree if node have children, set to parent
	RefreshDisplay();//REFRESH_ITEM_DELETE,nNodeID);

	SaveGroupTreeIntoCache();
}




void Selection_GroupTree::RefreshDisplay(int nAction, int nPrimaryKeyValue)
{

	//always try to refresh to old nPrimaryKeyValue, if empty then all
	if(nPrimaryKeyValue>0)
	{
		if(ui.treeWidget->GetDataSource()->find(m_nPrimaryKeyColumnIdx,nPrimaryKeyValue,true)!=-1) //it is possible that it not exists, check it
		{
			ui.treeWidget->RefreshDisplay();
			ui.treeWidget->SelectItemByRowID(nPrimaryKeyValue);
			ui.treeWidget->ExpandItemByRowID(nPrimaryKeyValue);
			return;
		}
	}

	ui.treeWidget->RefreshDisplay();
}



//reload current content:
void Selection_GroupTree::OnTreeRefreshData()
{
	on_cmbGroupTree_currentIndexChanged(ui.cmbGroupTree->currentIndex());
}


void Selection_GroupTree::OnTreeExpandAll()
{
	ui.treeWidget->ExpandTree();
}


//------------------------------------------------------------------------------------
//						GROUP COTNENT
//------------------------------------------------------------------------------------


//-1 if invalid
int Selection_GroupTree::GetCurrentTreeID()
{
	int i=ui.cmbGroupTree->currentIndex();
	if (i<0) return -1;
	return m_lstTrees.getDataRef(i,"BGTR_ID").toInt();
}

//-1 if invalid
int Selection_GroupTree::GetCurrentGroupID(int &nRowSelected)
{
	nRowSelected=m_lstData.getSelectedRow();
	if (nRowSelected==-1)return -1;
	return m_lstData.getDataRef(nRowSelected,"BGIT_ID").toInt();

}


//only signal router:
void Selection_GroupTree::OnGroupAddSelected()
{
	Q_ASSERT(m_pLstGroupContent); //must be set to acess this functions:

	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) return;
	//get selected data:
	DbRecordSet lstData;
	lstData.copyDefinition(*m_pLstGroupContent);
	lstData.merge(*m_pLstGroupContent,true);
	if (lstData.getRowCount()==0)return;

	//send signal to group content holder to add group content data:
	QVariant varData;
	qVariantSetValue(varData,lstData);
	notifyObservers(GROUP_ADD_SELECTED,nRecordID,varData,this);
}

void Selection_GroupTree::OnGroupRemoveSelected()
{
	Q_ASSERT(m_pLstGroupContent); //must be set to acess this functions:

	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) return;
	//get selected data:
	DbRecordSet lstData;
	lstData.copyDefinition(*m_pLstGroupContent);
	lstData.merge(*m_pLstGroupContent,true);
	if (lstData.getRowCount()==0)return;

	//send signal to group content holder to add group content data:
	QVariant varData;
	qVariantSetValue(varData,lstData);
	notifyObservers(GROUP_REMOVE_SELECTED,nRecordID,varData,this);

}

void Selection_GroupTree::OnGroupReplaceSelected()
{
	Q_ASSERT(m_pLstGroupContent); //must be set to acess this functions:

	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) return;
	//get selected data:
	DbRecordSet lstData;
	lstData.copyDefinition(*m_pLstGroupContent);
	lstData.merge(*m_pLstGroupContent,true);
	if (lstData.getRowCount()==0)return;

	//send signal to group content holder to add group content data:

	QVariant varData;
	qVariantSetValue(varData,lstData);
	notifyObservers(GROUP_REPLACE_SELECTED,nRecordID,varData,this);

}

void Selection_GroupTree::OnGroupIntersectSelected()
{
	Q_ASSERT(m_pLstGroupContent); //must be set to acess this functions:

	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) return;
	//get selected data:
	DbRecordSet lstData;
	lstData.copyDefinition(*m_pLstGroupContent);
	lstData.merge(*m_pLstGroupContent,true);
	if (lstData.getRowCount()==0)return;

	//send signal to group content holder to add group content data:
	QVariant varData;
	qVariantSetValue(varData,lstData);
	notifyObservers(GROUP_INTERSECT_SELECTED,nRecordID,varData,this);

}


void Selection_GroupTree::OnGroupAddWholeList()
{
	Q_ASSERT(m_pLstGroupContent); //must be set to acess this functions:

	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) return;

	//send signal to group content holder to add group content data:
	QVariant varData;
	qVariantSetValue(varData,*m_pLstGroupContent);
	notifyObservers(GROUP_ADD_WHOLE_LIST,nRecordID,varData,this);

}

void Selection_GroupTree::OnGroupReplaceWholeList()
{
	Q_ASSERT(m_pLstGroupContent); //must be set to acess this functions:

	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) return;

	//send signal to group content holder to add group content data:
	QVariant varData;
	qVariantSetValue(varData,*m_pLstGroupContent);
	notifyObservers(GROUP_REPLACE_WHOLE_LIST,nRecordID,varData,this);

}






//------------------------------------------------------------------------------------
//	OVERRIDEN FOR MAINENTITYSELECTOR/SAPNE/POPUP SELECTORS (do not use cache)
//------------------------------------------------------------------------------------

bool Selection_GroupTree::ReloadFromServer()
{
	OnTreeRefreshDataFromServer();
	return true;
}

bool Selection_GroupTree::ReloadFromCache()
{
	return false;
}

/*
QString Selection_GroupTree::GetCalculatedName(int nEntityRecordID)
{
	int nRow=m_lstData.find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
	if (nRow==-1)
	{
		DbRecordSet *data=g_ClientCache.GetCache(m_nEntityID);
		if (data)
		{
			nRow=data->find(m_nPrimaryKeyColumnIdx,nEntityRecordID,true);
			if (nRow!=-1)
				return m_CalcName.GetCalculatedName(m_lstData,nRow);
		}
		return "";
	}
	else
		return m_CalcName.GetCalculatedName(m_lstData,nRow);
}
*/




void Selection_GroupTree::SlotSelectionChanged()
{
	//qDebug()<<m_lstData.getSelectedCount();

	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) 
	{
		//invalidate
		InvalidateGroupData();
		return;
	}
	else
	{
		if (m_bIsFUISelectorWidget)					//allow edit only if in FUI
			if (ui.txtGroupNote->isReadOnly() && m_bEditMode)   //re enable text
				ui.txtGroupNote->setReadOnly(false);
	}

		

	//m_lstData.Dump();
	
	//set ext category on context menu:
	if (m_pActExtCategory!=NULL)
	{
		if (!m_lstData.getDataRef(nRow,m_nExtCatIdx).toString().isEmpty())
			m_pActExtCategory->setText(tr("External Category")+" ("+m_lstData.getDataRef(nRow,m_nExtCatIdx).toString()+")");
		else
			m_pActExtCategory->setText(tr("External Category"));
	}

	
	//set desc:
	CheckDescription(nRecordID);

	//get all selected:
	DbRecordSet lstNodeIDs;
	lstNodeIDs.addColumn(QVariant::Int,"BGIT_ID");
	lstNodeIDs.merge(*ui.treeWidget->GetDataSource(),true);  //B.T. select also chidlren:

	//if item is expanded, send only selected item:
	QTreeWidgetItem *item=ui.treeWidget->GetItemByRowID(nRecordID);
	if (item->isExpanded())
	{
		lstNodeIDs.clear();
		lstNodeIDs.addRow();
		lstNodeIDs.setData(0,0,nRecordID);
	}

	//_DUMP(lstNodeIDs)

	QVariant varData;
	qVariantSetValue(varData,lstNodeIDs);
	notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID,varData); //send selected node + all his children

	//if dbl clik emit signal:
	//if(!m_bSingleClickIsSelection)
}

void Selection_GroupTree::GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord)
{

	ui.treeWidget->GetDropValue(lstEntityRecord);
	if(lstEntityRecord.getRowCount()>0)
	{
		if(m_nPrimaryKeyColumnIdx!=-1)nEntityRecordID=lstEntityRecord.getDataRef(0,m_nPrimaryKeyColumnIdx).toInt();
		if(m_nCodeColumnIdx!=-1)strCode=lstEntityRecord.getDataRef(0,m_nCodeColumnIdx).toString();
		if(m_nNameColumnIdx!=-1)strName=lstEntityRecord.getDataRef(0,m_nNameColumnIdx).toString();
	}

}

void Selection_GroupTree::GetSelection(DbRecordSet& lstEntityRecord)
{
	ui.treeWidget->GetDropValue(lstEntityRecord,true);
}

//when expanding group, notify to clear cumulation:
void Selection_GroupTree::OnItemExpanded(QTreeWidgetItem * item)
{
	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) return;

	DbRecordSet lstNodeIDs;
	lstNodeIDs.addColumn(QVariant::Int,"BGIT_ID");
	lstNodeIDs.addRow();
	lstNodeIDs.setData(0,0,nRecordID);

	QVariant varData;
	qVariantSetValue(varData,lstNodeIDs);
	notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID,varData); //send selected node + all his children


}

//when item is collapsed again: send sel chaneg signal:
void Selection_GroupTree::OnItemCollapsed(QTreeWidgetItem * item)
{
	SlotSelectionChanged();
}



//some1 is dropping contacts on grid: notify all
void Selection_GroupTree::OnTreeDropEvent()
{



	QTreeWidgetItem *item=ui.treeWidget->m_lastitem;
	if (item==NULL)return;
	DbRecordSet lstData=ui.treeWidget->m_lastDroppedValue;

	Q_ASSERT(m_pLstGroupContent); //must be set to access this functions:

	int nRecordID=ui.treeWidget->GetRowIDByItem(item);
	ui.treeWidget->clearSelection();
	ui.treeWidget->SelectItemByRowID(nRecordID);

	if (nRecordID==-1 || lstData.getRowCount()==0) return;


	if (ThemeManager::GetViewMode()!=ThemeManager::VIEW_SIDEBAR)
	{
		//only if FUI
		if (!m_bIsFUISelectorWidget)
			return;


		//send signal to group content holder to add group content data:
		QVariant varData;
		qVariantSetValue(varData,lstData);
		notifyObservers(GROUP_ADD_SELECTED,nRecordID,varData,this);
	}
	else
	{
		//if sidebar mode, directly write to server

		DbRecordSet lstGroupContent;
		lstGroupContent.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
		int nGroupIdx=lstGroupContent.getColumnIdx("BGCN_ITEM_ID");

		//list is in contact format: must contain BCNT_ID field
		int nContactIdx=lstData.getColumnIdx("BCNT_ID");
		lstData.setColumn(nContactIdx,QVariant::Int,"BGCN_CONTACT_ID");
		Q_ASSERT(nContactIdx!=-1);

		lstGroupContent.merge(lstData);
		lstGroupContent.setColValue(nGroupIdx,nRecordID);
		Status err;
		_SERVER_CALL(BusGroupTree->ChangeGroupContent(err,ENTITY_BUS_CONTACT,nRecordID,DataHelper::OP_ADD,lstGroupContent))
		_CHK_ERR(err);

		//issue 2743: refresh group tree:
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_REFRESH_GROUP_DATA_INSIDE_CONTACT_DETAILS,0);
	}



}

void Selection_GroupTree::OnExtCategoryEdit()
{
	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID==-1) return;

	bool ok;
	QString strCat=m_lstData.getDataRef(nRow,m_nExtCatIdx).toString();
	QString text = QInputDialog::getText(this, tr("Enter Group External Category"),
		tr("External Category:"), QLineEdit::Normal,strCat, &ok);
	if (!ok)
		return;
	
	//save
	Status err;
	_SERVER_CALL(BusGroupTree->WriteGroupItemData(err,nRecordID,text,m_lstData.getDataRef(nRow,m_nDescIdx).toString()))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//set to action & list:
	m_lstData.setData(nRow,m_nExtCatIdx,text);
	QString strExt=tr("External Category")+" ("+text+")";
	if (m_pActExtCategory)m_pActExtCategory->setText(strExt);
	SaveGroupTreeIntoCache();
}





void Selection_GroupTree::OnGroupImport()
{

	//open file dlg
	QString fileName = QFileDialog::getOpenFileName(this, tr("Import Groups"),g_strLastDir_FileOpen,tr("Import File (*.txt)"));
	//if empty exit
	if(fileName.isEmpty())
		return;
	if (!fileName.isEmpty())
	{
		QFileInfo fileInfo(fileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	
	//use import 
	QString strFormat = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Groups - Subformat: Items";
	DbRecordSet data;
	Status status;
	CsvImportFile import;

	QStringList lstFormatPlain;
	lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Groups - Subformat: Items";
	QStringList lstFormatUtf8;

	import.Load(status, fileName, data, lstFormatPlain, lstFormatUtf8);
	if(!status.IsOK()){
		QMessageBox::information(this, "Error", status.getErrorText());
		return;
	}
	//data.Dump();

	//check colcount:
	int nColCount=data.getColumnCount();
	if (nColCount>4 || nColCount<1)
	{
		QMessageBox::information(this, tr("Error"), tr("Invalid format: column count must be inside [1-4]!"));
		return;

	}

	//get parent:
	QString strParentCode="";
	QString strChildCode;

	int nRow;
	int nRecordID=GetCurrentGroupID(nRow);
	if (nRecordID!=-1) 
	{
		strParentCode=m_lstData.getDataRef(nRow,"BGIT_CODE").toString();
		strChildCode=strParentCode;
		HierarchicalHelper::GenerateChildCode(strChildCode);
	}
	
	
	//generate list for write

	DbRecordSet lstRecords;
	lstRecords.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_GROUP_ITEMS));

	int nNameIdx=lstRecords.getColumnIdx("BGIT_NAME");
	int nCodeIdx=lstRecords.getColumnIdx("BGIT_CODE");
	int nExtCatIdx=lstRecords.getColumnIdx("BGIT_EXTERNAL_CATEGORY");
	int nDescIdx=lstRecords.getColumnIdx("BGIT_DESCRIPTION");


	//data.Dump();

	//data.sort(nCodeIdx);//sort by code (if empty first)
	int nSize=data.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstRecords.addRow();
		int nRow=lstRecords.getRowCount()-1;
		lstRecords.setData(nRow,nNameIdx,data.getDataRef(i,0));
		if (nColCount<2) continue;
		QString strCode=data.getDataRef(i,1).toString();
		if(!strParentCode.isEmpty() && !strCode.isEmpty())
		{
			if (strParentCode.lastIndexOf(".")==strParentCode.length()-1) //if delimiter is last, do not add it:
				lstRecords.setData(nRow,nCodeIdx,QString(strParentCode+data.getDataRef(i,1).toString())); //take as subchild (server will decide)
			else
				lstRecords.setData(nRow,nCodeIdx,QString(strParentCode+"."+data.getDataRef(i,1).toString())); //take as subchild (server will decide)
		}
			
		else if(!strParentCode.isEmpty() && strCode.isEmpty())
		{
			lstRecords.setData(nRow,nCodeIdx,strChildCode);
			HierarchicalHelper::IncrementLastSegment(strChildCode);
		}
		else if(strParentCode.isEmpty() && strCode.isEmpty())
		{
			if (strChildCode.isEmpty())strChildCode="P.";			//set to new node
			lstRecords.setData(nRow,nCodeIdx,strChildCode);
			HierarchicalHelper::IncrementLastSegment(strChildCode);
		}
		else if(strParentCode.isEmpty() && !strCode.isEmpty())
		{
			lstRecords.setData(nRow,nCodeIdx,strCode);
		}
		else
			Q_ASSERT(false);

		if (nColCount<3) continue;
		lstRecords.setData(nRow,nExtCatIdx,data.getDataRef(i,2));
		if (nColCount<4) continue;
		lstRecords.setData(nRow,nDescIdx,data.getDataRef(i,3));
	}

	
	int nTreeID=GetCurrentTreeID();
	lstRecords.setColValue(lstRecords.getColumnIdx("BGIT_TREE_ID"),nTreeID);
	//lstRecords.Dump();

	//try to generate subnodes: or if parent=0, main node (click on empty)....brb...
	Status err;
	_SERVER_CALL(BusGroupTree->WriteGroupItem(err,nTreeID,lstRecords))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//reload tree:
	OnTreeRefreshDataFromServer();

	//if parent was, set it:
	if (nRecordID!=-1)
	{
		ui.treeWidget->SelectItemByRowID(nRecordID);
		ui.treeWidget->ExpandItemByRowID(nRecordID);
	}


}




//save desc if id is changed: to invalidate set to 0;
void Selection_GroupTree::CheckDescription(int nGroupIDSelected)
{
	//save old only if old valid & text changed:
	if (m_nLastGroupID!=0 && m_bEditMode)
	{
		int nRow= m_lstData.find(0,m_nLastGroupID,true);
		Q_ASSERT(nRow!=-1);
		if (nRow==-1)return;
		if (ui.txtGroupNote->toHtml()!=m_lstData.getDataRef(nRow,m_nDescIdx).toString())
		{
			//try save desc:
			Status err;
			_SERVER_CALL(BusGroupTree->WriteGroupItemData(err,m_nLastGroupID,m_lstData.getDataRef(nRow,m_nExtCatIdx).toString(),ui.txtGroupNote->toHtml()))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
			m_lstData.setData(nRow,m_nDescIdx,ui.txtGroupNote->toHtml());
			SaveGroupTreeIntoCache();
		}
	}
				
	//assign new:
	m_nLastGroupID=nGroupIDSelected;
	int nRow= m_lstData.find(0,m_nLastGroupID,true);

	//display:
	if (nRow==-1)
	{
		ui.txtGroupNote->setHtml("");
		//set on current node as tooltip:
		QTreeWidgetItem *item=ui.treeWidget->currentItem();
		if (item)
			item->setToolTip(0,"");
	}
	else
	{
		QString strDesc=m_lstData.getDataRef(nRow,m_nDescIdx).toString();
		ui.txtGroupNote->setHtml(strDesc);
		//set on current node as tooltip:
		QTreeWidgetItem *item=ui.treeWidget->currentItem();
		if (item)
			item->setToolTip(0,strDesc);
	}

}


void Selection_GroupTree::InvalidateGroupData()
{
	//invalidate text
	CheckDescription(0);
	if (m_pActExtCategory)m_pActExtCategory->setText(tr("External Category"));
	ui.txtGroupNote->setReadOnly(true);


	//notify group content:

	//invalidate group content:
	int nRecordID=-1;

	DbRecordSet lstNodeIDs;
	lstNodeIDs.addColumn(QVariant::Int,"BGIT_ID");
	lstNodeIDs.addRow();
	lstNodeIDs.setData(0,0,nRecordID);

	QVariant varData;
	qVariantSetValue(varData,lstNodeIDs);
	notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID,varData); //send selected node + all his children
}



//load tree:
void Selection_GroupTree::LoadTree(bool bReloadFromServer)
{
	m_lstTrees.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_GROUP_TREE));


	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_BUS_CONTACT_TREES);
		if (plstCacheData)
		{
			m_lstTrees=*plstCacheData;
			return;
		}
	}


	//SERVER:
	Status err;
	_SERVER_CALL(BusGroupTree->ReadTree(err,m_nEntityID,m_lstTrees))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}

	//m_lstTrees.Dump();

	//Store in Cache:
	g_ClientCache.SetCache(ENTITY_BUS_CONTACT_TREES,m_lstTrees);

}


void Selection_GroupTree::OnTreeReloadFromServer()
{
	LoadTree(true);
	OnTreeReload();
}



void Selection_GroupTree::LoadGroupTree(int nTreeID,bool bReloadFromServer)
{

	m_nCurrTreeID=nTreeID;

	m_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_GROUP_ITEMS));


	if (!bReloadFromServer)
	{
		//CACHE:
		DbRecordSet *plstCacheData=g_ClientCache.GetCache(ENTITY_BUS_CONTACT_GROUPS);
		if (plstCacheData)
		{
			int nRow=plstCacheData->find(0,nTreeID,true);
			if (nRow!=-1)
			{
				m_lstData=plstCacheData->getDataRef(nRow,1).value<DbRecordSet>();
				//_DUMP(m_lstData);
				return;
			}
		}
	}


	//SERVER:
	Status err;
	_SERVER_CALL(BusGroupTree->ReadGroupTree(err,nTreeID,m_lstData))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}


	//store into cache:
	//if (m_lstData.getRowCount()>0)
	//{
		//Store in Cache:
		DbRecordSet lstCacheData;
		lstCacheData.addColumn(QVariant::Int,"TreeID");
		lstCacheData.addColumn(DbRecordSet::GetVariantType(),"Items");
		lstCacheData.addRow();
		lstCacheData.setData(0,0,nTreeID);
		lstCacheData.setData(0,1,m_lstData);
		g_ClientCache.ModifyCache(ENTITY_BUS_CONTACT_GROUPS,lstCacheData,DataHelper::OP_EDIT,this,true);
	//}

	



}

void Selection_GroupTree::SaveGroupTreeIntoCache()
{
	int nTreeID=GetCurrentTreeID();
	if (nTreeID<=0 || m_lstData.getRowCount()==0) return;

	DbRecordSet lstCacheData;
	lstCacheData.addColumn(QVariant::Int,"TreeID");
	lstCacheData.addColumn(DbRecordSet::GetVariantType(),"Items");
	lstCacheData.addRow();
	lstCacheData.setData(0,0,nTreeID);
	lstCacheData.setData(0,1,m_lstData);
	g_ClientCache.ModifyCache(ENTITY_BUS_CONTACT_GROUPS,lstCacheData,DataHelper::OP_EDIT,this,true);

}



void Selection_GroupTree::OnTreeRefreshDataFromServer()
{
	//load data
	int nTreeID=GetCurrentTreeID();
	LoadTree(true);
	LoadGroupTree(nTreeID,true);

	//refresh display:
	int nCurrentIndex=m_lstTrees.find("BGTR_ID",nTreeID,true);
	if (nCurrentIndex<0)nCurrentIndex=0;
	OnTreeReload(nCurrentIndex);
	
}


void Selection_GroupTree::OnSetGroupLoad()
{
	
	if (!m_pActGroupLoad->isChecked())
	{
		OnClearGroupLoad();
		return;
	}
	

	int nRowSelected=m_lstData.getSelectedRow();
	if (nRowSelected==-1) return;

	//set name:
	QString strGroupName=m_lstData.getDataRef(nRowSelected,"BGIT_CODE").toString()+" "+m_lstData.getDataRef(nRowSelected,"BGIT_NAME").toString();
	if(m_pActGroupLoad)m_pActGroupLoad->setText(tr("Startup: Load in Actual Contact List (")+strGroupName+")");

	//save id:
	//g_pSettings->SetPersonSetting(CONTACT_DEF_GROUP_TREE_FOR_LOAD,GetCurrentTreeID());
	int i;
	g_pSettings->SetPersonSetting(CONTACT_DEF_GROUP_FOR_LOAD,GetCurrentGroupID(i));
}

void Selection_GroupTree::OnClearGroupLoad()
{
	if(m_pActGroupLoad)m_pActGroupLoad->setText(tr("Startup: Load in Actual Contact List"));
	//g_pSettings->SetPersonSetting(CONTACT_DEF_GROUP_TREE_FOR_LOAD,-1);
	g_pSettings->SetPersonSetting(CONTACT_DEF_GROUP_FOR_LOAD,-1);
}


bool Selection_GroupTree::SetSelectionOnGroup(int GroupID, int nTreeID)
{
	int nCurrentIndex=m_lstTrees.find("BGTR_ID",nTreeID,true);
	if (nCurrentIndex<0)
		return false;
	OnTreeReload(nCurrentIndex);

	int nRow=m_lstData.find("BGIT_ID",GroupID,true);
	if (nRow!=-1)
	{
		ui.treeWidget->SelectItemByRowID(GroupID);
		ui.treeWidget->ExpandItemByRowID(GroupID);
		SlotSelectionChanged();
		return true;
	}
	else
		return false;
}



void Selection_GroupTree::SlotSelectionChanged_DoubleClicked()
{
	SlotSelectionChanged();
	notifyObservers(SelectorSignals::SELECTOR_ON_DOUBLE_CLICK);
}

QString Selection_GroupTree::GetGroupName(int nNodeID)
{
		int nRow=m_lstData.find("BGIT_ID",nNodeID,true);
		if (nRow!=-1)
		{
			return m_lstData.getDataRef(nRow,"BGIT_CODE").toString()+" "+m_lstData.getDataRef(nRow,"BGIT_NAME").toString();
		}
		else
		{
			QString strSQL=QString("SELECT BGIT_CODE, BGIT_NAME FROM bus_group_items WHERE BGIT_ID=%1").arg(nNodeID);
			Status err;
			DbRecordSet lstRet;
			_SERVER_CALL(ClientSimpleORM->ExecuteSQL(err,strSQL,lstRet));
			if (err.IsOK() && lstRet.getRowCount()>0)
			{
				return lstRet.getDataRef(0,"BGIT_CODE").toString()+" "+lstRet.getDataRef(0,"BGIT_NAME").toString();
			}
		}

		return "";
}

void Selection_GroupTree::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{
	if (pSubject==&g_ChangeManager && nMsgCode==ChangeManager::GLOBAL_THEME_CHANGED)
	{
		ui.treeWidget->setStyleSheet(ThemeManager::GetTreeBkg());
		return;
	}
	MainEntitySelectionController::updateObserver(pSubject,nMsgCode,nMsgDetail,val);
}

QString Selection_GroupTree::GetTreeName(int nTreeID)
{
	int nRow=m_lstTrees.find(0,nTreeID,true);
	if (nRow>=0)
		return m_lstTrees.getDataRef(nRow,"BGTR_NAME").toString();
	else
		return "";
}

void Selection_GroupTree::SaveExpandedTreeState()
{
	if (m_nCurrTreeID==-1)
		return;

	QList<int> lstExpanded;
	int nSize=m_lstData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nNodeID=m_lstData.getDataRef(i,"BGIT_ID").toInt();
		QTreeWidgetItem *item=ui.treeWidget->GetItemByRowID(nNodeID);
		if (item)
			if (item->isExpanded())
			{
				lstExpanded.append(nNodeID);
			}
	}

	m_lstExpandedItems[m_nCurrTreeID]=lstExpanded;

}

void Selection_GroupTree::LoadExpandedTreeState()
{
	if (m_nCurrTreeID==-1)
		return;

	QList<int> lstExpanded = m_lstExpandedItems.value(m_nCurrTreeID,QList<int>());

	int nSize=lstExpanded.size();
	ui.treeWidget->blockSignals(true);
	for (int i=0;i<nSize;i++)
	{
		QTreeWidgetItem *item= ui.treeWidget->GetItemByRowID(lstExpanded.at(i));
		if (item)
			item->setExpanded(true);
	}
	ui.treeWidget->blockSignals(false);


}