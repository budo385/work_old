#include "clientmanagerext.h"
#include "common/common/datahelper.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/logindlg.h"
#include "gui_core/gui_core/logindlg_embedded.h"
#include "common/common/config_version_sc.h"
#include "dlg_trialpopup.h"
#include <QDesktopServices>
#include "common/common/config.h"
#include "dlg_orderform.h"
#include "trans/trans/tcphelper.h"
#include "languagewarningdlg.h"
#include "selectionpopup.h"

extern QTranslator *g_translator;

#define  URL_ORDER_FORM			"http://www.sokrates-communicator.ch/"
#define  URL_UPDATE_CHECK		"http://www.sokrates-communicator.com/version/"
#define	 LOGIN_MAX_TRIES		4

//ESEENTIAL GLOBALS:
#include "bus_trans_client/bus_trans_client/businessservicemanager_thinclient.h"
//#include "businessservicemanager_thickclient.h"
BusinessServiceManager *g_pBoSet;
#include "modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "common/common/logger.h"
extern Logger					g_Logger;
#include "bus_client/bus_client/useraccessright_client.h"
extern UserAccessRight *g_AccessRight;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "bus_client/bus_client/clientdownloadmanager.h"
extern ClientDownloadManager		*g_DownloadManager;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_core/bus_core/serverremindermanagerabstract.h"
#include "bus_client/bus_client/clientremindermanager.h"
extern ClientReminderManager		*g_ReminderManager;


//LOCAL STATIC VARS:
#include "clientbackupmanager.h"
#include "clientimportexportmanager.h"

BackupManager*					g_pBackupManager=NULL;			//globa: as used in thick/thin  mode & bus_server 
ImportExportManagerAbstract*	g_pImportExportManager=NULL;	//import/export manager
ServerReminderManagerAbstract*	g_pReminderManager=NULL;		//just empty to make happy server_side objects...hehe

ClientManagerExt::ClientManagerExt(QObject *parent)
	: QObject(parent),m_VersionParser(URL_UPDATE_CHECK),m_nStartUpWizardEnabled(0),m_nGUICloseRetries(0),m_bIsLogged(false),m_pLoginDlg(NULL),m_nCurrentLoginTry(0),
	m_pSplashLogin(NULL),m_bLoginDlgShown(false),m_prgProgressDialogTransfer(NULL),m_bShowProgressDlg(false),m_nClientTimeZoneOffsetMinutes(0)
{
	QDir dirApp(QCoreApplication::applicationDirPath());
	QCoreApplication::instance()->setApplicationName(dirApp.dirName());		//for settings
}

ClientManagerExt::~ClientManagerExt()
{
	//service not started:
	if (!g_pBoSet)
		return;

	m_INIFile.Save();
	Logout();
	g_Logger.FlushFileLog();

	if (g_pBackupManager) 
	{
		delete g_pBackupManager;
		g_pBackupManager=NULL; 
	}
	if (g_DownloadManager){delete g_DownloadManager; g_DownloadManager=NULL;};
	if (g_ReminderManager){delete g_ReminderManager; g_ReminderManager=NULL;};
	if (g_pImportExportManager) {delete g_pImportExportManager;g_pImportExportManager=NULL;}
	if (g_AccessRight){delete g_AccessRight;g_AccessRight=NULL;}
	if (g_pSettings){delete g_pSettings;g_pSettings=NULL;}
	if (g_pBoSet){delete g_pBoSet; g_pBoSet=NULL;}
	if (g_translator){delete g_translator; g_translator=NULL;}
	//if (m_prgProgressDialogTransfer) delete m_prgProgressDialogTransfer;
	
	//if (m_prgProgressDialogTransfer) delete m_prgProgressDialogTransfer;
	m_prgProgressDialogTransfer->deleteLater();
}

void ClientManagerExt::Initialize(Status &pStatus, bool bEnableBcpManager,bool bEnableImportExportManager,FN_CREATE_THICK_CLIENT pfCallBack_CreateThick)
{
	pStatus.setError(0);
	QString strSettings=CheckSettingsDirectory(pStatus);
	
#if 0
	//OSX debugging
	FILE *pOut = fopen("/Users/helix/dump.txt", "w");
	if(pOut){
		fprintf(pOut, "******* Settings Dir path: [%s]\n", strSettings.toLatin1().constData());
		fclose(pOut);
	}
#endif

	if (!pStatus.IsOK()) return;

	//make transfer progress dialog:
	m_prgProgressDialogTransfer = new QProgressDialog;
	QSize size(400,100);
	m_prgProgressDialogTransfer->setWindowTitle(tr("Operation In Progress"));
	m_prgProgressDialogTransfer->setMinimumSize(size);
	m_prgProgressDialogTransfer->setWindowModality(Qt::ApplicationModal);
	m_prgProgressDialogTransfer->setMinimumDuration(2000);//2s before pops up
	m_prgProgressDialogTransfer->setCancelButtonText(tr("Cancel"));
	m_prgProgressDialogTransfer->setRange(0,100);
	m_prgProgressDialogTransfer->setAutoClose(false);
	HideTransferProgressDlg(); //patch for QT5.5, explicitly hide little mofo

	//load INI file:
	//------------------------------------------
	QString strIniFilePath = strSettings+"/client.ini";
	bool bOK=m_INIFile.Load(strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,tr("INI file is missing,corrupted or has invalid values!"));
		return;
	}

	m_bIsThinClient = m_INIFile.m_nIsThinClient;
	m_strModuleCode = m_INIFile.m_strModuleCode;
	m_strMLIID		= m_INIFile.m_strMLIID;
	DataHelper::SetPortableVersion((bool)m_INIFile.m_nPortableVersion);
	//DataHelper::SetPortableVersion(true);

	m_VersionParser.SetAbsoluteUpdateFileURL(m_INIFile.m_strUpdateSetupPath);
	
	if (bEnableBcpManager)
	{
		g_pBackupManager= new ClientBackupManager();
		ClientBackupManager* pBackupManager=dynamic_cast<ClientBackupManager*>(g_pBackupManager);
		pBackupManager->SetClientMode(m_bIsThinClient);
	}
	//import export at end:
	if (bEnableImportExportManager)
	{
		g_pImportExportManager= new ClientImportExportManager();
		ClientImportExportManager* pManager=dynamic_cast<ClientImportExportManager*>(g_pImportExportManager);
		pManager->SetClientMode(m_bIsThinClient);
	}


	//----------------------------------------------
	//Start logger 
	//----------------------------------------------
	if (m_INIFile.m_nLogLevel>LoggerAbstract::LOG_LEVEL_NONE)
	{
		if (m_INIFile.m_nLogTargetFile)
			g_Logger.EnableFileLogging(strSettings+"/client.log",m_INIFile.m_nLogBufferSize,m_INIFile.m_nLogMaxSize);
		if (m_INIFile.m_nLogTargetConsole)
			g_Logger.EnableConsoleLogging();
	}
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);



	//make thin or thick client:
	/*
#ifndef SP_NO_THICK_CLIENT
	if (!m_bIsThinClient)
	{
		//g_pBoSet= new BusinessServiceManager_ThickClient;
		if (!pfCallBack_CreateThick)
		{
			pStatus.setError(1,tr("Failed to create thick client"));
			return;
		}
		pfCallBack_CreateThick(pStatus);
		if (!g_pBoSet)
		{
			pStatus.setError(1,tr("Failed to create thick client"));
			return;
		}
		//g_pBoSet->Thick_Initialize(pStatus,&m_INIFile,dynamic_cast<ClientBackupManager*>(g_pBackupManager));
		if (!pStatus.IsOK())
			return;
	}
	else
#endif
*/
	{
		g_pBoSet= new BusinessServiceManager_ThinClient;
		BusinessServiceManager_ThinClient *pThin=dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet);
		connect(pThin,SIGNAL(CommunicationFatalError(int)),this,SLOT(OnCommunicationFatalError(int)));
		connect(pThin,SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SIGNAL(CommunicationInProgress(int,qint64,qint64)));
		connect(pThin,SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
		connect(pThin,SIGNAL(CommunicationServerMessageRecieved(DbRecordSet)),this,SLOT(OnServerMessageRecieved(DbRecordSet)));
	}

	g_AccessRight = new UserAccessRight_Client;
	g_pSettings = new ClientOptionsAndSettingsManager;
	g_DownloadManager = new ClientDownloadManager;
	g_ReminderManager = new ClientReminderManager;

	//must be before
	ThemeManager::SetViewMode(m_INIFile.m_nLastView);
	ThemeManager::SetCurrentThemeID(m_INIFile.m_nLastTheme);
}


QString ClientManagerExt::CheckSettingsDirectory(Status &pStatus)
{
	//settings are stored in application home, path
	//if directory does not exists, first time, all files will be copied there
	pStatus.setError(0);
	bool bCreated=false;
	QString strSettingsDir;
	if (QFile::exists(QCoreApplication::applicationDirPath()+"/template_settings/version_portable"))
	{
		strSettingsDir=QCoreApplication::applicationDirPath()+"/settings";
		QDir setDir(strSettingsDir);
		if (!setDir.exists())
		{
			QString strAppDir=QCoreApplication::applicationDirPath();
			QDir bcpDirCreate(strAppDir);
			if(bcpDirCreate.mkdir("settings"))
			{
				bCreated=true;
				strSettingsDir=strAppDir+"/settings";
			}
			else
			{
				pStatus.setError(1,tr("Settings directory:")+strAppDir+"/settings"+tr(" could not be created!"));
				return "";
			}
		}

		//if just created copy from template to that dir:
		//it will copy only missing files (coz installation now creates directory)
		DataHelper::CopyFolderContent(QCoreApplication::applicationDirPath()+"/template_settings",strSettingsDir);
	}
	else
	{
		strSettingsDir=DataHelper::GetApplicationHomeDir()+"/settings";
		QDir setDir(strSettingsDir);
		if (!setDir.exists())
		{
			QString strAppDir=DataHelper::GetApplicationHomeDir();
			QDir bcpDirCreate(strAppDir);
			if(bcpDirCreate.mkdir("settings"))
			{
				bCreated=true;
				strSettingsDir=strAppDir+"/settings";
			}
			else
			{
				pStatus.setError(1,tr("Settings directory:")+strAppDir+"/settings"+tr(" could not be created!"));
				return "";
			}
		}

		//if just created copy from template to that dir:
		//it will copy only missing files (coz installation now creates directory)
		DataHelper::CopyFolderContent(QCoreApplication::applicationDirPath()+"/template_settings",strSettingsDir);
	}
	
	return strSettingsDir;
}



void ClientManagerExt::Login()
{
	Q_ASSERT(!m_pLoginDlg);

	//try logout first:
	if (IsLogged()) Logout(); 
	m_nModeLogin						= LoginDialogAbstract::LOGIN_DLG_DEFAULT;
	QString strDefaultUserName			= m_INIFile.m_strLastUserName;
	QString strDefaultConnectionName	= m_INIFile.m_strConnectionName; 

#ifdef _DEBUG
	if(strDefaultConnectionName.isEmpty())
		strDefaultConnectionName="Administrator";
#endif
	if (ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR)
		m_nModeLogin=LoginDialogAbstract::LOGIN_DLG_SIDEBAR;

#ifdef WINCE 
	m_nModeLogin=LoginDialogAbstract::LOGIN_DLG_EMBEDDED;
#endif //WINCE

	Status err;
	QString strIni = DataHelper::GetApplicationHomeDir();
	if (m_bIsThinClient)
	{
		strIni += "/settings/network_connections.cfg";
	}
	else
		strIni += "/settings/database_connections.cfg";

	//initialize login dlg:
	m_pLoginDlg=NULL;
	if (m_nModeLogin==LoginDialogAbstract::LOGIN_DLG_EMBEDDED)
	{
		m_pLoginDlg=new LoginDlg_Embedded(QApplication::activeWindow());
		connect(m_pLoginDlg,SIGNAL(OnLoginClick()),this,SLOT(OnLoginClick()));
		connect(m_pLoginDlg,SIGNAL(OnCloseClick()),this,SLOT(OnLoginCloseClick()));
		connect(m_pLoginDlg,SIGNAL(destroyed(QObject *)),this,SLOT(OnLoginDialogDestroyed()));
	}
	else 
	{
		m_pLoginDlg=new LoginDlg(); //QApplication::activeWindow());
		connect(m_pLoginDlg,SIGNAL(OnLoginClick()),this,SLOT(OnLoginClick()));
		connect(m_pLoginDlg,SIGNAL(OnCloseClick()),this,SLOT(OnLoginCloseClick()));
		connect(m_pLoginDlg,SIGNAL(destroyed(QObject *)),this,SLOT(OnLoginDialogDestroyed()));
	}
	m_pLoginDlg->Initialize(strIni,(int)(ThemeManager::GetViewMode()==ThemeManager::VIEW_SIDEBAR),m_bIsThinClient);
	m_pLoginDlg->RefreshConnectionsList();
	m_pLoginDlg->SetDefaults(strDefaultUserName,strDefaultConnectionName);
	if (m_nModeLogin==LoginDialogAbstract::LOGIN_DLG_SIDEBAR)
	{
		m_pLoginDlg->setModal(false);
	}
	emit LoginDialogCreated(m_pLoginDlg);

	// allow up to 6 login attempts before closing the app
	m_nCurrentLoginTry = 0;
	bool bLogged = false;
	bool bTrySilentLogin=false;
	ShowLoginDialog();
}

void ClientManagerExt::OnLoginClick()
{
	Status err;

	//----------------------------------------LOOP LOGIN DLG----------------------------------

	m_nCurrentLoginTry ++;
	if (m_nCurrentLoginTry > LOGIN_MAX_TRIES)
	{
		QMessageBox::warning(NULL, tr("Error Message"), tr("You have reached maximum number of login attempts!\nThe application will be closed now."));
		CloseLoginDalog(m_pLoginDlg);
		emit ClientClosedLoginWnd();
		QApplication::closeAllWindows();
		return;
	}

	//BT: 12.05.2009: splash hides possible messasges, so started just before FUI open
	//start splash
	//g_pClientManager->StartProgressDialog(tr("Login in Progress..."));

	err.setError(0);
	Q_ASSERT(g_pBoSet!=NULL);	//intercept

	//if service already started, it will be stopped!
	if (m_bIsThinClient)
	{
		//dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet)->SetSettings(m_pLoginDlg->GetNetCurrentConnection());
		HTTPClientConnectionSettings settings = m_pLoginDlg->GetNetCurrentConnection();
		g_pBoSet->SetSettings(settings);
		SetConnectionName(m_pLoginDlg->GetNetCurrentConnection().m_strCustomName);
		SetConnectionIPAddress(dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet)->GetServerIPAddress());
	}
#ifndef SP_NO_THICK_CLIENT
	else
	{
		//dynamic_cast<BusinessServiceManager_ThickClient*>(g_pBoSet)->SetSettings(m_pLoginDlg->GetDbCurrentConnection());
		DbConnectionSettings settings = m_pLoginDlg->GetDbCurrentConnection();
		g_pBoSet->SetSettings(settings);
		SetConnectionName(m_pLoginDlg->GetDbCurrentConnection().m_strCustomName);
	}
#endif
	SetLoggedUserName(m_pLoginDlg->m_strUser);

	g_pBoSet->StartServiceSet(err);
	if(!err.IsOK())	
	{			
		ProcessAfterStop_BusinessService(true);
		QMessageBox::warning(NULL, tr("Error"), err.getErrorText());
		
		//terminate splash
		m_procSplashApp.terminate();
		//if (m_pSplashLogin){delete m_pSplashLogin; m_pSplashLogin=NULL;}	//remove splash
		//QTimer::singleShot(0,this,SLOT(ShowLoginDialog()));
		ShowLoginDialog();
		return;
	}
	else
		ProcessAfterStart_BusinessService();

	//login:
	BusinessLogin(err,m_pLoginDlg->m_strUser,m_pLoginDlg->m_strPass);
	if(!err.IsOK())	
	{
		//BT: after login error destroy socket!!!!
		Status errTemp;
		g_pBoSet->StopService(errTemp,!m_LastFatalError.IsOK());
		ProcessAfterStop_BusinessService();

		//show error message (do not show if version mismatch:9
		if (err.getErrorCode()==StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH)
		{
			//close application:
			//terminate splash
			m_procSplashApp.terminate();
			//if (m_pSplashLogin){delete m_pSplashLogin; m_pSplashLogin=NULL;}
			CloseLoginDalog(m_pLoginDlg);
			QApplication::closeAllWindows();
			return;
		}
		else if (err.getErrorCode()==StatusCodeSet::ERR_SYSTEM_SILENT_LOGIN_AGAIN)
		{
			//QTimer::singleShot(0,this,SLOT(ShowLoginDialog()));
			//terminate splash
			m_procSplashApp.terminate();
			//if (m_pSplashLogin){delete m_pSplashLogin; m_pSplashLogin=NULL;}
			ShowLoginDialog();
			return;
			//continue;	// loop again as we encounter d'error
		}
		else
		{
			//terminate splash
			m_procSplashApp.terminate();
			//if (m_pSplashLogin){delete m_pSplashLogin; m_pSplashLogin=NULL;}
			QMessageBox::warning(NULL, tr("Error Message"), err.getErrorText());
			m_INIFile.m_nHideLogin=0;							//<----disable hide login in case of error
			//QTimer::singleShot(0,this,SLOT(ShowLoginDialog()));
			ShowLoginDialog();
			return;
			//continue;	// loop again as we encounter d'error
		}
	}
	else
	{
		m_bIsLogged	= true;		 
	}
	
	//----------------------------------------LOOP LOGIN DLG----------------------------------
	
	CloseLoginDalog(m_pLoginDlg);

	if(m_bIsLogged)
	{
		StartProgressDialog(tr("Login in Progress..."));
		emit ClientLogged();	//notify successful login
		m_procSplashApp.terminate();
		//if (m_pSplashLogin){delete m_pSplashLogin; m_pSplashLogin=NULL;}
	}
	else	
	{
		//terminate splash
		m_procSplashApp.terminate();
		//if (m_pSplashLogin){delete m_pSplashLogin; m_pSplashLogin=NULL;}
		QMessageBox::warning(NULL, tr("Error Message"), tr("You have reached maximum number of login attempts!\nThe application will be closed now."));
		QApplication::closeAllWindows();
	}

	return;
}

void ClientManagerExt::OnLoginCloseClick()
{
#ifndef WINCE
	//if (m_pSplashLogin){delete m_pSplashLogin; m_pSplashLogin=NULL;}
	//terminate splash
	m_procSplashApp.terminate();
#endif
	CloseLoginDalog(m_pLoginDlg);
	emit ClientClosedLoginWnd();
}

void ClientManagerExt::OnLoginDialogDestroyed()
{
	m_pLoginDlg=NULL;
}

void ClientManagerExt::ShowLoginDialog()
{
	bool bSilentLogin = (bool)m_INIFile.m_nHideLogin;
	//if (m_bLoginDlgShown)bSilentLogin=false;
	//m_bLoginDlgShown=true;
	if (bSilentLogin) //jump on event:
	{
		OnLoginClick();
		return;
	}

	if (!bSilentLogin && m_nModeLogin != LoginDialogAbstract::LOGIN_DLG_SIDEBAR)
		QTimer::singleShot(0,dynamic_cast<QDialog *>(m_pLoginDlg),SLOT(exec()));

#ifdef WINCE
	m_pLoginDlg->showFullScreen();
#endif

	m_pLoginDlg->SetPasswordFocus();

}




void ClientManagerExt::Logout()
{
	Status err;

	if(!m_bIsLogged) 
	{
		if (g_pBoSet->IsServiceStarted()) 			//if not logged in, stops services: when application exists this ensures safe DBO connection deletion.(in PE mode at least)..and kills all timers...
		{
			g_pBoSet->StopService(err,!m_LastFatalError.IsOK());
			if(!err.IsOK())	
				QMessageBox::warning(NULL, tr("Error"), err.getErrorText());
			ProcessAfterStop_BusinessService();
		}
		return;					
	}

	BusinessLogout(err);
	if(!err.IsOK())	
		QMessageBox::warning(NULL, tr("Error"), err.getErrorText());
	else
	{
		emit ClientLogout();	
		g_pBoSet->StopService(err,!m_LastFatalError.IsOK());
		if(!err.IsOK())	
			QMessageBox::warning(NULL, tr("Error"), err.getErrorText());

		ProcessAfterStop_BusinessService();
	}
}


void ClientManagerExt::CloseLoginDalog(QDialog *pDlg)
{
	if (pDlg)
	{
		emit LoginDialogBeforeDestroy(pDlg);
		delete pDlg;
		m_pLoginDlg=NULL;
	}

}



void ClientManagerExt::BusinessLogin(Status &pStatus,QString pStrUserName,QString pStrPassword)
{

	Q_ASSERT(g_pBoSet!=NULL);	//intercept
	Q_ASSERT(!m_bIsLogged); //already logged
	pStatus.setError(0);
	ClearLastFatalError();


	int nClientTimeZone=DataHelper::GetTimeZoneOffsetUTC(); //this is offset from UTC as reference for server.
	//login:
	g_pBoSet->app->UserLogon->Login(pStatus,pStrUserName,pStrPassword,m_strModuleCode,m_strMLIID,"",nClientTimeZone);
	if(!pStatus.IsOK())
	{
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
			AskForUpdate("");
		}
		return;
	}


	//check APP version:
	QString strAppServerVersion,strAppServerDatabaseVersion,strTemplateInUse;
	int nAskForTemplate;
	QByteArray byteDemoKey;
	//int Ret_nClientTimeZoneOffsetMinutes; //this is actual difference from clien to server time-> use only in special messages from server, as XML, RPc, REST and other layer are automatically converted
	g_pBoSet->app->CoreServices->CheckVersion(pStatus,QString(APPLICATION_VERSION),strAppServerVersion,strAppServerDatabaseVersion,strTemplateInUse,nAskForTemplate,m_nStartUpWizardEnabled,byteDemoKey,m_nClientTimeZoneOffsetMinutes);
	if(!pStatus.IsOK())
	{
		Status err;
		g_pBoSet->app->UserLogon->Logout(err);
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
			AskForUpdate(strAppServerVersion);
		}
		return;
	}

	if (strAppServerVersion!=QString(APPLICATION_VERSION))
	{
		Status err;
		g_pBoSet->app->UserLogon->Logout(err);
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
		AskForUpdate(strAppServerVersion);
		return;
	}


	//check DB:
	if (strAppServerDatabaseVersion!=QVariant(DATABASE_VERSION).toString())
	{
		g_pBoSet->app->UserLogon->Logout(pStatus);	//logout first:
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
		//TEMPORARY: ask version:
		QString strMsg=tr("Database version do not match, notify administrator!");
		int nResult=QMessageBox::information(NULL,tr("Warning"),strMsg);
		return;
	}


	//inits global objects
	InitializeGlobalObjects(pStatus);
	if(!pStatus.IsOK())
	{
		Status err;
		g_pBoSet->app->UserLogon->Logout(err);
		return;
	}

	//check for updates (only for THICK):issue 1099 + 1378
	if (!m_bIsThinClient)
		if(g_pSettings->GetPersonSetting(THICK_CHECK_FOR_UPDATES).toBool())
			if (CheckForUpdatesThickClient()==2)
			{
				Status err;
				pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
				ClearGlobalObjects(err,true);
				return;
			}


	//reset flag if logged:
	m_INIFile.m_nOrderAsk=0;	


	//--------------------------------------------------------
	// CHECK FOR END SUBSCRIPTION PERIOD: must be after login
	//--------------------------------------------------------
	if (!CheckSubscriptionPeriod())
	{
		Status err;
		g_pBoSet->app->UserLogon->Logout(err);
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);		//quit application
		ClearGlobalObjects(err,true);
		return;
	}



	//-------------------------------------------------
	// CHECK FOR TEMPLATE DB: check for template database
	//-------------------------------------------------
	if (nAskForTemplate && g_pBackupManager)
	{
		ClientBackupManager* pBackupManager=dynamic_cast<ClientBackupManager*>(g_pBackupManager);
		if(pBackupManager->AskForTemplateDatabase())
		{
			Status err;
			//store username, right now!
			m_INIFile.m_strLastUserName=pStrUserName;
			m_INIFile.m_nHideLogin=1; //on next startup login is skiped!
			m_INIFile.Save();
			if (m_bIsThinClient)
			{
				g_pBoSet->app->UserLogon->Logout(err);
				pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);		//quit application
			}
			else
			{
				g_pBoSet->app->UserLogon->Logout(err);
				pStatus.setError(StatusCodeSet::ERR_SYSTEM_SILENT_LOGIN_AGAIN);
			}
			ClearGlobalObjects(err,true);
			return;
		}
	}


	int nPeriod=CheckForDemoPeriod(pStatus,byteDemoKey);
	if(!pStatus.IsOK()) 
	{
		Status err;
		g_pBoSet->app->UserLogon->Logout(err);
		//Q_ASSERT(err.IsOK());//if error
		ClearGlobalObjects(err,true);
		return;
	}


	//--------------------------------------------------------
	// compare ini language settings with user language settings - on clash, ask user to resolve
	//--------------------------------------------------------
	QString strULangCode;
	int nUL = g_pSettings->GetPersonSetting(DEFAULT_LANGUAGE).toInt();
	if(nUL == 1)
		strULangCode = "de";
	else
		strULangCode = "en";

	QString strDLCode = g_pClientManager->GetIniFile()->m_strLangCode;
	if(strDLCode != strULangCode)
	{
		//temporarily load UL
		QTranslator translator;
		if(strULangCode != "en"){
			bool bOK = translator.load("sokrates_"+strULangCode, QCoreApplication::applicationDirPath());
			Q_ASSERT(bOK);
			QCoreApplication::installTranslator(&translator);
		}
		else{
			QCoreApplication::removeTranslator(g_translator);
		}

		LanguageWarningDlg dlgLang(strULangCode, g_pClientManager->GetIniFile()->m_strLangCode);
		if (dlgLang.exec())
		{
			Status err;
			//g_pBoSet->app->UserLogon->Logout(err);

			pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);		//quit application
			ClearGlobalObjects(err,true);

			g_pClientManager->GetIniFile()->m_strLangCode = strULangCode;
			g_pClientManager->GetIniFile()->Save();

			//restart application
			QStringList lstArgs;
			QString strWorkDir;
			QProcess::startDetached(QCoreApplication::applicationFilePath(), lstArgs, strWorkDir);
			return;
		}

		//load back the original language
		if(strULangCode != "en")
			QCoreApplication::removeTranslator(&translator);
		if(strDLCode != "en"){
			QCoreApplication::installTranslator(g_translator);
		}
	}

	//-------------------------------------------------
	// CHECK FOR ORDER:
	//-------------------------------------------------
	if (nPeriod>=0 && nPeriod<=10) //only last 10 days check
	{
		Dlg_TrialPopUp Dlg;
		Dlg.Initialize(nPeriod,URL_ORDER_FORM,m_bIsThinClient);
		Dlg.exec();
		if (Dlg.GetState()!=0)
		{
			//QString strMsg=tr("Application will be restarted to complete registration procedure\n\n");
			//TimeMessageBox box(4,true,QMessageBox::Question,tr("Restart"),strMsg,QMessageBox::Ok);
			//box.exec();	//returns buttons->but if timeout then it will be undefined...
			if (!Dlg.GetLicensePath().isEmpty())
				m_INIFile.m_strKeyFilePath==Dlg.GetLicensePath();
			m_INIFile.m_strLastUserName=pStrUserName;
			m_INIFile.Save();
			Status err;
			g_pBoSet->app->UserLogon->Logout(err);
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
			ClearGlobalObjects(err,true);
			return;
		}
	}

	//BT: 2009-10-13: coz GUI can not init after all FUI's are started: hit it as last
	//if (m_bIsThinClient)
	//	CheckServerMessagesAfterLogin();

	ClearLastFatalError();

	//start thick timers:
	if (!m_bIsThinClient)
	{
		ClientBackupManager* pBackupManager=dynamic_cast<ClientBackupManager*>(g_pBackupManager);
		if (pBackupManager)pBackupManager->ThickModeRestartTimer();	
		ClientImportExportManager* pManager=dynamic_cast<ClientImportExportManager*>(g_pImportExportManager);
		if (pManager)
		{
			Status err;
			pManager->ReloadSettings(err);
			_CHK_ERR(err);
		}
	}

}
void ClientManagerExt::BusinessLogout(Status &pStatus)
{
	pStatus.setError(0);
	ClearLastFatalError();
	if (!m_bIsThinClient) //stop thick timers:
	{
		ClientBackupManager* pBackupManager=dynamic_cast<ClientBackupManager*>(g_pBackupManager);
		if (pBackupManager)pBackupManager->StopThickModeTimer();	
		ClientImportExportManager* pManager=dynamic_cast<ClientImportExportManager*>(g_pImportExportManager);
		if (pManager)pManager->StopThickModeTimer();
	}
	m_nStartUpWizardEnabled=0;
	if(!g_pBoSet->IsServiceStarted()) return;	//if service not started just exit
	Q_ASSERT(g_pBoSet->app!=NULL); //must be set

	emit ClientBeforeLogout(); //global objects are alive
	if(!m_LastFatalError.IsOK())
	{	pStatus=m_LastFatalError; return; }

	ClearGlobalObjects(pStatus);
	if(!pStatus.IsOK()) return;

	g_pBoSet->app->UserLogon->Logout(pStatus);
	if(!pStatus.IsOK()) return;

	m_bIsLogged=false;

	ClearLastFatalError();
}



void ClientManagerExt::AskForUpdate(QString strServerActualVersion)
{
	QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor)); //issue 1272

	QString strMsg;

	QString strUpdateFile=m_VersionParser.GetAbsoluteUpdateFileURL(); //can be http:// or file://
	if (!strUpdateFile.isEmpty())
	{
		strMsg=tr("Your version %1 is not actual any more. Do you want to download and install an update to the version used by the server?");
		strMsg=strMsg.arg(QString(APPLICATION_VERSION));

		int nResult=QMessageBox::question(NULL,tr("Warning"),strMsg,tr("Yes"),tr("No"));
		if (nResult==0)
		{
			QDesktopServices::openUrl(strUpdateFile);
		}

		QApplication::restoreOverrideCursor(); //issue 1272
		return;
	}

	double fVersionLatest;
	QString strUrl;
	QString strVersionID;
	m_VersionParser.GetAllVersionData(strVersionID,strUrl,fVersionLatest);

	double fCurrentClient=m_VersionParser.Version2Double(QString(APPLICATION_VERSION));
	double fCurrentServer;
	if (strServerActualVersion.isEmpty())
		fCurrentServer=fVersionLatest;
	else
		fCurrentServer=m_VersionParser.Version2Double(strServerActualVersion);


	//ALG: if client < server && server != latest -> upgrade to server ver
	//ALG: if client < server && server = latest -> normal
	//ALG: if client > server -> ask for downgrade to server ver
	
	bool bSetToServerVersion=false;
	if (fCurrentServer==fVersionLatest)
	{
		strMsg=tr("Your version %1 is not actual any more. Do you want to download and install an update to version %2 now?");
		strMsg=strMsg.arg(QString(APPLICATION_VERSION));
		strMsg=strMsg.arg(strVersionID);
	}
	else //fCurrentServer<=fVersionLatest
	{
		if (fCurrentClient>fCurrentServer)
		{
			strMsg=tr("Your version %1 is newer than server version. Do you want to download and install a downgrade to the server version now?");
			strMsg=strMsg.arg(QString(APPLICATION_VERSION));
			//strMsg=strMsg.arg(strVersionID);
		}
		else
		{
			strMsg=tr("Your version %1 is not actual any more. Do you want to download and install an update to the version used by the server?");
			strMsg=strMsg.arg(QString(APPLICATION_VERSION));
			//strMsg=strMsg.arg(strVersionID);
		}
		bSetToServerVersion=true;
	}


	int nResult=QMessageBox::question(NULL,tr("Warning"),strMsg,tr("Yes"),tr("No"));
	if (nResult==0)
	{
		//->link
		if (bSetToServerVersion)
			QDesktopServices::openUrl(m_VersionParser.GetSetupExePathBasedOnVersion(strServerActualVersion));
		else
			QDesktopServices::openUrl(m_VersionParser.GetSetupExePath());
	}

	QApplication::restoreOverrideCursor(); //issue 1272
}



//0 -expired, -1 no demo, else demo days left
int ClientManagerExt::CheckForDemoPeriod(Status &pStatus,QByteArray key)
{

	if (!g_FunctionPoint.IsFPAvailable(FP_SHOW_ORDER_BUTTON))
		return -1;

	int nDemoPeriod;
	if (!g_FunctionPoint.IsFPAvailable(FP_EVALUATION_PERIOD,nDemoPeriod))
		return -1;


	//read key;
	QDateTime lastModif,datStart;
	int nSize,nLocked;
	int nDbVersion;
	QString strAppVersion;
	if(!Authenticator::Demo_ParseKey(key,strAppVersion,nDbVersion,lastModif,nSize,nLocked,datStart))
	{
		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart);	//lock
		g_pBoSet->app->CoreServices->UpdateVersion(pStatus,key);
		//if (!pStatus.IsOK())
		return 0; //expired
	}

	if (nLocked!=0)
		return 0;

	if(nDbVersion!=DATABASE_VERSION)
	{
		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart); //lock
		g_pBoSet->app->CoreServices->UpdateVersion(pStatus,key);
		//if (!pStatus.IsOK())
		return 0;
	}


	//ONLY for THICK client check size & date of sokrates.exe
	if (!m_bIsThinClient)
	{
		//if application newer, update:
		bool bUpdate=false;
		if (strAppVersion!=QString(APPLICATION_VERSION))
		{
			//calc chk sum & last modif:
			QFileInfo sokrates(QCoreApplication::applicationDirPath()+"/sokrates.exe");
			if (sokrates.exists())
			{
				lastModif=sokrates.lastModified();
				nSize=sokrates.size();
			}
			Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,0,datStart); 
			g_pBoSet->app->CoreServices->UpdateVersion(pStatus,key);
			if (!pStatus.IsOK())
				return -1;
		}
		else //check sums
		{
			//calc chk sum & last modif:
			QFileInfo sokrates(QCoreApplication::applicationDirPath()+"/sokrates.exe");
			if (sokrates.exists())
			{
				QDateTime curr_lastModif=sokrates.lastModified();
				int cur_nSize=sokrates.size();
				qDebug()<<lastModif;
				qDebug()<<curr_lastModif;

				//to test last modified?
				//if (lastModif!=curr_lastModif || cur_nSize!=nSize)
				if (cur_nSize!=nSize)
				{
					Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart); //lock
					g_pBoSet->app->CoreServices->UpdateVersion(pStatus,key);
					//if (!pStatus.IsOK())
					return 0;
				}
			}
		}
	}


	//if empty
	if (datStart.isNull())
	{

		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,0,QDateTime::currentDateTime()); 
		g_pBoSet->app->CoreServices->UpdateVersion(pStatus,key);
		if (!pStatus.IsOK())
			return 0;
		else
			return nDemoPeriod;
	}


	//finally check date:
	int nPeriod=datStart.daysTo(QDateTime::currentDateTime());
	if (nPeriod<0)
	{		
		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart); //lock
		g_pBoSet->app->CoreServices->UpdateVersion(pStatus,key);
		//if (!pStatus.IsOK())
		return 0;
	}

	//lock
	if (nDemoPeriod<nPeriod)
	{
		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart); //lock
		g_pBoSet->app->CoreServices->UpdateVersion(pStatus,key);
		//if (!pStatus.IsOK())
		return 0;
	}

	int nLeft=nDemoPeriod-nPeriod;
	if(nLeft==0)nLeft=1; //last day, show hes got 1 day
	return nLeft;
}

//0-no updates available, 1- user abort, 2- new update exists
int ClientManagerExt::CheckForUpdatesThickClient()
{

	double fInternet;
	QString strUrl;
	QString strVersionID;
	m_VersionParser.GetAllVersionData(strVersionID,strUrl,fInternet);

	fInternet=m_VersionParser.GetVersionID_Double();
	if (fInternet==-1)
		return 0;

	strUrl=m_VersionParser.GetSetupExePath();
	if (strUrl.isEmpty())
		return 0;

	strVersionID=m_VersionParser.GetVersionID_String();


	double fCurrent=m_VersionParser.Version2Double(QString(APPLICATION_VERSION));
	if(fCurrent<fInternet)
	{
		//TEMPORARY: ask version:
		QString strMsg=tr("Your version %1 is not actual any more. Do you want to download and install an update to version %2 now?");
		strMsg=strMsg.arg(QString(APPLICATION_VERSION));
		strMsg=strMsg.arg(strVersionID);
		int nResult=QMessageBox::question(NULL,tr("Warning"),strMsg,tr("Yes"),tr("No"));
		if (nResult==0)
		{
			//->link
			QDesktopServices::openUrl(strUrl);
			return 2;
		}
		return 1;
	}

	return 0;
}



bool ClientManagerExt::CheckSubscriptionPeriod()
{
	return true;
	QDate dateEnd=QDate(2007,8,8);
	QDate dateWarning=QDate(2007,8,4);

	if (!dateEnd.isValid())
		return true;

	//check registry:
	QString strCode=m_strModuleCode;
	QSettings settingsTE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Team Edition\\Settings",	QSettings::NativeFormat);
	QSettings settingsPE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Personal Edition\\Settings",	QSettings::NativeFormat);
	QSettings settingsBE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Business Edition\\Settings",	QSettings::NativeFormat);

	QString strDateExpiredRegistry;
	if (strCode=="SC-TE")
	{
		strDateExpiredRegistry=settingsTE.value("Subscription End Date",QString("")).toString();
	}
	else if (strCode=="SC-PE")
	{
		strDateExpiredRegistry=settingsPE.value("Subscription End Date",QString("")).toString();
	}
	else if (strCode=="SC-BE")
	{
		strDateExpiredRegistry=settingsBE.value("Subscription End Date",QString("")).toString();
	}

	//if wrong then write:
	QString strDateEnd=dateEnd.toString("yyyyMMdd");
	if (strDateExpiredRegistry.isEmpty() || strDateExpiredRegistry!=strDateEnd)
	{
		if (strCode=="SC-TE")
		{
			settingsTE.setValue("Subscription End Date",strDateEnd);
		}
		else if (strCode=="SC-PE")
		{
			settingsPE.setValue("Subscription End Date",strDateEnd);
		}
		else if (strCode=="SC-BE")
		{
			settingsBE.setValue("Subscription End Date",strDateEnd);
		}
	}



	QDate dateBuild=QDate::fromString(APPLICATION_BUILD_DATE,"dd.MM.yyyy");
	//qDebug()<<dateBuild;


	if (QDate::currentDate()<dateWarning)
		return true;

	if (QDate::currentDate()<=dateEnd && QDate::currentDate()>dateWarning)
	{
		int nDaysLeft=dateWarning.daysTo(dateEnd);
		QString strMesg=tr("You have ")+QVariant(nDaysLeft).toString()+ tr(" days remaining before the update subscription expires. Please renew it to be able to download updates in the future!");
		QMessageBox::warning(NULL,tr("Warning"),strMesg);
		return true;
	}

	//check version:
	if (QDate::currentDate()>dateEnd)
	{
		if (dateBuild>dateEnd)
		{
			int nResult=QMessageBox::question(NULL,tr("Subscription period expired!"),tr("You can not use the new version because the update subscription period has expired. Order/renew your license to use this application!"),tr("  Renew update subscription "),tr("  Downgrade the un-licensed updated version to the previous, licensed one   "),tr(" Cancel "),0,2);
			if (nResult==0)
			{
				//QString strUrl=URL_ORDER_FORM
				QDesktopServices::openUrl(QString(URL_ORDER_FORM));
				return false;
			}
			else if (nResult==1)
			{
				//load version.xml, parse old valid version, download it
				QString strPath=m_VersionParser.GetSetupExePathBasedOnDate(dateEnd);
				if (strPath.isEmpty())
				{
					QMessageBox::critical(NULL,tr("Error"),tr("Could not fetch old version. Please try manually!"));
					return false;
				}
				else
				{
					QDesktopServices::openUrl(strPath);
					return false;
				}
			}
			else
				return false; //exit
		}
	}

	return true;
}





void ClientManagerExt::OnServerMessageRecieved(DbRecordSet rowMsg)
{
	//open qmessage
	Status err;

	if (rowMsg.getRowCount()==0)
		return;

	if (rowMsg.getDataRef(0,"MSG_TYPE").toInt()==StatusCodeSet::TYPE_SILENT_SERVER_NOTIFICATION)
	{
		emit ServerMessageReceived(rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());
		return;
	}

	if (rowMsg.getDataRef(0,"MSG_CODE").toInt()==StatusCodeSet::ERR_SYSTEM_SERVER_UPDATE_AT) //convert update time to local zone
	{
		QDateTime dateTime=QDateTime::fromString(rowMsg.getDataRef(0,"MSG_TEXT").toString(),"dd.MM.yyyy hh:mm");
		dateTime=dateTime.addSecs(-m_nClientTimeZoneOffsetMinutes);
		QString strCode=dateTime.toString("dd.MM.yyyy hh:mm");
		err.setError(rowMsg.getDataRef(0,"MSG_CODE").toInt(),strCode);
	}
	else
		err.setError(rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());

	switch(rowMsg.getDataRef(0,"MSG_TYPE").toInt())
	{
	case StatusCodeSet::TYPE_WARNING:
		{
			QMessageBox::warning(NULL,tr("New message"),err.getErrorText());
			g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());
		}
		break;
	case StatusCodeSet::TYPE_ERROR:
		{
			QMessageBox::critical(NULL,tr("New message"),err.getErrorText());
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());
		}
		break;
	default:
		QMessageBox::information(NULL,tr("New message"),err.getErrorText());
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());
		break;
	}

	emit CommunicationServerMessageRecieved(rowMsg);

}

void ClientManagerExt::CheckServerMessagesAfterLogin()
{
	if (m_recSrvMsg.getRowCount()==0)
		return;


	int nSize=m_recSrvMsg.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//dispatch messages:
		OnServerMessageRecieved(m_recSrvMsg.getRow(i));
	}

}



void ClientManagerExt::HideCommunicationInProgress(bool bHide)
{
	BusinessServiceManager_ThinClient *pThin=dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet);
	if (bHide)
	{
		disconnect(pThin,SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SIGNAL(CommunicationInProgress(int,qint64,qint64)));
		disconnect(pThin,SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	} 
	else
	{
		connect(pThin,SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SIGNAL(CommunicationInProgress(int,qint64,qint64)));
		disconnect(pThin,SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	}

}

//fatal error occurred: it is dangerous to process this event while thread is working, 
//schedule event when event loop is returned, in mean time, block socket operations to prevent strange states
void ClientManagerExt::OnCommunicationFatalError(int val)
{
	m_LastFatalError.setError(val);
	m_nGUICloseRetries=0;
	CloseAllModalWindows();
}



//when event loop is non working: use this to notify others
void ClientManagerExt::OnCommunicationFatalErrorDelayNotification()
{
	emit CommunicationFatalError(m_LastFatalError.getErrorCode());
}


//close all top windows
void ClientManagerExt::CloseAllModalWindows()
{
	qDebug()<<"Debug ClientManagerExt::CloseAllModalWindows()";
	QWidget *pWidget=QApplication::activeModalWidget();
	while (pWidget)
	{
		qDebug()<<"Close widget "<<pWidget;
		pWidget->deleteLater();		//queue for deletion
		pWidget->close();
		//QCoreApplication::instance()->processEvents(); //QEventLoop::DeferredDeletion);
		pWidget=QApplication::activeModalWidget();
	}


	if (m_nGUICloseRetries==GUI_CLOSE_WINDOW_RETIRES_AFTER_BROKEN_CONN)
	{
		qDebug()<<"ClientManagerExt::CloseAllModalWindows() OnCommunicationFatalErrorDelayNotification";
		QTimer::singleShot(0,this,SLOT(OnCommunicationFatalErrorDelayNotification()));
	}
	else
	{
		QTimer::singleShot(0,this,SLOT(CloseAllModalWindows()));
	}

	m_nGUICloseRetries++;
}





//load all data needed for user session
void ClientManagerExt::InitializeGlobalObjects(Status &pStatus)
{
	//Options and settings.
	DbRecordSet recSettings, recOptions,rowARSet;
	//Get user session data.
	DbRecordSet lstMessages;
	int nIsAliveInterval;
	_SERVER_CALL(CoreServices->ReadUserSessionData(pStatus,m_recUserData,rowARSet,m_recContactData,recSettings,recOptions,m_recSrvMsg))
	if(!pStatus.IsOK())return;

	g_pSettings->SetSettingsDataSource(&recSettings,GetPersonID());
	g_pSettings->SetOptionsDataSource(&recOptions);

	//	//init AR:
	int nUserID=GetUserID();
	if(g_AccessRight)
	{
		g_AccessRight->SetUserARRecordSet(nUserID,rowARSet);
	}
	//init FP:
	g_FunctionPoint.Initialize(pStatus,m_strModuleCode,m_strMLIID);
	if(!pStatus.IsOK())return;
	
	QString strMessage;
	//check fp:
	int nValue;
	if (!m_bIsThinClient)
	{
		if(!g_FunctionPoint.IsFPAvailable(FP_THICK_CLIENT_ACCESS,nValue))
		{
			pStatus.setError(1,tr("Access not allowed from thick client!"));
			return;
		}
		strMessage=tr("Client successfully logged to: ")+m_INIFile.m_strConnectionName;
	}
	else
	{
		if(!g_FunctionPoint.IsFPAvailable(FP_THIN_CLIENT_ACCESS,nValue))
		{
			pStatus.setError(1,tr("Access not allowed from thin client!"));
			return;
		}
		strMessage=tr("Client successfully logged to: ")+GetConnectionIPAddress();
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);

	g_DownloadManager->Initialize(2);
}

void ClientManagerExt::ClearGlobalObjects(Status &pStatus, bool bFatalErrorDetected, bool bPreserveCacheForOfflineMode)
{
	if (!bFatalErrorDetected)
		g_pSettings->SavePersonSettings(pStatus);
	if(g_AccessRight)
	{
		g_AccessRight->DeleteUserARRecordSet(GetUserID());
	}
	g_FunctionPoint.Invalidate();
	
	if(!bPreserveCacheForOfflineMode)
		g_ClientCache.PurgeCache();

	SelectionPopup::ClearCache();

	g_DownloadManager->AbortAllOperations();
}






int ClientManagerExt::GetUserID()
{
	if (m_recUserData.getRowCount()>0)
		return m_recUserData.getDataRef(0,"CUSR_ID").toInt();
	else
		return -1;
}

int ClientManagerExt::GetPersonID()
{
	return m_recUserData.getDataRef(0,"BPER_ID").toInt();

}

QString ClientManagerExt::GetPersonName()
{
	return m_recUserData.getDataRef(0,"BPER_LAST_NAME").toString()+", "+m_recUserData.getDataRef(0,"BPER_FIRST_NAME").toString();
}

QString ClientManagerExt::GetPersonCode()
{
	return m_recUserData.getDataRef(0,"BPER_CODE").toString();
}
bool ClientManagerExt::IsSystemUser()
{
	return (bool)m_recUserData.getDataRef(0,"CUSR_IS_SYSTEM").toInt();
}

QString ClientManagerExt::GetPersonInitials()
{
	QString strInitials = m_recUserData.getDataRef(0,"BPER_INITIALS").toString();

	if (strInitials.isEmpty())
		strInitials = m_recUserData.getDataRef(0,"BPER_LAST_NAME").toString() + m_recUserData.getDataRef(0,"BPER_FIRST_NAME").toString();

	return strInitials;
}

void ClientManagerExt::SetConnectionName(QString strName)
{
	m_strConnectionName=strName;
	m_INIFile.m_strConnectionName=strName; 
}

void ClientManagerExt::SetLoggedUserName(QString strName)
{
	m_strLoggedUserName=strName;
	m_INIFile.m_strLastUserName=strName; 
}

QString ClientManagerExt::GetConnectionName()
{
	return m_strConnectionName;
}

void ClientManagerExt::SetConnectionIPAddress(QString strIP)
{
	m_strConnIPAddress=strIP;
}

QString ClientManagerExt::GetConnectionIPAddress()
{
	return m_strConnIPAddress;
}

void ClientManagerExt::ReloadAccessRights()
{
	int nUserID=GetUserID();
	if(g_AccessRight)
	{
		Status pStatus;
		DbRecordSet rowARSet;
		_SERVER_CALL(AccessRights->GetUserAccRightsRecordSet(pStatus, rowARSet, nUserID));
			if(!pStatus.IsOK())return;

		g_AccessRight->SetUserARRecordSet(nUserID,rowARSet);
	}

}

//returns zero if invalid
int ClientManagerExt::GetPersonContactID()
{
	if (m_recUserData.getRowCount()==0)
		return 0;
	return m_recUserData.getDataRef(0,"BPER_CONTACT_ID").toInt();
}
int	ClientManagerExt::GetPersonProjectListID()
{
	//priority is c), a), b)
	int nListID = g_pSettings->GetPersonSetting(PROJECT_SETTINGS_DEF_STORED_LIST).toInt();
	if(nListID > 0)
		return nListID;
	if (m_recUserData.getRowCount()>0)
		nListID = m_recUserData.getDataRef(0,"BPER_DEF_PROJECT_LIST_ID").toInt();
	if(nListID > 0)
		return nListID;
	nListID = g_pSettings->GetApplicationOption(PROJECT_OPTIONS_DEF_STORED_LIST).toInt();
	if(nListID > 0)
		return nListID;

	return 0; //not set
}


void*	ClientManagerExt::GetBackupManager()
{
	return g_pBackupManager;
}
void*	ClientManagerExt::GetImportExportManager()
{
	return g_pImportExportManager;
}

//true to exit application
bool ClientManagerExt::CheckForOrderForm()
{
	//order form (must be in this place, after translator, only for THIN):
	if (m_INIFile.m_nOrderAsk!=0 && m_bIsThinClient)
	{
		Dlg_OrderForm Dlg;
		Dlg.Initialize(URL_ORDER_FORM);
		if (Dlg.exec())		//if order made, reset flag->it will not ask again
		{
			return true;
		}
	}
	return false;
}


void ClientManagerExt::StopAfterFatalError(Status &pStatus, bool bPreserveCacheForOfflineMode)
{
	pStatus.setError(0);
	ClearGlobalObjects(pStatus, true, bPreserveCacheForOfflineMode);
	if (g_pBoSet)
	{
		g_pBoSet->StopService(pStatus,true); //pointers will be cleared on next start...
		ProcessAfterStop_BusinessService(true);
	}
	m_bIsLogged=false;
}

bool ClientManagerExt::IsClientIPMatchHost()
{
	if (!m_bIsThinClient)
		return true;
	else
	{
		if(TcpHelper::IsHostAddress(GetConnectionIPAddress()))
			return true;
		else
			return false;
	}
}



void ClientManagerExt::ProcessAfterStart_BusinessService()
{
	Status err;
	if (g_pImportExportManager)
		g_pImportExportManager->SetBusinessServiceSet(err,g_pBoSet->app);

}

void ClientManagerExt::ProcessAfterStop_BusinessService(bool bFatalErrorDetected)
{
	Status err;
	if (!m_bIsThinClient)
	{
		ClientBackupManager* pBackupManager=dynamic_cast<ClientBackupManager*>(g_pBackupManager);
		if (pBackupManager)pBackupManager->StopThickModeTimer();	
		ClientImportExportManager* pManager=dynamic_cast<ClientImportExportManager*>(g_pImportExportManager);
		if (pManager)pManager->StopThickModeTimer();
	}
	if (!m_bIsThinClient && g_pBackupManager) //save bcp manager settings:
	{
		g_pBackupManager->GetBackupData(err,m_INIFile.m_nBackupFreq,m_INIFile.m_strBackupPath,m_INIFile.m_datBackupLastDate,m_INIFile.m_strRestoreOnNextStart,m_INIFile.m_nBackupDay,m_INIFile.m_strBackupTime,m_INIFile.m_nDeleteOldBackupsDay);
		if (err.IsOK())
			m_INIFile.Save();
	}
	if (g_pImportExportManager)
		g_pImportExportManager->SetBusinessServiceSet(err,NULL);

}



//show progrsss dialog:
void ClientManagerExt::OnCommunicationInProgress(int nTicks,qint64 done, qint64 total)
{
	if (!m_bShowProgressDlg)
		 return;


	if (total>0 && done < total)
	{
		if (m_prgProgressDialogTransfer->maximum()==0)
		{
			m_prgProgressDialogTransfer->setRange(0,100);
		}

		m_prgProgressDialogTransfer->setValue(((double)done/total)*100);
	}
	else if (nTicks*4>2) //after 2sec open progress
	{
		if (m_prgProgressDialogTransfer->maximum()!=0)
		{
			m_prgProgressDialogTransfer->setRange(0,0); //floating progress
		}

		m_prgProgressDialogTransfer->setValue(1);
	}
	//	m_prgDlgDocumentTransfer->hide();
	//qApp->processEvents();

	if (m_prgProgressDialogTransfer->wasCanceled())
	{
		g_pBoSet->AbortDataTransfer();
		m_prgProgressDialogTransfer->reset();
		m_prgProgressDialogTransfer->hide();
	}
}

//show progrsss dialog:
void ClientManagerExt::OnCommunicationInProgress_Ext(int done, int total)
{
	if (!m_bShowProgressDlg)
		return;


	if (total>0 && done < total)
	{
		if (m_prgProgressDialogTransfer->maximum()==0)
		{
			m_prgProgressDialogTransfer->setRange(0,100);
		}

		m_prgProgressDialogTransfer->setValue(((double)done/total)*100);
	}
	else 
	{
		if (m_prgProgressDialogTransfer->maximum()!=0)
		{
			m_prgProgressDialogTransfer->setRange(0,0); //floating progress
		}

		m_prgProgressDialogTransfer->setValue(1);
	}

	if (m_prgProgressDialogTransfer->wasCanceled())
	{
		m_prgProgressDialogTransfer->reset();
		m_prgProgressDialogTransfer->hide();
	}

}


void ClientManagerExt::ShowTransferProgressDlg(QString strLabel)
{
	m_bShowProgressDlg=true;
	m_prgProgressDialogTransfer->setLabelText(strLabel);
	m_prgProgressDialogTransfer->setRange(0,100);
}
void ClientManagerExt::HideTransferProgressDlg()
{
	m_bShowProgressDlg=false;
	m_prgProgressDialogTransfer->reset();
	m_prgProgressDialogTransfer->hide();
}

void ClientManagerExt::StartThreadConnection(Status& pStatus, QObject *pSignalReceiver)
{
	if (m_bIsThinClient)
		dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet)->StartThreadConnection(pStatus,pSignalReceiver);
}
void ClientManagerExt::CloseThreadConnection(Status& pStatus)
{
	if (m_bIsThinClient)
		dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet)->CloseThreadConnection(pStatus);
}


void ClientManagerExt::StopProgressDialog()
{
	//m_procSplashApp.terminate();
	if (m_procSplashApp.state()!=QProcess::NotRunning)
		m_procSplashApp.kill();
}
void ClientManagerExt::StartProgressDialog(QString strMsg)
{
	m_procSplashApp.terminate();	

	//start splash
	QString strSplashApp(QCoreApplication::applicationDirPath() + "/Sokrates_Progress.exe");
	if(QFile::exists(strSplashApp))
	{
		strSplashApp.prepend("\"");
		strSplashApp.append("\"");
		QStringList args;
		args << strMsg; //tr("Login in Progress...");
		m_procSplashApp.start(strSplashApp, args);

	}
}
