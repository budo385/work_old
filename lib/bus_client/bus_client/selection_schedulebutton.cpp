#include "bus_client/bus_client/selection_schedulebutton.h"
#include <QContextMenuEvent>
#include "gui_core/gui_core/gui_helper.h"
#include "bus_core/bus_core/globalconstants.h"

#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester



Selection_ScheduleButton::Selection_ScheduleButton(QWidget *parent)
: QPushButton(parent),m_pPieMenu(NULL),m_pPieMenuDoc(NULL)
{

	GUI_Helper::SetButtonTextIcon(this, tr("Schedule Follow-Up"), ":PieMenu16.png");

	//setIcon(QIcon(":piemenu.png"));
	//setToolTip(tr("Communicate"));


	//build menu:
	m_pPieMenuDoc = new QtPieMenu(QIcon(":Document32.png"),tr("Document"), NULL);

	m_pPieMenuDoc->insertItem(QIcon(":Local_File_32.png"),tr("Local File "),this,SLOT(OnLocalFile()));
	m_pPieMenuDoc->insertItem(QIcon(":Internet_File_32.png"),tr("Internet File"),this,SLOT(OnInternetFile()));
	m_pPieMenuDoc->insertItem(QIcon(":Note_32.png"),tr("Note"),this,SLOT(OnNote()));
	m_pPieMenuDoc->insertItem(QIcon(":Address_32.png"),tr("Address"),this,SLOT(OnAddress()));
	m_pPieMenuDoc->insertItem(QIcon(":Paper_Docs_32.png"),tr("Paper Document"),this,SLOT(OnPaperDoc()));
	m_pPieMenuDoc->setOuterRadius(125);

	//build menu:
	m_pPieMenu = new QtPieMenu("Root menu", this, "Root menu");


	m_pPieMenu->insertItem(QIcon(":Email32.png"),tr("Email"),this,SLOT(OnSendMail()));
	m_pPieMenu->insertItem(QIcon(":Phone32.png"),tr("Phone Call"),this,SLOT(OnPhoneCall()));
	//m_pPieMenu->insertItem(QIcon(":Document32.png"),tr("Document"),this,SLOT(OnDocument()));
	m_pPieMenu->insertItem(m_pPieMenuDoc);

	m_pPieMenu->setOuterRadius(100);
	connect(this,SIGNAL(clicked()),this,SLOT(OnClick()));


	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_LOCAL, nValue))
	{
		m_pPieMenuDoc->setItemEnabled(false,0);
	}
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_INTERNET, nValue))
	{
		m_pPieMenuDoc->setItemEnabled(false,1);
	}
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_NOTE, nValue))
	{
		m_pPieMenuDoc->setItemEnabled(false,2);
	}
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_ADDRESS, nValue))
	{
		m_pPieMenuDoc->setItemEnabled(false,3);
	}
	if(!g_FunctionPoint.IsFPAvailable(FP_DM_PAPER, nValue))
	{
		m_pPieMenuDoc->setItemEnabled(false,4);
	}

}

Selection_ScheduleButton::~Selection_ScheduleButton()
{
	//delete m_pPieMenu;
	//delete m_pPieMenuDoc;

}

void Selection_ScheduleButton::contextMenuEvent(QContextMenuEvent *event)
{
	m_pPieMenu->popup(event->globalPos());
}


void Selection_ScheduleButton::OnClick()
{
	//QPushButton::click();
	m_pPieMenu->popup(mapToGlobal(QPoint(15,15)));
}




void Selection_ScheduleButton::OnSendMail()
{
	emit OnScheduleEmail();
}

void Selection_ScheduleButton::OnPhoneCall()
{
	emit OnScheduleCall();
}






void Selection_ScheduleButton::OnLocalFile()
{
	emit OnScheduleDoc(GlobalConstants::DOC_TYPE_LOCAL_FILE_REFERENCE);
}
void Selection_ScheduleButton::OnInternetFile()
{
	emit OnScheduleDoc(GlobalConstants::DOC_TYPE_INTERNET_FILE);
}

void Selection_ScheduleButton::OnNote()
{
	emit OnScheduleDoc(GlobalConstants::DOC_TYPE_NOTE);
}
void Selection_ScheduleButton::OnAddress()
{
	emit OnScheduleDoc(GlobalConstants::DOC_TYPE_URL);
}
void Selection_ScheduleButton::OnPaperDoc()
{
	emit OnScheduleDoc(GlobalConstants::DOC_TYPE_PHYSICAL);
}


