#ifndef DLG_TRIALPOPUP_H
#define DLG_TRIALPOPUP_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_trialpopup.h"


/*

After Login, test FP_

*/

class Dlg_TrialPopUp : public QDialog
{
	Q_OBJECT

public:
	Dlg_TrialPopUp(QWidget *parent = 0);
	~Dlg_TrialPopUp();

	void Initialize(int nPeriodLeft, QString strOrderURL,bool bIsThinClientMode);
	int GetState(){return m_nState;}
	QString GetLicensePath(){return m_strLicensePath;};

private slots:
	void on_btnTrial_clicked();
	void on_btnFull_clicked();
	void on_btnRegister_clicked();
	void on_btnCancel_clicked();

private:
	Ui::Dlg_TrialPopUpClass ui;

	int m_nPeriod;
	int m_nState;		//0-proceed, 1- reload keyfile/restart, 2- shutdown
	QString m_strLicensePath;
	QString m_strOrderURL;
	bool m_bIsThinClientMode;
};

#endif // DLG_TRIALPOPUP_H
