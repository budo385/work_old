#include "clientimportexportmanager.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include <QtWidgets/QApplication>
#include "gui_core/gui_core/timemessagebox.h"
#include "common/common/threadid.h"
#include "common/common/logger.h"
extern Logger				g_Logger;

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;

ClientImportExportManager::ClientImportExportManager()
{
	m_nTimerID=-1;
	connect(this,SIGNAL(GlobalPeriodChanged(int)),this,SLOT(OnImportExportManager_GlobalPeriodChanged(int)));
	connect(this,SIGNAL(SignalStartTask(int,int)),this,SLOT(OnImportExportManager_StartTask(int,int)));
}

void ClientImportExportManager::SetClientMode(bool bIsThinClient)
{
	m_bIsThinClient=bIsThinClient;
	if (bIsThinClient)
		m_bFileWatcherEnabled=false;
}
void ClientImportExportManager::OnImportExportManager_GlobalPeriodChanged(int nPeriodMin)
{
	if (m_bIsThinClient)
		return;
	ThickModeRestartTimer();
}

void ClientImportExportManager::OnImportExportManager_StartTask(int nCallerID,int nTaskID)
{
	if (m_bIsThinClient)
		return;

	QString strLang = g_pClientManager->GetIniFile()->m_strLangCode;


	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Import/Export Manager processing started, thread:" +QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
	Status err;
	switch(nTaskID)
	{
	case PROCESS_TYPE_PROJECT:
		StartProcessProject(err);
		break;
	case PROCESS_TYPE_USER:
		StartProcessUser(err);
		break;
	case PROCESS_TYPE_CONTACT:
		StartProcessContact(err, strLang);
		break;
	case PROCESS_TYPE_DEBTOR:
		StartProcessDebtor(err);
		break;
	case PROCESS_TYPE_SPL:
		StartProcessStoredProjectList(err);
		break;
	default:
		StartAllScheduledTasks(err, strLang);
		break;
	}
	QApplication::restoreOverrideCursor();
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,err.getErrorCode(),tr("Import/Export Manager process failed: ") +err.getErrorText());
		QMessageBox::critical(NULL,tr("Error"),tr("Import/Export Manager process failed: ") +err.getErrorText());
	}
	else
	{
		QString strMsg=GetLastStatus(nTaskID);
		if (strMsg.isEmpty())
			err.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_SUCCESS_ALL);
		else
			err.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_SUCCESS,strMsg);
		QMessageBox::information(NULL,tr("Information"),err.getErrorText());
	}
	
}


void ClientImportExportManager::ExportProjects(Status &pStatus, QString strFileName,bool bUtf8,DbRecordSet *plstProjects)
{
	CheckFileForExport(pStatus,strFileName);
	if (!pStatus.IsOK())
		return; 

		DbRecordSet lstProjects;
		if (plstProjects)
			lstProjects=*plstProjects;

		//reload settings:
		DbRecordSet recSettings;
		GetSettings(pStatus,recSettings);
		if (!pStatus.IsOK())return; 
	

		QFileInfo file(strFileName);
		QString strPath=file.absolutePath();
		QString strPefix=file.baseName();
		QString strExt=file.suffix();
		QByteArray fileBuff;
		DbRecordSet Ret_ProcessedFiles,Ret_pErrors;

		_SERVER_CALL(BusImport->ProcessProjects_Export(pStatus,strExt,strPefix,recSettings.getDataRef(0,"CIE_PROJ_FILE_PREFIX_PROCESS").toString(),strPath,0,false,Ret_ProcessedFiles,Ret_pErrors,lstProjects,true,fileBuff, bUtf8))
		if (!pStatus.IsOK() && !(pStatus.getErrorCode()==StatusCodeSet::ERR_BUS_EXPORT_IMPORT_SUCESS))
			return;

		if (fileBuff.isEmpty())
		{
			pStatus.setError(1,tr("Export file is not saved as there is no data to save"));
			return;
		}
		else
		{
			QFile file(strFileName);
			if(!file.open(QIODevice::WriteOnly))
			{
				QString strTitle=tr("The File ")+strFileName+tr(" can not be written!");
				pStatus.setError(1,strTitle);
				return;
			}
			int nErr=file.write(fileBuff);
			if (nErr==-1)
			{
				QString strTitle=tr("The File ")+strFileName+tr(" can not be written!");
				pStatus.setError(1,strTitle);
				return;
			}
		}
		return;

}

void ClientImportExportManager::ExportUsers(Status &pStatus, QString strFileName,bool bUtf8,DbRecordSet *plstUsers)
{
	CheckFileForExport(pStatus,strFileName);
	if (!pStatus.IsOK())
		return; 

		DbRecordSet lstUsers;
		if (plstUsers)
			lstUsers=*plstUsers;
		DbRecordSet recSettings;
		GetSettings(pStatus,recSettings);
		if (!pStatus.IsOK())return; 

		QFileInfo file(strFileName);
		QString strPath=file.absolutePath();
		QString strPefix=file.baseName();
		QString strExt=file.suffix();
		QByteArray fileBuff;
		DbRecordSet Ret_ProcessedFiles,Ret_pErrors;

		_SERVER_CALL(BusImport->ProcessUsers_Export(pStatus,strExt,strPefix,recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString(),strPath,0,false,Ret_ProcessedFiles,Ret_pErrors,lstUsers,true,fileBuff, bUtf8))
		if (!pStatus.IsOK() && !(pStatus.getErrorCode()==StatusCodeSet::ERR_BUS_EXPORT_IMPORT_SUCESS))
			return;

		if (fileBuff.isEmpty())
		{
			pStatus.setError(1,tr("Export file is not saved as there is no data to save"));
			return;
		}
		else
		{
			QFile file(strFileName);
			if(!file.open(QIODevice::WriteOnly))
			{
				QString strTitle=tr("The File ")+strFileName+tr(" can not be written!");
				pStatus.setError(1,strTitle);
				return;
			}
			int nErr=file.write(fileBuff);
			if (nErr==-1)
			{
				QString strTitle=tr("The File ")+strFileName+tr(" can not be written!");
				pStatus.setError(1,strTitle);
				return;
			}
		}
		return;
}

void ClientImportExportManager::ExportContacts(Status &pStatus, QString strFileName, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, DbRecordSet *plstUsers)
{
	CheckFileForExport(pStatus,strFileName);
	if (!pStatus.IsOK())
		return; 


		DbRecordSet lstUsers;
		if (plstUsers)
			lstUsers=*plstUsers;
		DbRecordSet recSettings;
		GetSettings(pStatus,recSettings);
		if (!pStatus.IsOK())return; 


		QFileInfo file(strFileName);
		QString strPath=file.absolutePath();
		QString strPefix=file.baseName();
		QString strExt=file.suffix();
		QByteArray fileBuff;
		DbRecordSet Ret_ProcessedFiles,Ret_pErrors;
		
		QByteArray datHedFile;
		QString strHdrFile;
		if(strLang == "de"){
			if(bOutlookExport)
				strHdrFile = "Head_Cntct_Outlook_Ger.hed";
			else
				strHdrFile = "Head_Cntct_Comm_Ger.hed";
		}
		else{
			if(bOutlookExport)
				strHdrFile = "Head_Cntct_Outlook_Eng.hed";
			else
				strHdrFile = "Head_Cntct_Comm_Eng.hed";
		}
		QFile fileMap(QCoreApplication::applicationDirPath() + "/" + strHdrFile);
		if (fileMap.open(QIODevice::ReadOnly)){
			datHedFile = fileMap.readAll();
		}

		_SERVER_CALL(BusImport->ProcessContacts_Export(
			pStatus,
			strExt,
			strPefix,
			recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString(),
			strPath,
			0,
			false,
			Ret_ProcessedFiles,
			Ret_pErrors,
			lstUsers,
			true,
			fileBuff,
			bUtf8,
			bSokratesHdr,
			bColumnTitles,
			bOutlookExport,
			strLang,
			datHedFile))
		if (!pStatus.IsOK() && !(pStatus.getErrorCode()==StatusCodeSet::ERR_BUS_EXPORT_IMPORT_SUCESS))
			return;

		if (fileBuff.isEmpty())
		{
			pStatus.setError(1,tr("Export file is not saved as there is no data to save"));
			return;
		}
		else
		{
			QFile file(strFileName);
			if(!file.open(QIODevice::WriteOnly))
			{
				QString strTitle=tr("The File ")+strFileName+tr(" can not be written!");
				pStatus.setError(1,strTitle);
				return;
			}
			int nErr=file.write(fileBuff);
			if (nErr==-1)
			{
				QString strTitle=tr("The File ")+strFileName+tr(" can not be written!");
				pStatus.setError(1,strTitle);
				return;
			}
		}
		return;
}

void ClientImportExportManager::ExportContactContactRelationships(Status &pStatus, QString strFileName, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, DbRecordSet *plstUsers)
{
	CheckFileForExport(pStatus,strFileName);
	if (!pStatus.IsOK())
		return; 

	DbRecordSet lstUsers;
	if (plstUsers)
		lstUsers=*plstUsers;
	DbRecordSet recSettings;
	GetSettings(pStatus,recSettings);
	if (!pStatus.IsOK())return; 

	QFileInfo file(strFileName);
	QString strPath=file.absolutePath();
	QString strPefix=file.baseName();
	QString strExt=file.suffix();
	QByteArray fileBuff;
	DbRecordSet Ret_ProcessedFiles,Ret_pErrors;
	
	QByteArray datHedFile;

	_SERVER_CALL(BusImport->ProcessContactContactRelations_Export(
		pStatus,
		strExt,
		strPefix,
		recSettings.getDataRef(0,"CIE_USER_FILE_PREFIX_PROCESS").toString(),
		strPath,
		0,
		false,
		Ret_ProcessedFiles,
		Ret_pErrors,
		lstUsers,
		true,
		fileBuff,
		bUtf8,
		bSokratesHdr,
		bColumnTitles,
		bOutlookExport,
		strLang,
		datHedFile))
	if (!pStatus.IsOK() && !(pStatus.getErrorCode()==StatusCodeSet::ERR_BUS_EXPORT_IMPORT_SUCESS))
		return;

	if (fileBuff.isEmpty())
	{
		pStatus.setError(1,tr("Export file is not saved as there is no data to save"));
		return;
	}
	else
	{
		QFile file(strFileName);
		if(!file.open(QIODevice::WriteOnly))
		{
			QString strTitle=tr("The File ")+strFileName+tr(" can not be written!");
			pStatus.setError(1,strTitle);
			return;
		}
		int nErr=file.write(fileBuff);
		if (nErr==-1)
		{
			QString strTitle=tr("The File ")+strFileName+tr(" can not be written!");
			pStatus.setError(1,strTitle);
			return;
		}
	}
	return;
}

//restart timer
void ClientImportExportManager::ThickModeRestartTimer()
{
	if (m_bIsThinClient) //not used in thin
		return;

	if (m_nTimerID!=-1)
		killTimer(m_nTimerID);

	m_nTimerID=-1;

	Status err;
	int nNextInterval=GetGlobalPeriod(err);
	if (nNextInterval>0)
		m_nTimerID=startTimer(nNextInterval*60000);
}

void ClientImportExportManager::StopThickModeTimer()
{
	if (m_nTimerID!=-1)
		killTimer(m_nTimerID);

	m_nTimerID=-1;
}

//time to execute backup:
void ClientImportExportManager::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		if (m_nTimerID!=-1)
			killTimer(m_nTimerID);

		m_nTimerID=-1;
		if (m_bIsThinClient)
		{
			Q_ASSERT(false); //this could never happen in thin!
			return;
		}

		QString strMsg=tr("A scheduled import/export will start now. It will take few minutes. Proceed? (You can change schedule settings in File/Admin Tools)\n\n");
		TimeMessageBox box(60,true,QMessageBox::Question,tr("Import/export Scheduler"),strMsg,QMessageBox::Ok | QMessageBox::Cancel);
		box.exec();	
		if (box.result()==QMessageBox::Ok)
		{
			QString strLang = g_pClientManager->GetIniFile()->m_strLangCode;

			Status err;
			StartAllScheduledTasks(err, strLang);
			_CHK_ERR_NO_RET(err);
		}
		ThickModeRestartTimer();
	}
}





void ClientImportExportManager::CheckFileForExport(Status &pStatus,QString strFile)
{
	pStatus.setError(0);

	bool bFileOK=QFile::exists(strFile);
	if (!bFileOK)
	{
		QFile fileTest(strFile);
		bFileOK=fileTest.open(QIODevice::WriteOnly);
		if (bFileOK)
		{
			fileTest.close();
		}
	}

	if (!bFileOK)
	{
		pStatus.setError(1,tr("File can not be created, please specify full file path, including filename!"));
	}
}




bool ClientImportExportManager::IsUnicodeEnabled()
{
	Status err;
	DbRecordSet recSettings;
	GetSettings(err,recSettings);
	if (!err.IsOK() || recSettings.getRowCount()!=1)
	{
		Q_ASSERT(false);
		return false;
	}

	return (bool)(recSettings.getDataRef(0,"CIE_USE_UNICODE").toInt());
}


void ClientImportExportManager::GetSettings(Status &pStatus,DbRecordSet &settings)
{
	ImportExportManagerAbstract::GetSettings(pStatus,settings);
	if(!pStatus.IsOK())
		 return;

	if (settings.getRowCount()!=1)
	{
		ReloadSettings(pStatus);
		if(!pStatus.IsOK())
			return;
		ImportExportManagerAbstract::GetSettings(pStatus,settings);
	}
}