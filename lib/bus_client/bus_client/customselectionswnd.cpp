#include "customselectionswnd.h"
#include <QDebug>
#include <QDomDocument>
#include "bus_client/bus_client/clientcontactmanager.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

CustomSelectionsWnd::CustomSelectionsWnd(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	Initialize(BUS_PROJECT);
	//Initialize(BUS_CM_CONTACT); TOFIX

	//load persons:
	ui.frmPerson->Initialize(ENTITY_BUS_PERSON);
	connect(ui.frmPerson,SIGNAL(CurrentEntityRecordChanged(int)),this,SLOT(OnPersonChanged(int)));

	// init custom selection table
	m_lstSelectionData.addColumn(QVariant::Int,"m_nLogic");
	m_lstSelectionData.addColumn(QVariant::Int,"m_nField");
	m_lstSelectionData.addColumn(QVariant::Int,"m_nOperation");
	m_lstSelectionData.addColumn(QVariant::String,"m_strValue");
	m_lstSelectionData.addColumn(QVariant::String,"strDbField"); 

	//for display
	m_lstSelectionData.addColumn(QVariant::String,"strLogic"); 
	m_lstSelectionData.addColumn(QVariant::String,"strField"); 
	m_lstSelectionData.addColumn(QVariant::String,"strOperation"); 
	

	DbRecordSet columns;
	ui.tableWidgetCustSelection->AddColumnToSetup(columns,"strLogic",tr("Logic"),120,false);
	ui.tableWidgetCustSelection->AddColumnToSetup(columns,"strField",tr("Field"),200,false);
	ui.tableWidgetCustSelection->AddColumnToSetup(columns,"strOperation",tr("Operation"),80,false);
	ui.tableWidgetCustSelection->AddColumnToSetup(columns,"m_strValue",tr("Value"),170,false);
	ui.tableWidgetCustSelection->horizontalHeader()->stretchLastSection();

	ui.tableWidgetCustSelection->Initialize(&m_lstSelectionData,&columns,false,true,25);
	m_lstSelectionData.clear();
	ui.tableWidgetCustSelection->RefreshDisplay();

	connect(ui.tableWidgetCustSelection, SIGNAL(cellClicked(int,int)), this, SLOT(rowSelection(int,int)));
}

CustomSelectionsWnd::~CustomSelectionsWnd()
{
}

void CustomSelectionsWnd::rowSelection(int nRow, int nCol)
{
	int nLogic = m_lstSelectionData.getDataRef(nRow, "m_nLogic").toInt();
	if(nLogic == LOGIC_AND_IDX) ui.radioAND->setChecked(true);
	else if(nLogic== LOGIC_OR_IDX) ui.radioOR->setChecked(true);
	else if(nLogic== LOGIC_LEFTBRACKET_IDX) ui.radioLeftBracket->setChecked(true); 
	else if(nLogic== LOGIC_RIGHTBRACKET_IDX) ui.radioRightBracket->setChecked(true);
	else if(nLogic== LOGIC_NOTAND_LB_IDX) ui.radioNOTAND_lb->setChecked(true);
	else if(nLogic== LOGIC_OR_LB_IDX) ui.radioOR_lb->setChecked(true);
	else if(nLogic== LOGIC_NOT_LB_IDX)ui.radioNOT_lb->setChecked(true);

	int nFieldIdx = m_lstSelectionData.getDataRef(nRow, "m_nField").toInt();
	ui.cboCustSelectionField->setCurrentIndex(nFieldIdx);

	int nOperationIdx = m_lstSelectionData.getDataRef(nRow, "m_nOperation").toInt();
	ui.cboCustSelectionOperation->setCurrentIndex(nOperationIdx);

	QString strValue = m_lstSelectionData.getDataRef(nRow, "m_strValue").toString();
	ui.txtCustSelectionValue->setText(strValue);
}

// build fields combo
void CustomSelectionsWnd::Initialize(int nTableID)
{
	ui.cboCustSelectionField->blockSignals(true);

	ui.radioAND->setChecked(true);
	m_nTableID=nTableID;
	if (m_nTableID==BUS_CM_CONTACT)
	{
		//Build combo's:
		DbRecordSet lstFields;
		ClientContactManager::GetBuildAllContactGridSetup(lstFields);
		
		DbRecordSet lstCustomFlds;
		/*
		Status err;
		_SERVER_CALL(BusCustomFields->ReadFields(err,BUS_CM_CONTACT,lstCustomFlds));
		_CHK_ERR_NO_RET(err);
		*/
		int nSize=lstFields.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			ui.cboCustSelectionField->addItem(lstFields.getDataRef(i,0).toString(),lstFields.getDataRef(i,1));
		}
		nSize=lstCustomFlds.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			ui.cboCustSelectionField->addItem(lstCustomFlds.getDataRef(i,"BCF_NAME").toString(),lstCustomFlds.getDataRef(i,"BCF_ID"));
		}
	}
	else if (m_nTableID==BUS_PROJECT)
	{
		ui.cboCustSelectionField->addItem(tr("Project Code"),"BUSP_CODE");
		ui.cboCustSelectionField->addItem(tr("Department Code"),"BDEPT_CODE");
		ui.cboCustSelectionField->addItem(tr("Organization Code"),"BORG_CODE");
		ui.cboCustSelectionField->addItem(tr("Area Code"),"SEPB_CODE");
		ui.cboCustSelectionField->addItem(tr("Sector Code"),"SEFG_CODE");
		ui.cboCustSelectionField->addItem(tr("Industry Group Code"),"SEWG_CODE");
		ui.cboCustSelectionField->addItem(tr("Project Type"),"SEAP_CODE");
		ui.cboCustSelectionField->addItem(tr("Service Type"),"SELA_CODE");
		ui.cboCustSelectionField->addItem(tr("Project Leader"),"BPER_CODE_PROJ_LEADER"); //BUSP_LEADER_ID
		ui.cboCustSelectionField->addItem(tr("Responsible"),"BPER_CODE_RESPONSIBLE");	 //BUSP_RESPONSIBLE_ID
		ui.cboCustSelectionField->addItem(tr("Status"),"SFPS_CODE"); 
		ui.cboCustSelectionField->addItem(tr("Rate Category"),"SETK_CODE"); 
	}

	ui.cboCustSelectionField->blockSignals(false);
	on_cboCustSelectionField_currentIndexChanged(ui.cboCustSelectionField->currentIndex());
}

// inserts new line in the selection table
void CustomSelectionsWnd::on_btnCustSelectionInsertLine_clicked()
{
	// calculate radio logic 
	int nLogic=0;
	if(ui.radioAND->isChecked()) nLogic = LOGIC_AND_IDX;
	else if(ui.radioOR->isChecked()) nLogic = LOGIC_OR_IDX;
	else if(ui.radioLeftBracket->isChecked()) nLogic = LOGIC_LEFTBRACKET_IDX;
	else if(ui.radioRightBracket->isChecked()) nLogic = LOGIC_RIGHTBRACKET_IDX;
	else if(ui.radioNOTAND_lb->isChecked()) nLogic = LOGIC_NOTAND_LB_IDX;
	else if(ui.radioOR_lb->isChecked()) nLogic = LOGIC_OR_LB_IDX;
	else if(ui.radioNOT_lb->isChecked()) nLogic = LOGIC_NOT_LB_IDX;

	AddLine(nLogic, ui.cboCustSelectionField->currentIndex(), ui.cboCustSelectionOperation->currentIndex(),ui.txtCustSelectionValue->text());
	
}

QString CustomSelectionsWnd::getOperationFromIdx(int nOp)
{
	switch(nOp){
		case 0: return "=";
		case 1: return ">";
		case 2: return "<";
		case 3: return ">=";
		case 4: return "<=";
		case 5: return "<>";
		case 6: return "LIKE";
		case 7: return "BEGINS WITH";
		default: return "";
	}
}

QString CustomSelectionsWnd::getLogicDisplay(int nLogic)
{
	switch(nLogic){
		case LOGIC_AND_IDX: return ("AND");
		case LOGIC_OR_IDX: return ("OR");
		case LOGIC_LEFTBRACKET_IDX: return ("(");
		case LOGIC_RIGHTBRACKET_IDX: return (")");
		case LOGIC_NOTAND_LB_IDX: return ("NOT AND(");
		case LOGIC_OR_LB_IDX: return ("OR(");
		case LOGIC_NOT_LB_IDX: return ("NOT(");
		default: return "";
	}
}
int CustomSelectionsWnd::getLogicIndex(QString strLogic)
{
	if(strLogic == "AND") 
		return LOGIC_AND_IDX;
	else if(strLogic == "OR") 
		return LOGIC_OR_IDX;
	else if(strLogic == "(") 
		return LOGIC_LEFTBRACKET_IDX;
	else if(strLogic == ")") 
		return LOGIC_RIGHTBRACKET_IDX;
	else if(strLogic == "NOT AND(") 
		return LOGIC_NOTAND_LB_IDX;
	else if(strLogic == "OR(") 
		return LOGIC_OR_LB_IDX;
	else if(strLogic == "NOT(") 
		return LOGIC_NOT_LB_IDX;
	else 
		return -1;
}

void CustomSelectionsWnd::AddLine(int nLogic, int nField, int nOperation, QString strValue)
{
	//
	// prepare new row for inserting data
	//
	m_lstSelectionData.addRow();
	int nRow=m_lstSelectionData.getRowCount()-1;

	if(nLogic==LOGIC_LEFTBRACKET_IDX ||  nLogic==LOGIC_RIGHTBRACKET_IDX || nLogic==LOGIC_NOT_LB_IDX || nLogic==LOGIC_NOTAND_LB_IDX || nLogic==LOGIC_OR_LB_IDX)
	{
		QString strLogicDisplay = getLogicDisplay(nLogic);
		m_lstSelectionData.setData(nRow,"m_nLogic",nLogic);
		m_lstSelectionData.setData(nRow,"m_nField",-1);
		m_lstSelectionData.setData(nRow,"m_nOperation",-1);
		m_lstSelectionData.setData(nRow,"m_strValue","");
		m_lstSelectionData.setData(nRow,"strLogic",strLogicDisplay);
		m_lstSelectionData.setData(nRow,"strField","");
		m_lstSelectionData.setData(nRow,"strOperation","");
		m_lstSelectionData.setData(nRow, "strDbField", "");
		
		ui.tableWidgetCustSelection->RefreshDisplay();
		return;
	}

	//
	// set intern values
	// 
	m_lstSelectionData.setData(nRow,"m_nLogic",nLogic);
	m_lstSelectionData.setData(nRow,"m_nField",ui.cboCustSelectionField->currentIndex());
	m_lstSelectionData.setData(nRow,"m_nOperation",ui.cboCustSelectionOperation->currentIndex());
	m_lstSelectionData.setData(nRow,"m_strValue",ui.txtCustSelectionValue->text());

	//
	// set display values
	//
	QString strLogicDisplay = getLogicDisplay(nLogic);
	int nCboField = m_lstSelectionData.getDataRef(nRow,"m_nField").toInt();
	int nCboOperation = m_lstSelectionData.getDataRef(nRow,"m_nOperation").toInt();
	QString strField=ui.cboCustSelectionField->itemData(nCboField , Qt::DisplayRole).toString();
	QString strDbField=ui.cboCustSelectionField->itemData(nCboField , Qt::UserRole).toString();
	QString strOperation=ui.cboCustSelectionOperation->itemData(nCboOperation ,Qt::DisplayRole).toString();

	m_lstSelectionData.setData(nRow,"strLogic",strLogicDisplay);
	m_lstSelectionData.setData(nRow,"strField",strField);
	m_lstSelectionData.setData(nRow,"strOperation",strOperation);
	m_lstSelectionData.setData(nRow, "strDbField", strDbField);

	ui.tableWidgetCustSelection->RefreshDisplay();
}

// duplicate
void CustomSelectionsWnd::on_btnCustSelectionClear_clicked()
{
	// clear table
	m_lstSelectionData.clear();
	ui.tableWidgetCustSelection->RefreshDisplay();

	// reset controls
	ui.radioAND->setChecked(true);
	ui.cboCustSelectionField->setCurrentIndex(0);
	ui.cboCustSelectionOperation->setCurrentIndex(0);
	ui.txtCustSelectionValue->setText("");
}

// remove selected selection line
void CustomSelectionsWnd::on_btnCustSelectionRemoveLine_clicked()
{
	int nRowSelected=m_lstSelectionData.getSelectedRow();
	if (nRowSelected>=0)
	{
		m_lstSelectionData.deleteRow(nRowSelected);
		ui.tableWidgetCustSelection->RefreshDisplay();
	}
}

// move selected line one row up
void CustomSelectionsWnd::on_btnCustSelectionMoveUp_clicked()
{
	int nRowSelected=m_lstSelectionData.getSelectedRow();
	if (nRowSelected>0)
	{
		DbRecordSet rowData=m_lstSelectionData.getRow(nRowSelected);
		m_lstSelectionData.deleteRow(nRowSelected);
		m_lstSelectionData.insertRow(nRowSelected-1);
		m_lstSelectionData.assignRow(nRowSelected-1,rowData);
		ui.tableWidgetCustSelection->RefreshDisplay();
	}
}

// move selected line one row down
void CustomSelectionsWnd::on_btnCustSelectionMoveDown_clicked()
{
	int nRowSelected=m_lstSelectionData.getSelectedRow();
	if (nRowSelected>=0 && nRowSelected<m_lstSelectionData.getRowCount()-1)
	{
		DbRecordSet rowData=m_lstSelectionData.getRow(nRowSelected);
		m_lstSelectionData.deleteRow(nRowSelected);
		m_lstSelectionData.insertRow(nRowSelected+1);
		m_lstSelectionData.assignRow(nRowSelected+1,rowData);
		ui.tableWidgetCustSelection->RefreshDisplay();
	}
}

void CustomSelectionsWnd::LoadSelectionXML(QString strXML)
{
	m_lstSelectionData.clear();

	//parse XML:
	QDomDocument domDocument;
	if (!domDocument.setContent(strXML)) {
		qDebug()<<"Failed to parse XML";
		return;
	}
	
	// TOFIX <params><param>.. 

	// <term> nodes
	QString strDbField, strDisplayField, strDbOperation, strDisplayOperation, strValue, strLogic;
	QDomNodeList termList = domDocument.elementsByTagName("term");
	int nLogicIdx, nFieldIdx, nOperationIdx;
	int nTermCnt = termList.size();
	
	for(int i=0; i<nTermCnt; i++)
	{
		// new row
		m_lstSelectionData.addRow();
		int nRow = m_lstSelectionData.getRowCount()-1;

		// data
		strLogic			= termList.at(i).firstChildElement("log").text();	// ex: "AND"
		strDbField			= termList.at(i).firstChildElement("par").text();	// ex: "BUSP_CODE"
		strDbOperation		= termList.at(i).firstChildElement("op").text();	// ex: "eq"
		strValue			= termList.at(i).firstChildElement("const").text();	// ex: "1"
		
		nLogicIdx			= getLogicIndex(strLogic);														// ex: 0
		nFieldIdx			= ui.cboCustSelectionField->findData(strDbField,Qt::UserRole);					// ex: 0
		strDisplayField		= ui.cboCustSelectionField->itemData(nFieldIdx , Qt::DisplayRole).toString();	// ex: "Project Code" (BUSP_CODE)
		strDisplayOperation = DecodeXmlValue(strDbOperation);												// ex: "=" (eq)
		nOperationIdx		= ui.cboCustSelectionOperation->findData(strDisplayOperation,Qt::DisplayRole);	// ex: 0
		
		// set intern values
		m_lstSelectionData.setData(nRow,"m_nLogic",nLogicIdx);
		m_lstSelectionData.setData(nRow,"m_nField",nFieldIdx);
		m_lstSelectionData.setData(nRow,"m_nOperation",nOperationIdx);	
		m_lstSelectionData.setData(nRow,"m_strValue",strValue);

		// set display values
		m_lstSelectionData.setData(nRow,"strLogic",strLogic);	
		m_lstSelectionData.setData(nRow,"strField",strDisplayField);
		m_lstSelectionData.setData(nRow,"strOperation",strDisplayOperation);
		m_lstSelectionData.setData(nRow,"strDbField", strDbField);		
	}
	ui.tableWidgetCustSelection->RefreshDisplay();
}

QString CustomSelectionsWnd::GetSelectionXML()
{
	QString strXML="<xml><selection><params></params><terms>";
	int nSize=m_lstSelectionData.getRowCount();
	QString strLog, strPar, strOp, strConst;
	for (int i=0; i<nSize; i++)
	{
		strLog = m_lstSelectionData.getDataRef(i, "strLogic").toString();		// logical operation "AND, OR, .."
		strPar = m_lstSelectionData.getDataRef(i, "strDbField").toString();		// field name value
		//FIX: string below was translated version "BEGINS WITH" -> "BEGGINT MIT" so it created an invalid SQL query
		//strOp = m_lstSelectionData.getDataRef(i, "strOperation").toString();	// operation "+,-,.."
		int nOpIdx = m_lstSelectionData.getDataRef(i, "m_nOperation").toInt();	// index for operations: "+,-,.."
		strOp = getOperationFromIdx(nOpIdx);
		strConst  = m_lstSelectionData.getDataRef(i, "m_strValue").toString();  // value
		strXML+="<term>"
					"<log>"+strLog+"</log>"+
					"<par>"+strPar+"</par>"+
					"<op>"+EncodeXmlValue(strOp)+"</op>"+
					"<const>"+strConst+"</const>"+
				"</term>";	
	}
	strXML+="</terms></selection></xml>";
	return strXML;
}

QString CustomSelectionsWnd::EncodeXmlValue(QString strValue)
{
	if(strValue == "=") 
		return "eq";			
	else if(strValue == "<") 
		return "lt";			
	else if(strValue == "<=") 
		return "le";			
	else if(strValue == ">") 
		return "gt";			
	else if(strValue == ">=") 
		return "ge";			
	else if(strValue == "<>") 
		return "ne";		
	else 
		return strValue;
}

QString CustomSelectionsWnd::DecodeXmlValue(QString strValue)
{
	if(strValue == "eq") 
		return "=";			
	else if(strValue == "lt") 
		return "<";			
	else if(strValue == "le") 
		return "<=";			
	else if(strValue == "gt") 
		return ">";			
	else if(strValue == "ge") 
		return ">=";			
	else if(strValue == "ne") 
		return "<>";		
	else if(strValue == "BEGINS WITH") 
		return tr("BEGINS WITH");		
	else if(strValue == "LIKE") 
		return tr("LIKE");		
	else 
		return strValue;
}

bool CustomSelectionsWnd::IsValid()
{
	return true;
}

void CustomSelectionsWnd::on_radioLeftBracket_toggled( bool checked )
{
	if(checked){
		DisableFieldsOnBracketOperators(true);
	}
}

void CustomSelectionsWnd::on_radioRightBracket_toggled( bool checked )
{
	if(checked){
		DisableFieldsOnBracketOperators(true);
	}
}

void CustomSelectionsWnd::on_radioNOTAND_lb_toggled( bool checked )
{
	if(checked){
		DisableFieldsOnBracketOperators(true);
	}
}

void CustomSelectionsWnd::on_radioOR_lb_toggled( bool checked )
{
	if(checked){
		DisableFieldsOnBracketOperators(true);
	}
}

void CustomSelectionsWnd::on_radioNOT_lb_toggled( bool checked )
{
	if(checked){
		DisableFieldsOnBracketOperators(true);
	}
}

void CustomSelectionsWnd::on_radioAND_toggled( bool checked )
{
	if(checked){
		DisableFieldsOnBracketOperators(false);
	}
}

void CustomSelectionsWnd::on_radioOR_toggled( bool checked )
{
	if(checked){
		DisableFieldsOnBracketOperators(false);
	}
}

void CustomSelectionsWnd::DisableFieldsOnBracketOperators(bool bDisable)
{
	ui.cboCustSelectionField->setDisabled(bDisable);
	ui.cboCustSelectionOperation->setDisabled(bDisable);
	ui.txtCustSelectionValue->setDisabled(bDisable);
}	

void CustomSelectionsWnd::OnPersonChanged(int nPersonID)
{
	DbRecordSet recData;
	ui.frmPerson->GetCurrentEntityRecord(nPersonID,recData);
	if (recData.getRowCount()>0)
	{
		QString strPersonCode = recData.getDataRef(0,"BPER_CODE").toString();
		if (ui.frmPerson->isVisible())
			ui.txtCustSelectionValue->setText(strPersonCode);
	}
}

void CustomSelectionsWnd::on_cboCustSelectionField_currentIndexChanged(int nIdx)
{
	bool bShow=false;
	if (nIdx>=0)
	{
		QString strDbField=ui.cboCustSelectionField->itemData(nIdx , Qt::UserRole).toString();
		if (strDbField=="BPER_CODE_PROJ_LEADER" || strDbField=="BPER_CODE_RESPONSIBLE")
		{
			bShow=true;
		}
	}
	if (bShow)
	{
		ui.frmPerson->setVisible(true);
		ui.labelPerson->setVisible(true);
	}
	else
	{
		ui.frmPerson->setVisible(false);
		ui.labelPerson->setVisible(false);
	}
}



/*
<xml>
	<selection>
		<params>
			<param>
				<pnum>1</pnum>
				<pname>Date1</pname>
				<ptype>date</ptype>
			</param>
			<param>
				<pnum>2</pnum>
				<pname>Date2</pname>
				<ptype>date</ptype>
			</param>
		</params>
		<terms>
			<term>
				<log>AND</log>
				<par>PR_STARTDAT</par>
				<op>">="</op>
				<const>Date1</const>
			</term>
			<term>
				<log>AND</log>
				<par>PR_CODE</par>
				<op>"BEGINS WITH"</op>
				<const>"P.E."</const>
			</term>
		</terms>
	</selection>
</xml>
*/

