#ifndef ACCUSERRECORDUSERTABLE_H
#define ACCUSERRECORDUSERTABLE_H

#include "gui_core/gui_core/universaltablewidgetex.h"

/*!
	\class  AccUserRecordUserTable
	\brief  GUI widget for handling AR for users
	\ingroup Bus_Client

	-header (UserCode/ UserName/ Read chkbox/Modify chkbox/Full chkbox/ Group Dependent chkbox/  GroupCode-GroupName)
	-recreate table based on UAR data from CORE_ACC_USER_REC,BUS_GROUP_ITEMS, BUS_GROUP_TREE, BUS_CM_GROUP
	-add new on drop/add: new UAR records (no id)
	-result are UAR records (with or without id, with or without parent<->link)

	<record_set:
	- user_code, user_name, uar<all from CORE_ACC_USER_REC> for given record_id/table_id+group tree id/name/code/name with highest rights
	  (UserCode/ UserName/ Read chkbox/Modify chkbox/Full chkbox/ Group Dependent chkbox/  GroupCode-GroupName)

	 TO DO:
	 - if Others then disable Group dep
	 - intercept Group dep check: if uncheck: ok, if check->recalc rights
	 - On drop from groups tree or from contacts-> Add
	 - add context....

*/
class AccUserRecordUserTable : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	AccUserRecordUserTable(QWidget *parent);
	~AccUserRecordUserTable();

	void UpdateCheckBoxStates(int nRow);

signals:
	void CalcUARMaxRightForGroupDep(int nRow);
	void Signal_AddUser();
	void Signal_RemoveUser();
	void Signal_AssignMySelf();
	void Signal_RemoveGroupDep();
	void Signal_AssignToAll();

public slots:
	void RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);

private slots:
	void OnDataChanged(int nRow, int nCol);
	

private:
	void EnableItem(bool bEnable,int nRow,int nCol);
	void CreateContextMenu();

	
};

#endif // ACCUSERRECORDUSERTABLE_H
