#ifndef DLG_TEMPLATEDB_H
#define DLG_TEMPLATEDB_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_templatedb.h"
#include "common/common/dbrecordset.h"
#include "clientbackupmanager.h"

class Dlg_TemplateDb : public QDialog
{
	Q_OBJECT

public:
	Dlg_TemplateDb(QWidget *parent = 0);
	~Dlg_TemplateDb();

	bool Initialize(ClientBackupManager *pBackupManager);		//if false, do not open dialog

private slots:
		void on_btnCancel_clicked();
		void on_btnOK_clicked();

private:
	Ui::Dlg_TemplateDbClass ui;
	DbRecordSet m_lstData;
	QString m_strTemplateName;
	int m_nAskForTemplate,m_nStartStartUpWizard;
	ClientBackupManager *m_pBackupManager;

};

#endif // DLG_TEMPLATEDB_H
