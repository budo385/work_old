#include "bus_client/bus_client/phoneselectordlg.h"

PhoneSelectorDlg::PhoneSelectorDlg(QStringList &lstPhones, QStringList &lstNames, QStringList &lstTypes, QStringList lstPreselectedPhones,QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	//QStringList &lstPhones, QStringList &lstNames, QStringList &lstTypes, 
	ui.listWidget->setColumnCount(3);
	ui.listWidget->setColumnWidth(0, 130);
	ui.listWidget->setColumnWidth(1, 90);
	//ui.listWidget->setColumnWidth(2, 50);
	//ui.listWidget->header()->setHidden(false);
	QStringList lstLabels;
	lstLabels << tr("Phone") << tr("Name") << tr("Type");
	ui.listWidget->setHeaderLabels(lstLabels);

	//if(bSingleSelection) 
		ui.listWidget->setSelectionMode(QAbstractItemView::SingleSelection);
	//else
	//	ui.listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);


	int nCnt = lstPhones.size();
	for(int i=0; i<nCnt; i++)
	{
		QStringList lstRow;
		lstRow << lstPhones[i] << lstNames[i] << lstTypes[i];
		QTreeWidgetItem *item = new QTreeWidgetItem(lstRow);
		if (lstPreselectedPhones.indexOf(lstPhones[i])>=0)
			item->setSelected(true);
		ui.listWidget->addTopLevelItem (item);
	}
}

PhoneSelectorDlg::~PhoneSelectorDlg()
{

}

void PhoneSelectorDlg::on_btnOK_clicked()
{
	done(1);
}
	
void PhoneSelectorDlg::on_btnCancel_clicked()
{
	done(0);
}

QString PhoneSelectorDlg::getSelectedPhone()
{
	QTreeWidgetItem *item = ui.listWidget->currentItem();	
	if(item)
		return item->text(0);

	return QString();
}

int PhoneSelectorDlg::getSelectedPhoneIdx()
{
	QTreeWidgetItem *item = ui.listWidget->currentItem();	
	if(item)
		return ui.listWidget->indexOfTopLevelItem(item);
	return -1;
}
