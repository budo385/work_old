#ifndef SELECTION_COMBOBOX_H
#define SELECTION_COMBOBOX_H


#include <QtWidgets/QMessageBox>
#include <QComboBox>
#include <QObject>

#include "common/common/status.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "gui_core/gui_core/guifield.h"


/*!
	\class  Selection_ComboBox
	\brief  Selection_ComboBox for feeding entity data to combo-boxes
	\ingroup GUICore


	Use:
	- register own combo and associated field or table
	- all changes are propagated, if cache changes, data will be updated
*/
class Selection_ComboBox : public QObject, public MainEntitySelectionController
{
	Q_OBJECT

public:
	Selection_ComboBox();
	~Selection_ComboBox();

	void Initialize(int nEntityID);
	void SetDistinctDisplay(){m_bShowDistinct=true;};
	void SetDataForCombo(QComboBox* pCombo,QString strCmbDisplay,QString strCmbMapFld="",QString strDataMapFldTo="",GuiDataManipulator *plstData=NULL, GuiField *pField=NULL);
	int GetSelection(DbRecordSet &selectedRow);
	int GetSelectedID();
	QString GetSelectedName();
	void SetCurrentIndexFromName(QString strDisplayName);
	void SetCurrentIndexFromID(int nID);
	bool blockSignals(bool b){blockObserverSignals(b); return true;}
	void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nRowInsideDataSource=-1);

private:

	//mapping ops:
	QComboBox* m_pCombo;
	int m_nCmbMapFldIdx;
	int m_nCmbDisplayIdx;
	int m_nDataMapFldToIdx;
	GuiDataManipulator *m_plstData;
	GuiField *m_pField;					//connected field that will be refreshed when index changed.
	bool m_bShowDistinct;
	
private slots:
	void on_cmbName_currentIndexChanged(int index);
};


class Selection_ComboBoxEx : public QObject, public MainEntitySelectionController
{
	Q_OBJECT

public:

	void Initialize(int nEntityID,QString strDisplayCol="",QString strIDColumn="");
	void ReloadCombo(QComboBox* pCombo,bool m_bShowDistinct=false);
	void SetComboSelection(QComboBox* pCombo,QVariant varID);
	void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nRowInsideDataSource=-1){emit SignalReloadCombo();};

signals:
	void SignalReloadCombo();

private:
};




#endif // SELECTION_COMBOBOX_H
