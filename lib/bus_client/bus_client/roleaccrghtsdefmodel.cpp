#include "roleaccrghtsdefmodel.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
#include "bus_client/bus_client/servermessagehandlerset.h"
#include "trans/trans/httpclientconnectionsettings.h"
extern BusinessServiceManager *g_pBoSet;

RoleAccRghtsDefModel::RoleAccRghtsDefModel(QObject *Parent /*= NULL*/) : DbRecordSetListModel(Parent)
{

}

RoleAccRghtsDefModel::~RoleAccRghtsDefModel()
{

}

/*!
Load model (call BO) model.

\param &pStatus			- status.
\param Source			- Search by RoleID (if -1 then search without role).
\param SourceID			- Search by AccRSetID (if -1 then search without access right 
*/
void RoleAccRghtsDefModel::LoadModel(Status &pStatus, int Source, int SourceID)
{
	//Clear recordset.
	m_pDbRecordSet->clear();

	//Call BO.
	_SERVER_CALL(AccessRights->GetAccessRights(pStatus, *m_pDbRecordSet, Source, SourceID))
	if(!pStatus.IsOK())
	{
		qDebug() << pStatus.getErrorText();
		return;
	}
	else
		qDebug() << "Access rights retrieved.";

	//Initialize model.
	InitializeModel(m_pDbRecordSet);

	//Clear recordset.
	m_pDbRecordSet->clear();
}

/*!
Set header data.

\param index - section(column or row).
\param orientation - orientation.
\param value	 - value.
\param role	 - role.
\return bool.
*/
bool RoleAccRghtsDefModel::setHeaderData ( int section, Qt::Orientation orientation, const QVariant &value, int role)
{
	if (orientation == Qt::Horizontal)
	{
		m_pRootItem->SetData(section, value);
		return true;
	}

	return true;
}

Qt::ItemFlags RoleAccRghtsDefModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;

	Qt::ItemFlags DefaultFlags = QAbstractItemModel::flags(index);
	return DefaultFlags | Qt::ItemIsEditable;
}

bool RoleAccRghtsDefModel::setData ( const QModelIndex &index, const QVariant &value, int role /*= Qt::EditRole*/)
{
	if (index.isValid() && role == Qt::EditRole) 
	{
		//Get view row.
		int row = index.row();
		//Get item.
		DbRecordSetItem *item = m_hshModelItems.value(row);
		//Get item RowID.
		int RowID = item->GetRowId();
		Status status;

		QString str = value.toString();
		
		//Call BO.
		_SERVER_CALL(AccessRights->SetAccessRightValue(status, RowID, str))
		if(!status.IsOK())
		{
			qDebug() << status.getErrorText();
			return false;
		}
		else
			qDebug() << "Access right value set.";

		//Set value description.
		if (item->data(1).toInt() == 2)
			item->SetData(3, value);
		else
			item->SetData(2, value);

		emit dataChanged(index, index);
		return true;
	}

	return false;
}

Qt::DropActions RoleAccRghtsDefModel::supportedDropActions() const
{
	return Qt::IgnoreAction;
}

/*!
Setup model data from recordset.

\param Parent	 - Parent item.
*/
void RoleAccRghtsDefModel::SetupModelData(DbRecordSetItem *Parent /*= NULL*/)
{
	int row_count = m_pDbRecordSet->getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Row ID.
		int RowId = m_pDbRecordSet->getDataRef(row, 0).toInt();

		//Set some data to display.
		QList<QString> data;
		QString AccRName  = m_pDbRecordSet->getDataRef(row, 5).toString();
		QString AccRType  = m_pDbRecordSet->getDataRef(row, 6).toString();
		QString AccRValue = m_pDbRecordSet->getDataRef(row, 7).toString();
		QString AccRDescr = m_pDbRecordSet->getDataRef(row, 8).toString();
		QString AccRCode  = m_pDbRecordSet->getDataRef(row, 9).toString();

		//Set display data.
		QString DisplayData = AccRCode + " " + AccRName;
		data << DisplayData;

		//Set other data.
		data << AccRType << AccRValue << AccRDescr;

		//Role type (business or system).
		int RoleType = m_pDbRecordSet->getDataRef(row, 6).toInt();
		//Add some Icon.
		QStringList IconList;
		IconList << "3";

		//Create new item. In list item don't have children and Parent is always root item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, 0);
		m_hshModelItems.insert(row, item);
	}
	//Clear recordset.
	m_pDbRecordSet->clear();
}
