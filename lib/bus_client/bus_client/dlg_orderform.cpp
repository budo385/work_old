#include "dlg_orderform.h"
#include <QDesktopServices>

//GLOBAL BO SET:
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include <QUrl>

Dlg_OrderForm::Dlg_OrderForm(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle(tr("Order Now"));
}

Dlg_OrderForm::~Dlg_OrderForm()
{

}


void Dlg_OrderForm::on_btnTrial_clicked()
{
	//open web
	QDesktopServices::openUrl(m_strOrderURL);
	done(1);

}
void Dlg_OrderForm::on_btnFull_clicked()
{
	//open web
	QDesktopServices::openUrl(m_strOrderURL);
	done(1);
}
void Dlg_OrderForm::on_btnCancel_clicked()
{

	done(1);
}


void Dlg_OrderForm::on_btnContinue_clicked()
{
	done(0);
}
