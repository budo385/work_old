#include "dlg_accuserrecord.h"
#include "gui_core/gui_core/thememanager.h"
#include "common/common/entity_id_collection.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "gui_core/gui_core/universaltablewidgetex.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/accuarcore.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "gui_core/gui_core/gui_helper.h"
#include "selectionpopup.h"
#include "gui_core/gui_core/wizardbase.h"
#include "simpleselectionwizpage.h"
#include "gui_core/gui_core/macros.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;


Dlg_AccUserRecord::Dlg_AccUserRecord(QWidget *parent)
	: QDialog(parent)
{
	m_nParentRecordId=-1;
	m_nParentTableId=-1;
	m_sParentName="";

	ui.setupUi(this);
	ui.frameHeader->setStyleSheet("QFrame#frameHeader "+ThemeManager::GetSidebarChapter_Bkg());
	ui.labelRecord->setStyleSheet("QLabel " + ThemeManager::GetSidebarChapter_Font());
	ui.labelRecord->setAlignment(Qt::AlignLeft);

	setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
	mPersonCache.Initialize(ENTITY_BUS_PERSON);
	mPersonCache.ReloadData();
	ui.frameContact->Initialize();
	ui.frameContact->ShowOnlyUserContacts();

	//mProjectCache.Initialize(ENTITY_BUS_PROJECT);
	//mProjectCache.ReloadData();

	ui.ckbChildInherit->setChecked(false);
	ui.ckbCommChildInherit->setChecked(false);
	ui.ckbParentInherit->setChecked(false);

	//GRID:
	AccUARCore::DefineDisplayUARList(m_lstUAR);
	AccUARCore::DefineDisplayGARList(m_lstGAR);

	//code, username, read chx, modify chx, full chx, is group dep chx, group code/name, tree name
	DbRecordSet columns;
	UniversalTableWidgetEx::AddColumnToSetup(columns,"BPER_CODE",tr("Code"),80,false);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"BPER_NAME",tr("Name"),100,false);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"CX_READ",tr("Read"),50,true,"",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"CX_MODIFY",tr("Modify"),50,true,"",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"CX_FULL",tr("Full"),50,true,"",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"CUAR_GROUP_DEPENDENT",tr("Group Dep."),70,true,"",UniversalTableWidgetEx::COL_TYPE_CHECKBOX);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"BGTR_NAME",tr("Tree Name"),100,false);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"BGIT_CODE",tr("Group Code"),80,false);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"BGIT_NAME",tr("Group Name"),140,false);

	ui.tableUAR->Initialize(&m_lstUAR,&columns);
	ui.tableUAR->EnableDrop(true);
	ui.tableUAR->SetEditMode(true);

	connect(ui.tableUAR,SIGNAL(CalcUARMaxRightForGroupDep(int)),this,SLOT(OnCalcUARMaxRightForGroupDep(int)));
	connect(ui.tableUAR,SIGNAL(Signal_AddUser()),this,SLOT(on_btnAddSelected_UAR_clicked()));
	connect(ui.tableUAR,SIGNAL(Signal_RemoveUser()),this,SLOT(on_btnRemoveSelected_UAR_clicked()));
	connect(ui.tableUAR,SIGNAL(Signal_AssignMySelf()),this,SLOT(on_btnAsignMySelf_clicked()));
	connect(ui.tableUAR,SIGNAL(Signal_RemoveGroupDep()),this,SLOT(on_btnDeclareAsNonGroupDep_clicked()));
	connect(ui.tableUAR,SIGNAL(Signal_AssignToAll()),this,SLOT(on_btnAsignToSelected_clicked()));
	connect(ui.tableUAR,SIGNAL(SignalDataDroped(int,DbRecordSet &,QDropEvent *)),this,SLOT(OnTableDataDroped(int,DbRecordSet &,QDropEvent *)));

	ui.treeGAR->Initialize(&m_lstGAR);
	connect(ui.treeGAR,SIGNAL(DataChanged()),this,SLOT(OnGroupTreeChanged()));
	connect(ui.treeGAR,SIGNAL(UpdateUARRights()),this,SLOT(OnGroupTreeUpdateAR()));
	connect(ui.treeGAR,SIGNAL(SignalDataDroped(int,DbRecordSet &,QDropEvent *)),this,SLOT(OnTreeDataDroped(int,DbRecordSet &,QDropEvent *)));

	ui.btnAddSelected_UAR->setIcon(QIcon(":Selection_Arrow_Right.png"));
	ui.btnRemoveSelected_UAR->setIcon(QIcon(":Selection_Arrow_Remove.png"));
	ui.btnAddSelected_GAR->setIcon(QIcon(":Selection_Arrow_Right.png"));
	ui.btnRemoveSelected_GAR->setIcon(QIcon(":Selection_Arrow_Remove.png"));
	ui.btnAddSelected_UAR->setIconSize(QSize(50,55));
	ui.btnRemoveSelected_UAR->setIconSize(QSize(50,55));
	ui.btnAddSelected_GAR->setIconSize(QSize(50,55));
	ui.btnRemoveSelected_GAR->setIconSize(QSize(50,55));
	ui.btnAddSelected_UAR->setToolTip(tr("Add Selected Users"));
	ui.btnRemoveSelected_UAR->setToolTip(tr("Remove Selected Users"));
	ui.btnAddSelected_GAR->setToolTip(tr("Add Selected Groups"));
	ui.btnRemoveSelected_GAR->setToolTip(tr("Remove Selected Groups"));
}

Dlg_AccUserRecord::~Dlg_AccUserRecord()
{

}

//inits with predefined recordsets, if nots try to load from server
void Dlg_AccUserRecord::Initialize (Status &err, bool bReadOnly,QString strRecordDisplayName,int nLoggedPersonId,int nRecordId,int nTableId,DbRecordSet *lstUAR,DbRecordSet *lstGAR)
{
	m_nRecordId=nRecordId;
	m_nTableId=nTableId;
	m_nLoggedPersonId=nLoggedPersonId;

	if (lstUAR == NULL || lstGAR == NULL )
	{
		Status err;
		_SERVER_CALL(AccessRights->ReadGUAR(err,nRecordId,nTableId,m_lstUAR,m_lstGAR))
		_CHK_ERR(err);
	}

	m_lstUAR=*lstUAR;
	m_lstGAR=*lstGAR;
	m_nParentRecordId=-1;
	_SERVER_CALL(AccessRights->PrepareGUAR(err,nRecordId,nTableId, m_lstUAR, m_lstGAR, m_lstGroupUsers,m_nParentRecordId,m_nParentTableId,m_sParentName))
	_CHK_ERR(err);


	//get CHK boxes (if one found, set it):
	if(m_lstUAR.find("CUAR_CHILD_INHERIT",1,true)>=0)
		ui.ckbChildInherit->setChecked(true);
	if(m_lstUAR.find("CUAR_CHILD_COMM_INHERIT",1,true)>=0)
		ui.ckbCommChildInherit->setChecked(true);
	if(m_lstGAR.find("CGAR_CHILD_INHERIT",1,true)>=0)
		ui.ckbChildInherit->setChecked(true);
	if(m_lstGAR.find("CGAR_CHILD_COMM_INHERIT",1,true)>=0)
		ui.ckbCommChildInherit->setChecked(true);

	//Add empty record:
	int nOthersRow=m_lstUAR.find("CUAR_PERSON_ID",0,true);
	if (nOthersRow<0)
	{
		DbRecordSet lstUAR;
		GenerateNewUARFromPersonID(QVariant(QVariant::Int),lstUAR); //null person with FULL rights
		m_lstUAR.merge(lstUAR);
	}
	else //set name= others
	{
		m_lstUAR.setData(nOthersRow,"BPER_NAME",tr("Others"));
	}


	/*
	ui.labelRecord->setText(	QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
								<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:18pt; font-weight:800; font-style:italic; color:%1;\">%2</span></p>\
								</body></html>").arg(ThemeManager::GetToolbarTextColor()).arg(strRecordDisplayName));
	*/
	ui.labelRecord->setText(strRecordDisplayName);

	if (m_nParentRecordId>0)
	{
		ui.ckbParentInherit->setChecked(true);
		ui.labelParentName->setText(	QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
											<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600; font-style:italic; color:%1;\">%2</span></p>\
											</body></html>").arg(ThemeManager::GetToolbarTextColor()).arg(m_sParentName));

		ui.groupBoxGAR->setEnabled(false);
		ui.groupBoxUAR->setEnabled(false);
		ui.ckbChildInherit->setEnabled(false);
		//ui.ckbChildInherit->setChecked(true);
		ui.ckbCommChildInherit->setEnabled(false);
		//ui.ckbCommChildInherit->setChecked(true);
	}
	else
	{
		ui.ckbParentInherit->setEnabled(false); //-> can not re-enable
		ui.ckbParentInherit->setChecked(false);
		ui.labelParentName->setText("");
	}

	if (nTableId!=BUS_PROJECT)
	{
		ui.ckbChildInherit->setVisible(false);
		ui.ckbCommChildInherit->setVisible(false);
	}
	if (nTableId==BUS_PROJECT || nTableId==BUS_VOICECALLS ||nTableId==BUS_DM_DOCUMENTS ||nTableId==BUS_EMAIL || nTableId==BUS_CM_CONTACT)
		ui.ckbParentInherit->setVisible(true);
	else
		ui.ckbParentInherit->setVisible(false);

	if (nTableId==BUS_PROJECT || nTableId==BUS_CM_CONTACT)
		ui.btnCopy->setVisible(true);
	else
		ui.btnCopy->setVisible(false);


	//ui.groupBoxGAR->setVisible(false); //just for v1
	ui.tableUAR->RefreshDisplay();
	ui.treeGAR->RefreshDisplay();

	if (bReadOnly)
	{
		ui.groupBoxGAR->setEnabled(false);
		ui.groupBoxUAR->setEnabled(false);
		ui.btnCopy->setEnabled(false);
		ui.ckbChildInherit->setEnabled(false);
		ui.ckbCommChildInherit->setEnabled(false);
		ui.ckbParentInherit->setEnabled(false);
		ui.btnOK->setEnabled(false);
	}
}

void Dlg_AccUserRecord::GetResult(DbRecordSet &lstUAR,DbRecordSet &lstGAR)
{
	lstUAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	lstGAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_GROUP_REC));
	lstUAR.merge(m_lstUAR);
	lstGAR.merge(m_lstGAR);
}
void Dlg_AccUserRecord::on_btnCancel_clicked()
{
	done(0);
}


void Dlg_AccUserRecord::on_btnOK_clicked()
{
	if (m_nParentRecordId>0) //if still from parent just ignore
		{done(0); return;}

	if(CheckGUARRecords()) //validate data
		{done(1); return;}
}

void Dlg_AccUserRecord::on_btnCopy_clicked()
{
	//get GUAR records
	if(!CheckGUARRecords()) //validate data
		return;
	DbRecordSet lstUAR,lstGAR;
	GetResult(lstUAR,lstGAR);

	//get selection:
	QString strPK;
	DbRecordSet lstData;

	if (m_nTableId==BUS_PROJECT)
	{
		strPK="BUSP_ID";
		
		QList<WizardPage*> WizardPagesList;
		QHash<int, WizardPagesCollection*> m_hshWizardPageHash;		//< Wizard ID to page collection.
		WizardPagesList << new SimpleSelectionWizPage(WIZARD_PROJECT_SELECTION_PAGE, QString(QObject::tr("Select Projects")), ENTITY_BUS_PROJECT);
		m_hshWizardPageHash.insert(-1, new WizardPagesCollection(WizardPagesList));

		WizardBase wiz(-1, m_hshWizardPageHash, &g_ClientCache);
		if (!wiz.exec())
			return;
		
		QList<DbRecordSet> RecordSetList;
		wiz.GetWizPageRecordSetList(RecordSetList);
		lstData=RecordSetList.at(0);
	}
	else
	{
		SelectionPopup dlgPopUp;

		strPK="BCNT_ID";
		dlgPopUp.Initialize(ENTITY_BUS_CONTACT,true);
		int nResult=dlgPopUp.OpenSelector();
		if(nResult==0)
			return;

		int nRecID;
		dlgPopUp.GetSelectedData(nRecID,lstData);
	}

	//prepare, copy  for each selected entry: cnt_id or proj_id:
	DbRecordSet lstUAR_New,lstGAR_New;
	AccUARCore::CopyGUAR(m_nTableId,lstData,strPK,lstUAR,lstGAR,lstUAR_New,lstGAR_New);
	lstUAR_New.setColValue("CUAR_PARENT_ID",QVariant(QVariant::Int)); //reset all parents!!!
	lstGAR_New.setColValue("CGAR_PARENT_ID",QVariant(QVariant::Int));

	Status err;
	_SERVER_CALL(AccessRights->WriteGUAR(err,-1,m_nTableId, lstUAR_New, lstGAR_New))
	_CHK_ERR(err);
}



//from contact record, expand to uar
void Dlg_AccUserRecord::GenerateNewUARFromContacts(DbRecordSet &lstContacts, DbRecordSet &lstUAR)
{
	DbRecordSet *lstPersons=mPersonCache.GetDataSource();
	AccUARCore::GenerateNewUARFromContacts(m_nRecordId,m_nTableId,lstContacts, lstUAR, *lstPersons);
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_CHILD_INHERIT"),(int)ui.ckbChildInherit->isChecked());
	lstUAR.setColValue(lstUAR.getColumnIdx("CUAR_CHILD_COMM_INHERIT"),(int)ui.ckbCommChildInherit->isChecked());
	AccUARCore::UARList2DisplayList(&lstUAR);
}


//if already set and go uncheck then warn
void Dlg_AccUserRecord::on_ckbParentInherit_clicked()
{
	if (m_nParentRecordId>0 && !ui.ckbParentInherit->isChecked())
	{
		if(QMessageBox::question(this,tr("Warning"),tr("Any further change of the parent record access rights will not be reflected on this record. Proceed?"), tr("Yes"),tr("No"))!=0)
		{
			ui.ckbParentInherit->setChecked(true);
			return;
		}
		m_nParentRecordId=-1;
		m_nParentTableId=-1;
		m_sParentName="";
		ui.groupBoxGAR->setEnabled(true);
		ui.groupBoxUAR->setEnabled(true);

		if (m_nTableId==BUS_PROJECT)
		{
			ui.ckbChildInherit->setEnabled(true);
			ui.ckbCommChildInherit->setEnabled(true);
		}
		ui.labelParentName->setText("");
		ui.ckbParentInherit->setEnabled(false); //prevent to go back!!

		//reset parent id on all records:
		m_lstUAR.setColValue(m_lstUAR.getColumnIdx("CUAR_PARENT_ID"),QVariant(QVariant::Int));
		m_lstGAR.setColValue(m_lstGAR.getColumnIdx("CGAR_PARENT_ID"),QVariant(QVariant::Int));
	}

}

//when data is dropped:
void Dlg_AccUserRecord::OnTableDataDroped(int nDropType, DbRecordSet &lstDropValue, QDropEvent *event)
{
	int nSize=lstDropValue.getRowCount();
	if(nSize==0)
		return;

	switch(nDropType)
	{
	case ENTITY_BUS_CONTACT:
		{
			AddNewUARContacts(lstDropValue);
			return;
		}
		break;
	case ENTITY_BUS_GROUP_ITEMS:
		{
			//set filter on groups:
			QString strWhereIn;
			DbSqlTableDefinition::GenerateChunkedWhereStatement(lstDropValue,"BGIT_ID",strWhereIn,0,nSize+1);
			MainEntityFilter Filter;
			Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID IN "+strWhereIn);

			//read from server:
			Status err;
			_SERVER_CALL(BusContact->ReadShortContactList(err,Filter.Serialize(),lstDropValue))
			_CHK_ERR(err);
			AddNewUARContacts(lstDropValue);
			return;
		}
		break;
	}

}

//if already exists ignore
//filter users
//add on grid, sort it, select it
void Dlg_AccUserRecord::AddNewUARContacts(DbRecordSet &lstContacts)
{
	DbRecordSet lstUAR;
	GenerateNewUARFromContacts(lstContacts,lstUAR);

	lstUAR.clearSelection();
	int nSize=lstUAR.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_lstUAR.find("CUAR_PERSON_ID",lstUAR.getDataRef(i,"CUAR_PERSON_ID").toInt(),true)>=0)
			lstUAR.selectRow(i);
	}
	lstUAR.deleteSelectedRows();

	if (lstUAR.getRowCount()==0)
		return;

	//append to the end and select:
	m_lstUAR.merge(lstUAR);
	m_lstUAR.clearSelection();
	int nStartX=m_lstUAR.getRowCount()-lstUAR.getRowCount();
	nSize=m_lstUAR.getRowCount();
	for(int i=nStartX;i<nSize;i++)
		m_lstUAR.selectRow(i);
	
	ui.tableUAR->RefreshDisplay();
	ui.tableUAR->ScrollToLastSelectedItem();
}


//recalc read, modify, full flgas for group dep UAR records and corresponding group code 
void Dlg_AccUserRecord::OnCalcUARMaxRightForGroupDep(int nRow)
{
	AccUARCore::UpdateUARGroupDependentRights(m_lstGroupUsers,m_lstUAR,m_lstGAR,nRow);
	AccUARCore::UARList2DisplayList(&m_lstUAR,NULL);
	ui.tableUAR->RefreshDisplay();
}


void Dlg_AccUserRecord::on_btnAddSelected_UAR_clicked()
{
	DbRecordSet lstContacts;
	ui.frameContact->GetSelection(lstContacts);
	AddNewUARContacts(lstContacts);
}
void Dlg_AccUserRecord::on_btnRemoveSelected_UAR_clicked()
{
	//forbid 'Others' to delete (deselect):
	int nRow=m_lstUAR.find("CUAR_PERSON_ID",0,true);
	if (nRow>=0)
		m_lstUAR.selectRow(nRow,false);

	m_lstUAR.deleteSelectedRows();
	ui.tableUAR->RefreshDisplay();
}

void Dlg_AccUserRecord::on_btnAddSelected_GAR_clicked()
{
	//get groups:
	Selection_GroupTree* pGroup=ui.frameContact->GetGroupSelector();
	DbRecordSet lstGroups;
	pGroup->GetSelection(lstGroups);
	AddNewGroups(lstGroups);
}
void Dlg_AccUserRecord::on_btnRemoveSelected_GAR_clicked()
{
	ui.treeGAR->DeleteSelected();
}
void Dlg_AccUserRecord::on_btnAsignMySelf_clicked()
{
	//logged user:
	if (m_lstUAR.find("CUAR_PERSON_ID",m_nLoggedPersonId,true)>=0)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Can not assign, you are already assigned!"));
		return;
	}

	DbRecordSet lstUAR;
	GenerateNewUARFromPersonID(m_nLoggedPersonId,lstUAR);
	m_lstUAR.merge(lstUAR);
	ui.tableUAR->RefreshDisplay();
}

void Dlg_AccUserRecord::on_btnDeclareAsNonGroupDep_clicked()
{
	int nSize=m_lstUAR.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_lstUAR.isRowSelected(i))
		{
			m_lstUAR.setData(i,"CUAR_GROUP_DEPENDENT",0);
			ui.tableUAR->UpdateCheckBoxStates(i);
			OnCalcUARMaxRightForGroupDep(i);
		}
	}

	ui.tableUAR->RefreshDisplay();
}
void Dlg_AccUserRecord::on_btnAsignToSelected_clicked()
{
	int nSize=m_lstUAR.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_lstUAR.isRowSelected(i) && m_lstUAR.getDataRef(i,"CUAR_GROUP_DEPENDENT").toInt()==0)
		{
			m_lstUAR.setData(i,"CX_READ",(int)ui.ckbRead_UAR->isChecked());
			m_lstUAR.setData(i,"CX_MODIFY",(int)ui.ckbModify_UAR->isChecked());
			m_lstUAR.setData(i,"CX_FULL",(int)ui.ckbFull_UAR->isChecked());
		}
	}
	ui.tableUAR->RefreshDisplay();
}

void Dlg_AccUserRecord::GenerateNewUARFromPersonID(QVariant varPersonID, DbRecordSet &lstUAR)
{
	AccUARCore::DefineDisplayUARList(lstUAR);

	lstUAR.addRow();
	int nLast=lstUAR.getRowCount()-1;
	lstUAR.setData(nLast,"CUAR_RIGHT",QVariant(AccUARCore::ACC_LVL_FULL).toInt());
	lstUAR.setData(nLast,"CUAR_PERSON_ID",varPersonID); 
	lstUAR.setData(nLast,"CUAR_RECORD_ID",m_nRecordId);
	lstUAR.setData(nLast,"CUAR_TABLE_ID",m_nTableId);
	lstUAR.setData(nLast,"CUAR_CHILD_INHERIT",(int)ui.ckbChildInherit->isChecked());
	lstUAR.setData(nLast,"CUAR_CHILD_COMM_INHERIT",(int)ui.ckbCommChildInherit->isChecked());
	lstUAR.setData(nLast,"CUAR_GROUP_DEPENDENT",0); //user can not click here!

	if (varPersonID.toInt()>0)
	{
		//name:
		DbRecordSet *lstPersons=mPersonCache.GetDataSource();
		int nRow=lstPersons->find("BPER_ID",varPersonID.toInt(),true);
		if (nRow>=0)
		{
			DbRecordSet row = lstPersons->getRow(nRow);
			lstUAR.assignRow(nLast,row,true);
		}
	}
	else
	{
		lstUAR.setData(nLast,"BPER_NAME","Others");
	}

	AccUARCore::UARList2DisplayList(&lstUAR);

}

void Dlg_AccUserRecord::on_ckbRead_UAR_clicked()
{
	ui.ckbModify_UAR->setChecked(false);
	ui.ckbFull_UAR->setChecked(false);
}
void Dlg_AccUserRecord::on_ckbModify_UAR_clicked()
{
	ui.ckbRead_UAR->setChecked(true);
	ui.ckbFull_UAR->setChecked(false);
}
void Dlg_AccUserRecord::on_ckbFull_UAR_clicked()
{
	ui.ckbRead_UAR->setChecked(true);
	ui.ckbModify_UAR->setChecked(true);
}


//if already exists ignore
//if parent node is dragged: I except children...
//add: id, code, etc...
void Dlg_AccUserRecord::AddNewGroups(DbRecordSet &lstGroups)
{
	Selection_GroupTree* pGroup=ui.frameContact->GetGroupSelector();
	pGroup->GetSelection(lstGroups); //get all nodes: including children: drop op does not transmit children!

	DbRecordSet lstGAR;
	AccUARCore::GenerateNewGARFromGroups(m_nRecordId,m_nTableId,lstGroups,lstGAR);
	lstGAR.clearSelection();
	int nSize=lstGAR.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_lstGAR.find("CGAR_GROUP_ID",lstGAR.getDataRef(i,"CGAR_GROUP_ID").toInt(),true)>=0)
			lstGAR.selectRow(i);
	}
	lstGAR.deleteSelectedRows();
	if (lstGAR.getRowCount()==0)
		return;

	//get tree name:
	nSize=lstGAR.getRowCount();
	for(int i=0;i<nSize;i++)
		lstGAR.setData(i,"BGTR_NAME",pGroup->GetTreeName(lstGAR.getDataRef(i,"BGIT_TREE_ID").toInt()));

	//append to the end and select:
	AccUARCore::UARList2DisplayList(NULL,&lstGAR);
	lstGAR.setColValue(lstGAR.getColumnIdx("CGAR_CHILD_INHERIT"),(int)ui.ckbChildInherit->isChecked());
	lstGAR.setColValue(lstGAR.getColumnIdx("CGAR_CHILD_COMM_INHERIT"),(int)ui.ckbCommChildInherit->isChecked());
	
	m_lstGAR.merge(lstGAR);
	ui.treeGAR->RefreshDisplay();

	//expand to last node:
	int nGroupID=-1;
	if (lstGAR.getRowCount()>0)
		nGroupID=lstGAR.getDataRef(lstGAR.getRowCount()-1,"CGAR_GROUP_ID").toInt();
	ui.treeGAR->ExpandItemByRowID(nGroupID);

	OnGroupTreeChanged();
}

//recalc new UAR's from groups
void Dlg_AccUserRecord::OnGroupTreeChanged()
{
	Status err;
	_SERVER_CALL(AccessRights->PrepareGUAR(err,m_nRecordId,m_nTableId, m_lstUAR, m_lstGAR, m_lstGroupUsers,m_nParentRecordId,m_nParentTableId,m_sParentName))
	_CHK_ERR(err);

	ui.tableUAR->RefreshDisplay();
}

void Dlg_AccUserRecord::OnTreeDataDroped(int nDropType, DbRecordSet &lstDropValue, QDropEvent *event)
{
	int nSize=lstDropValue.getRowCount();
	if(nSize==0)
		return;

	switch(nDropType)
	{
	case ENTITY_BUS_GROUP_ITEMS:
		{
			AddNewGroups(lstDropValue);
		}
		break;
	}

}

void Dlg_AccUserRecord::OnGroupTreeUpdateAR()
{
	AccUARCore::UpdateUARGroupDependentRights(m_lstGroupUsers,m_lstUAR,m_lstGAR);
	AccUARCore::UARList2DisplayList(&m_lstUAR,NULL);
	ui.tableUAR->RefreshDisplay();
}

bool Dlg_AccUserRecord::CheckGUARRecords()
{
	Status err;
	AccUARCore::DisplayList2UAR(&m_lstUAR,&m_lstGAR); //store to data
	_SERVER_CALL(AccessRights->PrepareGUAR(err, m_nRecordId,m_nTableId,m_lstUAR, m_lstGAR, m_lstGroupUsers,m_nParentRecordId,m_nParentTableId,m_sParentName)) //refresh group dep
	_CHK_ERR_RET_BOOL_ON_FAIL(err);

	m_lstUAR.setColValue(m_lstUAR.getColumnIdx("CUAR_CHILD_INHERIT"),(int)ui.ckbChildInherit->isChecked());
	m_lstUAR.setColValue(m_lstUAR.getColumnIdx("CUAR_CHILD_COMM_INHERIT"),(int)ui.ckbCommChildInherit->isChecked());
	m_lstGAR.setColValue(m_lstGAR.getColumnIdx("CGAR_CHILD_INHERIT"),(int)ui.ckbChildInherit->isChecked());
	m_lstGAR.setColValue(m_lstGAR.getColumnIdx("CGAR_CHILD_COMM_INHERIT"),(int)ui.ckbCommChildInherit->isChecked());

	//test if some1 has FULL rights:
	if (m_lstUAR.find("CUAR_RIGHT",(int)AccUARCore::ACC_LVL_FULL,true)<0)
		if (m_lstGAR.find("CGAR_RIGHT",(int)AccUARCore::ACC_LVL_FULL,true)<0)
		{
			QMessageBox::warning(this,tr("Warning"),tr("At least one person must have full access right!"));
			return false;
		}

	//if empty (can't be): set to all-> full right, does notify server to erase prev, 
	if (m_lstUAR.getRowCount()==0 && m_lstGAR.getRowCount()==0 )
	{
		Q_ASSERT(false); //can't be both empty!!
		DbRecordSet lstUAR;
		GenerateNewUARFromPersonID(QVariant(QVariant::Int),lstUAR); //null person with FULL rights
		m_lstUAR.merge(lstUAR);
	}

	return true;
}