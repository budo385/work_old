#ifndef SELECTION_USERS_H
#define SELECTION_USERS_H

#include <QtWidgets/QFrame>
#include "./generatedfiles/ui_selection_users.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"


class Selection_Users : public QFrame, public MainEntitySelectionController
{
	Q_OBJECT

public:
	Selection_Users(QWidget *parent = 0);
	~Selection_Users();

	void Initialize(int nEntityID,bool bSkipLoadingData=false,bool bIsFUISelectorWidget=true,bool bSingleClickIsSelection=true,bool bMultiSelection=false,bool bEnableDrag=true); 
	void GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord);
	void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nPrimaryKeyValue=-1);
	bool blockSignals(bool b){blockObserverSignals(b);return QFrame::blockSignals(b);}
	void SetFilter(QString strColumnName, int nColumnVal,int nPosition=-1);
	void ReloadData();


private slots:
	void SlotReloadFromServer();				
	void SlotSelectionChanged();
	void SlotSelectionChanged_DoubleClicked();
	void OnFindWidgetEnterPressed();


private:
	Ui::Selection_UsersClass ui;
	bool m_bIsFUISelectorWidget;
	bool m_bSingleClickIsSelection;

signals:
	void SelectionChanged();

};

#endif // SELECTION_USERS_H
