#ifndef TABLE_SELECTIONCONTACTS_H
#define TABLE_SELECTIONCONTACTS_H

#include "gui_core/gui_core/universaltablewidgetex.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"

/*!
	\class  Table_SelectionContacts
	\brief  TableClass for Contact selection
	\ingroup GUICore

*/

class Table_SelectionContacts : public UniversalTableWidgetEx
{

Q_OBJECT

public:
	Table_SelectionContacts(QWidget * parent);

	void SortList(int nDef=-1);
	void LoadDataFromGroup(int nGroupID,DbRecordSet &lstAllNodes);

signals:
	void ContentChanged();

public slots:
	void RefreshDisplay(int nRow=-1,bool bApplyLastSortModel=false);
protected slots:
	void OnSortColumn(int);
private slots:
	void OnDataDroped(int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);
protected:
	void SetStyleSheetGlobal(){};
private:
};

#endif // TABLE_SELECTIONCONTACTS_H
