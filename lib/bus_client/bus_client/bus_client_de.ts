<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_CH">
<context>
    <name>AccUserRecordGroupTree</name>
    <message>
        <location filename="accuserrecordgrouptree.cpp" line="448"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="accuserrecordgrouptree.cpp" line="449"/>
        <source>Read</source>
        <translation>Lesen</translation>
    </message>
    <message>
        <location filename="accuserrecordgrouptree.cpp" line="450"/>
        <source>Modify</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="accuserrecordgrouptree.cpp" line="451"/>
        <source>Full</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="accuserrecordgrouptree.cpp" line="452"/>
        <source>Subgroups Inherit Access Rights</source>
        <translation>Untergruppen erben Rechte</translation>
    </message>
</context>
<context>
    <name>AccUserRecordSelector</name>
    <message>
        <location filename="accuserrecordselector.cpp" line="19"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location filename="accuserrecordselector.cpp" line="20"/>
        <source>Others can Read</source>
        <translation>Andere dürfen lesen</translation>
    </message>
    <message>
        <location filename="accuserrecordselector.cpp" line="21"/>
        <source>Others can Modify</source>
        <translation>Andere dürfen bearbeiten</translation>
    </message>
    <message>
        <location filename="accuserrecordselector.cpp" line="22"/>
        <source>Individual Rights</source>
        <translation>Rechte individuell</translation>
    </message>
    <message>
        <location filename="accuserrecordselector.cpp" line="29"/>
        <source>Set Individual Access Rights</source>
        <translation>Individuelle Rechte definieren</translation>
    </message>
</context>
<context>
    <name>AccUserRecordSelectorClass</name>
    <message>
        <location filename="accuserrecordselector.ui" line="13"/>
        <source>AccUserRecordSelector</source>
        <translation>AccUserRecordSelector</translation>
    </message>
    <message>
        <location filename="accuserrecordselector.ui" line="30"/>
        <source>Rights:</source>
        <translation>Rechte:</translation>
    </message>
</context>
<context>
    <name>AccUserRecordUserTable</name>
    <message>
        <location filename="accuserrecordusertable.cpp" line="175"/>
        <source>Add User</source>
        <translation>Benutzer hinzufügen</translation>
    </message>
    <message>
        <location filename="accuserrecordusertable.cpp" line="179"/>
        <source>Remove Selected User(s)</source>
        <translation>Ausgewählte(n) Benutzer entfernen</translation>
    </message>
    <message>
        <location filename="accuserrecordusertable.cpp" line="183"/>
        <source>Assign MySelf</source>
        <translation>Mich hinzufügen</translation>
    </message>
    <message>
        <location filename="accuserrecordusertable.cpp" line="187"/>
        <source>Remove Group Dependent Flag From Selected User(s)</source>
        <translation>Gruppen-Abhängigkeit aus ausgewählten Benutzern entfernen</translation>
    </message>
    <message>
        <location filename="accuserrecordusertable.cpp" line="191"/>
        <source>Assign Access Rights To All Selected User(s)</source>
        <translation>Zugriffsrechte allen ausgewählten Benutzern zuweisen</translation>
    </message>
</context>
<context>
    <name>ApplicationStarter</name>
    <message>
        <source>Select KeyFile</source>
        <translation type="obsolete">Keyfile wählen</translation>
    </message>
    <message>
        <source>Error Message</source>
        <translation type="obsolete">Fehlermeldung</translation>
    </message>
    <message>
        <source>INI file is missing,corrupted or has invalid values!</source>
        <translation type="obsolete">INI-Datei fehlt oder ist beschädigt!</translation>
    </message>
    <message>
        <source>License file is missing or corrupted!</source>
        <translation type="obsolete">Linzenzschlüsseldatei (Keyfile) fehlt oder ist beschädigt!</translation>
    </message>
    <message>
        <source>Settings directory:</source>
        <translation type="obsolete">Einstellungsverzeichnis:</translation>
    </message>
    <message>
        <source> could not be created!</source>
        <translation type="obsolete"> kann nicht erstellt werden!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusContact</name>
    <message>
        <source>Contact data</source>
        <translation type="obsolete">Kontaktdaten</translation>
    </message>
    <message>
        <source>Phones</source>
        <translation type="obsolete">Telefonnummern</translation>
    </message>
    <message>
        <source>Addresses</source>
        <translation type="obsolete">Adressen</translation>
    </message>
    <message>
        <source>Emails</source>
        <translation type="obsolete">E-Mails</translation>
    </message>
    <message>
        <source>Internet addresses</source>
        <translation type="obsolete">Internet-Adressen</translation>
    </message>
    <message>
        <source>Pictures</source>
        <translation type="obsolete">Bilder</translation>
    </message>
    <message>
        <source>Creditor data</source>
        <translation type="obsolete">Kreditoren-Daten</translation>
    </message>
    <message>
        <source>Debtor data</source>
        <translation type="obsolete">Debitoren-Daten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusCostCenter</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusCostCenterMain</name>
    <message>
        <source>Code is mandatory field!</source>
        <translation type="obsolete">Code zwingend!</translation>
    </message>
    <message>
        <source>Name is mandatory field!</source>
        <translation type="obsolete">Name zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusDepartment</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusDepartmentMain</name>
    <message>
        <source>Code is mandatory field!</source>
        <translation type="obsolete">Code zwingend!</translation>
    </message>
    <message>
        <source>Name is mandatory field!</source>
        <translation type="obsolete">Name zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusEmail</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusOrganization</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusOrganizationMain</name>
    <message>
        <source>Code is mandatory field!</source>
        <translation type="obsolete">Code zwingend!</translation>
    </message>
    <message>
        <source>Name is mandatory field!</source>
        <translation type="obsolete">Name zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusPerson</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
    <message>
        <source>User data</source>
        <translation type="obsolete">Benutzerdaten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusPersonMain</name>
    <message>
        <source>Last Name is mandatory field!</source>
        <translation type="obsolete">Nachname zwingend!</translation>
    </message>
    <message>
        <source>First Name is mandatory field!</source>
        <translation type="obsolete">Vorname zwingend!</translation>
    </message>
    <message>
        <source>Pers. No. is mandatory field!</source>
        <translation type="obsolete">Personalnummer zwingend!</translation>
    </message>
    <message>
        <source>Initials is mandatory field!</source>
        <translation type="obsolete">Initialen zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusProject</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_BusProjectMain</name>
    <message>
        <source>Code is mandatory field!</source>
        <translation type="obsolete">Code zwingend!</translation>
    </message>
    <message>
        <source>Name is mandatory field!</source>
        <translation type="obsolete">Name zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CM_Address</name>
    <message>
        <source>Address is mandatory field!</source>
        <translation type="obsolete">Adresse zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CM_AddressSet</name>
    <message>
        <source>Address</source>
        <translation type="obsolete">Adresse</translation>
    </message>
    <message>
        <source>Address Types</source>
        <translation type="obsolete">Adressarten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CM_ContactMain</name>
    <message>
        <source>Last Name is mandatory field!</source>
        <translation type="obsolete">Nachname zwingend!</translation>
    </message>
    <message>
        <source>Organization Name is mandatory field!</source>
        <translation type="obsolete">Organisationsname zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CM_Email</name>
    <message>
        <source>Email address is mandatory field!</source>
        <translation type="obsolete">E-Mail-Adresse zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CM_Internet</name>
    <message>
        <source>Internet address is mandatory field!</source>
        <translation type="obsolete">Internet-Adresse zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CM_Journal</name>
    <message>
        <source>Person is mandatory field inside journal entry!</source>
        <translation type="obsolete">Benutzerangabe zwingend innerhalb Journaleintrag!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CM_Phone</name>
    <message>
        <source>Phone number is mandatory field!</source>
        <translation type="obsolete">Telefonnummer zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CeEventTypes</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CeEventTypesMain</name>
    <message>
        <source>Code is mandatory field!</source>
        <translation type="obsolete">Code zwingend!</translation>
    </message>
    <message>
        <source>Name is mandatory field!</source>
        <translation type="obsolete">Name zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CeTypes</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CeTypesMain</name>
    <message>
        <source>Code is mandatory field!</source>
        <translation type="obsolete">Code zwingend!</translation>
    </message>
    <message>
        <source>Name is mandatory field!</source>
        <translation type="obsolete">Name zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_ContactType</name>
    <message>
        <source>Only one user defined type can have the selected system type!</source>
        <translation type="obsolete">Der Systemtyp darf nur einem einzigen benutzerdefinierten Typ zugewiesen werden!</translation>
    </message>
    <message>
        <source>Type Name is mandatory field!</source>
        <translation type="obsolete">Typenname zwingend!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CoreUser</name>
    <message>
        <source>Last Name is mandatory field!</source>
        <translation type="obsolete">Nachname zwingend!</translation>
    </message>
    <message>
        <source>First Name is mandatory field!</source>
        <translation type="obsolete">Vorname zwingend!</translation>
    </message>
    <message>
        <source>Username is mandatory field!</source>
        <translation type="obsolete">Benutzername zwingend!</translation>
    </message>
    <message>
        <source>Password must be set!</source>
        <translation type="obsolete">Passwort muss gesetzt sein!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_CoreUserRecord</name>
    <message>
        <source>Last Name is mandatory field!</source>
        <translation type="obsolete">Nachname zwingend!</translation>
    </message>
    <message>
        <source>First Name is mandatory field!</source>
        <translation type="obsolete">Vorname zwingend!</translation>
    </message>
    <message>
        <source>Username is mandatory field!</source>
        <translation type="obsolete">Benutzername zwingend!</translation>
    </message>
    <message>
        <source>Password must be set!</source>
        <translation type="obsolete">Passwort muss gesetzt sein!</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_ListRedefinator</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
</context>
<context>
    <name>BoClientEntity_VoiceCall</name>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
</context>
<context>
    <name>BusinessServiceManager_ThickClient</name>
    <message>
        <source>License file is missing or corrupted!</source>
        <translation type="obsolete">Linzenzschlüsseldatei (Keyfile) fehlt oder ist beschädigt!</translation>
    </message>
    <message>
        <source>Select KeyFile</source>
        <translation type="obsolete">Keyfile wählen</translation>
    </message>
</context>
<context>
    <name>CalendarReservationTable_WidgetRow_Cell_0</name>
    <message>
        <location filename="calendarreservationwidget.cpp" line="553"/>
        <source>Impossible</source>
        <translation>Nicht verfügbar</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="554"/>
        <source>Undesired</source>
        <translation>Nur in dringenden Fällen verfügbar</translation>
    </message>
</context>
<context>
    <name>CalendarReservationTable_WidgetRow_Cell_1</name>
    <message>
        <location filename="calendarreservationwidget.cpp" line="576"/>
        <source>Mo</source>
        <translation>Mo</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="577"/>
        <source>Tu</source>
        <translation>Di</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="578"/>
        <source>We</source>
        <translation>Mi</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="579"/>
        <source>Th</source>
        <translation>Do</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="580"/>
        <source>Fr</source>
        <translation>Fr</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="581"/>
        <source>Sa</source>
        <translation>Sa</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="582"/>
        <source>Su</source>
        <translation>So</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="595"/>
        <source>Period: </source>
        <translation>Zeitraum:</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="596"/>
        <source>From </source>
        <translation>Von</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="597"/>
        <source>To </source>
        <translation>Bis</translation>
    </message>
</context>
<context>
    <name>CalendarReservationTable_WidgetRow_Cell_2</name>
    <message>
        <location filename="calendarreservationwidget.cpp" line="676"/>
        <source>From </source>
        <translation>Von</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="685"/>
        <source>To </source>
        <translation>Bis</translation>
    </message>
</context>
<context>
    <name>CalendarReservationTable_WidgetRow_Cell_Before_0</name>
    <message>
        <location filename="calendarreservationwidget.cpp" line="513"/>
        <source>Remove Reservation</source>
        <translation>Reservierung entfernen</translation>
    </message>
</context>
<context>
    <name>CalendarReservationWidget</name>
    <message>
        <location filename="calendarreservationwidget.cpp" line="37"/>
        <source>Add New Calendar Reservation</source>
        <translation>Neue Reservierung hinzufügen</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="77"/>
        <source>Reservation times is not set for row: %1 !</source>
        <translation>Reservierungszeit für Zeile %1 nicht gesetzt!</translation>
    </message>
    <message>
        <location filename="calendarreservationwidget.cpp" line="83"/>
        <source>Reservation times are invalid for row: %1 !</source>
        <translation>Reservierungszeit ungültig für Zeile %1!</translation>
    </message>
</context>
<context>
    <name>ClientBackupManager</name>
    <message>
        <location filename="clientbackupmanager.cpp" line="34"/>
        <location filename="clientbackupmanager.cpp" line="72"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <source>A backup was scheduled for </source>
        <translation type="obsolete">Ein Backup war geplant für </translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="34"/>
        <location filename="clientbackupmanager.cpp" line="72"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="34"/>
        <location filename="clientbackupmanager.cpp" line="72"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="34"/>
        <source>Create new backup. Proceed?</source>
        <translation>Neues Backup erstellen. Weiterfahren?</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="72"/>
        <source>Restore Database from: </source>
        <translation>Datenbank wiederherstellen aus: </translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="72"/>
        <source>. This will destroy current database data. Proceed?</source>
        <translation>. Bei diesem Vorgang wird die aktuelle Datenbank überschrieben. Weiterfahren?</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="56"/>
        <location filename="clientbackupmanager.cpp" line="83"/>
        <location filename="clientbackupmanager.cpp" line="88"/>
        <location filename="clientbackupmanager.cpp" line="158"/>
        <location filename="clientbackupmanager.cpp" line="184"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>New backup successfully created!</source>
        <translation type="obsolete">Neues Backup erfolgreich erstellt!</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="83"/>
        <source>Please, restart application to start restore operation!</source>
        <translation>Bitte starten Sie die Anwendung erneut um das Datenbackup einzulesen!</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="88"/>
        <source>Please, save all work and close application, server will restart and restore from backup in few minutes.</source>
        <translation>Bitte alles speichern und Anwendung schliessen, der Server wird in wenigen Minuten neu gestartet mit einer Backup-Datenwiederherstellung.</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="172"/>
        <source>A scheduled backup will start now. It will take few minutes. Proceed? (You can change backup schedule settings in Menu/Backup &amp; Restore.)

</source>
        <translation>Ein geplantes Backup wird jetzt starten und einige Minuten dauern. Weiterfahren? (Die Backup-Einstellungen können geändert werden unter Menu/ Backup &amp; Restore.)
</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="173"/>
        <source>Backup Scheduler</source>
        <translation>Backup-Planer</translation>
    </message>
    <message>
        <location filename="clientbackupmanager.cpp" line="158"/>
        <source>Application will close now. Server will restart. Restart application in few minutes to start using new database.</source>
        <translation>Die Anwendung wird nun beendet, und der Server wird neu gestartet. Bitte starten Sie die Anwendung wieder in ein paar Minuten, um die neue Datenbank zu verwenden.</translation>
    </message>
</context>
<context>
    <name>ClientContactManager</name>
    <message>
        <source>Last Name</source>
        <translation type="obsolete">Nachname</translation>
    </message>
</context>
<context>
    <name>ClientDocumentHandler</name>
    <message>
        <location filename="clientdocumenthandler.cpp" line="55"/>
        <location filename="clientdocumenthandler.cpp" line="132"/>
        <source>Not available in the thick client</source>
        <translation>	Im Modul Thick Client nicht verfügbar</translation>
    </message>
</context>
<context>
    <name>ClientDownloadManager</name>
    <message>
        <location filename="clientdownloadmanager.cpp" line="731"/>
        <source>Upload of %1 complete</source>
        <translation>Upload von %1 vollständig</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="732"/>
        <source>Upload complete</source>
        <translation>Upload vollständig</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="735"/>
        <source>Download of %1 complete</source>
        <translation>Download von %1 vollständig</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="736"/>
        <source>Download complete</source>
        <translation>Download vollständig</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="746"/>
        <source>Uploading of %1 failed: %2</source>
        <translation>Upload von %1 abgebrochen: %2</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="747"/>
        <source>Error Uploading File</source>
        <translation>Fehler beim Upload der Datei</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="750"/>
        <source>Downloading of %1 failed: %2</source>
        <translation>Download von %1 abgebrochen: %2</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="751"/>
        <source>Error Downloading File</source>
        <translation>Fehler beim Download der Datei</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="506"/>
        <source>Show Current Download/Uploads Manager</source>
        <translation>Aktuellen Download/Upload-Manager zeigen</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="509"/>
        <source>Hide Current Download/Uploads</source>
        <translation>Aktuelle Downloads/Uploads ausblenden</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="590"/>
        <location filename="clientdownloadmanager.cpp" line="652"/>
        <source>Failed to write document %1</source>
        <translation>Fehler beim Schreiben des Dokuments %1</translation>
    </message>
    <message>
        <location filename="clientdownloadmanager.cpp" line="666"/>
        <source>Failed to download document %1. CRC checksum failed.</source>
        <translation>Fehler beim Download des Dokuments %1. CRC-Prüfsumme fehlerhaft.</translation>
    </message>
</context>
<context>
    <name>ClientImportExportManager</name>
    <message>
        <location filename="clientimportexportmanager.cpp" line="114"/>
        <location filename="clientimportexportmanager.cpp" line="164"/>
        <location filename="clientimportexportmanager.cpp" line="252"/>
        <location filename="clientimportexportmanager.cpp" line="321"/>
        <source>Export file is not saved as there is no data to save</source>
        <translation>Exportdatei nicht gespeichert, da keine Daten vorhanden sind</translation>
    </message>
    <message>
        <location filename="clientimportexportmanager.cpp" line="122"/>
        <location filename="clientimportexportmanager.cpp" line="129"/>
        <location filename="clientimportexportmanager.cpp" line="172"/>
        <location filename="clientimportexportmanager.cpp" line="179"/>
        <location filename="clientimportexportmanager.cpp" line="260"/>
        <location filename="clientimportexportmanager.cpp" line="267"/>
        <location filename="clientimportexportmanager.cpp" line="329"/>
        <location filename="clientimportexportmanager.cpp" line="336"/>
        <source>The File </source>
        <translation>Die Datei </translation>
    </message>
    <message>
        <location filename="clientimportexportmanager.cpp" line="122"/>
        <location filename="clientimportexportmanager.cpp" line="129"/>
        <location filename="clientimportexportmanager.cpp" line="172"/>
        <location filename="clientimportexportmanager.cpp" line="179"/>
        <location filename="clientimportexportmanager.cpp" line="260"/>
        <location filename="clientimportexportmanager.cpp" line="267"/>
        <location filename="clientimportexportmanager.cpp" line="329"/>
        <location filename="clientimportexportmanager.cpp" line="336"/>
        <source> can not be written!</source>
        <translation> kann nicht geschrieben werden!</translation>
    </message>
    <message>
        <location filename="clientimportexportmanager.cpp" line="420"/>
        <source>File can not be created, please specify full file path, including filename!</source>
        <translation>Die Datei kann nicht erstellt werden, bitte spezifizieren Sie den vollen Pfad inklusive Dateinamen!</translation>
    </message>
    <message>
        <location filename="clientimportexportmanager.cpp" line="69"/>
        <location filename="clientimportexportmanager.cpp" line="70"/>
        <source>Import/Export Manager process failed: </source>
        <translation>Vorgang in Import/Export-Manager nicht erfolgreich: </translation>
    </message>
    <message>
        <location filename="clientimportexportmanager.cpp" line="70"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="clientimportexportmanager.cpp" line="79"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="clientimportexportmanager.cpp" line="384"/>
        <source>A scheduled import/export will start now. It will take few minutes. Proceed? (You can change schedule settings in File/Admin Tools)

</source>
        <translation>Ein geplanter Import/Export-Vorgang wird nun starten und einige Minuten dauern. Weiterfahren? (Einstellungen des Plans können geändert werden unter (Datei/Administrationswerkzeuge)</translation>
    </message>
    <message>
        <location filename="clientimportexportmanager.cpp" line="385"/>
        <source>Import/export Scheduler</source>
        <translation>Import/Export-Planer</translation>
    </message>
</context>
<context>
    <name>ClientManager</name>
    <message>
        <source>Your version %1 is not actual any more. Do you want to download and install an update to version %2 now?</source>
        <translation type="obsolete">Version %1 ist nicht mehr aktuell. Wollen Sie jetzt die neue Version %2 herunterladen und installieren?</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <source>Database version do not match, notify administrator!</source>
        <translation type="obsolete">Datenbankversionen stimmen nicht überein, Administrator benachrichtigen!</translation>
    </message>
    <message>
        <source>Your version %1 is not actual any more. Do you want to download and install an update now?</source>
        <translation type="obsolete">Die installierte Version %1 ist nicht mehr aktuell. Wollen Sie jetzt ein Update herunterladen und installieren?</translation>
    </message>
    <message>
        <source>You have </source>
        <translation type="obsolete">Sie haben noch </translation>
    </message>
    <message>
        <source> days remaining before the update subscription expires. Please renew it to be able to download updates in the future!</source>
        <translation type="obsolete"> Tage bevor das Lizenz-Update-Abonnement ausläuft. Bitte erneuern Sie es, um updateberechtigt zu bleiben!</translation>
    </message>
    <message>
        <source>Subscription period expired!</source>
        <translation type="obsolete"> Lizenz-Update-Abonnement abgelaufen!</translation>
    </message>
    <message>
        <source>  Downgrade the un-licensed updated version to the previous, licensed one   </source>
        <translation type="obsolete">  nicht lizenziertes Update auf vorhergegende, lizenzierte Version zurücksetzen  </translation>
    </message>
    <message>
        <source>  Renew license </source>
        <translation type="obsolete">   Lizenz-Update-Abonnement verlängern </translation>
    </message>
    <message>
        <source> Cancel </source>
        <translation type="obsolete"> Abbrechen </translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Could not fetch old version. Please try manually!</source>
        <translation type="obsolete">Alte Version kann nicht heruntergeladen werden. Bitte manuell installieren!</translation>
    </message>
    <message>
        <source>You can not use the new version because the update subscription period has expired. Order/renew your license to use this application!</source>
        <translation type="obsolete">Die neue Version kann nicht verwendet werden, da das Update-Abonnement abgelaufen ist. Bitte bestellen bzw. verlängern Sie Ihre Lizenz, um das Programm zu verwenden!</translation>
    </message>
    <message>
        <source>  Renew update subscription </source>
        <translation type="obsolete">  Update-Abonnement erneuern </translation>
    </message>
    <message>
        <source>New message</source>
        <translation type="obsolete">Neue Meldung</translation>
    </message>
    <message>
        <source>You try to login to a server with version %1 with a newer client with version %2! Access denied.</source>
        <translation type="obsolete">Sie versuchen, mit einem neuen Client mit derVersion %1 auf einen älteren Server mit der Version %2 zuzugreifen. Zugriff verweigert.</translation>
    </message>
    <message>
        <source>Your version %1 is newer than server version. Do you want to download and install a downgrade to the server version now?</source>
        <translation type="obsolete">Ihre Version %1 ist neuer als die auf dem Server installierte. Möchten Sie die dem Server entsprechende Version herunterladen und installieren?</translation>
    </message>
    <message>
        <source>Your version %1 is not actual any more. Do you want to download and install an update to the version used by the server?</source>
        <translation type="obsolete">Die Version %1 ist nicht mehr aktuell. Wollen Sie jetzt ein Update auf die vom Server eingesetzte Version herunterladen und installieren?</translation>
    </message>
    <message>
        <source>Settings directory:</source>
        <translation type="obsolete">Einstellungsverzeichnis:</translation>
    </message>
    <message>
        <source> could not be created!</source>
        <translation type="obsolete"> kann nicht erstellt werden!</translation>
    </message>
    <message>
        <source>Select KeyFile</source>
        <translation type="obsolete">Keyfile wählen</translation>
    </message>
</context>
<context>
    <name>ClientManagerExt</name>
    <message>
        <location filename="clientmanagerext.cpp" line="122"/>
        <source>INI file is missing,corrupted or has invalid values!</source>
        <translation>INI-Datei fehlt oder ist beschädigt!</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="229"/>
        <location filename="clientmanagerext.cpp" line="253"/>
        <source>Settings directory:</source>
        <translation>Einstellungsverzeichnis:</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="229"/>
        <location filename="clientmanagerext.cpp" line="253"/>
        <source> could not be created!</source>
        <translation> kann nicht erstellt werden!</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="339"/>
        <location filename="clientmanagerext.cpp" line="424"/>
        <location filename="clientmanagerext.cpp" line="453"/>
        <source>Error Message</source>
        <translation>Fehlermeldung</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="339"/>
        <location filename="clientmanagerext.cpp" line="453"/>
        <source>You have reached maximum number of login attempts!
The application will be closed now.</source>
        <translation>Maximale Anzahl Login-Versuche erreicht!
Die Anwendung wird geschlossen.</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="443"/>
        <source>Login in Progress...</source>
        <translation>Login wird durchgeführt...</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="377"/>
        <location filename="clientmanagerext.cpp" line="511"/>
        <location filename="clientmanagerext.cpp" line="519"/>
        <location filename="clientmanagerext.cpp" line="525"/>
        <location filename="clientmanagerext.cpp" line="1137"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="602"/>
        <source>Database version do not match, notify administrator!</source>
        <translation>Datenbankversionen stimmen nicht überein, Administrator benachrichtigen!</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="603"/>
        <location filename="clientmanagerext.cpp" line="828"/>
        <location filename="clientmanagerext.cpp" line="880"/>
        <location filename="clientmanagerext.cpp" line="1040"/>
        <location filename="clientmanagerext.cpp" line="1115"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="858"/>
        <location filename="clientmanagerext.cpp" line="1037"/>
        <source>Your version %1 is not actual any more. Do you want to download and install an update to version %2 now?</source>
        <translation>Version %1 ist nicht mehr aktuell. Wollen Sie jetzt die neue Version %2 herunterladen und installieren?</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="866"/>
        <source>Your version %1 is newer than server version. Do you want to download and install a downgrade to the server version now?</source>
        <translation>Ihre Version %1 ist neuer als die auf dem Server installierte. Möchten Sie die dem Server entsprechende Version herunterladen und installieren?</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="825"/>
        <location filename="clientmanagerext.cpp" line="872"/>
        <source>Your version %1 is not actual any more. Do you want to download and install an update to the version used by the server?</source>
        <translation>Die Version %1 ist nicht mehr aktuell. Wollen Sie jetzt ein Update auf die vom Server eingesetzte Version herunterladen und installieren?</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="828"/>
        <location filename="clientmanagerext.cpp" line="880"/>
        <location filename="clientmanagerext.cpp" line="1040"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="828"/>
        <location filename="clientmanagerext.cpp" line="880"/>
        <location filename="clientmanagerext.cpp" line="1040"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1114"/>
        <source>You have </source>
        <translation>Sie haben noch </translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1114"/>
        <source> days remaining before the update subscription expires. Please renew it to be able to download updates in the future!</source>
        <translation> Tage bevor das Lizenz-Update-Abonnement ausläuft. Bitte erneuern Sie es, um updateberechtigt zu bleiben!</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1124"/>
        <source>Subscription period expired!</source>
        <translation> Lizenz-Update-Abonnement abgelaufen!</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1124"/>
        <source>You can not use the new version because the update subscription period has expired. Order/renew your license to use this application!</source>
        <translation>Die neue Version kann nicht verwendet werden, da das Update-Abonnement abgelaufen ist. Bitte bestellen bzw. verlängern Sie Ihre Lizenz, um das Programm zu verwenden!</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1124"/>
        <source>  Renew update subscription </source>
        <translation>  Update-Abonnement erneuern </translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1124"/>
        <source>  Downgrade the un-licensed updated version to the previous, licensed one   </source>
        <translation>  nicht lizenziertes Update auf vorhergegende, lizenzierte Version zurücksetzen  </translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1124"/>
        <source> Cancel </source>
        <translation> Abbrechen </translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1137"/>
        <source>Could not fetch old version. Please try manually!</source>
        <translation>Alte Version kann nicht heruntergeladen werden. Bitte manuell installieren!</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1186"/>
        <location filename="clientmanagerext.cpp" line="1192"/>
        <location filename="clientmanagerext.cpp" line="1197"/>
        <source>New message</source>
        <translation>Neue Meldung</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1320"/>
        <source>Access not allowed from thick client!</source>
        <translation>Kein Zugriff aus Personal Edition!</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1323"/>
        <location filename="clientmanagerext.cpp" line="1332"/>
        <source>Client successfully logged to: </source>
        <translation>Client erfolgreich verbunden mit: </translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="1329"/>
        <source>Access not allowed from thin client!</source>
        <translation>Zugriff aus Internet-Client nicht zugelassen!</translation>
    </message>
    <message>
        <source>New message Task: </source>
        <translation type="obsolete">New message Task: </translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="107"/>
        <source>Operation In Progress</source>
        <translation>Aufgabe  wird durchgeführt</translation>
    </message>
    <message>
        <location filename="clientmanagerext.cpp" line="111"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Failed to create thick client</source>
        <translation type="obsolete">&quot;Thick Client&quot; konnte nicht installiert werden</translation>
    </message>
</context>
<context>
    <name>ClientReminderManager</name>
    <message>
        <location filename="clientremindermanager.cpp" line="90"/>
        <source> days </source>
        <translation>Tage</translation>
    </message>
    <message>
        <location filename="clientremindermanager.cpp" line="92"/>
        <location filename="clientremindermanager.cpp" line="94"/>
        <source> hours </source>
        <translation>Stznden</translation>
    </message>
    <message>
        <location filename="clientremindermanager.cpp" line="97"/>
        <source>Overdue for</source>
        <translation>Überfällig seit</translation>
    </message>
    <message>
        <location filename="clientremindermanager.cpp" line="99"/>
        <source>Due in</source>
        <translation>Fällig in</translation>
    </message>
</context>
<context>
    <name>CustomCollectionsWnd</name>
    <message>
        <location filename="customcollectionswnd.ui" line="17"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="35"/>
        <source>New Column</source>
        <translation>Neue Spalte</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="40"/>
        <source>E.Enterprise Contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="44"/>
        <source>E.C. Customers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="49"/>
        <source>E.E. Employees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="55"/>
        <source>E.M. Marketing &amp; Sales</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="59"/>
        <source>E.M.O. Offers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="64"/>
        <source>E.M.P. Prospects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="70"/>
        <source>E.P. Partners</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="75"/>
        <source>E.S.Suppliers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="85"/>
        <source>Operation:</source>
        <translation>Operation:</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="94"/>
        <location filename="customcollectionswnd.ui" line="130"/>
        <location filename="customcollectionswnd.ui" line="217"/>
        <location filename="customcollectionswnd.ui" line="242"/>
        <location filename="customcollectionswnd.ui" line="267"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="101"/>
        <location filename="customcollectionswnd.ui" line="292"/>
        <location filename="customcollectionswnd.ui" line="322"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="108"/>
        <source>Intersect</source>
        <translation>Schnittmenge</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="172"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="177"/>
        <source>New Row</source>
        <translation>Neue Zeile</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="182"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="187"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="192"/>
        <source>Operation</source>
        <translation>Mengenoperation</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="197"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="202"/>
        <source>Tree Name</source>
        <translation>Baumname</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="207"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="212"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="222"/>
        <location filename="customcollectionswnd.ui" line="247"/>
        <location filename="customcollectionswnd.ui" line="272"/>
        <location filename="customcollectionswnd.ui" line="297"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="227"/>
        <location filename="customcollectionswnd.ui" line="252"/>
        <location filename="customcollectionswnd.ui" line="277"/>
        <location filename="customcollectionswnd.ui" line="302"/>
        <source>Firmenkontakte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="232"/>
        <source>F.L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="237"/>
        <source>Lieferanten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="257"/>
        <source>F.K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="262"/>
        <source>Kunden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="282"/>
        <source>F.V.P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="287"/>
        <source>Pressekontakte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="307"/>
        <source>F.K.NM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="312"/>
        <source>Bitte keine Email-Mailings senden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="329"/>
        <source>Move Up</source>
        <translation>Nach oben</translation>
    </message>
    <message>
        <location filename="customcollectionswnd.ui" line="336"/>
        <source>Move Down</source>
        <translation>Nach unten</translation>
    </message>
</context>
<context>
    <name>CustomSelectionsWnd</name>
    <message>
        <location filename="customselectionswnd.cpp" line="34"/>
        <source>Logic</source>
        <translation>Verknüpfung</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="35"/>
        <source>Field</source>
        <translation>Feld</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="36"/>
        <source>Operation</source>
        <translation>Operation</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="37"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="104"/>
        <source>Project Code</source>
        <translation>Projektcode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="105"/>
        <source>Department Code</source>
        <translation>Abteilungscode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="106"/>
        <source>Organization Code</source>
        <translation>Organisationscode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="107"/>
        <source>Area Code</source>
        <translation>Bereichscode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="108"/>
        <source>Sector Code</source>
        <translation>Gebietscode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="109"/>
        <source>Industry Group Code</source>
        <translation>Branchencode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="110"/>
        <source>Project Type</source>
        <translation>Projektartencode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="111"/>
        <source>Service Type</source>
        <translation>Leistungsartencode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="112"/>
        <source>Project Leader</source>
        <translation>Projektleiter</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="113"/>
        <source>Responsible</source>
        <translation>Verantwortlicher</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="114"/>
        <source>Status</source>
        <translation>Statuscode</translation>
    </message>
    <message>
        <location filename="customselectionswnd.cpp" line="115"/>
        <source>Rate Category</source>
        <translation>Tarifart</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="26"/>
        <source>Sokrates - selections</source>
        <translation>SOKRATES - Auswahl</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="55"/>
        <source>Logic:</source>
        <translation>Logische Verknüpfung:</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="62"/>
        <source>AND</source>
        <translation>UND</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="69"/>
        <source>OR</source>
        <translation>ODER</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="76"/>
        <source>(</source>
        <translation>(</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="83"/>
        <source>)</source>
        <translation>)</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="90"/>
        <source>NOT AND(</source>
        <translation>NICHT UND(</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="97"/>
        <source>OR(</source>
        <translation>ODER(</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="104"/>
        <source>NOT(</source>
        <translation>NICHT(</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="124"/>
        <source>Set</source>
        <translation>Übernehmen</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="141"/>
        <source>Field:</source>
        <translation>Feld:</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="177"/>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="182"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="187"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="192"/>
        <source>&gt;=</source>
        <translation>&gt;=</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="197"/>
        <source>&lt;=</source>
        <translation>&lt;=</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="202"/>
        <source>&lt;&gt;</source>
        <translation>&lt;&gt;</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="207"/>
        <location filename="customselectionswnd.cpp" line="402"/>
        <source>LIKE</source>
        <translation>WIE</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="212"/>
        <location filename="customselectionswnd.cpp" line="400"/>
        <source>BEGINS WITH</source>
        <translation>BEGINNT MIT</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="253"/>
        <source>Select:</source>
        <translation>Auswahl:</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="291"/>
        <source>Move Up</source>
        <translation>Nach oben</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="298"/>
        <source>Move Down</source>
        <translation>Nach unten</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="305"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="customselectionswnd.ui" line="312"/>
        <source>Clear All</source>
        <translation>Alles leeren</translation>
    </message>
</context>
<context>
    <name>Dlg_AccUserRecord</name>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="52"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="53"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="54"/>
        <source>Read</source>
        <translation>Lesen</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="55"/>
        <source>Modify</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="56"/>
        <source>Full</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="57"/>
        <source>Group Dep.</source>
        <translation>Gruppen-Abh.</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="58"/>
        <source>Tree Name</source>
        <translation>Baumname</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="59"/>
        <source>Group Code</source>
        <translation>Gruppencode</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="60"/>
        <source>Group Name</source>
        <translation>Gruppenname</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="139"/>
        <source>Others</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="297"/>
        <location filename="dlg_accuserrecord.cpp" line="434"/>
        <location filename="dlg_accuserrecord.cpp" line="618"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="618"/>
        <source>At least one person must have full access right!</source>
        <translation>Mindestens eine Person muss volle Zugriffsrechte erhalten!</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="297"/>
        <source>Any further change of the parent record access rights will not be reflected on this record. Proceed?</source>
        <translation>Weitere Änderungen an den Zugriffsrechten des Elterndatensatzes werden im aktuellen Datensatz nicht nachgetragen. Weiterfahren?</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="297"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="297"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="434"/>
        <source>Can not assign, you are already assigned!</source>
        <translation>Nicht zuweisbar, da schon zugewiesen!</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="87"/>
        <source>Add Selected Users</source>
        <translation>Ausgewählte Benutzer hinzufügen</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="88"/>
        <source>Remove Selected Users</source>
        <translation>Ausgewählte Benutzer entfernen</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="89"/>
        <source>Add Selected Groups</source>
        <translation>Ausgewählte Gruppen hinzufügen</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="90"/>
        <source>Remove Selected Groups</source>
        <translation>Ausgewählte Gruppen entfernen</translation>
    </message>
</context>
<context>
    <name>Dlg_AccUserRecordClass</name>
    <message>
        <location filename="dlg_accuserrecord.ui" line="13"/>
        <source>Access Rights</source>
        <translation>Zugriffsrechte</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="58"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;P.00.01. Test Project&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;P.00.01. Testprojekt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="74"/>
        <source>Subelements (subprojects) Inherit Access Rights</source>
        <translation>Unterelemente (Unterprojekte) erben Zugriffsrechte</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="81"/>
        <source>Assigned Communication Elements (Documents, Emails, etc.) Inherit Access Rights</source>
        <translation>Zugewiesene Dokumente, E-Mails usw. erben Zugriffsrechte</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="88"/>
        <source>Access Rights are Inherited From:</source>
        <translation>Zugriffsrechte werden geerbt von:</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="101"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;P.00. Test Project Parent&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; font-style:italic;&quot;&gt;P.00. Test Elternprojekt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="166"/>
        <source>Group Access Rights</source>
        <translation>Gruppen-Zugriffsrechte</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="217"/>
        <location filename="dlg_accuserrecord.ui" line="462"/>
        <source>Add User</source>
        <translation>Benutzer hinzufügen</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="264"/>
        <location filename="dlg_accuserrecord.ui" line="509"/>
        <source>Remove User</source>
        <translation>Benutzer entfernen</translation>
    </message>
    <message>
        <source>x</source>
        <translation type="obsolete">x</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="340"/>
        <location filename="dlg_accuserrecord.ui" line="593"/>
        <source>Read</source>
        <translation>Lesen</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="347"/>
        <location filename="dlg_accuserrecord.ui" line="600"/>
        <source>Modify</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="354"/>
        <location filename="dlg_accuserrecord.ui" line="607"/>
        <source>Full</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="361"/>
        <location filename="dlg_accuserrecord.ui" line="614"/>
        <source>Assign To Selected</source>
        <translation>Ausgewählten zuweisen</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="411"/>
        <source>User Access Rights</source>
        <translation>Benutzer-Zugriffsrechte</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="542"/>
        <source>Assign MySelf</source>
        <translation>Mich hinzufügen</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="549"/>
        <source>Declare User(s) As Non Group Dependent</source>
        <translation>Gruppenabhängigkeit von Benutzer(n) entfernen</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="655"/>
        <source>Copy Access Rights to Other Element(s)</source>
        <translation>Zugriffsrechte weiterkopieren</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="662"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.ui" line="669"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactBuildAll</name>
    <message>
        <location filename="dlg_contactbuildall.cpp" line="18"/>
        <source>Build Actual Contact List</source>
        <translation>Aktuelle Kontaktliste aufbauen</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactBuildAllClass</name>
    <message>
        <location filename="dlg_contactbuildall.ui" line="14"/>
        <source>Build All Contacts</source>
        <translation>Alle Kontakte aufbauen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="147"/>
        <source>Build List</source>
        <translation>Liste aufbauen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="160"/>
        <source>Based on Search:</source>
        <translation>Suche:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="153"/>
        <source>Build All</source>
        <translation>Alle auflisten</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="199"/>
        <source>Based on Group:</source>
        <translation>Gruppe:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="244"/>
        <source>Based on Selection:</source>
        <translation>Auswahl:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="289"/>
        <source>Based on Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <source>Search 2:</source>
        <translation type="obsolete">Suche 2:</translation>
    </message>
    <message>
        <source>Search Field 2:</source>
        <translation type="obsolete">Suchfeld 2:</translation>
    </message>
    <message>
        <source>Search 1:</source>
        <translation type="obsolete">Suche 1:</translation>
    </message>
    <message>
        <source>Search Field 1:</source>
        <translation type="obsolete">Suchfeld 1:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="343"/>
        <source>Operation</source>
        <translation>Mengenoperation</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="355"/>
        <source>Add To Contact List</source>
        <translation>Zu aktueller Kontaktliste dazufügen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="362"/>
        <source>Remove From Contact List</source>
        <translation>Aus Kontaktliste entfernen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="369"/>
        <source>Replace Contact List</source>
        <translation>Kontaktliste ersetzen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="376"/>
        <source>Intersect With Contact List</source>
        <translation>Schnittmenge mit Kontaktliste bilden</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="386"/>
        <source>Do not use Exclusion Group</source>
        <translation>Ausschlussgruppe nicht verwenden</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="20"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="40"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="67"/>
        <source>Standard Search</source>
        <translation>Standardsuche</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="79"/>
        <source>New Search</source>
        <translation>Neue Suche</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="92"/>
        <source>Refine Last Search</source>
        <translation>Letzte Suche verfeinern</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall.ui" line="129"/>
        <source>Extended Search</source>
        <translation>Erweiterte Suche</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactBuildAll_Embedded</name>
    <message>
        <location filename="dlg_contactbuildall_embedded.cpp" line="15"/>
        <source>Build Actual Contact List</source>
        <translation>Aktuelle Kontaktliste aufbauen</translation>
    </message>
</context>
<context>
    <name>Dlg_ContactBuildAll_EmbeddedClass</name>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="25"/>
        <source>Dlg_ContactBuildAll_Embedded</source>
        <translation>Dlg_ContactBuildAll_Embedded</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="90"/>
        <source>Do not use Exclusion Group</source>
        <translation>Ausschlussgruppe nicht verwenden</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="97"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="104"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="111"/>
        <source>Operation:</source>
        <translation>Operation:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="126"/>
        <source>Add To Contact List</source>
        <translation>Zu aktueller Kontaktliste dazufügen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="133"/>
        <source>Remove From Contact List</source>
        <translation>Aus Kontaktliste entfernen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="140"/>
        <source>Replace Contact List</source>
        <translation>Kontaktliste ersetzen</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="147"/>
        <source>Intersect With Contact List</source>
        <translation>Schnittmenge mit Kontaktliste bilden</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="162"/>
        <source>Build List:</source>
        <translation>Liste aufbauen:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="177"/>
        <source>Build All</source>
        <translation>Alle auflisten</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="184"/>
        <source>Based on Search:</source>
        <translation>Suche:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="197"/>
        <source>Search Field 1:</source>
        <translation>Suchfeld 1:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="213"/>
        <source>Search Field 2:</source>
        <translation>Suchfeld 2:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="229"/>
        <source>Search 1:</source>
        <translation>Suche 1:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="245"/>
        <source>Search 2:</source>
        <translation>Suche 2:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="277"/>
        <source>Based on Selection:</source>
        <translation>Auswahl:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="306"/>
        <source>Based on Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location filename="dlg_contactbuildall_embedded.ui" line="335"/>
        <source>Based on Group:</source>
        <translation>Gruppe:</translation>
    </message>
</context>
<context>
    <name>Dlg_CopyEntity</name>
    <message>
        <location filename="dlg_copyentity.cpp" line="23"/>
        <source>Group Name</source>
        <translation>Gruppenname</translation>
    </message>
</context>
<context>
    <name>Dlg_CopyEntityClass</name>
    <message>
        <location filename="dlg_copyentity.ui" line="13"/>
        <source>Select Groups For Copy</source>
        <translation>Auswahl Gruppen zum Kopieren</translation>
    </message>
    <message>
        <location filename="dlg_copyentity.ui" line="104"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="dlg_copyentity.ui" line="123"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="dlg_copyentity.ui" line="178"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="dlg_copyentity.ui" line="185"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_DownloadManager</name>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="37"/>
        <source>Download/Upload Manager</source>
        <translation>Download/Upload-Manager</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="42"/>
        <source>Clear All</source>
        <translation>Alles leeren</translation>
    </message>
</context>
<context>
    <name>Dlg_DownloadManagerClass</name>
    <message>
        <location filename="dlg_downloadmanager.ui" line="14"/>
        <source>Dlg_DownloadManager</source>
        <translation>Dlg_DownloadManager</translation>
    </message>
</context>
<context>
    <name>Dlg_OrderForm</name>
    <message>
        <location filename="dlg_orderform.cpp" line="13"/>
        <source>Order Now</source>
        <translation>Jetzt bestellen</translation>
    </message>
</context>
<context>
    <name>Dlg_OrderFormClass</name>
    <message>
        <location filename="dlg_orderform.ui" line="13"/>
        <source>Order Form</source>
        <translation>Bestellformular</translation>
    </message>
    <message>
        <location filename="dlg_orderform.ui" line="19"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:9pt; font-weight:600;&quot;&gt;To start using the application,  you have these options:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;a) Order a trial or a full licence and install it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;b) Continue and enter your access data manually&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:9pt; font-weight:600;&quot;&gt;c) Cancel and start your purchased licence keyfile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Um das Programm verwenden zu können, müssen Sie eine Evaluationsversion oder eine lizensierte Vollversion bestellen:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-weight:600;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlg_orderform.ui" line="56"/>
        <source>Order Trial Version</source>
        <translation>Evaluationsversion bestellen</translation>
    </message>
    <message>
        <location filename="dlg_orderform.ui" line="63"/>
        <source>Order Full Version</source>
        <translation>Vollversion bestellen</translation>
    </message>
    <message>
        <location filename="dlg_orderform.ui" line="70"/>
        <source>Continue</source>
        <translation>Weiterfahren</translation>
    </message>
    <message>
        <location filename="dlg_orderform.ui" line="77"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_RemindersClass</name>
    <message>
        <location filename="dlg_reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>Erinnerung</translation>
    </message>
    <message>
        <location filename="dlg_reminders.ui" line="66"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="dlg_reminders.ui" line="73"/>
        <source>Start Date:</source>
        <translation>Startdatum:</translation>
    </message>
    <message>
        <location filename="dlg_reminders.ui" line="101"/>
        <source>Dismiss</source>
        <translation>Verwerfen</translation>
    </message>
    <message>
        <location filename="dlg_reminders.ui" line="108"/>
        <source>Dismiss All</source>
        <translation>Alle verwerfen</translation>
    </message>
    <message>
        <location filename="dlg_reminders.ui" line="128"/>
        <source>Open Item</source>
        <translation>Element öffnen</translation>
    </message>
    <message>
        <location filename="dlg_reminders.ui" line="139"/>
        <source>Postpone selected reminder(s) for</source>
        <translation>Selektierte Erinnerung(en) verschieben um</translation>
    </message>
    <message>
        <location filename="dlg_reminders.ui" line="149"/>
        <source>minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="dlg_reminders.ui" line="169"/>
        <source>PostPone</source>
        <translation>Verschieben</translation>
    </message>
</context>
<context>
    <name>Dlg_StorageLocation</name>
    <message>
        <location filename="dlg_storagelocation.cpp" line="10"/>
        <source>Document Management</source>
        <translation>Dokumentenmanagement</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="25"/>
        <source>Choose Document Storage Location</source>
        <translation>Suche Speicherort für Dokument</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="30"/>
        <source>Save Reference(s) to Local File(s)</source>
        <translation>Verweis(e) auf lokale Datei(en) speichern</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="31"/>
        <source>Store File Centrally on Internet</source>
        <translation>Datei zentral in (Internet-)Datenbank speichern</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="32"/>
        <location filename="dlg_storagelocation.cpp" line="50"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="33"/>
        <location filename="dlg_storagelocation.cpp" line="35"/>
        <source>Save a reference to a local file. The file itself keeps being stored locally where it is (on your computer or on your local server). A double-click on this file name in Communicator will open the original file. Local files can only be used inside your own network (LAN) and are not accessible from other locations.</source>
        <translation>Sie können eine Referenz auf eine bestehendeDatei speichern. Dabei bleibt die Datei lokal gespeichert, wo sie ist(auf Ihrem Computer oder auf Ihrem lokalen Server). Ein Doppelklick aufdiese Datei in Communicator öffnet die Originaldatei. Lokale Dateienkönnen nur innerhalb des eigenen Netzwerkes (LAN) verwendet werden undsind (im Gegensatz zu Internet-Dateien) von anderen Standorten ausnicht verfügbar.</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="34"/>
        <location filename="dlg_storagelocation.cpp" line="36"/>
        <source>Save a complete file in the central database of SOKRATES(R) Communicator, on the SOKRATES (R) Application Server. These files are available everywhere, even from other location over the internet.</source>
        <translation>Save a complete file in the central database ofSOKRATES(R) Communicator, on the SOKRATES (R) Application Server. Thesefiles are available everywhere, even from other location over theinternet.</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="43"/>
        <source>Open Document</source>
        <translation>Dokument öffnen</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="48"/>
        <source>Load Original Document And Modify</source>
        <translation>Originaldokument laden und bearbeiten</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="49"/>
        <source>Load Document Copy to View Only</source>
        <translation>Kopie des Dokuments zur Ansicht öffnen</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="53"/>
        <location filename="dlg_storagelocation.cpp" line="55"/>
        <source>Check out document</source>
        <translation>Dokument auschecken</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.cpp" line="54"/>
        <location filename="dlg_storagelocation.cpp" line="56"/>
        <source>Check out document as read only</source>
        <translation>Dokument nur zum Lesen auschecken</translation>
    </message>
</context>
<context>
    <name>Dlg_StorageLocationClass</name>
    <message>
        <location filename="dlg_storagelocation.ui" line="35"/>
        <source>Document Management</source>
        <translation>Dokumentenmanagement</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.ui" line="83"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="dlg_storagelocation.ui" line="115"/>
        <source>File Name:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Choose Document Storage Location&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Speicherort für Dokument bestimmen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>  Save Reference(s) to Local File(s)  </source>
        <translation type="obsolete">  Verweis(e) auf lokale Datei(en) speichern  </translation>
    </message>
    <message>
        <source>  Store File Centrally on Internet  </source>
        <translation type="obsolete">  Datei zentral in (Internet-)Datenbank speichern  </translation>
    </message>
    <message>
        <source> Cancel </source>
        <translation type="obsolete"> Abbrechen </translation>
    </message>
</context>
<context>
    <name>Dlg_TemplateDb</name>
    <message>
        <source>Information</source>
        <translation type="obsolete">Information</translation>
    </message>
    <message>
        <source>Application will close now. Server will restart. Restart application in few minutes to start using new database.</source>
        <translation type="obsolete">Die Anwendung wird nun beendet, und der Server wird neu gestartet. Bitte starten Sie die Anwendung wieder in ein paar Minuten, um die neue Datenbank zu verwenden.</translation>
    </message>
</context>
<context>
    <name>Dlg_TemplateDbClass</name>
    <message>
        <location filename="dlg_templatedb.ui" line="13"/>
        <source>Template Databases</source>
        <translation>Datenbankvorlagen</translation>
    </message>
    <message>
        <location filename="dlg_templatedb.ui" line="46"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="dlg_templatedb.ui" line="53"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dlg_templatedb.ui" line="75"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Choose template database (pressing Cancel current database will be used). Later on you can start this wizard again in menu File/Admin Tools.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Vorlagendatanbank wählen (ABBRECHEN verwendet die aktuelle Datenbank). Dieser Assistent kann auch später im Menü Datei/Administrationswerkzeuge gestartet werden.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlg_templatedb.ui" line="110"/>
        <source>Current Template:</source>
        <translation>Aktuelle Vorlage:</translation>
    </message>
    <message>
        <location filename="dlg_templatedb.ui" line="129"/>
        <source>Choose Template:</source>
        <translation>Vorlage wählen:</translation>
    </message>
</context>
<context>
    <name>Dlg_TrialPopUp</name>
    <message>
        <location filename="dlg_trialpopup.cpp" line="17"/>
        <source>Order Now</source>
        <translation>Jetzt bestellen</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.cpp" line="69"/>
        <source>Select keyfile or registration utility</source>
        <translation>Keyfile (Lizenzschlüssel) oder das Registrierungsprogramm auswählen</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.cpp" line="98"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.cpp" line="98"/>
        <source>Please restart application, to start using registered version!</source>
        <translation>Die registrierte Anwendung ist nach einem Neustart aktiv!</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.cpp" line="136"/>
        <source> Communicator has expired! You can purchase a license keyfile for the unlimited version.</source>
        <translation>Ihre Evaluationsversion von SOKRATES®  Communicator  ist abgelaufen! Bitte erwerben Sie einen Lizenzschlüssel für eine unbeschränkte Vollersion.</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.cpp" line="140"/>
        <source> Communicator. You can use it for a </source>
        <translation> Communicator. Verwenden Sie das Programm während </translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.cpp" line="140"/>
        <source> day trial period or you can purchase a licence keyfile for the unlimited version.</source>
        <translation>   Tagen, oder erwerben Sie eine unbeschränkte Lizenz.</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.cpp" line="136"/>
        <source>Your trial version of SOKRATES</source>
        <translation>Diese Testversion von SOKRATES</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.cpp" line="140"/>
        <source>This is a trial version of SOKRATES</source>
        <translation>Dies ist eine Testversion von SOKRATES</translation>
    </message>
</context>
<context>
    <name>Dlg_TrialPopUpClass</name>
    <message>
        <location filename="dlg_trialpopup.ui" line="13"/>
        <source>Trial</source>
        <translation>Evaluation</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.ui" line="25"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;This is a trial version of SOKRATES®  Communicator. You can use it for a 30 day trial period or you can purchase a licence keyfile for the unlimited version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Dies ist eine Evaluationsversion von SOKRATES®  Communicator. Sie dürfen sie für 30 Tage zu Evaluationszwecken verwenden, oder Sie können eine Lizenzdatei (Keyfile) für die uneingeschränkte Verwendung bestellen.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.ui" line="62"/>
        <source>Order Trial</source>
        <translation>Evaluationsversion bestellen</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.ui" line="69"/>
        <source>Order Full Version</source>
        <translation>Vollversion bestellen</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.ui" line="76"/>
        <source>Register Purchased Keyfile</source>
        <translation>Erworbenes Keyfile (Lizenzschlüssel) registrieren</translation>
    </message>
    <message>
        <location filename="dlg_trialpopup.ui" line="83"/>
        <source>Continue With Trial</source>
        <translation>Mit Evaluation fortfahren</translation>
    </message>
</context>
<context>
    <name>DocumentHelper</name>
    <message>
        <location filename="documenthelper.cpp" line="246"/>
        <location filename="documenthelper.cpp" line="276"/>
        <source>Save As</source>
        <translation>Speichern unter</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="383"/>
        <location filename="documenthelper.cpp" line="414"/>
        <location filename="documenthelper.cpp" line="449"/>
        <location filename="documenthelper.cpp" line="698"/>
        <location filename="documenthelper.cpp" line="894"/>
        <location filename="documenthelper.cpp" line="910"/>
        <location filename="documenthelper.cpp" line="917"/>
        <location filename="documenthelper.cpp" line="949"/>
        <location filename="documenthelper.cpp" line="1145"/>
        <location filename="documenthelper.cpp" line="1588"/>
        <location filename="documenthelper.cpp" line="1601"/>
        <location filename="documenthelper.cpp" line="1880"/>
        <location filename="documenthelper.cpp" line="2424"/>
        <location filename="documenthelper.cpp" line="2430"/>
        <location filename="documenthelper.cpp" line="2492"/>
        <location filename="documenthelper.cpp" line="2635"/>
        <location filename="documenthelper.cpp" line="2820"/>
        <location filename="documenthelper.cpp" line="2834"/>
        <location filename="documenthelper.cpp" line="2959"/>
        <location filename="documenthelper.cpp" line="3556"/>
        <location filename="documenthelper.cpp" line="3737"/>
        <location filename="documenthelper.cpp" line="3777"/>
        <location filename="documenthelper.cpp" line="3869"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="510"/>
        <location filename="documenthelper.cpp" line="1176"/>
        <location filename="documenthelper.cpp" line="1193"/>
        <location filename="documenthelper.cpp" line="1291"/>
        <location filename="documenthelper.cpp" line="1374"/>
        <location filename="documenthelper.cpp" line="1459"/>
        <location filename="documenthelper.cpp" line="1503"/>
        <location filename="documenthelper.cpp" line="2160"/>
        <location filename="documenthelper.cpp" line="2174"/>
        <location filename="documenthelper.cpp" line="2299"/>
        <location filename="documenthelper.cpp" line="2386"/>
        <location filename="documenthelper.cpp" line="2412"/>
        <location filename="documenthelper.cpp" line="3264"/>
        <location filename="documenthelper.cpp" line="4074"/>
        <location filename="documenthelper.cpp" line="4097"/>
        <location filename="documenthelper.cpp" line="4117"/>
        <location filename="documenthelper.cpp" line="4165"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="510"/>
        <source>Application is not set for this document. Operation aborted!</source>
        <translation>Keine Anwendung für dieses Dokument zugewiesen. Vorgang abgebrochen!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="821"/>
        <source>Please Search Your Installation of </source>
        <translation>Bitte suchen Sie die Installation von </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="894"/>
        <source>Failed to open attachment:</source>
        <translation>Anhang konnte nicht geöffnet werden:</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="910"/>
        <location filename="documenthelper.cpp" line="917"/>
        <source>Failed to unzip attachment:</source>
        <translation>Anhang konnte nicht entpackt werden:</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1145"/>
        <source>Failed to Pack &amp; Send Documents</source>
        <translation>Dokumente konnten nicht gepackt &amp; versendet werden</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1175"/>
        <location filename="documenthelper.cpp" line="3263"/>
        <source>The Local File </source>
        <translation>Die lokale Date </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1175"/>
        <location filename="documenthelper.cpp" line="3263"/>
        <source> is not visible on this computer and can not be opened!</source>
        <translation> ist nicht sichtbar auf diesem Computer und kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1192"/>
        <source>The Document </source>
        <translation>Das Dokument </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1192"/>
        <source> can not be check out!</source>
        <translation> kann nicht ausgecheckt werden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1229"/>
        <source>Name: </source>
        <translation>Name: </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1231"/>
        <source>Location: </source>
        <translation>Ort: </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1290"/>
        <source>Failed to zip document </source>
        <translation>Packen (zippen) nicht erfolgreich von </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1374"/>
        <location filename="documenthelper.cpp" line="1459"/>
        <source>Failed to create document from template! To create Local Or Internet Document, Template must be either Local or Internet document type!</source>
        <translation>Dokument kann nicht aus Vorlage erstellt werden! Um ein lokales oder ein Internet-Dokument zu erzeugen muss die Vorlage ein lokales oder ein Internet-Dokument sein!</translation>
    </message>
    <message>
        <source>Enter Document Name</source>
        <translation type="obsolete">Dokumentennamen eingeben</translation>
    </message>
    <message>
        <source>Enter Document Name:</source>
        <translation type="obsolete">Dokumentennamen eingeben:</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1503"/>
        <source>Failed to create document from template!</source>
        <translation>Dokument konnte nicht aus Vorlage erstellt werden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="4074"/>
        <source>Check out not possible: Please check-in a document using &apos;Edit&apos; before you can open it!</source>
        <translation>Auschecken nicht möglich: Bitte checken Sie ein Dokument ein (unter &apos;Bearbeiten&apos;) bevor es geöffnet werden kann!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="4097"/>
        <source>Automated Temporary Storage mechanism failed to determine Document check out location. Choose path for check-out location?</source>
        <translation>Es konnte kein Pfad bestimmt werden für die temporäre Speicherung des ausgescheckten Dokumentes. Wollen Sie einen Pfad wählen?</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1972"/>
        <location filename="documenthelper.cpp" line="1994"/>
        <location filename="documenthelper.cpp" line="2921"/>
        <location filename="documenthelper.cpp" line="4097"/>
        <location filename="documenthelper.cpp" line="4117"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1972"/>
        <location filename="documenthelper.cpp" line="1994"/>
        <location filename="documenthelper.cpp" line="2921"/>
        <location filename="documenthelper.cpp" line="4097"/>
        <location filename="documenthelper.cpp" line="4117"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1601"/>
        <source>Document does not have any revisions uploaded!</source>
        <translation>Keine Dokumenten-Revisionen vorhanden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1746"/>
        <source>Find Checked-out Document</source>
        <translation>Ausgechecktes Dokument suchen</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1764"/>
        <source>Failed to check in document</source>
        <translation>Dokument konnte nicht eingecheckt werden</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1764"/>
        <source>Failed to check in document! Document Not Found!</source>
        <translation>Dokument konnte nicht eingecheckt werden! Dokument nicht gefunden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1764"/>
        <source>  Discard Checked-Out Revision of Document  </source>
        <translation>  Ausgecheckte Revision des Dokuments verwerfen  </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1764"/>
        <source> Cancel </source>
        <translation> Abbrechen </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1826"/>
        <source>No document for check in!</source>
        <translation>Kein Dokument zum Einchecken gefunden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3466"/>
        <location filename="documenthelper.cpp" line="3474"/>
        <location filename="documenthelper.cpp" line="3481"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1972"/>
        <location filename="documenthelper.cpp" line="1994"/>
        <source>Save Changes</source>
        <translation>Änderungen speichern</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1972"/>
        <location filename="documenthelper.cpp" line="1994"/>
        <source>There are some checked out and modified documents. Do you want to check in all modified documents?</source>
        <translation>Es gibt ausgecheckte und bearbeitete Dokumente. Wollen alle diese Dokumente wieder eingecheckt werden?</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2159"/>
        <location filename="documenthelper.cpp" line="2173"/>
        <source>File: </source>
        <translation>Datei: </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2159"/>
        <location filename="documenthelper.cpp" line="2173"/>
        <source> can not be uploaded on server!</source>
        <translation>kann nicht auf Server hochgeladen werden (max. 160MB)!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2299"/>
        <source>You do not have permission to save document either locally or on Internet!</source>
        <translation>Sie haben nicht genügend Zugriffsrecthe, um eine Datei lokal oder zentral in der (Internet-)Datenbank zu speichern!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2430"/>
        <source>Unable to retrieve latest revision from server</source>
        <translation>Letzte Revision kann auf dem Server nicht gefunden werden</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2492"/>
        <source>Template file  </source>
        <translation>Vorlagendatei  </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2492"/>
        <source> does not exists! Operation aborted!</source>
        <translation> existiert nicht! Vorgang abgebrochen!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2522"/>
        <source>Copy Failed!</source>
        <translation>Kopie konnte nicht erstellt werden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2522"/>
        <source>Copy failed to location:  </source>
        <translation>Kopie konnte nicht erstellt werden in:  </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2522"/>
        <source>. Operation aborted!</source>
        <translation>. Vorgang abgebrochen!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2921"/>
        <source>Check In</source>
        <translation>Einchecken</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2921"/>
        <source>Checked-out document with same name: </source>
        <translation>Ausgechecktes Dokument mit dem gleichen Namen: </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2921"/>
        <source> already exists. Do you wish to check-in?</source>
        <translation> existiert bereits. Soll es eingecheckt werden?</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2959"/>
        <source>Revision could not be loaded for file:</source>
        <translation>Die Version konnte für folgende Datei nicht geladen werden:</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3329"/>
        <source>Open Document</source>
        <translation>Dokument öffnen</translation>
    </message>
    <message>
        <source>Open Document: </source>
        <translation type="obsolete">Dokument öffnen: </translation>
    </message>
    <message>
        <source>Load Original And Modify</source>
        <translation type="obsolete">Original laden und bearbeiten</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3391"/>
        <source> Document path is </source>
        <translation> Dokumentenpfad ist </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3392"/>
        <source> and will be deleted after SOKRATES</source>
        <translation> und wird gelöscht nach dem Schliessen von SOKRATES.</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3556"/>
        <source>Application %1 does not exists on given path: %2. Application Registration skipped!</source>
        <translation>Anwendung %1 existiert nicht im erwarteten Pfad: %2. Registrieren der Anwendung abgebrochen!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3869"/>
        <source>Template %1 does not exists on given path: %2. Template Registration skipped!</source>
        <translation>Vorlage %1 existiert nicht im erwarteten Pfad: %2. Registrieren der Vorlage abgebrochen!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="4165"/>
        <source>You try to register a Microsoft(R) Office(R) template document. Office(R) templates are not valid to be registered as SOKRATES(R) templates: Please open the template in the Office(R) application and store it as a &quot;Document&quot; (with extension doc,docx,xls,xlsx,ppt,pptx...), not as a &quot;Template&quot;. You can register this new document in SOKRATES(R) Communicator as a template afterwords!</source>
        <translation>Sie versuchen, ein Microsoft(R) Office(R) Vorlagendokument als SOKRATES(R)-Vorlage zu registrieren. Dieses Format ist aber dazu nicht geeignet! Öffnen Sie daher das Vorlagendokument zuerst in der passenden Office(R)-Anwendung und speichern Sie es als &quot;Dokument&quot; (mit einer der Endungen doc,docx,xls,xlsx,ppt,pptx...), und nicht als &quot;Vorlage&quot;. Dieses neue Dokument können Sie dann als SOKRATES(R)-Vorlage registrieren!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3466"/>
        <source>Processed successfully: %1 applications, skipped/failed: %2</source>
        <translation>Erfolgreich verarbeitet: %1 Anwendungen; nicht erfolgreich verarbeitet/übersprungen: %2</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3474"/>
        <source>Processed successfully: %1 templates, skipped/failed: %2</source>
        <translation>Erfolgreich verarbeitet: %1 Vorlage(n), übersprungen: %2</translation>
    </message>
    <message>
        <source>Failed to save:</source>
        <translation type="obsolete">Konnte nicht gespeichert werden:</translation>
    </message>
    <message>
        <source>Create Template</source>
        <translation type="obsolete">Vorlage erstellen</translation>
    </message>
    <message>
        <source>Create </source>
        <translation type="obsolete">Erstellen </translation>
    </message>
    <message>
        <source>Create Documents</source>
        <translation type="obsolete">Dokumente erstellen</translation>
    </message>
    <message>
        <source> document(s) from dropped items(s)</source>
        <translation type="obsolete"> Dokument(e) aus hineingezogenem(/n) Element(en)</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3481"/>
        <source>Processed successfully: %1 interfaces, skipped/failed: %2</source>
        <translation>Erfolgreich verarbeitet: %1 Schnittstelle(n), übersprungen/abgebrochen: %2</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1572"/>
        <location filename="documenthelper.cpp" line="1682"/>
        <location filename="documenthelper.cpp" line="2608"/>
        <source>Transferring document: </source>
        <translation>Dokument wird übertragen:</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="4047"/>
        <source>Revision TAG</source>
        <translation>Versionsmarkierung</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="4047"/>
        <source>Set Revision TAG for file: </source>
        <translation>Versionsmarkierung für Datei setzen:</translation>
    </message>
    <message>
        <source>File exists. Overwrite?</source>
        <translation type="obsolete">Datei mit gleichem Namen besteht schon. Überschreiben?</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="4117"/>
        <source>File %1 exists. Overwrite?</source>
        <translation>Datei %1 besteht bereits. Überschreiben?</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2412"/>
        <source>Operation failed because document revision size is greater then %1!</source>
        <translation>Abbruch, weil die Grösse der Dokumenten-Revision grösser als %1 ist!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="1479"/>
        <location filename="documenthelper.cpp" line="2454"/>
        <source>Enter Document Name With Extension</source>
        <translation>Dateiname mit Erweiterung:</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2386"/>
        <source>Operation failed! Create from template and pack and send functions can not use documents with size greater then %1!</source>
        <translation>Ausführung abgebrochen! Die Funktionen &quot;Aus Vorlage erzeugen&quot; und &quot;Packen und Versenden&quot; können nicht verwendet werden für Dokumente, deren Grösse %1 übersteigt!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2454"/>
        <source>Enter Document Name With Extension:</source>
        <translation>Dokumentenname inklusive Dateierweiterung eingeben:</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="2160"/>
        <location filename="documenthelper.cpp" line="2174"/>
        <source>File size can not be larger than 80Mb!</source>
        <translation>Datei darf nicht grösser als 80MB sein!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3328"/>
        <source>Document is already checked out by user %1. Do you want to open document in read-only mode?</source>
        <translation>Das Dokument ist bereits zur Bearbeitung ausgecheckt durch den Benutzer %1. Wollen Sie es schreibgeschützt öffnen?</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3329"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3329"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3392"/>
        <source> closes.</source>
        <translation> schliesst.</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="163"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="164"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="165"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="233"/>
        <source>Queued</source>
        <translation>In der Warteschlange</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="244"/>
        <source>DownLoading</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="255"/>
        <location filename="dlg_downloadmanager.cpp" line="304"/>
        <source>Uploading</source>
        <translation>Hochladen</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="265"/>
        <source>Done</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="275"/>
        <source>Canceled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="285"/>
        <source>Error: %1</source>
        <translation>Fehler: %1</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="166"/>
        <source>Open Directory</source>
        <translation>Verzeichnis öffnen</translation>
    </message>
    <message>
        <location filename="dlg_downloadmanager.cpp" line="306"/>
        <source>Downloading</source>
        <translation>Herunterladen</translation>
    </message>
</context>
<context>
    <name>EmailSelectorDialog</name>
    <message>
        <location filename="emailselectordialog.cpp" line="17"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="emailselectordialog.cpp" line="17"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="emailselectordialog.cpp" line="17"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
    <message>
        <location filename="emailselectordialog.cpp" line="17"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
</context>
<context>
    <name>EmailSelectorDialogClass</name>
    <message>
        <location filename="emailselectordialog.ui" line="13"/>
        <source>Email Selector</source>
        <translation>Auswahl E-Mail</translation>
    </message>
    <message>
        <location filename="emailselectordialog.ui" line="32"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="emailselectordialog.ui" line="39"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
</context>
<context>
    <name>Email_AttachmentTable</name>
    <message>
        <location filename="email_attachmenttable.cpp" line="183"/>
        <source>Choose file</source>
        <translation>Datei wählen</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="218"/>
        <source>Save As</source>
        <translation>Speichern unter</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="279"/>
        <location filename="email_attachmenttable.cpp" line="286"/>
        <location filename="email_attachmenttable.cpp" line="345"/>
        <location filename="email_attachmenttable.cpp" line="352"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="279"/>
        <source>Attachment with this name already exists!</source>
        <translation>Ein Anhang mit dem gleichen Namen besteht bereits!</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="286"/>
        <source>Attachment can not be loaded!</source>
        <translation>Anhang kann nicht verarbeitet werden!</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="325"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="325"/>
        <source>Failed to compress attachments!</source>
        <translation>Anhänge konnten nicht komprimiert werden!</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="344"/>
        <source>Failed to zip document </source>
        <translation>Dokument kann nicht komprimiert werden </translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="352"/>
        <source>Attachment can not be saved!</source>
        <translation>Anhang kann nicht gespeichert werden!</translation>
    </message>
</context>
<context>
    <name>Email_RecepientTable</name>
    <message>
        <location filename="email_recepienttable.cpp" line="41"/>
        <source>TO:</source>
        <translation>TO:</translation>
    </message>
    <message>
        <location filename="email_recepienttable.cpp" line="44"/>
        <source>CC:</source>
        <translation>CC:</translation>
    </message>
    <message>
        <location filename="email_recepienttable.cpp" line="47"/>
        <source>BCC:</source>
        <translation>BCC:</translation>
    </message>
</context>
<context>
    <name>LanguageWarningDlg</name>
    <message>
        <location filename="languagewarningdlg.cpp" line="20"/>
        <source>Your preferred language &quot;%1&quot; is currently not the default language of the software.</source>
        <translation>Ihre bevorzugte Sprache %1 ist im Moment nicht voreingestellt.</translation>
    </message>
    <message>
        <location filename="languagewarningdlg.cpp" line="21"/>
        <source>Continue with the actual language &quot;%1&quot;</source>
        <translation>Weiterfahren mit der eingestellten Sprache &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="languagewarningdlg.cpp" line="22"/>
        <source>Switch your language &quot;%1&quot;. You will have to login again.</source>
        <translation>Sprache &quot;%1&quot; wählen. Eine erneute Anmeldung ist erforderlich.</translation>
    </message>
    <message>
        <location filename="languagewarningdlg.cpp" line="10"/>
        <location filename="languagewarningdlg.cpp" line="16"/>
        <source>english</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="languagewarningdlg.cpp" line="12"/>
        <location filename="languagewarningdlg.cpp" line="18"/>
        <source>german</source>
        <translation>Deutsch</translation>
    </message>
</context>
<context>
    <name>LanguageWarningDlgClass</name>
    <message>
        <location filename="languagewarningdlg.ui" line="56"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="languagewarningdlg.ui" line="14"/>
        <source>Language Warning</source>
        <translation>Sprachhinweis</translation>
    </message>
</context>
<context>
    <name>LoginController</name>
    <message>
        <source>Login In Progress</source>
        <translation type="obsolete">Login wird durchgeführt</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Error Message</source>
        <translation type="obsolete">Fehlermeldung</translation>
    </message>
    <message>
        <source>You have reached maximum number of login attempts!
The application will be closed now.</source>
        <translation type="obsolete">Maximale Anzahl Login-Versuche erreicht!
Die Anwendung wird geschlossen.</translation>
    </message>
</context>
<context>
    <name>PhoneSelectorDlg</name>
    <message>
        <location filename="phoneselectordlg.cpp" line="15"/>
        <source>Phone</source>
        <translation>Telefonnummer</translation>
    </message>
    <message>
        <location filename="phoneselectordlg.cpp" line="15"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="phoneselectordlg.cpp" line="15"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
</context>
<context>
    <name>PhoneSelectorDlgClass</name>
    <message>
        <location filename="phoneselectordlg.ui" line="16"/>
        <source>Select Phone</source>
        <translation>Telefonnummer wählen</translation>
    </message>
    <message>
        <location filename="phoneselectordlg.ui" line="64"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="phoneselectordlg.ui" line="71"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error Message</source>
        <translation type="obsolete">Fehlermeldung</translation>
    </message>
    <message>
        <source>You must order!</source>
        <translation type="obsolete">Bestellen!</translation>
    </message>
    <message>
        <source>INI file is missing,corrupted or has invalid values!</source>
        <translation type="obsolete">INI-Datei fehlt oder ist beschädigt!</translation>
    </message>
    <message>
        <source>License file is missing or corrupted!</source>
        <translation type="obsolete">Linzenzschlüsseldatei (Keyfile) fehlt oder ist beschädigt!</translation>
    </message>
    <message>
        <source>Settings directory:</source>
        <translation type="obsolete">Einstellungsverzeichnis:</translation>
    </message>
    <message>
        <source> could not be created!</source>
        <translation type="obsolete"> kann nicht erstellt werden!</translation>
    </message>
    <message>
        <source>.Do you want to backup data now? By clicking &apos;No&apos; the backup will be postponed.</source>
        <translation type="obsolete">. Wollen Sie jetzt ein Datenbackup durchführen? &apos;Nein&apos; verschiebt das Backup.</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Information</translation>
    </message>
    <message>
        <source>New backup successfully created!</source>
        <translation type="obsolete">Neues Backup erfolgreich erstellt!</translation>
    </message>
    <message>
        <source>Please, restart application to start restore operation!</source>
        <translation type="obsolete">Bitte starten Sie die Anwendung erneut um das Datenbackup einzulesen!</translation>
    </message>
    <message>
        <source>Please, save all work and close application, server will restart and restore from backup in few minutes.</source>
        <translation type="obsolete">Bitte alles speichern und Anwendung schliessen, der Server wird in wenigen Minuten neu gestartet mit einer Backup-Datenwiederherstellung.</translation>
    </message>
    <message>
        <source>A scheduled backup will start now. It will take few minutes. Proceed? (You can change backup schedule settings in Menu/Backup &amp; Restore.)

</source>
        <translation type="obsolete">Ein geplantes Backup wird jetzt starten und einige Minuten dauern. Weiterfahren? (Die Backup-Einstellungen können geändert werden unter Menu/ Backup &amp; Restore.)
</translation>
    </message>
    <message>
        <source>Backup Scheduler</source>
        <translation type="obsolete">Backup-Planer</translation>
    </message>
    <message>
        <source>Select KeyFile</source>
        <translation type="obsolete">Keyfile wählen</translation>
    </message>
    <message>
        <source>Contact data</source>
        <translation type="obsolete">Kontaktdaten</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="299"/>
        <source>Phones</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <source>Addresses</source>
        <translation type="obsolete">Adressen</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="300"/>
        <source>Emails</source>
        <translation>E-Mails</translation>
    </message>
    <message>
        <source>Internet addresses</source>
        <translation type="obsolete">Internet-Adressen</translation>
    </message>
    <message>
        <source>Pictures</source>
        <translation type="obsolete">Bilder</translation>
    </message>
    <message>
        <source>Creditor data</source>
        <translation type="obsolete">Kreditoren-Daten</translation>
    </message>
    <message>
        <source>Debtor data</source>
        <translation type="obsolete">Debitoren-Daten</translation>
    </message>
    <message>
        <source>Main Data</source>
        <translation type="obsolete">Stammdaten</translation>
    </message>
    <message>
        <source>Code is mandatory field!</source>
        <translation type="obsolete">Code zwingend!</translation>
    </message>
    <message>
        <source>Name is mandatory field!</source>
        <translation type="obsolete">Name zwingend!</translation>
    </message>
    <message>
        <source>User data</source>
        <translation type="obsolete">Benutzerdaten</translation>
    </message>
    <message>
        <source>Last Name is mandatory field!</source>
        <translation type="obsolete">Nachname zwingend!</translation>
    </message>
    <message>
        <source>First Name is mandatory field!</source>
        <translation type="obsolete">Vorname zwingend!</translation>
    </message>
    <message>
        <source>Pers. No. is mandatory field!</source>
        <translation type="obsolete">Personalnummer zwingend!</translation>
    </message>
    <message>
        <source>Initials is mandatory field!</source>
        <translation type="obsolete">Initialen zwingend!</translation>
    </message>
    <message>
        <source>Address is mandatory field!</source>
        <translation type="obsolete">Adresse zwingend!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="301"/>
        <location filename="clientcontactmanager.cpp" line="326"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>Organization Name is mandatory field!</source>
        <translation type="obsolete">Organisationsname zwingend!</translation>
    </message>
    <message>
        <source>Email address is mandatory field!</source>
        <translation type="obsolete">E-Mail-Adresse zwingend!</translation>
    </message>
    <message>
        <source>Internet address is mandatory field!</source>
        <translation type="obsolete">Internet-Adresse zwingend!</translation>
    </message>
    <message>
        <source>Person is mandatory field inside journal entry!</source>
        <translation type="obsolete">Benutzerangabe zwingend innerhalb Journaleintrag!</translation>
    </message>
    <message>
        <source>Phone number is mandatory field!</source>
        <translation type="obsolete">Telefonnummer zwingend!</translation>
    </message>
    <message>
        <source>Only one user defined type can have the selected system type!</source>
        <translation type="obsolete">Der Systemtyp darf nur einem einzigen benutzerdefinierten Typ zugewiesen werden!</translation>
    </message>
    <message>
        <source>Type Name is mandatory field!</source>
        <translation type="obsolete">Typenname zwingend!</translation>
    </message>
    <message>
        <source>Username is mandatory field!</source>
        <translation type="obsolete">Benutzername zwingend!</translation>
    </message>
    <message>
        <source>Password must be set!</source>
        <translation type="obsolete">Passwort muss gesetzt sein!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="3187"/>
        <location filename="emailhelper.cpp" line="52"/>
        <location filename="mainentityselectioncontroller.cpp" line="615"/>
        <location filename="mainentityselectioncontroller.cpp" line="1052"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Fixed</source>
        <translation type="obsolete">gültig</translation>
    </message>
    <message>
        <source>Preliminary</source>
        <translation type="obsolete">provisorisch</translation>
    </message>
    <message>
        <source>Cancelled</source>
        <translation type="obsolete">abgebrochen</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="83"/>
        <location filename="documenthelper.cpp" line="140"/>
        <location filename="documenthelper.cpp" line="3447"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3429"/>
        <source>File association information not complete!</source>
        <translation>Informationen über Dateiverknüpfung unvollständig!</translation>
    </message>
    <message>
        <source>Your version %1 is not actual any more. Do you want to download and install an update to version %2 now?</source>
        <translation type="obsolete">Version %1 ist nicht mehr aktuell. Wollen Sie jetzt die neue Version %2 herunterladen und installieren?</translation>
    </message>
    <message>
        <source>Not thick client mode!</source>
        <translation type="obsolete">Kein Thick mode!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="281"/>
        <location filename="clientcontactmanager.cpp" line="309"/>
        <source>External Code</source>
        <translation>Externer Code</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="282"/>
        <location filename="clientcontactmanager.cpp" line="310"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="283"/>
        <location filename="clientcontactmanager.cpp" line="311"/>
        <source>Ancient Last Name</source>
        <translation>Ehemaliger Nachname</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="341"/>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="342"/>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="286"/>
        <location filename="clientcontactmanager.cpp" line="314"/>
        <source>Middle Name</source>
        <translation>Zweiter Vorname</translation>
    </message>
    <message>
        <source>Department</source>
        <translation type="obsolete">Abteilung</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="288"/>
        <location filename="clientcontactmanager.cpp" line="316"/>
        <source>Day of birth</source>
        <translation>Geburtstag</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="289"/>
        <location filename="clientcontactmanager.cpp" line="317"/>
        <location filename="clientcontactmanager.cpp" line="343"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="290"/>
        <location filename="clientcontactmanager.cpp" line="318"/>
        <source>Organization Short Name</source>
        <translation>Kurzname der Organisation</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="291"/>
        <location filename="clientcontactmanager.cpp" line="319"/>
        <source>Organization Foundation Date</source>
        <translation>Gründungsdatum</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="292"/>
        <location filename="clientcontactmanager.cpp" line="320"/>
        <source>Profession</source>
        <translation>Beruf</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="293"/>
        <location filename="clientcontactmanager.cpp" line="321"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="294"/>
        <location filename="clientcontactmanager.cpp" line="322"/>
        <source>Male/Female</source>
        <translation>Mann/Frau</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="295"/>
        <location filename="clientcontactmanager.cpp" line="327"/>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="296"/>
        <location filename="clientcontactmanager.cpp" line="328"/>
        <location filename="mainentityselectioncontroller.cpp" line="336"/>
        <source>Valid From</source>
        <translation>Gültig ab</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="297"/>
        <location filename="clientcontactmanager.cpp" line="329"/>
        <location filename="mainentityselectioncontroller.cpp" line="337"/>
        <source>Valid To</source>
        <translation>Gültig bis</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="299"/>
        <source>Phone</source>
        <translation>Telefonnummer</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="302"/>
        <source>Web Site</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="302"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="323"/>
        <source>Any Phone Number</source>
        <translation>eine Telephonnummer</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="324"/>
        <source>Any Net Address</source>
        <translation>eine Internetadresse</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="325"/>
        <location filename="mainentityselectioncontroller.cpp" line="365"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="71"/>
        <location filename="mainentityselectioncontroller.cpp" line="189"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="73"/>
        <location filename="mainentityselectioncontroller.cpp" line="89"/>
        <location filename="mainentityselectioncontroller.cpp" line="174"/>
        <location filename="mainentityselectioncontroller.cpp" line="190"/>
        <location filename="mainentityselectioncontroller.cpp" line="225"/>
        <location filename="mainentityselectioncontroller.cpp" line="245"/>
        <location filename="mainentityselectioncontroller.cpp" line="437"/>
        <location filename="mainentityselectioncontroller.cpp" line="453"/>
        <location filename="mainentityselectioncontroller.cpp" line="464"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="74"/>
        <source>Dept. Name</source>
        <translation>Abteilung</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="88"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="103"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="121"/>
        <source>Role NM Pairs</source>
        <translation>Rolle in Paarbeziehung</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="155"/>
        <source>Part</source>
        <translation>Teil</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="206"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="289"/>
        <source>Role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="304"/>
        <source>Application Name</source>
        <translation>Applikationsname</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="319"/>
        <location filename="mainentityselectioncontroller.cpp" line="381"/>
        <location filename="mainentityselectioncontroller.cpp" line="397"/>
        <source>Template Name</source>
        <translation>Vorlagenname</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="334"/>
        <source>Tree</source>
        <translation>Baum</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="335"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="396"/>
        <source>Category Name</source>
        <translation>Kategorienname</translation>
    </message>
    <message>
        <location filename="selectionpopupfloat.cpp" line="49"/>
        <source>Selector</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="selectionpopupfloat.cpp" line="53"/>
        <source>User Selector</source>
        <translation>Benutzerauswahl</translation>
    </message>
    <message>
        <location filename="selectionpopupfloat.cpp" line="56"/>
        <source>Contact Selector</source>
        <translation>Kontaktauswahl</translation>
    </message>
    <message>
        <location filename="selectionpopupfloat.cpp" line="59"/>
        <source>Project Selector</source>
        <translation>Projektauswahl</translation>
    </message>
    <message>
        <source>Record not loaded!</source>
        <translation type="obsolete">Datensatz nicht geladen!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="2693"/>
        <source>Contact is already favorite!</source>
        <translation>Kontakt gehört schon zu den Favoriten!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="2698"/>
        <source>You can not have more then</source>
        <translation>Mehr als</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="2698"/>
        <source>favorites!</source>
        <translation>favoriten nicht zugelassen!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="2720"/>
        <source>Contacts is not favorite!</source>
        <translation>Der Kontakt gehört nicht zu den Favoriten!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="140"/>
        <source>File: </source>
        <translation>Datei: </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="71"/>
        <source> can not be written!</source>
        <translation> kann nicht geschrieben werden!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="3169"/>
        <source>Reading Detailed Contact List...</source>
        <translation>Detaillierte Kontaktliste wird geladen...</translation>
    </message>
    <message>
        <location filename="clientapihelper.cpp" line="63"/>
        <location filename="clientcontactmanager.cpp" line="3169"/>
        <location filename="emailhelper.cpp" line="920"/>
        <location filename="httplookup.cpp" line="48"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="3170"/>
        <source>Read In Progress</source>
        <translation>Lesen in Arbeit</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="3200"/>
        <source>User aborted operation!</source>
        <translation>Vorgang vom Benutzer abgebrochen!</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="352"/>
        <source>EMail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="71"/>
        <source>The File </source>
        <translation>Die Datei </translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="83"/>
        <source>File can not be written!</source>
        <translation>Datei kann nicht geschrieben werden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="123"/>
        <location filename="documenthelper.cpp" line="1101"/>
        <source>Load Document</source>
        <translation>Dokument laden</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="140"/>
        <source> was not found!</source>
        <translation> wurde nicht gefunden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3411"/>
        <source>File not found!</source>
        <translation>Datei nicht gefunden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3414"/>
        <source>Path not found!</source>
        <translation>Pfad nicht gefunden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3417"/>
        <source>Access denied!</source>
        <translation>Zugriff verweigert!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3420"/>
        <source>Out of memory!</source>
        <translation>Zu wenig Speicher!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3423"/>
        <source>Dynamic-link library not found!</source>
        <translation>DLL-Datei nicht gefunden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3426"/>
        <source>Cannot share an open file!</source>
        <translation>Eine geöffnete Datei kann nicht gleichzeitig bearbeitet werden!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3432"/>
        <source>DDE operation timed out!</source>
        <translation>Zeit abgelaufen für DDE-Datenaustauschvorgang!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3435"/>
        <source>DDE operation failed!</source>
        <translation>DDE-Vorgang nicht erfolgreich!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3438"/>
        <source>DDE operation is busy!</source>
        <translation>DDE-Vorgang in Arbeit!</translation>
    </message>
    <message>
        <location filename="documenthelper.cpp" line="3441"/>
        <source>File association not available!</source>
        <translation>Dateiverknüpfung nicht verfügbar!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="3052"/>
        <source>Failed to remove %1 file</source>
        <translation>Datei %1 kann nicht entfernt werden</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="3058"/>
        <location filename="clientcontactmanager.cpp" line="3069"/>
        <source>File %1  can not be written!</source>
        <translation>Datei %1  kann nicht gespeichert werden!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="3094"/>
        <source>File: %1 can not be loaded!</source>
        <translation>Datei: %1 kann nicht geladen werden!</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="2783"/>
        <source>Failed to load picture</source>
        <translation>Konnte Bild nicht laden</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="284"/>
        <source>Last Name</source>
        <comment>FUCK</comment>
        <translation>Nachname</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="285"/>
        <source>First Name</source>
        <comment>FUCK</comment>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="287"/>
        <source>Department</source>
        <comment>FUCK</comment>
        <translation>Abteilung</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.cpp" line="35"/>
        <source>Select Projects for Stored List</source>
        <translation>Projekte für die gespeicherte Projektliste auswäheln</translation>
    </message>
    <message>
        <location filename="dlg_accuserrecord.cpp" line="243"/>
        <source>Select Projects</source>
        <translation>Projekte auswählen</translation>
    </message>
    <message>
        <location filename="httplookup.cpp" line="26"/>
        <location filename="httplookup.cpp" line="30"/>
        <location filename="httplookup.cpp" line="85"/>
        <source>Invalid Research</source>
        <translation>Ungültige Suche</translation>
    </message>
    <message>
        <location filename="httplookup.cpp" line="26"/>
        <source>Neither ZIP nor Town fields are empty!</source>
        <translation>Weder Postleitzahl noch Ort sind leer!</translation>
    </message>
    <message>
        <location filename="httplookup.cpp" line="30"/>
        <source>Either ZIP or Town have to be entered to find the other!</source>
        <translation>Entweder eine Postleitzahl oder ein Ort müssen erfasst sein, um das zweite Feld suchen zu lassen!</translation>
    </message>
    <message>
        <location filename="httplookup.cpp" line="85"/>
        <source>The ZIP/Town server could not be reached. Please try again later!</source>
        <translation>Der Server für die Postleitzahl-/Ortssuche ist nicht erreichbar. Bitte versuchen Sie es später noch einmal!</translation>
    </message>
    <message>
        <location filename="httplookup.cpp" line="48"/>
        <source>Contacting Web Service...</source>
        <translation>Verbindung zu Webservice wird hergestellt...</translation>
    </message>
    <message>
        <location filename="emailhelper.cpp" line="920"/>
        <source>Contacting Web Site...</source>
        <translation>Kontakt mit Webseite wird hergestellt...</translation>
    </message>
    <message>
        <location filename="emailhelper.cpp" line="921"/>
        <location filename="httplookup.cpp" line="49"/>
        <source>Operation In Progress</source>
        <translation>Aufgabe  wird durchgeführt</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="72"/>
        <source>Is Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="330"/>
        <source>Creation Date</source>
        <translation>Erfassungsdatum</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="331"/>
        <source>ZIP Code</source>
        <translation>Postleitzahl</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="313"/>
        <source>First Name</source>
        <comment>FUCK1</comment>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="315"/>
        <source>Department</source>
        <comment>FUCK1</comment>
        <translation>Abteilung</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="312"/>
        <source>Last Name</source>
        <comment>FUCK1</comment>
        <translation>Nachname</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="332"/>
        <source>Debtor Code</source>
        <translation>Debitorencode</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="333"/>
        <source>Debtor Account</source>
        <translation>Debitorenkonto</translation>
    </message>
    <message>
        <location filename="clientcontactmanager.cpp" line="334"/>
        <source>Customer Code</source>
        <translation>Kundencode</translation>
    </message>
    <message>
        <source>Is part of</source>
        <translation type="obsolete">Bestandteil von</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="474"/>
        <source>Field Name</source>
        <translation>Feldname</translation>
    </message>
    <message>
        <location filename="mainentityselectioncontroller.cpp" line="475"/>
        <source>Table</source>
        <translation>Tabelle</translation>
    </message>
    <message>
        <location filename="clientapihelper.cpp" line="63"/>
        <source>Loading data ...</source>
        <translation>Daten laden ...</translation>
    </message>
    <message>
        <location filename="clientapihelper.cpp" line="64"/>
        <source>Import In Progress</source>
        <translation>Import wird durchgeführt</translation>
    </message>
    <message>
        <location filename="clientapihelper.cpp" line="134"/>
        <source>Importing data ...</source>
        <translation>Daten werden importiert...</translation>
    </message>
</context>
<context>
    <name>RecepientTable_WidgetRow</name>
    <message>
        <location filename="email_recepienttable.cpp" line="456"/>
        <source>Select Contact</source>
        <translation>Kontakt auswählen</translation>
    </message>
    <message>
        <location filename="email_recepienttable.cpp" line="457"/>
        <source>Select Contact Email</source>
        <translation>Kontakt-E-Mail auswählen</translation>
    </message>
    <message>
        <location filename="email_recepienttable.cpp" line="458"/>
        <source>Remove Recipient</source>
        <translation>Empfänger entfernen</translation>
    </message>
</context>
<context>
    <name>SelectionPopupClass</name>
    <message>
        <location filename="selectionpopup.ui" line="14"/>
        <source>Select an Item</source>
        <translation>Element auswählen</translation>
    </message>
    <message>
        <location filename="selectionpopup.ui" line="67"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="selectionpopup.ui" line="74"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Selection_ACP</name>
    <message>
        <location filename="selection_acp.cpp" line="30"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="selection_acp.cpp" line="31"/>
        <source>Remove Assignment</source>
        <translation>Zuweisung entfernen</translation>
    </message>
    <message>
        <location filename="selection_acp.cpp" line="687"/>
        <location filename="selection_acp.cpp" line="731"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>Selection_ActualOrganization</name>
    <message>
        <location filename="selection_actualorganization.cpp" line="29"/>
        <source>Actual Organization:</source>
        <translation>Aktuelle Organisation:</translation>
    </message>
</context>
<context>
    <name>Selection_ContactButton</name>
    <message>
        <location filename="selection_contactbutton.cpp" line="16"/>
        <source>Communicate</source>
        <translation>Kommunikation</translation>
    </message>
    <message>
        <location filename="selection_contactbutton.cpp" line="24"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="selection_contactbutton.cpp" line="25"/>
        <source>Phone Call</source>
        <translation>Anruf</translation>
    </message>
    <message>
        <location filename="selection_contactbutton.cpp" line="26"/>
        <source>Document</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="selection_contactbutton.cpp" line="27"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="selection_contactbutton.cpp" line="28"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="selection_contactbutton.cpp" line="29"/>
        <source>SMS</source>
        <translation>SMS</translation>
    </message>
</context>
<context>
    <name>Selection_Contacts</name>
    <message>
        <source>Build Contact List</source>
        <translation type="obsolete">Kontaktliste aufbauen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="874"/>
        <source>Show Contact Details</source>
        <translation>Kontakt-Details anzeigen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="880"/>
        <source>Show Contact Details In New Window</source>
        <translation>Kontakt-Details in neuem Fenster anzeigen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="886"/>
        <source>Delete Selected Contacts From Database</source>
        <translation>Ausgewählte Kontakte aus der Datenbank löschen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="902"/>
        <source>Clear List</source>
        <translation>Liste leeren</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="906"/>
        <source>Remove Selected Contacts From List</source>
        <translation>Ausgewählte Kontakte aus der Liste entfernen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="907"/>
        <source>Del</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="912"/>
        <source>Remove Duplicates From List</source>
        <translation>Doubletten aus Liste entfernen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="916"/>
        <source>Find Duplicates in Database</source>
        <translation>Doubletten in Datenbank suchen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="926"/>
        <source>Create Organization(s) From Person(s)</source>
        <translation>Organisation(en) zu Person(en) erzeugen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="941"/>
        <source>S&amp;ort</source>
        <translation>S&amp;ortieren</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="942"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="278"/>
        <location filename="selection_contacts.cpp" line="451"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="278"/>
        <source>Do you really want to delete the selected contacts (%1) from the database?</source>
        <translation>Wollen Sie wirklich die ausgewählten Kontakte (%1) endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <source>} from the database?</source>
        <translation type="obsolete">} aus der Datenbank?</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="278"/>
        <location filename="selection_contacts.cpp" line="816"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="278"/>
        <location filename="selection_contacts.cpp" line="816"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="291"/>
        <location filename="selection_contacts.cpp" line="364"/>
        <location filename="selection_contacts.cpp" line="411"/>
        <location filename="selection_contacts.cpp" line="669"/>
        <location filename="selection_contacts.cpp" line="682"/>
        <location filename="selection_contacts.cpp" line="696"/>
        <location filename="selection_contacts.cpp" line="708"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="451"/>
        <source>No person contact selected!</source>
        <translation>Kein Personenkontakt ausgewählt!</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Copy In Progress</source>
        <translation type="obsolete">Kopie wird durchgeführt</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="465"/>
        <source> contact successfully copied!</source>
        <translation> Kontakt erfolgreich kopiert!</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="466"/>
        <location filename="selection_contacts.cpp" line="486"/>
        <location filename="selection_contacts.cpp" line="853"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="486"/>
        <source>No duplicate contact found!</source>
        <translation>Keine Kontakt-Doubletten gefunden!</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="44"/>
        <source>Build Contacts</source>
        <translation>Liste aufbauen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="890"/>
        <source>Add Selected Contacts to Favorites</source>
        <translation>Ausgewählte Kontakte als Favoriten kennzeichnen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="894"/>
        <source>Remove Selected Contacts From Favorites</source>
        <translation>Ausgewählte Kontakte aus Favoriten entfernen</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="47"/>
        <source>Save Contacts To Cache</source>
        <translation>Kontakte in Cache speichern</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="816"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="816"/>
        <source>Do you want to save current contact list into the offline cache?</source>
        <translation>Wollen Sie die aktuelle Kontaktliste in den (Offline-)Cache übernehmen?</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="852"/>
        <source>%1 users generated from contact data!</source>
        <translation>%1 Benutzer aus Kontaktdaten erzeugt!</translation>
    </message>
    <message>
        <location filename="selection_contacts.cpp" line="930"/>
        <source>Create User</source>
        <translation>Benutzer erzeugen</translation>
    </message>
</context>
<context>
    <name>Selection_ContactsAndFavorites</name>
    <message>
        <location filename="selection_contactsandfavorites.cpp" line="21"/>
        <source>Contacts</source>
        <translation>Kontakte</translation>
    </message>
    <message>
        <location filename="selection_contactsandfavorites.cpp" line="22"/>
        <source>Favorites</source>
        <translation>Favoriten</translation>
    </message>
    <message>
        <location filename="selection_contactsandfavorites.cpp" line="57"/>
        <source>Add Favorite</source>
        <translation>Favorit(en) hinzufügen</translation>
    </message>
    <message>
        <location filename="selection_contactsandfavorites.cpp" line="59"/>
        <source>Remove Favorite</source>
        <translation>Favorit(en) entfernen</translation>
    </message>
    <message>
        <location filename="selection_contactsandfavorites.cpp" line="61"/>
        <source>Add Picture</source>
        <translation>Bild Laden</translation>
    </message>
    <message>
        <location filename="selection_contactsandfavorites.cpp" line="63"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
</context>
<context>
    <name>Selection_ContactsAndFavoritesClass</name>
    <message>
        <location filename="selection_contactsandfavorites.ui" line="16"/>
        <source>Selection_ContactsAndFavorites</source>
        <translation>Selection_ContactsAndFavorites</translation>
    </message>
</context>
<context>
    <name>Selection_EditTemplateTree</name>
    <message>
        <location filename="selection_edittemplatetree.cpp" line="20"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="selection_edittemplatetree.cpp" line="21"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
</context>
<context>
    <name>Selection_EditTemplateTreeClass</name>
    <message>
        <location filename="selection_edittemplatetree.ui" line="16"/>
        <source>Edit Items</source>
        <translation>Einträge bearbeiten</translation>
    </message>
    <message>
        <location filename="selection_edittemplatetree.ui" line="60"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="selection_edittemplatetree.ui" line="67"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Selection_GroupTree</name>
    <message>
        <location filename="selection_grouptree.cpp" line="78"/>
        <source>Add New Tree</source>
        <translation>Neuen Baum hinzufügen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="82"/>
        <source>Rename Tree</source>
        <translation>Baum umbenennen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="86"/>
        <source>Delete Tree</source>
        <translation>Baum löschen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="95"/>
        <source>Reload Trees</source>
        <translation>Bäume neu laden</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="116"/>
        <source>Add Group</source>
        <translation>Gruppe hinzufügen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="120"/>
        <source>Rename Group</source>
        <translation>Gruppe umbenennen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="124"/>
        <source>Delete Group</source>
        <translation>Gruppe löschen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="137"/>
        <source>Add Selected </source>
        <comment>Z</comment>
        <translation>Ausgewählte </translation>
    </message>
    <message>
        <source>Remove Selected </source>
        <translation type="obsolete">Ausgewählte </translation>
    </message>
    <message>
        <source> From Group</source>
        <translation type="obsolete"> aus der Gruppe entfernen</translation>
    </message>
    <message>
        <source>Replace Group </source>
        <translation type="obsolete">Gruppen-</translation>
    </message>
    <message>
        <source> With Selected Records</source>
        <translation type="obsolete"> durch ausgewählte Kontakte ersetzen  </translation>
    </message>
    <message>
        <source>Intersect Selected </source>
        <translation type="obsolete">Schnittmenge der ausgewählten </translation>
    </message>
    <message>
        <source> With Group Records</source>
        <translation type="obsolete"> mit Gruppenkontakten</translation>
    </message>
    <message>
        <source>Add Whole Actual </source>
        <translation type="obsolete">Gesamte aktuelle </translation>
    </message>
    <message>
        <source> List to Group</source>
        <translation type="obsolete"> liste zur Gruppe hinzufügen</translation>
    </message>
    <message>
        <source> With Whole Actual </source>
        <translation type="obsolete"> durch gesamte </translation>
    </message>
    <message>
        <source> List</source>
        <translation type="obsolete"> liste ersetzen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="1122"/>
        <location filename="selection_grouptree.cpp" line="1124"/>
        <location filename="selection_grouptree.cpp" line="1282"/>
        <location filename="selection_grouptree.cpp" line="1483"/>
        <source>External Category</source>
        <translation>Externe Kategorie</translation>
    </message>
    <message>
        <source>Import Sub-groups</source>
        <translation type="obsolete">Untergruppen importieren</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="1658"/>
        <source>Startup: Load in Actual Contact List</source>
        <translation>Bei Programmstart: In aktuelle Kontaktliste laden</translation>
    </message>
    <message>
        <source>Refresh Data</source>
        <translation type="obsolete">Daten aktualisieren</translation>
    </message>
    <message>
        <source>Expand All Nodes</source>
        <translation type="obsolete">Alle Zweige öffnen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="314"/>
        <source>Set Group For Load In Actual List (</source>
        <translation>Gruppe zum Laden in die aktuelle Kontaktliste (</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="351"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="352"/>
        <source>Contacts</source>
        <translation>Kontakte</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="355"/>
        <source>Record</source>
        <translation>Datensatz</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="356"/>
        <source>Records</source>
        <translation>Datensätze</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="374"/>
        <location filename="selection_grouptree.cpp" line="632"/>
        <location filename="selection_grouptree.cpp" line="733"/>
        <location filename="selection_grouptree.cpp" line="796"/>
        <location filename="selection_grouptree.cpp" line="849"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="374"/>
        <source>Please Select Tree For Edit!</source>
        <translation>Auswahl Baum zur Bearbeitung!</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="383"/>
        <location filename="selection_grouptree.cpp" line="424"/>
        <location filename="selection_grouptree.cpp" line="538"/>
        <location filename="selection_grouptree.cpp" line="583"/>
        <location filename="selection_grouptree.cpp" line="594"/>
        <location filename="selection_grouptree.cpp" line="605"/>
        <location filename="selection_grouptree.cpp" line="640"/>
        <location filename="selection_grouptree.cpp" line="744"/>
        <location filename="selection_grouptree.cpp" line="810"/>
        <location filename="selection_grouptree.cpp" line="864"/>
        <location filename="selection_grouptree.cpp" line="1276"/>
        <location filename="selection_grouptree.cpp" line="1326"/>
        <location filename="selection_grouptree.cpp" line="1409"/>
        <location filename="selection_grouptree.cpp" line="1445"/>
        <location filename="selection_grouptree.cpp" line="1527"/>
        <location filename="selection_grouptree.cpp" line="1577"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="390"/>
        <source>Go View Mode</source>
        <translation>In Ansichtsmodus wechseln</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="434"/>
        <source>Modify Tree</source>
        <translation>Baum bearbeiten</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="520"/>
        <location filename="selection_grouptree.cpp" line="568"/>
        <source>Enter Tree Name</source>
        <translation>Baumnamen eingeben</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="521"/>
        <location filename="selection_grouptree.cpp" line="569"/>
        <location filename="selection_grouptree.cpp" line="788"/>
        <source>Tree Name:</source>
        <translation>Baumname:</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="632"/>
        <source>Do you really want to delete this tree permanently from the database? Warning: all groups inside will be deleted!</source>
        <translation>Wollen Sie diesen Baum endgültig aus der Datenbank löschen? Warnung: Alle dazugehörigen Gruppen und deren Zuweisungen gehen verloren!</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="632"/>
        <location filename="selection_grouptree.cpp" line="849"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="632"/>
        <location filename="selection_grouptree.cpp" line="849"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="733"/>
        <location filename="selection_grouptree.cpp" line="796"/>
        <source>Group Code And Name Must Be Specified!</source>
        <translation>Gruppencode und -name müssen erfasst sein!</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="787"/>
        <source>Enter New Tree Name</source>
        <translation>Neuen Baumnamen erfassen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="849"/>
        <source>Do you really want to delete selected groups from the database?</source>
        <translation>Wollen Sie die ausgewählten Gruppe endgültig aus der Datenbank löschen?</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="1266"/>
        <source>Enter Group External Category</source>
        <translation>Externe Kategorie für Gruppe erfassen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="1267"/>
        <source>External Category:</source>
        <translation>Externe Kategorie:</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="1295"/>
        <source>Import Groups</source>
        <translation>Gruppen importieren</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="1295"/>
        <source>Import File (*.txt)</source>
        <translation>Datei importieren (*.txt)</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="1326"/>
        <source>Invalid format: column count must be inside [1-4]!</source>
        <translation>Ungültiges Format: 1-4 Spalten müssen vorhanden sein!</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="1648"/>
        <source>Startup: Load in Actual Contact List (</source>
        <translation>Bei Programmstart: In aktuelle Kontaktliste laden (</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="137"/>
        <source> to Group</source>
        <comment>Z</comment>
        <translation> zur Gruppe hinzufügen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="141"/>
        <source>Remove Selected </source>
        <comment>Z</comment>
        <translation>Ausgewählte </translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="141"/>
        <source> From Group</source>
        <comment>Z</comment>
        <translation> aus der Gruppe entfernen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="145"/>
        <location filename="selection_grouptree.cpp" line="163"/>
        <source>Replace Group </source>
        <comment>Z</comment>
        <translation>Gruppen-</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="145"/>
        <source> With Selected Records</source>
        <comment>Z</comment>
        <translation> durch ausgewählte Kontakte ersetzen  </translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="149"/>
        <source>Intersect Selected </source>
        <comment>Z</comment>
        <translation>Schnittmenge der ausgewählten </translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="149"/>
        <source> With Group Records</source>
        <comment>Z</comment>
        <translation> mit Gruppenkontakten</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="159"/>
        <source>Add Whole Actual </source>
        <comment>Z</comment>
        <translation>Gesamte aktuelle </translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="159"/>
        <source> List to Group</source>
        <comment>Z</comment>
        <translation> liste zur Gruppe hinzufügen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="163"/>
        <source> With Whole Actual </source>
        <comment>Z</comment>
        <translation> durch gesamte </translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="163"/>
        <source> List</source>
        <comment>Z</comment>
        <translation> liste ersetzen</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="182"/>
        <source>External Category</source>
        <comment>Z</comment>
        <translation>Externe Kategorie</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="186"/>
        <source>Import Sub-groups</source>
        <comment>Z</comment>
        <translation>Untergruppen importieren</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="195"/>
        <source>Startup: Load in Actual Contact List</source>
        <comment>Z</comment>
        <translation>Bei Programmstart: In aktuelle Kontaktliste laden</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="218"/>
        <source>Refresh Data</source>
        <comment>Z</comment>
        <translation>Daten aktualisieren</translation>
    </message>
    <message>
        <location filename="selection_grouptree.cpp" line="222"/>
        <source>Expand All Nodes</source>
        <comment>Z</comment>
        <translation>Alle Zweige öffnen</translation>
    </message>
</context>
<context>
    <name>Selection_GroupTreeClass</name>
    <message>
        <location filename="selection_grouptree.ui" line="115"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>Selection_NM_SAPNE</name>
    <message>
        <location filename="selection_nm_sapne.cpp" line="35"/>
        <source>Modify/Create</source>
        <translation>Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <location filename="selection_nm_sapne.cpp" line="36"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="selection_nm_sapne.cpp" line="37"/>
        <source>Remove Assignment</source>
        <translation>Zuweisung entfernen</translation>
    </message>
    <message>
        <location filename="selection_nm_sapne.cpp" line="38"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="selection_nm_sapne.cpp" line="39"/>
        <source>Open In New Window</source>
        <translation>In neuem Fenster öffnen</translation>
    </message>
    <message>
        <location filename="selection_nm_sapne.cpp" line="104"/>
        <source>Data</source>
        <translation>Daten</translation>
    </message>
</context>
<context>
    <name>Selection_NM_SAPNEClass</name>
    <message>
        <location filename="selection_nm_sapne.ui" line="34"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Assigned NM Records&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Zugewiesene Verknüpfungen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Selection_RecordSelectorClass</name>
    <message>
        <location filename="selection_recordselector.ui" line="16"/>
        <source>Selection_RecordSelector</source>
        <translation>Selection_RecordSelector</translation>
    </message>
</context>
<context>
    <name>Selection_SAPNE</name>
    <message>
        <location filename="selection_sapne.cpp" line="29"/>
        <source>Modify/Create</source>
        <translation>Bearbeiten/Einfügen</translation>
    </message>
    <message>
        <location filename="selection_sapne.cpp" line="30"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="selection_sapne.cpp" line="31"/>
        <source>Remove Assignment</source>
        <translation>Zuweisung entfernen</translation>
    </message>
    <message>
        <location filename="selection_sapne.cpp" line="32"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="selection_sapne.cpp" line="33"/>
        <source>View Details</source>
        <translation>Zeige Details</translation>
    </message>
    <message>
        <location filename="selection_sapne.cpp" line="634"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="selection_sapne.cpp" line="634"/>
        <source>No Assignment Made!</source>
        <translation>Keine bestehende Zuweisung!</translation>
    </message>
    <message>
        <location filename="selection_sapne.cpp" line="476"/>
        <source>Access Denied</source>
        <translation>Zugriff verweigert</translation>
    </message>
</context>
<context>
    <name>Selection_SAPNEClass</name>
    <message>
        <location filename="selection_sapne.ui" line="181"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;label&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;Bezeichnung&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Selection_ScheduleButton</name>
    <message>
        <location filename="selection_schedulebutton.cpp" line="15"/>
        <source>Schedule Follow-Up</source>
        <translation>Folgeaufgabe planen</translation>
    </message>
    <message>
        <source>Communicate</source>
        <translation type="obsolete">Kommunikation</translation>
    </message>
    <message>
        <location filename="selection_schedulebutton.cpp" line="22"/>
        <source>Document</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="selection_schedulebutton.cpp" line="24"/>
        <source>Local File </source>
        <translation>Lokale Datei  </translation>
    </message>
    <message>
        <location filename="selection_schedulebutton.cpp" line="25"/>
        <source>Internet File</source>
        <translation>Internet-Datei</translation>
    </message>
    <message>
        <location filename="selection_schedulebutton.cpp" line="26"/>
        <source>Note</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location filename="selection_schedulebutton.cpp" line="27"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="selection_schedulebutton.cpp" line="28"/>
        <source>Paper Document</source>
        <translation>Papier-Dokument</translation>
    </message>
    <message>
        <location filename="selection_schedulebutton.cpp" line="35"/>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="selection_schedulebutton.cpp" line="36"/>
        <source>Phone Call</source>
        <translation>Telefonanruf</translation>
    </message>
</context>
<context>
    <name>Selection_TreeBased</name>
    <message>
        <location filename="selection_treebased.cpp" line="64"/>
        <source>&amp;Reload</source>
        <translation>Neu l&amp;aden</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="69"/>
        <source>&amp;Find</source>
        <translation>&amp;Suchen</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="75"/>
        <source>Show Details In New Window</source>
        <translation>Details in neuem Fenster anzeigen</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="96"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="101"/>
        <location filename="selection_treebased.cpp" line="670"/>
        <source>New From Template</source>
        <translation>Neu aus Vorlage</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="106"/>
        <source>New Substructure From Template</source>
        <translation>Neue Unterstruktur aus Vorlage erzeugen</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="153"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="159"/>
        <source>&amp;Move</source>
        <translation>&amp;Verschieben</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="165"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="170"/>
        <source>&amp;Delete</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="119"/>
        <source>Load Project List</source>
        <translation>Projektliste laden</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="124"/>
        <location filename="selection_treebased.cpp" line="667"/>
        <source>Create Project List</source>
        <translation>Projektliste neu</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="129"/>
        <location filename="selection_treebased.cpp" line="664"/>
        <source>Modify Project List</source>
        <translation>Projektliste bearbeiten</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="134"/>
        <location filename="selection_treebased.cpp" line="666"/>
        <source>Delete Project List</source>
        <translation>Projektliste löschen</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="442"/>
        <location filename="selection_treebased.cpp" line="469"/>
        <location filename="selection_treebased.cpp" line="557"/>
        <location filename="selection_treebased.cpp" line="601"/>
        <source>You don&apos;t have the rights to perform this operation!</source>
        <translation>Kein Zugriffsrecht!</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="139"/>
        <source>Hide Inactive Projects</source>
        <translation>Inaktive Projekte ausblenden</translation>
    </message>
    <message>
        <location filename="selection_treebased.cpp" line="738"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Selection_TreeBasedClass</name>
    <message>
        <location filename="selection_treebased.ui" line="251"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionCheckBoxWizPageClass</name>
    <message>
        <location filename="simpleselectioncheckboxwizpage.ui" line="19"/>
        <source>Selection</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="simpleselectioncheckboxwizpage.ui" line="27"/>
        <source>CheckBox</source>
        <translation>Checkbox</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizFileSelection</name>
    <message>
        <location filename="simpleselectionwizfileselection.cpp" line="26"/>
        <source>Select directory</source>
        <translation>Verzeichnis wählen</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselection.cpp" line="48"/>
        <source>Select File</source>
        <translation>Datei wählen</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselection.cpp" line="50"/>
        <source>Text file (*.txt)</source>
        <translation>Text-Datei (*.txt)</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizFileSelectionClass</name>
    <message>
        <location filename="simpleselectionwizfileselection.ui" line="13"/>
        <source>SimpleSelectionWizFileSelection</source>
        <translation>SimpleSelectionWizFileSelection</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselection.ui" line="19"/>
        <source>File Path:</source>
        <translation>Dateipfad:</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizFileSelectionExp</name>
    <message>
        <location filename="simpleselectionwizfileselectionexp.cpp" line="29"/>
        <source>Select directory</source>
        <translation>Verzeichnis wählen</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselectionexp.cpp" line="53"/>
        <source>Select File</source>
        <translation>Datei wählen</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselectionexp.cpp" line="55"/>
        <source>Text file (*.txt)</source>
        <translation>Text-Datei (*.txt)</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizFileSelectionExpClass</name>
    <message>
        <location filename="simpleselectionwizfileselectionexp.ui" line="13"/>
        <source>SimpleSelectionWizFileSelectionExp</source>
        <translation>SimpleSelectionWizFileSelectionExp</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselectionexp.ui" line="21"/>
        <source>File Path:</source>
        <translation>Dateipfad:</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselectionexp.ui" line="53"/>
        <source>Include SOKRATES(R) Header</source>
        <translation>SOKRATES(R) Dateiheader verwenden</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselectionexp.ui" line="63"/>
        <source>Add Column Headers</source>
        <translation>Spaltenüberschriften einfügen</translation>
    </message>
    <message>
        <location filename="simpleselectionwizfileselectionexp.ui" line="70"/>
        <source>Export for Outlook(R)</source>
        <translation>Nach Outlook (R) exportieren</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizPage</name>
    <message>
        <location filename="simpleselectionwizpage.cpp" line="28"/>
        <source>Add Elements to Selection</source>
        <translation>Elemente zur Auswahl hinzufügen</translation>
    </message>
    <message>
        <location filename="simpleselectionwizpage.cpp" line="30"/>
        <source>Add Elements and Subelements to Selection</source>
        <translation>Elemente und Unterelemente zur Auswahl hinzufügen</translation>
    </message>
    <message>
        <location filename="simpleselectionwizpage.cpp" line="32"/>
        <source>Remove From Selection</source>
        <translation>Aus Auswahl entfernen</translation>
    </message>
</context>
<context>
    <name>SimpleSelectionWizPageClass</name>
    <message>
        <location filename="simpleselectionwizpage.ui" line="14"/>
        <source>Selection Page</source>
        <translation>Auswahl Seite</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation type="obsolete">&gt;&gt;</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
    <message>
        <location filename="simpleselectionwizpage.ui" line="106"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <location filename="simpleselectionwizpage.ui" line="86"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
</context>
<context>
    <name>StoredProjListsDlgClass</name>
    <message>
        <location filename="storedprojlistsdlg.ui" line="16"/>
        <source>Stored Project Lists</source>
        <translation>Gespeicherte Projektlisten</translation>
    </message>
    <message>
        <location filename="storedprojlistsdlg.ui" line="24"/>
        <source>Select a stored project list:</source>
        <translation>Gespeicherte Projektliste auswählen:</translation>
    </message>
    <message>
        <location filename="storedprojlistsdlg.ui" line="62"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="storedprojlistsdlg.ui" line="72"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>StoredProjListsEditor</name>
    <message>
        <source>Project Lists</source>
        <translation type="obsolete">Projektlisten</translation>
    </message>
    <message>
        <source>Actual Project List</source>
        <translation type="obsolete">Aktuelle Projektliste</translation>
    </message>
    <message>
        <source>Load:</source>
        <translation type="obsolete">Laden:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.cpp" line="185"/>
        <source>List name must not be empty!</source>
        <translation>Der Name der Liste darf nicht leer sein!</translation>
    </message>
    <message>
        <source>Loading Data...</source>
        <translation type="obsolete">Daten werden geladen...</translation>
    </message>
    <message>
        <source>Read In Progress</source>
        <translation type="obsolete">Lesen in Arbeit</translation>
    </message>
</context>
<context>
    <name>StoredProjListsEditorClass</name>
    <message>
        <location filename="storedprojlistseditor.ui" line="17"/>
        <source>Stored Project Lists</source>
        <translation>Gespeicherte Projektlisten</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="34"/>
        <source>Project Lists</source>
        <translation>Projektlisten</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="48"/>
        <source>Actual Project List</source>
        <translation>Aktuelle Projektliste</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="55"/>
        <source>Load:</source>
        <translation>Laden:</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="88"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="98"/>
        <source>Type:</source>
        <translation>Art:</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="125"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="132"/>
        <source>Selection</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="142"/>
        <source>Collection</source>
        <translation>Kollektion</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="200"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="storedprojlistseditor.ui" line="210"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Table_SelectionContacts</name>
    <message>
        <location filename="table_selectioncontacts.cpp" line="81"/>
        <location filename="table_selectioncontacts.cpp" line="129"/>
        <location filename="table_selectioncontacts.cpp" line="261"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>Table_SelectionTableBased</name>
    <message>
        <location filename="table_selectiontablebased.cpp" line="39"/>
        <source>&amp;Reload</source>
        <translation>Neu l&amp;aden</translation>
    </message>
    <message>
        <location filename="table_selectiontablebased.cpp" line="54"/>
        <source>S&amp;ort</source>
        <translation>S&amp;ortieren</translation>
    </message>
    <message>
        <location filename="table_selectiontablebased.cpp" line="55"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
</context>
<context>
    <name>Thick_Client</name>
    <message>
        <source>Access not allowed from thick client!</source>
        <translation type="obsolete">Kein Zugriff aus Personal Edition!</translation>
    </message>
    <message>
        <source>Client successfully logged to: </source>
        <translation type="obsolete">Client erfolgreich verbunden mit: </translation>
    </message>
    <message>
        <source>Your version %1 is not actual any more. Do you want to download and install an update to version %2 now?</source>
        <translation type="obsolete">Version %1 ist nicht mehr aktuell. Wollen Sie jetzt die neue Version %2 herunterladen und installieren?</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <source>License file is missing or corrupted!</source>
        <translation type="obsolete">Linzenzschlüsseldatei (Keyfile) fehlt oder ist beschädigt!</translation>
    </message>
</context>
<context>
    <name>Thin_Client</name>
    <message>
        <source>Access not allowed from thin client!</source>
        <translation type="obsolete">Zugriff aus Internet-Client nicht zugelassen!</translation>
    </message>
    <message>
        <source>Client successfully logged to: </source>
        <translation type="obsolete">Client erfolgreich verbunden mit: </translation>
    </message>
</context>
<context>
    <name>ToolBar_DateSelectorClass</name>
    <message>
        <location filename="toolbar_dateselector.ui" line="22"/>
        <source>Select Date</source>
        <translation>Auswahl Datum</translation>
    </message>
    <message>
        <location filename="toolbar_dateselector.ui" line="42"/>
        <source>Current Date</source>
        <translation>Aktuelles Datum</translation>
    </message>
    <message>
        <location filename="toolbar_dateselector.ui" line="49"/>
        <source>Per Date</source>
        <translation>Per Datum</translation>
    </message>
</context>
<context>
    <name>WidgetRow</name>
    <message>
        <location filename="email_attachmenttable.cpp" line="652"/>
        <source>ZIP</source>
        <translation>ZIP</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="626"/>
        <source>Add Attachment</source>
        <translation>Anhang hinzufügen</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="627"/>
        <source>Remove Attachment</source>
        <translation>Anhang entfernen</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="628"/>
        <source>Open Attachment</source>
        <translation>Anhang öffnen</translation>
    </message>
    <message>
        <location filename="email_attachmenttable.cpp" line="629"/>
        <source>Save Attachment</source>
        <translation>Anhang speichern</translation>
    </message>
</context>
<context>
    <name>ZipSelectorDlg</name>
    <message>
        <location filename="zipselectordlg.cpp" line="113"/>
        <source>ZIP</source>
        <translation>PLZ</translation>
    </message>
    <message>
        <location filename="zipselectordlg.cpp" line="113"/>
        <source>Town</source>
        <translation>Ort</translation>
    </message>
    <message>
        <location filename="zipselectordlg.cpp" line="113"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="zipselectordlg.cpp" line="123"/>
        <source>XML parsing failed.</source>
        <translation>XML-Analyse abgebrochen.</translation>
    </message>
    <message>
        <location filename="zipselectordlg.cpp" line="136"/>
        <source>You need to select a row first.</source>
        <translation>Bitte wählen Sie zuerst eine Zeile.</translation>
    </message>
</context>
<context>
    <name>ZipSelectorDlgClass</name>
    <message>
        <location filename="zipselectordlg.ui" line="13"/>
        <source>ZIP/Town Selector</source>
        <translation>Auswahl PLZ/Ort</translation>
    </message>
    <message>
        <location filename="zipselectordlg.ui" line="40"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="zipselectordlg.ui" line="63"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="zipselectordlg.ui" line="70"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>contactbuildall_searchfields</name>
    <message>
        <location filename="contactbuildall_searchfields.ui" line="20"/>
        <source>contactbuildall_searchfields</source>
        <translation>contactbuildall_searchfields</translation>
    </message>
    <message>
        <location filename="contactbuildall_searchfields.ui" line="41"/>
        <source>Search Field 1:</source>
        <translation>Suchfeld 1:</translation>
    </message>
    <message>
        <location filename="contactbuildall_searchfields.ui" line="114"/>
        <location filename="contactbuildall_searchfields.ui" line="140"/>
        <location filename="contactbuildall_searchfields.ui" line="226"/>
        <location filename="contactbuildall_searchfields.ui" line="252"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="contactbuildall_searchfields.ui" line="163"/>
        <source>Search Field 2:</source>
        <translation>Suchfeld 2:</translation>
    </message>
</context>
<context>
    <name>generatecodedlg</name>
    <message>
        <location filename="generatecodedlg.cpp" line="70"/>
        <source>Could not find parent node!</source>
        <translation>Übergeordnetes element nicht gefunden!</translation>
    </message>
    <message>
        <location filename="generatecodedlg.cpp" line="190"/>
        <source>This code is already taken!
Please try another one.</source>
        <translation>Code schon verwendet!
Bitte neuen Code eingeben.</translation>
    </message>
</context>
<context>
    <name>generatecodedlgClass</name>
    <message>
        <location filename="generatecodedlg.ui" line="16"/>
        <source>Define New Hierarchical Code</source>
        <translation>Neuen hierarchischen Code eingeben</translation>
    </message>
    <message>
        <location filename="generatecodedlg.ui" line="44"/>
        <source>New Name:</source>
        <translation>Neuer Name:</translation>
    </message>
    <message>
        <location filename="generatecodedlg.ui" line="61"/>
        <source>Actual Code:</source>
        <translation>Aktueller Code:</translation>
    </message>
    <message>
        <location filename="generatecodedlg.ui" line="68"/>
        <source>New Code:</source>
        <translation>Neuer Code:</translation>
    </message>
    <message>
        <location filename="generatecodedlg.ui" line="81"/>
        <source>Use Template</source>
        <translation>Vorlage verwenden</translation>
    </message>
    <message>
        <location filename="generatecodedlg.ui" line="122"/>
        <source>Proposal Same Level</source>
        <translation>Vorschlag gleiche Stufe</translation>
    </message>
    <message>
        <location filename="generatecodedlg.ui" line="137"/>
        <source>Proposal Sub-Level</source>
        <translation>Vorschlag Unterstufe</translation>
    </message>
    <message>
        <location filename="generatecodedlg.ui" line="167"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="generatecodedlg.ui" line="177"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
</TS>
