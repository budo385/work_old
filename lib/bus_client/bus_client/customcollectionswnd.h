#ifndef CUSTOMCOLLECTIONSWND_H
#define CUSTOMCOLLECTIONSWND_H

#include <QWidget>
#include "ui_customcollectionswnd.h"

class CustomCollectionsWnd : public QWidget
{
	Q_OBJECT

public:
	CustomCollectionsWnd(QWidget *parent = 0);
	~CustomCollectionsWnd();

private:
	Ui::CustomCollectionsWnd ui;
};

#endif // CUSTOMCOLLECTIONSWND_H
