#include "dlg_reminders.h"
#include "gui_core/gui_core/macros.h"


#include "bus_client/bus_client/clientremindermanager.h"
extern ClientReminderManager		*g_ReminderManager;

Dlg_Reminders::Dlg_Reminders(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

Dlg_Reminders::~Dlg_Reminders()
{


}


void Dlg_Reminders::NewReminderReceived(int nReminderID)
{
	m_lstReminders.clear();
	ui.tableWidget->RefreshDisplay();

	Status err;
	g_ReminderManager->ReloadUserReminders(err,m_lstReminders);
	_CHK_ERR(err);

	g_ReminderManager->RecalcDueTimes(m_lstReminders);

	
}