#ifndef CLIENTIMPORTEXPORTMANAGER_H
#define CLIENTIMPORTEXPORTMANAGER_H

#include <QMutex>
#include <QFileSystemWatcher>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "bus_core/bus_core/importexportmanagerabstract.h"



class ClientImportExportManager : public ImportExportManagerAbstract
{
	Q_OBJECT
	
public:
	ClientImportExportManager();

	void SetClientMode(bool bIsThinClient);
	bool IsUnicodeEnabled();
	void ExportProjects(Status &pStatus, QString strFileName,bool bUtf8,DbRecordSet *plstProjects=NULL); 
	void ExportUsers(Status &pStatus, QString strFileName,bool bUtf8,DbRecordSet *plstUsers=NULL); 
	void ExportContacts(Status &pStatus, QString strFileName, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, DbRecordSet *plstUsers=NULL);
	void ExportContactContactRelationships(Status &pStatus, QString strFileName, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, DbRecordSet *plstUsers=NULL);
	void ThickModeRestartTimer();
	void StopThickModeTimer();
	void GetSettings(Status &pStatus,DbRecordSet &settings);

private slots:
	void OnImportExportManager_GlobalPeriodChanged(int nPeriodMin);
	void OnImportExportManager_StartTask(int nCallerID=-1,int nTaskID=-1);
private:
	void timerEvent(QTimerEvent *event);
	int m_nTimerID;
	void CheckFileForExport(Status &pStatus,QString strPath);
	bool m_bIsThinClient;

};


#endif // CLIENTIMPORTEXPORTMANAGER_H
