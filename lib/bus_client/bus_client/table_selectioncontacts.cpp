#include "table_selectioncontacts.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "bus_client/bus_client/selection_grouptree.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "common/common/entity_id_collection.h"
#include "gui_core/gui_core/thememanager.h"
#include "common/common/datahelper.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;


Table_SelectionContacts::Table_SelectionContacts(QWidget *parent)
:UniversalTableWidgetEx(parent)
{
	//issue 1417
	horizontalHeader()->hide();
	verticalHeader()->hide(); //issue 1634
	//issue 1398
	//issue 1634
	//verticalHeader()->setDefaultSectionSize(20);
	setStyleSheet(ThemeManager::GetContactGridBkg());
	setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	setShowGrid(false);

	EnableDrop(true);
	EnableDrag(true,ENTITY_BUS_CONTACT,":Contacts32.png");
	connect(this,SIGNAL(SignalDataDroped(int,DbRecordSet &,QDropEvent *)),this,SLOT(OnDataDroped(int,DbRecordSet &,QDropEvent *)));
	SetEditMode(true);

}


//accepts drops from group/actual grid+tree of groups
//void Table_SelectionContacts::DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
void Table_SelectionContacts::OnDataDroped(int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
{
	int nSize=DroppedValue.getRowCount();
	if(nSize==0)
		return;

	//cant drop onself
	if(event->source()==this)
	{
		event->ignore();
		return;
	}
	//DroppedValue.Dump();

	switch(nDropType)
	{
	case ENTITY_BUS_CONTACT:
		{
			m_plstData->merge(DroppedValue);//cache??
			m_plstData->removeDuplicates(0);
			SortList();
			RefreshDisplay();
			emit ContentChanged();
		}
		break;
	case ENTITY_BUS_GROUP_ITEMS:
		{
			//check if source is good (must be contact):
			//Selection_GroupTree* pSource=dynamic_cast<Selection_GroupTree*>(event->source());
			//Q_ASSERT(pSource);
			//if(pSource->GetSelectionEntity()!=ENTITY_BUS_CONTACT) return;

			//set filter on groups:
			QString strWhereIn;
			DbSqlTableDefinition::GenerateChunkedWhereStatement(DroppedValue,"BGIT_ID",strWhereIn,0,nSize+1);

			//read from server:
			Status err;
			_SERVER_CALL(BusContact->ReadActiveContactsFromGroups(err,strWhereIn,DroppedValue))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}

			MainEntityFilter Filter;

/*
			Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID IN "+strWhereIn);

			//read from server:
			Status err;
			_SERVER_CALL(BusContact->ReadShortContactList(err,Filter.Serialize(),DroppedValue))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return;
			}
*/


			//issue 2657: exclude excluded group:
			//issue 1410:
			//load all contact from options and person groups, remove duplicates
			//remove them from list:
			//issue 1410---------------------------------------------------------------------------------------------
				Filter.ClearFilter();
				int nOptionExGroupID=g_pSettings->GetApplicationOption(APP_CONTACT_GROUP_EXCLUDE_ID).toInt();
				int nSettingExGroupID=g_pSettings->GetPersonSetting(CONTACT_GROUP_EXCLUDE_ID).toInt();
				if (nOptionExGroupID>0)
				{
					Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID="+QVariant(nOptionExGroupID).toString());
				}
				if (nSettingExGroupID>0)
				{
					Filter.ClearFilter();
					if (nOptionExGroupID>0)
						Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID IN ("+QVariant(nOptionExGroupID).toString()+","+QVariant(nSettingExGroupID).toString()+")");
					else
						Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID="+QVariant(nSettingExGroupID).toString());
				}
				DbRecordSet lstExFilter=Filter.Serialize();
				if (lstExFilter.getRowCount()>0)
				{
					DbRecordSet lstExcludeData;
					_SERVER_CALL(MainEntitySelector->ReadData(err,ENTITY_BUS_CONTACT,Filter.Serialize(),lstExcludeData))
						//error:
						if(!err.IsOK())
						{
							QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
							QApplication::restoreOverrideCursor();
							return;
						}
						else
						{
							lstExcludeData.removeDuplicates(0);
							DataHelper::DataOperation(DataHelper::OP_REMOVE,&lstExcludeData,&DroppedValue);
						}
				}



			//issue 1410---------------------------------------------------------------------------------------------



			m_plstData->merge(DroppedValue);
			m_plstData->removeDuplicates(0);
			SortList();
			RefreshDisplay();
			emit ContentChanged();
		}
	    break;

	}

}

void Table_SelectionContacts::OnSortColumn(int nColPos)
{

		int nColType=m_lstColumnSetup.getDataRef(nColPos,2).toInt();
		if(nColType>COL_TYPE_CHECKBOX) return; //no sorting on special widgets

		int nSortOrder = m_lstColumnSetup.getDataRef(nColPos,6).toInt(); // 0 - asc ,1 -desc
		//revert existing sort order (when first time, probably it go descending)
		nSortOrder = (nSortOrder==1 ? 0:1); 

		//store last:
		m_lstLastSort.clear();
		m_lstLastSort<<SortData(m_lstColumnMapping[nColPos],nSortOrder);
		horizontalHeader()->setSortIndicator(nColPos, (Qt::SortOrder)nSortOrder);


		SortDataList lstSort;

		//asc
		if (nSortOrder==0)
		{
			lstSort<<SortData(m_plstData->getColumnIdx("BCNT_ORGANIZATIONNAME"),0);
			lstSort<<SortData(m_plstData->getColumnIdx("BCNT_LASTNAME"),0);
			lstSort<<SortData(m_plstData->getColumnIdx("BCNT_FIRSTNAME"),0);
		}
		else
		{
			lstSort<<SortData(m_plstData->getColumnIdx("BCNT_ORGANIZATIONNAME"),1);
			lstSort<<SortData(m_plstData->getColumnIdx("BCNT_LASTNAME"),1);
			lstSort<<SortData(m_plstData->getColumnIdx("BCNT_FIRSTNAME"),1);
		}

		m_plstData->sortMulti(lstSort);
		RefreshDisplay();

		m_lstColumnSetup.setData(nColPos,6,nSortOrder); //store back reverse


}

//nSortOrder=-1 (use last sort order if set, 0->asc by name, 1-desc by name)
void Table_SelectionContacts::SortList(int nSortOrder)
{

	//store last:
	if (nSortOrder==-1 && m_lstLastSort.size()>0 )
	{
		if (m_lstLastSort.at(0).m_nColumn==5)
		{
			m_plstData->sortMulti(m_lstLastSort);
			RefreshDisplay();
			return;
		}
	}

	m_lstLastSort.clear();
	m_lstLastSort<<SortData(m_lstColumnMapping[0],nSortOrder);
	horizontalHeader()->setSortIndicator(0, (Qt::SortOrder)nSortOrder);


	if (nSortOrder==-1)nSortOrder=0;

	SortDataList lstSort;

	//asc
	if (nSortOrder==0)
	{
		lstSort<<SortData(m_plstData->getColumnIdx("BCNT_ORGANIZATIONNAME"),0);
		lstSort<<SortData(m_plstData->getColumnIdx("BCNT_LASTNAME"),0);
		lstSort<<SortData(m_plstData->getColumnIdx("BCNT_FIRSTNAME"),0);
	}
	else
	{
		lstSort<<SortData(m_plstData->getColumnIdx("BCNT_ORGANIZATIONNAME"),1);
		lstSort<<SortData(m_plstData->getColumnIdx("BCNT_LASTNAME"),1);
		lstSort<<SortData(m_plstData->getColumnIdx("BCNT_FIRSTNAME"),1);
	}

	m_plstData->sortMulti(lstSort);
	RefreshDisplay();

	m_lstColumnSetup.setData(0,6,nSortOrder); //store back reverse
	ScrollToLastSelectedItem();

}


void Table_SelectionContacts::LoadDataFromGroup(int nGroupID,DbRecordSet &lstAllNodes)
{
	int nSize=lstAllNodes.getRowCount();
	if(nSize==0)
		return;

	QString strWhereIn;
	DbSqlTableDefinition::GenerateChunkedWhereStatement(lstAllNodes,"BGIT_ID",strWhereIn,0,nSize+1);
	MainEntityFilter Filter;
	Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID IN "+strWhereIn);

	//read from server:
	Status err;
	_SERVER_CALL(BusContact->ReadShortContactList(err,Filter.Serialize(),lstAllNodes))
	if(!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		return;
	}
	m_plstData->merge(lstAllNodes);
	m_plstData->removeDuplicates(0);
	SortList();
	RefreshDisplay();


}




void Table_SelectionContacts::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	UniversalTableWidgetEx::RefreshDisplay(nRow,bApplyLastSortModel);

//issue 1634: in mobile hide numbers:
#ifdef WINCE
	setColumnWidth(0,0);
#endif
	

	//issue 1634:
	/*
	A. Person without Organization: Arial 10pt bold
	B. Organization: Arial 10 pt bold, add an icon Organization_Icon_White.png between line number (Arial 10 pt normal) and the name.
	C. Person With Organization: If the line above is *not* the same organization and *not* a person with the same organization, the organization follows the name separated by a comma in another font style (Arial 8 pt italic).
	D. Person With Organization: If the line above *is* the same organization or a person with the same organization, the organization is *not* shown, but the name is shown in another style (Arial 10 pt non-bold italic) and is shifted to the right. This way, the first contact *not* belonging to this organization will be shown with a name more left and makes clear where the organization ends.
	*/

#ifndef WINCE
	QFont strStyle1("Arial",10,QFont::Bold);
	QFont strStyle3("Arial",10,QFont::DemiBold,true);
	QFont strStyle4("Arial",10,QFont::DemiBold,true);
#else
	QFont strStyle1("Arial",8,QFont::DemiBold);
	QFont strStyle3("Arial",8,QFont::Normal,true);
	QFont strStyle4("Arial",8,QFont::Normal,true);
#endif


	strStyle1.setStyleStrategy(QFont::PreferAntialias);
	strStyle3.setStyleStrategy(QFont::PreferAntialias);
	strStyle4.setStyleStrategy(QFont::PreferAntialias);

	setUpdatesEnabled(false);
	blockSignals(true);
	bool bOrgPersonStarted=false;

	int nCount = m_plstData->getRowCount();
	for(int i=0; i<nCount; i++)
	{
		//item(i,0)->setFont(strStyle1);
		item(i,0)->setText(QVariant(i+1).toString());


		item(i,1)->setToolTip(item(i,1)->text()); //set as tooltip: issue 1661


		if(m_plstData->getDataRef(i, "BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION)
		{
			item(i,1)->setIcon(QIcon(":Icon_Organization_White.png"));
			//item(i,1)->setFont(strStyle1);
			bOrgPersonStarted=false;
		}
		else //person:
		{
			item(i,1)->setIcon(QIcon(""));
			if(m_plstData->getDataRef(i, "BCNT_ORGANIZATIONNAME").toString().isEmpty()) //person without org
			{
				//item(i,1)->setFont(strStyle1);
				bOrgPersonStarted=false;
				continue;
			}
			else //person with org
			{
				bool bOrgPrev=false;
				if (i>0)
				{
					if (m_plstData->getDataRef(i-1, "BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION)
						bOrgPrev=true;

					if (m_plstData->getDataRef(i-1, "BCNT_ORGANIZATIONNAME").toString()!=m_plstData->getDataRef(i, "BCNT_ORGANIZATIONNAME").toString())
					{
						//item(i,1)->setFont(strStyle4);
						bOrgPersonStarted=false;
						continue;
					}
				}
				
				if (bOrgPrev || bOrgPersonStarted)
				{
					QString strName=m_plstData->getDataRef(i, "BCNT_NAME").toString();
					strName="       "+strName.left(strName.indexOf(",")).trimmed();
					item(i,1)->setText(strName);
					item(i,1)->setToolTip(strName); //set as tooltip: issue 1661
					//item(i,1)->setFont(strStyle3);
					bOrgPersonStarted=true;
				}
				else
				{
					//item(i,1)->setFont(strStyle4);
					bOrgPersonStarted=false;
				}
			}
		}

		//issue 1634: add number when not WINCE:


	}

	setUpdatesEnabled(true);
	blockSignals(false);

}


