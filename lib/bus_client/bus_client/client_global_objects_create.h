#ifndef CLIENT_GLOBAL_OBJECTS
#define CLIENT_GLOBAL_OBJECTS


//-----------------------------------------------------------------
//				GLOBAL OBJECT CREATOR (APPLICATION FRAMEWORK)
//-----------------------------------------------------------------


#include "common/common/cliententitycache.h"
#include "modulelicenseclientcache.h"
#include "useraccessright_client.h"
#include "changemanager.h"
#include "common/common/logger.h"
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
#include "bus_client/bus_client/clientdownloadmanager.h"
#include "bus_client/bus_client/clientremindermanager.h"

//ESSENTIAL GLOBALS:
UserAccessRight					*g_AccessRight;		//global access right tester
ModuleLicenseClientCache		g_FunctionPoint;	//global function point tester
ClientEntityCache				g_ClientCache;		//global client cache
ChangeManager					g_ChangeManager;	//global message dispatcher
Logger							g_Logger;			//global logger
ClientOptionsAndSettingsManager *g_pSettings;		//settings manager;
ClientDownloadManager			*g_DownloadManager;
QTranslator *g_translator;
ClientReminderManager			*g_ReminderManager;

#endif 