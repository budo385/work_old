#ifndef QVIDEOSPLASH_H
#define QVIDEOSPLASH_H

#include <QWidget>
#include "generatedfiles/ui_qvideosplash.h"

class QVideoSplash : public QWidget
{
	Q_OBJECT

public:
	QVideoSplash(QString &strVideoPath, QWidget *parent = 0);
	~QVideoSplash();

private:
	Ui::QVideoSplashClass ui;
};

#endif // QVIDEOSPLASH_H
