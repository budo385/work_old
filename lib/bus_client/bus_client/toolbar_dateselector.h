#ifndef TOOLBAR_DATESELECTOR_H
#define TOOLBAR_DATESELECTOR_H

#include <QtWidgets/QFrame>
#include "generatedfiles/ui_toolbar_dateselector.h"
#include "gui_core/gui_core/guifieldmanager.h"
#include "gui_core/gui_core/guifield_validator.h"



class ToolBar_DateSelector : public QFrame, public GuiField_Validator
{
    Q_OBJECT

public:
    ToolBar_DateSelector(QWidget *parent = 0);
    ~ToolBar_DateSelector();

	bool ValidateUserInput(QWidget *pWidget,int nRow, int nCol, QVariant& value);


	QDate GetDate(){return m_Values.getDataRef(0,1).toDate();}
	void Initialize();


signals:
	void DateChanged(QDate);
private:
    Ui::ToolBar_DateSelectorClass ui;

	GuiFieldManager *m_GuiFieldManagerMain;
	GuiDataManipulator *m_pData;

	DbRecordSet m_Values;
};

#endif // TOOLBAR_DATESELECTOR_H
